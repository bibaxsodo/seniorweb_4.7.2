﻿Imports System.Data.OleDb
Imports System.Web
Imports System.Web.Services
Namespace WebHandler

    Public Class InserisciRigaBudget
        Implements System.Web.IHttpHandler, IRequiresSessionState

        Public Sub ProcessRequest(ByVal context As HttpContext) Implements IHttpHandler.ProcessRequest
            Dim Anno As String = context.Request.QueryString("Anno")
            Dim Livello1 As String = context.Request.QueryString("Liveollo1")
            Dim Livello2 As String = context.Request.QueryString("Livello2")
            Dim Livello3 As String = context.Request.QueryString("Livello3")
            Dim Colonna As String = context.Request.QueryString("Colonna")
            Dim Importo As String = context.Request.QueryString("Importo")
            Dim Utente As String = context.Request.QueryString("UTENTE")
            context.Response.Cache.SetCacheability(HttpCacheability.NoCache)


            Dim DbC As New Cls_Login
            Dim cn As OleDbConnection

            DbC.Utente = context.Session("UTENTE")
            DbC.LeggiSP(context.Application("SENIOR"))



            cn = New Data.OleDb.OleDbConnection(DbC.Generale)


            cn.Open()

            Dim CommandBudgetVR As New OleDbCommand("Select * From Budget Where Anno = ? And Livello1 = ? And Livello2 = ? And Livello3 = ? And Colonna = ?", cn)
            Dim TotImpo As Double = 0

            CommandBudgetVR.Parameters.AddWithValue("@Anno", Anno)
            CommandBudgetVR.Parameters.AddWithValue("@Livello1", Livello1)
            CommandBudgetVR.Parameters.AddWithValue("@Livello2", Livello2)
            CommandBudgetVR.Parameters.AddWithValue("@Livello3", Livello3)
            CommandBudgetVR.Parameters.AddWithValue("@Colonna", Colonna)
            Dim myBudget As OleDbDataReader = CommandBudgetVR.ExecuteReader()
            If myBudget.Read() Then
                Dim CommandBudget As New OleDbCommand("UPDATE  Budget set Importo = ? Where Anno = ? And Livello1 = ? And Livello2 = ? And Livello3 = ? And Colonna = ?", cn)

                CommandBudget.Parameters.AddWithValue("@Importo", CDbl(Importo))
                CommandBudget.Parameters.AddWithValue("@Anno", Anno)
                CommandBudget.Parameters.AddWithValue("@Livello1", Livello1)
                CommandBudget.Parameters.AddWithValue("@Livello2", Livello2)
                CommandBudget.Parameters.AddWithValue("@Livello3", Livello3)
                CommandBudget.Parameters.AddWithValue("@Colonna", Colonna)
                CommandBudget.ExecuteNonQuery()
            Else

                Dim CommandBudget As New OleDbCommand("Insert INTO Budget  (Anno ,Livello1 ,Livello2 ,Livello3 ,Colonna ,Importo) VALUES (?,?,?,?,?,?) ", cn)


                CommandBudget.Parameters.AddWithValue("@Anno", Anno)
                CommandBudget.Parameters.AddWithValue("@Livello1", Livello1)
                CommandBudget.Parameters.AddWithValue("@Livello2", Livello2)
                CommandBudget.Parameters.AddWithValue("@Livello3", Livello3)
                CommandBudget.Parameters.AddWithValue("@Colonna", Colonna)
                CommandBudget.Parameters.AddWithValue("@Importo", CDbl(Importo))
                CommandBudget.ExecuteNonQuery()
            End If

            cn.Close()

        End Sub

        Public ReadOnly Property IsReusable() As Boolean Implements IHttpHandler.IsReusable
            Get
                Return False
            End Get
        End Property

    End Class

End Namespace
