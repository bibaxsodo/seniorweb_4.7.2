﻿Imports System.Data.OleDb
Imports System.Web
Imports System.Web.Services


Namespace WebHandler
    Public Class AutocompleteComune
        Implements System.Web.IHttpHandler, IRequiresSessionState

        Public Sub ProcessRequest(ByVal context As HttpContext) Implements IHttpHandler.ProcessRequest
            context.Response.ContentType = "text/plain"
            Dim RICERCA As String = context.Request.QueryString("q")
            Dim UTENTE As String = context.Request.QueryString("UTENTE")
            context.Response.Cache.SetCacheability(HttpCacheability.NoCache)


            Dim cn As OleDbConnection
            Dim SenzaTipo As Integer = 0
            Dim sb As StringBuilder = New StringBuilder

            cn = New Data.OleDb.OleDbConnection(context.Session("DC_OSPITE"))

            cn.Open()
            Dim cmd As New OleDbCommand()

            Dim Valore1 As String = ""
            Dim Valore2 As String = ""

            Dim appoggio As String

            appoggio = Replace(Replace(Replace(RICERCA, ",", ""), ".", ""), " ", "")
            If IsNumeric(appoggio) And Len(appoggio) >= 2 Then
                Dim Vettore(100) As String
                SenzaTipo = 1
                Vettore = SplitWords(RICERCA)

                If Vettore.Length >= 2 Then
                    Valore1 = Vettore(0)
                    Valore2 = Vettore(1)

                    cmd.CommandText = ("select * from AnagraficaComune where Tipologia = 'C' And " &
                                   "CodiceProvincia = ? And CodiceComune = ?")
                    cmd.Parameters.AddWithValue("@CodiceProvincia", Valore1)
                    cmd.Parameters.AddWithValue("@CodiceComune", Valore2)
                Else

                    cmd.CommandText = ("select CodiceProvincia,CodiceComune,Nome,CodificaProvincia,'1' as tipo from AnagraficaComune where Tipologia = 'C' And Nome = ? union " &
                                   "select CodiceProvincia,CodiceComune,Nome,CodificaProvincia,'2' as tipo from AnagraficaComune where Tipologia = 'C' And " &
                                   "Nome Like ? Order by Tipo")
                    cmd.Parameters.AddWithValue("@Nome", RICERCA)
                    cmd.Parameters.AddWithValue("@Nome", "%" & RICERCA & "%")
                End If
            Else
                cmd.CommandText = ("select CodiceProvincia,CodiceComune,Nome,CodificaProvincia,'1' as tipo from AnagraficaComune where Tipologia = 'C' And Nome = ? union " &
                               "select CodiceProvincia,CodiceComune,Nome,CodificaProvincia,'2' as tipo from AnagraficaComune where Tipologia = 'C' And " &
                               "Nome Like ? Order by Tipo")
                cmd.Parameters.AddWithValue("@Nome", RICERCA)
                cmd.Parameters.AddWithValue("@Nome", "%" & RICERCA & "%")
            End If

            cmd.Connection = cn

            Dim Counter As Integer = 0
            Dim myPOSTreader As OleDbDataReader = cmd.ExecuteReader()
            Do While myPOSTreader.Read
                If campodb(myPOSTreader.Item("CodiceComune")) <> "" Then
                    If SenzaTipo = 1 Then
                        sb.Append(campodb(myPOSTreader.Item("CodiceProvincia")) & " " & campodb(myPOSTreader.Item("CodiceComune")) & " " & campodb(myPOSTreader.Item("Nome")) & " " & campodb(myPOSTreader.Item("CodificaProvincia"))).Append(Environment.NewLine)
                    Else
                        If campodb(myPOSTreader.Item("Nome")).ToUpper = RICERCA.ToUpper And campodb(myPOSTreader.Item("TIPO")) = "2" Then
                        Else
                            sb.Append(campodb(myPOSTreader.Item("CodiceProvincia")) & " " & campodb(myPOSTreader.Item("CodiceComune")) & " " & campodb(myPOSTreader.Item("Nome")) & " " & campodb(myPOSTreader.Item("CodificaProvincia"))).Append(Environment.NewLine)
                            Counter = Counter + 1
                            If Counter > 10 Then
                                Exit Do
                            End If
                        End If
                    End If
                End If
            Loop
            context.Response.Write(sb.ToString)
        End Sub

        Public ReadOnly Property IsReusable() As Boolean Implements IHttpHandler.IsReusable
            Get
                Return False
            End Get
        End Property

        Private Function SplitWords(ByVal s As String) As String()
            '
            ' Call Regex.Split function from the imported namespace.
            ' Return the result array.
            '
            Return Regex.Split(s, "\W+")
        End Function

        Function campodb(ByVal oggetto As Object) As String
            If IsDBNull(oggetto) Then
                Return ""
            Else
                Return oggetto
            End If
        End Function

    End Class

End Namespace