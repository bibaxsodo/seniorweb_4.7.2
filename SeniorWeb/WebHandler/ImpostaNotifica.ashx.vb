﻿Imports System.Web
Imports System.Web.Services
Namespace WebHandler

    Public Class ImpostaNotifica
        Implements System.Web.IHttpHandler, IRequiresSessionState

        Public Sub ProcessRequest(ByVal context As HttpContext) Implements IHttpHandler.ProcessRequest
            context.Response.ContentType = "text/plain"
            Dim API_KEY As String = context.Request.QueryString("API_KEY")
            Dim GCM_ENDPOINT As String = context.Request.QueryString("GCM_ENDPOINT")
            Dim subscriptionId As String = context.Request.QueryString("subscriptionId")
            context.Response.Cache.SetCacheability(HttpCacheability.NoCache)
            context.Session("API_KEY") = API_KEY
            context.Session("GCM_ENDPOINT") = GCM_ENDPOINT
            context.Session("subscriptionId") = subscriptionId
        End Sub

        Public ReadOnly Property IsReusable() As Boolean Implements IHttpHandler.IsReusable
            Get
                Return False
            End Get
        End Property

    End Class

End Namespace
