﻿Imports System.Data.OleDb
Imports System.Web
Imports System.Web.Services

Namespace WebHandler
    Public Class GestioneConto
        Implements System.Web.IHttpHandler, System.Web.SessionState.IRequiresSessionState

        Public Sub ProcessRequest(ByVal context As HttpContext) Implements IHttpHandler.ProcessRequest
            context.Response.ContentType = "text/plain"
            Dim RICERCA As String = context.Request.QueryString("q")
            Dim UTENTE As String = context.Request.QueryString("UTENTE")

            Dim sb As StringBuilder = New StringBuilder

            Dim cn As OleDbConnection


            cn = New Data.OleDb.OleDbConnection(context.Session("DC_GENERALE"))

            cn.Open()
            Dim cmd As New OleDbCommand()

            Dim appoggio As String = ""
            Dim Corretto As Boolean = True

            Dim Valore1 As Long = 0
            Dim Valore2 As Long = 0
            Dim Valore3 As Double = 0


            appoggio = Replace(Replace(Replace(RICERCA, ",", ""), ".", ""), " ", "")
            If IsNumeric(appoggio) And Len(appoggio) >= 2 Then
                Dim Vettore(100) As String

                Vettore = SplitWords(RICERCA)

                If Vettore.Length >= 3 Then
                    Valore1 = Val(Vettore(0))
                    Valore2 = Val(Vettore(1))
                    Valore3 = Val(Vettore(2))

                    cmd.CommandText = ("select * from PianoConti where " &
                                    "Mastro = ? And Conto = ? And SottoConto = ?  And (NonInUso = '' Or NonInUso is Null) ")

                    cmd.Parameters.AddWithValue("@Mastro", Valore1)
                    cmd.Parameters.AddWithValue("@Conto", Valore2)
                    cmd.Parameters.AddWithValue("@Sottoconto", Valore3)
                End If
                If Vettore.Length = 2 Then
                    Valore1 = Val(Vettore(0))
                    Valore2 = Val(Vettore(1))
                    Valore3 = 0

                    cmd.CommandText = ("select * from PianoConti where " &
                                    "Mastro = ? And Conto = ?  And (NonInUso = '' Or NonInUso is Null) Order by Mastro,Conto,Sottoconto  ")

                    cmd.Parameters.AddWithValue("@Mastro", Valore1)
                    cmd.Parameters.AddWithValue("@Conto", Valore2)
                End If
                If Vettore.Length = 1 Then
                    Valore1 = Val(Vettore(0))
                    Valore2 = 0
                    Valore3 = 0

                    cmd.CommandText = ("select * from PianoConti where " &
                    "Mastro = ?  And (NonInUso = '' Or NonInUso is Null)  Order by Mastro,Conto,Sottoconto")

                    cmd.Parameters.AddWithValue("@Mastro", Valore1)
                End If

            Else
                cmd.CommandText = ("select * from PianoConti where " &
                                       "Descrizione Like ?   And (NonInUso = '' Or NonInUso is Null)  Order by Descrizione")

                cmd.Parameters.AddWithValue("@Descrizione", "%" & RICERCA & "%")
            End If

            cmd.Connection = cn

            Dim Counter As Integer = 0
            Dim myPOSTreader As OleDbDataReader = cmd.ExecuteReader()
            Do While myPOSTreader.Read

                sb.Append(myPOSTreader.Item("MASTRO") & " " & myPOSTreader.Item("CONTO") & " " & myPOSTreader.Item("SOTTOCONTO") & " " & myPOSTreader.Item("DESCRIZIONE")).Append(Environment.NewLine)
                Counter = Counter + 1
                If Counter > 50 Then
                    Exit Do
                End If
            Loop
            context.Response.Write(sb.ToString)
        End Sub

        Public ReadOnly Property IsReusable() As Boolean Implements IHttpHandler.IsReusable
            Get
                Return False
            End Get
        End Property


        Private Function SplitWords(ByVal s As String) As String()
            '
            ' Call Regex.Split function from the imported namespace.
            ' Return the result array.
            '
            Return Regex.Split(s, "\W+")
        End Function
    End Class

End Namespace