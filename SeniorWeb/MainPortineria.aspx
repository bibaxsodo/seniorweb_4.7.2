﻿<%@ Page Language="VB" AutoEventWireup="false" Inherits="MainPortineria" CodeFile="MainPortineria.aspx.vb" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <meta http-equiv="x-ua-compatible" content="IE=9" />
    <title>Menu Portineria</title>
    <asp:PlaceHolder runat="server">
        <%: System.Web.Optimization.Styles.Render("~/Content/AjaxControlToolkit/Styles/Bundle") %>
    </asp:PlaceHolder>
    <link rel="stylesheet" href="ospiti.css?ver=11" type="text/css" />
    <link href="js/jquery.autocomplete.css" rel="stylesheet" type="text/css" />

    <script src="js/jquery-1.5.1.min.js" type="text/javascript"></script>
    <script src="js/jquery.autocomplete.js" type="text/javascript"></script>
    <script src="js/jquery.maskedinput-1.3.min.js" type="text/javascript"></script>
    <script src="js/NoEnter.js" type="text/javascript"></script>
    <script type="text/javascript">
        $(document).ready(function () {
            if (window.innerHeight > 0) { $("#BarraLaterale").css("height", (window.innerHeight - 105) + "px"); } else { $("#BarraLaterale").css("height", (document.documentElement.offsetHeight - 105) + "px"); }
        });
    </script>
</head>
<body>
    <form id="form1" runat="server">
        <asp:ScriptManager ID="ScriptManager1" runat="server" EnableScriptGlobalization="true">
            <Scripts>
                <asp:ScriptReference Path="~/Scripts/AjaxControlToolkit/Bundle" />
            </Scripts>
        </asp:ScriptManager>
        <asp:Label ID="Lbl_BarraSenior" runat="server" Text=""></asp:Label>
        <div>
            <table width="100%" cellpadding="0" cellspacing="0">
                <tr>
                    <td style="width: 160px; background-color: #F0F0F0;"></td>
                    <td>
                        <div class="Titolo">Menu Senior</div>
                        <div class="SottoTitolo">
                            <br />
                            <br />
                        </div>
                    </td>

                    <td style="text-align: right; vertical-align: top;">
                        <span class="BenvenutoText"><font size="4">Benvenuto <%=Session("UTENTE")%></font></span>
                        <asp:ImageButton ID="Btn_Esci" runat="server" ImageUrl="~/GeneraleWeb/images/esci.jpg" />
                    </td>
                </tr>
                <tr>

                    <td style="width: 160px; background-color: #F0F0F0; text-align: center; vertical-align: top;" id="BarraLaterale">
                        <a href="Login.aspx" style="border-width: 0px;">
                            <img src="images/Home.jpg" alt="Menù" class="Effetto" /></a><br />
                    </td>
                    <td colspan="2" style="vertical-align: top;">
                        <table>
                            <tr>

                                <td style="text-align: center; width: 150px;">
                                    <a href="OspitiWeb/RicercaAnagrafica.aspx?TIPO=MOVIMENTI">
                                        <img src="images/Menu_Movimenti.png" class="Effetto" alt="Movimenti" style="border-width: 0;"></a>
                                </td>
                                <td style="text-align: center; width: 150px;">
                                    <a href="OspitiWeb/Prospetto.aspx">
                                        <img src="images/bottonestatistica.jpg" alt="Prospetto Presenze" class="Effetto" style="border-width: 0;"></a>
                                </td>
                                <td style="text-align: center; width: 150px;"></td>
                                <td style="text-align: center; width: 112px;">&nbsp;</td>
                                <td style="text-align: center; width: 112px;">&nbsp;</td>
                            </tr>
                            <tr>
                                <td style="text-align: center; vertical-align: top;"><span class="MenuText">Movimenti</span></td>
                                <td style="text-align: center; vertical-align: top;"><span class="MenuText">PRO. PRESENZE.</span></td>
                                <td style="text-align: center; vertical-align: top;"><span class="MenuText"></span></td>
                                <td style="text-align: center; vertical-align: top;">
                                    <asp:Label ID="Lbl_PostIT" runat="server" Text=""></asp:Label></td>
                                <td style="text-align: center; vertical-align: top;">&nbsp;</td>
                            </tr>

                            <tr>
                                <br />
                                <td>&nbsp;</td>
                                <td></td>
                            </tr>


                            <tr>
                                <td style="text-align: center;">&nbsp;</td>
                                <td style="text-align: center;">&nbsp;</td>
                                <td style="text-align: center;">&nbsp;</td>
                                <td style="text-align: center;">&nbsp;</td>
                                <td style="text-align: center;"></td>
                            </tr>
                            <tr>
                                <td style="text-align: center; vertical-align: top;">&nbsp;</td>
                                <td style="text-align: center; vertical-align: top;">&nbsp;</td>
                                <td style="text-align: center; vertical-align: top;">&nbsp;</td>
                                <td style="text-align: center; vertical-align: top;">&nbsp;</td>
                                <td style="text-align: center; vertical-align: top;"><span class="MenuText"></span></td>
                            </tr>

                            <tr>
                    </td>
                    <br />
                    <td>&nbsp;</td>
                    <td></td>
                </tr>

                <tr>
                    <td style="text-align: center;"></td>
                    <td style="text-align: center;"></td>
                    <td style="text-align: center;"></td>
                    <td style="text-align: center;"></td>
                    <td style="text-align: center;"></td>
                </tr>

                <tr>
                    <td style="text-align: center; vertical-align: top;"><span class="MenuText"></span></td>
                    <td style="text-align: center; vertical-align: top;"><span class="MenuText"></span></td>
                    <td style="text-align: center; vertical-align: top;"><span class="MenuText"></span></td>
                    <td style="text-align: center; vertical-align: top;"><span class="MenuText"></span></td>
                    <td style="text-align: center; vertical-align: top;"><span class="MenuText"></span></td>
                </tr>

            </table>
            </td>
    </tr>
    
    <tr>
        <td style="width: 160px; background-color: #F0F0F0;">&nbsp;<br />
            &nbsp;<br />
            &nbsp;<br />
            &nbsp;<br />
            &nbsp;<br />
            &nbsp;<br />
            &nbsp;<br />
            &nbsp;<br />
            &nbsp;<br />
        </td>
        <td></td>
        <td></td>
    </tr>
            </table> 
        </div>
    </form>
</body>
</html>
