﻿
Partial Class OspitiWeb_ReportMavCreati
    Inherits System.Web.UI.Page

    Dim Tabella As New System.Data.DataTable("tabella")

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        LblErroriImportazione.Text = "<table class=""tabella"">"



        LblErroriImportazione.Text = LblErroriImportazione.Text & "<tr class=""miotr""><th class=""miaintestazione"">Cognome Nome</th>"    
        LblErroriImportazione.Text = LblErroriImportazione.Text & "<th class=""miaintestazione"">CodiceFiscale</th>"
        LblErroriImportazione.Text = LblErroriImportazione.Text & "<th class=""miaintestazione"">Indirizzo</th>"
        LblErroriImportazione.Text = LblErroriImportazione.Text & "<th class=""miaintestazione"">Cap</th>"
        LblErroriImportazione.Text = LblErroriImportazione.Text & "<th class=""miaintestazione"">Citta</th>"
        LblErroriImportazione.Text = LblErroriImportazione.Text & "<th class=""miaintestazione"">ImportoTotale</th></tr>"

        LblErroriImportazione.Text = LblErroriImportazione.Text & Session("ERRORIIMPORT")

        LblErroriImportazione.Text = LblErroriImportazione.Text & "</table>"
    End Sub

    Protected Sub BTN_CSV_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles BTN_CSV.Click
        Tabella = Session("TabellaRid")

        Response.Clear()
        Response.ContentType = "text/csv"
        Response.AddHeader("content-disposition", "attachment;filename=MavInviati.csv")


        Dim sb As New StringBuilder()

        For i As Integer = 0 To Tabella.Columns.Count - 1
            sb.Append(Tabella.Columns(i).ColumnName + ";")
        Next
        sb.Append(Environment.NewLine)

        For j As Integer = 0 To Tabella.Rows.Count - 1
            For k As Integer = 0 To Tabella.Columns.Count - 1
                sb.Append(Tabella.Rows(j)(k).ToString() + ";")
            Next
            sb.Append(Environment.NewLine)
        Next
        Response.Write(sb.ToString())
        Response.End()
    End Sub

End Class
