﻿Imports System.Web
Imports System.Web.Services

Public Class CambiaCellaDiurno
    Implements System.Web.IHttpHandler, IRequiresSessionState

    Public Sub ProcessRequest(ByVal context As HttpContext) Implements IHttpHandler.ProcessRequest
        context.Response.ContentType = "text/plain"
        Dim CSERV As String = context.Request.QueryString("CSERV")
        Dim CODOSP As String = context.Request.QueryString("CODOSP")

        Dim MESE As String = context.Request.QueryString("MESE")
        Dim GIORNO As String = context.Request.QueryString("GIORNO")

        Dim ANNO As String = context.Request.QueryString("ANNO")
        Dim COLORE As String = context.Request.QueryString("COLORE")


        context.Response.Cache.SetCacheability(HttpCacheability.NoCache)
        Dim DbC As New Cls_Login


        If Trim(context.Session("UTENTE")) = "" Then
            Exit Sub
        End If

        DbC.Utente = context.Session("UTENTE")
        DbC.LeggiSP(context.Application("SENIOR"))

        Dim XsDiurno As New Cls_Diurno




        XsDiurno.Leggi(DbC.Ospiti, CODOSP, CSERV, ANNO, MESE)

        Dim Causale(32) As String

        If XsDiurno.Anno = 0 Then
            Dim TbCServ As New Cls_CentroServizio
            Dim SETTIMANA As String

            Dim Gg As Integer
            TbCServ.Leggi(DbC.Ospiti, CSERV)

            Dim AnaOsp As New ClsOspite

            AnaOsp.Leggi(DbC.Ospiti, CODOSP)

            Dim K As New Cls_DatiOspiteParenteCentroServizio


            K.CentroServizio = CSERV
            K.CodiceOspite = CODOSP
            K.Leggi(DbC.Ospiti)
            If K.TipoOperazione <> "" Then
                AnaOsp.SETTIMANA = K.Settimana
            End If



            SETTIMANA = AnaOsp.SETTIMANA


            For I = 1 To GiorniMese(MESE, ANNO)
                Gg = Weekday(DateSerial(ANNO, MESE, I), vbMonday)
                If Trim(Mid(SETTIMANA, Gg, 1)) <> "" Then
                    Causale(I) = Mid(SETTIMANA, Gg, 1)
                End If
            Next I


            SETTIMANA = TbCServ.SETTIMANA
            For I = 1 To GiorniMese(MESE, ANNO)
                Gg = Weekday(DateSerial(ANNO, MESE, I), vbMonday)
                If Trim(Mid(SETTIMANA, Gg, 1)) <> "" Then
                    Causale(I) = "C"
                End If
            Next I


            If MESE = 1 Then
                If TbCServ.GIORNO0101 <> "S" Then
                    Causale(1) = "C"
                End If
                If TbCServ.GIORNO0601 <> "S" Then
                    Causale(6) = "C"
                End If
            End If
            If MESE = 3 Then
                If TbCServ.GIORNO1903 <> "S" Then
                    Causale(19) = "C"
                End If
            End If
            If MESE = 4 Then
                If TbCServ.GIORNO2504 <> "S" Then
                    Causale(25) = "C"
                End If
            End If
            If MESE = 5 Then
                If TbCServ.GIORNO0105 <> "S" Then
                    Causale(1) = "C"
                End If
            End If

            If MESE = 6 Then
                If TbCServ.GIORNO0206 <> "S" Then
                    Causale(2) = "C"
                End If
                If TbCServ.GIORNO2906 <> "S" Then
                    Causale(29) = "C"
                End If
            End If
            If MESE = 8 Then
                If TbCServ.GIORNO1508 <> "S" Then
                    Causale(15) = "C"
                End If
            End If
            If MESE = 11 Then
                If TbCServ.GIORNO0111 <> "S" Then
                    Causale(1) = "C"
                End If
                If TbCServ.GIORNO0411 <> "S" Then
                    Causale(4) = "C"
                End If
            End If
            If MESE = 12 Then
                If TbCServ.GIORNO0812 <> "S" Then
                    Causale(8) = "C"
                End If
                If TbCServ.GIORNO2512 <> "S" Then
                    Causale(25) = "C"
                End If
                If TbCServ.GIORNO2612 <> "S" Then
                    Causale(26) = "C"
                End If
            End If
            If TbCServ.GIORNO1ATTIVO <> "S" Then
                If Not IsDBNull(TbCServ.GIORNO1) Then
                    If Val(Mid(TbCServ.GIORNO1, 4, 2)) = MESE Then
                        Causale(Val(Mid(TbCServ.GIORNO1, 1, 2))) = "C"
                    End If
                End If
            End If

            If TbCServ.GIORNO2ATTIVO <> "S" Then
                If Not IsDBNull(TbCServ.GIORNO2) Then
                    If Val(Mid(TbCServ.GIORNO2, 4, 2)) = MESE Then
                        Causale(Val(Mid(TbCServ.GIORNO2, 1, 2))) = "C"
                    End If
                End If
            End If

            If TbCServ.GIORNO3ATTIVO <> "S" Then
                If Not IsDBNull(TbCServ.GIORNO3) Then
                    If Val(Mid(TbCServ.GIORNO3, 4, 2)) = MESE Then
                        Causale(Val(Mid(TbCServ.GIORNO3, 1, 2))) = "C"
                    End If
                End If
            End If

            If TbCServ.GIORNO4ATTIVO <> "S" Then
                If Not IsDBNull(TbCServ.GIORNO4) Then
                    If Val(Mid(TbCServ.GIORNO4, 4, 2)) = MESE Then
                        Causale(Val(Mid(TbCServ.GIORNO4, 1, 2))) = "C"
                    End If
                End If
            End If

            If TbCServ.GIORNO5ATTIVO <> "S" Then
                If Not IsDBNull(TbCServ.GIORNO5) Then
                    If Val(Mid(TbCServ.GIORNO5, 4, 2)) = MESE Then
                        Causale(Val(Mid(TbCServ.GIORNO5, 1, 2))) = "C"
                    End If
                End If
            End If

            XsDiurno.Giorno1 = Causale(1)
            XsDiurno.Giorno2 = Causale(2)
            XsDiurno.Giorno3 = Causale(3)
            XsDiurno.Giorno4 = Causale(4)
            XsDiurno.Giorno5 = Causale(5)
            XsDiurno.Giorno6 = Causale(6)
            XsDiurno.Giorno7 = Causale(7)
            XsDiurno.Giorno8 = Causale(8)
            XsDiurno.Giorno9 = Causale(9)
            XsDiurno.Giorno10 = Causale(10)
            XsDiurno.Giorno11 = Causale(11)
            XsDiurno.Giorno12 = Causale(12)
            XsDiurno.Giorno13 = Causale(13)
            XsDiurno.Giorno14 = Causale(14)
            XsDiurno.Giorno15 = Causale(15)
            XsDiurno.Giorno16 = Causale(16)
            XsDiurno.Giorno17 = Causale(17)
            XsDiurno.Giorno18 = Causale(18)
            XsDiurno.Giorno19 = Causale(19)
            XsDiurno.Giorno20 = Causale(20)
            XsDiurno.Giorno21 = Causale(21)
            XsDiurno.Giorno22 = Causale(22)
            XsDiurno.Giorno23 = Causale(23)
            XsDiurno.Giorno24 = Causale(24)
            XsDiurno.Giorno25 = Causale(25)
            XsDiurno.Giorno26 = Causale(26)
            XsDiurno.Giorno27 = Causale(27)
            XsDiurno.Giorno28 = Causale(28)
            XsDiurno.Giorno29 = Causale(29)
            XsDiurno.Giorno30 = Causale(30)
            XsDiurno.Giorno31 = Causale(31)

        End If
        XsDiurno.CodiceOspite = CODOSP
        XsDiurno.CENTROSERVIZIO = CSERV
        XsDiurno.Anno = ANNO
        XsDiurno.Mese = MESE

        Dim tbCausale As New Cls_CausaliEntrataUscita

        If Val(COLORE) = 0 Then
            tbCausale.Diurno = 0
            tbCausale.Codice = ""
        Else
            tbCausale.Diurno = COLORE
            tbCausale.LeggiCausaleColore(DbC.Ospiti)
        End If


        If GIORNO = 1 Then XsDiurno.Giorno1 = tbCausale.Codice
        If GIORNO = 2 Then XsDiurno.Giorno2 = tbCausale.Codice
        If GIORNO = 3 Then XsDiurno.Giorno3 = tbCausale.Codice
        If GIORNO = 4 Then XsDiurno.Giorno4 = tbCausale.Codice
        If GIORNO = 5 Then XsDiurno.Giorno5 = tbCausale.Codice
        If GIORNO = 6 Then XsDiurno.Giorno6 = tbCausale.Codice
        If GIORNO = 7 Then XsDiurno.Giorno7 = tbCausale.Codice
        If GIORNO = 8 Then XsDiurno.Giorno8 = tbCausale.Codice
        If GIORNO = 9 Then XsDiurno.Giorno9 = tbCausale.Codice
        If GIORNO = 10 Then XsDiurno.Giorno10 = tbCausale.Codice
        If GIORNO = 11 Then XsDiurno.Giorno11 = tbCausale.Codice
        If GIORNO = 12 Then XsDiurno.Giorno12 = tbCausale.Codice
        If GIORNO = 13 Then XsDiurno.Giorno13 = tbCausale.Codice
        If GIORNO = 14 Then XsDiurno.Giorno14 = tbCausale.Codice
        If GIORNO = 15 Then XsDiurno.Giorno15 = tbCausale.Codice
        If GIORNO = 16 Then XsDiurno.Giorno16 = tbCausale.Codice
        If GIORNO = 17 Then XsDiurno.Giorno17 = tbCausale.Codice
        If GIORNO = 18 Then XsDiurno.Giorno18 = tbCausale.Codice
        If GIORNO = 19 Then XsDiurno.Giorno19 = tbCausale.Codice
        If GIORNO = 20 Then XsDiurno.Giorno20 = tbCausale.Codice
        If GIORNO = 21 Then XsDiurno.Giorno21 = tbCausale.Codice
        If GIORNO = 22 Then XsDiurno.Giorno22 = tbCausale.Codice
        If GIORNO = 23 Then XsDiurno.Giorno23 = tbCausale.Codice
        If GIORNO = 24 Then XsDiurno.Giorno24 = tbCausale.Codice
        If GIORNO = 25 Then XsDiurno.Giorno25 = tbCausale.Codice
        If GIORNO = 26 Then XsDiurno.Giorno26 = tbCausale.Codice
        If GIORNO = 27 Then XsDiurno.Giorno27 = tbCausale.Codice
        If GIORNO = 28 Then XsDiurno.Giorno28 = tbCausale.Codice
        If GIORNO = 29 Then XsDiurno.Giorno29 = tbCausale.Codice
        If GIORNO = 30 Then XsDiurno.Giorno30 = tbCausale.Codice
        If GIORNO = 31 Then XsDiurno.Giorno31 = tbCausale.Codice



        XsDiurno.AggiornaDB(DbC.Ospiti)


        context.Response.Write("OK")

    End Sub

    Public ReadOnly Property IsReusable() As Boolean Implements IHttpHandler.IsReusable
        Get
            Return False
        End Get
    End Property


    Public Function GiorniMese(ByVal Mese As Byte, ByVal Anno As Integer) As Byte
        If Mese = 1 Or Mese = 3 Or Mese = 5 Or Mese = 7 Or Mese = 8 Or
           Mese = 10 Or Mese = 12 Then
            GiorniMese = 31
        Else
            If Mese <> 2 Then
                GiorniMese = 30
            Else
                If Day(DateSerial(Anno, Mese, 29)) = 29 Then
                    GiorniMese = 29
                Else
                    GiorniMese = 28
                End If
            End If
        End If
    End Function
End Class