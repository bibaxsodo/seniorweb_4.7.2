﻿<%@ Page Language="VB" AutoEventWireup="false" Inherits="OspitiWeb_Menu_Export" CodeFile="Menu_Export.aspx.vb" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
    <meta http-equiv="x-ua-compatible" content="IE=9" />
    <title>Menu Export</title>
    <asp:PlaceHolder runat="server">
        <%: System.Web.Optimization.Styles.Render("~/Content/AjaxControlToolkit/Styles/Bundle") %>
    </asp:PlaceHolder>
    <link href="ospiti.css?ver=11" rel="stylesheet" type="text/css" />
    <link rel="shortcut icon" href="images/SENIOR.ico" />
    <link href="js/jquery.autocomplete.css" rel="stylesheet" type="text/css" />


    <link rel="stylesheet" href="dialogbox/redips-dialog.css" type="text/css" media="screen" />
    <script type="text/javascript" src="dialogbox/redips-dialog-min.js"></script>
    <script type="text/javascript" src="dialogbox/script.js"></script>

    <script src="js/jquery-1.5.1.min.js" type="text/javascript"></script>
    <script src="js/jquery.autocomplete.js" type="text/javascript"></script>
    <script src="js/soapclient.js" type="text/javascript"></script>
    <script src="/js/convertinumero.js" type="text/javascript"></script>

    <link rel="stylesheet" href="dialogbox/redips-dialog.css" type="text/css" media="screen" />
    <script type="text/javascript" src="dialogbox/redips-dialog-min.js"></script>

    <script src="js/jquery.maskedinput-1.3.min.js" type="text/javascript"></script>
    <script src="/js/formatnumer.js" type="text/javascript"></script>
    <script src="js/JSErrore.js" type="text/javascript"></script>
    <script src="js/NoEnter.js" type="text/javascript"></script>
    <script type="text/javascript" src="../js/menuanagraficaospiti.js?ver=7"></script>
    <link rel="stylesheet" href="jqueryui/jquery-ui.css" type="text/css" />
    <script src="jqueryui/jquery-ui.js" type="text/javascript"></script>
    <script src="jqueryui/datapicker-it.js" type="text/javascript"></script>
    <script type="text/javascript">
        $(document).ready(function () {
            if (window.innerHeight > 0) { $("#BarraLaterale").css("height", (window.innerHeight - 105) + "px"); } else { $("#BarraLaterale").css("height", (document.documentElement.offsetHeight - 105) + "px"); }
        });
    </script>
</head>
<body>
    <form id="form1" runat="server">
        <asp:ScriptManager ID="ScriptManager1" runat="server" EnableScriptGlobalization="true">
            <Scripts>
                <asp:ScriptReference Path="~/Scripts/AjaxControlToolkit/Bundle" />
            </Scripts>
        </asp:ScriptManager>
        <asp:Label ID="Lbl_BarraSenior" runat="server" Text=""></asp:Label>
        <div>
            <table style="width: 100%;" cellpadding="0" cellspacing="0">
                <tr>
                    <td style="width: 160px; background-color: #F0F0F0;"></td>
                    <td>
                        <div class="Titolo">Ospiti - Export</div>
                        <div class="SottoTitolo">
                            <br />
                            <br />
                        </div>
                    </td>

                    <td style="text-align: right; vertical-align: top;">
                        <span class="BenvenutoText"><font size="4">Benvenuto <%=Session("UTENTE")%></font></span>
                    </td>
                </tr>
                <tr>

                    <td style="width: 160px; background-color: #F0F0F0; text-align: center; vertical-align: top;" id="BarraLaterale">
                        <asp:ImageButton ID="Btn_Esci" runat="server" BackColor="Transparent" ImageUrl="../images/Menu_Indietro.png" ToolTip="Chiudi" class="Effetto" />
                    </td>
                    <td colspan="2" style="vertical-align: top;">
                        <table style="width: 60%;">


                            <tr>
                                <td style="text-align: center; width: 150px;">
                                    <asp:ImageButton runat="server" ImageUrl="../images/Menu_ModificaCserv.jpg" class="Effetto" Style="border-width: 0;" ID="ImgFatXML" />

                                </td>
                                <td style="text-align: center; width: 150px;">
                                    <asp:ImageButton ID="Img_ExportDocumenti" runat="server" ImageUrl="../images/Menu_Export_1.png" class="Effetto" Style="border-width: 0;" />
                                </td>
                    </td>
                    <td style="text-align: center; width: 150px;"></td>
                    <td style="text-align: center; width: 150px;"></td>
                    <td style="text-align: center; width: 150px;"></td>
                </tr>
                <tr>
                    <td style="text-align: center; vertical-align: top;"><span class="MenuText">
                        <asp:Label ID="lbl_fatmaxnew" runat="server" Text="FAT.XML(new)"></asp:Label></span></td>
                    <td style="text-align: center; vertical-align: top;"><span class="MenuText">EXPORT DOCUMENTI</span></td>
                    <td style="text-align: center; vertical-align: top;"><span class="MenuText"></td>
                    <td style="text-align: center; vertical-align: top;"><span class="MenuText"></td>
                    <td style="text-align: center; vertical-align: top;"><span class="MenuText"></td>
                </tr>
                <tr>
                    <td style="text-align: center;">
                        <a href="Export_RID.aspx">
                            <img alt="Export RID" src="../images/Menu_Export.png" class="Effetto" style="border-width: 0;"></a>
                    </td>
                    <td style="text-align: center;">
                        <a href="Export_MAV.aspx">
                            <img alt="Export MAV" src="../images/Menu_Export.png" class="Effetto" style="border-width: 0;"></a>
                    </td>
                    <td style="text-align: center;"></td>
                    <td style="text-align: center;"></td>
                    <td style="text-align: center;"></td>
                </tr>

                <tr>
                    <td style="text-align: center; vertical-align: top;"><span class="MenuText">EXPORT RID</span></td>
                    <td style="text-align: center; vertical-align: top;"><span class="MenuText">EXPORT MAV</span></td>
                    <td style="text-align: center; vertical-align: top;"><span class="MenuText"></span></td>
                    <td style="text-align: center; vertical-align: top;"><span class="MenuText"></td>
                    <td style="text-align: center; vertical-align: top;"><span class="MenuText"></td>
                </tr>
                <tr>

                    <td style="text-align: center;">
                        <asp:ImageButton ID="ImgImportExcelToXml" runat="server" ToolTip="Import Excel To Xml" src="../images/Menu_Export.jpg" class="Effetto" Style="border-width: 0;" />
                    </td>
                    <td style="text-align: center;">
                        <asp:ImageButton ID="ImgTesseraTs" runat="server" ToolTip="Invio Sistema TS" src="../images/Menu_Export.jpg" class="Effetto" Style="border-width: 0;" />
                    </td>

                </tr>

                <tr>

                    <td style="text-align: center; vertical-align: top;"><span class="MenuText">730/TS XML</span></td>
                    <td style="text-align: center; vertical-align: top;"><span class="MenuText">INVIO SISTEMA TS</span></td>
                </tr>

                <tr>
                    <td></td>
                    <td></td>
                    <td></td>
                </tr>



            </table>
            </td>
    </tr>    
    <tr>
        <td style="width: 160px; background-color: #F0F0F0;">&nbsp;<br />
            &nbsp;<br />
            &nbsp;<br />
            &nbsp;<br />
            &nbsp;<br />
            &nbsp;<br />
            &nbsp;<br />
            &nbsp;<br />
            &nbsp;<br />
        </td>
        <td></td>
        <td></td>
    </tr>
            </table> 
    
       
        </div>
    </form>
</body>
</html>
