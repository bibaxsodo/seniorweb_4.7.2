﻿Imports Microsoft.VisualBasic
Imports System.Data.OleDb
Imports System.Web.Hosting
Imports System.Data.SqlTypes
Imports System.Net
Imports System.IO
Imports System.Security.Cryptography.X509Certificates
Imports System.Net.Security
Imports System.Web.Script.Serialization
Imports org.jivesoftware.util


Partial Class DatiSanitari
    Inherits System.Web.UI.Page


    Sub CaricaPagina()
        Dim x As New ClsOspite
        Dim d As New ClsComune
        Dim ConnectionString As String = Session("DC_OSPITE")

        x.Leggi(ConnectionString, Session("CODICEOSPITE"))
        Dim Med As New Cls_Medici

        Med.UpDateDropBox(Session("DC_OSPITE"), DD_Medico)
        DD_Medico.SelectedValue= x.CodiceMedico
        Txt_CodiceSanitario.Text = x.CodiceSanitario
        Txt_TesseraSanitaria.Text = x.TesseraSanitaria
        Txt_Numero1.Text = x.CodiceTicket
        Txt_Numero2.Text = x.CodiceTicket2
        If Year(x.DataRilascio) = 1 Then
            Txt_Dal1.Text = ""
        Else
            Txt_Dal1.Text = Format(x.DataRilascio, "dd/MM/yyyy")
        End If
        If Year(x.DataScadenza) = 1 Then
            Txt_Al1.Text = ""
        Else
            Txt_Al1.Text = Format(x.DataScadenza, "dd/MM/yyyy")
        End If
        If Year(x.DataRilascio2) = 1 Then
            Txt_Dal2.Text = ""
        Else
            Txt_Dal2.Text = Format(x.DataRilascio2, "dd/MM/yyyy")
        End If
        If Year(x.DataScadenza2) = 1 Then
            Txt_al2.Text = ""
        Else
            Txt_al2.Text = Format(x.DataScadenza2, "dd/MM/yyyy")
        End If

        Txt_DescrizioneTicket.Text = x.NoteTicket
        Dim y As New ClsUSL

        y.UpDateDropBox(ConnectionString, DD_Regione)
        DD_Regione.SelectedValue = x.USL

        Txt_NomeMedico.Text = x.NomeMedico

        Txt_NumeroCartella.Text = x.CartellaClinica

        If DD_Medico.SelectedValue <> "" Then
            Txt_NomeMedico.Text = DD_Medico.SelectedItem.Text
            Txt_NomeMedico.Enabled = False
        End If
    End Sub
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Trim(Session("UTENTE")) = "" Then
            Response.Redirect("../Login.aspx")
            Exit Sub
        End If
        If Session("ABILITAZIONI").IndexOf("<PORTINERIA>") > 0 Then
            Response.Redirect("..\MainPortineria.aspx")
        End If

        If Page.IsPostBack = False Then

            If Val(Request.Item("CodiceOspite")) > 0 And Request.Item("CentroServizio") <> "" Then

                Session("CODICESERVIZIO") = Request.Item("CentroServizio")
                Session("CODICEOSPITE") = Val(Request.Item("CodiceOspite"))
            End If
            ViewState("CODICESERVIZIO") = Session("CODICESERVIZIO")
            ViewState("CODICEOSPITE") = Session("CODICEOSPITE")

            Call CaricaPagina()
            Call EseguiJS()


            Dim Barra As New Cls_BarraSenior

            Lbl_BarraSenior.Text = Barra.CodiceBarra(Application("SENIOR"), Session("UTENTE"), Page)

            Dim x As New ClsOspite

            x.CodiceOspite = Session("CODICEOSPITE")
            x.Leggi(Session("DC_OSPITE"), x.CodiceOspite)

            Dim cs As New Cls_CentroServizio

            cs.Leggi(Session("DC_OSPITE"), Session("CODICESERVIZIO"))

            Lbl_NomeOspite.Text = x.Nome & " -  " & x.DataNascita & " - " & cs.DESCRIZIONE & "<i style=""font-size:small;"">(" & x.CodiceOspite & ")</i>"
        End If


    End Sub

    Protected Sub Modifica()
        Dim x As New ClsOspite
        Dim d As New ClsComune
        Dim ConnectionString As String = Session("DC_OSPITE")

        x.Leggi(ConnectionString, Session("CODICEOSPITE"))

        x.CodiceMedico = DD_Medico.SelectedValue
        x.CodiceSanitario = Txt_CodiceSanitario.Text

        x.TesseraSanitaria = Txt_TesseraSanitaria.Text
        x.CodiceTicket = Txt_Numero1.Text
        x.CodiceTicket2 = Txt_Numero2.Text
        If Txt_Dal1.Text = "" Then
            x.DataRilascio = Nothing
        Else
            x.DataRilascio = Txt_Dal1.Text
        End If
        If Txt_Al1.Text = "" Then
            x.DataScadenza = Nothing
        Else
            x.DataScadenza = Txt_Al1.Text
        End If
        If Txt_Dal2.Text = "" Then
            x.DataRilascio2 = Nothing
        Else
            x.DataRilascio2 = Txt_Dal2.Text
        End If

        If Txt_al2.Text = "" Then
            x.DataScadenza2 = Nothing
        Else
            x.DataScadenza2 = Txt_al2.Text
        End If
        x.NomeMedico = Txt_NomeMedico.Text

        x.CartellaClinica = Txt_NumeroCartella.Text

        x.NoteTicket = Txt_DescrizioneTicket.Text
        x.ScriviOspite(ConnectionString)
    End Sub

    Protected Sub Btn_Modifica0_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles Btn_Modifica0.Click

        Session("CODICESERVIZIO") = ViewState("CODICESERVIZIO")
        Session("CODICEOSPITE") = ViewState("CODICEOSPITE")
        If Val(Session("CODICEOSPITE")) = 0 Then
            ClientScript.RegisterClientScriptBlock(Me.GetType(), "Errore", "VisualizzaErrore('Devi ricercare l ospite');", True)
            Exit Sub
        End If

        If Txt_Dal1.Text <> "" Then
            If Not IsDate(Txt_Dal1.Text) Then
                REM Lbl_Errore.Text = "Specificare data addebito"
                ClientScript.RegisterClientScriptBlock(Me.GetType(), "Errore", "VisualizzaErrore('Specificare data DAL');", True)
                Exit Sub
            End If
        End If
        If Txt_Dal2.Text <> "" Then
            If Not IsDate(Txt_Dal2.Text) Then
                REM Lbl_Errore.Text = "Specificare data addebito"
                ClientScript.RegisterClientScriptBlock(Me.GetType(), "Errore", "VisualizzaErrore('Specificare data DAL');", True)
                Exit Sub
            End If
        End If
        If Txt_Al1.Text <> "" Then
            If Not IsDate(Txt_Al1.Text) Then
                REM Lbl_Errore.Text = "Specificare data addebito"
                ClientScript.RegisterClientScriptBlock(Me.GetType(), "Errore", "VisualizzaErrore('Specificare data AL');", True)
                Exit Sub
            End If
        End If
        If Txt_al2.Text <> "" Then
            If Not IsDate(Txt_al2.Text) Then
                REM Lbl_Errore.Text = "Specificare data addebito"
                ClientScript.RegisterClientScriptBlock(Me.GetType(), "Errore", "VisualizzaErrore('Specificare data AL');", True)
                Exit Sub
            End If
        End If

        If Txt_NumeroCartella.Text.Trim <> "" Then
            Dim cnOspiti As OleDbConnection

            cnOspiti = New Data.OleDb.OleDbConnection(Session("DC_OSPITE"))

            cnOspiti.Open()

            Dim CmdTot As New OleDbCommand
            CmdTot.Connection = cnOspiti
            CmdTot.CommandText = "Select * From AnagraficaComune Where CartellaClinica = ? And CodiceOspite <> ? And (Select count(*) From Movimenti Where Movimenti.CodiceOspite = AnagraficaComune.codiceospite and CentroServizio = ?) > 0"
            CmdTot.Parameters.AddWithValue("@CartellaClinica", Txt_NumeroCartella.Text)
            CmdTot.Parameters.AddWithValue("@CodiceOspite", Session("CODICEOSPITE"))
            CmdTot.Parameters.AddWithValue("@CODICESERVIZIO", Session("CODICESERVIZIO"))
            Dim RdTot As OleDbDataReader = CmdTot.ExecuteReader
            If RdTot.Read Then
                RdTot.Close()
                cnOspiti.Close()
                ClientScript.RegisterClientScriptBlock(Me.GetType(), "Errore", "VisualizzaErrore('Codice Cartella già presente');", True)
                Exit Sub
            End If
            RdTot.Close()

            cnOspiti.Close()
        End If

        If Not IsNothing(Session("ABILITAZIONI")) Then
            If Session("ABILITAZIONI").IndexOf("<EPERSONAM>") >= 0 Then
                'If Not ModificaOspite() Then
                '    ClientScript.RegisterClientScriptBlock(Me.GetType(), "Errore", "VisualizzaErrore('Errore nel dialogo con Epersonam');", True)
                '    Exit Sub
                'End If
            End If
        End If
        Dim Log1 As New Cls_LogPrivacy

        Log1.LogPrivacy(Application("SENIOR"), Session("CLIENTE"), Session("UTENTE"), Session("UTENTEIMPER"), Page.GetType.Name.ToUpper, Val(Session("CODICEOSPITE")), 0, "", "", 0, "", "M", "OSPITE", "")



        Call Modifica()

        Dim MyJs As String
        MyJs = "alert('Modifica Effettuata');"
        ScriptManager.RegisterClientScriptBlock(Me, Me.GetType(), "ModScM", MyJs, True)

        Call EseguiJS()
    End Sub




 

    Private Sub EseguiJS()
        Dim MyJs As String
        MyJs = "$(document).ready(function() {"

        MyJs = MyJs & "var els = document.getElementsByTagName(""*"");"

        MyJs = MyJs & "for (var i=0;i<els.length;i++)"
        MyJs = MyJs & "if ( els[i].id ) { "
        MyJs = MyJs & " var appoggio =els[i].id; "
        MyJs = MyJs & " if ((appoggio.match('Txt_Data')!= null) || (appoggio.match('Txt_Dal')!= null) || (appoggio.match('Txt_Al')!= null)  || (appoggio.match('Txt_al')!= null))  {  "
        MyJs = MyJs & " $(els[i]).mask(""99/99/9999"");"
        MyJs = MyJs & " $(els[i]).datepicker({ changeMonth: true,changeYear: true,  yearRange: ""1930:" & Year(Now) + 5 & """},$.datepicker.regional[""it""]);"
        MyJs = MyJs & "    }"
        MyJs = MyJs & " if ((appoggio.match('Txt_Deposito')!= null) ) {  "
        MyJs = MyJs & " $(els[i]).keypress(function() { ForceNumericInput($(this).val(), true, true); } ); "
        MyJs = MyJs & " $(els[i]).blur(function() { var ap = formatNumber($(this).val(),2); $(this).val(ap); });"
        MyJs = MyJs & "    }"

        MyJs = MyJs & " if (appoggio.match('Sottoconto')!= null) {   "
        MyJs = MyJs & " $(els[i]).autocomplete('/WebHandler/GestioneConto.ashx?Utente=" & Session("UTENTE") & "', {delay:5,minChars:3});"
        MyJs = MyJs & "    }"

        MyJs = MyJs & " if ((appoggio.match('Txt_Comune')!= null) || (appoggio.match('Txt_ComRes')!= null))  {  "
        MyJs = MyJs & " $(els[i]).autocomplete('/WebHandler/AutocompleteComune.ashx?Utente=" & Session("UTENTE") & "', {delay:5,minChars:3});"
        MyJs = MyJs & "    }"

        MyJs = MyJs & "} "
        MyJs = MyJs & "});"

        'MyJs = "$(document).ready(function() { $('.myClass').keypress(function() { handleEnter($('.myClass').val(), true, true); } );     $('#" & Txt_ImportoPagato.ClientID & "').blur(function() { var ap = formatNumber ($('#" & Txt_ImportoPagato.ClientID & "').val(),2); $('#" & Txt_ImportoPagato.ClientID & "').val(ap); }); });"
        ScriptManager.RegisterStartupScript(Me, Me.GetType(), "visualizzaRitIm", MyJs, True)
    End Sub

    Protected Sub Btn_Esci_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles Btn_Esci.Click
        If Request.Item("PAGINA") = "" Then
            Response.Redirect("RicercaAnagrafica.aspx")
        Else
            Response.Redirect("RicercaAnagrafica.aspx?CHIAMATA=RITORNO&PAGINA=" & Request.Item("PAGINA"))
        End If
    End Sub



    Private Function ModificaOspite() As Boolean

        ModificaOspite = True
        Try

            LoginPersonam()

            Dim Data() As Byte

            Dim request As New NameValueCollection

            Dim JSon As String
            Dim Provincia As String = ""
            Dim Comune As String = ""


   
            

            JSon = "{""doctor_fullname"":""" & DD_Medico.SelectedItem.Text & ""","
            JSon = JSon & """esenz_ticket"":""" & Txt_Numero1.Text & ""","
            JSon = JSon & """codsan"":""" & Txt_CodiceSanitario.Text & """}"

            'birthcity_id ...


            Data = System.Text.Encoding.ASCII.GetBytes(JSon)


            Dim M As New ClsOspite

            M.CodiceOspite = Session("CODICEOSPITE")
            M.Leggi(Session("DC_OSPITE"), M.CodiceOspite)


            System.Net.ServicePointManager.ServerCertificateValidationCallback = AddressOf CertificateHandler

            Dim Cs As New Cls_CentroServizio

            Cs.CENTROSERVIZIO = Session("CODICESERVIZIO")
            Cs.Leggi(Session("DC_OSPITE"), Cs.CENTROSERVIZIO)


            Dim client As HttpWebRequest = WebRequest.Create("https://api-v0.e-personam.com/v0/business_units/" & Cs.EPersonam & "/guests/" & M.CODICEFISCALE)

            client.Method = "PUT"
            client.Headers.Add("Authorization", "Bearer " & Session("access_token"))
            client.ContentType = "Content-Type: application/json"

            client.ContentLength = Data.Length

            Dim dataStream As Stream = client.GetRequestStream()


            dataStream.Write(Data, 0, Data.Length)
            dataStream.Close()

            Dim response As HttpWebResponse = client.GetResponse()

            Dim respStream As Stream = response.GetResponseStream()
            Dim reader As StreamReader = New StreamReader(respStream)
            Dim appoggio As String
            appoggio = reader.ReadToEnd()


        Catch ex As Exception
            ModificaOspite = False
        End Try

    End Function
    Private Sub LoginPersonam()
        'Dim request As WebRequest
        '-staging
        'request = HttpWebRequest.Create("https://api-v0.e-personam.com/v0/oauth/token")
        Dim request As New NameValueCollection

        Dim BlowFish As New Blowfish("advenias2014")



        'request.Method = "POST"
        request.Add("client_id", "98217ca6670c660e22f42d58e231d4e56c84fb34e216b0f66f1f30284fd3ec9d")
        request.Add("client_secret", "5570a684f69ca74d75d16bba669c156ec092eccb4111d0974adec3edb2fa4d6f")
        request.Add("grant_type", "authorization_code")

        Dim guarda As String

        If Trim(Session("EPersonamPSWCRYPT")) = "" Then
            request.Add("username", Session("UTENTE").ToString.Replace("<1>", "").Replace("<2>", "").Replace("<3>", "").Replace("<4>", "").Replace("<5>", "").Replace("<6>", ""))

            guarda = Session("ChiaveCr")
        Else
            request.Add("username", Session("EPersonamUser"))

            guarda = Session("EPersonamPSWCRYPT")
        End If

        request.Add("code", guarda)
        System.Net.ServicePointManager.ServerCertificateValidationCallback = AddressOf CertificateHandler
        'curl -i -k https://api-v0.e-personam.com/v0/oauth/token -F grant_type=password  -F client_id=98217ca6670c660e22f42d58e231d4e56c84fb34e216b0f66f1f30284fd3ec9d -F client_secret=5570a684f69ca74d75d16bba669c156ec092eccb4111d0974adec3edb2fa4d6f -F username=paolo -F password=cristina

        'Dim responsemia As System.Net.WebResponse
        'responsemia = request.GetResponse()


        Dim client As New WebClient()

        Dim result As Object = client.UploadValues("https://api-v0.e-personam.com/v0/oauth/token", "POST", request)


        Dim stringa As String = Encoding.Default.GetString(result)


        Dim serializer As JavaScriptSerializer


        serializer = New JavaScriptSerializer()


        Dim s As Autentificazione = serializer.Deserialize(Of Autentificazione)(stringa)


        Session("access_token") = s.access_token


    End Sub
    Public Class Autentificazione
        Public access_token As String
        Public expires_in As String
        Public scope As String
        Public token_type As String
        Public refresh_token As String
    End Class
    Private Shared Function CertificateHandler(ByVal sender As Object, ByVal certificate As X509Certificate, ByVal chain As X509Chain, ByVal SSLerror As SslPolicyErrors) As Boolean
        Return True
    End Function


    Protected Sub DD_Medico_TextChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles DD_Medico.TextChanged
        If DD_Medico.SelectedValue <> "" Then
            Txt_NomeMedico.Text = DD_Medico.SelectedItem.Text
            Txt_NomeMedico.Enabled = False
        Else
            Txt_NomeMedico.Enabled = True
        End If
    End Sub
End Class
