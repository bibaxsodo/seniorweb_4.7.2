﻿Imports HgCo.WindowsLive.SkyDrive
Imports System.Data.OleDb
Imports System.Web.Hosting

Partial Class OspitiWeb_Documentazione
    Inherits System.Web.UI.Page
    Dim MyTable As New System.Data.DataTable("tabella")
    Dim MyDataSet As New System.Data.DataSet()

    Private Sub LeggiDirectory()
        Dim skyDriveClient As New SkyDriveServiceClient
        Dim i As Long

        MyTable.Clear()
        MyTable.Columns.Clear()
        MyTable.Columns.Add("NomeFile", GetType(String))
        MyTable.Columns.Add("Descrizione", GetType(String))
        MyTable.Columns.Add("Data", GetType(String))

        Try
            skyDriveClient.LogOn(ViewState("LiveIDUser"), ViewState("LiveIDPassword"))
        Catch ex As Exception
            ScriptManager.RegisterStartupScript(Me, Me.GetType(), "Errore", "VisualizzaErrore('<center>Errore collegamento a SkyDrive</center>');", True)

            Exit Sub
        End Try


        Dim a() As HgCo.WindowsLive.SkyDrive.WebFolderInfo
        Dim y() As HgCo.WindowsLive.SkyDrive.WebFolderInfo

        Dim b As HgCo.WindowsLive.SkyDrive.WebFolderInfo


        a = skyDriveClient.ListRootWebFolders
        For i = 0 To a.Length - 1
            If a(i).Name = "DocumentiOspiti" Then
                Exit For
            End If
        Next

        y = skyDriveClient.ListSubWebFolders(a(i))
        Dim k As Long
        Dim Trovato As Boolean = False
        For k = 0 To y.Length - 1
            If y(k).Name = "Ospite_" & Session("CODICEOSPITE") Then
                Trovato = True
                Exit For
            End If
        Next
        If Trovato = False Then
            b = skyDriveClient.CreateSubWebFolder("Ospite_" & Session("CODICEOSPITE"), a(i))
            Dim f() As HgCo.WindowsLive.SkyDrive.WebFileInfo

            f = skyDriveClient.ListSubWebFiles(b)
            If f.Length = 0 Then
                Dim myriga As System.Data.DataRow = MyTable.NewRow()
                MyTable.Rows.Add(myriga)
            End If
            For i = 0 To f.Length - 1
                Dim myriga As System.Data.DataRow = MyTable.NewRow()
                myriga(0) = f(i).Name
                MyTable.Rows.Add(myriga)
            Next
        Else
            Dim f() As HgCo.WindowsLive.SkyDrive.WebFileInfo

            f = skyDriveClient.ListSubWebFiles(y(k))
            If f.Length = 0 Then
                Dim myriga As System.Data.DataRow = MyTable.NewRow()
                MyTable.Rows.Add(myriga)
            End If

            For i = 0 To f.Length - 1
                Dim myriga As System.Data.DataRow = MyTable.NewRow()
                myriga(0) = f(i).Name
                Dim kcLS As New Cls_Documentazione
                kcLS.CODICEOSPITE = Session("CODICEOSPITE")
                kcLS.NomeFile = f(i).Name
                kcLS.Leggi(Session("DC_OSPITE"))
                myriga(1) = kcLS.DESCRIZIONE
                If kcLS.DATA = Nothing Then
                    myriga(2) = ""
                Else
                    myriga(2) = Format(kcLS.DATA, "dd/MM/yyyy")
                End If
                MyTable.Rows.Add(myriga)
            Next
        End If

        Session("Documentazione") = MyTable
        Grid.AutoGenerateColumns = False
        Grid.DataSource = MyTable
        Grid.DataBind()


    End Sub

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Trim(Session("UTENTE")) = "" Then
            Response.Redirect("../Login.aspx")
            Exit Sub
        End If

        If Page.IsPostBack = True Then
            Exit Sub
        End If




        Dim x As New ClsOspite

        x.CodiceOspite = Session("CODICEOSPITE")
        x.Leggi(Session("DC_OSPITE"), x.CodiceOspite)

        Dim cs As New Cls_CentroServizio

        cs.Leggi(Session("DC_OSPITE"), Session("CODICESERVIZIO"))

        Lbl_NomeOspite.Text = x.Nome & " -  " & x.DataNascita & " - " & cs.DESCRIZIONE & "<i style=""font-size:small;"">(" & x.CodiceOspite & ")</i>"


        Dim MyLog As New Cls_Login
        MyLog.Utente = Session("UTENTE")
        MyLog.LeggiSP(Application("SENIOR"))

        ViewState("LiveIDUser") = MyLog.LiveIDUser
        ViewState("LiveIDPassword") = MyLog.LiveIDPassword

        Lbl_Percorso.Text = "<font size=""4""><b>SkyDrive\DocumentiOspiti\Ospite_" & Session("CODICEOSPITE") & "\</b></font>"
        Call LeggiDirectory()
    End Sub

    Private Sub faibind()
        MyTable = Session("Documentazione")
        Grid.AutoGenerateColumns = False
        Grid.DataSource = MyTable
        Grid.DataBind()

    End Sub

    Protected Sub Grid_RowCancelingEdit(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewCancelEditEventArgs) Handles Grid.RowCancelingEdit
        Grid.EditIndex = -1
        Call faibind()
    End Sub

    Protected Sub Grid_RowCommand(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewCommandEventArgs) Handles Grid.RowCommand
        If e.CommandName = "DOWNLOAD" Then
            DownloadFile(e.CommandArgument)
        End If
    End Sub

    Protected Sub Grid_RowDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs) Handles Grid.RowDataBound
        If e.Row.RowType = DataControlRowType.DataRow Then

            Dim LblDescrizione As Label = DirectCast(e.Row.FindControl("LblDescrizione"), Label)
            If Not IsNothing(LblDescrizione) Then

            Else
                Dim TxtDescrizione As TextBox = DirectCast(e.Row.FindControl("TxtDescrizione"), TextBox)
                Dim MyDv As System.Data.DataRowView = e.Row.DataItem
                TxtDescrizione.Text = MyDv(1).ToString
            End If
            Dim LblData As Label = DirectCast(e.Row.FindControl("LblData"), Label)
            If Not IsNothing(LblData) Then

            Else
                Dim TxtData As TextBox = DirectCast(e.Row.FindControl("TxtData"), TextBox)
                Dim MyDv As System.Data.DataRowView = e.Row.DataItem
                TxtData.Text = MyDv(2).ToString

                Dim MyJs As String

                MyJs = "$(document).ready(function() { $('#" & TxtData.ClientID & "').mask(""99/99/9999""); });"
                ScriptManager.RegisterStartupScript(Me, Me.GetType(), "ControlloData", MyJs, True)
            End If
        End If
    End Sub

    Protected Sub Grid_RowEditing(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewEditEventArgs) Handles Grid.RowEditing
        Grid.EditIndex = e.NewEditIndex
        Call faibind()
    End Sub

    Protected Sub Grid_RowUpdated(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewUpdatedEventArgs) Handles Grid.RowUpdated

    End Sub

    Protected Sub Grid_RowUpdating(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewUpdateEventArgs) Handles Grid.RowUpdating
        Dim TxtDescrizione As TextBox = DirectCast(Grid.Rows(e.RowIndex).FindControl("TxtDescrizione"), TextBox)
        Dim TxtData As TextBox = DirectCast(Grid.Rows(e.RowIndex).FindControl("TxtData"), TextBox)

        If Not IsDate(TxtData.Text) Then
            ScriptManager.RegisterStartupScript(Me, Me.GetType(), "Errore", "VisualizzaErrore('<center>Data formalmente errata</center>');", True)
            Dim MyJs As String

            MyJs = "$(document).ready(function() { $('#" & TxtData.ClientID & "').mask(""99/99/9999""); });"
            ScriptManager.RegisterStartupScript(Me, Me.GetType(), "ControlloData", MyJs, True)
            Exit Sub
        End If

        MyTable = Session("Documentazione")
        Dim row = Grid.Rows(e.RowIndex)
        MyTable.Rows(row.DataItemIndex).Item(1) = TxtDescrizione.Text
        MyTable.Rows(row.DataItemIndex).Item(2) = TxtData.Text
        Session("Documentazione") = MyTable

        Grid.EditIndex = -1
        Call faibind()
    End Sub


    Private Function SoloNomeFile(ByVal appoggio As String) As String
        Dim i As Long
        SoloNomeFile = ""
        For i = Len(appoggio) To 1 Step -1
            If Mid(appoggio, i, 1) = "\" Or Mid(appoggio, i, 1) = "/" Then
                Return Mid(appoggio, i + 1, Len(appoggio) - i)
                Exit Function
            End If
        Next
    End Function


    Protected Sub DownloadFile(ByVal NomeDocumento As String)
        Dim skyDriveClient As New SkyDriveServiceClient
        Dim i As Long, Conter As Long
        Dim MyCasualDir As String
        Dim AppoCau As String

        skyDriveClient.LogOn(ViewState("LiveIDUser"), ViewState("LiveIDPassword"))        

        Dim a() As HgCo.WindowsLive.SkyDrive.WebFolderInfo
        Dim y() As HgCo.WindowsLive.SkyDrive.WebFolderInfo

        Dim b As HgCo.WindowsLive.SkyDrive.WebFolderInfo


        a = skyDriveClient.ListRootWebFolders
        For i = 0 To a.Length - 1
            If a(i).Name = "DocumentiOspiti" Then
                Exit For
            End If
        Next
        y = skyDriveClient.ListSubWebFolders(a(i))
        Dim k As Long
        Dim appo As Boolean = False
        For k = 0 To y.Length - 1
            If y(k).Name = "Ospite_" & Session("CODICEOSPITE") Then
                appo = True
                Exit For
            End If
        Next

        Dim f() As HgCo.WindowsLive.SkyDrive.WebFileInfo

        f = skyDriveClient.ListSubWebFiles(y(k))
        For i = 0 To f.Length - 1
            If f(i).Name = NomeDocumento Then
                Dim xFile As System.IO.Stream
                xFile = skyDriveClient.DownloadWebFile(f(i))
                Dim MyB As Byte
                Dim Contas As Integer
                Dim Appoggio As Integer

                Randomize()
                MyCasualDir = ""
                For Conter = 1 To 12
                    AppoCau = Int(Rnd(1) * 35)
                    If AppoCau > 25 Then
                        AppoCau = Asc("0") + (AppoCau - 26)
                    Else
                        AppoCau = AppoCau + 65
                    End If
                    MyCasualDir = MyCasualDir & Chr(AppoCau)
                Next

                Try
                    MkDir(HostingEnvironment.ApplicationPhysicalPath() & "\FileUpLoad\" & MyCasualDir)
                Catch ex As Exception

                End Try

                Dim fs As New IO.FileStream(HostingEnvironment.ApplicationPhysicalPath() & "\FileUpLoad\" & MyCasualDir & "\" & f(i).Name, IO.FileMode.OpenOrCreate, IO.FileAccess.Write, IO.FileShare.ReadWrite)
                Do
                    Contas = Contas + 1
                    Appoggio = xFile.ReadByte()
                    If Appoggio <> -1 Then
                        MyB = Appoggio
                        fs.WriteByte(MyB)
                    End If
                Loop While Appoggio <> -1
                fs.Close()


                Response.Redirect("Downloadfile.aspx?MyCasualDir=" & MyCasualDir & "&FileName=" & f(i).Name)
                Exit Sub
            End If
        Next
    End Sub



    Private Function myUpLoad() As String
        Dim MyCasualDir As String
        Dim i As Integer
        Dim AppoCau As Integer
        Randomize()


        If FileUpload1.HasFile Then
            If FilePresente(FileUpload1.FileName) = True Then                
                ClientScript.RegisterClientScriptBlock(Me.GetType(), "Errore", "VisualizzaErrore('<center>File Presente non posso procedere</center>');", True)
                myUpLoad = "ERRORRE"
                Exit Function
            End If

            Try

                MyCasualDir = ""
                For i = 1 To 12
                    AppoCau = Int(Rnd(1) * 35)
                    If AppoCau > 25 Then
                        AppoCau = Asc("0") + (AppoCau - 26)
                    Else
                        AppoCau = AppoCau + 65
                    End If
                    MyCasualDir = MyCasualDir & Chr(AppoCau)
                Next
                MkDir(HostingEnvironment.ApplicationPhysicalPath() & "\FileUpLoad\" & MyCasualDir)
                FileUpload1.SaveAs(HostingEnvironment.ApplicationPhysicalPath() & "\FileUpLoad\" & MyCasualDir & "\" & FileUpload1.FileName)

                'Label1.Text = "Upload avvenuto con successo!!"
            
                Return HostingEnvironment.ApplicationPhysicalPath() & "\FileUpLoad\" & MyCasualDir & "\" & FileUpload1.FileName
            Catch ex As Exception                
                ClientScript.RegisterClientScriptBlock(Me.GetType(), "Errore", "VisualizzaErrore('<center>Si è verificato un errore</center>');", True)
                myUpLoad = "ERRORRE"
            End Try
        Else            
            ClientScript.RegisterClientScriptBlock(Me.GetType(), "Errore", "VisualizzaErrore('<center>Non è stato selzionato nessun file</center>');", True)
            myUpLoad = "ERRORRE"
        End If
    End Function

    Private Function FilePresente(ByVal NomeFile As String) As Boolean
        Dim skyDriveClient As New SkyDriveServiceClient
        Dim i As Long


        skyDriveClient.LogOn(ViewState("LiveIDUser"), ViewState("LiveIDPassword"))

        Dim a() As HgCo.WindowsLive.SkyDrive.WebFolderInfo
        Dim y() As HgCo.WindowsLive.SkyDrive.WebFolderInfo

        Dim b As HgCo.WindowsLive.SkyDrive.WebFolderInfo


        a = skyDriveClient.ListRootWebFolders
        For i = 0 To a.Length - 1
            If a(i).Name = "DocumentiOspiti" Then
                Exit For
            End If
        Next        
        y = skyDriveClient.ListSubWebFolders(a(i))
        Dim k As Long
        Dim Trovato As Boolean = False
        For k = 0 To y.Length - 1
            If y(k).Name = "Ospite_" & Session("CODICEOSPITE") Then
                Trovato = True
                Exit For
            End If
        Next
        If Trovato = False Then
            FilePresente = False
            Exit Function
        Else
            Dim f() As HgCo.WindowsLive.SkyDrive.WebFileInfo

            f = skyDriveClient.ListSubWebFiles(y(k))
            For i = 0 To f.Length - 1
                If UCase(f(i).Name) = UCase(NomeFile) Then
                    FilePresente = True
                    Exit Function
                End If
            Next
        End If
        FilePresente = False
        Exit Function
    End Function

    Protected Sub UpLoadFile_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles UpLoadFile.Click
        'FileUpload1.SaveAs(HostingEnvironment.ApplicationPhysicalPath() & "\FileUpLoad\" & FileUpload1.FileName)
        Dim FileName As String

        FileName = myUpLoad()
        If FileName = "ERRORE" Then
            Exit Sub
        End If


        Dim skyDriveClient As New SkyDriveServiceClient
        Dim i As Long

        skyDriveClient.LogOn(ViewState("LiveIDUser"), ViewState("LiveIDPassword"))

        Dim a() As HgCo.WindowsLive.SkyDrive.WebFolderInfo
        Dim y() As HgCo.WindowsLive.SkyDrive.WebFolderInfo

        Dim b As HgCo.WindowsLive.SkyDrive.WebFolderInfo


        a = skyDriveClient.ListRootWebFolders
        For i = 0 To a.Length - 1
            If a(i).Name = "DocumentiOspiti" Then
                Exit For
            End If
        Next
        y = skyDriveClient.ListSubWebFolders(a(i))
        Dim k As Long
        Dim appo As Boolean = False
        For k = 0 To y.Length - 1
            If y(k).Name = "Ospite_" & Session("CODICEOSPITE") Then
                appo = True
                Exit For
            End If
        Next

        If appo = False Then
            b = skyDriveClient.CreateSubWebFolder("Ospite_" & Session("CODICEOSPITE"), a(i))
            skyDriveClient.UploadWebFile(FileName, b)
        Else
            Dim f() As HgCo.WindowsLive.SkyDrive.WebFileInfo

            f = skyDriveClient.ListSubWebFiles(y(k))
            For i = 0 To f.Length - 1
                If f(i).Name = SoloNomeFile(FileName) Then
                    Try
                        skyDriveClient.DeleteWebFile(f(i))
                    Catch ex As Exception

                    End Try
                End If
            Next
            skyDriveClient.UploadWebFile(FileName, y(k))
        End If

        ClientScript.RegisterClientScriptBlock(Me.GetType(), "ControlloData", "alert('UpLoad eseguito');", True)
        Call LeggiDirectory()
    End Sub

    Protected Sub ImageButton1_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles ImageButton1.Click
        Dim Ks As New Cls_Documentazione

        MyTable = Session("Documentazione")

        Ks.CODICEOSPITE = Session("CODICEOSPITE")
        Ks.Scrivi(Session("DC_OSPITE"), MyTable)
        ClientScript.RegisterClientScriptBlock(Me.GetType(), "ControlloData", "alert('Modifica Effettuata');", True)
    End Sub

    Protected Sub Btn_Esci_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles Btn_Esci.Click
        If Request.Item("PAGINA") = "" Then
            Response.Redirect("RicercaAnagrafica.aspx")
        Else
            Response.Redirect("RicercaAnagrafica.aspx?CHIAMATA=RITORNO&PAGINA=" & Request.Item("PAGINA"))
        End If

    End Sub
End Class
