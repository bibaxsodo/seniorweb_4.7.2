﻿Imports Microsoft.VisualBasic
Imports System.Data.OleDb
Imports System.Web.Hosting
Imports System.Data.SqlTypes
Imports System.Net
Imports System.IO
Imports System.Security.Cryptography.X509Certificates
Imports System.Net.Security
Imports System.Web.Script.Serialization
Imports org.jivesoftware.util
Imports Newtonsoft.Json
Imports Newtonsoft.Json.Linq

Partial Class Residenza
    Inherits System.Web.UI.Page

    Protected Function Modifica() As Boolean
        Dim ConnectionString As String = Session("DC_OSPITE")
        Modifica = False

        Dim X As New ClsOspite

        X.Leggi(ConnectionString, Session("CODICEOSPITE"))



        X.RESIDENZADATA1 = IIf(IsDate(Txt_Data1.Text), Txt_Data1.Text, Nothing)
        X.RESIDENZAINDIRIZZO1 = Txt_Indirizzo1.Text

        X.RESIDENZATELEFONO1 = Txt_Telefono1.Text

        X.RESIDENZATELEFONO2 = Txt_Telefono2.Text
        X.RESIDENZATELEFONO3 = Txt_Telefono3.Text

        X.TELEFONO1 = Txt_Telefono4.Text

        X.TELEFONO4 = Txt_Telefono4_Des.Text


        If Txt_Comune1.Text <> "" Then
            Dim Vettore(100) As String

            Vettore = SplitWords(Txt_Comune1.Text)
            If Vettore.Length > 1 Then
                X.RESIDENZAPROVINCIA1 = Vettore(0)
                X.RESIDENZACOMUNE1 = Vettore(1)
            End If

            If Val(X.RESIDENZAPROVINCIA1) = 0 Then
                X.RESIDENZAPROVINCIA1 = ""
            End If
            If Val(X.RESIDENZACOMUNE1) = 0 Then
                X.RESIDENZACOMUNE1 = ""
            End If
        Else
            X.RESIDENZAPROVINCIA1 = ""
            X.RESIDENZACOMUNE1 = ""
        End If


        X.RESIDENZACAP1 = Txt_Cap1.Text

        X.RESIDENZADATA2 = IIf(IsDate(Txt_Data2.Text), Txt_Data2.Text, Nothing)
        X.RESIDENZAINDIRIZZO2 = Txt_Indirizzo2.Text

        If Txt_Comune2.Text <> "" Then
            Dim Vettore(100) As String

            Vettore = SplitWords(Txt_Comune2.Text)
            If Vettore.Length > 1 Then
                X.RESIDENZAPROVINCIA2 = Vettore(0)
                X.RESIDENZACOMUNE2 = Vettore(1)
            End If
        Else
            X.RESIDENZAPROVINCIA2 = ""
            X.RESIDENZACOMUNE2 = ""
        End If

        X.RESIDENZACAP2 = Txt_Cap2.Text

        X.RESIDENZADATA3 = IIf(IsDate(Txt_Data3.Text), Txt_Data3.Text, Nothing)
        X.RESIDENZAINDIRIZZO3 = Txt_Indirizzo3.Text

        If Txt_Comune3.Text <> "" Then
            Dim Vettore(100) As String

            Vettore = SplitWords(Txt_Comune3.Text)
            If Vettore.Length > 1 Then
                X.RESIDENZAPROVINCIA3 = Vettore(0)
                X.RESIDENZACOMUNE3 = Vettore(1)
            End If
        Else
            X.RESIDENZAPROVINCIA3 = ""
            X.RESIDENZACOMUNE3 = ""
        End If

        X.RESIDENZACAP3 = Txt_Cap3.Text



        X.RecapitoNome = Txt_Nome.Text
        X.RecapitoIndirizzo = Txt_Indirizzo.Text

        If Txt_Comune.Text <> "" Then
            Dim Vettore(100) As String

            Vettore = SplitWords(Txt_Comune.Text)
            If Vettore.Length > 1 Then
                X.RecapitoProvincia = Vettore(0)
                X.RecapitoComune = Vettore(1)
            Else
                X.RecapitoProvincia = ""
                X.RecapitoComune = ""
            End If
        Else
            X.RecapitoProvincia = ""
            X.RecapitoComune = ""
        End If


        X.RESIDENZATELEFONO4 = Txt_Telefono.Text

        X.TELEFONO4 = Txt_Telefono4_Des.Text

        X.RecapitoNome = Txt_Nome.Text
        X.RecapitoIndirizzo = Txt_Indirizzo.Text

        If Txt_Comune.Text <> "" Then
            Dim Vettore(100) As String

            Vettore = SplitWords(Txt_Comune.Text)
            If Vettore.Length > 1 Then
                X.RecapitoProvincia = Vettore(0)
                X.RecapitoComune = Vettore(1)
            End If
        Else
            X.RecapitoProvincia = ""
            X.RecapitoComune = ""
        End If

        X.RESIDENZACAP4 = Txt_Cap4.Text

        X.RESIDENZATELEFONO4 = Txt_Telefono.Text
        X.TELEFONO4 = Txt_Telefono4_Des.Text
        Try
            X.ScriviOspite(ConnectionString)
        Catch ex As Exception
            ClientScript.RegisterClientScriptBlock(Me.GetType(), "Errore", "VisualizzaErrore('Errore in fase di registrazione');", True)
            Exit Function
        End Try

        Modifica = True

    End Function


    Private Sub EseguiJS()
        Dim MyJs As String
        MyJs = "$(document).ready(function() {"

        MyJs = MyJs & "var els = document.getElementsByTagName(""*"");"

        MyJs = MyJs & "for (var i=0;i<els.length;i++)"
        MyJs = MyJs & "if ( els[i].id ) { "
        MyJs = MyJs & " var appoggio =els[i].id; "
        MyJs = MyJs & " if (appoggio.match('Txt_Data')!= null) {  "
        MyJs = MyJs & " $(els[i]).mask(""99/99/9999"");"
        MyJs = MyJs & " $(els[i]).datepicker({ changeMonth: true,changeYear: true,  yearRange: ""1990:" & Year(Now) + 5 & """},$.datepicker.regional[""it""]);"

        MyJs = MyJs & "    }"
        MyJs = MyJs & " if ((appoggio.match('TxtImporto')!= null) || (appoggio.match('TxtPercentuale')!= null) ) {  "
        MyJs = MyJs & " $(els[i]).keypress(function() { ForceNumericInput($(this).val(), true, true); } ); "
        MyJs = MyJs & " $(els[i]).blur(function() { var ap = formatNumber($(this).val(),2); $(this).val(ap); });"
        MyJs = MyJs & "    }"

        MyJs = MyJs & " if (appoggio.match('Sottoconto')!= null) {   "
        MyJs = MyJs & " $(els[i]).autocomplete('/WebHandler/GestioneConto.ashx?Utente=" & Session("UTENTE") & "', {delay:5,minChars:3});"
        MyJs = MyJs & "    }"

        MyJs = MyJs & " if ((appoggio.match('Txt_Comune')!= null) || (appoggio.match('Txt_ComRes')!= null))  {  "
        MyJs = MyJs & " $(els[i]).autocomplete('/WebHandler/AutocompleteComune.ashx?Utente=" & Session("UTENTE") & "', {delay:5,minChars:3});"
        MyJs = MyJs & "    }"

        MyJs = MyJs & "} "
        MyJs = MyJs & "});"

        'MyJs = "$(document).ready(function() { $('.myClass').keypress(function() { handleEnter($('.myClass').val(), true, true); } );     $('#" & Txt_ImportoPagato.ClientID & "').blur(function() { var ap = formatNumber ($('#" & Txt_ImportoPagato.ClientID & "').val(),2); $('#" & Txt_ImportoPagato.ClientID & "').val(ap); }); });"
        ScriptManager.RegisterStartupScript(Me, Me.GetType(), "visualizzaRitIm", MyJs, True)
    End Sub

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Trim(Session("UTENTE")) = "" Then
            Response.Redirect("../Login.aspx")
            Exit Sub
        End If
        If Session("ABILITAZIONI").IndexOf("<PORTINERIA>") > 0 Then
            Response.Redirect("..\MainPortineria.aspx")
        End If
        Btn_RecuperaEperesonam.Visible = False
        If Trim(Session("NomeEPersonam")) = "" Then
            Bnt_TT.Visible = False
        End If

        If Page.IsPostBack = True Then
            Exit Sub
        End If





        If Val(Request.Item("CodiceOspite")) > 0 And Request.Item("CentroServizio") <> "" Then

            Session("CODICESERVIZIO") = Request.Item("CentroServizio")
            Session("CODICEOSPITE") = Val(Request.Item("CodiceOspite"))
        End If
        ViewState("CODICESERVIZIO") = Session("CODICESERVIZIO")
        ViewState("CODICEOSPITE") = Session("CODICEOSPITE")


        Dim K1 As New Cls_SqlString

        Dim Barra As New Cls_BarraSenior

        If Not IsNothing(Session("RicercaAnagraficaSQLString")) Then
            K1 = Session("RicercaAnagraficaSQLString")
        End If

        Lbl_BarraSenior.Text = Barra.CodiceBarra(Application("SENIOR"), Session("UTENTE"), Page, K1)

        Dim x1 As New ClsOspite

        x1.CodiceOspite = Session("CODICEOSPITE")
        x1.Leggi(Session("DC_OSPITE"), x1.CodiceOspite)

        Dim cs As New Cls_CentroServizio

        cs.Leggi(Session("DC_OSPITE"), Session("CODICESERVIZIO"))

        Lbl_NomeOspite.Text = x1.Nome & " -  " & x1.DataNascita & " - " & cs.DESCRIZIONE & "<i style=""font-size:small;"">(" & x1.CodiceOspite & ")</i>"

        Dim ConnectionString As String = Session("DC_OSPITE")

        Dim X As New ClsOspite

        X.Leggi(ConnectionString, Session("CODICEOSPITE"))


        Txt_Telefono1.Text = X.RESIDENZATELEFONO1
        Txt_Telefono2.Text = X.RESIDENZATELEFONO2
        Txt_Telefono3.Text = X.RESIDENZATELEFONO3
        Txt_Telefono4.Text = X.TELEFONO1
        Txt_Telefono4_Des.Text = X.TELEFONO4

        Txt_Data1.Text = X.RESIDENZADATA1
        Txt_Indirizzo1.Text = X.RESIDENZAINDIRIZZO1

        Txt_Comune1.Text = X.RESIDENZAPROVINCIA1 & " " & X.RESIDENZACOMUNE1
        Txt_Cap1.Text = X.RESIDENZACAP1


        Txt_Data2.Text = X.RESIDENZADATA2
        Txt_Indirizzo2.Text = X.RESIDENZAINDIRIZZO2

        Txt_Comune2.Text = X.RESIDENZAPROVINCIA2 & " " & X.RESIDENZACOMUNE2
        Txt_Cap2.Text = X.RESIDENZACAP2

        Txt_Data3.Text = X.RESIDENZADATA3
        Txt_Indirizzo3.Text = X.RESIDENZAINDIRIZZO3        
        Txt_Comune3.Text = X.RESIDENZAPROVINCIA3 & " " & X.RESIDENZACOMUNE3
        Txt_Cap3.Text = X.RESIDENZACAP3

        Dim z As New ClsComune
        z.Comune = X.RESIDENZACOMUNE1
        z.Provincia = X.RESIDENZAPROVINCIA1
        z.DecodficaComune(ConnectionString)
        Txt_Comune1.Text = Trim(Txt_Comune1.Text & " " & z.Descrizione)

        z.Comune = X.RESIDENZACOMUNE2
        z.Provincia = X.RESIDENZAPROVINCIA2
        z.DecodficaComune(ConnectionString)
        Txt_Comune2.Text = Trim(Txt_Comune2.Text & " " & z.Descrizione)

        z.Comune = X.RESIDENZACOMUNE3
        z.Provincia = X.RESIDENZAPROVINCIA3
        z.DecodficaComune(ConnectionString)
        Txt_Comune3.Text = Trim(Txt_Comune3.Text & " " & z.Descrizione)

        Txt_Nome.Text = X.RecapitoNome
        Txt_Indirizzo.Text = X.RecapitoIndirizzo
        Txt_Telefono.Text = X.RESIDENZATELEFONO4

        Txt_Cap4.Text = X.RESIDENZACAP4

        Dim dc As New ClsComune

        dc.Comune = X.RecapitoComune
        dc.Provincia = X.RecapitoProvincia
        dc.DecodficaComune(ConnectionString)
        Txt_Comune.Text = Trim(dc.Provincia & " " & dc.Comune & " " & dc.Descrizione)

        Dim Appoggio As New Cls_Parenti

        Appoggio.CodiceOspite = Session("CODICEOSPITE")
        If Appoggio.AltroDestinatario(Session("DC_OSPITE")) = True Then
            Txt_Comune.Enabled = False
            Txt_Nome.Enabled = False
            Txt_Indirizzo.Enabled = False
            Txt_Cap4.Enabled = False
            Txt_Nome.Enabled = False
            Txt_Telefono.Enabled = False
            Txt_Telefono4_Des.Enabled = False
        End If

        If Not IsNothing(Session("ABILITAZIONI")) Then
            If Session("ABILITAZIONI").IndexOf("<EPERSONAM>") >= 0 And X.IdEpersonam > 0 Then
                Txt_Indirizzo1.Enabled = False
                Txt_Telefono1.Enabled = False
                Txt_Indirizzo1.Enabled = False
                Txt_Comune1.Enabled = False
                Txt_Cap1.Enabled = False

                REM Txt_Nome.Enabled = False
                REM Txt_Indirizzo.Enabled = False                
                REM Txt_Comune.Enabled = False

                REM Txt_Telefono.Enabled = False

                Txt_Telefono2.Enabled = False
                Txt_Telefono3.Enabled = False
                Txt_Telefono4.Enabled = False

                REM Txt_Telefono4_Des.Enabled = False
            End If
            If Not IsNothing(Session("ABILITAZIONI")) Then
                If Session("ABILITAZIONI").IndexOf("<EPERSONAM-AUT>") >= 0 Then
                    Btn_RecuperaEperesonam.Visible = False
                    Bnt_TT.Visible = False
                End If
            End If
        End If

        Call EseguiJS()
    End Sub

    
    Private Function SplitWords(ByVal s As String) As String()
        '
        ' Call Regex.Split function from the imported namespace.
        ' Return the result array.
        '
        Return Regex.Split(s, "\W+")
    End Function

    Protected Sub ImageButton1_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles ImageButton1.Click


        If Not IsNothing(Session("ABILITAZIONI")) Then
            If Session("ABILITAZIONI").IndexOf("<EPERSONAM>") >= 0 Then
                'If Not ModificaOspite() Then
                '    ClientScript.RegisterClientScriptBlock(Me.GetType(), "Errore", "VisualizzaErrore('Errore nel dialogo con Epersonam');", True)
                '    Exit Sub
                'End If
            End If
        End If

        Dim Log1 As New Cls_LogPrivacy

        Log1.LogPrivacy(Application("SENIOR"), Session("CLIENTE"), Session("UTENTE"), Session("UTENTEIMPER"), Page.GetType.Name.ToUpper, Val(Session("CODICEOSPITE")), 0, "", "", 0, "", "M", "OSPITE", "")


        If Modifica() Then

            Dim MyJs As String
            MyJs = "alert('Modifica Effettuata');"
            ScriptManager.RegisterClientScriptBlock(Me, Me.GetType(), "ModScM", MyJs, True)

            Txt_Indirizzo1.BackColor = Drawing.Color.White
            Txt_Comune1.BackColor = Drawing.Color.White
            Txt_Cap1.BackColor = Drawing.Color.White
        End If
        Call EseguiJS()
    End Sub

    Protected Sub Btn_Esci_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles Btn_Esci.Click
        If Request.Item("PAGINA") = "" Then
            Response.Redirect("RicercaAnagrafica.aspx")
        Else
            Response.Redirect("RicercaAnagrafica.aspx?CHIAMATA=RITORNO&PAGINA=" & Request.Item("PAGINA"))
        End If
    End Sub

    Private Function BusinessUnit(ByVal Token As String, ByVal context As HttpContext) As Long
        System.Net.ServicePointManager.ServerCertificateValidationCallback = AddressOf CertificateHandler



        Dim client As HttpWebRequest = WebRequest.Create("https://api-v0.e-personam.com/v0/whoami")

        client.Method = "GET"
        client.Headers.Add("Authorization", "Bearer " & Token)
        client.ContentType = "Content-Type: application/json"

        Dim reader As StreamReader
        Dim response As HttpWebResponse = Nothing

        response = DirectCast(client.GetResponse(), HttpWebResponse)

        reader = New StreamReader(response.GetResponseStream())


        Dim rawresp As String
        rawresp = reader.ReadToEnd()

        BusinessUnit = 0

        Dim jResults As JObject = JObject.Parse(rawresp)

        For Each jTok2 As JToken In jResults.Item("business_units").Children
            If Val(jTok2.Item("description").ToString.IndexOf(context.Session("NomeEPersonam"))) >= 0 Then

                BusinessUnit = Val(jTok2.Item("id").ToString)
                Exit For
            End If
        Next



    End Function

    Private Sub LeggiDestinatario()
        Dim M As New ClsOspite
        Dim Token As String

        LoginPersonam()
        Token = Session("access_token")

        M.CodiceOspite = Session("CODICEOSPITE")
        M.Leggi(Session("DC_OSPITE"), M.CodiceOspite)


        System.Net.ServicePointManager.ServerCertificateValidationCallback = AddressOf CertificateHandler

        Dim Cs As New Cls_CentroServizio

        Cs.CENTROSERVIZIO = Session("CODICESERVIZIO")
        Cs.Leggi(Session("DC_OSPITE"), Cs.CENTROSERVIZIO)


        Dim client As HttpWebRequest = WebRequest.Create("https://api-v0.e-personam.com/v0/guests/" & M.CODICEFISCALE & "/relatives")
        client.Method = "GET"
        client.Headers.Add("Authorization", "Bearer " & Token)
        client.ContentType = "Content-Type: application/json"

        Dim reader As StreamReader
        Dim response As HttpWebResponse = Nothing

        response = DirectCast(client.GetResponse(), HttpWebResponse)

        reader = New StreamReader(response.GetResponseStream())
        Dim rawresp As String


        rawresp = reader.ReadToEnd()


        Dim jResults As JArray = JArray.Parse(rawresp)

        Dim Appoggio As String

        For Each jTok2 As JToken In jResults

            Dim MyBill As String = ""


            Try
                MyBill = jTok2.Item("signed_to_receive_bill")
            Catch ex As Exception

            End Try


            If MyBill = "true" Or MyBill = "True" Then


                Txt_Nome.Text = jTok2.Item("surname").ToString & " " & jTok2.Item("firstname").ToString

                Try
                    Appoggio = jTok2.Item("rescity").ToString()

                    Dim ClsCom As New ClsComune

                    ClsCom.Provincia = Mid(Appoggio & Space(6), 1, 3)
                    ClsCom.Comune = Mid(Appoggio & Space(6), 4, 3)
                    ClsCom.Leggi(Session("DC_OSPITE"))

                    Txt_Comune.Text = ClsCom.Provincia & " " & ClsCom.Comune & " " & ClsCom.Descrizione
                Catch ex As Exception

                End Try

                Try
                    Txt_Indirizzo.Text = jTok2.Item("resaddress").ToString()
                Catch ex As Exception

                End Try

                Try
                    Txt_Cap4.Text = jTok2.Item("rescap").ToString()
                Catch ex As Exception

                End Try

            End If
        Next


    End Sub

    Private Sub LeggiOspiteEpersonam()
        Dim M As New ClsOspite
        Dim Token As String

        LoginPersonam()
        Token = Session("access_token")

        M.CodiceOspite = Session("CODICEOSPITE")
        M.Leggi(Session("DC_OSPITE"), M.CodiceOspite)


        System.Net.ServicePointManager.ServerCertificateValidationCallback = AddressOf CertificateHandler

        Dim Cs As New Cls_CentroServizio

        Cs.CENTROSERVIZIO = Session("CODICESERVIZIO")
        Cs.Leggi(Session("DC_OSPITE"), Cs.CENTROSERVIZIO)


        Dim client As HttpWebRequest = WebRequest.Create("https://api-v0.e-personam.com/v0/business_units/" & BusinessUnit(Token, Context) & "/guests/" & M.CODICEFISCALE)

        client.Method = "GET"
        client.Headers.Add("Authorization", "Bearer " & Token)
        client.ContentType = "Content-Type: application/json"

        Dim reader As StreamReader
        Dim response As HttpWebResponse = Nothing

        response = DirectCast(client.GetResponse(), HttpWebResponse)

        reader = New StreamReader(response.GetResponseStream())
        Dim rawresp As String


        rawresp = reader.ReadToEnd()


        Dim jTok2 As JObject = JObject.Parse(rawresp)

        Dim appoggio As String


        Try
            If Txt_Indirizzo1.Text <> jTok2.Item("resaddress").ToString() Then
                Txt_Indirizzo1.BackColor = Drawing.Color.Azure
            End If
            Txt_Indirizzo1.Text = jTok2.Item("resaddress").ToString()
        Catch ex As Exception

        End Try

        Try
            If Txt_Cap1.Text <> jTok2.Item("rescap").ToString() Then
                Txt_Cap1.BackColor = Drawing.Color.Azure
            End If
            Txt_Cap1.Text = jTok2.Item("rescap").ToString()
        Catch ex As Exception

        End Try


        Try
            appoggio = jTok2.Item("rescity").ToString()
            Dim ClsCom As New ClsComune
            M.RESIDENZAPROVINCIA1 = Mid(appoggio & Space(6), 1, 3)
            M.RESIDENZACOMUNE1 = Mid(appoggio & Space(6), 4, 3)

            ClsCom.Provincia = M.RESIDENZAPROVINCIA1
            ClsCom.Comune = M.RESIDENZACOMUNE1
            ClsCom.Leggi(Session("DC_OSPITE"))
            If Txt_Comune1.Text <> ClsCom.Provincia & " " & ClsCom.Comune & " " & ClsCom.Descrizione Then
                Txt_Comune1.BackColor = Drawing.Color.Azure
            End If

            Txt_Comune1.Text = ClsCom.Provincia & " " & ClsCom.Comune & " " & ClsCom.Descrizione

        Catch ex As Exception


        End Try


        Try
            If Txt_Telefono3.Text <> jTok2.Item("email").ToString() Then
                Txt_Telefono3.BackColor = Drawing.Color.Azure
            End If
            Txt_Telefono3.Text = jTok2.Item("email").ToString()
        Catch ex As Exception

        End Try

        Try
            If Txt_Telefono1.Text <> jTok2.Item("phone").ToString() Then
                Txt_Telefono1.BackColor = Drawing.Color.Azure
            End If
            Txt_Telefono1.Text = jTok2.Item("phone").ToString()
        Catch ex As Exception

        End Try


        Try
            If Txt_Telefono2.Text <> jTok2.Item("fax").ToString() Then
                Txt_Telefono2.BackColor = Drawing.Color.Azure
            End If
            Txt_Telefono2.Text = jTok2.Item("fax").ToString()
        Catch ex As Exception

        End Try
    End Sub
    Private Function ModificaOspite() As Boolean

        ModificaOspite = True
        Try

            LoginPersonam()

            Dim Data() As Byte

            Dim request As New NameValueCollection

            Dim JSon As String
            Dim Provincia As String = ""
            Dim Comune As String = ""


            If Txt_Comune1.Text <> "" Then
                Dim Vettore(100) As String

                Vettore = SplitWords(Txt_Comune1.Text)
                If Vettore.Length > 1 Then
                    Provincia = Vettore(0)
                    Comune = Vettore(1)
                End If
            End If

            If Provincia = "" Then
                JSon = "{""resaddress"":""" & Txt_Indirizzo1.Text & ""","
                JSon = JSon & """rescap"":""" & Txt_Cap1.Text & """}"
            Else

                JSon = "{""resaddress"":""" & Txt_Indirizzo1.Text & ""","
                JSon = JSon & """rescap"":""" & Txt_Cap1.Text & ""","
                JSon = JSon & """rescity"":""" & Provincia & Comune & """}"
            End If
            'birthcity_id ...


            Data = System.Text.Encoding.ASCII.GetBytes(JSon)


            Dim M As New ClsOspite

            M.CodiceOspite = Session("CODICEOSPITE")
            M.Leggi(Session("DC_OSPITE"), M.CodiceOspite)


            System.Net.ServicePointManager.ServerCertificateValidationCallback = AddressOf CertificateHandler

            Dim Cs As New Cls_CentroServizio

            Cs.CENTROSERVIZIO = Session("CODICESERVIZIO")
            Cs.Leggi(Session("DC_OSPITE"), Cs.CENTROSERVIZIO)


            Dim client As HttpWebRequest = WebRequest.Create("https://api-v0.e-personam.com/v0/business_units/" & Cs.EPersonam & "/guests/" & M.CODICEFISCALE)

            client.Method = "PUT"
            client.Headers.Add("Authorization", "Bearer " & Session("access_token"))
            client.ContentType = "Content-Type: application/json"

            client.ContentLength = Data.Length

            Dim dataStream As Stream = client.GetRequestStream()


            dataStream.Write(Data, 0, Data.Length)
            dataStream.Close()

            Dim response As HttpWebResponse = client.GetResponse()

            Dim respStream As Stream = response.GetResponseStream()
            Dim reader As StreamReader = New StreamReader(respStream)
            Dim appoggio As String
            appoggio = reader.ReadToEnd()


        Catch ex As Exception
            ModificaOspite = False
        End Try

    End Function
    Private Sub LoginPersonam()
        'Dim request As WebRequest
        '-staging
        'request = HttpWebRequest.Create("https://api-v0.e-personam.com/v0/oauth/token")
        Dim request As New NameValueCollection

        Dim BlowFish As New Blowfish("advenias2014")



        'request.Method = "POST"
        request.Add("client_id", "98217ca6670c660e22f42d58e231d4e56c84fb34e216b0f66f1f30284fd3ec9d")
        request.Add("client_secret", "5570a684f69ca74d75d16bba669c156ec092eccb4111d0974adec3edb2fa4d6f")
        request.Add("grant_type", "authorization_code")
        request.Add("username", Session("UTENTE"))

        Dim guarda As String

        guarda = Session("ChiaveCr")

        request.Add("code", guarda)
        System.Net.ServicePointManager.ServerCertificateValidationCallback = AddressOf CertificateHandler
        'curl -i -k https://api-v0.e-personam.com/v0/oauth/token -F grant_type=password  -F client_id=98217ca6670c660e22f42d58e231d4e56c84fb34e216b0f66f1f30284fd3ec9d -F client_secret=5570a684f69ca74d75d16bba669c156ec092eccb4111d0974adec3edb2fa4d6f -F username=paolo -F password=cristina

        'Dim responsemia As System.Net.WebResponse
        'responsemia = request.GetResponse()

        Dim client As New WebClient()

        Dim result As Object = client.UploadValues("https://api-v0.e-personam.com/v0/oauth/token", "POST", request)


        Dim stringa As String = Encoding.Default.GetString(result)


        Dim serializer As JavaScriptSerializer


        serializer = New JavaScriptSerializer()


        Dim s As Autentificazione = serializer.Deserialize(Of Autentificazione)(stringa)


        Session("access_token") = s.access_token


    End Sub
    Public Class Autentificazione
        Public access_token As String
        Public expires_in As String
        Public scope As String
        Public token_type As String
        Public refresh_token As String
    End Class
    Private Shared Function CertificateHandler(ByVal sender As Object, ByVal certificate As X509Certificate, ByVal chain As X509Chain, ByVal SSLerror As SslPolicyErrors) As Boolean
        Return True
    End Function

    Protected Sub Bnt_TT_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles Bnt_TT.Click
        LeggiOspiteEpersonam()
    End Sub

    Protected Sub Btn_RecuperaEperesonam_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles Btn_RecuperaEperesonam.Click
        LeggiDestinatario()
    End Sub

    Protected Sub Bnt_TT_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Bnt_TT.Load

    End Sub

    Protected Sub ImgStoricizza_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles ImgStoricizza.Click

        Txt_Indirizzo3.Text = Txt_Indirizzo2.Text
        Txt_Comune3.Text = Txt_Comune2.Text
        Txt_Data3.Text = Txt_Data2.Text
        Txt_Cap3.Text = Txt_Cap2.Text

        Txt_Indirizzo2.Text = Txt_Indirizzo1.Text
        Txt_Comune2.Text = Txt_Comune1.Text
        Txt_Data2.Text = Txt_Data1.Text
        Txt_Cap2.Text = Txt_Cap1.Text


        If Txt_Indirizzo1.Enabled = True Then
            Txt_Indirizzo1.Text = ""
            Txt_Comune1.Text = ""
            Txt_Data1.Text = ""
            Txt_Cap1.Text = ""
        End If



    End Sub
End Class
