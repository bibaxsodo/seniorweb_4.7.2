﻿Imports System
Imports System.Web
Imports Microsoft.VisualBasic
Imports System.Data.OleDb
Imports System.Web.Hosting
Imports System.Data.SqlTypes
Imports System.Net
Imports System.Collections.Generic
Imports System.IO
Imports System.Security.Cryptography.X509Certificates
Imports System.Net.Security
Imports System.Web.Script.Serialization
Imports org.jivesoftware.util
Imports Newtonsoft.Json
Imports Newtonsoft.Json.Linq
Imports System.Data.Odbc


Partial Class OspitiWeb_AssegnaListinoOspiti
    Inherits System.Web.UI.Page
    Dim Tabella As New System.Data.DataTable("tabella")

    Function campodb(ByVal oggetto As Object) As String
        If IsDBNull(oggetto) Then
            Return ""
        Else
            Return oggetto
        End If
    End Function

    Function campodbd(ByVal oggetto As Object) As Date
        If IsDBNull(oggetto) Then
            Return Nothing
        Else
            Return oggetto
        End If
    End Function

    Private Function SplitWordsCSV(ByVal s As String) As String()
        '
        ' Call Regex.Split function from the imported namespace.
        ' Return the result array.
        '
        Return Regex.Split(s, ";")
    End Function

    Private Function SplitWords(ByVal s As String) As String()
        '
        ' Call Regex.Split function from the imported namespace.
        ' Return the result array.
        '
        Return Regex.Split(s, "\W+")
    End Function

    Private Sub EstraiOspiti()


        Dim ConnectionString As String = Session("DC_OSPITE")
        Dim cn As OleDbConnection
        Dim MySql As String
        Dim Condizione As String
        Dim ContaOspiti As Integer = 0
        Dim CSV As String = ""
        Dim RecapitoProvincia As String = ""
        Dim RecapitoComune As String = ""



        cn = New Data.OleDb.OleDbConnection(ConnectionString)

        cn.Open()


        Tabella.Clear()
        Tabella.Columns.Clear()

        Tabella.Columns.Add("CodiceOspite", GetType(String))
        Tabella.Columns.Add("Cognome", GetType(String))
        Tabella.Columns.Add("Nome", GetType(String))
        Tabella.Columns.Add("DataNascita", GetType(String))
        Tabella.Columns.Add("Listini", GetType(String))
        Tabella.Columns.Add("DataValidita", GetType(String))




        Dim cmdCS As New OleDbCommand()

        If Cmb_CServ.SelectedValue = "" Then
            cmdCS.CommandText = "Select * From TABELLACENTROSERVIZIO Where VILLA = ?"
            cmdCS.Parameters.AddWithValue("@VILLA", DD_Struttura.SelectedValue)
        Else
            cmdCS.CommandText = "Select * From TABELLACENTROSERVIZIO Where CENTROSERVIZIO = ?"
            cmdCS.Parameters.AddWithValue("@CENTROSERVIZIO", Cmb_CServ.SelectedValue)
        End If
        cmdCS.Connection = cn
        Dim ReadCS As OleDbDataReader = cmdCS.ExecuteReader()

        Do While ReadCS.Read



            Dim cmd As New OleDbCommand()

            If Txt_Comune.Text = "" Then
                MySql = "Select * From AnagraficaComune Where (NonInUso = '' or NonInUso  is null ) And CodiceParente = 0 And CodiceOspite > 0 And ( (Select count(*) From Movimenti Where CodiceOspite = AnagraficaComune.CodiceOspite And Data >= ? And Data <= ? And CENTROSERVIZIO = '" & campodb(ReadCS.Item("CENTROSERVIZIO")) & "') > 0 OR " & _
                                                                " (" & _
                                                                " ((Select Top 1 TipoMov From Movimenti Where CodiceOspite = AnagraficaComune.CodiceOspite And Data < ? And CENTROSERVIZIO = '" & campodb(ReadCS.Item("CENTROSERVIZIO")) & "' Order By Data DESC,Progressivo Desc) <> '13') " & _
                                                                " And " & _
                                                                " ((Select  count(*) From Movimenti Where CodiceOspite = AnagraficaComune.CodiceOspite And Data < ? And CENTROSERVIZIO = '" & campodb(ReadCS.Item("CENTROSERVIZIO")) & "' And TipoMov = '05') <>  0) " & _
                                                                " )" & _
                                                                " ) order by Nome"
            Else
                Dim Vettore(100) As String

                Vettore = SplitWords(Txt_Comune.Text)
                If Vettore.Length > 1 Then
                    RecapitoProvincia = Vettore(0)
                    RecapitoComune = Vettore(1)
                End If

                MySql = "Select * From AnagraficaComune Where RESIDENZAPROVINCIA1 = '" & RecapitoProvincia & "' And RESIDENZACOMUNE1 = '" & RecapitoComune & "' And (NonInUso = '' or NonInUso  is null ) And CodiceParente = 0 And CodiceOspite > 0 And ( (Select count(*) From Movimenti Where CodiceOspite = AnagraficaComune.CodiceOspite And Data >= ? And Data <= ? And CENTROSERVIZIO = '" & campodb(ReadCS.Item("CENTROSERVIZIO")) & "') > 0 OR " & _
                                                                " (" & _
                                                                " ((Select Top 1 TipoMov From Movimenti Where CodiceOspite = AnagraficaComune.CodiceOspite And Data < ? And CENTROSERVIZIO = '" & campodb(ReadCS.Item("CENTROSERVIZIO")) & "' Order By Data DESC,Progressivo Desc) <> '13') " & _
                                                                " And " & _
                                                                " ((Select  count(*) From Movimenti Where CodiceOspite = AnagraficaComune.CodiceOspite And Data < ? And CENTROSERVIZIO = '" & campodb(ReadCS.Item("CENTROSERVIZIO")) & "' And TipoMov = '05') <>  0) " & _
                                                                " )" & _
                                                                " ) order by Nome"
            End If

            cmd.CommandText = MySql
            Dim Txt_DataDalText As Date = Txt_DataRegistrazione.Text
            Dim Txt_DataAlText As Date = Txt_DataRegistrazione.Text

            cmd.Parameters.AddWithValue("@DataDal", Txt_DataDalText)
            cmd.Parameters.AddWithValue("@DataAl", Txt_DataAlText)
            cmd.Parameters.AddWithValue("@DataDal", Txt_DataDalText)
            cmd.Parameters.AddWithValue("@DataDal", Txt_DataDalText)
            cmd.Connection = cn


            Dim myPOSTreader As OleDbDataReader = cmd.ExecuteReader()
            Do While myPOSTreader.Read
                'CSV = CSV & campodb(myPOSTreader.Item("CODICEFISCALE")) & ";" & campodb(myPOSTreader.Item("CognomeOspite")) & ";" & campodb(myPOSTreader.Item("NomeOspite")) & ";"""";0;"""";" & vbNewLine
                Dim myriga As System.Data.DataRow = Tabella.NewRow()

                myriga.Item(0) = campodb(myPOSTreader.Item("CodiceOspite"))
                myriga.Item(1) = campodb(myPOSTreader.Item("CognomeOspite"))
                myriga.Item(2) = campodb(myPOSTreader.Item("NomeOspite"))
                myriga.Item(3) = Format(campodbd(myPOSTreader.Item("DataNascita")), "dd/MM/yyyy")



                Dim MLs As New Cls_Listino

                MLs.CODICEOSPITE = campodb(myPOSTreader.Item("CodiceOspite"))
                MLs.CENTROSERVIZIO = campodb(ReadCS.Item("CENTROSERVIZIO"))
                MLs.LeggiAData(Session("DC_OSPITE"), Txt_DataAlText)


                Dim TabellaListi As New Cls_Tabella_Listino

                If MLs.CodiceListino <> "" Then
                    TabellaListi.Codice = MLs.CodiceListino
                    TabellaListi.Descrizione = ""
                    TabellaListi.LeggiCausale(ConnectionString)

                    myriga(4) = TabellaListi.Descrizione
                Else
                    myriga(4) = ""
                End If


                If MLs.CodiceListinoPrivatoSanitario <> "" Then
                    TabellaListi.Codice = MLs.CodiceListinoPrivatoSanitario
                    TabellaListi.Descrizione = ""
                    TabellaListi.LeggiCausale(ConnectionString)
                    myriga(4) = myriga(4) & "," & TabellaListi.Descrizione
                End If
                If MLs.CodiceListinoSanitario <> "" Then
                    TabellaListi.Codice = MLs.CodiceListinoSanitario
                    TabellaListi.Descrizione = ""
                    TabellaListi.LeggiCausale(ConnectionString)
                    myriga(4) = myriga(4) & "," & TabellaListi.Descrizione
                End If
                If MLs.CodiceListinoSociale <> "" Then
                    TabellaListi.Codice = MLs.CodiceListinoSociale
                    TabellaListi.Descrizione = ""
                    TabellaListi.LeggiCausale(ConnectionString)
                    myriga(4) = myriga(4) & "," & TabellaListi.Descrizione
                End If

                If MLs.CodiceListinoJolly <> "" Then
                    TabellaListi.Codice = MLs.CodiceListinoJolly
                    TabellaListi.Descrizione = ""
                    TabellaListi.LeggiCausale(ConnectionString)
                    myriga(4) = myriga(4) & "," & TabellaListi.Descrizione
                End If

                Dim M As New Cls_Movimenti

                If Cmb_CServ.SelectedValue <> "" Then
                    M.CENTROSERVIZIO = Cmb_CServ.SelectedValue
                    M.CodiceOspite = campodb(myPOSTreader.Item("CodiceOspite"))
                    M.UltimaDataAccoglimento(Session("DC_OSPITE"))

                Else                    
                    M.CodiceOspite = campodb(myPOSTreader.Item("CodiceOspite"))
                    M.UltimaDataAccoglimentoSenzaCserv(Session("DC_OSPITE"))
                End If

                If Chk_UsaDataAccoglimento.Checked = False Then

                    If Format(M.Data, "yyyyMMdd") > Format(Txt_DataAlText, "yyyyMMdd") Then
                        myriga.Item(5) = Format(M.Data, "dd/MM/yyyy")
                    Else
                        myriga.Item(5) = Format(Txt_DataAlText, "dd/MM/yyyy")
                    End If
                Else
                    If myriga(4) = "" Then
                        myriga.Item(5) = Format(M.Data, "dd/MM/yyyy")
                    Else
                        If Format(M.Data, "yyyyMMdd") > Format(Txt_DataAlText, "yyyyMMdd") Then
                            myriga.Item(5) = Format(M.Data, "dd/MM/yyyy")
                        Else
                            myriga.Item(5) = Format(Txt_DataAlText, "dd/MM/yyyy")
                        End If
                    End If
                End If
                If Chk_SenzaListino.Checked = False Then
                    Tabella.Rows.Add(myriga)
                Else
                    If myriga(4) = "" Then
                        Tabella.Rows.Add(myriga)
                    End If
                End If


            Loop
            myPOSTreader.Close()
        Loop

        cn.Close()


        ViewState("App_AddebitiMultiplo") = Tabella

        GridView1.AutoGenerateColumns = False

        GridView1.DataSource = Tabella
        GridView1.DataBind()
        GridView1.Visible = True
    End Sub


    Private Sub EseguiJS()
        Dim MyJs As String
        MyJs = "$(document).ready(function() {"

        MyJs = MyJs & "var els = document.getElementsByTagName(""*"");"

        MyJs = MyJs & "for (var i=0;i<els.length;i++)"
        MyJs = MyJs & "if ( els[i].id ) { "
        MyJs = MyJs & " var appoggio =els[i].id; "


        MyJs = MyJs & " if ((appoggio.match('Txt_Importo')!= null)) {  "
        MyJs = MyJs & " $(els[i]).keypress(function() { ForceNumericInput($(this).val(), true, true); } ); "
        MyJs = MyJs & " $(els[i]).blur(function() { var ap = formatNumber($(this).val(),2); $(this).val(ap); });"
        MyJs = MyJs & "    }"

        MyJs = MyJs & " if (appoggio.match('Txt_DataRegistrazione')!= null) {  "
        MyJs = MyJs & " $(els[i]).mask(""99/99/9999"");"
        MyJs = MyJs & " $(els[i]).datepicker({ changeMonth: true,changeYear: true,  yearRange: ""1990:" & Year(Now) + 5 & """},$.datepicker.regional[""it""]);"
        MyJs = MyJs & "    }"

        MyJs = MyJs & " if ((appoggio.match('Txt_Comune')!= null) || (appoggio.match('Txt_Comune')!= null))  {  "
        MyJs = MyJs & " $(els[i]).autocomplete('/WebHandler/AutocompleteComune.ashx?Utente=" & Session("UTENTE") & "', {delay:5,minChars:3});"
        MyJs = MyJs & "    }"

        MyJs = MyJs & "} "
        MyJs = MyJs & "});"

        'MyJs = "$(document).ready(function() { $('.myClass').keypress(function() { handleEnter($('.myClass').val(), true, true); } );     $('#" & Txt_ImportoPagato.ClientID & "').blur(function() { var ap = formatNumber ($('#" & Txt_ImportoPagato.ClientID & "').val(),2); $('#" & Txt_ImportoPagato.ClientID & "').val(ap); }); });"
        ScriptManager.RegisterStartupScript(Me, Me.GetType(), "visualizzaRitIm", MyJs, True)
    End Sub


    Protected Sub DD_Struttura_TextChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles DD_Struttura.TextChanged
        AggiornaCServ()
    End Sub
    Protected Sub Cmb_CServ_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles Cmb_CServ.SelectedIndexChanged

        Dim cS As New Cls_CentroServizio

        cS.CENTROSERVIZIO = Cmb_CServ.SelectedValue
        cS.Leggi(Session("DC_OSPITE"), cS.CENTROSERVIZIO)



        Dim k As New Cls_Tabella_Listino


        k.UpDateDropBoxConCodice(Session("DC_OSPITE"), DD_Lisitino, Cmb_CServ.SelectedValue, "")
        k.UpDateDropBoxConCodice(Session("DC_OSPITE"), DD_PrivatoSanitario, Cmb_CServ.SelectedValue, "P")
        k.UpDateDropBoxConCodice(Session("DC_OSPITE"), DD_LisitinoSanitario, Cmb_CServ.SelectedValue, "R")
        k.UpDateDropBoxConCodice(Session("DC_OSPITE"), DD_LisitinoSociale, Cmb_CServ.SelectedValue, "C")
        k.UpDateDropBoxConCodice(Session("DC_OSPITE"), DD_LisitinoJolly, Cmb_CServ.SelectedValue, "J")

        EseguiJS()

    End Sub
    Private Sub AggiornaCServ()
        Dim kCsrv As New Cls_CentroServizio

        If DD_Struttura.SelectedValue = "" Then
            kCsrv.UpDateDropBox(Session("DC_OSPITE"), Cmb_CServ)
        Else
            kCsrv.UpDateDropBoxStruttura(Session("DC_OSPITE"), Cmb_CServ, DD_Struttura.SelectedValue)
        End If

        EseguiJS()
    End Sub

    Protected Sub Btn_Esci_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles Btn_Esci.Click
        Response.Redirect("Menu_Strumenti.aspx")
    End Sub

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If IsNothing(Session("DC_OSPITE")) Then
            Response.Redirect("..\Login.aspx")
            Exit Sub
        End If
        EseguiJS()


        If Page.IsPostBack = False Then


            Dim K1 As New Cls_SqlString

            Dim Barra As New Cls_BarraSenior

            If Not IsNothing(Session("RicercaAnagraficaSQLString")) Then
                Try
                    K1 = Session("RicercaAnagraficaSQLString")
                Catch ex As Exception
                    K1 = Nothing
                End Try
            End If

            Lbl_BarraSenior.Text = Barra.CodiceBarra(Application("SENIOR"), Session("UTENTE"), Page, K1)

            Dim Param As New Cls_Parametri

            Param.LeggiParametri(Context.Session("DC_OSPITE"))



            Dim DataFatt As String

            If Param.MeseFatturazione > 9 Then
                DataFatt = Param.AnnoFatturazione & "-" & Param.MeseFatturazione & "-01"
            Else
                DataFatt = Param.AnnoFatturazione & "-0" & Param.MeseFatturazione & "-01"
            End If


            Dim k As New Cls_Tabella_Listino


            k.UpDateDropBoxConCodice(Session("DC_OSPITE"), DD_Lisitino, Cmb_CServ.SelectedValue, "")
            k.UpDateDropBoxConCodice(Session("DC_OSPITE"), DD_PrivatoSanitario, Cmb_CServ.SelectedValue, "P")
            k.UpDateDropBoxConCodice(Session("DC_OSPITE"), DD_LisitinoSanitario, Cmb_CServ.SelectedValue, "R")
            k.UpDateDropBoxConCodice(Session("DC_OSPITE"), DD_LisitinoSociale, Cmb_CServ.SelectedValue, "C")
            k.UpDateDropBoxConCodice(Session("DC_OSPITE"), DD_LisitinoJolly, Cmb_CServ.SelectedValue, "J")


            Dim kVilla As New Cls_TabelleDescrittiveOspitiAccessori

            kVilla.UpDateDropBox(Session("DC_OSPITIACCESSORI"), "VIL", DD_Struttura)
            If DD_Struttura.Items.Count = 1 Then
                Call AggiornaCServ()
            End If
            EseguiJS()



            If Param.AlberghieroAssistenziale = 1 Then
                LblListinoPrivatoSinatario.Text = "Assistenziale Privato"
            End If

            If DD_PrivatoSanitario.Items.Count <= 1 Then
                DD_PrivatoSanitario.Visible = False
                LblListinoPrivatoSinatario.Visible = False

            End If

            If DD_LisitinoSanitario.Items.Count <= 1 Then
                DD_LisitinoSanitario.Visible = False
                lblListinoSan.Visible = False
            End If
            If DD_LisitinoSociale.Items.Count <= 1 Then
                DD_LisitinoSociale.Visible = False
                lblListinoSociale.Visible = False
            End If
            If DD_LisitinoJolly.Items.Count <= 1 Then
                DD_LisitinoJolly.Visible = False

                lblListinoJolly.Visible = False
            End If
        End If
    End Sub

    Protected Sub Btn_Movimenti_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles Btn_Movimenti.Click

        If Not IsDate(Txt_DataRegistrazione.Text) Then
            ClientScript.RegisterClientScriptBlock(Me.GetType(), "Errore", "VisualizzaErrore('Data Registrazione Formalmente Errata');", True)
            Exit Sub
        End If


        If Cmb_CServ.SelectedValue = "" And DD_Struttura.SelectedValue = "" Then
            ClientScript.RegisterClientScriptBlock(Me.GetType(), "Errore", "VisualizzaErrore('Data Selezionare un centro servizio o una struttura  ');", True)
            Exit Sub
        End If


        If DD_Lisitino.SelectedValue = "" Then
            ClientScript.RegisterClientScriptBlock(Me.GetType(), "Errore", "VisualizzaErrore('Data Selezionare un listino');", True)
            Exit Sub
        End If


        EstraiOspiti()
    End Sub

    Protected Sub Btn_Seleziona_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles Btn_Seleziona.Click
        Dim Param As New Cls_Parametri

        Param.LeggiParametri(Session("DC_OSPITE"))

        For Riga = 0 To GridView1.Rows.Count - 1

            Dim CheckBox As CheckBox = DirectCast(GridView1.Rows(Riga).FindControl("ChkImporta"), CheckBox)

            If CheckBox.Enabled = True Then
                CheckBox.Checked = True
                CheckBox.Checked = True
            End If
        Next
    End Sub

    Protected Sub Btn_Modifica_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles Btn_Modifica.Click
        Dim Param As New Cls_Parametri

        Param.LeggiParametri(Session("DC_OSPITE"))


        Tabella = ViewState("App_AddebitiMultiplo")


        If Not IsDate(Txt_DataRegistrazione.Text) Then
            ScriptManager.RegisterClientScriptBlock(Me, Me.GetType(), "Errore", "VisualizzaErrore('Data formalmente errata');", True)
            Exit Sub
        End If

        If DD_Lisitino.SelectedValue = "" And DD_LisitinoSanitario.Visible = False Then
            ScriptManager.RegisterClientScriptBlock(Me, Me.GetType(), "Errore", "VisualizzaErrore('Devi indicare il listino');", True)
            Exit Sub
        End If

        If DD_Lisitino.SelectedValue = "" And DD_LisitinoSanitario.Visible = True Then
            ScriptManager.RegisterClientScriptBlock(Me, Me.GetType(), "Errore", "VisualizzaErrore('Devi indicare almeno il listino privati');", True)
            Exit Sub
        End If

        Dim Listino As New Cls_Tabella_Listino


        Listino.Codice = DD_Lisitino.SelectedValue
        Listino.LeggiCausale(Session("DC_OSPITE"))

        If DD_LisitinoSanitario.SelectedValue <> "" Or DD_LisitinoSociale.SelectedValue <> "" Or DD_LisitinoJolly.SelectedValue <> "" Then
            If Listino.TIPOLISTINO = "" Then
                ScriptManager.RegisterClientScriptBlock(Me, Me.GetType(), "Errore", "VisualizzaErrore('Non puoi un listino composto con listini sanitario,sociale o jolly');", True)
                Exit Sub
            End If
        End If

        If Listino.MODALITA = "P" Then
            Dim ParImp As New Cls_Parenti

            If ParImp.ParenteIntestatario(Session("DC_OSPITE"), Val(Session("CODICEOSPITE"))) = 0 Then
                ScriptManager.RegisterClientScriptBlock(Me, Me.GetType(), "Errore", "VisualizzaErrore('Non indicato parente intestatario');", True)
                Exit Sub
            End If
        End If

        If DD_LisitinoSanitario.SelectedValue <> "" Then

            Dim ListinoSan As New Cls_Tabella_Listino


            ListinoSan.Codice = DD_LisitinoSanitario.SelectedValue
            ListinoSan.LeggiCausale(Session("DC_OSPITE"))

            Dim ImpReg As New Cls_ImportoRegione


            If ListinoSan.USL = "" Then
                ImpReg.UltimaDataSenzaRegione(Session("DC_OSPITE"), ListinoSan.TIPORETTAUSL)
            Else
                ImpReg.UltimaData(Session("DC_OSPITE"), ListinoSan.USL, ListinoSan.TIPORETTAUSL)
            End If

            If ImpReg.Tipo <> Listino.TIPORETTATOTALE Then
                ScriptManager.RegisterClientScriptBlock(Me, Me.GetType(), "Errore", "VisualizzaErrore('Non puoi un listino composto con listini sanitario,sociale con tipo importo differenti');", True)
                Exit Sub
            End If
        End If




        For Riga = 0 To GridView1.Rows.Count - 1

            Dim CheckBox As CheckBox = DirectCast(GridView1.Rows(Riga).FindControl("ChkImporta"), CheckBox)

            If CheckBox.Checked = True Then
                Dim CodiceOspite As Integer

                CodiceOspite = Tabella.Rows(Riga).Item(0)

                If CodiceOspite > 0 Then

                    Dim Cserv As String = Cmb_CServ.SelectedValue

                    If Cserv = "" Then
                        Dim ulmov As New Cls_Movimenti

                        ulmov.CodiceOspite = CodiceOspite
                        ulmov.UltimaDataAccoglimentoSenzaCserv(Session("DC_OSPITE"))

                        Cserv = ulmov.CENTROSERVIZIO
                    End If


                    Dim VerRettaTotale As New Cls_rettatotale


                    VerRettaTotale.CENTROSERVIZIO = Cserv
                    VerRettaTotale.CODICEOSPITE = CodiceOspite
                    VerRettaTotale.UltimaData(Session("DC_OSPITE"), VerRettaTotale.CODICEOSPITE, VerRettaTotale.CENTROSERVIZIO)

                    Dim DataAppoggio As Date = Tabella.Rows(Riga).Item(5)

                    If Format(VerRettaTotale.Data, "yyyyMMdd") >= Format(DataAppoggio, "yyyyMMdd") Then
                        ScriptManager.RegisterClientScriptBlock(Me, Me.GetType(), "Errore", "VisualizzaErrore('Data in retta totale successiva alla data inserita');", True)
                        Exit Sub
                    End If

                    Dim ListinoOspite As New Cls_Listino

                    ListinoOspite.InserisciRette(Session("DC_OSPITE"), Cserv, CodiceOspite, DD_Lisitino.SelectedValue, DataAppoggio, DD_PrivatoSanitario.SelectedValue, DD_LisitinoSanitario.SelectedValue, DD_LisitinoSociale.SelectedValue, DD_LisitinoJolly.SelectedValue)

                End If

            End If
        Next
    End Sub

    Protected Sub Btn_SelezionaCF_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles Btn_SelezionaCF.Click
        Dim Appoggio As String
        Dim Vettore(1000) As String
        Dim ApInt As Integer


        Tabella = ViewState("App_AddebitiMultiplo")

        Appoggio = ListaCF.Text
        Try
            Vettore = SplitWords(Appoggio)
        Catch ex As Exception

        End Try

        For ApInt = 0 To Vettore.Length - 1
            If Not IsNothing(Vettore(ApInt)) Then
                Dim Riga As Integer

                For Riga = 0 To Tabella.Rows.Count - 1

                    Dim Posp As New ClsOspite

                    Posp.CodiceOspite = Tabella.Rows(Riga).Item(0)
                    Posp.Leggi(Session("DC_OSPITE"), Posp.CodiceOspite)

                    If Posp.CODICEFISCALE = Vettore(ApInt) Then
                        Dim CheckBox As CheckBox = DirectCast(GridView1.Rows(Riga).FindControl("ChkImporta"), CheckBox)

                        If CheckBox.Enabled = True Then
                            CheckBox.Checked = True
                            CheckBox.Checked = True
                        End If
                    End If
                Next
            End If
        Next
        ListaCF.Visible = False
        Btn_SelezionaCF.Visible = False
        Btn_SelezionaDaCF.Visible = True
    End Sub

    Protected Sub Btn_SelezionaDaCF_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles Btn_SelezionaDaCF.Click
        ListaCF.Visible = True
        Btn_SelezionaCF.Visible = True
        Btn_SelezionaDaCF.Visible = False
    End Sub


    Private Sub ImportFile(ByVal Appoggio As String)
        Dim cn As OdbcConnection
        Dim nomefileexcel As String

        Dim NomeSocieta As String


        If FileUpload1.FileName = "" Then
            ClientScript.RegisterClientScriptBlock(Me.GetType(), "Errore", "VisualizzaErrore('File non indicato');", True)
            Exit Sub
        End If

        Dim k As New Cls_Login

        k.Utente = Session("UTENTE")
        k.LeggiSP(Application("SENIOR"))

        NomeSocieta = k.NomeEPersonam




        FileUpload1.SaveAs(HostingEnvironment.ApplicationPhysicalPath() & "\Public\CsvImport" & Appoggio & ".csv")

        If Dir(HostingEnvironment.ApplicationPhysicalPath() & "\Public\CsvImport" & Appoggio & ".csv") = "" Then
            ClientScript.RegisterClientScriptBlock(Me.GetType(), "Errore", "VisualizzaErrore('File csv non trovata');", True)
            Exit Sub
        End If


        cn = New Data.Odbc.OdbcConnection("Driver={Microsoft Text Driver (*.txt; *.csv)};Dbq=" & HostingEnvironment.ApplicationPhysicalPath() & "Public\;HDR=Yes;")



        cn.Open()

        Dim cmd As New OdbcCommand()

        cmd.CommandText = ("select * from [" & "CsvImport" & Appoggio & ".csv" & "]")



        cmd.Connection = cn
        Dim myPOSTreader As OdbcDataReader = cmd.ExecuteReader()
        Do While myPOSTreader.Read
            Dim Vettore(100) As String
            Dim CampoInput As String
            Dim CODICEFISCALE As String
            Dim IMPORTO As Double
            Dim IMPORTO1 As Double


            CampoInput = myPOSTreader.Item(0)
            Try
                If campodb(myPOSTreader.Item(1)) <> "" Then
                    CampoInput = CampoInput & "," & myPOSTreader.Item(1)
                End If
                If campodb(myPOSTreader.Item(2)) <> "" Then
                    CampoInput = CampoInput & "," & myPOSTreader.Item(2)
                End If
            Catch ex As Exception

            End Try

            Vettore = SplitWordsCSV(CampoInput)
            Try
                CODICEFISCALE = Vettore(0)
                IMPORTO = Vettore(1)
                IMPORTO1 = Vettore(2)

            Catch ex As Exception
                cn.Close()
                ClientScript.RegisterClientScriptBlock(Me.GetType(), "Errore", "VisualizzaErrore('Errore nella struttura del file');", True)
                Kill(HostingEnvironment.ApplicationPhysicalPath() & "Public\CsvImport" & Appoggio & ".csv")
                Exit Sub
            End Try

            Dim Cos As New ClsOspite

            Cos.CodiceOspite = 0
            Cos.LeggiPerCodiceFiscale(Session("DC_OSPITE"), CODICEFISCALE)

            If Cos.CodiceOspite > 0 Then
                Dim CservMov As New Cls_Movimenti

                CservMov.CodiceOspite = Cos.CodiceOspite
                CservMov.UltimaData(Session("DC_OSPITE"), CservMov.CodiceOspite)

                Dim Paran As New Cls_Parenti
                Dim ParenteIntestatario As Integer


                ParenteIntestatario = Paran.ParenteIntestatario(Session("DC_OSPITE"), CservMov.CodiceOspite)
                If ParenteIntestatario = 0 Then
                    Dim ImpOsp As New Cls_ImportoOspite

                    ImpOsp.Data = CservMov.Data
                    ImpOsp.CENTROSERVIZIO = CservMov.CENTROSERVIZIO
                    ImpOsp.CODICEOSPITE = Cos.CodiceOspite
                    ImpOsp.UltimaData(Session("DC_OSPITE"), ImpOsp.CODICEOSPITE, ImpOsp.CENTROSERVIZIO)

                    ImpOsp.Importo = IMPORTO
                    ImpOsp.Importo1 = IMPORTO1
                    ImpOsp.AggiornaDB(Session("DC_OSPITE"))
                Else
                    Dim UpParente As New Cls_Parenti

                    UpParente.CodiceOspite = Cos.CodiceOspite
                    UpParente.CodiceParente = ParenteIntestatario
                    UpParente.Leggi(Session("DC_OSPITE"), UpParente.CodiceOspite, UpParente.CodiceParente)

                    UpParente.TIPOOPERAZIONE = Cos.TIPOOPERAZIONE
                    UpParente.CODICEIVA = Cos.CODICEIVA
                    UpParente.Compensazione = Cos.Compensazione
                    UpParente.MODALITAPAGAMENTO = Cos.MODALITAPAGAMENTO
                    UpParente.PERIODO = Cos.PERIODO
                    UpParente.FattAnticipata = Cos.FattAnticipata
                    UpParente.ScriviParente(Session("DC_OSPITE"))

                    Dim datiosppare As New Cls_DatiOspiteParenteCentroServizio

                    datiosppare.CentroServizio = CservMov.CENTROSERVIZIO
                    datiosppare.CodiceOspite = Cos.CodiceOspite
                    datiosppare.CodiceParente = ParenteIntestatario

                    datiosppare.TipoOperazione = Cos.TIPOOPERAZIONE
                    datiosppare.AliquotaIva = Cos.CODICEIVA
                    datiosppare.Compensazione = Cos.Compensazione
                    datiosppare.ModalitaPagamento = Cos.MODALITAPAGAMENTO
                    datiosppare.Anticipata = Cos.FattAnticipata
                    datiosppare.Scrivi(Session("DC_OSPITE"))

                    Dim ImpPar As New Cls_ImportoParente


                    ImpPar.CENTROSERVIZIO = CservMov.CENTROSERVIZIO
                    ImpPar.CODICEOSPITE = Cos.CodiceOspite
                    ImpPar.CODICEPARENTE = ParenteIntestatario
                    ImpPar.IMPORTO = IMPORTO
                    ImpPar.IMPORTO1 = IMPORTO1
                    ImpPar.TIPOIMPORTO = "G"
                    ImpPar.DATA = CservMov.Data
                    ImpPar.AggiornaDB(Session("DC_OSPITE"))
                End If
            End If
        Loop
        myPOSTreader.Close()

        cn.Close()

    End Sub

    Protected Sub Btn_ImportOspiti_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles Btn_ImportOspiti.Click

        Dim Appoggio As String = Format(Now, "yyyyMMddHHmmss")
        Try
            ImportFile(Appoggio)
        Finally
            Try
                If FileUpload1.FileName.ToString.IndexOf(".csv") > 0 Then
                    Kill(HostingEnvironment.ApplicationPhysicalPath() & "Public\CsvImport" & Appoggio & ".csv")
                Else
                    Kill(HostingEnvironment.ApplicationPhysicalPath() & "Public\ExcelImport" & Appoggio & ".xls")
                End If
            Catch ex As Exception

            End Try
        End Try
    End Sub

    Protected Sub Btn_Comune_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles Btn_Comune.Click
        Dim Appoggio As String = Format(Now, "yyyyMMddHHmmss")
        Try
            ImportFileComune(Appoggio)
        Finally
            Try
                If FileUpload1.FileName.ToString.IndexOf(".csv") > 0 Then
                    Kill(HostingEnvironment.ApplicationPhysicalPath() & "Public\CsvImport" & Appoggio & ".csv")
                Else
                    Kill(HostingEnvironment.ApplicationPhysicalPath() & "Public\ExcelImport" & Appoggio & ".xls")
                End If
            Catch ex As Exception

            End Try
        End Try
    End Sub


    Private Sub ImportFileComune(ByVal Appoggio As String)
        Dim cn As OdbcConnection
        Dim nomefileexcel As String

        Dim NomeSocieta As String


        If FileUpload2.FileName = "" Then
            ClientScript.RegisterClientScriptBlock(Me.GetType(), "Errore", "VisualizzaErrore('File non indicato');", True)
            Exit Sub
        End If

        Dim k As New Cls_Login

        k.Utente = Session("UTENTE")
        k.LeggiSP(Application("SENIOR"))

        NomeSocieta = k.NomeEPersonam




        FileUpload2.SaveAs(HostingEnvironment.ApplicationPhysicalPath() & "\Public\CsvImport" & Appoggio & ".csv")

        If Dir(HostingEnvironment.ApplicationPhysicalPath() & "\Public\CsvImport" & Appoggio & ".csv") = "" Then
            ClientScript.RegisterClientScriptBlock(Me.GetType(), "Errore", "VisualizzaErrore('File csv non trovata');", True)
            Exit Sub
        End If


        cn = New Data.Odbc.OdbcConnection("Driver={Microsoft Text Driver (*.txt; *.csv)};Dbq=" & HostingEnvironment.ApplicationPhysicalPath() & "Public\;HDR=Yes;")



        cn.Open()

        Dim cmd As New OdbcCommand()

        cmd.CommandText = ("select * from [" & "CsvImport" & Appoggio & ".csv" & "]")



        cmd.Connection = cn
        Dim myPOSTreader As OdbcDataReader = cmd.ExecuteReader()
        Do While myPOSTreader.Read
            Dim Vettore(100) As String
            Dim CampoInput As String
            Dim CODICEFISCALE As String
            Dim IMPORTO As Double
            Dim IMPORTO1 As Double


            CampoInput = myPOSTreader.Item(0)
            Try
                If campodb(myPOSTreader.Item(1)) <> "" Then
                    CampoInput = CampoInput & "," & myPOSTreader.Item(1)
                End If
                If campodb(myPOSTreader.Item(2)) <> "" Then
                    CampoInput = CampoInput & "," & myPOSTreader.Item(2)
                End If
            Catch ex As Exception

            End Try

            Vettore = SplitWordsCSV(CampoInput)
            Try
                CODICEFISCALE = Vettore(0)
                IMPORTO = Vettore(1)
                IMPORTO1 = Vettore(2)

            Catch ex As Exception
                cn.Close()
                ClientScript.RegisterClientScriptBlock(Me.GetType(), "Errore", "VisualizzaErrore('Errore nella struttura del file');", True)
                Kill(HostingEnvironment.ApplicationPhysicalPath() & "Public\CsvImport" & Appoggio & ".csv")
                Exit Sub
            End Try

            Dim Cos As New ClsOspite

            Cos.CodiceOspite = 0
            Cos.LeggiPerCodiceFiscale(Session("DC_OSPITE"), CODICEFISCALE)

            If Cos.CodiceOspite > 0 Then
                Dim CservMov As New Cls_Movimenti

                CservMov.CodiceOspite = Cos.CodiceOspite
                CservMov.UltimaData(Session("DC_OSPITE"), CservMov.CodiceOspite)

         
                Dim ImpOsp As New Cls_ImportoComune

                ImpOsp.Data = CservMov.Data
                ImpOsp.CENTROSERVIZIO = CservMov.CENTROSERVIZIO
                ImpOsp.CODICEOSPITE = Cos.CodiceOspite
                ImpOsp.UltimaData(Session("DC_OSPITE"), ImpOsp.CODICEOSPITE, ImpOsp.CENTROSERVIZIO)

                ImpOsp.Importo = IMPORTO
                ImpOsp.Importo1 = IMPORTO1
                ImpOsp.AggiornaDB(Session("DC_OSPITE"))

            End If
        Loop
        myPOSTreader.Close()

        cn.Close()

    End Sub




    Private Sub ImportFileAnagrafiche(ByVal Appoggio As String)
        Dim cn As OdbcConnection
        Dim nomefileexcel As String

        Dim NomeSocieta As String


        If FileUpload3.FileName = "" Then
            ClientScript.RegisterClientScriptBlock(Me.GetType(), "Errore", "VisualizzaErrore('File non indicato');", True)
            Exit Sub
        End If

        Dim k As New Cls_Login

        k.Utente = Session("UTENTE")
        k.LeggiSP(Application("SENIOR"))

        NomeSocieta = k.NomeEPersonam




        FileUpload3.SaveAs(HostingEnvironment.ApplicationPhysicalPath() & "\Public\CsvImport" & Appoggio & ".csv")

        If Dir(HostingEnvironment.ApplicationPhysicalPath() & "\Public\CsvImport" & Appoggio & ".csv") = "" Then
            ClientScript.RegisterClientScriptBlock(Me.GetType(), "Errore", "VisualizzaErrore('File csv non trovata');", True)
            Exit Sub
        End If


        cn = New Data.Odbc.OdbcConnection("Driver={Microsoft Text Driver (*.txt; *.csv)};Dbq=" & HostingEnvironment.ApplicationPhysicalPath() & "Public\;HDR=Yes;")



        cn.Open()

        Dim cmd As New OdbcCommand()

        cmd.CommandText = ("select * from [" & "CsvImport" & Appoggio & ".csv" & "]")



        cmd.Connection = cn
        Dim myPOSTreader As OdbcDataReader = cmd.ExecuteReader()
        Do While myPOSTreader.Read
            Dim Vettore(100) As String
            Dim CampoInput As String
            Dim CODICEFISCALE As String
            Dim Cognome As String
            Dim Nome As String
            Dim convenzionato As String

            Dim centroservizio As String
            Dim data_ingresso As String
            Dim Sesso As String
            Dim data_nascita As String
            Dim cod_comune_nascita As String
            Dim descrizione_comune_nascita As String

            Dim indirizzo_residenza As String

            Dim cod_comune_residenza As String
            Dim descrizione_comune_residenza As String
            Dim cap_residenza As String
            Dim telefoni As String
            Dim mail As String




            CampoInput = myPOSTreader.Item(0)
            Try
                If campodb(myPOSTreader.Item(1)) <> "" Then
                    CampoInput = CampoInput & "," & myPOSTreader.Item(1)
                End If
                If campodb(myPOSTreader.Item(2)) <> "" Then
                    CampoInput = CampoInput & "," & myPOSTreader.Item(2)
                End If
            Catch ex As Exception

            End Try

            Vettore = SplitWordsCSV(CampoInput)
            Try
                CODICEFISCALE = Vettore(0)
                Cognome = Vettore(1)
                Nome = Vettore(2)
                convenzionato = Vettore(3)

                centroservizio = Vettore(4)
                data_ingresso = Vettore(5)
                Sesso = Vettore(6)
                data_nascita = Vettore(7)
                cod_comune_nascita = Vettore(8)
                descrizione_comune_nascita = Vettore(9)

                indirizzo_residenza = Vettore(10)

                cod_comune_residenza = Vettore(11)
                descrizione_comune_residenza = Vettore(12)
                cap_residenza = Vettore(13)
                telefoni = Vettore(14)
                mail = Vettore(15)

            Catch ex As Exception
                cn.Close()
                ClientScript.RegisterClientScriptBlock(Me.GetType(), "Errore", "VisualizzaErrore('Errore nella struttura del file');", True)
                Kill(HostingEnvironment.ApplicationPhysicalPath() & "Public\CsvImport" & Appoggio & ".csv")
                Exit Sub
            End Try

            Dim Cos As New ClsOspite

            Cos.CodiceOspite = 0
            Cos.LeggiPerCodiceFiscale(Session("DC_OSPITE"), CODICEFISCALE)

            If Cos.CodiceOspite > 0 Then


                Cos.TIPOLOGIA = "O"
                Cos.Nome = Cognome & " " & Nome
                Cos.CognomeOspite = Cognome
                Cos.CODICEFISCALE = CODICEFISCALE
                Cos.NomeOspite = Nome
                Cos.Sesso = Sesso
                Try
                    Cos.DataNascita = data_nascita
                Catch ex As Exception

                End Try

                Dim AppoggioComune As String

                AppoggioComune = cod_comune_nascita
                If Len(AppoggioComune) = 5 Then
                    AppoggioComune = "0" & AppoggioComune
                End If

                Cos.ProvinciaDiNascita = Mid(AppoggioComune & Space(10), 1, 3)
                Cos.ComuneDiNascita = Mid(AppoggioComune & Space(10), 4, 3)

                Cos.RESIDENZAINDIRIZZO1 = indirizzo_residenza
                Cos.RESIDENZACAP1 = cap_residenza

                AppoggioComune = cod_comune_residenza
                If Len(AppoggioComune) = 5 Then
                    AppoggioComune = "0" & AppoggioComune
                End If

                Cos.RESIDENZAPROVINCIA1 = Mid(AppoggioComune & Space(10), 1, 3)
                Cos.RESIDENZACOMUNE1 = Mid(AppoggioComune & Space(10), 4, 3)


                Cos.RESIDENZATELEFONO1 = telefoni
                Cos.RESIDENZATELEFONO3 = mail
                Cos.UTENTE = "IMPORT"
                Cos.ScriviOspite(Session("DC_OSPITE"))


            Else
                Cos.Nome = Cognome & " " & Nome
                Cos.CognomeOspite = Cognome
                Cos.NomeOspite = Nome
                Try
                    Cos.DataNascita = data_nascita
                Catch ex As Exception

                End Try

                Cos.TIPOLOGIA = "O"

                Cos.InserisciOspite(Session("DC_OSPITE"), Cognome, Nome, Cos.DataNascita)



                Cos.TIPOLOGIA = "O"
                Cos.Nome = Cognome & " " & Nome
                Cos.CognomeOspite = Cognome
                Cos.CODICEFISCALE = CODICEFISCALE
                Cos.NomeOspite = Nome
                Cos.Sesso = Sesso
                Try
                    Cos.DataNascita = data_nascita
                Catch ex As Exception

                End Try

                Dim AppoggioComune As String

                AppoggioComune = cod_comune_nascita
                If Len(AppoggioComune) = 5 Then
                    AppoggioComune = "0" & AppoggioComune
                End If

                Cos.ProvinciaDiNascita = Mid(AppoggioComune & Space(10), 1, 3)
                Cos.ComuneDiNascita = Mid(AppoggioComune & Space(10), 4, 3)

                Cos.RESIDENZAINDIRIZZO1 = indirizzo_residenza
                Cos.RESIDENZACAP1 = cap_residenza

                AppoggioComune = cod_comune_residenza
                If Len(AppoggioComune) = 5 Then
                    AppoggioComune = "0" & AppoggioComune
                End If

                Cos.RESIDENZAPROVINCIA1 = Mid(AppoggioComune & Space(10), 1, 3)
                Cos.RESIDENZACOMUNE1 = Mid(AppoggioComune & Space(10), 4, 3)


                Cos.RESIDENZATELEFONO1 = telefoni
                Cos.RESIDENZATELEFONO3 = mail
                Cos.UTENTE = "IMPORT"
                Cos.ScriviOspite(Session("DC_OSPITE"))


                Dim Movim As New Cls_Movimenti


                Movim.CENTROSERVIZIO = centroservizio
                Try
                    Movim.Data = data_ingresso
                Catch ex As Exception

                End Try
                Movim.CodiceOspite = Cos.CodiceOspite
                Movim.TipoMov = "05"
                Movim.Utente = "IMPORT"
                Movim.AggiornaDB(Session("DC_OSPITE"))

            End If


        Loop
        myPOSTreader.Close()

        cn.Close()

    End Sub
    Protected Sub Btn_Anagrafiche_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles Btn_Anagrafiche.Click
        Dim Appoggio As String = Format(Now, "yyyyMMddHHmmss")
        Try
            ImportFileAnagrafiche(Appoggio)
        Finally
            Try
                If FileUpload1.FileName.ToString.IndexOf(".csv") > 0 Then
                    Kill(HostingEnvironment.ApplicationPhysicalPath() & "Public\CsvImport" & Appoggio & ".csv")
                Else
                    Kill(HostingEnvironment.ApplicationPhysicalPath() & "Public\ExcelImport" & Appoggio & ".xls")
                End If
            Catch ex As Exception

            End Try
        End Try
    End Sub


    Protected Sub Btn_MettiIlParentePagante_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles Btn_MettiIlParentePagante.Click


        Dim cn As OleDbConnection


        cn = New Data.OleDb.OleDbConnection(Session("DC_OSPITE"))

        cn.Open()
        Dim cmd As New OleDbCommand()


        cmd.CommandText = "select * from AnagraficaComune  where OspiteIntestatario = 1"
        cmd.Connection = cn

        Dim myPOSTreader As OleDbDataReader = cmd.ExecuteReader()
        Do While myPOSTreader.Read
            Dim ImpMov As New Cls_Movimenti


            ImpMov.CodiceOspite = campodb(myPOSTreader.Item("CodiceOspite"))
            ImpMov.UltimaDataAccoglimentoSenzaCserv(Session("DC_OSPITE"))

            Dim ImpPar As New Cls_ImportoParente


            ImpPar.CODICEOSPITE = campodb(myPOSTreader.Item("CodiceOspite"))
            ImpPar.CODICEPARENTE = campodb(myPOSTreader.Item("CodiceParente"))
            ImpPar.CENTROSERVIZIO = ImpMov.CENTROSERVIZIO
            ImpPar.UltimaData(Session("DC_OSPITE"), ImpPar.CODICEOSPITE, ImpPar.CENTROSERVIZIO, ImpPar.CODICEPARENTE)

            Dim OspP As New ClsOspite


            OspP.CodiceOspite = ImpPar.CODICEOSPITE
            OspP.Leggi(Session("DC_OSPITE"), OspP.CodiceOspite)



            Dim ImpOsp As New Cls_ImportoOspite

            ImpOsp.CODICEOSPITE = ImpPar.CODICEOSPITE
            ImpOsp.CENTROSERVIZIO = ImpMov.CENTROSERVIZIO
            ImpOsp.UltimaData(Session("DC_OSPITE"), ImpOsp.CODICEOSPITE, ImpMov.CENTROSERVIZIO)

            If ImpPar.IMPORTO = 0 Then
                Dim UpParente As New Cls_Parenti

                UpParente.CodiceOspite = ImpPar.CODICEOSPITE
                UpParente.CodiceParente = campodb(myPOSTreader.Item("CodiceParente"))
                UpParente.Leggi(Session("DC_OSPITE"), UpParente.CodiceOspite, UpParente.CodiceParente)

                UpParente.TIPOOPERAZIONE = OspP.TIPOOPERAZIONE
                UpParente.CODICEIVA = OspP.CODICEIVA
                UpParente.Compensazione = OspP.Compensazione
                UpParente.MODALITAPAGAMENTO = OspP.MODALITAPAGAMENTO
                UpParente.PERIODO = OspP.PERIODO
                UpParente.FattAnticipata = OspP.FattAnticipata
                UpParente.ScriviParente(Session("DC_OSPITE"))

                Dim datiosppare As New Cls_DatiOspiteParenteCentroServizio

                datiosppare.CentroServizio = ImpPar.CENTROSERVIZIO
                datiosppare.CodiceOspite = ImpPar.CODICEOSPITE
                datiosppare.CodiceParente = campodb(myPOSTreader.Item("CodiceParente"))

                datiosppare.TipoOperazione = OspP.TIPOOPERAZIONE
                datiosppare.AliquotaIva = OspP.CODICEIVA
                datiosppare.Compensazione = OspP.Compensazione
                datiosppare.ModalitaPagamento = OspP.MODALITAPAGAMENTO
                datiosppare.Anticipata = OspP.FattAnticipata
                datiosppare.Scrivi(Session("DC_OSPITE"))

                Dim ImpParR As New Cls_ImportoParente


                ImpParR.CENTROSERVIZIO = ImpPar.CENTROSERVIZIO
                ImpParR.CODICEOSPITE = OspP.CodiceOspite
                ImpParR.CODICEPARENTE = campodb(myPOSTreader.Item("CodiceParente"))
                ImpParR.IMPORTO = ImpOsp.Importo
                ImpParR.IMPORTO1 = ImpOsp.Importo1
                ImpParR.TIPOIMPORTO = "G"
                ImpParR.DATA = ImpMov.Data
                ImpParR.AggiornaDB(Session("DC_OSPITE"))

                Dim ImpOspR As New Cls_ImportoOspite

                ImpOspR.CODICEOSPITE = ImpPar.CODICEOSPITE
                ImpOspR.CENTROSERVIZIO = ImpMov.CENTROSERVIZIO
                ImpOspR.UltimaData(Session("DC_OSPITE"), ImpOsp.CODICEOSPITE, ImpMov.CENTROSERVIZIO)

                ImpOspR.Importo = 0
                ImpOspR.Importo1 = 0
                ImpOspR.AggiornaDB(Session("DC_OSPITE"))

            End If


        Loop
        myPOSTreader.Close()
        cn.Close()
    End Sub
End Class
