﻿Imports System.Web.Hosting
Imports Newtonsoft.Json
Imports Newtonsoft.Json.Linq
Imports System.Data.OleDb


Partial Class OspitiWeb_Menu_Ospiti
    Inherits System.Web.UI.Page
    Private Sub AutentificaLogin(ByVal utente As String)
        Dim tkt As FormsAuthenticationTicket
        Dim cookiestr As String
        Dim ck As HttpCookie

        tkt = New FormsAuthenticationTicket(1, utente, DateTime.Now, DateTime.Now.AddMinutes(30), True, "")
        cookiestr = FormsAuthentication.Encrypt(tkt)
        ck = New HttpCookie(FormsAuthentication.FormsCookieName, cookiestr)
        ck.Expires = tkt.Expiration
        ck.Path = FormsAuthentication.FormsCookiePath
        Response.Cookies.Add(ck)

    End Sub
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Dim M As New ValidaJwtws.WebService
        Dim TokenDAE As String = ""
        Dim UtenteDiEpersonam As String = ""

        TokenDAE = Request.Form("token")

        
        If TokenDAE <> "" Then


            Dim Utente As String = ""

            Try
                Utente = M.ValidaJwtws(TokenDAE)
            Catch ex As Exception
                Response.Redirect("/Login.aspx?UTENTE=" & TokenDAE)
            End Try



            Dim jResults As JObject = JObject.Parse(Utente)

            Try
                UtenteDiEpersonam = jResults.Item("una").ToString
            Catch ex As Exception

            End Try
        End If


        If UtenteDiEpersonam <> "" Then
            Call ForzaLogin(UtenteDiEpersonam)
            AutentificaLogin(UtenteDiEpersonam)
        End If
        Session("CODICEOSPITE") = 0

        Try
            If Session("ABILITAZIONI").IndexOf("<STATISTICHE>") >= 0 Then
                Response.Redirect("Menu_Visualizzazioni.aspx")
                Exit Sub
            End If


            If IsNothing(Session("UTENTE")) Then
                Response.Redirect("..\Login.aspx")
                Exit Sub
            End If

            If Trim(Session("UTENTE")) = "" Then
                Response.Redirect("..\Login.aspx")
                Exit Sub
            End If

            If IsNothing(Session("DC_OSPITE")) Then
                Response.Redirect("..\Login.aspx")
                Exit Sub
            End If
            If Session("ABILITAZIONI").IndexOf("<PORTINERIA>") > 0 Then
                Response.Redirect("..\MainPortineria.aspx")
            End If

            If Session("ABILITAZIONI").IndexOf("<NONOSPITI>") > 0 Then
                Response.Redirect("..\MainMenu.aspx")
            End If


        Catch ex As Exception
            Response.Redirect("..\Login.aspx?UTENTE=" & TokenDAE)
            Exit Sub
        End Try

        Try
            Dim Barra As New Cls_BarraSenior


            Lbl_BarraSenior.Text = Barra.CodiceBarra(Application("SENIOR"), Session("UTENTE"), Page)
        Catch ex As Exception
            Response.Redirect("..\Login.aspx?UTENTE=" & TokenDAE)
            Exit Sub
        End Try

        Dim Param As New Cls_Parametri

        Param.LeggiParametri(Session("DC_OSPITE"))



        If Not IsNothing(Session("ABILITAZIONI")) Then
            'id="idtabelle"
            If Session("ABILITAZIONI").IndexOf("<ANAGRAFICA>") < 1 Then
                ScriptManager.RegisterStartupScript(Me, Me.GetType(), "IdStatistiche", "$(document).ready( function() { $('#IdStatistiche').attr('href', ''); });", True)
            End If

            If Session("ABILITAZIONI").IndexOf("<TABELLA>") < 1 Then
                ScriptManager.RegisterStartupScript(Me, Me.GetType(), "ErroreT", "$(document).ready( function() { $('#idtabelle').attr('href', ''); });", True)

                ScriptManager.RegisterStartupScript(Me, Me.GetType(), "AddMult", "$(document).ready( function() { $('#IDAddebitiAccreditiMultipli').attr('href', ''); });", True)

                ScriptManager.RegisterStartupScript(Me, Me.GetType(), "IdEPersonam", "$(document).ready( function() { $('#IdEPersonam').attr('href', ''); });", True)
                ScriptManager.RegisterStartupScript(Me, Me.GetType(), "IdExport", "$(document).ready( function() { $('#IdExport').attr('href', ''); });", True)

                ScriptManager.RegisterStartupScript(Me, Me.GetType(), "IdElaborazione", "$(document).ready( function() { $('#IdElaborazione').attr('href', ''); });", True)

                ScriptManager.RegisterStartupScript(Me, Me.GetType(), "idDiurni", "$(document).ready( function() { $('#idDiurni').attr('href', ''); });", True)
                ScriptManager.RegisterStartupScript(Me, Me.GetType(), "idDomiciliari", "$(document).ready( function() { $('#idDomiciliari').attr('href', ''); });", True)

            End If
            'CALCOLO
            If Session("ABILITAZIONI").IndexOf("<CALCOLO>") < 1 Then
                ScriptManager.RegisterStartupScript(Me, Me.GetType(), "ErroreC", "$(document).ready( function() { $('#idcalcolo').attr('href', ''); });", True)
            End If
            If Session("ABILITAZIONI").IndexOf("<EMISSIONE>") < 1 Then
                ScriptManager.RegisterStartupScript(Me, Me.GetType(), "ErroreE", "$(document).ready( function() { $('#idemissione').attr('href', ''); });", True)
            End If
            'idstrumenti
            If Session("ABILITAZIONI").IndexOf("<SERVIZI>") < 1 Then
                ScriptManager.RegisterStartupScript(Me, Me.GetType(), "ErroreS", "$(document).ready( function() { $('#idstrumenti').attr('href', ''); });", True)
            End If

            If Session("ABILITAZIONI").IndexOf("<EPERSONAM-AUT>") >= 0 Then
                If Param.ApiV1 = 1 Then
                    If Not IsDate(Param.DataCheckEpersonam) Or Year(Param.DataCheckEpersonam) < 1900 Then
                        ScriptManager.RegisterStartupScript(Me, Me.GetType(), "Errore", "$(document).ready( function() { $('#idospiti').attr('href', 'ApiV1Epersonam/allineaospitiparentiv1.aspx?RICERCA=SI'); });", True)
                    Else
                        If DateDiff(DateInterval.Minute, Param.DataCheckEpersonam, Now) > 120 Then
                            ScriptManager.RegisterStartupScript(Me, Me.GetType(), "Errore", "$(document).ready( function() { $('#idospiti').attr('href', 'ApiV1Epersonam/allineaospitiparentiv1.aspx?RICERCA=SI'); });", True)
                        End If
                    End If
                Else
                    If Not IsDate(Param.DataCheckEpersonam) Or Year(Param.DataCheckEpersonam) < 1900 Then
                        ScriptManager.RegisterStartupScript(Me, Me.GetType(), "Errore", "$(document).ready( function() { $('#idospiti').attr('href', 'ApiV1Epersonam/allineaospitiparentiv1.aspx?RICERCAOSPITI=SI'); });", True)
                        ScriptManager.RegisterStartupScript(Me, Me.GetType(), "Errore", "$(document).ready( function() { $('#idmovimenti').attr('href', 'ApiV1Epersonam/allineaospitiparentiv1.aspx?RICERCAOSPITI=SI&TIPO=MOVIMENTI'); });", True)
                        ScriptManager.RegisterStartupScript(Me, Me.GetType(), "Errore", "$(document).ready( function() { $('#iddiurno').attr('href', 'ApiV1Epersonam/allineaospitiparentiv1.aspx?RICERCAOSPITI=SI&TIPO=DIURNO'); });", True)
                        ScriptManager.RegisterStartupScript(Me, Me.GetType(), "Errore", "$(document).ready( function() { $('#iddom').attr('href', 'ApiV1Epersonam/allineaospitiparentiv1.aspx?RICERCAOSPITI=SI&TIPO=DOM'); });", True)
                        ScriptManager.RegisterStartupScript(Me, Me.GetType(), "Errore", "$(document).ready( function() { $('#idaddacr').attr('href', 'ApiV1Epersonam/allineaospitiparentiv1.aspx?RICERCAOSPITI=SI&TIPO=ADDACR'); });", True)
                        ScriptManager.RegisterStartupScript(Me, Me.GetType(), "Errore", "$(document).ready( function() { $('#idfatnc').attr('href', 'ApiV1Epersonam/allineaospitiparentiv1.aspx?RICERCAOSPITI=SI&TIPO=FATTURANC'); });", True)
                        ScriptManager.RegisterStartupScript(Me, Me.GetType(), "Errore", "$(document).ready( function() { $('#idincant').attr('href', 'ApiV1Epersonam/allineaospitiparentiv1.aspx?RICERCAOSPITI=SI&TIPO=INCASSIANTICIPI'); });", True)
                    Else
                        If DateDiff(DateInterval.Minute, Param.DataCheckEpersonam, Now) > 120 Then
                            ScriptManager.RegisterStartupScript(Me, Me.GetType(), "Errore", "$(document).ready( function() { $('#idospiti').attr('href', 'ApiV1Epersonam/allineaospitiparentiv1.aspx?RICERCAOSPITI=SI'); });", True)
                            ScriptManager.RegisterStartupScript(Me, Me.GetType(), "Errore", "$(document).ready( function() { $('#idmovimenti').attr('href', 'ApiV1Epersonam/allineaospitiparentiv1.aspx?RICERCAOSPITI=SI&TIPO=MOVIMENTI'); });", True)
                            ScriptManager.RegisterStartupScript(Me, Me.GetType(), "Errore", "$(document).ready( function() { $('#iddiurno').attr('href', 'ApiV1Epersonam/allineaospitiparentiv1.aspx?RICERCAOSPITI=SI&TIPO=DIURNO'); });", True)
                            ScriptManager.RegisterStartupScript(Me, Me.GetType(), "Errore", "$(document).ready( function() { $('#iddom').attr('href', 'ApiV1Epersonam/allineaospitiparentiv1.aspx?RICERCAOSPITI=SI&TIPO=DOM'); });", True)
                            ScriptManager.RegisterStartupScript(Me, Me.GetType(), "Errore", "$(document).ready( function() { $('#idaddacr').attr('href', 'ApiV1Epersonam/allineaospitiparentiv1.aspx?RICERCAOSPITI=SI&TIPO=ADDACR'); });", True)
                            ScriptManager.RegisterStartupScript(Me, Me.GetType(), "Errore", "$(document).ready( function() { $('#idfatnc').attr('href', 'ApiV1Epersonam/allineaospitiparentiv1.aspx?RICERCAOSPITI=SI&TIPO=FATTURANC'); });", True)
                            ScriptManager.RegisterStartupScript(Me, Me.GetType(), "Errore", "$(document).ready( function() { $('#idincant').attr('href', 'ApiV1Epersonam/allineaospitiparentiv1.aspx?RICERCAOSPITI=SI&TIPO=INCASSIANTICIPI'); });", True)
                        End If
                    End If
                End If
            End If
        End If


        VerificaNews()



        VerificaNotifica()

        REM Response.Cookies("UserSettings")("Entrata") = "7"
        REM Response.Cookies("UserSettings").Expires = "July 31, 2020"
    End Sub



    Private Sub VerificaNotifica()
        Dim cn As OleDbConnection



        cn = New Data.OleDb.OleDbConnection(Session("DC_OSPITE"))

        cn.Open()
        Dim cmd As New OleDbCommand()
        cmd.CommandText = ("select * from notifiche ")
        cmd.Connection = cn
        Dim myPOSTreader As OleDbDataReader = cmd.ExecuteReader()
        If myPOSTreader.Read Then
            lblElaborazioneCompletata.Text = "<br/><a href=""Push/VerificaFattureNotifica.aspx"">Elaborazione completata</a>"
            If campodb(myPOSTreader.Item("ErroriCalcolo")) <> "" Then
                lblElaborazioneCompletata.Text = lblElaborazioneCompletata.Text & "<br/>Errori in calcolo"
            End If
            If campodb(myPOSTreader.Item("ErroriEmissione")) <> "" Then
                lblElaborazioneCompletata.Text = lblElaborazioneCompletata.Text & "<br/>Errori in emissione"
            End If
            If campodb(myPOSTreader.Item("ErroriWarning")) <> "" Then
                lblElaborazioneCompletata.Text = lblElaborazioneCompletata.Text & "<br/>Warning in emissione"
            End If
            If campodb(myPOSTreader.Item("ErroriStampa")) <> "" Then
                lblElaborazioneCompletata.Text = lblElaborazioneCompletata.Text & "<br/>Errori in stampa"
            End If

            ScriptManager.RegisterStartupScript(Me, Me.GetType(), "chiudinotifiche", "$(document).ready(function() { visualizzanotifiche(); });", True)

        Else
            ScriptManager.RegisterStartupScript(Me, Me.GetType(), "chiudinotifiche", "$(document).ready(function() { chiudinotifiche(); });", True)
        End If
        myPOSTreader.Close()
        cn.Close()
    End Sub


    Function campodb(ByVal oggetto As Object) As String
        If IsDBNull(oggetto) Then
            Return ""
        Else
            Return oggetto
        End If
    End Function

    Private Sub ForzaLogin(ByVal Utente As String)

        Dim ktest As New Cls_Login
        Dim k As New Cls_Login

        Session("TIPOAPP") = "RSA"

        ktest.Utente = Utente & "<1>"
        ktest.LeggiSP(Application("SENIOR"))
        Session("Password") = Request.Form("Txt_Enc_Password")


        If ktest.Ospiti <> "" Then
            Session("UTENTE") = Utente

            If DateDiff(DateInterval.Day, k.ScadenzaPassword, Now) > 0 Then
                'Response.Redirect("../ModificaPassword.aspx")
                'Exit Sub
            End If

            If ktest.PasswordCriptata <> "" Then
                Session("Password") = ktest.PasswordCriptata
            Else
                Session("Password") = ktest.Chiave
            End If
            AutentificaLogin(Utente)
            Response.Redirect("../Menu_Societa.aspx?UTENTE=" & Utente)
            Exit Sub
        End If


        k.Utente = Utente.ToLower
        k.Ip = Context.Request.ServerVariables("REMOTE_ADDR")
        k.Sistema = Request.UserAgent & " " & Request.Browser.Browser & " " & Request.Browser.MajorVersion & " " & Request.Browser.MinorVersion
        k.LeggiSP(Application("SENIOR"))

        If DateDiff(DateInterval.Day, k.ScadenzaPassword, Now) > 0 Then
            Response.Redirect("../ModificaPassword.aspx")
            Exit Sub
        End If



        Session("Password") = k.Chiave
        Session("ChiaveCr") = k.ChiaveCr
        Session("CLIENTE") = k.Cliente


        Session("SOCIETALI") = k.CaricaSocieta(Application("SENIOR"), k.Utente)

        Session("SENIOR") = Application("SENIOR")
        Session("DC_OSPITE") = k.Ospiti
        Session("DC_OSPITIACCESSORI") = k.OspitiAccessori
        Session("DC_TABELLE") = k.TABELLE
        Session("DC_GENERALE") = k.Generale
        Session("DC_TURNI") = k.Turni
        Session("STAMPEOSPITI") = k.STAMPEOSPITI
        Session("STAMPEFINANZIARIA") = k.STAMPEFINANZIARIA
        Session("ProgressBar") = ""
        Session("CODICEREGISTRO") = ""
        Session("NomeEPersonam") = k.NomeEPersonam
        Session("GDPRAttivo") = k.GDPRAttivo

        k.LeggiEpersonamUser(Application("SENIOR"))

        Session("EPersonamUser") = k.EPersonamUser.Replace("w", "")
        Session("EPersonamPSW") = k.EPersonamPSW
        Session("EPersonamPSWCRYPT") = k.EPersonamPSWCRYPT

        If k.Ospiti <> "" Then
            Session("UTENTE") = k.Utente.ToLower
            Session("Password") = k.Chiave
            Session("ChiaveCr") = k.ChiaveCr
            Session("ABILITAZIONI") = k.ABILITAZIONI

            'If DateDiff(DateInterval.Day, k.ScadenzaPassword, Now) > 0 Then
            '    Response.Redirect("../ModificaPassword.aspx")
            '    Exit Sub
            'End If

            If Session("ABILITAZIONI").IndexOf("<PORTINERIA>") > 0 Then
                Response.Redirect("MainPortineria.aspx")
                Exit Sub
            End If
            If Session("ABILITAZIONI").IndexOf("<SOLOGENERALE>") > 0 Then
                Response.Redirect("GeneraleWeb\Menu_Generale.aspx")
                Exit Sub
            End If
        Else
            ScriptManager.RegisterStartupScript(Me, Me.GetType(), "Errore", "VisualizzaErrore('<center><b>Nome utente o password errati</b></center>');", True)
        End If
    End Sub

    Protected Sub BtnClose_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles BtnClose.Click
        ScriptManager.RegisterStartupScript(Me, Me.GetType(), "chiudinews", "$(document).ready(function() { chiudinotifiche(); });", True)


        Dim cn As OleDbConnection



        cn = New Data.OleDb.OleDbConnection(Session("DC_OSPITE"))

        cn.Open()
        Dim cmd As New OleDbCommand()
        cmd.CommandText = ("Delete  from notifiche ")
        cmd.Connection = cn
        cmd.ExecuteNonQuery()
        cn.Close()

        ScriptManager.RegisterStartupScript(Me, Me.GetType(), "chiudinotifiche", "$(document).ready(function() { chiudinotifiche(); });", True)

        VerificaNotifica()

        Dim MyJs As String


        MyJs = "       $(document).ready(function() { chiudinotifiche();  "
        MyJs = MyJs & "   if (window.innerHeight > 0) { $(""#BarraLaterale"").css(""height"", (window.innerHeight - 105) + ""px""); } else"
        MyJs = MyJs & "   { $(""#BarraLaterale"").css(""height"", (document.documentElement.offsetHeight - 105) + ""px""); }"
        MyJs = MyJs & "           posizionenotifiche();"
        MyJs = MyJs & "       });"
        ScriptManager.RegisterStartupScript(Me, Me.GetType(), "impostabarra", MyJs, True)
    End Sub

    Protected Sub form1_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles form1.Load

    End Sub

    Protected Sub Notifica_Tick(ByVal sender As Object, ByVal e As System.EventArgs) Handles Notifica.Tick
        VerificaNotifica()
        Dim MyJs As String


        MyJs = "       $(document).ready(function() { "
        MyJs = MyJs & " $(""#Txt_NOMEOSPITEBARRA"").autocomplete('AutoCompleteOspitiTuttiCentriServizio.ashx?Utente=" & Session("UTENTE") & "&CSERV=2', {delay:5,minChars:3 });" & vbNewLine
        MyJs = MyJs & "    $(""#Txt_NOMEOSPITEBARRA"").focus();"
        MyJs = MyJs & "   if (window.innerHeight > 0) { $(""#BarraLaterale"").css(""height"", (window.innerHeight - 105) + ""px""); } else"
        MyJs = MyJs & "   { $(""#BarraLaterale"").css(""height"", (document.documentElement.offsetHeight - 105) + ""px""); }"
        MyJs = MyJs & "           posizionenotifiche();"
        MyJs = MyJs & "    });"

        ScriptManager.RegisterStartupScript(Me, Me.GetType(), "impostabarra", MyJs, True)

        MyJs = "$(document).ready(function() {"
        MyJs = MyJs & " $(""#Txt_NOMEOSPITEBARRA"").focus(function() {" & vbNewLine
        MyJs = MyJs & " $(""#menuolga"").css(""display"", ""block"");" & vbNewLine
        MyJs = MyJs & " });" & vbNewLine

        MyJs = MyJs & " $(""#TXT_NOTEOSPITE"").focus(function() { " & vbNewLine
        MyJs = MyJs & " $(""#menunote"").css(""display"", ""block"");" & vbNewLine
        MyJs = MyJs & " });" & vbNewLine

        MyJs = MyJs & "$('body').click(function() {" & vbNewLine
        MyJs = MyJs & "$(""#TastiSenior"").css(""display"", ""none"");" & vbNewLine
        MyJs = MyJs & "$(""#menunote"").css(""display"", ""none"");" & vbNewLine
        MyJs = MyJs & "$(""#menuolga"").css(""display"", ""none"");" & vbNewLine
        MyJs = MyJs & "});" & vbNewLine


        MyJs = MyJs & "$(""#MenuNote"").mouseover(function() {" & vbNewLine
        MyJs = MyJs & "$(""#menunote"").css(""display"", ""block"");" & vbNewLine
        MyJs = MyJs & "$(""#menuolga"").css(""display"", ""none"");" & vbNewLine
        MyJs = MyJs & "});" & vbNewLine

        MyJs = MyJs & "$(""#MenuOspitiLnk"").mouseover(function() {" & vbNewLine
        MyJs = MyJs & "$(""#menuolga"").css(""display"", ""block"");" & vbNewLine
        MyJs = MyJs & "$(""#menunote"").css(""display"", ""none"");" & vbNewLine
        MyJs = MyJs & " $(""#Txt_NOMEOSPITEBARRA"").focus();" & vbNewLine
        MyJs = MyJs & "       });"
        MyJs = MyJs & "       });"
        'MyJs = "   $(""#Txt_NOMEOSPITEBARRA"").focus(); "
        ScriptManager.RegisterStartupScript(Me, Me.GetType(), "fuocosunomeospitebarra", MyJs, True)

        VerificaNews()
    End Sub



    Private Sub VerificaNews()



        If (Request.Cookies("Entrata") IsNot Nothing) Then
            If Request.Cookies("Entrata").Value.ToUpper = "13" Then
                ScriptManager.RegisterStartupScript(Me, Me.GetType(), "chiudinews", "$(document).ready(function() { chiudinews(); });", True)
            End If
        End If
        If DateDiff(DateInterval.Day, DateSerial(2019, 5, 22), Now) > 1 Then
            ScriptManager.RegisterStartupScript(Me, Me.GetType(), "chiudinews", "$(document).ready(function() { chiudinews(); });", True)
        End If

      
    End Sub
End Class

