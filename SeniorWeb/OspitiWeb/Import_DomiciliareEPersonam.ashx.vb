﻿Imports System.Data.OleDb
Imports System.IO
Imports System.Net
Imports System.Net.Security
Imports System.Security.Cryptography.X509Certificates
Imports System.Web.Script.Serialization
Imports Newtonsoft.Json.Linq
Imports org.jivesoftware.util

Public Class Import_DomiciliareEPersonam
    Implements System.Web.IHttpHandler, IRequiresSessionState


    Public Sub ProcessRequest(ByVal context As HttpContext) Implements IHttpHandler.ProcessRequest
        context.Response.ContentType = "text/plain"
        context.Response.Cache.SetCacheability(HttpCacheability.NoCache)

        Dim VettoreMovimenti(10000) As Movimenti
        Dim Numero As Integer = 0
        Dim LastMov As String = ""


        If context.Session("UTENTE") = "" Then
            Exit Sub
        End If


        Dim Token As String
        Dim OutPutMovimenti As String = ""



        Dim cn As OleDbConnection
        Dim Trovato As Boolean = False

        cn = New Data.OleDb.OleDbConnection(context.Session("DC_OSPITE"))

        cn.Open()

        Try


            Token = LoginPersonam(context)

            Dim Data() As Byte

            Dim request As New NameValueCollection

            Dim Param As New Cls_Parametri

            Param.LeggiParametri(context.Session("DC_OSPITE"))




            System.Net.ServicePointManager.ServerCertificateValidationCallback = AddressOf CertificateHandler

            Dim cmdcs As New OleDbCommand()
            cmdcs.CommandText = ("select * from TABELLACENTROSERVIZIO Where TIPOCENTROSERVIZIO = 'A'")

            cmdcs.Connection = cn

            'Dim RDcs As OleDbDataReader = cmdcs.ExecuteReader()
            'Do While RDcs.Read


            '       " & RDcs.Item("Epersonam") & "
            '" & RDcs.Item("Epersonam") & "
            Dim client As HttpWebRequest = WebRequest.Create("https://api-v0.e-personam.com/v0/day_centers/1053/domestics/" & Param.AnnoFatturazione & "/" & Param.MeseFatturazione)

            client.Method = "GET"
            client.Headers.Add("Authorization", "Bearer " & Token)
            client.ContentType = "Content-Type: application/json"

            Dim reader As StreamReader
            Dim response As HttpWebResponse = Nothing

            response = DirectCast(client.GetResponse(), HttpWebResponse)

            reader = New StreamReader(response.GetResponseStream())


            Dim rawresp As String
            rawresp = reader.ReadToEnd()



            Dim jResults As JArray = JArray.Parse(rawresp)



            For Each jTok1 As JToken In jResults
                Dim CodiceFiscale As String
                CodiceFiscale = jTok1.Item("cf").ToString()

                Dim Ospite As New ClsOspite

                Ospite.CODICEFISCALE = CodiceFiscale
                Ospite.LeggiPerCodiceFiscale(context.Session("DC_OSPITE"), CodiceFiscale)
                If Ospite.CodiceOspite = 0 Then
                    VettoreMovimenti(Numero) = New Movimenti
                    VettoreMovimenti(Numero).Esito = "N"
                    VettoreMovimenti(Numero).CentroServizio = ""
                    VettoreMovimenti(Numero).CodiceOspite = 0
                    VettoreMovimenti(Numero).Nome = "NON PRESENTE" & CodiceFiscale
                    VettoreMovimenti(Numero).Tipo = ""
                    VettoreMovimenti(Numero).Data = ""
                    Numero = Numero + 1
                Else
                    For Each jTok2 As JToken In jTok1.Item("domestic_activities").Children

                        Dim MiaData As Date

                        Dim CentroServizio As String


                        'CentroServizio = RDcs.Item("CENTROSERVIZIO")

                        Dim MMov As New Cls_Movimenti

                        MMov.UltimaData(context.Session("DC_OSPITE"), Ospite.CodiceOspite)

                        CentroServizio = MMov.CENTROSERVIZIO


                        MiaData = jTok2.Item("date_done").ToString



                        Dim cmdcData As New OleDbCommand()
                        cmdcData.CommandText = ("select * from Domiciliare_EPersonma Where CentroServizio = ? and CodiceOspite = ? And ID_Epersonam = ?")

                        cmdcData.Parameters.AddWithValue("@CENTROSERVIZIO", CentroServizio)
                        cmdcData.Parameters.AddWithValue("@CodiceOspite", Ospite.CodiceOspite)
                        cmdcData.Parameters.AddWithValue("@Id", jTok2.Item("id").ToString)
                        cmdcData.Connection = cn
                        Dim RDData As OleDbDataReader = cmdcData.ExecuteReader()
                        If RDData.Read Then

                            VettoreMovimenti(Numero) = New Movimenti
                            VettoreMovimenti(Numero).Esito = "N"
                            VettoreMovimenti(Numero).CentroServizio = CentroServizio
                            VettoreMovimenti(Numero).CodiceOspite = Ospite.CodiceOspite
                            VettoreMovimenti(Numero).Nome = Ospite.Nome
                            VettoreMovimenti(Numero).Tipo = ""
                            VettoreMovimenti(Numero).Data = Mid(jTok2.Item("date_done").ToString, 1, 10)
                            Numero = Numero + 1
                        Else

                            Dim Vercentroservizio As New Cls_Movimenti



                            Vercentroservizio.CENTROSERVIZIO = CentroServizio
                            Vercentroservizio.CodiceOspite = Ospite.CodiceOspite
                            If Vercentroservizio.CServizioUsato(context.Session("DC_OSPITE")) Then

                                Dim Dom As New Cls_MovimentiDomiciliare

                                Dom.CENTROSERVIZIO = CentroServizio
                                Dom.CodiceOspite = Ospite.CodiceOspite

                                Dom.Data = jTok2.Item("date_done").ToString

                                Dim Ore As Integer = Mid(jTok2.Item("date_done").ToString, 12, 2)
                                Dim Minuti As Integer = Mid(jTok2.Item("date_done").ToString, 15, 2)
                                Dom.OraInizio = TimeSerial(Ore, Minuti, 0).AddYears(1899).AddMonths(12).AddDays(30)

                                Dim Durata As Long = Val(jTok2.Item("duration").ToString)

                                If Durata > 0 Then
                                    Durata = Math.Round(Durata / 15, 0) * 15
                                End If

                                Dom.OraFine = TimeSerial(Ore, Minuti, 0).AddMinutes(Durata).AddYears(1899).AddMonths(12).AddDays(30)
                                Dim NumeroOperatori As Integer = 0
                                For Each jTok3 As JToken In jTok2.Item("operators").Children
                                    NumeroOperatori = NumeroOperatori + 1
                                Next


                                Dom.Operatore = NumeroOperatori
                                If jTok2.Item("conv").ToString = "True" Then
                                    Dom.Tipologia = "01"
                                Else
                                    Dom.Tipologia = "02"
                                End If
                                If Val(jTok2.Item("activity_id").ToString) = 4064 Then
                                    Dom.Tipologia = "03" ' Pasto
                                End If
                                If Val(jTok2.Item("activity_id").ToString) = 3632 Then
                                    Dom.Tipologia = "04" ' Pasto + Cena Fredda
                                End If
                                If Val(jTok2.Item("activity_id").ToString) = 4224 Or Val(jTok2.Item("activity_id").ToString) = 3282 Or Val(jTok2.Item("activity_id").ToString) = 4156 Then
                                    Dom.Tipologia = "05" ' Tutoring
                                End If

                                Dom.Utente = context.Session("UTENTE")
                                Dom.AggiornaDB(context.Session("DC_OSPITE"))



                                Dim Mysql As String

                                Mysql = "INSERT INTO Domiciliare_EPersonma (Utente,DataAggiornamento,CentroServizio,CodiceOspite,Data,Durata,ID_Epersonam) VALUES (?,?,?,?,?,?,?)"
                                Dim cmdw As New OleDbCommand()
                                cmdw.CommandText = (Mysql)

                                cmdw.Parameters.AddWithValue("@Utente", context.Session("UTENTE"))
                                cmdw.Parameters.AddWithValue("@DataAggiornamento", Now)
                                cmdw.Parameters.AddWithValue("@CentroServizio", CentroServizio)
                                cmdw.Parameters.AddWithValue("@CodiceOspite", Ospite.CodiceOspite)
                                cmdw.Parameters.AddWithValue("@Data", Format(MiaData, "dd/MM/yyyy"))

                                cmdw.Parameters.AddWithValue("@Durata", Durata)


                                cmdw.Parameters.AddWithValue("@ID_Epersonam", Val(jTok2.Item("id").ToString))
                                cmdw.Connection = cn
                                cmdw.ExecuteNonQuery()



                                VettoreMovimenti(Numero) = New Movimenti
                                VettoreMovimenti(Numero).Esito = "I"
                                VettoreMovimenti(Numero).CentroServizio = CentroServizio
                                VettoreMovimenti(Numero).CodiceOspite = Ospite.CodiceOspite
                                VettoreMovimenti(Numero).Nome = Ospite.Nome

                                VettoreMovimenti(Numero).Tipo = ""
                                VettoreMovimenti(Numero).Data = MiaData
                                Numero = Numero + 1
                            Else
                                VettoreMovimenti(Numero) = New Movimenti
                                VettoreMovimenti(Numero).Esito = "E"
                                VettoreMovimenti(Numero).CentroServizio = CentroServizio
                                VettoreMovimenti(Numero).CodiceOspite = Ospite.CodiceOspite
                                VettoreMovimenti(Numero).Nome = Ospite.Nome
                                VettoreMovimenti(Numero).Tipo = ""
                                VettoreMovimenti(Numero).Data = MiaData
                                Numero = Numero + 1
                            End If
                        End If
                        RDData.Close()
                    Next
                End If
            Next
            'Loop
        Catch ex As Exception

        End Try

        cn.Close()
        Dim serializer As JavaScriptSerializer
        serializer = New JavaScriptSerializer()

        Dim Serializzazione As String

        Serializzazione = serializer.Serialize(VettoreMovimenti)

        context.Response.Write(Serializzazione)
    End Sub



    Private Function LoginPersonam(ByVal context As HttpContext) As String
        'Dim request As WebRequest
        '-staging
        'request = HttpWebRequest.Create("https://api-v0.e-personam.com/v0/oauth/token")
        Dim request As New NameValueCollection

        Dim BlowFish As New Blowfish("advenias2014")



        'request.Method = "POST"
        request.Add("client_id", "98217ca6670c660e22f42d58e231d4e56c84fb34e216b0f66f1f30284fd3ec9d")
        request.Add("client_secret", "5570a684f69ca74d75d16bba669c156ec092eccb4111d0974adec3edb2fa4d6f")
        request.Add("grant_type", "authorization_code")
        'request.Add("password", "advenias2014")

        'guarda = BlowFish.encryptString("advenias2014")
        Dim guarda As String

        If Trim(context.Session("EPersonamPSWCRYPT")) = "" Then
            request.Add("username", context.Session("UTENTE").ToString.Replace("<1>", "").Replace("<2>", "").Replace("<3>", "").Replace("<4>", "").Replace("<5>", "").Replace("<6>", ""))

            guarda = context.Session("ChiaveCr")
        Else
            request.Add("username", context.Session("EPersonamUser"))

            guarda = context.Session("EPersonamPSWCRYPT")
        End If


        request.Add("code", guarda)
        System.Net.ServicePointManager.ServerCertificateValidationCallback = AddressOf CertificateHandler


        Dim client As New WebClient()

        Dim result As Object = client.UploadValues("https://api-v0.e-personam.com/v0/oauth/token", "POST", request)


        Dim stringa As String = Encoding.Default.GetString(result)


        Dim serializer As JavaScriptSerializer


        serializer = New JavaScriptSerializer()




        Dim s As Autentificazione = serializer.Deserialize(Of Autentificazione)(stringa)


        Return s.access_token
    End Function

    Public Class Autentificazione
        Public access_token As String
        Public expires_in As String
        Public scope As String
        Public token_type As String
        Public refresh_token As String
    End Class




    Public Class Movimenti
        Public Esito As String
        Public CentroServizio As String
        Public CodiceOspite As String
        Public Nome As String
        Public Data As String
        Public Tipo As String
    End Class

    Private Shared Function CertificateHandler(ByVal sender As Object, ByVal certificate As X509Certificate, ByVal chain As X509Chain, ByVal SSLerror As SslPolicyErrors) As Boolean
        Return True
    End Function


    Public ReadOnly Property IsReusable() As Boolean Implements IHttpHandler.IsReusable
        Get
            Return False
        End Get
    End Property

End Class