﻿Imports System.Web.Hosting
Imports System.Data.OleDb


Partial Class OspitiWeb_ModificaRetta
    Inherits System.Web.UI.Page


    Dim MyTable As New System.Data.DataTable("tabella")
    Dim MyDataSet As New System.Data.DataSet()

    Function campodb(ByVal oggetto As Object) As String
        If IsDBNull(oggetto) Then
            Return ""
        Else
            Return oggetto
        End If
    End Function

    Protected Sub OspitiWeb_ModificaRetta_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Trim(Session("UTENTE")) = "" Then
            Response.Redirect("../Login.aspx")
            Exit Sub
        End If
        If Val(Request.Item("CodiceOspite")) > 0 And Request.Item("CentroServizio") <> "" Then

            Session("CODICESERVIZIO") = Request.Item("CentroServizio")
            Session("CODICEOSPITE") = Val(Request.Item("CodiceOspite"))
        End If



        Dim x As New ClsOspite

        x.CodiceOspite = Session("CODICEOSPITE")
        x.Leggi(Session("DC_OSPITE"), x.CodiceOspite)

        Dim cs As New Cls_CentroServizio

        cs.Leggi(Session("DC_OSPITE"), Session("CODICESERVIZIO"))


        Lbl_NomeOspite.Text = x.Nome & " -  " & x.DataNascita & " - " & cs.DESCRIZIONE & "<i style=""font-size:small;"">(" & x.CodiceOspite & ")</i>"

        Dim K1 As New Cls_SqlString

        Dim Barra As New Cls_BarraSenior

        If Not IsNothing(Session("RicercaAnagraficaSQLString")) Then
            K1 = Session("RicercaAnagraficaSQLString")
        End If

        Lbl_BarraSenior.Text = Barra.CodiceBarra(Application("SENIOR"), Session("UTENTE"), Page, K1)

        If Page.IsPostBack = True Then
            Exit Sub
        End If



        Txt_Anno.Text = Year(Now)
        Dd_MeseDa.SelectedValue = Year(Now)
    End Sub

    Protected Sub Btn_Visualizza_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles Btn_Visualizza.Click

        Dim cn As OleDbConnection
        Dim StringaConnessione As String = Session("DC_OSPITE")
        Dim MV As Boolean = False


        MyTable.Clear()
        MyTable.Columns.Add("Tipo", GetType(String))
        MyTable.Columns.Add("Giorni", GetType(String))
        MyTable.Columns.Add("Importo", GetType(String))
        MyTable.Columns.Add("Manuale", GetType(String))
        MyTable.Columns.Add("CODICEEXTRA", GetType(String))
        MyTable.Columns.Add("RIFERIMENTO", GetType(String))
        MyTable.Columns.Add("RIFERIMENTOb", GetType(String))
        MyTable.Columns.Add("AnnoCompetenza", GetType(String))
        MyTable.Columns.Add("MeseCompetenza", GetType(String))

        cn = New Data.OleDb.OleDbConnection(StringaConnessione)

        cn.Open()

        If DD_Tipo.SelectedValue = "O" Then

            Dim cmd As New OleDbCommand()
            cmd.CommandText = ("select * from RETTEOSPITE where " & _
                               "Anno = ? And Mese = ? And CODICEOSPITE = ? And CENTROSERVIZIO = ? ")
            cmd.Parameters.AddWithValue("@Anno", Txt_Anno.Text)
            cmd.Parameters.AddWithValue("@Mese", Dd_MeseDa.SelectedValue)
            cmd.Parameters.AddWithValue("@CodiceOspite", Session("CODICEOSPITE"))
            cmd.Parameters.AddWithValue("@CentroServizio", Session("CODICESERVIZIO"))

            cmd.Connection = cn

            MV = False
            Dim myPOSTreader As OleDbDataReader = cmd.ExecuteReader()
            Do While myPOSTreader.Read
                Dim myriga As System.Data.DataRow = MyTable.NewRow()

                myriga(0) = campodb(myPOSTreader.Item("ELEMENTO"))
                myriga(1) = Val(campodb(myPOSTreader.Item("GIORNI")))
                myriga(2) = CDbl(campodb(myPOSTreader.Item("IMPORTO")))
                myriga(3) = campodb(myPOSTreader.Item("MANUALE"))
                myriga(4) = campodb(myPOSTreader.Item("CODICEEXTRA"))
                myriga(5) = 0
                myriga(6) = 0
                myriga(7) = Val(campodb(myPOSTreader.Item("ANNOCOMPETENZA")))
                myriga(8) = Val(campodb(myPOSTreader.Item("MESECOMPETENZA")))

                MV = True
                MyTable.Rows.Add(myriga)
            Loop
            myPOSTreader.Close()
        End If

        If DD_Tipo.SelectedValue = "P" Then
            Dim cmd As New OleDbCommand()
            cmd.CommandText = ("select * from RETTEPARENTE where " & _
                               "Anno = ? And Mese = ? And CODICEOSPITE = ? And CENTROSERVIZIO = ? ")
            cmd.Parameters.AddWithValue("@Anno", Txt_Anno.Text)
            cmd.Parameters.AddWithValue("@Mese", Dd_MeseDa.SelectedValue)
            cmd.Parameters.AddWithValue("@CodiceOspite", Session("CODICEOSPITE"))
            cmd.Parameters.AddWithValue("@CentroServizio", Session("CODICESERVIZIO"))

            cmd.Connection = cn

            MV = False
            Dim myPOSTreader As OleDbDataReader = cmd.ExecuteReader()
            Do While myPOSTreader.Read
                Dim myriga As System.Data.DataRow = MyTable.NewRow()

                myriga(0) = campodb(myPOSTreader.Item("ELEMENTO"))
                myriga(1) = Val(campodb(myPOSTreader.Item("GIORNI")))
                myriga(2) = CDbl(campodb(myPOSTreader.Item("IMPORTO")))
                myriga(3) = campodb(myPOSTreader.Item("MANUALE"))
                myriga(4) = campodb(myPOSTreader.Item("CODICEEXTRA"))
                myriga(5) = campodb(myPOSTreader.Item("CODICEPARENTE"))
                myriga(6) = 0
                myriga(7) = Val(campodb(myPOSTreader.Item("ANNOCOMPETENZA")))
                myriga(8) = Val(campodb(myPOSTreader.Item("MESECOMPETENZA")))

                MyTable.Rows.Add(myriga)
                MV = True
            Loop
            myPOSTreader.Close()
        End If

        If DD_Tipo.SelectedValue = "C" Then
            Dim cmd As New OleDbCommand()
            cmd.CommandText = ("select * from RETTECOMUNE where " & _
                               "Anno = ? And Mese = ? And CODICEOSPITE = ? And CENTROSERVIZIO = ? ")
            cmd.Parameters.AddWithValue("@Anno", Txt_Anno.Text)
            cmd.Parameters.AddWithValue("@Mese", Dd_MeseDa.SelectedValue)
            cmd.Parameters.AddWithValue("@CodiceOspite", Session("CODICEOSPITE"))
            cmd.Parameters.AddWithValue("@CentroServizio", Session("CODICESERVIZIO"))

            cmd.Connection = cn

            MV = False
            Dim myPOSTreader As OleDbDataReader = cmd.ExecuteReader()
            Do While myPOSTreader.Read
                Dim myriga As System.Data.DataRow = MyTable.NewRow()

                myriga(0) = campodb(myPOSTreader.Item("ELEMENTO"))
                myriga(1) = Val(campodb(myPOSTreader.Item("GIORNI")))
                myriga(2) = CDbl(campodb(myPOSTreader.Item("IMPORTO")))
                myriga(3) = campodb(myPOSTreader.Item("MANUALE"))
                myriga(4) = campodb(myPOSTreader.Item("CODICEEXTRA"))
                myriga(5) = campodb(myPOSTreader.Item("PROVINCIA"))
                myriga(6) = campodb(myPOSTreader.Item("COMUNE"))
                myriga(7) = Val(campodb(myPOSTreader.Item("ANNOCOMPETENZA")))
                myriga(8) = Val(campodb(myPOSTreader.Item("MESECOMPETENZA")))


                MyTable.Rows.Add(myriga)
                MV = True
            Loop
            myPOSTreader.Close()
        End If

        If DD_Tipo.SelectedValue = "J" Then
            Dim cmd As New OleDbCommand()
            cmd.CommandText = ("select * from RETTEJOLLY where " & _
                               "Anno = ? And Mese = ? And CODICEOSPITE = ? And CENTROSERVIZIO = ? ")
            cmd.Parameters.AddWithValue("@Anno", Txt_Anno.Text)
            cmd.Parameters.AddWithValue("@Mese", Dd_MeseDa.SelectedValue)
            cmd.Parameters.AddWithValue("@CodiceOspite", Session("CODICEOSPITE"))
            cmd.Parameters.AddWithValue("@CentroServizio", Session("CODICESERVIZIO"))

            cmd.Connection = cn

            MV = False
            Dim myPOSTreader As OleDbDataReader = cmd.ExecuteReader()
            Do While myPOSTreader.Read
                Dim myriga As System.Data.DataRow = MyTable.NewRow()

                myriga(0) = campodb(myPOSTreader.Item("ELEMENTO"))
                myriga(1) = Val(campodb(myPOSTreader.Item("GIORNI")))
                myriga(2) = CDbl(campodb(myPOSTreader.Item("IMPORTO")))
                myriga(3) = campodb(myPOSTreader.Item("MANUALE"))
                myriga(4) = campodb(myPOSTreader.Item("CODICEEXTRA"))
                myriga(5) = campodb(myPOSTreader.Item("PROVINCIA"))
                myriga(6) = campodb(myPOSTreader.Item("COMUNE"))
                myriga(7) = Val(campodb(myPOSTreader.Item("ANNOCOMPETENZA")))
                myriga(8) = Val(campodb(myPOSTreader.Item("MESECOMPETENZA")))

                MyTable.Rows.Add(myriga)
                MV = True
            Loop
            myPOSTreader.Close()
        End If

        If DD_Tipo.SelectedValue = "R" Then
            Dim cmd As New OleDbCommand()
            cmd.CommandText = ("select * from RETTEREGIONE where " & _
                               "Anno = ? And Mese = ? And CODICEOSPITE = ? And CENTROSERVIZIO = ? ")
            cmd.Parameters.AddWithValue("@Anno", Txt_Anno.Text)
            cmd.Parameters.AddWithValue("@Mese", Dd_MeseDa.SelectedValue)
            cmd.Parameters.AddWithValue("@CodiceOspite", Session("CODICEOSPITE"))
            cmd.Parameters.AddWithValue("@CentroServizio", Session("CODICESERVIZIO"))

            cmd.Connection = cn

            MV = False
            Dim myPOSTreader As OleDbDataReader = cmd.ExecuteReader()
            Do While myPOSTreader.Read
                Dim myriga As System.Data.DataRow = MyTable.NewRow()

                myriga(0) = campodb(myPOSTreader.Item("ELEMENTO"))
                myriga(1) = Val(campodb(myPOSTreader.Item("GIORNI")))
                myriga(2) = CDbl(campodb(myPOSTreader.Item("IMPORTO")))
                myriga(3) = campodb(myPOSTreader.Item("MANUALE"))
                myriga(4) = campodb(myPOSTreader.Item("CODICEEXTRA"))
                myriga(5) = campodb(myPOSTreader.Item("REGIONE"))
                myriga(7) = 0
                myriga(8) = 0

                MyTable.Rows.Add(myriga)
                MV = True
            Loop
            myPOSTreader.Close()
        End If

        cn.Close()

        If MV = False Then
            Dim myriga As System.Data.DataRow = MyTable.NewRow()
            MyTable.Rows.Add(myriga)
        End If


        ViewState("App_Retta") = MyTable

        Grd_Retta.AutoGenerateColumns = False

        Grd_Retta.DataSource = MyTable

        Grd_Retta.DataBind()
        Call EseguiJS()


        REM Txt_Anno.Enabled = False
        REM Dd_MeseDa.Enabled = False
        REM DD_Tipo.Enabled = False

    End Sub

    Protected Sub Grd_Retta_RowCommand(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewCommandEventArgs) Handles Grd_Retta.RowCommand
        If (e.CommandName = "Inserisci") Then
            MyTable = ViewState("App_Retta")
            Dim myriga As System.Data.DataRow = MyTable.NewRow()

            MyTable.Rows.Add(myriga)
            ViewState("App_Retta") = MyTable

            Grd_Retta.AutoGenerateColumns = False

            Grd_Retta.DataSource = MyTable

            Grd_Retta.DataBind()
            Call EseguiJS()
        End If
    End Sub

    Private Sub UpDateTable()
        Dim i As Integer

        MyTable.Clear()
        MyTable.Columns.Add("Tipo", GetType(String))
        MyTable.Columns.Add("Giorni", GetType(String))
        MyTable.Columns.Add("Importo", GetType(String))
        MyTable.Columns.Add("Manuale", GetType(String))
        MyTable.Columns.Add("CODICEEXTRA", GetType(String))
        MyTable.Columns.Add("RIFERIMENTO", GetType(String))
        MyTable.Columns.Add("RIFERIMENTOb", GetType(String))
        MyTable.Columns.Add("AnnoCompetenza", GetType(String))
        MyTable.Columns.Add("MeseCompetenza", GetType(String))

        For i = 0 To Grd_Retta.Rows.Count - 1


            Dim TxtTipo As TextBox = DirectCast(Grd_Retta.Rows(i).FindControl("TxtTipo"), TextBox)
            Dim TxtGiorni As TextBox = DirectCast(Grd_Retta.Rows(i).FindControl("TxtGiorni"), TextBox)
            Dim TxtImporto As TextBox = DirectCast(Grd_Retta.Rows(i).FindControl("TxtImporto"), TextBox)
            Dim DD_Tipo As DropDownList = DirectCast(Grd_Retta.Rows(i).FindControl("DD_Tipo"), DropDownList)
            Dim TxtExtra As TextBox = DirectCast(Grd_Retta.Rows(i).FindControl("TxtExtra"), TextBox)
            Dim TxtRiferimento As TextBox = DirectCast(Grd_Retta.Rows(i).FindControl("TxtRiferimento"), TextBox)
            Dim TxtRiferimentob As TextBox = DirectCast(Grd_Retta.Rows(i).FindControl("TxtRiferimentob"), TextBox)
            Dim TxtAnno As TextBox = DirectCast(Grd_Retta.Rows(i).FindControl("TxtAnno"), TextBox)
            Dim TxtMese As TextBox = DirectCast(Grd_Retta.Rows(i).FindControl("TxtMese"), TextBox)
            Dim myrigaR As System.Data.DataRow = MyTable.NewRow()


            myrigaR(0) = TxtTipo.Text
            myrigaR(1) = TxtGiorni.Text
            myrigaR(2) = TxtImporto.Text
            myrigaR(3) = DD_Tipo.SelectedValue
            myrigaR(4) = TxtExtra.Text
            myrigaR(5) = TxtRiferimento.Text
            myrigaR(6) = TxtRiferimentob.Text
            myrigaR(7) = TxtAnno.Text
            myrigaR(8) = TxtMese.Text

            MyTable.Rows.Add(myrigaR)
        Next
        ViewState("App_Retta") = MyTable

    End Sub


    Protected Sub Grd_Retta_RowDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs) Handles Grd_Retta.RowDataBound
        If e.Row.RowType = DataControlRowType.DataRow Then


            Dim TxtTipo As TextBox = DirectCast(e.Row.FindControl("TxtTipo"), TextBox)

            TxtTipo.Text = MyTable.Rows(e.Row.RowIndex).Item(0).ToString

            Dim TxtGiorni As TextBox = DirectCast(e.Row.FindControl("TxtGiorni"), TextBox)

            TxtGiorni.Text = Val(MyTable.Rows(e.Row.RowIndex).Item(1).ToString)


            Dim TxtImporto As TextBox = DirectCast(e.Row.FindControl("TxtImporto"), TextBox)
            TxtImporto.Text = MyTable.Rows(e.Row.RowIndex).Item(2).ToString



            Dim DD_Tipo As DropDownList = DirectCast(e.Row.FindControl("DD_Tipo"), DropDownList)
            DD_Tipo.SelectedValue = MyTable.Rows(e.Row.RowIndex).Item(3).ToString


            Dim TxtExtra As TextBox = DirectCast(e.Row.FindControl("TxtExtra"), TextBox)
            TxtExtra.Text = MyTable.Rows(e.Row.RowIndex).Item(4).ToString

            Dim TxtRiferimento As TextBox = DirectCast(e.Row.FindControl("TxtRiferimento"), TextBox)
            TxtRiferimento.Text = MyTable.Rows(e.Row.RowIndex).Item(5).ToString


            Dim TxtRiferimentob As TextBox = DirectCast(e.Row.FindControl("TxtRiferimentob"), TextBox)
            TxtRiferimentob.Text = MyTable.Rows(e.Row.RowIndex).Item(6).ToString


            Dim TxtAnno As TextBox = DirectCast(e.Row.FindControl("TxtAnno"), TextBox)
            TxtAnno.Text = MyTable.Rows(e.Row.RowIndex).Item(7).ToString


            Dim TxtMese As TextBox = DirectCast(e.Row.FindControl("TxtMese"), TextBox)
            TxtMese.Text = MyTable.Rows(e.Row.RowIndex).Item(8).ToString

            Call EseguiJS()

        End If
    End Sub


    Private Sub EseguiJS()
        Dim MyJs As String
        MyJs = "$(document).ready(function() {"

        MyJs = MyJs & "var els = document.getElementsByTagName(""*"");"

        MyJs = MyJs & "for (var i=0;i<els.length;i++)"
        MyJs = MyJs & "if ( els[i].id ) { "
        MyJs = MyJs & " var appoggio =els[i].id; "
        MyJs = MyJs & " if (appoggio.match('TxtData')!= null) {  "
        MyJs = MyJs & " $(els[i]).mask(""99/99/9999"");"
        MyJs = MyJs & "    }"

        MyJs = MyJs & " if ((appoggio.match('TxtImporto')!= null) ) {  "
        MyJs = MyJs & " $(els[i]).keypress(function() { ForceNumericInput($(this).val(), true, true); } ); "
        MyJs = MyJs & " $(els[i]).blur(function() { var ap = formatNumber($(this).val(),2); $(this).val(ap); });"
        MyJs = MyJs & "    }"


        MyJs = MyJs & "} "
        MyJs = MyJs & "});"

        'MyJs = "$(document).ready(function() { $('.myClass').keypress(function() { handleEnter($('.myClass').val(), true, true); } );     $('#" & Txt_ImportoPagato.ClientID & "').blur(function() { var ap = formatNumber ($('#" & Txt_ImportoPagato.ClientID & "').val(),2); $('#" & Txt_ImportoPagato.ClientID & "').val(ap); }); });"
        ScriptManager.RegisterStartupScript(Me, Me.GetType(), "visualizzaRitIm", MyJs, True)
    End Sub

    Protected Sub Grd_Retta_RowDeleted(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewDeletedEventArgs) Handles Grd_Retta.RowDeleted

    End Sub


    Protected Sub Grd_Retta_RowDeleting(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewDeleteEventArgs) Handles Grd_Retta.RowDeleting

        UpDateTable()
        MyTable = ViewState("App_Retta")
        Try
            If Request.UserAgent.Contains("Chrome") Then
                If IsDBNull(Session("RI_TIMER")) Or DateDiff("s", Session("RI_TIMER"), Now) > 1 Then
                    MyTable.Rows.RemoveAt(e.RowIndex)
                    If MyTable.Rows.Count = 0 Then
                        Dim myriga As System.Data.DataRow = MyTable.NewRow()
                        myriga(1) = 0
                        myriga(2) = 0
                        MyTable.Rows.Add(myriga)
                    End If
                    Session("RI_TIMER") = Now
                End If
            Else
                MyTable.Rows.RemoveAt(e.RowIndex)
                If MyTable.Rows.Count = 0 Then
                    Dim myriga As System.Data.DataRow = MyTable.NewRow()
                    myriga(1) = 0
                    myriga(2) = 0
                    MyTable.Rows.Add(myriga)
                End If
            End If
        Catch ex As Exception

        End Try


        ViewState("App_Retta") = MyTable

        Grd_Retta.AutoGenerateColumns = False

        Grd_Retta.DataSource = MyTable

        Grd_Retta.DataBind()
        Call EseguiJS()

        e.Cancel = True
    End Sub

    Protected Sub Btn_Esci_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles Btn_Esci.Click
        If Request.Item("PAGINA") = "" Then
            Response.Redirect("RicercaAnagrafica.aspx?TIPO=MODIFICARETTA")
        Else
            Response.Redirect("RicercaAnagrafica.aspx?CHIAMATA=RITORNO&TIPO=MODIFICARETTA&PAGINA=" & Request.Item("PAGINA"))
        End If
    End Sub

    Protected Sub Btn_Modifica_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles Btn_Modifica.Click
        Dim cn As OleDbConnection
        Dim StringaConnessione As String = Session("DC_OSPITE")

        Call UpDateTable()

        MyTable = ViewState("App_Retta")
        cn = New Data.OleDb.OleDbConnection(StringaConnessione)

        cn.Open()

        If DD_Tipo.SelectedValue = "O" Then
            Dim cmdDel As New OleDbCommand()
            cmdDel.CommandText = ("DELETE RETTEOSPITE where " & _
                               "Anno = ? And Mese = ? And CODICEOSPITE = ? And CENTROSERVIZIO = ? ")
            cmdDel.Parameters.AddWithValue("@Anno", Txt_Anno.Text)
            cmdDel.Parameters.AddWithValue("@Mese", Dd_MeseDa.SelectedValue)
            cmdDel.Parameters.AddWithValue("@CodiceOspite", Session("CODICEOSPITE"))
            cmdDel.Parameters.AddWithValue("@CentroServizio", Session("CODICESERVIZIO"))

            cmdDel.Connection = cn
            cmdDel.ExecuteNonQuery()

            For i = 0 To MyTable.Rows.Count - 1
                Dim cmd1 As New OleDbCommand()
                cmd1.CommandText = ("INSERT INTO RETTEOSPITE (Anno,Mese,CODICEOSPITE,CENTROSERVIZIO,ELEMENTO,Giorni,Importo,Manuale,CODICEEXTRA,ANNOCOMPETENZA,MESECOMPETENZA) VALUES " & _
                                                              "(  ? ,  ? ,     ?      ,    ?,          ? ,    ?,     ?,     ?,      ?,          ?,             ?) ")
                cmd1.Parameters.AddWithValue("@Anno", Txt_Anno.Text)
                cmd1.Parameters.AddWithValue("@Mese", Dd_MeseDa.SelectedValue)
                cmd1.Parameters.AddWithValue("@CodiceOspite", Session("CODICEOSPITE"))
                cmd1.Parameters.AddWithValue("@CentroServizio", Session("CODICESERVIZIO"))
                cmd1.Parameters.AddWithValue("@Tipo", MyTable.Rows(i).Item(0))
                cmd1.Parameters.AddWithValue("@Giorni", MyTable.Rows(i).Item(1))
                cmd1.Parameters.AddWithValue("@Importo", CDbl(MyTable.Rows(i).Item(2)))
                cmd1.Parameters.AddWithValue("@Manuale", MyTable.Rows(i).Item(3))
                cmd1.Parameters.AddWithValue("@CODICEEXTRA", MyTable.Rows(i).Item(4))
                'cmd1.Parameters.AddWithValue("@RIFERIMENTO", MyTable.Rows(i).Item(5))
                'cmd1.Parameters.AddWithValue("@RIFERIMENTOB", MyTable.Rows(i).Item(6))
                cmd1.Parameters.AddWithValue("@ANNOCOMPETENZA", MyTable.Rows(i).Item(7))
                cmd1.Parameters.AddWithValue("@MESECOMPETENZA", MyTable.Rows(i).Item(8))
                cmd1.Connection = cn
                cmd1.ExecuteNonQuery()
            Next
        End If

        If DD_Tipo.SelectedValue = "P" Then
            Dim cmdDel As New OleDbCommand()
            cmdDel.CommandText = ("DELETE RETTEPARENTE where " & _
                               "Anno = ? And Mese = ? And CODICEOSPITE = ? And CENTROSERVIZIO = ? ")
            cmdDel.Parameters.AddWithValue("@Anno", Txt_Anno.Text)
            cmdDel.Parameters.AddWithValue("@Mese", Dd_MeseDa.SelectedValue)
            cmdDel.Parameters.AddWithValue("@CodiceOspite", Session("CODICEOSPITE"))
            cmdDel.Parameters.AddWithValue("@CentroServizio", Session("CODICESERVIZIO"))

            cmdDel.Connection = cn
            cmdDel.ExecuteNonQuery()

            For i = 0 To MyTable.Rows.Count - 1
                Dim cmd1 As New OleDbCommand()
                cmd1.CommandText = ("INSERT INTO RETTEPARENTE (Anno,Mese,CODICEOSPITE,CENTROSERVIZIO,ELEMENTO,Giorni,Importo,Manuale,CODICEEXTRA,CODICEPARENTE,ANNOCOMPETENZA,MESECOMPETENZA) VALUES " & _
                                                              "(  ? ,  ? ,     ?      ,    ?,          ? ,    ?,     ?,     ?,      ?,           ?,             ?,             ?) ")                                                                  
                cmd1.Parameters.AddWithValue("@Anno", Txt_Anno.Text)
                cmd1.Parameters.AddWithValue("@Mese", Dd_MeseDa.SelectedValue)
                cmd1.Parameters.AddWithValue("@CodiceOspite", Session("CODICEOSPITE"))
                cmd1.Parameters.AddWithValue("@CentroServizio", Session("CODICESERVIZIO"))
                cmd1.Parameters.AddWithValue("@ELEMENTO", MyTable.Rows(i).Item(0))
                cmd1.Parameters.AddWithValue("@Giorni", MyTable.Rows(i).Item(1))
                cmd1.Parameters.AddWithValue("@Importo", CDbl(MyTable.Rows(i).Item(2)))
                cmd1.Parameters.AddWithValue("@Manuale", MyTable.Rows(i).Item(3))
                cmd1.Parameters.AddWithValue("@CODICEEXTRA", MyTable.Rows(i).Item(4))
                cmd1.Parameters.AddWithValue("@CODICEPARENTE", MyTable.Rows(i).Item(5))
                'cmd1.Parameters.AddWithValue("@RIFERIMENTOB", MyTable.Rows(i).Item(6))
                cmd1.Parameters.AddWithValue("@ANNOCOMPETENZA", MyTable.Rows(i).Item(7))
                cmd1.Parameters.AddWithValue("@MESECOMPETENZA", MyTable.Rows(i).Item(8))
                cmd1.Connection = cn
                cmd1.ExecuteNonQuery()
            Next
        End If


        If DD_Tipo.SelectedValue = "C" Then
            Dim cmdDel As New OleDbCommand()
            cmdDel.CommandText = ("DELETE RETTECOMUNE where " & _
                               "Anno = ? And Mese = ? And CODICEOSPITE = ? And CENTROSERVIZIO = ? ")
            cmdDel.Parameters.AddWithValue("@Anno", Txt_Anno.Text)
            cmdDel.Parameters.AddWithValue("@Mese", Dd_MeseDa.SelectedValue)
            cmdDel.Parameters.AddWithValue("@CodiceOspite", Session("CODICEOSPITE"))
            cmdDel.Parameters.AddWithValue("@CentroServizio", Session("CODICESERVIZIO"))

            cmdDel.Connection = cn
            cmdDel.ExecuteNonQuery()

            For i = 0 To MyTable.Rows.Count - 1
                Dim cmd1 As New OleDbCommand()
                cmd1.CommandText = ("INSERT INTO RETTECOMUNE (Anno,Mese,CODICEOSPITE,CENTROSERVIZIO,ELEMENTO,Giorni,Importo,Manuale,CODICEEXTRA,[PROVINCIA],[COMUNE],ANNOCOMPETENZA,MESECOMPETENZA) VALUES " & _
                                                              "(?,?,?,?,?,?,?,?,?,?,?,?,?) ")
                cmd1.Parameters.AddWithValue("@Anno", Txt_Anno.Text)
                cmd1.Parameters.AddWithValue("@Mese", Dd_MeseDa.SelectedValue)
                cmd1.Parameters.AddWithValue("@CodiceOspite", Session("CODICEOSPITE"))
                cmd1.Parameters.AddWithValue("@CentroServizio", Session("CODICESERVIZIO"))
                cmd1.Parameters.AddWithValue("@Tipo", MyTable.Rows(i).Item(0))
                cmd1.Parameters.AddWithValue("@Giorni", MyTable.Rows(i).Item(1))
                cmd1.Parameters.AddWithValue("@Importo", CDbl(MyTable.Rows(i).Item(2)))
                cmd1.Parameters.AddWithValue("@Manuale", MyTable.Rows(i).Item(3))
                cmd1.Parameters.AddWithValue("@CODICEEXTRA", MyTable.Rows(i).Item(4))
                cmd1.Parameters.AddWithValue("@RIFERIMENTO", MyTable.Rows(i).Item(5))
                cmd1.Parameters.AddWithValue("@RIFERIMENTOB", MyTable.Rows(i).Item(6))
                cmd1.Parameters.AddWithValue("@ANNOCOMPETENZA", MyTable.Rows(i).Item(7))
                cmd1.Parameters.AddWithValue("@MESECOMPETENZA", MyTable.Rows(i).Item(8))
                cmd1.Connection = cn
                cmd1.ExecuteNonQuery()
            Next
        End If

        If DD_Tipo.SelectedValue = "J" Then
            Dim cmdDel As New OleDbCommand()
            cmdDel.CommandText = ("DELETE RETTEJOLLY where " & _
                               "Anno = ? And Mese = ? And CODICEOSPITE = ? And CENTROSERVIZIO = ? ")
            cmdDel.Parameters.AddWithValue("@Anno", Txt_Anno.Text)
            cmdDel.Parameters.AddWithValue("@Mese", Dd_MeseDa.SelectedValue)
            cmdDel.Parameters.AddWithValue("@CodiceOspite", Session("CODICEOSPITE"))
            cmdDel.Parameters.AddWithValue("@CentroServizio", Session("CODICESERVIZIO"))

            cmdDel.Connection = cn
            cmdDel.ExecuteNonQuery()

            For i = 0 To MyTable.Rows.Count - 1
                Dim cmd1 As New OleDbCommand()
                cmd1.CommandText = ("INSERT INTO RETTEJOLLY (Anno,Mese,CODICEOSPITE,CENTROSERVIZIO,ELEMENTO,Giorni,Importo,Manuale,CODICEEXTRA,[PROVINCIA],[COMUNE],ANNOCOMPETENZA,MESECOMPETENZA) VALUES " & _
                                              "(?,?,?,?,?,?,?,?,?,?,?,?,?) ")
                cmd1.Parameters.AddWithValue("@Anno", Txt_Anno.Text)
                cmd1.Parameters.AddWithValue("@Mese", Dd_MeseDa.SelectedValue)
                cmd1.Parameters.AddWithValue("@CodiceOspite", Session("CODICEOSPITE"))
                cmd1.Parameters.AddWithValue("@CentroServizio", Session("CODICESERVIZIO"))
                cmd1.Parameters.AddWithValue("@Tipo", MyTable.Rows(i).Item(0))
                cmd1.Parameters.AddWithValue("@Giorni", MyTable.Rows(i).Item(1))
                cmd1.Parameters.AddWithValue("@Importo", CDbl(MyTable.Rows(i).Item(2)))
                cmd1.Parameters.AddWithValue("@Manuale", MyTable.Rows(i).Item(3))
                cmd1.Parameters.AddWithValue("@CODICEEXTRA", MyTable.Rows(i).Item(4))
                cmd1.Parameters.AddWithValue("@RIFERIMENTO", MyTable.Rows(i).Item(5))
                cmd1.Parameters.AddWithValue("@RIFERIMENTOB", MyTable.Rows(i).Item(6))
                cmd1.Parameters.AddWithValue("@ANNOCOMPETENZA", MyTable.Rows(i).Item(7))
                cmd1.Parameters.AddWithValue("@MESECOMPETENZA", MyTable.Rows(i).Item(8))
                cmd1.Connection = cn
                cmd1.ExecuteNonQuery()
            Next
        End If


        If DD_Tipo.SelectedValue = "R" Then
            Dim cmdDel As New OleDbCommand()
            cmdDel.CommandText = ("DELETE RETTEREGIONE where " & _
                               "Anno = ? And Mese = ? And CODICEOSPITE = ? And CENTROSERVIZIO = ? ")
            cmdDel.Parameters.AddWithValue("@Anno", Txt_Anno.Text)
            cmdDel.Parameters.AddWithValue("@Mese", Dd_MeseDa.SelectedValue)
            cmdDel.Parameters.AddWithValue("@CodiceOspite", Session("CODICEOSPITE"))
            cmdDel.Parameters.AddWithValue("@CentroServizio", Session("CODICESERVIZIO"))

            cmdDel.Connection = cn
            cmdDel.ExecuteNonQuery()

            For i = 0 To MyTable.Rows.Count - 1
                Dim cmd1 As New OleDbCommand()
                cmd1.CommandText = ("INSERT INTO RETTEREGIONE (Anno,Mese,CODICEOSPITE,CENTROSERVIZIO,ELEMENTO,Giorni,Importo,Manuale,CODICEEXTRA,[REGIONE]) VALUES " & _
                                              "(?,?,?,?,?,?,?,?,?,?) ")
                cmd1.Parameters.AddWithValue("@Anno", Txt_Anno.Text)
                cmd1.Parameters.AddWithValue("@Mese", Dd_MeseDa.SelectedValue)
                cmd1.Parameters.AddWithValue("@CodiceOspite", Session("CODICEOSPITE"))
                cmd1.Parameters.AddWithValue("@CentroServizio", Session("CODICESERVIZIO"))
                cmd1.Parameters.AddWithValue("@Tipo", MyTable.Rows(i).Item(0))
                cmd1.Parameters.AddWithValue("@Giorni", MyTable.Rows(i).Item(1))
                cmd1.Parameters.AddWithValue("@Importo", CDbl(MyTable.Rows(i).Item(2)))
                cmd1.Parameters.AddWithValue("@Manuale", MyTable.Rows(i).Item(3))
                cmd1.Parameters.AddWithValue("@CODICEEXTRA", MyTable.Rows(i).Item(4))
                cmd1.Parameters.AddWithValue("@RIFERIMENTO", MyTable.Rows(i).Item(5))
                'cmd1.Parameters.AddWithValue("@RIFERIMENTOB", MyTable.Rows(i).Item(6))
                'cmd1.Parameters.AddWithValue("@ANNOCOMPETENZA", MyTable.Rows(i).Item(7))
                'cmd1.Parameters.AddWithValue("@MESECOMPETENZA", MyTable.Rows(i).Item(8))
                cmd1.Connection = cn
                cmd1.ExecuteNonQuery()
            Next
        End If
        cn.Close()

        MyTable.Clear()
        MyTable.Columns.Clear()
        MyTable.Columns.Add("Tipo", GetType(String))
        MyTable.Columns.Add("Giorni", GetType(String))
        MyTable.Columns.Add("Importo", GetType(String))
        MyTable.Columns.Add("Manuale", GetType(String))
        MyTable.Columns.Add("CODICEEXTRA", GetType(String))
        MyTable.Columns.Add("RIFERIMENTO", GetType(String))
        MyTable.Columns.Add("RIFERIMENTOb", GetType(String))
        MyTable.Columns.Add("AnnoCompetenza", GetType(String))
        MyTable.Columns.Add("MeseCompetenza", GetType(String))



        ViewState("App_Retta") = MyTable

        Grd_Retta.AutoGenerateColumns = False

        Grd_Retta.DataSource = MyTable

        Grd_Retta.DataBind()
        Call EseguiJS()


    End Sub
End Class

