﻿<%@ Page Language="VB" AutoEventWireup="false" Inherits="OspitiWeb_Export_Documenti" CodeFile="Export_Documenti.aspx.vb" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="ajtsauro" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
    <meta http-equiv="x-ua-compatible" content="IE=9" />
    <title>Export Documenti</title>
    <asp:PlaceHolder runat="server">
        <%: System.Web.Optimization.Styles.Render("~/Content/AjaxControlToolkit/Styles/Bundle") %>
    </asp:PlaceHolder>
    <link href="ospiti.css?ver=11" rel="stylesheet" type="text/css" />
    <link rel="shortcut icon" href="images/SENIOR.ico" />
    <link href="js/jquery.autocomplete.css" rel="stylesheet" type="text/css" />


    <script src="js/jquery-1.5.1.min.js" type="text/javascript"></script>
    <script src="js/jquery.autocomplete.js" type="text/javascript"></script>
    <script src="js/soapclient.js" type="text/javascript"></script>
    <script src="/js/convertinumero.js" type="text/javascript"></script>

    <link rel="stylesheet" href="dialogbox/redips-dialog.css" type="text/css" media="screen" />
    <script type="text/javascript" src="dialogbox/redips-dialog-min.js"></script>

    <script src="js/jquery.maskedinput-1.3.min.js" type="text/javascript"></script>
    <script src="/js/formatnumer.js" type="text/javascript"></script>
    <script src="js/JSErrore.js" type="text/javascript"></script>
    <script src="js/NoEnter.js" type="text/javascript"></script>
    <script type="text/javascript" src="../js/menuanagraficaospiti.js?ver=7"></script>

    <link rel="stylesheet" href="dialogbox/redips-dialog.css" type="text/css" media="screen" />
    <script type="text/javascript" src="dialogbox/redips-dialog-min.js"></script>
    <script type="text/javascript" src="dialogbox/script.js"></script>

    <link rel="stylesheet" href="js/chosen/docsupport/style.css">
    <link rel="stylesheet" href="js/chosen/docsupport/prism.css">
    <link rel="stylesheet" href="js/chosen/chosen.css">

    <link rel="stylesheet" href="jqueryui/jquery-ui.css" type="text/css">
    <script src="jqueryui/jquery-ui.js" type="text/javascript"></script>
    <script src="jqueryui/datapicker-it.js" type="text/javascript"></script>


    <script src="js/chosen/chosen.jquery.js" type="text/javascript"></script>
    <script src="js/chosen/docsupport/prism.js" type="text/javascript" charset="utf-8"></script>


    <script type="text/javascript">  
        $(document).ready(function () {
            if (window.innerHeight > 0) { $("#BarraLaterale").css("height", (window.innerHeight - 94) + "px"); } else { $("#BarraLaterale").css("height", (document.documentElement.offsetHeight - 94) + "px"); }
        });
    </script>
    <style>
        .wait {
            border: solid 1px #2d2d2d;
            text-align: center;
            vertical-align: top;
            background: white;
            width: 20%;
            height: 180px;
            top: 30%;
            left: 40%;
            position: absolute;
            -moz-border-radius: 5px;
            -webkit-border-radius: 5px;
            border-radius: 5px;
            box-shadow: 0px 0px 10px 4px rgba(0, 125, 196, 0.75);
            -moz-box-shadow: 0px 0px 10px 4px rgba(0, 125, 196, 0.75);
            -webkit-box-shadow: 0px 0px 10px 4px rgba(0, 125, 196, 0.75);
        }

        #blur {
            width: 100%;
            background-color: black;
            moz-opacity: 0.5;
            khtml-opacity: .5;
            opacity: .5;
            filter: alpha(opacity=50);
            z-index: 120;
            height: 100%;
            position: absolute;
            top: 0;
            left: 0;
        }

        #progress {
            z-index: 200;
            background-color: White;
            position: absolute;
            top: 0pt;
            left: 0pt;
            border: solid 1px black;
            padding: 5px 5px 5px 5px;
            text-align: center;
        }
    </style>

</head>
<body>
    <form id="form1" runat="server">
        <asp:ScriptManager ID="ScriptManager1" runat="server" EnableScriptGlobalization="true">
            <Scripts>
                <asp:ScriptReference Path="~/Scripts/AjaxControlToolkit/Bundle" />
            </Scripts>
        </asp:ScriptManager>
        <asp:Label ID="Lbl_BarraSenior" runat="server" Text=""></asp:Label>
        <div align="left">
            <table style="width: 100%;" cellpadding="0" cellspacing="0">
                <tr>
                    <td style="width: 160px; background-color: #F0F0F0;"></td>
                    <td>
                        <div class="Titolo">Ospiti - Export - Export Documenti</div>
                        <div class="SottoTitolo">
                            <asp:Label ID="Lbl_NomeOspite" runat="server"></asp:Label><br />
                            <br />
                        </div>
                    </td>
                    <td style="text-align: right;">
                        <div class="DivTasti">
                            <asp:ImageButton runat="server" ImageUrl="~/images/elabora.png" class="EffettoBottoniTondi"
                                Height="38px" ToolTip="Esegui" ID="Btn_Modifica"></asp:ImageButton>
                        </div>
                    </td>
                </tr>

                <tr>
                    <td style="width: 160px; background-color: #F0F0F0; vertical-align: top; text-align: center;" id="BarraLaterale">
                        <asp:ImageButton ID="Btn_Esci" runat="server" BackColor="Transparent" ImageUrl="../images/Menu_Indietro.png" ToolTip="Chiudi" />
                        <br />


                        <br />
                        <br />
                        <br />
                        <br />
                        <br />
                        <br />
                        <br />
                        <br />
                        <br />
                        <br />
                        <br />
                        <br />
                    </td>
                    <td colspan="2" style="background-color: #FFFFFF;" valign="top">
                        <ajtsauro:TabContainer ID="TabContainer1" runat="server" ActiveTabIndex="0" CssClass="TabSenior" Width="100%" BorderStyle="None" Style="margin-right: 39px">
                            <ajtsauro:TabPanel runat="server" HeaderText="Prima Nota" ID="Tab_Anagrafica">
                                <HeaderTemplate>
                                    Export Documenti
                                </HeaderTemplate>
                                <ContentTemplate>

                                    <ajtsauro:TabContainer ID="Tab_PeriodoData" runat="server" ActiveTabIndex="0"
                                        CssClass="TabSenior" Width="300px" Height="120px" BorderStyle="None"
                                        Style="margin-right: 39px">
                                        <ajtsauro:TabPanel runat="server" HeaderText="Prima Nota" ID="TabPanel1">
                                            <HeaderTemplate>
                                                Periodo
                                            </HeaderTemplate>
                                            <ContentTemplate>


                                                <label class="LabelCampo">Anno :</label>
                                                &nbsp;&nbsp;&nbsp;<asp:TextBox ID="Txt_AnnoRif" MaxLength="4" onkeypress="return soloNumeri(event);" runat="server" Width="64px"></asp:TextBox>
                                                (Anno Contabile)
 <br />
                                                <br />

                                                <label class="LabelCampo">Anno :</label>
                                                &nbsp;&nbsp;&nbsp;<asp:TextBox ID="Txt_Anno" MaxLength="4" onkeypress="return soloNumeri(event);" runat="server" Width="64px"></asp:TextBox>
                                                <br />
                                                <br />


                                                <label class="LabelCampo">Mese :</label>
                                                &nbsp;&nbsp;&nbsp;<asp:DropDownList ID="DD_Mese" runat="server" Width="128px">
                                                    <asp:ListItem Value="1">Gennaio</asp:ListItem>
                                                    <asp:ListItem Value="2">Febbraio</asp:ListItem>
                                                    <asp:ListItem Value="3">Marzo</asp:ListItem>
                                                    <asp:ListItem Value="4">Aprile</asp:ListItem>
                                                    <asp:ListItem Value="5">Maggio</asp:ListItem>
                                                    <asp:ListItem Value="6">Giugno</asp:ListItem>
                                                    <asp:ListItem Value="7">Luglio</asp:ListItem>
                                                    <asp:ListItem Value="8">Agosto</asp:ListItem>
                                                    <asp:ListItem Value="9">Settembre</asp:ListItem>
                                                    <asp:ListItem Value="10">Ottobre</asp:ListItem>
                                                    <asp:ListItem Value="11">Novembre</asp:ListItem>
                                                    <asp:ListItem Value="12">Dicembre</asp:ListItem>
                                                </asp:DropDownList>
                                                <br />
                                                <br />
                                            </ContentTemplate>
                                        </ajtsauro:TabPanel>
                                        <ajtsauro:TabPanel runat="server" HeaderText="Prima Nota" ID="TabPanel2">
                                            <HeaderTemplate>
                                                Data
                                            </HeaderTemplate>
                                            <ContentTemplate>

                                                <label class="LabelCampo">Dal num documento:</label>
                                                &nbsp;&nbsp;&nbsp;<asp:TextBox ID="Txt_DataDal1" runat="server" Width="84px"></asp:TextBox>
                                                <br />
                                                <br />

                                                <label class="LabelCampo">Al num documento:</label>
                                                &nbsp;&nbsp;&nbsp;<asp:TextBox ID="Txt_DataAl1" runat="server" Width="84px"></asp:TextBox>
                                                <br />
                                                <br />
                                            </ContentTemplate>
                                        </ajtsauro:TabPanel>
                                    </ajtsauro:TabContainer>

                                    <label class="LabelCampo">Registro :</label>
                                    &nbsp;&nbsp;<asp:DropDownList ID="DD_Registro" OnTextChanged="DD_Registro_TextChanged" AutoPostBack="True" runat="server"></asp:DropDownList>
                                    <asp:ImageButton src="../images/update.png" runat="server" Width="16px" ID="ImgUpDate" />
                                    <br />
                                    <br />

                                    <label class="LabelCampo">Dal num documento:</label>
                                    &nbsp;&nbsp;<asp:TextBox ID="Txt_DalDocumento" onkeypress="return soloNumeri(event);" runat="server" Width="64px"></asp:TextBox>
                                    <br />
                                    <br />

                                    <label class="LabelCampo">Al num documento:</label>
                                    &nbsp;&nbsp;<asp:TextBox ID="Txt_AlDocumento" onkeypress="return soloNumeri(event);" runat="server" Width="64px"></asp:TextBox>
                                    <br />
                                    <asp:Button ID="Btn_Testa" runat="server" Text="TESTA" Visible="False" />
                                    <asp:Button ID="Btn_Riga" runat="server" Text="RIGA" Visible="False" />
                                    <br />
                                    <br />
                                    <asp:RadioButton ID="RB_AlianteNew" Text="ALYANTE NEW" runat="server" GroupName="TIPOSTAMPA" /><br />
                                    <asp:RadioButton ID="RB_Aliante" Text="ALYANTE" runat="server" GroupName="TIPOSTAMPA" /><br />
                                    <asp:RadioButton ID="RB_SEAC" Text="Seac" runat="server" GroupName="TIPOSTAMPA" /><br />
                                    <asp:RadioButton ID="RB_Multi" Text="Multi" runat="server" GroupName="TIPOSTAMPA" /><br />
                                    <asp:RadioButton ID="RB_Gamma" Text="Gamma" runat="server" GroupName="TIPOSTAMPA" /><br />
                                    <asp:RadioButton ID="RB_AdHoc" Text="AdHoc" runat="server" GroupName="TIPOSTAMPA" /><br />
                                    <asp:RadioButton ID="RB_IPSOA" Text="IpSoa" runat="server" GroupName="TIPOSTAMPA" /><br />
                                    <asp:RadioButton ID="RB_PassePartout" Text="PassePartout" runat="server" GroupName="TIPOSTAMPA" /><br />
                                    <asp:RadioButton ID="RB_Ats" Text="ADS SYSTEMATICA" runat="server" GroupName="TIPOSTAMPA" /><br />
                                    <asp:RadioButton ID="RB_DocumentiIncassi" Text="Documenti Incassi" runat="server" GroupName="TIPOSTAMPA" /><br />
                                    <asp:RadioButton ID="RB_POLIFARMA" Text="Polifamra" runat="server" GroupName="TIPOSTAMPA" /><br />
                                    <asp:RadioButton ID="RB_Zucchetti" Text="Zucchetti" runat="server" GroupName="TIPOSTAMPA" /><br />
                                    <br />
                                    <br />
                                    <asp:Label ID="Lbl_Errori" runat="server"></asp:Label>
                                    <asp:GridView ID="GridView1" runat="server" CellPadding="3"
                                        Width="100%" BackColor="White" BorderColor="#999999" BorderStyle="None"
                                        BorderWidth="1px" GridLines="Vertical">
                                        <RowStyle ForeColor="#565151" BackColor="#EEEEEE" />
                                        <AlternatingRowStyle BackColor="Gainsboro" />
                                        <FooterStyle BackColor="#CCCCCC" ForeColor="Black" />
                                        <PagerStyle BackColor="#999999" ForeColor="Black" HorizontalAlign="Center" />
                                        <SelectedRowStyle BackColor="#008A8C" Font-Bold="True" ForeColor="White" />
                                        <HeaderStyle BackColor="#565151" Font-Bold="False" Font-Size="Small"
                                            ForeColor="White" />
                                    </asp:GridView>
                                    <br />
                                    <div id="msg"></div>

                                    <asp:UpdatePanel ID="UpdatePanel2" runat="server">
                                        <ContentTemplate>
                                            <asp:Timer ID="Timer1" runat="server" Interval="1000">
                                            </asp:Timer>
                                            <asp:Label ID="Lbl_Waiting" runat="server" Text="Label"></asp:Label>

                                        </ContentTemplate>
                                    </asp:UpdatePanel>
                                </ContentTemplate>
                            </ajtsauro:TabPanel>
                            <ajtsauro:TabPanel runat="server" HeaderText="PassPartout" ID="TabPassPartout">
                                <HeaderTemplate>
                                    Export Documenti 
                                </HeaderTemplate>
                                <ContentTemplate>


                                    <label class="LabelCampo">Registro :</label>
                                    &nbsp;&nbsp;<asp:DropDownList ID="Dd_RegistroDATA" runat="server"></asp:DropDownList>
                                    <br />
                                    <br />


                                    <label class="LabelCampo">Data Dal Documento :</label>
                                    &nbsp;&nbsp;<asp:TextBox ID="Txt_DataDal" runat="server" Width="84px"></asp:TextBox>
                                    <br />
                                    <br />

                                    <label class="LabelCampo">Data Al Documento :</label>
                                    &nbsp;&nbsp;<asp:TextBox ID="Txt_DataAl" runat="server" Width="84px"></asp:TextBox>
                                    <br />
                                    <br />


                                    <label class="LabelCampo">
                                        <asp:Label ID="lbl_prova" runat="server" Text="Prova"></asp:Label></label>
                                    <asp:CheckBox ID="Chk_Prova" Text="" runat="server" Width="84px"></asp:CheckBox>
                                    <br />
                                    <br />

                                    <asp:GridView ID="GridView2" runat="server" CellPadding="3"
                                        Width="100%" BackColor="White" BorderColor="#999999" BorderStyle="None"
                                        BorderWidth="1px" GridLines="Vertical">
                                        <RowStyle ForeColor="#565151" BackColor="#EEEEEE" />
                                        <AlternatingRowStyle BackColor="Gainsboro" />
                                        <FooterStyle BackColor="#CCCCCC" ForeColor="Black" />
                                        <PagerStyle BackColor="#999999" ForeColor="Black" HorizontalAlign="Center" />
                                        <SelectedRowStyle BackColor="#008A8C" Font-Bold="True" ForeColor="White" />
                                        <HeaderStyle BackColor="#565151" Font-Bold="False" Font-Size="Small"
                                            ForeColor="White" />
                                    </asp:GridView>
                                    <br />
                                    <asp:Button ID="Btn_Testa2" runat="server" Text="TESTA" Visible="false" />
                                    <asp:Button ID="Btn_Riga2" runat="server" Text="RIGA" Visible="false" />



                                </ContentTemplate>
                            </ajtsauro:TabPanel>
                        </ajtsauro:TabContainer>
                    </td>
                </tr>
            </table>
        </div>
    </form>
</body>
</html>
