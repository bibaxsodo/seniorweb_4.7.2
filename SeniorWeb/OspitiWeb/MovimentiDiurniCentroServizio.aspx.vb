﻿Imports System.Web.Hosting
Imports System.Data.OleDb
Partial Class OspitiWeb_MovimentiDiurniCentroServizio
    Inherits System.Web.UI.Page
    Dim MyTable As New System.Data.DataTable("tabella")
    Dim MyDataSet As New System.Data.DataSet()

    Function campodb(ByVal oggetto As Object) As String
        If IsDBNull(oggetto) Then
            Return ""
        Else
            Return oggetto
        End If
    End Function
    Public Function GiorniMese(ByVal Mese As Byte, ByVal Anno As Integer) As Byte
        If Mese = 1 Or Mese = 3 Or Mese = 5 Or Mese = 7 Or Mese = 8 Or _
           Mese = 10 Or Mese = 12 Then
            GiorniMese = 31
        Else
            If Mese <> 2 Then
                GiorniMese = 30
            Else
                If Day(DateSerial(Anno, Mese, 29)) = 29 Then
                    GiorniMese = 29
                Else
                    GiorniMese = 28
                End If
            End If
        End If
    End Function
    Private Sub ProspettoPresenze()
        Dim ConnectionString As String = Session("DC_OSPITE")
        Dim cn As OleDbConnection
        Dim MySql As String
        Dim Formula As String = ""
        Dim i As Integer

        cn = New Data.OleDb.OleDbConnection(ConnectionString)

        cn.Open()

        
        Dim cmdCau As New OleDbCommand()
        Dim VettoreCodici(100) As String

        Dim Causali As String = ""

        cmdCau.CommandText = ("select * from Causali where Diurno >= 1 and Diurno <= 24  ORDER BY Codice,Descrizione")

        cmdCau.Connection = cn
        Dim RigaDescrizione As String = "<td style=""width:100px; border-width:1px; border-color:black; border-style:solid; text-align: center; font-size:small;""  >Presente</td>"
        Lbl_Schema.Text = "<table>"
        Lbl_Schema.Text = Lbl_Schema.Text & "<tr>"
        Lbl_Schema.Text = Lbl_Schema.Text & "<td style=""width:100px; border-width:1px; border-color:black; border-style:solid; text-align: center;font-size:small;"" id=""Colore0"" onclick=""selezionacolore(0);""  ></td>"
        Dim RsCaus As OleDbDataReader = cmdCau.ExecuteReader()
        Do While RsCaus.Read
            Lbl_Schema.Text = Lbl_Schema.Text & "<td style=""width:100px; border-width:1px; border-color:black; border-style:solid; text-align: center;font-size:small;"" id=""Colore" & Val(RsCaus.Item("Diurno")) & """ ><label style='position: relative; top:-5px; left:-15px;'>" & RsCaus.Item("Codice") & "</label><img style='Width:20px; Height:20px;' src=""images\Diurno_" & Val(RsCaus.Item("Diurno")) & ".png""  onclick=""selezionacolore(" & Val(RsCaus.Item("Diurno")) & ");""  /></td>"
            RigaDescrizione = RigaDescrizione & "<td style=""width:100px; border-width:1px; border-color:black; border-style:solid; font-size:small;""  ><span class=""rotate"">" & RsCaus.Item("Descrizione") & "</span></td>"
        Loop
        RsCaus.Close()

        Lbl_Schema.Text = Lbl_Schema.Text & "</tr>"
        Lbl_Schema.Text = Lbl_Schema.Text & "<tr>" & RigaDescrizione & "</tr>"

        Lbl_Schema.Text = Lbl_Schema.Text & "</table>"

        RigaDescrizione = "<td style=""width:100px; border-width:1px; border-color:black; border-style:solid; text-align: center;font-size:small;"">Icone</td>"
        Dim cmdCauTr As New OleDbCommand()

        cmdCauTr.CommandText = ("select * from Causali where Diurno >= 25 ORDER BY Codice,Descrizione")

        cmdCauTr.Connection = cn

        Lbl_Schema.Text = Lbl_Schema.Text & "<table>"
        Lbl_Schema.Text = Lbl_Schema.Text & "<tr>"
        Lbl_Schema.Text = Lbl_Schema.Text & "<td style=""width:100px; border-width:1px; border-color:black; border-style:solid; text-align: center;""></td>"
        Dim RsCausTr As OleDbDataReader = cmdCauTr.ExecuteReader()
        Do While RsCausTr.Read
            Lbl_Schema.Text = Lbl_Schema.Text & "<td style=""width:100px; border-width:1px; border-color:black; border-style:solid; text-align: center;"" id=""Colore" & Val(RsCausTr.Item("Diurno")) & """ ><label style='position: relative; top:-5px; left:-15px;font-size:small;'>" & RsCausTr.Item("Codice") & "</label><img src=""images\Diurno_" & Val(RsCausTr.Item("Diurno")) & ".png"" style='Width:20px; Height:20px;'  onclick=""selezionacolore(" & Val(RsCausTr.Item("Diurno")) & ");""  /></td>"
            RigaDescrizione = RigaDescrizione & "<td style=""width:100px; border-width:1px; border-color:black; border-style:solid;font-size:small; ""  ><span class=""rotate"" >" & RsCausTr.Item("Descrizione") & "</span></td>"

        Loop
        RsCausTr.Close()

        Lbl_Schema.Text = Lbl_Schema.Text & "</tr>"
        Lbl_Schema.Text = Lbl_Schema.Text & "<tr>" & RigaDescrizione & "</tr>"

        Lbl_Schema.Text = Lbl_Schema.Text & "</table>"



        Lbl_Schema.Text = Lbl_Schema.Text & "<br/>"
        Lbl_Schema.Text = Lbl_Schema.Text & "<table>"
        Lbl_Schema.Text = Lbl_Schema.Text & "<tr>"
        Lbl_Schema.Text = Lbl_Schema.Text & "<td style=""width:50px;font-size:small;"">Codice</td>"
        Lbl_Schema.Text = Lbl_Schema.Text & "<td style=""width:200px;font-size:small;"">Nome</td>"
        Lbl_Schema.Text = Lbl_Schema.Text & "<td style=""width:80px;font-size:small;"">Data Nascita</td>"
        For i = Val(Txt_Dal.Text) To Val(Txt_Al.Text)
            Dim DataVerifica As Date

            DataVerifica = DateSerial(Val(Txt_Anno.Text), Dd_Mese.SelectedValue, i)
            If DataVerifica.DayOfWeek = DayOfWeek.Saturday Or DataVerifica.DayOfWeek = DayOfWeek.Sunday Then
                Lbl_Schema.Text = Lbl_Schema.Text & "<td style=""color:red;width:30px;text-align: center;"">" & i & "</td>"
            Else
                Lbl_Schema.Text = Lbl_Schema.Text & "<td style=""width:30px;text-align: center;"">" & i & "</td>"
            End If
        Next

        Lbl_Schema.Text = Lbl_Schema.Text & "</tr>"
        Dim cmd As New OleDbCommand()


        Dim Riga As Integer = 0

        MySql = "Select * From AnagraficaComune Where CodiceParente = 0 And CodiceOspite > 0 And (NonInUso Is Null Or NonInUso = '')  And ( (Select count(*) From Movimenti Where CodiceOspite = AnagraficaComune.CodiceOspite And Data >= ? And Data <= ? And CENTROSERVIZIO = '" & Cmb_CServ.SelectedValue & "') > 0 OR " & _
                                                        " (" & _
                                                        " ((Select Top 1 TipoMov From Movimenti Where CodiceOspite = AnagraficaComune.CodiceOspite And Data < ? And CENTROSERVIZIO = '" & Cmb_CServ.SelectedValue & "' Order By Data DESC,Progressivo) <> '13') " & _
                                                        " And " & _
                                                        " ((Select  count(*) From Movimenti Where CodiceOspite = AnagraficaComune.CodiceOspite And Data < ? And CENTROSERVIZIO = '" & Cmb_CServ.SelectedValue & "' And TipoMov = '05') <>  0) " & _
                                                        " )" & _
                                                        " ) order by Nome"


        cmd.CommandText = MySql
        Dim Txt_DataDalText As Date = DateSerial(Val(Txt_Anno.Text), Dd_Mese.SelectedValue, 1)
        Dim Txt_DataAlText As Date = DateSerial(Val(Txt_Anno.Text), Dd_Mese.SelectedValue, GiorniMese(Dd_Mese.SelectedValue, Val(Txt_Anno.Text)))

        cmd.Parameters.AddWithValue("@DataDal", Txt_DataDalText)
        cmd.Parameters.AddWithValue("@DataAl", Txt_DataAlText)
        cmd.Parameters.AddWithValue("@DataDal", Txt_DataDalText)
        cmd.Parameters.AddWithValue("@DataDal", Txt_DataDalText)
        cmd.Connection = cn

        Dim COLORECELLA As String = "white"
        Dim myPOSTreader As OleDbDataReader = cmd.ExecuteReader()
        Do While myPOSTreader.Read


            Lbl_Schema.Text = Lbl_Schema.Text & "<tr>"
            Lbl_Schema.Text = Lbl_Schema.Text & "<td style=""border-width:1px; border-color:black; background-color:" & COLORECELLA & "; border-style:solid;font-size:small; "">" & campodb(myPOSTreader.Item("CodiceOspite")) & "</td>"
            Lbl_Schema.Text = Lbl_Schema.Text & "<td id='lbl" & campodb(myPOSTreader.Item("CodiceOspite")) & "' style=""border-width:1px; border-color:black; background-color:" & COLORECELLA & "; border-style:solid;font-size:small;"" onmouseout=""deletebox('lbl" & campodb(myPOSTreader.Item("CodiceOspite")) & "');"" onmouseover=""apribox(" & campodb(myPOSTreader.Item("CodiceOspite")) & ",'" & Cmb_CServ.SelectedValue & "'," & Txt_Anno.Text & "," & Dd_Mese.SelectedValue & ",'lbl" & campodb(myPOSTreader.Item("CodiceOspite")) & "');"">" & campodb(myPOSTreader.Item("Nome")) & "</td>"
            Lbl_Schema.Text = Lbl_Schema.Text & "<td style=""border-width:1px; border-color:black; background-color:" & COLORECELLA & "; border-style:solid;font-size:small;"">" & campodb(myPOSTreader.Item("DataNascita")) & "</td>"

            For i = Val(Txt_Dal.Text) To Val(Txt_Al.Text)
                Dim NomeCella As String
                NomeCella = "Cella" & i & "O" & campodb(myPOSTreader.Item("CodiceOspite"))
                Lbl_Schema.Text = Lbl_Schema.Text & "<td style=""border-width:1px; border-color:black; background-color:" & COLORECELLA & ";width:30px; height:34px; border-style:solid; text-align: center;""><div id=""" & NomeCella & """ onclick=""apricella('" & Cmb_CServ.SelectedValue & "'," & campodb(myPOSTreader.Item("CodiceOspite")) & "," & Val(Txt_Anno.Text) & "," & Dd_Mese.SelectedValue & "," & i & ",'" & NomeCella & "');"" >&nbsp;</div></td>"
                Formula = Formula & "leggicella('" & Cmb_CServ.SelectedValue & "'," & campodb(myPOSTreader.Item("CodiceOspite")) & "," & Val(Txt_Anno.Text) & "," & Dd_Mese.SelectedValue & "," & i & ",'" & NomeCella & "');"
            Next i


            If COLORECELLA = "white" Then
                COLORECELLA = "#F8F8F8"
            Else
                COLORECELLA = "white"
            End If
            Lbl_Schema.Text = Lbl_Schema.Text & "</tr>"

            Riga = Riga + 1

            If Riga > 15 Then
                Lbl_Schema.Text = Lbl_Schema.Text & "<tr>"
                Lbl_Schema.Text = Lbl_Schema.Text & "<td style=""width:50px;font-size:small;"">Codice</td>"
                Lbl_Schema.Text = Lbl_Schema.Text & "<td style=""width:200px;font-size:small;"">Nome</td>"
                Lbl_Schema.Text = Lbl_Schema.Text & "<td style=""width:80px;font-size:small;"">Data Nascita</td>"
                For i = Val(Txt_Dal.Text) To Val(Txt_Al.Text)
                    Dim DataVerifica As Date

                    DataVerifica = DateSerial(Val(Txt_Anno.Text), Dd_Mese.SelectedValue, i)
                    If DataVerifica.DayOfWeek = DayOfWeek.Saturday Or DataVerifica.DayOfWeek = DayOfWeek.Sunday Then
                        Lbl_Schema.Text = Lbl_Schema.Text & "<td style=""color:red;width:30px;text-align: center;"">" & i & "</td>"
                    Else
                        Lbl_Schema.Text = Lbl_Schema.Text & "<td style=""width:30px;text-align: center;"">" & i & "</td>"
                    End If
                Next
                Lbl_Schema.Text = Lbl_Schema.Text & "</tr>"
                Riga = 0
            End If
        Loop
        cn.Close()
        Lbl_Schema.Text = Lbl_Schema.Text & "</table>"




        ScriptManager.RegisterStartupScript(Me, Me.GetType(), "XSFormula", Formula, True)

        Call EseguiJS()
    End Sub

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Page.IsPostBack = True Then Exit Sub


        Dim K1 As New Cls_SqlString

        Dim Barra As New Cls_BarraSenior

        If Not IsNothing(Session("RicercaAnagraficaSQLString")) Then
            K1 = Session("RicercaAnagraficaSQLString")
        End If

        Lbl_BarraSenior.Text = Barra.CodiceBarra(Application("SENIOR"), Session("UTENTE"), Page, K1)



        Dim kVilla As New Cls_TabelleDescrittiveOspitiAccessori


        kVilla.UpDateDropBox(Session("DC_OSPITIACCESSORI"), "VIL", DD_Struttura)
        If DD_Struttura.Items.Count = 1 Then
            Call AggiornaCServ()
        End If



        Dim f As New Cls_Parametri

        f.LeggiParametri(Session("DC_OSPITE"))
        Dd_Mese.SelectedValue = f.MeseFatturazione
        Txt_Anno.Text = f.AnnoFatturazione



        Txt_Dal.Text = "1"
        Txt_Al.Text = GiorniMese(Dd_Mese.SelectedValue, Val(Txt_Anno.Text))

        Call EseguiJS()
    End Sub



    Protected Sub ImageButton3_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles ImageButton3.Click

        If Cmb_CServ.SelectedValue = "" Then        
            ScriptManager.RegisterStartupScript(Me, Me.GetType(), "visualizzaA", "VisualizzaErrore('Specificare centro servizio');", True)
            Exit Sub
        End If

        If Val(Txt_Anno.Text) < 1900 Then            
            ScriptManager.RegisterStartupScript(Me, Me.GetType(), "visualizzaA", "VisualizzaErrore('Specificare Anno');", True)
            Exit Sub
        End If

        If Val(Txt_Dal.Text) > Val(Txt_Al.Text) Then
            ScriptManager.RegisterStartupScript(Me, Me.GetType(), "visualizzaA", "VisualizzaErrore('Dal non può essere maggiore di al');", True)
            Exit Sub
        End If

        If Val(Txt_Al.Text) > GiorniMese(Dd_Mese.SelectedValue, Val(Txt_Anno.Text)) Then
            ScriptManager.RegisterStartupScript(Me, Me.GetType(), "visualizzaA", "VisualizzaErrore('Al non può essere maggiore del numero di giorni del mese');", True)
            Exit Sub
        End If

        If Val(Txt_Dal.Text) > GiorniMese(Dd_Mese.SelectedValue, Val(Txt_Anno.Text)) Then
            ScriptManager.RegisterStartupScript(Me, Me.GetType(), "visualizzaA", "VisualizzaErrore('Dal non può essere maggiore del numero di giorni del mese');", True)
            Exit Sub
        End If
        If Val(Txt_Dal.Text) = 0 Then
            ScriptManager.RegisterStartupScript(Me, Me.GetType(), "visualizzaA", "VisualizzaErrore('Dal non può essere zero');", True)
            Exit Sub
        End If
        If Val(Txt_Al.Text) = 0 Then
            ScriptManager.RegisterStartupScript(Me, Me.GetType(), "visualizzaA", "VisualizzaErrore('Al non può essere zero');", True)
            Exit Sub
        End If

        Call ProspettoPresenze()
    End Sub


    Protected Sub Btn_Esci_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles Btn_Esci.Click
        Response.Redirect("Menu_Ospiti.aspx")
    End Sub

    Private Sub AggiornaCServ()
        Dim kCsrv As New Cls_CentroServizio

        If DD_Struttura.SelectedValue = "" Then
            kCsrv.UpDateDropBox(Session("DC_OSPITE"), Cmb_CServ)
        Else
            kCsrv.UpDateDropBoxStruttura(Session("DC_OSPITE"), Cmb_CServ, DD_Struttura.SelectedValue)
        End If

    End Sub



    Private Sub EseguiJS()
        Dim MyJs As String
        MyJs = "$(document).ready(function() {"

        MyJs = MyJs & "if (window.innerHeight>0) { $(""#BarraLaterale"").css(""height"",(window.innerHeight - 94) + ""px""); } else"
        MyJs = MyJs & "{ $(""#BarraLaterale"").css(""height"",(document.documentElement.offsetHeight - 94) + ""px"");  }"

        MyJs = MyJs & "var els = document.getElementsByTagName(""*"");"

        MyJs = MyJs & "for (var i=0;i<els.length;i++)"
        MyJs = MyJs & "if ( els[i].id ) { "
        MyJs = MyJs & " var appoggio =els[i].id; "
        MyJs = MyJs & " if (appoggio.match('Txt_Data')!= null) {  "
        MyJs = MyJs & " $(els[i]).mask(""99/99/9999"");"
        MyJs = MyJs & "    }"


        MyJs = MyJs & "} "
        MyJs = MyJs & "});"

        'MyJs = "$(document).ready(function() { $('.myClass').keypress(function() { handleEnter($('.myClass').val(), true, true); } );     $('#" & Txt_ImportoPagato.ClientID & "').blur(function() { var ap = formatNumber ($('#" & Txt_ImportoPagato.ClientID & "').val(),2); $('#" & Txt_ImportoPagato.ClientID & "').val(ap); }); });"
        ScriptManager.RegisterStartupScript(Me, Me.GetType(), "visualizzaRitIm", MyJs, True)
    End Sub



    Protected Sub DD_Struttura_TextChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles DD_Struttura.TextChanged
        AggiornaCServ()
        Call EseguiJS()
    End Sub

    Protected Sub Dd_Mese_TextChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles Dd_Mese.TextChanged
        Txt_Dal.Text = "1"
        Txt_Al.Text = GiorniMese(Dd_Mese.SelectedValue, Val(Txt_Anno.Text))
    End Sub

    Protected Sub Cmb_CServ_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Cmb_CServ.Load

    End Sub
End Class
