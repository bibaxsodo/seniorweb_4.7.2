﻿Public Class notefattura
    Implements System.Web.IHttpHandler, IRequiresSessionState

    Public Sub ProcessRequest(ByVal context As HttpContext) Implements IHttpHandler.ProcessRequest
        context.Response.ContentType = "text/plain"
        Dim CODICEOSPITE As String = context.Request.QueryString("CODICEOSPITE")
        Dim NOTE As String = context.Request.QueryString("NOTE")

        context.Response.Cache.SetCacheability(HttpCacheability.NoCache)


        If Trim(context.Session("UTENTE")) = "" Then
            Exit Sub
        End If


        Dim K As New Cls_NoteOspite

        K.CodiceOspite = CODICEOSPITE
        K.Leggi(context.Session("DC_OSPITE"), K.CodiceOspite)
        K.CodiceOspite = CODICEOSPITE
        K.Note = NOTE
        K.Scrivi(context.Session("DC_OSPITE"))


    End Sub

    Public ReadOnly Property IsReusable() As Boolean Implements IHttpHandler.IsReusable
        Get
            Return False
        End Get
    End Property

End Class