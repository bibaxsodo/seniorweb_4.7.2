﻿Imports System.Data.OleDb
Imports System.Web.Hosting

Public Class XmlTabellaPresenze
    Implements System.Web.IHttpHandler

    Public Sub ProcessRequest(ByVal context As HttpContext) Implements IHttpHandler.ProcessRequest
        Dim DbC As New Cls_Login
        Dim ANNO As String = context.Request.QueryString("ANNO")
        Dim MESE As String = context.Request.QueryString("MESE")
        Dim UTENTE As String = context.Request.QueryString("UTENTE")
        Dim CSERV As String = context.Request.QueryString("CSERV")
        Dim MySql As String


        If IsNothing(CSERV) Then
            CSERV = ""
        End If

        DbC.Utente = UTENTE
        DbC.LeggiSP(context.Application("SENIOR"))

        Dim cn As OleDbConnection

        cn = New Data.OleDb.OleDbConnection(DbC.Ospiti)

        cn.Open()

        MySql = "SELECT * FROM [PresenzeOspiti] WHERE ANNO = ?  AND MESE = ?"
        If CSERV <> "" Then
            MySql = MySql & " And Cserv = '" & CSERV & "'"
        End If
        Dim cmd As New OleDbCommand()
        cmd.CommandText = MySql
        cmd.Parameters.AddWithValue("@ANNO", ANNO)
        cmd.Parameters.AddWithValue("@MESE", MESE)
        cmd.Connection = cn
        Dim myXML As String

        myXML = "<tabella>"

        Dim myPOSTreader As OleDbDataReader = cmd.ExecuteReader()
        Do While myPOSTreader.Read
            myXML = myXML & "<righe>"
            myXML = myXML & "<ID>" & campodb(myPOSTreader("ID")) & "</ID>"
            myXML = myXML & "<Cserv>" & campodb(myPOSTreader("Cserv")) & "</Cserv>"
            myXML = myXML & "<DecodificaCentroServizio>" & campodb(myPOSTreader("DecodificaCentroServizio")) & "</DecodificaCentroServizio>"
            myXML = myXML & "<CodiceOspite>" & campodb(myPOSTreader("CodiceOspite")) & "</CodiceOspite>"
            myXML = myXML & "<Anno>" & campodb(myPOSTreader("Anno")) & "</Anno>"
            myXML = myXML & "<Mese>" & campodb(myPOSTreader("Mese")) & "</Mese>"
            myXML = myXML & "<Nome>" & campodb(myPOSTreader("Nome")) & "</Nome>"
            myXML = myXML & "<codicefiscale>" & campodb(myPOSTreader("codicefiscale")) & "</codicefiscale>"
            myXML = myXML & "<Giorno>" & campodb(myPOSTreader("Giorno")) & "</Giorno>"
            myXML = myXML & "<Causale>" & campodb(myPOSTreader("Causale")) & "</Causale>"
            myXML = myXML & "<DecodificaCausale>" & campodb(myPOSTreader("DecodificaCausale")) & "</DecodificaCausale>"
            myXML = myXML & "<QuotaOspite>" & campodb(myPOSTreader("QuotaOspite")) & "</QuotaOspite>"
            myXML = myXML & "<QuotaParenti>" & campodb(myPOSTreader("QuotaParenti")) & "</QuotaParenti>"
            myXML = myXML & "<QuotaComune>" & campodb(myPOSTreader("QuotaComune")) & "</QuotaComune>"
            myXML = myXML & "<QuotaRegione>" & campodb(myPOSTreader("QuotaRegione")) & "</QuotaRegione>"
            myXML = myXML & "<TipoQuotaSaniaria>" & campodb(myPOSTreader("TipoQuotaSaniaria")) & "</TipoQuotaSaniaria>"
            myXML = myXML & "<TipoRetta>" & campodb(myPOSTreader("TipoRetta")) & "</TipoRetta>"
            myXML = myXML & "<PresenzaAssenza>" & campodb(myPOSTreader("PresenzaAssenza")) & "</PresenzaAssenza>"

            Dim DatiOspite As New ClsOspite

            DatiOspite.CodiceOspite = campodb(myPOSTreader("CodiceOspite"))
            DatiOspite.Leggi(DbC.Ospiti, DatiOspite.CodiceOspite)


            myXML = myXML & "<DataNascita>" & Format(DatiOspite.DataNascita, "dd/MM/yyyy") & "</DataNascita>"

            myXML = myXML & "<Sesso>" & DatiOspite.Sesso & "</Sesso>"


            Dim UltimoAccoglimento As New Cls_Movimenti

            UltimoAccoglimento.CodiceOspite = campodb(myPOSTreader("CodiceOspite"))
            UltimoAccoglimento.CENTROSERVIZIO = campodb(myPOSTreader("Cserv"))
            UltimoAccoglimento.Data = DateSerial(Val(campodb(myPOSTreader("Anno"))), Val(campodb(myPOSTreader("Mese"))), Val(campodb(myPOSTreader("Giorno"))))

            UltimoAccoglimento.UltimaDataAccoglimentoAData(DbC.Ospiti)


            myXML = myXML & "<DataAccoglimento>" & Format(UltimoAccoglimento.Data, "dd/MM/yyyy") & "</DataAccoglimento>"


            If UltimoAccoglimento.Causale = "" Then
                myXML = myXML & "<MotivoIngresso>ND</MotivoIngresso>"
            Else
                Dim DecodificaCausale As New Cls_CausaliEntrataUscita


                DecodificaCausale.Codice = UltimoAccoglimento.Causale
                DecodificaCausale.LeggiCausale(DbC.Ospiti)

                myXML = myXML & "<MotivoIngresso>" & DecodificaCausale.Descrizione & "</MotivoIngresso>"
            End If


            Dim UltimaUscitaDef As New Cls_Movimenti


            UltimaUscitaDef.CodiceOspite = campodb(myPOSTreader("CodiceOspite"))
            UltimaUscitaDef.CENTROSERVIZIO = campodb(myPOSTreader("Cserv"))
            UltimaUscitaDef.Data = DateSerial(Val(campodb(myPOSTreader("Anno"))), Val(campodb(myPOSTreader("Mese"))), Val(campodb(myPOSTreader("Giorno"))))

            UltimaUscitaDef.UltimaDataUscita(DbC.Ospiti, UltimaUscitaDef.CodiceOspite, UltimaUscitaDef.CENTROSERVIZIO, UltimaUscitaDef.Data)

            myXML = myXML & "<DataDimissione>" & Format(UltimaUscitaDef.Data, "dd/MM/yyyy") & "</DataDimissione>"

            If UltimaUscitaDef.Causale = "" Then
                myXML = myXML & "<MotivoDimissione>ND</MotivoDimissione>"
            Else
                Dim DecodificaCausale As New Cls_CausaliEntrataUscita


                DecodificaCausale.Codice = UltimaUscitaDef.Causale
                DecodificaCausale.LeggiCausale(DbC.Ospiti)

                myXML = myXML & "<MotivoIngresso>" & DecodificaCausale.Descrizione & "</MotivoIngresso>"
            End If


            Dim ComuneResidenza As New ClsComune

            ComuneResidenza.Descrizione = ""
            ComuneResidenza.Provincia = DatiOspite.RESIDENZAPROVINCIA1
            ComuneResidenza.Comune = DatiOspite.RESIDENZACOMUNE1
            ComuneResidenza.Leggi(DbC.Ospiti)
            If ComuneResidenza.Descrizione = "" Then
                myXML = myXML & "<ComuneResidenza>ND</ComuneResidenza>"
            Else
                myXML = myXML & "<ComuneResidenza>" & ComuneResidenza.Descrizione & "</ComuneResidenza>"
            End If

            myXML = myXML & "<IndirizzoResidenza>" & DatiOspite.RESIDENZAINDIRIZZO1 & "</IndirizzoResidenza>"

            myXML = myXML & "<CapResidenza>" & DatiOspite.RESIDENZACAP1 & "</CapResidenza>"


            Dim Parente As New Cls_Parenti
            Parente.Nome = ""

            Parente.CodiceOspite = campodb(myPOSTreader("CodiceOspite"))
            Parente.CodiceParente = 1
            Parente.Leggi(DbC.Ospiti, Parente.CodiceOspite, Parente.CodiceParente)

            If Parente.Nome <> "" Then
                myXML = myXML & "<Parente1Nome>" & Parente.Nome & "</Parente1Nome>"

                If Parente.CODICEFISCALE = "" Then
                    myXML = myXML & "<Parente1CF>ND</Parente1CF>"
                Else
                    myXML = myXML & "<Parente1CF>" & Parente.CODICEFISCALE & "</Parente1CF>"
                End If

                If Parente.RESIDENZAINDIRIZZO1 = "" Then
                    myXML = myXML & "<Parente1ResidenzaIndirizzo>ND</Parente1ResidenzaIndirizzo>"
                Else
                    myXML = myXML & "<Parente1ResidenzaIndirizzo>" & Parente.RESIDENZAINDIRIZZO1 & "</Parente1ResidenzaIndirizzo>"
                End If

                If Parente.RESIDENZACOMUNE1 = "" Then
                    myXML = myXML & "<Parente1ResidenzaComune>ND</Parente1ResidenzaComune>"
                Else

                    ComuneResidenza.Descrizione = ""
                    ComuneResidenza.Provincia = Parente.RESIDENZAPROVINCIA1
                    ComuneResidenza.Comune = Parente.RESIDENZACOMUNE1
                    ComuneResidenza.Leggi(DbC.Ospiti)

                    myXML = myXML & "<Parente1ResidenzaComune>" & ComuneResidenza.Descrizione & "</Parente1ResidenzaComune>"
                End If

                If Parente.RESIDENZACAP1 = "" Then
                    myXML = myXML & "<Parente1ResidenzaCap>ND</Parente1ResidenzaCap>"
                Else
                    myXML = myXML & "<Parente1ResidenzaCap>" & Parente.RESIDENZACAP1 & "</Parente1ResidenzaCap>"
                End If
            Else
                myXML = myXML & "<Parente1Nome>ND</Parente1Nome>"
                myXML = myXML & "<Parente1CF>ND</Parente1CF>"
                myXML = myXML & "<Parente1ResidenzaIndirizzo>ND</Parente1ResidenzaIndirizzo>"
                myXML = myXML & "<Parente1ResidenzaComune>ND</Parente1ResidenzaComune>"
                myXML = myXML & "<Parente1ResidenzaCap>ND</Parente1ResidenzaCap>"
            End If


            Parente.Nome = ""
            Parente.CodiceOspite = campodb(myPOSTreader("CodiceOspite"))
            Parente.CodiceParente = 2
            Parente.Leggi(DbC.Ospiti, Parente.CodiceOspite, Parente.CodiceParente)

            If Parente.Nome <> "" Then
                myXML = myXML & "<Parente2Nome>" & Parente.Nome & "</Parente2Nome>"

                If Parente.CODICEFISCALE = "" Then
                    myXML = myXML & "<Parente2CF>ND</Parente2CF>"
                Else
                    myXML = myXML & "<Parente2CF>" & Parente.CODICEFISCALE & "</Parente2CF>"
                End If

                If Parente.RESIDENZAINDIRIZZO1 = "" Then
                    myXML = myXML & "<Parente2ResidenzaIndirizzo>ND</Parente2ResidenzaIndirizzo>"
                Else
                    myXML = myXML & "<Parente2ResidenzaIndirizzo>" & Parente.RESIDENZAINDIRIZZO1 & "</Parente2ResidenzaIndirizzo>"
                End If

                If Parente.RESIDENZACOMUNE1 = "" Then
                    myXML = myXML & "<Parente2ResidenzaComune>ND</Parente2ResidenzaComune>"
                Else

                    ComuneResidenza.Descrizione = ""
                    ComuneResidenza.Provincia = Parente.RESIDENZAPROVINCIA1
                    ComuneResidenza.Comune = Parente.RESIDENZACOMUNE1
                    ComuneResidenza.Leggi(DbC.Ospiti)

                    myXML = myXML & "<Parente2ResidenzaComune>" & ComuneResidenza.Descrizione & "</Parente2ResidenzaComune>"
                End If

                If Parente.RESIDENZACAP1 = "" Then
                    myXML = myXML & "<Parente2ResidenzaCap>ND</Parente2ResidenzaCap>"
                Else
                    myXML = myXML & "<Parente2ResidenzaCap>" & Parente.RESIDENZACAP1 & "</Parente2ResidenzaCap>"
                End If
            Else
                myXML = myXML & "<Parente2Nome>ND</Parente2Nome>"
                myXML = myXML & "<Parente2CF>ND</Parente2CF>"
                myXML = myXML & "<Parente2ResidenzaIndirizzo>ND</Parente2ResidenzaIndirizzo>"
                myXML = myXML & "<Parente2ResidenzaComune>ND</Parente2ResidenzaComune>"
                myXML = myXML & "<Parente2ResidenzaCap>ND</Parente2ResidenzaCap>"
            End If


            Dim Letto As New Cls_MovimentiStanze

            Letto.Letto = ""
            Letto.CodiceOspite = campodb(myPOSTreader("CodiceOspite"))
            Letto.UltimoMovimentoOspite(DbC.Ospiti)

            If Letto.Letto = "" Then
                myXML = myXML & "<Letto>ND</Letto>"
            Else
                myXML = myXML & "<Letto>" & Letto.Villa & Letto.Reparto & Letto.Piano & Letto.Reparto & Letto.Stanza & Letto.Letto & "</Letto>"
            End If

            myXML = myXML & "</righe>"
        Loop
        myPOSTreader.Close()

        myXML = myXML & "</tabella>"


        Dim NomeFile As String

        NomeFile = HostingEnvironment.ApplicationPhysicalPath() & "\Public\Xml_" & DbC.RagioneSociale & "_" & ANNO & "_" & MESE & ".xml"
        Dim tw As System.IO.TextWriter = System.IO.File.CreateText(NomeFile)
        tw.Write(myXML)

        tw.Close()

        context.Response.Write("OK")
    End Sub



    Function campodb(ByVal oggetto As Object) As String
        If IsDBNull(oggetto) Then
            Return ""
        Else
            Return oggetto
        End If
    End Function
    Public ReadOnly Property IsReusable() As Boolean Implements IHttpHandler.IsReusable
        Get
            Return False
        End Get
    End Property

End Class