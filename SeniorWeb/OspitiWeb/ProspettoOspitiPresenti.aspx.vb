﻿Imports System.Web.Hosting
Imports System.Data.OleDb

Partial Class OspitiWeb_ProspettoOspitiPresenti
    Inherits System.Web.UI.Page
    Dim MyTable As New System.Data.DataTable("tabella")
    Dim MyDataSet As New System.Data.DataSet()

    Function campodb(ByVal oggetto As Object) As String
        If IsDBNull(oggetto) Then
            Return ""
        Else
            Return oggetto
        End If
    End Function
    Private Sub ProspettoPresenze(ByVal Cancella As Boolean)
        Dim ConnectionString As String = Session("DC_OSPITE")
        Dim cn As OleDbConnection
        Dim MySql As String
        Dim Condizione As String
        Dim ContaOspiti As Integer = 0

        cn = New Data.OleDb.OleDbConnection(ConnectionString)

        cn.Open()

        If Cancella Then

            MyTable.Clear()
            MyTable.Columns.Clear()
            MyTable.Columns.Add("Codice Ospite", GetType(Long)) '1
            MyTable.Columns.Add("Nome", GetType(String)) '2
            MyTable.Columns.Add("Data Nascita", GetType(String)) '3
            MyTable.Columns.Add("Luogo Nascita", GetType(String)) '4
            MyTable.Columns.Add("Data Accoglimento", GetType(String)) '5
            MyTable.Columns.Add("Data Uscita Definitiva", GetType(String)) '6
            MyTable.Columns.Add("Codice Fiscale", GetType(String)) '7
            MyTable.Columns.Add("Indirizzo", GetType(String)) '8
            MyTable.Columns.Add("Comune", GetType(String)) '9
            MyTable.Columns.Add("Provincia", GetType(String)) '10
            MyTable.Columns.Add("Cap", GetType(String)) '11
            MyTable.Columns.Add("Medico", GetType(String)) '12
            MyTable.Columns.Add("Quota Ospite", GetType(String)) '13
            MyTable.Columns.Add("Quota Parenti", GetType(String)) '14
            MyTable.Columns.Add("Quota Comune", GetType(String)) '15
            MyTable.Columns.Add("Comune Compartecipazione", GetType(String)) '16
            MyTable.Columns.Add("Quota Regione", GetType(String)) '17
            MyTable.Columns.Add("Regione Compartecipazione", GetType(String)) '18
            MyTable.Columns.Add("TipoRetta", GetType(String)) '19
            MyTable.Columns.Add("Extra", GetType(String)) '20
            MyTable.Columns.Add("ModalitaPagamento", GetType(String)) '21
            MyTable.Columns.Add("Dati Banca", GetType(String)) '22
            MyTable.Columns.Add("Numero Mandato", GetType(String)) '23
            MyTable.Columns.Add("Data Mandato", GetType(String)) '24

            MyTable.Columns.Add("Cognome Nome Pagante", GetType(String)) '25
            MyTable.Columns.Add("Data Nascita Pagante", GetType(String)) '26
            MyTable.Columns.Add("Indirizzo Pagante", GetType(String)) '27
            MyTable.Columns.Add("Comune Pagante", GetType(String)) '28
            MyTable.Columns.Add("Cap Pagante", GetType(String)) '29
            MyTable.Columns.Add("Cognome Destinatario Fattura", GetType(String)) '30
            MyTable.Columns.Add("Indirizzo Destinatario Fattura", GetType(String)) '31
            MyTable.Columns.Add("Comune Destinatario Fattura", GetType(String)) '32
            MyTable.Columns.Add("Cap Destinatario Fattura", GetType(String)) '33
            MyTable.Columns.Add("Codice Iva", GetType(String)) '34
            MyTable.Columns.Add("Tipo Operazione", GetType(String)) '35

            MyTable.Columns.Add("Addebiti/Accrediti Programmato", GetType(String)) '36
            MyTable.Columns.Add("MailDestinatarioFatturaOspite", GetType(String)) '37

            MyTable.Columns.Add("Data Inizio", GetType(String)) '38
            MyTable.Columns.Add("Data Fine", GetType(String)) '39
            MyTable.Columns.Add("Numero Impegnativa", GetType(String)) '40

            MyTable.Columns.Add("Assegno Accompagnamento", GetType(String)) '41

            MyTable.Columns.Add("Listino", GetType(String)) '42
            MyTable.Columns.Add("Scadenza Listino", GetType(String)) '43

            MyTable.Columns.Add("Stato", GetType(String)) '44

            MyTable.Columns.Add("Documenti", GetType(String)) '45
            MyTable.Columns.Add("Importo Fatturato", GetType(String)) '46


            MyTable.Columns.Add("Importi Extra Ospite", GetType(String)) '47
            MyTable.Columns.Add("Importi Extra Parente", GetType(String)) '48
            MyTable.Columns.Add("Importi Extra Comunte", GetType(String)) '49
            MyTable.Columns.Add("Cartella", GetType(String)) '50
            MyTable.Columns.Add("Note", GetType(String)) '51

            MyTable.Columns.Add("NonCalcolareComune", GetType(String)) '52

        End If



        Dim CentroServizioIndicato As Boolean = True
        If Cmb_CServ.SelectedValue = "" Then

            CentroServizioIndicato = False
        End If

        Dim XCserv As Integer

        For XCserv = 0 To Cmb_CServ.Items.Count - 1


            If CentroServizioIndicato = False Then

                Cmb_CServ.SelectedIndex = XCserv
            End If

            Dim cmd As New OleDbCommand()
            Dim SelezioneInUSo As String = ""
            If RB_InUSo.Checked = True Then
                SelezioneInUSo = "(NonInUso = '' or NonInUso  is null ) And"
            End If
            If RB_NonInUso.Checked = True Then
                SelezioneInUSo = " NonInUso = 'S'  And"
            End If
            If RB_Tutti.Checked = True Then
                SelezioneInUSo = ""
            End If
            MySql = "Select * From AnagraficaComune Where " & SelezioneInUSo & " CodiceParente = 0 And CodiceOspite > 0 And ( (Select count(*) From Movimenti Where CodiceOspite = AnagraficaComune.CodiceOspite And Data >= ? And Data <= ? And CENTROSERVIZIO = '" & Cmb_CServ.SelectedValue & "') > 0 OR " & _
                                                            " (" & _
                                                            " ((Select Top 1 TipoMov From Movimenti Where CodiceOspite = AnagraficaComune.CodiceOspite And Data < ? And CENTROSERVIZIO = '" & Cmb_CServ.SelectedValue & "' Order By Data DESC,Progressivo Desc) <> '13') " & _
                                                            " And " & _
                                                            " ((Select  count(*) From Movimenti Where CodiceOspite = AnagraficaComune.CodiceOspite And Data < ? And CENTROSERVIZIO = '" & Cmb_CServ.SelectedValue & "' And TipoMov = '05') <>  0) " & _
                                                            " )" & _
                                                            " ) order by Nome"


            cmd.CommandText = MySql
            Dim Txt_DataDalText As Date = Txt_DataDal.Text
            Dim Txt_DataAlText As Date = Txt_DataAl.Text

            cmd.Parameters.AddWithValue("@DataDal", Txt_DataDalText)
            cmd.Parameters.AddWithValue("@DataAl", Txt_DataAlText)
            cmd.Parameters.AddWithValue("@DataDal", Txt_DataDalText)
            cmd.Parameters.AddWithValue("@DataDal", Txt_DataDalText)
            cmd.Connection = cn
            Dim myPOSTreader As OleDbDataReader = cmd.ExecuteReader()
            Do While myPOSTreader.Read

                If (campodb(myPOSTreader.Item("Sesso")) = "F" And Chk_Donne.Checked = True) Or (campodb(myPOSTreader.Item("Sesso")) = "M" And Chk_Uomini.Checked = True) Or campodb(myPOSTreader.Item("Sesso")) = "" Then
                    Dim myriga As System.Data.DataRow = MyTable.NewRow()
                    myriga(0) = campodb(myPOSTreader.Item("CodiceOspite"))
                    myriga(1) = campodb(myPOSTreader.Item("Nome"))
                    myriga(2) = campodb(myPOSTreader.Item("DataNascita"))

                    Dim DecCom As New ClsComune

                    DecCom.Comune = campodb(myPOSTreader.Item("ComuneDiNascita"))
                    DecCom.Provincia = campodb(myPOSTreader.Item("ProvinciaDiNascita"))
                    DecCom.DecodficaComune(Session("DC_OSPITE"))
                    myriga(3) = DecCom.Descrizione


                    Dim DAcc As New Cls_Movimenti
                    DAcc.CodiceOspite = campodb(myPOSTreader.Item("CodiceOspite"))
                    DAcc.CENTROSERVIZIO = Cmb_CServ.SelectedValue
                    DAcc.UltimaDataAccoglimento(Session("DC_OSPITE"))
                    myriga(4) = Format(DAcc.Data, "dd/MM/yyyy")
                    DAcc.CodiceOspite = campodb(myPOSTreader.Item("CodiceOspite"))
                    DAcc.CENTROSERVIZIO = Cmb_CServ.SelectedValue
                    DAcc.Data = Nothing
                    DAcc.UltimaDataUscitaDefinitiva(Session("DC_OSPITE"))
                    If DAcc.Data = Nothing Then
                        myriga(5) = ""
                    Else
                        myriga(5) = Format(DAcc.Data, "dd/MM/yyyy")
                    End If

                    myriga(6) = campodb(myPOSTreader.Item("CODICEFISCALE"))
                    myriga(7) = campodb(myPOSTreader.Item("RESIDENZAINDIRIZZO1"))


                    DecCom.Comune = campodb(myPOSTreader.Item("RESIDENZACOMUNE1"))
                    DecCom.Provincia = campodb(myPOSTreader.Item("RESIDENZAPROVINCIA1"))
                    DecCom.DecodficaComune(Session("DC_OSPITE"))
                    myriga(8) = DecCom.Descrizione

                    DecCom.Comune = campodb(myPOSTreader.Item("RESIDENZACOMUNE1"))
                    DecCom.Provincia = ""
                    DecCom.DecodficaComune(Session("DC_OSPITE"))
                    myriga(9) = DecCom.Descrizione

                    myriga(10) = campodb(myPOSTreader.Item("RESIDENZACAP1"))


                    Dim Med As New Cls_Medici

                    Med.Pulisci()
                    Med.CodiceMedico = campodb(myPOSTreader.Item("CodiceMedico"))
                    Med.Leggi(Session("DC_OSPITE"))

                    If campodb(myPOSTreader.Item("CodiceMedico")) = "" Or Val(campodb(myPOSTreader.Item("CodiceMedico"))) = 0 Then
                        myriga(11) = campodb(myPOSTreader.Item("NomeMedico"))
                    Else
                        myriga(11) = Med.Nome
                    End If

                    Dim s As New Cls_CalcoloRette
                    s.STRINGACONNESSIONEDB = Session("DC_OSPITE")
                    s.ApriDB(Session("DC_OSPITE"), Session("DC_OSPITIACCESSORI"))

                    myriga(12) = Format(Math.Round(s.QuoteGiornaliere(Cmb_CServ.SelectedValue, campodb(myPOSTreader.Item("CodiceOspite")), "O", 0, Txt_DataAl.Text), 2), "#,##0.00")


                    If s.QuoteMensile(Cmb_CServ.SelectedValue, campodb(myPOSTreader.Item("CodiceOspite")), "O", 0, Txt_DataAl.Text) > 0 Then
                        myriga(12) = myriga(12) & " (" & s.QuoteMensile(Cmb_CServ.SelectedValue, campodb(myPOSTreader.Item("CodiceOspite")), "O", 0, Txt_DataAl.Text) & ") "
                    End If
                    Dim ImpParenti As Double = 0
                    Dim NumeroParentePagante As Integer
                    Dim i As Long
                    For i = 1 To 5
                        Dim ImpSingolo As Double = s.QuoteGiornaliere(Cmb_CServ.SelectedValue, campodb(myPOSTreader.Item("CodiceOspite")), "P", i, Txt_DataAl.Text)
                        If ImpSingolo > 0 Then
                            NumeroParentePagante = i
                        End If
                        ImpParenti = ImpParenti + ImpSingolo
                    Next
                    myriga(13) = Format(Math.Round(ImpParenti, 2), "#,##0.00")
                    For i = 1 To 5
                        Dim ImpSingolo As Double = s.QuoteMensile(Cmb_CServ.SelectedValue, campodb(myPOSTreader.Item("CodiceOspite")), "P", i, Txt_DataAl.Text)
                        If ImpSingolo > 0 Then
                            myriga(13) = myriga(13) & " (" & ImpSingolo & ")"
                        End If
                    Next
                    myriga(14) = Format(Math.Round(s.QuoteGiornaliere(Cmb_CServ.SelectedValue, campodb(myPOSTreader.Item("CodiceOspite")), "C", 0, Txt_DataAl.Text), 2), "#,##0.00")
                    If CDbl(myriga(14)) > 0 Then
                        Dim KlComune As New Cls_ImportoComune

                        KlComune.CENTROSERVIZIO = Cmb_CServ.SelectedValue
                        KlComune.CODICEOSPITE = Val(campodb(myPOSTreader.Item("CodiceOspite")))
                        KlComune.UltimaData(Session("DC_OSPITE"), KlComune.CODICEOSPITE, KlComune.CENTROSERVIZIO)

                        If KlComune.PROV <> "" And KlComune.COMUNE <> "" Then
                            Dim LkPVCM As New ClsComune

                            LkPVCM.Provincia = KlComune.PROV
                            LkPVCM.Comune = KlComune.COMUNE
                            LkPVCM.Leggi(Session("DC_OSPITE"))

                            myriga(15) = LkPVCM.Descrizione
                        Else
                            myriga(15) = ""
                        End If
                    Else
                        myriga(15) = ""
                    End If

                    If s.QuoteMensile(Cmb_CServ.SelectedValue, campodb(myPOSTreader.Item("CodiceOspite")), "C", 0, Txt_DataAl.Text) > 0 Then
                        myriga(14) = myriga(14) & " (" & s.QuoteMensile(Cmb_CServ.SelectedValue, campodb(myPOSTreader.Item("CodiceOspite")), "C", 0, Txt_DataAl.Text) & ")"
                    End If

                    myriga(16) = Format(Math.Round(s.QuoteGiornaliere(Cmb_CServ.SelectedValue, campodb(myPOSTreader.Item("CodiceOspite")), "R", 0, Txt_DataAl.Text), 2), "#,##0.00")

                    If CDbl(myriga(16)) > 0 Then
                        Dim KlComune As New Cls_StatoAuto

                        KlComune.CENTROSERVIZIO = Cmb_CServ.SelectedValue
                        KlComune.CODICEOSPITE = Val(campodb(myPOSTreader.Item("CodiceOspite")))
                        KlComune.UltimaData(Session("DC_OSPITE"), KlComune.CODICEOSPITE, KlComune.CENTROSERVIZIO)

                        Dim LkPVCM As New ClsUSL

                        LkPVCM.CodiceRegione = KlComune.USL
                        LkPVCM.Leggi(Session("DC_OSPITE"))



                        Dim TipoImporto As New Cls_TabellaTipoImportoRegione

                        TipoImporto.Codice = KlComune.TipoRetta
                        TipoImporto.Leggi(Session("DC_OSPITE"), TipoImporto.Codice)

                        myriga(17) = LkPVCM.Nome & " " & TipoImporto.Descrizione
                    Else
                        myriga(17) = ""
                    End If

                    s.ChiudiDB()

                    Dim kretta As New Cls_rettatotale

                    kretta.CODICEOSPITE = Val(campodb(myPOSTreader.Item("CodiceOspite")))
                    kretta.UltimaData(Session("DC_OSPITE"), kretta.CODICEOSPITE, Cmb_CServ.SelectedValue)

                    Dim KTpRet As New Cls_TipoRetta

                    KTpRet.Codice = kretta.TipoRetta
                    KTpRet.Leggi(Session("DC_OSPITE"), KTpRet.Codice)

                    myriga(18) = KTpRet.Descrizione

                    Dim Extra As New Cls_ExtraFisso

                    If Year(kretta.Data) > 1900 Then
                        Extra.Data = kretta.Data
                        Extra.CODICEOSPITE = Val(campodb(myPOSTreader.Item("CodiceOspite")))
                        Extra.CENTROSERVIZIO = Cmb_CServ.SelectedValue
                        myriga(19) = Extra.ElencaExtrafissiTesto(Session("DC_OSPITE"))
                    End If

                    Dim Ma As New Cls_DatiOspiteParenteCentroServizio

                    Ma.CodiceOspite = Val(campodb(myPOSTreader.Item("CodiceOspite")))
                    Ma.CentroServizio = Cmb_CServ.SelectedValue                    
                    Ma.Leggi(Session("DC_OSPITE"))

                    If Ma.ModalitaPagamento = "" Then
                        Ma.ModalitaPagamento = campodb(myPOSTreader.Item("ModalitaPagamento"))
                    End If


                    Dim MyModalita As New ClsModalitaPagamento

                    MyModalita.Codice = Ma.ModalitaPagamento
                    MyModalita.Leggi(Session("DC_OSPITE"))

                    myriga(20) = MyModalita.DESCRIZIONE


                    Dim KPag As New Cls_DatiPagamento

                    KPag.CodiceParente = 0
                    KPag.CodiceOspite = Val(campodb(myPOSTreader.Item("CodiceOspite")))
                    KPag.LeggiUltimaData(Session("DC_OSPITE"))

                    

                    Dim CCAp As String = KPag.CCBancario

                    If Len(CCAp) = 11 Then
                        CCAp = "0" & CCAp
                    End If
                    If Len(CCAp) = 10 Then
                        CCAp = "00" & CCAp
                    End If
                    If Len(CCAp) = 9 Then
                        CCAp = "000" & CCAp
                    End If
                    If Len(CCAp) = 8 Then
                        CCAp = "0000" & CCAp
                    End If

                    myriga(21) = KPag.Banca & " " & KPag.CodiceInt & Format(Val(KPag.NumeroControllo), "00") & KPag.Cin & Format(Val(KPag.Abi), "00000") & Format(Val(KPag.Cab), "00000") & CCAp
                    myriga(22) = KPag.IdMandatoDebitore
                    If Year(KPag.DataMandatoDebitore) > 1970 Then
                        myriga(23) = Format(KPag.DataMandatoDebitore, "dd/MM/yyyy")
                    Else
                        myriga(23) = ""
                    End If



                    If ImpParenti > 0 Then
                        Dim DatiPar As New Cls_Parenti

                        DatiPar.CodiceOspite = Val(campodb(myPOSTreader.Item("CodiceOspite")))
                        DatiPar.CodiceParente = NumeroParentePagante
                        DatiPar.Leggi(Session("DC_OSPITE"), DatiPar.CodiceOspite, DatiPar.CodiceParente)

                        myriga(24) = DatiPar.Nome
                        myriga(25) = Format(DatiPar.DataNascita, "dd/MM/yyyy")
                        myriga(26) = DatiPar.RESIDENZAINDIRIZZO1
                        DecCom.Descrizione = ""
                        DecCom.Comune = DatiPar.RESIDENZACOMUNE1
                        DecCom.Provincia = DatiPar.RESIDENZAPROVINCIA1
                        DecCom.DecodficaComune(Session("DC_OSPITE"))
                        myriga(27) = DecCom.Descrizione
                        myriga(28) = DatiPar.RESIDENZACAP1
                        myriga(29) = DatiPar.RECAPITONOME

                        myriga(30) = DatiPar.RECAPITOINDIRIZZO
                        DecCom.Descrizione = ""
                        DecCom.Comune = DatiPar.RECAPITOCOMUNE
                        DecCom.Provincia = DatiPar.RECAPITOPROVINCIA
                        DecCom.DecodficaComune(Session("DC_OSPITE"))
                        myriga(31) = DecCom.Descrizione
                        myriga(32) = DatiPar.RESIDENZACAP4


                        Dim k As New Cls_DatiOspiteParenteCentroServizio

                        k.CodiceOspite = campodb(myPOSTreader.Item("CODICEOSPITE"))
                        k.CentroServizio = Cmb_CServ.SelectedValue
                        k.CodiceParente = DatiPar.CodiceParente
                        k.Leggi(Session("DC_OSPITE"))
                        If k.TipoOperazione = "" And k.AliquotaIva = "" Then
                            k.TipoOperazione = campodb(myPOSTreader.Item("TIPOOPERAZIONE"))
                            k.AliquotaIva = campodb(myPOSTreader.Item("CODICEIVA"))
                        End If

                        Dim MIva As New Cls_IVA

                        MIva.Codice = k.AliquotaIva
                        MIva.Leggi(Session("DC_TABELLE"), k.AliquotaIva)
                        myriga(33) = MIva.Descrizione

                        Dim MOperazione As New Cls_TipoOperazione

                        MOperazione.Codice = k.TipoOperazione
                        MOperazione.Leggi(Session("DC_OSPITE"), k.TipoOperazione)

                        myriga(34) = MOperazione.Descrizione


                        Dim MyModalita1 As New ClsModalitaPagamento

                        MyModalita1.Codice = k.ModalitaPagamento
                        MyModalita1.Leggi(Session("DC_OSPITE"))

                        myriga(20) = "Parente :" & MyModalita1.DESCRIZIONE


                        Dim KPag1 As New Cls_DatiPagamento

                        KPag1.CodiceParente = DatiPar.CodiceParente
                        KPag1.CodiceOspite = Val(campodb(myPOSTreader.Item("CodiceOspite")))
                        KPag1.LeggiUltimaData(Session("DC_OSPITE"))


                        Dim CCAp1 As String = KPag1.CCBancario

                        If Len(CCAp1) = 11 Then
                            CCAp1 = "0" & CCAp
                        End If
                        If Len(CCAp1) = 10 Then
                            CCAp1 = "00" & CCAp
                        End If
                        If Len(CCAp1) = 9 Then
                            CCAp1 = "000" & CCAp
                        End If
                        If Len(CCAp1) = 8 Then
                            CCAp1 = "0000" & CCAp
                        End If

                        myriga(21) = "Parente :" & DatiPar.CodiceParente & KPag1.Banca & " " & KPag1.CodiceInt & Format(Val(KPag1.NumeroControllo), "00") & KPag1.Cin & Format(Val(KPag1.Abi), "00000") & Format(Val(KPag1.Cab), "00000") & CCAp1
                        myriga(22) = KPag1.IdMandatoDebitore
                        If Year(KPag1.DataMandatoDebitore) > 1970 Then
                            myriga(23) = Format(KPag1.DataMandatoDebitore, "dd/MM/yyyy")
                        Else
                            myriga(23) = ""
                        End If

                    Else
                        myriga(24) = campodb(myPOSTreader.Item("NOME"))
                        myriga(25) = campodb(myPOSTreader.Item("DataNascita"))
                        myriga(26) = campodb(myPOSTreader.Item("RESIDENZAINDIRIZZO1"))

                        DecCom.Descrizione = ""
                        DecCom.Comune = campodb(myPOSTreader.Item("RESIDENZACOMUNE1"))
                        DecCom.Provincia = campodb(myPOSTreader.Item("RESIDENZAPROVINCIA1"))
                        DecCom.DecodficaComune(Session("DC_OSPITE"))
                        myriga(27) = DecCom.Descrizione



                        myriga(28) = campodb(myPOSTreader.Item("RESIDENZACAP1"))



                        myriga(29) = campodb(myPOSTreader.Item("RECAPITONOME"))

                        myriga(30) = campodb(myPOSTreader.Item("RECAPITOINDIRIZZO"))
                        DecCom.Descrizione = ""
                        DecCom.Comune = campodb(myPOSTreader.Item("RECAPITOCOMUNE"))
                        DecCom.Provincia = campodb(myPOSTreader.Item("RECAPITOPROVINCIA"))
                        DecCom.DecodficaComune(Session("DC_OSPITE"))
                        myriga(31) = DecCom.Descrizione
                        myriga(32) = campodb(myPOSTreader.Item("RESIDENZACAP4"))


                        Dim k As New Cls_DatiOspiteParenteCentroServizio

                        k.CodiceOspite = campodb(myPOSTreader.Item("CODICEOSPITE"))
                        k.CentroServizio = Cmb_CServ.SelectedValue
                        k.Leggi(Session("DC_OSPITE"))
                        If k.TipoOperazione = "" And k.AliquotaIva = "" Then
                            k.TipoOperazione = campodb(myPOSTreader.Item("TIPOOPERAZIONE"))
                            k.AliquotaIva = campodb(myPOSTreader.Item("CODICEIVA"))
                        End If

                        Dim MIva As New Cls_IVA

                        MIva.Codice = k.AliquotaIva
                        MIva.Leggi(Session("DC_TABELLE"), k.AliquotaIva)
                        myriga(33) = MIva.Descrizione

                        Dim MOperazione As New Cls_TipoOperazione

                        MOperazione.Codice = k.TipoOperazione
                        MOperazione.Leggi(Session("DC_OSPITE"), k.TipoOperazione)

                        myriga(34) = MOperazione.Descrizione

                        Dim AddProgrammati As New Cls_AddebitoAccreditiProgrammati
                        AddProgrammati.CodiceOspite = campodb(myPOSTreader.Item("CODICEOSPITE"))
                        AddProgrammati.CentroServizio = Cmb_CServ.SelectedValue
                        AddProgrammati.UltimaData(Session("DC_OSPITE"))
                        If AddProgrammati.Importo > 0 Then

                            Dim MTipoAddebito As New Cls_Addebito

                            MTipoAddebito.Codice = AddProgrammati.TipoAddebito
                            MTipoAddebito.Leggi(Session("DC_OSPITE"), MTipoAddebito.Codice)


                            myriga(35) = MTipoAddebito.Descrizione & " " & AddProgrammati.Importo
                        End If

                        If campodb(myPOSTreader.Item("TELEFONO4")) = "" Then
                            myriga(36) = campodb(myPOSTreader.Item("RESIDENZATELEFONO3")) 'RESIDENZATELEFONO3
                        Else
                            myriga(36) = campodb(myPOSTreader.Item("TELEFONO4"))
                        End If

                    End If

                    Dim MImp As New Cls_Impegnativa

                    MImp.CENTROSERVIZIO = Cmb_CServ.SelectedValue
                    MImp.CODICEOSPITE = campodb(myPOSTreader.Item("CODICEOSPITE"))
                    MImp.UltimaData(Session("DC_OSPITE"), MImp.CODICEOSPITE, MImp.CENTROSERVIZIO)

                    If Year(MImp.DATAINIZIO) > 1990 Then
                        myriga(37) = Format(MImp.DATAINIZIO, "dd/MM/yyyy")
                    End If

                    If Year(MImp.DATAFINE) > 1990 Then
                        myriga(38) = Format(MImp.DATAINIZIO, "dd/MM/yyyy")
                    End If
                    If MImp.DESCRIZIONE <> "" Then
                        myriga(39) = MImp.DESCRIZIONE
                    End If


                    Dim MAcco As New Cls_Accompagnamento

                    MAcco.CENTROSERVIZIO = Cmb_CServ.SelectedValue
                    MAcco.CODICEOSPITE = campodb(myPOSTreader.Item("CODICEOSPITE"))
                    MAcco.LeggiAData(Session("DC_OSPITE"), Txt_DataDalText)
                    If MAcco.Accompagnamento = 1 Then
                        myriga(40) = "SI"
                    Else
                        myriga(40) = ""
                    End If


                    Dim MOs As New Cls_Listino

                    MOs.CENTROSERVIZIO = Cmb_CServ.SelectedValue
                    MOs.CODICEOSPITE = campodb(myPOSTreader.Item("CODICEOSPITE"))
                    MOs.LeggiAData(Session("DC_OSPITE"), Txt_DataAlText)

                    myriga(41) = ""

                    If MOs.CodiceListino <> "" Then
                        Dim DeCListi As New Cls_Tabella_Listino

                        DeCListi.Codice = MOs.CodiceListino
                        DeCListi.LeggiCausale(Session("DC_OSPITE"))

                        myriga(41) = DeCListi.Descrizione

                        DeCListi.Descrizione = ""
                        DeCListi.Codice = MOs.CodiceListinoSanitario
                        DeCListi.LeggiCausale(Session("DC_OSPITE"))
                        If DeCListi.Descrizione <> "" Then
                            myriga(41) = myriga(41) & "," & DeCListi.Descrizione
                        End If

                        DeCListi.Descrizione = ""
                        DeCListi.Codice = MOs.CodiceListinoSociale
                        DeCListi.LeggiCausale(Session("DC_OSPITE"))
                        If DeCListi.Descrizione <> "" Then
                            myriga(41) = myriga(41) & "," & DeCListi.Descrizione
                        End If


                        DeCListi.Descrizione = ""
                        DeCListi.Codice = MOs.CodiceListinoJolly
                        DeCListi.LeggiCausale(Session("DC_OSPITE"))
                        If DeCListi.Descrizione <> "" Then
                            myriga(41) = myriga(41) & "," & DeCListi.Descrizione
                        End If


                        If Year(DeCListi.DATA) > 1900 Then
                            myriga(42) = Format(DeCListi.DATA, "dd/MM/yyyy")
                        End If
                    End If

                    myriga(43) = campodb(myPOSTreader.Item("NonInUSo"))

                    If Chk_FatturaNelPeriodo.Checked = True Then
                        Dim Appoggio As String
                        Dim ImpAppoggio As Double

                        FattureNelPeriodo(Cmb_CServ.SelectedValue, campodb(myPOSTreader.Item("CODICEOSPITE")), Txt_DataDalText, Txt_DataAlText, Appoggio, ImpAppoggio)

                        myriga(44) = Appoggio
                        myriga(45) = ImpAppoggio
                    End If




                    Dim MImpOsp As New Cls_ImportoOspite

                    MImpOsp.CENTROSERVIZIO = Cmb_CServ.SelectedValue
                    MImpOsp.CODICEOSPITE = campodb(myPOSTreader.Item("CodiceOspite"))
                    MImpOsp.UltimaData(Session("DC_OSPITE"), MImpOsp.CODICEOSPITE, MImpOsp.CENTROSERVIZIO)

                    myriga(46) = ""
                    If MImpOsp.Importo1 > 0 Then
                        myriga(46) = Format(MImpOsp.Importo1, "#,##0.00")
                    End If
                    If MImpOsp.Importo2 > 0 Then
                        myriga(46) = myriga(46) & " - " & Format(MImpOsp.Importo2, "#,##0.00")
                    End If
                    If MImpOsp.Importo3 > 0 Then
                        myriga(46) = myriga(46) & " - " & Format(MImpOsp.Importo3, "#,##0.00")
                    End If
                    If MImpOsp.Importo4 > 0 Then
                        myriga(46) = myriga(46) & " - " & Format(MImpOsp.Importo4, "#,##0.00")
                    End If


                    Dim MImpPar As New Cls_ImportoParente

                    MImpPar.CENTROSERVIZIO = Cmb_CServ.SelectedValue
                    MImpPar.CODICEOSPITE = campodb(myPOSTreader.Item("CodiceOspite"))
                    MImpPar.UltimaData(Session("DC_OSPITE"), MImpOsp.CODICEOSPITE, MImpOsp.CENTROSERVIZIO, 1)

                    myriga(47) = ""
                    If MImpPar.Importo1 > 0 Then
                        myriga(47) = Format(MImpPar.IMPORTO1, "#,##0.00")
                    End If
                    If MImpPar.Importo2 > 0 Then
                        myriga(47) = myriga(47) & " - " & Format(MImpPar.Importo2, "#,##0.00")
                    End If
                    If MImpPar.Importo3 > 0 Then
                        myriga(47) = myriga(47) & " - " & Format(MImpPar.Importo3, "#,##0.00")
                    End If
                    If MImpPar.Importo4 > 0 Then
                        myriga(47) = myriga(47) & " - " & Format(MImpPar.Importo4, "#,##0.00")
                    End If

                    MImpPar.IMPORTO1 = 0
                    MImpPar.IMPORTO2 = 0
                    MImpPar.IMPORTO3 = 0
                    MImpPar.IMPORTO4 = 0

                    MImpPar.CENTROSERVIZIO = Cmb_CServ.SelectedValue
                    MImpPar.CODICEOSPITE = campodb(myPOSTreader.Item("CodiceOspite"))
                    MImpPar.UltimaData(Session("DC_OSPITE"), MImpOsp.CODICEOSPITE, MImpOsp.CENTROSERVIZIO, 2)

                    If MImpPar.IMPORTO1 > 0 Then
                        If myriga(47) <> "" Then
                            myriga(47) = myriga(47) & " - "
                        End If
                        myriga(47) = myriga(47) & Format(MImpPar.IMPORTO1, "#,##0.00")
                    End If
                    If MImpPar.IMPORTO2 > 0 Then
                        myriga(47) = myriga(47) & " - " & Format(MImpPar.IMPORTO2, "#,##0.00")
                    End If
                    If MImpPar.IMPORTO3 > 0 Then
                        myriga(47) = myriga(47) & " - " & Format(MImpPar.IMPORTO3, "#,##0.00")
                    End If
                    If MImpPar.IMPORTO4 > 0 Then
                        myriga(47) = myriga(47) & " - " & Format(MImpPar.IMPORTO4, "#,##0.00")
                    End If

                    MImpPar.IMPORTO1 = 0
                    MImpPar.IMPORTO2 = 0
                    MImpPar.IMPORTO3 = 0
                    MImpPar.IMPORTO4 = 0

                    MImpPar.CENTROSERVIZIO = Cmb_CServ.SelectedValue
                    MImpPar.CODICEOSPITE = campodb(myPOSTreader.Item("CodiceOspite"))
                    MImpPar.UltimaData(Session("DC_OSPITE"), MImpOsp.CODICEOSPITE, MImpOsp.CENTROSERVIZIO, 3)

                    If MImpPar.IMPORTO1 > 0 Then
                        If myriga(47) <> "" Then
                            myriga(47) = myriga(47) & " - "
                        End If
                        myriga(47) = myriga(47) & Format(MImpPar.IMPORTO1, "#,##0.00")
                    End If
                    If MImpPar.IMPORTO2 > 0 Then
                        myriga(47) = myriga(47) & " - " & Format(MImpPar.IMPORTO2, "#,##0.00")
                    End If
                    If MImpPar.IMPORTO3 > 0 Then
                        myriga(47) = myriga(47) & " - " & Format(MImpPar.IMPORTO3, "#,##0.00")
                    End If
                    If MImpPar.IMPORTO4 > 0 Then
                        myriga(47) = myriga(47) & " - " & Format(MImpPar.IMPORTO4, "#,##0.00")
                    End If


                    MImpPar.IMPORTO1 = 0
                    MImpPar.IMPORTO2 = 0
                    MImpPar.IMPORTO3 = 0
                    MImpPar.IMPORTO4 = 0

                    MImpPar.CENTROSERVIZIO = Cmb_CServ.SelectedValue
                    MImpPar.CODICEOSPITE = campodb(myPOSTreader.Item("CodiceOspite"))
                    MImpPar.UltimaData(Session("DC_OSPITE"), MImpOsp.CODICEOSPITE, MImpOsp.CENTROSERVIZIO, 4)

                    If MImpPar.IMPORTO1 > 0 Then
                        If myriga(47) <> "" Then
                            myriga(47) = myriga(47) & " - "
                        End If
                        myriga(47) = myriga(47) & Format(MImpPar.IMPORTO1, "#,##0.00")
                    End If
                    If MImpPar.IMPORTO2 > 0 Then
                        myriga(47) = myriga(47) & " - " & Format(MImpPar.IMPORTO2, "#,##0.00")
                    End If
                    If MImpPar.IMPORTO3 > 0 Then
                        myriga(47) = myriga(47) & " - " & Format(MImpPar.IMPORTO3, "#,##0.00")
                    End If
                    If MImpPar.IMPORTO4 > 0 Then
                        myriga(47) = myriga(47) & " - " & Format(MImpPar.IMPORTO4, "#,##0.00")
                    End If


                    MImpPar.IMPORTO1 = 0
                    MImpPar.IMPORTO2 = 0
                    MImpPar.IMPORTO3 = 0
                    MImpPar.IMPORTO4 = 0

                    MImpPar.CENTROSERVIZIO = Cmb_CServ.SelectedValue
                    MImpPar.CODICEOSPITE = campodb(myPOSTreader.Item("CodiceOspite"))
                    MImpPar.UltimaData(Session("DC_OSPITE"), MImpOsp.CODICEOSPITE, MImpOsp.CENTROSERVIZIO, 5)

                    If MImpPar.IMPORTO1 > 0 Then
                        If myriga(47) <> "" Then
                            myriga(47) = myriga(47) & " - "
                        End If
                        myriga(47) = myriga(47) & Format(MImpPar.IMPORTO1, "#,##0.00")
                    End If
                    If MImpPar.IMPORTO2 > 0 Then
                        myriga(47) = myriga(47) & " - " & Format(MImpPar.IMPORTO2, "#,##0.00")
                    End If
                    If MImpPar.IMPORTO3 > 0 Then
                        myriga(47) = myriga(47) & " - " & Format(MImpPar.IMPORTO3, "#,##0.00")
                    End If
                    If MImpPar.IMPORTO4 > 0 Then
                        myriga(47) = myriga(47) & " - " & Format(MImpPar.IMPORTO4, "#,##0.00")
                    End If

                    Dim MImpCom As New Cls_ImportoComune

                    MImpCom.CENTROSERVIZIO = Cmb_CServ.SelectedValue
                    MImpCom.CODICEOSPITE = campodb(myPOSTreader.Item("CodiceOspite"))
                    MImpCom.UltimaData(Session("DC_OSPITE"), MImpOsp.CODICEOSPITE, MImpOsp.CENTROSERVIZIO)

                    myriga(48) = ""
                    If MImpCom.Importo1 > 0 Then
                        myriga(48) = Format(MImpCom.Importo1, "#,##0.00")
                    End If
                    If MImpCom.Importo2 > 0 Then
                        myriga(48) = myriga(46) & " - " & Format(MImpCom.Importo2, "#,##0.00")
                    End If
                    If MImpCom.Importo3 > 0 Then
                        myriga(48) = myriga(46) & " - " & Format(MImpCom.Importo3, "#,##0.00")
                    End If
                    If MImpCom.Importo4 > 0 Then
                        myriga(48) = myriga(46) & " - " & Format(MImpCom.Importo4, "#,##0.00")
                    End If

                    myriga(49) = campodb(myPOSTreader.Item("CartellaClinica"))

                    myriga(50) = campodb(myPOSTreader.Item("Note"))


                    Dim KCs As New Cls_DatiOspiteParenteCentroServizio

                    KCs.CentroServizio = Cmb_CServ.SelectedValue
                    KCs.CodiceOspite = campodb(myPOSTreader.Item("CodiceOspite"))
                    KCs.CodiceParente = 0
                    KCs.Leggi(Session("DC_OSPITE"))

                    If KCs.NonCalcoloComune = 1 Then
                        myriga(51) = "Non Calcolare"
                    Else
                        myriga(51) = ""
                    End If



                    ContaOspiti = ContaOspiti + 1


                    MyTable.Rows.Add(myriga)
                End If
            Loop
            myPOSTreader.Close()

            If CentroServizioIndicato = True Then
                Exit For
            End If
        Next
        cn.Close()

        Dim myriga1 As System.Data.DataRow = MyTable.NewRow()

        myriga1(1) = "Estratti"
        myriga1(2) = ContaOspiti

        MyTable.Rows.Add(myriga1)




        GridView1.AutoGenerateColumns = True
        GridView1.DataSource = MyTable
        GridView1.Font.Size = 10
        GridView1.DataBind()


        Session("Estrazione") = MyTable

        For i = 0 To GridView1.Rows.Count - 1

            Dim jk As Integer
            jk = Val(GridView1.Rows(i).Cells(0).Text)

            If jk > 0 Then
                GridView1.Rows(i).Cells(0).Text = "<a href=""#"" onclick=""DialogBox('Anagrafica.aspx?CodiceOspite=" & jk & "&CentroServizio=" & Cmb_CServ.SelectedValue & "');"" >" & GridView1.Rows(i).Cells(0).Text & "</a>"
            End If
        Next

    End Sub


    Private Sub FattureNelPeriodo(ByVal Cserv As String, ByVal codiceospite As Integer, ByVal datadal As Date, ByVal dataal As Date, ByRef Documenti As String, ByRef Importo As Double)
        Dim ConnectionString As String = Session("DC_GENERALE")
        Dim cn As OleDbConnection
        Dim MySql As String

        cn = New Data.OleDb.OleDbConnection(ConnectionString)

        cn.Open()

        Dim CservT As New Cls_CentroServizio

        CservT.CENTROSERVIZIO = Cserv
        CservT.Leggi(Session("DC_OSPITE"), CservT.CENTROSERVIZIO)


        Documenti = ""
        Importo = 0


        MySql = "Select * from MovimentiContabilitesta where annoprotocollo > 0 and DataRegistrazione >= ? And DataRegistrazione <= ? and (select count(*) from MovimentiContabiliRiga where Numero = NumeroRegistrazione And RigaDaCausale =1 And MastroPartita = ? and ContoPartita = ? and (SottocontoPartita >= ? And SottocontoPartita <= ? )) > 0 "
        Dim cmd As New OleDbCommand()

        cmd.Parameters.AddWithValue("@DataRegistrazione", datadal)
        cmd.Parameters.AddWithValue("@DataRegistrazione", dataal)
        cmd.Parameters.AddWithValue("@Mastro", CservT.MASTRO)
        cmd.Parameters.AddWithValue("@CONTO", CservT.CONTO)
        cmd.Parameters.AddWithValue("@Sottoconto", codiceospite * 100)
        cmd.Parameters.AddWithValue("@Sottoconto", (codiceospite * 100) + 90)
        cmd.CommandText = MySql
        cmd.Connection = cn
        Dim myPOSTreader As OleDbDataReader = cmd.ExecuteReader()
        Do While myPOSTreader.Read
            Dim Registrazione As New Cls_MovimentoContabile

            Registrazione.NumeroRegistrazione = campodb(myPOSTreader.Item("NumeroRegistrazione"))
            Registrazione.Leggi(Session("DC_GENERALE"), Registrazione.NumeroRegistrazione)

            Dim Causale As New Cls_CausaleContabile

            Causale.Codice = Registrazione.CausaleContabile
            Causale.Leggi(Session("DC_TABELLE"), Causale.Codice)

            If Causale.TipoDocumento = "NC" Then
                Importo = Importo + Registrazione.ImportoDocumento(Session("DC_TABELLE"))
                Documenti = Documenti & Registrazione.NumeroProtocollo & "/" & Registrazione.AnnoProtocollo & " NC" & " - "
            Else
                Importo = Importo + Registrazione.ImportoDocumento(Session("DC_TABELLE"))
                Documenti = Documenti & Registrazione.NumeroProtocollo & "/" & Registrazione.AnnoProtocollo & " FT" & " - "
            End If


        Loop

        cn.Close()

    End Sub

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load

        Call EseguiJS()

        If Page.IsPostBack = True Then Exit Sub


        Dim K1 As New Cls_SqlString

        Dim Barra As New Cls_BarraSenior

        If Not IsNothing(Session("RicercaAnagraficaSQLString")) Then
            K1 = Session("RicercaAnagraficaSQLString")
        End If

        Lbl_BarraSenior.Text = Barra.CodiceBarra(Application("SENIOR"), Session("UTENTE"), Page, K1)



        Dim kVilla As New Cls_TabelleDescrittiveOspitiAccessori


        kVilla.UpDateDropBox(Session("DC_OSPITIACCESSORI"), "VIL", DD_Struttura)
        If DD_Struttura.Items.Count = 1 Then
            Call AggiornaCServ()
        End If


        Lbl_Utente.Text = Session("UTENTE")


        Txt_DataAl.Text = Format(Now, "dd/MM/yyyy")
        Txt_DataDal.Text = Format(Now, "dd/MM/yyyy")

        Lbl_Utente.Text = Session("UTENTE")

        Call EseguiJS()
    End Sub



    Protected Sub ImageButton3_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles ImageButton3.Click
        If Not IsDate(Txt_DataDal.Text) Then
            REM ClientScript.RegisterClientScriptBlock(Me.GetType(), "Errore", "VisualizzaErrore('Data dal non corretta');", True)
            ScriptManager.RegisterStartupScript(Me, Me.GetType(), "visualizzaA", "VisualizzaErrore('Data dal non corretta');", True)
            Exit Sub
        End If
        If Not IsDate(Txt_DataAl.Text) Then
            REM ClientScript.RegisterClientScriptBlock(Me.GetType(), "Errore", "VisualizzaErrore('Data al non corretta');", True)
            ScriptManager.RegisterStartupScript(Me, Me.GetType(), "visualizzaA", "VisualizzaErrore('Data al non corretta');", True)
            Exit Sub
        End If
        If (Cmb_CServ.SelectedValue = "" And Cmb_CServ.Items.Count = 0) And DD_Struttura.SelectedValue = "" Then
            REM ClientScript.RegisterClientScriptBlock(Me.GetType(), "Errore", "VisualizzaErrore('Specificare centro servizio');", True)
            ScriptManager.RegisterStartupScript(Me, Me.GetType(), "visualizzaA", "VisualizzaErrore('Specificare centro servizio');", True)
            Exit Sub
        End If


        Call ProspettoPresenze(True)


        If Session("DC_OSPITE").ToString.IndexOf("Ronzoni") >= 0 Then
            Chk_StampaEtichette.Visible = True
        End If
    End Sub

    Protected Sub ImageButton4_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles ImageButton4.Click
        If Not IsDate(Txt_DataDal.Text) Then
            ClientScript.RegisterClientScriptBlock(Me.GetType(), "Errore", "VisualizzaErrore('Data dal non corretta');", True)
            Exit Sub
        End If
        If Not IsDate(Txt_DataAl.Text) Then
            ClientScript.RegisterClientScriptBlock(Me.GetType(), "Errore", "VisualizzaErrore('Data al non corretta');", True)
            Exit Sub
        End If
        If (Cmb_CServ.SelectedValue = "" And Cmb_CServ.Items.Count = 0) And DD_Struttura.SelectedValue = "" Then
            ClientScript.RegisterClientScriptBlock(Me.GetType(), "Errore", "VisualizzaErrore('Specificare centro servizio');", True)
            Exit Sub
        End If

        Call ProspettoPresenze(True)


        If MyTable.Rows.Count > 1 Then
            Response.Clear()
            Response.AddHeader("content-disposition", "attachment;filename=ProspettoPresenze.xls")
            Response.Charset = String.Empty
            Response.Cache.SetCacheability(HttpCacheability.NoCache)
            Response.ContentType = "application/vnd.xls"
            Dim stringWrite As New System.IO.StringWriter
            Dim htmlWrite As New HtmlTextWriter(stringWrite)
            'GridView1.RenderControl(htmlWrite)


            form1.Controls.Clear()
            form1.Controls.Add(GridView1)

            form1.RenderControl(htmlWrite)

            Response.Write(stringWrite.ToString())
            Response.End()
        End If
    End Sub

    Protected Sub Btn_Esci_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles Btn_Esci.Click
        Response.Redirect("Menu_Visualizzazioni.aspx")
    End Sub

    Private Sub AggiornaCServ()
        Dim kCsrv As New Cls_CentroServizio

        If DD_Struttura.SelectedValue = "" Then
            kCsrv.UpDateDropBox(Session("DC_OSPITE"), Cmb_CServ)
        Else
            kCsrv.UpDateDropBoxStruttura(Session("DC_OSPITE"), Cmb_CServ, DD_Struttura.SelectedValue)
        End If

    End Sub



    Protected Sub DD_Struttura_TextChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles DD_Struttura.TextChanged
        AggiornaCServ()
        Call EseguiJS()
    End Sub

    Private Sub EseguiJS()
        Dim MyJs As String
        MyJs = "$(document).ready(function() {"

        MyJs = MyJs & "var els = document.getElementsByTagName(""*"");"

        MyJs = MyJs & "for (var i=0;i<els.length;i++)"
        MyJs = MyJs & "if ( els[i].id ) { "
        MyJs = MyJs & " var appoggio =els[i].id; "
        MyJs = MyJs & " if (appoggio.match('Txt_Data')!= null) {  "
        MyJs = MyJs & " $(els[i]).mask(""99/99/9999"");"

        MyJs = MyJs & " $(els[i]).datepicker({ changeMonth: true,changeYear: true,  yearRange: ""1990:" & Year(Now) + 5 & """},$.datepicker.regional[""it""]);"

        MyJs = MyJs & "    }"


        MyJs = MyJs & "} "

        MyJs = MyJs & "if (window.innerHeight>0) { $(""#BarraLaterale"").css(""height"",(window.innerHeight - 94) + ""px""); } else"
        MyJs = MyJs & "{ $(""#BarraLaterale"").css(""height"",(document.documentElement.offsetHeight - 94) + ""px"");  }"

        MyJs = MyJs & "});"

        'MyJs = "$(document).ready(function() { $('.myClass').keypress(function() { handleEnter($('.myClass').val(), true, true); } );     $('#" & Txt_ImportoPagato.ClientID & "').blur(function() { var ap = formatNumber ($('#" & Txt_ImportoPagato.ClientID & "').val(),2); $('#" & Txt_ImportoPagato.ClientID & "').val(ap); }); });"
        ScriptManager.RegisterStartupScript(Me, Me.GetType(), "visualizzaRitIm", MyJs, True)
    End Sub

    Protected Sub Btn_Stampa_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles Btn_Stampa.Click
        If Chk_StampaEtichette.Visible = True Then

            Call CreaEtichette()
            Exit Sub
        End If

        If Not IsDate(Txt_DataDal.Text) Then
            REM ClientScript.RegisterClientScriptBlock(Me.GetType(), "Errore", "VisualizzaErrore('Data dal non corretta');", True)
            ScriptManager.RegisterStartupScript(Me, Me.GetType(), "visualizzaA", "VisualizzaErrore('Data dal non corretta');", True)
            Exit Sub
        End If
        If Not IsDate(Txt_DataAl.Text) Then
            REM ClientScript.RegisterClientScriptBlock(Me.GetType(), "Errore", "VisualizzaErrore('Data al non corretta');", True)
            ScriptManager.RegisterStartupScript(Me, Me.GetType(), "visualizzaA", "VisualizzaErrore('Data al non corretta');", True)
            Exit Sub
        End If
        If (Cmb_CServ.SelectedValue = "" And Cmb_CServ.Items.Count = 0) And DD_Struttura.SelectedValue = "" Then
            REM ClientScript.RegisterClientScriptBlock(Me.GetType(), "Errore", "VisualizzaErrore('Specificare centro servizio');", True)
            ScriptManager.RegisterStartupScript(Me, Me.GetType(), "visualizzaA", "VisualizzaErrore('Specificare centro servizio');", True)
            Exit Sub
        End If


        Call ProspettoPresenze(True)

        MyTable.DefaultView.Sort = "Nome Asc"

        Session("GrigliaSoloStampa") = MyTable

        Session("ParametriSoloStampa") = ""

        ScriptManager.RegisterClientScriptBlock(Me, Me.GetType(), "Stampa", "openPopUp('GrigliaSoloStampa.aspx','Stampe','width=800,height=600');", True)

    End Sub

    Private Sub CreaEtichette()
        MyTable = Session("Estrazione")

        Dim Scheda As New StampeOspiti
        Dim I As Integer


        Dim ConnectionString As String = Session("DC_OSPITE")
        Dim cn As OleDbConnection
        
        cn = New Data.OleDb.OleDbConnection(ConnectionString)

        cn.Open()

        For I = 0 To MyTable.Rows.Count - 1
            Dim CodiceOspite As Integer

            CodiceOspite = Val(MyTable.Rows(I).Item(0).ToString)

            If CodiceOspite > 0 Then
                Dim MySql As String = ""
                MySql = "Select * from AnagraficaComune Where CodiceOspite = ? And CodiceParente  > 0 "
                Dim cmd As New OleDbCommand()
                Dim Entrato As Boolean = False

                cmd.Parameters.AddWithValue("@CodiceOspite", CodiceOspite)

                cmd.CommandText = MySql
                cmd.Connection = cn
                Dim myPOSTreader As OleDbDataReader = cmd.ExecuteReader()
                Do While myPOSTreader.Read

                    Dim MyRs As System.Data.DataRow = Scheda.Tables("ANAGRAFICA").NewRow
                    MyRs.Item("Nome") = MyTable.Rows(I).Item("Nome").ToString

                    MyRs.Item("PARENTENOME") = campodb(myPOSTreader.Item("Nome"))
                    Scheda.Tables("ANAGRAFICA").Rows.Add(MyRs)
                    Entrato = True
                Loop
                myPOSTreader.Close()
                If Entrato = False Then

                    Dim MyRs As System.Data.DataRow = Scheda.Tables("ANAGRAFICA").NewRow
                    MyRs.Item("Nome") = MyTable.Rows(I).Item("Nome").ToString

                    MyRs.Item("PARENTENOME") = ""
                    Scheda.Tables("ANAGRAFICA").Rows.Add(MyRs)
                End If
            End If
        Next

        Session("stampa") = Scheda


        cn.Close()

        ScriptManager.RegisterClientScriptBlock(Me, Me.GetType(), "Stampa1", "openPopUp('Stampa_ReportXSD.aspx?REPORT=STAMPAETICHETTE&EXPORT=PDF','Stampe1','width=800,height=600');", True)

    End Sub
End Class
