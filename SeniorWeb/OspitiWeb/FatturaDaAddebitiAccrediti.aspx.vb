﻿Imports System.Web.Hosting
Imports System.Data.OleDb

Partial Class OspitiWeb_FatturaDaAddebitiAccrediti
    Inherits System.Web.UI.Page
    Dim Tabella As New System.Data.DataTable("tabella")

    Function campodb(ByVal oggetto As Object) As String
        If IsDBNull(oggetto) Then
            Return ""
        Else
            Return oggetto
        End If

    End Function
    Public Sub AssegnaBolloVirtuale(ByVal SalvaId As Integer)
        Dim Cn As New OleDbConnection

        Cn.ConnectionString = Session("DC_GENERALE")
        Cn.Open()

        Dim M As New OleDbCommand

        M.Parameters.Clear()
        M.Connection = Cn
        M.CommandText = "UPDATE Temp_MovimentiContabiliTesta Set BolloVirtuale = 1 Where NumeroRegistrazione = ?"
        M.Parameters.AddWithValue("NumeroRegistrazione", SalvaId)
        M.ExecuteNonQuery()

        Cn.Close()
    End Sub
    Protected Sub Btn_Visualizza_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles Btn_Visualizza.Click
        If DD_CentroServizio.SelectedValue = "" Then
            ClientScript.RegisterClientScriptBlock(Me.GetType(), "Errore", "VisualizzaErrore('Specifica centroservizio');", True)
            Call EseguiJS()
            Exit Sub
        End If

        If Val(Txt_Anno.Text) < 1990 Then
            ClientScript.RegisterClientScriptBlock(Me.GetType(), "Errore", "VisualizzaErrore('Specificare Anno');", True)
            Call EseguiJS()
            Exit Sub
        End If

        If Not IsDate(Txt_DataRegistrazione.Text) Then
            ClientScript.RegisterClientScriptBlock(Me.GetType(), "Errore", "VisualizzaErrore('Data validita formalmente errata');", True)
            Call EseguiJS()
            Exit Sub
        End If

        Dim VerificaElaborazione As New Cls_Parametri


        If VerificaElaborazione.InElaborazione(Session("DC_OSPITE")) Then
            ClientScript.RegisterClientScriptBlock(Me.GetType(), "Errore", "VisualizzaErrore('Elaborazione in esecuzione non posso procedere');", True)
            Exit Sub
        End If


        Dim ParametriGenerale As New Cls_ParametriGenerale

        If ParametriGenerale.InElaborazione(Session("DC_GENERALE")) Then
            ClientScript.RegisterClientScriptBlock(Me.GetType(), "Errore", "alert('Elaborazione in esecuzione non posso procedere');", True)
            Exit Sub
        End If




        Dim cn As OleDbConnection


        cn = New Data.OleDb.OleDbConnection(Session("DC_OSPITE"))

        cn.Open()



        Dim cmdDeleteNotifiche As New OleDbCommand()
        cmdDeleteNotifiche.CommandText = ("Delete  from notifiche ")
        cmdDeleteNotifiche.Connection = cn
        cmdDeleteNotifiche.ExecuteNonQuery()

        Dim cmd As New OleDbCommand()
        Dim MySql As String
        Dim TRig As String
        Dim Fig As String

        Tabella.Clear()
        Tabella.Columns.Clear()
        Tabella.Columns.Add("CodiceOspite", GetType(Integer))
        Tabella.Columns.Add("NomeOspite", GetType(String))
        Tabella.Columns.Add("Data", GetType(String))
        Tabella.Columns.Add("TipoMovimento", GetType(String))
        Tabella.Columns.Add("Riferimento", GetType(String))
        Tabella.Columns.Add("FiguradiRiferimento", GetType(String))
        Tabella.Columns.Add("Importo", GetType(String))
        Tabella.Columns.Add("Retta", GetType(String))
        Tabella.Columns.Add("PeriodoCompetenza", GetType(String))
        Tabella.Columns.Add("Descrizione", GetType(String))
        Tabella.Columns.Add("Codice", GetType(String))
        Tabella.Columns.Add("Id", GetType(Long))

        Dim Condizione As String = ""

        ' Tolto il controllo e disabilitate per problema flag elaborato dopo conferma (stesso ospite due addebiti di tipo diverso uno da fatturare l'altro no)
        'If DD_TipoAddebito.SelectedValue <> "" Then
        ' Condizione = Condizione & " And CodiceIva = '" & DD_TipoAddebito.SelectedValue & "' "
        ' End If

        If Chk_AllAddebiti.Checked = False Then
            If DD_Tipo.SelectedValue = "O" Then
                MySql = "Select * From ADDACR Where Data >= ? And Data <= ? And Riferimento = ? And CentroServizio = ? " & Condizione & " Order By CentroServizio,(select top 1 nome From AnagraficaComune Where AnagraficaComune.CodiceOspite = ADDACR.CodiceOspite And CodiceParente = 0), CodiceOspite"
            End If
            If DD_Tipo.SelectedValue = "P" Then
                MySql = "Select * From ADDACR Where Data >= ? And Data <= ? And Riferimento = ? And CentroServizio = ? " & Condizione & "Order By CentroServizio,(select top 1 nome From AnagraficaComune Where AnagraficaComune.CodiceOspite = ADDACR.CodiceOspite And CodiceParente = 0),CodiceOspite"
            End If
            If DD_Tipo.SelectedValue = "R" Then
                MySql = "Select * From ADDACR Where Data >= ? And Data <= ? And Riferimento = ? And CentroServizio = ? " & Condizione & "Order By CentroServizio,Regione"
            End If
            If DD_Tipo.SelectedValue = "C" Then
                MySql = "Select * From ADDACR Where Data >= ? And Data <= ? And Riferimento = ? And CentroServizio = ? " & Condizione & "Order By CentroServizio,Provincia,Comune"
            End If
            If DD_Tipo.SelectedValue = "J" Then
                MySql = "Select * From ADDACR Where Data >= ? And Data <= ? And Riferimento = ? And CentroServizio = ? " & Condizione & "Order By CentroServizio,Provincia,Comune"
            End If
        Else
            If DD_Tipo.SelectedValue = "O" Then
                MySql = "Select * From ADDACR Where Data >= ? And Data <= ? And Riferimento = ? And CentroServizio = ? And (Elaborato = 0 or Elaborato  is null)  " & Condizione & "Order By CentroServizio,CodiceOspite"
            End If
            If DD_Tipo.SelectedValue = "P" Then
                MySql = "Select * From ADDACR Where Data >= ? And Data <= ? And Riferimento = ? And CentroServizio = ? And (Elaborato = 0 or Elaborato  is null) " & Condizione & "Order By CentroServizio,CodiceOspite"
            End If
            If DD_Tipo.SelectedValue = "R" Then
                MySql = "Select * From ADDACR Where Data >= ? And Data <= ? And Riferimento = ? And CentroServizio = ? And (Elaborato = 0 or Elaborato  is null) " & Condizione & "Order By CentroServizio,Regione"
            End If
            If DD_Tipo.SelectedValue = "C" Then
                MySql = "Select * From ADDACR Where Data >= ? And Data <= ? And Riferimento = ? And CentroServizio = ? And (Elaborato = 0 or Elaborato  is null) " & Condizione & "Order By CentroServizio,Provincia,Comune"
            End If
            If DD_Tipo.SelectedValue = "J" Then
                MySql = "Select * From ADDACR Where Data >= ? And Data <= ? And Riferimento = ? And CentroServizio = ? And (Elaborato = 0 or Elaborato  is null) " & Condizione & "Order By CentroServizio,Provincia,Comune"
            End If
        End If


        cmd.CommandText = MySql
        cmd.Connection = cn
        cmd.Parameters.AddWithValue("@DATA1", DateSerial(Txt_Anno.Text, Dd_Mese.SelectedValue, 1))
        cmd.Parameters.AddWithValue("@DATA2", DateSerial(Txt_Anno.Text, Dd_Mese.SelectedValue, GiorniMese(Dd_Mese.SelectedValue, Txt_Anno.Text)))
        cmd.Parameters.AddWithValue("@RIFERIMENTO", DD_Tipo.SelectedValue)
        cmd.Parameters.AddWithValue("@CENTROSERVIZIO", DD_CentroServizio.SelectedValue)


        Dim myPOSTreader As OleDbDataReader = cmd.ExecuteReader()
        Do While myPOSTreader.Read

            Dim CodiceOSpite As Integer = 0
            Dim XAna As New ClsOspite


            CodiceOSpite = Val(campodb(myPOSTreader.Item("CodiceOspite")))

            XAna.Leggi(Session("DC_OSPITE"), CodiceOSpite)

            Dim myriga As System.Data.DataRow = Tabella.NewRow()

            myriga(0) = CodiceOSpite
            myriga(1) = XAna.Nome
            myriga(2) = campodb(myPOSTreader.Item("DATA"))
            myriga(3) = campodb(myPOSTreader.Item("TIPOMOV"))


            TRig = ""
            Fig = ""
            If campodb(myPOSTreader.Item("Riferimento")) = "O" Then
                TRig = "OSPITE"
                Fig = ""
            End If
            If campodb(myPOSTreader.Item("Riferimento")) = "P" Then
                TRig = "PARENTE"
                'Fig = CampoParente(MoveFromDb(MyRs!CodiceOspite), MoveFromDb(MyRs!Parente), "NOME")
                Fig = Val(campodb(myPOSTreader.Item("Parente")))
            End If
            If campodb(myPOSTreader.Item("Riferimento")) = "R" Then
                TRig = "REGIONE"
                Fig = campodb(myPOSTreader.Item("Regione"))  '& Space(4 - Len(MoveFromDb(MyRs!Regione))) & CampoRegione(MoveFromDb(MyRs!Regione), "NOME")
            End If
            If campodb(myPOSTreader.Item("Riferimento")) = "C" Then
                TRig = "COMUNE"
                Fig = campodb(myPOSTreader.Item("Provincia")) & Space(3 - Len(campodb(myPOSTreader.Item("Provincia")))) & campodb(myPOSTreader.Item("Comune")) & Space(3 - Len(campodb(myPOSTreader.Item("Comune"))))
            End If
            If campodb(myPOSTreader.Item("Riferimento")) = "J" Then
                TRig = "JOLLY"
                Fig = campodb(myPOSTreader.Item("Provincia")) & Space(3 - Len(campodb(myPOSTreader.Item("Provincia")))) & campodb(myPOSTreader.Item("Comune")) & Space(3 - Len(campodb(myPOSTreader.Item("Comune"))))
            End If
            myriga(4) = TRig
            myriga(5) = Fig
            myriga(6) = campodb(myPOSTreader.Item("Importo"))
            myriga(7) = campodb(myPOSTreader.Item("Retta"))
            myriga(8) = campodb(myPOSTreader.Item("MESECOMPETENZA")) & "/" & campodb(myPOSTreader.Item("ANNOCOMPETENZA"))
            myriga(9) = campodb(myPOSTreader.Item("Descrizione"))
            myriga(10) = campodb(myPOSTreader.Item("CodiceIVA"))
            myriga(11) = Val(campodb(myPOSTreader.Item("ID")))


            Tabella.Rows.Add(myriga)
        Loop

        cn.Close()

        ViewState("SalvaTabella") = Tabella

        Grd_AggiornaRette.AutoGenerateColumns = False
        Grd_AggiornaRette.DataSource = Tabella
        Grd_AggiornaRette.Font.Size = 10
        Grd_AggiornaRette.DataBind()
        Call EseguiJS()

        lblFC.Visible = False
        lblFCDes.Visible = False
        DD_Ente.Visible = False
        If DD_Tipo.SelectedValue = "R" Then
            lblFC.Visible = True
            lblFCDes.Visible = True
            DD_Ente.Visible = True
            Dim Usl As New ClsUSL

            Usl.UpDateDropBoxCodice(Session("DC_OSPITE"), DD_Ente)
        End If

        If DD_Tipo.SelectedValue = "C" Or DD_Tipo.SelectedValue = "J" Then
            lblFC.Visible = True
            lblFCDes.Visible = True
            DD_Ente.Visible = True
            Dim Usl As New ClsComune

            Usl.UpDateDropBox(Session("DC_OSPITE"), DD_Ente)
        End If

    End Sub

    Public Function GiorniMese(ByVal Mese As Byte, ByVal Anno As Integer) As Byte
        If Mese = 1 Or Mese = 3 Or Mese = 5 Or Mese = 7 Or Mese = 8 Or _
           Mese = 10 Or Mese = 12 Then
            GiorniMese = 31
        Else
            If Mese <> 2 Then
                GiorniMese = 30
            Else
                If Day(DateSerial(Anno, Mese, 29)) = 29 Then
                    GiorniMese = 29
                Else
                    GiorniMese = 28
                End If
            End If
        End If
    End Function

    Private Sub EseguiJS()
        Dim MyJs As String
        MyJs = "$(document).ready(function() {"

        MyJs = MyJs & "var els = document.getElementsByTagName(""*"");"

        MyJs = MyJs & "for (var i=0;i<els.length;i++)"
        MyJs = MyJs & "if ( els[i].id ) { "
        MyJs = MyJs & " var appoggio =els[i].id; "
        MyJs = MyJs & " if (appoggio.match('Txt_Data')!= null) {  "
        MyJs = MyJs & " $(els[i]).mask(""99/99/9999"");"

        MyJs = MyJs & " $(els[i]).datepicker({ changeMonth: true,changeYear: true,  yearRange: ""1990:" & Year(Now) + 5 & """},$.datepicker.regional[""it""]);"

        MyJs = MyJs & "    }"

        MyJs = MyJs & " if (appoggio.match('Txt_DataRegistrazione')!= null) {  "
        MyJs = MyJs & " $(els[i]).mask(""99/99/9999"");"

        MyJs = MyJs & " $(els[i]).datepicker({ changeMonth: true,changeYear: true,  yearRange: ""1990:" & Year(Now) + 5 & """},$.datepicker.regional[""it""]);"

        MyJs = MyJs & "    }"



        MyJs = MyJs & " if ((appoggio.match('TxtImporto')!= null) ) {  "
        MyJs = MyJs & " $(els[i]).keypress(function() { ForceNumericInput($(this).val(), true, true); } ); "
        MyJs = MyJs & " $(els[i]).blur(function() { var ap = formatNumber($(this).val(),2); $(this).val(ap); });"
        MyJs = MyJs & "    }"


        MyJs = MyJs & "} "
        MyJs = MyJs & "});"

        'MyJs = "$(document).ready(function() { $('.myClass').keypress(function() { handleEnter($('.myClass').val(), true, true); } );     $('#" & Txt_ImportoPagato.ClientID & "').blur(function() { var ap = formatNumber ($('#" & Txt_ImportoPagato.ClientID & "').val(),2); $('#" & Txt_ImportoPagato.ClientID & "').val(ap); }); });"
        ScriptManager.RegisterStartupScript(Me, Me.GetType(), "visualizzaRitIm", MyJs, True)
    End Sub

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Trim(Session("UTENTE")) = "" Then
            Response.Redirect("../Login.aspx")
            Exit Sub
        End If

        Session("CampoErrori") = ""

        If Page.IsPostBack = True Then
            Exit Sub
        End If



        Call EseguiJS()


        Dim K1 As New Cls_SqlString

        Dim Barra As New Cls_BarraSenior

        If Not IsNothing(Session("RicercaAnagraficaSQLString")) Then
            Try
                K1 = Session("RicercaAnagraficaSQLString")
            Catch ex As Exception

            End Try
        End If

        Lbl_BarraSenior.Text = Barra.CodiceBarra(Application("SENIOR"), Session("UTENTE"), Page, K1)

        Dim kVilla As New Cls_TabelleDescrittiveOspitiAccessori


        kVilla.UpDateDropBox(Session("DC_OSPITIACCESSORI"), "VIL", DD_Struttura)
        If DD_Struttura.Items.Count = 1 Then
            Call AggiornaCServ()
        End If


        Dim Pagam As New Cls_Parametri

        Pagam.LeggiParametri(Session("DC_OSPITE"))

        Txt_Anno.Text = Pagam.AnnoFatturazione
        Dd_Mese.SelectedValue = Pagam.MeseFatturazione

        Txt_DataRegistrazione.Text = Format(Now, "dd/MM/yyyy")

        Session("CampoErrori") = ""


        Dim MTad As New Cls_Addebito

        MTad.UpDateDropBox(Session("DC_OSPITE"), DD_TipoAddebito)

        Dim m As New Cls_Notifica


        m.Delete(Session("DC_OSPITE"))


    End Sub

    Private Sub CreaDocOspiti()
        Dim i As Integer
        Dim x As Integer
        Dim Centroservizio As String = ""
        Dim CodiceOspite As Long = 0
        Dim OldCodiceOspite As Long = 0
        Dim MaxAddebito As Long = 0
        Dim MaxAccredito As Long = 0

        Dim CodiceIVA As String

        Dim BolloImpo As Double
        Dim Bollo As Integer
        Dim IVABollo As String


        Dim ImportoRetta As Double

        Dim ImportoAddebiti As Double
        Dim ImportoAccrediti As Double
        Dim EM_Retta As New Cls_EmissioneRetta

        EM_Retta.ConnessioneGenerale = Session("DC_GENERALE")
        EM_Retta.ConnessioneOspiti = Session("DC_OSPITE")
        EM_Retta.ConnessioneTabelle = Session("DC_TABELLE")

        EM_Retta.ApriDB()

        EM_Retta.EliminaTemporanei()


        Tabella = ViewState("SalvaTabella")

        Dim Xk As New Cls_CentroServizio


        Xk.Leggi(Session("DC_OSPITE"), DD_CentroServizio.SelectedValue)
        OldCodiceOspite = 0
        CodiceOspite = Tabella.Rows(i).Item(0)
        For i = 0 To Tabella.Rows.Count - 1

            Centroservizio = DD_CentroServizio.SelectedValue
            CodiceOspite = Tabella.Rows(i).Item(0)
            Dim CheckBoxV As CheckBox = DirectCast(Grd_AggiornaRette.Rows(i).FindControl("Chk_Seleziona"), CheckBox)


            If CheckBoxV.Checked = True Then
                If CodiceOspite <> OldCodiceOspite And OldCodiceOspite <> 0 Then
                    Dim xs As New ClsOspite

                    xs.Leggi(Session("DC_OSPITE"), OldCodiceOspite)

                    Dim TipoCserv As New Cls_DatiOspiteParenteCentroServizio

                    TipoCserv.CentroServizio = DD_CentroServizio.SelectedValue
                    TipoCserv.CodiceOspite = xs.CodiceOspite
                    TipoCserv.Leggi(Session("DC_OSPITE"))
                    If TipoCserv.TipoOperazione <> "" Then
                        xs.TIPOOPERAZIONE = TipoCserv.TipoOperazione
                        xs.CODICEIVA = TipoCserv.AliquotaIva
                    End If


                    If xs.TIPOOPERAZIONE.Trim <> "" Then
                        Dim TipoOperasazione As New Cls_TipoOperazione

                        TipoOperasazione.Leggi(Session("DC_OSPITE"), xs.TIPOOPERAZIONE)
                        EM_Retta.GlbTipoOperazione = xs.TIPOOPERAZIONE

                        CodiceIVA = xs.CODICEIVA

                        Dim AliquotaIva As New Cls_IVA
                        Dim AliquotaBollo As New Cls_IVA

                        AliquotaIva.Leggi(Session("DC_TABELLE"), CodiceIVA)

                        If TipoOperasazione.SoggettaABollo = "" Or (TipoOperasazione.SoggettaABollo = "F" And ImportoRetta < 0) Then
                            BolloImpo = 0
                            Bollo = 0
                            IVABollo = ""
                        Else
                            Dim KBollo As New Cls_bolli

                            KBollo.Leggi(Session("DC_TABELLE"), Txt_DataRegistrazione.Text)
                            BolloImpo = KBollo.ImportoBollo
                            Bollo = KBollo.ImportoBollo
                            IVABollo = KBollo.CodiceIVA


                            AliquotaBollo.Leggi(Session("DC_TABELLE"), IVABollo)

                        End If



                        If xs.Compensazione = "D" Then
                            Dim CausaleRetta As String
                            Dim ValutazioneRetta As Double = 0
                            Dim TipoOperazione = TipoOperasazione.Codice
                            Dim AliquotaIvaBollo As Double = 0
                            Dim indice2 As Integer


                            ValutazioneRetta = 0
                            For indice2 = 1 To 40
                                If Modulo.MathRound(EM_Retta.Emr_ImpExtra(indice2), 2) > 0 Then
                                    Dim DecExtraFisso As New Cls_TipoExtraFisso

                                    DecExtraFisso.CODICEEXTRA = EM_Retta.Emr_CodiceExtra(indice2)
                                    DecExtraFisso.Leggi(Session("DC_OSPITE"))
                                    If DecExtraFisso.FuoriRetta = 0 Then
                                        ValutazioneRetta = ValutazioneRetta + Modulo.MathRound(EM_Retta.Emr_ImpExtra(indice2), 2)
                                    End If
                                End If
                                If Modulo.MathRound(EM_Retta.Emr_ImpAddebito(indice2), 2) > 0 Then
                                    Dim TipAddebito As New Cls_Addebito

                                    TipAddebito.Codice = EM_Retta.Emr_CodivaAddebito(indice2)
                                    TipAddebito.Leggi(Session("DC_OSPITE"), TipAddebito.Codice)
                                    If TipAddebito.FuoriRetta = 0 Then
                                        ValutazioneRetta = ValutazioneRetta + Modulo.MathRound(EM_Retta.Emr_ImpAddebito(indice2), 2)
                                    End If
                                End If
                                If Modulo.MathRound(EM_Retta.Emr_ImpAccredito(indice2), 2) > 0 Then
                                    If EM_Retta.Emr_AccreditoDaExtra(indice2) = "E" Then
                                        Dim DecExtraFisso As New Cls_TipoExtraFisso

                                        DecExtraFisso.CODICEEXTRA = EM_Retta.Emr_CodivaAccredito(indice2)
                                        DecExtraFisso.Leggi(Session("DC_OSPITE"))
                                        If DecExtraFisso.FuoriRetta = 0 Then
                                            ValutazioneRetta = ValutazioneRetta + Modulo.MathRound(EM_Retta.Emr_ImpAccredito(indice2), 2)
                                        End If
                                    Else
                                        Dim TipAddebito As New Cls_Addebito
                                        TipAddebito.Codice = EM_Retta.Emr_CodivaAccredito(indice2)
                                        TipAddebito.Leggi(Session("DC_OSPITE"), TipAddebito.Codice)
                                        If TipAddebito.FuoriRetta = 0 Then
                                            ValutazioneRetta = ValutazioneRetta - Modulo.MathRound(EM_Retta.Emr_ImpAccredito(indice2), 2)
                                        End If
                                    End If
                                End If
                            Next

                            If ValutazioneRetta < 0 Then
                                CausaleRetta = EM_Retta.CampoOperazione(TipoOperazione, "CAUSALEACCREDITO")
                                If CausaleRetta = "" Then Exit Sub
                                If EM_Retta.CampoOperazione(TipoOperazione, "SoggettaABollo") = "" Or EM_Retta.CampoOperazione(TipoOperazione, "SoggettaABollo") = "F" Then
                                    Bollo = 0
                                    IVABollo = ""
                                    AliquotaIvaBollo = 0
                                Else
                                    Bollo = EM_Retta.ImportoBollo(DateSerial(Txt_Anno.Text, Dd_Mese.SelectedValue, 1), ValutazioneRetta)
                                    IVABollo = EM_Retta.CodiceIVABollo(DateSerial(Txt_Anno.Text, Dd_Mese.SelectedValue, 1))
                                    AliquotaIvaBollo = EM_Retta.DecodificaIva(IVABollo)
                                End If

                                If Not EM_Retta.CreaRegistrazioneContabile(Txt_Anno.Text, Dd_Mese.SelectedValue, DD_CentroServizio.SelectedValue, CausaleRetta, Xk.MASTRO, Xk.CONTO, OldCodiceOspite * 100, 0, 0, 0, Math.Abs(ImportoAddebiti), Math.Abs(ImportoAccrediti), Bollo, 0, AliquotaIva.Aliquota, CodiceIVA, AliquotaBollo.Aliquota, IVABollo, Txt_DataRegistrazione.Text, 0, 0, 0, 0, 0, 0, xs.TIPOOPERAZIONE, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1) Then Exit Sub
                                'If Not EM_Retta.CreaRegistrazioneContabile(Txt_Anno.Text, Dd_Mese.SelectedValue, DD_CentroServizio.SelectedValue, CausaleRetta, Xk.MASTRO, Xk.CONTO, OldCodiceOspite * 100, 0, 0, 0, Math.Abs(ImportoAddebiti), Math.Abs(ImportoAccrediti), Bollo, 0, AliquotaIva.Aliquota, CodiceIVA, AliquotaBollo.Aliquota, IVABollo, Txt_DataRegistrazione.Text, 0, 0, 0, 0, 0, 0, xs.TIPOOPERAZIONE, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1) Then Exit Sub
                            End If
                            If ValutazioneRetta > 0 Then
                                If EM_Retta.CampoOperazione(TipoOperazione, "SoggettaABollo") = "" Then
                                    Bollo = 0
                                    IVABollo = ""
                                    AliquotaIvaBollo = 0
                                Else
                                    Bollo = EM_Retta.ImportoBollo(DateSerial(Txt_Anno.Text, Dd_Mese.SelectedValue, 1), ValutazioneRetta)
                                    IVABollo = EM_Retta.CodiceIVABollo(DateSerial(Txt_Anno.Text, Dd_Mese.SelectedValue, 1))
                                    AliquotaIvaBollo = EM_Retta.DecodificaIva(IVABollo)
                                End If
                                'If Not EM_Retta.CreaRegistrazioneContabile(Txt_Anno.Text, Dd_Mese.SelectedValue, DD_CentroServizio.SelectedValue, , CausaleRetta, Xk.MASTRO, Xk.CONTO, OldCodiceOspite * 100, ImportoRetta, 0, ImportoExtra, ImportoAddebito, ImportoAccredito, Bollo, ImportoAnticipo, AliquotaIva, IVA, AliquotaIvaBollo, IVABollo, DataRegistrazione, MastroAnticipo, ContoAnticipo, SottoContoAnticipo, GiorniPres, MeseRiferimento, AnnoRiferimento, ClOsp.TIPOOPERAZIONE, ImportoRettaAssenza, GiorniAss, ImportoAddebitoFuoriRetta, ImportoMeseSuccessivo, 0, GiorniMeseSuc, 0, 0, 0, ImportoRettaSecondoPeriodo, ImportoRettaAssenzeSecondoPeriodo, GiorniPresSecondoPeriodo, GiorniAssSecondoPeriodo, ImportoSuccessivoP2, GiorniP2, 0) Then Exit Sub
                                If Not EM_Retta.CreaRegistrazioneContabile(Txt_Anno.Text, Dd_Mese.SelectedValue, DD_CentroServizio.SelectedValue, CausaleRetta, Xk.MASTRO, Xk.CONTO, OldCodiceOspite * 100, 0, 0, 0, Math.Abs(ImportoAddebiti), Math.Abs(ImportoAccrediti), Bollo, 0, AliquotaIva.Aliquota, CodiceIVA, AliquotaBollo.Aliquota, IVABollo, Txt_DataRegistrazione.Text, 0, 0, 0, 0, 0, 0, xs.TIPOOPERAZIONE, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1) Then Exit Sub
                            End If

                            ValutazioneRetta = 0

                            For indice2 = 1 To 40
                                If Modulo.MathRound(EM_Retta.Emr_ImpExtra(indice2), 2) > 0 Then
                                    Dim DecExtraFisso As New Cls_TipoExtraFisso

                                    DecExtraFisso.CODICEEXTRA = EM_Retta.Emr_CodiceExtra(indice2)
                                    DecExtraFisso.Leggi(Session("DC_OSPITE"))
                                    If DecExtraFisso.FuoriRetta = 1 Then
                                        ValutazioneRetta = ValutazioneRetta + Modulo.MathRound(EM_Retta.Emr_ImpExtra(indice2), 2)
                                    End If
                                End If
                                If Modulo.MathRound(EM_Retta.Emr_ImpAddebito(indice2), 2) > 0 Then
                                    Dim TipAddebito As New Cls_Addebito

                                    TipAddebito.Codice = EM_Retta.Emr_CodivaAddebito(indice2)
                                    TipAddebito.Leggi(Session("DC_OSPITE"), TipAddebito.Codice)
                                    If TipAddebito.FuoriRetta = 1 Then
                                        ValutazioneRetta = ValutazioneRetta + Modulo.MathRound(EM_Retta.Emr_ImpAddebito(indice2), 2)
                                    End If
                                End If
                                If Modulo.MathRound(EM_Retta.Emr_ImpAccredito(indice2), 2) > 0 Then
                                    If EM_Retta.Emr_AccreditoDaExtra(indice2) = "E" Then
                                        Dim DecExtraFisso As New Cls_TipoExtraFisso

                                        DecExtraFisso.CODICEEXTRA = EM_Retta.Emr_CodivaAccredito(indice2)
                                        DecExtraFisso.Leggi(Session("DC_OSPITE"))
                                        If DecExtraFisso.FuoriRetta = 1 Then
                                            ValutazioneRetta = ValutazioneRetta - Modulo.MathRound(EM_Retta.Emr_ImpAccredito(indice2), 2)
                                        End If
                                    Else
                                        Dim TipAddebito As New Cls_Addebito

                                        TipAddebito.Codice = EM_Retta.Emr_CodivaAccredito(indice2)
                                        TipAddebito.Leggi(Session("DC_OSPITE"), TipAddebito.Codice)
                                        If TipAddebito.FuoriRetta = 1 Then
                                            ValutazioneRetta = ValutazioneRetta - Modulo.MathRound(EM_Retta.Emr_ImpAccredito(indice2), 2)
                                        End If
                                    End If
                                End If
                            Next

                            If ValutazioneRetta < 0 Then
                                CausaleRetta = EM_Retta.CampoOperazione(TipoOperazione, "CausaleStorno")
                                If CausaleRetta = "" Then Exit Sub
                                If EM_Retta.CampoOperazione(TipoOperazione, "SoggettaABollo") = "" Or EM_Retta.CampoOperazione(TipoOperazione, "SoggettaABollo") = "F" Then
                                    Bollo = 0
                                    IVABollo = ""
                                    AliquotaIvaBollo = 0
                                Else
                                    Bollo = EM_Retta.ImportoBollo(DateSerial(Txt_Anno.Text, Dd_Mese.SelectedValue, 1), ValutazioneRetta)
                                    IVABollo = EM_Retta.CodiceIVABollo(DateSerial(Txt_Anno.Text, Dd_Mese.SelectedValue, 1))
                                    AliquotaIvaBollo = EM_Retta.DecodificaIva(IVABollo)
                                End If


                                'If Not EM_Retta.CreaRegistrazioneContabile(Txt_Anno.Text, Dd_Mese.SelectedValue, DD_CentroServizio.SelectedValue, , CausaleRetta, Xk.MASTRO, Xk.CONTO, OldCodiceOspite * 100, 0, 0, 0, Math.Abs(ImportoAddebiti), Math.Abs(ImportoAccrediti), Bollo, 0, AliquotaIva.Aliquota, CodiceIVA, AliquotaIvaBollo, IVABollo, DataRegistrazione, MastroAnticipo, ContoAnticipo, SottoContoAnticipo, 0, MeseRiferimento, AnnoRiferimento, ClOsp.TIPOOPERAZIONE, 0, 0, ImportoAddebitoFuoriRetta, 0, ImportoMeseSuccessivo2, GiorniMeseSuc, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1) Then Exit Sub
                                If Not EM_Retta.CreaRegistrazioneContabile(Txt_Anno.Text, Dd_Mese.SelectedValue, DD_CentroServizio.SelectedValue, CausaleRetta, Xk.MASTRO, Xk.CONTO, OldCodiceOspite * 100, 0, 0, 0, Math.Abs(ImportoAddebiti), Math.Abs(ImportoAccrediti), Bollo, 0, AliquotaIva.Aliquota, CodiceIVA, AliquotaBollo.Aliquota, IVABollo, Txt_DataRegistrazione.Text, 0, 0, 0, 0, 0, 0, xs.TIPOOPERAZIONE, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1) Then Exit Sub
                            End If
                            If ValutazioneRetta > 0 Then
                                Dim XOpe1 As New Cls_TipoOperazione
                                XOpe1.Leggi(Session("DC_OSPITE"), TipoOperazione)
                                If XOpe1.CausaleAddebito <> "" Then
                                    CausaleRetta = XOpe1.CausaleAddebito
                                End If


                                If EM_Retta.CampoOperazione(TipoOperazione, "SoggettaABollo") = "" Then
                                    Bollo = 0
                                    IVABollo = ""
                                    AliquotaIvaBollo = 0
                                Else
                                    Bollo = EM_Retta.ImportoBollo(DateSerial(Txt_Anno.Text, Dd_Mese.SelectedValue, 1), ValutazioneRetta)
                                    IVABollo = EM_Retta.CodiceIVABollo(DateSerial(Txt_Anno.Text, Dd_Mese.SelectedValue, 1))
                                    AliquotaIvaBollo = EM_Retta.DecodificaIva(IVABollo)
                                End If

                                If Not EM_Retta.CreaRegistrazioneContabile(Txt_Anno.Text, Dd_Mese.SelectedValue, DD_CentroServizio.SelectedValue, CausaleRetta, Xk.MASTRO, Xk.CONTO, OldCodiceOspite * 100, 0, 0, 0, Math.Abs(ImportoAddebiti), Math.Abs(ImportoAccrediti), Bollo, 0, AliquotaIva.Aliquota, CodiceIVA, AliquotaBollo.Aliquota, IVABollo, Txt_DataRegistrazione.Text, 0, 0, 0, 0, 0, 0, xs.TIPOOPERAZIONE, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1) Then Exit Sub
                            End If
                        End If

                        If xs.Compensazione <> "D" Then

                            If ImportoRetta < 0 Then
                                EM_Retta.GlbTipoOperazione = TipoOperasazione.Codice
                                EM_Retta.CreaRegistrazioneContabile(Txt_Anno.Text, Dd_Mese.SelectedValue, DD_CentroServizio.SelectedValue, TipoOperasazione.CausaleStorno, Xk.MASTRO, Xk.CONTO, OldCodiceOspite * 100, 0, 0, 0, 0, Math.Abs(ImportoAccrediti), BolloImpo, 0, AliquotaIva.Aliquota, CodiceIVA, AliquotaBollo.Aliquota, IVABollo, Txt_DataRegistrazione.Text, 0, 0, 0, 0, Dd_Mese.SelectedValue, Txt_Anno.Text, TipoOperasazione.Codice, 0, 0, Math.Abs(ImportoAddebiti), 0, 0, 0, 0, 0, 1)
                            Else
                                EM_Retta.GlbTipoOperazione = TipoOperasazione.Codice
                                EM_Retta.CreaRegistrazioneContabile(Txt_Anno.Text, Dd_Mese.SelectedValue, DD_CentroServizio.SelectedValue, TipoOperasazione.CausaleAddebito, Xk.MASTRO, Xk.CONTO, OldCodiceOspite * 100, 0, 0, 0, 0, Math.Abs(ImportoAccrediti), BolloImpo, 0, AliquotaIva.Aliquota, CodiceIVA, AliquotaBollo.Aliquota, IVABollo, Txt_DataRegistrazione.Text, 0, 0, 0, 0, Dd_Mese.SelectedValue, Txt_Anno.Text, TipoOperasazione.Codice, 0, 0, Math.Abs(ImportoAddebiti), 0, 0, 0, 0, 0, 1)
                            End If
                        End If

                        ImportoRetta = 0
                        MaxAddebito = 0
                        MaxAccredito = 0
                        ImportoAddebiti = 0
                        ImportoAccrediti = 0

                        For x = 0 To 40
                            EM_Retta.Emr_ImpAddebito(x) = 0
                            EM_Retta.Emr_DesAddebito(x) = ""
                            EM_Retta.Emr_CodivaAddebito(x) = ""
                            EM_Retta.Emr_ImpAccredito(x) = 0
                            EM_Retta.Emr_DesAccredito(x) = ""
                            EM_Retta.Emr_CodivaAccredito(x) = ""
                        Next
                    End If
                End If


                If Tabella.Rows(i).Item(3) = "AD" Then
                    ImportoRetta = ImportoRetta + CDbl(Tabella.Rows(i).Item(6))
                    MaxAddebito = MaxAddebito + 1
                    EM_Retta.Emr_ImpAddebito(MaxAddebito) = CDbl(Tabella.Rows(i).Item(6))
                    EM_Retta.Emr_DesAddebito(MaxAddebito) = Tabella.Rows(i).Item(9)
                    EM_Retta.Emr_CodivaAddebito(MaxAddebito) = Tabella.Rows(i).Item(10)
                    ImportoAddebiti = ImportoAddebiti + CDbl(Tabella.Rows(i).Item(6))
                Else
                    ImportoRetta = ImportoRetta - CDbl(Tabella.Rows(i).Item(6))
                    MaxAccredito = MaxAccredito + 1
                    EM_Retta.Emr_ImpAccredito(MaxAccredito) = CDbl(Tabella.Rows(i).Item(6))
                    EM_Retta.Emr_DesAccredito(MaxAccredito) = Tabella.Rows(i).Item(9)
                    EM_Retta.Emr_CodivaAccredito(MaxAccredito) = Tabella.Rows(i).Item(10)

                    ImportoAccrediti = ImportoAccrediti + CDbl(Tabella.Rows(i).Item(6))
                End If
                OldCodiceOspite = CodiceOspite
            End If
        Next

        If Math.Abs(ImportoRetta) > 0 Then
            Dim xs As New ClsOspite

            xs.Leggi(Session("DC_OSPITE"), OldCodiceOspite)

            Dim TipoCserv As New Cls_DatiOspiteParenteCentroServizio

            TipoCserv.CentroServizio = DD_CentroServizio.SelectedValue
            TipoCserv.CodiceOspite = xs.CodiceOspite
            TipoCserv.Leggi(Session("DC_OSPITE"))
            If TipoCserv.TipoOperazione <> "" Then
                xs.TIPOOPERAZIONE = TipoCserv.TipoOperazione
                xs.CODICEIVA = TipoCserv.AliquotaIva
            End If


            If xs.TIPOOPERAZIONE.Trim <> "" Then

                Dim KC2s As New Cls_DatiOspiteParenteCentroServizio

                KC2s.CentroServizio = DD_CentroServizio.SelectedValue
                KC2s.CodiceOspite = OldCodiceOspite
                KC2s.CodiceParente = 0
                KC2s.Leggi(Session("DC_OSPITE"))

                REM Assegna la tipologia operazione e l'aliquota prendendo quella relativa al cserv
                If KC2s.CodiceOspite <> 0 Then
                    xs.TIPOOPERAZIONE = KC2s.TipoOperazione
                    xs.CODICEIVA = KC2s.AliquotaIva
                    xs.FattAnticipata = KC2s.Anticipata
                    xs.MODALITAPAGAMENTO = KC2s.ModalitaPagamento
                    xs.Compensazione = KC2s.Compensazione
                    xs.SETTIMANA = KC2s.Settimana
                End If


                Dim TipoOperasazione As New Cls_TipoOperazione

                TipoOperasazione.Leggi(Session("DC_OSPITE"), xs.TIPOOPERAZIONE)


                CodiceIVA = xs.CODICEIVA

                Dim AliquotaIva As New Cls_IVA
                Dim AliquotaBollo As New Cls_IVA

                AliquotaIva.Leggi(Session("DC_TABELLE"), CodiceIVA)

                If TipoOperasazione.SoggettaABollo = "" Or (TipoOperasazione.SoggettaABollo = "F" And ImportoRetta < 0) Then
                    BolloImpo = 0
                    Bollo = 0
                    IVABollo = ""
                Else
                    Dim KBollo As New Cls_bolli

                    KBollo.Leggi(Session("DC_TABELLE"), Txt_DataRegistrazione.Text)
                    BolloImpo = KBollo.ImportoBollo
                    Bollo = KBollo.ImportoBollo
                    IVABollo = KBollo.CodiceIVA


                    AliquotaBollo.Leggi(Session("DC_TABELLE"), IVABollo)

                End If




                If xs.Compensazione = "D" Then
                    Dim CausaleRetta As String
                    Dim ValutazioneRetta As Double = 0
                    Dim TipoOperazione = TipoOperasazione.Codice
                    Dim AliquotaIvaBollo As Double = 0
                    Dim indice2 As Integer


                    ValutazioneRetta = 0
                    For indice2 = 1 To 40
                        If Modulo.MathRound(EM_Retta.Emr_ImpExtra(indice2), 2) > 0 Then
                            Dim DecExtraFisso As New Cls_TipoExtraFisso

                            DecExtraFisso.CODICEEXTRA = EM_Retta.Emr_CodiceExtra(indice2)
                            DecExtraFisso.Leggi(Session("DC_OSPITE"))
                            If DecExtraFisso.FuoriRetta = 0 Then
                                ValutazioneRetta = ValutazioneRetta + Modulo.MathRound(EM_Retta.Emr_ImpExtra(indice2), 2)
                            End If
                        End If
                        If Modulo.MathRound(EM_Retta.Emr_ImpAddebito(indice2), 2) > 0 Then
                            Dim TipAddebito As New Cls_Addebito

                            TipAddebito.Codice = EM_Retta.Emr_CodivaAddebito(indice2)
                            TipAddebito.Leggi(Session("DC_OSPITE"), TipAddebito.Codice)
                            If TipAddebito.FuoriRetta = 0 Then
                                ValutazioneRetta = ValutazioneRetta + Modulo.MathRound(EM_Retta.Emr_ImpAddebito(indice2), 2)
                            End If
                        End If
                        If Modulo.MathRound(EM_Retta.Emr_ImpAccredito(indice2), 2) > 0 Then
                            If EM_Retta.Emr_AccreditoDaExtra(indice2) = "E" Then
                                Dim DecExtraFisso As New Cls_TipoExtraFisso

                                DecExtraFisso.CODICEEXTRA = EM_Retta.Emr_CodivaAccredito(indice2)
                                DecExtraFisso.Leggi(Session("DC_OSPITE"))
                                If DecExtraFisso.FuoriRetta = 0 Then
                                    ValutazioneRetta = ValutazioneRetta + Modulo.MathRound(EM_Retta.Emr_ImpAccredito(indice2), 2)
                                End If
                            Else
                                Dim TipAddebito As New Cls_Addebito
                                TipAddebito.Codice = EM_Retta.Emr_CodivaAccredito(indice2)
                                TipAddebito.Leggi(Session("DC_OSPITE"), TipAddebito.Codice)
                                If TipAddebito.FuoriRetta = 0 Then
                                    ValutazioneRetta = ValutazioneRetta - Modulo.MathRound(EM_Retta.Emr_ImpAccredito(indice2), 2)
                                End If
                            End If
                        End If
                    Next

                    If ValutazioneRetta < 0 Then
                        CausaleRetta = EM_Retta.CampoOperazione(TipoOperazione, "CAUSALEACCREDITO")
                        If CausaleRetta = "" Then Exit Sub
                        If EM_Retta.CampoOperazione(TipoOperazione, "SoggettaABollo") = "" Or EM_Retta.CampoOperazione(TipoOperazione, "SoggettaABollo") = "F" Then
                            Bollo = 0
                            IVABollo = ""
                            AliquotaIvaBollo = 0
                        Else
                            Bollo = EM_Retta.ImportoBollo(DateSerial(Txt_Anno.Text, Dd_Mese.SelectedValue, 1), ValutazioneRetta)
                            IVABollo = EM_Retta.CodiceIVABollo(DateSerial(Txt_Anno.Text, Dd_Mese.SelectedValue, 1))
                            AliquotaIvaBollo = EM_Retta.DecodificaIva(IVABollo)
                        End If

                        If Not EM_Retta.CreaRegistrazioneContabile(Txt_Anno.Text, Dd_Mese.SelectedValue, DD_CentroServizio.SelectedValue, CausaleRetta, Xk.MASTRO, Xk.CONTO, OldCodiceOspite * 100, 0, 0, 0, Math.Abs(ImportoAddebiti), Math.Abs(ImportoAccrediti), Bollo, 0, AliquotaIva.Aliquota, CodiceIVA, AliquotaBollo.Aliquota, IVABollo, Txt_DataRegistrazione.Text, 0, 0, 0, 0, 0, 0, xs.TIPOOPERAZIONE, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0) Then Exit Sub
                        'If Not EM_Retta.CreaRegistrazioneContabile(Txt_Anno.Text, Dd_Mese.SelectedValue, DD_CentroServizio.SelectedValue, CausaleRetta, Xk.MASTRO, Xk.CONTO, OldCodiceOspite * 100, 0, 0, 0, Math.Abs(ImportoAddebiti), Math.Abs(ImportoAccrediti), Bollo, 0, AliquotaIva.Aliquota, CodiceIVA, AliquotaBollo.Aliquota, IVABollo, Txt_DataRegistrazione.Text, 0, 0, 0, 0, 0, 0, xs.TIPOOPERAZIONE, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1) Then Exit Sub
                    End If
                    If ValutazioneRetta > 0 Then
                        CausaleRetta = EM_Retta.CampoOperazione(TipoOperazione, "CausaleRetta")
                        If EM_Retta.CampoOperazione(TipoOperazione, "SoggettaABollo") = "" Then
                            Bollo = 0
                            IVABollo = ""
                            AliquotaIvaBollo = 0
                        Else
                            Bollo = EM_Retta.ImportoBollo(DateSerial(Txt_Anno.Text, Dd_Mese.SelectedValue, 1), ValutazioneRetta)
                            IVABollo = EM_Retta.CodiceIVABollo(DateSerial(Txt_Anno.Text, Dd_Mese.SelectedValue, 1))
                            AliquotaIvaBollo = EM_Retta.DecodificaIva(IVABollo)
                        End If
                        'If Not EM_Retta.CreaRegistrazioneContabile(Txt_Anno.Text, Dd_Mese.SelectedValue, DD_CentroServizio.SelectedValue, , CausaleRetta, Xk.MASTRO, Xk.CONTO, OldCodiceOspite * 100, ImportoRetta, 0, ImportoExtra, ImportoAddebito, ImportoAccredito, Bollo, ImportoAnticipo, AliquotaIva, IVA, AliquotaIvaBollo, IVABollo, DataRegistrazione, MastroAnticipo, ContoAnticipo, SottoContoAnticipo, GiorniPres, MeseRiferimento, AnnoRiferimento, ClOsp.TIPOOPERAZIONE, ImportoRettaAssenza, GiorniAss, ImportoAddebitoFuoriRetta, ImportoMeseSuccessivo, 0, GiorniMeseSuc, 0, 0, 0, ImportoRettaSecondoPeriodo, ImportoRettaAssenzeSecondoPeriodo, GiorniPresSecondoPeriodo, GiorniAssSecondoPeriodo, ImportoSuccessivoP2, GiorniP2, 0) Then Exit Sub
                        If Not EM_Retta.CreaRegistrazioneContabile(Txt_Anno.Text, Dd_Mese.SelectedValue, DD_CentroServizio.SelectedValue, CausaleRetta, Xk.MASTRO, Xk.CONTO, OldCodiceOspite * 100, 0, 0, 0, Math.Abs(ImportoAddebiti), Math.Abs(ImportoAccrediti), Bollo, 0, AliquotaIva.Aliquota, CodiceIVA, AliquotaBollo.Aliquota, IVABollo, Txt_DataRegistrazione.Text, 0, 0, 0, 0, 0, 0, xs.TIPOOPERAZIONE, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0) Then Exit Sub
                    End If

                    ValutazioneRetta = 0

                    For indice2 = 1 To 40
                        If Modulo.MathRound(EM_Retta.Emr_ImpExtra(indice2), 2) > 0 Then
                            Dim DecExtraFisso As New Cls_TipoExtraFisso

                            DecExtraFisso.CODICEEXTRA = EM_Retta.Emr_CodiceExtra(indice2)
                            DecExtraFisso.Leggi(Session("DC_OSPITE"))
                            If DecExtraFisso.FuoriRetta = 1 Then
                                ValutazioneRetta = ValutazioneRetta + Modulo.MathRound(EM_Retta.Emr_ImpExtra(indice2), 2)
                            End If
                        End If
                        If Modulo.MathRound(EM_Retta.Emr_ImpAddebito(indice2), 2) > 0 Then
                            Dim TipAddebito As New Cls_Addebito

                            TipAddebito.Codice = EM_Retta.Emr_CodivaAddebito(indice2)
                            TipAddebito.Leggi(Session("DC_OSPITE"), TipAddebito.Codice)
                            If TipAddebito.FuoriRetta = 1 Then
                                ValutazioneRetta = ValutazioneRetta + Modulo.MathRound(EM_Retta.Emr_ImpAddebito(indice2), 2)
                            End If
                        End If
                        If Modulo.MathRound(EM_Retta.Emr_ImpAccredito(indice2), 2) > 0 Then
                            If EM_Retta.Emr_AccreditoDaExtra(indice2) = "E" Then
                                Dim DecExtraFisso As New Cls_TipoExtraFisso

                                DecExtraFisso.CODICEEXTRA = EM_Retta.Emr_CodivaAccredito(indice2)
                                DecExtraFisso.Leggi(Session("DC_OSPITE"))
                                If DecExtraFisso.FuoriRetta = 1 Then
                                    ValutazioneRetta = ValutazioneRetta - Modulo.MathRound(EM_Retta.Emr_ImpAccredito(indice2), 2)
                                End If
                            Else
                                Dim TipAddebito As New Cls_Addebito

                                TipAddebito.Codice = EM_Retta.Emr_CodivaAccredito(indice2)
                                TipAddebito.Leggi(Session("DC_OSPITE"), TipAddebito.Codice)
                                If TipAddebito.FuoriRetta = 1 Then
                                    ValutazioneRetta = ValutazioneRetta - Modulo.MathRound(EM_Retta.Emr_ImpAccredito(indice2), 2)
                                End If
                            End If
                        End If
                    Next

                    If ValutazioneRetta < 0 Then
                        CausaleRetta = EM_Retta.CampoOperazione(TipoOperazione, "CausaleStorno")
                        If CausaleRetta = "" Then Exit Sub
                        If EM_Retta.CampoOperazione(TipoOperazione, "SoggettaABollo") = "" Or EM_Retta.CampoOperazione(TipoOperazione, "SoggettaABollo") = "F" Then
                            Bollo = 0
                            IVABollo = ""
                            AliquotaIvaBollo = 0
                        Else
                            Bollo = EM_Retta.ImportoBollo(DateSerial(Txt_Anno.Text, Dd_Mese.SelectedValue, 1), ValutazioneRetta)
                            IVABollo = EM_Retta.CodiceIVABollo(DateSerial(Txt_Anno.Text, Dd_Mese.SelectedValue, 1))
                            AliquotaIvaBollo = EM_Retta.DecodificaIva(IVABollo)
                        End If


                        'If Not EM_Retta.CreaRegistrazioneContabile(Txt_Anno.Text, Dd_Mese.SelectedValue, DD_CentroServizio.SelectedValue, , CausaleRetta, Xk.MASTRO, Xk.CONTO, OldCodiceOspite * 100, 0, 0, 0, Math.Abs(ImportoAddebiti), Math.Abs(ImportoAccrediti), Bollo, 0, AliquotaIva.Aliquota, CodiceIVA, AliquotaIvaBollo, IVABollo, DataRegistrazione, MastroAnticipo, ContoAnticipo, SottoContoAnticipo, 0, MeseRiferimento, AnnoRiferimento, ClOsp.TIPOOPERAZIONE, 0, 0, ImportoAddebitoFuoriRetta, 0, ImportoMeseSuccessivo2, GiorniMeseSuc, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1) Then Exit Sub
                        If Not EM_Retta.CreaRegistrazioneContabile(Txt_Anno.Text, Dd_Mese.SelectedValue, DD_CentroServizio.SelectedValue, CausaleRetta, Xk.MASTRO, Xk.CONTO, OldCodiceOspite * 100, 0, 0, 0, Math.Abs(ImportoAddebiti), Math.Abs(ImportoAccrediti), Bollo, 0, AliquotaIva.Aliquota, CodiceIVA, AliquotaBollo.Aliquota, IVABollo, Txt_DataRegistrazione.Text, 0, 0, 0, 0, 0, 0, xs.TIPOOPERAZIONE, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1) Then Exit Sub
                    End If
                    If ValutazioneRetta > 0 Then
                        CausaleRetta = EM_Retta.CampoOperazione(TipoOperazione, "CausaleAddebito")



                        Dim XOpe1 As New Cls_TipoOperazione
                        XOpe1.Leggi(Session("DC_OSPITE"), TipoOperazione)
                        If XOpe1.CausaleAddebito <> "" Then
                            CausaleRetta = XOpe1.CausaleAddebito
                        End If


                        If EM_Retta.CampoOperazione(TipoOperazione, "SoggettaABollo") = "" Then
                            Bollo = 0
                            IVABollo = ""
                            AliquotaIvaBollo = 0
                        Else
                            Bollo = EM_Retta.ImportoBollo(DateSerial(Txt_Anno.Text, Dd_Mese.SelectedValue, 1), ValutazioneRetta)
                            IVABollo = EM_Retta.CodiceIVABollo(DateSerial(Txt_Anno.Text, Dd_Mese.SelectedValue, 1))
                            AliquotaIvaBollo = EM_Retta.DecodificaIva(IVABollo)
                        End If

                        If Not EM_Retta.CreaRegistrazioneContabile(Txt_Anno.Text, Dd_Mese.SelectedValue, DD_CentroServizio.SelectedValue, CausaleRetta, Xk.MASTRO, Xk.CONTO, OldCodiceOspite * 100, 0, 0, 0, Math.Abs(ImportoAddebiti), Math.Abs(ImportoAccrediti), Bollo, 0, AliquotaIva.Aliquota, CodiceIVA, AliquotaBollo.Aliquota, IVABollo, Txt_DataRegistrazione.Text, 0, 0, 0, 0, 0, 0, xs.TIPOOPERAZIONE, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1) Then Exit Sub
                    End If
                End If


                If xs.Compensazione <> "D" Then
                    If ImportoRetta < 0 Then
                        EM_Retta.GlbTipoOperazione = TipoOperasazione.Codice
                        EM_Retta.CreaRegistrazioneContabile(Txt_Anno.Text, Dd_Mese.SelectedValue, DD_CentroServizio.SelectedValue, TipoOperasazione.CausaleStorno, Xk.MASTRO, Xk.CONTO, OldCodiceOspite * 100, 0, 0, 0, 0, Math.Abs(ImportoAccrediti), BolloImpo, 0, AliquotaIva.Aliquota, CodiceIVA, AliquotaBollo.Aliquota, IVABollo, Txt_DataRegistrazione.Text, 0, 0, 0, 0, Dd_Mese.SelectedValue, Txt_Anno.Text, TipoOperasazione.Codice, 0, 0, Math.Abs(ImportoAddebiti), 0, 0, 0, 0, 0, 1)


                    Else
                        EM_Retta.GlbTipoOperazione = TipoOperasazione.Codice
                        'EM_Retta.CreaRegistrazioneContabile(Txt_Anno.Text, Dd_Mese.SelectedValue, DD_CentroServizio.SelectedValue, TipoOperasazione.CausaleRetta, Xk.MASTRO, Xk.CONTO, OldCodiceOspite * 100, 0, 0, 0, ImportoRetta, 0, BolloImpo, 0, AliquotaIva.Aliquota, CodiceIVA, AliquotaBollo.Aliquota, IVABollo, Txt_DataRegistrazione.Text, 0, 0, 0, 0, Dd_Mese.SelectedValue, Txt_Anno.Text, TipoOperasazione.Codice, 0, 0, 0, 0, 0, 0, 0, 0, 1)
                        EM_Retta.CreaRegistrazioneContabile(Txt_Anno.Text, Dd_Mese.SelectedValue, DD_CentroServizio.SelectedValue, TipoOperasazione.CausaleAddebito, Xk.MASTRO, Xk.CONTO, OldCodiceOspite * 100, 0, 0, 0, 0, Math.Abs(ImportoAccrediti), BolloImpo, 0, AliquotaIva.Aliquota, CodiceIVA, AliquotaBollo.Aliquota, IVABollo, Txt_DataRegistrazione.Text, 0, 0, 0, 0, Dd_Mese.SelectedValue, Txt_Anno.Text, TipoOperasazione.Codice, 0, 0, Math.Abs(ImportoAddebiti), 0, 0, 0, 0, 0, 1)
                    End If
                End If
            End If
        End If

        EM_Retta.Commit()
        EM_Retta.CloseDB()
    End Sub
    Protected Sub Btn_Modifica0_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles Btn_Modifica0.Click
        If DD_CentroServizio.SelectedValue = "" Then
            ClientScript.RegisterClientScriptBlock(Me.GetType(), "Errore", "VisualizzaErrore('Specifica centroservizio');", True)
            Exit Sub
        End If

        If Val(Txt_Anno.Text) < 1990 Then
            ClientScript.RegisterClientScriptBlock(Me.GetType(), "Errore", "VisualizzaErrore('Specificare Anno');", True)
            Exit Sub
        End If

        If Not IsDate(Txt_DataRegistrazione.Text) Then
            ClientScript.RegisterClientScriptBlock(Me.GetType(), "Errore", "VisualizzaErrore('Data validita formalmente errata');", True)
            Exit Sub
        End If

        Tabella = ViewState("SalvaTabella")
        If IsNothing(Tabella) Then
            ClientScript.RegisterClientScriptBlock(Me.GetType(), "Errore", "VisualizzaErrore('Visualizzare elenco prima di effettuare la modifica');", True)
            Exit Sub
        End If

        If Tabella.Rows.Count = 0 Then
            ClientScript.RegisterClientScriptBlock(Me.GetType(), "Errore", "VisualizzaErrore('Visualizzare elenco prima di effettuare la modifica');", True)
            Exit Sub
        End If

        Dim VerificaElaborazione As New Cls_Parametri


        If VerificaElaborazione.InElaborazione(Session("DC_OSPITE")) Then
            ClientScript.RegisterClientScriptBlock(Me.GetType(), "Errore", "VisualizzaErrore('Elaborazione in esecuzione non posso procedere');", True)
            Exit Sub
        End If

        Dim ParametriGenerale As New Cls_ParametriGenerale

        If ParametriGenerale.InElaborazione(Session("DC_GENERALE")) Then
            ClientScript.RegisterClientScriptBlock(Me.GetType(), "Errore", "alert('Elaborazione in esecuzione non posso procedere');", True)
            Exit Sub
        End If



        VerificaElaborazione.Elaborazione(Session("DC_OSPITE"))
        ParametriGenerale.Elaborazione(Session("DC_GENERALE"))

        If DD_Tipo.SelectedValue = "O" Then
            Call CreaDocOspiti()
        End If
        If DD_Tipo.SelectedValue = "P" Then
            Call CreaDocParenti()
        End If
        If DD_Tipo.SelectedValue = "C" Then
            Call CreaDocComuni()
        End If


        If DD_Tipo.SelectedValue = "J" Then
            Call CreaDocJolly()
        End If


        If DD_Tipo.SelectedValue = "R" Then
            Call CreaDocRegione()
        End If


        Dim EmOsp As New Cls_EmissioneRetta

        EmOsp.ConnessioneGenerale = Session("DC_GENERALE")
        EmOsp.ConnessioneOspiti = Session("DC_OSPITE")
        EmOsp.ConnessioneTabelle = Session("DC_TABELLE")
        EmOsp.ApriDB()
        Dim cn As OleDbConnection


        cn = New Data.OleDb.OleDbConnection(Session("DC_GENERALE"))

        cn.Open()
        Dim cmd As New OleDbCommand()
        cmd.CommandText = ("select * from Temp_MovimentiContabiliTesta Order by NumeroRegistrazione  ")        
        cmd.Connection = cn
        Dim VerReader As OleDbDataReader = cmd.ExecuteReader()
        Do While VerReader.Read
            EmOsp.VerificaRegistrazioneIVA(campodb(VerReader.Item("NumeroRegistrazione")), False)
        Loop
        VerReader.Close()

        If Chk_FatturaDiAnticipo.Checked = True Then
            Dim cmdMod As New OleDbCommand()
            cmdMod.CommandText = ("UPDATE Temp_MovimentiContabiliTesta SET  FatturaDiAnticipo = 'S' ")
            cmdMod.Connection = cn
            cmdMod.ExecuteNonQuery()
        End If
        EmOsp.CloseDB()

        Dim Vettore(1000) As Integer
        Dim Max As Integer = 0
        Tabella = ViewState("SalvaTabella")

        For i = 0 To Tabella.Rows.Count - 1

            Dim CheckBoxV As CheckBox = DirectCast(Grd_AggiornaRette.Rows(i).FindControl("Chk_Seleziona"), CheckBox)


            If CheckBoxV.Checked = False Then
                Vettore(Max) = Tabella.Rows(i).Item("Id")
                Max = Max + 1
            End If
        Next

        Session("RIATTIVAADD") = Vettore

        Dim Emc As New Cls_EmissioneRetta

        Emc.ConnessioneGenerale = Session("DC_GENERALE")
        Emc.ConnessioneOspiti = Session("DC_OSPITE")
        Emc.ConnessioneTabelle = Session("DC_TABELLE")
        If EmC.ComuniIntestatiOspiti() Then
            EmC.ModificaFatturaComuniIntestatario()
        End If


        

        Response.Redirect("StampaFattureProva.aspx?TIPO=NONEMISSIONE&URL=FatturaDaAddebitiAccrediti.aspx")
    End Sub
    Private Sub CreaDocParenti()
        Dim i As Integer
        Dim x As Integer
        Dim Centroservizio As String = ""
        Dim CodiceOspite As Long = 0
        Dim CodiceParente As Long = 0
        Dim OldCodiceParente As Long = 0
        Dim OldCodiceOspite As Long = 0
        Dim MaxAddebito As Long = 0
        Dim MaxAccredito As Long = 0


        Dim ImportoAddebiti As Double = 0
        Dim ImportoAccrediti As Double = 0

        Dim CodiceIVA As String

        Dim BolloImpo As Double
        Dim Bollo As Integer
        Dim IVABollo As String


        Dim ImportoRetta As Double

        Dim EM_Retta As New Cls_EmissioneRetta

        EM_Retta.ConnessioneGenerale = Session("DC_GENERALE")
        EM_Retta.ConnessioneOspiti = Session("DC_OSPITE")
        EM_Retta.ConnessioneTabelle = Session("DC_TABELLE")

        EM_Retta.ApriDB()

        EM_Retta.EliminaTemporanei()

        Tabella = ViewState("SalvaTabella")

        Dim Xk As New Cls_CentroServizio


        Xk.Leggi(Session("DC_OSPITE"), DD_CentroServizio.SelectedValue)

        CodiceOspite = Tabella.Rows(i).Item(0)
        For i = 0 To Tabella.Rows.Count - 1

            Centroservizio = DD_CentroServizio.SelectedValue
            CodiceOspite = Tabella.Rows(i).Item(0)
            CodiceParente = Tabella.Rows(i).Item(5)
            Dim CheckBoxV As CheckBox = DirectCast(Grd_AggiornaRette.Rows(i).FindControl("Chk_Seleziona"), CheckBox)


            If CheckBoxV.Checked = True Then
                If (CodiceParente <> OldCodiceParente Or CodiceOspite <> OldCodiceOspite) And OldCodiceOspite <> 0 Then
                    Dim xs As New Cls_Parenti

                    xs.Leggi(Session("DC_OSPITE"), OldCodiceOspite, OldCodiceParente)


                    Dim TipoCserv As New Cls_DatiOspiteParenteCentroServizio

                    TipoCserv.CentroServizio = DD_CentroServizio.SelectedValue
                    TipoCserv.CodiceOspite = xs.CodiceOspite
                    TipoCserv.CodiceParente = xs.CodiceParente
                    TipoCserv.Leggi(Session("DC_OSPITE"))
                    If TipoCserv.TipoOperazione <> "" Then
                        xs.TIPOOPERAZIONE = TipoCserv.TipoOperazione
                        xs.CODICEIVA = TipoCserv.AliquotaIva
                    End If

                    If xs.TIPOOPERAZIONE.Trim <> "" Then

                        Dim TipoOperasazione As New Cls_TipoOperazione


                        TipoOperasazione.Leggi(Session("DC_OSPITE"), xs.TIPOOPERAZIONE)

                        EM_Retta.GlbTipoOperazione = xs.TIPOOPERAZIONE

                        CodiceIVA = xs.CODICEIVA

                        Dim AliquotaIva As New Cls_IVA
                        Dim AliquotaBollo As New Cls_IVA

                        AliquotaIva.Leggi(Session("DC_TABELLE"), CodiceIVA)

                        If TipoOperasazione.SoggettaABollo = "" Or (TipoOperasazione.SoggettaABollo = "F" And ImportoRetta < 0) Then
                            BolloImpo = 0
                            Bollo = 0
                            IVABollo = ""
                        Else
                            Dim KBollo As New Cls_bolli

                            KBollo.Leggi(Session("DC_TABELLE"), Txt_DataRegistrazione.Text)
                            BolloImpo = KBollo.ImportoBollo
                            Bollo = KBollo.ImportoBollo
                            IVABollo = KBollo.CodiceIVA


                            AliquotaBollo.Leggi(Session("DC_TABELLE"), IVABollo)

                        End If


                        If ImportoRetta < 0 Then
                            EM_Retta.GlbTipoOperazione = TipoOperasazione.Codice
                            EM_Retta.CreaRegistrazioneContabile(Txt_Anno.Text, Dd_Mese.SelectedValue, DD_CentroServizio.SelectedValue, TipoOperasazione.CausaleStorno, Xk.MASTRO, Xk.CONTO, OldCodiceOspite * 100 + OldCodiceParente, 0, 0, 0, 0, Math.Abs(ImportoRetta), BolloImpo, 0, AliquotaIva.Aliquota, CodiceIVA, AliquotaBollo.Aliquota, IVABollo, Txt_DataRegistrazione.Text, 0, 0, 0, 0, Dd_Mese.SelectedValue, Txt_Anno.Text, TipoOperasazione.Codice, 0, 0, 0, 0, 0, 0, 0, 0, 1)
                        Else
                            EM_Retta.GlbTipoOperazione = TipoOperasazione.Codice
                            EM_Retta.CreaRegistrazioneContabile(Txt_Anno.Text, Dd_Mese.SelectedValue, DD_CentroServizio.SelectedValue, TipoOperasazione.CausaleAddebito, Xk.MASTRO, Xk.CONTO, OldCodiceOspite * 100 + OldCodiceParente, 0, 0, 0, 0, 0, BolloImpo, 0, AliquotaIva.Aliquota, CodiceIVA, AliquotaBollo.Aliquota, IVABollo, Txt_DataRegistrazione.Text, 0, 0, 0, 0, Dd_Mese.SelectedValue, Txt_Anno.Text, TipoOperasazione.Codice, 0, 0, ImportoRetta, 0, 0, 0, 0, 0, 1)
                        End If

                        ImportoRetta = 0
                        MaxAddebito = 0
                        MaxAccredito = 0

                        For x = 0 To 40
                            EM_Retta.Emr_ImpAddebito(x) = 0
                            EM_Retta.Emr_DesAddebito(x) = ""
                            EM_Retta.Emr_CodivaAddebito(x) = ""
                            EM_Retta.Emr_ImpAccredito(x) = 0
                            EM_Retta.Emr_DesAccredito(x) = ""
                            EM_Retta.Emr_CodivaAccredito(x) = ""
                        Next
                    End If
                End If

                If Tabella.Rows(i).Item(3) = "AD" Then
                    ImportoAddebiti = ImportoAddebiti + CDbl(Tabella.Rows(i).Item(6))
                    ImportoRetta = ImportoRetta + CDbl(Tabella.Rows(i).Item(6))
                    MaxAddebito = MaxAddebito + 1
                    EM_Retta.Emr_ImpAddebito(MaxAddebito) = CDbl(Tabella.Rows(i).Item(6))
                    EM_Retta.Emr_DesAddebito(MaxAddebito) = Tabella.Rows(i).Item(9)
                    EM_Retta.Emr_CodivaAddebito(MaxAddebito) = Tabella.Rows(i).Item(10)
                Else
                    ImportoAccrediti = ImportoAccrediti + CDbl(Tabella.Rows(i).Item(6))
                    ImportoRetta = ImportoRetta - CDbl(Tabella.Rows(i).Item(6))
                    MaxAccredito = MaxAccredito + 1
                    EM_Retta.Emr_ImpAccredito(MaxAccredito) = CDbl(Tabella.Rows(i).Item(6))
                    EM_Retta.Emr_DesAccredito(MaxAccredito) = Tabella.Rows(i).Item(9)
                    EM_Retta.Emr_CodivaAccredito(MaxAccredito) = Tabella.Rows(i).Item(10)
                End If
                OldCodiceOspite = CodiceOspite
                OldCodiceParente = CodiceParente
            End If


        Next

        If Math.Abs(ImportoRetta) > 0 Then
            Dim xs As New Cls_Parenti

            xs.Leggi(Session("DC_OSPITE"), OldCodiceOspite, OldCodiceParente)

            Dim TipoCserv As New Cls_DatiOspiteParenteCentroServizio

            TipoCserv.CentroServizio = DD_CentroServizio.SelectedValue
            TipoCserv.CodiceOspite = xs.CodiceOspite
            TipoCserv.CodiceParente = xs.CodiceParente
            TipoCserv.Leggi(Session("DC_OSPITE"))
            If TipoCserv.TipoOperazione <> "" Then
                xs.TIPOOPERAZIONE = TipoCserv.TipoOperazione
                xs.CODICEIVA = TipoCserv.AliquotaIva
            End If


            If xs.TIPOOPERAZIONE.Trim <> "" Then
                Dim TipoOperasazione As New Cls_TipoOperazione

                TipoOperasazione.Leggi(Session("DC_OSPITE"), xs.TIPOOPERAZIONE)


                CodiceIVA = xs.CODICEIVA

                Dim AliquotaIva As New Cls_IVA
                Dim AliquotaBollo As New Cls_IVA

                AliquotaIva.Leggi(Session("DC_TABELLE"), CodiceIVA)

                If TipoOperasazione.SoggettaABollo = "" Or (TipoOperasazione.SoggettaABollo = "F" And ImportoRetta < 0) Then
                    BolloImpo = 0
                    Bollo = 0
                    IVABollo = ""
                Else
                    Dim KBollo As New Cls_bolli

                    KBollo.Leggi(Session("DC_TABELLE"), Txt_DataRegistrazione.Text)
                    BolloImpo = KBollo.ImportoBollo
                    Bollo = KBollo.ImportoBollo
                    IVABollo = KBollo.CodiceIVA


                    AliquotaBollo.Leggi(Session("DC_TABELLE"), IVABollo)

                End If
                If ImportoRetta < 0 Then
                    EM_Retta.GlbTipoOperazione = TipoOperasazione.Codice
                    EM_Retta.CreaRegistrazioneContabile(Txt_Anno.Text, Dd_Mese.SelectedValue, DD_CentroServizio.SelectedValue, TipoOperasazione.CausaleStorno, Xk.MASTRO, Xk.CONTO, OldCodiceOspite * 100 + OldCodiceParente, 0, 0, 0, 0, ImportoAccrediti, BolloImpo, 0, AliquotaIva.Aliquota, CodiceIVA, AliquotaBollo.Aliquota, IVABollo, Txt_DataRegistrazione.Text, 0, 0, 0, 0, Dd_Mese.SelectedValue, Txt_Anno.Text, TipoOperasazione.Codice, 0, 0, ImportoAddebiti, 0, 0, 0, 0, 0, 1)
                Else
                    EM_Retta.GlbTipoOperazione = TipoOperasazione.Codice
                    EM_Retta.CreaRegistrazioneContabile(Txt_Anno.Text, Dd_Mese.SelectedValue, DD_CentroServizio.SelectedValue, TipoOperasazione.CausaleAddebito, Xk.MASTRO, Xk.CONTO, OldCodiceOspite * 100 + OldCodiceParente, 0, 0, 0, 0, ImportoAccrediti, BolloImpo, 0, AliquotaIva.Aliquota, CodiceIVA, AliquotaBollo.Aliquota, IVABollo, Txt_DataRegistrazione.Text, 0, 0, 0, 0, Dd_Mese.SelectedValue, Txt_Anno.Text, TipoOperasazione.Codice, 0, 0, ImportoAddebiti, 0, 0, 0, 0, 0, 1)
                End If
            End If

        End If

        EM_Retta.Commit()
        EM_Retta.CloseDB()
    End Sub



    Private Sub CreaDocComuni()
        Dim i As Integer
        Dim x As Integer
        Dim Centroservizio As String = ""
        Dim CodiceOspite As Long = 0
        Dim CodiceComune As String = ""
        Dim OldCodiceComune As String = ""
        Dim OldCodiceOspite As Long = 0
        Dim MaxAddebito As Long = 0
        Dim MaxAccredito As Long = 0


        Dim Provincia As String
        Dim Comune As String

        Dim CodiceIVA As String

        Dim BolloImpo As Double        
        Dim Bollo As Integer
        Dim IVABollo As String
        Dim SalvaId As Long

        Dim ImportoRetta As Double
        Dim Sconto As Double
        Dim EM_Retta As New Cls_EmissioneRetta

        Dim ImportoIVA As Double
        Dim ImportoRegistrazione As Double

        EM_Retta.ConnessioneGenerale = Session("DC_GENERALE")
        EM_Retta.ConnessioneOspiti = Session("DC_OSPITE")
        EM_Retta.ConnessioneTabelle = Session("DC_TABELLE")

        EM_Retta.ApriDB()

        EM_Retta.EliminaTemporanei()


        Tabella = ViewState("SalvaTabella")


        If DD_Ente.SelectedValue <> "" Then
            For i = 0 To Tabella.Rows.Count - 1
                Tabella.Rows(i).Item(5) = DD_Ente.SelectedValue.Replace(" ", "") & Space(6 - Len(DD_Ente.SelectedValue.Replace(" ", "")))
            Next
        End If

        Dim Xk As New Cls_CentroServizio


        Xk.Leggi(Session("DC_OSPITE"), DD_CentroServizio.SelectedValue)

        CodiceOspite = Tabella.Rows(0).Item(0)
        For i = 0 To Tabella.Rows.Count - 1

            Centroservizio = DD_CentroServizio.SelectedValue
            CodiceOspite = Tabella.Rows(i).Item(0)
            CodiceComune = Tabella.Rows(i).Item(5)
            Dim CheckBoxV As CheckBox = DirectCast(Grd_AggiornaRette.Rows(i).FindControl("Chk_Seleziona"), CheckBox)


            If CheckBoxV.Checked = True Then
                If CodiceComune <> OldCodiceComune And OldCodiceComune <> "" Then
                    Provincia = Mid(OldCodiceComune, 1, 3)
                    Comune = Mid(OldCodiceComune, 4, 3)

                    Dim xs As New ClsComune


                    xs.Provincia = Provincia
                    xs.Comune = Comune
                    xs.Leggi(Session("DC_OSPITE"))


                    Dim DCr As New Cls_DatiComnueRegioneServizio

                    DCr.CodiceProvincia = xs.Provincia
                    DCr.CodiceComune = xs.Comune
                    DCr.CentroServizio = Centroservizio
                    DCr.Leggi(Session("DC_OSPITE"))
                    If DCr.TipoOperazione <> "" Then
                        xs.TipoOperazione = DCr.TipoOperazione
                        xs.CodiceIva = DCr.AliquotaIva
                    End If


                    Dim TipoOperasazione As New Cls_TipoOperazione

                    TipoOperasazione.Leggi(Session("DC_OSPITE"), xs.TipoOperazione)

                    EM_Retta.GlbTipoOperazione = xs.TipoOperazione

                    CodiceIVA = xs.CodiceIva

                    Dim AliquotaIva As New Cls_IVA
                    Dim AliquotaBollo As New Cls_IVA
                    Dim CausaleRetta As String

                    AliquotaIva.Leggi(Session("DC_TABELLE"), CodiceIVA)


                    If ImportoRetta > 0 Then
                        CausaleRetta = TipoOperasazione.CausaleAddebito
                    Else
                        CausaleRetta = TipoOperasazione.CausaleStorno
                    End If

                    If Not EM_Retta.CreaRigaTesta(Txt_Anno.Text, Dd_Mese.SelectedValue, DD_CentroServizio.SelectedValue, CausaleRetta, SalvaId, Txt_DataRegistrazione.Text, "C" & OldCodiceComune, 0) Then
                        'errore
                        Exit Sub
                    End If



                    ImportoIVA = Modulo.MathRound(AliquotaIva.Aliquota * Math.Abs(ImportoRetta), 2)
                    ImportoRegistrazione = Modulo.MathRound(Math.Abs(ImportoRetta + ImportoIVA), 2)

                    If TipoOperasazione.SoggettaABollo = "" Or (TipoOperasazione.SoggettaABollo = "F" And CausaleRetta = TipoOperasazione.CausaleStorno) Then
                        BolloImpo = 0
                        Bollo = 0
                        IVABollo = ""
                    Else
                        Dim KBollo As New Cls_bolli



                        KBollo.Leggi(Session("DC_TABELLE"), Txt_DataRegistrazione.Text)
                        'And (TipoOperasazione.NonBolloConIVA = 0 Or (TipoOperasazione.NonBolloConIVA = 1 And ImportoIVA = 0))

                        If KBollo.ImportoApplicazione < Math.Round(ImportoRegistrazione, 2) Then
                            BolloImpo = KBollo.ImportoBollo
                            Bollo = KBollo.ImportoBollo
                            IVABollo = KBollo.CodiceIVA

                            AliquotaBollo.Leggi(Session("DC_TABELLE"), IVABollo)
                        Else
                            BolloImpo = 0
                            Bollo = 0
                            IVABollo = ""
                        End If
                    End If

                    Dim Causale As New Cls_CausaleContabile

                    Causale.Leggi(Session("DC_TABELLE"), CausaleRetta)
                    If ImportoRetta > 0 Then
                        If Causale.Righe(8).DareAvere = "D" Then
                            ImportoRegistrazione = ImportoRegistrazione - BolloImpo
                        Else
                            ImportoRegistrazione = ImportoRegistrazione + BolloImpo
                        End If
                    Else
                        If Causale.Righe(8).DareAvere = "A" Then
                            ImportoRegistrazione = ImportoRegistrazione - BolloImpo
                        Else
                            ImportoRegistrazione = ImportoRegistrazione + BolloImpo
                        End If
                    End If

                    If xs.ImportoSconto > 0 Then
                        Sconto = Modulo.MathRound(ImportoRegistrazione * xs.ImportoSconto / 100, 2)
                        ImportoRegistrazione = ImportoRegistrazione - Modulo.MathRound(ImportoRegistrazione * xs.ImportoSconto / 100, 2)
                    End If


                    If Not EM_Retta.CreaRigaCliente(CausaleRetta, SalvaId, xs.MastroCliente, xs.ContoCliente, xs.SottocontoCliente, ImportoRegistrazione) Then Exit Sub

                    If BolloImpo <> 0 And IVABollo <> AliquotaIva.Codice Then
                        If Not EM_Retta.CreaRigaIVA(CausaleRetta, SalvaId, xs.MastroCliente, xs.ContoCliente, xs.SottocontoCliente, AliquotaIva.Codice, ImportoIVA, ImportoRetta - ImportoIVA) Then Exit Sub
                        If ImportoRetta > 0 Then
                            If Not EM_Retta.CreaRigaIVA(CausaleRetta, SalvaId, xs.MastroCliente, xs.ContoCliente, xs.SottocontoCliente, IVABollo, 0, BolloImpo) Then Exit Sub
                        Else
                            Dim CausaleContabile As New Cls_CausaleContabile

                            CausaleContabile.Codice = CausaleRetta
                            CausaleContabile.Leggi(Session("DC_TABELLE"), CausaleRetta)

                            If CausaleContabile.Righe(8).DareAvere = CausaleContabile.Righe(1).DareAvere Then
                                If Not EM_Retta.CreaRigaIVA(CausaleRetta, SalvaId, xs.MastroCliente, xs.ContoCliente, xs.SottocontoCliente, IVABollo, 0, BolloImpo) Then Exit Sub
                            Else
                                If Not EM_Retta.CreaRigaIVA(CausaleRetta, SalvaId, xs.MastroCliente, xs.ContoCliente, xs.SottocontoCliente, IVABollo, 0, BolloImpo, True) Then Exit Sub
                            End If
                        End If
                    Else
                        If Not EM_Retta.CreaRigaIVA(CausaleRetta, SalvaId, xs.MastroCliente, xs.ContoCliente, xs.SottocontoCliente, AliquotaIva.Codice, ImportoIVA, ImportoRegistrazione - ImportoIVA) Then Exit Sub
                    End If

                    If BolloImpo > 0 Then
                        If Not EM_Retta.CreaRigaBollo(CausaleRetta, SalvaId, xs.MastroCliente, xs.ContoCliente, xs.SottocontoCliente, IVABollo, Bollo, "Bollo ") Then Exit Sub
                    End If

                    
                    If BolloImpo > 0 Or TipoOperasazione.BolloVirtuale = 1 Then
                        Dim CausaleContabile As New Cls_CausaleContabile

                        CausaleContabile.Codice = CausaleRetta
                        CausaleContabile.Leggi(Session("DC_TABELLE"), CausaleRetta)
                        Dim RegistroIVA As New Cls_RegistroIVA

                        RegistroIVA.RegistroCartaceo = 0
                        RegistroIVA.Tipo = CausaleContabile.RegistroIVA
                        RegistroIVA.Leggi(Session("DC_TABELLE"), RegistroIVA.Tipo)

                        If RegistroIVA.RegistroCartaceo = 0 Then
                            AssegnaBolloVirtuale(SalvaId)
                        End If
                    End If

                    Dim Imposta(100) As Double
                    Dim TipoIVA(100) As String
                    Dim Max As Integer = 0

                    For x = i - 1 To 0 Step -1
                        If OldCodiceComune <> Tabella.Rows(x).Item(5) Then
                            Exit For
                        End If

                        Dim CheckBoxV2 As CheckBox = DirectCast(Grd_AggiornaRette.Rows(x).FindControl("Chk_Seleziona"), CheckBox)


                        If CheckBoxV2.Checked = True Then
                            If Tabella.Rows(x).Item(3) = "AD" Then
                                Dim TipoAdd As New Cls_Addebito

                                TipoAdd.Codice = Tabella.Rows(x).Item(10)
                                TipoAdd.Leggi(Session("DC_OSPITE"), TipoAdd.Codice)

                                Dim IVA As New Cls_IVA

                                IVA.Codice = TipoAdd.CodiceIVA
                                IVA.Leggi(Session("DC_TABELLE"), IVA.Codice)

                                Dim Cerca As Integer

                                For Cerca = 0 To 100
                                    If TipoIVA(Cerca) = Tabella.Rows(x).Item(10) Then
                                        Imposta(Cerca) = Imposta(Cerca) + (CDbl(Tabella.Rows(x).Item(6)) - Modulo.MathRound((CDbl(Tabella.Rows(x).Item(6)) * 100) / (100 + (IVA.Aliquota * 100)), 10))
                                        Exit For
                                    End If
                                Next
                                If Cerca = 101 Then
                                    Imposta(Max) = (CDbl(Tabella.Rows(x).Item(6)) - Modulo.MathRound((CDbl(Tabella.Rows(x).Item(6)) * 100) / (100 + (IVA.Aliquota * 100)), 10))
                                    TipoIVA(Max) = Tabella.Rows(x).Item(10)
                                    Max = Max + 1
                                End If


                                If Not EM_Retta.CreaRigaAddebiti(CausaleRetta, SalvaId, Xk.MASTRO, Xk.CONTO, Tabella.Rows(x).Item(0) * 100, AliquotaIva.Codice, CDbl(Tabella.Rows(x).Item(6)), "Addebito " & Tabella.Rows(x).Item(9), 0, Tabella.Rows(x).Item(10)) Then Exit Sub

                            Else
                                Dim TipoAdd As New Cls_Addebito

                                TipoAdd.Codice = Tabella.Rows(x).Item(10)
                                TipoAdd.Leggi(Session("DC_OSPITE"), TipoAdd.Codice)

                                Dim IVA As New Cls_IVA

                                IVA.Codice = TipoAdd.CodiceIVA
                                IVA.Leggi(Session("DC_TABELLE"), IVA.Codice)


                                Dim Cerca As Integer

                                For Cerca = 0 To 100
                                    If TipoIVA(Cerca) = Tabella.Rows(x).Item(10) Then
                                        Imposta(Cerca) = Imposta(Cerca) - (CDbl(Tabella.Rows(x).Item(6)) - Modulo.MathRound((CDbl(Tabella.Rows(x).Item(6)) * 100) / (100 + (IVA.Aliquota * 100)), 10))
                                        Exit For
                                    End If
                                Next
                                If Cerca = 101 Then
                                    Imposta(Max) = (CDbl(Tabella.Rows(x).Item(6)) - Modulo.MathRound((CDbl(Tabella.Rows(x).Item(6)) * 100) / (100 + (IVA.Aliquota * 100)), 10)) * -1
                                    TipoIVA(Max) = Tabella.Rows(x).Item(10)
                                    Max = Max + 1
                                End If


                                If Not EM_Retta.CreaRigaAccrediti(CausaleRetta, SalvaId, Xk.MASTRO, Xk.CONTO, Tabella.Rows(x).Item(0) * 100, AliquotaIva.Codice, CDbl(Tabella.Rows(x).Item(6)), "Accredito " & Tabella.Rows(x).Item(9), 0, Tabella.Rows(x).Item(10)) Then Exit Sub

                            End If
                        End If
                    Next


                    If TipoOperasazione.SCORPORAIVA > 0 Then

                        Dim Cerca As Integer

                        For Cerca = 0 To Max
                            If Imposta(Cerca) <> 0 Then
                                Dim TipoAdd As New Cls_Addebito

                                TipoAdd.Codice = TipoIVA(Cerca)
                                TipoAdd.Leggi(Session("DC_OSPITE"), TipoAdd.Codice)

                                Dim IVA As New Cls_IVA

                                IVA.Codice = TipoAdd.CodiceIVA
                                IVA.Leggi(Session("DC_TABELLE"), IVA.Codice)

                                If Imposta(Cerca) > 0 Then
                                    If Not EM_Retta.CreaRigaAccrediti(CausaleRetta, SalvaId, 0, 0, 0, TipoAdd.CodiceIVA, Math.Abs(Imposta(Cerca)), "Scorporo IVA", 0, TipoIVA(Cerca)) Then Exit Sub
                                Else
                                    If Not EM_Retta.CreaRigaAddebiti(CausaleRetta, SalvaId, 0, 0, 0, TipoAdd.CodiceIVA, Math.Abs(Imposta(Cerca)), "Scorporo IVA", 0, TipoIVA(Cerca)) Then Exit Sub
                                End If
                            End If
                        Next

                    End If

                    ImportoRegistrazione = 0
                    ImportoRetta = 0
                End If

                If Tabella.Rows(i).Item(3) = "AD" Then
                    ImportoRetta = ImportoRetta + CDbl(Tabella.Rows(i).Item(6))
                Else
                    ImportoRetta = ImportoRetta - CDbl(Tabella.Rows(i).Item(6))

                End If
                OldCodiceComune = CodiceComune
            End If
        Next

        If Math.Abs(ImportoRetta) > 0 Then
            Provincia = Mid(OldCodiceComune, 1, 3)
            Comune = Mid(OldCodiceComune, 4, 3)

            Dim xs As New ClsComune


            xs.Provincia = Provincia
            xs.Comune = Comune
            xs.Leggi(Session("DC_OSPITE"))

            Dim DCr As New Cls_DatiComnueRegioneServizio

            DCr.CodiceProvincia = xs.Provincia
            DCr.CodiceComune = xs.Comune
            DCr.CentroServizio = Centroservizio
            DCr.Leggi(Session("DC_OSPITE"))
            If DCr.TipoOperazione <> "" Then
                xs.TipoOperazione = DCr.TipoOperazione
                xs.CodiceIva = DCr.AliquotaIva
            End If


            Dim TipoOperasazione As New Cls_TipoOperazione

            TipoOperasazione.Leggi(Session("DC_OSPITE"), xs.TipoOperazione)


            CodiceIVA = xs.CodiceIva

            Dim AliquotaIva As New Cls_IVA
            Dim AliquotaBollo As New Cls_IVA
            Dim CausaleRetta As String

            AliquotaIva.Leggi(Session("DC_TABELLE"), CodiceIVA)


            If ImportoRetta > 0 Then
                CausaleRetta = TipoOperasazione.CausaleAddebito
            Else
                CausaleRetta = TipoOperasazione.CausaleStorno
            End If

            If Not EM_Retta.CreaRigaTesta(Txt_Anno.Text, Dd_Mese.SelectedValue, DD_CentroServizio.SelectedValue, CausaleRetta, SalvaId, Txt_DataRegistrazione.Text, "C" & OldCodiceComune, 0) Then
                'errore
                Exit Sub
            End If



            ImportoIVA = Modulo.MathRound(AliquotaIva.Aliquota * Math.Abs(ImportoRetta), 2)
            ImportoRegistrazione = Modulo.MathRound(Math.Abs(ImportoRetta + ImportoIVA), 2)

            If TipoOperasazione.SoggettaABollo = "" Or (TipoOperasazione.SoggettaABollo = "F" And CausaleRetta = TipoOperasazione.CausaleStorno) Then
                BolloImpo = 0
                Bollo = 0
                IVABollo = ""
            Else
                Dim KBollo As New Cls_bolli

                KBollo.Leggi(Session("DC_TABELLE"), Txt_DataRegistrazione.Text)
                ''CercaCAusale

                'And (TipoOperasazione.NonBolloConIVA = 0 Or (TipoOperasazione.NonBolloConIVA = 1 And ImportoIVA = 0)) 

                If KBollo.ImportoApplicazione < Math.Round(ImportoRegistrazione, 2) Then
                    BolloImpo = KBollo.ImportoBollo
                    Bollo = KBollo.ImportoBollo
                    IVABollo = KBollo.CodiceIVA

                    AliquotaBollo.Leggi(Session("DC_TABELLE"), IVABollo)
                Else
                    BolloImpo = 0
                    Bollo = 0
                    IVABollo = ""
                End If
                
            End If

            Dim Causale As New Cls_CausaleContabile

            Causale.Leggi(Session("DC_TABELLE"), CausaleRetta)
            If ImportoRetta > 0 Then
                If Causale.Righe(8).DareAvere = "D" Then
                    ImportoRegistrazione = ImportoRegistrazione - BolloImpo
                Else
                    ImportoRegistrazione = ImportoRegistrazione + BolloImpo
                End If
            Else
                If Causale.Righe(8).DareAvere = "A" Then
                    ImportoRegistrazione = ImportoRegistrazione - BolloImpo
                Else
                    ImportoRegistrazione = ImportoRegistrazione + BolloImpo
                End If
            End If

            If xs.ImportoSconto > 0 Then
                Sconto = Modulo.MathRound(ImportoRegistrazione * xs.ImportoSconto / 100, 2)
                ImportoRegistrazione = ImportoRegistrazione - Modulo.MathRound(ImportoRegistrazione * xs.ImportoSconto / 100, 2)
            End If



            If Not EM_Retta.CreaRigaCliente(CausaleRetta, SalvaId, xs.MastroCliente, xs.ContoCliente, xs.SottocontoCliente, ImportoRegistrazione) Then Exit Sub

            Dim Imposta(100) As Double
            Dim TipoIVA(100) As String
            Dim Max As Integer = 0

            If BolloImpo <> 0 And IVABollo <> AliquotaIva.Codice Then
                If Not EM_Retta.CreaRigaIVA(CausaleRetta, SalvaId, xs.MastroCliente, xs.ContoCliente, xs.SottocontoCliente, AliquotaIva.Codice, ImportoIVA, Math.Abs(ImportoRetta)) Then Exit Sub
                If ImportoRetta > 0 Then
                    If Not EM_Retta.CreaRigaIVA(CausaleRetta, SalvaId, xs.MastroCliente, xs.ContoCliente, xs.SottocontoCliente, IVABollo, 0, BolloImpo) Then Exit Sub
                Else
                    Dim CausaleContabile As New Cls_CausaleContabile

                    CausaleContabile.Codice = CausaleRetta
                    CausaleContabile.Leggi(Session("DC_TABELLE"), CausaleRetta)

                    If CausaleContabile.Righe(8).DareAvere = CausaleContabile.Righe(1).DareAvere Then
                        If Not EM_Retta.CreaRigaIVA(CausaleRetta, SalvaId, xs.MastroCliente, xs.ContoCliente, xs.SottocontoCliente, IVABollo, 0, BolloImpo) Then Exit Sub
                    Else
                        If Not EM_Retta.CreaRigaIVA(CausaleRetta, SalvaId, xs.MastroCliente, xs.ContoCliente, xs.SottocontoCliente, IVABollo, 0, BolloImpo, True) Then Exit Sub
                    End If
                End If
            Else
                If Not EM_Retta.CreaRigaIVA(CausaleRetta, SalvaId, xs.MastroCliente, xs.ContoCliente, xs.SottocontoCliente, AliquotaIva.Codice, ImportoIVA, Math.Abs(ImportoRetta)) Then Exit Sub
            End If

            If BolloImpo > 0 Then
                If Not EM_Retta.CreaRigaBollo(CausaleRetta, SalvaId, xs.MastroCliente, xs.ContoCliente, xs.SottocontoCliente, IVABollo, Bollo, "Bollo ") Then Exit Sub
            End If
            If BolloImpo > 0 Or TipoOperasazione.BolloVirtuale = 1 Then
                Dim CausaleContabile As New Cls_CausaleContabile

                CausaleContabile.Codice = CausaleRetta
                CausaleContabile.Leggi(Session("DC_TABELLE"), CausaleRetta)
                Dim RegistroIVA As New Cls_RegistroIVA

                RegistroIVA.RegistroCartaceo = 0
                RegistroIVA.Tipo = CausaleContabile.RegistroIVA
                RegistroIVA.Leggi(Session("DC_TABELLE"), RegistroIVA.Tipo)

                If RegistroIVA.RegistroCartaceo = 0 Then
                    AssegnaBolloVirtuale(SalvaId)
                End If
            End If

            For x = i - 1 To 0 Step -1


                Dim CheckBoxV1 As CheckBox = DirectCast(Grd_AggiornaRette.Rows(x).FindControl("Chk_Seleziona"), CheckBox)


                If CheckBoxV1.Checked = True Then
                    If OldCodiceComune = Tabella.Rows(x).Item(5) Then


                        If Tabella.Rows(x).Item(3) = "AD" Then
                            Dim TipoAdd As New Cls_Addebito

                            TipoAdd.Codice = Tabella.Rows(x).Item(10)
                            TipoAdd.Leggi(Session("DC_OSPITE"), TipoAdd.Codice)

                            Dim IVA As New Cls_IVA

                            IVA.Codice = TipoAdd.CodiceIVA
                            IVA.Leggi(Session("DC_TABELLE"), IVA.Codice)

                            Dim Cerca As Integer

                            For Cerca = 0 To 100
                                If TipoIVA(Cerca) = Tabella.Rows(x).Item(10) Then
                                    Imposta(Cerca) = Imposta(Cerca) + (CDbl(Tabella.Rows(x).Item(6)) - Modulo.MathRound((CDbl(Tabella.Rows(x).Item(6)) * 100) / (100 + (IVA.Aliquota * 100)), 10))
                                    Exit For
                                End If
                            Next
                            If Cerca = 101 Then
                                Imposta(Max) = (CDbl(Tabella.Rows(x).Item(6)) - Modulo.MathRound((CDbl(Tabella.Rows(x).Item(6)) * 100) / (100 + (IVA.Aliquota * 100)), 10))
                                TipoIVA(Max) = Tabella.Rows(x).Item(10)
                                Max = Max + 1
                            End If



                            If Not EM_Retta.CreaRigaAddebiti(CausaleRetta, SalvaId, Xk.MASTRO, Xk.CONTO, Tabella.Rows(x).Item(0) * 100, AliquotaIva.Codice, CDbl(Tabella.Rows(x).Item(6)), "Addebito " & Tabella.Rows(x).Item(9), 0, Tabella.Rows(x).Item(10)) Then Exit Sub


                        Else

                            Dim TipoAdd As New Cls_Addebito

                            TipoAdd.Codice = Tabella.Rows(x).Item(10)
                            TipoAdd.Leggi(Session("DC_OSPITE"), TipoAdd.Codice)

                            Dim IVA As New Cls_IVA

                            IVA.Codice = TipoAdd.CodiceIVA
                            IVA.Leggi(Session("DC_TABELLE"), IVA.Codice)


                            Dim Cerca As Integer

                            For Cerca = 0 To 100
                                If TipoIVA(Cerca) = Tabella.Rows(x).Item(10) Then
                                    Imposta(Cerca) = Imposta(Cerca) - (CDbl(Tabella.Rows(x).Item(6)) - Modulo.MathRound((CDbl(Tabella.Rows(x).Item(6)) * 100) / (100 + (IVA.Aliquota * 100)), 10))
                                    Exit For
                                End If
                            Next
                            If Cerca = 101 Then
                                Imposta(Max) = (CDbl(Tabella.Rows(x).Item(6)) - Modulo.MathRound((CDbl(Tabella.Rows(x).Item(6)) * 100) / (100 + (IVA.Aliquota * 100)), 10)) * -1
                                TipoIVA(Max) = Tabella.Rows(x).Item(10)
                                Max = Max + 1
                            End If

                            If Not EM_Retta.CreaRigaAccrediti(CausaleRetta, SalvaId, Xk.MASTRO, Xk.CONTO, Tabella.Rows(x).Item(0) * 100, AliquotaIva.Codice, CDbl(Tabella.Rows(x).Item(6)), "Accredito " & Tabella.Rows(x).Item(9), 0, Tabella.Rows(x).Item(10)) Then Exit Sub



                        End If
                    End If
                End If
            Next

            If TipoOperasazione.SCORPORAIVA > 0 Then

                Dim Cerca As Integer

                For Cerca = 0 To Max
                    If Imposta(Cerca) <> 0 Then
                        Dim TipoAdd As New Cls_Addebito

                        TipoAdd.Codice = TipoIVA(Cerca)
                        TipoAdd.Leggi(Session("DC_OSPITE"), TipoAdd.Codice)

                        Dim IVA As New Cls_IVA

                        IVA.Codice = TipoAdd.CodiceIVA
                        IVA.Leggi(Session("DC_TABELLE"), IVA.Codice)

                        If Imposta(Cerca) > 0 Then
                            If Not EM_Retta.CreaRigaAccrediti(CausaleRetta, SalvaId, 0, 0, 0, TipoAdd.CodiceIVA, Math.Abs(Imposta(Cerca)), "Scorporo IVA", 0, TipoIVA(Cerca)) Then Exit Sub
                        Else
                            If Not EM_Retta.CreaRigaAddebiti(CausaleRetta, SalvaId, 0, 0, 0, TipoAdd.CodiceIVA, Math.Abs(Imposta(Cerca)), "Scorporo IVA", 0, TipoIVA(Cerca)) Then Exit Sub
                        End If
                    End If
                Next

            End If

        End If

        EM_Retta.Commit()
        EM_Retta.CloseDB()
    End Sub


    Private Sub CreaDocJolly()
        Dim i As Integer
        Dim x As Integer
        Dim Centroservizio As String = ""
        Dim CodiceOspite As Long = 0
        Dim CodiceComune As String = ""
        Dim OldCodiceComune As String = ""
        Dim OldCodiceOspite As Long = 0
        Dim MaxAddebito As Long = 0
        Dim MaxAccredito As Long = 0


        Dim Provincia As String
        Dim Comune As String

        Dim CodiceIVA As String

        Dim BolloImpo As Double
        Dim Bollo As Integer
        Dim IVABollo As String
        Dim SalvaId As Long

        Dim ImportoRetta As Double
        Dim Sconto As Double
        Dim EM_Retta As New Cls_EmissioneRetta

        Dim ImportoIVA As Double
        Dim ImportoRegistrazione As Double

        EM_Retta.ConnessioneGenerale = Session("DC_GENERALE")
        EM_Retta.ConnessioneOspiti = Session("DC_OSPITE")
        EM_Retta.ConnessioneTabelle = Session("DC_TABELLE")

        EM_Retta.ApriDB()

        EM_Retta.EliminaTemporanei()


        Tabella = ViewState("SalvaTabella")


        If DD_Ente.SelectedValue <> "" Then
            For i = 0 To Tabella.Rows.Count - 1
                Tabella.Rows(i).Item(5) = DD_Ente.SelectedValue.Replace(" ", "") & Space(6 - Len(DD_Ente.SelectedValue.Replace(" ", "")))
            Next
        End If

        Dim Xk As New Cls_CentroServizio


        Xk.Leggi(Session("DC_OSPITE"), DD_CentroServizio.SelectedValue)

        CodiceOspite = Tabella.Rows(0).Item(0)
        For i = 0 To Tabella.Rows.Count - 1

            Centroservizio = DD_CentroServizio.SelectedValue
            CodiceOspite = Tabella.Rows(i).Item(0)
            CodiceComune = Tabella.Rows(i).Item(5)
            Dim CheckBoxV As CheckBox = DirectCast(Grd_AggiornaRette.Rows(i).FindControl("Chk_Seleziona"), CheckBox)


            If CheckBoxV.Checked = True Then
                If CodiceComune <> OldCodiceComune And OldCodiceComune <> "" Then
                    Provincia = Mid(OldCodiceComune, 1, 3)
                    Comune = Mid(OldCodiceComune, 4, 3)

                    Dim xs As New ClsComune


                    xs.Provincia = Provincia
                    xs.Comune = Comune
                    xs.Leggi(Session("DC_OSPITE"))


                    Dim DCr As New Cls_DatiComnueRegioneServizio

                    DCr.CodiceProvincia = xs.Provincia
                    DCr.CodiceComune = xs.Comune
                    DCr.CentroServizio = Centroservizio
                    DCr.Leggi(Session("DC_OSPITE"))
                    If DCr.TipoOperazione <> "" Then
                        xs.TipoOperazione = DCr.TipoOperazione
                        xs.CodiceIva = DCr.AliquotaIva
                    End If


                    Dim TipoOperasazione As New Cls_TipoOperazione

                    TipoOperasazione.Leggi(Session("DC_OSPITE"), xs.TipoOperazione)

                    EM_Retta.GlbTipoOperazione = xs.TipoOperazione

                    CodiceIVA = xs.CodiceIva

                    Dim AliquotaIva As New Cls_IVA
                    Dim AliquotaBollo As New Cls_IVA
                    Dim CausaleRetta As String

                    AliquotaIva.Leggi(Session("DC_TABELLE"), CodiceIVA)


                    If ImportoRetta > 0 Then
                        CausaleRetta = TipoOperasazione.CausaleAddebito
                    Else
                        CausaleRetta = TipoOperasazione.CausaleStorno
                    End If

                    If Not EM_Retta.CreaRigaTesta(Txt_Anno.Text, Dd_Mese.SelectedValue, DD_CentroServizio.SelectedValue, CausaleRetta, SalvaId, Txt_DataRegistrazione.Text, "J" & OldCodiceComune, 0) Then
                        'errore
                        Exit Sub
                    End If



                    ImportoIVA = Modulo.MathRound(AliquotaIva.Aliquota * Math.Abs(ImportoRetta), 2)
                    ImportoRegistrazione = Modulo.MathRound(Math.Abs(ImportoRetta + ImportoIVA), 2)

                    If TipoOperasazione.SoggettaABollo = "" Or (TipoOperasazione.SoggettaABollo = "F" And CausaleRetta = TipoOperasazione.CausaleStorno) Then
                        BolloImpo = 0
                        Bollo = 0
                        IVABollo = ""
                    Else
                        Dim KBollo As New Cls_bolli

                        KBollo.Leggi(Session("DC_TABELLE"), Txt_DataRegistrazione.Text)
                        'And (TipoOperasazione.NonBolloConIVA = 0  Or (TipoOperasazione.NonBolloConIVA = 1 And ImportoIVA = 0)) 

                        If KBollo.ImportoApplicazione < Math.Round(ImportoRegistrazione, 2) Then
                            BolloImpo = KBollo.ImportoBollo
                            Bollo = KBollo.ImportoBollo
                            IVABollo = KBollo.CodiceIVA

                            AliquotaBollo.Leggi(Session("DC_TABELLE"), IVABollo)
                        Else
                            BolloImpo = 0
                            Bollo = 0
                            IVABollo = ""
                        End If
                    End If

                    Dim Causale As New Cls_CausaleContabile

                    Causale.Leggi(Session("DC_TABELLE"), CausaleRetta)
                    If ImportoRetta > 0 Then
                        If Causale.Righe(8).DareAvere = "D" Then
                            ImportoRegistrazione = ImportoRegistrazione - BolloImpo
                        Else
                            ImportoRegistrazione = ImportoRegistrazione + BolloImpo
                        End If
                    Else
                        If Causale.Righe(8).DareAvere = "A" Then
                            ImportoRegistrazione = ImportoRegistrazione - BolloImpo
                        Else
                            ImportoRegistrazione = ImportoRegistrazione + BolloImpo
                        End If
                    End If

                    If xs.ImportoSconto > 0 Then
                        Sconto = Modulo.MathRound(ImportoRegistrazione * xs.ImportoSconto / 100, 2)
                        ImportoRegistrazione = ImportoRegistrazione - Modulo.MathRound(ImportoRegistrazione * xs.ImportoSconto / 100, 2)
                    End If


                    If Not EM_Retta.CreaRigaCliente(CausaleRetta, SalvaId, xs.MastroCliente, xs.ContoCliente, xs.SottocontoCliente, ImportoRegistrazione) Then Exit Sub

                    If BolloImpo <> 0 And IVABollo <> AliquotaIva.Codice Then
                        If Not EM_Retta.CreaRigaIVA(CausaleRetta, SalvaId, xs.MastroCliente, xs.ContoCliente, xs.SottocontoCliente, AliquotaIva.Codice, ImportoIVA, ImportoRetta - ImportoIVA) Then Exit Sub
                        If ImportoRetta > 0 Then
                            If Not EM_Retta.CreaRigaIVA(CausaleRetta, SalvaId, xs.MastroCliente, xs.ContoCliente, xs.SottocontoCliente, IVABollo, 0, BolloImpo) Then Exit Sub
                        Else
                            Dim CausaleContabile As New Cls_CausaleContabile

                            CausaleContabile.Codice = CausaleRetta
                            CausaleContabile.Leggi(Session("DC_TABELLE"), CausaleRetta)

                            If CausaleContabile.Righe(8).DareAvere = CausaleContabile.Righe(1).DareAvere Then
                                If Not EM_Retta.CreaRigaIVA(CausaleRetta, SalvaId, xs.MastroCliente, xs.ContoCliente, xs.SottocontoCliente, IVABollo, 0, BolloImpo) Then Exit Sub
                            Else
                                If Not EM_Retta.CreaRigaIVA(CausaleRetta, SalvaId, xs.MastroCliente, xs.ContoCliente, xs.SottocontoCliente, IVABollo, 0, BolloImpo, True) Then Exit Sub
                            End If
                        End If
                    Else
                        If Not EM_Retta.CreaRigaIVA(CausaleRetta, SalvaId, xs.MastroCliente, xs.ContoCliente, xs.SottocontoCliente, AliquotaIva.Codice, ImportoIVA, ImportoRegistrazione - ImportoIVA) Then Exit Sub
                    End If

                    If BolloImpo > 0 Then
                        If Not EM_Retta.CreaRigaBollo(CausaleRetta, SalvaId, xs.MastroCliente, xs.ContoCliente, xs.SottocontoCliente, IVABollo, Bollo, "Bollo ") Then Exit Sub
                    End If
                    If BolloImpo > 0 Or TipoOperasazione.BolloVirtuale = 1 Then
                        Dim CausaleContabile As New Cls_CausaleContabile

                        CausaleContabile.Codice = CausaleRetta
                        CausaleContabile.Leggi(Session("DC_TABELLE"), CausaleRetta)
                        Dim RegistroIVA As New Cls_RegistroIVA

                        RegistroIVA.RegistroCartaceo = 0
                        RegistroIVA.Tipo = CausaleContabile.RegistroIVA
                        RegistroIVA.Leggi(Session("DC_TABELLE"), RegistroIVA.Tipo)

                        If RegistroIVA.RegistroCartaceo = 0 Then
                            AssegnaBolloVirtuale(SalvaId)
                        End If
                    End If

                    Dim Imposta(100) As Double
                    Dim TipoIVA(100) As String
                    Dim Max As Integer = 0

                    For x = i - 1 To 0 Step -1
                        If OldCodiceComune <> Tabella.Rows(x).Item(5) Then
                            Exit For
                        End If

                        Dim CheckBoxV2 As CheckBox = DirectCast(Grd_AggiornaRette.Rows(x).FindControl("Chk_Seleziona"), CheckBox)


                        If CheckBoxV2.Checked = True Then
                            If Tabella.Rows(x).Item(3) = "AD" Then
                                Dim TipoAdd As New Cls_Addebito

                                TipoAdd.Codice = Tabella.Rows(x).Item(10)
                                TipoAdd.Leggi(Session("DC_OSPITE"), TipoAdd.Codice)

                                Dim IVA As New Cls_IVA

                                IVA.Codice = TipoAdd.CodiceIVA
                                IVA.Leggi(Session("DC_TABELLE"), IVA.Codice)


                                Dim Cerca As Integer

                                For Cerca = 0 To 100
                                    If TipoIVA(Cerca) = Tabella.Rows(x).Item(10) Then
                                        Imposta(Cerca) = Imposta(Cerca) + (CDbl(Tabella.Rows(x).Item(6)) - Modulo.MathRound((CDbl(Tabella.Rows(x).Item(6)) * 100) / (100 + (IVA.Aliquota * 100)), 10))
                                        Exit For
                                    End If
                                Next
                                If Cerca = 101 Then
                                    Imposta(Max) = (CDbl(Tabella.Rows(x).Item(6)) - Modulo.MathRound((CDbl(Tabella.Rows(x).Item(6)) * 100) / (100 + (IVA.Aliquota * 100)), 10))
                                    TipoIVA(Max) = Tabella.Rows(x).Item(10)
                                    Max = Max + 1
                                End If

                                If Not EM_Retta.CreaRigaAddebiti(CausaleRetta, SalvaId, Xk.MASTRO, Xk.CONTO, Tabella.Rows(x).Item(0) * 100, AliquotaIva.Codice, CDbl(Tabella.Rows(x).Item(6)), "Addebito " & Tabella.Rows(x).Item(9), 0, Tabella.Rows(x).Item(10)) Then Exit Sub

                            Else

                                Dim TipoAdd As New Cls_Addebito

                                TipoAdd.Codice = Tabella.Rows(x).Item(10)
                                TipoAdd.Leggi(Session("DC_OSPITE"), TipoAdd.Codice)

                                Dim IVA As New Cls_IVA

                                IVA.Codice = TipoAdd.CodiceIVA
                                IVA.Leggi(Session("DC_TABELLE"), IVA.Codice)


                                Dim Cerca As Integer

                                For Cerca = 0 To 100
                                    If TipoIVA(Cerca) = Tabella.Rows(x).Item(10) Then
                                        Imposta(Cerca) = Imposta(Cerca) - (CDbl(Tabella.Rows(x).Item(6)) - Modulo.MathRound((CDbl(Tabella.Rows(x).Item(6)) * 100) / (100 + (IVA.Aliquota * 100)), 10))
                                        Exit For
                                    End If
                                Next
                                If Cerca = 101 Then
                                    Imposta(Max) = (CDbl(Tabella.Rows(x).Item(6)) - Modulo.MathRound((CDbl(Tabella.Rows(x).Item(6)) * 100) / (100 + (IVA.Aliquota * 100)), 10)) * -1
                                    TipoIVA(Max) = Tabella.Rows(x).Item(10)
                                    Max = Max + 1
                                End If


                                If Not EM_Retta.CreaRigaAccrediti(CausaleRetta, SalvaId, Xk.MASTRO, Xk.CONTO, Tabella.Rows(x).Item(0) * 100, AliquotaIva.Codice, CDbl(Tabella.Rows(x).Item(6)), "Accredito " & Tabella.Rows(x).Item(9), 0, Tabella.Rows(x).Item(10)) Then Exit Sub


                            End If
                        End If
                    Next

                    If TipoOperasazione.SCORPORAIVA > 0 Then

                        Dim Cerca As Integer

                        For Cerca = 0 To Max
                            If Imposta(Cerca) <> 0 Then
                                Dim TipoAdd As New Cls_Addebito

                                TipoAdd.Codice = TipoIVA(Cerca)
                                TipoAdd.Leggi(Session("DC_OSPITE"), TipoAdd.Codice)

                                Dim IVA As New Cls_IVA

                                IVA.Codice = TipoAdd.CodiceIVA
                                IVA.Leggi(Session("DC_TABELLE"), IVA.Codice)

                                If Imposta(Cerca) > 0 Then
                                    If Not EM_Retta.CreaRigaAccrediti(CausaleRetta, SalvaId, 0, 0, 0, TipoAdd.CodiceIVA, Math.Abs(Imposta(Cerca)), "Scorporo IVA", 0, TipoIVA(Cerca)) Then Exit Sub
                                Else
                                    If Not EM_Retta.CreaRigaAddebiti(CausaleRetta, SalvaId, 0, 0, 0, TipoAdd.CodiceIVA, Math.Abs(Imposta(Cerca)), "Scorporo IVA", 0, TipoIVA(Cerca)) Then Exit Sub
                                End If
                            End If
                        Next

                    End If

                    ImportoRegistrazione = 0
                    ImportoRetta = 0
                End If

                If Tabella.Rows(i).Item(3) = "AD" Then
                    ImportoRetta = ImportoRetta + CDbl(Tabella.Rows(i).Item(6))
                Else
                    ImportoRetta = ImportoRetta - CDbl(Tabella.Rows(i).Item(6))

                End If
                OldCodiceComune = CodiceComune
            End If
        Next

        If Math.Abs(ImportoRetta) > 0 Then
            Provincia = Mid(OldCodiceComune, 1, 3)
            Comune = Mid(OldCodiceComune, 4, 3)

            Dim xs As New ClsComune


            xs.Provincia = Provincia
            xs.Comune = Comune
            xs.Leggi(Session("DC_OSPITE"))

            Dim DCr As New Cls_DatiComnueRegioneServizio

            DCr.CodiceProvincia = xs.Provincia
            DCr.CodiceComune = xs.Comune
            DCr.CentroServizio = Centroservizio
            DCr.Leggi(Session("DC_OSPITE"))
            If DCr.TipoOperazione <> "" Then
                xs.TipoOperazione = DCr.TipoOperazione
                xs.CodiceIva = DCr.AliquotaIva
            End If


            Dim TipoOperasazione As New Cls_TipoOperazione

            TipoOperasazione.Leggi(Session("DC_OSPITE"), xs.TipoOperazione)


            CodiceIVA = xs.CodiceIva

            Dim AliquotaIva As New Cls_IVA
            Dim AliquotaBollo As New Cls_IVA
            Dim CausaleRetta As String

            AliquotaIva.Leggi(Session("DC_TABELLE"), CodiceIVA)


            If ImportoRetta > 0 Then
                CausaleRetta = TipoOperasazione.CausaleAddebito
            Else
                CausaleRetta = TipoOperasazione.CausaleStorno
            End If

            If Not EM_Retta.CreaRigaTesta(Txt_Anno.Text, Dd_Mese.SelectedValue, DD_CentroServizio.SelectedValue, CausaleRetta, SalvaId, Txt_DataRegistrazione.Text, "J" & OldCodiceComune, 0) Then
                'errore
                Exit Sub
            End If



            ImportoIVA = Modulo.MathRound(AliquotaIva.Aliquota * Math.Abs(ImportoRetta), 2)
            ImportoRegistrazione = Modulo.MathRound(Math.Abs(ImportoRetta + ImportoIVA), 2)

            If TipoOperasazione.SoggettaABollo = "" Or (TipoOperasazione.SoggettaABollo = "F" And CausaleRetta = TipoOperasazione.CausaleStorno) Then
                BolloImpo = 0
                Bollo = 0
                IVABollo = ""
            Else
                Dim KBollo As New Cls_bolli



                KBollo.Leggi(Session("DC_TABELLE"), Txt_DataRegistrazione.Text)
                'And (TipoOperasazione.NonBolloConIVA = 0 Or (TipoOperasazione.NonBolloConIVA = 1 And ImportoIVA = 0))
                If KBollo.ImportoApplicazione < Math.Round(ImportoRegistrazione, 2) Then
                    BolloImpo = KBollo.ImportoBollo
                    Bollo = KBollo.ImportoBollo
                    IVABollo = KBollo.CodiceIVA

                    AliquotaBollo.Leggi(Session("DC_TABELLE"), IVABollo)
                Else
                    BolloImpo = 0
                    Bollo = 0
                    IVABollo = ""
                End If
               
            End If

            Dim Causale As New Cls_CausaleContabile

            Causale.Leggi(Session("DC_TABELLE"), CausaleRetta)
            If ImportoRetta > 0 Then
                If Causale.Righe(8).DareAvere = "D" Then
                    ImportoRegistrazione = ImportoRegistrazione - BolloImpo
                Else
                    ImportoRegistrazione = ImportoRegistrazione + BolloImpo
                End If
            Else
                If Causale.Righe(8).DareAvere = "A" Then
                    ImportoRegistrazione = ImportoRegistrazione - BolloImpo
                Else
                    ImportoRegistrazione = ImportoRegistrazione + BolloImpo
                End If
            End If

            If xs.ImportoSconto > 0 Then
                Sconto = Modulo.MathRound(ImportoRegistrazione * xs.ImportoSconto / 100, 2)
                ImportoRegistrazione = ImportoRegistrazione - Modulo.MathRound(ImportoRegistrazione * xs.ImportoSconto / 100, 2)
            End If


            If Not EM_Retta.CreaRigaCliente(CausaleRetta, SalvaId, xs.MastroCliente, xs.ContoCliente, xs.SottocontoCliente, ImportoRegistrazione) Then Exit Sub

            If BolloImpo <> 0 And IVABollo <> AliquotaIva.Codice Then
                If Not EM_Retta.CreaRigaIVA(CausaleRetta, SalvaId, xs.MastroCliente, xs.ContoCliente, xs.SottocontoCliente, AliquotaIva.Codice, ImportoIVA, Math.Abs(ImportoRetta)) Then Exit Sub
                If ImportoRetta > 0 Then
                    If Not EM_Retta.CreaRigaIVA(CausaleRetta, SalvaId, xs.MastroCliente, xs.ContoCliente, xs.SottocontoCliente, IVABollo, 0, BolloImpo) Then Exit Sub
                Else
                    Dim CausaleContabile As New Cls_CausaleContabile

                    CausaleContabile.Codice = CausaleRetta
                    CausaleContabile.Leggi(Session("DC_TABELLE"), CausaleRetta)

                    If CausaleContabile.Righe(8).DareAvere = CausaleContabile.Righe(1).DareAvere Then
                        If Not EM_Retta.CreaRigaIVA(CausaleRetta, SalvaId, xs.MastroCliente, xs.ContoCliente, xs.SottocontoCliente, IVABollo, 0, BolloImpo) Then Exit Sub
                    Else
                        If Not EM_Retta.CreaRigaIVA(CausaleRetta, SalvaId, xs.MastroCliente, xs.ContoCliente, xs.SottocontoCliente, IVABollo, 0, BolloImpo, True) Then Exit Sub
                    End If
                End If
            Else
                If Not EM_Retta.CreaRigaIVA(CausaleRetta, SalvaId, xs.MastroCliente, xs.ContoCliente, xs.SottocontoCliente, AliquotaIva.Codice, ImportoIVA, Math.Abs(ImportoRetta)) Then Exit Sub
            End If

            If BolloImpo > 0 Then
                If Not EM_Retta.CreaRigaBollo(CausaleRetta, SalvaId, xs.MastroCliente, xs.ContoCliente, xs.SottocontoCliente, IVABollo, Bollo, "Bollo ") Then Exit Sub
            End If

            If BolloImpo > 0 Or TipoOperasazione.BolloVirtuale = 1 Then
                Dim CausaleContabile As New Cls_CausaleContabile

                CausaleContabile.Codice = CausaleRetta
                CausaleContabile.Leggi(Session("DC_TABELLE"), CausaleRetta)
                Dim RegistroIVA As New Cls_RegistroIVA

                RegistroIVA.RegistroCartaceo = 0
                RegistroIVA.Tipo = CausaleContabile.RegistroIVA
                RegistroIVA.Leggi(Session("DC_TABELLE"), RegistroIVA.Tipo)

                If RegistroIVA.RegistroCartaceo = 0 Then
                    AssegnaBolloVirtuale(SalvaId)
                End If
            End If

            Dim Imposta(100) As Double
            Dim TipoIVA(100) As String
            Dim Max As Integer = 0


            For x = i - 1 To 0 Step -1


                Dim CheckBoxV1 As CheckBox = DirectCast(Grd_AggiornaRette.Rows(x).FindControl("Chk_Seleziona"), CheckBox)



                If CheckBoxV1.Checked = True Then
                    If OldCodiceComune = Tabella.Rows(x).Item(5) Then


                        If Tabella.Rows(x).Item(3) = "AD" Then
                            Dim TipoAdd As New Cls_Addebito

                            TipoAdd.Codice = Tabella.Rows(x).Item(10)
                            TipoAdd.Leggi(Session("DC_OSPITE"), TipoAdd.Codice)

                            Dim IVA As New Cls_IVA

                            IVA.Codice = TipoAdd.CodiceIVA
                            IVA.Leggi(Session("DC_TABELLE"), IVA.Codice)


                            Dim Cerca As Integer

                            For Cerca = 0 To 100
                                If TipoIVA(Cerca) = Tabella.Rows(x).Item(10) Then
                                    Imposta(Cerca) = Imposta(Cerca) + (CDbl(Tabella.Rows(x).Item(6)) - Modulo.MathRound((CDbl(Tabella.Rows(x).Item(6)) * 100) / (100 + (IVA.Aliquota * 100)), 10))
                                    Exit For
                                End If
                            Next
                            If Cerca = 101 Then
                                Imposta(Max) = (CDbl(Tabella.Rows(x).Item(6)) - Modulo.MathRound((CDbl(Tabella.Rows(x).Item(6)) * 100) / (100 + (IVA.Aliquota * 100)), 10))
                                TipoIVA(Max) = Tabella.Rows(x).Item(10)
                                Max = Max + 1
                            End If



                            If Not EM_Retta.CreaRigaAddebiti(CausaleRetta, SalvaId, Xk.MASTRO, Xk.CONTO, Tabella.Rows(x).Item(0) * 100, AliquotaIva.Codice, CDbl(Tabella.Rows(x).Item(6)), "Addebito " & Tabella.Rows(x).Item(9), 0, Tabella.Rows(x).Item(10)) Then Exit Sub


                        Else

                            Dim TipoAdd As New Cls_Addebito

                            TipoAdd.Codice = Tabella.Rows(x).Item(10)
                            TipoAdd.Leggi(Session("DC_OSPITE"), TipoAdd.Codice)

                            Dim IVA As New Cls_IVA

                            IVA.Codice = TipoAdd.CodiceIVA
                            IVA.Leggi(Session("DC_TABELLE"), IVA.Codice)


                            Dim Cerca As Integer

                            For Cerca = 0 To 100
                                If TipoIVA(Cerca) = Tabella.Rows(x).Item(10) Then
                                    Imposta(Cerca) = Imposta(Cerca) - (CDbl(Tabella.Rows(x).Item(6)) - Modulo.MathRound((CDbl(Tabella.Rows(x).Item(6)) * 100) / (100 + (IVA.Aliquota * 100)), 10))
                                    Exit For
                                End If
                            Next
                            If Cerca = 101 Then
                                Imposta(Max) = (CDbl(Tabella.Rows(x).Item(6)) - Modulo.MathRound((CDbl(Tabella.Rows(x).Item(6)) * 100) / (100 + (IVA.Aliquota * 100)), 10)) * -1
                                TipoIVA(Max) = Tabella.Rows(x).Item(10)
                                Max = Max + 1
                            End If




                            If Not EM_Retta.CreaRigaAccrediti(CausaleRetta, SalvaId, Xk.MASTRO, Xk.CONTO, Tabella.Rows(x).Item(0) * 100, AliquotaIva.Codice, CDbl(Tabella.Rows(x).Item(6)), "Accredito " & Tabella.Rows(x).Item(9), 0, Tabella.Rows(x).Item(10)) Then Exit Sub


                        End If
                    End If
                End If
            Next
            If TipoOperasazione.SCORPORAIVA > 0 Then

                Dim Cerca As Integer

                For Cerca = 0 To Max
                    If Imposta(Cerca) <> 0 Then
                        Dim TipoAdd As New Cls_Addebito

                        TipoAdd.Codice = TipoIVA(Cerca)
                        TipoAdd.Leggi(Session("DC_OSPITE"), TipoAdd.Codice)

                        Dim IVA As New Cls_IVA

                        IVA.Codice = TipoAdd.CodiceIVA
                        IVA.Leggi(Session("DC_TABELLE"), IVA.Codice)

                        If Imposta(Cerca) > 0 Then
                            If Not EM_Retta.CreaRigaAccrediti(CausaleRetta, SalvaId, 0, 0, 0, TipoAdd.CodiceIVA, Math.Abs(Imposta(Cerca)), "Scorporo IVA", 0, TipoIVA(Cerca)) Then Exit Sub
                        Else
                            If Not EM_Retta.CreaRigaAddebiti(CausaleRetta, SalvaId, 0, 0, 0, TipoAdd.CodiceIVA, Math.Abs(Imposta(Cerca)), "Scorporo IVA", 0, TipoIVA(Cerca)) Then Exit Sub
                        End If
                    End If
                Next

            End If
        End If

        EM_Retta.Commit()
        EM_Retta.CloseDB()
    End Sub

    Private Sub CreaDocRegione()
        Dim i As Integer
        Dim x As Integer
        Dim Centroservizio As String = ""
        Dim CodiceOspite As Long = 0
        Dim CodiceRegione As String = ""
        Dim OldCodiceRegione As String = ""
        Dim OldCodiceOspite As Long = 0
        Dim MaxAddebito As Long = 0
        Dim MaxAccredito As Long = 0


        Dim Provincia As String
        Dim Comune As String

        Dim CodiceIVA As String

        Dim BolloImpo As Double
        Dim Bollo As Integer
        Dim IVABollo As String
        Dim SalvaId As Long

        Dim ImportoRetta As Double
        Dim Sconto As Double
        Dim EM_Retta As New Cls_EmissioneRetta

        Dim ImportoIVA As Double
        Dim ImportoRegistrazione As Double

        EM_Retta.ConnessioneGenerale = Session("DC_GENERALE")
        EM_Retta.ConnessioneOspiti = Session("DC_OSPITE")
        EM_Retta.ConnessioneTabelle = Session("DC_TABELLE")

        EM_Retta.ApriDB()

        EM_Retta.EliminaTemporanei()

        Tabella = ViewState("SalvaTabella")

        If DD_Ente.SelectedValue <> "" Then
            For i = 0 To Tabella.Rows.Count - 1
                Tabella.Rows(i).Item(5) = DD_Ente.SelectedValue
            Next
        End If
        Dim Xk As New Cls_CentroServizio


        Xk.Leggi(Session("DC_OSPITE"), DD_CentroServizio.SelectedValue)

        CodiceOspite = Tabella.Rows(0).Item(0)
        For i = 0 To Tabella.Rows.Count - 1

            Centroservizio = DD_CentroServizio.SelectedValue
            CodiceOspite = Tabella.Rows(i).Item(0)
            CodiceRegione = Tabella.Rows(i).Item(5)
            Dim CheckBoxV As CheckBox = DirectCast(Grd_AggiornaRette.Rows(i).FindControl("Chk_Seleziona"), CheckBox)


            If CheckBoxV.Checked = True Then
                If CodiceRegione <> OldCodiceRegione And OldCodiceRegione <> "" Then


                    Dim xs As New ClsUSL

                    xs.CodiceRegione = OldCodiceRegione
                    xs.Leggi(Session("DC_OSPITE"))

                    Dim DCr As New Cls_DatiComnueRegioneServizio

                    DCr.CodiceRegione = xs.CodiceRegione
                    DCr.CentroServizio = Centroservizio
                    DCr.Leggi(Session("DC_OSPITE"))
                    If DCr.TipoOperazione <> "" Then
                        xs.TipoOperazione = DCr.TipoOperazione
                        xs.CodiceIva = DCr.AliquotaIva
                    End If


                    Dim TipoOperasazione As New Cls_TipoOperazione

                    TipoOperasazione.Leggi(Session("DC_OSPITE"), xs.TipoOperazione)

                    EM_Retta.GlbTipoOperazione = xs.TipoOperazione

                    CodiceIVA = xs.CodiceIva

                    Dim AliquotaIva As New Cls_IVA
                    Dim AliquotaBollo As New Cls_IVA
                    Dim CausaleRetta As String

                    AliquotaIva.Leggi(Session("DC_TABELLE"), CodiceIVA)


                    If ImportoRetta > 0 Then
                        CausaleRetta = TipoOperasazione.CausaleAddebito
                    Else
                        CausaleRetta = TipoOperasazione.CausaleStorno
                    End If

                    If Not EM_Retta.CreaRigaTesta(Txt_Anno.Text, Dd_Mese.SelectedValue, DD_CentroServizio.SelectedValue, CausaleRetta, SalvaId, Txt_DataRegistrazione.Text, "R" & OldCodiceRegione, 0) Then
                        'errore
                        Exit Sub
                    End If



                    ImportoIVA = Modulo.MathRound(AliquotaIva.Aliquota * Math.Abs(ImportoRetta), 2)
                    ImportoRegistrazione = Modulo.MathRound(Math.Abs(ImportoRetta + ImportoIVA), 2)

                    If TipoOperasazione.SoggettaABollo = "" Or (TipoOperasazione.SoggettaABollo = "F" And CausaleRetta = TipoOperasazione.CausaleStorno) Then
                        BolloImpo = 0
                        Bollo = 0
                        IVABollo = ""
                    Else
                        Dim KBollo As New Cls_bolli

                        KBollo.Leggi(Session("DC_TABELLE"), Txt_DataRegistrazione.Text)

                        'And (TipoOperasazione.NonBolloConIVA = 0 Or (TipoOperasazione.NonBolloConIVA = 1 And ImportoIVA = 0))

                        If KBollo.ImportoApplicazione < Math.Round(ImportoRegistrazione, 2) Then
                            BolloImpo = KBollo.ImportoBollo
                            Bollo = KBollo.ImportoBollo
                            IVABollo = KBollo.CodiceIVA

                            AliquotaBollo.Leggi(Session("DC_TABELLE"), IVABollo)
                        Else
                            BolloImpo = 0
                            Bollo = 0
                            IVABollo = ""
                        End If
                    End If

                    Dim Causale As New Cls_CausaleContabile

                    Causale.Leggi(Session("DC_TABELLE"), CausaleRetta)
                    If ImportoRetta > 0 Then
                        If Causale.Righe(8).DareAvere = "D" Then
                            ImportoRegistrazione = ImportoRegistrazione - BolloImpo
                        Else
                            ImportoRegistrazione = ImportoRegistrazione + BolloImpo
                        End If
                    Else
                        If Causale.Righe(8).DareAvere = "A" Then
                            ImportoRegistrazione = ImportoRegistrazione - BolloImpo
                        Else
                            ImportoRegistrazione = ImportoRegistrazione + BolloImpo
                        End If
                    End If

                    If xs.ImportoSconto > 0 Then
                        Sconto = Modulo.MathRound(ImportoRegistrazione * xs.ImportoSconto / 100, 2)
                        ImportoRegistrazione = ImportoRegistrazione - Modulo.MathRound(ImportoRegistrazione * xs.ImportoSconto / 100, 2)
                    End If


                    If Not EM_Retta.CreaRigaCliente(CausaleRetta, SalvaId, xs.MastroCliente, xs.ContoCliente, xs.SottoContoCliente, ImportoRegistrazione) Then Exit Sub

                    If BolloImpo <> 0 And IVABollo <> AliquotaIva.Codice Then
                        If Not EM_Retta.CreaRigaIVA(CausaleRetta, SalvaId, xs.MastroCliente, xs.ContoCliente, xs.SottoContoCliente, AliquotaIva.Codice, ImportoIVA, Math.Abs(ImportoRetta)) Then Exit Sub
                        If ImportoRetta > 0 Then
                            If Not EM_Retta.CreaRigaIVA(CausaleRetta, SalvaId, xs.MastroCliente, xs.ContoCliente, xs.SottoContoCliente, IVABollo, 0, BolloImpo) Then Exit Sub
                        Else
                            Dim CausaleContabile As New Cls_CausaleContabile

                            CausaleContabile.Codice = CausaleRetta
                            CausaleContabile.Leggi(Session("DC_TABELLE"), CausaleRetta)

                            If CausaleContabile.Righe(8).DareAvere = CausaleContabile.Righe(1).DareAvere Then
                                If Not EM_Retta.CreaRigaIVA(CausaleRetta, SalvaId, xs.MastroCliente, xs.ContoCliente, xs.SottoContoCliente, IVABollo, 0, BolloImpo) Then Exit Sub
                            Else
                                If Not EM_Retta.CreaRigaIVA(CausaleRetta, SalvaId, xs.MastroCliente, xs.ContoCliente, xs.SottoContoCliente, IVABollo, 0, BolloImpo, True) Then Exit Sub
                            End If
                        End If
                    Else
                        If Not EM_Retta.CreaRigaIVA(CausaleRetta, SalvaId, xs.MastroCliente, xs.ContoCliente, xs.SottoContoCliente, AliquotaIva.Codice, ImportoIVA, Math.Abs(ImportoRegistrazione)) Then Exit Sub
                    End If
                    If BolloImpo > 0 Then
                        If Not EM_Retta.CreaRigaBollo(CausaleRetta, SalvaId, xs.MastroCliente, xs.ContoCliente, xs.SottoContoCliente, IVABollo, Bollo, "Bollo ") Then Exit Sub
                    End If
                    If BolloImpo > 0 Or TipoOperasazione.BolloVirtuale = 1 Then
                        Dim CausaleContabile As New Cls_CausaleContabile

                        CausaleContabile.Codice = CausaleRetta
                        CausaleContabile.Leggi(Session("DC_TABELLE"), CausaleRetta)
                        Dim RegistroIVA As New Cls_RegistroIVA

                        RegistroIVA.RegistroCartaceo = 0
                        RegistroIVA.Tipo = CausaleContabile.RegistroIVA
                        RegistroIVA.Leggi(Session("DC_TABELLE"), RegistroIVA.Tipo)

                        If RegistroIVA.RegistroCartaceo = 0 Then
                            AssegnaBolloVirtuale(SalvaId)
                        End If
                    End If

                    For x = i - 1 To 0 Step -1


                        Dim CheckBoxV2 As CheckBox = DirectCast(Grd_AggiornaRette.Rows(x).FindControl("Chk_Seleziona"), CheckBox)


                        If CheckBoxV2.Checked = True Then
                            If OldCodiceRegione <> Tabella.Rows(x).Item(5) Then
                                Exit For
                            End If

                            If Tabella.Rows(x).Item(3) = "AD" Then
                                If Not EM_Retta.CreaRigaAddebiti(CausaleRetta, SalvaId, Xk.MASTRO, Xk.CONTO, Tabella.Rows(x).Item(0) * 100, AliquotaIva.Codice, CDbl(Tabella.Rows(x).Item(6)), "Addebito " & Tabella.Rows(x).Item(9), 0, Tabella.Rows(x).Item(10)) Then Exit Sub
                            Else
                                If Not EM_Retta.CreaRigaAccrediti(CausaleRetta, SalvaId, Xk.MASTRO, Xk.CONTO, Tabella.Rows(x).Item(0) * 100, AliquotaIva.Codice, CDbl(Tabella.Rows(x).Item(6)), "Accredito " & Tabella.Rows(x).Item(9), 0, Tabella.Rows(x).Item(10)) Then Exit Sub
                            End If
                        End If
                    Next
                    ImportoRegistrazione = 0
                    ImportoRetta = 0
                End If

                If Tabella.Rows(i).Item(3) = "AD" Then
                    ImportoRetta = ImportoRetta + CDbl(Tabella.Rows(i).Item(6))
                Else
                    ImportoRetta = ImportoRetta - CDbl(Tabella.Rows(i).Item(6))

                End If
                OldCodiceRegione = CodiceRegione
            End If
        Next

        If Math.Abs(ImportoRetta) > 0 Then


            Dim xs As New ClsUSL



            xs.CodiceRegione = OldCodiceRegione
            xs.Leggi(Session("DC_OSPITE"))

            Dim DCr As New Cls_DatiComnueRegioneServizio

            DCr.CodiceRegione = xs.CodiceRegione
            DCr.CentroServizio = Centroservizio
            DCr.Leggi(Session("DC_OSPITE"))
            If DCr.TipoOperazione <> "" Then
                xs.TipoOperazione = DCr.TipoOperazione
                xs.CodiceIva = DCr.AliquotaIva
            End If

            Dim TipoOperasazione As New Cls_TipoOperazione

            TipoOperasazione.Leggi(Session("DC_OSPITE"), xs.TipoOperazione)


            CodiceIVA = xs.CodiceIva

            Dim AliquotaIva As New Cls_IVA
            Dim AliquotaBollo As New Cls_IVA
            Dim CausaleRetta As String

            AliquotaIva.Leggi(Session("DC_TABELLE"), CodiceIVA)


            If ImportoRetta > 0 Then
                CausaleRetta = TipoOperasazione.CausaleAddebito
            Else
                CausaleRetta = TipoOperasazione.CausaleStorno
            End If

            If Not EM_Retta.CreaRigaTesta(Txt_Anno.Text, Dd_Mese.SelectedValue, DD_CentroServizio.SelectedValue, CausaleRetta, SalvaId, Txt_DataRegistrazione.Text, "R" & OldCodiceRegione, 0) Then
                'errore
                Exit Sub
            End If



            ImportoIVA = Modulo.MathRound(AliquotaIva.Aliquota * Math.Abs(ImportoRetta), 2)
            ImportoRegistrazione = Modulo.MathRound(Math.Abs(ImportoRetta + ImportoIVA), 2)

            If TipoOperasazione.SoggettaABollo = "" Then
                BolloImpo = 0
                Bollo = 0
                IVABollo = ""
            Else
                Dim KBollo As New Cls_bolli

                KBollo.Leggi(Session("DC_TABELLE"), Txt_DataRegistrazione.Text)
                'And (TipoOperasazione.NonBolloConIVA = 0 Or (TipoOperasazione.NonBolloConIVA = 1 And ImportoIVA = 0))

                If KBollo.ImportoApplicazione < Math.Round(ImportoRegistrazione, 2) Then
                    BolloImpo = KBollo.ImportoBollo
                    Bollo = KBollo.ImportoBollo
                    IVABollo = KBollo.CodiceIVA
                    AliquotaBollo.Leggi(Session("DC_TABELLE"), IVABollo)
                Else
                    BolloImpo = 0
                    Bollo = 0
                    IVABollo = ""
                End If


            End If
                Dim Causale As New Cls_CausaleContabile

                Causale.Leggi(Session("DC_TABELLE"), CausaleRetta)
                If ImportoRetta > 0 Then
                    If Causale.Righe(8).DareAvere = "D" Then
                        ImportoRegistrazione = ImportoRegistrazione - BolloImpo
                    Else
                        ImportoRegistrazione = ImportoRegistrazione + BolloImpo
                    End If
                Else
                    If Causale.Righe(8).DareAvere = "A" Then
                        ImportoRegistrazione = ImportoRegistrazione - BolloImpo
                    Else
                        ImportoRegistrazione = ImportoRegistrazione + BolloImpo
                    End If
                End If

                If xs.ImportoSconto > 0 Then
                    Sconto = Modulo.MathRound(ImportoRegistrazione * xs.ImportoSconto / 100, 2)
                    ImportoRegistrazione = ImportoRegistrazione - Modulo.MathRound(ImportoRegistrazione * xs.ImportoSconto / 100, 2)
                End If


                If Not EM_Retta.CreaRigaCliente(CausaleRetta, SalvaId, xs.MastroCliente, xs.ContoCliente, xs.SottoContoCliente, ImportoRegistrazione) Then Exit Sub

                If BolloImpo <> 0 And IVABollo <> AliquotaIva.Codice Then
                    If Not EM_Retta.CreaRigaIVA(CausaleRetta, SalvaId, xs.MastroCliente, xs.ContoCliente, xs.SottoContoCliente, AliquotaIva.Codice, ImportoIVA, Math.Abs(ImportoRetta - ImportoIVA)) Then Exit Sub
                    If ImportoRetta > 0 Then
                        If Not EM_Retta.CreaRigaIVA(CausaleRetta, SalvaId, xs.MastroCliente, xs.ContoCliente, xs.SottoContoCliente, IVABollo, 0, BolloImpo) Then Exit Sub
                    Else
                        Dim CausaleContabile As New Cls_CausaleContabile

                        CausaleContabile.Codice = CausaleRetta
                        CausaleContabile.Leggi(Session("DC_TABELLE"), CausaleRetta)

                        If CausaleContabile.Righe(8).DareAvere = CausaleContabile.Righe(1).DareAvere Then
                            If Not EM_Retta.CreaRigaIVA(CausaleRetta, SalvaId, xs.MastroCliente, xs.ContoCliente, xs.SottoContoCliente, IVABollo, 0, BolloImpo) Then Exit Sub
                        Else
                            If Not EM_Retta.CreaRigaIVA(CausaleRetta, SalvaId, xs.MastroCliente, xs.ContoCliente, xs.SottoContoCliente, IVABollo, 0, BolloImpo, True) Then Exit Sub
                        End If
                    End If
                Else
                    If Not EM_Retta.CreaRigaIVA(CausaleRetta, SalvaId, xs.MastroCliente, xs.ContoCliente, xs.SottoContoCliente, AliquotaIva.Codice, ImportoIVA, Math.Abs(ImportoRegistrazione - ImportoIVA)) Then Exit Sub
                End If


            If BolloImpo > 0 Then
                If Not EM_Retta.CreaRigaBollo(CausaleRetta, SalvaId, xs.MastroCliente, xs.ContoCliente, xs.SottoContoCliente, IVABollo, Bollo, "Bollo ") Then Exit Sub
            End If

            If BolloImpo > 0 Or TipoOperasazione.BolloVirtuale = 1 Then
                Dim CausaleContabile As New Cls_CausaleContabile

                CausaleContabile.Codice = CausaleRetta
                CausaleContabile.Leggi(Session("DC_TABELLE"), CausaleRetta)
                Dim RegistroIVA As New Cls_RegistroIVA

                RegistroIVA.RegistroCartaceo = 0
                RegistroIVA.Tipo = CausaleContabile.RegistroIVA
                RegistroIVA.Leggi(Session("DC_TABELLE"), RegistroIVA.Tipo)

                If RegistroIVA.RegistroCartaceo = 0 Then
                    AssegnaBolloVirtuale(SalvaId)
                End If
            End If

                For x = i - 1 To 0 Step -1


                    Dim CheckBoxV1 As CheckBox = DirectCast(Grd_AggiornaRette.Rows(x).FindControl("Chk_Seleziona"), CheckBox)


                    If CheckBoxV1.Checked = True Then
                        If OldCodiceRegione <> Tabella.Rows(x).Item(5) Then
                            Exit For
                        End If
                        If Tabella.Rows(x).Item(3) = "AD" Then
                            If Not EM_Retta.CreaRigaAddebiti(CausaleRetta, SalvaId, Xk.MASTRO, Xk.CONTO, Tabella.Rows(x).Item(0) * 100, AliquotaIva.Codice, CDbl(Tabella.Rows(x).Item(6)), "Addebito " & Tabella.Rows(x).Item(9), 0, Tabella.Rows(x).Item(10)) Then Exit Sub
                        Else
                            If Not EM_Retta.CreaRigaAccrediti(CausaleRetta, SalvaId, Xk.MASTRO, Xk.CONTO, Tabella.Rows(x).Item(0) * 100, AliquotaIva.Codice, CDbl(Tabella.Rows(x).Item(6)), "Accredito " & Tabella.Rows(x).Item(9), 0, Tabella.Rows(x).Item(10)) Then Exit Sub
                        End If

                    End If
                Next

        End If

        EM_Retta.Commit()
        EM_Retta.CloseDB()
    End Sub

    Protected Sub Lbl_SelezionaTutti_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles Lbl_SelezionaTutti.Click
        Tabella = ViewState("SalvaTabella")
        If IsNothing(Tabella) Then
            ClientScript.RegisterClientScriptBlock(Me.GetType(), "Errore", "VisualizzaErrore('Visualizzare elenco prima di richiedere la selezione');", True)
            Exit Sub
        End If

        If Tabella.Rows.Count = 0 Then
            ClientScript.RegisterClientScriptBlock(Me.GetType(), "Errore", "VisualizzaErrore('Visualizzare elenco prima di richiedere la selezione');", True)
            Exit Sub
        End If


        For i = 0 To Tabella.Rows.Count - 1
            Dim CheckBoxV1 As CheckBox = DirectCast(Grd_AggiornaRette.Rows(i).FindControl("Chk_Seleziona"), CheckBox)

            If CheckBoxV1.Enabled = True Then
                CheckBoxV1.Checked = True
            End If
        Next
    End Sub

    Protected Sub Lbl_InvertiSelezione_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles Lbl_InvertiSelezione.Click
        Tabella = ViewState("SalvaTabella")
        If IsNothing(Tabella) Then
            ClientScript.RegisterClientScriptBlock(Me.GetType(), "Errore", "VisualizzaErrore('Visualizzare elenco prima di richiedere la selezione');", True)
            Exit Sub
        End If

        If Tabella.Rows.Count = 0 Then
            ClientScript.RegisterClientScriptBlock(Me.GetType(), "Errore", "VisualizzaErrore('Visualizzare elenco prima di richiedere la selezione');", True)
            Exit Sub
        End If

        For i = 0 To Tabella.Rows.Count - 1
            Dim CheckBoxV1 As CheckBox = DirectCast(Grd_AggiornaRette.Rows(i).FindControl("Chk_Seleziona"), CheckBox)
            If CheckBoxV1.Enabled = True Then
                If CheckBoxV1.Checked = True Then
                    CheckBoxV1.Checked = False
                Else
                    CheckBoxV1.Checked = True
                End If
            End If
        Next
    End Sub

    Protected Sub Btn_Esci_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles Btn_Esci.Click
        Response.Redirect("Menu_Strumenti.aspx")
    End Sub




    Private Sub AggiornaCServ()
        Dim kCsrv As New Cls_CentroServizio

        If DD_Struttura.SelectedValue = "" Then
            kCsrv.UpDateDropBox(Session("DC_OSPITE"), DD_CentroServizio)
        Else
            kCsrv.UpDateDropBoxStruttura(Session("DC_OSPITE"), DD_CentroServizio, DD_Struttura.SelectedValue)
        End If
    End Sub



    Protected Sub DD_Struttura_TextChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles DD_Struttura.TextChanged
        AggiornaCServ()
        Call EseguiJS()
    End Sub

    Protected Sub Grd_AggiornaRette_RowCommand(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewCommandEventArgs) Handles Grd_AggiornaRette.RowCommand

    End Sub

    Protected Sub Grd_AggiornaRette_RowDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs) Handles Grd_AggiornaRette.RowDataBound
        'myriga(10)
        If e.Row.RowType = DataControlRowType.DataRow Then


            If DD_TipoAddebito.SelectedValue <> "" Then
                If Tabella.Rows(e.Row.RowIndex).Item(10).ToString <> DD_TipoAddebito.SelectedValue Then
                    Dim ChkImporta As CheckBox = DirectCast(e.Row.FindControl("Chk_Seleziona"), CheckBox)


                    e.Row.BackColor = System.Drawing.Color.Gray
                    ChkImporta.Enabled = False
                End If
            End If
        End If

    End Sub
End Class
