﻿<%@ Page Language="VB" AutoEventWireup="false" Inherits="OspitiWeb_AddebitiImportati" CodeFile="AddebitiImportati.aspx.vb" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
        <title>Errori Importazione</title>
    <asp:PlaceHolder runat="server">
        <%: System.Web.Optimization.Styles.Render("~/Content/AjaxControlToolkit/Styles/Bundle") %>
    </asp:PlaceHolder>
         <style>
           th {
        font-weight: normal;
        }
        .tabella {
          border: 1px solid #ccc;
          border-collapse: collapse;
          margin: 0;
          padding: 0;
          width: 100%;
          table-layout: fixed;
        }

        .miotr  {
          background: #f8f8f8;
          border: 1px solid #ddd;
          padding: .35em;
        }
        .miacella {
          padding: .625em;
          text-align: center;
        }
        .miaintestazione {
          padding: .625em;
          text-align: center;
          font-size: .85em;
          letter-spacing: .1em;
          text-transform: uppercase;
        }
        td 
        {
        	 text-align:center;
        }
     </style>
</head>
<body>
    <form id="form1" runat="server">
    <div>
      <asp:Label ID="LblErroriImportazione" runat="server" Text=""></asp:Label>
    </div>
    </form>
</body>
</html>
