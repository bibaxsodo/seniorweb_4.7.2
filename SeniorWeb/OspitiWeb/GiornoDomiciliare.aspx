﻿<%@ Page Language="VB" AutoEventWireup="false" Inherits="OspitiWeb_GiornoDomiciliare" CodeFile="GiornoDomiciliare.aspx.vb" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="axex" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
    <asp:PlaceHolder runat="server">
        <%: System.Web.Optimization.Styles.Render("~/Content/AjaxControlToolkit/Styles/Bundle") %>
    </asp:PlaceHolder>
    <link href="ospiti.css?ver=11" rel="stylesheet" type="text/css" />
    <link rel="shortcut icon" href="images/SENIOR.ico" />
    <link href="js/jquery.autocomplete.css" rel="stylesheet" type="text/css" />


    <script src="js/jquery-1.5.1.min.js" type="text/javascript"></script>
    <script src="js/jquery.autocomplete.js" type="text/javascript"></script>
    <script src="js/soapclient.js" type="text/javascript"></script>
    <script src="/js/convertinumero.js" type="text/javascript"></script>

    <script src="js/JSErrore.js" type="text/javascript"></script>

    <script src="js/jquery.maskedinput-1.3.min.js" type="text/javascript"></script>
    <script src="/js/formatnumer.js" type="text/javascript"></script>
    <link rel="stylesheet" href="dialogbox/redips-dialog.css" type="text/css" media="screen" />
    <script type="text/javascript" src="dialogbox/redips-dialog-min.js"></script>
    <script type="text/javascript" src="dialogbox/script.js"></script>
    <script type="text/javascript" src="../js/menuanagraficaospiti.js?ver=7"></script>
    <link rel="stylesheet" href="jqueryui/jquery-ui.css" type="text/css" />
    <script src="jqueryui/jquery-ui.js" type="text/javascript"></script>
    <script src="jqueryui/datapicker-it.js" type="text/javascript"></script>

</head>
<body>
    <form id="form1" runat="server">
        <asp:ScriptManager ID="ScriptManager1" runat="server" EnableScriptGlobalization="true">
            <Scripts>
                <asp:ScriptReference Path="~/Scripts/AjaxControlToolkit/Bundle" />
            </Scripts>
        </asp:ScriptManager>
        <div>
            <div style="float: right;">
                <asp:ImageButton ID="Btn_Modifica" runat="server" Height="38px" ImageUrl="~/images/salva.jpg" class="EffettoBottoniTondi" Width="38px" ToolTip="Modifica / Inserisci (F2)" />
            </div>
            <axex:TabContainer ID="TabContainer1" runat="server" ActiveTabIndex="0" CssClass="TabSenior"
                Width="100%" BorderStyle="None" Style="margin-right: 39px">
                <axex:TabPanel runat="server" HeaderText="Prima Nota" ID="Tab_Anagrafica">
                    <HeaderTemplate>
                        Invervallo 1     
                    </HeaderTemplate>
                    <ContentTemplate>
                        <asp:Label ID="Lbl_Id1" runat="server" Text="0"></asp:Label>
                        <label style="display: block; float: left; width: 160px;">Data  :</label>
                        <asp:TextBox ID="Txt_DataRegistrazione" runat="server" Width="90px"></asp:TextBox>
                        <br />
                        <br />

                        <label style="display: block; float: left; width: 160px;">Ora Inizio  :</label>
                        <asp:TextBox ID="Txt_OraInizio" runat="server" Width="100px"></asp:TextBox>
                        <br />
                        <br />

                        <label style="display: block; float: left; width: 160px;">Ora Fine  :</label>
                        <asp:TextBox ID="Txt_OraFine" runat="server" Width="100px"></asp:TextBox>
                        <br />
                        <br />

                        <label style="display: block; float: left; width: 160px;">Descrizione :</label>
                        <asp:TextBox ID="Txt_Descrizione" runat="server" Width="534px" MaxLength="50"></asp:TextBox><br />
                        <br />
                        <br />

                        <label style="display: block; float: left; width: 160px;">Operatore :</label>
                        <asp:TextBox ID="Txt_Operatore1" runat="server" onkeypress="return soloNumeri(event);" Width="100px" MaxLength="50"></asp:TextBox><br />
                        <label style="display: block; float: left; width: 160px;">&nbsp;</label><asp:DropDownList ID="DD_Operatore1_1" runat="server" Visible="true"></asp:DropDownList><br />
                        <label style="display: block; float: left; width: 160px;">&nbsp;</label><asp:DropDownList ID="DD_Operatore2_1" runat="server" Visible="true"></asp:DropDownList><br />
                        <label style="display: block; float: left; width: 160px;">&nbsp;</label><asp:DropDownList ID="DD_Operatore3_1" runat="server" Visible="true"></asp:DropDownList><br />
                        <label style="display: block; float: left; width: 160px;">&nbsp;</label><asp:DropDownList ID="DD_Operatore4_1" runat="server" Visible="true"></asp:DropDownList><br />
                        <label style="display: block; float: left; width: 160px;">&nbsp;</label><asp:DropDownList ID="DD_Operatore5_1" runat="server" Visible="true"></asp:DropDownList><br />
                        <br />
                        <br />

                        <label style="display: block; float: left; width: 160px;">Tipologia :</label>
                        <asp:DropDownList ID="DD_Tipologia" runat="server"></asp:DropDownList>
                        <br />
                        <br />
                    </ContentTemplate>
                </axex:TabPanel>
                <axex:TabPanel runat="server" HeaderText="Prima Nota" ID="Tab_Intervallo2">
                    <HeaderTemplate>
                        Invervallo 2     
                    </HeaderTemplate>
                    <ContentTemplate>
                        <asp:Label ID="Lbl_Id2" runat="server" Text="0"></asp:Label>
                        <label style="display: block; float: left; width: 160px;">Data  :</label>
                        <asp:TextBox ID="Txt_DataRegistrazione_2" runat="server" Width="100px"></asp:TextBox>
                        <br />
                        <br />

                        <label style="display: block; float: left; width: 160px;">Ora Inizio  :</label>
                        <asp:TextBox ID="Txt_OraInizio_2" runat="server" Width="100px"></asp:TextBox>
                        <br />
                        <br />

                        <label style="display: block; float: left; width: 160px;">Ora Fine  :</label>
                        <asp:TextBox ID="Txt_OraFine_2" runat="server" Width="100px"></asp:TextBox>
                        <br />
                        <br />

                        <label style="display: block; float: left; width: 160px;">Descrizione :</label>
                        <asp:TextBox ID="Txt_Descrizione_2" runat="server" Width="534px" MaxLength="50"></asp:TextBox><br />
                        <br />
                        <br />

                        <label style="display: block; float: left; width: 160px;">Operatore :</label>
                        <asp:TextBox ID="Txt_Operatore2" runat="server" onkeypress="return soloNumeri(event);" Width="100px" MaxLength="50"></asp:TextBox><br />
                        <label style="display: block; float: left; width: 160px;">&nbsp;</label><asp:DropDownList ID="DD_Operatore1_2" runat="server" Visible="true"></asp:DropDownList><br />
                        <label style="display: block; float: left; width: 160px;">&nbsp;</label><asp:DropDownList ID="DD_Operatore2_2" runat="server" Visible="true"></asp:DropDownList><br />
                        <label style="display: block; float: left; width: 160px;">&nbsp;</label><asp:DropDownList ID="DD_Operatore3_2" runat="server" Visible="true"></asp:DropDownList><br />
                        <label style="display: block; float: left; width: 160px;">&nbsp;</label><asp:DropDownList ID="DD_Operatore4_2" runat="server" Visible="true"></asp:DropDownList><br />
                        <label style="display: block; float: left; width: 160px;">&nbsp;</label><asp:DropDownList ID="DD_Operatore5_2" runat="server" Visible="true"></asp:DropDownList><br />
                        <br />
                        <br />

                        <label style="display: block; float: left; width: 160px;">Tipologia :</label>
                        <asp:DropDownList ID="DD_Tipologia_2" runat="server"></asp:DropDownList>
                        <br />
                        <br />
                    </ContentTemplate>
                </axex:TabPanel>
                <axex:TabPanel runat="server" HeaderText="Prima Nota" ID="Tab_Intervallo3">
                    <HeaderTemplate>
                        Invervallo 3     
                    </HeaderTemplate>
                    <ContentTemplate>
                        <asp:Label ID="Lbl_Id3" runat="server" Text="0"></asp:Label>
                        <label style="display: block; float: left; width: 160px;">Data  :</label>
                        <asp:TextBox ID="Txt_DataRegistrazione_3" runat="server" Width="100px"></asp:TextBox>
                        <br />
                        <br />

                        <label style="display: block; float: left; width: 160px;">Ora Inizio  :</label>
                        <asp:TextBox ID="Txt_OraInizio_3" runat="server" Width="100px"></asp:TextBox>
                        <br />
                        <br />

                        <label style="display: block; float: left; width: 160px;">Ora Fine  :</label>
                        <asp:TextBox ID="Txt_OraFine_3" runat="server" Width="100px"></asp:TextBox>
                        <br />
                        <br />

                        <label style="display: block; float: left; width: 160px;">Descrizione :</label>
                        <asp:TextBox ID="Txt_Descrizione_3" runat="server" Width="534px" MaxLength="50"></asp:TextBox><br />
                        <br />
                        <br />


                        <label style="display: block; float: left; width: 160px;">Operatore :</label>
                        <asp:TextBox ID="Txt_Operatore3" runat="server" onkeypress="return soloNumeri(event);" Width="100px" MaxLength="50"></asp:TextBox><br />
                        <label style="display: block; float: left; width: 160px;">&nbsp;</label><asp:DropDownList ID="DD_Operatore1_3" runat="server" Visible="true"></asp:DropDownList><br />
                        <label style="display: block; float: left; width: 160px;">&nbsp;</label><asp:DropDownList ID="DD_Operatore2_3" runat="server" Visible="true"></asp:DropDownList><br />
                        <label style="display: block; float: left; width: 160px;">&nbsp;</label><asp:DropDownList ID="DD_Operatore3_3" runat="server" Visible="true"></asp:DropDownList><br />
                        <label style="display: block; float: left; width: 160px;">&nbsp;</label><asp:DropDownList ID="DD_Operatore4_3" runat="server" Visible="true"></asp:DropDownList><br />
                        <label style="display: block; float: left; width: 160px;">&nbsp;</label><asp:DropDownList ID="DD_Operatore5_3" runat="server" Visible="true"></asp:DropDownList><br />
                        <br />
                        <br />

                        <label style="display: block; float: left; width: 160px;">Tipologia :</label>
                        <asp:DropDownList ID="DD_Tipologia_3" runat="server"></asp:DropDownList>
                        <br />
                        <br />
                    </ContentTemplate>

                </axex:TabPanel>

            </axex:TabContainer>
        </div>
    </form>
</body>
</html>
