﻿Imports Microsoft.VisualBasic
Imports System
Imports System.Web
Imports System.Collections.Generic
Imports System.Data.OleDb
Imports System.Web.Hosting
Imports System.Data.SqlTypes
Imports System.Net
Imports System.IO
Imports System.Security.Cryptography.X509Certificates
Imports System.Net.Security
Imports System.Web.Script.Serialization
Imports org.jivesoftware.util
Imports Newtonsoft.Json
Imports Newtonsoft.Json.Linq
Imports System.Threading

Partial Class OspitiWeb_stampaschedaospiti
    Inherits System.Web.UI.Page

    Private Sub StampaContratto(ByVal Copie As Long)


        ExportPDF()

    End Sub

    Private Sub LeggiAnagraficaEpersonam(ByRef Contenuto As String, ByVal CodiceFiscale As String, ByVal bu As String)
        Dim Token As String

        Token = LoginPersonam()


        Dim Room As String = ""
        Dim JSonGuest As String

        JSonGuest = RichiestaGuests(Token, bu, CodiceFiscale)

        Try
            Dim jResults As JObject = JObject.Parse(JSonGuest)


            Dim Movimenti As JArray = JArray.Parse(jResults.Item("movements").ToString())

            For Each jTok As JToken In Movimenti

                Try
                    Room = jTok.Item("room").ToString()
                Catch ex As Exception

                End Try


            Next

        Catch ex As Exception

        End Try

        Contenuto = Contenuto.Replace("@EPERSONAM-ROOM@", Room)
    End Sub


    Private Function RichiestaGuests(ByVal Token As String, ByVal BussinesUnit As String, ByVal CodiceFiscale As String) As String


        Try

            Dim UrlConnessione As String


            UrlConnessione = "https://api.e-personam.com/api/v1.0/business_units/" & BussinesUnit & "/guests/" & CodiceFiscale

            Dim client As HttpWebRequest = WebRequest.Create(UrlConnessione)

            client.Method = "GET"
            client.Headers.Add("Authorization", "Bearer " & Token)
            client.ContentType = "Content-Type: application/json"

            Dim reader As StreamReader
            Dim response As HttpWebResponse = Nothing

            response = DirectCast(client.GetResponse(), HttpWebResponse)

            reader = New StreamReader(response.GetResponseStream())


            Dim rawresp As String
            rawresp = reader.ReadToEnd()

            
            Return rawresp

        Catch ex As Exception
            Return ""
        End Try



    End Function

    Private Shared Function CertificateHandler(ByVal sender As Object, ByVal certificate As X509Certificate, ByVal chain As X509Chain, ByVal SSLerror As SslPolicyErrors) As Boolean
        Return True
    End Function
    Public Class Autentificazione
        Public access_token As String
        Public expires_in As String
        Public scope As String
        Public token_type As String
        Public refresh_token As String
    End Class

    Private Function LoginPersonam() As String
        Try



            Dim request As New NameValueCollection


            request.Add("client_id", "98217ca6670c660e22f42d58e231d4e56c84fb34e216b0f66f1f30284fd3ec9d")
            request.Add("client_secret", "5570a684f69ca74d75d16bba669c156ec092eccb4111d0974adec3edb2fa4d6f")


            request.Add("username", Session("EPersonamUser"))



            request.Add("password", Session("EPersonamPSW"))
            System.Net.ServicePointManager.ServerCertificateValidationCallback = AddressOf CertificateHandler


            Dim client As New WebClient()

            Dim result As Object = client.UploadValues("https://api.e-personam.com/api/v1.0/token/password", "POST", request)


            Dim stringa As String = Encoding.Default.GetString(result)


            Dim serializer As JavaScriptSerializer


            serializer = New JavaScriptSerializer()




            Dim s As Autentificazione = serializer.Deserialize(Of Autentificazione)(stringa)


            Return s.access_token
        Catch ex As Exception
            Return ""
        End Try
    End Function


    Private Sub ExportPDF()
        Session("PdfDaHTML") = ""
        Dim TabParametri As New Cls_Parametri

        TabParametri.LeggiParametri(Session("DC_OSPITE"))

        Dim Appo As New Cls_Contratto


        Appo.Id = Val(DD_TipoContratto.SelectedValue)

        Appo.Leggi(Session("DC_OSPITE"))


        ' Leggo il contenuto del file sino alla fine (ReadToEnd)
        Dim contenuto As String = Appo.Testo

        Dim TabSoc As New Cls_TabellaSocieta

        TabSoc.Leggi(Session("DC_TABELLE"))

        Dim Osp As New ClsOspite

        Osp.CodiceOspite = Session("CODICEOSPITE")
        Osp.Leggi(Session("DC_OSPITE"), Osp.CodiceOspite)


        Dim CentroServizio As New Cls_CentroServizio

        CentroServizio.CENTROSERVIZIO = Session("CODICESERVIZIO")
        CentroServizio.Leggi(Session("DC_OSPITE"), CentroServizio.CENTROSERVIZIO)

        contenuto = contenuto.Replace("@CENTROSERVIZIO@", Osp.Nome)

        If TabParametri.ApiV1 = 1 Then

            Call LeggiAnagraficaEpersonam(contenuto, Osp.IdEpersonam, CentroServizio.EPersonam)
        End If


        contenuto = contenuto.Replace("@COGNOMENOME@", Osp.Nome)
        contenuto = contenuto.Replace("@CODICEFISCALE@", Osp.CODICEFISCALE)

        contenuto = contenuto.Replace("@COGNOMENOME@", Osp.Nome)

        contenuto = contenuto.Replace("@TELEFONOOSPITE@", Osp.RESIDENZATELEFONO1)


        contenuto = contenuto.Replace("@NCARTELLA@", Osp.CartellaClinica)
        contenuto = contenuto.Replace("@SESSO@", Osp.Sesso)

        contenuto = contenuto.Replace("@CODICEOSPITE@", Osp.CodiceOspite)



        Dim Stato As New Cls_StatoAuto


        Stato.CODICEOSPITE = Osp.CodiceOspite
        Stato.CENTROSERVIZIO = Session("CODICESERVIZIO")
        Stato.UltimaData(Session("DC_OSPITE"), Stato.CODICEOSPITE, Stato.CENTROSERVIZIO)

        If Stato.STATOAUTO = "A" Then
            contenuto = contenuto.Replace("@STATO@", "Autosufficiente")
        End If

        If Stato.STATOAUTO = "N" Then
            contenuto = contenuto.Replace("@STATO@", "Autosufficiente")
        End If

        Dim TabReg As New ClsUSL

        TabReg.CodiceRegione = Stato.USL
        TabReg.Leggi(Session("DC_OSPITE"))

        contenuto = contenuto.Replace("@USL@", TabReg.Nome)


        Dim Accoglimento As New Cls_Movimenti
        Accoglimento.CodiceOspite = Osp.CodiceOspite
        Accoglimento.CENTROSERVIZIO = Session("CODICESERVIZIO")
        Accoglimento.UltimaDataAccoglimento(Session("DC_OSPITE"))


        contenuto = contenuto.Replace("@DATAACCOGLIMENTO@", Format(Accoglimento.Data, "dd/MM/yyyy"))

        contenuto = contenuto.Replace("@DATANASCITA@", Format(Osp.DataNascita, "dd/MM/yyyy"))

        Dim ComNasc As New ClsComune

        ComNasc.Comune = Osp.ComuneDiNascita
        ComNasc.Provincia = Osp.ProvinciaDiNascita
        ComNasc.Leggi(Session("DC_OSPITE"))

        contenuto = contenuto.Replace("@COMUNENASCITA@", ComNasc.Descrizione)

        Dim UltimaQuota As New Cls_rettatotale



        UltimaQuota.CENTROSERVIZIO = Session("CODICESERVIZIO")
        UltimaQuota.CODICEOSPITE = Osp.CodiceOspite
        UltimaQuota.UltimaData(Session("DC_OSPITE"), UltimaQuota.CODICEOSPITE, UltimaQuota.CENTROSERVIZIO)

        Dim TipoExtra As New Cls_ExtraFisso

        TipoExtra.CODICEOSPITE = UltimaQuota.CODICEOSPITE
        TipoExtra.CENTROSERVIZIO = UltimaQuota.CENTROSERVIZIO
        TipoExtra.Data = UltimaQuota.Data
        TipoExtra.UltimaData(Session("DC_OSPITE"))

        Dim Decodifica As New Cls_TipoExtraFisso

        Decodifica.Descrizione = ""
        If TipoExtra.CODICEEXTRA <> "" Then
            Decodifica.CODICEEXTRA = TipoExtra.CODICEEXTRA
            Decodifica.Leggi(Session("DC_OSPITE"))

            Decodifica.Descrizione = " + " & Decodifica.Descrizione
        End If

        Dim MovStanza As New Cls_MovimentiStanze


        MovStanza.CentroServizio = Session("CODICESERVIZIO")
        MovStanza.CodiceOspite = Osp.CodiceOspite
        MovStanza.UltimoMovimentoOspiteSenza(Session("DC_OSPITIACCESSORI"))

        contenuto = contenuto.Replace("@LETTO@", MovStanza.Villa & " " & MovStanza.Reparto & " " & MovStanza.Stanza & " " & MovStanza.Letto)



        contenuto = contenuto.Replace("@OSPITEINDIRIZZO@", Osp.RESIDENZAINDIRIZZO1)
        contenuto = contenuto.Replace("@OSPITECAP@", Osp.RESIDENZACAP1)

        Dim ApoO As New ClsComune

        ApoO.Provincia = Osp.RESIDENZAPROVINCIA1
        ApoO.Comune = Osp.RESIDENZACOMUNE1
        ApoO.Leggi(Session("DC_OSPITE"))

        contenuto = contenuto.Replace("@OSPITECOMUNE@", ApoO.Descrizione)


        Dim QR As New Cls_CalcoloRette

        QR.ConnessioneGenerale = Session("DC_GENERALE")

        QR.STRINGACONNESSIONEDB = Session("DC_OSPITE")

        QR.ApriDB(Session("DC_OSPITE"), Session("DC_OSPITIACCESSORI"))

        If QR.QuoteGiornaliere(Session("CODICESERVIZIO"), Session("CODICEOSPITE"), "O", 0, Now) > 0 Then

            contenuto = contenuto.Replace("@INTESTATARIO@", Osp.Nome)
            contenuto = contenuto.Replace("@INTESTATARIOINDIRIZZO@", Osp.RESIDENZAINDIRIZZO1)
            contenuto = contenuto.Replace("@INTESTATARIOCAP@", Osp.RESIDENZACAP1)

            Dim ApoC As New ClsComune

            ApoC.Provincia = Osp.RESIDENZAPROVINCIA1
            ApoC.Comune = Osp.RESIDENZACOMUNE1
            ApoC.Leggi(Session("DC_OSPITE"))

            contenuto = contenuto.Replace("@INTESTATARIOCOMUNE@", ApoC.Descrizione)


            contenuto = contenuto.Replace("@QUOTA@", QR.QuoteGiornaliere(Session("CODICESERVIZIO"), Session("CODICEOSPITE"), "O", 0, Now) & "&euro; " & Decodifica.Descrizione)


        Else
            If QR.QuoteGiornaliere(Session("CODICESERVIZIO"), Session("CODICEOSPITE"), "P", 1, Now) > 0 Then
                Dim PAR As New Cls_Parenti


                PAR.CodiceOspite = Session("CODICEOSPITE")
                PAR.CodiceParente = 1
                PAR.Leggi(Session("DC_OSPITE"), PAR.CodiceOspite, 1)
                contenuto = contenuto.Replace("@INTESTATARIO@", PAR.Nome)
                contenuto = contenuto.Replace("@INTESTATARIOINDIRIZZO@", PAR.RESIDENZAINDIRIZZO1)
                contenuto = contenuto.Replace("@INTESTATARIOCAP@", PAR.RESIDENZACAP1)

                Dim ApoC As New ClsComune

                ApoC.Provincia = PAR.RESIDENZAPROVINCIA1
                ApoC.Comune = PAR.RESIDENZACOMUNE1
                ApoC.Leggi(Session("DC_OSPITE"))

                contenuto = contenuto.Replace("@INTESTATARIOCOMUNE@", ApoC.Descrizione)

                contenuto = contenuto.Replace("@QUOTA@", QR.QuoteGiornaliere(Session("CODICESERVIZIO"), Session("CODICEOSPITE"), "P", 1, Now) & "&euro; " & Decodifica.Descrizione)
            End If
            If QR.QuoteGiornaliere(Session("CODICESERVIZIO"), Session("CODICEOSPITE"), "P", 2, Now) > 0 Then
                Dim PAR As New Cls_Parenti


                PAR.CodiceOspite = Session("CODICEOSPITE")
                PAR.CodiceParente = 2
                PAR.Leggi(Session("DC_OSPITE"), PAR.CodiceOspite, 2)
                contenuto = contenuto.Replace("@INTESTATARIO@", PAR.Nome)
                contenuto = contenuto.Replace("@INTESTATARIOINDIRIZZO@", PAR.RESIDENZAINDIRIZZO1)
                contenuto = contenuto.Replace("@INTESTATARIOCAP@", PAR.RESIDENZACAP1)

                Dim ApoC As New ClsComune

                ApoC.Provincia = PAR.RESIDENZAPROVINCIA1
                ApoC.Comune = PAR.RESIDENZACOMUNE1
                ApoC.Leggi(Session("DC_OSPITE"))

                contenuto = contenuto.Replace("@INTESTATARIOCOMUNE@", ApoC.Descrizione)

                contenuto = contenuto.Replace("@QUOTA@", QR.QuoteGiornaliere(Session("CODICESERVIZIO"), Session("CODICEOSPITE"), "P", 2, Now) & "&euro; " & Decodifica.Descrizione)
            End If
            If QR.QuoteGiornaliere(Session("CODICESERVIZIO"), Session("CODICEOSPITE"), "P", 3, Now) > 0 Then
                Dim PAR As New Cls_Parenti


                PAR.CodiceOspite = Session("CODICEOSPITE")
                PAR.CodiceParente = 3
                PAR.Leggi(Session("DC_OSPITE"), PAR.CodiceOspite, 3)
                contenuto = contenuto.Replace("@INTESTATARIO@", PAR.Nome)
                contenuto = contenuto.Replace("@INTESTATARIOINDIRIZZO@", PAR.RESIDENZAINDIRIZZO1)
                contenuto = contenuto.Replace("@INTESTATARIOCAP", PAR.RESIDENZACAP1)

                Dim ApoC As New ClsComune

                ApoC.Provincia = PAR.RESIDENZAPROVINCIA1
                ApoC.Comune = PAR.RESIDENZACOMUNE1
                ApoC.Leggi(Session("DC_OSPITE"))

                contenuto = contenuto.Replace("@INTESTATARIOCOMUNE@", ApoC.Descrizione)

                contenuto = contenuto.Replace("@QUOTA@", QR.QuoteGiornaliere(Session("CODICESERVIZIO"), Session("CODICEOSPITE"), "P", 3, Now) & "&euro; " & Decodifica.Descrizione)
            End If
        End If



        contenuto = contenuto.Replace("@CODICEFISCALE@", Osp.CODICEFISCALE)

        contenuto = contenuto.Replace("@MEDICO@", Osp.NomeMedico)

        contenuto = contenuto.Replace("@LIBERO1@", Txt_Libero1.text)
        contenuto = contenuto.Replace("@LIBERO2@", Txt_Libero2.text)
        contenuto = contenuto.Replace("@LIBERO3@", Txt_Libero3.text)



        contenuto = contenuto.Replace("@NUMERODOCUMENTO@", Osp.NumeroDocumento)

        Select Case Osp.TipoDocumento
            Case "IDENT"
                contenuto = contenuto.Replace("@TIPODOCUMENTO@", "CARTA IDENTITA'")
            Case "CERID"
                contenuto = contenuto.Replace("@TIPODOCUMENTO@", "CERTIFICATO D'IDENTITA'")
            Case "PASOR"
                contenuto = contenuto.Replace("@TIPODOCUMENTO@", "PASSAPORTO ORDINARIO")
            Case "PASDI"
                contenuto = contenuto.Replace("@TIPODOCUMENTO@", "PASSAPORTO DIPLOMATICO")
            Case "TESMS"
                contenuto = contenuto.Replace("@TIPODOCUMENTO@", "TESS.MINISTERO SANITA'")
            Case Else
                contenuto = contenuto.Replace("@TIPODOCUMENTO@", Osp.TipoDocumento)
        End Select


        If Year(Osp.DataDocumento) > 1900 Then
            contenuto = contenuto.Replace("@DATADOCUMENTO@", Osp.DataDocumento)
        Else
            contenuto = contenuto.Replace("@DATADOCUMENTO@", "")
        End If


        Dim PARE As New Cls_Parenti


        PARE.CodiceOspite = Session("CODICEOSPITE")
        PARE.CodiceParente = 1
        PARE.Leggi(Session("DC_OSPITE"), PARE.CodiceOspite, 1)
        contenuto = contenuto.Replace("@PARENTE1@", PARE.Nome)
        contenuto = contenuto.Replace("@INDIRIZZOPARENTE1@", PARE.RESIDENZAINDIRIZZO1)
        contenuto = contenuto.Replace("@CAPPARENTE1@", PARE.RESIDENZACAP1)

        contenuto = contenuto.Replace("@TELEFONOPARENTE1@", PARE.Telefono1)

        Dim ApoC2 As New ClsComune

        ApoC2.Provincia = PARE.RESIDENZAPROVINCIA1
        ApoC2.Comune = PARE.RESIDENZACOMUNE1
        ApoC2.Leggi(Session("DC_OSPITE"))

        contenuto = contenuto.Replace("@COMUNEPARENTE1@", ApoC2.Descrizione)

        Dim PARE1 As New Cls_Parenti


        PARE1.CodiceOspite = Session("CODICEOSPITE")
        PARE1.CodiceParente = 2
        PARE1.Leggi(Session("DC_OSPITE"), PARE1.CodiceOspite, 2)
        contenuto = contenuto.Replace("@PARENTE2@", PARE1.Nome)
        contenuto = contenuto.Replace("@INDIRIZZOPARENTE2@", PARE1.RESIDENZAINDIRIZZO1)
        contenuto = contenuto.Replace("@CAPPARENTE2@", PARE1.RESIDENZACAP1)

        contenuto = contenuto.Replace("@TELEFONOPARENTE2@", PARE1.Telefono1)
        Dim ApoC1 As New ClsComune

        ApoC1.Provincia = PARE1.RESIDENZAPROVINCIA1
        ApoC1.Comune = PARE1.RESIDENZACOMUNE1
        ApoC1.Leggi(Session("DC_OSPITE"))

        contenuto = contenuto.Replace("@COMUNEPARENTE2@", ApoC1.Descrizione)


        contenuto = contenuto.Replace("é", "&eacute;")
        contenuto = contenuto.Replace("è", "&egrave;")
        contenuto = contenuto.Replace("à", "&agrave;")
        contenuto = contenuto.Replace("ò", "&ograve;")
        contenuto = contenuto.Replace("ù", "&ugrave;")
        contenuto = contenuto.Replace("ì", "&igrave;")

        contenuto = contenuto.Replace("é", "&eacute;")
        contenuto = contenuto.Replace("È", "&Egrave;")
        contenuto = contenuto.Replace("À", "&Aacute;")
        contenuto = contenuto.Replace("Ò", "&Ograve;")
        contenuto = contenuto.Replace("Ù", "&Ugrave;")
        contenuto = contenuto.Replace("Ì", "&Igrave;")
        contenuto = contenuto.Replace("'", "&#39;")

        contenuto = contenuto.Replace("´", "&#180;")
        contenuto = contenuto.Replace("`", "&#96;")


        Session("htmlAppoggio") = contenuto
        contenuto = Server.UrlEncode(contenuto.Replace(vbNewLine, ""))

        Session("PdfDaHTML") = contenuto
    End Sub


    Public Function SHA1(ByVal StringaDaConvertire As String) As String
        Dim sha2 As New System.Security.Cryptography.SHA1CryptoServiceProvider()
        Dim hash() As Byte
        Dim bytes() As Byte
        Dim output As String = ""
        Dim i As Integer

        bytes = System.Text.Encoding.UTF8.GetBytes(StringaDaConvertire)
        hash = sha2.ComputeHash(bytes)

        For i = 0 To hash.Length - 1
            output = output & hash(i).ToString("x2")
        Next

        Return output
    End Function


    Private Sub StampaScheda(ByVal Copie As Long)            
        Dim RsLet As New ADODB.Recordset
        Dim RsParenti As New ADODB.Recordset
        Dim RsImporti As New ADODB.Recordset
        Dim OspitiDb As New ADODB.Connection

        OspitiDb.Open(Session("DC_OSPITE"))



        Dim XOsp As New ClsOspite

        XOsp.Leggi(Session("DC_OSPITE"), Session("CODICEOSPITE"))

        Dim X As New Cls_FunzioniVB6

        X.StringaGenerale = Session("DC_GENERALE")
        X.StringaTabelle = Session("DC_TABELLE")
        X.StringaOspiti = Session("DC_OSPITE")
        X.ApriDB()

        Dim q As New Cls_CalcoloRette

        q.STRINGACONNESSIONEDB = Session("DC_OSPITE")
        q.ConnessioneGenerale = Session("DC_GENERALE")
        q.ApriDB(Session("DC_OSPITE"), Session("DC_OSPITIACCESSORI"))

        Dim K As New Cls_Movimenti

        Dim Scheda As New StampeOspiti

        For I = 1 To Copie
            Dim MyRs As System.Data.DataRow = Scheda.Tables("ANAGRAFICA").NewRow
            MyRs.Item("Nome") = XOsp.Nome
            If XOsp.RESIDENZAINDIRIZZO2 = "" Then
                MyRs.Item("Indirizzo") = XOsp.RESIDENZAINDIRIZZO1
                MyRs.Item("Cap") = XOsp.RESIDENZACAP1

                Dim KComunew As New ClsComune

                KComunew.Provincia = XOsp.RESIDENZAPROVINCIA1
                KComunew.Comune = XOsp.RESIDENZACOMUNE1
                KComunew.Leggi(Session("DC_OSPITE"))


                MyRs.Item("Comune") = X.DecodificaComune(XOsp.RESIDENZAPROVINCIA1, XOsp.RESIDENZACOMUNE1) ' & " (" & x.CampoProvincia(CampoOspite(Txt_CodiceOspite.Text, "RESIDENZAPROVINCIA1"), "RESIDENZAPROVINCIA1") & ")")
            Else

                MyRs.Item("Indirizzo") = XOsp.RESIDENZAINDIRIZZO2
                MyRs.Item("Cap") = XOsp.RESIDENZACAP2
                MyRs.Item("Comune") = X.DecodificaComune(XOsp.RESIDENZAPROVINCIA2, XOsp.RESIDENZACOMUNE2) '& " (" & CampoProvincia(CampoOspite(Txt_CodiceOspite.Text, "RESIDENZAPROVINCIA2"), "RESIDENZAPROVINCIA2") & ")")

                MyRs.Item("DataCambio") = XOsp.RESIDENZADATA1
                MyRs.Item("VecchioComune") = X.DecodificaComune(XOsp.RESIDENZAPROVINCIA1, XOsp.RESIDENZACOMUNE1) '& " (" & CampoProvincia(CampoOspite(Txt_CodiceOspite.Text, "RESIDENZAPROVINCIA1"), "RESIDENZAPROVINCIA1") & ")")
                MyRs.Item("VecchioIndirizzo") = XOsp.RESIDENZAINDIRIZZO1
            End If
            MyRs.Item("CodiceFiscale") = XOsp.CODICEFISCALE
            MyRs.Item("CodiceOspite") = XOsp.CodiceOspite
            MyRs.Item("DataNascita") = XOsp.DataNascita

            K.CENTROSERVIZIO = Session("CODICESERVIZIO")
            K.CodiceOspite = Session("CODICEOSPITE")
            K.UltimaDataAccoglimento(Session("DC_OSPITE"))
            MyRs.Item("ACCOGLIMENTO") = K.Data
            MyRs.Item("ComuneNascita") = X.DecodificaComune(X.CampoOspite(XOsp.CodiceOspite, "ProvinciaDiNascita"), X.CampoOspite(XOsp.CodiceOspite, "ComuneDiNascita"))

            Dim ktab As New Cls_Tabelle

            ktab.Codice = XOsp.StatoCivile
            ktab.TipTab = "STC"
            ktab.Leggi(Session("DC_OSPITE"))

            MyRs.Item("STATOCIVILE") = ktab.Descrizione
            MyRs.Item("StrutturaProvenienza") = XOsp.StrutturaProvenienza

            MyRs.Item("COGNOMECONIUGE") = XOsp.NOMECONIUGE

            MyRs.Item("CodiceLavanderia") = XOsp.CodiceLavanderia
            MyRs.Item("StrutturaProvenienza") = XOsp.StrutturaProvenienza
            MyRs.Item("AssistenteSociale") = XOsp.AssistenteSociale
            MyRs.Item("Distretto") = XOsp.Distretto
            MyRs.Item("NumeroDocumento") = XOsp.NumeroDocumento
            MyRs.Item("DataScadenzaDocumento") = XOsp.ScadenzaDocumento


            Dim DecCserv As New Cls_CentroServizio

            DecCserv.CENTROSERVIZIO = Session("CODICESERVIZIO")
            DecCserv.Leggi(Session("DC_OSPITE"), Session("CODICESERVIZIO"))


            MyRs.Item("CentroServizio") = DecCserv.DESCRIZIONE

            Dim DecTabelle As New Cls_Tabelle

            DecTabelle.TipTab = "STC"
            DecTabelle.Codice = XOsp.StatoCivile

            DecTabelle.Leggi(Session("DC_OSPITE"))
            MyRs.Item("StatoCivile") = DecTabelle.Descrizione

            RsParenti.Open("Select * From AnagraficaComune Where CODICEOSPITE = " & XOsp.CodiceOspite & " and TIPOLOGIA = 'P' ", OspitiDb, ADODB.CursorTypeEnum.adOpenKeyset, ADODB.LockTypeEnum.adLockOptimistic)
            If Not RsParenti.EOF Then
                MyRs.Item("PARENTENOME") = X.MoveFromDbWC(RsParenti, "Nome")  '& " TEL :" & X.MoveFromDbWC(RsParenti, "TELEFONO1")
                MyRs.Item("PARENTEINDIRIZZO") = X.MoveFromDbWC(RsParenti, "RESIDENZAINDIRIZZO1")

                MyRs.Item("PARENTE1TELEFONO") = X.MoveFromDbWC(RsParenti, "TELEFONO1") & " " & X.MoveFromDbWC(RsParenti, "TELEFONO2") & " " & X.MoveFromDbWC(RsParenti, "RESIDENZATELEFONO1") & " " & X.MoveFromDbWC(RsParenti, "RESIDENZATELEFONO2")

                Dim MyTabella As New Cls_Tabelle

                MyTabella.Codice = X.MoveFromDbWC(RsParenti, "GradoParentela")
                MyTabella.TipTab = "GPA"
                MyTabella.Leggi(Session("DC_OSPITE"))
                MyRs.Item("PARENTE1GRADOPARENTELA") = MyTabella.Descrizione

                
                MyRs.Item("PARENTECAP") = X.MoveFromDbWC(RsParenti, "RESIDENZACAP1")
                MyRs.Item("PARENTECOMUNE") = X.DecodificaComune(X.MoveFromDbWC(RsParenti, "RESIDENZAPROVINCIA1"), X.MoveFromDbWC(RsParenti, "RESIDENZACOMUNE1"))
                MyRs.Item("PARENTECODICEFISCALE") = X.MoveFromDbWC(RsParenti, "CodiceFiscale")

                If IsDate(X.MoveFromDbWC(RsParenti, "DataNascita")) Then
                    MyRs.Item("PARENTEDataNascita") = X.MoveFromDbWC(RsParenti, "DataNascita")
                End If

                MyRs.Item("PARENTEIMPORTORETTA") = q.QuoteGiornaliere(Session("CODICESERVIZIO"), XOsp.CodiceOspite, "O", 0, Now)


                MyRs.Item("TOTALEIMPORTIPARENTI") = q.QuoteGiornaliere(Session("CODICESERVIZIO"), XOsp.CodiceOspite, "P", 1, Now) + q.QuoteGiornaliere(Session("CODICESERVIZIO"), XOsp.CodiceOspite, "P", 2, Now) + _
                                                    q.QuoteGiornaliere(Session("CODICESERVIZIO"), XOsp.CodiceOspite, "P", 3, Now) + q.QuoteGiornaliere(Session("CODICESERVIZIO"), XOsp.CodiceOspite, "P", 4, Now) + _
                                                    q.QuoteGiornaliere(Session("CODICESERVIZIO"), XOsp.CodiceOspite, "P", 5, Now)

                RsParenti.MoveNext()
                If Not RsParenti.EOF Then
                    MyRs.Item("NOMEPARENTE2") = X.MoveFromDbWC(RsParenti, "Nome") '& " TEL :" & X.MoveFromDbWC(RsParenti, "TELEFONO1")
                    MyRs.Item("PARENTE2TELEFONO") = X.MoveFromDbWC(RsParenti, "TELEFONO1") & " " & X.MoveFromDbWC(RsParenti, "TELEFONO2") & " " & X.MoveFromDbWC(RsParenti, "RESIDENZATELEFONO1") & " " & X.MoveFromDbWC(RsParenti, "RESIDENZATELEFONO2")
                    MyTabella.Codice = X.MoveFromDbWC(RsParenti, "GradoParentela")
                    MyTabella.TipTab = "GPA"
                    MyTabella.Leggi(Session("DC_OSPITE"))
                    MyRs.Item("PARENTE2GRADOPARENTELA") = MyTabella.Descrizione
                    RsParenti.MoveNext()
                    If Not RsParenti.EOF Then
                        MyRs.Item("NOMEPARENTE3") = X.MoveFromDbWC(RsParenti, "Nome") '& " TEL :" & X.MoveFromDbWC(RsParenti, "TELEFONO1")
                        MyRs.Item("PARENTE3TELEFONO") = X.MoveFromDbWC(RsParenti, "TELEFONO1") & " " & X.MoveFromDbWC(RsParenti, "TELEFONO2") & " " & X.MoveFromDbWC(RsParenti, "RESIDENZATELEFONO1") & " " & X.MoveFromDbWC(RsParenti, "RESIDENZATELEFONO2")
                        MyTabella.Codice = X.MoveFromDbWC(RsParenti, "GradoParentela")
                        MyTabella.TipTab = "GPA"
                        MyTabella.Leggi(Session("DC_OSPITE"))
                        MyRs.Item("PARENTE3GRADOPARENTELA") = MyTabella.Descrizione
                        RsParenti.MoveNext()
                        If Not RsParenti.EOF Then
                            MyRs.Item("NOMEPARENTE4") = X.MoveFromDbWC(RsParenti, "Nome") '& " TEL :" & X.MoveFromDbWC(RsParenti, "TELEFONO1")
                            MyRs.Item("PARENTE4TELEFONO") = X.MoveFromDbWC(RsParenti, "TELEFONO1") & " " & X.MoveFromDbWC(RsParenti, "TELEFONO2") & " " & X.MoveFromDbWC(RsParenti, "RESIDENZATELEFONO1") & " " & X.MoveFromDbWC(RsParenti, "RESIDENZATELEFONO2")

                            MyTabella.Codice = X.MoveFromDbWC(RsParenti, "GradoParentela")
                            MyTabella.TipTab = "GPA"
                            MyTabella.Leggi(Session("DC_OSPITE"))
                            MyRs.Item("PARENTE4GRADOPARENTELA") = MyTabella.Descrizione

                            RsParenti.MoveNext()
                            If Not RsParenti.EOF Then
                                MyRs.Item("NOMEPARENTE5") = X.MoveFromDbWC(RsParenti, "Nome") ' & " TEL :" & X.MoveFromDbWC(RsParenti, "TELEFONO1")
                                MyRs.Item("PARENTE5TELEFONO") = X.MoveFromDbWC(RsParenti, "TELEFONO1") & " " & X.MoveFromDbWC(RsParenti, "TELEFONO2") & " " & X.MoveFromDbWC(RsParenti, "RESIDENZATELEFONO1") & " " & X.MoveFromDbWC(RsParenti, "RESIDENZATELEFONO2")

                                MyTabella.Codice = X.MoveFromDbWC(RsParenti, "GradoParentela")
                                MyTabella.TipTab = "GPA"
                                MyTabella.Leggi(Session("DC_OSPITE"))
                                MyRs.Item("PARENTE5GRADOPARENTELA") = MyTabella.Descrizione
                            End If
                        End If
                    End If
                End If
            End If
            RsParenti.Close()

            MyRs.Item("ComuneNascita") = X.DecodificaComune(X.CampoOspite(XOsp.CodiceOspite, "ProvinciaDiNascita"), X.CampoOspite(XOsp.CodiceOspite, "ComuneDiNascita"))
            If IsDate(X.CampoOspite(XOsp.CodiceOspite, "DataMorte")) Then
                MyRs.Item("DataDecesso") = X.CampoOspite(XOsp.CodiceOspite, "DataMorte")
            End If

            MyRs.Item("LuogoDecesso") = X.CampoOspite(XOsp.CodiceOspite, "LuogoMorte")
            If IsDate(X.CampoOspite(XOsp.CodiceOspite, "DataDomanda")) Then
                MyRs.Item("DataDomanda") = X.CampoOspite(XOsp.CodiceOspite, "DataDomanda")
            End If
            'MoveToDb MyRs.Fields("DataUscitaDefinitiva, CampoOspite(XOsp.CodiceOspite, "")
            MyRs.Item("RECAPITOINDIRIZZO") = X.CampoOspite(XOsp.CodiceOspite, "RECAPITOINDIRIZZO")
            MyRs.Item("RECAPITOCOMUNE") = X.DecodificaComune(X.CampoOspite(XOsp.CodiceOspite, "RECAPITOPROVINCIA"), X.CampoOspite(XOsp.CodiceOspite, "RECAPITOCOMUNE"))
            MyRs.Item("RecapitoTelefono") = X.CampoOspite(XOsp.CodiceOspite, "ResidenzaTelefono4")
            MyRs.Item("Note") = XOsp.NOTE

            XOsp.CodiceCentroServizio = Session("CODICESERVIZIO")
            MyRs.Item("ImportoDeposito") = XOsp.Func_ImportoDeposito(Session("DC_OSPITE"), Session("DC_GENERALE"), Session("DC_TABELLE"))
            MyRs.Item("DataDeposito") = XOsp.Func_DataDeposito(Session("DC_OSPITE"), Session("DC_GENERALE"))

            Dim StatoD As String = ""
            Dim DecodRegione As String = ""

            RsLet.Open("Select * From StatoAuto Where CENTROSERVIZIO = '" & Session("CODICESERVIZIO") & "' and CodiceOspite = " & XOsp.CodiceOspite & " Order by DATA DESC", OspitiDb, ADODB.CursorTypeEnum.adOpenKeyset, ADODB.LockTypeEnum.adLockOptimistic)
            If Not RsLet.EOF Then
                If X.MoveFromDbWC(RsLet, "STATOAUTO") = "A" Then
                    StatoD = "Autosufficente"
                End If
                If X.MoveFromDbWC(RsLet, "STATOAUTO") = "N" Then
                    StatoD = "Nonautosufficente"
                End If
                If X.MoveFromDbWC(RsLet, "USL") <> "" Then
                    Dim DcReg As New ClsUSL
                    DcReg.CodiceRegione = X.MoveFromDbWC(RsLet, "USL")
                    DcReg.Leggi(Session("DC_OSPITE"))
                    DecodRegione = DcReg.Nome
                End If

                MyRs.Item("DataAutoSufficenza") = X.MoveFromDbWC(RsLet, "Data")
                MyRs.Item("StatoAutosufficenza") = StatoD
                MyRs.Item("UslAuto") = DecodRegione
            End If
            RsLet.Close()


            Dim MyModalita As New Cls_Modalita

            
            MyModalita.UltimaData(Session("DC_OSPITE"), Session("CODICEOSPITE"), Session("CODICESERVIZIO"))

            MyRs.Item("ModalitaDiCalcolo") = MyModalita.MODALITA
            MyRs.Item("DataModalita") = MyModalita.Data



            

            RsLet.Open("Select * From IMPEGNATIVE Where CENTROSERVIZIO = '" & Session("CODICESERVIZIO") & "' and CodiceOspite = " & XOsp.CodiceOspite & " Order by DATAINIZIO DESC", OspitiDb, ADODB.CursorTypeEnum.adOpenKeyset, ADODB.LockTypeEnum.adLockOptimistic)
            If Not RsLet.EOF Then
                If IsDate(X.MoveFromDbWC(RsLet, "DATAINIZIO")) Then
                    MyRs.Item("DataDalImpegnativa") = X.MoveFromDbWC(RsLet, "DATAINIZIO")
                End If
                If IsDate(X.MoveFromDbWC(RsLet, "DATAFINE")) Then
                    MyRs.Item("DataAlImpegnativa") = X.MoveFromDbWC(RsLet, "DATAFINE")
                End If
                MyRs.Item("IMPEGNATIVA") = X.MoveFromDbWC(RsLet, "Descrizione")
            End If
            RsLet.Close()

            Dim Medico As New Cls_Medici

            Medico.CodiceMedico = XOsp.CodiceMedico
            Medico.Leggi(Session("DC_OSPITE"))

            If XOsp.CodiceMedico <> "" Then
                MyRs.Item("Medico") = XOsp.CodiceMedico & " " & Medico.Nome
            Else
                MyRs.Item("Medico") = XOsp.NomeMedico
            End If



            MyRs.Item("CodiceSanitario") = XOsp.CodiceSanitario

            MyRs.Item("TesseraSanitaria") = XOsp.TesseraSanitaria
            MyRs.Item("TelefonoMedico") = Medico.Telefono1 & " " & Medico.Telefono2

            MyRs.Item("EsenzioneTicket") = XOsp.CodiceTicket & " " & XOsp.CodiceTicket2
            MyRs.Item("RilascioTicket") = XOsp.DataRilascio
            MyRs.Item("ScadenzaTicket") = XOsp.DataScadenza
            MyRs.Item("ImportoUsl") = q.QuoteGiornaliere(Session("CODICESERVIZIO"), XOsp.CodiceOspite, "R", 1, Now)
            MyRs.Item("ImportoComune") = q.QuoteGiornaliere(Session("CODICESERVIZIO"), XOsp.CodiceOspite, "C", 1, Now)
            MyRs.Item("ImportoRetta") = q.QuoteGiornaliere(Session("CODICESERVIZIO"), XOsp.CodiceOspite, "O", 0, Now)

            Scheda.Tables("ANAGRAFICA").Rows.Add(MyRs)
        Next


        OspitiDb.Close()

        X.ChiudiDB()

        q.ChiudiDB()

        Session("stampa") = Scheda
    End Sub

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load

        If Page.IsPostBack = True Then
            Exit Sub
        End If


        Dim x As New ClsOspite

        x.CodiceOspite = Session("CODICEOSPITE")
        x.Leggi(Session("DC_OSPITE"), x.CodiceOspite)

        Dim Log As New Cls_LogPrivacy
        Log.LogPrivacy(Application("SENIOR"), Session("CLIENTE"), Session("UTENTE"), Session("UTENTEIMPER"), Page.GetType.Name.ToUpper, Session("CODICEOSPITE"), 0, "", "", 0, x.Nome, "V", "SCHEDA", "")


        Dim M As New Cls_Contratto

        M.UpDropDownList(Session("DC_OSPITE"), DD_TipoContratto)


        DD_NumeroEtichette.Items.Clear()
        For i = 1 To 50
            DD_NumeroEtichette.Items.Add(i)
            DD_NumeroEtichette.Items(DD_NumeroEtichette.Items.Count - 1).Value = i
        Next
    End Sub



    Protected Sub Btn_Scheda_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles Btn_Scheda.Click

        Call StampaScheda(1)


        Response.Redirect("Stampa_ReportXSD.aspx?REPORT=STAMPASCHEDA")
    End Sub

    Protected Sub Btn_Etichetta_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles Btn_Etichetta.Click
        Call StampaScheda(Val(DD_NumeroEtichette.SelectedValue))


        Response.Redirect("Stampa_ReportXSD.aspx?REPORT=STAMPAETICHETTE")
    End Sub

    Protected Sub Btn_Contratto_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles Btn_Contratto.Click
        Call StampaContratto(1)
    End Sub

    Protected Sub Btn_Word_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles Btn_Word.Click
        ExportPDF()

        Dim strBody As New StringBuilder()

        strBody.Append("<html xmlns:o='urn:schemas-microsoft-com:office:office' " & _
           "xmlns:w='urn:schemas-microsoft-com:office:word'" & _
           "xmlns='http://www.w3.org/TR/REC-html40'>" & _
           "<head><title>Time</title>")

        strBody.Append("<!--[if gte mso 9]>" & _
            "<xml>" & _
            "<w:WordDocument>" & _
            "<w:View>Print</w:View>" & _
            "<w:Zoom>90</w:Zoom>" & _
            "<w:DoNotOptimizeForBrowser/>" & _
            "</w:WordDocument>" & _
            "</xml>" & _
            "<![endif]-->")

        strBody.Append("<style>" & _
            "<!-- /* Style Definitions */" & _
            "@page Section1" & _
            "   {size:8.5in 11.0in; " & _
            "   margin:1.0in 1.25in 1.0in 1.25in ; " & _
            "   mso-header-margin:.5in; " & _
            "   mso-footer-margin:.5in; mso-paper-source:0;}" & _
            " div.Section1" & _
            "   {page:Section1;}" & _
            "-->" & _
            "</style></head>")

        strBody.Append("<body lang=EN-US style='tab-interval:.5in'><div class=Section1>")

        strBody.Append(Session("htmlAppoggio"))

        strBody.Append("</div></body></html>")


        HttpContext.Current.Response.Clear()
        HttpContext.Current.Response.Charset = ""
        HttpContext.Current.Response.ContentType = "application/msword"
        HttpContext.Current.Response.AddHeader("Content-Disposition", "inline;filename=Contratto.doc")

        HttpContext.Current.Response.Write(strBody)
        HttpContext.Current.Response.End()
        HttpContext.Current.Response.Flush()
    End Sub
End Class
