﻿Imports System.Web.Hosting

Partial Class centrale
    Inherits System.Web.UI.Page

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Trim(Session("UTENTE")) = "" Then
            Response.Redirect("../Login.aspx")
            Exit Sub
        End If

        If Session("ABILITAZIONI").ToString.IndexOf("<GO>") < 0 Then
            Response.Redirect("../MainMenu.aspx")
            Exit Sub
        End If

        lbl_container.Text = "<h1>Gestione Ospiti Web<br/></h1><table><tr><td><img src=images\qospiti.jpg></td><td bgcolor=#808080></td><td valign=top align=right>Anagrafica<br />Presenze<br />Calcolo<br />Emissione Documenti<br /></td><tr><td bgcolor=#C0C0C0></td><td><img src=images\qcontabilita.jpg></td><td bgcolor=#808080></td></tr><tr><td valign=bottom>Sodo Informatica S.r.l.<br/><a href=http://www.sodo.it>www.sodo.it</a></td><td bgcolor=#C0C0C0></td><td><img src=images\qpersonale.jpg></td></tr></table>"

        If Request.Item("TIPO") = "RicercaAnagrafica" Then
            Label1.Text = " <a href=""centrale.aspx?TIPO=RicercaAnagrafica"" iconpos=""right"" data-role=""button"" data-theme=""b""  rel=""external"">Ricerca Anagrafica</a>    "
        Else
            Label1.Text = " <a href=""centrale.aspx?TIPO=RicercaAnagrafica"" iconpos=""right"" data-role=""button""  rel=""external"">Ricerca Anagrafica</a>  "
        End If


        'Dim Tabella As New System.Data.DataTable("tabella")
        'Dim MyDataSet As New System.Data.DataSet()

        'Tabella.Clear()
        'Tabella.Columns.Add("Quota", GetType(String))
        'Tabella.Columns.Add("Importo", GetType(String))
        'Tabella.Columns.Add("Giorni", GetType(String))
        'Tabella.Columns.Add("ImportoMeseSuccessivo", GetType(String))
        'Tabella.Columns.Add("GiornoMeseSuccessivo", GetType(String))

        'Dim myriga As System.Data.DataRow = Tabella.NewRow()
        'myriga(0) = "quota"
        'myriga(1) = "importo"
        'myriga(2) = "giorni"
        'myriga(3) = "importo"
        'myriga(4) = "giorno"
        'Tabella.Rows.Add(myriga)

        'Dim myriga1 As System.Data.DataRow = Tabella.NewRow()
        'myriga1(0) = "quota1"
        'myriga1(1) = "importo1"
        'myriga1(2) = "giorni1"
        'myriga1(3) = "importo1"
        'myriga1(4) = "giorno1"
        'Tabella.Rows.Add(myriga1)

        'Tabella.WriteXml(HostingEnvironment.ApplicationPhysicalPath() & "appo.xml")


        Dim x As New ClsOspite
        Dim d As New ClsComune


        Dim ConnectionString As String = Session("DC_OSPITE")

        If Val(Request.Item("AzzeraOspite")) > 0 Then
            Session("CODICEOSPITE") = 0
        End If

        x.Leggi(ConnectionString, Session("CODICEOSPITE"))

        Dim k As New Cls_Movimenti
        Dim DataAccogilmento As Date
        Dim DataUscitaDefinitiva As Date

        k.CodiceOspite = Session("CODICEOSPITE")
        k.CENTROSERVIZIO = Session("CODICESERVIZIO")

        k.UltimaDataUscitaDefinitiva(Session("DC_OSPITE"))

        DataUscitaDefinitiva = k.Data

        k.UltimaDataAccoglimento(Session("DC_OSPITE"))
        DataAccogilmento = k.Data


        Lbl_nomeospite.Text = ""

        If Val(Session("CODICEOSPITE")) > 0 Then
            If Year(DataUscitaDefinitiva) > 1900 Then
                If Format(DataAccogilmento, "yyyyMMdd") < Format(DataUscitaDefinitiva, "yyyyMMdd") Then
                    Lbl_nomeospite.Text = "<div style=""background-color:#00FFFF; "" ><font style=""font-size: small; font-family: Arial;background-color: #00FFFF; ""><a href=""#"" class=""commentato"">Anagrafica : " & x.Nome & " " & x.DataNascita & " C.Serv. " & Session("CODICESERVIZIO") & "<div>Data Accoglimento :" & Format(DataAccogilmento, "dd/MM/yyyy") & "<BR />Uscita Definitiva : " & Format(DataUscitaDefinitiva, "dd/MM/yyyy") & "</div></a> - <a href=""centrale.aspx?AzzeraOspite=1"" rel=""external"">Esci</a></div>"
                Else
                    Lbl_nomeospite.Text = "<div style=""background-color:#00FFFF; "" ><font style=""font-size: small; font-family: Arial;background-color: #00FFFF; ""><a href=""#"" class=""commentato"">Anagrafica : " & x.Nome & " " & x.DataNascita & " C.Serv. " & Session("CODICESERVIZIO") & "<div>Data Accoglimento :" & Format(DataAccogilmento, "dd/MM/yyyy") & "</div></a> - <a href=""centrale.aspx?AzzeraOspite=1"" rel=""external"">Esci</a></div>"
                End If
            Else
                Lbl_nomeospite.Text = "<div style=""background-color:#00FFFF; "" ><font style=""font-size: small; font-family: Arial;background-color: #00FFFF; ""><a href=""#"" class=""commentato"">Anagrafica : " & x.Nome & " " & x.DataNascita & " C.Serv. " & Session("CODICESERVIZIO") & "<div>Data Accoglimento :" & Format(DataAccogilmento, "dd/MM/yyyy") & "</div></a> - <a href=""centrale.aspx?AzzeraOspite=1"" rel=""external"">Esci</a></div>"
            End If
        End If

        If Request.Item("TIPO") = "DatiAnagrafici" Then
            Label1.Text = Label1.Text & " <a href=""centrale.aspx"" data-icon=""arrow-d"" iconpos=""right"" data-role=""button"" data-theme=""a""  rel=""external"">Dati Anagrafici</a>  "
        Else
            Label1.Text = Label1.Text & " <a href=""centrale.aspx?TIPO=DatiAnagrafici"" data-icon=""arrow-d"" iconpos=""right"" data-role=""button""   rel=""external"">Dati Anagrafici</a>  "
        End If


        If Session("ABILITAZIONI").ToString.IndexOf("<ANAGRAFICA>") >= 0 And (Request.Item("TIPO") = "Anagrafica" Or Request.Item("TIPO") = "Parenti" Or _
           Request.Item("TIPO") = "Residenza" Or Request.Item("TIPO") = "Recapito" Or _
           Request.Item("TIPO") = "Deposito" Or Request.Item("TIPO") = "Deposito" Or _
           Request.Item("TIPO") = "Totale" Or Request.Item("TIPO") = "StatoAuto" Or _
           Request.Item("TIPO") = "Modalita" Or Request.Item("TIPO") = "ImpOspite" Or Request.Item("TIPO") = "ImpComune" Or _
           Request.Item("TIPO") = "ImpJolly" Or Request.Item("TIPO") = "Impegnativa" Or _
           Request.Item("TIPO") = "DatiSanitari" Or Request.Item("TIPO") = "DatiAnagrafici" Or Request.Item("TIPO") = "StampaScheda" Or _
           Request.Item("TIPO") = "MAnagrafica") Or Request.Item("TIPO") = "Documentazione" Then

            If Request.Item("TIPO") = "Anagrafica" Then
                Label1.Text = Label1.Text & " <a href=""centrale.aspx?TIPO=Anagrafica"" data-icon=""arrow-r"" iconpos=""right"" data-role=""button"" data-theme=""b""  rel=""external"">Anagrafica</a>  "
            Else
                Label1.Text = Label1.Text & " <a href=""centrale.aspx?TIPO=Anagrafica"" data-icon=""arrow-r"" iconpos=""right"" data-role=""button""   rel=""external"">Anagrafica</a>  "
            End If

            If Request.Item("TIPO") = "Parenti" Then
                Label1.Text = Label1.Text & "<a href=""centrale.aspx?TIPO=Parenti"" data-role=""button"" data-icon=""arrow-r"" iconpos=""right""  data-theme=""b""  rel=""external"">Parenti</a>  "
            Else
                Label1.Text = Label1.Text & "<a href=""centrale.aspx?TIPO=Parenti"" data-role=""button"" data-icon=""arrow-r"" iconpos=""right"" rel=""external"">Parenti</a>  "
            End If
            If Request.Item("TIPO") = "Residenza" Then
                Label1.Text = Label1.Text & "<a href=""centrale.aspx?TIPO=Residenza"" data-role=""button"" data-icon=""arrow-r"" iconpos=""right"" data-theme=""b"" rel=""external"">Residenza</a>  "
            Else
                Label1.Text = Label1.Text & "<a href=""centrale.aspx?TIPO=Residenza"" data-role=""button"" data-icon=""arrow-r"" iconpos=""right"" rel=""external"">Residenza</a>  "
            End If

            If Request.Item("TIPO") = "Recapito" Then
                Label1.Text = Label1.Text & "<a href=""centrale.aspx?TIPO=Recapito"" data-role=""button"" data-icon=""arrow-r"" iconpos=""right"" data-theme=""b""   rel=""external"">Recapito</a>  "
            Else
                Label1.Text = Label1.Text & "<a href=""centrale.aspx?TIPO=Recapito"" data-role=""button"" data-icon=""arrow-r"" iconpos=""right"" rel=""external"">Recapito</a>  "
            End If

            If Request.Item("TIPO") = "Deposito" Then
                Label1.Text = Label1.Text & "<a href=""centrale.aspx?TIPO=Deposito"" data-role=""button"" data-icon=""arrow-r"" iconpos=""right"" data-theme=""b""  rel=""external"">Deposito</a>  "
            Else
                Label1.Text = Label1.Text & "<a href=""centrale.aspx?TIPO=Deposito"" data-role=""button"" data-icon=""arrow-r"" iconpos=""right"" rel=""external"">Deposito</a>  "
            End If

            If Request.Item("TIPO") = "Totale" Then
                Label1.Text = Label1.Text & "<a href=""centrale.aspx?TIPO=Totale"" data-role=""button"" data-icon=""arrow-r"" iconpos=""right"" data-theme=""b""  rel=""external"">Retta Totale</a>  "
            Else
                Label1.Text = Label1.Text & "<a href=""centrale.aspx?TIPO=Totale"" data-role=""button"" data-icon=""arrow-r"" iconpos=""right"" rel=""external"">Retta Totale</a>  "
            End If

            If Request.Item("TIPO") = "StatoAuto" Then
                Label1.Text = Label1.Text & "<a href=""centrale.aspx?TIPO=StatoAuto"" data-role=""button"" data-icon=""arrow-r"" iconpos=""right"" data-theme=""b""  rel=""external"">Stato Auto</a>  "
            Else
                Label1.Text = Label1.Text & "<a href=""centrale.aspx?TIPO=StatoAuto"" data-role=""button"" data-icon=""arrow-r"" iconpos=""right"" rel=""external"">Stato Auto</a>  "
            End If

            If Request.Item("TIPO") = "Modalita" Then
                Label1.Text = Label1.Text & "<a href=""centrale.aspx?TIPO=Modalita"" data-role=""button"" data-icon=""arrow-r"" iconpos=""right"" data-theme=""b""  rel=""external"">Modalità Calcolo</a>  "
            Else
                Label1.Text = Label1.Text & "<a href=""centrale.aspx?TIPO=Modalita"" data-role=""button"" data-icon=""arrow-r"" iconpos=""right"" rel=""external"">Modalità Calcolo</a>  "
            End If

            If Request.Item("TIPO") = "ImpOspite" Then
                Label1.Text = Label1.Text & "<a href=""centrale.aspx?TIPO=ImpOspite"" data-role=""button"" data-icon=""arrow-r"" iconpos=""right"" data-theme=""b""  rel=""external"">Importo Ospite</a>  "
            Else
                Label1.Text = Label1.Text & "<a href=""centrale.aspx?TIPO=ImpOspite"" data-role=""button"" data-icon=""arrow-r"" iconpos=""right"" rel=""external"">Importo Ospite</a>  "
            End If

            If Request.Item("TIPO") = "ImpJolly" Then
                Label1.Text = Label1.Text & "<a href=""centrale.aspx?TIPO=ImpJolly"" data-role=""button"" data-icon=""arrow-r"" iconpos=""right"" data-theme=""b""   rel=""external"">Importo Jolly</a>  "
            Else
                Label1.Text = Label1.Text & "<a href=""centrale.aspx?TIPO=ImpJolly"" data-role=""button"" data-icon=""arrow-r"" iconpos=""right"" rel=""external"">Importo Jolly</a>  "
            End If

            If Request.Item("TIPO") = "ImpComune" Then
                Label1.Text = Label1.Text & "<a href=""centrale.aspx?TIPO=ImpComune"" data-role=""button"" data-icon=""arrow-r"" iconpos=""right"" data-theme=""b""   rel=""external"">Importo Comune</a>  "
            Else
                Label1.Text = Label1.Text & "<a href=""centrale.aspx?TIPO=ImpComune"" data-role=""button"" data-icon=""arrow-r"" iconpos=""right"" rel=""external"">Importo Comune</a>  "
            End If

            If Request.Item("TIPO") = "Impegnativa" Then
                Label1.Text = Label1.Text & "<a href=""centrale.aspx?TIPO=Impegnativa"" data-role=""button"" data-icon=""arrow-r"" iconpos=""right"" data-theme=""b""  rel=""external"">Impegnativa</a>  "
            Else
                Label1.Text = Label1.Text & "<a href=""centrale.aspx?TIPO=Impegnativa"" data-role=""button"" data-icon=""arrow-r"" iconpos=""right"" rel=""external"">Impegnativa</a>  "
            End If
            If Request.Item("TIPO") = "DatiSanitari" Then
                Label1.Text = Label1.Text & "<a href=""centrale.aspx?TIPO=DatiSanitari"" data-role=""button"" data-icon=""arrow-r"" iconpos=""right"" data-theme=""b""  rel=""external"">Dati Sanitari</a>  "
            Else
                Label1.Text = Label1.Text & "<a href=""centrale.aspx?TIPO=DatiSanitari"" data-role=""button"" data-icon=""arrow-r"" iconpos=""right"" rel=""external"">Dati Sanitari</a>  "
            End If
            If Request.Item("TIPO") = "Documentazione" Then
                Label1.Text = Label1.Text & "<a href=""centrale.aspx?TIPO=Documentazione"" data-role=""button"" data-icon=""arrow-r"" iconpos=""right"" data-theme=""b""  rel=""external"">Documentazione</a>  "
            Else
                Label1.Text = Label1.Text & "<a href=""centrale.aspx?TIPO=Documentazione"" data-role=""button"" data-icon=""arrow-r"" iconpos=""right"" rel=""external"">Documentazione</a>  "
            End If


            If Request.Item("TIPO") = "StampaScheda" Then
                Label1.Text = Label1.Text & "<a href=""centrale.aspx?TIPO=StampaScheda"" data-role=""button"" data-icon=""arrow-r"" iconpos=""right"" data-theme=""b""  rel=""external"">StampaScheda</a>  "
            Else
                Label1.Text = Label1.Text & "<a href=""centrale.aspx?TIPO=StampaScheda"" data-role=""button"" data-icon=""arrow-r"" iconpos=""right"" rel=""external"">Stampa Scheda</a>  "
            End If
        End If

        If Request.Item("TIPO") = "RicercaAnagrafica" Then
            lbl_container.Text = "<iframe id=""output"" src=""RicercaAnagrafica.aspx"" style=""border-width:0;"" width=""100%""  data-theme=""b""   height=""600px""></iframe> "
        End If

        If Request.Item("TIPO") = "Anagrafica" Then
            If Val(Session("CODICEOSPITE")) > 0 Then
                lbl_container.Text = "<iframe id=""output"" src=""anagrafica.aspx"" style=""border-width:0;"" width=""100%""  data-theme=""b""   height=""600px""></iframe> "
            Else
                ClientScript.RegisterClientScriptBlock(Me.GetType(), "Errore", "VisualizzaErrore('<center>Devi prima richiamare un ospite</center>');", True)
            End If
        End If

        If Request.Item("TIPO") = "RicercaOspite" Then
            lbl_container.Text = "<iframe id=""output"" src=""RicercaAnagrafica.aspx"" style=""border-width:0;"" width=""100%""  data-theme=""b""  height=""600px""></iframe> "
        End If

        If Request.Item("TIPO") = "Parenti" Then
            If Val(Session("CODICEOSPITE")) > 0 Then
                lbl_container.Text = "<iframe id=""output"" src=""SelezionaParente.aspx"" style=""border-width:0;"" width=""100%""  data-theme=""b""   height=""600px""></iframe> "
            Else
                ClientScript.RegisterClientScriptBlock(Me.GetType(), "Errore", "VisualizzaErrore('<center>Devi prima richiamare un ospite</center>');", True)
            End If
        End If

        If Request.Item("TIPO") = "Residenza" Then
            If Val(Session("CODICEOSPITE")) > 0 Then
                lbl_container.Text = "<iframe id=""output"" src=""Residenza.aspx"" style=""border-width:0;"" width=""100%""  data-theme=""b""   height=""600px""></iframe> "
            Else
                ClientScript.RegisterClientScriptBlock(Me.GetType(), "Errore", "VisualizzaErrore('<center>Devi prima richiamare un ospite</center>');", True)
            End If
        End If

        If Request.Item("TIPO") = "Recapito" Then
            If Val(Session("CODICEOSPITE")) > 0 Then
                lbl_container.Text = "<iframe id=""output"" src=""Recapito.aspx"" style=""border-width:0;"" width=""100%""  data-theme=""b""   height=""600px""></iframe> "
            Else
                ClientScript.RegisterClientScriptBlock(Me.GetType(), "Errore", "VisualizzaErrore('<center>Devi prima richiamare un ospite</center>');", True)
            End If
        End If

        If Request.Item("TIPO") = "Deposito" Then
            If Val(Session("CODICEOSPITE")) > 0 Then
                lbl_container.Text = "<iframe id=""output"" src=""DatiAnagrafici.aspx"" style=""border-width:0;"" width=""100%""   data-theme=""b""  height=""600px""></iframe> "
            Else
                ClientScript.RegisterClientScriptBlock(Me.GetType(), "Errore", "VisualizzaErrore('<center>Devi prima richiamare un ospite</center>');", True)
            End If
        End If

        If Request.Item("TIPO") = "Totale" Then
            If Val(Session("CODICEOSPITE")) > 0 Then
                lbl_container.Text = "<iframe id=""output"" src=""RettaTotale.aspx"" style=""border-width:0;"" width=""100%""   data-theme=""b""   height=""600px""></iframe> "
            Else
                ClientScript.RegisterClientScriptBlock(Me.GetType(), "Errore", "VisualizzaErrore('<center>Devi prima richiamare un ospite</center>');", True)
            End If
        End If


        If Request.Item("TIPO") = "StatoAuto" Then
            If Val(Session("CODICEOSPITE")) > 0 Then
                lbl_container.Text = "<iframe id=""output"" src=""StatoAuto.aspx"" style=""border-width:0;"" width=""100%""   data-theme=""b""   height=""600px""></iframe> "
            Else
                ClientScript.RegisterClientScriptBlock(Me.GetType(), "Errore", "VisualizzaErrore('<center>Devi prima richiamare un ospite</center>');", True)
            End If
        End If

        If Request.Item("TIPO") = "Modalita" Then
            If Val(Session("CODICEOSPITE")) > 0 Then
                lbl_container.Text = "<iframe id=""output"" src=""ModalitaCalcolo.aspx"" style=""border-width:0;"" width=""100%""   data-theme=""b""  height=""600px""></iframe> "
            Else
                ClientScript.RegisterClientScriptBlock(Me.GetType(), "Errore", "VisualizzaErrore('<center>Devi prima richiamare un ospite</center>');", True)
            End If
        End If

        If Request.Item("TIPO") = "Impegnativa" Then
            If Val(Session("CODICEOSPITE")) > 0 Then
                lbl_container.Text = "<iframe id=""output"" src=""Impegnativa.aspx"" style=""border-width:0;"" width=""100%""   data-theme=""b"" height=""600px""></iframe> "
            Else
                ClientScript.RegisterClientScriptBlock(Me.GetType(), "Errore", "VisualizzaErrore('<center>Devi prima richiamare un ospite</center>');", True)
            End If
        End If

        If Request.Item("TIPO") = "ImpOspite" Then
            If Val(Session("CODICEOSPITE")) > 0 Then
                lbl_container.Text = "<iframe id=""output"" src=""ImportoOspite.aspx"" style=""border-width:0;"" width=""100%""   data-theme=""b""  height=""600px""></iframe> "
            Else
                ClientScript.RegisterClientScriptBlock(Me.GetType(), "Errore", "VisualizzaErrore('<center>Devi prima richiamare un ospite</center>');", True)
            End If
        End If
        If Request.Item("TIPO") = "ImpComune" Then
            If Val(Session("CODICEOSPITE")) > 0 Then
                lbl_container.Text = "<iframe id=""output"" src=""ImportoComune.aspx"" style=""border-width:0;"" width=""100%""   data-theme=""b""   height=""600px""></iframe> "
            Else
                ClientScript.RegisterClientScriptBlock(Me.GetType(), "Errore", "VisualizzaErrore('<center>Devi prima richiamare un ospite</center>');", True)
            End If
        End If


        If Request.Item("TIPO") = "ImpJolly" Then
            If Val(Session("CODICEOSPITE")) > 0 Then
                lbl_container.Text = "<iframe id=""output"" src=""ImportoJolly.aspx"" style=""border-width:0;"" width=""100%""   data-theme=""b""   height=""600px""></iframe> "
            Else
                ClientScript.RegisterClientScriptBlock(Me.GetType(), "Errore", "VisualizzaErrore('<center>Devi prima richiamare un ospite</center>');", True)
            End If
        End If


        If Request.Item("TIPO") = "DatiSanitari" Then
            If Val(Session("CODICEOSPITE")) > 0 Then
                lbl_container.Text = "<iframe id=""output"" src=""DatiSanitari.aspx"" style=""border-width:0;"" width=""100%""   data-theme=""b""  height=""600px""></iframe> "
            Else
                ClientScript.RegisterClientScriptBlock(Me.GetType(), "Errore", "VisualizzaErrore('<center>Devi prima richiamare un ospite</center>');", True)
            End If
        End If

        If Request.Item("TIPO") = "Documentazione" Then
            If Val(Session("CODICEOSPITE")) = 0 Then
                ClientScript.RegisterClientScriptBlock(Me.GetType(), "Errore", "VisualizzaErrore('<center>Devi prima richiamare un ospite</center>');", True)
            Else
                Dim MyLog As New Cls_Login
                MyLog.Utente = Session("UTENTE")
                MyLog.LeggiSP(Application("SENIOR"))
                If MyLog.LiveIDUser = "" Then
                    ClientScript.RegisterClientScriptBlock(Me.GetType(), "Errore", "VisualizzaErrore('<center>Devi specificare live user id</center>');", True)
                Else
                    lbl_container.Text = "<iframe id=""output"" src=""Documentazione.aspx"" style=""border-width:0;"" width=""100%""   data-theme=""b""  height=""600px""></iframe> "
                End If
            End If
        End If


        If Request.Item("TIPO") = "StampaScheda" Then
            If Val(Session("CODICEOSPITE")) > 0 Then
                lbl_container.Text = "<iframe id=""output"" src=""StampaSchedaOspiti.aspx"" style=""border-width:0;"" width=""100%""   data-theme=""b""  height=""600px""></iframe> "
            Else
                ClientScript.RegisterClientScriptBlock(Me.GetType(), "Errore", "VisualizzaErrore('<center>Devi prima richiamare un ospite</center>');", True)
            End If
        End If


        If Request.Item("TIPO") = "Quote" Then
            If Val(Session("CODICEOSPITE")) > 0 Then
                lbl_container.Text = "<iframe id=""output"" src=""quotemensili.aspx"" style=""border-width:0;"" width=""100%""   data-theme=""b""  height=""600px""></iframe> "
            Else
                ClientScript.RegisterClientScriptBlock(Me.GetType(), "Errore", "VisualizzaErrore('<center>Devi prima richiamare un ospite</center>');", True)
            End If
        End If

        If Request.Item("TIPO") = "Calcolo" And Session("ABILITAZIONI").ToString.IndexOf("<CALCOLO>") >= 0 Then
            lbl_container.Text = "<iframe id=""output"" src=""Calcolo.aspx"" style=""border-width:0;"" width=""100%""   data-theme=""b""  height=""600px""></iframe> "
        End If

        If Request.Item("TIPO") = "Emissione" And Session("ABILITAZIONI").ToString.IndexOf("<EMISSIONE>") >= 0 Then
            lbl_container.Text = "<iframe id=""output"" src=""EmissioneRette.aspx"" style=""border-width:0;"" width=""100%""   data-theme=""b""  height=""600px""></iframe> "
        End If

            If Request.Item("TIPO") = "Prospetto" Then
                lbl_container.Text = "<iframe id=""output"" src=""Prospetto.aspx"" style=""border-width:0;"" width=""100%""   data-theme=""b""  height=""600px""></iframe> "
            End If

            If Request.Item("TIPO") = "Regioni" Then
                lbl_container.Text = "<iframe id=""output"" src=""TabelleRegioni.aspx"" style=""border-width:0;"" width=""100%""   data-theme=""b""  height=""600px""></iframe> "
            End If
            If Request.Item("TIPO") = "ProvinciaComuni" Then
                lbl_container.Text = "<iframe id=""output"" src=""TabellaComuni.aspx"" style=""border-width:0;"" width=""100%""   data-theme=""b""  height=""600px""></iframe> "
            End If

            If Request.Item("TIPO") = "CentroServizio" Then
                lbl_container.Text = "<iframe id=""output"" src=""CentroServizio.aspx"" style=""border-width:0;"" width=""100%""   data-theme=""b""  height=""600px""></iframe> "
            End If

            If Request.Item("TIPO") = "CausaliEntrataUscita" Then
                lbl_container.Text = "<iframe id=""output"" src=""CausaliEntrataUscita.aspx"" style=""border-width:0;"" width=""100%""   data-theme=""b""  height=""600px""></iframe> "
            End If
            If Request.Item("TIPO") = "TIPOAD" Then
                lbl_container.Text = "<iframe id=""output"" src=""TipoAddebito.aspx"" style=""border-width:0;"" width=""100%""   data-theme=""b""  height=""600px""></iframe> "
            End If

            If Request.Item("TIPO") = "TABDESC" Then
                lbl_container.Text = "<iframe id=""output"" src=""TabelleDescrittive.aspx"" style=""border-width:0;"" width=""100%""   data-theme=""b""  height=""600px""></iframe> "
            End If

            If Request.Item("TIPO") = "PARAMETRI" Then
                lbl_container.Text = "<iframe id=""output"" src=""Parametri.aspx"" style=""border-width:0;"" width=""100%""   data-theme=""b""  height=""600px""></iframe> "
            End If

            If Request.Item("TIPO") = "VerificaRette" Then
                lbl_container.Text = "<iframe id=""output"" src=""VerificaRette.aspx"" style=""border-width:0;"" width=""100%""   data-theme=""b""  height=""600px""></iframe> "
            End If
            If Request.Item("TIPO") = "VSMOVEN" Then
                lbl_container.Text = "<iframe id=""output"" src=""VisualizzaMovimenti.aspx"" style=""border-width:0;"" width=""100%""   data-theme=""b""  height=""600px""></iframe> "
            End If
            If Request.Item("TIPO") = "VSADDACC" Then
                lbl_container.Text = "<iframe id=""output"" src=""VisualizzazioneAddebitoAccredito.aspx"" style=""border-width:0;"" width=""100%""   data-theme=""b""  height=""600px""></iframe> "
            End If
            If Request.Item("TIPO") = "VSESTRATTOCONTODENARO" Then
                lbl_container.Text = "<iframe id=""output"" src=""EstrattoContoDenaro.aspx"" style=""border-width:0;"" width=""100%""   data-theme=""b""  height=""600px""></iframe> "
        End If


        If Request.Item("TIPO") = "EliminaFatturazione" Then
            lbl_container.Text = "<iframe id=""output"" src=""EliminaFatturazione.aspx"" style=""border-width:0;"" width=""100%""   data-theme=""b""  height=""600px""></iframe> "
        End If

        If Request.Item("TIPO") = "ControllaPianoConti" Then
            lbl_container.Text = "<iframe id=""output"" src=""ControllaPianoConti.aspx"" style=""border-width:0;"" width=""100%""   data-theme=""b""  height=""600px""></iframe> "
        End If

        If Request.Item("TIPO") = "TPOPERAZIONE" Then
            lbl_container.Text = "<iframe id=""output"" src=""TipoOperazione.aspx"" style=""border-width:0;"" width=""100%""   data-theme=""b""  height=""600px""></iframe> "
        End If


        If Request.Item("TIPO") = "GESMEDICI" Then
            lbl_container.Text = "<iframe id=""output"" src=""GestioneMedici.aspx"" style=""border-width:0;"" width=""100%""   data-theme=""b""  height=""600px""></iframe> "
        End If
        If Request.Item("TIPO") = "OSPITIPRESENTI" Then
            lbl_container.Text = "<iframe id=""output"" src=""ProspettoOspitiPresenti.aspx"" style=""border-width:0;"" width=""100%""   data-theme=""b""  height=""600px""></iframe> "
        End If

        If Request.Item("TIPO") = "Denaro" And Session("ABILITAZIONI").ToString.IndexOf("<DENARO>") >= 0 Then
            If Val(Session("CODICEOSPITE")) > 0 Then
                lbl_container.Text = "<iframe id=""output"" src=""GestioneDenaro.aspx"" style=""border-width:0;"" width=""100%""   data-theme=""b""  height=""600px""></iframe> "
            Else
                ClientScript.RegisterClientScriptBlock(Me.GetType(), "Errore", "VisualizzaErrore('<center>Devi prima richiamare un ospite</center>');", True)
            End If
        End If

        If Request.Item("TIPO") = "DocumentiOspitiParenti" Then
            If Val(Session("CODICEOSPITE")) > 0 Then
                lbl_container.Text = "<iframe id=""output"" src=""DocumentiOspiteParente.aspx"" style=""border-width:0;"" width=""100%""   data-theme=""b""  height=""600px""></iframe> "
            Else
                ClientScript.RegisterClientScriptBlock(Me.GetType(), "Errore", "VisualizzaErrore('<center>Devi prima richiamare un ospite</center>');", True)
            End If
        End If

        If Request.Item("TIPO") = "FatNc" And Session("ABILITAZIONI").ToString.IndexOf("<INCASSO>") >= 0 Then
            If Val(Session("CODICEOSPITE")) > 0 Then
                lbl_container.Text = "<iframe id=""output"" src=""FatturaNC.aspx"" style=""border-width:0;"" width=""100%""   data-theme=""b""  height=""600px""></iframe> "
            Else
                ClientScript.RegisterClientScriptBlock(Me.GetType(), "Errore", "VisualizzaErrore('<center>Devi prima richiamare un ospite</center>');", True)
            End If
        End If


        If Request.Item("TIPO") = "IncAnt" And Session("ABILITAZIONI").ToString.IndexOf("<INCASSO>") >= 0 Then
            If Val(Session("CODICEOSPITE")) > 0 Then
                lbl_container.Text = "<iframe id=""output"" src=""IncassiAnticipi.aspx"" style=""border-width:0;"" width=""100%""   data-theme=""b""  height=""600px""></iframe> "
            Else
                ClientScript.RegisterClientScriptBlock(Me.GetType(), "Errore", "VisualizzaErrore('<center>Devi prima richiamare un ospite</center>');", True)
            End If
        End If

        If Request.Item("TIPO") = "Ricalcolo" And Session("ABILITAZIONI").ToString.IndexOf("<ANAGRAFICA>") >= 0 Then
            lbl_container.Text = "<iframe id=""output"" src=""ricalcolo.aspx"" style=""border-width:0;"" width=""100%""   data-theme=""b""  height=""600px""></iframe> "
        End If

        If Request.Item("TIPO") = "Movimenti" And Session("ABILITAZIONI").ToString.IndexOf("<MOVIMENTI>") >= 0 Then
            If Val(Session("CODICEOSPITE")) > 0 Then
                lbl_container.Text = "<iframe id=""output"" src=""movimenti.aspx"" style=""border-width:0;"" width=""100%""   data-theme=""b""  height=""600px""></iframe> "
            Else
                ClientScript.RegisterClientScriptBlock(Me.GetType(), "Errore", "VisualizzaErrore('<center>Devi prima richiamare un ospite</center>');", True)
            End If
        End If
        If Request.Item("TIPO") = "Movimenti" And Val(Session("CODICEOSPITE")) > 0 Then
            Label1.Text = Label1.Text & "<a href=""centrale.aspx?TIPO=Movimenti"" data-role=""button"" data-theme=""b"" rel=""external"">Movimenti</a>  "
        Else
            Label1.Text = Label1.Text & "<a href=""centrale.aspx?TIPO=Movimenti"" data-role=""button""  rel=""external"">Movimenti</a>  "
        End If

        If Request.Item("TIPO") = "Diurno" And Session("ABILITAZIONI").ToString.IndexOf("<MOVIMENTI>") >= 0 Then
            If Val(Session("CODICEOSPITE")) > 0 Then
                lbl_container.Text = "<iframe id=""output"" src=""Diurno.aspx"" style=""border-width:0;"" width=""100%""   data-theme=""b""  height=""600px""></iframe> "
            Else
                ClientScript.RegisterClientScriptBlock(Me.GetType(), "Errore", "VisualizzaErrore('<center>Devi prima richiamare un ospite</center>');", True)
            End If
        End If

        If Request.Item("TIPO") = "Diurno" And Val(Session("CODICEOSPITE")) > 0 Then
            Label1.Text = Label1.Text & "<a href=""centrale.aspx?TIPO=Diurno"" data-role=""button"" data-theme=""b""  rel=""external"">Diurno</a>  "
        Else
            Label1.Text = Label1.Text & "<a href=""centrale.aspx?TIPO=Diurno"" data-role=""button""  rel=""external"">Diurno</a>  "
        End If

        If Request.Item("TIPO") = "AddAcc" And Session("ABILITAZIONI").ToString.IndexOf("<ADDEBITI>") >= 0 Then
            If Val(Session("CODICEOSPITE")) > 0 Then
                lbl_container.Text = "<iframe id=""output"" src=""AddebitoAccredito.aspx"" style=""border-width:0;"" width=""100%""   data-theme=""b""  height=""600px""></iframe> "
            Else
                ClientScript.RegisterClientScriptBlock(Me.GetType(), "Errore", "VisualizzaErrore('<center>Devi prima richiamare un ospite</center>');", True)
            End If
        End If


        If Request.Item("TIPO") = "AddAcc" And Val(Session("CODICEOSPITE")) > 0 Then
            Label1.Text = Label1.Text & "<a href=""centrale.aspx?TIPO=AddAcc"" data-role=""button"" data-theme=""b"" rel=""external"">Addebi Accrediti</a> "
        Else
            Label1.Text = Label1.Text & "<a href=""centrale.aspx?TIPO=AddAcc"" data-role=""button"" rel=""external"">Addebi Accrediti</a> "
        End If




        If Request.Item("TIPO") = "Calcolo" Then
            Label1.Text = Label1.Text & "<a href=""centrale.aspx?TIPO=Calcolo"" data-role=""button"" data-theme=""b""  rel=""external"">Calcolo</a> "
        Else
            Label1.Text = Label1.Text & "<a href=""centrale.aspx?TIPO=Calcolo"" data-role=""button"" rel=""external"">Calcolo</a> "
        End If


        If Request.Item("TIPO") = "Quote" Then
            Label1.Text = Label1.Text & "<a href=""centrale.aspx?TIPO=Quote"" data-role=""button"" data-theme=""b""  rel=""external"">Quote</a> "
        Else
            Label1.Text = Label1.Text & "<a href=""centrale.aspx?TIPO=Quote"" data-role=""button"" rel=""external"">Quote</a> "
        End If

        If Request.Item("TIPO") = "Ricalcolo" Then
            Label1.Text = Label1.Text & "<a href=""centrale.aspx?TIPO=Ricalcolo"" data-role=""button"" data-theme=""b"" rel=""external"">Ricalcolo</a> "
        Else
            Label1.Text = Label1.Text & "<a href=""centrale.aspx?TIPO=Ricalcolo"" data-role=""button"" rel=""external"">Ricalcolo</a> "
        End If


        If Request.Item("TIPO") = "Emissione" Then
            Label1.Text = Label1.Text & "<a href=""centrale.aspx?TIPO=Emissione"" data-role=""button"" data-theme=""b"" rel=""external"">Emissione</a> "
        Else
            Label1.Text = Label1.Text & "<a href=""centrale.aspx?TIPO=Emissione"" data-role=""button"" rel=""external"">Emissione</a> "
        End If


        If Request.Item("TIPO") = "FatNc" Then
            Label1.Text = Label1.Text & "<a href=""centrale.aspx?TIPO=FatNc"" data-role=""button"" data-theme=""b"" rel=""external"">Fattura NC</a> "
        Else
            Label1.Text = Label1.Text & "<a href=""centrale.aspx?TIPO=FatNc"" data-role=""button"" rel=""external"">Fattura NC</a> "
        End If

        If Request.Item("TIPO") = "IncAnt" Then
            Label1.Text = Label1.Text & "<a href=""centrale.aspx?TIPO=IncAnt"" data-role=""button"" data-theme=""b"" rel=""external"">Incasso Anticipi</a> "
        Else
            Label1.Text = Label1.Text & "<a href=""centrale.aspx?TIPO=IncAnt"" data-role=""button"" rel=""external"">Incasso Anticipi</a> "
        End If


        If Request.Item("TIPO") = "StampaDoc" Then
            Label1.Text = Label1.Text & "<a href=""centrale.aspx?TIPO=StampaDoc"" data-role=""button"" data-theme=""b"" rel=""external"">Stampa Documenti</a> "
            lbl_container.Text = "<iframe id=""output"" src=""StampaDocumenti.aspx"" style=""border-width:0;"" width=""100%""   data-theme=""b""  height=""600px""></iframe> "
        Else
            Label1.Text = Label1.Text & "<a href=""centrale.aspx?TIPO=StampaDoc"" data-role=""button"" rel=""external"">Stampa Documenti</a> "
        End If

        If Request.Item("TIPO") = "StampaFatDet" Then
            Label1.Text = Label1.Text & "<a href=""centrale.aspx?TIPO=StampaFatDet"" data-role=""button"" data-theme=""b"" rel=""external"">Fatture Dettagliate</a> "
            lbl_container.Text = "<iframe id=""output"" src=""StampaFattureDettagliate.aspx"" style=""border-width:0;"" width=""100%""   data-theme=""b""  height=""600px""></iframe> "
        Else
            Label1.Text = Label1.Text & "<a href=""centrale.aspx?TIPO=StampaFatDet"" data-role=""button"" rel=""external"">Fatture Dettagliate</a> "
        End If

        If Request.Item("TIPO") = "Denaro" Then
            Label1.Text = Label1.Text & "<a href=""centrale.aspx?TIPO=Denaro"" data-role=""button"" data-theme=""b"" rel=""external"">Gestione Denaro</a> "
        Else
            Label1.Text = Label1.Text & "<a href=""centrale.aspx?TIPO=Denaro"" data-role=""button"" rel=""external"">Gestione Denaro</a> "
        End If



        If Request.Item("TIPO") = "FatNc" And Session("ABILITAZIONI").ToString.IndexOf("<INCASSO>") >= 0 Then
            If Val(Session("CODICEOSPITE")) > 0 Then
                lbl_container.Text = "<iframe id=""output"" src=""FatturaNC.aspx"" style=""border-width:0;"" width=""100%""   data-theme=""b""  height=""600px""></iframe> "
            Else
                ClientScript.RegisterClientScriptBlock(Me.GetType(), "Errore", "VisualizzaErrore('<center>Devi prima richiamare un ospite</center>');", True)
            End If
        End If
        REM Label1.Text = Label1.Text & "<a href=""centrale.aspx?TIPO=Statistiche"" data-role=""button"" rel=""external"">Statistiche</a> "

        If Request.Item("TIPO") = "Statistiche" Then
            Label1.Text = Label1.Text & " <a href=""centrale.aspx"" data-icon=""arrow-d"" iconpos=""right"" data-role=""button"" data-theme=""a"" rel=""external"">Statistiche</a>  "
        Else
            Label1.Text = Label1.Text & " <a href=""centrale.aspx?Tipo=Statistiche"" data-icon=""arrow-d"" iconpos=""right"" data-role=""button""   rel=""external"">Statistiche</a>  "
        End If
        If Request.Item("TIPO") = "Statistiche" Or Request.Item("TIPO") = "Prospetto" Or _
           Request.Item("TIPO") = "VerificaRette" Or Request.Item("TIPO") = "VSMOVEN" Or _
           Request.Item("TIPO") = "DocumentiOspitiParenti" Or Request.Item("TIPO") = "VSADDACC" Or _
           Request.Item("TIPO") = "VSESTRATTOCONTODENARO" Or Request.Item("TIPO") = "OSPITIPRESENTI" Then
            If Request.Item("TIPO") = "OSPITIPRESENTI" Then
                Label1.Text = Label1.Text & "<a href=""centrale.aspx?TIPO=OSPITIPRESENTI"" data-role=""button"" data-icon=""arrow-r"" iconpos=""right"" data-theme=""b""  rel=""external"">Ospiti Presenti</a>  "
            Else
                Label1.Text = Label1.Text & "<a href=""centrale.aspx?TIPO=OSPITIPRESENTI"" data-role=""button"" data-icon=""arrow-r"" iconpos=""right"" rel=""external"">Ospiti Presenti</a>  "
            End If
            If Request.Item("TIPO") = "Prospetto" Then
                Label1.Text = Label1.Text & "<a href=""centrale.aspx?TIPO=Prospetto"" data-role=""button"" data-icon=""arrow-r"" iconpos=""right"" data-theme=""b""  rel=""external"">Prospetto Presenze</a>  "
            Else
                Label1.Text = Label1.Text & "<a href=""centrale.aspx?TIPO=Prospetto"" data-role=""button"" data-icon=""arrow-r"" iconpos=""right"" rel=""external"">Prospetto Presenze</a>  "
            End If
            If Request.Item("TIPO") = "VerificaRette" Then
                Label1.Text = Label1.Text & "<a href=""centrale.aspx?TIPO=VerificaRette"" data-role=""button"" data-icon=""arrow-r"" iconpos=""right"" data-theme=""b""  rel=""external"">Verifica Rette</a>  "
            Else
                Label1.Text = Label1.Text & "<a href=""centrale.aspx?TIPO=VerificaRette"" data-role=""button"" data-icon=""arrow-r"" iconpos=""right"" rel=""external"">Verifica Rette</a>  "
            End If
            If Request.Item("TIPO") = "DocumentiOspitiParenti" Then
                Label1.Text = Label1.Text & "<a href=""centrale.aspx?TIPO=DocumentiOspitiParenti"" data-role=""button"" data-icon=""arrow-r"" iconpos=""right"" data-theme=""b""  rel=""external"">Documenti Ospiti/Parenti</a>  "
            Else
                Label1.Text = Label1.Text & "<a href=""centrale.aspx?TIPO=DocumentiOspitiParenti"" data-role=""button"" data-icon=""arrow-r"" iconpos=""right"" rel=""external"">Documenti Ospiti/Parenti</a>  "
            End If
            If Request.Item("TIPO") = "VSMOVEN" Then
                Label1.Text = Label1.Text & "<a href=""centrale.aspx?TIPO=VSMOVEN"" data-role=""button"" data-icon=""arrow-r"" iconpos=""right"" data-theme=""b""  rel=""external"">Visualizza Movimenti</a>  "
            Else
                Label1.Text = Label1.Text & "<a href=""centrale.aspx?TIPO=VSMOVEN"" data-role=""button"" data-icon=""arrow-r"" iconpos=""right"" rel=""external"">Visualizza Movimenti</a>  "
            End If

            If Request.Item("TIPO") = "VSADDACC" Then
                Label1.Text = Label1.Text & "<a href=""centrale.aspx?TIPO=VSADDACC"" data-role=""button"" data-icon=""arrow-r"" iconpos=""right"" data-theme=""b""  rel=""external"">Visualizza Add/Acc</a>  "
            Else
                Label1.Text = Label1.Text & "<a href=""centrale.aspx?TIPO=VSADDACC"" data-role=""button"" data-icon=""arrow-r"" iconpos=""right"" rel=""external"">Visualizza Add/Acc</a>  "
            End If

            If Request.Item("TIPO") = "VSESTRATTOCONTODENARO" Then
                Label1.Text = Label1.Text & "<a href=""centrale.aspx?TIPO=VSESTRATTOCONTODENARO"" data-role=""button"" data-icon=""arrow-r"" iconpos=""right"" data-theme=""b""  rel=""external"">Estratto Conto Denaro</a>  "
            Else
                Label1.Text = Label1.Text & "<a href=""centrale.aspx?TIPO=VSESTRATTOCONTODENARO"" data-role=""button"" data-icon=""arrow-r"" iconpos=""right"" rel=""external"">Estratto Conto Denaro</a>  "
            End If
 

        End If

        REM Label1.Text = Label1.Text & "<a href=""centrale.aspx?TIPO=Tabelle"" data-role=""button"" rel=""external"">Tabelle</a> "

        If Request.Item("TIPO") = "Tabelle" Then
            Label1.Text = Label1.Text & " <a href=""centrale.aspx"" data-icon=""arrow-d"" iconpos=""right"" data-role=""button"" data-theme=""a"" rel=""external"">Tabelle</a>  "
        Else
            Label1.Text = Label1.Text & " <a href=""centrale.aspx?Tipo=Tabelle"" data-icon=""arrow-d"" iconpos=""right"" data-role=""button""   rel=""external"">Tabelle</a>  "
        End If

        If Session("ABILITAZIONI").ToString.IndexOf("<TABELLA>") >= 0 And Request.Item("TIPO") = "Tabelle" Or Request.Item("TIPO") = "Regioni" Or Request.Item("TIPO") = "TPOPERAZIONE" Or _
           Request.Item("TIPO") = "CentroServizio" Or Request.Item("TIPO") = "CausaliEntrataUscita" Or _
           Request.Item("TIPO") = "ProvinciaComuni" Or Request.Item("TIPO") = "TIPOAD" Or Request.Item("TIPO") = "PARAMETRI" Or Request.Item("TIPO") = "TABDESC" Or Request.Item("TIPO") = "GESMEDICI" Then
            If Request.Item("TIPO") = "CentroServizio" Then
                Label1.Text = Label1.Text & "<a href=""centrale.aspx?TIPO=CentroServizio"" data-role=""button"" data-icon=""arrow-r"" iconpos=""right"" data-theme=""b""  rel=""external"">Centro Servizio</a>  "
            Else
                Label1.Text = Label1.Text & "<a href=""centrale.aspx?TIPO=CentroServizio"" data-role=""button"" data-icon=""arrow-r"" iconpos=""right"" rel=""external"">Centro Servizio</a>  "
            End If
            If Request.Item("TIPO") = "CausaliEntrataUscita" Then
                Label1.Text = Label1.Text & "<a href=""centrale.aspx?TIPO=CausaliEntrataUscita"" data-role=""button"" data-icon=""arrow-r"" iconpos=""right"" data-theme=""b""  rel=""external"">Causali E/U</a>  "
            Else
                Label1.Text = Label1.Text & "<a href=""centrale.aspx?TIPO=CausaliEntrataUscita"" data-role=""button"" data-icon=""arrow-r"" iconpos=""right"" rel=""external"">Causali E/U</a>  "
            End If
            If Request.Item("TIPO") = "ProvinciaComuni" Then
                Label1.Text = Label1.Text & "<a href=""centrale.aspx?TIPO=ProvinciaComuni"" data-role=""button"" data-icon=""arrow-r"" iconpos=""right"" data-theme=""b""  rel=""external"">Comuni</a>  "
            Else
                Label1.Text = Label1.Text & "<a href=""centrale.aspx?TIPO=ProvinciaComuni"" data-role=""button"" data-icon=""arrow-r"" iconpos=""right"" rel=""external"">Comuni</a>  "
            End If
            If Request.Item("TIPO") = "Regioni" Then
                Label1.Text = Label1.Text & "<a href=""centrale.aspx?TIPO=Regioni"" data-role=""button"" data-icon=""arrow-r"" iconpos=""right"" data-theme=""b""  rel=""external"">Regioni</a>  "
            Else
                Label1.Text = Label1.Text & "<a href=""centrale.aspx?TIPO=Regioni"" data-role=""button"" data-icon=""arrow-r"" iconpos=""right"" rel=""external"">Regioni</a>  "
            End If
            If Request.Item("TIPO") = "TIPOAD" Then
                Label1.Text = Label1.Text & "<a href=""centrale.aspx?TIPO=TIPOAD"" data-role=""button"" data-icon=""arrow-r"" iconpos=""right"" data-theme=""b""  rel=""external"">Tipo Addebito</a>  "
            Else
                Label1.Text = Label1.Text & "<a href=""centrale.aspx?TIPO=TIPOAD"" data-role=""button"" data-icon=""arrow-r"" iconpos=""right"" rel=""external"">Tipo Addebito</a>  "
            End If
            If Request.Item("TIPO") = "TABDESC" Then
                Label1.Text = Label1.Text & "<a href=""centrale.aspx?TIPO=TABDESC"" data-role=""button"" data-icon=""arrow-r"" iconpos=""right"" data-theme=""b""  rel=""external"">Tabelle descrittive</a>  "
            Else
                Label1.Text = Label1.Text & "<a href=""centrale.aspx?TIPO=TABDESC"" data-role=""button"" data-icon=""arrow-r"" iconpos=""right"" rel=""external"">Tabelle descrittive</a>  "
            End If
            If Request.Item("TIPO") = "PARAMETRI" Then
                Label1.Text = Label1.Text & "<a href=""centrale.aspx?TIPO=PARAMETRI"" data-role=""button"" data-icon=""arrow-r"" iconpos=""right"" data-theme=""b""  rel=""external"">Parametri</a>  "
            Else
                Label1.Text = Label1.Text & "<a href=""centrale.aspx?TIPO=PARAMETRI"" data-role=""button"" data-icon=""arrow-r"" iconpos=""right"" rel=""external"">Parametri</a>  "
            End If
            If Request.Item("TIPO") = "GESMEDICI" Then
                Label1.Text = Label1.Text & "<a href=""centrale.aspx?TIPO=GESMEDICI"" data-role=""button"" data-icon=""arrow-r"" iconpos=""right"" data-theme=""b""  rel=""external"">Gestione Medici</a>  "
            Else
                Label1.Text = Label1.Text & "<a href=""centrale.aspx?TIPO=GESMEDICI"" data-role=""button"" data-icon=""arrow-r"" iconpos=""right"" rel=""external"">Gestione Medici</a>  "
            End If
            If Request.Item("TIPO") = "TPOPERAZIONE" Then
                Label1.Text = Label1.Text & "<a href=""centrale.aspx?TIPO=TPOPERAZIONE"" data-role=""button"" data-icon=""arrow-r"" iconpos=""right"" data-theme=""b""  rel=""external"">Tipo Operazione</a>  "
            Else
                Label1.Text = Label1.Text & "<a href=""centrale.aspx?TIPO=TPOPERAZIONE"" data-role=""button"" data-icon=""arrow-r"" iconpos=""right"" rel=""external"">Tipo Operazione</a>  "
            End If
        End If

        If Request.Item("TIPO") = "Servizi" Then
            Label1.Text = Label1.Text & " <a href=""centrale.aspx"" data-icon=""arrow-d"" iconpos=""right"" data-role=""button"" data-theme=""a"" rel=""external"">Servizi</a>  "
        Else
            Label1.Text = Label1.Text & " <a href=""centrale.aspx?Tipo=Servizi"" data-icon=""arrow-d"" iconpos=""right"" data-role=""button""   rel=""external"">Servizi</a>  "
        End If

        If Session("ABILITAZIONI").ToString.IndexOf("<SERVIZI>") >= 0 And Request.Item("TIPO") = "Servizi" Or Request.Item("TIPO") = "EliminaFatturazione" Or Request.Item("TIPO") = "ControllaPianoConti" Then
            If Request.Item("TIPO") = "EliminaFatturazione" Then
                Label1.Text = Label1.Text & "<a href=""centrale.aspx?TIPO=EliminaFatturazione"" data-role=""button"" data-icon=""arrow-r"" iconpos=""right"" data-theme=""b""  rel=""external"">Elimina Fatturazione</a>  "
            Else
                Label1.Text = Label1.Text & "<a href=""centrale.aspx?TIPO=EliminaFatturazione"" data-role=""button"" data-icon=""arrow-r"" iconpos=""right"" rel=""external"">Elimina Fatturazione</a>  "
            End If
            If Request.Item("TIPO") = "ControllaPianoConti" Then
                Label1.Text = Label1.Text & "<a href=""centrale.aspx?TIPO=ControllaPianoConti"" data-role=""button"" data-icon=""arrow-r"" iconpos=""right"" data-theme=""b""  rel=""external"">Controlla Piano Dei Conti</a>  "
            Else
                Label1.Text = Label1.Text & "<a href=""centrale.aspx?TIPO=ControllaPianoConti"" data-role=""button"" data-icon=""arrow-r"" iconpos=""right"" rel=""external"">Controlla Piano Dei Conti</a>  "
            End If
        End If
        Label1.Text = Label1.Text & "<a href=""centrale.aspx?TIPO=Menu"" data-role=""button"" rel=""external"">Menù</a> "
        If Request.Item("TIPO") = "Menu" Then
            System.Web.HttpContext.Current.Response.Redirect("..\MainMenu.aspx")
        End If
    End Sub



End Class

