﻿<%@ Page Language="VB" AutoEventWireup="false" Inherits="OspitiWeb_DatiPagamento" CodeFile="DatiPagamento.aspx.vb" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="xasp" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">


<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
    <meta http-equiv="x-ua-compatible" content="IE=9" />
    <title>Dati Pagamento</title>
    <asp:PlaceHolder runat="server">
        <%: System.Web.Optimization.Styles.Render("~/Content/AjaxControlToolkit/Styles/Bundle") %>
    </asp:PlaceHolder>
    <link href="ospiti.css?ver=11" rel="stylesheet" type="text/css" />
    <link rel="shortcut icon" href="images/SENIOR.ico" />
    <link href="js/jquery.autocomplete.css" rel="stylesheet" type="text/css" />


    <script src="js/jquery-1.5.1.min.js" type="text/javascript"></script>
    <script src="js/jquery.autocomplete.js" type="text/javascript"></script>
    <script src="js/soapclient.js" type="text/javascript"></script>
    <script src="/js/convertinumero.js" type="text/javascript"></script>



    <script src="/js/pianoconti.js" type="text/javascript"></script>
    <script src="js/jquery.maskedinput-1.3.min.js" type="text/javascript"></script>
    <script src="/js/formatnumer.js" type="text/javascript"></script>
    <script src="js/JSErrore.js" type="text/javascript"></script>
    <script type="text/javascript" src="../js/menuanagraficaospiti.js?ver=7"></script>

    <link rel="stylesheet" href="dialogbox/redips-dialog.css" type="text/css" media="screen" />
    <script type="text/javascript" src="dialogbox/redips-dialog-min.js"></script>
    <script type="text/javascript" src="dialogbox/script.js"></script>

    <link rel="stylesheet" href="jqueryui/jquery-ui.css" type="text/css">
    <script src="jqueryui/jquery-ui.js" type="text/javascript"></script>
    <script src="jqueryui/datapicker-it.js" type="text/javascript"></script>

    <script type="text/javascript">         
        $(document).ready(function () {
            $('html').keyup(function (event) {

                if (event.keyCode == 113) {
                    __doPostBack("Imb_Modifica", "0");
                }


            });
        });

        function DialogBox(Path) {

            REDIPS.dialog.show(500, 300, '<iframe id="output" src="' + Path + '" height="300px" width="500"></iframe>');
            return false;

        }
        $(document).ready(function () {
            $('html').keyup(function (event) {

                if (event.keyCode == 113) {
                    __doPostBack("Btn_Modifica", "0");
                }


            });
        });

        $(document).ready(function () {
            if (window.innerHeight > 0) { $("#BarraLaterale").css("height", (window.innerHeight - 94) + "px"); } else { $("#BarraLaterale").css("height", (document.documentElement.offsetHeight - 94) + "px"); }
        });
    </script>
</head>
<body>
    <form id="form1" runat="server">
        <asp:ScriptManager ID="ScriptManager1" runat="server" EnableScriptGlobalization="true">
            <Scripts>
                <asp:ScriptReference Path="~/Scripts/AjaxControlToolkit/Bundle" />
            </Scripts>
        </asp:ScriptManager>
        <asp:Label ID="Lbl_BarraSenior" runat="server" Text=""></asp:Label>
        <div style="text-align: left;">
            <table style="width: 100%;" cellpadding="0" cellspacing="0">
                <tr>
                    <td style="width: 160px; background-color: #F0F0F0;"></td>
                    <td>
                        <div class="Titolo">Ospiti - Dati Pagamento</div>
                        <div class="SottoTitoloOSPITE">
                            <asp:Label ID="Lbl_NomeOspite" runat="server"></asp:Label><br />
                            <br />
                        </div>
                    </td>
                    <td style="text-align: right;">
                        <div class="DivTastiOspite">
                            <asp:ImageButton runat="server" ImageUrl="~/images/salva.jpg" class="EffettoBottoniTondi" Height="38px" ToolTip="Modifica / Inserisci" ID="Imb_Modifica"></asp:ImageButton>
                            <asp:ImageButton runat="server" OnClientClick="return window.confirm('Eliminare?');" ImageUrl="~/images/elimina.jpg" class="EffettoBottoniTondi" Height="38px" ToolTip="Elimina" ID="ImageButton2"></asp:ImageButton>
                        </div>
                    </td>
                </tr>

                <tr>
                    <td style="width: 160px; background-color: #F0F0F0; vertical-align: top; text-align: center;" id="BarraLaterale">
                        <a href="Menu_Ospiti.aspx" style="border-width: 0px;">
                            <img src="images/Home.jpg" alt="Menù" class="Effetto" /></a><br />
                        <asp:ImageButton ID="Btn_Esci" runat="server" BackColor="Transparent" ImageUrl="../images/Menu_Indietro.png" class="Effetto" ToolTip="Chiudi" />
                        <br />
                        <div id="MENUDIV"></div>
                        <br />
                        <br />
                        <br />
                        <br />
                        <br />
                        <br />
                        <br />
                        <br />
                    </td>
                    <td colspan="2" style="background-color: #FFFFFF;" valign="top">
                        <xasp:TabContainer ID="TabContainer1" runat="server" ActiveTabIndex="0" CssClass="TabSenior" Width="100%" BorderStyle="None" Style="margin-right: 39px">
                            <xasp:TabPanel runat="server" HeaderText="Prima Nota" ID="Tab_Anagrafica">
                                <HeaderTemplate>
                                    Dati Pagamento      
                                </HeaderTemplate>
                                <ContentTemplate>
                                    <br />

                                    <label class="LabelCampo">Data :</label>
                                    <asp:TextBox ID="Txt_Data" runat="server" Width="90px" MaxLength="30"></asp:TextBox><br />
                                    <br />

                                    <label class="LabelCampo">Parente :</label>
                                    <asp:DropDownList ID="DD_Parente" AutoPostBack="true" runat="server" Width="264px"></asp:DropDownList>
                                    (Indicare il parente solo se la fattura è intestata al parente)<br />
                                    <br />

                                    <label class="LabelCampo">Intestatario Conto:</label>
                                    <asp:TextBox ID="Txt_IntestarioCC" runat="server" Width="320px" MaxLength="30"></asp:TextBox><br />
                                    <br />
                                    <label class="LabelCampo">Intestatario CF Conto:</label>
                                    <asp:TextBox ID="Txt_CodiceFiscaleCC" runat="server" Width="320px" MaxLength="16"></asp:TextBox><br />
                                    <br />
                                    <label class="LabelCampo">IBAN: Int</label>
                                    <asp:TextBox ID="Txt_Int" MaxLength="2" runat="server" Width="24px"></asp:TextBox>
                                    Num Cont
         <asp:TextBox ID="Txt_NumCont" MaxLength="2" onkeypress="return soloNumeri(event);" runat="server" Width="32px"></asp:TextBox>
                                    Cin
         <asp:TextBox ID="Txt_Cin" MaxLength="1" runat="server" Width="40px"></asp:TextBox>
                                    Abi
         <asp:TextBox ID="Txt_Abi" MaxLength="5" onkeypress="return soloNumeri(event);" runat="server" Width="58px"></asp:TextBox>
                                    Cab
         <asp:TextBox ID="Txt_Cab" MaxLength="5" onkeypress="return soloNumeri(event);" runat="server" Width="58px"></asp:TextBox>
                                    C/C Bancario&nbsp;
         <asp:TextBox ID="Txt_Ccbancario" MaxLength="12" runat="server" Width="328px"></asp:TextBox><br />
                                    <br />

                                    <label class="LabelCampo">Banca Cliente:</label>
                                    <asp:TextBox ID="Txt_BancaCliente" runat="server" Width="320px" MaxLength="30"></asp:TextBox><br />
                                    <br />

                                    <label class="LabelCampo">Data Mandato Debitore:</label>
                                    <asp:TextBox ID="Txt_DataMandato" runat="server" Width="90px"></asp:TextBox><br />
                                    <br />

                                    <label class="LabelCampo">Id Mandato Debitore :</label>
                                    <asp:TextBox ID="Txt_IdMandato" runat="server" Width="320px" MaxLength="30"></asp:TextBox><br />
                                    <br />

                                    <label class="LabelCampo">Banca Ricevente :</label>
                                    <asp:DropDownList ID="DD_Banca" runat="server" Width="264px"></asp:DropDownList><br />
                                    <br />


                                    <label class="LabelCampo">First :</label>
                                    <asp:CheckBox ID="Chk_First" runat="server" Text="" /><br />
                                    <br />

                                </ContentTemplate>
                            </xasp:TabPanel>
                        </xasp:TabContainer>
                    </td>
                </tr>
            </table>
    </form>
</body>
</html>
