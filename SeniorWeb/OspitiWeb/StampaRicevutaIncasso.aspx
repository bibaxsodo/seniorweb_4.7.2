﻿<%@ Page Language="VB" AutoEventWireup="false" Inherits="OspitiWeb_StampaRicevutaIncasso" CodeFile="StampaRicevutaIncasso.aspx.vb" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>SeniorWeb</title>
    <asp:PlaceHolder runat="server">
        <%: System.Web.Optimization.Styles.Render("~/Content/AjaxControlToolkit/Styles/Bundle") %>
    </asp:PlaceHolder>

    <script type="text/javascript">
        function openPopUp(urlToOpen, nome, parametri) {
            window.location.href = urlToOpen;
        }
    </script>

</head>
<body>
    <form id="form1" runat="server">
        <asp:ScriptManager ID="ScriptManager1" runat="server" EnableScriptGlobalization="true">
            <Scripts>
                <asp:ScriptReference Path="~/Scripts/AjaxControlToolkit/Bundle" />
            </Scripts>
        </asp:ScriptManager>
        <div>
            <asp:Timer ID="Timer1" runat="server">
            </asp:Timer>
        </div>
    </form>
</body>
</html>
