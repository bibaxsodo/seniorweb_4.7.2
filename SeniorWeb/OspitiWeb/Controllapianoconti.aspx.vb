﻿
Partial Class OspitiWeb_Controllapianoconti
    Inherits System.Web.UI.Page


    Protected Sub ImageButton4_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles ImageButton4.Click
        Dim MyRs As New ADODB.Recordset
        Dim MyRsW As New ADODB.Recordset
        Dim RsCserv As New ADODB.Recordset
        Dim RsCodosp As New ADODB.Recordset
        Dim k As New Cls_FunzioniVB6

        Dim Mastro As Long
        Dim Conto As Long
        Dim SottoConto As Long
        Dim OspitiDb As New ADODB.Connection
        Dim GeneraleDB As New ADODB.Connection

        OspitiDb.Open(Session("DC_OSPITE"))
        GeneraleDB.Open(Session("DC_GENERALE"))

        MyRs.Open("SELECT MOVIMENTI.CENTROSERVIZIO, AnagraficaComune.CODICEOSPITE FROM AnagraficaComune INNER JOIN MOVIMENTI ON AnagraficaComune.CODICEOSPITE = MOVIMENTI.CODICEOSPITE WHERE (((AnagraficaComune.TIPOLOGIA)='O')) And MOVIMENTI.TIPOMOV = '05' GROUP BY MOVIMENTI.CENTROSERVIZIO, AnagraficaComune.CODICEOSPITE ", OspitiDb, ADODB.CursorTypeEnum.adOpenKeyset, ADODB.LockTypeEnum.adLockOptimistic)

        Do While Not MyRs.EOF

            RsCodosp.Open("Select * From AnagraficaComune Where CODICEOSPITE = " & k.MoveFromDbWC(MyRs, "CodiceOspite"), OspitiDb, ADODB.CursorTypeEnum.adOpenKeyset, ADODB.LockTypeEnum.adLockOptimistic)
            Do While Not RsCodosp.EOF

                RsCserv.Open("Select * From TABELLACENTROSERVIZIO Where CENTROSERVIZIO = '" & k.MoveFromDbWC(MyRs, "CentroServizio") & "'", OspitiDb, ADODB.CursorTypeEnum.adOpenKeyset, ADODB.LockTypeEnum.adLockOptimistic)
                Do Until RsCserv.EOF
                    Mastro = k.MoveFromDbWC(RsCserv, "Mastro")
                    Conto = k.MoveFromDbWC(RsCserv, "Conto")
                    SottoConto = (k.MoveFromDbWC(RsCodosp, "CodiceOspite") * 100) + k.MoveFromDbWC(RsCodosp, "CODICEPARENTE")

                    If Val(Mastro) <> 0 Then
                        MyRsW.Open("Select * From PianoConti Where Mastro = " & Mastro & " And Conto = " & Conto & " And SottoConto = " & SottoConto, GeneraleDB, ADODB.CursorTypeEnum.adOpenKeyset, ADODB.LockTypeEnum.adLockOptimistic)
                        If MyRsW.EOF Then
                            MyRsW.AddNew()
                        End If
                        k.MoveToDb(MyRsW.Fields("Mastro"), Mastro)
                        k.MoveToDb(MyRsW.Fields("Conto"), Conto)
                        k.MoveToDb(MyRsW.Fields("SottoConto"), SottoConto)
                        k.MoveToDb(MyRsW.Fields("Tipo"), "A")

                        k.MoveToDb(MyRsW.Fields("Descrizione"), k.MoveFromDbWC(RsCodosp, "Nome"))
                        If k.MoveFromDbWC(RsCodosp, "CODICEPARENTE") = 0 Then
                            k.MoveToDb(MyRsW.Fields("TipoAnagrafica"), "O")
                        Else
                            k.MoveToDb(MyRsW.Fields("TipoAnagrafica"), "P")
                        End If
                        k.MoveToDb(MyRsW.Fields("Utente"), "Controllo")
                        k.MoveToDb(MyRsW.Fields("DataAggiornamento"), Now)
                        MyRsW.Update()
                        MyRsW.Close()
                        If k.MoveFromDbWC(RsCodosp, "TipoOperazione") <> "" And Not IsDBNull(RsCodosp.Fields("TipoOperazione")) Then
                            Dim CampOpera As New Cls_TipoOperazione

                            CampOpera.Codice = k.MoveFromDbWC(RsCodosp, "TipoOperazione")
                            CampOpera.Leggi(Session("DC_OSPITE"), k.MoveFromDbWC(RsCodosp, "TipoOperazione"))

                            If CampOpera.Anticipo <> "" Then
                                Dim MarPar As New Cls_Parametri

                                MarPar.LeggiParametri(Session("DC_OSPITE"))

                                Mastro = MarPar.MastroAnticipo
                                Conto = k.MoveFromDbWC(RsCserv, "Conto")
                                SottoConto = (k.MoveFromDbWC(RsCodosp, "CodiceOspite") * 100) + k.MoveFromDbWC(RsCodosp, "CODICEPARENTE")
                                If Val(Mastro) <> 0 Then
                                    MyRsW.Open("Select * From PianoConti Where Mastro = " & Mastro & " And Conto = " & Conto & " And SottoConto = " & SottoConto, GeneraleDB, ADODB.CursorTypeEnum.adOpenKeyset, ADODB.LockTypeEnum.adLockOptimistic)
                                    If MyRsW.EOF Then
                                        MyRsW.AddNew()
                                    End If
                                    k.MoveToDb(MyRsW.Fields("Mastro"), Mastro)
                                    k.MoveToDb(MyRsW.Fields("Conto"), Conto)
                                    k.MoveToDb(MyRsW.Fields("SottoConto"), SottoConto)
                                    k.MoveToDb(MyRsW.Fields("Tipo"), "A")
                                    k.MoveToDb(MyRsW.Fields("Descrizione"), k.MoveFromDbWC(RsCodosp, "Nome"))
                                    If k.MoveFromDbWC(RsCodosp, "CODICEPARENTE") = 0 Then
                                        k.MoveToDb(MyRsW.Fields("TipoAnagrafica"), "O")
                                    Else
                                        k.MoveToDb(MyRsW.Fields("TipoAnagrafica"), "P")
                                    End If
                                    k.MoveToDb(MyRsW.Fields("Utente"), "Controllo")
                                    k.MoveToDb(MyRsW.Fields("DataAggiornamento"), Now)
                                    MyRsW.Update()
                                    MyRsW.Close()
                                End If
                            End If
                        End If
                    End If
                    RsCserv.MoveNext()
                Loop
                RsCserv.Close()
                k.MoveToDb(RsCodosp.Fields("MastroCliente"), 0)
                k.MoveToDb(RsCodosp.Fields("ContoCliente"), 0)
                k.MoveToDb(RsCodosp.Fields("SottoContoCliente"), 0)
                RsCodosp.Update()
                RsCodosp.MoveNext()
            Loop
            RsCodosp.Close()



            MyRs.MoveNext()
        Loop
        MyRs.Close()


        OspitiDb.Close()
        GeneraleDB.Close()

        ScriptManager.RegisterClientScriptBlock(Me, Me.GetType(), "ESEGUITO", "alert('Controllo effettuato  ');", True)

        lbl_testo.Text = "Controllo Effettuato"
    End Sub

    Protected Sub Btn_Esci_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles Btn_Esci.Click
        Response.Redirect("Menu_Strumenti.aspx")
    End Sub

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load

        If Page.IsPostBack = True Then
            Exit Sub
        End If
        Dim Barra As New Cls_BarraSenior

        Lbl_BarraSenior.Text = Barra.CodiceBarra(Application("SENIOR"), Session("UTENTE"), Page)
    End Sub
End Class
