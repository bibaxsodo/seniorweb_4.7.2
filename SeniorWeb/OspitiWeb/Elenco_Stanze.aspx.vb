﻿Imports Microsoft.VisualBasic
Imports System.Data.OleDb
Imports System.Web.Hosting

Partial Class OspitiWeb_Elenco_Stanze
    Inherits System.Web.UI.Page

    Dim Tabella As New System.Data.DataTable("tabella")
    Dim MyDataSet As New System.Data.DataSet()

    Function campodb(ByVal oggetto As Object) As String
        If IsDBNull(oggetto) Then
            Return ""
        Else
            Return oggetto
        End If
    End Function
    Function campodbn(ByVal oggetto As Object) As Double
        If IsDBNull(oggetto) Then
            Return 0
        Else
            Return oggetto
        End If
    End Function
    Function campodbd(ByVal oggetto As Object) As Date
        If IsDBNull(oggetto) Then
            Return Nothing
        Else
            Return oggetto
        End If
    End Function

    Private Sub CaricaStanze()
        Dim cn As OleDbConnection


        cn = New Data.OleDb.OleDbConnection(Session("DC_OSPITIACCESSORI"))

        cn.Open()
        Dim cmd As New OleDbCommand()
        cmd.CommandText = ("select * from Stanze Order by Villa,Reparto,Piano,Stanza,Letto")
        cmd.Connection = cn
        Dim sb As StringBuilder = New StringBuilder

        Tabella.Clear()
        Tabella.Columns.Clear()
        Tabella.Columns.Add("ID", GetType(String))
        Tabella.Columns.Add("Villa", GetType(String))
        Tabella.Columns.Add("Reparto", GetType(String))
        Tabella.Columns.Add("Piano", GetType(String))
        Tabella.Columns.Add("Stanza", GetType(String))
        Tabella.Columns.Add("Letto", GetType(String))


        Dim myPOSTreader As OleDbDataReader = cmd.ExecuteReader()
        Do While myPOSTreader.Read

            Dim myriga As System.Data.DataRow = Tabella.NewRow()

            Dim TOCVilla As New Cls_TabelleDescrittiveOspitiAccessori
            Dim TOCReparto As New Cls_TabelleDescrittiveOspitiAccessori

            TOCVilla.TipoTabella = "VIL"
            TOCVilla.CodiceTabella = campodb(myPOSTreader.Item("VILLA"))
            TOCVilla.Leggi(Session("DC_OSPITIACCESSORI"))

            TOCReparto.TipoTabella = "REP"
            TOCReparto.CodiceTabella = campodb(myPOSTreader.Item("REPARTO"))
            TOCReparto.Leggi(Session("DC_OSPITIACCESSORI"))

            myriga(0) = campodbn(myPOSTreader.Item("ID"))
            myriga(1) = TOCVilla.Descrizione
            myriga(2) = TOCReparto.Descrizione
            myriga(3) = campodb(myPOSTreader.Item("PIANO"))
            myriga(4) = campodb(myPOSTreader.Item("STANZA"))
            myriga(5) = campodb(myPOSTreader.Item("LETTO"))

            Tabella.Rows.Add(myriga)
        Loop
        myPOSTreader.Close()
        cn.Close()
        ViewState("Appoggio") = Tabella

        Grd_Denaro.AutoGenerateColumns = True
        Grd_Denaro.DataSource = Tabella
        'Grd_Denaro.Columns(1).Visible = False
        Grd_Denaro.DataBind()

    End Sub

    Protected Sub ImageButton1_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles ImageButton1.Click

    End Sub

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Page.IsPostBack = True Then
            Exit Sub
        End If



        If Val(Request.Item("CodiceOspite")) > 0 And Request.Item("CentroServizio") <> "" Then

            Session("CODICESERVIZIO") = Request.Item("CentroServizio")
            Session("CODICEOSPITE") = Val(Request.Item("CodiceOspite"))
        End If




        Dim K1 As New Cls_SqlString

        Dim Barra As New Cls_BarraSenior

        If Not IsNothing(Session("RicercaAnagraficaSQLString")) Then
            K1 = Session("RicercaAnagraficaSQLString")
        End If

        Lbl_BarraSenior.Text = Barra.CodiceBarra(Application("SENIOR"), Session("UTENTE"), Page, K1)

        Try
            Call CaricaStanze()
        Catch ex As Exception

        End Try

        
    End Sub

    Protected Sub Grd_Denaro_PageIndexChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewPageEventArgs) Handles Grd_Denaro.PageIndexChanging
        Tabella = ViewState("Appoggio")
        Grd_Denaro.PageIndex = e.NewPageIndex
        Grd_Denaro.DataSource = Tabella
        Grd_Denaro.DataBind()
    End Sub

    Protected Sub Grd_Denaro_RowCommand(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewCommandEventArgs) Handles Grd_Denaro.RowCommand

        If e.CommandName = "Seleziona" Then


            Dim d As Integer


            Tabella = ViewState("Appoggio")

            d = Val(e.CommandArgument)


            Dim Codice As String

            Codice = Tabella.Rows(d).Item(0).ToString


            Response.Redirect("GestioneStanza.aspx?CODICE=" & Codice)
        End If
    End Sub

    Protected Sub Grd_Denaro_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles Grd_Denaro.SelectedIndexChanged

    End Sub

    Protected Sub Btn_Nuovo_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles Btn_Nuovo.Click
        Response.Redirect("GestioneStanza.aspx?CODICE=0&Anno=0")
    End Sub

    Protected Sub Btn_Esci_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles Btn_Esci.Click
        Response.Redirect("Menu_Tabelle.aspx")
    End Sub



    Protected Sub Lnk_ToExcel_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles Lnk_ToExcel.Click
        Dim cn As New OleDbConnection


        Dim ConnectionString As String = Session("DC_OSPITE")

        cn = New Data.OleDb.OleDbConnection(Session("DC_OSPITIACCESSORI"))

        cn.Open()

        Dim cmd As New OleDbCommand




        cmd.CommandText = ("select * from Stanze Order by Villa,Reparto,Piano,Stanza,Letto")

        cmd.Connection = cn
        cmd.Connection = cn


        Tabella.Clear()
        Tabella.Columns.Clear()
        Tabella.Columns.Add("ID", GetType(String))
        Tabella.Columns.Add("Villa", GetType(String))
        Tabella.Columns.Add("Reparto", GetType(String))
        Tabella.Columns.Add("Piano", GetType(String))
        Tabella.Columns.Add("Stanza", GetType(String))
        Tabella.Columns.Add("Letto", GetType(String))
        Tabella.Columns.Add("Tipologia", GetType(String))

        Tabella.Columns.Add("NumeroTelefono", GetType(String))
        Tabella.Columns.Add("NumeroBiancheria", GetType(String))
        Tabella.Columns.Add("TipoArredo", GetType(String))


        Dim myPOSTreader As OleDbDataReader = cmd.ExecuteReader()
        Do While myPOSTreader.Read
            Dim myriga As System.Data.DataRow = Tabella.NewRow()



            Dim TOCVilla As New Cls_TabelleDescrittiveOspitiAccessori
            Dim TOCReparto As New Cls_TabelleDescrittiveOspitiAccessori

            TOCVilla.TipoTabella = "VIL"
            TOCVilla.CodiceTabella = campodb(myPOSTreader.Item("VILLA"))
            TOCVilla.Leggi(Session("DC_OSPITIACCESSORI"))

            TOCReparto.TipoTabella = "REP"
            TOCReparto.CodiceTabella = campodb(myPOSTreader.Item("REPARTO"))
            TOCReparto.Leggi(Session("DC_OSPITIACCESSORI"))

            myriga(0) = campodbn(myPOSTreader.Item("ID"))
            myriga(1) = TOCVilla.Descrizione
            myriga(2) = TOCReparto.Descrizione
            myriga(3) = campodb(myPOSTreader.Item("PIANO"))
            myriga(4) = campodb(myPOSTreader.Item("STANZA"))
            myriga(5) = campodb(myPOSTreader.Item("LETTO"))
            myriga(6) = campodb(myPOSTreader.Item("Tipologia"))

            myriga(7) = campodb(myPOSTreader.Item("NumeroTelefono"))
            myriga(8) = campodb(myPOSTreader.Item("NumeroBiancheria"))
            myriga(9) = campodb(myPOSTreader.Item("TipoArredo"))


            Tabella.Rows.Add(myriga)
        Loop

        myPOSTreader.Close()
        cn.Close()


        Session("GrigliaSoloStampa") = Tabella
        ScriptManager.RegisterClientScriptBlock(Me, Me.GetType(), "Stampa", "window.open('ExportExcel.aspx','Excel','width=800,height=600');", True)

    End Sub



End Class
