﻿Imports System.Web.Hosting
Imports System.Data.OleDb

Partial Class OspitiWeb_StatisticaDomiciliari
    Inherits System.Web.UI.Page
    Dim MyTable As New System.Data.DataTable("tabella")
    Dim MyDataSet As New System.Data.DataSet()

    Public Function GiorniMese(ByVal Mese As Byte, ByVal Anno As Integer) As Byte
        If Mese = 1 Or Mese = 3 Or Mese = 5 Or Mese = 7 Or Mese = 8 Or _
           Mese = 10 Or Mese = 12 Then
            GiorniMese = 31
        Else
            If Mese <> 2 Then
                GiorniMese = 30
            Else
                If Day(DateSerial(Anno, Mese, 29)) = 29 Then
                    GiorniMese = 29
                Else
                    GiorniMese = 28
                End If
            End If
        End If
    End Function

    Protected Sub OspitiWeb_StatisticaDomiciliari_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load

        If Trim(Session("UTENTE")) = "" Then
            Response.Redirect("../Login.aspx")
            Exit Sub
        End If


        Call EseguiJS()

        If Page.IsPostBack = True Then Exit Sub


        Dim K1 As New Cls_SqlString

        Dim Barra As New Cls_BarraSenior

        If Not IsNothing(Session("RicercaAnagraficaSQLString")) Then
            K1 = Session("RicercaAnagraficaSQLString")
        End If

        Lbl_BarraSenior.Text = Barra.CodiceBarra(Application("SENIOR"), Session("UTENTE"), Page, K1)



        Dim kVilla As New Cls_TabelleDescrittiveOspitiAccessori


        kVilla.UpDateDropBox(Session("DC_OSPITIACCESSORI"), "VIL", DD_Struttura)
        If DD_Struttura.Items.Count = 1 Then
            Call AggiornaCServ()
        End If


        Lbl_Utente.Text = Session("UTENTE")

        Dim f As New Cls_Parametri

        f.LeggiParametri(Session("DC_OSPITE"))



        Dim DataDal As Date = DateSerial(f.AnnoFatturazione, f.MeseFatturazione, 1)
        Dim DataAl As Date = DateSerial(f.AnnoFatturazione, f.MeseFatturazione, GiorniMese(f.MeseFatturazione, f.AnnoFatturazione))

        Txt_DataDal.Text = Format(DataDal, "dd/MM/yyyy")
        Txt_DataAl.Text = Format(DataAl, "dd/MM/yyyy")


        Lbl_Utente.Text = Session("UTENTE")

        Call EseguiJS()
    End Sub


    Private Sub AggiornaCServ()
        Dim kCsrv As New Cls_CentroServizio

        If DD_Struttura.SelectedValue = "" Then
            kCsrv.UpDateDropBox(Session("DC_OSPITE"), Cmb_CServ)
        Else
            kCsrv.UpDateDropBoxStruttura(Session("DC_OSPITE"), Cmb_CServ, DD_Struttura.SelectedValue)
        End If

    End Sub

    Function campodbn(ByVal oggetto As Object) As Double
        If IsDBNull(oggetto) Then
            Return 0
        Else
            Return oggetto
        End If
    End Function

    Function campodb(ByVal oggetto As Object) As String
        If IsDBNull(oggetto) Then
            Return ""
        Else
            Return oggetto
        End If
    End Function
    Private Sub ProspettoPresenze()
        Dim ConnectionString As String = Session("DC_OSPITE")
        Dim cn As OleDbConnection
        Dim MySql As String
        Dim Condizione As String

        cn = New Data.OleDb.OleDbConnection(ConnectionString)

        cn.Open()

        MyTable.Clear()
        MyTable.Columns.Clear()
        MyTable.Columns.Add("Codice Ospite", GetType(Long)) '0
        MyTable.Columns.Add("Nome", GetType(String)) '1
        MyTable.Columns.Add("Codice Fiscale", GetType(String)) ''2
        MyTable.Columns.Add("Data Accoglimento", GetType(String)) '3
        MyTable.Columns.Add("Data Uscita", GetType(String)) '4
        MyTable.Columns.Add("Data", GetType(String)) '5
        MyTable.Columns.Add("Ore(cent) 1 Operatore Accreditato 1° Ingresso", GetType(String)) '6
        MyTable.Columns.Add("Ore(cent) 2 Operatori Accreditato 1° Ingresso", GetType(String)) '7
        MyTable.Columns.Add("Ore(cent) 1 Operatore Accreditato 2° Ingresso", GetType(String)) '8
        MyTable.Columns.Add("Ore(cent) 2 Operatori Accreditato 2° Ingresso", GetType(String)) '9
        MyTable.Columns.Add("Ore(cent) 1 Operatore Privato", GetType(String)) '10
        MyTable.Columns.Add("Ore(cent) 2 Operatori Privato", GetType(String)) '11
        MyTable.Columns.Add("Ore(cent) Tutoring", GetType(String)) '12
        MyTable.Columns.Add("Ore(cent) Dimissione Protetta", GetType(String)) '13
        MyTable.Columns.Add("N.Pasto", GetType(String))
        MyTable.Columns.Add("N.Pasto+Cena", GetType(String))
        MyTable.Columns.Add("N.Accessi Acr", GetType(String))
        MyTable.Columns.Add("N.Accessi Prv", GetType(String))
        MyTable.Columns.Add("MOTIVO DIMISSIONE/USCITA", GetType(String))
        MyTable.Columns.Add("RETTA+ADDEBITO OSPITE", GetType(String))
        MyTable.Columns.Add("RETTA+ADDEBITO ENTE", GetType(String))
        MyTable.Columns.Add("TIPO RETTA", GetType(String))

        Dim cmd As New OleDbCommand()

        Dim Minu1Ope As Double = 0
        Dim Minu2Ope As Double = 0

        Dim Minu1Ope2I As Double = 0
        Dim Minu2Ope2I As Double = 0

        Dim Minu1OpeN As Double = 0
        Dim Minu2OpeN As Double = 0
        Dim Tutoring As Double = 0
        Dim DimissioneProtetta As Double = 0

        Dim Pasto As Integer = 0
        Dim Pasto2 As Integer = 0

        Dim DataApp As String = 0
        Dim CodiceOspiteApp As Integer = 0


        Dim NumeroAccessiAcr As Integer = 0
        Dim NumeroAccessiPrv As Integer = 0


        Dim WDATA As Date
        Dim OldCodiceOspite As Integer = 0
        Dim OraDal As DateTime
        Dim OraAl As DateTime
        Dim Minuti As Integer
        Dim PrimoIngresso As Boolean = True

        MySql = "Select * From MovimentiDomiciliare Where CentroServizio = ? And Data >= ? And Data <= ? Order by CodiceOspite,Data,OraInizio"
        cmd.CommandText = MySql
        Dim Txt_DataDalText As Date = Txt_DataDal.Text
        Dim Txt_DataAlText As Date = Txt_DataAl.Text

        cmd.Parameters.AddWithValue("@CentroServizio", Cmb_CServ.SelectedValue)
        cmd.Parameters.AddWithValue("@DataDal", Txt_DataDalText)
        cmd.Parameters.AddWithValue("@DataAl", Txt_DataAlText)
        
        cmd.Connection = cn
        Dim myPOSTreader As OleDbDataReader = cmd.ExecuteReader()
        Do While myPOSTreader.Read
            If campodb(myPOSTreader.Item("CodiceOspite")) = 428 Then
                CodiceOspiteApp = CodiceOspiteApp
            End If
            If PrimoIngresso = False Then
                If Chk_Raggruppato.Checked = False Then
                    If campodb(myPOSTreader.Item("DATA")) <> DataApp Or campodb(myPOSTreader.Item("CodiceOspite")) <> CodiceOspiteApp Then

                        Dim myriga As System.Data.DataRow = MyTable.NewRow()
                        myriga(0) = CodiceOspiteApp
                        Dim KOs As New ClsOspite

                        KOs.CodiceOspite = CodiceOspiteApp
                        KOs.Leggi(ConnectionString, KOs.CodiceOspite)
                        myriga(1) = KOs.Nome
                        myriga(2) = KOs.CODICEFISCALE

                        Dim K As New Cls_Movimenti

                        K.CENTROSERVIZIO = Cmb_CServ.SelectedValue
                        K.CodiceOspite = KOs.CodiceOspite
                        K.UltimaDataAccoglimento(Session("DC_OSPITE"))
                        myriga(3) = Format(K.Data, "dd/MM/yyyy")

                        K.Data = Nothing

                        K.UltimaDataUscitaDefinitiva(Session("DC_OSPITE"))
                        If Format(K.Data, "yyyyMMdd") > Format(DateSerial(Year(Txt_DataDalText), Month(Txt_DataAlText), 1), "yyyyMMdd") Then
                            myriga(4) = Format(K.Data, "dd/MM/yyyy")
                        Else
                            myriga(4) = ""
                        End If

                        myriga(5) = DataApp
                        myriga(6) = Math.Round(Minu1Ope / 60, 2)
                        myriga(7) = Math.Round(Minu2Ope / 60, 2)

                        myriga(8) = Math.Round(Minu1Ope2I / 60, 2)
                        myriga(9) = Math.Round(Minu2Ope2I / 60, 2)


                        myriga(10) = Math.Round(Minu1OpeN / 60, 2)
                        myriga(11) = Math.Round(Minu2OpeN / 60, 2)
                        myriga(12) = Math.Round(Tutoring / 60, 2)
                        myriga(13) = Math.Round(DimissioneProtetta / 60, 2)
                        myriga(14) = Pasto
                        myriga(15) = Pasto2
                        myriga(16) = NumeroAccessiAcr
                        myriga(17) = NumeroAccessiPrv
                        myriga(19) = RettaMese(Cmb_CServ.SelectedValue, CodiceOspiteApp, Year(Txt_DataDalText), Month(Txt_DataDalText), Month(Txt_DataAlText))
                        myriga(20) = RettaMeseC(Cmb_CServ.SelectedValue, CodiceOspiteApp, Year(Txt_DataDalText), Month(Txt_DataDalText), Month(Txt_DataAlText))
                        myriga(21) = TipoRetta(Cmb_CServ.SelectedValue, CodiceOspiteApp, Txt_DataDalText, Txt_DataAlText)
                        MyTable.Rows.Add(myriga)

                        Minu1Ope = 0
                        Minu2Ope = 0
                        Minu1Ope2I = 0
                        Minu2Ope2I = 0
                        Minu1OpeN = 0
                        Minu2OpeN = 0
                        Tutoring = 0
                        DimissioneProtetta = 0
                        Pasto = 0
                        Pasto2 = 0
                        NumeroAccessiPrv = 0
                        NumeroAccessiAcr = 0
                    End If
                Else
                    If campodb(myPOSTreader.Item("CodiceOspite")) <> CodiceOspiteApp Then

                        Dim myriga As System.Data.DataRow = MyTable.NewRow()
                        myriga(0) = CodiceOspiteApp
                        Dim KOs As New ClsOspite

                        KOs.CodiceOspite = CodiceOspiteApp
                        KOs.Leggi(ConnectionString, KOs.CodiceOspite)
                        myriga(1) = KOs.Nome
                        myriga(2) = KOs.CODICEFISCALE

                        Dim K As New Cls_Movimenti

                        K.CENTROSERVIZIO = Cmb_CServ.SelectedValue
                        K.CodiceOspite = KOs.CodiceOspite
                        K.UltimaDataAccoglimento(Session("DC_OSPITE"))
                        myriga(3) = Format(K.Data, "dd/MM/yyyy")

                        K.Data = Nothing

                        K.UltimaDataUscitaDefinitiva(Session("DC_OSPITE"))
                        If Format(K.Data, "yyyyMMdd") > Format(DateSerial(Year(Txt_DataDalText), Month(Txt_DataAlText), 1), "yyyyMMdd") Then
                            myriga(4) = Format(K.Data, "dd/MM/yyyy")
                        Else
                            myriga(4) = ""
                        End If

                        myriga(5) = Txt_DataDal.Text & " - " & Txt_DataAl.Text
                        myriga(6) = Math.Round(Minu1Ope / 60, 2)
                        myriga(7) = Math.Round(Minu2Ope / 60, 2)

                        myriga(8) = Math.Round(Minu1Ope2I / 60, 2)
                        myriga(9) = Math.Round(Minu2Ope2I / 60, 2)

                        myriga(10) = Math.Round(Minu1OpeN / 60, 2)
                        myriga(11) = Math.Round(Minu2OpeN / 60, 2)
                        myriga(12) = Math.Round(Tutoring / 60, 2)
                        myriga(13) = Math.Round(DimissioneProtetta / 60, 2)

                        myriga(14) = Pasto
                        myriga(15) = Pasto2
                        myriga(16) = NumeroAccessiAcr
                        myriga(17) = NumeroAccessiPrv
                        myriga(19) = RettaMese(Cmb_CServ.SelectedValue, CodiceOspiteApp, Year(Txt_DataAlText), Month(Txt_DataDalText), Month(Txt_DataAlText))
                        myriga(20) = RettaMeseC(Cmb_CServ.SelectedValue, CodiceOspiteApp, Year(Txt_DataAlText), Month(Txt_DataDalText), Month(Txt_DataAlText))
                        myriga(21) = TipoRetta(Cmb_CServ.SelectedValue, CodiceOspiteApp, Txt_DataDalText, Txt_DataAlText)
                        MyTable.Rows.Add(myriga)

                        Minu1Ope = 0
                        Minu2Ope = 0

                        Minu1Ope2I = 0
                        Minu2Ope2I = 0


                        Minu1OpeN = 0
                        Minu2OpeN = 0
                        Tutoring = 0
                        DimissioneProtetta = 0
                        Pasto = 0
                        Pasto2 = 0
                        NumeroAccessiPrv = 0
                        NumeroAccessiAcr = 0
                    End If
                End If
            End If
            PrimoIngresso = False

            DataApp = campodb(myPOSTreader.Item("DATA"))
            CodiceOspiteApp = campodb(myPOSTreader.Item("CodiceOspite"))
            OraDal = campodb(myPOSTreader.Item("OraInizio"))
            OraAl = campodb(myPOSTreader.Item("OraFine"))

            If CodiceOspiteApp = 1728 Then
                CodiceOspiteApp = 1728
            End If

            Minuti = ((OraAl.Minute + (OraAl.Hour * 60)) - (OraDal.Minute + (OraDal.Hour * 60)))

            If WDATA = DataApp And CodiceOspiteApp = OldCodiceOspite Then
                If campodb(myPOSTreader.Item("tipologia")) = "01" Then
                    If campodb(myPOSTreader.Item("Operatore")) = "1" Then
                        Minu1Ope2I = Minu1Ope2I + Minuti
                    Else
                        Minu2Ope2I = Minu2Ope2I + Minuti
                    End If
                    NumeroAccessiAcr = NumeroAccessiAcr + 1
                End If
            Else
                If campodb(myPOSTreader.Item("tipologia")) = "01" Then
                    If campodb(myPOSTreader.Item("Operatore")) = "1" Then
                        Minu1Ope = Minu1Ope + Minuti
                    Else
                        Minu2Ope = Minu2Ope + Minuti
                    End If
                    NumeroAccessiAcr = NumeroAccessiAcr + 1

                    WDATA = DataApp
                    OldCodiceOspite = CodiceOspiteApp
                End If
            End If

  

            If campodb(myPOSTreader.Item("tipologia")) = "02" Then
                If campodb(myPOSTreader.Item("Operatore")) = "1" Then
                    Minu1OpeN = Minu1OpeN + Minuti
                Else
                    Minu2OpeN = Minu2OpeN + Minuti
                End If
                NumeroAccessiPrv = NumeroAccessiPrv + 1
            End If
            If campodb(myPOSTreader.Item("tipologia")) = "05" Then
                Tutoring = Tutoring + Minuti
            End If
            If campodb(myPOSTreader.Item("tipologia")) = "06" Then
                DimissioneProtetta = DimissioneProtetta + Minuti
            End If
            If campodb(myPOSTreader.Item("tipologia")) = "03" Then
                Pasto = Pasto + 1
            End If
            If campodb(myPOSTreader.Item("tipologia")) = "04" Then
                Pasto2 = Pasto2 + 1
            End If
        Loop
        cn.Close()

        If Chk_Raggruppato.Checked = False Then
            If Minu1Ope <> 0 Or Minu2Ope <> 0 Or Minu1OpeN <> 0 Or Minu2OpeN <> 0 Or Tutoring <> 0 Or DimissioneProtetta <> 0 Or Pasto <> 0 Or Pasto2 <> 0 Then

                Dim myriga As System.Data.DataRow = MyTable.NewRow()
                myriga(0) = CodiceOspiteApp
                Dim KOs As New ClsOspite

                KOs.CodiceOspite = CodiceOspiteApp
                KOs.Leggi(ConnectionString, KOs.CodiceOspite)
                myriga(1) = KOs.Nome
                myriga(2) = KOs.CODICEFISCALE

                Dim K As New Cls_Movimenti

                K.CENTROSERVIZIO = Cmb_CServ.SelectedValue
                K.CodiceOspite = KOs.CodiceOspite
                K.UltimaDataAccoglimento(Session("DC_OSPITE"))
                myriga(3) = Format(K.Data, "dd/MM/yyyy")

                K.Data = Nothing

                K.UltimaDataUscitaDefinitiva(Session("DC_OSPITE"))
                If Format(K.Data, "yyyyMMdd") > Format(DateSerial(Year(Txt_DataDalText), Month(Txt_DataAlText), 1), "yyyyMMdd") Then
                    myriga(4) = Format(K.Data, "dd/MM/yyyy")
                Else
                    myriga(4) = ""
                End If

                myriga(5) = DataApp
                myriga(6) = Math.Round(Minu1Ope / 60, 2)
                myriga(7) = Math.Round(Minu2Ope / 60, 2)

                myriga(8) = Math.Round(Minu1Ope2I / 60, 2)
                myriga(9) = Math.Round(Minu2Ope2I / 60, 2)

                myriga(10) = Math.Round(Minu1OpeN / 60, 2)
                myriga(11) = Math.Round(Minu2OpeN / 60, 2)
                myriga(12) = Math.Round(Tutoring / 60, 2)
                myriga(13) = Math.Round(Tutoring / 60, 2)
                myriga(14) = Pasto
                myriga(15) = Pasto2
                myriga(16) = NumeroAccessiAcr
                myriga(17) = NumeroAccessiPrv
                myriga(19) = RettaMese(Cmb_CServ.SelectedValue, CodiceOspiteApp, Year(Txt_DataAlText), Month(Txt_DataAlText), Month(Txt_DataAlText))
                myriga(20) = RettaMeseC(Cmb_CServ.SelectedValue, CodiceOspiteApp, Year(Txt_DataAlText), Month(Txt_DataAlText), Month(Txt_DataAlText))
                myriga(21) = TipoRetta(Cmb_CServ.SelectedValue, CodiceOspiteApp, Txt_DataDalText, Txt_DataAlText)
                MyTable.Rows.Add(myriga)

                Minu1Ope = 0
                Minu2Ope = 0
                Minu1Ope2I = 0
                Minu2Ope2I = 0
                Minu1OpeN = 0
                Minu2OpeN = 0
                Tutoring = 0
                DimissioneProtetta = 0
                Pasto = 0
                Pasto2 = 0
            End If
        Else
            If Minu1Ope <> 0 Or Minu2Ope <> 0 Or Minu1OpeN <> 0 Or Minu2OpeN <> 0 Or Tutoring <> 0 Or DimissioneProtetta <> 0 Or Pasto <> 0 Or Pasto2 <> 0 Then

                Dim myriga As System.Data.DataRow = MyTable.NewRow()
                myriga(0) = CodiceOspiteApp
                Dim KOs As New ClsOspite

                KOs.CodiceOspite = CodiceOspiteApp
                KOs.Leggi(ConnectionString, KOs.CodiceOspite)
                myriga(1) = KOs.Nome
                myriga(2) = KOs.CODICEFISCALE

                Dim K As New Cls_Movimenti

                K.CENTROSERVIZIO = Cmb_CServ.SelectedValue
                K.CodiceOspite = KOs.CodiceOspite
                K.UltimaDataAccoglimento(Session("DC_OSPITE"))
                myriga(3) = Format(K.Data, "dd/MM/yyyy")

                K.Data = Nothing

                K.UltimaDataUscitaDefinitiva(Session("DC_OSPITE"))
                If Format(K.Data, "yyyyMMdd") > Format(DateSerial(Year(Txt_DataDalText), Month(Txt_DataAlText), 1), "yyyyMMdd") Then
                    myriga(4) = Format(K.Data, "dd/MM/yyyy")
                Else
                    myriga(4) = ""
                End If

                myriga(5) = Txt_DataDal.Text & " - " & Txt_DataAl.Text

                myriga(6) = Math.Round(Minu1Ope / 60, 2)
                myriga(7) = Math.Round(Minu2Ope / 60, 2)

                myriga(8) = Math.Round(Minu1Ope2I / 60, 2)
                myriga(9) = Math.Round(Minu2Ope2I / 60, 2)

                myriga(10) = Math.Round(Minu1OpeN / 60, 2)
                myriga(11) = Math.Round(Minu2OpeN / 60, 2)
                myriga(12) = Math.Round(Tutoring / 60, 2)
                myriga(13) = Math.Round(DimissioneProtetta / 60, 2)
                myriga(14) = Pasto
                myriga(15) = Pasto2
                myriga(16) = NumeroAccessiAcr
                myriga(17) = NumeroAccessiPrv
                myriga(19) = RettaMese(Cmb_CServ.SelectedValue, CodiceOspiteApp, Year(Txt_DataAlText), Month(Txt_DataDalText), Month(Txt_DataAlText))
                myriga(20) = RettaMese(Cmb_CServ.SelectedValue, CodiceOspiteApp, Year(Txt_DataAlText), Month(Txt_DataDalText), Month(Txt_DataAlText))
                myriga(21) = TipoRetta(Cmb_CServ.SelectedValue, CodiceOspiteApp, Txt_DataDalText, Txt_DataAlText)
                MyTable.Rows.Add(myriga)

                Minu1Ope = 0
                Minu2Ope = 0
                Minu1Ope2I = 0
                Minu2Ope2I = 0
                Minu1OpeN = 0
                Minu2OpeN = 0
                Tutoring = 0
                Pasto = 0
                Pasto2 = 0
            End If
        End If

        Dim myriga1 As System.Data.DataRow = MyTable.NewRow()
        MyTable.Rows.Add(myriga1)



        If Chk_Accreditato.Checked = True Then
            For i = MyTable.Rows.Count - 1 To 0 Step -1

                Dim jk As Double
                Dim jk2 As Double
                Dim jk3 As Double
                Dim jk4 As Double
                Dim jk5 As Double
                Dim jk6 As Double

                jk = campodbn(MyTable.Rows(i).Item(6))
                jk2 = campodbn(MyTable.Rows(i).Item(7))


                jk3 = campodbn(MyTable.Rows(i).Item(8))
                jk4 = campodbn(MyTable.Rows(i).Item(9))
                jk5 = campodbn(MyTable.Rows(i).Item(12))
                jk6 = campodbn(MyTable.Rows(i).Item(13))


                If jk = 0 And jk2 = 0 And jk3 = 0 And jk4 = 0 And jk5 = 0 And jk6 = 0 Then
                    MyTable.Rows.RemoveAt(i)
                End If
            Next
        End If

        GridView1.AutoGenerateColumns = True
        GridView1.DataSource = MyTable
        GridView1.Font.Size = 10
        GridView1.DataBind()


        For i = 0 To GridView1.Rows.Count - 1

            Dim jk As Integer
            jk = Val(GridView1.Rows(i).Cells(0).Text)

            If jk > 0 Then
                GridView1.Rows(i).Cells(0).Text = "<a href=""#"" onclick=""DialogBox('Elenco_MovimentiDomiciliare.aspx?CodiceOspite=" & jk & "&CentroServizio=" & Cmb_CServ.SelectedValue & "');"" >" & GridView1.Rows(i).Cells(0).Text & "</a>"
            End If
        Next


    End Sub

    Function TipoRetta(ByVal Cserv As String, ByVal CodiceOspite As Long, ByVal DataIni As Date, ByVal DataFin As Date) As String

        Dim cn As OleDbConnection
        Dim MySql As String

        MySql = ""
        TipoRetta = ""

        cn = New Data.OleDb.OleDbConnection(Session("DC_OSPITE"))

        cn.Open()
        Dim cmdW As New OleDbCommand()
        cmdW.CommandText = ("select * from IMPORTORETTA where CentroServizio = '" & Cserv & "' And  CodiceOspite = " & CodiceOspite & "  And Data <= ? ORDER BY DATA DESC")
        cmdW.Parameters.AddWithValue("@Data", DataIni)
        cmdW.Connection = cn
        Dim ReadW As OleDbDataReader = cmdW.ExecuteReader()
        If ReadW.Read Then
            Dim TipoR1 As New Cls_TipoRetta

            TipoR1.Codice = campodb(ReadW.Item("TipoRetta"))
            TipoR1.Leggi(Session("DC_OSPITE"), TipoR1.Codice)
            TipoRetta = TipoRetta & campodb(ReadW.Item("Data")) & "-" & TipoR1.Descrizione

        End If
        ReadW.Close()



        Dim cmd As New OleDbCommand()
        cmd.CommandText = ("select * from IMPORTORETTA where CentroServizio = '" & Cserv & "' And  CodiceOspite = " & CodiceOspite & "  And Data > ? And Data <= ? ORDER BY DATA ")
        cmd.Parameters.AddWithValue("@Data", DataIni)
        cmd.Parameters.AddWithValue("@Data", DataFin)
        cmd.Connection = cn
        Dim myPOSTreader As OleDbDataReader = cmd.ExecuteReader()
        Do While myPOSTreader.Read
            Dim TipoR As New Cls_TipoRetta

            TipoR.Codice = campodb(myPOSTreader.Item("TipoRetta"))
            TipoR.Leggi(Session("DC_OSPITE"), TipoR.Codice)

            If TipoRetta <> "" Then
                TipoRetta = TipoRetta & ", "
            End If
            TipoRetta = TipoRetta & campodb(myPOSTreader.Item("Data")) & "-" & TipoR.Descrizione

        Loop
        myPOSTreader.Close()

        cn.Close()

    End Function



    Function RettaMeseC(ByVal Cserv As String, ByVal CodiceOspite As Long, ByVal Anno As Long, ByVal MeseDal As Long, ByVal MeseAl As Long) As Double

        Dim cn As OleDbConnection


        cn = New Data.OleDb.OleDbConnection(Session("DC_OSPITE"))
        cn.Open()

        Dim cmdRead As New OleDbCommand()
        cmdRead.CommandText = "Select sum(IMPORTO) from RETTECOMUNE Where CENTROSERVIZIO = ? And CODICEOSPITE = ? And Anno = ? And (Mese >= ? and Mese <= ?)  And (ELEMENTO = 'ADD' OR ELEMENTO = 'RGP')"
        cmdRead.Parameters.AddWithValue("@CentroServizio", Cserv)
        cmdRead.Parameters.AddWithValue("@CODICEOSPITE", CodiceOspite)
        cmdRead.Parameters.AddWithValue("@Anno", Anno)
        cmdRead.Parameters.AddWithValue("@MESE", MeseDal)
        cmdRead.Parameters.AddWithValue("@MESE", MeseAl)
        cmdRead.Connection = cn
        Dim Retta As OleDbDataReader = cmdRead.ExecuteReader()
        If Retta.Read Then
            RettaMeseC = campodbn(Retta.Item(0))
        End If
        Retta.Close()

        Dim cmdReadA As New OleDbCommand()
        cmdReadA.CommandText = "Select sum(IMPORTO) from RETTECOMUNE Where CENTROSERVIZIO = ? And CODICEOSPITE = ? And Anno = ? And (Mese >= ? and Mese <= ?)  And ELEMENTO = 'ACC' "
        cmdReadA.Parameters.AddWithValue("@CentroServizio", Cserv)
        cmdReadA.Parameters.AddWithValue("@CODICEOSPITE", CodiceOspite)
        cmdReadA.Parameters.AddWithValue("@Anno", Anno)
        cmdReadA.Parameters.AddWithValue("@MESE", MeseDal)
        cmdReadA.Parameters.AddWithValue("@MESE", MeseAl)
        cmdReadA.Connection = cn
        Dim RettaA As OleDbDataReader = cmdReadA.ExecuteReader()
        If RettaA.Read Then
            RettaMeseC = RettaMeseC - campodbn(RettaA.Item(0))
        End If
        RettaA.Close()


        cn.Close()

        RettaMeseC = Math.Round(RettaMeseC, 2)
    End Function

    Function RettaMese(ByVal Cserv As String, ByVal CodiceOspite As Long, ByVal Anno As Long, ByVal MeseDal As Long, ByVal MeseAl As Long) As Double

        Dim cn As OleDbConnection


        cn = New Data.OleDb.OleDbConnection(Session("DC_OSPITE"))
        cn.Open()

        Dim cmdRead As New OleDbCommand()
        cmdRead.CommandText = "Select sum(IMPORTO) from RetteOspite Where CENTROSERVIZIO = ? And CODICEOSPITE = ? And Anno = ? And (Mese >= ? and Mese <= ?) And (ELEMENTO = 'ADD' OR ELEMENTO = 'RGP')"
        cmdRead.Parameters.AddWithValue("@CentroServizio", Cserv)
        cmdRead.Parameters.AddWithValue("@CODICEOSPITE", CodiceOspite)
        cmdRead.Parameters.AddWithValue("@Anno", Anno)
        cmdRead.Parameters.AddWithValue("@MESE", MeseDal)
        cmdRead.Parameters.AddWithValue("@MESE", MeseAl)
        cmdRead.Connection = cn
        Dim Retta As OleDbDataReader = cmdRead.ExecuteReader()
        If Retta.Read Then
            RettaMese = campodbn(Retta.Item(0))
        End If
        Retta.Close()

        Dim cmdReadA As New OleDbCommand()
        cmdReadA.CommandText = "Select sum(IMPORTO) from RetteOspite Where CENTROSERVIZIO = ? And CODICEOSPITE = ? And Anno = ? And (Mese >= ? and Mese <= ?)  And ELEMENTO = 'ACC' "
        cmdReadA.Parameters.AddWithValue("@CentroServizio", Cserv)
        cmdReadA.Parameters.AddWithValue("@CODICEOSPITE", CodiceOspite)
        cmdReadA.Parameters.AddWithValue("@Anno", Anno)
        cmdReadA.Parameters.AddWithValue("@MESE", MeseDal)
        cmdReadA.Parameters.AddWithValue("@MESE", MeseAl)
        cmdReadA.Connection = cn
        Dim RettaA As OleDbDataReader = cmdReadA.ExecuteReader()
        If RettaA.Read Then
            RettaMese = RettaMese - campodbn(RettaA.Item(0))
        End If
        RettaA.Close()


        cn.Close()

        RettaMese = Math.Round(RettaMese, 2)
    End Function


    Protected Sub DD_Struttura_TextChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles DD_Struttura.TextChanged
        AggiornaCServ()
        Call EseguiJS()
    End Sub

    Private Sub EseguiJS()
        Dim MyJs As String
        MyJs = "$(document).ready(function() {"

        MyJs = MyJs & "var els = document.getElementsByTagName(""*"");"

        MyJs = MyJs & "for (var i=0;i<els.length;i++)"
        MyJs = MyJs & "if ( els[i].id ) { "
        MyJs = MyJs & " var appoggio =els[i].id; "
        MyJs = MyJs & " if (appoggio.match('Txt_Data')!= null) {  "
        MyJs = MyJs & " $(els[i]).mask(""99/99/9999"");"
        MyJs = MyJs & " $(els[i]).datepicker({ changeMonth: true,changeYear: true,  yearRange: ""1990:" & Year(Now) + 5 & """},$.datepicker.regional[""it""]);"

        MyJs = MyJs & "    }"


        MyJs = MyJs & "} "

        MyJs = MyJs & "if (window.innerHeight>0) { $(""#BarraLaterale"").css(""height"",(window.innerHeight - 94) + ""px""); } else"
        MyJs = MyJs & "{ $(""#BarraLaterale"").css(""height"",(document.documentElement.offsetHeight - 94) + ""px"");  }"

        MyJs = MyJs & "});"

        'MyJs = "$(document).ready(function() { $('.myClass').keypress(function() { handleEnter($('.myClass').val(), true, true); } );     $('#" & Txt_ImportoPagato.ClientID & "').blur(function() { var ap = formatNumber ($('#" & Txt_ImportoPagato.ClientID & "').val(),2); $('#" & Txt_ImportoPagato.ClientID & "').val(ap); }); });"
        ScriptManager.RegisterStartupScript(Me, Me.GetType(), "visualizzaRitIm", MyJs, True)
    End Sub

    Protected Sub ImageButton3_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles ImageButton3.Click
        If Not IsDate(Txt_DataDal.Text) Then
            REM ClientScript.RegisterClientScriptBlock(Me.GetType(), "Errore", "VisualizzaErrore('Data dal non corretta');", True)
            ScriptManager.RegisterStartupScript(Me, Me.GetType(), "visualizzaA", "VisualizzaErrore('Data dal non corretta');", True)
            Exit Sub
        End If
        If Not IsDate(Txt_DataAl.Text) Then
            REM ClientScript.RegisterClientScriptBlock(Me.GetType(), "Errore", "VisualizzaErrore('Data al non corretta');", True)
            ScriptManager.RegisterStartupScript(Me, Me.GetType(), "visualizzaA", "VisualizzaErrore('Data al non corretta');", True)
            Exit Sub
        End If
        If Cmb_CServ.SelectedValue = "" Then
            REM ClientScript.RegisterClientScriptBlock(Me.GetType(), "Errore", "VisualizzaErrore('Specificare centro servizio');", True)
            ScriptManager.RegisterStartupScript(Me, Me.GetType(), "visualizzaA", "VisualizzaErrore('Specificare centro servizio');", True)
            Exit Sub
        End If
        Call ProspettoPresenze()
    End Sub

    Protected Sub ImageButton4_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles ImageButton4.Click
        If Not IsDate(Txt_DataDal.Text) Then
            REM ClientScript.RegisterClientScriptBlock(Me.GetType(), "Errore", "VisualizzaErrore('Data dal non corretta');", True)
            ScriptManager.RegisterStartupScript(Me, Me.GetType(), "visualizzaA", "VisualizzaErrore('Data dal non corretta');", True)
            Exit Sub
        End If
        If Not IsDate(Txt_DataAl.Text) Then
            REM ClientScript.RegisterClientScriptBlock(Me.GetType(), "Errore", "VisualizzaErrore('Data al non corretta');", True)
            ScriptManager.RegisterStartupScript(Me, Me.GetType(), "visualizzaA", "VisualizzaErrore('Data al non corretta');", True)
            Exit Sub
        End If
        If Cmb_CServ.SelectedValue = "" Then
            REM ClientScript.RegisterClientScriptBlock(Me.GetType(), "Errore", "VisualizzaErrore('Specificare centro servizio');", True)
            ScriptManager.RegisterStartupScript(Me, Me.GetType(), "visualizzaA", "VisualizzaErrore('Specificare centro servizio');", True)
            Exit Sub
        End If
        Call ProspettoPresenze()

        If MyTable.Rows.Count > 1 Then
            Response.Clear()
            Response.AddHeader("content-disposition", "attachment;filename=ProspettoPresenze.xls")
            Response.Charset = String.Empty
            Response.Cache.SetCacheability(HttpCacheability.NoCache)
            Response.ContentType = "application/vnd.xls"
            Dim stringWrite As New System.IO.StringWriter
            Dim htmlWrite As New HtmlTextWriter(stringWrite)
            'GridView1.RenderControl(htmlWrite)


            form1.Controls.Clear()
            form1.Controls.Add(GridView1)

            form1.RenderControl(htmlWrite)

            Response.Write(stringWrite.ToString())
            Response.End()
        End If
    End Sub

    Protected Sub Btn_Esci_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles Btn_Esci.Click
        Response.Redirect("Menu_StatisticheDomiciliari.aspx")
    End Sub

    Protected Sub Btn_Stampa_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles Btn_Stampa.Click

        If Not IsDate(Txt_DataDal.Text) Then
            REM ClientScript.RegisterClientScriptBlock(Me.GetType(), "Errore", "VisualizzaErrore('Data dal non corretta');", True)
            ScriptManager.RegisterStartupScript(Me, Me.GetType(), "visualizzaA", "VisualizzaErrore('Data dal non corretta');", True)
            Exit Sub
        End If
        If Not IsDate(Txt_DataAl.Text) Then
            REM ClientScript.RegisterClientScriptBlock(Me.GetType(), "Errore", "VisualizzaErrore('Data al non corretta');", True)
            ScriptManager.RegisterStartupScript(Me, Me.GetType(), "visualizzaA", "VisualizzaErrore('Data al non corretta');", True)
            Exit Sub
        End If
        If Cmb_CServ.SelectedValue = "" Then
            REM ClientScript.RegisterClientScriptBlock(Me.GetType(), "Errore", "VisualizzaErrore('Specificare centro servizio');", True)
            ScriptManager.RegisterStartupScript(Me, Me.GetType(), "visualizzaA", "VisualizzaErrore('Specificare centro servizio');", True)
            Exit Sub
        End If

        Call ProspettoPresenze()

        Session("GrigliaSoloStampa") = MyTable

        Session("ParametriSoloStampa") = ""

        ScriptManager.RegisterStartupScript(Me, Me.GetType(), "Stampa", "openPopUp('GrigliaSoloStampa.aspx','Stampe','width=800,height=600');", True)

        Call EseguiJS()
    End Sub
End Class
