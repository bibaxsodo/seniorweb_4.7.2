﻿Imports System.Web.Hosting
Imports System.Data.OleDb


Partial Class OspitiWeb_ForzaNonInUso
    Inherits System.Web.UI.Page
    Protected Sub Btn_Elimina_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles Btn_Elimina.Click
        Dim ConnectionString As String = Session("DC_GENERALE")
        Dim cn As OleDbConnection
        Dim cnOspiti As OleDbConnection
        Dim MySql As String
        Dim Condizione As String
        Dim UltimoNumero As Long




        Dim Txt_DataEmissioneText As Date



        If Not IsDate(Txt_DataEmissione.Text) Then
            ClientScript.RegisterClientScriptBlock(Me.GetType(), "visualizzaA", "VisualizzaErrore('Data formalmente errata');", True)
            Exit Sub
        End If




        Txt_DataEmissioneText = Txt_DataEmissione.Text
        cn = New Data.OleDb.OleDbConnection(ConnectionString)

        cn.Open()

        cnOspiti = New Data.OleDb.OleDbConnection(Session("DC_OSPITE"))

        cnOspiti.Open()

        Dim cmdUpdate As New OleDbCommand()
        cmdUpdate.Connection = cnOspiti
        cmdUpdate.CommandText = "UPDATE [AnagraficaComune] SET NONINUSO ='S' WHERE  [AnagraficaComune].TIPOLOGIA = 'O' AND    (SELECT TOP 1 TIPOMOV FROM MOVIMENTI WHERE MOVIMENTI.CODICEOSPITE = [AnagraficaComune].CODICEOSPITE  ORDER BY DATA DESC) ='13' AND (SELECT TOP 1 DATA  FROM MOVIMENTI WHERE MOVIMENTI.CODICEOSPITE = [AnagraficaComune].CODICEOSPITE  ORDER BY DATA DESC) < ?"
        cmdUpdate.Parameters.AddWithValue("@DATA", Txt_DataEmissioneText)
        cmdUpdate.ExecuteNonQuery()


        cn.Close()
        cnOspiti.Close()

        ScriptManager.RegisterClientScriptBlock(Me, Me.GetType(), "ESEGUITO", "alert('Imposta non in uso terminata');", True)
    End Sub

    Protected Sub Btn_Esci_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles Btn_Esci.Click
        Response.Redirect("Menu_Strumenti.aspx")
    End Sub


    Private Sub EseguiJS()
        Dim MyJs As String
        MyJs = "$(document).ready(function() {"

        MyJs = MyJs & "var els = document.getElementsByTagName(""*"");"

        MyJs = MyJs & "for (var i=0;i<els.length;i++)"
        MyJs = MyJs & "if ( els[i].id ) { "
        MyJs = MyJs & " var appoggio =els[i].id; "
        MyJs = MyJs & " if (appoggio.match('Txt_Data')!= null) {  "
        MyJs = MyJs & " $(els[i]).mask(""99/99/9999"");"
        MyJs = MyJs & " $(els[i]).datepicker({ changeMonth: true,changeYear: true,  yearRange: ""1990:" & Year(Now) + 5 & """},$.datepicker.regional[""it""]);"

        MyJs = MyJs & "    }"


        MyJs = MyJs & "} "
        MyJs = MyJs & "});"

        'MyJs = "$(document).ready(function() { $('.myClass').keypress(function() { handleEnter($('.myClass').val(), true, true); } );     $('#" & Txt_ImportoPagato.ClientID & "').blur(function() { var ap = formatNumber ($('#" & Txt_ImportoPagato.ClientID & "').val(),2); $('#" & Txt_ImportoPagato.ClientID & "').val(ap); }); });"
        ScriptManager.RegisterStartupScript(Me, Me.GetType(), "visualizzaRitIm", MyJs, True)
    End Sub

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load

        Call EseguiJS()

        If Page.IsPostBack = True Then
            Exit Sub
        End If

        Dim K1 As New Cls_SqlString

        Dim Barra As New Cls_BarraSenior

        If Not IsNothing(Session("RicercaAnagraficaSQLString")) Then
            K1 = Session("RicercaAnagraficaSQLString")
        End If

        Lbl_BarraSenior.Text = Barra.CodiceBarra(Application("SENIOR"), Session("UTENTE"), Page, K1)
    End Sub
End Class
