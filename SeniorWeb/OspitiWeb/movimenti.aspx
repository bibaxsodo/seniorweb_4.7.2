﻿<%@ Page Language="VB" AutoEventWireup="false" Inherits="movimenti" EnableEventValidation="false" CodeFile="movimenti.aspx.vb" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="xasp" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <meta http-equiv="x-ua-compatible" content="IE=9" />
    <title>Movimenti</title>
    <asp:PlaceHolder runat="server">
        <%: System.Web.Optimization.Styles.Render("~/Content/AjaxControlToolkit/Styles/Bundle") %>
    </asp:PlaceHolder>
    <link href="ospiti.css?ver=11" rel="stylesheet" type="text/css" />
    <link rel="shortcut icon" href="images/SENIOR.ico" />
    <link href="js/jquery.autocomplete.css" rel="stylesheet" type="text/css" />

    <link rel="stylesheet" href="dialogbox/redips-dialog.css" type="text/css" media="screen" />
    <script type="text/javascript" src="dialogbox/redips-dialog-min.js"></script>
    <script type="text/javascript" src="dialogbox/script.js"></script>


    <script src="js/jquery-1.5.1.min.js" type="text/javascript"></script>
    <script src="js/jquery.autocomplete.js" type="text/javascript"></script>
    <script src="js/soapclient.js" type="text/javascript"></script>
    <script src="/js/convertinumero.js" type="text/javascript"></script>

    <link rel="stylesheet" href="dialogbox/redips-dialog.css" type="text/css" media="screen" />
    <script type="text/javascript" src="dialogbox/redips-dialog-min.js"></script>


    <script src="js/jquery.maskedinput-1.3.min.js" type="text/javascript"></script>
    <script src="/js/formatnumer.js" type="text/javascript"></script>
    <script src="js/JSErrore.js" type="text/javascript"></script>
    <script src="js/NoEnter.js" type="text/javascript"></script>
    <script type="text/javascript" src="../js/menuanagraficaospiti.js?ver=7"></script>

    <script src="js/chosen/chosen.jquery.js" type="text/javascript"></script>
    <script src="js/chosen/docsupport/prism.js" type="text/javascript" charset="utf-8"></script>
    <link rel="stylesheet" href="js/chosen/docsupport/style.css">
    <link rel="stylesheet" href="js/chosen/docsupport/prism.css">
    <link rel="stylesheet" href="js/chosen/chosen.css">

    <link rel="stylesheet" href="jqueryui/jquery-ui.css" type="text/css" />
    <script src="jqueryui/jquery-ui.js" type="text/javascript"></script>
    <script src="jqueryui/datapicker-it.js" type="text/javascript"></script>
    <script type="text/javascript">
        function DialogBox(Path) {
            REDIPS.dialog.show(500, 300, '<iframe id="output" src="' + Path + '" height="300px" width="500"></iframe>');
            return false;
        }

        $(document).ready(function () {
            $('html').keyup(function (event) {

                if (event.keyCode == 113) {
                    __doPostBack("Btn_Modifica0", "0");
                }


                if (event.keyCode == 120) {
                    __doPostBack("BTN_InserisciRiga", "0");
                }
            });
        });

        $(document).ready(function () {
            if (window.innerHeight > 0) { $("#BarraLaterale").css("height", (window.innerHeight - 94) + "px"); } else { $("#BarraLaterale").css("height", (document.documentElement.offsetHeight - 94) + "px"); }
        });
        function Chiudi() {
            $("#blur").css('visibility', 'hidden');
            $("#pippo").css('visibility', 'hidden');
        }
        function VisualizzaDivRegistrazione() {
            $("#blur").css('visibility', 'visible');
            $("#pippo").css('visibility', 'visible');
            $("#Txt_DataAcco").mask("99/99/9999");
        }

        function ApriLiberazione() {
            $(document).ready(function () {
                $("#LiberaStanza").css('visibility', 'visible');
            });
        }
    </script>
    <style>
        .wait {
            border: solid 1px #2d2d2d;
            text-align: left;
            vertical-align: top;
            background: white;
            width: 50%;
            height: 350px;
            top: 30%;
            left: 30%;
            position: absolute;
            -moz-border-radius: 5px;
            -webkit-border-radius: 5px;
            border-radius: 5px;
            box-shadow: 0px 0px 10px 4px rgba(0, 125, 196, 0.75);
            -moz-box-shadow: 0px 0px 10px 4px rgba(0, 125, 196, 0.75);
            -webkit-box-shadow: 0px 0px 10px 4px rgba(0, 125, 196, 0.75);
            z-index: 133;
        }

        #blur {
            width: 100%;
            background-color: black;
            moz-opacity: 0.5;
            khtml-opacity: .5;
            opacity: .5;
            filter: alpha(opacity=50);
            z-index: 120;
            height: 100%;
            position: absolute;
            top: 0;
            left: 0;
        }

        .CLSLibera {
            position: absolute;
            top: 30%;
            left: 40%;
            height: 200px;
            width: 300px;
            -ms-filter: "progid:DXImageTransform.Microsoft.Alpha(Opacity=84)";
            filter: alpha(opacity=84);
            -moz-opacity: 0.84;
            -khtml-opacity: 0.84;
            opacity: 0.84;
            border: 2px #1474f0 solid;
            background-color: #033fa5;
            -moz-box-shadow: 13px 13px 4px 5px #190000;
            -webkit-box-shadow: 13px 13px 4px 5px #190000;
            box-shadow: 13px 13px 4px 5px #190000;
        }
    </style>
</head>
<body>
    <form id="form1" runat="server">
        <asp:ScriptManager ID="ScriptManager1" runat="server" EnableScriptGlobalization="true">
            <Scripts>
                <asp:ScriptReference Path="~/Scripts/AjaxControlToolkit/Bundle" />
            </Scripts>
        </asp:ScriptManager>
        <asp:Label ID="Lbl_BarraSenior" runat="server" Text=""></asp:Label>
        <div align="left">
            <asp:Button ID="BTN_InserisciRiga" runat="server" Visible="false" />

            <table style="width: 100%;" cellpadding="0" cellspacing="0">
                <tr>
                    <td style="width: 185px; background-color: #F0F0F0;"></td>
                    <td>
                        <div class="Titolo">Ospiti - Anagrafica - Movimenti</div>
                        <div class="SottoTitoloOSPITE">
                            <asp:Label ID="Lbl_NomeOspite" runat="server"></asp:Label><br />
                            <br />
                        </div>
                    </td>
                    <td style="text-align: right;">
                        <div class="DivTastiOspite">
                            <asp:ImageButton ID="Btn_Modifica0" runat="server" Height="38px" ImageUrl="~/images/salva.jpg" class="EffettoBottoniTondi" Width="38px" ToolTip="Modifica / Inserisci (F2)" />
                            <br />
                        </div>
                    </td>
                </tr>
            </table>
            <table style="width: 100%;" cellpadding="0" cellspacing="0">

                <tr>
                    <td style="width: 160px; background-color: #F0F0F0; vertical-align: top; text-align: center;" id="BarraLaterale">
                        <a href="Menu_Ospiti.aspx" style="border-width: 0px;">
                            <img src="images/Home.jpg" alt="Menù" class="Effetto" /></a><br />
                        <asp:ImageButton ID="Btn_Esci" runat="server" BackColor="Transparent" ImageUrl="../images/Menu_Indietro.png" class="Effetto" ToolTip="Chiudi" />
                        <br />

                        <div id="MENUDIV"></div>
                        <br />
                        <br />
                        <br />
                        <br />
                        <br />
                        <br />
                        <br />
                        <br />
                        <br />
                        <br />
                        <br />
                        <br />
                    </td>
                    <td colspan="2" style="background-color: #FFFFFF; vertical-align: top;">
                        <xasp:TabContainer ID="TabContainer1" runat="server" ActiveTabIndex="0" CssClass="TabSenior"
                            Width="100%" BorderStyle="None" Style="margin-right: 39px">
                            <xasp:TabPanel runat="server" HeaderText="Prima Nota" ID="Tab_Anagrafica">
                                <HeaderTemplate>
                                    Movimenti Entrata/Uscita        
                                </HeaderTemplate>
                                <ContentTemplate>
                                    <asp:UpdatePanel ID="UpdatePanel1" runat="server">
                                        <ContentTemplate>
                                            <br />
                                            <a href="#" onclick="VisualizzaDivRegistrazione();">Nuovo Centro Servizio</a>
                                            <br />
                                            <asp:GridView ID="Grd_ImportoComune" runat="server" CellPadding="4" Height="60px"
                                                ShowFooter="True" BackColor="White" BorderColor="#6FA7D1"
                                                BorderStyle="Dotted" BorderWidth="1px">
                                                <RowStyle ForeColor="#333333" BackColor="White" />
                                                <Columns>
                                                    <asp:TemplateField>
                                                        <ItemTemplate>
                                                            <asp:ImageButton ID="IB_Delete" CommandName="Delete" runat="Server" ImageUrl="~/images/cancella.png" />

                                                        </ItemTemplate>
                                                        <FooterTemplate>
                                                            <div style="text-align: right">
                                                                <asp:ImageButton ID="ImageButton1" ImageUrl="~/images/inserisci.png" CommandName="Inserisci" runat="server" />
                                                            </div>

                                                        </FooterTemplate>
                                                    </asp:TemplateField>

                                                    <asp:TemplateField HeaderText="Data">
                                                        <ItemTemplate>
                                                            <asp:TextBox ID="TxtData" onkeypress="return handleEnter(this, event)" Width="90px" runat="server"></asp:TextBox>
                                                        </ItemTemplate>
                                                        <HeaderStyle Font-Bold="False" Font-Italic="False" />
                                                        <ItemStyle Width="30px" />
                                                    </asp:TemplateField>

                                                    <asp:TemplateField HeaderText="Tipo Movimento">
                                                        <ItemTemplate>
                                                            <asp:DropDownList ID="DD_TipoMov" runat="server" Width="150px" OnSelectedIndexChanged="DD_TipoMov_SelectedIndexChanged" AutoPostBack="True">
                                                                <asp:ListItem Text="Uscita Temporanea" Value="03"></asp:ListItem>
                                                                <asp:ListItem Text="Accoglimento" Value="05"></asp:ListItem>
                                                                <asp:ListItem Text="Uscita Definitiva" Value="13"></asp:ListItem>
                                                                <asp:ListItem Text="Entrata" Value="04"></asp:ListItem>
                                                            </asp:DropDownList>
                                                        </ItemTemplate>
                                                        <HeaderStyle Font-Bold="False" Font-Italic="False" />
                                                        <ItemStyle Width="150px" />
                                                    </asp:TemplateField>


                                                    <asp:TemplateField HeaderText="Raggruppamento">
                                                        <ItemTemplate>
                                                            <asp:DropDownList ID="DD_Raggruppamento" OnSelectedIndexChanged="DD_Raggruppamento_SelectedIndexChanged" runat="server" AutoPostBack="True" Width="150px"></asp:DropDownList>
                                                        </ItemTemplate>
                                                        <HeaderStyle Font-Bold="False" Font-Italic="False" />
                                                        <ItemStyle Width="30px" />
                                                    </asp:TemplateField>

                                                    <asp:TemplateField HeaderText="Causale">
                                                        <ItemTemplate>
                                                            <asp:DropDownList ID="DD_Causale" runat="server" Width="280px"></asp:DropDownList>
                                                        </ItemTemplate>
                                                        <HeaderStyle Font-Bold="False" Font-Italic="False" />
                                                        <ItemStyle Width="30px" />
                                                    </asp:TemplateField>

                                                    <asp:TemplateField HeaderText="Descrizione">
                                                        <ItemTemplate>
                                                            <asp:TextBox ID="Txt_Descrizione" onkeypress="return handleEnter(this, event)" Width="350px" runat="server"></asp:TextBox>
                                                        </ItemTemplate>
                                                        <HeaderStyle Font-Bold="False" Font-Italic="False" />
                                                        <ItemStyle Width="250px" />
                                                    </asp:TemplateField>


                                                    <asp:TemplateField HeaderText="Ora">
                                                        <ItemTemplate>
                                                            <asp:TextBox ID="TxtOra" onkeypress="return handleEnter(this, event)" Width="80px" runat="server"></asp:TextBox>
                                                        </ItemTemplate>
                                                        <HeaderStyle Font-Bold="False" Font-Italic="False" />
                                                        <ItemStyle Width="80px" />
                                                    </asp:TemplateField>

                                                </Columns>

                                                <FooterStyle BackColor="White" ForeColor="#023102" />

                                                <HeaderStyle BackColor="#A6C9E2" Font-Bold="False" ForeColor="White" BorderColor="#6FA7D1" BorderWidth="1" />

                                                <PagerStyle BackColor="#336666" ForeColor="White" HorizontalAlign="Center" />

                                                <SelectedRowStyle BackColor="#339966" Font-Bold="True" ForeColor="White" />
                                            </asp:GridView>
                                            <br />


                                            <br />
                                            <asp:Label ID="Lbl_errori" runat="server" ForeColor="DarkRed" Width="272px"></asp:Label><br />
                                            <br />


                                            </div>
                                        </ContentTemplate>
                                    </asp:UpdatePanel>
                                </ContentTemplate>
                            </xasp:TabPanel>
                        </xasp:TabContainer>

                    </td>
                </tr>
            </table>
            <div id="blur" style="visibility: hidden;">&nbsp;</div>
            <div id="pippo" style="visibility: hidden;" class="wait">
                <div style="text-align: left; display: inline-table;"><a href="#" onclick="Chiudi();">
                    <img src="../images/annulla.png" title="Chiudi" /></a></div>
                <div style="text-align: right; float: right;">
                    <asp:ImageButton ID="Img_CreaAccoglimento" runat="server" Height="38px" ImageUrl="~/images/salva.jpg" class="EffettoBottoniTondi" Width="38px" ToolTip="Modifica / Inserisci (F2)" /></div>
                <br />
                <label class="LabelCampo">Struttura:</label>
                <asp:DropDownList runat="server" ID="DD_Struttura" AutoPostBack="true"></asp:DropDownList><br />
                <br />
                <label class="LabelCampo">Centro Servizio:</label>
                <asp:DropDownList runat="server" ID="DD_CServ"></asp:DropDownList><br />
                <br />
                <label class="LabelCampo">Data Accoglimento :</label>
                <asp:TextBox ID="Txt_DataAcco" runat="server" Width="100px"></asp:TextBox><br />
                <br />
                <label class="LabelCampo">Raggruppamento :</label>
                <asp:DropDownList ID="DD_RaggruppamentoAcc" OnSelectedIndexChanged="DD_RaggruppamentoAcc_SelectedIndexChanged" runat="server" AutoPostBack="True" Width="150px"></asp:DropDownList><br />
                <br />
                <label class="LabelCampo">Causale :</label>
                <asp:DropDownList ID="DD_CausaleAcc" runat="server" Width="150px"></asp:DropDownList><br />
                <br />
                <label class="LabelCampo">Allinea Importi :</label>
                <asp:CheckBox ID="Chk_RiportaImporti" runat="server" />

            </div>

            <div id="LiberaStanza" class="CLSLibera" style="visibility: hidden;">
                <b style="color: white;">E' presente il movimento di uscita definitiva, vuoi generare la liberazione della stanza? </b>
                <br />
                <br />
                <br />
                <asp:Button ID="Btn_Si" runat="server" Text="Si" Width="40px" />
                <asp:Button ID="Btn_no" runat="server" Text="No" Width="40px" />
            </div>
        </div>
    </form>
</body>
</html>
