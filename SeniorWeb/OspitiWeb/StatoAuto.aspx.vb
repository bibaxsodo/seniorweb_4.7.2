﻿Imports System.Web.Hosting
Imports System.Data.OleDb


Partial Class StatoAuto
    Inherits System.Web.UI.Page
    Dim MyTable As New System.Data.DataTable("tabella")
    Dim MyDataSet As New System.Data.DataSet()


    Sub loadpagina()
        Dim x As New ClsOspite
        Dim d As New Cls_StatoAuto


        Dim ConnectionString As String = Session("DC_OSPITE")


        x.Leggi(ConnectionString, Session("CODICEOSPITE"))



        d.loaddati(ConnectionString, Session("CODICEOSPITE"), Session("CODICESERVIZIO"), MyTable)

        ViewState("App_Retta") = MyTable

        Grd_Retta.AutoGenerateColumns = False

        Grd_Retta.DataSource = MyTable

        Grd_Retta.DataBind()

    End Sub
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load

        If IsNothing(Session("DC_OSPITE")) Then
            Response.Redirect("..\Login.aspx")
            Exit Sub
        End If
        If Session("ABILITAZIONI").IndexOf("<PORTINERIA>") > 0 Then
            Response.Redirect("..\MainPortineria.aspx")
        End If

        If Page.IsPostBack = False Then

            If Val(Request.Item("CodiceOspite")) > 0 And Request.Item("CentroServizio") <> "" Then

                Session("CODICESERVIZIO") = Request.Item("CentroServizio")
                Session("CODICEOSPITE") = Val(Request.Item("CodiceOspite"))
            End If

            ViewState("CODICESERVIZIO") = Session("CODICESERVIZIO")
            ViewState("CODICEOSPITE") = Session("CODICEOSPITE")

            Dim K1 As New Cls_SqlString

            Dim Barra As New Cls_BarraSenior

            If Not IsNothing(Session("RicercaAnagraficaSQLString")) Then
                K1 = Session("RicercaAnagraficaSQLString")
            End If

            Lbl_BarraSenior.Text = Barra.CodiceBarra(Application("SENIOR"), Session("UTENTE"), Page, K1)

            Call loadpagina()

            Dim x As New ClsOspite

            x.CodiceOspite = Session("CODICEOSPITE")
            x.Leggi(Session("DC_OSPITE"), x.CodiceOspite)

            Dim cs As New Cls_CentroServizio

            cs.Leggi(Session("DC_OSPITE"), Session("CODICESERVIZIO"))

            Lbl_NomeOspite.Text = x.Nome & " -  " & x.DataNascita & " - " & cs.DESCRIZIONE & "<i style=""font-size:small;"">(" & x.CodiceOspite & ")</i>"
        End If
    End Sub

    Private Sub InserisciRiga()
        UpDateTable()
        MyTable = ViewState("App_Retta")
        Dim myriga As System.Data.DataRow = MyTable.NewRow()
        myriga(0) = ""
        myriga(1) = 0

        MyTable.Rows.Add(myriga)

        ViewState("App_Retta") = MyTable
        Grd_Retta.AutoGenerateColumns = False

        Grd_Retta.DataSource = MyTable

        Grd_Retta.DataBind()

        Dim TxtData As TextBox = DirectCast(Grd_Retta.Rows(Grd_Retta.Rows.Count - 1).FindControl("TxtData"), TextBox)

        TxtData.Focus()

        ClientScript.RegisterClientScriptBlock(Me.GetType(), "setFocus", "$(document).ready(function() {  setTimeout(function(){ document.getElementById('" + TxtData.ClientID + "').focus(); }); }, 500);", True)

        Call EseguiJS()
    End Sub
    Protected Sub Grd_Retta_RowCommand(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewCommandEventArgs) Handles Grd_Retta.RowCommand
        If (e.CommandName = "Inserisci") Then

            Call InserisciRiga()

        End If

    End Sub






    Protected Sub Grd_Retta_RowDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs) Handles Grd_Retta.RowDataBound
        If e.Row.RowType = DataControlRowType.DataRow Then


            Dim TxtData As TextBox = DirectCast(e.Row.FindControl("TxtData"), TextBox)

            TxtData.Text = MyTable.Rows(e.Row.RowIndex).Item(0).ToString



            Dim RB_Auto As RadioButton = DirectCast(e.Row.FindControl("RB_Auto"), RadioButton)
            Dim RB_NonAuto As RadioButton = DirectCast(e.Row.FindControl("RB_NonAuto"), RadioButton)


            If MyTable.Rows(e.Row.RowIndex).Item(1).ToString = "A" Or MyTable.Rows(e.Row.RowIndex).Item(1).ToString = "" Then
                RB_Auto.Checked = True
            End If
            If MyTable.Rows(e.Row.RowIndex).Item(1).ToString = "N" Then
                RB_NonAuto.Checked = True
            End If
 


            Dim DD_Usl As DropDownList = DirectCast(e.Row.FindControl("DD_Usl"), DropDownList)

            Dim MyE As New ClsUSL

            MyE.UpDateDropBox(Session("DC_OSPITE"), DD_Usl)

            Try
                DD_Usl.SelectedValue = MyTable.Rows(e.Row.RowIndex).Item(2).ToString
            Catch ex As Exception


            End Try


            If MyTable.Rows(e.Row.RowIndex).Item(2).ToString <> "" And DD_Usl.SelectedValue = "" Then
                Dim TrovaCodice As Boolean = False
                Dim Indice As Integer


                For Indice = 0 To DD_Usl.Items.Count - 1
                    If DD_Usl.Items(Indice).Value = MyTable.Rows(e.Row.RowIndex).Item(2).ToString Then
                        TrovaCodice = True
                    End If
                Next

                If TrovaCodice = False Then

                    Dim USL As New ClsUSL

                    USL.Nome = ""
                    USL.CodiceRegione = MyTable.Rows(e.Row.RowIndex).Item(2).ToString
                    USL.Leggi(Session("DC_OSPITE"))

                    If USL.Nome <> "" Then
                        DD_Usl.Items.Add(USL.Nome & " (" & USL.NonInVisualizzazione & ")")
                        DD_Usl.Items(DD_Usl.Items.Count - 1).Value = MyTable.Rows(e.Row.RowIndex).Item(2).ToString

                        DD_Usl.SelectedValue = MyTable.Rows(e.Row.RowIndex).Item(2).ToString
                    End If
                End If
            End If

            Dim DD_TipoRetta As DropDownList = DirectCast(e.Row.FindControl("DD_TipoRetta"), DropDownList)

            Dim MyTipoRetta As New Cls_TabellaTipoImportoRegione

            MyTipoRetta.UpDateDropBox(Session("DC_OSPITE"), DD_TipoRetta)

            DD_TipoRetta.SelectedValue = MyTable.Rows(e.Row.RowIndex).Item(3).ToString

            Call EseguiJS()

        End If
    End Sub


    Private Sub EseguiJS()
        Dim MyJs As String
        MyJs = "$(document).ready(function() {"

        MyJs = MyJs & "var els = document.getElementsByTagName(""*"");"

        MyJs = MyJs & "for (var i=0;i<els.length;i++)"
        MyJs = MyJs & "if ( els[i].id ) { "
        MyJs = MyJs & " var appoggio =els[i].id; "
        MyJs = MyJs & " if (appoggio.match('TxtData')!= null) {  "
        MyJs = MyJs & " $(els[i]).mask(""99/99/9999"");"

        MyJs = MyJs & " $(els[i]).datepicker({ changeMonth: true,changeYear: true,  yearRange: ""1990:" & Year(Now) + 5 & """},$.datepicker.regional[""it""]);"
        MyJs = MyJs & "    }"

        MyJs = MyJs & " if ((appoggio.match('TxtImporto')!= null) ) {  "
        MyJs = MyJs & " $(els[i]).keypress(function() { ForceNumericInput($(this).val(), true, true); } ); "
        MyJs = MyJs & " $(els[i]).blur(function() { var ap = formatNumber($(this).val(),2); $(this).val(ap); });"
        MyJs = MyJs & "    }"


        MyJs = MyJs & "} "
        MyJs = MyJs & "});"

        'MyJs = "$(document).ready(function() { $('.myClass').keypress(function() { handleEnter($('.myClass').val(), true, true); } );     $('#" & Txt_ImportoPagato.ClientID & "').blur(function() { var ap = formatNumber ($('#" & Txt_ImportoPagato.ClientID & "').val(),2); $('#" & Txt_ImportoPagato.ClientID & "').val(ap); }); });"
        ScriptManager.RegisterStartupScript(Me, Me.GetType(), "visualizzaRitIm", MyJs, True)
    End Sub

    Private Sub UpDateTable()



        Dim i As Integer

        MyTable.Clear()
        MyTable.Columns.Add("Data", GetType(String))
        MyTable.Columns.Add("STATOAUTO", GetType(String))
        MyTable.Columns.Add("USL", GetType(String))
        MyTable.Columns.Add("TipoRetta", GetType(String))

        For i = 0 To Grd_Retta.Rows.Count - 1

            Dim TxtData As TextBox = DirectCast(Grd_Retta.Rows(i).FindControl("TxtData"), TextBox)
            Dim RB_Auto As RadioButton = DirectCast(Grd_Retta.Rows(i).FindControl("RB_Auto"), RadioButton)
            Dim RB_NonAuto As RadioButton = DirectCast(Grd_Retta.Rows(i).FindControl("RB_NonAuto"), RadioButton)
            Dim DD_Usl As DropDownList = DirectCast(Grd_Retta.Rows(i).FindControl("DD_Usl"), DropDownList)
            Dim DD_TipoRetta As DropDownList = DirectCast(Grd_Retta.Rows(i).FindControl("DD_TipoRetta"), DropDownList)

            Dim myrigaR As System.Data.DataRow = MyTable.NewRow()


            myrigaR(0) = TxtData.Text

            If RB_Auto.Checked = True Then
                myrigaR(1) = "A"
            End If
            If RB_NonAuto.Checked = True Then
                myrigaR(1) = "N"
            End If

            myrigaR(2) = DD_Usl.Text

            myrigaR(3) = DD_TipoRetta.Text
        
            MyTable.Rows.Add(myrigaR)
        Next
        ViewState("App_Retta") = MyTable
    End Sub

    Protected Sub Grd_Retta_RowDeleted(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewDeletedEventArgs) Handles Grd_Retta.RowDeleted

    End Sub


    Protected Sub Grd_Retta_RowDeleting(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewDeleteEventArgs) Handles Grd_Retta.RowDeleting

        UpDateTable()
        MyTable = ViewState("App_Retta")
        Try
            If Request.UserAgent.Contains("Chrome") Then
                If IsDBNull(Session("RI_TIMER")) Or DateDiff("s", Session("RI_TIMER"), Now) > 1 Then
                    MyTable.Rows.RemoveAt(e.RowIndex)
                    If MyTable.Rows.Count = 0 Then
                        Dim myriga As System.Data.DataRow = MyTable.NewRow()
                        myriga(1) = 0
                        myriga(2) = 0
                        MyTable.Rows.Add(myriga)
                    End If
                    Session("RI_TIMER") = Now
                End If
            Else
                MyTable.Rows.RemoveAt(e.RowIndex)
                If MyTable.Rows.Count = 0 Then
                    Dim myriga As System.Data.DataRow = MyTable.NewRow()
                    myriga(1) = 0
                    myriga(2) = 0
                    MyTable.Rows.Add(myriga)
                End If
            End If
        Catch ex As Exception

        End Try


        ViewState("App_Retta") = MyTable

        Grd_Retta.AutoGenerateColumns = False

        Grd_Retta.DataSource = MyTable

        Grd_Retta.DataBind()
        Call EseguiJS()

        e.Cancel = True
    End Sub

    Protected Sub Btn_Modifica_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles Btn_Modifica.Click
        Session("CODICESERVIZIO") = ViewState("CODICESERVIZIO")
        Session("CODICEOSPITE") = ViewState("CODICEOSPITE")
        If Val(Session("CODICEOSPITE")) = 0 Then
            ClientScript.RegisterClientScriptBlock(Me.GetType(), "Errore", "VisualizzaErrore('Devi ricercare l ospite');", True)
            Exit Sub
        End If

        Dim i As Integer
        Dim T As Integer

        For i = 0 To Grd_Retta.Rows.Count - 1
            Dim TxtData As TextBox = DirectCast(Grd_Retta.Rows(i).FindControl("TxtData"), TextBox)
            Dim RB_Auto As RadioButton = DirectCast(Grd_Retta.Rows(i).FindControl("RB_Auto"), RadioButton)
            Dim RB_NonAuto As RadioButton = DirectCast(Grd_Retta.Rows(i).FindControl("RB_NonAuto"), RadioButton)
            Dim DD_Usl As DropDownList = DirectCast(Grd_Retta.Rows(i).FindControl("DD_Usl"), DropDownList)
            Dim DD_TipoRetta As DropDownList = DirectCast(Grd_Retta.Rows(i).FindControl("DD_TipoRetta"), DropDownList)

            If Not IsDate(TxtData.Text) Then
                ClientScript.RegisterClientScriptBlock(Me.GetType(), "Errore", "VisualizzaErrore('Data errata per riga " & i + 1 & "');", True)
                Call EseguiJS()
                Exit Sub
            End If

            If RB_Auto.Checked = False And RB_NonAuto.Checked = False Then
                ClientScript.RegisterClientScriptBlock(Me.GetType(), "Errore", "VisualizzaErrore('Specificare stato per riga " & i + 1 & "');", True)
                Call EseguiJS()
                Exit Sub
            End If

            If DD_Usl.SelectedValue <> "" Then
                Dim MyUsl As New ClsUSL

                If MyUsl.RegionePadre(Session("DC_OSPITE"), DD_Usl.SelectedValue) Then
                    ClientScript.RegisterClientScriptBlock(Me.GetType(), "Errore", "VisualizzaErrore('Non puoi usare una regione padre " & i + 1 & "');", True)
                    Call EseguiJS()
                    Exit Sub
                End If
            End If

            If DD_TipoRetta.SelectedValue <> "" Then
                Dim Mi As New Cls_ImportoRegione
                Mi.Data = Nothing
                Mi.UltimaData(Session("DC_OSPITE"), DD_Usl.SelectedValue, DD_TipoRetta.SelectedValue)
                If Year(Mi.Data) < 1900 Then
                    ClientScript.RegisterClientScriptBlock(Me.GetType(), "Errore", "VisualizzaErrore('Tipo Retta non resente per la regione in  riga " & i + 1 & "');", True)
                    Call EseguiJS()
                    Exit Sub
                End If
            End If

            For T = 0 To Grd_Retta.Rows.Count - 1
                If T <> i Then
                    Dim TxtDatax As TextBox = DirectCast(Grd_Retta.Rows(T).FindControl("TxtData"), TextBox)

                    If TxtDatax.Text = TxtData.Text Then
                        ClientScript.RegisterClientScriptBlock(Me.GetType(), "Errore", "VisualizzaErrore('Data duplicata per riga " & i + 1 & " e riga " & T + 1 & "');", True)
                        Call EseguiJS()
                        Exit Sub
                    End If


                End If
            Next
        Next
        Call UpDateTable()


        Dim Log1 As New Cls_LogPrivacy

        Log1.LogPrivacy(Application("SENIOR"), Session("CLIENTE"), Session("UTENTE"), Session("UTENTEIMPER"), Page.GetType.Name.ToUpper, Val(Session("CODICEOSPITE")), 0, "", "", 0, "", "M", "OSPITE", "")

        Dim Log As New Cls_LogPrivacy

        Dim ConvT As New Cls_DataTableToJson
        Dim OldTable As New System.Data.DataTable("tabellaOld")


        Dim OldDatiPar As New Cls_StatoAuto

        OldDatiPar.loaddati(Session("DC_OSPITE"), Session("CODICEOSPITE"), Session("CODICESERVIZIO"), OldTable)
        Dim AppoggioJS As String = ConvT.DataTableToJsonObj(OldTable)

        Log.LogPrivacy(Application("SENIOR"), Session("CLIENTE"), Session("UTENTE"), Session("UTENTEIMPER"), Page.GetType.Name.ToUpper, Session("CODICEOSPITE"), 0, "", "", 0, "", "M", "STATO", AppoggioJS)


        MyTable = ViewState("App_Retta")
        Dim X1 As New Cls_StatoAuto

        X1.CODICEOSPITE = Session("CODICEOSPITE")
        X1.CENTROSERVIZIO = Session("CODICESERVIZIO")

        X1.AggiornaDaTabella(Session("DC_OSPITE"), MyTable)
       

        Dim MyJs As String

        MyJs = "alert('Modifica Effettuata');"
        ScriptManager.RegisterClientScriptBlock(Me, Me.GetType(), "ModScM", MyJs, True)

    End Sub

    Protected Sub Btn_Esci_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles Btn_Esci.Click
        If Request.Item("PAGINA") = "" Then
            Response.Redirect("RicercaAnagrafica.aspx")
        Else
            Response.Redirect("RicercaAnagrafica.aspx?CHIAMATA=RITORNO&PAGINA=" & Request.Item("PAGINA"))
        End If
    End Sub

    Protected Sub BTN_InserisciRiga_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles BTN_InserisciRiga.Click
        Call InserisciRiga()
    End Sub
End Class
