﻿Imports System.Web.Hosting
Imports System.Data.OleDb
Partial Class OspitiWeb_VisualizzazioneAddebitoAccredito
    Inherits System.Web.UI.Page
    Dim MyTable As New System.Data.DataTable("tabella")
    Dim MyDataSet As New System.Data.DataSet()

    Function campodb(ByVal oggetto As Object) As String
        If IsDBNull(oggetto) Then
            Return ""
        Else
            Return oggetto
        End If
    End Function
    Private Sub CaricaTabella()
        Dim ConnectionString As String = Session("DC_OSPITE")
        Dim cn As OleDbConnection
        Dim MySql As String
        Dim Condizione As String
        Dim CondOspite As String
        Dim Ordine As String = ""

        cn = New Data.OleDb.OleDbConnection(ConnectionString)

        cn.Open()

        MyTable.Clear()
        MyTable.Columns.Clear()

        MyTable.Columns.Add("Centro Servizio", GetType(String))
        MyTable.Columns.Add("Codice Ospite", GetType(Long))
        MyTable.Columns.Add("Nome", GetType(String))        
        MyTable.Columns.Add("Tipo Movimento", GetType(String))
        MyTable.Columns.Add("Data", GetType(String))
        MyTable.Columns.Add("Riferimento", GetType(String))
        MyTable.Columns.Add("Figura di Riferimento", GetType(String))
        MyTable.Columns.Add("Importo", GetType(Double))
        MyTable.Columns.Add("Retta", GetType(String))
        MyTable.Columns.Add("Periodo Competenza", GetType(String))
        MyTable.Columns.Add("Tipo Addebiti", GetType(String))
        MyTable.Columns.Add("Descrizione", GetType(String))
        MyTable.Columns.Add("Elaborato", GetType(String))
        Dim cmd As New OleDbCommand()

        CondOspite = ""
        If Txt_Ospite.Text.Trim <> "" Then
            Dim Vettore(100) As String

            Vettore = SplitWords(Txt_Ospite.Text)
            If Not IsNothing(Vettore(0)) Then
                If Val(Vettore(0)) > 0 Then                    
                    CondOspite = CondOspite & "  CodiceOspite = " & Val(Vettore(0))
                End If
            End If
        End If

        If RB_Data.Checked = True Then
            Ordine = " Order by Data"
        End If

        If RB_CodiceOspite.Checked = True Then
            Ordine = " Order by (Select top 1 Nome From AnagraficaComune Where AnagraficaComune.CodiceOspite = ADDACR.CodiceOspite And CodiceParente = 0),Data"
        End If

        If DD_CServ.SelectedValue = "" Then
            Condizione = ""
            If RD_SoloAutomatiici.Checked = True Then
                Condizione = Condizione & " (Not ChiaveSelezione is Null and ChiaveSelezione <> '')"
            End If

            If RD_SoloManauli.Checked = True Then
                Condizione = Condizione & " ( ChiaveSelezione is Null or ChiaveSelezione = '')"
            End If

            If DD_Struttura.SelectedValue <> "" Then
                If Condizione <> "" Then
                    Condizione = Condizione & " And ("
                Else
                    Condizione = Condizione & " ("
                End If
                Dim Indice As Integer
                Call AggiornaCServ()
                For Indice = 0 To DD_CServ.Items.Count - 1
                    If Indice >= 1 Then
                        If Condizione <> "" Then
                            Condizione = Condizione & " Or "
                        End If
                    End If
                    Condizione = Condizione & "  CentroServizio = '" & DD_CServ.Items(Indice).Value & "'"
                Next
                Condizione = Condizione & ") "
                If CondOspite <> "" Then
                    'CondOspite 
                    Condizione = Condizione & " And " & CondOspite
                End If
                MySql = "Select * From ADDACR Where Data >= ? And Data <= ? And " & Condizione & Ordine
            Else
                If Condizione <> "" Then
                    Condizione = " And " & Condizione
                End If
                If CondOspite <> "" Then
                    MySql = "Select * From ADDACR Where Data >= ? And Data <= ? " & " And " & CondOspite & " " & Condizione & Ordine
                Else
                    MySql = "Select * From ADDACR Where Data >= ? And Data <= ? " & Condizione & Ordine
                End If

            End If
        Else
            Condizione = ""
            If RD_SoloAutomatiici.Checked = True Then
                Condizione = " And (Not ChiaveSelezione is Null and ChiaveSelezione <> '')"
            End If

            If RD_SoloManauli.Checked = True Then
                Condizione = " And ( ChiaveSelezione is Null or ChiaveSelezione = '')"
            End If

            If CondOspite <> "" Then
                MySql = "Select * From ADDACR Where Data >= ? And Data <= ?  And CENTROSERVIZIO = '" & DD_CServ.SelectedValue & "'" & " And " & CondOspite & " " & Condizione & Ordine
            Else
                MySql = "Select * From ADDACR Where Data >= ? And Data <= ?  And CENTROSERVIZIO = '" & DD_CServ.SelectedValue & "'" & " " & Condizione & Ordine
            End If

        End If


        cmd.CommandText = MySql
        Dim Txt_DataDalText As Date = Txt_DataDal.Text
        Dim Txt_DataAlText As Date = Txt_DataAl.Text

        cmd.Parameters.AddWithValue("@DataDal", Txt_DataDalText)
        cmd.Parameters.AddWithValue("@DataAl", Txt_DataAlText)
        cmd.Connection = cn
        Dim myPOSTreader As OleDbDataReader = cmd.ExecuteReader()
        Do While myPOSTreader.Read
            Dim xOsp As New ClsOspite

            xOsp.Leggi(Session("DC_OSPITE"), Val(campodb(myPOSTreader.Item("CodiceOspite"))))

            If (dd_TipoAddebito.SelectedValue = "" Or dd_TipoAddebito.SelectedValue = campodb(myPOSTreader.Item("CodiceIVA"))) And xOsp.NonInUso <> "S" Then
                Dim myriga As System.Data.DataRow = MyTable.NewRow()

                Dim DSC As New Cls_CentroServizio


                DSC.Leggi(Session("DC_OSPITE"), campodb(myPOSTreader.Item("CentroServizio")))
                myriga(0) = DSC.DESCRIZIONE
                myriga(1) = Val(campodb(myPOSTreader.Item("CodiceOspite")))


                myriga(2) = xOsp.Nome


                If campodb(myPOSTreader.Item("TipoMov")) = "AD" Then
                    myriga(3) = "Addebito"
                Else
                    myriga(3) = "Accredito"
                End If
                If campodb(myPOSTreader.Item("Data")) = "" Then
                    myriga(4) = ""
                Else
                    myriga(4) = Format(myPOSTreader.Item("Data"), "dd/MM/yyyy")
                End If
                Dim TRig As String = ""
                Dim Fig As String = ""
                If campodb(myPOSTreader.Item("Riferimento")) = "O" Then
                    TRig = "OSPITE"
                    Fig = ""
                End If
                If campodb(myPOSTreader.Item("Riferimento")) = "P" Then
                    TRig = "PARENTE"
                    Dim XPar As New Cls_Parenti

                    XPar.Leggi(Session("DC_OSPITE"), Val(campodb(myPOSTreader.Item("CodiceOspite"))), Val(campodb(myPOSTreader.Item("Parente"))))
                    Fig = XPar.Nome
                End If
                If campodb(myPOSTreader.Item("Riferimento")) = "R" Then
                    TRig = "REGIONE"
                    Dim XReg As New ClsUSL

                    XReg.CodiceRegione = campodb(myPOSTreader.Item("Regione"))
                    XReg.Leggi(Session("DC_OSPITE"))
                    Fig = XReg.Nome
                End If
                If campodb(myPOSTreader.Item("Riferimento")) = "C" Then
                    TRig = "COMUNE"

                    Dim XComune As New ClsComune

                    XComune.Provincia = campodb(myPOSTreader.Item("Provincia"))
                    XComune.Comune = campodb(myPOSTreader.Item("Comune"))
                    XComune.DecodficaComune(Session("DC_OSPITE"))
                    Fig = XComune.Descrizione
                End If
                myriga(5) = TRig
                myriga(6) = Fig
                If campodb(myPOSTreader.Item("Importo")) = "" Then
                    myriga(7) = 0
                Else
                    myriga(7) = Format(CDbl((myPOSTreader.Item("Importo"))), "#,##0.00")
                End If
                myriga(8) = campodb(myPOSTreader.Item("Retta"))

                myriga(9) = campodb(myPOSTreader.Item("MESECOMPETENZA")) & "/" & campodb(myPOSTreader.Item("ANNOCOMPETENZA"))

                Dim K As New Cls_Addebito

                K.Codice = campodb(myPOSTreader.Item("CodiceIVA"))
                K.Leggi(ConnectionString, K.Codice)

                myriga(10) = K.Descrizione
                myriga(11) = campodb(myPOSTreader.Item("Descrizione"))

                If Val(campodb(myPOSTreader.Item("Elaborato"))) = 1 Then
                    myriga(12) = "SI"
                Else
                    myriga(12) = "NO"
                End If

                MyTable.Rows.Add(myriga)
            End If
        Loop
        cn.Close()

        Dim myriga1 As System.Data.DataRow = MyTable.NewRow()
        MyTable.Rows.Add(myriga1)

        GridView1.AutoGenerateColumns = True
        GridView1.DataSource = MyTable
        GridView1.Font.Size = 10
        GridView1.DataBind()

    End Sub

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load

        If Trim(Session("UTENTE")) = "" Then
            Response.Redirect("../Login.aspx")
            Exit Sub
        End If

        If Page.IsPostBack = True Then Exit Sub

        Dim K1 As New Cls_SqlString

        Dim Barra As New Cls_BarraSenior

        If Not IsNothing(Session("RicercaAnagraficaSQLString")) Then
            K1 = Session("RicercaAnagraficaSQLString")
        End If

        Lbl_BarraSenior.Text = Barra.CodiceBarra(Application("SENIOR"), Session("UTENTE"), Page, K1)

        Dim x As New Cls_Addebito

        x.UpDateDropBox(Session("DC_OSPITE"), dd_TipoAddebito)



        Dim kVilla As New Cls_TabelleDescrittiveOspitiAccessori


        kVilla.UpDateDropBox(Session("DC_OSPITIACCESSORI"), "VIL", DD_Struttura)
        If DD_Struttura.Items.Count = 1 Then
            Call AggiornaCServ()
        End If

        Lbl_Utente.Text = Session("UTENTE")

        Txt_DataAl.Text = Format(Now, "dd/MM/yyyy")
        Txt_DataDal.Text = Format(Now, "dd/MM/yyyy")

        Call EseguiJS()
    End Sub

    Protected Sub ImageButton3_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles ImageButton3.Click
        If Not IsDate(Txt_DataDal.Text) Then
            REM ClientScript.RegisterClientScriptBlock(Me.GetType(), "Errore", "VisualizzaErrore('Data dal non corretta');", True)
            ScriptManager.RegisterStartupScript(Me, Me.GetType(), "visualizzaA", "VisualizzaErrore('Data dal non corretta');", True)
            Exit Sub
        End If
        If Not IsDate(Txt_DataAl.Text) Then
            REM ClientScript.RegisterClientScriptBlock(Me.GetType(), "Errore", "VisualizzaErrore('Data al non corretta');", True)
            ScriptManager.RegisterStartupScript(Me, Me.GetType(), "visualizzaA", "VisualizzaErrore('Data al non corretta');", True)
            Exit Sub
        End If

        Call CaricaTabella()

        call EseguiJS()
    End Sub

    Protected Sub ImageButton4_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles ImageButton4.Click
        If Not IsDate(Txt_DataDal.Text) Then
            ClientScript.RegisterClientScriptBlock(Me.GetType(), "Errore", "VisualizzaErrore('Data dal non corretta');", True)
            Exit Sub
        End If
        If Not IsDate(Txt_DataAl.Text) Then
            ClientScript.RegisterClientScriptBlock(Me.GetType(), "Errore", "VisualizzaErrore('Data al non corretta');", True)
            Exit Sub
        End If


        Call CaricaTabella()
        If MyTable.Rows.Count > 1 Then
            Response.Clear()
            Response.AddHeader("content-disposition", "attachment;filename=AddebitiAccrediti.xls")
            Response.Charset = String.Empty
            Response.Cache.SetCacheability(HttpCacheability.NoCache)
            Response.ContentType = "application/vnd.xls"
            Dim stringWrite As New System.IO.StringWriter
            Dim htmlWrite As New HtmlTextWriter(stringWrite)

            form1.Controls.Clear()
            form1.Controls.Add(GridView1)

            form1.RenderControl(htmlWrite)

            Response.Write(stringWrite.ToString())
            Response.End()
        End If
    End Sub



    Private Sub EseguiJS()
        Dim MyJs As String
        MyJs = "$(document).ready(function() {"
        MyJs = MyJs & "      if (window.innerHeight>0) { $(""#BarraLaterale"").css(""height"",(window.innerHeight - 94) + ""px""); } else"
        MyJs = MyJs & "                { $(""#BarraLaterale"").css(""height"",(document.documentElement.offsetHeight - 94) + ""px"");  } "
        MyJs = MyJs & "var els = document.getElementsByTagName(""*"");"

        MyJs = MyJs & "for (var i=0;i<els.length;i++)"
        MyJs = MyJs & "if ( els[i].id ) { "
        MyJs = MyJs & " var appoggio =els[i].id; "


        MyJs = MyJs & " if ((appoggio.match('Txt_Ospite')!= null) || (appoggio.match('Txt_Ospite')!= null))  {  "
        MyJs = MyJs & " $(els[i]).autocomplete('AutocompleteOspitiSenzaCserv.ashx?Utente=" & Session("UTENTE") & "', {delay:5,minChars:3});"
        MyJs = MyJs & "    }"


        MyJs = MyJs & " if (appoggio.match('Txt_Data')!= null) {  "
        MyJs = MyJs & " $(els[i]).mask(""99/99/9999"");"
        MyJs = MyJs & " $(els[i]).datepicker({ changeMonth: true,changeYear: true,  yearRange: ""1990:" & Year(Now) + 5 & """},$.datepicker.regional[""it""]);"
        MyJs = MyJs & "    }"


        MyJs = MyJs & "} "
        MyJs = MyJs & "});"

        'MyJs = "$(document).ready(function() { $('.myClass').keypress(function() { handleEnter($('.myClass').val(), true, true); } );     $('#" & Txt_ImportoPagato.ClientID & "').blur(function() { var ap = formatNumber ($('#" & Txt_ImportoPagato.ClientID & "').val(),2); $('#" & Txt_ImportoPagato.ClientID & "').val(ap); }); });"
        ScriptManager.RegisterStartupScript(Me, Me.GetType(), "visualizzaRitIm", MyJs, True)
    End Sub

    Protected Sub Btn_Esci_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles Btn_Esci.Click
        Response.Redirect("Menu_Visualizzazioni.aspx")
    End Sub


    Private Sub AggiornaCServ()
        Dim kCsrv As New Cls_CentroServizio

        If DD_Struttura.SelectedValue = "" Then
            kCsrv.UpDateDropBox(Session("DC_OSPITE"), DD_CServ)
        Else
            kCsrv.UpDateDropBoxStruttura(Session("DC_OSPITE"), DD_CServ, DD_Struttura.SelectedValue)
        End If
    End Sub



    Protected Sub DD_Struttura_TextChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles DD_Struttura.TextChanged
        AggiornaCServ()
        Call EseguiJS()
    End Sub

    Protected Sub DD_CServ_TextChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles DD_CServ.TextChanged
        Dim kTipoAdd As New Cls_Addebito

        If DD_CServ.SelectedValue = "" Then
            kTipoAdd.UpDateDropBox(Session("DC_OSPITE"), dd_TipoAddebito)
        Else
            kTipoAdd.UpDateDropBoxCentroServizio(Session("DC_OSPITE"), dd_TipoAddebito, DD_CServ.SelectedValue)
        End If
        Call EseguiJS()
    End Sub


    Private Function SplitWords(ByVal s As String) As String()
        '
        ' Call Regex.Split function from the imported namespace.
        ' Return the result array.
        '
        Return Regex.Split(s, "\W+")
    End Function

    Protected Sub Btn_Stampa_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles Btn_Stampa.Click
        Call CaricaTabella()


        Session("GrigliaSoloStampa") = MyTable

        Session("ParametriSoloStampa") = ""

        ScriptManager.RegisterStartupScript(Me, Me.GetType(), "Stampa", "openPopUp('GrigliaSoloStampa.aspx','Stampe','width=800,height=600');", True)

    End Sub
End Class
