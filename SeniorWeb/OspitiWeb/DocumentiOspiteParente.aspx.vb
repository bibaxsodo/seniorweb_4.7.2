﻿Imports System.Web.Hosting
Imports System.Data.OleDb

Partial Class DocumentiOspiteParente
    Inherits System.Web.UI.Page
    Dim MyTable As New System.Data.DataTable("tabella")
    Dim MyDataSet As New System.Data.DataSet()

    Private Sub CaricaGriglia(ByVal Excel As Boolean)
        Dim ConnectionString As String = Session("DC_OSPITE")
        Dim StringaConnessioneTabelle As String = Session("DC_TABELLE")
        Dim StringaConnessioneGenerale As String = Session("DC_GENERALE")

        Dim Stampa As New StampeOspiti

        Dim MySql As String = ""
        Dim Saldo As Double
        Dim SaldoIniziale As Double = 0
        Dim DisponibileSuDocumenti As Double


        Dim cn As OleDbConnection

        cn = New Data.OleDb.OleDbConnection(ConnectionString)

        cn.Open()

        Dim cnGenerale As OleDbConnection

        cnGenerale = New Data.OleDb.OleDbConnection(StringaConnessioneGenerale)

        cnGenerale.Open()


        Dim cnTabelle As OleDbConnection

        cnTabelle = New Data.OleDb.OleDbConnection(StringaConnessioneTabelle)

        cnTabelle.Open()

        Dim Mastro As Long = 0
        Dim Conto As Long = 0
        Dim SottoConto As Long = 0

        Dim CServizio As New Cls_CentroServizio

        CServizio.Leggi(ConnectionString, Session("CODICESERVIZIO"))
        Mastro = CServizio.MASTRO
        Conto = CServizio.CONTO
        SottoConto = Int(Session("CODICEOSPITE") * 100)
        If Val(DD_Parenti.SelectedValue) > 0 Then
            SottoConto = SottoConto + Val(DD_Parenti.SelectedValue)
        End If

        MyTable.Clear()
        MyTable.Columns.Clear()
        MyTable.Columns.Add("Registrazione", GetType(String))
        MyTable.Columns.Add("Numero Documento", GetType(String))
        MyTable.Columns.Add("Data Documento", GetType(String))
        MyTable.Columns.Add("Tipo Documento", GetType(String))
        MyTable.Columns.Add("Importo Dovuto", GetType(String))
        MyTable.Columns.Add("Importo Disponibile", GetType(String))
        MyTable.Columns.Add("Importo Pagato", GetType(String))
        MyTable.Columns.Add("Importo Legato", GetType(String))
        MyTable.Columns.Add("Incassi Legati", GetType(String))
        MyTable.Columns.Add("Descrizione", GetType(String))

        If Val(Txt_Anno.Text) > 0 Then
            MySql = "Select * From MovimentiContabiliRiga INNER JOIN MOVIMENTICONTABILITESTA ON NUMERO = NUMEROREGISTRAZIONE Where MastroPartita = " & Mastro & " And ContoPartita = " & Conto & " And SottoContoPartita = " & SottoConto & " And year(DataRegistrazione) < " & Val(Txt_Anno.Text) & " Order by DataRegistrazione"
            Dim cmdSaldo As New OleDbCommand()
            cmdSaldo.CommandText = (MySql)
            cmdSaldo.Connection = cnGenerale

            Dim ReadSaldo As OleDbDataReader = cmdSaldo.ExecuteReader()
            Do While ReadSaldo.Read()
                Dim CausaleContabile As String = ""
                Dim TotImp As Double
                Dim Numero As Integer = 0
                Dim TipoDocumento As String = ""


                Numero = campodb(ReadSaldo.Item("Numero"))

                MySql = "Select * From MovimentiContabiliTesta Where NumeroRegistrazione = " & Numero
                Dim cmdT As New OleDbCommand()
                cmdT.CommandText = (MySql)
                cmdT.Connection = cnGenerale

                Dim RdT As OleDbDataReader = cmdT.ExecuteReader()
                Do While RdT.Read
                    CausaleContabile = campodb(RdT.Item("CausaleContabile"))
                Loop
                RdT.Close()


                Dim Mov As New Cls_MovimentoContabile


                Mov.NumeroRegistrazione = campodb(ReadSaldo.Item("Numero"))
                Mov.Leggi(StringaConnessioneGenerale, Mov.NumeroRegistrazione)

                TotImp = Mov.ImportoDocumento(StringaConnessioneTabelle)

                If TotImp = 0 Then
                    MySql = "Select sum(Importo) as totimp From MovimentiContabiliRiga Where  DareAvere = 'D' And Numero = " & campodb(ReadSaldo.Item("Numero"))
                    Dim cmdG As New OleDbCommand()
                    cmdG.CommandText = (MySql)
                    cmdG.Connection = cnGenerale

                    Dim RdG As OleDbDataReader = cmdG.ExecuteReader()
                    Do While RdG.Read
                        If IsDBNull(RdG.Item("totimp")) Then
                            TotImp = 0
                        Else
                            TotImp = RdG.Item("totimp")
                        End If
                    Loop
                    RdG.Close()
                End If

                Dim TipoCausale As String = ""

                MySql = "Select * From CausaliContabiliTesta Where Codice = '" & CausaleContabile & "'"
                Dim cmdCau As New OleDbCommand()
                cmdCau.CommandText = (MySql)
                cmdCau.Connection = cnTabelle

                Dim RdCau As OleDbDataReader = cmdCau.ExecuteReader()
                If RdCau.Read Then
                    TipoDocumento = campodb(RdCau.Item("TipoDocumento"))
                    If campodb(RdCau.Item("Tipo")) = "P" Then
                        TipoDocumento = "INC"
                        If Mov.Righe(0).DareAvere = "D" Then
                            TotImp = TotImp
                            TipoDocumento = "PAG"
                        Else
                            TotImp = TotImp * -1
                        End If
                    End If
                    If campodb(RdCau.Item("Tipo")) = "G" Or campodb(RdCau.Item("Tipo")) = "C" Then
                        If Mov.Righe(0).DareAvere = "A" Then
                            TotImp = TotImp * -1
                            TipoDocumento = "INC"
                            TipoCausale = "X"
                        Else
                            TipoDocumento = "DOC"
                            TipoCausale = "X"
                        End If
                    End If
                End If
                RdCau.Close()

                Saldo = Saldo + TotImp
            Loop
            ReadSaldo.Close()

        End If
        Lbl_SaldoIniziale.Text = "Saldo Iniziale " & Format(Saldo, "#,##0.00")
        SaldoIniziale=Saldo


        MySql = "Select * From MovimentiContabiliRiga INNER JOIN MOVIMENTICONTABILITESTA ON NUMERO = NUMEROREGISTRAZIONE Where MastroPartita = " & Mastro & " And ContoPartita = " & Conto & " And SottoContoPartita = " & SottoConto & " Order by DataRegistrazione"
        Dim cmdCC As New OleDbCommand()
        cmdCC.CommandText = (MySql)
        cmdCC.Connection = cnGenerale

        Dim RdCC As OleDbDataReader = cmdCC.ExecuteReader()
        Do While RdCC.Read()
            Dim Numero As Long

            Dim NumeroDocumento As Long = 0
            Dim DataDocumento As String = ""
            Dim TipoDocumento As String = ""
            Dim ImportoTotale As Double = 0
            Dim ImportoLegato As Double = 0
            Dim CausaleContabile As String = ""
            Dim TotImp As Double

            Numero = campodb(RdCC.Item("Numero"))

            MySql = "Select * From MovimentiContabiliTesta Where NumeroRegistrazione = " & Numero
            Dim cmdT As New OleDbCommand()
            cmdT.CommandText = (MySql)
            cmdT.Connection = cnGenerale

            Dim RdT As OleDbDataReader = cmdT.ExecuteReader()
            Do While RdT.Read
                If campodb(RdT.Item("NumeroDocumento")) = "" Then
                    NumeroDocumento = 0
                Else
                    NumeroDocumento = campodb(RdT.Item("NumeroDocumento"))
                End If

                DataDocumento = campodb(RdT.Item("DataDocumento"))
                If Not IsDate(DataDocumento) Then
                    DataDocumento = campodb(RdT.Item("DataRegistrazione"))
                End If

                TipoDocumento = ""
                CausaleContabile = campodb(RdT.Item("CausaleContabile"))
            Loop
            RdT.Close()


            Dim Mov As New Cls_MovimentoContabile


            Mov.NumeroRegistrazione = campodb(RdCC.Item("Numero"))
            Mov.Leggi(StringaConnessioneGenerale, Mov.NumeroRegistrazione)

            TotImp = Mov.ImportoDocumento(StringaConnessioneTabelle)

            If TotImp = 0 Then
                MySql = "Select sum(Importo) as totimp From MovimentiContabiliRiga Where  DareAvere = 'D' And Numero = " & campodb(RdCC.Item("Numero"))
                Dim cmdG As New OleDbCommand()
                cmdG.CommandText = (MySql)
                cmdG.Connection = cnGenerale

                Dim RdG As OleDbDataReader = cmdG.ExecuteReader()
                Do While RdG.Read
                    If IsDBNull(RdG.Item("totimp")) Then
                        TotImp = 0
                    Else
                        TotImp = RdG.Item("totimp")
                    End If
                Loop
                RdG.Close()
            End If

            Dim TipoCausale As String = ""

            MySql = "Select * From CausaliContabiliTesta Where Codice = '" & CausaleContabile & "'"
            Dim cmdCau As New OleDbCommand()
            cmdCau.CommandText = (MySql)
            cmdCau.Connection = cnTabelle

            Dim RdCau As OleDbDataReader = cmdCau.ExecuteReader()
            If RdCau.Read Then
                TipoDocumento = campodb(RdCau.Item("TipoDocumento"))
                If campodb(RdCau.Item("Tipo")) = "P" Then
                    TipoDocumento = "INC"
                    If Mov.Righe(0).DareAvere = "D" Then
                        TotImp = TotImp
                        TipoDocumento = "PAG"
                    Else
                        TotImp = TotImp * -1
                    End If
                End If
                If campodb(RdCau.Item("Tipo")) = "G" Or campodb(RdCau.Item("Tipo")) = "C" Then
                    If Mov.Righe(0).DareAvere = "A" Then
                        TotImp = TotImp * -1
                        TipoDocumento = "INC"
                        TipoCausale = "X"
                    Else
                        TipoDocumento = "DOC"
                        TipoCausale = "X"
                    End If
                End If
            End If
            RdCau.Close()
            Dim IncassiLegati As String = ""


            If TipoDocumento = "INC" Or TipoDocumento = "PAG" Then
                MySql = "Select sum(importo) as totimp From TabellaLegami Where  CodicePagamento = " & Numero
                Dim cmdRL As New OleDbCommand()
                cmdRL.CommandText = (MySql)
                cmdRL.Connection = cnGenerale
                Dim RDRL As OleDbDataReader = cmdRL.ExecuteReader()
                If RDRL.Read Then
                    ImportoLegato = IIf(campodb(RDRL.Item("totimp")) = "", 0, campodb(RDRL.Item("totimp")))
                End If
                RDRL.Close()
            Else
                MySql = "Select sum(importo) as totimp From TabellaLegami Where  CodiceDocumento = " & Numero
                Dim cmdRL As New OleDbCommand()
                cmdRL.CommandText = (MySql)
                cmdRL.Connection = cnGenerale
                Dim RDRL As OleDbDataReader = cmdRL.ExecuteReader()
                If RDRL.Read Then
                    ImportoLegato = IIf(campodb(RDRL.Item("totimp")) = "", 0, campodb(RDRL.Item("totimp")))
                End If
                RDRL.Close()

                MySql = "Select * From TabellaLegami Where  CodiceDocumento = " & Numero
                Dim cmdRLD As New OleDbCommand()
                cmdRLD.CommandText = (MySql)
                cmdRLD.Connection = cnGenerale
                Dim RDRLD As OleDbDataReader = cmdRLD.ExecuteReader()
                Do While RDRLD.Read
                    Dim KReg As New Cls_MovimentoContabile

                    KReg.NumeroRegistrazione = Val(campodb(RDRLD.Item("CodicePagamento")))
                    KReg.Leggi(Session("DC_GENERALE"), KReg.NumeroRegistrazione)


                    If IncassiLegati <> "" Then
                        IncassiLegati = IncassiLegati & " - "
                    End If
                    IncassiLegati = IncassiLegati & KReg.NumeroRegistrazione & "  " & KReg.DataRegistrazione & " " & Format(campodbN(RDRLD.Item("Importo")), "#,##0.00")
                Loop
                RDRLD.Close()
            End If

            Saldo = Saldo + TotImp

            Dim myriga As System.Data.DataRow = MyTable.NewRow()

            Dim WrRs As System.Data.DataRow = Stampa.Tables("SituazioneContabile").NewRow

            myriga(0) = Numero
            myriga(1) = NumeroDocumento
            myriga(2) = DataDocumento

            myriga(3) = TipoDocumento

            If TipoCausale = "X" Then
                Dim KS As New Cls_CausaleContabile

                KS.Leggi(Session("DC_TABELLE"), CausaleContabile)

                myriga(3) = KS.Descrizione
            End If


            If Excel Then
                If TotImp > 0 Then

                    myriga(4) = Modulo.MathRound(TotImp, 2)

                    myriga(5) = Modulo.MathRound(TotImp - ImportoLegato, 2)
                    DisponibileSuDocumenti = Modulo.MathRound(DisponibileSuDocumenti + (TotImp - ImportoLegato), 2)
                    myriga(6) = "0,00"
                Else
                    If TipoDocumento <> "INC" Then
                        myriga(4) = Modulo.MathRound(TotImp, 2)
                        myriga(5) = Modulo.MathRound(TotImp - ImportoLegato, 2)
                    End If
                    myriga(6) = Modulo.MathRound(Math.Abs(TotImp), 2)
                End If
                myriga(7) = Modulo.MathRound(ImportoLegato, 2)
                myriga(8) = IncassiLegati
            Else
                If TotImp > 0 Then
                    myriga(4) = Format(TotImp, "#,##0.00")
                    myriga(5) = Format(TotImp - ImportoLegato, "#,##0.00")
                    DisponibileSuDocumenti = DisponibileSuDocumenti + (TotImp - ImportoLegato)
                    myriga(6) = "0,00"
                Else
                    If TipoDocumento <> "INC" Then
                        myriga(4) = Format(TotImp, "#,##0.00")
                        myriga(5) = Format(TotImp - ImportoLegato, "#,##0.00")
                    Else
                        myriga(6) = Format(Math.Abs(TotImp), "#,##0.00")
                    End If

                End If
                myriga(7) = Format(ImportoLegato, "#,##0.00")
                myriga(8) = IncassiLegati
            End If

            myriga(9) = Mov.Descrizione


            WrRs.Item("Registrazione") = myriga(0)
            WrRs.Item("NumeroDocumento") = myriga(1)
            WrRs.Item("DataDocumento") = myriga(2)
            WrRs.Item("TipoDocumento") = myriga(3)
            WrRs.Item("ImportoDovuto") = myriga(4)
            WrRs.Item("ImportoDisponibile") = myriga(5)
            WrRs.Item("ImportoPagato") = myriga(6)
            WrRs.Item("ImportoLegato") = myriga(7)
            WrRs.Item("IncassiLegati") = myriga(8)
            WrRs.Item("Descrizione") = myriga(9)
            WrRs.Item("SaldoIniziale") = SaldoIniziale
            WrRs.Item("DescrizioneIniziale") = ""
            If Val(Txt_Anno.Text) > 0 Then
                WrRs.Item("DescrizioneIniziale") = "Anno : " & Txt_Anno.Text
            End If

            Dim OspN As New ClsOspite

            OspN.CodiceOspite = Session("CODICEOSPITE")
            OspN.Leggi(Session("DC_OSPITE"), OspN.CodiceOspite)

            WrRs.Item("Instestatario") = OspN.Nome
            If Val(DD_Parenti.SelectedValue) > 0 Then
                Dim Par As New Cls_Parenti


                Par.CodiceParente = Val(DD_Parenti.SelectedValue)
                Par.CodiceOspite = Session("CODICEOSPITE")
                Par.Leggi(Session("DC_OSPITE"), Par.CodiceOspite, Par.CodiceParente)

                WrRs.Item("Instestatario") = Par.Nome
            End If



            Stampa.Tables("SituazioneContabile").Rows.Add(WrRs)

            MyTable.Rows.Add(myriga)


        Loop
        RdCC.Close()


        Lbl_Totali.Text = "Saldo " & Format(Saldo, "#,##0.00") & " - Totale documenti aperti " & Format(DisponibileSuDocumenti, "#,##0.00")




        cn.Close()
        If Excel Then
            ExcelGrid.AutoGenerateColumns = True
            ExcelGrid.DataSource = MyTable
            ExcelGrid.Font.Size = 10
            ExcelGrid.DataBind()
        Else
            GridView1.AutoGenerateColumns = True
            GridView1.DataSource = MyTable
            GridView1.Font.Size = 10
            GridView1.DataBind()
        End If

        Session("stampa") = Stampa
    End Sub
    Function campodbN(ByVal oggetto As Object) As Double
        If IsDBNull(oggetto) Then
            Return 0
        Else
            Return oggetto
        End If
    End Function
    Function campodb(ByVal oggetto As Object) As String
        If IsDBNull(oggetto) Then
            Return ""
        Else
            Return oggetto
        End If
    End Function
    Private Sub caricapagina()
        Dim ConnectionString As String = Session("DC_OSPITE")
        Dim x As New Cls_Parenti

        x.UpDateDropBox(ConnectionString, Session("CodiceOspite"), DD_Parenti)

    End Sub
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Trim(Session("UTENTE")) = "" Then
            Response.Redirect("../Login.aspx")
            Exit Sub
        End If

        Call EseguiJS()

        If Page.IsPostBack = True Then
            Exit Sub
        End If


        Dim x As New ClsOspite

        x.CodiceOspite = Session("CODICEOSPITE")
        x.Leggi(Session("DC_OSPITE"), x.CodiceOspite)

        Dim cs As New Cls_CentroServizio

        cs.Leggi(Session("DC_OSPITE"), Session("CODICESERVIZIO"))


        Lbl_NomeOspite.Text = x.Nome & " -  " & x.DataNascita & " - " & cs.DESCRIZIONE & "<i style=""font-size:small;"">(" & x.CodiceOspite & ")</i>"

        Dim Barra As New Cls_BarraSenior

        Lbl_BarraSenior.Text = Barra.CodiceBarra(Application("SENIOR"), Session("UTENTE"), Page)

        Lbl_Utente.Text = Session("UTENTE")

        Call caricapagina()
    End Sub

    Protected Sub Esporta()
  
        Response.Redirect("DocumentiOspiteParenteExcel.aspx")
    End Sub



    Private Sub EseguiJS()
        Dim MyJs As String
        MyJs = "$(document).ready(function() {"
        MyJs = MyJs & "if ($('#MENUDIV').html().length < 50) { ImpostaMenuAgraficaOspiti(); }"
        MyJs = MyJs & "if (window.innerHeight>0) { $(""#BarraLaterale"").css(""height"",(window.innerHeight - 94) + ""px""); } else"
        MyJs = MyJs & "{ $(""#BarraLaterale"").css(""height"",(document.documentElement.offsetHeight - 94) + ""px"");  }"
        MyJs = MyJs & "});"

        'MyJs = "$(document).ready(function() { $('.myClass').keypress(function() { handleEnter($('.myClass').val(), true, true); } );     $('#" & Txt_ImportoPagato.ClientID & "').blur(function() { var ap = formatNumber ($('#" & Txt_ImportoPagato.ClientID & "').val(),2); $('#" & Txt_ImportoPagato.ClientID & "').val(ap); }); });"
        ScriptManager.RegisterStartupScript(Me, Me.GetType(), "visualizzaRitIm", MyJs, True)
    End Sub
    

    Protected Sub ImageButton3_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles ImageButton3.Click
        Call CaricaGriglia(False)
    End Sub

    Protected Sub ImageButton4_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles ImageButton4.Click
        Call CaricaGriglia(True)

        Dim Kx As New ClsOspite

        Kx.Leggi(Session("DC_OSPITE"), Session("CodiceOspite"))

        If MyTable.Rows.Count > 1 Then
            Response.Clear()
            Response.AddHeader("content-disposition", "attachment;filename=" & Kx.Nome.Replace(" ", "") & ".xls")
            Response.Charset = String.Empty
            Response.Cache.SetCacheability(HttpCacheability.NoCache)
            Response.ContentType = "application/vnd.xls"
            Dim stringWrite As New System.IO.StringWriter
            Dim htmlWrite As New HtmlTextWriter(stringWrite)
            'GridView1.RenderControl(htmlWrite)


            form1.Controls.Clear()
            form1.Controls.Add(ExcelGrid)

            form1.RenderControl(htmlWrite)

            Response.Write(stringWrite.ToString())
            Response.End()
        End If
    End Sub

    Protected Sub Btn_Esci_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles Btn_Esci.Click



        If Request.Item("PAGINA") = "" Then
            Response.Redirect("RicercaAnagrafica.aspx?TIPO=DOCOSPPAR")
        Else
            Response.Redirect("RicercaAnagrafica.aspx?CHIAMATA=RITORNO&TIPO=DOCOSPPAR&PAGINA=" & Request.Item("PAGINA"))
        End If
    End Sub

    Protected Sub Ib_Stampa_Click(sender As Object, e As ImageClickEventArgs) Handles Ib_Stampa.Click

        ScriptManager.RegisterClientScriptBlock(Me, Me.GetType(), "Stampa", "openPopUp('Stampa_ReportXSD.aspx?REPORT=SITUAZIONECONTABILE&PRINTERKEY=OFF','Stampe','width=800,height=600');", True)
    End Sub
End Class
