﻿<%@ Page Language="VB" AutoEventWireup="false" Inherits="DocumentiOspiteParente" CodeFile="DocumentiOspiteParente.aspx.vb" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="xasp" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <meta http-equiv="x-ua-compatible" content="IE=9" />
    <title>Documenti Ospite/Parente</title>
    <asp:PlaceHolder runat="server">
        <%: System.Web.Optimization.Styles.Render("~/Content/AjaxControlToolkit/Styles/Bundle") %>
    </asp:PlaceHolder>
    <link href="ospiti.css?ver=11" rel="stylesheet" type="text/css" />
    <link rel="shortcut icon" href="images/SENIOR.ico" />
    <link href="js/jquery.autocomplete.css" rel="stylesheet" type="text/css" />
    <link rel="stylesheet" href="jqueryui/jquery-ui.css" type="text/css">

    <script src="js/jquery-1.5.1.min.js" type="text/javascript"></script>
    <script src="js/jquery.autocomplete.js" type="text/javascript"></script>
    <script src="js/soapclient.js" type="text/javascript"></script>
    <script src="/js/convertinumero.js" type="text/javascript"></script>

    <script src="js/JSErrore.js" type="text/javascript"></script>

    <script src="js/jquery.maskedinput-1.3.min.js" type="text/javascript"></script>
    <script src="/js/formatnumer.js" type="text/javascript"></script>
    <link rel="stylesheet" href="dialogbox/redips-dialog.css" type="text/css" media="screen" />
    <script type="text/javascript" src="dialogbox/redips-dialog-min.js"></script>
    <script type="text/javascript" src="dialogbox/script.js"></script>
    <script type="text/javascript" src="../js/menuanagraficaospiti.js?ver=7"></script>

    <script src="jqueryui/jquery-ui.js" type="text/javascript"></script>
    <script src="jqueryui/datapicker-it.js" type="text/javascript"></script>
    <script type="text/javascript">  
        $(document).ready(function () {
            if (window.innerHeight > 0) { $("#BarraLaterale").css("height", (window.innerHeight - 94) + "px"); } else { $("#BarraLaterale").css("height", (document.documentElement.offsetHeight - 94) + "px"); }
        });

        function DialogBox(Path) {
            alert('s');

            REDIPS.dialog.show(500, 300, '<iframe id="output" src="' + Path + '" height="300px" width="500"></iframe>');
            return false;

        }
        function openPopUp(urlToOpen, nome, parametri) {
            var popup_window = window.open(urlToOpen, '_blank');
            try {
                popup_window.focus();
            } catch (e) {
                alert("E' stato bloccato l'aperutra del popup da parte del browser");
            }
        }
    </script>

</head>
<body>
    <form id="form1" runat="server">
        <asp:ScriptManager ID="ScriptManager1" runat="server" EnableScriptGlobalization="true">
            <Scripts>
                <asp:ScriptReference Path="~/Scripts/AjaxControlToolkit/Bundle" />
            </Scripts>
        </asp:ScriptManager>
        <asp:Label ID="Lbl_BarraSenior" runat="server" Text=""></asp:Label>
        <div align="left">
            <asp:UpdatePanel ID="UpdatePanel1" runat="server">
                <ContentTemplate>
                    <table style="width: 100%;" cellpadding="0" cellspacing="0">
                        <tr>
                            <td style="width: 160px; background-color: #F0F0F0;"></td>
                            <td>
                                <div class="Titolo">Ospiti - Statistiche - Elenco Documenti Ospiti/Parenti</div>
                                <div class="SottoTitoloOSPITE">
                                    <asp:Label ID="Lbl_NomeOspite" runat="server"></asp:Label><br />
                                    <br />
                                </div>
                            </td>
                            <td style="text-align: right;">
                                <div class="DivTastiOspite">
                                    <span class="BenvenutoText">Benvenuto
                                        <asp:Label ID="Lbl_Utente" runat="server"></asp:Label>&nbsp;&nbsp;&nbsp;&nbsp;</span>
                                    <asp:ImageButton ID="ImageButton3" runat="server" Height="38px" ImageUrl="~/images/esegui.png" class="EffettoBottoniTondi" Width="38px" />
                                    <asp:ImageButton ID="ImageButton4" runat="server" Height="38px" ImageUrl="~/images/Excel.png" class="EffettoBottoniTondi" Style="width: 38px" />
                                    <asp:ImageButton ID="Ib_Stampa" runat="server" Height="38px" ImageUrl="~/images/printer-blue.png" class="EffettoBottoniTondi" Width="38px" />
                                </div>
                            </td>
                        </tr>

                        <tr>
                            <td style="width: 160px; background-color: #F0F0F0; vertical-align: top; text-align: center;" id="BarraLaterale">
                                <a href="Menu_Visualizzazioni.aspx" style="border-width: 0px;">
                                    <img src="images/Home.jpg" alt="Menù" class="Effetto" /></a><br />
                                <asp:ImageButton ID="Btn_Esci" runat="server" BackColor="Transparent" ImageUrl="../images/Menu_Indietro.png" class="Effetto" ToolTip="Chiudi" />
                                <br />

                                <div id="MENUDIV"></div>
                                <br />
                                <br />
                                <br />
                                <br />
                                <br />
                                <br />
                                <br />
                                <br />
                                <br />
                                <br />
                                <br />
                                <br />
                            </td>
                            <td colspan="2" style="background-color: #FFFFFF;" valign="top">
                                <xasp:TabContainer ID="TabContainer1" runat="server" ActiveTabIndex="0" CssClass="TabSenior" Width="100%" BorderStyle="None" Style="margin-right: 39px">
                                    <xasp:TabPanel runat="server" HeaderText="Prima Nota" ID="Tab_Anagrafica">
                                        <HeaderTemplate>
                                            Elenco Documenti Ospiti/Parenti
                                        </HeaderTemplate>
                                        <ContentTemplate>
                                            <asp:Label ID="Lbl_Errori" runat="server" ForeColor="Red" Width="392px"></asp:Label><br />
                                            <br />
                                            <asp:DropDownList ID="DD_Parenti" runat="server" Width="264px">
                                            </asp:DropDownList>&nbsp;
                                            &nbsp;
                                            Anno : 
                                            <asp:TextBox ID="Txt_Anno" runat="server" Text="" Width="80px"></asp:TextBox>
                                            <br />
                                            <br />
                                            <asp:Label ID="Lbl_SaldoIniziale" runat="server" Text="" ></asp:Label>                                            
                                            <asp:GridView ID="GridView1" runat="server" CellPadding="3"
                                                Width="100%" BackColor="White" BorderColor="#999999" BorderStyle="None"
                                                BorderWidth="1px" GridLines="Vertical">
                                                <RowStyle ForeColor="#565151" BackColor="#EEEEEE" />
                                                <AlternatingRowStyle BackColor="Gainsboro" />

                                                <FooterStyle BackColor="#CCCCCC" ForeColor="Black" />
                                                <PagerStyle BackColor="#999999" ForeColor="Black" HorizontalAlign="Center" />
                                                <SelectedRowStyle BackColor="#008A8C" Font-Bold="True" ForeColor="White" />
                                                <HeaderStyle BackColor="#565151" Font-Bold="False" Font-Size="Small"
                                                    ForeColor="White" />
                                            </asp:GridView>

                                            <asp:GridView ID="ExcelGrid" runat="server">
                                            </asp:GridView>
                                            <asp:Label ID="Lbl_Totali" runat="server" Text=""></asp:Label>
                                        </ContentTemplate>

                                    </xasp:TabPanel>
                                </xasp:TabContainer>
                            </td>
                        </tr>
                    </table>
                </ContentTemplate>
                <Triggers>
                    <asp:PostBackTrigger ControlID="ImageButton4" />
                </Triggers>
            </asp:UpdatePanel>

            <br />
            <asp:UpdateProgress ID="UpdateProgress2" runat="server"
                AssociatedUpdatePanelID="UpdatePanel1">
                <ProgressTemplate>
                    <div id="blur">&nbsp;</div>

                    <div id="progress" style="width: 200px; height: 50px; left: 40%; position: absolute; top: 372px; text-align: center;">
                        Attendere prego.....<br />
                        &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<img height="30px" src="images/loading.gif">
                    </div>
                </ProgressTemplate>
            </asp:UpdateProgress>
        </div>
    </form>
</body>
</html>
