﻿Imports System.Web.Hosting
Imports System.Data.OleDb

Partial Class movimenti
    Inherits System.Web.UI.Page
    Dim MyTable As New System.Data.DataTable("tabella")
    Dim MyDataSet As New System.Data.DataSet()

    Sub CaricaPagina()
        Dim x As New ClsOspite
        Dim d As New Cls_Movimenti

        Dim ConnectionString As String = Session("DC_OSPITE")
        

        x.Leggi(ConnectionString, Session("CODICEOSPITE"))


        d.loaddati(ConnectionString, Session("CODICEOSPITE"), Session("CODICESERVIZIO"), MyTable, Val(Session("NUMEROMOVIMENTI")))
        Session("Appoggio") = MyTable

        Grd_ImportoComune.AutoGenerateColumns = False
        Grd_ImportoComune.DataSource = MyTable
        Grd_ImportoComune.DataBind()

    End Sub

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Trim(Session("UTENTE")) = "" Then

            Response.Redirect("..\Login.aspx")

            Exit Sub
        End If
        Call EseguiJS()

        If Page.IsPostBack = False Then

            If Val(Request.Item("CodiceOspite")) > 0 And Request.Item("CentroServizio") <> "" Then

                Session("CODICESERVIZIO") = Request.Item("CentroServizio")
                Session("CODICEOSPITE") = Val(Request.Item("CodiceOspite"))
            End If


            ViewState("CODICESERVIZIO") = Session("CODICESERVIZIO")
            ViewState("CODICEOSPITE") = Session("CODICEOSPITE")



            Dim kVilla As New Cls_TabelleDescrittiveOspitiAccessori


            kVilla.UpDateDropBox(Session("DC_OSPITIACCESSORI"), "VIL", DD_Struttura)
            If DD_Struttura.Items.Count = 1 Then
                Call AggiornaCServ()
            End If

            Dim XC_Raggruppamento As New Cls_TabellaRaggruppamento

            XC_Raggruppamento.UpDateDropBox(Session("DC_OSPITE"), DD_RaggruppamentoAcc)
            Dim MyCau As New Cls_CausaliEntrataUscita


            MyCau.UpDateDropBox(Session("DC_OSPITE"), DD_RaggruppamentoAcc.SelectedValue, DD_CausaleAcc)


            Dim K1 As New Cls_SqlString

            Dim Barra As New Cls_BarraSenior

            If Not IsNothing(Session("RicercaAnagraficaSQLString")) Then
                Try
                    K1 = Session("RicercaAnagraficaSQLString")
                Catch ex As Exception

                End Try
            End If

            Lbl_BarraSenior.Text = Barra.CodiceBarra(Application("SENIOR"), Session("UTENTE"), Page, K1)

            Session("NUMEROMOVIMENTI") = 0
            Dim cn As OleDbConnection


            cn = New Data.OleDb.OleDbConnection(Session("DC_OSPITE"))

            cn.Open()
            Dim cmd As New OleDbCommand()
            cmd.CommandText = ("select count(*) as Totale from MOVIMENTI where centroservizio = '" & Session("CODICESERVIZIO") & "' And CodiceOspite = " & Session("CODICEOSPITE"))
            cmd.Connection = cn
            Dim sb As StringBuilder = New StringBuilder

            Dim myPOSTreader As OleDbDataReader = cmd.ExecuteReader()
            If myPOSTreader.Read Then
                Session("NUMEROMOVIMENTI") = myPOSTreader.Item("Totale")
            End If
            myPOSTreader.Close()

            cn.Close()

            Call CaricaPagina()

            Dim x As New ClsOspite

            x.CodiceOspite = Session("CODICEOSPITE")
            x.Leggi(Session("DC_OSPITE"), x.CodiceOspite)

            Dim cs As New Cls_CentroServizio

            cs.Leggi(Session("DC_OSPITE"), Session("CODICESERVIZIO"))

            Lbl_NomeOspite.Text = x.Nome & " -  " & x.DataNascita & " - " & cs.DESCRIZIONE & "<i style=""font-size:small;"">(" & x.CodiceOspite & ")</i>"


        End If
    End Sub



    Protected Sub Modifica()
        Dim xs As New Cls_Movimenti
        Lbl_errori.Text = ""

        Dim X1 As New Cls_Movimenti

        X1.CODICEOSPITE = Session("CODICEOSPITE")
        X1.CENTROSERVIZIO = Session("CODICESERVIZIO")

        X1.EliminaAll(Session("DC_OSPITE"), Val(Session("NUMEROMOVIMENTI")))

        For i = 0 To Grd_ImportoComune.Rows.Count - 1

            Dim TxtData As TextBox = DirectCast(Grd_ImportoComune.Rows(i).FindControl("TxtData"), TextBox)
            Dim DD_TipoMov As DropDownList = DirectCast(Grd_ImportoComune.Rows(i).FindControl("DD_TipoMov"), DropDownList)
            Dim DD_Causale As DropDownList = DirectCast(Grd_ImportoComune.Rows(i).FindControl("DD_Causale"), DropDownList)
            Dim Txt_Descrizione As TextBox = DirectCast(Grd_ImportoComune.Rows(i).FindControl("Txt_Descrizione"), TextBox)
            Dim Txt_Ora As TextBox = DirectCast(Grd_ImportoComune.Rows(i).FindControl("TxtOra"), TextBox)


            Dim X As New Cls_Movimenti

            X.CODICEOSPITE = Session("CODICEOSPITE")
            X.CENTROSERVIZIO = Session("CODICESERVIZIO")
            X.Utente = Session("UTENTE")

            If IsDate(TxtData.Text) Then
                X.Data = TxtData.Text
                X.TipoMov = DD_TipoMov.SelectedValue
                X.Causale = DD_Causale.SelectedValue
                X.Descrizione = Txt_Descrizione.Text
                X.Utente = Session("UTENTE")
                If Txt_Ora.Text.Trim <> "" Then
                    Dim Ore As Integer = 0
                    Dim Minuti As Integer = 0

                    X.DataOra = Nothing
                    If Txt_Ora.Text.IndexOf(":") > 0 Then
                        Ore = Val(Mid(Txt_Ora.Text, 1, Txt_Ora.Text.IndexOf(":")))
                        Minuti = Val(Mid(Txt_Ora.Text, Txt_Ora.Text.IndexOf(":") + 2, Len(Txt_Ora.Text) - Txt_Ora.Text.IndexOf(":")))

                        X.DataOra = Format(DateSerial(Year(X.Data), Month(X.Data), Day(X.Data)), "dd/MM/yyyy") & " " & Format(TimeSerial(Ore, Minuti, 0), "HH:m:ss")
                    End If
                Else
                    X.DataOra = Nothing
                End If

                X.InserisciDB(Session("DC_OSPITE"))

            End If
        Next
    End Sub






    
    Protected Sub Btn_Modifica0_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles Btn_Modifica0.Click
        Session("CODICESERVIZIO") = ViewState("CODICESERVIZIO")
        Session("CODICEOSPITE") = ViewState("CODICEOSPITE")
        If Val(Session("CODICEOSPITE")) = 0 Then
            ClientScript.RegisterClientScriptBlock(Me.GetType(), "Errore", "VisualizzaErrore('Devi ricercare l ospite');", True)
            Exit Sub
        End If


        Dim Log As New Cls_LogPrivacy
        Dim ConvT As New Cls_DataTableToJson
        Dim OldTable As New System.Data.DataTable("tabellaOld")


        Dim OldDatiPar As New Cls_Movimenti

        OldDatiPar.loaddati(Session("DC_OSPITE"), Session("CODICEOSPITE"), Session("CODICESERVIZIO"), OldTable)
        Dim AppoggioJS As String = ConvT.DataTableToJsonObj(OldTable)

        Log.LogPrivacy(Application("SENIOR"), Session("CLIENTE"), Session("UTENTE"), Session("UTENTEIMPER"), Page.GetType.Name.ToUpper, Session("CODICEOSPITE"), 0, "", "", 0, "", "M", "MOVIMENTI", AppoggioJS)


        Dim i As Integer



        Dim UltimoMov As String = ""
        Dim UltimaData As Date



        For i = 0 To Grd_ImportoComune.Rows.Count - 1
            Dim TxtData As TextBox = DirectCast(Grd_ImportoComune.Rows(i).FindControl("TxtData"), TextBox)
            Dim DD_TipoMov As DropDownList = DirectCast(Grd_ImportoComune.Rows(i).FindControl("DD_TipoMov"), DropDownList)
            Dim DD_Causale As DropDownList = DirectCast(Grd_ImportoComune.Rows(i).FindControl("DD_Causale"), DropDownList)
            Dim Txt_Descrizione As TextBox = DirectCast(Grd_ImportoComune.Rows(i).FindControl("Txt_Descrizione"), TextBox)


            If TxtData.Text = "" And i = Grd_ImportoComune.Rows.Count - 1 Then
            Else
                If Not IsDate(TxtData.Text) Then
                    ClientScript.RegisterClientScriptBlock(Me.GetType(), "Errore", "VisualizzaErrore('Data riga " & i + 1 & " formalmente errata');", True)
                    Exit Sub
                End If
            End If

            Dim AppoggioData As Date
            If IsDate(TxtData.Text) Then
                AppoggioData = TxtData.Text
            Else
                AppoggioData = Now
            End If



            If Session("ABILITAZIONI").IndexOf("<PORTINERIA>") > 0 Then
                If IsDate(TxtData.Text) And TxtData.Enabled = True Then
                    If DateDiff(DateInterval.Day, AppoggioData, Now) > 60 Then
                        ClientScript.RegisterClientScriptBlock(Me.GetType(), "Errore", "VisualizzaErrore('Non puoi inserire date precedenti di 60 giorni');", True)
                        Exit Sub
                    End If
                End If
            End If

            If DD_TipoMov.SelectedValue = "03" And DD_Causale.SelectedValue = "" Then
                ClientScript.RegisterClientScriptBlock(Me.GetType(), "Errore", "VisualizzaErrore('Specifica causale per riga " & i + 1 & "');", True)
                Exit Sub
            End If

            If Session("NUMEROMOVIMENTI") < 101 Then
                If i = 0 And DD_TipoMov.SelectedValue <> "05" Then
                    ClientScript.RegisterClientScriptBlock(Me.GetType(), "Errore", "VisualizzaErrore('Il primo movimento deve essere un Accoglimento');", True)
                    Exit Sub
                End If
            End If



            For x = 0 To Grd_ImportoComune.Rows.Count - 1
                If x <> i Then
                    Dim TxtDatax As TextBox = DirectCast(Grd_ImportoComune.Rows(x).FindControl("TxtData"), TextBox)

                    If TxtDatax.Text = TxtData.Text Then
                        'ClientScript.RegisterClientScriptBlock(Me.GetType(), "Errore", "VisualizzaErrore('Data duplicata per riga " & i + 1 & " e riga " & x + 1 & "');", True)
                        'Exit Sub
                    End If
                End If
            Next
            If Format(UltimaData, "yyyyMMdd") > Format(AppoggioData, "yyyyMMdd") Then
                REM ClientScript.RegisterClientScriptBlock(Me.GetType(), "Errore", "VisualizzaErrore('Data precedente maggiore della nuova data');", True)
                REM Exit Sub
            End If
            If TxtData.Text <> "" Then
                UltimaData = TxtData.Text

                If UltimoMov <> "" Then
                    If UltimoMov = "13" And DD_TipoMov.SelectedValue <> "05" Then
                        REM ClientScript.RegisterClientScriptBlock(Me.GetType(), "Errore", "VisualizzaErrore('Errore dopo un uscita definitiva, solo un accoglimento');", True)
                        REM Exit Sub
                    End If
                    If UltimoMov = "04" And DD_TipoMov.SelectedValue = "04" Then
                        ClientScript.RegisterClientScriptBlock(Me.GetType(), "Errore", "VisualizzaErrore('Non vi possono essere due uscite in sequenza');", True)
                        Exit Sub
                    End If
                    If UltimoMov = "03" And DD_TipoMov.SelectedValue = "03" Then
                        ClientScript.RegisterClientScriptBlock(Me.GetType(), "Errore", "VisualizzaErrore('Non vi possono essere due uscite in sequenza');", True)
                        Exit Sub
                    End If
                    If UltimoMov = "05" And DD_TipoMov.SelectedValue = "05" Then
                        ClientScript.RegisterClientScriptBlock(Me.GetType(), "Errore", "VisualizzaErrore('Non vi possono essere due uscite in sequenza');", True)
                        Exit Sub
                    End If
                    If UltimoMov = "13" And DD_TipoMov.SelectedValue = "13" Then
                        ClientScript.RegisterClientScriptBlock(Me.GetType(), "Errore", "VisualizzaErrore('Non vi possono essere due uscite in sequenza');", True)
                        Exit Sub
                    End If

                    If UltimoMov = "13" And DD_TipoMov.SelectedValue <> "05" Then
                        ClientScript.RegisterClientScriptBlock(Me.GetType(), "Errore", "VisualizzaErrore('Dopo un uscita definitva può solo esserci un accoglimento');", True)
                        Exit Sub
                    End If
                    If UltimoMov <> "13" And DD_TipoMov.SelectedValue = "05" Then
                        ClientScript.RegisterClientScriptBlock(Me.GetType(), "Errore", "VisualizzaErrore('Accoglimento non inseribile solo dopo un uscita definitiva');", True)
                        Exit Sub
                    End If
                End If

                UltimoMov = DD_TipoMov.SelectedValue
            End If

        Next


        Call Modifica()

        

        i = Grd_ImportoComune.Rows.Count - 1
        Dim AData As TextBox = DirectCast(Grd_ImportoComune.Rows(i).FindControl("TxtData"), TextBox)
        Dim ADD_TipoMov As DropDownList = DirectCast(Grd_ImportoComune.Rows(i).FindControl("DD_TipoMov"), DropDownList)
        Dim ADD_Causale As DropDownList = DirectCast(Grd_ImportoComune.Rows(i).FindControl("DD_Causale"), DropDownList)
        Dim ATxt_Descrizione As TextBox = DirectCast(Grd_ImportoComune.Rows(i).FindControl("Txt_Descrizione"), TextBox)

        If ADD_TipoMov.SelectedValue = 13 Then

            Dim K As New Cls_MovimentiStanze


            K.CodiceOspite = Session("CODICEOSPITE")
            K.CentroServizio = Session("CODICESERVIZIO")
            K.UltimoMovimentoOspiteData(Session("DC_OSPITIACCESSORI"), AData.Text)

            If K.Tipologia = "OC" Then

                ScriptManager.RegisterClientScriptBlock(Me, Me.GetType(), "UD", " ApriLiberazione();", True)
            End If
        End If
        If ADD_TipoMov.SelectedValue = 13 Then
            Dim x As New ClsOspite
            Dim ConnectionString As String = Session("DC_OSPITE")
            Dim ConnectionStringTabelle As String = Session("DC_TABELLE")
            Dim ConnectionStringGenerale As String = Session("DC_GENERALE")

            x.Leggi(ConnectionString, Session("CODICEOSPITE"))


            Dim KCs As New Cls_DatiOspiteParenteCentroServizio

            KCs.CentroServizio = Session("CODICESERVIZIO")
            KCs.CodiceOspite = Session("CODICEOSPITE")
            KCs.CodiceParente = 0
            KCs.Leggi(ConnectionString)

            REM Assegna la tipologia operazione e l'aliquota prendendo quella relativa al cserv
            If KCs.CodiceOspite <> 0 Then
                x.TIPOOPERAZIONE = KCs.TipoOperazione
                x.CODICEIVA = KCs.AliquotaIva
                x.FattAnticipata = KCs.Anticipata
                x.MODALITAPAGAMENTO = KCs.ModalitaPagamento
                x.Compensazione = KCs.Compensazione
                x.SETTIMANA = KCs.Settimana
            End If

            Dim TipoOperazione As New Cls_TipoOperazione

            TipoOperazione.Leggi(ConnectionString, x.TIPOOPERAZIONE)
  

        End If


        Call EseguiJS()

        Dim MyJs As String
        MyJs = "alert('Modifica Effettuata');"
        ScriptManager.RegisterClientScriptBlock(Me, Me.GetType(), "ModScM", MyJs, True)
    End Sub

    




    Protected Sub Grd_ImportoComune_RowDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs) Handles Grd_ImportoComune.RowDataBound
        If e.Row.RowType = DataControlRowType.DataRow Then


            Dim TxtData As TextBox = DirectCast(e.Row.FindControl("TxtData"), TextBox)


            TxtData.Text = MyTable.Rows(e.Row.RowIndex).Item(0).ToString

            Dim AppoggioData As Date
            If IsDate(TxtData.Text) Then
                AppoggioData = TxtData.Text
            Else
                AppoggioData = Now
            End If


            If Session("ABILITAZIONI").IndexOf("<PORTINERIA>") > 0 Then
                If IsDate(TxtData.Text) Then
                    If DateDiff(DateInterval.Day, AppoggioData, Now) > 60 Then
                        TxtData.Enabled = False
                    End If
                End If
            End If

            Dim DD_TipoMov As DropDownList = DirectCast(e.Row.FindControl("DD_TipoMov"), DropDownList)


            DD_TipoMov.SelectedValue = MyTable.Rows(e.Row.RowIndex).Item(1).ToString


            Dim ConnectionString As String = Session("DC_OSPITE")

            Dim MyCau As New Cls_CausaliEntrataUscita

            MyCau.Codice = MyTable.Rows(e.Row.RowIndex).Item(2).ToString()
            MyCau.LeggiCausale(ConnectionString)




            If Session("ABILITAZIONI").IndexOf("<PORTINERIA>") > 0 Then
                If IsDate(TxtData.Text) Then
                    If DateDiff(DateInterval.Day, AppoggioData, Now) > 60 Then
                        DD_TipoMov.Enabled = False
                    End If
                End If
            End If


            Dim DD_Raggruppamento As DropDownList = DirectCast(e.Row.FindControl("DD_Raggruppamento"), DropDownList)

            Dim XC_Raggruppamento As New Cls_TabellaRaggruppamento

            XC_Raggruppamento.UpDateDropBox(ConnectionString, DD_Raggruppamento)

            DD_Raggruppamento.SelectedValue = MyCau.Raggruppamento


            If Session("ABILITAZIONI").IndexOf("<PORTINERIA>") > 0 Then
                If IsDate(TxtData.Text) Then
                    If DateDiff(DateInterval.Day, AppoggioData, Now) > 60 Then
                        DD_Raggruppamento.Enabled = False
                    End If
                End If
            End If

            Dim DD_Causale As DropDownList = DirectCast(e.Row.FindControl("DD_Causale"), DropDownList)

            MyCau.UpDateDropBox(ConnectionString, MyCau.Raggruppamento, DD_Causale)

            DD_Causale.SelectedValue = MyCau.Codice

            If Session("ABILITAZIONI").IndexOf("<PORTINERIA>") > 0 Then
                If IsDate(TxtData.Text) Then
                    If DateDiff(DateInterval.Day, AppoggioData, Now) > 60 Then
                        DD_Causale.Enabled = False
                    End If
                End If
            End If

            Dim TxtDescrizione As TextBox = DirectCast(e.Row.FindControl("Txt_Descrizione"), TextBox)

            TxtDescrizione.Text = MyTable.Rows(e.Row.RowIndex).Item(3).ToString

            If Session("ABILITAZIONI").IndexOf("<PORTINERIA>") > 0 Then
                If IsDate(TxtData.Text) Then
                    If DateDiff(DateInterval.Day, AppoggioData, Now) > 60 Then
                        TxtDescrizione.Enabled = False
                    End If
                End If
            End If


            Dim TxtOra As TextBox = DirectCast(e.Row.FindControl("TxtOra"), TextBox)

            If IsDate(MyTable.Rows(e.Row.RowIndex).Item(5)) Then
                TxtOra.Text = Format(MyTable.Rows(e.Row.RowIndex).Item(5), "HH:mm")
            End If

            If Session("ABILITAZIONI").IndexOf("<PORTINERIA>") > 0 Then
                If IsDate(TxtData.Text) Then
                    If DateDiff(DateInterval.Day, AppoggioData, Now) > 60 Then
                        TxtOra.Enabled = False
                    End If
                End If
            End If


            Dim Param As New Cls_Parametri

            Param.LeggiParametri(Session("DC_OSPITE"))

            If Param.OraSuMovimenti = 0 Then
                Grd_ImportoComune.Columns(6).Visible = False

            End If

            Call EseguiJS()

        End If
    End Sub



    Protected Sub Grd_ImportoComune_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles Grd_ImportoComune.SelectedIndexChanged

    End Sub

    Protected Sub DD_TipoMov_SelectedIndexChanged(ByVal sender As Object, ByVal e As EventArgs)
        Dim I As Integer
        Dim riga As Integer = 0

        For I = 0 To Grd_ImportoComune.Rows.Count - 1
            If sender.clientid = Grd_ImportoComune.Rows(I).Cells(2).Controls.Item(1).ClientID Then
                riga = I
            End If
        Next


        Dim DD_TipoMov As DropDownList = DirectCast(Grd_ImportoComune.Rows(riga).FindControl("DD_TipoMov"), DropDownList)

        Dim DD_Raggruppamento As DropDownList = DirectCast(Grd_ImportoComune.Rows(riga).Cells(2).FindControl("DD_Raggruppamento"), DropDownList)

        Dim DD_Causale As DropDownList = DirectCast(Grd_ImportoComune.Rows(riga).Cells(3).FindControl("DD_Causale"), DropDownList)

        If DD_TipoMov.SelectedValue = "04" Then
            DD_Raggruppamento.Enabled = False
            DD_Causale.Enabled = False
            DD_Raggruppamento.SelectedValue = "99"
            DD_Causale.SelectedValue = ""
        Else
            DD_Raggruppamento.Enabled = True
            DD_Causale.Enabled = True
        End If


    End Sub


    Protected Sub DD_Raggruppamento_SelectedIndexChanged(ByVal sender As Object, ByVal e As EventArgs)


        Dim I As Integer
        Dim riga As Integer = 0

        For I = 0 To Grd_ImportoComune.Rows.Count - 1
            If sender.clientid = Grd_ImportoComune.Rows(I).Cells(3).Controls.Item(1).ClientID Then
                riga = I
            End If
        Next


        Dim ConnectionString As String = Session("DC_OSPITE")

        Dim MyCau As New Cls_CausaliEntrataUscita

        Dim DD_Raggruppamento As DropDownList = DirectCast(Grd_ImportoComune.Rows(riga).Cells(2).FindControl("DD_Raggruppamento"), DropDownList)

        Dim DD_Causale As DropDownList = DirectCast(Grd_ImportoComune.Rows(riga).Cells(3).FindControl("DD_Causale"), DropDownList)

        MyCau.UpDateDropBox(ConnectionString, DD_Raggruppamento.SelectedValue, DD_Causale)
    End Sub

    Private Sub EseguiJS()
        Dim MyJs As String
        MyJs = "$(document).ready(function() {"

        MyJs = MyJs & "var els = document.getElementsByTagName(""*"");"

        MyJs = MyJs & "for (var i=0;i<els.length;i++)"
        MyJs = MyJs & "if ( els[i].id ) { "
        MyJs = MyJs & " var appoggio =els[i].id; "
        MyJs = MyJs & " if (appoggio.match('TxtData')!= null) {  "
        MyJs = MyJs & " $(els[i]).mask(""99/99/9999"");"
        MyJs = MyJs & " $(els[i]).datepicker({ changeMonth: true,changeYear: true,  yearRange: ""1990:" & Year(Now) + 5 & """},$.datepicker.regional[""it""]);"
        MyJs = MyJs & "    }"

        MyJs = MyJs & " if ((appoggio.match('TxtImporto')!= null) ) {  "
        MyJs = MyJs & " $(els[i]).keypress(function() { ForceNumericInput($(this).val(), true, true); } ); "
        MyJs = MyJs & " $(els[i]).blur(function() { var ap = formatNumber($(this).val(),2); $(this).val(ap); });"
        MyJs = MyJs & "    }"

        MyJs = MyJs & " if (appoggio.match('TxtOra')!= null) {  "
        MyJs = MyJs & " $(els[i]).mask(""99:99"");"
        MyJs = MyJs & "    }"

        MyJs = MyJs & "} "
        MyJs = MyJs & "});"

        'MyJs = "$(document).ready(function() { $('.myClass').keypress(function() { handleEnter($('.myClass').val(), true, true); } );     $('#" & Txt_ImportoPagato.ClientID & "').blur(function() { var ap = formatNumber ($('#" & Txt_ImportoPagato.ClientID & "').val(),2); $('#" & Txt_ImportoPagato.ClientID & "').val(ap); }); });"
        ScriptManager.RegisterStartupScript(Me, Me.GetType(), "visualizzaRitIm", MyJs, True)

    End Sub

    Private Sub InserisciRiga()
        UpDateTable()
        MyTable = ViewState("App_Retta")
        Dim myriga As System.Data.DataRow = MyTable.NewRow()
        myriga(0) = ""
        myriga(1) = 0

        MyTable.Rows.Add(myriga)

        ViewState("App_Retta") = MyTable
        Grd_ImportoComune.AutoGenerateColumns = False

        Grd_ImportoComune.DataSource = MyTable

        Grd_ImportoComune.DataBind()

        Dim TxtData As TextBox = DirectCast(Grd_ImportoComune.Rows(Grd_ImportoComune.Rows.Count - 1).FindControl("TxtData"), TextBox)

        TxtData.Focus()

        ClientScript.RegisterClientScriptBlock(Me.GetType(), "setFocus", "$(document).ready(function() {  setTimeout(function(){ document.getElementById('" + TxtData.ClientID + "').focus(); }); }, 500);", True)
    End Sub

    Protected Sub Grd_ImportoComune_RowCommand(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewCommandEventArgs) Handles Grd_ImportoComune.RowCommand
        If (e.CommandName = "Inserisci") Then
            Call InserisciRiga()

            Call EseguiJS()
        End If
    End Sub


    Private Sub UpDateTable()
        Dim i As Integer


        MyTable.Clear()
        MyTable.Columns.Clear()
        MyTable.Columns.Add("Data", GetType(String))
        MyTable.Columns.Add("Tipo", GetType(String))
        MyTable.Columns.Add("Causale", GetType(String))
        MyTable.Columns.Add("Descrizione", GetType(String))
        MyTable.Columns.Add("PROGRESSIVO", GetType(Long))
        MyTable.Columns.Add("Ora", GetType(Date))


        For i = 0 To Grd_ImportoComune.Rows.Count - 1

            Dim TxtData As TextBox = DirectCast(Grd_ImportoComune.Rows(i).FindControl("TxtData"), TextBox)
            Dim DD_TipoMov As DropDownList = DirectCast(Grd_ImportoComune.Rows(i).FindControl("DD_TipoMov"), DropDownList)
            Dim DD_Causale As DropDownList = DirectCast(Grd_ImportoComune.Rows(i).FindControl("DD_Causale"), DropDownList)
            Dim Txt_Descrizione As TextBox = DirectCast(Grd_ImportoComune.Rows(i).FindControl("Txt_Descrizione"), TextBox)
            Dim TxtOra As TextBox = DirectCast(Grd_ImportoComune.Rows(i).FindControl("TxtOra"), TextBox)

            Dim myrigaR As System.Data.DataRow = MyTable.NewRow()


            myrigaR(0) = TxtData.Text
            myrigaR(1) = DD_TipoMov.SelectedValue
            myrigaR(2) = DD_Causale.SelectedValue
            myrigaR(3) = Txt_Descrizione.Text

            myrigaR(5) = System.DBNull.Value

            If TxtOra.Text.IndexOf(":") > 0 Then
                Dim Ore As Integer = 0
                Dim Minuti As Integer = 0

                Ore = Val(Mid(TxtOra.Text, 1, TxtOra.Text.IndexOf(":")))
                Minuti = Val(Mid(TxtOra.Text, TxtOra.Text.IndexOf(":") + 2, Len(TxtOra.Text) - TxtOra.Text.IndexOf(":")))

                myrigaR(5) = Format(DateSerial(Year(Now), Month(Now), Day(Now)), "dd/MM/yyyy") & " " & Format(TimeSerial(Ore, Minuti, 0), "HH:m:ss")
            End If




            MyTable.Rows.Add(myrigaR)
        Next
        ViewState("App_Retta") = MyTable

    End Sub

    Protected Sub Grd_ImportoComune_RowDeleting(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewDeleteEventArgs) Handles Grd_ImportoComune.RowDeleting
        UpDateTable()
        MyTable = ViewState("App_Retta")
        Try
            If Request.UserAgent.Contains("Chrome") Then
                If IsDBNull(Session("RI_TIMER")) Or DateDiff("s", Session("RI_TIMER"), Now) > 1 Then
                    MyTable.Rows.RemoveAt(e.RowIndex)
                    If MyTable.Rows.Count = 0 Then
                        Dim myriga As System.Data.DataRow = MyTable.NewRow()
                        myriga(1) = 0
                        myriga(2) = 0
                        MyTable.Rows.Add(myriga)
                    End If
                    Session("RI_TIMER") = Now
                End If
            Else
                MyTable.Rows.RemoveAt(e.RowIndex)
                If MyTable.Rows.Count = 0 Then
                    Dim myriga As System.Data.DataRow = MyTable.NewRow()
                    myriga(1) = 0
                    myriga(2) = 0
                    MyTable.Rows.Add(myriga)
                End If
            End If
        Catch ex As Exception

        End Try


        ViewState("App_Retta") = MyTable

        Grd_ImportoComune.AutoGenerateColumns = False

        Grd_ImportoComune.DataSource = MyTable

        Grd_ImportoComune.DataBind()
        Call EseguiJS()

        e.Cancel = True
    End Sub

    Protected Sub Btn_Esci_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles Btn_Esci.Click
        If Request.Item("PAGINA") = "" Then
            Response.Redirect("RicercaAnagrafica.aspx?TIPO=MOVIMENTI")
        Else
            Response.Redirect("RicercaAnagrafica.aspx?TIPO=MOVIMENTI&CHIAMATA=RITORNO&PAGINA=" & Request.Item("PAGINA"))
        End If

    End Sub


    Protected Sub Btn_Esci_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Btn_Esci.Load

    End Sub

    Protected Sub BTN_InserisciRiga_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles BTN_InserisciRiga.Click
        Call InserisciRiga()

        Call EseguiJS()
    End Sub

    Protected Sub DD_RaggruppamentoAcc_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles DD_RaggruppamentoAcc.SelectedIndexChanged
        Dim ConnectionString As String = Session("DC_OSPITE")

        Dim MyCau As New Cls_CausaliEntrataUscita


        MyCau.UpDateDropBox(ConnectionString, DD_RaggruppamentoAcc.SelectedValue, DD_CausaleAcc)



        ScriptManager.RegisterStartupScript(Me, Me.GetType(), "Scheramta", "VisualizzaDivRegistrazione();", True)
        Call EseguiJS()
    End Sub

    Protected Sub Img_CreaAccoglimento_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles Img_CreaAccoglimento.Click
        If Not IsDate(Txt_DataAcco.Text) Then
            ClientScript.RegisterClientScriptBlock(Me.GetType(), "Errore", "VisualizzaErrore('Data Non indicata');", True)
            ScriptManager.RegisterStartupScript(Me, Me.GetType(), "Scheramta", "VisualizzaDivRegistrazione();", True)
            Call EseguiJS()
            Exit Sub
        End If

        If DD_CServ.SelectedValue = "" Then
            ClientScript.RegisterClientScriptBlock(Me.GetType(), "Errore", "VisualizzaErrore('Centro Servizio non indicato');", True)
            ScriptManager.RegisterStartupScript(Me, Me.GetType(), "Scheramta", "VisualizzaDivRegistrazione();", True)
            Call EseguiJS()
            Exit Sub
        End If

        If DD_CServ.SelectedValue = Session("CODICESERVIZIO") Then
            ClientScript.RegisterClientScriptBlock(Me.GetType(), "Errore", "VisualizzaErrore('Centro Servizio già presente per questo ospite');", True)
            ScriptManager.RegisterStartupScript(Me, Me.GetType(), "Scheramta", "VisualizzaDivRegistrazione();", True)
            Call EseguiJS()
            Exit Sub
        End If

        Dim K As New Cls_Movimenti
        Dim DattaAppo As Date = Txt_DataAcco.Text

        K.CodiceOspite = Session("CODICEOSPITE")
        K.CENTROSERVIZIO = DD_CServ.SelectedValue
        K.TipoMov = "05"
        K.Data = DattaAppo
        K.Causale = DD_CausaleAcc.SelectedValue

        If K.CServizioUsato(Session("DC_OSPITE")) Then
            ClientScript.RegisterClientScriptBlock(Me.GetType(), "Errore", "VisualizzaErrore('Centro Servizio già presente per questo ospite');", True)
            ScriptManager.RegisterStartupScript(Me, Me.GetType(), "Scheramta", "VisualizzaDivRegistrazione();", True)
            Call EseguiJS()
            Exit Sub
        End If

        K.AggiornaDB(Session("DC_OSPITE"))

        Dim Cserv As New Cls_CentroServizio

        Cserv.CENTROSERVIZIO = DD_CServ.SelectedValue
        Cserv.Leggi(Session("DC_OSPITE"), DD_CServ.SelectedValue)
        Dim s As New Cls_Pianodeiconti

        s.Descrizione = ""
        s.Mastro = Cserv.MASTRO
        s.Conto = Cserv.CONTO
        s.Sottoconto = Session("CODICEOSPITE") * 100
        s.Decodfica(Session("DC_GENERALE"))
        If s.Descrizione = "" Then
            Dim xOsp As New ClsOspite

            xOsp.CodiceOspite = Session("CODICEOSPITE")
            xOsp.Leggi(Session("DC_OSPITE"), Session("CODICEOSPITE"))
            s.Descrizione = xOsp.Nome
            s.Tipo = "A"
            s.TipoAnagrafica = "O"
            s.Scrivi(Session("DC_GENERALE"))
        End If

        Dim KPar As New Cls_Parametri

        KPar.LeggiParametri(Session("DC_OSPITE"))

        If KPar.MastroAnticipo > 0 Then
            s.Descrizione = ""
            s.Mastro = KPar.MastroAnticipo
            s.Conto = Cserv.CONTO
            s.Sottoconto = Session("CODICEOSPITE") * 100
            s.Decodfica(Session("DC_GENERALE"))
            If s.Descrizione = "" Then
                Dim xOsp As New ClsOspite

                xOsp.CodiceOspite = Session("CODICEOSPITE")
                xOsp.Leggi(Session("DC_OSPITE"), Session("CODICEOSPITE"))
                s.Descrizione = xOsp.Nome
                s.Tipo = "P"
                s.TipoAnagrafica = "O"
                s.Scrivi(Session("DC_GENERALE"))
            End If
        End If

        Dim Par As Integer
        Dim OldCserv As New Cls_CentroServizio

        OldCserv.CENTROSERVIZIO = Session("CODICESERVIZIO")
        OldCserv.Leggi(Session("DC_OSPITE"), Session("CODICESERVIZIO"))

        For Par = 1 To 10
            Dim sPar As New Cls_Pianodeiconti

            sPar.Descrizione = ""
            sPar.Mastro = OldCserv.MASTRO
            sPar.Conto = OldCserv.CONTO
            sPar.Sottoconto = (Session("CODICEOSPITE") * 100) + Par
            sPar.Decodfica(Session("DC_GENERALE"))
            If sPar.Descrizione <> "" Then
                sPar.Descrizione = ""
                sPar.Mastro = Cserv.MASTRO
                sPar.Conto = Cserv.CONTO
                sPar.Sottoconto = (Session("CODICEOSPITE") * 100) + Par
                sPar.Decodfica(Session("DC_GENERALE"))
                If sPar.Descrizione = "" Then
                    Dim xOsp As New Cls_Parenti


                    xOsp.CodiceOspite = Session("CODICEOSPITE")
                    xOsp.Leggi(Session("DC_OSPITE"), Session("CODICEOSPITE"), Par)
                    sPar.Tipo = "A"
                    sPar.TipoAnagrafica = "P"
                    sPar.Descrizione = xOsp.Nome
                    sPar.Scrivi(Session("DC_GENERALE"))
                End If
                If KPar.MastroAnticipo > 0 Then
                    s.Descrizione = ""
                    s.Mastro = KPar.MastroAnticipo
                    s.Conto = Cserv.CONTO
                    s.Sottoconto = Session("CODICEOSPITE") * 100 + Par
                    s.Decodfica(Session("DC_GENERALE"))
                    If s.Descrizione = "" Then
                        Dim xOsp As New Cls_Parenti

                        xOsp.CodiceOspite = Session("CODICEOSPITE")
                        xOsp.Leggi(Session("DC_OSPITE"), Session("CODICEOSPITE"), Par)
                        s.Descrizione = xOsp.Nome
                        s.Tipo = "A"
                        s.TipoAnagrafica = "P"
                        s.Scrivi(Session("DC_GENERALE"))
                    End If
                End If

            End If
        Next




        If Chk_RiportaImporti.Checked = True Then
            Dim kRt As New Cls_rettatotale

            kRt.UltimaData(Session("DC_OSPITE"), Session("CODICEOSPITE"), Session("CODICESERVIZIO"))
            If Year(kRt.Data) > 1900 Then
                kRt.CENTROSERVIZIO = DD_CServ.SelectedValue
                kRt.CODICEOSPITE = Session("CODICEOSPITE")
                kRt.AggiornaDB(Session("DC_OSPITE"))
            End If

            Dim kRO As New Cls_ImportoOspite

            kRO.UltimaData(Session("DC_OSPITE"), Session("CODICEOSPITE"), Session("CODICESERVIZIO"))
            If Year(kRO.Data) > 1900 Then
                kRO.CODICEOSPITE = Session("CODICEOSPITE")
                kRO.CENTROSERVIZIO = DD_CServ.SelectedValue
                kRO.AggiornaDB(Session("DC_OSPITE"))
            End If

            Dim kMO As New Cls_Modalita

            kMO.UltimaData(Session("DC_OSPITE"), Session("CODICEOSPITE"), Session("CODICESERVIZIO"))
            If Year(kMO.Data) > 1900 Then
                kMO.CodiceOspite = Session("CODICEOSPITE")
                kMO.CentroServizio = DD_CServ.SelectedValue
                kMO.AggiornaDB(Session("DC_OSPITE"))
            End If

            Dim kRA As New Cls_StatoAuto

            kRA.UltimaData(Session("DC_OSPITE"), Session("CODICEOSPITE"), Session("CODICESERVIZIO"))
            If Year(kRA.Data) > 1900 Then
                kRA.CODICEOSPITE = Session("CODICEOSPITE")
                kRA.CENTROSERVIZIO = DD_CServ.SelectedValue
                kRA.AggiornaDB(Session("DC_OSPITE"))
            End If

            Dim kRC As New Cls_ImportoComune

            kRC.UltimaData(Session("DC_OSPITE"), Session("CODICEOSPITE"), Session("CODICESERVIZIO"))
            If Year(kRC.Data) > 1900 Then
                kRC.CODICEOSPITE = Session("CODICEOSPITE")
                kRC.CENTROSERVIZIO = DD_CServ.SelectedValue
                kRC.AggiornaDB(Session("DC_OSPITE"))
            End If


            Dim kRja As New Cls_ImportoJolly

            kRja.UltimaData(Session("DC_OSPITE"), Session("CODICEOSPITE"), Session("CODICESERVIZIO"))
            If Year(kRja.Data) > 1900 Then
                kRja.CODICEOSPITE = Session("CODICEOSPITE")
                kRja.CENTROSERVIZIO = DD_CServ.SelectedValue
                kRja.AggiornaDB(Session("DC_OSPITE"))
            End If

            Dim kPAR1 As New Cls_ImportoParente

            kPAR1.UltimaData(Session("DC_OSPITE"), Session("CODICEOSPITE"), Session("CODICESERVIZIO"), 1)
            If Year(kPAR1.DATA) > 1900 Then
                kPAR1.CODICEOSPITE = Session("CODICEOSPITE")
                kPAR1.CENTROSERVIZIO = DD_CServ.SelectedValue
                kPAR1.CODICEPARENTE = 1
                kPAR1.AggiornaDB(Session("DC_OSPITE"))
            End If
            Dim kPAR2 As New Cls_ImportoParente

            kPAR2.UltimaData(Session("DC_OSPITE"), Session("CODICEOSPITE"), Session("CODICESERVIZIO"), 2)
            If Year(kPAR2.DATA) > 1900 Then
                kPAR2.CODICEOSPITE = Session("CODICEOSPITE")
                kPAR2.CENTROSERVIZIO = DD_CServ.SelectedValue
                kPAR1.CODICEPARENTE = 2
                kPAR2.AggiornaDB(Session("DC_OSPITE"))
            End If

            Dim kPAR3 As New Cls_ImportoParente

            kPAR3.UltimaData(Session("DC_OSPITE"), Session("CODICEOSPITE"), Session("CODICESERVIZIO"), 3)
            If Year(kPAR3.DATA) > 1900 Then
                kPAR3.CODICEOSPITE = Session("CODICEOSPITE")
                kPAR3.CENTROSERVIZIO = DD_CServ.SelectedValue
                kPAR1.CODICEPARENTE = 3
                kPAR3.AggiornaDB(Session("DC_OSPITE"))
            End If
        End If


        If Cserv.IncrementaNumeroCartella = 1 Then
            Dim Ospite As New ClsOspite

            Ospite.CodiceOspite = Session("CODICEOSPITE")
            Ospite.CodiceCentroServizio = Cserv.CENTROSERVIZIO
            Ospite.Leggi(Session("DC_OSPITE"), Ospite.CodiceOspite)

            Ospite.CodiceCentroServizio = Cserv.CENTROSERVIZIO
            Ospite.CartellaClinica = Ospite.MaxTesseraSanitaria(Session("DC_OSPITE"))

            Ospite.ScriviOspite(Session("DC_OSPITE"))
        End If

        If Request.Item("PAGINA") = "" Then
            Response.Redirect("RicercaAnagrafica.aspx?TIPO=MOVIMENTI")
        Else
            Response.Redirect("RicercaAnagrafica.aspx?TIPO=MOVIMENTI&CHIAMATA=RITORNO&PAGINA=" & Request.Item("PAGINA"))
        End If
    End Sub


    Private Sub AggiornaCServ()
        Dim kCsrv As New Cls_CentroServizio

        If DD_Struttura.SelectedValue = "" Then
            kCsrv.UpDateDropBox(Session("DC_OSPITE"), DD_CServ)
        Else
            kCsrv.UpDateDropBoxStruttura(Session("DC_OSPITE"), DD_CServ, DD_Struttura.SelectedValue)
        End If
    End Sub



    Protected Sub DD_Struttura_TextChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles DD_Struttura.TextChanged
        AggiornaCServ()

        ScriptManager.RegisterStartupScript(Me, Me.GetType(), "Scheramta", "VisualizzaDivRegistrazione();", True)
        Call EseguiJS()
    End Sub

    Protected Sub Btn_Si_Click(sender As Object, e As EventArgs) Handles Btn_Si.Click
        Dim K As New Cls_MovimentiStanze
        Dim i As Integer

        


        i = Grd_ImportoComune.Rows.Count - 1
        Dim AData As TextBox = DirectCast(Grd_ImportoComune.Rows(i).FindControl("TxtData"), TextBox)
        Dim ADD_TipoMov As DropDownList = DirectCast(Grd_ImportoComune.Rows(i).FindControl("DD_TipoMov"), DropDownList)
        Dim ADD_Causale As DropDownList = DirectCast(Grd_ImportoComune.Rows(i).FindControl("DD_Causale"), DropDownList)
        Dim ATxt_Descrizione As TextBox = DirectCast(Grd_ImportoComune.Rows(i).FindControl("Txt_Descrizione"), TextBox)

        K.CodiceOspite = Session("CODICEOSPITE")
        K.CentroServizio = Session("CODICESERVIZIO")
        K.UltimoMovimentoOspiteData(Session("DC_OSPITIACCESSORI"), AData.Text)

        If ADD_TipoMov.SelectedValue = 13 Then
            K.Data = AData.Text
        End If
        K.Tipologia = "13"
        K.Utente = Session("UTENTE")        
        K.AggiornaDB(Session("DC_OSPITIACCESSORI"))
    End Sub
End Class

