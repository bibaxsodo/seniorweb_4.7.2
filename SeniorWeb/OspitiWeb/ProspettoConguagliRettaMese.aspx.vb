﻿Imports System.Web.Hosting
Imports System.Data.OleDb

Partial Class OspitiWeb_ProspettoConguagliRettaMese
    Inherits System.Web.UI.Page

    Dim MyTable As New System.Data.DataTable("tabella")
    Dim MyDataSet As New System.Data.DataSet()




    Public Function GiorniMese(ByVal Mese As Byte, ByVal Anno As Integer) As Byte
        If Mese = 1 Or Mese = 3 Or Mese = 5 Or Mese = 7 Or Mese = 8 Or _
           Mese = 10 Or Mese = 12 Then
            GiorniMese = 31
        Else
            If Mese <> 2 Then
                GiorniMese = 30
            Else
                If Day(DateSerial(Anno, Mese, 29)) = 29 Then
                    GiorniMese = 29
                Else
                    GiorniMese = 28
                End If
            End If
        End If
    End Function
    Private Sub CaricaGriglia()
        Dim ConnectionString As String = Session("DC_OSPITE")
        Dim cn As OleDbConnection

        cn = New Data.OleDb.OleDbConnection(ConnectionString)

        cn.Open()


        Dim Anno As Integer
        Dim Mese As Integer
        Dim Giorni As Integer
        Dim Cserv As String
        Dim MySelectedCserv As String
        Dim MySql As String
        Dim PGiorni As Integer = 4
        Dim IndiceCs As Integer

        Anno = Txt_Anno.Text
        Mese = Dd_Mese.SelectedValue
        Cserv = Cmb_CServ.SelectedValue
        MySelectedCserv = Cserv
        Giorni = GiorniMese(Mese, Anno)

        MyTable.Clear()
        MyTable.Columns.Clear()

        MyTable.Columns.Add("Nome", GetType(String))

        MyTable.Columns.Add("Tipo Retta", GetType(String))
        MyTable.Columns.Add("Giorni P", GetType(Long))
        MyTable.Columns.Add("Giorni A", GetType(Long))
        MyTable.Columns.Add("Mese Anticipato", GetType(String))

        MyTable.Columns.Add("Retta Ospite", GetType(Double))


        MyTable.Columns.Add("Retta Parenti", GetType(Double))


        MyTable.Columns.Add("Retta Comune", GetType(Double))
        MyTable.Columns.Add("Retta Regione", GetType(Double))
        MyTable.Columns.Add("Retta Jolly", GetType(Double))

        MyTable.Columns.Add("Conguaglio Ospite", GetType(Double))        

        MyTable.Columns.Add("Conguaglio Parente", GetType(Double))        

        MyTable.Columns.Add("Giorni P Ente", GetType(Long))
        MyTable.Columns.Add("Giorni A Ente", GetType(Long))

        
        MyTable.Columns.Add("Extra Fissi", GetType(String))


        For IndiceCs = 0 To Cmb_CServ.Items.Count - 1
            If (Cmb_CServ.Items(IndiceCs).Value = MySelectedCserv Or MySelectedCserv = "") And Cmb_CServ.Items(IndiceCs).Value <> "" Then
                Cserv = Cmb_CServ.Items(IndiceCs).Value

                If Mese = 0 Then
                    MySql = "SELECT  CODICEOSPITE  FROM RETTEOSPITE WHERE RETTEOSPITE.ANNO = " & Anno & "  UNION SELECT CODICEOSPITE FROM RETTEPARENTE WHERE RETTEPARENTE.ANNO = " & Anno & "  UNION SELECT CODICEOSPITE FROM RETTECOMUNE WHERE RETTECOMUNE.ANNO = " & Anno & " UNION SELECT CODICEOSPITE FROM RETTEJOLLY  WHERE RETTEJOLLY.ANNO = " & Anno & " UNION SELECT CODICEOSPITE FROM RETTEREGIONE WHERE RETTEREGIONE.ANNO = " & Anno & "  UNION SELECT CODICEOSPITE FROM RETTEENTE WHERE RETTEENTE.ANNO = " & Anno & "  GROUP BY CODICEOSPITE"
                Else
                    MySql = "SELECT  CODICEOSPITE  FROM RETTEOSPITE WHERE RETTEOSPITE.ANNO = " & Anno & "  AND RETTEOSPITE.MESE = " & Mese & " UNION SELECT CODICEOSPITE FROM RETTEPARENTE WHERE RETTEPARENTE.ANNO = " & Anno & " AND RETTEPARENTE.MESE = " & Mese & " UNION SELECT CODICEOSPITE FROM RETTECOMUNE WHERE RETTECOMUNE.ANNO = " & Anno & " AND RETTECOMUNE.MESE = " & Mese & " UNION SELECT CODICEOSPITE FROM RETTEJOLLY  WHERE RETTEJOLLY.ANNO = " & Anno & " AND RETTEJOLLY.MESE = " & Mese & "  UNION SELECT CODICEOSPITE FROM RETTEREGIONE WHERE RETTEREGIONE.ANNO = " & Anno & " AND RETTEREGIONE.MESE = " & Mese & " UNION SELECT CODICEOSPITE FROM RETTEENTE WHERE RETTEENTE.ANNO = " & Anno & " AND RETTEENTE.MESE = " & Mese & " GROUP BY CODICEOSPITE"
                End If
                Dim cmd As New OleDbCommand()
                cmd.CommandText = MySql
                cmd.Connection = cn
                Dim myPOSTreader As OleDbDataReader = cmd.ExecuteReader()
                Do While myPOSTreader.Read
                    Dim CodiceOspite As Long
                    Dim ImportoOspite As Double = 0
                    Dim ImportoParente As Double = 0
                    Dim ImportoComune As Double = 0
                    Dim ImportoRegione As Double = 0
                    Dim ImportoJolly As Double = 0

                    Dim ImportoAddebitiOspite As Double = 0
                    Dim ImportoAccreditiOspite As Double = 0

                    Dim ImportoAddebitiParente As Double = 0
                    Dim ImportoAccreditiParente As Double = 0
                    Dim ImportoAddebitiComune As Double = 0
                    Dim ImportoAccreditiComune As Double = 0
                    Dim ImportoAddebitiJolly As Double = 0
                    Dim ImportoAccreditiJolly As Double = 0

                    Dim ImportoAddebitiRegione As Double = 0
                    Dim ImportoAccreditiRegione As Double = 0


                    Dim GiorniPresO As Long = 0
                    Dim GiorniAssO As Long = 0
                    Dim GiorniPresP As Long = 0
                    Dim GiorniAssP As Long = 0
                    Dim GiorniPresC As Long = 0
                    Dim GiorniAssC As Long = 0
                    Dim GiorniPresR As Long = 0
                    Dim GiorniAssR As Long = 0

                    Dim GiorniPresJ As Long = 0
                    Dim GiorniAssJ As Long = 0

                    Dim xMese As Integer
                    Dim xAnno As Integer
                    If Mese = 1 Then
                        xAnno = Anno - 1
                        xMese = 12
                    Else
                        xMese = Mese - 1
                        xAnno = Anno
                    End If

                    CodiceOspite = myPOSTreader.Item("CodiceOspite")
                    
                    
                    MySql = "SELECT  *  FROM RETTEOSPITE WHERE RETTEOSPITE.ANNO = " & xAnno & "  AND RETTEOSPITE.MESE = " & xMese & " And CodiceOspite = " & CodiceOspite & " And CentroServizio = '" & Cserv & "'"
                    Dim cmdO As New OleDbCommand()
                    cmdO.CommandText = MySql
                    cmdO.Connection = cn
                    Dim OReader As OleDbDataReader = cmdO.ExecuteReader()
                    Do While OReader.Read
                        If OReader.Item("ELEMENTO") = "RPX" Then
                            ImportoOspite = ImportoOspite + OReader.Item("IMPORTO")
                            GiorniPresO = GiorniPresO + OReader.Item("GIORNI")
                        End If
                    Loop
                    OReader.Close()

                    
                    MySql = "SELECT  *  FROM RETTEPARENTE WHERE ANNO = " & xAnno & "  AND MESE = " & xMese & " And CodiceOspite = " & CodiceOspite & " And CentroServizio = '" & Cserv & "'"
                    Dim cmdP As New OleDbCommand()
                    cmdP.CommandText = MySql
                    cmdP.Connection = cn
                    Dim PReader As OleDbDataReader = cmdP.ExecuteReader()
                    Do While PReader.Read
                        If PReader.Item("ELEMENTO") = "RPX" Then
                            ImportoParente = ImportoParente + PReader.Item("IMPORTO")
                            GiorniPresP = GiorniPresP + PReader.Item("GIORNI")
                        End If
                    Loop
                    PReader.Close()


                    MySql = "SELECT  *  FROM RETTEOSPITE WHERE RETTEOSPITE.ANNO = " & Anno & "  AND RETTEOSPITE.MESE = " & Mese & " And CodiceOspite = " & CodiceOspite & " And CentroServizio = '" & Cserv & "' And MeseCompetenza = 0 And AnnoCompetenza = 0"
                    Dim cmdOAd As New OleDbCommand()
                    cmdOAd.CommandText = MySql
                    cmdOAd.Connection = cn
                    Dim OAdReader As OleDbDataReader = cmdOAd.ExecuteReader()
                    Do While OAdReader.Read
                        If campodb(OAdReader.Item("ELEMENTO")) = "RGA" Then
                            ImportoAddebitiOspite = ImportoAddebitiOspite + campodbN(OAdReader.Item("IMPORTO"))
                            GiorniAssO = GiorniAssO + campodbN(OAdReader.Item("GIORNI"))
                        End If
                        If campodb(OAdReader.Item("ELEMENTO")) = "RGP" Then
                            ImportoAccreditiOspite = ImportoAccreditiOspite + campodbN(OAdReader.Item("IMPORTO"))
                            GiorniPresO = GiorniPresO + campodbN(OAdReader.Item("GIORNI"))
                        End If
                    Loop
                    OAdReader.Close()


                    MySql = "SELECT  *  FROM RETTEOSPITE WHERE RETTEOSPITE.AnnoCompetenza = " & Anno & "  AND RETTEOSPITE.MeseCompetenza = " & Mese & " And CodiceOspite = " & CodiceOspite & " And CentroServizio = '" & Cserv & "'  "
                    Dim cmdOAdComp As New OleDbCommand()
                    cmdOAdComp.CommandText = MySql
                    cmdOAdComp.Connection = cn
                    Dim OAdReaderComp As OleDbDataReader = cmdOAdComp.ExecuteReader()
                    Do While OAdReaderComp.Read
                        If campodb(OAdReaderComp.Item("ELEMENTO")) = "ADD" Then
                            ImportoAddebitiOspite = ImportoAddebitiOspite + campodbN(OAdReaderComp.Item("IMPORTO"))
                        End If
                        If campodb(OAdReaderComp.Item("ELEMENTO")) = "ACC" Then
                            ImportoAccreditiOspite = ImportoAccreditiOspite + campodbN(OAdReaderComp.Item("IMPORTO"))
                        End If
                    Loop
                    OAdReaderComp.Close()

                    ImportoAddebitiParente = 0
                    ImportoAccreditiParente = 0


                    MySql = "SELECT  *  FROM RETTEPARENTE WHERE AnnoCompetenza  = " & Anno & "  AND MeseCompetenza = " & Mese & " And CodiceOspite = " & CodiceOspite & " And CentroServizio = '" & Cserv & "' "
                    Dim cmdPAComp As New OleDbCommand()
                    cmdPAComp.CommandText = MySql
                    cmdPAComp.Connection = cn
                    Dim PACompdReader As OleDbDataReader = cmdPAComp.ExecuteReader()
                    Do While PACompdReader.Read
                        If campodb(PACompdReader.Item("ELEMENTO")) = "ADD" Then
                            ImportoAddebitiParente = ImportoAddebitiParente + campodbN(PACompdReader.Item("IMPORTO"))
                        End If
                        If campodb(PACompdReader.Item("ELEMENTO")) = "ACC" Then
                            ImportoAccreditiParente = ImportoAccreditiParente + campodbN(PACompdReader.Item("IMPORTO"))
                        End If
                    Loop
                    PACompdReader.Close()

                    MySql = "SELECT  *  FROM RETTEPARENTE WHERE ANNO = " & Anno & "  AND MESE = " & Mese & " And CodiceOspite = " & CodiceOspite & " And CentroServizio = '" & Cserv & "' And MeseCompetenza = 0 And AnnoCompetenza = 0"

                    Dim cmdPAd As New OleDbCommand()
                    cmdPAd.CommandText = MySql
                    cmdPAd.Connection = cn
                    Dim PAdReader As OleDbDataReader = cmdPAd.ExecuteReader()
                    Do While PAdReader.Read
                        If campodb(PAdReader.Item("ELEMENTO")) = "RGA" Then
                            ImportoAddebitiParente = ImportoAddebitiParente + campodbN(PAdReader.Item("IMPORTO"))
                            GiorniAssP = GiorniAssP + campodbN(PAdReader.Item("GIORNI"))
                        End If
                        If campodb(PAdReader.Item("ELEMENTO")) = "RGP" Then
                            ImportoAccreditiParente = ImportoAccreditiParente + campodbN(PAdReader.Item("IMPORTO"))
                            GiorniPresP = GiorniPresP + campodb(PAdReader.Item("GIORNI"))
                        End If
                    Loop
                    PAdReader.Close()

                    If Mese = 0 Then
                        MySql = "SELECT  *  FROM RETTECOMUNE WHERE ANNO = " & Anno & " And CodiceOspite = " & CodiceOspite & " And CentroServizio = '" & Cserv & "'"
                    Else
                        MySql = "SELECT  *  FROM RETTECOMUNE WHERE ANNO = " & Anno & "  AND MESE = " & Mese & " And CodiceOspite = " & CodiceOspite & " And CentroServizio = '" & Cserv & "'"
                    End If
                    Dim cmdC As New OleDbCommand()
                    cmdC.CommandText = MySql
                    cmdC.Connection = cn
                    Dim CReader As OleDbDataReader = cmdC.ExecuteReader()
                    Do While CReader.Read
                        If campodb(CReader.Item("ELEMENTO")) = "ADD" Or campodb(CReader.Item("ELEMENTO")) = "ACC" Then
                            If CReader.Item("ELEMENTO") = "ADD" Then
                                ImportoAddebitiComune = ImportoAddebitiComune + campodbN(CReader.Item("IMPORTO"))
                            End If
                            If campodb(CReader.Item("ELEMENTO")) = "ACC" Then
                                ImportoAccreditiComune = ImportoAccreditiComune + campodbN(CReader.Item("IMPORTO"))
                            End If
                        Else
                            ImportoComune = ImportoComune + campodbN(CReader.Item("IMPORTO"))
                        End If
                        If campodbN(CReader.Item("GIORNI")) > 0 And campodb(CReader.Item("ELEMENTO")) = "RGA" Then
                            GiorniAssC = GiorniAssC + CReader.Item("GIORNI")
                        End If
                        If campodbN(CReader.Item("GIORNI")) > 0 And (campodb(CReader.Item("ELEMENTO")) = "RGP" Or campodb(CReader.Item("ELEMENTO")) = "RPX") Then
                            GiorniPresC = GiorniPresC + campodbN(CReader.Item("GIORNI"))
                        End If
                    Loop
                    CReader.Close()

                    If Mese = 0 Then
                        MySql = "SELECT  *  FROM RETTEREGIONE WHERE ANNO = " & Anno & "  And CodiceOspite = " & CodiceOspite & " And CentroServizio = '" & Cserv & "'"
                    Else
                        MySql = "SELECT  *  FROM RETTEREGIONE WHERE ANNO = " & Anno & "  AND MESE = " & Mese & " And CodiceOspite = " & CodiceOspite & " And CentroServizio = '" & Cserv & "'"
                    End If
                    Dim cmdR As New OleDbCommand()
                    cmdR.CommandText = MySql
                    cmdR.Connection = cn
                    Dim RReader As OleDbDataReader = cmdR.ExecuteReader()
                    Do While RReader.Read
                        If campodb(RReader.Item("ELEMENTO")) = "ADD" Or campodb(RReader.Item("ELEMENTO")) = "ACC" Then
                            If campodb(RReader.Item("ELEMENTO")) = "ADD" Then
                                ImportoAddebitiRegione = ImportoAddebitiRegione + campodbN(RReader.Item("IMPORTO"))
                            End If
                            If campodb(RReader.Item("ELEMENTO")) = "ACC" Then
                                ImportoAccreditiRegione = ImportoAccreditiRegione + campodbN(RReader.Item("IMPORTO"))
                            End If
                        Else
                            ImportoRegione = ImportoRegione + campodbN(RReader.Item("IMPORTO"))
                        End If
                        If campodbN(RReader.Item("GIORNI")) > 0 And campodb(RReader.Item("ELEMENTO")) = "RGA" Then
                            GiorniAssR = GiorniAssR + campodb(RReader.Item("GIORNI"))
                        End If
                        If campodbN(RReader.Item("GIORNI")) > 0 And (campodb(RReader.Item("ELEMENTO")) = "RGP" Or campodb(RReader.Item("ELEMENTO")) = "RPX") Then
                            GiorniPresR = GiorniPresR + RReader.Item("GIORNI")
                        End If
                    Loop
                    RReader.Close()


                    If Mese = 0 Then
                        MySql = "SELECT  *  FROM RETTEJOLLY WHERE ANNO = " & Anno & " And CodiceOspite = " & CodiceOspite & " And CentroServizio = '" & Cserv & "'"
                    Else
                        MySql = "SELECT  *  FROM RETTEJOLLY WHERE ANNO = " & Anno & "  AND MESE = " & Mese & " And CodiceOspite = " & CodiceOspite & " And CentroServizio = '" & Cserv & "'"
                    End If
                    Dim cmdJ As New OleDbCommand()
                    cmdJ.CommandText = MySql
                    cmdJ.Connection = cn
                    Dim JReader As OleDbDataReader = cmdJ.ExecuteReader()
                    Do While JReader.Read
                        If campodb(JReader.Item("ELEMENTO")) = "ADD" Or campodb(JReader.Item("ELEMENTO")) = "ACC" Then
                            If campodb(JReader.Item("ELEMENTO")) = "ADD" Then
                                ImportoAddebitiJolly = ImportoAddebitiJolly + campodbN(JReader.Item("IMPORTO"))
                            End If
                            If campodb(JReader.Item("ELEMENTO")) = "ACC" Then
                                ImportoAccreditiJolly = ImportoAccreditiJolly + campodbN(JReader.Item("IMPORTO"))
                            End If
                        Else
                            ImportoJolly = ImportoJolly + campodbN(JReader.Item("IMPORTO"))
                        End If
                        If campodbN(JReader.Item("GIORNI")) > 0 And campodb(JReader.Item("ELEMENTO")) = "RGA" Then
                            GiorniAssJ = GiorniAssJ + campodb(JReader.Item("GIORNI"))
                        End If
                        If campodbN(JReader.Item("GIORNI")) > 0 And (campodb(JReader.Item("ELEMENTO")) = "RGP" Or campodb(JReader.Item("ELEMENTO")) = "RPX") Then
                            GiorniPresJ = GiorniPresJ + campodbN(JReader.Item("GIORNI"))
                        End If
                    Loop
                    JReader.Close()





                    If ImportoOspite > 0 Or ImportoParente > 0 Or ImportoComune > 0 Or ImportoRegione > 0 Or ImportoAddebitiOspite > 0 Or ImportoAccreditiOspite > 0 Or _
                       ImportoAddebitiParente > 0 Or ImportoAccreditiParente > 0 Or ImportoAddebitiJolly > 0 Or ImportoAccreditiJolly > 0 Or ImportoAddebitiComune > 0 Or ImportoAccreditiComune > 0 Or ImportoAddebitiRegione > 0 Or ImportoAccreditiRegione > 0 Then

                        Dim myriga As System.Data.DataRow = MyTable.NewRow()

                        Dim XR As New ClsOspite

                        XR.Leggi(ConnectionString, CodiceOspite)


                        myriga("Nome") = XR.Nome
                        'myriga("Nome") = XR.CODICEFISCALE


                        If GiorniPresO > 0 Or GiorniAssO Then
                            myriga("Giorni P") = GiorniPresO
                            myriga("Giorni A") = GiorniAssO
                        Else
                            If GiorniPresC > 0 Or GiorniAssC Then
                                myriga("Giorni P") = GiorniPresC
                                myriga("Giorni A") = GiorniAssC
                            Else
                                If GiorniPresR > 0 Or GiorniAssR Then
                                    myriga("Giorni P") = GiorniPresR
                                    myriga("Giorni A") = GiorniAssR
                                Else
                                    If GiorniPresP > 0 Or GiorniAssP Then
                                        myriga("Giorni P") = GiorniPresP
                                        myriga("Giorni A") = GiorniAssP
                                    End If
                                End If
                            End If
                        End If


                        Dim RTot As New Cls_rettatotale

                        RTot.CENTROSERVIZIO = Cserv
                        RTot.CODICEOSPITE = CodiceOspite

                        RTot.UltimaData(Session("DC_OSPITE"), CodiceOspite, Cserv)

                        Dim dec As New Cls_TipoRetta

                        dec.Descrizione = ""
                        If RTot.TipoRetta <> "" Then
                            dec.Codice = RTot.TipoRetta
                            dec.Leggi(Session("DC_OSPITE"), dec.Codice)
                        End If

                        myriga("Tipo Retta") = dec.Descrizione

                        Dim Extra As New Cls_ExtraFisso
                        Extra.Data = RTot.Data
                        Extra.CODICEOSPITE = CodiceOspite
                        Extra.CENTROSERVIZIO = Cserv
                        myriga("Extra Fissi") = Extra.ElencaExtrafissiTesto(Session("DC_OSPITE"))

                        

                        myriga("Retta Ospite") = Math.Round(CDbl(ImportoOspite), 2)
                        myriga("Retta Parenti") = Math.Round(CDbl(ImportoParente), 2)

                        myriga("Retta Comune") = Math.Round(CDbl(ImportoComune), 2)
                        myriga("Retta Regione") = Math.Round(CDbl(ImportoRegione), 2)
                        myriga("Retta Jolly") = Math.Round(CDbl(ImportoJolly), 2)

                        If CDbl(ImportoAddebitiOspite) < CDbl(ImportoAccreditiOspite) Then
                            myriga("Conguaglio Ospite") = Math.Round((CDbl(ImportoAccreditiOspite) - CDbl(ImportoAddebitiOspite)) * -1, 2)
                        Else
                            myriga("Conguaglio Ospite") = Math.Round(CDbl(ImportoAddebitiOspite) - CDbl(ImportoAccreditiOspite), 2)
                        End If

                        If CDbl(ImportoAddebitiParente) < CDbl(ImportoAccreditiParente) Then
                            myriga("Conguaglio Parente") = Math.Round((CDbl(ImportoAccreditiParente) - CDbl(ImportoAddebitiParente)) * -1, 2)
                        Else
                            myriga("Conguaglio Parente") = Math.Round(CDbl(ImportoAddebitiParente) - CDbl(ImportoAccreditiParente), 2)
                        End If



                        If GiorniAssR + GiorniPresR > GiorniAssC + GiorniPresC Then
                            myriga("Giorni P Ente") = Math.Round(CDbl(GiorniPresR), 2)
                            myriga("Giorni A Ente") = Math.Round(CDbl(GiorniAssR), 0)
                        Else
                            If GiorniAssJ + GiorniPresJ > GiorniAssC + GiorniPresC Then
                                myriga("Giorni P Ente") = Math.Round(CDbl(GiorniPresJ), 2)
                                myriga("Giorni A Ente") = Math.Round(CDbl(GiorniAssJ), 0)
                            Else
                                myriga("Giorni P Ente") = Math.Round(CDbl(GiorniPresC), 2)
                                myriga("Giorni A Ente") = Math.Round(CDbl(GiorniAssC), 0)
                            End If
                        End If

                        myriga("Mese Anticipato") = xMese & "/" & xAnno

                        ' myriga("CodiceOspite") = CodiceOspite

                        MyTable.Rows.Add(myriga)
                    End If
                Loop
                myPOSTreader.Close()
            End If
        Next
        cn.Close()



        Dim view As System.Data.DataView = MyTable.DefaultView
        view.Sort = "Nome"


        Dim myriga1 As System.Data.DataRow = MyTable.NewRow()
        MyTable.Rows.Add(myriga1)

        GridView1.AutoGenerateColumns = True
        GridView1.DataSource = MyTable
        GridView1.Font.Size = 10
        GridView1.DataBind()



    End Sub

    Private Function GiorniAssenza(ByVal CServ As String, ByVal CodiceOspite As Integer, ByVal DataDal As Date, ByVal DataAl As Date) As Integer
        Dim ConnectionString As String = Session("DC_OSPITE")
        Dim MySql As String
        Dim cn As OleDbConnection
        Dim I As Integer
        GiorniAssenza = 0
        cn = New Data.OleDb.OleDbConnection(ConnectionString)

        cn.Open()

        If CodiceOspite = 347 Then
            CodiceOspite = 347
        End If
        Dim M As New Cls_Parametri

        M.LeggiParametri(ConnectionString)



        For I = 0 To DateDiff("d", DataDal, DataAl)


            MySql = "select top 1 * from Movimenti Where data <= ? and CENTROSERVIZIO = ? And CodiceOspite = ? order by data desc,PROGRESSIVO "

            Dim cmd As New OleDbCommand()
            cmd.CommandText = MySql
            cmd.Connection = cn
            cmd.Parameters.AddWithValue("@Data", DateAdd("d", I, DataDal))
            cmd.Parameters.AddWithValue("@Cserv", Cmb_CServ.SelectedValue)
            cmd.Parameters.AddWithValue("@CodiceOspite", CodiceOspite)
            Dim myPOSTreader As OleDbDataReader = cmd.ExecuteReader()
            Do While myPOSTreader.Read
                If campodbd(myPOSTreader.Item("DATA")) = DateAdd(DateInterval.Day, I, DataDal) Then
                    Dim tCausale As New Cls_CausaliEntrataUscita

                    tCausale.Codice = campodb(myPOSTreader.Item("CAUSALE"))
                    tCausale.LeggiCausale(ConnectionString)
                    If M.GIORNOUSCITA = "P" And campodb(myPOSTreader.Item("TIPOMOV")) = "03" Then
                    Else
                        If tCausale.GiornoUscita = "A" Then
                            GiorniAssenza = GiorniAssenza + 1
                        End If
                    End If
                Else
                    Dim tCausale As New Cls_CausaliEntrataUscita

                    tCausale.Codice = campodb(myPOSTreader.Item("CAUSALE"))
                    tCausale.LeggiCausale(ConnectionString)
                    If campodb(myPOSTreader.Item("TIPOMOV")) = "03" Then
                        If tCausale.TIPOMOVIMENTO = "A" Then
                            GiorniAssenza = GiorniAssenza + 1
                        End If
                    End If
                    If campodb(myPOSTreader.Item("TIPOMOV")) = "04" Then
                        REM GiorniPresenza = GiorniPresenza + 1
                    End If
                End If
            Loop
            myPOSTreader.Close()
            cmd.Dispose()
        Next

    End Function

    Private Function GiorniPresenza(ByVal CServ As String, ByVal CodiceOspite As Integer, ByVal DataDal As Date, ByVal DataAl As Date) As Integer
        Dim ConnectionString As String = Session("DC_OSPITE")
        Dim MySql As String
        Dim cn As OleDbConnection
        Dim I As Integer
        GiorniPresenza = 0
        cn = New Data.OleDb.OleDbConnection(ConnectionString)

        cn.Open()

        If CodiceOspite = 864 Then
            CodiceOspite = 864
        End If

        Dim M As New Cls_Parametri

        M.LeggiParametri(ConnectionString)


        For I = 0 To DateDiff("d", DataDal, DataAl)


            MySql = "select top 1 * from Movimenti Where data <= ? and CENTROSERVIZIO = ? And CodiceOspite = ? order by data desc,PROGRESSIVO desc "

            Dim cmd As New OleDbCommand()
            cmd.CommandText = MySql
            cmd.Connection = cn
            cmd.Parameters.AddWithValue("@Data", DateAdd("d", I, DataDal))
            cmd.Parameters.AddWithValue("@Cserv", Cmb_CServ.SelectedValue)
            cmd.Parameters.AddWithValue("@CodiceOspite", CodiceOspite)
            Dim myPOSTreader As OleDbDataReader = cmd.ExecuteReader()
            Do While myPOSTreader.Read
                If campodbd(myPOSTreader.Item("DATA")) = DateAdd(DateInterval.Day, I, DataDal) Then
                    Dim tCausale As New Cls_CausaliEntrataUscita

                    tCausale.Codice = campodb(myPOSTreader.Item("CAUSALE"))
                    tCausale.LeggiCausale(ConnectionString)
                    If tCausale.GiornoUscita = "P" Then
                        GiorniPresenza = GiorniPresenza + 1
                    End If
                    If tCausale.Codice = "" Then
                        GiorniPresenza = GiorniPresenza + 1
                    End If

                    If M.GIORNOUSCITA = "P" Then
                        If campodb(myPOSTreader.Item("TIPOMOV")) = "03" Then
                            If tCausale.TIPOMOVIMENTO = "A" Then
                                GiorniPresenza = GiorniPresenza + 1
                            End If
                        End If
                    End If
                Else
                    Dim tCausale As New Cls_CausaliEntrataUscita

                    tCausale.Codice = campodb(myPOSTreader.Item("CAUSALE"))
                    tCausale.LeggiCausale(ConnectionString)
                    If campodb(myPOSTreader.Item("TIPOMOV")) = "03" Then
                        If tCausale.TIPOMOVIMENTO = "P" Then
                            GiorniPresenza = GiorniPresenza + 1
                        End If
                    End If
                    If campodb(myPOSTreader.Item("TIPOMOV")) = "04" Then
                        GiorniPresenza = GiorniPresenza + 1
                    End If
                    If campodb(myPOSTreader.Item("TIPOMOV")) = "05" Then
                        GiorniPresenza = GiorniPresenza + 1
                    End If
                End If
            Loop
            myPOSTreader.Close()
            cmd.Dispose()
        Next

    End Function


    Function campodbN(ByVal oggetto As Object) As Double
        If IsDBNull(oggetto) Then
            Return 0
        Else
            Return oggetto
        End If
    End Function
    Function campodb(ByVal oggetto As Object) As String
        If IsDBNull(oggetto) Then
            Return ""
        Else
            Return oggetto
        End If
    End Function
    Function campodbd(ByVal oggetto As Object) As Date
        If IsDBNull(oggetto) Then
            Return Nothing
        Else
            Return oggetto
        End If
    End Function

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Call EseguiJS()
        If Page.IsPostBack = True Then
            Exit Sub
        End If


        Dim K1 As New Cls_SqlString

        Dim Barra As New Cls_BarraSenior

        If Not IsNothing(Session("RicercaAnagraficaSQLString")) Then
            K1 = Session("RicercaAnagraficaSQLString")
        End If

        Lbl_BarraSenior.Text = Barra.CodiceBarra(Application("SENIOR"), Session("UTENTE"), Page, K1)

        Lbl_Errori.Text = ""
        If Request.Item("Errore") = "NONDATI" Then
            REM Lbl_Errori.Text = "Nessun dato estratto"
            ClientScript.RegisterClientScriptBlock(Me.GetType(), "Errore", "VisualizzaErrore('Nessun dato estratto');", True)
        End If

        Dim k As New Cls_Parametri

        k.LeggiParametri(Session("DC_OSPITE"))

        Txt_Anno.Text = k.AnnoFatturazione
        Dd_Mese.SelectedValue = k.MeseFatturazione

        Lbl_Utente.Text = Session("UTENTE")


        Dim ConnectionString As String = Session("DC_OSPITE")

        Dim kVilla As New Cls_TabelleDescrittiveOspitiAccessori


        kVilla.UpDateDropBox(Session("DC_OSPITIACCESSORI"), "VIL", DD_Struttura)
        If DD_Struttura.Items.Count = 1 Then
            Call AggiornaCServ()
        End If

    End Sub

    Protected Sub ImageButton3_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles ImageButton3.Click
        If Val(Txt_Anno.Text) = 0 Then
            REM ClientScript.RegisterClientScriptBlock(Me.GetType(), "Errore", "VisualizzaErrore('Specificare anno');", True)
            ScriptManager.RegisterStartupScript(Me, Me.GetType(), "visualizzaA", "VisualizzaErrore('Specificare anno');", True)
            Exit Sub
        End If

        'If Cmb_CServ.SelectedValue = "" Then
        '    ClientScript.RegisterClientScriptBlock(Me.GetType(), "Errore", "VisualizzaErrore('Specificare centro servizio');", True)
        '    Exit Sub
        'End If

        Call CaricaGriglia()
    End Sub

    Protected Sub ImageButton4_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles ImageButton4.Click
        If Val(Txt_Anno.Text) = 0 Then
            ClientScript.RegisterClientScriptBlock(Me.GetType(), "Errore", "VisualizzaErrore('Specificare anno');", True)
            Exit Sub
        End If
        'If Cmb_CServ.SelectedValue = "" Then
        '    ClientScript.RegisterClientScriptBlock(Me.GetType(), "Errore", "VisualizzaErrore('Specificare centro servizio');", True)
        '    Exit Sub
        'End If

        Call CaricaGriglia()
        If MyTable.Rows.Count > 1 Then
            Response.Clear()
            Response.AddHeader("content-disposition", "attachment;filename=VerificaRette.xls")

            Response.Charset = String.Empty
            Response.Cache.SetCacheability(HttpCacheability.NoCache)
            Response.ContentType = "application/vnd.ms-excel"
            Dim stringWrite As New System.IO.StringWriter
            Dim htmlWrite As New HtmlTextWriter(stringWrite)

            form1.Controls.Clear()
            form1.Controls.Add(GridView1)

            form1.RenderControl(htmlWrite)

            Response.Write(stringWrite.ToString())
            Response.End()
        End If
    End Sub

    Protected Sub Btn_Esci_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles Btn_Esci.Click
        Response.Redirect("Menu_Visualizzazioni.aspx")
    End Sub

    Private Sub AggiornaCServ()
        Dim kCsrv As New Cls_CentroServizio

        If DD_Struttura.SelectedValue = "" Then
            kCsrv.UpDateDropBox(Session("DC_OSPITE"), Cmb_CServ)
        Else
            kCsrv.UpDateDropBoxStruttura(Session("DC_OSPITE"), Cmb_CServ, DD_Struttura.SelectedValue)
        End If
    End Sub

    Private Sub EseguiJS()
        Dim MyJs As String
        MyJs = "$(document).ready(function() {"

        MyJs = MyJs & "if (window.innerHeight>0) { $(""#BarraLaterale"").css(""height"",(window.innerHeight - 94) + ""px""); } else"
        MyJs = MyJs & "{ $(""#BarraLaterale"").css(""height"",(document.documentElement.offsetHeight - 94) + ""px"");  }"
        MyJs = MyJs & "});"

        'MyJs = "$(document).ready(function() { $('.myClass').keypress(function() { handleEnter($('.myClass').val(), true, true); } );     $('#" & Txt_ImportoPagato.ClientID & "').blur(function() { var ap = formatNumber ($('#" & Txt_ImportoPagato.ClientID & "').val(),2); $('#" & Txt_ImportoPagato.ClientID & "').val(ap); }); });"
        ScriptManager.RegisterStartupScript(Me, Me.GetType(), "visualizzaRitIm", MyJs, True)
    End Sub



    Protected Sub DD_Struttura_TextChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles DD_Struttura.TextChanged
        AggiornaCServ()
    End Sub

    Protected Sub Btn_Stampa_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles Btn_Stampa.Click

        If Val(Txt_Anno.Text) = 0 Then
            REM ClientScript.RegisterClientScriptBlock(Me.GetType(), "Errore", "VisualizzaErrore('Specificare anno');", True)
            ScriptManager.RegisterStartupScript(Me, Me.GetType(), "visualizzaA", "VisualizzaErrore('Specificare anno');", True)
            Exit Sub
        End If

        'If Cmb_CServ.SelectedValue = "" Then
        '    ClientScript.RegisterClientScriptBlock(Me.GetType(), "Errore", "VisualizzaErrore('Specificare centro servizio');", True)
        '    Exit Sub
        'End If

        Call CaricaGriglia()

        Session("GrigliaSoloStampa") = MyTable

        Session("ParametriSoloStampa") = ""

        ScriptManager.RegisterStartupScript(Me, Me.GetType(), "Stampa", "openPopUp('GrigliaSoloStampa.aspx','Stampe','width=800,height=600');", True)

    End Sub
End Class
