﻿Imports Microsoft.VisualBasic
Imports System.Data.OleDb
Imports System.Web.Hosting


Partial Class TabellaComuni
    Inherits System.Web.UI.Page

    Dim MyEccezioni As New System.Data.DataTable("Eccezioni")
    Dim DSEccezioni As New System.Data.DataSet()




    Protected Sub Grd_Eccezioni_RowCommand(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewCommandEventArgs) Handles Grd_Eccezioni.RowCommand
        If (e.CommandName = "Inserisci") Then
            Call InserisciRigaEccezioni()
        End If
    End Sub

    Protected Sub Grd_Eccezioni_RowDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs) Handles Grd_Eccezioni.RowDataBound
        If e.Row.RowType = DataControlRowType.DataRow Then
            MyEccezioni = ViewState("Eccezioni")


            Dim DD_CentroSerivizio As DropDownList = DirectCast(e.Row.FindControl("DD_CentroSerivizio"), DropDownList)

            Dim k As New Cls_CentroServizio

            k.UpDateDropBox(Session("DC_OSPITE"), DD_CentroSerivizio)

            DD_CentroSerivizio.SelectedValue = MyEccezioni.Rows(e.Row.RowIndex).Item(0).ToString


            Dim DD_TipoOperazione As DropDownList = DirectCast(e.Row.FindControl("DD_TipoOperazione"), DropDownList)

            Dim k1 As New Cls_TipoOperazione

            k1.UpDateDropBoxTipo(Session("DC_OSPITE"), DD_TipoOperazione, "E")

            DD_TipoOperazione.SelectedValue = MyEccezioni.Rows(e.Row.RowIndex).Item(1).ToString


            Dim DD_IVA As DropDownList = DirectCast(e.Row.FindControl("DD_IVA"), DropDownList)

            Dim k2 As New Cls_IVA

            k2.UpDateDropBox(Session("DC_TABELLE"), DD_IVA)

            DD_IVA.SelectedValue = MyEccezioni.Rows(e.Row.RowIndex).Item(2).ToString




            Call EseguiJS()

        End If
    End Sub

    Protected Sub Grd_Eccezioni_RowDeleting(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewDeleteEventArgs) Handles Grd_Eccezioni.RowDeleting
        UpDateTableEccezioni()
        MyEccezioni = ViewState("Eccezioni")
        Try
            If Request.UserAgent.Contains("Chrome") Then
                If IsDBNull(Session("RI_TIMER")) Or DateDiff("s", Session("RI_TIMER"), Now) > 1 Then
                    MyEccezioni.Rows.RemoveAt(e.RowIndex)
                    If MyEccezioni.Rows.Count = 0 Then
                        Dim myriga As System.Data.DataRow = MyEccezioni.NewRow()
                        myriga(1) = 0
                        myriga(2) = 0
                        MyEccezioni.Rows.Add(myriga)
                    End If
                    Session("RI_TIMER") = Now
                End If
            Else
                MyEccezioni.Rows.RemoveAt(e.RowIndex)
                If MyEccezioni.Rows.Count = 0 Then
                    Dim myriga As System.Data.DataRow = MyEccezioni.NewRow()
                    myriga(1) = 0
                    myriga(2) = 0
                    MyEccezioni.Rows.Add(myriga)
                End If
            End If
        Catch ex As Exception

        End Try


        ViewState("Eccezioni") = MyEccezioni

        Grd_Eccezioni.AutoGenerateColumns = False

        Grd_Eccezioni.DataSource = MyEccezioni

        Grd_Eccezioni.DataBind()
        Call EseguiJS()

        e.Cancel = True
    End Sub


    Protected Function Modifica() As Boolean
        Dim k As New ClsComune
        Modifica = False
        Dim ConnectionString As String = Session("DC_OSPITE")

        k.Provincia = Txt_Prov.Text
        k.Comune = Txt_Comune.Text

        k.Leggi(ConnectionString)

        k.Provincia = Txt_Prov.Text
        k.Comune = Txt_Comune.Text
        k.Descrizione = Txt_Descrizione.Text


        If Chk_NonInUso.Checked = True Then
            k.NonInUso = "S"
        Else
            k.NonInUso = ""
        End If


        k.NomeConiuge = Txt_NomeConiuge.Text

        k.RESIDENZAINDIRIZZO1 = Txt_Indirizzo.Text
        k.RESIDENZACAP1 = Txt_Cap.Text


        k.Attenzione = Txt_Attenzione.Text

        k.PartitaIVA = Val(Txt_Piva.Text)
        k.CodiceFiscale = Txt_CodiceFiscale.Text

        k.IdDocumentoData = Txt_IdDocumentoData.Text

        Dim Vettore(100) As String

        If Txt_ComuneIndirizzo.Text <> "" Then
            Vettore = SplitWords(Txt_ComuneIndirizzo.Text)
            If Vettore.Length > 1 Then
                k.RESIDENZAPROVINCIA1 = Vettore(0)
                k.RESIDENZACOMUNE1 = Vettore(1)
            End If
        Else
            k.RESIDENZAPROVINCIA1 = ""
            k.RESIDENZACOMUNE1 = ""
        End If

        'k.RESIDENZAPROVINCIA1 = Txt_ProvRes.Text

        'k.RESIDENZACOMUNE1 = Txt_ComRes.Text

        k.RESIDENZATELEFONO3 = Txt_Mail.Text

        k.CONTOPERESATTO = Txt_ContoEsportazione.Text

        k.IntCliente = Txt_Int.Text
        k.NumeroControlloCliente = Val(Txt_NumCont.Text)
        k.CABCLIENTE = Txt_Cab.Text
        k.ABICLIENTE = Txt_Abi.Text
        k.CINCLIENTE = Txt_Cin.Text

        k.CCBANCARIOCLIENTE = Txt_Ccbancario.Text
        k.BANCACLIENTE = Txt_BancaCliente.Text
        k.CodiceCig = Txt_BancaCIG.Text
        k.ModalitaPagamento = DD_ModalitaPagamento.SelectedValue

        k.TipoOperazione = DD_TipoOperazione.SelectedValue

        k.CodiceIva = DD_IVA.SelectedValue


        k.RecapitoNome = Txt_Referente.Text
        k.RESIDENZATELEFONO4 = Txt_TelefonoReferente.Text


        k.SoggettoABollo = ""
        If Chk_EscludiBolloInXML.Checked Then
            k.SoggettoABollo = "N"
        End If


        If RB_Mensile.Checked = True Then
            k.PERIODO = "M"
        End If
        If RB_Bimestrale.Checked = True Then
            k.PERIODO = "B"
        End If
        If RB_Trimestrale.Checked = True Then
            k.PERIODO = "T"
        End If

        If RB_NonFatturare.Checked = True Then
            k.PERIODO = "N"
        End If

        If RB_FatturaDa.Checked = True Then
            k.PERIODO = "X"

            k.AnnoFatturazione = TxtAnno.Text
            k.MeseFatturazione = TxtMese.Text

        End If

        If RB_SI.Checked = True Then
            k.Compensazione = "S"
        End If
        If RB_NO.Checked = True Then
            RB_NO.Checked = True
            k.Compensazione = "N"
        End If

        If Chk_RotturaOspite.Checked = True Then
            k.RotturaOspite = 1
        Else
            k.RotturaOspite = 0
        End If

        If Chk_RotturaCServ.Checked = True Then
            k.RotturaCserv = "S"
        Else
            k.RotturaCserv = "N"
        End If

        If Chk_ImportiAzero.Checked = True Then
            k.ImportoAZero = 1
        Else
            k.ImportoAZero = 0
        End If

        If Chk_IvaSospesa.Checked = True Then
            k.IvaSospesa = "S"
        Else
            k.IvaSospesa = ""
        End If

        k.CODXCODF = Txt_CodiceCatastale.Text

        k.EGo = DD_EGO.SelectedValue


        If Chk_DestinatarioOspite.Checked = True Then
            k.OspiteDestinatario = 1
        Else
            k.OspiteDestinatario = 0
        End If

        If Chk_IntestatarioOspite.Checked = True Then
            k.OspiteIntestatario = 1
        Else
            k.OspiteIntestatario = 0
        End If

        k.CodiceCup = Txt_Cup.Text
        k.IdDocumento = Txt_IdDocumento.Text
        k.RiferimentoAmministrazione = Txt_RiferimentoAmministrazione.Text
        If Chk_NFDtt.Checked = True Then
            k.NumeroFatturaDDT = 1
        Else
            k.NumeroFatturaDDT = 0
        End If

        k.CodiceDestinatario = Txt_CodiceDestinatario.Text
        k.CodificaProvincia = Txt_CodificaProvincia.Text

        If Chk_Exprovincia.Checked = True Then
            k.Specializazione = "S"
        Else
            k.Specializazione = "N"
        End If

        If Chk_Anticipita.Checked = True Then
            k.FattAnticipata = "S"
        Else
            k.FattAnticipata = "N"
        End If
        If Chk_Privato.Checked = True Then
            k.Privato = 1
        Else
            k.Privato = 0
        End If

        If Chk_EnteSanitario.Checked = True Then
            k.TIPOENTE = "S"
        Else
            k.TIPOENTE = ""
        End If

        If Rb_NonRagruppare.Checked = True Then
            k.RaggruppaInExport = 0
        End If
        If Rb_GiorniInDesc.Checked = True Then
            k.RaggruppaInExport = 1
        End If
        If Rb_GiorniInQty.Checked = True Then
            k.RaggruppaInExport = 2
        End If
        'Txt_Mastro.Text = k.MastroCliente

        'Txt_Conto.Text = k.ContoCliente
        'Txt_Sottoconto.Text = k.SottocontoCliente

        k.Note = Txt_NoteFe.Text

        k.Causale = Txt_Causale.Text


        If Chk_Personalizzare.Checked = False Then
            k.DescrizioneXML = ""
        Else
            k.DescrizioneXML = "<PERSONOLIZZA>"

            Chk_FE_RaggruppaRicavi.Enabled = True
            Chk_FE_IndicaMastroPartita.Enabled = True
            Chk_FE_Competenza.Enabled = True
            Chk_FE_CentroServizio.Enabled = True
            Chk_FE_GiorniInDescrizione.Enabled = True
            Chk_FE_TipoRetta.Enabled = True
            Chk_FE_SoloIniziali.Enabled = True
            Chk_FE_NoteFatture.Enabled = True
            If Chk_FE_RaggruppaRicavi.Checked = True Then
                k.DescrizioneXML = k.DescrizioneXML & "<FE_RaggruppaRicavi>"
            End If
            If Chk_FE_IndicaMastroPartita.Checked = True Then
                k.DescrizioneXML = k.DescrizioneXML & "<FE_IndicaMastroPartita>"
            End If
            If Chk_FE_Competenza.Checked = True Then
                k.DescrizioneXML = k.DescrizioneXML & "<FE_Competenza>"
            End If
            If Chk_FE_CentroServizio.Checked = True Then
                k.DescrizioneXML = k.DescrizioneXML & "<FE_CentroServizio>"
            End If

            If Chk_FE_GiorniInDescrizione.Checked = True Then

                k.DescrizioneXML = k.DescrizioneXML & "<FE_GiorniInDescrizione>"
            End If
            If Chk_FE_TipoRetta.Checked = True Then
                k.DescrizioneXML = k.DescrizioneXML & "<FE_TipoRetta>"
            End If

            If Chk_FE_SoloIniziali.Checked = True Then

                k.DescrizioneXML = k.DescrizioneXML & "<FE_SoloIniziali>"
            End If

            If Chk_FE_NoteFatture.Checked = True Then

                k.DescrizioneXML = k.DescrizioneXML & "<FE_NoteFatture>"
            End If
        End If


        If Txt_Sottoconto.Text <> "" Then
            k.MastroCliente = 0
            k.ContoCliente = 0
            k.SottocontoCliente = 0


            Vettore = SplitWords(Txt_Sottoconto.Text)
            If Vettore.Length > 2 Then
                k.MastroCliente = Vettore(0)
                k.ContoCliente = Vettore(1)
                k.SottocontoCliente = Vettore(2)
            End If
        Else
            k.MastroCliente = 0
            k.ContoCliente = 0
            k.SottocontoCliente = 0
        End If

        If k.SottocontoCliente = 0 And DD_TipoOperazione.SelectedValue <> "" Then
            Dim TabellaParametri As New Cls_Parametri

            TabellaParametri.LeggiParametri(Session("DC_OSPITE"))

            Dim PianoConti As New Cls_Pianodeiconti

            PianoConti.Mastro = TabellaParametri.Mastro
            PianoConti.Conto = TabellaParametri.ContoComune
            PianoConti.Sottoconto = k.Provincia & k.Comune
            PianoConti.Decodfica(Session("DC_GENERALE"))

            If PianoConti.Descrizione = "" Then
                PianoConti.Mastro = TabellaParametri.Mastro
                PianoConti.Conto = TabellaParametri.ContoComune
                PianoConti.Sottoconto = k.Provincia & k.Comune
                PianoConti.Descrizione = Txt_Descrizione.Text
                PianoConti.Tipo = "A"
                PianoConti.TipoAnagrafica = "C"
                PianoConti.Scrivi(Session("DC_GENERALE"))

                k.MastroCliente = PianoConti.Mastro
                k.ContoCliente = PianoConti.Conto
                k.SottocontoCliente = PianoConti.Sottoconto
            Else

                k.MastroCliente = PianoConti.Mastro
                k.ContoCliente = PianoConti.Conto
                k.SottocontoCliente = PianoConti.Sottoconto
            End If
        End If


        If Txt_Percentuale.Text = "" Then
            k.ImportoSconto = 0
        Else
            k.ImportoSconto = Txt_Percentuale.Text
        End If
        k.NumItem = Txt_NumItem.Text

        k.CodiceCommessaConvezione = Txt_CodiceCommessaConvezione.Text

        k.PEC = Txt_PEC.Text

        Try
            k.ScriviComune(ConnectionString)
        Catch ex As Exception
            ClientScript.RegisterClientScriptBlock(Me.GetType(), "Errore", "VisualizzaErrore('Errore in registrazione anagrafica comune');", True)
            Exit Function
        End Try


        Dim E1 As New Cls_DatiComnueRegioneServizio

        E1.CodiceProvincia = Txt_Prov.Text
        E1.CodiceComune = Txt_Comune.Text

        Try

            E1.EliminaAll(Session("DC_OSPITE"))
        Catch ex As Exception
            ClientScript.RegisterClientScriptBlock(Me.GetType(), "Errore", "VisualizzaErrore('Errore su Eccezioni');", True)
            Exit Function
        End Try

        Try

            For i = 0 To Grd_Eccezioni.Rows.Count - 1



                Dim DD_CentroSerivizio As DropDownList = DirectCast(Grd_Eccezioni.Rows(i).FindControl("DD_CentroSerivizio"), DropDownList)
                Dim DD_TipoOperazione As DropDownList = DirectCast(Grd_Eccezioni.Rows(i).FindControl("DD_TipoOperazione"), DropDownList)
                Dim DD_IVA As DropDownList = DirectCast(Grd_Eccezioni.Rows(i).FindControl("DD_IVA"), DropDownList)

                Dim EX As New Cls_DatiComnueRegioneServizio

                EX.CodiceProvincia = Txt_Prov.Text
                EX.CodiceComune = Txt_Comune.Text

                EX.CentroServizio = DD_CentroSerivizio.SelectedValue
                EX.TipoOperazione = DD_TipoOperazione.SelectedValue
                EX.AliquotaIva = DD_IVA.SelectedValue

                EX.Scrivi(Session("DC_OSPITE"))

            Next
        Catch ex As Exception
            ClientScript.RegisterClientScriptBlock(Me.GetType(), "Errore", "VisualizzaErrore('Errore su Eccezioni');", True)
            Exit Function
        End Try

        Modifica = True
    End Function

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load

        If IsNothing(Session("UTENTE")) Then
            Response.Redirect("../Login.aspx")
            Exit Sub

        End If
        If Page.IsPostBack = True Then
            Call EseguiJS()
            Exit Sub
        End If




        Dim K1 As New Cls_SqlString

        Dim Barra As New Cls_BarraSenior

        If Not IsNothing(Session("RicercaAnagraficaSQLString")) Then
            K1 = Session("RicercaAnagraficaSQLString")
        End If

        Lbl_BarraSenior.Text = Barra.CodiceBarra(Application("SENIOR"), Session("UTENTE"), Page, K1)

        Dim ConnectionString As String = Session("DC_OSPITE")
        Dim ConnectionStringTabelle As String = Session("DC_TABELLE")
        Dim k As New ClsUSL

        Dim DDTipoOpe As New Cls_TipoOperazione
        DDTipoOpe.UpDateDropBoxTipo(ConnectionString, DD_TipoOperazione, "E")

        Dim DDModalita As New ClsModalitaPagamento
        DDModalita.UpDateDropBox(ConnectionString, DD_ModalitaPagamento)

        Dim DDIVA As New Cls_IVA
        DDIVA.UpDateDropBox(ConnectionStringTabelle, DD_IVA)

        Call EseguiJS()

        Dim Com As New ClsComune

        If Trim(Request.Item("PROVINCIA")) = "" And Trim(Request.Item("COMUNE")) = "" Then
            Txt_Prov.Text = "888"
            Txt_Comune.Text = Format(Com.MaxComune(Session("DC_OSPITE")), "000")
            Exit Sub
        End If





        Com.Provincia = Request.Item("PROVINCIA")
        Com.Comune = Request.Item("COMUNE")

        Com.Leggi(ConnectionString)

        Txt_Prov.Text = Com.Provincia
        Txt_Comune.Text = Com.Comune


        Txt_Prov.Enabled = False
        Txt_Comune.Enabled = False


        If Com.NonInUso = "S" Then
            Chk_NonInUso.Checked = True
        Else
            Chk_NonInUso.Checked = False
        End If



        Txt_Descrizione.Text = Com.Descrizione

        Txt_NomeConiuge.Text = Com.NomeConiuge

        Txt_Indirizzo.Text = Com.RESIDENZAINDIRIZZO1
        Txt_Cap.Text = Com.RESIDENZACAP1


        Txt_Attenzione.Text = Com.Attenzione

        Txt_Piva.Text = Com.PartitaIVA

        Txt_CodiceFiscale.Text = Com.CodiceFiscale

        Dim DecCom As New ClsComune

        If Com.RESIDENZACOMUNE1 <> "" And Com.RESIDENZAPROVINCIA1 <> "" Then
            DecCom.Comune = Com.RESIDENZACOMUNE1
            DecCom.Provincia = Com.RESIDENZAPROVINCIA1
            DecCom.Leggi(Session("DC_OSPITE"))
            Txt_ComuneIndirizzo.Text = DecCom.Provincia & " " & DecCom.Comune & " " & DecCom.Descrizione
        Else
            Txt_ComuneIndirizzo.Text = ""
        End If

        DD_TipoOperazione.SelectedValue = Com.TipoOperazione




        If Com.SoggettoABollo = "" Then
            Chk_EscludiBolloInXML.Checked = False
        Else
            Chk_EscludiBolloInXML.Checked = True
        End If


        DD_IVA.SelectedValue = Com.CodiceIva

        Txt_Int.Text = Com.IntCliente
        Txt_NumCont.Text = Com.NumeroControlloCliente
        Txt_Cab.Text = Com.CABCLIENTE
        Txt_Abi.Text = Com.ABICLIENTE
        Txt_Cin.Text = Com.CINCLIENTE

        Txt_Ccbancario.Text = Com.CCBANCARIOCLIENTE
        Txt_BancaCliente.Text = Com.BANCACLIENTE
        Txt_BancaCIG.Text = Com.CodiceCig
        DD_ModalitaPagamento.SelectedValue = Com.ModalitaPagamento

        Txt_CodiceDestinatario.Text = Com.CodiceDestinatario
        Txt_CodificaProvincia.Text = Com.CodificaProvincia


        Txt_Mail.Text = Com.RESIDENZATELEFONO3

        Txt_Referente.Text = Com.RecapitoNome
        Txt_TelefonoReferente.Text = Com.RESIDENZATELEFONO4

        Txt_NoteFe.Text = Com.Note

        Txt_IdDocumentoData.Text = Com.IdDocumentoData

        Txt_ContoEsportazione.Text = Com.CONTOPERESATTO

        Txt_Causale.Text = Com.Causale

        Txt_Cup.Text = Com.CodiceCup
        Txt_IdDocumento.Text = Com.IdDocumento
        Txt_RiferimentoAmministrazione.Text = Com.RiferimentoAmministrazione
        If Com.NumeroFatturaDDT = 1 Then
            Chk_NFDtt.Checked = True
        Else
            Chk_NFDtt.Checked = False
        End If

        Txt_CodiceCatastale.Text = Com.CODXCODF

        If Com.Specializazione = "S" Then
            Chk_Exprovincia.Checked = True
        Else
            Chk_Exprovincia.Checked = False
        End If

        If Com.PERIODO = "M" Then
            RB_Mensile.Checked = True
        End If
        If Com.PERIODO = "B" Then
            RB_Bimestrale.Checked = True
        End If
        If Com.PERIODO = "T" Then
            RB_Trimestrale.Checked = True
        End If

        If Com.PERIODO = "N" Then
            RB_NonFatturare.Checked = True
        End If


        If Com.PERIODO = "X" Then
            RB_FatturaDa.Checked = True

            TxtAnno.Text = Com.AnnoFatturazione
            TxtMese.Text = Com.MeseFatturazione

            TxtAnno.Enabled = True
            TxtAnno.BackColor = System.Drawing.Color.White
            TxtMese.Enabled = True
            TxtMese.BackColor = System.Drawing.Color.White
        End If


        If Com.Compensazione = "S" Then
            RB_SI.Checked = True
        End If
        If Com.Compensazione = "N" Then
            RB_NO.Checked = True
        End If


        If Com.IvaSospesa = "S" Then
            Chk_IvaSospesa.Checked = True
        Else
            Chk_IvaSospesa.Checked = False
        End If

        Txt_Percentuale.Text = Format(Com.ImportoSconto, "#,##0.00")


        If Com.RotturaCserv = "S" Then
            Chk_RotturaCServ.Checked = True
        Else
            Chk_RotturaCServ.Checked = False
        End If

        If Com.FattAnticipata = "S" Then
            Chk_Anticipita.Checked = True
        Else
            Chk_Anticipita.Checked = False
        End If



        If Com.RotturaOspite = 1 Then
            Chk_RotturaOspite.Checked = True
        Else
            Chk_RotturaOspite.Checked = False
        End If

        If Com.ImportoAZero = 1 Then
            Chk_ImportiAzero.Checked = True
        Else
            Chk_ImportiAzero.Checked = False
        End If

        If Com.OspiteDestinatario = 1 Then
            Chk_DestinatarioOspite.Checked = True
        Else
            Chk_DestinatarioOspite.Checked = False
        End If

        If Com.OspiteIntestatario = 1 Then
            Chk_IntestatarioOspite.Checked = True
        Else
            Chk_IntestatarioOspite.Checked = False
        End If

        If Com.Privato = 1 Then
            Chk_Privato.Checked = True
        Else
            Chk_Privato.Checked = False
        End If

        If Com.EGo = "" Or Com.EGo = "N" Then
            DD_EGO.SelectedValue = "N"
        Else
            DD_EGO.SelectedValue = Com.EGo
        End If


        If Com.TIPOENTE = "S" Then
            Chk_EnteSanitario.Checked = True
        Else
            Chk_EnteSanitario.Checked = False
        End If


        Rb_NonRagruppare.Checked = False
        Rb_GiorniInDesc.Checked = False
        Rb_GiorniInQty.Checked = False
        If Com.RaggruppaInExport = 0 Then
            Rb_NonRagruppare.Checked = True
        End If
        If Com.RaggruppaInExport = 1 Then
            Rb_GiorniInDesc.Checked = True
        End If
        If Com.RaggruppaInExport = 2 Then
            Rb_GiorniInQty.Checked = True
        End If


        Txt_NumItem.Text = Com.NumItem

        Txt_CodiceCommessaConvezione.Text = Com.CodiceCommessaConvezione

        Txt_PEC.Text = Com.PEC


        If Com.DescrizioneXML = "" Then
            Chk_Personalizzare.Checked = False
            Chk_FE_RaggruppaRicavi.Enabled = False
            Chk_FE_IndicaMastroPartita.Enabled = False
            Chk_FE_Competenza.Enabled = False
            Chk_FE_CentroServizio.Enabled = False
            Chk_FE_GiorniInDescrizione.Enabled = False
            Chk_FE_TipoRetta.Enabled = False
            Chk_FE_SoloIniziali.Enabled = False
            Chk_FE_NoteFatture.Enabled = False
        Else
            Chk_Personalizzare.Checked = True

            Chk_FE_RaggruppaRicavi.Enabled = True
            Chk_FE_IndicaMastroPartita.Enabled = True
            Chk_FE_Competenza.Enabled = True
            Chk_FE_CentroServizio.Enabled = True
            Chk_FE_GiorniInDescrizione.Enabled = True
            Chk_FE_TipoRetta.Enabled = True
            Chk_FE_SoloIniziali.Enabled = True
            Chk_FE_NoteFatture.Enabled = True
            If Com.DescrizioneXML.IndexOf("<FE_RaggruppaRicavi>") >= 0 Then
                Chk_FE_RaggruppaRicavi.Checked = True
            End If
            If Com.DescrizioneXML.IndexOf("<FE_IndicaMastroPartita>") >= 0 Then
                Chk_FE_IndicaMastroPartita.Checked = True
            End If
            If Com.DescrizioneXML.IndexOf("<FE_Competenza>") >= 0 Then
                Chk_FE_Competenza.Checked = True
            End If
            If Com.DescrizioneXML.IndexOf("<FE_CentroServizio>") >= 0 Then
                Chk_FE_CentroServizio.Checked = True
            End If

            If Com.DescrizioneXML.IndexOf("<FE_GiorniInDescrizione>") >= 0 Then
                Chk_FE_GiorniInDescrizione.Checked = True
            End If
            If Com.DescrizioneXML.IndexOf("<FE_TipoRetta>") >= 0 Then
                Chk_FE_TipoRetta.Checked = True
            End If

            If Com.DescrizioneXML.IndexOf("<FE_SoloIniziali>") >= 0 Then
                Chk_FE_SoloIniziali.Checked = True
            End If

            If Com.DescrizioneXML.IndexOf("<FE_NoteFatture>") >= 0 Then
                Chk_FE_NoteFatture.Checked = True
            End If
        End If


        Dim x As New Cls_Pianodeiconti

        x.Mastro = Com.MastroCliente
        x.Conto = Com.ContoCliente
        x.Sottoconto = Com.SottocontoCliente


        x.Decodfica(Session("DC_GENERALE"))


        Dim IC As New Cls_DatiComnueRegioneServizio

        IC.CodiceProvincia = Com.Provincia
        IC.CodiceComune = Com.Comune
        IC.loaddati(ConnectionString, MyEccezioni)


        ViewState("Eccezioni") = MyEccezioni

        Grd_Eccezioni.AutoGenerateColumns = False
        Grd_Eccezioni.DataSource = MyEccezioni.DefaultView
        Grd_Eccezioni.DataBind()

        Txt_Sottoconto.Text = Com.MastroCliente & " " & Com.ContoCliente & " " & Com.SottocontoCliente & " " & x.Descrizione


        If Val(Txt_Piva.Text) > 0 Then
            Dim Stringa As String = Format(Val(Txt_Piva.Text), "00000000000")

            Dim VerCf As New Cls_CodiceFiscale

            If VerCf.CheckPartitaIva(Stringa) = False Then
                Img_VerPIVA.ImageUrl = "~/images/errore.gif"
            Else
                Img_VerPIVA.ImageUrl = "~/images/Blanco.png"
            End If
        End If
    End Sub

    Protected Sub Btn_Modifica1_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles Btn_Modifica1.Click
        If Txt_Prov.Text.Trim = "" Then
            ClientScript.RegisterClientScriptBlock(Me.GetType(), "Errore", "VisualizzaErrore('Specificare provincia');", True)
            Call EseguiJS()
            Exit Sub
        End If
        If Txt_Descrizione.Text.Trim = "" Then
            ClientScript.RegisterClientScriptBlock(Me.GetType(), "Errore", "VisualizzaErrore('Specificare descrizione');", True)
            Call EseguiJS()
            Exit Sub
        End If


        If Chk_RotturaOspite.Checked = False And Chk_IntestatarioOspite.Checked = True Then
            ClientScript.RegisterClientScriptBlock(Me.GetType(), "Errore", "VisualizzaErrore('Se indichi intestatario Ospite devi indicare rottura ospite');", True)
            Call EseguiJS()
            Exit Sub
        End If

        If Chk_DestinatarioOspite.Checked = True And Chk_IntestatarioOspite.Checked = False Then
            ClientScript.RegisterClientScriptBlock(Me.GetType(), "Errore", "VisualizzaErrore('Se indichi destinatario Ospite devi indicare intestatario ospite');", True)
            Call EseguiJS()
            Exit Sub
        End If

        

        If Txt_Prov.Enabled = True Then
            Dim k As New ClsComune

            k.Provincia = Txt_Prov.Text
            k.Comune = Txt_Comune.Text
            k.DecodficaComune(Session("DC_OSPITE"))

            If k.Descrizione.Trim <> "" Then
                ClientScript.RegisterClientScriptBlock(Me.GetType(), "Errore", "VisualizzaErrore('Comune già presente');", True)
                Call EseguiJS()
                Exit Sub
            End If
        End If


        If RB_SI.Checked = True Then
            Dim MS As New Cls_TipoOperazione

            MS.Codice = DD_TipoOperazione.SelectedValue
            MS.Leggi(Session("DC_OSPITE"), MS.Codice)

            If MS.SCORPORAIVA = 1 Then
                ClientScript.RegisterClientScriptBlock(Me.GetType(), "Errore", "VisualizzaErrore('Compensazione Si non compatibile con Scorporo IVA');", True)
                Call EseguiJS()
                Exit Sub
            End If
        End If

        If Modifica() Then


            Call PaginaPrecedente()

        End If
    End Sub


    Protected Sub Btn_Elimina_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles Btn_Elimina.Click
        If Txt_Prov.Text.Trim = "" Then
            ClientScript.RegisterClientScriptBlock(Me.GetType(), "Errore", "VisualizzaErrore('Specificare provincia');", True)
            Exit Sub
        End If
        If Txt_Descrizione.Text.Trim = "" Then
            ClientScript.RegisterClientScriptBlock(Me.GetType(), "Errore", "VisualizzaErrore('Specificare descrizione');", True)
            Exit Sub
        End If
        Dim x As New ClsComune

        x.Provincia = Txt_Prov.Text
        x.Comune = Txt_Comune.Text


        If x.InUso(Session("DC_OSPITE")) Then
            ClientScript.RegisterClientScriptBlock(Me.GetType(), "Errore", "VisualizzaErrore('Non posso eliminare, codice usato');", True)
            Exit Sub
        End If


        x.Elimina(Session("DC_OSPITE"))

        Call PaginaPrecedente()
    End Sub


    Protected Sub Txt_Descrizione_TextChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles Txt_Descrizione.TextChanged
        If Txt_Prov.Enabled = True Then
            If Txt_Descrizione.Text.Trim <> "" Then
                Img_VerificaDes.ImageUrl = "~/images/corretto.gif"

                Dim x As New ClsComune

                x.Provincia = ""
                x.Comune = ""
                x.Descrizione = Txt_Descrizione.Text
                x.LeggiDescrizione(Session("DC_OSPITE"))

                If x.Provincia <> "" Then
                    Img_VerificaDes.ImageUrl = "~/images/errore.gif"
                End If
            End If
        End If
    End Sub

    Protected Sub Btn_Esci_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles Btn_Esci.Click
        Call PaginaPrecedente()
    End Sub

    Private Sub PaginaPrecedente()
        If Request.Item("PAGINA") = "" Then
            Response.Redirect("ElencoComuni.aspx")
        Else
            Response.Redirect("ElencoComuni.aspx?CHIAMATA=RITORNO&PAGINA=" & Request.Item("PAGINA"))
        End If

    End Sub


    Private Sub EseguiJS()
        Dim MyJs As String
        MyJs = "$(document).ready(function() {"

        MyJs = MyJs & "var els = document.getElementsByTagName(""*"");"

        MyJs = MyJs & "for (var i=0;i<els.length;i++)"
        MyJs = MyJs & "if ( els[i].id ) { "
        MyJs = MyJs & " var appoggio =els[i].id; "
        MyJs = MyJs & " if (appoggio.match('TxtData')!= null) {  "
        MyJs = MyJs & " $(els[i]).mask(""99/99/9999"");"
        MyJs = MyJs & "    }"
        MyJs = MyJs & " if ((appoggio.match('TxtImporto')!= null) || (appoggio.match('Txt_Percentuale')!= null) ) {  "
        MyJs = MyJs & " $(els[i]).keypress(function() { ForceNumericInput($(this).val(), true, true); } ); "
        MyJs = MyJs & " $(els[i]).blur(function() { var ap = formatNumber($(this).val(),2); $(this).val(ap); });"
        MyJs = MyJs & "    }"

        MyJs = MyJs & " if (appoggio.match('Txt_Sottoconto')!= null) {   "
        MyJs = MyJs & " $(els[i]).autocomplete('/WebHandler/GestioneConto.ashx?Utente=" & Session("UTENTE") & "', {delay:5,minChars:3});"
        MyJs = MyJs & "    }"

        MyJs = MyJs & " if (appoggio.match('Txt_ComuneIndirizzo')!= null)  {  "
        MyJs = MyJs & " $(els[i]).autocomplete('/WebHandler/AutocompleteComune.ashx?Utente=" & Session("UTENTE") & "', {delay:5,minChars:3});"
        MyJs = MyJs & "    }"

        MyJs = MyJs & "} "
        MyJs = MyJs & "});"

        'MyJs = "$(document).ready(function() { $('.myClass').keypress(function() { handleEnter($('.myClass').val(), true, true); } );     $('#" & Txt_ImportoPagato.ClientID & "').blur(function() { var ap = formatNumber ($('#" & Txt_ImportoPagato.ClientID & "').val(),2); $('#" & Txt_ImportoPagato.ClientID & "').val(ap); }); });"
        ScriptManager.RegisterStartupScript(Me, Me.GetType(), "visualizzaRitIm", MyJs, True)
    End Sub


    Private Function SplitWords(ByVal s As String) As String()
        '
        ' Call Regex.Split function from the imported namespace.
        ' Return the result array.
        '
        Return Regex.Split(s, "\W+")
    End Function

    Protected Sub IB_Duplica_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles IB_Duplica.Click
        Dim com As New ClsComune

        Txt_Prov.Text = "888"
        Txt_Comune.Text = Format(com.MaxComune(Session("DC_OSPITE")), "000")
        Txt_Prov.Enabled = True
        Txt_Comune.Enabled = True


        Txt_Sottoconto.Text = ""

        Call Txt_Comune_TextChanged(sender, e)
        Call Txt_Descrizione_TextChanged(sender, e)

    End Sub

    Protected Sub Txt_Comune_TextChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles Txt_Comune.TextChanged
        If Txt_Prov.Enabled = True Then
            If Txt_Comune.Text.Trim <> "" Then
                Img_VerCodice.ImageUrl = "~/images/corretto.gif"

                Dim x As New ClsComune

                x.Provincia = Txt_Prov.Text
                x.Comune = Txt_Comune.Text
                x.Leggi(Session("DC_OSPITE"))

                If x.Descrizione <> "" Then
                    Img_VerCodice.ImageUrl = "~/images/errore.gif"
                End If
            End If
        End If


    End Sub



    Private Sub UpDateTableEccezioni()
        Dim i As Integer

        MyEccezioni.Clear()
        MyEccezioni.Columns.Clear()
        MyEccezioni.Columns.Add("Servizio", GetType(String))
        MyEccezioni.Columns.Add("TipoOperazione", GetType(String))
        MyEccezioni.Columns.Add("IVA", GetType(String))

        For i = 0 To Grd_Eccezioni.Rows.Count - 1

            Dim DD_CentroSerivizio As DropDownList = DirectCast(Grd_Eccezioni.Rows(i).FindControl("DD_CentroSerivizio"), DropDownList)
            Dim DD_TipoOperazione As DropDownList = DirectCast(Grd_Eccezioni.Rows(i).FindControl("DD_TipoOperazione"), DropDownList)
            Dim DD_IVA As DropDownList = DirectCast(Grd_Eccezioni.Rows(i).FindControl("DD_IVA"), DropDownList)

            Dim myrigaR As System.Data.DataRow = MyEccezioni.NewRow()


            myrigaR(0) = DD_CentroSerivizio.SelectedValue
            myrigaR(1) = DD_TipoOperazione.SelectedValue
            myrigaR(2) = DD_IVA.SelectedValue

            MyEccezioni.Rows.Add(myrigaR)
        Next
        ViewState("Eccezioni") = MyEccezioni

    End Sub

    Private Sub InserisciRigaEccezioni()
        UpDateTableEccezioni()
        MyEccezioni = ViewState("Eccezioni")
        Dim myriga As System.Data.DataRow = MyEccezioni.NewRow()
        myriga(0) = ""
        myriga(1) = 0

        MyEccezioni.Rows.Add(myriga)

        ViewState("Eccezioni") = MyEccezioni
        Grd_Eccezioni.AutoGenerateColumns = False

        Grd_Eccezioni.DataSource = MyEccezioni

        Grd_Eccezioni.DataBind()

        Call EseguiJS()
    End Sub

    Protected Sub Txt_Piva_TextChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles Txt_Piva.TextChanged
        If Val(Txt_Piva.Text) > 0 Then
            Dim Stringa As String = Format(Val(Txt_Piva.Text), "00000000000")

            Dim VerCf As New Cls_CodiceFiscale

            If VerCf.CheckPartitaIva(Stringa) = False Then
                Img_VerPIVA.ImageUrl = "~/images/errore.gif"
            Else
                Img_VerPIVA.ImageUrl = "~/images/Blanco.png"
            End If
        End If

    End Sub

    Function campodb(ByVal oggetto As Object) As String
        If IsDBNull(oggetto) Then
            Return ""
        Else
            Return oggetto
        End If
    End Function


    Protected Sub RB_FatturaDa_CheckedChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles RB_FatturaDa.CheckedChanged
        If RB_FatturaDa.Checked = True Then
            TxtAnno.Enabled = True
            TxtAnno.BackColor = System.Drawing.Color.White
            TxtMese.Enabled = True
            TxtMese.BackColor = System.Drawing.Color.White
            Dim cn As OleDbConnection
            Dim MySql As String

            MySql = ""

            cn = New Data.OleDb.OleDbConnection(Session("DC_GENERALE"))

            cn.Open()

            Dim cmd As New OleDbCommand()
            cmd.CommandText = ("Select AnnoCompetenza,MeseCompetenza from MovimentiContabiliTesta Where Tipologia like ? Order by AnnoCompetenza Desc,MeseCompetenza Desc ")
            cmd.Parameters.AddWithValue("@Tipologia", "%" & Txt_Prov.Text & Txt_Comune.Text & "%")
            cmd.Connection = cn
            Dim myPOSTreader As OleDbDataReader = cmd.ExecuteReader()
            If myPOSTreader.Read Then
                TxtAnno.Text = campodb(myPOSTreader.Item("AnnoCompetenza"))
                TxtMese.Text = campodb(myPOSTreader.Item("MeseCompetenza"))

            End If
            cn.Close()

        Else

            TxtAnno.Enabled = False
            TxtAnno.BackColor = System.Drawing.Color.Gray
            TxtMese.Enabled = False
            TxtMese.BackColor = System.Drawing.Color.Gray
            TxtAnno.Text = ""
            TxtMese.Text = ""
        End If

    End Sub

    Protected Sub RB_SI_CheckedChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles RB_SI.CheckedChanged
        If RB_FatturaDa.Checked = False Then
            TxtAnno.Enabled = False
            TxtAnno.BackColor = System.Drawing.Color.Gray
            TxtMese.Enabled = False
            TxtMese.BackColor = System.Drawing.Color.Gray
            TxtAnno.Text = ""
            TxtMese.Text = ""
        End If
    End Sub

    Protected Sub RB_Trimestrale_CheckedChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles RB_Trimestrale.CheckedChanged
        If RB_FatturaDa.Checked = False Then
            TxtAnno.Enabled = False
            TxtAnno.BackColor = System.Drawing.Color.Gray
            TxtMese.Enabled = False
            TxtMese.BackColor = System.Drawing.Color.Gray
            TxtAnno.Text = ""
            TxtMese.Text = ""
        End If
    End Sub

    Protected Sub RB_NonFatturare_CheckedChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles RB_NonFatturare.CheckedChanged
        If RB_FatturaDa.Checked = False Then
            TxtAnno.Enabled = False
            TxtAnno.BackColor = System.Drawing.Color.Gray
            TxtMese.Enabled = False
            TxtMese.BackColor = System.Drawing.Color.Gray
            TxtAnno.Text = ""
            TxtMese.Text = ""
        End If
    End Sub

    Protected Sub RB_Mensile_CheckedChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles RB_Mensile.CheckedChanged
        If RB_FatturaDa.Checked = False Then
            TxtAnno.Enabled = False
            TxtAnno.BackColor = System.Drawing.Color.Gray
            TxtMese.Enabled = False
            TxtMese.BackColor = System.Drawing.Color.Gray
            TxtAnno.Text = ""
            TxtMese.Text = ""
        End If
    End Sub

    Protected Sub RB_Bimestrale_CheckedChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles RB_Bimestrale.CheckedChanged
        If RB_FatturaDa.Checked = False Then
            TxtAnno.Enabled = False
            TxtAnno.BackColor = System.Drawing.Color.Gray
            TxtMese.Enabled = False
            TxtMese.BackColor = System.Drawing.Color.Gray
            TxtAnno.Text = ""
            TxtMese.Text = ""
        End If
    End Sub

    Protected Sub Chk_Personalizzare_CheckedChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles Chk_Personalizzare.CheckedChanged
        If Chk_Personalizzare.Checked = False Then
            Chk_FE_RaggruppaRicavi.Enabled = False
            Chk_FE_IndicaMastroPartita.Enabled = False
            Chk_FE_Competenza.Enabled = False
            Chk_FE_CentroServizio.Enabled = False
            Chk_FE_GiorniInDescrizione.Enabled = False
            Chk_FE_TipoRetta.Enabled = False
            Chk_FE_SoloIniziali.Enabled = False
            Chk_FE_NoteFatture.Enabled = False
            Chk_FE_RaggruppaRicavi.Enabled = False
        Else            
            Chk_FE_RaggruppaRicavi.Enabled = True
            Chk_FE_IndicaMastroPartita.Enabled = True
            Chk_FE_Competenza.Enabled = True
            Chk_FE_CentroServizio.Enabled = True
            Chk_FE_GiorniInDescrizione.Enabled = True
            Chk_FE_TipoRetta.Enabled = True
            Chk_FE_SoloIniziali.Enabled = True
            Chk_FE_NoteFatture.Enabled = True
            Chk_FE_RaggruppaRicavi.Enabled = True

        End If
    End Sub
End Class
