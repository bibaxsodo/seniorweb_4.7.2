﻿Imports System.Web.Hosting
Imports System.Data.OleDb
Partial Class OspitiWeb_EstrattoContoDenaro
    Inherits System.Web.UI.Page
    Dim MyTable As New System.Data.DataTable("tabella")
    Dim MyDataSet As New System.Data.DataSet()

    Function campodb(ByVal oggetto As Object) As String
        If IsDBNull(oggetto) Then
            Return ""
        Else
            Return oggetto
        End If
    End Function

    Function campodbN(ByVal oggetto As Object) As Double
        If IsDBNull(oggetto) Then
            Return 0
        Else
            Return oggetto
        End If
    End Function

    Private Sub CaricaGriglia()
        Dim ConnectionString As String = Session("DC_OSPITIACCESSORI")
        Dim cn As OleDbConnection
        Dim cn1 As OleDbConnection
        Dim MySql As String
        Dim CodiceOspite As Long

        Dim cmd1 As New OleDbCommand()

        Dim cmd As New OleDbCommand()
        Dim MyRd As OleDbDataReader

        Dim MyRdX As OleDbDataReader

        Dim cmdG As New OleDbCommand()
        Dim MyRdG As OleDbDataReader

        Dim cmdN As New OleDbCommand()
        Dim TotP As Double
        Dim TotV As Double
        Dim Txt_DataDalText As Date = Txt_DataDal.Text
        Dim Txt_DataAlText As Date = Txt_DataAl.Text

        cn1 = New Data.OleDb.OleDbConnection(Session("DC_OSPITE"))

        cn1.Open()


        cn = New Data.OleDb.OleDbConnection(ConnectionString)

        cn.Open()

        Dim Totale1 As Double = 0
        Dim Totale2 As Double = 0
        Dim Totale3 As Double = 0
        Dim Totale4 As Double = 0

        MyTable.Clear()
        MyTable.Columns.Clear()

        MyTable.Columns.Add("Codice Ospite", GetType(Long))
        MyTable.Columns.Add("Nome", GetType(String))
        MyTable.Columns.Add("Data", GetType(String))
        MyTable.Columns.Add("Descrizione", GetType(String))
        MyTable.Columns.Add("Periodo", GetType(String))
        MyTable.Columns.Add("Saldo Iniziale", GetType(String))
        MyTable.Columns.Add("Versamento", GetType(String))
        MyTable.Columns.Add("Prelievo", GetType(String))
        MyTable.Columns.Add("Saldo Finale", GetType(String))

        If Val(Session("CODICEOSPITE")) = 0 Then
            MySql = "Select * From AnagraficaComune Where (NonInUso Is Null OR NonInUso = '') And Tipologia = 'O' ORDER BY Nome"
        Else
            MySql = "Select * From AnagraficaComune Where Tipologia = 'O' And CodiceOspite = " & Val(Session("CODICEOSPITE"))
        End If
        cmdG.CommandText = MySql
        cmdG.Parameters.AddWithValue("@DataDal", Txt_DataDalText)
        cmdG.Connection = cn1
        MyRdG = cmdG.ExecuteReader()

        Do While MyRdG.Read
            CodiceOspite = Val(campodb(MyRdG.Item("CodiceOspite")))


            If Est_Data.Checked = True Then
                MySql = "SELECT * FROM Denaro WHERE CODICEOSPITE = " & CodiceOspite & _
                            " AND DataRegistrazione >= ? " & _
                            " AND DataRegistrazione <= ?  Order by DataRegistrazione"
                cmd.CommandText = MySql
                cmd.Parameters.Clear()
                cmd.Parameters.AddWithValue("@DataDal", Txt_DataDalText)
                cmd.Parameters.AddWithValue("@DataAl", Txt_DataAlText)
            Else
                MySql = "SELECT * FROM Denaro WHERE CODICEOSPITE = " & CodiceOspite & _
                            " AND (( AnnoCompetenza = ?  And MeseCompetenza >= ?) Or AnnoCompetenza > ?)" & _
                            " AND (( AnnoCompetenza = ?  And MeseCompetenza <= ?) Or AnnoCompetenza < ?) Order by DataRegistrazione"
                cmd.CommandText = MySql
                cmd.Parameters.Clear()
                cmd.Parameters.AddWithValue("@AnnoCompetenzaDal", Txt_AnnoCompetenzaDal.Text)
                cmd.Parameters.AddWithValue("@MeseCompetenzaDal", Dd_MeseCompetenzaDal.SelectedValue)
                cmd.Parameters.AddWithValue("@AnnoCompetenzaDal", Txt_AnnoCompetenzaDal.Text)

                cmd.Parameters.AddWithValue("@AnnoCompetenzaDal", Txt_AnnoCompetenzaAl.Text)
                cmd.Parameters.AddWithValue("@MeseCompetenzaDal", Dd_MeseCompetenzaAl.SelectedValue)
                cmd.Parameters.AddWithValue("@AnnoCompetenzaDal", Txt_AnnoCompetenzaAl.Text)
            End If
            cmd.Connection = cn
            MyRdX = cmd.ExecuteReader()
            If MyRdX.Read Then

                If Est_Data.Checked = True Then
                    MySql = "SELECT SUM(Importo) AS Totale FROM Denaro " & _
                            " WHERE CodiceOspite = " & CodiceOspite & _
                            " AND DataRegistrazione < ? " & _
                            " AND TipoOperazione = 'P'"
                    cmd1.CommandText = MySql
                    cmd1.Parameters.Clear()
                    cmd1.Parameters.AddWithValue("@DataDal", Txt_DataDalText)
                    cmd1.Connection = cn
                    MyRd = cmd1.ExecuteReader()
                    If MyRd.Read Then
                        TotP = CDbl(campodbN(MyRd.Item("Totale")))
                    End If
                    MyRd.Close()

                    MySql = "SELECT SUM(Importo) AS Totale FROM Denaro " & _
                            " WHERE CodiceOspite = " & CodiceOspite & _
                            " AND DataRegistrazione < ? " & _
                            " AND TipoOperazione = 'V'"
                    cmd1.CommandText = MySql
                    cmd1.Parameters.Clear()
                    cmd1.Parameters.AddWithValue("@DataDal", Txt_DataDalText)
                    cmd1.Connection = cn
                    MyRd = cmd1.ExecuteReader()
                    If MyRd.Read Then
                        TotV = CDbl(campodbN(MyRd.Item("Totale")))
                    End If
                    MyRd.Close()

                    Dim myriga As System.Data.DataRow = MyTable.NewRow()
                    myriga(0) = CodiceOspite
                    Dim XNome As New ClsOspite
                    XNome.Leggi(Session("DC_OSPITE"), CodiceOspite)
                    myriga(1) = XNome.Nome
                    myriga(2) = ""
                    myriga(3) = ""
                    myriga(4) = ""
                    myriga(5) = Format(Math.Round(TotV - TotP, 2), "#,##0.00")
                    myriga(6) = 0
                    myriga(7) = 0
                    myriga(8) = 0

                    MyTable.Rows.Add(myriga)
                Else
                    TotV = 0
                    TotP = 0
                End If

                Totale1 = Totale1 + Math.Round(TotV - TotP, 2)


                If Est_Data.Checked = True Then
                    MySql = "SELECT * FROM Denaro WHERE CODICEOSPITE = " & CodiceOspite & _
                                " AND DataRegistrazione >= ? " & _
                                " AND DataRegistrazione <= ? Order by DataRegistrazione"
                    cmd1.CommandText = MySql
                    cmd1.Parameters.Clear()
                    cmd1.Parameters.AddWithValue("@DataDal", Txt_DataDalText)
                    cmd1.Parameters.AddWithValue("@DataAl", Txt_DataAlText)
                Else
                    MySql = "SELECT * FROM Denaro WHERE CODICEOSPITE = " & CodiceOspite & _
                                " AND (( AnnoCompetenza = ?  And MeseCompetenza >= ?) Or AnnoCompetenza > ?)" & _
                                " AND (( AnnoCompetenza = ?  And MeseCompetenza <= ?) Or AnnoCompetenza < ?) Order by DataRegistrazione"
                    cmd1.CommandText = MySql
                    cmd1.Parameters.Clear()
                    cmd1.Parameters.AddWithValue("@AnnoCompetenzaDal", Txt_AnnoCompetenzaDal.Text)
                    cmd1.Parameters.AddWithValue("@MeseCompetenzaDal", Dd_MeseCompetenzaDal.SelectedValue)
                    cmd1.Parameters.AddWithValue("@AnnoCompetenzaDal", Txt_AnnoCompetenzaDal.Text)

                    cmd1.Parameters.AddWithValue("@AnnoCompetenzaDal", Txt_AnnoCompetenzaAl.Text)
                    cmd1.Parameters.AddWithValue("@MeseCompetenzaDal", Dd_MeseCompetenzaAl.SelectedValue)
                    cmd1.Parameters.AddWithValue("@AnnoCompetenzaDal", Txt_AnnoCompetenzaAl.Text)
                End If
                cmd1.Connection = cn
                MyRd = cmd1.ExecuteReader()
                Do While MyRd.Read

                    Dim myriga3 As System.Data.DataRow = MyTable.NewRow()
                    myriga3(0) = CodiceOspite
                    Dim XNome1 As New ClsOspite
                    XNome1.Leggi(Session("DC_OSPITE"), CodiceOspite)
                    myriga3(1) = XNome1.Nome

                    Dim MyTipoAdd As New Cls_Addebito

                    MyTipoAdd.Codice = campodb(MyRd.Item("TipoAddebito"))
                    MyTipoAdd.Leggi(Session("DC_OSPITE"), MyTipoAdd.Codice)

                    myriga3(2) = campodb(MyRd.Item("DataRegistrazione"))
                    myriga3(3) = MyTipoAdd.Descrizione & "  " & campodb(MyRd.Item("Descrizione"))
                    myriga3(4) = MyRd.Item("AnnoCompetenza") & "/" & MyRd.Item("MeseCompetenza")
                    myriga3(5) = 0
                    myriga3(6) = 0
                    myriga3(7) = 0
                    If MyRd.Item("TipoOperazione") = "V" Then
                        Totale2 = Totale2 + MyRd.Item("Importo")
                        myriga3(6) = Format(MyRd.Item("Importo"), "#,##0.00")
                    End If
                    If MyRd.Item("TipoOperazione") = "P" Then
                        Totale3 = Totale3 + MyRd.Item("Importo")
                        myriga3(7) = Format(MyRd.Item("Importo"), "#,##0.00")
                    End If
                    myriga3(8) = 0
                    MyTable.Rows.Add(myriga3)
                Loop
                MyRd.Close()

                If Est_Data.Checked = True Then
                    Dim FinaleTotP As Double
                    Dim FinaleTotV As Double


                    MySql = "SELECT SUM(Importo) AS Totale FROM Denaro " & _
                            " WHERE CodiceOspite = " & CodiceOspite & _
                            " AND DataRegistrazione <= ? " & _
                            " AND TipoOperazione = 'P'"
                    cmd1.CommandText = MySql
                    cmd1.Parameters.Clear()
                    cmd1.Parameters.AddWithValue("@DataAl", Txt_DataAlText)
                    cmd1.Connection = cn
                    MyRd = cmd1.ExecuteReader()
                    If MyRd.Read Then
                        FinaleTotP = campodbN(MyRd.Item("Totale"))
                    End If
                    MyRd.Close()

                    MySql = "SELECT SUM(Importo) AS Totale FROM Denaro " & _
                            " WHERE CodiceOspite = " & CodiceOspite & _
                            " AND DataRegistrazione <= ? " & _
                            " AND TipoOperazione = 'V'"
                    cmd1.CommandText = MySql
                    cmd1.Parameters.Clear()
                    cmd1.Parameters.AddWithValue("@DataAl", Txt_DataAlText)
                    cmd1.Connection = cn
                    MyRd = cmd1.ExecuteReader()
                    If MyRd.Read Then
                        FinaleTotV = campodbN(MyRd.Item("Totale"))
                    End If
                    MyRd.Close()

                    Dim myriga2 As System.Data.DataRow = MyTable.NewRow()
                    myriga2(0) = CodiceOspite
                    Dim XNome2 As New ClsOspite
                    XNome2.Leggi(Session("DC_OSPITE"), CodiceOspite)
                    myriga2(1) = XNome2.Nome
                    myriga2(2) = ""
                    myriga2(3) = ""
                    myriga2(4) = ""
                    myriga2(5) = 0
                    myriga2(6) = 0
                    myriga2(7) = 0
                    myriga2(8) = Format(Math.Round(FinaleTotV - FinaleTotP, 2), "#,##0.00")
                    MyTable.Rows.Add(myriga2)

                    Totale4 = Totale4 + Math.Round(FinaleTotV - FinaleTotP, 2)
                End If

            End If
            MyRdX.Close()
            If Chk_ADP.Checked = True Then
                MySql = "SELECT * FROM [ADDACR] Where (select COUNT(*) from AddebitoAccreditiProgrammati where addacr.chiaveselezione = AddebitoAccreditiProgrammati.Id and ADDACR.CodiceOspite =  AddebitoAccreditiProgrammati.CodiceOspite and ADDACR.CentroServizio =  AddebitoAccreditiProgrammati.CentroServizio) > 0 " & _
                        " AND Data  >= ? " & _
                        " AND Data <= ? " & _
                        " AND CodiceOspite = ? " & _
                        " Order by CodiceOspite "

                cmd.CommandText = MySql
                cmd.Parameters.Clear()
                cmd.Parameters.AddWithValue("@DataDal", Txt_DataDalText)
                cmd.Parameters.AddWithValue("@DataAl", Txt_DataAlText)
                cmd.Parameters.AddWithValue("@CodiceOspite", CodiceOspite)
                cmd.Connection = cn1
                MyRdX = cmd.ExecuteReader()
                Do While MyRdX.Read
                    Dim myriga3 As System.Data.DataRow = MyTable.NewRow()
                    myriga3(0) = CodiceOspite
                    Dim XNome2 As New ClsOspite
                    XNome2.Leggi(Session("DC_OSPITE"), CodiceOspite)

                    Dim MtpA As New Cls_Addebito

                    MtpA.Codice = campodb(MyRdX.Item("CodiceIva"))
                    MtpA.Leggi(Session("DC_OSPITE"), MtpA.Codice)

                    myriga3(1) = XNome2.Nome
                    myriga3(2) = campodb(MyRdX.Item("Data"))
                    myriga3(3) = MtpA.Descrizione
                    myriga3(4) = ""
                    myriga3(5) = 0
                    myriga3(6) = 0
                    myriga3(7) = Format(MyRdX.Item("Importo"), "#,##0.00")
                    myriga3(8) = ""
                    MyTable.Rows.Add(myriga3)
                Loop
                MyRdX.Close()
            End If

        Loop
        MyRdG.Close()
        cn1.Close()
        cn.Close()

        If Val(Session("CODICEOSPITE")) = 0 Then
            Dim myrigat As System.Data.DataRow = MyTable.NewRow()
            myrigat(0) = 0
            myrigat(1) = "Totale"
            myrigat(2) = ""
            myrigat(3) = ""
            myrigat(4) = ""
            myrigat(5) = Totale1
            myrigat(6) = Totale2
            myrigat(7) = Totale3
            myrigat(8) = Totale4
            MyTable.Rows.Add(myrigat)

        End If



        Dim myriga1 As System.Data.DataRow = MyTable.NewRow()
        MyTable.Rows.Add(myriga1)

        GridView1.AutoGenerateColumns = True
        GridView1.DataSource = MyTable
        GridView1.Font.Size = 10
        GridView1.DataBind()

        Call EseguiJS()
    End Sub

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Trim(Session("UTENTE")) = "" Then
            Response.Redirect("../Login.aspx")
            Exit Sub
        End If

        If Page.IsPostBack = True Then Exit Sub


        If Val(Request.Item("CodiceOspite")) > 0 And Request.Item("CentroServizio") <> "" Then

            Session("CODICEOSPITE") = Val(Request.Item("CodiceOspite"))

            
        End If
        'CodiceOspite=356&CentroServizio=CA


        If Val(Session("CODICEOSPITE")) > 0 Then
            Dim NomeOspiti As New ClsOspite

            NomeOspiti.Leggi(Session("DC_OSPITE"), Session("CODICEOSPITE"))

            Lbl_NomeOspite.Text = NomeOspiti.Nome
        End If

        Dim K1 As New Cls_SqlString

        Dim Barra As New Cls_BarraSenior

        If Not IsNothing(Session("RicercaAnagraficaSQLString")) Then
            K1 = Session("RicercaAnagraficaSQLString")
        End If

        Lbl_BarraSenior.Text = Barra.CodiceBarra(Application("SENIOR"), Session("UTENTE"), Page, K1)
        Lbl_Utente.Text = Session("UTENTE")

        Txt_DataAl.Text = Format(Now, "dd/MM/yyyy")
        Txt_DataDal.Text = Format(Now, "dd/MM/yyyy")

        Txt_AnnoCompetenzaDal.Text = Now.Year
        Txt_AnnoCompetenzaAl.Text = Now.Year

        Dd_MeseCompetenzaDal.SelectedValue = Now.Month
        Dd_MeseCompetenzaAl.SelectedValue = Now.Month

        Est_Data.Checked = True
        Est_Tipo.Checked = False


        Est_Tipo_CheckedChanged(sender, e)


        Call EseguiJS()
    End Sub

    Private Sub EseguiJS()
        Dim MyJs As String
        MyJs = "$(document).ready(function() {"
        MyJs = MyJs & " if (window.innerHeight>0) { $(""#BarraLaterale"").css(""height"",(window.innerHeight - 94) + ""px""); } else"
        MyJs = MyJs & "  { $(""#BarraLaterale"").css(""height"",(document.documentElement.offsetHeight - 94) + ""px"");  } "
        MyJs = MyJs & "var els = document.getElementsByTagName(""*"");"

        MyJs = MyJs & "for (var i=0;i<els.length;i++)"
        MyJs = MyJs & "if ( els[i].id ) { "
        MyJs = MyJs & " var appoggio =els[i].id; "
        MyJs = MyJs & " if (appoggio.match('Txt_Data')!= null) {  "
        MyJs = MyJs & " $(els[i]).mask(""99/99/9999"");"
        MyJs = MyJs & " $(els[i]).datepicker({ changeMonth: true,changeYear: true,  yearRange: ""1990:" & Year(Now) + 5 & """},$.datepicker.regional[""it""]);"

        MyJs = MyJs & "    }"


        MyJs = MyJs & "} "
        MyJs = MyJs & "});"

        'MyJs = "$(document).ready(function() { $('.myClass').keypress(function() { handleEnter($('.myClass').val(), true, true); } );     $('#" & Txt_ImportoPagato.ClientID & "').blur(function() { var ap = formatNumber ($('#" & Txt_ImportoPagato.ClientID & "').val(),2); $('#" & Txt_ImportoPagato.ClientID & "').val(ap); }); });"
        ScriptManager.RegisterStartupScript(Me, Me.GetType(), "visualizzaRitIm", MyJs, True)
    End Sub

    Protected Sub ImageButton6_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles ImageButton6.Click
        If Not IsDate(Txt_DataDal.Text) Then
            ClientScript.RegisterClientScriptBlock(Me.GetType(), "Errore", "VisualizzaErrore('Data dal non corretta');", True)
            Exit Sub
        End If
        If Not IsDate(Txt_DataAl.Text) Then
            ClientScript.RegisterClientScriptBlock(Me.GetType(), "Errore", "VisualizzaErrore('Data al non corretta');", True)
            Exit Sub
        End If

        Call CaricaGriglia()

        If MyTable.Rows.Count > 1 Then
            Response.Clear()
            Response.AddHeader("content-disposition", "attachment;filename=Senior.xls")
            Response.Charset = String.Empty
            Response.Cache.SetCacheability(HttpCacheability.NoCache)
            Response.ContentType = "application/vnd.xls"
            Dim stringWrite As New System.IO.StringWriter
            Dim htmlWrite As New HtmlTextWriter(stringWrite)

            form1.Controls.Clear()
            form1.Controls.Add(GridView1)

            form1.RenderControl(htmlWrite)

            Response.Write(stringWrite.ToString())
            Response.End()
        End If
    End Sub

    Protected Sub ImageButton5_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles ImageButton5.Click
        If Not IsDate(Txt_DataDal.Text) Then
            REM ClientScript.RegisterClientScriptBlock(Me.GetType(), "Errore", "VisualizzaErrore('Data dal non corretta');", True)
            ScriptManager.RegisterStartupScript(Me, Me.GetType(), "visualizzaA", "VisualizzaErrore('Data dal non corretta');", True)
            Call EseguiJS()
            Exit Sub
        End If
        If Not IsDate(Txt_DataAl.Text) Then
            REM ClientScript.RegisterClientScriptBlock(Me.GetType(), "Errore", "VisualizzaErrore('Data al non corretta');", True)
            ScriptManager.RegisterStartupScript(Me, Me.GetType(), "visualizzaA", "VisualizzaErrore('Data al non corretta');", True)
            Call EseguiJS()
            Exit Sub
        End If

        Call CaricaGriglia()
        Call EseguiJS()
    End Sub

    Protected Sub Btn_Esci_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles Btn_Esci.Click
        If Val(Session("CODICEOSPITE")) = 0 Then
            Response.Redirect("Menu_Visualizzazioni.aspx")
        Else
            If Request.Item("PAGINA") = "" Then
                Response.Redirect("Menu_Visualizzazioni.aspx")
            Else
                If Request.Item("PAGINA") = "DENARO" Or Request.Item("PAGINA") = "0,DENARO" Then
                    Response.Redirect("ElencoMovimentiDenaro.aspx?CodiceOspite=" & Session("CODICEOSPITE") & "&CentroServizio=" & Session("CODICESERVIZIO"))
                Else
                    Response.Redirect("RicercaAnagrafica.aspx?TIPO=EstrattoContoDenaro&CHIAMATA=RITORNO&PAGINA=" & Request.Item("PAGINA"))
                End If
            End If
        End If
    End Sub

    Private Sub imgBtnStampa_Command(sender As Object, e As CommandEventArgs) Handles imgBtnStampa.Command
        If IsDate(Txt_DataDal.Text) AndAlso IsDate(Txt_DataAl.Text) Then
            StampaEstrattoConto()
            ScriptManager.RegisterClientScriptBlock(Me, Me.GetType(), "StampaEstrattoConto", "openPopUp('Stampa_ReportXSD.aspx?REPORT=ESTRATTOCONTO','StampeEstrattoconto','width=800,height=600');", True)
        ElseIf Not IsDate(Txt_DataAl.Text) Then
            ScriptManager.RegisterStartupScript(Me, Me.GetType(), "visualizzaA", "VisualizzaErrore('Data al non corretta');", True)
        ElseIf Not IsDate(Txt_DataDal.Text) Then
            ScriptManager.RegisterStartupScript(Me, Me.GetType(), "visualizzaA", "VisualizzaErrore('Data dal non corretta');", True)
        End If

        Call EseguiJS()
    End Sub

    Private Sub StampaEstrattoConto()
        Dim MySql As String

        Dim CodiceOspite As String = ""

        Dim ConnectionString As String = Session("DC_OSPITIACCESSORI")
        Dim cn As OleDbConnection
        Dim cn1 As OleDbConnection

        Dim cmd1 As New OleDbCommand()
        Dim cmd As New OleDbCommand()
        Dim MyRd As OleDbDataReader
        Dim MyRdX As OleDbDataReader

        Dim cmdG As New OleDbCommand()
        Dim MyRdG As OleDbDataReader

        Dim cmdN As New OleDbCommand()
        Dim TotP As Double
        Dim TotV As Double
        Dim Txt_DataDalText As Date = Txt_DataDal.Text
        Dim Txt_DataAlText As Date = Txt_DataAl.Text

        cn1 = New Data.OleDb.OleDbConnection(Session("DC_OSPITE"))
        cn1.Open()

        cn = New Data.OleDb.OleDbConnection(ConnectionString)
        cn.Open()

        Dim Totale1 As Double = 0
        Dim Totale2 As Double = 0
        Dim Totale3 As Double = 0
        Dim Totale4 As Double = 0

        Dim Ordine As Integer = 0

        Dim Stampa As New StampeOspiti


        If Val(Session("CODICEOSPITE")) = 0 Then
            MySql = "Select * From AnagraficaComune Where Tipologia = 'O' ORDER BY Nome"
        Else
            MySql = "Select * From AnagraficaComune Where Tipologia = 'O' And CodiceOspite = " & Val(Session("CODICEOSPITE"))
        End If
        cmdG.CommandText = MySql
        cmdG.Parameters.AddWithValue("@DataDal", Txt_DataDalText)
        cmdG.Connection = cn1
        MyRdG = cmdG.ExecuteReader()


        Do While MyRdG.Read
            CodiceOspite = Val(campodb(MyRdG.Item("CodiceOspite")))

            If Est_Data.Checked = True Then
                MySql = "SELECT * FROM Denaro WHERE CODICEOSPITE = " & CodiceOspite & _
                            " AND DataRegistrazione >= ? " & _
                            " AND DataRegistrazione <= ? Order by DataRegistrazione"
                cmd.CommandText = MySql
                cmd.Parameters.Clear()
                cmd.Parameters.AddWithValue("@DataDal", Txt_DataDalText)
                cmd.Parameters.AddWithValue("@DataAl", Txt_DataAlText)
            Else
                MySql = "SELECT * FROM Denaro WHERE CODICEOSPITE = " & CodiceOspite & _
                            " AND (( AnnoCompetenza = ?  And MeseCompetenza >= ?) Or AnnoCompetenza > ?)" & _
                            " AND (( AnnoCompetenza = ?  And MeseCompetenza <= ?) Or AnnoCompetenza < ?) Order by DataRegistrazione"
                cmd.CommandText = MySql
                cmd.Parameters.Clear()
                cmd.Parameters.AddWithValue("@AnnoCompetenzaDal", Txt_AnnoCompetenzaDal.Text)
                cmd.Parameters.AddWithValue("@MeseCompetenzaDal", Dd_MeseCompetenzaDal.SelectedValue)
                cmd.Parameters.AddWithValue("@AnnoCompetenzaDal", Txt_AnnoCompetenzaDal.Text)

                cmd.Parameters.AddWithValue("@AnnoCompetenzaDal", Txt_AnnoCompetenzaAl.Text)
                cmd.Parameters.AddWithValue("@MeseCompetenzaDal", Dd_MeseCompetenzaAl.SelectedValue)
                cmd.Parameters.AddWithValue("@AnnoCompetenzaDal", Txt_AnnoCompetenzaAl.Text)
            End If

            cmd.Connection = cn
            MyRdX = cmd.ExecuteReader()
            If MyRdX.Read Then

                If Est_Data.Checked = True Then
                    MySql = "SELECT SUM(Importo) AS Totale FROM Denaro " & _
                            " WHERE CodiceOspite = " & CodiceOspite & _
                            " AND DataRegistrazione < ? " & _
                            " AND TipoOperazione = 'P'"
                    cmd1.CommandText = MySql
                    cmd1.Parameters.Clear()
                    cmd1.Parameters.AddWithValue("@DataDal", Txt_DataDalText)
                    cmd1.Connection = cn
                    MyRd = cmd1.ExecuteReader()
                    If MyRd.Read Then
                        TotP = CDbl(campodbN(MyRd.Item("Totale")))
                    End If
                    MyRd.Close()

                    MySql = "SELECT SUM(Importo) AS Totale FROM Denaro " & _
                            " WHERE CodiceOspite = " & CodiceOspite & _
                            " AND DataRegistrazione < ? " & _
                            " AND TipoOperazione = 'V'"
                    cmd1.CommandText = MySql
                    cmd1.Parameters.Clear()
                    cmd1.Parameters.AddWithValue("@DataDal", Txt_DataDalText)
                    cmd1.Connection = cn
                    MyRd = cmd1.ExecuteReader()
                    If MyRd.Read Then
                        TotV = CDbl(campodbN(MyRd.Item("Totale")))
                    End If
                    MyRd.Close()
                    Dim Rs_Stampa As System.Data.DataRow
                    Rs_Stampa = Stampa.Tables("EstrattoConto").NewRow
                    Rs_Stampa.Item("CodiceOspite") = CodiceOspite

                    Dim XNome As New ClsOspite
                    XNome.Leggi(Session("DC_OSPITE"), CodiceOspite)
                    Rs_Stampa.Item("Nome") = XNome.Nome
                    Rs_Stampa.Item("SaldoIniziale") = Format(Math.Round(TotV - TotP, 2), "#,##0.00")
                    Rs_Stampa.Item("Versamento") = 0
                    Rs_Stampa.Item("Prelievo") = 0
                    Rs_Stampa.Item("SaldoFinale") = 0
                    Rs_Stampa.Item("PeriodoDataInizio") = Txt_DataDal.Text
                    Rs_Stampa.Item("PeriodoDataFine") = Txt_DataAl.Text

                    Dim OspMov As New Cls_Movimenti

                    OspMov.CodiceOspite = CodiceOspite
                    OspMov.Data = campodb(MyRdX.Item("DataRegistrazione"))
                    OspMov.UltimaDataAccoglimentoSenzaCserv(Session("DC_OSPITE"))

                    Dim Cserv As New Cls_CentroServizio

                    Cserv.CENTROSERVIZIO = OspMov.CENTROSERVIZIO
                    Cserv.Leggi(Session("DC_OSPITE"), Cserv.CENTROSERVIZIO)


                    Rs_Stampa.Item("CentroServizio") = Cserv.DESCRIZIONE

                    Ordine = Ordine + 1

                    Rs_Stampa.Item("Ordine") = Format(Ordine, "000000")
                    Stampa.Tables("EstrattoConto").Rows.Add(Rs_Stampa)
                Else
                    TotV = 0
                    TotP = 0
                End If

                Totale1 = Totale1 + Math.Round(TotV - TotP, 2)


                'MySql = "SELECT * FROM Denaro WHERE CODICEOSPITE = " & CodiceOspite & _
                '        " AND DataRegistrazione >= ? " & _
                '        " AND DataRegistrazione <= ? " & _
                '        " ORDER BY DataRegistrazione, NumeroMovimento "
                'cmd1.CommandText = MySql
                'cmd1.Parameters.Clear()
                'cmd1.Parameters.AddWithValue("@DataDal", Txt_DataDalText)
                'cmd1.Parameters.AddWithValue("@DataAl", Txt_DataAlText)


                If Est_Data.Checked = True Then
                    MySql = "SELECT * FROM Denaro WHERE CODICEOSPITE = " & CodiceOspite & _
                                " AND DataRegistrazione >= ? " & _
                                " AND DataRegistrazione <= ? Order by DataRegistrazione"
                    cmd1.CommandText = MySql
                    cmd1.Parameters.Clear()
                    cmd1.Parameters.AddWithValue("@DataDal", Txt_DataDalText)
                    cmd1.Parameters.AddWithValue("@DataAl", Txt_DataAlText)
                Else
                    MySql = "SELECT * FROM Denaro WHERE CODICEOSPITE = " & CodiceOspite & _
                                " AND (( AnnoCompetenza = ?  And MeseCompetenza >= ?) Or AnnoCompetenza > ?)" & _
                                " AND (( AnnoCompetenza = ?  And MeseCompetenza <= ?) Or AnnoCompetenza < ?) Order by DataRegistrazione"
                    cmd1.CommandText = MySql
                    cmd1.Parameters.Clear()
                    cmd1.Parameters.AddWithValue("@AnnoCompetenzaDal", Txt_AnnoCompetenzaDal.Text)
                    cmd1.Parameters.AddWithValue("@MeseCompetenzaDal", Dd_MeseCompetenzaDal.SelectedValue)
                    cmd1.Parameters.AddWithValue("@AnnoCompetenzaDal", Txt_AnnoCompetenzaDal.Text)

                    cmd1.Parameters.AddWithValue("@AnnoCompetenzaDal", Txt_AnnoCompetenzaAl.Text)
                    cmd1.Parameters.AddWithValue("@MeseCompetenzaDal", Dd_MeseCompetenzaAl.SelectedValue)
                    cmd1.Parameters.AddWithValue("@AnnoCompetenzaDal", Txt_AnnoCompetenzaAl.Text)
                End If
                cmd1.Connection = cn
                MyRd = cmd1.ExecuteReader()
                Do While MyRd.Read
                    Dim Rs_Stampa3 As System.Data.DataRow
                    Rs_Stampa3 = Stampa.Tables("EstrattoConto").NewRow
                    Rs_Stampa3.Item("CodiceOspite") = CodiceOspite
                    Rs_Stampa3.Item("PeriodoDataInizio") = Txt_DataDal.Text
                    Rs_Stampa3.Item("PeriodoDataFine") = Txt_DataAl.Text

                    Dim XNome1 As New ClsOspite
                    XNome1.Leggi(Session("DC_OSPITE"), CodiceOspite)
                    Rs_Stampa3.Item("Nome") = XNome1.Nome

                    Dim MyTipoAdd As New Cls_Addebito

                    MyTipoAdd.Codice = campodb(MyRd.Item("TipoAddebito"))
                    MyTipoAdd.Leggi(Session("DC_OSPITE"), MyTipoAdd.Codice)


                    Rs_Stampa3.Item("Data") = campodb(MyRd.Item("DataRegistrazione"))
                    Rs_Stampa3.Item("Descrizione") = MyTipoAdd.Descrizione & "  " & campodb(MyRd.Item("Descrizione"))
                    Rs_Stampa3.Item("SaldoIniziale") = 0
                    Rs_Stampa3.Item("Versamento") = 0
                    Rs_Stampa3.Item("Prelievo") = 0
                    If MyRd.Item("TipoOperazione") = "V" Then
                        Totale2 = Totale2 + MyRd.Item("Importo")
                        Rs_Stampa3.Item("Versamento") = Format(MyRd.Item("Importo"), "#,##0.00")
                    End If
                    If MyRd.Item("TipoOperazione") = "P" Then
                        Totale3 = Totale3 + MyRd.Item("Importo")
                        Rs_Stampa3.Item("Prelievo") = Format(MyRd.Item("Importo"), "#,##0.00")
                    End If
                    Rs_Stampa3.Item("SaldoFinale") = 0

                    Rs_Stampa3.Item("Periodo") = campodb(MyRd.Item("MeseCompetenza")) & "/" & campodb(MyRd.Item("AnnoCompetenza"))
                    Dim OspMov As New Cls_Movimenti

                    OspMov.CodiceOspite = CodiceOspite
                    OspMov.Data = campodb(MyRd.Item("DataRegistrazione"))
                    OspMov.UltimaDataAccoglimentoSenzaCserv(Session("DC_OSPITE"))

                    Dim Cserv As New Cls_CentroServizio

                    Cserv.CENTROSERVIZIO = OspMov.CENTROSERVIZIO
                    Cserv.Leggi(Session("DC_OSPITE"), Cserv.CENTROSERVIZIO)


                    Rs_Stampa3.Item("CentroServizio") = Cserv.DESCRIZIONE

                    Ordine = Ordine + 1

                    Rs_Stampa3.Item("Ordine") = Format(Ordine, "000000")

                    Stampa.Tables("EstrattoConto").Rows.Add(Rs_Stampa3)
                Loop
                MyRd.Close()
                If Est_Data.Checked = True Then
                    Dim FinaleTotP As Double
                    Dim FinaleTotV As Double


                    MySql = "SELECT SUM(Importo) AS Totale FROM Denaro " & _
                            " WHERE CodiceOspite = " & CodiceOspite & _
                            " AND DataRegistrazione <= ? " & _
                            " AND TipoOperazione = 'P'"
                    cmd1.CommandText = MySql
                    cmd1.Parameters.Clear()
                    cmd1.Parameters.AddWithValue("@DataAl", Txt_DataAlText)
                    cmd1.Connection = cn
                    MyRd = cmd1.ExecuteReader()
                    If MyRd.Read Then
                        FinaleTotP = campodbN(MyRd.Item("Totale"))
                    End If
                    MyRd.Close()

                    MySql = "SELECT SUM(Importo) AS Totale FROM Denaro " & _
                            " WHERE CodiceOspite = " & CodiceOspite & _
                            " AND DataRegistrazione <= ? " & _
                            " AND TipoOperazione = 'V'"
                    cmd1.CommandText = MySql
                    cmd1.Parameters.Clear()
                    cmd1.Parameters.AddWithValue("@DataAl", Txt_DataAlText)
                    cmd1.Connection = cn
                    MyRd = cmd1.ExecuteReader()
                    If MyRd.Read Then
                        FinaleTotV = campodbN(MyRd.Item("Totale"))
                    End If
                    MyRd.Close()

                    Dim Rs_Stampa2 As System.Data.DataRow
                    Rs_Stampa2 = Stampa.Tables("EstrattoConto").NewRow
                    Rs_Stampa2.Item("CodiceOspite") = CodiceOspite
                    Dim XNome2 As New ClsOspite
                    XNome2.Leggi(Session("DC_OSPITE"), CodiceOspite)
                    Rs_Stampa2.Item("Nome") = XNome2.Nome
                    Rs_Stampa2.Item("Data") = ""
                    Rs_Stampa2.Item("PeriodoDataInizio") = Txt_DataDal.Text
                    Rs_Stampa2.Item("PeriodoDataFine") = Txt_DataAl.Text
                    Rs_Stampa2.Item("Descrizione") = ""
                    Rs_Stampa2.Item("SaldoIniziale") = 0
                    Rs_Stampa2.Item("Versamento") = 0
                    Rs_Stampa2.Item("Prelievo") = 0
                    Rs_Stampa2.Item("SaldoFinale") = Format(Math.Round(FinaleTotV - FinaleTotP, 2), "#,##0.00")

                    Dim OspMov As New Cls_Movimenti

                    OspMov.CodiceOspite = CodiceOspite
                    OspMov.Data = campodb(MyRdX.Item("DataRegistrazione"))
                    OspMov.UltimaDataAccoglimentoSenzaCserv(Session("DC_OSPITE"))

                    Dim Cserv As New Cls_CentroServizio

                    Cserv.CENTROSERVIZIO = OspMov.CENTROSERVIZIO
                    Cserv.Leggi(Session("DC_OSPITE"), Cserv.CENTROSERVIZIO)


                    Ordine = Ordine + 1

                    Rs_Stampa2.Item("Ordine") = Format(Ordine, "000000")

                    Rs_Stampa2.Item("CentroServizio") = Cserv.DESCRIZIONE
                    Stampa.Tables("EstrattoConto").Rows.Add(Rs_Stampa2)


                    Totale4 = Totale4 + Math.Round(FinaleTotV - FinaleTotP, 2)
                End If

            End If
            MyRdX.Close()

            If Chk_ADP.Checked = True Then
                MySql = "SELECT * FROM [ADDACR] Where (select COUNT(*) from AddebitoAccreditiProgrammati where addacr.chiaveselezione = AddebitoAccreditiProgrammati.Id and ADDACR.CodiceOspite =  AddebitoAccreditiProgrammati.CodiceOspite and ADDACR.CentroServizio =  AddebitoAccreditiProgrammati.CentroServizio) > 0 " & _
                  " AND Data  >= ? " & _
                    " AND Data <= ? " & _
                    " AND CodiceOspite = ? " & _
                    " Order by CodiceOspite "

                cmd.CommandText = MySql
                cmd.Parameters.Clear()
                cmd.Parameters.AddWithValue("@DataDal", Txt_DataDalText)
                cmd.Parameters.AddWithValue("@DataAl", Txt_DataAlText)
                cmd.Parameters.AddWithValue("@CodiceOspite", CodiceOspite)
                cmd.Connection = cn1
                MyRdX = cmd.ExecuteReader()
                Do While MyRdX.Read
                    Dim Rs_StampaP As System.Data.DataRow
                    Rs_StampaP = Stampa.Tables("EstrattoConto").NewRow


                    Rs_StampaP.Item("CodiceOspite") = CodiceOspite
                    Dim XNome3 As New ClsOspite
                    XNome3.Leggi(Session("DC_OSPITE"), CodiceOspite)

                    Dim MtpA As New Cls_Addebito

                    MtpA.Codice = campodb(MyRdX.Item("CodiceIva"))
                    MtpA.Leggi(Session("DC_OSPITE"), MtpA.Codice)

                    Rs_StampaP.Item("Nome") = XNome3.Nome
                    Rs_StampaP.Item("Data") = campodb(MyRdX.Item("Data"))
                    Rs_StampaP.Item("PeriodoDataInizio") = Txt_DataDal.Text
                    Rs_StampaP.Item("PeriodoDataFine") = Txt_DataAl.Text
                    Rs_StampaP.Item("Descrizione") = MtpA.Descrizione
                    Rs_StampaP.Item("SaldoIniziale") = 0
                    Rs_StampaP.Item("Versamento") = 0
                    Rs_StampaP.Item("Prelievo") = Format(MyRdX.Item("Importo"), "#,##0.00")
                    Rs_StampaP.Item("SaldoFinale") = 0
                    Ordine = Ordine + 1

                    Rs_StampaP.Item("Ordine") = Format(Ordine, "000000")
                    Dim OspMov As New Cls_Movimenti

                    OspMov.CodiceOspite = CodiceOspite
                    OspMov.Data = campodb(MyRd.Item("DataRegistrazione"))
                    OspMov.UltimaDataAccoglimentoSenzaCserv(Session("DC_OSPITE"))

                    Dim Cserv As New Cls_CentroServizio

                    Cserv.CENTROSERVIZIO = OspMov.CENTROSERVIZIO
                    Cserv.Leggi(Session("DC_OSPITE"), Cserv.CENTROSERVIZIO)


                    Rs_StampaP.Item("CentroServizio") = Cserv.DESCRIZIONE

                    Stampa.Tables("EstrattoConto").Rows.Add(Rs_StampaP)
                Loop
                MyRdX.Close()
            End If
        Loop
        MyRdG.Close()
        cn1.Close()
        cn.Close()


        Session("stampa") = Stampa
    End Sub




    Protected Sub Est_Data_CheckedChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles Est_Data.CheckedChanged
        If Est_Data.Checked = True Then
            Txt_DataAl.Enabled = True
            Txt_DataDal.Enabled = True
            Txt_DataAl.BackColor = System.Drawing.Color.White
            Txt_DataDal.BackColor = System.Drawing.Color.White

            Txt_AnnoCompetenzaDal.Enabled = False
            Txt_AnnoCompetenzaAl.Enabled = False
            Txt_AnnoCompetenzaDal.BackColor = System.Drawing.Color.Silver
            Txt_AnnoCompetenzaAl.BackColor = System.Drawing.Color.Silver

            Dd_MeseCompetenzaDal.BackColor = System.Drawing.Color.Silver
            Dd_MeseCompetenzaAl.BackColor = System.Drawing.Color.Silver
        Else
            Txt_DataAl.Enabled = False
            Txt_DataDal.Enabled = False
            Txt_DataAl.BackColor = System.Drawing.Color.Silver
            Txt_DataDal.BackColor = System.Drawing.Color.Silver

            Txt_AnnoCompetenzaDal.Enabled = True
            Txt_AnnoCompetenzaAl.Enabled = True
            Txt_AnnoCompetenzaDal.BackColor = System.Drawing.Color.White
            Txt_AnnoCompetenzaAl.BackColor = System.Drawing.Color.White

            Dd_MeseCompetenzaDal.BackColor = System.Drawing.Color.White
            Dd_MeseCompetenzaAl.BackColor = System.Drawing.Color.White
        End If
    End Sub

    Protected Sub Est_Tipo_CheckedChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles Est_Tipo.CheckedChanged
        If Est_Data.Checked = True Then
            Txt_DataAl.Enabled = True
            Txt_DataDal.Enabled = True
            Txt_DataAl.BackColor = System.Drawing.Color.White
            Txt_DataDal.BackColor = System.Drawing.Color.White


            Txt_AnnoCompetenzaDal.Enabled = False
            Txt_AnnoCompetenzaAl.Enabled = False
            Txt_AnnoCompetenzaDal.BackColor = System.Drawing.Color.Silver
            Txt_AnnoCompetenzaAl.BackColor = System.Drawing.Color.Silver

            Dd_MeseCompetenzaDal.BackColor = System.Drawing.Color.Silver
            Dd_MeseCompetenzaAl.BackColor = System.Drawing.Color.Silver

        Else
            Txt_DataAl.Enabled = False
            Txt_DataDal.Enabled = False
            Txt_DataAl.BackColor = System.Drawing.Color.Silver
            Txt_DataDal.BackColor = System.Drawing.Color.Silver


            Txt_AnnoCompetenzaDal.Enabled = True
            Txt_AnnoCompetenzaAl.Enabled = True
            Txt_AnnoCompetenzaDal.BackColor = System.Drawing.Color.White
            Txt_AnnoCompetenzaAl.BackColor = System.Drawing.Color.White

            Dd_MeseCompetenzaDal.BackColor = System.Drawing.Color.White
            Dd_MeseCompetenzaAl.BackColor = System.Drawing.Color.White
        End If
    End Sub
End Class
