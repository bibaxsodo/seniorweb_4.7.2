﻿Imports Microsoft.VisualBasic
Imports System.Data.OleDb
Imports System.Web.Hosting
Imports System.Data.SqlTypes


Partial Class OspitiWeb_Elenco_DatiPagamento
    Inherits System.Web.UI.Page

    Dim Tabella As New System.Data.DataTable("tabella")


    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Session("UTENTE") = "" Then
            Response.Redirect("..\Login.aspx")
            Exit Sub
        End If

        If Page.IsPostBack = True Then
            Exit Sub
        End If




        Dim K1 As New Cls_SqlString

        Dim Barra As New Cls_BarraSenior

        If Not IsNothing(Session("RicercaAnagraficaSQLString")) Then
            K1 = Session("RicercaAnagraficaSQLString")
        End If

        If Val(Request.Item("CodiceOspite")) > 0 And Request.Item("CentroServizio") <> "" Then

            Session("CODICESERVIZIO") = Request.Item("CentroServizio")
            Session("CODICEOSPITE") = Val(Request.Item("CodiceOspite"))
        End If

        ViewState("CODICESERVIZIO") = Session("CODICESERVIZIO")
        ViewState("CODICEOSPITE") = Session("CODICEOSPITE")



        Lbl_BarraSenior.Text = Barra.CodiceBarra(Application("SENIOR"), Session("UTENTE"), Page, K1)


        
        Dim x As New ClsOspite

        x.CodiceOspite = Session("CODICEOSPITE")
        x.Leggi(Session("DC_OSPITE"), x.CodiceOspite)

        Dim cs As New Cls_CentroServizio

        cs.Leggi(Session("DC_OSPITE"), Session("CODICESERVIZIO"))

        Lbl_NomeOspite.Text = x.Nome & " -  " & x.DataNascita & " - " & cs.DESCRIZIONE & "<i style=""font-size:small;"">(" & x.CodiceOspite & ")</i>"


        Dim cn As New OleDbConnection


        Dim ConnectionString As String = Session("DC_OSPITE")

        cn = New Data.OleDb.OleDbConnection(ConnectionString)

        cn.Open()

        Dim cmd As New OleDbCommand

        cmd.CommandText = "Select * From DatiPagamento Where CodiceOspite = ? Order By Data"
        cmd.Connection = cn
        cmd.Parameters.AddWithValue("@CodiceOspite", Session("CODICEOSPITE"))


        Tabella.Clear()
        Tabella.Columns.Add("Data Validita", GetType(String))
        Tabella.Columns.Add("Codice Parente", GetType(String))
        Tabella.Columns.Add("Nome", GetType(String))
        Tabella.Columns.Add("Data Mandato", GetType(String))
        Tabella.Columns.Add("Iban", GetType(String))


        Dim myPOSTreader As OleDbDataReader = cmd.ExecuteReader()
        Do While myPOSTreader.Read
            Dim myriga As System.Data.DataRow = Tabella.NewRow()

            myriga(0) = myPOSTreader.Item("Data")
            myriga(1) = campodb(myPOSTreader.Item("CodiceParente"))
            myriga(2) = ""
            If Val(campodb(myPOSTreader.Item("CodiceParente"))) > 0 Then
                Dim Parente As New Cls_Parenti

                Parente.CodiceOspite= campodb(myPOSTreader.Item("CodiceOspite"))
                Parente.CodiceParente = campodb(myPOSTreader.Item("CodiceParente"))
                Parente.Leggi(Session("DC_OSPITE"), Parente.CodiceOspite, Parente.CodiceParente)
                myriga(2) = Parente.Nome
            End If
            If Year(campodbd(myPOSTreader.Item("DataMandatoDebitore"))) = 1900 Then
                myriga(3) = ""
            Else
                myriga(3) = campodb(myPOSTreader.Item("DataMandatoDebitore"))
            End If
            myriga(4) = campodb(myPOSTreader.Item("CodiceInt")) & Format(Val(campodb(myPOSTreader.Item("NumeroControllo"))), "00") & campodb(myPOSTreader.Item("Cin")) & Format(Val(campodb(myPOSTreader.Item("Abi"))), "00000") & Format(Val(campodb(myPOSTreader.Item("Cab"))), "00000") & Format(Val(campodb(myPOSTreader.Item("CCBancario"))), "000000000000")

            Tabella.Rows.Add(myriga)
        Loop

        myPOSTreader.Close()
        cn.Close()


        ViewState("Appoggio") = Tabella

        Grd_ImportoOspite.AutoGenerateColumns = True

        Grd_ImportoOspite.DataSource = Tabella

        Grd_ImportoOspite.DataBind()

        Btn_Nuovo.Visible = True
    End Sub

    Function campodbD(ByVal oggetto As Object) As Date
        If IsDBNull(oggetto) Then
            Return Nothing
        Else
            Return oggetto
        End If
    End Function

    Protected Sub Grd_ImportoOspite_PageIndexChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewPageEventArgs) Handles Grd_ImportoOspite.PageIndexChanging
        Tabella = ViewState("Appoggio")
        Grd_ImportoOspite.PageIndex = e.NewPageIndex
        Grd_ImportoOspite.DataSource = Tabella
        Grd_ImportoOspite.DataBind()
    End Sub

    Protected Sub Grd_ImportoOspite_RowCommand(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewCommandEventArgs) Handles Grd_ImportoOspite.RowCommand

        If e.CommandName = "Seleziona" Then


            Dim d As Integer


            Tabella = ViewState("Appoggio")

            d = Val(e.CommandArgument)


            Dim DATA As String

            DATA = Tabella.Rows(d).Item(0).ToString

            If Val(Request.Item("CodiceOspite")) > 0 And Request.Item("CentroServizio") <> "" Then
                Session("CODICESERVIZIO") = Request.Item("CentroServizio")
                Session("CODICEOSPITE") = Val(Request.Item("CodiceOspite"))
            End If


            Response.Redirect("DatiPagamento.aspx?CodiceOspite=" & Session("CODICEOSPITE") & "&CentroServizio=" & Session("CODICESERVIZIO") & "&DATA=" & DATA & "&CodiceParente=" & Tabella.Rows(d).Item(1).ToString)
        End If
    End Sub

    Protected Sub Grd_ImportoOspite_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles Grd_ImportoOspite.SelectedIndexChanged

    End Sub

    Protected Sub Btn_Esci_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles Btn_Esci.Click
        If Request.Item("PAGINA") = "" Then
            Response.Redirect("RicercaAnagrafica.aspx")
        Else
            Response.Redirect("RicercaAnagrafica.aspx?CHIAMATA=RITORNO&PAGINA=" & Request.Item("PAGINA"))
        End If
    End Sub


    Protected Sub Btn_Nuovo_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles Btn_Nuovo.Click

        If Val(Request.Item("CodiceOspite")) > 0 And Request.Item("CentroServizio") <> "" Then
            Session("CODICESERVIZIO") = Request.Item("CentroServizio")
            Session("CODICEOSPITE") = Val(Request.Item("CodiceOspite"))
        End If

        Response.Redirect("DatiPagamento.aspx?CodiceOspite=" & Session("CODICEOSPITE") & "&CentroServizio=" & Session("CODICESERVIZIO"))
    End Sub

    Function campodbn(ByVal oggetto As Object) As Double
        If IsDBNull(oggetto) Then
            Return 0
        Else
            Return oggetto
        End If
    End Function

    Function campodb(ByVal oggetto As Object) As String
        If IsDBNull(oggetto) Then
            Return ""
        Else
            Return oggetto
        End If
    End Function
End Class

