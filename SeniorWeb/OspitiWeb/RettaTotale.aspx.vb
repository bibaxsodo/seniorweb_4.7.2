﻿Imports System.Web.Hosting
Imports System.Data.OleDb

Partial Class RettaTotale
    Inherits System.Web.UI.Page
    Dim MyTable As New System.Data.DataTable("tabella")
    Dim MyDataSet As New System.Data.DataSet()


    Sub loadpagina()
        Dim x As New ClsOspite
        Dim d As New Cls_rettatotale


        Dim ConnectionString As String = Session("DC_OSPITE")


        x.Leggi(ConnectionString, Session("CODICEOSPITE"))



        d.loaddati(ConnectionString, Session("CODICEOSPITE"), Session("CODICESERVIZIO"), MyTable)

        ViewState("App_Retta") = MyTable

        Grd_Retta.AutoGenerateColumns = False

        Grd_Retta.DataSource = MyTable

        Grd_Retta.DataBind()

    End Sub

  

    Private Sub DisabilitaCache()

        Response.Cache.SetCacheability(HttpCacheability.NoCache)

        Response.Headers.Add("Cache-Control", "no-cache, no-store")


        Response.Cache.SetExpires(DateTime.UtcNow.AddYears(-1))
    End Sub

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
    
        If IsNothing(Session("DC_OSPITE")) Then
            Response.Redirect("..\Login.aspx")
            Exit Sub
        End If
        If Session("ABILITAZIONI").IndexOf("<PORTINERIA>") > 0 Then
            Response.Redirect("..\MainPortineria.aspx")
        End If

        Dim K1 As New Cls_SqlString

        Dim Barra As New Cls_BarraSenior

        If Not IsNothing(Session("RicercaAnagraficaSQLString")) Then
            K1 = Session("RicercaAnagraficaSQLString")
        End If

        Lbl_BarraSenior.Text = Barra.CodiceBarra(Application("SENIOR"), Session("UTENTE"), Page, K1)

        If Page.IsPostBack = False Then

            'DisabilitaCache()

            If Val(Request.Item("CodiceOspite")) > 0 And Request.Item("CentroServizio") <> "" Then

                Session("CODICESERVIZIO") = Request.Item("CentroServizio")
                Session("CODICEOSPITE") = Val(Request.Item("CodiceOspite"))
            End If

            ViewState("CODICESERVIZIO") = Session("CODICESERVIZIO")
            ViewState("CODICEOSPITE") = Session("CODICEOSPITE")

            Lbl_BarraSenior.Text = Barra.CodiceBarra(Application("SENIOR"), Session("UTENTE"), Page, K1)
     

            Call loadpagina()

            Dim x As New ClsOspite

            x.CodiceOspite = Session("CODICEOSPITE")
            x.Leggi(Session("DC_OSPITE"), x.CodiceOspite)

            Dim cs As New Cls_CentroServizio

            cs.Leggi(Session("DC_OSPITE"), Session("CODICESERVIZIO"))

            Lbl_NomeOspite.Text = x.Nome & " -  " & x.DataNascita & " - " & cs.DESCRIZIONE & "<i style=""font-size:small;"">(" & x.CodiceOspite & ")</i>"

            'RB_Comune.Attributes.Add("onclick", "clientJsFunction()")
            'RB_Ospite.Attributes.Add("onclick", "clientJsFunction()")
            'RB_Parente.Attributes.Add("onclick", "clientJsFunction()")

            RB_Ospite.Checked = True

            
            Call EseguiJS()
        End If



    End Sub


    Private Sub InserisciRiga()
        UpDateTable()
        MyTable = ViewState("App_Retta")
        Dim myriga As System.Data.DataRow = MyTable.NewRow()
        myriga(0) = ""
        myriga(1) = 0
        myriga(3) = ""
        MyTable.Rows.Add(myriga)

        ViewState("App_Retta") = MyTable
        Grd_Retta.AutoGenerateColumns = False

        Grd_Retta.DataSource = MyTable

        Grd_Retta.DataBind()


        Dim TxtData As TextBox = DirectCast(Grd_Retta.Rows(Grd_Retta.Rows.Count - 1).FindControl("TxtData"), TextBox)

        TxtData.Focus()

        ClientScript.RegisterClientScriptBlock(Me.GetType(), "setFocus", "$(document).ready(function() {  setTimeout(function(){ document.getElementById('" + TxtData.ClientID + "').focus(); }); }, 500);", True)

        Call EseguiJS()
    End Sub

    Protected Sub Grd_Retta_DataBound(ByVal sender As Object, ByVal e As System.EventArgs) Handles Grd_Retta.DataBound
        REM Call EseguiJS()
    End Sub

    Protected Sub Grd_Retta_RowCommand(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewCommandEventArgs) Handles Grd_Retta.RowCommand
        If (e.CommandName = "Inserisci") Then

            Call InserisciRiga()

        End If

    End Sub






    Protected Sub Grd_Retta_RowDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs) Handles Grd_Retta.RowDataBound
        If e.Row.RowType = DataControlRowType.DataRow Then


            Dim TxtData As TextBox = DirectCast(e.Row.FindControl("TxtData"), TextBox)

            TxtData.Text = MyTable.Rows(e.Row.RowIndex).Item(0).ToString

            Dim TxtImporto As TextBox = DirectCast(e.Row.FindControl("TxtImporto"), TextBox)
            TxtImporto.Text = MyTable.Rows(e.Row.RowIndex).Item(1).ToString
            Dim MyJs As String

            MyJs = "$('#" & TxtImporto.ClientID & "').keypress(function() { ForceNumericInput($('#" & TxtImporto.ClientID & "').val(), true, true); } );  $('#" & TxtImporto.ClientID & "').blur(function() { var ap = formatNumber ($('#" & TxtImporto.ClientID & "').val(),2); $('#" & TxtImporto.ClientID & "').val(ap); }); "
            ScriptManager.RegisterStartupScript(Me, Me.GetType(), "visualizzaD", MyJs, True)


            Dim DD_Tipo As DropDownList = DirectCast(e.Row.FindControl("DD_Tipo"), DropDownList)

            DD_Tipo.SelectedValue = MyTable.Rows(e.Row.RowIndex).Item(2).ToString


            Dim DD_TipoRetta As DropDownList = DirectCast(e.Row.FindControl("DD_TipoRetta"), DropDownList)

            Dim k As New Cls_TipoRetta

            k.UpDateDropBox(Session("DC_OSPITE"), DD_TipoRetta)

            DD_TipoRetta.SelectedValue = MyTable.Rows(e.Row.RowIndex).Item(3).ToString


            Dim TxtSort As TextBox = DirectCast(e.Row.FindControl("TxtSort"), TextBox)
            Dim Lbl_Sort As Label = DirectCast(e.Row.FindControl("Lbl_Sortablelist"), Label)

            TxtSort.Text = MyTable.Rows(e.Row.RowIndex).Item(4).ToString.Replace("<li class=""ui-state-default"" style=""color:#AAAAAA;"">", "").Replace("<li class=""ui-state-default"" style=""color: rgb(170, 170, 170);"">", "")

            Dim vettore(100) As String

            Dim i As Integer

            vettore = Split(TxtSort.Text, "|")


            Lbl_Sort.Text = "<ul id=""sortable" & e.Row.RowIndex + 1 & """ class=""connectedSortable ui-sortable"">"

            For i = 0 To vettore.Length - 1
                If vettore(i).Trim <> "" Then
                    Lbl_Sort.Text = Lbl_Sort.Text & "<li class=""ui-state-default"" style=""color:#AAAAAA;"">" & vettore(i).Trim & "</li>"
                End If
            Next
            Lbl_Sort.Text = Lbl_Sort.Text & "</ul>"

        End If
    End Sub


    Private Sub EseguiJS(Optional ByVal NonUpDate As Boolean = False)
        Dim MyJs As String
        MyJs = "$(document).ready(function() {" & vbNewLine

        MyJs = MyJs & "var els = document.getElementsByTagName(""*"");" & vbNewLine
        MyJs = MyJs & "CreaLista();" & vbNewLine
        MyJs = MyJs & "for (var i=0;i<els.length;i++)" & vbNewLine
        MyJs = MyJs & "if ( els[i].id ) { " & vbNewLine
        MyJs = MyJs & " var appoggio =els[i].id; " & vbNewLine
        MyJs = MyJs & " if (appoggio.match('TxtData')!= null) {  " & vbNewLine
        MyJs = MyJs & " $(els[i]).mask(""99/99/9999"");" & vbNewLine
        MyJs = MyJs & " $(els[i]).datepicker({ changeMonth: true,changeYear: true,  yearRange: ""1990:" & Year(Now) + 5 & """},$.datepicker.regional[""it""]);"
        MyJs = MyJs & "    }" & vbNewLine

        MyJs = MyJs & " if ((appoggio.match('TxtImporto')!= null) ) {  " & vbNewLine
        MyJs = MyJs & " $(els[i]).keypress(function() { ForceNumericInput($(this).val(), true, true); } ); " & vbNewLine
        MyJs = MyJs & " $(els[i]).blur(function() { var ap = formatNumber($(this).val(),2); $(this).val(ap); });" & vbNewLine
        MyJs = MyJs & "    }" & vbNewLine


        MyJs = MyJs & "} " & vbNewLine


        Dim i As Integer


        For i = 0 To Grd_Retta.Rows.Count
            MyJs = MyJs & " $(""#sortable" & i + 1 & ", #sortableLista"").sortable({" & vbNewLine
            MyJs = MyJs & "connectWith: "".connectedSortable""" & vbNewLine
            MyJs = MyJs & "}).disableSelection();" & vbNewLine

            MyJs = MyJs & " $(""#sortable" & i + 1 & ", #sortableLista"").sortable({" & vbNewLine
            MyJs = MyJs & "receive : function(event, ui) { CreaLista(); }" & vbNewLine
            MyJs = MyJs & "}).disableSelection();" & vbNewLine
        Next



        MyJs = MyJs & "});" & vbNewLine


        MyJs = MyJs & "function Update() {  " & vbNewLine
        For i = 0 To Grd_Retta.Rows.Count - 1
            Dim TxtSort As TextBox = DirectCast(Grd_Retta.Rows(i).FindControl("TxtSort"), TextBox)

            MyJs = MyJs & " var appoggio = $('#sortable" & i + 1 & "').html();" & vbNewLine
            MyJs = MyJs & " if (appoggio != null) {"
            MyJs = MyJs & " appoggio = replaceAll('<li class=""ui-state-default"">','',appoggio);" & vbNewLine
            MyJs = MyJs & " appoggio = replaceAll('<li style="""" class=""ui-state-default"">','',appoggio);" & vbNewLine
            MyJs = MyJs & " appoggio = replaceAll('</li>','|',appoggio);" & vbNewLine
            MyJs = MyJs & " } else { appoggio ='';}"


            MyJs = MyJs & " $('#" & TxtSort.ClientID & "').val(appoggio);" & vbNewLine

        Next
            

        MyJs = MyJs & " }" & vbNewLine

        Dim cn As OleDbConnection



        cn = New Data.OleDb.OleDbConnection(Session("DC_OSPITE"))

        cn.Open()
        Dim cmd As New OleDbCommand()
        Dim Condizione As String = ""
        '(SCADENZA IS NULL OR SCADENZA > ?) AND (((CentroServizio Is Null or CentroServizio  = '') and (Struttura Is Null or Struttura  = '')) Or CentroServizio  = ?  Or ((CentroServizio Is Null or CentroServizio  = '') and  Struttura = ?))

        If RB_iva4.Visible = True Then

            If RB_iva4.Checked = True Then
                Condizione = " And CodiceIva = '04' "
            End If
            If RB_iva5.Checked = True Then
                Condizione = " And CodiceIva = '05' "
            End If
            If RB_ivaA.Checked = True Then
                Condizione = " And (CodiceIva <> '04' AND CodiceIva <> '05') "
            End If
        End If


        If RB_Ospite.Checked = True Then
            cmd.CommandText = ("select * from TABELLAEXTRAFISSI Where (SCADENZA IS NULL OR SCADENZA > GETDATE()) AND (((CENTROSERVIZIO Is Null or CENTROSERVIZIO  = '') and (Struttura Is Null or Struttura  = '')) Or CENTROSERVIZIO  = ?  Or ((CENTROSERVIZIO Is Null or CENTROSERVIZIO  = '') and  Struttura = ?))  And RIPARTIZIONE  ='O' " & Condizione & "  Order by  Descrizione")
        End If
        If RB_Comune.Checked = True Then
            cmd.CommandText = ("select * from TABELLAEXTRAFISSI Where (SCADENZA IS NULL OR SCADENZA > GETDATE()) AND (((CENTROSERVIZIO Is Null or CENTROSERVIZIO  = '') and (Struttura Is Null or Struttura  = '')) Or CENTROSERVIZIO  = ?  Or ((CENTROSERVIZIO Is Null or CENTROSERVIZIO  = '') and  Struttura = ?)) And RIPARTIZIONE  ='C'  " & Condizione & " Order by  Descrizione")
        End If
        If RB_Parente.Checked = True Then
            cmd.CommandText = ("select * from TABELLAEXTRAFISSI Where (SCADENZA IS NULL OR SCADENZA > GETDATE()) AND (((CENTROSERVIZIO Is Null or CENTROSERVIZIO  = '') and (Struttura Is Null or Struttura  = '')) Or CENTROSERVIZIO  = ?  Or ((CENTROSERVIZIO Is Null or CENTROSERVIZIO  = '') and  Struttura = ?)) And RIPARTIZIONE  ='P' " & Condizione & "  Order by  Descrizione")
        End If
        cmd.Connection = cn
        cmd.Parameters.AddWithValue("@centroServizio", Session("CODICESERVIZIO"))


        Dim CS As New Cls_CentroServizio

        CS.CENTROSERVIZIO = Session("CODICESERVIZIO")
        CS.Leggi(Session("DC_OSPITE"), CS.CENTROSERVIZIO)

        cmd.Parameters.AddWithValue("@Struttura", CS.Villa)

        Dim Appoggio As String

        Appoggio = "<ul id=""sortableLista"" class=""connectedSortable ui-sortable"">"

        Dim myPOSTreader As OleDbDataReader = cmd.ExecuteReader()
        Do While myPOSTreader.Read
            Dim Ripartizione As String
            If campodb(myPOSTreader.Item("RIPARTIZIONE")) = "O" Then
                Ripartizione = "(O)"
            End If
            If campodb(myPOSTreader.Item("RIPARTIZIONE")) = "P" Then
                Ripartizione = "(P)"
            End If
            If campodb(myPOSTreader.Item("RIPARTIZIONE")) = "C" Then
                Ripartizione = "(C)"
            End If

            Appoggio = Appoggio & "<li class=""ui-state-default""  style=""color:#AAAAAA;"">" & campodb(myPOSTreader.Item("Descrizione")) & " " & campodb(myPOSTreader.Item("DescrizioneInterna")) & " <b><font codiceiva=""" & campodb(myPOSTreader.Item("CodiceExtra")) & """ color=""#000000"">€ " & Format(campodbN(myPOSTreader.Item("IMPORTO")), "#,##0.00") & "</font> " & campodb(myPOSTreader.Item("TipoImporto")) & " " & Ripartizione & "</b></li>"

        Loop
        myPOSTreader.Close()
        cn.Close()
        Appoggio = Appoggio & "</ul>"


        MyJs = MyJs & "function CreaLista() { "
        MyJs = MyJs & "$('#MyID').empty();"
        MyJs = MyJs & "$('#MyID').html('" & Appoggio & "');"
        For i = 0 To Grd_Retta.Rows.Count - 1
            MyJs = MyJs & " $(""#sortable" & i + 1 & ", #sortableLista"").sortable({" & vbNewLine
            MyJs = MyJs & "connectWith: "".connectedSortable""" & vbNewLine
            MyJs = MyJs & "}).disableSelection();" & vbNewLine

            MyJs = MyJs & " $(""#sortable" & i + 1 & ", #sortableLista"").sortable({" & vbNewLine
            MyJs = MyJs & "receive : function(event, ui) { CreaLista(); }" & vbNewLine
            MyJs = MyJs & "}).disableSelection();" & vbNewLine
        Next
        MyJs = MyJs & "}"

        'MyJs = "$(document).ready(function() { $('.myClass').keypress(function() { handleEnter($('.myClass').val(), true, true); } );     $('#" & Txt_ImportoPagato.ClientID & "').blur(function() { var ap = formatNumber ($('#" & Txt_ImportoPagato.ClientID & "').val(),2); $('#" & Txt_ImportoPagato.ClientID & "').val(ap); }); });"
        ScriptManager.RegisterStartupScript(Me, Me.GetType(), "visualizzaRitIm" & Format(Now, "HHmmss"), MyJs, True)


    End Sub



    Function campodb(ByVal oggetto As Object) As String
        If IsDBNull(oggetto) Then
            Return ""
        Else
            Return oggetto
        End If
    End Function
    Function campodbN(ByVal oggetto As Object) As Double
        If IsDBNull(oggetto) Then
            Return 0
        Else
            Return oggetto
        End If
    End Function
    Private Sub UpDateTable()
        Dim i As Integer

        MyTable.Clear()
        MyTable.Columns.Clear()
        MyTable.Columns.Add("Data", GetType(String))
        MyTable.Columns.Add("Importo", GetType(String))
        MyTable.Columns.Add("Tipo", GetType(String))
        MyTable.Columns.Add("TipoRetta", GetType(String))
        MyTable.Columns.Add("Extra", GetType(String))

        For i = 0 To Grd_Retta.Rows.Count - 1

            Dim TxtData As TextBox = DirectCast(Grd_Retta.Rows(i).FindControl("TxtData"), TextBox)
            Dim TxtImporto As TextBox = DirectCast(Grd_Retta.Rows(i).FindControl("TxtImporto"), TextBox)
            Dim DD_Tipo As DropDownList = DirectCast(Grd_Retta.Rows(i).FindControl("DD_Tipo"), DropDownList)
            Dim DD_TipoRetta As DropDownList = DirectCast(Grd_Retta.Rows(i).FindControl("DD_TipoRetta"), DropDownList)
            Dim TxtSort As TextBox = DirectCast(Grd_Retta.Rows(i).FindControl("TxtSort"), TextBox)

            Dim myrigaR As System.Data.DataRow = MyTable.NewRow()


            myrigaR(0) = TxtData.Text
            myrigaR(1) = TxtImporto.Text
            myrigaR(2) = DD_Tipo.Text
            myrigaR(3) = DD_TipoRetta.Text
            myrigaR(4) = TxtSort.Text

            MyTable.Rows.Add(myrigaR)
        Next
        ViewState("App_Retta") = MyTable

    End Sub

    Protected Sub Grd_Retta_RowDeleted(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewDeletedEventArgs) Handles Grd_Retta.RowDeleted

    End Sub


    Protected Sub Grd_Retta_RowDeleting(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewDeleteEventArgs) Handles Grd_Retta.RowDeleting

        UpDateTable()
        MyTable = ViewState("App_Retta")
        Try
            If Request.UserAgent.Contains("Chrome") Then
                If IsDBNull(Session("RI_TIMER")) Or DateDiff("s", Session("RI_TIMER"), Now) > 1 Then
                    MyTable.Rows.RemoveAt(e.RowIndex)
                    If MyTable.Rows.Count = 0 Then
                        Dim myriga As System.Data.DataRow = MyTable.NewRow()
                        myriga(1) = 0
                        myriga(2) = 0
                        MyTable.Rows.Add(myriga)
                    End If
                    Session("RI_TIMER") = Now
                End If
            Else
                MyTable.Rows.RemoveAt(e.RowIndex)
                If MyTable.Rows.Count = 0 Then
                    Dim myriga As System.Data.DataRow = MyTable.NewRow()
                    myriga(1) = 0
                    myriga(2) = 0
                    MyTable.Rows.Add(myriga)
                End If
            End If
        Catch ex As Exception

        End Try


        ViewState("App_Retta") = MyTable

        Grd_Retta.AutoGenerateColumns = False

        Grd_Retta.DataSource = MyTable

        Grd_Retta.DataBind()
        Call EseguiJS()

        e.Cancel = True
    End Sub

    Protected Sub Btn_Modifica_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles Btn_Modifica.Click
        Dim i As Integer
        Dim T As Integer

        Session("CODICESERVIZIO") = ViewState("CODICESERVIZIO")
        Session("CODICEOSPITE") = ViewState("CODICEOSPITE")
        If Val(Session("CODICEOSPITE")) = 0 Then
            ClientScript.RegisterClientScriptBlock(Me.GetType(), "Errore", "VisualizzaErrore('Devi ricercare l ospite');", True)
            Exit Sub
        End If

        For i = 0 To Grd_Retta.Rows.Count - 1
            Dim TxtData As TextBox = DirectCast(Grd_Retta.Rows(i).FindControl("TxtData"), TextBox)
            Dim TxtImporto As TextBox = DirectCast(Grd_Retta.Rows(i).FindControl("TxtImporto"), TextBox)
            Dim DD_Tipo As DropDownList = DirectCast(Grd_Retta.Rows(i).FindControl("DD_Tipo"), DropDownList)


            For T = 0 To Grd_Retta.Rows.Count - 1
                If T <> i Then
                    Dim TxtDatax As TextBox = DirectCast(Grd_Retta.Rows(T).FindControl("TxtData"), TextBox)

                    If TxtDatax.Text = TxtData.Text Then
                        ClientScript.RegisterClientScriptBlock(Me.GetType(), "Errore", "VisualizzaErrore('Data duplicata per riga " & i + 1 & " e riga " & T + 1 & "');", True)
                        Call EseguiJS()
                        Exit Sub
                    End If
                End If
            Next
        Next


        Dim Log1 As New Cls_LogPrivacy

        Log1.LogPrivacy(Application("SENIOR"), Session("CLIENTE"), Session("UTENTE"), Session("UTENTEIMPER"), Page.GetType.Name.ToUpper, Val(Session("CODICEOSPITE")), 0, "", "", 0, "", "M", "OSPITE", "")

        Dim Log As New Cls_LogPrivacy

        Dim ConvT As New Cls_DataTableToJson
        Dim OldTable As New System.Data.DataTable("tabellaOld")


        Dim OldDatiPar As New Cls_rettatotale

        OldDatiPar.loaddati(Session("DC_OSPITE"), Session("CODICEOSPITE"), Session("CODICESERVIZIO"), OldTable)
        Dim AppoggioJS As String = ConvT.DataTableToJsonObj(OldTable)

        Log.LogPrivacy(Application("SENIOR"), Session("CLIENTE"), Session("UTENTE"), Session("UTENTEIMPER"), Page.GetType.Name.ToUpper, Session("CODICEOSPITE"), 0, "", "", 0, "", "M", "RETTETOTALE", AppoggioJS)


        Dim X1 As New Cls_rettatotale

        X1.CODICEOSPITE = Session("CODICEOSPITE")
        X1.CENTROSERVIZIO = Session("CODICESERVIZIO")

        X1.Utente = Session("UTENTE")
        X1.DataAggiornamento = Now


        Call UpDateTable()
        MyTable = ViewState("App_Retta")
        X1.AggiornaDaTabella(Session("DC_OSPITE"), MyTable)



        Dim MyJs As String
        MyJs = "alert('Modifica Effettuata');"
        ScriptManager.RegisterClientScriptBlock(Me, Me.GetType(), "ModScM", MyJs, True)
        Call loadpagina()

        Call EseguiJS()
    End Sub

    Protected Sub Btn_Esci_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles Btn_Esci.Click
        If Request.Item("PAGINA") = "" Then
            Response.Redirect("RicercaAnagrafica.aspx")
        Else
            Response.Redirect("RicercaAnagrafica.aspx?CHIAMATA=RITORNO&PAGINA=" & Request.Item("PAGINA"))
        End If
    End Sub

    Protected Sub Btn_Modifica_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Btn_Modifica.Load

    End Sub

    Protected Sub BTN_InserisciRiga_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles BTN_InserisciRiga.Click
        Call InserisciRiga()
    End Sub

    Protected Sub RB_Comune_CheckedChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles RB_Comune.CheckedChanged
        Call UpDateTable()
        Call EseguiJS(True)

        MyTable = ViewState("App_Retta")

        Grd_Retta.AutoGenerateColumns = False

        Grd_Retta.DataSource = MyTable

        Grd_Retta.DataBind()
    End Sub

    Protected Sub RB_Ospite_CheckedChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles RB_Ospite.CheckedChanged
        Call UpDateTable()
        Call EseguiJS(True)
        MyTable = ViewState("App_Retta")

        Grd_Retta.AutoGenerateColumns = False

        Grd_Retta.DataSource = MyTable

        Grd_Retta.DataBind()
    End Sub

    Protected Sub RB_Parente_CheckedChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles RB_Parente.CheckedChanged
        Call UpDateTable()
        Call EseguiJS(True)
        MyTable = ViewState("App_Retta")

        Grd_Retta.AutoGenerateColumns = False

        Grd_Retta.DataSource = MyTable

        Grd_Retta.DataBind()
    End Sub

    Protected Sub RB_iva5_CheckedChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles RB_iva5.CheckedChanged
        Call UpDateTable()
        Call EseguiJS(True)
        MyTable = ViewState("App_Retta")

        Grd_Retta.AutoGenerateColumns = False

        Grd_Retta.DataSource = MyTable

        Grd_Retta.DataBind()
    End Sub

    Protected Sub RB_iva4_CheckedChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles RB_iva4.CheckedChanged
        Call UpDateTable()
        Call EseguiJS(True)
        MyTable = ViewState("App_Retta")

        Grd_Retta.AutoGenerateColumns = False

        Grd_Retta.DataSource = MyTable

        Grd_Retta.DataBind()
    End Sub

    Protected Sub RB_ivaA_CheckedChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles RB_ivaA.CheckedChanged
        Call UpDateTable()
        Call EseguiJS(True)
        MyTable = ViewState("App_Retta")

        Grd_Retta.AutoGenerateColumns = False

        Grd_Retta.DataSource = MyTable

        Grd_Retta.DataBind()
    End Sub


End Class
