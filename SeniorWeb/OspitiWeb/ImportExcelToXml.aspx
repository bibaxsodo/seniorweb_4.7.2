﻿<%@ Page Language="VB" AutoEventWireup="false" Inherits="OspitiWeb_ImportExcelToXml" EnableEventValidation="false" CodeFile="ImportExcelToXml.aspx.vb" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="sau" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
    <meta http-equiv="x-ua-compatible" content="IE=9" />
    <title>Import XLS export XML</title>
    <asp:PlaceHolder runat="server">
        <%: System.Web.Optimization.Styles.Render("~/Content/AjaxControlToolkit/Styles/Bundle") %>
    </asp:PlaceHolder>
    <link href="ospiti.css?ver=11" rel="stylesheet" type="text/css" />
    <link rel="shortcut icon" href="images/SENIOR.ico" />
    <link href="js/jquery.autocomplete.css" rel="stylesheet" type="text/css" />


    <script src="js/jquery-1.5.1.min.js" type="text/javascript"></script>
    <script src="js/jquery.autocomplete.js" type="text/javascript"></script>
    <script src="js/soapclient.js" type="text/javascript"></script>
    <script src="/js/convertinumero.js" type="text/javascript"></script>



    <script src="js/jquery.maskedinput-1.3.min.js" type="text/javascript"></script>
    <script src="/js/formatnumer.js" type="text/javascript"></script>
    <script src="js/JSErrore.js" type="text/javascript"></script>

    <link rel="stylesheet" href="dialogbox/redips-dialog.css" type="text/css" media="screen" />
    <script type="text/javascript" src="dialogbox/redips-dialog-min.js"></script>
    <script type="text/javascript" src="dialogbox/script.js"></script>

    <link rel="stylesheet" href="jqueryui/jquery-ui.css" type="text/css" />
    <script src="jqueryui/jquery-ui.js" type="text/javascript"></script>
    <script src="jqueryui/datapicker-it.js" type="text/javascript"></script>
    <link rel="stylesheet" href="dialogbox/redips-dialog.css" type="text/css" media="screen" />
    <script type="text/javascript" src="dialogbox/redips-dialog-min.js"></script>
    <script type="text/javascript" src="dialogbox/script.js"></script>

    <script type="text/javascript">  
        $(document).ready(function () {
            if (window.innerHeight > 0) { $("#BarraLaterale").css("height", (window.innerHeight - 94) + "px"); } else { $("#BarraLaterale").css("height", (document.documentElement.offsetHeight - 94) + "px"); }
        });
        function DialogBox(Path) {


            REDIPS.dialog.show(500, 300, '<iframe id="output" src="' + Path + '" height="300px" width="500"></iframe>');
            return false;

        }
        function DialogBoxx(Path) {

            var winW = 630, winH = 460;

            if (document.body && document.body.offsetWidth) {
                winW = document.body.offsetWidth;
                winH = document.body.offsetHeight;
            }
            if (document.compatMode == 'CSS1Compat' &&
                document.documentElement &&
                document.documentElement.offsetWidth) {
                winW = document.documentElement.offsetWidth;
                winH = document.documentElement.offsetHeight;
            }
            if (window.innerWidth && window.innerHeight) {
                winW = window.innerWidth;
                winH = window.innerHeight;
            }
            REDIPS.dialog.show(winW - 200, winH - 100, '<iframe id="output" src="' + Path + '" height="' + (winH - 100) + 'px" width="100%"></iframe>');
            return false;

        }
    </script>
</head>
<body>
    <form id="form1" runat="server">
        <asp:ScriptManager ID="ScriptManager1" runat="server" EnableScriptGlobalization="true">
            <Scripts>
                <asp:ScriptReference Path="~/Scripts/AjaxControlToolkit/Bundle" />
            </Scripts>
        </asp:ScriptManager>
        <asp:Button ID="Btn_Dowload" runat="server" Text="Download" Visible="false" />
        <asp:Label ID="Lbl_BarraSenior" runat="server" Text=""></asp:Label>
        <div style="text-align: left;">
            <table style="width: 100%;" cellpadding="0" cellspacing="0">
                <tr>
                    <td style="width: 160px; background-color: #F0F0F0;"></td>
                    <td>
                        <div class="Titolo">Ospiti - Export - Import XLS export XML</div>
                        <div class="SottoTitolo">
                            <asp:Label ID="Lbl_NomeOspite" runat="server"></asp:Label><br />
                            <br />
                        </div>
                    </td>
                    <td style="text-align: right;">
                        <span class="BenvenutoText">Benvenuto
                            <asp:Label ID="Lbl_Utente" runat="server"></asp:Label></span>
                        <div class="DivTasti">
                            <asp:ImageButton ID="Btn_Modifica" runat="server" Height="38px" ImageUrl="~/images/salva.jpg" class="EffettoBottoniTondi" Width="38px" ToolTip="Modifica" />
                        </div>
                    </td>
                </tr>

                <tr>
                    <td style="width: 160px; background-color: #F0F0F0; vertical-align: top; text-align: center;" id="BarraLaterale">
                        <asp:ImageButton ID="Btn_Esci" runat="server" BackColor="Transparent" ImageUrl="../images/Menu_Indietro.png" class="Effetto" ToolTip="Chiudi" />

                        <br />

                        <div id="MENUDIV"></div>
                        <br />
                        <br />
                        <br />
                        <br />
                        <br />
                        <br />
                        <br />
                        <br />
                        <br />
                        <br />
                        <br />
                        <br />
                    </td>
                    <td colspan="2" style="background-color: #FFFFFF; vertical-align: top;">
                        <sau:TabContainer ID="TabContainer1" runat="server" ActiveTabIndex="0" CssClass="TabSenior"
                            Width="100%" BorderStyle="None" Style="margin-right: 39px">
                            <sau:TabPanel runat="server" HeaderText="Prima Nota" ID="Tab_Anagrafica">
                                <HeaderTemplate>
                                    Generazione file XML             
                                </HeaderTemplate>
                                <ContentTemplate>
                                    <label style="display: block; float: left; width: 160px;">File  :</label>
                                    <asp:FileUpload ID="FileUpload1" runat="server" />
                                    <br />
                                    <br />

                                    <label class="LabelCampo">Codice Regione:</label>
                                    <asp:TextBox ID="Txt_CodiceRegione" runat="server" Width="90px" MaxLength="3"></asp:TextBox><br />
                                    <br />
                                    <label class="LabelCampo">Codice Asl:</label>
                                    <asp:TextBox ID="Txt_CodiceAsl" runat="server" Width="90px" MaxLength="3"></asp:TextBox><br />
                                    <br />
                                    <label class="LabelCampo">Codice SSa:</label>
                                    <asp:TextBox ID="Txt_CodiceSSA" runat="server" Width="90px" MaxLength="6"></asp:TextBox><br />
                                    <br />
                                    <label class="LabelCampo">CF Proprietario:</label>
                                    <asp:TextBox ID="Txt_CFPropietario" MaxLength="16" runat="server" Width="90px"></asp:TextBox><br />
                                    <br />
                                    <label class="LabelCampo">Piva Struttura:</label>
                                    <asp:TextBox ID="Txt_PIVA" runat="server" MaxLength="11" Width="90px"></asp:TextBox><br />
                                    <br />
                                    <label class="LabelCampo">Dispositivo :</label>
                                    <asp:TextBox ID="Txt_Dispositivo" runat="server" MaxLength="2" Text="1" Width="90px"></asp:TextBox><br />
                                    <br />
                                    <hr />
                                    <asp:Label ID="lbl_errori" runat="server" ForeColor="Red" Text=""></asp:Label>

                                    <asp:TextBox ID="Txt_Pin" runat="server" Width="90px" Visible="false"></asp:TextBox>
                                    <asp:Button ID="btn_codifica" runat="server" Text="codifica" Visible="false" />
                                    <br />

                                    </asp:Timer>       

       <br />
                                </ContentTemplate>
                            </sau:TabPanel>
                        </sau:TabContainer>
                    </td>
                </tr>
            </table>


        </div>
    </form>
</body>
</html>
