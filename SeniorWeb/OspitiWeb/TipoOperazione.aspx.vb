﻿
Partial Class OspitiWeb_TipoOperazione
    Inherits System.Web.UI.Page

    Private Sub Pulisci()

        'DD_TipoOperazione.SelectedValue = ""
        Txt_Codice.Text = ""
        Txt_Descrizione.Text = ""
        DD_Retta.SelectedValue = ""
        DD_Addebito.SelectedValue = ""
        DD_Accredito.SelectedValue = ""
        DD_Incasso.SelectedValue = ""
        DD_Storno.SelectedValue = ""
        DD_DocumentoAnticipo.SelectedValue = ""
        DD_IncassoAnticipo.SelectedValue = ""
        DD_DocumentoAzero.SelectedValue = ""
        DD_Giroconto.SelectedValue = ""
        DD_DepositoCauzionale.SelectedValue = ""
        DD_NCDepositoCauzionale.SelectedValue = ""

        Chk_NonContoAnticipi.Checked = False
        Chk_SoloParentiOSpiti.Checked = False
        Chk_SoloAnticipo.Checked = False
        Chk_ScorporaIVA.Checked = False
        Chk_Bollettino.Checked = False
        Chk_Fattura.Checked = False



        'RB_BolloSi.Checked = False
        RB_BolloNo.Checked = True
        RB_BolloSoloFt.Checked = False


        Opt_NonAnticipo.Checked = True
        Opt_Anticipo.Checked = False
        Opt_AnticipoComune.Checked = False
        Opt_AnticipoCompensazione.Checked = False
        Opt_Rettagiroconto.Checked = False

    End Sub



    

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Trim(Session("UTENTE")) = "" Then
            Response.Redirect("..\Login.aspx")
            Exit Sub
        End If

        If Page.IsPostBack = True Then
            Exit Sub
        End If


        Dim K1 As New Cls_SqlString

        Dim Barra As New Cls_BarraSenior

        If Not IsNothing(Session("RicercaAnagraficaSQLString")) Then
            K1 = Session("RicercaAnagraficaSQLString")
        End If

        Lbl_BarraSenior.Text = Barra.CodiceBarra(Application("SENIOR"), Session("UTENTE"), Page, K1)

        Dim CauCont As New Cls_CausaleContabile


        CauCont.UpDateDropBox(Session("DC_TABELLE"), DD_Retta)
        CauCont.UpDateDropBox(Session("DC_TABELLE"), DD_Addebito)
        CauCont.UpDateDropBox(Session("DC_TABELLE"), DD_Accredito)
        CauCont.UpDateDropBox(Session("DC_TABELLE"), DD_Incasso)
        CauCont.UpDateDropBox(Session("DC_TABELLE"), DD_Storno)
        CauCont.UpDateDropBox(Session("DC_TABELLE"), DD_DocumentoAnticipo)
        CauCont.UpDateDropBox(Session("DC_TABELLE"), DD_IncassoAnticipo)
        CauCont.UpDateDropBox(Session("DC_TABELLE"), DD_DocumentoAzero)
        CauCont.UpDateDropBox(Session("DC_TABELLE"), DD_Giroconto)
        CauCont.UpDateDropBox(Session("DC_TABELLE"), DD_DepositoCauzionale)
        CauCont.UpDateDropBox(Session("DC_TABELLE"), DD_NCDepositoCauzionale)


        CauCont.UpDateDropBox(Session("DC_TABELLE"), DD_Caparra)
        CauCont.UpDateDropBox(Session("DC_TABELLE"), DD_NCCaparra)



        Dim kVilla As New Cls_TabelleDescrittiveOspitiAccessori


        kVilla.UpDateDropBox(Session("DC_OSPITIACCESSORI"), "VIL", DD_Struttura)
        If DD_Struttura.Items.Count = 1 Then
            Call AggiornaCServ()
        End If


        Dim Tb As New Cls_Addebito

        Tb.UpDateDropBox(Session("DC_OSPITE"), DD_TipoAddebito)

        Tb.UpDateDropBox(Session("DC_OSPITE"), DD_TipoAddebito2)

        Dim IvaCau As New Cls_IVA

        IvaCau.UpDateDropBox(Session("DC_TABELLE"), DD_IvaCaparra)


        DD_REGOLA.Items.Add("Importo Fisso")
        DD_REGOLA.Items(DD_REGOLA.Items.Count - 1).Value = "I"
        DD_REGOLA.Items.Add("30 Giorni x Quota Giornaliera")
        DD_REGOLA.Items(DD_REGOLA.Items.Count - 1).Value = "3"
        DD_REGOLA.Items.Add("31 Giorni x Quota Giornaliera")
        DD_REGOLA.Items(DD_REGOLA.Items.Count - 1).Value = "1"
        DD_REGOLA.Items.Add("")
        DD_REGOLA.Items(DD_REGOLA.Items.Count - 1).Value = ""


        If Request.Item("Codice") = "" Then
            Dim MaxTipoOperazione As New Cls_TipoOperazione

            Txt_Codice.Text = MaxTipoOperazione.MaxTipoOperazione(Session("DC_OSPITE"))
            Exit Sub
        End If



        Dim XB As New Cls_TipoOperazione


        XB.Codice = Request.Item("Codice")

        XB.Leggi(Session("DC_OSPITE"), XB.Codice)

        Txt_Codice.Text = XB.Codice

        Txt_Codice.Enabled = False
        Txt_Descrizione.Text = XB.Descrizione
        DD_Retta.SelectedValue = XB.CausaleRetta
        DD_Addebito.SelectedValue = XB.CausaleAddebito
        DD_Accredito.SelectedValue = XB.CausaleAccredito
        DD_Incasso.SelectedValue = XB.CausaleIncasso
        DD_Storno.SelectedValue = XB.CausaleStorno
        DD_DocumentoAnticipo.SelectedValue = XB.CausaleDocumentoAnticipo
        DD_IncassoAnticipo.SelectedValue = XB.CausaleAnticipo
        DD_DocumentoAzero.SelectedValue = XB.DocumentoZero
        DD_Giroconto.SelectedValue = XB.GirocontoAnticipo
        DD_DepositoCauzionale.SelectedValue = XB.CausaleDeposito
        DD_NCDepositoCauzionale.SelectedValue = XB.CausaleNcDeposito

        DD_Caparra.SelectedValue = XB.CausaleCaparraConfirmatoria
        DD_NCCaparra.SelectedValue = XB.NcCausaleCaparraConfirmatoria

        DD_IvaCaparra.SelectedValue = XB.CodiceIVACaparra

        DD_REGOLA.SelectedValue = XB.RegolaDeposito

        Txt_ImportoDeposito.Text = Format(XB.ImportoDeposito, "#,##0.00")


        DD_Struttura.SelectedValue = XB.Struttura
        Cmb_CServ.SelectedValue = XB.CENTROSERVIZIO

        If XB.NonContoAnticipi = "S" Then
            Chk_NonContoAnticipi.Checked = True
        Else
            Chk_NonContoAnticipi.Checked = False
        End If

        If XB.ParentiOspiti = 1 Then
            Chk_SoloParentiOSpiti.Checked = True
        Else
            Chk_SoloParentiOSpiti.Checked = False
        End If

        If XB.SoloAnticipo = "S" Then
            Chk_SoloAnticipo.Checked = True
        Else
            Chk_SoloAnticipo.Checked = False
        End If

        If XB.SCORPORAIVA = 1 Then
            Chk_ScorporaIVA.Checked = True
        Else
            Chk_ScorporaIVA.Checked = False
        End If


        If XB.StampaBollettinoPostale = "S" Then
            Chk_Bollettino.Checked = True
        Else
            Chk_Bollettino.Checked = False
        End If

        If XB.StampaFattura = "S" Then
            Chk_Fattura.Checked = True
        Else
            Chk_Fattura.Checked = False
        End If

        RB_BolloSoloFt.Checked = False
        RB_BolloSi.Checked = False
        RB_BolloNo.Checked = True

        If XB.SoggettaABollo = "S" Then
            RB_BolloSi.Checked = True
            RB_BolloNo.Checked = False
            RB_BolloSoloFt.Checked = False
        End If
        If XB.SoggettaABollo = "F" Then
            RB_BolloSi.Checked = False
            RB_BolloNo.Checked = False
            RB_BolloSoloFt.Checked = True
        End If


        Opt_NonAnticipo.Checked = False
        Opt_Anticipo.Checked = False
        Opt_AnticipoComune.Checked = False
        Opt_AnticipoCompensazione.Checked = False
        Opt_Rettagiroconto.Checked = False
        If XB.Anticipo = "" Then
            Opt_NonAnticipo.Checked = True
        End If
        If XB.Anticipo = "S" Then
            Opt_Anticipo.Checked = True
        End If
        If XB.Anticipo = "C" Then
            Opt_AnticipoComune.Checked = True
        End If
        If XB.Anticipo = "M" Then
            Opt_AnticipoCompensazione.Checked = True
        End If
        If XB.Anticipo = "X" Then
            Opt_Rettagiroconto.Checked = True
        End If
        DD_TipoAddebito.SelectedValue = XB.TipoAddebitoAccredito

        DD_TipoAddebito2.SelectedValue = XB.TipoAddebitoAccredito2

        Txt_Percentuale.Text = XB.PercentualeDivisione



        If XB.TIPOFILTRO = "" Then
            RB_Tutti.Checked = True
        End If
        If XB.TIPOFILTRO = "E" Then
            RB_SoloEnti.Checked = True
        End If
        If XB.TIPOFILTRO = "O" Then
            RB_OspitiParenti.Checked = True
        End If

        If XB.NonBolloConIVA = 1 Then
            Chk_NonBolloSeIVA.Checked = True
        Else
            Chk_NonBolloSeIVA.Checked = False
        End If



        If XB.BolloVirtuale = 1 Then
            Chk_BolloVirtuale.Checked = True
        Else
            Chk_BolloVirtuale.Checked = False
        End If
    End Sub

    

    Protected Sub Imb_Modifica_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles Imb_Modifica.Click
        If Txt_Descrizione.Text.Trim = "" Then
            ClientScript.RegisterClientScriptBlock(Me.GetType(), "Errore", "VisualizzaErrore('Descrizione obbligatoria');", True)
            Exit Sub
        End If

        If Txt_Codice.Text.Trim = "" Then
            ClientScript.RegisterClientScriptBlock(Me.GetType(), "Errore", "VisualizzaErrore('Codice obbligatorio');", True)
            Exit Sub
        End If


        If DD_TipoAddebito.SelectedValue = "" Then
            ClientScript.RegisterClientScriptBlock(Me.GetType(), "Errore", "VisualizzaErrore('Specificare tipo addebito');", True)
            Exit Sub
        End If

        If DD_Retta.SelectedValue = "" Then
            ClientScript.RegisterClientScriptBlock(Me.GetType(), "Errore", "VisualizzaErrore('Specificare causale retta');", True)
            Exit Sub
        End If

        If Chk_BolloVirtuale.Checked = True And RB_BolloNo.Checked = False Then
            ClientScript.RegisterClientScriptBlock(Me.GetType(), "Errore", "VisualizzaErrore('Il bollo solo virtuale può essere attivato solo se non è attivo il bollo');", True)
            Exit Sub
        End If


        If Txt_Codice.Enabled = True Then
            Dim TpVr As New Cls_TipoOperazione

            TpVr.Codice = Txt_Codice.Text
            TpVr.Leggi(Session("DC_OSPITE"), TpVr.Codice)

            If TpVr.Descrizione <> "" Then
                ClientScript.RegisterClientScriptBlock(Me.GetType(), "Errore", "VisualizzaErrore('Codice già utilizzato');", True)
                Exit Sub
            End If
        End If

        Dim i As Integer

    

        Dim XB As New Cls_TipoOperazione

        XB.Leggi(Session("DC_OSPITE"), Txt_Codice.Text)

        XB.Codice = Txt_Codice.Text
        XB.Descrizione = Txt_Descrizione.Text
        XB.CausaleRetta = DD_Retta.SelectedValue
        XB.CausaleAddebito = DD_Addebito.SelectedValue
        XB.CausaleAccredito = DD_Accredito.SelectedValue
        XB.CausaleIncasso = DD_Incasso.SelectedValue
        XB.CausaleStorno = DD_Storno.SelectedValue
        XB.CausaleDocumentoAnticipo = DD_DocumentoAnticipo.SelectedValue
        XB.CausaleAnticipo = DD_IncassoAnticipo.SelectedValue
        XB.DocumentoZero = DD_DocumentoAzero.SelectedValue
        XB.GirocontoAnticipo = DD_Giroconto.SelectedValue
        XB.CausaleDeposito = DD_DepositoCauzionale.SelectedValue
        XB.CausaleNcDeposito = DD_NCDepositoCauzionale.SelectedValue

        XB.CausaleCaparraConfirmatoria = DD_Caparra.SelectedValue
        XB.NcCausaleCaparraConfirmatoria = DD_NCCaparra.SelectedValue

        XB.CodiceIVACaparra = DD_IvaCaparra.SelectedValue


        XB.Struttura = DD_Struttura.SelectedValue
        XB.CENTROSERVIZIO = Cmb_CServ.SelectedValue

        If RB_Tutti.Checked = True Then
            XB.TIPOFILTRO = ""
        End If
        If RB_SoloEnti.Checked = True Then
            XB.TIPOFILTRO = "E"
        End If
        If RB_OspitiParenti.Checked = True Then
            XB.TIPOFILTRO = "O"
        End If

        XB.RegolaDeposito = DD_REGOLA.SelectedValue
        If Trim(Txt_ImportoDeposito.Text) = "" Then
            Txt_ImportoDeposito.Text = 0
        End If
        XB.ImportoDeposito = Txt_ImportoDeposito.Text

        If Chk_NonContoAnticipi.Checked = True Then
            XB.NonContoAnticipi = "S"
        Else
            XB.NonContoAnticipi = 0
        End If

        If Chk_SoloParentiOSpiti.Checked = True Then
            XB.ParentiOspiti = 1
        Else
            XB.ParentiOspiti = 0
        End If

        If Chk_SoloAnticipo.Checked = True Then
            XB.SoloAnticipo = "S"
        Else
            XB.SoloAnticipo = ""
        End If

        If Chk_ScorporaIVA.Checked = True Then
            XB.SCORPORAIVA = 1
        Else
            XB.SCORPORAIVA = 0
        End If


        If Chk_Bollettino.Checked = True Then
            XB.StampaBollettinoPostale = "S"
        Else
            XB.StampaBollettinoPostale = ""
        End If

        If Chk_Fattura.Checked = True Then
            XB.StampaFattura = "S"
        Else
            XB.StampaFattura = ""
        End If

        XB.SoggettaABollo = ""
        If RB_BolloSi.Checked = True Then
            XB.SoggettaABollo = "S"        
        End If
        If RB_BolloSoloFt.Checked = True Then
            XB.SoggettaABollo = "F"
        End If


        If Opt_NonAnticipo.Checked = True Then
            XB.Anticipo = ""
        End If
        If Opt_Anticipo.Checked = True Then
            XB.Anticipo = "S"
        End If
        If Opt_AnticipoComune.Checked = True Then
            XB.Anticipo = "C"
        End If
        If Opt_AnticipoCompensazione.Checked = True Then
            XB.Anticipo = "M"
        End If
        If Opt_Rettagiroconto.Checked = True Then
            XB.Anticipo = "X"
        End If

        XB.TipoAddebitoAccredito = DD_TipoAddebito.SelectedValue
        XB.TipoAddebitoAccredito2 = DD_TipoAddebito2.SelectedValue

        If Txt_Percentuale.Text = "" Then
            Txt_Percentuale.Text = "0"
        End If
        XB.PercentualeDivisione = Txt_Percentuale.Text



        If Chk_NonBolloSeIVA.Checked = True Then
            XB.NonBolloConIVA = 1
        Else
            XB.NonBolloConIVA = 0
        End If



        If Chk_BolloVirtuale.Checked = True Then
            XB.BolloVirtuale = 1
        Else
            XB.BolloVirtuale = 0
        End If

        XB.Scrivi(Session("DC_OSPITE"))

        Call PaginaPrecedente()
    End Sub


    Protected Sub ImageButton2_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles ImageButton2.Click
        If Txt_Codice.Text.Trim = "" Then
            ClientScript.RegisterClientScriptBlock(Me.GetType(), "Errore", "VisualizzaErrore('Codice obbligatorio');", True)
            Exit Sub
        End If

        Dim XB As New Cls_TipoOperazione

        XB.Codice = Txt_Codice.Text

        If XB.InUso(Session("DC_OSPITE")) Then
            ClientScript.RegisterClientScriptBlock(Me.GetType(), "Errore", "VisualizzaErrore('Non posso eliminare, codice usato');", True)
            Exit Sub
        End If

        XB.Delete(Session("DC_OSPITE"))

        Call PaginaPrecedente()

    End Sub

    Protected Sub Imb_Duplica_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles Imb_Duplica.Click
        Txt_Codice.Text = ""
        Txt_Codice.Enabled = True

        Dim MaxTipoOperazione As New Cls_TipoOperazione

        Txt_Codice.Text = MaxTipoOperazione.MaxTipoOperazione(Session("DC_OSPITE"))


        Call Txt_Codice_TextChanged(sender, e)

        Call Txt_Descrizione_TextChanged(sender, e)
    End Sub

    Protected Sub Btn_Esci_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles Btn_Esci.Click
        Call PaginaPrecedente()
    End Sub

    Private Sub PaginaPrecedente()
        Response.Redirect("ElencoTipoOperazione.aspx")
    End Sub

    Protected Sub Txt_Codice_TextChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles Txt_Codice.TextChanged
        If Txt_Codice.Enabled = True Then
            Img_VerificaCodice.ImageUrl = "~/images/corretto.gif"

            Dim x As New Cls_TipoOperazione

            x.Codice = Txt_Codice.Text.Trim
            x.Descrizione = ""
            x.Leggi(Session("DC_OSPITE"), x.Codice)

            If x.Descrizione <> "" Then
                Img_VerificaCodice.ImageUrl = "~/images/errore.gif"
            End If
        End If
    End Sub

    Protected Sub Txt_Descrizione_TextChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles Txt_Descrizione.TextChanged
        If Txt_Codice.Enabled = True Then
            If Txt_Descrizione.Text <> "" Then
                Img_VerificaDes.ImageUrl = "~/images/corretto.gif"

                Dim x As New Cls_TipoOperazione

                x.Codice = ""
                x.Descrizione = Txt_Descrizione.Text.Trim
                x.LeggiDescrizione(Session("DC_OSPITE"), x.Descrizione)

                If x.Codice <> "" Then
                    Img_VerificaDes.ImageUrl = "~/images/errore.gif"
                End If
            End If
        End If
    End Sub



    Protected Sub DD_Struttura_TextChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles DD_Struttura.TextChanged
        AggiornaCServ()
        Call EseguiJS()
    End Sub

    Private Sub AggiornaCServ()
        Dim kCsrv As New Cls_CentroServizio

        If DD_Struttura.SelectedValue = "" Then
            kCsrv.UpDateDropBox(Session("DC_OSPITE"), Cmb_CServ)
        Else
            kCsrv.UpDateDropBoxStruttura(Session("DC_OSPITE"), Cmb_CServ, DD_Struttura.SelectedValue)
        End If

    End Sub


    Private Sub EseguiJS()
        Dim MyJs As String
        MyJs = "$(document).ready(function() {"

        MyJs = MyJs & "var els = document.getElementsByTagName(""*"");"

        MyJs = MyJs & "for (var i=0;i<els.length;i++)"
        MyJs = MyJs & "if ( els[i].id ) { "
        MyJs = MyJs & " var appoggio =els[i].id; "


        MyJs = MyJs & " if (appoggio.match('Txt_Conto')!= null) {   "
        MyJs = MyJs & " $(els[i]).autocomplete('/WebHandler/GestioneConto.ashx?Utente=" & Session("UTENTE") & "', {delay:5,minChars:3});"
        MyJs = MyJs & "    }"


        MyJs = MyJs & " if ((appoggio.match('Txt_Importo')!= null) ) {  "
        MyJs = MyJs & " $(els[i]).keypress(function() { ForceNumericInput($(this).val(), true, true); } ); "
        MyJs = MyJs & " $(els[i]).blur(function() { var ap = formatNumber($(this).val(),2); $(this).val(ap); });"
        MyJs = MyJs & "    }"

        MyJs = MyJs & " if ((appoggio.match('Txt_Data')!= null)) {  "
        MyJs = MyJs & " $(els[i]).mask(""99/99/9999"");"
        MyJs = MyJs & " $(els[i]).datepicker({ changeMonth: true,changeYear: true,  yearRange: ""1890:" & Year(Now) & """},$.datepicker.regional[""it""]);"
        MyJs = MyJs & "    }"


        MyJs = MyJs & "} "
        MyJs = MyJs & "});"

        'MyJs = "$(document).ready(function() { $('.myClass').keypress(function() { handleEnter($('.myClass').val(), true, true); } );     $('#" & Txt_ImportoPagato.ClientID & "').blur(function() { var ap = formatNumber ($('#" & Txt_ImportoPagato.ClientID & "').val(),2); $('#" & Txt_ImportoPagato.ClientID & "').val(ap); }); });"
        ScriptManager.RegisterStartupScript(Me, Me.GetType(), "visualizzaRitIm", MyJs, True)
    End Sub

End Class
