﻿Imports Microsoft.VisualBasic
Imports System.Data.OleDb
Imports System.Web.Hosting

Partial Class OspitiWeb_quotemensili
    Inherits System.Web.UI.Page

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load

        If Trim(Session("UTENTE")) = "" Then
            Response.Redirect("..\Login.aspx")
            Exit Sub
        End If

        If Page.IsPostBack = True Then
            Exit Sub
        End If

        If Val(Request.Item("CodiceOspite")) > 0 And Request.Item("CentroServizio") <> "" Then

            Session("CODICESERVIZIO") = Request.Item("CentroServizio")
            Session("CODICEOSPITE") = Val(Request.Item("CodiceOspite"))
        End If

        Dim x1 As New ClsOspite

        x1.CodiceOspite = Session("CODICEOSPITE")
        x1.Leggi(Session("DC_OSPITE"), x1.CodiceOspite)

        Dim cs As New Cls_CentroServizio

        cs.Leggi(Session("DC_OSPITE"), Session("CODICESERVIZIO"))

        Lbl_NomeOspite.Text = x1.Nome & " -  " & x1.DataNascita & " - " & cs.DESCRIZIONE & "<i style=""font-size:small;"">(" & x1.CodiceOspite & ")</i>"


        Dim K1 As New Cls_SqlString

        Dim Barra As New Cls_BarraSenior

        If Not IsNothing(Session("RicercaAnagraficaSQLString")) Then
            K1 = Session("RicercaAnagraficaSQLString")
        End If

        Lbl_BarraSenior.Text = Barra.CodiceBarra(Application("SENIOR"), Session("UTENTE"), Page, K1)

        Dim k As New Cls_Parametri

        k.LeggiParametri(Session("DC_OSPITE"))

        Txt_Anno.Text = k.AnnoFatturazione
        Dd_Mese.SelectedValue = k.MeseFatturazione
        Call VisualizzaDatiMese(Val(Txt_Anno.Text), Val(Dd_Mese.SelectedValue))
    End Sub

    Function campodbN(ByVal oggetto As Object) As Double
        If IsDBNull(oggetto) Then
            Return 0
        Else
            Return oggetto
        End If
    End Function

    Private Sub VisualizzaDatiMese(ByVal anno As Long, ByVal mese As Integer)
        Dim cn As OleDbConnection
        Dim ImportoOspite As Double
        Dim ImportoParenti As Double
        Dim ImportoOspiteAnticipati As Double
        Dim ImportoParentiAnticipati As Double
        Dim ImportoComune As Double
        Dim ImportoRegione As Double
        Dim ImportoJolly As Double


        Dim GiorniOspite As Double
        Dim GiorniParenti As Double
        Dim GiorniOspiteAnticipati As Double
        Dim GiorniParentiAnticipati As Double

        Dim GiorniComune As Double
        Dim GiorniRegione As Double
        Dim GiorniJolly As Double


        Dim ImportoOspiteAssenza As Double
        Dim ImportoParentiAssenza As Double
        Dim ImportoComuneAssenza As Double
        Dim ImportoRegioneAssenza As Double
        Dim ImportoJollyAssenza As Double

        Dim GiorniOspiteAssenza As Double
        Dim GiorniParentiAssenza As Double
        Dim GiorniComuneAssenza As Double
        Dim GiorniRegioneAssenza As Double
        Dim GiorniJollyAssenza As Double


        cn = New Data.OleDb.OleDbConnection(Session("DC_OSPITE"))

        cn.Open()
        Dim cmd As New OleDbCommand()
        cmd.CommandText = ("select SUM(Importo),sum(giorni) from RETTEOSPITE where (Not (Elemento  Like 'E%') and not Elemento Like '%2%' and Elemento <> 'ADD' And Elemento <> 'ACC') And CentroServizio = '" & Session("CODICESERVIZIO") & "' And  CodiceOspite = " & Session("CODICEOSPITE")) & " And Mese = " & mese & " And Anno = " & anno
        cmd.Connection = cn
        Dim myPOSTreader As OleDbDataReader = cmd.ExecuteReader()
        If myPOSTreader.Read Then
            GiorniOspite = campodbN(myPOSTreader.Item(1))
        End If
        myPOSTreader.Close()

        Dim cmd_Imp As New OleDbCommand()
        cmd_Imp.CommandText = ("select SUM(Importo),sum(giorni) from RETTEOSPITE where (Not (Elemento  Like 'E%') and Elemento <> 'ADD' And Elemento <> 'ACC') And CentroServizio = '" & Session("CODICESERVIZIO") & "' And  CodiceOspite = " & Session("CODICEOSPITE")) & " And Mese = " & mese & " And Anno = " & anno
        cmd_Imp.Connection = cn
        Dim myPOSTreader_Imp As OleDbDataReader = cmd_Imp.ExecuteReader()
        If myPOSTreader_Imp.Read Then
            ImportoOspite = campodbN(myPOSTreader_Imp.Item(0))
        End If
        myPOSTreader_Imp.Close()

        Dim cmd1 As New OleDbCommand()
        cmd1.CommandText = ("select SUM(Importo),sum(giorni) from RETTEPARENTE where (Not (Elemento  Like 'E%') and not Elemento Like '%2%'  and Elemento <> 'ADD' And Elemento <> 'ACC') And  CentroServizio = '" & Session("CODICESERVIZIO") & "' And  CodiceOspite = " & Session("CODICEOSPITE")) & " And Mese = " & mese & " And Anno = " & anno
        cmd1.Connection = cn
        Dim myPOSTreader1 As OleDbDataReader = cmd1.ExecuteReader()
        If myPOSTreader1.Read Then
            ImportoParenti = campodbN(myPOSTreader1.Item(0))
        End If
        myPOSTreader1.Close()

        myPOSTreader1.Close()

        cmd1.CommandText = ("select sum(giorni) from RETTEPARENTE where (Not (Elemento  Like 'E%') and not Elemento Like '%2%' and Elemento <> 'ADD' And Elemento <> 'ACC')  And CentroServizio = '" & Session("CODICESERVIZIO") & "' And  CodiceOspite = " & Session("CODICEOSPITE")) & " And Mese = " & mese & " And Anno = " & anno
        cmd1.Connection = cn
        myPOSTreader1 = cmd1.ExecuteReader()
        If myPOSTreader1.Read Then
            GiorniParenti = campodbN(myPOSTreader1.Item(0))
        End If
        myPOSTreader1.Close()


        Dim cmd_Imp_P As New OleDbCommand()
        cmd_Imp_P.CommandText = ("select SUM(Importo),sum(giorni) from RETTEPARENTE where (Not (Elemento  Like 'E%')  and Elemento <> 'ADD' And Elemento <> 'ACC') And  CentroServizio = '" & Session("CODICESERVIZIO") & "' And  CodiceOspite = " & Session("CODICEOSPITE")) & " And Mese = " & mese & " And Anno = " & anno
        cmd_Imp_P.Connection = cn
        Dim myPOSTreader_p As OleDbDataReader = cmd_Imp_P.ExecuteReader()
        If myPOSTreader_p.Read Then
            ImportoParenti = campodbN(myPOSTreader_p.Item(0))
        End If
        myPOSTreader_p.Close()

        'SEZIONE IMPORTI E GIORNI ANTICIPATI
        cmd.CommandText = ("select sum(giorni) from RETTEOSPITE where (Elemento='RPX' )  And CentroServizio = '" & Session("CODICESERVIZIO") & "' And  CodiceOspite = " & Session("CODICEOSPITE")) & " And Mese = " & mese & " And Anno = " & anno
        cmd.Connection = cn
        myPOSTreader = cmd.ExecuteReader()
        If myPOSTreader.Read Then
            GiorniOspiteAnticipati = campodbN(myPOSTreader.Item(0))
        End If
        myPOSTreader.Close()

        cmd.CommandText = ("select SUM(Importo),sum(giorni) from RETTEOSPITE where (Elemento='RPX' OR Elemento='R2X' )  And CentroServizio = '" & Session("CODICESERVIZIO") & "' And  CodiceOspite = " & Session("CODICEOSPITE")) & " And Mese = " & mese & " And Anno = " & anno
        cmd.Connection = cn
        myPOSTreader = cmd.ExecuteReader()
        If myPOSTreader.Read Then            
            ImportoOspiteAnticipati = campodbN(myPOSTreader.Item(0))
        End If
        myPOSTreader.Close()

        cmd.CommandText = ("select sum(giorni) from RETTEPARENTE where (Elemento='RPX'  ) And CentroServizio = '" & Session("CODICESERVIZIO") & "' And  CodiceOspite = " & Session("CODICEOSPITE")) & " And Mese = " & mese & " And Anno = " & anno
        cmd.Connection = cn
        myPOSTreader = cmd.ExecuteReader()
        If myPOSTreader.Read Then            
            GiorniParentiAnticipati = campodbN(myPOSTreader.Item(0))
        End If
        myPOSTreader.Close()


        cmd.CommandText = ("select SUM(Importo)  from RETTEPARENTE where (Elemento='RPX' OR Elemento='R2X' ) And CentroServizio = '" & Session("CODICESERVIZIO") & "' And  CodiceOspite = " & Session("CODICEOSPITE")) & " And Mese = " & mese & " And Anno = " & anno
        cmd.Connection = cn
        myPOSTreader = cmd.ExecuteReader()
        If myPOSTreader.Read Then
            ImportoParentiAnticipati = campodbN(myPOSTreader.Item(0))            
        End If
        myPOSTreader.Close()

        'FINE SEZIONE


        Dim cmd2 As New OleDbCommand()
        cmd2.CommandText = ("select SUM(Importo) from RETTECOMUNE where (Not (Elemento  Like 'E%') and Elemento <> 'ADD' And Elemento <> 'ACC') And CentroServizio = '" & Session("CODICESERVIZIO") & "' And  CodiceOspite = " & Session("CODICEOSPITE")) & " And Mese = " & mese & " And Anno = " & anno
        cmd2.Connection = cn
        Dim myPOSTreader2 As OleDbDataReader = cmd2.ExecuteReader()
        If myPOSTreader2.Read Then
            ImportoComune = campodbN(myPOSTreader2.Item(0))            
        End If
        myPOSTreader2.Close()

        myPOSTreader2.Close()

        cmd2.CommandText = ("select sum(giorni) from RETTECOMUNE where Elemento  = 'RGP' and  CentroServizio = '" & Session("CODICESERVIZIO") & "' And  CodiceOspite = " & Session("CODICEOSPITE")) & " And Mese = " & mese & " And Anno = " & anno
        cmd2.Connection = cn
        myPOSTreader2 = cmd2.ExecuteReader()
        If myPOSTreader2.Read Then            
            GiorniComune = campodbN(myPOSTreader2.Item(0))
        End If
        myPOSTreader2.Close()

        Dim cmd3 As New OleDbCommand()
        cmd3.CommandText = ("select SUM(Importo)  from RETTEREGIONE where (Not (Elemento  Like 'E%') and Elemento <> 'ADD' And Elemento <> 'ACC') And CentroServizio = '" & Session("CODICESERVIZIO") & "' And  CodiceOspite = " & Session("CODICEOSPITE")) & " And Mese = " & mese & " And Anno = " & anno
        cmd3.Connection = cn
        Dim myPOSTreader3 As OleDbDataReader = cmd3.ExecuteReader()
        If myPOSTreader3.Read Then
            ImportoRegione = campodbN(myPOSTreader3.Item(0))            
        End If
        myPOSTreader3.Close()

        cmd3.CommandText = ("select sum(giorni) from RETTEREGIONE where (Not (Elemento  Like 'E%') and Elemento <> 'ADD' And Elemento <> 'ACC') And CentroServizio = '" & Session("CODICESERVIZIO") & "' And  CodiceOspite = " & Session("CODICEOSPITE")) & " And Mese = " & mese & " And Anno = " & anno
        cmd3.Connection = cn
        myPOSTreader3 = cmd3.ExecuteReader()
        If myPOSTreader3.Read Then
            GiorniRegione = campodbN(myPOSTreader3.Item(0))
        End If
        myPOSTreader3.Close()


        Dim cmd4 As New OleDbCommand()
        cmd4.CommandText = ("select SUM(Importo),sum(giorni) from RETTEJOLLY where (Not (Elemento  Like 'E%') and Elemento <> 'ADD' And Elemento <> 'ACC') And CentroServizio = '" & Session("CODICESERVIZIO") & "' And  CodiceOspite = " & Session("CODICEOSPITE")) & " And Mese = " & mese & " And Anno = " & anno
        cmd4.Connection = cn
        Dim myPOSTreader4 As OleDbDataReader = cmd4.ExecuteReader()
        If myPOSTreader4.Read Then
            ImportoJolly = campodbN(myPOSTreader4.Item(0))
            GiorniJolly = campodbN(myPOSTreader4.Item(1))
        End If
        myPOSTreader4.Close()


        cmd4.CommandText = ("select sum(giorni) from RETTEJOLLY where (Not (Elemento  Like 'E%') and Elemento <> 'ADD' And Elemento <> 'ACC') And CentroServizio = '" & Session("CODICESERVIZIO") & "' And  CodiceOspite = " & Session("CODICEOSPITE")) & " And Mese = " & mese & " And Anno = " & anno
        cmd4.Connection = cn
        myPOSTreader4 = cmd4.ExecuteReader()
        If myPOSTreader4.Read Then            
            GiorniJolly = campodbN(myPOSTreader4.Item(0))
        End If
        myPOSTreader4.Close()


        '******************************
        '*
        '************************
        Dim cmd5 As New OleDbCommand()
        cmd5.CommandText = ("select SUM(Importo),sum(giorni) from RETTEOSPITE where Elemento  = 'RGA' And CentroServizio = '" & Session("CODICESERVIZIO") & "' And  CodiceOspite = " & Session("CODICEOSPITE")) & " And Mese = " & mese & " And Anno = " & anno
        cmd5.Connection = cn
        Dim myPOSTreader5 As OleDbDataReader = cmd5.ExecuteReader()
        If myPOSTreader5.Read Then
            GiorniOspiteAssenza = campodbN(myPOSTreader5.Item(1))
        End If
        myPOSTreader.Close()


        Dim cmd5_imp As New OleDbCommand()
        cmd5_imp.CommandText = ("select SUM(Importo),sum(giorni) from RETTEOSPITE where (Elemento  = 'RGA' OR Elemento  = 'R2A')  And CentroServizio = '" & Session("CODICESERVIZIO") & "' And  CodiceOspite = " & Session("CODICEOSPITE")) & " And Mese = " & mese & " And Anno = " & anno
        cmd5_imp.Connection = cn
        Dim myPOSTreader5_imp As OleDbDataReader = cmd5_imp.ExecuteReader()
        If myPOSTreader5_imp.Read Then
            ImportoOspiteAssenza = campodbN(myPOSTreader5_imp.Item(0))
        End If
        myPOSTreader5_imp.Close()

        Dim cmd6 As New OleDbCommand()
        cmd6.CommandText = ("select SUM(Importo),sum(giorni) from RETTEPARENTE where Elemento  = 'RGA'  And   CentroServizio = '" & Session("CODICESERVIZIO") & "' And  CodiceOspite = " & Session("CODICEOSPITE")) & " And Mese = " & mese & " And Anno = " & anno
        cmd6.Connection = cn
        Dim myPOSTreader6 As OleDbDataReader = cmd6.ExecuteReader()
        If myPOSTreader6.Read Then
            GiorniParentiAssenza = campodbN(myPOSTreader6.Item(1))
        End If
        myPOSTreader6.Close()

        Dim cmd6_Imp As New OleDbCommand()
        cmd6_Imp.CommandText = ("select SUM(Importo),sum(giorni) from RETTEPARENTE where (Elemento  = 'RGA' OR Elemento  = 'R2A')   And   CentroServizio = '" & Session("CODICESERVIZIO") & "' And  CodiceOspite = " & Session("CODICEOSPITE")) & " And Mese = " & mese & " And Anno = " & anno
        cmd6_Imp.Connection = cn
        Dim myPOSTreader6_Imp As OleDbDataReader = cmd6_Imp.ExecuteReader()
        If myPOSTreader6_Imp.Read Then
            ImportoParentiAssenza = campodbN(myPOSTreader6_Imp.Item(0))
        End If
        myPOSTreader6_Imp.Close()


        Dim cmd7 As New OleDbCommand()
        cmd7.CommandText = ("select SUM(Importo),sum(giorni) from RETTECOMUNE where Elemento  = 'RGA'  And CentroServizio = '" & Session("CODICESERVIZIO") & "' And  CodiceOspite = " & Session("CODICEOSPITE")) & " And Mese = " & mese & " And Anno = " & anno
        cmd7.Connection = cn
        Dim myPOSTreader7 As OleDbDataReader = cmd7.ExecuteReader()
        If myPOSTreader7.Read Then
            ImportoComuneAssenza = campodbN(myPOSTreader7.Item(0))
            GiorniComuneAssenza = campodbN(myPOSTreader7.Item(1))
        End If
        myPOSTreader2.Close()

        Dim cmd8 As New OleDbCommand()
        cmd8.CommandText = ("select SUM(Importo),sum(giorni) from RETTEREGIONE where Elemento  = 'RGA'  And CentroServizio = '" & Session("CODICESERVIZIO") & "' And  CodiceOspite = " & Session("CODICEOSPITE")) & " And Mese = " & mese & " And Anno = " & anno
        cmd8.Connection = cn
        Dim myPOSTreader8 As OleDbDataReader = cmd8.ExecuteReader()
        If myPOSTreader8.Read Then
            ImportoRegioneAssenza = campodbN(myPOSTreader8.Item(0))
            GiorniRegioneAssenza = campodbN(myPOSTreader8.Item(1))
        End If
        myPOSTreader8.Close()


        Dim cmd9 As New OleDbCommand()
        cmd9.CommandText = ("select SUM(Importo),sum(giorni) from RETTEJOLLY where Elemento  = 'RGA'  And CentroServizio = '" & Session("CODICESERVIZIO") & "' And  CodiceOspite = " & Session("CODICEOSPITE")) & " And Mese = " & mese & " And Anno = " & anno
        cmd9.Connection = cn
        Dim myPOSTreader9 As OleDbDataReader = cmd9.ExecuteReader()
        If myPOSTreader9.Read Then
            ImportoJollyAssenza = campodbN(myPOSTreader9.Item(0))
            GiorniJollyAssenza = campodbN(myPOSTreader9.Item(1))
        End If
        myPOSTreader9.Close()



        Dim ExtraOspite As Double
        Dim ADDOspite As Double
        Dim ACCOspite As Double

        Dim ExtraParenti As Double
        Dim ADDParenti As Double
        Dim ACCParenti As Double

        Dim ExtraComune As Double
        Dim ADDComune As Double
        Dim ACCComune As Double

        Dim ExtraRegione As Double
        Dim ADDRegione As Double
        Dim ACCRegione As Double

        Dim ExtraJolly As Double
        Dim ADDJolly As Double
        Dim ACCJolly As Double

        Dim MyRd As OleDbDataReader

        cmd.CommandText = ("select SUM(Importo) from RETTEOSPITE where Elemento  Like 'E%' And CentroServizio = '" & Session("CODICESERVIZIO") & "' And  CodiceOspite = " & Session("CODICEOSPITE")) & " And Mese = " & mese & " And Anno = " & anno
        cmd.Connection = cn
        MyRd = cmd.ExecuteReader()
        If MyRd.Read Then
            ExtraOspite = campodbN(MyRd.Item(0))
        End If
        MyRd.Close()

        cmd.CommandText = ("select SUM(Importo) from RETTEOSPITE where Elemento  = 'ADD' And CentroServizio = '" & Session("CODICESERVIZIO") & "' And  CodiceOspite = " & Session("CODICEOSPITE")) & " And Mese = " & mese & " And Anno = " & anno
        cmd.Connection = cn
        MyRd = cmd.ExecuteReader()
        If MyRd.Read Then
            ADDOspite = campodbN(MyRd.Item(0))
        End If
        MyRd.Close()

        cmd.CommandText = ("select SUM(Importo) from RETTEOSPITE where Elemento  = 'ACC' And CentroServizio = '" & Session("CODICESERVIZIO") & "' And  CodiceOspite = " & Session("CODICEOSPITE")) & " And Mese = " & mese & " And Anno = " & anno
        cmd.Connection = cn
        MyRd = cmd.ExecuteReader()
        If MyRd.Read Then
            ACCOspite = campodbN(MyRd.Item(0))
        End If
        MyRd.Close()


        cmd.CommandText = ("select SUM(Importo) from RETTEPARENTE where Elemento  Like 'E%' And CentroServizio = '" & Session("CODICESERVIZIO") & "' And  CodiceOspite = " & Session("CODICEOSPITE")) & " And Mese = " & mese & " And Anno = " & anno
        cmd.Connection = cn
        MyRd = cmd.ExecuteReader()
        If MyRd.Read Then
            ExtraParenti = campodbN(MyRd.Item(0))
        End If
        MyRd.Close()

        cmd.CommandText = ("select SUM(Importo) from RETTEPARENTE where Elemento  = 'ADD' And CentroServizio = '" & Session("CODICESERVIZIO") & "' And  CodiceOspite = " & Session("CODICEOSPITE")) & " And Mese = " & mese & " And Anno = " & anno
        cmd.Connection = cn
        MyRd = cmd.ExecuteReader()
        If MyRd.Read Then
            ADDParenti = campodbN(MyRd.Item(0))
        End If
        MyRd.Close()

        cmd.CommandText = ("select SUM(Importo) from RETTEPARENTE where Elemento  = 'ACC' And CentroServizio = '" & Session("CODICESERVIZIO") & "' And  CodiceOspite = " & Session("CODICEOSPITE")) & " And Mese = " & mese & " And Anno = " & anno
        cmd.Connection = cn
        MyRd = cmd.ExecuteReader()
        If MyRd.Read Then
            ACCParenti = campodbN(MyRd.Item(0))
        End If
        MyRd.Close()

        cmd.CommandText = ("select SUM(Importo) from RETTECOMUNE where Elemento  Like 'E%' And CentroServizio = '" & Session("CODICESERVIZIO") & "' And  CodiceOspite = " & Session("CODICEOSPITE")) & " And Mese = " & mese & " And Anno = " & anno
        cmd.Connection = cn
        MyRd = cmd.ExecuteReader()
        If MyRd.Read Then
            ExtraComune = campodbN(MyRd.Item(0))
        End If
        MyRd.Close()

        cmd.CommandText = ("select SUM(Importo) from RETTECOMUNE where Elemento  = 'ADD' And CentroServizio = '" & Session("CODICESERVIZIO") & "' And  CodiceOspite = " & Session("CODICEOSPITE")) & " And Mese = " & mese & " And Anno = " & anno
        cmd.Connection = cn
        MyRd = cmd.ExecuteReader()
        If MyRd.Read Then
            ADDComune = campodbN(MyRd.Item(0))
        End If
        MyRd.Close()

        cmd.CommandText = ("select SUM(Importo) from RETTECOMUNE where Elemento  = 'ACC' And CentroServizio = '" & Session("CODICESERVIZIO") & "' And  CodiceOspite = " & Session("CODICEOSPITE")) & " And Mese = " & mese & " And Anno = " & anno
        cmd.Connection = cn
        MyRd = cmd.ExecuteReader()
        If MyRd.Read Then
            ACCComune = campodbN(MyRd.Item(0))
        End If
        MyRd.Close()


        cmd.CommandText = ("select SUM(Importo) from RETTEREGIONE where Elemento  Like 'E%' And CentroServizio = '" & Session("CODICESERVIZIO") & "' And  CodiceOspite = " & Session("CODICEOSPITE")) & " And Mese = " & mese & " And Anno = " & anno
        cmd.Connection = cn
        MyRd = cmd.ExecuteReader()
        If MyRd.Read Then
            ExtraRegione = campodbN(MyRd.Item(0))
        End If
        MyRd.Close()

        cmd.CommandText = ("select SUM(Importo) from RETTEREGIONE where Elemento  = 'ADD' And CentroServizio = '" & Session("CODICESERVIZIO") & "' And  CodiceOspite = " & Session("CODICEOSPITE")) & " And Mese = " & mese & " And Anno = " & anno
        cmd.Connection = cn
        MyRd = cmd.ExecuteReader()
        If MyRd.Read Then
            ADDRegione = campodbN(MyRd.Item(0))
        End If
        MyRd.Close()

        cmd.CommandText = ("select SUM(Importo) from RETTEREGIONE where Elemento  = 'ACC' And CentroServizio = '" & Session("CODICESERVIZIO") & "' And  CodiceOspite = " & Session("CODICEOSPITE")) & " And Mese = " & mese & " And Anno = " & anno
        cmd.Connection = cn
        MyRd = cmd.ExecuteReader()
        If MyRd.Read Then
            ACCRegione = campodbN(MyRd.Item(0))
        End If
        MyRd.Close()

        cmd.CommandText = ("select SUM(Importo) from RETTEJOLLY where Elemento  Like 'E%' And CentroServizio = '" & Session("CODICESERVIZIO") & "' And  CodiceOspite = " & Session("CODICEOSPITE")) & " And Mese = " & mese & " And Anno = " & anno
        cmd.Connection = cn
        MyRd = cmd.ExecuteReader()
        If MyRd.Read Then
            ExtraJolly = campodbN(MyRd.Item(0))
        End If
        MyRd.Close()

        cmd.CommandText = ("select SUM(Importo) from RETTEJOLLY where Elemento  = 'ADD' And CentroServizio = '" & Session("CODICESERVIZIO") & "' And  CodiceOspite = " & Session("CODICEOSPITE")) & " And Mese = " & mese & " And Anno = " & anno
        cmd.Connection = cn
        MyRd = cmd.ExecuteReader()
        If MyRd.Read Then
            ADDJolly = campodbN(MyRd.Item(0))
        End If
        MyRd.Close()

        cmd.CommandText = ("select SUM(Importo) from RETTEJOLLY where Elemento  = 'ACC' And CentroServizio = '" & Session("CODICESERVIZIO") & "' And  CodiceOspite = " & Session("CODICEOSPITE")) & " And Mese = " & mese & " And Anno = " & anno
        cmd.Connection = cn
        MyRd = cmd.ExecuteReader()
        If MyRd.Read Then
            ACCJolly = campodbN(MyRd.Item(0))
        End If
        MyRd.Close()

        Lbl_Dettagli.Text = "<table cellspacing=""0"" class=""style1"" style=""width: 40%"">"
        Lbl_Dettagli.Text = Lbl_Dettagli.Text & "<tr>"
        Lbl_Dettagli.Text = Lbl_Dettagli.Text & "<td class=""style8"">&nbsp;</td>"
        Lbl_Dettagli.Text = Lbl_Dettagli.Text & "<td class=""style9"">Presenza</td>"
        Lbl_Dettagli.Text = Lbl_Dettagli.Text & "<td class=""style9"">Assenza</td>"
        Lbl_Dettagli.Text = Lbl_Dettagli.Text & "<td class=""style9"">Totale</td>"
        Lbl_Dettagli.Text = Lbl_Dettagli.Text & "<td class=""style9"">Giorni P</td>"
        Lbl_Dettagli.Text = Lbl_Dettagli.Text & "<td class=""style9"">Giorni A</td>"
        Lbl_Dettagli.Text = Lbl_Dettagli.Text & "</tr>"

        Lbl_Dettagli.Text = Lbl_Dettagli.Text & "<tr>"
        Lbl_Dettagli.Text = Lbl_Dettagli.Text & "<td class=""style2"">Ospite Presenza</td>"
        Lbl_Dettagli.Text = Lbl_Dettagli.Text & "<td class=""style2"" style=""text-align:right;"">" & Format(Math.Round(ImportoOspite - ImportoOspiteAssenza - ImportoOspiteAnticipati, 2), "#,##0.00") & "</td>"
        Lbl_Dettagli.Text = Lbl_Dettagli.Text & "<td class=""style2"" style=""text-align:right;"">" & Format(Math.Round(ImportoOspiteAssenza, 2), "#,##0.00") & "</td>"
        Lbl_Dettagli.Text = Lbl_Dettagli.Text & "<td class=""style2"" style=""text-align:right;"">" & Format(Math.Round(ImportoOspite - ImportoOspiteAnticipati, 2), "#,##0.00") & "</td>"
        Lbl_Dettagli.Text = Lbl_Dettagli.Text & "<td class=""style2"" style=""text-align:right;"">" & Math.Abs(GiorniOspite - GiorniOspiteAnticipati - GiorniOspiteAssenza) & "</td>"
        Lbl_Dettagli.Text = Lbl_Dettagli.Text & "<td class=""style2"" style=""text-align:right;"">" & GiorniOspiteAssenza & "</td>"
        Lbl_Dettagli.Text = Lbl_Dettagli.Text & "</tr>"

        Lbl_Dettagli.Text = Lbl_Dettagli.Text & "<tr>"
        Lbl_Dettagli.Text = Lbl_Dettagli.Text & "<td class=""style2"">Ospite Anticipato</td>"
        Lbl_Dettagli.Text = Lbl_Dettagli.Text & "<td class=""style2"" style=""text-align:right;"">" & Format(Math.Round(ImportoOspiteAnticipati, 2), "#,##0.00") & "</td>"
        Lbl_Dettagli.Text = Lbl_Dettagli.Text & "<td class=""style2"" style=""text-align:right;"">0,00</td>"
        Lbl_Dettagli.Text = Lbl_Dettagli.Text & "<td class=""style2"" style=""text-align:right;"">" & Format(Math.Round(ImportoOspiteAnticipati, 2), "#,##0.00") & "</td>"
        Lbl_Dettagli.Text = Lbl_Dettagli.Text & "<td class=""style2"" style=""text-align:right;"">" & GiorniOspiteAnticipati & "</td>"
        Lbl_Dettagli.Text = Lbl_Dettagli.Text & "<td class=""style2"" style=""text-align:right;"">0</td>"
        Lbl_Dettagli.Text = Lbl_Dettagli.Text & "</tr>"

        Lbl_Dettagli.Text = Lbl_Dettagli.Text & "<tr>"
        Lbl_Dettagli.Text = Lbl_Dettagli.Text & "<td class=""style2"">Ospite Totale</td>"
        Lbl_Dettagli.Text = Lbl_Dettagli.Text & "<td class=""style2"" style=""text-align:right;"">" & Format(Math.Round(ImportoOspite - ImportoOspiteAssenza, 2), "#,##0.00") & "</td>"
        Lbl_Dettagli.Text = Lbl_Dettagli.Text & "<td class=""style2"" style=""text-align:right;"">" & Format(Math.Round(ImportoOspiteAssenza, 2), "#,##0.00") & "</td>"
        Lbl_Dettagli.Text = Lbl_Dettagli.Text & "<td class=""style2"" style=""text-align:right;"">" & Format(Math.Round(ImportoOspite, 2), "#,##0.00") & "</td>"
        Lbl_Dettagli.Text = Lbl_Dettagli.Text & "<td class=""style2"" style=""text-align:right;"">" & GiorniOspite - GiorniOspiteAssenza & "</td>"
        Lbl_Dettagli.Text = Lbl_Dettagli.Text & "<td class=""style2"" style=""text-align:right;"">" & GiorniOspiteAssenza & "</td>"
        Lbl_Dettagli.Text = Lbl_Dettagli.Text & "</tr>"

        Lbl_Dettagli.Text = Lbl_Dettagli.Text & "<tr>"
        Lbl_Dettagli.Text = Lbl_Dettagli.Text & "<td class=""style2"" colspan=""6"">&nbsp;</td>"
        Lbl_Dettagli.Text = Lbl_Dettagli.Text & "</tr>"

        Lbl_Dettagli.Text = Lbl_Dettagli.Text & "<tr>"
        Lbl_Dettagli.Text = Lbl_Dettagli.Text & "<td class=""style2"">Parenti Presenza</td>"
        Lbl_Dettagli.Text = Lbl_Dettagli.Text & "<td class=""style2"" style=""text-align:right;"">" & Format(Math.Round(ImportoParenti - ImportoParentiAssenza - ImportoParentiAnticipati, 2), "#,##0.00") & "</td>"
        Lbl_Dettagli.Text = Lbl_Dettagli.Text & "<td class=""style2"" style=""text-align:right;"">" & Format(Math.Round(ImportoParentiAssenza, 2), "#,##0.00") & "</td>"
        Lbl_Dettagli.Text = Lbl_Dettagli.Text & "<td class=""style2"" style=""text-align:right;"">" & Format(Math.Round(ImportoParenti - ImportoParentiAnticipati, 2), "#,##0.00") & "</td>"
        Lbl_Dettagli.Text = Lbl_Dettagli.Text & "<td class=""style2"" style=""text-align:right;"">" & Math.Abs(GiorniParenti - GiorniParentiAnticipati - GiorniParentiAssenza) & "</td>"
        Lbl_Dettagli.Text = Lbl_Dettagli.Text & "<td class=""style2"" style=""text-align:right;"">" & GiorniParentiAssenza & "</td>"
        Lbl_Dettagli.Text = Lbl_Dettagli.Text & "</tr>"

        Lbl_Dettagli.Text = Lbl_Dettagli.Text & "<tr>"
        Lbl_Dettagli.Text = Lbl_Dettagli.Text & "<td class=""style2"">Parenti Anticipato</td>"
        Lbl_Dettagli.Text = Lbl_Dettagli.Text & "<td class=""style2"" style=""text-align:right;"">" & Format(Math.Round(ImportoParentiAnticipati, 2), "#,##0.00") & "</td>"
        Lbl_Dettagli.Text = Lbl_Dettagli.Text & "<td class=""style2"" style=""text-align:right;"">0,00</td>"
        Lbl_Dettagli.Text = Lbl_Dettagli.Text & "<td class=""style2"" style=""text-align:right;"">" & Format(Math.Round(ImportoParentiAnticipati, 2), "#,##0.00") & "</td>"
        Lbl_Dettagli.Text = Lbl_Dettagli.Text & "<td class=""style2"" style=""text-align:right;"">" & GiorniParentiAnticipati & "</td>"
        Lbl_Dettagli.Text = Lbl_Dettagli.Text & "<td class=""style2"" style=""text-align:right;"">0</td>"
        Lbl_Dettagli.Text = Lbl_Dettagli.Text & "</tr>"

        Lbl_Dettagli.Text = Lbl_Dettagli.Text & "<tr>"
        Lbl_Dettagli.Text = Lbl_Dettagli.Text & "<td class=""style2"">Parenti Totale</td>"
        Lbl_Dettagli.Text = Lbl_Dettagli.Text & "<td class=""style2"" style=""text-align:right;"">" & Format(Math.Round(ImportoParenti - ImportoParentiAssenza, 2), "#,##0.00") & "</td>"
        Lbl_Dettagli.Text = Lbl_Dettagli.Text & "<td class=""style2"" style=""text-align:right;"">" & Format(Math.Round(ImportoParentiAssenza, 2), "#,##0.00") & "</td>"
        Lbl_Dettagli.Text = Lbl_Dettagli.Text & "<td class=""style2"" style=""text-align:right;"">" & Format(Math.Round(ImportoParenti, 2), "#,##0.00") & "</td>"
        Lbl_Dettagli.Text = Lbl_Dettagli.Text & "<td class=""style2"" style=""text-align:right;"">" & GiorniParenti - GiorniParentiAssenza & "</td>"
        Lbl_Dettagli.Text = Lbl_Dettagli.Text & "<td class=""style2"" style=""text-align:right;"">" & GiorniParentiAssenza & "</td>"
        Lbl_Dettagli.Text = Lbl_Dettagli.Text & "</tr>"

        Lbl_Dettagli.Text = Lbl_Dettagli.Text & "<tr>"
        Lbl_Dettagli.Text = Lbl_Dettagli.Text & "<td class=""style2"" colspan=""6"">&nbsp;</td>"
        Lbl_Dettagli.Text = Lbl_Dettagli.Text & "</tr>"

        Lbl_Dettagli.Text = Lbl_Dettagli.Text & "<tr>"
        Lbl_Dettagli.Text = Lbl_Dettagli.Text & "<td class=""style2"">Comune</td>"
        Lbl_Dettagli.Text = Lbl_Dettagli.Text & "<td class=""style2"" style=""text-align:right;"">" & Format(Math.Round(ImportoComune - ImportoComuneAssenza, 2), "#,##0.00") & "</td>"
        Lbl_Dettagli.Text = Lbl_Dettagli.Text & "<td class=""style2"" style=""text-align:right;"">" & Format(Math.Round(ImportoComuneAssenza, 2), "#,##0.00") & "</td>"
        Lbl_Dettagli.Text = Lbl_Dettagli.Text & "<td class=""style2"" style=""text-align:right;"">" & Format(Math.Round(ImportoComune, 2), "#,##0.00") & "</td>"
        Lbl_Dettagli.Text = Lbl_Dettagli.Text & "<td class=""style2"" style=""text-align:right;"">" & GiorniComune & "</td>"
        Lbl_Dettagli.Text = Lbl_Dettagli.Text & "<td class=""style2"" style=""text-align:right;"">" & GiorniComuneAssenza & "</td>"
        Lbl_Dettagli.Text = Lbl_Dettagli.Text & "</tr>"

        Lbl_Dettagli.Text = Lbl_Dettagli.Text & "<tr>"
        Lbl_Dettagli.Text = Lbl_Dettagli.Text & "<td class=""style2"">Jolly</td>"
        Lbl_Dettagli.Text = Lbl_Dettagli.Text & "<td class=""style2"" style=""text-align:right;"">" & Format(Math.Round(ImportoJolly - ImportoJollyAssenza, 2), "#,##0.00") & "</td>"
        Lbl_Dettagli.Text = Lbl_Dettagli.Text & "<td class=""style2"" style=""text-align:right;"">" & Format(Math.Round(ImportoJollyAssenza, 2), "#,##0.00") & "</td>"
        Lbl_Dettagli.Text = Lbl_Dettagli.Text & "<td class=""style2"" style=""text-align:right;"">" & Format(Math.Round(ImportoJolly, 2), "#,##0.00") & "</td>"
        Lbl_Dettagli.Text = Lbl_Dettagli.Text & "<td class=""style2"" style=""text-align:right;"">" & GiorniJolly & "</td>"
        Lbl_Dettagli.Text = Lbl_Dettagli.Text & "<td class=""style2"" style=""text-align:right;"">" & GiorniJollyAssenza & "</td>"
        Lbl_Dettagli.Text = Lbl_Dettagli.Text & "</tr>"

        Lbl_Dettagli.Text = Lbl_Dettagli.Text & "<tr>"
        Lbl_Dettagli.Text = Lbl_Dettagli.Text & "<td class=""style2"">Regione</td>"
        Lbl_Dettagli.Text = Lbl_Dettagli.Text & "<td class=""style2"" style=""text-align:right;"">" & Format(Math.Round(ImportoRegione - ImportoRegioneAssenza, 2), "#,##0.00") & "</td>"
        Lbl_Dettagli.Text = Lbl_Dettagli.Text & "<td class=""style2"" style=""text-align:right;"">" & Format(Math.Round(ImportoRegioneAssenza, 2), "#,##0.00") & "</td>"
        Lbl_Dettagli.Text = Lbl_Dettagli.Text & "<td class=""style2"" style=""text-align:right;"">" & Format(Math.Round(ImportoRegione, 2), "#,##0.00") & "</td>"
        Lbl_Dettagli.Text = Lbl_Dettagli.Text & "<td class=""style2"" style=""text-align:right;"">" & GiorniRegione - GiorniRegioneAssenza & "</td>"
        Lbl_Dettagli.Text = Lbl_Dettagli.Text & "<td class=""style2"" style=""text-align:right;"">" & GiorniRegioneAssenza & "</td>"
        Lbl_Dettagli.Text = Lbl_Dettagli.Text & "</tr>"

        Lbl_Dettagli.Text = Lbl_Dettagli.Text & "</table><br /><br />"

        Lbl_Dettagli.Text = Lbl_Dettagli.Text & "<table cellspacing=""0"" class=""style1"" style=""width: 50%"">"


        Lbl_Dettagli.Text = Lbl_Dettagli.Text & "<tr>"
        Lbl_Dettagli.Text = Lbl_Dettagli.Text & "<td class=""style8"">&nbsp;</td>"
        Lbl_Dettagli.Text = Lbl_Dettagli.Text & "<td class=""style9"">Addebiti</td>"
        Lbl_Dettagli.Text = Lbl_Dettagli.Text & "<td class=""style9"">Accrediti</td>"
        Lbl_Dettagli.Text = Lbl_Dettagli.Text & "<td class=""style9"">&nbsp;Extra Fissi</td>"
        Lbl_Dettagli.Text = Lbl_Dettagli.Text & "</tr>"
        Lbl_Dettagli.Text = Lbl_Dettagli.Text & "<tr>"
        Lbl_Dettagli.Text = Lbl_Dettagli.Text & "<td class=""styleOspite"" style=""text-align:left;"">Ospite</td>"
        Lbl_Dettagli.Text = Lbl_Dettagli.Text & "<td class=""styleOspite"" style=""text-align:right;"">" & Format(ADDOspite, "#,##0.00") & "</td>"
        Lbl_Dettagli.Text = Lbl_Dettagli.Text & "<td class=""styleOspite"" style=""text-align:right;"">" & Format(ACCOspite, "#,##0.00") & "</td>"
        Lbl_Dettagli.Text = Lbl_Dettagli.Text & "<td class=""styleOspite"" style=""text-align:right;"">" & Format(ExtraOspite, "#,##0.00") & "</td>"
        Lbl_Dettagli.Text = Lbl_Dettagli.Text & "</tr>"
        Lbl_Dettagli.Text = Lbl_Dettagli.Text & "<tr>"
        Lbl_Dettagli.Text = Lbl_Dettagli.Text & "<td class=""styleParente"" style=""text-align:left;"">Parenti</td>"
        Lbl_Dettagli.Text = Lbl_Dettagli.Text & "<td class=""styleParente"" style=""text-align:right;"" >" & Format(ADDParenti, "#,##0.00") & "</td>"
        Lbl_Dettagli.Text = Lbl_Dettagli.Text & "<td class=""styleParente"" style=""text-align:right;"">" & Format(ACCParenti, "#,##0.00") & "</td>"
        Lbl_Dettagli.Text = Lbl_Dettagli.Text & "<td class=""styleParente"" style=""text-align:right;"">" & Format(ExtraParenti, "#,##0.00") & "</td>"
        Lbl_Dettagli.Text = Lbl_Dettagli.Text & "</tr>"
        Lbl_Dettagli.Text = Lbl_Dettagli.Text & "<tr>"
        Lbl_Dettagli.Text = Lbl_Dettagli.Text & "<td class=""styleComune"" style=""text-align:left;"">Comune</td>"
        Lbl_Dettagli.Text = Lbl_Dettagli.Text & "<td class=""styleComune"" style=""text-align:right;"">" & Format(ADDComune, "#,##0.00") & "</td>"
        Lbl_Dettagli.Text = Lbl_Dettagli.Text & "<td class=""styleComune"" style=""text-align:right;"">" & Format(ACCComune, "#,##0.00") & "</td>"
        Lbl_Dettagli.Text = Lbl_Dettagli.Text & "<td class=""styleComune"" style=""text-align:right;"">" & Format(ExtraComune, "#,##0.00") & "</td>"
        Lbl_Dettagli.Text = Lbl_Dettagli.Text & "</tr>"
        Lbl_Dettagli.Text = Lbl_Dettagli.Text & "<tr>"
        Lbl_Dettagli.Text = Lbl_Dettagli.Text & "<td class=""styleRegione"" style=""text-align:left;"">Regione</td>"
        Lbl_Dettagli.Text = Lbl_Dettagli.Text & "<td class=""styleRegione"" style=""text-align:right;"">" & Format(ADDRegione, "#,##0.00") & "</td>"
        Lbl_Dettagli.Text = Lbl_Dettagli.Text & "<td class=""styleRegione"" style=""text-align:right;"">" & Format(ACCRegione, "#,##0.00") & "</td>"
        Lbl_Dettagli.Text = Lbl_Dettagli.Text & "<td class=""styleRegione"" style=""text-align:right;"">" & Format(ExtraRegione, "#,##0.00") & "</td>"
        Lbl_Dettagli.Text = Lbl_Dettagli.Text & "</tr>"
        Lbl_Dettagli.Text = Lbl_Dettagli.Text & "<tr>"
        Lbl_Dettagli.Text = Lbl_Dettagli.Text & "<td class=""styleJolly"" style=""text-align:left;"">Jolly</td>"
        Lbl_Dettagli.Text = Lbl_Dettagli.Text & "<td class=""styleJolly"" style=""text-align:right;"">" & Format(ADDJolly, "#,##0.00") & "</td>"
        Lbl_Dettagli.Text = Lbl_Dettagli.Text & "<td class=""styleJolly"" style=""text-align:right;"">" & Format(ACCJolly, "#,##0.00") & "</td>"
        Lbl_Dettagli.Text = Lbl_Dettagli.Text & "<td class=""styleJolly"" style=""text-align:right;"">" & Format(ExtraJolly, "#,##0.00") & "</td>"
        Lbl_Dettagli.Text = Lbl_Dettagli.Text & "</tr>"
        Lbl_Dettagli.Text = Lbl_Dettagli.Text & "</table>"


        Dim MyJs As String


        MyJs = "    $(document).ready(function(){"

        'MyJs = MyJs & "var salesData=["
        'MyJs = MyJs & "{label:""Ospite"", color:""#3366CC"",value: " & Replace(Math.Round(ImportoOspite), ",", ".") & "},"
        'MyJs = MyJs & "{label:""Parente"", color:""#DC3912"",value: " & Replace(Math.Round(ImportoParenti), ",", ".") & "},"
        'MyJs = MyJs & "{label:""Comune"", color:""#FF9900"",value: " & Replace(Math.Round(ImportoComune), ",", ".") & "},"
        'MyJs = MyJs & "{label:""Regione"", color:""#109618"",value: " & Replace(Math.Round(ImportoRegione), ",", ".") & "},"
        'MyJs = MyJs & "{label:""Jolly"", color:""#990099"",value: " & Replace(Math.Round(ImportoJolly), ",", ".") & "}"
        'MyJs = MyJs & "];"


        MyJs = MyJs & "var salesData=["
        MyJs = MyJs & "{label:""Ospite"", color:""#F2C485"",value: " & Replace(Math.Round(ImportoOspite, 2), ",", ".") & "},"
        MyJs = MyJs & "{label:""Parente"", color:""#F28DB3"",value: " & Replace(Math.Round(ImportoParenti, 2), ",", ".") & "},"
        MyJs = MyJs & "{label:""Comune"", color:""#BE87EB"",value: " & Replace(Math.Round(ImportoComune, 2), ",", ".") & "},"
        MyJs = MyJs & "{label:""Regione"", color:""#8AB8F7"",value: " & Replace(Math.Round(ImportoRegione, 2), ",", ".") & "},"
        MyJs = MyJs & "{label:""Jolly"", color:""#8AEFC9"",value: " & Replace(Math.Round(ImportoJolly, 2), ",", ".") & "}"
        MyJs = MyJs & "];"

        MyJs = MyJs & "var svg = d3.select(""#chart2"").append(""svg"").attr(""width"",400).attr(""height"",300);"

        MyJs = MyJs & "svg.append(""g"").attr(""id"",""quotesDonut"");"

        MyJs = MyJs & "Donut3D.draw(""quotesDonut"", salesData, 170, 110, 130, 100, 30, 0);"


        MyJs = MyJs & "});"



        'Dim MyJs As String

        'MyJs = "    $(document).ready(function(){"
        'MyJs = MyJs & "var data = ["
        'MyJs = MyJs & "['Ospite', " & Replace(Math.Round(ImportoOspite), ",", ".") & "],['Parenti', " & Replace(Math.Round(ImportoParenti), ",", ".") & "], ['Comune', " & Replace(Math.Round(ImportoComune), ",", ".") & "], "
        'MyJs = MyJs & "['Regione'," & Replace(Math.Round(ImportoRegione), ",", ".") & " ],['Jolly', " & Replace(Math.Round(ImportoJolly), ",", ".") & "]"
        'MyJs = MyJs & "];"
        'MyJs = MyJs & "var plot1 = jQuery.jqplot ('chart2', [data], "
        'MyJs = MyJs & "{ "
        'MyJs = MyJs & "  seriesDefaults: {           "
        'MyJs = MyJs & "renderer: jQuery.jqplot.PieRenderer, "
        'MyJs = MyJs & "rendererOptions: {"
        'MyJs = MyJs & "showDataLabels: true"
        'MyJs = MyJs & "}"
        'MyJs = MyJs & "}, "
        'MyJs = MyJs & "legend: { show:true, location: 'e' }"
        'MyJs = MyJs & "}"
        'MyJs = MyJs & ");"
        'MyJs = MyJs & "});"

        ScriptManager.RegisterStartupScript(Me, Me.GetType(), "visualizzaRitIm", MyJs, True)
        cn.Close()
    End Sub

    Protected Sub Button1_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles Button1.Click
        If Val(Txt_Anno.Text) < 1990 Then
            ClientScript.RegisterClientScriptBlock(Me.GetType(), "Errore", "VisualizzaErrore('Specificare anno');", True)            
            Exit Sub
        End If
        Call VisualizzaDatiMese(Val(Txt_Anno.Text), Val(Dd_Mese.SelectedValue))
    End Sub

    Protected Sub Btn_Esci_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles Btn_Esci.Click
        Response.Redirect("RicercaAnagrafica.aspx")
    End Sub
End Class
