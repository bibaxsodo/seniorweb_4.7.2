﻿
Partial Class OspitiWeb_FlussoOspiti
    Inherits System.Web.UI.Page

    Private Function OspitePresente(ByVal Cserv As String, ByVal CodOsp As Long, ByVal DataControllo As Date) As Boolean
        Dim RsMovimenti As New ADODB.Recordset
        Dim VB6 As New Cls_FunzioniVB6
        Dim OspitiDb As New ADODB.Connection
        Dim MySql As String
        Dim DataDal As Date = Txt_DataDal.Text
        Dim DataAl As Date = Txt_DataAl.Text


        OspitiDb.Open(Session("DC_OSPITE"))

        MySql = "Select top 1 * From Movimenti Where  CENTROSERVIZIO = '" & Cserv & "' And CODICEOSPITE =" & CodOsp & " And Data <= {ts '" & Format(DataControllo, "yyyy-MM-dd") & " 00:00:00'}  Order By DATA Desc,PROGRESSIVO Desc"


        RsMovimenti.Open(MySql, OspitiDb, ADODB.CursorTypeEnum.adOpenForwardOnly, ADODB.LockTypeEnum.adLockOptimistic)
        If RsMovimenti.EOF Then
            OspitePresente = False
        Else
            OspitePresente = False
            If VB6.MoveFromDbWC(RsMovimenti, "TipoMov") = "13" And Format(VB6.MoveFromDbWC(RsMovimenti, "Data"), "yyyyMMdd") >= Format(DataControllo, "yyyyMMdd") Then
                OspitePresente = True
                'Debug.Print CodOsp & vbTab & RsMovimenti!Data & vbTab & RsMovimenti!TipoMov
            End If
            If VB6.MoveFromDbWC(RsMovimenti, "TipoMov") <> "13" Then
                OspitePresente = True
                'Debug.Print CodOsp & vbTab & RsMovimenti!Data & vbTab & RsMovimenti!TipoMov
            End If
        End If

        RsMovimenti.Close()
    End Function


    Private Function OspitePresenteNelPeriodo(ByVal Cserv As String, ByVal CodOsp As Long, ByVal DataDal As Date, ByVal DataControllo As Date) As Boolean
        Dim RsMovimenti As New ADODB.Recordset
        Dim VB6 As New Cls_FunzioniVB6
        Dim OspitiDb As New ADODB.Connection
        Dim MySql As String


        OspitiDb.Open(Session("DC_OSPITE"))


        MySql = "Select top 1 * From Movimenti Where  CENTROSERVIZIO = '" & Cserv & "' And CODICEOSPITE =" & CodOsp & " And Data <= {ts '" & Format(DataControllo, "yyyy-MM-dd") & " 00:00:00'}  Order By DATA Desc,PROGRESSIVO Desc"


        RsMovimenti.Open(MySql, OspitiDb, ADODB.CursorTypeEnum.adOpenForwardOnly, ADODB.LockTypeEnum.adLockOptimistic)
        If RsMovimenti.EOF Then
            OspitePresenteNelPeriodo = False
        Else
            OspitePresenteNelPeriodo = False
            If VB6.MoveFromDbWC(RsMovimenti, "TipoMov") = "13" And Format(VB6.MoveFromDbWC(RsMovimenti, "Data"), "yyyyMMdd") >= Format(DataDal, "yyyyMMdd") Then
                OspitePresenteNelPeriodo = True
            End If
            If VB6.MoveFromDbWC(RsMovimenti, "TipoMov") <> "13" Then
                OspitePresenteNelPeriodo = True
            End If
        End If

        RsMovimenti.Close()

        OspitiDb.Close()
    End Function

    Private Function ISAssegnaCentroServizio(ByVal CodOsp As Long, ByVal Cserv As String) As Boolean
        Dim MyRs As New ADODB.Recordset
        Dim VB6 As New Cls_FunzioniVB6
        Dim OspitiDb As New ADODB.Connection

        Dim DataDal As Date = Txt_DataDal.Text
        Dim DataAl As Date = Txt_DataAl.Text


        OspitiDb.Open(Session("DC_OSPITE"))


        MyRs.Open("Select * From Movimenti Where TipoMov = '05' And CODICEOSPITE = " & CodOsp & " And CENTROSERVIZIO = '" & Cserv & "' And Data >= {ts '" & Format(DataDal, "yyyy-MM-dd") & " 00:00:00'} and Data <= {ts '" & Format(DataAl, "yyyy-MM-dd") & " 00:00:00'} ", OspitiDb, ADODB.CursorTypeEnum.adOpenKeyset, ADODB.LockTypeEnum.adLockOptimistic)

        If Not MyRs.EOF Then
            ISAssegnaCentroServizio = True
        Else
            ISAssegnaCentroServizio = False
        End If
        MyRs.Close()

        If ISAssegnaCentroServizio = False Then

            MyRs.Open("Select * From Movimenti Where CODICEOSPITE = " & CodOsp & " And CENTROSERVIZIO = '" & Cserv & "' And Data <= {ts '" & Format(DataDal, "yyyy-MM-dd") & " 00:00:00'} ORDER BY DATA DESC", OspitiDb, ADODB.CursorTypeEnum.adOpenKeyset, ADODB.LockTypeEnum.adLockOptimistic)
            If Not MyRs.EOF Then
                If VB6.MoveFromDbWC(MyRs, "TipoMov") = "13" Then
                    ISAssegnaCentroServizio = False
                Else
                    ISAssegnaCentroServizio = True
                End If
            Else
                ISAssegnaCentroServizio = False
            End If
            MyRs.Close()
        End If


        OspitiDb.Close()
    End Function
    Private Function AssegnaCentroServizio(ByVal CodiceOspite As Long, ByVal Data As Date) As String
        Dim MyRs As New ADODB.Recordset
        Dim OspitiDb As New ADODB.Connection
        Dim VB6 As New Cls_FunzioniVB6

        OspitiDb.Open(Session("DC_OSPITE"))



        MyRs.Open("Select Top 1 CENTROSERVIZIO From Movimenti Where TipoMov = '05' And CODICEOSPITE = " & CodiceOspite & " And Data <= {ts '" & Format(Data, "yyyy-MM-dd") & " 00:00:00'} ORDER by DATA Desc ", OspitiDb, ADODB.CursorTypeEnum.adOpenKeyset, ADODB.LockTypeEnum.adLockOptimistic)

        If Not MyRs.EOF Then
            AssegnaCentroServizio = VB6.MoveFromDbWC(MyRs, "CentroServizio")
        End If
        MyRs.Close()

        OspitiDb.Close()
    End Function

    Private Function StatoOspite(ByVal Cserv As String, ByVal CodOsp As Long, ByVal DataControllo As Date) As String
        Dim RsMovimenti As New ADODB.Recordset
        Dim OspitiDb As New ADODB.Connection
        Dim MySql As String
        Dim VB6 As New Cls_FunzioniVB6
        OspitiDb.Open(Session("DC_OSPITE"))

        StatoOspite = "A"


        MySql = "Select top 1 * From StatoAuto Where  CENTROSERVIZIO = '" & Cserv & "' And CODICEOSPITE =" & CodOsp & " And Data <= {ts '" & Format(DataControllo, "yyyy-MM-dd") & " 00:00:00'}  Order By DATA Desc"

        RsMovimenti.Open(MySql, OspitiDb, ADODB.CursorTypeEnum.adOpenKeyset, ADODB.LockTypeEnum.adLockOptimistic)
        If Not RsMovimenti.EOF Then
            StatoOspite = VB6.MoveFromDbWC(RsMovimenti, "STATOAUTO")
        End If
        RsMovimenti.Close()

        

        OspitiDb.Close()
    End Function

    Private Sub FlussoOspiti()
        Dim MyRs As New ADODB.Recordset
        Dim TestRs As New ADODB.Recordset

        Dim PRES0101 As Integer
        Dim PRES3112 As Integer
        Dim Entr As Integer
        Dim Usc As Integer
        Dim Dec As Integer
        Dim CodOsp As Integer
        Dim VB6 As New Cls_FunzioniVB6
        Dim OspitiDb As New ADODB.Connection
        Dim CentroServizio As String

        Dim Eta As String
        Dim Presente As String
        Dim StatoAutoSufficenzaOspite As String

        Dim Eta05 As Integer
        Dim Eta13 As Integer
        Dim Eta14 As Integer
        Dim Eta54 As Integer
        Dim Eta64 As Integer
        Dim Eta65 As Integer
        Dim Eta75 As Integer
        Dim Eta80 As Integer
        Dim Eta100 As Integer

        Dim NONPRES0101 As Integer
        Dim NONPRES3112 As Integer
        Dim NONEntr As Integer
        Dim NONUsc As Integer
        Dim NONDec As Integer
        Dim NONEta05 As Integer
        Dim NONEta13 As Integer
        Dim NONEta14 As Integer
        Dim NONEta54 As Integer
        Dim NONEta64 As Integer
        Dim NONEta65 As Integer
        Dim NONEta75 As Integer
        Dim NONEta80 As Integer
        Dim NONEta100 As Integer

        Dim MPRES0101 As Integer
        Dim MPRES3112 As Integer
        Dim MEntr As Integer
        Dim MUsc As Integer
        Dim MDec As Integer
        Dim MEta05 As Integer
        Dim MEta13 As Integer
        Dim MEta14 As Integer
        Dim MEta54 As Integer
        Dim MEta64 As Integer
        Dim MEta65 As Integer
        Dim MEta75 As Integer
        Dim MEta80 As Integer
        Dim MEta100 As Integer

        Dim MNONPRES0101 As Integer
        Dim MNONPRES3112 As Integer
        Dim MNONEntr As Integer
        Dim MNONUsc As Integer
        Dim MNONDec As Integer
        Dim MNONEta05 As Integer
        Dim MNONEta13 As Integer
        Dim MNONEta14 As Integer
        Dim MNONEta54 As Integer
        Dim MNONEta64 As Integer
        Dim MNONEta65 As Integer
        Dim MNONEta75 As Integer
        Dim MNONEta80 As Integer
        Dim MNONEta100 As Integer

        Dim DataDal As Date = Txt_DataDal.Text
        Dim DataAl As Date = Txt_DataAl.Text

        Dim Appoggio As String = ""


        VB6.StringaOspiti = Session("DC_OSPITE")
        VB6.StringaGenerale = Session("DC_GENERALE")
        VB6.StringaTabelle = Session("DC_TABELLE")


        VB6.ApriDB()


        OspitiDb.Open(Session("DC_OSPITE"))

        PRES0101 = 0
        PRES3112 = 0
        Entr = 0
        Usc = 0
        Dec = 0



        MyRs.Open("Select * From AnagraficaComune Where (NonInUso Is Null OR NonInUso = '') And Tipologia = 'O' ORDER BY CODICEOSPITE", OspitiDb, ADODB.CursorTypeEnum.adOpenKeyset, ADODB.LockTypeEnum.adLockOptimistic)

        Do While Not MyRs.EOF
            CodOsp = VB6.MoveFromDbWC(MyRs, "CodiceOspite")

            If CodOsp = 616 Then
                CodOsp = 616
            End If

            If ISAssegnaCentroServizio(CodOsp, DD_CServ.SelectedValue) Or DD_CServ.SelectedValue = "" Then
                If DD_CServ.SelectedValue = "" Then
                    CentroServizio = AssegnaCentroServizio(CodOsp, DataDal)
                    If CentroServizio = "" Then
                        CentroServizio = AssegnaCentroServizio(CodOsp, DataAl)
                    End If
                Else
                    CentroServizio = DD_CServ.SelectedValue
                End If                

                If VB6.CampoOspite(CodOsp, "SESSO") = "F" Then

                    If StatoOspite(CentroServizio, CodOsp, DataDal) = "A" Then
                        If OspitePresente(CentroServizio, CodOsp, DataDal) = True Then
                            PRES0101 = PRES0101 + 1
                        End If
                    End If
                    If StatoOspite(CentroServizio, CodOsp, DataAl) = "A" Then
                        If OspitePresente(CentroServizio, CodOsp, DataAl) = True Then
                            PRES3112 = PRES3112 + 1
                            lbl_Risultato.Text = lbl_Risultato.Text & CodOsp & "<br/>"
                        End If
                    End If

                    StatoAutoSufficenzaOspite = StatoOspite(CentroServizio, CodOsp, DataDal)
                    If StatoAutoSufficenzaOspite = "" Then
                        StatoAutoSufficenzaOspite = StatoOspite(CentroServizio, CodOsp, DataAl)
                    End If


                    If StatoAutoSufficenzaOspite = "A" Then
                        TestRs.Open("Select count(CodiceOspite) From Movimenti Where TipoMov = '05' And CodiceOspite = " & CodOsp & " And DATA >= {ts '" & Format(DataDal, "yyyy-MM-dd") & " 00:00:00'} And DATA <= {ts '" & Format(DataAl, "yyyy-MM-dd") & " 00:00:00'}", OspitiDb, ADODB.CursorTypeEnum.adOpenKeyset, ADODB.LockTypeEnum.adLockOptimistic)
                        Entr = Entr + Val(TestRs.Fields(0).Value)
                        TestRs.Close()

                        If Session("DC_OSPITE").ToString.ToUpper.IndexOf("CITTADELLA") >= 0 Then
                            TestRs.Open("Select count(CodiceOspite) From Movimenti Where TipoMov = '13' And ((Causale <> '99' And Causale <> '98' ) or Causale  is null)  And CodiceOspite = " & CodOsp & " And DATA >= {ts '" & Format(DataDal, "yyyy-MM-dd") & " 00:00:00'} And DATA <= {ts '" & Format(DataAl, "yyyy-MM-dd") & " 00:00:00'} And  CENTROSERVIZIO  = '" & CentroServizio & "'", OspitiDb, ADODB.CursorTypeEnum.adOpenKeyset, ADODB.LockTypeEnum.adLockOptimistic)
                            Usc = Usc + Val(TestRs.Fields(0).Value)

                            If Val(TestRs.Fields(0).Value) > 0 Then
                                Appoggio = Appoggio & CodOsp & ";"
                            End If
                            TestRs.Close()

                            TestRs.Open("Select count(CodiceOspite) From Movimenti Where TipoMov = '13' And (Causale = '99' or Causale = '98') And CodiceOspite = " & CodOsp & " And DATA >= {ts '" & Format(DataDal, "yyyy-MM-dd") & " 00:00:00'} And DATA <= {ts '" & Format(DataAl, "yyyy-MM-dd") & " 00:00:00'} And  CENTROSERVIZIO = '" & CentroServizio & "'", OspitiDb, ADODB.CursorTypeEnum.adOpenKeyset, ADODB.LockTypeEnum.adLockOptimistic)
                            Dec = Dec + Val(TestRs.Fields(0).Value)
                            If Val(TestRs.Fields(0).Value) > 0 Then
                                Appoggio = Appoggio & CodOsp & ";"
                            End If
                            TestRs.Close()

                        Else
                            TestRs.Open("Select count(CodiceOspite) From Movimenti Where TipoMov = '13' And Causale <> '97' And Causale <> '98'  And CodiceOspite = " & CodOsp & " And DATA >= {ts '" & Format(DataDal, "yyyy-MM-dd") & " 00:00:00'} And DATA <= {ts '" & Format(DataAl, "yyyy-MM-dd") & " 00:00:00'} And  CENTROSERVIZIO = '" & CentroServizio & "'", OspitiDb, ADODB.CursorTypeEnum.adOpenKeyset, ADODB.LockTypeEnum.adLockOptimistic)
                            Usc = Usc + Val(TestRs.Fields(0).Value)
                            TestRs.Close()

                            TestRs.Open("Select count(CodiceOspite) From Movimenti Where TipoMov = '13' And (Causale = '97' or Causale = '98') And CodiceOspite = " & CodOsp & " And DATA >= {ts '" & Format(DataDal, "yyyy-MM-dd") & " 00:00:00'} And DATA <= {ts '" & Format(DataAl, "yyyy-MM-dd") & " 00:00:00'} And  CENTROSERVIZIO = '" & CentroServizio & "'", OspitiDb, ADODB.CursorTypeEnum.adOpenKeyset, ADODB.LockTypeEnum.adLockOptimistic)
                            Dec = Dec + Val(TestRs.Fields(0).Value)
                            TestRs.Close()

                        End If


                        Eta = 0
                        If IsDate(VB6.MoveFromDbWC(MyRs, "DataNascita")) Then
                            Eta = DateDiff("M", VB6.MoveFromDbWC(MyRs, "DataNascita"), DataDal)
                        End If

                        Presente = OspitePresenteNelPeriodo(CentroServizio, CodOsp, DataDal, DataAl)
                        'If DataDal = DataAl Then
                        '    Presente = True
                        'End If
                        Dim Variato As Boolean = False

                        If Presente = True And Eta < (5 * 12) Then
                            Eta05 = Eta05 + 1
                            Variato = True
                        End If
                        If Presente = True And Eta < (13 * 12) And Eta >= (5 * 12) Then
                            Eta13 = Eta13 + 1
                            Variato = True
                        End If
                        If Presente = True And Eta < (17 * 12) And Eta >= (13 * 12) Then
                            Eta14 = Eta14 + 1
                            Variato = True
                        End If
                        If Presente = True And Eta < (55 * 12) And Eta >= (17 * 12) Then
                            Eta54 = Eta54 + 1
                            Variato = True
                        End If
                        If Presente = True And Eta < (65 * 12) And Eta >= (55 * 12) Then
                            Eta64 = Eta64 + 1
                            Variato = True
                        End If


                        If Presente = True And Eta >= 65 * 12 And Eta < 75 * 12 Then
                            Eta65 = Eta65 + 1
                            Variato = True
                        End If

                        If Presente = True And Eta < (80 * 12) And Eta >= (75 * 12) Then
                            Eta75 = Eta75 + 1
                            Variato = True
                        End If

                        If Presente = True And Eta < (85 * 12) And Eta >= (80 * 12) Then
                            Eta80 = Eta80 + 1
                            Variato = True
                        End If


                        If Presente = True And (Eta >= (85 * 12) Or IsDBNull(Eta)) Then
                            Eta100 = Eta100 + 1
                            Variato = True
                        End If

                        If Variato = False Then
                            Variato = True
                        End If
                    End If

                    If StatoOspite(CentroServizio, CodOsp, DataDal) = "N" Then
                        If OspitePresente(CentroServizio, CodOsp, DataDal) = True Then
                            NONPRES0101 = NONPRES0101 + 1
                        End If
                    End If
                    If StatoOspite(CentroServizio, CodOsp, DataAl) = "N" Then
                        If OspitePresente(CentroServizio, CodOsp, DataAl) = True Then
                            NONPRES3112 = NONPRES3112 + 1
                            lbl_Risultato.Text = lbl_Risultato.Text & CodOsp & "<br/>"
                        End If
                    End If
                    If StatoAutoSufficenzaOspite = "N" Then
                        TestRs.Open("Select count(CodiceOspite) From Movimenti Where TipoMov = '05' And CodiceOspite = " & CodOsp & " And DATA >= {ts '" & Format(DataDal, "yyyy-MM-dd") & " 00:00:00'} And DATA <= {ts '" & Format(DataAl, "yyyy-MM-dd") & " 00:00:00'}", OspitiDb, ADODB.CursorTypeEnum.adOpenKeyset, ADODB.LockTypeEnum.adLockOptimistic)
                        If Not TestRs.EOF Then
                            NONEntr = NONEntr + Val(TestRs.Fields(0).Value)
                        End If
                        TestRs.Close()



                        If Session("DC_OSPITE").ToString.ToUpper.IndexOf("CITTADELLA") >= 0 Then
                            TestRs.Open("Select count(CodiceOspite) From Movimenti Where TipoMov = '13' And ((Causale <> '99' And Causale <> '98' ) or Causale  is null)  And CodiceOspite = " & CodOsp & " And DATA >= {ts '" & Format(DataDal, "yyyy-MM-dd") & " 00:00:00'} And DATA <= {ts '" & Format(DataAl, "yyyy-MM-dd") & " 00:00:00'} And  CENTROSERVIZIO = '" & CentroServizio & "'", OspitiDb, ADODB.CursorTypeEnum.adOpenKeyset, ADODB.LockTypeEnum.adLockOptimistic)
                            NONUsc = NONUsc + Val(TestRs.Fields(0).Value)
                            If Val(TestRs.Fields(0).Value) > 0 Then
                                Appoggio = Appoggio & CodOsp & ";"
                            End If
                            TestRs.Close()

                            TestRs.Open("Select count(CodiceOspite) From Movimenti Where TipoMov = '13' And (Causale = '99' or Causale = '98') And CodiceOspite = " & CodOsp & " And DATA >= {ts '" & Format(DataDal, "yyyy-MM-dd") & " 00:00:00'} And DATA <= {ts '" & Format(DataAl, "yyyy-MM-dd") & " 00:00:00'} And  CENTROSERVIZIO = '" & CentroServizio & "'", OspitiDb, ADODB.CursorTypeEnum.adOpenKeyset, ADODB.LockTypeEnum.adLockOptimistic)
                            NONDec = NONDec + Val(TestRs.Fields(0).Value)
                            If Val(TestRs.Fields(0).Value) > 0 Then
                                Appoggio = Appoggio & CodOsp & ";"
                            End If
                            TestRs.Close()

                        Else
                            TestRs.Open("Select count(CodiceOspite) From Movimenti Where TipoMov = '13' And Causale <> '97' And Causale <> '98'  And CodiceOspite = " & CodOsp & " And DATA >= {ts '" & Format(DataDal, "yyyy-MM-dd") & " 00:00:00'} And DATA <= {ts '" & Format(DataAl, "yyyy-MM-dd") & " 00:00:00'} And  CENTROSERVIZIO = '" & CentroServizio & "'", OspitiDb, ADODB.CursorTypeEnum.adOpenKeyset, ADODB.LockTypeEnum.adLockOptimistic)
                            NONUsc = NONUsc + Val(TestRs.Fields(0).Value)
                            TestRs.Close()

                            TestRs.Open("Select count(CodiceOspite) From Movimenti Where TipoMov = '13' And (Causale = '97' or Causale = '98') And CodiceOspite = " & CodOsp & " And DATA >= {ts '" & Format(DataDal, "yyyy-MM-dd") & " 00:00:00'} And DATA <= {ts '" & Format(DataAl, "yyyy-MM-dd") & " 00:00:00'} And  CENTROSERVIZIO = '" & CentroServizio & "'", OspitiDb, ADODB.CursorTypeEnum.adOpenKeyset, ADODB.LockTypeEnum.adLockOptimistic)
                            NONDec = NONDec + Val(TestRs.Fields(0).Value)
                            TestRs.Close()

                        End If


                        Eta = 0
                        If IsDate(VB6.MoveFromDbWC(MyRs, "DataNascita")) Then
                            Eta = DateDiff("M", VB6.MoveFromDbWC(MyRs, "DataNascita"), DataDal)
                        End If

                        Presente = OspitePresenteNelPeriodo(CentroServizio, CodOsp, DataDal, DataAl)
                        'If DataDal = DataAl Then
                        '    Presente = True
                        'End If

                        If Presente = True And Eta < 5 * 12 Then
                            NONEta05 = NONEta05 + 1
                        End If
                        If Presente = True And Eta < 13 * 12 And Eta >= 5 * 12 Then
                            NONEta13 = NONEta13 + 1
                        End If
                        If Presente = True And Eta < 17 * 12 And Eta >= 13 * 12 Then
                            NONEta14 = NONEta14 + 1
                        End If
                        If Presente = True And Eta < 55 * 12 And Eta >= 17 * 12 Then
                            NONEta54 = NONEta54 + 1
                        End If
                        If Presente = True And Eta < 64 * 12 And Eta >= 55 * 12 Then
                            NONEta64 = NONEta64 + 1
                        End If

                        If Presente = True And Eta >= 64 * 12 And Eta < 75 * 12 Then
                            NONEta65 = NONEta65 + 1
                        End If

                        If Presente = True And Eta < (80 * 12) And Eta >= (75 * 12) Then
                            NONEta75 = NONEta75 + 1
                        End If

                        If Presente = True And Eta < (85 * 12) And Eta >= (80 * 12) Then
                            NONEta80 = NONEta80 + 1
                        End If


                        If Presente = True And (Eta >= (85 * 12) Or IsDBNull(Eta)) Then
                            NONEta100 = NONEta100 + 1
                        End If


                        'If Presente = True And (ETA >= 75 * 12 Or IsNull(ETA)) Then
                        '   NONEta75 = NONEta75 + 1
                        'End If
                    End If
                Else

                    StatoAutoSufficenzaOspite = StatoOspite(CentroServizio, CodOsp, DataDal)
                    If StatoAutoSufficenzaOspite = "" Then
                        StatoAutoSufficenzaOspite = StatoOspite(CentroServizio, CodOsp, DataAl)
                    End If



                    If StatoOspite(CentroServizio, CodOsp, DataDal) = "A" Then
                        If OspitePresente(CentroServizio, CodOsp, DataDal) = True Then
                            MPRES0101 = MPRES0101 + 1
                        End If
                    End If
                    If StatoOspite(CentroServizio, CodOsp, DataAl) = "A" Then
                        If OspitePresente(CentroServizio, CodOsp, DataAl) = True Then
                            MPRES3112 = MPRES3112 + 1
                            lbl_Risultato.Text = lbl_Risultato.Text & CodOsp & "<br/>"
                        End If
                    End If


                    If StatoAutoSufficenzaOspite = "A" Then
                        TestRs.Open("Select count(CodiceOspite) From Movimenti Where TipoMov = '05' And CodiceOspite = " & CodOsp & " And DATA >= {ts '" & Format(DataDal, "yyyy-MM-dd") & " 00:00:00'} And DATA <= {ts '" & Format(DataAl, "yyyy-MM-dd") & " 00:00:00'}", OspitiDb, ADODB.CursorTypeEnum.adOpenKeyset, ADODB.LockTypeEnum.adLockOptimistic)
                        MEntr = MEntr + Val(TestRs.Fields(0).Value)
                        TestRs.Close()


                        If Session("DC_OSPITE").ToString.ToUpper.IndexOf("CITTADELLA") >= 0 Then
                            TestRs.Open("Select count(CodiceOspite) From Movimenti Where TipoMov = '13' And ((Causale <> '99' And Causale <> '98' ) or Causale  is null)  And CodiceOspite = " & CodOsp & " And DATA >= {ts '" & Format(DataDal, "yyyy-MM-dd") & " 00:00:00'} And DATA <= {ts '" & Format(DataAl, "yyyy-MM-dd") & " 00:00:00'} And  CENTROSERVIZIO = '" & CentroServizio & "'", OspitiDb, ADODB.CursorTypeEnum.adOpenKeyset, ADODB.LockTypeEnum.adLockOptimistic)
                            MUsc = MUsc + Val(TestRs.Fields(0).Value)
                            TestRs.Close()

                            TestRs.Open("Select count(CodiceOspite) From Movimenti Where TipoMov = '13' And (Causale = '99' or Causale = '98') And CodiceOspite = " & CodOsp & " And DATA >= {ts '" & Format(DataDal, "yyyy-MM-dd") & " 00:00:00'} And DATA <= {ts '" & Format(DataAl, "yyyy-MM-dd") & " 00:00:00'} And  CENTROSERVIZIO = '" & CentroServizio & "'", OspitiDb, ADODB.CursorTypeEnum.adOpenKeyset, ADODB.LockTypeEnum.adLockOptimistic)
                            MDec = MDec + Val(TestRs.Fields(0).Value)
                            TestRs.Close()

                        Else
                            TestRs.Open("Select count(CodiceOspite) From Movimenti Where TipoMov = '13' And Causale <> '97' And Causale <> '98'  And CodiceOspite = " & CodOsp & " And DATA >= {ts '" & Format(DataDal, "yyyy-MM-dd") & " 00:00:00'} And DATA <= {ts '" & Format(DataAl, "yyyy-MM-dd") & " 00:00:00'} And  CENTROSERVIZIO = '" & CentroServizio & "'", OspitiDb, ADODB.CursorTypeEnum.adOpenKeyset, ADODB.LockTypeEnum.adLockOptimistic)
                            MUsc = MUsc + Val(TestRs.Fields(0).Value)
                            TestRs.Close()

                            TestRs.Open("Select count(CodiceOspite) From Movimenti Where TipoMov = '13' And (Causale = '97' or Causale = '98') And CodiceOspite = " & CodOsp & " And DATA >= {ts '" & Format(DataDal, "yyyy-MM-dd") & " 00:00:00'} And DATA <= {ts '" & Format(DataAl, "yyyy-MM-dd") & " 00:00:00'} And  CENTROSERVIZIO = '" & CentroServizio & "'", OspitiDb, ADODB.CursorTypeEnum.adOpenKeyset, ADODB.LockTypeEnum.adLockOptimistic)
                            MDec = MDec + Val(TestRs.Fields(0).Value)
                            TestRs.Close()

                        End If


                        Eta = 0
                        If IsDate(VB6.MoveFromDbWC(MyRs, "DataNascita")) Then
                            Eta = DateDiff("M", VB6.MoveFromDbWC(MyRs, "DataNascita"), DataDal)
                        End If


                        Presente = OspitePresenteNelPeriodo(CentroServizio, CodOsp, DataDal, DataAl)
                        'If DataDal = DataAl Then
                        '    Presente = True
                        'End If

                        If Presente = True And Eta < 5 * 12 Then
                            MEta05 = MEta05 + 1
                        End If
                        If Presente = True And Eta < 13 * 12 And Eta > 5 * 12 Then
                            MEta13 = MEta13 + 1
                        End If
                        If Presente = True And Eta < 17 * 12 And Eta >= 13 * 12 Then
                            MEta14 = MEta14 + 1
                        End If
                        If Presente = True And Eta < 54 * 12 And Eta >= 17 * 12 Then
                            MEta54 = MEta54 + 1
                        End If
                        If Presente = True And Eta < 64 * 12 And Eta >= 55 * 12 Then
                            MEta64 = MEta64 + 1
                        End If
                        If Presente = True And Eta >= 64 * 12 And Eta < 75 * 12 Then
                            MEta65 = MEta65 + 1
                        End If

                        If Presente = True And Eta < (80 * 12) And Eta >= (75 * 12) Then
                            MEta75 = MEta75 + 1
                        End If

                        If Presente = True And Eta < (85 * 12) And Eta >= (80 * 12) Then
                            MEta80 = MEta80 + 1
                        End If



                        If Presente = True And (Eta >= (85 * 12) Or IsDBNull(Eta)) Then
                            MEta100 = MEta100 + 1
                        End If

                        'If Presente = True And ETA >= 75 * 12 Or IsNull(ETA) Then
                        '   MEta75 = MEta75 + 1
                        'End If

                    End If


                    If StatoOspite(CentroServizio, CodOsp, DataDal) = "N" Then
                        If OspitePresente(CentroServizio, CodOsp, DataDal) = True Then
                            MNONPRES0101 = MNONPRES0101 + 1
                        End If
                    End If

                    If StatoOspite(CentroServizio, CodOsp, DataAl) = "N" Then
                        If OspitePresente(CentroServizio, CodOsp, DataAl) = True Then
                            MNONPRES3112 = MNONPRES3112 + 1
                            lbl_Risultato.Text = lbl_Risultato.Text & CodOsp & "<br/>"
                        End If
                    End If

                    If StatoAutoSufficenzaOspite = "N" Then
                        TestRs.Open("Select count(CodiceOspite) From Movimenti Where TipoMov = '05' And CodiceOspite = " & CodOsp & " And DATA >= {ts '" & Format(DataDal, "yyyy-MM-dd") & " 00:00:00'} And DATA <= {ts '" & Format(DataAl, "yyyy-MM-dd") & " 00:00:00'}", OspitiDb, ADODB.CursorTypeEnum.adOpenKeyset, ADODB.LockTypeEnum.adLockOptimistic)
                        If Not TestRs.EOF Then
                            MNONEntr = MNONEntr + Val(TestRs.Fields(0).Value)
                        End If
                        TestRs.Close()

                        If Session("DC_OSPITE").ToString.ToUpper.IndexOf("CITTADELLA") >= 0 Then
                            TestRs.Open("Select count(CodiceOspite) From Movimenti Where TipoMov = '13' And ((Causale <> '99' And Causale <> '98' ) or Causale  is null)  And CodiceOspite = " & CodOsp & " And DATA >= {ts '" & Format(DataDal, "yyyy-MM-dd") & " 00:00:00'} And DATA <= {ts '" & Format(DataAl, "yyyy-MM-dd") & " 00:00:00'} And  CENTROSERVIZIO = '" & CentroServizio & "'", OspitiDb, ADODB.CursorTypeEnum.adOpenKeyset, ADODB.LockTypeEnum.adLockOptimistic)
                            MNONUsc = MNONUsc + Val(TestRs.Fields(0).Value)
                            TestRs.Close()

                            TestRs.Open("Select count(CodiceOspite) From Movimenti Where TipoMov = '13' And (Causale = '99' or Causale = '98') And CodiceOspite = " & CodOsp & " And DATA >= {ts '" & Format(DataDal, "yyyy-MM-dd") & " 00:00:00'} And DATA <= {ts '" & Format(DataAl, "yyyy-MM-dd") & " 00:00:00'} And  CENTROSERVIZIO = '" & CentroServizio & "'", OspitiDb, ADODB.CursorTypeEnum.adOpenKeyset, ADODB.LockTypeEnum.adLockOptimistic)
                            MNONDec = MNONDec + Val(TestRs.Fields(0).Value)
                            TestRs.Close()

                        Else
                            TestRs.Open("Select count(CodiceOspite) From Movimenti Where TipoMov = '13' And Causale <> '97' And Causale <> '98'  And CodiceOspite = " & CodOsp & " And DATA >= {ts '" & Format(DataDal, "yyyy-MM-dd") & " 00:00:00'} And DATA <= {ts '" & Format(DataAl, "yyyy-MM-dd") & " 00:00:00'} And  CENTROSERVIZIO = '" & CentroServizio & "'", OspitiDb, ADODB.CursorTypeEnum.adOpenKeyset, ADODB.LockTypeEnum.adLockOptimistic)
                            MNONUsc = MNONUsc + Val(TestRs.Fields(0).Value)
                            TestRs.Close()

                            TestRs.Open("Select count(CodiceOspite) From Movimenti Where TipoMov = '13' And (Causale = '97' or Causale = '98') And CodiceOspite = " & CodOsp & " And DATA >= {ts '" & Format(DataDal, "yyyy-MM-dd") & " 00:00:00'} And DATA <= {ts '" & Format(DataAl, "yyyy-MM-dd") & " 00:00:00'} And  CENTROSERVIZIO = '" & CentroServizio & "'", OspitiDb, ADODB.CursorTypeEnum.adOpenKeyset, ADODB.LockTypeEnum.adLockOptimistic)
                            MNONDec = MNONDec + Val(TestRs.Fields(0).Value)
                            TestRs.Close()

                        End If


                        Eta = 0
                        If IsDate(VB6.MoveFromDbWC(MyRs, "DataNascita")) Then
                            Eta = DateDiff("M", VB6.MoveFromDbWC(MyRs, "DataNascita"), DataDal)
                        End If


                        Presente = OspitePresenteNelPeriodo(CentroServizio, CodOsp, DataDal, DataAl)
                        'If DataDal = DataAl Then
                        '    Presente = True
                        'End If

                        Dim Entrato As Boolean = False
                        If Presente = True And Eta < 5 * 12 Then
                            MNONEta05 = MNONEta05 + 1
                            Entrato = True
                        End If
                        If Presente = True And Eta < 13 * 12 And Eta >= 5 * 12 Then
                            MNONEta13 = MNONEta13 + 1
                            Entrato = True
                        End If
                        If Presente = True And Eta < 17 * 12 And Eta >= 13 * 12 Then
                            MNONEta14 = MNONEta14 + 1
                            Entrato = True
                        End If
                        If Presente = True And Eta < 55 * 12 And Eta >= 17 * 12 Then
                            MNONEta54 = MNONEta54 + 1
                            Entrato = True
                        End If
                        If Presente = True And Eta < 64 * 12 And Eta >= 55 * 12 Then
                            MNONEta64 = MNONEta64 + 1
                            Entrato = True
                        End If
                        If Presente = True And Eta >= 64 * 12 And Eta < 75 * 12 Then
                            MNONEta65 = MNONEta65 + 1
                            Entrato = True
                        End If

                        If Presente = True And Eta < (80 * 12) And Eta >= (75 * 12) Then
                            MNONEta75 = MNONEta75 + 1
                            Entrato = True
                        End If

                        If Presente = True And Eta < (85 * 12) And Eta >= (80 * 12) Then
                            MNONEta80 = MNONEta80 + 1
                            Entrato = True
                        End If

                        If Presente = True And (Eta >= (85 * 12) Or IsDBNull(Eta)) Then
                            MNONEta100 = MNONEta100 + 1
                            Entrato = True
                        End If

                        If Entrato = False Then
                            Entrato = True
                        End If
                        'If Presente = True And ETA >= 75 * 12 Or IsNull(ETA) Then
                        '  MNONEta75 = MNONEta75 + 1
                        'End If

                    End If
                End If
            End If
            MyRs.MoveNext()
        Loop
        MyRs.Close()



        lbl_Risultato.Text = "<table><tr>"


        lbl_Risultato.Text = lbl_Risultato.Text & "<td></td>"
        lbl_Risultato.Text = lbl_Risultato.Text & "<td>" & "Presenti " & Txt_DataDal.Text & "</td>"
        lbl_Risultato.Text = lbl_Risultato.Text & "<td>" & "Presenti " & Txt_DataAl.Text & "</td>"
        lbl_Risultato.Text = lbl_Risultato.Text & "<td>" & "Accolti " & "</td>"
        lbl_Risultato.Text = lbl_Risultato.Text & "<td>" & "Usciti " & "</td>"
        lbl_Risultato.Text = lbl_Risultato.Text & "<td>" & "Deceduti " & "</td>"
        lbl_Risultato.Text = lbl_Risultato.Text & "<td>" & "0-5" & "</td>"
        lbl_Risultato.Text = lbl_Risultato.Text & "<td>" & "6-13" & "</td>"
        lbl_Risultato.Text = lbl_Risultato.Text & "<td>" & "14-17" & "</td>"
        lbl_Risultato.Text = lbl_Risultato.Text & "<td>" & "18-54" & "</td>"
        lbl_Risultato.Text = lbl_Risultato.Text & "<td>" & "55-64" & "</td>"
        lbl_Risultato.Text = lbl_Risultato.Text & "<td>" & "65-74" & "</td>"
        lbl_Risultato.Text = lbl_Risultato.Text & "<td>" & "75-79" & "</td>"
        lbl_Risultato.Text = lbl_Risultato.Text & "<td>" & "80-84" & "</td>"
        lbl_Risultato.Text = lbl_Risultato.Text & "<td>" & ">85" & "</td>"
        lbl_Risultato.Text = lbl_Risultato.Text & "</tr>"


        lbl_Risultato.Text = lbl_Risultato.Text & "<tr>"
        lbl_Risultato.Text = lbl_Risultato.Text & "<td>" & "Auto F" & "</td>"
        lbl_Risultato.Text = lbl_Risultato.Text & "<td>" & PRES0101 & "</td>"
        lbl_Risultato.Text = lbl_Risultato.Text & "<td>" & PRES3112 & "</td>"
        lbl_Risultato.Text = lbl_Risultato.Text & "<td>" & Entr & "</td>"
        lbl_Risultato.Text = lbl_Risultato.Text & "<td>" & Usc & "</td>"
        lbl_Risultato.Text = lbl_Risultato.Text & "<td>" & Dec & "</td>"
        lbl_Risultato.Text = lbl_Risultato.Text & "<td>" & Eta05 & "</td>"
        lbl_Risultato.Text = lbl_Risultato.Text & "<td>" & Eta13 & "</td>"
        lbl_Risultato.Text = lbl_Risultato.Text & "<td>" & Eta14 & "</td>"
        lbl_Risultato.Text = lbl_Risultato.Text & "<td>" & Eta54 & "</td>"
        lbl_Risultato.Text = lbl_Risultato.Text & "<td>" & Eta64 & "</td>"
        lbl_Risultato.Text = lbl_Risultato.Text & "<td>" & Eta65 & "</td>"
        lbl_Risultato.Text = lbl_Risultato.Text & "<td>" & Eta75 & "</td>"
        lbl_Risultato.Text = lbl_Risultato.Text & "<td>" & Eta80 & "</td>"
        lbl_Risultato.Text = lbl_Risultato.Text & "<td>" & Eta100 & "</td>"
        lbl_Risultato.Text = lbl_Risultato.Text & "</tr>"
        lbl_Risultato.Text = lbl_Risultato.Text & "<tr>"
        lbl_Risultato.Text = lbl_Risultato.Text & "<td>" & "Nonauto F" & "</td>"
        lbl_Risultato.Text = lbl_Risultato.Text & "<td>" & NONPRES0101 & "</td>"
        lbl_Risultato.Text = lbl_Risultato.Text & "<td>" & NONPRES3112 & "</td>"
        lbl_Risultato.Text = lbl_Risultato.Text & "<td>" & NONEntr & "</td>"
        lbl_Risultato.Text = lbl_Risultato.Text & "<td>" & NONUsc & "</td>"
        lbl_Risultato.Text = lbl_Risultato.Text & "<td>" & NONDec & "</td>"
        lbl_Risultato.Text = lbl_Risultato.Text & "<td>" & NONEta05 & "</td>"
        lbl_Risultato.Text = lbl_Risultato.Text & "<td>" & NONEta13 & "</td>"
        lbl_Risultato.Text = lbl_Risultato.Text & "<td>" & NONEta14 & "</td>"
        lbl_Risultato.Text = lbl_Risultato.Text & "<td>" & NONEta54 & "</td>"
        lbl_Risultato.Text = lbl_Risultato.Text & "<td>" & NONEta64 & "</td>"
        lbl_Risultato.Text = lbl_Risultato.Text & "<td>" & NONEta65 & "</td>"
        lbl_Risultato.Text = lbl_Risultato.Text & "<td>" & NONEta75 & "</td>"
        lbl_Risultato.Text = lbl_Risultato.Text & "<td>" & NONEta80 & "</td>"
        lbl_Risultato.Text = lbl_Risultato.Text & "<td>" & NONEta100 & "</td>"
        lbl_Risultato.Text = lbl_Risultato.Text & "</tr>"

        lbl_Risultato.Text = lbl_Risultato.Text & "<tr>"
        lbl_Risultato.Text = lbl_Risultato.Text & "<td>Auto M" & "</td>"
        lbl_Risultato.Text = lbl_Risultato.Text & "<td>" & MPRES0101 & "</td>"
        lbl_Risultato.Text = lbl_Risultato.Text & "<td>" & MPRES3112 & "</td>"
        lbl_Risultato.Text = lbl_Risultato.Text & "<td>" & MEntr & "</td>"
        lbl_Risultato.Text = lbl_Risultato.Text & "<td>" & MUsc & "</td>"
        lbl_Risultato.Text = lbl_Risultato.Text & "<td>" & MDec & "</td>"
        lbl_Risultato.Text = lbl_Risultato.Text & "<td>" & MEta05 & "</td>"
        lbl_Risultato.Text = lbl_Risultato.Text & "<td>" & MEta13 & "</td>"
        lbl_Risultato.Text = lbl_Risultato.Text & "<td>" & MEta14 & "</td>"
        lbl_Risultato.Text = lbl_Risultato.Text & "<td>" & MEta54 & "</td>"
        lbl_Risultato.Text = lbl_Risultato.Text & "<td>" & MEta64 & "</td>"
        lbl_Risultato.Text = lbl_Risultato.Text & "<td>" & MEta65 & "</td>"
        lbl_Risultato.Text = lbl_Risultato.Text & "<td>" & MEta75 & "</td>"
        lbl_Risultato.Text = lbl_Risultato.Text & "<td>" & MEta80 & "</td>"
        lbl_Risultato.Text = lbl_Risultato.Text & "<td>" & MEta100 & "</td>"
        lbl_Risultato.Text = lbl_Risultato.Text & "</tr>"

        lbl_Risultato.Text = lbl_Risultato.Text & "<tr>"
        lbl_Risultato.Text = lbl_Risultato.Text & "<td>Nonauto M" & "</td>"
        lbl_Risultato.Text = lbl_Risultato.Text & "<td>" & MNONPRES0101 & "</td>"
        lbl_Risultato.Text = lbl_Risultato.Text & "<td>" & MNONPRES3112 & "</td>"
        lbl_Risultato.Text = lbl_Risultato.Text & "<td>" & MNONEntr & "</td>"
        lbl_Risultato.Text = lbl_Risultato.Text & "<td>" & MNONUsc & "</td>"
        lbl_Risultato.Text = lbl_Risultato.Text & "<td>" & MNONDec & "</td>"
        lbl_Risultato.Text = lbl_Risultato.Text & "<td>" & MNONEta05 & "</td>"
        lbl_Risultato.Text = lbl_Risultato.Text & "<td>" & MNONEta13 & "</td>"
        lbl_Risultato.Text = lbl_Risultato.Text & "<td>" & MNONEta14 & "</td>"
        lbl_Risultato.Text = lbl_Risultato.Text & "<td>" & MNONEta54 & "</td>"
        lbl_Risultato.Text = lbl_Risultato.Text & "<td>" & MNONEta64 & "</td>"
        lbl_Risultato.Text = lbl_Risultato.Text & "<td>" & MNONEta65 & "</td>"
        lbl_Risultato.Text = lbl_Risultato.Text & "<td>" & MNONEta75 & "</td>"
        lbl_Risultato.Text = lbl_Risultato.Text & "<td>" & MNONEta80 & "</td>"
        lbl_Risultato.Text = lbl_Risultato.Text & "<td>" & MNONEta100 & "</td>"
        lbl_Risultato.Text = lbl_Risultato.Text & "</tr>"

        lbl_Risultato.Text = lbl_Risultato.Text & "</table>"




    End Sub

    Protected Sub ImageButton3_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles ImageButton3.Click

        If Not IsDate(Txt_DataDal.Text) Then
            REM ClientScript.RegisterClientScriptBlock(Me.GetType(), "Errore", "VisualizzaErrore('Data dal non corretta');", True)
            ScriptManager.RegisterStartupScript(Me, Me.GetType(), "visualizzaA", "VisualizzaErrore('Data dal non corretta');", True)
            Call EseguiJS()
            Exit Sub
        End If
        If Not IsDate(Txt_DataAl.Text) Then
            REM ClientScript.RegisterClientScriptBlock(Me.GetType(), "Errore", "VisualizzaErrore('Data al non corretta');", True)
            ScriptManager.RegisterStartupScript(Me, Me.GetType(), "visualizzaA", "VisualizzaErrore('Data al non corretta');", True)
            Call EseguiJS()
            Exit Sub
        End If


        If DD_CServ.SelectedValue = "" Then
            REM ClientScript.RegisterClientScriptBlock(Me.GetType(), "Errore", "VisualizzaErrore('Data al non corretta');", True)
            ScriptManager.RegisterStartupScript(Me, Me.GetType(), "visualizzaA", "VisualizzaErrore('Specifica centro servizio');", True)
            Call EseguiJS()
            Exit Sub
        End If



        Call FlussoOspiti()

        Call EseguiJS()
    End Sub

    Private Sub EseguiJS()
        Dim MyJs As String
        MyJs = "$(document).ready(function() {"

        MyJs = MyJs & "var els = document.getElementsByTagName(""*"");"

        MyJs = MyJs & "for (var i=0;i<els.length;i++)"
        MyJs = MyJs & "if ( els[i].id ) { "
        MyJs = MyJs & " var appoggio =els[i].id; "
        MyJs = MyJs & " if (appoggio.match('Txt_Data')!= null) {  "
        MyJs = MyJs & " $(els[i]).mask(""99/99/9999"");"
        MyJs = MyJs & " $(els[i]).datepicker({ changeMonth: true,changeYear: true,  yearRange: ""1990:" & Year(Now) + 5 & """},$.datepicker.regional[""it""]);"
        MyJs = MyJs & "    }"
        MyJs = MyJs & "} "
        MyJs = MyJs & "if (window.innerHeight>0) { $(""#BarraLaterale"").css(""height"",(window.innerHeight - 94) + ""px""); } else"
        MyJs = MyJs & "{ $(""#BarraLaterale"").css(""height"",(document.documentElement.offsetHeight - 94) + ""px"");  }"
        MyJs = MyJs & "});"

        'MyJs = "$(document).ready(function() { $('.myClass').keypress(function() { handleEnter($('.myClass').val(), true, true); } );     $('#" & Txt_ImportoPagato.ClientID & "').blur(function() { var ap = formatNumber ($('#" & Txt_ImportoPagato.ClientID & "').val(),2); $('#" & Txt_ImportoPagato.ClientID & "').val(ap); }); });"
        ScriptManager.RegisterStartupScript(Me, Me.GetType(), "visualizzaRitIm", MyJs, True)
    End Sub

    Private Sub AggiornaCServ()
        Dim kCsrv As New Cls_CentroServizio

        If DD_Struttura.SelectedValue = "" Then
            kCsrv.UpDateDropBox(Session("DC_OSPITE"), DD_CServ)
        Else
            kCsrv.UpDateDropBoxStruttura(Session("DC_OSPITE"), DD_CServ, DD_Struttura.SelectedValue)
        End If
    End Sub



    Protected Sub DD_Struttura_TextChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles DD_Struttura.TextChanged
        AggiornaCServ()
        Call EseguiJS()
    End Sub


    Protected Sub OspitiWeb_FlussoOspiti_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Call EseguiJS()
        If Trim(Session("UTENTE")) = "" Then
            Response.Redirect("../Login.aspx")
            Exit Sub
        End If

        If Page.IsPostBack = True Then Exit Sub


        Dim K1 As New Cls_SqlString

        Dim Barra As New Cls_BarraSenior

        If Not IsNothing(Session("RicercaAnagraficaSQLString")) Then
            K1 = Session("RicercaAnagraficaSQLString")
        End If

        Lbl_BarraSenior.Text = Barra.CodiceBarra(Application("SENIOR"), Session("UTENTE"), Page, K1)

        Dim kVilla As New Cls_TabelleDescrittiveOspitiAccessori


        kVilla.UpDateDropBox(Session("DC_OSPITIACCESSORI"), "VIL", DD_Struttura)
        If DD_Struttura.Items.Count = 1 Then
            Call AggiornaCServ()
        End If

        Txt_DataAl.Text = Format(Now, "dd/MM/yyyy")
        Txt_DataDal.Text = Format(Now, "dd/MM/yyyy")

        Lbl_Utente.Text = Session("UTENTE")

        Call EseguiJS()
    End Sub

    Protected Sub Btn_Esci_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles Btn_Esci.Click
        Response.Redirect("Menu_Visualizzazioni.aspx")
    End Sub
End Class
