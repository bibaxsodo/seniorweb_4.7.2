﻿<%@ Page Language="VB" AutoEventWireup="false" Inherits="OspitiWeb_CreaDocumentoRetta" CodeFile="CreaDocumentoRetta.aspx.vb" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="xasp" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
    <meta http-equiv="x-ua-compatible" content="IE=9" />
    <title>Crea Documento</title>
    <asp:PlaceHolder runat="server">
        <%: System.Web.Optimization.Styles.Render("~/Content/AjaxControlToolkit/Styles/Bundle") %>
    </asp:PlaceHolder>
    <link href="ospiti.css?ver=11" rel="stylesheet" type="text/css" />
    <link rel="shortcut icon" href="images/SENIOR.ico" />
    <link href="js/jquery.autocomplete.css" rel="stylesheet" type="text/css" />

    <script src="js/jquery-1.5.1.min.js" type="text/javascript"></script>
    <script src="js/jquery.autocomplete.js" type="text/javascript"></script>
    <script src="js/soapclient.js" type="text/javascript"></script>
    <script src="/js/convertinumero.js" type="text/javascript"></script>

    <script src="js/jquery.maskedinput-1.3.min.js" type="text/javascript"></script>
    <script src="/js/formatnumer.js" type="text/javascript"></script>

    <script src="/js/pianoconti.js" type="text/javascript"></script>
    <script src="js/JSErrore.js" type="text/javascript"></script>
    <link rel="stylesheet" href="dialogbox/redips-dialog.css" type="text/css" media="screen" />
    <script type="text/javascript" src="dialogbox/redips-dialog-min.js"></script>
    <script type="text/javascript" src="dialogbox/script.js"></script>
    <script type="text/javascript" src="../js/menuanagraficaospiti.js?ver=7"></script>
    <link rel="stylesheet" href="jqueryui/jquery-ui.css" type="text/css">
    <script src="jqueryui/jquery-ui.js" type="text/javascript"></script>
    <script src="jqueryui/datapicker-it.js" type="text/javascript"></script>
    <script type="text/javascript">                 

        $(document).ready(function () {
            if (window.innerHeight > 0) { $("#BarraLaterale").css("height", (window.innerHeight - 94) + "px"); } else { $("#BarraLaterale").css("height", (document.documentElement.offsetHeight - 94) + "px"); }
        });
    </script>
</head>
<body>
    <form id="form1" runat="server">
        <asp:ScriptManager ID="ScriptManager1" runat="server" AsyncPostBackTimeout="900" EnableScriptGlobalization="true">
            <Scripts>
                <asp:ScriptReference Path="~/Scripts/AjaxControlToolkit/Bundle" />
            </Scripts>
        </asp:ScriptManager>
        <asp:Label ID="Lbl_BarraSenior" runat="server" Text=""></asp:Label>
        <div style="text-align: left;">
            <table style="width: 100%;" cellpadding="0" cellspacing="0">
                <tr>
                    <td style="width: 160px; background-color: #F0F0F0;"></td>
                    <td>
                        <div class="Titolo">Ospiti - Crea Documento</div>
                        <div class="SottoTitoloOSPITE">
                            <asp:Label ID="Lbl_NomeOspite" runat="server"></asp:Label><br />
                            <br />
                        </div>
                    </td>
                    <td style="text-align: right; vertical-align: top;">
                        <div class="DivTastiOspite">
                            <asp:ImageButton ID="Btn_Modifica" runat="server" Height="38px" ImageUrl="~/images/salva.jpg" class="EffettoBottoniTondi" Width="38px" ToolTip="Modifica / Inserisci (F2)" />
                        </div>
                    </td>
                </tr>
                <tr>
                    <td style="width: 160px; background-color: #F0F0F0; vertical-align: top; text-align: center;" id="BarraLaterale">
                        <a href="Menu_Ospiti.aspx" style="border-width: 0px;">
                            <img src="images/Home.jpg" alt="Menù" class="Effetto" /></a><br />
                        <asp:ImageButton ID="Btn_Esci" runat="server" BackColor="Transparent" ImageUrl="../images/Menu_Indietro.png" class="Effetto" ToolTip="Chiudi" />

                    </td>
                    <td colspan="2" style="background-color: #FFFFFF;" valign="top">
                        <xasp:TabContainer ID="TabContainer1" runat="server" ActiveTabIndex="0" CssClass="TabSenior" Width="100%" BorderStyle="None" Style="margin-right: 39px">
                            <xasp:TabPanel runat="server" HeaderText="Prima Nota" ID="Tab_Anagrafica">
                                <HeaderTemplate>Crea Documento</HeaderTemplate>
                                <ContentTemplate>
                                    <br />
                                    <label class="LabelCampo">
                                        <asp:Label ID="Lbl_Anno" runat="server" Text="Anno :"></asp:Label></label>
                                    <asp:TextBox ID="Txt_Anno" onkeypress="return soloNumeri(event);" runat="server" Width="64px"></asp:TextBox><br />
                                    <br />
                                    <label class="LabelCampo">
                                        <asp:Label ID="Lbl_Mese" runat="server" Text="Mese :"></asp:Label></label>
                                    <asp:DropDownList ID="Dd_Mese" runat="server" Width="128px">
                                        <asp:ListItem Value="1">Gennaio</asp:ListItem>
                                        <asp:ListItem Value="2">Febbraio</asp:ListItem>
                                        <asp:ListItem Value="3">Marzo</asp:ListItem>
                                        <asp:ListItem Value="4">Aprile</asp:ListItem>
                                        <asp:ListItem Value="5">Maggio</asp:ListItem>
                                        <asp:ListItem Value="6">Giugno</asp:ListItem>
                                        <asp:ListItem Value="7">Luglio</asp:ListItem>
                                        <asp:ListItem Value="8">Agosto</asp:ListItem>
                                        <asp:ListItem Value="9">Settembre</asp:ListItem>
                                        <asp:ListItem Value="10">Ottobre</asp:ListItem>
                                        <asp:ListItem Value="11">Novembre</asp:ListItem>
                                        <asp:ListItem Value="12">Dicembre</asp:ListItem>
                                    </asp:DropDownList>
                                    <asp:RadioButton ID="RB_FatturaRetta" runat="server" Text="Retta" GroupName="TipoDoc" />
                                    <asp:RadioButton ID="RB_FatturaFuoriRetta" runat="server" Text="Fuori Retta" GroupName="TipoDoc" />
                                    <asp:Button ID="btn_Calcolo" runat="server" Text="Calcola Mese" />
                                    <asp:CheckBox ID="Chk_Conferma" runat="server" Text="" Visible="false" />
                                    <asp:Label ID="lbl_Errore" runat="server" Text=""></asp:Label>
                                    <br />

                                    <br />
                                    <label class="LabelCampo">Data Documento :</label><asp:TextBox ID="Txt_DataRegistrazione" runat="server" Width="90px"></asp:TextBox>
                                    <br />
                                    <br />
                                    <label class="LabelCampo">Data Incasso :</label><asp:TextBox ID="Txt_DataIncasso" runat="server" Width="90px"></asp:TextBox>
                                    <br />
                                    <br />
                                    <label class="LabelCampo">Causale Incasso :</label><asp:DropDownList ID="Dd_CausaleContabile" runat="server" Width="408px"></asp:DropDownList><br />
                                    <br />
                                    <label class="LabelCampo">Modalità Pagamento :</label>
                                    <asp:DropDownList ID="DD_ModalitaPagamento" runat="server" Width="248px">
                                    </asp:DropDownList><br />
                                    <br />
                                    <label class="LabelCampo">Descrizione:</label><asp:TextBox ID="Txt_Descrizione" runat="server" Height="72px" TextMode="MultiLine" Width="680px"></asp:TextBox><br />
                                    <br />

                                    <label class="LabelCampo">Descrizione :</label>
                                    <asp:TextBox ID="Txt_DescrizioneQD" runat="server" Width="680px"></asp:TextBox><br />
                                    <br />

                                    <table>
                                        <td>
                                            <label class="LabelCampo">Importo Retta Presenza :</label>
                                            <asp:TextBox ID="Txt_ImportoRettaPresenza" Style="text-align: right;" onkeypress="ForceNumericInput(this, true, true)" runat="server" Width="104px"></asp:TextBox>
                                        </td>
                                        <td>
                                        <td>
                                            <label class="LabelCampo">Importo Retta Presenza 2° Importo :</label>
                                            <asp:TextBox ID="Txt_ImportoRettaPresenza2" Style="text-align: right;" onkeypress="ForceNumericInput(this, true, true)" runat="server" Width="104px"></asp:TextBox>
                                        </td>
                                        <td>
                                            <label class="LabelCampo">
                                                <asp:Label ID="Lbl_ImpFatturato" runat="server" Text=" Fatturato :"></asp:Label></label>
                                            <asp:TextBox ID="Txt_TotaleFatturato" Enabled="false" Style="text-align: right;" onkeypress="ForceNumericInput(this, true, true)" runat="server" Width="104px"></asp:TextBox>
                                        </td>
                                        <td>
                                            <label class="LabelCampo">
                                                <asp:Label ID="Lbl_ImpCalcolato" runat="server" Text=" Calcolato :"></asp:Label></label>
                                            <asp:TextBox ID="Txt_ImportoRettaPresenzaCalcolato" Enabled="false" Style="text-align: right;" onkeypress="ForceNumericInput(this, true, true)" runat="server" Width="104px"></asp:TextBox>
                                        </td>
                                    </table>

                                    <table>
                                        <td>
                                            <label class="LabelCampo">Giorni Pres.:</label>
                                            <asp:TextBox ID="Txt_GiorniPresenza" Style="text-align: right;" onkeypress="ForceNumericInput(this, true, true)" runat="server" Width="104px"></asp:TextBox>
                                        </td>
                                        <td>
                                            <label class="LabelCampo">
                                                <asp:Label ID="lbl_fatturato" runat="server" Text=" Fatturato :"></asp:Label></label>
                                            <asp:TextBox ID="Txt_GiorniFattura" Enabled="false" Style="text-align: right;" onkeypress="ForceNumericInput(this, true, true)" runat="server" Width="104px"></asp:TextBox>
                                        </td>
                                        <td>
                                            <label class="LabelCampo">
                                                <asp:Label ID="Lbl_Calcolato" runat="server" Text=" Calcolato :"></asp:Label></label>
                                            <asp:TextBox ID="Txt_GiorniCalcolato" Enabled="false" Style="text-align: right;" onkeypress="ForceNumericInput(this, true, true)" runat="server" Width="104px"></asp:TextBox>
                                        </td>
                                    </table>



                                    <br />
                                    <br />
                                    <label class="LabelCampo">
                                        <asp:Label ID="Lbl_ImportoRetteAssenza" runat="server" Text="Importo Retta Assenza :"></asp:Label></label>
                                    <asp:TextBox ID="Txt_ImportoRettaAssenza" Style="text-align: right;" onkeypress="ForceNumericInput(this, true, true)" runat="server" Width="104px"></asp:TextBox>
                                    <br />
                                    <br />
                                    <label class="LabelCampo">
                                        <asp:Label ID="Lbl_GiorniAss" runat="server" Text="Giorni Assenz.:"></asp:Label></label>
                                    <asp:TextBox ID="Txt_GiorniAssenza" Style="text-align: right;" onkeypress="ForceNumericInput(this, true, true)" runat="server" Width="104px"></asp:TextBox>
                                    <br />
                                    <br />

                                    <label class="LabelCampo">Importo Extra :</label>
                                    <asp:TextBox ID="Txt_ExtraFissi" Enabled="false" Style="text-align: right;" onkeypress="ForceNumericInput(this, true, true)" runat="server" Width="104px"></asp:TextBox>
                                    <asp:CheckBox ID="Chk_ExtraFissi" runat="server" />
                                    <br />
                                    <br />
                                    <label class="LabelCampo">&nbsp;</label><asp:CheckBox ID="Chk_SoloMeseCorrente" AutoPostBack="true" runat="server" Text="Solo Mese Calcolato" /><br />
                                    <br />
                                    <label class="LabelCampo">Importo Addebiti :</label>
                                    <asp:TextBox ID="Txt_ImportoAddibiti" Enabled="false" Style="text-align: right;" onkeypress="ForceNumericInput(this, true, true)" runat="server" Width="104px"></asp:TextBox>
                                    <asp:CheckBox ID="Chk_ImportoAddibiti" runat="server" />
                                    <br />

                                    <br />
                                    <label class="LabelCampo">Importo Accrediti :</label>
                                    <asp:TextBox ID="Txt_ImportoAccrediti" Enabled="false" Style="text-align: right;" onkeypress="ForceNumericInput(this, true, true)" runat="server" Width="104px"></asp:TextBox>
                                    <asp:CheckBox ID="Chk_ImportoAccrediti" runat="server" />

                                    <br />
                                    <br />

                                    <label class="LabelCampo">&nbsp;</label>
                                    <asp:Button ID="btn_calcola" runat="server" Text="Aggiorna" /><br />
                                    <br />

                                    <label class="LabelCampo">Importo IVA :</label>
                                    <asp:TextBox ID="Txt_ImportoIVA" Enabled="false" Style="text-align: right;" onkeypress="ForceNumericInput(this, true, true)" runat="server" Width="104px"></asp:TextBox>
                                    <br />

                                    <label class="LabelCampo">Bollo :</label>
                                    <asp:TextBox ID="Txt_Bollo" Enabled="false" Style="text-align: right;" onkeypress="ForceNumericInput(this, true, true)" runat="server" Width="104px"></asp:TextBox>
                                    <br />

                                    <label class="LabelCampo">Totale :</label>
                                    <asp:TextBox ID="Txt_Totale" Enabled="false" Style="text-align: right;" onkeypress="ForceNumericInput(this, true, true)" runat="server" Width="104px"></asp:TextBox>
                                    <br />


                                </ContentTemplate>
                            </xasp:TabPanel>
                        </xasp:TabContainer>
                    </td>
                </tr>
            </table>
        </div>
    </form>
</body>
</html>

