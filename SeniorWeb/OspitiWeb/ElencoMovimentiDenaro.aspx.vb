﻿
Partial Class OspitiWeb_ElencoMovimentiDenaro
    Inherits System.Web.UI.Page

    Dim MyTable As New System.Data.DataTable("tabella")
    Dim MyDataSet As New System.Data.DataSet()


    Protected Sub ImageButton1_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles ImageButton1.Click
        If Val(Txt_Anno.Text) = 0 Then
            ClientScript.RegisterClientScriptBlock(Me.GetType(), "Errore", "VisualizzaErrore('Digitare Anno');", True)
            Exit Sub
        End If
        Dim kX As New Cls_Denaro


        kX.loaddati(Session("DC_OSPITIACCESSORI"), Session("CODICEOSPITE"), Txt_Anno.Text, MyTable)
        ViewState("Appoggio") = MyTable

        Grd_Denaro.AutoGenerateColumns = False
        Grd_Denaro.DataSource = MyTable
        Grd_Denaro.DataBind()

        Call ColoreGriglia()
    End Sub

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Page.IsPostBack = True Then
            Exit Sub
        End If



        If Val(Request.Item("CodiceOspite")) > 0 And Request.Item("CentroServizio") <> "" Then

            Session("CODICESERVIZIO") = Request.Item("CentroServizio")
            Session("CODICEOSPITE") = Val(Request.Item("CodiceOspite"))
        End If




        Dim K1 As New Cls_SqlString

        Dim Barra As New Cls_BarraSenior

        If Not IsNothing(Session("RicercaAnagraficaSQLString")) Then
            K1 = Session("RicercaAnagraficaSQLString")
        End If

        Lbl_BarraSenior.Text = Barra.CodiceBarra(Application("SENIOR"), Session("UTENTE"), Page, K1)
        Dim x As New ClsOspite

        x.CodiceOspite = Session("CODICEOSPITE")
        x.Leggi(Session("DC_OSPITE"), x.CodiceOspite)

        Dim cs As New Cls_CentroServizio

        cs.Leggi(Session("DC_OSPITE"), Session("CODICESERVIZIO"))


        Lbl_NomeOspite.Text = x.Nome & " -  " & x.DataNascita & " - " & cs.DESCRIZIONE & "<i style=""font-size:small;"">(" & x.CodiceOspite & ")</i>"

        Dim kX As New Cls_Denaro


        Txt_Anno.Text = Year(Now)

        kX.loaddati(Session("DC_OSPITIACCESSORI"), Session("CODICEOSPITE"), Year(Now), MyTable)
        ViewState("Appoggio") = MyTable

        Grd_Denaro.AutoGenerateColumns = False
        Grd_Denaro.DataSource = MyTable
        Grd_Denaro.DataBind()

        Dim K As New Cls_Denaro

        K.CodiceOspite = Session("CODICEOSPITE")
        K.AnnoMovimento = Val(Txt_Anno.Text)
        K.NumeroMovimento = 0

        Lbl_SaldoAttuale.Text = "<div style=""width:100%; background-color:#CCCCCC;""><div style=""float:Left; background-color:#CCCCCC; width:50%;""> Saldo Attuale : " & Format(K.Saldo(Session("DC_OSPITIACCESSORI")), "#,##0.00") & "</div><div style=""float:right; background-color:#CCCCCC; width:50%;"">Saldo Inizio Anno : " & Format(K.SaldoInizioAnno(Session("DC_OSPITIACCESSORI")), "#,##0.00") & "</div></div>"

        Lbl_SaldoInizioAnno.Text = ""

        Call ColoreGriglia()
    End Sub

    Protected Sub Grd_Denaro_PageIndexChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewPageEventArgs) Handles Grd_Denaro.PageIndexChanging
        MyTable = ViewState("Appoggio")
        Grd_Denaro.PageIndex = e.NewPageIndex
        Grd_Denaro.DataSource = MyTable
        Grd_Denaro.DataBind()

        Call ColoreGriglia()
    End Sub

    Private Sub ColoreGriglia()

        Dim k As Integer
        Dim i As Integer

        For i = 0 To Grd_Denaro.Rows.Count - 1
            If Grd_Denaro.Rows(i).Cells(2).Text = "Versamento" Then
                For k = 0 To 7
                    Grd_Denaro.Rows(i).Cells(k).ForeColor = Drawing.Color.Red
                Next
            End If
            If Grd_Denaro.Rows(i).Cells(2).Text = "Prelievo" Then

                For k = 0 To 7
                    Grd_Denaro.Rows(i).Cells(k).ForeColor = Drawing.Color.Green
                Next k
            End If
        Next

    End Sub

    Protected Sub Grd_Denaro_RowCommand(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewCommandEventArgs) Handles Grd_Denaro.RowCommand

        If e.CommandName = "Stampa" Then
            Dim d As Integer


            MyTable = ViewState("Appoggio")

            d = Val(e.CommandArgument)


            Dim Codice As String

            Codice = MyTable.Rows(d).Item(6).ToString

            Dim k As New Cls_Denaro


            k.AnnoMovimento = Txt_Anno.Text
            k.NumeroMovimento = Codice            
            k.CodiceOspite = Session("CODICEOSPITE")
            k.Leggi(Session("DC_OSPITIACCESSORI"))

            Dim Stampa As New StampeOspiti

            Dim WrRs As System.Data.DataRow = Stampa.Tables("RicevutaDenaro").NewRow

            Dim Ospite As New ClsOspite

            Ospite.CodiceOspite = Session("CODICEOSPITE")
            Ospite.Leggi(Session("DC_OSPITE"), Ospite.CodiceOspite)

            WrRs.Item("CognomeNome") = Ospite.Nome
            WrRs.Item("CodiceOspite") = Ospite.CodiceOspite

            WrRs.Item("DataRegistrazione") = Format(k.DataRegistrazione, "dd/MM/yyyy")
            WrRs.Item("Descrizione") = k.Descrizione
            WrRs.Item("Importo") = Format(k.Importo, "#,##0.00")
            If k.TipoOperazione = "V" Then
                WrRs.Item("Tipo") = "Versamento"
            End If
            If k.TipoOperazione = "P" Then
                WrRs.Item("Tipo") = "Prelievo"
            End If

            WrRs.Item("Saldo") = Format(k.Saldo(Session("DC_OSPITIACCESSORI")), "#,##0.00")


            Stampa.Tables("RicevutaDenaro").Rows.Add(WrRs)

            Session("stampa") = Stampa '"{Mastrino.PRINTERKEY} = " & Chr(34) & ViewState("PRINTERKEY") & Chr(34)

            ScriptManager.RegisterClientScriptBlock(Me, Me.GetType(), "Stampa", "openPopUp('Stampa_ReportXSD.aspx?REPORT=RICEVUTADENARO&PRINTERKEY=OFF','Stampe','width=800,height=600');", True)

        End If
        If e.CommandName = "Seleziona" Then


            Dim d As Integer


            MyTable = ViewState("Appoggio")

            d = Val(e.CommandArgument)


            Dim Codice As String

            Codice = MyTable.Rows(d).Item(6).ToString



            If Val(Request.Item("CodiceOspite")) > 0 And Request.Item("CentroServizio") <> "" Then

                Session("CODICESERVIZIO") = Request.Item("CentroServizio")
                Session("CODICEOSPITE") = Val(Request.Item("CodiceOspite"))
            End If


            Response.Redirect("GestioneDenaro.aspx?CODICE=" & Codice & "&Anno=" & Txt_Anno.Text & "&CentroServizio=" & Session("CODICESERVIZIO") & "&CodiceOspite=" & Session("CODICEOSPITE"))
        End If
    End Sub

    Protected Sub Grd_Denaro_RowDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs) Handles Grd_Denaro.RowDataBound
        If e.Row.RowType = DataControlRowType.DataRow Then
            e.Row.Cells(3).HorizontalAlign = HorizontalAlign.Right
        End If
    End Sub

    Protected Sub Grd_Denaro_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles Grd_Denaro.SelectedIndexChanged

    End Sub

    Protected Sub Btn_Nuovo_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles Btn_Nuovo.Click

        If Val(Request.Item("CodiceOspite")) > 0 And Request.Item("CentroServizio") <> "" Then

            Session("CODICESERVIZIO") = Request.Item("CentroServizio")
            Session("CODICEOSPITE") = Val(Request.Item("CodiceOspite"))
        End If


        Response.Redirect("GestioneDenaro.aspx?CODICE=0&Anno=" & Txt_Anno.Text & "&CentroServizio=" & Session("CODICESERVIZIO") & "&CodiceOspite=" & Session("CODICEOSPITE"))
    End Sub

    Protected Sub Btn_Esci_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles Btn_Esci.Click
        If Request.Item("PAGINA") = "" Then
            Response.Redirect("RicercaAnagrafica.aspx?TIPO=GESTIONEDENARO")
        Else
            Response.Redirect("RicercaAnagrafica.aspx?TIPO=GESTIONEDENARO&CHIAMATA=RITORNO&PAGINA=" & Request.Item("PAGINA"))
        End If
    End Sub

    Protected Sub Grd_Denaro_Sorting(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewSortEventArgs) Handles Grd_Denaro.Sorting
        MyTable = ViewState("Appoggio")

        MyTable.DefaultView.Sort = e.SortExpression
        Grd_Denaro.AutoGenerateColumns = False
        Grd_Denaro.DataSource = MyTable

        Grd_Denaro.DataBind()
    End Sub

End Class
