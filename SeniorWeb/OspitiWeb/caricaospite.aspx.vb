﻿Imports Microsoft.VisualBasic
Imports System.Data.OleDb
Imports System.Web.Hosting
Partial Class OspitiWeb_caricaospite
    Inherits System.Web.UI.Page

    Protected Sub form1_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles form1.Load

        Dim OSPITE As String
        Dim PAGINA As String
        Dim PARAMETRI As String

        OSPITE = Request.Item("OSPITE")

        If IsNothing(OSPITE) Then
            Exit Sub
        End If

        PAGINA = Request.Item("PAGINA")

        If IsNothing(PAGINA) Then
            Exit Sub
        End If


        PARAMETRI = Request.Item("PARAMETRI")
        If IsNothing(PAGINA) Then            
            PARAMETRI = ""
        End If

        Try
            If Session("ABILITAZIONI").IndexOf("<ANAGRAFICA>") < 1 Then
                Response.Redirect("Menu_Ospiti.aspx")
                Exit Sub
            End If

        Catch ex As Exception
            Response.Redirect("Menu_Ospiti.aspx")
            Exit Sub
        End Try
        

        If OSPITE.IndexOf(" ") = -1 Then
            
            Response.Redirect("RicercaAnagrafica.aspx?RICERCACOGNOME=" & OSPITE)
            Exit Sub
        End If
        Try
            Session("CODICESERVIZIO") = Mid(OSPITE, 1, OSPITE.IndexOf(" "))
            Session("CODICEOSPITE") = Val(Mid(OSPITE, OSPITE.IndexOf(" ") + 1, OSPITE.IndexOf(" ", OSPITE.IndexOf(" ") + 1)))
        Catch ex As Exception
            Dim k As New ClsOspite

            k.Nome = OSPITE
            k.LeggiNome(Session("DC_OSPITE"))

            Dim Mov As New Cls_Movimenti

            Mov.UltimaData(Session("DC_OSPITE"), k.CodiceOspite)
            Session("CODICEOSPITE") = k.CodiceOspite
            Session("CODICESERVIZIO") = Mov.CENTROSERVIZIO
        End Try
        
        If OSPITE.IndexOf("Parente di") > 0 Then
            Dim Nome As String

            Nome = Mid(OSPITE, OSPITE.IndexOf(" ", OSPITE.IndexOf(" ") + 1) + 1, OSPITE.IndexOf("Parente di") - (OSPITE.IndexOf(" ", OSPITE.IndexOf(" ") + 1)))

            Dim cn As OleDbConnection



            cn = New Data.OleDb.OleDbConnection(Session("DC_OSPITE"))

            cn.Open()
            Dim cmd As New OleDbCommand()
            cmd.CommandText = ("select * from AnagraficaComune where CodiceOspite = ? And " & _
                               "Nome= ? And TIPOLOGIA = 'P'")
            cmd.Parameters.AddWithValue("@CodiceOspite", Session("CODICEOSPITE"))
            cmd.Parameters.AddWithValue("@Nome", Trim(Nome))
            cmd.Connection = cn


            Dim myPOSTreader As OleDbDataReader = cmd.ExecuteReader()
            If myPOSTreader.Read Then

                Session("CODICEPARENTE") = Val(campodb(myPOSTreader.Item("CodiceParente")))

            End If
            myPOSTreader.Close()
            cn.Close()
        End If

        
        If PARAMETRI = "TIPO=MODIFICARETTA" Or PARAMETRI = "TIPO=MODIFICACSERV" Or PAGINA.ToUpper = "ospitiweb_Anagrafica_aspx".ToUpper Or PAGINA.ToUpper = "ospitiweb_DatiAnagrafici_aspx".ToUpper Or PAGINA.ToUpper = "ospitiweb_RettaTotale_aspx".ToUpper Or _
           PAGINA.ToUpper = "ospitiweb_DatiSanitari_aspx".ToUpper Or PAGINA.ToUpper = "ospitiweb_Residenza_aspx".ToUpper Or _
           PAGINA.ToUpper = "ospitiweb_ExtraFissi_aspx".ToUpper Or PAGINA.ToUpper = "ospitiweb_StatoAuto_aspx".ToUpper Or _
           PAGINA.ToUpper = "ospitiweb_ModalitaCalcolo_aspx".ToUpper Or PAGINA.ToUpper = "ospitiweb_ImportoOspite_aspx".ToUpper Or _
           PAGINA.ToUpper = "ospitiweb_ImportoComune_aspx".ToUpper Or PAGINA.ToUpper = "ospitiweb_ImportoJolly_aspx".ToUpper Or _
           PAGINA.ToUpper = "ospitiweb_Impegnativa_aspx".ToUpper Or PAGINA.ToUpper = "ospitiweb_SelezionaParente_aspx".ToUpper Or _
           PAGINA.ToUpper = "ospitiweb_quotemensili_aspx".ToUpper Or PAGINA.ToUpper = "ospitiweb_movimenti_aspx".ToUpper Or _
           PAGINA.ToUpper = "ospitiweb_MesiDiurno_aspx".ToUpper Or PAGINA.ToUpper = "ospitiweb_Diurno_aspx".ToUpper Or PAGINA.ToUpper = "ospitiweb_Elenco_MovimentiDomiciliare" Or _
           PAGINA.ToUpper = "ospitiweb_GrigliaAddebitiAccrediti_aspx".ToUpper Or PAGINA.ToUpper = "ospitiweb_ElencoMovimentiDenaro_aspx".ToUpper Or _
           PAGINA.ToUpper = "ospitiweb_Calcolo_aspx".ToUpper Or PAGINA.ToUpper = "ospitiweb_ricalcolo_aspx".ToUpper Or PAGINA.ToUpper = "ospitiweb_DocumentazioneLocal_aspx".ToUpper Or _
           PAGINA.ToUpper = "ospitiweb_FatturaNC_aspx".ToUpper Or PAGINA.ToUpper = "ospitiweb_IncassiAnticipi_aspx".ToUpper Or PAGINA.ToUpper = "ospitiweb_ElencoMovimentiStanze_aspx".ToUpper Or PAGINA.ToUpper = "ospitiweb_EstrattoContoDenaro_aspx".ToUpper Then
            If PARAMETRI = "TIPO=MODIFICACSERV" Then
                PAGINA = "ospitiweb_ModificaCentroServizio_aspx"
            End If
            If PARAMETRI = "TIPO=MODIFICARETTA" Then
                PAGINA = "ospitiweb_ModificaRetta_aspx"
            End If

            PAGINA = PAGINA.ToUpper.Replace("ospitiweb_".ToUpper, "")
            PAGINA = PAGINA.Replace("_ASPX", ".ASPX")
            If OSPITE.IndexOf("Parente di") > 0 Then
                Response.Redirect("Parenti.aspx")
            Else
                Response.Redirect(PAGINA)
            End If
        Else
            If OSPITE.IndexOf("Parente di") > 0 Then

                Response.Redirect("Parenti.aspx")
            Else
                Response.Redirect("Anagrafica.aspx")
            End If


        End If

    End Sub

    Function campodb(ByVal oggetto As Object) As String
        If IsDBNull(oggetto) Then
            Return ""
        Else
            Return oggetto
        End If
    End Function
End Class
