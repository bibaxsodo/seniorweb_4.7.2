﻿Imports System
Imports System.Web
Imports Microsoft.VisualBasic
Imports System.Data.OleDb
Imports System.Web.Hosting
Imports System.Data.SqlTypes
Imports System.Net
Imports System.Collections.Generic
Imports System.IO
Imports System.Security.Cryptography.X509Certificates
Imports System.Net.Security
Imports System.Web.Script.Serialization
Imports org.jivesoftware.util
Imports Newtonsoft.Json
Imports Newtonsoft.Json.Linq
Partial Class OspitiWeb_ImportSosia
    Inherits System.Web.UI.Page

    Dim Tabella As New System.Data.DataTable("tabella")


    Function campodb(ByVal oggetto As Object) As String
        If IsDBNull(oggetto) Then
            Return ""
        Else
            Return oggetto
        End If
    End Function



    Private Function BusinessUnit(ByVal Token As String, ByVal context As HttpContext) As Long
        System.Net.ServicePointManager.ServerCertificateValidationCallback = AddressOf CertificateHandler



        Dim client As HttpWebRequest = WebRequest.Create("https://api-v0.e-personam.com/v0/whoami")

        client.Method = "GET"
        client.Headers.Add("Authorization", "Bearer " & Token)
        client.ContentType = "Content-Type: application/json"

        Dim reader As StreamReader
        Dim response As HttpWebResponse = Nothing

        response = DirectCast(client.GetResponse(), HttpWebResponse)

        reader = New StreamReader(response.GetResponseStream())


        Dim rawresp As String
        rawresp = reader.ReadToEnd()

        BusinessUnit = 0

        Dim jResults As JObject = JObject.Parse(rawresp)

        For Each jTok2 As JToken In jResults.Item("business_units").Children
            If Val(jTok2.Item("description").ToString.IndexOf(context.Session("NomeEPersonam"))) >= 0 Then
                BusinessUnit = Val(jTok2.Item("id").ToString)
                Exit For
            End If
        Next



    End Function



    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If IsNothing(Session("DC_OSPITE")) Then
            Response.Redirect("..\Login.aspx")
            Exit Sub
        End If


        If Page.IsPostBack = False Then


            Dim K1 As New Cls_SqlString

            Dim Barra As New Cls_BarraSenior

            If Not IsNothing(Session("RicercaAnagraficaSQLString")) Then
                K1 = Session("RicercaAnagraficaSQLString")
            End If

            Lbl_BarraSenior.Text = Barra.CodiceBarra(Application("SENIOR"), Session("UTENTE"), Page, K1)

            Dim Param As New Cls_Parametri

            Param.LeggiParametri(Context.Session("DC_OSPITE"))


            Dim kVilla As New Cls_TabelleDescrittiveOspitiAccessori

            kVilla.UpDateDropBox(Session("DC_OSPITIACCESSORI"), "VIL", DD_Struttura)
            If DD_Struttura.Items.Count = 1 Then
                Call AggiornaCServ()
            End If

            Dim DataFatt As String

            If Param.MeseFatturazione > 9 Then
                DataFatt = Param.AnnoFatturazione & "-" & Param.MeseFatturazione & "-01"
            Else
                DataFatt = Param.AnnoFatturazione & "-0" & Param.MeseFatturazione & "-01"
            End If


        End If

    End Sub



    Protected Sub Btn_Esci_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles Btn_Esci.Click
        Response.Redirect("Menu_Strumenti.aspx")
    End Sub




    Private Function LoginPersonam(ByVal context As HttpContext) As String
        Try

            'Dim request As WebRequest
            '-staging
            'request = HttpWebRequest.Create("https://api-v0.e-personam.com/v0/oauth/token")
            Dim request As New NameValueCollection

            Dim BlowFish As New Blowfish("advenias2014")



            'request.Method = "POST"
            request.Add("client_id", "98217ca6670c660e22f42d58e231d4e56c84fb34e216b0f66f1f30284fd3ec9d")
            request.Add("client_secret", "5570a684f69ca74d75d16bba669c156ec092eccb4111d0974adec3edb2fa4d6f")
            request.Add("grant_type", "authorization_code")
            'request.Add("username", context.Session("UTENTE"))
            'request.Add("username", context.Session("UTENTE").ToString.Replace("<1>", "").Replace("<2>", "").Replace("<3>", ""))
            'request.Add("password", "advenias2014")

            'guarda = BlowFish.encryptString("advenias2014")
            Dim guarda As String

            If Trim(context.Session("EPersonamPSWCRYPT")) = "" Then
                request.Add("username", context.Session("UTENTE").ToString.Replace("<1>", "").Replace("<2>", "").Replace("<3>", "").Replace("<4>", "").Replace("<5>", "").Replace("<6>", ""))

                guarda = context.Session("ChiaveCr")
            Else
                request.Add("username", context.Session("EPersonamUser"))

                guarda = context.Session("EPersonamPSWCRYPT")
            End If

            request.Add("code", guarda)
            REM request.Add("password", guarda)
            System.Net.ServicePointManager.ServerCertificateValidationCallback = AddressOf CertificateHandler


            Dim client As New WebClient()

            Dim result As Object = client.UploadValues("https://api-v0.e-personam.com/v0/oauth/token", "POST", request)
            REM Dim result As Object = client.UploadValues("http://api-v1.e-personam.com/v0/oauth/token", "POST", request)


            Dim stringa As String = Encoding.Default.GetString(result)


            Dim serializer As JavaScriptSerializer


            serializer = New JavaScriptSerializer()


            Dim s As Autentificazione = serializer.Deserialize(Of Autentificazione)(stringa)


            Return s.access_token
        Catch ex As Exception
            Return ""
        End Try
    End Function
    Public Class Autentificazione
        Public access_token As String
        Public expires_in As String
        Public scope As String
        Public token_type As String
        Public refresh_token As String
    End Class



    Private Shared Function CertificateHandler(ByVal sender As Object, ByVal certificate As X509Certificate, ByVal chain As X509Chain, ByVal SSLerror As SslPolicyErrors) As Boolean
        Return True
    End Function

    Protected Sub Btn_Movimenti_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles Btn_Movimenti.Click
        Dim Token As String


        Token = LoginPersonam(Context)

        If Token = "" Then
            ClientScript.RegisterClientScriptBlock(Me.GetType(), "Errore", "VisualizzaErrore('Errore nel dialogo con Epersonam');", True)
            Exit Sub
        End If



        System.Net.ServicePointManager.ServerCertificateValidationCallback = AddressOf CertificateHandler


        Dim Data() As Byte

        Dim request As New NameValueCollection

        Dim Param As New Cls_Parametri

        Param.LeggiParametri(Context.Session("DC_OSPITE"))


        Dim DataFatt As String

        If Param.MeseFatturazione > 9 Then
            DataFatt = Param.AnnoFatturazione & "-" & Param.MeseFatturazione & "-01"
        Else
            DataFatt = Param.AnnoFatturazione & "-0" & Param.MeseFatturazione & "-01"
        End If


        Dim cn As OleDbConnection
        Dim Trovato As Boolean = False

        cn = New Data.OleDb.OleDbConnection(Session("DC_OSPITE"))

        cn.Open()

        Dim rawresp As String

        System.Net.ServicePointManager.ServerCertificateValidationCallback = AddressOf CertificateHandler


        Dim MyCserv As New Cls_CentroServizio

        MyCserv.CENTROSERVIZIO = Cmb_CServ.SelectedValue
        MyCserv.Leggi(Session("DC_OSPITE"), MyCserv.CENTROSERVIZIO)


        '& BusinessUnit(Token, Context) & 
        Dim client As HttpWebRequest = WebRequest.Create("https://api-v0.e-personam.com/v0/business_units/" & MyCserv.EPersonam & "/sosia/updated/" & DataFatt)

        client.Method = "GET"
        client.Headers.Add("Authorization", "Bearer " & Token)
        client.ContentType = "Content-Type: application/json"

        Dim reader As StreamReader
        Dim response As HttpWebResponse = Nothing

        response = DirectCast(client.GetResponse(), HttpWebResponse)

        reader = New StreamReader(response.GetResponseStream())

        rawresp = reader.ReadToEnd()

        Tabella.Clear()
        Tabella.Columns.Clear()

        Tabella.Columns.Add("Cognome", GetType(String))
        Tabella.Columns.Add("Nome", GetType(String))
        Tabella.Columns.Add("CodiceFiscale", GetType(String))
        Tabella.Columns.Add("Data", GetType(String))
        Tabella.Columns.Add("Classe", GetType(String))
        Tabella.Columns.Add("Tipologia Ospite", GetType(String))
        Tabella.Columns.Add("Asl Prov", GetType(String))

        Tabella.Columns.Add("Stato", GetType(String))
        Tabella.Columns.Add("CodiceOspite", GetType(String))



        REM Dim jResults As JArray = JArray.Parse(rawresp)

        Dim jResults As JObject = JObject.Parse(rawresp)



        For Each jTok1 As JToken In jResults.Item("last_sosia2s").Children

            Dim CodiceFiscale As String = ""
            Dim DataCambio As String = ""
            Dim asl_prov As String = ""
            Dim classe As String = ""
            Dim tipologia_ospite As String = ""

            Try
                CodiceFiscale = jTok1.Item("cf").ToString()
            Catch ex As Exception

            End Try


            For Each jTok2 As JToken In jTok1.Item("sosia2s").Children
                Try
                    DataCambio = jTok2.Item("from").ToString()
                Catch ex As Exception

                End Try
                Try
                    tipologia_ospite = jTok2.Item("tipologia_ospite").ToString()
                Catch ex As Exception

                End Try
                Try
                    classe = jTok2.Item("classe").ToString()
                Catch ex As Exception

                End Try

                Try
                    asl_prov = jTok2.Item("asl_prov").ToString()
                Catch ex As Exception

                End Try



                Dim Ospite As New ClsOspite


                Ospite.LeggiPerCodiceFiscale(Session("DC_OSPITE"), CodiceFiscale)

                Dim UltimoStato As New Cls_StatoAuto
                Dim Stato As String = ""

                UltimoStato.CODICEOSPITE = Ospite.CodiceOspite
                UltimoStato.CENTROSERVIZIO = Cmb_CServ.SelectedValue
                UltimoStato.UltimaData(Session("DC_OSPITE"), Ospite.CodiceOspite, Cmb_CServ.SelectedValue)

                If UltimoStato.TipoRetta = "01" And classe = "1" Then
                    Stato = "OK"
                End If
                If UltimoStato.TipoRetta = "02" And classe = "2" Then
                    Stato = "OK"
                End If
                If UltimoStato.TipoRetta = "03" And classe = "3" Then
                    Stato = "OK"
                End If
                If UltimoStato.TipoRetta = "04" And classe = "4" Then
                    Stato = "OK"
                End If
                If UltimoStato.TipoRetta = "05" And classe = "5" Then
                    Stato = "OK"
                End If
                If UltimoStato.TipoRetta = "06" And classe = "6" Then
                    Stato = "OK"
                End If
                If UltimoStato.TipoRetta = "07" And classe = "7" Then
                    Stato = "OK"
                End If
                If UltimoStato.TipoRetta = "08" And classe = "8" Then
                    Stato = "OK"
                End If
                If UltimoStato.TipoRetta = "10" And tipologia_ospite.IndexOf("(3) Alzheimer") >= 0 Then
                    Stato = "OK"
                End If


                Dim myriga As System.Data.DataRow = Tabella.NewRow()
                myriga(0) = Ospite.CognomeOspite
                myriga(1) = Ospite.NomeOspite
                myriga(2) = Ospite.CODICEFISCALE
                myriga(3) = DataCambio
                myriga(4) = classe

                myriga(5) = ""
                Try

                    Dim jTipoOspite As JObject = JObject.Parse(tipologia_ospite)

                    myriga(5) = jTipoOspite.Item("description")

                Catch ex As Exception

                End Try
                myriga(6) = asl_prov
                myriga(7) = Stato
                myriga(8) = Ospite.CodiceOspite



                Tabella.Rows.Add(myriga)


            Next
        Next

        REM Tabella.DefaultView.Sort = "DATA Asc"
        ViewState("App_AddebitiMultiplo") = Tabella

        GridView1.AutoGenerateColumns = False

        GridView1.DataSource = Tabella
        GridView1.DataBind()
        GridView1.Visible = True
    End Sub

    Private Sub AggiornaCServ()
        Dim kCsrv As New Cls_CentroServizio

        If DD_Struttura.SelectedValue = "" Then
            kCsrv.UpDateDropBox(Session("DC_OSPITE"), Cmb_CServ)
        Else
            kCsrv.UpDateDropBoxStruttura(Session("DC_OSPITE"), Cmb_CServ, DD_Struttura.SelectedValue)
        End If
    End Sub

    Protected Sub Btn_Modifica_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles Btn_Modifica.Click
        Dim MySql As String
        Dim Cn As New OleDbConnection(Session("DC_OSPITE"))

        Cn.Open()

        MySql = "DELETE FROM TempSosia"
        Dim cmddel As New OleDbCommand()
        cmddel.CommandText = (MySql)
        cmddel.Connection = Cn
        cmddel.ExecuteNonQuery()

        Tabella = ViewState("App_AddebitiMultiplo")
        For Riga = 0 To GridView1.Rows.Count - 1

            Dim CheckBox As CheckBox = DirectCast(GridView1.Rows(Riga).FindControl("ChkImporta"), CheckBox)

            If CheckBox.Checked = True Then
                MySql = "INSERT INTO TempSosia (CodiceOspite,Data,Sosia,Tipologia) VALUES (?,?,?,?)"
                Dim cmdw As New OleDbCommand()
                cmdw.CommandText = (MySql)

                cmdw.Parameters.AddWithValue("@CodiceOspite", Val(Tabella.Rows(Riga).Item(8)))
                cmdw.Parameters.AddWithValue("@Data", Tabella.Rows(Riga).Item(3))
                cmdw.Parameters.AddWithValue("@Sosia", Tabella.Rows(Riga).Item(4))
                cmdw.Parameters.AddWithValue("@Tipologia", Tabella.Rows(Riga).Item(5))
                cmdw.Connection = Cn
                cmdw.ExecuteNonQuery()
            End If
        Next

        Cn.Close()


        ClientScript.RegisterClientScriptBlock(Me.GetType(), "Errore", "alert('Tab temporanea aggiornata');", True)

    End Sub

    Protected Sub Btn_Seleziona_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles Btn_Seleziona.Click
        Dim Param As New Cls_Parametri

        Param.LeggiParametri(Context.Session("DC_OSPITE"))

        For Riga = 0 To GridView1.Rows.Count - 1

            Dim CheckBox As CheckBox = DirectCast(GridView1.Rows(Riga).FindControl("ChkImporta"), CheckBox)
            
            If CheckBox.Enabled = True Then
                REM If GridView1.Rows(Riga).Cells(6).Text.IndexOf(Param.MeseFatturazione & "/" & Param.AnnoFatturazione) > 0 Then
                CheckBox.Checked = True
                REM End If
            End If
        Next
    End Sub
End Class
