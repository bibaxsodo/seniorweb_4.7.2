﻿Imports System.Data.OleDb
Imports System.IO
Imports System.Net
Imports System.Net.Security
Imports System.Security.Cryptography.X509Certificates
Imports System.Web.Script.Serialization
Imports Newtonsoft.Json.Linq
Imports org.jivesoftware.util

Public Class Import_MovimentiEPersonam
    Implements System.Web.IHttpHandler, IRequiresSessionState


    Public Sub ProcessRequest(ByVal context As HttpContext) Implements IHttpHandler.ProcessRequest
        context.Response.ContentType = "text/plain"
        context.Response.Cache.SetCacheability(HttpCacheability.NoCache)

        Dim VettoreMovimenti(100) As Movimenti
        Dim Numero As Integer = 0
        Dim LastMov As String = ""


        If context.Session("UTENTE") = "" Then
            Exit Sub
        End If


        Dim Token As String
        Dim OutPutMovimenti As String = ""



        Dim cn As OleDbConnection
        Dim Trovato As Boolean = False

        cn = New Data.OleDb.OleDbConnection(context.Session("DC_OSPITE"))

        cn.Open()

        Try


            Token = LoginPersonam(context)

            Dim Data() As Byte

            Dim request As New NameValueCollection

            Dim Param As New Cls_Parametri

            Param.LeggiParametri(context.Session("DC_OSPITE"))


            Dim DataFatt As String

            If Param.MeseFatturazione > 9 Then
                DataFatt = Param.AnnoFatturazione & "-" & Param.MeseFatturazione & "-01"
            Else
                DataFatt = Param.AnnoFatturazione & "-0" & Param.MeseFatturazione & "-01"
            End If



            System.Net.ServicePointManager.ServerCertificateValidationCallback = AddressOf CertificateHandler



            Dim client As HttpWebRequest = WebRequest.Create("https://api-v0.e-personam.com/v0/business_units/" & BusinessUnit(Token, context) & "/guests/movements/from/" & DataFatt)

            client.Method = "GET"
            client.Headers.Add("Authorization", "Bearer " & Token)
            client.ContentType = "Content-Type: application/json"

            Dim reader As StreamReader
            Dim response As HttpWebResponse = Nothing

            response = DirectCast(client.GetResponse(), HttpWebResponse)

            reader = New StreamReader(response.GetResponseStream())


            Dim rawresp As String
            rawresp = reader.ReadToEnd()


            Dim jResults As JArray = JArray.Parse(rawresp)



            For Each jTok As JToken In jResults
                rawresp = rawresp & jTok.Item("guests_movements").ToString()
                For Each jTok1 As JToken In jTok.Item("guests_movements").Children
                    Dim CodiceFiscale As String
                    Dim ward_id As Integer
                    CodiceFiscale = jTok1.Item("cf").ToString()
                    ward_id = jTok1.Item("ward_id").ToString()

                    Dim Ospite As New ClsOspite

                    Ospite.CODICEFISCALE = CodiceFiscale
                    Ospite.LeggiPerCodiceFiscale(context.Session("DC_OSPITE"), CodiceFiscale)
                    If Ospite.CodiceOspite > 0 Then
                        For Each jTok2 As JToken In jTok1.Item("movements").Children

                            Dim IdEpersonam As String

                            IdEpersonam = jTok2.Item("id").ToString


                            If LastMov = "11" And jTok2.Item("type").ToString = "8" Then

                            Else

                                LastMov = jTok2.Item("type").ToString

                                Dim cmd As New OleDbCommand()

                                If jTok2.Item("type").ToString = "11" Then
                                    cmd.CommandText = ("select * from Movimenti_EPersonam Where  CodiceOspite = " & Ospite.CodiceOspite & " And Data = ?")
                                    cmd.Parameters.AddWithValue("@Data", Mid(jTok2.Item("date").ToString, 1, 10))

                                Else
                                    cmd.CommandText = ("select * from Movimenti_EPersonam Where  IdEpersonam = " & IdEpersonam)
                                End If

                                cmd.Connection = cn

                                Dim myPOSTreader As OleDbDataReader = cmd.ExecuteReader()
                                If Not myPOSTreader.Read Then
                                    Dim Tipo As String = ""
                                    Dim DataMov As String = ""

                                    If jTok2.Item("type").ToString = "11" Then
                                        Tipo = "05"
                                    End If
                                    If jTok2.Item("type").ToString = "9" Then
                                        Tipo = "03"
                                    End If
                                    If jTok2.Item("type").ToString = "8" Then
                                        Tipo = "04"
                                    End If
                                    If jTok2.Item("type").ToString = "12" Then
                                        Tipo = "13"
                                    End If

                                    DataMov = jTok2.Item("date").ToString

                                    ' ASP CARITAS - Personalizzazione
                                    If ward_id = 1071 Or ward_id = 1072 Or ward_id = 1073 Or ward_id = 1074 Then
                                        ward_id = 1071
                                    End If
                                    If ward_id = 1168 Or ward_id = 1169 Then
                                        ward_id = 1168
                                    End If



                                    Dim Progressivo As Long
                                    Dim MySql As String
                                    Dim CS As New Cls_CentroServizio
                                    CS.DESCRIZIONE = ""
                                    CS.EPersonam = ward_id
                                    CS.LeggiEpersonam(context.Session("DC_OSPITE"), CS.EPersonam, Val(jTok2.Item("conv").ToString))
                                    Dim Vercentroservizio As New Cls_Movimenti


                                    Vercentroservizio.CENTROSERVIZIO = CS.CENTROSERVIZIO
                                    Vercentroservizio.CodiceOspite = Ospite.CodiceOspite
                                    If Vercentroservizio.CServizioUsato(context.Session("DC_OSPITE")) Or Tipo = "05" Then

                                        Dim cmdIns As New OleDbCommand()
                                        cmdIns.CommandText = ("INSERT INTO Movimenti_EPersonam  (IdEpersonam,CodiceFiscale,CentroServizio,CodiceOspite,Data,TipoMovimento,Descrizione,DataModifica) VALUES (?,?,?,?,?,?,?,?)")
                                        cmdIns.Connection = cn
                                        cmdIns.Parameters.AddWithValue("@IdEpersonam", IdEpersonam)
                                        cmdIns.Parameters.AddWithValue("@CodiceFiscale", CodiceFiscale)
                                        cmdIns.Parameters.AddWithValue("@CentroServizio", CS.CENTROSERVIZIO)
                                        cmdIns.Parameters.AddWithValue("@CodiceOspite", Ospite.CodiceOspite)
                                        cmdIns.Parameters.AddWithValue("@Data", Mid(DataMov, 1, 10))
                                        cmdIns.Parameters.AddWithValue("@TipoMovimento", "M")
                                        cmdIns.Parameters.AddWithValue("@Descrizione", "")
                                        cmdIns.Parameters.AddWithValue("@DataModifica", Now)
                                        cmdIns.ExecuteNonQuery()


                                        Dim cmd1 As New OleDbCommand()
                                        cmd1.CommandText = ("select MAX(Progressivo) from Movimenti where CentroServizio = '" & CS.CENTROSERVIZIO & "' And  CodiceOspite = " & Ospite.CodiceOspite)
                                        cmd1.Connection = cn
                                        Dim myPOSTreader1 As OleDbDataReader = cmd1.ExecuteReader()
                                        If myPOSTreader1.Read Then
                                            If Not IsDBNull(myPOSTreader1.Item(0)) Then
                                                Progressivo = myPOSTreader1.Item(0) + 1
                                            Else
                                                Progressivo = 1
                                            End If
                                        Else
                                            Progressivo = 1
                                        End If

                                        MySql = "INSERT INTO Movimenti (Utente,DataAggiornamento,CentroServizio,CodiceOspite,Data,TIPOMOV,PROGRESSIVO,CAUSALE,DESCRIZIONE,EPersonam) VALUES (?,?,?,?,?,?,?,?,?,?)"
                                        Dim cmdw As New OleDbCommand()
                                        cmdw.CommandText = (MySql)

                                        cmdw.Parameters.AddWithValue("@Utente", context.Session("UTENTE"))
                                        cmdw.Parameters.AddWithValue("@DataAggiornamento", Now)
                                        cmdw.Parameters.AddWithValue("@CentroServizio", CS.CENTROSERVIZIO)
                                        cmdw.Parameters.AddWithValue("@CodiceOspite", Ospite.CodiceOspite)
                                        cmdw.Parameters.AddWithValue("@Data", Mid(DataMov, 1, 10))
                                        cmdw.Parameters.AddWithValue("@TIPOMOV", Tipo)
                                        cmdw.Parameters.AddWithValue("@PROGRESSIVO", Progressivo)
                                        cmdw.Parameters.AddWithValue("@CAUSALE", "")
                                        cmdw.Parameters.AddWithValue("@Descrizione", jTok2.Item("description").ToString)
                                        cmdw.Parameters.AddWithValue("@EPersonam", 1)
                                        cmdw.Connection = cn
                                        cmdw.ExecuteNonQuery()





                                        VettoreMovimenti(Numero) = New Movimenti
                                        VettoreMovimenti(Numero).Esito = "I"
                                        VettoreMovimenti(Numero).CentroServizio = CS.CENTROSERVIZIO
                                        VettoreMovimenti(Numero).CodiceOspite = Ospite.CodiceOspite
                                        VettoreMovimenti(Numero).Nome = Ospite.Nome
                                        VettoreMovimenti(Numero).Tipo = Tipo
                                        VettoreMovimenti(Numero).Data = Mid(DataMov, 1, 10)



                                        Numero = Numero + 1
                                    Else
                                        VettoreMovimenti(Numero) = New Movimenti
                                        VettoreMovimenti(Numero).Esito = "E"
                                        VettoreMovimenti(Numero).CentroServizio = CS.CENTROSERVIZIO
                                        VettoreMovimenti(Numero).CodiceOspite = Ospite.CodiceOspite
                                        VettoreMovimenti(Numero).Nome = Ospite.Nome
                                        VettoreMovimenti(Numero).Tipo = Tipo
                                        VettoreMovimenti(Numero).Data = Mid(DataMov, 1, 10)

                                        Numero = Numero + 1
                                    End If

                                Else

                                    Dim CS1 As New Cls_CentroServizio
                                    CS1.DESCRIZIONE = ""
                                    CS1.EPersonam = ward_id
                                    CS1.LeggiEpersonam(context.Session("DC_OSPITE"), CS1.EPersonam, jTok2.Item("conv").ToString)
                                    Dim Tipo1 As String = ""

                                    If jTok2.Item("type").ToString = "11" Then
                                        Tipo1 = "05"
                                    End If
                                    If jTok2.Item("type").ToString = "9" Then
                                        Tipo1 = "03"
                                    End If
                                    If jTok2.Item("type").ToString = "8" Then
                                        Tipo1 = "04"
                                    End If
                                    If jTok2.Item("type").ToString = "12" Then
                                        Tipo1 = "13"
                                    End If



                                    VettoreMovimenti(Numero) = New Movimenti
                                    VettoreMovimenti(Numero).Esito = "N"
                                    VettoreMovimenti(Numero).CentroServizio = CS1.CENTROSERVIZIO
                                    VettoreMovimenti(Numero).CodiceOspite = Ospite.CodiceOspite
                                    VettoreMovimenti(Numero).Nome = Ospite.Nome
                                    VettoreMovimenti(Numero).Tipo = Tipo1
                                    VettoreMovimenti(Numero).Data = Mid(jTok2.Item("date").ToString, 1, 10)

                                    Numero = Numero + 1
                                End If
                                myPOSTreader.Close()
                            End If
                        Next
                    End If
                Next
            Next



        Catch ex As Exception

        End Try

        cn.close()
        Dim serializer As JavaScriptSerializer
        serializer = New JavaScriptSerializer()

        Dim Serializzazione As String

        Serializzazione = serializer.Serialize(VettoreMovimenti)

        context.Response.Write(Serializzazione)
    End Sub



    Private Function LoginPersonam(ByVal context As HttpContext) As String
        'Dim request As WebRequest
        '-staging
        'request = HttpWebRequest.Create("https://api-v0.e-personam.com/v0/oauth/token")
        Dim request As New NameValueCollection

        Dim BlowFish As New Blowfish("advenias2014")



        'request.Method = "POST"
        request.Add("client_id", "98217ca6670c660e22f42d58e231d4e56c84fb34e216b0f66f1f30284fd3ec9d")
        request.Add("client_secret", "5570a684f69ca74d75d16bba669c156ec092eccb4111d0974adec3edb2fa4d6f")
        request.Add("grant_type", "authorization_code")

        'request.Add("password", "advenias2014")

        'guarda = BlowFish.encryptString("advenias2014")
        Dim guarda As String

        If Trim(context.Session("EPersonamPSWCRYPT")) = "" Then
            request.Add("username", context.Session("UTENTE").ToString.Replace("<1>", "").Replace("<2>", "").Replace("<3>", "").Replace("<4>", "").Replace("<5>", "").Replace("<6>", ""))

            guarda = context.Session("ChiaveCr")
        Else
            request.Add("username", context.Session("EPersonamUser"))

            guarda = context.Session("EPersonamPSWCRYPT")
        End If


        request.Add("code", guarda)
        System.Net.ServicePointManager.ServerCertificateValidationCallback = AddressOf CertificateHandler


        Dim client As New WebClient()

        Dim result As Object = client.UploadValues("https://api-v0.e-personam.com/v0/oauth/token", "POST", request)


        Dim stringa As String = Encoding.Default.GetString(result)


        Dim serializer As JavaScriptSerializer


        serializer = New JavaScriptSerializer()




        Dim s As Autentificazione = serializer.Deserialize(Of Autentificazione)(stringa)


        Return s.access_token
    End Function


    Private Function BusinessUnit(ByVal Token As String, ByVal context As HttpContext) As Long
        System.Net.ServicePointManager.ServerCertificateValidationCallback = AddressOf CertificateHandler



        Dim client As HttpWebRequest = WebRequest.Create("https://api-v0.e-personam.com/v0/whoami")

        client.Method = "GET"
        client.Headers.Add("Authorization", "Bearer " & Token)
        client.ContentType = "Content-Type: application/json"

        Dim reader As StreamReader
        Dim response As HttpWebResponse = Nothing

        response = DirectCast(client.GetResponse(), HttpWebResponse)

        reader = New StreamReader(response.GetResponseStream())


        Dim rawresp As String
        rawresp = reader.ReadToEnd()

        BusinessUnit = 0

        Dim jResults As JObject = JObject.Parse(rawresp)

        For Each jTok2 As JToken In jResults.Item("business_units").Children
            If Val(jTok2.Item("description").ToString.IndexOf(context.Session("NomeEPersonam"))) >= 0 Then
                BusinessUnit = Val(jTok2.Item("id").ToString)
                Exit For
            End If
        Next



    End Function

    Public Class Autentificazione
        Public access_token As String
        Public expires_in As String
        Public scope As String
        Public token_type As String
        Public refresh_token As String
    End Class




    Public Class Movimenti
        Public Esito As String
        Public CentroServizio As String
        Public CodiceOspite As String
        Public Nome As String
        Public Data As String
        Public Tipo As String
    End Class

    Private Shared Function CertificateHandler(ByVal sender As Object, ByVal certificate As X509Certificate, ByVal chain As X509Chain, ByVal SSLerror As SslPolicyErrors) As Boolean
        Return True
    End Function


    Public ReadOnly Property IsReusable() As Boolean Implements IHttpHandler.IsReusable
        Get
            Return False
        End Get
    End Property

End Class


