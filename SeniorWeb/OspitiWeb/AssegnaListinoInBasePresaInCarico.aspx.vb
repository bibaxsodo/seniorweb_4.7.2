﻿Imports System
Imports System.IO
Imports System.Net
Imports System.Web
Imports Microsoft.VisualBasic
Imports System.Data.OleDb
Imports System.Web.Hosting
Imports System.Collections.Generic
Imports System.Security.Cryptography.X509Certificates
Imports System.Net.Security
Imports System.Web.Script.Serialization
Imports org.jivesoftware.util
Imports Newtonsoft.Json
Imports Newtonsoft.Json.Linq

Partial Class OspitiWeb_AssegnaListinoInBasePresaInCarico
    Inherits System.Web.UI.Page
    Dim MyTable As New System.Data.DataTable("tabella")
    Dim MyDataSet As New System.Data.DataSet()

    Public Class DatiPresaInCarico
        Public DataInizio As Date
        Public DataFine As Date
        Public Codice As String
        Public Descrizione As String
    End Class



    Function campodbd(ByVal oggetto As Object) As Date
        If IsDBNull(oggetto) Then
            Return Nothing
        Else
            Return oggetto
        End If
    End Function

    Function campodb(ByVal oggetto As Object) As String
        If IsDBNull(oggetto) Then
            Return ""
        Else
            Return oggetto
        End If
    End Function

    Private Function CompareData(ByVal value1 As Date, ByVal value2 As Date) As Boolean
        If Format(value1, "yyyyMMdd") > Format(value2, "yyyyMMdd") Then
            Return True
        Else
            Return False
        End If
    End Function

    Private Function VerificaListinoPresaInCarico() As Boolean
        Dim cn As OleDbConnection
        Dim MySql As String

        MySql = ""

        cn = New Data.OleDb.OleDbConnection(Session("DC_OSPITE"))

        cn.Open()
        Dim cmd As New OleDbCommand()
        cmd.CommandText = ("Select * from Tabella_Listino Where CodiceEpersonam > 0")    
        cmd.Connection = cn
        Dim myPOSTreader As OleDbDataReader = cmd.ExecuteReader()
        If myPOSTreader.Read Then
            VerificaListinoPresaInCarico = True
        Else
            VerificaListinoPresaInCarico = False
        End If
        myPOSTreader.Close()
        cn.Close()
    End Function

    Private Sub EliminaListinoOSpite(ByVal CentroServizio As String, ByVal CodiceOspite As Integer)
        Dim cn As OleDbConnection
        Dim MySql As String

        MySql = ""

        cn = New Data.OleDb.OleDbConnection(Session("DC_OSPITE"))

        cn.Open()
        Dim cmd As New OleDbCommand()

        cmd.Parameters.Clear()
        cmd.CommandText = ("Delete from IMPORTORETTA Where CentroServizio = '" & CentroServizio & "' And  CodiceOspite = " & CodiceOspite)
        cmd.Connection = cn
        cmd.ExecuteNonQuery()

        cmd.Parameters.Clear()
        cmd.CommandText = ("Delete from EXTRAOSPITE Where CENTROSERVIZIO = '" & CentroServizio & "' And  CODICEOSPITE = " & CodiceOspite)
        cmd.Connection = cn
        cmd.ExecuteNonQuery()


        cmd.Parameters.Clear()
        cmd.CommandText = ("Delete from MODALITA Where CentroServizio = '" & CentroServizio & "' And  CodiceOspite = " & CodiceOspite)
        cmd.Connection = cn
        cmd.ExecuteNonQuery()


        cmd.Parameters.Clear()
        cmd.CommandText = ("Delete from STATOAUTO Where CentroServizio = '" & CentroServizio & "' And  CodiceOspite = " & CodiceOspite)
        cmd.Connection = cn
        cmd.ExecuteNonQuery()

        cmd.Parameters.Clear()
        cmd.CommandText = ("Delete from IMPORTOOSPITE Where CentroServizio = '" & CentroServizio & "' And  CodiceOspite = " & CodiceOspite)
        cmd.Connection = cn
        cmd.ExecuteNonQuery()

        cmd.Parameters.Clear()
        cmd.CommandText = ("Delete from IMPORTOJOLLY Where CentroServizio = '" & CentroServizio & "' And  CodiceOspite = " & CodiceOspite)
        cmd.Connection = cn
        cmd.ExecuteNonQuery()

        cmd.Parameters.Clear()
        cmd.CommandText = ("Delete from IMPORTOCOMUNE Where CentroServizio = '" & CentroServizio & "' And  CodiceOspite = " & CodiceOspite)
        cmd.Connection = cn
        cmd.ExecuteNonQuery()

        cmd.Parameters.Clear()
        cmd.CommandText = ("Delete from IMPORTOPARENTI Where CentroServizio = '" & CentroServizio & "' And  CodiceOspite = " & CodiceOspite)
        cmd.Connection = cn
        cmd.ExecuteNonQuery()

        cmd.Parameters.Clear()
        cmd.CommandText = ("Delete from Listino Where CentroServizio = '" & CentroServizio & "' And  CodiceOspite = " & CodiceOspite)
        cmd.Connection = cn
        cmd.ExecuteNonQuery()

        cn.Close()
    End Sub

    Private Sub ELIMINALISTINI()
        Dim ConnectionString As String = Session("DC_OSPITE")
        Dim cn As OleDbConnection
        Dim MySql As String
        Dim Condizione As String

        cn = New Data.OleDb.OleDbConnection(ConnectionString)

        cn.Open()

        Condizione = "("
        If Chk_Accoglimento.Checked = True Then
            If Condizione <> "(" Then
                Condizione = Condizione & " OR "
            End If
            Condizione = Condizione & " TipoMov = '05'"
        End If
        If Chk_UscitaDefinitiva.Checked = True Then
            If Condizione <> "(" Then
                Condizione = Condizione & " OR "
            End If
            Condizione = Condizione & " TipoMov = '13'"
        End If
        Condizione = Condizione & ")"

        If DD_CServ.SelectedValue = "" Then
            If DD_Struttura.SelectedValue <> "" Then
                If Condizione <> "" Then
                    Condizione = Condizione & " And ("
                Else
                    Condizione = Condizione & " ("
                End If
                Dim Indice As Integer
                Call AggiornaCServ()
                For Indice = 0 To DD_CServ.Items.Count - 1
                    If Indice >= 1 Then
                        If Condizione <> "" Then
                            Condizione = Condizione & " Or "
                        End If
                    End If
                    Condizione = Condizione & "  MOVIMENTI.CentroServizio = '" & DD_CServ.Items(Indice).Value & "'"
                Next
                Condizione = Condizione & ") "
            End If
            MySql = "Select CentroServizio,CodiceOspite From Movimenti Where Data >= ? And Data <= ? And " & Condizione & " Group by CentroServizio,CodiceOspite"
        Else
            MySql = "Select CentroServizio,CodiceOspite From Movimenti Where Data >= ? And Data <= ? And " & Condizione & "  And CENTROSERVIZIO = '" & DD_CServ.SelectedValue & "'" & " Group by CentroServizio,CodiceOspite"
        End If


        Dim cmd As New OleDbCommand()

        cmd.CommandText = MySql
        Dim DataDal As Date = Txt_DataDal.Text
        Dim DataAl As Date = Txt_DataAl.Text

        cmd.Parameters.AddWithValue("@DataDal", DataDal)
        cmd.Parameters.AddWithValue("@DataAl", DataAl)
        cmd.Connection = cn
        Dim myPOSTreader As OleDbDataReader = cmd.ExecuteReader()
        Do While myPOSTreader.Read
            Dim ArrayPreseInCarico As New List(Of DatiPresaInCarico)

            Dim DecOsp As New ClsOspite

            DecOsp.Leggi(Session("DC_OSPITE"), Val(campodb(myPOSTreader.Item("CodiceOspite"))))

            If DecOsp.NonInUso <> "S" Then

                Dim x1 As New ClsOspite

                x1.CodiceOspite = Val(campodb(myPOSTreader.Item("CodiceOspite")))
                x1.Leggi(Session("DC_OSPITE"), x1.CodiceOspite)


                Dim cs As New Cls_CentroServizio

                cs.Leggi(Session("DC_OSPITE"), campodb(myPOSTreader.Item("CENTROSERVIZIO")))

                Dim Listino As New Cls_Listino

                Listino.CODICEOSPITE = Val(campodb(myPOSTreader.Item("CodiceOspite")))
    

                EliminaListinoOSpite(campodb(myPOSTreader.Item("CENTROSERVIZIO")), Listino.CODICEOSPITE)
            End If
        Loop
        myPOSTreader.Close()

        cn.Close()
    End Sub

    Private Sub CaricaTabella()
        Dim ConnectionString As String = Session("DC_OSPITE")
        Dim cn As OleDbConnection
        Dim MySql As String
        Dim Condizione As String

        cn = New Data.OleDb.OleDbConnection(ConnectionString)

        cn.Open()

        MyTable.Clear()
        MyTable.Columns.Clear()

        MyTable.Columns.Add("Centro Servizio", GetType(String))
        MyTable.Columns.Add("Codice Ospite", GetType(Long))
        MyTable.Columns.Add("Nome", GetType(String))
        MyTable.Columns.Add("Data Nascita", GetType(String))
        MyTable.Columns.Add("Data Validita", GetType(String))
        MyTable.Columns.Add("Codice Listino E-Personam", GetType(String))
        MyTable.Columns.Add("Descrizione Listino E-Personam", GetType(String))
        MyTable.Columns.Add("Segnalazione", GetType(String))

        

        Dim cmd As New OleDbCommand()

        Condizione = "("
        If Chk_Accoglimento.Checked = True Then
            If Condizione <> "(" Then
                Condizione = Condizione & " OR "
            End If
            Condizione = Condizione & " TipoMov = '05'"
        End If
        If Chk_UscitaDefinitiva.Checked = True Then
            If Condizione <> "(" Then
                Condizione = Condizione & " OR "
            End If
            Condizione = Condizione & " TipoMov = '13'"
        End If
        Condizione = Condizione & ")"


        If DD_CServ.SelectedValue = "" Then
            If DD_Struttura.SelectedValue <> "" Then
                If Condizione <> "" Then
                    Condizione = Condizione & " And ("
                Else
                    Condizione = Condizione & " ("
                End If
                Dim Indice As Integer
                Call AggiornaCServ()
                For Indice = 0 To DD_CServ.Items.Count - 1
                    If Indice >= 1 Then
                        If Condizione <> "" Then
                            Condizione = Condizione & " Or "
                        End If
                    End If
                    Condizione = Condizione & "  MOVIMENTI.CentroServizio = '" & DD_CServ.Items(Indice).Value & "'"
                Next
                Condizione = Condizione & ") "
            End If
            MySql = "Select * From Movimenti Where Data >= ? And Data <= ? And " & Condizione & " Order By Data "
        Else
            MySql = "Select * From Movimenti Where Data >= ? And Data <= ? And " & Condizione & "  And CENTROSERVIZIO = '" & DD_CServ.SelectedValue & "'" & " Order By Data "
        End If


        ELIMINALISTINI()

        cmd.CommandText = MySql
        Dim DataDal As Date = Txt_DataDal.Text
        Dim DataAl As Date = Txt_DataAl.Text

        cmd.Parameters.AddWithValue("@DataDal", DataDal)
        cmd.Parameters.AddWithValue("@DataAl", DataAl)
        cmd.Connection = cn
        Dim myPOSTreader As OleDbDataReader = cmd.ExecuteReader()
        Do While myPOSTreader.Read
            Dim ArrayPreseInCarico As New List(Of DatiPresaInCarico)

            Dim DecOsp As New ClsOspite

            DecOsp.Leggi(Session("DC_OSPITE"), Val(campodb(myPOSTreader.Item("CodiceOspite"))))

            If DecOsp.NonInUso <> "S" Then
                


                Dim x1 As New ClsOspite

                x1.CodiceOspite = Val(campodb(myPOSTreader.Item("CodiceOspite")))
                x1.Leggi(Session("DC_OSPITE"), x1.CodiceOspite)


                Dim cs As New Cls_CentroServizio

                cs.Leggi(Session("DC_OSPITE"), campodb(myPOSTreader.Item("CENTROSERVIZIO")))

                Dim Listino As New Cls_Listino

                Listino.CODICEOSPITE = Val(campodb(myPOSTreader.Item("CodiceOspite")))
                Listino.CENTROSERVIZIO = campodb(myPOSTreader.Item("CENTROSERVIZIO"))
                Listino.LeggiAData(Session("DC_OSPITE"), campodbd(myPOSTreader.Item("Data")))

                If Listino.CodiceListino <> "" Then
                    Dim Declistino As New Cls_Tabella_Listino

                    Declistino.Codice = Listino.CodiceListino
                    Declistino.LeggiCausale(Session("DC_OSPITE"))

                End If

                If x1.CodiceOspite = 316 Then
                    x1.CodiceOspite = 316
                End If
                
                If Session("EPersonamUser").ToString <> "" And x1.IdEpersonam > 0 Then

                    Dim Token As String = ""

                    Try
                        Token = LoginPersonam(Context)

                    Catch ex As Exception

                    End Try

                    If Token <> "" Then
                        Try
                            Dim BuDaUtente As String = Session("EPersonamUser").ToString.Replace("senioruser_", "").Replace("w", "")

                            Dim ePersonamID As Integer = 0
                            If cs.EPersonam > 0 Then
                                ePersonamID = cs.EPersonam
                            Else
                                ePersonamID = cs.EPersonamN
                            End If

                            Dim Codice As String = ""

                            If campodb(myPOSTreader.Item("CENTROSERVIZIO")) = "AMB" Then
                                ePersonamID = 4959
                            End If

                            If campodb(myPOSTreader.Item("CENTROSERVIZIO")) = "RIC" Then
                                ePersonamID = 4956
                            End If

                            Dim Descrizione As String = PresaInCarico(Token, ePersonamID, x1.IdEpersonam, Context, Codice, ArrayPreseInCarico)

                            ArrayPreseInCarico.Sort(Function(x, y) x.DataInizio.CompareTo(y.DataFine))

                            Dim i As Integer
                            For i = 0 To ArrayPreseInCarico.Count - 1
                                If Format(ArrayPreseInCarico.Item(i).DataFine, "yyyyMMdd") <= Format(DataAl, "yyyyMMdd") And _
                                   Format(ArrayPreseInCarico.Item(i).DataFine, "yyyyMMdd") >= Format(DataDal, "yyyyMMdd") Then
                                    Dim myriga As System.Data.DataRow = MyTable.NewRow()
                                    myriga(0) = campodb(myPOSTreader.Item("CENTROSERVIZIO"))
                                    myriga(1) = Val(campodb(myPOSTreader.Item("CodiceOspite")))

                                    myriga(2) = x1.Nome
                                    myriga(3) = Format(x1.DataNascita, "dd/MM/yyyy")


                                    myriga(4) = ArrayPreseInCarico.Item(i).DataInizio

                                    myriga(5) = ArrayPreseInCarico.Item(i).Codice

                                    myriga(6) = ArrayPreseInCarico.Item(i).Descrizione



                                    'If campodb(myriga(4)) = "" Then
                                    Dim KL As New Cls_Tabella_Listino

                                    KL.CodiceEpersonam = ArrayPreseInCarico.Item(i).Codice
                                    KL.LeggiCodiceEpersonam(Session("DC_OSPITE"))


                                    
                                    Dim InserisciLisctino As New Cls_Listino

                                    InserisciLisctino.CODICEOSPITE = Val(campodb(myPOSTreader.Item("CodiceOspite")))
                                    InserisciLisctino.CENTROSERVIZIO = campodb(myPOSTreader.Item("CENTROSERVIZIO"))

                                    InserisciLisctino.DATA = ArrayPreseInCarico.Item(i).DataInizio
                                    InserisciLisctino.InserisciRette(Session("DC_OSPITE"), campodb(myPOSTreader.Item("CENTROSERVIZIO")), Val(campodb(myPOSTreader.Item("CodiceOspite"))), KL.Codice, InserisciLisctino.DATA)

                                    Dim StringaErrore As String = ""

                                    If LeggiMovimento(Session("DC_OSPITE"), campodb(myPOSTreader.Item("CENTROSERVIZIO")), Val(campodb(myPOSTreader.Item("CodiceOspite"))), ArrayPreseInCarico.Item(i).DataInizio) = False Then
                                        StringaErrore = " Accoglimento errato (" & ArrayPreseInCarico.Item(i).DataInizio & ")"
                                    End If
                                    If LeggiMovimento(Session("DC_OSPITE"), campodb(myPOSTreader.Item("CENTROSERVIZIO")), Val(campodb(myPOSTreader.Item("CodiceOspite"))), ArrayPreseInCarico.Item(i).DataFine) = False Then
                                        StringaErrore = StringaErrore & " Uscita errate (" & ArrayPreseInCarico.Item(i).DataFine & ")"
                                    End If

                                    myriga(7) = "Inserito Listino " & KL.Descrizione & " " & StringaErrore


                                    MyTable.Rows.Add(myriga)
                                End If
                            Next
                        Catch ex As Exception

                        End Try

                    End If
                End If



                'Dim EscludiPrimo As Boolean = False
                'For Each item As DatiPresaInCarico In ArrayPreseInCarico
                '    If EscludiPrimo Then

                '        Dim myriga2 As System.Data.DataRow = MyTable.NewRow()

                '        myriga2(0) = myriga(0)
                '        myriga2(1) = myriga(1)
                '        myriga2(2) = myriga(2)
                '        myriga2(3) = myriga(3)
                '        myriga2(4) = myriga(4)
                '        myriga2(5) = myriga(5)
                '        myriga2(6) = myriga(6)
                '        myriga2(7) = myriga(7)

                '        myriga2(4) = item.DataInizio
                '        myriga2(5) = item.Codice

                '        myriga2(6) = item.Descrizione

                '        Dim KL As New Cls_Tabella_Listino

                '        KL.CodiceEpersonam = item.Codice
                '        KL.LeggiCodiceEpersonam(Session("DC_OSPITE"))


                '        If KL.Descrizione <> "" Then


                '            Dim InserisciLisctino As New Cls_Listino

                '            InserisciLisctino.CODICEOSPITE = Val(campodb(myPOSTreader.Item("CodiceOspite")))
                '            InserisciLisctino.CENTROSERVIZIO = campodb(myPOSTreader.Item("CENTROSERVIZIO"))

                '            InserisciLisctino.DATA = item.DataInizio
                '            InserisciLisctino.InserisciRette(Session("DC_OSPITE"), campodb(myPOSTreader.Item("CENTROSERVIZIO")), Val(campodb(myPOSTreader.Item("CodiceOspite"))), KL.Codice, InserisciLisctino.DATA)

                '            myriga2(7) = "Inserito Listino " & KL.Descrizione
                '        Else
                '            myriga2(7) = "Listino non trovato"
                '        End If

                '        MyTable.Rows.Add(myriga2)

                '    End If
                '    EscludiPrimo = True
                'Next

            End If
        Loop
        cn.Close()

        Dim myriga1 As System.Data.DataRow = MyTable.NewRow()
        MyTable.Rows.Add(myriga1)

        GridView1.AutoGenerateColumns = True
        GridView1.DataSource = MyTable
        GridView1.Font.Size = 10
        GridView1.DataBind()



    End Sub


    Public Function LeggiMovimento(ByVal StringaConnessione As String, ByVal CENTROSERVIZIO As String, ByVal CodiceOspite As Integer, ByVal Data As Date) As Boolean
        Dim cn As OleDbConnection

        cn = New Data.OleDb.OleDbConnection(StringaConnessione)

        cn.Open()
        Dim cmd As New OleDbCommand()
        cmd.CommandText = ("select * from MOVIMENTI where CodiceOspite = " & CodiceOspite & " And CENTROSERVIZIO = '" & CENTROSERVIZIO & "' And Data = ?  ")
        cmd.Connection = cn
        cmd.Parameters.AddWithValue("@Data", Format(Data, "dd/MM/yyyy"))

        Dim myPOSTreader As OleDbDataReader = cmd.ExecuteReader()
        If myPOSTreader.Read Then
            LeggiMovimento = True
        Else
            LeggiMovimento = False
        End If
        myPOSTreader.Close()
        cn.Close()
    End Function

    'Dim ArrayPreseInCarico As New List(Of DatiPresaInCarico)
    Private Function PresaInCarico(ByVal Token As String, ByVal BU As Integer, ByVal IdOspite As Integer, ByVal context As HttpContext, ByRef Codice As String, ByRef ArrayPreseInCarico As List(Of DatiPresaInCarico)) As String
        System.Net.ServicePointManager.ServerCertificateValidationCallback = AddressOf CertificateHandler


        ' /api/v1.0/business_units/:business_unit_id/guests/:id_or_cf/periods
        Dim client As HttpWebRequest = WebRequest.Create("https://api.e-personam.com/api/v1.0/business_units/" & BU & "/guests/" & IdOspite & "/periods")

        client.Method = "GET"
        client.Headers.Add("Authorization", "Bearer " & Token)
        client.ContentType = "Content-Type: application/json"

        Dim reader As StreamReader
        Dim response As HttpWebResponse = Nothing

        response = DirectCast(client.GetResponse(), HttpWebResponse)

        reader = New StreamReader(response.GetResponseStream())

        Dim Descrizione As String = ""



        Dim rawresp As String
        rawresp = reader.ReadToEnd()

        Dim Periodi As JArray = JArray.Parse(rawresp)

        For Each jPeriodo As JToken In Periodi

            Dim TArrayPreseInCarico As New DatiPresaInCarico


            Try
                Descrizione = jPeriodo.Item("intype").Item("description")
            Catch ex As Exception

            End Try
            Try
                Codice = jPeriodo.Item("intype").Item("cod")
            Catch ex As Exception

            End Try

            Try
                TArrayPreseInCarico.DataInizio = jPeriodo.Item("data_ora_ingresso")
            Catch ex As Exception

            End Try

            Try
                TArrayPreseInCarico.DataFine = jPeriodo.Item("data_ora_dimissione")
            Catch ex As Exception

            End Try


            TArrayPreseInCarico.Codice = Codice
            TArrayPreseInCarico.Descrizione = Descrizione

            ArrayPreseInCarico.Add(TArrayPreseInCarico)

        Next



        Return Descrizione


    End Function
    Private Function LoginPersonam(ByVal context As HttpContext) As String
        Try


            Dim request As New NameValueCollection


            request.Add("client_id", "98217ca6670c660e22f42d58e231d4e56c84fb34e216b0f66f1f30284fd3ec9d")
            request.Add("client_secret", "5570a684f69ca74d75d16bba669c156ec092eccb4111d0974adec3edb2fa4d6f")


            request.Add("username", context.Session("EPersonamUser"))



            request.Add("password", context.Session("EPersonamPSW"))
            System.Net.ServicePointManager.ServerCertificateValidationCallback = AddressOf CertificateHandler


            Dim client As New WebClient()

            Dim result As Object = client.UploadValues("https://api.e-personam.com/api/v1.0/token/password", "POST", request)


            Dim stringa As String = Encoding.Default.GetString(result)


            Dim serializer As JavaScriptSerializer


            serializer = New JavaScriptSerializer()




            Dim s As Autentificazione = serializer.Deserialize(Of Autentificazione)(stringa)


            Return s.access_token
        Catch ex As Exception
            Return ""
        End Try
    End Function

    Public Class Autentificazione
        Public access_token As String
        Public expires_in As String
        Public scope As String
        Public token_type As String
        Public refresh_token As String
    End Class


    Private Shared Function CertificateHandler(ByVal sender As Object, ByVal certificate As X509Certificate, ByVal chain As X509Chain, ByVal SSLerror As SslPolicyErrors) As Boolean
        Return True
    End Function

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Call EseguiJS()
        If Trim(Session("UTENTE")) = "" Then
            Response.Redirect("../Login.aspx")
            Exit Sub
        End If

        If Page.IsPostBack = True Then Exit Sub


        Dim K1 As New Cls_SqlString

        Dim Barra As New Cls_BarraSenior

        If Not IsNothing(Session("RicercaAnagraficaSQLString")) Then
            K1 = Session("RicercaAnagraficaSQLString")
        End If

        Lbl_BarraSenior.Text = Barra.CodiceBarra(Application("SENIOR"), Session("UTENTE"), Page, K1)

        Dim kVilla As New Cls_TabelleDescrittiveOspitiAccessori


        kVilla.UpDateDropBox(Session("DC_OSPITIACCESSORI"), "VIL", DD_Struttura)
        If DD_Struttura.Items.Count = 1 Then
            Call AggiornaCServ()
        End If

        Txt_DataAl.Text = Format(Now, "dd/MM/yyyy")
        Txt_DataDal.Text = Format(Now, "dd/MM/yyyy")

        Lbl_Utente.Text = Session("UTENTE")

        Lbl_NomeOspite.Text = "ATTENZIONE IL PROGRAMA ELIMINERA' I LISTINE PER GLI OSPITI ESTRATTI"
        Lbl_NomeOspite.ControlStyle.ForeColor = Drawing.Color.Red



        Call EseguiJS()
    End Sub

    Protected Sub ImageButton3_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles ImageButton3.Click
        If Not IsDate(Txt_DataDal.Text) Then
            REM ClientScript.RegisterClientScriptBlock(Me.GetType(), "Errore", "VisualizzaErrore('Data dal non corretta');", True)
            ScriptManager.RegisterStartupScript(Me, Me.GetType(), "visualizzaA", "VisualizzaErrore('Data dal non corretta');", True)
            Call EseguiJS()
            Exit Sub
        End If
        If Not IsDate(Txt_DataAl.Text) Then
            REM ClientScript.RegisterClientScriptBlock(Me.GetType(), "Errore", "VisualizzaErrore('Data al non corretta');", True)
            ScriptManager.RegisterStartupScript(Me, Me.GetType(), "visualizzaA", "VisualizzaErrore('Data al non corretta');", True)
            Call EseguiJS()
            Exit Sub
        End If
        If Chk_Accoglimento.Checked = False And Chk_UscitaDefinitiva.Checked = False Then
            REM ClientScript.RegisterClientScriptBlock(Me.GetType(), "Errore", "VisualizzaErrore('Specificare almeno un tipo movimento');", True)
            ScriptManager.RegisterStartupScript(Me, Me.GetType(), "visualizzaA", "VisualizzaErrore('Specificare almeno un tipo movimento');", True)
            Call EseguiJS()
            Exit Sub
        End If
        If Not VerificaListinoPresaInCarico() Then
            ScriptManager.RegisterStartupScript(Me, Me.GetType(), "visualizzaA", "VisualizzaErrore('Non posso procedere nessun collegamento listini-prese in carico');", True)
            Call EseguiJS()
            Exit Sub
        End If

        Call CaricaTabella()
        Call EseguiJS()
    End Sub



    Private Sub EseguiJS()
        Dim MyJs As String
        MyJs = "$(document).ready(function() {"

        MyJs = MyJs & "var els = document.getElementsByTagName(""*"");"

        MyJs = MyJs & "for (var i=0;i<els.length;i++)"
        MyJs = MyJs & "if ( els[i].id ) { "
        MyJs = MyJs & " var appoggio =els[i].id; "

        MyJs = MyJs & " if ((appoggio.match('Txt_Ospite')!= null) || (appoggio.match('Txt_Ospite')!= null))  {  "
        MyJs = MyJs & " $(els[i]).autocomplete('AutocompleteOspitiSenzaCserv.ashx?Utente=" & Session("UTENTE") & "', {delay:5,minChars:3});"
        MyJs = MyJs & "    }"

        MyJs = MyJs & " if (appoggio.match('Txt_Data')!= null) {  "
        MyJs = MyJs & " $(els[i]).mask(""99/99/9999"");"

        MyJs = MyJs & " $(els[i]).datepicker({ changeMonth: true,changeYear: true,  yearRange: ""1990:" & Year(Now) + 5 & """},$.datepicker.regional[""it""]);"

        MyJs = MyJs & "    }"
        MyJs = MyJs & "} "
        MyJs = MyJs & "if (window.innerHeight>0) { $(""#BarraLaterale"").css(""height"",(window.innerHeight - 94) + ""px""); } else"
        MyJs = MyJs & "{ $(""#BarraLaterale"").css(""height"",(document.documentElement.offsetHeight - 94) + ""px"");  }"
        MyJs = MyJs & "});"

        'MyJs = "$(document).ready(function() { $('.myClass').keypress(function() { handleEnter($('.myClass').val(), true, true); } );     $('#" & Txt_ImportoPagato.ClientID & "').blur(function() { var ap = formatNumber ($('#" & Txt_ImportoPagato.ClientID & "').val(),2); $('#" & Txt_ImportoPagato.ClientID & "').val(ap); }); });"
        ScriptManager.RegisterStartupScript(Me, Me.GetType(), "visualizzaRitIm", MyJs, True)
    End Sub

    Protected Sub Btn_Esci_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles Btn_Esci.Click
        Response.Redirect("Menu_Strumenti.aspx")
    End Sub


    Private Sub AggiornaCServ()
        Dim kCsrv As New Cls_CentroServizio

        If DD_Struttura.SelectedValue = "" Then
            kCsrv.UpDateDropBox(Session("DC_OSPITE"), DD_CServ)
        Else
            kCsrv.UpDateDropBoxStruttura(Session("DC_OSPITE"), DD_CServ, DD_Struttura.SelectedValue)
        End If
    End Sub



    Protected Sub DD_Struttura_TextChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles DD_Struttura.TextChanged
        AggiornaCServ()
        Call EseguiJS()
    End Sub

    Protected Sub DD_CServ_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles DD_CServ.Load

    End Sub


    Private Function SplitWords(ByVal s As String) As String()
        '
        ' Call Regex.Split function from the imported namespace.
        ' Return the result array.
        '
        Return Regex.Split(s, "\W+")
    End Function



End Class
