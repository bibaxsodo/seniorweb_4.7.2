﻿Imports System.Web.Hosting
Imports System.Data.OleDb

Partial Class OspitiWeb_TabelleDescrittiveOspitiAccessori
    Inherits System.Web.UI.Page
    Dim MyTable As New System.Data.DataTable("tabella")
    Dim MyDataSet As New System.Data.DataSet()
    Private Sub faibind()

        MyTable = ViewState("TabDescrittive")
        Grid.AutoGenerateColumns = False
        Grid.DataSource = MyTable
        Grid.DataBind()

    End Sub
    Protected Sub Button1_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles Button1.Click
        Dim k As New Cls_TabelleDescrittiveOspitiAccessori

        DD_Tabella.Enabled = False
        k.loaddati(Session("DC_OSPITIACCESSORI"), DD_Tabella.SelectedValue, MyTable)



        If MyTable.Rows.Count = 0 Then

            Dim myriga As System.Data.DataRow = MyTable.NewRow()
            MyTable.Rows.Add(myriga)

        End If

        ViewState("TabDescrittive") = MyTable
        Grid.DataSource = MyTable


        Grid.AutoGenerateColumns = False


        Grid.DataBind()


    End Sub

    Protected Sub Grid_RowCancelingEdit(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewCancelEditEventArgs) Handles Grid.RowCancelingEdit
        Grid.EditIndex = -1
        Call faibind()
    End Sub

    Private Sub InserisciRiga()

        Call UpDateTable()
        MyTable = ViewState("TabDescrittive")
        Dim myriga As System.Data.DataRow = MyTable.NewRow()
        MyTable.Rows.Add(myriga)

        ViewState("TabDescrittive") = MyTable
        Call faibind()
    End Sub

    Protected Sub Grid_RowCommand(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewCommandEventArgs) Handles Grid.RowCommand
        If (e.CommandName = "Inserisci") Then
            Call InserisciRiga()
        End If
    End Sub

    Protected Sub Grid_RowDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs) Handles Grid.RowDataBound
        If e.Row.RowType = DataControlRowType.DataRow Then




            Dim LblCodice As Label = DirectCast(e.Row.FindControl("LblCodice"), Label)
            If Not IsNothing(LblCodice) Then

            Else
                Dim TxtCodice As TextBox = DirectCast(e.Row.FindControl("TxtCodice"), TextBox)
                TxtCodice.Text = MyTable.Rows(e.Row.RowIndex).Item(0).ToString

            End If

            Dim LblDescrizione As Label = DirectCast(e.Row.FindControl("LblDescrizione"), Label)
            If Not IsNothing(LblDescrizione) Then

            Else
                Dim TxtDescrizione As TextBox = DirectCast(e.Row.FindControl("TxtDescrizione"), TextBox)
                TxtDescrizione.Text = MyTable.Rows(e.Row.RowIndex).Item(1).ToString
            End If

        End If
    End Sub

    Protected Sub Grid_RowDeleting(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewDeleteEventArgs) Handles Grid.RowDeleting
        MyTable = ViewState("TabDescrittive")


        MyTable.Rows.RemoveAt(e.RowIndex)

        If MyTable.Rows.Count = 0 Then
            Dim myriga As System.Data.DataRow = MyTable.NewRow()

            MyTable.Rows.Add(myriga)
        End If

        ViewState("TabDescrittive") = MyTable

        Call faibind()
    End Sub

    Protected Sub Grid_RowEditing(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewEditEventArgs) Handles Grid.RowEditing
        Grid.EditIndex = e.NewEditIndex
        Call faibind()
    End Sub

    Protected Sub Grid_RowUpdating(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewUpdateEventArgs) Handles Grid.RowUpdating
        Dim TxtCodice As TextBox = DirectCast(Grid.Rows(e.RowIndex).FindControl("TxtCodice"), TextBox)
        Dim TxtDescrizione As TextBox = DirectCast(Grid.Rows(e.RowIndex).FindControl("TxtDescrizione"), TextBox)

        If TxtCodice.Text = "" Then
            Exit Sub
        End If
        If TxtDescrizione.Text = "" Then
            Exit Sub
        End If

        MyTable = ViewState("TabDescrittive")
        Dim row = Grid.Rows(e.RowIndex)

        MyTable.Rows(row.DataItemIndex).Item(0) = TxtCodice.Text
        MyTable.Rows(row.DataItemIndex).Item(1) = TxtDescrizione.Text

        ViewState("TabDescrittive") = MyTable


        Grid.EditIndex = -1
        Call faibind()
    End Sub

    Private Sub Pulisci()
        MyTable.Clear()
        MyTable.Columns.Clear()
        MyTable.Columns.Add("Codice", GetType(String))
        MyTable.Columns.Add("Descrizione", GetType(String))
        Dim myriga As System.Data.DataRow = MyTable.NewRow()
        MyTable.Rows.Add(myriga)

        ViewState("TabDescrittive") = MyTable

        DD_Tabella.Enabled = True

        DD_Tabella.SelectedValue = ""
        Call faibind()
    End Sub







    Protected Sub Imb_Modifica_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles Imb_Modifica.Click
        Dim I As Long

        If DD_Tabella.SelectedValue = "" Then
            ClientScript.RegisterClientScriptBlock(Me.GetType(), "Errore", "VisualizzaErrore('Specificare tabella');", True)
            Exit Sub
        End If

        Call UpDateTable()
        MyTable = ViewState("TabDescrittive")

        Dim cn As OleDbConnection
        Dim MySql As String

        MySql = ""


        cn = New Data.OleDb.OleDbConnection(Session("DC_OSPITIACCESSORI"))

        cn.Open()
        Dim cmd As New OleDbCommand()
        cmd.CommandText = ("DELETE from TABELLE where TipoTabella = '" & DD_Tabella.SelectedValue & "'")
        cmd.Connection = cn
        cmd.ExecuteNonQuery()

        cn.Close()

        For I = 0 To MyTable.Rows.Count - 1
            Dim MC As New Cls_TabelleDescrittiveOspitiAccessori

            If MyTable.Rows(I).Item(0).ToString <> "" And MyTable.Rows(I).Item(1).ToString <> "" Then
                MC.TipoTabella = DD_Tabella.SelectedValue
                MC.CodiceTabella = MyTable.Rows(I).Item(0).ToString
                MC.Descrizione = MyTable.Rows(I).Item(1).ToString
                MC.Scrivi(Session("DC_OSPITIACCESSORI"))
            End If
        Next

        Dim MyJs As String

        MyJs = "$(document).ready(function() { alert('Modificato Tabella'); } );"
        ClientScript.RegisterClientScriptBlock(Me.GetType(), "ModScM", MyJs, True)
        Call Pulisci()
    End Sub

    Private Sub UpDateTable()
        Dim i As Integer

        MyTable.Clear()
        MyTable.Columns.Clear()
        MyTable.Columns.Add("Codice", GetType(String))
        MyTable.Columns.Add("Descrizione", GetType(String))

        For i = 0 To Grid.Rows.Count - 1

            Dim TxtData As TextBox = DirectCast(Grid.Rows(i).FindControl("TxtCodice"), TextBox)
            Dim TxtImporto As TextBox = DirectCast(Grid.Rows(i).FindControl("TxtDescrizione"), TextBox)


            Dim myrigaR As System.Data.DataRow = MyTable.NewRow()


            myrigaR(0) = TxtData.Text
            myrigaR(1) = TxtImporto.Text

            MyTable.Rows.Add(myrigaR)
        Next
        ViewState("TabDescrittive") = MyTable

    End Sub

    Protected Sub Btn_Esci_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles Btn_Esci.Click
        Response.Redirect("Menu_Tabelle.aspx")
    End Sub

    Protected Sub BTN_InserisciRiga_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles BTN_InserisciRiga.Click
        Call InserisciRiga()
    End Sub

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load


        If Page.IsPostBack Then Exit Sub


        Dim K1 As New Cls_SqlString

        Dim Barra As New Cls_BarraSenior

        If Not IsNothing(Session("RicercaAnagraficaSQLString")) Then
            K1 = Session("RicercaAnagraficaSQLString")
        End If

        Lbl_BarraSenior.Text = Barra.CodiceBarra(Application("SENIOR"), Session("UTENTE"), Page, K1)
    End Sub

End Class
