﻿<%@ Page Language="VB" AutoEventWireup="false" ValidateRequest="false" Inherits="OspitiWeb_Documentazione" CodeFile="Documentazione.aspx.vb" %>
<%@ Register assembly="AjaxControlToolkit" namespace="AjaxControlToolkit" tagprefix="xasp" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <meta http-equiv="x-ua-compatible" content="IE=9" />
    <title>Documentazione</title>
    <link href="ospiti.css?ver=11" rel="stylesheet" type="text/css" /> <link rel="shortcut icon" href="images/SENIOR.ico"/>
    <link href="js/jquery.autocomplete.css" rel="stylesheet" type="text/css" />
       
    <link rel="stylesheet" href="dialogbox/redips-dialog.css" type="text/css" media="screen" />
	<script type="text/javascript" src="dialogbox/redips-dialog-min.js"></script>
    <script type="text/javascript" src="dialogbox/script.js"></script>
    
    <script src="js/jquery-1.5.1.min.js" type="text/javascript"></script>
    <script src="js/jquery.autocomplete.js" type="text/javascript"></script>
    <script src="js/soapclient.js" type="text/javascript"></script>
    <script src="js/convertinumero.js" type="text/javascript"></script>
    
    
    <script src="js/jquery.maskedinput-1.3.min.js" type="text/javascript"></script>
    <script src="js/formatnumer.js" type="text/javascript"></script>  
    <script src="js/JSErrore.js" type="text/javascript"></script>
    <script src="js/NoEnter.js" type="text/javascript"></script>
    <script type="text/javascript" src="../js/menuanagraficaospiti.js?ver=7"></script>    
          <script type="text/javascript">
       function DialogBox(Path)
        {
        
          REDIPS.dialog.show(500, 300, '<iframe id="output" src="' + Path + '" height="300px" width="500"></iframe>');
          return false;                    
        } 
    $(document).ready( function() {
      if (window.innerHeight>0) { $("#BarraLaterale").css("height",(window.innerHeight - 94) + "px"); } else
        { $("#BarraLaterale").css("height",(document.documentElement.offsetHeight - 94) + "px");  }
    });         
</script>
</head>
<body>
    <form id="form1" runat="server">
    <div align="left">
<asp:ScriptManager ID="ScriptManager1" runat="server" EnableScriptGlobalization="true"></asp:ScriptManager>
    <table style="width:100%;" cellpadding="0" cellspacing="0">
    <tr>
    <td style="width:160px; background-color:#F0F0F0;"></td>
    <td>
    <div class="Titolo">Ospiti - Anagrafica - Documentazione</div>
    <div class="SottoTitoloOSPITE">
        <asp:Label ID="Lbl_NomeOspite" runat="server" ></asp:Label><br />        
        <br />
    </div>
    </td>
    <td style="text-align:right;">
        <span class="BenvenutoText">Benvenuto <%=Session("UTENTE")%>&nbsp;&nbsp;&nbsp;&nbsp;</span>
             <asp:ImageButton runat="server" ImageUrl="~/images/salva.jpg"  class="EffettoBottoniTondi"  Height="38px" ToolTip="Modifica / Inserisci"  ID="ImageButton1"></asp:ImageButton>                    
     </td>
    </tr>
   
    <tr>
    <td style="width:160px; background-color:#F0F0F0; vertical-align:top; text-align:center;" id="BarraLaterale">  
     <a href="Menu_Ospiti.aspx" style="border-width:0px;"><img src="images/Home.jpg" alt="Menù" class="Effetto" /></a><br />
     <asp:ImageButton ID="Btn_Esci" runat="server" BackColor="Transparent" ImageUrl="../images/Menu_Indietro.png" class="Effetto" ToolTip="Chiudi"  />
     <br />
     
     <div id="MENUDIV"></div>
     <br />
     <br />
     <br />
     <br />
     <br />
     <br />
     <br />
     <br />
     <br />
     <br />
     <br />
     <br />
    </td>    
    <td colspan="2" style="background-color: #FFFFFF; vertical-align:top; " > 
     <xasp:TabContainer ID="TabContainer1" runat="server" ActiveTabIndex="0" CssClass="TabSenior"  
            Height="558px" Width="100%" BorderStyle="None" style="margin-right: 39px">               
     <xasp:TabPanel runat="server" HeaderText="Prima Nota" ID="Tab_Anagrafica"><HeaderTemplate>
          Documentazione                
         </HeaderTemplate>     
<ContentTemplate>  
    
    <asp:UpdatePanel ID="UpdatePanel1" runat="server">
    <ContentTemplate>      
        <asp:Label ID="Lbl_Percorso" runat="server" Text="Label"></asp:Label>
              <asp:GridView ID="Grid" runat="server" CellPadding="4" Height="60px"  
ShowFooter="True" BackColor="White"  BorderColor="#6FA7D1"
               BorderStyle="Dotted" BorderWidth="1px"  >
               <RowStyle ForeColor="#333333" BackColor="White" />
            <Columns>

                <asp:TemplateField HeaderText=""> 
                    <ItemTemplate>
                      <asp:ImageButton ID="IB_Modify" CommandName="Edit" Runat="Server"
                      ImageUrl="~/images/modifica.png" />
                    </ItemTemplate>
                    <EditItemTemplate>
                      <asp:ImageButton ID="IB_Update" CommandName="Update" Runat="Server"
                      ImageUrl="~/images/aggiorna.png" /> 
                      <asp:ImageButton ID="IB_Cancel" CommandName="Cancel" Runat="Server"
                      ImageUrl="~/images/annulla.png" />
                    </EditItemTemplate>
                </asp:TemplateField>
                
                <asp:TemplateField HeaderText=""> 
                    <ItemTemplate>
                      <asp:ImageButton ID="IB_Printer" CommandName="DOWNLOAD" Runat="Server"
                      ImageUrl="~/images/open.png" 
                       CommandArgument= <%#  Eval("NomeFile") %>
                      />
                    </ItemTemplate>
                </asp:TemplateField> 
               
                <asp:TemplateField HeaderText="NomeFile">
                <EditItemTemplate>
                <asp:Label ID="LblNomeFile" runat="server" Text='<%# Eval("NomeFile") %>' Width="236px"></asp:Label>
                </EditItemTemplate>
                <ItemTemplate>                
                 <asp:Label ID="LblNomeFile" runat="server" Text='<%# Eval("NomeFile") %>' Width="236px"></asp:Label>
                </ItemTemplate>
                </asp:TemplateField>
     
                
                
                <asp:TemplateField HeaderText="Descrizione">
                <EditItemTemplate>
                <asp:TextBox ID="TxtDescrizione" MaxLength="100" Width="300px" runat="server"></asp:TextBox>
                </EditItemTemplate>
                <ItemTemplate>                
                 <asp:Label ID="LblDescrizione" runat="server" Text='<%# Eval("Descrizione") %>' Width="136px"></asp:Label>
                </ItemTemplate>
                </asp:TemplateField>

                <asp:TemplateField HeaderText="Data">
                <EditItemTemplate>
                <asp:TextBox ID="TxtData" Width="100px" runat="server"></asp:TextBox>
                </EditItemTemplate>
                <ItemTemplate>                
                 <asp:Label ID="LblData" runat="server" Text='<%# Eval("Data") %>' Width="136px"></asp:Label>
                </ItemTemplate>
                </asp:TemplateField>                
            </Columns>
              <FooterStyle BackColor="White" ForeColor="#023102" />
              <HeaderStyle BackColor="#A6C9E2" Font-Bold="False"  ForeColor="White" BorderColor="#6FA7D1"  BorderWidth="1"  />
              <PagerStyle BackColor="#336666" ForeColor="White" HorizontalAlign="Center" />
              <SelectedRowStyle BackColor="#339966" Font-Bold="True" ForeColor="White" />
         </asp:GridView>        
    
              </ContentTemplate>
    </asp:UpdatePanel>
    
    <div style="text-align:left;">
            Carica un file in skydrive :<br />
            <asp:FileUpload ID="FileUpload1" runat="server" Width="599px" />
            <asp:ImageButton ID="UpLoadFile" runat="server" Height="48px" ImageUrl="~/images/upload.png" Width="48px" ToolTip="Carica File" />    
            </div>
         </ContentTemplate>
            
</xasp:TabPanel>
         </xasp:TabContainer>         
     </td>
     </tr>     
     </table>                    
    </form>
</body>
</html>