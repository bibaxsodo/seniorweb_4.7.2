﻿Imports System
Imports System.Web
Imports Microsoft.VisualBasic
Imports System.Data.OleDb
Imports System.Web.Hosting
Imports System.Data.SqlTypes
Imports System.Net
Imports System.Collections.Generic
Imports System.IO
Imports System.Security.Cryptography.X509Certificates
Imports System.Net.Security
Imports System.Web.Script.Serialization
Imports org.jivesoftware.util
Imports Newtonsoft.Json
Imports Newtonsoft.Json.Linq

Partial Class OspitiWeb_InserisciDatiOspitiDaCF
    Inherits System.Web.UI.Page

    Protected Sub Button1_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles Button1.Click
        Dim i As Integer
        Dim Appoggio As String = ""
        Dim TestoInput As String = TextBox1.Text

        i = 1
        Do While i < Len(TestoInput)

            If Asc(Mid(TestoInput, i, 1)) = 13 Then
                Dim Ospite As New ClsOspite

                If Chk_CercaPerCognomeNome.Checked = False Then
                    Ospite.CODICEFISCALE = Appoggio
                    Ospite.LeggiPerCodiceFiscale(Session("DC_OSPITE"), Appoggio)
                Else
                    Ospite.Nome = Appoggio
                    Ospite.LeggiNome(Session("DC_OSPITE"))
                End If

                If Ospite.CodiceOspite > 0 Then
                    Call RigaStato(Ospite.CodiceOspite)
                    'RigaRettaTotaleERettaOspite(Ospite.CodiceOspite)
                End If
                Appoggio = ""
            Else
                If Asc(Mid(TestoInput, i, 2)) <> 10 Then
                    Appoggio = Appoggio & Mid(TestoInput, i, 1)
                End If
            End If
            i = i + 1
        Loop
    End Sub

    Private Sub RigaRettaTotaleERettaOspite(ByVal CodiceOspite As Integer)
        Dim MyStato As New Cls_StatoAuto
        Dim ImportoOspite As Double
        Dim MyVerifica As New Cls_ImportoOspite
        Dim MyImportoOspite As New Cls_ImportoOspite
        Dim MyModalita As New Cls_Modalita

        Dim MyVerificaRT As New Cls_rettatotale
        Dim MyRettaTotale As New Cls_rettatotale
        Dim MyMovimento As New Cls_Movimenti

        MyMovimento.CodiceOspite = CodiceOspite
        MyMovimento.UltimaData(Session("DC_OSPITE"), CodiceOspite)


        If MyMovimento.CENTROSERVIZIO <> "" Then
            Dim ImportoUsl As Double

            MyStato.CENTROSERVIZIO = MyMovimento.CENTROSERVIZIO
            MyStato.CODICEOSPITE = CodiceOspite
            MyStato.UltimaDataConUsl(Session("DC_OSPITE"), MyStato.CODICEOSPITE, MyStato.CENTROSERVIZIO)

            Dim MyUsl As New Cls_ImportoRegione

            MyUsl.Codice = MyStato.USL
            MyUsl.TipoRetta = MyStato.TipoRetta
            MyUsl.UltimaData(Session("DC_OSPITE"), MyUsl.Codice, MyUsl.TipoRetta)
            ImportoUsl = MyUsl.Importo
            ImportoOspite = 153

            MyVerifica.CENTROSERVIZIO = MyMovimento.CENTROSERVIZIO
            MyVerifica.CODICEOSPITE = CodiceOspite
            MyVerifica.UltimaData(Session("DC_OSPITE"), MyStato.CODICEOSPITE, MyStato.CENTROSERVIZIO)
            If MyVerifica.Importo = 0 Then

                MyImportoOspite.CENTROSERVIZIO = MyMovimento.CENTROSERVIZIO
                MyImportoOspite.CODICEOSPITE = CodiceOspite
                MyImportoOspite.Importo = 0
                MyImportoOspite.Tipo = "G"
                MyImportoOspite.Data = MyMovimento.Data
                MyImportoOspite.AggiornaDB(Session("DC_OSPITE"))


                MyModalita.CENTROSERVIZIO = MyMovimento.CENTROSERVIZIO
                MyModalita.CODICEOSPITE = CodiceOspite
                MyModalita.MODALITA = "O"
                MyModalita.Data = MyMovimento.Data
                MyModalita.AggiornaDB(Session("DC_OSPITE"))
            End If


            MyVerificaRT.CENTROSERVIZIO = MyMovimento.CENTROSERVIZIO
            MyVerificaRT.CODICEOSPITE = CodiceOspite
            MyVerificaRT.UltimaData(Session("DC_OSPITE"), MyStato.CODICEOSPITE, MyStato.CENTROSERVIZIO)
            If MyVerificaRT.Importo = 0 Then

                MyRettaTotale.CENTROSERVIZIO = MyMovimento.CENTROSERVIZIO
                MyRettaTotale.CODICEOSPITE = CodiceOspite
                MyRettaTotale.Importo = ImportoOspite + ImportoUsl
                MyRettaTotale.Tipo = "G"
                MyRettaTotale.Data = MyMovimento.Data
                MyRettaTotale.AggiornaDB(Session("DC_OSPITE"))
            End If

        End If
    End Sub

    Private Sub RigaStato(ByVal CodiceOspite As Integer)
        Dim MyStato As New Cls_StatoAuto
        Dim MyVerifica As New Cls_StatoAuto
        Dim MyMovimento As New Cls_Movimenti

        MyMovimento.CodiceOspite = CodiceOspite
        MyMovimento.UltimaData(Session("DC_OSPITE"), CodiceOspite)


        If MyMovimento.CENTROSERVIZIO <> "" Then
            MyVerifica.USL = ""
            MyVerifica.CENTROSERVIZIO = MyMovimento.CENTROSERVIZIO
            MyVerifica.CODICEOSPITE = CodiceOspite
            MyVerifica.UltimaData(Session("DC_OSPITE"), MyVerifica.CODICEOSPITE, MyVerifica.CENTROSERVIZIO)
            If MyVerifica.USL = "" Then
                MyStato.CENTROSERVIZIO = MyMovimento.CENTROSERVIZIO
                MyStato.CODICEOSPITE = CodiceOspite

                MyStato.Data = MyMovimento.Data
                MyStato.USL = "2"
                MyStato.TipoRetta = "03"
                MyStato.AggiornaDB(Session("DC_OSPITE"))
            End If
        End If
    End Sub

    Protected Sub OspitiWeb_InserisciDatiOspitiDaCF_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Trim(Session("UTENTE")) = "" Then
            Response.Redirect("..\Login.aspx")
            Exit Sub
        End If
    End Sub
End Class
