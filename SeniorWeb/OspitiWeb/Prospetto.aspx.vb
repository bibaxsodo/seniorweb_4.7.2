﻿Imports System.Web.Hosting
Imports System.Data.OleDb


Partial Class Prospetto
    Inherits System.Web.UI.Page

    Dim MyTable As New System.Data.DataTable("tabella")
    Dim MyTable2 As New System.Data.DataTable("tabellaGriglia")
    Dim MyDataSet As New System.Data.DataSet()

    Dim MytabellaPersonalizzata As New System.Data.DataTable("tabellaPersonalizzata")

    Public Function GiorniMese(ByVal Mese As Byte, ByVal Anno As Integer) As Byte
        If Mese = 1 Or Mese = 3 Or Mese = 5 Or Mese = 7 Or Mese = 8 Or _
           Mese = 10 Or Mese = 12 Then
            GiorniMese = 31
        Else
            If Mese <> 2 Then
                GiorniMese = 30
            Else
                If Day(DateSerial(Anno, Mese, 29)) = 29 Then
                    GiorniMese = 29
                Else
                    GiorniMese = 28
                End If
            End If
        End If
    End Function
    Private Sub CaricaGriglia()
        Dim ConnectionString As String = Session("DC_OSPITE")
        Dim cn As OleDbConnection
        Dim x As Long

        cn = New Data.OleDb.OleDbConnection(ConnectionString)

        cn.Open()
        Dim TotaleGenerale As Long = 0
        Dim TotaleGeneraleCausali(100) As Long

        Dim VettoriCausali(100) As String
        Dim NumeroCausali As Integer
        Dim Anno As Integer
        Dim Mese As Integer
        Dim Giorni As Integer
        Dim I As Integer
        Dim MySql As String
        Dim PGiorni As Integer = 4
        Dim NonIndicatoCServ As Boolean = False
        Dim CondOspite As String

        Anno = Txt_Anno.Text
        Mese = Dd_Mese.SelectedValue
        Giorni = GiorniMese(Mese, Anno)


        Dim Parametri As New Cls_Parametri


        Parametri.LeggiParametri(Session("DC_OSPITE"))


        If Cmb_CServ.SelectedValue = "" Then
            NonIndicatoCServ = True
            MyTable2.Clear()
            MyTable2.Columns.Clear()
            MyTable2.Columns.Add("N", GetType(Long))
            MyTable2.Columns.Add("Nome", GetType(String))
            MyTable2.Columns.Add("Auto", GetType(String))
            MyTable2.Columns.Add("Ing", GetType(String))
            MyTable2.Columns.Add("Dec", GetType(String))


            MyTable.Clear()
            MyTable.Columns.Clear()
            MyTable.Columns.Add("N", GetType(Long))
            MyTable.Columns.Add("Nome", GetType(String))
            MyTable.Columns.Add("Auto", GetType(String))
            MyTable.Columns.Add("Ing", GetType(String))
            MyTable.Columns.Add("Dec", GetType(String))
            For I = 1 To Giorni
                MyTable.Columns.Add(I, GetType(String))
                MyTable2.Columns.Add(I, GetType(String))
            Next
            MyTable.Columns.Add("Totale", GetType(String))
            MyTable2.Columns.Add("Totale", GetType(String))

            I = 0

            MySql = "SELECT * FROM CAUSALI   Order by Descrizione"
            Dim cmdCau As New OleDbCommand()
            cmdCau.CommandText = MySql
            cmdCau.Connection = cn

            Dim MyCau As OleDbDataReader = cmdCau.ExecuteReader()
            Do While MyCau.Read
                MyTable.Columns.Add("(" & MyCau.Item("CODICE") & ") " & MyCau.Item("DESCRIZIONE"), GetType(String))
                MyTable2.Columns.Add("(" & MyCau.Item("CODICE") & ") " & MyCau.Item("DESCRIZIONE"), GetType(String))
                VettoriCausali(I) = MyCau.Item("CODICE")
                I = I + 1
            Loop
            MyCau.Close()
            NumeroCausali = I
        End If

        Dim CentroServizioIndicato As Boolean = True
        If Cmb_CServ.SelectedValue = "" Then

            CentroServizioIndicato = False
        End If

        CondOspite = ""
        If Txt_Ospite.Text.Trim <> "" Then
            Dim Vettore(100) As String

            Vettore = SplitWords(Txt_Ospite.Text)
            If Not IsNothing(Vettore(0)) Then
                If Val(Vettore(0)) > 0 Then
                    CondOspite = CondOspite & "  AnagraficaComune.CodiceOspite = " & Val(Vettore(0))
                End If
            End If
        End If


        Dim XCserv As Integer

        For XCserv = 0 To Cmb_CServ.Items.Count - 1



            Dim Cs As New Cls_CentroServizio

            If CentroServizioIndicato = False Then

                Cmb_CServ.SelectedIndex = XCserv
            End If

            Cs.CENTROSERVIZIO = Cmb_CServ.SelectedValue
            Cs.Leggi(Session("DC_OSPITE"), Cs.CENTROSERVIZIO)
            If Cs.TIPOCENTROSERVIZIO = "D" Then
                If NonIndicatoCServ = False Then
                    MyTable2.Clear()
                    MyTable2.Columns.Clear()
                    MyTable2.Columns.Add("N", GetType(Long))
                    MyTable2.Columns.Add("Nome", GetType(String))
                    MyTable2.Columns.Add("Auto", GetType(String))

                    PGiorni = 2
                    MyTable.Clear()
                    MyTable.Columns.Clear()
                    MyTable.Columns.Add("N", GetType(Long))
                    MyTable.Columns.Add("Nome", GetType(String))
                    MyTable.Columns.Add("Auto", GetType(String))
                    For I = 1 To Giorni
                        MyTable.Columns.Add(I, GetType(String))
                        MyTable2.Columns.Add(I, GetType(String))
                    Next
                    MyTable.Columns.Add("Totale", GetType(String))
                    MyTable2.Columns.Add("Totale", GetType(String))


                    I = 0

                    MySql = "SELECT * FROM CAUSALI  where diurno >0 Order by Descrizione"
                    Dim cmdCau As New OleDbCommand()
                    cmdCau.CommandText = MySql
                    cmdCau.Connection = cn

                    Dim MyCau As OleDbDataReader = cmdCau.ExecuteReader()
                    Do While MyCau.Read
                        MyTable.Columns.Add("(" & MyCau.Item("CODICE") & ") " & MyCau.Item("DESCRIZIONE"), GetType(String))
                        MyTable2.Columns.Add("(" & MyCau.Item("CODICE") & ") " & MyCau.Item("DESCRIZIONE"), GetType(String))
                        VettoriCausali(I) = MyCau.Item("CODICE")
                        I = I + 1
                    Loop
                    MyCau.Close()
                    NumeroCausali = I
                End If

                If CondOspite = "" Then
                    MySql = "SELECT movimenti.codiceospite  FROM AnagraficaComune  inner join  movimenti on movimenti.codiceospite = AnagraficaComune.CODICEOSPITE where  (AnagraficaComune.NonInUso IS Null OR  AnagraficaComune.NonInUso  = '') And  CENTROSERVIZIO = ? group by movimenti.CODICEOSPITE"
                Else
                    MySql = "SELECT movimenti.codiceospite  FROM AnagraficaComune  inner join  movimenti on movimenti.codiceospite = AnagraficaComune.CODICEOSPITE where  (AnagraficaComune.NonInUso IS Null OR  AnagraficaComune.NonInUso  = '') And  CENTROSERVIZIO = ? And " & CondOspite & " group by movimenti.CODICEOSPITE"
                End If

                Dim cmd As New OleDbCommand()
                cmd.CommandText = MySql
                cmd.Connection = cn

                cmd.Parameters.AddWithValue("@Cserv", Cmb_CServ.SelectedValue)
                Dim myPOSTreader As OleDbDataReader = cmd.ExecuteReader()
                Do While myPOSTreader.Read

                    Dim AccoltoNelMese As Integer = 0
                    Dim Mov As New Cls_Movimenti

                    Mov.CodiceOspite = myPOSTreader.Item("CodiceOspite")
                    Mov.CENTROSERVIZIO = Cmb_CServ.SelectedValue
                    Mov.UltimaDataAccoglimento(Session("DC_OSPITE"))
                    If Month(Mov.Data) = Mese And Year(Mov.Data) = Anno Then
                        AccoltoNelMese = Day(Mov.Data)
                    End If

                    If Mov.CodiceOspite = 22 Then
                        Mov.CodiceOspite = 22
                    End If

                    Dim UscitiNelMese As Integer = 0
                    Dim MovU As New Cls_Movimenti

                    MovU.CodiceOspite = myPOSTreader.Item("CodiceOspite")
                    MovU.CENTROSERVIZIO = Cmb_CServ.SelectedValue
                    MovU.UltimaDataUscitaDefinitivaMese(Session("DC_OSPITE"), Anno, Mese)
                    If Month(MovU.Data) = Mese And Year(MovU.Data) = Anno Then
                        UscitiNelMese = Day(MovU.Data)
                    End If

                    Dim cmd1 As New OleDbCommand()
                    cmd1.CommandText = "Select * From AssenzeCentroDiurno where [CENTROSERVIZIO] = ? and  [CODICEOSPITE] = ? and anno = ? and mese = ?"
                    cmd1.Connection = cn

                    cmd1.Parameters.AddWithValue("@Cserv", Cmb_CServ.SelectedValue)
                    cmd1.Parameters.AddWithValue("@CodiceOspite", myPOSTreader.Item("CodiceOspite"))
                    cmd1.Parameters.AddWithValue("@Anno", Anno)
                    cmd1.Parameters.AddWithValue("@Mese", Mese)

                    Dim Read1 As OleDbDataReader = cmd1.ExecuteReader()
                    If Read1.Read Then
                        Dim myriga As System.Data.DataRow = MyTable.NewRow()

                        Dim K As New ClsOspite

                        K.CodiceOspite = myPOSTreader.Item("CodiceOspite")
                        K.Leggi(Session("DC_OSPITE"), K.CodiceOspite)


                        myriga(0) = K.CodiceOspite
                        myriga(1) = " " & K.Nome

                        For I = 1 To Giorni

                            myriga(I + PGiorni) = campodb(Read1.Item("GIORNO" & I))

                            If AccoltoNelMese > 0 Then
                                If AccoltoNelMese > I Then
                                    myriga(I + PGiorni) = "C"
                                End If
                            End If
                            If UscitiNelMese > 0 Then
                                If UscitiNelMese < I Then
                                    myriga(I + PGiorni) = "C"
                                End If
                            End If
                        Next
                        MyTable.Rows.Add(myriga)
                    Else
                        Dim Presente As New Cls_Movimenti

                        Presente.CENTROSERVIZIO = Cmb_CServ.SelectedValue
                        Presente.CodiceOspite = myPOSTreader.Item("CodiceOspite")
                        Presente.TipoMov = ""
                        Presente.UltimaMovimentoPrimaData(Session("DC_OSPITE"), Presente.CodiceOspite, Presente.CENTROSERVIZIO, DateSerial(Anno, Mese, 1))

                        If Presente.TipoMov <> "13" And Presente.TipoMov <> "" Then
                            Dim myriga As System.Data.DataRow = MyTable.NewRow()

                            Dim Kx As New ClsOspite

                            Kx.CodiceOspite = myPOSTreader.Item("CodiceOspite")
                            Kx.Leggi(Session("DC_OSPITE"), Kx.CodiceOspite)


                            myriga(0) = Kx.CodiceOspite
                            myriga(1) = " " & Kx.Nome


                            Dim TbCServ As New Cls_CentroServizio
                            Dim SETTIMANA As String
                            Dim Causale(32) As String

                            For I = 1 To 31
                                Causale(I) = ""
                            Next

                            Dim Gg As Integer
                            TbCServ.Leggi(Session("DC_OSPITE"), Cs.CENTROSERVIZIO)

                            Dim AnaOsp As New ClsOspite

                            AnaOsp.Leggi(Session("DC_OSPITE"), Presente.CodiceOspite)

                            Dim K As New Cls_DatiOspiteParenteCentroServizio


                            K.CentroServizio = Cs.CENTROSERVIZIO
                            K.CodiceOspite = Presente.CodiceOspite
                            K.Leggi(Session("DC_OSPITE"))
                            If K.TipoOperazione <> "" Then
                                AnaOsp.SETTIMANA = K.Settimana
                            End If



                            SETTIMANA = AnaOsp.SETTIMANA


                            For I = 1 To GiorniMese(Mese, Anno)
                                Gg = Weekday(DateSerial(Anno, Mese, I), vbMonday)
                                If Trim(Mid(SETTIMANA, Gg, 1)) <> "" Then
                                    Causale(I) = Mid(SETTIMANA, Gg, 1)
                                End If
                            Next I


                            SETTIMANA = TbCServ.SETTIMANA
                            For I = 1 To GiorniMese(Mese, Anno)
                                Gg = Weekday(DateSerial(Anno, Mese, I), vbMonday)
                                If Trim(Mid(SETTIMANA, Gg, 1)) <> "" Then
                                    Causale(I) = "C"
                                End If
                            Next I


                            If Mese = 1 Then
                                If TbCServ.GIORNO0101 <> "S" Then
                                    Causale(1) = "C"
                                End If
                                If TbCServ.GIORNO0601 <> "S" Then
                                    Causale(6) = "C"
                                End If
                            End If
                            If Mese = 3 Then
                                If TbCServ.GIORNO1903 <> "S" Then
                                    Causale(19) = "C"
                                End If
                            End If
                            If Mese = 4 Then
                                If TbCServ.GIORNO2504 <> "S" Then
                                    Causale(25) = "C"
                                End If
                            End If
                            If Mese = 5 Then
                                If TbCServ.GIORNO0105 <> "S" Then
                                    Causale(1) = "C"
                                End If
                            End If

                            If Mese = 6 Then
                                If TbCServ.GIORNO0206 <> "S" Then
                                    Causale(2) = "C"
                                End If
                                If TbCServ.GIORNO2906 <> "S" Then
                                    Causale(29) = "C"
                                End If
                            End If
                            If Mese = 8 Then
                                If TbCServ.GIORNO1508 <> "S" Then
                                    Causale(15) = "C"
                                End If
                            End If
                            If Mese = 11 Then
                                If TbCServ.GIORNO0111 <> "S" Then
                                    Causale(1) = "C"
                                End If
                                If TbCServ.GIORNO0411 <> "S" Then
                                    Causale(4) = "C"
                                End If
                            End If
                            If Mese = 12 Then
                                If TbCServ.GIORNO0812 <> "S" Then
                                    Causale(8) = "C"
                                End If
                                If TbCServ.GIORNO2512 <> "S" Then
                                    Causale(25) = "C"
                                End If
                                If TbCServ.GIORNO2612 <> "S" Then
                                    Causale(26) = "C"
                                End If
                            End If
                            If TbCServ.GIORNO1ATTIVO <> "S" Then
                                If Not IsDBNull(TbCServ.GIORNO1) Then
                                    If Val(Mid(TbCServ.GIORNO1, 4, 2)) = Mese Then
                                        Causale(Val(Mid(TbCServ.GIORNO1, 1, 2))) = "C"
                                    End If
                                End If
                            End If

                            If TbCServ.GIORNO2ATTIVO <> "S" Then
                                If Not IsDBNull(TbCServ.GIORNO2) Then
                                    If Val(Mid(TbCServ.GIORNO2, 4, 2)) = Mese Then
                                        Causale(Val(Mid(TbCServ.GIORNO2, 1, 2))) = "C"
                                    End If
                                End If
                            End If

                            If TbCServ.GIORNO3ATTIVO <> "S" Then
                                If Not IsDBNull(TbCServ.GIORNO3) Then
                                    If Val(Mid(TbCServ.GIORNO3, 4, 2)) = Mese Then
                                        Causale(Val(Mid(TbCServ.GIORNO3, 1, 2))) = "C"
                                    End If
                                End If
                            End If

                            If TbCServ.GIORNO4ATTIVO <> "S" Then
                                If Not IsDBNull(TbCServ.GIORNO4) Then
                                    If Val(Mid(TbCServ.GIORNO4, 4, 2)) = Mese Then
                                        Causale(Val(Mid(TbCServ.GIORNO4, 1, 2))) = "C"
                                    End If
                                End If
                            End If

                            If TbCServ.GIORNO5ATTIVO <> "S" Then
                                If Not IsDBNull(TbCServ.GIORNO5) Then
                                    If Val(Mid(TbCServ.GIORNO5, 4, 2)) = Mese Then
                                        Causale(Val(Mid(TbCServ.GIORNO5, 1, 2))) = "C"
                                    End If
                                End If
                            End If


                            For I = 1 To 31
                                myriga(I + PGiorni) = Causale(I)
                            Next

                            MyTable.Rows.Add(myriga)
                        End If
                    End If
                    Read1.Close()
                Loop
                myPOSTreader.Close()


                'Dim x1 As Integer
                'Dim Totale As Long
                'Dim Totale2 As Long
                'Dim TotaleCausali(100) As Long
                'Dim IndI As Integer


                'For I = 0 To MyTable.Rows.Count - 1
                '    Dim pr As New Cls_StatoAuto

                '    pr.UltimaData(ConnectionString, MyTable.Rows(I).Item(0), Cmb_CServ.SelectedValue)
                '    MyTable.Rows(I).Item(2) = pr.STATOAUTO
                '    MyTable.Rows(I).Item(0) = I + 1

                '    Totale = 0
                '    Totale2 = 0

                '    For IndI = 0 To 100
                '        TotaleCausali(IndI) = 0
                '    Next
                '    For x1 = 1 To Giorni
                '        If IsDBNull(MyTable.Rows(I).Item(x1 + PGiorni)) Then
                '        Else


                '            If Trim(MyTable.Rows(I).Item(x1 + PGiorni)) = "" Then
                '                Totale = Totale + 1
                '            End If

                '            For IndI = 0 To NumeroCausali - 1
                '                If Trim(MyTable.Rows(I).Item(x1 + PGiorni)) = VettoriCausali(IndI) Then
                '                    TotaleCausali(IndI) = TotaleCausali(IndI) + 1
                '                End If
                '            Next
                '        End If
                '    Next
                '    MyTable.Rows(I).Item(Giorni + 1 + PGiorni) = Totale
                '    For IndI = 0 To NumeroCausali - 1
                '        MyTable.Rows(I).Item(Giorni + IndI + 2 + PGiorni) = TotaleCausali(IndI)
                '        TotaleGeneraleCausali(IndI) = TotaleGeneraleCausali(IndI) + TotaleCausali(IndI)
                '    Next
                '    TotaleGenerale = TotaleGenerale + Totale
                'Next

            Else
                If NonIndicatoCServ = False Then
                    MyTable2.Clear()
                    MyTable2.Columns.Clear()
                    MyTable2.Columns.Add("N", GetType(Long))
                    MyTable2.Columns.Add("Nome", GetType(String))
                    MyTable2.Columns.Add("Auto", GetType(String))
                    MyTable2.Columns.Add("Ing", GetType(String))
                    MyTable2.Columns.Add("Dec", GetType(String))


                    MyTable.Clear()
                    MyTable.Columns.Clear()
                    MyTable.Columns.Add("N", GetType(Long))
                    MyTable.Columns.Add("Nome", GetType(String))
                    MyTable.Columns.Add("Auto", GetType(String))
                    MyTable.Columns.Add("Ing", GetType(String))
                    MyTable.Columns.Add("Dec", GetType(String))
                    For I = 1 To Giorni
                        MyTable.Columns.Add(I, GetType(String))
                        MyTable2.Columns.Add(I, GetType(String))
                    Next
                    MyTable.Columns.Add("Totale", GetType(String))
                    MyTable2.Columns.Add("Totale", GetType(String))

                    I = 0

                    MySql = "SELECT * FROM CAUSALI  where diurno  = 0 Order by Descrizione"
                    Dim cmdCau As New OleDbCommand()
                    cmdCau.CommandText = MySql
                    cmdCau.Connection = cn

                    Dim MyCau As OleDbDataReader = cmdCau.ExecuteReader()
                    Do While MyCau.Read
                        MyTable.Columns.Add("(" & MyCau.Item("CODICE") & ") " & MyCau.Item("DESCRIZIONE"), GetType(String))
                        MyTable2.Columns.Add("(" & MyCau.Item("CODICE") & ") " & MyCau.Item("DESCRIZIONE"), GetType(String))
                        VettoriCausali(I) = MyCau.Item("CODICE")
                        I = I + 1
                    Loop
                    MyCau.Close()
                    NumeroCausali = I
                End If

                For I = 1 To Giorni
                    If CondOspite = "" Then
                        MySql = "SELECT codiceospite,nome,(select top 1 TIPOMOV from Movimenti Where movimenti.codiceospite = anagraficacomune.codiceospite and data <= ? and CENTROSERVIZIO = ? order by data desc,PROGRESSIVO )  as tipo,(select top 1 Data from Movimenti Where movimenti.codiceospite = anagraficacomune.codiceospite and data <= ? and CENTROSERVIZIO = ? order by data desc,PROGRESSIVO )  as Data,(select top 1 CAUSALE  from Movimenti Where movimenti.codiceospite = anagraficacomune.codiceospite and data <= ? and CENTROSERVIZIO = ? order by data desc,PROGRESSIVO )  as CAUSALE"
                        MySql = MySql & " FROM AnagraficaComune "
                        MySql = MySql & "where CodiceParente = 0 And (select top 1 TIPOMOV from Movimenti Where movimenti.codiceospite = anagraficacomune.codiceospite  and ((data < ?  AND TIPOMOV  = '13') OR (data <= ?  AND (TIPOMOV  = '05' OR TIPOMOV  = '03' OR TIPOMOV  = '04'))) and CENTROSERVIZIO = ? order by data desc,PROGRESSIVO desc ) <> '13' And (AnagraficaComune.NonInUso IS Null OR  AnagraficaComune.NonInUso  = '')"
                    Else
                        MySql = "SELECT codiceospite,nome,(select top 1 TIPOMOV from Movimenti Where movimenti.codiceospite = anagraficacomune.codiceospite and data <= ? and CENTROSERVIZIO = ? order by data desc,PROGRESSIVO )  as tipo,(select top 1 Data from Movimenti Where movimenti.codiceospite = anagraficacomune.codiceospite and data <= ? and CENTROSERVIZIO = ? order by data desc,PROGRESSIVO )  as Data,(select top 1 CAUSALE  from Movimenti Where movimenti.codiceospite = anagraficacomune.codiceospite and data <= ? and CENTROSERVIZIO = ? order by data desc,PROGRESSIVO )  as CAUSALE"
                        MySql = MySql & " FROM AnagraficaComune "
                        MySql = MySql & "where CodiceParente = 0 And (select top 1 TIPOMOV from Movimenti Where movimenti.codiceospite = anagraficacomune.codiceospite  and ((data < ?  AND TIPOMOV  = '13') OR (data <= ?  AND (TIPOMOV  = '05' OR TIPOMOV  = '03' OR TIPOMOV  = '04'))) and CENTROSERVIZIO = ? order by data desc,PROGRESSIVO desc ) <> '13' And (AnagraficaComune.NonInUso IS Null OR  AnagraficaComune.NonInUso  = '') And " & CondOspite
                    End If

                    Dim cmd As New OleDbCommand()
                    cmd.CommandText = MySql
                    cmd.Connection = cn
                    cmd.Parameters.AddWithValue("@Data", DateSerial(Anno, Mese, I))
                    cmd.Parameters.AddWithValue("@Cserv", Cmb_CServ.SelectedValue)
                    cmd.Parameters.AddWithValue("@Data", DateSerial(Anno, Mese, I))
                    cmd.Parameters.AddWithValue("@Cserv", Cmb_CServ.SelectedValue)
                    cmd.Parameters.AddWithValue("@Data", DateSerial(Anno, Mese, I))
                    cmd.Parameters.AddWithValue("@Cserv", Cmb_CServ.SelectedValue)
                    cmd.Parameters.AddWithValue("@Data", DateSerial(Anno, Mese, I))
                    cmd.Parameters.AddWithValue("@Data", DateSerial(Anno, Mese, I))
                    cmd.Parameters.AddWithValue("@Cserv", Cmb_CServ.SelectedValue)
                    Dim myPOSTreader As OleDbDataReader = cmd.ExecuteReader()
                    Do While myPOSTreader.Read
                        Dim OspFind As Boolean
                        If myPOSTreader.Item("CodiceOspite") = 513 And I = 14 Then
                            OspFind = False
                        End If

                        If myPOSTreader.Item("CodiceOspite") = 22 Then
                            OspFind = False
                        End If

                        OspFind = False
                        For x = 0 To MyTable.Rows.Count - 1
                            If MyTable.Rows(x).Item(0) = myPOSTreader.Item("CodiceOspite") Then
                                OspFind = True
                                If campodb(myPOSTreader.Item("tipo")) = "05" Then
                                    If campodbD(myPOSTreader.Item("data")) = DateSerial(Anno, Mese, I) Then
                                        MyTable.Rows(x).Item(I + PGiorni) = ""
                                    Else
                                        MyTable.Rows(x).Item(I + PGiorni) = ""
                                    End If
                                End If
                                If campodb(myPOSTreader.Item("tipo")) = "13" Then
                                    If campodbD(myPOSTreader.Item("data")) = DateSerial(Anno, Mese, I) Then
                                        MyTable.Rows(x).Item(I + PGiorni) = myPOSTreader.Item("CAUSALE")  '"U"
                                        MyTable.Rows(x).Item(4) = I
                                    End If
                                End If
                                If campodb(myPOSTreader.Item("tipo")) = "04" Then
                                    MyTable.Rows(x).Item(I + PGiorni) = ""
                                End If
                                If campodb(myPOSTreader.Item("tipo")) = "03" Then
                                    MyTable.Rows(x).Item(I + PGiorni) = myPOSTreader.Item("CAUSALE")  '"A"
                                    If campodb(myPOSTreader.Item("CAUSALE")) = "01" Or campodb(myPOSTreader.Item("CAUSALE")) = "03" Then
                                        MyTable.Rows(x).Item(I + PGiorni) = myPOSTreader.Item("CAUSALE")  '"O"
                                    End If
                                End If
                            End If
                        Next
                        If OspFind = False Then
                            Dim myriga As System.Data.DataRow = MyTable.NewRow()

                            myriga(0) = myPOSTreader.Item("CodiceOspite")
                            myriga(1) = " " & myPOSTreader.Item("Nome")
                            If campodb(myPOSTreader.Item("tipo")) = "05" Then
                                If myPOSTreader.Item("data") = DateSerial(Anno, Mese, I) Then
                                    myriga(3) = I
                                    myriga(I + PGiorni) = ""
                                Else
                                    myriga(I + PGiorni) = ""
                                End If
                            End If
                            If campodb(myPOSTreader.Item("tipo")) = "13" Then
                                If campodbD(myPOSTreader.Item("data")) = DateSerial(Anno, Mese, I) Then
                                    myriga(I + PGiorni) = campodb(myPOSTreader.Item("CAUSALE"))  '"U"
                                End If
                            End If
                            If campodb(myPOSTreader.Item("tipo")) = "04" Then
                                myriga(I + PGiorni) = ""
                            End If
                            If campodb(myPOSTreader.Item("tipo")) = "03" Then
                                myriga(I + PGiorni) = campodb(myPOSTreader.Item("CAUSALE"))  '"A"
                                If campodb(myPOSTreader.Item("CAUSALE")) = "01" Or campodb(myPOSTreader.Item("CAUSALE")) = "03" Then
                                    myriga(I + PGiorni) = campodb(myPOSTreader.Item("CAUSALE"))  '"O"
                                End If
                            End If
                            MyTable.Rows.Add(myriga)
                        End If
                    Loop
                    myPOSTreader.Close()
                Next

            End If
            If CentroServizioIndicato = True Then
                Exit For
            End If
        Next

        Call TabTotaleGenerale(MyTable, VettoriCausali, TotaleGeneraleCausali, ConnectionString, Giorni, PGiorni, NumeroCausali, TotaleGenerale)

        MyTable.DefaultView.Sort = "Nome Asc"

        Dim xCol As Integer
        For I = 0 To MyTable.Rows.Count - 1
            Dim myriga2 As System.Data.DataRow = MyTable2.NewRow()
            For xCol = 0 To MyTable.Columns.Count - 1

                myriga2.Item(xCol) = MyTable.Rows(I).Item(xCol)
            Next
            MyTable2.Rows.Add(myriga2)
        Next
        Dim TotaleAssoluto As Integer = 0

        If chk_PresenzeAssenze.Checked = False Then

            Dim myriga1 As System.Data.DataRow = MyTable.NewRow()
            myriga1.Item(1) = "Totale :"
            myriga1.Item(Giorni + 1 + PGiorni) = TotaleGenerale
            If NumeroCausali > 0 Then
                For IndI = 0 To NumeroCausali - 1
                    If VettoriCausali(IndI) = "95" Or VettoriCausali(IndI) = "96" Or VettoriCausali(IndI) = "97" Or VettoriCausali(IndI) = "98" Or VettoriCausali(IndI) = "99" Then
                        TotaleAssoluto = TotaleAssoluto + TotaleGeneraleCausali(IndI)
                    End If
                    myriga1.Item(Giorni + IndI + 2 + PGiorni) = TotaleGeneraleCausali(IndI)
                Next
            End If
            MyTable.Rows.Add(myriga1)
        End If

        If chk_PresenzeAssenze.Checked = False Then
            Dim Cs1 As New Cls_CentroServizio

            If CentroServizioIndicato = True Then

                Cs1.CENTROSERVIZIO = Cmb_CServ.SelectedValue
                Cs1.Leggi(Session("DC_OSPITE"), Cs1.CENTROSERVIZIO)

                If Cs1.TIPOCENTROSERVIZIO = "" Then
                    If Session("DC_OSPITE").ToString.ToUpper.IndexOf("CREA") > 0 Then
                        Dim myriga3 As System.Data.DataRow = MyTable.NewRow()
                        myriga3.Item(1) = "Totale Raggruppato :"
                        myriga3.Item(Giorni + 1 + PGiorni) = TotaleGenerale + TotaleAssoluto
                        MyTable.Rows.Add(myriga3)
                    End If
                End If
            End If
        End If

        Dim TotPresenze As Integer = 0
        Dim TotAssenze As Integer = 0
        Dim TotPresenzeAss As Integer = 0
        Dim TotAssenzeAss As Integer = 0

        Dim TotPresenzeAssPt As Integer = 0
        Dim TotAssenzeAssPt As Integer = 0

        If chk_PresenzeAssenzePartime.Checked = True Then
            MytabellaPersonalizzata.Clear()
            MytabellaPersonalizzata.Columns.Clear()
            MytabellaPersonalizzata.Columns.Add("Nome", GetType(String))
            For I = 1 To GiorniMese(Mese, Anno)
                MytabellaPersonalizzata.Columns.Add(I, GetType(String))
            Next
            MytabellaPersonalizzata.Columns.Add("Tot. p. ft.", GetType(String))
            MytabellaPersonalizzata.Columns.Add("Tot. a. ft.", GetType(String))

            MytabellaPersonalizzata.Columns.Add("Tot. p. pt.", GetType(String))
            MytabellaPersonalizzata.Columns.Add("Tot. a. pt.", GetType(String))

            TotPresenzeAss = 0
            TotAssenzeAss = 0

            MyTable.DefaultView.Sort = "Nome Asc"

            Dim MyTabellaAppoggio As New System.Data.DataTable("TabellaAppoggio")
            MyTabellaAppoggio = MyTable.DefaultView.ToTable()


            For I = 0 To MyTabellaAppoggio.Rows.Count - 2

                TotPresenze = 0
                TotAssenze = 0

                Dim myrigaP As System.Data.DataRow = MytabellaPersonalizzata.NewRow()
                myrigaP(0) = MyTabellaAppoggio.Rows(I).Item(1)
                For xCol = 1 To GiorniMese(Mese, Anno)
                    Dim Tipo As String = ""
                    Dim Sommatore As Integer = 4

                    Dim MyCserv As New Cls_CentroServizio

                    MyCserv.CENTROSERVIZIO = Cmb_CServ.SelectedValue
                    MyCserv.Leggi(Session("DC_OSPITE"), MyCserv.CENTROSERVIZIO)

                    If MyCserv.TIPOCENTROSERVIZIO = "D" Then
                        Sommatore = 2
                    End If

                    If Trim(campodb(MyTabellaAppoggio.Rows(I).Item(xCol + Sommatore))) = "" Then
                        Tipo = "P"
                    End If

                    If MyCserv.CausalePartTime = Trim(campodb(MyTabellaAppoggio.Rows(I).Item(xCol + Sommatore))) Then
                        Tipo = ""
                    End If
                    If MyCserv.CausaleTempoPieno = Trim(campodb(MyTabellaAppoggio.Rows(I).Item(xCol + Sommatore))) Then
                        Tipo = "P"
                    End If

                    If MyCserv.CausaleChiuso = Trim(campodb(MyTabellaAppoggio.Rows(I).Item(xCol + Sommatore))) Then
                        Tipo = "C"
                    End If

                    If Trim(campodb(MyTabellaAppoggio.Rows(I).Item(xCol + Sommatore))) = "C" Then
                        Tipo = "C"
                    End If



                    If Tipo = "P" Then
                        myrigaP(xCol) = "1"
                        TotPresenze = TotPresenze + 1
                        TotPresenzeAss = TotPresenzeAss + 1
                    Else
                        myrigaP(xCol) = ""
                    End If
                Next
                myrigaP(xCol) = TotPresenze

                MytabellaPersonalizzata.Rows.Add(myrigaP)

                TotPresenze = 0
                TotAssenze = 0

                Dim myrigaa As System.Data.DataRow = MytabellaPersonalizzata.NewRow()
                'myrigaa(0) = MyTabellaAppoggio.Rows(I).Item(1)
                For xCol = 1 To GiorniMese(Mese, Anno)
                    Dim Tipo As String = ""
                    Dim Sommatore As Integer = 4

                    Dim MyCserv As New Cls_CentroServizio

                    MyCserv.CENTROSERVIZIO = Cmb_CServ.SelectedValue
                    MyCserv.Leggi(Session("DC_OSPITE"), MyCserv.CENTROSERVIZIO)

                    If MyCserv.TIPOCENTROSERVIZIO = "D" Then
                        Sommatore = 2
                    End If

                    If Trim(campodb(MyTabellaAppoggio.Rows(I).Item(xCol + Sommatore))) = "" Then
                        Tipo = "P"
                    End If

                    If MyCserv.CausalePartTime = Trim(campodb(MyTabellaAppoggio.Rows(I).Item(xCol + Sommatore))) Then
                        Tipo = "P"
                    End If
                    If MyCserv.CausaleTempoPieno = Trim(campodb(MyTabellaAppoggio.Rows(I).Item(xCol + Sommatore))) Then
                        Tipo = "P"
                    End If

                    If MyCserv.CausalePartTimeAss = Trim(campodb(MyTabellaAppoggio.Rows(I).Item(xCol + Sommatore))) Then
                        Tipo = "P"
                    End If
                    If MyCserv.CausalePartTimeAss1 = Trim(campodb(MyTabellaAppoggio.Rows(I).Item(xCol + Sommatore))) Then
                        Tipo = "P"
                    End If
                    If MyCserv.CausalePartTimeAss2 = Trim(campodb(MyTabellaAppoggio.Rows(I).Item(xCol + Sommatore))) Then
                        Tipo = "P"
                    End If

                    If MyCserv.CausaleTempoPienoAss = Trim(campodb(MyTabellaAppoggio.Rows(I).Item(xCol + Sommatore))) Then
                        Tipo = "A"
                    End If
                    If MyCserv.CausaleTempoPienoAss1 = Trim(campodb(MyTabellaAppoggio.Rows(I).Item(xCol + Sommatore))) Then
                        Tipo = "A"
                    End If
                    If MyCserv.CausaleTempoPienoAss2 = Trim(campodb(MyTabellaAppoggio.Rows(I).Item(xCol + Sommatore))) Then
                        Tipo = "A"
                    End If

                    If MyCserv.CausaleChiuso = Trim(campodb(MyTabellaAppoggio.Rows(I).Item(xCol + Sommatore))) Then
                        Tipo = "C"
                    End If

                    If Trim(campodb(MyTabellaAppoggio.Rows(I).Item(xCol + Sommatore))) = "C" Then
                        Tipo = "C"
                    End If


                    If Tipo = "A" Then
                        myrigaa(xCol) = "1"
                        TotAssenze = TotAssenze + 1
                        TotAssenzeAss = TotAssenzeAss + 1
                    Else
                        myrigaa(xCol) = ""
                    End If
                Next
                myrigaa(xCol + 1) = TotAssenze
                MytabellaPersonalizzata.Rows.Add(myrigaa)


                TotPresenze = 0
                TotAssenze = 0

                Dim myrigappt As System.Data.DataRow = MytabellaPersonalizzata.NewRow()
                'myrigappt(0) = MyTabellaAppoggio.Rows(I).Item(1)
                For xCol = 1 To GiorniMese(Mese, Anno)
                    Dim Tipo As String = ""
                    Dim Sommatore As Integer = 4

                    Dim MyCserv As New Cls_CentroServizio

                    MyCserv.CENTROSERVIZIO = Cmb_CServ.SelectedValue
                    MyCserv.Leggi(Session("DC_OSPITE"), MyCserv.CENTROSERVIZIO)

                    If MyCserv.TIPOCENTROSERVIZIO = "D" Then
                        Sommatore = 2
                    End If

                    If Trim(campodb(MyTabellaAppoggio.Rows(I).Item(xCol + Sommatore))) = "" Then
                        Tipo = ""
                    End If

                    If MyCserv.CausalePartTime = Trim(campodb(MyTabellaAppoggio.Rows(I).Item(xCol + Sommatore))) Then
                        Tipo = "P"
                    End If
                    If MyCserv.CausaleTempoPieno = Trim(campodb(MyTabellaAppoggio.Rows(I).Item(xCol + Sommatore))) Then
                        Tipo = ""
                    End If

                    If MyCserv.CausaleChiuso = Trim(campodb(MyTabellaAppoggio.Rows(I).Item(xCol + Sommatore))) Then
                        Tipo = "C"
                    End If

                    If Trim(campodb(MyTabellaAppoggio.Rows(I).Item(xCol + Sommatore))) = "C" Then
                        Tipo = "C"
                    End If


                    If Tipo = "P" Then
                        myrigappt(xCol) = "1"
                        TotPresenze = TotPresenze + 1
                        TotPresenzeAssPt = TotPresenzeAssPt + 1
                    Else
                        myrigappt(xCol) = ""
                    End If
                Next
                myrigappt(xCol + 2) = TotPresenze
                MytabellaPersonalizzata.Rows.Add(myrigappt)



                TotPresenze = 0
                TotAssenze = 0

                Dim myrigappta As System.Data.DataRow = MytabellaPersonalizzata.NewRow()
                '                myrigappta(0) = MyTabellaAppoggio.Rows(I).Item(1)
                For xCol = 1 To GiorniMese(Mese, Anno)
                    Dim Tipo As String = ""
                    Dim Sommatore As Integer = 4

                    Dim MyCserv As New Cls_CentroServizio

                    MyCserv.CENTROSERVIZIO = Cmb_CServ.SelectedValue
                    MyCserv.Leggi(Session("DC_OSPITE"), MyCserv.CENTROSERVIZIO)

                    If MyCserv.TIPOCENTROSERVIZIO = "D" Then
                        Sommatore = 2
                    End If

                    If Trim(campodb(MyTabellaAppoggio.Rows(I).Item(xCol + Sommatore))) = "" Then
                        Tipo = ""
                    End If

                    If MyCserv.CausalePartTime = Trim(campodb(MyTabellaAppoggio.Rows(I).Item(xCol + Sommatore))) And MyCserv.CausalePartTime <> "" Then
                        Tipo = "P"
                    End If
                    If MyCserv.CausaleTempoPieno = Trim(campodb(MyTabellaAppoggio.Rows(I).Item(xCol + Sommatore))) And MyCserv.CausaleTempoPieno <> "" Then
                        Tipo = "P"
                    End If

                    If MyCserv.CausalePartTimeAss = Trim(campodb(MyTabellaAppoggio.Rows(I).Item(xCol + Sommatore))) Then
                        Tipo = "A"
                    End If
                    If MyCserv.CausalePartTimeAss1 = Trim(campodb(MyTabellaAppoggio.Rows(I).Item(xCol + Sommatore))) And MyCserv.CausalePartTimeAss1 <> "" Then
                        Tipo = "A"
                    End If
                    If MyCserv.CausalePartTimeAss2 = Trim(campodb(MyTabellaAppoggio.Rows(I).Item(xCol + Sommatore))) And MyCserv.CausalePartTimeAss2 <> "" Then
                        Tipo = "A"
                    End If

                    If MyCserv.CausaleChiuso = Trim(campodb(MyTabellaAppoggio.Rows(I).Item(xCol + Sommatore))) Then
                        Tipo = "C"
                    End If

                    If Trim(campodb(MyTabellaAppoggio.Rows(I).Item(xCol + Sommatore))) = "C" Then
                        Tipo = "C"
                    End If



                    If Tipo = "A" Then
                        myrigappta(xCol) = "1"
                        TotAssenze = TotAssenze + 1
                        TotAssenzeAssPt = TotAssenzeAssPt + 1
                    Else
                        myrigappta(xCol) = ""
                    End If

                Next
                myrigappta(xCol + 3) = TotAssenze
                MytabellaPersonalizzata.Rows.Add(myrigappta)
            Next

            Dim myriga1 As System.Data.DataRow = MytabellaPersonalizzata.NewRow()
            myriga1.Item(0) = "Totale :"
            myriga1.Item(xCol) = TotPresenzeAss
            myriga1.Item(xCol + 1) = TotAssenzeAss
            myriga1.Item(xCol + 2) = TotPresenzeAssPt
            myriga1.Item(xCol + 3) = TotAssenzeAssPt
            MytabellaPersonalizzata.Rows.Add(myriga1)

            'MytabellaPersonalizzata.DefaultView.Sort = "Nome Asc"
            MyTable = MytabellaPersonalizzata

        End If

        If chk_PresenzeAssenze.Checked = True Then

            Dim MyTabellaAppoggio2 As New System.Data.DataTable("TabellaAppoggio2")
            MyTabellaAppoggio2 = MyTable.DefaultView.ToTable()

            MytabellaPersonalizzata.Clear()
            MytabellaPersonalizzata.Columns.Clear()
            MytabellaPersonalizzata.Columns.Add("Nome", GetType(String))
            For I = 1 To GiorniMese(Mese, Anno)
                MytabellaPersonalizzata.Columns.Add(I, GetType(String))
            Next
            MytabellaPersonalizzata.Columns.Add("Totale Presenze", GetType(String))
            MytabellaPersonalizzata.Columns.Add("Totale Assenze", GetType(String))

            TotPresenzeAss = 0
            TotAssenzeAss = 0


            For I = 0 To MyTabellaAppoggio2.Rows.Count - 1
                TotPresenze = 0
                TotAssenze = 0




                Dim myriga3 As System.Data.DataRow = MytabellaPersonalizzata.NewRow()
                myriga3(0) = MyTabellaAppoggio2.Rows(I).Item(1)

                If myriga3(0).ToString.IndexOf("Rebecchi Milena") > 0 Then
                    TotAssenze = 0
                End If

                For xCol = 1 To GiorniMese(Mese, Anno)
                    Dim Tipo As String = ""
                    Dim Successivo As String = ""
                    Dim Sommatore As Integer = 4

                    Dim MyCserv As New Cls_CentroServizio

                    MyCserv.CENTROSERVIZIO = Cmb_CServ.SelectedValue
                    MyCserv.Leggi(Session("DC_OSPITE"), MyCserv.CENTROSERVIZIO)

                    If MyCserv.TIPOCENTROSERVIZIO = "D" Then
                        Sommatore = 2
                    End If

                    If Trim(campodb(MyTabellaAppoggio2.Rows(I).Item(xCol + Sommatore))) = "" Then
                        Tipo = "P"
                    End If

                    If MyCserv.CausalePartTime = Trim(campodb(MyTabellaAppoggio2.Rows(I).Item(xCol + Sommatore))) Then
                        Tipo = "P"
                    End If
                    If MyCserv.CausaleTempoPieno = Trim(campodb(MyTabellaAppoggio2.Rows(I).Item(xCol + Sommatore))) Then
                        Tipo = "P"
                    End If

                    If Trim(campodb(MyTabellaAppoggio2.Rows(I).Item(xCol + Sommatore))) = "C" Then
                        Tipo = "C"
                    End If
                    If Trim(campodb(MyTabellaAppoggio2.Rows(I).Item(xCol + Sommatore))) <> "" And Trim(campodb(MyTabellaAppoggio2.Rows(I).Item(xCol + Sommatore))) <> "*" Then
                        Dim MyCausale As New Cls_CausaliEntrataUscita

                        MyCausale.Codice = Trim(campodb(MyTabellaAppoggio2.Rows(I).Item(xCol + Sommatore)))
                        MyCausale.LeggiCausale(Session("DC_OSPITE"))
                        If Trim(campodb(MyTabellaAppoggio2.Rows(I).Item(xCol + Sommatore))) <> Trim(campodb(MyTabellaAppoggio2.Rows(I).Item(xCol + Sommatore - 1))) Then
                            If MyCausale.GiornoUscita = "N" Then
                                Tipo = "C"
                            End If
                            If MyCausale.GiornoUscita = "" Then
                                Tipo = "C"
                            End If
                            If MyCausale.GiornoUscita = "P" Then
                                Tipo = "P"
                            End If
                            If Trim(campodb(MyTabellaAppoggio2.Rows(I).Item(xCol + Sommatore))) <> Trim(campodb(MyTabellaAppoggio2.Rows(I).Item(xCol + Sommatore + 1))) Then
                                If Parametri.GIORNOENTRATA = "P" And Parametri.GIORNOUSCITA = "P" Then
                                    Tipo = "P"
                                End If
                            End If
                        Else

                            If MyCausale.TIPOMOVIMENTO = "N" Then
                                Tipo = "C"
                            End If
                            If MyCausale.TIPOMOVIMENTO = "" Then
                                Tipo = "C"
                            End If
                            If MyCausale.TIPOMOVIMENTO = "P" Then
                                Tipo = "P"
                            End If
                            If MyCausale.TIPOMOVIMENTO = "A" Then
                                Tipo = "A"
                            End If
                        End If

                    End If

                    If Trim(campodb(MyTabellaAppoggio2.Rows(I).Item(xCol + Sommatore))) = "*" Then
                        Tipo = "*"
                    End If


                    If Tipo <> "C" Then
                        If Tipo = "*" Then
                            myriga3(xCol) = ""
                        Else
                            If Tipo = "P" Then
                                myriga3(xCol) = "P"
                                TotPresenze = TotPresenze + 1
                                TotPresenzeAss = TotPresenzeAss + 1
                            Else
                                myriga3(xCol) = "A"
                                TotAssenze = TotAssenze + 1
                                TotAssenzeAss = TotAssenzeAss + 1
                            End If

                        End If
                    Else
                        myriga3(xCol) = ""
                    End If
                Next
                myriga3(xCol) = TotPresenze
                myriga3(xCol + 1) = TotAssenze

                MytabellaPersonalizzata.Rows.Add(myriga3)
            Next
            Dim myriga1 As System.Data.DataRow = MytabellaPersonalizzata.NewRow()
            myriga1.Item(0) = "Totale :"
            myriga1.Item(xCol) = TotPresenzeAss
            myriga1.Item(xCol + 1) = TotAssenzeAss
            MytabellaPersonalizzata.Rows.Add(myriga1)

            Dim myrigaV As System.Data.DataRow = MytabellaPersonalizzata.NewRow()
            MytabellaPersonalizzata.Rows.Add(myrigaV)

            Dim myrigaP As System.Data.DataRow = MytabellaPersonalizzata.NewRow()

            myrigaP(0) = "P = Presenza"
            MytabellaPersonalizzata.Rows.Add(myrigaP)
            Dim myrigaA As System.Data.DataRow = MytabellaPersonalizzata.NewRow()
            myrigaA(0) = "A = Assenza"
            MytabellaPersonalizzata.Rows.Add(myrigaA)



            MyTable = MytabellaPersonalizzata
        End If


        If Chk_DataNasscita.Checked = True Then
            MyTable.Columns.Add("DataNascita")
            MyTable.Columns.Add("Accoglimento")
            MyTable.Columns.Add("Uscita Definitiva")
            For I = 0 To MyTable.Rows.Count - 1
                Dim Ospite As New ClsOspite

                Ospite.CodiceOspite = Val(MyTable.Rows(I).Item(0).ToString)
                Ospite.Leggi(Session("DC_OSPITE"), Ospite.CodiceOspite)

                MyTable.Rows(I).Item(MyTable.Columns.Count - 3) = Format(Ospite.DataNascita, "dd/MM/yyyy")

                Dim UscitaDef As New Cls_Movimenti

                UscitaDef.CENTROSERVIZIO = Cmb_CServ.SelectedValue
                UscitaDef.CodiceOspite = Ospite.CodiceOspite
                UscitaDef.UltimaDataUscitaDefinitiva(Session("DC_OSPITE"))


                Dim Accoglimento As New Cls_Movimenti

                Accoglimento.CENTROSERVIZIO = Cmb_CServ.SelectedValue
                Accoglimento.CodiceOspite = Ospite.CodiceOspite
                Accoglimento.UltimaDataAccoglimento(Session("DC_OSPITE"))


                If Format(UscitaDef.Data, "yyyyMMdd") > Format(Accoglimento.Data, "yyyyMMdd") Then
                    MyTable.Rows(I).Item(MyTable.Columns.Count - 1) = Format(UscitaDef.Data, "dd/MM/yyyy")
                End If

                MyTable.Rows(I).Item(MyTable.Columns.Count - 2) = Format(Accoglimento.Data, "dd/MM/yyyy")
            Next
        End If


        GridView1.AutoGenerateColumns = True
        GridView1.DataSource = MyTable
        GridView1.Font.Size = 10
        GridView1.DataBind()



    End Sub

    Private Sub TabTotaleGenerale(ByRef MyTable As Data.DataTable, ByVal VettoriCausali() As String, ByRef TotaleGeneraleCausali() As Long, ByVal ConnectionString As String, ByVal Giorni As Integer, ByVal PGiorni As Integer, ByVal NumeroCausali As Integer, ByRef TotaleGenerale As Integer)
        Dim x1 As Integer
        Dim Totale As Long
        Dim Totale2 As Long
        Dim TotaleCausali(100) As Long
        Dim IndI As Integer
        TotaleGenerale = 0

        For I = 0 To MyTable.Rows.Count - 1
            Dim pr As New Cls_StatoAuto

            pr.UltimaData(ConnectionString, MyTable.Rows(I).Item(0), Cmb_CServ.SelectedValue)
            MyTable.Rows(I).Item(2) = pr.STATOAUTO
            'MyTable.Rows(I).Item(0) = I + 1

            Totale = 0
            Totale2 = 0

            For IndI = 0 To 100
                TotaleCausali(IndI) = 0
            Next
            For x1 = 1 To Giorni
                If IsDBNull(MyTable.Rows(I).Item(x1 + PGiorni)) Then
                    MyTable.Rows(I).Item(x1 + PGiorni) = "*"
                Else


                    If Trim(MyTable.Rows(I).Item(x1 + PGiorni)) = "" Then
                        Totale = Totale + 1
                    End If

                    For IndI = 0 To NumeroCausali - 1
                        If Trim(MyTable.Rows(I).Item(x1 + PGiorni)) = VettoriCausali(IndI) Then
                            TotaleCausali(IndI) = TotaleCausali(IndI) + 1
                        End If
                    Next
                End If
            Next
            MyTable.Rows(I).Item(Giorni + 1 + PGiorni) = Totale
            For IndI = 0 To NumeroCausali - 1
                MyTable.Rows(I).Item(Giorni + IndI + 2 + PGiorni) = TotaleCausali(IndI)
                TotaleGeneraleCausali(IndI) = TotaleGeneraleCausali(IndI) + TotaleCausali(IndI)
            Next
            TotaleGenerale = TotaleGenerale + Totale
        Next

    End Sub

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Trim(Session("UTENTE")) = "" Then
            Response.Redirect("..\Login.aspx")
            Exit Sub
        End If

        Call EseguiJS()

        If Page.IsPostBack = True Then
            Exit Sub
        End If

        Dim K1 As New Cls_SqlString

        Dim Barra As New Cls_BarraSenior

        If Not IsNothing(Session("RicercaAnagraficaSQLString")) Then
            K1 = Session("RicercaAnagraficaSQLString")
        End If

        Lbl_BarraSenior.Text = Barra.CodiceBarra(Application("SENIOR"), Session("UTENTE"), Page, K1)


        Txt_Anno.Text = Year(Now)
        Dd_Mese.SelectedValue = Month(Now)

        Dim ConnectionString As String = Session("DC_OSPITE")
        Dim kVilla As New Cls_TabelleDescrittiveOspitiAccessori


        kVilla.UpDateDropBox(Session("DC_OSPITIACCESSORI"), "VIL", DD_Struttura)
        If DD_Struttura.Items.Count = 1 Then
            Call AggiornaCServ()
        End If

        Lbl_Utente.Text = Session("UTENTE")

        Call EseguiJS()
    End Sub



    
    Protected Sub ImageButton4_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles ImageButton4.Click
        If Val(Txt_Anno.Text) = 0 Then
            ClientScript.RegisterClientScriptBlock(Me.GetType(), "Errore", "VisualizzaErrore('Specificare anno');", True)
            Exit Sub
        End If

        If Val(Dd_Mese.SelectedValue) = 0 Then
            ClientScript.RegisterClientScriptBlock(Me.GetType(), "Errore", "VisualizzaErrore('Specificare mese');", True)
            Exit Sub
        End If

        REM     If Cmb_CServ.SelectedValue = "" Then
        REM             ClientScript.RegisterClientScriptBlock(Me.GetType(), "Errore", "VisualizzaErrore('Specificare centro servizio');", True)
        REM     Exit Sub
        REM End If


        Call CaricaGriglia()


        If MyTable.Rows.Count > 1 Then
            Response.Clear()
            Response.AddHeader("content-disposition", "attachment;filename=ProspettoPresenze.xls")
            Response.Charset = String.Empty
            Response.Cache.SetCacheability(HttpCacheability.NoCache)
            Response.ContentType = "application/vnd.xls"
            Dim stringWrite As New System.IO.StringWriter
            Dim htmlWrite As New HtmlTextWriter(stringWrite)
            'GridView1.RenderControl(htmlWrite)


            form1.Controls.Clear()
            form1.Controls.Add(GridView1)

            form1.RenderControl(htmlWrite)

            Response.Write(stringWrite.ToString())
            Response.End()
        End If
    End Sub

    Protected Sub ImageButton3_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles ImageButton3.Click
        If Val(Txt_Anno.Text) = 0 Then
            REM ClientScript.RegisterClientScriptBlock(Me.GetType(), "Errore", "VisualizzaErrore('Specificare anno');", True)
            ScriptManager.RegisterStartupScript(Me, Me.GetType(), "visualizzaA", "VisualizzaErrore('Specificare anno');", True)
            Exit Sub
        End If

        If Val(Dd_Mese.SelectedValue) = 0 Then
            REM ClientScript.RegisterClientScriptBlock(Me.GetType(), "Errore", "VisualizzaErrore('Specificare mese');", True)
            ScriptManager.RegisterStartupScript(Me, Me.GetType(), "visualizzaA", "VisualizzaErrore('Specificare mese');", True)
            Exit Sub
        End If

        If chk_PresenzeAssenze.Checked = True And Cmb_CServ.SelectedValue = "" Then
            ScriptManager.RegisterStartupScript(Me, Me.GetType(), "visualizzaA", "VisualizzaErrore('Devi indicare il centro servizio');", True)
            Exit Sub
        End If

        If chk_PresenzeAssenzePartime.Checked = True And Cmb_CServ.SelectedValue = "" Then
            ScriptManager.RegisterStartupScript(Me, Me.GetType(), "visualizzaA", "VisualizzaErrore('Devi indicare il centro servizio');", True)
            Exit Sub
        End If

        If chk_PresenzeAssenzePartime.Checked = True Then
            Dim M As New Cls_CentroServizio

            M.CENTROSERVIZIO = Cmb_CServ.SelectedValue
            M.Leggi(Session("DC_OSPITE"), M.CENTROSERVIZIO)
            If M.TIPOCENTROSERVIZIO <> "D" Then
                ScriptManager.RegisterStartupScript(Me, Me.GetType(), "visualizzaA", "VisualizzaErrore('Devi indicare un centro servizio diurno');", True)
                Exit Sub
            End If
        End If

        REM     If Cmb_CServ.SelectedValue = "" Then
        REM ClientScript.RegisterClientScriptBlock(Me.GetType(), "Errore", "VisualizzaErrore('Specificare centro servizio');", True)
        REM             ScriptManager.RegisterStartupScript(Me, Me.GetType(), "visualizzaA", "VisualizzaErrore('Specificare centro servizio');", True)
        REM     Exit Sub
        REM End If


        Call CaricaGriglia()

    End Sub

    Protected Sub Btn_Esci_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles Btn_Esci.Click
        If Session("ABILITAZIONI") = "" Then
            Response.Redirect("..\Login.aspx")
        End If
        If Session("ABILITAZIONI").IndexOf("<PORTINERIA>") > 0 Then
            Response.Redirect("..\MainPortineria.aspx")
        Else
            Response.Redirect("Menu_Visualizzazioni.aspx")
        End If
    End Sub

    Private Sub EseguiJS()
        Dim MyJs As String
        MyJs = "$(document).ready(function() {"


        MyJs = MyJs & "var els = document.getElementsByTagName(""*"");"
        MyJs = MyJs & "for (var i=0;i<els.length;i++)"
        MyJs = MyJs & "if ( els[i].id ) { "
        MyJs = MyJs & " var appoggio =els[i].id; "

        MyJs = MyJs & " if ((appoggio.match('Txt_Ospite')!= null) || (appoggio.match('Txt_Ospite')!= null))  {  "
        MyJs = MyJs & " $(els[i]).autocomplete('AutocompleteOspitiSenzaCserv.ashx?Utente=" & Session("UTENTE") & "', {delay:5,minChars:3});"
        MyJs = MyJs & "    }"

        MyJs = MyJs & " if (appoggio.match('Txt_Data')!= null) {  "
        MyJs = MyJs & " $(els[i]).mask(""99/99/9999"");"
        MyJs = MyJs & "    }"


        MyJs = MyJs & "} "


        MyJs = MyJs & "if (window.innerHeight>0) { $(""#BarraLaterale"").css(""height"",(window.innerHeight - 94) + ""px""); } else"
        MyJs = MyJs & "{ $(""#BarraLaterale"").css(""height"",(document.documentElement.offsetHeight - 94) + ""px"");  }"
        MyJs = MyJs & "});"




        'MyJs = "$(document).ready(function() { $('.myClass').keypress(function() { handleEnter($('.myClass').val(), true, true); } );     $('#" & Txt_ImportoPagato.ClientID & "').blur(function() { var ap = formatNumber ($('#" & Txt_ImportoPagato.ClientID & "').val(),2); $('#" & Txt_ImportoPagato.ClientID & "').val(ap); }); });"
        ScriptManager.RegisterStartupScript(Me, Me.GetType(), "visualizzaRitIm", MyJs, True)
    End Sub

    Protected Sub Btn_Stampa_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles Btn_Stampa.Click
        Call CaricaGriglia()


        MyTable.DefaultView.Sort = "Nome Asc"

        Session("GrigliaSoloStampa") = MyTable

        Session("ParametriSoloStampa") = "Anno " & Txt_Anno.Text & " Mese " & Dd_Mese.SelectedItem.Text & " Centroservizio " & Cmb_CServ.SelectedItem.Text

        ScriptManager.RegisterClientScriptBlock(Me, Me.GetType(), "Stampa", "openPopUp('GrigliaSoloStampa.aspx','Stampe','width=800,height=600');", True)

    End Sub

    Private Sub AggiornaCServ()
        Dim kCsrv As New Cls_CentroServizio

        If DD_Struttura.SelectedValue = "" Then
            kCsrv.UpDateDropBox(Session("DC_OSPITE"), Cmb_CServ)
        Else
            kCsrv.UpDateDropBoxStruttura(Session("DC_OSPITE"), Cmb_CServ, DD_Struttura.SelectedValue)
        End If
        Call EseguiJS()
    End Sub



    Protected Sub DD_Struttura_TextChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles DD_Struttura.TextChanged
        AggiornaCServ()
        Call EseguiJS()
    End Sub

    Function campodb(ByVal oggetto As Object) As String
        If IsDBNull(oggetto) Then
            Return ""
        Else
            Return oggetto
        End If
    End Function
    Function campodbD(ByVal oggetto As Object) As Date
        If IsDBNull(oggetto) Then
            Return Nothing
        Else
            Return oggetto
        End If
    End Function

    Private Function SplitWords(ByVal s As String) As String()
        '
        ' Call Regex.Split function from the imported namespace.
        ' Return the result array.
        '
        Return Regex.Split(s, "\W+")
    End Function
End Class