﻿Imports System
Imports System.Web
Imports Microsoft.VisualBasic
Imports System.Data.OleDb
Imports System.Web.Hosting
Imports System.Data.SqlTypes
Imports System.Net
Imports System.Collections.Generic
Imports System.IO
Imports System.Security.Cryptography.X509Certificates
Imports System.Net.Security
Imports System.Web.Script.Serialization
Imports org.jivesoftware.util
Imports Newtonsoft.Json
Imports Newtonsoft.Json.Linq
Partial Class OspitiWeb_Menu_Epersonam
    Inherits System.Web.UI.Page

    Protected Sub Btn_Esci_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles Btn_Esci.Click
        Response.Redirect("Menu_Ospiti.aspx")
    End Sub

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load

        If Trim(Session("UTENTE")) = "" Then
            Response.Redirect("..\Login.aspx")
            Exit Sub
        End If

        If Session("ABILITAZIONI").IndexOf("<PORTINERIA>") > 0 Then
            Response.Redirect("..\MainPortineria.aspx")
        End If

        Dim Barra As New Cls_BarraSenior

        Lbl_BarraSenior.Text = Barra.CodiceBarra(Application("SENIOR"), Session("UTENTE"), Page)


        Dim M As New Cls_Parametri


        M.LeggiParametri(Session("DC_OSPITE"))


        Img_AllineDb.Visible = True
        Img_AllineDb.ImageUrl = "../images/Menu_EPersonam.PNG"
        If Not IsNothing(Session("ABILITAZIONI")) Then
            If Session("ABILITAZIONI").IndexOf("<EPERSONAM>") >= 0 Then
                Img_AllineDb.ImageUrl = "../images/Menu_EPersonam.PNG"
                'End If
            Else

                Img_AllineDb.ImageUrl = "../images/Menu_EPersonam.PNG"
            End If
        End If


        If M.ApiV1 = 1 Then
            Img_Extra.Visible = True
            lblExtra.Text = "EXTRA"

            Img_Listino.Visible = True
            Lbl_Listino.Visible = True
            Lbl_Listino.Text = "ANAGRAFICA LISTINO"


            Img_ListinoAggironamento.Visible = True
            Lbl_ListinoAggironamento.Text = "LISTINI-AGGIORNAMENTI"
        Else
            Img_Extra.Visible = False
            lblExtra.Text = ""
            Lbl_Listino.Text = ""
            Img_Listino.Visible = False
            Lbl_Listino.Visible = False


            Img_ListinoAggironamento.Visible = False
            Lbl_ListinoAggironamento.Text = ""
            Lbl_ListinoAggironamento.Visible = False
        End If


        If M.ImportListino = 0 Then
            Lbl_Listino.Text = ""
            Img_Listino.Visible = False
            Lbl_Listino.Visible = False

            Img_ListinoAggironamento.Visible = False
            Lbl_ListinoAggironamento.Text = ""
            Lbl_ListinoAggironamento.Visible = False
        End If

    End Sub

    Function campodb(ByVal oggetto As Object) As String
        If IsDBNull(oggetto) Then
            Return ""
        Else
            Return oggetto
        End If
    End Function

    Protected Sub Img_AllineDb_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles Img_AllineDb.Click
        Dim M As New Cls_Parametri


        M.LeggiParametri(Session("DC_OSPITE"))



        If Not IsNothing(Session("ABILITAZIONI")) Then
            If Session("ABILITAZIONI").IndexOf("<EPERSONAM>") >= 0 Then
                Response.Redirect("AllineaDbInterfaccia.aspx")
            Else
                If M.ApiV1 = 0 Then
                    Response.Redirect("ImportOspiti.aspx")
                Else
                    Response.Redirect("ApiV1Epersonam/ImportOspitiV1.aspx")
                End If
            End If
        Else
            If M.ApiV1 = 0 Then
                Response.Redirect("ImportOspiti.aspx")
            Else
                Response.Redirect("ApiV1Epersonam/ImportOspitiV1.aspx")
            End If
        End If
    End Sub

    Protected Sub Img_ImportaMovimenti_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles Img_ImportaMovimenti.Click
        Dim cn As OleDbConnection

        cn = New Data.OleDb.OleDbConnection(Session("DC_OSPITE"))

        cn.Open()

        Dim M As New Cls_Parametri


        M.LeggiParametri(Session("DC_OSPITE"))

        If M.ApiV1 = 1 Then
            Response.Redirect("ApiV1Epersonam/importomovimenti_newV1.aspx")
            Exit Sub
        End If

        Dim CmdTemp As New OleDbCommand

        CmdTemp.Connection = cn
        CmdTemp.CommandText = "Select * From EPersonam"

        Dim ReadTemp As OleDbDataReader = CmdTemp.ExecuteReader
        If ReadTemp.Read Then
            ReadTemp.Close()
            cn.Close()
            If M.ApiV1 = 1 Then
                Response.Redirect("ApiV1Epersonam/importomovimenti_newV1.aspx")
                Exit Sub
            Else
                Response.Redirect("importomovimenti_new.aspx")
                Exit Sub
            End If
        Else
            ReadTemp.Close()
            cn.Close()
            Response.Redirect("ImportMovimenti.aspx")
            Exit Sub
        End If
        ReadTemp.Close()
        cn.Close()
    End Sub

    
    Protected Sub Img_Diurno_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles Img_Diurno.Click
        Dim Param As New Cls_Parametri

        Param.LeggiParametri(Session("DC_OSPITE"))

        If Param.ApiV1 = 1 Then
            Response.Redirect("ApiV1Epersonam/ImportDiurnoNewV1.aspx")
        Else
            Response.Redirect("ImportDiurnoNew.aspx")
        End If

    End Sub

    Protected Sub Img_Domiciliare_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles Img_Domiciliare.Click
        Dim Param As New Cls_Parametri

        Param.LeggiParametri(Session("DC_OSPITE"))

        If Param.ApiV1 = 1 Then
            Response.Redirect("ApiV1Epersonam/ImportDomiciliareV1.aspx")
        Else
            Response.Redirect("ImportDomiciliare.aspx")
        End If

    End Sub

    Protected Sub Img_Extra_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles Img_Extra.Click
        Response.Redirect("ApiV1Epersonam/ImportExtra.aspx")
    End Sub

    Protected Sub Img_Listino_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles Img_Listino.Click
        Response.Redirect("ApiV1Epersonam/ImportListini.aspx")
    End Sub

    Protected Sub Img_ListinoAggironamento_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles Img_ListinoAggironamento.Click
        Response.Redirect("ApiV1Epersonam/ImportMovimentiListini.aspx")
    End Sub
End Class
