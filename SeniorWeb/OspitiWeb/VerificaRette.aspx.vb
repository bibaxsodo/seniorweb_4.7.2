﻿Imports System.Web.Hosting
Imports System.Data.OleDb

Partial Class VerificaRette
    Inherits System.Web.UI.Page
    Dim MyTable As New System.Data.DataTable("tabella")
    Dim MyDataSet As New System.Data.DataSet()

   


    Public Function GiorniMese(ByVal Mese As Byte, ByVal Anno As Integer) As Byte
        If Mese = 1 Or Mese = 3 Or Mese = 5 Or Mese = 7 Or Mese = 8 Or _
           Mese = 10 Or Mese = 12 Then
            GiorniMese = 31
        Else
            If Mese <> 2 Then
                GiorniMese = 30
            Else
                If Day(DateSerial(Anno, Mese, 29)) = 29 Then
                    GiorniMese = 29
                Else
                    GiorniMese = 28
                End If
            End If
        End If
    End Function
    Private Sub CaricaGriglia()
        Dim ConnectionString As String = Session("DC_OSPITE")
        Dim cn As OleDbConnection

        cn = New Data.OleDb.OleDbConnection(ConnectionString)

        cn.Open()


        Dim Anno As Integer
        Dim Mese As Integer
        Dim Giorni As Integer
        Dim Cserv As String
        Dim MySelectedCserv As String
        Dim MySql As String
        Dim PGiorni As Integer = 4
        Dim IndiceCs As Integer

        Anno = Txt_Anno.Text
        Mese = Dd_Mese.SelectedValue
        Cserv = Cmb_CServ.SelectedValue
        MySelectedCserv = Cserv
        Giorni = GiorniMese(Mese, Anno)

        MyTable.Clear()
        MyTable.Columns.Clear()

        MyTable.Columns.Add("Nome", GetType(String))
        If Chk_Sintetica.Checked = False Then
            MyTable.Columns.Add("Tipo Retta", GetType(String))
        End If
        MyTable.Columns.Add("Giorni Presenza", GetType(Long))
        MyTable.Columns.Add("Giorni Assenza", GetType(Long))
        MyTable.Columns.Add("Retta Ospite", GetType(Double))
        If Chk_Sintetica.Checked = False Then
            MyTable.Columns.Add("Tipo Operazione Ospite", GetType(String))
        End If
        MyTable.Columns.Add("Retta Parenti", GetType(Double))
        If Chk_Sintetica.Checked = False Then
            MyTable.Columns.Add("Tipo Operazione Parente", GetType(String))
        End If
        MyTable.Columns.Add("Retta Comune", GetType(Double))
        MyTable.Columns.Add("Retta Regione", GetType(Double))
        MyTable.Columns.Add("Retta Jolly", GetType(Double))
        MyTable.Columns.Add("Addebiti Ospite", GetType(Double))
        MyTable.Columns.Add("Accrediti Ospite", GetType(Double))

        MyTable.Columns.Add("Addebiti Parente", GetType(Double))
        MyTable.Columns.Add("Accrediti Parente", GetType(Double))

        MyTable.Columns.Add("Addebiti Comune", GetType(Double))
        MyTable.Columns.Add("Accrediti Comune", GetType(Double))

        MyTable.Columns.Add("Addebiti Jolly", GetType(Double))
        MyTable.Columns.Add("Accrediti Jolly", GetType(Double))

        MyTable.Columns.Add("Addebiti Regione", GetType(Double))
        MyTable.Columns.Add("Accrediti Regione", GetType(Double))
        If Chk_Sintetica.Checked = False Then
            MyTable.Columns.Add("Extra Fissi", GetType(String))
        End If

        MyTable.Columns.Add("CodiceOspite", GetType(String))
        If Chk_GGSanitario.Checked = True Then
            MyTable.Columns.Add("GG Sanitario Presenze", GetType(String))
            MyTable.Columns.Add("GG Sanitario Assenze", GetType(String))
        End If
        If Chk_Ricalcolati.Checked = True Then
            MyTable.Columns.Add("GG Ric. Presenze", GetType(String))
            MyTable.Columns.Add("GG Ric. Assenze", GetType(String))
        End If
        

        For IndiceCs = 0 To Cmb_CServ.Items.Count - 1
            If (Cmb_CServ.Items(IndiceCs).Value = MySelectedCserv Or MySelectedCserv = "") And Cmb_CServ.Items(IndiceCs).Value <> "" Then
                Cserv = Cmb_CServ.Items(IndiceCs).Value

                If Mese = 0 Then
                    MySql = "SELECT  CODICEOSPITE  FROM RETTEOSPITE WHERE RETTEOSPITE.ANNO = " & Anno & "  UNION SELECT CODICEOSPITE FROM RETTEPARENTE WHERE RETTEPARENTE.ANNO = " & Anno & "  UNION SELECT CODICEOSPITE FROM RETTECOMUNE WHERE RETTECOMUNE.ANNO = " & Anno & " UNION SELECT CODICEOSPITE FROM RETTEJOLLY  WHERE RETTEJOLLY.ANNO = " & Anno & " UNION SELECT CODICEOSPITE FROM RETTEREGIONE WHERE RETTEREGIONE.ANNO = " & Anno & "  UNION SELECT CODICEOSPITE FROM RETTEENTE WHERE RETTEENTE.ANNO = " & Anno & "  GROUP BY CODICEOSPITE"
                Else
                    MySql = "SELECT  CODICEOSPITE  FROM RETTEOSPITE WHERE RETTEOSPITE.ANNO = " & Anno & "  AND RETTEOSPITE.MESE = " & Mese & " UNION SELECT CODICEOSPITE FROM RETTEPARENTE WHERE RETTEPARENTE.ANNO = " & Anno & " AND RETTEPARENTE.MESE = " & Mese & " UNION SELECT CODICEOSPITE FROM RETTECOMUNE WHERE RETTECOMUNE.ANNO = " & Anno & " AND RETTECOMUNE.MESE = " & Mese & " UNION SELECT CODICEOSPITE FROM RETTEJOLLY  WHERE RETTEJOLLY.ANNO = " & Anno & " AND RETTEJOLLY.MESE = " & Mese & "  UNION SELECT CODICEOSPITE FROM RETTEREGIONE WHERE RETTEREGIONE.ANNO = " & Anno & " AND RETTEREGIONE.MESE = " & Mese & " UNION SELECT CODICEOSPITE FROM RETTEENTE WHERE RETTEENTE.ANNO = " & Anno & " AND RETTEENTE.MESE = " & Mese & " GROUP BY CODICEOSPITE"
                End If
                Dim cmd As New OleDbCommand()
                cmd.CommandText = MySql
                cmd.Connection = cn
                Dim myPOSTreader As OleDbDataReader = cmd.ExecuteReader()
                Do While myPOSTreader.Read
                    Dim CodiceOspite As Long
                    Dim ImportoOspite As Double = 0
                    Dim ImportoParente As Double = 0
                    Dim ImportoComune As Double = 0
                    Dim ImportoRegione As Double = 0
                    Dim ImportoJolly As Double = 0

                    Dim ImportoAddebitiOspite As Double = 0
                    Dim ImportoAccreditiOspite As Double = 0

                    Dim ImportoAddebitiParente As Double = 0
                    Dim ImportoAccreditiParente As Double = 0
                    Dim ImportoAddebitiComune As Double = 0
                    Dim ImportoAccreditiComune As Double = 0
                    Dim ImportoAddebitiJolly As Double = 0
                    Dim ImportoAccreditiJolly As Double = 0

                    Dim ImportoAddebitiRegione As Double = 0
                    Dim ImportoAccreditiRegione As Double = 0


                    Dim GiorniPresO As Long = 0
                    Dim GiorniAssO As Long = 0
                    Dim GiorniPresP As Long = 0
                    Dim GiorniAssP As Long = 0
                    Dim GiorniPresC As Long = 0
                    Dim GiorniAssC As Long = 0
                    Dim GiorniPresR As Long = 0
                    Dim GiorniAssR As Long = 0

                    Dim GiorniPresJ As Long = 0
                    Dim GiorniAssJ As Long = 0

                    CodiceOspite = myPOSTreader.Item("CodiceOspite")
                    If CodiceOspite = 546 Then
                        CodiceOspite = 546
                    End If
                    If Mese = 0 Then
                        MySql = "SELECT  *  FROM RETTEOSPITE WHERE RETTEOSPITE.ANNO = " & Anno & "  And CodiceOspite = " & CodiceOspite & " And CentroServizio = '" & Cserv & "'"
                    Else
                        MySql = "SELECT  *  FROM RETTEOSPITE WHERE RETTEOSPITE.ANNO = " & Anno & "  AND RETTEOSPITE.MESE = " & Mese & " And CodiceOspite = " & CodiceOspite & " And CentroServizio = '" & Cserv & "'"
                    End If
                    Dim cmdO As New OleDbCommand()
                    cmdO.CommandText = MySql
                    cmdO.Connection = cn
                    Dim OReader As OleDbDataReader = cmdO.ExecuteReader()
                    Do While OReader.Read
                        If OReader.Item("ELEMENTO") = "ADD" Or OReader.Item("ELEMENTO") = "ACC" Then
                            If OReader.Item("ELEMENTO") = "ADD" Then
                                ImportoAddebitiOspite = ImportoAddebitiOspite + OReader.Item("IMPORTO")
                            End If
                            If OReader.Item("ELEMENTO") = "ACC" Then
                                ImportoAccreditiOspite = ImportoAccreditiOspite + OReader.Item("IMPORTO")
                            End If
                        Else
                            If Chk_ExtraInRetta.Checked = True Then
                                ImportoOspite = ImportoOspite + OReader.Item("IMPORTO")
                            Else
                                If Mid(campodb(OReader.Item("ELEMENTO")) & Space(10), 1, 1) <> "E" Then
                                    ImportoOspite = ImportoOspite + OReader.Item("IMPORTO")
                                End If
                            End If

                        End If
                        If OReader.Item("GIORNI") > 0 And OReader.Item("ELEMENTO") = "RGA" Then
                            GiorniAssO = GiorniAssO + OReader.Item("GIORNI")
                        End If
                        If OReader.Item("GIORNI") > 0 And (OReader.Item("ELEMENTO") = "RGP" Or OReader.Item("ELEMENTO") = "RPX") Then
                            GiorniPresO = GiorniPresO + OReader.Item("GIORNI")
                        End If
                    Loop
                    OReader.Close()

                    If CodiceOspite = 4265 Then
                        CodiceOspite = 4265
                    End If
                    If Mese = 0 Then
                        MySql = "SELECT  *  FROM RETTEPARENTE WHERE ANNO = " & Anno & "  And CodiceOspite = " & CodiceOspite & " And CentroServizio = '" & Cserv & "'"
                    Else
                        MySql = "SELECT  *  FROM RETTEPARENTE WHERE ANNO = " & Anno & "  AND MESE = " & Mese & " And CodiceOspite = " & CodiceOspite & " And CentroServizio = '" & Cserv & "'"
                    End If
                    Dim cmdP As New OleDbCommand()
                    cmdP.CommandText = MySql
                    cmdP.Connection = cn
                    Dim PReader As OleDbDataReader = cmdP.ExecuteReader()
                    Do While PReader.Read
                        If PReader.Item("ELEMENTO") = "ADD" Or PReader.Item("ELEMENTO") = "ACC" Then
                            If PReader.Item("ELEMENTO") = "ADD" Then
                                ImportoAddebitiParente = ImportoAddebitiParente + PReader.Item("IMPORTO")
                            End If
                            If PReader.Item("ELEMENTO") = "ACC" Then
                                ImportoAccreditiParente = ImportoAccreditiParente + PReader.Item("IMPORTO")
                            End If
                        Else
                            If Chk_ExtraInRetta.Checked = True Then
                                ImportoParente = ImportoParente + PReader.Item("IMPORTO")
                            Else
                                If Mid(campodb(PReader.Item("ELEMENTO")) & Space(10), 1, 1) <> "E" Then
                                    ImportoParente = ImportoParente + PReader.Item("IMPORTO")
                                End If
                            End If
                        End If
                        If PReader.Item("GIORNI") > 0 And PReader.Item("ELEMENTO") = "RGA" Then
                            GiorniAssP = GiorniAssP + PReader.Item("GIORNI")
                        End If
                        If PReader.Item("GIORNI") > 0 And (PReader.Item("ELEMENTO") = "RGP" Or PReader.Item("ELEMENTO") = "RPX") Then
                            GiorniPresP = GiorniPresP + PReader.Item("GIORNI")
                        End If
                    Loop
                    PReader.Close()

                    'If Chk_MeseRiferemento.Checked = True And Mese > 0 Then
                    '    '1363
                    '    Dim xMese As Integer
                    '    Dim xAnno As Integer
                    '    If Mese = 12 Then
                    '        xAnno = Anno + 1
                    '        xMese = 1
                    '    Else
                    '        xMese = Mese + 1
                    '        xAnno = Anno
                    '    End If
                    '    If CodiceOspite = 1363 Then
                    '        ImportoAddebitiOspite = 0
                    '    End If
                    '    ImportoAddebitiOspite = 0
                    '    ImportoAccreditiOspite = 0
                    '    MySql = "SELECT  *  FROM RETTEOSPITE WHERE RETTEOSPITE.ANNO = " & Anno & "  AND RETTEOSPITE.MESE = " & Mese & " And CodiceOspite = " & CodiceOspite & " And CentroServizio = '" & Cserv & "' And MeseCompetenza = 0 And AnnoCompetenza = 0"
                    '    Dim cmdOAd As New OleDbCommand()
                    '    cmdOAd.CommandText = MySql
                    '    cmdOAd.Connection = cn
                    '    Dim OAdReader As OleDbDataReader = cmdOAd.ExecuteReader()
                    '    Do While OAdReader.Read
                    '        If OAdReader.Item("ELEMENTO") = "ADD" Then
                    '            ImportoAddebitiOspite = ImportoAddebitiOspite + OAdReader.Item("IMPORTO")
                    '        End If
                    '        If OAdReader.Item("ELEMENTO") = "ACC" Then
                    '            ImportoAccreditiOspite = ImportoAccreditiOspite + OAdReader.Item("IMPORTO")
                    '        End If
                    '    Loop
                    '    OAdReader.Close()

                    '    MySql = "SELECT  *  FROM RETTEOSPITE WHERE RETTEOSPITE.AnnoCompetenza = " & xAnno & "  AND RETTEOSPITE.MeseCompetenza = " & xMese & " And CodiceOspite = " & CodiceOspite & " And CentroServizio = '" & Cserv & "'  "
                    '    Dim cmdOAdComp As New OleDbCommand()
                    '    cmdOAdComp.CommandText = MySql
                    '    cmdOAdComp.Connection = cn
                    '    Dim OAdReaderComp As OleDbDataReader = cmdOAdComp.ExecuteReader()
                    '    Do While OAdReaderComp.Read
                    '        If OAdReaderComp.Item("ELEMENTO") = "ADD" Then
                    '            ImportoAddebitiOspite = ImportoAddebitiOspite + OAdReaderComp.Item("IMPORTO")
                    '        End If
                    '        If OAdReaderComp.Item("ELEMENTO") = "ACC" Then
                    '            ImportoAccreditiOspite = ImportoAccreditiOspite + OAdReaderComp.Item("IMPORTO")
                    '        End If
                    '    Loop
                    '    OAdReaderComp.Close()

                    '    ImportoAddebitiParente = 0
                    '    ImportoAccreditiParente = 0
                    '    MySql = "SELECT  *  FROM RETTEPARENTE WHERE ANNO = " & Anno & "  AND MESE = " & Mese & " And CodiceOspite = " & CodiceOspite & " And CentroServizio = '" & Cserv & "' And MeseCompetenza = 0 And AnnoCompetenza = 0"

                    '    Dim cmdPAd As New OleDbCommand()
                    '    cmdPAd.CommandText = MySql
                    '    cmdPAd.Connection = cn
                    '    Dim PAdReader As OleDbDataReader = cmdPAd.ExecuteReader()
                    '    Do While PAdReader.Read
                    '        If PAdReader.Item("ELEMENTO") = "ADD" Then
                    '            ImportoAddebitiParente = ImportoAddebitiParente + PAdReader.Item("IMPORTO")
                    '        End If
                    '        If PAdReader.Item("ELEMENTO") = "ACC" Then
                    '            ImportoAccreditiParente = ImportoAccreditiParente + PAdReader.Item("IMPORTO")
                    '        End If
                    '    Loop
                    '    PAdReader.Close()


                    '    MySql = "SELECT  *  FROM RETTEPARENTE WHERE AnnoCompetenza  = " & xAnno & "  AND MeseCompetenza = " & xMese & " And CodiceOspite = " & CodiceOspite & " And CentroServizio = '" & Cserv & "' "
                    '    Dim cmdPAComp As New OleDbCommand()
                    '    cmdPAComp.CommandText = MySql
                    '    cmdPAComp.Connection = cn
                    '    Dim PACompdReader As OleDbDataReader = cmdPAComp.ExecuteReader()
                    '    Do While PACompdReader.Read
                    '        If PACompdReader.Item("ELEMENTO") = "ADD" Then
                    '            ImportoAddebitiParente = ImportoAddebitiParente + PACompdReader.Item("IMPORTO")
                    '        End If
                    '        If PACompdReader.Item("ELEMENTO") = "ACC" Then
                    '            ImportoAccreditiParente = ImportoAccreditiParente + PACompdReader.Item("IMPORTO")
                    '        End If
                    '    Loop
                    '    PACompdReader.Close()
                    'End If

                    If Mese = 0 Then
                        MySql = "SELECT  *  FROM RETTECOMUNE WHERE ANNO = " & Anno & " And CodiceOspite = " & CodiceOspite & " And CentroServizio = '" & Cserv & "'"
                    Else
                        MySql = "SELECT  *  FROM RETTECOMUNE WHERE ANNO = " & Anno & "  AND MESE = " & Mese & " And CodiceOspite = " & CodiceOspite & " And CentroServizio = '" & Cserv & "'"
                    End If
                    Dim cmdC As New OleDbCommand()
                    cmdC.CommandText = MySql
                    cmdC.Connection = cn
                    Dim CReader As OleDbDataReader = cmdC.ExecuteReader()
                    Do While CReader.Read
                        If CReader.Item("ELEMENTO") = "ADD" Or CReader.Item("ELEMENTO") = "ACC" Then
                            If CReader.Item("ELEMENTO") = "ADD" Then
                                ImportoAddebitiComune = ImportoAddebitiComune + CReader.Item("IMPORTO")
                            End If
                            If CReader.Item("ELEMENTO") = "ACC" Then
                                ImportoAccreditiComune = ImportoAccreditiComune + CReader.Item("IMPORTO")
                            End If
                        Else                            
                            If Chk_ExtraInRetta.Checked = True Then
                                ImportoComune = ImportoComune + CReader.Item("IMPORTO")
                            Else
                                If Mid(campodb(CReader.Item("ELEMENTO")) & Space(10), 1, 1) <> "E" Then
                                    ImportoComune = ImportoComune + CReader.Item("IMPORTO")
                                End If
                            End If
                        End If
                        If CReader.Item("GIORNI") > 0 And CReader.Item("ELEMENTO") = "RGA" Then
                            GiorniAssC = GiorniAssC + CReader.Item("GIORNI")
                        End If
                        If CReader.Item("GIORNI") > 0 And (CReader.Item("ELEMENTO") = "RGP" Or CReader.Item("ELEMENTO") = "RPX") Then
                            GiorniPresC = GiorniPresC + CReader.Item("GIORNI")
                        End If
                    Loop
                    CReader.Close()

                    If Mese = 0 Then
                        MySql = "SELECT  *  FROM RETTEREGIONE WHERE ANNO = " & Anno & "  And CodiceOspite = " & CodiceOspite & " And CentroServizio = '" & Cserv & "'"
                    Else
                        MySql = "SELECT  *  FROM RETTEREGIONE WHERE ANNO = " & Anno & "  AND MESE = " & Mese & " And CodiceOspite = " & CodiceOspite & " And CentroServizio = '" & Cserv & "'"
                    End If
                    Dim cmdR As New OleDbCommand()
                    cmdR.CommandText = MySql
                    cmdR.Connection = cn
                    Dim RReader As OleDbDataReader = cmdR.ExecuteReader()
                    Do While RReader.Read
                        If RReader.Item("ELEMENTO") = "ADD" Or RReader.Item("ELEMENTO") = "ACC" Then
                            If RReader.Item("ELEMENTO") = "ADD" Then
                                ImportoAddebitiRegione = ImportoAddebitiRegione + RReader.Item("IMPORTO")
                            End If
                            If RReader.Item("ELEMENTO") = "ACC" Then
                                ImportoAccreditiRegione = ImportoAccreditiRegione + RReader.Item("IMPORTO")
                            End If
                        Else
                            ImportoRegione = ImportoRegione + RReader.Item("IMPORTO")
                        End If
                        If RReader.Item("GIORNI") > 0 And RReader.Item("ELEMENTO") = "RGA" Then
                            GiorniAssR = GiorniAssR + RReader.Item("GIORNI")
                        End If
                        If RReader.Item("GIORNI") > 0 And (RReader.Item("ELEMENTO") = "RGP" Or RReader.Item("ELEMENTO") = "RPX") Then
                            GiorniPresR = GiorniPresR + RReader.Item("GIORNI")
                        End If
                    Loop
                    RReader.Close()


                    If Mese = 0 Then
                        MySql = "SELECT  *  FROM RETTEJOLLY WHERE ANNO = " & Anno & " And CodiceOspite = " & CodiceOspite & " And CentroServizio = '" & Cserv & "'"
                    Else
                        MySql = "SELECT  *  FROM RETTEJOLLY WHERE ANNO = " & Anno & "  AND MESE = " & Mese & " And CodiceOspite = " & CodiceOspite & " And CentroServizio = '" & Cserv & "'"
                    End If
                    Dim cmdJ As New OleDbCommand()
                    cmdJ.CommandText = MySql
                    cmdJ.Connection = cn
                    Dim JReader As OleDbDataReader = cmdJ.ExecuteReader()
                    Do While JReader.Read
                        If JReader.Item("ELEMENTO") = "ADD" Or JReader.Item("ELEMENTO") = "ACC" Then
                            If JReader.Item("ELEMENTO") = "ADD" Then
                                ImportoAddebitiJolly = ImportoAddebitiJolly + JReader.Item("IMPORTO")
                            End If
                            If JReader.Item("ELEMENTO") = "ACC" Then
                                ImportoAccreditiJolly = ImportoAccreditiJolly + JReader.Item("IMPORTO")
                            End If
                        Else
                            ImportoJolly = ImportoJolly + JReader.Item("IMPORTO")
                        End If
                        If JReader.Item("GIORNI") > 0 And JReader.Item("ELEMENTO") = "RGA" Then
                            GiorniAssJ = GiorniAssJ + JReader.Item("GIORNI")
                        End If
                        If JReader.Item("GIORNI") > 0 And (JReader.Item("ELEMENTO") = "RGP" Or JReader.Item("ELEMENTO") = "RPX") Then
                            GiorniPresJ = GiorniPresJ + JReader.Item("GIORNI")
                        End If
                    Loop
                    JReader.Close()


                    If DD_ModalitaPagamento.SelectedValue <> "" Then
                        Dim Visualizzare As Boolean = False
                        Dim OSp As New ClsOspite

                        OSp.Leggi(Session("DC_OSPITE"), CodiceOspite)

                        Dim OspD As New Cls_DatiOspiteParenteCentroServizio

                        OspD.CentroServizio = Cserv
                        OspD.CodiceOspite = CodiceOspite
                        OspD.CodiceParente = 0
                        OspD.Leggi(Session("DC_OSPITE"))
                        If OspD.CodiceOspite <> 0 Then
                            OSp.MODALITAPAGAMENTO = OspD.ModalitaPagamento
                        End If
                        If OSp.MODALITAPAGAMENTO = DD_ModalitaPagamento.SelectedValue Then
                            Visualizzare = True
                        End If

                        If Not Visualizzare Then
                            Dim Par As New Cls_Parenti

                            Par.Leggi(Session("DC_OSPITE"), CodiceOspite, 1)

                            Dim ParD As New Cls_DatiOspiteParenteCentroServizio

                            ParD.CentroServizio = Cserv
                            ParD.CodiceOspite = CodiceOspite
                            ParD.CodiceParente = 1
                            ParD.Leggi(Session("DC_OSPITE"))
                            If ParD.CodiceOspite <> 0 Then
                                ParD.ModalitaPagamento = ParD.ModalitaPagamento
                            End If
                            If ParD.ModalitaPagamento = DD_ModalitaPagamento.SelectedValue Then
                                Visualizzare = True
                            End If
                        End If
                        If Not Visualizzare Then
                            Dim Par As New Cls_Parenti

                            Par.Leggi(Session("DC_OSPITE"), CodiceOspite, 2)

                            Dim ParD As New Cls_DatiOspiteParenteCentroServizio

                            ParD.CentroServizio = Cserv
                            ParD.CodiceOspite = CodiceOspite
                            ParD.CodiceParente = 2
                            ParD.Leggi(Session("DC_OSPITE"))
                            If ParD.CodiceOspite <> 0 Then
                                ParD.ModalitaPagamento = ParD.ModalitaPagamento
                            End If
                            If ParD.ModalitaPagamento = DD_ModalitaPagamento.SelectedValue Then
                                Visualizzare = True
                            End If
                        End If
                        If Not Visualizzare Then
                            Dim Par As New Cls_Parenti

                            Par.Leggi(Session("DC_OSPITE"), CodiceOspite, 3)

                            Dim ParD As New Cls_DatiOspiteParenteCentroServizio

                            ParD.CentroServizio = Cserv
                            ParD.CodiceOspite = CodiceOspite
                            ParD.CodiceParente = 3
                            ParD.Leggi(Session("DC_OSPITE"))
                            If ParD.CodiceOspite <> 0 Then
                                ParD.ModalitaPagamento = ParD.ModalitaPagamento
                            End If
                            If ParD.ModalitaPagamento = DD_ModalitaPagamento.SelectedValue Then
                                Visualizzare = True
                            End If
                        End If
                        If Not Visualizzare Then
                            ImportoOspite = 0
                            ImportoParente = 0
                            ImportoComune = 0
                            ImportoRegione = 0
                            ImportoAddebitiOspite = 0
                            ImportoAccreditiOspite = 0

                            ImportoAddebitiParente = 0
                            ImportoAccreditiParente = 0
                            ImportoAddebitiComune = 0
                            ImportoAccreditiComune = 0
                            ImportoAddebitiJolly = 0
                            ImportoAccreditiJolly = 0

                            ImportoAddebitiRegione = 0
                            ImportoAccreditiRegione = 0
                        End If
                    End If

                    If ImportoOspite > 0 Or ImportoParente > 0 Or ImportoComune > 0 Or ImportoRegione > 0 Or ImportoAddebitiOspite > 0 Or ImportoAccreditiOspite > 0 Or _
                       ImportoAddebitiParente > 0 Or ImportoAccreditiParente > 0 Or ImportoAddebitiJolly > 0 Or ImportoAccreditiJolly > 0 Or ImportoAddebitiComune > 0 Or ImportoAccreditiComune > 0 Or ImportoAddebitiRegione > 0 Or ImportoAccreditiRegione > 0 Then

                        Dim myriga As System.Data.DataRow = MyTable.NewRow()

                        Dim XR As New ClsOspite

                        XR.Leggi(ConnectionString, CodiceOspite)


                        myriga("Nome") = XR.Nome
                        'myriga("Nome") = XR.CODICEFISCALE


                        If GiorniPresO > 0 Or GiorniAssO Then
                            myriga("Giorni Presenza") = GiorniPresO
                            myriga("Giorni Assenza") = GiorniAssO
                        Else
                            If GiorniPresC > 0 Or GiorniAssC Then
                                myriga("Giorni Presenza") = GiorniPresC
                                myriga("Giorni Assenza") = GiorniAssC
                            Else
                                If GiorniPresR > 0 Or GiorniAssR Then
                                    myriga("Giorni Presenza") = GiorniPresR
                                    myriga("Giorni Assenza") = GiorniAssR
                                Else
                                    If GiorniPresP > 0 Or GiorniAssP Then
                                        myriga("Giorni Presenza") = GiorniPresP
                                        myriga("Giorni Assenza") = GiorniAssP
                                    End If
                                End If
                            End If
                        End If
                        If Chk_Sintetica.Checked = False Then

                            Dim RTot As New Cls_rettatotale

                            RTot.CENTROSERVIZIO = Cserv
                            RTot.CODICEOSPITE = CodiceOspite

                            RTot.UltimaData(Session("DC_OSPITE"), CodiceOspite, Cserv)

                            Dim dec As New Cls_TipoRetta

                            dec.Descrizione = ""
                            If RTot.TipoRetta <> "" Then
                                dec.Codice = RTot.TipoRetta
                                dec.Leggi(Session("DC_OSPITE"), dec.Codice)
                            End If

                            myriga("Tipo Retta") = dec.Descrizione

                            Dim Extra As New Cls_ExtraFisso
                            Extra.Data = RTot.Data
                            Extra.CODICEOSPITE = CodiceOspite
                            Extra.CENTROSERVIZIO = Cserv
                            myriga("Extra Fissi") = Extra.ElencaExtrafissiTesto(Session("DC_OSPITE"))

                            If CDbl(ImportoOspite) > 0 Then
                                Dim Kj As New Cls_DatiOspiteParenteCentroServizio

                                Kj.TipoOperazione = ""
                                Kj.CodiceOspite = CodiceOspite
                                Kj.CentroServizio = Cserv
                                Kj.CodiceParente = 0
                                Kj.Leggi(Session("DC_OSPITE"))

                                If Kj.TipoOperazione <> "" Then
                                    XR.TIPOOPERAZIONE = Kj.TipoOperazione
                                End If

                                Dim DecTO As New Cls_TipoOperazione

                                DecTO.Codice = XR.TIPOOPERAZIONE
                                DecTO.Leggi(Session("DC_OSPITE"), DecTO.Codice)
                                myriga("Tipo Operazione Ospite") = DecTO.Descrizione
                            End If


                            If CDbl(ImportoParente) > 0 Then
                                Dim Par As New Cls_Parenti

                                Dim KjP As New Cls_DatiOspiteParenteCentroServizio

                                Par.CodiceOspite = CodiceOspite
                                Par.CodiceParente = 1
                                Par.Leggi(Session("DC_OSPITE"), CodiceOspite, 1)

                                KjP.TipoOperazione = ""
                                KjP.CodiceOspite = CodiceOspite
                                KjP.CentroServizio = Cserv
                                KjP.CodiceParente = 1
                                KjP.Leggi(Session("DC_OSPITE"))

                                If KjP.TipoOperazione <> "" Then
                                    Par.TIPOOPERAZIONE = KjP.TipoOperazione
                                End If
                                If Par.TIPOOPERAZIONE <> "" Then
                                    Dim DecTOP As New Cls_TipoOperazione

                                    DecTOP.Codice = Par.TIPOOPERAZIONE
                                    DecTOP.Leggi(Session("DC_OSPITE"), DecTOP.Codice)
                                    myriga("Tipo Operazione Parente") = DecTOP.Descrizione
                                Else
                                    Dim Kj1 As New Cls_DatiOspiteParenteCentroServizio

                                    Par.CodiceOspite = CodiceOspite
                                    Par.CodiceParente = 2
                                    Par.Leggi(Session("DC_OSPITE"), CodiceOspite, 2)

                                    Kj1.TipoOperazione = ""
                                    Kj1.CodiceOspite = CodiceOspite
                                    Kj1.CentroServizio = Cserv
                                    Kj1.CodiceParente = 2
                                    Kj1.Leggi(Session("DC_OSPITE"))

                                    If Kj1.TipoOperazione <> "" Then
                                        Par.TIPOOPERAZIONE = Kj1.TipoOperazione
                                    End If
                                    If Par.TIPOOPERAZIONE <> "" Then
                                        Dim DecTO1 As New Cls_TipoOperazione

                                        DecTO1.Codice = Par.TIPOOPERAZIONE
                                        DecTO1.Leggi(Session("DC_OSPITE"), DecTO1.Codice)
                                        myriga("Tipo Operazione Parente") = myriga("Tipo Operazione Parente") & "," & DecTO1.Descrizione
                                    End If
                                End If
                            End If
                        End If


                        myriga("Retta Ospite") = Math.Round(CDbl(ImportoOspite), 2)
                        myriga("Retta Parenti") = Math.Round(CDbl(ImportoParente), 2)
                        myriga("Retta Comune") = Math.Round(CDbl(ImportoComune), 2)
                        myriga("Retta Regione") = Math.Round(CDbl(ImportoRegione), 2)
                        myriga("Retta Jolly") = Math.Round(CDbl(ImportoJolly), 2)

                        myriga("Addebiti Ospite") = Math.Round(CDbl(ImportoAddebitiOspite), 2)
                        myriga("Accrediti Ospite") = Math.Round(CDbl(ImportoAccreditiOspite), 2)

                        myriga("Addebiti Parente") = Math.Round(CDbl(ImportoAddebitiParente), 2)
                        myriga("Accrediti Parente") = Math.Round(CDbl(ImportoAccreditiParente), 2)

                        myriga("Addebiti Comune") = Math.Round(CDbl(ImportoAddebitiComune), 2)
                        myriga("Accrediti Comune") = Math.Round(CDbl(ImportoAccreditiComune), 2)

                        myriga("Addebiti Jolly") = Math.Round(CDbl(ImportoAddebitiJolly), 2)
                        myriga("Accrediti Jolly") = Math.Round(CDbl(ImportoAccreditiJolly), 2)

                        myriga("Addebiti Regione") = Math.Round(CDbl(ImportoAddebitiRegione), 2)
                        myriga("Accrediti Regione") = Math.Round(CDbl(ImportoAccreditiRegione), 2)

                        myriga("CodiceOspite") = CodiceOspite



                        If Chk_GGSanitario.Checked = True Then

                            myriga("GG Sanitario Presenze") = GiorniPresR
                            myriga("GG Sanitario Assenze") = GiorniAssR
                        End If
                        If Chk_Ricalcolati.Checked = True Then
                            If Mese = 0 Then
                                myriga("GG Ric. Presenze") = GiorniPresenza(Cserv, CodiceOspite, DateSerial(Anno, 1, 1), DateSerial(Anno, 12, GiorniMese(12, Anno)))
                                myriga("GG Ric. Assenze") = GiorniAssenza(Cserv, CodiceOspite, DateSerial(Anno, 1, 1), DateSerial(Anno, 12, GiorniMese(12, Anno)))
                            Else
                                myriga("GG Ric. Presenze") = GiorniPresenza(Cserv, CodiceOspite, DateSerial(Anno, Mese, 1), DateSerial(Anno, Mese, GiorniMese(Mese, Anno)))
                                myriga("GG Ric. Assenze") = GiorniAssenza(Cserv, CodiceOspite, DateSerial(Anno, Mese, 1), DateSerial(Anno, Mese, GiorniMese(Mese, Anno)))
                            End If
                        End If
                        MyTable.Rows.Add(myriga)
                    End If
                Loop
                myPOSTreader.Close()
            End If
        Next
        cn.Close()


        Dim tb As Integer
        Dim cs As Integer
        Dim VettoreColonne(100) As Double
        For tb = 0 To MyTable.Rows.Count - 1
            For cs = 1 To MyTable.Columns.Count - 1
                Try
                    VettoreColonne(cs) = VettoreColonne(cs) + CDbl(MyTable.Rows(tb).Item(cs).ToString)
                Catch ex As Exception
                    VettoreColonne(cs) = VettoreColonne(cs) + 0
                End Try
            Next
        Next

        Dim myrigaTot As System.Data.DataRow = MyTable.NewRow()

        myrigaTot.Item(0) = "zzz-Totale"
        For cs = 1 To MyTable.Columns.Count - 1
            myrigaTot.Item(cs) = VettoreColonne(cs)
        Next
        MyTable.Rows.Add(myrigaTot)



        Dim view As System.Data.DataView = MyTable.DefaultView
        view.Sort = "Nome"


        Dim myriga1 As System.Data.DataRow = MyTable.NewRow()
        MyTable.Rows.Add(myriga1)

        GridView1.AutoGenerateColumns = True
        GridView1.DataSource = MyTable
        GridView1.Font.Size = 10
        GridView1.DataBind()



        If MySelectedCserv <> "" Then
            Dim i As Integer

            For i = 0 To GridView1.Rows.Count - 1

                Dim jk As Integer
                If Chk_GGSanitario.Checked = False And Chk_Ricalcolati.Checked = False Then
                    jk = Val(GridView1.Rows(i).Cells(MyTable.Columns.Count - 1).Text)
                End If
                If Chk_GGSanitario.Checked = True And Chk_Ricalcolati.Checked = False Then
                    jk = Val(GridView1.Rows(i).Cells(MyTable.Columns.Count - 3).Text)
                End If
                If Chk_GGSanitario.Checked = False And Chk_Ricalcolati.Checked = True Then
                    jk = Val(GridView1.Rows(i).Cells(MyTable.Columns.Count - 3).Text)
                End If
                If Chk_GGSanitario.Checked = True And Chk_Ricalcolati.Checked = True Then
                    jk = Val(GridView1.Rows(i).Cells(MyTable.Columns.Count - 5).Text)
                End If

                If jk > 0 Then
                    GridView1.Rows(i).Cells(0).Text = "<a href=""#"" onclick=""DialogBox('Anagrafica.aspx?CodiceOspite=" & jk & "&CentroServizio=" & Cmb_CServ.SelectedValue & "');"" >" & GridView1.Rows(i).Cells(0).Text & "</a>"
                End If
            Next
        End If

    End Sub

    Private Function GiorniAssenza(ByVal CServ As String, ByVal CodiceOspite As Integer, ByVal DataDal As Date, ByVal DataAl As Date) As Integer
        Dim ConnectionString As String = Session("DC_OSPITE")
        Dim MySql As String
        Dim cn As OleDbConnection
        Dim I As Integer
        GiorniAssenza = 0
        cn = New Data.OleDb.OleDbConnection(ConnectionString)

        cn.Open()

        If CodiceOspite = 347 Then
            CodiceOspite = 347
        End If
        Dim M As New Cls_Parametri

        M.LeggiParametri(ConnectionString)



        For I = 0 To DateDiff("d", DataDal, DataAl)


            MySql = "select top 1 * from Movimenti Where data <= ? and CENTROSERVIZIO = ? And CodiceOspite = ? order by data desc,PROGRESSIVO "

            Dim cmd As New OleDbCommand()
            cmd.CommandText = MySql
            cmd.Connection = cn
            cmd.Parameters.AddWithValue("@Data", DateAdd("d", I, DataDal))
            cmd.Parameters.AddWithValue("@Cserv", Cmb_CServ.SelectedValue)
            cmd.Parameters.AddWithValue("@CodiceOspite", CodiceOspite)
            Dim myPOSTreader As OleDbDataReader = cmd.ExecuteReader()
            Do While myPOSTreader.Read
                If campodbd(myPOSTreader.Item("DATA")) = DateAdd(DateInterval.Day, I, DataDal) Then
                    Dim tCausale As New Cls_CausaliEntrataUscita

                    tCausale.Codice = campodb(myPOSTreader.Item("CAUSALE"))
                    tCausale.LeggiCausale(ConnectionString)
                    If M.GIORNOUSCITA = "P" And campodb(myPOSTreader.Item("TIPOMOV")) = "03" Then
                    Else
                        If tCausale.GiornoUscita = "A" Then
                            GiorniAssenza = GiorniAssenza + 1
                        End If
                    End If
                Else
                    Dim tCausale As New Cls_CausaliEntrataUscita

                    tCausale.Codice = campodb(myPOSTreader.Item("CAUSALE"))
                    tCausale.LeggiCausale(ConnectionString)
                    If campodb(myPOSTreader.Item("TIPOMOV")) = "03" Then
                        If tCausale.TIPOMOVIMENTO = "A" Then
                            GiorniAssenza = GiorniAssenza + 1
                        End If
                    End If
                    If campodb(myPOSTreader.Item("TIPOMOV")) = "04" Then
                        REM GiorniPresenza = GiorniPresenza + 1
                    End If
                End If
            Loop
            myPOSTreader.Close()            
            cmd.Dispose()
        Next

    End Function

    Private Function GiorniPresenza(ByVal CServ As String, ByVal CodiceOspite As Integer, ByVal DataDal As Date, ByVal DataAl As Date) As Integer
        Dim ConnectionString As String = Session("DC_OSPITE")
        Dim MySql As String
        Dim cn As OleDbConnection
        Dim I As Integer
        GiorniPresenza = 0
        cn = New Data.OleDb.OleDbConnection(ConnectionString)

        cn.Open()

        If CodiceOspite = 864 Then
            CodiceOspite = 864
        End If

        Dim M As New Cls_Parametri

        M.LeggiParametri(ConnectionString)


        For I = 0 To DateDiff("d", DataDal, DataAl)


            MySql = "select top 1 * from Movimenti Where data <= ? and CENTROSERVIZIO = ? And CodiceOspite = ? order by data desc,PROGRESSIVO desc "

            Dim cmd As New OleDbCommand()
            cmd.CommandText = MySql
            cmd.Connection = cn
            cmd.Parameters.AddWithValue("@Data", DateAdd("d", I, DataDal))
            cmd.Parameters.AddWithValue("@Cserv", Cmb_CServ.SelectedValue)
            cmd.Parameters.AddWithValue("@CodiceOspite", CodiceOspite)
            Dim myPOSTreader As OleDbDataReader = cmd.ExecuteReader()
            Do While myPOSTreader.Read
                If campodbd(myPOSTreader.Item("DATA")) = DateAdd(DateInterval.Day, I, DataDal) Then
                    Dim tCausale As New Cls_CausaliEntrataUscita

                    tCausale.Codice = campodb(myPOSTreader.Item("CAUSALE"))
                    tCausale.LeggiCausale(ConnectionString)
                    If tCausale.GiornoUscita = "P" Then
                        GiorniPresenza = GiorniPresenza + 1
                    End If
                    If tCausale.Codice = "" Then
                        GiorniPresenza = GiorniPresenza + 1
                    End If

                    If M.GIORNOUSCITA = "P" Then
                        If campodb(myPOSTreader.Item("TIPOMOV")) = "03" Then
                            If tCausale.TIPOMOVIMENTO = "A" Then
                                GiorniPresenza = GiorniPresenza + 1
                            End If
                        End If
                    End If
                Else
                    Dim tCausale As New Cls_CausaliEntrataUscita

                    tCausale.Codice = campodb(myPOSTreader.Item("CAUSALE"))
                    tCausale.LeggiCausale(ConnectionString)
                    If campodb(myPOSTreader.Item("TIPOMOV")) = "03" Then
                        If tCausale.TIPOMOVIMENTO = "P" Then
                            GiorniPresenza = GiorniPresenza + 1
                        End If
                    End If
                    If campodb(myPOSTreader.Item("TIPOMOV")) = "04" Then
                        GiorniPresenza = GiorniPresenza + 1
                    End If
                    If campodb(myPOSTreader.Item("TIPOMOV")) = "05" Then
                        GiorniPresenza = GiorniPresenza + 1
                    End If
                End If
            Loop
            myPOSTreader.Close()
            cmd.Dispose()
        Next

    End Function

    Function campodb(ByVal oggetto As Object) As String
        If IsDBNull(oggetto) Then
            Return ""
        Else
            Return oggetto
        End If
    End Function
    Function campodbd(ByVal oggetto As Object) As Date
        If IsDBNull(oggetto) Then
            Return Nothing
        Else
            Return oggetto
        End If
    End Function
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load

        Call EseguiJS()
        If Page.IsPostBack = True Then
            Exit Sub
        End If


        Dim K1 As New Cls_SqlString

        Dim Barra As New Cls_BarraSenior

        If Not IsNothing(Session("RicercaAnagraficaSQLString")) Then
            K1 = Session("RicercaAnagraficaSQLString")
        End If

        Lbl_BarraSenior.Text = Barra.CodiceBarra(Application("SENIOR"), Session("UTENTE"), Page, K1)

        Lbl_Errori.Text = ""
        If Request.Item("Errore") = "NONDATI" Then
            REM Lbl_Errori.Text = "Nessun dato estratto"
            ClientScript.RegisterClientScriptBlock(Me.GetType(), "Errore", "VisualizzaErrore('Nessun dato estratto');", True)
        End If

        Dim k As New Cls_Parametri

        k.LeggiParametri(Session("DC_OSPITE"))

        Txt_Anno.Text = k.AnnoFatturazione
        Dd_Mese.SelectedValue = k.MeseFatturazione

        Lbl_Utente.Text = Session("UTENTE")

        Dim MP As New Cls_ModalitaPagamento

        MP.UpDateDropBox(Session("DC_OSPITE"), DD_ModalitaPagamento)


        Dim ConnectionString As String = Session("DC_OSPITE")

        Dim kVilla As New Cls_TabelleDescrittiveOspitiAccessori


        kVilla.UpDateDropBox(Session("DC_OSPITIACCESSORI"), "VIL", DD_Struttura)
        If DD_Struttura.Items.Count = 1 Then
            Call AggiornaCServ()
        End If

    End Sub


    
    Protected Sub ImageButton3_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles ImageButton3.Click
        If Val(Txt_Anno.Text) = 0 Then
            REM ClientScript.RegisterClientScriptBlock(Me.GetType(), "Errore", "VisualizzaErrore('Specificare anno');", True)
            ScriptManager.RegisterStartupScript(Me, Me.GetType(), "visualizzaA", "VisualizzaErrore('Specificare anno');", True)
            Exit Sub
        End If

        'If Cmb_CServ.SelectedValue = "" Then
        '    ClientScript.RegisterClientScriptBlock(Me.GetType(), "Errore", "VisualizzaErrore('Specificare centro servizio');", True)
        '    Exit Sub
        'End If

        Call CaricaGriglia()
    End Sub

    Protected Sub ImageButton4_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles ImageButton4.Click
        If Val(Txt_Anno.Text) = 0 Then
            ClientScript.RegisterClientScriptBlock(Me.GetType(), "Errore", "VisualizzaErrore('Specificare anno');", True)
            Exit Sub
        End If
        'If Cmb_CServ.SelectedValue = "" Then
        '    ClientScript.RegisterClientScriptBlock(Me.GetType(), "Errore", "VisualizzaErrore('Specificare centro servizio');", True)
        '    Exit Sub
        'End If

        Call CaricaGriglia()
        If MyTable.Rows.Count > 1 Then
            Response.Clear()
            Response.AddHeader("content-disposition", "attachment;filename=VerificaRette.xls")
            
            Response.Charset = String.Empty
            Response.Cache.SetCacheability(HttpCacheability.NoCache)
            Response.ContentType = "application/vnd.ms-excel"
            Dim stringWrite As New System.IO.StringWriter
            Dim htmlWrite As New HtmlTextWriter(stringWrite)

            form1.Controls.Clear()
            form1.Controls.Add(GridView1)

            form1.RenderControl(htmlWrite)

            Response.Write(stringWrite.ToString())
            Response.End()
        End If
    End Sub

    Protected Sub Btn_Esci_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles Btn_Esci.Click
        Response.Redirect("Menu_Visualizzazioni.aspx")
    End Sub

    Private Sub AggiornaCServ()
        Dim kCsrv As New Cls_CentroServizio

        If DD_Struttura.SelectedValue = "" Then
            kCsrv.UpDateDropBox(Session("DC_OSPITE"), Cmb_CServ)
        Else
            kCsrv.UpDateDropBoxStruttura(Session("DC_OSPITE"), Cmb_CServ, DD_Struttura.SelectedValue)
        End If
    End Sub

    Private Sub EseguiJS()
        Dim MyJs As String
        MyJs = "$(document).ready(function() {"

        MyJs = MyJs & "if (window.innerHeight>0) { $(""#BarraLaterale"").css(""height"",(window.innerHeight - 94) + ""px""); } else"
        MyJs = MyJs & "{ $(""#BarraLaterale"").css(""height"",(document.documentElement.offsetHeight - 94) + ""px"");  }"
        MyJs = MyJs & "});"

        'MyJs = "$(document).ready(function() { $('.myClass').keypress(function() { handleEnter($('.myClass').val(), true, true); } );     $('#" & Txt_ImportoPagato.ClientID & "').blur(function() { var ap = formatNumber ($('#" & Txt_ImportoPagato.ClientID & "').val(),2); $('#" & Txt_ImportoPagato.ClientID & "').val(ap); }); });"
        ScriptManager.RegisterStartupScript(Me, Me.GetType(), "visualizzaRitIm", MyJs, True)
    End Sub



    Protected Sub DD_Struttura_TextChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles DD_Struttura.TextChanged
        AggiornaCServ()
    End Sub

    Protected Sub Btn_Stampa_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles Btn_Stampa.Click

        
        If Val(Txt_Anno.Text) = 0 Then
            REM ClientScript.RegisterClientScriptBlock(Me.GetType(), "Errore", "VisualizzaErrore('Specificare anno');", True)
            ScriptManager.RegisterStartupScript(Me, Me.GetType(), "visualizzaA", "VisualizzaErrore('Specificare anno');", True)
            Exit Sub
        End If

        'If Cmb_CServ.SelectedValue = "" Then
        '    ClientScript.RegisterClientScriptBlock(Me.GetType(), "Errore", "VisualizzaErrore('Specificare centro servizio');", True)
        '    Exit Sub
        'End If

        Call CaricaGriglia()

        Session("GrigliaSoloStampa") = MyTable

        Session("ParametriSoloStampa") = ""

        ScriptManager.RegisterStartupScript(Me, Me.GetType(), "Stampa", "openPopUp('GrigliaSoloStampa.aspx','Stampe','width=800,height=600');", True)

    End Sub
End Class
