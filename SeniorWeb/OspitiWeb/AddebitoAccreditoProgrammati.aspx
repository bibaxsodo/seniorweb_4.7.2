﻿<%@ Page Language="VB" AutoEventWireup="false" Inherits="OspitiWeb_AddebitoAccreditoProgrammati" CodeFile="AddebitoAccreditoProgrammati.aspx.vb" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="xasp" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
    <meta http-equiv="x-ua-compatible" content="IE=9" />
    <title>Addebiti Accrediti Programmati</title>
    <asp:PlaceHolder runat="server">
        <%: System.Web.Optimization.Styles.Render("~/Content/AjaxControlToolkit/Styles/Bundle") %>
    </asp:PlaceHolder>
    <link href="ospiti.css?ver=11" rel="stylesheet" type="text/css" />
    <link rel="shortcut icon" href="images/SENIOR.ico" />
    <link href="js/jquery.autocomplete.css" rel="stylesheet" type="text/css" />


    <script src="js/jquery-1.5.1.min.js" type="text/javascript"></script>
    <script src="js/jquery.autocomplete.js" type="text/javascript"></script>
    <script src="js/soapclient.js" type="text/javascript"></script>
    <script src="/js/convertinumero.js" type="text/javascript"></script>

    <link rel="stylesheet" href="dialogbox/redips-dialog.css" type="text/css" media="screen" />
    <script type="text/javascript" src="dialogbox/redips-dialog-min.js"></script>

    <script src="js/jquery.maskedinput-1.3.min.js" type="text/javascript"></script>
    <script src="/js/formatnumer.js" type="text/javascript"></script>
    <script src="js/JSErrore.js" type="text/javascript"></script>
    <script src="js/NoEnter.js" type="text/javascript"></script>
    <script type="text/javascript" src="../js/menuanagraficaospiti.js?ver=7"></script>

    <link rel="stylesheet" href="dialogbox/redips-dialog.css" type="text/css" media="screen" />
    <script type="text/javascript" src="dialogbox/redips-dialog-min.js"></script>
    <script type="text/javascript" src="dialogbox/script.js"></script>

    <link rel="stylesheet" href="js/chosen/chosen.css">

    <link rel="stylesheet" href="jqueryui/jquery-ui.css" type="text/css">
    <script src="jqueryui/jquery-ui.js" type="text/javascript"></script>
    <script src="jqueryui/datapicker-it.js" type="text/javascript"></script>


    <script src="js/chosen/chosen.jquery.js" type="text/javascript"></script>
    <script type="text/javascript">         
        $(document).ready(function () {
            $('html').keyup(function (event) {

                if (event.keyCode == 113) {
                    __doPostBack("Btn_Modifica0", "0");
                }

            });
        });

        $(document).ready(function () {
            if (window.innerHeight > 0) { $("#BarraLaterale").css("height", (window.innerHeight - 94) + "px"); } else { $("#BarraLaterale").css("height", (document.documentElement.offsetHeight - 94) + "px"); }
        });

        function DialogBox(Path) {
            REDIPS.dialog.show(500, 300, '<iframe id="output" src="' + Path + '" height="300px" width="500"></iframe>');
            return false;

        }
    </script>
    <style>
        .chosen-container-single .chosen-single {
            height: 30px;
            background: white;
            color: Black;
            font-family: "Cuprum","Roboto","Calibri",Lucida Grande,"Helvetica Neue",Helvetica,Arial,sans-serif;
            font-size: 16px;
        }

        .chosen-container {
            font-family: "Cuprum","Roboto","Calibri",Lucida Grande,"Helvetica Neue",Helvetica,Arial,sans-serif;
            font-size: 16px;
        }
    </style>
</head>
<body>
    <form id="form1" runat="server">
        <asp:ScriptManager ID="ScriptManager1" runat="server" EnableScriptGlobalization="true">
            <Scripts>
                <asp:ScriptReference Path="~/Scripts/AjaxControlToolkit/Bundle" />
            </Scripts>
        </asp:ScriptManager>
        <asp:Label ID="Lbl_BarraSenior" runat="server" Text=""></asp:Label>
        <div align="left">
            <table style="width: 100%;" cellpadding="0" cellspacing="0">
                <tr>
                    <td style="width: 160px; background-color: #F0F0F0;"></td>
                    <td>
                        <div class="Titolo">Ospiti - Anagrafica - Addebiti Accrediti</div>
                        <div class="SottoTitoloOSPITE">
                            <asp:Label ID="Lbl_NomeOspite" runat="server"></asp:Label><br />
                            <br />
                        </div>
                    </td>
                    <td style="text-align: right;">
                        <div class="DivTastiOspite">
                            <asp:ImageButton ID="Btn_Modifica0" runat="server" Height="38px" ImageUrl="~/images/salva.jpg" class="EffettoBottoniTondi" Width="38px" ToolTip="Modifica / Inserisci (F2)" />
                            <asp:ImageButton ID="Btn_Elimina" OnClientClick="return window.confirm('Eliminare?');" runat="server" Height="38px" ImageUrl="~/images/elimina.jpg" class="EffettoBottoniTondi" Width="38px" ToolTip="Elimina" />
                        </div>
                    </td>
                </tr>

                <tr>
                    <td style="width: 160px; background-color: #F0F0F0; vertical-align: top; text-align: center;" id="BarraLaterale">
                        <a href="Menu_Ospiti.aspx" style="border-width: 0px;">
                            <img src="images/Home.jpg" alt="Menù" class="Effetto" /></a><br />
                        <asp:ImageButton ID="Btn_Esci" runat="server" BackColor="Transparent" ImageUrl="../images/Menu_Indietro.png" class="Effetto" ToolTip="Chiudi" />
                        <br />

                        <div id="MENUDIV"></div>
                        <br />
                        <br />
                        <br />
                        <br />
                        <br />
                        <br />
                        <br />
                        <br />
                        <br />
                        <br />
                        <br />
                        <br />
                    </td>
                    <td colspan="2" style="background-color: #FFFFFF; vertical-align: top;">
                        <xasp:TabContainer ID="TabContainer1" runat="server" ActiveTabIndex="0" CssClass="TabSenior"
                            Width="100%" BorderStyle="None" Style="margin-right: 39px">
                            <xasp:TabPanel runat="server" HeaderText="Prima Nota" ID="Tab_Anagrafica">
                                <HeaderTemplate>
                                    Addebiti Accrediti
                                </HeaderTemplate>
                                <ContentTemplate>



                                    <asp:UpdatePanel ID="UpdatePanel1" runat="server">
                                        <ContentTemplate>


                                            <label class="LabelCampo">Tipo Movimento :</label>
                                            <asp:RadioButton ID="RB_ADDEBITO" GroupName="mTIPOAD" runat="server" Text="Addebito" ForeColor="red" Checked="True" />
                                            <asp:RadioButton ID="RB_ACCREDITO" GroupName="mTIPOAD" runat="server" Text="Accredito" ForeColor="green" /><br />
                                            <br />

                                            <label class="LabelCampo">Data Inizio:</label>
                                            <asp:TextBox ID="Txt_DataInizio" runat="server" Width="90px"></asp:TextBox>
                                            <asp:Label ID="Lbl_Progressivo" runat="server" Text="0" Width="136px"></asp:Label><br />
                                            <br />

                                            <label class="LabelCampo">Data Fine:</label>
                                            <asp:TextBox ID="Txt_DataFine" runat="server" Width="90px"></asp:TextBox>
                                            <asp:Label ID="Label1" runat="server" Text="0" Width="136px"></asp:Label><br />
                                            <br />



                                            <label class="LabelCampo">Importo :</label>
                                            <asp:TextBox ID="Txt_Importo" Style="text-align: right;" onkeypress="ForceNumericInput(this, true, true)" runat="server" Width="120px"></asp:TextBox><br />
                                            <br />

                                            <label class="LabelCampo">Descrizione :</label>
                                            <asp:TextBox ID="Txt_Descrizione" MaxLength="150" runat="server"
                                                Width="450px"></asp:TextBox><br />
                                            <br />

                                            <label class="LabelCampo">Tipo Addebito :</label>
                                            <asp:DropDownList ID="dd_TipoAddebito" class="chosen-select" runat="server" Width="450px">
                                            </asp:DropDownList>
                                            <br />

                                            <br />
                                            <label class="LabelCampo">Tipo Ripetizione :</label>
                                            <asp:DropDownList ID="DD_Tipo" runat="server" Width="460px">
                                                <asp:ListItem Value="G" Text="Giorni Mese"></asp:ListItem>
                                                <asp:ListItem Value="P" Text="Giorni Presenza"></asp:ListItem>
                                                <asp:ListItem Value="L" Text="Giorni Presenza Lavorativi"></asp:ListItem>
                                                <asp:ListItem Value="F" Text="Giorni Presenza Festivi"></asp:ListItem>
                                                <asp:ListItem Value="A" Text="Giorni Assenza"></asp:ListItem>
                                                <asp:ListItem Value="M" Text="Mensile"></asp:ListItem>
                                                <asp:ListItem Value="N" Text="Mensile/Presenze"></asp:ListItem>
                                            </asp:DropDownList>
                                            <br />
                                            <br />


                                            <label class="LabelCampo">Ospite :</label>
                                            <asp:RadioButton ID="RB_OSPITE" runat="server" GroupName="TIPO"
                                                Checked="True" />


                                            <br />


                                            <br />


                                            <label class="LabelCampo">Parente :</label>
                                            <asp:RadioButton ID="RB_PARENTE" runat="server" GroupName="TIPO" />
                                            : 
         <asp:DropDownList ID="DD_Parente" runat="server" Width="430px"></asp:DropDownList><br />
                                            <br />

                                            <label class="LabelCampo">Comune :</label>
                                            <asp:RadioButton ID="RB_COMUNE" runat="server" GroupName="TIPO" />: 
         <asp:DropDownList ID="DD_Comune" runat="server" class="chosen-select"></asp:DropDownList>
                                            <br />
                                            <br />

                                            <label class="LabelCampo">Regione :</label>
                                            <asp:RadioButton ID="Rb_Regione" runat="server" GroupName="TIPO" />:
         <asp:DropDownList ID="DD_Regione" runat="server" class="chosen-select" Width="336px"></asp:DropDownList><br />
                                            <br />

                                            <label class="LabelCampo">Rendiconto :</label>
                                            <asp:CheckBox ID="ChK_Rendiconto" runat="server" Text="" />

                                            <br />
                                        </ContentTemplate>
                                    </asp:UpdatePanel>



                                </ContentTemplate>


                            </xasp:TabPanel>
                        </xasp:TabContainer>
                    </td>
                </tr>
            </table>


        </div>

    </form>
</body>
</html>
