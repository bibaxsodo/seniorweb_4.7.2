﻿Imports System.Web.Hosting
Imports System.Data.OleDb

Partial Class OspitiWeb_AggiornaRette
    Inherits System.Web.UI.Page
    Dim Tabella As New System.Data.DataTable("tabella")

    Function campodb(ByVal oggetto As Object) As String
        If IsDBNull(oggetto) Then
            Return ""
        Else
            Return oggetto
        End If
    End Function


    Protected Sub Btn_Visualizza_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles Btn_Visualizza.Click
        If DD_CentroServizio.SelectedValue = "" Then
            ClientScript.RegisterClientScriptBlock(Me.GetType(), "Errore", "VisualizzaErrore('Specifica centroservizio');", True)
            Exit Sub
        End If

        If Not IsDate(Txt_DataRegistrazione.Text) Then
            ClientScript.RegisterClientScriptBlock(Me.GetType(), "Errore", "VisualizzaErrore('Data validita formalmente errata');", True)
            Exit Sub
        End If

        Call Ricerca()

    End Sub

    Private Sub Ricerca()
        Dim cn As OleDbConnection


        cn = New Data.OleDb.OleDbConnection(Session("DC_OSPITE"))

        cn.Open()
        Dim cmd As New OleDbCommand()
        Dim MySql As String
        Dim DataRegistrazione As Date


        DataRegistrazione = Txt_DataRegistrazione.Text

        MySql = "SELECT     CODICEOSPITE,"
        MySql = MySql & "(SELECT     MAX(DATA) AS Expr1"
        MySql = MySql & " FROM          MOVIMENTI"
        MySql = MySql & "   WHERE      (CODICEOSPITE = AnagraficaComune.CODICEOSPITE) AND (TIPOMOV = '13') AND (CENTROSERVIZIO = '" & DD_CentroServizio.SelectedValue & "')) "
        MySql = MySql & " AS DataUscitaDefinitiva,"
        MySql = MySql & "(SELECT     MAX(DATA) AS Expr1"
        MySql = MySql & " FROM          MOVIMENTI AS MOVIMENTI_1"
        MySql = MySql & "    WHERE      (CODICEOSPITE = AnagraficaComune.CODICEOSPITE) AND (TIPOMOV = '05') AND (CENTROSERVIZIO = '" & DD_CentroServizio.SelectedValue & "')) "
        MySql = MySql & " AS DataAccoglimento"
        MySql = MySql & " FROM         AnagraficaComune WHERE     (TIPOLOGIA = 'O') Order by Nome"

        cmd.CommandText = MySql
        cmd.Connection = cn

        Tabella.Clear()
        Tabella.Columns.Clear()
        Tabella.Columns.Add("CodiceOspite", GetType(Integer))
        Tabella.Columns.Add("NomeOspite", GetType(String))
        Tabella.Columns.Add("Data", GetType(String))
        Tabella.Columns.Add("ImportoAttuale", GetType(String))
        Tabella.Columns.Add("Importo", GetType(String))
        Tabella.Columns.Add("StessaData", GetType(String))
        Tabella.Columns.Add("QSANITARIA", GetType(String))
        Tabella.Columns.Add("IVA", GetType(String))

        Dim myPOSTreader As OleDbDataReader = cmd.ExecuteReader()
        Do While myPOSTreader.Read

            If campodb(myPOSTreader.Item("DataAccoglimento")) <> "" Then
                Dim CodiceOSpite As Long
                Dim DataUscita As Date
                Dim DataAccoglimento As Date

                CodiceOSpite = campodb(myPOSTreader.Item("CODICEOSPITE"))


                If campodb(myPOSTreader.Item("DataUscitaDefinitiva")) = "" Then
                    DataUscita = DateSerial(2099, 1, 1)
                Else
                    DataUscita = campodb(myPOSTreader.Item("DataUscitaDefinitiva"))
                End If

                If campodb(myPOSTreader.Item("DataAccoglimento")) = "" Then
                    DataAccoglimento = DateSerial(1900, 1, 1)
                Else
                    DataAccoglimento = campodb(myPOSTreader.Item("DataAccoglimento"))
                End If

                If CodiceOSpite = 426 Then
                    CodiceOSpite = 426
                End If



                If Format(DataUscita, "yyyyMMdd") > Format(DataRegistrazione, "yyyyMMdd") Or (Format(DataAccoglimento, "yyyyMMdd") > Format(DataRegistrazione, "yyyyMMdd")) Or (Format(DataAccoglimento, "yyyyMMdd") > Format(DataUscita, "yyyyMMdd") And Format(DataAccoglimento, "yyyyMMdd") < Format(DataRegistrazione, "yyyyMMdd")) Then

                    Dim KAuto As New Cls_StatoAuto

                    KAuto.UltimaData(Session("DC_OSPITE"), CodiceOSpite, DD_CentroServizio.SelectedValue)

                    If (KAuto.STATOAUTO = "N" And Chk_SoloNonAuto.Checked = True) Or Chk_SoloNonAuto.Checked = False Then


                        Dim XAna As New ClsOspite

                        XAna.Leggi(Session("DC_OSPITE"), CodiceOSpite)

                        Dim myriga As System.Data.DataRow = Tabella.NewRow()


                        myriga(0) = CodiceOSpite
                        myriga(1) = XAna.Nome

                        Dim Qusl As New Cls_CalcoloRette


                        Qusl.STRINGACONNESSIONEDB = Session("DC_OSPITE")
                        Qusl.ApriDB(Session("DC_OSPITE"), Session("DC_OSPITIACCESSORI"))

                        Dim KCS As New Cls_DatiOspiteParenteCentroServizio


                        KCS.CentroServizio = DD_CentroServizio.SelectedValue
                        KCS.CodiceOspite = CodiceOSpite
                        KCS.CodiceParente = 0
                        KCS.Leggi(Session("DC_OSPITE"))

                        If KCS.AliquotaIva <> "" Then
                            XAna.CODICEIVA = KCS.AliquotaIva
                        End If


                        If Qusl.QuoteGiornaliere(DD_CentroServizio.SelectedValue, CodiceOSpite, "P", 1, Now) > 0 Then


                            Dim XAnap As New Cls_Parenti


                            XAnap.Leggi(Session("DC_OSPITE"), CodiceOSpite, 1)

                            Dim KCSp As New Cls_DatiOspiteParenteCentroServizio


                            KCSp.CentroServizio = DD_CentroServizio.SelectedValue
                            KCSp.CodiceOspite = CodiceOSpite
                            KCSp.CodiceParente = 1
                            KCSp.Leggi(Session("DC_OSPITE"))

                            If KCSp.AliquotaIva <> "" Then
                                XAna.CODICEIVA = KCSp.AliquotaIva
                            End If
                        End If


                        If Qusl.QuoteGiornaliere(DD_CentroServizio.SelectedValue, CodiceOSpite, "P", 2, Now) > 0 Then

                            Dim XAnap As New Cls_Parenti


                            XAnap.Leggi(Session("DC_OSPITE"), CodiceOSpite, 2)


                            Dim KCSp As New Cls_DatiOspiteParenteCentroServizio


                            KCSp.CentroServizio = DD_CentroServizio.SelectedValue
                            KCSp.CodiceOspite = CodiceOSpite
                            KCSp.CodiceParente = 2
                            KCSp.Leggi(Session("DC_OSPITE"))

                            If KCSp.AliquotaIva <> "" Then
                                XAna.CODICEIVA = KCSp.AliquotaIva
                            End If
                        End If



                        If Qusl.QuoteGiornaliere(DD_CentroServizio.SelectedValue, CodiceOSpite, "P", 3, Now) > 0 Then

                            Dim XAnap As New Cls_Parenti


                            XAnap.Leggi(Session("DC_OSPITE"), CodiceOSpite, 3)

                            Dim KCSp As New Cls_DatiOspiteParenteCentroServizio


                            KCSp.CentroServizio = DD_CentroServizio.SelectedValue
                            KCSp.CodiceOspite = CodiceOSpite
                            KCSp.CodiceParente = 3
                            KCSp.Leggi(Session("DC_OSPITE"))

                            If KCSp.AliquotaIva <> "" Then
                                XAna.CODICEIVA = KCSp.AliquotaIva
                            End If
                        End If


                        Dim k As New Cls_rettatotale

                        k.UltimaData(Session("DC_OSPITE"), CodiceOSpite, DD_CentroServizio.SelectedValue)

                        If Format(DataAccoglimento, "yyyyMMdd") > Format(DataRegistrazione, "yyyyMMdd") Then
                            myriga(2) = Format(DataAccoglimento, "dd/MM/yyyy")
                        Else
                            myriga(2) = Format(DataRegistrazione, "dd/MM/yyyy")
                        End If
                        myriga(3) = Format(k.Importo, "#,##0.00")
                        If Format(k.Data, "yyyyMMdd") = Format(DataRegistrazione, "yyyyMMdd") Then
                            myriga(4) = "S"
                        End If

                        


                        myriga(6) = Qusl.QuoteGiornaliere(DD_CentroServizio.SelectedValue, CodiceOSpite, "R", 0, Now)

                        Dim kDec As New Cls_IVA

                        kDec.Codice = XAna.CODICEIVA
                        kDec.Leggi(Session("DC_TABELLE"), kDec.Codice)


                        myriga(7) = kDec.Descrizione


                        Tabella.Rows.Add(myriga)
                    End If
                End If
            End If
        Loop
        myPOSTreader.Close()
        cn.Close()


        ViewState("SalvaTabella") = Tabella


        Grd_AggiornaRette.AutoGenerateColumns = False
        Grd_AggiornaRette.DataSource = Tabella
        Grd_AggiornaRette.DataBind()


        For i = 0 To Grd_AggiornaRette.Rows.Count - 1
            If campodb(Tabella.Rows(i).Item(4)) = "S" Then
                Grd_AggiornaRette.Rows(i).Cells(0).ForeColor = Drawing.Color.Violet
                Grd_AggiornaRette.Rows(i).Cells(1).ForeColor = Drawing.Color.Violet
                Grd_AggiornaRette.Rows(i).Cells(2).ForeColor = Drawing.Color.Violet
                Grd_AggiornaRette.Rows(i).Cells(3).ForeColor = Drawing.Color.Violet

            End If
        Next



        Call EseguiJS()


    End Sub

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load

        If Trim(Session("UTENTE")) = "" Then
            Response.Redirect("../Login.aspx")
            Exit Sub
        End If

        If Page.IsPostBack = True Then
            Exit Sub
        End If


        Dim kVilla As New Cls_TabelleDescrittiveOspitiAccessori


        kVilla.UpDateDropBox(Session("DC_OSPITIACCESSORI"), "VIL", DD_Struttura)
        If DD_Struttura.Items.Count = 1 Then
            Call AggiornaCServ()
        End If


        Dim Barra As New Cls_BarraSenior

        Lbl_BarraSenior.Text = Barra.CodiceBarra(Application("SENIOR"), Session("UTENTE"), Page)

        Dim DDIVA As New Cls_IVA

        DDIVA.UpDateDropBox(Session("DC_TABELLE"), DD_IVA)

        Call EseguiJS()
    End Sub



    Private Sub EseguiJS()
        Dim MyJs As String
        MyJs = "$(document).ready(function() {"

        MyJs = MyJs & "var els = document.getElementsByTagName(""*"");"

        MyJs = MyJs & "for (var i=0;i<els.length;i++)"
        MyJs = MyJs & "if ( els[i].id ) { "
        MyJs = MyJs & " var appoggio =els[i].id; "


        MyJs = MyJs & " if ((appoggio.match('Txt_Importo')!= null)) {  "
        MyJs = MyJs & " $(els[i]).keypress(function() { ForceNumericInput($(this).val(), true, true); } ); "
        MyJs = MyJs & " $(els[i]).blur(function() { var ap = formatNumber($(this).val(),2); $(this).val(ap); });"
        MyJs = MyJs & "    }"

        MyJs = MyJs & " if (appoggio.match('Txt_DataRegistrazione')!= null) {  "
        MyJs = MyJs & " $(els[i]).mask(""99/99/9999"");"
        MyJs = MyJs & " $(els[i]).datepicker({ changeMonth: true,changeYear: true,  yearRange: ""1990:" & Year(Now) + 5 & """},$.datepicker.regional[""it""]);"
        MyJs = MyJs & "    }"


        MyJs = MyJs & "} "
        MyJs = MyJs & "});"

        'MyJs = "$(document).ready(function() { $('.myClass').keypress(function() { handleEnter($('.myClass').val(), true, true); } );     $('#" & Txt_ImportoPagato.ClientID & "').blur(function() { var ap = formatNumber ($('#" & Txt_ImportoPagato.ClientID & "').val(),2); $('#" & Txt_ImportoPagato.ClientID & "').val(ap); }); });"
        ScriptManager.RegisterStartupScript(Me, Me.GetType(), "visualizzaRitIm", MyJs, True)
    End Sub



    Private Sub UpDateTable()
        Dim i As Integer

        Tabella = ViewState("SalvaTabella")
   
 

    End Sub

    Protected Sub Btn_Modifica0_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles Btn_Modifica0.Click
        If DD_CentroServizio.SelectedValue = "" Then
            ClientScript.RegisterClientScriptBlock(Me.GetType(), "Errore", "VisualizzaErrore('Specifica centroservizio');", True)
            Call EseguiJS()
            Exit Sub
        End If

        If Not IsDate(Txt_DataRegistrazione.Text) Then
            ClientScript.RegisterClientScriptBlock(Me.GetType(), "Errore", "VisualizzaErrore('Data validita formalmente errata');", True)
            Call EseguiJS()
            Exit Sub
        End If


        Dim cn As OleDbConnection

        Dim DataRegistrazione As Date

        Dim DataAccoglimento As Date


        DataRegistrazione = Txt_DataRegistrazione.Text


        cn = New Data.OleDb.OleDbConnection(Session("DC_OSPITE"))

        cn.Open()

        Dim i As Integer

        Tabella = ViewState("SalvaTabella")

        For i = 0 To Grd_AggiornaRette.Rows.Count - 1


            Dim TxtImporto As TextBox = DirectCast(Grd_AggiornaRette.Rows(i).FindControl("Txt_Importo"), TextBox)

            Dim ChkAggiornamento As CheckBox = DirectCast(Grd_AggiornaRette.Rows(i).FindControl("Chk_ModifcaDopoData"), CheckBox)
            Dim Importo As Double
            Dim CODICEOSPITE As Long
            Dim MySql As String

            If TxtImporto.Text <> "" Then

                If IsNumeric(TxtImporto.Text) Then


                    If CDbl(TxtImporto.Text) > 0 Then
                        Importo = TxtImporto.Text

                        CODICEOSPITE = Tabella.Rows(i).Item(0)
                        DataAccoglimento = Tabella.Rows(i).Item(2)

                        Dim XR1 As New Cls_ExtraFisso
                        XR1.CODICEOSPITE = CODICEOSPITE
                        XR1.CENTROSERVIZIO = DD_CentroServizio.SelectedValue
                        XR1.UltimaData(Session("DC_OSPITE"))


                        If ChkAggiornamento.Checked = True Then
                            MySql = "UPDATE IMPORTORETTA SET IMPORTO = ?,Utente=?,DataAggiornamento= ? WHERE  CentroServizio = '" & DD_CentroServizio.SelectedValue & "' And  CodiceOspite = " & CODICEOSPITE & " And Data > ?"
                            Dim cmdw As New OleDbCommand()
                            cmdw.CommandText = (MySql)
                            cmdw.Parameters.AddWithValue("@IMPORTO", Importo)
                            cmdw.Parameters.AddWithValue("@UTENTE", Session("UTENTE"))
                            cmdw.Parameters.AddWithValue("@DataAggiornamento", Now)
                            cmdw.Parameters.AddWithValue("@Data", Txt_DataRegistrazione.Text)
                            cmdw.Connection = cn
                            cmdw.ExecuteNonQuery()
                        Else

                            MySql = "SELECT * FROM IMPORTORETTA WHERE  CentroServizio = '" & DD_CentroServizio.SelectedValue & "' And  CodiceOspite = " & CODICEOSPITE & " And Data = ?"
                            Dim cmdR As New OleDbCommand()
                            cmdR.CommandText = (MySql)
                            cmdR.Parameters.AddWithValue("@Data", DataAccoglimento)
                            cmdR.Connection = cn

                            Dim myPOSTreader As OleDbDataReader = cmdR.ExecuteReader()

                            If myPOSTreader.Read Then
                                MySql = "UPDATE IMPORTORETTA SET IMPORTO = ?,Utente=?,DataAggiornamento= ? WHERE  CentroServizio = '" & DD_CentroServizio.SelectedValue & "' And  CodiceOspite = " & CODICEOSPITE & " And Data = ?"
                                Dim cmdw As New OleDbCommand()
                                cmdw.CommandText = (MySql)
                                cmdw.Parameters.AddWithValue("@IMPORTO", Importo)
                                cmdw.Parameters.AddWithValue("@UTENTE", Session("UTENTE"))
                                cmdw.Parameters.AddWithValue("@DataAggiornamento", Now)
                                cmdw.Parameters.AddWithValue("@Data", DataAccoglimento)
                                cmdw.Connection = cn
                                cmdw.ExecuteNonQuery()
                            Else

                                Dim UltimaRetta As New Cls_rettatotale

                                UltimaRetta.CENTROSERVIZIO = DD_CentroServizio.SelectedValue
                                UltimaRetta.CODICEOSPITE = CODICEOSPITE
                                UltimaRetta.UltimaData(Session("DC_OSPITE"), UltimaRetta.CODICEOSPITE, UltimaRetta.CENTROSERVIZIO)


                                MySql = "INSERT INTO IMPORTORETTA (CentroServizio,CodiceOspite,Data,IMPORTO,TIPOIMPORTO,TIPORETTA,UTENTE,DATAAGGIORNAMENTO) VALUES " & _
                                                                " (?,?,?,?,?,?,?,?)  "

                                Dim cmdw As New OleDbCommand()
                                cmdw.CommandText = (MySql)
                                cmdw.Parameters.AddWithValue("@CentroServizio", DD_CentroServizio.SelectedValue)
                                cmdw.Parameters.AddWithValue("@CodiceOspite", CODICEOSPITE)
                                cmdw.Parameters.AddWithValue("@Data", DataAccoglimento)
                                cmdw.Parameters.AddWithValue("@IMPORTO", Importo)
                                cmdw.Parameters.AddWithValue("@TIPOIMPORTO", "G")
                                cmdw.Parameters.AddWithValue("@TIPORETTA", UltimaRetta.TipoRetta)
                                cmdw.Parameters.AddWithValue("@UTENTE", Session("UTENTE"))
                                cmdw.Parameters.AddWithValue("@DataAggiornamento", Now)

                                cmdw.Connection = cn
                                cmdw.ExecuteNonQuery()

                                Dim XR As New Cls_ExtraFisso

                                XR.CENTROSERVIZIO = DD_CentroServizio.SelectedValue
                                XR.CODICEOSPITE = CODICEOSPITE
                                XR.Data = UltimaRetta.Data
                                XR.DuplicaExtraAdata(Session("DC_OSPITE"), DataAccoglimento)

                                'XR.UltimaData(Session("DC_OSPITE"))
                                'If XR.CODICEEXTRA <> "" Then
                                '    XR.Data = DataAccoglimento
                                '    XR.AggiornaDB(Session("DC_OSPITE"))
                                'End If

                            End If

                        End If
                    End If
                End If
            End If
        Next
        cn.Close()

        Call Ricerca()
    End Sub

    Protected Sub Btn_Esci_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles Btn_Esci.Click
        Response.Redirect("Menu_Strumenti.aspx")
    End Sub

    Private Sub AggiornaCServ()
        Dim kCsrv As New Cls_CentroServizio

        If DD_Struttura.SelectedValue = "" Then
            kCsrv.UpDateDropBox(Session("DC_OSPITE"), DD_CentroServizio)
        Else
            kCsrv.UpDateDropBoxStruttura(Session("DC_OSPITE"), DD_CentroServizio, DD_Struttura.SelectedValue)
        End If
    End Sub



    Protected Sub DD_Struttura_TextChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles DD_Struttura.TextChanged
        AggiornaCServ()
        Call EseguiJS()
    End Sub

    Protected Sub Btn_Copia_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles Btn_Copia.Click
        Dim I As Integer

        For I = 0 To Grd_AggiornaRette.Rows.Count - 1
            Try
                Dim TxtImporto As TextBox = DirectCast(Grd_AggiornaRette.Rows(I).FindControl("Txt_Importo"), TextBox)
                If DD_IVA.SelectedValue = "" Then
                    If CDbl(Grd_AggiornaRette.Rows(I).Cells(3).Text) = CDbl(Txt_ImportoDa.Text) Then
                        TxtImporto.Text = Txt_ImportoA.Text
                    End If
                Else
                    If DD_IVA.SelectedItem.Text = Grd_AggiornaRette.Rows(I).Cells(6).Text And CDbl(Grd_AggiornaRette.Rows(I).Cells(3).Text) = CDbl(Txt_ImportoDa.Text) Then
                        TxtImporto.Text = Txt_ImportoA.Text
                    End If
                End If


            Catch ex As Exception

            End Try
        Next

        Txt_ImportoA.Text = "0"
        Txt_ImportoDa.Text = "0"
        Call EseguiJS()


    End Sub
End Class

