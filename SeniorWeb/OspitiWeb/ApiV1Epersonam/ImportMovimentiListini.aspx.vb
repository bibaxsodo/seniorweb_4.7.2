﻿Imports System
Imports System.Web
Imports Microsoft.VisualBasic
Imports System.Data.OleDb
Imports System.Web.Hosting
Imports System.Data.SqlTypes
Imports System.Net
Imports System.Collections.Generic
Imports System.IO
Imports System.Security.Cryptography.X509Certificates
Imports System.Net.Security
Imports System.Web.Script.Serialization
Imports org.jivesoftware.util
Imports Newtonsoft.Json
Imports Newtonsoft.Json.Linq
Imports System.Threading

Partial Class OspitiWeb_ApiV1Epersonam_ImportMovimentiListini
    Inherits System.Web.UI.Page
    Dim Tabella As New System.Data.DataTable("tabella")


    Function campodb(ByVal oggetto As Object) As String
        If IsDBNull(oggetto) Then
            Return ""
        Else
            Return oggetto
        End If
    End Function
    Private Function LoginPersonam(ByVal context As HttpContext) As String
        Try


            Dim request As New NameValueCollection


            request.Add("client_id", "98217ca6670c660e22f42d58e231d4e56c84fb34e216b0f66f1f30284fd3ec9d")
            request.Add("client_secret", "5570a684f69ca74d75d16bba669c156ec092eccb4111d0974adec3edb2fa4d6f")


            request.Add("username", context.Session("EPersonamUser"))



            request.Add("password", context.Session("EPersonamPSW"))
            System.Net.ServicePointManager.ServerCertificateValidationCallback = AddressOf CertificateHandler


            Dim client As New WebClient()

            Dim result As Object = client.UploadValues("https://api.e-personam.com/api/v1.0/token/password", "POST", request)


            Dim stringa As String = Encoding.Default.GetString(result)


            Dim serializer As JavaScriptSerializer


            serializer = New JavaScriptSerializer()




            Dim s As Autentificazione = serializer.Deserialize(Of Autentificazione)(stringa)


            Return s.access_token
        Catch ex As Exception
            Return ""
        End Try
    End Function

    Protected Sub Btn_Movimenti_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles Btn_Movimenti.Click
        Call StartImport()
    End Sub


    Private Sub StartImport()



        Dim cn As OleDbConnection
        Dim Trovato As Boolean = False
        Dim Token As String

        cn = New Data.OleDb.OleDbConnection(Session("DC_OSPITE"))

        cn.Open()


        Token = LoginPersonam(Context)

        BusinessUnit(Token, Context)
        cn.Close()
    End Sub

    Public Class Autentificazione
        Public access_token As String
        Public expires_in As String
        Public scope As String
        Public token_type As String
        Public refresh_token As String
    End Class

    Private Shared Function CertificateHandler(ByVal sender As Object, ByVal certificate As X509Certificate, ByVal chain As X509Chain, ByVal SSLerror As SslPolicyErrors) As Boolean
        Return True
    End Function



    Private Function BusinessUnit(ByVal Token As String, ByVal context As HttpContext) As Long
        Dim rawresp As String
        Dim Param As New Cls_Parametri

        Param.LeggiParametri(context.Session("DC_OSPITE"))


        Dim DataFatt As String

        If Param.MeseFatturazione > 9 Then
            DataFatt = Param.AnnoFatturazione & "/" & Param.MeseFatturazione
        Else
            DataFatt = Param.AnnoFatturazione & "/" & Param.MeseFatturazione
        End If
        Dim OraFatt As String = "T00:00:00+00:00"



        BusinessUnit = 0
        Try



            Tabella.Clear()
            Tabella.Columns.Clear()

            Tabella.Columns.Add("CodiceOspite", GetType(String))
            Tabella.Columns.Add("Cognome", GetType(String))
            Tabella.Columns.Add("Nome", GetType(String))
            Tabella.Columns.Add("CodiceFiscale", GetType(String))
            Tabella.Columns.Add("Data", GetType(String))
            Tabella.Columns.Add("ListinoSociale", GetType(String))
            Tabella.Columns.Add("ListinoSanitario", GetType(String))
            Tabella.Columns.Add("CentroServizio", GetType(String))
            Tabella.Columns.Add("CodiceCentroServizio", GetType(String))
            Tabella.Columns.Add("CodiceListinoSociale", GetType(String))
            Tabella.Columns.Add("CodiceListinoSanitario", GetType(String))
            Tabella.Columns.Add("Segnalazione", GetType(String))



            Dim client As HttpWebRequest = WebRequest.Create("https://api.e-personam.com/api/v1.0/business_units")


            client.Method = "GET"
            client.Headers.Add("Authorization", "Bearer " & Token)
            client.ContentType = "Content-Type: application/json"

            Dim reader As StreamReader
            Dim response As HttpWebResponse = Nothing
            Dim JSonString As String = ""
            Dim JSonOldString As String = ""
            Dim NumeroPagina As Integer = 0

            response = DirectCast(client.GetResponse(), HttpWebResponse)

            reader = New StreamReader(response.GetResponseStream())

            rawresp = reader.ReadToEnd()


            Dim jArr As JArray = JArray.Parse(rawresp)


            For Each jResults As JToken In jArr

                BusinessUnit = Val(jResults.Item("id").ToString)
                ImportaMovimenti(BusinessUnit, Token)
            
            Next
        Catch ex As Exception

        End Try

        ViewState("App_AddebitiMultiplo") = Tabella

        GridView1.AutoGenerateColumns = False

        GridView1.DataSource = Tabella
        GridView1.DataBind()
        GridView1.Visible = True



    End Function

    Private Function RichiestaGuests(ByVal Token As String, ByVal BussinesUnit As String, ByVal DataFatt As String, ByVal OraFATT As String, ByVal NumeroPagina As Integer, ByVal NonInAttesa As Integer) As String


        Try

            Dim UrlConnessione As String



            'curl -k -H "Authorization: Bearer $TOKEN" -H "Content-Type: application/json" https://api.e-personam.com/api/v1.0/business_units/1771/guests?from=2018-02-04T00:00:00+00:00

            'curl -k -H "Authorization: Bearer $TOKEN" -H "Content-Type: application/json" https://api.e-personam.com/api/v1.0/business_units/3302/guests/updated/2018-04-10T00:00:00+00:00

            If NumeroPagina = 0 Then
                UrlConnessione = "https://api.e-personam.com/api/v1.0/business_units/" & BussinesUnit & "/guests/updated/" & DataFatt & OraFATT '"T00:00:00+00:00" ' &to=2050-12-31T00:00:00+00:00"
            Else
                UrlConnessione = "https://api.e-personam.com/api/v1.0/business_units/" & BussinesUnit & "/guests/updated/" & DataFatt & OraFATT & "&page=" & NumeroPagina
            End If

            Dim client As HttpWebRequest = WebRequest.Create(UrlConnessione)

            client.Method = "GET"
            client.Headers.Add("Authorization", "Bearer " & Token)
            client.ContentType = "Content-Type: application/json"

            Dim reader As StreamReader
            Dim response As HttpWebResponse = Nothing

            response = DirectCast(client.GetResponse(), HttpWebResponse)

            reader = New StreamReader(response.GetResponseStream())


            Dim rawresp As String
            rawresp = reader.ReadToEnd()

            If rawresp.ToUpper.IndexOf("BNCCRL22P18F205O") > 0 Then
                rawresp = rawresp
            End If

            Return rawresp

        Catch ex As Exception
            Return "ERRORE :[" & ex.Message & " " & "https://api.e-personam.com/api/v1.0/business_units/" & BussinesUnit & "/guests/updated/" & DataFatt & OraFATT & "]"
        End Try

    End Function



    Private Sub ImportaMovimenti(ByVal BusinessID As Integer, ByVal Token As String)
        Dim request As New NameValueCollection
        Dim rawresp As String
        Dim cn As OleDbConnection

        Dim Param As New Cls_Parametri

        Param.LeggiParametri(Context.Session("DC_OSPITE"))


        Dim DataFatt As String

        If Param.MeseFatturazione > 9 Then
            DataFatt = Param.AnnoFatturazione & "/" & Param.MeseFatturazione
        Else
            DataFatt = Param.AnnoFatturazione & "/" & Param.MeseFatturazione
        End If

        Try

            System.Net.ServicePointManager.ServerCertificateValidationCallback = AddressOf CertificateHandler


            Dim client As HttpWebRequest = WebRequest.Create("https://api.e-personam.com/api/v1.0/business_units/" & BusinessID & "/movements/" & DataFatt & "?only=new_price")

            client.Method = "GET"
            client.Headers.Add("Authorization", "Bearer " & Token)
            client.ContentType = "Content-Type: application/json"


            Dim reader As StreamReader
            Dim response As HttpWebResponse = Nothing

            response = DirectCast(client.GetResponse(), HttpWebResponse)

            reader = New StreamReader(response.GetResponseStream())


            rawresp = reader.ReadToEnd()

            Dim jResults As JArray = JArray.Parse(rawresp)


            cn = New Data.OleDb.OleDbConnection(Session("DC_OSPITE"))

            cn.Open()
            If Param.DebugV1 = 1 Then
                Dim NomeFile As String

                Dim serializer As JavaScriptSerializer
                serializer = New JavaScriptSerializer()

                NomeFile = HostingEnvironment.ApplicationPhysicalPath() & "\Public\Json_" & Format(Now, "yyyyMMdd") & "OSP.xml"
                Dim tw As System.IO.TextWriter = System.IO.File.CreateText(NomeFile)
                tw.Write(rawresp)
                tw.Close()
            End If


            For Each jTok As JToken In jResults
                rawresp = rawresp & jTok.Item("guest_movements").ToString
                For Each jTok1 As JToken In jTok.Item("guest_movements").Children
                    Dim CodiceFiscale As String = ""
                    Dim Fullname As String = ""

                    Try
                        CodiceFiscale = jTok1.Item("cf").ToString()
                    Catch ex As Exception

                    End Try
                    Try
                        Fullname = jTok1.Item("fullname").ToString()
                    Catch ex As Exception

                    End Try



                    Dim Ospite As New ClsOspite




                    Ospite.CODICEFISCALE = CodiceFiscale
                    Ospite.LeggiPerCodiceFiscale(Session("DC_OSPITE"), CodiceFiscale)


                    If Ospite.CodiceOspite > 0 Then
                        For Each jTok2 As JToken In jTok1.Item("movements").Children
                            Dim ListinoSociale As Integer
                            Dim ListinoSanitario As Integer
                            Dim Data As Date
                            Dim ward_id As Integer
                            Dim conv As String = ""

                            Try
                                ListinoSociale = jTok2.Item("p_alb").ToString()
                            Catch ex As Exception

                            End Try

                            Try
                                ListinoSanitario = jTok2.Item("p_san").ToString()
                            Catch ex As Exception

                            End Try


                            Try
                                ward_id = jTok2.Item("unit").Item("id").ToString()
                            Catch ex As Exception

                            End Try




                            Try
                                conv = jTok2.Item("conv").ToString.ToUpper()
                            Catch ex As Exception

                            End Try



                            Try
                                Data = jTok2.Item("date").ToString()
                            Catch ex As Exception

                            End Try
                            Dim cause_id As String = ""
                            Try
                                cause_id = jTok2.Item("cause_id").ToString.ToUpper
                            Catch ex As Exception

                            End Try
                            If cause_id <> "NEW_PRICE" And Val(ListinoSociale) > 0 Then
                                Dim MyCs As New Cls_CentroServizio
                                Dim MyClassEpersonam As New Cls_Epersonam


                                If conv.ToUpper = "TRUE" Then
                                    MyCs.EPersonam = MyClassEpersonam.RendiWardid(ward_id, Session("DC_OSPITE"))
                                    MyCs.LeggiEpersonam(Session("DC_OSPITE"), ward_id, True)
                                Else
                                    MyCs.EPersonamN = MyClassEpersonam.RendiWardid(ward_id, Session("DC_OSPITE"))
                                    MyCs.LeggiEpersonam(Session("DC_OSPITE"), ward_id, False)
                                End If



                                Dim Ls As New Cls_Listino

                                Ls.CodiceListino = ""
                                Ls.CODICEOSPITE = Ospite.CodiceOspite
                                Ls.CENTROSERVIZIO = MyCs.CENTROSERVIZIO
                                Ls.DATA = Nothing
                                Ls.LeggiAData(Session("DC_OSPITE"), Format(Data, "dd/MM/yyyy"))

                                Dim Diverso As Boolean = False

                                If Val(ListinoSociale) <> Val(Ls.CodiceListino) Then
                                    Diverso = True
                                End If

                                If Val(ListinoSanitario) <> Val(Ls.CodiceListinoSanitario) And Val(ListinoSanitario) <> Val(Ls.CodiceListinoPrivatoSanitario) Then
                                    Diverso = True
                                End If

                                If Diverso = True Then
                                    Dim x As Integer
                                    Dim Trovato As Boolean = False
                                    For x = 0 To Tabella.Rows.Count - 1
                                        If Tabella.Rows(x).Item(0) = Ospite.CodiceOspite And Tabella.Rows(x).Item(4) = Format(Data, "dd/MM/yyyy") Then
                                            Trovato = True
                                        End If
                                    Next
                                    If Trovato = True Then
                                        Diverso = False
                                    End If
                                End If
                                If Diverso = True Then
                                    Dim myriga As System.Data.DataRow = Tabella.NewRow()

                                    myriga.Item(0) = Ospite.CodiceOspite

                                    myriga.Item(1) = Ospite.CognomeOspite
                                    myriga.Item(2) = Ospite.NomeOspite
                                    myriga.Item(3) = Ospite.CODICEFISCALE

                                    myriga.Item(4) = Format(Data, "dd/MM/yyyy")



                                    Dim Listino As New Cls_Tabella_Listino

                                    Listino.Descrizione = ""
                                    Listino.Codice = ListinoSociale
                                    Listino.LeggiCausale(Session("DC_OSPITE"))


                                    myriga.Item(5) = Listino.Descrizione


                                    Listino.Descrizione = ""
                                    Listino.Codice = ListinoSanitario
                                    Listino.LeggiCausale(Session("DC_OSPITE"))

                                    myriga.Item(6) = Listino.Descrizione



                                    myriga.Item(7) = MyCs.DESCRIZIONE
                                    myriga.Item(8) = MyCs.CENTROSERVIZIO

                                    myriga.Item(9) = ListinoSociale
                                    myriga.Item(10) = ListinoSanitario

                                    myriga.Item(11) = "DIVERSO"

                                    Tabella.Rows.Add(myriga)

                                End If

                            End If

                            If cause_id = "NEW_PRICE" Then
                                Dim x As Integer
                                Dim Trovato As Boolean = False
                                For x = 0 To Tabella.Rows.Count - 1
                                    If Tabella.Rows(x).Item(0) = Ospite.CodiceOspite And Tabella.Rows(x).Item(4) = Format(Data, "dd/MM/yyyy") Then
                                        Trovato = True
                                    End If
                                Next

                                If Not Trovato Then

                                    Dim MyCs As New Cls_CentroServizio
                                    Dim MyClassEpersonam As New Cls_Epersonam


                                    If conv.ToUpper = "TRUE" Then
                                        MyCs.EPersonam = MyClassEpersonam.RendiWardid(ward_id, Session("DC_OSPITE"))
                                        MyCs.LeggiEpersonam(Session("DC_OSPITE"), ward_id, True)
                                    Else
                                        MyCs.EPersonamN = MyClassEpersonam.RendiWardid(ward_id, Session("DC_OSPITE"))
                                        MyCs.LeggiEpersonam(Session("DC_OSPITE"), ward_id, False)
                                    End If


                                    Dim myriga As System.Data.DataRow = Tabella.NewRow()

                                    myriga.Item(0) = Ospite.CodiceOspite

                                    myriga.Item(1) = Ospite.CognomeOspite
                                    myriga.Item(2) = Ospite.NomeOspite
                                    myriga.Item(3) = Ospite.CODICEFISCALE

                                    myriga.Item(4) = Format(Data, "dd/MM/yyyy")



                                    Dim Listino As New Cls_Tabella_Listino

                                    Listino.Descrizione = ""
                                    Listino.Codice = ListinoSociale
                                    Listino.LeggiCausale(Session("DC_OSPITE"))

                                    Dim Ls As New Cls_Listino

                                    Ls.CodiceListino = ""
                                    Ls.CODICEOSPITE = Ospite.CodiceOspite
                                    Ls.CENTROSERVIZIO = MyCs.CENTROSERVIZIO
                                    Ls.DATA = Nothing
                                    Ls.LeggiAData(Session("DC_OSPITE"), Format(Data, "dd/MM/yyyy"))
                                    If Ls.DATA = Format(Data, "dd/MM/yyyy") Then
                                        myriga.Item(11) = "TROVATO"
                                    End If

                                    myriga.Item(5) = Listino.Descrizione


                                    Listino.Descrizione = ""
                                    Listino.Codice = ListinoSanitario
                                    Listino.LeggiCausale(Session("DC_OSPITE"))

                                    myriga.Item(6) = Listino.Descrizione



                                    myriga.Item(7) = MyCs.DESCRIZIONE
                                    myriga.Item(8) = MyCs.CENTROSERVIZIO

                                    myriga.Item(9) = ListinoSociale
                                    myriga.Item(10) = ListinoSanitario



                                    Tabella.Rows.Add(myriga)
                                End If
                            End If
                        Next
                    End If
                Next

            Next

            cn.Close()

        Catch ex As Exception

        End Try

    End Sub

    Protected Sub GridView1_RowDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs) Handles GridView1.RowDataBound
        If e.Row.RowType = DataControlRowType.DataRow Then
            Dim CheckBox As CheckBox = DirectCast(e.Row.FindControl("ChkImporta"), CheckBox)
            If Tabella.Rows(e.Row.RowIndex).Item(11).ToString = "TROVATO" Then
                CheckBox.Enabled = False

                e.Row.BackColor = Drawing.Color.Aqua

            End If
            If Tabella.Rows(e.Row.RowIndex).Item(11).ToString = "DIVERSO" Then
                CheckBox.Enabled = False

                e.Row.BackColor = Drawing.Color.Red

            End If
        End If

    End Sub

    Protected Sub Btn_Modifica_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles Btn_Modifica.Click
        Dim Indice As Integer
        Tabella = ViewState("App_AddebitiMultiplo")

        For Indice = 0 To GridView1.Rows.Count - 1
            Dim CheckBox As CheckBox = DirectCast(GridView1.Rows(Indice).FindControl("ChkImporta"), CheckBox)

            If CheckBox.Checked = True Then
                Dim Listino As New Cls_Listino

                Listino.CODICEOSPITE = Tabella.Rows(Indice).Item(0)
                Listino.CENTROSERVIZIO = Tabella.Rows(Indice).Item(8)
                Listino.DATA = Tabella.Rows(Indice).Item(4)
                Listino.CodiceListino = Val(Tabella.Rows(Indice).Item(9))

                Dim M As New Cls_Tabella_Listino

                M.Codice = Val(Tabella.Rows(Indice).Item(10))
                M.LeggiCausale(Session("DC_OSPITE"))
                If M.Descrizione <> "" Then
                    If M.TIPOLISTINO = "R" Then
                        Listino.CodiceListinoSanitario = Val(Tabella.Rows(Indice).Item(10))
                        Listino.CodiceListinoPrivatoSanitario = ""
                    Else
                        Listino.CodiceListinoSanitario = ""
                        Listino.CodiceListinoPrivatoSanitario = Val(Tabella.Rows(Indice).Item(10))
                    End If
                End If
                Listino.InserisciRette(Session("DC_OSPITE"), Listino.CENTROSERVIZIO, Listino.CODICEOSPITE, Listino.CodiceListino, Listino.DATA, Listino.CodiceListinoPrivatoSanitario, Listino.CodiceListinoSanitario, "", "")
            End If

        Next

        StartImport()
    End Sub


    Function campodbN(ByVal oggetto As Object) As Double
        If IsDBNull(oggetto) Then
            Return 0
        Else
            Return oggetto
        End If
    End Function

    Function campodbd(ByVal oggetto As Object) As Date
        If IsDBNull(oggetto) Then
            Return Nothing
        Else
            Return oggetto
        End If
    End Function

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If IsNothing(Session("DC_OSPITE")) Then
            Response.Redirect("..\Login.aspx")
            Exit Sub
        End If


        If Page.IsPostBack = False Then


            Dim K1 As New Cls_SqlString

            Dim Barra As New Cls_BarraSenior

            If Not IsNothing(Session("RicercaAnagraficaSQLString")) Then
                Try
                    K1 = Session("RicercaAnagraficaSQLString")
                Catch ex As Exception
                    K1 = Nothing
                End Try
            End If

            Lbl_BarraSenior.Text = Barra.CodiceBarra(Application("SENIOR"), Session("UTENTE"), Page, K1)

            Dim Param As New Cls_Parametri

            Param.LeggiParametri(Context.Session("DC_OSPITE"))



            Dim DataFatt As String

            If Param.MeseFatturazione > 9 Then
                DataFatt = Param.AnnoFatturazione & "-" & Param.MeseFatturazione & "-01"
            Else
                DataFatt = Param.AnnoFatturazione & "-0" & Param.MeseFatturazione & "-01"
            End If



        End If
    End Sub

    Protected Sub Btn_Esci_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles Btn_Esci.Click
        Response.Redirect("../Menu_Epersonam.aspx")
    End Sub

    Protected Sub Btn_Seleziona_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles Btn_Seleziona.Click
        Dim Param As New Cls_Parametri

        Param.LeggiParametri(Session("DC_OSPITE"))

        For Riga = 0 To GridView1.Rows.Count - 1

            Dim CheckBox As CheckBox = DirectCast(GridView1.Rows(Riga).FindControl("ChkImporta"), CheckBox)

            If CheckBox.Enabled = True And GridView1.Rows(Riga).BackColor <> System.Drawing.Color.Orange Then
                CheckBox.Checked = True
                If GridView1.Rows(Riga).Cells(5).Text.IndexOf(Param.MeseFatturazione & "/" & Param.AnnoFatturazione) > 0 Then
                    CheckBox.Checked = True
                End If
            End If
        Next
    End Sub
End Class
