﻿Imports System
Imports System.Web
Imports Microsoft.VisualBasic
Imports System.Data.OleDb
Imports System.Web.Hosting
Imports System.Data.SqlTypes
Imports System.Net
Imports System.Collections.Generic
Imports System.IO
Imports System.Security.Cryptography.X509Certificates
Imports System.Net.Security
Imports System.Web.Script.Serialization
Imports org.jivesoftware.util
Imports Newtonsoft.Json
Imports Newtonsoft.Json.Linq
Imports System.Threading



Partial Class OspitiWeb_ApiV1Epersonam_allineaospitiparentiv1
    Inherits System.Web.UI.Page
    Private Delegate Sub DoWorkDelegate(ByRef data As Object)

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load

        If Page.IsPostBack = True Then Exit Sub

        Session("INTERROMPI") = ""

        If Request.Item("UTENTE") <> "" And Request.Item("PASSWORD") <> "" Then
            Dim k As New Cls_Login

            k.Utente = Request.Item("UTENTE")
            k.Chiave = Request.Item("PASSWORD")
            k.Leggi(Application("SENIOR"))
            Session("Password") = k.Chiave
            If k.Ospiti <> "" Then
                Session("UTENTE") = k.Utente
            End If

            k.Utente = UCase(k.Utente)
            k.Chiave = k.Chiave

            k.Ip = Context.Request.ServerVariables("REMOTE_ADDR")
            k.Sistema = Request.UserAgent & " " & Request.Browser.Browser & " " & Request.Browser.MajorVersion & " " & Request.Browser.MinorVersion
            k.Leggi(Application("SENIOR"))
            Session("Password") = k.Chiave
            Session("ChiaveCr") = k.ChiaveCr

            Session("DC_OSPITE") = k.Ospiti
            Session("DC_OSPITIACCESSORI") = k.OspitiAccessori
            Session("DC_TABELLE") = k.TABELLE
            Session("DC_GENERALE") = k.Generale
            Session("DC_TURNI") = k.Turni
            Session("STAMPEOSPITI") = k.STAMPEOSPITI
            Session("STAMPEFINANZIARIA") = k.STAMPEFINANZIARIA
            Session("ProgressBar") = ""
            Session("CODICEREGISTRO") = ""
            Session("NomeEPersonam") = k.NomeEPersonam
            Session("ABILITAZIONI") = k.ABILITAZIONI
            Session("UTENTE") = k.Utente.ToLower

        End If


        If Trim(Session("UTENTE")) = "" Then
            Response.Redirect("..\..\Login.aspx")
            Exit Sub
        End If

        If Page.IsPostBack = True Then
            Exit Sub
        End If
        Session("INIZIO") = Now
        Session("CampoProgressBar") = 0
        System.Web.HttpRuntime.Cache("CampoProgressBar" + Session.SessionID) = 0


        Dim VSession As New VariabiliSesssione


        VSession.DC_OSPITE = Session("DC_OSPITE")
        VSession.DC_OSPITIACCESSORI = Session("DC_OSPITIACCESSORI")
        VSession.DC_TABELLE = Session("DC_TABELLE")
        VSession.DC_GENERALE = Session("DC_GENERALE")
        VSession.UTENTE = Session("EPersonamUser")
        VSession.PASSWORD = Session("EPersonamPSW")
        VSession.RICERCAOSPITI = Request.Item("RICERCA")



        If Request.Item("RICERCA") = "SI" Then
            Lbl_Waiting.Text = "<div id=""blur"">&nbsp;</div><div id=""pippo""  class=""wait"">"
            Lbl_Waiting.Text = Lbl_Waiting.Text & "Dialogo In Corso... <br /><br /><br />"
            Lbl_Waiting.Text = Lbl_Waiting.Text & "<img  height=30px src=""../images/loading.gif""><br />"
            Lbl_Waiting.Text = Lbl_Waiting.Text & "</div>"
            lnk_Continua.Text = "<label style=""position: absolute;z-index: 999; top :0px; left:0px; height:20px; width:100%; background-color: #007DC4; text-align: center;color: white;"">Click per continuare senza allineare Anagrafiche</label>"

            Dim t As New Thread(New ParameterizedThreadStart(AddressOf DoWork))

            t.Start(VSession)

            Timer1.Interval = 1000
        Else

            If Request.Item("UTENTE") <> "" And Request.Item("PASSWORD") <> "" Then
                DoWork(VSession)
            Else

                Dim t As New Thread(New ParameterizedThreadStart(AddressOf DoWork))

                t.Start(VSession)

                Timer1.Interval = 1000
            End If


        End If


    End Sub

    Public Class VariabiliSesssione
        Public DC_OSPITE As String
        Public DC_OSPITIACCESSORI As String
        Public DC_TABELLE As String
        Public DC_GENERALE As String
        Public UTENTE As String
        Public PASSWORD As String
        Public RICERCAOSPITI As String
    End Class

    Public Sub DoWork(ByVal data As VariabiliSesssione)

        AllineaDB(data)
    End Sub

    Private Sub AllineamentoBU(ByVal data As VariabiliSesssione, ByVal Token As String, ByVal DataFatt As String, ByVal OraFatt As String, ByVal BU As Integer)
        Dim Param As New Cls_Parametri
        Param.LeggiParametri(data.DC_OSPITE)
        Dim EPersonam As New Cls_Epersonam

        Dim NumeroOspiti As Integer = 0
        Dim NumeroPagina As Integer = 0
        Dim JSonString As String
        Dim JSonOldString As String = ""

        Do

            JSonString = RichiestaGuests(Token, BU, DataFatt, OraFatt, NumeroPagina, Param.NonInAtteasa)
            If JSonOldString = JSonString Then
                Exit Do
            End If


            If Len(JSonOldString) > 5000 And Len(JSonString) > 500 Then
                If Mid(JSonOldString, 1, 5000) = Mid(JSonString, 1, 5000) Then
                    Exit Do
                End If
            End If
            JSonOldString = JSonString


            If Param.DebugV1 = 1 Then
                Dim NomeFile As String

                NomeFile = HostingEnvironment.ApplicationPhysicalPath() & "\Public\Json_" & Format(Now, "yyyyMMdd") & "_" & BU & "_" & NumeroPagina & ".xml"
                Dim tw As System.IO.TextWriter = System.IO.File.CreateText(NomeFile)
                tw.Write(JSonString)
                tw.Close()
            End If

            If JSonString.IndexOf("ERRORE :") >= 0 Then
                JSonString = "[]"
            End If

            NumeroPagina = NumeroPagina + 1

            Dim jResults As JArray = JArray.Parse(JSonString)


            For Each jTok As JToken In jResults
                Dim Ospite As New ClsOspite
                Dim IdEpersonam As Integer
                Dim appoggio As String
                Dim Inserimento As Boolean = False
                Dim AbilitaInserimentoModifica As Boolean = True



                If Not IsNothing(Session("INTERROMPI")) Then
                    If Session("INTERROMPI") = "SI" Then
                        Exit Do
                    End If
                End If
                Ospite.CodiceOspite = 0
                Ospite.CODICEFISCALE = ""

                Try
                    Ospite.CODICEFISCALE = jTok.Item("cf").ToString()
                Catch ex As Exception

                End Try


                If Ospite.CODICEFISCALE = "CRSNLN34A63F205I" Then
                    Ospite.CODICEFISCALE = "CRSNLN34A63F205I"
                End If


                If Ospite.CODICEFISCALE <> "" Then
                    Try
                        IdEpersonam = jTok.Item("id").ToString()
                    Catch ex As Exception

                    End Try

                    NumeroOspiti = NumeroOspiti + 1
                    Ospite.LeggiPerCodiceFiscale(data.DC_OSPITE, Ospite.CODICEFISCALE)


                    If Ospite.CodiceOspite = 0 Then
                        Ospite.IdEpersonam = IdEpersonam
                        Ospite.LeggiPerIdEpersonam(data.DC_OSPITE)
                    End If

                    Try
                        Ospite.CognomeOspite = jTok.Item("surname").ToString()
                    Catch ex As Exception

                    End Try

                    Try
                        Ospite.NomeOspite = jTok.Item("firstname").ToString()
                    Catch ex As Exception

                    End Try

                    Try
                        Ospite.DataNascita = campodbd(jTok.Item("birthdate").ToString())
                    Catch ex As Exception

                    End Try

                    If Ospite.CodiceOspite = 0 Then
                        If Param.SoloModAnagraficaDaEpersonam = 0 Then
                            Inserimento = True

                            Try
                                AbilitaInserimentoModifica = MovimentiInserimentoVerifica(data, jTok.Item("movements").ToString(), Ospite.CodiceOspite, Param, True)
                            Catch ex As Exception
                                If Param.DebugV1 = 1 Then
                                    Dim NomeFile As String

                                    NomeFile = HostingEnvironment.ApplicationPhysicalPath() & "\Public\Errori_" & Format(Now, "yyyyMMdd") & "_" & NumeroPagina & ".txt"
                                    Dim tw As System.IO.TextWriter = System.IO.File.CreateText(NomeFile)
                                    tw.Write("Codosp :" & Ospite.CodiceOspite & " - " & Ospite.Nome & " " & Ospite.CODICEFISCALE)
                                    tw.Close()
                                End If

                                AbilitaInserimentoModifica = False
                                Inserimento = False
                            End Try

                            If AbilitaInserimentoModifica = True Then
                                Ospite.InserisciOspiteSeCFNONPresente(data.DC_OSPITE, Ospite.CognomeOspite, Ospite.NomeOspite, Ospite.DataNascita, Ospite.CODICEFISCALE)
                            End If
                        Else
                            Inserimento = True
                            AbilitaInserimentoModifica = False
                        End If
                    End If

                    Try
                        Ospite.DataNascita = campodbd(jTok.Item("birthdate").ToString())
                    Catch ex As Exception

                    End Try
                    Try
                        Ospite.CognomeOspite = jTok.Item("surname").ToString()
                    Catch ex As Exception

                    End Try

                    Try
                        Ospite.NomeOspite = jTok.Item("firstname").ToString()
                    Catch ex As Exception

                    End Try

                    Ospite.Nome = Ospite.CognomeOspite & " " & Ospite.NomeOspite


                    Try
                        Ospite.CODICEFISCALE = jTok.Item("cf").ToString()
                    Catch ex As Exception

                    End Try

                    Try
                        Ospite.Sesso = jTok.Item("gender").ToString()
                    Catch ex As Exception

                    End Try

                    Try
                        Ospite.NOMECONIUGE = jTok.Item("spouse").ToString() 'spouse
                    Catch ex As Exception

                    End Try

                    Try
                        appoggio = jTok.Item("birthplace").Item("city_alpha6").ToString()

                        Ospite.ProvinciaDiNascita = Mid(appoggio & Space(6), 1, 3)
                        Ospite.ComuneDiNascita = Mid(appoggio & Space(6), 4, 3)
                    Catch ex As Exception

                    End Try

                    Try
                        Ospite.RESIDENZATELEFONO1 = jTok.Item("phone").ToString()
                    Catch ex As Exception

                    End Try

                    Try
                        Ospite.RESIDENZATELEFONO2 = jTok.Item("mobile").ToString()
                    Catch ex As Exception

                    End Try

                    Try
                        Ospite.RESIDENZATELEFONO3 = jTok.Item("email").ToString()
                    Catch ex As Exception

                    End Try

                    Try
                        Ospite.TELEFONO1 = jTok.Item("fax").ToString()
                    Catch ex As Exception

                    End Try


                    Try
                        Ospite.NomeMedico = jTok.Item("references").Item("primary_care_physician").Item("fullname").ToString()
                    Catch ex As Exception

                    End Try


                    Try
                        'aggiungere campo per telefono v.jole
                        Ospite.AssistenteSociale = jTok.Item("references").Item("almonar").Item("fullname").ToString()
                        If Len(Ospite.AssistenteSociale) > 49 Then
                            Ospite.AssistenteSociale = Mid(Ospite.AssistenteSociale, 1, 49)
                        End If
                    Catch ex As Exception

                    End Try


                    Try
                        Ospite.RESIDENZAINDIRIZZO1 = jTok.Item("residence").Item("street").ToString()
                    Catch ex As Exception

                    End Try


                    Try
                        Ospite.RESIDENZACAP1 = jTok.Item("residence").Item("cap").ToString()
                    Catch ex As Exception

                    End Try

                    Try
                        appoggio = jTok.Item("residence").Item("city_alpha6").ToString()

                        Ospite.RESIDENZAPROVINCIA1 = Mid(appoggio & Space(6), 1, 3)
                        Ospite.RESIDENZACOMUNE1 = Mid(appoggio & Space(6), 4, 3)

                    Catch ex As Exception

                    End Try

                    'marital_status
                    Try
                        Ospite.StatoCivile = Epersonam.DecodificaStatoCivile(jTok.Item("marital_status").Item("cod").ToString(), jTok.Item("marital_status").Item("description").ToString(), data.DC_OSPITE)
                    Catch ex As Exception

                    End Try


                    'Try
                    '    Ospite.AssistenteSociale = jTok.Item("ass_soc_fullname").ToString()
                    'Catch ex As Exception

                    'End Try
                    'spouse

                    Try
                        Ospite.NOMECONIUGE = jTok.Item("spouse").ToString()
                        If Len(Ospite.NOMECONIUGE) > 49 Then
                            Ospite.NOMECONIUGE = Mid(Ospite.NOMECONIUGE, 1, 49)
                        End If
                    Catch ex As Exception

                    End Try

                    Try
                        Ospite.TesseraSanitaria = jTok.Item("ehic_card").Item("card_number").ToString()
                    Catch ex As Exception

                    End Try


                    Try
                        Ospite.TesseraSanitaria = jTok.Item("ehic_card").Item("card_number").ToString()
                    Catch ex As Exception

                    End Try

                    Try
                        Ospite.TesseraSanitaria = jTok.Item("ehic_card").Item("card_number").ToString()
                    Catch ex As Exception

                    End Try


                    Try
                        Ospite.CodiceSanitario = jTok.Item("ehic_card").Item("codsan").ToString()
                    Catch ex As Exception

                    End Try


                    If Ospite.CodiceSanitario = "" Then
                        Try
                            Ospite.CodiceSanitario = jTok.Item("codsan").ToString()
                        Catch ex As Exception

                        End Try
                    End If

                    Try
                        Ospite.EsenzioneTicket = jTok.Item("ticket_exemption").ToString()
                    Catch ex As Exception

                    End Try


                    Try
                        Ospite.IdEpersonam = jTok.Item("id").ToString()
                    Catch ex As Exception

                    End Try

                    ' va inserito un controllo per non fare l'update a tutti (e/o in tutti i casi)

                    If Param.CartellaEpersonam = 1 Then
                        Ospite.CartellaClinica = Cartella(Token, BU, Ospite.CODICEFISCALE)
                    End If

                    Ospite.UTENTE = data.UTENTE
                    Ospite.TIPOLOGIA = "O"


                    If AbilitaInserimentoModifica = True Then
                        If Inserimento = True Then
                            If Param.DebugV1 = 1 Then
                                Dim NomeFile As String

                                Dim serializer As JavaScriptSerializer
                                serializer = New JavaScriptSerializer()

                                NomeFile = HostingEnvironment.ApplicationPhysicalPath() & "\Public\Json_" & Format(Now, "yyyyMMdd") & "OSP.xml"
                                Dim tw As System.IO.TextWriter = System.IO.File.CreateText(NomeFile)
                                tw.Write(serializer.Serialize(Ospite))
                                tw.Close()
                            End If


                            Ospite.ScriviOspite(data.DC_OSPITE)
                            Try
                                MovimentiInserimentoVerifica(data, jTok.Item("movements").ToString(), Ospite.CodiceOspite, Param, False)


                                If Param.ImportListino = 1 Then

                                    Dim LastAcco As New Cls_Movimenti

                                    LastAcco.CodiceOspite = Ospite.CodiceOspite
                                    LastAcco.UltimaDataAccoglimentoSenzaCserv(data.DC_OSPITE)

                                    If IsDate(LastAcco.Data) Then
                                        If Year(LastAcco.Data) > 1990 Then
                                            Dim p_alb As String = ""
                                            Dim p_ass As String = ""
                                            Dim p_san As String = ""
                                            Try
                                                p_alb = jTok.Item("current_status")("p_alb").ToString()
                                            Catch ex As Exception

                                            End Try

                                            Try
                                                p_ass = jTok.Item("current_status")("p_ass").ToString()
                                            Catch ex As Exception

                                            End Try

                                            Try
                                                p_san = jTok.Item("current_status")("p_san").ToString()
                                            Catch ex As Exception

                                            End Try

                                            If Val(p_alb) > 0 Then
                                                Dim Listino As New Cls_Listino

                                                Listino.CODICEOSPITE = Ospite.CodiceOspite
                                                Listino.CENTROSERVIZIO = LastAcco.CENTROSERVIZIO
                                                Listino.DATA = DateSerial(Year(LastAcco.Data), Month(LastAcco.Data), Day(LastAcco.Data))
                                                Listino.CodiceListino = Val(p_alb)

                                                Dim M As New Cls_Tabella_Listino

                                                M.Codice = Val(p_san)
                                                M.LeggiCausale(data.DC_OSPITE)
                                                If M.Descrizione <> "" Then
                                                    If M.TIPOLISTINO = "R" Then
                                                        Listino.CodiceListinoSanitario = Val(p_san)
                                                        Listino.CodiceListinoPrivatoSanitario = ""
                                                    Else
                                                        Listino.CodiceListinoSanitario = ""
                                                        Listino.CodiceListinoPrivatoSanitario = Val(p_san)
                                                    End If
                                                End If
                                                Listino.InserisciRette(data.DC_OSPITE, Listino.CENTROSERVIZIO, Listino.CODICEOSPITE, Listino.CodiceListino, DateSerial(Year(LastAcco.Data), Month(LastAcco.Data), Day(LastAcco.Data)), Listino.CodiceListinoPrivatoSanitario, Listino.CodiceListinoSanitario, "", "")
                                            End If
                                        End If
                                    End If
                                End If
                                Parenti(data, jTok.Item("relatives").ToString(), Ospite.CodiceOspite)
                            Catch ex As Exception

                            End Try
                        Else
                            If Param.DebugV1 = 1 Then
                                Dim NomeFile As String

                                Dim serializer As JavaScriptSerializer
                                serializer = New JavaScriptSerializer()

                                NomeFile = HostingEnvironment.ApplicationPhysicalPath() & "\Public\Json_" & Format(Now, "yyyyMMdd") & "_" & Ospite.CodiceOspite & "_OSP.xml"
                                Dim tw As System.IO.TextWriter = System.IO.File.CreateText(NomeFile)
                                tw.Write(serializer.Serialize(Ospite))
                                tw.Close()
                            End If


                            Ospite.ScriviOspite(data.DC_OSPITE)


                            If Param.ImportListino = 1 Then

                                Dim LastAcco As New Cls_Movimenti

                                LastAcco.CodiceOspite = Ospite.CodiceOspite
                                LastAcco.UltimaDataAccoglimentoSenzaCserv(data.DC_OSPITE)

                                Dim Ls As New Cls_Listino

                                Ls.CodiceListino = ""
                                Ls.CODICEOSPITE = Ospite.CodiceOspite
                                Ls.CENTROSERVIZIO = LastAcco.CENTROSERVIZIO
                                Ls.LeggiAData(data.DC_OSPITE, Now)


                                If IsDate(LastAcco.Data) Then
                                    If Year(LastAcco.Data) > 1990 Then
                                        Dim p_alb As String = ""
                                        Dim p_ass As String = ""
                                        Dim p_san As String = ""
                                        Try
                                            p_alb = jTok.Item("current_status")("p_alb").ToString()
                                        Catch ex As Exception

                                        End Try

                                        Try
                                            p_ass = jTok.Item("current_status")("p_ass").ToString()
                                        Catch ex As Exception

                                        End Try

                                        Try
                                            p_san = jTok.Item("current_status")("p_san").ToString()
                                        Catch ex As Exception

                                        End Try

                                        If Val(p_alb) > 0 And Ls.CodiceListino = "" Then
                                            Dim Listino As New Cls_Listino

                                            Listino.CODICEOSPITE = Ospite.CodiceOspite
                                            Listino.CENTROSERVIZIO = LastAcco.CENTROSERVIZIO
                                            Listino.DATA = DateSerial(Year(LastAcco.Data), Month(LastAcco.Data), Day(LastAcco.Data))
                                            Listino.CodiceListino = Val(p_alb)

                                            Dim M As New Cls_Tabella_Listino

                                            M.Codice = Val(p_san)
                                            M.LeggiCausale(data.DC_OSPITE)
                                            If M.Descrizione <> "" Then
                                                If M.TIPOLISTINO = "R" Then
                                                    Listino.CodiceListinoSanitario = Val(p_san)
                                                    Listino.CodiceListinoPrivatoSanitario = ""
                                                Else
                                                    Listino.CodiceListinoSanitario = ""
                                                    Listino.CodiceListinoPrivatoSanitario = Val(p_san)
                                                End If
                                            End If
                                            Listino.InserisciRette(data.DC_OSPITE, Listino.CENTROSERVIZIO, Listino.CODICEOSPITE, Listino.CodiceListino, DateSerial(Year(LastAcco.Data), Month(LastAcco.Data), Day(LastAcco.Data)), Listino.CodiceListinoPrivatoSanitario, Listino.CodiceListinoSanitario, "", "")
                                        End If
                                    End If
                                End If
                            End If

                            Parenti(data, jTok.Item("relatives").ToString(), Ospite.CodiceOspite)
                        End If
                    End If
                End If
            Next

            If IsNothing(JSonString) Then JSonString = ""

        Loop While Len(JSonString) > 10

    End Sub

    Private Sub AllineaDB(ByVal data As VariabiliSesssione)
        Dim Token As String = ""


        
        Try
            Token = LoginPersonam(data)
        Catch ex As Exception

        End Try

        If Token = "" Then Exit Sub



        Dim Param As New Cls_Parametri
        Dim OraFatt As String = "T00:00:00+00:00"
        Param.LeggiParametri(data.DC_OSPITE)

        Dim DataFatt As String

        If Param.MeseFatturazione > 9 Then
            DataFatt = Param.AnnoFatturazione & "-" & Param.MeseFatturazione & "-01"
        Else
            DataFatt = Param.AnnoFatturazione & "-0" & Param.MeseFatturazione & "-01"
        End If
        If data.RICERCAOSPITI = "SI" And IsDate(Param.DataCheckEpersonam) Then
            If Year(Param.DataCheckEpersonam) > 2000 Then
                Dim Giorno As String = ""
                If Param.DataCheckEpersonam.Day > 9 Then
                    Giorno = Param.DataCheckEpersonam.Day
                Else
                    Giorno = "0" & Param.DataCheckEpersonam.Day
                End If

                If Param.MeseFatturazione > 9 Then
                    DataFatt = Param.DataCheckEpersonam.Year & "-" & Param.DataCheckEpersonam.Month & "-" & Giorno
                Else
                    DataFatt = Param.DataCheckEpersonam.Year & "-0" & Param.DataCheckEpersonam.Month & "-" & Giorno
                End If
                If Hour(Param.DataCheckEpersonam) < 10 Then
                    OraFatt = "T0" & Hour(Param.DataCheckEpersonam) & ":00:00+00:00"
                Else
                    OraFatt = "T" & Hour(Param.DataCheckEpersonam) & ":00:00+00:00"
                End If
            End If
        End If


        System.Net.ServicePointManager.ServerCertificateValidationCallback = AddressOf CertificateHandler

        Dim BU As Integer
        BU = BussineUnit(Token, data, DataFatt, OraFatt)
        
        System.Web.HttpRuntime.Cache("CampoProgressBar" + Session.SessionID) = 101


        Dim ConnectionString As String = data.DC_OSPITE
        Dim cn As OleDbConnection

        cn = New Data.OleDb.OleDbConnection(ConnectionString)

        cn.Open()

        Dim CmdI18 As New OleDbCommand
        CmdI18.CommandText = "UPDATE TabellaParametri SET  DataCheckEpersonam = ?"
        CmdI18.Connection = cn
        CmdI18.Parameters.AddWithValue("@DataCheckEpersonam", Now)
        CmdI18.ExecuteNonQuery()


        cn.Close()
    End Sub

    Private Function MovimentiInserimentoVerifica(ByVal data As VariabiliSesssione, ByVal JSonMovimenti As String, ByVal CodiceOspite As Integer, ByVal Param As Cls_Parametri, ByVal Verifica As Boolean) As Boolean
        Dim jResults As JArray = JArray.Parse(JSonMovimenti)

        Dim SaveUnityID As Integer = 0
        Dim SaveTypeMov As String = ""
        Dim SaveConvenz As Boolean = False
        Dim SaveDataMovimento As Date
        Dim SaveIdEpersonam As Integer


        For Each jTok As JToken In jResults

            Dim UnityID As Integer = 0
            Dim TypeMov As String = ""
            Dim Convenz As Boolean = False
            Dim DataMovimento As Date
            Dim IdEpersonam As Integer


            Try
                UnityID = Val(jTok.Item("unit").Item("id").ToString())
            Catch ex As Exception

            End Try


            Try
                IdEpersonam = Val(jTok.Item("id").ToString())
            Catch ex As Exception

            End Try


            Try
                DataMovimento = Mid(jTok.Item("date").ToString() & Space(10), 1, 10)
            Catch ex As Exception

            End Try

            Try
                TypeMov = jTok.Item("type").ToString()
            Catch ex As Exception

            End Try

            Try
                If jTok.Item("conv").ToString.ToUpper = "TRUE" Then
                    Convenz = True
                Else
                    Convenz = False
                End If
            Catch ex As Exception

            End Try

            If TypeMov = "8" Then
                SaveUnityID = UnityID
                SaveIdEpersonam = IdEpersonam
                SaveDataMovimento = DataMovimento
                SaveTypeMov = TypeMov
                SaveConvenz = Convenz
                SaveTypeMov = TypeMov
            End If

        Next

        If SaveTypeMov = "8" Then
            Dim CentroServizio As New Cls_CentroServizio
            Dim EPersonam As New Cls_Epersonam
            Dim Ospite As New ClsOspite
            Dim Movimenti As New Cls_Movimenti

            Ospite.CodiceOspite = CodiceOspite
            Ospite.Leggi(data.DC_OSPITE, CodiceOspite)



            CentroServizio.EPersonam = EPersonam.RendiWardid(SaveUnityID, data.DC_OSPITE)
            CentroServizio.LeggiEpersonam(data.DC_OSPITE, EPersonam.RendiWardid(SaveUnityID, data.DC_OSPITE), SaveConvenz)

            If Not Verifica Then
                Dim Pc As New Cls_Pianodeiconti
                Pc.Mastro = CentroServizio.MASTRO
                Pc.Conto = CentroServizio.CONTO
                Pc.Sottoconto = CodiceOspite * 100
                Pc.Decodfica(data.DC_GENERALE)
                Pc.Descrizione = Ospite.Nome
                Pc.Tipo = "A"
                Pc.TipoAnagrafica = "O"

                Pc.Scrivi(data.DC_GENERALE)



                Movimenti.CENTROSERVIZIO = CentroServizio.CENTROSERVIZIO
                Movimenti.CodiceOspite = CodiceOspite
                Movimenti.Data = SaveDataMovimento
                Movimenti.TipoMov = "05"
                Movimenti.Descrizione = "Accoglimento EPersonam"
                Movimenti.Progressivo = 1
                Movimenti.EPersonam = SaveIdEpersonam
                Movimenti.Utente = data.UTENTE
                Movimenti.AggiornaDB(data.DC_OSPITE)

                Dim cn As OleDbConnection

                cn = New Data.OleDb.OleDbConnection(data.DC_OSPITE)

                cn.Open()



                Dim cmdIns As New OleDbCommand()
                cmdIns.CommandText = ("INSERT INTO Movimenti_EPersonam  (IdEpersonam,CodiceFiscale,CentroServizio,CodiceOspite,Data,TipoMovimento,Descrizione,DataModifica) VALUES (?,?,?,?,?,?,?,?)")
                cmdIns.Connection = cn
                cmdIns.Parameters.AddWithValue("@IdEpersonam", SaveIdEpersonam)
                cmdIns.Parameters.AddWithValue("@CodiceFiscale", Ospite.CODICEFISCALE)
                cmdIns.Parameters.AddWithValue("@CentroServizio", CentroServizio.CENTROSERVIZIO)
                cmdIns.Parameters.AddWithValue("@CodiceOspite", Ospite.CodiceOspite)
                cmdIns.Parameters.AddWithValue("@Data", Format(DateAdd(DateInterval.Hour, 2, SaveDataMovimento), "dd/MM/yyyy"))
                cmdIns.Parameters.AddWithValue("@TipoMovimento", "A")
                cmdIns.Parameters.AddWithValue("@Descrizione", "")
                cmdIns.Parameters.AddWithValue("@DataModifica", Now)
                cmdIns.ExecuteNonQuery()


                cn.Close()
            End If
            If CentroServizio.CENTROSERVIZIO = "" Then
                MovimentiInserimentoVerifica = False
            Else
                MovimentiInserimentoVerifica = True
            End If
            If MovimentiInserimentoVerifica = True Then
                If Param.NonInAtteasa = 1 Then
                    If Format(SaveDataMovimento, "yyyyMMdd") > Format(Now, "yyyyMMdd") Then
                        MovimentiInserimentoVerifica = False
                    End If
                End If
            End If
        End If

    End Function

    Private Sub Parenti(ByRef data As VariabiliSesssione, ByVal JSonParenti As String, ByVal CodiceOspite As Integer)
        Dim jResults As JArray = JArray.Parse(JSonParenti)
        Dim EPersonam As New Cls_Epersonam
        Dim Appoggio As String

        For Each jTok As JToken In jResults

            Dim Parente As New Cls_Parenti
            Dim DatiSalvatiParente As New Cls_Parenti

            Parente.CodiceOspite = CodiceOspite

            Try
                Parente.CODICEFISCALE = jTok.Item("cf").ToString
            Catch ex As Exception

            End Try
            If Parente.CODICEFISCALE <> "" Then
                Parente.CodiceOspite = CodiceOspite
                Parente.LeggiPerCodiceFiscale(data.DC_OSPITE, Parente.CODICEFISCALE)
            End If


            If Parente.CodiceParente = 0 Then
                Try
                    Parente.IdEpersonam = jTok.Item("id").ToString()
                Catch ex As Exception

                End Try
                Parente.CodiceOspite = CodiceOspite
                Parente.LeggiPerIdEpersonam(data.DC_OSPITE, Parente.IdEpersonam)
            End If


            If Parente.CodiceParente = 0 Then


                Try
                    Parente.CognomeParente = jTok.Item("surname").ToString
                Catch ex As Exception

                End Try

                Try
                    Parente.NomeParente = jTok.Item("firstname").ToString
                Catch ex As Exception

                End Try

                DatiSalvatiParente.NomeParente = Parente.NomeParente
                DatiSalvatiParente.CognomeParente = Parente.CognomeParente
                DatiSalvatiParente.CodiceOspite = 0
                If Not IsNothing(Parente.CognomeParente) And Not IsNothing(Parente.NomeParente) Then
                    DatiSalvatiParente.LeggiPerCognomeNome(data.DC_OSPITE)
                End If
            End If



            Dim CodiceFiscale As New Cls_CodiceFiscale

            'If Parente.CODICEFISCALE <> "" Then
            '    If CodiceFiscale.Check_CodiceFiscale(Parente.CODICEFISCALE) = True Then
            DatiSalvatiParente.CodiceOspite = Parente.CodiceOspite
            DatiSalvatiParente.CodiceParente = Parente.CodiceParente
            DatiSalvatiParente.Leggi(data.DC_OSPITE, DatiSalvatiParente.CodiceOspite, DatiSalvatiParente.CodiceParente)


            Try
                Parente.CognomeParente = jTok.Item("surname").ToString
            Catch ex As Exception

            End Try

            Try
                Parente.NomeParente = jTok.Item("firstname").ToString
            Catch ex As Exception

            End Try

            Parente.Nome = Parente.CognomeParente & " " & Parente.NomeParente

            Try
                Parente.CODICEFISCALE = jTok.Item("cf").ToString
            Catch ex As Exception

            End Try


            Try
                Parente.Telefono2 = jTok.Item("mobile").ToString
            Catch ex As Exception

            End Try

            Try
                Parente.Telefono1 = jTok.Item("phone").ToString
            Catch ex As Exception

            End Try
            Try
                Parente.Telefono3 = jTok.Item("email").ToString
            Catch ex As Exception

            End Try

            Try
                Parente.RESIDENZACAP1 = jTok.Item("residence").Item("cap").ToString
            Catch ex As Exception

            End Try
            'parental_type

            Try
                Parente.GradoParentela = EPersonam.DecodificaGradoParente(jTok.Item("parental_type").Item("cod").ToString, jTok.Item("parental_type").Item("description").ToString, data.DC_OSPITE)
            Catch ex As Exception

            End Try

            Try
                Appoggio = jTok.Item("residence").Item("city_alpha6").ToString()

                Parente.RESIDENZAPROVINCIA1 = Mid(Appoggio & Space(6), 1, 3)
                Parente.RESIDENZACOMUNE1 = Mid(Appoggio & Space(6), 4, 3)
            Catch ex As Exception

            End Try


            Try
                If jTok.Item("signed_to_receive_bill?").ToString.ToUpper = "TRUE" Then
                    Parente.ParenteIndirizzo = 1
                Else
                    Parente.ParenteIndirizzo = 0
                End If
            Catch ex As Exception
                Parente.ParenteIndirizzo = 0
            End Try


            'OspiteIntestatario

            Try
                If jTok.Item("signed_to_bill?").ToString.ToUpper = "TRUE" Then
                    Parente.Intestatario = 1
                Else
                    Parente.Intestatario = 0
                End If
            Catch ex As Exception
                Parente.Intestatario = 0
            End Try


            Try
                Parente.CODICEFISCALE = jTok.Item("cf").ToString()
            Catch ex As Exception

            End Try

            Try
                Parente.RESIDENZAINDIRIZZO1 = jTok.Item("residence").Item("street").ToString()
            Catch ex As Exception

            End Try

            Try
                Parente.IdEpersonam = jTok.Item("id").ToString()
            Catch ex As Exception

            End Try

            If Parente.CodiceParente = 0 Then
                If IsNothing(Parente.CODICEFISCALE) Then
                    Parente.CODICEFISCALE = ""
                End If
                If Parente.CODICEFISCALE <> "" Then
                    Parente.VerificaPrimaScrivereParente(data.DC_OSPITE)
                End If

                Dim Movimenti As New Cls_Movimenti

                Movimenti.CodiceOspite = CodiceOspite
                Movimenti.UltimaDataAccoglimentoSenzaCserv(data.DC_OSPITE)

                Dim CentroServizio As New Cls_CentroServizio


                CentroServizio.CENTROSERVIZIO = Movimenti.CENTROSERVIZIO
                CentroServizio.Leggi(data.DC_OSPITE, CentroServizio.CENTROSERVIZIO)


                Parente.ScriviParente(data.DC_OSPITE)

                Dim Pc As New Cls_Pianodeiconti

                If Parente.CodiceParente = 0 Then

                    Parente.CodiceParente = 0
                End If

                Pc.Mastro = CentroServizio.MASTRO
                Pc.Conto = CentroServizio.CONTO
                Pc.Sottoconto = (CodiceOspite * 100) + Parente.CodiceParente
                Pc.Decodfica(data.DC_GENERALE)
                Pc.Descrizione = Parente.Nome
                Pc.Tipo = "A"
                Pc.TipoAnagrafica = "P"

                Pc.Scrivi(data.DC_GENERALE)

            Else
                Parente.ScriviParente(data.DC_OSPITE)
            End If


            If Parente.ParenteIndirizzo = 1 Then
                Dim Ospite As New ClsOspite

                Ospite.Leggi(data.DC_OSPITE, CodiceOspite)

                Ospite.RecapitoComune = Parente.RESIDENZACOMUNE1
                Ospite.RecapitoProvincia = Parente.RESIDENZAPROVINCIA1
                Ospite.RecapitoIndirizzo = Parente.RESIDENZAINDIRIZZO1
                Ospite.RESIDENZACAP4 = Parente.RESIDENZACAP1
                Ospite.RecapitoNome = Parente.Nome

                Ospite.RESIDENZATELEFONO4 = Parente.Telefono1
                Ospite.TELEFONO4 = Parente.Telefono3
                Ospite.ScriviOspite(data.DC_OSPITE)
            Else
                If DatiSalvatiParente.ParenteIndirizzo = 1 Then
                    Dim Ospite As New ClsOspite

                    Ospite.Leggi(data.DC_OSPITE, CodiceOspite)

                    Ospite.RecapitoComune = ""
                    Ospite.RecapitoProvincia = ""
                    Ospite.RecapitoIndirizzo = ""
                    Ospite.RESIDENZACAP4 = ""
                    Ospite.RecapitoNome = ""

                    Ospite.RESIDENZATELEFONO4 = ""
                    Ospite.TELEFONO4 = ""
                    Ospite.ScriviOspite(data.DC_OSPITE)
                End If
            End If
            '    End If
            'End If
        Next
    End Sub


    Private Function Cartella(ByVal Token As String, ByVal BussinesUnit As String, ByVal CodiceFiscale As String) As String
        Try

            Dim UrlConnessione As String



            'curl -k -H "Authorization: Bearer $TOKEN" -H "Content-Type: application/json" https://api.e-personam.com/api/v1.0/business_units/1771/guests?from=2018-02-04T00:00:00+00:00

            'curl -k -H "Authorization: Bearer $TOKEN" -H "Content-Type: application/json" https://api.e-personam.com/api/v1.0/business_units/3302/guests/updated/2018-04-10T00:00:00+00:00

            UrlConnessione = "https://api.e-personam.com/api/v1.0/business_units/" & BussinesUnit & "/guests/" & CodiceFiscale & "/periods"

            Dim client As HttpWebRequest = WebRequest.Create(UrlConnessione)

            client.Method = "GET"
            client.Headers.Add("Authorization", "Bearer " & Token)
            client.ContentType = "Content-Type: application/json"

            Dim reader As StreamReader
            Dim response As HttpWebResponse = Nothing

            response = DirectCast(client.GetResponse(), HttpWebResponse)

            reader = New StreamReader(response.GetResponseStream())


            Dim rawresp As String
            rawresp = reader.ReadToEnd()

            Dim jResults As JArray = JArray.Parse(rawresp)
            Dim CartellaAppo As String = ""

            For Each jTok As JToken In jResults
                CartellaAppo = jTok.Item("nr_cartella")
            Next
            Return CartellaAppo

        Catch ex As Exception
            Return ""
        End Try


    End Function

    Private Function RichiestaGuests(ByVal Token As String, ByVal BussinesUnit As String, ByVal DataFatt As String, ByVal OraFATT As String, ByVal NumeroPagina As Integer, ByVal NonInAttesa As Integer) As String

        If BussinesUnit = 4508 Then
            BussinesUnit = 4508
        End If

        Try

            Dim UrlConnessione As String



            'curl -k -H "Authorization: Bearer $TOKEN" -H "Content-Type: application/json" https://api.e-personam.com/api/v1.0/business_units/1771/guests?from=2018-02-04T00:00:00+00:00

            'curl -k -H "Authorization: Bearer $TOKEN" -H "Content-Type: application/json" https://api.e-personam.com/api/v1.0/business_units/3302/guests/updated/2018-04-10T00:00:00+00:00

            If NumeroPagina = 0 Then
                UrlConnessione = "https://api.e-personam.com/api/v1.0/business_units/" & BussinesUnit & "/guests/updated/" & DataFatt & OraFATT '"T00:00:00+00:00" ' &to=2050-12-31T00:00:00+00:00"
            Else
                UrlConnessione = "https://api.e-personam.com/api/v1.0/business_units/" & BussinesUnit & "/guests/updated/" & DataFatt & OraFATT & "&page=" & NumeroPagina
            End If

            Dim client As HttpWebRequest = WebRequest.Create(UrlConnessione)

            client.Method = "GET"
            client.Headers.Add("Authorization", "Bearer " & Token)
            client.ContentType = "Content-Type: application/json"

            Dim reader As StreamReader
            Dim response As HttpWebResponse = Nothing

            response = DirectCast(client.GetResponse(), HttpWebResponse)

            reader = New StreamReader(response.GetResponseStream())


            Dim rawresp As String
            rawresp = reader.ReadToEnd()

            If rawresp.ToUpper.IndexOf("BNCCRL22P18F205O") > 0 Then
                rawresp = rawresp
            End If

            Return rawresp

        Catch ex As Exception
            Return "ERRORE :[" & ex.Message & " " & "https://api.e-personam.com/api/v1.0/business_units/" & BussinesUnit & "/guests/updated/" & DataFatt & OraFATT & "]"
        End Try

    End Function

    Private Function LoginPersonam(ByVal data As VariabiliSesssione) As String
        Try


            Dim request As New NameValueCollection


            request.Add("client_id", "98217ca6670c660e22f42d58e231d4e56c84fb34e216b0f66f1f30284fd3ec9d")
            request.Add("client_secret", "5570a684f69ca74d75d16bba669c156ec092eccb4111d0974adec3edb2fa4d6f")


            request.Add("username", data.UTENTE)



            request.Add("password", data.PASSWORD)
            System.Net.ServicePointManager.ServerCertificateValidationCallback = AddressOf CertificateHandler


            Dim client As New WebClient()

            Dim result As Object = client.UploadValues("https://api.e-personam.com/api/v1.0/token/password", "POST", request)


            Dim stringa As String = Encoding.Default.GetString(result)


            Dim serializer As JavaScriptSerializer


            serializer = New JavaScriptSerializer()




            Dim s As Autentificazione = serializer.Deserialize(Of Autentificazione)(stringa)


            Return s.access_token
        Catch ex As Exception
            Return ""
        End Try
    End Function

    Private Function BussineUnit(ByVal Token As String, ByVal data As VariabiliSesssione, ByVal DataFatt As String, ByVal OraFatt As String) As Integer
        Dim rawresp As String

        BussineUnit = 0
        Try



            Dim client As HttpWebRequest = WebRequest.Create("https://api.e-personam.com/api/v1.0/business_units")


            client.Method = "GET"
            client.Headers.Add("Authorization", "Bearer " & Token)
            client.ContentType = "Content-Type: application/json"

            Dim reader As StreamReader
            Dim response As HttpWebResponse = Nothing

            response = DirectCast(client.GetResponse(), HttpWebResponse)

            reader = New StreamReader(response.GetResponseStream())



            rawresp = reader.ReadToEnd()

            Dim jArr As JArray = JArray.Parse(rawresp)
            Dim ElaboratoUnaStruttura As Boolean = False

            For Each jResults As JToken In jArr
                'If Val(jResults.Item("id").ToString) = Val(data.UTENTE.Replace("senioruser_", "").Replace("w", "")) Then
                'If campodb(jResults.Item("structure").ToString).ToUpper = "TRUE" Then

                Dim VerificaSubStructure As String = ""
                Dim Struttura As String = ""

                Try
                    Struttura = campodb(jResults.Item("structure").ToString)
                Catch ex As Exception

                End Try
                If jResults.ToString.IndexOf("Monteverde dopo di noi") > 0 Then
                    Struttura = Struttura
                End If
                Try
                    VerificaSubStructure = campodb(jResults.Item("substructures").ToString)
                Catch ex As Exception

                End Try
                If VerificaSubStructure <> "" Or Struttura.ToUpper = "TRUE" Then
                    AllineamentoBU(data, Token, DataFatt, OraFatt, Val(jResults.Item("id").ToString))
                    ElaboratoUnaStruttura = True
                End If
                'End If
                'Return Val(jResults.Item("id").ToString)
                'Exit Function
                'End If
            Next


            If Not ElaboratoUnaStruttura Then
                For Each jResults As JToken In jArr
                    AllineamentoBU(data, Token, DataFatt, OraFatt, Val(jResults.Item("id").ToString))
                Next
            End If


            'For Each jResults As JToken In jArr
            '    AllineamentoBU(data, Token, DataFatt, OraFatt, Val(jResults.Item("id").ToString))
            'Next
        Catch ex As Exception

        End Try

    End Function

    'https://api.e-personam.com/api/v1.0/business_units

    Public Class Autentificazione
        Public access_token As String
        Public expires_in As String
        Public scope As String
        Public token_type As String
        Public refresh_token As String
    End Class


    Private Shared Function CertificateHandler(ByVal sender As Object, ByVal certificate As X509Certificate, ByVal chain As X509Chain, ByVal SSLerror As SslPolicyErrors) As Boolean
        Return True
    End Function

    Function campodb(ByVal oggetto As Object) As String
        If IsDBNull(oggetto) Then
            Return ""
        Else
            Return oggetto
        End If
    End Function
    Function campodbN(ByVal oggetto As Object) As Double
        If IsDBNull(oggetto) Then
            Return 0
        Else
            Return oggetto
        End If
    End Function

    Function campodbd(ByVal oggetto As Object) As Date
        If IsDBNull(oggetto) Then
            Return Nothing
        Else
            Return oggetto
        End If
    End Function

    Protected Sub Timer1_Tick(ByVal sender As Object, ByVal e As System.EventArgs) Handles Timer1.Tick
        If Val(System.Web.HttpRuntime.Cache("CampoProgressBar" + Session.SessionID)) > 100 Or (DateDiff("s", Session("INIZIO"), Now) > 60 And Request.Item("RICERCA") = "SI") Then
            Dim Param As New Cls_Parametri

            Param.LeggiParametri(Session("DC_OSPITE"))
            Param.DataCheckEpersonam = Now
            Param.ScriviParametri(Session("DC_OSPITE"))

            If Request.Item("RICERCA") = "SI" Then
                Response.Redirect("../RicercaAnagrafica.aspx")
            Else
                Response.Redirect("../Menu_Epersonam.aspx")
            End If
        Else
            Lbl_Waiting.Text = "<div id=""blur"">&nbsp;</div><div id=""pippo""  class=""wait"">"
            Lbl_Waiting.Text = Lbl_Waiting.Text & "Dialogo In Corso... <br /><br /><br />"
            Lbl_Waiting.Text = Lbl_Waiting.Text & "<img  height=30px src=""../images/loading.gif""><br />"
            Lbl_Waiting.Text = Lbl_Waiting.Text & "</div>"
            REM Lbl_Waiting.Text = Lbl_Waiting.Text & "<a href=""RicercaAnagrafica.aspx"" style=""z-index: 130;"">Continua senza allineare</a>"
        End If
    End Sub

    Protected Sub lnk_Continua_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lnk_Continua.Click
        Session("INTERROMPI") = "SI"
        Response.Redirect("../RicercaAnagrafica.aspx")
    End Sub
End Class
