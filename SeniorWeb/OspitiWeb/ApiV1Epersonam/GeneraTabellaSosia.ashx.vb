﻿Imports System.Data.OleDb
Imports System.IO
Imports System.Net
Imports System.Net.Security
Imports System.Security.Cryptography.X509Certificates
Imports System.Web.Script.Serialization
Imports Newtonsoft.Json.Linq

Public Class GeneraTabellaSosia
    Implements System.Web.IHttpHandler


    Public Sub ProcessRequest(ByVal context As HttpContext) Implements IHttpHandler.ProcessRequest
        context.Response.ContentType = "text/plain"
        Dim CSERV As String = context.Request.QueryString("CSERV")
        Dim ANNO As String = context.Request.QueryString("ANNO")
        Dim MESE As String = context.Request.QueryString("MESE")
        Dim UTENTE As String = context.Request.QueryString("UTENTE")

        Dim Inizio As String = Now.ToString

        context.Response.Cache.SetCacheability(HttpCacheability.NoCache)
        Dim DbC As New Cls_Login



        DbC.Utente = UTENTE
        DbC.LeggiSP(context.Application("SENIOR"))

        Dim Letto As String = Now.ToString
        Dim Token As String

        Token = LoginPersonam(context)

        Dim cn As OleDbConnection

        cn = New Data.OleDb.OleDbConnection(DbC.Ospiti)

        cn.Open()
        Dim cmdInsert As New OleDbCommand()

        cmdInsert.CommandText = "DELETE FROM DatiSosia WHERE CentroServizio = ?"
        cmdInsert.Connection = cn
        cmdInsert.Parameters.AddWithValue("@CentroServizio", CSERV)
        cmdInsert.ExecuteNonQuery()

        cn.Close()


        CreaTabellaSosia(DbC.Ospiti, CSERV, DateSerial(ANNO, 1, 1), DateSerial(ANNO + 4, 1, 1), Token)

        context.Response.Write(Token)
    End Sub

    Private Sub CreaTabellaSosia(ByVal ConnectionString As String, ByVal Cserv As String, ByVal DataDal As Date, ByVal DataAl As Date, ByVal Token As String)
        Dim x As Long
        Dim MySql As String
        Dim cn As OleDbConnection

        cn = New Data.OleDb.OleDbConnection(ConnectionString)

        cn.Open()

        MySql = "Select * From AnagraficaComune Where CodiceParente = 0 And CodiceOspite > 0 And ( (Select count(*) From Movimenti Where CodiceOspite = AnagraficaComune.CodiceOspite And Data >= ? And Data <= ? And CENTROSERVIZIO = '" & Cserv & "') > 0 OR " &
                                                " (" &
                                                " ((Select Top 1 TipoMov From Movimenti Where CodiceOspite = AnagraficaComune.CodiceOspite And Data < ? And CENTROSERVIZIO = '" & Cserv & "' Order By Data DESC,Progressivo Desc) <> '13') " &
                                                " And " &
                                                " ((Select  count(*) From Movimenti Where CodiceOspite = AnagraficaComune.CodiceOspite And Data < ? And CENTROSERVIZIO = '" & Cserv & "' And TipoMov = '05') <>  0) " &
                                                " )" &
                                                " ) order by Nome"
        Dim cmdCau As New OleDbCommand()
        cmdCau.CommandText = MySql
        cmdCau.Connection = cn

        cmdCau.Parameters.AddWithValue("@DataDal", DataDal)
        cmdCau.Parameters.AddWithValue("@DataAl", DataAl)
        cmdCau.Parameters.AddWithValue("@DataDal", DataDal)
        cmdCau.Parameters.AddWithValue("@DataDal", DataDal)
        Dim MyCau As OleDbDataReader = cmdCau.ExecuteReader()
        Do While MyCau.Read
            Dim CodiceFiscale As String
            CodiceFiscale = campodb(MyCau.Item("CODICEFISCALE"))
            Dim CentroServizio As New Cls_CentroServizio

            CentroServizio.CENTROSERVIZIO = Cserv
            CentroServizio.Leggi(ConnectionString, CentroServizio.CENTROSERVIZIO)

            Dim Json As String

            Json = LeggiSosia(CentroServizio.EPersonam, Token, Val(campodb(MyCau.Item("IdePersonam"))))

            Dim jResults As JArray = JArray.Parse(Json)


            For Each jTok1 As JToken In jResults.Children

                Dim from As String
                Dim classe As String
                from = jTok1.Item("from").ToString()
                classe = jTok1.Item("classe").ToString()

                Dim cmdInsert As New OleDbCommand()

                cmdInsert.CommandText = "INSERT INTO DatiSosia (CentroServizio,CodiceOspite,Sosia,Data) VALUES (?,?,?,?)"
                cmdInsert.Connection = cn
                cmdInsert.Parameters.AddWithValue("@CentroServizio", Cserv)
                cmdInsert.Parameters.AddWithValue("@CodiceOspite", Val(campodb(MyCau.Item("CodiceOspite"))))
                cmdInsert.Parameters.AddWithValue("@Sosia", classe)
                cmdInsert.Parameters.AddWithValue("@Data", from)
                cmdInsert.ExecuteNonQuery()

            Next

        Loop
        MyCau.Close()

    End Sub

    Public ReadOnly Property IsReusable() As Boolean Implements IHttpHandler.IsReusable
        Get
            Return False
        End Get
    End Property

    Public Class Autentificazione
        Public access_token As String
        Public expires_in As String
        Public scope As String
        Public token_type As String
        Public refresh_token As String
    End Class


    Private Function LeggiSosia(ByVal Nucleo As String, ByVal token As String, ByVal cf As String) As String
        Dim rawresp As String

        Try



            Dim client As HttpWebRequest = WebRequest.Create("https://api.e-personam.com/api/v1.0/business_units/" & Nucleo & "/guests/" & cf & "/sosia2s?from=20170101")


            client.Method = "GET"
            client.Headers.Add("Authorization", "Bearer " & token)
            client.ContentType = "Content-Type: application/json"

            Dim reader As StreamReader
            Dim response As HttpWebResponse = Nothing

            response = DirectCast(client.GetResponse(), HttpWebResponse)

            reader = New StreamReader(response.GetResponseStream())



            rawresp = reader.ReadToEnd()

        Catch ex As Exception

        End Try


        Return rawresp
    End Function




    Private Shared Function CertificateHandler(ByVal sender As Object, ByVal certificate As X509Certificate, ByVal chain As X509Chain, ByVal SSLerror As SslPolicyErrors) As Boolean
        Return True
    End Function


    Private Function LoginPersonam(ByVal context As HttpContext) As String
        Try


            Dim request As New NameValueCollection


            request.Add("client_id", "98217ca6670c660e22f42d58e231d4e56c84fb34e216b0f66f1f30284fd3ec9d")
            request.Add("client_secret", "5570a684f69ca74d75d16bba669c156ec092eccb4111d0974adec3edb2fa4d6f")

            request.Add("username", "gabellini sauro")

            Dim guarda As String

            guarda = "Serpiolle!"

            request.Add("password", guarda)
            System.Net.ServicePointManager.ServerCertificateValidationCallback = AddressOf CertificateHandler


            Dim client As New WebClient()

            Dim result As Object = client.UploadValues("https://api.e-personam.com/api/v1.0/token/password", "POST", request)


            Dim stringa As String = Encoding.Default.GetString(result)


            Dim serializer As JavaScriptSerializer


            serializer = New JavaScriptSerializer()




            Dim s As Autentificazione = serializer.Deserialize(Of Autentificazione)(stringa)


            Return s.access_token
        Catch ex As Exception
            Return ""
        End Try

    End Function

    Function campodb(ByVal oggetto As Object) As String
        If IsDBNull(oggetto) Then
            Return ""
        Else
            Return oggetto
        End If
    End Function

End Class