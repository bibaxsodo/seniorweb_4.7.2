﻿Imports System
Imports System.Web
Imports Microsoft.VisualBasic
Imports System.Data.OleDb
Imports System.Web.Hosting
Imports System.Data.SqlTypes
Imports System.Net
Imports System.Collections.Generic
Imports System.IO
Imports System.Security.Cryptography.X509Certificates
Imports System.Net.Security
Imports System.Web.Script.Serialization
Imports org.jivesoftware.util
Imports Newtonsoft.Json
Imports Newtonsoft.Json.Linq
Partial Class OspitiWeb_ApiV1Epersonam_ImportListini
    Inherits System.Web.UI.Page
    Dim Tabella As New System.Data.DataTable("tabella")


    Function campodb(ByVal oggetto As Object) As String
        If IsDBNull(oggetto) Then
            Return ""
        Else
            Return oggetto
        End If
    End Function
    Private Function LoginPersonam(ByVal context As HttpContext) As String
        Try


            Dim request As New NameValueCollection


            request.Add("client_id", "98217ca6670c660e22f42d58e231d4e56c84fb34e216b0f66f1f30284fd3ec9d")
            request.Add("client_secret", "5570a684f69ca74d75d16bba669c156ec092eccb4111d0974adec3edb2fa4d6f")


            request.Add("username", context.Session("EPersonamUser"))



            request.Add("password", context.Session("EPersonamPSW"))
            System.Net.ServicePointManager.ServerCertificateValidationCallback = AddressOf CertificateHandler


            Dim client As New WebClient()

            Dim result As Object = client.UploadValues("https://api.e-personam.com/api/v1.0/token/password", "POST", request)


            Dim stringa As String = Encoding.Default.GetString(result)


            Dim serializer As JavaScriptSerializer


            serializer = New JavaScriptSerializer()




            Dim s As Autentificazione = serializer.Deserialize(Of Autentificazione)(stringa)


            Return s.access_token
        Catch ex As Exception
            Return ""
        End Try
    End Function

    Protected Sub Btn_Movimenti_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles Btn_Movimenti.Click
        Call StartImport()
    End Sub


    Private Sub StartImport()



        Dim cn As OleDbConnection
        Dim Trovato As Boolean = False
        Dim Token As String

        cn = New Data.OleDb.OleDbConnection(Session("DC_OSPITE"))

        cn.Open()


        Token = LoginPersonam(Context)

        BusinessUnit(Token, Context)
        cn.Close()
    End Sub

    Public Class Autentificazione
        Public access_token As String
        Public expires_in As String
        Public scope As String
        Public token_type As String
        Public refresh_token As String
    End Class

    Private Shared Function CertificateHandler(ByVal sender As Object, ByVal certificate As X509Certificate, ByVal chain As X509Chain, ByVal SSLerror As SslPolicyErrors) As Boolean
        Return True
    End Function



    Private Function BusinessUnit(ByVal Token As String, ByVal context As HttpContext) As Long
        Dim rawresp As String

        BusinessUnit = 0
        Try



            Tabella.Clear()
            Tabella.Columns.Clear()

            Tabella.Columns.Add("IdListino", GetType(String))
            Tabella.Columns.Add("Descrizione", GetType(String))
            Tabella.Columns.Add("Tipo", GetType(String))
            Tabella.Columns.Add("Importo", GetType(String))
            Tabella.Columns.Add("DataScadenza", GetType(String))
            Tabella.Columns.Add("Elabora", GetType(String))



            Dim client As HttpWebRequest = WebRequest.Create("https://api.e-personam.com/api/v1.0/business_units")


            client.Method = "GET"
            client.Headers.Add("Authorization", "Bearer " & Token)
            client.ContentType = "Content-Type: application/json"

            Dim reader As StreamReader
            Dim response As HttpWebResponse = Nothing

            response = DirectCast(client.GetResponse(), HttpWebResponse)

            reader = New StreamReader(response.GetResponseStream())

            rawresp = reader.ReadToEnd()


            Dim jArr As JArray = JArray.Parse(rawresp)


            For Each jResults As JToken In jArr

                BusinessUnit = Val(jResults.Item("id").ToString)
                ImportaMovimenti(BusinessUnit, Token)
                Exit For
            Next
        Catch ex As Exception

        End Try

        ViewState("App_AddebitiMultiplo") = Tabella

        GridView1.AutoGenerateColumns = False

        GridView1.DataSource = Tabella
        GridView1.DataBind()
        GridView1.Visible = True
    End Function



    Private Sub ImportaMovimenti(ByVal BusinessID As Integer, ByVal Token As String)
        Dim request As New NameValueCollection
        Dim rawresp As String
        Dim cn As OleDbConnection

        Dim Param As New Cls_Parametri

        Param.LeggiParametri(Context.Session("DC_OSPITE"))


        Dim DataFatt As String

        If Param.MeseFatturazione > 9 Then
            DataFatt = Param.AnnoFatturazione & "/" & Param.MeseFatturazione
        Else
            DataFatt = Param.AnnoFatturazione & "/" & Param.MeseFatturazione
        End If

        Try

            System.Net.ServicePointManager.ServerCertificateValidationCallback = AddressOf CertificateHandler


            Dim client As HttpWebRequest = WebRequest.Create("https://api.e-personam.com/api/v1.0/business_units/" & BusinessID & "/price_templates")

            client.Method = "GET"
            client.Headers.Add("Authorization", "Bearer " & Token)
            client.ContentType = "Content-Type: application/json"


            Dim reader As StreamReader
            Dim response As HttpWebResponse = Nothing

            response = DirectCast(client.GetResponse(), HttpWebResponse)

            reader = New StreamReader(response.GetResponseStream())


            rawresp = reader.ReadToEnd()

            Dim jResults As JArray = JArray.Parse(rawresp)


            cn = New Data.OleDb.OleDbConnection(Session("DC_OSPITE"))

            cn.Open()


            For Each jTok As JToken In jResults
               
                Dim myriga As System.Data.DataRow = Tabella.NewRow()

                myriga.Item(0) = jTok.Item("id")
                myriga.Item(1) = jTok.Item("name")

                Dim Tipo As String = ""
                Try
                    Tipo = jTok.Item("price_type").Item("name")
                Catch ex As Exception

                End Try
                myriga.Item(2) = Tipo

                myriga.Item(3) = jTok.Item("value")
                Try
                    myriga.Item(4) = jTok.Item("dateout")
                Catch ex As Exception

                End Try
                Dim Listino As New Cls_Tabella_Listino

                Listino.Descrizione = ""
                Listino.Codice = jTok.Item("id")
                Listino.LeggiCausale(Session("DC_OSPITE"))

                If Listino.Descrizione <> "" Then
                    myriga.Item(5) = "ERROREPRE"
                End If


                Tabella.Rows.Add(myriga)
                
            Next


            cn.Close()

        Catch ex As Exception

        End Try

    End Sub

    Protected Sub GridView1_RowDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs) Handles GridView1.RowDataBound
        If e.Row.RowType = DataControlRowType.DataRow Then

            If Tabella.Rows(e.Row.RowIndex).Item(5).ToString = "ERROREPRE" Then


                e.Row.BackColor = Drawing.Color.Aqua

            End If

        End If

    End Sub

    Protected Sub Btn_Modifica_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles Btn_Modifica.Click
        Dim Indice As Integer
        Tabella = ViewState("App_AddebitiMultiplo")

        For Indice = 0 To GridView1.Rows.Count - 1
            Dim CheckBox As CheckBox = DirectCast(GridView1.Rows(Indice).FindControl("ChkImporta"), CheckBox)

            If CheckBox.Checked = True Then
                Dim Listino As New Cls_Tabella_Listino

                Listino.Descrizione = ""
                Listino.Codice = Tabella.Rows(Indice).Item(0)
                Listino.LeggiCausale(Session("DC_OSPITE"))

                Listino.Codice = Tabella.Rows(Indice).Item(0)
                Listino.Descrizione = Tabella.Rows(Indice).Item(1)
                If Tabella.Rows(Indice).Item(2) = "alberghiero" Then
                    Listino.TIPOLISTINO = "S"
                    Listino.IMPORTORETTATOTALE = Tabella.Rows(Indice).Item(3)
                End If
                If Tabella.Rows(Indice).Item(2) = "sanitario" Then
                    Listino.TIPOLISTINO = "R"
                End If

                Listino.DATA = campodbd(Tabella.Rows(Indice).Item(4))

                Listino.Scrivi(Session("DC_OSPITE"))
            End If
        Next

        StartImport()
    End Sub


    Function campodbN(ByVal oggetto As Object) As Double
        If IsDBNull(oggetto) Then
            Return 0
        Else
            Return oggetto
        End If
    End Function

    Function campodbd(ByVal oggetto As Object) As Date
        If IsDBNull(oggetto) Then
            Return Nothing
        Else
            Return oggetto
        End If
    End Function

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If IsNothing(Session("DC_OSPITE")) Then
            Response.Redirect("..\Login.aspx")
            Exit Sub
        End If


        If Page.IsPostBack = False Then


            Dim K1 As New Cls_SqlString

            Dim Barra As New Cls_BarraSenior

            If Not IsNothing(Session("RicercaAnagraficaSQLString")) Then
                Try
                    K1 = Session("RicercaAnagraficaSQLString")
                Catch ex As Exception
                    K1 = Nothing
                End Try
            End If

            Lbl_BarraSenior.Text = Barra.CodiceBarra(Application("SENIOR"), Session("UTENTE"), Page, K1)

            Dim Param As New Cls_Parametri

            Param.LeggiParametri(Context.Session("DC_OSPITE"))



            Dim DataFatt As String

            If Param.MeseFatturazione > 9 Then
                DataFatt = Param.AnnoFatturazione & "-" & Param.MeseFatturazione & "-01"
            Else
                DataFatt = Param.AnnoFatturazione & "-0" & Param.MeseFatturazione & "-01"
            End If



        End If
    End Sub

    Protected Sub Btn_Esci_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles Btn_Esci.Click
        Response.Redirect("../Menu_Epersonam.aspx")
    End Sub

    Protected Sub Btn_Seleziona_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles Btn_Seleziona.Click
        Dim Param As New Cls_Parametri

        Param.LeggiParametri(Session("DC_OSPITE"))

        For Riga = 0 To GridView1.Rows.Count - 1

            Dim CheckBox As CheckBox = DirectCast(GridView1.Rows(Riga).FindControl("ChkImporta"), CheckBox)

            If CheckBox.Enabled = True And GridView1.Rows(Riga).BackColor <> System.Drawing.Color.Orange Then
                CheckBox.Checked = True
                If GridView1.Rows(Riga).Cells(5).Text.IndexOf(Param.MeseFatturazione & "/" & Param.AnnoFatturazione) > 0 Then
                    CheckBox.Checked = True
                End If
            End If
        Next
    End Sub
End Class
