﻿<%@ Page Language="VB" AutoEventWireup="false" Inherits="DatiSanitari" EnableEventValidation="false" CodeFile="DatiSanitari.aspx.vb" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="xasp" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <meta http-equiv="x-ua-compatible" content="IE=9" />
    <title>Dati Sanitari</title>
    <asp:PlaceHolder runat="server">
        <%: System.Web.Optimization.Styles.Render("~/Content/AjaxControlToolkit/Styles/Bundle") %>
    </asp:PlaceHolder>
    <link href="ospiti.css?ver=11" rel="stylesheet" type="text/css" />
    <link rel="shortcut icon" href="images/SENIOR.ico" />
    <link href="js/jquery.autocomplete.css" rel="stylesheet" type="text/css" />


    <script src="js/jquery-1.5.1.min.js" type="text/javascript"></script>
    <script src="js/jquery.autocomplete.js" type="text/javascript"></script>
    <script src="js/soapclient.js" type="text/javascript"></script>
    <script src="/js/convertinumero.js" type="text/javascript"></script>



    <script src="/js/pianoconti.js" type="text/javascript"></script>
    <script src="js/jquery.maskedinput-1.3.min.js" type="text/javascript"></script>
    <script src="/js/formatnumer.js" type="text/javascript"></script>
    <script src="js/JSErrore.js" type="text/javascript"></script>
    <script type="text/javascript" src="../js/menuanagraficaospiti.js?ver=7"></script>

    <link rel="stylesheet" href="dialogbox/redips-dialog.css" type="text/css" media="screen" />
    <script type="text/javascript" src="dialogbox/redips-dialog-min.js"></script>
    <script type="text/javascript" src="dialogbox/script.js"></script>

    <link rel="stylesheet" href="jqueryui/jquery-ui.css" type="text/css">
    <script src="jqueryui/jquery-ui.js" type="text/javascript"></script>
    <script src="jqueryui/datapicker-it.js" type="text/javascript"></script>
    <script type="text/javascript">
        function DialogBox(Path) {

            REDIPS.dialog.show(500, 300, '<iframe id="output" src="' + Path + '" height="300px" width="500"></iframe>');
            return false;

        }
        $(document).ready(function () {
            $('html').keyup(function (event) {

                if (event.keyCode == 113) {
                    __doPostBack("Btn_Modifica", "0");
                }


            });
        });

        $(document).ready(function () {
            if (window.innerHeight > 0) { $("#BarraLaterale").css("height", (window.innerHeight - 94) + "px"); } else { $("#BarraLaterale").css("height", (document.documentElement.offsetHeight - 94) + "px"); }
        });
    </script>
</head>
<body>
    <form id="form1" runat="server">
        <asp:ScriptManager ID="ScriptManager1" runat="server" EnableScriptGlobalization="true">
            <Scripts>
                <asp:ScriptReference Path="~/Scripts/AjaxControlToolkit/Bundle" />
            </Scripts>
        </asp:ScriptManager>
        <asp:Label ID="Lbl_BarraSenior" runat="server" Text=""></asp:Label>
        <div align="left">
            <table style="width: 100%; vertical-align: top;" cellpadding="0" cellspacing="0">
                <tr>
                    <td style="width: 160px; background-color: #F0F0F0;"></td>
                    <td>
                        <div class="Titolo">Ospiti - Dati Sanitari</div>
                        <div class="SottoTitoloOSPITE">
                            <asp:Label ID="Lbl_NomeOspite" runat="server"></asp:Label><br />
                            <br />
                        </div>
                    </td>
                    <td style="text-align: right;">
                        <div class="DivTastiOspite">
                            <asp:ImageButton ID="Btn_Modifica0" runat="server" Height="38px" ImageUrl="~/images/salva.jpg" class="EffettoBottoniTondi" Width="38px" ToolTip="Modifica / Inserisci (F2)" />
                        </div>
                    </td>
                </tr>

                <tr>
                    <td style="width: 160px; background-color: #F0F0F0; vertical-align: top; text-align: center;" id="BarraLaterale">
                        <a href="Menu_Ospiti.aspx" style="border-width: 0px;">
                            <img src="images/Home.jpg" alt="Menù" class="Effetto" /></a><br />
                        <asp:ImageButton ID="Btn_Esci" runat="server" BackColor="Transparent" ImageUrl="../images/Menu_Indietro.png" class="Effetto" ToolTip="Chiudi" />
                        <br />
                        <div id="MENUDIV"></div>

                        <br />
                        <br />
                        <br />
                        <br />
                        <br />
                        <br />
                        <br />
                        <br />
                    </td>
                    <td colspan="2" style="background-color: #FFFFFF; vertical-align: top;">
                        <xasp:TabContainer ID="TabContainer1" runat="server" ActiveTabIndex="0" CssClass="TabSenior"
                            Width="100%" BorderStyle="None" Style="margin-right: 39px">
                            <xasp:TabPanel runat="server" HeaderText="Prima Nota" ID="Tab_Anagrafica">
                                <HeaderTemplate>
                                    Dati Sanitari              
                                </HeaderTemplate>
                                <ContentTemplate>

                                    <label class="LabelCampo">Codice Medico :</label>
                                    <asp:DropDownList ID="DD_Medico" runat="server" AutoPostBack="true" Width="411px"></asp:DropDownList><br />
                                    <br />
                                    <label class="LabelCampo">Nome Medico :</label>
                                    <asp:TextBox ID="Txt_NomeMedico" MaxLength="120" runat="server" Width="250px"></asp:TextBox>
                                    <br />
                                    <br />
                                    <label class="LabelCampo">Codice Sanitario :</label>
                                    <asp:TextBox ID="Txt_CodiceSanitario" MaxLength="14" runat="server" Width="190px"></asp:TextBox>
                                    <br />
                                    <br />
                                    <label class="LabelCampo">Tessera Sanitaria :</label>
                                    <asp:TextBox ID="Txt_TesseraSanitaria" MaxLength="40" runat="server" Width="190px"></asp:TextBox>
                                    <br />
                                    <br />

                                    <label class="LabelCampo">Azienda USL :</label>
                                    <asp:DropDownList ID="DD_Regione" runat="server"></asp:DropDownList><br />
                                    <br />

                                    <label class="LabelCampo">Esenzione Ticket :</label>
                                    <asp:TextBox ID="Txt_Numero1" MaxLength="10" runat="server" Width="150px"></asp:TextBox>
                                    Dal :<asp:TextBox ID="Txt_Dal1" runat="server" Width="90px"></asp:TextBox>
                                    Al :<asp:TextBox ID="Txt_Al1" runat="server" Width="90px"></asp:TextBox><br />
                                    <br />

                                    <label class="LabelCampo">Esenzione Ticket :</label>
                                    <asp:TextBox ID="Txt_Numero2" MaxLength="10" runat="server" Width="150px"></asp:TextBox>
                                    Dal :<asp:TextBox ID="Txt_Dal2" runat="server" Width="90px"></asp:TextBox>
                                    Al :<asp:TextBox ID="Txt_al2" runat="server" Width="90px"></asp:TextBox><br />
                                    <br />
                                    <label class="LabelCampo">Descrizione Ticket :</label>
                                    <asp:TextBox ID="Txt_DescrizioneTicket" MaxLength="40" runat="server" Width="350px"></asp:TextBox><br />
                                    <br />
                                    <label class="LabelCampo">Numero Cartella :</label>
                                    <asp:TextBox ID="Txt_NumeroCartella" MaxLength="40" runat="server" Width="190px"></asp:TextBox>
                                    <br />
                                    <br />


                                </ContentTemplate>

                            </xasp:TabPanel>
                        </xasp:TabContainer>
                    </td>
                </tr>
            </table>
        </div>
    </form>
</body>
</html>
