﻿Imports System.Web.Hosting
Imports System.Data.OleDb

Partial Class RicercaAnagrafica
    Inherits System.Web.UI.Page

    Dim MyTable As New System.Data.DataTable("tabella")
    Dim MyDataSet As New System.Data.DataSet()

    Protected Sub Grd_ImportoOspite_PageIndexChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewPageEventArgs) Handles Grd_ImportoOspite.PageIndexChanging
        MyTable = ViewState("Appoggio")

        Grd_ImportoOspite.AutoGenerateColumns = True

        Grd_ImportoOspite.PageIndex = e.NewPageIndex
        Grd_ImportoOspite.DataSource = MyTable

        Grd_ImportoOspite.DataBind()

        For i = 0 To Grd_ImportoOspite.Rows.Count - 1
            Dim K As New ClsOspite
            K.CodiceOspite = Grd_ImportoOspite.Rows(i).Cells(1).Text
            K.Leggi(Session("DC_OSPITE"), K.CodiceOspite)

            Grd_ImportoOspite.Rows(i).Cells(4).Text = Grd_ImportoOspite.Rows(i).Cells(4).Text & "<br><font size=""2""  color=""#3572b0"">" & K.CODICEFISCALE & "</font> <font size=""2""  color=""#3572b0"">" & K.CartellaClinica & "</font>"
            Grd_ImportoOspite.Rows(i).Cells(5).Text = Grd_ImportoOspite.Rows(i).Cells(5).Text & "<br><font size=""2""  color=""#3572b0"">" & DateDiff(DateInterval.Year, K.DataNascita, Now) & " anni</font>"
        Next

    End Sub

    Protected Sub Grd_ImportoOspite_RowCommand(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewCommandEventArgs) Handles Grd_ImportoOspite.RowCommand
        If Session("ABILITAZIONI").IndexOf("<ANAGRAFICA>") < 1 Then
            Exit Sub
        End If
        If e.CommandName = "Seleziona" Then
            Dim d As Integer


            MyTable = ViewState("Appoggio")

            d = Val(e.CommandArgument)
            Session("CODICESERVIZIO") = MyTable.Rows(d).Item(1).ToString
            Session("CODICEOSPITE") = MyTable.Rows(d).Item(0).ToString



            If Request.Item("TIPO") = "FATTURANC" Then
                Response.Redirect("FatturaNC.aspx?PAGINA=" & Grd_ImportoOspite.PageIndex)
                Exit Sub
            End If

            If Request.Item("TIPO") = "INCASSIANTICIPI" Then
                Response.Redirect("ElencoRegistrazione.aspx?PAGINA=" & Grd_ImportoOspite.PageIndex)
                Exit Sub
            End If

            If Request.Item("TIPO") = "MOVIMENTI" Then
                Response.Redirect("MOVIMENTI.aspx?PAGINA=" & Grd_ImportoOspite.PageIndex)
                Exit Sub
            End If

            If Request.Item("TIPO") = "ADDACR" Then
                Response.Redirect("GrigliaAddebitiAccrediti.aspx?PAGINA=" & Grd_ImportoOspite.PageIndex)
                Exit Sub
            End If


            If Request.Item("TIPO") = "DIURNO" Then
                Response.Redirect("MesiDiurno.aspx?PAGINA=" & Grd_ImportoOspite.PageIndex)
                Exit Sub
            End If

            If Request.Item("TIPO") = "GESTIONEDENARO" Then
                Response.Redirect("ElencoMovimentiDenaro.aspx?PAGINA=" & Grd_ImportoOspite.PageIndex)
                Exit Sub
            End If

            If Request.Item("TIPO") = "DOCOSPPAR" Then
                Response.Redirect("DocumentiOspiteParente.aspx?PAGINA=" & Grd_ImportoOspite.PageIndex)
                Exit Sub
            End If

            If Request.Item("TIPO") = "EstrattoContoDenaro" Then
                Response.Redirect("EstrattoContoDenaro.aspx?PAGINA=" & Grd_ImportoOspite.PageIndex)
                Exit Sub
            End If

            If Request.Item("TIPO") = "MODIFICARETTA" Then
                Response.Redirect("ModificaRetta.aspx?PAGINA=" & Grd_ImportoOspite.PageIndex)
                Exit Sub
            End If

            If Request.Item("TIPO") = "MODIFICACSERV" Then
                Response.Redirect("ModificaCentroServizio.aspx?PAGINA=" & Grd_ImportoOspite.PageIndex)
                Exit Sub
            End If

            If Request.Item("TIPO") = "DOM" Then
                Response.Redirect("Elenco_MovimentiDomiciliare.aspx?PAGINA=" & Grd_ImportoOspite.PageIndex)
                Exit Sub
            End If


            Response.Redirect("Anagrafica.aspx?PAGINA=" & Grd_ImportoOspite.PageIndex & "&CentroServizio=" & Session("CODICESERVIZIO") & "&CODICEOSPITE=" & Session("CODICEOSPITE"))

        End If
    End Sub

 

    Protected Sub Ricerca()
        Dim f As New ClsOspite

        Dim StringaConnessione As String = Session("DC_OSPITE")

        Dim cn As OleDbConnection
        Dim Riga As Long = 0

        cn = New Data.OleDb.OleDbConnection(StringaConnessione)

        cn.Open()
        Dim cmd As New OleDbCommand()

        Dim k1 As New Cls_SqlString
        Dim Condizione As String = ""

        k1.Add("Txt_ID", Txt_CodiceOspite.Text)
        k1.Add("Txt_Descrizione", Txt_CognomeNome.Text)
        k1.Add("DD_CServ", DD_CServ.SelectedValue)
        k1.Add("DD_Struttura", DD_Struttura.SelectedValue)
        If Chk_NonInUso.Checked = True Then
            k1.Add("SoloNonInUSO", "T")
        Else
            k1.Add("SoloNonInUSO", "F")
        End If
        If Chk_OrdinePerCodice.Checked = True Then
            k1.Add("OrdinePerCodice", "T")
        Else
            k1.Add("OrdinePerCodice", "F")
        End If
        Session("RicercaAnagraficaSQLString") = k1


        Condizione = ""
        If Val(Txt_CodiceOspite.Text) > 0 Then
            If Condizione <> "" Then
                Condizione = Condizione & " And "
            End If
            Condizione = Condizione & " AnagraficaComune.CodiceOspite = " & Val(Txt_CodiceOspite.Text)
        End If

        If DD_CServ.SelectedValue <> "" Then
            If Condizione <> "" Then
                Condizione = Condizione & " And "
            End If
            Condizione = Condizione & "  MOVIMENTI.CentroServizio = '" & DD_CServ.SelectedValue & "'"
        Else
            If DD_Struttura.SelectedValue <> "" Then
                If Condizione <> "" Then
                    Condizione = Condizione & " And ("
                Else
                    Condizione = Condizione & " ("
                End If
                Dim Indice As Integer
                Call AggiornaCServ()
                For Indice = 0 To DD_CServ.Items.Count - 1
                    If Indice >= 1 Then
                        If Condizione <> "" Then
                            Condizione = Condizione & " Or "
                        End If
                    End If
                    Condizione = Condizione & "  MOVIMENTI.CentroServizio = '" & DD_CServ.Items(Indice).Value & "'"
                Next
                Condizione = Condizione & ") "
            End If
        End If



        If Txt_CognomeNome.Text <> "" Then
            If Condizione <> "" Then
                Condizione = Condizione & " And "
            End If
            Condizione = Condizione & "  AnagraficaComune.CodiceOspite > 0 And Nome Like '" & Txt_CognomeNome.Text.Replace("'", "''") & "'"
        End If

        If Chk_NonInUso.Checked = False Then
            If Condizione <> "" Then
                Condizione = Condizione & " And "
            End If
            Condizione = Condizione & " (AnagraficaComune.NonInUso Is Null OR AnagraficaComune.NonInUso ='' Or AnagraficaComune.NonInUso = 'N')"
        End If

        If Condizione <> " (AnagraficaComune.NonInUso Is Null OR AnagraficaComune.NonInUso ='' Or AnagraficaComune.NonInUso = 'N')" Then
            Condizione = " And " & Condizione
            If Trim(Condizione) = "And" Then
                If Chk_OrdinePerCodice.Checked = False Then
                    cmd.CommandText = ("select AnagraficaComune.CodiceOspite as CodiceOSpite,AnagraficaComune.CodiceFiscale as CodiceFiscale,AnagraficaComune.DataNascita, MOVIMENTI.Data, MOVIMENTI.CentroServizio as CentroServizio, Nome,NonInUso  from AnagraficaComune INNER JOIN MOVIMENTI ON AnagraficaComune.CODICEOSPITE = MOVIMENTI.CODICEOSPITE where TIPOLOGIA = 'O'  AND MOVIMENTI.DATA = (SELECT MAX(DATA) FROM MOVIMENTI AS K WHERE K.CODICEOSPITE = AnagraficaComune.CODICEOSPITE AND K.CentroServizio  = MOVIMENTI.CentroServizio) AND MOVIMENTI.PROGRESSIVO = (SELECT MAX(PROGRESSIVO) FROM MOVIMENTI AS K WHERE K.CODICEOSPITE = AnagraficaComune.CODICEOSPITE AND K.CentroServizio  = MOVIMENTI.CentroServizio And K.DATA = (SELECT MAX(DATA) FROM MOVIMENTI AS K1 WHERE K1.CODICEOSPITE = AnagraficaComune.CODICEOSPITE AND K1.CentroServizio  = MOVIMENTI.CentroServizio) ) ORDER BY NOME,Data ,CENTROSERVIZIO")
                Else
                    cmd.CommandText = ("select AnagraficaComune.CodiceOspite as CodiceOSpite,AnagraficaComune.CodiceFiscale as CodiceFiscale,AnagraficaComune.DataNascita, MOVIMENTI.Data, MOVIMENTI.CentroServizio as CentroServizio, Nome,NonInUso  from AnagraficaComune INNER JOIN MOVIMENTI ON AnagraficaComune.CODICEOSPITE = MOVIMENTI.CODICEOSPITE where TIPOLOGIA = 'O'  AND MOVIMENTI.DATA = (SELECT MAX(DATA) FROM MOVIMENTI AS K WHERE K.CODICEOSPITE = AnagraficaComune.CODICEOSPITE AND K.CentroServizio  = MOVIMENTI.CentroServizio) AND MOVIMENTI.PROGRESSIVO = (SELECT MAX(PROGRESSIVO) FROM MOVIMENTI AS K WHERE K.CODICEOSPITE = AnagraficaComune.CODICEOSPITE AND K.CentroServizio  = MOVIMENTI.CentroServizio And K.DATA = (SELECT MAX(DATA) FROM MOVIMENTI AS K1 WHERE K1.CODICEOSPITE = AnagraficaComune.CODICEOSPITE AND K1.CentroServizio  = MOVIMENTI.CentroServizio) )  ORDER BY CodiceOspite Desc")
                End If
            Else
                If Chk_OrdinePerCodice.Checked = False Then
                    cmd.CommandText = ("select AnagraficaComune.CodiceOspite as CodiceOSpite,AnagraficaComune.CodiceFiscale as CodiceFiscale,AnagraficaComune.DataNascita, MOVIMENTI.Data, MOVIMENTI.CentroServizio as CentroServizio, Nome,NonInUso  from AnagraficaComune INNER JOIN MOVIMENTI ON AnagraficaComune.CODICEOSPITE = MOVIMENTI.CODICEOSPITE where TIPOLOGIA = 'O'  AND MOVIMENTI.DATA = (SELECT MAX(DATA) FROM MOVIMENTI AS K WHERE K.CODICEOSPITE = AnagraficaComune.CODICEOSPITE AND K.CentroServizio  = MOVIMENTI.CentroServizio) AND MOVIMENTI.PROGRESSIVO = (SELECT MAX(PROGRESSIVO) FROM MOVIMENTI AS K WHERE K.CODICEOSPITE = AnagraficaComune.CODICEOSPITE AND K.CentroServizio  = MOVIMENTI.CentroServizio And K.DATA = (SELECT MAX(DATA) FROM MOVIMENTI AS K1 WHERE K1.CODICEOSPITE = AnagraficaComune.CODICEOSPITE AND K1.CentroServizio  = MOVIMENTI.CentroServizio) )  " & Condizione & "  ORDER BY NOME,Data ,CENTROSERVIZIO")
                Else
                    cmd.CommandText = ("select AnagraficaComune.CodiceOspite as CodiceOSpite,AnagraficaComune.CodiceFiscale as CodiceFiscale,AnagraficaComune.DataNascita, MOVIMENTI.Data, MOVIMENTI.CentroServizio as CentroServizio, Nome,NonInUso  from AnagraficaComune INNER JOIN MOVIMENTI ON AnagraficaComune.CODICEOSPITE = MOVIMENTI.CODICEOSPITE where TIPOLOGIA = 'O'  AND MOVIMENTI.DATA = (SELECT MAX(DATA) FROM MOVIMENTI AS K WHERE K.CODICEOSPITE = AnagraficaComune.CODICEOSPITE AND K.CentroServizio  = MOVIMENTI.CentroServizio) AND MOVIMENTI.PROGRESSIVO = (SELECT MAX(PROGRESSIVO) FROM MOVIMENTI AS K WHERE K.CODICEOSPITE = AnagraficaComune.CODICEOSPITE AND K.CentroServizio  = MOVIMENTI.CentroServizio And K.DATA = (SELECT MAX(DATA) FROM MOVIMENTI AS K1 WHERE K1.CODICEOSPITE = AnagraficaComune.CODICEOSPITE AND K1.CentroServizio  = MOVIMENTI.CentroServizio) )  " & Condizione & "  ORDER BY CodiceOspite Desc")
                End If
            End If
        Else
            If Chk_OrdinePerCodice.Checked = False Then
                cmd.CommandText = ("select top 20 AnagraficaComune.CodiceOspite as CodiceOSpite,AnagraficaComune.CodiceFiscale as CodiceFiscale,AnagraficaComune.DataNascita, MOVIMENTI.Data, MOVIMENTI.CentroServizio as CentroServizio, Nome,NonInUso  from AnagraficaComune INNER JOIN MOVIMENTI ON AnagraficaComune.CODICEOSPITE = MOVIMENTI.CODICEOSPITE where TIPOLOGIA = 'O'  AND MOVIMENTI.DATA = (SELECT MAX(DATA) FROM MOVIMENTI AS K WHERE K.CODICEOSPITE = AnagraficaComune.CODICEOSPITE AND K.CentroServizio  = MOVIMENTI.CentroServizio) AND MOVIMENTI.PROGRESSIVO = (SELECT MAX(PROGRESSIVO) FROM MOVIMENTI AS K WHERE K.CODICEOSPITE = AnagraficaComune.CODICEOSPITE AND K.CentroServizio  = MOVIMENTI.CentroServizio And K.DATA = (SELECT MAX(DATA) FROM MOVIMENTI AS K1 WHERE K1.CODICEOSPITE = AnagraficaComune.CODICEOSPITE AND K1.CentroServizio  = MOVIMENTI.CentroServizio) ) AND " & Condizione & "  ORDER BY NOME,Data ,CENTROSERVIZIO")
            Else
                cmd.CommandText = ("select top 20 AnagraficaComune.CodiceOspite as CodiceOSpite,AnagraficaComune.CodiceFiscale as CodiceFiscale,AnagraficaComune.DataNascita, MOVIMENTI.Data, MOVIMENTI.CentroServizio as CentroServizio, Nome,NonInUso  from AnagraficaComune INNER JOIN MOVIMENTI ON AnagraficaComune.CODICEOSPITE = MOVIMENTI.CODICEOSPITE where TIPOLOGIA = 'O'  AND MOVIMENTI.DATA = (SELECT MAX(DATA) FROM MOVIMENTI AS K WHERE K.CODICEOSPITE = AnagraficaComune.CODICEOSPITE AND K.CentroServizio  = MOVIMENTI.CentroServizio) AND MOVIMENTI.PROGRESSIVO = (SELECT MAX(PROGRESSIVO) FROM MOVIMENTI AS K WHERE K.CODICEOSPITE = AnagraficaComune.CODICEOSPITE AND K.CentroServizio  = MOVIMENTI.CentroServizio And K.DATA = (SELECT MAX(DATA) FROM MOVIMENTI AS K1 WHERE K1.CODICEOSPITE = AnagraficaComune.CODICEOSPITE AND K1.CentroServizio  = MOVIMENTI.CentroServizio) ) AND " & Condizione & "  ORDER BY CodiceOspite Desc")
            End If
        End If

        cmd.Connection = cn

        MyTable.Clear()
        MyTable.Columns.Add("Codice Ospite", GetType(String))
        MyTable.Columns.Add("Codice Servizio", GetType(String))
        MyTable.Columns.Add("Centro Servizio", GetType(String))
        MyTable.Columns.Add("Cognome Nome", GetType(String))
        MyTable.Columns.Add("Data Nascita", GetType(String))
        MyTable.Columns.Add("Data Accoglimento", GetType(String))
        MyTable.Columns.Add("Data Uscita Definitiva", GetType(String))

        Dim CsTp As New Cls_CentroServizio
        CsTp.CENTROSERVIZIO = ""
        Dim NumeroRiga As Integer = 0
        Dim myPOSTreader As OleDbDataReader = cmd.ExecuteReader()
        Do While myPOSTreader.Read

            NumeroRiga = NumeroRiga + 1

            Dim myriga As System.Data.DataRow = MyTable.NewRow()



            If CsTp.CENTROSERVIZIO = campodb(myPOSTreader.Item("CentroServizio")) Then
                myriga(1) = CsTp.CENTROSERVIZIO
                myriga(2) = CsTp.DESCRIZIONE
            Else
                CsTp.CENTROSERVIZIO = campodb(myPOSTreader.Item("CentroServizio"))
                CsTp.Leggi(Session("DC_OSPITE"), CsTp.CENTROSERVIZIO)

                myriga(1) = CsTp.CENTROSERVIZIO
                myriga(2) = CsTp.DESCRIZIONE
            End If


            myriga(0) = myPOSTreader.Item("CodiceOspite")


            If campodb(myPOSTreader.Item("NonInUso")) = "S" Then
                myriga(3) = myPOSTreader.Item("Nome") & "*"
            Else
                myriga(3) = myPOSTreader.Item("Nome")
            End If

            If Not IsDBNull(myPOSTreader.Item("DataNascita")) Then
                myriga(4) = Format(myPOSTreader.Item("DataNascita"), "dd/MM/yyyy")
            End If
            Dim XS As New Cls_Movimenti
            Dim DataAccoglimento As Date = Nothing

            XS.CENTROSERVIZIO = myPOSTreader.Item("CentroServizio")
            XS.CodiceOspite = myPOSTreader.Item("CodiceOspite")
            XS.UltimaDataAccoglimento(StringaConnessione)

            If Not IsDBNull(XS.Data) Then
                myriga(5) = Format(XS.Data, "dd/MM/yyyy")
                DataAccoglimento = XS.Data
            End If

            XS.Data = Nothing
            XS.CENTROSERVIZIO = myPOSTreader.Item("CentroServizio")
            XS.CodiceOspite = myPOSTreader.Item("CodiceOspite")

            XS.UltimaDataUscitaDefinitiva(StringaConnessione)

            If Not IsDBNull(XS.Data) Then
                If Year(XS.Data) > 1900 Then
                    If Format(DataAccoglimento, "yyyyMMdd") <= Format(XS.Data, "yyyyMMdd") Then
                        myriga(6) = Format(XS.Data, "dd/MM/yyyy")
                    End If
                End If
            End If
            MyTable.Rows.Add(myriga)

        Loop
        myPOSTreader.Close()
        cn.Close()

        Session("UltimoNumero") = NumeroRiga + 1

        ViewState("Appoggio") = MyTable

        Grd_ImportoOspite.AutoGenerateColumns = True
        Grd_ImportoOspite.DataSource = MyTable
        Grd_ImportoOspite.DataBind()

        Dim Param As New Cls_Parametri

        Param.LeggiParametri(Session("DC_OSPITE"))


        For i = 0 To Grd_ImportoOspite.Rows.Count - 1
            Dim K As New ClsOspite
            K.CodiceOspite = Grd_ImportoOspite.Rows(i).Cells(1).Text
            K.Leggi(Session("DC_OSPITE"), K.CodiceOspite)


            Grd_ImportoOspite.Rows(i).Cells(4).Text = Grd_ImportoOspite.Rows(i).Cells(4).Text & "<br><font size=""2"" color=""#3572b0"">" & K.CODICEFISCALE & "</font> <font size=""2"" >Cartella</font> <font size=""2""  color=""#3572b0"">" & K.CartellaClinica & "</font>"
            Grd_ImportoOspite.Rows(i).Cells(5).Text = Grd_ImportoOspite.Rows(i).Cells(5).Text & "<br><font size=""2"" color=""#3572b0"">" & DateDiff(DateInterval.Year, K.DataNascita, Now) & " anni</font>"
        Next
    End Sub




    Private Sub DisabilitaCache()

        Response.Cache.SetCacheability(HttpCacheability.NoCache)

        Response.Headers.Add("Cache-Control", "no-cache, no-store")


        Response.Cache.SetExpires(DateTime.UtcNow.AddYears(-1))
    End Sub


    Protected Sub ImageButton1_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles ImageButton1.Click
        Call Ricerca()
    End Sub

    Protected Sub Btn_Nuovo_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles Btn_Nuovo.Click
        Response.Redirect("NuovoOspite.aspx")
    End Sub

    Function campodb(ByVal oggetto As Object) As String
        If IsDBNull(oggetto) Then
            Return ""
        Else
            Return oggetto
        End If
    End Function

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Trim(Session("UTENTE")) = "" Then            
            Response.Redirect("..\Login.aspx")
            Exit Sub
        End If

        If IsNothing(Session("DC_OSPITIACCESSORI")) Then
            Response.Redirect("..\Login.aspx")
            Exit Sub
        End If
        Dim Appoggio As String = Session("ABILITAZIONI")


        Dim Barra As New Cls_BarraSenior


        Lbl_BarraSenior.Text = Barra.CodiceBarra(Application("SENIOR"), Session("UTENTE"), Page)

        If Page.IsPostBack = True Then Exit Sub

        'DisabilitaCache()

        Try
            If Request.UrlReferrer.ToString.ToUpper.IndexOf("Menu_Ospiti.aspx".ToUpper) > 0 Then
                Session("CODICEOSPITE") = 0
                Session("CODICESERVIZIO") = ""
            End If
        Catch ex As Exception
            Session("CODICEOSPITE") = 0
            Session("CODICESERVIZIO") = ""
        End Try


        Dim kVilla As New Cls_TabelleDescrittiveOspitiAccessori


        kVilla.UpDateDropBox(Session("DC_OSPITIACCESSORI"), "VIL", DD_Struttura)
        If DD_Struttura.Items.Count = 1 Then
            Call AggiornaCServ()
        End If

        If Appoggio.IndexOf("<STR=") > 0 Then
            Dim CentroServizioAbilitato As String
            Dim Posizione As Integer = Appoggio.IndexOf("<STR=") + 5

            CentroServizioAbilitato = Appoggio.Substring(Posizione, Appoggio.IndexOf(">", Posizione) - Posizione)

            DD_Struttura.SelectedValue = CentroServizioAbilitato
            DD_Struttura.Enabled = False
            Call AggiornaCServ()
        End If


        If Appoggio.IndexOf("<CSERV=") > 0 Then
            Dim CentroServizioAbilitato As String
            Dim Posizione As Integer = Appoggio.IndexOf("<CSERV=") + 7

            CentroServizioAbilitato = Appoggio.Substring(Posizione, Appoggio.IndexOf(">", Posizione) - Posizione)

            DD_CServ.SelectedValue = CentroServizioAbilitato
            DD_CServ.Enabled = False
        End If


        Btn_Nuovo.Visible = True


        If Request.Item("TIPO") = "FATTURANC" Then
            Btn_Nuovo.Visible = False

        End If

        If Request.Item("TIPO") = "INCASSIANTICIPI" Then
            Btn_Nuovo.Visible = False
        End If

        If Request.Item("TIPO") = "MOVIMENTI" Then
            Btn_Nuovo.Visible = False
        End If

        If Request.Item("TIPO") = "ADDACR" Then
            Btn_Nuovo.Visible = False
        End If

        If Request.Item("TIPO") = "DIURNO" Then
            Btn_Nuovo.Visible = False
        End If

        If Request.Item("TIPO") = "GESTIONEDENARO" Then
            Btn_Nuovo.Visible = False
        End If

        If Request.Item("TIPO") = "DOCOSPPAR" Then
            Btn_Nuovo.Visible = False
        End If


        If Request.Item("TIPO") = "EstrattoContoDenaro" Then
            Btn_Nuovo.Visible = False
        End If



        If Not IsNothing(Session("ABILITAZIONI")) Then
            If Session("ABILITAZIONI").IndexOf("<EPERSONAM>") >= 0 Then
                Btn_Nuovo.Visible = False
            End If
            If Session("ABILITAZIONI").IndexOf("<INSERIMENTOON>") >= 0 Then
                Btn_Nuovo.Visible = True
            End If
        End If



        If Request.Item("CHIAMATA") <> "RITORNO" Then
            Call Ricerca()
            Session("RicercaAnagraficaSQLString") = Nothing
        Else

            If Not IsNothing(Session("RicercaAnagraficaSQLString")) Then
                Dim k As New Cls_SqlString
                REM Dim Appoggio As System.Web.UI.ImageClickEventArgs

                k = Session("RicercaAnagraficaSQLString")

                Txt_CodiceOspite.Text = k.GetValue("Txt_ID")
                Txt_CognomeNome.Text = k.GetValue("Txt_Descrizione")
                DD_CServ.SelectedValue = k.GetValue("DD_CServ")
                DD_Struttura.SelectedValue = k.GetValue("DD_Struttura")
                If k.GetValue("SoloNonInUSO") = "T" Then
                    Chk_NonInUso.Checked = True
                Else
                    Chk_NonInUso.Checked = False
                End If

                If k.GetValue("OrdinePerCodice") = "T" Then
                    Chk_OrdinePerCodice.Checked = True
                Else
                    Chk_OrdinePerCodice.Checked = False
                End If


                Call Ricerca()


            Else
                Call Ricerca()
            End If


            
            If Val(Request.Item("PAGINA")) > 0 Then
                If Val(Request.Item("PAGINA")) < Grd_ImportoOspite.PageCount Then
                    Grd_ImportoOspite.PageIndex = Val(Request.Item("PAGINA"))
                End If
            End If

            MyTable = ViewState("Appoggio")

            Grd_ImportoOspite.AutoGenerateColumns = True
            Grd_ImportoOspite.DataSource = MyTable
            Grd_ImportoOspite.DataBind()

            For i = 0 To Grd_ImportoOspite.Rows.Count - 1
                Dim K As New ClsOspite
                K.CodiceOspite = Grd_ImportoOspite.Rows(i).Cells(1).Text
                K.Leggi(Session("DC_OSPITE"), K.CodiceOspite)

                Grd_ImportoOspite.Rows(i).Cells(4).Text = Grd_ImportoOspite.Rows(i).Cells(4).Text & "<br><font size=""2"" color=""#3572b0"">" & K.CODICEFISCALE & "</font> <font size=""2"" >Cartella</font> <font size=""2""  color=""#3572b0"">" & K.CartellaClinica & "</font>"
                Grd_ImportoOspite.Rows(i).Cells(5).Text = Grd_ImportoOspite.Rows(i).Cells(5).Text & "<br><font size=""2"" color=""#3572b0"">" & DateDiff(DateInterval.Year, K.DataNascita, Now) & " anni</font>"
            Next
        End If

        Dim Log As New Cls_LogPrivacy

        Log.LogPrivacy(Application("SENIOR"), Session("CLIENTE"), Session("UTENTE"), Session("UTENTEIMPER"), Page.GetType.Name.ToUpper, 0, 0, DD_Struttura.SelectedValue, DD_CServ.SelectedValue, 0, "ELENCO ANAGRAFICHE", "", "", "")

    End Sub

  
  

    Protected Sub Btn_Esci_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles Btn_Esci.Click
        If Trim(Session("UTENTE")) = "" Then
            Response.Redirect("..\Login.aspx")
            Exit Sub
        End If

        If Request.Item("TIPO") = "DOCOSPPAR" Or Request.Item("TIPO") = "EstrattoContoDenaro" Then
            Response.Redirect("Menu_Visualizzazioni.aspx")
        End If

        If Request.Item("TIPO") = "MODIFICARETTA" Or Request.Item("TIPO") = "MODIFICACSERV" Then
            Response.Redirect("Menu_Strumenti.aspx")
        End If


        If Session("ABILITAZIONI").IndexOf("<PORTINERIA>") > 0 Then
            Response.Redirect("..\MainPortineria.aspx")
        Else
            Response.Redirect("Menu_Ospiti.aspx")
        End If
    End Sub

    Protected Sub Grd_ImportoOspite_RowDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs) Handles Grd_ImportoOspite.RowDataBound
        If (e.Row.RowType = DataControlRowType.DataRow) Then
            e.Row.Cells(1).Width = Unit.Pixel(80)
            e.Row.Cells(2).Width = Unit.Pixel(80)
            e.Row.Cells(3).Width = Unit.Pixel(250)
            e.Row.Cells(5).Width = Unit.Pixel(100)
            e.Row.Cells(6).Width = Unit.Pixel(100)
            e.Row.Cells(7).Width = Unit.Pixel(100)

        End If
    End Sub

    Protected Sub Grd_ImportoOspite_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles Grd_ImportoOspite.SelectedIndexChanged


    End Sub

    Protected Sub Grd_ImportoOspite_SelectedIndexChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewSelectEventArgs) Handles Grd_ImportoOspite.SelectedIndexChanging

    End Sub
    Private Sub AggiornaCServ()
        Dim kCsrv As New Cls_CentroServizio

        If DD_Struttura.SelectedValue = "" Then
            kCsrv.UpDateDropBox(Session("DC_OSPITE"), DD_CServ)
        Else
            kCsrv.UpDateDropBoxStruttura(Session("DC_OSPITE"), DD_CServ, DD_Struttura.SelectedValue)
        End If
    End Sub

    Protected Sub DD_Struttura_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles DD_Struttura.SelectedIndexChanged

    End Sub
    Protected Sub DD_Struttura_TextChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles DD_Struttura.TextChanged

        AggiornaCServ()
    End Sub

    Protected Sub Img_Espandi_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles Img_Espandi.Click

        Img_Espandi.Enabled = False

        MyTable = ViewState("Appoggio")

        Dim f As New ClsOspite

        Dim StringaConnessione As String = Session("DC_OSPITE")

        Dim cn As OleDbConnection
        Dim Riga As Long = 0

        cn = New Data.OleDb.OleDbConnection(StringaConnessione)

        cn.Open()
        Dim cmd As New OleDbCommand()

        Dim k1 As New Cls_SqlString
        Dim Condizione As String = ""

        k1.Add("Txt_ID", Txt_CodiceOspite.Text)
        k1.Add("Txt_Descrizione", Txt_CognomeNome.Text)
        k1.Add("DD_CServ", DD_CServ.SelectedValue)
        k1.Add("DD_Struttura", DD_Struttura.SelectedValue)
        If Chk_NonInUso.Checked = True Then
            k1.Add("SoloNonInUSO", "T")
        Else
            k1.Add("SoloNonInUSO", "F")
        End If
        If Chk_OrdinePerCodice.Checked = True Then
            k1.Add("OrdinePerCodice", "T")
        Else
            k1.Add("OrdinePerCodice", "F")
        End If

        Session("RicercaAnagraficaSQLString") = k1


        Condizione = ""
        If Val(Txt_CodiceOspite.Text) > 0 Then
            If Condizione <> "" Then
                Condizione = Condizione & " And "
            End If
            Condizione = Condizione & " AnagraficaComune.CodiceOspite = " & Val(Txt_CodiceOspite.Text)
        End If

        If DD_CServ.SelectedValue <> "" Then
            If Condizione <> "" Then
                Condizione = Condizione & " And "
            End If
            Condizione = Condizione & "  MOVIMENTI.CentroServizio = '" & DD_CServ.SelectedValue & "'"
        Else
            If DD_Struttura.SelectedValue <> "" Then
                If Condizione <> "" Then
                    Condizione = Condizione & " And ("
                Else
                    Condizione = Condizione & " ("
                End If
                Dim Indice As Integer
                Call AggiornaCServ()
                For Indice = 0 To DD_CServ.Items.Count - 1
                    If Indice >= 1 Then
                        If Condizione <> "" Then
                            Condizione = Condizione & " Or "
                        End If
                    End If
                    Condizione = Condizione & "  MOVIMENTI.CentroServizio = '" & DD_CServ.Items(Indice).Value & "'"
                Next
                Condizione = Condizione & ") "
            End If
        End If



        If Txt_CognomeNome.Text <> "" Then
            If Condizione <> "" Then
                Condizione = Condizione & " And "
            End If
            Condizione = Condizione & "  AnagraficaComune.CodiceOspite > 0 And Nome Like '" & Txt_CognomeNome.Text.Replace("'", "''") & "'"
        End If

        If Chk_NonInUso.Checked = False Then
            If Condizione <> "" Then
                Condizione = Condizione & " And "
            End If
            Condizione = Condizione & " (AnagraficaComune.NonInUso Is Null OR AnagraficaComune.NonInUso ='' Or AnagraficaComune.NonInUso = 'N')"
        End If

        If Condizione <> " (AnagraficaComune.NonInUso Is Null OR AnagraficaComune.NonInUso ='' Or AnagraficaComune.NonInUso = 'N')" Then
            Condizione = " And " & Condizione
            If Trim(Condizione) = "And" Then
                If Chk_OrdinePerCodice.Checked = False Then
                    cmd.CommandText = ("select AnagraficaComune.CodiceOspite as CodiceOSpite,AnagraficaComune.CodiceFiscale as CodiceFiscale,AnagraficaComune.DataNascita, MOVIMENTI.Data, MOVIMENTI.CentroServizio as CentroServizio, Nome,NonInUso  from AnagraficaComune INNER JOIN MOVIMENTI ON AnagraficaComune.CODICEOSPITE = MOVIMENTI.CODICEOSPITE where TIPOLOGIA = 'O'  AND MOVIMENTI.DATA = (SELECT MAX(DATA) FROM MOVIMENTI AS K WHERE K.CODICEOSPITE = AnagraficaComune.CODICEOSPITE AND K.CentroServizio  = MOVIMENTI.CentroServizio) AND MOVIMENTI.PROGRESSIVO = (SELECT MAX(PROGRESSIVO) FROM MOVIMENTI AS K WHERE K.CODICEOSPITE = AnagraficaComune.CODICEOSPITE AND K.CentroServizio  = MOVIMENTI.CentroServizio And K.DATA = (SELECT MAX(DATA) FROM MOVIMENTI AS K1 WHERE K1.CODICEOSPITE = AnagraficaComune.CODICEOSPITE AND K1.CentroServizio  = MOVIMENTI.CentroServizio) ) ORDER BY NOME,Data ,CENTROSERVIZIO")
                Else
                    cmd.CommandText = ("select AnagraficaComune.CodiceOspite as CodiceOSpite,AnagraficaComune.CodiceFiscale as CodiceFiscale,AnagraficaComune.DataNascita, MOVIMENTI.Data, MOVIMENTI.CentroServizio as CentroServizio, Nome,NonInUso  from AnagraficaComune INNER JOIN MOVIMENTI ON AnagraficaComune.CODICEOSPITE = MOVIMENTI.CODICEOSPITE where TIPOLOGIA = 'O'  AND MOVIMENTI.DATA = (SELECT MAX(DATA) FROM MOVIMENTI AS K WHERE K.CODICEOSPITE = AnagraficaComune.CODICEOSPITE AND K.CentroServizio  = MOVIMENTI.CentroServizio) AND MOVIMENTI.PROGRESSIVO = (SELECT MAX(PROGRESSIVO) FROM MOVIMENTI AS K WHERE K.CODICEOSPITE = AnagraficaComune.CODICEOSPITE AND K.CentroServizio  = MOVIMENTI.CentroServizio And K.DATA = (SELECT MAX(DATA) FROM MOVIMENTI AS K1 WHERE K1.CODICEOSPITE = AnagraficaComune.CODICEOSPITE AND K1.CentroServizio  = MOVIMENTI.CentroServizio) )  ORDER BY CodiceOspite Desc")
                End If
            Else
                If Chk_OrdinePerCodice.Checked = False Then
                    cmd.CommandText = ("select AnagraficaComune.CodiceOspite as CodiceOSpite,AnagraficaComune.CodiceFiscale as CodiceFiscale,AnagraficaComune.DataNascita, MOVIMENTI.Data, MOVIMENTI.CentroServizio as CentroServizio, Nome,NonInUso  from AnagraficaComune INNER JOIN MOVIMENTI ON AnagraficaComune.CODICEOSPITE = MOVIMENTI.CODICEOSPITE where TIPOLOGIA = 'O'  AND MOVIMENTI.DATA = (SELECT MAX(DATA) FROM MOVIMENTI AS K WHERE K.CODICEOSPITE = AnagraficaComune.CODICEOSPITE AND K.CentroServizio  = MOVIMENTI.CentroServizio) AND MOVIMENTI.PROGRESSIVO = (SELECT MAX(PROGRESSIVO) FROM MOVIMENTI AS K WHERE K.CODICEOSPITE = AnagraficaComune.CODICEOSPITE AND K.CentroServizio  = MOVIMENTI.CentroServizio And K.DATA = (SELECT MAX(DATA) FROM MOVIMENTI AS K1 WHERE K1.CODICEOSPITE = AnagraficaComune.CODICEOSPITE AND K1.CentroServizio  = MOVIMENTI.CentroServizio) )  " & Condizione & "  ORDER BY NOME,Data ,CENTROSERVIZIO")
                Else
                    cmd.CommandText = ("select AnagraficaComune.CodiceOspite as CodiceOSpite,AnagraficaComune.CodiceFiscale as CodiceFiscale,AnagraficaComune.DataNascita, MOVIMENTI.Data, MOVIMENTI.CentroServizio as CentroServizio, Nome,NonInUso  from AnagraficaComune INNER JOIN MOVIMENTI ON AnagraficaComune.CODICEOSPITE = MOVIMENTI.CODICEOSPITE where TIPOLOGIA = 'O'  AND MOVIMENTI.DATA = (SELECT MAX(DATA) FROM MOVIMENTI AS K WHERE K.CODICEOSPITE = AnagraficaComune.CODICEOSPITE AND K.CentroServizio  = MOVIMENTI.CentroServizio) AND MOVIMENTI.PROGRESSIVO = (SELECT MAX(PROGRESSIVO) FROM MOVIMENTI AS K WHERE K.CODICEOSPITE = AnagraficaComune.CODICEOSPITE AND K.CentroServizio  = MOVIMENTI.CentroServizio And K.DATA = (SELECT MAX(DATA) FROM MOVIMENTI AS K1 WHERE K1.CODICEOSPITE = AnagraficaComune.CODICEOSPITE AND K1.CentroServizio  = MOVIMENTI.CentroServizio) )  " & Condizione & "  ORDER BY CodiceOspite Desc")
                End If
            End If
        Else
            If Chk_OrdinePerCodice.Checked = False Then
                cmd.CommandText = ("select AnagraficaComune.CodiceOspite as CodiceOSpite,AnagraficaComune.CodiceFiscale as CodiceFiscale,AnagraficaComune.DataNascita, MOVIMENTI.Data, MOVIMENTI.CentroServizio as CentroServizio, Nome,NonInUso  from AnagraficaComune INNER JOIN MOVIMENTI ON AnagraficaComune.CODICEOSPITE = MOVIMENTI.CODICEOSPITE where TIPOLOGIA = 'O'  AND MOVIMENTI.DATA = (SELECT MAX(DATA) FROM MOVIMENTI AS K WHERE K.CODICEOSPITE = AnagraficaComune.CODICEOSPITE AND K.CentroServizio  = MOVIMENTI.CentroServizio) AND MOVIMENTI.PROGRESSIVO = (SELECT MAX(PROGRESSIVO) FROM MOVIMENTI AS K WHERE K.CODICEOSPITE = AnagraficaComune.CODICEOSPITE AND K.CentroServizio  = MOVIMENTI.CentroServizio And K.DATA = (SELECT MAX(DATA) FROM MOVIMENTI AS K1 WHERE K1.CODICEOSPITE = AnagraficaComune.CODICEOSPITE AND K1.CentroServizio  = MOVIMENTI.CentroServizio) ) AND " & Condizione & "  ORDER BY NOME,Data ,CENTROSERVIZIO")
            Else
                cmd.CommandText = ("select AnagraficaComune.CodiceOspite as CodiceOSpite,AnagraficaComune.CodiceFiscale as CodiceFiscale,AnagraficaComune.DataNascita, MOVIMENTI.Data, MOVIMENTI.CentroServizio as CentroServizio, Nome,NonInUso  from AnagraficaComune INNER JOIN MOVIMENTI ON AnagraficaComune.CODICEOSPITE = MOVIMENTI.CODICEOSPITE where TIPOLOGIA = 'O'  AND MOVIMENTI.DATA = (SELECT MAX(DATA) FROM MOVIMENTI AS K WHERE K.CODICEOSPITE = AnagraficaComune.CODICEOSPITE AND K.CentroServizio  = MOVIMENTI.CentroServizio) AND MOVIMENTI.PROGRESSIVO = (SELECT MAX(PROGRESSIVO) FROM MOVIMENTI AS K WHERE K.CODICEOSPITE = AnagraficaComune.CODICEOSPITE AND K.CentroServizio  = MOVIMENTI.CentroServizio And K.DATA = (SELECT MAX(DATA) FROM MOVIMENTI AS K1 WHERE K1.CODICEOSPITE = AnagraficaComune.CODICEOSPITE AND K1.CentroServizio  = MOVIMENTI.CentroServizio) ) AND " & Condizione & "  ORDER BY CodiceOspite Desc")
            End If
        End If

        cmd.Connection = cn

        Dim PrimoInsert As Boolean = True
        Dim IndiceRiga As Integer = 0

        Dim myPOSTreader As OleDbDataReader = cmd.ExecuteReader()
        Do While myPOSTreader.Read

            IndiceRiga = IndiceRiga + 1

            If IndiceRiga > 5 + Session("UltimoNumero") Then
                Exit Do
            End If
            If IndiceRiga >= Session("UltimoNumero") Then
                Dim CsTp As New Cls_CentroServizio
                Dim Salta As Boolean = False

                CsTp.CENTROSERVIZIO = myPOSTreader.Item("CentroServizio")
                CsTp.Leggi(Session("DC_OSPITE"), CsTp.CENTROSERVIZIO)

                If PrimoInsert Then
                    Try
                        If MyTable.Rows(MyTable.Rows.Count - 1).Item(1) = CsTp.CENTROSERVIZIO And _
                           MyTable.Rows(MyTable.Rows.Count - 1).Item(0) = myPOSTreader.Item("CodiceOspite") Then
                            Salta = True
                        End If
                    Catch ex As Exception

                    End Try
                End If

                PrimoInsert = True
                If Not Salta Then
                    PrimoInsert = True
                    Dim myriga As System.Data.DataRow = MyTable.NewRow()


                    'myriga(0) = myPOSTreader.Item("CentroServizio")
                    myriga(1) = CsTp.CENTROSERVIZIO
                    myriga(2) = CsTp.DESCRIZIONE
                    myriga(0) = myPOSTreader.Item("CodiceOspite")


                    If campodb(myPOSTreader.Item("NonInUso")) = "S" Then
                        myriga(3) = myPOSTreader.Item("Nome") & "*"
                    Else
                        myriga(3) = myPOSTreader.Item("Nome")
                    End If

                    If Not IsDBNull(myPOSTreader.Item("DataNascita")) Then
                        myriga(4) = Format(myPOSTreader.Item("DataNascita"), "dd/MM/yyyy")
                    End If
                    Dim XS As New Cls_Movimenti
                    Dim DataAccoglimento As Date = Nothing

                    XS.CENTROSERVIZIO = myPOSTreader.Item("CentroServizio")
                    XS.CodiceOspite = myPOSTreader.Item("CodiceOspite")
                    XS.UltimaDataAccoglimento(StringaConnessione)

                    If Not IsDBNull(XS.Data) Then
                        myriga(5) = Format(XS.Data, "dd/MM/yyyy")
                        DataAccoglimento = XS.Data
                    End If

                    XS.Data = Nothing
                    XS.CENTROSERVIZIO = myPOSTreader.Item("CentroServizio")
                    XS.CodiceOspite = myPOSTreader.Item("CodiceOspite")

                    XS.UltimaDataUscitaDefinitiva(StringaConnessione)

                    If Not IsDBNull(XS.Data) Then
                        If Year(XS.Data) > 1900 Then
                            If Format(DataAccoglimento, "yyyyMMdd") <= Format(XS.Data, "yyyyMMdd") Then
                                myriga(6) = Format(XS.Data, "dd/MM/yyyy")
                            End If
                        End If
                    End If
                    MyTable.Rows.Add(myriga)
                End If
            End If
        Loop
        myPOSTreader.Close()
        cn.Close()
        Session("UltimoNumero") = IndiceRiga

        ViewState("Appoggio") = MyTable

        Grd_ImportoOspite.AutoGenerateColumns = True

        Grd_ImportoOspite.DataSource = MyTable

        Grd_ImportoOspite.DataBind()

        For i = 0 To Grd_ImportoOspite.Rows.Count - 1
            Dim K As New ClsOspite
            K.CodiceOspite = Grd_ImportoOspite.Rows(i).Cells(1).Text
            K.Leggi(Session("DC_OSPITE"), K.CodiceOspite)

            Grd_ImportoOspite.Rows(i).Cells(4).Text = Grd_ImportoOspite.Rows(i).Cells(4).Text & "<br><font size=""2"" color=""#3572b0"">" & K.CODICEFISCALE & "</font> <font size=""2"" >Cartella</font> <font size=""2""  color=""#3572b0"">" & K.CartellaClinica & "</font>"
            Grd_ImportoOspite.Rows(i).Cells(5).Text = Grd_ImportoOspite.Rows(i).Cells(5).Text & "<br><font size=""2"" color=""#3572b0"">" & DateDiff(DateInterval.Year, K.DataNascita, Now) & " anni</font>"
        Next

        Img_Espandi.Enabled = True
    End Sub
End Class
