﻿<%@ Page Language="VB" AutoEventWireup="false" Inherits="OspitiWeb_ImportAddebitiDaFileExcel" CodeFile="ImportAddebitiDaFileExcel.aspx.vb" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="sau" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
    <meta http-equiv="x-ua-compatible" content="IE=9" />
    <title>Import Addebiti Accreidit</title>
    <asp:PlaceHolder runat="server">
        <%: System.Web.Optimization.Styles.Render("~/Content/AjaxControlToolkit/Styles/Bundle") %>
    </asp:PlaceHolder>
    <link href="ospiti.css?ver=11" rel="stylesheet" type="text/css" />
    <link rel="shortcut icon" href="images/SENIOR.ico" />
    <link href="js/jquery.autocomplete.css" rel="stylesheet" type="text/css" />


    <script src="js/jquery-1.5.1.min.js" type="text/javascript"></script>
    <script src="js/jquery.autocomplete.js" type="text/javascript"></script>
    <script src="js/soapclient.js" type="text/javascript"></script>
    <script src="/js/convertinumero.js" type="text/javascript"></script>



    <script src="js/jquery.maskedinput-1.3.min.js" type="text/javascript"></script>
    <script src="/js/formatnumer.js" type="text/javascript"></script>
    <script src="js/JSErrore.js" type="text/javascript"></script>

    <link rel="stylesheet" href="dialogbox/redips-dialog.css" type="text/css" media="screen" />
    <script type="text/javascript" src="dialogbox/redips-dialog-min.js"></script>
    <script type="text/javascript" src="dialogbox/script.js"></script>

    <link rel="stylesheet" href="jqueryui/jquery-ui.css" type="text/css" />
    <script src="jqueryui/jquery-ui.js" type="text/javascript"></script>
    <script src="jqueryui/datapicker-it.js" type="text/javascript"></script>
    <script type="text/javascript">
        $(document).ready(function () {
            if (window.innerHeight > 0) { $("#BarraLaterale").css("height", (window.innerHeight - 105) + "px"); } else { $("#BarraLaterale").css("height", (document.documentElement.offsetHeight - 105) + "px"); }
        });
    </script>
</head>
<body>
    <form id="form1" runat="server">
        <asp:ScriptManager ID="ScriptManager1" runat="server" EnableScriptGlobalization="true">
            <Scripts>
                <asp:ScriptReference Path="~/Scripts/AjaxControlToolkit/Bundle" />
            </Scripts>
        </asp:ScriptManager>
        <asp:Label ID="Lbl_BarraSenior" runat="server" Text=""></asp:Label>
        <div style="text-align: left;">
            <table style="width: 100%;" cellpadding="0" cellspacing="0">
                <tr>
                    <td style="width: 160px; background-color: #F0F0F0;"></td>
                    <td>
                        <div class="Titolo">Ospiti - Strumenti - Import da file Excel</div>
                        <div class="SottoTitolo">
                            <asp:Label ID="Lbl_NomeOspite" runat="server"></asp:Label><br />
                            <br />
                        </div>
                    </td>
                    <td style="text-align: right;">
                        <span class="BenvenutoText">Benvenuto
                            <asp:Label ID="Lbl_Utente" runat="server"></asp:Label></span>
                        <div class="DivTasti">
                            <asp:ImageButton ID="Btn_Modifica" runat="server" Height="38px" ImageUrl="~/images/salva.jpg" class="EffettoBottoniTondi" Width="38px" ToolTip="Modifica" />
                            <asp:ImageButton ID="ImageButton3" runat="server" Height="38px" ImageUrl="~/images/esegui.png" class="EffettoBottoniTondi" Width="38px" />
                            <asp:ImageButton ID="Img_CSV" runat="server" Height="38px" ImageUrl="~/images/download.png" class="EffettoBottoniTondi" Style="width: 38px" ToolTip="Scarica csv presenti" />
                        </div>
                    </td>
                </tr>
                <tr>
                    <td style="width: 160px; background-color: #F0F0F0; vertical-align: top; text-align: center;" id="BarraLaterale">
                        <a href="Menu_Ospiti.aspx" style="border-width: 0px;">
                            <img src="images/Home.jpg" class="Effetto" alt="Menù" /></a><br />
                        <asp:ImageButton ID="Btn_Esci" runat="server" BackColor="Transparent" ImageUrl="../images/Menu_Indietro.png" class="Effetto" ToolTip="Chiudi" />
                        <br />
                        <div id="MENUDIV"></div>
                        <br />
                        <br />
                        <br />
                        <br />
                        <br />
                        <br />
                        <br />
                        <br />
                        <br />
                        <br />
                        <br />
                        <br />
                    </td>
                    <td colspan="2" style="background-color: #FFFFFF; vertical-align: top;">
                        <sau:TabContainer ID="TabContainer1" runat="server" ActiveTabIndex="0" CssClass="TabSenior"
                            Width="100%" BorderStyle="None" Style="margin-right: 39px">
                            <sau:TabPanel runat="server" HeaderText="Prima Nota" ID="Tab_Anagrafica">
                                <HeaderTemplate>
                                    Import Addebiti Accrediti da file Excel                 
                                </HeaderTemplate>
                                <ContentTemplate>
                                    <label style="display: block; float: left; width: 160px;">File  :</label>
                                    <asp:FileUpload ID="FileUpload1" runat="server" />
                                    <br />
                                    <br />

                                    <label class="LabelCampo">Data Validita :</label>
                                    <asp:TextBox ID="Txt_DataRegistrazione" runat="server" Width="90px"></asp:TextBox><br />
                                    <br />

                                    <label class="LabelCampo">Anno :</label>
                                    <asp:TextBox ID="Txt_Anno" MaxLength="4" onkeypress="return soloNumeri(event);" runat="server" Width="64px"></asp:TextBox>
                                    Mese :
       <asp:DropDownList ID="Dd_Mese" runat="server" Width="128px">
           <asp:ListItem Value="1">Gennaio</asp:ListItem>
           <asp:ListItem Value="2">Febbraio</asp:ListItem>
           <asp:ListItem Value="3">Marzo</asp:ListItem>
           <asp:ListItem Value="4">Aprile</asp:ListItem>
           <asp:ListItem Value="5">Maggio</asp:ListItem>
           <asp:ListItem Value="6">Giugno</asp:ListItem>
           <asp:ListItem Value="7">Luglio</asp:ListItem>
           <asp:ListItem Value="8">Agosto</asp:ListItem>
           <asp:ListItem Value="9">Settembre</asp:ListItem>
           <asp:ListItem Value="10">Ottobre</asp:ListItem>
           <asp:ListItem Value="11">Novembre</asp:ListItem>
           <asp:ListItem Value="12">Dicembre</asp:ListItem>
       </asp:DropDownList>
                                    <asp:RadioButton ID="RB_FuoriRetta" runat="server" Text="Fuori Retta" GroupName="mTIPO" />
                                    <asp:RadioButton ID="RB_Retta" runat="server" Text="Retta" GroupName="mTIPO" />
                                    <asp:RadioButton ID="Rb_Rendiconto" runat="server" Text="Rendiconto" GroupName="mTIPO" /><br />
                                    <br />
                                    <label class="LabelCampo">Tipo Addebito :</label>
                                    <asp:DropDownList ID="dd_TipoAddebito" runat="server" Width="350px"></asp:DropDownList>
                                    <asp:CheckBox ID="Chk_Ragruppa" runat="server" Text="Raggruppa in Emissione" />
                                    <br />
                                    <br />
                                    <asp:GridView ID="Grd_ImportoOspite" runat="server" CellPadding="3"
                                        Width="100%" BackColor="White" BorderColor="#999999" BorderStyle="None"
                                        BorderWidth="1px" GridLines="Vertical" AllowPaging="False"
                                        ShowFooter="True" PageSize="200">
                                        <RowStyle ForeColor="#565151" BackColor="#EEEEEE" />
                                        <AlternatingRowStyle BackColor="White" />
                                        <Columns>
                                            <asp:TemplateField ItemStyle-Width="40px" HeaderText="Selezionato">
                                                <ItemTemplate>
                                                    <asp:CheckBox ID="Chk_Selezionato" runat="server" Text="" />
                                                </ItemTemplate>
                                            </asp:TemplateField>

                                            <asp:TemplateField HeaderText="Centro Servizio">
                                                <ItemTemplate>
                                                    <asp:DropDownList ID="DD_Cserv" runat="server"></asp:DropDownList>
                                                </ItemTemplate>
                                                <HeaderStyle Font-Bold="False" Font-Italic="False" />
                                                <ItemStyle Width="30px" />
                                            </asp:TemplateField>

                                            <asp:BoundField HeaderText="CodiceOspite" ItemStyle-Width="10%" DataField="CodiceOspite" />
                                            <asp:BoundField HeaderText="Codice Fiscale" ItemStyle-Width="20%" DataField="CodiceFiscale" />
                                            <asp:BoundField HeaderText="Cognome Nome" ItemStyle-Width="20%" DataField="CognomeNome" />
                                            <asp:BoundField HeaderText="Codice Tipo Addebito" ItemStyle-Width="10%" DataField="TipoAddebito" />
                                            <asp:BoundField HeaderText="Tipo Addebito" ItemStyle-Width="20%" DataField="Decodifica" />
                                            <asp:BoundField HeaderText="Importo" ItemStyle-Width="10%" DataField="Importo" />
                                            <asp:BoundField HeaderText="Ripartizione" ItemStyle-Width="10%" DataField="Ripartizione" />

                                        </Columns>
                                        <FooterStyle BackColor="#CCCCCC" ForeColor="Black" />
                                        <PagerStyle BackColor="#999999" ForeColor="Black" HorizontalAlign="Center" />
                                        <SelectedRowStyle BackColor="#008A8C" Font-Bold="True" ForeColor="White" />
                                        <HeaderStyle BackColor="#565151" Font-Bold="false" Font-Size="Small" ForeColor="White" />
                                        <AlternatingRowStyle BackColor="#DCDCDC" />
                                    </asp:GridView>
                                    <br />
                                    <br />
                                    <asp:TextBox ID="Txt_NonTrovati" runat="server" TextMode="MultiLine" Text=""></asp:TextBox>
                                </ContentTemplate>
                            </sau:TabPanel>
                        </sau:TabContainer>
                    </td>
                </tr>
            </table>
        </div>
    </form>
</body>
</html>
