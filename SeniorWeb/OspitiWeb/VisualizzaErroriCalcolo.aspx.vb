﻿Imports System.Web.Hosting
Imports System.Diagnostics
Imports System.ComponentModel

Partial Class VisualizzaErroriCalcolo
    Inherits System.Web.UI.Page

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Dim ConnectionString As String = Session("DC_OSPITE")

        Dim f As New Cls_Parametri

        f.LeggiParametri(ConnectionString)
        If f.PercEsec >= 100 Then
            Label1.Text = "Segnalazione Errori : <br />" & f.Errori
        End If
    End Sub
End Class
