﻿Imports System.Web.Hosting
Imports System.Data.OleDb
Partial Class OspitiWeb_StatistiDomiciliareSchema
    Inherits System.Web.UI.Page
    Dim MyTable As New System.Data.DataTable("tabella")
    Dim MyDataSet As New System.Data.DataSet()


    Protected Sub OspitiWeb_StatisticaDomiciliari_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load


        If Trim(Session("UTENTE")) = "" Then
            Response.Redirect("../Login.aspx")
            Exit Sub
        End If


        Call EseguiJS()



        If Page.IsPostBack = True Then Exit Sub


        Dim K1 As New Cls_SqlString

        Dim Barra As New Cls_BarraSenior

        If Not IsNothing(Session("RicercaAnagraficaSQLString")) Then
            K1 = Session("RicercaAnagraficaSQLString")
        End If

        Lbl_BarraSenior.Text = Barra.CodiceBarra(Application("SENIOR"), Session("UTENTE"), Page, K1)



        Dim kVilla As New Cls_TabelleDescrittiveOspitiAccessori


        kVilla.UpDateDropBox(Session("DC_OSPITIACCESSORI"), "VIL", DD_Struttura)
        If DD_Struttura.Items.Count = 1 Then
            Call AggiornaCServ()
        End If


        Lbl_Utente.Text = Session("UTENTE")




        Lbl_Utente.Text = Session("UTENTE")

        Call EseguiJS()


        Dim f As New Cls_Parametri

        f.LeggiParametri(Session("DC_OSPITE"))
        Dd_Mese.SelectedValue = f.MeseFatturazione
        Txt_Anno.Text = f.AnnoFatturazione


        Lbl_Utente.Text = Session("UTENTE")

        Txt_Dal.Text = "1"
        Txt_Al.Text = GiorniMese(Dd_Mese.SelectedValue, Val(Txt_Anno.Text))
    End Sub


    Private Sub AggiornaCServ()
        Dim kCsrv As New Cls_CentroServizio

        If DD_Struttura.SelectedValue = "" Then
            kCsrv.UpDateDropBox(Session("DC_OSPITE"), Cmb_CServ)
        Else
            kCsrv.UpDateDropBoxStruttura(Session("DC_OSPITE"), Cmb_CServ, DD_Struttura.SelectedValue)
        End If

    End Sub

    Function campodb(ByVal oggetto As Object) As String
        If IsDBNull(oggetto) Then
            Return ""
        Else
            Return oggetto
        End If
    End Function

    Public Function GiorniMese(ByVal Mese As Byte, ByVal Anno As Integer) As Byte
        If Mese = 1 Or Mese = 3 Or Mese = 5 Or Mese = 7 Or Mese = 8 Or _
           Mese = 10 Or Mese = 12 Then
            GiorniMese = 31
        Else
            If Mese <> 2 Then
                GiorniMese = 30
            Else
                If Day(DateSerial(Anno, Mese, 29)) = 29 Then
                    GiorniMese = 29
                Else
                    GiorniMese = 28
                End If
            End If
        End If
    End Function
    Function campodbd(ByVal oggetto As Object) As Date
        If IsDBNull(oggetto) Then
            Return Nothing
        Else
            Return oggetto
        End If
    End Function

    Private Function LeggiCella(ByVal CSERV As String, ByVal CODOSP As String, ByVal MESE As String, ByVal GIORNO As String, ByVal ANNO As String, ByVal Verifica As Boolean) As String

        Dim Appoggio As String
        Dim NumerIngressi As Integer = 0

        If Verifica = False Then
            Appoggio = "<p  style=""font-size: small"">"
        End If

        Dim cn As OleDbConnection



        cn = New Data.OleDb.OleDbConnection(Session("DC_OSPITE"))

        cn.Open()

        Dim KCausale As New Cls_CausaliEntrataUscita
        Dim MovimentiTipo As String = ""
        Dim cmd As New OleDbCommand()
        cmd.CommandText = ("select * from MovimentiDomiciliare where CodiceOspite = " & CODOSP & " And CentroServizio = '" & CSERV & "' And Data = ? ")
        cmd.Parameters.AddWithValue("@Data", DateSerial(ANNO, MESE, GIORNO))
        cmd.Connection = cn

        Dim myPOSTreader As OleDbDataReader = cmd.ExecuteReader()
        Do While myPOSTreader.Read

            Dim KTD As New Cls_TipoDomiciliare

            KTD.Codice = campodb(myPOSTreader.Item("Tipologia"))
            KTD.Leggi(Session("DC_OSPITE"), KTD.Codice)

            Dim KOP As New Cls_Operatore


            KOP.CodiceMedico = Val(campodb(myPOSTreader.Item("Operatore")))
            KOP.Leggi(Session("DC_OSPITE"))
            If Chk_Accreditato.Checked = True Then
                If campodb(myPOSTreader.Item("Tipologia")) = "01" Then
                    If Chk_SoloTotali.Checked = False Then
                        If campodb(myPOSTreader.Item("Tipologia")) = "03" Or campodb(myPOSTreader.Item("Tipologia")) = "04" Then
                            If Verifica = False Then
                                Appoggio = Appoggio & Replace(Replace(KTD.Descrizione, "Accreditato", "Accr."), "accreditato", "accr.") & "<br/>"
                            Else
                                Appoggio = Appoggio & "*"
                            End If
                        Else
                            If Verifica = False Then
                                Appoggio = Appoggio & Format(campodbd(myPOSTreader.Item("OraInizio")), "HH:mm") & " - " & Format(campodbd(myPOSTreader.Item("OraFine")), "HH:mm") & "<br/>" & Replace(KOP.Nome, "OPERATORE", "OP.") & " - " & Replace(Replace(KTD.Descrizione, "Accreditato", "Accr."), "accreditato", "accr.") & "<br/>"
                            Else
                                Appoggio = Appoggio & "*"
                            End If
                        End If
                    Else
                        If campodb(myPOSTreader.Item("Tipologia")) = "03" Then
                            If Verifica = False Then
                                Appoggio = Appoggio & "Pasto<br/>"
                            Else
                                Appoggio = Appoggio & ""
                            End If
                        Else
                            NumerIngressi = NumerIngressi + 1
                        End If
                    End If
                End If
            Else
                If Chk_SoloTotali.Checked = False Then
                    If campodb(myPOSTreader.Item("Tipologia")) = "03" Or campodb(myPOSTreader.Item("Tipologia")) = "04" Then
                        If Verifica = False Then
                            Appoggio = Appoggio & Replace(Replace(KTD.Descrizione, "Accreditato", "Accr."), "accreditato", "accr.") & "<br/>"
                        Else
                            Appoggio = Appoggio & "*"
                        End If
                    Else
                        If Verifica = False Then
                            Appoggio = Appoggio & Format(campodbd(myPOSTreader.Item("OraInizio")), "HH:mm") & " - " & Format(campodbd(myPOSTreader.Item("OraFine")), "HH:mm") & "<br/>" & Replace(KOP.Nome, "OPERATORE", "OP.") & " - " & Replace(Replace(KTD.Descrizione, "Accreditato", "Accr."), "accreditato", "accr.") & "<br/>"
                        Else
                            Appoggio = Appoggio & "*"
                        End If
                    End If
                Else
                    If campodb(myPOSTreader.Item("Tipologia")) = "03" Then
                        If Verifica = False Then
                            Appoggio = Appoggio & "Pasto<br/>"
                        Else
                            Appoggio = Appoggio & "*"
                        End If
                    Else
                        NumerIngressi = NumerIngressi + 1
                    End If
                End If
            End If
        Loop
        myPOSTreader.Close()
        cn.Close()
        If NumerIngressi > 0 Then
            Appoggio = Appoggio & "Accessi " & NumerIngressi & "<br/>"
        End If


        If Verifica = False Then
            Appoggio = Appoggio & "</p>"

            If Appoggio = "<p  style=""font-size: small""></p>" Then
                Dim Kas As New Cls_Movimenti

                Kas.CENTROSERVIZIO = CSERV
                Kas.CodiceOspite = CODOSP
                Kas.UltimaMovimentoPrimaData(Session("DC_OSPITE"), CODOSP, CSERV, DateSerial(ANNO, MESE, GIORNO))
                If Kas.TipoMov = "13" Or Kas.TipoMov = "" Then
                    Appoggio = "<p  style=""font-size: small"">NON PRESENTE</p>"
                Else
                    Appoggio = "&nbsp;&nbsp;&nbsp;"
                End If
            End If
        End If
        Return Appoggio
    End Function
    Private Sub ProspettoPresenze()
        Dim ConnectionString As String = Session("DC_OSPITE")
        Dim cn As OleDbConnection
        Dim MySql As String
        Dim Condizione As String

        cn = New Data.OleDb.OleDbConnection(ConnectionString)

        cn.Open()

        MyTable.Clear()
        MyTable.Columns.Clear()
        MyTable.Columns.Add("Codice Ospite", GetType(Long))
        MyTable.Columns.Add("Nome", GetType(String))
        MyTable.Columns.Add("DataNascita", GetType(String))
        MyTable.Columns.Add("Data Accoglimento", GetType(String))
        MyTable.Columns.Add("Data Uscita", GetType(String))
        Dim i As Integer
     
        For i = Val(Txt_Dal.Text) To Val(Txt_Al.Text)
            Dim DataVerifica As Date

            DataVerifica = DateSerial(Val(Txt_Anno.Text), Dd_Mese.SelectedValue, i)
            If DataVerifica.DayOfWeek = DayOfWeek.Saturday Then
                MyTable.Columns.Add("Sab-" & i)
            End If
            If DataVerifica.DayOfWeek = DayOfWeek.Sunday Then
                MyTable.Columns.Add("Dom-" & i)
            End If
            If DataVerifica.DayOfWeek = DayOfWeek.Monday Then
                MyTable.Columns.Add("Lun-" & i)
            End If
            If DataVerifica.DayOfWeek = DayOfWeek.Tuesday Then
                MyTable.Columns.Add("Mar-" & i)
            End If
            If DataVerifica.DayOfWeek = DayOfWeek.Wednesday Then
                MyTable.Columns.Add("Mer-" & i)
            End If
            If DataVerifica.DayOfWeek = DayOfWeek.Thursday Then
                MyTable.Columns.Add("Gio-" & i)
            End If
            If DataVerifica.DayOfWeek = DayOfWeek.Friday Then
                MyTable.Columns.Add("Ven-" & i)
            End If
        Next
        MyTable.Columns.Add("MOTIVO DIMISSIONE/USCITA", GetType(String))
        Dim cmd As New OleDbCommand()
        MySql = "Select * From AnagraficaComune Where CodiceParente = 0 And CodiceOspite > 0 And ( (Select count(*) From Movimenti Where CodiceOspite = AnagraficaComune.CodiceOspite And Data >= ? And Data <= ? And CENTROSERVIZIO = '" & Cmb_CServ.SelectedValue & "') > 0 OR " & _
                                                " (" & _
                                                " ((Select Top 1 TipoMov From Movimenti Where CodiceOspite = AnagraficaComune.CodiceOspite And Data < ? And CENTROSERVIZIO = '" & Cmb_CServ.SelectedValue & "' Order By Data DESC,Progressivo) <> '13') " & _
                                                " And " & _
                                                " ((Select  count(*) From Movimenti Where CodiceOspite = AnagraficaComune.CodiceOspite And Data < ? And CENTROSERVIZIO = '" & Cmb_CServ.SelectedValue & "' And TipoMov = '05') <>  0) " & _
                                                " )" & _
                                                " ) order by Nome"


        cmd.CommandText = MySql
        Dim Txt_DataDalText As Date = DateSerial(Val(Txt_Anno.Text), Dd_Mese.SelectedValue, 1)
        Dim Txt_DataAlText As Date = DateSerial(Val(Txt_Anno.Text), Dd_Mese.SelectedValue, GiorniMese(Dd_Mese.SelectedValue, Val(Txt_Anno.Text)))

        cmd.Parameters.AddWithValue("@DataDal", Txt_DataDalText)
        cmd.Parameters.AddWithValue("@DataAl", Txt_DataAlText)
        cmd.Parameters.AddWithValue("@DataDal", Txt_DataDalText)
        cmd.Parameters.AddWithValue("@DataDal", Txt_DataDalText)
        cmd.Connection = cn

        Dim COLORECELLA As String = "white"
        Dim myPOSTreader As OleDbDataReader = cmd.ExecuteReader()
        Do While myPOSTreader.Read
            Dim Appoggio As String = ""
            If Chk_Accreditato.Checked = True Then
                For i = Val(Txt_Dal.Text) To Val(Txt_Al.Text)

                    Appoggio = Appoggio & LeggiCella(Cmb_CServ.SelectedValue, Val(campodb(myPOSTreader.Item("CodiceOspite"))), Dd_Mese.SelectedValue, i, Val(Txt_Anno.Text), True)
                Next
            Else
                Appoggio = "*"
            End If
            If Appoggio <> "" Then
                Dim myriga As System.Data.DataRow = MyTable.NewRow()

                myriga(0) = campodb(myPOSTreader.Item("CodiceOspite"))

                myriga(1) = campodb(myPOSTreader.Item("Nome"))
                myriga(2) = campodb(myPOSTreader.Item("DataNascita"))

                Dim K As New Cls_Movimenti

                K.CENTROSERVIZIO = Cmb_CServ.SelectedValue
                K.CodiceOspite = Val(campodb(myPOSTreader.Item("CodiceOspite")))
                K.UltimaDataAccoglimento(Session("DC_OSPITE"))
                myriga(3) = Format(K.Data, "dd/MM/yyyy")

                K.Data = Nothing

                K.UltimaDataUscitaDefinitiva(Session("DC_OSPITE"))
                If Format(K.Data, "yyyyMMdd") > Format(DateSerial(Txt_Anno.Text, Dd_Mese.SelectedValue, 1), "yyyyMMdd") Then
                    myriga(4) = Format(K.Data, "dd/MM/yyyy")
                Else
                    myriga(4) = ""
                End If

                For i = Val(Txt_Dal.Text) To Val(Txt_Al.Text)

                    myriga(4 + i) = LeggiCella(Cmb_CServ.SelectedValue, Val(campodb(myPOSTreader.Item("CodiceOspite"))), Dd_Mese.SelectedValue, i, Val(Txt_Anno.Text), False)
                Next
                MyTable.Rows.Add(myriga)
            End If
        Loop
        cn.Close()


        Dim myriga1 As System.Data.DataRow = MyTable.NewRow()
        MyTable.Rows.Add(myriga1)



        GridView1.AutoGenerateColumns = True
        GridView1.DataSource = MyTable
        GridView1.Font.Size = 10
        GridView1.DataBind()

        Dim x As Integer

        For i = 0 To GridView1.Rows.Count - 1

            Dim jk As Integer
            jk = Val(GridView1.Rows(i).Cells(0).Text)


            Dim SoloHTML As String

            For x = Val(Txt_Dal.Text) To Val(Txt_Al.Text)
                SoloHTML = HttpContext.Current.Server.HtmlDecode(GridView1.Rows(i).Cells(x + 4).Text.ToString)
                GridView1.Rows(i).Cells(x + 4).Text = SoloHTML
            Next


            If jk > 0 Then
                GridView1.Rows(i).Cells(0).Text = "<a href=""#"" onclick=""DialogBox('Elenco_MovimentiDomiciliare.aspx?CodiceOspite=" & jk & "&CentroServizio=" & Cmb_CServ.SelectedValue & "');"" >" & GridView1.Rows(i).Cells(0).Text & "</a>"
            End If
        Next

    End Sub




    Protected Sub DD_Struttura_TextChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles DD_Struttura.TextChanged
        AggiornaCServ()
        Call EseguiJS()
    End Sub

    Private Sub EseguiJS()
        Dim MyJs As String
        MyJs = "$(document).ready(function() {"

        MyJs = MyJs & "var els = document.getElementsByTagName(""*"");"

        MyJs = MyJs & "for (var i=0;i<els.length;i++)"
        MyJs = MyJs & "if ( els[i].id ) { "
        MyJs = MyJs & " var appoggio =els[i].id; "
        MyJs = MyJs & " if (appoggio.match('Txt_Data')!= null) {  "
        MyJs = MyJs & " $(els[i]).mask(""99/99/9999"");"
        MyJs = MyJs & "    }"


        MyJs = MyJs & "} "

        MyJs = MyJs & "if (window.innerHeight>0) { $(""#BarraLaterale"").css(""height"",(window.innerHeight - 94) + ""px""); } else"
        MyJs = MyJs & "{ $(""#BarraLaterale"").css(""height"",(document.documentElement.offsetHeight - 94) + ""px"");  }"

        MyJs = MyJs & "});"

        'MyJs = "$(document).ready(function() { $('.myClass').keypress(function() { handleEnter($('.myClass').val(), true, true); } );     $('#" & Txt_ImportoPagato.ClientID & "').blur(function() { var ap = formatNumber ($('#" & Txt_ImportoPagato.ClientID & "').val(),2); $('#" & Txt_ImportoPagato.ClientID & "').val(ap); }); });"
        ScriptManager.RegisterStartupScript(Me, Me.GetType(), "visualizzaRitIm", MyJs, True)
    End Sub

    Protected Sub ImageButton3_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles ImageButton3.Click
        
        If Cmb_CServ.SelectedValue = "" Then
            REM ClientScript.RegisterClientScriptBlock(Me.GetType(), "Errore", "VisualizzaErrore('Specificare centro servizio');", True)
            ScriptManager.RegisterStartupScript(Me, Me.GetType(), "visualizzaA", "VisualizzaErrore('Specificare centro servizio');", True)
            Exit Sub
        End If
        Call ProspettoPresenze()
    End Sub

    Protected Sub ImageButton4_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles ImageButton4.Click
        If Cmb_CServ.SelectedValue = "" Then
            REM ClientScript.RegisterClientScriptBlock(Me.GetType(), "Errore", "VisualizzaErrore('Specificare centro servizio');", True)
            ScriptManager.RegisterStartupScript(Me, Me.GetType(), "visualizzaA", "VisualizzaErrore('Specificare centro servizio');", True)
            Exit Sub
        End If
        Call ProspettoPresenze()

        If MyTable.Rows.Count > 1 Then
            Response.Clear()
            Response.AddHeader("content-disposition", "attachment;filename=ProspettoPresenze.xls")
            Response.Charset = String.Empty
            Response.Cache.SetCacheability(HttpCacheability.NoCache)
            Response.ContentType = "application/vnd.xls"
            Dim stringWrite As New System.IO.StringWriter
            Dim htmlWrite As New HtmlTextWriter(stringWrite)
            'GridView1.RenderControl(htmlWrite)


            form1.Controls.Clear()
            form1.Controls.Add(GridView1)

            form1.RenderControl(htmlWrite)

            Response.Write(stringWrite.ToString())
            Response.End()
        End If
    End Sub

    Protected Sub Btn_Esci_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles Btn_Esci.Click
        Response.Redirect("Menu_StatisticheDomiciliari.aspx")
    End Sub

    Protected Sub Btn_Stampa_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles Btn_Stampa.Click

        Call ProspettoPresenze()

        Session("GrigliaSoloStampa") = MyTable

        Session("ParametriSoloStampa") = ""

        ScriptManager.RegisterStartupScript(Me, Me.GetType(), "Stampa", "openPopUp('GrigliaSoloStampa.aspx','Stampe','width=800,height=600');", True)

        Call EseguiJS()
    End Sub
End Class
