﻿Imports System.Data.OleDb
Imports System.Web.Hosting

Partial Class OspitiWeb_Uploadfoto
    Inherits System.Web.UI.Page




    Private Function myUpLoad() As String
        Dim MyCasualDir As String
        Dim i As Integer
        Dim AppoCau As Integer
        Randomize()


        If FileUpload1.PostedFile.ContentLength > 1000000 Then
            ClientScript.RegisterClientScriptBlock(Me.GetType(), "Errore", "VisualizzaErrore('<center>Le dimensioni del file sono troppo elevate</center>');", True)
            myUpLoad = "ERRORRE"
            Exit Function
        End If
        If FileUpload1.HasFile Then


            Try

                MyCasualDir = Format(Now, "yyMMddmmss")
                For i = 1 To 6
                    AppoCau = Int(Rnd(1) * 35)
                    If AppoCau > 25 Then
                        AppoCau = Asc("0") + (AppoCau - 26)
                    Else
                        AppoCau = AppoCau + 65
                    End If
                    MyCasualDir = MyCasualDir & Chr(AppoCau)
                Next
                MkDir(HostingEnvironment.ApplicationPhysicalPath() & "\FotoOspiti\" & MyCasualDir)
                FileUpload1.SaveAs(HostingEnvironment.ApplicationPhysicalPath() & "\FotoOspiti\" & MyCasualDir & "\" & FileUpload1.FileName)

                'Label1.Text = "Upload avvenuto con successo!!"

                Return "FotoOspiti\" & MyCasualDir & "\" & FileUpload1.FileName
            Catch ex As Exception
                ClientScript.RegisterClientScriptBlock(Me.GetType(), "Errore", "VisualizzaErrore('<center>Si è verificato un errore</center>');", True)
                myUpLoad = "ERRORRE"
            End Try
        Else
            ClientScript.RegisterClientScriptBlock(Me.GetType(), "Errore", "VisualizzaErrore('<center>Non è stato selzionato nessun file</center>');", True)
            myUpLoad = "ERRORRE"
        End If
    End Function

    Protected Sub UpLoadFile_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles UpLoadFile.Click
        Dim FileName As String = myUpLoad()
        If FileName = "ERRORRE" Then
            Exit Sub
        End If

        Dim MyOspite As New ClsOspite

        MyOspite.Leggi(Session("DC_OSPITE"), Session("CODICEOSPITE"))


        MyOspite.PathImmagine = FileName
        MyOspite.ScriviOspite(Session("DC_OSPITE"))
        ClientScript.RegisterClientScriptBlock(Me.GetType(), "ControlloData", "alert('UpLoad eseguito');", True)
    End Sub
End Class
