﻿<%@ Page Language="VB" AutoEventWireup="false" Inherits="OspitiWeb_FatturaDaAddebitiAccrediti" CodeFile="FatturaDaAddebitiAccrediti.aspx.vb" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="xasp" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">


<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
    <title>Fatture Addebiti-Accrediti</title>
    <asp:PlaceHolder runat="server">
        <%: System.Web.Optimization.Styles.Render("~/Content/AjaxControlToolkit/Styles/Bundle") %>
    </asp:PlaceHolder>
    <link href="ospiti.css?ver=11" rel="stylesheet" type="text/css" />
    <link rel="shortcut icon" href="images/SENIOR.ico" />
    <link href="js/jquery.autocomplete.css" rel="stylesheet" type="text/css" />


    <script src="js/jquery-1.5.1.min.js" type="text/javascript"></script>
    <script src="js/jquery.autocomplete.js" type="text/javascript"></script>
    <script src="js/soapclient.js" type="text/javascript"></script>
    <script src="/js/convertinumero.js" type="text/javascript"></script>

    <link rel="stylesheet" href="dialogbox/redips-dialog.css" type="text/css" media="screen" />
    <script type="text/javascript" src="dialogbox/redips-dialog-min.js"></script>

    <script src="js/jquery.maskedinput-1.3.min.js" type="text/javascript"></script>
    <script src="/js/formatnumer.js" type="text/javascript"></script>
    <script src="js/JSErrore.js" type="text/javascript"></script>
    <script src="js/NoEnter.js" type="text/javascript"></script>
    <link rel="stylesheet" href="jqueryui/jquery-ui.css" type="text/css" />
    <script src="jqueryui/jquery-ui.js" type="text/javascript"></script>
    <script src="jqueryui/datapicker-it.js" type="text/javascript"></script>
    <script type="text/javascript">         

        $(document).ready(function () {
            if (window.innerHeight > 0) { $("#BarraLaterale").css("height", (window.innerHeight - 94) + "px"); } else { $("#BarraLaterale").css("height", (document.documentElement.offsetHeight - 94) + "px"); }
        });


    </script>
    <style>
        .chosen-container-single .chosen-single {
            height: 30px;
            background: white;
            color: Black;
            font-family: "Cuprum","Roboto","Calibri",Lucida Grande,"Helvetica Neue",Helvetica,Arial,sans-serif;
            font-size: 16px;
        }

        .chosen-container {
            font-family: "Cuprum","Roboto","Calibri",Lucida Grande,"Helvetica Neue",Helvetica,Arial,sans-serif;
            font-size: 16px;
        }
    </style>
</head>
<body>
    <form id="form1" runat="server">
        <asp:ScriptManager ID="ScriptManager1" runat="server" EnableScriptGlobalization="true">
            <Scripts>
                <asp:ScriptReference Path="~/Scripts/AjaxControlToolkit/Bundle" />
            </Scripts>
        </asp:ScriptManager>
        <asp:Label ID="Lbl_BarraSenior" runat="server" Text=""></asp:Label>
        <div align="left">
            <table style="width: 100%;" cellpadding="0" cellspacing="0">
                <tr>
                    <td style="width: 160px; background-color: #F0F0F0;"></td>
                    <td>
                        <div class="Titolo">Ospiti - Strumenti- Elabora Fatture da Addebiti Accrediti</div>
                        <div class="SottoTitolo">
                            <br />
                            <br />
                        </div>
                    </td>
                    <td style="text-align: right;">
                        <div class="DivTasti">
                            <asp:ImageButton ID="Btn_Visualizza" runat="server" Height="38px" ImageUrl="~/images/esegui.png" class="EffettoBottoniTondi" Width="38px" />
                            <asp:ImageButton ID="Btn_Modifica0" runat="server" Height="38px" ImageUrl="~/images/salva.jpg" class="EffettoBottoniTondi" Width="38px" ToolTip="Modifica" />
                        </div>
                    </td>
                </tr>

                <tr>
                    <td style="width: 160px; background-color: #F0F0F0; vertical-align: top; text-align: center;" id="BarraLaterale">
                        <a href="Menu_Ospiti.aspx" style="border-width: 0px;">
                            <img src="images/Home.jpg" class="Effetto" alt="Menù" /></a><br />
                        <asp:ImageButton ID="Btn_Esci" runat="server" BackColor="Transparent" ImageUrl="../images/Menu_Indietro.png" ToolTip="Chiudi" />
                        <br />
                        <br />
                        <br />
                        <br />
                        <br />
                        <br />
                        <br />
                        <br />
                        <br />
                        <br />
                        <br />
                        <br />
                        <br />
                    </td>
                    <td colspan="2" style="background-color: #FFFFFF; vertical-align: top;">
                        <xasp:TabContainer ID="TabContainer1" runat="server" ActiveTabIndex="0" CssClass="TabSenior" Width="100%" BorderStyle="None" Style="margin-right: 39px">
                            <xasp:TabPanel runat="server" HeaderText="Prima Nota" ID="Tab_Anagrafica">
                                <HeaderTemplate>Fattura da Addebiti Accrediti</HeaderTemplate>
                                <ContentTemplate>

                                    <asp:UpdatePanel ID="UpdatePanel1" runat="server">
                                        <ContentTemplate>
                                            <br />

                                            <label class="LabelCampo">Data Registrazione :</label>
                                            <asp:TextBox ID="Txt_DataRegistrazione" runat="server" Width="90px"></asp:TextBox><br />
                                            <br />

                                            <label class="LabelCampo">Struttura:</label>
                                            <asp:DropDownList runat="server" ID="DD_Struttura" AutoPostBack="true"></asp:DropDownList><br />
                                            <br />
                                            <label class="LabelCampo">Centro Servizio :</label>
                                            <asp:DropDownList ID="DD_CentroServizio" runat="server"></asp:DropDownList><br />
                                            <br />


                                            <label class="LabelCampo">Anno :</label>
                                            <asp:TextBox ID="Txt_Anno" onkeypress="return soloNumeri(event);" MaxLength="4" runat="server" Width="64px"></asp:TextBox><br />
                                            <br />
                                            <label class="LabelCampo">Mese :</label>
                                            <asp:DropDownList ID="Dd_Mese" runat="server" Width="128px">
                                                <asp:ListItem Value="1">Gennaio</asp:ListItem>
                                                <asp:ListItem Value="2">Febbraio</asp:ListItem>
                                                <asp:ListItem Value="3">Marzo</asp:ListItem>
                                                <asp:ListItem Value="4">Aprile</asp:ListItem>
                                                <asp:ListItem Value="5">Maggio</asp:ListItem>
                                                <asp:ListItem Value="6">Giugno</asp:ListItem>
                                                <asp:ListItem Value="7">Luglio</asp:ListItem>
                                                <asp:ListItem Value="8">Agosto</asp:ListItem>
                                                <asp:ListItem Value="9">Settembre</asp:ListItem>
                                                <asp:ListItem Value="10">Ottobre</asp:ListItem>
                                                <asp:ListItem Value="11">Novembre</asp:ListItem>
                                                <asp:ListItem Value="12">Dicembre</asp:ListItem>
                                            </asp:DropDownList><br />
                                            <br />
                                            <label class="LabelCampo">Tipo Anagrafica :</label>
                                            <asp:DropDownList ID="DD_Tipo" runat="server" Width="128px">
                                                <asp:ListItem Value="O">Ospiti</asp:ListItem>
                                                <asp:ListItem Value="P">Parente</asp:ListItem>
                                                <asp:ListItem Value="C">Comune</asp:ListItem>
                                                <asp:ListItem Value="R">Regione</asp:ListItem>
                                                <asp:ListItem Value="J">Jolly</asp:ListItem>
                                            </asp:DropDownList><br />
                                            <br />

                                            <label class="LabelCampo">Tipo Addebito :</label>
                                            <asp:DropDownList ID="DD_TipoAddebito" runat="server" Width="128px"></asp:DropDownList>
                                            <br />
                                            <br />


                                            <label class="LabelCampo">Solo non elaborati :</label>
                                            <asp:CheckBox ID="Chk_AllAddebiti" runat="server" Text="" Checked="true" />
                                            <br />
                                            <br />
                                            <label class="LabelCampo">Non considerare in emissione :</label>
                                            <asp:CheckBox ID="Chk_FatturaDiAnticipo" runat="server" Text="" Checked="false" />
                                            <br />
                                            <br />
                                            <asp:LinkButton ID="Lbl_SelezionaTutti" runat="server">Seleziona Tutti</asp:LinkButton><br />
                                            <asp:LinkButton ID="Lbl_InvertiSelezione" runat="server">Inverti Selezione</asp:LinkButton><br />
                                            <br />
                                            <asp:Label ID="lblFC" runat="server" Text="Forza Cliente Unico :" Visible="false"></asp:Label>
                                            <asp:DropDownList ID="DD_Ente" runat="server" Visible="false" class="chosen-select"></asp:DropDownList>
                                            <asp:Label ID="lblFCDes" runat="server" ForeColor="Red" Text="(Attenzione selezionando un ente gli add/acc saranno emessi tutti al cliente selezionato)<BR/><BR/><BR/>" Visible="false"></asp:Label>


                                            <asp:GridView ID="Grd_AggiornaRette" runat="server" CellPadding="4" Height="60px"
                                                ShowFooter="True" BackColor="White" BorderColor="#6FA7D1" BorderStyle="Solid" BorderWidth="1px">
                                                <RowStyle ForeColor="#333333" BackColor="White" />
                                                <Columns>
                                                    <asp:TemplateField HeaderText="Seleziona">
                                                        <ItemTemplate>
                                                            <asp:CheckBox ID="Chk_Seleziona" runat="server" />
                                                        </ItemTemplate>
                                                        <HeaderStyle Font-Bold="False" Font-Italic="False" />
                                                        <ItemStyle Width="100px" />
                                                    </asp:TemplateField>

                                                    <asp:BoundField DataField="CodiceOspite" HeaderText="Codice Ospite" />
                                                    <asp:BoundField DataField="NomeOspite" HeaderText="Nome Ospite" />
                                                    <asp:BoundField DataField="Data" HeaderText="Data" />
                                                    <asp:BoundField DataField="TipoMovimento" HeaderText="Tipo Movimento" />
                                                    <asp:BoundField DataField="Riferimento" HeaderText="Riferimento" />
                                                    <asp:BoundField DataField="FiguradiRiferimento" HeaderText="Figura di Riferimento" />
                                                    <asp:BoundField DataField="Importo" HeaderText="Importo" />
                                                    <asp:BoundField DataField="Retta" HeaderText="Retta" />
                                                    <asp:BoundField DataField="PeriodoCompetenza" HeaderText="Periodo Competenza" />
                                                    <asp:BoundField DataField="Descrizione" HeaderText="Descrizione" />
                                                    <asp:BoundField DataField="Codice" HeaderText="Codice" />

                                                </Columns>
                                                <FooterStyle BackColor="White" ForeColor="#023102" />
                                                <HeaderStyle BackColor="#A6C9E2" Font-Bold="False" ForeColor="White" BorderColor="#6FA7D1" BorderWidth="1" />
                                                <PagerStyle BackColor="#336666" ForeColor="White" HorizontalAlign="Center" />
                                                <SelectedRowStyle BackColor="#339966" Font-Bold="True" ForeColor="White" />
                                            </asp:GridView>
                                            <br />
                                            <br />
                                            <asp:Label ID="Lbl_errori" runat="server" ForeColor="DarkRed" Width="272px"></asp:Label><br />
                                            <br />
                                        </ContentTemplate>
                                    </asp:UpdatePanel>
                                </ContentTemplate>
                            </xasp:TabPanel>
                        </xasp:TabContainer>
                    </td>
                </tr>
            </table>
        </div>
    </form>
</body>
</html>

