﻿<%@ Page Language="VB" AutoEventWireup="false" Inherits="OspitiWeb_GestioneNoteFatture" CodeFile="GestioneNoteFatture.aspx.vb" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="xasp" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
	<meta http-equiv="x-ua-compatible" content="IE=9" />
	<title>Note Fatture</title>
	<asp:PlaceHolder runat="server">
		<%: System.Web.Optimization.Styles.Render("~/Content/AjaxControlToolkit/Styles/Bundle") %>
	</asp:PlaceHolder>
	<link href="ospiti.css?ver=11" rel="stylesheet" type="text/css" />
	<link rel="shortcut icon" href="images/SENIOR.ico" />
	<link href="js/jquery.autocomplete.css" rel="stylesheet" type="text/css" />

	<script src="js/jquery-1.5.1.min.js" type="text/javascript"></script>
	<script src="js/jquery.autocomplete.js" type="text/javascript"></script>
	<script src="js/soapclient.js" type="text/javascript"></script>
	<script src="/js/convertinumero.js" type="text/javascript"></script>



	<script src="js/jquery.maskedinput-1.3.min.js" type="text/javascript"></script>
	<script src="/js/formatnumer.js" type="text/javascript"></script>
	<script src="/js/pianoconti.js" type="text/javascript"></script>
	<script src="js/JSErrore.js" type="text/javascript"></script>
	<script type="text/javascript">         
		$(document).ready(function () {
			$('html').keyup(function (event) {

				if (event.keyCode == 113) {
					__doPostBack("Imb_Modifica", "0");
				}


			});
		});

		$(document).ready(function () {
			if (window.innerHeight > 0) { $("#BarraLaterale").css("height", (window.innerHeight - 94) + "px"); } else { $("#BarraLaterale").css("height", (document.documentElement.offsetHeight - 94) + "px"); }
		});
	</script>
</head>
<body>
	<form id="form1" runat="server">
		<asp:ScriptManager ID="ScriptManager1" runat="server" EnableScriptGlobalization="true">
			<Scripts>
				<asp:ScriptReference Path="~/Scripts/AjaxControlToolkit/Bundle" />
			</Scripts>
		</asp:ScriptManager>
		<asp:Label ID="Lbl_BarraSenior" runat="server" Text=""></asp:Label>
		<div style="text-align: left;">

			<table style="width: 100%;" cellpadding="0" cellspacing="0">
				<tr>
					<td style="width: 160px; background-color: #F0F0F0;"></td>
					<td>
						<div class="Titolo">Ospiti - Tabelle - Note Fatture</div>
						<div class="SottoTitolo">
							<br />
							<br />
						</div>
					</td>
					<td style="text-align: right;">
						<div class="DivTasti">
							<asp:ImageButton runat="server" ImageUrl="~/images/duplica.png" CssClass="EffettoBottoniTondi" Height="38px" ToolTip="Duplica Nota" ID="ImageButton1"></asp:ImageButton>&nbsp;
			<asp:ImageButton runat="server" ImageUrl="~/images/salva.jpg" CssClass="EffettoBottoniTondi" Height="38px" ToolTip="Modifica / Inserisci" ID="Imb_Modifica"></asp:ImageButton>
							<asp:ImageButton runat="server" OnClientClick="return window.confirm('Eliminare?');" ImageUrl="~/images/elimina.jpg" CssClass="EffettoBottoniTondi" Height="38px" ToolTip="Elimina" ID="ImageButton2"></asp:ImageButton>
						</div>
					</td>
				</tr>
				<tr>
					<td style="width: 160px; background-color: #F0F0F0; vertical-align: top; text-align: center;" id="BarraLaterale">
						<a href="Menu_Ospiti.aspx" style="border-width: 0px;">
							<img src="images/Home.jpg" class="Effetto" alt="Menù" /></a><br />
						<asp:ImageButton ID="Btn_Esci" runat="server" BackColor="Transparent" ImageUrl="../images/Menu_Indietro.png" class="Effetto" ToolTip="Chiudi" />
						<br />
						<br />
						<br />
						<br />
						<br />
						<br />
						<br />
						<br />
						<br />
					</td>
					<td colspan="2" style="background-color: #FFFFFF;" valign="top">
						<xasp:TabContainer ID="TabContainer1" runat="server" ActiveTabIndex="0" CssClass="TabSenior" Width="100%" BorderStyle="None" Style="margin-right: 39px">
							<xasp:TabPanel runat="server" HeaderText="Note Fatture" ID="Tab_Anagrafica">
								<HeaderTemplate>
									Note Fatture      
								</HeaderTemplate>
								<ContentTemplate>
									<label class="LabelCampo">ID : </label>
									<asp:TextBox ID="Txt_ID" runat="server"></asp:TextBox><br />
									<br />
									<label class="LabelCampo">Struttura:</label>
									&nbsp;&nbsp;<asp:DropDownList runat="server" ID="DD_Struttura"
										AutoPostBack="True">
									</asp:DropDownList><br />
									<br />

									<label class="LabelCampo">Centro Servizio : </label>
									<asp:DropDownList ID="Cmb_CServ" runat="server" Width="288px"></asp:DropDownList><br />
									<br />

									<label class="LabelCampo">Tipo : </label>
									<asp:RadioButton ID="RB_OspitiParenti" runat="server" Text="Ospiti-Parenti" GroupName="Tipo" />
									<asp:RadioButton ID="RB_ComuneRegioni" runat="server" Text="Comuni-Regioni" GroupName="Tipo" />
									<asp:RadioButton ID="RB_Entrambe" runat="server" Text="Entrambe" GroupName="Tipo" /><br />
									<br />

									<label class="LabelCampo">Comune :</label>
									<asp:TextBox ID="Txt_ComRes" MaxLength="30" runat="server" Width="375px"></asp:TextBox><br />
									<br />

									<label class="LabelCampo">Regione :</label>
									<asp:DropDownList ID="DD_Regione" runat="server" Width="336px"></asp:DropDownList><br />
									<br />

									<label class="LabelCampo">Codici Ospiti (separati da una virgola) :</label>
									<asp:TextBox ID="Txt_CodiciOspiti" MaxLength="30" runat="server" Width="375px"></asp:TextBox><br />
									<br />

									<label class="LabelCampo">Numero Registrazione :</label>
									<asp:TextBox ID="Txt_NumeroRegistrazione" onkeypress="return soloNumeri(event);" MaxLength="30" runat="server" Width="75px"></asp:TextBox><br />
									<br />

									<label class="LabelCampo">Note Up :</label>
									<asp:TextBox ID="Txt_NoteUp" Height="180px" TextMode="MultiLine" runat="server" Width="575px"></asp:TextBox><br />
									<br />
									<label class="LabelCampo">Note Down :</label>
									<asp:TextBox ID="Txt_NoteDown" Height="180px" TextMode="MultiLine" runat="server" Width="575px"></asp:TextBox><br />
								</ContentTemplate>
							</xasp:TabPanel>
						</xasp:TabContainer>
					</td>
				</tr>
			</table>
		</div>
	</form>
</body>
</html>
