﻿<%@ Page Language="VB" AutoEventWireup="false" Inherits="_Default" EnableEventValidation="false" CodeFile="Anagrafica.aspx.vb" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="xasp" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <asp:PlaceHolder runat="server">
        <%: System.Web.Optimization.Styles.Render("~/Content/AjaxControlToolkit/Styles/Bundle") %>
    </asp:PlaceHolder>
    <meta http-equiv="x-ua-compatible" content="IE=9" />
    <title>Scheda Anagrafica</title>

    <link href="ospiti.css?ver=11" rel="stylesheet" type="text/css" />
    <link rel="shortcut icon" href="images/SENIOR.ico" />

    <link rel="stylesheet" href="//maxcdn.bootstrapcdn.com/font-awesome/4.3.0/css/font-awesome.min.css" />

    <link href="js/jquery.autocomplete.css" rel="stylesheet" type="text/css" />


    <script src="js/jquery-1.5.1.min.js" type="text/javascript"></script>
    <script src="js/jquery.autocomplete.js" type="text/javascript"></script>
    <link rel="stylesheet" href="dialogbox/redips-dialog.css" type="text/css" media="screen" />
    <script type="text/javascript" src="dialogbox/redips-dialog-min.js"></script>
    <script type="text/javascript" src="dialogbox/script.js"></script>



    <script src="js/soapclient.js" type="text/javascript"></script>
    <script src="/js/convertinumero.js" type="text/javascript"></script>



    <script src="js/jquery.maskedinput-1.3.min.js" type="text/javascript"></script>
    <script src="/js/formatnumer.js" type="text/javascript"></script>
    <script src="js/JSErrore.js" type="text/javascript"></script>
    <script src="js/NoEnter.js" type="text/javascript"></script>
    <link rel="stylesheet" href="jqueryui/jquery-ui.css" type="text/css" />
    <script src="jqueryui/jquery-ui.js" type="text/javascript"></script>
    <script src="jqueryui/datapicker-it.js" type="text/javascript"></script>


    <script type="text/javascript" src="../js/menuanagraficaospiti.js?ver=7"></script>
    <style type="text/css">
        #UltimoMese {
            position: absolute;
            right: 240px;
            top: 290px;
            visibility: hidden;
        }

        #ApriMese {
        }

        .CSSTableGenerator {
            margin: 0px;
            padding: 0px;
            width: 350px;
            box-shadow: 10px 10px 5px #888888;
            border: 1px solid #000000;
            -moz-border-radius-bottomleft: 0px;
            -webkit-border-bottom-left-radius: 0px;
            border-bottom-left-radius: 0px;
            -moz-border-radius-bottomright: 0px;
            -webkit-border-bottom-right-radius: 0px;
            border-bottom-right-radius: 0px;
            -moz-border-radius-topright: 0px;
            -webkit-border-top-right-radius: 0px;
            border-top-right-radius: 0px;
            -moz-border-radius-topleft: 0px;
            -webkit-border-top-left-radius: 0px;
            border-top-left-radius: 0px;
        }

            .CSSTableGenerator table {
                border-collapse: collapse;
                border-spacing: 0;
                width: 100%;
                height: 100%;
                margin: 0px;
                padding: 0px;
            }

            .CSSTableGenerator tr:last-child td:last-child {
                -moz-border-radius-bottomright: 0px;
                -webkit-border-bottom-right-radius: 0px;
                border-bottom-right-radius: 0px;
            }

            .CSSTableGenerator table tr:first-child td:first-child {
                -moz-border-radius-topleft: 0px;
                -webkit-border-top-left-radius: 0px;
                border-top-left-radius: 0px;
            }

            .CSSTableGenerator table tr:first-child td:last-child {
                -moz-border-radius-topright: 0px;
                -webkit-border-top-right-radius: 0px;
                border-top-right-radius: 0px;
            }

            .CSSTableGenerator tr:last-child td:first-child {
                -moz-border-radius-bottomleft: 0px;
                -webkit-border-bottom-left-radius: 0px;
                border-bottom-left-radius: 0px;
            }

            .CSSTableGenerator tr:hover td {
            }

            .CSSTableGenerator tr:nth-child(odd) {
                background-color: #ffffff;
            }

            .CSSTableGenerator tr:nth-child(even) {
                background-color: #ffffff;
            }

            .CSSTableGenerator td {
                vertical-align: middle;
                border: 1px solid #000000;
                border-width: 0px 1px 1px 0px;
                text-align: left;
                padding: 7px;
                font-size: 10px;
                font-family: "Source Sans Pro",sans-serif;
                font-weight: normal;
                color: #000000;
            }

            .CSSTableGenerator tr:last-child td {
                border-width: 0px 1px 0px 0px;
            }

            .CSSTableGenerator tr td:last-child {
                border-width: 0px 0px 1px 0px;
            }

            .CSSTableGenerator tr:last-child td:last-child {
                border-width: 0px 0px 0px 0px;
            }

            .CSSTableGenerator tr:first-child td {
                background: -o-linear-gradient(bottom, #005fbf 5%, #003f7f 100%);
                background: -webkit-gradient( linear, left top, left bottom, color-stop(0.05, #005fbf), color-stop(1, #003f7f) );
                background: -moz-linear-gradient( center top, #005fbf 5%, #003f7f 100% );
                filter: progid:DXImageTransform.Microsoft.gradient(startColorstr="#005fbf", endColorstr="#003f7f");
                background: -o-linear-gradient(top,#005fbf,003f7f);
                background-color: #005fbf;
                border: 0px solid #000000;
                text-align: center;
                border-width: 0px 0px 1px 1px;
                font-size: 14px;
                font-family: "Source Sans Pro",sans-serif;
                font-weight: bold;
                color: #ffffff;
            }

            .CSSTableGenerator tr:first-child:hover td {
                background: -o-linear-gradient(bottom, #005fbf 5%, #003f7f 100%);
                background: -webkit-gradient( linear, left top, left bottom, color-stop(0.05, #005fbf), color-stop(1, #003f7f) );
                background: -moz-linear-gradient( center top, #005fbf 5%, #003f7f 100% );
                filter: progid:DXImageTransform.Microsoft.gradient(startColorstr="#005fbf", endColorstr="#003f7f");
                background: -o-linear-gradient(top,#005fbf,003f7f);
                background-color: #005fbf;
            }

            .CSSTableGenerator tr:first-child td:first-child {
                border-width: 0px 0px 1px 0px;
            }

            .CSSTableGenerator tr:first-child td:last-child {
                border-width: 0px 0px 1px 1px;
            }

        .fotosub {
            position: relative;
            color: black;
            text-decoration: none;
        }


        .Row {
            display: table;
            width: 100%; /*Optional*/
            table-layout: fixed; /*Optional*/
            border-spacing: 10px; /*Optional*/
        }

        .Column {
            display: table-cell;
            vertical-align: top;
        }

        .cornicegrigia {
            border-bottom-color: rgb(227, 227, 227);
            border-bottom-left-radius: 4px;
            border-bottom-right-radius: 4px;
            border-bottom-style: solid;
            border-bottom-width: 1px;
            border-image-outset: 0px;
            border-image-repeat: stretch;
            border-image-slice: 100%;
            border-image-source: none;
            border-image-width: 1;
            border-left-color: rgb(227, 227, 227);
            border-left-style: solid;
            border-left-width: 1px;
            border-right-color: rgb(227, 227, 227);
            border-right-style: solid;
            border-right-width: 1px;
            border-top-color: rgb(227, 227, 227);
            border-top-left-radius: 4px;
            border-top-right-radius: 4px;
            border-top-style: solid;
            border-top-width: 1px;
            box-shadow: rgba(0, 0, 0, 0.0470588) 0px 1px 1px 0px inset;
            color: rgb(75, 75, 75);
            display: block;
            font-family: "Source Sans Pro",sans-serif;
            font-size: 16px;
            height: 325px;
            line-height: 18px;
            margin-bottom: 4.80000019073486px;
            margin-left: 0px;
            margin-right: 0px;
            margin-top: 4.80000019073486px;
            min-height: 20px;
            padding-bottom: 8px;
            padding-left: 8px;
            padding-right: 8px;
            padding-top: 8px;
        }

        .fotosub div {
            position: absolute;
            visibility: hidden;
            background-color: #c8ffb4;
            border: dotted #c8ffb4 1px;
            color: black;
            text-decoration: none;
            padding: 3px;
            top: 20;
        }

        .fotosub:hover div {
            visibility: visible;
            index-z: 100;
        }
    </style>
    <script type="text/javascript">
        function CloseMese() {
            if ($("#UltimoMese").css("visibility") != "hidden") {
                $("#UltimoMese").css('visibility', 'hidden');
            } else {
                $("#UltimoMese").css('visibility', 'visible');
            }
        }

        function DialogBox(Path) {

            REDIPS.dialog.show(500, 300, '<iframe id="output" src="' + Path + '" height="300px" width="500"></iframe>');
            return false;

        }



        $(document).ready(function () {
            $('html').keyup(function (event) {

                if (event.keyCode == 113) {
                    __doPostBack("Btn_Modifica", "0");
                }


            });
        });

        $(document).ready(function () {
            if (window.innerHeight > 0) { $("#BarraLaterale").css("height", (window.innerHeight - 94) + "px"); } else { $("#BarraLaterale").css("height", (document.documentElement.offsetHeight - 94) + "px"); }
            $("#BarraLaterale").css("height");

        });
    </script>

    <style type="text/css">
        .HeaderGriglia {
            background-color: #D5E4F4;
            background-image: url('images/sfondogrigliaanagrafica.jpg');
            color: #0000FF;
            font-size: small;
        }
    </style>
</head>
<body>
    <form id="form1" name="form1" runat="server">
        <asp:ScriptManager ID="ScriptManager1" runat="server" EnableScriptGlobalization="true">
            <Scripts>
                <asp:ScriptReference Path="~/Scripts/AjaxControlToolkit/Bundle" />
            </Scripts>
        </asp:ScriptManager>
        <asp:Label ID="Lbl_BarraSenior" runat="server" Text=""></asp:Label>

        <div align="left">



            <table style="width: 100%;" cellpadding="0" cellspacing="0">
                <tr>
                    <td style="width: 160px; background-color: #F0F0F0;"></td>
                    <td>
                        <div class="Titolo">Ospiti - Anagrafica</div>
                        <div class="SottoTitoloOSPITE">
                            <asp:Label ID="Lbl_NomeOspite" runat="server" Style="text-indent: 120px;"></asp:Label><br />
                            <br />
                        </div>
                    </td>
                    </td>
    <td style="text-align: right;">
        <div class="DivTastiOspite">
            <asp:ImageButton runat="server" ImageUrl="~/images/salva.jpg" class="EffettoBottoniTondi" Height="38px" ToolTip="Modifica / Inserisci (F2)" ID="Btn_Modifica"></asp:ImageButton>
            <asp:ImageButton runat="server" OnClientClick="return window.confirm('Eliminare?');" ImageUrl="~/images/elimina.jpg" class="EffettoBottoniTondi" Height="38px" ToolTip="Elimina" ID="Btn_Elimina"></asp:ImageButton>
        </div>
    </td>
                </tr>

                <tr>
                    <td style="width: 160px; background-color: #F0F0F0; vertical-align: top; text-align: center;" id="BarraLaterale">
                        <a href="Menu_Ospiti.aspx" style="border-width: 0px;">
                            <img src="images/Home.jpg" class="Effetto" alt="Menù" /></a><br />
                        <asp:ImageButton ID="Btn_Esci" runat="server" BackColor="Transparent" ImageUrl="../images/Menu_Indietro.png" class="Effetto" ToolTip="Chiudi" />
                        <br />
                        <asp:Label ID="lbl_Lisitino" runat="server" Text=""></asp:Label>
                        <div id="MENUDIV" width="160px"></div>

                        <br />
                        <br />
                        <br />
                        <br />
                        <br />
                        <br />
                        <br />
                        <br />
                    </td>
                    <td colspan="2" style="background-color: #FFFFFF; vertical-align: top;">
                        <xasp:TabContainer ID="TabContainer1" runat="server" ActiveTabIndex="0" CssClass="TabSenior"
                            Width="100%" BorderStyle="None" Style="margin-right: 39px">
                            <xasp:TabPanel runat="server" HeaderText="Anagrafica" ID="Tab_Anagrafica">
                                <HeaderTemplate>
                                    Anagrafica
            
     
         
                                </HeaderTemplate>
                                <ContentTemplate>


                                    <asp:CheckBox ID="Chk_NonInUso" runat="server" Text="Non In Uso" /><asp:TextBox ID="Txt_MotivoNonInUso" runat="server" Width="30%" MaxLength="50"></asp:TextBox><br />
                                    <div class="Row">
                                        <div class="Column">
                                            <div class="cornicegrigia">

                                                <br />
                                                <label class="LabelCampo">Cognome :</label>
                                                <asp:TextBox ID="Txt_Cognome" runat="server" Width="30%" Style="border-color: Red;"></asp:TextBox><br />
                                                <br />
                                                <label class="LabelCampo">Nome :</label>
                                                <asp:TextBox ID="Txt_Nome" runat="server" Width="30%" Style="border-color: Red;"></asp:TextBox>
                                                <br />

                                                <br />

                                                <label class="LabelCampo">Sesso : </label>
                                                <asp:RadioButton ID="RB_Maschile" runat="server" GroupName="sesso" />Maschile
                                                <asp:RadioButton ID="RB_Femminile" runat="server" GroupName="sesso" />Femminile&nbsp;
                                                <br />
                                                <br />
                                                <label class="LabelCampo">Data Nascita :</label>
                                                <asp:TextBox ID="Txt_DataNascita" runat="server" Width="90px" Style="border-color: Red;"></asp:TextBox>
                                                <br />
                                                <br />


                                                <label class="LabelCampo">Comune nascita :</label>
                                                <asp:TextBox ID="Txt_ComuneNascita" runat="server" Width="50%"></asp:TextBox><br />
                                                <br />

                                                <label class="LabelCampo">Codice Fiscale :</label>
                                                <asp:TextBox ID="Txt_CodiceFiscale" runat="server" Width="192px"></asp:TextBox><br />
                                                <br />

                                                <label class="LabelCampo">Data Domanda :</label>
                                                <asp:TextBox ID="Txt_DataDomanda" runat="server" Width="90px"></asp:TextBox>

                                            </div>
                                        </div>

                                        <div class="Column">
                                            <div class="cornicegrigia">
                                                <br />
                                                <label class="LabelCampo">Accoglimento :</label>
                                                <asp:Label ID="Txt_DataAccoglimento" ForeColor="Blue" runat="server"></asp:Label>
                                                <br />
                                                <br />
                                                <label class="LabelCampo">Uscita Definitiva :</label>
                                                <asp:Label ID="Txt_DataUscitaDefinitiva" ForeColor="Red" runat="server"></asp:Label>
                                                <br />
                                                <br />
                                                <label class="LabelCampo">Telefono :</label>
                                                <asp:Label ID="Lbl_DatiContatto" runat="server"></asp:Label>
                                                <br />
                                                <br />
                                                <label class="LabelCampo">Cellulare :</label>
                                                <asp:Label ID="Lbl_Cellulare" runat="server"></asp:Label>
                                                <br />
                                                <br />
                                                <label class="LabelCampo">Mail :</label>
                                                <asp:Label ID="Lbl_Mail" runat="server"></asp:Label>
                                                <br />
                                                <br />
                                                <label class="LabelCampo">Fax :</label>
                                                <asp:Label ID="Lbl_Fax" runat="server"></asp:Label>
                                                <br />
                                                <br />
                                                <label class="LabelCampo">Medico :</label>
                                                <asp:Label ID="Lbl_Medico" runat="server"></asp:Label><br />
                                                <br />

                                                <label class="LabelCampo">Riferimenti :</label>
                                                <div id="ApriMese">
                                                    <a href="#" onclick="CloseMese();">
                                                        <img src="../images/RIFERIMENTI.jpg" alt="Chiudi Mese" /></a>
                                                </div>
                                                <div id="UltimoMese">
                                                    <div class="CSSTableGenerator">
                                                        <asp:Label ID="Lbl_Riferimenti" runat="server"></asp:Label>
                                                    </div>
                                                </div>

                                            </div>
                                        </div>

                                    </div>


                                    </div>
     <div class="Row">

         <div class="Column" style="width: 75%">
             <div class="cornicegrigia" style="height: 400px;">
                 <label class="LabelCampo">
                     Retta Totale :
                 </label>

                 <div style="background-color: #FFFFFF; border-style: solid; border-color: white;">
                     <asp:Label ID="Lbl_RettaTotale" runat="server" Width="50%"></asp:Label>
                 </div>
                 <br />
                 <label class="LabelCampo">
                     Stato autos. :
                 </label>

                 <div style="background-color: #FFFFFF; border-style: solid; border-color: white;">
                     <asp:Label ID="Lbl_StatoAuto" runat="server" Width="50%"></asp:Label>
                 </div>
                 <br />
                 <label class="LabelCampo">
                     Modalità di calcolo :
                 </label>

                 <div style="background-color: #FFFFFF; border-style: solid; border-color: white;">
                     <asp:Label ID="Lbl_Modalita" runat="server" Width="50%"></asp:Label>
                 </div>
                 <br />
                 <label class="LabelCampo">
                     Importo Ospite :       
                 </label>

                 <div style="background-color: #FFFFFF; border-style: solid; border-color: white;">
                     <asp:Label ID="Lbl_ImportoOspite" runat="server" Width="50%"></asp:Label>
                 </div>
                 <br />
                 <label class="LabelCampo">
                     Importo Comune :
                 </label>

                 <div style="background-color: #FFFFFF; border-style: solid; border-color: white;">
                     <asp:Label ID="Lbl_ImportoComune" runat="server" Width="50%"></asp:Label>
                 </div>
                 <br />
                 <label class="LabelCampo">
                     Importo Jolly :
                 </label>

                 <div style="background-color: #FFFFFF; border-style: solid; border-color: white;">
                     <asp:Label ID="Lbl_ImportoJolly" runat="server" Width="50%"></asp:Label>
                 </div>
                 <br />
                 <label class="LabelCampo">
                     Impegnativa :
                 </label>
                 <div style="background-color: #FFFFFF; border-style: solid; border-color: white;">
                     <asp:Label ID="Lbl_Impegnativa" runat="server" Width="50%"></asp:Label>
                 </div>
                 <br />
                 <label class="LabelCampo">
                     Dati Ise :
                 </label>
                 <div style="background-color: #FFFFFF; border-style: solid; border-color: white;">
                     <asp:Label ID="Lbl_DatiIse" runat="server" Width="50%"></asp:Label>
                 </div>
                 </label>  
         <br />
                 <label class="LabelCampo">
                     Dati Pagamento :
                 </label>
                 <div style="background-color: #FFFFFF; border-style: solid; border-color: white;">
                     <asp:Label ID="Lbl_DatiPagamento" runat="server" Width="50%"></asp:Label>
                 </div>
                 <br />
                 </label>                      
             </div>
         </div>


         <div class="Column" style="width: 25%; text-align: center; vertical-align: middle;">
             <div class="cornicegrigia" style="height: 400px;">
                 <a href="#" class="fotosub">
                     <asp:Image ID="Imd_FotoOspite" runat="server" Width="180px" Height="200px" />

                     <div id="loadfoto" style="position: absolute; top: 0px; left: 100px; z-index: 1px;">
                         <input name="Button1" type="button" value="Carica Foto" onclick="DialogBox('Uploadfoto.aspx');" />
                     </div>
                 </a>
             </div>
         </div>
     </div>


                                    <br />
                                    <br />
                                    <asp:Label ID="Lbl_Parenti" runat="server" Width="400px"></asp:Label>
                                    </div>
    <asp:Label ID="Lbl_Errori" runat="server" ForeColor="Red" Width="352px"></asp:Label>
                                    <br />
                                    <br />
                                </ContentTemplate>

                            </xasp:TabPanel>


                        </xasp:TabContainer>
                    </td>
                </tr>
            </table>
        </div>





    </form>


</body>
</html>

