﻿Imports System.Web.Hosting
Imports System.Data.OleDb
Partial Class OspitiWeb_FatturaAddExtra
    Inherits System.Web.UI.Page
    Dim Tabella As New System.Data.DataTable("tabella")

    Function campodb(ByVal oggetto As Object) As String
        If IsDBNull(oggetto) Then
            Return ""
        Else
            Return oggetto
        End If

    End Function

    Private Sub EseguiJS()
        Dim MyJs As String
        MyJs = "$(document).ready(function() {"

        MyJs = MyJs & "var els = document.getElementsByTagName(""*"");"

        MyJs = MyJs & "for (var i=0;i<els.length;i++)"
        MyJs = MyJs & "if ( els[i].id ) { "
        MyJs = MyJs & " var appoggio =els[i].id; "
        MyJs = MyJs & " if (appoggio.match('Txt_Data')!= null) {  "
        MyJs = MyJs & " $(els[i]).mask(""99/99/9999"");"

        MyJs = MyJs & " $(els[i]).datepicker({ changeMonth: true,changeYear: true,  yearRange: ""1990:" & Year(Now) + 5 & """},$.datepicker.regional[""it""]);"

        MyJs = MyJs & "    }"

        MyJs = MyJs & " if (appoggio.match('Txt_DataRegistrazione')!= null) {  "
        MyJs = MyJs & " $(els[i]).mask(""99/99/9999"");"

        MyJs = MyJs & " $(els[i]).datepicker({ changeMonth: true,changeYear: true,  yearRange: ""1990:" & Year(Now) + 5 & """},$.datepicker.regional[""it""]);"

        MyJs = MyJs & "    }"



        MyJs = MyJs & " if ((appoggio.match('TxtImporto')!= null) ) {  "
        MyJs = MyJs & " $(els[i]).keypress(function() { ForceNumericInput($(this).val(), true, true); } ); "
        MyJs = MyJs & " $(els[i]).blur(function() { var ap = formatNumber($(this).val(),2); $(this).val(ap); });"
        MyJs = MyJs & "    }"


        MyJs = MyJs & "} "
        MyJs = MyJs & "});"

        'MyJs = "$(document).ready(function() { $('.myClass').keypress(function() { handleEnter($('.myClass').val(), true, true); } );     $('#" & Txt_ImportoPagato.ClientID & "').blur(function() { var ap = formatNumber ($('#" & Txt_ImportoPagato.ClientID & "').val(),2); $('#" & Txt_ImportoPagato.ClientID & "').val(ap); }); });"
        ScriptManager.RegisterStartupScript(Me, Me.GetType(), "visualizzaRitIm", MyJs, True)
    End Sub


    Public Function GiorniMese(ByVal Mese As Byte, ByVal Anno As Integer) As Byte
        If Mese = 1 Or Mese = 3 Or Mese = 5 Or Mese = 7 Or Mese = 8 Or _
           Mese = 10 Or Mese = 12 Then
            GiorniMese = 31
        Else
            If Mese <> 2 Then
                GiorniMese = 30
            Else
                If Day(DateSerial(Anno, Mese, 29)) = 29 Then
                    GiorniMese = 29
                Else
                    GiorniMese = 28
                End If
            End If
        End If
    End Function
    Protected Sub Btn_Visualizza_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles Btn_Visualizza.Click
        If DD_CentroServizio.SelectedValue = "" Then
            ClientScript.RegisterClientScriptBlock(Me.GetType(), "Errore", "VisualizzaErrore('Specifica centroservizio');", True)
            Call EseguiJS()
            Exit Sub
        End If

        If Val(Txt_Anno.Text) < 1990 Then
            ClientScript.RegisterClientScriptBlock(Me.GetType(), "Errore", "VisualizzaErrore('Specificare Anno');", True)
            Call EseguiJS()
            Exit Sub
        End If

        If Not IsDate(Txt_DataRegistrazione.Text) Then
            ClientScript.RegisterClientScriptBlock(Me.GetType(), "Errore", "VisualizzaErrore('Data validita formalmente errata');", True)
            Call EseguiJS()
            Exit Sub
        End If


        Dim VerificaElaborazione As New Cls_Parametri


        If VerificaElaborazione.InElaborazione(Session("DC_OSPITE")) Then
            ClientScript.RegisterClientScriptBlock(Me.GetType(), "Errore", "VisualizzaErrore('Elaborazione in esecuzione non posso procedere');", True)
            Exit Sub
        End If


        Dim ParametriGenerale As New Cls_ParametriGenerale

        If ParametriGenerale.InElaborazione(Session("DC_GENERALE")) Then
            ClientScript.RegisterClientScriptBlock(Me.GetType(), "Errore", "alert('Elaborazione in esecuzione non posso procedere');", True)
            Exit Sub
        End If



        Dim cn As OleDbConnection


        cn = New Data.OleDb.OleDbConnection(Session("DC_OSPITE"))

        cn.Open()



        Dim cmdDeleteNotifiche As New OleDbCommand()
        cmdDeleteNotifiche.CommandText = ("Delete  from notifiche ")
        cmdDeleteNotifiche.Connection = cn
        cmdDeleteNotifiche.ExecuteNonQuery()

        Dim cmd As New OleDbCommand()
        Dim MySql As String
        Dim TRig As String
        Dim Fig As String

        Tabella.Clear()
        Tabella.Columns.Clear()
        Tabella.Columns.Add("CodiceOspite", GetType(Integer))
        Tabella.Columns.Add("NomeOspite", GetType(String))
        Tabella.Columns.Add("Data", GetType(String))
        Tabella.Columns.Add("TipoMovimento", GetType(String))
        Tabella.Columns.Add("Riferimento", GetType(String))
        Tabella.Columns.Add("FiguradiRiferimento", GetType(String))
        Tabella.Columns.Add("Importo", GetType(String))
        Tabella.Columns.Add("Retta", GetType(String))
        Tabella.Columns.Add("PeriodoCompetenza", GetType(String))
        Tabella.Columns.Add("Descrizione", GetType(String))
        Tabella.Columns.Add("Codice", GetType(String))
        Tabella.Columns.Add("Id", GetType(Long))

        Dim Condizione As String = ""

        MySql = "Select * From AnagraficaComune Where (NonInUso = '' or NonInUso  is null ) And CodiceParente = 0 And CodiceOspite > 0 And ( (Select count(*) From Movimenti Where CodiceOspite = AnagraficaComune.CodiceOspite And Data >= ? And Data <= ? And CENTROSERVIZIO = '" & DD_CentroServizio.SelectedValue & "') > 0 OR " & _
                                                " (" & _
                                                " ((Select Top 1 TipoMov From Movimenti Where CodiceOspite = AnagraficaComune.CodiceOspite And Data < ? And CENTROSERVIZIO = '" & DD_CentroServizio.SelectedValue & "' Order By Data DESC,Progressivo Desc) <> '13') " & _
                                                " And " & _
                                                " ((Select  count(*) From Movimenti Where CodiceOspite = AnagraficaComune.CodiceOspite And Data < ? And CENTROSERVIZIO = '" & DD_CentroServizio.SelectedValue & "' And TipoMov = '05') <>  0) " & _
                                                " )" & _
                                                " ) order by Nome"


        cmd.CommandText = MySql
        Dim Txt_DataDalText As Date = DateSerial(Txt_Anno.Text, Dd_Mese.SelectedValue, 1)
        Dim Txt_DataAlText As Date = DateSerial(Txt_Anno.Text, Dd_Mese.SelectedValue, GiorniMese(Dd_Mese.SelectedValue, Txt_Anno.Text))

        cmd.Parameters.AddWithValue("@DataDal", Txt_DataDalText)
        cmd.Parameters.AddWithValue("@DataAl", Txt_DataAlText)
        cmd.Parameters.AddWithValue("@DataDal", Txt_DataDalText)
        cmd.Parameters.AddWithValue("@DataDal", Txt_DataDalText)
        cmd.Connection = cn
        Dim myPOSTreader As OleDbDataReader = cmd.ExecuteReader()
        Do While myPOSTreader.Read




            Dim CodiceOSpite As Integer = 0
            Dim XAna As New ClsOspite


            CodiceOSpite = Val(campodb(myPOSTreader.Item("CodiceOspite")))

            XAna.Leggi(Session("DC_OSPITE"), CodiceOSpite)


            Dim UltRetta As New Cls_rettatotale

            UltRetta.CODICEOSPITE = CodiceOSpite
            UltRetta.CENTROSERVIZIO = DD_CentroServizio.SelectedValue
            UltRetta.UltimaData(Session("DC_OSPITE"), UltRetta.CODICEOSPITE, UltRetta.CENTROSERVIZIO)

            Dim Extra As New Cls_ExtraFisso

            Extra.CODICEOSPITE = UltRetta.CODICEOSPITE
            Extra.CENTROSERVIZIO = UltRetta.CENTROSERVIZIO
            Extra.Data = UltRetta.Data
            Extra.UltimaDataAData(Session("DC_OSPITE"))
            If Extra.Data = UltRetta.Data Then
                Dim Pre As New Cls_TipoExtraFisso

                Pre.CODICEEXTRA = Extra.CODICEEXTRA
                Pre.Leggi(Session("DC_OSPITE"))

                If Pre.TipoImporto = "M" And Pre.Ripartizione = DD_Tipo.SelectedValue Then
                    Dim myrigaExt As System.Data.DataRow = Tabella.NewRow()

                    myrigaExt(0) = CodiceOSpite
                    myrigaExt(1) = XAna.Nome
                    myrigaExt(2) = ""
                    myrigaExt(3) = "E"
                    If Extra.RIPARTIZIONE = "O" Then
                        myrigaExt(4) = "OSPITE"
                        myrigaExt(5) = ""
                    End If
                    If Extra.RIPARTIZIONE = "P" Then
                        myrigaExt(4) = "PARENTE"

                        Dim Par1 As New Cls_ImportoParente

                        Par1.CODICEOSPITE = CodiceOSpite
                        Par1.CENTROSERVIZIO = UltRetta.CENTROSERVIZIO
                        Par1.CODICEPARENTE = 1
                        Par1.UltimaData(Session("DC_OSPITE"), Par1.CODICEOSPITE, UltRetta.CENTROSERVIZIO, Par1.CODICEPARENTE)
                        If Par1.IMPORTO > 0 Or Par1.PERCENTUALE > 0 Then
                            myrigaExt(5) = 1
                        Else
                            Par1.CODICEOSPITE = CodiceOSpite
                            Par1.CENTROSERVIZIO = UltRetta.CENTROSERVIZIO
                            Par1.CODICEPARENTE = 2
                            Par1.UltimaData(Session("DC_OSPITE"), Par1.CODICEOSPITE, UltRetta.CENTROSERVIZIO, Par1.CODICEPARENTE)
                            If Par1.IMPORTO > 0 Or Par1.PERCENTUALE > 0 Then
                                myrigaExt(5) = 2
                            Else
                                Par1.CODICEOSPITE = CodiceOSpite
                                Par1.CENTROSERVIZIO = UltRetta.CENTROSERVIZIO
                                Par1.CODICEPARENTE = 3
                                Par1.UltimaData(Session("DC_OSPITE"), Par1.CODICEOSPITE, UltRetta.CENTROSERVIZIO, Par1.CODICEPARENTE)
                                If Par1.IMPORTO > 0 Or Par1.PERCENTUALE > 0 Then
                                    myrigaExt(5) = 3
                                End If
                            End If
                        End If
                    End If


                    myrigaExt(6) = Modulo.MathRound(Pre.IMPORTO, 2)
                    myrigaExt(7) = ""
                    myrigaExt(8) = Dd_Mese.SelectedValue & "/" & Txt_Anno.Text
                    myrigaExt(9) = Pre.Descrizione
                    myrigaExt(10) = Pre.CODICEEXTRA
                    myrigaExt(11) = 0


                    Tabella.Rows.Add(myrigaExt)
                End If

            End If


            If DD_Tipo.SelectedValue = "O" Then
                MySql = "Select * From ADDACR Where Data >= ? And Data <= ? And CodiceOspite = ? And CentroServizio = ? And (Elaborato = 0 or Elaborato  is null) And Riferimento = '" & DD_Tipo.SelectedValue & "' "
            End If
            If DD_Tipo.SelectedValue = "P" Then
                MySql = "Select * From ADDACR Where Data >= ? And Data <= ? And CodiceOspite = ? And CentroServizio = ? And (Elaborato = 0 or Elaborato  is null) And Riferimento = '" & DD_Tipo.SelectedValue & "' "
            End If


            Dim cmdRd As New OleDbCommand()
            cmdRd.CommandText = MySql
            cmdRd.Connection = cn
            cmdRd.Parameters.AddWithValue("@DATA1", DateSerial(Txt_Anno.Text, Dd_Mese.SelectedValue, 1))
            cmdRd.Parameters.AddWithValue("@DATA2", DateSerial(Txt_Anno.Text, Dd_Mese.SelectedValue, GiorniMese(Dd_Mese.SelectedValue, Txt_Anno.Text)))
            cmdRd.Parameters.AddWithValue("@CodiceOspite", CodiceOSpite)
            cmdRd.Parameters.AddWithValue("@CENTROSERVIZIO", DD_CentroServizio.SelectedValue)


            Dim myRead As OleDbDataReader = cmdRd.ExecuteReader()
            Do While myRead.Read



                Dim myriga As System.Data.DataRow = Tabella.NewRow()

                myriga(0) = CodiceOSpite
                myriga(1) = XAna.Nome
                myriga(2) = campodb(myRead.Item("DATA"))
                myriga(3) = campodb(myRead.Item("TIPOMOV"))


                TRig = ""
                Fig = ""
                If campodb(myRead.Item("Riferimento")) = "O" Then
                    TRig = "OSPITE"
                    Fig = ""
                End If
                If campodb(myRead.Item("Riferimento")) = "P" Then
                    TRig = "PARENTE"
                    'Fig = CampoParente(MoveFromDb(MyRs!CodiceOspite), MoveFromDb(MyRs!Parente), "NOME")
                    Fig = Val(campodb(myRead.Item("Parente")))
                End If

                myriga(4) = TRig
                myriga(5) = Fig
                myriga(6) = campodb(myRead.Item("Importo"))
                myriga(7) = campodb(myRead.Item("Retta"))
                myriga(8) = campodb(myRead.Item("MESECOMPETENZA")) & "/" & campodb(myRead.Item("ANNOCOMPETENZA"))
                myriga(9) = campodb(myRead.Item("Descrizione"))
                myriga(10) = campodb(myRead.Item("CodiceIVA"))
                myriga(11) = Val(campodb(myRead.Item("ID")))


                Tabella.Rows.Add(myriga)
            Loop

        Loop

        cn.Close()

        ViewState("SalvaTabella") = Tabella

        Grd_AggiornaRette.AutoGenerateColumns = False
        Grd_AggiornaRette.DataSource = Tabella
        Grd_AggiornaRette.Font.Size = 10
        Grd_AggiornaRette.DataBind()
        Call EseguiJS()

        lblFC.Visible = False
        lblFCDes.Visible = False
        DD_Ente.Visible = False


    End Sub

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Trim(Session("UTENTE")) = "" Then
            Response.Redirect("../Login.aspx")
            Exit Sub
        End If

        Session("CampoErrori") = ""

        If Page.IsPostBack = True Then
            Exit Sub
        End If



        Call EseguiJS()


        Dim K1 As New Cls_SqlString

        Dim Barra As New Cls_BarraSenior

        If Not IsNothing(Session("RicercaAnagraficaSQLString")) Then
            K1 = Session("RicercaAnagraficaSQLString")
        End If

        Lbl_BarraSenior.Text = Barra.CodiceBarra(Application("SENIOR"), Session("UTENTE"), Page, K1)

        Dim kVilla As New Cls_TabelleDescrittiveOspitiAccessori


        kVilla.UpDateDropBox(Session("DC_OSPITIACCESSORI"), "VIL", DD_Struttura)
        If DD_Struttura.Items.Count = 1 Then
            Call AggiornaCServ()
        End If


        Dim Pagam As New Cls_Parametri

        Pagam.LeggiParametri(Session("DC_OSPITE"))

        Txt_Anno.Text = Pagam.AnnoFatturazione
        Dd_Mese.SelectedValue = Pagam.MeseFatturazione

        Txt_DataRegistrazione.Text = Format(Now, "dd/MM/yyyy")

        Session("CampoErrori") = ""

        Dim Stato As String = Session("DC_GENERALE")

        If Stato.ToUpper.IndexOf("senior_SanGiuseppeMoscati_generale".ToUpper) < 0 Then
            Chk_Effettiva.VISIBLE = False
            Chk_Effettiva.checked = True
        End If


    End Sub


    Protected Sub Btn_Esci_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles Btn_Esci.Click
        Response.Redirect("Menu_Strumenti.aspx")
    End Sub




    Private Sub AggiornaCServ()
        Dim kCsrv As New Cls_CentroServizio

        If DD_Struttura.SelectedValue = "" Then
            kCsrv.UpDateDropBox(Session("DC_OSPITE"), DD_CentroServizio)
        Else
            kCsrv.UpDateDropBoxStruttura(Session("DC_OSPITE"), DD_CentroServizio, DD_Struttura.SelectedValue)
        End If
    End Sub



    Protected Sub DD_Struttura_TextChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles DD_Struttura.TextChanged
        AggiornaCServ()
        Call EseguiJS()
    End Sub

    Protected Sub Btn_Modifica0_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles Btn_Modifica0.Click
        If DD_CentroServizio.SelectedValue = "" Then
            ClientScript.RegisterClientScriptBlock(Me.GetType(), "Errore", "VisualizzaErrore('Specifica centroservizio');", True)
            Exit Sub
        End If

        If Val(Txt_Anno.Text) < 1990 Then
            ClientScript.RegisterClientScriptBlock(Me.GetType(), "Errore", "VisualizzaErrore('Specificare Anno');", True)
            Exit Sub
        End If

        If Not IsDate(Txt_DataRegistrazione.Text) Then
            ClientScript.RegisterClientScriptBlock(Me.GetType(), "Errore", "VisualizzaErrore('Data validita formalmente errata');", True)
            Exit Sub
        End If

        Tabella = ViewState("SalvaTabella")
        If IsNothing(Tabella) Then
            ClientScript.RegisterClientScriptBlock(Me.GetType(), "Errore", "VisualizzaErrore('Visualizzare elenco prima di effettuare la modifica');", True)
            Exit Sub
        End If

        If Tabella.Rows.Count = 0 Then
            ClientScript.RegisterClientScriptBlock(Me.GetType(), "Errore", "VisualizzaErrore('Visualizzare elenco prima di effettuare la modifica');", True)
            Exit Sub
        End If


        Dim VerificaElaborazione As New Cls_Parametri

        If Chk_Effettiva.checked = True Then
            If VerificaElaborazione.InElaborazione(Session("DC_OSPITE")) Then
                ClientScript.RegisterClientScriptBlock(Me.GetType(), "Errore", "VisualizzaErrore('Elaborazione in esecuzione non posso procedere');", True)
                Exit Sub
            End If
        End If


        Dim ParametriGenerale As New Cls_ParametriGenerale

        If ParametriGenerale.InElaborazione(Session("DC_GENERALE")) Then
            ClientScript.RegisterClientScriptBlock(Me.GetType(), "Errore", "alert('Elaborazione in esecuzione non posso procedere');", True)
            Exit Sub
        End If



        VerificaElaborazione.Elaborazione(Session("DC_OSPITE"))
        ParametriGenerale.Elaborazione(Session("DC_GENERALE"))



        Dim EmOsp As New Cls_EmissioneRetta

        EmOsp.ConnessioneGenerale = Session("DC_GENERALE")

        If Chk_Effettiva.checked = False Then
            EmOsp.ConnessioneGenerale = EmOsp.ConnessioneGenerale.ToUpper.Replace("senior_SanGiuseppeMoscati_generale".ToUpper, "senior_SanGiuseppeMoscati_generaleTemp")
        End If



        Dim SalvaSessio As String

        SalvaSessio = Session("DC_GENERALE")

        Try

            Session("DC_GENERALE") = EmOsp.ConnessioneGenerale


            If DD_Tipo.SelectedValue = "O" Then
                Call CreaDocOspiti()
            End If
            If DD_Tipo.SelectedValue = "P" Then
                Call CreaDocParenti()
            End If



            EmOsp.ConnessioneOspiti = Session("DC_OSPITE")
            EmOsp.ConnessioneTabelle = Session("DC_TABELLE")
            EmOsp.ApriDB()
            Dim cn As OleDbConnection


            cn = New Data.OleDb.OleDbConnection(EmOsp.ConnessioneGenerale)

            cn.Open()
            Dim cmd As New OleDbCommand()
            Dim RegistroIVA As Integer = 0

            cmd.CommandText = ("select * from Temp_MovimentiContabiliTesta Order by NumeroRegistrazione  ")
            cmd.Connection = cn
            Dim VerReader As OleDbDataReader = cmd.ExecuteReader()
            Do While VerReader.Read
                EmOsp.VerificaRegistrazioneIVA(campodb(VerReader.Item("NumeroRegistrazione")), False)
                RegistroIVA = Val(campodb(VerReader.Item("RegistroIVA")))
            Loop
            VerReader.Close()

            If Chk_FatturaDiAnticipo.Checked = True Then
                Dim cmdMod As New OleDbCommand()
                cmdMod.CommandText = ("UPDATE Temp_MovimentiContabiliTesta SET  FatturaDiAnticipo = 'S' ")
                cmdMod.Connection = cn
                cmdMod.ExecuteNonQuery()
            End If

            cn.Close()
            EmOsp.CloseDB()


            Dim Vettore(1000) As Integer
            Dim Max As Integer = 0
            Tabella = ViewState("SalvaTabella")

            For i = 0 To Tabella.Rows.Count - 1

                Dim CheckBoxV As CheckBox = DirectCast(Grd_AggiornaRette.Rows(i).FindControl("Chk_Seleziona"), CheckBox)


                If CheckBoxV.Checked = False Then
                    Vettore(Max) = Tabella.Rows(i).Item("Id")
                    Max = Max + 1
                End If
            Next

            Session("RIATTIVAADD") = Vettore

            If Chk_Effettiva.Checked = False Then
                Try
                    Dim cnOspite As New OleDbConnection
                    Dim VettoreAdac(300) As Integer
                    Dim I As Integer

                    Vettore = Session("RIATTIVAADD")
                    cnOspite.ConnectionString = Session("DC_OSPITE")

                    cnOspite.Open()

                    For I = 0 To 100
                        If Not IsNothing(VettoreAdac(I)) Then
                            If VettoreAdac(I) > 0 Then
                                Dim CmdUpdate As New OleDbCommand

                                CmdUpdate.Connection = cnOspite
                                CmdUpdate.CommandText = "UPDATE ADDACR SET ADDACR.NumeroRegistrazione =0,ADDACR.Elaborato = 0   WHERE DataModifica = {ts '" & Format(Now, "yyyy-MM-dd") & " 00:00:00'} AND ID = ?"
                                CmdUpdate.Parameters.AddWithValue("@ID", Vettore(I))
                                CmdUpdate.ExecuteNonQuery()
                            End If
                        End If
                    Next
                    cnOspite.Close()

                Catch ex As Exception

                End Try
            End If

            Dim StampaFatture As New Cls_StampaFatture



            StampaFatture.CreaRecordStampa(0, 0, False, 0, 0, 0, RegistroIVA, "", "", "", 1, Session, 0, 99999999)


            VerificaElaborazione.FineElaborazione(Session("DC_OSPITE"))

            If Chk_Effettiva.checked = False Then
                Session("SelectionFormula") = "{FatturaTesta.PRINTERKEY} = " & Chr(34) & Session.SessionID & Chr(34) & " AND " & "{FatturaRighe.PRINTERKEY} = " & Chr(34) & Session.SessionID & Chr(34)

                Session("stampa") = System.Web.HttpRuntime.Cache("stampa" + Session.SessionID)


                ClientScript.RegisterClientScriptBlock(Me.GetType(), "Stampa1", "openPopUp('Stampa_ReportXSD.aspx?REPORT=STAMPADOCUMENTIOSPITI1&EXPORT=PDF','Stampe1','width=800,height=600');", True)            
            End If
            Session("DC_GENERALE") = SalvaSessio
        Catch ex As Exception
            Session("DC_GENERALE") = SalvaSessio
        End Try

        If Chk_Effettiva.checked = True Then
            Response.Redirect("StampaFattureProva.aspx?TIPO=NONEMISSIONE&URL=FatturaAddExtra.aspx")
        End If
    End Sub

    Protected Sub Lbl_SelezionaTutti_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles Lbl_SelezionaTutti.Click
        Tabella = ViewState("SalvaTabella")
        If IsNothing(Tabella) Then
            ClientScript.RegisterClientScriptBlock(Me.GetType(), "Errore", "VisualizzaErrore('Visualizzare elenco prima di richiedere la selezione');", True)
            Exit Sub
        End If

        If Tabella.Rows.Count = 0 Then
            ClientScript.RegisterClientScriptBlock(Me.GetType(), "Errore", "VisualizzaErrore('Visualizzare elenco prima di richiedere la selezione');", True)
            Exit Sub
        End If


        For i = 0 To Tabella.Rows.Count - 1
            Dim CheckBoxV1 As CheckBox = DirectCast(Grd_AggiornaRette.Rows(i).FindControl("Chk_Seleziona"), CheckBox)


            CheckBoxV1.Checked = True
        Next
    End Sub

    Protected Sub Lbl_InvertiSelezione_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles Lbl_InvertiSelezione.Click
        Tabella = ViewState("SalvaTabella")
        If IsNothing(Tabella) Then
            ClientScript.RegisterClientScriptBlock(Me.GetType(), "Errore", "VisualizzaErrore('Visualizzare elenco prima di richiedere la selezione');", True)
            Exit Sub
        End If

        If Tabella.Rows.Count = 0 Then
            ClientScript.RegisterClientScriptBlock(Me.GetType(), "Errore", "VisualizzaErrore('Visualizzare elenco prima di richiedere la selezione');", True)
            Exit Sub
        End If

        For i = 0 To Tabella.Rows.Count - 1
            Dim CheckBoxV1 As CheckBox = DirectCast(Grd_AggiornaRette.Rows(i).FindControl("Chk_Seleziona"), CheckBox)

            If CheckBoxV1.Checked = True Then
                CheckBoxV1.Checked = False
            Else
                CheckBoxV1.Checked = True
            End If
        Next
    End Sub
    Private Sub CreaDocOspiti()
        Dim i As Integer
        Dim x As Integer
        Dim Centroservizio As String = ""
        Dim CodiceOspite As Long = 0
        Dim OldCodiceOspite As Long = 0
        Dim MaxAddebito As Long = 0
        Dim MaxAccredito As Long = 0
        Dim MaxExtra As Long = 0

        Dim CodiceIVA As String

        Dim BolloImpo As Double
        Dim Bollo As Integer
        Dim IVABollo As String


        Dim ImportoRetta As Double

        Dim ImportoExtra As Double

        Dim ImportoAddebiti As Double
        Dim ImportoAccrediti As Double
        Dim EM_Retta As New Cls_EmissioneRetta

        EM_Retta.ConnessioneGenerale = Session("DC_GENERALE")
        EM_Retta.ConnessioneOspiti = Session("DC_OSPITE")
        EM_Retta.ConnessioneTabelle = Session("DC_TABELLE")

        EM_Retta.ApriDB()

        EM_Retta.EliminaTemporanei()


        Tabella = ViewState("SalvaTabella")

        Dim Xk As New Cls_CentroServizio


        Xk.Leggi(Session("DC_OSPITE"), DD_CentroServizio.SelectedValue)
        OldCodiceOspite = 0
        CodiceOspite = Tabella.Rows(i).Item(0)
        For i = 0 To Tabella.Rows.Count - 1

            Centroservizio = DD_CentroServizio.SelectedValue
            CodiceOspite = Tabella.Rows(i).Item(0)
            Dim CheckBoxV As CheckBox = DirectCast(Grd_AggiornaRette.Rows(i).FindControl("Chk_Seleziona"), CheckBox)


            If CheckBoxV.Checked = True Then
                If CodiceOspite <> OldCodiceOspite And OldCodiceOspite <> 0 Then
                    Dim xs As New ClsOspite

                    xs.Leggi(Session("DC_OSPITE"), OldCodiceOspite)

                    Dim TipoCserv As New Cls_DatiOspiteParenteCentroServizio

                    TipoCserv.CentroServizio = DD_CentroServizio.SelectedValue
                    TipoCserv.CodiceOspite = xs.CodiceOspite
                    TipoCserv.Leggi(Session("DC_OSPITE"))
                    If TipoCserv.TipoOperazione <> "" Then
                        xs.TIPOOPERAZIONE = TipoCserv.TipoOperazione
                        xs.CODICEIVA = TipoCserv.AliquotaIva
                    End If


                    If xs.TIPOOPERAZIONE.Trim <> "" Then
                        Dim TipoOperasazione As New Cls_TipoOperazione

                        TipoOperasazione.Leggi(Session("DC_OSPITE"), xs.TIPOOPERAZIONE)
                        EM_Retta.GlbTipoOperazione = xs.TIPOOPERAZIONE

                        CodiceIVA = xs.CODICEIVA

                        Dim AliquotaIva As New Cls_IVA
                        Dim AliquotaBollo As New Cls_IVA

                        AliquotaIva.Leggi(Session("DC_TABELLE"), CodiceIVA)

                        If TipoOperasazione.SoggettaABollo = "" Or (TipoOperasazione.SoggettaABollo = "F" And ImportoRetta < 0) Then
                            BolloImpo = 0
                            Bollo = 0
                            IVABollo = ""
                        Else
                            Dim KBollo As New Cls_bolli

                            KBollo.Leggi(Session("DC_TABELLE"), Txt_DataRegistrazione.Text)
                            BolloImpo = KBollo.ImportoBollo
                            Bollo = KBollo.ImportoBollo
                            IVABollo = KBollo.CodiceIVA


                            AliquotaBollo.Leggi(Session("DC_TABELLE"), IVABollo)

                        End If


                        If ImportoRetta < 0 Then
                            EM_Retta.GlbTipoOperazione = TipoOperasazione.Codice
                            EM_Retta.CreaRegistrazioneContabile(Txt_Anno.Text, Dd_Mese.SelectedValue, DD_CentroServizio.SelectedValue, TipoOperasazione.CausaleStorno, Xk.MASTRO, Xk.CONTO, OldCodiceOspite * 100, 0, 0, ImportoExtra, 0, Math.Abs(ImportoAccrediti), BolloImpo, 0, AliquotaIva.Aliquota, CodiceIVA, AliquotaBollo.Aliquota, IVABollo, Txt_DataRegistrazione.Text, 0, 0, 0, 0, Dd_Mese.SelectedValue, Txt_Anno.Text, TipoOperasazione.Codice, 0, 0, Math.Abs(ImportoAddebiti), 0, 0, 0, 0, 0, 1)
                        Else
                            EM_Retta.GlbTipoOperazione = TipoOperasazione.Codice
                            EM_Retta.CreaRegistrazioneContabile(Txt_Anno.Text, Dd_Mese.SelectedValue, DD_CentroServizio.SelectedValue, TipoOperasazione.CausaleAddebito, Xk.MASTRO, Xk.CONTO, OldCodiceOspite * 100, 0, 0, ImportoExtra, 0, Math.Abs(ImportoAccrediti), BolloImpo, 0, AliquotaIva.Aliquota, CodiceIVA, AliquotaBollo.Aliquota, IVABollo, Txt_DataRegistrazione.Text, 0, 0, 0, 0, Dd_Mese.SelectedValue, Txt_Anno.Text, TipoOperasazione.Codice, 0, 0, Math.Abs(ImportoAddebiti), 0, 0, 0, 0, 0, 1)
                        End If

                        ImportoRetta = 0
                        MaxAddebito = 0
                        MaxAccredito = 0
                        MaxExtra = 0
                        ImportoAddebiti = 0
                        ImportoAccrediti = 0

                        For x = 0 To 40
                            EM_Retta.Emr_ImpAddebito(x) = 0
                            EM_Retta.Emr_DesAddebito(x) = ""
                            EM_Retta.Emr_CodivaAddebito(x) = ""
                            EM_Retta.Emr_ImpAccredito(x) = 0
                            EM_Retta.Emr_DesAccredito(x) = ""
                            EM_Retta.Emr_CodivaAccredito(x) = ""

                            EM_Retta.Emr_ImpExtra(x) = 0
                            EM_Retta.Emr_DesExtra(x) = ""
                            EM_Retta.Emr_CodivaExtra(x) = ""
                            EM_Retta.Emr_CodiceExtra(x) = ""

                        Next
                    End If
                End If

                If Tabella.Rows(i).Item(3) = "E" Then
                    ImportoRetta = ImportoRetta + CDbl(Tabella.Rows(i).Item(6))
                    MaxExtra = MaxExtra + 1
                    EM_Retta.Emr_ImpExtra(MaxExtra) = CDbl(Tabella.Rows(i).Item(6))
                    EM_Retta.Emr_DesExtra(MaxExtra) = Tabella.Rows(i).Item(9)
                    EM_Retta.Emr_CodiceExtra(MaxExtra) = Tabella.Rows(i).Item(10)

                    Dim DecIVa As New Cls_IVA

                    DecIVa.Codice = Tabella.Rows(i).Item(10)
                    DecIVa.Leggi(Session("DC_TABELLE"), DecIVa.Codice)

                    EM_Retta.Emr_CodivaExtra(MaxExtra) = DecIVa.Aliquota
                    ImportoExtra = ImportoExtra + CDbl(Tabella.Rows(i).Item(6))
                Else
                    If Tabella.Rows(i).Item(3) = "AD" Then
                        ImportoRetta = ImportoRetta + CDbl(Tabella.Rows(i).Item(6))
                        MaxAddebito = MaxAddebito + 1
                        EM_Retta.Emr_ImpAddebito(MaxAddebito) = CDbl(Tabella.Rows(i).Item(6))
                        EM_Retta.Emr_DesAddebito(MaxAddebito) = Tabella.Rows(i).Item(9)
                        EM_Retta.Emr_CodivaAddebito(MaxAddebito) = Tabella.Rows(i).Item(10)
                        ImportoAddebiti = ImportoAddebiti + CDbl(Tabella.Rows(i).Item(6))
                    Else
                        ImportoRetta = ImportoRetta - CDbl(Tabella.Rows(i).Item(6))
                        MaxAccredito = MaxAccredito + 1
                        EM_Retta.Emr_ImpAccredito(MaxAccredito) = CDbl(Tabella.Rows(i).Item(6))
                        EM_Retta.Emr_DesAccredito(MaxAccredito) = Tabella.Rows(i).Item(9)
                        EM_Retta.Emr_CodivaAccredito(MaxAccredito) = Tabella.Rows(i).Item(10)

                        ImportoAccrediti = ImportoAccrediti + CDbl(Tabella.Rows(i).Item(6))
                    End If
                End If
                OldCodiceOspite = CodiceOspite
            End If
        Next

        If Math.Abs(ImportoRetta) > 0 Then
            Dim xs As New ClsOspite

            xs.Leggi(Session("DC_OSPITE"), OldCodiceOspite)

            Dim TipoCserv As New Cls_DatiOspiteParenteCentroServizio

            TipoCserv.CentroServizio = DD_CentroServizio.SelectedValue
            TipoCserv.CodiceOspite = xs.CodiceOspite
            TipoCserv.Leggi(Session("DC_OSPITE"))
            If TipoCserv.TipoOperazione <> "" Then
                xs.TIPOOPERAZIONE = TipoCserv.TipoOperazione
                xs.CODICEIVA = TipoCserv.AliquotaIva
            End If


            If xs.TIPOOPERAZIONE.Trim <> "" Then

                Dim KC2s As New Cls_DatiOspiteParenteCentroServizio

                KC2s.CentroServizio = DD_CentroServizio.SelectedValue
                KC2s.CodiceOspite = OldCodiceOspite
                KC2s.CodiceParente = 0
                KC2s.Leggi(Session("DC_OSPITE"))

                REM Assegna la tipologia operazione e l'aliquota prendendo quella relativa al cserv
                If KC2s.CodiceOspite <> 0 Then
                    xs.TIPOOPERAZIONE = KC2s.TipoOperazione
                    xs.CODICEIVA = KC2s.AliquotaIva
                    xs.FattAnticipata = KC2s.Anticipata
                    xs.MODALITAPAGAMENTO = KC2s.ModalitaPagamento
                    xs.Compensazione = KC2s.Compensazione
                    xs.SETTIMANA = KC2s.Settimana
                End If


                Dim TipoOperasazione As New Cls_TipoOperazione

                TipoOperasazione.Leggi(Session("DC_OSPITE"), xs.TIPOOPERAZIONE)


                CodiceIVA = xs.CODICEIVA

                Dim AliquotaIva As New Cls_IVA
                Dim AliquotaBollo As New Cls_IVA

                AliquotaIva.Leggi(Session("DC_TABELLE"), CodiceIVA)

                If TipoOperasazione.SoggettaABollo = "" Then
                    BolloImpo = 0
                    Bollo = 0
                    IVABollo = ""
                Else
                    Dim KBollo As New Cls_bolli

                    KBollo.Leggi(Session("DC_TABELLE"), Txt_DataRegistrazione.Text)
                    BolloImpo = KBollo.ImportoBollo
                    Bollo = KBollo.ImportoBollo
                    IVABollo = KBollo.CodiceIVA


                    AliquotaBollo.Leggi(Session("DC_TABELLE"), IVABollo)

                End If
                If ImportoRetta < 0 Then
                    EM_Retta.GlbTipoOperazione = TipoOperasazione.Codice
                    EM_Retta.CreaRegistrazioneContabile(Txt_Anno.Text, Dd_Mese.SelectedValue, DD_CentroServizio.SelectedValue, TipoOperasazione.CausaleStorno, Xk.MASTRO, Xk.CONTO, OldCodiceOspite * 100, 0, 0, ImportoExtra, 0, Math.Abs(ImportoAccrediti), BolloImpo, 0, AliquotaIva.Aliquota, CodiceIVA, AliquotaBollo.Aliquota, IVABollo, Txt_DataRegistrazione.Text, 0, 0, 0, 0, Dd_Mese.SelectedValue, Txt_Anno.Text, TipoOperasazione.Codice, 0, 0, Math.Abs(ImportoAddebiti), 0, 0, 0, 0, 0, 1)
                Else
                    EM_Retta.GlbTipoOperazione = TipoOperasazione.Codice
                    EM_Retta.CreaRegistrazioneContabile(Txt_Anno.Text, Dd_Mese.SelectedValue, DD_CentroServizio.SelectedValue, TipoOperasazione.CausaleAddebito, Xk.MASTRO, Xk.CONTO, OldCodiceOspite * 100, 0, 0, ImportoExtra, 0, Math.Abs(ImportoAccrediti), BolloImpo, 0, AliquotaIva.Aliquota, CodiceIVA, AliquotaBollo.Aliquota, IVABollo, Txt_DataRegistrazione.Text, 0, 0, 0, 0, Dd_Mese.SelectedValue, Txt_Anno.Text, TipoOperasazione.Codice, 0, 0, Math.Abs(ImportoAddebiti), 0, 0, 0, 0, 0, 1)
                End If

            End If
        End If

        EM_Retta.Commit()
        EM_Retta.CloseDB()
    End Sub


    Private Sub CreaDocParenti()
        Dim i As Integer
        Dim x As Integer
        Dim Centroservizio As String = ""
        Dim CodiceOspite As Long = 0
        Dim CodiceParente As Long = 0
        Dim OldCodiceParente As Long = 0
        Dim OldCodiceOspite As Long = 0
        Dim MaxAddebito As Long = 0
        Dim MaxAccredito As Long = 0
        Dim MaxExtra As Long = 0


        Dim ImportoAddebiti As Double = 0
        Dim ImportoAccrediti As Double = 0

        Dim CodiceIVA As String

        Dim BolloImpo As Double
        Dim Bollo As Integer
        Dim IVABollo As String


        Dim ImportoRetta As Double

        Dim ImportoExtra As Double

        Dim EM_Retta As New Cls_EmissioneRetta

        EM_Retta.ConnessioneGenerale = Session("DC_GENERALE")
        EM_Retta.ConnessioneOspiti = Session("DC_OSPITE")
        EM_Retta.ConnessioneTabelle = Session("DC_TABELLE")

        EM_Retta.ApriDB()

        EM_Retta.EliminaTemporanei()

        Tabella = ViewState("SalvaTabella")

        Dim Xk As New Cls_CentroServizio


        Xk.Leggi(Session("DC_OSPITE"), DD_CentroServizio.SelectedValue)

        CodiceOspite = Tabella.Rows(i).Item(0)
        For i = 0 To Tabella.Rows.Count - 1

            Centroservizio = DD_CentroServizio.SelectedValue
            CodiceOspite = Tabella.Rows(i).Item(0)
            CodiceParente = Tabella.Rows(i).Item(5)
            Dim CheckBoxV As CheckBox = DirectCast(Grd_AggiornaRette.Rows(i).FindControl("Chk_Seleziona"), CheckBox)


            If CheckBoxV.Checked = True Then
                If (CodiceParente <> OldCodiceParente Or CodiceOspite <> OldCodiceOspite) And OldCodiceOspite <> 0 Then
                    Dim xs As New Cls_Parenti

                    xs.Leggi(Session("DC_OSPITE"), OldCodiceOspite, OldCodiceParente)


                    Dim TipoCserv As New Cls_DatiOspiteParenteCentroServizio

                    TipoCserv.CentroServizio = DD_CentroServizio.SelectedValue
                    TipoCserv.CodiceOspite = xs.CodiceOspite
                    TipoCserv.CodiceParente = xs.CodiceParente
                    TipoCserv.Leggi(Session("DC_OSPITE"))
                    If TipoCserv.TipoOperazione <> "" Then
                        xs.TIPOOPERAZIONE = TipoCserv.TipoOperazione
                        xs.CODICEIVA = TipoCserv.AliquotaIva
                    End If

                    If xs.TIPOOPERAZIONE.Trim <> "" Then

                        Dim TipoOperasazione As New Cls_TipoOperazione


                        TipoOperasazione.Leggi(Session("DC_OSPITE"), xs.TIPOOPERAZIONE)

                        EM_Retta.GlbTipoOperazione = xs.TIPOOPERAZIONE

                        CodiceIVA = xs.CODICEIVA

                        Dim AliquotaIva As New Cls_IVA
                        Dim AliquotaBollo As New Cls_IVA

                        AliquotaIva.Leggi(Session("DC_TABELLE"), CodiceIVA)

                        If TipoOperasazione.SoggettaABollo = "" Or (TipoOperasazione.SoggettaABollo = "F" And ImportoRetta < 0) Then
                            BolloImpo = 0
                            Bollo = 0
                            IVABollo = ""
                        Else
                            Dim KBollo As New Cls_bolli

                            KBollo.Leggi(Session("DC_TABELLE"), Txt_DataRegistrazione.Text)
                            BolloImpo = KBollo.ImportoBollo
                            Bollo = KBollo.ImportoBollo
                            IVABollo = KBollo.CodiceIVA


                            AliquotaBollo.Leggi(Session("DC_TABELLE"), IVABollo)

                        End If


                        If ImportoRetta < 0 Then
                            EM_Retta.GlbTipoOperazione = TipoOperasazione.Codice
                            EM_Retta.CreaRegistrazioneContabile(Txt_Anno.Text, Dd_Mese.SelectedValue, DD_CentroServizio.SelectedValue, TipoOperasazione.CausaleStorno, Xk.MASTRO, Xk.CONTO, OldCodiceOspite * 100 + OldCodiceParente, 0, 0, ImportoExtra, 0, Math.Abs(ImportoRetta), BolloImpo, 0, AliquotaIva.Aliquota, CodiceIVA, AliquotaBollo.Aliquota, IVABollo, Txt_DataRegistrazione.Text, 0, 0, 0, 0, Dd_Mese.SelectedValue, Txt_Anno.Text, TipoOperasazione.Codice, 0, 0, 0, 0, 0, 0, 0, 0, 1)
                        Else
                            EM_Retta.GlbTipoOperazione = TipoOperasazione.Codice
                            EM_Retta.CreaRegistrazioneContabile(Txt_Anno.Text, Dd_Mese.SelectedValue, DD_CentroServizio.SelectedValue, TipoOperasazione.CausaleAddebito, Xk.MASTRO, Xk.CONTO, OldCodiceOspite * 100 + OldCodiceParente, 0, 0, ImportoExtra, 0, 0, BolloImpo, 0, AliquotaIva.Aliquota, CodiceIVA, AliquotaBollo.Aliquota, IVABollo, Txt_DataRegistrazione.Text, 0, 0, 0, 0, Dd_Mese.SelectedValue, Txt_Anno.Text, TipoOperasazione.Codice, 0, 0, ImportoRetta, 0, 0, 0, 0, 0, 1)
                        End If

                        ImportoRetta = 0
                        ImportoExtra = 0
                        MaxAddebito = 0
                        MaxAccredito = 0
                        MaxExtra = 0

                        For x = 0 To 40
                            EM_Retta.Emr_ImpAddebito(x) = 0
                            EM_Retta.Emr_DesAddebito(x) = ""
                            EM_Retta.Emr_CodivaAddebito(x) = ""
                            EM_Retta.Emr_ImpAccredito(x) = 0
                            EM_Retta.Emr_DesAccredito(x) = ""
                            EM_Retta.Emr_CodivaAccredito(x) = ""
                            EM_Retta.Emr_ImpExtra(x) = 0
                            EM_Retta.Emr_DesExtra(x) = ""
                            EM_Retta.Emr_CodivaExtra(x) = ""
                            EM_Retta.Emr_CodiceExtra(x) = ""

                        Next
                    End If
                End If

                If Tabella.Rows(i).Item(3) = "E" Then
                    ImportoRetta = ImportoRetta + CDbl(Tabella.Rows(i).Item(6))
                    MaxExtra = MaxExtra + 1
                    EM_Retta.Emr_ImpExtra(MaxExtra) = CDbl(Tabella.Rows(i).Item(6))
                    EM_Retta.Emr_DesExtra(MaxExtra) = Tabella.Rows(i).Item(9)
                    EM_Retta.Emr_CodiceExtra(MaxExtra) = Tabella.Rows(i).Item(10)

                    Dim DecIVa As New Cls_IVA

                    DecIVa.Codice = Tabella.Rows(i).Item(10)
                    DecIVa.Leggi(Session("DC_TABELLE"), DecIVa.Codice)

                    EM_Retta.Emr_CodivaExtra(MaxExtra) = DecIVa.Aliquota
                    ImportoExtra = ImportoExtra + CDbl(Tabella.Rows(i).Item(6))
                Else
                    If Tabella.Rows(i).Item(3) = "AD" Then
                        ImportoAddebiti = ImportoAddebiti + CDbl(Tabella.Rows(i).Item(6))
                        ImportoRetta = ImportoRetta + CDbl(Tabella.Rows(i).Item(6))
                        MaxAddebito = MaxAddebito + 1
                        EM_Retta.Emr_ImpAddebito(MaxAddebito) = CDbl(Tabella.Rows(i).Item(6))
                        EM_Retta.Emr_DesAddebito(MaxAddebito) = Tabella.Rows(i).Item(9)
                        EM_Retta.Emr_CodivaAddebito(MaxAddebito) = Tabella.Rows(i).Item(10)
                    Else
                        ImportoAccrediti = ImportoAccrediti + CDbl(Tabella.Rows(i).Item(6))
                        ImportoRetta = ImportoRetta - CDbl(Tabella.Rows(i).Item(6))
                        MaxAccredito = MaxAccredito + 1
                        EM_Retta.Emr_ImpAccredito(MaxAccredito) = CDbl(Tabella.Rows(i).Item(6))
                        EM_Retta.Emr_DesAccredito(MaxAccredito) = Tabella.Rows(i).Item(9)
                        EM_Retta.Emr_CodivaAccredito(MaxAccredito) = Tabella.Rows(i).Item(10)
                    End If
                End If

                OldCodiceOspite = CodiceOspite
                OldCodiceParente = CodiceParente
            End If


        Next

        If Math.Abs(ImportoRetta) > 0 Then
            Dim xs As New Cls_Parenti

            xs.Leggi(Session("DC_OSPITE"), OldCodiceOspite, OldCodiceParente)

            Dim TipoCserv As New Cls_DatiOspiteParenteCentroServizio

            TipoCserv.CentroServizio = DD_CentroServizio.SelectedValue
            TipoCserv.CodiceOspite = xs.CodiceOspite
            TipoCserv.CodiceParente = xs.CodiceParente
            TipoCserv.Leggi(Session("DC_OSPITE"))
            If TipoCserv.TipoOperazione <> "" Then
                xs.TIPOOPERAZIONE = TipoCserv.TipoOperazione
                xs.CODICEIVA = TipoCserv.AliquotaIva
            End If


            If xs.TIPOOPERAZIONE.Trim <> "" Then
                Dim TipoOperasazione As New Cls_TipoOperazione

                TipoOperasazione.Leggi(Session("DC_OSPITE"), xs.TIPOOPERAZIONE)


                CodiceIVA = xs.CODICEIVA

                Dim AliquotaIva As New Cls_IVA
                Dim AliquotaBollo As New Cls_IVA

                AliquotaIva.Leggi(Session("DC_TABELLE"), CodiceIVA)

                If TipoOperasazione.SoggettaABollo = "" Then
                    BolloImpo = 0
                    Bollo = 0
                    IVABollo = ""
                Else
                    Dim KBollo As New Cls_bolli

                    KBollo.Leggi(Session("DC_TABELLE"), Txt_DataRegistrazione.Text)
                    BolloImpo = KBollo.ImportoBollo
                    Bollo = KBollo.ImportoBollo
                    IVABollo = KBollo.CodiceIVA


                    AliquotaBollo.Leggi(Session("DC_TABELLE"), IVABollo)

                End If
                If ImportoRetta < 0 Then
                    EM_Retta.GlbTipoOperazione = TipoOperasazione.Codice
                    EM_Retta.CreaRegistrazioneContabile(Txt_Anno.Text, Dd_Mese.SelectedValue, DD_CentroServizio.SelectedValue, TipoOperasazione.CausaleStorno, Xk.MASTRO, Xk.CONTO, OldCodiceOspite * 100 + OldCodiceParente, 0, 0, ImportoExtra, 0, Math.Abs(ImportoRetta), BolloImpo, 0, AliquotaIva.Aliquota, CodiceIVA, AliquotaBollo.Aliquota, IVABollo, Txt_DataRegistrazione.Text, 0, 0, 0, 0, Dd_Mese.SelectedValue, Txt_Anno.Text, TipoOperasazione.Codice, 0, 0, 0, 0, 0, 0, 0, 0, 1)
                Else
                    EM_Retta.GlbTipoOperazione = TipoOperasazione.Codice
                    EM_Retta.CreaRegistrazioneContabile(Txt_Anno.Text, Dd_Mese.SelectedValue, DD_CentroServizio.SelectedValue, TipoOperasazione.CausaleAddebito, Xk.MASTRO, Xk.CONTO, OldCodiceOspite * 100 + OldCodiceParente, 0, 0, ImportoExtra, 0, 0, BolloImpo, 0, AliquotaIva.Aliquota, CodiceIVA, AliquotaBollo.Aliquota, IVABollo, Txt_DataRegistrazione.Text, 0, 0, 0, 0, Dd_Mese.SelectedValue, Txt_Anno.Text, TipoOperasazione.Codice, 0, 0, ImportoRetta, 0, 0, 0, 0, 0, 1)
                End If
            End If

        End If

        EM_Retta.Commit()
        EM_Retta.CloseDB()
    End Sub

End Class
