﻿Imports Microsoft.VisualBasic
Imports System.Data.OleDb
Imports System.Web.Hosting
Imports System.Data.SqlTypes

Partial Class OspitiWeb_Elenco_Listino
    Inherits System.Web.UI.Page
    Dim Tabella As New System.Data.DataTable("tabella")

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If IsNothing(Session("UTENTE")) Then
            Response.Redirect("..\Login.aspx")
            Exit Sub
        End If

        If Page.IsPostBack = True Then
            Exit Sub
        End If

        Dim Servizio As New Cls_CentroServizio

        Servizio.UpDateDropBox(Session("DC_OSPITE"), DD_Cserv)


        Dim K1 As New Cls_SqlString

        Dim Barra As New Cls_BarraSenior

        If Not IsNothing(Session("RicercaListinoSQLString")) Then
            Try
                K1 = Session("RicercaListinoSQLString")

                Txt_Descrizione.Text = K1.GetValue("Txt_Descrizione")
                RB_SoloPriv.Checked = K1.GetValue("RB_SoloPriv")
                DD_Cserv.SelectedValue = K1.GetValue("DD_Cserv")
                RB_SoloCon.Checked = K1.GetValue("RB_SoloCon")
            Catch ex As Exception

            End Try


        End If

        Lbl_BarraSenior.Text = Barra.CodiceBarra(Application("SENIOR"), Session("UTENTE"), Page, K1)

        CaricaExtra()
    End Sub

    Private Sub CaricaExtra()

        Dim f As New Cls_CausaliEntrataUscita

        Dim cn As New OleDbConnection


        Dim ConnectionString As String = Session("DC_OSPITE")

        cn = New Data.OleDb.OleDbConnection(ConnectionString)

        cn.Open()

        Dim k1 As New Cls_SqlString



        k1.Add("Txt_Descrizione", Txt_Descrizione.Text)

        k1.Add("RB_SoloPriv", RB_SoloPriv.Checked)    
        k1.Add("RB_SoloCon", RB_SoloCon.Checked)
        k1.Add("DD_Cserv", DD_Cserv.SelectedValue)

        Session("RicercaListinoSQLString") = k1

        Dim cmd As New OleDbCommand
        Dim Condizione As String = ""
        If Txt_Descrizione.Text <> "" Then
            Condizione = " Descrizione like '" & Txt_Descrizione.Text.Replace(Chr(34), "") & "'"
        End If
        If RB_SoloPriv.Checked = True Then
            If Condizione = "" Then
                Condizione = Condizione & " And "
            End If
            Condizione = " (USL is null or USL = '') "
        End If
        If RB_SoloCon.Checked = True Then
            If Condizione = "" Then
                Condizione = Condizione & " And "
            End If
            Condizione = " Not (USL is null or USL = '') "
        End If

        If DD_Cserv.SelectedValue = "" Then

            If Condizione = "" Then
                cmd.CommandText = "Select * From Tabella_Listino Order By Descrizione"
            Else
                cmd.CommandText = "Select * From Tabella_Listino Where " & Condizione & " Order By Descrizione"
            End If
        Else
            If Condizione = "" Then
                cmd.CommandText = "Select * From Tabella_Listino Where CENTROSERVIZIO = ?  Order By Descrizione"
                cmd.Parameters.AddWithValue("@CENTROSERVIZIO", DD_Cserv.SelectedValue)
            Else
                cmd.CommandText = "Select * From Tabella_Listino Where " & Condizione & " And CENTROSERVIZIO = ?  Order By Descrizione"
                cmd.Parameters.AddWithValue("@CENTROSERVIZIO", DD_Cserv.SelectedValue)
            End If
        End If
        cmd.Connection = cn



        Tabella.Clear()
        Tabella.Columns.Add("Codice", GetType(String))
        Tabella.Columns.Add("Tipo", GetType(String))
        Tabella.Columns.Add("Struttura/Servizio", GetType(String))

        Tabella.Columns.Add("Descrizione", GetType(String))
        Tabella.Columns.Add("Retta Totale", GetType(String))
        Tabella.Columns.Add("Extra", GetType(String))
        Tabella.Columns.Add("Data Scadenza", GetType(String))


        Dim myPOSTreader As OleDbDataReader = cmd.ExecuteReader()
        Do While myPOSTreader.Read
            Dim myriga As System.Data.DataRow = Tabella.NewRow()

            myriga(0) = myPOSTreader.Item("Codice")

            myriga(1) = myPOSTreader.Item("TIPOLISTINO")


            Dim Cserv As New Cls_CentroServizio

            Cserv.CENTROSERVIZIO = campodb(myPOSTreader.Item("CENTROSERVIZIO"))
            Cserv.Leggi(Session("DC_OSPITE"), Cserv.CENTROSERVIZIO)


            Dim kVilla As New Cls_TabelleDescrittiveOspitiAccessori

            kVilla.TipoTabella = "VIL"
            kVilla.CodiceTabella = campodb(myPOSTreader.Item("struttura"))
            kVilla.Leggi(Session("DC_OSPITIACCESSORI"))

            If kVilla.Descrizione <> "" Then
                myriga(2) = kVilla.Descrizione
                If Cserv.DESCRIZIONE <> "" Then
                    myriga(2) = kVilla.Descrizione & " - " & Cserv.DESCRIZIONE
                End If
            Else
                myriga(2) = Cserv.DESCRIZIONE
            End If


            myriga(3) = campodb(myPOSTreader.Item("Descrizione"))
            myriga(4) = campodb(myPOSTreader.Item("IMPORTORETTATOTALE"))

            Dim Descrizione As String = ""

            For i = 1 To 10
                If campodb(myPOSTreader.Item("CODICEEXTRA" & i)) <> "" Then
                    Dim DecodificaExtra As New Cls_TipoExtraFisso

                    DecodificaExtra.Descrizione = ""
                    DecodificaExtra.CODICEEXTRA = campodb(myPOSTreader.Item("CODICEEXTRA" & i))
                    DecodificaExtra.Leggi(Session("DC_OSPITE"))
                    If Descrizione <> "" And DecodificaExtra.Descrizione <> "" Then
                        Descrizione = Descrizione & ", "
                    End If
                    Descrizione = Descrizione & DecodificaExtra.Descrizione & "(" & Format(DecodificaExtra.IMPORTO, "#,###0.00") & ")"
                End If
            Next

            myriga(5) = Descrizione

            If campodb(myPOSTreader.Item("DATA")) <> "" Then
                myriga(6) = Format(campodbd(myPOSTreader.Item("DATA")), "dd/MM/yyyy")
            End If


            Tabella.Rows.Add(myriga)
        Loop

        myPOSTreader.Close()
        cn.Close()


        DD_PrivatoSanitario.Visible = False
        Dim Param As New Cls_Parametri

        Param.LeggiParametri(Session("DC_OSPITE"))

        If Param.SocialeSanitario = 1 Then
            DD_PrivatoSanitario.Visible = True
        End If
        If Param.AlberghieroAssistenziale = 1 Then
            DD_PrivatoSanitario.Visible = True
            DD_PrivatoSanitario.Text = "Assistenziale Privato"
        End If

        ViewState("Appoggio") = Tabella

        Grd_ImportoOspite.AutoGenerateColumns = True

        Grd_ImportoOspite.DataSource = Tabella

        Grd_ImportoOspite.DataBind()

        Btn_Nuovo.Visible = True
    End Sub

    Protected Sub Grd_ImportoOspite_PageIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles Grd_ImportoOspite.PageIndexChanged

    End Sub

    Protected Sub Grd_ImportoOspite_PageIndexChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewPageEventArgs) Handles Grd_ImportoOspite.PageIndexChanging
        Tabella = ViewState("Appoggio")
        Grd_ImportoOspite.PageIndex = e.NewPageIndex
        Grd_ImportoOspite.DataSource = Tabella
        Grd_ImportoOspite.DataBind()
    End Sub

    Protected Sub Grd_ImportoOspite_RowCommand(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewCommandEventArgs) Handles Grd_ImportoOspite.RowCommand

        If e.CommandName = "Seleziona" Then


            Dim d As Integer


            Tabella = ViewState("Appoggio")

            d = Val(e.CommandArgument)

            Dim Codice As String
            Dim TIPO As String

            Codice = Tabella.Rows(d).Item(0).ToString
            TIPO = Tabella.Rows(d).Item(1).ToString

            Response.Redirect("GestioneListino.aspx?CODICE=" & Codice & "&TIPOLISTINO=" & TIPO)
        End If
    End Sub

    Protected Sub Grd_ImportoOspite_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles Grd_ImportoOspite.SelectedIndexChanged

    End Sub

    Protected Sub Btn_Esci_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles Btn_Esci.Click
        Response.Redirect("Menu_Tabelle.aspx")
    End Sub


    Protected Sub Btn_Nuovo_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles Btn_Nuovo.Click
        If DD_Componenti.Checked = True Then
            Response.Redirect("GestioneListino.aspx")
        End If

        If DD_SoloSociale.Checked = True Then
            Response.Redirect("GestioneListino.aspx?TIPOLISTINO=S")
        End If


        If DD_PrivatoSanitario.Checked = True Then
            Response.Redirect("GestioneListino.aspx?TIPOLISTINO=P")
        End If


        If DD_Sanitorio.Checked = True Then
            Response.Redirect("GestioneListino.aspx?TIPOLISTINO=R")
        End If


        If DD_Comune.Checked = True Then
            Response.Redirect("GestioneListino.aspx?TIPOLISTINO=C")
        End If

        If DD_Jolly.Checked = True Then
            Response.Redirect("GestioneListino.aspx?TIPOLISTINO=J")
        End If

    End Sub

    Protected Sub Lnk_ToExcel_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles Lnk_ToExcel.Click
        Dim cn As New OleDbConnection


        Dim ConnectionString As String = Session("DC_OSPITE")

        cn = New Data.OleDb.OleDbConnection(ConnectionString)

        cn.Open()

        Dim cmd As New OleDbCommand




        cmd.CommandText = "Select Top 100 * From [Tabella_Listino] Order By Codice"


        cmd.Connection = cn
        cmd.Connection = cn

        Tabella.Clear()
        Tabella.Columns.Add("Codice")
        Tabella.Columns.Add("DataScadenza")        
        Tabella.Columns.Add("Descrizione")
        Tabella.Columns.Add("IMPORTORETTATOTALE")
        Tabella.Columns.Add("TIPORETTATOTALE")
        Tabella.Columns.Add("EXTRA FISSI")

        Tabella.Columns.Add("TIPOOPERAZIONE")

        Tabella.Columns.Add("CODICEIVA")
        Tabella.Columns.Add("MODALITAPAGAMENTO")
        Tabella.Columns.Add("COMPENSAZIONE")
        Tabella.Columns.Add("ANTICIPATA")
        Tabella.Columns.Add("IMPORTORETTTAOSPITE1")
        Tabella.Columns.Add("IMPORTORETTTAOSPITE2")
        Tabella.Columns.Add("TIPORETTTAOSPITE")
        Tabella.Columns.Add("USL")
        Tabella.Columns.Add("TIPORETTAUSL")
        Tabella.Columns.Add("SOCIALECOMUNE")
        Tabella.Columns.Add("SOCIALEPROVINCIA")
        Tabella.Columns.Add("IMPORTORETTTASOCIALE")
        Tabella.Columns.Add("TIPORETTASOCIALE")
        Tabella.Columns.Add("JOLLYCOMUNE")
        Tabella.Columns.Add("JOLLYPROVINCIA")
        Tabella.Columns.Add("IMPORTORETTTAJOLLY")
        Tabella.Columns.Add("TIPORETTAJOLLY")
        Tabella.Columns.Add("TIPORETTA")
        Tabella.Columns.Add("TIPOLISTINO")
        Tabella.Columns.Add("CENTROSERVIZIO")
        Tabella.Columns.Add("Struttura")

        Dim myPOSTreader As OleDbDataReader = cmd.ExecuteReader()
        Do While myPOSTreader.Read
            Dim myriga As System.Data.DataRow = Tabella.NewRow()


            myriga(0) = campodb(myPOSTreader.Item("Codice"))
            myriga(1) = campodb(myPOSTreader.Item("DATA"))
            myriga(2) = campodb(myPOSTreader.Item("Descrizione"))


            myriga(3) = campodb(myPOSTreader.Item("IMPORTORETTATOTALE"))
            myriga(4) = campodb(myPOSTreader.Item("TIPORETTATOTALE"))

            Dim ExtraFisso As New Cls_TipoExtraFisso
            Dim Decodifica As String = ""

            If campodb(myPOSTreader.Item("CODICEEXTRA1")) <> "" Then
                ExtraFisso.CODICEEXTRA = campodb(myPOSTreader.Item("CODICEEXTRA1"))
                ExtraFisso.Leggi(Session("DC_OSPITE"))
                Decodifica = ExtraFisso.Descrizione
            End If

            If campodb(myPOSTreader.Item("CODICEEXTRA2")) <> "" Then
                ExtraFisso.CODICEEXTRA = campodb(myPOSTreader.Item("CODICEEXTRA2"))
                ExtraFisso.Leggi(Session("DC_OSPITE"))
                Decodifica = Decodifica & ExtraFisso.Descrizione
            End If

            If campodb(myPOSTreader.Item("CODICEEXTRA3")) <> "" Then
                ExtraFisso.CODICEEXTRA = campodb(myPOSTreader.Item("CODICEEXTRA3"))
                ExtraFisso.Leggi(Session("DC_OSPITE"))
                Decodifica = Decodifica & ExtraFisso.Descrizione
            End If

            If campodb(myPOSTreader.Item("CODICEEXTRA4")) <> "" Then
                ExtraFisso.CODICEEXTRA = campodb(myPOSTreader.Item("CODICEEXTRA4"))
                ExtraFisso.Leggi(Session("DC_OSPITE"))
                Decodifica = Decodifica & ExtraFisso.Descrizione
            End If


            myriga(5) = Decodifica

            Dim TipoOpe As New Cls_TipoOperazione


            TipoOpe.Codice = campodb(myPOSTreader.Item("TIPOOPERAIZONE"))
            TipoOpe.Leggi(Session("DC_OSPITE"), TipoOpe.Codice)

            myriga(6) = TipoOpe.Descrizione


            Dim CodIva As New Cls_IVA

            CodIva.Codice = campodb(myPOSTreader.Item("CODICEIVA"))
            CodIva.Leggi(Session("DC_TABELLE"), CodIva.Codice)


            myriga(7) = CodIva.Descrizione

            myriga(8) = campodb(myPOSTreader.Item("MODALITAPAGAMENTO"))
            myriga(9) = campodb(myPOSTreader.Item("COMPENSAZIONE"))
            myriga(10) = campodb(myPOSTreader.Item("ANTICIPATA"))
            myriga(11) = campodb(myPOSTreader.Item("IMPORTORETTTAOSPITE1"))
            myriga(12) = campodb(myPOSTreader.Item("IMPORTORETTTAOSPITE2"))
            myriga(13) = campodb(myPOSTreader.Item("TIPORETTTAOSPITE"))

            Dim Regione As New ClsUSL

            Regione.CodiceRegione = campodb(myPOSTreader.Item("USL"))
            Regione.Leggi(Session("DC_OSPITE"))

            myriga(14) = Regione.Nome
            myriga(15) = campodb(myPOSTreader.Item("TIPORETTAUSL"))
            myriga(16) = campodb(myPOSTreader.Item("SOCIALECOMUNE"))
            myriga(17) = campodb(myPOSTreader.Item("SOCIALEPROVINCIA"))
            myriga(18) = campodb(myPOSTreader.Item("IMPORTORETTTASOCIALE"))
            myriga(19) = campodb(myPOSTreader.Item("TIPORETTASOCIALE"))
            myriga(20) = campodb(myPOSTreader.Item("JOLLYCOMUNE"))
            myriga(21) = campodb(myPOSTreader.Item("JOLLYPROVINCIA"))
            myriga(22) = campodb(myPOSTreader.Item("IMPORTORETTTAJOLLY"))
            myriga(23) = campodb(myPOSTreader.Item("TIPORETTAJOLLY"))
            myriga(24) = campodb(myPOSTreader.Item("TIPORETTA"))
            myriga(25) = campodb(myPOSTreader.Item("TIPOLISTINO"))

            Dim Cserv As New Cls_CentroServizio

            Cserv.CENTROSERVIZIO = campodb(myPOSTreader.Item("CENTROSERVIZIO"))
            Cserv.Leggi(Session("DC_OSPITE"), Cserv.CENTROSERVIZIO)

            myriga(26) = Cserv.DESCRIZIONE


            Dim kVilla As New Cls_TabelleDescrittiveOspitiAccessori

            kVilla.TipoTabella = "VIL"
            kVilla.CodiceTabella = campodb(myPOSTreader.Item("struttura"))
            kVilla.Leggi(Session("DC_OSPITIACCESSORI"))

            myriga(27) = kVilla.Descrizione


            Tabella.Rows.Add(myriga)
        Loop

        myPOSTreader.Close()
        cn.Close()


        Session("GrigliaSoloStampa") = Tabella
        ScriptManager.RegisterClientScriptBlock(Me, Me.GetType(), "Stampa", "openPopUp('ExportExcel.aspx','Excel','width=800,height=600');", True)


    End Sub

    Function campodb(ByVal oggetto As Object) As String
        If IsDBNull(oggetto) Then
            Return ""
        Else
            Return oggetto
        End If
    End Function


    Function campodbd(ByVal oggetto As Object) As Date
        If IsDBNull(oggetto) Then
            Return Nothing
        Else
            Return oggetto
        End If
    End Function

    Protected Sub ImageButton1_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles ImageButton1.Click
        CaricaExtra()
    End Sub
End Class
