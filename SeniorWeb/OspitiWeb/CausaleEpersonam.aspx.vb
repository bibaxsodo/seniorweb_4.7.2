﻿Imports Microsoft.VisualBasic
Imports System.Data.OleDb
Imports System.Web.Hosting
Partial Class OspitiWeb_CausaleEpersonam
    Inherits System.Web.UI.Page
    Protected Sub Btn_Modifica_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles Btn_Modifica.Click
        If DD_Causale.SelectedValue = "" Then
            ClientScript.RegisterClientScriptBlock(Me.GetType(), "Errore", "VisualizzaErrore('Indicare Causael Senior');", True)
            Exit Sub
        End If
        If Txt_CausaleEPersonam.Text.Trim = "" Then
            ClientScript.RegisterClientScriptBlock(Me.GetType(), "Errore", "VisualizzaErrore('Indicare Causale E-Personam');", True)
            Exit Sub
        End If


        Dim cn As OleDbConnection
        Dim MySql As String = ""

        cn = New Data.OleDb.OleDbConnection(Session("DC_OSPITE"))

        cn.Open()
        Dim cmd As New OleDbCommand()

        If Val(Txt_Id.Text) > 0 Then
            cmd.CommandText = ("Update EPersonam set CausaleEPersonam = ?,CentroServizio = ?,CausaleOspiti = ?  Where id = " & Val(Txt_Id.Text))
        Else
            cmd.CommandText = ("Insert into EPersonam (CausaleEPersonam,CentroServizio,CausaleOspiti) VALUES (?,?,?)")

        End If
        'CausaleEPersonam,CentroServizio,CausaleOspiti
        cmd.Parameters.AddWithValue("@CausaleEPersonam", Txt_CausaleEPersonam.Text)
        cmd.Parameters.AddWithValue("@CentroServizio", Cmb_CServ.SelectedValue)
        cmd.Parameters.AddWithValue("@CausaleOspiti", DD_Causale.SelectedValue)
        cmd.Connection = cn
        cmd.ExecuteNonQuery()

        cn.Close()

        Response.Redirect("ElencoCausaliEpersonam.aspx")

    End Sub

    Private Function SplitWords(ByVal s As String) As String()
        '
        ' Call Regex.Split function from the imported namespace.
        ' Return the result array.
        '
        Return Regex.Split(s, "\W+")
    End Function

    Protected Sub OspitiWeb_Tabella_ModalitaPagamento_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load

        If Page.IsPostBack = True Then
            Exit Sub
        End If


        Dim K1 As New Cls_SqlString

        Dim Barra As New Cls_BarraSenior

        If Not IsNothing(Session("RicercaAnagraficaSQLString")) Then
            K1 = Session("RicercaAnagraficaSQLString")
        End If


        Dim K As New Cls_CausaliEntrataUscita

        K.UpDateDropBox(Session("DC_OSPITE"), "99", DD_Causale)

        Dim kVilla As New Cls_TabelleDescrittiveOspitiAccessori


        kVilla.UpDateDropBox(Session("DC_OSPITIACCESSORI"), "VIL", DD_Struttura)
        If DD_Struttura.Items.Count = 1 Then
            Call AggiornaCServ()
        End If

        Lbl_BarraSenior.Text = Barra.CodiceBarra(Application("SENIOR"), Session("UTENTE"), Page, K1)

        Txt_Id.Enabled = False
        If Request.Item("CODICE") <> "" Then

            Dim cn As OleDbConnection
            Dim MySql As String = ""

            cn = New Data.OleDb.OleDbConnection(Session("DC_OSPITE"))

            cn.Open()
            Dim cmd As New OleDbCommand()
            cmd.CommandText = ("select * from EPersonam Where id = ?")

            cmd.Parameters.AddWithValue("@id", Val(Request.Item("CODICE")))
            cmd.Connection = cn            


            Dim myPOSTreader As OleDbDataReader = cmd.ExecuteReader()
            If myPOSTreader.Read Then


                Txt_Id.Text = campodb(myPOSTreader.Item("Id"))
                Txt_CausaleEPersonam.Text = campodb(myPOSTreader.Item("CausaleEPersonam"))

                Dim KCserv As New Cls_CentroServizio

                KCserv.CENTROSERVIZIO = campodb(myPOSTreader.Item("CentroServizio"))
                KCserv.Leggi(Session("DC_OSPITE"), campodb(myPOSTreader.Item("CentroServizio")))
                If KCserv.DESCRIZIONE <> "" Then

                    DD_Struttura.SelectedValue = KCserv.Villa
                    AggiornaCServ()
                    Cmb_CServ.SelectedValue = campodb(myPOSTreader.Item("CentroServizio"))
                End If

                DD_Causale.SelectedValue = campodb(myPOSTreader.Item("CausaleOspiti"))

            End If
            myPOSTreader.Close()

        End If

        Call EseguiJS()
    End Sub


    Private Sub EseguiJS()
        Dim MyJs As String
        MyJs = "$(document).ready(function() {"

        MyJs = MyJs & "var els = document.getElementsByTagName(""*"");"

        MyJs = MyJs & "for (var i=0;i<els.length;i++)"
        MyJs = MyJs & "if ( els[i].id ) { "
        MyJs = MyJs & " var appoggio =els[i].id; "
        MyJs = MyJs & " if (appoggio.match('TxtData')!= null) {  "
        MyJs = MyJs & " $(els[i]).mask(""99/99/9999"");"
        MyJs = MyJs & "    }"

        MyJs = MyJs & " if ((appoggio.match('TxtImporto')!= null) ) {  "
        MyJs = MyJs & " $(els[i]).keypress(function() { ForceNumericInput($(this).val(), true, true); } ); "
        MyJs = MyJs & " $(els[i]).blur(function() { var ap = formatNumber($(this).val(),2); $(this).val(ap); });"
        MyJs = MyJs & "    }"

        MyJs = MyJs & " if (appoggio.match('Txt_Sottoconto')!= null) {   "
        MyJs = MyJs & " $(els[i]).autocomplete('/WebHandler/GestioneConto.ashx?Utente=" & Session("UTENTE") & "', {delay:5,minChars:3});"
        MyJs = MyJs & "    }"

        MyJs = MyJs & "} "
        MyJs = MyJs & "});"

        'MyJs = "$(document).ready(function() { $('.myClass').keypress(function() { handleEnter($('.myClass').val(), true, true); } );     $('#" & Txt_ImportoPagato.ClientID & "').blur(function() { var ap = formatNumber ($('#" & Txt_ImportoPagato.ClientID & "').val(),2); $('#" & Txt_ImportoPagato.ClientID & "').val(ap); }); });"
        ScriptManager.RegisterStartupScript(Me, Me.GetType(), "visualizzaRitIm", MyJs, True)
    End Sub

    Protected Sub ImageButton1_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles ImageButton1.Click
        If DD_Causale.SelectedValue = "" Then
            ClientScript.RegisterClientScriptBlock(Me.GetType(), "Errore", "VisualizzaErrore('Indicare Causael Senior');", True)
            Exit Sub
        End If
        If Txt_CausaleEPersonam.Text.Trim = "" Then
            ClientScript.RegisterClientScriptBlock(Me.GetType(), "Errore", "VisualizzaErrore('Indicare Causale E-Personam');", True)
            Exit Sub
        End If


        Dim cn As OleDbConnection
        Dim MySql As String = ""

        cn = New Data.OleDb.OleDbConnection(Session("DC_OSPITE"))

        cn.Open()
        Dim cmd As New OleDbCommand()


        cmd.CommandText = ("Delete from EPersonam Where id = " & Val(Txt_Id.Text))
        cmd.Connection = cn
        cmd.ExecuteNonQuery()

        cn.Close()

        Response.Redirect("ElencoCausaliEpersonam.aspx")
    End Sub

    Protected Sub Btn_Esci_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles Btn_Esci.Click
        Response.Redirect("ElencoCausaliEpersonam.aspx")
    End Sub

    Protected Sub DD_Struttura_TextChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles DD_Struttura.TextChanged
        AggiornaCServ()
        Call EseguiJS()
    End Sub

    Private Sub AggiornaCServ()
        Dim kCsrv As New Cls_CentroServizio

        If DD_Struttura.SelectedValue = "" Then
            kCsrv.UpDateDropBox(Session("DC_OSPITE"), Cmb_CServ)
        Else
            kCsrv.UpDateDropBoxStruttura(Session("DC_OSPITE"), Cmb_CServ, DD_Struttura.SelectedValue)
        End If

    End Sub


    Function campodb(ByVal oggetto As Object) As String
        If IsDBNull(oggetto) Then
            Return ""
        Else
            Return oggetto
        End If
    End Function


End Class
