﻿<%@ Page Language="VB" AutoEventWireup="false" Inherits="OspitiWeb_AggiornaRette" CodeFile="AggiornaRette.aspx.vb" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="xasp" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">

    <title>Aggiorna Rette</title>
    <asp:PlaceHolder runat="server">
        <%: System.Web.Optimization.Styles.Render("~/Content/AjaxControlToolkit/Styles/Bundle") %>
    </asp:PlaceHolder>
    <link href="ospiti.css?ver=11" rel="stylesheet" type="text/css" />
    <link rel="shortcut icon" href="images/SENIOR.ico" />
    <link href="js/jquery.autocomplete.css" rel="stylesheet" type="text/css" />


    <script src="js/jquery-1.5.1.min.js" type="text/javascript"></script>
    <script src="js/jquery.autocomplete.js" type="text/javascript"></script>
    <script src="js/soapclient.js" type="text/javascript"></script>
    <script src="/js/convertinumero.js" type="text/javascript"></script>

    <link rel="stylesheet" href="dialogbox/redips-dialog.css" type="text/css" media="screen" />
    <script type="text/javascript" src="dialogbox/redips-dialog-min.js"></script>

    <script src="js/jquery.maskedinput-1.3.min.js" type="text/javascript"></script>
    <script src="/js/formatnumer.js" type="text/javascript"></script>
    <script src="js/JSErrore.js" type="text/javascript"></script>
    <script src="js/NoEnter.js" type="text/javascript"></script>
    <link rel="stylesheet" href="jqueryui/jquery-ui.css" type="text/css">
    <script src="jqueryui/jquery-ui.js" type="text/javascript"></script>
    <script src="jqueryui/datapicker-it.js" type="text/javascript"></script>
    <style>
        .CSSTableGenerator {
            margin: 0px;
            padding: 0px;
            width: 350px;
            height: 200px;
            box-shadow: 10px 10px 5px #888888;
            border: 1px solid #000000;
            -moz-border-radius-bottomleft: 0px;
            -webkit-border-bottom-left-radius: 0px;
            border-bottom-left-radius: 0px;
            -moz-border-radius-bottomright: 0px;
            -webkit-border-bottom-right-radius: 0px;
            border-bottom-right-radius: 0px;
            -moz-border-radius-topright: 0px;
            -webkit-border-top-right-radius: 0px;
            border-top-right-radius: 0px;
            -moz-border-radius-topleft: 0px;
            -webkit-border-top-left-radius: 0px;
            border-top-left-radius: 0px;
        }

            .CSSTableGenerator table {
                border-collapse: collapse;
                border-spacing: 0;
                width: 100%;
                height: 100%;
                margin: 0px;
                padding: 0px;
            }

            .CSSTableGenerator tr:last-child td:last-child {
                -moz-border-radius-bottomright: 0px;
                -webkit-border-bottom-right-radius: 0px;
                border-bottom-right-radius: 0px;
            }

            .CSSTableGenerator table tr:first-child td:first-child {
                -moz-border-radius-topleft: 0px;
                -webkit-border-top-left-radius: 0px;
                border-top-left-radius: 0px;
            }

            .CSSTableGenerator table tr:first-child td:last-child {
                -moz-border-radius-topright: 0px;
                -webkit-border-top-right-radius: 0px;
                border-top-right-radius: 0px;
            }

            .CSSTableGenerator tr:last-child td:first-child {
                -moz-border-radius-bottomleft: 0px;
                -webkit-border-bottom-left-radius: 0px;
                border-bottom-left-radius: 0px;
            }

            .CSSTableGenerator tr:hover td {
            }

            .CSSTableGenerator tr:nth-child(odd) {
                background-color: #ffffff;
            }

            .CSSTableGenerator tr:nth-child(even) {
                background-color: #ffffff;
            }

            .CSSTableGenerator td {
                vertical-align: middle;
                border: 1px solid #000000;
                border-width: 0px 1px 1px 0px;
                text-align: left;
                padding: 7px;
                font-size: 10px;
                font-family: Arial;
                font-weight: normal;
                color: #000000;
            }

            .CSSTableGenerator tr:last-child td {
                border-width: 0px 1px 0px 0px;
            }

            .CSSTableGenerator tr td:last-child {
                border-width: 0px 0px 1px 0px;
            }

            .CSSTableGenerator tr:last-child td:last-child {
                border-width: 0px 0px 0px 0px;
            }

            .CSSTableGenerator tr:first-child td {
                background: -o-linear-gradient(bottom, #005fbf 5%, #003f7f 100%);
                background: -webkit-gradient( linear, left top, left bottom, color-stop(0.05, #005fbf), color-stop(1, #003f7f) );
                background: -moz-linear-gradient( center top, #005fbf 5%, #003f7f 100% );
                filter: progid:DXImageTransform.Microsoft.gradient(startColorstr="#005fbf", endColorstr="#003f7f");
                background: -o-linear-gradient(top,#005fbf,003f7f);
                background-color: #005fbf;
                border: 0px solid #000000;
                text-align: center;
                border-width: 0px 0px 1px 1px;
                font-size: 14px;
                font-family: Arial;
                font-weight: bold;
                color: #ffffff;
            }

            .CSSTableGenerator tr:first-child:hover td {
                background: -o-linear-gradient(bottom, #005fbf 5%, #003f7f 100%);
                background: -webkit-gradient( linear, left top, left bottom, color-stop(0.05, #005fbf), color-stop(1, #003f7f) );
                background: -moz-linear-gradient( center top, #005fbf 5%, #003f7f 100% );
                filter: progid:DXImageTransform.Microsoft.gradient(startColorstr="#005fbf", endColorstr="#003f7f");
                background: -o-linear-gradient(top,#005fbf,003f7f);
                background-color: #005fbf;
            }

            .CSSTableGenerator tr:first-child td:first-child {
                border-width: 0px 0px 1px 0px;
            }

            .CSSTableGenerator tr:first-child td:last-child {
                border-width: 0px 0px 1px 1px;
            }
    </style>
    <script type="text/javascript">         

        $(document).ready(function () {
            if (window.innerHeight > 0) { $("#BarraLaterale").css("height", (window.innerHeight - 94) + "px"); } else { $("#BarraLaterale").css("height", (document.documentElement.offsetHeight - 94) + "px"); }
        });

        function CloseMese() {
            if ($("#copia").css("visibility") != "hidden") {
                $("#copia").css('visibility', 'hidden');
            } else {
                $("#copia").css('visibility', 'visible');
            }
        }
    </script>
</head>
<body>
    <form id="form1" runat="server">
        <asp:ScriptManager ID="ScriptManager1" runat="server" EnableScriptGlobalization="true">
            <Scripts>
                <asp:ScriptReference Path="~/Scripts/AjaxControlToolkit/Bundle" />
            </Scripts>
        </asp:ScriptManager>
        <asp:Label ID="Lbl_BarraSenior" runat="server" Text=""></asp:Label>
        <div align="left">
            <table style="width: 100%;" cellpadding="0" cellspacing="0">
                <tr>
                    <td style="width: 160px; background-color: #F0F0F0;"></td>
                    <td>
                        <div class="Titolo">Ospiti - Strumenti - Aggiorna Rette</div>
                        <div class="SottoTitolo">
                            <br />
                            <br />
                        </div>
                    </td>
                    <td style="text-align: right;">
                        <div class="DivTasti">
                            <asp:ImageButton ID="Btn_Visualizza" runat="server" Height="38px" ImageUrl="~/images/esegui.png" class="EffettoBottoniTondi" Width="38px" />
                            <asp:ImageButton ID="Btn_Modifica0" runat="server" Height="38px" ImageUrl="~/images/salva.jpg" class="EffettoBottoniTondi" Width="38px" ToolTip="Modifica" />
                        </div>
                    </td>
                </tr>

                <tr>
                    <td style="width: 160px; background-color: #F0F0F0; vertical-align: top; text-align: center;" id="BarraLaterale">
                        <a href="Menu_Ospiti.aspx" style="border-width: 0px;">
                            <img src="images/Home.jpg" class="Effetto" alt="Menù" /></a><br />
                        <asp:ImageButton ID="Btn_Esci" runat="server" BackColor="Transparent" ImageUrl="../images/Menu_Indietro.png" class="Effetto" ToolTip="Chiudi" />

                        <br />
                        <br />
                        <br />
                        <br />
                        <br />
                        <br />
                        <br />
                        <br />
                        <br />
                        <br />
                        <br />
                        <br />
                        <br />
                    </td>
                    <td colspan="2" style="background-color: #FFFFFF; vertical-align: top;">
                        <xasp:TabContainer ID="TabContainer1" runat="server" ActiveTabIndex="0" CssClass="TabSenior"
                            Width="100%" BorderStyle="None" Style="margin-right: 39px">
                            <xasp:TabPanel runat="server" HeaderText="Prima Nota" ID="Tab_Anagrafica">
                                <HeaderTemplate>Aggiorna Rette</HeaderTemplate>


                                <ContentTemplate>

                                    <asp:UpdatePanel ID="UpdatePanel1" runat="server">
                                        <ContentTemplate>
                                            <table style="width: 100%;">
                                                <tr>
                                                    <td style="width: 50%;">
                                                        <br />
                                                        <label class="LabelCampo">Data Validita :</label>
                                                        <asp:TextBox ID="Txt_DataRegistrazione" runat="server" Width="90px"></asp:TextBox><br />
                                                        <br />
                                                        <label class="LabelCampo">Struttura:</label>
                                                        <asp:DropDownList runat="server" ID="DD_Struttura" AutoPostBack="true"></asp:DropDownList><br />
                                                        <br />

                                                        <label class="LabelCampo">Centro Servizio :</label>
                                                        <asp:DropDownList ID="DD_CentroServizio" runat="server"></asp:DropDownList><br />
                                                        <br />
                                                        <label class="LabelCampo">Solo Non Auto :</label>
                                                        <asp:CheckBox ID="Chk_SoloNonAuto" runat="server" />
                                                        <br />
                                                        <br />
                                                    </td>
                                                    <td style="width: 50%;">
                                                        <a href="#" onclick="CloseMese();">
                                                            <img src="../images/copiaimporti.jpg" title="" />
                                                        </a>
                                                        <div id="copia" style="visibility: hidden;">
                                                            <div class="CSSTableGenerator">
                                                                <br />
                                                                <label class="LabelCampo">Importo Da :</label>
                                                                <asp:TextBox ID="Txt_ImportoDa" runat="server" Width="30%" Style="border-color: Red;"></asp:TextBox>
                                                                <br />
                                                                <br />
                                                                <label class="LabelCampo">Iva :</label>
                                                                <asp:DropDownList ID="DD_IVA" runat="server" Width="176px"></asp:DropDownList><br />
                                                                <br />
                                                                <label class="LabelCampo">Importo a :</label>
                                                                <asp:TextBox ID="Txt_ImportoA" runat="server" Width="30%" Style="border-color: Red;"></asp:TextBox>
                                                                <br />
                                                                <br />
                                                                <label class="LabelCampo">&nbsp;</label><asp:Button ID="Btn_Copia" runat="server" Text="Copia" />
                                                            </div>
                                                        </div>
                                                    </td>
                                                </tr>
                                            </table>
                                            <asp:GridView ID="Grd_AggiornaRette" runat="server" CellPadding="4" Height="60px"
                                                ShowFooter="True" BackColor="White" BorderColor="#6FA7D1"
                                                BorderStyle="Dotted" BorderWidth="1px">
                                                <RowStyle ForeColor="#333333" BackColor="White" />
                                                <Columns>
                                                    <asp:BoundField DataField="CodiceOspite" HeaderText="Codice Ospite" />
                                                    <asp:BoundField DataField="NomeOspite" HeaderText="Nome Ospite" />
                                                    <asp:BoundField DataField="Data" HeaderText="Data" />
                                                    <asp:BoundField DataField="ImportoAttuale" HeaderText="Ultimo Importo" />
                                                    <asp:TemplateField HeaderText="Importo">
                                                        <ItemTemplate>
                                                            <asp:TextBox ID="Txt_Importo" Style="text-align: right;" onkeypress="return handleEnter(this, event)" Width="100px" runat="server"></asp:TextBox>
                                                        </ItemTemplate>
                                                        <HeaderStyle Font-Bold="False" Font-Italic="False" />
                                                        <ItemStyle Width="100px" />
                                                    </asp:TemplateField>
                                                    <asp:TemplateField HeaderText="Dopo Data">
                                                        <ItemTemplate>
                                                            <asp:CheckBox ID="Chk_ModifcaDopoData" runat="server" /></ItemTemplate>
                                                        <HeaderStyle Font-Bold="False" Font-Italic="False" />
                                                        <ItemStyle Width="100px" />
                                                    </asp:TemplateField>
                                                    <asp:BoundField DataField="IVA" HeaderText="IVA" FooterStyle-Font-Italic="true" />
                                                    <asp:BoundField DataField="QSANITARIA" HeaderText="Q.SANITARIA" FooterStyle-Font-Italic="true" />
                                                </Columns>
                                                <FooterStyle BackColor="White" ForeColor="#023102" />
                                                <HeaderStyle BackColor="#A6C9E2" Font-Bold="False" ForeColor="White" BorderColor="#6FA7D1" BorderWidth="1" />
                                                <PagerStyle BackColor="#336666" ForeColor="White" HorizontalAlign="Center" />
                                                <SelectedRowStyle BackColor="#339966" Font-Bold="True" ForeColor="White" />
                                            </asp:GridView>

                                            <br />
                                            <br />
                                            <asp:Label ID="Lbl_errori" runat="server" ForeColor="DarkRed" Width="272px"></asp:Label><br />
                                            <br />
                                            </div>
                                        </ContentTemplate>
                                    </asp:UpdatePanel>



                                </ContentTemplate>


                            </xasp:TabPanel>
                        </xasp:TabContainer>
                    </td>
                </tr>
            </table>

        </div>
    </form>
</body>
</html>

