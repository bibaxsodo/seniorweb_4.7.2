﻿<%@ Page Language="VB" AutoEventWireup="false" Inherits="Diurno" CodeFile="Diurno.aspx.vb" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="xasp" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <meta http-equiv="x-ua-compatible" content="IE=9" />
    <title>Diurno</title>
    <asp:PlaceHolder runat="server">
        <%: System.Web.Optimization.Styles.Render("~/Content/AjaxControlToolkit/Styles/Bundle") %>
    </asp:PlaceHolder>
    <link href="ospiti.css?ver=11" rel="stylesheet" type="text/css" />
    <link rel="shortcut icon" href="images/SENIOR.ico" />
    <link href="js/jquery.autocomplete.css" rel="stylesheet" type="text/css" />

    <link rel="stylesheet" href="dialogbox/redips-dialog.css" type="text/css" media="screen" />
    <script type="text/javascript" src="dialogbox/redips-dialog-min.js"></script>
    <script type="text/javascript" src="dialogbox/script.js"></script>


    <script src="js/jquery-1.5.1.min.js" type="text/javascript"></script>
    <script src="js/jquery.autocomplete.js" type="text/javascript"></script>
    <script src="js/soapclient.js" type="text/javascript"></script>
    <script src="/js/convertinumero.js" type="text/javascript"></script>

    <link rel="stylesheet" href="dialogbox/redips-dialog.css" type="text/css" media="screen" />
    <script type="text/javascript" src="dialogbox/redips-dialog-min.js"></script>


    <script src="js/jquery.maskedinput-1.3.min.js" type="text/javascript"></script>
    <script src="/js/formatnumer.js" type="text/javascript"></script>
    <script src="js/JSErrore.js" type="text/javascript"></script>
    <script src="js/NoEnter.js" type="text/javascript"></script>
    <script type="text/javascript" src="../js/menuanagraficaospiti.js?ver=7"></script>

    <script src="js/chosen/chosen.jquery.js" type="text/javascript"></script>
    <script src="js/chosen/docsupport/prism.js" type="text/javascript" charset="utf-8"></script>
    <link rel="stylesheet" href="js/chosen/docsupport/style.css">
    <link rel="stylesheet" href="js/chosen/docsupport/prism.css">
    <link rel="stylesheet" href="js/chosen/chosen.css">

    <link rel="stylesheet" href="jqueryui/jquery-ui.css" type="text/css">
    <script src="jqueryui/jquery-ui.js" type="text/javascript"></script>
    <script src="jqueryui/datapicker-it.js" type="text/javascript"></script>
    <style type="text/css">
        .CELLA {
            text-align: center;
            border: 1px solid #66CCFF;
            background-color: #FFFFFF;
        }

        .Tabella {
            border: 1px solid #66CCFF;
        }

        .Testata {
            text-align: center;
            border: 1px solid #66CCFF;
            background-color: #C3D9FF;
            color: #7A5AD5;
        }

        .GIORNO {
            background-color: #E8EEF7;
            text-align: right;
        }

        .LinkGriglia {
            font-family: Verdana;
            color: #777777;
            font-size: small;
            display: block;
            vertical-align: text-top;
            text-align: center;
            width: 100px;
            background-color: Gray;
        }

            .LinkGriglia a {
                color: #000000;
            }
    </style>
    <script type="text/javascript">
        function DialogBox(Path) {

            REDIPS.dialog.show(500, 300, '<iframe id="output" src="' + Path + '" height="300px" width="500"></iframe>');
            return false;
        }
    </script>
</head>
<body>
    <form id="form1" runat="server">
        <asp:ScriptManager ID="ScriptManager1" runat="server" EnableScriptGlobalization="true">
            <Scripts>
                <asp:ScriptReference Path="~/Scripts/AjaxControlToolkit/Bundle" />
            </Scripts>
        </asp:ScriptManager>
        <asp:Label ID="Lbl_BarraSenior" runat="server" Text=""></asp:Label>
        <div align="left">


            <table style="width: 100%;" cellpadding="0" cellspacing="0">
                <tr>
                    <td style="width: 185px; background-color: #F0F0F0;"></td>
                    <td>
                        <div class="Titolo">Ospiti - Diurno</div>
                        <div class="SottoTitoloOSPITE">
                            <asp:Label ID="Lbl_NomeOspite" runat="server"></asp:Label><br />
                            <br />
                        </div>
                    </td>
                    <td style="text-align: right;">
                        <div class="DivTastiOspite">
                            <asp:ImageButton ID="Btn_Modifica" runat="server" Height="38px" ImageUrl="~/images/salva.jpg" class="EffettoBottoniTondi" Width="38px" ToolTip="Modifica / Inserisci" />
                        </div>
                    </td>
                </tr>
            </table>
            <table style="width: 100%;" cellpadding="0" cellspacing="0">
                <tr>
                    <td style="width: 160px; background-color: #F0F0F0; vertical-align: top; text-align: center;">
                        <a href="Menu_Ospiti.aspx" style="border-width: 0px;">
                            <img src="images/Home.jpg" alt="Menù" class="Effetto" /></a><br />
                        <asp:ImageButton ID="Btn_Esci" runat="server" BackColor="Transparent" ImageUrl="../images/Menu_Indietro.png" class="Effetto" ToolTip="Chiudi" />
                        <br />
                        <div id="MENUDIV"></div>

                        <br />
                        <br />
                        <br />
                        <br />
                        <br />
                        <br />
                        <br />
                        <br />
                    </td>
                    <td colspan="2" style="background-color: #FFFFFF; vertical-align: top;">
                        <xasp:TabContainer ID="TabContainer1" runat="server" ActiveTabIndex="0" CssClass="TabSenior"
                            Width="100%" BorderStyle="None" Style="margin-right: 39px">
                            <xasp:TabPanel runat="server" HeaderText="Prima Nota" ID="Tab_Anagrafica">
                                <HeaderTemplate>
                                    Diurno              
                                </HeaderTemplate>
                                <ContentTemplate>
                                    <asp:UpdatePanel ID="UpdatePanel1" runat="server">
                                        <ContentTemplate>
                                            <label class="LabelCampo">Anno :</label>
                                            <asp:TextBox ID="Txt_Anno" onkeypress="return soloNumeri(event);" runat="server" Width="64px"></asp:TextBox>
                                            <br />
                                            <br />
                                            <label class="LabelCampo">Mese :</label>
                                            <asp:DropDownList ID="Dd_Mese" runat="server" Width="128px">
                                                <asp:ListItem Value="1">Gennaio</asp:ListItem>
                                                <asp:ListItem Value="2">Febbraio</asp:ListItem>
                                                <asp:ListItem Value="3">Marzo</asp:ListItem>
                                                <asp:ListItem Value="4">Aprile</asp:ListItem>
                                                <asp:ListItem Value="5">Maggio</asp:ListItem>
                                                <asp:ListItem Value="6">Giugno</asp:ListItem>
                                                <asp:ListItem Value="7">Luglio</asp:ListItem>
                                                <asp:ListItem Value="8">Agosto</asp:ListItem>
                                                <asp:ListItem Value="9">Settembre</asp:ListItem>
                                                <asp:ListItem Value="10">Ottobre</asp:ListItem>
                                                <asp:ListItem Value="11">Novembre</asp:ListItem>
                                                <asp:ListItem Value="12">Dicembre</asp:ListItem>
                                            </asp:DropDownList>
                                            &nbsp;<asp:Button ID="Btn_CaricaMese" runat="server" BackColor="White" ForeColor="Gray" Text="Carica Mese" Width="112px" /><br />

                                            <table>
                                                <tr>

                                                    <td style="width: 300px;">

                                                        <table>
                                                            <tr>
                                                                <td>
                                                                    <asp:ImageButton ID="Img_Causale1" ImageUrl="images/diurno_1.png" runat="server" Width="20px" Height="20px" />
                                                                </td>
                                                                <td>
                                                                    <asp:Label ID="Lbl_Causale1" Font-Size="Small" runat="server" Text="" Width="200px"></asp:Label>
                                                                </td>
                                                            </tr>
                                                            <tr>
                                                                <td>
                                                                    <asp:ImageButton ID="Img_Causale2" ImageUrl="images/diurno_2.png" runat="server" Width="20px" Height="20px" />
                                                                </td>
                                                                <td>
                                                                    <asp:Label ID="Lbl_Causale2" Font-Size="Small" runat="server" Text="" Width="200px"></asp:Label>
                                                                </td>
                                                            </tr>
                                                            <tr>
                                                                <td>
                                                                    <asp:ImageButton ID="Img_Causale3" ImageUrl="images/diurno_3.png" runat="server" Width="20px" Height="20px" />
                                                                </td>
                                                                <td>
                                                                    <asp:Label ID="Lbl_Causale3" Font-Size="Small" runat="server" Text="" Width="200px"></asp:Label>
                                                                </td>
                                                            </tr>
                                                            <tr>
                                                                <td>
                                                                    <asp:ImageButton ID="Img_Causale4" ImageUrl="images/diurno_4.png" runat="server" Width="20px" Height="20px" />
                                                                </td>
                                                                <td>
                                                                    <asp:Label ID="Lbl_Causale4" Font-Size="Small" runat="server" Text="" Width="200px"></asp:Label>
                                                                </td>
                                                            </tr>
                                                            <tr>
                                                                <td>
                                                                    <asp:ImageButton ID="Img_Causale5" ImageUrl="images/diurno_5.png" runat="server" Width="20px" Height="20px" />
                                                                </td>
                                                                <td>
                                                                    <asp:Label ID="Lbl_Causale5" Font-Size="Small" runat="server" Text="" Width="200px"></asp:Label>
                                                                </td>
                                                            </tr>
                                                            <tr>
                                                                <td>
                                                                    <asp:ImageButton ID="Img_Causale6" ImageUrl="images/diurno_6.png" runat="server" Width="20px" Height="20px" />
                                                                </td>
                                                                <td>
                                                                    <asp:Label ID="Lbl_Causale6" Font-Size="Small" runat="server" Text="" Width="200px"></asp:Label>
                                                                </td>
                                                            </tr>
                                                            <tr>
                                                                <td>
                                                                    <asp:ImageButton ID="Img_Causale7" ImageUrl="images/diurno_7.png" runat="server" Width="20px" Height="20px" />
                                                                </td>
                                                                <td>
                                                                    <asp:Label ID="Lbl_Causale7" Font-Size="Small" runat="server" Text="" Width="200px"></asp:Label>
                                                                </td>
                                                            </tr>
                                                            <tr>
                                                                <td>
                                                                    <asp:ImageButton ID="Img_Causale8" ImageUrl="images/diurno_8.png" runat="server" Width="20px" Height="20px" />
                                                                </td>
                                                                <td>
                                                                    <asp:Label ID="Lbl_Causale8" Font-Size="Small" runat="server" Text="" Width="200px"></asp:Label>
                                                                </td>
                                                            </tr>

                                                            <tr>
                                                                <td>
                                                                    <asp:ImageButton ID="Img_Causale9" ImageUrl="images/diurno_9.png" runat="server" Width="20px" Height="20px" />
                                                                </td>
                                                                <td>
                                                                    <asp:Label ID="Lbl_Causale9" Font-Size="Small" runat="server" Text="" Width="200px"></asp:Label>
                                                                </td>
                                                            </tr>
                                                            <tr>
                                                                <td>
                                                                    <asp:ImageButton ID="Img_Causale10" ImageUrl="images/diurno_10.png" runat="server" Width="20px" Height="20px" />
                                                                </td>
                                                                <td>
                                                                    <asp:Label ID="Lbl_Causale10" Font-Size="Small" runat="server" Text="" Width="200px"></asp:Label>
                                                                </td>
                                                            </tr>
                                                            <tr>
                                                                <td>
                                                                    <asp:ImageButton ID="Img_Causale11" ImageUrl="images/diurno_11.png" runat="server" Width="20px" Height="20px" />
                                                                </td>
                                                                <td>
                                                                    <asp:Label ID="Lbl_Causale11" Font-Size="Small" runat="server" Text="" Width="200px"></asp:Label>
                                                                </td>
                                                            </tr>
                                                            <tr>
                                                                <td>
                                                                    <asp:ImageButton ID="Img_Causale12" ImageUrl="images/diurno_12.png" runat="server" Width="20px" Height="20px" />
                                                                </td>
                                                                <td>
                                                                    <asp:Label ID="Lbl_Causale12" Font-Size="Small" runat="server" Text="" Width="200px"></asp:Label>
                                                                </td>
                                                            </tr>
                                                            <tr>
                                                                <td>
                                                                    <asp:ImageButton ID="Img_Causale13" ImageUrl="images/diurno_13.png" runat="server" Width="20px" Height="20px" />
                                                                </td>
                                                                <td>
                                                                    <asp:Label ID="Lbl_Causale13" Font-Size="Small" runat="server" Text="" Width="200px"></asp:Label>
                                                                </td>
                                                            </tr>
                                                            <tr>
                                                                <td>
                                                                    <asp:ImageButton ID="Img_Causale14" ImageUrl="images/diurno_14.png" runat="server" Width="20px" Height="20px" />
                                                                </td>
                                                                <td>
                                                                    <asp:Label ID="Lbl_Causale14" Font-Size="Small" runat="server" Text="" Width="200px"></asp:Label>
                                                                </td>
                                                            </tr>
                                                            <tr>
                                                                <td>
                                                                    <asp:ImageButton ID="Img_Causale15" ImageUrl="images/diurno_15.png" runat="server" Width="20px" Height="20px" />
                                                                </td>
                                                                <td>
                                                                    <asp:Label ID="Lbl_Causale15" Font-Size="Small" runat="server" Text="" Width="200px"></asp:Label>
                                                                </td>
                                                            </tr>
                                                            <tr>
                                                                <td>
                                                                    <asp:ImageButton ID="Img_Causale16" ImageUrl="images/diurno_16.png" runat="server" Width="20px" Height="20px" />
                                                                </td>
                                                                <td>
                                                                    <asp:Label ID="Lbl_Causale16" Font-Size="Small" runat="server" Text="" Width="200px"></asp:Label>
                                                                </td>
                                                            </tr>
                                                            <tr>
                                                                <td>
                                                                    <asp:ImageButton ID="Img_Causale17" ImageUrl="images/diurno_17.png" runat="server" Width="20px" Height="20px" />
                                                                </td>
                                                                <td>
                                                                    <asp:Label ID="Lbl_Causale17" Font-Size="Small" runat="server" Text="" Width="200px"></asp:Label>
                                                                </td>
                                                            </tr>
                                                            <tr>
                                                                <td>
                                                                    <asp:ImageButton ID="Img_Causale18" ImageUrl="images/diurno_18.png" runat="server" Width="20px" Height="20px" />
                                                                </td>
                                                                <td>
                                                                    <asp:Label ID="Lbl_Causale18" Font-Size="Small" runat="server" Text="" Width="200px"></asp:Label>
                                                                </td>
                                                            </tr>
                                                            <tr>
                                                                <td>
                                                                    <asp:ImageButton ID="Img_Causale19" ImageUrl="images/diurno_19.png" runat="server" Width="20px" Height="20px" />
                                                                </td>
                                                                <td>
                                                                    <asp:Label ID="Lbl_Causale19" Font-Size="Small" runat="server" Text="" Width="200px"></asp:Label>
                                                                </td>
                                                            </tr>
                                                            <tr>
                                                                <td>
                                                                    <asp:ImageButton ID="Img_Causale20" ImageUrl="images/diurno_20.png" runat="server" Width="20px" Height="20px" />
                                                                </td>
                                                                <td>
                                                                    <asp:Label ID="Lbl_Causale20" Font-Size="Small" runat="server" Text="" Width="200px"></asp:Label>
                                                                </td>
                                                            </tr>

                                                            <tr>
                                                                <td>
                                                                    <asp:ImageButton ID="Img_Causale25" ImageUrl="images/diurno_25.png" runat="server" Width="20px" Height="20px" />
                                                                </td>
                                                                <td>
                                                                    <asp:Label ID="Lbl_Causale25" Font-Size="Small" runat="server" Text="" Width="200px"></asp:Label>
                                                                </td>
                                                            </tr>
                                                            <tr>
                                                                <td>
                                                                    <asp:ImageButton ID="Img_Causale26" ImageUrl="images/diurno_26.png" runat="server" Width="20px" Height="20px" />
                                                                </td>
                                                                <td>
                                                                    <asp:Label ID="Lbl_Causale26" Font-Size="Small" runat="server" Text="" Width="200px"></asp:Label>
                                                                </td>
                                                            </tr>
                                                            <tr>
                                                                <td>
                                                                    <asp:ImageButton ID="Img_Causale27" ImageUrl="images/diurno_27.png" runat="server" Width="20px" Height="20px" />
                                                                </td>
                                                                <td>
                                                                    <asp:Label ID="Lbl_Causale27" Font-Size="Small" runat="server" Text="" Width="200px"></asp:Label>
                                                                </td>
                                                            </tr>
                                                            <tr>
                                                                <td>
                                                                    <asp:ImageButton ID="Img_Causale28" ImageUrl="images/diurno_28.png" runat="server" Width="20px" Height="20px" />
                                                                </td>
                                                                <td>
                                                                    <asp:Label ID="Lbl_Causale28" Font-Size="Small" runat="server" Text="" Width="200px"></asp:Label>
                                                                </td>
                                                            </tr>

                                                            <tr>
                                                                <td>
                                                                    <asp:ImageButton ID="Img_Causale29" ImageUrl="images/diurno_29.png" runat="server" Width="20px" Height="20px" />
                                                                </td>
                                                                <td>
                                                                    <asp:Label ID="Lbl_Causale29" Font-Size="Small" runat="server" Text="" Width="200px"></asp:Label>
                                                                </td>
                                                            </tr>
                                                            <tr>
                                                                <td>
                                                                    <asp:ImageButton ID="Img_Causale30" ImageUrl="images/diurno_30.png" runat="server" Width="20px" Height="20px" />
                                                                </td>
                                                                <td>
                                                                    <asp:Label ID="Lbl_Causale30" Font-Size="Small" runat="server" Text="" Width="200px"></asp:Label>
                                                                </td>
                                                            </tr>
                                                            <tr>
                                                                <td>
                                                                    <asp:ImageButton ID="Img_Causale31" Font-Size="Small" ImageUrl="images/diurno_31.png" runat="server" Width="20px" Height="20px" />
                                                                </td>
                                                                <td>
                                                                    <asp:Label ID="Lbl_Causale31" Font-Size="Small" runat="server" Text="" Width="200px"></asp:Label>
                                                                </td>
                                                            </tr>
                                                            <tr>
                                                                <td>
                                                                    <asp:ImageButton ID="Img_Causale32" ImageUrl="images/diurno_32.png" runat="server" Width="20px" Height="20px" />
                                                                </td>
                                                                <td>
                                                                    <asp:Label ID="Lbl_Causale32" Font-Size="Small" runat="server" Text="" Width="200px"></asp:Label>
                                                                </td>
                                                            </tr>
                                                            <tr>
                                                                <td>
                                                                    <asp:ImageButton ID="Img_Causale33" ImageUrl="images/diurno_33.png" runat="server" Width="20px" Height="20px" />
                                                                </td>
                                                                <td>
                                                                    <asp:Label ID="Lbl_Causale33" Font-Size="Small" runat="server" Text="" Width="200px"></asp:Label>
                                                                </td>
                                                            </tr>
                                                            <tr>
                                                                <td>
                                                                    <asp:ImageButton ID="Img_Causale34" ImageUrl="images/diurno_34.png" runat="server" Width="20px" Height="20px" />
                                                                </td>
                                                                <td>
                                                                    <asp:Label ID="Lbl_Causale34" Font-Size="Small" runat="server" Text="" Width="200px"></asp:Label>
                                                                </td>
                                                            </tr>
                                                            <tr>
                                                                <td>
                                                                    <asp:ImageButton ID="Img_Causale35" ImageUrl="images/diurno_35.png" runat="server" Width="20px" Height="20px" />
                                                                </td>
                                                                <td>
                                                                    <asp:Label ID="Lbl_Causale35" Font-Size="Small" runat="server" Text="" Width="200px"></asp:Label>
                                                                </td>
                                                            </tr>
                                                            <tr>
                                                                <td>
                                                                    <asp:ImageButton ID="Img_Causale36" ImageUrl="images/diurno_37.png" runat="server" Width="20px" Height="20px" />
                                                                </td>
                                                                <td>
                                                                    <asp:Label ID="Lbl_Causale36" Font-Size="Small" runat="server" Text="" Width="200px"></asp:Label>
                                                                </td>
                                                            </tr>
                                                            <tr>
                                                                <td>
                                                                    <asp:ImageButton ID="Img_Causale37" ImageUrl="images/diurno_37.png" runat="server" Width="20px" Height="20px" />
                                                                </td>
                                                                <td>
                                                                    <asp:Label ID="Lbl_Causale37" Font-Size="Small" runat="server" Text="" Width="200px"></asp:Label>
                                                                </td>
                                                            </tr>
                                                            <tr>
                                                                <td>
                                                                    <asp:ImageButton ID="Img_Causale38" ImageUrl="images/diurno_38.png" runat="server" Width="20px" Height="20px" />
                                                                </td>
                                                                <td>
                                                                    <asp:Label ID="Lbl_Causale38" Font-Size="Small" runat="server" Text="" Width="200px"></asp:Label>
                                                                </td>
                                                            </tr>
                                                            <tr>
                                                                <td>
                                                                    <asp:ImageButton ID="Img_Causale39" ImageUrl="images/diurno_39.png" runat="server" Width="20px" Height="20px" />
                                                                </td>
                                                                <td>
                                                                    <asp:Label ID="Lbl_Causale39" Font-Size="Small" runat="server" Text="" Width="200px"></asp:Label>
                                                                </td>
                                                            </tr>
                                                            <tr>
                                                                <td>
                                                                    <asp:ImageButton ID="Img_Causale40" ImageUrl="images/diurno_40.png" runat="server" Width="20px" Height="20px" />
                                                                </td>
                                                                <td>
                                                                    <asp:Label ID="Lbl_Causale40" Font-Size="Small" runat="server" Text="" Width="200px"></asp:Label>
                                                                </td>
                                                            </tr>

                                                            <tr>
                                                                <td>
                                                                    <asp:ImageButton ID="Img_Causale99" ImageUrl="images/diurno_Blanco.png"
                                                                        runat="server" Width="20px" Height="20px" BorderColor="Black"
                                                                        BorderStyle="Solid" BorderWidth="1px" />
                                                                </td>
                                                                <td>
                                                                    <asp:Label ID="Lbl_Causale99" Font-Size="Small" runat="server" Text="Presente" Width="200px" BorderColor="Black" BorderWidth="1px"></asp:Label>
                                                                </td>
                                                            </tr>

                                                        </table>
                                                    </td>
                                                    <td>


                                                        <table style="width: 400px; height: 60px" cellpadding="0" cellspacing="0" class="Tabella">
                                                            <tr>
                                                                <td class="Testata" style="width: 14%; height: 22px">Lun</td>
                                                                <td class="Testata" style="width: 14%; height: 22px">Mar</td>
                                                                <td class="Testata" style="width: 14%; height: 22px">Mer</td>
                                                                <td class="Testata" style="width: 14%; height: 22px">Gio</td>
                                                                <td class="Testata" style="width: 14%; height: 22px">Ven</td>
                                                                <td class="Testata" style="width: 14%; height: 22px;">Sab</td>
                                                                <td class="Testata" style="width: 14%; height: 22px;">Dom</td>
                                                            </tr>
                                                            <tr>
                                                                <td class="CELLA" style="width: 14%; height: 40px;">
                                                                    <table cellpadding="0" cellspacing="0" style="width: 100%; height: 98%">
                                                                        <tr>
                                                                            <td class="GIORNO" style="height: 100%">
                                                                                <asp:Label ID="Lbl_Giorno1" runat="server" Text="1"></asp:Label>
                                                                            </td>
                                                                        </tr>
                                                                        <tr>
                                                                            <td style="height: 100%">
                                                                                <asp:ImageButton ID="Lnk_Giorno1" runat="server" CssClass="LinkGriglia" Width="100%" Height="100%" />
                                                                            </td>
                                                                        </tr>
                                                                    </table>
                                                                </td>
                                                                <td class="CELLA" style="width: 14%; height: 40px;">
                                                                    <table cellpadding="0" cellspacing="0" style="width: 100%; height: 98%">
                                                                        <tr>
                                                                            <td class="GIORNO" style="height: 100%">
                                                                                <asp:Label ID="Lbl_Giorno2" runat="server" Text="1"></asp:Label></td>
                                                                        </tr>
                                                                        <tr>
                                                                            <td style="height: 100%">
                                                                                <asp:ImageButton ID="Lnk_Giorno2" runat="server" Width="100%" CssClass="LinkGriglia" Height="100%" />

                                                                            </td>
                                                                        </tr>
                                                                    </table>

                                                                </td>
                                                                <td class="CELLA" style="width: 14%; height: 40px;">
                                                                    <table cellpadding="0" cellspacing="0" style="width: 100%; height: 98%">
                                                                        <tr>
                                                                            <td class="GIORNO" style="height: 100%">
                                                                                <asp:Label ID="Lbl_Giorno3" runat="server" Text="1"></asp:Label></td>
                                                                        </tr>
                                                                        <tr>
                                                                            <td style="height: 100%">
                                                                                <asp:ImageButton ID="Lnk_Giorno3" runat="server" Width="100%" CssClass="LinkGriglia" Height="100%" />
                                                                            </td>
                                                                        </tr>
                                                                    </table>
                                                                </td>
                                                                <td class="CELLA" style="width: 14%; height: 40px;">
                                                                    <table cellpadding="0" cellspacing="0" style="width: 100%; height: 98%">
                                                                        <tr>
                                                                            <td class="GIORNO" style="height: 100%">
                                                                                <asp:Label ID="Lbl_Giorno4" runat="server" Text="1"></asp:Label></td>
                                                                        </tr>
                                                                        <tr>
                                                                            <td style="height: 100%">
                                                                                <asp:ImageButton ID="Lnk_Giorno4" runat="server" Width="100%" CssClass="LinkGriglia" Height="100%" />
                                                                            </td>
                                                                        </tr>
                                                                    </table>

                                                                </td>
                                                                <td class="CELLA" style="width: 14%; height: 40px;">
                                                                    <table cellpadding="0" cellspacing="0" style="width: 100%; height: 98%">
                                                                        <tr>
                                                                            <td class="GIORNO" style="height: 100%">
                                                                                <asp:Label ID="Lbl_Giorno5" runat="server" Text="1"></asp:Label></td>
                                                                        </tr>
                                                                        <tr>
                                                                            <td style="height: 100%">
                                                                                <asp:ImageButton ID="Lnk_Giorno5" runat="server" Width="100%" CssClass="LinkGriglia" Height="100%" />
                                                                            </td>
                                                                        </tr>
                                                                    </table>
                                                                </td>
                                                                <td class="CELLA" style="width: 14%; height: 40px;">
                                                                    <table cellpadding="0" cellspacing="0" style="width: 100%; height: 98%">
                                                                        <tr>
                                                                            <td class="GIORNO" style="height: 100%">
                                                                                <asp:Label ID="Lbl_Giorno6" runat="server" Text="1"></asp:Label></td>
                                                                        </tr>
                                                                        <tr>
                                                                            <td style="height: 100%">
                                                                                <asp:ImageButton ID="Lnk_Giorno6" runat="server" Width="100%" CssClass="LinkGriglia" Height="100%" />
                                                                            </td>
                                                                        </tr>
                                                                    </table>

                                                                </td>
                                                                <td class="CELLA" style="width: 14%; height: 40px;">
                                                                    <table cellpadding="0" cellspacing="0" style="width: 100%; height: 98%">
                                                                        <tr>
                                                                            <td class="GIORNO" style="height: 100%">
                                                                                <asp:Label ID="Lbl_Giorno7" runat="server" Text="1"></asp:Label></td>
                                                                        </tr>
                                                                        <tr>
                                                                            <td style="height: 100%">
                                                                                <asp:ImageButton ID="Lnk_Giorno7" runat="server" Width="100%" CssClass="LinkGriglia" Height="100%" />
                                                                            </td>
                                                                        </tr>
                                                                    </table>

                                                                </td>
                                                            </tr>

                                                            <tr>
                                                                <td class="CELLA" style="width: 14%; height: 40px;">
                                                                    <table cellpadding="0" cellspacing="0" style="width: 100%; height: 98%">
                                                                        <tr>
                                                                            <td class="GIORNO" style="height: 100%">
                                                                                <asp:Label ID="Lbl_Giorno8" runat="server" Text="1"></asp:Label></td>
                                                                        </tr>
                                                                        <tr>
                                                                            <td style="height: 100%">
                                                                                <asp:ImageButton ID="Lnk_Giorno8" runat="server" Width="100%" CssClass="LinkGriglia" Height="100%" />
                                                                            </td>
                                                                        </tr>
                                                                    </table>
                                                                </td>
                                                                <td class="CELLA" style="width: 14%; height: 40px;">
                                                                    <table cellpadding="0" cellspacing="0" style="width: 100%; height: 98%">
                                                                        <tr>
                                                                            <td class="GIORNO" style="height: 100%">
                                                                                <asp:Label ID="Lbl_Giorno9" runat="server" Text="1"></asp:Label></td>
                                                                        </tr>
                                                                        <tr>
                                                                            <td style="height: 100%">
                                                                                <asp:ImageButton ID="Lnk_Giorno9" runat="server" Width="100%" CssClass="LinkGriglia" Height="100%" />
                                                                            </td>
                                                                        </tr>
                                                                    </table>

                                                                </td>
                                                                <td class="CELLA" style="width: 14%; height: 40px;">
                                                                    <table cellpadding="0" cellspacing="0" style="width: 100%; height: 98%">
                                                                        <tr>
                                                                            <td class="GIORNO" style="height: 100%">
                                                                                <asp:Label ID="Lbl_Giorno10" runat="server" Text="1"></asp:Label></td>
                                                                        </tr>
                                                                        <tr>
                                                                            <td style="height: 100%">
                                                                                <asp:ImageButton ID="Lnk_Giorno10" runat="server" Width="100%" CssClass="LinkGriglia" Height="100%" />
                                                                            </td>
                                                                        </tr>
                                                                    </table>
                                                                </td>
                                                                <td class="CELLA" style="width: 14%; height: 40px;">
                                                                    <table cellpadding="0" cellspacing="0" style="width: 100%; height: 98%">
                                                                        <tr>
                                                                            <td class="GIORNO" style="height: 100%">
                                                                                <asp:Label ID="Lbl_Giorno11" runat="server" Text="1"></asp:Label></td>
                                                                        </tr>
                                                                        <tr>
                                                                            <td style="height: 100%">
                                                                                <asp:ImageButton ID="Lnk_Giorno11" runat="server" Width="100%" CssClass="LinkGriglia" Height="100%" />
                                                                            </td>
                                                                        </tr>
                                                                    </table>

                                                                </td>
                                                                <td class="CELLA" style="width: 14%; height: 40px;">
                                                                    <table cellpadding="0" cellspacing="0" style="width: 100%; height: 98%">
                                                                        <tr>
                                                                            <td class="GIORNO" style="height: 100%">
                                                                                <asp:Label ID="Lbl_Giorno12" runat="server" Text="1"></asp:Label></td>
                                                                        </tr>
                                                                        <tr>
                                                                            <td style="height: 100%">
                                                                                <asp:ImageButton ID="Lnk_Giorno12" runat="server" Width="100%" CssClass="LinkGriglia" Height="100%" />
                                                                            </td>
                                                                        </tr>
                                                                    </table>
                                                                </td>
                                                                <td class="CELLA" style="width: 14%; height: 40px;">
                                                                    <table cellpadding="0" cellspacing="0" style="width: 100%; height: 98%">
                                                                        <tr>
                                                                            <td class="GIORNO" style="height: 100%">
                                                                                <asp:Label ID="Lbl_Giorno13" runat="server" Text="1"></asp:Label></td>
                                                                        </tr>
                                                                        <tr>
                                                                            <td style="height: 100%">
                                                                                <asp:ImageButton ID="Lnk_Giorno13" runat="server" Width="100%" CssClass="LinkGriglia" Height="100%" />
                                                                            </td>
                                                                        </tr>
                                                                    </table>

                                                                </td>
                                                                <td class="CELLA" style="width: 14%; height: 40px;">
                                                                    <table cellpadding="0" cellspacing="0" style="width: 100%; height: 98%">
                                                                        <tr>
                                                                            <td class="GIORNO" style="height: 100%">
                                                                                <asp:Label ID="Lbl_Giorno14" runat="server" Text="1"></asp:Label></td>
                                                                        </tr>
                                                                        <tr>
                                                                            <td style="height: 100%">
                                                                                <asp:ImageButton ID="Lnk_Giorno14" runat="server" Width="100%" CssClass="LinkGriglia" Height="100%" />
                                                                            </td>
                                                                        </tr>
                                                                    </table>

                                                                </td>
                                                            </tr>

                                                            <tr>
                                                                <td class="CELLA" style="width: 14%; height: 40px;">
                                                                    <table cellpadding="0" cellspacing="0" style="width: 100%; height: 98%">
                                                                        <tr>
                                                                            <td class="GIORNO" style="height: 100%">
                                                                                <asp:Label ID="Lbl_Giorno15" runat="server" Text="1"></asp:Label></td>
                                                                        </tr>
                                                                        <tr>
                                                                            <td style="height: 100%">
                                                                                <asp:ImageButton ID="Lnk_Giorno15" runat="server" Width="100%" CssClass="LinkGriglia" Height="100%" />
                                                                            </td>
                                                                        </tr>
                                                                    </table>
                                                                </td>
                                                                <td class="CELLA" style="width: 14%; height: 40px;">
                                                                    <table cellpadding="0" cellspacing="0" style="width: 100%; height: 98%">
                                                                        <tr>
                                                                            <td class="GIORNO" style="height: 100%">
                                                                                <asp:Label ID="Lbl_Giorno16" runat="server" Text="1"></asp:Label></td>
                                                                        </tr>
                                                                        <tr>
                                                                            <td style="height: 100%">
                                                                                <asp:ImageButton ID="Lnk_Giorno16" runat="server" Width="100%" CssClass="LinkGriglia" Height="100%" />
                                                                            </td>
                                                                        </tr>
                                                                    </table>

                                                                </td>
                                                                <td class="CELLA" style="width: 14%; height: 40px;">
                                                                    <table cellpadding="0" cellspacing="0" style="width: 100%; height: 98%">
                                                                        <tr>
                                                                            <td class="GIORNO" style="height: 100%">
                                                                                <asp:Label ID="Lbl_Giorno17" runat="server" Text="1"></asp:Label></td>
                                                                        </tr>
                                                                        <tr>
                                                                            <td style="height: 100%">
                                                                                <asp:ImageButton ID="Lnk_Giorno17" runat="server" Width="100%" CssClass="LinkGriglia" Height="100%" />
                                                                            </td>
                                                                        </tr>
                                                                    </table>
                                                                </td>
                                                                <td class="CELLA" style="width: 14%; height: 40px;">
                                                                    <table cellpadding="0" cellspacing="0" style="width: 100%; height: 98%">
                                                                        <tr>
                                                                            <td class="GIORNO" style="height: 100%">
                                                                                <asp:Label ID="Lbl_Giorno18" runat="server" Text="1"></asp:Label></td>
                                                                        </tr>
                                                                        <tr>
                                                                            <td style="height: 100%">
                                                                                <asp:ImageButton ID="Lnk_Giorno18" runat="server" Width="100%" CssClass="LinkGriglia" Height="100%" />
                                                                            </td>
                                                                        </tr>
                                                                    </table>

                                                                </td>
                                                                <td class="CELLA" style="width: 14%; height: 40px;">
                                                                    <table cellpadding="0" cellspacing="0" style="width: 100%; height: 98%">
                                                                        <tr>
                                                                            <td class="GIORNO" style="height: 100%">
                                                                                <asp:Label ID="Lbl_Giorno19" runat="server" Text="1"></asp:Label></td>
                                                                        </tr>
                                                                        <tr>
                                                                            <td style="height: 100%">
                                                                                <asp:ImageButton ID="Lnk_Giorno19" runat="server" Width="100%" CssClass="LinkGriglia" Height="100%" />
                                                                            </td>
                                                                        </tr>
                                                                    </table>
                                                                </td>
                                                                <td class="CELLA" style="width: 14%; height: 40px;">
                                                                    <table cellpadding="0" cellspacing="0" style="width: 100%; height: 98%">
                                                                        <tr>
                                                                            <td class="GIORNO" style="height: 100%">
                                                                                <asp:Label ID="Lbl_Giorno20" runat="server" Text="1"></asp:Label></td>
                                                                        </tr>
                                                                        <tr>
                                                                            <td style="height: 100%">
                                                                                <asp:ImageButton ID="Lnk_Giorno20" runat="server" Width="100%" CssClass="LinkGriglia" Height="100%" />
                                                                            </td>
                                                                        </tr>
                                                                    </table>

                                                                </td>
                                                                <td class="CELLA" style="width: 14%; height: 40px;">
                                                                    <table cellpadding="0" cellspacing="0" style="width: 100%; height: 98%">
                                                                        <tr>
                                                                            <td class="GIORNO" style="height: 100%">
                                                                                <asp:Label ID="Lbl_Giorno21" runat="server" Text="1"></asp:Label></td>
                                                                        </tr>
                                                                        <tr>
                                                                            <td style="height: 100%">
                                                                                <asp:ImageButton ID="Lnk_Giorno21" runat="server" Width="100%" CssClass="LinkGriglia" Height="100%" />
                                                                            </td>
                                                                        </tr>
                                                                    </table>

                                                                </td>
                                                            </tr>

                                                            <tr>
                                                                <td class="CELLA" style="width: 14%; height: 40px;">
                                                                    <table cellpadding="0" cellspacing="0" style="width: 100%; height: 98%">
                                                                        <tr>
                                                                            <td class="GIORNO" style="height: 100%">
                                                                                <asp:Label ID="Lbl_Giorno22" runat="server" Text="1"></asp:Label></td>
                                                                        </tr>
                                                                        <tr>
                                                                            <td style="height: 100%">
                                                                                <asp:ImageButton ID="Lnk_Giorno22" runat="server" Width="100%" CssClass="LinkGriglia" Height="100%" />
                                                                            </td>
                                                                        </tr>
                                                                    </table>
                                                                </td>
                                                                <td class="CELLA" style="width: 14%; height: 40px;">
                                                                    <table cellpadding="0" cellspacing="0" style="width: 100%; height: 98%">
                                                                        <tr>
                                                                            <td class="GIORNO" style="height: 100%">
                                                                                <asp:Label ID="Lbl_Giorno23" runat="server" Text="1"></asp:Label></td>
                                                                        </tr>
                                                                        <tr>
                                                                            <td style="height: 100%">
                                                                                <asp:ImageButton ID="Lnk_Giorno23" runat="server" Width="100%" CssClass="LinkGriglia" Height="100%" />
                                                                            </td>
                                                                        </tr>
                                                                    </table>

                                                                </td>
                                                                <td class="CELLA" style="width: 14%; height: 40px;">
                                                                    <table cellpadding="0" cellspacing="0" style="width: 100%; height: 98%">
                                                                        <tr>
                                                                            <td class="GIORNO" style="height: 100%">
                                                                                <asp:Label ID="Lbl_Giorno24" runat="server" Text="1"></asp:Label></td>
                                                                        </tr>
                                                                        <tr>
                                                                            <td style="height: 100%">
                                                                                <asp:ImageButton ID="Lnk_Giorno24" runat="server" Width="100%" CssClass="LinkGriglia" Height="100%" />
                                                                            </td>
                                                                        </tr>
                                                                    </table>
                                                                </td>
                                                                <td class="CELLA" style="width: 14%; height: 40px;">
                                                                    <table cellpadding="0" cellspacing="0" style="width: 100%; height: 98%">
                                                                        <tr>
                                                                            <td class="GIORNO" style="height: 100%">
                                                                                <asp:Label ID="Lbl_Giorno25" runat="server" Text="1"></asp:Label></td>
                                                                        </tr>
                                                                        <tr>
                                                                            <td style="height: 100%">
                                                                                <asp:ImageButton ID="Lnk_Giorno25" runat="server" Width="100%" CssClass="LinkGriglia" Height="100%" />
                                                                            </td>
                                                                        </tr>
                                                                    </table>

                                                                </td>
                                                                <td class="CELLA" style="width: 14%; height: 40px;">
                                                                    <table cellpadding="0" cellspacing="0" style="width: 100%; height: 98%">
                                                                        <tr>
                                                                            <td class="GIORNO" style="height: 100%">
                                                                                <asp:Label ID="Lbl_Giorno26" runat="server" Text="1"></asp:Label></td>
                                                                        </tr>
                                                                        <tr>
                                                                            <td style="height: 100%">
                                                                                <asp:ImageButton ID="Lnk_Giorno26" runat="server" Width="100%" CssClass="LinkGriglia" Height="100%" />
                                                                            </td>
                                                                        </tr>
                                                                    </table>
                                                                </td>
                                                                <td class="CELLA" style="width: 14%; height: 40px;">
                                                                    <table cellpadding="0" cellspacing="0" style="width: 100%; height: 98%">
                                                                        <tr>
                                                                            <td class="GIORNO" style="height: 100%">
                                                                                <asp:Label ID="Lbl_Giorno27" runat="server" Text="1"></asp:Label></td>
                                                                        </tr>
                                                                        <tr>
                                                                            <td style="height: 100%">
                                                                                <asp:ImageButton ID="Lnk_Giorno27" runat="server" Width="100%" CssClass="LinkGriglia" Height="100%" />
                                                                            </td>
                                                                        </tr>
                                                                    </table>

                                                                </td>
                                                                <td class="CELLA" style="width: 14%; height: 40px;">
                                                                    <table cellpadding="0" cellspacing="0" style="width: 100%; height: 98%">
                                                                        <tr>
                                                                            <td class="GIORNO" style="height: 100%">
                                                                                <asp:Label ID="Lbl_Giorno28" runat="server" Text="1"></asp:Label></td>
                                                                        </tr>
                                                                        <tr>
                                                                            <td style="height: 100%">
                                                                                <asp:ImageButton ID="Lnk_Giorno28" runat="server" Width="100%" CssClass="LinkGriglia" Height="100%" />
                                                                            </td>
                                                                        </tr>
                                                                    </table>

                                                                </td>
                                                            </tr>
                                                            <tr>
                                                                <td class="CELLA" style="width: 14%; height: 40px;">
                                                                    <table cellpadding="0" cellspacing="0" style="width: 100%; height: 98%">
                                                                        <tr>
                                                                            <td class="GIORNO" style="height: 100%">
                                                                                <asp:Label ID="Lbl_Giorno29" runat="server" Text="1"></asp:Label></td>
                                                                        </tr>
                                                                        <tr>
                                                                            <td style="height: 100%">
                                                                                <asp:ImageButton ID="Lnk_Giorno29" runat="server" Width="100%" CssClass="LinkGriglia" Height="100%" />
                                                                            </td>
                                                                        </tr>
                                                                    </table>
                                                                </td>
                                                                <td class="CELLA" style="width: 14%; height: 40px;">
                                                                    <table cellpadding="0" cellspacing="0" style="width: 100%; height: 98%">
                                                                        <tr>
                                                                            <td class="GIORNO" style="height: 100%">
                                                                                <asp:Label ID="Lbl_Giorno30" runat="server" Text="1"></asp:Label></td>
                                                                        </tr>
                                                                        <tr>
                                                                            <td style="height: 100%">
                                                                                <asp:ImageButton ID="Lnk_Giorno30" runat="server" Width="100%" CssClass="LinkGriglia" Height="100%" />
                                                                            </td>
                                                                        </tr>
                                                                    </table>

                                                                </td>
                                                                <td class="CELLA" style="width: 14%; height: 40px;">
                                                                    <table cellpadding="0" cellspacing="0" style="width: 100%; height: 98%">
                                                                        <tr>
                                                                            <td class="GIORNO" style="height: 100%">
                                                                                <asp:Label ID="Lbl_Giorno31" runat="server" Text="1"></asp:Label></td>
                                                                        </tr>
                                                                        <tr>
                                                                            <td style="height: 100%">
                                                                                <asp:ImageButton ID="Lnk_Giorno31" runat="server" Width="100%" CssClass="LinkGriglia" Height="100%" />
                                                                            </td>
                                                                        </tr>
                                                                    </table>
                                                                </td>
                                                                <td class="CELLA" style="width: 14%; height: 40px;">
                                                                    <table cellpadding="0" cellspacing="0" style="width: 100%; height: 98%">
                                                                        <tr>
                                                                            <td class="GIORNO" style="height: 100%">
                                                                                <asp:Label ID="Lbl_Giorno32" runat="server" Text="1"></asp:Label></td>
                                                                        </tr>
                                                                        <tr>
                                                                            <td style="height: 100%">
                                                                                <asp:ImageButton ID="Lnk_Giorno32" runat="server" Width="100%" CssClass="LinkGriglia" Height="100%" />
                                                                            </td>
                                                                        </tr>
                                                                    </table>

                                                                </td>
                                                                <td class="CELLA" style="width: 14%; height: 40px;">
                                                                    <table cellpadding="0" cellspacing="0" style="width: 100%; height: 98%">
                                                                        <tr>
                                                                            <td class="GIORNO" style="height: 100%">
                                                                                <asp:Label ID="Lbl_Giorno33" runat="server" Text="1"></asp:Label></td>
                                                                        </tr>
                                                                        <tr>
                                                                            <td style="height: 100%">
                                                                                <asp:ImageButton ID="Lnk_Giorno33" runat="server" Width="100%" CssClass="LinkGriglia" Height="100%" />
                                                                            </td>
                                                                        </tr>
                                                                    </table>
                                                                </td>
                                                                <td class="CELLA" style="width: 14%; height: 40px;">
                                                                    <table cellpadding="0" cellspacing="0" style="width: 100%; height: 98%">
                                                                        <tr>
                                                                            <td class="GIORNO" style="height: 100%">
                                                                                <asp:Label ID="Lbl_Giorno34" runat="server" Text="1"></asp:Label></td>
                                                                        </tr>
                                                                        <tr>
                                                                            <td style="height: 100%">
                                                                                <asp:ImageButton ID="Lnk_Giorno34" runat="server" Width="100%" CssClass="LinkGriglia" Height="100%" />
                                                                            </td>
                                                                        </tr>
                                                                    </table>

                                                                </td>
                                                                <td class="CELLA" style="width: 14%; height: 40px;">
                                                                    <table cellpadding="0" cellspacing="0" style="width: 100%; height: 98%">
                                                                        <tr>
                                                                            <td class="GIORNO" style="height: 100%">
                                                                                <asp:Label ID="Lbl_Giorno35" runat="server" Text="1"></asp:Label></td>
                                                                        </tr>
                                                                        <tr>
                                                                            <td style="height: 100%">
                                                                                <asp:ImageButton ID="Lnk_Giorno35" runat="server" Width="100%" CssClass="LinkGriglia" Height="100%" />
                                                                            </td>
                                                                        </tr>
                                                                    </table>

                                                                </td>
                                                            </tr>


                                                            <tr>
                                                                <td class="CELLA" style="width: 14%; height: 40px;">
                                                                    <table cellpadding="0" cellspacing="0" style="width: 100%; height: 98%">
                                                                        <tr>
                                                                            <td class="GIORNO" style="height: 100%">
                                                                                <asp:Label ID="Lbl_Giorno36" runat="server" Text="1"></asp:Label></td>
                                                                        </tr>
                                                                        <tr>
                                                                            <td style="height: 100%">
                                                                                <asp:ImageButton ID="Lnk_Giorno36" runat="server" Width="100%" CssClass="LinkGriglia" Height="100%" />
                                                                            </td>
                                                                        </tr>
                                                                    </table>
                                                                </td>
                                                                <td class="CELLA" style="width: 14%; height: 40px;">
                                                                    <table cellpadding="0" cellspacing="0" style="width: 100%; height: 98%">
                                                                        <tr>
                                                                            <td class="GIORNO" style="height: 100%">
                                                                                <asp:Label ID="Lbl_Giorno37" runat="server" Text="1"></asp:Label></td>
                                                                        </tr>
                                                                        <tr>
                                                                            <td style="height: 100%">
                                                                                <asp:ImageButton ID="Lnk_Giorno37" runat="server" Width="100%" CssClass="LinkGriglia" Height="100%" />
                                                                            </td>
                                                                        </tr>
                                                                    </table>

                                                                </td>
                                                                <td class="CELLA" style="width: 14%; height: 40px;">
                                                                    <table cellpadding="0" cellspacing="0" style="width: 100%; height: 98%">
                                                                        <tr>
                                                                            <td class="GIORNO" style="height: 100%">
                                                                                <asp:Label ID="Lbl_Giorno38" runat="server" Text="1"></asp:Label></td>
                                                                        </tr>
                                                                        <tr>
                                                                            <td style="height: 100%">
                                                                                <asp:ImageButton ID="Lnk_Giorno38" runat="server" Width="100%" CssClass="LinkGriglia" Height="100%" />
                                                                            </td>
                                                                        </tr>
                                                                    </table>
                                                                </td>
                                                                <td class="CELLA" style="width: 14%; height: 40px;">
                                                                    <table cellpadding="0" cellspacing="0" style="width: 100%; height: 98%">
                                                                        <tr>
                                                                            <td class="GIORNO" style="height: 100%">
                                                                                <asp:Label ID="Lbl_Giorno39" runat="server" Text="1"></asp:Label></td>
                                                                        </tr>
                                                                        <tr>
                                                                            <td style="height: 100%">
                                                                                <asp:ImageButton ID="Lnk_Giorno39" runat="server" Width="100%" CssClass="LinkGriglia" Height="100%" />
                                                                            </td>
                                                                        </tr>
                                                                    </table>

                                                                </td>
                                                                <td class="CELLA" style="width: 14%; height: 40px;">
                                                                    <table cellpadding="0" cellspacing="0" style="width: 100%; height: 98%">
                                                                        <tr>
                                                                            <td class="GIORNO" style="height: 100%">
                                                                                <asp:Label ID="Lbl_Giorno40" runat="server" Text="1"></asp:Label></td>
                                                                        </tr>
                                                                        <tr>
                                                                            <td style="height: 100%">
                                                                                <asp:ImageButton ID="Lnk_Giorno40" runat="server" Width="100%" CssClass="LinkGriglia" Height="100%" />
                                                                            </td>
                                                                        </tr>
                                                                    </table>
                                                                </td>
                                                                <td class="CELLA" style="width: 14%; height: 40px;">
                                                                    <table cellpadding="0" cellspacing="0" style="width: 100%; height: 98%">
                                                                        <tr>
                                                                            <td class="GIORNO" style="height: 100%">
                                                                                <asp:Label ID="Lbl_Giorno41" runat="server" Text="1"></asp:Label></td>
                                                                        </tr>
                                                                        <tr>
                                                                            <td style="height: 100%">
                                                                                <asp:ImageButton ID="Lnk_Giorno41" runat="server" Width="100%" CssClass="LinkGriglia" Height="100%" />
                                                                            </td>
                                                                        </tr>
                                                                    </table>

                                                                </td>
                                                                <td class="CELLA" style="width: 14%; height: 40px;">
                                                                    <table cellpadding="0" cellspacing="0" style="width: 100%; height: 98%">
                                                                        <tr>
                                                                            <td class="GIORNO" style="height: 100%">
                                                                                <asp:Label ID="Lbl_Giorno42" runat="server" Text="1"></asp:Label></td>
                                                                        </tr>
                                                                        <tr>
                                                                            <td style="height: 100%">
                                                                                <asp:ImageButton ID="Lnk_Giorno42" runat="server" Width="100%" CssClass="LinkGriglia" Height="100%" />
                                                                            </td>
                                                                        </tr>
                                                                    </table>

                                                                </td>
                                                            </tr>
                                                        </table>

                                                        <br />
                                                        <br />
                                                        <asp:Label ID="lblTotale" runat="server" Tex=""></asp:Label>
                                                    </td>

                                                </tr>

                                            </table>


                                        </ContentTemplate>
                                    </asp:UpdatePanel>

                                </ContentTemplate>
                            </xasp:TabPanel>
                        </xasp:TabContainer>
                    </td>
                </tr>
            </table>
        </div>

    </form>
</body>
</html>
