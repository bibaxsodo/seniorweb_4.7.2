﻿Imports System
Imports System.Web
Imports Microsoft.VisualBasic
Imports System.Data.OleDb
Imports System.Web.Hosting
Imports System.Data.SqlTypes
Imports System.Net
Imports System.Collections.Generic
Imports System.IO
Imports System.Security.Cryptography.X509Certificates
Imports System.Net.Security
Imports System.Web.Script.Serialization
Imports org.jivesoftware.util
Imports Newtonsoft.Json
Imports Newtonsoft.Json.Linq

Partial Class OspitiWeb_Menu_Export
    Inherits System.Web.UI.Page

    Protected Sub Btn_Esci_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles Btn_Esci.Click
        Response.Redirect("Menu_Ospiti.aspx")
    End Sub

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load

        If Trim(Session("UTENTE")) = "" Then
            Response.Redirect("..\Login.aspx")
            Exit Sub
        End If

        If Session("ABILITAZIONI").IndexOf("<PORTINERIA>") > 0 Then
            Response.Redirect("..\MainPortineria.aspx")
        End If

        Dim Barra As New Cls_BarraSenior

        Lbl_BarraSenior.Text = Barra.CodiceBarra(Application("SENIOR"), Session("UTENTE"), Page)


        Dim DatiGenerali As New Cls_DatiGenerali

        DatiGenerali.LeggiDati(Session("DC_TABELLE"))

        If DatiGenerali.FatturazionePrivati = 1 Then
            lbl_fatmaxnew.Text = "FAT XML (Privati E PA)"
        Else
            lbl_fatmaxnew.Text = "FAT XML"
        End If



    End Sub

    Function campodb(ByVal oggetto As Object) As String
        If IsDBNull(oggetto) Then
            Return ""
        Else
            Return oggetto
        End If
    End Function



    Protected Sub ImgTesseraTs_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles ImgTesseraTs.Click
        Dim k As New Cls_Login

        k.Utente = Session("UTENTE")
        k.LeggiSP(Application("SENIOR"))

        Dim Utente As String
        Utente = Session("UTENTE")

        For i = 1 To 99
            Utente = Utente.Replace("<" & i & ">", "")
        Next

        Response.Redirect("https://seniorweb.e-personam.com/Invio730TS/Invio730TS.aspx?Utente=" & Utente & "&Societa=" & k.RagioneSociale & "&Data=" & Format(Now, "dd/MM/yyyy") & "&Url=" & Request.Url.ToString)
    End Sub

    Protected Sub ImgFatXML_Click(sender As Object, e As ImageClickEventArgs) Handles ImgFatXML.Click

        Dim DatiGenerali As New Cls_DatiGenerali

        DatiGenerali.LeggiDati(Session("DC_TABELLE"))

        If DatiGenerali.FatturazionePrivati = 1 Then
            Response.Redirect("../GeneraleWeb/FatturaElettronicaPrivati.aspx")
        Else
            Response.Redirect("../GeneraleWeb/FatturazioneElettronica.aspx")
        End If

        '../GeneraleWeb/FatturazioneElettronica.aspx


    End Sub

    Protected Sub Img_ExportDocumenti_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles Img_ExportDocumenti.Click
        Dim Param As New Cls_Parametri


        Param.LeggiParametri(Session("DC_OSPITE"))

        If Param.TipoExport = "Documenti Incassi" Or Param.TipoExport = "Documenti Incassi NO FTP" Then
            Response.Redirect("Export\Export_DocumentiIncassi.aspx")
            Exit Sub
        End If
        'EUSIS
        If Param.TipoExport = "EUSIS" Then
            Response.Redirect("Export\Export_DocumentiEusis.aspx")
            Exit Sub
        End If

        If Param.TipoExport = "POLIFARMA" Then
            Response.Redirect("Export\Export_DocumentiPoliforma.aspx")
            Exit Sub
        End If

        If Param.TipoExport = "BPOINT" Then
            Response.Redirect("Export\Export_DocumentiBPOINT.aspx")
            Exit Sub
        End If



        If Param.TipoExport = "SISTEMI" Then
            Response.Redirect("Export\Export_DocumentiSistemiIC.aspx")
            Exit Sub
        End If



        If Param.TipoExport = "ADHOC GABBIANO" Then
            Response.Redirect("Export\Export_Adhoc.aspx")
            Exit Sub
        End If

        '"SISTEMA UNO IST.C."
        If Param.TipoExport = "SISTEMA UNO IST.C." Then
            Response.Redirect("Export\Export_IstitutoCiechi.aspx")
            Exit Sub
        End If

        Response.Redirect("Export_Documenti.aspx")

    End Sub

    Protected Sub ImgImportExcelToXml_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles ImgImportExcelToXml.Click
        Dim k As New Cls_Login

        k.Utente = Session("UTENTE")
        k.LeggiSP(Application("SENIOR"))

        Dim Utente As String
        Utente = Session("UTENTE")

        For i = 1 To 99
            Utente = Utente.Replace("<" & i & ">", "")
        Next


        Response.Redirect("https://seniorweb.e-personam.com/Invio730TS/ImportExcel730.aspx?Utente=" & Utente & "&Societa=" & k.RagioneSociale & "&Data=" & Format(Now, "dd/MM/yyyy") & "&Url=" & Request.Url.ToString)
    End Sub
End Class
