﻿Imports Microsoft.VisualBasic
Imports System.Data.OleDb
Imports System.Web.Hosting


Partial Class OspitiWeb_StampaRicevutaIncasso
    Inherits System.Web.UI.Page

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Page.IsPostBack = True Then
            Exit Sub
        End If


        Dim Reg As New Cls_MovimentoContabile

        Reg.Leggi(Session("DC_GENERALE"), Request.Item("NumeroRegistrazione"))
        Dim CauCon As New Cls_CausaleContabile

        CauCon.Leggi(Session("DC_TABELLE"), Reg.CausaleContabile)

        If CauCon.Tipo = "P" Then
            Call stamparicevuta(Request.Item("NumeroRegistrazione"))
        Else

            Session("CampoProgressBar") = 0
            Session("RagioneSocialeFattura") = ""

            Dim k As New Cls_StampaFatture

            k.CreaRecordStampa(Reg.AnnoCompetenza, Reg.MeseCompetenza, False, Reg.NumeroProtocollo, Reg.NumeroProtocollo, Year(Reg.DataRegistrazione), Reg.RegistroIVA, "", "", "", 0, Session, 0, 0)

            Timer1.Interval = 1000
            Timer1.Enabled = True
        End If
    End Sub


    Private Sub stamparicevuta(ByVal numeroregistrazione As Long)
        Dim StringaConnessioneStampa As String = Session("STAMPEFINANZIARIA")

        Dim Stampa As New StampeGenerale



        ViewState("PRINTERKEY") = Format(Now, "yyyyMMddHHmmss") & Session.SessionID

        Dim Reg As New Cls_MovimentoContabile

        Reg.Leggi(Session("DC_GENERALE"), numeroregistrazione)



        Dim MySql As String


        Dim MyRecordStampaT As System.Data.DataRow
        MyRecordStampaT = Stampa.Tables("PagamentoIncasso").NewRow


        Dim XCs As New Cls_CentroServizio
        XCs.Leggi(Session("DC_OSPITE"), Session("CODICESERVIZIO"))
        Dim Cognome As String
        Dim Indirizzo As String
        Dim Comune As String
        Dim CodiceFiscale As String
        Dim CAP As String

        If Val(Request.Item("OspiteParenti")) = 99 Then
            Dim kOsp As New ClsOspite

            kOsp.Leggi(Session("DC_OSPITE"), Session("CODICEOSPITE"))
            Cognome = kOsp.Nome
            Indirizzo = kOsp.RESIDENZAINDIRIZZO1
            Dim DecCom As New ClsComune

            DecCom.Comune = kOsp.RESIDENZACOMUNE1
            DecCom.Provincia = kOsp.RESIDENZAPROVINCIA1
            DecCom.DecodficaComune(Session("DC_OSPITE"))
            Comune = DecCom.Descrizione
            CodiceFiscale = kOsp.CODICEFISCALE
            CAP = kOsp.RESIDENZACAP1
        Else
            Dim kPar As New Cls_Parenti

            kPar.Leggi(Session("DC_OSPITE"), Session("CODICEOSPITE"), Val(Request.Item("OspiteParenti")))
            Cognome = kPar.Nome
            Indirizzo = kPar.RESIDENZAINDIRIZZO1
            Dim DecCom As New ClsComune

            DecCom.Comune = kPar.RESIDENZACOMUNE1
            DecCom.Provincia = kPar.RESIDENZAPROVINCIA1
            DecCom.DecodficaComune(Session("DC_OSPITE"))
            Comune = DecCom.Descrizione
            CodiceFiscale = kPar.CODICEFISCALE
            CAP = kPar.RESIDENZACAP1
        End If

        Dim Xs As New Cls_ImportoInLettere

        MyRecordStampaT.Item("DataStampa") = Format(Reg.DataRegistrazione, "dd/MM/yyyy")
        MyRecordStampaT.Item("NUMEROREGISTRAZIONE") = numeroregistrazione
        MyRecordStampaT.Item("COGNOMENOME") = Cognome
        MyRecordStampaT.Item("Indirizzo") = Indirizzo
        MyRecordStampaT.Item("Cap") = Comune
        MyRecordStampaT.Item("Comune") = CAP
        MyRecordStampaT.Item("CodiceFiscale") = CodiceFiscale
        MyRecordStampaT.Item("ImportoInLettere") = Xs.ImportoInTesto(Reg.Righe(0).Importo, 30)
        MyRecordStampaT.Item("ImportoTotale") = Format(Reg.Righe(0).Importo, "#,##0.00")

        Dim NomeOspite As New ClsOspite

        NomeOspite.Leggi(Session("DC_OSPITE"), Session("CODICEOSPITE"))
        MyRecordStampaT.Item("NOMEOSPITE") = NomeOspite.Nome

        MyRecordStampaT.Item("NumeroBolletta") = Reg.NumeroBolletta
        If IsDate(Reg.DataBolletta) Then
            If Year(Reg.DataBolletta) < 1940 Then
                MyRecordStampaT.Item("DataBolletta") = ""
            Else
                MyRecordStampaT.Item("DataBolletta") = Reg.DataBolletta
            End If
        Else
            MyRecordStampaT.Item("DataBolletta") = Nothing
        End If
        MyRecordStampaT.Item("Descrizione") = Reg.Descrizione

        Dim CentroServizio As New Cls_CentroServizio

        CentroServizio.Leggi(Session("DC_OSPITE"), Session("CODICESERVIZIO"))

        MyRecordStampaT.Item("CentroServizio") = Session("CODICESERVIZIO")

        Dim CauCon As New Cls_CausaleContabile

        CauCon.Leggi(Session("DC_TABELLE"), Reg.CausaleContabile)

        MyRecordStampaT.Item("TipoDocumento") = CauCon.Descrizione

        'If CauCon.TipoDocumento = "NC" Then
        '    MyRecordStampaT.Item("TipoDocumento") = "NOTA CREDITO"
        'Else
        '    MyRecordStampaT.Item("TipoDocumento") = "RICEVUTA"
        'End If

        Stampa.Tables("PagamentoIncasso").Rows.Add(MyRecordStampaT)


        Dim MyRecordStampaR As System.Data.DataRow


        Dim ks As New Cls_Legami
        Dim i As Long
        Dim Collegata As Boolean = False

        ks.Leggi(Session("DC_GENERALE"), 0, numeroregistrazione)

        For i = 0 To 100

            If Not IsNothing(ks.Importo(i)) Then
                If Math.Abs(ks.Importo(i)) > 0 Then


                    Dim RegDoc As New Cls_MovimentoContabile

                    RegDoc.Leggi(Session("DC_GENERALE"), ks.NumeroDocumento(i))

                    Dim CR As Integer

                    For CR = 0 To RegDoc.Righe.LongLength - 1
                        If Not IsNothing(RegDoc.Righe(CR)) Then
                            If RegDoc.Righe(CR).RigaDaCausale = 3 Or RegDoc.Righe(CR).RigaDaCausale = 4 Or RegDoc.Righe(CR).RigaDaCausale = 5 Or RegDoc.Righe(CR).RigaDaCausale = 6 Then
                                MyRecordStampaR = Stampa.Tables("RighePagamentIIncassi").NewRow

                                MyRecordStampaR.Item("numeroregistrazione") = numeroregistrazione
                                MyRecordStampaR.Item("DataDocumento") = RegDoc.DataDocumento
                                MyRecordStampaR.Item("NumeroDocumento") = RegDoc.NumeroDocumento
                                MyRecordStampaR.Item("Importo") = Format(RegDoc.Righe(CR).Importo, "#,##0.00")
                                MyRecordStampaR.Item("Descrizione") = RegDoc.Righe(CR).Descrizione

                                MyRecordStampaR.Item("TotaleImporto") = Format(RegDoc.ImportoDocumento(Session("DC_TABELLE")), "#,##0.00")

                                Stampa.Tables("RighePagamentIIncassi").Rows.Add(MyRecordStampaR)
                            End If
                        End If
                    Next
                    Collegata = True
                End If
            End If
        Next
        

        Session("stampa") = Stampa

        Response.Redirect("Stampa_ReportXSD.aspx?REPORT=RICEVUTA&PRINTERKEY=OFF")
    End Sub


    Function campodb(ByVal oggetto As Object) As String
        If IsDBNull(oggetto) Then
            Return ""
        Else
            Return oggetto
        End If
    End Function

    Function campodbN(ByVal oggetto As Object) As Double
        If IsDBNull(oggetto) Then
            Return 0
        Else
            Return oggetto
        End If
    End Function

    Protected Sub Timer1_Tick(ByVal sender As Object, ByVal e As System.EventArgs) Handles Timer1.Tick
        If Val(Session("CampoProgressBar")) > 100 Then            
            Timer1.Enabled = False
            Session("CampoProgressBar") = 0

            Session("SelectionFormula") = "{FatturaTesta.PRINTERKEY} = " & Chr(34) & Session.SessionID & Chr(34) & " AND " & "{FatturaRighe.PRINTERKEY} = " & Chr(34) & Session.SessionID & Chr(34)

            Dim XS As New Cls_Login

            XS.Utente = Session("UTENTE")
            XS.LeggiSP(Application("SENIOR"))

            If Session("ReportPersonalizzato") <> "" Then
                ScriptManager.RegisterClientScriptBlock(Me, Me.GetType(), "Stampar1", "openPopUp('Stampa_ReportXSD.aspx?REPORT=" & Session("ReportPersonalizzato") & "&PRINTERKEY=ON','Stamper1','width=800,height=600');", True)
            Else
                If XS.ReportPersonalizzato("STAMPADOCUMENTIOSPITI2") <> "" Then
                    ScriptManager.RegisterClientScriptBlock(Me, Me.GetType(), "Stampa2", "openPopUp('Stampa_ReportXSD.aspx?REPORT=STAMPADOCUMENTIOSPITI2&PRINTERKEY=ON','Stampe2','width=800,height=600');", True)
                End If
                If XS.ReportPersonalizzato("STAMPADOCUMENTIOSPITI3") <> "" Then
                    ScriptManager.RegisterClientScriptBlock(Me, Me.GetType(), "Stampa3", "openPopUp('Stampa_ReportXSD.aspx?REPORT=STAMPADOCUMENTIOSPITI3&PRINTERKEY=ON','Stampe3','width=800,height=600');", True)
                End If
                If XS.ReportPersonalizzato("STAMPADOCUMENTIOSPITI4") <> "" Then
                    ScriptManager.RegisterClientScriptBlock(Me, Me.GetType(), "Stampa4", "openPopUp('Stampa_ReportXSD.aspx?REPORT=STAMPADOCUMENTIOSPITI4&PRINTERKEY=ON','Stampe4','width=800,height=600');", True)
                End If
                If XS.ReportPersonalizzato("STAMPADOCUMENTIOSPITI1") <> "" Then
                    ScriptManager.RegisterClientScriptBlock(Me, Me.GetType(), "Stampa1", "openPopUp('Stampa_ReportXSD.aspx?REPORT=STAMPADOCUMENTIOSPITI1&PRINTERKEY=ON','Stampe1','width=800,height=600');", True)
                    'Response.Redirect("Stampa_ReportXSD.aspx?REPORT=STAMPADOCUMENTIOSPITI1&PRINTERKEY=ON")
                End If
            End If

            REM ScriptManager.RegisterClientScriptBlock(Me, Me.GetType(), "Stampa1", "window.close()", True)

        End If
    End Sub
End Class
