﻿Imports System.Data.OleDb

Public Class AutocompleteOspitiSenzaCserv
    Implements System.Web.IHttpHandler, IRequiresSessionState

    Public Sub ProcessRequest(ByVal context As HttpContext) Implements IHttpHandler.ProcessRequest
        context.Response.ContentType = "text/plain"
        Dim RICERCA As String = context.Request.QueryString("q")
        Dim UTENTE As String = context.Request.QueryString("UTENTE")
        Dim CSERV As String = context.Request.QueryString("CSERV")
        context.Response.Cache.SetCacheability(HttpCacheability.NoCache)

        Dim sb As StringBuilder = New StringBuilder

        Dim cn As OleDbConnection

        cn = New Data.OleDb.OleDbConnection(context.Session("DC_OSPITE"))

        cn.Open()
        Dim cmd As New OleDbCommand()

        Dim Valore1 As String = ""
        Dim Valore2 As String = ""

        Dim appoggio As String

        appoggio = Replace(Replace(Replace(RICERCA, ",", ""), ".", ""), " ", "")
        If IsNumeric(appoggio) And Len(appoggio) >= 2 Then
            Dim Vettore(100) As String

            Vettore = SplitWords(RICERCA)

            If Vettore.Length >= 2 Then
                Valore1 = Vettore(0)

                cmd.CommandText = ("select * from AnagraficaComune where Tipologia = 'O' And " &
                                   "CodiceOspite = ? And (NonInUso = '' Or NonInUso is Null) ")
                cmd.Parameters.AddWithValue("@CodiceOspite", Valore1)
            Else

                cmd.CommandText = ("select * from AnagraficaComune where Tipologia = 'O' And " &
                                   "Nome Like ? And (NonInUso = '' Or NonInUso is Null) ")
                cmd.Parameters.AddWithValue("@Nome", "%" & RICERCA & "%")
            End If
        Else
            cmd.CommandText = ("select * from AnagraficaComune where Tipologia = 'O' And " &
                   "Nome Like ? And (NonInUso = '' Or NonInUso is Null) ")
            cmd.Parameters.AddWithValue("@Nome", "%" & RICERCA & "%")
        End If

        cmd.Connection = cn

        Dim Counter As Integer = 0
        Dim myPOSTreader As OleDbDataReader = cmd.ExecuteReader()
        Do While myPOSTreader.Read


            sb.Append(myPOSTreader.Item("CodiceOspite") & " " & myPOSTreader.Item("Nome")).Append(Environment.NewLine)
            Counter = Counter + 1
            If Counter > 10 Then
                Exit Do
            End If
        Loop
        context.Response.Write(sb.ToString)
    End Sub

    Public ReadOnly Property IsReusable() As Boolean Implements IHttpHandler.IsReusable
        Get
            Return False
        End Get
    End Property

    Function campodb(ByVal oggetto As Object) As String
        If IsDBNull(oggetto) Then
            Return ""
        Else
            Return oggetto
        End If
    End Function

    Private Function SplitWords(ByVal s As String) As String()
        '
        ' Call Regex.Split function from the imported namespace.
        ' Return the result array.
        '
        Return Regex.Split(s, "\W+")
    End Function

End Class