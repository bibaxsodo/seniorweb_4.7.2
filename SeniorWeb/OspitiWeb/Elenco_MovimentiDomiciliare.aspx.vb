﻿Imports System
Imports System.Web
Imports Microsoft.VisualBasic
Imports System.Data.OleDb
Imports System.Web.Hosting
Imports System.Data.SqlTypes
Imports System.Net
Imports System.Collections.Generic
Imports System.IO
Imports System.Security.Cryptography.X509Certificates
Imports System.Net.Security
Imports System.Web.Script.Serialization
Imports org.jivesoftware.util
Imports Newtonsoft.Json
Imports Newtonsoft.Json.Linq

Partial Class OspitiWeb_Elenco_MovimentiDomiciliare
    Inherits System.Web.UI.Page
    Dim MyTable As New System.Data.DataTable("tabella")
    Dim MyDataSet As New System.Data.DataSet()


    Public Function GiorniMese(ByVal Mese As Byte, ByVal Anno As Integer) As Byte
        If Mese = 1 Or Mese = 3 Or Mese = 5 Or Mese = 7 Or Mese = 8 Or _
           Mese = 10 Or Mese = 12 Then
            GiorniMese = 31
        Else
            If Mese <> 2 Then
                GiorniMese = 30
            Else
                If Day(DateSerial(Anno, Mese, 29)) = 29 Then
                    GiorniMese = 29
                Else
                    GiorniMese = 28
                End If
            End If
        End If
    End Function

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Trim(Session("UTENTE")) = "" Then
            Response.Redirect("../Login.aspx")
            Exit Sub
        End If
        If Session("ABILITAZIONI").IndexOf("<PORTINERIA>") > 0 Then
            Response.Redirect("..\MainPortineria.aspx")
        End If

        Call EseguiJS()


        If Page.IsPostBack = True Then Exit Sub

        If Val(Request.Item("CodiceOspite")) > 0 And Request.Item("CentroServizio") <> "" Then

            Session("CODICESERVIZIO") = Request.Item("CentroServizio")
            Session("CODICEOSPITE") = Val(Request.Item("CodiceOspite"))
        End If




        Dim K1 As New Cls_SqlString

        Dim Barra As New Cls_BarraSenior

        If Not IsNothing(Session("RicercaAnagraficaSQLString")) Then
            K1 = Session("RicercaAnagraficaSQLString")
        End If

        Lbl_BarraSenior.Text = Barra.CodiceBarra(Application("SENIOR"), Session("UTENTE"), Page, K1)

        Dim ConnectionString As String = Session("DC_OSPITE")
        Dim z As New Cls_MovimentiDomiciliare

        Dim f As New Cls_Parametri

        f.LeggiParametri(Session("DC_OSPITE"))
        


        Dim DataDal As Date = DateSerial(f.AnnoFatturazione, f.MeseFatturazione, 1)
        Dim DataAl As Date = DateSerial(f.AnnoFatturazione, f.MeseFatturazione, GiorniMese(f.MeseFatturazione, f.AnnoFatturazione))

        Txt_DataDal.Text = Format(DataDal, "dd/MM/yyyy")
        Txt_DataAl.Text = Format(DataAl, "dd/MM/yyyy")

        z.loaddati(ConnectionString, Session("CODICEOSPITE"), Session("CODICESERVIZIO"), MyTable, DataDal, DataAl)
        ViewState("Appoggio") = MyTable

        Grd_AddebitiAccrediti.AutoGenerateColumns = True
        Grd_AddebitiAccrediti.DataSource = MyTable
        Grd_AddebitiAccrediti.DataBind()



        Dim x As New ClsOspite

        x.CodiceOspite = Session("CODICEOSPITE")
        x.Leggi(Session("DC_OSPITE"), x.CodiceOspite)

        Dim cs As New Cls_CentroServizio

        cs.Leggi(Session("DC_OSPITE"), Session("CODICESERVIZIO"))

        Lbl_NomeOspite.Text = x.Nome & " -  " & x.DataNascita & " - " & cs.DESCRIZIONE & "<i style=""font-size:small;"">(" & x.CodiceOspite & ")</i>"

        

        Dim Tipologia As New Cls_TipoDomiciliare

        Tipologia.UpDateDropBox(ConnectionString, DD_Tipologia)

        Call EseguiJS()

        Call ColoreGriglia()
    End Sub

    Protected Sub Grd_AddebitiAccrediti_PageIndexChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewPageEventArgs) Handles Grd_AddebitiAccrediti.PageIndexChanging
        MyTable = ViewState("Appoggio")
        Grd_AddebitiAccrediti.PageIndex = e.NewPageIndex
        Grd_AddebitiAccrediti.DataSource = MyTable
        Grd_AddebitiAccrediti.DataBind()

        Call ColoreGriglia()
    End Sub





    Protected Sub Grd_AddebitiAccrediti_RowCommand(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewCommandEventArgs) Handles Grd_AddebitiAccrediti.RowCommand
        If e.CommandName = "Seleziona" Then
            Dim d As Integer



            MyTable = ViewState("Appoggio")

            d = Val(e.CommandArgument)

            Dim k As Long

            k = MyTable.Rows(d).Item(0).ToString

            Response.Redirect("GestioneDomiciliare.aspx?ID=" & k)
        End If
    End Sub

    Protected Sub Grd_AddebitiAccrediti_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles Grd_AddebitiAccrediti.SelectedIndexChanged



    End Sub

    Protected Sub Btn_Nuovo_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles Btn_Nuovo.Click
        Response.Redirect("GestioneDomiciliare.aspx?ID=0")
    End Sub

    Protected Sub Btn_Esci_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles Btn_Esci.Click

        If Request.Item("PAGINA") = "" Then
            Response.Redirect("RicercaAnagrafica.aspx?TIPO=DOM")
        Else
            Response.Redirect("RicercaAnagrafica.aspx?TIPO=DOM&CHIAMATA=RITORNO&PAGINA=" & Request.Item("PAGINA"))
        End If
    End Sub

    Private Sub ColoreGriglia()



    End Sub

    Protected Sub ImageButton1_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles ImageButton1.Click
        Dim ConnectionString As String = Session("DC_OSPITE")
        Dim z As New Cls_MovimentiDomiciliare

        If IsDate(Txt_DataDal.Text) And IsDate(Txt_DataAl.Text) Then
            Dim DataDal As Date = Txt_DataDal.Text
            Dim DataAl As Date = Txt_DataAl.Text

            z.loaddati(ConnectionString, Session("CODICEOSPITE"), Session("CODICESERVIZIO"), MyTable, DataDal, DataAl)
        Else
            z.loaddati(ConnectionString, Session("CODICEOSPITE"), Session("CODICESERVIZIO"), MyTable)
        End If
        ViewState("Appoggio") = MyTable

        Grd_AddebitiAccrediti.AutoGenerateColumns = True
        Grd_AddebitiAccrediti.DataSource = MyTable
        Grd_AddebitiAccrediti.DataBind()



        Dim x As New ClsOspite

        x.CodiceOspite = Session("CODICEOSPITE")
        x.Leggi(Session("DC_OSPITE"), x.CodiceOspite)

        Dim cs As New Cls_CentroServizio

        cs.Leggi(Session("DC_OSPITE"), Session("CODICESERVIZIO"))

        Lbl_NomeOspite.Text = x.Nome & " -  " & x.DataNascita & " - " & cs.DESCRIZIONE & "<i style=""font-size:small;"">(" & x.CodiceOspite & ")</i>"

        Call ColoreGriglia()
    End Sub




    Private Sub EseguiJS()
        Dim MyJs As String


        MyJs = "$(document).ready(function() {"
        MyJs = MyJs & "var els = document.getElementsByTagName(""*"");"
        MyJs = MyJs & "for (var i=0;i<els.length;i++)"
        MyJs = MyJs & "if ( els[i].id ) { "
        MyJs = MyJs & " var appoggio =els[i].id; "
        MyJs = MyJs & " if ((appoggio.match('Txt_Data')!= null) || (appoggio.match('Txt_Data')!= null)) {  "
        MyJs = MyJs & " $(els[i]).mask(""99/99/9999"");"

        MyJs = MyJs & " $(els[i]).datepicker({ changeMonth: true,changeYear: true,  yearRange: ""1990:" & Year(Now) + 5 & """},$.datepicker.regional[""it""]);"
        MyJs = MyJs & "    }"

        MyJs = MyJs & "} "
        MyJs = MyJs & "});"


        'MyJs = "$(document).ready(function() { $('.myClass').keypress(function() { handleEnter($('.myClass').val(), true, true); } );     $('#" & Txt_ImportoPagato.ClientID & "').blur(function() { var ap = formatNumber ($('#" & Txt_ImportoPagato.ClientID & "').val(),2); $('#" & Txt_ImportoPagato.ClientID & "').val(ap); }); });"
        ScriptManager.RegisterStartupScript(Me, Me.GetType(), "visualizzaRitIm", MyJs, True)
    End Sub

    Protected Sub Btn_ModificaOperatore_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles Btn_ModificaOperatore.Click
        Dim K As Integer

        For K = 0 To Grd_AddebitiAccrediti.Rows.Count - 1
            Dim CheckBox As CheckBox = DirectCast(Grd_AddebitiAccrediti.Rows(K).FindControl("Chk_Seleziona"), CheckBox)

            If CheckBox.Checked = True Then
                Dim appoggio As String = Grd_AddebitiAccrediti.Rows(K).Cells(2).Text

                Dim MyDom As New Cls_MovimentiDomiciliare

                MyDom.ID = Val(appoggio)
                MyDom.CodiceOspite = Session("CODICEOSPITE")
                MyDom.CENTROSERVIZIO = Session("CODICESERVIZIO")
                MyDom.leggi(Session("DC_OSPITE"))
                MyDom.Operatore = Val(Txt_Operatori.Text)
                MyDom.AggiornaDB(Session("DC_OSPITE"))

            End If
        Next

        Dim ConnectionString As String = Session("DC_OSPITE")
        Dim z As New Cls_MovimentiDomiciliare

        If IsDate(Txt_DataDal.Text) And IsDate(Txt_DataAl.Text) Then
            Dim DataDal As Date = Txt_DataDal.Text
            Dim DataAl As Date = Txt_DataAl.Text

            z.loaddati(ConnectionString, Session("CODICEOSPITE"), Session("CODICESERVIZIO"), MyTable, DataDal, DataAl)
        Else
            z.loaddati(ConnectionString, Session("CODICEOSPITE"), Session("CODICESERVIZIO"), MyTable)
        End If

        ViewState("Appoggio") = MyTable

        Grd_AddebitiAccrediti.AutoGenerateColumns = True
        Grd_AddebitiAccrediti.DataSource = MyTable
        Grd_AddebitiAccrediti.DataBind()
    End Sub

    Protected Sub Btn_ModificaTiplogia_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles Btn_ModificaTiplogia.Click
        Dim K As Integer

        For K = 0 To Grd_AddebitiAccrediti.Rows.Count - 1
            Dim CheckBox As CheckBox = DirectCast(Grd_AddebitiAccrediti.Rows(K).FindControl("Chk_Seleziona"), CheckBox)

            If CheckBox.Checked = True Then
                Dim appoggio As String = Grd_AddebitiAccrediti.Rows(K).Cells(2).Text

                Dim MyDom As New Cls_MovimentiDomiciliare

                MyDom.ID = Val(appoggio)
                MyDom.CodiceOspite = Session("CODICEOSPITE")
                MyDom.CENTROSERVIZIO = Session("CODICESERVIZIO")
                MyDom.leggi(Session("DC_OSPITE"))
                MyDom.Tipologia = DD_Tipologia.SelectedValue
                MyDom.AggiornaDB(Session("DC_OSPITE"))

            End If
        Next

        Dim ConnectionString As String = Session("DC_OSPITE")
        Dim z As New Cls_MovimentiDomiciliare

        If IsDate(Txt_DataDal.Text) And IsDate(Txt_DataAl.Text) Then
            Dim DataDal As Date = Txt_DataDal.Text
            Dim DataAl As Date = Txt_DataAl.Text

            z.loaddati(ConnectionString, Session("CODICEOSPITE"), Session("CODICESERVIZIO"), MyTable, DataDal, DataAl)
        Else
            z.loaddati(ConnectionString, Session("CODICEOSPITE"), Session("CODICESERVIZIO"), MyTable)
        End If

        ViewState("Appoggio") = MyTable

        Grd_AddebitiAccrediti.AutoGenerateColumns = True
        Grd_AddebitiAccrediti.DataSource = MyTable
        Grd_AddebitiAccrediti.DataBind()
    End Sub

    Protected Sub Btn_MultiMovimenti_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles Btn_MultiMovimenti.Click
        Response.Redirect("GestioneDomiciliareMultipli.aspx?ID=0")
    End Sub



    Private Function LoginPersonam(ByVal context As HttpContext) As String
        Try


            Dim request As New NameValueCollection


            request.Add("client_id", "98217ca6670c660e22f42d58e231d4e56c84fb34e216b0f66f1f30284fd3ec9d")
            request.Add("client_secret", "5570a684f69ca74d75d16bba669c156ec092eccb4111d0974adec3edb2fa4d6f")


            request.Add("username", context.Session("EPersonamUser"))



            request.Add("password", context.Session("EPersonamPSW"))
            System.Net.ServicePointManager.ServerCertificateValidationCallback = AddressOf CertificateHandler


            Dim client As New WebClient()

            Dim result As Object = client.UploadValues("https://api.e-personam.com/api/v1.0/token/password", "POST", request)


            Dim stringa As String = Encoding.Default.GetString(result)


            Dim serializer As JavaScriptSerializer


            serializer = New JavaScriptSerializer()




            Dim s As Autentificazione = serializer.Deserialize(Of Autentificazione)(stringa)


            Return s.access_token
        Catch ex As Exception
            Return ""
        End Try
    End Function

    Public Class Autentificazione
        Public access_token As String
        Public expires_in As String
        Public scope As String
        Public token_type As String
        Public refresh_token As String
    End Class



    Private Shared Function CertificateHandler(ByVal sender As Object, ByVal certificate As X509Certificate, ByVal chain As X509Chain, ByVal SSLerror As SslPolicyErrors) As Boolean
        Return True
    End Function

    Protected Sub Imb_ImportEPersonam_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles Imb_ImportEPersonam.Click
        Dim Token As String

        Dim cn As OleDbConnection

        cn = New Data.OleDb.OleDbConnection(Session("DC_OSPITE"))

        cn.Open()





        Try
            Token = LoginPersonam(Context)

        Catch ex As Exception
            ClientScript.RegisterClientScriptBlock(Me.GetType(), "Errore", "VisualizzaErrore('Errore nel dialogo con Epersonam');", True)
            Exit Sub
        End Try
        If Token = "" Then
            ClientScript.RegisterClientScriptBlock(Me.GetType(), "Errore", "VisualizzaErrore('Errore nel dialogo con Epersonam');", True)
            Exit Sub
        End If
        Dim rawresp As String = ""
        Dim Param As New Cls_Parametri

        Param.LeggiParametri(Context.Session("DC_OSPITE"))

        Dim Cserv As New Cls_CentroServizio

        Cserv.CENTROSERVIZIO = Session("CODICESERVIZIO")
        Cserv.Leggi(Context.Session("DC_OSPITE"), Cserv.CENTROSERVIZIO)

        Dim Ospite As New ClsOspite

        Ospite.CodiceOspite = Session("CODICEOSPITE")
        Ospite.Leggi(Context.Session("DC_OSPITE"), Ospite.CodiceOspite)


        Dim NucleiDb As New OleDbCommand

        NucleiDb.CommandText = "Select * From NucleiCentroServizi Where IdSenior = " & Cserv.EPersonam
              

        NucleiDb.Connection = cn

        Dim NucleiRB As OleDbDataReader = NucleiDb.ExecuteReader()


        Do While NucleiRB.Read


            rawresp = RichiediJSONEpersonam(Token, NucleiRB.Item("IdEpersonam"), 0, Param.AnnoFatturazione, Param.MeseFatturazione)

            If rawresp = "" Then Exit Do
            If rawresp = "[]" Then Exit Do
            If rawresp = "{}" Then Exit Do

            Dim jResults As JArray = JArray.Parse(rawresp)


            Dim IndiceRighe As Integer = 0

            For Each jTok1 As JToken In jResults
                Dim CodiceFiscale As String

                CodiceFiscale = jTok1.Item("cf").ToString()

                If CodiceFiscale = Ospite.CODICEFISCALE Then

                    For Each jTok2 As JToken In jTok1.Item("domestic_activities").Children

                        Dim MMov As New Cls_Movimenti
                        Dim Motivation As Integer

                        Dim UnitID As Integer
                        Try
                            UnitID = jTok2.Item("unit_id").ToString()
                        Catch ex As Exception

                        End Try


                        Try
                            If IsDBNull(jTok2.Item("done").ToString) Or jTok2.Item("done").ToString = "" Then
                                Motivation = 1 ' Eseguito se Null??
                            Else
                                Motivation = jTok2.Item("done").ToString
                            End If

                        Catch ex As Exception
                            Motivation = 99
                        End Try


                        Dim ErroreCS As Boolean = False
                        Dim UnitStruttura As New Cls_Epersonam

                        Dim ServizioUnitID As New Cls_CentroServizio
                        Dim UnitTrascodifica As Integer

                        UnitTrascodifica = UnitStruttura.RendiWardid(UnitID, Session("DC_OSPITE"))

                        If UnitTrascodifica > 0 Then
                            ServizioUnitID.EPersonam = UnitTrascodifica
                            ServizioUnitID.LeggiEpersonam(Session("DC_OSPITE"), UnitTrascodifica, True)
                        Else
                            ServizioUnitID.EPersonam = UnitID
                            ServizioUnitID.LeggiEpersonam(Session("DC_OSPITE"), UnitID, True)
                        End If


                        If Motivation >= 1 Then
                            Dim cmdcData As New OleDbCommand()
                            cmdcData.CommandText = ("select * from Domiciliare_EPersonam Where CentroServizio = ? and CodiceOspite = ? And ID_Epersonam = ?")

                            cmdcData.Parameters.AddWithValue("@CENTROSERVIZIO", Cserv.CENTROSERVIZIO)
                            cmdcData.Parameters.AddWithValue("@CodiceOspite", Ospite.CodiceOspite)
                            cmdcData.Parameters.AddWithValue("@Id", jTok2.Item("id").ToString)
                            cmdcData.Connection = cn
                            Dim RDData As OleDbDataReader = cmdcData.ExecuteReader()
                            If Not RDData.Read Then

                                Dim Ore As Integer = Mid(jTok2.Item("date_done").ToString, 12, 2)
                                Dim Minuti As Integer = Mid(jTok2.Item("date_done").ToString, 15, 2)
                                Dim OraInizio As Date
                                Dim OraFine As Date
                                Dim Tipologia As String
                                Dim Warning As String = ""

                                OraInizio = TimeSerial(Ore, Minuti, 0).AddYears(1899).AddMonths(12).AddDays(30)

                                Dim Durata As Long = Val(jTok2.Item("duration").ToString)

                                If Val(Durata) = 0 Then
                                    Durata = Val(jTok2.Item("duration_prev").ToString)
                                End If

                                REM Import minuti effettivi non a 15  min. per aqua (da parametrizzarE)
                                'If Durata > 0 Then
                                'Durata = Math.Round(Durata / 15, 0) * 15
                                'End If

                                Dim NumeroOperatori As Integer = 0
                                Dim IdOperatore(100) As Integer
                                Dim NomeOperatore(100) As String

                                For Each jTok3 As JToken In jTok2.Item("operators").Children
                                    NumeroOperatori = NumeroOperatori + 1

                                    IdOperatore(NumeroOperatori) = jTok3.Item("id")
                                    NomeOperatore(NumeroOperatori) = jTok3.Item("fullname")

                                    Dim Operatore As New Cls_Operatore

                                    Operatore.CodiceMedico = IdOperatore(NumeroOperatori)
                                    Operatore.Leggi(Session("DC_OSPITE"))

                                    If Operatore.Nome = "" Then
                                        Operatore.Nome = NomeOperatore(NumeroOperatori)
                                        Operatore.Scrivi(Session("DC_OSPITE"))
                                    End If


                                Next

                                If NumeroOperatori > 2 Then
                                    NumeroOperatori = 2
                                End If

                                OraFine = TimeSerial(Ore, Minuti, 0).AddMinutes(Durata).AddYears(1899).AddMonths(12).AddDays(30)
                                Tipologia = DecodificaTabellaTrascodifica(Cserv.CENTROSERVIZIO, Val(jTok2.Item("activity_id").ToString), campodb(jTok2.Item("conv").ToString))
                                Dim mTp As New Cls_TipoDomiciliare

                                mTp.Codice = Tipologia
                                mTp.Leggi(Session("DC_OSPITE"), mTp.Codice)
                                If mTp.Descrizione = "" Then
                                    Warning = "Trascodifica non possibile " & Val(jTok2.Item("activity_id").ToString) & " " & jTok2.Item("description").ToString
                                End If
                                Tipologia = mTp.Codice


                                Dim Dom As New Cls_MovimentiDomiciliare


                                Dom.CENTROSERVIZIO = Cserv.CENTROSERVIZIO
                                Dom.CodiceOspite = Ospite.CodiceOspite

                                Dom.Data = Mid(jTok2.Item("date_done").ToString, 1, 10)

                                Dom.OraInizio = OraInizio

                                Dom.OraFine = OraFine


                                Dom.Operatore = NumeroOperatori

                                Dim I As Integer

                                For I = 0 To NumeroOperatori
                                    Dom.Righe(I) = New Cls_MovimentiDomiciliare_Operatori

                                    Dom.Righe(I).CodiceOperatore = IdOperatore(I)
                                Next


                                Dom.Tipologia = Tipologia
                                Dom.Utente = Context.Session("UTENTE")
                                Dom.AggiornaDB(Context.Session("DC_OSPITE"))

                                Dim MySql As String

                                MySql = "INSERT INTO Domiciliare_EPersonam (Utente,DataAggiornamento,CentroServizio,CodiceOspite,Data,Durata,ID_Epersonam) VALUES (?,?,?,?,?,?,?)"
                                Dim cmdw As New OleDbCommand()
                                cmdw.CommandText = (MySql)

                                cmdw.Parameters.AddWithValue("@Utente", Context.Session("UTENTE"))
                                cmdw.Parameters.AddWithValue("@DataAggiornamento", Now)
                                cmdw.Parameters.AddWithValue("@CentroServizio", Cserv.CENTROSERVIZIO)
                                cmdw.Parameters.AddWithValue("@CodiceOspite", Ospite.CodiceOspite)

                                Dim DataAppoggio As Date = Now
                                Try
                                    DataAppoggio = Dom.Data
                                Catch ex As Exception

                                End Try
                                cmdw.Parameters.AddWithValue("@Data", Format(DataAppoggio, "dd/MM/yyyy"))

                                Dim AppoD1 As Date
                                Dim AppoD2 As Date

                                AppoD1 = OraInizio
                                AppoD2 = OraFine

                                cmdw.Parameters.AddWithValue("@Durata", DateDiff(DateInterval.Minute, AppoD2, AppoD1))


                                cmdw.Parameters.AddWithValue("@ID_Epersonam", jTok2.Item("id").ToString)
                                cmdw.Connection = cn
                                cmdw.ExecuteNonQuery()
                            End If
                            RDData.Close()
                        End If
                    Next
                End If
            Next




        Loop

        cn.Close()


        ImageButton1_Click(sender, e)


    End Sub


    Function RichiediJSONEpersonam(ByVal Token As String, ByVal BU As Long, ByRef Pagina As Integer, ByVal Anno As Integer, ByVal Mese As Integer) As String
        Dim rawresp As String = ""
        Dim Url As String
        Dim cn As OleDbConnection

        cn = New Data.OleDb.OleDbConnection(Session("DC_OSPITE"))

        cn.Open()




        If Pagina = 0 Then
            Url = "https://api.e-personam.com/api/v1.0/domestics/visits/business_units/" & BU & "/" & Anno & "/" & Mese '& "?per_page=10"
            Pagina = Pagina + 1
        Else
            Url = "https://api.e-personam.com/api/v1.0/domestics/visits/business_units/" & BU & "/" & Anno & "/" & Mese & "?page=" & Pagina ' & "&per_page=100"
        End If
        Pagina = Pagina + 1

        Try

            Dim client As HttpWebRequest = WebRequest.Create(Url)

            client.Method = "GET"
            client.Headers.Add("Authorization", "Bearer " & Token)
            client.ContentType = "Content-Type: application/json"

            client.KeepAlive = True
            client.ReadWriteTimeout = 62000
            client.MaximumResponseHeadersLength = 262144
            client.Timeout = 62000


            Dim reader As StreamReader
            Dim response As HttpWebResponse = Nothing



            response = DirectCast(client.GetResponse(), HttpWebResponse)

            reader = New StreamReader(response.GetResponseStream())

            rawresp = reader.ReadToEnd()

        Catch ex As Exception
            ClientScript.RegisterClientScriptBlock(Me.GetType(), "Errore", "VisualizzaErrore('Errore nel dialogo con Epersonam " & BU & " ');", True)
            Return ""
            Exit Function
        End Try

        Dim SalvaDb As New OleDbCommand

        SalvaDb.CommandText = "Select * From DatiTemporaneiEpersonam Where Pagina = " & Pagina - 1 & " And Periodo = ? And Tipo = 'D' And Nucleo = ? "
        SalvaDb.Parameters.AddWithValue("@Periodo", Format(Anno, "00") & Anno)
        SalvaDb.Parameters.AddWithValue("@Nucleo", BU)
        SalvaDb.Connection = cn

        Dim VerificaDB As OleDbDataReader = SalvaDb.ExecuteReader()
        If VerificaDB.Read Then
            Dim UpDb As New OleDbCommand
            UpDb.Connection = cn
            UpDb.CommandText = "UPDATE DatiTemporaneiEpersonam  SET Dati = ?, Utente = ?,UltimaModifica = ?  Where Pagina = ? And Periodo = ? And Tipo = 'D' And Nucleo =  ? "
            UpDb.Parameters.AddWithValue("@Dati", rawresp)
            UpDb.Parameters.AddWithValue("@Utente", Session("UTENTE"))
            UpDb.Parameters.AddWithValue("@UltimaModifica", Now)
            UpDb.Parameters.AddWithValue("@Pagina", Pagina - 1)
            UpDb.Parameters.AddWithValue("@Periodo", Format(Mese, "00") & Anno)
            UpDb.Parameters.AddWithValue("@Nucleo", BU)
            UpDb.ExecuteNonQuery()
        Else
            Dim UpDb As New OleDbCommand
            UpDb.Connection = cn
            UpDb.CommandText = "INSERT INTO DatiTemporaneiEpersonam  (Dati , Utente ,UltimaModifica ,Pagina,Periodo,Tipo,Nucleo) values (?,?,?,?,?,'D',?) "
            UpDb.Parameters.AddWithValue("@Dati", rawresp)
            UpDb.Parameters.AddWithValue("@Utente", Session("UTENTE"))
            UpDb.Parameters.AddWithValue("@UltimaModifica", Now)
            UpDb.Parameters.AddWithValue("@Pagina", Pagina - 1)
            UpDb.Parameters.AddWithValue("@Periodo", Format(Mese, "00") & Anno)
            UpDb.Parameters.AddWithValue("@Nucleo", BU)
            UpDb.ExecuteNonQuery()
        End If
        VerificaDB.Close()
        
        cn.Close()

        Return rawresp
    End Function



    Function DecodificaTabellaTrascodifica(ByVal CentroServizio As String, ByVal activity_id As Integer, ByVal Convenzione As String) As String
        Dim cn As OleDbConnection


        cn = New Data.OleDb.OleDbConnection(Session("DC_OSPITE"))

        cn.Open()
        DecodificaTabellaTrascodifica = ""

        If Convenzione.ToUpper = "FALSE" Then
            Convenzione = "N"
        End If
        If Convenzione.ToUpper = "TRUE" Then
            Convenzione = "S"
        End If

        Dim cmd As New OleDbCommand()
        If CentroServizio = "" Then
            cmd.CommandText = ("select * from DomiciliareEPersonam where activity_id = ?")
            cmd.Parameters.AddWithValue("@activity_id", activity_id)
        Else
            cmd.CommandText = ("select * from DomiciliareEPersonam where activity_id = ? And (CentroServizio = ? or (CentroServizio = '' OR CentroServizio is null)) And (Convenzione = ? or (Convenzione = '' OR Convenzione is null))")
            cmd.Parameters.AddWithValue("@activity_id", activity_id)
            cmd.Parameters.AddWithValue("@CentroServizio", CentroServizio)
            cmd.Parameters.AddWithValue("@Convenzione", Convenzione)

        End If
        cmd.Connection = cn
        Dim myPOSTreader As OleDbDataReader = cmd.ExecuteReader()
        If myPOSTreader.Read Then
            DecodificaTabellaTrascodifica = campodb(myPOSTreader.Item("TipologiaDomiciliare"))
        End If
        cn.Close()
    End Function

    Function campodb(ByVal oggetto As Object) As String
        If IsDBNull(oggetto) Then
            Return ""
        Else
            Return oggetto
        End If
    End Function
End Class
