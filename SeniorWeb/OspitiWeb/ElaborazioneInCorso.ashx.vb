﻿Public Class ElaborazioneInCorso
    Implements System.Web.IHttpHandler, IRequiresSessionState

    Public Sub ProcessRequest(ByVal context As HttpContext) Implements IHttpHandler.ProcessRequest
        context.Response.ContentType = "text/plain"
        context.Response.Cache.SetCacheability(HttpCacheability.NoCache)
        If Trim(context.Session("UTENTE")) = "" Then
            Exit Sub
        End If

        Dim M As New Cls_Parametri

        M.Elaborazione(context.Session("DC_OSPITE"))


        Dim MettiElaborazioneGenerale As New Cls_ParametriGenerale

        MettiElaborazioneGenerale.Elaborazione(context.Session("DC_GENERALE"))


        context.Response.Write("OK")

    End Sub

    Public ReadOnly Property IsReusable() As Boolean Implements IHttpHandler.IsReusable
        Get
            Return False
        End Get
    End Property

End Class