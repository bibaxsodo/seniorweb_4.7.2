﻿Imports System.Data.OleDb
Partial Class OspitiWeb_DepositoCauzionaleCaparra
    Inherits System.Web.UI.Page

    Function VerificaSeInserito() As Boolean
        Dim cn As OleDbConnection

        cn = New Data.OleDb.OleDbConnection(Session("DC_GENERALE"))

        cn.Open()
        Dim cmdTesta As New OleDbCommand()
        cmdTesta.CommandText = "SELECT * FROM MovimentiContabiliTesta  Where Utente = ? And DATAAGGIORNAMENTO >= ?"
        cmdTesta.Connection = cn
        cmdTesta.Parameters.AddWithValue("@Utente", Session("UTENTE"))
        cmdTesta.Parameters.AddWithValue("@Utente", Now.AddSeconds(-2))
        Dim ReadTesta As OleDbDataReader = cmdTesta.ExecuteReader()
        If ReadTesta.Read Then
            ReadTesta.Close()

            cn.Close()
            Return True
        End If
        ReadTesta.Close()

        cn.Close()


        Return False
    End Function

    Private Sub CreaDocumento(ByVal TipoMovimento As String)
        If Val(Txt_Importo.Text) = 0 Then
            REM Lbl_Errore.Text = "Specificare importo deposito cauzionale"
            ClientScript.RegisterClientScriptBlock(Me.GetType(), "Errore", "VisualizzaErrore('Specificare importo');", True)
            Exit Sub
        End If
        If Txt_DataRegistrazione.Text = "" Then
            ClientScript.RegisterClientScriptBlock(Me.GetType(), "Errore", "VisualizzaErrore('Specificare data registrazione');", True)
            REM Lbl_Errore.Text = "Specificare data addebito"
            Exit Sub
        End If

        Call CalcolaTotale()



        Dim x As New ClsOspite
        Dim ConnectionString As String = Session("DC_OSPITE")
        Dim ConnectionStringTabelle As String = Session("DC_TABELLE")
        Dim ConnectionStringGenerale As String = Session("DC_GENERALE")

        x.Leggi(ConnectionString, Session("CODICEOSPITE"))


        Dim KCs As New Cls_DatiOspiteParenteCentroServizio

        KCs.CentroServizio = Session("CODICESERVIZIO")
        KCs.CodiceOspite = Session("CODICEOSPITE")
        KCs.CodiceParente = 0
        KCs.Leggi(ConnectionString)

        REM Assegna la tipologia operazione e l'aliquota prendendo quella relativa al cserv
        If KCs.CodiceOspite <> 0 Then
            x.TIPOOPERAZIONE = KCs.TipoOperazione
            x.CODICEIVA = KCs.AliquotaIva
            x.FattAnticipata = KCs.Anticipata
            x.MODALITAPAGAMENTO = KCs.ModalitaPagamento
            x.Compensazione = KCs.Compensazione
            x.SETTIMANA = KCs.Settimana
        End If

        Dim TipoOperazione As New Cls_TipoOperazione

        TipoOperazione.Leggi(ConnectionString, x.TIPOOPERAZIONE)

        If TipoMovimento = "DOCDEP" Then
            If TipoOperazione.CausaleDeposito.Trim = "" Then
                ClientScript.RegisterClientScriptBlock(Me.GetType(), "Errore", "VisualizzaErrore('Causale deposito non specificata in tipo operazione');", True)
                Exit Sub
            End If
        End If
        If TipoMovimento = "NCDEP" Then
            If TipoOperazione.CausaleNcDeposito.Trim = "" Then
                ClientScript.RegisterClientScriptBlock(Me.GetType(), "Errore", "VisualizzaErrore('Causale nc deposito non specificata in tipo operazione');", True)
                Exit Sub
            End If
        End If

        If TipoMovimento = "DOCCAP" Then
            If TipoOperazione.CausaleCaparraConfirmatoria.Trim = "" Then
                ClientScript.RegisterClientScriptBlock(Me.GetType(), "Errore", "VisualizzaErrore('Causale caparra non specificata in tipo operazione');", True)
                Exit Sub
            End If
        End If
        If TipoMovimento = "NCCAP" Then
            If TipoOperazione.NcCausaleCaparraConfirmatoria.Trim = "" Then
                ClientScript.RegisterClientScriptBlock(Me.GetType(), "Errore", "VisualizzaErrore('Causale nc caparra non specificata in tipo operazione');", True)
                Exit Sub
            End If
        End If

        Dim CausaleContabile As New Cls_CausaleContabile
        Dim Registrazione As New Cls_MovimentoContabile

        If TipoMovimento = "DOCDEP" Then
            CausaleContabile.Leggi(ConnectionStringTabelle, TipoOperazione.CausaleDeposito)
        End If
        If TipoMovimento = "NCDEP" Then
            CausaleContabile.Leggi(ConnectionStringTabelle, TipoOperazione.CausaleNcDeposito)
        End If
        If TipoMovimento = "DOCCAP" Then
            CausaleContabile.Leggi(ConnectionStringTabelle, TipoOperazione.CausaleCaparraConfirmatoria)
        End If
        If TipoMovimento = "NCCAP" Then
            CausaleContabile.Leggi(ConnectionStringTabelle, TipoOperazione.NcCausaleCaparraConfirmatoria)
        End If

        Dim ImportIVA As Double
        Dim Importo As Double
        Dim CodiceBollo As String
        Dim ImportoBollo As Double

        Dim CIva As New Cls_IVA

        CIva.Codice = x.CODICEIVA


        'If TipoMovimento = "DOCCAP" Or TipoMovimento = "NCCAP" Then
        If TipoOperazione.CodiceIVACaparra <> "" Then
            CIva.Codice = TipoOperazione.CodiceIVACaparra
            x.CODICEIVA = TipoOperazione.CodiceIVACaparra
        End If
        'End If


        CIva.Leggi(Session("DC_TABELLE"), CIva.Codice)

        ImportIVA = CDbl(CDbl(Txt_Importo.Text) * CIva.Aliquota)

        Importo = CDbl(Txt_Importo.Text)

        If TipoOperazione.SoggettaABollo = "S" Then
            Dim ClsBollo As New Cls_bolli

            ClsBollo.Leggi(Session("DC_TABELLE"), Txt_DataRegistrazione.Text)

            If Importo > ClsBollo.ImportoApplicazione Then
                If TipoMovimento = "NCDEP" Or TipoMovimento = "NCCAP" Then
                    ImportoBollo = ClsBollo.ImportoBollo
                    CodiceBollo = ClsBollo.CodiceIVA
                    If CausaleContabile.Righe(8).DareAvere = CausaleContabile.Righe(0).DareAvere Then
                        Importo = Importo - ImportoBollo
                    Else
                        Importo = Importo + ImportoBollo
                    End If
                Else
                    ImportoBollo = ClsBollo.ImportoBollo
                    CodiceBollo = ClsBollo.CodiceIVA
                    Importo = Importo + ImportoBollo
                End If
            End If
        End If

        Registrazione.DataRegistrazione = Txt_DataRegistrazione.Text
        Registrazione.DataDocumento = Txt_DataRegistrazione.Text
        Registrazione.NumeroDocumento = Registrazione.MaxProtocollo(ConnectionStringGenerale, Year(Txt_DataRegistrazione.Text), CausaleContabile.RegistroIVA) + 1
        If TipoMovimento = "DOCDEP" Then
            Registrazione.CausaleContabile = TipoOperazione.CausaleDeposito
        End If
        If TipoMovimento = "NCDEP" Then
            Registrazione.CausaleContabile = TipoOperazione.CausaleNcDeposito
        End If
        If TipoMovimento = "DOCCAP" Then
            Registrazione.CausaleContabile = TipoOperazione.CausaleCaparraConfirmatoria
        End If
        If TipoMovimento = "NCCAP" Then
            Registrazione.CausaleContabile = TipoOperazione.NcCausaleCaparraConfirmatoria
        End If
        Registrazione.AnnoCompetenza = 0
        Registrazione.MeseCompetenza = 0
        Registrazione.RegistroIVA = CausaleContabile.RegistroIVA
        Registrazione.AnnoProtocollo = Year(Txt_DataRegistrazione.Text)
        Registrazione.NumeroProtocollo = Registrazione.NumeroDocumento

        Dim ModalitaPagamentoOspite As New ClsModalitaPagamento

        ModalitaPagamentoOspite.Codice = x.MODALITAPAGAMENTO
        ModalitaPagamentoOspite.Leggi(Session("DC_OSPITE"))

        Registrazione.CodicePagamento = ModalitaPagamentoOspite.ModalitaPagamento


        Registrazione.CentroServizio = Session("CODICESERVIZIO")
        Registrazione.Utente = Session("UTENTE")


        Dim CentroServizio As New Cls_CentroServizio

        CentroServizio.Leggi(ConnectionString, Session("CODICESERVIZIO"))
        Registrazione.Righe(0) = New Cls_MovimentiContabiliRiga
        Registrazione.Righe(0).MastroPartita = CentroServizio.MASTRO
        Registrazione.Righe(0).ContoPartita = CentroServizio.CONTO
        Registrazione.Righe(0).SottocontoPartita = Session("CODICEOSPITE") * 100
        If Val(Request.Item("CodiceParente")) < 99 And Val(Request.Item("CodiceParente")) > 0 Then
            Registrazione.Righe(0).SottocontoPartita = (Session("CODICEOSPITE") * 100) + Val(Request.Item("CodiceParente"))
        End If
        Registrazione.Righe(0).DareAvere = CausaleContabile.Righe(0).DareAvere
        Registrazione.Righe(0).Segno = CausaleContabile.Righe(0).Segno

        Registrazione.Righe(0).Importo = Importo + ImportIVA



        Registrazione.Righe(0).Tipo = "CF"
        Registrazione.Righe(0).RigaDaCausale = 1
        Registrazione.Righe(0).Utente = Session("UTENTE")


        Registrazione.Righe(1) = New Cls_MovimentiContabiliRiga
        Registrazione.Righe(1).MastroPartita = CausaleContabile.Righe(1).Mastro
        Registrazione.Righe(1).ContoPartita = CausaleContabile.Righe(1).Conto
        Registrazione.Righe(1).SottocontoPartita = CausaleContabile.Righe(1).Sottoconto
        If TipoMovimento = "NCDEP" Or TipoMovimento = "NCCAP" Then
            If CausaleContabile.Righe(1).DareAvere = "D" Then
                Registrazione.Righe(1).DareAvere = "A"
            Else
                Registrazione.Righe(1).DareAvere = "D"
            End If
        Else
            Registrazione.Righe(1).DareAvere = CausaleContabile.Righe(1).DareAvere
        End If
        Registrazione.Righe(1).Segno = CausaleContabile.Righe(1).Segno
        Registrazione.Righe(1).Importo = ImportIVA
        Registrazione.Righe(1).Imponibile = CDbl(Txt_Importo.Text)
        Registrazione.Righe(1).CodiceIVA = x.CODICEIVA
        Registrazione.Righe(1).Tipo = "IV"
        Registrazione.Righe(1).RigaDaCausale = 2
        Registrazione.Righe(1).Utente = Session("UTENTE")

        If TipoMovimento = "NCDEP" Or TipoMovimento = "NCCAP" Then
            Registrazione.Righe(2) = New Cls_MovimentiContabiliRiga
            Registrazione.Righe(2).MastroPartita = CausaleContabile.Righe(5).Mastro
            Registrazione.Righe(2).ContoPartita = CausaleContabile.Righe(5).Conto
            Registrazione.Righe(2).SottocontoPartita = CausaleContabile.Righe(5).Sottoconto
            Registrazione.Righe(2).DareAvere = CausaleContabile.Righe(5).DareAvere
            Registrazione.Righe(2).Segno = CausaleContabile.Righe(5).Segno
            Registrazione.Righe(2).Importo = CDbl(Txt_Importo.Text)
            Registrazione.Righe(2).Descrizione = CausaleContabile.Descrizione

            Registrazione.Righe(2).Imponibile = 0
            Registrazione.Righe(2).CodiceIVA = x.CODICEIVA
            Registrazione.Righe(2).Tipo = ""
            Registrazione.Righe(2).RigaDaCausale = 6
            Registrazione.Righe(2).Utente = Session("UTENTE")
        Else
            Registrazione.Righe(2) = New Cls_MovimentiContabiliRiga
            Registrazione.Righe(2).MastroPartita = CausaleContabile.Righe(2).Mastro
            Registrazione.Righe(2).ContoPartita = CausaleContabile.Righe(2).Conto
            Registrazione.Righe(2).SottocontoPartita = CausaleContabile.Righe(2).Sottoconto
            Registrazione.Righe(2).DareAvere = CausaleContabile.Righe(2).DareAvere
            Registrazione.Righe(2).Segno = CausaleContabile.Righe(2).Segno
            Registrazione.Righe(2).Importo = CDbl(Txt_Importo.Text)
            Registrazione.Righe(2).Descrizione = CausaleContabile.Descrizione
            Registrazione.Righe(2).Imponibile = 0
            Registrazione.Righe(2).CodiceIVA = x.CODICEIVA
            Registrazione.Righe(2).Tipo = ""
            Registrazione.Righe(2).RigaDaCausale = 3
            Registrazione.Righe(2).Utente = Session("UTENTE")
        End If

        Dim RegistroIVA As New Cls_RegistroIVA

        RegistroIVA.RegistroCartaceo = 0
        RegistroIVA.Tipo = CausaleContabile.RegistroIVA
        RegistroIVA.Leggi(Session("DC_TABELLE"), RegistroIVA.Tipo)

        If (ImportoBollo > 0 Or TipoOperazione.BolloVirtuale = 1) And RegistroIVA.RegistroCartaceo = 0 Then
            Registrazione.BolloVirtuale = 1
        End If

        If ImportoBollo > 0 Then


            Registrazione.Righe(3) = New Cls_MovimentiContabiliRiga
            Registrazione.Righe(3).MastroPartita = CausaleContabile.Righe(1).Mastro
            Registrazione.Righe(3).ContoPartita = CausaleContabile.Righe(1).Conto
            Registrazione.Righe(3).SottocontoPartita = CausaleContabile.Righe(1).Sottoconto
            Registrazione.Righe(3).DareAvere = CausaleContabile.Righe(1).DareAvere

            Registrazione.Righe(3).Segno = CausaleContabile.Righe(1).Segno
            Registrazione.Righe(3).Descrizione = "Bollo"
            Registrazione.Righe(3).Importo = 0
            Registrazione.Righe(3).Imponibile = ImportoBollo
            Registrazione.Righe(3).CodiceIVA = CodiceBollo
            Registrazione.Righe(3).Tipo = "IV"
            Registrazione.Righe(3).RigaDaCausale = 2
            Registrazione.Righe(3).Utente = Session("UTENTE")

            Registrazione.Righe(4) = New Cls_MovimentiContabiliRiga


            Registrazione.Righe(4).MastroPartita = CausaleContabile.Righe(8).Mastro
            Registrazione.Righe(4).ContoPartita = CausaleContabile.Righe(8).Conto
            Registrazione.Righe(4).SottocontoPartita = CausaleContabile.Righe(8).Sottoconto

            Registrazione.Righe(4).DareAvere = CausaleContabile.Righe(8).DareAvere
            Registrazione.Righe(4).Segno = CausaleContabile.Righe(8).Segno
            Registrazione.Righe(4).Importo = ImportoBollo
            Registrazione.Righe(4).Imponibile = 0
            Registrazione.Righe(4).CodiceIVA = CodiceBollo
            Registrazione.Righe(4).Tipo = ""
            Registrazione.Righe(4).RigaDaCausale = 9
            Registrazione.Righe(4).Utente = Session("UTENTE")

        End If

        Dim VerificaValidita As String = Registrazione.VerificaValidita(Session("DC_GENERALE"), Session("DC_TABELLE"))

        If VerificaValidita <> "OK" Then
            ScriptManager.RegisterStartupScript(Me, Me.GetType(), "Errore", "VisualizzaErrore('<center>" & VerificaValidita & "</center>');", True)
            Exit Sub
        End If

        If Registrazione.Scrivi(ConnectionStringGenerale, -1) = False Then
            ClientScript.RegisterClientScriptBlock(Me.GetType(), "Errore", "VisualizzaErrore('Errore nella registrazione');", True)
            Call EseguiJS()
            Exit Sub
        End If

        If Dd_CausaleContabile.SelectedValue <> "" Then
            CreatoIncasso(Registrazione.NumeroRegistrazione, Importo)
        End If



        
    End Sub



    Private Sub CreatoIncasso(ByVal NumeroDocumento As Long, ByVal Importo As Double)
        Dim X As New Cls_MovimentoContabile
        Dim MyCau As New Cls_CausaleContabile

        X.Leggi(Session("DC_GENERALE"), 0)

        X.NumeroRegistrazione = 0
        X.DataRegistrazione = Txt_DataRegistrazione.Text
        X.DataBolletta = Nothing
        X.NumeroBolletta = ""
        X.Descrizione = Txt_Descrizione.Text

        X.CentroServizio = Session("CODICESERVIZIO")

        X.CausaleContabile = Dd_CausaleContabile.SelectedValue
        X.Utente = Session("UTENTE")
        MyCau.Leggi(Session("DC_TABELLE"), Dd_CausaleContabile.SelectedValue)

        Dim Mastro As Long
        Dim Conto As Long
        Dim Sottoconto As Long
        Dim XCs As New Cls_CentroServizio

        Dim KoSP As New Cls_Parametri

        KoSP.LeggiParametri(Session("DC_OSPITE"))

        XCs.Leggi(Session("DC_OSPITE"), Session("CODICESERVIZIO"))
        If Val(Request.Item("CodiceParente")) = 99 Then
            Mastro = XCs.MASTRO
            Conto = XCs.CONTO
            Sottoconto = Session("CODICEOSPITE") * 100
        Else
            Mastro = XCs.MASTRO
            Conto = XCs.CONTO
            Sottoconto = (Session("CODICEOSPITE") * 100) + Val(Request.Item("CodiceParente"))
        End If
        Dim Riga As Long

        Riga = 0

        If IsNothing(X.Righe(Riga)) Then
            X.Righe(Riga) = New Cls_MovimentiContabiliRiga
        End If


        X.Righe(0).MastroPartita = Mastro
        X.Righe(0).ContoPartita = Conto
        X.Righe(0).SottocontoPartita = Sottoconto
        X.Righe(0).Importo = Importo
        X.Righe(0).Segno = "+"
        X.Righe(0).Tipo = "CF"
        X.Righe(0).RigaDaCausale = 1
        X.Righe(0).DareAvere = MyCau.Righe(0).DareAvere
        X.Righe(Riga).Utente = Session("UTENTE")
        Riga = Riga + 1


        If IsNothing(X.Righe(Riga)) Then
            X.Righe(Riga) = New Cls_MovimentiContabiliRiga
        End If


        Dim X1 As New Cls_CausaleContabile

        X1.Leggi(Session("DC_TABELLE"), Dd_CausaleContabile.SelectedValue)

        Dim xS As New Cls_Pianodeiconti

        Mastro = X1.Righe(1).Mastro
        Conto = X1.Righe(1).Conto
        Sottoconto = X1.Righe(1).Sottoconto

        X.Righe(Riga).MastroPartita = Mastro
        X.Righe(Riga).ContoPartita = Conto
        X.Righe(Riga).SottocontoPartita = Sottoconto
        X.Righe(Riga).Importo = Importo
        X.Righe(Riga).Segno = "+"
        X.Righe(Riga).Tipo = ""
        X.Righe(Riga).RigaDaCausale = 2
        X.Righe(Riga).DareAvere = MyCau.Righe(1).DareAvere
        X.Righe(Riga).Utente = Session("UTENTE")
        Riga = Riga + 1

        If X.Scrivi(Session("DC_GENERALE"), 0) = False Then
            ClientScript.RegisterClientScriptBlock(Me.GetType(), "Errore", "VisualizzaErrore('Errore nella registrazione');", True)
            Exit Sub
        End If

        Dim Legami As New Cls_Legami
        Legami.Importo(0) = Importo
        Legami.NumeroDocumento(0) = NumeroDocumento
        Legami.NumeroPagamento(0) = X.NumeroRegistrazione
        Legami.Scrivi(Session("DC_GENERALE"), 0, X.NumeroRegistrazione)
    End Sub



    Private Sub EseguiJS()
        Dim MyJs As String
        MyJs = "$(document).ready(function() {"

        MyJs = MyJs & "var els = document.getElementsByTagName(""*"");"

        MyJs = MyJs & "for (var i=0;i<els.length;i++)"
        MyJs = MyJs & "if ( els[i].id ) { "
        MyJs = MyJs & " var appoggio =els[i].id; "
        MyJs = MyJs & " if ((appoggio.match('Txt_Data')!= null) || (appoggio.match('Txt_Decesso')!= null))  {  "
        MyJs = MyJs & " $(els[i]).mask(""99/99/9999"");"
        MyJs = MyJs & " $(els[i]).datepicker({ changeMonth: true,changeYear: true,  yearRange: ""1990:" & Year(Now) + 5 & """},$.datepicker.regional[""it""]);"
        MyJs = MyJs & "    }"
        MyJs = MyJs & " if ((appoggio.match('Txt_Importo')!= null) ) {  "
        MyJs = MyJs & " $(els[i]).keypress(function() { ForceNumericInput($(this).val(), true, true); } ); "
        MyJs = MyJs & " $(els[i]).blur(function() { var ap = formatNumber($(this).val(),2); $(this).val(ap); });"
        MyJs = MyJs & "    }"

        MyJs = MyJs & "} "
        MyJs = MyJs & "});"

        'MyJs = "$(document).ready(function() { $('.myClass').keypress(function() { handleEnter($('.myClass').val(), true, true); } );     $('#" & Txt_ImportoPagato.ClientID & "').blur(function() { var ap = formatNumber ($('#" & Txt_ImportoPagato.ClientID & "').val(),2); $('#" & Txt_ImportoPagato.ClientID & "').val(ap); }); });"
        ScriptManager.RegisterStartupScript(Me, Me.GetType(), "visualizzaRitIm", MyJs, True)
    End Sub

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Trim(Session("UTENTE")) = "" Then

            Response.Redirect("..\Login.aspx")
            Exit Sub
        End If

        If Session("ABILITAZIONI").IndexOf("<PORTINERIA>") > 0 Then
            Response.Redirect("..\MainPortineria.aspx")
        End If

        If Val(Request.Item("CodiceOspite")) > 0 And Request.Item("CentroServizio") <> "" Then

            Session("CODICESERVIZIO") = Request.Item("CentroServizio")
            Session("CODICEOSPITE") = Val(Request.Item("CodiceOspite"))
        End If



        If Page.IsPostBack = True Then Exit Sub


        If Request.Item("TIPO") = "CAPARRA" Then

            RB_Fat.Text = "Caparra"
            RB_Nc.Text = "NC Caparra"

        End If

        Dim K1 As New Cls_SqlString

        Dim Barra As New Cls_BarraSenior

        If Not IsNothing(Session("RicercaAnagraficaSQLString")) Then
            K1 = Session("RicercaAnagraficaSQLString")
        End If

        Lbl_BarraSenior.Text = Barra.CodiceBarra(Application("SENIOR"), Session("UTENTE"), Page, K1)

        Dim x As New ClsOspite

        x.CodiceOspite = Session("CODICEOSPITE")
        x.Leggi(Session("DC_OSPITE"), x.CodiceOspite)

        Dim KCs As New Cls_DatiOspiteParenteCentroServizio

        KCs.CentroServizio = Session("CODICESERVIZIO")
        KCs.CodiceOspite = Session("CODICEOSPITE")
        KCs.CodiceParente = 0
        KCs.Leggi(Session("DC_OSPITE"))

        REM Assegna la tipologia operazione e l'aliquota prendendo quella relativa al cserv
        If KCs.CodiceOspite <> 0 Then
            x.TIPOOPERAZIONE = KCs.TipoOperazione
            x.CODICEIVA = KCs.AliquotaIva
            x.FattAnticipata = KCs.Anticipata
            x.MODALITAPAGAMENTO = KCs.ModalitaPagamento
            x.Compensazione = KCs.Compensazione
            x.SETTIMANA = KCs.Settimana
        End If


        Dim cs As New Cls_CentroServizio

        cs.Leggi(Session("DC_OSPITE"), Session("CODICESERVIZIO"))

        Lbl_NomeOspite.Text = x.Nome & " -  " & x.DataNascita & " - " & cs.DESCRIZIONE & "<i style=""font-size:small;"">(" & x.CodiceOspite & ")</i>"
        If Val(Request.Item("CodiceParente")) < 99 And Val(Request.Item("CodiceParente")) > 0 Then
            Dim Parente As New Cls_Parenti

            Parente.CodiceOspite = x.CodiceOspite
            Parente.CodiceParente = Val(Request.Item("CodiceParente"))
            Parente.Leggi(Session("DC_OSPITE"), Parente.CodiceOspite, Parente.CodiceParente)
            Lbl_NomeOspite.Text = x.Nome & " -  " & x.DataNascita & " - " & cs.DESCRIZIONE & " - " & Parente.Nome & "<i style=""font-size:small;"">(" & x.CodiceOspite & ")</i>"
        End If


        If Request.Item("TIPO") <> "CAPARRA" Then

            Dim TipOper As New Cls_TipoOperazione

            TipOper.Codice = x.TIPOOPERAZIONE
            TipOper.Leggi(Session("DC_OSPITE"), TipOper.Codice)

            If TipOper.RegolaDeposito = "I" Then
                Txt_Importo.Text = Format(TipOper.ImportoDeposito, "#,##0.00")
            End If
            If TipOper.RegolaDeposito = "3" Then
                Dim Cl As New Cls_CalcoloRette
                Cl.STRINGACONNESSIONEDB = Session("DC_OSPITE")
                Cl.ApriDB(Session("DC_OSPITE"), Session("DC_OSPITIACCESSORI"))
                If Request.Item("CodiceParente") = 99 Then
                    Txt_Importo.Text = Format(Cl.QuoteGiornaliere(Session("CODICESERVIZIO"), Session("CODICEOSPITE"), "O", 0, Now) * 30, "#,##0.00")
                Else
                    Txt_Importo.Text = Format(Cl.QuoteGiornaliere(Session("CODICESERVIZIO"), Session("CODICEOSPITE"), "P", Val(Request.Item("CodiceParente")), Now) * 30, "#,##0.00")
                End If
                Cl.ChiudiDB()
            End If
            If TipOper.RegolaDeposito = "1" Then
                Dim Cl As New Cls_CalcoloRette
                Cl.STRINGACONNESSIONEDB = Session("DC_OSPITE")
                Cl.ApriDB(Session("DC_OSPITE"), Session("DC_OSPITIACCESSORI"))
                If Request.Item("CodiceParente") = 99 Then
                    Txt_Importo.Text = Format(Cl.QuoteGiornaliere(Session("CODICESERVIZIO"), Session("CODICEOSPITE"), "O", 0, Now) * 31, "#,##0.00")
                Else
                    Txt_Importo.Text = Format(Cl.QuoteGiornaliere(Session("CODICESERVIZIO"), Session("CODICEOSPITE"), "P", Val(Request.Item("CodiceParente")), Now) * 31, "#,##0.00")
                End If
                Cl.ChiudiDB()
            End If
        End If

        Dim k As New Cls_CausaleContabile
        Dim ConnectionString As String = Session("DC_TABELLE")

        k.UpDateDropBoxPag(ConnectionString, Dd_CausaleContabile)

        Txt_DataRegistrazione.Text = Format(Now, "dd/MM/yyyy")

        Call CalcolaTotale()
        Call EseguiJS()
    End Sub

    Protected Sub Btn_Modifica_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles Btn_Modifica.Click

        If VerificaSeInserito() Then
            Response.Redirect("ElencoRegistrazione.aspx")
            Exit Sub
        End If

        If Request.Item("TIPO") = "CAPARRA" Then
            If RB_Fat.Checked = True Then
                Call CreaDocumento("DOCCAP")
            End If
            If RB_Nc.Checked = True Then
                Call CreaDocumento("NCCAP")
            End If
        Else
            If RB_Fat.Checked = True Then
                Call CreaDocumento("DOCDEP")
                Dim SOspite As New ClsOspite

                SOspite.CodiceOspite = Session("CODICEOSPITE")
                SOspite.Leggi(Session("DC_OSPITE"), SOspite.CodiceOspite)

                SOspite.DataDeposito = Txt_DataRegistrazione.Text
                SOspite.ImportoDeposito = CDbl(Txt_Totale.Text)
                SOspite.ScriviOspite(Session("DC_OSPITE"))
            End If
            If RB_Nc.Checked = True Then
                Call CreaDocumento("NCDEP")

                Dim SOspite As New ClsOspite

                SOspite.CodiceOspite = Session("CODICEOSPITE")
                SOspite.Leggi(Session("DC_OSPITE"), SOspite.CodiceOspite)

                SOspite.DataAccreditoDeposito = Txt_DataRegistrazione.Text
                SOspite.ScriviOspite(Session("DC_OSPITE"))
            End If
        End If

        Response.Redirect("ElencoRegistrazione.aspx?CodiceParente=" & Val(Request.Item("CodiceParente")))
        Call EseguiJS()
    End Sub

    Protected Sub Btn_Esci_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles Btn_Esci.Click
        If Val(Request.Item("CodiceOspite")) > 0 And Request.Item("CentroServizio") <> "" Then

            Session("CODICESERVIZIO") = Request.Item("CentroServizio")
            Session("CODICEOSPITE") = Val(Request.Item("CodiceOspite"))
        End If


        Response.Redirect("ElencoRegistrazione.aspx?CodiceParente=" & Val(Request.Item("CodiceParente")) & "&CentroServizio=" & Session("CODICESERVIZIO") & "&CodiceOspite=" & Session("CODICEOSPITE"))
    End Sub

    Protected Sub Txt_Importo_TextChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles Txt_Importo.TextChanged
        Call CalcolaTotale()
    End Sub

    Protected Sub CalcolaTotale()
        Dim x As New ClsOspite
        Dim ConnectionString As String = Session("DC_OSPITE")
        Dim ConnectionStringTabelle As String = Session("DC_TABELLE")
        Dim ConnectionStringGenerale As String = Session("DC_GENERALE")
        Dim ImportIVA As Double

        x.Leggi(ConnectionString, Session("CODICEOSPITE"))

        Dim KCs As New Cls_DatiOspiteParenteCentroServizio

        KCs.CentroServizio = Session("CODICESERVIZIO")
        KCs.CodiceOspite = Session("CODICEOSPITE")
        KCs.CodiceParente = 0
        KCs.Leggi(ConnectionString)

        REM Assegna la tipologia operazione e l'aliquota prendendo quella relativa al cserv
        If KCs.CodiceOspite <> 0 Then
            x.TIPOOPERAZIONE = KCs.TipoOperazione
            x.CODICEIVA = KCs.AliquotaIva
            x.FattAnticipata = KCs.Anticipata
            x.MODALITAPAGAMENTO = KCs.ModalitaPagamento
            x.Compensazione = KCs.Compensazione
            x.SETTIMANA = KCs.Settimana
        End If

        Dim TipoOperazione As New Cls_TipoOperazione

        TipoOperazione.Leggi(ConnectionString, x.TIPOOPERAZIONE)

        Dim CIva As New Cls_IVA

        CIva.Codice = x.CODICEIVA


        If Request.Item("TIPO") = "CAPARRA" Then
            CIva.Codice = TipoOperazione.CodiceIVACaparra
        End If
        CIva.Leggi(Session("DC_TABELLE"), CIva.Codice)

        If Txt_Importo.Text = "" Then
            Txt_Importo.Text = "0,00"

        End If

        ImportIVA = CDbl(CDbl(Txt_Importo.Text) * CIva.Aliquota)

        TxT_Iva.Text = Format(ImportIVA, "#,##0.00")
        Txt_Totale.Text = Format(CDbl(Txt_Importo.Text) + ImportIVA, "#,##0.00")

    End Sub
End Class
