﻿<%@ Page Language="VB" AutoEventWireup="false" Inherits="OspitiWeb_Rendiconto" CodeFile="Rendiconto.aspx.vb" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="xasp" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
    <meta http-equiv="x-ua-compatible" content="IE=9" />
    <title>Stampa Rendiconto</title>
    <asp:PlaceHolder runat="server">
        <%: System.Web.Optimization.Styles.Render("~/Content/AjaxControlToolkit/Styles/Bundle") %>
    </asp:PlaceHolder>
    <link href="ospiti.css?ver=11" rel="stylesheet" type="text/css" />
    <link rel="shortcut icon" href="images/SENIOR.ico" />
    <link href="js/jquery.autocomplete.css" rel="stylesheet" type="text/css" />

    <script src="js/jquery-1.5.1.min.js" type="text/javascript"></script>
    <script src="js/jquery.autocomplete.js" type="text/javascript"></script>
    <script src="js/soapclient.js" type="text/javascript"></script>
    <script src="/js/convertinumero.js" type="text/javascript"></script>

    <script src="js/jquery.maskedinput-1.3.min.js" type="text/javascript"></script>
    <script src="/js/formatnumer.js" type="text/javascript"></script>

    <script src="/js/pianoconti.js" type="text/javascript"></script>
    <script src="js/JSErrore.js" type="text/javascript"></script>

    <script src="js/chosen/chosen.jquery.js" type="text/javascript"></script>
    <script src="js/chosen/docsupport/prism.js" type="text/javascript" charset="utf-8"></script>
    <link rel="stylesheet" href="js/chosen/docsupport/style.css">
    <link rel="stylesheet" href="js/chosen/docsupport/prism.css">
    <link rel="stylesheet" href="js/chosen/chosen.css">
    <style>
        .wait {
            border: solid 1px #2d2d2d;
            text-align: center;
            vertical-align: top;
            background: white;
            width: 20%;
            height: 180px;
            top: 30%;
            left: 40%;
            position: absolute;
            -moz-border-radius: 5px;
            -webkit-border-radius: 5px;
            border-radius: 5px;
            box-shadow: 0px 0px 10px 4px rgba(0, 125, 196, 0.75);
            -moz-box-shadow: 0px 0px 10px 4px rgba(0, 125, 196, 0.75);
            -webkit-box-shadow: 0px 0px 10px 4px rgba(0, 125, 196, 0.75);
        }

        #blur {
            width: 100%;
            background-color: black;
            moz-opacity: 0.5;
            khtml-opacity: .5;
            opacity: .5;
            filter: alpha(opacity=50);
            z-index: 120;
            height: 100%;
            position: absolute;
            top: 0;
            left: 0;
        }

        #progress {
            z-index: 200;
            background-color: White;
            position: absolute;
            top: 0pt;
            left: 0pt;
            border: solid 1px black;
            padding: 5px 5px 5px 5px;
            text-align: center;
        }

        .NoteCheck {
            float: right;
            margin-right: 50px;
            font-size: x-small;
            width: 70%;
        }

        .chosen-container-single .chosen-single {
            height: 30px;
            background: white;
            color: Black;
            font-family: "Cuprum","Roboto","Calibri",Lucida Grande,"Helvetica Neue",Helvetica,Arial,sans-serif;
            font-size: 16px;
        }

        .chosen-container {
            font-family: "Cuprum","Roboto","Calibri",Lucida Grande,"Helvetica Neue",Helvetica,Arial,sans-serif;
            font-size: 16px;
        }
    </style>
    <script type="text/javascript"> 
        $(document).ready(function () {
            if (window.innerHeight > 0) { $("#BarraLaterale").css("height", (window.innerHeight - 94) + "px"); } else { $("#BarraLaterale").css("height", (document.documentElement.offsetHeight - 94) + "px"); }
        });

        function Stampa() {
            if ($("#OpzioniStampa").css("visibility") != "hidden") {
                $("#OpzioniStampa").css('visibility', 'hidden');
                localStorage.setItem("OpzioniStampa", "invisibile");
            } else {
                $("#OpzioniStampa").css('visibility', 'visible');
                localStorage.setItem("OpzioniStampa", "visibile");
            }
        }

        function openPopUp(urlToOpen, nome, parametri) {
            var popup_window = window.open(urlToOpen, '_blank');
            try {
                popup_window.focus();
            } catch (e) {
                alert("E' stato bloccato l'apertura del popup da parte del browser");
            }
        }
    </script>
</head>
<body>
    <form id="form1" runat="server">
        <asp:ScriptManager ID="ScriptManager1" runat="server" AsyncPostBackTimeout="600" EnableScriptGlobalization="true">
            <Scripts>
                <asp:ScriptReference Path="~/Scripts/AjaxControlToolkit/Bundle" />
            </Scripts>
        </asp:ScriptManager>
        <asp:Label ID="Lbl_BarraSenior" runat="server" Text=""></asp:Label>
        <div align="left">
            <table style="width: 100%;" cellpadding="0" cellspacing="0">
                <tr>
                    <td style="width: 160px; background-color: #F0F0F0;"></td>
                    <td>
                        <div class="Titolo">Ospiti - Stampa Rendiconti</div>
                        <div class="SottoTitolo">
                            <asp:Label ID="Lbl_NomeOspite" runat="server"></asp:Label><br />
                            <br />
                        </div>
                    </td>
                    <td style="text-align: right;">
                        <div class="DivTasti">
                            <asp:UpdatePanel ID="UpdatePanel1" runat="server">
                                <ContentTemplate>
                                    <a href="#" onclick="Stampa();">
                                        <img src="images\printer-blue.png" class="EffettoBottoniTondi" /></a>
                                    <div id="OpzioniStampa" style="visibility: hidden; position: absolute; float: right; top: 50px; right: 10px; background-color: White; border-color: Black; border-width: 1px; border-style: solid;">
                                        <table>
                                            <tr>
                                                <td style="text-align: center; width: 70px;">
                                                    <asp:ImageButton runat="server" ImageUrl="~/images/printer-blue.png" class="EffettoBottoniTondi" Height="38px" ToolTip="Stampa Sanitaria" ID="ImageButton1"></asp:ImageButton>
                                                </td>
                                                <td style="text-align: center; width: 70px;">
                                                    <asp:ImageButton runat="server" ImageUrl="~/images/printer-blue.png" class="EffettoBottoniTondi" Height="38px" ToolTip="Stampa Sociale" ID="Btn_Modifica"></asp:ImageButton>
                                                </td>
                                                <td style="text-align: center; width: 70px;">
                                                    <asp:ImageButton runat="server" ImageUrl="~/images/printer-blue.png" class="EffettoBottoniTondi" Height="38px" ToolTip="Stampa Jolly" ID="Img_Jolly"></asp:ImageButton>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td style="text-align: center;">Sanitario
                                                </td>
                                                <td style="text-align: center;">Sociale
                                                </td>
                                                <td style="text-align: center;">Jolly
                                                </td>
                                            </tr>
                                        </table>
                                    </div>
                                </ContentTemplate>
                            </asp:UpdatePanel>
                        </div>
                    </td>
                </tr>
                <tr>
                    <td style="width: 160px; background-color: #F0F0F0; vertical-align: top; text-align: center;" id="BarraLaterale">
                        <asp:ImageButton ID="Btn_Esci" runat="server" BackColor="Transparent" ImageUrl="../images/Menu_Indietro.png" class="Effetto" ToolTip="Chiudi" />
                        <br />
                        <br />
                        <br />
                        <br />
                        <br />
                        <br />
                        <br />
                        <br />
                        <br />
                        <br />
                        <br />
                        <br />
                        <br />
                    </td>
                    <td colspan="2" style="background-color: #FFFFFF;" valign="top">
                        <xasp:TabContainer ID="TabContainer1" runat="server" ActiveTabIndex="0" CssClass="TabSenior" Width="100%" BorderStyle="None" Style="margin-right: 39px">
                            <xasp:TabPanel runat="server" HeaderText="Prima Nota" ID="Tab_Anagrafica">
                                <HeaderTemplate>
                                    Rendiconto
                                </HeaderTemplate>
                                <ContentTemplate>
                                    <label class="LabelCampo">Anno :</label>
                                    <asp:TextBox ID="Txt_Anno" onkeypress="return soloNumeri(event);" runat="server" Width="80px" MaxLength="4"></asp:TextBox>
                                    <br />
                                    <br />
                                    <label class="LabelCampo">Mese Da :</label>
                                    <asp:DropDownList ID="DD_MeseDa" runat="server" Width="168px">
                                    </asp:DropDownList>
                                    <br />
                                    <br />
                                    <br />
                                    <label class="LabelCampo">Mese A :</label>
                                    <asp:DropDownList ID="DD_MeseA" runat="server" Width="168px">
                                    </asp:DropDownList>
                                    <br />
                                    <br />
                                    <label class="LabelCampo">Struttura:</label>
                                    <asp:DropDownList runat="server" ID="DD_Struttura"
                                        AutoPostBack="True">
                                    </asp:DropDownList><br />
                                    <br />
                                    <label class="LabelCampo">Centro Servizio :</label>
                                    <asp:DropDownList ID="DD_CServ" runat="server" Width="354px"></asp:DropDownList><br />
                                    <br />
                                    <label class="LabelCampo">Comune :</label>
                                    <asp:DropDownList ID="DD_Comune" runat="server" class="chosen-select"></asp:DropDownList><br />
                                    <br />
                                    <label class="LabelCampo">Regione :</label>
                                    <asp:DropDownList ID="DD_Regione" runat="server" class="chosen-select" Width="354px"></asp:DropDownList><br />
                                    <br />
                                    <label class="LabelCampo">Filtro Anno :</label>
                                    <asp:TextBox ID="Txt_FiltroAnno" onkeypress="return soloNumeri(event);" runat="server" Width="80px" MaxLength="4"></asp:TextBox>
                                    <br />
                                    <br />
                                    <label class="LabelCampo">Tipo Stampa :</label>
                                    <asp:CheckBox ID="Chk_StampaDownloadPerGruppo" runat="server" Text="Separa per Ente/Centro Servizio" /><br />
                                    <br />
                                    <label class="LabelCampo">Seleziona Report:</label>
                                    <asp:DropDownList ID="DD_Report" runat="server"></asp:DropDownList>
                                    <br />
                                    <br />
                                    <div id="msg"></div>
                                    <br />
                                </ContentTemplate>
                            </xasp:TabPanel>

                            <xasp:TabPanel runat="server" HeaderText="Selezione" ID="Tab_Selezione">
                                <HeaderTemplate>Selezioni</HeaderTemplate>
                                <ContentTemplate>
                                    <table>
                                        <tr>
                                            <td>
                                                <b>OSPITE</b>
                                            </td>
                                            <td>
                                                <b>ORGANIZZAZIONE INFO STAMPA</b>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                <asp:CheckBox ID="ChkPresenze" runat="server" Text="Solo Presenze" ToolTip="Include solo le presenze, senza includere le assenze" /><br />
                                                <i class="NoteCheck">Include solo le presenze, senza includere le assenze</i><br />
                                                <asp:CheckBox ID="ChkAssenza" runat="server" Text="Solo Assenze" ToolTip="Include solo le assenze, senza includere le presenze" /><br />
                                                <i class="NoteCheck">Include solo le assenze, senza includere le presenze</i><br />
                                                <asp:CheckBox ID="Chk_NonAuto" runat="server" Text="Non Autosufficienti" ToolTip="Su Rendiconto Sociale, include solo le rette di tipo Non Autosufficienti" />
                                                <font color="aqua">*</font>
                                                <br />
                                                <i class="NoteCheck">Su Rendiconto Sociale, include solo le rette di tipo Non Autosufficienti</i><br />
                                                <asp:CheckBox ID="Chk_Auto" runat="server" Text="Autosufficienti" ToolTip="Su Rendiconto Sociale, include solo le rette di tipo Autosufficienti" />
                                                <font color="aqua">*</font>
                                                <br />
                                                <i class="NoteCheck">Su Rendiconto Sociale, include solo le rette di tipo Autosufficienti</i><br />
                                                <asp:CheckBox ID="Chk_MovImp" runat="server" Text="Impegnativa" ToolTip="Include i dati dell'impegnativa in descrizione" /><br />
                                                <i class="NoteCheck">Include i dati dell'impegnativa in descrizione</i><br />
                                                <asp:CheckBox ID="Chk_MovEU" runat="server" Text="Movimenti Entrata/Uscita" ToolTip="Include i movimenti di entrata uscita nel periodo" /><br />
                                                <i class="NoteCheck">Include i movimenti di entrata uscita nel periodo</i><br />
                                                <asp:CheckBox ID="Chk_MovStato" runat="server" Text="Movimenti Entrata/Uscita (Stato Auto = Accoglimento)" ToolTip="Include i movimenti di entrata uscita nel periodo,includendo il cambio stao come accoglimento, solo sanitario" />
                                                <font color="purple">*</font>
                                                <br />
                                                <i class="NoteCheck">Include i movimenti di entrata uscita nel periodo,includendo il cambio stao come accoglimento, solo sanitario</i><br />
                                                <asp:CheckBox ID="Chk_NonIncludereDataNascita" runat="server" Text="Non Includere Data Nascita" ToolTip="Non Riporta data nascita nel nome ospite" /><br />
                                                <i class="NoteCheck">Non Riporta data nascita nel nome ospite</i><br />
                                                <asp:CheckBox ID="Chk_SoloIniziali" runat="server" Text="Solo Iniziali Cognome e Nome" ToolTip="Solo Iniziali" /><br />
                                                <i class="NoteCheck">Solo Iniziali Cognome e Nome</i><br />
                                                <asp:CheckBox ID="Chk_AccoglimentoUscitaDef" runat="server" Text="Accoglimento / Uscita Definitiva" ToolTip="Riporta solo i movimenti di Accoglimento e Uscita definitiva, solo sociale" />
                                                <font color="aqua">*</font>
                                                <br />
                                                <i class="NoteCheck">Riporta solo i movimenti di Accoglimento e Uscita definitiva, solo sociale</i><br />
                                                <asp:CheckBox ID="ChkDataIniFinePrestazione" runat="server" Text="Date Dal/Al prestazione" ToolTip="Riporta Data Inizio-Fine Prestazione" /><br />
                                                <i class="NoteCheck">Riporta Data Inizio-Fine Prestazione</i><br />
                                            </td>
                                            <td style="vertical-align: top; width: 50%;">
                                                <asp:CheckBox ID="Chk_NomeInVisualizzaioneInUsl" runat="server" Text="Nome in visualizzazione in Regione" ToolTip="Usa il campo nome in visualizzazione per l'intestazione del rendiconto, solo sanitario" />
                                                <font color="purple">*</font>
                                                <br />
                                                <i class="NoteCheck">Usa il campo nome in visualizzazione per l'intestazione del rendiconto, solo sanitario</i><br />
                                                <asp:CheckBox ID="Chk_Raggruppamento" runat="server" Text="Raggruppamento Regione" ToolTip="Usa il ragruppamento regioni in rendiconto" /><br />
                                                <i class="NoteCheck">Usa il ragruppamento regioni in rendiconto</i><br />
                                            </td>
                                        </tr>
                                        <tr>
                                            <td></td>
                                            <td></td>
                                        </tr>
                                        <tr>
                                            <td><b>ORGANIZZAZIONE - VISUALIZZAZIONE IMPORTI</b></td>
                                        </tr>
                                        <tr>
                                            <td>
                                                <asp:CheckBox ID="Chk_ImpGiornalieri" runat="server" Text="Importo Giornaliero" ToolTip="Riporta le quote giornaliere in stampa" /><br />
                                                <i class="NoteCheck">Riporta le quote giornaliere in stampa</i><br />
                                                <asp:CheckBox ID="Chk_ImpComEnt" runat="server" Text="Importo Comune-Ente" ToolTip="Sottrae la quota ente dalla rette del comune, solo rendiconto sociale" />
                                                <font color="aqua">*</font>
                                                <br />
                                                <i class="NoteCheck">Sottrae la quota ente dalla rette del comune, solo rendiconto sociale</i><br />
                                                <asp:CheckBox ID="Chk_ImportoA0" runat="server" Text="Importo a 0" ToolTip="Riporta solo ospiti con importo regione/comune diverso da zero" /><br />
                                                <i class="NoteCheck">Riporta anche ospiti con importo regione/comune uguale zero</i><br />
                                                <asp:CheckBox ID="Chk_SoloAddebitiAccrediti" runat="server" Text="Solo Addebiti/Accrediti" ToolTip="include solo gli addebiti e accrediti e non la retta (mostrando il totale regione e comune, solo per il valore degli add/acc)" /><br />
                                                <i class="NoteCheck">include solo gli addebiti e accrediti e non la retta (mostrando il totale regione e comune, solo per il valore degli add/acc)</i><br />
                                                <asp:CheckBox ID="Chk_SoloRetta" runat="server" Text="Solo Retta" ToolTip="Indica di non includere gli addebiti accrediti nell'importo estratto" /><br />
                                                <i class="NoteCheck">Indica di non includere gli addebiti accrediti nell'importo estratto</i><br />
                                                <asp:CheckBox ID="Chk_TipoRetta" runat="server" Text="Tipo Retta" ToolTip="Riporta tipo retta ospite" /><br />
                                                <i class="NoteCheck">Riporta tipo retta ospite</i><br />
                                                <asp:CheckBox ID="Chk_ADDACR" runat="server" Text="Addebiti Accrediti" ToolTip="Visualizza gli addebiti accrediti" /><br />
                                                <i class="NoteCheck">Visualizza gli addebiti accrediti del comune/regione</i><br />
                                                <asp:CheckBox ID="Chk_ACOspitiParenti" runat="server" AutoPostBack="true" Text="Ad-Ac Ospiti/Parenti" ToolTip="Visualizza gli addebiti accrediti di Ospite/Parente" /><br />
                                                <i class="NoteCheck">Visualizza gli addebiti accrediti di Ospite/Parente</i><br />
                                                <asp:CheckBox ID="Chk_NonExtra" runat="server" Text="Extra Fissi" AutoPostBack="true" ToolTip="Visualizza gli extra fissi di ospite/parente/comune" /><br />
                                                <i class="NoteCheck">Visualizza gli extra fissi di ospite/parente/comune</i><br />
                                            </td>
                                            <td></td>
                                        </tr>
                                    </table>
                                    <font color="aqua">*</font>Solo Rendiconto Sociale<br />
                                    <font color="purple">*</font>Solo Rendiconto Sanitario<br />
                                    <br />
                                </ContentTemplate>
                            </xasp:TabPanel>
                        </xasp:TabContainer>
                    </td>
                </tr>
            </table>

            <asp:UpdateProgress ID="UpdateProgress1" runat="server" AssociatedUpdatePanelID="UpdatePanel1">
                <ProgressTemplate>
                    <div id="blur"></div>
                    <div id="Div1"></div>
                    <div id="pippo" class="wait">
                        <br />
                        <img height="30px" src="images/loading.gif"><br />
                    </div>
                </ProgressTemplate>
            </asp:UpdateProgress>
        </div>
    </form>
</body>
</html>
