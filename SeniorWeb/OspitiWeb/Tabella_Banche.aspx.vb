﻿
Partial Class OspitiWeb_Tabella_Banche
    Inherits System.Web.UI.Page


    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Trim(Session("UTENTE")) = "" Then
            Response.Redirect("../Login.aspx")
            Exit Sub
        End If

        EseguiJS()

        If Page.IsPostBack = True Then
            Exit Sub
        End If

        Dim K1 As New Cls_SqlString

        Dim Barra As New Cls_BarraSenior

        If Not IsNothing(Session("RicercaAnagraficaSQLString")) Then
            K1 = Session("RicercaAnagraficaSQLString")
        End If

        Lbl_BarraSenior.Text = Barra.CodiceBarra(Application("SENIOR"), Session("UTENTE"), Page, K1)


        If Val(Request.Item("CODICE")) = 0 Then
            Dim MaxBanche As New Cls_Banche


            Txt_Codice.Text = MaxBanche.MaxBanche(Session("DC_OSPITE"))
            Exit Sub
        End If


        Dim Med As New Cls_Banche


        Med.Codice = Request.Item("CODICE")
        Med.Leggi(Session("DC_OSPITE"), Med.Codice)
        Txt_Codice.Text = Med.Codice

        Txt_Descrizione.Text = Med.Descrizione
        Txt_Agenzia.Text = Med.Agenzia
        Txt_Indirizzo.Text = Med.Indirizzo

        Dim DeCom As New ClsComune
        DeCom.Comune = Med.Comune
        DeCom.Provincia = Med.Provincia
        DeCom.DecodficaComune(Session("DC_OSPITE"))
        Txt_ComRes.Text = Med.Provincia & " " & Med.Comune & " " & DeCom.Descrizione

        Txt_Cap.Text = Med.Cap

        Txt_IBANRID.Text = Med.IBANRID        
        Txt_EndToEndId.Text = Med.EndToEndId
        Txt_PrvtId.Text = Med.PrvtId
        Txt_MmbId.Text = Med.MmbId
        Txt_OrgId.Text = Med.OrgId

        Txt_Codice.Enabled = False
    End Sub




    Protected Sub Btn_Modifica_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles Btn_Modifica.Click
        Dim Med As New Cls_Banche

        If Txt_Codice.Text.Trim = "" Then
            ClientScript.RegisterClientScriptBlock(Me.GetType(), "Errore", "VisualizzaErrore('Specificare codice');", True)
            Exit Sub
        End If
        If Txt_Descrizione.Text.Trim = "" Then
            ClientScript.RegisterClientScriptBlock(Me.GetType(), "Errore", "VisualizzaErrore('Specificare Descrizione');", True)
            Exit Sub
        End If


        Med.Codice = Txt_Codice.Text
        Med.Descrizione = Txt_Descrizione.Text
        Med.Agenzia = Txt_Agenzia.Text
        Med.Indirizzo = Txt_Indirizzo.Text

        Dim Vettore(100) As String

        If Txt_ComRes.Text <> "" Then
            Vettore = SplitWords(Txt_ComRes.Text)
            If Vettore.Length > 1 Then
                Med.Provincia = Vettore(0)
                Med.Comune = Vettore(1)
            End If
        End If

        Med.Cap = Txt_Cap.Text

        Med.IBANRID = Txt_IBANRID.Text
        Med.EndToEndId = Txt_EndToEndId.Text
        Med.PrvtId = Txt_PrvtId.Text
        Med.MmbId = Txt_MmbId.Text
        Med.OrgId = Txt_OrgId.Text

        Med.Scrivi(Session("DC_OSPITE"))

        Call PaginaPrecedente()
    End Sub



    Private Function SplitWords(ByVal s As String) As String()
        '
        ' Call Regex.Split function from the imported namespace.
        ' Return the result array.
        '
        Return Regex.Split(s, "\W+")
    End Function

    Protected Sub ImageButton1_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles ImageButton1.Click
        If Txt_Codice.Text.Trim = "" Then
            ClientScript.RegisterClientScriptBlock(Me.GetType(), "Errore", "VisualizzaErrore('Specificare codice');", True)
            Exit Sub
        End If

        Dim Banca As New Cls_Banche
        Banca.Codice = Txt_Codice.Text



        Banca.Delete(Session("DC_OSPITE"))

        Call PaginaPrecedente()
    End Sub

    Protected Sub Btn_Esci_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles Btn_Esci.Click

        Call PaginaPrecedente()
    End Sub
    Private Sub PaginaPrecedente()
        Response.Redirect("Elenco_Banche.aspx")
    End Sub



    Private Sub EseguiJS()
        Dim MyJs As String
        MyJs = "$(document).ready(function() {"

        MyJs = MyJs & "var els = document.getElementsByTagName(""*"");"

        MyJs = MyJs & "for (var i=0;i<els.length;i++)"
        MyJs = MyJs & "if ( els[i].id ) { "
        MyJs = MyJs & " var appoggio =els[i].id; "
        MyJs = MyJs & " if (appoggio.match('TxtData')!= null) {  "
        MyJs = MyJs & " $(els[i]).mask(""99/99/9999"");"
        MyJs = MyJs & "    }"
        MyJs = MyJs & " if ((appoggio.match('TxtImporto')!= null) || (appoggio.match('Txt_Percentuale')!= null) ) {  "
        MyJs = MyJs & " $(els[i]).keypress(function() { ForceNumericInput($(this).val(), true, true); } ); "
        MyJs = MyJs & " $(els[i]).blur(function() { var ap = formatNumber($(this).val(),2); $(this).val(ap); });"
        MyJs = MyJs & "    }"

        MyJs = MyJs & " if (appoggio.match('Txt_Sottoconto')!= null) {   "
        MyJs = MyJs & " $(els[i]).autocomplete('/WebHandler/GestioneConto.ashx?Utente=" & Session("UTENTE") & "', {delay:5,minChars:3});"
        MyJs = MyJs & "    }"

        MyJs = MyJs & " if (appoggio.match('Txt_ComRes')!= null)  {  "
        MyJs = MyJs & " $(els[i]).autocomplete('/WebHandler/AutocompleteComune.ashx?Utente=" & Session("UTENTE") & "', {delay:5,minChars:3});"
        MyJs = MyJs & "    }"

        MyJs = MyJs & "} "
        MyJs = MyJs & "});"

        'MyJs = "$(document).ready(function() { $('.myClass').keypress(function() { handleEnter($('.myClass').val(), true, true); } );     $('#" & Txt_ImportoPagato.ClientID & "').blur(function() { var ap = formatNumber ($('#" & Txt_ImportoPagato.ClientID & "').val(),2); $('#" & Txt_ImportoPagato.ClientID & "').val(ap); }); });"
        ScriptManager.RegisterStartupScript(Me, Me.GetType(), "visualizzaRitIm", MyJs, True)
    End Sub


    Protected Sub Txt_Codice_TextChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles Txt_Codice.TextChanged
        If Txt_Codice.Enabled = True Then
            Img_VerificaCodice.ImageUrl = "~/images/corretto.gif"

            Dim x As New Cls_TipoExtraFisso

            x.CODICEEXTRA = Txt_Codice.Text
            x.Leggi(Session("DC_OSPITE"))

            If x.Descrizione <> "" Then
                Img_VerificaCodice.ImageUrl = "~/images/errore.gif"
            End If
        End If
        Call EseguiJS()
    End Sub
End Class
