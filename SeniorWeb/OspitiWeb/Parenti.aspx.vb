﻿Imports Microsoft.VisualBasic
Imports System.Data.OleDb
Imports System.Web.Hosting
Imports System.Data.SqlTypes
Imports System.Net
Imports System.IO
Imports System.Security.Cryptography.X509Certificates
Imports System.Net.Security
Imports System.Web.Script.Serialization
Imports org.jivesoftware.util
Imports Newtonsoft.Json
Imports Newtonsoft.Json.Linq

Partial Class Parenti
    Inherits System.Web.UI.Page
    Dim MyTable As New System.Data.DataTable("tabella")
    Dim MyDataSet As New System.Data.DataSet()




    Private Sub Leggi_Parente()
        Dim M As New ClsOspite
        Dim Token As String

        LoginPersonam()
        Token = Session("access_token")

        M.CodiceOspite = Session("CODICEOSPITE")
        M.Leggi(Session("DC_OSPITE"), M.CodiceOspite)


        System.Net.ServicePointManager.ServerCertificateValidationCallback = AddressOf CertificateHandler

        Dim Cs As New Cls_CentroServizio

        Cs.CENTROSERVIZIO = Session("CODICESERVIZIO")
        Cs.Leggi(Session("DC_OSPITE"), Cs.CENTROSERVIZIO)


        Dim client As HttpWebRequest = WebRequest.Create("https://api-v0.e-personam.com/v0/guests/" & M.CODICEFISCALE & "/relatives")
        client.Method = "GET"
        client.Headers.Add("Authorization", "Bearer " & Token)
        client.ContentType = "Content-Type: application/json"

        Dim reader As StreamReader
        Dim response As HttpWebResponse = Nothing

        response = DirectCast(client.GetResponse(), HttpWebResponse)

        reader = New StreamReader(response.GetResponseStream())
        Dim rawresp As String


        rawresp = reader.ReadToEnd()


        Dim jResults As JArray = JArray.Parse(rawresp)

        Dim Appoggio As String

        For Each jTok2 As JToken In jResults

            Dim MyBill As String = ""


            Try
                MyBill = jTok2.Item("signed_to_receive_bill")
            Catch ex As Exception

            End Try


            If MyBill = "true" Or MyBill = "True" Then

                If Txt_CognomeNome.Text <> jTok2.Item("surname").ToString Then
                    Txt_CognomeNome.BackColor = Drawing.Color.Azure
                End If
                Txt_CognomeNome.Text = jTok2.Item("surname").ToString

                If Txt_Nome.Text <> jTok2.Item("firstname").ToString Then
                    Txt_Nome.BackColor = Drawing.Color.Azure
                End If
                Txt_Nome.Text = jTok2.Item("firstname").ToString

                Try
                    Appoggio = jTok2.Item("rescity_alpha6").ToString()

                    Dim ClsCom As New ClsComune

                    ClsCom.Provincia = Mid(Appoggio & Space(6), 1, 3)
                    ClsCom.Comune = Mid(Appoggio & Space(6), 4, 3)
                    ClsCom.Leggi(Session("DC_OSPITE"))

                    If Txt_ComRes.Text <> ClsCom.Provincia & " " & ClsCom.Comune & " " & ClsCom.Descrizione Then
                        Txt_ComRes.BackColor = Drawing.Color.Azure
                    End If

                    Txt_ComRes.Text = ClsCom.Provincia & " " & ClsCom.Comune & " " & ClsCom.Descrizione
                Catch ex As Exception

                End Try

                Try
                    If Txt_Indirizzo.Text <> jTok2.Item("resaddress").ToString() Then
                        Txt_Indirizzo.BackColor = Drawing.Color.Azure
                    End If
                    Txt_Indirizzo.Text = jTok2.Item("resaddress").ToString()
                Catch ex As Exception

                End Try

                Try
                    If Txt_Cap.Text <> jTok2.Item("rescap").ToString() Then
                        Txt_Cap.BackColor = Drawing.Color.Azure
                    End If
                    Txt_Cap.Text = jTok2.Item("rescap").ToString()
                Catch ex As Exception

                End Try

                Try
                    If Txt_Telefono.Text <> jTok2.Item("phone").ToString() Then
                        Txt_Telefono.BackColor = Drawing.Color.Azure
                    End If
                    Txt_Telefono.Text = jTok2.Item("phone").ToString()
                Catch ex As Exception

                End Try

                Try
                    If Txt_Telefono3.Text <> jTok2.Item("email").ToString() Then
                        Txt_Telefono3.BackColor = Drawing.Color.Azure
                    End If
                    Txt_Telefono3.Text = jTok2.Item("email").ToString()
                Catch ex As Exception

                End Try

            End If
        Next


    End Sub

    Sub AggiornaDati()
        Dim x As New Cls_Parenti
        Dim d As New ClsComune
        Dim ConnectionString As String = Session("DC_OSPITE")

        Dim ConnectionStringTabelle As String = Session("DC_TABELLE")

        If Val(Session("CODICEPARENTE")) <> 0 Then
            x.Leggi(ConnectionString, Session("CODICEOSPITE"), Session("CODICEPARENTE"))
        Else
            x.Pulisci()
        End If

        If x.CognomeParente.Trim <> "" Then
            Txt_CognomeNome.Text = x.CognomeParente
            Txt_Nome.Text = x.NomeParente
        Else
            Dim Seperatore As Integer

            Seperatore = x.Nome.IndexOf(" ")

            If Seperatore > 0 Then
                Txt_CognomeNome.Text = Mid(x.Nome, 1, Seperatore).Trim
                Txt_Nome.Text = Mid(x.Nome, Seperatore + 1, Len(x.Nome) - Seperatore).Trim
            Else
                Txt_CognomeNome.Text = x.Nome
            End If
        End If
        If x.Sesso = "F" Then
            RB_Femminile.Checked = True
        End If
        If x.Sesso = "M" Then
            RB_Maschile.Checked = True
        End If

        If x.Opposizione730 = 1 Then
            chk_Opposizione730.Checked = True
        Else
            chk_Opposizione730.Checked = False
        End If

        If IsDBNull(x.DataNascita) Then
            Txt_DataNascita.Text = ""
        Else
            If Year(x.DataNascita) < 1900 Then
                Txt_DataNascita.Text = ""
            Else
                Txt_DataNascita.Text = Format(x.DataNascita, "dd/MM/yyyy")
            End If
        End If

        Txt_Note.Content = x.NOTE
        Dim DecCom As New ClsComune


        DecCom.Comune = x.ComuneDiNascita
        DecCom.Provincia = x.ProvinciaDiNascita
        DecCom.DecodficaComune(Session("DC_OSPITE"))
        Txt_Comune.Text = Trim(DecCom.Provincia & " " & DecCom.Comune & " " & DecCom.Descrizione)



        Txt_CodiceFiscale.Text = x.CODICEFISCALE


        If Not IsNothing(Session("ABILITAZIONI")) Then
            If Session("ABILITAZIONI").IndexOf("<EPERSONAM>") >= 0 And Val(Session("CODICEPARENTE")) <> 0 Then
                Txt_CodiceFiscale.Enabled = False
                Txt_Nome.Enabled = False
                Txt_CognomeNome.Enabled = False
                Txt_Indirizzo.Enabled = False
                Txt_ComRes.Enabled = False

                Button1.Enabled = False
            End If
        End If


        Txt_Telefono.Text = x.Telefono1
        Txt_Telefono2.Text = x.Telefono2
        Txt_Telefono3.Text = x.Telefono3


        Txt_Telefono4_Mod.Text = x.Telefono4
        Txt_ImportoSconto.Text = Format(x.ImportoSconto, "#,##0.00")
        Dim Lp As New Cls_Sconto

        Lp.UpDateDropBoxCentroServizio(Session("DC_OSPITE"), dd_TipoSconto, Session("CODICESERVIZIO"))



        dd_TipoSconto.SelectedValue = x.TipoSconto


        Txt_Indirizzo.Text = x.RESIDENZAINDIRIZZO1
        Txt_Cap.Text = x.RESIDENZACAP1


        Txt_Esportazione.Text = x.CONTOPERESATTO

        Txt_CodiceCup.Text = x.CodiceCup


        If x.FattAnticipata = "S" Then
            Chk_Anticipata.Checked = True
        Else
            Chk_Anticipata.Checked = False
        End If

        If x.RotturaOspite = 1 Then
            Chk_RotturaOspite.Checked = True
        Else
            Chk_RotturaOspite.Checked = False
        End If

        DecCom.Comune = x.RESIDENZACOMUNE1
        DecCom.Provincia = x.RESIDENZAPROVINCIA1
        DecCom.DecodficaComune(Session("DC_OSPITE"))
        Txt_ComRes.Text = Trim(DecCom.Provincia & " " & DecCom.Comune & " " & DecCom.Descrizione)



        Txt_NomeRecapito.Text = x.RECAPITONOME
        Txt_IndirizzoRecapito.Text = x.RECAPITOINDIRIZZO


        DecCom.Comune = x.RECAPITOCOMUNE
        DecCom.Provincia = x.RECAPITOPROVINCIA
        DecCom.DecodficaComune(Session("DC_OSPITE"))
        Txt_ComuneRecapito.Text = Trim(DecCom.Provincia & " " & DecCom.Comune & " " & DecCom.Descrizione)



        Txt_Cap4.Text = x.RESIDENZACAP4





        If x.CONSENSOINSERIMENTO = 2 Then
            RB_NOConsenso1.Checked = True
        End If

        If x.CONSENSOINSERIMENTO = 1 Then
            RB_SIConsenso1.Checked = True
        End If



        If x.CONSENSOMARKETING = 0 Or x.CONSENSOMARKETING = 2 Then
            RB_NOConsenso2.Checked = True
        End If

        If x.CONSENSOMARKETING = 1 Then
            RB_SIConsenso2.Checked = True
        End If


        Dim DDTipoOpe As New Cls_TipoOperazione
        'DDTipoOpe.UpDateDropBox(ConnectionString, DD_TipoOperazione)
        DDTipoOpe.UpDateDropBoxCentroServizio(ConnectionString, DD_TipoOperazione, Session("CODICESERVIZIO"), "O")


        Dim DDModalita As New ClsModalitaPagamento
        DDModalita.UpDateDropBox(ConnectionString, DD_ModalitaPagamento)

        Dim DDIVA As New Cls_IVA
        DDIVA.UpDateDropBox(ConnectionStringTabelle, DD_IVA)



        Dim DDGRParentela As New Cls_GradoParentela
        DDGRParentela.UpDateDropBox(Session("DC_OSPITE"), DD_Parentela)

        DD_TipoOperazione.SelectedValue = x.TIPOOPERAZIONE
        DD_ModalitaPagamento.SelectedValue = x.MODALITAPAGAMENTO
        DD_IVA.SelectedValue = x.CODICEIVA
        DD_Parentela.SelectedValue = x.GradoParentela

        If x.PERIODO = "M" Then
            RB_Mensile.Checked = True
        End If

        If x.PERIODO = "B" Then
            RB_Bimestrale.Checked = True
        End If
        If x.PERIODO = "T" Then
            RB_Trimestrale.Checked = True
        End If

        Chk_ExtraFuoriFattura.Visible = False
        RB_NO.Checked = False
        RB_SI.Checked = False
        If x.Compensazione = "S" Then
            RB_SI.Checked = True
        End If
        If x.Compensazione = "N" Then
            RB_NO.Checked = True
            Chk_ExtraFuoriFattura.Visible = True
        End If
        If x.Compensazione = "" Then
            RB_SI.Checked = True
        End If
        If x.Compensazione = "D" Then
            RB_Dettaglio.Checked = True
        End If


        If x.IdEpersonam = 0 Then
            Txt_CodiceFiscale.Enabled = True
            Txt_Nome.Enabled = True
            Txt_CognomeNome.Enabled = True
            Txt_Indirizzo.Enabled = True
            Txt_ComRes.Enabled = True

            Button1.Enabled = True
        End If

        If x.ParenteIndirizzo = 1 Then
            Chk_ParenteIndirizzo.Checked = True
        Else
            Chk_ParenteIndirizzo.Checked = False
        End If

        If x.Intestatario = 1 Then
            Chk_Intestatario.Checked = True
        Else
            Chk_Intestatario.Checked = False
        End If

        Txt_Int.Text = x.IntCliente
        Txt_Abi.Text = x.ABICLIENTE
        Txt_Cab.Text = x.CABCLIENTE
        Txt_Ccbancario.Text = x.CCBANCARIOCLIENTE
        Txt_Cin.Text = x.CINCLIENTE
        Txt_NumCont.Text = x.NumeroControlloCliente
        Txt_BancaCliente.Text = x.BancaCliente

        Txt_IntestarioCC.Text = x.IntestatarioCC
        Txt_CodiceFiscaleCC.Text = x.CodiceFiscaleCC

        Dim DecSot As New Cls_Pianodeiconti

        DecSot.Mastro = x.MastroCliente
        DecSot.Conto = x.ContoCliente
        DecSot.Sottoconto = x.SottoContoCliente
        DecSot.Decodfica(Session("DC_GENERALE"))
        Txt_Sottoconto.Text = Trim(x.MastroCliente & " " & x.ContoCliente & " " & x.SottoContoCliente & " " & DecSot.Descrizione)

        Lbl_Metodo.Text = ""
        If Session("NomeEPersonam") = "MAGIERA" Then
            'Lbl_Metodo.Text = LeggiDatiIbanMetodo(x.CODICEFISCALE) & "<br/>"
        End If

        Dim Pr As New Cls_ImportoParente

        Pr.loaddati(ConnectionString, Session("CODICEOSPITE"), Session("CODICEPARENTE"), Session("CODICESERVIZIO"), MyTable)

        Session("Appoggio") = MyTable

        Grd_ImportoParenti.AutoGenerateColumns = False
        Grd_ImportoParenti.DataSource = MyTable

        Call EtichetteImporti()
        Grd_ImportoParenti.DataBind()

        Dim Kp As New Cls_LoginParente
        Dim StLog As New Cls_Login

        StLog.Utente = Session("UTENTE")
        StLog.LeggiSP(Application("SENIOR"))


        Kp.Societa = StLog.Cliente
        Kp.CodiceOspite = Session("CODICEOSPITE")
        Kp.CodiceParente = Session("CODICEPARENTE")
        Kp.LeggiDatiParente(Application("SENIOR"))

        Txt_Utente.Text = Kp.Utente
        TxT_Password.Text = Kp.Password
        TxT_ConfermaPassword.Text = Kp.Password
        Chk_Stanze.Checked = False
        Chk_Denaro.Checked = False
        Chk_Contabilita.Checked = False
        If Kp.Stanza = 1 Then
            Chk_Stanze.Checked = True
        End If
        If Kp.Denaro = 1 Then
            Chk_Denaro.Checked = True
        End If
        If Kp.Contabilita = 1 Then
            Chk_Contabilita.Checked = True
        End If
        If Kp.Diurno = 1 Then
            Chk_Diurno.Checked = True
        End If



        Dim KCs As New Cls_DatiOspiteParenteCentroServizio

        KCs.CentroServizio = Session("CODICESERVIZIO")
        KCs.CodiceOspite = Session("CODICEOSPITE")
        KCs.CodiceParente = Session("CODICEPARENTE")
        KCs.Leggi(Session("DC_OSPITE"))

        If KCs.CodiceOspite <> 0 Then
            DD_TipoOperazione.SelectedValue = KCs.TipoOperazione
            DD_IVA.SelectedValue = KCs.AliquotaIva
            DD_ModalitaPagamento.SelectedValue = KCs.ModalitaPagamento
            If KCs.Anticipata = "S" Then
                Chk_Anticipata.Checked = True
            Else
                Chk_Anticipata.Checked = False
            End If

            Chk_ExtraFuoriFattura.Visible = False
            If KCs.Compensazione = "S" Then
                RB_SI.Checked = True
            End If
            If KCs.Compensazione = "N" Then
                RB_NO.Checked = True
                Chk_ExtraFuoriFattura.Visible = True
            End If
            If KCs.Compensazione = "D" Then
                RB_Dettaglio.Checked = True
            End If
        End If


        If x.ExtraFuoriRetta = 1 Then
            Chk_ExtraFuoriFattura.Checked = True
        Else
            Chk_ExtraFuoriFattura.Checked = False
        End If

        VerificaCodiceFiscale(x)
      
    End Sub
    Private Sub VerificaCodiceFiscale(ByVal x As Cls_Parenti)
        Dim VerCf As New Cls_CodiceFiscale

        Dim Verifica As Boolean = False


        Try
            Verifica = VerCf.Check_CodiceFiscale(Txt_CodiceFiscale.Text)
        Catch ex As Exception

        End Try


        If Verifica = False Then
            Txt_CodiceFiscale.BackColor = Drawing.Color.Red
        Else
            VerCf.Cognome = Txt_CognomeNome.Text
            VerCf.Nome = Txt_Nome.Text

            If IsDate(Txt_DataNascita.Text) Then
                VerCf.DataNascita = Txt_DataNascita.Text
            Else
                VerCf.DataNascita = Now
            End If
            VerCf.Comune = x.ComuneDiNascita
            VerCf.Provincia = x.ProvinciaDiNascita

            If RB_Femminile.Checked = True Then
                VerCf.Sesso = "F"
            Else
                VerCf.Sesso = "M"
            End If


            Dim AppoggioCF As String

            VerCf.StringaConnessione = Session("DC_OSPITE")
            AppoggioCF = VerCf.Make_CodiceFiscale()


            Dim TestComune As New ClsComune

            TestComune.Provincia = x.ProvinciaDiNascita
            TestComune.Comune = x.ComuneDiNascita
            TestComune.Leggi(Session("DC_OSPITE"))


            If TestComune.CODXCODF <> "" Then
                If Mid(AppoggioCF.ToUpper & Space(16), 12, 4) <> Mid(Txt_CodiceFiscale.Text.ToUpper & Space(16), 12, 4) Then
                    Txt_CodiceFiscale.BackColor = Drawing.Color.Yellow
                End If
            End If

            If Mid(AppoggioCF.ToUpper & Space(16), 1, 6) <> Mid(Txt_CodiceFiscale.Text.ToUpper & Space(16), 1, 6) Then
                Txt_CodiceFiscale.BackColor = Drawing.Color.Yellow
            End If

            If IsDate(Txt_DataNascita.Text) Then
                If Mid(AppoggioCF.ToUpper & Space(16), 7, 5) <> Mid(Txt_CodiceFiscale.Text.ToUpper & Space(16), 7, 5) Then
                    Txt_CodiceFiscale.BackColor = Drawing.Color.Yellow
                End If
            End If
        End If
    End Sub
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Call EseguiJS()

        If Trim(Session("UTENTE")) = "" Then
            Response.Redirect("..\Login.aspx")
            Exit Sub
        End If

        If Trim(Session("NomeEPersonam")) = "" Then
            Btn_RecuperaEperesonam.Visible = False
        End If

        If Not IsNothing(Session("ABILITAZIONI")) Then
            If Session("ABILITAZIONI").IndexOf("<EPERSONAM>") >= 0 And Val(Session("CODICEPARENTE")) <> 0 Then
                Txt_Nome.Enabled = False
                Txt_CognomeNome.Enabled = False
                Txt_Telefono.Enabled = False
                Txt_Telefono3.Enabled = False
                Txt_Telefono4_Mod.Enabled = False
                Txt_Cap.Enabled = False
                Txt_ComRes.Enabled = False
                Txt_CodiceFiscale.Enabled = False
                Chk_ParenteIndirizzo.Enabled = False
                Chk_Intestatario.Enabled = False
            End If
            If Session("ABILITAZIONI").IndexOf("<EPERSONAM-AUT>") >= 0 Then
                Btn_RecuperaEperesonam.Visible = False
            End If
        End If

        If Page.IsPostBack = False Then            
            If Val(Session("GDPRAttivo")) = 0 Then
                RB_NOConsenso1.Enabled = False
                RB_NOConsenso2.Enabled = False
                RB_SIConsenso1.Enabled = False
                RB_SIConsenso2.Enabled = False

                DD_Report.Enabled = False
                IB_Download.Enabled = False
            Else
                DD_Report.Items.Add("Informativa Ospite Fornita da Parente")
                DD_Report.Items.Add("Informativa Parente Fornita da Ospite")
                DD_Report.Items.Add("Informativa Parente Forniti da Parente")
                DD_Report.Items.Add("Informativa Ospite Non Autosufficiente")


                Dim MGdpr As New Cls_Tabella_GDPR

                MGdpr.UpDropDownListInAggiunta(Session("DC_OSPITE"), DD_Report)
            End If

            If Val(Request.Item("CodiceOspite")) > 0 And Request.Item("CentroServizio") <> "" Then

                Session("CODICESERVIZIO") = Request.Item("CentroServizio")
                Session("CODICEOSPITE") = Val(Request.Item("CodiceOspite"))
                Session("CODICEPARENTE") = Val(Request.Item("CODICEPARENTE"))
            End If

            ViewState("CODICESERVIZIO") = Session("CODICESERVIZIO")
            ViewState("CODICEOSPITE") = Session("CODICEOSPITE")
            ViewState("CODICEPARENTE") = Session("CODICEPARENTE")

            Dim x As New ClsOspite

            x.CodiceOspite = Session("CODICEOSPITE")
            x.Leggi(Session("DC_OSPITE"), x.CodiceOspite)

            Dim cs As New Cls_CentroServizio

            cs.Leggi(Session("DC_OSPITE"), Session("CODICESERVIZIO"))

            Lbl_NomeOspite.Text = x.Nome & " -  " & x.DataNascita & " - " & cs.DESCRIZIONE & "<i style=""font-size:small;"">(" & x.CodiceOspite & ")</i>"


            Dim K1 As New Cls_SqlString

            Dim Barra As New Cls_BarraSenior

            If Not IsNothing(Session("RicercaAnagraficaSQLString")) Then
                K1 = Session("RicercaAnagraficaSQLString")
            End If

            Lbl_BarraSenior.Text = Barra.CodiceBarra(Application("SENIOR"), Session("UTENTE"), Page, K1)
            If Not IsNothing(Session("ABILITAZIONI")) Then
                If Session("ABILITAZIONI").IndexOf("<EPERSONAM>") >= 0 Then
                    ImageButton1.Visible = False
                End If
            End If


            Dim m As New Cls_Parametri

            m.LeggiParametri(Session("DC_OSPITE"))

            If m.SeparaPeriodoOspiteParente = 1 Then
                DD_TipoOperazione.Enabled = False
            End If

            Call AggiornaDati()
            Call EseguiJS()
        Else
            Call EseguiJS()
        End If


        



        If DD_ModalitaPagamento.SelectedValue = "" Then

            Dim MioPar As New Cls_Parametri

            MioPar.LeggiParametri(Session("DC_OSPITE"))


            DD_ModalitaPagamento.SelectedValue = MioPar.ModalitaPagamento
        End If
    End Sub

    Private Function LeggiDatiIbanMetodo(ByVal CodiceFiscale As String) As String

        Dim cnMetodo As OleDbConnection
        Dim DataBaseMetodo As String = ""
        Dim Codice As String = ""

        LeggiDatiIbanMetodo = ""

        If Session("NomeEPersonam") = "MAGIERA" Then
            DataBaseMetodo = "Provider=SQLOLEDB;Data Source=localhost\sqlexpress;Initial Catalog=ASPMAGANS;User Id=sa;Password=advenias2012;"
        End If



        cnMetodo = New Data.OleDb.OleDbConnection(DataBaseMetodo)

        cnMetodo.Open()

        Dim cmd As New OleDbCommand()

        cmd.CommandText = ("select * from ANAGRAFICACF  where CODFISCALE = ? order by DataModifica")
        cmd.Connection = cnMetodo
        cmd.Parameters.AddWithValue("@CODFISCALE", CodiceFiscale)

        Dim myPOSTreader As OleDbDataReader = cmd.ExecuteReader()
        If myPOSTreader.Read Then
            Codice = campodb(myPOSTreader.Item("CODCONTO"))
        End If
        myPOSTreader.Close()

        Dim cmd1 As New OleDbCommand()

        cmd1.CommandText = ("select * from BANCAAPPCF  where CODCONTO = ? order by DataModifica")
        cmd1.Connection = cnMetodo
        cmd1.Parameters.AddWithValue("@CODCONTO", Codice)

        Dim myPOSTreader1 As OleDbDataReader = cmd1.ExecuteReader()
        If myPOSTreader1.Read Then
            LeggiDatiIbanMetodo = campodb(myPOSTreader1.Item("BANCAAPPOGGIO"))
            LeggiDatiIbanMetodo = LeggiDatiIbanMetodo & " " & campodb(myPOSTreader1.Item("CODICEIBAN"))
        End If
        myPOSTreader1.Close()

        cnMetodo.Close()

    End Function
    Function campodb(ByVal oggetto As Object) As String
        If IsDBNull(oggetto) Then
            Return ""
        Else
            Return oggetto
        End If
    End Function


    Private Sub InserisciRiga()
        UpDateTable()
        MyTable = ViewState("App_Retta")
        Dim myriga As System.Data.DataRow = MyTable.NewRow()
        myriga(0) = ""
        myriga(1) = 0
        myriga(3) = 0
        myriga(4) = 0
        myriga(5) = 0
        myriga(6) = 0
        myriga(7) = 0



        MyTable.Rows.Add(myriga)

        ViewState("App_Retta") = MyTable
        Grd_ImportoParenti.AutoGenerateColumns = False

        Grd_ImportoParenti.DataSource = MyTable

        Call EtichetteImporti()

        Grd_ImportoParenti.DataBind()

        Call EseguiJS()


        Dim TxtData As TextBox = DirectCast(Grd_ImportoParenti.Rows(Grd_ImportoParenti.Rows.Count - 1).FindControl("TxtData"), TextBox)

        TxtData.Focus()

        ClientScript.RegisterClientScriptBlock(Me.GetType(), "setFocus", "$(document).ready(function() {  setTimeout(function(){ document.getElementById('" + TxtData.ClientID + "').focus(); }); }, 500);", True)


    End Sub

    Protected Sub Grd_ImportoParenti_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Grd_ImportoParenti.Load

    End Sub

    Protected Sub Grd_ImportoParenti_RowCommand(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewCommandEventArgs) Handles Grd_ImportoParenti.RowCommand
        If (e.CommandName = "Inserisci") Then
            Call InserisciRiga()
        End If
    End Sub





    Protected Sub Modifica()
        Dim x As New Cls_Parenti
        Dim d As New ClsComune
        Dim ConnectionString As String = Session("DC_OSPITE")

        If Val(Session("CODICEPARENTE")) > 0 Then
            x.Leggi(ConnectionString, Session("CODICEOSPITE"), Session("CODICEPARENTE"))
        End If

        If Val(Session("CODICEPARENTE")) > 0 Then
            Dim Log As New Cls_LogPrivacy

            Log.LogPrivacy(Application("SENIOR"), Session("CLIENTE"), Session("UTENTE"), Session("UTENTEIMPER"), Page.GetType.Name.ToUpper, Val(Session("CODICEOSPITE")), Val(Session("CODICEPARENTE")), "", "", 0, "", "M", "PARENTE", "")

        End If

        x.Nome = Txt_CognomeNome.Text & " " & Txt_Nome.Text

        x.CognomeParente = Txt_CognomeNome.Text
        x.NomeParente = Txt_Nome.Text

        If RB_Femminile.Checked = True Then
            x.Sesso = "F"
        End If
        If RB_Maschile.Checked = True Then
            x.Sesso = "M"
        End If

        x.DataNascita = IIf(Txt_DataNascita.Text = "", Nothing, Txt_DataNascita.Text)

        Dim Vettore(100) As String

        If Txt_Comune.Text <> "" Then
            Vettore = SplitWords(Txt_Comune.Text)
            If Vettore.Length > 1 Then
                x.ProvinciaDiNascita = Vettore(0)
                x.ComuneDiNascita = Vettore(1)
            End If
        End If

        x.CODICEFISCALE = Txt_CodiceFiscale.Text
        x.Telefono1 = Txt_Telefono.Text
        x.Telefono2 = Txt_Telefono2.Text
        x.Telefono3 = Txt_Telefono3.Text.Trim
        x.Telefono4 = Txt_Telefono4_Mod.Text
        x.ImportoSconto = Txt_ImportoSconto.Text
        x.TipoSconto = dd_TipoSconto.SelectedValue


        x.GradoParentela = DD_Parentela.SelectedValue

        x.CONTOPERESATTO = Txt_Esportazione.Text

        x.CodiceCup = Txt_CodiceCup.Text


        If Chk_Anticipata.Checked = True Then
            x.FattAnticipata = "S"
        Else
            x.FattAnticipata = "N"
        End If
        If Chk_RotturaOspite.Checked = True Then
            x.RotturaOspite = 1
        Else
            x.RotturaOspite = 0
        End If

        x.NOTE = Txt_Note.Content


        x.RESIDENZAINDIRIZZO1 = Txt_Indirizzo.Text
        x.RESIDENZACAP1 = Txt_Cap.Text

        x.Utente = Session("UTENTE")


        If Txt_ComRes.Text <> "" Then
            Vettore = SplitWords(Txt_ComRes.Text)
            If Vettore.Length > 1 Then
                x.RESIDENZAPROVINCIA1 = Vettore(0)
                x.RESIDENZACOMUNE1 = Vettore(1)
            End If
        End If


        If Chk_Intestatario.Checked = True Then
            x.Intestatario = 1
        Else
            x.Intestatario = 0
        End If

        If Chk_ParenteIndirizzo.Checked = True Then
            x.ParenteIndirizzo = 1

            Dim KO As New ClsOspite


            KO.Leggi(Session("DC_OSPITE"), Session("CODICEOSPITE"))
            KO.RecapitoIndirizzo = x.RESIDENZAINDIRIZZO1
            KO.RESIDENZACAP4 = x.RESIDENZACAP1
            KO.RecapitoProvincia = x.RESIDENZAPROVINCIA1
            KO.RecapitoComune = x.RESIDENZACOMUNE1
            KO.RecapitoNome = Txt_CognomeNome.Text & " " & Txt_Nome.Text
            KO.RESIDENZATELEFONO4 = Txt_Telefono.Text
            KO.TELEFONO4 = Txt_Telefono3.Text.Trim
            KO.ScriviOspite(Session("DC_OSPITE"))


        Else
            x.ParenteIndirizzo = 0

            Dim AppoggioParente As New Cls_Parenti

            AppoggioParente.CodiceOspite = Session("CODICEOSPITE")
            AppoggioParente.CodiceParente = Session("CODICEPARENTE")
            AppoggioParente.Leggi(Session("DC_OSPITE"), Session("CODICEOSPITE"), Session("CODICEPARENTE"))
            If AppoggioParente.ParenteIndirizzo = 1 Then


                Dim KO As New ClsOspite


                KO.Leggi(Session("DC_OSPITE"), Session("CODICEOSPITE"))
                KO.RecapitoIndirizzo = ""
                KO.RESIDENZACAP4 = ""
                KO.RecapitoProvincia = ""
                KO.RecapitoComune = ""
                KO.RecapitoNome = ""
                KO.RESIDENZATELEFONO4 = ""
                KO.ScriviOspite(Session("DC_OSPITE"))
            End If
        End If


        If RB_NOConsenso1.Checked = True Then
            x.CONSENSOINSERIMENTO = 2
        End If

        If RB_SIConsenso1.Checked = True Then
            x.CONSENSOINSERIMENTO = 1
        End If


        If RB_NOConsenso2.Checked = True Then
            x.CONSENSOMARKETING = 2
        End If

        If RB_SIConsenso2.Checked = True Then
            x.CONSENSOMARKETING = 1
        End If




        x.TIPOOPERAZIONE = DD_TipoOperazione.SelectedValue
        x.MODALITAPAGAMENTO = DD_ModalitaPagamento.SelectedValue
        x.CODICEIVA = DD_IVA.SelectedValue

        If RB_Mensile.Checked = True Then
            x.PERIODO = "M"
        End If

        If RB_Bimestrale.Checked = True Then
            x.PERIODO = "B"
        End If
        If RB_Trimestrale.Checked = True Then
            x.PERIODO = "T"
        End If

        If Chk_ExtraFuoriFattura.Checked = True Then
            x.ExtraFuoriRetta = 1
        Else
            x.ExtraFuoriRetta = 0
        End If


        If RB_SI.Checked = True Then
            x.Compensazione = "S"
        End If
        If RB_NO.Checked = True Then
            x.Compensazione = "N"
        End If

        If chk_Opposizione730.Checked = True Then
            x.Opposizione730 = 1
        Else
            x.Opposizione730 = 0
        End If


        If Txt_Sottoconto.Text <> "" Then
            Vettore = SplitWords(Txt_Sottoconto.Text)
            If Vettore.Length > 1 Then
                x.MastroCliente = Vettore(0)
                x.ContoCliente = Vettore(1)
                x.SottoContoCliente = Vettore(2)
            End If
        End If

        x.RECAPITONOME = Txt_NomeRecapito.Text
        x.RECAPITOINDIRIZZO = Txt_IndirizzoRecapito.Text

        If Txt_ComuneRecapito.Text <> "" Then
            Dim XVettore(100) As String

            XVettore = SplitWords(Txt_ComuneRecapito.Text)
            If XVettore.Length > 1 Then
                x.RECAPITOPROVINCIA = XVettore(0)
                x.RECAPITOCOMUNE = XVettore(1)
            End If
        End If

        x.RESIDENZACAP4 = Txt_Cap4.Text

        x.IntCliente = Txt_Int.Text
        x.ABICLIENTE = Txt_Abi.Text
        x.CABCLIENTE = Txt_Cab.Text
        x.CCBANCARIOCLIENTE = Txt_Ccbancario.Text
        x.CINCLIENTE = Txt_Cin.Text
        x.NumeroControlloCliente = Txt_NumCont.Text

        x.BancaCliente = Txt_BancaCliente.Text

        x.IntestatarioCC = Txt_IntestarioCC.Text
        x.CodiceFiscaleCC = Txt_CodiceFiscaleCC.Text
        x.Utente = Session("UTENTE")

        x.CodiceParente = Session("CODICEPARENTE")
        x.CodiceOspite = Session("CODICEOSPITE")
        x.ScriviParente(ConnectionString)
        If Val(Session("CODICEPARENTE")) = 0 Then
            Dim PartX As New Cls_Pianodeiconti

            Dim CentroServizioP As New Cls_CentroServizio

            CentroServizioP.Leggi(ConnectionString, Session("CODICESERVIZIO"))

            PartX.Mastro = CentroServizioP.MASTRO
            PartX.Conto = CentroServizioP.CONTO
            PartX.Sottoconto = (x.CodiceOspite * 100) + x.CodiceParente
            PartX.Descrizione = x.Nome
            PartX.TipoAnagrafica = "P"
            PartX.Scrivi(Session("DC_GENERALE"))

            Dim ParXpAR As New Cls_Parametri

            ParXpAR.LeggiParametri(Session("DC_OSPITE"))

            If ParXpAR.MastroAnticipo > 0 Then
                Dim PartX1 As New Cls_Pianodeiconti

                Dim CentroServiziop1 As New Cls_CentroServizio

                CentroServiziop1.Leggi(ConnectionString, Session("CODICESERVIZIO"))

                PartX1.Mastro = ParXpAR.MastroAnticipo
                PartX1.Conto = CentroServiziop1.CONTO
                PartX1.Sottoconto = (x.CodiceOspite * 100) + x.CodiceParente
                PartX1.Descrizione = x.Nome
                PartX1.TipoAnagrafica = "P"
                PartX1.Scrivi(Session("DC_GENERALE"))
            End If
        End If


        Dim KCs As New Cls_DatiOspiteParenteCentroServizio

        KCs.CentroServizio = Session("CODICESERVIZIO")
        KCs.CodiceOspite = Session("CODICEOSPITE")
        KCs.CodiceParente = x.CodiceParente
        KCs.Leggi(Session("DC_OSPITE"))

        KCs.CentroServizio = Session("CODICESERVIZIO")
        KCs.CodiceOspite = Session("CODICEOSPITE")
        KCs.CodiceParente = x.CodiceParente
        KCs.TipoOperazione = DD_TipoOperazione.SelectedValue

        KCs.ModalitaPagamento = DD_ModalitaPagamento.SelectedValue

        KCs.AliquotaIva = DD_IVA.SelectedValue
        If RB_SI.Checked = True Then
            KCs.Compensazione = "S"
        End If
        If RB_NO.Checked = True Then
            KCs.Compensazione = "N"
        End If
        If RB_Dettaglio.Checked = True Then
            KCs.Compensazione = "D"
        End If

        If Chk_Anticipata.Checked = True Then
            KCs.Anticipata = "S"
        Else
            KCs.Anticipata = ""
        End If


        KCs.Scrivi(Session("DC_OSPITE"))

        If Val(Session("CODICEPARENTE")) = 0 Then

            Dim Log As New Cls_LogPrivacy

            Log.LogPrivacy(Application("SENIOR"), Session("CLIENTE"), Session("UTENTE"), Session("UTENTEIMPER"), Page.GetType.Name.ToUpper, x.CodiceOspite, x.CodiceParente, "", "", 0, "", "I", "PARENTE", "")

        End If

        Session("CODICEPARENTE") = x.CodiceParente
        ViewState("CODICEPARENTE") = x.CodiceParente


        REM Lbl_Errori.Text = "Modifica Effettuata"



    End Sub

    Protected Sub Grd_ImportoParenti_RowDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs) Handles Grd_ImportoParenti.RowDataBound
        If e.Row.RowType = DataControlRowType.DataRow Then


            Dim TxtData As TextBox = DirectCast(e.Row.FindControl("TxtData"), TextBox)

            TxtData.Text = MyTable.Rows(e.Row.RowIndex).Item(0).ToString



            Dim TxtImporto As TextBox = DirectCast(e.Row.FindControl("TxtImporto"), TextBox)

            TxtImporto.Text = MyTable.Rows(e.Row.RowIndex).Item(1).ToString


            Dim DD_Tipo As DropDownList = DirectCast(e.Row.FindControl("DD_Tipo"), DropDownList)

            DD_Tipo.SelectedValue = MyTable.Rows(e.Row.RowIndex).Item(2).ToString


            Dim TxtPercentuale As TextBox = DirectCast(e.Row.FindControl("TxtPercentuale"), TextBox)

            TxtPercentuale.Text = MyTable.Rows(e.Row.RowIndex).Item(3).ToString


            Dim TxtImporto_2 As TextBox = DirectCast(e.Row.FindControl("TxtImporto_2"), TextBox)

            TxtImporto_2.Text = MyTable.Rows(e.Row.RowIndex).Item(4).ToString

            Dim TxtImporto1 As TextBox = DirectCast(e.Row.FindControl("TxtImporto1"), TextBox)

            TxtImporto1.Text = MyTable.Rows(e.Row.RowIndex).Item(5).ToString


            Dim TxtImporto2 As TextBox = DirectCast(e.Row.FindControl("TxtImporto2"), TextBox)

            TxtImporto2.Text = MyTable.Rows(e.Row.RowIndex).Item(6).ToString


            Dim TxtImporto3 As TextBox = DirectCast(e.Row.FindControl("TxtImporto3"), TextBox)

            TxtImporto3.Text = MyTable.Rows(e.Row.RowIndex).Item(7).ToString


            Dim TxtImporto4 As TextBox = DirectCast(e.Row.FindControl("TxtImporto4"), TextBox)

            TxtImporto4.Text = MyTable.Rows(e.Row.RowIndex).Item(8).ToString



            Dim DdTipoOperazione As DropDownList = DirectCast(e.Row.FindControl("DD_TipoOperazione"), DropDownList)

            Dim m As New Cls_Parametri

            m.LeggiParametri(Session("DC_OSPITE"))

            If m.SeparaPeriodoOspiteParente = 1 Then
                Dim ml As New Cls_TipoOperazione

                'ml.UpDateDropBox(Session("DC_OSPITE"), DdTipoOperazione)

                ml.UpDateDropBoxCentroServizio(Session("DC_OSPITE"), DdTipoOperazione, Session("CODICESERVIZIO"), "O")


                DdTipoOperazione.SelectedValue = MyTable.Rows(e.Row.RowIndex).Item(10).ToString
            End If


            Call EseguiJS()
        End If

    End Sub

    Protected Sub Grd_ImportoParenti_RowDeleting(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewDeleteEventArgs) Handles Grd_ImportoParenti.RowDeleting
        UpDateTable()
        MyTable = ViewState("App_Retta")
        Try
            If Request.UserAgent.Contains("Chrome") Then
                If IsDBNull(Session("RI_TIMER")) Or DateDiff("s", Session("RI_TIMER"), Now) > 1 Then
                    MyTable.Rows.RemoveAt(e.RowIndex)
                    If MyTable.Rows.Count = 0 Then
                        Dim myriga As System.Data.DataRow = MyTable.NewRow()
                        myriga(1) = 0
                        myriga(2) = 0
                        MyTable.Rows.Add(myriga)
                    End If
                    Session("RI_TIMER") = Now
                End If
            Else
                MyTable.Rows.RemoveAt(e.RowIndex)
                If MyTable.Rows.Count = 0 Then
                    Dim myriga As System.Data.DataRow = MyTable.NewRow()
                    myriga(1) = 0
                    myriga(2) = 0
                    MyTable.Rows.Add(myriga)
                End If
            End If
        Catch ex As Exception

        End Try


        ViewState("App_Retta") = MyTable

        Grd_ImportoParenti.AutoGenerateColumns = False

        Grd_ImportoParenti.DataSource = MyTable

        Grd_ImportoParenti.DataBind()
        Call EseguiJS()

        e.Cancel = True
    End Sub



    Private Function CercaData(ByVal Data As String) As Boolean
        Dim MyTable As New System.Data.DataTable("tabella")
        Dim i As Long
        MyTable = Session("Appoggio")
        CercaData = False
        For i = 0 To MyTable.Rows.Count - 1
            If MyTable.Rows(i).Item(0).ToString = Data Then
                CercaData = True
                Exit Function
            End If
        Next
    End Function




    Private Sub LoginPersonam()
        'Dim request As WebRequest
        '-staging
        'request = HttpWebRequest.Create("https://api-v0.e-personam.com/v0/oauth/token")
        Dim request As New NameValueCollection

        Dim BlowFish As New Blowfish("advenias2014")



        'request.Method = "POST"
        request.Add("client_id", "98217ca6670c660e22f42d58e231d4e56c84fb34e216b0f66f1f30284fd3ec9d")
        request.Add("client_secret", "5570a684f69ca74d75d16bba669c156ec092eccb4111d0974adec3edb2fa4d6f")
        request.Add("grant_type", "authorization_code")
        request.Add("username", Session("UTENTE"))
        'request.Add("password", "advenias2014")

        'guarda = BlowFish.encryptString("advenias2014")
        Dim guarda As String

        guarda = Session("ChiaveCr") '"$2a$10$0jD7O/J51.5cJI7eO5joLOEr9YLdm715UE/YKRqE8skrMTZFL5r6u"

        request.Add("code", guarda)
        System.Net.ServicePointManager.ServerCertificateValidationCallback = AddressOf CertificateHandler


        Dim client As New WebClient()

        Dim result As Object = client.UploadValues("https://api-v0.e-personam.com/v0/oauth/token", "POST", request)


        Dim stringa As String = Encoding.Default.GetString(result)


        Dim serializer As JavaScriptSerializer


        serializer = New JavaScriptSerializer()


        Dim s As Autentificazione = serializer.Deserialize(Of Autentificazione)(stringa)


        Session("access_token") = s.access_token


    End Sub
    Public Class Autentificazione
        Public access_token As String
        Public expires_in As String
        Public scope As String
        Public token_type As String
        Public refresh_token As String
    End Class




    Private Function ModificaPerente() As Boolean

        ModificaPerente = True
        Try

            LoginPersonam()

            Dim Data() As Byte

            Dim request As New NameValueCollection

            Dim JSon As String

            JSon = "{""surname"":""" & Txt_CognomeNome.Text & ""","
            JSon = JSon & """firstname"":""" & Txt_Nome.Text & ""","
            JSon = JSon & """phone"":""" & Txt_Telefono.Text & ""","
            JSon = JSon & """cf"":""" & Txt_CodiceFiscale.Text & ""","
            JSon = JSon & """resaddress"":""" & Txt_Indirizzo.Text & """}"


            Data = System.Text.Encoding.ASCII.GetBytes(JSon)


            Dim M As New ClsOspite

            M.CodiceOspite = Session("CODICEOSPITE")
            M.Leggi(Session("DC_OSPITE"), M.CodiceOspite)


            Dim P As New Cls_Parenti

            P.CodiceOspite = Session("CODICEOSPITE")
            P.CodiceParente = Session("CODICEPARENTE")
            P.Leggi(Session("DC_OSPITE"), M.CodiceOspite, P.CodiceParente)


            System.Net.ServicePointManager.ServerCertificateValidationCallback = AddressOf CertificateHandler


            Dim client As HttpWebRequest = WebRequest.Create("https://api-v0.e-personam.com/v0/guests/" & M.CODICEFISCALE & "/relatives/" & P.CODICEFISCALE)

            client.Method = "PUT"
            client.Headers.Add("Authorization", "Bearer " & Session("access_token"))
            client.ContentType = "Content-Type: application/json"

            client.ContentLength = Data.Length

            Dim dataStream As Stream = client.GetRequestStream()


            dataStream.Write(Data, 0, Data.Length)
            dataStream.Close()

            Dim response As HttpWebResponse = client.GetResponse()

            Dim respStream As Stream = response.GetResponseStream()
            Dim reader As StreamReader = New StreamReader(respStream)
            Dim appoggio As String
            appoggio = reader.ReadToEnd()


        Catch ex As Exception
            ModificaPerente = False
        End Try

    End Function


    
 

    Protected Sub Btn_Modifica_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs)
        Session("CODICESERVIZIO") = ViewState("CODICESERVIZIO")
        Session("CODICEOSPITE") = ViewState("CODICEOSPITE")
        Session("CODICEPARENTE") = ViewState("CODICEPARENTE")
        If Val(Session("CODICEOSPITE")) = 0 Then
            ClientScript.RegisterClientScriptBlock(Me.GetType(), "Errore", "VisualizzaErrore('Devi ricercare l ospite');", True)
            Exit Sub
        End If



        If Txt_CognomeNome.Text.Trim = "" Then
            ClientScript.RegisterClientScriptBlock(Me.GetType(), "Errore", "VisualizzaErrore('Specificare cognome nome');", True)
            Exit Sub
        End If
        If Txt_DataNascita.Text <> "" Then
            If Not IsDate(Txt_DataNascita.Text) Then
                ClientScript.RegisterClientScriptBlock(Me.GetType(), "Errore", "VisualizzaErrore('Data nascita formalmente errata');", True)
                Exit Sub
            End If
        End If


        Dim Param As New Cls_Parametri


        Param.LeggiParametri(Session("DC_OSPITE"))

        If Param.ModalitaPagamentoObligatoria = 1 Then
            If DD_ModalitaPagamento.SelectedValue = "" And DD_TipoOperazione.SelectedValue <> "" Then
                ClientScript.RegisterClientScriptBlock(Me.GetType(), "Errore", "VisualizzaErrore('Modalità pagamento obbligatoria,se indicato tipo operazione');", True)
                Call EseguiJS()
                Exit Sub
            End If
        End If


        If Val(Session("GDPRAttivo")) = 1 Then
            If RB_SIConsenso1.Checked = False Then
                ClientScript.RegisterClientScriptBlock(Me.GetType(), "Errore", "VisualizzaErrore('Devi indicare il consenso all inserimento dei dati in Senior');", True)
                Exit Sub
            End If
        End If


        If DD_ModalitaPagamento.SelectedValue = "" Then
            Dim MioPar As New Cls_Parametri

            MioPar.LeggiParametri(Session("DC_OSPITE"))

            DD_ModalitaPagamento.SelectedValue = MioPar.ModalitaPagamento
        End If

        If Txt_CodiceFiscale.Text = "" Then
            Dim FCodFis As New ClsCodiceFiscale


            If Not FCodFis.ControlloIntegritaCodiceFiscale(Txt_CodiceFiscale.Text) Then
                Dim NonOk As Boolean = True
                If Txt_CodiceFiscale.Text.Length = 11 Then
                    NonOk = False
                    For K = 1 To 11
                        If Mid(Txt_CodiceFiscale.Text, K, 1) < "0" Or Mid(Txt_CodiceFiscale.Text, K, 1) > "9" Then
                            NonOk = True
                        End If
                    Next K
                End If
                If NonOk = True Then
                    ClientScript.RegisterClientScriptBlock(Me.GetType(), "Errore", "VisualizzaErrore('Codice fiscale formalmente errato');", True)
                    Exit Sub
                End If
            End If
        End If

        If Not IsNothing(Session("ABILITAZIONI")) Then
            If Session("ABILITAZIONI").IndexOf("<EPERSONAM>") >= 0 Then
                'If Not ModificaPerente() Then
                '    ClientScript.RegisterClientScriptBlock(Me.GetType(), "Errore", "VisualizzaErrore('Errore nel dialogo con Epersonam');", True)
                '    Exit Sub
                'End If
            End If
        End If


        Dim i As Integer
        Dim T As Integer

        For i = 0 To Grd_ImportoParenti.Rows.Count - 1
            Dim TxtData As TextBox = DirectCast(Grd_ImportoParenti.Rows(i).FindControl("TxtData"), TextBox)
            Dim TxtImporto As TextBox = DirectCast(Grd_ImportoParenti.Rows(i).FindControl("TxtImporto"), TextBox)
            Dim DD_Tipo As DropDownList = DirectCast(Grd_ImportoParenti.Rows(i).FindControl("DD_Tipo"), DropDownList)


            If Not IsDate(TxtData.Text) And TxtData.Text <> "" Then
                ClientScript.RegisterClientScriptBlock(Me.GetType(), "Errore", "VisualizzaErrore('Data errata per riga " & i + 1 & "');", True)
                Call EseguiJS()
                Exit Sub
            End If

            For T = 0 To Grd_ImportoParenti.Rows.Count - 1
                If T <> i Then
                    Dim TxtDatax As TextBox = DirectCast(Grd_ImportoParenti.Rows(T).FindControl("TxtData"), TextBox)

                    If TxtDatax.Text = TxtData.Text Then
                        ClientScript.RegisterClientScriptBlock(Me.GetType(), "Errore", "VisualizzaErrore('Data duplicata per riga " & i + 1 & " e riga " & T + 1 & "');", True)
                        Call EseguiJS()
                        Exit Sub
                    End If
                End If
            Next
        Next


        Dim Kl As New Cls_Parenti

        Kl.CodiceOspite = Session("CODICEOSPITE")
        Kl.CodiceParente = Session("CODICEPARENTE")

        If Param.ParenteIntestatarioUnico = 1 Then
            If Chk_ParenteIndirizzo.Checked = True Then
                If Kl.AltroDestinatario(Session("DC_OSPITE")) = True Then
                    ClientScript.RegisterClientScriptBlock(Me.GetType(), "Errore", "VisualizzaErrore('Altro parente già indicato come indirizzo fattura ospite');", True)
                    Call EseguiJS()
                    Exit Sub
                End If
            End If
        End If


        Call UpDateTable()
        Dim VerCom As New ClsComune

        'If Txt_Provincia.Text <> "" Then
        '    VerCom.Descrizione = ""
        '    VerCom.Provincia = Txt_Provincia.Text
        '    VerCom.Comune = Txt_Comune.Text
        '    VerCom.DecodficaComune(Session("DC_OSPITE"))
        '    If VerCom.Descrizione = "" Then
        '        ClientScript.RegisterClientScriptBlock(Me.GetType(), "Errore", "VisualizzaErrore('Comune nascita formalmente errato');", True)
        '        Exit Sub
        '    End If
        'End If

        'If Txt_ProvRes.Text <> "" Then
        '    VerCom.Descrizione = ""
        '    VerCom.Provincia = Txt_ProvRes.Text
        '    VerCom.Comune = Txt_ComRes.Text
        '    VerCom.DecodficaComune(Session("DC_OSPITE"))
        '    If VerCom.Descrizione = "" Then
        '        ClientScript.RegisterClientScriptBlock(Me.GetType(), "Errore", "VisualizzaErrore('Comune residenza formalmente errato');", True)
        '        Exit Sub
        '    End If
        'End If

        Dim xPiano As New Cls_Pianodeiconti

        'If Val(Txt_Mastro.Text) > 0 Then
        '    xPiano.Descrizione = ""
        '    xPiano.Mastro = Txt_Mastro.Text
        '    xPiano.Conto = Txt_Conto.Text
        '    xPiano.Sottoconto = Txt_Sottoconto.Text
        '    xPiano.Decodfica(Session("DC_GENERALE"))
        '    If xPiano.Descrizione = "" Then
        '        ClientScript.RegisterClientScriptBlock(Me.GetType(), "Errore", "VisualizzaErrore('Conto inesistente');", True)
        '        Exit Sub
        '    End If
        'End If

        Call Modifica()

        MyTable = ViewState("App_Retta")

        If Val(Session("CODICEPARENTE")) > 0 Then
            Dim Log As New Cls_LogPrivacy
            Dim serializer As JavaScriptSerializer
            serializer = New JavaScriptSerializer()

            Dim ConvT As New Cls_DataTableToJson
            Dim OldTable As New System.Data.DataTable("tabellaOld")


            Dim OldDatiPar As New Cls_ImportoParente

            OldDatiPar.loaddati(Session("DC_OSPITE"), Session("CODICEOSPITE"), Val(Session("CODICEPARENTE")), Session("CODICESERVIZIO"), OldTable)
            Dim Appoggio As String = ConvT.DataTableToJsonObj(OldTable)

            Log.LogPrivacy(Application("SENIOR"), Session("CLIENTE"), Session("UTENTE"), Session("UTENTEIMPER"), Page.GetType.Name.ToUpper, Session("CODICEOSPITE"), Val(Session("CODICEPARENTE")), "", "", 0, "", "M", "RETTEPARENTE", Appoggio)
        End If


        Dim X1 As New Cls_ImportoParente

        X1.CODICEOSPITE = Session("CODICEOSPITE")
        X1.CENTROSERVIZIO = Session("CODICESERVIZIO")
        X1.CODICEPARENTE = Session("CODICEPARENTE")
        X1.Utente = Session("UTENTE")
        X1.DataAggiornamento = Now
        X1.AggiornaDaTabella(Session("DC_OSPITE"), MyTable)

        Dim MyJs As String
        MyJs = "alert('Modifica Effettuata');"
        ScriptManager.RegisterClientScriptBlock(Me, Me.GetType(), "ModScM", MyJs, True)

        Txt_CognomeNome.BackColor = Drawing.Color.White
        Txt_Nome.BackColor = Drawing.Color.White
        Txt_Telefono.BackColor = Drawing.Color.White
        Txt_Telefono2.BackColor = Drawing.Color.White
        Txt_Telefono3.BackColor = Drawing.Color.White
        Txt_Telefono4_Mod.BackColor = Drawing.Color.White
        Txt_Indirizzo.BackColor = Drawing.Color.White
        Txt_Cap.BackColor = Drawing.Color.White
        Txt_ComRes.BackColor = Drawing.Color.White


        Response.Redirect("SelezionaParente.aspx")
    End Sub


    Private Function SplitWords(ByVal s As String) As String()
        '
        ' Call Regex.Split function from the imported namespace.
        ' Return the result array.
        '
        Return Regex.Split(s, "\W+")
    End Function

    Private Sub EseguiJS()
        Dim MyJs As String
        MyJs = "$(document).ready(function() {"

        MyJs = MyJs & "var els = document.getElementsByTagName(""*"");"

        MyJs = MyJs & "for (var i=0;i<els.length;i++)"
        MyJs = MyJs & "if ( els[i].id ) { "
        MyJs = MyJs & " var appoggio =els[i].id; "

        MyJs = MyJs & " if ((appoggio.match('Txt_Comune')!= null) || (appoggio.match('Txt_ComRes')!= null))  {  "
        MyJs = MyJs & " $(els[i]).autocomplete('/WebHandler/AutocompleteComune.ashx?Utente=" & Session("UTENTE") & "', {delay:5,minChars:3});"
        MyJs = MyJs & "    }"

        MyJs = MyJs & " if ((appoggio.match('TxtData')!= null)) {  "
        MyJs = MyJs & " $(els[i]).mask(""99/99/9999"");"
        MyJs = MyJs & " $(els[i]).datepicker({ changeMonth: true,changeYear: true,  yearRange: ""1990:" & Year(Now) + 5 & """},$.datepicker.regional[""it""]);"
        MyJs = MyJs & "    }"
        MyJs = MyJs & " if ((appoggio.match('Txt_Data')!= null)) {  "
        MyJs = MyJs & " $(els[i]).mask(""99/99/9999"");"
        MyJs = MyJs & " $(els[i]).datepicker({ changeMonth: true,changeYear: true,  yearRange: ""1890:" & Year(Now) & """},$.datepicker.regional[""it""]);"
        MyJs = MyJs & "    }"
        MyJs = MyJs & " if ((appoggio.match('TxtImporto')!= null) || (appoggio.match('TxtPercentuale')!= null) ) {  "
        MyJs = MyJs & " $(els[i]).keypress(function() { ForceNumericInput($(this).val(), true, true); } ); "
        MyJs = MyJs & " $(els[i]).blur(function() { var ap = formatNumber($(this).val(),2); $(this).val(ap); });"
        MyJs = MyJs & "    }"

        MyJs = MyJs & " if (appoggio.match('Sottoconto')!= null) {   "
        MyJs = MyJs & " $(els[i]).autocomplete('/WebHandler/GestioneConto.ashx?Utente=" & Session("UTENTE") & "', {delay:5,minChars:3});"
        MyJs = MyJs & "    }"



        MyJs = MyJs & "} "
        MyJs = MyJs & "});"

        'MyJs = "$(document).ready(function() { $('.myClass').keypress(function() { handleEnter($('.myClass').val(), true, true); } );     $('#" & Txt_ImportoPagato.ClientID & "').blur(function() { var ap = formatNumber ($('#" & Txt_ImportoPagato.ClientID & "').val(),2); $('#" & Txt_ImportoPagato.ClientID & "').val(ap); }); });"
        ScriptManager.RegisterStartupScript(Me, Me.GetType(), "visualizzaRitIm", MyJs, True)
    End Sub

    Private Sub UpDateTable()

        MyTable.Clear()
        MyTable.Columns.Clear()
        MyTable.Columns.Add("Data", GetType(String))
        MyTable.Columns.Add("Importo", GetType(String))
        MyTable.Columns.Add("Tipo", GetType(String))
        MyTable.Columns.Add("Percentuale", GetType(String))
        MyTable.Columns.Add("Importo_2", GetType(String))
        MyTable.Columns.Add("Importo1", GetType(String))
        MyTable.Columns.Add("Importo2", GetType(String))
        MyTable.Columns.Add("Importo3", GetType(String))
        MyTable.Columns.Add("Importo4", GetType(String))
        MyTable.Columns.Add("MensileFisso", GetType(String))
        MyTable.Columns.Add("TIPOOPERAZIONE", GetType(String))

        For i = 0 To Grd_ImportoParenti.Rows.Count - 1

            Dim TxtData As TextBox = DirectCast(Grd_ImportoParenti.Rows(i).FindControl("TxtData"), TextBox)
            Dim TxtImporto As TextBox = DirectCast(Grd_ImportoParenti.Rows(i).FindControl("TxtImporto"), TextBox)
            Dim DD_Tipo As DropDownList = DirectCast(Grd_ImportoParenti.Rows(i).FindControl("DD_Tipo"), DropDownList)
            Dim TxtPercentuale As TextBox = DirectCast(Grd_ImportoParenti.Rows(i).FindControl("TxtPercentuale"), TextBox)

            Dim TxtImporto_2 As TextBox = DirectCast(Grd_ImportoParenti.Rows(i).FindControl("TxtImporto_2"), TextBox)
            Dim TxtImporto1 As TextBox = DirectCast(Grd_ImportoParenti.Rows(i).FindControl("TxtImporto1"), TextBox)
            Dim TxtImporto2 As TextBox = DirectCast(Grd_ImportoParenti.Rows(i).FindControl("TxtImporto2"), TextBox)
            Dim TxtImporto3 As TextBox = DirectCast(Grd_ImportoParenti.Rows(i).FindControl("TxtImporto3"), TextBox)
            Dim TxtImporto4 As TextBox = DirectCast(Grd_ImportoParenti.Rows(i).FindControl("TxtImporto4"), TextBox)

            Dim DDTipooperazione As DropDownList = DirectCast(Grd_ImportoParenti.Rows(i).FindControl("DD_TipoOperazione"), DropDownList)


            Dim myrigaR As System.Data.DataRow = MyTable.NewRow()

            myrigaR(0) = TxtData.Text
            myrigaR(1) = TxtImporto.Text
            myrigaR(2) = DD_Tipo.SelectedValue
            myrigaR(3) = TxtPercentuale.Text

            myrigaR(4) = IIf(TxtImporto_2.Text = "", 0, TxtImporto_2.Text)
            myrigaR(5) = IIf(TxtImporto1.Text = "", 0, TxtImporto1.Text)
            myrigaR(6) = IIf(TxtImporto2.Text = "", 0, TxtImporto2.Text)
            myrigaR(7) = IIf(TxtImporto3.Text = "", 0, TxtImporto3.Text)
            myrigaR(8) = IIf(TxtImporto4.Text = "", 0, TxtImporto4.Text)

            myrigaR(10) = DDTipooperazione.SelectedValue


            MyTable.Rows.Add(myrigaR)

        Next

        ViewState("App_Retta") = MyTable
    End Sub

    Protected Sub Btn_Esci_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles Btn_Esci.Click
        If Val(Request.Item("CodiceOspite")) > 0 And Request.Item("CentroServizio") <> "" Then

            Session("CODICESERVIZIO") = Request.Item("CentroServizio")
            Session("CODICEOSPITE") = Val(Request.Item("CodiceOspite"))
            Session("CODICEPARENTE") = Val(Request.Item("CodiceParente"))
        End If


        Response.Redirect("SelezionaParente.aspx?CodiceOspite=" & Session("CODICEOSPITE") & "&CentroServizio=" & Session("CODICESERVIZIO") & "&CodiceParente=" & Session("CODICEPARENTE"))
    End Sub

    Protected Sub DD_TipoOperazione_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles DD_TipoOperazione.SelectedIndexChanged
        'If Grd_ImportoParenti.Rows.Count = 0 Then
        '    MyTable.Clear()
        '    MyTable.Columns.Clear()
        '    MyTable.Columns.Add("Data", GetType(String))
        '    MyTable.Columns.Add("Importo", GetType(String))
        '    MyTable.Columns.Add("Tipo", GetType(String))
        '    MyTable.Columns.Add("Percentuale", GetType(String))
        '    MyTable.Columns.Add("Importo1", GetType(String))
        '    MyTable.Columns.Add("Importo2", GetType(String))
        '    MyTable.Columns.Add("Importo3", GetType(String))
        '    MyTable.Columns.Add("Importo4", GetType(String))

        '    Dim myrigaR As System.Data.DataRow = MyTable.NewRow()
        '    myrigaR(0) = Format(Now, "dd/MM/yyyy")
        '    myrigaR(1) = 0
        '    myrigaR(2) = "G"
        '    myrigaR(3) = 0
        '    myrigaR(4) = 0
        '    myrigaR(5) = 0
        '    myrigaR(6) = 0            

        '    MyTable.Rows.Add(myrigaR)
        '    ViewState("App_Retta") = MyTable
        '    Grd_ImportoParenti.AutoGenerateColumns = False

        '    Grd_ImportoParenti.DataSource = MyTable
        '    Call EtichetteImporti()
        '    Grd_ImportoParenti.DataBind()

        '    Call EseguiJS()
        'End If
    End Sub

    Protected Sub Button1_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles Button1.Click
        Dim k As New Cls_CodiceFiscale
        Dim Nome As String
        Dim Cognome As String
        Dim FineCognome As Integer
        Dim Appoggio As String

        If Txt_CognomeNome.Text = "" Then
            Appoggio = "Necessari il cognome e nome per creare il codice fiscale"
            ClientScript.RegisterClientScriptBlock(Me.GetType(), "Errore", "VisualizzaErrore('" & Appoggio & "');", True)
            Exit Sub
        End If

        If Not IsDate(Txt_DataNascita.Text) Then
            Appoggio = "Necessaria data nascita per creare il codice fiscale"
            ClientScript.RegisterClientScriptBlock(Me.GetType(), "Errore", "VisualizzaErrore('" & Appoggio & "');", True)
            Exit Sub
        End If

        If Txt_Comune.Text = "" Then
            Appoggio = "Necessario comune nascita per creare il codice fiscale"
            ClientScript.RegisterClientScriptBlock(Me.GetType(), "Errore", "VisualizzaErrore('" & Appoggio & "');", True)
            Exit Sub
        End If

        If RB_Maschile.Checked = False And RB_Femminile.Checked = False Then
            Appoggio = "Specificare il sesso per creare il codice fiscale"
            ClientScript.RegisterClientScriptBlock(Me.GetType(), "Errore", "VisualizzaErrore('" & Appoggio & "');", True)
            Exit Sub
        End If



        Cognome = Txt_CognomeNome.Text
        Nome = Txt_Nome.Text

        Dim VettoreNS(100) As String

        VettoreNS = SplitWords(Txt_Comune.Text)
        If VettoreNS.Length > 2 Then
            k.Comune = VettoreNS(1)
            k.Provincia = VettoreNS(0)
        End If

        k.Nome = Nome
        k.Cognome = Cognome
        k.DataNascita = Txt_DataNascita.Text
        If RB_Maschile.Checked = True Then
            k.Sesso = "M"
        Else
            k.Sesso = "F"
        End If
        k.StringaConnessione = Session("DC_OSPITE")
        Txt_CodiceFiscale.Text = k.Make_CodiceFiscale()

        Call EseguiJS()
        Dim x As New Cls_Parenti


        x.NomeParente = Nome
        x.CognomeParente = Cognome
        x.ProvinciaDiNascita = k.Provincia
        x.ComuneDiNascita = k.Comune
        x.DataNascita = k.DataNascita
        x.Sesso = k.Sesso
        x.CODICEFISCALE = Txt_CodiceFiscale.Text

        Txt_CodiceFiscale.BackColor = Drawing.Color.White
        VerificaCodiceFiscale(x)

    End Sub

    Private Sub EtichetteImporti()

        Dim Param As New Cls_Parametri

        Param.LeggiParametri(Session("DC_OSPITE"))

        If Param.AlberghieroAssistenziale = 1 Then

            Grd_ImportoParenti.Columns(5).HeaderText = "Importo Assistenziale"
            Grd_ImportoParenti.Columns(2).HeaderText = "Importo Alberghiero"
        End If
        If Param.SocialeSanitario = 1 Then
            Grd_ImportoParenti.Columns(5).HeaderText = "Importo Sanitario"
            Grd_ImportoParenti.Columns(2).HeaderText = "Importo Sociale"
        End If

        If Param.AlberghieroAssistenziale = 0 And Param.SocialeSanitario = 0 Then
            Grd_ImportoParenti.Columns(5).Visible = False
            Grd_ImportoParenti.Columns(2).HeaderText = "Importo"
        End If


        Dim m As New Cls_Parametri

        m.LeggiParametri(Session("DC_OSPITE"))


        If m.SeparaPeriodoOspiteParente = 0 Then
            Grd_ImportoParenti.Columns(10).Visible = False
        End If

        Dim k As New Cls_Tabelle

        k.Descrizione = ""
        k.TipTab = "IME"
        k.Codice = "01"
        k.Leggi(Session("DC_OSPITE"))
        Grd_ImportoParenti.Columns(6).HeaderText = k.Descrizione
        If k.Descrizione = "" Then
            Grd_ImportoParenti.Columns(6).Visible = False
        End If

        k.Descrizione = ""
        k.TipTab = "IME"
        k.Codice = "02"
        k.Leggi(Session("DC_OSPITE"))

        Grd_ImportoParenti.Columns(7).HeaderText = k.Descrizione
        If k.Descrizione = "" Then
            Grd_ImportoParenti.Columns(7).Visible = False
        End If

        k.Descrizione = ""
        k.TipTab = "IME"
        k.Codice = "03"
        k.Leggi(Session("DC_OSPITE"))
        Grd_ImportoParenti.Columns(8).HeaderText = k.Descrizione
        If k.Descrizione = "" Then
            Grd_ImportoParenti.Columns(8).Visible = False
        End If

        k.Descrizione = ""
        k.TipTab = "IME"
        k.Codice = "04"
        k.Leggi(Session("DC_OSPITE"))
        Grd_ImportoParenti.Columns(9).HeaderText = k.Descrizione
        If k.Descrizione = "" Then
            Grd_ImportoParenti.Columns(9).Visible = False
        End If

    End Sub

    Protected Sub BTN_InserisciRiga_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles BTN_InserisciRiga.Click
        Call InserisciRiga()
    End Sub

    Protected Sub ImageButton1_Click(sender As Object, e As ImageClickEventArgs) Handles ImageButton1.Click
        If DD_TipoOperazione.SelectedValue <> "" Then
            ScriptManager.RegisterStartupScript(Me, Me.GetType(), "Errore", "VisualizzaErrore('Non posso eliminare c è il tipo operazione');", True)
            Call EseguiJS()
            Exit Sub
        End If


        Dim Mastro As Long
        Dim Conto As Long
        Dim Sottoconto As Long
        Dim MySql As String

        Dim cn As OleDbConnection

        cn = New Data.OleDb.OleDbConnection(Session("DC_GENERALE"))

        cn.Open()


        Dim XCs As New Cls_CentroServizio
        XCs.Leggi(Session("DC_OSPITE"), Session("CODICESERVIZIO"))
        Mastro = XCs.MASTRO
        Conto = XCs.CONTO
        Sottoconto = (Session("CODICEOSPITE") * 100) + Session("CODICEPARENTE")



        If Mastro = 0 Or Sottoconto = 0 Then Exit Sub



        MySql = "SELECT NumeroRegistrazione,DataRegistrazione,NumeroDocumento,DataDocumento,NumeroProtocollo,AnnoProtocollo,CausaleContabile,MovimentiContabiliRiga.Descrizione,DareAvere,Importo,RigaDaCausale,MastroContropartita,ContoContropartita,SottocontoContropartita,MovimentiContabiliTesta.NumeroBolletta,MovimentiContabiliTesta.DataBolletta " & _
                " FROM MovimentiContabiliRiga , MovimentiContabiliTesta " & _
                " WHERE MovimentiContabiliRiga.Numero = MovimentiContabiliTesta.NumeroRegistrazione " & _
                " AND MastroPartita = " & Mastro & _
                " AND  ContoPartita = " & Conto & _
                " AND SottoContoPartita = " & Sottoconto
        MySql = MySql & " ORDER BY  DataRegistrazione DESC,NumeroRegistrazione Desc, RigaDaCausale, MastroPartita, ContoPartita, SottocontoPartita"

        Dim cmd As New OleDbCommand()

        cmd.CommandText = (MySql)
        cmd.Connection = cn
        Dim Saldo As Double
        Saldo = 0
        Dim myPOSTreader As OleDbDataReader = cmd.ExecuteReader()
        If myPOSTreader.Read Then
            ScriptManager.RegisterStartupScript(Me, Me.GetType(), "Errore", "VisualizzaErrore('Non posso eliminare ci sono registrazioni');", True)
            Call EseguiJS()            
            myPOSTreader.Close()
            Exit Sub
        End If
        myPOSTreader.Close()

        Dim ParCod As New Cls_Parenti

        ParCod.CodiceOspite = Session("CODICEOSPITE")
        ParCod.CodiceParente = Session("CODICEPARENTE")

        Dim Log1 As New Cls_LogPrivacy

        Log1.LogPrivacy(Application("SENIOR"), Session("CLIENTE"), Session("UTENTE"), Session("UTENTEIMPER"), Page.GetType.Name.ToUpper, Val(Session("CODICEOSPITE")), 0, "", "", 0, "", "D", "PARENTE", "")


        ParCod.Elimina(Session("DC_OSPITE"), ParCod.CodiceOspite, ParCod.CodiceParente)


        Response.Redirect("SelezionaParente.aspx?CodiceParente=0&CodiceOspite=" & Session("CODICEOSPITE") & "&CentroServizio=" & Session("CODICESERVIZIO"))

    End Sub

    Protected Sub Img_CreaAccoglimento_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles Img_CreaAccoglimento.Click

        If Val(Session("CODICEPARENTE")) = 0 Then
            ScriptManager.RegisterStartupScript(Me, Me.GetType(), "Errore", "VisualizzaErrore('Devi prima inserire il parente');", True)
            ScriptManager.RegisterStartupScript(Me, Me.GetType(), "Scheramta", "VisualizzaDivRegistrazione();", True)
            Call EseguiJS()
            Exit Sub
        End If

        Dim Kp As New Cls_LoginParente
        Dim StLog As New Cls_Login

        StLog.Utente = Session("UTENTE")
        StLog.LeggiSP(Application("SENIOR"))

        Kp.Password = ""
        Kp.CodiceOspite = Session("CODICEOSPITE")
        Kp.CodiceParente = Session("CODICEPARENTE")
        Kp.Societa = StLog.Cliente
        Kp.LeggiDatiParente(Application("SENIOR"))

        If Txt_Utente.Text.Length < 5 Then
            ScriptManager.RegisterStartupScript(Me, Me.GetType(), "Errore", "VisualizzaErrore('Il nome utente deve essere almeno di 6 caratteri');", True)
            ScriptManager.RegisterStartupScript(Me, Me.GetType(), "Scheramta", "VisualizzaDivRegistrazione();", True)
            Call EseguiJS()
            Exit Sub
        End If

        Dim FoundMatch As Boolean
        Try
            FoundMatch = Regex.IsMatch(Txt_Utente.Text, "\A(?:[a-z0-9!#$%&'*+/=?^_`{|}~-]+(?:\.[a-z0-9!#$%&'*+/=?^_`{|}~-]+)*@(?:[a-z0-9](?:[a-z0-9-]*[a-z0-9])?\.)+[a-z0-9](?:[a-z0-9-]*[a-z0-9])?)\Z", RegexOptions.IgnoreCase)
        Catch ex As ArgumentException
            'Syntax error in the regular expression
        End Try

        If Not FoundMatch Then
            ScriptManager.RegisterStartupScript(Me, Me.GetType(), "Errore", "VisualizzaErrore('Il nome utente deve essere un indirizzo mail');", True)
            ScriptManager.RegisterStartupScript(Me, Me.GetType(), "Scheramta", "VisualizzaDivRegistrazione();", True)
            Call EseguiJS()
            Exit Sub
        End If

        If TxT_Password.Text = "" And Kp.Password = "" Then
            ScriptManager.RegisterStartupScript(Me, Me.GetType(), "Errore", "VisualizzaErrore('Devi digitare la Password');", True)
            ScriptManager.RegisterStartupScript(Me, Me.GetType(), "Scheramta", "VisualizzaDivRegistrazione();", True)
            Call EseguiJS()
            Exit Sub
        End If
        Dim Ver As New Cls_LoginParente

        Ver.Password = ""
        Ver.CodiceOspite = Session("CODICEOSPITE")
        Ver.CodiceParente = Session("CODICEPARENTE")
        Ver.Societa = StLog.Cliente
        Ver.Utente = Txt_Utente.Text
        If Ver.PresenteUtente(Application("SENIOR")) Then
            ScriptManager.RegisterStartupScript(Me, Me.GetType(), "Errore", "VisualizzaErrore('Utente già presente');", True)
            ScriptManager.RegisterStartupScript(Me, Me.GetType(), "Scheramta", "VisualizzaDivRegistrazione();", True)
            Call EseguiJS()
            Exit Sub
        End If


        If TxT_Password.Text <> "" Then
    


            If TxT_Password.Text <> TxT_ConfermaPassword.Text Then
                ScriptManager.RegisterStartupScript(Me, Me.GetType(), "Errore", "VisualizzaErrore('Password non coincidente');", True)
                ScriptManager.RegisterStartupScript(Me, Me.GetType(), "Scheramta", "VisualizzaDivRegistrazione();", True)
                Call EseguiJS()
                Exit Sub
            End If
            If Not Regex.IsMatch(TxT_Password.Text, "[a-z]") Then
                ScriptManager.RegisterStartupScript(Me, Me.GetType(), "Errore", "VisualizzaErrore('Password deve contenere caratteri minuscoli');", True)
                ScriptManager.RegisterStartupScript(Me, Me.GetType(), "Scheramta", "VisualizzaDivRegistrazione();", True)
                Call EseguiJS()
                Exit Sub
            End If
            If Not Regex.IsMatch(TxT_Password.Text, "[A-Z]") Then
                ScriptManager.RegisterStartupScript(Me, Me.GetType(), "Errore", "VisualizzaErrore('Password deve contenere caratteri maiuscoli');", True)
                ScriptManager.RegisterStartupScript(Me, Me.GetType(), "Scheramta", "VisualizzaDivRegistrazione();", True)
                Call EseguiJS()
                Exit Sub
            End If
            If Not Regex.IsMatch(TxT_Password.Text, "[0-9]") Then
                ScriptManager.RegisterStartupScript(Me, Me.GetType(), "Errore", "VisualizzaErrore('Password deve contenere numeri');", True)
                ScriptManager.RegisterStartupScript(Me, Me.GetType(), "Scheramta", "VisualizzaDivRegistrazione();", True)
                Call EseguiJS()
                Exit Sub
            End If
            If TxT_Password.Text.Length < 6 Then
                ScriptManager.RegisterStartupScript(Me, Me.GetType(), "Errore", "VisualizzaErrore('Password deve essere lunga almeno 6 caratteri');", True)
                ScriptManager.RegisterStartupScript(Me, Me.GetType(), "Scheramta", "VisualizzaDivRegistrazione();", True)
                Call EseguiJS()
                Exit Sub
            End If
        End If


        Kp.Utente = Txt_Utente.Text
        If TxT_Password.Text <> "" Then
            Kp.Password = TxT_Password.Text
        End If
        Kp.Societa = StLog.Cliente
        If Chk_Stanze.Checked = True Then
            Kp.Stanza = 1
        End If
        If Chk_Denaro.Checked = True Then
            Kp.Denaro = 1
        End If
        If Chk_Contabilita.Checked = True Then

            Kp.Contabilita = 1
        End If
        If Chk_Diurno.Checked = True Then
            Kp.Diurno = 1
        End If
        Kp.ScriviDatiParente(Application("SENIOR"))
    End Sub


    Private Shared Function CertificateHandler(ByVal sender As Object, ByVal certificate As X509Certificate, ByVal chain As X509Chain, ByVal SSLerror As SslPolicyErrors) As Boolean
        Return True
    End Function

    Protected Overrides Sub Finalize()
        MyBase.Finalize()
    End Sub

    Protected Sub Btn_RecuperaEperesonam_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles Btn_RecuperaEperesonam.Click
        Leggi_Parente()
    End Sub



    Protected Sub DD_TipoOperazioneChange(ByVal sender As Object, ByVal e As System.EventArgs)
        Dim appoggio As String

        Dim I As Integer
        Dim riga As Integer = 0

        For I = 0 To Grd_ImportoParenti.Rows.Count - 1
            If sender.clientid = Grd_ImportoParenti.Rows(I).Cells(10).Controls.Item(1).ClientID Then
                riga = I
            End If
        Next

        If riga = Grd_ImportoParenti.Rows.Count - 1 Then
            DD_TipoOperazione.SelectedValue = sender.SelectedValue
        End If


    End Sub



    Private Sub ExportPDF(ByVal NomeFile As String)
        Dim contenuto As String
        If Val(NomeFile) > 0 Then
            Dim Personalizzazione As New Cls_Tabella_GDPR

            Personalizzazione.Id = Val(NomeFile)
            Personalizzazione.Leggi(Session("DC_OSPITE"))

            contenuto = Personalizzazione.Testo
        Else
            Dim objStreamReader As StreamReader


            objStreamReader = File.OpenText(Server.MapPath(NomeFile))

            ' Leggo il contenuto del file sino alla fine (ReadToEnd)
            contenuto = objStreamReader.ReadToEnd()

            objStreamReader.Close()
        End If

        Dim TabSoc As New Cls_TabellaSocieta

        TabSoc.Leggi(Session("DC_TABELLE"))


        Dim Osp As New ClsOspite

        Osp.CodiceOspite = Session("CODICEOSPITE")
        Osp.Leggi(Session("DC_OSPITE"), Osp.CodiceOspite)


        contenuto = contenuto.Replace("@NOMEOSPITE@", Osp.Nome)
        contenuto = contenuto.Replace("@NOMEPARENTE@", Txt_CognomeNome.Text & " " & Txt_Nome.Text)
        contenuto = contenuto.Replace("@NOMETITOLARE@", TabSoc.RagioneSociale)
        contenuto = contenuto.Replace("@INDIRIZZO@", TabSoc.Indirizzo)
        contenuto = contenuto.Replace("@CAP@", TabSoc.Cap)
        contenuto = contenuto.Replace("@CITTA@", TabSoc.Localita)
        contenuto = contenuto.Replace("@TEL@", TabSoc.Telefono)
        contenuto = contenuto.Replace("@EMAIL@", TabSoc.EMail)


        contenuto = contenuto.Replace("é", "&eacute;")
        contenuto = contenuto.Replace("è", "&egrave;")
        contenuto = contenuto.Replace("à", "&agrave;")
        contenuto = contenuto.Replace("ò", "&ograve;")
        contenuto = contenuto.Replace("ù", "&ugrave;")
        contenuto = contenuto.Replace("ì", "&igrave;")

        contenuto = contenuto.Replace("é", "&eacute;")
        contenuto = contenuto.Replace("È", "&Egrave;")
        contenuto = contenuto.Replace("À", "&Aacute;")
        contenuto = contenuto.Replace("Ò", "&Ograve;")
        contenuto = contenuto.Replace("Ù", "&Ugrave;")
        contenuto = contenuto.Replace("Ì", "&Igrave;")
        contenuto = contenuto.Replace("'", "&#39;")

        contenuto = contenuto.Replace("´", "&#180;")
        contenuto = contenuto.Replace("`", "&#96;")

        contenuto = Server.UrlEncode(contenuto.Replace(vbNewLine, ""))

        Session("PdfDaHTML") = contenuto
    End Sub



    Public Function SHA1(ByVal StringaDaConvertire As String) As String
        Dim sha2 As New System.Security.Cryptography.SHA1CryptoServiceProvider()
        Dim hash() As Byte
        Dim bytes() As Byte
        Dim output As String = ""
        Dim i As Integer

        bytes = System.Text.Encoding.UTF8.GetBytes(StringaDaConvertire)
        hash = sha2.ComputeHash(bytes)

        For i = 0 To hash.Length - 1
            output = output & hash(i).ToString("x2")
        Next

        Return output
    End Function

    Protected Sub IB_Download_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles IB_Download.Click
        If DD_Report.SelectedIndex = 0 Then
            ExportPDF("..\modulo\Informativa_GDPR_DatiOspitefornitiDaParente.html")
        End If
        If DD_Report.SelectedIndex = 1 Then
            ExportPDF("..\modulo\Informativa_GDPR_DatiParentefornitiDaOspite.html")
        End If
        If DD_Report.SelectedIndex = 2 Then
            ExportPDF("..\modulo\Informativa_GDPR_DatiParentefornitiDaParente.html")
        End If
        If DD_Report.SelectedIndex = 3 Then
            ExportPDF("..\modulo\Informativa_GDPR_DatiOSPITEnonautosuff.html")
        End If

        If DD_Report.SelectedIndex > 3 Then
            ExportPDF(DD_Report.SelectedValue)
        End If

        'DD_Report.Items.Add("Informativa Ospite Fornita da Parente")
        'DD_Report.Items.Add("Informativa Parente Fornita da Ospite")
        'DD_Report.Items.Add("Informativa Parente Forniti da Parente")
        'DD_Report.Items.Add("Informativa Ospite Non Autosufficiente")

    End Sub

    Protected Sub RB_NO_CheckedChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles RB_NO.CheckedChanged
        If RB_NO.Checked = True Then
            Chk_ExtraFuoriFattura.Visible = True
        Else
            Chk_ExtraFuoriFattura.Visible = False
            Chk_ExtraFuoriFattura.Checked = False
        End If
    End Sub

    Protected Sub RB_SI_CheckedChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles RB_SI.CheckedChanged
        If RB_NO.Checked = True Then
            Chk_ExtraFuoriFattura.Visible = True
        Else
            Chk_ExtraFuoriFattura.Visible = False
            Chk_ExtraFuoriFattura.Checked = False
        End If
    End Sub

    Protected Sub RB_Dettaglio_CheckedChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles RB_Dettaglio.CheckedChanged
        If RB_NO.Checked = True Then
            Chk_ExtraFuoriFattura.Visible = True
        Else
            Chk_ExtraFuoriFattura.Visible = False
            Chk_ExtraFuoriFattura.Checked = False
        End If
    End Sub

   
End Class
