﻿Imports System.Data.OleDb
Imports System.Web.Hosting

Public Class CreaTabellaPresenze
    Implements System.Web.IHttpHandler
    Dim MyTable As New System.Data.DataTable("tabella")
    Dim MyTable2 As New System.Data.DataTable("tabellaGriglia")
    Dim MyDataSet As New System.Data.DataSet()

    Dim MytabellaPersonalizzata As New System.Data.DataTable("tabellaPersonalizzata")

    Public Function GiorniMese(ByVal Mese As Byte, ByVal Anno As Integer) As Byte
        If Mese = 1 Or Mese = 3 Or Mese = 5 Or Mese = 7 Or Mese = 8 Or
           Mese = 10 Or Mese = 12 Then
            GiorniMese = 31
        Else
            If Mese <> 2 Then
                GiorniMese = 30
            Else
                If Day(DateSerial(Anno, Mese, 29)) = 29 Then
                    GiorniMese = 29
                Else
                    GiorniMese = 28
                End If
            End If
        End If
    End Function


    Private Sub CaricaGriglia(ByVal ConnectionString As String, ByVal Cserv As String, ByVal Anno As Integer, ByVal Mese As Integer, ByVal ConnectionStringGenerale As String, ByVal ConnectionStringOspitiAccessori As String)
        Dim cn As OleDbConnection
        Dim x As Long

        cn = New Data.OleDb.OleDbConnection(ConnectionString)

        cn.Open()
        Dim TotaleGenerale As Long = 0
        Dim TotaleGeneraleCausali(100) As Long

        Dim VettoriCausali(100) As String
        Dim NumeroCausali As Integer
        Dim Giorni As Integer
        Dim I As Integer
        Dim MySql As String
        Dim PGiorni As Integer = 4
        Dim NonIndicatoCServ As Boolean = False
        Dim CondOspite As String


        Giorni = GiorniMese(Mese, Anno)


        NonIndicatoCServ = True
        I = 0

        NumeroCausali = I

        If Cserv = "CC" Then
            Cserv = "CC"
        End If


        Dim CentroServizioIndicato As Boolean = True

        Dim Param As New Cls_Parametri


        Param.LeggiParametri(ConnectionString)

        Dim Cs As New Cls_CentroServizio


        Cs.CENTROSERVIZIO = Cserv
        Cs.Leggi(ConnectionString, Cserv)
        If Cs.TIPOCENTROSERVIZIO = "D" Then
            'If NonIndicatoCServ = False Then
            MyTable2.Clear()
            MyTable2.Columns.Clear()
            MyTable2.Columns.Add("N", GetType(Long))
            MyTable2.Columns.Add("Nome", GetType(String))
            MyTable2.Columns.Add("Auto", GetType(String))

            PGiorni = 2
            MyTable.Clear()
            MyTable.Columns.Clear()
            MyTable.Columns.Add("N", GetType(Long))
            MyTable.Columns.Add("Nome", GetType(String))
            MyTable.Columns.Add("Auto", GetType(String))
            For I = 1 To Giorni
                MyTable.Columns.Add(I, GetType(String))
                MyTable2.Columns.Add(I, GetType(String))
            Next
            MyTable.Columns.Add("Totale", GetType(String))
            MyTable2.Columns.Add("Totale", GetType(String))


            I = 0

            MySql = "SELECT * FROM CAUSALI  where diurno >0 Order by Descrizione"
            Dim cmdCau As New OleDbCommand()
            cmdCau.CommandText = MySql
            cmdCau.Connection = cn

            Dim MyCau As OleDbDataReader = cmdCau.ExecuteReader()
            Do While MyCau.Read
                MyTable.Columns.Add("(" & MyCau.Item("CODICE") & ") " & MyCau.Item("DESCRIZIONE"), GetType(String))
                MyTable2.Columns.Add("(" & MyCau.Item("CODICE") & ") " & MyCau.Item("DESCRIZIONE"), GetType(String))
                VettoriCausali(I) = MyCau.Item("CODICE")
                I = I + 1
            Loop
            MyCau.Close()
            NumeroCausali = I
            'End If


            'CondOspite = " movimenti.codiceospite = 494 "
            If CondOspite = "" Then
                MySql = "SELECT movimenti.codiceospite  FROM AnagraficaComune  inner join  movimenti on movimenti.codiceospite = AnagraficaComune.CODICEOSPITE where  (AnagraficaComune.NonInUso IS Null OR  AnagraficaComune.NonInUso  = '') And  CENTROSERVIZIO = ? group by movimenti.CODICEOSPITE"
            Else
                MySql = "SELECT movimenti.codiceospite  FROM AnagraficaComune  inner join  movimenti on movimenti.codiceospite = AnagraficaComune.CODICEOSPITE where  (AnagraficaComune.NonInUso IS Null OR  AnagraficaComune.NonInUso  = '') And  CENTROSERVIZIO = ? And " & CondOspite & " group by movimenti.CODICEOSPITE"
            End If

            Dim cmd As New OleDbCommand()
            cmd.CommandText = MySql
            cmd.Connection = cn

            cmd.Parameters.AddWithValue("@Cserv", Cserv)
            Dim myPOSTreader As OleDbDataReader = cmd.ExecuteReader()
            Do While myPOSTreader.Read

                Dim AccoltoNelMese As Integer = 0
                Dim Mov As New Cls_Movimenti

                Mov.CodiceOspite = myPOSTreader.Item("CodiceOspite")
                Mov.CENTROSERVIZIO = Cserv
                Mov.UltimaDataAccoglimento(ConnectionString)
                If Month(Mov.Data) = Mese And Year(Mov.Data) = Anno Then
                    AccoltoNelMese = Day(Mov.Data)
                End If

                If Mov.CodiceOspite = 22 Then
                    Mov.CodiceOspite = 22
                End If

                Dim UscitiNelMese As Integer = 0
                Dim MovU As New Cls_Movimenti

                MovU.CodiceOspite = myPOSTreader.Item("CodiceOspite")
                MovU.CENTROSERVIZIO = Cserv
                MovU.UltimaDataUscitaDefinitivaMese(ConnectionString, Anno, Mese)
                If Month(MovU.Data) = Mese And Year(MovU.Data) = Anno Then
                    UscitiNelMese = Day(MovU.Data)
                End If

                Dim cmd1 As New OleDbCommand()
                cmd1.CommandText = "Select * From AssenzeCentroDiurno where [CENTROSERVIZIO] = ? and  [CODICEOSPITE] = ? and anno = ? and mese = ?"
                cmd1.Connection = cn

                cmd1.Parameters.AddWithValue("@Cserv", Cserv)
                cmd1.Parameters.AddWithValue("@CodiceOspite", myPOSTreader.Item("CodiceOspite"))
                cmd1.Parameters.AddWithValue("@Anno", Anno)
                cmd1.Parameters.AddWithValue("@Mese", Mese)

                Dim Read1 As OleDbDataReader = cmd1.ExecuteReader()
                If Read1.Read Then
                    Dim myriga As System.Data.DataRow = MyTable.NewRow()

                    Dim K As New ClsOspite

                    K.CodiceOspite = myPOSTreader.Item("CodiceOspite")
                    K.Leggi(ConnectionString, K.CodiceOspite)


                    myriga(0) = K.CodiceOspite
                    myriga(1) = " " & K.Nome

                    For I = 1 To Giorni

                        myriga(I + PGiorni) = campodb(Read1.Item("GIORNO" & I))

                        If AccoltoNelMese > 0 Then
                            If AccoltoNelMese > I Then
                                myriga(I + PGiorni) = "C"
                            End If
                        End If
                        If UscitiNelMese > 0 Then
                            If UscitiNelMese < I Then
                                myriga(I + PGiorni) = "C"
                            End If
                        End If
                    Next
                    MyTable.Rows.Add(myriga)
                Else
                    Dim Presente As New Cls_Movimenti

                    Presente.CENTROSERVIZIO = Cserv
                    Presente.CodiceOspite = myPOSTreader.Item("CodiceOspite")
                    Presente.TipoMov = ""
                    Presente.UltimaMovimentoPrimaData(ConnectionString, Presente.CodiceOspite, Presente.CENTROSERVIZIO, DateSerial(Anno, Mese, 1))

                    If Presente.TipoMov <> "13" And Presente.TipoMov <> "" Then
                        Dim myriga As System.Data.DataRow = MyTable.NewRow()

                        Dim Kx As New ClsOspite

                        Kx.CodiceOspite = myPOSTreader.Item("CodiceOspite")
                        Kx.Leggi(ConnectionString, Kx.CodiceOspite)


                        myriga(0) = Kx.CodiceOspite
                        myriga(1) = " " & Kx.Nome


                        Dim TbCServ As New Cls_CentroServizio
                        Dim SETTIMANA As String
                        Dim Causale(32) As String

                        For I = 1 To 31
                            Causale(I) = ""
                        Next

                        Dim Gg As Integer
                        TbCServ.Leggi(ConnectionString, Cs.CENTROSERVIZIO)

                        Dim AnaOsp As New ClsOspite

                        AnaOsp.Leggi(ConnectionString, Presente.CodiceOspite)

                        Dim K As New Cls_DatiOspiteParenteCentroServizio


                        K.CentroServizio = Cs.CENTROSERVIZIO
                        K.CodiceOspite = Presente.CodiceOspite
                        K.Leggi(ConnectionString)
                        If K.TipoOperazione <> "" Then
                            AnaOsp.SETTIMANA = K.Settimana
                        End If



                        SETTIMANA = AnaOsp.SETTIMANA


                        For I = 1 To GiorniMese(Mese, Anno)
                            Gg = Weekday(DateSerial(Anno, Mese, I), vbMonday)
                            If Trim(Mid(SETTIMANA, Gg, 1)) <> "" Then
                                Causale(I) = Mid(SETTIMANA, Gg, 1)
                            End If
                        Next I


                        SETTIMANA = TbCServ.SETTIMANA
                        For I = 1 To GiorniMese(Mese, Anno)
                            Gg = Weekday(DateSerial(Anno, Mese, I), vbMonday)
                            If Trim(Mid(SETTIMANA, Gg, 1)) <> "" Then
                                Causale(I) = "C"
                            End If
                        Next I


                        If Mese = 1 Then
                            If TbCServ.GIORNO0101 <> "S" Then
                                Causale(1) = "C"
                            End If
                            If TbCServ.GIORNO0601 <> "S" Then
                                Causale(6) = "C"
                            End If
                        End If
                        If Mese = 3 Then
                            If TbCServ.GIORNO1903 <> "S" Then
                                Causale(19) = "C"
                            End If
                        End If
                        If Mese = 4 Then
                            If TbCServ.GIORNO2504 <> "S" Then
                                Causale(25) = "C"
                            End If
                        End If
                        If Mese = 5 Then
                            If TbCServ.GIORNO0105 <> "S" Then
                                Causale(1) = "C"
                            End If
                        End If

                        If Mese = 6 Then
                            If TbCServ.GIORNO0206 <> "S" Then
                                Causale(2) = "C"
                            End If
                            If TbCServ.GIORNO2906 <> "S" Then
                                Causale(29) = "C"
                            End If
                        End If
                        If Mese = 8 Then
                            If TbCServ.GIORNO1508 <> "S" Then
                                Causale(15) = "C"
                            End If
                        End If
                        If Mese = 11 Then
                            If TbCServ.GIORNO0111 <> "S" Then
                                Causale(1) = "C"
                            End If
                            If TbCServ.GIORNO0411 <> "S" Then
                                Causale(4) = "C"
                            End If
                        End If
                        If Mese = 12 Then
                            If TbCServ.GIORNO0812 <> "S" Then
                                Causale(8) = "C"
                            End If
                            If TbCServ.GIORNO2512 <> "S" Then
                                Causale(25) = "C"
                            End If
                            If TbCServ.GIORNO2612 <> "S" Then
                                Causale(26) = "C"
                            End If
                        End If
                        If TbCServ.GIORNO1ATTIVO <> "S" Then
                            If Not IsDBNull(TbCServ.GIORNO1) Then
                                If Val(Mid(TbCServ.GIORNO1, 4, 2)) = Mese Then
                                    Causale(Val(Mid(TbCServ.GIORNO1, 1, 2))) = "C"
                                End If
                            End If
                        End If

                        If TbCServ.GIORNO2ATTIVO <> "S" Then
                            If Not IsDBNull(TbCServ.GIORNO2) Then
                                If Val(Mid(TbCServ.GIORNO2, 4, 2)) = Mese Then
                                    Causale(Val(Mid(TbCServ.GIORNO2, 1, 2))) = "C"
                                End If
                            End If
                        End If

                        If TbCServ.GIORNO3ATTIVO <> "S" Then
                            If Not IsDBNull(TbCServ.GIORNO3) Then
                                If Val(Mid(TbCServ.GIORNO3, 4, 2)) = Mese Then
                                    Causale(Val(Mid(TbCServ.GIORNO3, 1, 2))) = "C"
                                End If
                            End If
                        End If

                        If TbCServ.GIORNO4ATTIVO <> "S" Then
                            If Not IsDBNull(TbCServ.GIORNO4) Then
                                If Val(Mid(TbCServ.GIORNO4, 4, 2)) = Mese Then
                                    Causale(Val(Mid(TbCServ.GIORNO4, 1, 2))) = "C"
                                End If
                            End If
                        End If

                        If TbCServ.GIORNO5ATTIVO <> "S" Then
                            If Not IsDBNull(TbCServ.GIORNO5) Then
                                If Val(Mid(TbCServ.GIORNO5, 4, 2)) = Mese Then
                                    Causale(Val(Mid(TbCServ.GIORNO5, 1, 2))) = "C"
                                End If
                            End If
                        End If


                        For I = 1 To 31
                            myriga(I + PGiorni) = Causale(I)
                        Next

                        MyTable.Rows.Add(myriga)
                    End If
                End If
                Read1.Close()
            Loop
            myPOSTreader.Close()



        Else

            MyTable2.Clear()
            MyTable2.Columns.Clear()
            MyTable2.Columns.Add("N", GetType(Long))
            MyTable2.Columns.Add("Nome", GetType(String))
            MyTable2.Columns.Add("Auto", GetType(String))
            MyTable2.Columns.Add("Ing", GetType(String))
            MyTable2.Columns.Add("Dec", GetType(String))


            MyTable.Clear()
            MyTable.Columns.Clear()
            MyTable.Columns.Add("N", GetType(Long))
            MyTable.Columns.Add("Nome", GetType(String))
            MyTable.Columns.Add("Auto", GetType(String))
            MyTable.Columns.Add("Ing", GetType(String))
            MyTable.Columns.Add("Dec", GetType(String))
            For I = 1 To Giorni
                MyTable.Columns.Add(I, GetType(String))
                MyTable2.Columns.Add(I, GetType(String))
            Next
            MyTable.Columns.Add("Totale", GetType(String))
            MyTable2.Columns.Add("Totale", GetType(String))

            I = 0

            MySql = "SELECT * FROM CAUSALI  where diurno  = 0 Order by Descrizione"
            Dim cmdCau As New OleDbCommand()
            cmdCau.CommandText = MySql
            cmdCau.Connection = cn

            Dim MyCau As OleDbDataReader = cmdCau.ExecuteReader()
            Do While MyCau.Read
                MyTable.Columns.Add("(" & MyCau.Item("CODICE") & ") " & MyCau.Item("DESCRIZIONE"), GetType(String))
                MyTable2.Columns.Add("(" & MyCau.Item("CODICE") & ") " & MyCau.Item("DESCRIZIONE"), GetType(String))
                VettoriCausali(I) = MyCau.Item("CODICE")
                I = I + 1
            Loop
            MyCau.Close()
            NumeroCausali = I

            'CondOspite = " Codiceospite = 914 "
            'CondOspite = " codiceospite = 494 "
            For I = 1 To Giorni
                If CondOspite = "" Then
                    MySql = "SELECT codiceospite,nome,(select top 1 TIPOMOV from Movimenti Where movimenti.codiceospite = anagraficacomune.codiceospite and data <= ? and CENTROSERVIZIO = ? order by data desc,PROGRESSIVO )  as tipo,(select top 1 Data from Movimenti Where movimenti.codiceospite = anagraficacomune.codiceospite and data <= ? and CENTROSERVIZIO = ? order by data desc,PROGRESSIVO )  as Data,(select top 1 CAUSALE  from Movimenti Where movimenti.codiceospite = anagraficacomune.codiceospite and data <= ? and CENTROSERVIZIO = ? order by data desc,PROGRESSIVO )  as CAUSALE"
                    MySql = MySql & " FROM AnagraficaComune "
                    MySql = MySql & "where CodiceParente = 0 And (select top 1 TIPOMOV from Movimenti Where movimenti.codiceospite = anagraficacomune.codiceospite  and ((data < ?  AND TIPOMOV  = '13') OR (data <= ?  AND (TIPOMOV  = '05' OR TIPOMOV  = '03' OR TIPOMOV  = '04'))) and CENTROSERVIZIO = ? order by data desc,PROGRESSIVO desc ) <> '13' And (AnagraficaComune.NonInUso IS Null OR  AnagraficaComune.NonInUso  = '')"
                Else
                    MySql = "SELECT codiceospite,nome,(select top 1 TIPOMOV from Movimenti Where movimenti.codiceospite = anagraficacomune.codiceospite and data <= ? and CENTROSERVIZIO = ? order by data desc,PROGRESSIVO )  as tipo,(select top 1 Data from Movimenti Where movimenti.codiceospite = anagraficacomune.codiceospite and data <= ? and CENTROSERVIZIO = ? order by data desc,PROGRESSIVO )  as Data,(select top 1 CAUSALE  from Movimenti Where movimenti.codiceospite = anagraficacomune.codiceospite and data <= ? and CENTROSERVIZIO = ? order by data desc,PROGRESSIVO )  as CAUSALE"
                    MySql = MySql & " FROM AnagraficaComune "
                    MySql = MySql & "where CodiceParente = 0 And (select top 1 TIPOMOV from Movimenti Where movimenti.codiceospite = anagraficacomune.codiceospite  and ((data < ?  AND TIPOMOV  = '13') OR (data <= ?  AND (TIPOMOV  = '05' OR TIPOMOV  = '03' OR TIPOMOV  = '04'))) and CENTROSERVIZIO = ? order by data desc,PROGRESSIVO desc ) <> '13' And (AnagraficaComune.NonInUso IS Null OR  AnagraficaComune.NonInUso  = '') And " & CondOspite
                End If

                Dim cmd As New OleDbCommand()
                cmd.CommandText = MySql
                cmd.Connection = cn
                cmd.Parameters.AddWithValue("@Data", DateSerial(Anno, Mese, I))
                cmd.Parameters.AddWithValue("@Cserv", Cserv)
                cmd.Parameters.AddWithValue("@Data", DateSerial(Anno, Mese, I))
                cmd.Parameters.AddWithValue("@Cserv", Cserv)
                cmd.Parameters.AddWithValue("@Data", DateSerial(Anno, Mese, I))
                cmd.Parameters.AddWithValue("@Cserv", Cserv)
                cmd.Parameters.AddWithValue("@Data", DateSerial(Anno, Mese, I))
                cmd.Parameters.AddWithValue("@Data", DateSerial(Anno, Mese, I))
                cmd.Parameters.AddWithValue("@Cserv", Cserv)
                Dim myPOSTreader As OleDbDataReader = cmd.ExecuteReader()
                Do While myPOSTreader.Read
                    Dim OspFind As Boolean


                    OspFind = False
                    For x = 0 To MyTable.Rows.Count - 1
                        If MyTable.Rows(x).Item(0) = myPOSTreader.Item("CodiceOspite") Then
                            OspFind = True
                            If campodb(myPOSTreader.Item("tipo")) = "05" Then
                                If campodbD(myPOSTreader.Item("data")) = DateSerial(Anno, Mese, I) Then
                                    MyTable.Rows(x).Item(I + PGiorni) = ""
                                Else
                                    MyTable.Rows(x).Item(I + PGiorni) = ""
                                End If
                            End If
                            If campodb(myPOSTreader.Item("tipo")) = "13" Then
                                If campodbD(myPOSTreader.Item("data")) = DateSerial(Anno, Mese, I) Then
                                    MyTable.Rows(x).Item(I + PGiorni) = myPOSTreader.Item("CAUSALE")  '"U"
                                    MyTable.Rows(x).Item(4) = I
                                End If
                            End If
                            If campodb(myPOSTreader.Item("tipo")) = "04" Then
                                MyTable.Rows(x).Item(I + PGiorni) = ""
                            End If
                            If campodb(myPOSTreader.Item("tipo")) = "03" Then
                                MyTable.Rows(x).Item(I + PGiorni) = myPOSTreader.Item("CAUSALE")  '"A"                                
                            End If
                        End If
                    Next
                    If OspFind = False Then
                        Dim myriga As System.Data.DataRow = MyTable.NewRow()

                        myriga(0) = myPOSTreader.Item("CodiceOspite")
                        myriga(1) = " " & myPOSTreader.Item("Nome")
                        If campodb(myPOSTreader.Item("tipo")) = "05" Then
                            If myPOSTreader.Item("data") = DateSerial(Anno, Mese, I) Then
                                myriga(3) = I
                                myriga(I + PGiorni) = ""
                            Else
                                myriga(I + PGiorni) = ""
                            End If
                        End If
                        If campodb(myPOSTreader.Item("tipo")) = "13" Then
                            If campodbD(myPOSTreader.Item("data")) = DateSerial(Anno, Mese, I) Then
                                myriga(I + PGiorni) = campodb(myPOSTreader.Item("CAUSALE"))  '"U"
                            End If
                        End If
                        If campodb(myPOSTreader.Item("tipo")) = "04" Then
                            myriga(I + PGiorni) = ""
                        End If
                        If campodb(myPOSTreader.Item("tipo")) = "03" Then
                            myriga(I + PGiorni) = campodb(myPOSTreader.Item("CAUSALE"))  '"A"                                                                                   
                        End If
                        MyTable.Rows.Add(myriga)
                    End If
                Loop
                myPOSTreader.Close()
            Next

        End If

        If Cs.TIPOCENTROSERVIZIO <> "D" Then
            Dim IndRighe As Integer
            Dim IndColonne As Integer
            For IndRighe = 0 To MyTable.Rows.Count - 1
                Dim IndCodiceOspite As Integer
                Dim UscitoPrima As Boolean = False
                IndCodiceOspite = MyTable.Rows(IndRighe).Item(0)
                Dim UltMov As New Cls_Movimenti
                Dim UltimaCausale As String = ""
                Dim Modificato As Boolean = False

                UltMov.CodiceOspite = IndCodiceOspite
                UltMov.CENTROSERVIZIO = Cserv
                UltMov.TipoMov = ""
                UltMov.UltimaMovimentoPrimaData(ConnectionString, IndCodiceOspite, Cserv, DateSerial(Anno, Mese, 1))
                If UltMov.TipoMov = "03" Then
                    UscitoPrima = True
                    UltimaCausale = UltMov.Causale
                End If
                For IndColonne = PGiorni + 1 To MyTable.Columns.Count - 1
                    Modificato = False
                    If campodb(MyTable.Rows(IndRighe).Item(IndColonne)) <> "" And campodb(MyTable.Rows(IndRighe).Item(IndColonne)) <> "*" Then
                        If UscitoPrima = False Then
                            If Param.GIORNOUSCITA = "P" Then

                                UltMov.CodiceOspite = IndCodiceOspite
                                UltMov.CENTROSERVIZIO = Cserv
                                UltMov.TipoMov = ""
                                Dim DataAppoggio As Date = DateAdd("d", 1, DateSerial(Anno, Mese, IndColonne - PGiorni))
                                UltMov.UltimaMovimentoPrimaData(ConnectionString, IndCodiceOspite, Cserv, DataAppoggio)
                                If UltMov.TipoMov = "03" Then
                                    MyTable.Rows(IndRighe).Item(IndColonne) = ""
                                End If
                            End If
                            UscitoPrima = True
                        End If
                    Else
                        If Trim(campodb(MyTable.Rows(IndRighe).Item(IndColonne))) = "" Then
                            UscitoPrima = False
                            If UltimaCausale <> "" And UltimaCausale <> "*" Then
                                If Param.GIORNOENTRATA = "" Then
                                    UltMov.CodiceOspite = IndCodiceOspite
                                    UltMov.CENTROSERVIZIO = Cserv
                                    UltMov.TipoMov = ""
                                    Dim DataAppoggio As Date = DateAdd("d", 1, DateSerial(Anno, Mese, IndColonne - PGiorni))
                                    UltMov.UltimaMovimentoPrimaData(ConnectionString, IndCodiceOspite, Cserv, DataAppoggio)
                                    If UltMov.TipoMov = "03" Then
                                        MyTable.Rows(IndRighe).Item(IndColonne) = UltimaCausale
                                    End If
                                    UltimaCausale = ""
                                    Modificato = True
                                End If
                            End If
                        End If
                    End If
                    If Not Modificato Then
                        UltimaCausale = campodb(MyTable.Rows(IndRighe).Item(IndColonne))
                    End If
                Next
            Next
        End If



        Call TabTotaleGenerale(MyTable, VettoriCausali, TotaleGeneraleCausali, ConnectionString, Giorni, PGiorni, NumeroCausali, TotaleGenerale, Cserv)

        Dim CodiceOspite As Integer
        Dim Righe As Integer

        MySql = "DELETE FROM Tabelle_Interrogazione  where Cserv  = ? And Anno = ? And Mese = ? "
        Dim cmdVerInt As New OleDbCommand()
        cmdVerInt.CommandText = MySql
        cmdVerInt.Connection = cn
        cmdVerInt.Parameters.AddWithValue("@Cserv", Cserv)
        cmdVerInt.Parameters.AddWithValue("@Anno", Anno)
        cmdVerInt.Parameters.AddWithValue("@Mese", Mese)
        cmdVerInt.ExecuteNonQuery()

        Dim Quote As New Cls_CalcoloRette

        Quote.ConnessioneGenerale = ConnectionStringGenerale
        Quote.STRINGACONNESSIONEDB = ConnectionString
        Quote.ApriDB(ConnectionString, ConnectionStringOspitiAccessori)
        Dim TipoCausale As New Cls_CausaliEntrataUscita

        For Righe = 0 To MyTable.Rows.Count - 1
            Dim GiorniAssenza As Integer = 0

            CodiceOspite = MyTable.Rows(Righe).Item(0)

            Dim NomeOspite As New ClsOspite

            NomeOspite.CodiceOspite = CodiceOspite
            NomeOspite.Leggi(ConnectionString, NomeOspite.CodiceOspite)

            Dim UltMov As New Cls_Movimenti
            Dim UltimaCausale As String = ""
            Dim UscitoPrima As Boolean = False

            UltMov.CodiceOspite = CodiceOspite
            UltMov.CENTROSERVIZIO = Cserv
            UltMov.TipoMov = ""
            UltMov.UltimaMovimentoPrimaData(ConnectionString, CodiceOspite, Cserv, DateSerial(Anno, Mese, 1))
            If UltMov.TipoMov = "03" Then
                UscitoPrima = True
                UltimaCausale = UltMov.Causale
            End If

            Dim ImportoParenti As Boolean = False

            Dim cmd As New OleDbCommand()
            cmd.CommandText = ("select * from IMPORTOPARENTI where CentroServizio = '" & Cserv & "' And  CodiceOspite = " & CodiceOspite)
            cmd.Connection = cn
            Dim myPOSTreader As OleDbDataReader = cmd.ExecuteReader()
            If myPOSTreader.Read Then
                ImportoParenti = True
            End If
            myPOSTreader.Close()

            For I = 1 To GiorniMese(Mese, Anno)
                Dim InizioNumero As Integer = 4
                If Cs.TIPOCENTROSERVIZIO = "D" Then
                    InizioNumero = 2
                End If

                Dim Causale As String = ""
                Dim PresenzeAssenze As String = ""

                If campodb(MyTable.Rows(Righe).Item(I + InizioNumero)) = "" Then
                    Causale = ""
                    PresenzeAssenze = "P"
                Else
                    Causale = campodb(MyTable.Rows(Righe).Item(I + InizioNumero))

                    If TipoCausale.Codice <> Causale Then
                        TipoCausale.Codice = Causale
                        TipoCausale.LeggiCausale(ConnectionString)
                    End If
                    If UscitoPrima = False Then
                        If TipoCausale.GiornoUscita = "A" Then
                            PresenzeAssenze = "A"
                        End If
                        If TipoCausale.GiornoUscita = "P" Then
                            PresenzeAssenze = "P"
                        End If
                        If TipoCausale.GiornoUscita = "N" Then
                            PresenzeAssenze = "*"
                        End If
                        If TipoCausale.GiornoUscita = "" Then
                            PresenzeAssenze = "*"
                        End If
                    Else
                        If TipoCausale.TIPOMOVIMENTO = "A" Then
                            PresenzeAssenze = "A"
                        End If
                        If TipoCausale.TIPOMOVIMENTO = "P" Then
                            PresenzeAssenze = "P"
                        End If
                        If TipoCausale.TIPOMOVIMENTO = "N" Then
                            PresenzeAssenze = "*"
                        End If
                        If TipoCausale.TIPOMOVIMENTO = "" Then
                            PresenzeAssenze = "*"
                        End If
                    End If
                    UscitoPrima = True
                End If

                MySql = "Insert Into Tabelle_Interrogazione ([Cserv],CodiceoSpite,Anno,Mese,Giorno,Nome,[Causale],[QuotaOspite],[QuotaParenti],[QuotaComune],[QuotaRegione],[TipoQuotaSaniaria],[TipoRetta],[PresenzaAssenza]) VALUES " &
                                                           "(      ?,           ?,   ?,   ?,     ?,   ?,        ?,            ?,             ?,            ?,             ?,                  ?,          ?,?) "
                Dim cmdInsertDettaglio As New OleDbCommand()

                cmdInsertDettaglio.CommandText = MySql
                cmdInsertDettaglio.Connection = cn

                cmdInsertDettaglio.Parameters.AddWithValue("@Cserv", Cserv)
                cmdInsertDettaglio.Parameters.AddWithValue("@CodiceoSpite", CodiceOspite)
                cmdInsertDettaglio.Parameters.AddWithValue("@Anno", Anno)
                cmdInsertDettaglio.Parameters.AddWithValue("@Mese", Mese)
                cmdInsertDettaglio.Parameters.AddWithValue("@Giorno", I)
                cmdInsertDettaglio.Parameters.AddWithValue("@Nome", NomeOspite.Nome)
                cmdInsertDettaglio.Parameters.AddWithValue("@Causale", Causale)


                If Causale = "" Then
                    GiorniAssenza = 0
                    cmdInsertDettaglio.Parameters.AddWithValue("@QuotaOspite", Quote.QuoteGiornaliere(Cserv, CodiceOspite, "O", 0, DateSerial(Anno, Mese, I)))
                    If ImportoParenti Then
                        cmdInsertDettaglio.Parameters.AddWithValue("@QuotaParenti", Quote.QuoteGiornaliere(Cserv, CodiceOspite, "P", 0, DateSerial(Anno, Mese, I)))
                    Else
                        cmdInsertDettaglio.Parameters.AddWithValue("@QuotaParenti", 0)
                    End If
                    cmdInsertDettaglio.Parameters.AddWithValue("@QuotaComune", Quote.QuoteGiornaliere(Cserv, CodiceOspite, "C", 0, DateSerial(Anno, Mese, I)))
                    cmdInsertDettaglio.Parameters.AddWithValue("@QuotaRegione", Quote.QuotaRegione(Cserv, CodiceOspite, "R", 0, DateSerial(Anno, Mese, I)))
                Else
                    If Causale = "*" Then
                        cmdInsertDettaglio.Parameters.AddWithValue("@QuotaOspite", 0)
                        cmdInsertDettaglio.Parameters.AddWithValue("@QuotaParenti", 0)
                        cmdInsertDettaglio.Parameters.AddWithValue("@QuotaComune", 0)
                        cmdInsertDettaglio.Parameters.AddWithValue("@QuotaRegione", 0)
                    Else
                        Dim sc2 As New MSScriptControl.ScriptControl

                        sc2.Language = "vbscript"

                        GiorniAssenza = GiorniAssenza + 1
                        cmdInsertDettaglio.Parameters.AddWithValue("@QuotaOspite", Quote.QuoteGiornaliereAssenzaCausale(Cserv, CodiceOspite, "O", 0, sc2, Causale, GiorniAssenza, DateSerial(Anno, Mese, I)))
                        If ImportoParenti Then
                            cmdInsertDettaglio.Parameters.AddWithValue("@QuotaParenti", Quote.QuoteGiornaliereAssenzaCausale(Cserv, CodiceOspite, "P", 0, sc2, Causale, GiorniAssenza, DateSerial(Anno, Mese, I)))
                        Else
                            cmdInsertDettaglio.Parameters.AddWithValue("@QuotaParenti", 0)
                        End If
                        cmdInsertDettaglio.Parameters.AddWithValue("@QuotaComune", Quote.QuoteGiornaliereAssenzaCausale(Cserv, CodiceOspite, "C", 0, sc2, Causale, GiorniAssenza, DateSerial(Anno, Mese, I)))
                        cmdInsertDettaglio.Parameters.AddWithValue("@QuotaRegione", Quote.QuoteGiornaliereAssenzaCausale(Cserv, CodiceOspite, "R", 0, sc2, Causale, GiorniAssenza, DateSerial(Anno, Mese, I)))
                    End If
                End If

                Dim AppoggioStato As New Cls_StatoAuto

                AppoggioStato.CENTROSERVIZIO = Cserv
                AppoggioStato.CODICEOSPITE = CodiceOspite
                AppoggioStato.Data = DateSerial(Anno, Mese, I)
                AppoggioStato.StatoPrimaData(ConnectionString, CodiceOspite, Cserv)


                Dim MTipo As New Cls_TabellaTipoImportoRegione

                If campodb(AppoggioStato.TipoRetta) <> "" Then
                    MTipo.Codice = campodb(AppoggioStato.TipoRetta)
                    MTipo.Leggi(ConnectionString, MTipo.Codice)
                Else
                    MTipo.Descrizione = ""
                End If

                cmdInsertDettaglio.Parameters.AddWithValue("@TipoQuotaSaniaria", MTipo.Descrizione)

                Dim RettaTotale As New Cls_rettatotale

                RettaTotale.CENTROSERVIZIO = Cserv
                RettaTotale.CODICEOSPITE = CodiceOspite
                RettaTotale.Data = DateSerial(Anno, Mese, I)
                RettaTotale.RettaTotaleAData(ConnectionString, CodiceOspite, Cserv, RettaTotale.Data)

                Dim TipoRetta As New Cls_TipoRetta

                If campodb(RettaTotale.TipoRetta) <> "" Then
                    TipoRetta.Codice = campodb(RettaTotale.TipoRetta)
                    TipoRetta.Leggi(ConnectionString, TipoRetta.Codice)
                Else
                    TipoRetta.Descrizione = ""
                End If

                cmdInsertDettaglio.Parameters.AddWithValue("@TipoRetta", TipoRetta.Descrizione)
                cmdInsertDettaglio.Parameters.AddWithValue("@PresenzaAssenza", PresenzeAssenze)
                cmdInsertDettaglio.ExecuteNonQuery()
            Next
        Next
        Quote.ChiudiDB()

        MySql = "Insert Into Tabelle_Interrogazione ([Cserv],CodiceoSpite,Anno,Mese,Nome) VALUES " &
                                                   "(      ?,           ?,   ?,   ?,   ?) "
        Dim cmdInsertFine As New OleDbCommand()

        cmdInsertFine.CommandText = MySql
        cmdInsertFine.Connection = cn

        cmdInsertFine.Parameters.AddWithValue("@Cserv", Cserv)
        cmdInsertFine.Parameters.AddWithValue("@CodiceoSpite", -1)
        cmdInsertFine.Parameters.AddWithValue("@Anno", Anno)
        cmdInsertFine.Parameters.AddWithValue("@Mese", Mese)
        cmdInsertFine.Parameters.AddWithValue("@Nome", Now.ToString)
        cmdInsertFine.ExecuteNonQuery


    End Sub


    Private Sub TabTotaleGenerale(ByRef MyTable As Data.DataTable, ByVal VettoriCausali() As String, ByRef TotaleGeneraleCausali() As Long, ByVal ConnectionString As String, ByVal Giorni As Integer, ByVal PGiorni As Integer, ByVal NumeroCausali As Integer, ByRef TotaleGenerale As Integer, ByVal cserv As String)
        Dim x1 As Integer
        Dim Totale As Long
        Dim Totale2 As Long
        Dim TotaleCausali(100) As Long
        Dim IndI As Integer
        TotaleGenerale = 0

        For I = 0 To MyTable.Rows.Count - 1
            Dim pr As New Cls_StatoAuto

            pr.UltimaData(ConnectionString, MyTable.Rows(I).Item(0), cserv)
            MyTable.Rows(I).Item(2) = pr.STATOAUTO
            ' MyTable.Rows(I).Item(0) = I + 1

            Totale = 0
            Totale2 = 0

            For IndI = 0 To 100
                TotaleCausali(IndI) = 0
            Next
            For x1 = 1 To Giorni
                If IsDBNull(MyTable.Rows(I).Item(x1 + PGiorni)) Then
                    MyTable.Rows(I).Item(x1 + PGiorni) = "*"
                Else


                    If Trim(MyTable.Rows(I).Item(x1 + PGiorni)) = "" Then
                        Totale = Totale + 1
                    End If

                    For IndI = 0 To NumeroCausali - 1
                        If Trim(MyTable.Rows(I).Item(x1 + PGiorni)) = VettoriCausali(IndI) Then
                            TotaleCausali(IndI) = TotaleCausali(IndI) + 1
                        End If
                    Next
                End If
            Next
            MyTable.Rows(I).Item(Giorni + 1 + PGiorni) = Totale
            For IndI = 0 To NumeroCausali - 1
                MyTable.Rows(I).Item(Giorni + IndI + 2 + PGiorni) = TotaleCausali(IndI)
                TotaleGeneraleCausali(IndI) = TotaleGeneraleCausali(IndI) + TotaleCausali(IndI)
            Next
            TotaleGenerale = TotaleGenerale + Totale
        Next

    End Sub

    Public Sub ProcessRequest(ByVal context As HttpContext) Implements IHttpHandler.ProcessRequest
        context.Response.ContentType = "text/plain"
        Dim CSERV As String = context.Request.QueryString("CSERV")
        Dim ANNO As String = context.Request.QueryString("ANNO")
        Dim MESE As String = context.Request.QueryString("MESE")
        Dim UTENTE As String = context.Request.QueryString("UTENTE")

        Dim Inizio As String = Now.ToString

        context.Response.Cache.SetCacheability(HttpCacheability.NoCache)
        Dim DbC As New Cls_Login



        DbC.Utente = UTENTE
        DbC.LeggiSP(context.Application("SENIOR"))

        If Mid(MESE & Space(10), 1, 1) = "-" Then
            Dim I As Integer

            Dim cn As OleDbConnection

            cn = New Data.OleDb.OleDbConnection(DbC.Ospiti)

            cn.Open()
            Dim cmd As New OleDbCommand()

            ANNO = Year(Now)
            If Month(Now) >= Val(MESE.Replace("-", "")) Then
                MESE = Month(Now) - Val(MESE.Replace("-", ""))
            Else
                ANNO = ANNO - 1
                MESE = 12 - (Val(MESE.Replace("-", "")) - Month(Now))
            End If
            If MESE = 0 Then
                ANNO = ANNO - 1
                MESE = 12
            End If

            cmd.CommandText = ("select * from TABELLACENTROSERVIZIO ")
            cmd.Connection = cn

            Dim myPOSTreader As OleDbDataReader = cmd.ExecuteReader()
            Do While myPOSTreader.Read
                Call CaricaGriglia(DbC.Ospiti, myPOSTreader.Item("CENTROSERVIZIO"), ANNO, MESE, DbC.Generale, DbC.OspitiAccessori)

                CreaXML(DbC, myPOSTreader.Item("CENTROSERVIZIO"), MESE, ANNO)

            Loop
            myPOSTreader.Close()
            cn.Close()
        Else
            If MESE = "ANNO" Then
                Dim I As Integer
                For I = 1 To 12
                    Call CaricaGriglia(DbC.Ospiti, CSERV, ANNO, I, DbC.Generale, DbC.OspitiAccessori)
                Next
            Else
                Call CaricaGriglia(DbC.Ospiti, CSERV, ANNO, MESE, DbC.Generale, DbC.OspitiAccessori)
            End If
        End If





        context.Response.Write(Inizio & " " & Now.ToString)
    End Sub


    Public Sub CreaXML(ByVal DbC As Cls_Login, ByVal CSERV As String, ByVal MESE As String, ByVal ANNO As String)
        Dim MySql As String = ""
        Dim cn As OleDbConnection

        cn = New Data.OleDb.OleDbConnection(DbC.Ospiti)

        cn.Open()

        'CondOspite = " movimenti.codiceospite = 914 "
        MySql = "SELECT * FROM [PresenzeOspiti] WHERE ANNO = ?  AND MESE = ?"
        If CSERV <> "" Then
            MySql = MySql & " And Cserv = '" & CSERV & "'"
        End If
        Dim cmd As New OleDbCommand()
        cmd.CommandText = MySql
        cmd.Parameters.AddWithValue("@ANNO", ANNO)
        cmd.Parameters.AddWithValue("@MESE", MESE)
        cmd.Connection = cn
        Dim myXML As String

        Dim NomeFile As String

        NomeFile = HostingEnvironment.ApplicationPhysicalPath() & "\Public\Xml_" & DbC.RagioneSociale & "_" & CSERV & "_" & ANNO & "_" & MESE & ".xml"
        Dim tw As System.IO.TextWriter = System.IO.File.CreateText(NomeFile)

        myXML = "<tabella>"

        tw.Write(myXML)
        Dim Crypt As New Cls_Crypt


        Dim myPOSTreader As OleDbDataReader = cmd.ExecuteReader()
        Do While myPOSTreader.Read
            myXML = "<righe>" & vbNewLine
            myXML = myXML & "<ID>" & campodb(myPOSTreader("ID")) & "</ID>" & vbNewLine
            myXML = myXML & "<Cserv>" & campodb(myPOSTreader("Cserv")) & "</Cserv>" & vbNewLine
            myXML = myXML & "<DecodificaCentroServizio>" & campodb(myPOSTreader("DecodificaCentroServizio")) & "</DecodificaCentroServizio>" & vbNewLine
            myXML = myXML & "<CodiceOspite>" & campodb(myPOSTreader("CodiceOspite")) & "</CodiceOspite>" & vbNewLine
            myXML = myXML & "<Anno>" & campodb(myPOSTreader("Anno")) & "</Anno>" & vbNewLine
            myXML = myXML & "<Mese>" & campodb(myPOSTreader("Mese")) & "</Mese>" & vbNewLine
            myXML = myXML & "<Nome>" & Crypt.aesEncrypt(campodb(myPOSTreader("Nome")), "0bScUr3@Dv3n1@Sf0rS3CuR1Ty") & "</Nome>" & vbNewLine


            Dim codicefiscale As String = Crypt.aesEncrypt(campodb(myPOSTreader("codicefiscale")), "0bScUr3@Dv3n1@Sf0rS3CuR1Ty")
            myXML = myXML & "<codicefiscale>" & codicefiscale & "</codicefiscale>" & vbNewLine

            'myXML = myXML & "<codicefiscale>" & campodb(myPOSTreader("codicefiscale")) & "</codicefiscale>"
            myXML = myXML & "<Giorno>" & campodb(myPOSTreader("Giorno")) & "</Giorno>" & vbNewLine
            myXML = myXML & "<Causale>" & campodb(myPOSTreader("Causale")) & "</Causale>" & vbNewLine
            myXML = myXML & "<DecodificaCausale>" & campodb(myPOSTreader("DecodificaCausale")) & "</DecodificaCausale>" & vbNewLine
            myXML = myXML & "<QuotaOspite>" & campodb(myPOSTreader("QuotaOspite")) & "</QuotaOspite>" & vbNewLine
            myXML = myXML & "<QuotaParenti>" & campodb(myPOSTreader("QuotaParenti")) & "</QuotaParenti>" & vbNewLine
            myXML = myXML & "<QuotaComune>" & campodb(myPOSTreader("QuotaComune")) & "</QuotaComune>" & vbNewLine
            myXML = myXML & "<QuotaRegione>" & campodb(myPOSTreader("QuotaRegione")) & "</QuotaRegione>" & vbNewLine
            myXML = myXML & "<TipoQuotaSaniaria>" & campodbXML(myPOSTreader("TipoQuotaSaniaria")) & "</TipoQuotaSaniaria>" & vbNewLine
            myXML = myXML & "<TipoRetta>" & campodbXML(myPOSTreader("TipoRetta")) & "</TipoRetta>" & vbNewLine
            myXML = myXML & "<PresenzaAssenza>" & campodb(myPOSTreader("PresenzaAssenza")) & "</PresenzaAssenza>" & vbNewLine

            Dim DatiOspite As New ClsOspite

            DatiOspite.CodiceOspite = campodb(myPOSTreader("CodiceOspite"))
            DatiOspite.Leggi(DbC.Ospiti, DatiOspite.CodiceOspite)


            myXML = myXML & "<DataNascita>" & Format(DatiOspite.DataNascita, "dd/MM/yyyy") & "</DataNascita>" & vbNewLine

            myXML = myXML & "<Sesso>" & DatiOspite.Sesso & "</Sesso>" & vbNewLine


            Dim UltimoAccoglimento As New Cls_Movimenti

            UltimoAccoglimento.CodiceOspite = campodb(myPOSTreader("CodiceOspite"))
            UltimoAccoglimento.CENTROSERVIZIO = campodb(myPOSTreader("Cserv"))
            UltimoAccoglimento.Data = DateSerial(Val(campodb(myPOSTreader("Anno"))), Val(campodb(myPOSTreader("Mese"))), Val(campodb(myPOSTreader("Giorno"))))

            UltimoAccoglimento.UltimaDataAccoglimentoAData(DbC.Ospiti)


            myXML = myXML & "<DataAccoglimento>" & Format(UltimoAccoglimento.Data, "dd/MM/yyyy") & "</DataAccoglimento>" & vbNewLine


            If UltimoAccoglimento.Causale = "" Then
                myXML = myXML & "<MotivoIngresso>ND</MotivoIngresso>" & vbNewLine
            Else
                Dim DecodificaCausale As New Cls_CausaliEntrataUscita


                DecodificaCausale.Codice = UltimoAccoglimento.Causale
                DecodificaCausale.LeggiCausale(DbC.Ospiti)

                myXML = myXML & "<MotivoIngresso>" & DecodificaCausale.Descrizione & "</MotivoIngresso>" & vbNewLine
            End If


            Dim UltimaUscitaDef As New Cls_Movimenti


            UltimaUscitaDef.CodiceOspite = campodb(myPOSTreader("CodiceOspite"))
            UltimaUscitaDef.CENTROSERVIZIO = campodb(myPOSTreader("Cserv"))
            'UltimaUscitaDef.Data = DateSerial(Val(campodb(myPOSTreader("Anno"))), Val(campodb(myPOSTreader("Mese"))), Val(campodb(myPOSTreader("Giorno"))))
            UltimaUscitaDef.Data = Nothing
            UltimaUscitaDef.UltimaDataUscitaDefinitiva(DbC.Ospiti)


            If IsNothing(UltimaUscitaDef.Data) Then
                myXML = myXML & "<DataDimissione>ND</DataDimissione>" & vbNewLine
            Else
                If Format(UltimoAccoglimento.Data, "yyyyMMdd") <= Format(UltimaUscitaDef.Data, "yyyyMMdd") Then
                    myXML = myXML & "<DataDimissione>" & Format(UltimaUscitaDef.Data, "dd/MM/yyyy") & "</DataDimissione>" & vbNewLine
                Else
                    myXML = myXML & "<DataDimissione>ND</DataDimissione>" & vbNewLine
                End If
            End If
            If UltimaUscitaDef.Causale = "" Then
                myXML = myXML & "<MotivoDimissione>ND</MotivoDimissione>" & vbNewLine
            Else
                Dim DecodificaCausale As New Cls_CausaliEntrataUscita


                DecodificaCausale.Codice = UltimaUscitaDef.Causale
                DecodificaCausale.LeggiCausale(DbC.Ospiti)

                myXML = myXML & "<MotivoIngresso>" & DecodificaCausale.Descrizione & "</MotivoIngresso>" & vbNewLine
            End If


            Dim ComuneResidenza As New ClsComune

            ComuneResidenza.Descrizione = ""
            ComuneResidenza.Provincia = DatiOspite.RESIDENZAPROVINCIA1
            ComuneResidenza.Comune = DatiOspite.RESIDENZACOMUNE1
            ComuneResidenza.Leggi(DbC.Ospiti)
            If ComuneResidenza.Descrizione = "" Then
                myXML = myXML & "<ComuneResidenza>ND</ComuneResidenza>" & vbNewLine
            Else
                myXML = myXML & "<ComuneResidenza>" & ComuneResidenza.Descrizione & "</ComuneResidenza>" & vbNewLine
            End If

            myXML = myXML & "<IndirizzoResidenza>" & DatiOspite.RESIDENZAINDIRIZZO1 & "</IndirizzoResidenza>" & vbNewLine

            myXML = myXML & "<CapResidenza>" & DatiOspite.RESIDENZACAP1 & "</CapResidenza>" & vbNewLine


            myXML = myXML & "<CartellaClinica>" & DatiOspite.CartellaClinica & "</CartellaClinica>" & vbNewLine


            myXML = myXML & "<ProgressivoAccoglimento>" & Format(UltimoAccoglimento.CodiceOspite, "00000") & Format(UltimoAccoglimento.Progressivo, "000") & "</ProgressivoAccoglimento>" & vbNewLine


            Dim Parente As New Cls_Parenti
            Parente.Nome = ""

            Parente.CodiceOspite = campodb(myPOSTreader("CodiceOspite"))
            Parente.CodiceParente = 1
            Parente.Leggi(DbC.Ospiti, Parente.CodiceOspite, Parente.CodiceParente)

            If Parente.Nome <> "" Then
                myXML = myXML & "<Parente1Nome>" & Crypt.aesEncrypt(Parente.Nome, "0bScUr3@Dv3n1@Sf0rS3CuR1Ty") & "</Parente1Nome>" & vbNewLine

                If Parente.CODICEFISCALE = "" Then
                    myXML = myXML & "<Parente1CF>ND</Parente1CF>" & vbNewLine
                Else
                    Dim codicefiscaleP As String = Crypt.aesEncrypt(Parente.CODICEFISCALE, "0bScUr3@Dv3n1@Sf0rS3CuR1Ty")

                    myXML = myXML & "<Parente1CF>" & codicefiscaleP & "</Parente1CF>" & vbNewLine
                End If

                If Parente.RESIDENZAINDIRIZZO1 = "" Then
                    myXML = myXML & "<Parente1ResidenzaIndirizzo>ND</Parente1ResidenzaIndirizzo>" & vbNewLine
                Else
                    myXML = myXML & "<Parente1ResidenzaIndirizzo>" & Parente.RESIDENZAINDIRIZZO1 & "</Parente1ResidenzaIndirizzo>" & vbNewLine
                End If

                If Parente.RESIDENZACOMUNE1 = "" Then
                    myXML = myXML & "<Parente1ResidenzaComune>ND</Parente1ResidenzaComune>" & vbNewLine
                Else

                    ComuneResidenza.Descrizione = ""
                    ComuneResidenza.Provincia = Parente.RESIDENZAPROVINCIA1
                    ComuneResidenza.Comune = Parente.RESIDENZACOMUNE1
                    ComuneResidenza.Leggi(DbC.Ospiti)

                    myXML = myXML & "<Parente1ResidenzaComune>" & ComuneResidenza.Descrizione & "</Parente1ResidenzaComune>" & vbNewLine
                End If

                If Parente.RESIDENZACAP1 = "" Then
                    myXML = myXML & "<Parente1ResidenzaCap>ND</Parente1ResidenzaCap>" & vbNewLine
                Else
                    myXML = myXML & "<Parente1ResidenzaCap>" & Parente.RESIDENZACAP1 & "</Parente1ResidenzaCap>" & vbNewLine
                End If
            Else
                myXML = myXML & "<Parente1Nome>ND</Parente1Nome>" & vbNewLine
                myXML = myXML & "<Parente1CF>ND</Parente1CF>" & vbNewLine
                myXML = myXML & "<Parente1ResidenzaIndirizzo>ND</Parente1ResidenzaIndirizzo>" & vbNewLine
                myXML = myXML & "<Parente1ResidenzaComune>ND</Parente1ResidenzaComune>" & vbNewLine
                myXML = myXML & "<Parente1ResidenzaCap>ND</Parente1ResidenzaCap>" & vbNewLine
            End If


            Parente.Nome = ""
            Parente.CodiceOspite = campodb(myPOSTreader("CodiceOspite"))
            Parente.CodiceParente = 2
            Parente.Leggi(DbC.Ospiti, Parente.CodiceOspite, Parente.CodiceParente)

            If Parente.Nome <> "" Then
                myXML = myXML & "<Parente2Nome>" & Parente.Nome & "</Parente2Nome>" & vbNewLine

                If Parente.CODICEFISCALE = "" Then
                    myXML = myXML & "<Parente2CF>ND</Parente2CF>"
                Else
                    Dim codicefiscaleP As String = Crypt.aesEncrypt(Parente.CODICEFISCALE, "0bScUr3@Dv3n1@Sf0rS3CuR1Ty")


                    myXML = myXML & "<Parente2CF>" & codicefiscaleP & "</Parente2CF>" & vbNewLine
                End If

                If Parente.RESIDENZAINDIRIZZO1 = "" Then
                    myXML = myXML & "<Parente2ResidenzaIndirizzo>ND</Parente2ResidenzaIndirizzo>" & vbNewLine
                Else
                    myXML = myXML & "<Parente2ResidenzaIndirizzo>" & Parente.RESIDENZAINDIRIZZO1 & "</Parente2ResidenzaIndirizzo>" & vbNewLine
                End If

                If Parente.RESIDENZACOMUNE1 = "" Then
                    myXML = myXML & "<Parente2ResidenzaComune>ND</Parente2ResidenzaComune>" & vbNewLine
                Else

                    ComuneResidenza.Descrizione = ""
                    ComuneResidenza.Provincia = Parente.RESIDENZAPROVINCIA1
                    ComuneResidenza.Comune = Parente.RESIDENZACOMUNE1
                    ComuneResidenza.Leggi(DbC.Ospiti)

                    myXML = myXML & "<Parente2ResidenzaComune>" & ComuneResidenza.Descrizione & "</Parente2ResidenzaComune>" & vbNewLine
                End If

                If Parente.RESIDENZACAP1 = "" Then
                    myXML = myXML & "<Parente2ResidenzaCap>ND</Parente2ResidenzaCap>" & vbNewLine
                Else
                    myXML = myXML & "<Parente2ResidenzaCap>" & Parente.RESIDENZACAP1 & "</Parente2ResidenzaCap>" & vbNewLine
                End If
            Else
                myXML = myXML & "<Parente2Nome>ND</Parente2Nome>" & vbNewLine
                myXML = myXML & "<Parente2CF>ND</Parente2CF>" & vbNewLine
                myXML = myXML & "<Parente2ResidenzaIndirizzo>ND</Parente2ResidenzaIndirizzo>" & vbNewLine
                myXML = myXML & "<Parente2ResidenzaComune>ND</Parente2ResidenzaComune>" & vbNewLine
                myXML = myXML & "<Parente2ResidenzaCap>ND</Parente2ResidenzaCap>" & vbNewLine
            End If


            Dim Letto As New Cls_MovimentiStanze

            Letto.Letto = ""
            Letto.CodiceOspite = campodb(myPOSTreader("CodiceOspite"))
            Letto.UltimoMovimentoOspite(DbC.Ospiti)

            If Letto.Letto = "" Then
                myXML = myXML & "<Letto>ND</Letto>" & vbNewLine
            Else
                myXML = myXML & "<Letto>" & Letto.Villa & Letto.Reparto & Letto.Piano & Letto.Reparto & Letto.Stanza & Letto.Letto & "</Letto>" & vbNewLine
            End If

            myXML = myXML & "</righe>" & vbNewLine


            tw.Write(myXML)

        Loop
        myPOSTreader.Close()

        myXML = "</tabella>" & vbNewLine


        tw.Write(myXML)

        tw.Close()

        Dim AppoggioNomeFile As String = "Xml_" & DbC.RagioneSociale & "_" & CSERV & "_" & ANNO & "_" & MESE & ".xml"


        If DbC.Ospiti.ToUpper.IndexOf("SMARIAMONTE".ToUpper) >= 0 Then
            UploadFile(NomeFile, "ftp://194.79.58.9/AdveniasFDG/bi/" & AppoggioNomeFile, "AdveniasFtpUser", "R35upTf541?")

        End If
        If DbC.Ospiti.ToUpper.IndexOf("GNOCCHI".ToUpper) >= 0 Then
            UploadFile(NomeFile, "ftp://194.79.58.9/AdveniasFDG/bi/" & AppoggioNomeFile, "AdveniasFtpUser", "R35upTf541?")
        End If

        If DbC.Ospiti.ToUpper.IndexOf("SantaMariaProvvidenza".ToUpper) >= 0 Then
            UploadFile(NomeFile, "ftp://194.79.58.9/AdveniasFDG/bi/" & AppoggioNomeFile, "AdveniasFtpUser", "R35upTf541?")
        End If

        If DbC.Ospiti.ToUpper.IndexOf("PoloSpRiabilitativo".ToUpper) >= 0 Then
            UploadFile(NomeFile, "ftp://194.79.58.9/AdveniasFDG/bi/" & AppoggioNomeFile, "AdveniasFtpUser", "R35upTf541?")
        End If

        If DbC.Ospiti.ToUpper.IndexOf("RonzoniVilla".ToUpper) >= 0 Then
            UploadFile(NomeFile, "ftp://194.79.58.9/AdveniasFDG/bi/" & AppoggioNomeFile, "AdveniasFtpUser", "R35upTf541?")
        End If


    End Sub





    Public Sub UploadFile(ByVal _FileName As String, ByVal _UploadPath As String, ByVal _FTPUser As String, ByVal _FTPPass As String)
        Dim _FileInfo As New System.IO.FileInfo(_FileName)

        ' Create FtpWebRequest object from the Uri provided
        Dim _FtpWebRequest As System.Net.FtpWebRequest = CType(System.Net.FtpWebRequest.Create(New Uri(_UploadPath)), System.Net.FtpWebRequest)

        ' Provide the WebPermission Credintials
        _FtpWebRequest.Credentials = New System.Net.NetworkCredential(_FTPUser, _FTPPass)

        ' By default KeepAlive is true, where the control connection is not closed
        ' after a command is executed.
        _FtpWebRequest.KeepAlive = False

        ' set timeout for 20 seconds
        _FtpWebRequest.Timeout = 20000

        ' Specify the command to be executed.
        _FtpWebRequest.Method = System.Net.WebRequestMethods.Ftp.UploadFile

        ' Specify the data transfer type.
        _FtpWebRequest.UseBinary = True

        ' Notify the server about the size of the uploaded file
        _FtpWebRequest.ContentLength = _FileInfo.Length

        ' The buffer size is set to 2kb
        Dim buffLength As Integer = 2048
        Dim buff(buffLength - 1) As Byte

        ' Opens a file stream (System.IO.FileStream) to read the file to be uploaded
        Dim _FileStream As System.IO.FileStream = _FileInfo.OpenRead()


        ' Stream to which the file to be upload is written
        Dim _Stream As System.IO.Stream = _FtpWebRequest.GetRequestStream()

        ' Read from the file stream 2kb at a time
        Dim contentLen As Integer = _FileStream.Read(buff, 0, buffLength)

        ' Till Stream content ends
        Do While contentLen <> 0
            ' Write Content from the file stream to the FTP Upload Stream
            _Stream.Write(buff, 0, contentLen)
            contentLen = _FileStream.Read(buff, 0, buffLength)
        Loop

        ' Close the file stream and the Request Stream
        _Stream.Close()
        _Stream.Dispose()
        _FileStream.Close()
        _FileStream.Dispose()

    End Sub




    Public ReadOnly Property IsReusable() As Boolean Implements IHttpHandler.IsReusable
        Get
            Return False
        End Get
    End Property


    Function campodb(ByVal oggetto As Object) As String
        If IsDBNull(oggetto) Then
            Return ""
        Else
            Return oggetto
        End If
    End Function


    '
    Function campodbXML(ByVal oggetto As Object) As String
        If IsDBNull(oggetto) Then
            Return ""
        Else
            Dim Appoggio As String = oggetto

            Return Appoggio.ToString.Replace(">", "").Replace("<", "")
        End If
    End Function


    Function campodbD(ByVal oggetto As Object) As Date
        If IsDBNull(oggetto) Then
            Return Nothing
        Else
            Return oggetto
        End If
    End Function

    Private Function SplitWords(ByVal s As String) As String()
        '
        ' Call Regex.Split function from the imported namespace.
        ' Return the result array.
        '
        Return Regex.Split(s, "\W+")
    End Function



End Class