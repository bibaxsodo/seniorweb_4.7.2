﻿
Partial Class OspitiWeb_GestioneMedici
    Inherits System.Web.UI.Page

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Trim(Session("UTENTE")) = "" Then
            Response.Redirect("../Login.aspx")
            Exit Sub
        End If

        If Page.IsPostBack = True Then
            Exit Sub
        End If

        
        Dim K1 As New Cls_SqlString

        Dim Barra As New Cls_BarraSenior

        If Not IsNothing(Session("RicercaAnagraficaSQLString")) Then
            K1 = Session("RicercaAnagraficaSQLString")
        End If

        Lbl_BarraSenior.Text = Barra.CodiceBarra(Application("SENIOR"), Session("UTENTE"), Page, K1)

        If Val(Request.Item("CODICE")) = 0 Then
            Exit Sub
        End If


        Dim Med As New Cls_Medici

        Med.CodiceMedico = Request.Item("CODICE")
        Med.Leggi(Session("DC_OSPITE"))
        Txt_CodiceMedico.Text = Med.CodiceMedico
        Txt_Nome.Text = Med.Nome
        Txt_CodiceFiscale.Text = Med.CodiceFiscale
        Txt_Indirizzo.Text = Med.RESIDENZAINDIRIZZO1

        Dim DeCom As New ClsComune
        DeCom.Comune = Med.RESIDENZACOMUNE1
        DeCom.Provincia = Med.RESIDENZAPROVINCIA1
        DeCom.DecodficaComune(Session("DC_OSPITE"))
        Txt_ComRes.Text = DeCom.Descrizione

        Txt_Telefono.Text = Med.Telefono1
        Txt_TelefonoCellulare.Text = Med.Telefono2
        Txt_Specializzazione.Text = Med.Specializazione
        Txt_Note.Text = Med.Note

        Txt_CodiceMedico.Enabled = False

        Dim Log As New Cls_LogPrivacy
        Log.LogPrivacy(Application("SENIOR"), Session("CLIENTE"), Session("UTENTE"), Session("UTENTEIMPER"), Page.GetType.Name.ToUpper, 0, 0, "", "", 0, Txt_Nome.Text, "V", "MEDICO", "")


    End Sub

    

    
    Protected Sub Btn_Modifica_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles Btn_Modifica.Click
        Dim Med As New Cls_Medici
        If Txt_CodiceMedico.Text.Trim = "" Then
            ClientScript.RegisterClientScriptBlock(Me.GetType(), "Errore", "VisualizzaErrore('Specificare codice');", True)
            Exit Sub
        End If
        If Txt_Nome.Text.Trim = "" Then
            ClientScript.RegisterClientScriptBlock(Me.GetType(), "Errore", "VisualizzaErrore('Specificare cognome nome');", True)
            Exit Sub
        End If


        If Request.Item("CODICE") = "" Then
            Dim Log As New Cls_LogPrivacy
            Log.LogPrivacy(Application("SENIOR"), Session("CLIENTE"), Session("UTENTE"), Session("UTENTEIMPER"), Page.GetType.Name.ToUpper, 0, 0, "", "", 0, Txt_Nome.Text, "I", "MEDICO", "")
        Else

            Dim Log As New Cls_LogPrivacy
            Dim ConvT As New Cls_DataTableToJson
            Dim OldTable As New System.Data.DataTable("tabellaOld")


            Dim OldDatiPar As New Cls_Medici

            OldDatiPar.CodiceMedico = Txt_CodiceMedico.Text
            OldDatiPar.Leggi(Session("DC_OSPITE"))
            Dim AppoggioJS As String


            AppoggioJS = ConvT.SerializeObject(OldDatiPar)

            Log.LogPrivacy(Application("SENIOR"), Session("CLIENTE"), Session("UTENTE"), Session("UTENTEIMPER"), Page.GetType.Name.ToUpper, Session("CODICEOSPITE"), 0, "", "", 0, Txt_Nome.Text, "M", "MEDICO", AppoggioJS)

        End If

        Med.CodiceMedico = Txt_CodiceMedico.Text
        Med.Nome = Txt_Nome.Text
        Med.CodiceFiscale = Txt_CodiceFiscale.Text
        Med.RESIDENZAINDIRIZZO1 = Txt_Indirizzo.Text

        Dim Vettore(100) As String

        If Txt_ComRes.Text <> "" Then
            Vettore = SplitWords(Txt_ComRes.Text)
            If Vettore.Length > 1 Then
                Med.RESIDENZAPROVINCIA1 = Vettore(0)
                Med.RESIDENZACOMUNE1 = Vettore(1)
            End If
        End If


        Med.Telefono1 = Txt_Telefono.Text
        Med.Telefono2 = Txt_TelefonoCellulare.Text
        Med.Specializazione = Txt_Specializzazione.Text
        Med.Note = Txt_Note.Text
        Med.Scrivi(Session("DC_OSPITE"))

        Call PaginaPrecedente()
    End Sub



    Private Function SplitWords(ByVal s As String) As String()
        '
        ' Call Regex.Split function from the imported namespace.
        ' Return the result array.
        '
        Return Regex.Split(s, "\W+")
    End Function

    Protected Sub ImageButton1_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles ImageButton1.Click
        If Txt_CodiceMedico.Text.Trim = "" Then
            ClientScript.RegisterClientScriptBlock(Me.GetType(), "Errore", "VisualizzaErrore('Specificare codice');", True)
            Exit Sub
        End If

        Dim MyMed As New Cls_Medici
        MyMed.CodiceMedico = Txt_CodiceMedico.Text

        If MyMed.VerificaElimina(Session("DC_OSPITE")) = False Then
            ClientScript.RegisterClientScriptBlock(Me.GetType(), "Errore", "VisualizzaErrore('Non posso utilizzare codice usate');", True)
            Exit Sub
        End If


        Dim Log As New Cls_LogPrivacy
        Dim ConvT As New Cls_DataTableToJson
        Dim OldTable As New System.Data.DataTable("tabellaOld")


        Dim OldDatiPar As New Cls_Medici

        OldDatiPar.CodiceMedico = Txt_CodiceMedico.Text
        OldDatiPar.Leggi(Session("DC_OSPITE"))
        Dim AppoggioJS As String


        AppoggioJS = ConvT.SerializeObject(OldDatiPar)

        Log.LogPrivacy(Application("SENIOR"), Session("CLIENTE"), Session("UTENTE"), Session("UTENTEIMPER"), Page.GetType.Name.ToUpper, Session("CODICEOSPITE"), 0, "", "", 0, Txt_Nome.Text, "D", "MEDICO", AppoggioJS)


        MyMed.Elimina(Session("DC_OSPITE"))

        Call PaginaPrecedente()
    End Sub

    Protected Sub Btn_Esci_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles Btn_Esci.Click

        Call PaginaPrecedente()
    End Sub
    Private Sub PaginaPrecedente()
        Response.Redirect("ElencoMedici.aspx")
    End Sub
End Class

