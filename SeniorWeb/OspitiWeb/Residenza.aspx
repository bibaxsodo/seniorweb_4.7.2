﻿<%@ Page Language="VB" AutoEventWireup="false" Inherits="Residenza" EnableEventValidation="false" CodeFile="Residenza.aspx.vb" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="xasp" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <meta http-equiv="x-ua-compatible" content="IE=9" />
    <title>Recapiti - Residenza</title>
    <asp:PlaceHolder runat="server">
        <%: System.Web.Optimization.Styles.Render("~/Content/AjaxControlToolkit/Styles/Bundle") %>
    </asp:PlaceHolder>
    <link href="ospiti.css?ver=11" rel="stylesheet" type="text/css" />
    <link rel="shortcut icon" href="images/SENIOR.ico" />
    <link href="js/jquery.autocomplete.css" rel="stylesheet" type="text/css" />

    <script src="js/jquery-1.5.1.min.js" type="text/javascript"></script>
    <script src="js/jquery.autocomplete.js" type="text/javascript"></script>
    <script src="js/soapclient.js" type="text/javascript"></script>
    <script src="/js/convertinumero.js" type="text/javascript"></script>

    <script src="js/JSErrore.js" type="text/javascript"></script>

    <script src="js/jquery.maskedinput-1.3.min.js" type="text/javascript"></script>
    <script src="/js/formatnumer.js" type="text/javascript"></script>

    <script type="text/javascript" src="../js/menuanagraficaospiti.js?ver=7"></script>


    <link rel="stylesheet" href="dialogbox/redips-dialog.css" type="text/css" media="screen" />
    <script type="text/javascript" src="dialogbox/redips-dialog-min.js"></script>
    <script type="text/javascript" src="dialogbox/script.js"></script>

    <link rel="stylesheet" href="jqueryui/jquery-ui.css" type="text/css" />
    <script src="jqueryui/jquery-ui.js" type="text/javascript"></script>
    <script src="jqueryui/datapicker-it.js" type="text/javascript"></script>
    <script type="text/javascript">
        function DialogBox(Path) {

            REDIPS.dialog.show(500, 300, '<iframe id="output" src="' + Path + '" height="300px" width="500"></iframe>');
            return false;

        }

        $(document).ready(function () {
            $('html').keyup(function (event) {

                if (event.keyCode == 113) {
                    __doPostBack("ImageButton1", "0");
                }

            });
        });

        $(document).ready(function () {
            if (window.innerHeight > 0) { $("#BarraLaterale").css("height", (window.innerHeight - 94) + "px"); } else { $("#BarraLaterale").css("height", (document.documentElement.offsetHeight - 94) + "px"); }
        });
    </script>
</head>
<body>
    <form id="form1" runat="server">
        <asp:ScriptManager ID="ScriptManager1" runat="server" EnableScriptGlobalization="true">
            <Scripts>
                <asp:ScriptReference Path="~/Scripts/AjaxControlToolkit/Bundle" />
            </Scripts>
        </asp:ScriptManager>
        <asp:Label ID="Lbl_BarraSenior" runat="server" Text=""></asp:Label>
        <div align="left">
            <table style="width: 100%;" cellpadding="0" cellspacing="0">
                <tr>
                    <td style="width: 160px; background-color: #F0F0F0;"></td>
                    <td>
                        <div class="Titolo">Ospiti - Residenza/Recapito</div>
                        <div class="SottoTitoloOSPITE">
                            <asp:Label ID="Lbl_NomeOspite" runat="server"></asp:Label><br />
                            <br />
                        </div>
                    </td>
                    <td style="text-align: right;">
                        <div class="DivTastiOspite">
                            <asp:ImageButton runat="server" ImageUrl="~/images/salva.jpg" class="EffettoBottoniTondi" Height="38px" ToolTip="Modifica / Inserisci (F2)" ID="ImageButton1"></asp:ImageButton>
                            <asp:ImageButton runat="server" ImageUrl="~/images/goright.png" class="EffettoBottoniTondi" Height="38px" ToolTip="Storicizza indirizzi" ID="ImgStoricizza"></asp:ImageButton>
                        </div>
                    </td>

                </tr>
                <tr>
                    <td style="width: 160px; background-color: #F0F0F0; vertical-align: top; text-align: center;" id="BarraLaterale">
                        <asp:ImageButton ID="Btn_Esci" runat="server" BackColor="Transparent" ImageUrl="../images/Menu_Indietro.png" class="Effetto" ToolTip="Chiudi" />
                        <a href="Menu_Ospiti.aspx" style="border-width: 0px;">
                            <img src="images/Home.jpg" alt="Menù" class="Effetto" /></a><br />
                        <div id="MENUDIV"></div>

                        <br />
                        <br />
                        <br />
                        <br />
                        <br />
                        <br />
                        <br />
                        <br />
                    </td>
                    <td colspan="2" style="background-color: #FFFFFF; vertical-align: top;">
                        <xasp:TabContainer ID="TabContainer1" runat="server" ActiveTabIndex="0" CssClass="TabSenior"
                            Width="100%" BorderStyle="None" Style="margin-right: 39px">
                            <xasp:TabPanel runat="server" HeaderText="Prima Nota" ID="Tab_Anagrafica">
                                <HeaderTemplate>
                                    Residenza/Recapito      
                                </HeaderTemplate>
                                <ContentTemplate>
                                    <label class="LabelCampo">Data :</label>
                                    <asp:TextBox ID="Txt_Data1" MaxLength="10" runat="server" Width="90px"></asp:TextBox>
                                    <br />
                                    <br />
                                    <label class="LabelCampo">Indirizzo :</label>
                                    <asp:TextBox ID="Txt_Indirizzo1" MaxLength="100" runat="server" Width="440px"></asp:TextBox>
                                    <br />
                                    <br />
                                    <label class="LabelCampo">Comune :</label>
                                    <asp:TextBox ID="Txt_Comune1" MaxLength="80" runat="server" Width="440px"></asp:TextBox><br />
                                    <br />
                                    <label class="LabelCampo">CAP :</label>
                                    <asp:TextBox ID="Txt_Cap1" MaxLength="5" onkeypress="return soloNumeri(event);" runat="server" Width="64px"></asp:TextBox>
                                    <br />
                                    <br />
                                    <label class="LabelCampo">Telefono :</label>
                                    <asp:TextBox ID="Txt_Telefono1" MaxLength="50" runat="server" Width="440px"></asp:TextBox>
                                    <br />
                                    <br />
                                    <label class="LabelCampo">Cellulare :</label>
                                    <asp:TextBox ID="Txt_Telefono2" MaxLength="50" runat="server" Width="440px"></asp:TextBox>
                                    <br />
                                    <br />
                                    <label class="LabelCampo">Mail :</label>
                                    <asp:TextBox ID="Txt_Telefono3" MaxLength="150" runat="server" Width="440px"></asp:TextBox>
                                    <br />
                                    <br />
                                    <label class="LabelCampo">Fax :</label>
                                    <asp:TextBox ID="Txt_Telefono4" MaxLength="50" runat="server" Width="440px"></asp:TextBox>
                                    <br />
                                    <br />
                                    <br />
                                    <asp:Button ID="Bnt_TT" runat="server" Text="Recupera da Epersonam" Visible="false" />
                                    <br />
                                    <br />
                                </ContentTemplate>
                            </xasp:TabPanel>


                            <xasp:TabPanel runat="server" HeaderText="Prima Nota" ID="TabPanel2">
                                <HeaderTemplate>
                                    Dati Storicizzati
                                </HeaderTemplate>
                                <ContentTemplate>
                                    <label class="LabelCampo">Data :</label>
                                    <asp:TextBox ID="Txt_Data2" MaxLength="10" runat="server" Width="90px"></asp:TextBox>
                                    <br />
                                    <br />
                                    <label class="LabelCampo">Indirizzo :</label>
                                    <asp:TextBox ID="Txt_Indirizzo2" MaxLength="100" runat="server" Width="440px"></asp:TextBox><br />
                                    <br />
                                    <label class="LabelCampo">Comune :</label>
                                    <asp:TextBox ID="Txt_Comune2" runat="server" Width="440px"></asp:TextBox><br />
                                    <br />
                                    <label class="LabelCampo">CAP :</label>
                                    <asp:TextBox ID="Txt_Cap2" MaxLength="5" onkeypress="return soloNumeri(event);" runat="server" Width="64px"></asp:TextBox>
                                    <br />
                                    <br />
                                    <br />
                                    <label class="LabelCampo">Data :</label>
                                    <asp:TextBox ID="Txt_Data3" MaxLength="10" runat="server" Width="90px"></asp:TextBox>
                                    <br />
                                    <br />
                                    <label class="LabelCampo">Indirizzo :</label>
                                    <asp:TextBox ID="Txt_Indirizzo3" MaxLength="50" runat="server" Width="440px"></asp:TextBox><br />
                                    <br />
                                    <label class="LabelCampo">Comune :</label>
                                    <asp:TextBox ID="Txt_Comune3" runat="server" Width="438px"></asp:TextBox><br />
                                    <br />
                                    <label class="LabelCampo">CAP :</label>
                                    <asp:TextBox ID="Txt_Cap3" MaxLength="5" onkeypress="return soloNumeri(event);" runat="server" Width="64px"></asp:TextBox><br />
                                    <br />
                                    <br />
                                </ContentTemplate>
                            </xasp:TabPanel>

                            <xasp:TabPanel runat="server" HeaderText="Prima Nota" ID="TabPanel3">
                                <HeaderTemplate>
                                    Destinatario Fattura      
                                </HeaderTemplate>
                                <ContentTemplate>
                                    <label class="LabelCampo">Nome :</label>
                                    <asp:TextBox ID="Txt_Nome" MaxLength="100" runat="server" Width="440px"></asp:TextBox>
                                    <br />
                                    <br />
                                    <label class="LabelCampo">Indirizzo :</label>
                                    <asp:TextBox ID="Txt_Indirizzo" MaxLength="100" runat="server" Width="440px"></asp:TextBox><br />
                                    <br />
                                    <label class="LabelCampo">Comune :</label>
                                    <asp:TextBox ID="Txt_Comune" MaxLength="100" runat="server" Width="440px"></asp:TextBox><br />
                                    <br />
                                    <label class="LabelCampo">CAP :</label>
                                    <asp:TextBox ID="Txt_Cap4" MaxLength="5" onkeypress="return soloNumeri(event);" runat="server" Width="64px"></asp:TextBox><br />
                                    <br />

                                    <label class="LabelCampo">Telefono :</label>
                                    <asp:TextBox ID="Txt_Telefono" MaxLength="16" runat="server" Width="440px"></asp:TextBox>
                                    <br />
                                    <br />
                                    <br />
                                    <label class="LabelCampo">Mail :</label>
                                    <asp:TextBox ID="Txt_Telefono4_Des" MaxLength="50" runat="server" Width="440px"></asp:TextBox>
                                    <br />
                                    <br />
                                    <br />
                                    <asp:Button ID="Btn_RecuperaEperesonam" runat="server" Text="Recupera da Epersonam" Visible="false" />
                                    <br />
                                    <br />
                                </ContentTemplate>
                            </xasp:TabPanel>
                        </xasp:TabContainer>
                    </td>
                </tr>
            </table>
        </div>
    </form>
</body>
</html>
