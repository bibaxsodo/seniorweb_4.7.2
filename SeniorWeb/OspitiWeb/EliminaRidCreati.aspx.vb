﻿Imports Microsoft.VisualBasic
Imports System.Data.OleDb
Imports System.Web.Hosting
Imports System.Diagnostics
Imports System.ComponentModel
Imports System.Threading
Imports System.IO
Imports System.IO.Compression
Partial Class OspitiWeb_EliminaRidCreati
    Inherits System.Web.UI.Page

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load

        If Page.IsPostBack = True Then
            Exit Sub
        End If

        Dim K1 As New Cls_SqlString

        Dim Barra As New Cls_BarraSenior

        If Not IsNothing(Session("RicercaAnagraficaSQLString")) Then
            K1 = Session("RicercaAnagraficaSQLString")
        End If

        Lbl_BarraSenior.Text = Barra.CodiceBarra(Application("SENIOR"), Session("UTENTE"), Page, K1)
        Dim cn As OleDbConnection



        cn = New Data.OleDb.OleDbConnection(Session("DC_GENERALE"))

        cn.Open()


        Dim Cmd As New OleDbCommand

        Cmd.CommandText = "select DATARID,IdRid  from MOVIMENTICONTABILITESTA where not DATARID is null and not (IdRid = '' or IdRid is null) group by DATARID,IdRid  Order by DATARID desc"
        Cmd.Connection = cn
        Cmd.ExecuteNonQuery()
        Dim myPOSTreader As OleDbDataReader = Cmd.ExecuteReader()
        Do While myPOSTreader.Read
            DD_SelezionaInvia.Items.Add(Format(campodbD(myPOSTreader.Item("DATARID")), "dd/MM/yyyy") & " " & campodb(myPOSTreader.Item("IdRid")))
            DD_SelezionaInvia.Items(DD_SelezionaInvia.Items.Count - 1).Value = Format(campodbD(myPOSTreader.Item("DATARID")), "dd/MM/yyyy") & " " & campodb(myPOSTreader.Item("IdRid"))
        Loop
        cn.Close()
    End Sub




    Function campodbD(ByVal oggetto As Object) As Date
        If IsDBNull(oggetto) Then
            Return Nothing
        Else
            Return oggetto
        End If
    End Function
    Function campodb(ByVal oggetto As Object) As String
        If IsDBNull(oggetto) Then
            Return ""
        Else
            Return oggetto
        End If
    End Function

    Function campodbN(ByVal oggetto As Object) As Double
        If IsDBNull(oggetto) Then
            Return 0
        Else
            Return oggetto
        End If
    End Function

    Protected Sub Btn_Elimina_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles Btn_Elimina.Click
        If DD_SelezionaInvia.SelectedValue = "" Then
            ClientScript.RegisterClientScriptBlock(Me.GetType(), "visualizzaA", "VisualizzaErrore('Devi indicare il flusso da cancellare');", True)
            Exit Sub
        End If

        Dim cn As OleDbConnection



        cn = New Data.OleDb.OleDbConnection(Session("DC_GENERALE"))

        cn.Open()

        Dim CmdRead As New OleDbCommand

        CmdRead.CommandText = "select * From  MOVIMENTICONTABILITESTA  where DATARID = ? and IdRid = ?"
        CmdRead.Connection = cn
        CmdRead.Parameters.AddWithValue("@DataRid", Mid(DD_SelezionaInvia.SelectedValue, 1, 10))
        CmdRead.Parameters.AddWithValue("@IdRid", Mid(DD_SelezionaInvia.SelectedValue, 12, Len(DD_SelezionaInvia.SelectedValue) - 11))
        Dim ReadRid As OleDbDataReader = CmdRead.ExecuteReader()
        Do While ReadRid.Read


            Dim Log As New Cls_LogPrivacy

            Log.LogPrivacy(Application("SENIOR"), Session("CLIENTE"), Session("UTENTE"), Session("UTENTEIMPER"), Page.GetType.Name.ToUpper, 0, 0, "", "", campodbN(ReadRid.Item("NumeroRegistrazione")), "", "M", "DOCUMENTO", "")


            Dim Cmd As New OleDbCommand

            Cmd.CommandText = "update MOVIMENTICONTABILITESTA set DataRid = Null, IdRid = Null where NumeroRegistrazione = ? "
            Cmd.Connection = cn
            Cmd.Parameters.AddWithValue("@NumeroRegistrazione", campodbN(ReadRid.Item("NumeroRegistrazione")))
            Cmd.ExecuteNonQuery()



        Loop
        ReadRid.Close()
        cn.Close()


        ScriptManager.RegisterClientScriptBlock(Me, Me.GetType(), "ESEGUITO", "alert('Eliminazione effettuata');", True)
    End Sub

    Protected Sub Btn_Esci_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles Btn_Esci.Click
        Response.Redirect("Menu_Strumenti.aspx")
    End Sub
End Class
