﻿Imports Microsoft.VisualBasic
Imports System.Data.OleDb
Imports System.Web.Hosting
Partial Class OspitiWeb_EstraiPerUSLSanitario
    Inherits System.Web.UI.Page
    Dim Tabella As New System.Data.DataTable("tabella")

    Protected Sub OspitiWeb_EstraiPerUSLSanitario_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load

        If Page.IsPostBack = True Then Exit Sub

        RB_RegioneCserv.Checked = True

        Elabora()
    End Sub


    Private Sub Elabora()
        Dim Stampa As New StampeOspiti
        Dim Cserv As String = ""
        Dim Regione As String = ""
        Dim ComuneResidenza As String = ""

        Stampa = Session("stampa")


        'Dim Stampa1 As New StampeOspiti

        'For i = 0 To Stampa.RendicontoUSl.Rows.Count - 1
        '    Dim myriga As System.Data.DataRow = Stampa1.RendicontoUSl.NewRow
        '    For x = 0 To Stampa.RendicontoUSl.Columns.Count - 1
        '        myriga(x) = Stampa.RendicontoUSl.Rows(i).Item(x)
        '    Next
        '    Stampa1.RendicontoUSl.Rows.Add(myriga)
        'Next

        'Session("Salvastampa1") = Stampa1




        Dim view As System.Data.DataView = Stampa.Tables("RendicontoUSl").DefaultView
        view.Sort = "CODICESERVIZIO,REGIONE,ComuneResidenza"

        view.Table.Select("", "CODICESERVIZIO,REGIONE,ComuneResidenza")
        
        Tabella.Clear()
        Tabella.Columns.Add("CentroServizio", GetType(String))
        Tabella.Columns.Add("Regione", GetType(String))

        Tabella.Columns.Add("Decodifica CentroServizio", GetType(String))
        Tabella.Columns.Add("Decodifica Regione", GetType(String))
        Tabella.Columns.Add("Decodifica Comune", GetType(String))


        Dim DataTable As New System.Data.DataTable

        DataTable = Stampa.Tables("RendicontoUSl")

        Dim rowList As System.Data.DataRow() = DataTable.Select("", "CODICESERVIZIO,REGIONE,ComuneResidenza")

        For Each Riga As System.Data.DataRow In rowList


            If RB_RegioneCserv.Checked Then
                If Riga.Item("REGIONE").ToString <> Regione Or Riga.Item("CODICESERVIZIO").ToString <> Cserv Then
                    Regione = Riga.Item("REGIONE").ToString()
                    Cserv = Riga.Item("CODICESERVIZIO").ToString
                    Dim myriga As System.Data.DataRow = Tabella.NewRow()

                    myriga(0) = Cserv
                    myriga(1) = Regione

                    Dim DecCserv As New Cls_CentroServizio

                    DecCserv.CENTROSERVIZIO = Cserv
                    DecCserv.Leggi(Session("DC_OSPITE"), Cserv)

                    myriga(2) = DecCserv.DESCRIZIONE

                    Dim USL As New ClsUSL

                    USL.CodiceRegione = Regione
                    USL.DecodificaRegione(Session("DC_OSPITE"), USL.CodiceRegione)

                    myriga(3) = USL.Nome

                    Tabella.Rows.Add(myriga)

                End If
            End If
            If RB_RegioneCservComune.Checked Then
                If Riga.Item("REGIONE").ToString <> Regione Or Riga.Item("ComuneResidenza").ToString <> ComuneResidenza Or Riga.Item("CODICESERVIZIO").ToString <> Cserv Then
                    Regione = Riga.Item("REGIONE").ToString()
                    Cserv = Riga.Item("CODICESERVIZIO").ToString
                    ComuneResidenza = Riga.Item("ComuneResidenza").ToString

                    Dim Importo As Double

                    Importo = 0

                    If Not IsDBNull(Riga.Item("IMPREGIONE")) Then
                        Importo = Riga.Item("IMPREGIONE")
                    End If

                    If Importo > 0 Then

                        Dim myriga As System.Data.DataRow = Tabella.NewRow()

                        myriga(0) = Cserv
                        myriga(1) = Regione

                        Dim DecCserv As New Cls_CentroServizio

                        DecCserv.CENTROSERVIZIO = Cserv
                        DecCserv.Leggi(Session("DC_OSPITE"), Cserv)

                        myriga(2) = DecCserv.DESCRIZIONE

                        Dim USL As New ClsUSL

                        USL.CodiceRegione = Regione
                        USL.DecodificaRegione(Session("DC_OSPITE"), USL.CodiceRegione)

                        myriga(3) = USL.Nome


                        myriga(4) = ComuneResidenza

                        Tabella.Rows.Add(myriga)
                    End If
                End If
            End If
        Next


        Grd_ImportoOspite.DataSource = Tabella
        Grd_ImportoOspite.DataBind()
        ViewState("Appoggio") = Tabella
    End Sub

    Protected Sub Grd_ImportoOspite_RowCommand(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewCommandEventArgs) Handles Grd_ImportoOspite.RowCommand
        If e.CommandName = "Seleziona" Then


            Dim d As Integer


            Tabella = ViewState("Appoggio")

            d = Val(e.CommandArgument)


            Dim Cserv As String = ""
            Dim Regione As String = ""
            Dim ComuneResidenza As String

            Cserv = Tabella.Rows(d).Item(0).ToString
            Regione = Tabella.Rows(d).Item(1).ToString

            ComuneResidenza = Tabella.Rows(d).Item(4).ToString


            Dim Stampa As New StampeOspiti


            If RB_RegioneCserv.Checked Then
                Session("SelectionFormula") = "({RendicontoUSl.GIORNIPRESENZA} >0  Or {RendicontoUSl.GIORNIASSENZA} >0)  And {RendicontoUSl.REGIONE} = " & Chr(34) & Regione & Chr(34) & " AND {RendicontoUSl.CODICESERVIZIO} = " & Chr(34) & Cserv & Chr(34)

            Else
                Session("SelectionFormula") = "({RendicontoUSl.GIORNIPRESENZA} >0  Or {RendicontoUSl.GIORNIASSENZA} >0)    And {RendicontoUSl.REGIONE} = " & Chr(34) & Regione & Chr(34) & " AND {RendicontoUSl.CODICESERVIZIO} = " & Chr(34) & Cserv & Chr(34) & " AND {RendicontoUSl.ComuneResidenza} = " & Chr(34) & ComuneResidenza & Chr(34)

            End If



            If Request.Item("REPORT") = "" Then
                ScriptManager.RegisterClientScriptBlock(Me, Me.GetType(), "Stampa_New", "window.open('Stampa_ReportXSD.aspx?REPORT=RENDICONTOREGIONE&FORMULA=SI','StampeRendiconto_2','width=800,height=600');", True)
            Else
                ScriptManager.RegisterClientScriptBlock(Me, Me.GetType(), "Stampa_New", "window.open('Stampa_ReportXSD.aspx?REPORT=" & Request.Item("REPORT") & "&FORMULA=SI','StampeRendiconto_2','width=800,height=600');", True)
            End If

        End If

        If e.CommandName = "Export" Then


            Dim d As Integer


            Tabella = ViewState("Appoggio")

            d = Val(e.CommandArgument)


            Dim Cserv As String = ""
            Dim Regione As String = ""
            Dim ComuneResidenza As String


            Cserv = Tabella.Rows(d).Item(0).ToString
            Regione = Tabella.Rows(d).Item(1).ToString
            ComuneResidenza = Tabella.Rows(d).Item(4).ToString


            Dim Stampa As New StampeOspiti

            If RB_RegioneCserv.Checked Then
                Session("SelectionFormula") = "({RendicontoUSl.GIORNIPRESENZA} >0  Or {RendicontoUSl.GIORNIASSENZA} >0)   And {RendicontoUSl.REGIONE} = " & Chr(34) & Regione & Chr(34) & " AND {RendicontoUSl.CODICESERVIZIO} = " & Chr(34) & Cserv & Chr(34)
            Else
                Session("SelectionFormula") = "({RendicontoUSl.GIORNIPRESENZA} >0  Or {RendicontoUSl.GIORNIASSENZA} >0)   And {RendicontoUSl.REGIONE} = " & Chr(34) & Regione & Chr(34) & " AND {RendicontoUSl.CODICESERVIZIO} = " & Chr(34) & Cserv & Chr(34) & " AND {RendicontoUSl.ComuneResidenza} = " & Chr(34) & ComuneResidenza & Chr(34)

            End If


            If Request.Item("REPORT") = "" Then
                ScriptManager.RegisterClientScriptBlock(Me, Me.GetType(), "Stampa_New", "window.open('Stampa_ReportXSD.aspx?REPORT=RENDICONTOREGIONE&FORMULA=SI&EXPORT=PDF&NOME=" & Cserv & Regione & "','StampeRendiconto_2','width=700,height=100,top=300');", True)
            Else
                ScriptManager.RegisterClientScriptBlock(Me, Me.GetType(), "Stampa_New", "window.open('Stampa_ReportXSD.aspx?REPORT=" & Request.Item("REPORT") & "&FORMULA=SI&EXPORT=PDF&NOME=" & Cserv & Regione & "','StampeRendiconto_2','width=700,height=100,top=300');", True)
            End If

        End If
    End Sub

    
    Protected Overrides Sub Finalize()
        MyBase.Finalize()
    End Sub

    Protected Sub RB_RegioneCserv_CheckedChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles RB_RegioneCserv.CheckedChanged
        Elabora()
    End Sub

    Protected Sub RB_RegioneCservComune_CheckedChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles RB_RegioneCservComune.CheckedChanged
        Elabora()
    End Sub
End Class
