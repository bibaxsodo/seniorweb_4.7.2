﻿<%@ Page Language="VB" AutoEventWireup="false" Inherits="OspitiWeb_GestioneListino" CodeFile="GestioneListino.aspx.vb" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="xasp" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
    <meta http-equiv="x-ua-compatible" content="IE=9" />
    <title>Listino</title>
    <asp:PlaceHolder runat="server">
        <%: System.Web.Optimization.Styles.Render("~/Content/AjaxControlToolkit/Styles/Bundle") %>
    </asp:PlaceHolder>
    <link href="ospiti.css?ver=11" rel="stylesheet" type="text/css" />
    <link rel="shortcut icon" href="images/SENIOR.ico" />
    <link href="js/jquery.autocomplete.css" rel="stylesheet" type="text/css" />

    <script src="js/jquery-1.5.1.min.js" type="text/javascript"></script>
    <script src="js/jquery.autocomplete.js" type="text/javascript"></script>
    <script src="js/soapclient.js" type="text/javascript"></script>
    <script src="/js/convertinumero.js" type="text/javascript"></script>


    <script src="js/chosen/chosen.jquery.js" type="text/javascript"></script>
    <script src="js/chosen/docsupport/prism.js" type="text/javascript" charset="utf-8"></script>
    <link rel="stylesheet" href="js/chosen/docsupport/style.css">
    <link rel="stylesheet" href="js/chosen/docsupport/prism.css">
    <link rel="stylesheet" href="js/chosen/chosen.css">


    <script src="js/jquery.maskedinput-1.3.min.js" type="text/javascript"></script>
    <script src="/js/formatnumer.js" type="text/javascript"></script>
    <script src="/js/pianoconti.js" type="text/javascript"></script>
    <script src="js/JSErrore.js" type="text/javascript"></script>
    <link rel="stylesheet" href="jqueryui/jquery-ui.css" type="text/css" />
    <script src="jqueryui/jquery-ui.js" type="text/javascript"></script>
    <script src="jqueryui/datapicker-it.js" type="text/javascript"></script>
    <style>
        .Sfondo {
            background-color: White;
            visibility: hidden;
            position: absolute;
            top: 650px;
        }
    </style>
    <script type="text/javascript">
        function ChiudiTutti() {
            $("#idOspite").css('visibility', 'hidden');
            $("#idSanitario").css('visibility', 'hidden');
            $("#idSociale").css('visibility', 'hidden');
            $("#idJolly").css('visibility', 'hidden');
            $("#IdParente1").css('visibility', 'hidden');
            $("#IdParente2").css('visibility', 'hidden');
            $("#IdParente3").css('visibility', 'hidden');
            $("#IdParente4").css('visibility', 'hidden');
            $("#IdParente5").css('visibility', 'hidden');
            $("#idTotale").css('visibility', 'hidden');
            $("#idSalaOperatoria").css('visibility', 'hidden');
        }

        function OpenRettaTotale() {
            ChiudiTutti();
            if ($("#idTotale").css("visibility") != "hidden") {
                $("#idTotale").css('visibility', 'hidden');
            } else {
                $("#idTotale").css('visibility', 'visible');
            }
        }
        function OpenOspite() {
            ChiudiTutti();
            if ($("#idOspite").css("visibility") != "hidden") {
                $("#idOspite").css('visibility', 'hidden');
            } else {
                $("#idOspite").css('visibility', 'visible');
            }
        }
        function OpenSanitario() {
            ChiudiTutti();
            if ($("#idSanitario").css("visibility") != "hidden") {
                $("#idSanitario").css('visibility', 'hidden');
            } else {
                $("#idSanitario").css('visibility', 'visible');
            }
        }
        function OpenSociale() {
            ChiudiTutti();
            if ($("#idSociale").css("visibility") != "hidden") {
                $("#idSociale").css('visibility', 'hidden');
            } else {
                $("#idSociale").css('visibility', 'visible');
            }
        }
        function OpenJolly() {
            ChiudiTutti();
            if ($("#idJolly").css("visibility") != "hidden") {
                $("#idJolly").css('visibility', 'hidden');
            } else {
                $("#idJolly").css('visibility', 'visible');
            }
        }


        function OpenidSalaOperatoria() {
            ChiudiTutti();
            if ($("#idSalaOperatoria").css("visibility") != "hidden") {
                $("#idSalaOperatoria").css('visibility', 'hidden');
            } else {
                $("#idSalaOperatoria").css('visibility', 'visible');
            }
        }

        function OpenParente1() {
            ChiudiTutti();
            if ($("#IdParente1").css("visibility") != "hidden") {
                $("#IdParente1").css('visibility', 'hidden');
            } else {
                $("#IdParente1").css('visibility', 'visible');
            }
        }
        function OpenParente2() {
            ChiudiTutti();
            if ($("#IdParente2").css("visibility") != "hidden") {
                $("#IdParente2").css('visibility', 'hidden');
            } else {
                $("#IdParente2").css('visibility', 'visible');
            }
        }
        function OpenParente3() {
            ChiudiTutti();
            if ($("#IdParente3").css("visibility") != "hidden") {
                $("#IdParente3").css('visibility', 'hidden');
            } else {
                $("#IdParente3").css('visibility', 'visible');
            }
        }
        function OpenParente4() {
            ChiudiTutti();
            if ($("#IdParente4").css("visibility") != "hidden") {
                $("#IdParente4").css('visibility', 'hidden');
            } else {
                $("#IdParente4").css('visibility', 'visible');
            }
        }
        function OpenParente5() {
            ChiudiTutti();
            if ($("#IdParente5").css("visibility") != "hidden") {
                $("#IdParente5").css('visibility', 'hidden');
            } else {
                $("#IdParente5").css('visibility', 'visible');
            }
        }


        $(document).ready(function () {
            $('html').keyup(function (event) {

                if (event.keyCode == 113) {
                    __doPostBack("Imb_Modifica", "0");
                }

            });
        });

        $(document).ready(function () {
            if (window.innerHeight > 0) { $("#BarraLaterale").css("height", (window.innerHeight - 94) + "px"); } else { $("#BarraLaterale").css("height", (document.documentElement.offsetHeight - 94) + "px"); }
        });
    </script>

</head>
<body>
    <form id="form1" runat="server">
        <asp:ScriptManager ID="ScriptManager1" runat="server" EnableScriptGlobalization="true">
            <Scripts>
                <asp:ScriptReference Path="~/Scripts/AjaxControlToolkit/Bundle" />
            </Scripts>
        </asp:ScriptManager>
        <asp:Label ID="Lbl_BarraSenior" runat="server" Text=""></asp:Label>
        <div style="text-align: left;">
            <table style="width: 100%;" cellpadding="0" cellspacing="0">
                <tr>
                    <td style="width: 160px; background-color: #F0F0F0;"></td>
                    <td>
                        <div class="Titolo">Ospiti - Tabelle - Listino</div>
                        <div class="SottoTitolo">
                            <br />
                            <br />
                        </div>
                    </td>
                    <td style="text-align: right;">
                        <div class="DivTasti">
                            <asp:ImageButton runat="server" ImageUrl="~/images/iconaospite.png" CssClass="EffettoBottoniTondi" Height="38px" ToolTip="Modifica In Sanitaria Privata" ID="Img_SanitariaPrivata" Visible="false"></asp:ImageButton>&nbsp;
			<asp:ImageButton runat="server" ImageUrl="~/images/duplica.png" CssClass="EffettoBottoniTondi" Height="38px" ToolTip="Duplica Causale" ID="Imb_Duplica"></asp:ImageButton>&nbsp;
			<asp:ImageButton runat="server" ImageUrl="~/images/salva.jpg" CssClass="EffettoBottoniTondi" Height="38px" ToolTip="Modifica / Inserisci (F2)" ID="Imb_Modifica"></asp:ImageButton>
                            <asp:ImageButton runat="server" OnClientClick="return window.confirm('Eliminare?');" ImageUrl="~/images/elimina.jpg" CssClass="EffettoBottoniTondi" Height="38px" ToolTip="Elimina" ID="ImageButton2"></asp:ImageButton>
                        </div>
                    </td>
                </tr>

                <tr>
                    <td style="width: 160px; background-color: #F0F0F0; vertical-align: top; text-align: center;" id="BarraLaterale">
                        <a href="Menu_Tabelle.aspx" style="border-width: 0px;">
                            <img src="images/Home.jpg" alt="Menù" class="Effetto" /></a><br />
                        <asp:ImageButton ID="Btn_Esci" runat="server" BackColor="Transparent" ImageUrl="../images/Menu_Indietro.png" ToolTip="Chiudi" />
                        <br />
                        <br />
                        <br />
                        <br />
                        <br />
                        <br />
                        <br />
                        <br />
                        <br />
                    </td>
                    <td colspan="2" style="background-color: #FFFFFF;" valign="top">
                        <xasp:TabContainer ID="TabContainer1" runat="server" ActiveTabIndex="0" CssClass="TabSenior"
                            Width="100%" BorderStyle="None" Style="margin-right: 39px">
                            <xasp:TabPanel runat="server" HeaderText="Prima Nota" ID="Tab_Anagrafica">
                                <HeaderTemplate>
                                    Listino
                                </HeaderTemplate>
                                <ContentTemplate>

                                    <label class="LabelCampo">Codice :</label>
                                    <asp:TextBox ID="Txt_Codice" AutoPostBack="true" runat="server" Width="49px"></asp:TextBox>
                                    <asp:Image ID="Img_VerificaCod" runat="server" Height="18px" ImageUrl="~/images/Blanco.png" Width="18px" />

                                    <br />

                                    <br />
                                    <hr />
                                    <br />
                                    <label class="LabelCampo">Struttura:</label>
                                    <asp:DropDownList runat="server" ID="DD_Struttura" AutoPostBack="true"></asp:DropDownList><br />
                                    <br />

                                    <label class="LabelCampo">Centro Servizio : </label>
                                    <asp:DropDownList ID="Cmb_CServ" runat="server" Width="288px"></asp:DropDownList><br />
                                    <br />
                                    <hr />
                                    <br />
                                    <label class="LabelCampo">Descrizione :</label>
                                    <asp:TextBox ID="Txt_Descrizione" runat="server" Width="483px"></asp:TextBox>
                                    <asp:Image ID="Img_VerificaDes" runat="server" Height="18px" ImageUrl="~/images/Blanco.png" Width="18px" />
                                    <br />
                                    <br />
                                    <label class="LabelCampo">Data Scadenza :</label>
                                    <asp:TextBox ID="Txt_DataScadenza" runat="server" Width="110px"></asp:TextBox><br />
                                    <br />
                                    <label class="LabelCampo">Codice Epersonam :</label>
                                    <asp:TextBox ID="Txt_CodiceEPersonam" runat="server" Width="49px"></asp:TextBox><br />
                                    <br />

                                    <asp:Panel ID="Pan_GiorniSettimana" runat="server">
                                        <label class="LabelCampo">Presenza Settimana Ospite:</label>
                                        <asp:CheckBox ID="Chk_Lunedi" runat="server" Text="L" />
                                        <asp:CheckBox ID="Chk_Martedi" runat="server" Text="M" />
                                        <asp:CheckBox ID="Chk_Mercoledi" runat="server" Text="M" />
                                        <asp:CheckBox ID="Chk_Giovedi" runat="server" Text="G" />
                                        <asp:CheckBox ID="Chk_Venerdi" runat="server" Text="V" />
                                        <asp:CheckBox ID="Chk_Sabato" runat="server" Text="S" />
                                        <asp:CheckBox ID="Chk_Domenica" runat="server" Text="D" />
                                    </asp:Panel>
                                    <br />
                                    <br />
                                    <br />

                                    <a href="#" onclick="OpenRettaTotale();">
                                        <img src="../images/rettatotale.png" id="IdRettaTotale" /></a>
                                    <a href="#" onclick="OpenOspite();">
                                        <img src="../images/iconaospite.png" id="IdRettaOspite" /></a>
                                    <a href="#" onclick="OpenSanitario();">
                                        <img src="../images/sanitaria.png" id="IdRettaSanitario" /></a>
                                    <a href="#" onclick="OpenSociale();">
                                        <img src="../images/sociale.png" id="IdRettaSociale" /></a>
                                    <a href="#" onclick="OpenJolly();">
                                        <img src="../images/jolly.png" id="IdRettaJolly" /></a>

                                    <a href="#" onclick="OpenidSalaOperatoria();">
                                        <img src="../images/sanitaria.png" id="idSalaOp" /></a>


                                    <br />
                                    <br />
                                    <br />
                                    <div id="idTotale" class="Sfondo">

                                        <label class="LabelCampo">Retta Totale :</label>
                                        <asp:TextBox ID="Txt_ImportoRettaTotale" Style="text-align: right;" Width="100px" runat="server"></asp:TextBox><br />
                                        <br />
                                        <label class="LabelCampo">Tipo Retta Totale :</label>
                                        <asp:DropDownList ID="DD_TipoRettaTotale" runat="server">
                                            <asp:ListItem Value="G">Giornaliero</asp:ListItem>
                                            <asp:ListItem Value="M">Mensile</asp:ListItem>
                                            <asp:ListItem Value="A">Annuale</asp:ListItem>
                                        </asp:DropDownList><br />
                                        <br />
                                        <label class="LabelCampo">Tipo Retta :</label>
                                        <asp:DropDownList ID="DD_TipoRetta" runat="server">
                                        </asp:DropDownList><br />
                                        <br />

                                        <label class="LabelCampo">Modalità  :</label>
                                        <asp:RadioButton ID="RB_Ospite" runat="server" GroupName="TIPO" Text="Ospite" />&nbsp;    
              <asp:RadioButton ID="RB_Parente" runat="server" GroupName="TIPO" Text="Parente" />
                                        <asp:RadioButton ID="RB_Comune" runat="server" GroupName="TIPO" Text="Comune" />
                                        <asp:RadioButton ID="RB_Ente" runat="server" GroupName="TIPO" Text="Ente" /><br />
                                        <br />
                                        <label class="LabelCampo">Extra Fissi :</label>
                                        <asp:DropDownList ID="DD_ExtraFisso1" runat="server"></asp:DropDownList><asp:Button ID="Btn_Plus1" runat="server" Text="+" />
                                        <br />
                                        <label class="LabelCampo">&nbsp;</label>
                                        <asp:DropDownList ID="DD_ExtraFisso2" runat="server" Visible="false"></asp:DropDownList><asp:Button ID="Btn_Plus2" runat="server" Text="+" Visible="false" />
                                        <br />
                                        <label class="LabelCampo">&nbsp;</label>
                                        <asp:DropDownList ID="DD_ExtraFisso3" runat="server" Visible="false"></asp:DropDownList><asp:Button ID="Btn_Plus3" runat="server" Text="+" Visible="false" />
                                        <br />
                                        <label class="LabelCampo">&nbsp;</label>
                                        <asp:DropDownList ID="DD_ExtraFisso4" runat="server" Visible="false"></asp:DropDownList><asp:Button ID="Btn_Plus4" runat="server" Text="+" Visible="false" />
                                        <br />
                                        <label class="LabelCampo">&nbsp;</label>
                                        <asp:DropDownList ID="DD_ExtraFisso5" runat="server" Visible="false"></asp:DropDownList><asp:Button ID="Btn_Plus5" runat="server" Text="+" Visible="false" />
                                        <br />
                                        <label class="LabelCampo">&nbsp;</label>
                                        <asp:DropDownList ID="DD_ExtraFisso6" runat="server" Visible="false"></asp:DropDownList><asp:Button ID="Btn_Plus6" runat="server" Text="+" Visible="false" />
                                        <br />
                                        <label class="LabelCampo">&nbsp;</label>
                                        <asp:DropDownList ID="DD_ExtraFisso7" runat="server" Visible="false"></asp:DropDownList><asp:Button ID="Btn_Plus7" runat="server" Text="+" Visible="false" />
                                        <br />
                                        <label class="LabelCampo">&nbsp;</label>
                                        <asp:DropDownList ID="DD_ExtraFisso8" runat="server" Visible="false"></asp:DropDownList><asp:Button ID="Btn_Plus8" runat="server" Text="+" Visible="false" />
                                        <br />
                                        <label class="LabelCampo">&nbsp;</label>
                                        <asp:DropDownList ID="DD_ExtraFisso9" runat="server" Visible="false"></asp:DropDownList><asp:Button ID="Btn_Plus9" runat="server" Text="+" Visible="false" />
                                        <br />
                                        <label class="LabelCampo">&nbsp;</label>
                                        <asp:DropDownList ID="DD_ExtraFisso10" runat="server" Visible="false"></asp:DropDownList><asp:Button ID="Btn_Plus10" runat="server" Text="+" Visible="false" />
                                        <br />
                                    </div>

                                    <div id="idSanitario" class="Sfondo">
                                        <label class="LabelCampo">Codice Regione da Residenza :</label>
                                        <asp:CheckBox ID="Chk_CodRegDaComRes" runat="server" Text="" />
                                        <br />
                                        <br />
                                        <br />
                                        <label class="LabelCampo">Sanitario :</label>
                                        <asp:DropDownList ID="DD_Usl" class="chosen-select" runat="server"></asp:DropDownList><br />
                                        <br />
                                        <label class="LabelCampo">Tipo Retta:</label>
                                        <asp:DropDownList ID="DD_SanitarioTipoRetta" class="chosen-select" runat="server"></asp:DropDownList><br />
                                        <br />

                                    </div>


                                    <div id="idOspite" class="Sfondo">

                                        <asp:Panel ID="Pan_OspiteSOC" runat="server">
                                            <label class="LabelCampo">Tipo Operazione Ospite :</label>
                                            &nbsp;&nbsp;<asp:DropDownList ID="DD_TipoOperazione" runat="server" Width="248px">
                                            </asp:DropDownList><br />
                                            <br />
                                            <label class="LabelCampo">IVA Ospite:</label>
                                            &nbsp;&nbsp;<asp:DropDownList ID="DD_IVA" runat="server" Width="176px"></asp:DropDownList><br />
                                            <br />
                                            <label class="LabelCampo">Anticipata :</label>
                                            <asp:CheckBox ID="Chk_Anticipata" runat="server" Text="" /><br />
                                            <br />

                                            <label class="LabelCampo">Modalità Pagamento Ospite :</label>
                                            &nbsp;&nbsp;<asp:DropDownList ID="DD_ModalitaPagamento" runat="server" Width="248px">
                                            </asp:DropDownList><br />
                                            <br />

                                            <label class="LabelCampo">Compensazione Ospite:</label>
                                            &nbsp;&nbsp;<asp:RadioButton ID="RB_SI" runat="server" GroupName="compesazione" Text="SI" Checked />
                                            <asp:RadioButton ID="RB_NO" runat="server" GroupName="compesazione" Text="NO" />
                                            <asp:RadioButton ID="RB_Dettaglio" runat="server" GroupName="compesazione" Text="Compensa in base al tipo extra" /><br />
                                            <br />
                                            <label class="LabelCampo">Periodo :</label>
                                            <asp:RadioButton ID="RB_Mensile" runat="server" GroupName="Periodo" Text="M" Checked="true" />
                                            <asp:RadioButton ID="RB_Bimestrale" runat="server" GroupName="Periodo" Text="B" />
                                            <asp:RadioButton ID="RB_Trimestrale" runat="server" GroupName="Periodo" Text="T" />
                                            <asp:RadioButton ID="RB_FinePeriodoAss" runat="server" GroupName="Periodo" Text="F" /><br />
                                            <br />

                                            <label class="LabelCampo">
                                                <asp:Label ID="Lbl_ImportoOspite1" runat="server" Text="Importo1"></asp:Label></label>
                                            <asp:TextBox ID="Txt_ImportoOspite" Style="text-align: right;" Width="100px" runat="server"></asp:TextBox>
                                            Tipo Retta Ospite :
              <asp:DropDownList ID="DD_TipoRettaOspite" runat="server">
                  <asp:ListItem Value="G">Giornaliero</asp:ListItem>
                  <asp:ListItem Value="M">Mensile</asp:ListItem>
                  <asp:ListItem Value="A">Annuale</asp:ListItem>
              </asp:DropDownList><br />
                                            <br />
                                        </asp:Panel>
                                        <label class="LabelCampo">
                                            <asp:Label ID="Lbl_ImportoOspite2" runat="server" Text="Importo1"></asp:Label></label>
                                        <asp:TextBox ID="Txt_ImportoOspite2" Style="text-align: right;" Width="100px" runat="server"></asp:TextBox><br />
                                        <br />
                                    </div>

                                    <div id="idSociale" class="Sfondo">
                                        <label class="LabelCampo">Sociale :</label>
                                        <asp:DropDownList ID="DD_Comune" runat="server" class="chosen-select"></asp:DropDownList>
                                        <br />
                                        <br />
                                        <label class="LabelCampo">Retta Sociale :</label>
                                        &nbsp;&nbsp;<asp:TextBox ID="Txt_ImportoSociale" Style="text-align: right;" Width="100px" runat="server"></asp:TextBox>
                                        Tipo Retta Sociale :
              <asp:DropDownList ID="DD_TipoRettaSociale" runat="server">
                  <asp:ListItem Value="G">Giornaliero</asp:ListItem>
                  <asp:ListItem Value="M">Mensile</asp:ListItem>
                  <asp:ListItem Value="A">Annuale</asp:ListItem>
              </asp:DropDownList><br />
                                        <br />
                                    </div>

                                    <div id="idJolly" class="Sfondo">
                                        <label class="LabelCampo">Jolly :</label>
                                        <asp:DropDownList ID="DD_ComuneJolly" runat="server" class="chosen-select"></asp:DropDownList><br />
                                        <br />
                                        <label class="LabelCampo">Retta Jolly :</label>
                                        &nbsp;&nbsp;<asp:TextBox ID="Txt_Jolly" Style="text-align: right;" Width="100px" runat="server"></asp:TextBox>
                                        Tipo Retta Sociale :
              <asp:DropDownList ID="DD_TipoRettaJolly" runat="server">
                  <asp:ListItem Value="G">Giornaliero</asp:ListItem>
                  <asp:ListItem Value="M">Mensile</asp:ListItem>
                  <asp:ListItem Value="A">Annuale</asp:ListItem>
              </asp:DropDownList><br />
                                        <br />
                                    </div>
                                    <div id="idSalaOperatoria" class="Sfondo">
                                        <label class="LabelCampo">Ricoveri >0 :</label>
                                        <asp:TextBox ID="Txt_ImportoRicoveriMaggioreZero" Style="text-align: right;"  runat="server" Width="100px"></asp:TextBox>
                                        <br />
                                        <br />
                                        <label class="LabelCampo">Giornate Soglia :</label>
                                        <asp:TextBox ID="Txt_GiornateSoglia" runat="server" Style="text-align: right;"  Width="49px"></asp:TextBox>
                                        <br />
                                        <br />
                                        <label class="LabelCampo">Incremento Soglia :</label>
                                        <asp:TextBox ID="Txt_ImportoIncremento" runat="server" Style="text-align: right;"  Width="100px"></asp:TextBox>
                                        <br />
                                        <br />
                                        <label class="LabelCampo">Importo Diurno :</label>
                                        <asp:TextBox ID="Txt_ImportoDiurno" runat="server" Style="text-align: right;"  Width="100px"></asp:TextBox>
                                        <br />
                                    </div>

                                </ContentTemplate>
                            </xasp:TabPanel>
                        </xasp:TabContainer>
                    </td>
                </tr>
            </table>
        </div>
    </form>
</body>
</html>
