﻿Public Class DatiDiurno
    Implements System.Web.IHttpHandler, IRequiresSessionState
    Private Sub CaricamentoMeseStandard(ByVal StringaConnessione As String, ByVal CODOSP As Integer, ByVal Cserv As String, ByVal Anno As Integer, ByVal Mese As Integer, ByRef Giorni() As String)
        Dim Giorno As Integer = 0
        Dim TbCServ As New Cls_CentroServizio
        Dim SETTIMANA As String
        Dim SETTIMANAOspite As String

        Dim Gg As Integer
        TbCServ.Leggi(StringaConnessione, Cserv)

        Dim AnaOsp As New ClsOspite

        AnaOsp.Leggi(StringaConnessione, CODOSP)


        SETTIMANAOspite = AnaOsp.SETTIMANA

        Dim m As New Cls_DatiOspiteParenteCentroServizio

        m.CodiceOspite = CODOSP
        m.CodiceParente = 0
        m.CentroServizio = Cserv
        m.Leggi(StringaConnessione)
        If Trim(m.Settimana) <> "" Then
            SETTIMANAOspite = m.Settimana
        End If

        For Giorno = 1 To GiorniMese(Mese, Anno)

            Gg = Weekday(DateSerial(Anno, Mese, Giorno), vbMonday)
            If Trim(Mid(SETTIMANAOspite, Gg, 1)) <> "" Then
                Giorni(Giorno) = Mid(SETTIMANAOspite, Gg, 1)
            End If


            SETTIMANA = TbCServ.SETTIMANA

            Gg = Weekday(DateSerial(Anno, Mese, Giorno), vbMonday)
            If Trim(Mid(SETTIMANA, Gg, 1)) <> "" Then
                Giorni(Giorno) = "C"

            End If


            If Mese = 1 Then
                If TbCServ.GIORNO0101 <> "S" And Giorno = 1 Then
                    Giorni(Giorno) = "C"
                End If
                If TbCServ.GIORNO0601 = "S" And Giorno = 6 Then
                    Giorni(Giorno) = "C"
                End If
            End If
            If Mese = 3 Then
                If TbCServ.GIORNO1903 <> "S" And Giorno = 19 Then
                    Giorni(Giorno) = "C"
                End If
            End If
            If Mese = 4 Then
                If TbCServ.GIORNO2504 <> "S" And Giorno = 25 Then
                    Giorni(Giorno) = "C"
                End If
            End If
            If Mese = 5 Then
                If TbCServ.GIORNO0105 <> "S" And Giorno = 1 Then
                    Giorni(Giorno) = "C"
                End If
            End If

            If Mese = 6 Then
                If TbCServ.GIORNO0206 <> "S" And Giorno = 2 Then
                    Giorni(Giorno) = "C"
                End If
                If TbCServ.GIORNO2906 <> "S" And Giorno = 29 Then
                    Giorni(Giorno) = "C"
                End If
            End If
            If Mese = 8 Then
                If TbCServ.GIORNO1508 <> "S" And Giorno = 29 Then
                    Giorni(Giorno) = "C"
                End If
            End If
            If Mese = 11 Then
                If TbCServ.GIORNO0111 <> "S" And Giorno = 1 Then
                    Giorni(Giorno) = "C"
                End If
                If TbCServ.GIORNO0411 <> "S" And Giorno = 4 Then
                    Giorni(Giorno) = "C"
                End If
            End If
            If Mese = 12 Then
                If TbCServ.GIORNO0812 <> "S" And Giorno = 8 Then
                    Giorni(Giorno) = "C"
                End If
                If TbCServ.GIORNO2512 <> "S" And Giorno = 25 Then
                    Giorni(Giorno) = "C"
                End If
                If TbCServ.GIORNO2612 <> "S" And Giorno = 26 Then
                    Giorni(Giorno) = "C"
                End If
            End If
            If TbCServ.GIORNO1ATTIVO <> "S" Then
                If Not IsDBNull(TbCServ.GIORNO1) Then
                    If Val(Mid(TbCServ.GIORNO1, 4, 2)) = Mese Then
                        If Val(Mid(TbCServ.GIORNO1, 1, 2)) = Giorno Then
                            Giorni(Giorno) = "C"
                        End If
                    End If
                End If

                If TbCServ.GIORNO2ATTIVO <> "S" Then
                    If Not IsDBNull(TbCServ.GIORNO2) Then
                        If Val(Mid(TbCServ.GIORNO2, 4, 2)) = Mese Then
                            If Val(Mid(TbCServ.GIORNO2, 1, 2)) = Giorno Then
                                Giorni(Giorno) = "C"
                            End If
                        End If
                    End If
                End If


                If TbCServ.GIORNO3ATTIVO <> "S" Then
                    If Not IsDBNull(TbCServ.GIORNO3) Then
                        If Val(Mid(TbCServ.GIORNO3, 4, 2)) = Mese Then
                            If Val(Mid(TbCServ.GIORNO3, 1, 2)) = Giorno Then
                                Giorni(Giorno) = "C"
                            End If
                        End If
                    End If
                End If

                If TbCServ.GIORNO4ATTIVO <> "S" Then
                    If Not IsDBNull(TbCServ.GIORNO4) Then
                        If Val(Mid(TbCServ.GIORNO4, 4, 2)) = Mese Then
                            If Val(Mid(TbCServ.GIORNO4, 1, 2)) = Giorno Then
                                Giorni(Giorno) = "C"
                            End If
                        End If
                    End If
                End If

                If TbCServ.GIORNO5ATTIVO <> "S" Then
                    If Not IsDBNull(TbCServ.GIORNO5) Then
                        If Val(Mid(TbCServ.GIORNO5, 4, 2)) = Mese Then
                            If Val(Mid(TbCServ.GIORNO5, 1, 2)) = Giorno Then
                                Giorni(Giorno) = "C"
                            End If
                        End If
                    End If
                End If
            End If
        Next
    End Sub

    Public Function GiorniMese(ByVal Mese As Byte, ByVal Anno As Integer) As Byte
        If Mese = 1 Or Mese = 3 Or Mese = 5 Or Mese = 7 Or Mese = 8 Or
           Mese = 10 Or Mese = 12 Then
            GiorniMese = 31
        Else
            If Mese <> 2 Then
                GiorniMese = 30
            Else
                If Day(DateSerial(Anno, Mese, 29)) = 29 Then
                    GiorniMese = 29
                Else
                    GiorniMese = 28
                End If
            End If
        End If
    End Function
    Public Sub ProcessRequest(ByVal context As HttpContext) Implements IHttpHandler.ProcessRequest
        context.Response.ContentType = "text/plain"
        context.Response.Cache.SetCacheability(HttpCacheability.NoCache)
        Dim NumeroRegistrazione As String = context.Request.QueryString("NumeroRegistrazione")
        '-ms-filter: ""progid:DXImageTransform.Microsoft.Alpha(Opacity=70)"";
        '
        '

        Dim CSERV As String = context.Request.QueryString("CSERV")
        Dim CODOSP As String = context.Request.QueryString("CodiceOspite")

        Dim MESE As String = context.Request.QueryString("MESE")

        Dim ANNO As String = context.Request.QueryString("ANNO")

        Dim NumeroRighe As Integer = 0


        Dim Ospite As New ClsOspite

        Ospite.CodiceOspite = CODOSP
        Ospite.Leggi(context.Session("DC_OSPITE"), Ospite.CodiceOspite)


        Dim Tabella As String

        Tabella = "<table width=""100%"">"
        Tabella = Tabella & "<tr>"
        Tabella = Tabella & Ospite.Nome
        Tabella = Tabella & "<td width=""40%"" style=""border-bottom-style: solid; border-bottom-width: 1px;"">"
        Tabella = Tabella & "</td>"
        Tabella = Tabella & "<td width=""10%"" style=""text-align: right;border-bottom-style: solid; border-bottom-width: 1px;"">Causale"
        Tabella = Tabella & "</td>"

        Tabella = Tabella & "<td width=""40%"" style=""border-bottom-style: solid; border-bottom-width: 1px;"">"
        Tabella = Tabella & "</td>"
        Tabella = Tabella & "<td width=""10%"" style=""text-align: right;border-bottom-style: solid; border-bottom-width: 1px;"">Quantita"
        Tabella = Tabella & "</td>"
        Tabella = Tabella & "</tr>"


        Dim MeseP As New Cls_Diurno
        Dim Giorni(31) As String


        MeseP.Leggi(context.Session("DC_OSPITE"), CODOSP, CSERV, ANNO, MESE)
        If MeseP.Anno = 0 Then
            CaricamentoMeseStandard(context.Session("DC_OSPITE"), CODOSP, CSERV, ANNO, MESE, Giorni)
        Else

            Giorni(1) = MeseP.Giorno1
            Giorni(2) = MeseP.Giorno2
            Giorni(3) = MeseP.Giorno3
            Giorni(4) = MeseP.Giorno4
            Giorni(5) = MeseP.Giorno5
            Giorni(6) = MeseP.Giorno6
            Giorni(7) = MeseP.Giorno7
            Giorni(8) = MeseP.Giorno8
            Giorni(9) = MeseP.Giorno9
            Giorni(10) = MeseP.Giorno10
            Giorni(11) = MeseP.Giorno11
            Giorni(12) = MeseP.Giorno12
            Giorni(13) = MeseP.Giorno13
            Giorni(14) = MeseP.Giorno14
            Giorni(15) = MeseP.Giorno15
            Giorni(16) = MeseP.Giorno16
            Giorni(17) = MeseP.Giorno17
            Giorni(18) = MeseP.Giorno18
            Giorni(19) = MeseP.Giorno19
            Giorni(20) = MeseP.Giorno20
            Giorni(21) = MeseP.Giorno21
            Giorni(22) = MeseP.Giorno22
            Giorni(23) = MeseP.Giorno23
            Giorni(24) = MeseP.Giorno24
            Giorni(25) = MeseP.Giorno25
            Giorni(26) = MeseP.Giorno26
            Giorni(27) = MeseP.Giorno27
            Giorni(28) = MeseP.Giorno28
            Giorni(29) = MeseP.Giorno29
            Giorni(30) = MeseP.Giorno30
            Giorni(31) = MeseP.Giorno31
        End If


        Dim Causale(31) As String
        Dim Numero(31) As Integer
        Dim Causali As Integer = 0
        Dim MaxCausali As Integer = 0
        For Indice = 1 To GiorniMese(MESE, ANNO)
            For Causali = 0 To MaxCausali
                If Giorni(Indice) = Causale(Causali) Then
                    Numero(Causali) = Numero(Causali) + 1
                    Exit For
                End If
            Next
            If Causali > MaxCausali Then
                MaxCausali = MaxCausali + 1
                Causale(MaxCausali) = Giorni(Indice)
                Numero(MaxCausali) = 1
            End If
        Next

        For Indice = 0 To MaxCausali
            Dim DecodificaCausale As New Cls_CausaliEntrataUscita
            If Causale(Indice) = "" Then
                DecodificaCausale.Descrizione = "Presenze"
            Else
                DecodificaCausale.Descrizione = ""
                DecodificaCausale.Codice = Causale(Indice)
                DecodificaCausale.LeggiCausale(context.Session("DC_OSPITE"))
            End If
            Tabella = Tabella & "<tr>"
            Tabella = Tabella & "<td width=""40%"">" & DecodificaCausale.Descrizione & "</td>"
            Tabella = Tabella & "<td width=""10%"" style=""text-align: right;""></td>"

            Tabella = Tabella & "<td width=""40%"">"
            Tabella = Tabella & "</td>"
            Tabella = Tabella & "<td width=""10%"">"
            Tabella = Tabella & Numero(Indice)
            Tabella = Tabella & "</td >"
            Tabella = Tabella & "</tr>"
        Next


        Tabella = Tabella & "</table>"
        Dim altezza As String
        altezza = ((MaxCausali + 1) * 30) + 30

        context.Response.Write("<div id=""RegistrazioneViewer""  style=""position: absolute; -moz-box-shadow: 8px 6px 5px 5px #000000;-webkit-box-shadow: 8px 6px 5px 5px #000000;box-shadow: 8px 6px 5px 5px #000000; filter: alpha(opacity=70);-moz-opacity: 0.7;-khtml-opacity: 0.7;opacity: 0.7; width: 600px;height: " & altezza & "px; border: 2px #00117a solid;background-color: #2c2ddd; color: #ffffff;left:300px;"">" & Tabella & "</div>")

    End Sub

    Public ReadOnly Property IsReusable() As Boolean Implements IHttpHandler.IsReusable
        Get
            Return False
        End Get
    End Property

End Class