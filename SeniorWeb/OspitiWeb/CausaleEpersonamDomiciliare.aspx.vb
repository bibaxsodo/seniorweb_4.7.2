﻿Imports Microsoft.VisualBasic
Imports System.Data.OleDb
Imports System.Web.Hosting
Partial Class OspitiWeb_CausaleEpersonamDomiciliare
    Inherits System.Web.UI.Page

    Protected Sub Btn_Modifica_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles Btn_Modifica.Click
        If DD_Causale.SelectedValue = "" Then
            ClientScript.RegisterClientScriptBlock(Me.GetType(), "Errore", "VisualizzaErrore('Indicare Causael Senior');", True)
            Exit Sub
        End If
        If Txt_CausaleEPersonam.Text.Trim = "" Then
            ClientScript.RegisterClientScriptBlock(Me.GetType(), "Errore", "VisualizzaErrore('Indicare Causale E-Personam');", True)
            Exit Sub
        End If


        Dim cn As OleDbConnection
        Dim MySql As String = ""
        Dim Convenzione As String = ""


        If RB_NO.Checked = True Then
            Convenzione = "N"
        End If

        If RB_SI.Checked = True Then
            Convenzione = "S"
        End If


        If RB_Tutti.Checked = True Then
            Convenzione = ""
        End If

        cn = New Data.OleDb.OleDbConnection(Session("DC_OSPITE"))

        cn.Open()
        Dim cmd As New OleDbCommand()

        If Val(Txt_Id.Text) > 0 Then
            cmd.CommandText = ("Update DomiciliareEPersonam set activity_id = ?,CentroServizio = ?,TipologiaDomiciliare = ?,Convenzione = ?  Where id = " & Val(Txt_Id.Text))
        Else
            cmd.CommandText = ("Insert into DomiciliareEPersonam (activity_id,CentroServizio,TipologiaDomiciliare,Convenzione) VALUES (?,?,?,?)")

        End If
        'CausaleEPersonam,CentroServizio,CausaleOspiti
        cmd.Parameters.AddWithValue("@activity_id", Txt_CausaleEPersonam.Text)
        cmd.Parameters.AddWithValue("@CentroServizio", Cmb_CServ.SelectedValue)
        cmd.Parameters.AddWithValue("@TipologiaDomiciliare", DD_Causale.SelectedValue)
        cmd.Parameters.AddWithValue("@Convenzione", Convenzione)
        'Convenzione
        cmd.Connection = cn
        cmd.ExecuteNonQuery()

        cn.Close()

        Response.Redirect("Elenco_CausaliEpersonamDomiciliare.aspx")

    End Sub


    Private Function SplitWords(ByVal s As String) As String()
        '
        ' Call Regex.Split function from the imported namespace.
        ' Return the result array.
        '
        Return Regex.Split(s, "\W+")
    End Function

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load

        If Page.IsPostBack = True Then
            Exit Sub
        End If


        Dim K1 As New Cls_SqlString

        Dim Barra As New Cls_BarraSenior

        If Not IsNothing(Session("RicercaAnagraficaSQLString")) Then
            K1 = Session("RicercaAnagraficaSQLString")
        End If


        Dim K As New Cls_TipoDomiciliare

        K.UpDateDropBox(Session("DC_OSPITE"), DD_Causale)





        Dim kVilla As New Cls_TabelleDescrittiveOspitiAccessori


        kVilla.UpDateDropBox(Session("DC_OSPITIACCESSORI"), "VIL", DD_Struttura)
        If DD_Struttura.Items.Count = 1 Then
            Call AggiornaCServ()
        End If

        Lbl_BarraSenior.Text = Barra.CodiceBarra(Application("SENIOR"), Session("UTENTE"), Page, K1)

        Txt_Id.Enabled = False
        If Request.Item("CODICE") <> "" Then

            Dim cn As OleDbConnection
            Dim MySql As String = ""

            cn = New Data.OleDb.OleDbConnection(Session("DC_OSPITE"))

            cn.Open()
            Dim cmd As New OleDbCommand()
            cmd.CommandText = ("select * from DomiciliareEPersonam Where id = ?")

            cmd.Parameters.AddWithValue("@id", Val(Request.Item("CODICE")))
            cmd.Connection = cn


            Dim myPOSTreader As OleDbDataReader = cmd.ExecuteReader()
            If myPOSTreader.Read Then


                Txt_Id.Text = campodb(myPOSTreader.Item("Id"))
                Txt_CausaleEPersonam.Text = campodb(myPOSTreader.Item("activity_id"))

                Dim KCserv As New Cls_CentroServizio

                KCserv.CENTROSERVIZIO = campodb(myPOSTreader.Item("CentroServizio"))
                KCserv.Leggi(Session("DC_OSPITE"), campodb(myPOSTreader.Item("CentroServizio")))
                If KCserv.DESCRIZIONE <> "" Then

                    DD_Struttura.SelectedValue = KCserv.Villa
                    AggiornaCServ()
                    Cmb_CServ.SelectedValue = campodb(myPOSTreader.Item("CentroServizio"))
                End If

                DD_Causale.SelectedValue = campodb(myPOSTreader.Item("TipologiaDomiciliare"))

                RB_Tutti.Checked = True
                RB_SI.Checked = False
                RB_NO.Checked = False
            

                If campodb(myPOSTreader.Item("Convenzione")) = "S" Then
                    RB_Tutti.Checked = False
                    RB_SI.Checked = True
                End If

                If campodb(myPOSTreader.Item("Convenzione")) = "N" Then
                    RB_Tutti.Checked = False
                    RB_NO.Checked = True
                End If


            End If
            myPOSTreader.Close()

        End If

        Call EseguiJS()
    End Sub

    Private Sub EseguiJS()
        Dim MyJs As String
        MyJs = "$(document).ready(function() {"

        MyJs = MyJs & "var els = document.getElementsByTagName(""*"");"

        MyJs = MyJs & "for (var i=0;i<els.length;i++)"
        MyJs = MyJs & "if ( els[i].id ) { "
        MyJs = MyJs & " var appoggio =els[i].id; "
        MyJs = MyJs & " if (appoggio.match('TxtData')!= null) {  "
        MyJs = MyJs & " $(els[i]).mask(""99/99/9999"");"
        MyJs = MyJs & "    }"

        MyJs = MyJs & " if ((appoggio.match('TxtImporto')!= null) ) {  "
        MyJs = MyJs & " $(els[i]).keypress(function() { ForceNumericInput($(this).val(), true, true); } ); "
        MyJs = MyJs & " $(els[i]).blur(function() { var ap = formatNumber($(this).val(),2); $(this).val(ap); });"
        MyJs = MyJs & "    }"

        MyJs = MyJs & " if (appoggio.match('Txt_Sottoconto')!= null) {   "
        MyJs = MyJs & " $(els[i]).autocomplete('/WebHandler/GestioneConto.ashx?Utente=" & Session("UTENTE") & "', {delay:5,minChars:3});"
        MyJs = MyJs & "    }"

        MyJs = MyJs & "} "
        MyJs = MyJs & "});"

        'MyJs = "$(document).ready(function() { $('.myClass').keypress(function() { handleEnter($('.myClass').val(), true, true); } );     $('#" & Txt_ImportoPagato.ClientID & "').blur(function() { var ap = formatNumber ($('#" & Txt_ImportoPagato.ClientID & "').val(),2); $('#" & Txt_ImportoPagato.ClientID & "').val(ap); }); });"
        ScriptManager.RegisterStartupScript(Me, Me.GetType(), "visualizzaRitIm", MyJs, True)
    End Sub

    Protected Sub ImageButton1_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles ImageButton1.Click
        If DD_Causale.SelectedValue = "" Then
            ClientScript.RegisterClientScriptBlock(Me.GetType(), "Errore", "VisualizzaErrore('Indicare Causael Senior');", True)
            Exit Sub
        End If
        If Txt_CausaleEPersonam.Text.Trim = "" Then
            ClientScript.RegisterClientScriptBlock(Me.GetType(), "Errore", "VisualizzaErrore('Indicare Causale E-Personam');", True)
            Exit Sub
        End If


        Dim cn As OleDbConnection
        Dim MySql As String = ""

        cn = New Data.OleDb.OleDbConnection(Session("DC_OSPITE"))

        cn.Open()
        Dim cmd As New OleDbCommand()


        cmd.CommandText = ("Delete from DomiciliareEPersonam Where id = " & Val(Txt_Id.Text))
        cmd.Connection = cn
        cmd.ExecuteNonQuery()

        cn.Close()

        Response.Redirect("Elenco_CausaliEpersonamDomiciliare.aspx")
    End Sub


    Protected Sub DD_Struttura_TextChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles DD_Struttura.TextChanged
        AggiornaCServ()
        Call EseguiJS()
    End Sub

    Private Sub AggiornaCServ()
        Dim kCsrv As New Cls_CentroServizio

        If DD_Struttura.SelectedValue = "" Then
            kCsrv.UpDateDropBox(Session("DC_OSPITE"), Cmb_CServ)
        Else
            kCsrv.UpDateDropBoxStruttura(Session("DC_OSPITE"), Cmb_CServ, DD_Struttura.SelectedValue)
        End If

    End Sub


    Function campodb(ByVal oggetto As Object) As String
        If IsDBNull(oggetto) Then
            Return ""
        Else
            Return oggetto
        End If
    End Function

    Protected Sub Btn_Esci_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles Btn_Esci.Click
        Response.Redirect("Elenco_CausaliEpersonamDomiciliare.aspx")
    End Sub

    Protected Sub RB_Tutti_CheckedChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles RB_Tutti.CheckedChanged

    End Sub
End Class
