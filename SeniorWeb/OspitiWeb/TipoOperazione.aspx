﻿<%@ Page Language="VB" AutoEventWireup="false" Inherits="OspitiWeb_TipoOperazione" CodeFile="TipoOperazione.aspx.vb" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="xasp" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <meta http-equiv="x-ua-compatible" content="IE=9" />
    <title>Tabella Tipo Operazione</title>
    <asp:PlaceHolder runat="server">
        <%: System.Web.Optimization.Styles.Render("~/Content/AjaxControlToolkit/Styles/Bundle") %>
    </asp:PlaceHolder>
    <link href="ospiti.css?ver=11" rel="stylesheet" type="text/css" />
    <link rel="shortcut icon" href="images/SENIOR.ico" />
    <link href="js/jquery.autocomplete.css" rel="stylesheet" type="text/css" />

    <script src="js/jquery-1.5.1.min.js" type="text/javascript"></script>
    <script src="js/jquery.autocomplete.js" type="text/javascript"></script>
    <script src="js/soapclient.js" type="text/javascript"></script>
    <script src="/js/convertinumero.js" type="text/javascript"></script>



    <script src="js/jquery.maskedinput-1.3.min.js" type="text/javascript"></script>
    <script src="/js/formatnumer.js" type="text/javascript"></script>
    <script src="/js/pianoconti.js" type="text/javascript"></script>
    <script src="js/JSErrore.js" type="text/javascript"></script>
    <script type="text/javascript">         
        $(document).ready(function () {
            $('html').keyup(function (event) {

                if (event.keyCode == 113) {
                    __doPostBack("Imb_Modifica", "0");
                }

            });
        });

        $(document).ready(function () {
            if (window.innerHeight > 0) { $("#BarraLaterale").css("height", (window.innerHeight - 94) + "px"); } else { $("#BarraLaterale").css("height", (document.documentElement.offsetHeight - 94) + "px"); }
        });
    </script>

</head>
<body>
    <form id="form1" runat="server">
        <asp:ScriptManager ID="ScriptManager1" runat="server" EnableScriptGlobalization="true">
            <Scripts>
                <asp:ScriptReference Path="~/Scripts/AjaxControlToolkit/Bundle" />
            </Scripts>
        </asp:ScriptManager>
        <asp:Label ID="Lbl_BarraSenior" runat="server" Text=""></asp:Label>
        <div style="text-align: left;">
            <table style="width: 100%;" cellpadding="0" cellspacing="0">
                <tr>
                    <td style="width: 160px; background-color: #F0F0F0;"></td>
                    <td>
                        <div class="Titolo">Ospiti - Tabelle - Tipo Operazione</div>
                        <div class="SottoTitolo">
                            <br />
                            <br />
                        </div>
                    </td>
                    <td style="text-align: right;">
                        <div class="DivTasti">
                            <asp:ImageButton runat="server" ImageUrl="~/images/duplica.png" class="EffettoBottoniTondi" Height="38px" ToolTip="Duplica Causale" ID="Imb_Duplica"></asp:ImageButton>&nbsp;
			<asp:ImageButton runat="server" ImageUrl="~/images/salva.jpg" class="EffettoBottoniTondi" Height="38px" ToolTip="Modifica / Inserisci (F2)" ID="Imb_Modifica"></asp:ImageButton>
                            <asp:ImageButton runat="server" OnClientClick="return window.confirm('Eliminare?');" ImageUrl="~/images/elimina.jpg" class="EffettoBottoniTondi" Height="38px" ToolTip="Elimina" ID="ImageButton2"></asp:ImageButton>
                        </div>
                    </td>
                </tr>

                <tr>
                    <td style="width: 160px; background-color: #F0F0F0; vertical-align: top; text-align: center;" id="BarraLaterale">
                        <a href="Menu_Ospiti.aspx" style="border-width: 0px;">
                            <img src="images/Home.jpg" class="Effetto" alt="Menù" /></a><br />
                        <asp:ImageButton ID="Btn_Esci" runat="server" BackColor="Transparent" ImageUrl="../images/Menu_Indietro.png" class="Effetto" ToolTip="Chiudi" />
                        <br />
                        <br />
                        <br />
                        <br />
                        <br />
                        <br />
                        <br />
                        <br />
                        <br />
                    </td>
                    <td colspan="2" style="background-color: #FFFFFF;" valign="top">
                        <xasp:TabContainer ID="TabContainer1" runat="server" ActiveTabIndex="0" CssClass="TabSenior"
                            Width="100%" BorderStyle="None" Style="margin-right: 39px">
                            <xasp:TabPanel runat="server" HeaderText="Prima Nota" ID="Tab_Anagrafica">
                                <HeaderTemplate>
                                    Tipo Operazione
                                </HeaderTemplate>
                                <ContentTemplate>

                                    <label class="LabelCampo">Codice</label>
                                    <asp:TextBox ID="Txt_Codice" runat="server" Width="49px" AutoPostBack="true"></asp:TextBox>
                                    <asp:Image ID="Img_VerificaCodice" runat="server" Height="18px" ImageUrl="~/images/Blanco.png" Width="18px" />
                                    <br />
                                    <br />
                                    <hr />
                                    <label class="LabelCampo">Struttura:</label>
                                    <asp:DropDownList runat="server" ID="DD_Struttura" AutoPostBack="true"></asp:DropDownList><br />
                                    <br />

                                    <label class="LabelCampo">Centro Servizio : </label>
                                    <asp:DropDownList ID="Cmb_CServ" runat="server" Width="288px"></asp:DropDownList><br />
                                    <br />
                                    <label class="LabelCampo">Tipo : </label>
                                    <asp:RadioButton ID="RB_Tutti" runat="server" Text="Tutti" GroupName="Tipo" />
                                    <asp:RadioButton ID="RB_OspitiParenti" runat="server" Text="Solo Ospiti/Parenti" GroupName="Tipo" />
                                    <asp:RadioButton ID="RB_SoloEnti" runat="server" Text="Solo Enti" GroupName="Tipo" />


                                    <br />
                                    <hr />
                                    <br />
                                    <label class="LabelCampo">Descrizione</label>
                                    <asp:TextBox ID="Txt_Descrizione" runat="server" Width="483px" AutoPostBack="true"></asp:TextBox>
                                    <asp:Image ID="Img_VerificaDes" runat="server" Height="18px" ImageUrl="~/images/Blanco.png" Width="18px" />
                                    <br />
                                    <br />

                                    <table>
                                        <tr>
                                            <td>
                                                <label class="LabelCampo">Causale Retta : </label>
                                                <asp:DropDownList ID="DD_Retta" runat="server"></asp:DropDownList>
                                            </td>
                                            <td>
                                                <label class="LabelCampo">Causale Addebito :</label>
                                                <asp:DropDownList ID="DD_Addebito" runat="server"></asp:DropDownList>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                <label class="LabelCampo">Causale Accredito :</label>
                                                <asp:DropDownList ID="DD_Accredito" runat="server"></asp:DropDownList>
                                            </td>
                                            <td>
                                                <label class="LabelCampo">Causale Storno :</label>
                                                <asp:DropDownList ID="DD_Storno" runat="server"></asp:DropDownList>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                <label class="LabelCampo">Causale Incasso : </label>
                                                <asp:DropDownList ID="DD_Incasso" runat="server"></asp:DropDownList>
                                            </td>
                                            <td>
                                                <label class="LabelCampo">Causale Giroconto :</label>
                                                <asp:DropDownList ID="DD_Giroconto" runat="server"></asp:DropDownList><br />
                                            </td>
                                        </tr>

                                        <tr>
                                            <td>
                                                <label class="LabelCampo">Causale Documento Anticipo :</label>
                                                <asp:DropDownList ID="DD_DocumentoAnticipo" runat="server"></asp:DropDownList>
                                            </td>
                                            <td>
                                                <label class="LabelCampo">Causale Incasso Anticipo :</label>
                                                <asp:DropDownList ID="DD_IncassoAnticipo" runat="server"></asp:DropDownList>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                <label class="LabelCampo">Causale Documento A Zero :</label>
                                                <asp:DropDownList ID="DD_DocumentoAzero" runat="server"></asp:DropDownList>
                                            </td>

                                        </tr>

                                        <tr>
                                            <td>
                                                <label class="LabelCampo">Deposito Cauzionale :</label>
                                                <asp:DropDownList ID="DD_DepositoCauzionale" runat="server"></asp:DropDownList>
                                            </td>
                                            <td>
                                                <label class="LabelCampo">N.C. Deposito Causazionale :</label>
                                                <asp:DropDownList ID="DD_NCDepositoCauzionale" runat="server"></asp:DropDownList>
                                            </td>
                                            <td></td>
                                        </tr>

                                        <tr>
                                            <td>
                                                <label class="LabelCampo">Caparra :</label>
                                                <asp:DropDownList ID="DD_Caparra" runat="server"></asp:DropDownList>
                                            </td>
                                            <td>
                                                <label class="LabelCampo">N.C. Caparra :</label>
                                                <asp:DropDownList ID="DD_NCCaparra" runat="server"></asp:DropDownList>
                                            </td>
                                        </tr>

                                        <tr>
                                            <td>
                                                <label class="LabelCampo">IVA Caparra :</label>
                                                <asp:DropDownList ID="DD_IvaCaparra" runat="server"></asp:DropDownList>
                                            </td>
                                            <td></td>
                                        </tr>


                                        <tr>
                                            <td>
                                                <label class="LabelCampo">Regola Deposito :</label>
                                                <asp:DropDownList ID="DD_REGOLA" runat="server"></asp:DropDownList>
                                            </td>
                                            <td>
                                                <label class="LabelCampo">Importo Deposito :</label>
                                                <asp:TextBox ID="Txt_ImportoDeposito" Style="text-align: right;" onkeypress="ForceNumericInput(this, true, true)" runat="server" MaxLength="8" Width="100px"></asp:TextBox>
                                            </td>
                                            <td></td>
                                        </tr>
                                    </table>

                                    <br />
                                    <label style="display: block; float: left; width: 300px;">Non considerare conto anticipi in emissione :</label>
                                    <asp:CheckBox ID="Chk_NonContoAnticipi" runat="server" /><br />
                                    <br />
                                    <label style="display: block; float: left; width: 300px;">Non crer. reg. x Com. Reg.:</label>
                                    <asp:CheckBox ID="Chk_SoloParentiOSpiti" runat="server" /><br />
                                    <br />
                                    <label style="display: block; float: left; width: 300px;">Emetti Solo Anticipo per Ospiti e Parenti :</label>
                                    <asp:CheckBox ID="Chk_SoloAnticipo" runat="server" /><br />
                                    <br />
                                    <label class="LabelCampo">Scorpora IVA :</label>
                                    <asp:CheckBox ID="Chk_ScorporaIVA" runat="server" />
                                    (Su enti funziona solo se compensazione è No)<br />
                                    <br />
                                    <label class="LabelCampo">Bollettino :</label>
                                    <asp:CheckBox ID="Chk_Bollettino" runat="server" /><br />
                                    <br />
                                    <label class="LabelCampo">Fattura :</label>
                                    <asp:CheckBox ID="Chk_Fattura" runat="server" /><br />
                                    <br />

                                    <label class="LabelCampo">Bollo :</label>
                                    <asp:RadioButton ID="RB_BolloSi" GroupName="bollo" Text="SI" runat="server" />
                                    <asp:RadioButton ID="RB_BolloNo" GroupName="bollo" Text="NO" runat="server" />
                                    <asp:RadioButton ID="RB_BolloSoloFt" GroupName="bollo" Text="Solo Fatture" runat="server" />
                                    <asp:CheckBox ID="Chk_NonBolloSeIVA" runat="server" Text="Se presente una voce con Iva non includere il bollo" /><br />
                                    <br />
                                    <label class="LabelCampo">Bollo Virtuale :</label>
                                    <asp:CheckBox ID="Chk_BolloVirtuale" runat="server" Text="Solo Bollo Virtuale" /><br />
                                    <br />
                                    <label class="LabelCampo">Fattura di Anticipo :</label>
                                    <asp:RadioButton ID="Opt_NonAnticipo" GroupName="Anticipo" Text="Non Anticipo" runat="server" />
                                    <asp:RadioButton ID="Opt_Anticipo" GroupName="Anticipo" Text="Anticipo" runat="server" />
                                    <asp:RadioButton ID="Opt_AnticipoComune" GroupName="Anticipo" Text="Anticipo Comune" runat="server" />
                                    <asp:RadioButton ID="Opt_AnticipoCompensazione" GroupName="Anticipo" Text="Anticipo Compensazione" runat="server" />
                                    <asp:RadioButton ID="Opt_Rettagiroconto" GroupName="Anticipo" Text="Retta giroconto" runat="server" /><br />
                                    <br />


                                    <label class="LabelCampo">Tipo Addebito :</label>
                                    <asp:DropDownList ID="DD_TipoAddebito" runat="server"></asp:DropDownList><br />
                                    <br />

                                    <label class="LabelCampo">Tipo Addebito 2 retta :</label>
                                    <asp:DropDownList ID="DD_TipoAddebito2" runat="server"></asp:DropDownList><br />
                                    <br />

                                    <label class="LabelCampo">Percentuale :</label>
                                    <asp:TextBox ID="Txt_Percentuale" Style="text-align: right;" onkeypress="ForceNumericInput(this, true, true)" runat="server" MaxLength="8" Width="100px"></asp:TextBox><br />
                                    <br />

                                </ContentTemplate>
                            </xasp:TabPanel>
                        </xasp:TabContainer>
                    </td>
                </tr>
            </table>
        </div>
    </form>
</body>
</html>
