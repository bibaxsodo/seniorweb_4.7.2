﻿Imports System.Web.Hosting
Imports System.Diagnostics
Imports System.ComponentModel
Imports Microsoft.VisualBasic
Imports System.Data.OleDb
Imports System.Threading

Partial Class ricalcolo
    Inherits System.Web.UI.Page
    Dim Tabella As New System.Data.DataTable("tabella")
    Private Delegate Sub DoWorkDelegate()
    Private _work As DoWorkDelegate
    Private CServInS As String
    Private CodOsp As Long
    Private ConessioniOspiti As String
    Private CampoTimer As String
    Private CampoProgressBar As String
    Private TabellaNome As String


    Private Sub AggiornaCServ()
        Dim kCsrv As New Cls_CentroServizio

        If DD_Struttura.SelectedValue = "" Then
            kCsrv.UpDateDropBox(Session("DC_OSPITE"), DD_CServ)
        Else
            kCsrv.UpDateDropBoxStruttura(Session("DC_OSPITE"), DD_CServ, DD_Struttura.SelectedValue)
        End If
    End Sub



    Protected Sub DD_Struttura_TextChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles DD_Struttura.TextChanged
        AggiornaCServ()
        Call EseguiJS()
    End Sub

    Private Function OspitePresenteSenzaCserv(ByVal CodOsp As Long, ByVal Mese As Integer, ByVal Anno As Integer, ByVal Connessione As String) As Boolean
        Dim RsMovimenti As New ADODB.Recordset
        Dim MySql As String
        Dim VECCHIADATA As Date
        Dim OspitiDb As New ADODB.Connection

        OspitiDb.Open(Connessione)

        MySql = "Select top 1 * From Movimenti Where  CODICEOSPITE =" & CodOsp & " And Data <= {ts '" & Format(DateSerial(Anno, Mese, GiorniMese(Mese, Anno)), "yyyy-mm-dd") & " 00:00:00'}  Order By DATA Desc,Progressivo desc"


        RsMovimenti.Open(MySql, OspitiDb, ADODB.CursorTypeEnum.adOpenKeyset, ADODB.LockTypeEnum.adLockOptimistic)
        If RsMovimenti.EOF Then
            OspitePresenteSenzaCserv = False
        Else
            OspitePresenteSenzaCserv = False
            If MoveFromDb(RsMovimenti.Fields("TipoMov")) = "13" And Format(RsMovimenti.Fields("Data"), "yyyymmdd") >= Format(DateSerial(Anno, Mese, 1), "yyyymmdd") Then
                OspitePresenteSenzaCserv = True
            End If
            If MoveFromDb(RsMovimenti.Fields("TipoMov")) <> "13" Then
                OspitePresenteSenzaCserv = True
                VECCHIADATA = MoveFromDb(RsMovimenti.Fields("Data"))
            End If
        End If

        RsMovimenti.Close()
        OspitiDb.Close()
    End Function

    Function NonAutoOspiteMese(ByVal CodiceOspite As Long, ByVal Cserv As String, ByVal Anno As Long, ByVal Mese As Long, ByVal Connessione As String) As Boolean
        Dim MyRs As New ADODB.Recordset
        Dim OspitiDb As New ADODB.Connection
        OspitiDb.Open(Connessione)
        NonAutoOspiteMese = False
        MyRs.Open("Select * From  StatoAuto Where CodiceOspite = " & CodiceOspite & " And Data <= {ts '" & Format(DateSerial(Anno, Mese, GiorniMese(Mese, Anno)), "yyyy-mm-dd") & " 00:00:00'}  And CENTROSERVIZIO  = '" & Cserv & "' And STATOAUTO = 'N'  Order by Data Desc", OspitiDb, ADODB.CursorTypeEnum.adOpenKeyset, ADODB.LockTypeEnum.adLockOptimistic)
        If Not MyRs.EOF Then
            NonAutoOspiteMese = True
        End If
        MyRs.Close()
        If NonAutoOspiteMese = False Then
            MyRs.Open("Select * From  StatoAuto Where CodiceOspite = " & CodiceOspite & " And Data <= {ts '" & Format(DateSerial(Anno, Mese, GiorniMese(Mese, Anno)), "yyyy-mm-dd") & " 00:00:00'} And Data >= {ts '" & Format(DateSerial(Anno, Mese, 1), "yyyy-mm-dd") & " 00:00:00'}  And CENTROSERVIZIO  = '" & Cserv & "' And STATOAUTO = 'N'  Order by Data Desc", OspitiDb, ADODB.CursorTypeEnum.adOpenKeyset, ADODB.LockTypeEnum.adLockOptimistic)
            If Not MyRs.EOF Then
                NonAutoOspiteMese = True
            End If
            MyRs.Close()
        End If
        OspitiDb.Close()
    End Function
    Private Function MoveFromDb(ByVal MyField As ADODB.Field, Optional ByVal Tipo As Integer = 0) As Object
        If Tipo = 0 Then
            Select Case MyField.Type
                Case ADODB.DataTypeEnum.adDBDate Or ADODB.DataTypeEnum.adDBTimeStamp
                    If IsDBNull(MyField.Value) Then
                        MoveFromDb = "__/__/____"
                    Else
                        If Not IsDate(MyField.Value) Then
                            MoveFromDb = "__/__/____"
                        Else
                            MoveFromDb = MyField.Value
                        End If
                    End If
                Case ADODB.DataTypeEnum.adSmallInt
                    If IsDBNull(MyField.Value) Then
                        MoveFromDb = 0
                    Else
                        MoveFromDb = MyField.Value
                    End If
                Case 20 ' INTERO PER MYSQL
                    If IsDBNull(MyField.Value) Then
                        MoveFromDb = 0
                    Else
                        MoveFromDb = MyField.Value
                    End If
                Case ADODB.DataTypeEnum.adInteger
                    If IsDBNull(MyField.Value) Then
                        MoveFromDb = 0
                    Else
                        MoveFromDb = MyField.Value
                    End If
                Case ADODB.DataTypeEnum.adNumeric
                    If IsDBNull(MyField.Value) Then
                        MoveFromDb = 0
                    Else
                        MoveFromDb = MyField.Value
                    End If
                Case ADODB.DataTypeEnum.adTinyInt
                    If IsDBNull(MyField.Value) Then
                        MoveFromDb = 0
                    Else
                        MoveFromDb = MyField.Value
                    End If
                Case ADODB.DataTypeEnum.adSingle
                    If IsDBNull(MyField.Value) Then
                        MoveFromDb = 0
                    Else
                        MoveFromDb = MyField.Value
                    End If
                Case ADODB.DataTypeEnum.adLongVarBinary
                    If IsDBNull(MyField.Value) Then
                        MoveFromDb = 0
                    Else
                        MoveFromDb = MyField.Value
                    End If
                Case ADODB.DataTypeEnum.adDouble
                    If IsDBNull(MyField.Value) Then
                        MoveFromDb = 0
                    Else
                        MoveFromDb = MyField.Value
                    End If
                Case 139
                    If IsDBNull(MyField.Value) Then
                        MoveFromDb = 0
                    Else
                        MoveFromDb = MyField.Value
                    End If
                Case ADODB.DataTypeEnum.adVarChar
                    If IsDBNull(MyField.Value) Then
                        MoveFromDb = vbNullString
                    Else
                        MoveFromDb = CStr(MyField.Value)
                    End If
                Case ADODB.DataTypeEnum.adUnsignedTinyInt
                    If IsDBNull(MyField.Value) Then
                        MoveFromDb = 0
                    Else
                        MoveFromDb = MyField.Value
                    End If
                Case Else
                    If IsDBNull(MyField.Value) Then
                        MoveFromDb = ""
                    End If
                    If IsDBNull(MyField.Value) Then
                        MoveFromDb = ""
                    Else
                        'MoveFromDb = StringaSqlS(MyField.Value)
                        MoveFromDb = RTrim(MyField.Value)
                    End If
            End Select
        End If
    End Function
    Private Function OspitePresente(ByVal Cserv As String, ByVal CodOsp As Long, ByVal Mese As Integer, ByVal Anno As Integer, ByVal Connessione As String) As Boolean
        Dim RsMovimenti As New ADODB.Recordset
        Dim MySql As String
        Dim OspitiDb As New ADODB.Connection

        OspitiDb.Open(Connessione)

        MySql = "Select top 1 * From Movimenti Where  CENTROSERVIZIO = '" & Cserv & "' And CODICEOSPITE =" & CodOsp & " And Data <= {ts '" & Format(DateSerial(Anno, Mese, GiorniMese(Mese, Anno)), "yyyy-MM-dd") & " 00:00:00'}  Order By DATA Desc,PROGRESSIVO Desc"


        RsMovimenti.Open(MySql, OspitiDb, ADODB.CursorTypeEnum.adOpenKeyset, ADODB.LockTypeEnum.adLockOptimistic)
        If RsMovimenti.EOF Then
            OspitePresente = False
        Else
            OspitePresente = False
            If MoveFromDb(RsMovimenti.Fields("TipoMov")) = "13" And Format(MoveFromDb(RsMovimenti.Fields("Data")), "yyyyMMdd") >= Format(DateSerial(Anno, Mese, 1), "yyyyMMdd") Then
                OspitePresente = True
            End If
            If MoveFromDb(RsMovimenti.Fields("TipoMov")) <> "13" Then
                OspitePresente = True
            End If
        End If
        RsMovimenti.Close()
        OspitiDb.Close()
    End Function
    Public Function GiorniMese(ByVal Mese As Byte, ByVal Anno As Integer) As Byte
        If Mese = 1 Or Mese = 3 Or Mese = 5 Or Mese = 7 Or Mese = 8 Or _
           Mese = 10 Or Mese = 12 Then
            GiorniMese = 31
        Else
            If Mese <> 2 Then
                GiorniMese = 30
            Else
                If Day(DateSerial(Anno, Mese, 29)) = 29 Then
                    GiorniMese = 29
                Else
                    GiorniMese = 28
                End If
            End If
        End If
    End Function

    Public Function MoveFromDbWC(ByVal MyRs As ADODB.Recordset, ByVal Campo As String) As Object
        Dim MyField As ADODB.Field

#If FLAG_DEBUG = False Then
        On Error GoTo ErroreCampoInesistente
#End If
        MyField = MyRs.Fields(Campo)

        Select Case MyField.Type
            Case ADODB.DataTypeEnum.adDBDate
                If IsDBNull(MyField.Value) Then
                    MoveFromDbWC = "__/__/____"
                Else
                    If Not IsDate(MyField.Value) Or MyField.Value = "0.00.00" Then
                        MoveFromDbWC = "__/__/____"
                    Else
                        MoveFromDbWC = MyField.Value
                    End If
                End If
            Case ADODB.DataTypeEnum.adDBTimeStamp
                If IsDBNull(MyField.Value) Then
                    MoveFromDbWC = "__/__/____"
                Else
                    If Not IsDate(MyField.Value) Or MyField.Value = "0.00.00" Then
                        MoveFromDbWC = "__/__/____"
                    Else
                        MoveFromDbWC = MyField.Value
                    End If
                End If
            Case ADODB.DataTypeEnum.adSmallInt
                If IsDBNull(MyField.Value) Then
                    MoveFromDbWC = 0
                Else
                    MoveFromDbWC = MyField.Value
                End If
            Case ADODB.DataTypeEnum.adInteger
                If IsDBNull(MyField.Value) Then
                    MoveFromDbWC = 0
                Else
                    MoveFromDbWC = MyField.Value
                End If
            Case ADODB.DataTypeEnum.adNumeric
                If IsDBNull(MyField.Value) Then
                    MoveFromDbWC = 0
                Else
                    MoveFromDbWC = MyField.Value
                End If
            Case ADODB.DataTypeEnum.adTinyInt
                If IsDBNull(MyField.Value) Then
                    MoveFromDbWC = 0
                Else
                    MoveFromDbWC = MyField.Value
                End If
            Case ADODB.DataTypeEnum.adSingle
                If IsDBNull(MyField.Value) Then
                    MoveFromDbWC = 0
                Else
                    MoveFromDbWC = MyField.Value
                End If
            Case ADODB.DataTypeEnum.adLongVarBinary
                If IsDBNull(MyField.Value) Then
                    MoveFromDbWC = 0
                Else
                    MoveFromDbWC = MyField.Value
                End If
            Case ADODB.DataTypeEnum.adDouble
                If IsDBNull(MyField.Value) Then
                    MoveFromDbWC = 0
                Else
                    MoveFromDbWC = MyField.Value
                End If
            Case ADODB.DataTypeEnum.adVarChar
                If IsDBNull(MyField.Value) Then
                    MoveFromDbWC = vbNullString
                Else
                    MoveFromDbWC = CStr(MyField.Value)
                End If
            Case ADODB.DataTypeEnum.adUnsignedTinyInt
                If IsDBNull(MyField.Value) Then
                    MoveFromDbWC = 0
                Else
                    MoveFromDbWC = MyField.Value
                End If
            Case Else
                If IsDBNull(MyField.Value) Then
                    MoveFromDbWC = ""
                End If
                If IsDBNull(MyField.Value) Then
                    MoveFromDbWC = ""
                Else
                    MoveFromDbWC = MyField.Value
                End If
        End Select

        On Error GoTo 0
        Exit Function
ErroreCampoInesistente:

        On Error GoTo 0
    End Function

    Protected Sub Ricalcola(ByVal AnnoIn As Long, ByVal MeseDa As Long, ByVal MeseA As Long, ByVal SoloNonAuto As Boolean, ByVal SoloPresenti As Boolean, ByVal MioAddibito As Boolean, ByVal Connessione As String, ByVal Descrizione As String, ByRef data As Object)
        Dim IndiceVariazione As Long
        Dim MyRs As New ADODB.Recordset
        Dim OspitiDb As New ADODB.Connection
        Dim Anno As Long
        Dim SessioneTP As System.Web.SessionState.HttpSessionState = data
        Dim GGMESE As Long


        Dim IndiceGiorni As Integer
        Dim RDataINI As String
        Dim ControllaOspite As Boolean


        IndiceVariazione = 0

        Dim f As New Cls_Parametri


        OspitiDb.Open(Connessione)
        f.LeggiParametri(Connessione)


        Dim sc2 As New MSScriptControl.ScriptControl

        sc2.Language = "vbscript"
        Dim XS As New Cls_CalcoloRette



        XS.CampoParametriGIORNOUSCITA = f.GIORNOUSCITA
        XS.CampoParametriGIORNOENTRATA = f.GIORNOENTRATA
        XS.CampoParametriCAUSALEACCOGLIMENTO = f.CAUSALEACCOGLIMENTO
        XS.CampoParametriMastroAnticipo = f.MastroAnticipo
        XS.CampoParametriAddebitiAccreditiComulati = f.AddebitiAccreditiComulati
        XS.ConnessioneGenerale = Session("DC_GENERALE")

        XS.ApriDB(Connessione, SessioneTP("DC_OSPITIACCESSORI"))
        XS.STRINGACONNESSIONEDB = Connessione
        XS.CaricaCausali()

        Dim Errori As Boolean
        Dim StringaDegliErrori As String
        Dim MeseINI, MeseFINE As Long
        Dim xNum As Long
        Dim xMax As Long

        MeseINI = MeseDa
        MeseFINE = MeseA
        Errori = False
        StringaDegliErrori = ""
        For Mese = MeseINI To MeseFINE




            If CodOsp <> 0 Then
                MyRs.Open("SELECT MOVIMENTI.CENTROSERVIZIO, AnagraficaComune.CODICEOSPITE FROM AnagraficaComune INNER JOIN MOVIMENTI ON AnagraficaComune.CODICEOSPITE = MOVIMENTI.CODICEOSPITE WHERE (((AnagraficaComune.TIPOLOGIA)='O')) And CENTROSERVIZIO = '" & CServInS & "' And AnagraficaComune.CODICEOSPITE = " & CodOsp & " And MOVIMENTI.TIPOMOV = '05' GROUP BY MOVIMENTI.CENTROSERVIZIO, AnagraficaComune.CODICEOSPITE ", OspitiDb, ADODB.CursorTypeEnum.adOpenKeyset, ADODB.LockTypeEnum.adLockOptimistic)
            Else

                If DD_CServ.SelectedValue <> "" Then
                    MyRs.Open("SELECT MOVIMENTI.CENTROSERVIZIO, AnagraficaComune.CODICEOSPITE FROM AnagraficaComune INNER JOIN MOVIMENTI ON AnagraficaComune.CODICEOSPITE = MOVIMENTI.CODICEOSPITE WHERE (((AnagraficaComune.TIPOLOGIA)='O')) And CENTROSERVIZIO = '" & DD_CServ.SelectedValue & "'  And MOVIMENTI.TIPOMOV = '05' GROUP BY MOVIMENTI.CENTROSERVIZIO, AnagraficaComune.CODICEOSPITE ", OspitiDb, ADODB.CursorTypeEnum.adOpenKeyset, ADODB.LockTypeEnum.adLockOptimistic)
                Else
                    If DD_Struttura.SelectedValue <> "" Then

                        Dim Indice As Integer
                        Dim MySql As String = "("
                        Call AggiornaCServ()
                        For Indice = 0 To DD_CServ.Items.Count - 1
                            If Indice >= 1 Then
                                MySql = MySql & " Or "
                            End If
                            MySql = MySql & "  CENTROSERVIZIO = '" & DD_CServ.Items(Indice).Value & "'"
                        Next
                        MySql = MySql & ") "
                        MyRs.Open("SELECT MOVIMENTI.CENTROSERVIZIO, AnagraficaComune.CODICEOSPITE FROM AnagraficaComune INNER JOIN MOVIMENTI ON AnagraficaComune.CODICEOSPITE = MOVIMENTI.CODICEOSPITE WHERE (((AnagraficaComune.TIPOLOGIA)='O')) And " & MySql & "  And MOVIMENTI.TIPOMOV = '05' GROUP BY MOVIMENTI.CENTROSERVIZIO, AnagraficaComune.CODICEOSPITE ", OspitiDb, ADODB.CursorTypeEnum.adOpenKeyset, ADODB.LockTypeEnum.adLockOptimistic)
                    Else
                        MyRs.Open("SELECT MOVIMENTI.CENTROSERVIZIO, AnagraficaComune.CODICEOSPITE FROM AnagraficaComune INNER JOIN MOVIMENTI ON AnagraficaComune.CODICEOSPITE = MOVIMENTI.CODICEOSPITE WHERE (((AnagraficaComune.TIPOLOGIA)='O')) And MOVIMENTI.TIPOMOV = '05' GROUP BY MOVIMENTI.CENTROSERVIZIO, AnagraficaComune.CODICEOSPITE ", OspitiDb, ADODB.CursorTypeEnum.adOpenKeyset, ADODB.LockTypeEnum.adLockOptimistic)
                    End If
                End If
            End If


            xMax = 0

            Do While Not MyRs.EOF
                xMax = xMax + 1
                MyRs.MoveNext()
            Loop
            MyRs.Requery()

            Anno = AnnoIn

            GGMESE = GiorniMese(Mese, Anno)

            RDataINI = Format(DateSerial(Anno, Mese, 1), "yyyyMMdd")
            xNum = 0
            Do While Not MyRs.EOF

                SessioneTP("CampoProgressBar") = Math.Round(100 / xMax * xNum, 2) & "%<br />" & Dd_MeseA.Items(Mese - 1).Text
                System.Web.HttpRuntime.Cache("CampoProgressBar" + Session.SessionID) = Math.Round(100 / xMax * xNum, 2) & "%<br />" & Dd_MeseA.Items(Mese - 1).Text

                OspitiDb.Execute("Update TabellaParametri Set CodiceOspite = " & Val(MoveFromDbWC(MyRs, "CodiceOspite")))

                f.Elaborazione(Connessione)
                xNum = xNum + 1

                ControllaOspite = True
                If SoloPresenti = True Then
                    If Not OspitePresenteSenzaCserv(MoveFromDbWC(MyRs, "CodiceOspite"), Month(Now), Year(Now), Connessione) Then
                        ControllaOspite = False
                    End If
                End If
                If SoloNonAuto = True Then
                    If Not NonAutoOspiteMese(MoveFromDbWC(MyRs, "CodiceOspite"), MoveFromDbWC(MyRs, "CentroServizio"), Year(Now), Month(Now), Connessione) Then
                        ControllaOspite = False
                    End If
                End If


                If ControllaOspite = True Then
                    If Not OspitePresente(MoveFromDbWC(MyRs, "CentroServizio"), MoveFromDbWC(MyRs, "CodiceOspite"), Mese, Anno, Connessione) Then

                        Call XS.AzzeraTabella()


                        Call XS.CreaTabellaImportoComune(MoveFromDbWC(MyRs, "CentroServizio"), MoveFromDbWC(MyRs, "CodiceOspite"), Mese, Anno)


                        Call XS.CreaTabellaImportoJolly(MoveFromDbWC(MyRs, "CentroServizio"), MoveFromDbWC(MyRs, "CodiceOspite"), Mese, Anno)

                        Call XS.CreaTabellaUsl(MoveFromDbWC(MyRs, "CentroServizio"), MoveFromDbWC(MyRs, "CodiceOspite"), Mese, Anno)


                        For IndiceGiorni = 0 To GiorniMese(Mese, Anno)
                            If XS.MyTabCodiceComune(IndiceGiorni) <> "****" Then
                                XS.InserisciComune(XS.MyTabCodiceProv(IndiceGiorni) & XS.MyTabCodiceComune(IndiceGiorni))
                            End If
                            If XS.MyTabCodiceJolly(IndiceGiorni) <> "****" Then
                                XS.InserisciJolly(XS.MyTabCodiceProvJolly(IndiceGiorni) & XS.MyTabCodiceJolly(IndiceGiorni))
                            End If
                            If XS.MyTabCodiceRegione(IndiceGiorni) <> "****" Then
                                XS.InserisciRegione(XS.MyTabCodiceRegione(IndiceGiorni), XS.MyTabRegioneTipoImporto(IndiceGiorni))
                            End If
                        Next


                        If Chk_Addebito.Checked = True Then
                            Call XS.ConfrontaDati(MoveFromDbWC(MyRs, "CentroServizio"), MoveFromDbWC(MyRs, "CodiceOspite"), Mese, Anno, 1, Txt_Data.Text, "Competenza " & Dd_MeseA.Items(Mese - 1).Text & "/" & Anno)
                        Else
                            Call XS.ConfrontaDati(MoveFromDbWC(MyRs, "CentroServizio"), MoveFromDbWC(MyRs, "CodiceOspite"), Mese, Anno, 0, Txt_Data.Text, "Competenza " & Dd_MeseA.Items(Mese - 1).Text & "/" & Anno)
                        End If
                    Else
                        Call XS.AzzeraTabella()

                        Dim XCentro As New Cls_CentroServizio

                        XCentro.Leggi(Connessione, MoveFromDbWC(MyRs, "CentroServizio"))

                        If XCentro.TIPOCENTROSERVIZIO = "D" Then
                            Call XS.PresenzeAssenzeDiurno(MoveFromDbWC(MyRs, "CentroServizio"), MoveFromDbWC(MyRs, "CodiceOspite"), Mese, Anno)
                        End If


                        Call XS.CreaTabellaPresenze(MoveFromDbWC(MyRs, "CentroServizio"), MoveFromDbWC(MyRs, "CodiceOspite"), Mese, Anno)

                        If XCentro.TIPOCENTROSERVIZIO = "A" Then
                            If MioAddibito = True Then
                                If Descrizione.Trim = "" Then
                                    XS.PresenzeDomiciliare(MoveFromDbWC(MyRs, "CentroServizio"), MoveFromDbWC(MyRs, "CodiceOspite"), Mese, Anno, True, 1, Txt_Data.Text, "Competenza " & Dd_MeseA.Items(Mese - 1).Text & "/" & Anno)
                                Else
                                    XS.PresenzeDomiciliare(MoveFromDbWC(MyRs, "CentroServizio"), MoveFromDbWC(MyRs, "CodiceOspite"), Mese, Anno, True, 1, Txt_Data.Text, Descrizione)
                                End If
                            Else
                                XS.PresenzeDomiciliare(MoveFromDbWC(MyRs, "CentroServizio"), MoveFromDbWC(MyRs, "CodiceOspite"), Mese, Anno, True, 0)
                            End If

                        End If

                        Call XS.CreaTabellaImportoOspite(MoveFromDbWC(MyRs, "CentroServizio"), MoveFromDbWC(MyRs, "CodiceOspite"), Mese, Anno)



                        Call XS.CreaTabellaImportoComune(MoveFromDbWC(MyRs, "CentroServizio"), MoveFromDbWC(MyRs, "CodiceOspite"), Mese, Anno)


                        Call XS.CreaTabellaImportoJolly(MoveFromDbWC(MyRs, "CentroServizio"), MoveFromDbWC(MyRs, "CodiceOspite"), Mese, Anno)

                        Call XS.CreaTabellaUsl(MoveFromDbWC(MyRs, "CentroServizio"), MoveFromDbWC(MyRs, "CodiceOspite"), Mese, Anno)

                        Call XS.CreaTabellaImportoRetta(MoveFromDbWC(MyRs, "CentroServizio"), MoveFromDbWC(MyRs, "CodiceOspite"), Mese, Anno)


                        Call XS.CreaTabellaModalita(MoveFromDbWC(MyRs, "CentroServizio"), MoveFromDbWC(MyRs, "CodiceOspite"), Mese, Anno, True)

                        Call XS.CreaTabellaImportoParente(MoveFromDbWC(MyRs, "CentroServizio"), MoveFromDbWC(MyRs, "CodiceOspite"), Mese, Anno)
                        'Call CalcolaRettaDaTabella(Tabella, MoveFromDbWC(MyRs,"CentroServizio"), MoveFromDbWC(MyRs,"CodiceOspite"))

                        Call XS.RiassociaComuni(MoveFromDbWC(MyRs, "CentroServizio"), MoveFromDbWC(MyRs, "CodiceOspite"), Mese, Anno)

                        Call XS.CalcolaRettaDaTabella(MoveFromDbWC(MyRs, "CentroServizio"), MoveFromDbWC(MyRs, "CodiceOspite"), Anno, Mese, sc2)


                        Call XS.CreaAddebitiAcrrediti(MoveFromDbWC(MyRs, "CentroServizio"), MoveFromDbWC(MyRs, "CodiceOspite"), Mese, Anno)

                        Call XS.AllineaImportiMensili(MoveFromDbWC(MyRs, "CentroServizio"), MoveFromDbWC(MyRs, "CodiceOspite"), Mese, Anno)
                        If Not XS.VerificaRette(MoveFromDbWC(MyRs, "CentroServizio"), MoveFromDbWC(MyRs, "CodiceOspite"), Mese, Anno) Then
                            Errori = True
                        Else
                            If MioAddibito = True Then
                                If Descrizione.Trim = "" Then
                                    Call XS.ConfrontaDati(MoveFromDbWC(MyRs, "CentroServizio"), MoveFromDbWC(MyRs, "CodiceOspite"), Mese, Anno, 1, Txt_Data.Text, "Competenza " & Dd_MeseA.Items(Mese - 1).Text & "/" & Anno)
                                Else
                                    Call XS.ConfrontaDati(MoveFromDbWC(MyRs, "CentroServizio"), MoveFromDbWC(MyRs, "CodiceOspite"), Mese, Anno, 1, Txt_Data.Text, Descrizione)
                                End If
                            Else
                                Call XS.ConfrontaDati(MoveFromDbWC(MyRs, "CentroServizio"), MoveFromDbWC(MyRs, "CodiceOspite"), Mese, Anno, 0, Txt_Data.Text, "Competenza " & Dd_MeseA.Items(Mese - 1).Text & "/" & Anno)
                            End If
                        End If
                        End If
                End If
                    MyRs.MoveNext()
            Loop
            MyRs.Close()
        Next Mese


        OspitiDb.Execute("Update TabellaParametri Set CodiceOspite = 0")
        f.FineElaborazione(Connessione)

        Tabella.Clear()
        Tabella.Columns.Clear()
        Tabella.Columns.Add("CentroServizio", GetType(String))
        Tabella.Columns.Add("CodiceOspite", GetType(String))
        Tabella.Columns.Add("NumeroMovimento", GetType(String))
        Tabella.Columns.Add("Periodo", GetType(String))
        Tabella.Columns.Add("Ospite", GetType(String))
        Tabella.Columns.Add("Parente_id", GetType(Long))
        Tabella.Columns.Add("Parente", GetType(String))
        Tabella.Columns.Add("Comune", GetType(String))
        Tabella.Columns.Add("Jolly", GetType(String))
        Tabella.Columns.Add("Regione", GetType(String))
        Tabella.Columns.Add("Descrizione", GetType(String))
        Tabella.Columns.Add("OspiteRiferimento", GetType(String))
        Dim i As Integer
        For i = 1 To 500
            If IsNothing(XS.RicalcoloGriglia_CServ(i)) Then Exit For

            Dim myriga As System.Data.DataRow = Tabella.NewRow()

            myriga(0) = XS.RicalcoloGriglia_CServ(i)
            myriga(1) = XS.RicalcoloGriglia_CodiceOspite(i)
            myriga(2) = XS.RicalcoloGriglia_NumeroMovimento(i)
            myriga(3) = Dd_MeseA.Items(XS.RicalcoloGriglia_Mese(i) - 1).Text & " /" & XS.RicalcoloGriglia_Anno(i)
            myriga(4) = Format(XS.RicalcoloGriglia_Ospite(i), "#,##0.00")
            myriga(5) = XS.RicalcoloGriglia_Parente_id(i)
            myriga(6) = Format(XS.RicalcoloGriglia_Parente(i), "#,##0.00")
            myriga(7) = Format(XS.RicalcoloGriglia_Comune(i), "#,##0.00")
            myriga(8) = Format(XS.RicalcoloGriglia_Jolly(i), "#,##0.00")
            myriga(9) = Format(XS.RicalcoloGriglia_Regione(i), "#,##0.00")
            myriga(10) = XS.RicalcoloGriglia_Descrizione(i)
            Dim MyOspite As New ClsOspite

            MyOspite.Leggi(Session("DC_OSPITE"), XS.RicalcoloGriglia_CodiceOspite(i))
            myriga(11) = MyOspite.Nome

            Tabella.Rows.Add(myriga)
        Next
        SessioneTP("TabellaNome") = Tabella

        SessioneTP("CampoProgressBar") = "STOP"

        System.Web.HttpRuntime.Cache("TabellaNome" + Session.SessionID) = Tabella
        System.Web.HttpRuntime.Cache("CampoProgressBar" + Session.SessionID) = "STOP"
    End Sub

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load


        CServInS = Session("CODICESERVIZIO")
        CodOsp = Session("CODICEOSPITE")
        ConessioniOspiti = Session("DC_OSPITE")



        If Trim(Session("UTENTE")) = "" Then
            Response.Redirect("..\Login.aspx")
            Exit Sub
        End If


        If Session("ABILITAZIONI").IndexOf("<CALCOLO>") < 1 Then
            Response.Redirect("Menu_Ospiti.aspx")
            Exit Sub
        End If

        If Page.IsPostBack = True Then Exit Sub

        Dim KBegin As New Cls_Parametri

        KBegin.LeggiParametri(Session("DC_OSPITE"))


        Try
            If Request.UrlReferrer.ToString.ToUpper.IndexOf("Menu_Ospiti.aspx".ToUpper) > 0 Then
                Session("CODICESERVIZIO") = ""
                Session("CODICEOSPITE") = 0
                ViewState("CODICESERVIZIO") = ""
                ViewState("CODICEOSPITE") = 0

                CServInS = Session("CODICESERVIZIO")
                CodOsp = Session("CODICEOSPITE")
            Else
                ViewState("CODICESERVIZIO") = Session("CODICESERVIZIO")
                ViewState("CODICEOSPITE") = Session("CODICEOSPITE")
            End If


        Catch ex As Exception

        End Try

        Dim kVilla As New Cls_TabelleDescrittiveOspitiAccessori


        kVilla.UpDateDropBox(Session("DC_OSPITIACCESSORI"), "VIL", DD_Struttura)
        If DD_Struttura.Items.Count = 1 Then
            Call AggiornaCServ()
        End If


        Dim K1 As New Cls_SqlString

        Dim Barra As New Cls_BarraSenior

        If Not IsNothing(Session("RicercaAnagraficaSQLString")) Then
            K1 = Session("RicercaAnagraficaSQLString")
        End If

        Lbl_BarraSenior.Text = Barra.CodiceBarra(Application("SENIOR"), Session("UTENTE"), Page, K1)


        Dim ConnectionString As String = Session("DC_OSPITE")

        Dim f As New Cls_Parametri

        f.LeggiParametri(ConnectionString)

        Txt_Anno.Text = f.AnnoFatturazione




        Dd_MeseDa.SelectedValue = f.MeseFatturazione
        Dd_MeseA.SelectedValue = f.MeseFatturazione


        REM Application("RC_TBL") = ""
        Lbl_Waiting.Text = ""
        f.PercEsec = 0
        f.Errori = ""
        f.CodiceOspite = 0
        f.ScriviParametri(ConnectionString)

        Session("CampoProgressBar") = ""
        System.Web.HttpRuntime.Cache("CampoProgressBar" + Session.SessionID) = ""

        Dim x As New ClsOspite

        x.CodiceOspite = Session("CODICEOSPITE")
        x.Leggi(Session("DC_OSPITE"), x.CodiceOspite)

        Dim cs As New Cls_CentroServizio

        cs.Leggi(Session("DC_OSPITE"), Session("CODICESERVIZIO"))

        If Val(Session("CODICEOSPITE")) > 0 Then
            Lbl_NomeOspite.Text = x.Nome & " -  " & x.DataNascita & " - " & cs.DESCRIZIONE & "<i style=""font-size:small;"">(" & x.CodiceOspite & ")</i>"
        Else
            Dim MyJs As String

            MyJs = "$(document).ready(function() {  $('#MENUDIV').html(''); });"
            ScriptManager.RegisterStartupScript(Me, Me.GetType(), "DISATTIVAMENU", MyJs, True)
        End If

        If Val(Session("CODICEOSPITE")) > 0 Then
            Chk_SoloNonAuto.Enabled = False
            Chk_SoloPresenti.Enabled = False
            DD_CServ.Enabled = False
            DD_Struttura.Enabled = False
        End If

        Call EseguiJS()


    End Sub
    Private Sub DoWork(ByVal data As Object)
        Application(CampoProgressBar) = ""
        Call Ricalcola(Val(Txt_Anno.Text), Val(Dd_MeseDa.SelectedValue), Val(Dd_MeseA.SelectedValue), Chk_SoloNonAuto.Checked, Chk_SoloPresenti.Checked, Chk_Addebito.Checked, ConessioniOspiti, Txt_Descrizione.Text, data)

    End Sub



    Protected Sub ImageButton1_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles ImageButton1.Click
        Session("CODICESERVIZIO") = ViewState("CODICESERVIZIO")
        Session("CODICEOSPITE") = ViewState("CODICEOSPITE")
        CServInS = Session("CODICESERVIZIO")
        CodOsp = Session("CODICEOSPITE")


        Dim Parametri As New Cls_Parametri

        
        If Parametri.InElaborazione(Session("DC_OSPITE")) Then
            ClientScript.RegisterClientScriptBlock(Me.GetType(), "Errore", "alert('Elaborazione in esecuzione non posso procedere');", True)
            Exit Sub
        End If


        Lbl_Errori.Text = ""
        If Val(Session("CODICEOSPITE")) = 0 Then
            Dim MyJs As String

            MyJs = "$(document).ready(function() {  $('#MENUDIV').html(''); });"
            ScriptManager.RegisterStartupScript(Me, Me.GetType(), "DISATTIVAMENU", MyJs, True)
        End If

        If Not IsDate(Txt_Data.Text) Then            
            ClientScript.RegisterClientScriptBlock(Me.GetType(), "Errore", "VisualizzaErrore('Data formalmente errata');", True)
            Call EseguiJS()
            Exit Sub
        End If
        If Val(Txt_Anno.Text) = 0 Then
            ClientScript.RegisterClientScriptBlock(Me.GetType(), "Errore", "VisualizzaErrore('Anno formalmente errato');", True)
            Call EseguiJS()
            Exit Sub
        End If

        If Chk_Addebito.Checked = True Then
            Txt_Anno.Enabled = False
            Txt_Data.Enabled = False
            Chk_Addebito.Enabled = False
            Chk_SoloNonAuto.Enabled = False
            Chk_SoloPresenti.Enabled = False
            Dd_MeseA.Enabled = False
            Dd_MeseDa.Enabled = False
            ImageButton1.Enabled = False
        End If


        Timer1.Enabled = True

        SyncLock (Session.SyncRoot())
            Session("CampoErrori") = ""
            Session("NomeOspite") = ""
            Session("NomeOspite1") = ""
            Session("NomeOspite2") = ""
            Session("LastOspite") = ""
            Session("CampoProgressBar") = ""
        End SyncLock

        System.Web.HttpRuntime.Cache("CampoErrori" + Session.SessionID) = ""
        System.Web.HttpRuntime.Cache("NomeOspite" + Session.SessionID) = ""
        System.Web.HttpRuntime.Cache("NomeOspite1" + Session.SessionID) = ""
        System.Web.HttpRuntime.Cache("NomeOspite2" + Session.SessionID) = ""
        System.Web.HttpRuntime.Cache("LastOspite" + Session.SessionID) = ""
        System.Web.HttpRuntime.Cache("CampoProgressBar" + Session.SessionID) = ""

        Dim t As New Thread(New ParameterizedThreadStart(AddressOf DoWork))

        t.Start(Session)
    End Sub


    Protected Sub Timer1_Tick(ByVal sender As Object, ByVal e As System.EventArgs) Handles Timer1.Tick
        Dim Appoggio As String = System.Web.HttpRuntime.Cache("CampoProgressBar" + Session.SessionID)

        If Appoggio = "STOP" Then
            Tabella = Session("TabellaNome")
            Grid.AutoGenerateColumns = True
            Grid.Visible = True
            Grid.DataSource = System.Web.HttpRuntime.Cache("TabellaNome" + Session.SessionID)
            Grid.DataBind()

            Lbl_Waiting.Text = ""
            System.Web.HttpRuntime.Cache("CampoProgressBar" + Session.SessionID) = ""
            Appoggio = ""
            Timer1.Enabled = False
            If Chk_Addebito.Checked = True Then
                Btn_CancellaOspiti.Visible = True
                Btn_CancellaParenti.Visible = True
                Btn_CancellaComune.Visible = True
                Btn_CancellaRegione.Visible = True
                Btn_CancellaJolly.Visible = True
            End If
        End If
        If Appoggio <> "" Then

            If Session("LastOspite") <> Cache("NomeOspite" + Session.SessionID) And Session("LastOspite") <> "" Then
                Session("NomeOspite1") = Session("NomeOspite2")
                Session("NomeOspite2") = Session("LastOspite")
            End If



            Lbl_Waiting.Text = "<div id=""blur"">&nbsp;</div><div id=""pippo""  class=""wait"">"
            Lbl_Waiting.Text = Lbl_Waiting.Text & "<font size=""7"">" & System.Web.HttpRuntime.Cache("CampoProgressBar" + Session.SessionID) & "</font>"
            Lbl_Waiting.Text = Lbl_Waiting.Text & "<img  height=30px src=""images/loading.gif""><br />"
            Lbl_Waiting.Text = Lbl_Waiting.Text & "Elaborazione... <br /><br />"
            If Session("NomeOspite1") = "" Then
                Lbl_Waiting.Text = Lbl_Waiting.Text & " <br />"
            Else
                Lbl_Waiting.Text = Lbl_Waiting.Text & " ...... <br />"
            End If
            Lbl_Waiting.Text = Lbl_Waiting.Text & Session("NomeOspite1") & " <br />"
            Lbl_Waiting.Text = Lbl_Waiting.Text & Session("NomeOspite2") & " <br />"
            Lbl_Waiting.Text = Lbl_Waiting.Text & "<font color=""007dc4"">" & Cache("NomeOspite" + Session.SessionID) & " </font><br />"
            Lbl_Waiting.Text = Lbl_Waiting.Text & "</div>"


            Session("LastOspite") = Cache("NomeOspite" + Session.SessionID)
        End If
    End Sub

    Protected Sub Grid_PageIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles Grid.PageIndexChanged

    End Sub

    Protected Sub Grid_PageIndexChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewPageEventArgs) Handles Grid.PageIndexChanging



        Tabella = Session("TabellaNome")
        Grid.PageIndex = e.NewPageIndex
        Grid.AutoGenerateColumns = True
        Grid.Visible = True
        Grid.DataSource = Tabella
        Grid.DataBind()

    End Sub

    Protected Sub Grid_RowDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs) Handles Grid.RowDataBound
        If e.Row.RowType = DataControlRowType.DataRow Then
            e.Row.Cells(5).HorizontalAlign = HorizontalAlign.Right
            e.Row.Cells(7).HorizontalAlign = HorizontalAlign.Right
            e.Row.Cells(8).HorizontalAlign = HorizontalAlign.Right
            e.Row.Cells(9).HorizontalAlign = HorizontalAlign.Right
            e.Row.Cells(10).HorizontalAlign = HorizontalAlign.Right
        End If
    End Sub


    Protected Sub Grid_RowDeleting(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewDeleteEventArgs) Handles Grid.RowDeleting
        If Not IsDate(Txt_Data.Text) Then Exit Sub

        Dim cn As OleDbConnection
        Dim MySql As String
        Dim D As Long

        MySql = ""

        cn = New Data.OleDb.OleDbConnection(Session("DC_OSPITE"))


        Tabella = System.Web.HttpRuntime.Cache("TabellaNome" + Session.SessionID)

        D = e.RowIndex



        cn.Open()

        Try
            Dim cmd As New OleDbCommand()
            cmd.CommandText = ("Delete from ADDACR where Data = ? And Progressivo = " & Val(Grid.Rows(D).Cells(3).Text))
            Dim Txt_DataText As Date = Txt_Data.Text
            cmd.Parameters.AddWithValue("@Data", Txt_DataText)
            cmd.Connection = cn
            cmd.ExecuteNonQuery()
            

            Tabella.Rows.RemoveAt(D + (Grid.PageIndex * Grid.PageSize))

            Grid.AutoGenerateColumns = True
            Grid.Visible = True
            Grid.DataSource = Tabella
            Grid.DataBind()
            Application(TabellaNome) = Tabella

        Catch ex As Exception

        End Try

        cn.Close()

        e.Cancel = True
    End Sub





    Protected Sub Btn_CancellaOspiti_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles Btn_CancellaOspiti.Click
        If IsNothing(Session("TabellaNome")) Then Exit Sub

        Dim cn As OleDbConnection
        Dim MySql As String
        Dim D As Long
        Dim Appoggio As Double
        MySql = ""

        cn = New Data.OleDb.OleDbConnection(Session("DC_OSPITE"))


        Tabella = System.Web.HttpRuntime.Cache("TabellaNome" + Session.SessionID)

        For D = Tabella.Rows.Count - 1 To 0 Step -1

            If IsDBNull(Tabella.Rows(D).Item(4)) Then
                Appoggio = 0
            Else
                Appoggio = Tabella.Rows(D).Item(4).ToString
            End If
            If CDbl(Appoggio) <> 0 Then
                cn.Open()
                Dim cmd As New OleDbCommand()
                cmd.CommandText = ("Delete from ADDACR where Data = ? And Progressivo = " & Val(Tabella.Rows(D).Item(2).ToString))
                Dim Txt_DataText As Date = Txt_Data.Text
                cmd.Parameters.AddWithValue("@Data", Txt_DataText)
                cmd.Connection = cn
                cmd.ExecuteNonQuery()
                cn.Close()

                Tabella.Rows.RemoveAt(D)
            End If
        Next

        Grid.AutoGenerateColumns = True
        Grid.Visible = True
        Grid.DataSource = Tabella
        Grid.DataBind()
        System.Web.HttpRuntime.Cache("TabellaNome" + Session.SessionID) = Tabella
    End Sub

    Protected Sub Btn_CancellaParenti_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles Btn_CancellaParenti.Click
        If IsNothing(Session("TabellaNome")) Then Exit Sub


        Dim cn As OleDbConnection
        Dim MySql As String
        Dim D As Long
        Dim Appoggio As Double
        MySql = ""

        cn = New Data.OleDb.OleDbConnection(Session("DC_OSPITE"))


        Tabella = System.Web.HttpRuntime.Cache("TabellaNome" + Session.SessionID)

        For D = Tabella.Rows.Count - 1 To 0 Step -1

            If IsDBNull(Tabella.Rows(D).Item(6)) Then
                Appoggio = 0
            Else
                Appoggio = Tabella.Rows(D).Item(6).ToString
            End If
            If CDbl(Appoggio) <> 0 Then
                cn.Open()
                Dim cmd As New OleDbCommand()
                cmd.CommandText = ("Delete from ADDACR where Data = ? And Progressivo = " & Val(Tabella.Rows(D).Item(2).ToString))
                Dim Txt_DataText As Date = Txt_Data.Text
                cmd.Parameters.AddWithValue("@Data", Txt_DataText)
                cmd.Connection = cn
                cmd.ExecuteNonQuery()
                cn.Close()

                Tabella.Rows.RemoveAt(D)
            End If
        Next

        Grid.AutoGenerateColumns = True
        Grid.Visible = True
        Grid.DataSource = Tabella
        Grid.DataBind()
        System.Web.HttpRuntime.Cache("TabellaNome" + Session.SessionID) = Tabella
    End Sub

    Protected Sub Btn_CancellaComune_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles Btn_CancellaComune.Click

        Dim cn As OleDbConnection
        Dim MySql As String
        Dim D As Long
        Dim Appoggio As Double
        MySql = ""

        cn = New Data.OleDb.OleDbConnection(Session("DC_OSPITE"))


        Tabella = System.Web.HttpRuntime.Cache("TabellaNome" + Session.SessionID)

        For D = Tabella.Rows.Count - 1 To 0 Step -1

            If IsDBNull(Tabella.Rows(D).Item(7)) Then
                Appoggio = 0
            Else
                Appoggio = Tabella.Rows(D).Item(7).ToString
            End If
            If CDbl(Appoggio) <> 0 Then
                cn.Open()
                Dim cmd As New OleDbCommand()
                cmd.CommandText = ("Delete from ADDACR where Data = ? And Progressivo = " & Val(Tabella.Rows(D).Item(2).ToString))
                Dim Txt_DataText As Date = Txt_Data.Text
                cmd.Parameters.AddWithValue("@Data", Txt_DataText)
                cmd.Connection = cn
                cmd.ExecuteNonQuery()
                cn.Close()

                Tabella.Rows.RemoveAt(D)
            End If
        Next

        Grid.AutoGenerateColumns = True
        Grid.Visible = True
        Grid.DataSource = Tabella
        Grid.DataBind()
        System.Web.HttpRuntime.Cache("TabellaNome" + Session.SessionID) = Tabella
    End Sub

    Protected Sub Btn_CancellaRegione_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles Btn_CancellaRegione.Click
        If IsNothing(Session("TabellaNome")) Then Exit Sub


        Dim cn As OleDbConnection
        Dim MySql As String
        Dim D As Long
        Dim Appoggio As Double
        MySql = ""

        cn = New Data.OleDb.OleDbConnection(Session("DC_OSPITE"))


        Tabella = System.Web.HttpRuntime.Cache("TabellaNome" + Session.SessionID)

        For D = 0 To Tabella.Rows.Count - 1

            If IsDBNull(Tabella.Rows(D).Item(9)) Then
                Appoggio = 0
            Else
                Appoggio = Tabella.Rows(D).Item(9).ToString
            End If
            If CDbl(Appoggio) <> 0 Then
                cn.Open()
                Dim cmd As New OleDbCommand()
                cmd.CommandText = ("Delete from ADDACR where Data = ? And Progressivo = " & Val(Tabella.Rows(D).Item(2).ToString))
                Dim Txt_DataText As Date = Txt_Data.Text
                cmd.Parameters.AddWithValue("@Data", Txt_DataText)
                cmd.Connection = cn
                cmd.ExecuteNonQuery()
                cn.Close()

                Tabella.Rows.RemoveAt(D)
            End If
        Next

        Grid.AutoGenerateColumns = True
        Grid.Visible = True
        Grid.DataSource = Tabella
        Grid.DataBind()
        System.Web.HttpRuntime.Cache("TabellaNome" + Session.SessionID) = Tabella
    End Sub

    Private Sub EseguiJS()
        Dim MyJs As String
        MyJs = "$(document).ready(function() {"

        MyJs = MyJs & "var els = document.getElementsByTagName(""*"");"

        MyJs = MyJs & "for (var i=0;i<els.length;i++)"
        MyJs = MyJs & "if ( els[i].id ) { "
        MyJs = MyJs & " var appoggio =els[i].id; "
        MyJs = MyJs & " if (appoggio.match('Txt_Data')!= null) {  "
        MyJs = MyJs & " $(els[i]).mask(""99/99/9999"");"
        MyJs = MyJs & " $(els[i]).datepicker({ changeMonth: true,changeYear: true,  yearRange: ""1990:" & Year(Now) + 5 & """},$.datepicker.regional[""it""]);"
        MyJs = MyJs & "    }"

        MyJs = MyJs & "} "
        MyJs = MyJs & "});"

        'MyJs = "$(document).ready(function() { $('.myClass').keypress(function() { handleEnter($('.myClass').val(), true, true); } );     $('#" & Txt_ImportoPagato.ClientID & "').blur(function() { var ap = formatNumber ($('#" & Txt_ImportoPagato.ClientID & "').val(),2); $('#" & Txt_ImportoPagato.ClientID & "').val(ap); }); });"
        ScriptManager.RegisterStartupScript(Me, Me.GetType(), "visualizzaRitIm", MyJs, True)
    End Sub

    Protected Sub Btn_CancellaJolly_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles Btn_CancellaJolly.Click

        If IsNothing(Session("TabellaNome")) Then Exit Sub


        Dim cn As OleDbConnection
        Dim MySql As String
        Dim D As Long
        Dim Appoggio As Double
        MySql = ""

        cn = New Data.OleDb.OleDbConnection(Session("DC_OSPITE"))


        Tabella = Session("TabellaNome")

        For D = Tabella.Rows.Count - 1 To 0 Step -1

            If IsDBNull(Tabella.Rows(D).Item(10)) Then
                Appoggio = 0
            Else
                Appoggio = Tabella.Rows(D).Item(10).ToString
            End If
            If CDbl(Appoggio) <> 0 Then
                cn.Open()
                Dim cmd As New OleDbCommand()
                cmd.CommandText = ("Delete from ADDACR where Data = ? And Progressivo = " & Val(Tabella.Rows(D).Item(2).ToString))
                Dim Txt_DataText As Date = Txt_Data.Text
                cmd.Parameters.AddWithValue("@Data", Txt_DataText)
                cmd.Connection = cn
                cmd.ExecuteNonQuery()
                cn.Close()

                Tabella.Rows.RemoveAt(D)
            End If
        Next

        Grid.AutoGenerateColumns = True
        Grid.Visible = True
        Grid.DataSource = Tabella
        Grid.DataBind()
        Application(TabellaNome) = Tabella
    End Sub

    Protected Sub Btn_Esci_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles Btn_Esci.Click
        Response.Redirect("Menu_Ospiti.aspx")
    End Sub

    Protected Sub Lnk_ToExcel_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles Lnk_ToExcel.Click


        Tabella = Session("TabellaNome")
        Session("GrigliaSoloStampa") = Session("TabellaNome")
        ScriptManager.RegisterClientScriptBlock(Me, Me.GetType(), "Stampa", "window.open('ExportExcel.aspx','Excel','width=800,height=600');", True)
    End Sub
End Class

