﻿<%@ Page Language="VB" AutoEventWireup="false" Inherits="OspitiWeb_Menu_Tabelleaspx" CodeFile="Menu_Tabelle.aspx.vb" %>

<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
    <title>Menu Tabelle</title>
    <asp:PlaceHolder runat="server">
        <%: System.Web.Optimization.Styles.Render("~/Content/AjaxControlToolkit/Styles/Bundle") %>
    </asp:PlaceHolder>
    <link rel="stylesheet" href="ospiti.css?ver=11" type="text/css" />
    <link rel="shortcut icon" href="images/SENIOR.ico" />
    <link href="js/jquery.autocomplete.css" rel="stylesheet" type="text/css" />

    <script src="js/jquery-1.5.1.min.js" type="text/javascript"></script>
    <script src="js/jquery.autocomplete.js" type="text/javascript"></script>
    <script src="js/jquery.maskedinput-1.3.min.js" type="text/javascript"></script>
    <script src="js/NoEnter.js" type="text/javascript"></script>
    <script type="text/javascript">  
        $(document).ready(function () {
            if (window.innerHeight > 0) { $("#BarraLaterale").css("height", (window.innerHeight - 94) + "px"); } else { $("#BarraLaterale").css("height", (document.documentElement.offsetHeight - 94) + "px"); }
        });
    </script>
</head>
<body>
    <form id="form1" runat="server">
        <asp:ScriptManager ID="ScriptManager1" runat="server" EnableScriptGlobalization="true">
            <Scripts>
                <asp:ScriptReference Path="~/Scripts/AjaxControlToolkit/Bundle" />
            </Scripts>
        </asp:ScriptManager>
        <asp:Label ID="Lbl_BarraSenior" runat="server" Text=""></asp:Label>
        <div>
            <table style="width: 100%;" cellpadding="0" cellspacing="0">
                <tr>
                    <td style="width: 160px; background-color: #F0F0F0;"></td>
                    <td>
                        <div class="Titolo">Ospiti - Tabelle</div>
                        <div class="SottoTitolo">
                            <br />
                            <br />
                        </div>
                    </td>

                    <td style="text-align: right; vertical-align: top;">
                        <span class="BenvenutoText"><font size="4">Benvenuto <%=Session("UTENTE")%></font></span>
                    </td>
                </tr>
                <tr>

                    <td style="width: 160px; background-color: #F0F0F0; text-align: center; vertical-align: top;" id="BarraLaterale">
                        <asp:ImageButton ID="Btn_Esci" runat="server" BackColor="Transparent" ImageUrl="../images/Menu_Indietro.png" ToolTip="Chiudi" class="Effetto" />
                    </td>
                    <td colspan="2" valign="top">
                        <table style="width: 60%;">
                            <tr>
                                <td style="text-align: center;">
                                    <a href="ElencoComuni.aspx">
                                        <img src="../images/Menu_Tabelle.png" alt="Comuni" class="Effetto" style="border-width: 0;"></a>
                                </td>
                                <td style="text-align: center;">
                                    <a href="ElencoRegioni.aspx">
                                        <img src="../images/Menu_Tabelle.png" alt="Regioni" class="Effetto" style="border-width: 0;"></a>
                                </td>
                                <td style="text-align: center;">
                                    <a href="ElencoTipoAddebito.aspx">
                                        <img src="../images/Menu_Tabelle.png" alt="Tipo Addebito/Accredito" class="Effetto" style="border-width: 0;"></a>
                                </td>
                                <td style="text-align: center;">
                                    <a href="TabelleDescrittive.aspx">
                                        <img src="../images/Menu_Tabelle.png" alt="Tabella descrittive" class="Effetto" style="border-width: 0;"></a>
                                </td>
                                <td style="text-align: center;">
                                    <a href="ElencoMedici.aspx">
                                        <img alt="Gestione Medici" src="../images/Menu_Tabelle.png" class="Effetto" style="border-width: 0;"></a>
                                </td>
                            </tr>
                            <tr>

                                <td style="text-align: center; vertical-align: top;"><span class="MenuText">COMUNI</span></td>
                                <td style="text-align: center; vertical-align: top;"><span class="MenuText">REGIONE</span></td>
                                <td style="text-align: center; vertical-align: top;"><span class="MenuText">TIPO ADD/ACR</span></td>
                                <td style="text-align: center; vertical-align: top;"><span class="MenuText">TAB.DESCR.</span></td>
                                <td style="text-align: center; vertical-align: top;"><span class="MenuText">MEDICI</span></td>
                            </tr>


                            <tr>

                                <td style="text-align: center;">
                                    <a href="Elenco_TipoOperatore.aspx">
                                        <img src="../images/Menu_Tabelle.png" alt="Tabella Tipo Operazione" class="Effetto" style="border-width: 0;"></a>
                                </td>
                                <td style="text-align: center;">
                                    <a href="Elenco_Stanze.aspx">
                                        <img src="../images/Menu_Tabelle.png" alt="Stanze" class="Effetto" style="border-width: 0;"></a>
                                </td>
                                <td style="text-align: center;">
                                    <a href="TabelleDescrittiveOspitiAccessori.aspx">
                                        <img src="../images/Menu_Tabelle.png" alt="Tabella descrittive Stanze" class="Effetto" style="border-width: 0;"></a>
                                </td>
                                <td style="text-align: center;">
                                    <a href="ElencoExtraFissi.aspx">
                                        <img src="../images/Menu_Tabelle.png" alt="Tabella Extra Fissi" class="Effetto" style="border-width: 0;"></a>
                                </td>
                                <td style="text-align: center;">
                                    <a href="ElencoNoteFatture.aspx">
                                        <img src="../images/Menu_Tabelle.png" alt="Listino" class="Effetto" style="border-width: 0;"></a></td>
                            </tr>
                            <tr>



                                <td style="text-align: center; vertical-align: top;"><span class="MenuText">TIPO OPERATORE</span></td>
                                <td style="text-align: center; vertical-align: top;"><span class="MenuText">STANZE</span></td>
                                <td style="text-align: center; vertical-align: top;"><span class="MenuText">DESCR.STANZE</span></td>
                                <td style="text-align: center; vertical-align: top;"><span class="MenuText">EXTRA FISSI</span></td>
                                <td style="text-align: center; vertical-align: top;"><span class="MenuText">NOTE FATTURE</span></td>
                            </tr>


                            <tr>
                                <td style="text-align: center;">
                                    <a href="ElencoTipoRetta.aspx">
                                        <img src="../images/Menu_Tabelle.png" alt="Tabella Tipo Retta" class="Effetto" style="border-width: 0;"></a>
                                </td>
                                <td style="text-align: center;">
                                    <a href="Elenco_Listino.aspx">
                                        <img src="../images/Menu_Tabelle.png" alt="Tabella Listini" class="Effetto" style="border-width: 0;"></a>
                                </td>
                                <td style="text-align: center;">
                                    <a href="Elenco_TabellaTipoImportoRegione.aspx">
                                        <img src="../images/Menu_Tabelle.png" alt="Tabella Tipo Importo Regione" class="Effetto" style="border-width: 0;"></a>
                                </td>
                                <td style="text-align: center;">
                                    <a href="ElencoTipoSconto.aspx">
                                        <img src="../images/Menu_Tabelle.png" alt="Tabella Tipo Sconto" class="Effetto" style="border-width: 0;"></a>
                                </td>
                                <td style="text-align: center;">
                                    <a href="Elenco_Banche.aspx">
                                        <img src="../images/Menu_Tabelle.png" alt="Tabella Banche" class="Effetto" style="border-width: 0;"></a>
                                </td>
                            </tr>
                            <tr>
                                <td style="text-align: center; vertical-align: top;"><span class="MenuText">TIPO RETTA</span></td>
                                <td style="text-align: center; vertical-align: top;"><span class="MenuText">LISTINI</span></td>

                                <td style="text-align: center; vertical-align: top;"><span class="MenuText">TIPO IMP.</span></td>
                                <td style="text-align: center; vertical-align: top;"><span class="MenuText">TIPO SCONTO</span></td>
                                <td style="text-align: center; vertical-align: top;"><span class="MenuText">BANCHE</span></td>
                            </tr>

                            <tr>
                                <td style="text-align: center;">
                                    <a href="Elenco_TipoDomiciliare.aspx">
                                        <img src="../images/Menu_Tabelle.png" alt="Tabella Tipo Domiciliare" class="Effetto" style="border-width: 0;"></a>
                                </td>
                                <td style="text-align: center;">
                                    <a href="Elenco_Operatori.aspx">
                                        <img src="../images/Menu_Tabelle.png" alt="Tabella Operatori" class="Effetto" style="border-width: 0;"></a>
                                </td>
                                <td style="text-align: center;">
                                    <a href="Elenco_RaggruppamentoOperatori.aspx">
                                        <img src="../images/Menu_Tabelle.png" alt="Tabella Listini" class="Effetto" style="border-width: 0;"></a>
                                </td>
                                <td style="text-align: center;"></td>
                                <td style="text-align: center;">
                                    <a href="Menu_Config.aspx">
                                        <img src="../images/Menu_Config.png" alt="Menu Configurazione" class="Effetto" style="border-width: 0;"></a>
                                </td>
                            </tr>
                            <tr>

                                <td style="text-align: center; vertical-align: top;"><span class="MenuText">TIPOL.DOMIC.</span></td>
                                <td style="text-align: center; vertical-align: top;"><span class="MenuText">OPERATORI</span></td>
                                <td style="text-align: center; vertical-align: top;"><span class="MenuText">RAGGRUP. OPER.</span></td>
                                <td style="text-align: center; vertical-align: top;"><span class="MenuText"></span></td>
                                <td style="text-align: center; vertical-align: top;"><span class="MenuText">MENU CONFIG</span></td>
                            </tr>

                        </table>
                    </td>
                </tr>

                <tr>
                    <td style="width: 160px; background-color: #F0F0F0;">&nbsp;<br />
                        &nbsp;<br />
                        &nbsp;<br />
                        &nbsp;<br />
                        &nbsp;<br />
                        &nbsp;<br />
                        &nbsp;<br />
                        &nbsp;<br />
                        &nbsp;<br />
                    </td>
                    <td></td>
                    <td></td>
                </tr>
            </table>


        </div>
    </form>
</body>
</html>
