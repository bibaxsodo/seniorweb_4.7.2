﻿<%@ Page Language="VB" AutoEventWireup="false" Inherits="OspitiWeb_Export_FE" CodeFile="Export_FE.aspx.vb" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="ajtsauro" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
    <meta http-equiv="x-ua-compatible" content="IE=9" />
    <title>Export FE</title>
    <asp:PlaceHolder runat="server">
        <%: System.Web.Optimization.Styles.Render("~/Content/AjaxControlToolkit/Styles/Bundle") %>
    </asp:PlaceHolder>
    <link href="ospiti.css?ver=11" rel="stylesheet" type="text/css" />
    <link rel="shortcut icon" href="images/SENIOR.ico" />
    <link href="js/jquery.autocomplete.css" rel="stylesheet" type="text/css" />


    <script src="js/jquery-1.5.1.min.js" type="text/javascript"></script>
    <script src="js/jquery.autocomplete.js" type="text/javascript"></script>
    <script src="js/soapclient.js" type="text/javascript"></script>
    <script src="/js/convertinumero.js" type="text/javascript"></script>



    <script src="js/jquery.maskedinput-1.3.min.js" type="text/javascript"></script>
    <script src="/js/formatnumer.js" type="text/javascript"></script>
    <script src="js/JSErrore.js" type="text/javascript"></script>

    <script type="text/javascript">  
        $(document).ready(function () {
            if (window.innerHeight > 0) { $("#BarraLaterale").css("height", (window.innerHeight - 94) + "px"); } else { $("#BarraLaterale").css("height", (document.documentElement.offsetHeight - 94) + "px"); }
        });
    </script>
    <style>
        .wait {
            border: solid 1px #2d2d2d;
            text-align: center;
            vertical-align: top;
            background: white;
            width: 20%;
            height: 180px;
            top: 30%;
            left: 40%;
            position: absolute;
            -moz-border-radius: 5px;
            -webkit-border-radius: 5px;
            border-radius: 5px;
            box-shadow: 0px 0px 10px 4px rgba(0, 125, 196, 0.75);
            -moz-box-shadow: 0px 0px 10px 4px rgba(0, 125, 196, 0.75);
            -webkit-box-shadow: 0px 0px 10px 4px rgba(0, 125, 196, 0.75);
        }

        #blur {
            width: 100%;
            background-color: black;
            moz-opacity: 0.5;
            khtml-opacity: .5;
            opacity: .5;
            filter: alpha(opacity=50);
            z-index: 120;
            height: 100%;
            position: absolute;
            top: 0;
            left: 0;
        }

        #progress {
            z-index: 200;
            background-color: White;
            position: absolute;
            top: 0pt;
            left: 0pt;
            border: solid 1px black;
            padding: 5px 5px 5px 5px;
            text-align: center;
        }
    </style>

</head>
<body>
    <form id="form1" runat="server">
        <asp:ScriptManager ID="ScriptManager1" runat="server" EnableScriptGlobalization="true">
            <Scripts>
                <asp:ScriptReference Path="~/Scripts/AjaxControlToolkit/Bundle" />
            </Scripts>
        </asp:ScriptManager>
        <asp:Label ID="Lbl_BarraSenior" runat="server" Text=""></asp:Label>
        <div align="left">
            <table style="width: 100%;" cellpadding="0" cellspacing="0">
                <tr>
                    <td style="width: 160px; background-color: #F0F0F0;"></td>
                    <td>
                        <div class="Titolo">Ospiti - Strumenti - Export FE</div>
                        <div class="SottoTitolo">
                            <asp:Label ID="Lbl_NomeOspite" runat="server"></asp:Label><br />
                            <br />
                        </div>
                    </td>
                    <td style="text-align: right;">
                        <span class="BenvenutoText">Benvenuto <%=Session("UTENTE")%></span>
                        <asp:ImageButton runat="server" ImageUrl="~/images/elabora.png" class="EffettoBottoniTondi"
                            Height="38px" ToolTip="Esegui" ID="Btn_Modifica"></asp:ImageButton>
                    </td>
                </tr>

                <tr>
                    <td style="width: 160px; background-color: #F0F0F0; vertical-align: top; text-align: center;" id="BarraLaterale">
                        <a href="Menu_Ospiti.aspx" style="border-width: 0px;">
                            <img src="images/Home.jpg" class="Effetto" alt="Menù" /></a><br />
                        <asp:ImageButton ID="Btn_Esci" runat="server" BackColor="Transparent" ImageUrl="../images/Menu_Indietro.png" ToolTip="Chiudi" />
                        <br />


                        <br />
                        <br />
                        <br />
                        <br />
                        <br />
                        <br />
                        <br />
                        <br />
                        <br />
                        <br />
                        <br />
                        <br />
                    </td>
                    <td colspan="2" style="background-color: #FFFFFF;" valign="top">
                        <ajtsauro:TabContainer ID="TabContainer1" runat="server" ActiveTabIndex="0" CssClass="TabSenior" Width="100%" BorderStyle="None" Style="margin-right: 39px">
                            <ajtsauro:TabPanel runat="server" HeaderText="Prima Nota" ID="Tab_Anagrafica">
                                <HeaderTemplate>
                                    Export FE
                                </HeaderTemplate>
                                <ContentTemplate>

                                    <label class="LabelCampo">Struttura:</label>
                                    &nbsp;&nbsp;<asp:DropDownList runat="server" ID="DD_Struttura" AutoPostBack="True"></asp:DropDownList><br />
                                    <br />

                                    <label class="LabelCampo">Centro Servizio : </label>
                                    <asp:DropDownList ID="Cmb_CServ" runat="server" Width="288px"></asp:DropDownList><br />
                                    <br />



                                    <label class="LabelCampo">Anno :</label>
                                    &nbsp;&nbsp;<asp:TextBox ID="Txt_Anno" AutoPostBack="True" MaxLength="4" onkeypress="return soloNumeri(event);" runat="server" Width="64px"></asp:TextBox>
                                    <br />
                                    <br />


                                    <label class="LabelCampo">Trimerstre :</label>
                                    &nbsp;&nbsp;<asp:DropDownList ID="DD_Mese" runat="server" Width="128px">
                                        <asp:ListItem Value="1">1 Trimestre</asp:ListItem>
                                        <asp:ListItem Value="2">2 Trimestre</asp:ListItem>
                                        <asp:ListItem Value="3">3 Trimestre</asp:ListItem>
                                        <asp:ListItem Value="4">4 Trimestre</asp:ListItem>
                                    </asp:DropDownList>
                                    <br />
                                    <br />
                                    <br />
                                    <asp:Label ID="Lbl_Errori" runat="server"></asp:Label>
                                    <br />
                                    <div id="msg"></div>

                                    <asp:UpdatePanel ID="UpdatePanel2" runat="server">
                                        <ContentTemplate>
                                            <asp:Timer ID="Timer1" runat="server" Interval="1000">
                                            </asp:Timer>
                                            <asp:Label ID="Lbl_Waiting" runat="server" Text="Label"></asp:Label>

                                        </ContentTemplate>
                                    </asp:UpdatePanel>
                                </ContentTemplate>


                            </ajtsauro:TabPanel>
                        </ajtsauro:TabContainer>
                    </td>
                </tr>
            </table>
        </div>
    </form>
</body>
</html>
