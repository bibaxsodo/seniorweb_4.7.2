﻿Imports System.Web.Hosting
Imports System.Diagnostics
Imports System.ComponentModel
Imports System
Imports System.Xml
Imports System.Xml.Schema
Imports System.IO
Imports System.Data
Imports System.Data.OleDb
Partial Class OspitiWeb_Frm_EstrazionePer730
    Inherits System.Web.UI.Page
    Dim MyTable As New System.Data.DataTable("tabella")
    Dim MyDataSet As New System.Data.DataSet()


    Private Function TotalePerContoContabile(ByVal Registrazione As Cls_MovimentoContabile, ByVal InpConto1 As String, ByVal InpConto2 As String, ByVal InpConto3 As String, ByVal InpConto4 As String, ByVal InpConto5 As String, ByVal InpConto6 As String, ByVal InpConto7 As String, ByVal InpConto8 As String, ByVal IncludiSoloSeEsente1 as Boolean,ByVal IncludiSoloSeEsente2 as Boolean,ByVal IncludiSoloSeEsente3 as Boolean) As Double
        Dim Indice As Integer =0
        Dim Mastro(100) As Integer 
        Dim Conto(100) As Integer  
        Dim Sottoconto(100) As Integer  
        Dim ControlloConti(100) as String
        

        Dim Vettore(10) as String

        ControlloConti(0) =InpConto1
        ControlloConti(1) =InpConto2
        ControlloConti(2) =InpConto3
        ControlloConti(3) =InpConto4
        ControlloConti(4) =InpConto5
        ControlloConti(5) =InpConto6
        ControlloConti(6) =InpConto7
        ControlloConti(7) =InpConto8

        For  i= 0 To 7 
            If ControlloConti(i)  <>"" then 
                Vettore = SplitWords(ControlloConti(i))

                Mastro(i) = Vettore(0)
                conto(i) = Vettore(1)
                Sottoconto(i)  = Vettore(2)
            End If

        Next

        Dim CausaleContabile As New Cls_CausaleContabile

        CausaleContabile.Codice = Registrazione.CausaleContabile
        CausaleContabile.Leggi(Session("DC_TABELLE"), CausaleContabile.Codice)


        TotalePerContoContabile=0
        For Indice =0 To 300
            If Not IsNothing(Registrazione.Righe(Indice)) Then
                Dim Segno As String ="A"

                If CausaleContabile.TipoDocumento = "NC" Then
                    Segno ="D"
                End If
                For  i= 0 To 7 
                    If Registrazione.Righe(Indice).MastroPartita = Mastro(i) And Registrazione.Righe(Indice).ContoPartita = Conto(i) And Registrazione.Righe(Indice).SottocontoPartita = Sottoconto(i) Then
                        Dim ConsideraInTotale as Boolean = True
                        Dim IVA as new Cls_IVA

                        iva.Aliquota =0
                        if Registrazione.Righe(Indice).CodiceIVA <> "" then
                            IVa.Codice = Registrazione.Righe(Indice).CodiceIVA
                            iva.Leggi(Session("DC_TABELLE"), iva.Codice)
                        end if

                        if i =0 and IncludiSoloSeEsente1 = True then
                            if iva.Aliquota>0 then
                                ConsideraInTotale = False
                                else
                                ConsideraInTotale = True
                            End If
                        End If
                        if i =1 and IncludiSoloSeEsente2 = True then
                            if iva.Aliquota>0 then
                                ConsideraInTotale = False
                                else
                                ConsideraInTotale = True
                            End If
                        End If
                        if i =2 and IncludiSoloSeEsente3 = True then
                            if iva.Aliquota>0 then
                                ConsideraInTotale = False
                                else
                                ConsideraInTotale = True
                            End If
                        End If

                        Dim SommaIVA as Double =0

                        SommaIVA = Registrazione.Righe(Indice).importo
                        if iva.Aliquota > 0 then
                            SommaIVA = SommaIVA + Modulo.MathRound(SommaIVA * iva.Aliquota,2)
                        End If

                        if ConsideraInTotale then
                            If Registrazione.Righe(Indice).DareAvere =Segno THEN
                                TotalePerContoContabile += SommaIVA
                            Else
                                TotalePerContoContabile -= SommaIVA
                            End IF
                        End IF
                    End If
                Next
            End If
        Next

    End Function

    Private Sub Esport730()
        Dim cn As OleDbConnection


         If DD_CausaleIncasso.SelectedValue = "" Then
            ScriptManager.RegisterStartupScript(Me, Me.GetType(), "Errore", "VisualizzaErrore('Devi indicare una causale di incasso');", True)
            Exit Sub
        End If



        lbl_errori.Text =""

        cn = New Data.OleDb.OleDbConnection(Session("DC_GENERALE"))

        cn.Open()

        MyTable.Clear()
        MyTable.Columns.Clear()

        MyTable.Columns.Add("Nome", GetType(String))
        MyTable.Columns.Add("codicefiscale", GetType(String))
        MyTable.Columns.Add("datadocumento", GetType(String))
        MyTable.Columns.Add("numerodocumento", GetType(String))
        MyTable.Columns.Add("datapagamento", GetType(String))
        MyTable.Columns.Add("flagoperazione", GetType(String))
        MyTable.Columns.Add("tipospesa", GetType(String))
        MyTable.Columns.Add("importo", GetType(String))

        MyTable.Columns.Add("Data Documento Rimborso", GetType(String))
        MyTable.Columns.Add("Numero Documento Rimborso", GetType(String))


        If Chk_Sosia.Checked Then
            MyTable.Columns.Add("Classe", GetType(String))
            MyTable.Columns.Add("Tipologia", GetType(String))
            MyTable.Columns.Add("DataAccoglimento", GetType(String))
            MyTable.Columns.Add("DataUscitaDefinitiva", GetType(String))
            MyTable.Columns.Add("Quota", GetType(String))

            MyTable.Columns.Add("Totale Dare (periodo)", GetType(String))
            MyTable.Columns.Add("Totale Avere (periodo)", GetType(String))

        End If


        Dim cmd As New OleDbCommand()



        cmd.CommandText = "Select * From MovimentiContabiliTesta  where DataRegistrazione >= ? And DataRegistrazione <= ? And ( CausaleContabile = ?"

        cmd.Parameters.AddWithValue("@DataRegistrazioneDal", Txt_DataDalIncasso.Text)
        cmd.Parameters.AddWithValue("@DataRegistrazioneAl", Txt_DataAlIncasso.Text)
        cmd.Parameters.AddWithValue("@CausaleContabile", DD_CausaleIncasso.SelectedValue)
        If DD_CausaleIncasso1.SelectedValue <> "" Then
            cmd.CommandText = cmd.CommandText & " OR CausaleContabile = ? "
            cmd.Parameters.AddWithValue("@CausaleContabile", DD_CausaleIncasso1.SelectedValue)
        End If
        If DD_CausaleIncasso2.SelectedValue <> "" Then
            cmd.CommandText = cmd.CommandText & " OR CausaleContabile = ? "
            cmd.Parameters.AddWithValue("@CausaleContabile", DD_CausaleIncasso2.SelectedValue)
        End If
        If DD_CausaleIncasso3.SelectedValue <> "" Then
            cmd.CommandText = cmd.CommandText & " OR CausaleContabile = ? "
            cmd.Parameters.AddWithValue("@CausaleContabile", DD_CausaleIncasso3.SelectedValue)
        End If
        If DD_CausaleIncasso4.SelectedValue <> "" Then
            cmd.CommandText = cmd.CommandText & " OR CausaleContabile = ? "
            cmd.Parameters.AddWithValue("@CausaleContabile", DD_CausaleIncasso4.SelectedValue)
        End If
        cmd.CommandText = cmd.CommandText & " )"
        cmd.Connection = cn
        Dim RsOspiteParente As OleDbDataReader = cmd.ExecuteReader()
        Do While RsOspiteParente.Read

            Dim TrovatoLegame As Boolean =False

            Dim cmdLegami As New OleDbCommand()

            cmdLegami.CommandText = "Select * From TabellaLegami Where CodicePagamento = ? "
            cmdLegami.Parameters.AddWithValue("@Legami", campodbN(RsOspiteParente.Item("NumeroRegistrazione")))
            cmdLegami.Connection = cn
            Dim RsLegami As OleDbDataReader = cmdLegami.ExecuteReader
            Do While RsLegami.Read
                TrovatoLegame =True
                Dim Documento As New Cls_MovimentoContabile

                Documento.NumeroRegistrazione = campodbN(RsLegami.Item("CodiceDocumento"))
                Documento.Leggi(Session("DC_GENERALE"), Documento.NumeroRegistrazione)


                Dim FlagAccettato As Boolean = False
                If Documento.Tipologia = "" Or Documento.Tipologia = "O" Or Documento.Tipologia = "P" Then
                    FlagAccettato = True
                End If

                If DD_CausaleDocumento_inc.SelectedValue <> "" Or DD_CausaleDocumento_inc2.SelectedValue <> "" Or DD_CausaleDocumento_inc3.SelectedValue <> ""  Or DD_CausaleDocumento_inc4.SelectedValue <> ""   Or DD_CausaleDocumento_inc5.SelectedValue <> ""   Or DD_CausaleDocumento_Inc6.SelectedValue <> ""  Then
                    If DD_CausaleDocumento_inc.SelectedValue <> Documento.CausaleContabile And _
                        DD_CausaleDocumento_inc2.SelectedValue <> Documento.CausaleContabile And _
                        DD_CausaleDocumento_inc3.SelectedValue <> Documento.CausaleContabile  And _
                        DD_CausaleDocumento_inc4.SelectedValue <> Documento.CausaleContabile And _ 
                        DD_CausaleDocumento_inc5.SelectedValue <> Documento.CausaleContabile And _
                        DD_CausaleDocumento_inc6.SelectedValue <> Documento.CausaleContabile And _
                        DD_CausaleDocumento_inc7.SelectedValue <> Documento.CausaleContabile And _
                        DD_CausaleDocumento_inc8.SelectedValue <> Documento.CausaleContabile Then
                        FlagAccettato = False
                    End If
                End If

                If FlagAccettato Then
                    Dim Sottoconto As Integer
                    Dim Nome As String = ""
                    Dim CodiceFiscale As String = ""

                    Sottoconto = Documento.Righe(0).SottocontoPartita

                    If Int(Sottoconto / 100) = Math.Round(Sottoconto / 100, 2) Then
                        Dim Ospite As New ClsOspite

                        Ospite.CodiceOspite = Int(Sottoconto / 100)
                        Ospite.Leggi(Session("DC_OSPITE"), Ospite.CodiceOspite)

                        Nome = Ospite.Nome
                        CodiceFiscale = Ospite.CODICEFISCALE
                    Else
                        Dim Parenti As New Cls_Parenti

                        Parenti.CodiceOspite = Int(Sottoconto / 100)
                        Parenti.CodiceParente = Sottoconto - (Int(Sottoconto / 100) * 100)

                        Parenti.Leggi(Session("DC_OSPITE"), Parenti.CodiceOspite, Parenti.CodiceParente)


                        Nome = Parenti.Nome
                        CodiceFiscale = Parenti.CODICEFISCALE
                    End If


                    If DD_CServ2.SelectedValue = "" Or (DD_CServ2.SelectedValue <> "" And Documento.CentroServizio = DD_CServ2.SelectedValue) Then

                        Dim myriga1 As System.Data.DataRow = MyTable.NewRow()

                        myriga1(0) = Nome
                        myriga1(1) = CodiceFiscale
                        myriga1(2) = Format(Documento.DataDocumento, "dd/MM/yyyy")

                        Dim Registro As New Cls_RegistroIVA

                        Registro.Tipo = Documento.RegistroIVA
                        Registro.Leggi(Session("DC_TABELLE"),Registro.Tipo)

                        If Registro.IndicatoreRegistro <>"" THEN
                            myriga1(3) = Documento.NumeroProtocollo & "/" & Registro.IndicatoreRegistro
                          Else
                           myriga1(3) = Documento.NumeroProtocollo 
                        End IF
                        myriga1(4) = Format(campodbD(RsOspiteParente.Item("DataRegistrazione")), "dd/MM/yyyy")

                        Dim CausaleContabile As New Cls_CausaleContabile

                        CausaleContabile.Codice = Documento.CausaleContabile
                        CausaleContabile.Leggi(Session("DC_TABELLE"), CausaleContabile.Codice)

                        If CausaleContabile.TipoDocumento = "NC" Then
                            myriga1(8) = Format( Documento.RifData,"dd/MM/yyyy")
                            myriga1(9) = Documento.RifNumero

                            myriga1(5) = "RI"
                        Else
                            myriga1(5) = "IN"
                        End If
                        myriga1(6) = "AA"

                        Dim TotDocumento As Double = 0
                        Dim TotIncasso As Double = 0
                        Dim Proporzione As Double = 0
                        TotDocumento = Documento.ImportoDocumento(Session("DC_TABELLE"))

                        TotIncasso = Math.Abs(campodbN(RsLegami.Item("Importo")))
                        

                        If (Txt_ContoContabile_1.Text.Trim <> "") Then
                             Dim TotaleDocumentoContiRicavo As Double =0

                            TotaleDocumentoContiRicavo = TotalePerContoContabile(Documento, Txt_ContoContabile_1.Text.Trim,Txt_ContoContabile2_1.Text.Trim,Txt_ContoContabile3_1.Text.Trim,"","","","","", Chk_IncludiSoloSeEsente.Checked, Chk_IncludiSoloSeEsente1.Checked, Chk_IncludiSoloSeEsente2.Checked)
                            
                            Proporzione = Math.Round(TotaleDocumentoContiRicavo / TotDocumento ,5)
                            if TotDocumento = TotIncasso Then
                                 
                                 myriga1(7) = Math.Abs( Math.Round(  TotaleDocumentoContiRicavo,2) )
                                Else                                
                                 myriga1(7) = Math.Abs( Math.Round(  Math.Abs(campodbN(RsLegami.Item("Importo"))) * Proporzione,2) )
                            End If
 
                           else
                            myriga1(7) = Math.Abs(campodbN(RsLegami.Item("Importo"))) 
                        End If


                        
                        If Chk_Sosia.Checked Then

                            myriga1("Classe") = Classe(Int(Sottoconto / 100), Documento.DataDocumento)
                            myriga1("Tipologia") = TipoClasse(Int(Sottoconto / 100), Documento.DataDocumento)
                            If  myriga1("Classe")="" And myriga1("Tipologia")="" Then
                                Dim StatoAuto As New Cls_StatoAuto

                                StatoAuto.USL =""
                                StatoAuto.CODICEOSPITE = Int(Sottoconto / 100)
                                StatoAuto.CENTROSERVIZIO = Documento.CentroServizio
                                StatoAuto.Data = Documento.DataRegistrazione
                                StatoAuto.StatoPrimaData(Session("DC_OSPITE"),StatoAuto.CODICEOSPITE,StatoAuto.CENTROSERVIZIO)
                                If isnothing(StatoAuto.USL) Then
                                    StatoAuto.USL =""
                                End If

                                If StatoAuto.USL.Trim <>"" Then
                                   Dim Usl As New ClsUSL

                                    Usl.CodiceRegione = StatoAuto.USL
                                    Usl.Leggi(Session("DC_OSPITE"))

                                    myriga1("Classe") =Usl.Nome
                                    
                                End If
                                If StatoAuto.TipoRetta.Trim <>"" Then
                                   Dim TipoRetta As New Cls_TabellaTipoImportoRegione

                                    TipoRetta.Codice = StatoAuto.TipoRetta
                                    TipoRetta.Leggi(Session("DC_OSPITE"),TipoRetta.Codice)

                                    myriga1("Tipologia") =TipoRetta.Descrizione                                    
                                End If

                               If StatoAuto.USL ="" Then
                                    Dim ListinoOspite As New Cls_Listino

                                    ListinoOspite.CENTROSERVIZIO =  Documento.CentroServizio
                                    ListinoOspite.CODICEOSPITE = Int(Sottoconto / 100)
                                    ListinoOspite.LeggiAData(Session("DC_OSPITE"), Documento.DataRegistrazione)

                                    If ListinoOspite.CodiceListino <> "" then
                                        Dim DecodificaListino As New Cls_Tabella_Listino

                                        DecodificaListino.Codice = ListinoOspite.CodiceListino
                                        DecodificaListino.LeggiCausale(Session("DC_OSPITE"))

                                        myriga1("Tipologia") =DecodificaListino.Descrizione
                                     End If
                               End If
                            End If
                            Dim Movimenti As New Cls_Movimenti
                            Dim CentroServizio As String = Documento.CentroServizio

                            Movimenti.CodiceOspite = Int(Sottoconto / 100)
                            Movimenti.CENTROSERVIZIO = Documento.CentroServizio
                            Movimenti.Data = Nothing
                            Movimenti.UltimaDataAccoglimento(Session("DC_OSPITE"))
                            If Year(Movimenti.Data) < 1990 Then
                                Movimenti.UltimaDataAccoglimentoSenzaCserv(Session("DC_OSPITE"))
                                CentroServizio = Movimenti.CENTROSERVIZIO
                            End If
                            myriga1("DataAccoglimento") = Format(Movimenti.Data, "dd/MM/yyyy")



                            Movimenti.Data = Nothing

                            Movimenti.CodiceOspite = Int(Sottoconto / 100)
                            Movimenti.CENTROSERVIZIO = Documento.CentroServizio

                            Movimenti.UltimaDataUscitaDefinitiva(Session("DC_OSPITE"))

                            If Year(Movimenti.Data) > 2000 Then
                                myriga1("DataUscitaDefinitiva") = Format(Movimenti.Data, "dd/MM/yyyy")
                            Else
                                myriga1("DataUscitaDefinitiva") = ""
                            End If



                            Dim ImportoOspite As Double = 0
                            Dim ImportoParenti As Double = 0

                            Dim x As New Cls_CalcoloRette
                            x.STRINGACONNESSIONEDB = Session("DC_OSPITE")

                            x.ApriDB(Session("DC_OSPITE"), Session("DC_OSPITIACCESSORI"))

                            ImportoOspite = Math.Round(x.QuoteGiornaliere(CentroServizio, Int(Sottoconto / 100), "O", 0, Documento.DataDocumento), 2)
                            For i = 1 To 10
                                ImportoParenti = ImportoParenti + Math.Round(CDbl(x.QuoteGiornaliere(CentroServizio, Int(Sottoconto / 100), "P", i, Documento.DataDocumento)), 2)
                            Next

                            myriga1("Quota") = Format(ImportoOspite + ImportoParenti, "#,##0.00")

                            
                            '     MyTable.Columns.Add("Totale Dare (periodo)", GetType(String))
                            myriga1("Totale Dare (periodo)") = totaleSottoConto(Documento.Righe(0).MastroPartita,Documento.Righe(0).ContoPartita,Documento.Righe(0).SottocontoPartita,Txt_DataDalIncasso.Text, Txt_DataAlIncasso.Text,"D")
                            myriga1("Totale Avere (periodo)") = totaleSottoConto(Documento.Righe(0).MastroPartita,Documento.Righe(0).ContoPartita,Documento.Righe(0).SottocontoPartita,Txt_DataDalIncasso.Text, Txt_DataAlIncasso.Text,"A")


                            x.ChiudiDB()
                        End If


                        If myriga1(7) >0 THEN
                           MyTable.Rows.Add(myriga1)
                        End If
                    End If
                End If
            Loop
            RsLegami.Close()
            If TrovatoLegame = False Then

                Dim Documento As New Cls_MovimentoContabile

                Documento.NumeroRegistrazione = campodbN(RsOspiteParente.Item("NumeroRegistrazione"))
                Documento.Leggi(Session("DC_GENERALE"), Documento.NumeroRegistrazione)


                lbl_errori.Text =lbl_errori.Text  & "Incasso  n. " & Documento.NumeroRegistrazione & " del " & Format(Documento.DataRegistrazione,"dd/MM/yyyy") & " non collegato <br/>"
            End If

        Loop
        RsOspiteParente.Close()

        cn.Close()


        Dim dataView As New System.Data.DataView(MyTable)
        dataView.Sort = "  Nome,CodiceFiscale,datadocumento,numerodocumento"        
        Dim dataTable As System.Data.DataTable = dataView.ToTable()


        If Chk_Bollo.Checked = True Then
            Dim Bollo  As New Cls_bolli

            Bollo.Leggi(Session("DC_TABELLE"),now)

            Dim Documento As String =""
            Dim OldDocumento As String =""
            Dim ImportoTotale As Double =0
            For i = 0 To dataTable.Rows.Count-1
                Documento = dataTable.Rows(i).Item("numerodocumento")
                ImportoTotale= ImportoTotale + CDbl(dataTable.Rows(i).Item("importo"))

                If Documento <> OldDocumento And OldDocumento <>"" Then
                    If ImportoTotale > Bollo.ImportoApplicazione Then
                        If dataTable.Rows(i-1).Item("importo") - Bollo.ImportoBollo > 0 then
                            If dataTable.Rows(i-1).Item("flagoperazione") ="RI" then
                               dataTable.Rows(i-1).Item("importo")= dataTable.Rows(i-1).Item("importo") + Bollo.ImportoBollo
                            else
                               dataTable.Rows(i-1).Item("importo")= dataTable.Rows(i-1).Item("importo") - Bollo.ImportoBollo
                            End if
                        End if
                    End If
                    ImportoTotale=0
                End If

                OldDocumento = dataTable.Rows(i).Item("numerodocumento")
            Next

            MyTable = dataTable
        End If

        GridView2.AutoGenerateColumns = True
        GridView2.DataSource = dataTable
        GridView2.Font.Size = 10
        GridView2.DataBind()


        lbl_errori.Text ="<font color=""red"">" & lbl_errori.Text & "</font><br/>"
    End Sub

    Private Function SplitWords(ByVal s As String) As String()
        '
        ' Call Regex.Split function from the imported namespace.
        ' Return the result array.
        '
        Return Regex.Split(s, "\W+")
    End Function

    Private Function totaleSottoConto(ByVal Mastro As Integer, ByVal Conto As Integer, ByVal Sottoconto As Long, ByVal DataInzio As Date, ByVal DataFine As Date, ByVal Segno As String) As Double
        Dim MCRs As New ADODB.Recordset
        Dim Condizione As String
        Dim MySql As String
        Dim Dare, Avere As Double

        Dim cn As OleDbConnection

        

        cn = New Data.OleDb.OleDbConnection(Session("DC_GENERALE"))

        cn.Open()


        Condizione = ""
        MySql = "SELECT  SUM(case when DAREAVERE ='D' THEN IMPORTO else 0 end) as TD,SUM(case when DAREAVERE ='A' THEN IMPORTO else 0 end) as TA FROM MovimentiContabiliTesta " & _
                    " INNER JOIN MovimentiContabiliRiga " & _
                    " ON MovimentiContabiliTesta.NumeroRegistrazione = MovimentiContabiliRiga.Numero " & _
                    " WHERE MastroPartita = " & Mastro & _
                    " AND ContoPartita = " & Conto & _
                    " AND SottocontoPartita = " & Sottoconto & _
                    " AND DataRegistrazione >= {ts'" & Format(DataInzio, "yyyy-MM-dd") & " 00:00:00'}" & _
                    " AND DataRegistrazione <= {ts'" & Format(DataFine, "yyyy-MM-dd") & " 00:00:00'}" & Condizione
        
        Dim cmd As New OleDbCommand()
        cmd.CommandText = MySql
        cmd.Connection = cn
        cmd.CommandTimeout = 120
        Dim myPOSTreader As OleDbDataReader = cmd.ExecuteReader()
        If myPOSTreader.Read Then
            Dare = campodbN(myPOSTreader.Item("TD"))
            Avere = campodbN(myPOSTreader.Item("TA"))
        End If
        myPOSTreader.Close()


        cn.Close()


        If Segno ="D" then
            totaleSottoConto = Modulo.MathRound(Dare, 2)
        End if
        If Segno ="A" then
            totaleSottoConto = Modulo.MathRound(Avere, 2)
        End if
    End Function


    Private Sub Esporta()


        Dim ImportoRetta As Double

        Dim Legami As New Cls_Legami

        Dim Quantita As Double
        Dim cn As OleDbConnection



        If   DD_CServ.SelectedValue = "" Then
           ScriptManager.RegisterStartupScript(Me, Me.GetType(), "Errore", "VisualizzaErrore('Devi indicare un centro servizio');", True)
            Exit Sub
        End If

        Dim MastroFiltro As Integer
        Dim ContoFiltro As Integer
        Dim SottocontoFiltro As Integer
        Dim Vettore(100) As String


        Vettore = SplitWords(Txt_ContoContabile.Text)


        If Val(Vettore(0)) > 0 Then
            MastroFiltro = Vettore(0)
            ContoFiltro = Vettore(1)
            SottocontoFiltro = Vettore(2)
        End If


        Vettore(0) = 0

        Dim MastroFiltro2 As Integer
        Dim ContoFiltro2 As Integer
        Dim SottocontoFiltro2 As Integer

        Vettore = SplitWords(Txt_ContoContabile2.Text)


        If Val(Vettore(0)) > 0 Then
            MastroFiltro2 = Vettore(0)
            ContoFiltro2 = Vettore(1)
            SottocontoFiltro2 = Vettore(2)
        End If



        cn = New Data.OleDb.OleDbConnection(Session("DC_OSPITE"))

        cn.Open()



        Dim cnGenerale As OleDbConnection

        cnGenerale = New Data.OleDb.OleDbConnection(Session("DC_GENERALE"))

        cnGenerale.Open()

        MyTable.Clear()
        MyTable.Columns.Clear()

        MyTable.Columns.Add("Nome", GetType(String))
        MyTable.Columns.Add("codicefiscale", GetType(String))
        MyTable.Columns.Add("datadocumento", GetType(String))
        MyTable.Columns.Add("numerodocumento", GetType(String))
        MyTable.Columns.Add("datapagamento", GetType(String))
        MyTable.Columns.Add("flagoperazione", GetType(String))
        MyTable.Columns.Add("tipospesa", GetType(String))
        MyTable.Columns.Add("importo", GetType(String))
        
        MyTable.Columns.Add("Data Documento Rimborso", GetType(String))
        MyTable.Columns.Add("Numero Documento Rimborso", GetType(String))


        If Chk_DatiAggiuntivi.Checked Then
            MyTable.Columns.Add("QuotaOspite", GetType(String))
            MyTable.Columns.Add("QuotaRegione", GetType(String))
            MyTable.Columns.Add("Stato", GetType(String))
            MyTable.Columns.Add("Classe", GetType(String))
            MyTable.Columns.Add("Tipologia", GetType(String))
            MyTable.Columns.Add("DataAccoglimento", GetType(String))
            MyTable.Columns.Add("DataUscitaDefinitiva", GetType(String))
        End If


        Dim Mastro As Integer
        Dim Parametri As New Cls_Parametri

        Parametri.LeggiParametri(Session("DC_OSPITE"))
        Mastro = Parametri.Mastro



        Dim cmdRegistrazioni As New OleDbCommand()

        Dim Condizione As String =""

        If DD_CausaleDocumento1.SelectedValue <> "" Then
            If DD_CausaleDocumento2.SelectedValue  <> "" Then
                Condizione=" And ( CausaleContabile = ?"
                Else
                Condizione=" And CausaleContabile = ?"
            End If
        End If

        If DD_CausaleDocumento2.SelectedValue  <> "" And DD_CausaleDocumento1.SelectedValue <> "" Then
            Condizione = Condizione & " OR CausaleContabile = ? )  "
        End If

        cmdRegistrazioni.CommandText = "SELECT * FROM MovimentiContabiliTesta WHERE DataRegistrazione >= ? And DataRegistrazione <= ? And NumeroProtocollo > 0 And MovimentiContabiliTesta.CentroServizio = ?" & Condizione
        cmdRegistrazioni.Parameters.AddWithValue("@DataRegistrazioneDal", Txt_DataDal.Text)
        cmdRegistrazioni.Parameters.AddWithValue("@DataRegistrazioneAl", Txt_DataAl.Text)
        cmdRegistrazioni.Parameters.AddWithValue("@CentroServizio", DD_CServ.SelectedValue)
        cmdRegistrazioni.Connection = cnGenerale
        
        If DD_CausaleDocumento1.SelectedValue  <> "" Then
            cmdRegistrazioni.Parameters.AddWithValue("@CausaleDocumento1", DD_CausaleDocumento1.SelectedValue)
        End If
        
        If DD_CausaleDocumento2.SelectedValue  <> "" Then
            cmdRegistrazioni.Parameters.AddWithValue("@CausaleDocumento", DD_CausaleDocumento2.SelectedValue)
        End If

        Dim RsRegistrazioni As OleDbDataReader = cmdRegistrazioni.ExecuteReader()
        Do While RsRegistrazioni.Read
            Dim Importare As Boolean = True
            Dim CodiceOspite As Integer = 0
            Dim CodiceParente As Integer = 0


            Dim Registrazione As New Cls_MovimentoContabile


            If campodbN(RsRegistrazioni.Item("NumeroRegistrazione")) = 3066 Then
                Registrazione.NumeroRegistrazione = 3287
            End If

            Registrazione.Leggi(Session("DC_GENERALE"), campodbN(RsRegistrazioni.Item("NumeroRegistrazione")))


            If Mid(Registrazione.Tipologia & Space(10), 1, 1) = "C" Or Mid(Registrazione.Tipologia & Space(10), 1, 1) = "R" Or Mid(Registrazione.Tipologia & Space(10), 1, 1) = "J" Then
                Importare = False
            End If



            CodiceOspite = 0
            CodiceParente = 0
            For i = 0 To 300
                If Not IsNothing(Registrazione.Righe(i)) Then
                    If Registrazione.Righe(i).Tipo = "CF" Then
                        CodiceOspite = Int(Registrazione.Righe(i).SottocontoPartita / 100)
                        CodiceParente = Registrazione.Righe(i).SottocontoPartita - (CodiceOspite * 100)

                    End If
                End If
            Next

            If CodiceOspite = 102 Then
                CodiceOspite = 102
            End If


            If (Rb_NonAuto.Checked = True Or RB_Auto.Checked = True) And Importare = True Then
                Dim StatoAuto As New Cls_StatoAuto

                StatoAuto.CENTROSERVIZIO = Registrazione.CentroServizio
                StatoAuto.CODICEOSPITE = CodiceOspite
                StatoAuto.Data = Registrazione.DataRegistrazione
                StatoAuto.StatoPrimaData(Session("DC_OSPITE"), StatoAuto.CODICEOSPITE, StatoAuto.CENTROSERVIZIO)
                If Rb_NonAuto.Checked = True Then
                    If StatoAuto.STATOAUTO = "A" Then
                        Importare = False
                    End If
                End If
                If RB_Auto.Checked = True Then
                    If StatoAuto.STATOAUTO = "N" Then
                        Importare = False
                    End If
                End If
            End If


            If Importare = True Then
                Dim CausaleContabile As New Cls_CausaleContabile
                Dim ImportoRegistrazione As Double =0

                ImportoRegistrazione = Registrazione.ImportoDocumento(Session("DC_TABELLE"))


                CausaleContabile.Leggi(Session("DC_TABELLE"), Registrazione.CausaleContabile)
                

                If Txt_ContoContabile.Text <>"" Then
                    ImportoRegistrazione =  TotalePerContoContabile(Registrazione, Txt_ContoContabile.Text.Trim,Txt_ContoContabile2.Text.Trim,Txt_ContoContabile3.Text.Trim,Txt_ContoContabile4.Text.Trim,Txt_ContoContabile5.Text.Trim,Txt_ContoContabile6.Text.Trim,Txt_ContoContabile7.Text.Trim,Txt_ContoContabile8.Text.Trim,False,False,False)
                End If

                Dim M As New ClsOspite

                M.Leggi(Session("DC_OSPITE"), CodiceOspite)

                Dim Parente As New Cls_Parenti

                Parente.CodiceOspite = CodiceOspite
                Parente.CodiceParente = CodiceParente
                Parente.Leggi(Session("DC_OSPITE"), CodiceOspite, Parente.CodiceParente)

      

                Dim myriga1 As System.Data.DataRow = MyTable.NewRow()
                If Parente.CodiceParente = 0 Then
                    myriga1(0) = M.Nome
                    myriga1(1) = M.CODICEFISCALE

                Else
                    myriga1(0) = Parente.Nome
                    myriga1(1) = Parente.CODICEFISCALE
                End If

                If CausaleContabile.TipoDocumento ="NC" Then
                   myriga1(8) = Format( Registrazione.RifData,"dd/MM/yyyy")
                   myriga1(9) = Registrazione.RifNumero
                End If

                myriga1(2) = Format(Registrazione.DataDocumento, "dd/MM/yyyy")
                

                Dim Registro As New Cls_RegistroIVA

                Registro.Tipo = Registrazione.RegistroIVA
                Registro.Leggi(Session("DC_TABELLE"),Registro.Tipo)

                If Registro.IndicatoreRegistro <>"" THEN
                    myriga1(3) = Registrazione.NumeroProtocollo & "/" & Registro.IndicatoreRegistro
                    Else
                    myriga1(3) = Registrazione.NumeroProtocollo 
                End IF


                myriga1(4) = Format(campodbD(RsRegistrazioni.Item("DataRegistrazione")), "dd/MM/yyyy")

                If Chk_DatiAggiuntivi.Checked Then

                    myriga1("Classe") = Classe(M.CodiceOspite, Registrazione.DataDocumento)
                    myriga1("Tipologia") = TipoClasse(M.CodiceOspite, Registrazione.DataDocumento)

                    If  myriga1("Classe")="" And myriga1("Tipologia")="" Then
                        Dim StatoAuto As New Cls_StatoAuto

                        StatoAuto.TipoRetta=""
                        StatoAuto.USL=""
                        StatoAuto.CODICEOSPITE = CodiceOspite
                        StatoAuto.CENTROSERVIZIO = Registrazione.CentroServizio
                        StatoAuto.Data = Registrazione.DataRegistrazione
                        StatoAuto.StatoPrimaData(Session("DC_OSPITE"),StatoAuto.CODICEOSPITE,StatoAuto.CENTROSERVIZIO)
                                
                        If StatoAuto.USL.Trim <>"" Then
                            Dim Usl As New ClsUSL

                            Usl.CodiceRegione = StatoAuto.USL
                            Usl.Leggi(Session("DC_OSPITE"))

                            myriga1("Classe") =Usl.Nome
                                    
                        End If
                        If StatoAuto.TipoRetta.Trim <>"" Then
                            Dim TipoRetta As New Cls_TabellaTipoImportoRegione

                            TipoRetta.Codice = StatoAuto.TipoRetta
                            TipoRetta.Leggi(Session("DC_OSPITE"),TipoRetta.Codice)

                            myriga1("Tipologia") =TipoRetta.Descrizione                                    
                        End If

                    End If
                    Dim Movimenti As New Cls_Movimenti

                    Movimenti.CodiceOspite = M.CodiceOspite
                    Movimenti.CENTROSERVIZIO = Registrazione.CentroServizio
                    Movimenti.Data = Nothing
                    Movimenti.UltimaDataAccoglimento(Session("DC_OSPITE"))
                    If Year(Movimenti.Data) < 1990 Then
                        Movimenti.UltimaDataAccoglimentoSenzaCserv(Session("DC_OSPITE"))
                    End If
                    myriga1("DataAccoglimento") = Format(Movimenti.Data, "dd/MM/yyyy")



                    Movimenti.Data = Nothing

                    Movimenti.CodiceOspite = M.CodiceOspite
                    Movimenti.CENTROSERVIZIO = Registrazione.CentroServizio

                    Movimenti.UltimaDataUscitaDefinitiva(Session("DC_OSPITE"))
                    If Year(Movimenti.Data) > 2000 Then
                        myriga1("DataUscitaDefinitiva") = Format(Movimenti.Data, "dd/MM/yyyy")
                    Else
                        myriga1("DataUscitaDefinitiva") = ""
                    End If

                    Dim ImportoOspite As Double = 0
                    Dim ImportoParenti As Double = 0

                    Dim x As New Cls_CalcoloRette
                    x.STRINGACONNESSIONEDB = Session("DC_OSPITE")

                    x.ApriDB(Session("DC_OSPITE"), Session("DC_OSPITIACCESSORI"))

                    ImportoOspite = Math.Round(x.QuoteGiornaliere(Registrazione.CentroServizio, M.CodiceOspite, "O", 0, Registrazione.DataDocumento), 2)
                    For I = 1 To 10
                        ImportoParenti = ImportoParenti + Math.Round(CDbl(x.QuoteGiornaliere(Registrazione.CentroServizio, M.CodiceOspite, "P", I, Registrazione.DataDocumento)), 2)
                    Next

                    'myriga1("Quota") = Format(ImportoOspite + ImportoParenti, "#,##0.00")

                    x.ChiudiDB()
                End If



                If CausaleContabile.TipoDocumento = "NC" Then
                    myriga1(5) = "RI"
                Else
                    myriga1(5) = "IN"
                End If
                myriga1(6) = "AA"

                Quantita = 0
                For I = 0 To Registrazione.Righe.Length - 1
                    If Not IsNothing(Registrazione.Righe(I)) Then
                        If Registrazione.Righe(I).Tipo = "" Then
                            If Registrazione.Righe(I).RigaDaCausale = 3 Then
                                Quantita = Quantita + Registrazione.Righe(I).Quantita
                            End If
                        End If
                    End If
                Next

         
                myriga1(7) = Math.Round(ImportoRegistrazione , 2)

                If Chk_DatiAggiuntivi.Checked Then

                    Dim k As New Cls_CalcoloRette

                    k.ConnessioneGenerale = Session("DC_GENERALE")
                    k.STRINGACONNESSIONEDB = Session("DC_OSPITE")
                    k.ApriDB(Session("DC_OSPITE"), Session("DC_OSPITIACCESSORI"))

                    myriga1(10) = k.QuoteGiornaliere(DD_CServ.SelectedValue, M.CodiceOspite, "O", 0, Registrazione.DataDocumento) + k.QuoteGiornaliere(DD_CServ.SelectedValue, M.CodiceOspite, "P", 1, Registrazione.DataDocumento) + k.QuoteGiornaliere(DD_CServ.SelectedValue, M.CodiceOspite, "P", 2, Registrazione.DataDocumento) + k.QuoteGiornaliere(DD_CServ.SelectedValue, M.CodiceOspite, "P", 3, Registrazione.DataDocumento)

                    Dim StatoAuto As New Cls_StatoAuto

                    StatoAuto.Data = Registrazione.DataRegistrazione
                    StatoAuto.StatoPrimaData(Session("DC_OSPITE"), M.CodiceOspite, DD_CServ.SelectedValue)

                    Dim Im As New Cls_ImportoRegione

                    Im.Codice = StatoAuto.USL
                    Im.UltimaData(Session("DC_OSPITE"), StatoAuto.USL, StatoAuto.TipoRetta)

                    myriga1(11) = Im.Importo
                    myriga1(12) = StatoAuto.STATOAUTO
                    StatoAuto.Data = Registrazione.DataRegistrazione
                    StatoAuto.StatoDopoData(Session("DC_OSPITE"), M.CodiceOspite, DD_CServ.SelectedValue)
                    If Month(StatoAuto.Data) = Month(Registrazione.DataRegistrazione) And Year(StatoAuto.Data) = Year(Registrazione.DataRegistrazione) Then
                        myriga1(12) = myriga1(12) & " Cambio Stato " & StatoAuto.Data & " " & StatoAuto.STATOAUTO
                    End If
                    k.ChiudiDB()
                End If

                If CDbl(myriga1.Item("importo")) <> 0 then
                    MyTable.Rows.Add(myriga1)
                End If
            End If
        Loop
        RsRegistrazioni.Close()

        cnGenerale.Close()


        Dim dataView As New System.Data.DataView(MyTable)
        dataView.Sort = "  Nome,CodiceFiscale,datadocumento,numerodocumento"        
        Dim dataTable As System.Data.DataTable = dataView.ToTable()

        GridView1.AutoGenerateColumns = True
        GridView1.DataSource = dataTable
        GridView1.Font.Size = 10
        GridView1.DataBind()
    End Sub

    Function campodbN(ByVal oggetto As Object) As Double


        If IsDBNull(oggetto) Then
            Return 0
        Else
            Return oggetto
        End If
    End Function
    Function campodb(ByVal oggetto As Object) As String


        If IsDBNull(oggetto) Then
            Return ""
        Else
            Return oggetto
        End If
    End Function
    Function campodbD(ByVal oggetto As Object) As Date


        If IsDBNull(oggetto) Then
            Return Nothing
        Else
            Return oggetto
        End If
    End Function

    Protected Sub ImageButton3_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles ImageButton3.Click
        If TabContainer1.ActiveTabIndex = 0 Then
            Call Esporta()
        Else
            Call Esport730()
        End If

    End Sub

    Protected Sub OspitiWeb_Frm_EstrazionePer730_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Trim(Session("UTENTE")) = "" Then
            Response.Redirect("../Login.aspx")
            Exit Sub
        End If

        Call EseguiJS()

        If Page.IsPostBack = True Then Exit Sub


        Dim K1 As New Cls_SqlString

        Dim Barra As New Cls_BarraSenior

        If Not IsNothing(Session("RicercaAnagraficaSQLString")) Then
            K1 = Session("RicercaAnagraficaSQLString")
        End If

        Lbl_BarraSenior.Text = Barra.CodiceBarra(Application("SENIOR"), Session("UTENTE"), Page, K1)

        Dim kVilla As New Cls_TabelleDescrittiveOspitiAccessori


        Txt_DataAl.Text = Format(DateSerial(Year(Now)-1,12,31), "dd/MM/yyyy")
        Txt_DataDal.Text = Format(DateSerial(Year(Now)-1,1,1), "dd/MM/yyyy")

        
        Txt_DataAlIncasso.Text = Format(DateSerial(Year(Now)-1,12,31), "dd/MM/yyyy")
        Txt_DataDalIncasso.Text = Format(DateSerial(Year(Now)-1,1,1), "dd/MM/yyyy")

        kVilla.UpDateDropBox(Session("DC_OSPITIACCESSORI"), "VIL", DD_Struttura)
        If DD_Struttura.Items.Count = 1 Then
            Call AggiornaCServ()
        End If

        kVilla.UpDateDropBox(Session("DC_OSPITIACCESSORI"), "VIL", DD_Struttura2)
        If DD_Struttura2.Items.Count = 1 Then
            Call AggiornaCServ2()
        End If

        Dim M As New Cls_CausaleContabile

        M.UpDateDropBoxPag(Session("DC_TABELLE"), DD_CausaleIncasso)

        M.UpDateDropBoxPag(Session("DC_TABELLE"), DD_CausaleIncasso1)

        M.UpDateDropBoxPag(Session("DC_TABELLE"), DD_CausaleIncasso2)

        M.UpDateDropBoxPag(Session("DC_TABELLE"), DD_CausaleIncasso3)

        M.UpDateDropBoxPag(Session("DC_TABELLE"), DD_CausaleIncasso4)


        M.UpDateDropBoxDoc(Session("DC_TABELLE"), DD_CausaleDocumento1)

        M.UpDateDropBoxDoc(Session("DC_TABELLE"), DD_CausaleDocumento2)


        M.UpDateDropBoxDoc(Session("DC_TABELLE"), DD_CausaleDocumento_Inc)
        M.UpDateDropBoxDoc(Session("DC_TABELLE"), DD_CausaleDocumento_Inc2)
        M.UpDateDropBoxDoc(Session("DC_TABELLE"), DD_CausaleDocumento_Inc3)
        M.UpDateDropBoxDoc(Session("DC_TABELLE"), DD_CausaleDocumento_Inc4)
        M.UpDateDropBoxDoc(Session("DC_TABELLE"), DD_CausaleDocumento_Inc5)
        M.UpDateDropBoxDoc(Session("DC_TABELLE"), DD_CausaleDocumento_Inc6)
        M.UpDateDropBoxDoc(Session("DC_TABELLE"), DD_CausaleDocumento_Inc7)
        M.UpDateDropBoxDoc(Session("DC_TABELLE"), DD_CausaleDocumento_Inc8)

        
    End Sub

    Protected Sub ImageButton4_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles ImageButton4.Click
        If TabContainer1.ActiveTabIndex = 0 Then
            Call Esporta()
        Else
            Call Esport730()
        End If

        'If MyTable.Rows.Count > 1 Then
        '    Response.Clear()
        '    Response.AddHeader("content-disposition", "attachment;filename=730telematico.xls")
        '    Response.Charset = String.Empty
        '    Response.Cache.SetCacheability(HttpCacheability.NoCache)
        '    Response.ContentType = "application/vnd.ms-excel"
        '    Dim stringWrite As New System.IO.StringWriter
        '    Dim htmlWrite As New HtmlTextWriter(stringWrite)

        '    form1.Controls.Clear()
        '    If TabContainer1.ActiveTabIndex = 0 Then
        '        form1.Controls.Add(GridView1)

        '        For Each row As GridViewRow In GridView1.Rows
        '            For Each cell As TableCell In row.Cells
        '                cell.CssClass = "textmode"
        '                cell.Text = "=" & Chr(34) & cell.Text & Chr(34)
        '            Next
        '        Next
        '        Else
        '        form1.Controls.Add(GridView2)

        '        For Each row As GridViewRow In GridView2.Rows
        '            For Each cell As TableCell In row.Cells
        '                cell.CssClass = "textmode"
        '                cell.Text = "=" & Chr(34) & cell.Text & Chr(34)
        '            Next
        '        Next
        '    End If

        '    form1.RenderControl(htmlWrite)

        '    Dim style As String = "<style> .textmode {  } </style>"
        '    Response.Write(style)

        '    Response.Write(stringWrite.ToString())
        '    Response.End()
        'End If



        Response.Clear()
        Response.ContentType = "text/csv"
        Response.AddHeader("content-disposition", "attachment;filename=Estrazione730.csv")

        Dim sb As New StringBuilder()
        For i As Integer = 0 To MyTable.Columns.Count - 1
            sb.Append(MyTable.Columns(i).ColumnName + ";")
        Next
        sb.Append(Environment.NewLine)

        For j As Integer = 0 To MyTable.Rows.Count - 1
            For k As Integer = 0 To MyTable.Columns.Count - 1
                sb.Append(Chr(34) & MyTable.Rows(j)(k).ToString & Chr(34) & ";")
            Next
            sb.Append(Environment.NewLine)
        Next


        Response.Write(sb.ToString())
        Response.End()
    End Sub

    Private Sub EseguiJS()
        Dim MyJs As String
        MyJs = "$(document).ready(function() {"

        MyJs = MyJs & "var els = document.getElementsByTagName(""*"");"

        MyJs = MyJs & "for (var i=0;i<els.length;i++)"
        MyJs = MyJs & "if ( els[i].id ) { "
        MyJs = MyJs & " var appoggio =els[i].id; "



        MyJs = MyJs & " if (appoggio.match('Txt_ContoContabile')!= null) {   "
        MyJs = MyJs & " $(els[i]).autocomplete('/WebHandler/GestioneConto.ashx?Utente=" & Session("UTENTE") & "', {delay:5,minChars:3});"
        MyJs = MyJs & "    }"



        MyJs = MyJs & " if (appoggio.match('Txt_Data')!= null) {  "
        MyJs = MyJs & " $(els[i]).mask(""99/99/9999"");"

        MyJs = MyJs & " $(els[i]).datepicker({ changeMonth: true,changeYear: true,  yearRange: ""1990:" & Year(Now) + 5 & """},$.datepicker.regional[""it""]);"

        MyJs = MyJs & "    }"
        MyJs = MyJs & "} "
        MyJs = MyJs & "if (window.innerHeight>0) { $(""#BarraLaterale"").css(""height"",(window.innerHeight - 94) + ""px""); } else"
        MyJs = MyJs & "{ $(""#BarraLaterale"").css(""height"",(document.documentElement.offsetHeight - 94) + ""px"");  }"
        MyJs = MyJs & "});"

        'MyJs = "$(document).ready(function() { $('.myClass').keypress(function() { handleEnter($('.myClass').val(), true, true); } );     $('#" & Txt_ImportoPagato.ClientID & "').blur(function() { var ap = formatNumber ($('#" & Txt_ImportoPagato.ClientID & "').val(),2); $('#" & Txt_ImportoPagato.ClientID & "').val(ap); }); });"
        ScriptManager.RegisterStartupScript(Me, Me.GetType(), "visualizzaRitIm", MyJs, True)
    End Sub

    Protected Sub Btn_Esci_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles Btn_Esci.Click
        Response.Redirect("Menu_Visualizzazioni.aspx")
    End Sub


    Private Sub AggiornaCServ()
        Dim kCsrv As New Cls_CentroServizio

        If DD_Struttura.SelectedValue = "" Then
            kCsrv.UpDateDropBox(Session("DC_OSPITE"), DD_CServ)
        Else
            kCsrv.UpDateDropBoxStruttura(Session("DC_OSPITE"), DD_CServ, DD_Struttura.SelectedValue)
        End If
    End Sub
    Private Sub AggiornaCServ2()
        Dim kCsrv As New Cls_CentroServizio

        If DD_Struttura2.SelectedValue = "" Then
            kCsrv.UpDateDropBox(Session("DC_OSPITE"), DD_CServ2)
        Else
            kCsrv.UpDateDropBoxStruttura(Session("DC_OSPITE"), DD_CServ2, DD_Struttura2.SelectedValue)
        End If
    End Sub


    Protected Sub DD_Struttura_TextChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles DD_Struttura.TextChanged
        AggiornaCServ()
        Call EseguiJS()
    End Sub

    Private Function Classe(ByVal CodiceOspite As Integer, ByVal Data As Date) As String
        Dim Cn As New OleDbConnection(Session("DC_OSPITE"))
        Dim MySql As String = ""

        Classe = ""

        Cn.Open()

        MySql = "Select * FROM TempSosia where CodiceOspite = ? And Data <= ? Order by Data Desc"
        Dim cmddel As New OleDbCommand()
        cmddel.CommandText = (MySql)
        cmddel.Connection = Cn
        cmddel.Parameters.AddWithValue("@Codiceospite", CodiceOspite)
        cmddel.Parameters.AddWithValue("@Data", Data)
        Dim myPOSTreader As OleDbDataReader = cmddel.ExecuteReader()
        If myPOSTreader.Read Then
            Classe = campodb(myPOSTreader.Item("Sosia"))
        End If
        myPOSTreader.Close()

        Cn.Close()

        Return Classe
    End Function

    Private Function TipoClasse(ByVal CodiceOspite As Integer, ByVal Data As Date) As String
        Dim Cn As New OleDbConnection(Session("DC_OSPITE"))
        Dim MySql As String = ""

        TipoClasse = ""

        Cn.Open()

        MySql = "Select * FROM TempSosia where CodiceOspite = ? And Data <= ? Order by Data Desc"
        Dim cmddel As New OleDbCommand()
        cmddel.CommandText = (MySql)
        cmddel.Connection = Cn
        cmddel.Parameters.AddWithValue("@Codiceospite", CodiceOspite)
        cmddel.Parameters.AddWithValue("@Data", Data)
        Dim myPOSTreader As OleDbDataReader = cmddel.ExecuteReader()
        If myPOSTreader.Read Then
            TipoClasse = campodb(myPOSTreader.Item("Tipologia"))
        End If
        myPOSTreader.Close()

        Cn.Close()

        Return TipoClasse
    End Function


    Protected Sub DD_Struttura2_TextChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles DD_Struttura2.TextChanged
        AggiornaCServ2()
        Call EseguiJS()
    End Sub
End Class

