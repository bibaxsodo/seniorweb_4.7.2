﻿Imports Microsoft.VisualBasic
Imports System
Imports System.Collections.Generic
Imports System.Web
Imports System.Web.Services
Imports System.Web.Services.Protocols
Imports System.Data.OleDb
Imports System.Web.Hosting
Imports BCrypt.Net
Imports System.Web.Script.Serialization


Partial Class OspitiWeb_FatturaDomiciliare
    Inherits System.Web.UI.Page


    Dim MyTable As New System.Data.DataTable("tabella")
    Dim MyDataSet As New System.Data.DataSet()





    Function campodb(ByVal oggetto As Object) As String
        If IsDBNull(oggetto) Then
            Return ""
        Else
            Return oggetto
        End If
    End Function
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Dim Riga As String
        Dim Indice As Integer
        Dim TotaleStringa As String
        Dim CodiceOperatore As Integer
        Dim VtTipo(100) As String
        Dim VtOre(100) As Double
        Dim VtImporto(100) As Double
        Dim IndiceT As Integer = 0
        Dim ToatleCassaProfessionale As Double = 0


        Dim mN As New Cls_Notifica


        mN.Delete(Session("DC_OSPITE"))



        MyTable = Session("MYTABLE")

        Riga = Request.Item("RIGA")


        If Val(Riga) = 0 Then
            If Riga = "ALL" Then
                TotaleStringa = 0
                For Indice = 0 To MyTable.Rows.Count - 1                    
                    If Val(campodb(MyTable.Rows(Indice).Item(0))) > 0 Then
                        CodiceOperatore = Val(campodb(MyTable.Rows(Indice).Item(0)))
                        VtTipo(IndiceT) = MyTable.Rows(Indice).Item(2)
                        VtOre(IndiceT) = MyTable.Rows(Indice).Item(4)
                        VtImporto(IndiceT) = MyTable.Rows(Indice).Item(5)
                        TotaleStringa = TotaleStringa + CDbl(MyTable.Rows(Indice).Item(5))
                        IndiceT = IndiceT + 1
                    End If
                Next

            End If
        Else
            TotaleStringa = MyTable.Rows(Riga).Item(5).ToString
            For Indice = Riga - 1 To 0 Step -1
                If Val(campodb(MyTable.Rows(Indice).Item(0))) > 0 Then
                    CodiceOperatore = Val(MyTable.Rows(Indice).Item(0))
                    VtTipo(IndiceT) = MyTable.Rows(Indice).Item(2)
                    VtOre(IndiceT) = MyTable.Rows(Indice).Item(4)
                    VtImporto(IndiceT) = MyTable.Rows(Indice).Item(5)
                    IndiceT = IndiceT + 1
                Else
                    Exit For
                End If
            Next

        End If

        Dim Operatore As New Cls_Operatore

        Operatore.CodiceMedico = CodiceOperatore
        Operatore.Leggi(Session("DC_OSPITE"))

        Dim TipoOper As New Cls_TipoOperatore

        TipoOper.Codice = Operatore.TipoOperatore
        TipoOper.Leggi(Session("DC_OSPITE"), TipoOper.Codice)


        Dim TipoRaggr As New Cls_RaggruppamentoOperatori

        TipoRaggr.Codice = Operatore.RaggruppamentoOperatori
        TipoRaggr.Leggi(Session("DC_OSPITE"), TipoRaggr.Codice)


        Dim CliFor As New Cls_ClienteFornitore


        CliFor.CODICEDEBITORECREDITORE = 0
        CliFor.MastroFornitore = Operatore.MastroFornitore
        CliFor.ContoFornitore = Operatore.ContoFornitore
        CliFor.SottoContoFornitore = Operatore.SottoContoFornitore
        CliFor.Leggi(Session("DC_OSPITE"))

        Dim Cn As New OleDbConnection

        Cn.ConnectionString = Session("DC_GENERALE")
        Cn.Open()

        Dim CausaleRetta As New Cls_CausaleContabile

        CausaleRetta.Codice = TipoRaggr.CausaleContabile
        CausaleRetta.Leggi(Session("DC_TABELLE"), CausaleRetta.Codice)


        If TipoRaggr.Mastro > 0 And TipoRaggr.Percentuale > 0 Then
            Dim NumericoDouble As Double = TotaleStringa
            ToatleCassaProfessionale = Math.Round(NumericoDouble * TipoRaggr.Percentuale / 100, 2)
            TotaleStringa = NumericoDouble + ToatleCassaProfessionale
        End If


        Dim TempNumeroRegistrazione As Long
        Dim TempDataRegistrazione As Date
        Dim TempDataDocumento As Date
        Dim TempANNOCOMPETENZA As Integer
        Dim TempMESECOMPETENZA As Integer
        Dim TempTipologia As String
        Dim TempModalitaPagamento As String
        Dim TempCausaleContabile As String
        Dim TempCentroServizio As String
        Dim TempCodicePagamento As String
        Dim TempNumeroProtocollo As Integer
        Dim TempNumeroDocumento As Integer
        Dim TempAnnoProtocollo As Integer
        Dim TempRegistroIva As Integer
        Dim Utente As String
        Dim TempDataAggiornamento As Date
        Dim MovCont As New Cls_MovimentoContabile




        TempNumeroRegistrazione = 1
        TempDataRegistrazione = Now
        TempDataDocumento = Now
        TempANNOCOMPETENZA = Year(Now)
        TempMESECOMPETENZA = Month(Now)
        TempTipologia = ""
        TempCausaleContabile = CausaleRetta.Codice
        TempCentroServizio = ""
        TempModalitaPagamento = ""

        TempNumeroProtocollo = 0
        TempNumeroDocumento = 0
        TempAnnoProtocollo = Year(Now)
        TempRegistroIva = CausaleRetta.RegistroIVA
        TempDataAggiornamento = Now
        TempModalitaPagamento = CliFor.ModalitaPagamentoFornitore



        Dim MDel As New OleDbCommand
        MDel.Connection = Cn
        MDel.CommandText = "delete from Temp_MovimentiContabiliTesta "
        MDel.ExecuteNonQuery()

        Dim MDelRig As New OleDbCommand
        MDelRig.Connection = Cn
        MDelRig.CommandText = "delete from Temp_MovimentiContabiliRiga "
        MDelRig.ExecuteNonQuery()



        Dim M As New OleDbCommand
        M.Connection = Cn
        M.CommandText = "INSERT INTO Temp_MovimentiContabiliTesta (NumeroRegistrazione,DataRegistrazione,DataDocumento,ANNOCOMPETENZA,MESECOMPETENZA,Tipologia,CausaleContabile,CentroServizio,CodicePagamento,NumeroProtocollo,NumeroDocumento,AnnoProtocollo,RegistroIva,ModalitaPagamento,Utente,DataAggiornamento) values " & _
                               " (?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)"
        M.Parameters.AddWithValue("NumeroRegistrazione", TempNumeroRegistrazione)
        M.Parameters.AddWithValue("DataRegistrazione", TempDataRegistrazione)
        M.Parameters.AddWithValue("DataDocumento", TempDataDocumento)
        M.Parameters.AddWithValue("ANNOCOMPETENZA", TempANNOCOMPETENZA)
        M.Parameters.AddWithValue("MESECOMPETENZA", TempMESECOMPETENZA)
        M.Parameters.AddWithValue("Tipologia", TempTipologia)
        M.Parameters.AddWithValue("CausaleContabile", TempCausaleContabile)
        M.Parameters.AddWithValue("CentroServizio", TempCentroServizio)
        M.Parameters.AddWithValue("CodicePagamento", TempModalitaPagamento)
        M.Parameters.AddWithValue("NumeroProtocollo", TempNumeroProtocollo)
        M.Parameters.AddWithValue("NumeroDocumento", TempNumeroDocumento)
        M.Parameters.AddWithValue("AnnoProtocollo", TempAnnoProtocollo)
        M.Parameters.AddWithValue("RegistroIva", TempRegistroIva)
        M.Parameters.AddWithValue("TempModalitaPagamento", TempModalitaPagamento)
        M.Parameters.AddWithValue("Utente", "")
        M.Parameters.AddWithValue("DataAggiornamento", TempDataAggiornamento)
        M.ExecuteNonQuery()



        Dim TempRigaNumero As Long
        Dim TempRigaMastroPartita As Long
        Dim TempRigaContoPartita As Long
        Dim TempRigaSottoContoPartita As Double
        Dim TempRigaImporto As Double
        Dim TempRigaDescrizione As String
        Dim TempRigaTipo As String
        Dim TempRigaDareAvere As String
        Dim TempRigaSegno As String
        Dim TempRigaDaCausale As String
        Dim TempRigaUtente As String
        Dim TempRigaDataAggiornamento As Date
        Dim TempRigaCodIVA As String
        Dim TempRigaImponibile As Double
        Dim TempRigaQuantita As Double



        TempRigaNumero = 1
        TempRigaMastroPartita = Operatore.MastroFornitore
        TempRigaContoPartita = Operatore.ContoFornitore
        TempRigaSottoContoPartita = Operatore.SottoContoFornitore
        TempRigaImporto = TotaleStringa
        TempRigaTipo = "CF"
        TempRigaDareAvere = CausaleRetta.Righe(0).DareAvere
        TempRigaSegno = "+"
        TempRigaDaCausale = 1
        TempRigaUtente = ""
        TempRigaDataAggiornamento = Now



        M.Parameters.Clear()
        M.Connection = Cn
        M.CommandText = "INSERT INTO Temp_MovimentiContabiliRiga (Numero,MastroPartita,ContoPartita,SottoContoPartita,Importo,Tipo,DareAvere,Segno,RigaDaCausale,Utente,DataAggiornamento) values " & _
                               " (?,?,?,?,?,?,?,?,?,?,?)"
        M.Parameters.AddWithValue("Numero", TempRigaNumero)
        M.Parameters.AddWithValue("MastroPartita", TempRigaMastroPartita)
        M.Parameters.AddWithValue("ContoPartita", TempRigaContoPartita)
        M.Parameters.AddWithValue("SottoContoPartita", TempRigaSottoContoPartita)
        M.Parameters.AddWithValue("Importo", TempRigaImporto)
        M.Parameters.AddWithValue("Tipo", TempRigaTipo)
        M.Parameters.AddWithValue("DareAvere", TempRigaDareAvere)
        M.Parameters.AddWithValue("Segno", TempRigaSegno)
        M.Parameters.AddWithValue("RigaDaCausale", TempRigaDaCausale)
        M.Parameters.AddWithValue("Utente", Utente)
        M.Parameters.AddWithValue("DataAggiornamento", TempDataAggiornamento)
        M.ExecuteNonQuery()


        TempRigaNumero = 1
        TempRigaMastroPartita = CausaleRetta.Righe(2).Mastro
        TempRigaContoPartita = CausaleRetta.Righe(2).Conto
        TempRigaSottoContoPartita = CausaleRetta.Righe(2).Sottoconto

        TempRigaTipo = "IV"
        TempRigaDareAvere = CausaleRetta.Righe(2).DareAvere
        TempRigaSegno = "+"
        TempRigaDaCausale = 3
        TempRigaUtente = ""
        TempRigaDataAggiornamento = Now
        TempRigaCodIVA = TipoRaggr.CodiceIVA

        Dim CodIVA As New Cls_IVA

        CodIVA.Codice = TipoRaggr.CodiceIVA
        CodIVA.Leggi(Session("DC_TABELLE"), CodIVA.Codice)

        TempRigaImponibile = TotaleStringa
        TempRigaImporto = Math.Round(TotaleStringa * CodIVA.Aliquota, 2)


        M.Parameters.Clear()
        M.Connection = Cn
        M.CommandText = "INSERT INTO Temp_MovimentiContabiliRiga (Numero,MastroPartita,ContoPartita,SottoContoPartita,Importo,Tipo,DareAvere,Segno,RigaDaCausale,Utente,DataAggiornamento,CodiceIVA,Imponibile) values " & _
                               " (?,?,?,?,?,?,?,?,?,?,?,?,?)"
        M.Parameters.AddWithValue("Numero", TempRigaNumero)
        M.Parameters.AddWithValue("MastroPartita", TempRigaMastroPartita)
        M.Parameters.AddWithValue("ContoPartita", TempRigaContoPartita)
        M.Parameters.AddWithValue("SottoContoPartita", TempRigaSottoContoPartita)
        M.Parameters.AddWithValue("Importo", TempRigaImporto)
        M.Parameters.AddWithValue("Tipo", TempRigaTipo)
        M.Parameters.AddWithValue("DareAvere", TempRigaDareAvere)
        M.Parameters.AddWithValue("Segno", TempRigaSegno)
        M.Parameters.AddWithValue("RigaDaCausale", TempRigaDaCausale)
        M.Parameters.AddWithValue("Utente", Utente)
        M.Parameters.AddWithValue("DataAggiornamento", TempDataAggiornamento)
        M.Parameters.AddWithValue("CodiceIVA", TempRigaCodIVA)
        M.Parameters.AddWithValue("Imponibile", TempRigaImponibile)
        M.ExecuteNonQuery()

        For Indice = 0 To IndiceT - 1
            TempRigaNumero = 1
            TempRigaMastroPartita = CausaleRetta.Righe(1).Mastro
            TempRigaContoPartita = CausaleRetta.Righe(1).Conto
            TempRigaSottoContoPartita = CausaleRetta.Righe(1).Sottoconto
            TempRigaImporto = VtImporto(Indice)
            TempRigaTipo = ""

            Dim tp As New Cls_TipoDomiciliare

            tp.Codice = VtTipo(Indice)
            tp.Descrizione = ""
            tp.Leggi(Session("DC_OSPITE"), tp.Codice)

            TempRigaDescrizione = tp.Descrizione


            TempRigaDareAvere = CausaleRetta.Righe(1).DareAvere
            TempRigaSegno = "+"
            TempRigaDaCausale = 2
            TempRigaUtente = ""
            TempRigaDataAggiornamento = Now
            TempRigaQuantita = VtOre(Indice) * 60


            M.Parameters.Clear()
            M.Connection = Cn
            M.CommandText = "INSERT INTO Temp_MovimentiContabiliRiga (Numero,MastroPartita,ContoPartita,SottoContoPartita,Importo,Tipo,DareAvere,Segno,RigaDaCausale,Utente,DataAggiornamento,CodiceIVA,Descrizione,Quantita) values " & _
                                   " (?,?,?,?,?,?,?,?,?,?,?,?,?,?)"
            M.Parameters.AddWithValue("Numero", TempRigaNumero)
            M.Parameters.AddWithValue("MastroPartita", TempRigaMastroPartita)
            M.Parameters.AddWithValue("ContoPartita", TempRigaContoPartita)
            M.Parameters.AddWithValue("SottoContoPartita", TempRigaSottoContoPartita)
            M.Parameters.AddWithValue("Importo", TempRigaImporto)
            M.Parameters.AddWithValue("Tipo", TempRigaTipo)
            M.Parameters.AddWithValue("DareAvere", TempRigaDareAvere)
            M.Parameters.AddWithValue("Segno", TempRigaSegno)
            M.Parameters.AddWithValue("RigaDaCausale", TempRigaDaCausale)
            M.Parameters.AddWithValue("Utente", Utente)
            M.Parameters.AddWithValue("DataAggiornamento", TempDataAggiornamento)
            M.Parameters.AddWithValue("CodiceIVA", TipoRaggr.CodiceIVA)
            M.Parameters.AddWithValue("TempRigaDescrizione", TempRigaDescrizione)
            M.Parameters.AddWithValue("TempRigaQuantita ", TempRigaQuantita)
            M.ExecuteNonQuery()

        Next

        If TipoRaggr.Mastro > 0 And ToatleCassaProfessionale > 0 Then

            TempRigaNumero = 1
            TempRigaMastroPartita = TipoRaggr.Mastro
            TempRigaContoPartita = TipoRaggr.Conto
            TempRigaSottoContoPartita = TipoRaggr.Sottoconto
            TempRigaImporto = ToatleCassaProfessionale
            TempRigaTipo = ""



            TempRigaDescrizione = ""


            TempRigaDareAvere = CausaleRetta.Righe(1).DareAvere
            TempRigaSegno = "+"
            TempRigaDaCausale = 2
            TempRigaUtente = ""
            TempRigaDataAggiornamento = Now
            TempRigaQuantita = 1


            M.Parameters.Clear()
            M.Connection = Cn
            M.CommandText = "INSERT INTO Temp_MovimentiContabiliRiga (Numero,MastroPartita,ContoPartita,SottoContoPartita,Importo,Tipo,DareAvere,Segno,RigaDaCausale,Utente,DataAggiornamento,CodiceIVA,Descrizione,Quantita) values " & _
                                   " (?,?,?,?,?,?,?,?,?,?,?,?,?,?)"
            M.Parameters.AddWithValue("Numero", TempRigaNumero)
            M.Parameters.AddWithValue("MastroPartita", TempRigaMastroPartita)
            M.Parameters.AddWithValue("ContoPartita", TempRigaContoPartita)
            M.Parameters.AddWithValue("SottoContoPartita", TempRigaSottoContoPartita)
            M.Parameters.AddWithValue("Importo", TempRigaImporto)
            M.Parameters.AddWithValue("Tipo", TempRigaTipo)
            M.Parameters.AddWithValue("DareAvere", TempRigaDareAvere)
            M.Parameters.AddWithValue("Segno", TempRigaSegno)
            M.Parameters.AddWithValue("RigaDaCausale", TempRigaDaCausale)
            M.Parameters.AddWithValue("Utente", Utente)
            M.Parameters.AddWithValue("DataAggiornamento", TempDataAggiornamento)
            M.Parameters.AddWithValue("CodiceIVA", TipoRaggr.CodiceIVA)
            M.Parameters.AddWithValue("TempRigaDescrizione", TempRigaDescrizione)
            M.Parameters.AddWithValue("TempRigaQuantita ", TempRigaQuantita)
            M.ExecuteNonQuery()
        End If

        If TipoRaggr.CodiceRitenuta <> "" Then
            Dim TipoRitenuto As New ClsRitenuta

            TipoRitenuto.Codice = TipoRaggr.CodiceRitenuta
            TipoRitenuto.Leggi(Session("DC_TABELLE"))


            TempRigaNumero = 1
            TempRigaMastroPartita = CausaleRetta.Righe(8).Mastro
            TempRigaContoPartita = CausaleRetta.Righe(8).Conto
            TempRigaSottoContoPartita = CausaleRetta.Righe(8).Sottoconto
            TempRigaImponibile = TotaleStringa

            TempRigaImporto = Math.Round(TotaleStringa * TipoRitenuto.Percentuale, 2)
            TempRigaTipo = "RI"
            TempRigaDareAvere = CausaleRetta.Righe(8).DareAvere
            TempRigaSegno = "+"
            TempRigaDaCausale = 9
            TempRigaUtente = ""
            TempRigaDataAggiornamento = Now



            M.Parameters.Clear()
            M.Connection = Cn
            M.CommandText = "INSERT INTO Temp_MovimentiContabiliRiga (Numero,MastroPartita,ContoPartita,SottoContoPartita,Imponibile,Importo,Tipo,DareAvere,Segno,RigaDaCausale,CodiceRitenuta,Utente,DataAggiornamento) values " & _
                                   " (?,?,?,?,?,?,?,?,?,?,?,?,?)"
            M.Parameters.AddWithValue("Numero", TempRigaNumero)
            M.Parameters.AddWithValue("MastroPartita", TempRigaMastroPartita)
            M.Parameters.AddWithValue("ContoPartita", TempRigaContoPartita)
            M.Parameters.AddWithValue("SottoContoPartita", TempRigaSottoContoPartita)
            M.Parameters.AddWithValue("Imponibile", TempRigaImponibile)
            M.Parameters.AddWithValue("Importo", TempRigaImporto)
            M.Parameters.AddWithValue("Tipo", TempRigaTipo)
            M.Parameters.AddWithValue("DareAvere", TempRigaDareAvere)
            M.Parameters.AddWithValue("Segno", TempRigaSegno)
            M.Parameters.AddWithValue("RigaDaCausale", TempRigaDaCausale)
            M.Parameters.AddWithValue("CodiceRitenuta", TipoRaggr.CodiceRitenuta)
            M.Parameters.AddWithValue("Utente", Utente)
            M.Parameters.AddWithValue("DataAggiornamento", TempDataAggiornamento)
            M.ExecuteNonQuery()



            TempRigaNumero = 1
            TempRigaMastroPartita = Operatore.MastroFornitore
            TempRigaContoPartita = Operatore.ContoFornitore
            TempRigaSottoContoPartita = Operatore.SottoContoFornitore
            TempRigaImponibile = TotaleStringa

            TempRigaImporto = Math.Round(TotaleStringa * TipoRitenuto.Percentuale, 2)
            TempRigaTipo = "RI"
            If CausaleRetta.Righe(8).DareAvere = "D" Then
                TempRigaDareAvere = "A"
            Else
                TempRigaDareAvere = "D"
            End If
            TempRigaSegno = "+"
            TempRigaDaCausale = 0
            TempRigaUtente = ""
            TempRigaDataAggiornamento = Now



            M.Parameters.Clear()
            M.Connection = Cn
            M.CommandText = "INSERT INTO Temp_MovimentiContabiliRiga (Numero,MastroPartita,ContoPartita,SottoContoPartita,Imponibile,Importo,Tipo,DareAvere,Segno,RigaDaCausale,CodiceRitenuta,Utente,DataAggiornamento) values " & _
                                   " (?,?,?,?,?,?,?,?,?,?,?,?,?)"
            M.Parameters.AddWithValue("Numero", TempRigaNumero)
            M.Parameters.AddWithValue("MastroPartita", TempRigaMastroPartita)
            M.Parameters.AddWithValue("ContoPartita", TempRigaContoPartita)
            M.Parameters.AddWithValue("SottoContoPartita", TempRigaSottoContoPartita)
            M.Parameters.AddWithValue("Imponibile", TempRigaImponibile)
            M.Parameters.AddWithValue("Importo", TempRigaImporto)
            M.Parameters.AddWithValue("Tipo", TempRigaTipo)
            M.Parameters.AddWithValue("DareAvere", TempRigaDareAvere)
            M.Parameters.AddWithValue("Segno", TempRigaSegno)
            M.Parameters.AddWithValue("RigaDaCausale", TempRigaDaCausale)
            M.Parameters.AddWithValue("CodiceRitenuta", TipoRaggr.CodiceRitenuta)
            M.Parameters.AddWithValue("Utente", Utente)
            M.Parameters.AddWithValue("DataAggiornamento", TempDataAggiornamento)
            M.ExecuteNonQuery()
        End If

        Response.Redirect("../GeneraleWeb/documenti.aspx?NUMEROREGISTRAZIONETEMP=1&NONMENU=OK")
    End Sub
End Class
