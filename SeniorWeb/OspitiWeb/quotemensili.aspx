﻿<%@ Page Language="VB" AutoEventWireup="false" Inherits="OspitiWeb_quotemensili" CodeFile="quotemensili.aspx.vb" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="xasp" %>
<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <meta http-equiv="x-ua-compatible" content="IE=9" />
    <title>Quote Mensile</title>
    <asp:PlaceHolder runat="server">
        <%: System.Web.Optimization.Styles.Render("~/Content/AjaxControlToolkit/Styles/Bundle") %>
    </asp:PlaceHolder>
    <link href="ospiti.css?ver=11" rel="stylesheet" type="text/css" />
      <link rel="shortcut icon" href="images/SENIOR.ico" />
    
    <link href="js/jquery.autocomplete.css" rel="stylesheet" type="text/css" />

    
    <script src="js/jquery-1.5.1.min.js" type="text/javascript"></script>
        <script src="js/jquery.autocomplete.js" type="text/javascript"></script>
    <script src="js/soapclient.js" type="text/javascript"></script>
    <script src="/js/convertinumero.js" type="text/javascript"></script>

    <link rel="stylesheet" href="dialogbox/redips-dialog.css" type="text/css" media="screen" />
    <script type="text/javascript" src="dialogbox/redips-dialog-min.js"></script>
    <script type="text/javascript" src="dialogbox/script.js"></script>

    
    <script src="/js/pianoconti.js" type="text/javascript"></script>
    <script src="js/jquery.maskedinput-1.3.min.js" type="text/javascript"></script>
    <script src="/js/formatnumer.js" type="text/javascript"></script>
    <script src="js/JSErrore.js" type="text/javascript"></script>
    <script type="text/javascript" src="../js/menuanagraficaospiti.js?ver=7"></script>


    <script src="../js/d3.v3.min.js" type="text/javascript"></script>
    
    <script src="../js/Donut3D.js" type="text/javascript"></script>
    <style type="text/css">
        body {
            font-family: Arial, Helvetica, sans-serif;
            font-size: 12px;
            line-height: 16px;
        }

        p {
            margin: 0 0 9px 0;
            padding: 0;
        }

        .style1 {
            text-align: center;
            border: 1px solid #FFFFFF;
        }

        .style2 {
            text-align: center;
            background-color: #f9f0b9;
        }

        .styleOspite {
            text-align: center;
            background-color: #F2C485;
        }

        .styleParente {
            text-align: center;
            background-color: #F28DB3;
        }

        .styleComune {
            text-align: center;
            background-color: #BE87EB;
        }

        .styleRegione {
            text-align: center;
            background-color: #8AB8F7;
        }

        .styleJolly {
            text-align: center;
            background-color: #8AEFC9;
        }

        .style9 {
            text-align: center;
            background-color: #EFEFEF;
        }
        
        path.slice{
	        stroke-width:2px;
        }
        polyline{
	        opacity: .3;
	        stroke: black;
	        stroke-width: 2px;
	        fill: none;
        } 
        svg text.percent{
	        fill:white;
	        text-anchor:middle;
	        font-size:12px;
        }        
    </style>
    <script type="text/javascript">
        function DialogBox(Path) {

            REDIPS.dialog.show(500, 300, '<iframe id="output" src="' + Path + '" height="300px" width="500"></iframe>');
            return false;

        }
        $(document).ready(function () {
            if (window.innerHeight > 0) { $("#BarraLaterale").css("height", (window.innerHeight - 94) + "px"); } else
            { $("#BarraLaterale").css("height", (document.documentElement.offsetHeight - 94) + "px"); }
            
                      
           /** var salesData=[
	            {label:"Basic", color:"#3366CC",value: 10},
	            {label:"Plus", color:"#DC3912",value: 10},
	            {label:"Lite", color:"#FF9900",value: 10},
	            {label:"Elite", color:"#109618",value: 10},
	            {label:"Delux", color:"#990099",value: 60}
            ];

            var svg = d3.select("#chart2").append("svg").attr("width",700).attr("height",300);

            //svg.append("g").attr("id","salesDonut");
            svg.append("g").attr("id","quotesDonut");

            //Donut3D.draw("salesDonut", randomData(), 150, 150, 130, 100, 30, 0.4);
            Donut3D.draw("quotesDonut", salesData, 150, 150, 130, 100, 30, 0);**/
        });
    </script>
</head>
<body>
    <form id="form1" runat="server">
        <asp:ScriptManager ID="ScriptManager1" runat="server" EnableScriptGlobalization="true">
            <Scripts>
                <asp:ScriptReference Path="~/Scripts/AjaxControlToolkit/Bundle" />
            </Scripts>
        </asp:ScriptManager>
        <asp:Label ID="Lbl_BarraSenior" runat="server" Text=""></asp:Label>

        <table style="width: 100%; vertical-align: top;" cellpadding="0" cellspacing="0">
            <tr>
                <td style="width: 160px; background-color: #F0F0F0;"></td>
                <td>
                    <div class="Titolo">Ospiti - Quote Mensili</div>
                    <div class="SottoTitoloOSPITE">
                        <asp:Label ID="Lbl_NomeOspite" runat="server" Text=""></asp:Label><br />
                        <br />
                    </div>
                </td>
                <td style="text-align: right;">
                    <span class="BenvenutoText">Benvenuto <%=Session("UTENTE")%>&nbsp;&nbsp;&nbsp;&nbsp;</span>

                </td>
            </tr>

            <tr>
                <td style="width: 160px; background-color: #F0F0F0; vertical-align: top; text-align: center;" id="BarraLaterale">
                    <a href="Menu_Ospiti.aspx" style="border-width: 0px;">
                        <img src="images/Home.jpg" alt="Menù" class="Effetto" /></a><br />
                    <asp:ImageButton ID="Btn_Esci" runat="server" BackColor="Transparent" ImageUrl="../images/Menu_Indietro.png" class="Effetto" ToolTip="Chiudi" />
                    <br />
                    <div id="MENUDIV"></div>

                    <br />
                    <br />
                    <br />
                    <br />
                    <br />
                    <br />
                    <br />
                    <br />
                </td>
                <td colspan="2" style="background-color: #FFFFFF; vertical-align: top;">
                    <xasp:TabContainer ID="TabContainer1" runat="server" ActiveTabIndex="0" CssClass="TabSenior"
                        Width="100%" BorderStyle="None" Style="margin-right: 39px">
                        <xasp:TabPanel runat="server" HeaderText="Prima Nota" ID="Tab_Anagrafica">
                            <HeaderTemplate>
                                Quote Mensili           
                            </HeaderTemplate>
                            <ContentTemplate>

                                <label class="LabelCampo">Anno :</label>
                                <asp:TextBox ID="Txt_Anno" MaxLength="4" onkeypress="return soloNumeri(event);" runat="server" Width="64px"></asp:TextBox><br />
                                <br />
                                <label class="LabelCampo">Mese :</label>
                                <asp:DropDownList ID="Dd_Mese" runat="server" Width="128px">
                                    <asp:ListItem Value="1">Gennaio</asp:ListItem>
                                    <asp:ListItem Value="2">Febbraio</asp:ListItem>
                                    <asp:ListItem Value="3">Marzo</asp:ListItem>
                                    <asp:ListItem Value="4">Aprile</asp:ListItem>
                                    <asp:ListItem Value="5">Maggio</asp:ListItem>
                                    <asp:ListItem Value="6">Giugno</asp:ListItem>
                                    <asp:ListItem Value="7">Luglio</asp:ListItem>
                                    <asp:ListItem Value="8">Agosto</asp:ListItem>
                                    <asp:ListItem Value="9">Settembre</asp:ListItem>
                                    <asp:ListItem Value="10">Ottobre</asp:ListItem>
                                    <asp:ListItem Value="11">Novembre</asp:ListItem>
                                    <asp:ListItem Value="12">Dicembre</asp:ListItem>
                                </asp:DropDownList>
                                &nbsp;<asp:Button ID="Button1" runat="server" Text="Visualizza" Width="83px" />
                                </div>      
                                <br />
                                <br />
                                <br />
                                <div style="display:flex;">
                                <div id="chart2" style="height: 300px; width: 400px; float: left;"></div>
                                <div style="width: 100px;">
                                <br />
                                   <div style="width:20px; height:20px; background-color:#F2C485; float:left;" ></div> <font style="padding-left:5px;">Ospite</font> <br /><br />
                                   <div style="width:20px; height:20px; background-color:#F28DB3; float:left;" ></div> <font style="padding-left:5px;">Parenti</font><br /> <br />
                                   <div style="width:20px; height:20px; background-color:#BE87EB; float:left;" ></div> <font style="padding-left:5px;">Comune</font> <br /><br />
                                   <div style="width:20px; height:20px; background-color:#8AB8F7; float:left;" ></div> <font style="padding-left:5px;">Regione</font><br /><br />
                                   <div style="width:20px; height:20px; background-color:#8AEFC9; float:left;" ></div> <font style="padding-left:5px;">Jolly</font> <br /><br />
                                </div>
                                </div>
                                
                                
                                <asp:Label ID="Lbl_Dettagli" runat="server" Text="" ></asp:Label>
                            </ContentTemplate>
                        </xasp:TabPanel>
                    </xasp:TabContainer>
                </td>
            </tr>
        </table>
    </form>
</body>
</html>
