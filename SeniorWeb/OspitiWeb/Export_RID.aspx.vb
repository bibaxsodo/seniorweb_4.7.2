﻿Imports Microsoft.VisualBasic
Imports System.Data.OleDb
Imports System.Web.Hosting
Imports System.Diagnostics
Imports System.ComponentModel
Imports System.Threading

Partial Class OspitiWeb_Export_RID
    Inherits System.Web.UI.Page
    Dim Tabella As New System.Data.DataTable("tabella")



    Sub Crea_XML()

        Tabella = Session("TabellaRid")

        Dim MeseContr As Long
        Dim MySql As String
        Dim ProgressivoAssoluto As Integer = 0
        Dim XML As String


        Dim APPOGGIO As String
        Dim AppoggioTotale As Double = 0
        Dim AppoggioNumero As Integer = 0
        Dim ImportoPerID As Double = 0
        Dim DataScadenza As Date = Txt_DataScadenza.Text



        Dim NomeFile As String

        NomeFile = HostingEnvironment.ApplicationPhysicalPath() & "\Public\Esportazione_" & Session("NomeEPersonam") & "_" & Format(Now, "yyyyMMddHHmm") & ".Txt"
        Dim tw As System.IO.TextWriter = System.IO.File.CreateText(NomeFile)


        Dim KTab As New Cls_TabellaSocieta

        KTab.Leggi(Session("DC_TABELLE"))

        If DD_Banca.SelectedValue <> "" Then
            Dim mpD As New Cls_Banche

            mpD.Codice = DD_Banca.SelectedValue
            mpD.Leggi(Session("DC_OSPITE"), mpD.Codice)


            KTab.OrgId = mpD.OrgId
            KTab.MmbId = mpD.MmbId
            KTab.IBANRID = mpD.IBANRID
            KTab.PrvtId = mpD.PrvtId
            KTab.EndToEndId = mpD.EndToEndId
        End If

        XML = "<?xml version=""1.0"" encoding=""UTF-8""?>" & vbNewLine
        XML = XML & "<CBISDDReqLogMsg xmlns=""urn:CBI:xsd:CBISDDReqLogMsg.00.01.00"" xmlns:xsi=""http://www.w3.org/2001/XMLSchema-instance"">" & vbNewLine
        XML = XML & "<GrpHdr>" & vbNewLine
        XML = XML & "<MsgId>" & Txt_MsgID.Text & "</MsgId>" & vbNewLine
        XML = XML & "<CreDtTm>" & Format(Now, "yyyy-MM-dd") & "T" & Format(Now, "hh:mm:ss") & "</CreDtTm>" & vbNewLine
        XML = XML & "<NbOfTxs>@NbOfTxs@</NbOfTxs>" & vbNewLine
        XML = XML & "<CtrlSum>@CtrlSum@</CtrlSum>" & vbNewLine
        XML = XML & "<InitgPty>" & vbNewLine
        XML = XML & "<Nm>" & KTab.RagioneSociale & "</Nm>" & vbNewLine
        XML = XML & "<Id>" & vbNewLine
        XML = XML & "<OrgId>" & vbNewLine
        XML = XML & "<Othr>" & vbNewLine
        XML = XML & "<Id>" & KTab.OrgId & "</Id>" & vbNewLine
        XML = XML & "<Issr>CBI</Issr>" & vbNewLine
        XML = XML & "</Othr>" & vbNewLine
        XML = XML & "</OrgId>" & vbNewLine
        XML = XML & "</Id>" & vbNewLine
        XML = XML & "</InitgPty>" & vbNewLine
        XML = XML & "</GrpHdr>" & vbNewLine
        XML = XML & "<PmtInf>" & vbNewLine
        XML = XML & "<PmtInfId>1</PmtInfId>" & vbNewLine
        XML = XML & "<PmtMtd>DD</PmtMtd>" & vbNewLine
        XML = XML & "<PmtTpInf>" & vbNewLine
        XML = XML & "<SvcLvl>" & vbNewLine
        XML = XML & "<Cd>SEPA</Cd>" & vbNewLine
        XML = XML & "</SvcLvl>" & vbNewLine
        XML = XML & "<LclInstrm>" & vbNewLine
        XML = XML & "<Cd>CORE</Cd>" & vbNewLine
        XML = XML & "</LclInstrm>" & vbNewLine
        XML = XML & "<SeqTp>@SEQTP@</SeqTp>" & vbNewLine
        XML = XML & "</PmtTpInf>" & vbNewLine
        XML = XML & "<ReqdColltnDt>" & Format(DataScadenza, "yyyy-MM-dd") & "</ReqdColltnDt>" & vbNewLine
        XML = XML & "<Cdtr>" & vbNewLine
        XML = XML & "<Nm>" & KTab.RagioneSociale & "</Nm>" & vbNewLine
        XML = XML & "<PstlAdr>" & vbNewLine
        XML = XML & "<AdrLine>" & KTab.Indirizzo & " </AdrLine>" & vbNewLine
        XML = XML & "<AdrLine>" & KTab.Cap & " " & KTab.Localita & " " & KTab.Provincia & "</AdrLine>" & vbNewLine
        XML = XML & "</PstlAdr>" & vbNewLine
        XML = XML & "</Cdtr>" & vbNewLine
        XML = XML & "<CdtrAcct>" & vbNewLine
        XML = XML & "<Id>" & vbNewLine
        XML = XML & "<IBAN>" & KTab.IBANRID & "</IBAN>" & vbNewLine
        XML = XML & "</Id>" & vbNewLine
        XML = XML & "</CdtrAcct>" & vbNewLine
        XML = XML & "<CdtrAgt>" & vbNewLine
        XML = XML & "<FinInstnId>" & vbNewLine
        XML = XML & "<ClrSysMmbId>" & vbNewLine
        XML = XML & "<MmbId>" & KTab.MmbId & "</MmbId>" & vbNewLine
        XML = XML & "</ClrSysMmbId>" & vbNewLine
        XML = XML & "</FinInstnId>" & vbNewLine
        XML = XML & "</CdtrAgt>" & vbNewLine
        XML = XML & "<CdtrSchmeId>" & vbNewLine
        XML = XML & "<Id>" & vbNewLine
        XML = XML & "<PrvtId>" & vbNewLine
        XML = XML & "<Othr>" & vbNewLine
        XML = XML & "<Id>" & KTab.PrvtId & "</Id>" & vbNewLine
        XML = XML & "</Othr>" & vbNewLine
        XML = XML & "</PrvtId>" & vbNewLine
        XML = XML & "</Id>" & vbNewLine
        XML = XML & "</CdtrSchmeId>" & vbNewLine

        AppoggioNumero = 0
        AppoggioTotale = 0

        Dim Riga As Integer
        Dim RecurrentOrFirst As String = ""
        Dim UltimoIdMandato As String = ""
        Dim UltimoImporto As Double = 0
        Dim TotaleImporto As Double = 0
        Dim RegistrazioneRiferimento As String = ""

        For Riga = 0 To GridView1.Rows.Count - 1
            Tabella.Rows(Riga).Item("ImportoCalcolato") = 0 '  Tabella.Rows(Riga).Item("ImportoTotale")
        Next

        Dim cn As OleDbConnection



        cn = New Data.OleDb.OleDbConnection(Session("DC_GENERALE"))

        cn.Open()



        For Riga = 0 To GridView1.Rows.Count - 1
            Dim CheckBox As CheckBox = DirectCast(GridView1.Rows(Riga).FindControl("ChkImporta"), CheckBox)

            If CheckBox.Checked = True Then


                If Val(campodb(Tabella.Rows(Riga).Item("NumeroRegistrazione"))) > 0 Then

                    Dim Log As New Cls_LogPrivacy

                    Log.LogPrivacy(Application("SENIOR"), Session("CLIENTE"), Session("UTENTE"), Session("UTENTEIMPER"), Page.GetType.Name.ToUpper, 0, 0, "", "", campodb(Tabella.Rows(Riga).Item("NumeroRegistrazione")), "", "M", "DOCUMENTO", "")


                    Dim Cmd As New OleDbCommand

                    Cmd.CommandText = "UPDATE MOVIMENTICONTABILITESTA SET DataRid = ?,IdRid = ? where  NumeroRegistrazione = ?"
                    Cmd.Parameters.AddWithValue("@DataRid", Txt_DataScadenza.Text)
                    Cmd.Parameters.AddWithValue("@MsgID", Txt_MsgID.Text)
                    Cmd.Parameters.AddWithValue("@NumeroRegistrazione", campodb(Tabella.Rows(Riga).Item("NumeroRegistrazione")))
                    Cmd.Connection = cn
                    Cmd.ExecuteNonQuery()


               

                End If

                If Tabella.Rows(Riga).Item("First") = "SI" And Val(campodb(Tabella.Rows(Riga).Item("NumeroRegistrazione"))) > 0 Then

                    Dim ModPag As New Cls_DatiPagamento
                    Dim Documento As New Cls_MovimentoContabile

                    Documento.NumeroRegistrazione = Tabella.Rows(Riga).Item("NumeroRegistrazione")
                    Documento.Leggi(Session("DC_GENERALE"), Documento.NumeroRegistrazione)


                    ModPag.CodiceOspite = Tabella.Rows(Riga).Item("CODICEOSPITE")
                    ModPag.CodiceParente = Tabella.Rows(Riga).Item("CODICEPARENTE")
                    If Year(Documento.DataDocumento) > 1910 Then
                        ModPag.LeggiUltimaDataData(Session("DC_OSPITE"), Documento.DataDocumento)
                        If ModPag.First = 1 Then
                            ModPag.First = 0
                            ModPag.Scrivi(Session("DC_OSPITE"))
                        End If
                    End If
                End If


                If campodb(Tabella.Rows(Riga).Item("IdMandato")) = UltimoIdMandato Then
                    TotaleImporto = TotaleImporto + campodbN(Tabella.Rows(Riga).Item("ImportoTotale"))
                    Tabella.Rows(Riga).Item("NumeroRegistrazioneRif") = RegistrazioneRiferimento
                Else
                    If Riga >= 1 Then
                        Tabella.Rows(Riga - 1).Item("ImportoCalcolato") = Format(TotaleImporto / 100, "0.00").Replace(",", ".")
                    End If
                    UltimoIdMandato = campodb(Tabella.Rows(Riga).Item("IdMandato"))
                    UltimoImporto = campodbN(Tabella.Rows(Riga).Item("ImportoTotale"))
                    RegistrazioneRiferimento = campodbN(Tabella.Rows(Riga).Item("NumeroRegistrazione"))

                    TotaleImporto = campodbN(Tabella.Rows(Riga).Item("ImportoTotale"))
                End If
            Else
                If Riga >= 1 Then
                    Tabella.Rows(Riga - 1).Item("ImportoCalcolato") = Format(TotaleImporto / 100, "0.00").Replace(",", ".")
                End If
                UltimoIdMandato = campodb(Tabella.Rows(Riga).Item("IdMandato"))
                RegistrazioneRiferimento = ""
                UltimoImporto = 0
                TotaleImporto = 0
            End If
        Next
        cn.Close()

        If Riga >= 1 And TotaleImporto > 0 Then
            Tabella.Rows(Riga - 1).Item("ImportoCalcolato") = Format(TotaleImporto / 100, "0.00").Replace(",", ".")
        End If



        For Riga = 0 To GridView1.Rows.Count - 1
            Dim CheckBox As CheckBox = DirectCast(GridView1.Rows(Riga).FindControl("ChkImporta"), CheckBox)


            If CheckBox.Checked = True And CDbl(Tabella.Rows(Riga).Item("ImportoCalcolato")) > 0 Then
                AppoggioNumero = AppoggioNumero + 1
                AppoggioTotale = AppoggioTotale + Replace(Tabella.Rows(Riga).Item("ImportoCalcolato"), ".", ",")

                XML = XML & "<DrctDbtTxInf>" & vbNewLine
                XML = XML & "<PmtId>" & vbNewLine
                If campodbN(Tabella.Rows(Riga).Item("NumeroRegistrazioneRif")) = 0 Then
                    XML = XML & "<InstrId>" & campodbN(Tabella.Rows(Riga).Item("NumeroRegistrazione")) & "</InstrId>" & vbNewLine
                Else
                    XML = XML & "<InstrId>" & campodbN(Tabella.Rows(Riga).Item("NumeroRegistrazione")) & "-" & campodbN(Tabella.Rows(Riga).Item("NumeroRegistrazioneRif")) & "</InstrId>" & vbNewLine
                End If

                XML = XML & "<EndToEndId>" & KTab.EndToEndId & campodbN(Tabella.Rows(Riga).Item("NumeroRegistrazione")) & "</EndToEndId>" & vbNewLine
                XML = XML & "</PmtId>" & vbNewLine

                XML = XML & "<InstdAmt Ccy=""EUR"">" & Tabella.Rows(Riga).Item("ImportoCalcolato") & "</InstdAmt>" & vbNewLine

                XML = XML & "<DrctDbtTx>" & vbNewLine
                XML = XML & "<MndtRltdInf>" & vbNewLine

                XML = XML & "<MndtId>" & Tabella.Rows(Riga).Item("IdMandato") & "</MndtId>" & vbNewLine
                XML = XML & "<DtOfSgntr>" & Tabella.Rows(Riga).Item("DataMandato") & "</DtOfSgntr>" & vbNewLine
                XML = XML & "</MndtRltdInf>" & vbNewLine
                XML = XML & "</DrctDbtTx>" & vbNewLine
                XML = XML & "<Dbtr>" & vbNewLine

                If Trim(Tabella.Rows(Riga).Item("CognomeNomeIntestatario")) <> "" Then
                    XML = XML & "<Nm>" & Tabella.Rows(Riga).Item("CognomeNomeIntestatario") & "</Nm>" & vbNewLine
                Else
                    XML = XML & "<Nm>" & Tabella.Rows(Riga).Item("CognomeNome") & "</Nm>" & vbNewLine
                End If

                If Tabella.Rows(Riga).Item("DO11_INDIRIZZO") <> "" Then
                    XML = XML & "<PstlAdr>" & vbNewLine
                    XML = XML & "<AdrLine>" & Tabella.Rows(Riga).Item("DO11_INDIRIZZO") & "</AdrLine>" & vbNewLine
                    XML = XML & "<AdrLine>" & Tabella.Rows(Riga).Item("AdrLine") & "</AdrLine>" & vbNewLine
                    XML = XML & "</PstlAdr>" & vbNewLine
                End If

                XML = XML & "</Dbtr>" & vbNewLine
                XML = XML & "<DbtrAcct>" & vbNewLine
                XML = XML & "<Id>" & vbNewLine
                XML = XML & "<IBAN>" & Tabella.Rows(Riga).Item("Iban") & "</IBAN>" & vbNewLine
                XML = XML & "</Id>" & vbNewLine
                XML = XML & "</DbtrAcct>" & vbNewLine
                XML = XML & "</DrctDbtTxInf>" & vbNewLine

                If Tabella.Rows(Riga).Item("First") = "SI" Then
                    RecurrentOrFirst = "FRST"
                Else
                    RecurrentOrFirst = "RCUR"
                End If

            End If

        Next



        XML = XML & "</PmtInf>"
        XML = XML & "</CBISDDReqLogMsg>"

        XML = XML.Replace("@NbOfTxs@", AppoggioNumero)
        XML = XML.Replace("@CtrlSum@", Format(AppoggioTotale, "0.00").Replace(",", "."))

        XML = XML.Replace("@SEQTP@", RecurrentOrFirst)


        tw.WriteLine(XML)

        tw.Close()

        tw = Nothing
        Dim ErroriNonCongruita As String = ""

        Session("CSVFILE") = "COGNOME NOME,IBAN,CODICE FISCALE,DATA MANDATO,IDMANDATO,IMPORTOTOTALE" & vbNewLine

        For Riga = GridView1.Rows.Count - 1 To 0 Step -1
            Dim CheckBox As CheckBox = DirectCast(GridView1.Rows(Riga).FindControl("ChkImporta"), CheckBox)

            If CheckBox.Checked = True And CDbl(Tabella.Rows(Riga).Item("ImportoCalcolato")) > 0 Then
                ErroriNonCongruita = ErroriNonCongruita & "<tr><td>" & Tabella.Rows(Riga).Item("CognomeNome") & "</td>"
                ErroriNonCongruita = ErroriNonCongruita & "<td>" & Tabella.Rows(Riga).Item("Iban") & "</td>"
                ErroriNonCongruita = ErroriNonCongruita & "<td>" & Tabella.Rows(Riga).Item("CodiceFiscale") & "</td>"
                ErroriNonCongruita = ErroriNonCongruita & "<td>" & Tabella.Rows(Riga).Item("DataMandato") & "</td>"
                ErroriNonCongruita = ErroriNonCongruita & "<td>" & Tabella.Rows(Riga).Item("IdMandato") & "</td>"
                ErroriNonCongruita = ErroriNonCongruita & "<td style=""text-align:right;"">" & Tabella.Rows(Riga).Item("ImportoCalcolato") & "</td>"
                ErroriNonCongruita = ErroriNonCongruita & "</tr>"

                Session("CSVFILE") = Session("CSVFILE") & Tabella.Rows(Riga).Item("CognomeNome") & "," & Tabella.Rows(Riga).Item("Iban") & "," & Tabella.Rows(Riga).Item("CodiceFiscale") & "," & Tabella.Rows(Riga).Item("DataMandato") & "," & Tabella.Rows(Riga).Item("IdMandato") & "," & Tabella.Rows(Riga).Item("ImportoCalcolato") & vbNewLine
            End If
        Next

        ErroriNonCongruita = ErroriNonCongruita & "<tr><td><b>" & DD_Banca.SelectedItem.Text & "</b></td>"
        ErroriNonCongruita = ErroriNonCongruita & "<td><b>NUMERO RID ESTRATTI</b></td>"
        ErroriNonCongruita = ErroriNonCongruita & "<td><b>" & AppoggioNumero & "</b></td>"
        ErroriNonCongruita = ErroriNonCongruita & "<td></td>"
        ErroriNonCongruita = ErroriNonCongruita & "<td><b>PER UN TOTALE DI: </b></td>"
        ErroriNonCongruita = ErroriNonCongruita & "<td style=""text-align:right;""><b>" & Format(AppoggioTotale, "#,##0.00") & "</b></td>"
        ErroriNonCongruita = ErroriNonCongruita & "</tr>"


        GridView1.DataSource = Tabella
        GridView1.DataBind()
        GridView1.Visible = True

        Session("NOMEFILE") = NomeFile

        Session("ERRORIIMPORT") = ErroriNonCongruita

        REM ClientScript.RegisterClientScriptBlock(Me.GetType(), "MyBox", "$(window).load(function() { __doPostBack('Btn_Dowload', '0');  DialogBoxx('ReportRidCreati.aspx');  });", True)

        Response.Redirect("Export_RID_Step2.aspx?ID=" & Txt_MsgID.Text)

    End Sub



    Private Sub EseguiJS()
        Dim MyJs As String
        MyJs = "$(document).ready(function() {"

        MyJs = MyJs & "var els = document.getElementsByTagName(""*"");"

        MyJs = MyJs & "for (var i=0;i<els.length;i++)"
        MyJs = MyJs & "if ( els[i].id ) { "
        MyJs = MyJs & " var appoggio =els[i].id; "
        MyJs = MyJs & " if (appoggio.match('Txt_Data')!= null) {  "
        MyJs = MyJs & " $(els[i]).mask(""99/99/9999"");"

        MyJs = MyJs & " $(els[i]).datepicker({ changeMonth: true,changeYear: true,  yearRange: ""1990:" & Year(Now) + 5 & """},$.datepicker.regional[""it""]);"
        MyJs = MyJs & "    }"
        MyJs = MyJs & " if (appoggio.match('Txt_Scadenza')!= null) {  "
        MyJs = MyJs & " $(els[i]).mask(""99/99/9999"");"

        MyJs = MyJs & " $(els[i]).datepicker({ changeMonth: true,changeYear: true,  yearRange: ""1990:" & Year(Now) + 5 & """},$.datepicker.regional[""it""]);"
        MyJs = MyJs & "    }"

        MyJs = MyJs & " if ((appoggio.match('Txt_Importo')!= null)) {  "
        MyJs = MyJs & " $(els[i]).keypress(function() { ForceNumericInput($(this).val(), true, true); } ); "
        MyJs = MyJs & " $(els[i]).blur(function() { var ap = formatNumber($(this).val(),2); $(this).val(ap); });"
        MyJs = MyJs & "    }"

        MyJs = MyJs & " if ((appoggio.match('Txt_NomeOspiti')!= null) || (appoggio.match('Txt_NomeOspiti')!= null))  {  "
        MyJs = MyJs & " $(els[i]).autocomplete('AutoCompleteOspitiTuttiCentriServizio.ashx?Utente=" & Session("UTENTE") & "', {delay:5,minChars:3});"
        MyJs = MyJs & "    }"
        MyJs = MyJs & "} "
        MyJs = MyJs & "});"

        'MyJs = "$(document).ready(function() { $('.myClass').keypress(function() { handleEnter($('.myClass').val(), true, true); } );     $('#" & Txt_ImportoPagato.ClientID & "').blur(function() { var ap = formatNumber ($('#" & Txt_ImportoPagato.ClientID & "').val(),2); $('#" & Txt_ImportoPagato.ClientID & "').val(ap); }); });"
        ScriptManager.RegisterStartupScript(Me, Me.GetType(), "visualizzaRitIm", MyJs, True)
    End Sub

    Function MinimoDocumento() As Long
        Dim cn As OleDbConnection

        Dim MeseContr As Long
        Dim MySql As String

        MeseContr = DD_Mese.SelectedValue

        MinimoDocumento = 0



        MySql = "SELECT MIN(NumeroProtocollo) " & _
                " FROM MovimentiContabiliRiga INNER JOIN MovimentiContabiliTesta " & _
                " ON MovimentiContabiliRiga.Numero = MovimentiContabiliTesta.NumeroRegistrazione " & _
                " WHERE MovimentiContabiliTesta.AnnoCompetenza = " & Txt_Anno.Text & " " & _
                " AND MovimentiContabiliTesta.MeseCompetenza = " & MeseContr & " " & _
                " AND AnnoProtocollo = " & Txt_AnnoRif.Text

        If DD_Registro.SelectedValue <> "" Then
            MySql = MySql & " AND MovimentiContabiliTesta.RegistroIva = " & DD_Registro.SelectedValue
        End If




        cn = New Data.OleDb.OleDbConnection(Session("DC_GENERALE"))

        cn.Open()
        Dim cmd As New OleDbCommand()
        cmd.CommandText = MySql

        cmd.Connection = cn


        Dim myPOSTreader As OleDbDataReader = cmd.ExecuteReader()
        If myPOSTreader.Read Then
            If IsDBNull(myPOSTreader.Item(0)) Then
                MinimoDocumento = 0
            Else
                MinimoDocumento = Val(myPOSTreader.Item(0))
            End If
        End If
        myPOSTreader.Close()
        cn.Close()

    End Function
    Function MassimoDocumento() As Long
        Dim cn As OleDbConnection

        Dim MeseContr As Long
        Dim MySql As String

        MeseContr = DD_Mese.SelectedValue

        MassimoDocumento = 0



        MySql = "SELECT MAX(NumeroProtocollo) " & _
                " FROM MovimentiContabiliRiga INNER JOIN MovimentiContabiliTesta " & _
                " ON MovimentiContabiliRiga.Numero = MovimentiContabiliTesta.NumeroRegistrazione " & _
                " WHERE MovimentiContabiliTesta.AnnoCompetenza = " & Txt_Anno.Text & " " & _
                " AND MovimentiContabiliTesta.MeseCompetenza = " & MeseContr & " " & _
                " AND AnnoProtocollo = " & Txt_AnnoRif.Text


        If DD_Registro.SelectedValue <> "" Then
            MySql = MySql & " AND MovimentiContabiliTesta.RegistroIva = " & DD_Registro.SelectedValue
        End If




        cn = New Data.OleDb.OleDbConnection(Session("DC_GENERALE"))

        cn.Open()
        Dim cmd As New OleDbCommand()
        cmd.CommandText = MySql

        cmd.Connection = cn


        Dim myPOSTreader As OleDbDataReader = cmd.ExecuteReader()
        If myPOSTreader.Read Then
            If IsDBNull(myPOSTreader.Item(0)) Then
                MassimoDocumento = 0
            Else
                MassimoDocumento = Val(myPOSTreader.Item(0))
            End If
        End If
        myPOSTreader.Close()
        cn.Close()

    End Function

    Protected Sub DD_Registro_TextChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles DD_Registro.TextChanged
        Txt_DalDocumento.Text = MinimoDocumento()
        Txt_AlDocumento.Text = MassimoDocumento()
    End Sub

    Protected Sub DD_Mese_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles DD_Mese.SelectedIndexChanged
        Txt_DalDocumento.Text = MinimoDocumento()
        Txt_AlDocumento.Text = MassimoDocumento()
    End Sub

    Protected Sub Txt_Anno_TextChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles Txt_Anno.TextChanged
        Txt_DalDocumento.Text = MinimoDocumento()
        Txt_AlDocumento.Text = MassimoDocumento()
    End Sub

    Protected Sub Txt_AnnoRif_TextChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles Txt_AnnoRif.TextChanged
        Txt_DalDocumento.Text = MinimoDocumento()
        Txt_AlDocumento.Text = MassimoDocumento()
    End Sub


    Protected Sub OspitiWeb_Export_Documenti_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Trim(Session("UTENTE")) = "" Then
            Response.Redirect("..\Login.aspx")
            Exit Sub
        End If

        EseguiJS()

        If Page.IsPostBack = True Then Exit Sub


        Dim K1 As New Cls_SqlString

        Dim Barra As New Cls_BarraSenior

        If Not IsNothing(Session("RicercaAnagraficaSQLString")) Then
            K1 = Session("RicercaAnagraficaSQLString")
        End If

        Lbl_BarraSenior.Text = Barra.CodiceBarra(Application("SENIOR"), Session("UTENTE"), Page, K1)


        Dim X As New Cls_RegistroIVA
        Dim f As New Cls_Parametri
        Lbl_Waiting.Text = ""
        f.LeggiParametri(Session("DC_OSPITE"))
        X.UpDateDropBox(Session("DC_TABELLE"), DD_Registro)

        Txt_Anno.Text = f.AnnoFatturazione
        DD_Mese.SelectedValue = f.MeseFatturazione
        Txt_AnnoRif.Text = f.AnnoFatturazione

        Dim Bn As New Cls_Banche

        Bn.UpDateDropBox(Session("DC_OSPITE"), DD_Banca)


        Dim kVilla As New Cls_TabelleDescrittiveOspitiAccessori


        kVilla.UpDateDropBox(Session("DC_OSPITIACCESSORI"), "VIL", DD_Struttura)
        If DD_Struttura.Items.Count = 1 Then
            Call AggiornaCServ()
        End If




        Dim XS As New Cls_Login

        XS.Utente = Session("UTENTE")
        XS.LeggiSP(Application("SENIOR"))

        Session("CampoProgressBar") = 0


        Dim cn As OleDbConnection



        cn = New Data.OleDb.OleDbConnection(Session("DC_GENERALE"))

        cn.Open()


        Dim Cmd As New OleDbCommand

        Cmd.CommandText = "select max(IdRid) from MOVIMENTICONTABILITESTA "
        Cmd.Connection = cn
        Cmd.ExecuteNonQuery()
        Dim myPOSTreader As OleDbDataReader = Cmd.ExecuteReader()
        If myPOSTreader.Read Then
            Txt_MsgID.Text = campodb(myPOSTreader.Item(0))
        End If
        cn.Close()


        Txt_NomeOspiti.Text = ""
        Txt_DataRid.Text = ""
        Txt_Importo.Text = "0,00"
    End Sub

    Protected Sub Btn_Modifica_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles Btn_Modifica.Click
        

        If Txt_MsgID.Text = "" Or Txt_DataScadenza.Text = "" Then
            ScriptManager.RegisterClientScriptBlock(Me, Me.GetType(), "Stampa2", "alert('Indicare MSG ID e Data Scadenza');", True)
            TabContainer1.ActiveTabIndex = 2
            Exit Sub
        End If

        

        Call CreaGriglia()

        Btn_Salva.Visible = True
        TabContainer1.ActiveTabIndex = 2

    End Sub




    Function campodbD(ByVal oggetto As Object) As Date
        If IsDBNull(oggetto) Then
            Return Nothing
        Else
            Return oggetto
        End If
    End Function
    Function campodb(ByVal oggetto As Object) As String
        If IsDBNull(oggetto) Then
            Return ""
        Else
            Return oggetto
        End If
    End Function

    Function campodbN(ByVal oggetto As Object) As Double
        If IsDBNull(oggetto) Then
            Return 0
        Else
            Return oggetto
        End If
    End Function




    Function AdattaLunghezzaNumero(ByVal Testo As String, ByVal Lunghezza As Integer) As String
        If Len(Testo) > Lunghezza Then
            AdattaLunghezzaNumero = Mid(Testo, 1, Lunghezza)
            Exit Function
        End If
        AdattaLunghezzaNumero = Replace(Space(Lunghezza - Len(Testo)) & Testo, " ", "0")
        REM AdattaLunghezzaNumero = Space(Lunghezza - Len(Testo)) & Testo


    End Function

    Function AdattaLunghezza(ByVal Testo As String, ByVal Lunghezza As Integer) As String

        If Len(Testo) > Lunghezza Then
            Testo = Mid(Testo, 1, Lunghezza)
        End If

        AdattaLunghezza = Testo & Space(Lunghezza - Len(Testo) + 1)

        If Len(AdattaLunghezza) > Lunghezza Then
            AdattaLunghezza = Mid(AdattaLunghezza, 1, Lunghezza)
        End If

    End Function




    Sub Export_Gamma()

        Dim cn As OleDbConnection

        Dim MeseContr As Long
        Dim MySql As String
        Dim ProgressivoAssoluto As Integer = 0
        Dim XML As String

        MeseContr = DD_Mese.SelectedValue


        MySql = "SELECT * " & _
                " FROM MovimentiContabiliTesta " & _
                " WHERE MovimentiContabiliTesta.AnnoCompetenza = " & Txt_Anno.Text & " " & _
                " AND MovimentiContabiliTesta.MeseCompetenza = " & MeseContr & " " & _
                " AND AnnoProtocollo = " & Txt_AnnoRif.Text

        If DD_Registro.SelectedValue <> "" Then
            MySql = MySql & " AND MovimentiContabiliTesta.RegistroIva = " & DD_Registro.SelectedValue
        End If
        MySql = MySql & " AND (NumeroProtocollo >= " & Txt_DalDocumento.Text & " And NumeroProtocollo <= " & Txt_AlDocumento.Text & ") "


        cn = New Data.OleDb.OleDbConnection(Session("DC_GENERALE"))

        cn.Open()
        Dim cmd As New OleDbCommand()
        cmd.CommandText = MySql & " Order by NumeroProtocollo"
        cmd.Connection = cn

        Dim CodiceInstallazione As Integer
        Dim CodiceDitta As Integer
        Dim ProgressivoTesta As Integer
        Dim RegistroIva As Integer
        Dim APPOGGIO As String
        Dim AppoggioTotale As Double = 0
        Dim AppoggioNumero As Integer = 0
        Dim DataScadenza As Date = Txt_DataScadenza.Text

        CodiceInstallazione = 1
        CodiceDitta = 1

        RegistroIva = DD_Registro.SelectedValue
        Dim NomeFile As String
        'Dim NomeFileRiga As String
        NomeFile = HostingEnvironment.ApplicationPhysicalPath() & "\Public\Esportazione_" & Session("NomeEPersonam") & "_" & RegistroIva & "_" & Format(Now, "yyyyMMddHHmm") & ".Txt"
        'NomeFileRiga = HostingEnvironment.ApplicationPhysicalPath() & "\Riga_" & CodiceDitta & "_" & Format(Now, "yyyyMMddHHmm") & ".Txt"

        Dim tw As System.IO.TextWriter = System.IO.File.CreateText(NomeFile)
        'Dim twR As System.IO.TextWriter = System.IO.File.CreateText(NomeFileRiga)

        XML = "<?xml version=""1.0"" encoding=""UTF-8""?>" & vbNewLine
        XML = XML & "<CBISDDReqLogMsg xmlns=""urn:CBI:xsd:CBISDDReqLogMsg.00.01.00"" xmlns:xsi=""http://www.w3.org/2001/XMLSchema-instance"">" & vbNewLine
        XML = XML & "<GrpHdr>" & vbNewLine
        XML = XML & "<MsgId>" & Txt_MsgID.Text & "</MsgId>" & vbNewLine
        XML = XML & "<CreDtTm>" & Format(Now, "yyyy-MM-dd") & "T" & Format(Now, "hh:mm:ss") & "</CreDtTm>" & vbNewLine
        XML = XML & "<NbOfTxs>@NbOfTxs@</NbOfTxs>" & vbNewLine
        XML = XML & "<CtrlSum>@CtrlSum@</CtrlSum>" & vbNewLine
        XML = XML & "<InitgPty>" & vbNewLine
        XML = XML & "<Nm>CASA ALBERGO DI LIETO SOGGIORNO SAS </Nm>" & vbNewLine
        XML = XML & "<Id>" & vbNewLine
        XML = XML & "<OrgId>" & vbNewLine
        XML = XML & "<Othr>" & vbNewLine
        XML = XML & "<Id>0935716D</Id>" & vbNewLine
        XML = XML & "<Issr>CBI</Issr>" & vbNewLine
        XML = XML & "</Othr>" & vbNewLine
        XML = XML & "</OrgId>" & vbNewLine
        XML = XML & "</Id>" & vbNewLine
        XML = XML & "</InitgPty>" & vbNewLine
        XML = XML & "</GrpHdr>" & vbNewLine
        XML = XML & "<PmtInf>" & vbNewLine
        XML = XML & "<PmtInfId>1</PmtInfId>" & vbNewLine
        XML = XML & "<PmtMtd>DD</PmtMtd>" & vbNewLine
        XML = XML & "<PmtTpInf>" & vbNewLine
        XML = XML & "<SvcLvl>" & vbNewLine
        XML = XML & "<Cd>SEPA</Cd>" & vbNewLine
        XML = XML & "</SvcLvl>" & vbNewLine
        XML = XML & "<LclInstrm>" & vbNewLine
        XML = XML & "<Cd>CORE</Cd>" & vbNewLine
        XML = XML & "</LclInstrm>" & vbNewLine
        XML = XML & "<SeqTp>RCUR</SeqTp>" & vbNewLine
        XML = XML & "</PmtTpInf>" & vbNewLine
        XML = XML & "<ReqdColltnDt>" & Format(DataScadenza, "yyyy-MM-dd") & "</ReqdColltnDt>" & vbNewLine
        XML = XML & "<Cdtr>" & vbNewLine
        XML = XML & "<Nm>CASA ALBERGO DI LIETO SOGGIORNO SAS </Nm>" & vbNewLine
        XML = XML & "<PstlAdr>" & vbNewLine
        XML = XML & "<AdrLine>MACERONE,  VIA 18 AGOSTO 110</AdrLine>" & vbNewLine
        XML = XML & "<AdrLine>47023 CESENA FC</AdrLine>" & vbNewLine
        XML = XML & "</PstlAdr>" & vbNewLine
        XML = XML & "</Cdtr>" & vbNewLine
        XML = XML & "<CdtrAcct>" & vbNewLine
        XML = XML & "<Id>" & vbNewLine
        XML = XML & "<IBAN>IT67E0313923906000000133719</IBAN>" & vbNewLine
        XML = XML & "</Id>" & vbNewLine
        XML = XML & "</CdtrAcct>" & vbNewLine
        XML = XML & "<CdtrAgt>" & vbNewLine
        XML = XML & "<FinInstnId>" & vbNewLine
        XML = XML & "<ClrSysMmbId>" & vbNewLine
        XML = XML & "<MmbId>03139</MmbId>" & vbNewLine
        XML = XML & "</ClrSysMmbId>" & vbNewLine
        XML = XML & "</FinInstnId>" & vbNewLine
        XML = XML & "</CdtrAgt>" & vbNewLine
        XML = XML & "<CdtrSchmeId>" & vbNewLine
        XML = XML & "<Id>" & vbNewLine
        XML = XML & "<PrvtId>" & vbNewLine
        XML = XML & "<Othr>" & vbNewLine
        XML = XML & "<Id>IT700010000000161970405</Id>" & vbNewLine
        XML = XML & "</Othr>" & vbNewLine
        XML = XML & "</PrvtId>" & vbNewLine
        XML = XML & "</Id>" & vbNewLine
        XML = XML & "</CdtrSchmeId>" & vbNewLine

        AppoggioNumero = 0
        AppoggioTotale = 0

        ProgressivoTesta = 0

        Dim myPOSTreader As OleDbDataReader = cmd.ExecuteReader()
        Do While myPOSTreader.Read
            ProgressivoTesta = ProgressivoTesta + 1


            APPOGGIO = ""
            Dim CausaleContabile As New Cls_CausaleContabile

            CausaleContabile.Codice = campodb(myPOSTreader.Item("CAUSALECONTABILE"))
            CausaleContabile.Leggi(Session("DC_TABELLE"), CausaleContabile.Codice)





            Dim Cliente As String = ""

            Dim Tipo As String = campodb(myPOSTreader.Item("Tipologia"))
            Dim CodiceOspite As Integer = 0
            Dim CodiceParente As Integer = 0

            Dim CodiceProvincia As String = ""
            Dim CodiceComune As String = ""
            Dim CodiceRegione As String = ""
            Dim ImporotTotale As Double = 0


            Dim cmdRd As New OleDbCommand()
            cmdRd.CommandText = "Select * From MovimentiContabiliRiga Where Numero = ?  And Tipo ='CF'   "
            cmdRd.Connection = cn
            cmdRd.Parameters.AddWithValue("@Numero", campodbN(myPOSTreader.Item("NumeroRegistrazione")))
            Dim MyReadSC As OleDbDataReader = cmdRd.ExecuteReader()
            If MyReadSC.Read Then
                Cliente = campodb(MyReadSC.Item("MastroPartita")) & "." & campodb(MyReadSC.Item("ContoPartita")) & "." & campodb(MyReadSC.Item("SottocontoPartita"))
                If Mid(Tipo & Space(10), 1, 1) = " " Then
                    CodiceOspite = Int(campodbN(MyReadSC.Item("SottocontoPartita")) / 100)
                    CodiceParente = campodbN(MyReadSC.Item("SottocontoPartita")) - (CodiceOspite * 100)

                    If CodiceParente = 0 And CodiceOspite > 0 Then
                        Tipo = "O" & Space(10)
                    End If
                    If CodiceParente > 0 And CodiceOspite > 0 Then
                        Tipo = "P" & Space(10)
                    End If
                End If
                If Mid(Tipo & Space(10), 1, 1) = "C" Then
                    CodiceProvincia = Mid(Tipo & Space(10), 2, 3)
                    CodiceComune = Mid(Tipo & Space(10), 5, 3)
                End If
                If Mid(Tipo & Space(10), 1, 1) = "J" Then
                    CodiceProvincia = Mid(Tipo & Space(10), 2, 3)
                    CodiceComune = Mid(Tipo & Space(10), 5, 3)
                End If
                If Mid(Tipo & Space(10), 1, 1) = "R" Then
                    CodiceRegione = Mid(Tipo & Space(10), 2, 4)
                End If

                ImporotTotale = campodbN(MyReadSC.Item("Importo"))
            End If
            MyReadSC.Close()


            Dim DO11_RAGIONESOCIALE As String = ""
            Dim DO11_OSPITERIFERIMENTO As String = ""
            Dim DO11_PIVA As String = ""
            Dim DO11_CF As String = ""
            Dim DO11_INDIRIZZO As String = ""
            Dim DO11_CAP As String = ""
            Dim DO11_CITTA_NOME As String = ""
            Dim DO11_CITTA_ISTAT As String = ""
            Dim DO11_PROVINCIA As String = ""
            Dim DO11_CONDPAG_CODICE_SEN As String = ""
            Dim DO11_CONDPAG_NOME_SEN As String = ""
            Dim DO11_BANCA_CIN As String = ""
            Dim DO11_BANCA_ABI As String = ""
            Dim DO11_BANCA_CAB As String = ""
            Dim DO11_BANCA_CONTO As String = ""
            Dim DO11_BANCA_IBAN As String = ""
            Dim DO11_BANCA_IDMANDATO As String = ""
            Dim DO11_BANCA_DATAMANDATO As String = ""
            Dim AddDescrizione As String
            Dim DO11_TIPOFLUSSO As String = ""
            Dim DO11_BANCA_DESTINATARIO As String = ""
            Dim DO11_BANCA_CFDESTINATARIO As String = ""


            AddDescrizione = ""
            If Mid(Tipo & Space(10), 1, 1) = "O" Then
                Dim Ospite As New ClsOspite

                Ospite.CodiceOspite = CodiceOspite
                Ospite.Leggi(Session("DC_OSPITE"), Ospite.CodiceOspite)
                DO11_RAGIONESOCIALE = Ospite.Nome
                DO11_OSPITERIFERIMENTO = Ospite.Nome
                DO11_PIVA = ""
                DO11_CF = Ospite.CODICEFISCALE
                DO11_INDIRIZZO = Ospite.RESIDENZAINDIRIZZO1
                DO11_CAP = Ospite.RESIDENZACAP1
                Dim DcCom As New ClsComune

                DcCom.Provincia = Ospite.RESIDENZAPROVINCIA1
                DcCom.Comune = Ospite.RESIDENZACOMUNE1
                DcCom.Leggi(Session("DC_OSPITE"))

                DO11_CITTA_NOME = DcCom.Descrizione
                DO11_CITTA_ISTAT = DcCom.Provincia & DcCom.Comune

                Dim DcProv As New ClsComune

                DcProv.Provincia = Ospite.RESIDENZAPROVINCIA1
                DcProv.Comune = ""
                DcProv.Leggi(Session("DC_OSPITE"))
                DO11_PROVINCIA = DcProv.CodificaProvincia


                Dim Kl As New Cls_DatiOspiteParenteCentroServizio

                Kl.CodiceOspite = CodiceOspite
                Kl.CodiceParente = 0
                Kl.CentroServizio = campodb(myPOSTreader.Item("CentroServizio"))
                Kl.Leggi(Session("DC_OSPITE"))
                If Kl.ModalitaPagamento <> "" Then
                    Ospite.MODALITAPAGAMENTO = Kl.ModalitaPagamento
                End If


                DO11_CONDPAG_CODICE_SEN = Ospite.MODALITAPAGAMENTO
                Dim MPAg As New ClsModalitaPagamento

                MPAg.Codice = Ospite.MODALITAPAGAMENTO
                MPAg.Leggi(Session("DC_OSPITE"))
                DO11_CONDPAG_NOME_SEN = MPAg.DESCRIZIONE

                DO11_TIPOFLUSSO = MPAg.Tipo
                Dim ModPag As New Cls_DatiPagamento

                ModPag.CodiceOspite = Ospite.CodiceOspite
                ModPag.CodiceParente = 0
                ModPag.LeggiUltimaDataData(Session("DC_OSPITE"), campodb(myPOSTreader.Item("DataDocumento")))

                DO11_BANCA_CIN = ModPag.Cin
                DO11_BANCA_ABI = ModPag.Abi
                DO11_BANCA_CAB = ModPag.Cin
                DO11_BANCA_CONTO = ModPag.CCBancario
                DO11_BANCA_IBAN = ModPag.CodiceInt & Format(Val(ModPag.NumeroControllo), "00") & ModPag.Cin & Format(Val(ModPag.Abi), "00000") & Format(Val(ModPag.Cab), "00000") & AdattaLunghezzaNumero(ModPag.CCBancario, 12)

                DO11_BANCA_IDMANDATO = ModPag.IdMandatoDebitore
                DO11_BANCA_DATAMANDATO = ModPag.DataMandatoDebitore
                DO11_BANCA_CFDESTINATARIO = ModPag.IntestatarioCFConto
                DO11_BANCA_DESTINATARIO = ModPag.IntestatarioConto

            End If

            If Mid(Tipo & Space(10), 1, 1) = "P" Then
                Dim OspiteRIFERIMENTO As New ClsOspite

                OspiteRIFERIMENTO.CodiceOspite = CodiceOspite
                OspiteRIFERIMENTO.Leggi(Session("DC_OSPITE"), OspiteRIFERIMENTO.CodiceOspite)

                DO11_OSPITERIFERIMENTO = OspiteRIFERIMENTO.Nome


                Dim Ospite As New Cls_Parenti

                Ospite.CodiceOspite = CodiceOspite
                Ospite.CodiceParente = CodiceParente
                Ospite.Leggi(Session("DC_OSPITE"), Ospite.CodiceOspite, Ospite.CodiceParente)
                DO11_RAGIONESOCIALE = Ospite.Nome
                DO11_PIVA = ""
                DO11_CF = Ospite.CODICEFISCALE
                DO11_INDIRIZZO = Ospite.RESIDENZAINDIRIZZO1
                DO11_CAP = Ospite.RESIDENZACAP1
                Dim DcCom As New ClsComune

                DcCom.Provincia = Ospite.RESIDENZAPROVINCIA1
                DcCom.Comune = Ospite.RESIDENZACOMUNE1
                DcCom.Leggi(Session("DC_OSPITE"))

                DO11_CITTA_NOME = DcCom.Descrizione
                DO11_CITTA_ISTAT = DcCom.Provincia & DcCom.Comune

                Dim DcProv As New ClsComune

                DcProv.Provincia = Ospite.RESIDENZAPROVINCIA1
                DcProv.Comune = ""
                DcProv.Leggi(Session("DC_OSPITE"))
                DO11_PROVINCIA = DcProv.CodificaProvincia
                DO11_CONDPAG_CODICE_SEN = Ospite.MODALITAPAGAMENTO

                Dim Kl As New Cls_DatiOspiteParenteCentroServizio

                Kl.CodiceOspite = CodiceOspite
                Kl.CodiceParente = CodiceParente
                Kl.CentroServizio = campodb(myPOSTreader.Item("CentroServizio"))
                Kl.Leggi(Session("DC_OSPITE"))
                If Kl.ModalitaPagamento <> "" Then
                    Ospite.MODALITAPAGAMENTO = Kl.ModalitaPagamento
                End If

                Dim MPAg1 As New ClsModalitaPagamento

                MPAg1.Codice = Ospite.MODALITAPAGAMENTO
                MPAg1.Leggi(Session("DC_OSPITE"))
                DO11_CONDPAG_NOME_SEN = MPAg1.DESCRIZIONE

                DO11_TIPOFLUSSO = MPAg1.Tipo

                Dim ModPag As New Cls_DatiPagamento

                ModPag.CodiceOspite = Ospite.CodiceOspite
                ModPag.CodiceParente = Ospite.CodiceParente
                ModPag.LeggiUltimaDataData(Session("DC_OSPITE"), campodb(myPOSTreader.Item("DataDocumento")))

                DO11_BANCA_CIN = ModPag.Cin
                DO11_BANCA_ABI = ModPag.Abi
                DO11_BANCA_CAB = ModPag.Cin
                DO11_BANCA_CONTO = ModPag.CCBancario
                DO11_BANCA_IBAN = ModPag.CodiceInt & Format(Val(ModPag.NumeroControllo), "00") & ModPag.Cin & Format(Val(ModPag.Abi), "00000") & Format(Val(ModPag.Cab), "00000") & AdattaLunghezzaNumero(ModPag.CCBancario, 12)

                DO11_BANCA_IDMANDATO = ModPag.IdMandatoDebitore
                DO11_BANCA_DATAMANDATO = ModPag.DataMandatoDebitore

                DO11_BANCA_CFDESTINATARIO = ModPag.IntestatarioCFConto
                DO11_BANCA_DESTINATARIO = ModPag.IntestatarioConto
            End If


            If DO11_CONDPAG_CODICE_SEN = "" Then
                DO11_CONDPAG_CODICE_SEN = "XX"
            End If
            Dim M As New Cls_TipoPagamento
            M.Descrizione = ""
            M.Codice = DO11_CONDPAG_CODICE_SEN
            M.Leggi(Session("DC_TABELLE"))
            If M.Descrizione <> "" Then
                DO11_CONDPAG_CODICE_SEN = M.Codice
                DO11_CONDPAG_NOME_SEN = M.Descrizione
            End If

            Dim MPAgP As New ClsModalitaPagamento

            MPAgP.Codice = DO11_CONDPAG_CODICE_SEN
            MPAgP.Leggi(Session("DC_OSPITE"))
            DO11_TIPOFLUSSO = MPAgP.Tipo


            If M.Descrizione.IndexOf("R.I.D.") > 0 Or M.Descrizione.IndexOf("SEPA") > 0 Or DO11_TIPOFLUSSO = "R" Then

                Dim AppoggioData As Date

                AppoggioTotale = AppoggioTotale + ImporotTotale
                AppoggioNumero = AppoggioNumero + 1


                Try
                    AppoggioData = DO11_BANCA_DATAMANDATO
                Catch ex As Exception

                End Try

                XML = XML & "<DrctDbtTxInf>" & vbNewLine
                XML = XML & "<PmtId>" & vbNewLine
                XML = XML & "<InstrId>" & campodbN(myPOSTreader.Item("NumeroRegistrazione")) & "</InstrId>" & vbNewLine
                XML = XML & "<EndToEndId>39" & campodbN(myPOSTreader.Item("NumeroRegistrazione")) & "</EndToEndId>" & vbNewLine
                XML = XML & "</PmtId>" & vbNewLine
                XML = XML & "<InstdAmt Ccy=""EUR"">" & Format(ImporotTotale, "0.00").Replace(",", ".") & "</InstdAmt>" & vbNewLine
                XML = XML & "<DrctDbtTx>" & vbNewLine
                XML = XML & "<MndtRltdInf>" & vbNewLine
                XML = XML & "<MndtId>" & DO11_BANCA_IDMANDATO & "</MndtId>" & vbNewLine
                XML = XML & "<DtOfSgntr>" & Format(AppoggioData, "yyyy-MM-dd") & "</DtOfSgntr>" & vbNewLine
                XML = XML & "</MndtRltdInf>" & vbNewLine
                XML = XML & "</DrctDbtTx>" & vbNewLine
                XML = XML & "<Dbtr>" & vbNewLine

                If Trim(DO11_BANCA_DESTINATARIO) = "" Then
                    XML = XML & "<Nm>" & DO11_RAGIONESOCIALE & "</Nm>" & vbNewLine
                    XML = XML & "<PstlAdr>" & vbNewLine
                    XML = XML & "<AdrLine>" & DO11_INDIRIZZO & "</AdrLine>" & vbNewLine
                    XML = XML & "<AdrLine>" & DO11_CAP & " " & DO11_CITTA_NOME & " " & DO11_PROVINCIA & "</AdrLine>" & vbNewLine
                    XML = XML & "</PstlAdr>" & vbNewLine
                Else
                    XML = XML & "<Nm>" & DO11_BANCA_DESTINATARIO & "</Nm>" & vbNewLine
                End If

                XML = XML & "</Dbtr>" & vbNewLine
                XML = XML & "<DbtrAcct>" & vbNewLine
                XML = XML & "<Id>" & vbNewLine
                XML = XML & "<IBAN>" & DO11_BANCA_IBAN & "</IBAN>" & vbNewLine
                XML = XML & "</Id>" & vbNewLine
                XML = XML & "</DbtrAcct>" & vbNewLine
                XML = XML & "<RmtInf>" & vbNewLine
                XML = XML & "<Ustrd>N.Doc.: " & campodb(myPOSTreader.Item("NumeroProtocollo")) & " del " & Format(campodbD(myPOSTreader.Item("Dataregistrazione")), "dd/MM/yyyy") & " Ospite: " & DO11_OSPITERIFERIMENTO & "</Ustrd>"
                XML = XML & "</RmtInf>"
                XML = XML & "</DrctDbtTxInf>" & vbNewLine

            End If

        Loop
        myPOSTreader.Close()
        cn.Close()

        XML = XML & "</PmtInf>"
        XML = XML & "</CBISDDReqLogMsg>"

        XML = XML.Replace("@NbOfTxs@", AppoggioNumero)
        XML = XML.Replace("@CtrlSum@", Format(AppoggioTotale, "0.00").Replace(",", "."))

        tw.WriteLine(XML)

        tw.Close()

        tw = Nothing
        'twR = Nothing

        Response.Clear()
        Response.Buffer = True
        Response.ContentType = "text/plain"
        Response.AppendHeader("content-disposition", "attachment;filename=Rid" & Txt_MsgID.Text & ".xml")
        Response.WriteFile(NomeFile)
        Response.Flush()
        Response.End()
    End Sub



    Sub CreaGriglia()

        Dim cn As OleDbConnection

        Dim MeseContr As Long
        Dim MySql As String
        Dim ProgressivoAssoluto As Integer = 0
        Dim XML As String

        MeseContr = DD_Mese.SelectedValue

        Try
            Tabella.Clear()
            Tabella.Columns.Clear()

            Tabella.Columns.Add("CognomeNome", GetType(String)) '0
            Tabella.Columns.Add("Iban", GetType(String)) '1
            Tabella.Columns.Add("CodiceFiscale", GetType(String)) '2
            Tabella.Columns.Add("DataMandato", GetType(String)) '3
            Tabella.Columns.Add("IdMandato", GetType(String)) '4
            Tabella.Columns.Add("ImportoTotale", GetType(String)) '5
            Tabella.Columns.Add("NumeroRegistrazione", GetType(String)) '6
            Tabella.Columns.Add("DO11_INDIRIZZO", GetType(String)) '7
            Tabella.Columns.Add("AdrLine", GetType(String)) '8
            Tabella.Columns.Add("First", GetType(String)) '9
            Tabella.Columns.Add("CODICEOSPITE", GetType(String)) '10
            Tabella.Columns.Add("CODICEPARENTE", GetType(String)) '11
            Tabella.Columns.Add("CognomeNomeIntestatario", GetType(String)) '12
            Tabella.Columns.Add("ImportoCalcolato", GetType(String)) '13
            Tabella.Columns.Add("NumeroRegistrazioneRif", GetType(String)) '14
        Catch ex As Exception

        End Try


        Dim Condizione As String = ""

        If DD_Struttura.SelectedValue <> "" Then
            If DD_CServ.SelectedValue <> "" Then
                Condizione = " And CentroServizio =  '" & DD_CServ.SelectedValue & "'"
            Else
                Dim Indice As Integer

                For Indice = 0 To DD_CServ.Items.Count - 1
                    If DD_CServ.Items(Indice).Value <> "" Then
                        If Condizione <> "" Then
                            Condizione = Condizione & " OR "
                        End If
                        Condizione = Condizione & " CentroServizio =  '" & DD_CServ.Items(Indice).Value & "'"
                    End If
                Next
                Condizione = " And (" & Condizione & ") "
            End If
        Else
            If DD_CServ.SelectedValue <> "" Then
                Condizione = " And CentroServizio =  '" & DD_CServ.SelectedValue & "'"
            End If
        End If

        If Chk_NonXML.Checked = True Then
            Condizione = Condizione & " And DataRid IS Null And (IdRid = '' or IdRid is null) "
        End If

        If IsDate(Txt_DataDal.Text) And IsDate(Txt_DataAl.Text) Then
            MySql = "SELECT * " & _
                    " FROM MovimentiContabiliTesta " & _
                    " WHERE MovimentiContabiliTesta.DataRegistrazione >= ? And " & _
                    " MovimentiContabiliTesta.DataRegistrazione <= ? And NumeroProtocollo > 0"
        Else
            MySql = "SELECT * " & _
                    " FROM MovimentiContabiliTesta " & _
                    " WHERE MovimentiContabiliTesta.AnnoCompetenza = " & Val(Txt_Anno.Text) & " " & _
                    " AND MovimentiContabiliTesta.MeseCompetenza = " & Val(MeseContr) & " " & _
                    " AND AnnoProtocollo = " & Val(Txt_AnnoRif.Text)
            MySql = MySql & " AND (NumeroProtocollo >= " & Val(Txt_DalDocumento.Text) & " And NumeroProtocollo <= " & Val(Txt_AlDocumento.Text) & ") "
        End If

        If DD_Registro.SelectedValue <> "0" Then
            MySql = MySql & " AND MovimentiContabiliTesta.RegistroIva = " & DD_Registro.SelectedValue
        End If

        MySql = MySql & Condizione


        If Chk_NonIncassato.Checked = True Then
            MySql = MySql & " And (Select count(tabellalegami.Importo) from tabellalegami where CodiceDocumento = MovimentiContabiliTesta.NumeroRegistrazione) =0 "
        End If


        cn = New Data.OleDb.OleDbConnection(Session("DC_GENERALE"))

        cn.Open()
        Dim cmd As New OleDbCommand()

        MySql = MySql & " ORDER BY (SELECT TOP 1  SottocontoPartita FROM [MovimentiContabiliRiga] WHERE Numero = NumeroRegistrazione AND RigaDaCausale =1 )"
        cmd.CommandText = MySql  '& " Order by NumeroProtocollo"
        cmd.Connection = cn

        If IsDate(Txt_DataDal.Text) And IsDate(Txt_DataAl.Text) Then
            cmd.Parameters.AddWithValue("@DataRegistrazione", Txt_DataDal.Text)
            cmd.Parameters.AddWithValue("@DataRegistrazione", Txt_DataAl.Text)
        End If

        Dim CodiceInstallazione As Integer
        Dim CodiceDitta As Integer
        Dim ProgressivoTesta As Integer
        Dim RegistroIva As Integer
        Dim APPOGGIO As String
        Dim AppoggioTotale As Double = 0
        Dim AppoggioNumero As Integer = 0
        Dim DataScadenza As Date = Txt_DataScadenza.Text

        CodiceInstallazione = 1
        CodiceDitta = 1

        RegistroIva = DD_Registro.SelectedValue

        AppoggioNumero = 0
        AppoggioTotale = 0

        ProgressivoTesta = 0

        Dim myPOSTreader As OleDbDataReader = cmd.ExecuteReader()
        Do While myPOSTreader.Read
            ProgressivoTesta = ProgressivoTesta + 1


            APPOGGIO = ""
            Dim CausaleContabile As New Cls_CausaleContabile

            CausaleContabile.Codice = campodb(myPOSTreader.Item("CAUSALECONTABILE"))
            CausaleContabile.Leggi(Session("DC_TABELLE"), CausaleContabile.Codice)





            Dim Cliente As String = ""

            Dim Tipo As String = campodb(myPOSTreader.Item("Tipologia"))
            Dim CodiceOspite As Integer = 0
            Dim CodiceParente As Integer = 0

            Dim CodiceProvincia As String = ""
            Dim CodiceComune As String = ""
            Dim CodiceRegione As String = ""
            Dim ImporotTotale As Double = 0


            Dim cmdRd As New OleDbCommand()
            cmdRd.CommandText = "Select * From MovimentiContabiliRiga Where Numero = ?  And Tipo ='CF'   "
            cmdRd.Connection = cn
            cmdRd.Parameters.AddWithValue("@Numero", campodbN(myPOSTreader.Item("NumeroRegistrazione")))
            Dim MyReadSC As OleDbDataReader = cmdRd.ExecuteReader()
            If MyReadSC.Read Then
                Cliente = campodb(MyReadSC.Item("MastroPartita")) & "." & campodb(MyReadSC.Item("ContoPartita")) & "." & campodb(MyReadSC.Item("SottocontoPartita"))
                If Mid(Tipo & Space(10), 1, 1) = " " Then
                    CodiceOspite = Int(campodbN(MyReadSC.Item("SottocontoPartita")) / 100)
                    CodiceParente = campodbN(MyReadSC.Item("SottocontoPartita")) - (CodiceOspite * 100)

                    If CodiceParente = 0 And CodiceOspite > 0 Then
                        Tipo = "O" & Space(10)
                    End If
                    If CodiceParente > 0 And CodiceOspite > 0 Then
                        Tipo = "P" & Space(10)
                    End If
                End If
                If Mid(Tipo & Space(10), 1, 1) = "C" Then
                    CodiceProvincia = Mid(Tipo & Space(10), 2, 3)
                    CodiceComune = Mid(Tipo & Space(10), 5, 3)
                End If
                If Mid(Tipo & Space(10), 1, 1) = "J" Then
                    CodiceProvincia = Mid(Tipo & Space(10), 2, 3)
                    CodiceComune = Mid(Tipo & Space(10), 5, 3)
                End If
                If Mid(Tipo & Space(10), 1, 1) = "R" Then
                    CodiceRegione = Mid(Tipo & Space(10), 2, 4)
                End If


                If campodb(MyReadSC.Item("DareAvere")) = "A" Then
                    ImporotTotale = campodbN(MyReadSC.Item("Importo")) * -1
                Else
                    ImporotTotale = campodbN(MyReadSC.Item("Importo"))
                End If
            End If
            MyReadSC.Close()


            Dim DO11_RAGIONESOCIALE As String = ""
            Dim DO11_PIVA As String = ""
            Dim DO11_CF As String = ""
            Dim DO11_INDIRIZZO As String = ""
            Dim DO11_CAP As String = ""
            Dim DO11_CITTA_NOME As String = ""
            Dim DO11_CITTA_ISTAT As String = ""
            Dim DO11_PROVINCIA As String = ""
            Dim DO11_CONDPAG_CODICE_SEN As String = ""
            Dim DO11_CONDPAG_NOME_SEN As String = ""
            Dim DO11_BANCA_CIN As String = ""
            Dim DO11_BANCA_ABI As String = ""
            Dim DO11_BANCA_CAB As String = ""
            Dim DO11_BANCA_CONTO As String = ""
            Dim DO11_BANCA_IBAN As String = ""
            Dim DO11_BANCA_IDMANDATO As String = ""
            Dim DO11_BANCA_DATAMANDATO As String = ""
            Dim DO11_FIRST As String = ""
            Dim DO11_CODICEOSPITE As Integer = 0
            Dim DO11_CODICEPARENTE As Integer = 0
            Dim DO11_TIPOFLUSSO As String = ""
            Dim DO11_INTESTATARIO As String = ""
            Dim DO11_BANCA As String = ""


            Dim AddDescrizione As String

            AddDescrizione = ""
            If Mid(Tipo & Space(10), 1, 1) = "O" Then
                Dim Ospite As New ClsOspite

                DO11_CODICEOSPITE = CodiceOspite
                Ospite.CodiceOspite = CodiceOspite
                Ospite.Leggi(Session("DC_OSPITE"), Ospite.CodiceOspite)
                If Ospite.Nome <> "" Then

                    DO11_RAGIONESOCIALE = Ospite.Nome
                    DO11_PIVA = ""
                    DO11_CF = Ospite.CODICEFISCALE
                    DO11_INDIRIZZO = Ospite.RESIDENZAINDIRIZZO1
                    DO11_CAP = Ospite.RESIDENZACAP1
                    Dim DcCom As New ClsComune

                    DcCom.Provincia = Ospite.RESIDENZAPROVINCIA1
                    DcCom.Comune = Ospite.RESIDENZACOMUNE1
                    DcCom.Leggi(Session("DC_OSPITE"))

                    DO11_CITTA_NOME = DcCom.Descrizione
                    DO11_CITTA_ISTAT = DcCom.Provincia & DcCom.Comune

                    Dim DcProv As New ClsComune

                    DcProv.Provincia = Ospite.RESIDENZAPROVINCIA1
                    DcProv.Comune = ""
                    DcProv.Leggi(Session("DC_OSPITE"))
                    DO11_PROVINCIA = DcProv.CodificaProvincia


                    Dim Kl As New Cls_DatiOspiteParenteCentroServizio

                    Kl.CodiceOspite = CodiceOspite
                    Kl.CodiceParente = 0
                    Kl.CentroServizio = campodb(myPOSTreader.Item("CentroServizio"))
                    Kl.Leggi(Session("DC_OSPITE"))
                    If Kl.ModalitaPagamento <> "" Then
                        Ospite.MODALITAPAGAMENTO = Kl.ModalitaPagamento
                    End If


                    DO11_CONDPAG_CODICE_SEN = Ospite.MODALITAPAGAMENTO
                    Dim MPAg As New ClsModalitaPagamento

                    MPAg.Codice = Ospite.MODALITAPAGAMENTO
                    MPAg.Leggi(Session("DC_OSPITE"))
                    DO11_CONDPAG_NOME_SEN = MPAg.DESCRIZIONE

                    DO11_TIPOFLUSSO = MPAg.Tipo

                    Dim ModPag As New Cls_DatiPagamento

                    ModPag.CodiceOspite = Ospite.CodiceOspite
                    ModPag.CodiceParente = 0
                    If Year(campodbD(myPOSTreader.Item("DataDocumento"))) > 1900 Then
                        ModPag.LeggiUltimaDataData(Session("DC_OSPITE"), campodbD(myPOSTreader.Item("DataDocumento")))
                    Else
                        ModPag.LeggiUltimaDataData(Session("DC_OSPITE"), campodbD(myPOSTreader.Item("DataRegistrazione")))
                    End If


                    DO11_BANCA_CIN = ModPag.Cin
                    DO11_BANCA_ABI = ModPag.Abi
                    DO11_BANCA_CAB = ModPag.Cin
                    DO11_BANCA_CONTO = ModPag.CCBancario
                    DO11_BANCA_IBAN = ModPag.CodiceInt & Format(Val(ModPag.NumeroControllo), "00") & ModPag.Cin & Format(Val(ModPag.Abi), "00000") & Format(Val(ModPag.Cab), "00000") & AdattaLunghezzaNumero(ModPag.CCBancario, 12)

                    DO11_BANCA_IDMANDATO = ModPag.IdMandatoDebitore
                    DO11_BANCA_DATAMANDATO = ModPag.DataMandatoDebitore

                    DO11_BANCA = ModPag.CodiceBanca

                    If Trim(ModPag.IntestatarioConto) <> "" Then
                        DO11_INTESTATARIO = ModPag.IntestatarioConto.Trim
                        DO11_INDIRIZZO = ""
                        DO11_CAP = ""
                        DO11_CITTA_NOME = ""
                        DO11_PROVINCIA = ""
                    End If

                    If ModPag.First = 1 Then
                        DO11_FIRST = "SI"
                    End If
                End If
            End If

            If Mid(Tipo & Space(10), 1, 1) = "P" Then
                Dim Ospite As New Cls_Parenti


                DO11_CODICEOSPITE = CodiceOspite
                DO11_CODICEPARENTE = CodiceParente

                Ospite.CodiceOspite = CodiceOspite
                Ospite.CodiceParente = CodiceParente
                Ospite.Leggi(Session("DC_OSPITE"), Ospite.CodiceOspite, Ospite.CodiceParente)
                DO11_RAGIONESOCIALE = Ospite.Nome
                DO11_PIVA = ""
                DO11_CF = Ospite.CODICEFISCALE
                DO11_INDIRIZZO = Ospite.RESIDENZAINDIRIZZO1
                DO11_CAP = Ospite.RESIDENZACAP1
                Dim DcCom As New ClsComune

                DcCom.Provincia = Ospite.RESIDENZAPROVINCIA1
                DcCom.Comune = Ospite.RESIDENZACOMUNE1
                DcCom.Leggi(Session("DC_OSPITE"))

                DO11_CITTA_NOME = DcCom.Descrizione
                DO11_CITTA_ISTAT = DcCom.Provincia & DcCom.Comune

                Dim DcProv As New ClsComune

                DcProv.Provincia = Ospite.RESIDENZAPROVINCIA1
                DcProv.Comune = ""
                DcProv.Leggi(Session("DC_OSPITE"))
                DO11_PROVINCIA = DcProv.CodificaProvincia
                DO11_CONDPAG_CODICE_SEN = Ospite.MODALITAPAGAMENTO

                Dim Kl As New Cls_DatiOspiteParenteCentroServizio

                Kl.CodiceOspite = CodiceOspite
                Kl.CodiceParente = CodiceParente
                Kl.CentroServizio = campodb(myPOSTreader.Item("CentroServizio"))
                Kl.Leggi(Session("DC_OSPITE"))
                If Kl.ModalitaPagamento <> "" Then
                    Ospite.MODALITAPAGAMENTO = Kl.ModalitaPagamento
                End If

                Dim MPAg As New ClsModalitaPagamento

                MPAg.Codice = Ospite.MODALITAPAGAMENTO
                MPAg.Leggi(Session("DC_OSPITE"))
                DO11_CONDPAG_NOME_SEN = MPAg.DESCRIZIONE
                DO11_TIPOFLUSSO = MPAg.Tipo

                Dim ModPag As New Cls_DatiPagamento

                ModPag.CodiceOspite = Ospite.CodiceOspite
                ModPag.CodiceParente = Ospite.CodiceParente
                ModPag.LeggiUltimaDataData(Session("DC_OSPITE"), campodb(myPOSTreader.Item("DataDocumento")))

                DO11_BANCA_CIN = ModPag.Cin
                DO11_BANCA_ABI = ModPag.Abi
                DO11_BANCA_CAB = ModPag.Cin
                DO11_BANCA_CONTO = ModPag.CCBancario
                DO11_BANCA_IBAN = ModPag.CodiceInt & Format(Val(ModPag.NumeroControllo), "00") & ModPag.Cin & Format(Val(ModPag.Abi), "00000") & Format(Val(ModPag.Cab), "00000") & AdattaLunghezzaNumero(ModPag.CCBancario, 12)

                DO11_BANCA = ModPag.CodiceBanca

                DO11_BANCA_IDMANDATO = ModPag.IdMandatoDebitore
                DO11_BANCA_DATAMANDATO = ModPag.DataMandatoDebitore
                If ModPag.First = 1 Then
                    DO11_FIRST = "SI"
                End If

                If Trim(ModPag.IntestatarioConto) <> "" Then
                    DO11_INTESTATARIO = ModPag.IntestatarioConto.Trim
                    DO11_INDIRIZZO = ""
                    DO11_CAP = ""
                    DO11_CITTA_NOME = ""
                    DO11_PROVINCIA = ""
                End If
            End If

            If campodbN(myPOSTreader.Item("NumeroRegistrazione")) = 564 Then
                DO11_CONDPAG_CODICE_SEN = campodb(myPOSTreader.Item("CodicePagamento"))
            End If
            If campodb(myPOSTreader.Item("CodicePagamento")) <> "" Then
                DO11_CONDPAG_CODICE_SEN = campodb(myPOSTreader.Item("CodicePagamento"))
            End If

            If DO11_CONDPAG_CODICE_SEN = "" Then
                DO11_CONDPAG_CODICE_SEN = "XX"
            End If
            Dim M As New Cls_TipoPagamento
            M.Descrizione = ""
            M.Codice = DO11_CONDPAG_CODICE_SEN
            M.Leggi(Session("DC_TABELLE"))
            If M.Descrizione <> "" Then
                DO11_CONDPAG_CODICE_SEN = M.Codice
                DO11_CONDPAG_NOME_SEN = M.Descrizione
            End If

            Dim MPAgP As New ClsModalitaPagamento

            MPAgP.Codice = DO11_CONDPAG_CODICE_SEN
            MPAgP.Leggi(Session("DC_OSPITE"))
            DO11_TIPOFLUSSO = MPAgP.Tipo


            If DD_Banca.SelectedValue = "" Or (DO11_BANCA = DD_Banca.SelectedValue) Then
                If M.Descrizione.IndexOf("R.I.D.") > 0 Or M.Descrizione.IndexOf("SEPA") > 0 Or DO11_TIPOFLUSSO = "R" Then

                    Dim AppoggioData As Date

                    AppoggioTotale = AppoggioTotale + ImporotTotale
                    AppoggioNumero = AppoggioNumero + 1


                    Try
                        AppoggioData = DO11_BANCA_DATAMANDATO
                    Catch ex As Exception

                    End Try

                    Dim myriga As System.Data.DataRow = Tabella.NewRow()
                    myriga(0) = DO11_RAGIONESOCIALE
                    myriga(1) = DO11_BANCA_IBAN
                    myriga(2) = DO11_CF
                    myriga(3) = Format(AppoggioData, "yyyy-MM-dd")
                    myriga(4) = DO11_BANCA_IDMANDATO
                    myriga(5) = Format(ImporotTotale, "0.00").Replace(",", ".")
                    myriga(6) = campodbN(myPOSTreader.Item("NumeroRegistrazione"))
                    myriga(7) = DO11_INDIRIZZO
                    myriga(8) = DO11_CAP & " " & DO11_CITTA_NOME & " " & DO11_PROVINCIA
                    myriga(9) = DO11_FIRST
                    myriga(10) = DO11_CODICEOSPITE
                    myriga(11) = DO11_CODICEPARENTE
                    myriga(12) = DO11_INTESTATARIO

                    Tabella.Rows.Add(myriga)
                End If
            End If
        Loop
        myPOSTreader.Close()
        cn.Close()

        GridView1.AutoGenerateColumns = False



        Session("TabellaRid") = Tabella
        GridView1.DataSource = Tabella
        GridView1.DataBind()
        GridView1.Visible = True
        Lbl_Totali.Text = "Numero Rid estratti " & AppoggioNumero & " per un importo di " & Format(AppoggioTotale, "#,##0.00")
        Lbl_Totali.Visible = True
        Btn_Seleziona.Visible = True
        Btn_SelezionaFirst.Visible = True

        Dim UltimoCF As String = ""
        Dim Totale As Double = 0

        For i = 0 To GridView1.Rows.Count - 1

            If Tabella.Rows(i).Item(2) <> UltimoCF And UltimoCF <> "" Then
                If Totale < 0 Then
                    Dim k As Integer

                    For k = i - 1 To 0 Step -1
                        If Tabella.Rows(k).Item(2) = UltimoCF Then
                            Dim CheckBox As CheckBox = DirectCast(GridView1.Rows(k).FindControl("ChkImporta"), CheckBox)
                            GridView1.Rows(k).BackColor = Drawing.Color.Red
                            CheckBox.Enabled = False
                        Else
                            Exit For
                        End If
                    Next
                End If
                Totale = 0
            End If

            Totale = Totale + CDbl(Tabella.Rows(i).Item(5).ToString.Replace(".", ","))
            UltimoCF = Tabella.Rows(i).Item(2)
        Next

        ScriptManager.RegisterClientScriptBlock(Me, Me.GetType(), "Stampa2", "VisualizzaButtoni();", True)
        'Btn_Piu

    End Sub



    Protected Sub Btn_Esci_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles Btn_Esci.Click
        Response.Redirect("Menu_Export.aspx")
    End Sub

    Protected Sub Btn_Seleziona_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles Btn_Seleziona.Click
        Dim Param As New Cls_Parametri

        Param.LeggiParametri(Context.Session("DC_OSPITE"))

        For Riga = 0 To GridView1.Rows.Count - 1

            Dim CheckBox As CheckBox = DirectCast(GridView1.Rows(Riga).FindControl("ChkImporta"), CheckBox)

            If CheckBox.Enabled = True Then
                If GridView1.Rows(Riga).Cells(3).Text <> "000000000000000000000000" And GridView1.Rows(Riga).Cells(4).Text <> "SI" Then
                    CheckBox.Checked = True
                    CheckBox.Checked = True
                End If
            End If
        Next

        ScriptManager.RegisterClientScriptBlock(Me, Me.GetType(), "Stampa2", "VisualizzaButtoni();", True)
    End Sub

    Protected Sub Btn_Salva_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles Btn_Salva.Click
        Crea_XML()
    End Sub



    Protected Sub GridView1_RowDeleted(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewDeletedEventArgs) Handles GridView1.RowDeleted

    End Sub

    Protected Sub GridView1_RowDeleting(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewDeleteEventArgs) Handles GridView1.RowDeleting

    End Sub



    Protected Sub Btn_SelezionaFirst_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles Btn_SelezionaFirst.Click
        Dim Param As New Cls_Parametri

        Param.LeggiParametri(Context.Session("DC_OSPITE"))

        For Riga = 0 To GridView1.Rows.Count - 1

            Dim CheckBox As CheckBox = DirectCast(GridView1.Rows(Riga).FindControl("ChkImporta"), CheckBox)

            If CheckBox.Enabled = True Then
                If GridView1.Rows(Riga).Cells(3).Text <> "000000000000000000000000" And GridView1.Rows(Riga).Cells(4).Text = "SI" Then
                    CheckBox.Checked = True
                    CheckBox.Checked = True
                End If
            End If
        Next

        ScriptManager.RegisterClientScriptBlock(Me, Me.GetType(), "Stampa2", "VisualizzaButtoni();", True)
    End Sub



    Private Sub AggiornaCServ()
        Dim kCsrv As New Cls_CentroServizio

        If DD_Struttura.SelectedValue = "" Then
            kCsrv.UpDateDropBox(Session("DC_OSPITE"), DD_CServ)
        Else
            kCsrv.UpDateDropBoxStruttura(Session("DC_OSPITE"), DD_CServ, DD_Struttura.SelectedValue)
        End If
    End Sub



    Protected Sub DD_Struttura_TextChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles DD_Struttura.TextChanged
        AggiornaCServ()
        Call EseguiJS()
    End Sub

    Protected Sub Timer1_Tick(ByVal sender As Object, ByVal e As System.EventArgs) Handles Timer1.Tick

    End Sub


    Protected Sub BtnAggiungi_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles BtnAggiungi.Click
        Dim DO11_RAGIONESOCIALE As String = ""
        Dim DO11_PIVA As String = ""
        Dim DO11_CF As String = ""
        Dim DO11_INDIRIZZO As String = ""
        Dim DO11_CAP As String = ""
        Dim DO11_CITTA_NOME As String = ""
        Dim DO11_CITTA_ISTAT As String = ""
        Dim DO11_PROVINCIA As String = ""
        Dim DO11_CONDPAG_CODICE_SEN As String = ""
        Dim DO11_CONDPAG_NOME_SEN As String = ""
        Dim DO11_BANCA_CIN As String = ""
        Dim DO11_BANCA_ABI As String = ""
        Dim DO11_BANCA_CAB As String = ""
        Dim DO11_BANCA_CONTO As String = ""
        Dim DO11_BANCA_IBAN As String = ""
        Dim DO11_BANCA_IDMANDATO As String = ""
        Dim DO11_BANCA_DATAMANDATO As String = ""
        Dim DO11_FIRST As String = ""
        Dim DO11_CODICEOSPITE As Integer = 0
        Dim DO11_CODICEPARENTE As Integer = 0
        Dim DO11_TIPOFLUSSO As String = ""
        Dim DO11_INTESTATARIO As String = ""
        Dim DO11_BANCA As String = ""
        Dim AddDescrizione As String
        Dim CentroServizio As String
        Dim CodiceOspite As String
        Dim CodiceParente As String
        Dim Tipo As String

        Tipo = "O"
        Try
            CentroServizio = Mid(Txt_NomeOspiti.Text, 1, Txt_NomeOspiti.Text.IndexOf(" "))
            CodiceOspite = Val(Mid(Txt_NomeOspiti.Text, Txt_NomeOspiti.Text.IndexOf(" ") + 1, Txt_NomeOspiti.Text.IndexOf(" ", Txt_NomeOspiti.Text.IndexOf(" ") + 1)))
        Catch ex As Exception
            Dim k As New ClsOspite

            k.Nome = Txt_NomeOspiti.Text
            k.LeggiNome(Session("DC_OSPITE"))

            Dim Mov As New Cls_Movimenti

            Mov.UltimaData(Session("DC_OSPITE"), k.CodiceOspite)
            CodiceOspite = k.CodiceOspite
            CentroServizio = Mov.CENTROSERVIZIO
        End Try


        If Txt_NomeOspiti.Text.IndexOf("Parente di") > 0 Then
            Dim Nome As String

            Nome = Mid(Txt_NomeOspiti.Text, Txt_NomeOspiti.Text.IndexOf(" ", Txt_NomeOspiti.Text.IndexOf(" ") + 1) + 1, Txt_NomeOspiti.Text.IndexOf("Parente di") - (Txt_NomeOspiti.Text.IndexOf(" ", Txt_NomeOspiti.Text.IndexOf(" ") + 1)))

            Dim cn As OleDbConnection



            cn = New Data.OleDb.OleDbConnection(Session("DC_OSPITE"))

            cn.Open()
            Dim cmd As New OleDbCommand()
            cmd.CommandText = ("select * from AnagraficaComune where CodiceOspite = ? And " & _
                               "Nome= ? And TIPOLOGIA = 'P'")
            cmd.Parameters.AddWithValue("@CodiceOspite", CodiceOspite)
            cmd.Parameters.AddWithValue("@Nome", Trim(Nome))
            cmd.Connection = cn


            Dim myPOSTreader As OleDbDataReader = cmd.ExecuteReader()
            If myPOSTreader.Read Then

                CodiceParente = Val(campodb(myPOSTreader.Item("CodiceParente")))
                Tipo = "P"
            End If
            myPOSTreader.Close()
            cn.Close()
        End If

        AddDescrizione = ""
        If Mid(Tipo & Space(10), 1, 1) = "O" Then
            Dim Ospite As New ClsOspite

            DO11_CODICEOSPITE = CodiceOspite
            Ospite.CodiceOspite = CodiceOspite
            Ospite.Leggi(Session("DC_OSPITE"), Ospite.CodiceOspite)
            If Ospite.Nome <> "" Then

                DO11_RAGIONESOCIALE = Ospite.Nome
                DO11_PIVA = ""
                DO11_CF = Ospite.CODICEFISCALE
                DO11_INDIRIZZO = Ospite.RESIDENZAINDIRIZZO1
                DO11_CAP = Ospite.RESIDENZACAP1
                Dim DcCom As New ClsComune

                DcCom.Provincia = Ospite.RESIDENZAPROVINCIA1
                DcCom.Comune = Ospite.RESIDENZACOMUNE1
                DcCom.Leggi(Session("DC_OSPITE"))

                DO11_CITTA_NOME = DcCom.Descrizione
                DO11_CITTA_ISTAT = DcCom.Provincia & DcCom.Comune

                Dim DcProv As New ClsComune

                DcProv.Provincia = Ospite.RESIDENZAPROVINCIA1
                DcProv.Comune = ""
                DcProv.Leggi(Session("DC_OSPITE"))
                DO11_PROVINCIA = DcProv.CodificaProvincia


                Dim Kl As New Cls_DatiOspiteParenteCentroServizio

                Kl.CodiceOspite = CodiceOspite
                Kl.CodiceParente = 0
                Kl.CentroServizio = CentroServizio
                Kl.Leggi(Session("DC_OSPITE"))
                If Kl.ModalitaPagamento <> "" Then
                    Ospite.MODALITAPAGAMENTO = Kl.ModalitaPagamento
                End If


                DO11_CONDPAG_CODICE_SEN = Ospite.MODALITAPAGAMENTO
                Dim MPAg As New ClsModalitaPagamento

                MPAg.Codice = Ospite.MODALITAPAGAMENTO
                MPAg.Leggi(Session("DC_OSPITE"))
                DO11_CONDPAG_NOME_SEN = MPAg.DESCRIZIONE

                DO11_TIPOFLUSSO = MPAg.Tipo

                Dim ModPag As New Cls_DatiPagamento

                ModPag.CodiceOspite = Ospite.CodiceOspite
                ModPag.CodiceParente = 0

                ModPag.LeggiUltimaDataData(Session("DC_OSPITE"), Now)


                DO11_BANCA_CIN = ModPag.Cin
                DO11_BANCA_ABI = ModPag.Abi
                DO11_BANCA_CAB = ModPag.Cin
                DO11_BANCA_CONTO = ModPag.CCBancario
                DO11_BANCA_IBAN = ModPag.CodiceInt & Format(Val(ModPag.NumeroControllo), "00") & ModPag.Cin & Format(Val(ModPag.Abi), "00000") & Format(Val(ModPag.Cab), "00000") & AdattaLunghezzaNumero(ModPag.CCBancario, 12)

                DO11_BANCA_IDMANDATO = ModPag.IdMandatoDebitore
                DO11_BANCA_DATAMANDATO = ModPag.DataMandatoDebitore

                DO11_BANCA = ModPag.CodiceBanca

                If Trim(ModPag.IntestatarioConto) <> "" Then
                    DO11_INTESTATARIO = ModPag.IntestatarioConto.Trim
                    DO11_INDIRIZZO = ""
                    DO11_CAP = ""
                    DO11_CITTA_NOME = ""
                    DO11_PROVINCIA = ""
                End If

                If ModPag.First = 1 Then
                    DO11_FIRST = "SI"
                End If
            End If
        End If

        If Mid(Tipo & Space(10), 1, 1) = "P" Then
            Dim Ospite As New Cls_Parenti


            DO11_CODICEOSPITE = CodiceOspite
            DO11_CODICEPARENTE = CodiceParente

            Ospite.CodiceOspite = CodiceOspite
            Ospite.CodiceParente = CodiceParente
            Ospite.Leggi(Session("DC_OSPITE"), Ospite.CodiceOspite, Ospite.CodiceParente)
            DO11_RAGIONESOCIALE = Ospite.Nome
            DO11_PIVA = ""
            DO11_CF = Ospite.CODICEFISCALE
            DO11_INDIRIZZO = Ospite.RESIDENZAINDIRIZZO1
            DO11_CAP = Ospite.RESIDENZACAP1
            Dim DcCom As New ClsComune

            DcCom.Provincia = Ospite.RESIDENZAPROVINCIA1
            DcCom.Comune = Ospite.RESIDENZACOMUNE1
            DcCom.Leggi(Session("DC_OSPITE"))

            DO11_CITTA_NOME = DcCom.Descrizione
            DO11_CITTA_ISTAT = DcCom.Provincia & DcCom.Comune

            Dim DcProv As New ClsComune

            DcProv.Provincia = Ospite.RESIDENZAPROVINCIA1
            DcProv.Comune = ""
            DcProv.Leggi(Session("DC_OSPITE"))
            DO11_PROVINCIA = DcProv.CodificaProvincia
            DO11_CONDPAG_CODICE_SEN = Ospite.MODALITAPAGAMENTO

            Dim Kl As New Cls_DatiOspiteParenteCentroServizio

            Kl.CodiceOspite = CodiceOspite
            Kl.CodiceParente = CodiceParente
            Kl.CentroServizio = CentroServizio
            Kl.Leggi(Session("DC_OSPITE"))
            If Kl.ModalitaPagamento <> "" Then
                Ospite.MODALITAPAGAMENTO = Kl.ModalitaPagamento
            End If

            Dim MPAg As New ClsModalitaPagamento

            MPAg.Codice = Ospite.MODALITAPAGAMENTO
            MPAg.Leggi(Session("DC_OSPITE"))
            DO11_CONDPAG_NOME_SEN = MPAg.DESCRIZIONE
            DO11_TIPOFLUSSO = MPAg.Tipo

            Dim ModPag As New Cls_DatiPagamento

            ModPag.CodiceOspite = Ospite.CodiceOspite
            ModPag.CodiceParente = Ospite.CodiceParente
            ModPag.LeggiUltimaDataData(Session("DC_OSPITE"), Now)

            DO11_BANCA_CIN = ModPag.Cin
            DO11_BANCA_ABI = ModPag.Abi
            DO11_BANCA_CAB = ModPag.Cin
            DO11_BANCA_CONTO = ModPag.CCBancario
            DO11_BANCA_IBAN = ModPag.CodiceInt & Format(Val(ModPag.NumeroControllo), "00") & ModPag.Cin & Format(Val(ModPag.Abi), "00000") & Format(Val(ModPag.Cab), "00000") & AdattaLunghezzaNumero(ModPag.CCBancario, 12)

            DO11_BANCA = ModPag.CodiceBanca

            DO11_BANCA_IDMANDATO = ModPag.IdMandatoDebitore
            DO11_BANCA_DATAMANDATO = ModPag.DataMandatoDebitore
            If ModPag.First = 1 Then
                DO11_FIRST = "SI"
            End If

            If Trim(ModPag.IntestatarioConto) <> "" Then
                DO11_INTESTATARIO = ModPag.IntestatarioConto.Trim
                DO11_INDIRIZZO = ""
                DO11_CAP = ""
                DO11_CITTA_NOME = ""
                DO11_PROVINCIA = ""
            End If
        End If

        Tabella = Session("TabellaRid")
        Dim myriga As System.Data.DataRow = Tabella.NewRow()
        myriga(0) = DO11_RAGIONESOCIALE
        myriga(1) = DO11_BANCA_IBAN
        myriga(2) = DO11_CF
        myriga(3) = Format(Now, "yyyy-MM-dd")
        myriga(4) = DO11_BANCA_IDMANDATO
        myriga(5) = Format(CDbl(Txt_Importo.Text), "0.00").Replace(",", ".")
        myriga(6) = Txt_MsgID.Text & "0" & Tabella.Rows.Count
        myriga(7) = DO11_INDIRIZZO
        myriga(8) = DO11_CAP & " " & DO11_CITTA_NOME & " " & DO11_PROVINCIA
        myriga(9) = DO11_FIRST
        myriga(10) = DO11_CODICEOSPITE
        myriga(11) = DO11_CODICEPARENTE
        myriga(12) = DO11_INTESTATARIO
        myriga(14) = 0
        Tabella.Rows.Add(myriga)
        Dim AppoggioNumero As Integer = 0
        Dim AppoggioTotale As Double = 0
        Dim Indice As Integer

        For Indice = 0 To Tabella.Rows.Count - 1
            AppoggioNumero = AppoggioNumero + 1
            AppoggioTotale = AppoggioTotale + CDbl(Tabella.Rows(Indice).Item(5).ToString.Replace(".", ","))
        Next
        GridView1.AutoGenerateColumns = False



        Session("TabellaRid") = Tabella
        GridView1.DataSource = Tabella
        GridView1.DataBind()
        GridView1.Visible = True
        Lbl_Totali.Text = "Numero Rid estratti " & AppoggioNumero & " per un importo di " & Format(AppoggioTotale, "#,##0.00")
        Lbl_Totali.Visible = True


        Txt_NomeOspiti.Text = ""
        Txt_DataRid.Text = ""
        Txt_Importo.Text = "0,00"

        ScriptManager.RegisterClientScriptBlock(Me, Me.GetType(), "Stampa2", "VisualizzaButtoni();", True)
    End Sub

    Protected Sub Btn_Dowload_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles Btn_Dowload.Click

    End Sub
End Class
