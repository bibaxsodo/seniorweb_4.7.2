﻿Imports Microsoft.VisualBasic
Imports System.Data.OleDb
Imports System.Web.Hosting
Imports System.Diagnostics
Imports System.ComponentModel
Imports System.Threading
Imports System.IO.Compression
Imports System.IO

Partial Class OspitiWeb_Export_Documenti
    Inherits System.Web.UI.Page
    Dim MyTable As New System.Data.DataTable("tabella")
    Dim MyDataSet As New System.Data.DataSet()


    Function quanteVolte(ByVal str1 As String, ByVal str2 As String) As Long
        Dim strArray(100) As String
        strArray = Split(str1, str2)
        quanteVolte = UBound(strArray)
    End Function


    Public Function GiorniMese(ByVal Mese As Byte, ByVal Anno As Integer) As Byte
        If Mese = 1 Or Mese = 3 Or Mese = 5 Or Mese = 7 Or Mese = 8 Or _
           Mese = 10 Or Mese = 12 Then
            GiorniMese = 31
        Else
            If Mese <> 2 Then
                GiorniMese = 30
            Else
                If Day(DateSerial(Anno, Mese, 29)) = 29 Then
                    GiorniMese = 29
                Else
                    GiorniMese = 28
                End If
            End If
        End If
    End Function

    Public Function DecodificaMese(ByVal Mese As Long) As String
        Select Case Mese
            Case 1
                DecodificaMese = "Gennaio"
            Case 2
                DecodificaMese = "Febbraio"
            Case 3
                DecodificaMese = "Marzo"
            Case 4
                DecodificaMese = "Aprile"
            Case 5
                DecodificaMese = "Maggio"
            Case 6
                DecodificaMese = "Giugno"
            Case 7
                DecodificaMese = "Luglio"
            Case 8
                DecodificaMese = "Agosto"
            Case 9
                DecodificaMese = "Settembre"
            Case 10
                DecodificaMese = "Ottobre"
            Case 11
                DecodificaMese = "Novembre"
            Case 12
                DecodificaMese = "Dicembre"
        End Select
    End Function


    Function MinimoDocumento() As Long
        Dim cn As OleDbConnection

        Dim MeseContr As Long
        Dim MySql As String

        Dim TabellaSocieta As New Cls_TabellaSocieta


        TabellaSocieta.Leggi(Session("DC_TABELLE"))

        MeseContr = DD_Mese.SelectedValue

        MinimoDocumento = 0

        
        If Tab_PeriodoData.ActiveTabIndex = 1 And IsDate(Txt_DataAl1.Text) And IsDate(Txt_DataDal1.Text) Then
            MySql = "SELECT MIN(NumeroProtocollo) " & _
                    " FROM MovimentiContabiliRiga INNER JOIN MovimentiContabiliTesta " & _
                    " ON MovimentiContabiliRiga.Numero = MovimentiContabiliTesta.NumeroRegistrazione " & _
                    " WHERE MovimentiContabiliTesta.DataRegistrazione >=  ? " & _
                    " AND MovimentiContabiliTesta.DataRegistrazione <= ? "
        Else
            MySql = "SELECT MIN(NumeroProtocollo) " & _
                    " FROM MovimentiContabiliRiga INNER JOIN MovimentiContabiliTesta " & _
                    " ON MovimentiContabiliRiga.Numero = MovimentiContabiliTesta.NumeroRegistrazione " & _
                    " WHERE MovimentiContabiliTesta.AnnoCompetenza = " & Txt_Anno.Text & " " & _
                    " AND MovimentiContabiliTesta.MeseCompetenza = " & MeseContr & " " & _
                    " AND AnnoProtocollo = " & Txt_AnnoRif.Text

        End If

        If DD_Registro.SelectedValue <> "" Then
            MySql = MySql & " AND MovimentiContabiliTesta.RegistroIva = " & DD_Registro.SelectedValue
        End If


        If TabellaSocieta.NomeFile.Trim = "MDAZEGLIO" Then
            MySql = MySql & " AND MovimentiContabiliTesta.CentroServizio like '13%'"
        End If
        If TabellaSocieta.NomeFile.Trim = "CHIABRERA" Then
            MySql = MySql & " AND MovimentiContabiliTesta.CentroServizio like '14%'"
        End If


        cn = New Data.OleDb.OleDbConnection(Session("DC_GENERALE"))

        cn.Open()
        Dim cmd As New OleDbCommand()
        cmd.CommandText = MySql

        cmd.Connection = cn
        If Tab_PeriodoData.ActiveTabIndex = 1 And IsDate(Txt_DataAl1.Text) And IsDate(Txt_DataDal1.Text) Then
            cmd.Parameters.AddWithValue("@DataRegistrazione", Txt_DataDal1.Text)
            cmd.Parameters.AddWithValue("@DataRegistrazione", Txt_DataAl1.Text)
        End If

        Dim myPOSTreader As OleDbDataReader = cmd.ExecuteReader()
        If myPOSTreader.Read Then
            If IsDBNull(myPOSTreader.Item(0)) Then
                MinimoDocumento = 0
            Else
                MinimoDocumento = Val(myPOSTreader.Item(0))
            End If
        End If
        myPOSTreader.Close()
        cn.Close()

    End Function
    Function MassimoDocumento() As Long
        Dim cn As OleDbConnection

        Dim MeseContr As Long
        Dim MySql As String

        MeseContr = DD_Mese.SelectedValue

        MassimoDocumento = 0

        Dim TabellaSocieta As New Cls_TabellaSocieta


        TabellaSocieta.Leggi(Session("DC_TABELLE"))


        If Tab_PeriodoData.ActiveTabIndex = 1 And IsDate(Txt_DataAl1.Text) And IsDate(Txt_DataDal1.Text) Then
            MySql = "SELECT max(NumeroProtocollo) " & _
                    " FROM MovimentiContabiliRiga INNER JOIN MovimentiContabiliTesta " & _
                    " ON MovimentiContabiliRiga.Numero = MovimentiContabiliTesta.NumeroRegistrazione " & _
                    " WHERE MovimentiContabiliTesta.DataRegistrazione >=  ? " & _
                    " AND MovimentiContabiliTesta.DataRegistrazione <= ? "
        Else
            MySql = "SELECT max(NumeroProtocollo) " & _
                    " FROM MovimentiContabiliRiga INNER JOIN MovimentiContabiliTesta " & _
                    " ON MovimentiContabiliRiga.Numero = MovimentiContabiliTesta.NumeroRegistrazione " & _
                    " WHERE MovimentiContabiliTesta.AnnoCompetenza = " & Txt_Anno.Text & " " & _
                    " AND MovimentiContabiliTesta.MeseCompetenza = " & MeseContr & " " & _
                    " AND AnnoProtocollo = " & Txt_AnnoRif.Text

        End If

        
        If DD_Registro.SelectedValue <> "" Then
            MySql = MySql & " AND MovimentiContabiliTesta.RegistroIva = " & DD_Registro.SelectedValue
        End If



        If TabellaSocieta.NomeFile.Trim = "MDAZEGLIO" Then
            MySql = MySql & " AND MovimentiContabiliTesta.CentroServizio like '13%'"
        End If
        If TabellaSocieta.NomeFile.Trim = "CHIABRERA" Then
            MySql = MySql & " AND MovimentiContabiliTesta.CentroServizio like '14%'"
        End If



        cn = New Data.OleDb.OleDbConnection(Session("DC_GENERALE"))

        cn.Open()
        Dim cmd As New OleDbCommand()
        cmd.CommandText = MySql
        If Tab_PeriodoData.ActiveTabIndex = 1 And IsDate(Txt_DataAl1.Text) And IsDate(Txt_DataDal1.Text) Then
            cmd.Parameters.AddWithValue("@DataRegistrazione", Txt_DataDal1.Text)
            cmd.Parameters.AddWithValue("@DataRegistrazione", Txt_DataAl1.Text)
        End If
        cmd.Connection = cn


        Dim myPOSTreader As OleDbDataReader = cmd.ExecuteReader()
        If myPOSTreader.Read Then
            If IsDBNull(myPOSTreader.Item(0)) Then
                MassimoDocumento = 0
            Else
                MassimoDocumento = Val(myPOSTreader.Item(0))
            End If
        End If
        myPOSTreader.Close()
        cn.Close()

    End Function

    Protected Sub DD_Registro_TextChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles DD_Registro.TextChanged
        Txt_DalDocumento.Text = MinimoDocumento()
        Txt_AlDocumento.Text = MassimoDocumento()
    End Sub

    
    Protected Sub OspitiWeb_Export_Documenti_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Trim(Session("UTENTE")) = "" Then
            Response.Redirect("..\Login.aspx")
            Exit Sub
        End If

        Call EseguiJS()


        If Page.IsPostBack = True Then Exit Sub


        Dim M As New Cls_Parametri


        M.LeggiParametri(Session("DC_OSPITE"))

        If M.TipoExport <> "" Then
            If M.TipoExport = "Documenti Incassi" Or M.TipoExport = "PassePartout" Or M.TipoExport = "POLIFARMA" Or M.TipoExport = "ZUCCHETTI" Then
                Tab_Anagrafica.Visible = False
            Else
                TabPassPartout.Visible = False
            End If

            RB_AlianteNew.Visible = False
            RB_Aliante.Visible = False
            RB_SEAC.Visible = False
            RB_Multi.Visible = False
            RB_Gamma.Visible = False
            RB_AdHoc.Visible = False
            RB_IPSOA.Visible = False
            RB_PassePartout.Visible = False
            RB_Ats.Visible = False
            RB_DocumentiIncassi.Visible = False
            RB_POLIFARMA.Visible = False
            RB_Zucchetti.Visible = False
            Dd_RegistroDATA.Visible = False

            If M.TipoExport = "Documenti Incassi" Then
                RB_DocumentiIncassi.Checked = True
                Chk_Prova.Visible = True
                lbl_prova.Visible = True
            End If

            If M.TipoExport = "PassePartout" Then
                RB_PassePartout.Checked = True
                Chk_Prova.Visible = True
                lbl_prova.Visible = True
                lbl_prova.Text = "Prova"
                Dd_RegistroDATA.Visible = True

            End If

            If M.TipoExport = "POLIFARMA" Then
                RB_POLIFARMA.Checked = True
            End If

            If M.TipoExport = "ZUCCHETTI" Then
                RB_Zucchetti.Checked = True
            End If

            'RB_Zucchetti.Visible = False

            If M.TipoExport = "ALYANTE" Then
                RB_AlianteNew.Visible = True
                RB_AlianteNew.Checked = True
                RB_Aliante.Visible = True
            End If

            If M.TipoExport = "Seac" Then
                RB_SEAC.Checked = True
            End If

            If M.TipoExport = "Multi" Then
                RB_Multi.Checked = True
            End If


            If M.TipoExport = "Gamma" Then
                RB_Gamma.Checked = True
            End If



            If M.TipoExport = "AdHoc" Then
                RB_AdHoc.Checked = True
            End If

            If M.TipoExport = "IpSoa" Then
                RB_AdHoc.Checked = True
            End If

            If M.TipoExport = "ADS SYSTEMATICA" Then
                RB_Ats.Checked = True
            End If

            If M.TipoExport = "ZUCCHETTI" Then
                Dd_RegistroDATA.Visible = True
            End If
        End If


        Dim K1 As New Cls_SqlString

        Dim Barra As New Cls_BarraSenior

        If Not IsNothing(Session("RicercaAnagraficaSQLString")) Then
            K1 = Session("RicercaAnagraficaSQLString")
        End If

        Lbl_BarraSenior.Text = Barra.CodiceBarra(Application("SENIOR"), Session("UTENTE"), Page, K1)


        Dim X As New Cls_RegistroIVA
        Dim f As New Cls_Parametri
        Lbl_Waiting.Text = ""
        f.LeggiParametri(Session("DC_OSPITE"))
        X.UpDateDropBox(Session("DC_TABELLE"), DD_Registro)
        X.UpDateDropBox(Session("DC_TABELLE"), Dd_RegistroDATA)


        Txt_Anno.Text = f.AnnoFatturazione
        DD_Mese.SelectedValue = f.MeseFatturazione
        Txt_AnnoRif.Text = f.AnnoFatturazione



        Dim XS As New Cls_Login

        XS.Utente = Session("UTENTE")
        XS.LeggiSP(Application("SENIOR"))

        Session("CampoProgressBar") = 0

    End Sub

    Protected Sub Btn_Modifica_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles Btn_Modifica.Click
        Dim Verifica As Boolean = True
        If RB_Aliante.Checked = True Then
            Call Export_Aliante()
        End If

        If RB_AlianteNew.Checked = True Then
            Verifica = Export_AlianteNuovaVersione()
        End If

        If RB_SEAC.Checked = True Then
            Call EsportaSeac()
        End If

        If RB_Multi.Checked = True Then
            Call Export_Multi()
        End If

        If RB_Gamma.Checked = True Then
            Call Export_Gamma()
        End If

        If RB_AdHoc.Checked = True Then
            Call Export_AdHoc()
        End If

        If RB_IPSOA.Checked = True Then
            Call Export_IpSoa()
        End If

        If RB_PassePartout.Checked = True Then
            Call Export_PassePartout()
        End If

 

        If RB_POLIFARMA.Checked = True Then
            Verifica = Export_polifarma()
        End If


        If RB_Ats.Checked = True Then
            Call Export_ATS_Ver0()
        End If

        If RB_Zucchetti.Checked = True Then
            Call Export_Zucchetti()
        End If

        'Export_PassePartout

        If Verifica Then
            ScriptManager.RegisterClientScriptBlock(Me, Me.GetType(), "Stampa2", "alert('Operazione Terminata');", True)
        Else
            ScriptManager.RegisterClientScriptBlock(Me, Me.GetType(), "Stampa2", "alert('Errore in esportazione');", True)
        End If
    End Sub

    Sub Export_IpSoa()
        Dim cn As OleDbConnection
        Dim Anno As Integer
        Dim Mese As Integer
        Dim MeseContr As Integer
        Dim MySql As String

        Dim NomeFileClienti As String
        Dim NomeFileMovim As String
        Dim Stringa As String

        Dim CampoClienti(500) As String
        Dim CampoMovimenti(500) As String

        NomeFileClienti = HostingEnvironment.ApplicationPhysicalPath() & "\Public\Clienti_.txt"
        NomeFileMovim = HostingEnvironment.ApplicationPhysicalPath() & "\Public\Movim_.txt"


        Dim FileClienti As System.IO.TextWriter = System.IO.File.CreateText(NomeFileClienti)

        Dim FileMovim As System.IO.TextWriter = System.IO.File.CreateText(NomeFileMovim)


        Anno = Txt_Anno.Text
        Mese = DD_Mese.SelectedValue

        MeseContr = DD_Mese.SelectedValue



        MySql = "SELECT * " & _
                " FROM MovimentiContabiliTesta " & _
                " WHERE AnnoProtocollo = " & Txt_AnnoRif.Text

        If DD_Registro.SelectedValue <> "" Then
            MySql = MySql & " AND MovimentiContabiliTesta.RegistroIva = " & DD_Registro.SelectedValue
        End If
        If Tab_PeriodoData.ActiveTabIndex = 1 And IsDate(Txt_DataAl1.Text) And IsDate(Txt_DataDal1.Text) Then
            MySql = MySql & " AND (DataRegistrazione >= ? And DataRegistrazione <= ?) "
        Else
            MySql = MySql & " And MovimentiContabiliTesta.AnnoCompetenza = " & Txt_Anno.Text & " AND MovimentiContabiliTesta.MeseCompetenza = " & MeseContr & " "
        End If
        MySql = MySql & " AND (NumeroProtocollo >= " & Txt_DalDocumento.Text & " And NumeroProtocollo <= " & Txt_AlDocumento.Text & ") "

        cn = New Data.OleDb.OleDbConnection(Session("DC_GENERALE"))

        cn.Open()
        Dim cmd As New OleDbCommand()
        cmd.CommandText = MySql & " Order by NumeroProtocollo"
        cmd.Connection = cn
        If Tab_PeriodoData.ActiveTabIndex = 1 And IsDate(Txt_DataAl1.Text) And IsDate(Txt_DataDal1.Text) Then
            cmd.Parameters.AddWithValue("@DataDal", Txt_DataDal1.Text)
            cmd.Parameters.AddWithValue("@DataDal", Txt_DataAl1.Text)
        End If



        Dim MyRsDB As OleDbDataReader = cmd.ExecuteReader()

        Do While MyRsDB.Read
            Dim Imponibile As Double
            Dim Registrazione As New Cls_MovimentoContabile
            Dim CausaleContabile As New Cls_CausaleContabile

            Registrazione.Leggi(Session("DC_GENERALE"), MyRsDB.Item("NumeroRegistrazione"))

            CausaleContabile.Codice = Registrazione.CausaleContabile
            CausaleContabile.Leggi(Session("DC_TABELLE"), CausaleContabile.Codice)


            Dim TipoAnagrafica As String
            Dim Ospiti As New ClsOspite
            Dim Parente As New Cls_Parenti
            Dim Regioni As New ClsUSL
            Dim Comune As New ClsComune

            If Registrazione.Tipologia = "" Then
                Dim CercaCf As Integer
                Dim SottocontoCf As Integer

                For CercaCf = 0 To Registrazione.Righe.Length - 1
                    If Not IsNothing(Registrazione.Righe(CercaCf)) Then
                        If Registrazione.Righe(CercaCf).Tipo = "CF" Then
                            SottocontoCf = Registrazione.Righe(CercaCf).SottocontoPartita
                        End If
                    End If
                Next

                If Int(SottocontoCf / 100) = Math.Round(SottocontoCf / 100, 2) Then
                    Ospiti.CodiceOspite = Int(SottocontoCf / 100)
                    Ospiti.Leggi(Session("DC_OSPITE"), Ospiti.CodiceOspite)
                    TipoAnagrafica = "O"
                Else
                    Parente.CodiceParente = SottocontoCf - (Int(SottocontoCf / 100) * 100)
                    Parente.CodiceOspite = Int(SottocontoCf / 100)
                    Parente.Leggi(Session("DC_OSPITE"), Parente.CodiceOspite, Parente.CodiceParente)
                    TipoAnagrafica = "P"
                End If
            Else
                If Mid(Registrazione.Tipologia & Space(10), 1, 1) = "C" Then
                    Comune.Provincia = Mid(Registrazione.Tipologia & Space(10), 2, 3)
                    Comune.Comune = Mid(Registrazione.Tipologia & Space(10), 5, 3)
                    Comune.Leggi(Session("DC_OSPITE"))

                    TipoAnagrafica = "C"
                End If
                If Mid(Registrazione.Tipologia & Space(10), 1, 1) = "J" Then
                    Comune.Provincia = Mid(Registrazione.Tipologia & Space(10), 2, 3)
                    Comune.Comune = Mid(Registrazione.Tipologia & Space(10), 5, 3)
                    Comune.Leggi(Session("DC_OSPITE"))
                    TipoAnagrafica = "C"
                End If
                If Mid(Registrazione.Tipologia & Space(10), 1, 1) = "R" Then
                    Regioni.CodiceRegione = Mid(Registrazione.Tipologia & Space(10), 2, 4)
                    Regioni.Leggi(Session("DC_OSPITE"))

                    TipoAnagrafica = "R"
                End If
            End If

            Dim Indice As Integer
            Dim I As Integer
            Dim CodiceIVA As String
            Dim Imposta As Double
            Dim Mastro As Integer = 0
            Dim Conto As Integer = 0
            Dim Sottoconto As Integer = 0
            Dim DescrizioneRiga As String = ""

            Imposta = 0
            Imponibile = 0
            CodiceIVA = ""
            For Indice = 0 To Registrazione.Righe.Length - 1
                If Not IsNothing(Registrazione.Righe(Indice)) Then
                    If Registrazione.Righe(Indice).Tipo = "IV" Then
                        Imponibile = Imponibile + Registrazione.Righe(Indice).Imponibile
                        Imposta = Imposta + Registrazione.Righe(Indice).Importo
                        CodiceIVA = Registrazione.Righe(Indice).CodiceIVA
                    End If

                    If Registrazione.Righe(Indice).RigaDaCausale = 3 Then
                        Mastro = Registrazione.Righe(Indice).MastroPartita
                        Conto = Registrazione.Righe(Indice).ContoPartita
                        Sottoconto = Registrazione.Righe(Indice).SottocontoPartita
                        DescrizioneRiga = Registrazione.Righe(Indice).Descrizione
                    End If
                End If
            Next
            Dim RagioneSociale As String = ""
            Dim Codice As String = ""
            Dim ResidenzaIndirizzo As String = ""
            Dim ComuneP As String = ""
            Dim ProvinciaP As String = ""
            Dim Cap As String = ""
            Dim PersonaFisica As String = "S"
            Dim PartitaIVA As String = ""
            Dim CodiceFiscale As String = ""
            Dim ContoRicavo As String = ""


            If TipoAnagrafica = "C" Then
                RagioneSociale = Comune.Descrizione
                Codice = Comune.Provincia & Comune.Comune
                ResidenzaIndirizzo = Comune.RESIDENZAINDIRIZZO1
                Cap = Comune.RESIDENZACAP1
                ComuneP = Comune.RESIDENZACOMUNE1
                ProvinciaP = Comune.RESIDENZAPROVINCIA1
                PersonaFisica = "N"

                PartitaIVA = Comune.PartitaIVA
                CodiceFiscale = Comune.CodiceFiscale

                If Conto = 2 Then
                    ContoRicavo = "51010305" 'non auto
                End If
                If Conto = 1 Then
                    ContoRicavo = "51010105" 'auto
                End If

            End If
            If TipoAnagrafica = "R" Then
                Codice = Regioni.CodiceRegione    'CoidceCliente
                RagioneSociale = Regioni.Nome
                ResidenzaIndirizzo = Regioni.RESIDENZAINDIRIZZO1
                Cap = Regioni.RESIDENZACAP1
                ComuneP = Regioni.RESIDENZACOMUNE1
                ProvinciaP = Regioni.RESIDENZAPROVINCIA1
                PersonaFisica = "N"

                PartitaIVA = Regioni.PARTITAIVA
                CodiceFiscale = Regioni.CodiceFiscale

                If Conto = 2 Then
                    ContoRicavo = "51010303" 'non auto
                End If
                If Conto = 1 Then
                    ContoRicavo = "51010103" 'auto
                End If

                If Conto = 20 Then
                    ContoRicavo = "51010303" 'non auto
                End If
            End If
            If TipoAnagrafica = "O" Then
                Codice = Ospiti.CodiceOspite * 100
                RagioneSociale = Ospiti.Nome
                ResidenzaIndirizzo = Ospiti.RESIDENZAINDIRIZZO1
                Cap = Ospiti.RESIDENZACAP1
                ComuneP = Ospiti.RESIDENZACOMUNE1
                ProvinciaP = Ospiti.RESIDENZAPROVINCIA1

                CodiceFiscale = Ospiti.CODICEFISCALE

                If Conto = 2 Then
                    ContoRicavo = "51010301" 'non auto
                End If
                If Conto = 1 Then
                    ContoRicavo = "51010101" 'auto
                End If
            End If
            If TipoAnagrafica = "P" Then
                Codice = (Parente.CodiceOspite * 100) + Parente.CodiceParente    'CoidceCliente
                RagioneSociale = Parente.Nome
                ResidenzaIndirizzo = Parente.RESIDENZAINDIRIZZO1
                Cap = Parente.RESIDENZACAP1
                ComuneP = Parente.RESIDENZACOMUNE1
                ProvinciaP = Parente.RESIDENZAPROVINCIA1

                CodiceFiscale = Parente.CODICEFISCALE

                If Conto = 2 Then
                    ContoRicavo = "51010301" 'non auto
                End If
                If Conto = 1 Then
                    ContoRicavo = "51010101" 'auto
                End If
            End If

            I = 0

            I = I + 1 : CampoClienti(I) = AccodaSpazio(Codice, 6)   'CoidceCliente
            I = I + 1 : CampoClienti(I) = AccodaSpazio(RagioneSociale, 40)
            I = I + 1 : CampoClienti(I) = Space(40)
            I = I + 1 : CampoClienti(I) = AccodaSpazio(ResidenzaIndirizzo, 40)

            Dim DecoComune As New ClsComune

            DecoComune.Provincia = ComuneP
            DecoComune.Comune = ProvinciaP
            DecoComune.Leggi(Session("DC_OSPITE"))

            I = I + 1 : CampoClienti(I) = AccodaSpazio(DecoComune.Descrizione, 62)
            I = I + 1 : CampoClienti(I) = AccodaSpazio(Cap, 9)
            I = I + 1 : CampoClienti(I) = AccodaSpazio(DecoComune.CodificaProvincia, 2)  'ID_PROVINCIA
            I = I + 1 : CampoClienti(I) = Space(3) 'ID_REGIONE
            I = I + 1 : CampoClienti(I) = Space(3)  'ID_NAZIONE_ESTERA
            I = I + 1 : CampoClienti(I) = Space(40) 'EMAIL
            I = I + 1 : CampoClienti(I) = Space(5) 'TELEFONO1_1
            I = I + 1 : CampoClienti(I) = Space(5) 'TELEFONO1_2
            I = I + 1 : CampoClienti(I) = Space(10) 'TELEFONO1_3
            I = I + 1 : CampoClienti(I) = Space(5) 'TELEFONO 2_1
            I = I + 1 : CampoClienti(I) = Space(5) 'TELEFONO 2_2
            I = I + 1 : CampoClienti(I) = Space(10) 'TELEFONO 2_3
            I = I + 1 : CampoClienti(I) = Space(5) 'FAX 1
            I = I + 1 : CampoClienti(I) = Space(5) 'FAX 2
            I = I + 1 : CampoClienti(I) = Space(10) 'FAX 3
            I = I + 1 : CampoClienti(I) = Space(30) 'PERSONA CONTATTARE
            I = I + 1 : CampoClienti(I) = Space(40) 'CHIAVE RICERCA
            I = I + 1 : CampoClienti(I) = Space(80) 'NOTA
            I = I + 1 : CampoClienti(I) = PersonaFisica 'FLAG PERSONA FISICA
            I = I + 1 : CampoClienti(I) = Space(1) 'SESSO            
            I = I + 1 : CampoClienti(I) = Space(8) 'DATA NASCITA
            I = I + 1 : CampoClienti(I) = Space(62) 'CITTA NASCITA
            I = I + 1 : CampoClienti(I) = Space(25) 'COGNOME
            I = I + 1 : CampoClienti(I) = Space(25) 'NOME            
            I = I + 1 : CampoClienti(I) = Space(40) 'INDIRIZZO LEGALE
            I = I + 1 : CampoClienti(I) = Space(62) 'CITTA LEGALE
            I = I + 1 : CampoClienti(I) = Space(9) 'CAP LEGALE
            I = I + 1 : CampoClienti(I) = Space(2) 'ID PROVINCIA LEGALE
            I = I + 1 : CampoClienti(I) = Space(3) 'ID_REGIONE_LEGALE
            I = I + 1 : CampoClienti(I) = Space(2) 'CODICE ISO PER PARTITA IVA (?)
            I = I + 1 : CampoClienti(I) = AdattaLunghezzaNumero(PartitaIVA, 12) 'PIVA
            I = I + 1 : CampoClienti(I) = AdattaLunghezzaNumero(CodiceFiscale, 16) 'CF
            I = I + 1 : CampoClienti(I) = Space(16) 'CODICEFISCASLE ESTERO
            I = I + 1 : CampoClienti(I) = ContoRicavo 'ID_cONTO
            I = I + 1 : CampoClienti(I) = Space(12) 'ID_CONTO_FAT
            I = I + 1 : CampoClienti(I) = Space(12) 'ID_CONTO_PAG
            I = I + 1 : CampoClienti(I) = Space(3) 'ID_IVA
            I = I + 1 : CampoClienti(I) = Space(3) 'ID_DIVISA
            I = I + 1 : CampoClienti(I) = "0" 'BLOCCATO
            I = I + 1 : CampoClienti(I) = Space(8) 'DATA INIZIO RAPPORTO CON IL CLIENTE
            I = I + 1 : CampoClienti(I) = Space(1) 'TIPO PAGAMENTO DEFAULT
            I = I + 1 : CampoClienti(I) = "N" 'TITOLA PARTITA IVA
            I = I + 1 : CampoClienti(I) = Space(24) 'RE_STATOFEDERALE
            I = I + 1 : CampoClienti(I) = Space(24) 'RE_LOCALITA
            I = I + 1 : CampoClienti(I) = Space(35) 'RE_INDIRIZZO


            Stringa = ""
            For Z = 1 To I
                Stringa = Stringa & CampoClienti(Z) & vbTab
            Next Z
            FileClienti.WriteLine(Stringa)


            For Indice = 0 To Registrazione.Righe.Length - 1
                If Not IsNothing(Registrazione.Righe(Indice)) Then

                    I = 0

                    I = I + 1 : CampoMovimenti(I) = AdattaLunghezzaNumero(Registrazione.NumeroRegistrazione, 10)
                    I = I + 1 : CampoMovimenti(I) = Format(Registrazione.DataRegistrazione, "yyyyMMdd")
                    I = I + 1 : CampoMovimenti(I) = Space(8)
                    I = I + 1 : CampoMovimenti(I) = Space(8)
                    I = I + 1 : CampoMovimenti(I) = Format(Registrazione.DataDocumento, "yyyyMMdd")
                    I = I + 1 : CampoMovimenti(I) = AdattaLunghezzaNumero(Registrazione.NumeroProtocollo, 10)

                    If CausaleContabile.TipoDocumento = "NC" Then
                        If TipoAnagrafica = "O" Or TipoAnagrafica = "P" Then
                            I = I + 1 : CampoMovimenti(I) = "V03 " 'VENDITE FUORI CAMPO IVA
                        Else
                            I = I + 1 : CampoMovimenti(I) = "V61 " 'SPLIT PAYMENT
                        End If
                    Else
                        If TipoAnagrafica = "O" Or TipoAnagrafica = "P" Then
                            I = I + 1 : CampoMovimenti(I) = "V21 " 'VENDITE FUORI CAMPO IVA
                        Else
                            I = I + 1 : CampoMovimenti(I) = "V60 " 'SPLIT PAYMENT
                        End If
                    End If
                    I = I + 1 : CampoMovimenti(I) = Space(40) 'DESCRIZIONE
                    I = I + 1 : CampoMovimenti(I) = Space(2) 'CODICE PROVISORIO


                    If Registrazione.RegistroIVA = 11 Then
                        I = I + 1 : CampoMovimenti(I) = AdattaLunghezzaNumero(Registrazione.RegistroIVA, 2) 'REGISTRO IVA PA
                    Else
                        I = I + 1 : CampoMovimenti(I) = AdattaLunghezzaNumero(Registrazione.RegistroIVA, 2) 'REGISTRO IVA VENDITE
                    End If

                    I = I + 1 : CampoMovimenti(I) = AdattaLunghezzaNumero(Registrazione.NumeroProtocollo, 10)

                    If Registrazione.Righe(Indice).Tipo = "CF" Then
                        I = I + 1 : CampoMovimenti(I) = "110101" 'id_Conto
                    Else
                        If Registrazione.Righe(Indice).Tipo = "IV" Then
                            I = I + 1 : CampoMovimenti(I) = "274101" 'id_Conto
                        Else
                            I = I + 1 : CampoMovimenti(I) = ContoRicavo  'id_Conto
                        End If
                    End If


                    I = I + 1 : CampoMovimenti(I) = Space(12) 'id_Conto competenza?


                    If Registrazione.Righe(Indice).Tipo = "CF" Then
                        I = I + 1 : CampoMovimenti(I) = AccodaSpazio(Codice, 6)  'SubConto
                    Else
                        I = I + 1 : CampoMovimenti(I) = ""  'SubConto
                    End If

                    I = I + 1 : CampoMovimenti(I) = AdattaLunghezza(Registrazione.Righe(Indice).Descrizione, 40) 'Descrizione
                    If Registrazione.Righe(Indice).Tipo = "IV" Then
                        I = I + 1 : CampoMovimenti(I) = AdattaLunghezza(0, 19) 'Importo_Lire

                        I = I + 1 : CampoMovimenti(I) = AdattaLunghezza(0, 19) 'Imponibile IVA
                        I = I + 1 : CampoMovimenti(I) = "" 'Codice IVA
                        I = I + 1 : CampoMovimenti(I) = AdattaLunghezza(0, 19) 'Imposta
                    Else

                        If Registrazione.Righe(Indice).DareAvere = "D" Then
                            I = I + 1 : CampoMovimenti(I) = AdattaLunghezza(Registrazione.Righe(Indice).Imponibile + Registrazione.Righe(Indice).Importo, 19) 'Importo_Lire

                            I = I + 1 : CampoMovimenti(I) = AdattaLunghezza(Registrazione.Righe(Indice).Imponibile, 19) 'Imponibile IVA

                            If Registrazione.Righe(Indice).Tipo = "CF" Then
                                I = I + 1 : CampoMovimenti(I) = "" 'Codice IVA
                                I = I + 1 : CampoMovimenti(I) = AdattaLunghezza(0, 19) 'Imposta
                            Else
                                I = I + 1 : CampoMovimenti(I) = "095" 'Codice IVA
                                I = I + 1 : CampoMovimenti(I) = AdattaLunghezza(0, 19) 'Imposta
                            End If
                        Else

                            I = I + 1 : CampoMovimenti(I) = AdattaLunghezza((Registrazione.Righe(Indice).Imponibile + Registrazione.Righe(Indice).Importo) * -1, 19) 'Importo_Lire

                            I = I + 1 : CampoMovimenti(I) = AdattaLunghezza(Registrazione.Righe(Indice).Imponibile * -1, 19) 'Imponibile IVA


                            If Registrazione.Righe(Indice).Tipo = "CF" Then
                                I = I + 1 : CampoMovimenti(I) = "" 'Codice IVA
                                I = I + 1 : CampoMovimenti(I) = AdattaLunghezza(0, 19) 'Imposta
                            Else
                                I = I + 1 : CampoMovimenti(I) = "095" 'Codice IVA
                                I = I + 1 : CampoMovimenti(I) = AdattaLunghezza(0, 19) 'Imposta
                            End If

                        End If
                    End If

                    I = I + 1 : CampoMovimenti(I) = AdattaLunghezza(0, 1) 'Indetraibilità

                    I = I + 1 : CampoMovimenti(I) = AdattaLunghezza(0, 1) 'Importo in divisa
                    I = I + 1 : CampoMovimenti(I) = "" 'divisa
                    I = I + 1 : CampoMovimenti(I) = "" 'cambio
                    I = I + 1 : CampoMovimenti(I) = "0" 'partita
                    I = I + 1 : CampoMovimenti(I) = "0" 'annopartita
                    I = I + 1 : CampoMovimenti(I) = "" 'datainzio comp

                    I = I + 1 : CampoMovimenti(I) = "" 'datafine comp
                    I = I + 1 : CampoMovimenti(I) = "" 'id forfait
                    If Registrazione.Righe(Indice).Tipo = "IV" Then
                        I = I + 1 : CampoMovimenti(I) = "I" 'Tipo_Riga
                    Else
                        If Registrazione.Righe(Indice).Tipo = "CF" Then
                            I = I + 1 : CampoMovimenti(I) = "S" 'Tipo_Riga
                        Else
                            I = I + 1 : CampoMovimenti(I) = "" 'Tipo_Riga
                        End If
                    End If
                    I = I + 1 : CampoMovimenti(I) = "" 'ID_CAURIGA
                    I = I + 1 : CampoMovimenti(I) = "" 'ID_RegIVA_Vendite
                    I = I + 1 : CampoMovimenti(I) = "0" 'ID_PROT_IVA_Vendite
                    I = I + 1 : CampoMovimenti(I) = "" 'ID_SCADENZA
                    I = I + 1 : CampoMovimenti(I) = "" 'TITOLARE_PIVA
                    I = I + 1 : CampoMovimenti(I) = "" 'IDCENTROCOSTO
                    I = I + 1 : CampoMovimenti(I) = "" 'IDCOMMESSA
                    I = I + 1 : CampoMovimenti(I) = "" 'TIPOOPERAZIONE



                    Stringa = ""
                    For Z = 1 To I
                        Stringa = Stringa & CampoMovimenti(Z) & vbTab
                    Next Z
                    FileMovim.WriteLine(Stringa)

                End If
            Next


        Loop
        MyRsDB.Close()
        cn.Close()
        FileMovim.Close()
        FileClienti.Close()



        Btn_Testa.Text = "Movimenti"
        Btn_Riga.Text = "Clienti"
        Btn_Testa.Visible = True
        Btn_Riga.Visible = True


    End Sub
    Sub Export_AdHoc()
        Dim cn As OleDbConnection

        Dim MeseContr As Long
        Dim MySql As String
        Dim ProgressivoAssoluto As Integer = 0
        Dim Ragguppato As Boolean = False
        Dim ErroriEsportazione As String = ""
        Dim Anno As Integer
        Dim Mese As Integer


        Dim CampoTesta(500) As String
        Dim CampoRiga(200) As String
        Dim StampeDb As New ADODB.Connection


        Dim NomeFile As String
        Dim NomeFileRiga As String

        NomeFile = HostingEnvironment.ApplicationPhysicalPath() & "\Public\Esporta.Tfa"
        NomeFileRiga = HostingEnvironment.ApplicationPhysicalPath() & "\Public\Esporta.Rfa"


        Dim FileTesta As System.IO.TextWriter = System.IO.File.CreateText(NomeFile)

        Dim FileRiga As System.IO.TextWriter = System.IO.File.CreateText(NomeFileRiga)


        Anno = Txt_Anno.Text
        Mese = DD_Mese.SelectedValue

        MeseContr = DD_Mese.SelectedValue


        MySql = "SELECT * " & _
                " FROM MovimentiContabiliTesta " & _
                " WHERE AnnoProtocollo = " & Txt_AnnoRif.Text

        If DD_Registro.SelectedValue <> "" Then
            MySql = MySql & " AND MovimentiContabiliTesta.RegistroIva = " & DD_Registro.SelectedValue
        End If
        If Tab_PeriodoData.ActiveTabIndex = 1 And IsDate(Txt_DataAl1.Text) And IsDate(Txt_DataDal1.Text) Then
            MySql = MySql & " AND (DataRegistrazione >= ? And DataRegistrazione <= ?) "
        Else
            MySql = MySql & " And MovimentiContabiliTesta.AnnoCompetenza = " & Txt_Anno.Text & " AND MovimentiContabiliTesta.MeseCompetenza = " & MeseContr & " "
        End If
        MySql = MySql & " AND (NumeroProtocollo >= " & Txt_DalDocumento.Text & " And NumeroProtocollo <= " & Txt_AlDocumento.Text & ") "

        cn = New Data.OleDb.OleDbConnection(Session("DC_GENERALE"))

        cn.Open()
        Dim cmd As New OleDbCommand()
        cmd.CommandText = MySql & " Order by NumeroProtocollo"
        cmd.Connection = cn
        If Tab_PeriodoData.ActiveTabIndex = 1 And IsDate(Txt_DataAl1.Text) And IsDate(Txt_DataDal1.Text) Then
            cmd.Parameters.AddWithValue("@DataDal", Txt_DataDal1.Text)
            cmd.Parameters.AddWithValue("@DataDal", Txt_DataAl1.Text)
        End If



        Dim MyRsDB As OleDbDataReader = cmd.ExecuteReader()

        Do While MyRsDB.Read
            Dim Imponibile As Double
            Dim Registrazione As New Cls_MovimentoContabile
            Dim CausaleContabile As New Cls_CausaleContabile

            Registrazione.Leggi(Session("DC_GENERALE"), MyRsDB.Item("NumeroRegistrazione"))

            CausaleContabile.Codice = Registrazione.CausaleContabile
            CausaleContabile.Leggi(Session("DC_TABELLE"), CausaleContabile.Codice)


            Dim TipoAnagrafica As String
            Dim Ospiti As New ClsOspite
            Dim Parente As New Cls_Parenti
            Dim Regioni As New ClsUSL
            Dim Comune As New ClsComune

            If Registrazione.Tipologia = "" Then
                Dim CercaCf As Integer
                Dim SottocontoCf As Integer

                For CercaCf = 0 To Registrazione.Righe.Length - 1
                    If Not IsNothing(Registrazione.Righe(CercaCf)) Then
                        If Registrazione.Righe(CercaCf).Tipo = "CF" Then
                            SottocontoCf = Registrazione.Righe(CercaCf).SottocontoPartita
                        End If
                    End If
                Next

                If Int(SottocontoCf / 100) = Math.Round(SottocontoCf / 100, 2) Then
                    Ospiti.CodiceOspite = Int(SottocontoCf / 100)
                    Ospiti.Leggi(Session("DC_OSPITE"), Ospiti.CodiceOspite)
                    TipoAnagrafica = "O"
                Else
                    Parente.CodiceParente = SottocontoCf - (Int(SottocontoCf / 100) * 100)
                    Parente.CodiceOspite = Int(SottocontoCf / 100)
                    Parente.Leggi(Session("DC_OSPITE"), Parente.CodiceOspite, Parente.CodiceParente)
                    TipoAnagrafica = "P"
                End If
            Else
                If Mid(Registrazione.Tipologia & Space(10), 1, 1) = "C" Then
                    Comune.Provincia = Mid(Registrazione.Tipologia & Space(10), 2, 3)
                    Comune.Comune = Mid(Registrazione.Tipologia & Space(10), 5, 3)
                    Comune.Leggi(Session("DC_OSPITE"))

                    TipoAnagrafica = "C"
                End If
                If Mid(Registrazione.Tipologia & Space(10), 1, 1) = "J" Then
                    Comune.Provincia = Mid(Registrazione.Tipologia & Space(10), 2, 3)
                    Comune.Comune = Mid(Registrazione.Tipologia & Space(10), 5, 3)
                    Comune.Leggi(Session("DC_OSPITE"))
                    TipoAnagrafica = "C"
                End If
                If Mid(Registrazione.Tipologia & Space(10), 1, 1) = "R" Then
                    Regioni.CodiceRegione = Mid(Registrazione.Tipologia & Space(10), 2, 4)
                    Regioni.Leggi(Session("DC_OSPITE"))

                    TipoAnagrafica = "R"
                End If
            End If

            Dim Indice As Integer
            Dim I As Integer
            Dim CodiceIVA As String
            Dim Imposta As Double
            Dim Stringa As String


            Imposta = 0
            Imponibile = 0
            CodiceIVA = ""
            For Indice = 0 To Registrazione.Righe.Length - 1
                If Not IsNothing(Registrazione.Righe(Indice)) Then
                    If Registrazione.Righe(Indice).Tipo = "IV" Then
                        Imponibile = Imponibile + Registrazione.Righe(Indice).Imponibile
                        Imposta = Imposta + Registrazione.Righe(Indice).Importo
                        CodiceIVA = Registrazione.Righe(Indice).CodiceIVA
                    End If
                End If
            Next



            I = 0
            I = I + 1 : CampoTesta(I) = Format(Registrazione.DataRegistrazione, "yyyyMMdd") ' datadocumento
            I = I + 1 : CampoTesta(I) = PrimaSpazio(Format(Registrazione.NumeroProtocollo, "######"), 6) ' NumeroDocumento
            I = I + 1
            If UCase(CausaleContabile.TipoDocumento) = UCase("RE") Or UCase(CausaleContabile.TipoDocumento) = UCase("FT") Then
                CampoTesta(I) = "FA"  ' Causale Documento

                If Val(DD_Registro.SelectedValue) = 10 And Val(Dd_RegistroDATA.SelectedValue) = 10 Then
                    CampoTesta(I) = "PI"
                End If

                If TipoAnagrafica = "R" Or TipoAnagrafica = "C" Then
                    CampoTesta(I) = "SP"
                End If
            Else
                CampoTesta(I) = "NC"  ' Causale Documento
            End If
            If TipoAnagrafica = "C" Then
                I = I + 1 : CampoTesta(I) = AccodaSpazio(Comune.CONTOPERESATTO, 7)   'CoidceCliente
            End If
            If TipoAnagrafica = "R" Then
                I = I + 1 : CampoTesta(I) = AccodaSpazio(Regioni.CONTOPERESATTO, 7)   'CoidceCliente
            End If
            If TipoAnagrafica = "O" Then
                I = I + 1 : CampoTesta(I) = AccodaSpazio(Ospiti.CONTOPERESATTO, 7)   'CoidceCliente
            End If
            If TipoAnagrafica = "P" Then
                I = I + 1 : CampoTesta(I) = AccodaSpazio(Parente.CONTOPERESATTO, 7)   'CoidceCliente
            End If

            Dim Ente As Boolean
            Dim Piva As String
            Dim CodiceFiscale As String

            If TipoAnagrafica = "C" Then
                Piva = Comune.PartitaIVA
            End If
            If TipoAnagrafica = "R" Then
                Piva = Regioni.PARTITAIVA
            End If

            If TipoAnagrafica = "O" Then
                CodiceFiscale = Ospiti.CODICEFISCALE
            End If
            If TipoAnagrafica = "P" Then
                CodiceFiscale = Parente.CODICEFISCALE
            End If



            Ente = False

            If TipoAnagrafica = "O" Or TipoAnagrafica = "P" Then
                I = I + 1 : CampoTesta(I) = AccodaSpazio(CodiceFiscale, 16) 'CodiceFiscale
                I = I + 1 : CampoTesta(I) = Space(12) 'Partita IVA
                Ente = True
            Else
                I = I + 1 : CampoTesta(I) = Space(16) 'CodiceFiscale
                I = I + 1 : CampoTesta(I) = AccodaSpazio(Piva, 12) 'Partita IVA
                Ente = False
            End If


            If TipoAnagrafica = "C" Then
                I = I + 1 : CampoTesta(I) = AccodaSpazio(Comune.Descrizione, 40)   ' Nominativo/RagioneSociale
            End If
            If TipoAnagrafica = "R" Then
                I = I + 1 : CampoTesta(I) = AccodaSpazio(Regioni.Nome, 40)   ' Nominativo/RagioneSociale
            End If

            If TipoAnagrafica = "O" Then
                I = I + 1 : CampoTesta(I) = AccodaSpazio(Ospiti.Nome, 40)   ' Nominativo/RagioneSociale
            End If
            If TipoAnagrafica = "P" Then
                I = I + 1 : CampoTesta(I) = AccodaSpazio(Parente.Nome, 40)   ' Nominativo/RagioneSociale
            End If


            If TipoAnagrafica = "O" Or TipoAnagrafica = "P" Then
                I = I + 1 : CampoTesta(I) = AccodaSpazio("O", 3)   ' TipoCliente
            Else
                If TipoAnagrafica = "C" Then
                    I = I + 1 : CampoTesta(I) = AccodaSpazio("C", 3)
                End If
                If TipoAnagrafica = "R" Then
                    I = I + 1 : CampoTesta(I) = AccodaSpazio("R", 3)
                End If

            End If

            'If MoveFromDbWC(MyRs, "CodiceOspite") > 0 Then
            '   i = i + 1: CampoTesta(i) = AccodaSpazio("O", 3)   ' TipoCliente
            '  Else
            '   i = i + 1: CampoTesta(i) = AccodaSpazio("C", 3)   ' TipoCliente'
            'End If
            I = I + 1 : CampoTesta(I) = "   " ' Codice Pagamento
            I = I + 1 : CampoTesta(I) = "EUR" ' Codice Valuta
            I = I + 1 : CampoTesta(I) = PrimaSpazio("1936,27", 12)  ' Codice Valuta
            I = I + 1 : CampoTesta(I) = Space(5) ' Sconto Cliente
            I = I + 1 : CampoTesta(I) = Space(5) '  Sconto Pagamento
            I = I + 1 : CampoTesta(I) = Space(16) ' Spese Incasso
            I = I + 1 : CampoTesta(I) = AccodaSpazio(Registrazione.MeseCompetenza & "/" & Registrazione.AnnoCompetenza, 50)  ' Periodo Competenza

            I = I + 1 : CampoTesta(I) = PrimaSpazio(Replace(Math.Round(Imponibile, 2), ",", "."), 18) 'Imponibile1
            I = I + 1 : CampoTesta(I) = AccodaSpazio(Mid(CodiceIVA, 1, 2), 5) ' Codice IVA 1

            If Trim(CampoTesta(I)) = "04" Or Trim(CampoTesta(I)) = "4" Then
                CampoTesta(I) = "4SP  "
            End If
            If Trim(CampoTesta(I)) = "05" Then
                CampoTesta(I) = "5SP  "
            End If

            If CodiceIVA = "22" Then
                CampoTesta(I) = "22   "
            End If

            I = I + 1 : CampoTesta(I) = PrimaSpazio(Replace(Math.Round(Imposta, 2), ",", "."), 18) 'IVA
            I = I + 1 : CampoTesta(I) = PrimaSpazio("0", 18) 'Imponibile2
            I = I + 1 : CampoTesta(I) = "     " ' Codice IVA 2
            I = I + 1 : CampoTesta(I) = PrimaSpazio("0", 18) 'Imponibile2

            I = I + 1 : CampoTesta(I) = PrimaSpazio("0", 18) 'Imponibile3
            I = I + 1 : CampoTesta(I) = "     " ' Codice IVA 3
            I = I + 1 : CampoTesta(I) = PrimaSpazio("0", 18) 'Imponibile3

            I = I + 1 : CampoTesta(I) = PrimaSpazio("0", 18) 'Imponibile4
            I = I + 1 : CampoTesta(I) = "     " ' Codice IVA 4
            I = I + 1 : CampoTesta(I) = PrimaSpazio("0", 18) 'Imponibile4

            I = I + 1 : CampoTesta(I) = PrimaSpazio("0", 18) 'Imponibile5
            I = I + 1 : CampoTesta(I) = "     " ' Codice IVA 5
            I = I + 1 : CampoTesta(I) = PrimaSpazio("0", 18) 'Imponibile5


            Dim MYCENTROSERVIZIO As String

            MYCENTROSERVIZIO = AccodaSpazio(Registrazione.CentroServizio, 15) ' Rsa

            If Session("NomeEPersonam").ToString.ToUpper = "CHICCO" Then
                If Registrazione.CentroServizio = "0001" Then
                    MYCENTROSERVIZIO = AccodaSpazio("0101", 15)
                End If
                If Registrazione.CentroServizio = "0002" Then
                    MYCENTROSERVIZIO = AccodaSpazio("0102", 15)
                End If
            End If
            If Session("NomeEPersonam").ToString.ToUpper = "BELVEDERE" Then
                If Registrazione.CentroServizio = "0001" Then
                    MYCENTROSERVIZIO = AccodaSpazio("0201", 15)
                End If
            End If
            I = I + 1 : CampoTesta(I) = MYCENTROSERVIZIO

            Dim DataFattura As String
            Dim NumeroFattura As String
            Dim RegistroIva As String
            Dim Tipologia As String
            Dim Cig As String
            Dim RIFAMMIN As String
            Dim IdDocumento As String
            Dim RIFAMMINRIGAO As String


            DataFattura = Year(Registrazione.DataRegistrazione)
            NumeroFattura = Registrazione.NumeroProtocollo
            RegistroIva = Registrazione.RegistroIVA
            Tipologia = Registrazione.Tipologia

            Cig = ""
            RIFAMMIN = ""
            IdDocumento = ""
            RIFAMMINRIGAO = ""


            If Mid(Tipologia & Space(10), 1, 1) = "C" Then
                Cig = Comune.CodiceCig
                RIFAMMIN = Comune.RiferimentoAmministrazione
                IdDocumento = Comune.IdDocumento
            End If

            If Mid(Tipologia & Space(10), 1, 1) = "R" Then
                Cig = Regioni.CodiceCig
                RIFAMMIN = Regioni.RiferimentoAmministrazione
                IdDocumento = Regioni.IdDocumento
            End If

            I = I + 1 : CampoTesta(I) = AccodaSpazio(Cig, 15)
            I = I + 1 : CampoTesta(I) = AccodaSpazio(RIFAMMIN, 20)
            I = I + 1 : CampoTesta(I) = AccodaSpazio(IdDocumento, 20)
            I = I + 1 : CampoTesta(I) = AccodaSpazio(RIFAMMINRIGAO, 20)


            Stringa = ""
            For Z = 1 To I
                Stringa = Stringa & CampoTesta(Z) '& vbTab
            Next Z
            FileTesta.WriteLine(Stringa)


            REM Esporta Riga Esatto
            I = 0
            I = I + 1 : CampoRiga(I) = Format(Registrazione.DataRegistrazione, "yyyyMMdd")  ' Data Documento
            I = I + 1 : CampoRiga(I) = PrimaSpazio(Format(Registrazione.NumeroProtocollo, "#"), 6) ' Numero Documento

            I = I + 1
            If UCase(CausaleContabile.TipoDocumento) = UCase("RE") Or UCase(CausaleContabile.TipoDocumento) = UCase("FT") Then
                CampoRiga(I) = "FA"
                If TipoAnagrafica = "C" Or TipoAnagrafica = "R" Then
                    CampoRiga(I) = "SP"  ' Causale Documento
                End If
            Else
                CampoRiga(I) = "NC"  ' Causale Documento
            End If

            If TipoAnagrafica = "O" Or TipoAnagrafica = "P" Then
                I = I + 1 : CampoRiga(I) = AccodaSpazio("O", 3)   ' TipoCliente
            Else
                I = I + 1 : CampoRiga(I) = AccodaSpazio(TipoAnagrafica, 3)
            End If


            MYCENTROSERVIZIO = AccodaSpazio(Registrazione.CentroServizio, 15) ' Rsa
            If Session("NomeEPersonam").ToString.ToUpper = "CHICCO" Then
                If Registrazione.CentroServizio = "0001" Then
                    MYCENTROSERVIZIO = AccodaSpazio("0101", 15)
                End If
                If Registrazione.CentroServizio = "0002" Then
                    MYCENTROSERVIZIO = AccodaSpazio("0102", 15)
                End If
            End If
            If Session("NomeEPersonam").ToString.ToUpper = "BELVEDERE" Then
                If Registrazione.CentroServizio = "0001" Then
                    MYCENTROSERVIZIO = AccodaSpazio("0201", 15)
                End If
            End If
            I = I + 1 : CampoRiga(I) = MYCENTROSERVIZIO

            'I = I + 1: CampoRiga(I) = AccodaSpazio(CodiceCentroServizio(MoveFromDbWC(MyRs, "CentroServizio")), 15)  ' Rsa
            I = I + 1 : CampoRiga(I) = AccodaSpazio(" ", 15) ' Articolo

            If TipoAnagrafica = "O" Or TipoAnagrafica = "P" Then
                If TipoAnagrafica = "O" Then
                    I = I + 1 : CampoRiga(I) = AccodaSpazio("Retta di " & Ospiti.Nome, 40) ' Descrizione
                End If
                If TipoAnagrafica = "P" Then
                    Dim MyOspiti As New ClsOspite
                    MyOspiti.CodiceOspite = Parente.CodiceOspite
                    MyOspiti.Leggi(Session("DC_OSPITE"), MyOspiti.CodiceOspite)

                    I = I + 1 : CampoRiga(I) = AccodaSpazio("Retta di " & MyOspiti.Nome, 40) ' Descrizione
                End If
            Else
                If TipoAnagrafica = "R" Then
                    I = I + 1 : CampoRiga(I) = AccodaSpazio("Retta di " & Regioni.Nome, 40) ' Descrizione
                End If
                If TipoAnagrafica = "C" Then
                    I = I + 1 : CampoRiga(I) = AccodaSpazio("Retta di " & Comune.Descrizione, 40) ' Descrizione
                End If
            End If


            I = I + 1 : CampoRiga(I) = AccodaSpazio("Del Mese di " & Registrazione.MeseCompetenza & "/" & Registrazione.AnnoCompetenza, 40)  ' Descrizione Aggiuntiva
            I = I + 1 : CampoRiga(I) = "  " ' Unità di Misura
            I = I + 1 : CampoRiga(I) = PrimaSpazio("1", 12) 'Q.ta
            I = I + 1 : CampoRiga(I) = PrimaSpazio(Replace(Math.Round(Imponibile, 2), ",", "."), 15) 'Prezzo Unitario
            I = I + 1 : CampoRiga(I) = " " ' Flag Omaggio
            I = I + 1 : CampoRiga(I) = AccodaSpazio(Mid(CodiceIVA, 1, 2), 5) ' Codice IVA
            If CodiceIVA = "22" Then
                CampoRiga(I) = "22   "
            End If
            If TipoAnagrafica = "R" Or TipoAnagrafica = "C" Then
                If Trim(CampoRiga(I)) = "04" Or Trim(CampoRiga(I)) = "4" Then
                    CampoRiga(I) = "4SP  "
                End If
                If Trim(CampoRiga(I)) = "05" Then
                    CampoRiga(I) = "5SP  "
                End If
            End If


            I = I + 1 : CampoRiga(I) = PrimaSpazio(Replace(Math.Round(Imposta, 2), ",", "."), 18) 'IVA


            If TipoAnagrafica = "C" And Comune.PERIODO = "B" Then
                I = I + 1 : CampoRiga(I) = Format(DateSerial(Txt_Anno.Text, Mese - 1, 1), "yyyyMMdd") ' Data Documento
                I = I + 1 : CampoRiga(I) = Format(DateSerial(Txt_Anno.Text, Mese, GiorniMese(Mese, Txt_Anno.Text)), "yyyyMMdd")  ' Data Documento
            Else
                I = I + 1 : CampoRiga(I) = Format(DateSerial(Txt_Anno.Text, Mese, 1), "yyyyMMdd") ' Data Documento
                I = I + 1 : CampoRiga(I) = Format(DateSerial(Txt_Anno.Text, Mese, GiorniMese(Mese, Txt_Anno.Text)), "yyyyMMdd") ' Data Documento
            End If


            Stringa = ""
            For Z = 1 To I
                Stringa = Stringa & CampoRiga(Z) '& vbTab
            Next Z
            FileRiga.WriteLine(Stringa)
        Loop
        FileTesta.Close()
        FileRiga.Close()


        'NomeFile = HostingEnvironment.ApplicationPhysicalPath() & "\Public\Esporta.Tfa"
        'NomeFileRiga = HostingEnvironment.ApplicationPhysicalPath() & "\Public\Esporta.Rfa"

        Btn_Testa.Visible = True
        Btn_Riga.Visible = True
    End Sub

    Protected Sub btn_riga_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles Btn_Riga.Click
        Dim NomeFile As String
        Dim NomeFileRiga As String


        If RB_AdHoc.Checked = True Then
            NomeFile = HostingEnvironment.ApplicationPhysicalPath() & "\Public\Esporta.Tfa"
            NomeFileRiga = HostingEnvironment.ApplicationPhysicalPath() & "\Public\Esporta.Rfa"
            Btn_Riga.Visible = False
            Response.Clear()
            Response.Buffer = True
            Response.ContentType = "text/plain"
            Response.AppendHeader("content-disposition", "attachment;filename=Esporta.Rfa")
            Response.WriteFile(NomeFileRiga)
            Response.Flush()
            Response.End()
        End If


        If RB_IPSOA.Checked = True Then
            NomeFileRiga = HostingEnvironment.ApplicationPhysicalPath() & "\Public\Clienti_.TXT"


            Response.Clear()
            Response.Buffer = True
            Response.ContentType = "text/plain"
            Response.AppendHeader("content-disposition", "attachment;filename=Clienti_.TXT")
            Response.WriteFile(NomeFileRiga)
            Response.Flush()
            Response.End()

        End If



        If RB_Ats.Checked = True Then
            NomeFileRiga = HostingEnvironment.ApplicationPhysicalPath() & "\Public\Farmaci_.txt"


            Response.Clear()
            Response.Buffer = True
            Response.ContentType = "text/plain"
            Response.AppendHeader("content-disposition", "attachment;filename=Farmaci_.txt")
            Response.WriteFile(NomeFileRiga)
            Response.Flush()
            Response.End()

            Kill(NomeFileRiga)
        End If



    End Sub


    Protected Sub btn_testa_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles Btn_Testa.Click

        Dim NomeFile As String
        Dim NomeFileRiga As String

        If RB_AdHoc.Checked = True Then
            NomeFile = HostingEnvironment.ApplicationPhysicalPath() & "\Public\Esporta.Tfa"
            NomeFileRiga = HostingEnvironment.ApplicationPhysicalPath() & "\Public\Esporta.Rfa"

            Response.Clear()
            Response.Buffer = True
            Response.ContentType = "text/plain"
            Response.AppendHeader("content-disposition", "attachment;filename=Esporta.Tfa")
            Response.WriteFile(NomeFile)
            Response.Flush()
            Response.End()

            Kill(NomeFile)
        End If

        If RB_IPSOA.Checked = True Then
            NomeFile = HostingEnvironment.ApplicationPhysicalPath() & "\Public\Movim_.Txt"

            Response.Clear()
            Response.Buffer = True
            Response.ContentType = "text/plain"
            Response.AppendHeader("content-disposition", "attachment;filename=Movim_.Txt")
            Response.WriteFile(NomeFile)
            Response.Flush()
            Response.End()

            Kill(NomeFile)
        End If




        If RB_Ats.Checked = True Then
            NomeFile = HostingEnvironment.ApplicationPhysicalPath() & "\Public\Movim_.Txt"

            Response.Clear()
            Response.Buffer = True
            Response.ContentType = "text/plain"
            Response.AppendHeader("content-disposition", "attachment;filename=Movim_.Txt")
            Response.WriteFile(NomeFile)
            Response.Flush()
            Response.End()

            Kill(NomeFile)
        End If
    End Sub



    Private Function AccodaSpazio(ByVal Testo As String, ByVal Lunghezza As Long) As String
        If Len(Testo) < Lunghezza Then
            AccodaSpazio = Testo & Space(Lunghezza - Len(Testo))
        Else
            AccodaSpazio = Mid(Testo, 1, Lunghezza)
        End If

    End Function


    Private Function PrimaSpazio(ByVal Testo As String, ByVal Lunghezza As Long) As String
        If Len(Testo) < Lunghezza Then
            PrimaSpazio = Space(Lunghezza - Len(Testo)) & Testo
        Else
            PrimaSpazio = Mid(Testo, 1, Lunghezza)
        End If
    End Function



    Sub Export_Aliante()

        Dim cn As OleDbConnection

        Dim MeseContr As Long
        Dim MySql As String
        Dim ProgressivoAssoluto As Integer = 0
        Dim Ragguppato As Boolean = False
        Dim ErroriEsportazione As String = ""
        Dim Anno As Integer
        Dim Mese As Integer

        Dim LeggiParametri As New Cls_Parametri


        LeggiParametri.LeggiParametri(Session("DC_OSPITE"))

        Anno = Txt_Anno.Text
        Mese = DD_Mese.SelectedValue

        MeseContr = DD_Mese.SelectedValue


        MySql = "SELECT * " & _
                " FROM MovimentiContabiliTesta " & _
                " WHERE AnnoProtocollo = " & Txt_AnnoRif.Text

        If DD_Registro.SelectedValue <> "" Then
            MySql = MySql & " AND MovimentiContabiliTesta.RegistroIva = " & DD_Registro.SelectedValue
        End If
        If Tab_PeriodoData.ActiveTabIndex = 1 And IsDate(Txt_DataAl1.Text) And IsDate(Txt_DataDal1.Text) Then
            MySql = MySql & " AND (DataRegistrazione >= ? And DataRegistrazione <= ?) "
        Else
            MySql = MySql & " And MovimentiContabiliTesta.AnnoCompetenza = " & Txt_Anno.Text & " AND MovimentiContabiliTesta.MeseCompetenza = " & MeseContr & " "
        End If
        MySql = MySql & " AND (NumeroProtocollo >= " & Txt_DalDocumento.Text & " And NumeroProtocollo <= " & Txt_AlDocumento.Text & ") "

        cn = New Data.OleDb.OleDbConnection(Session("DC_GENERALE"))

        cn.Open()
        Dim cmd As New OleDbCommand()
        cmd.CommandText = MySql & " Order by NumeroProtocollo"
        cmd.Connection = cn
        If Tab_PeriodoData.ActiveTabIndex = 1 And IsDate(Txt_DataAl1.Text) And IsDate(Txt_DataDal1.Text) Then
            cmd.Parameters.AddWithValue("@DataDal", Txt_DataDal1.Text)
            cmd.Parameters.AddWithValue("@DataDal", Txt_DataAl1.Text)
        End If


        Dim CodiceInstallazione As String
        Dim CodiceDitta As Integer
        Dim ProgressivoTesta As Integer
        Dim RegistroIva As Integer
        CodiceInstallazione = 1
        CodiceDitta = 1

        RegistroIva = DD_Registro.SelectedValue
        Dim NomeFile As String
        'Dim NomeFileRiga As String
        NomeFile = HostingEnvironment.ApplicationPhysicalPath() & "\Public\" & Session("NomeEPersonam") & "\Esportazione_" & Session("NomeEPersonam") & "_" & RegistroIva & ".Txt"
        'NomeFileRiga = HostingEnvironment.ApplicationPhysicalPath() & "\Riga_" & CodiceDitta & "_" & Format(Now, "yyyyMMddHHmm") & ".Txt"

        Dim tw As System.IO.TextWriter = System.IO.File.CreateText(NomeFile)
        'Dim twR As System.IO.TextWriter = System.IO.File.CreateText(NomeFileRiga)


        Dim Fnvb6 As New Cls_FunzioniVB6
        Fnvb6.StringaOspiti = Session("DC_OSPITE")
        Fnvb6.StringaTabelle = Session("DC_TABELLE")
        Fnvb6.StringaGenerale = Session("DC_GENERALE")
        Fnvb6.ApriDB()


        Dim AppoggioRiga As String = ""

        AppoggioRiga = AppoggioRiga & "DO_TIPO_RIGA" & ";"
        AppoggioRiga = AppoggioRiga & "DO30_ID" & ";"
        AppoggioRiga = AppoggioRiga & "DO30_ID_SEN" & ";"
        AppoggioRiga = AppoggioRiga & "DO30_ID_DO11" & ";"
        AppoggioRiga = AppoggioRiga & "DO30_ID_DO11_SEN" & ";"
        AppoggioRiga = AppoggioRiga & "DO30_CODICE_INS" & ";"
        AppoggioRiga = AppoggioRiga & "DO30_DITTA_CG18" & ";"
        AppoggioRiga = AppoggioRiga & "DO30_PROGRIGA" & ";"
        AppoggioRiga = AppoggioRiga & "DO30_INDTIPORIGA" & ";"
        AppoggioRiga = AppoggioRiga & "DO30_CODART_MG66" & ";"
        AppoggioRiga = AppoggioRiga & "DO30_CODART_MG66_SEN" & ";"
        AppoggioRiga = AppoggioRiga & "DO30_OPZIONE_MG5E" & ";"
        AppoggioRiga = AppoggioRiga & "DO30_DESCART" & ";"
        AppoggioRiga = AppoggioRiga & "DO30_DESCART_SEN" & ";"
        AppoggioRiga = AppoggioRiga & "DO30_UM1" & ";"
        AppoggioRiga = AppoggioRiga & "DO30_QTA1" & ";"
        AppoggioRiga = AppoggioRiga & "DO30_COLLI" & ";"
        AppoggioRiga = AppoggioRiga & "DO30_PREZZO" & ";"
        AppoggioRiga = AppoggioRiga & "DO30_CODIVA_CODICE_SEN" & ";"
        AppoggioRiga = AppoggioRiga & "DO30_CODIVA_NOME_SEN" & ";"


        'twR.WriteLine(AppoggioRiga)


        Dim Appoggio As String = ""

        Appoggio = Appoggio & "DO_TIPO_RIGA" & ";"
        Appoggio = Appoggio & "DO11_ID" & ";"
        Appoggio = Appoggio & "DO11_NUMREG_SEN" & ";"
        Appoggio = Appoggio & "DO11_CODICE_INS" & ";"
        Appoggio = Appoggio & "DO11_DITTA_CG18" & ";"
        Appoggio = Appoggio & "DO11_DOCUM_MG36" & ";"
        Appoggio = Appoggio & "DO11_NUMDOC" & ";"
        Appoggio = Appoggio & "DO11_DATADOC" & ";"
        Appoggio = Appoggio & "DO11_TIPOCF_CG44" & ";"
        Appoggio = Appoggio & "DO11_CLIFOR_CG44" & ";"
        Appoggio = Appoggio & "DO11_CONTOCLI_SEN" & ";"

        Appoggio = Appoggio & "DO11_RAGIONESOCIALE" & ";"
        Appoggio = Appoggio & "DO11_PIVA" & ";"
        Appoggio = Appoggio & "DO11_CF" & ";"
        Appoggio = Appoggio & "DO11_INDIRIZZO" & ";"
        Appoggio = Appoggio & "DO11_CITTA_NOME" & ";"
        Appoggio = Appoggio & "DO11_CITTA_ISTAT" & ";"
        Appoggio = Appoggio & "DO11_PROVINCIA"
        Appoggio = Appoggio & "DO11_BANCA_CIN" & ";"
        Appoggio = Appoggio & "DO11_BANCA_ABI" & ";"
        Appoggio = Appoggio & "DO11_BANCA_CAB" & ";"
        Appoggio = Appoggio & "DO11_BANCA_CONTO" & ";"
        Appoggio = Appoggio & "DO11_BANCA_IBAN" & ";"
        Appoggio = Appoggio & "DO11_CONDPAG_CODICE_SEN" & ";"
        Appoggio = Appoggio & "DO11_CONDPAG_NOME_SEN" & ";"


        'tw.WriteLine(Appoggio)
        Dim CodiceRegistro As String = ""

        If Session("NomeEPersonam").ToString.ToUpper = "NAVIGLIO" Then
            CodiceInstallazione = "02"
            CodiceDitta = "100"
            CodiceRegistro = "20"
            If RegistroIva = 3 Then
                CodiceRegistro = "2P"
            End If
        End If

        If Session("NomeEPersonam").ToString.ToUpper = "BAGGIO" Or Session("NomeEPersonam").ToString.ToUpper.IndexOf("PARCO") >= 0 Then
            CodiceInstallazione = "04"
            CodiceDitta = "100"

            If RegistroIva > 3 Then
                CodiceRegistro = "40"
            Else
                CodiceRegistro = "30"
            End If

            If RegistroIva = 3 Then
                CodiceRegistro = "4P"
            End If
            If RegistroIva = 7 Then
                CodiceRegistro = "3P"
            End If
        End If
        If Session("NomeEPersonam").ToString.ToUpper = "RSA CASA SS CONCEZIONE" Then
            CodiceInstallazione = "05"
            CodiceDitta = "100"
            CodiceRegistro = "50"

            If RegistroIva = 3 Then
                CodiceRegistro = "5P"
            End If
        End If
        If Session("NomeEPersonam").ToString.ToUpper = "DOMIZIANA" Then
            CodiceInstallazione = "06"
            CodiceDitta = "100"

            CodiceRegistro = "60"
            If RegistroIva = 3 Then
                CodiceRegistro = "6P"
            End If
        End If
        If Session("NomeEPersonam").ToString.ToUpper = "ZUCCHI FALCINA" Then
            CodiceInstallazione = "07"
            CodiceDitta = "100"
            CodiceRegistro = "70"

            If RegistroIva = 3 Then
                CodiceRegistro = "7P"
            End If
        End If

        If Session("NomeEPersonam").ToString.ToUpper = "MEZZALUNA" Then
            CodiceInstallazione = "08"
            CodiceDitta = "100"

            CodiceRegistro = "80"

            If RegistroIva = 3 Then
                CodiceRegistro = "8P"
            End If
        End If

        If Session("NomeEPersonam").ToString.ToUpper = "CARPANEDA" Then
            CodiceInstallazione = "09"
            CodiceDitta = "100"

            CodiceRegistro = "90"

            If RegistroIva = 5 Then
                CodiceRegistro = "91"
            End If
            If RegistroIva = 3 Then
                CodiceRegistro = "9P"
            End If
        End If


        ProgressivoTesta = 0

        Dim myPOSTreader As OleDbDataReader = cmd.ExecuteReader()
        Do While myPOSTreader.Read
            ProgressivoTesta = ProgressivoTesta + 1
            Appoggio = "T" & ";"  'DO_TIPO_RIGA
            Appoggio = Appoggio & ProgressivoTesta & ";"  'DO11_ID
            Appoggio = Appoggio & campodbN(myPOSTreader.Item("NumeroRegistrazione")) & ";" 'DO11_NUMREG_SEN


            Appoggio = Appoggio & CodiceInstallazione & ";" 'DO11_CODICE_INS

            Appoggio = Appoggio & CodiceDitta & ";" 'DO11_DITTA_CG18
            Appoggio = Appoggio & "" & ";" 'TIPO DOCUMENTO NON IN USO
            Appoggio = Appoggio & campodb(myPOSTreader.Item("NumeroDocumento")) & ";" 'DO11_NUMDOC
            Appoggio = Appoggio & campodb(myPOSTreader.Item("DataDocumento")) & ";" 'DO11_DATADOC
            Appoggio = Appoggio & "0" & ";" 'DO11_TIPOCF_CG44 -  0=  CLIENTE

            Dim CausaleContabile As New Cls_CausaleContabile

            CausaleContabile.Codice = campodb(myPOSTreader.Item("CAUSALECONTABILE"))
            CausaleContabile.Leggi(Session("DC_TABELLE"), CausaleContabile.Codice)



            Dim Cliente As String = ""

            Dim Tipo As String = campodb(myPOSTreader.Item("Tipologia"))
            Dim CodiceOspite As Integer = 0
            Dim CodiceParente As Integer = 0

            Dim CodiceProvincia As String = ""
            Dim CodiceComune As String = ""
            Dim CodiceRegione As String = ""


            Dim cmdRd As New OleDbCommand()
            cmdRd.CommandText = "Select * From MovimentiContabiliRiga Where Numero = ?  And Tipo ='CF'   "
            cmdRd.Connection = cn
            cmdRd.Parameters.AddWithValue("@Numero", campodbN(myPOSTreader.Item("NumeroRegistrazione")))
            Dim MyReadSC As OleDbDataReader = cmdRd.ExecuteReader()
            If MyReadSC.Read Then
                Cliente = campodb(MyReadSC.Item("MastroPartita")) & "." & campodb(MyReadSC.Item("ContoPartita")) & "." & campodb(MyReadSC.Item("SottocontoPartita"))
                If Mid(Tipo & Space(10), 1, 1) = " " Then
                    CodiceOspite = Int(campodbN(MyReadSC.Item("SottocontoPartita")) / 100)
                    CodiceParente = campodbN(MyReadSC.Item("SottocontoPartita")) - (CodiceOspite * 100)

                    If CodiceParente = 0 And CodiceOspite > 0 Then
                        Tipo = "O" & Space(10)
                    End If
                    If CodiceParente > 0 And CodiceOspite > 0 Then
                        Tipo = "P" & Space(10)
                    End If
                End If
                If Mid(Tipo & Space(10), 1, 1) = "C" Then
                    CodiceProvincia = Mid(Tipo & Space(10), 2, 3)
                    CodiceComune = Mid(Tipo & Space(10), 5, 3)
                End If
                If Mid(Tipo & Space(10), 1, 1) = "J" Then
                    CodiceProvincia = Mid(Tipo & Space(10), 2, 3)
                    CodiceComune = Mid(Tipo & Space(10), 5, 3)
                End If
                If Mid(Tipo & Space(10), 1, 1) = "R" Then
                    CodiceRegione = Mid(Tipo & Space(10), 2, 4)
                End If

            End If
            MyReadSC.Close()

            Appoggio = Appoggio & "" & ";" 'DO11_CLIFOR_CG44 CODICE CLIENTE NON IN USO
            Appoggio = Appoggio & Cliente & ";" 'CODICE CLIENTE DI SENIOR
            Dim DO11_RAGIONESOCIALE As String = ""
            Dim DO11_PIVA As String = ""
            Dim DO11_CF As String = ""
            Dim DO11_INDIRIZZO As String = ""
            Dim DO11_CAP As String
            Dim DO11_CITTA_NOME As String = ""
            Dim DO11_CITTA_ISTAT As String = ""
            Dim DO11_CITTA_CATASTALE As String = ""
            Dim DO11_PROVINCIA As String = ""
            Dim DO11_CONDPAG_CODICE_SEN As String = ""
            Dim DO11_CONDPAG_NOME_SEN As String = ""
            Dim DO11_BANCA_CIN As String = ""
            Dim DO11_BANCA_ABI As String = ""
            Dim DO11_BANCA_CAB As String = ""
            Dim DO11_BANCA_CONTO As String = ""
            Dim DO11_BANCA_IBAN As String = ""
            Dim DO11_BANCA_IDMANDATO As String = ""
            Dim DO11_BANCA_DATAMANDATO As String = ""
            Dim DO11_INTESTATARIOSDD As String = ""
            Dim DO11_INTESTATARIOCFSDD As String = ""

            Dim DO11_COGNOMENOME_OSPITERIFERIMENO As String = ""
            Dim DO11_CODICEFISCALE_OSPITERIFERIMENO As String = ""
            Dim DO11_DATANASCITA_OSPITERIFERIMENO As String = ""

            Dim DO11_COGNOMENOME_DESTINATARIO As String = ""
            Dim DO11_INDIRIZZO_DESTINATARIO As String = ""
            Dim DO11_COMUNE_DESTINATARIO As String = ""
            Dim DO11_COMUNECOD_DESTINATARIO As String = ""
            Dim DO11_CAP_DESTINATARIO As String = ""
            Dim DO11_CITTA_DESTINATARIO As String = ""
            Dim DO11_MAIL_DESTINATARIO As String = ""






            Dim AddDescrizione As String


            Dim CIG As String = ""
            Dim IDDocumento As String = ""
            Dim IDDocumentoData As String = ""
            Dim CodiceDestinatario As String = ""
            Dim RiferimentoAmministrazione As String = ""

            AddDescrizione = ""
            If Mid(Tipo & Space(10), 1, 1) = "O" Then
                Dim Ospite As New ClsOspite

                Ospite.CodiceOspite = CodiceOspite
                Ospite.Leggi(Session("DC_OSPITE"), Ospite.CodiceOspite)
                DO11_RAGIONESOCIALE = Ospite.Nome
                DO11_PIVA = ""
                DO11_CF = Ospite.CODICEFISCALE

                Dim VerificaCodiceFiscale As New Cls_CodiceFiscale

                If VerificaCodiceFiscale.Check_CodiceFiscale(DO11_CF) = False Then
                    ErroriEsportazione = ErroriEsportazione & " Codice Fiscale formalmente errato per ospite " & DO11_RAGIONESOCIALE & "<br/>"
                End If

                DO11_INDIRIZZO = Ospite.RESIDENZAINDIRIZZO1


                Dim DcCom As New ClsComune

                DcCom.Provincia = Ospite.RESIDENZAPROVINCIA1
                DcCom.Comune = Ospite.RESIDENZACOMUNE1
                DcCom.Leggi(Session("DC_OSPITE"))

                DO11_CITTA_NOME = DcCom.Descrizione
                DO11_CITTA_ISTAT = DcCom.Provincia & DcCom.Comune
                DO11_CITTA_CATASTALE = DcCom.CODXCODF

                Dim DcProv As New ClsComune

                DcProv.Provincia = Ospite.RESIDENZAPROVINCIA1
                DcProv.Comune = ""
                DcProv.Leggi(Session("DC_OSPITE"))
                DO11_PROVINCIA = DcProv.CodificaProvincia


                Dim Kl As New Cls_DatiOspiteParenteCentroServizio

                Kl.CodiceOspite = CodiceOspite
                Kl.CodiceParente = 0
                Kl.CentroServizio = campodb(myPOSTreader.Item("CentroServizio"))
                Kl.Leggi(Session("DC_OSPITE"))
                If Kl.ModalitaPagamento <> "" Then
                    Ospite.MODALITAPAGAMENTO = Kl.ModalitaPagamento
                End If


                DO11_CONDPAG_CODICE_SEN = Ospite.MODALITAPAGAMENTO
                Dim MPAg As New ClsModalitaPagamento

                MPAg.Codice = Ospite.MODALITAPAGAMENTO
                MPAg.Leggi(Session("DC_OSPITE"))
                DO11_CONDPAG_NOME_SEN = MPAg.DESCRIZIONE


                Dim ModPag As New Cls_DatiPagamento

                ModPag.CodiceOspite = Ospite.CodiceOspite
                ModPag.CodiceParente = 0
                ModPag.LeggiUltimaDataData(Session("DC_OSPITE"), campodb(myPOSTreader.Item("DataDocumento")))

                DO11_BANCA_CIN = ModPag.Cin
                DO11_BANCA_ABI = ModPag.Abi
                DO11_BANCA_CAB = ModPag.Cab
                DO11_BANCA_CONTO = ModPag.CCBancario
                DO11_BANCA_IBAN = ModPag.CodiceInt & Format(Val(ModPag.NumeroControllo), "00") & ModPag.Cin & Format(Val(ModPag.Abi), "00000") & Format(Val(ModPag.Cab), "00000") & AdattaLunghezzaNumero(ModPag.CCBancario, 12)



                DO11_BANCA_IDMANDATO = ModPag.IdMandatoDebitore
                DO11_BANCA_DATAMANDATO = ModPag.DataMandatoDebitore
                DO11_INTESTATARIOSDD = ModPag.IntestatarioConto
                DO11_INTESTATARIOCFSDD = ModPag.IntestatarioCFConto


                DO11_COGNOMENOME_OSPITERIFERIMENO = Ospite.Nome
                DO11_CODICEFISCALE_OSPITERIFERIMENO = Ospite.CODICEFISCALE
                DO11_DATANASCITA_OSPITERIFERIMENO = Ospite.DataNascita

                DO11_COGNOMENOME_DESTINATARIO = Ospite.RecapitoNome
                DO11_INDIRIZZO_DESTINATARIO = Ospite.RecapitoIndirizzo

                Dim MInt As New ClsComune

                MInt.Provincia = Ospite.RecapitoProvincia
                MInt.Comune = Ospite.RecapitoComune
                MInt.Leggi(Session("DC_OSPITE"))

                DO11_COMUNE_DESTINATARIO = MInt.Descrizione
                DO11_COMUNECOD_DESTINATARIO = Ospite.RecapitoProvincia & Ospite.RecapitoComune





                DO11_CAP_DESTINATARIO = Ospite.RESIDENZACAP4
                DO11_MAIL_DESTINATARIO = Ospite.TELEFONO4

                Dim cnOspiti As OleDbConnection

                cnOspiti = New Data.OleDb.OleDbConnection(Session("DC_OSPITE"))

                cnOspiti.Open()

                Dim cmdC As New OleDbCommand()
                cmdC.CommandText = ("select * from NoteFatture where [OspitiParentiComuniRegioni] = 'O'")
                cmdC.Connection = cnOspiti
                Dim RdCom As OleDbDataReader = cmdC.ExecuteReader()
                If RdCom.Read Then
                    AddDescrizione = campodb(RdCom.Item("NoteUp"))
                End If
                RdCom.Close()
                cnOspiti.Close()

            End If

            If Mid(Tipo & Space(10), 1, 1) = "P" Then
                Dim Ospite As New Cls_Parenti

                Ospite.CodiceOspite = CodiceOspite
                Ospite.CodiceParente = CodiceParente
                Ospite.Leggi(Session("DC_OSPITE"), Ospite.CodiceOspite, Ospite.CodiceParente)
                DO11_RAGIONESOCIALE = Ospite.Nome
                DO11_PIVA = ""
                DO11_CF = Ospite.CODICEFISCALE
                DO11_INDIRIZZO = Ospite.RESIDENZAINDIRIZZO1

                Dim VerificaCodiceFiscale As New Cls_CodiceFiscale



                If VerificaCodiceFiscale.Check_CodiceFiscale(DO11_CF) = False Then
                    ErroriEsportazione = ErroriEsportazione & " Codice Fiscale formalmente errato per parente " & DO11_RAGIONESOCIALE & "<br/>"
                End If

                Dim DcCom As New ClsComune

                DcCom.Provincia = Ospite.RESIDENZAPROVINCIA1
                DcCom.Comune = Ospite.RESIDENZACOMUNE1
                DcCom.Leggi(Session("DC_OSPITE"))

                DO11_CITTA_NOME = DcCom.Descrizione
                DO11_CITTA_ISTAT = DcCom.Provincia & DcCom.Comune
                DO11_CITTA_CATASTALE = DcCom.CODXCODF

                Dim DcProv As New ClsComune

                DcProv.Provincia = Ospite.RESIDENZAPROVINCIA1
                DcProv.Comune = ""
                DcProv.Leggi(Session("DC_OSPITE"))
                DO11_PROVINCIA = DcProv.CodificaProvincia
                DO11_CONDPAG_CODICE_SEN = Ospite.MODALITAPAGAMENTO

                Dim Kl As New Cls_DatiOspiteParenteCentroServizio

                Kl.CodiceOspite = CodiceOspite
                Kl.CodiceParente = CodiceParente
                Kl.CentroServizio = campodb(myPOSTreader.Item("CentroServizio"))
                Kl.Leggi(Session("DC_OSPITE"))
                If Kl.ModalitaPagamento <> "" Then
                    Ospite.MODALITAPAGAMENTO = Kl.ModalitaPagamento
                End If

                Dim MPAg As New ClsModalitaPagamento

                MPAg.Codice = Ospite.MODALITAPAGAMENTO
                MPAg.Leggi(Session("DC_OSPITE"))
                DO11_CONDPAG_NOME_SEN = MPAg.DESCRIZIONE

                Dim ModPag As New Cls_DatiPagamento

                ModPag.CodiceOspite = Ospite.CodiceOspite
                ModPag.CodiceParente = Ospite.CodiceParente
                ModPag.LeggiUltimaDataData(Session("DC_OSPITE"), campodb(myPOSTreader.Item("DataDocumento")))

                DO11_BANCA_CIN = ModPag.Cin
                DO11_BANCA_ABI = ModPag.Abi
                DO11_BANCA_CAB = ModPag.Cab
                DO11_BANCA_CONTO = ModPag.CCBancario
                DO11_BANCA_IBAN = ModPag.CodiceInt & Format(Val(ModPag.NumeroControllo), "00") & ModPag.Cin & Format(Val(ModPag.Abi), "00000") & Format(Val(ModPag.Cab), "00000") & AdattaLunghezzaNumero(ModPag.CCBancario, 12)

                DO11_BANCA_IDMANDATO = ModPag.IdMandatoDebitore
                DO11_BANCA_DATAMANDATO = ModPag.DataMandatoDebitore
                DO11_INTESTATARIOSDD = ModPag.IntestatarioConto
                DO11_INTESTATARIOCFSDD = ModPag.IntestatarioCFConto



                Dim OspiteReale As New ClsOspite

                OspiteReale.CodiceOspite = Ospite.CodiceOspite
                OspiteReale.Leggi(Session("DC_OSPITE"), OspiteReale.CodiceOspite)


                DO11_COGNOMENOME_OSPITERIFERIMENO = OspiteReale.Nome
                DO11_CODICEFISCALE_OSPITERIFERIMENO = OspiteReale.CODICEFISCALE
                DO11_DATANASCITA_OSPITERIFERIMENO = OspiteReale.DataNascita

                DO11_COGNOMENOME_DESTINATARIO = Ospite.RECAPITONOME
                DO11_INDIRIZZO_DESTINATARIO = Ospite.RECAPITOINDIRIZZO


                Dim MInt As New ClsComune

                MInt.Provincia = Ospite.RECAPITOPROVINCIA
                MInt.Comune = Ospite.RECAPITOCOMUNE
                MInt.Leggi(Session("DC_OSPITE"))

                DO11_COMUNE_DESTINATARIO = MInt.Descrizione
                DO11_COMUNECOD_DESTINATARIO = Ospite.RECAPITOPROVINCIA & Ospite.RECAPITOCOMUNE

                DO11_CAP_DESTINATARIO = Ospite.RESIDENZACAP4
                DO11_MAIL_DESTINATARIO = Ospite.Telefono4

                Dim cnOspiti As OleDbConnection

                cnOspiti = New Data.OleDb.OleDbConnection(Session("DC_OSPITE"))

                cnOspiti.Open()

                Dim cmdC As New OleDbCommand()
                cmdC.CommandText = ("select * from NoteFatture where [OspitiParentiComuniRegioni] = 'O'")
                cmdC.Connection = cnOspiti
                Dim RdCom As OleDbDataReader = cmdC.ExecuteReader()
                If RdCom.Read Then
                    AddDescrizione = campodb(RdCom.Item("NoteUp"))
                End If
                RdCom.Close()
                cnOspiti.Close()

            End If
            If Mid(Tipo & Space(10), 1, 1) = "C" Then
                Dim Ospite As New ClsComune

                Ospite.Provincia = CodiceProvincia
                Ospite.Comune = CodiceComune
                Ospite.Leggi(Session("DC_OSPITE"))
                DO11_RAGIONESOCIALE = Ospite.Descrizione
                DO11_PIVA = Ospite.PartitaIVA
                DO11_CF = Ospite.CodiceFiscale
                DO11_INDIRIZZO = Ospite.RESIDENZAINDIRIZZO1


                Dim VerificaPIVA As New Cls_CodiceFiscale



                If VerificaPIVA.CheckPartitaIva(AdattaLunghezzaNumero(DO11_PIVA, 11)) = False Then
                    ErroriEsportazione = ErroriEsportazione & " Partita Iva formalmente errato per ente " & DO11_RAGIONESOCIALE & " " & DO11_PIVA & "<br/>"
                End If


                Dim DcCom As New ClsComune

                DcCom.Provincia = Ospite.RESIDENZAPROVINCIA1
                DcCom.Comune = Ospite.RESIDENZACOMUNE1
                DcCom.Leggi(Session("DC_OSPITE"))

                DO11_CITTA_NOME = DcCom.Descrizione
                DO11_CITTA_ISTAT = DcCom.Provincia & DcCom.Comune
                DO11_CITTA_CATASTALE = DcCom.CODXCODF

                Dim DcProv As New ClsComune

                DcProv.Provincia = Ospite.RESIDENZAPROVINCIA1
                DcProv.Comune = ""
                DcProv.Leggi(Session("DC_OSPITE"))
                DO11_PROVINCIA = DcProv.CodificaProvincia
                DO11_CONDPAG_CODICE_SEN = Ospite.ModalitaPagamento
                Dim MPAg As New ClsModalitaPagamento

                MPAg.Codice = Ospite.ModalitaPagamento
                MPAg.Leggi(Session("DC_OSPITE"))
                DO11_CONDPAG_NOME_SEN = MPAg.DESCRIZIONE


                AddDescrizione = Trim(Fnvb6.LeggiNote(campodb(myPOSTreader.Item("CentroServizio")), CodiceProvincia, CodiceComune, "", True, campodb(myPOSTreader.Item("NumeroRegistrazione")), 0, Session("DC_OSPITE")))

                CIG = Ospite.CodiceCig
                IDDocumento = Ospite.IdDocumento
                IDDocumentoData = Ospite.IdDocumentoData
                CodiceDestinatario = Ospite.CodiceDestinatario
                RiferimentoAmministrazione = Ospite.RiferimentoAmministrazione
            End If
            If Mid(Tipo & Space(10), 1, 1) = "J" Then
                Dim Ospite As New ClsComune

                Ospite.Provincia = CodiceProvincia
                Ospite.Comune = CodiceComune
                Ospite.Leggi(Session("DC_OSPITE"))
                DO11_RAGIONESOCIALE = Ospite.Descrizione
                DO11_PIVA = Ospite.PartitaIVA
                DO11_CF = Ospite.CodiceFiscale
                DO11_INDIRIZZO = Ospite.RESIDENZAINDIRIZZO1


                Dim VerificaPIVA As New Cls_CodiceFiscale


                If VerificaPIVA.CheckPartitaIva(AdattaLunghezzaNumero(DO11_CF, 11)) = False Then
                    ErroriEsportazione = ErroriEsportazione & " Partita Iva formalmente errato per ente " & DO11_RAGIONESOCIALE & "<br/>"
                End If


                Dim DcCom As New ClsComune

                DcCom.Provincia = Ospite.RESIDENZAPROVINCIA1
                DcCom.Comune = Ospite.RESIDENZACOMUNE1
                DcCom.Leggi(Session("DC_OSPITE"))

                DO11_CITTA_NOME = DcCom.Descrizione
                DO11_CITTA_ISTAT = DcCom.Provincia & DcCom.Comune
                DO11_CITTA_CATASTALE = DcCom.CODXCODF

                Dim DcProv As New ClsComune

                DcProv.Provincia = Ospite.RESIDENZAPROVINCIA1
                DcProv.Comune = ""
                DcProv.Leggi(Session("DC_OSPITE"))
                DO11_PROVINCIA = DcProv.CodificaProvincia
                DO11_CONDPAG_CODICE_SEN = Ospite.ModalitaPagamento
                Dim MPAg As New ClsModalitaPagamento

                MPAg.Codice = Ospite.ModalitaPagamento
                MPAg.Leggi(Session("DC_OSPITE"))
                DO11_CONDPAG_NOME_SEN = MPAg.DESCRIZIONE


                AddDescrizione = Trim(Fnvb6.LeggiNote(campodb(myPOSTreader.Item("CentroServizio")), CodiceProvincia, CodiceComune, "", True, campodb(myPOSTreader.Item("NumeroRegistrazione")), 0, Session("DC_OSPITE")))

                CIG = Ospite.CodiceCig
                IDDocumento = Ospite.IdDocumento
                IDDocumentoData = Ospite.IdDocumentoData
                CodiceDestinatario = Ospite.CodiceDestinatario
                RiferimentoAmministrazione = Ospite.RiferimentoAmministrazione
            End If
            If Mid(Tipo & Space(10), 1, 1) = "R" Then
                Dim Ospite As New ClsUSL

                Ospite.CodiceRegione = CodiceRegione
                Ospite.Leggi(Session("DC_OSPITE"))
                DO11_RAGIONESOCIALE = Ospite.Nome
                DO11_PIVA = Ospite.PARTITAIVA
                DO11_CF = Ospite.CodiceFiscale
                DO11_INDIRIZZO = Ospite.RESIDENZAINDIRIZZO1


                Dim VerificaPIVA As New Cls_CodiceFiscale


                If VerificaPIVA.CheckPartitaIva(AdattaLunghezzaNumero(DO11_CF, 11)) = False Then
                    ErroriEsportazione = ErroriEsportazione & " Partita Iva formalmente errato per ente " & DO11_RAGIONESOCIALE & "<br/>"
                End If


                Dim DcCom As New ClsComune

                DcCom.Provincia = Ospite.RESIDENZAPROVINCIA1
                DcCom.Comune = Ospite.RESIDENZACOMUNE1
                DcCom.Leggi(Session("DC_OSPITE"))

                DO11_CITTA_NOME = DcCom.Descrizione
                DO11_CITTA_ISTAT = DcCom.Provincia & DcCom.Comune
                DO11_CITTA_CATASTALE = DcCom.CODXCODF

                Dim DcProv As New ClsComune

                DcProv.Provincia = Ospite.RESIDENZAPROVINCIA1
                DcProv.Comune = ""
                DcProv.Leggi(Session("DC_OSPITE"))
                DO11_PROVINCIA = DcProv.CodificaProvincia
                DO11_CONDPAG_CODICE_SEN = Ospite.ModalitaPagamento
                Dim MPAg As New ClsModalitaPagamento

                MPAg.Codice = Ospite.ModalitaPagamento
                MPAg.Leggi(Session("DC_OSPITE"))
                DO11_CONDPAG_NOME_SEN = MPAg.DESCRIZIONE

                CIG = Ospite.CodiceCig
                IDDocumento = Ospite.IdDocumento
                REM IDDocumentoData = Format(Ospite.IdDocumento, "dd/MM/yyyy")
                CodiceDestinatario = Ospite.CodiceDestinatario
                RiferimentoAmministrazione = Ospite.RiferimentoAmministrazione

                AddDescrizione = Trim(Fnvb6.LeggiNote(campodb(myPOSTreader.Item("CentroServizio")), "", "", CodiceRegione, True, campodb(myPOSTreader.Item("NumeroRegistrazione")), 0, Session("DC_OSPITE")))

            End If


            Dim M As New Cls_TipoPagamento

            M.Codice = campodb(myPOSTreader.Item("CodicePagamento"))
            M.Leggi(Session("DC_TABELLE"))
            If M.Descrizione <> "" Then
                DO11_CONDPAG_CODICE_SEN = M.Codice
                DO11_CONDPAG_NOME_SEN = M.Descrizione
            End If

            If DO11_CONDPAG_CODICE_SEN = "" Or DO11_CONDPAG_NOME_SEN = "" Then
                ErroriEsportazione = ErroriEsportazione & " Modalita pagamento non inserita per registrazione " & campodbN(myPOSTreader.Item("NumeroRegistrazione")) & "<br/>"
            End If

            If DO11_CONDPAG_NOME_SEN <> "" Then
                If DO11_CONDPAG_NOME_SEN.IndexOf("SEPA") >= 0 Or DO11_CONDPAG_NOME_SEN.IndexOf("RID") >= 0 Or DO11_CONDPAG_NOME_SEN.IndexOf("R.I.D") >= 0 Then
                    Dim MyCheckIban As New CheckIban

                    If Mid(DO11_BANCA_IBAN & Space(10), 1, 2) = "IT" Then
                        If MyCheckIban.ValidateIBAN(DO11_BANCA_IBAN) <> "Codice Corretto" Then
                            ErroriEsportazione = ErroriEsportazione & " Codice Iban formalmente errato " & DO11_RAGIONESOCIALE & "-" & MyCheckIban.ValidateIBAN(DO11_BANCA_IBAN) & "<br/>"
                        End If
                    End If


                    If DO11_BANCA_IDMANDATO = "" Then
                        ErroriEsportazione = ErroriEsportazione & " Specificare ID MANDATO  " & DO11_RAGIONESOCIALE & "<br/>"
                    End If


                    If Not IsDate(DO11_BANCA_DATAMANDATO) Or DO11_BANCA_DATAMANDATO = "00:00:00" Or DO11_BANCA_DATAMANDATO = "00.00.00" Then
                        ErroriEsportazione = ErroriEsportazione & " Specificare DATA MANDATO " & DO11_RAGIONESOCIALE & "<br/>"
                    End If
                End If
            End If

            If DO11_CITTA_CATASTALE = "" And DO11_CITTA_NOME <> "" Then
                ErroriEsportazione = ErroriEsportazione & " Codice catastale non indicato per comune " & DO11_CITTA_NOME & "<br/>"
            End If

            If Session("DC_OSPITE").ToString.ToUpper.IndexOf("RISO") >= 0 Then

                DO11_CONDPAG_CODICE_SEN = "O"
                DO11_CONDPAG_NOME_SEN = "bonif.banc. a vista"
                If Mid(Tipo & Space(10), 1, 1) = "C" Or Mid(Tipo & Space(10), 1, 1) = "R" Or Mid(Tipo & Space(10), 1, 1) = "J" Then
                    DO11_CONDPAG_CODICE_SEN = "R"
                    DO11_CONDPAG_NOME_SEN = "bonif.bamc. 30 gg.df."
                End If
            End If

            Appoggio = Appoggio & DO11_RAGIONESOCIALE.Replace("  ", " ") & ";"
            Appoggio = Appoggio & DO11_PIVA & ";"
            Appoggio = Appoggio & DO11_CF & ";"
            Appoggio = Appoggio & DO11_INDIRIZZO & ";"
            Appoggio = Appoggio & DO11_CITTA_NOME & ";"
            Appoggio = Appoggio & DO11_CITTA_ISTAT & ";"
            Appoggio = Appoggio & DO11_PROVINCIA & ";"
            Appoggio = Appoggio & DO11_BANCA_CIN & ";"
            Appoggio = Appoggio & DO11_BANCA_ABI & ";"
            Appoggio = Appoggio & DO11_BANCA_CAB & ";"
            Appoggio = Appoggio & DO11_BANCA_CONTO & ";"
            Appoggio = Appoggio & DO11_BANCA_IBAN & ";"
            'If campodb(myPOSTreader.Item("CodicePagamento")) <> "" Then
            '    Dim MPAg As New ClsModalitaPagamento

            '    MPAg.Codice = campodb(myPOSTreader.Item("CodicePagamento"))
            '    MPAg.Leggi(Session("DC_OSPITE"))
            '    DO11_CONDPAG_CODICE_SEN = MPAg.Codice
            '    DO11_CONDPAG_NOME_SEN = MPAg.DESCRIZIONE
            'End If
            If CodiceInstallazione = "07" Then
                If DO11_CONDPAG_CODICE_SEN = "04" Then
                    DO11_CONDPAG_CODICE_SEN = "05"
                End If
            End If

            Appoggio = Appoggio & DO11_CONDPAG_CODICE_SEN & ";"
            Appoggio = Appoggio & DO11_CONDPAG_NOME_SEN & ";"
            Appoggio = Appoggio & DO11_BANCA_IDMANDATO & ";"
            Appoggio = Appoggio & DO11_BANCA_DATAMANDATO & ";"
            If CausaleContabile.TipoDocumento = "NC" Then
                Appoggio = Appoggio & "NC" & ";"
            Else
                Appoggio = Appoggio & "FT" & ";"
            End If
            Appoggio = Appoggio & CIG & ";"
            Appoggio = Appoggio & IDDocumento & ";"
            Appoggio = Appoggio & IDDocumentoData & ";"
            Appoggio = Appoggio & CodiceDestinatario & ";"
            Appoggio = Appoggio & RiferimentoAmministrazione & ";"


            If CodiceInstallazione <> "" Then
                If Session("DC_OSPITE").ToString.ToUpper.IndexOf("Lebetulle".ToUpper) > 0 Or Session("DC_OSPITE").ToString.ToUpper.IndexOf("Sicurezz".ToUpper) > 0 Then
                    Appoggio = Appoggio & RegistroIva & ";"
                Else
                    'If Session("NomeEPersonam").ToString.ToUpper = "BAGGIO" Then

                    '    If RegistroIva > 3 Then
                    '        Appoggio = Appoggio & Mid(CodiceInstallazione, 2, 1) & "0" & ";"
                    '    Else
                    '        Appoggio = Appoggio & "30" & ";"
                    '    End If
                    'Else
                    '    Appoggio = Appoggio & Mid(CodiceInstallazione, 2, 1) & "0" & ";"
                    'End If
                    Appoggio = Appoggio & CodiceRegistro & ";"
                End If

                If DO11_INTESTATARIOSDD = "" Then
                    Appoggio = Appoggio & "" & ";"
                Else
                    Appoggio = Appoggio & DO11_INTESTATARIOSDD.Replace("  ", " ") & ";"
                End If

                Appoggio = Appoggio & DO11_INTESTATARIOCFSDD & ";"



                If DO11_COGNOMENOME_OSPITERIFERIMENO = "" Then
                    Appoggio = Appoggio & "" & ";"
                Else
                    Appoggio = Appoggio & DO11_COGNOMENOME_OSPITERIFERIMENO.Replace("  ", " ") & ";"
                End If

                Appoggio = Appoggio & DO11_CODICEFISCALE_OSPITERIFERIMENO & ";"
                Appoggio = Appoggio & DO11_DATANASCITA_OSPITERIFERIMENO & ";"

                If DO11_COGNOMENOME_DESTINATARIO = "" Then
                    Appoggio = Appoggio & "" & ";"
                Else
                    Appoggio = Appoggio & DO11_COGNOMENOME_DESTINATARIO.Replace("  ", " ") & ";"
                End If

                Appoggio = Appoggio & DO11_INDIRIZZO_DESTINATARIO & ";"

                Appoggio = Appoggio & DO11_COMUNE_DESTINATARIO & ";"
                Appoggio = Appoggio & DO11_COMUNECOD_DESTINATARIO & ";"

                Appoggio = Appoggio & DO11_CAP_DESTINATARIO & ";"
                Appoggio = Appoggio & DO11_MAIL_DESTINATARIO & ";"

            End If


            If Session("DC_OSPITE").ToString.ToUpper.IndexOf("Lebetulle".ToUpper) <= 0 Then
                Appoggio = Appoggio & AddDescrizione & ";"
            Else

                If Mid(Tipo & Space(10), 1, 1) = "O" Or Mid(Tipo & Space(10), 1, 1) = "P" Then

                    Dim MR As New Cls_rettatotale

                    MR.CODICEOSPITE = CodiceOspite
                    MR.UltimaData(Session("DC_OSPITE"), CodiceOspite, myPOSTreader.Item("CentroServizio"))

                    Dim GiorniPresenza As Integer
                    'RigaDaCausale = 3 And MoveFromDbWC(MyRs, "MastroPartita") = 58 And MoveFromDbWC(MyRs, "ContoPartita") = 20 And MoveFromDbWC(MyRs, "SottocontoPartita") = 2 

                    Dim cmdRd1 As New OleDbCommand()
                    cmdRd1.CommandText = "Select sum(Quantita) As Giorni From MovimentiContabiliRiga Where RigaDaCausale = 3 And (Not Descrizione like '%Sconto%' Or Descrizione is Null)  And SottocontoPartita <> 2 And Numero = " & campodbN(myPOSTreader.Item("NumeroRegistrazione"))
                    cmdRd1.Connection = cn
                    cmdRd1.Parameters.AddWithValue("@Numero", campodbN(myPOSTreader.Item("NumeroRegistrazione")))
                    Dim MyReadSC1 As OleDbDataReader = cmdRd1.ExecuteReader()
                    If MyReadSC1.Read Then
                        GiorniPresenza = campodbN(MyReadSC1.Item("Giorni"))
                    End If
                    MyReadSC1.Close()

                    If myPOSTreader.Item("CentroServizio") = "CD" Then
                    Else
                        Dim MeseComp As Integer = campodbN(myPOSTreader.Item("MeseCompetenza"))
                        Dim AnnoComp As Integer = campodbN(myPOSTreader.Item("AnnoCompetenza"))
                        Dim DescrizioneRighe As String = ""

                        Dim cmdRighe As New OleDbCommand()
                        cmdRighe.CommandText = "Select MastroPartita,ContoPartita,SottocontoPartita,Descrizione From MovimentiContabiliRiga   Where RigaDaCausale =3 And Numero = " & campodbN(myPOSTreader.Item("NumeroRegistrazione"))
                        cmdRighe.Connection = cn
                        cmdRighe.Parameters.AddWithValue("@Numero", campodbN(myPOSTreader.Item("NumeroRegistrazione")))
                        Dim MyReadRighe As OleDbDataReader = cmdRighe.ExecuteReader()
                        Do While MyReadRighe.Read
                            Dim PinConto As New Cls_Pianodeiconti

                            PinConto.Mastro = campodb(MyReadRighe.Item("MastroPartita"))
                            PinConto.Conto = campodb(MyReadRighe.Item("ContoPartita"))
                            PinConto.Sottoconto = campodb(MyReadRighe.Item("SottoContoPartita"))
                            PinConto.Decodfica(Session("DC_GENERALE"))


                            DescrizioneRighe = DescrizioneRighe & PinConto.Descrizione
                        Loop
                        MyReadRighe.Close()


                        If MeseComp < 12 Then
                            MeseComp = MeseComp + 1
                        Else
                            MeseComp = 1
                            AnnoComp = AnnoComp + 1
                        End If

                        Dim UltLiv As New Cls_rettatotale

                        UltLiv.CENTROSERVIZIO = campodb(myPOSTreader.Item("CentroServizio"))
                        UltLiv.CODICEOSPITE = CodiceOspite
                        UltLiv.UltimaData(Session("DC_OSPITE"), UltLiv.CODICEOSPITE, UltLiv.CENTROSERVIZIO)
                        Dim Xs As New Cls_TipoRetta

                        Xs.Tipo = UltLiv.TipoRetta
                        Xs.Leggi(Session("DC_OSPITE"), Xs.Tipo)
                        If Xs.Descrizione = "PROVVISORIO" Then
                            Dim UltAcc As New Cls_Movimenti

                            UltAcc.CENTROSERVIZIO = UltLiv.CENTROSERVIZIO
                            UltAcc.CodiceOspite = UltLiv.CODICEOSPITE
                            UltAcc.UltimaDataAccoglimento(Session("DC_OSPITE"))

                            Dim UltUsc As New Cls_Movimenti

                            UltUsc.CENTROSERVIZIO = UltLiv.CENTROSERVIZIO
                            UltUsc.CodiceOspite = UltLiv.CODICEOSPITE
                            UltUsc.UltimaDataUscitaDefinitiva(Session("DC_OSPITE"))

                            GiorniPresenza = DateDiff(DateInterval.Day, UltAcc.Data, UltUsc.Data)
                            If GiorniPresenza > 0 Then
                                If DescrizioneRighe.ToUpper.IndexOf("Alberg".ToUpper) >= 0 Then
                                    If campodb(myPOSTreader.Item("CentroServizio")) = "NA" Then
                                        Appoggio = Appoggio & Format(GiorniPresenza * 7.24, "0.00") & ";"
                                    Else
                                        If GiorniPresenza > 0 Then
                                            Appoggio = Appoggio & Format(GiorniPresenza * 14.14, "0.00") & ";"
                                        Else
                                            Appoggio = Appoggio & ";"
                                        End If
                                    End If
                                End If
                            Else
                                Appoggio = Appoggio & ";"
                            End If
                        Else
                            If campodb(myPOSTreader.Item("CentroServizio")) = "NA" Then
                                If GiorniPresenza = GiorniMese(MeseComp, AnnoComp) Then
                                    Appoggio = Appoggio & "220,00" & ";"
                                Else
                                    If GiorniPresenza > 0 Then
                                        Appoggio = Appoggio & Format(GiorniPresenza * 7.24, "0.00") & ";"
                                    Else
                                        Appoggio = Appoggio & ";"
                                    End If
                                End If
                            Else
                                If GiorniPresenza = GiorniMese(MeseComp, AnnoComp) Then
                                    Appoggio = Appoggio & "430,00" & ";"
                                Else
                                    If GiorniPresenza > 0 Then
                                        Appoggio = Appoggio & Format(GiorniPresenza * 14.14, "0.00") & ";"
                                    Else
                                        Appoggio = Appoggio & ";"
                                    End If
                                End If
                            End If
                        End If
                    End If
                End If
            End If

            tw.WriteLine(Appoggio.Replace(vbNewLine, " "))

            Dim Progressivo As Integer = 0
            Dim Entrato As Boolean = False


            If Ragguppato = True Then

                Dim cmdRiga As New OleDbCommand()
                cmdRiga.CommandText = "Select * From MovimentiContabiliRiga Where Numero = ?  And (Tipo Is Null Or Tipo <> 'IV')  And RigaDaCausale = 1"
                cmdRiga.Connection = cn
                cmdRiga.Parameters.AddWithValue("@Numero", campodbN(myPOSTreader.Item("NumeroRegistrazione")))
                Dim MyRiga As OleDbDataReader = cmdRiga.ExecuteReader()
                Do While MyRiga.Read
                    Progressivo = Progressivo + 1
                    ProgressivoAssoluto = ProgressivoAssoluto + 1
                    AppoggioRiga = "R" & ";"  'DO_TIPO_RIGA
                    AppoggioRiga = AppoggioRiga & ProgressivoAssoluto & ";" 'DO30_ID
                    AppoggioRiga = AppoggioRiga & campodbN(myPOSTreader.Item("Id")) & ";" 'DO30_ID_SEN
                    AppoggioRiga = AppoggioRiga & ProgressivoTesta & ";" 'DO30_ID_DO11
                    AppoggioRiga = AppoggioRiga & campodbN(myPOSTreader.Item("NumeroRegistrazione")) & ";" 'DO30_ID_DO11_SEN
                    AppoggioRiga = AppoggioRiga & CodiceInstallazione & ";" 'DO30_CODICE_INS
                    AppoggioRiga = AppoggioRiga & CodiceDitta & ";" ' 'DO30_DITTA_CG18
                    AppoggioRiga = AppoggioRiga & Progressivo & ";" 'DO30_PROGRIGA
                    AppoggioRiga = AppoggioRiga & "1" & ";" 'DO30_INDTIPORIGA
                    AppoggioRiga = AppoggioRiga & "" & ";" 'DO30_CODART_MG66
                    AppoggioRiga = AppoggioRiga & campodbN(MyRiga.Item("MastroPartita")) & "." & campodbN(MyRiga.Item("ContoPartita")) & "." & campodbN(MyRiga.Item("SottocontoPartita")) & ";" 'DO30_CODART_MG66_SEN
                    AppoggioRiga = AppoggioRiga & "" & ";" 'DO30_OPZIONE_MG5E

                    Dim DecConto As New Cls_Pianodeiconti

                    DecConto.Mastro = campodbN(MyRiga.Item("MastroControPartita"))
                    DecConto.Conto = campodbN(MyRiga.Item("ContoControPartita"))
                    DecConto.Sottoconto = campodbN(MyRiga.Item("SottocontoControPartita"))
                    DecConto.Decodfica(Session("DC_GENERALE"))


                    If campodb(MyRiga.Item("Descrizione")).ToUpper.IndexOf("PRESENZE") >= 0 Or campodb(MyRiga.Item("Descrizione")).ToUpper.IndexOf("ASSENZE") >= 0 Then
                        AppoggioRiga = AppoggioRiga & campodb(MyRiga.Item("Descrizione")) & " " & DecConto.Descrizione & ";" 'DO30_DESCART
                    Else
                        AppoggioRiga = AppoggioRiga & campodb(MyRiga.Item("Descrizione")) & " " & DecConto.Descrizione & ";" 'DO30_DESCART
                    End If
                    Dim PConti As New Cls_Pianodeiconti

                    PConti.Mastro = campodbN(MyRiga.Item("MastroPartita"))
                    PConti.Conto = campodbN(MyRiga.Item("ContoPartita"))
                    PConti.Sottoconto = campodbN(MyRiga.Item("SottocontoPartita"))
                    PConti.Decodfica(Session("DC_GENERALE"))

                    AppoggioRiga = AppoggioRiga & PConti.Descrizione & ";" 'DO30_DESCART_SEN

                    AppoggioRiga = AppoggioRiga & "" & ";" 'DO30_UM1
                    AppoggioRiga = AppoggioRiga & campodbN(MyRiga.Item("Quantita")) & ";"
                    AppoggioRiga = AppoggioRiga & 0 & ";" 'DO30_COLLI

                    If campodbN(MyRiga.Item("RigaDaCausale")) = 1 Then
                        If CausaleContabile.TipoDocumento = "NC" Then
                            If campodb(MyRiga.Item("DareAVere")) = "A" Then
                                AppoggioRiga = AppoggioRiga & campodbN(MyRiga.Item("Importo")) & ";" 'DO30_PREZZO
                            Else
                                AppoggioRiga = AppoggioRiga & "-" & campodbN(MyRiga.Item("Importo")) & ";" 'DO30_PREZZO
                            End If
                        Else
                            If campodb(MyRiga.Item("DareAVere")) = "D" Then
                                AppoggioRiga = AppoggioRiga & campodbN(MyRiga.Item("Importo")) & ";" 'DO30_PREZZO
                            Else
                                AppoggioRiga = AppoggioRiga & "-" & campodbN(MyRiga.Item("Importo")) & ";" 'DO30_PREZZO
                            End If
                        End If
                    Else
                        If CausaleContabile.TipoDocumento = "NC" Then
                            If campodb(MyRiga.Item("DareAVere")) = "D" Then
                                AppoggioRiga = AppoggioRiga & campodbN(MyRiga.Item("Importo")) & ";" 'DO30_PREZZO
                            Else
                                AppoggioRiga = AppoggioRiga & "-" & campodbN(MyRiga.Item("Importo")) & ";" 'DO30_PREZZO
                            End If
                        Else
                            If campodb(MyRiga.Item("DareAVere")) = "A" Then
                                AppoggioRiga = AppoggioRiga & campodbN(MyRiga.Item("Importo")) & ";" 'DO30_PREZZO
                            Else
                                AppoggioRiga = AppoggioRiga & "-" & campodbN(MyRiga.Item("Importo")) & ";" 'DO30_PREZZO
                            End If
                        End If
                    End If



                    AppoggioRiga = AppoggioRiga & campodb(MyRiga.Item("CodiceIVA")) & ";" 'DO30_CODIVA_CODICE_SEN

                    Dim CIVA As New Cls_IVA

                    CIVA.Codice = campodb(MyRiga.Item("CodiceIVA"))
                    CIVA.Leggi(Session("DC_TABELLE"), CIVA.Codice)

                    AppoggioRiga = AppoggioRiga & CIVA.Descrizione & ";" 'DO30_CODIVA_NOME_SEN
                    Dim Riferimento As String = ""
                    Dim DescrizioneAppoggio As String = ""
                    Dim Cserv As New Cls_CentroServizio



                    Cserv.CENTROSERVIZIO = campodb(myPOSTreader.Item("CentroServizio"))
                    Cserv.Leggi(Session("DC_OSPITE"), Cserv.CENTROSERVIZIO)
                    Riferimento = Cserv.DescrizioneRigaInFattura


                    If Riferimento = "Residenziale Cittadella" Then
                        If (Mid(Tipo & Space(10), 1, 1) = "C" Or Mid(Tipo & Space(10), 1, 1) = "R") Or campodbN(MyRiga.Item("RigaDaCausale")) = 5 Or campodbN(MyRiga.Item("RigaDaCausale")) = 6 Or campodbN(MyRiga.Item("RigaDaCausale")) = 9 Then
                            DescrizioneAppoggio = Fnvb6.DecodificaConto(campodbN(MyRiga.Item("MastroPartita")), campodbN(MyRiga.Item("ContoPartita")), campodbN(MyRiga.Item("SottocontoPartita"))) & " " & Fnvb6.DecodificaConto(campodbN(MyRiga.Item("MastroControPartita")), campodbN(MyRiga.Item("ContoControPartita")), campodbN(MyRiga.Item("SottocontoControPartita"))) & " " & campodb(MyRiga.Item("DescrizioneRiga"))
                        Else
                            Dim P As New Cls_CalcoloRette
                            CodiceOspite = CodiceOspite
                            Dim ComuneCittadella As New Cls_ImportoComune
                            ComuneCittadella.CODICEOSPITE = CodiceOspite
                            ComuneCittadella.CENTROSERVIZIO = Cserv.CENTROSERVIZIO
                            ComuneCittadella.UltimaData(Session("DC_OSPITE"), ComuneCittadella.CODICEOSPITE, ComuneCittadella.CENTROSERVIZIO)

                            If ComuneCittadella.PROV <> "" And ComuneCittadella.COMUNE <> "" Then
                                Dim MyCodificaComumeCittadella As New ClsComune
                                MyCodificaComumeCittadella.Provincia = ComuneCittadella.PROV
                                MyCodificaComumeCittadella.Comune = ComuneCittadella.COMUNE
                                MyCodificaComumeCittadella.Leggi(Session("DC_OSPITE"))

                                If ComuneCittadella.Importo > 0 Then
                                    DescrizioneAppoggio = Fnvb6.DecodificaConto(campodbN(MyRiga.Item("MastroPartita")), campodbN(MyRiga.Item("ContoPartita")), campodbN(MyRiga.Item("SottocontoPartita"))) & " " & Fnvb6.DecodificaConto(campodbN(MyRiga.Item("MastroControPartita")), campodbN(MyRiga.Item("ContoControPartita")), campodbN(MyRiga.Item("SottocontoControPartita"))) & " " & campodb(MyRiga.Item("DescrizioneRiga")) & vbNewLine & "Compartecipazione comune " & MyCodificaComumeCittadella.Descrizione & " Euro " & Format(ComuneCittadella.Importo, "#,##0.00") & " " & ComuneCittadella.Tipo
                                Else
                                    DescrizioneAppoggio = Fnvb6.DecodificaConto(campodbN(MyRiga.Item("MastroPartita")), campodbN(MyRiga.Item("ContoPartita")), campodbN(MyRiga.Item("SottocontoPartita"))) & " " & Fnvb6.DecodificaConto(campodbN(MyRiga.Item("MastroControPartita")), campodbN(MyRiga.Item("ContoControPartita")), campodbN(MyRiga.Item("SottocontoControPartita"))) & " " & campodb(MyRiga.Item("DescrizioneRiga")) & vbNewLine & "Compartecipazione comune " & MyCodificaComumeCittadella.Descrizione
                                End If

                            Else
                                DescrizioneAppoggio = Fnvb6.DecodificaConto(campodbN(MyRiga.Item("MastroPartita")), campodbN(MyRiga.Item("ContoPartita")), campodbN(MyRiga.Item("SottocontoPartita"))) & " " & Fnvb6.DecodificaConto(campodbN(MyRiga.Item("MastroControPartita")), campodbN(MyRiga.Item("ContoControPartita")), campodbN(MyRiga.Item("SottocontoControPartita"))) & " " & campodb(MyRiga.Item("DescrizioneRiga"))
                            End If

                        End If
                    End If

                    If Riferimento = "Med Service" Then
                        If Mid(Tipo & Space(10), 1, 1) = "C" Or Mid(Tipo & Space(10), 1, 1) = "R" Or Mid(Tipo & Space(10), 1, 1) = "J" Then
                            If campodbN(MyRiga.Item("RigaDaCausale")) = 3 Then
                                DescrizioneAppoggio = "Retta " & Fnvb6.DecodificaConto(campodbN(MyRiga.Item("MastroControPartita")), campodbN(MyRiga.Item("ContoControPartita")), campodbN(MyRiga.Item("SottocontoControPartita"))) & " " & campodb(MyRiga.Item("DescrizioneRiga")) & " " & Fnvb6.DescrizioneRigaOspite(Int(campodbN(MyRiga.Item("SottocontoControPartita")) / 100), campodbN(myPOSTreader.Item("AnnoCompetenza")), campodbN(myPOSTreader.Item("MeseCompetenza")))
                            Else
                                DescrizioneAppoggio = "Retta " & Fnvb6.DecodificaConto(campodbN(MyRiga.Item("MastroControPartita")), campodbN(MyRiga.Item("ContoControPartita")), campodbN(MyRiga.Item("SottoContoControPartita"))) & " " & Fnvb6.DecodificaConto(campodbN(MyRiga.Item("MastroPartita")), campodbN(MyRiga.Item("ContoPartita")), campodbN(MyRiga.Item("SottocontoPartita"))) & " " & campodb(MyRiga.Item("DescrizioneRiga")) ' & " " & Fnvb6.DescrizioneRigaOspite(Int(campodbN(MyRiga.Item("SottocontoControPartita")) / 100), campodbN(myPOSTreader.Item("AnnoCompetenza")), campodbN(myPOSTreader.Item("MeseCompetenza"))))
                            End If
                        Else
                            If campodbN(MyRiga.Item("RigaDaCausale")) = 3 Then
                                If campodbN(MyRiga.Item("SottocontoPartita")) = 5800400016 Or campodbN(MyRiga.Item("SottocontoPartita")) = 5800400012 Or campodbN(MyRiga.Item("SottocontoPartita")) = 5800400014 Or campodbN(MyRiga.Item("SottocontoPartita")) = 5800400015 Then
                                    DescrizioneAppoggio = "Retta Alzheimer " & Fnvb6.DecodificaConto(campodbN(MyRiga.Item("MastroControPartita")), campodbN(MyRiga.Item("ContoControPartita")), Int(campodbN(MyRiga.Item("SottocontoControPartita")) / 100) * 100) & " " & campodb(MyRiga.Item("DescrizioneRiga"))
                                Else
                                    DescrizioneAppoggio = "Retta " & Fnvb6.DecodificaConto(campodbN(MyRiga.Item("MastroControPartita")), campodbN(MyRiga.Item("ContoControPartita")), Int(campodbN(MyRiga.Item("SottocontoControPartita")) / 100) * 100) & " " & campodb(MyRiga.Item("DescrizioneRiga"))
                                End If
                            Else
                                DescrizioneAppoggio = campodb(MyRiga.Item("DescrizioneRiga")) & " " & Fnvb6.DecodificaConto(campodbN(MyRiga.Item("MastroControPartita")), campodbN(MyRiga.Item("ContoControPartita")), campodbN(MyRiga.Item("SottocontoControPartita")))
                            End If
                        End If

                        '5800
                        Dim AppoggioSottoconto As String = campodb(MyRiga.Item("SottocontoPartita"))


                        If CodiceInstallazione = "04" And RegistroIva = 1 Then
                            If AppoggioSottoconto.IndexOf("580" & "03") < 0 And AppoggioSottoconto.IndexOf("580" & "04") < 0 Then
                                ErroriEsportazione = ErroriEsportazione & "Sottoconto non conforme con codice installazione reg " & campodbN(myPOSTreader.Item("NumeroRegistrazione") & " CONTO " & AppoggioSottoconto)
                            End If
                        Else
                            If AppoggioSottoconto.IndexOf("580" & CodiceInstallazione) < 0 And CodiceInstallazione <> "12" Then
                                ErroriEsportazione = ErroriEsportazione & "Sottoconto non conforme con codice installazione reg " & campodbN(myPOSTreader.Item("NumeroRegistrazione") & " CONTO " & AppoggioSottoconto)
                            End If
                        End If
                    End If


                    If campodb(MyRiga.Item("CentroServizio")) = "" Then
                        AppoggioRiga = AppoggioRiga & campodb(myPOSTreader.Item("CentroServizio")) & ";"
                    Else
                        AppoggioRiga = AppoggioRiga & campodb(MyRiga.Item("CentroServizio")) & ";"
                    End If


                    If campodbN(MyRiga.Item("AnnoRiferimento")) = 0 Then
                        AppoggioRiga = AppoggioRiga & campodb(myPOSTreader.Item("AnnoCompetenza")) & ";"
                    Else
                        AppoggioRiga = AppoggioRiga & campodb(MyRiga.Item("AnnoRiferimento")) & ";"
                    End If

                    If campodbN(MyRiga.Item("MeseRiferimento")) = 0 Then
                        AppoggioRiga = AppoggioRiga & campodb(myPOSTreader.Item("MeseCompetenza")) & ";"
                    Else
                        AppoggioRiga = AppoggioRiga & campodb(MyRiga.Item("MeseRiferimento")) & ";"
                    End If


                    Entrato = True

                    tw.WriteLine(AppoggioRiga.Replace(vbNewLine, " "))

                Loop
                MyRiga.Close()

                Dim cmdRiga1 As New OleDbCommand()
                cmdRiga1.CommandText = "Select MastroPartita,ContoPartita,SottocontoPartita,RigaDaCausale,CodiceIVA,SUM(CASE WHEN DareAvere = 'A' THEN Importo ELSE Importo * -1 END) as ImportoScorporato  From MovimentiContabiliRiga Where Numero = ?  And (Tipo Is Null Or Tipo <> 'IV') And RigaDaCausale > 1  GROUP BY MastroPartita,ContoPartita,SottocontoPartita,RigaDaCausale,CodiceIVA"
                cmdRiga1.Connection = cn
                cmdRiga1.Parameters.AddWithValue("@Numero", campodbN(myPOSTreader.Item("NumeroRegistrazione")))
                Dim MyRiga1 As OleDbDataReader = cmdRiga1.ExecuteReader()
                Do While MyRiga1.Read
                    Progressivo = Progressivo + 1
                    ProgressivoAssoluto = ProgressivoAssoluto + 1
                    AppoggioRiga = "R" & ";"  'DO_TIPO_RIGA
                    AppoggioRiga = AppoggioRiga & ProgressivoAssoluto & ";" 'DO30_ID
                    AppoggioRiga = AppoggioRiga & campodbN(myPOSTreader.Item("Id")) & ";" 'DO30_ID_SEN
                    AppoggioRiga = AppoggioRiga & ProgressivoTesta & ";" 'DO30_ID_DO11
                    AppoggioRiga = AppoggioRiga & campodbN(myPOSTreader.Item("NumeroRegistrazione")) & ";" 'DO30_ID_DO11_SEN
                    AppoggioRiga = AppoggioRiga & CodiceInstallazione & ";" 'DO30_CODICE_INS
                    AppoggioRiga = AppoggioRiga & CodiceDitta & ";" ' 'DO30_DITTA_CG18
                    AppoggioRiga = AppoggioRiga & Progressivo & ";" 'DO30_PROGRIGA
                    AppoggioRiga = AppoggioRiga & "1" & ";" 'DO30_INDTIPORIGA
                    AppoggioRiga = AppoggioRiga & "" & ";" 'DO30_CODART_MG66
                    AppoggioRiga = AppoggioRiga & campodbN(MyRiga1.Item("MastroPartita")) & "." & campodbN(MyRiga1.Item("ContoPartita")) & "." & campodbN(MyRiga1.Item("SottocontoPartita")) & ";" 'DO30_CODART_MG66_SEN
                    AppoggioRiga = AppoggioRiga & "" & ";" 'DO30_OPZIONE_MG5E


                    AppoggioRiga = AppoggioRiga & " " & ";" 'DO30_DESCART

                    Dim PConti As New Cls_Pianodeiconti

                    PConti.Mastro = campodbN(MyRiga1.Item("MastroPartita"))
                    PConti.Conto = campodbN(MyRiga1.Item("ContoPartita"))
                    PConti.Sottoconto = campodbN(MyRiga1.Item("SottocontoPartita"))
                    PConti.Decodfica(Session("DC_GENERALE"))



                    AppoggioRiga = AppoggioRiga & PConti.Descrizione & ";" 'DO30_DESCART_SEN

                    AppoggioRiga = AppoggioRiga & "" & ";" 'DO30_UM1
                    AppoggioRiga = AppoggioRiga & "0;"
                    AppoggioRiga = AppoggioRiga & 0 & ";" 'DO30_COLLI

                    If CausaleContabile.TipoDocumento = "NC" Then
                        If campodbN(MyRiga1.Item("ImportoScorporato")) > 0 Then
                            AppoggioRiga = AppoggioRiga & campodbN(MyRiga1.Item("ImportoScorporato")) & ";" 'DO30_PREZZO
                        Else
                            AppoggioRiga = AppoggioRiga & campodbN(MyRiga1.Item("ImportoScorporato")) & ";" 'DO30_PREZZO
                        End If
                    Else
                        If campodbN(MyRiga1.Item("ImportoScorporato")) > 0 Then
                            AppoggioRiga = AppoggioRiga & campodbN(MyRiga1.Item("ImportoScorporato")) & ";" 'DO30_PREZZO
                        Else
                            AppoggioRiga = AppoggioRiga & campodbN(MyRiga1.Item("ImportoScorporato")) & ";" 'DO30_PREZZO
                        End If
                    End If




                    AppoggioRiga = AppoggioRiga & campodb(MyRiga1.Item("CodiceIVA")) & ";" 'DO30_CODIVA_CODICE_SEN

                    Dim CIVA As New Cls_IVA

                    CIVA.Codice = campodb(MyRiga1.Item("CodiceIVA"))
                    CIVA.Leggi(Session("DC_TABELLE"), CIVA.Codice)

                    AppoggioRiga = AppoggioRiga & CIVA.Descrizione & ";" 'DO30_CODIVA_NOME_SEN



                    AppoggioRiga = AppoggioRiga & campodb(myPOSTreader.Item("CentroServizio")) & ";"

                    AppoggioRiga = AppoggioRiga & campodb(myPOSTreader.Item("AnnoCompetenza")) & ";"

                    AppoggioRiga = AppoggioRiga & campodb(myPOSTreader.Item("MeseCompetenza")) & ";"




                    Entrato = True

                    tw.WriteLine(AppoggioRiga)

                Loop
                MyRiga.Close()

            Else

                Dim cmdRiga As New OleDbCommand()


                If LeggiParametri.RaggruppaRicavi = 1 Then
                    cmdRiga.CommandText = "Select * From MovimentiContabiliRiga Where Numero = ?  And (Tipo Is Null Or Tipo <> 'IV') And (Importo > 0 OR (Importo = 0 And Imponibile =0))  Order by RigaDaCausale"
                Else
                    cmdRiga.CommandText = "Select * From MovimentiContabiliRiga Where Numero = ?  And (Tipo Is Null Or Tipo <> 'IV')  Order by RigaDaCausale"
                End If

                cmdRiga.Connection = cn
                cmdRiga.Parameters.AddWithValue("@Numero", campodbN(myPOSTreader.Item("NumeroRegistrazione")))
                Dim MyRiga As OleDbDataReader = cmdRiga.ExecuteReader()
                Do While MyRiga.Read
                    Progressivo = Progressivo + 1
                    ProgressivoAssoluto = ProgressivoAssoluto + 1
                    AppoggioRiga = "R" & ";"  'DO_TIPO_RIGA
                    AppoggioRiga = AppoggioRiga & ProgressivoAssoluto & ";" 'DO30_ID
                    AppoggioRiga = AppoggioRiga & campodbN(myPOSTreader.Item("Id")) & ";" 'DO30_ID_SEN
                    AppoggioRiga = AppoggioRiga & ProgressivoTesta & ";" 'DO30_ID_DO11
                    AppoggioRiga = AppoggioRiga & campodbN(myPOSTreader.Item("NumeroRegistrazione")) & ";" 'DO30_ID_DO11_SEN
                    AppoggioRiga = AppoggioRiga & CodiceInstallazione & ";" 'DO30_CODICE_INS
                    AppoggioRiga = AppoggioRiga & CodiceDitta & ";" ' 'DO30_DITTA_CG18
                    AppoggioRiga = AppoggioRiga & Progressivo & ";" 'DO30_PROGRIGA
                    AppoggioRiga = AppoggioRiga & "1" & ";" 'DO30_INDTIPORIGA - TIPO RIGA
                    AppoggioRiga = AppoggioRiga & "" & ";" 'DO30_CODART_MG66 - NON IN USO


                    AppoggioRiga = AppoggioRiga & campodbN(MyRiga.Item("MastroPartita")) & "." & campodbN(MyRiga.Item("ContoPartita")) & "." & campodbN(MyRiga.Item("SottocontoPartita")) & ";" 'DO30_CODART_MG66_SEN
                    AppoggioRiga = AppoggioRiga & "" & ";" 'DO30_OPZIONE_MG5E NON IN USO

                    Dim DecConto As New Cls_Pianodeiconti

                    DecConto.Mastro = campodbN(MyRiga.Item("MastroControPartita"))
                    DecConto.Conto = campodbN(MyRiga.Item("ContoControPartita"))
                    DecConto.Sottoconto = campodbN(MyRiga.Item("SottocontoControPartita"))
                    If DecConto.Sottoconto / 100 <> Int(DecConto.Sottoconto / 100) Then
                        DecConto.Sottoconto = Int(DecConto.Sottoconto / 100) * 100
                    End If
                    DecConto.Decodfica(Session("DC_GENERALE"))




                    Dim Riferimento As String = ""
                    Dim DescrizioneAppoggio As String = ""
                    Dim Cserv As New Cls_CentroServizio

                    REM AppoggioRiga = ""

                    Cserv.CENTROSERVIZIO = campodb(myPOSTreader.Item("CentroServizio"))
                    Cserv.Leggi(Session("DC_OSPITE"), Cserv.CENTROSERVIZIO)
                    Riferimento = Cserv.DescrizioneRigaInFattura

                    If Riferimento = "Conto Retta - descrizione - Betulle" Then
                        If Mid(Tipo & Space(10), 1, 1) = "C" Or Mid(Tipo & Space(10), 1, 1) = "R" Or Mid(Tipo & Space(10), 1, 1) = "J" Then
                            If campodbN(MyRiga.Item("RigaDaCausale")) = 3 Then
                                DescrizioneAppoggio = Fnvb6.DecodificaCommentoConto(campodbN(MyRiga.Item("MastroPartita")), campodbN(MyRiga.Item("ContoPartita")), campodbN(MyRiga.Item("SottocontoPartita"))) & "  " & Fnvb6.DecodificaConto(campodbN(MyRiga.Item("MastroControPartita")), campodbN(MyRiga.Item("ContoControPartita")), campodbN(MyRiga.Item("SottocontoControPartita")))
                            Else
                                If campodbN(MyRiga.Item("RigaDaCausale")) = 9 Then
                                    DescrizioneAppoggio = Fnvb6.DecodificaCommentoConto(campodbN(MyRiga.Item("MastroPartita")), campodbN(MyRiga.Item("ContoPartita")), campodbN(MyRiga.Item("SottocontoPartita")))
                                Else
                                    DescrizioneAppoggio = Fnvb6.DecodificaCommentoConto(campodbN(MyRiga.Item("MastroPartita")), campodbN(MyRiga.Item("ContoPartita")), campodbN(MyRiga.Item("SottocontoPartita"))) & "  " & Fnvb6.DecodificaConto(campodbN(MyRiga.Item("MastroControPartita")), campodbN(MyRiga.Item("ContoControPartita")), campodbN(MyRiga.Item("SottocontoControPartita")))
                                End If
                            End If

                        Else
                            If campodbN(MyRiga.Item("RigaDaCausale")) = 3 Then
                                Dim MeseBetulle As Integer
                                Dim AnnoBetulle As Integer
                                MeseBetulle = campodb(MyRiga.Item("MeseRiferimento"))
                                AnnoBetulle = campodb(MyRiga.Item("AnnoRiferimento"))


                                Dim UltLiv As New Cls_rettatotale

                                UltLiv.CENTROSERVIZIO = campodb(myPOSTreader.Item("CentroServizio"))
                                UltLiv.CODICEOSPITE = Fnvb6.CampoOspiteParenteMCS("CodiceOspite", campodbN(MyRiga.Item("MastroControPartita")), campodbN(MyRiga.Item("ContoControPartita")), campodbN(MyRiga.Item("SottocontoControPartita")))
                                UltLiv.UltimaData(Session("DC_OSPITE"), UltLiv.CODICEOSPITE, UltLiv.CENTROSERVIZIO)
                                Dim Xs As New Cls_TipoRetta

                                Xs.Tipo = UltLiv.TipoRetta
                                Xs.Leggi(Session("DC_OSPITE"), Xs.Tipo)
                                If Xs.Descrizione = "PROVVISORIO" Then
                                    Dim MIng As New Cls_Movimenti

                                    MIng.CENTROSERVIZIO = campodb(myPOSTreader.Item("CentroServizio"))
                                    MIng.CodiceOspite = Fnvb6.CampoOspiteParenteMCS("CodiceOspite", campodbN(MyRiga.Item("MastroControPartita")), campodbN(MyRiga.Item("ContoControPartita")), campodbN(MyRiga.Item("SottocontoControPartita")))
                                    MIng.UltimaDataAccoglimento(Session("DC_OSPITE"))

                                    Dim MUsc As New Cls_Movimenti

                                    MUsc.CENTROSERVIZIO = campodb(myPOSTreader.Item("CentroServizio"))
                                    MUsc.CodiceOspite = Fnvb6.CampoOspiteParenteMCS("CodiceOspite", campodbN(MyRiga.Item("MastroControPartita")), campodbN(MyRiga.Item("ContoControPartita")), campodbN(MyRiga.Item("SottocontoControPartita")))
                                    MUsc.UltimaDataUscitaDefinitiva(Session("DC_OSPITE"))


                                    If campodbN(MyRiga.Item("RigaDaCausale")) = 3 And campodbN(MyRiga.Item("MastroPartita")) = 58 And campodbN(MyRiga.Item("SottocontoPartita")) = 20 And campodbN(MyRiga.Item("SottocontoPartita")) = 2 Then
                                        DescrizioneAppoggio = Fnvb6.DecodificaCommentoConto(campodbN(MyRiga.Item("MastroPartita")), campodbN(MyRiga.Item("ContoPartita")), campodbN(MyRiga.Item("SottocontoPartita"))) & "  " & campodb(MyRiga.Item("Descrizione")) & " Periodo Dal : " & Format(MIng.Data, "dd/MM/yyyy") & " al : " & Format(MUsc.Data, "dd/MM/yyyy")
                                    Else
                                        Dim aPPOGGIOVerificaAssistenza As String = Fnvb6.DecodificaCommentoConto(campodbN(MyRiga.Item("MastroPartita")), campodbN(MyRiga.Item("ContoPartita")), campodbN(MyRiga.Item("SottocontoPartita")))

                                        If aPPOGGIOVerificaAssistenza.IndexOf("Assistenza") > 0 Then
                                            DescrizioneAppoggio = Fnvb6.DecodificaCommentoConto(campodbN(MyRiga.Item("MastroPartita")), campodbN(MyRiga.Item("ContoPartita")), campodbN(MyRiga.Item("SottocontoPartita"))) & "  " & campodb(MyRiga.Item("Descrizione")) & " Periodo Dal : " & Format(MIng.Data, "dd/MM/yyyy") & " al : " & Format(DateSerial(Anno, Mese, GiorniMese(Mese, Anno)), "dd/MM/yyyy")
                                        Else
                                            DescrizioneAppoggio = Fnvb6.DecodificaCommentoConto(campodbN(MyRiga.Item("MastroPartita")), campodbN(MyRiga.Item("ContoPartita")), campodbN(MyRiga.Item("SottocontoPartita"))) & "  " & campodb(MyRiga.Item("Descrizione")) & " Periodo Dal : " & Format(MIng.Data, "dd/MM/yyyy") & " al : " & Format(MUsc.Data, "dd/MM/yyyy")
                                        End If
                                    End If
                                Else
                                    If campodbN(MyRiga.Item("RigaDaCausale")) = 3 And campodbN(MyRiga.Item("MastroPartita")) = 58 And campodbN(MyRiga.Item("ContoPartita")) = 20 And campodbN(MyRiga.Item("SottocontoPartita")) = 2 Then
                                        DescrizioneAppoggio = Fnvb6.DecodificaCommentoConto(campodbN(MyRiga.Item("MastroPartita")), campodbN(MyRiga.Item("ContoPartita")), campodbN(MyRiga.Item("SottocontoPartita"))) & campodb(MyRiga.Item("Descrizione")) & " Mese di " & DecodificaMese(campodb(myPOSTreader.Item("MeseCompetenza"))) & campodb(myPOSTreader.Item("AnnoCompetenza"))
                                    Else
                                        DescrizioneAppoggio = Fnvb6.DecodificaCommentoConto(campodbN(MyRiga.Item("MastroPartita")), campodbN(MyRiga.Item("ContoPartita")), campodbN(MyRiga.Item("SottocontoPartita"))) & campodb(MyRiga.Item("Descrizione")) & " Mese di " & DecodificaMese(MeseBetulle) & AnnoBetulle
                                        If CausaleContabile.TipoDocumento = "NC" Then
                                            If campodb(MyRiga.Item("DareAVere")) = "D" Then
                                                DescrizioneAppoggio = "Reso " & Fnvb6.DecodificaCommentoConto(campodbN(MyRiga.Item("MastroPartita")), campodbN(MyRiga.Item("ContoPartita")), campodbN(MyRiga.Item("SottocontoPartita"))) & campodb(MyRiga.Item("Descrizione")) & " Mese di " & DecodificaMese(MeseBetulle) & AnnoBetulle
                                            End If
                                        End If
                                    End If
                                End If
                            Else
                                Dim AppoggioDescrizione As String = campodb(MyRiga.Item("Descrizione"))
                                '58 20 2 
                                If campodbN(MyRiga.Item("RigaDaCausale")) = 5 And campodbN(MyRiga.Item("MastroPartita")) = 58 And campodbN(MyRiga.Item("ContoPartita")) = 20 And campodbN(MyRiga.Item("SottocontoPartita")) = 2 Then
                                    DescrizioneAppoggio = Fnvb6.DecodificaCommentoConto(campodbN(MyRiga.Item("MastroPartita")), campodbN(MyRiga.Item("ContoPartita")), campodbN(MyRiga.Item("SottocontoPartita"))) & campodb(MyRiga.Item("Descrizione")) & " Mese di " & DecodificaMese(campodb(myPOSTreader.Item("MeseCompetenza"))) & campodb(myPOSTreader.Item("AnnoCompetenza"))
                                Else
                                    DescrizioneAppoggio = Fnvb6.DecodificaCommentoConto(campodbN(MyRiga.Item("MastroPartita")), campodbN(MyRiga.Item("ContoPartita")), campodbN(MyRiga.Item("SottocontoPartita"))) & "  " & campodb(MyRiga.Item("Descrizione"))
                                End If
                            End If
                        End If
                        If DescrizioneAppoggio <> "" Then
                            AppoggioRiga = AppoggioRiga & DescrizioneAppoggio & ";"
                        End If
                    End If

                    If Riferimento = "Uscita Sicurezza" Then
                        If campodbN(MyRiga.Item("RigaDaCausale")) <> 3 Then
                            If (Mid(Tipo & Space(10), 1, 1) = "O" Or Mid(Tipo & Space(10), 1, 1) = "P") Then
                                DescrizioneAppoggio = Fnvb6.DecodificaConto(campodbN(MyRiga.Item("MastroControPartita")), campodbN(MyRiga.Item("ContoControPartita")), campodbN(MyRiga.Item("SottocontoControPartita"))) & " " & campodb(MyRiga.Item("Descrizione"))
                            Else
                                DescrizioneAppoggio = Fnvb6.DecodificaConto(campodbN(MyRiga.Item("MastroControPartita")), campodbN(MyRiga.Item("ContoControPartita")), campodbN(MyRiga.Item("SottocontoControPartita"))) & " " & campodb(MyRiga.Item("Descrizione"))
                            End If
                        Else
                            If (Mid(Tipo & Space(10), 1, 1) = "O" Or Mid(Tipo & Space(10), 1, 1) = "P") Then
                                DescrizioneAppoggio = "Retta Sociale " & Fnvb6.DecodificaConto(campodbN(MyRiga.Item("MastroControPartita")), campodbN(MyRiga.Item("ContoControPartita")), Int(campodbN(MyRiga.Item("SottocontoControPartita")) / 100) * 100) & " " & campodb(MyRiga.Item("Descrizione"))
                            Else
                                DescrizioneAppoggio = Fnvb6.DecodificaConto(campodbN(MyRiga.Item("MastroControPartita")), campodbN(MyRiga.Item("ContoControPartita")), campodbN(MyRiga.Item("SottocontoControPartita"))) & " " & campodb(MyRiga.Item("Descrizione"))
                            End If
                        End If
                        If DescrizioneAppoggio <> "" Then
                            AppoggioRiga = AppoggioRiga & DescrizioneAppoggio.Replace(";", " ").Replace(vbNewLine, " ") & ";"
                        End If
                    End If


                    If Riferimento = "Residenziale Cittadella" Then
                        If campodbN(MyRiga.Item("RigaDaCausale")) >= 3 Then
                            If (Mid(Tipo & Space(10), 1, 1) = "C" Or Mid(Tipo & Space(10), 1, 1) = "R") Or campodbN(MyRiga.Item("RigaDaCausale")) = 5 Or campodbN(MyRiga.Item("RigaDaCausale")) = 6 Or campodbN(MyRiga.Item("RigaDaCausale")) = 9 Then
                                DescrizioneAppoggio = Fnvb6.DecodificaConto(campodbN(MyRiga.Item("MastroPartita")), campodbN(MyRiga.Item("ContoPartita")), campodbN(MyRiga.Item("SottocontoPartita"))) & " " & Fnvb6.DecodificaConto(campodbN(MyRiga.Item("MastroControPartita")), campodbN(MyRiga.Item("ContoControPartita")), campodbN(MyRiga.Item("SottocontoControPartita"))) & " " & campodb(MyRiga.Item("Descrizione"))
                            Else
                                Dim P As New Cls_CalcoloRette
                                CodiceOspite = CodiceOspite
                                Dim ComuneCittadella As New Cls_ImportoComune
                                ComuneCittadella.CODICEOSPITE = CodiceOspite
                                ComuneCittadella.CENTROSERVIZIO = Cserv.CENTROSERVIZIO
                                ComuneCittadella.UltimaData(Session("DC_OSPITE"), ComuneCittadella.CODICEOSPITE, ComuneCittadella.CENTROSERVIZIO)

                                If ComuneCittadella.PROV <> "" And ComuneCittadella.COMUNE <> "" Then
                                    Dim MyCodificaComumeCittadella As New ClsComune
                                    MyCodificaComumeCittadella.Provincia = ComuneCittadella.PROV
                                    MyCodificaComumeCittadella.Comune = ComuneCittadella.COMUNE
                                    MyCodificaComumeCittadella.Leggi(Session("DC_OSPITE"))

                                    If ComuneCittadella.Importo > 0 Then
                                        DescrizioneAppoggio = Fnvb6.DecodificaConto(campodbN(MyRiga.Item("MastroPartita")), campodbN(MyRiga.Item("ContoPartita")), campodbN(MyRiga.Item("SottocontoPartita"))) & " " & Fnvb6.DecodificaConto(campodbN(MyRiga.Item("MastroControPartita")), campodbN(MyRiga.Item("ContoControPartita")), campodbN(MyRiga.Item("SottocontoControPartita"))) & " " & campodb(MyRiga.Item("Descrizione")) & vbNewLine & "Compartecipazione comune " & MyCodificaComumeCittadella.Descrizione & " Euro " & Format(ComuneCittadella.Importo, "#,##0.00") & " " & ComuneCittadella.Tipo
                                    Else
                                        DescrizioneAppoggio = Fnvb6.DecodificaConto(campodbN(MyRiga.Item("MastroPartita")), campodbN(MyRiga.Item("ContoPartita")), campodbN(MyRiga.Item("SottocontoPartita"))) & " " & Fnvb6.DecodificaConto(campodbN(MyRiga.Item("MastroControPartita")), campodbN(MyRiga.Item("ContoControPartita")), campodbN(MyRiga.Item("SottocontoControPartita"))) & " " & campodb(MyRiga.Item("Descrizione")) & vbNewLine & "Compartecipazione comune " & MyCodificaComumeCittadella.Descrizione
                                    End If

                                Else
                                    DescrizioneAppoggio = Fnvb6.DecodificaConto(campodbN(MyRiga.Item("MastroPartita")), campodbN(MyRiga.Item("ContoPartita")), campodbN(MyRiga.Item("SottocontoPartita"))) & " " & Fnvb6.DecodificaConto(campodbN(MyRiga.Item("MastroControPartita")), campodbN(MyRiga.Item("ContoControPartita")), campodbN(MyRiga.Item("SottocontoControPartita"))) & " " & campodb(MyRiga.Item("Descrizione"))
                                End If
                            End If
                        End If
                        If DescrizioneAppoggio <> "" Then
                            AppoggioRiga = AppoggioRiga & DescrizioneAppoggio.Replace(";", " ").Replace(vbNewLine, " ") & ";"
                        End If
                    End If

                    If Riferimento = "Med Service" Then
                        If Mid(Tipo & Space(10), 1, 1) = "C" Or Mid(Tipo & Space(10), 1, 1) = "R" Or Mid(Tipo & Space(10), 1, 1) = "J" Then
                            If campodbN(MyRiga.Item("RigaDaCausale")) = 3 Then

                                DescrizioneAppoggio = "Retta " & Fnvb6.DecodificaConto(campodbN(MyRiga.Item("MastroControPartita")), campodbN(MyRiga.Item("ContoControPartita")), campodbN(MyRiga.Item("SottocontoControPartita"))) & " " & campodb(MyRiga.Item("Descrizione")) & " " & Fnvb6.DescrizioneRigaOspite(Int(campodbN(MyRiga.Item("SottocontoControPartita")) / 100), campodbN(myPOSTreader.Item("AnnoCompetenza")), campodbN(myPOSTreader.Item("MeseCompetenza")))
                            Else
                                DescrizioneAppoggio = "Retta " & Fnvb6.DecodificaConto(campodbN(MyRiga.Item("MastroControPartita")), campodbN(MyRiga.Item("ContoControPartita")), campodbN(MyRiga.Item("SottocontoControPartita")) & " " & Fnvb6.DecodificaConto(campodbN(MyRiga.Item("MastroPartita")), campodbN(MyRiga.Item("ContoPartita")), campodbN(MyRiga.Item("SottocontoPartita"))) & " " & campodb(MyRiga.Item("Descrizione")) & " " & Fnvb6.DescrizioneRigaOspite(Int(campodbN(MyRiga.Item("SottocontoControPartita")) / 100), campodbN(myPOSTreader.Item("AnnoCompetenza")), campodbN(myPOSTreader.Item("MeseCompetenza"))))
                            End If
                        Else
                            If campodbN(MyRiga.Item("RigaDaCausale")) = 3 Then
                                If campodbN(MyRiga.Item("SottocontoPartita")) = 5800400012 Then
                                    DescrizioneAppoggio = "Retta Alzheimer " & Fnvb6.DecodificaConto(campodbN(MyRiga.Item("MastroControPartita")), campodbN(MyRiga.Item("ContoControPartita")), Int(campodbN(MyRiga.Item("SottocontoControPartita")) / 100) * 100) & " " & campodb(MyRiga.Item("Descrizione"))
                                Else
                                    DescrizioneAppoggio = "Retta " & Fnvb6.DecodificaConto(campodbN(MyRiga.Item("MastroControPartita")), campodbN(MyRiga.Item("ContoControPartita")), Int(campodbN(MyRiga.Item("SottocontoControPartita")) / 100) * 100) & " " & campodb(MyRiga.Item("Descrizione"))
                                End If
                            Else
                                DescrizioneAppoggio = campodb(MyRiga.Item("Descrizione")) & " " & Fnvb6.DecodificaConto(campodbN(MyRiga.Item("MastroControPartita")), campodbN(MyRiga.Item("ContoControPartita")), campodbN(MyRiga.Item("SottocontoControPartita")))
                            End If
                        End If

                        '5800
                        Dim AppoggioSottoconto As String = campodb(MyRiga.Item("SottocontoPartita"))

                        If CodiceInstallazione = "04" And RegistroIva = 1 Then
                            If AppoggioSottoconto.IndexOf("580" & "03") < 0 And AppoggioSottoconto.IndexOf("580" & "04") < 0 And campodbN(MyRiga.Item("MastroPartita")) = 90 Then
                                ErroriEsportazione = ErroriEsportazione & "Sottoconto non conforme con codice installazione reg " & campodbN(myPOSTreader.Item("NumeroRegistrazione") & " CONTO " & AppoggioSottoconto)
                            End If
                        Else
                            If AppoggioSottoconto.IndexOf("580" & CodiceInstallazione) < 0 And campodbN(MyRiga.Item("MastroPartita")) = 90 And CodiceInstallazione <> "12" Then
                                ErroriEsportazione = ErroriEsportazione & "Sottoconto non conforme con codice installazione reg " & campodbN(myPOSTreader.Item("NumeroRegistrazione")) & " CONTO " & AppoggioSottoconto
                            End If
                        End If

                        AppoggioRiga = AppoggioRiga & DescrizioneAppoggio.Replace(vbNewLine, " ") & ";"
                    End If

                    If Riferimento = "Diurno Soresina" Then
                        If campodbN(MyRiga.Item("RigaDaCausale")) <> 3 And campodbN(MyRiga.Item("RigaDaCausale")) <> 9 Then
                            DescrizioneAppoggio = Fnvb6.DecodificaConto(campodbN(MyRiga.Item("MastroPartita")), campodbN(MyRiga.Item("ContoPartita")), campodbN(MyRiga.Item("SottocontoPartita"))) & " " & Fnvb6.DecodificaConto(campodbN(MyRiga.Item("MastroControPartita")), campodbN(MyRiga.Item("ContoControPartita")), campodbN(MyRiga.Item("SottocontoControPartita"))) & " " & campodb(MyRiga.Item("Descrizione"))
                        End If
                        If campodbN(MyRiga.Item("RigaDaCausale")) = 3 Then
                            If (Mid(Tipo & Space(10), 1, 1) = "C" Or Mid(Tipo & Space(10), 1, 1) = "R") Then
                                DescrizioneAppoggio = Fnvb6.DecodificaConto(campodbN(MyRiga.Item("MastroPartita")), campodbN(MyRiga.Item("ContoPartita")), campodbN(MyRiga.Item("SottocontoPartita"))) & " " & Fnvb6.DecodificaConto(campodbN(MyRiga.Item("MastroControPartita")), campodbN(MyRiga.Item("ContoControPartita")), campodbN(MyRiga.Item("SottocontoControPartita")))
                            Else
                                Dim xAnno As Integer = campodbN(myPOSTreader.Item("AnnoCompetenza"))
                                Dim xMese As Integer = campodbN(myPOSTreader.Item("MeseCompetenza"))
                                Dim OMastro As Integer
                                Dim OConto As Integer
                                Dim OSottoconto As Integer
                                MySql = ""

                                OMastro = campodbN(MyRiga.Item("MastroControPartita"))
                                OConto = campodbN(MyRiga.Item("ContoControPartita"))
                                OSottoconto = campodbN(MyRiga.Item("SottocontoControPartita"))
                                CodiceOspite = Val(Fnvb6.CampoOspiteParenteMCS("CodiceOspite", OMastro, OConto, OSottoconto))


                                Dim DiurnoString As String = ""
                                Dim K As New Cls_Diurno

                                K.Anno = xAnno
                                K.Mese = xMese
                                K.CodiceOspite = CodiceOspite
                                K.CENTROSERVIZIO = campodb(myPOSTreader.Item("CentroServizio"))

                                Dim AppoggioMese As String = ""
                                AppoggioMese = K.LeggiMese(Session("DC_OSPITE"), K.CodiceOspite, K.CENTROSERVIZIO, K.Anno, K.Mese)

                                AppoggioMese = Mid(AppoggioMese, 1, GiorniMese(K.Mese, K.Anno) * 2)


                                Dim TrasportoDiurno1 As Integer = 0
                                Dim TrasportoDiurno2 As Integer = 0
                                Dim PresenzeFullTime As Integer = 0
                                Dim PresenzeMattina As Integer = 0
                                Dim PresenzePomeriggio As Integer = 0


                                TrasportoDiurno1 = quanteVolte(AppoggioMese, "t1") + quanteVolte(AppoggioMese, "M1") + quanteVolte(AppoggioMese, "P1")
                                TrasportoDiurno2 = quanteVolte(AppoggioMese, "t2")
                                PresenzeFullTime = quanteVolte(AppoggioMese, "  ") + quanteVolte(AppoggioMese, "t1") + quanteVolte(AppoggioMese, "t2")
                                PresenzeMattina = quanteVolte(AppoggioMese, "M1") + quanteVolte(AppoggioMese, "PM")
                                PresenzePomeriggio = quanteVolte(AppoggioMese, "P1") + quanteVolte(AppoggioMese, "PP")

                                If PresenzeFullTime > 0 Then

                                    DiurnoString = DiurnoString & vbNewLine

                                    DiurnoString = DiurnoString & "Presenze Full Time (20,19 Euro) :" & PresenzeFullTime
                                End If
                                If PresenzeMattina > 0 Then

                                    DiurnoString = DiurnoString & vbNewLine

                                    DiurnoString = DiurnoString & "Presenze Mattina (14,42 Euro) :" & PresenzeMattina
                                End If
                                If PresenzePomeriggio > 0 Then

                                    DiurnoString = DiurnoString & vbNewLine

                                    DiurnoString = DiurnoString & "Presenze Pomeriggio (8,65 Euro) :" & PresenzePomeriggio
                                End If

                                If TrasportoDiurno1 > 0 Then

                                    DiurnoString = DiurnoString & vbNewLine

                                    DiurnoString = DiurnoString & "1° Trasporto :" & TrasportoDiurno1
                                End If
                                If TrasportoDiurno2 > 0 Then

                                    DiurnoString = DiurnoString & vbNewLine

                                    DiurnoString = DiurnoString & "2° Trasporti :" & TrasportoDiurno2
                                End If


                                DescrizioneAppoggio = Fnvb6.DecodificaConto(campodbN(MyRiga.Item("MastroPartita")), campodbN(MyRiga.Item("ContoPartita")), campodbN(MyRiga.Item("SottocontoPartita"))) & " " & Fnvb6.DecodificaConto(campodbN(MyRiga.Item("MastroControPartita")), campodbN(MyRiga.Item("ContoControPartita")), campodbN(MyRiga.Item("SottocontoControPartita"))) & " " & DiurnoString
                            End If
                        End If
                        AppoggioRiga = AppoggioRiga & DescrizioneAppoggio.Replace(vbNewLine, " ") & ";"
                    End If


                    If Riferimento = "Prealpina" Then
                        If campodbN(MyRiga.Item("RigaDaCausale")) = 9 Then
                            DescrizioneAppoggio = Fnvb6.DecodificaConto(campodbN(MyRiga.Item("MastroPartita")), campodbN(MyRiga.Item("ContoPartita")), campodbN(MyRiga.Item("SottocontoPartita")))
                        Else
                            Dim Sotto As Integer = Int(campodbN(MyRiga.Item("SottocontoContropartita")) / 100) * 100
                            If Mid(Tipo & Space(10), 1, 1) = "C" Then
                                DescrizioneAppoggio = Fnvb6.DecodificaConto(campodbN(MyRiga.Item("MastroContropartita")), campodbN(MyRiga.Item("ContoContropartita")), Sotto) & " " & campodb(MyRiga.Item("Descrizione")) & " " & Fnvb6.DescrizioneRigaOspite(Int(campodbN(MyRiga.Item("SottocontoControPartita")) / 100), Anno, Mese)
                            Else
                                DescrizioneAppoggio = Fnvb6.DecodificaConto(campodbN(MyRiga.Item("MastroContropartita")), campodbN(MyRiga.Item("ContoContropartita")), Sotto) & " " & campodb(MyRiga.Item("Descrizione"))
                            End If
                        End If
                        AppoggioRiga = AppoggioRiga & DescrizioneAppoggio.Replace(vbNewLine, " ") & ";"
                    End If

                    If DescrizioneAppoggio = "" Then
                        If campodb(MyRiga.Item("Descrizione")).ToUpper.IndexOf("PRESENZE") >= 0 Or campodb(MyRiga.Item("Descrizione")).ToUpper.IndexOf("ASSENZE") >= 0 Then
                            AppoggioRiga = AppoggioRiga & campodb(MyRiga.Item("Descrizione")) & " " & DecConto.Descrizione & ";" 'DO30_DESCART
                        Else
                            AppoggioRiga = AppoggioRiga & Trim(DecConto.Descrizione & " " & campodb(MyRiga.Item("Descrizione"))) & ";" 'DO30_DESCART
                        End If
                    End If




                    Dim PConti As New Cls_Pianodeiconti

                    PConti.Mastro = campodbN(MyRiga.Item("MastroPartita"))
                    PConti.Conto = campodbN(MyRiga.Item("ContoPartita"))
                    PConti.Sottoconto = campodbN(MyRiga.Item("SottocontoPartita"))
                    PConti.Decodfica(Session("DC_GENERALE"))

                    AppoggioRiga = AppoggioRiga & PConti.Descrizione & ";" 'DO30_DESCART_SEN

                    AppoggioRiga = AppoggioRiga & "" & ";" 'DO30_UM1 UNITA MISURA
                    AppoggioRiga = AppoggioRiga & campodbN(MyRiga.Item("Quantita")) & ";"
                    AppoggioRiga = AppoggioRiga & 0 & ";" 'DO30_COLLI

                    If campodbN(MyRiga.Item("RigaDaCausale")) = 1 Then
                        If CausaleContabile.TipoDocumento = "NC" Then
                            If campodb(MyRiga.Item("DareAVere")) = "A" Then
                                AppoggioRiga = AppoggioRiga & campodbN(MyRiga.Item("Importo")) & ";" 'DO30_PREZZO
                            Else
                                AppoggioRiga = AppoggioRiga & "-" & campodbN(MyRiga.Item("Importo")) & ";" 'DO30_PREZZO
                            End If
                        Else
                            If campodb(MyRiga.Item("DareAVere")) = "D" Then
                                AppoggioRiga = AppoggioRiga & campodbN(MyRiga.Item("Importo")) & ";" 'DO30_PREZZO
                            Else
                                AppoggioRiga = AppoggioRiga & "-" & campodbN(MyRiga.Item("Importo")) & ";" 'DO30_PREZZO
                            End If
                        End If
                    Else
                        If CausaleContabile.TipoDocumento = "NC" Then
                            If campodb(MyRiga.Item("DareAVere")) = "D" Then
                                AppoggioRiga = AppoggioRiga & campodbN(MyRiga.Item("Importo")) & ";" 'DO30_PREZZO
                            Else
                                AppoggioRiga = AppoggioRiga & "-" & campodbN(MyRiga.Item("Importo")) & ";" 'DO30_PREZZO
                            End If
                        Else
                            If campodb(MyRiga.Item("DareAVere")) = "A" Then
                                AppoggioRiga = AppoggioRiga & campodbN(MyRiga.Item("Importo")) & ";" 'DO30_PREZZO
                            Else
                                AppoggioRiga = AppoggioRiga & "-" & campodbN(MyRiga.Item("Importo")) & ";" 'DO30_PREZZO
                            End If
                        End If
                    End If



                    AppoggioRiga = AppoggioRiga & campodb(MyRiga.Item("CodiceIVA")) & ";" 'DO30_CODIVA_CODICE_SEN

                    Dim CIVA As New Cls_IVA

                    CIVA.Codice = campodb(MyRiga.Item("CodiceIVA"))
                    CIVA.Leggi(Session("DC_TABELLE"), CIVA.Codice)

                    AppoggioRiga = AppoggioRiga & CIVA.Descrizione & ";" 'DO30_CODIVA_NOME_SEN


                    If campodb(MyRiga.Item("CentroServizio")) = "" Then
                        AppoggioRiga = AppoggioRiga & campodb(myPOSTreader.Item("CentroServizio")) & ";"
                    Else
                        AppoggioRiga = AppoggioRiga & campodb(MyRiga.Item("CentroServizio")) & ";"
                    End If


                    If campodbN(MyRiga.Item("AnnoRiferimento")) = 0 Then
                        AppoggioRiga = AppoggioRiga & campodb(myPOSTreader.Item("AnnoCompetenza")) & ";"
                    Else
                        AppoggioRiga = AppoggioRiga & campodb(MyRiga.Item("AnnoRiferimento")) & ";"
                    End If

                    If campodbN(MyRiga.Item("MeseRiferimento")) = 0 Then
                        AppoggioRiga = AppoggioRiga & campodb(myPOSTreader.Item("MeseCompetenza")) & ";"
                    Else
                        AppoggioRiga = AppoggioRiga & campodb(MyRiga.Item("MeseRiferimento")) & ";"
                    End If



                    Entrato = True

                    tw.WriteLine(AppoggioRiga.Replace(vbNewLine, " "))

                    If campodbN(MyRiga.Item("RigaDaCausale")) = 1 Then
                        If Session("DC_OSPITE").ToString.ToUpper.IndexOf("Lebetulle".ToUpper) <= 0 Then
                            If AddDescrizione <> "" Then
                                Progressivo = Progressivo + 1
                                ProgressivoAssoluto = ProgressivoAssoluto + 1


                                AppoggioRiga = "R" & ";"  'DO_TIPO_RIGA
                                AppoggioRiga = AppoggioRiga & ProgressivoAssoluto & ";" 'DO30_ID
                                AppoggioRiga = AppoggioRiga & campodbN(myPOSTreader.Item("Id")) & ";" 'DO30_ID_SEN
                                AppoggioRiga = AppoggioRiga & ProgressivoTesta & ";" 'DO30_ID_DO11
                                AppoggioRiga = AppoggioRiga & campodbN(myPOSTreader.Item("NumeroRegistrazione")) & ";" 'DO30_ID_DO11_SEN
                                AppoggioRiga = AppoggioRiga & CodiceInstallazione & ";" 'DO30_CODICE_INS
                                AppoggioRiga = AppoggioRiga & CodiceDitta & ";" ' 'DO30_DITTA_CG18
                                AppoggioRiga = AppoggioRiga & Progressivo & ";" 'DO30_PROGRIGA
                                AppoggioRiga = AppoggioRiga & "1" & ";" 'DO30_INDTIPORIGA - TIPO RIGA
                                AppoggioRiga = AppoggioRiga & "" & ";" 'DO30_CODART_MG66 - NON IN USO
                                AppoggioRiga = AppoggioRiga & ";" 'DO30_CODART_MG66_SEN
                                AppoggioRiga = AppoggioRiga & "" & ";" 'DO30_OPZIONE_MG5E NON IN USO
                                AppoggioRiga = AppoggioRiga & AddDescrizione & ";" 'DO30_DESCART
                                AppoggioRiga = AppoggioRiga & ";" 'DO30_DESCART_SEN
                                AppoggioRiga = AppoggioRiga & "" & ";" 'DO30_UM1 UNITA MISURA
                                AppoggioRiga = AppoggioRiga & "0;"
                                AppoggioRiga = AppoggioRiga & 0 & ";" 'DO30_COLLI
                                AppoggioRiga = AppoggioRiga & "0;" 'DO30_PREZZO
                                AppoggioRiga = AppoggioRiga & ";" 'DO30_CODIVA_CODICE_SEN
                                AppoggioRiga = AppoggioRiga & ";" 'DO30_CODIVA_NOME_SEN
                                AppoggioRiga = AppoggioRiga & ";" 'CSERV
                                AppoggioRiga = AppoggioRiga & ";" 'ANNOCOMP
                                AppoggioRiga = AppoggioRiga & ";" 'MESECOMP

                                tw.WriteLine(AppoggioRiga.Replace(vbNewLine, " "))
                            End If
                        End If
                    End If
                Loop
                MyRiga.Close()

            End If


            If Entrato = False Then
                Entrato = False

            End If

        Loop
        myPOSTreader.Close()
        cn.Close()
        tw.Close()
        'twR.Close()
        Fnvb6.ChiudiDB()
        tw = Nothing
        'twR = Nothing

        Dim tr As System.IO.TextReader = System.IO.File.OpenText(NomeFile)
        Dim Appo As String
        Dim ImportoTotale As Double = 0
        Dim ImportoRighe As Double = 0
        Dim ImportoAppoggio As Double = 0
        Dim UltimaReg As String
        Dim UltimaRegPerErrore As String = ""

        Do
            Dim VettoreCampi(100) As String


            Appo = tr.ReadLine()




            If Not IsNothing(Appo) Then
                VettoreCampi = Appo.Split(";")
                If VettoreCampi(0) = "T" Then
                    If Math.Abs(Math.Round(ImportoRighe, 2) - Math.Round(ImportoTotale, 2)) > 0.1 Then
                        ErroriEsportazione = ErroriEsportazione & "Registrazione non quadra in esportazione - " & UltimaRegPerErrore & " Diffrenza : " & Math.Round(ImportoRighe, 2) & "-" & Math.Round(ImportoTotale, 2) & "<br/>"
                    End If

                    UltimaRegPerErrore = VettoreCampi(2)
                    ImportoRighe = 0
                    ImportoTotale = 0
                End If
                If VettoreCampi(0) = "R" Then
                    If ImportoTotale = 0 Then
                        Try
                            ImportoTotale = campodbN(VettoreCampi(17))
                        Catch ex As Exception
                            ImportoTotale = 0
                        End Try

                        UltimaReg = campodbN(VettoreCampi(2))
                    Else
                        Try
                            ImportoAppoggio = campodbN(VettoreCampi(17))
                        Catch ex As Exception
                            ImportoTotale = 0
                        End Try
                        If VettoreCampi(18) = "05" Or VettoreCampi(18) = "25" Then
                            ImportoAppoggio = ImportoAppoggio + Math.Round(ImportoAppoggio * 5 / 100, 2)
                        End If
                        If VettoreCampi(18) = "04" Or VettoreCampi(18) = "24" Then
                            ImportoAppoggio = ImportoAppoggio + Math.Round(ImportoAppoggio * 4 / 100, 2)
                        End If
                        If VettoreCampi(18) = "11" Then
                            ImportoAppoggio = ImportoAppoggio + Math.Round(ImportoAppoggio * 10 / 100, 2)
                        End If
                        If VettoreCampi(18) = "22" Then
                            ImportoAppoggio = ImportoAppoggio + Math.Round(ImportoAppoggio * 22 / 100, 2)
                        End If
                        ImportoRighe = ImportoRighe + ImportoAppoggio
                    End If
                End If
            End If
        Loop While Not IsNothing(Appo)
        If Math.Abs(Math.Round(ImportoRighe, 2) - Math.Round(ImportoTotale, 2)) > 0.1 Then
            ErroriEsportazione = ErroriEsportazione & "Registrazione non quadra in esportazione!<BR/>"
        End If
        tr.Close()

        Lbl_Errori.Text = ErroriEsportazione


    End Sub

    Function campodbD(ByVal oggetto As Object) As Date
        If IsDBNull(oggetto) Then
            Return Nothing
        Else
            Return oggetto
        End If
    End Function
    Function campodb(ByVal oggetto As Object) As String
        If IsDBNull(oggetto) Then
            Return ""
        Else
            Return oggetto
        End If
    End Function

    Function campodbN(ByVal oggetto As Object) As Double
        If IsDBNull(oggetto) Then
            Return 0
        Else
            Return oggetto
        End If
    End Function


    Private Sub EsportaSeac()

        Dim cn As OleDbConnection

        Dim MeseContr As Long
        Dim MySql As String
        Dim ProgressivoAssoluto As Integer = 0


        MeseContr = DD_Mese.SelectedValue


        MySql = "SELECT * " & _
                " FROM MovimentiContabiliTesta " & _
                " WHERE  AnnoProtocollo = " & Txt_AnnoRif.Text & " And (Tipologia Is Null OR Tipologia = '' Or Tipologia = 'O' Or Tipologia = 'P' OR Tipologia Like 'C%')"

        If DD_Registro.SelectedValue <> "" Then
            MySql = MySql & " AND MovimentiContabiliTesta.RegistroIva = " & DD_Registro.SelectedValue
        End If
        If Tab_PeriodoData.ActiveTabIndex = 1 And IsDate(Txt_DataAl1.Text) And IsDate(Txt_DataDal1.Text) Then
            MySql = MySql & " AND (DataRegistrazione >= ? And DataRegistrazione <= ?) "
        Else
            MySql = MySql & " And MovimentiContabiliTesta.AnnoCompetenza = " & Txt_Anno.Text & " AND MovimentiContabiliTesta.MeseCompetenza = " & MeseContr & " "
        End If
        MySql = MySql & " AND (NumeroProtocollo >= " & Txt_DalDocumento.Text & " And NumeroProtocollo <= " & Txt_AlDocumento.Text & ") "




        cn = New Data.OleDb.OleDbConnection(Session("DC_GENERALE"))

        cn.Open()
        Dim cmd As New OleDbCommand()
        cmd.CommandText = MySql & " Order by NumeroProtocollo"
        cmd.Connection = cn
        If Tab_PeriodoData.ActiveTabIndex = 1 And IsDate(Txt_DataAl1.Text) And IsDate(Txt_DataDal1.Text) Then
            cmd.Parameters.AddWithValue("@DataDal", Txt_DataDal1.Text)
            cmd.Parameters.AddWithValue("@DataDal", Txt_DataAl1.Text)
        End If


        Dim CodiceInstallazione As Integer
        Dim CodiceDitta As Integer
        Dim ProgressivoTesta As Integer
        Dim RegistroIva As Integer

        Dim RigaNumero As Integer

        CodiceInstallazione = 1
        CodiceDitta = 1

        RegistroIva = DD_Registro.SelectedValue
        Dim NomeFile As String
        'Dim NomeFileRiga As String
        NomeFile = HostingEnvironment.ApplicationPhysicalPath() & "\Public\Esportazione_" & CodiceDitta & "_" & RegistroIva & "_" & Format(Now, "yyyyMMddHHmm") & ".Txt"
        'NomeFileRiga = HostingEnvironment.ApplicationPhysicalPath() & "\Riga_" & CodiceDitta & "_" & Format(Now, "yyyyMMddHHmm") & ".Txt"

        Dim tw As System.IO.TextWriter = System.IO.File.CreateText(NomeFile)
        'Dim twR As System.IO.TextWriter = System.IO.File.CreateText(NomeFileRiga)



        'twR.WriteLine(AppoggioRiga)


        Dim Appoggio As String = ""

        Appoggio = Appoggio & "ATT;REG;SEZ;DATA OPER;N.PROTO;PROG.P;RIGA;DATA DOC;N.DOC;CLIFOR;ALIQ;TIT.INAP;IMPONIB;IVA;SETT.;DARE;AVERE;VOCE IVA;FUNZ;DESCR CLIFOR;NOTE;CODFISCALE;PARTITAIVA;NOMINATIVO;INDIRIZZO;CAP;COMUNE;PROV;TIPO CLIFOR;SOGG 770;72000TR CLIFOR;NUM STATO EST;COD STATO"


        tw.WriteLine(Appoggio)

        RigaNumero = 1

        Dim myPOSTreader As OleDbDataReader = cmd.ExecuteReader()
        Do While myPOSTreader.Read
            ProgressivoTesta = ProgressivoTesta + 1


            Dim Cliente As String = ""

            Dim Tipo As String = campodb(myPOSTreader.Item("Tipologia"))
            Dim CodiceOspite As Integer = 0
            Dim CodiceParente As Integer = 0

            Dim CodiceProvincia As String = ""
            Dim CodiceComune As String = ""
            Dim CodiceRegione As String = ""


            Dim cmdRd As New OleDbCommand()
            cmdRd.CommandText = "Select * From MovimentiContabiliRiga Where Numero = ?  And Tipo ='CF'   "
            cmdRd.Connection = cn
            cmdRd.Parameters.AddWithValue("@Numero", campodbN(myPOSTreader.Item("NumeroRegistrazione")))
            Dim MyReadSC As OleDbDataReader = cmdRd.ExecuteReader()
            If MyReadSC.Read Then
                Cliente = campodb(MyReadSC.Item("MastroPartita")) & "." & campodb(MyReadSC.Item("ContoPartita")) & "." & campodb(MyReadSC.Item("SottocontoPartita"))
                If Mid(Tipo & Space(10), 1, 1) = " " Then
                    CodiceOspite = Int(campodbN(MyReadSC.Item("SottocontoPartita")) / 100)
                    CodiceParente = campodbN(MyReadSC.Item("SottocontoPartita")) - (CodiceOspite * 100)

                    If CodiceParente = 0 And CodiceOspite > 0 Then
                        Tipo = "O" & Space(10)
                    End If
                    If CodiceParente > 0 And CodiceOspite > 0 Then
                        Tipo = "P" & Space(10)
                    End If
                End If
                If Mid(Tipo & Space(10), 1, 1) = "C" Then
                    CodiceProvincia = Mid(Tipo & Space(10), 2, 3)
                    CodiceComune = Mid(Tipo & Space(10), 5, 3)
                End If
                If Mid(Tipo & Space(10), 1, 1) = "J" Then
                    CodiceProvincia = Mid(Tipo & Space(10), 2, 3)
                    CodiceComune = Mid(Tipo & Space(10), 5, 3)
                End If
                If Mid(Tipo & Space(10), 1, 1) = "R" Then
                    CodiceRegione = Mid(Tipo & Space(10), 2, 4)
                End If

            End If
            MyReadSC.Close()

            Appoggio = Appoggio & "" & ";" 'DO11_CLIFOR_CG44
            Appoggio = Appoggio & Cliente & ";"
            Dim DO11_RAGIONESOCIALE As String = ""
            Dim DO11_PIVA As String = ""
            Dim DO11_CF As String = ""
            Dim DO11_INDIRIZZO As String = ""
            Dim DO11_CITTA_NOME As String = ""
            Dim DO11_CITTA_ISTAT As String = ""
            Dim DO11_PROVINCIA As String = ""
            Dim DO11_CAP As String = ""
            Dim DO11_CONDPAG_CODICE_SEN As String = ""
            Dim DO11_CONDPAG_NOME_SEN As String = ""
            Dim DO11_BANCA_CIN As String = ""
            Dim DO11_BANCA_ABI As String = ""
            Dim DO11_BANCA_CAB As String = ""
            Dim DO11_BANCA_CONTO As String = ""
            Dim DO11_BANCA_IBAN As String = ""
            Dim DO11_BANCA_IDMANDATO As String = ""
            Dim DO11_BANCA_DATAMANDATO As String = ""
            Dim AddDescrizione As String

            AddDescrizione = ""
            If Mid(Tipo & Space(10), 1, 1) = "O" Then
                Dim Ospite As New ClsOspite

                Ospite.CodiceOspite = CodiceOspite
                Ospite.Leggi(Session("DC_OSPITE"), Ospite.CodiceOspite)
                DO11_RAGIONESOCIALE = Ospite.Nome
                DO11_PIVA = ""
                DO11_CF = Ospite.CODICEFISCALE
                DO11_INDIRIZZO = Ospite.RESIDENZAINDIRIZZO1
                Dim DcCom As New ClsComune

                DO11_CAP = Ospite.RESIDENZACAP1

                DcCom.Provincia = Ospite.RESIDENZAPROVINCIA1
                DcCom.Comune = Ospite.RESIDENZACOMUNE1
                DcCom.Leggi(Session("DC_OSPITE"))

                DO11_CITTA_NOME = DcCom.Descrizione
                DO11_CITTA_ISTAT = DcCom.Provincia & DcCom.Comune


                Dim DcProv As New ClsComune

                DcProv.Provincia = Ospite.RESIDENZAPROVINCIA1
                DcProv.Comune = ""
                DcProv.Leggi(Session("DC_OSPITE"))
                DO11_PROVINCIA = DcProv.CodificaProvincia


                Dim Kl As New Cls_DatiOspiteParenteCentroServizio

                Kl.CodiceOspite = CodiceOspite
                Kl.CodiceParente = 0
                Kl.CentroServizio = campodb(myPOSTreader.Item("CentroServizio"))
                Kl.Leggi(Session("DC_OSPITE"))
                If Kl.ModalitaPagamento <> "" Then
                    Ospite.MODALITAPAGAMENTO = Kl.ModalitaPagamento
                End If


                DO11_CONDPAG_CODICE_SEN = Ospite.MODALITAPAGAMENTO
                Dim MPAg As New ClsModalitaPagamento

                MPAg.Codice = Ospite.MODALITAPAGAMENTO
                MPAg.Leggi(Session("DC_OSPITE"))
                DO11_CONDPAG_NOME_SEN = MPAg.DESCRIZIONE


                Dim ModPag As New Cls_DatiPagamento

                ModPag.CodiceOspite = Ospite.CodiceOspite
                ModPag.CodiceParente = 0
                ModPag.LeggiUltimaDataData(Session("DC_OSPITE"), campodb(myPOSTreader.Item("DataDocumento")))

                DO11_BANCA_CIN = ModPag.Cin
                DO11_BANCA_ABI = ModPag.Abi
                DO11_BANCA_CAB = ModPag.Cin
                DO11_BANCA_CONTO = ModPag.CCBancario
                DO11_BANCA_IBAN = ModPag.CodiceInt & Format(Val(ModPag.NumeroControllo), "00") & ModPag.Cin & Format(Val(ModPag.Abi), "00000") & Format(Val(ModPag.Cab), "00000") & AdattaLunghezzaNumero(ModPag.CCBancario, 12)

                DO11_BANCA_IDMANDATO = ModPag.IdMandatoDebitore
                DO11_BANCA_DATAMANDATO = ModPag.DataMandatoDebitore
            End If

            If Mid(Tipo & Space(10), 1, 1) = "P" Then
                Dim Ospite As New Cls_Parenti

                Ospite.CodiceOspite = CodiceOspite
                Ospite.CodiceParente = CodiceParente
                Ospite.Leggi(Session("DC_OSPITE"), Ospite.CodiceOspite, Ospite.CodiceParente)
                DO11_RAGIONESOCIALE = Ospite.Nome
                DO11_PIVA = ""
                DO11_CF = Ospite.CODICEFISCALE
                DO11_INDIRIZZO = Ospite.RESIDENZAINDIRIZZO1
                Dim DcCom As New ClsComune

                DcCom.Provincia = Ospite.RESIDENZAPROVINCIA1
                DcCom.Comune = Ospite.RESIDENZACOMUNE1
                DcCom.Leggi(Session("DC_OSPITE"))

                DO11_CITTA_NOME = DcCom.Descrizione
                DO11_CITTA_ISTAT = DcCom.Provincia & DcCom.Comune
                DO11_CAP = Ospite.RESIDENZACAP1

                Dim DcProv As New ClsComune

                DcProv.Provincia = Ospite.RESIDENZAPROVINCIA1
                DcProv.Comune = ""
                DcProv.Leggi(Session("DC_OSPITE"))
                DO11_PROVINCIA = DcProv.CodificaProvincia
                DO11_CONDPAG_CODICE_SEN = Ospite.MODALITAPAGAMENTO

                Dim Kl As New Cls_DatiOspiteParenteCentroServizio

                Kl.CodiceOspite = CodiceOspite
                Kl.CodiceParente = CodiceParente
                Kl.CentroServizio = campodb(myPOSTreader.Item("CentroServizio"))
                Kl.Leggi(Session("DC_OSPITE"))
                If Kl.ModalitaPagamento <> "" Then
                    Ospite.MODALITAPAGAMENTO = Kl.ModalitaPagamento
                End If

                Dim MPAg As New ClsModalitaPagamento

                MPAg.Codice = Ospite.MODALITAPAGAMENTO
                MPAg.Leggi(Session("DC_OSPITE"))
                DO11_CONDPAG_NOME_SEN = MPAg.DESCRIZIONE

                Dim ModPag As New Cls_DatiPagamento

                ModPag.CodiceOspite = Ospite.CodiceOspite
                ModPag.CodiceParente = Ospite.CodiceParente
                ModPag.LeggiUltimaDataData(Session("DC_OSPITE"), campodb(myPOSTreader.Item("DataDocumento")))

                DO11_BANCA_CIN = ModPag.Cin
                DO11_BANCA_ABI = ModPag.Abi
                DO11_BANCA_CAB = ModPag.Cin
                DO11_BANCA_CONTO = ModPag.CCBancario
                DO11_BANCA_IBAN = ModPag.CodiceInt & Format(Val(ModPag.NumeroControllo), "00") & ModPag.Cin & Format(Val(ModPag.Abi), "00000") & Format(Val(ModPag.Cab), "00000") & AdattaLunghezzaNumero(ModPag.CCBancario, 12)

                DO11_BANCA_IDMANDATO = ModPag.IdMandatoDebitore
                DO11_BANCA_DATAMANDATO = ModPag.DataMandatoDebitore
            End If
            If Mid(Tipo & Space(10), 1, 1) = "C" Then
                Dim Ospite As New ClsComune

                Ospite.Provincia = CodiceProvincia
                Ospite.Comune = CodiceComune
                Ospite.Leggi(Session("DC_OSPITE"))
                DO11_RAGIONESOCIALE = Ospite.Descrizione
                DO11_PIVA = Ospite.PartitaIVA
                DO11_CF = Ospite.CodiceFiscale
                DO11_INDIRIZZO = Ospite.RESIDENZAINDIRIZZO1
                DO11_CAP = Ospite.RESIDENZACAP1

                Dim DcCom As New ClsComune

                DcCom.Provincia = Ospite.RESIDENZAPROVINCIA1
                DcCom.Comune = Ospite.RESIDENZACOMUNE1
                DcCom.Leggi(Session("DC_OSPITE"))

                DO11_CITTA_NOME = DcCom.Descrizione
                DO11_CITTA_ISTAT = DcCom.Provincia & DcCom.Comune

                Dim DcProv As New ClsComune

                DcProv.Provincia = Ospite.RESIDENZAPROVINCIA1
                DcProv.Comune = ""
                DcProv.Leggi(Session("DC_OSPITE"))
                DO11_PROVINCIA = DcProv.CodificaProvincia
                DO11_CONDPAG_CODICE_SEN = Ospite.ModalitaPagamento
                Dim MPAg As New ClsModalitaPagamento

                MPAg.Codice = Ospite.ModalitaPagamento
                MPAg.Leggi(Session("DC_OSPITE"))
                DO11_CONDPAG_NOME_SEN = MPAg.DESCRIZIONE

                Dim cnOspiti As OleDbConnection

                cnOspiti = New Data.OleDb.OleDbConnection(Session("DC_OSPITE"))

                cnOspiti.Open()

                Dim cmdC As New OleDbCommand()
                cmdC.CommandText = ("select * from NoteFatture where CodiceProvincia = ? And CodiceComune = ?")
                cmdC.Parameters.AddWithValue("@CodiceProvincia", CodiceProvincia)
                cmdC.Parameters.AddWithValue("@CodiceComune", CodiceComune)
                cmdC.Connection = cnOspiti
                Dim RdCom As OleDbDataReader = cmdC.ExecuteReader()
                If RdCom.Read Then
                    AddDescrizione = campodb(RdCom.Item("NoteUp"))
                End If
                RdCom.Close()
                cnOspiti.Close()
            End If
            If Mid(Tipo & Space(10), 1, 1) = "J" Then
                Dim Ospite As New ClsComune

                Ospite.Provincia = CodiceProvincia
                Ospite.Comune = CodiceComune
                Ospite.Leggi(Session("DC_OSPITE"))
                DO11_RAGIONESOCIALE = Ospite.Descrizione
                DO11_PIVA = Ospite.PartitaIVA
                DO11_CF = Ospite.CodiceFiscale
                DO11_INDIRIZZO = Ospite.RESIDENZAINDIRIZZO1
                DO11_CAP = Ospite.RESIDENZACAP1
                Dim DcCom As New ClsComune

                DcCom.Provincia = Ospite.RESIDENZAPROVINCIA1
                DcCom.Comune = Ospite.RESIDENZACOMUNE1
                DcCom.Leggi(Session("DC_OSPITE"))

                DO11_CITTA_NOME = DcCom.Descrizione
                DO11_CITTA_ISTAT = DcCom.Provincia & DcCom.Comune

                Dim DcProv As New ClsComune

                DcProv.Provincia = Ospite.RESIDENZAPROVINCIA1
                DcProv.Comune = ""
                DcProv.Leggi(Session("DC_OSPITE"))
                DO11_PROVINCIA = DcProv.CodificaProvincia
                DO11_CONDPAG_CODICE_SEN = Ospite.ModalitaPagamento
                Dim MPAg As New ClsModalitaPagamento

                MPAg.Codice = Ospite.ModalitaPagamento
                MPAg.Leggi(Session("DC_OSPITE"))
                DO11_CONDPAG_NOME_SEN = MPAg.DESCRIZIONE

                Dim cnOspiti As OleDbConnection

                cnOspiti = New Data.OleDb.OleDbConnection(Session("DC_OSPITE"))

                cnOspiti.Open()

                Dim cmdC As New OleDbCommand()
                cmdC.CommandText = ("select * from NoteFatture where CodiceProvincia = ? And CodiceComune = ?")
                cmdC.Parameters.AddWithValue("@CodiceProvincia", CodiceProvincia)
                cmdC.Parameters.AddWithValue("@CodiceComune", CodiceComune)
                cmdC.Connection = cnOspiti
                Dim RdCom As OleDbDataReader = cmdC.ExecuteReader()
                If RdCom.Read Then
                    AddDescrizione = campodb(RdCom.Item("NoteUp"))
                End If
                RdCom.Close()
                cnOspiti.Close()
            End If
            If Mid(Tipo & Space(10), 1, 1) = "R" Then
                Dim Ospite As New ClsUSL

                Ospite.CodiceRegione = CodiceRegione
                Ospite.Leggi(Session("DC_OSPITE"))
                DO11_RAGIONESOCIALE = Ospite.Nome
                DO11_PIVA = Ospite.PARTITAIVA
                DO11_CF = Ospite.CodiceFiscale
                DO11_INDIRIZZO = Ospite.RESIDENZAINDIRIZZO1
                Dim DcCom As New ClsComune

                DcCom.Provincia = Ospite.RESIDENZAPROVINCIA1
                DcCom.Comune = Ospite.RESIDENZACOMUNE1
                DcCom.Leggi(Session("DC_OSPITE"))

                DO11_CITTA_NOME = DcCom.Descrizione
                DO11_CITTA_ISTAT = DcCom.Provincia & DcCom.Comune

                Dim DcProv As New ClsComune

                DcProv.Provincia = Ospite.RESIDENZAPROVINCIA1
                DcProv.Comune = ""
                DcProv.Leggi(Session("DC_OSPITE"))
                DO11_PROVINCIA = DcProv.CodificaProvincia
                DO11_CONDPAG_CODICE_SEN = Ospite.ModalitaPagamento
                Dim MPAg As New ClsModalitaPagamento

                MPAg.Codice = Ospite.ModalitaPagamento
                MPAg.Leggi(Session("DC_OSPITE"))
                DO11_CONDPAG_NOME_SEN = MPAg.DESCRIZIONE
            End If


            Dim M As New Cls_TipoPagamento

            M.Codice = campodb(myPOSTreader.Item("ModalitaPagamento"))
            M.Leggi(Session("DC_TABELLE"))
            If M.Descrizione <> "" Then
                DO11_CONDPAG_CODICE_SEN = M.Codice
                DO11_CONDPAG_NOME_SEN = M.Descrizione
            End If


            Dim Progressivo As Integer = 0
            Dim Entrato As Boolean = False
            RigaNumero = 0
            Dim cmdRiga As New OleDbCommand()
            cmdRiga.CommandText = "Select MastroPartita,ContoPartita,Sottocontopartita,DareAvere,CodiceIVA,sum(importo) AS tOTALE from MovimentiContabiliRiga Where Numero = ? And RigaDaCausale > 2 Group by MastroPartita,ContoPartita,Sottocontopartita,CodiceIVA,DareAvere"
            cmdRiga.Connection = cn
            cmdRiga.Parameters.AddWithValue("@Numero", campodbN(myPOSTreader.Item("NumeroRegistrazione")))
            Dim MyRiga As OleDbDataReader = cmdRiga.ExecuteReader()
            Do While MyRiga.Read
                Dim TITINAP As String
                Progressivo = Progressivo + 1
                ProgressivoAssoluto = ProgressivoAssoluto + 1

                Appoggio = "0" & ";" 'ATT
                Appoggio = Appoggio & "E" & ";"  'REG

                If campodbN(myPOSTreader.Item("RegistroIva")) = 2 Then
                    Appoggio = Appoggio & "4" & ";" ' SEZIONALE
                Else
                    If UCase(Session("NomeEPersonam")) = "BINOTTO" Then
                        Appoggio = Appoggio & "2" & ";" ' SEZIONALE
                    Else
                        Appoggio = Appoggio & "0" & ";" ' SEZIONALE
                    End If
                End If

                RigaNumero = RigaNumero + 1
                Appoggio = Appoggio & Format(campodbD(myPOSTreader.Item("DataDocumento")), "ddMMyy") & ";" 'DATA DOCUMETNO

                Appoggio = Appoggio & AdattaLunghezzaNumero(campodb(myPOSTreader.Item("NumeroProtocollo")), 5) & ";" 'NUMEROPROTOCOLLO
                Appoggio = Appoggio & "0" & ";" ' PROGRESSIVO PROTOCOLLO
                Appoggio = Appoggio & RigaNumero & ";" ' RIGA PROTOCOLLO

                Appoggio = Appoggio & Format(campodbD(myPOSTreader.Item("DataDocumento")), "ddMMyy") & ";" 'DATA DOCUMETNO
                Appoggio = Appoggio & AdattaLunghezzaNumero(campodb(myPOSTreader.Item("NumeroProtocollo")), 5) & ";" 'NUMEROPROTOCOLLO

                Appoggio = Appoggio & "" & ";" 'CLIFOR

                TITINAP = "  "
                If campodb(MyRiga.Item("CodiceIva")) = "04" Then
                    TITINAP = "04"
                End If
                If campodb(MyRiga.Item("CodiceIva")) = "11" Then
                    TITINAP = "10"
                End If
                If campodb(MyRiga.Item("CodiceIva")) = "22" Then
                    TITINAP = "22"
                End If

                Appoggio = Appoggio & TITINAP & ";" ' CODICE IVA (DA LASCIARE?)
                'Riga = Riga & MoveFromDb(RsSt!Importo) & ";"
                Dim CODIVA As String = ""
                TITINAP = ""

                If campodb(MyRiga.Item("CodiceIva")) = "10" Then
                    TITINAP = "10"
                End If
                If campodb(MyRiga.Item("CodiceIva")) = "15" Then
                    TITINAP = "15"
                End If
                Appoggio = Appoggio & TITINAP & ";"
                Dim Totale As Double = campodbN(MyRiga.Item("Totale"))
                Appoggio = Appoggio & AdattaLunghezzaNumero(Math.Round(Totale, 2), 11) & ";" 'IMPONIBILE

                If TITINAP = "10" Or TITINAP = "15" Then
                    Appoggio = Appoggio & AdattaLunghezzaNumero(0, 10) & ";" ' IVA ZERO
                End If
                If campodb(MyRiga.Item("CodiceIva")) = "11" Then
                    Appoggio = Appoggio & AdattaLunghezzaNumero(Math.Round(Totale * 10 / 100, 2), 10) & ";" ' CALCOLO IVA
                End If
                If campodb(MyRiga.Item("CodiceIva")) = "04" Then
                    Appoggio = Appoggio & AdattaLunghezzaNumero(Math.Round(Totale * 4 / 100, 2), 10) & ";" ' CALCOLO IVA
                End If
                If campodb(MyRiga.Item("CodiceIva")) = "22" Then
                    Appoggio = Appoggio & AdattaLunghezzaNumero(Math.Round(Totale * 22 / 100, 2), 10) & ";" ' CALCOLO IVA
                End If
                Appoggio = Appoggio & "" & ";" 'SETTORE
                Dim Conto As Integer

                Conto = 310740
                If UCase(Session("NomeEPersonam")) = "BINOTTO" Then
                    Conto = 310742
                End If

                If campodb(MyRiga.Item("SottoContoPartita")) = 700102 Then
                    Conto = 375000 ' RILIEVI SANITARI
                End If

                If campodb(MyRiga.Item("SottoContoPartita")) = 810118 Then
                    Conto = 310740  'FERMO SALMA
                End If
                If campodb(MyRiga.Item("SottoContoPartita")) = 700101 Then
                    REM   Conto = 310740  'FERMO SALMA
                End If

                If campodb(MyRiga.Item("SottoContoPartita")) = 810101 Then
                    Conto = 371000   'PASTO FAMILIARI
                    If UCase(Session("NomeEPersonam")) = "BINOTTO" Then
                        Conto = 371001   'PASTO FAMILIARI
                    End If
                End If

                If campodb(MyRiga.Item("SottoContoPartita")) = 810105 Then
                    Conto = 371082  'SPESE TELEFONICHE
                End If

                If campodb(MyRiga.Item("SottoContoPartita")) = 810102 Then
                    Conto = 371080  'FARMACI
                    If UCase(Session("NomeEPersonam")) = "BINOTTO" Then
                        Conto = 371082
                    End If
                End If

                If campodb(MyRiga.Item("SottoContoPartita")) = 810105 Then
                    Conto = 371082  'RIMBORSO LAVANDERIA
                End If

                If campodb(MyRiga.Item("SottoContoPartita")) = 810126 Then
                    Conto = 51201   'RIMBORSO LAVANDERIA ANTICIPATE
                End If

                If campodb(MyRiga.Item("SottoContoPartita")) = 810105 Then
                    Conto = 371082   'CANONE TV
                End If


                If campodb(MyRiga.Item("SottoContoPartita")) = 810119 Then
                    Conto = 371080   'TRASPORTO
                    If UCase(Session("NomeEPersonam")) = "BINOTTO" Then
                        Conto = 371082
                    End If
                End If
                If campodb(MyRiga.Item("SottoContoPartita")) = 810120 Then
                    Conto = 371080  'CONTO SPESE PREALPINA
                End If

                If campodb(MyRiga.Item("SottoContoPartita")) = 810105 Then
                    Conto = 371080  'CONTO SPESE
                    If UCase(Session("NomeEPersonam")) = "BINOTTO" Then
                        Conto = 371082
                    End If
                End If

                If campodb(MyRiga.Item("SottoContoPartita")) = 810126 Then
                    Conto = 51200  'SPESE ANTICIPATE PARRUCCHIERA
                    If UCase(Session("NomeEPersonam")) = "BINOTTO" Then
                        Conto = 51201
                    End If
                End If




                If campodb(MyRiga.Item("SottoContoPartita")) = 840102 Then
                    Conto = 371170    ' BOLLO
                End If

                Dim Invertito As Boolean

                Invertito = False
                If campodb(MyRiga.Item("DareAvere")) = "D" Then
                    Appoggio = Appoggio & AdattaLunghezzaNumero(Conto, 6) & ";"
                    Invertito = True
                Else
                    Appoggio = Appoggio & "051000;" ' AdattaLunghezzaNumero(0, 6) & ";"
                End If

                If campodb(MyRiga.Item("DareAvere")) = "A" Then
                    Appoggio = Appoggio & AdattaLunghezzaNumero(Conto, 6) & ";"
                Else
                    Appoggio = Appoggio & "051000;" 'AdattaLunghezzaNumero(0, 6) & ";"
                    Invertito = True
                End If

                If campodb(MyRiga.Item("CodiceIva")) = "11" Then
                    TITINAP = "M01"
                End If
                If campodb(MyRiga.Item("CodiceIva")) = "10" Then
                    TITINAP = "M10"
                End If
                If campodb(MyRiga.Item("CodiceIva")) = "04" Then
                    TITINAP = "M01"
                End If
                If campodb(MyRiga.Item("CodiceIva")) = "22" Then
                    TITINAP = "M01"
                End If
                If campodb(MyRiga.Item("CodiceIva")) = "15" Then
                    TITINAP = "M88"
                End If
                If TITINAP = "M01" And (Appoggio = "R" Or Appoggio = "C" Or Appoggio = "J") Then
                    TITINAP = "M04"
                End If


                Appoggio = Appoggio & TITINAP & ";"
                If Invertito = True Then
                    Appoggio = Appoggio & "6" & ";"
                Else
                    Appoggio = Appoggio & " " & ";"
                End If

                Appoggio = Appoggio & AdattaLunghezza(DO11_RAGIONESOCIALE, 40) & ";"
                Appoggio = Appoggio & Space(20) & ";"

                Appoggio = Appoggio & DO11_CF & ";"
                Appoggio = Appoggio & DO11_PIVA & ";"
                Appoggio = Appoggio & DO11_RAGIONESOCIALE & ";"
                Appoggio = Appoggio & DO11_INDIRIZZO & ";"
                Appoggio = Appoggio & DO11_CAP & ";"
                Appoggio = Appoggio & DO11_CITTA_NOME & ";"
                Appoggio = Appoggio & DO11_PROVINCIA & ";"

                Dim StAppoggio As String = ""
                StAppoggio = Tipo & Space(10)

                If Mid(StAppoggio, 1, 1) = "C" Or Mid(StAppoggio, 1, 1) = "R" Then
                    Appoggio = Appoggio & "T" & ";"
                Else
                    Appoggio = Appoggio & "P" & ";"
                End If
                Appoggio = Appoggio & " " & ";"
                Appoggio = Appoggio & " " & ";"
                Appoggio = Appoggio & "000" & ";"
                Appoggio = Appoggio & "  " & ";"

                tw.WriteLine(Appoggio)

            Loop
            MyRiga.Close()

            If Entrato = False Then
                Entrato = False

            End If

        Loop
        myPOSTreader.Close()
        cn.Close()
        tw.Close()
        'twR.Close()
        tw = Nothing
        'twR = Nothing
        Try
            Response.Clear()
            Response.Buffer = True
            Response.ContentType = "text/plain"
            Response.AppendHeader("content-disposition", "attachment;filename=output.txt")
            Response.WriteFile(NomeFile)
            Response.Flush()
            Response.End()
        Finally
            Kill(NomeFile)

        End Try

    End Sub


    Function AdattaLunghezzaNumero(ByVal Testo As String, ByVal Lunghezza As Integer) As String
        If Len(Testo) > Lunghezza Then
            AdattaLunghezzaNumero = Mid(Testo, 1, Lunghezza)
            Exit Function
        End If
        AdattaLunghezzaNumero = Replace(Space(Lunghezza - Len(Testo)) & Testo, " ", "0")
        REM AdattaLunghezzaNumero = Space(Lunghezza - Len(Testo)) & Testo


    End Function

    Function AdattaLunghezza(ByVal Testo As String, ByVal Lunghezza As Integer) As String

        If Len(Testo) > Lunghezza Then
            Testo = Mid(Testo, 1, Lunghezza)
        End If

        AdattaLunghezza = Testo & Space(Lunghezza - Len(Testo) + 1)

        If Len(AdattaLunghezza) > Lunghezza Then
            AdattaLunghezza = Mid(AdattaLunghezza, 1, Lunghezza)
        End If

    End Function

    Public Function FtpFileExists(ByVal Address As String, ByVal UserName As String, ByVal Password As String) As FtpResult
        Try
            Dim request As System.Net.FtpWebRequest = DirectCast(System.Net.WebRequest.Create(Address), System.Net.FtpWebRequest)
            request.Credentials = New System.Net.NetworkCredential(UserName, Password)
            request.GetResponse()
            Return FtpResult.Exists
        Catch ex As Exception
            If UserName = "" Then Return FtpResult.BlankUserName
            If Password = "" Then Return FtpResult.BlankPassword
            If ex.Message.IndexOf("530") > 0 Then Return FtpResult.InvalidLogin
            If Address.ToLower.IndexOf("ftp://ftp") = -1 Then Return FtpResult.InvalidUrl
            If Address = String.Empty Then Return FtpResult.InvalidUrl
            Return FtpResult.DoesNotExist
        End Try
    End Function

    Enum FtpResult
        Exists
        DoesNotExist
        InvalidLogin
        InvalidUrl
        BlankPassword
        BlankUserName
    End Enum

    Public Sub UploadFile(ByVal _FileName As String, ByVal _UploadPath As String, ByVal _FTPUser As String, ByVal _FTPPass As String)
        Dim _FileInfo As New System.IO.FileInfo(_FileName)

        ' Create FtpWebRequest object from the Uri provided
        Dim _FtpWebRequest As System.Net.FtpWebRequest = CType(System.Net.FtpWebRequest.Create(New Uri(_UploadPath)), System.Net.FtpWebRequest)

        ' Provide the WebPermission Credintials
        _FtpWebRequest.Credentials = New System.Net.NetworkCredential(_FTPUser, _FTPPass)

        ' By default KeepAlive is true, where the control connection is not closed
        ' after a command is executed.
        _FtpWebRequest.KeepAlive = False

        ' set timeout for 20 seconds
        _FtpWebRequest.Timeout = 20000

        ' Specify the command to be executed.
        _FtpWebRequest.Method = System.Net.WebRequestMethods.Ftp.UploadFile

        ' Specify the data transfer type.
        _FtpWebRequest.UseBinary = True

        ' Notify the server about the size of the uploaded file
        _FtpWebRequest.ContentLength = _FileInfo.Length

        ' The buffer size is set to 2kb
        Dim buffLength As Integer = 2048
        Dim buff(buffLength - 1) As Byte

        ' Opens a file stream (System.IO.FileStream) to read the file to be uploaded
        Dim _FileStream As System.IO.FileStream = _FileInfo.OpenRead()


        ' Stream to which the file to be upload is written
        Dim _Stream As System.IO.Stream = _FtpWebRequest.GetRequestStream()

        ' Read from the file stream 2kb at a time
        Dim contentLen As Integer = _FileStream.Read(buff, 0, buffLength)

        ' Till Stream content ends
        Do While contentLen <> 0
            ' Write Content from the file stream to the FTP Upload Stream
            _Stream.Write(buff, 0, contentLen)
            contentLen = _FileStream.Read(buff, 0, buffLength)
        Loop

        ' Close the file stream and the Request Stream
        _Stream.Close()
        _Stream.Dispose()
        _FileStream.Close()
        _FileStream.Dispose()

    End Sub


    Sub Export_PassePartout()
        Dim cn As OleDbConnection
        Dim MyRs As New ADODB.Recordset
        Dim RsIva As New ADODB.Recordset

        Dim RsSt As New ADODB.Recordset
        Dim UtilVb6 As New Cls_FunzioniVB6

        Dim CampoTesta(500)
        Dim CampoRiga(200)


        Dim NomeFile As String
        Dim NomeFileRighe As String
        Dim I As Integer


        Session("MYDATE") = Now

        NomeFile = HostingEnvironment.ApplicationPhysicalPath() & "\Public\Testata_" & Format(Session("MYDATE"), "yyyyMMddHHmm") & ".Txt"
        NomeFileRighe = HostingEnvironment.ApplicationPhysicalPath() & "\Public\Righe_" & Format(Session("MYDATE"), "yyyyMMddHHmm") & ".Txt"


        UtilVb6.StringaOspiti = Session("DC_OSPITE")
        UtilVb6.StringaGenerale = Session("DC_GENERALE")
        UtilVb6.StringaTabelle = Session("DC_TABELLE")

        UtilVb6.ApriDB()


        Dim tw As System.IO.TextWriter = System.IO.File.CreateText(NomeFile)
        Dim twRighe As System.IO.TextWriter = System.IO.File.CreateText(NomeFileRighe)

        cn = New Data.OleDb.OleDbConnection(Session("DC_GENERALE"))

        cn.Open()


        I = 0
        I = I + 1 : CampoTesta(I) = "PNPRN"
        I = I + 1 : CampoTesta(I) = "PNDRE"
        I = I + 1 : CampoTesta(I) = "PNOPE"
        I = I + 1 : CampoTesta(I) = "PNCAU"
        I = I + 1 : CampoTesta(I) = "CAUGUI"
        I = I + 1 : CampoTesta(I) = "PNTDE"
        I = I + 1 : CampoTesta(I) = "PNRPR"
        I = I + 1 : CampoTesta(I) = "PNSPR"
        I = I + 1 : CampoTesta(I) = "PNNPR"
        I = I + 1 : CampoTesta(I) = "PNRPS"
        I = I + 1 : CampoTesta(I) = "PNSPS"
        I = I + 1 : CampoTesta(I) = "PNNPS"
        I = I + 1 : CampoTesta(I) = "PNINT"
        I = I + 1 : CampoTesta(I) = "SIGVAL"
        I = I + 1 : CampoTesta(I) = "PNEST"
        I = I + 1 : CampoTesta(I) = "SERDOC"
        I = I + 1 : CampoTesta(I) = "PNNDO"
        I = I + 1 : CampoTesta(I) = "PNDDO"
        I = I + 1 : CampoTesta(I) = "DTFTPG"
        I = I + 1 : CampoTesta(I) = "NREGIP"
        I = I + 1 : CampoTesta(I) = "GIORN"
        I = I + 1 : CampoTesta(I) = "REGIP"
        I = I + 1 : CampoTesta(I) = "REGIS"
        I = I + 1 : CampoTesta(I) = "PNBIL"
        I = I + 1 : CampoTesta(I) = "PNDST"
        I = I + 1 : CampoTesta(I) = "DAPARC"
        I = I + 1 : CampoTesta(I) = "USOINT1"
        I = I + 1 : CampoTesta(I) = "USOINT2"
        I = I + 1 : CampoTesta(I) = "USOINT3"
        I = I + 1 : CampoTesta(I) = "USOINT4"
        I = I + 1 : CampoTesta(I) = "USOINT5"
        I = I + 1 : CampoTesta(I) = "_PNGCO"
        I = I + 1 : CampoTesta(I) = "_PNNVA"
        I = I + 1 : CampoTesta(I) = "GESTCORR"
        I = I + 1 : CampoTesta(I) = "VERS "
        I = I + 1 : CampoTesta(I) = "UTENTE"
        I = I + 1 : CampoTesta(I) = "DATAINVIO"
        I = I + 1 : CampoTesta(I) = "ORAINVIO "
        I = I + 1 : CampoTesta(I) = "PNDAVERIF"
        I = I + 1 : CampoTesta(I) = "PNPROVEN"
        I = I + 1 : CampoTesta(I) = "PNCODSAZ"
        I = I + 1 : CampoTesta(I) = "PNNUMRE"
        I = I + 1 : CampoTesta(I) = "PNNVABL"
        I = I + 1 : CampoTesta(I) = "PNNVPBL"
        I = I + 1 : CampoTesta(I) = "PNCTRBL"
        I = I + 1 : CampoTesta(I) = "PNVRP"
        I = I + 1 : CampoTesta(I) = "PNIOPBL"
        I = I + 1 : CampoTesta(I) = "PNEOPBL"
        I = I + 1 : CampoTesta(I) = "PNDCOBL"
        I = I + 1 : CampoTesta(I) = "PNNIDPN"
        I = I + 1 : CampoTesta(I) = "PNAIDPN"
        I = I + 1 : CampoTesta(I) = "PNDRV"
        I = I + 1 : CampoTesta(I) = "PNEDPE"
        I = I + 1 : CampoTesta(I) = "PNFCS"
        I = I + 1 : CampoTesta(I) = "PNTDER"

        Dim Stringa As String = ""

        For Z = 1 To I
            Stringa = Stringa & CampoTesta(Z) & ";"
        Next Z
        tw.WriteLine(Stringa)

        I = 0
        I = I + 1 : CampoRiga(I) = "PNPRN"
        I = I + 1 : CampoRiga(I) = "PNDRE"
        I = I + 1 : CampoRiga(I) = "PNCTO"
        I = I + 1 : CampoRiga(I) = "PNIMP"
        I = I + 1 : CampoRiga(I) = "PNCCR"
        I = I + 1 : CampoRiga(I) = "PNDES"
        I = I + 1 : CampoRiga(I) = "PNIMB1"
        I = I + 1 : CampoRiga(I) = "PNIVA1"
        I = I + 1 : CampoRiga(I) = "PNALI1"
        I = I + 1 : CampoRiga(I) = "PNALD1"
        I = I + 1 : CampoRiga(I) = "PNIMB2"
        I = I + 1 : CampoRiga(I) = "PNIVA2"
        I = I + 1 : CampoRiga(I) = "PNALI2"
        I = I + 1 : CampoRiga(I) = "PNALD2"
        I = I + 1 : CampoRiga(I) = "PNIMB3"
        I = I + 1 : CampoRiga(I) = "PNIVA3"
        I = I + 1 : CampoRiga(I) = "PNALI3"
        I = I + 1 : CampoRiga(I) = "PNALD3"
        I = I + 1 : CampoRiga(I) = "PNIMB4"
        I = I + 1 : CampoRiga(I) = "PNIVA4"
        I = I + 1 : CampoRiga(I) = "PNALI4"
        I = I + 1 : CampoRiga(I) = "PNALD4"
        I = I + 1 : CampoRiga(I) = "CODPAG"
        I = I + 1 : CampoRiga(I) = "CODAGE"
        I = I + 1 : CampoRiga(I) = "IMPPRO"
        I = I + 1 : CampoRiga(I) = "USOINT1"
        I = I + 1 : CampoRiga(I) = "USOINT2"
        I = I + 1 : CampoRiga(I) = "USOINT3"
        I = I + 1 : CampoRiga(I) = "USOINT4"
        I = I + 1 : CampoRiga(I) = "USOINT5"
        I = I + 1 : CampoRiga(I) = "USOINT6"
        I = I + 1 : CampoRiga(I) = "USOINT7"
        I = I + 1 : CampoRiga(I) = "USOINT8"
        I = I + 1 : CampoRiga(I) = "USOINT9"
        I = I + 1 : CampoRiga(I) = "PNEDS"
        I = I + 1 : CampoRiga(I) = "PNEDN"
        I = I + 1 : CampoRiga(I) = "PNEDC"
        I = I + 1 : CampoRiga(I) = "PNDDO"
        I = I + 1 : CampoRiga(I) = "PNCND1"
        I = I + 1 : CampoRiga(I) = "PNCND2"
        I = I + 1 : CampoRiga(I) = "PNCND3"
        I = I + 1 : CampoRiga(I) = "PNCND4"
        I = I + 1 : CampoRiga(I) = "PNRSVA"
        I = I + 1 : CampoRiga(I) = "PNRDN"
        I = I + 1 : CampoRiga(I) = "PNRDVA"
        I = I + 1 : CampoRiga(I) = "PNORCO"
        I = I + 1 : CampoRiga(I) = "PNECAC"
        I = I + 1 : CampoRiga(I) = "PNCFPI"
        I = I + 1 : CampoRiga(I) = "PNCCS"
        I = I + 1 : CampoRiga(I) = "PNCCTR"
        I = I + 1 : CampoRiga(I) = "PNTPC"

        Stringa = ""
        For Z = 1 To I
            Stringa = Stringa & CampoRiga(Z) & ";"
        Next Z
        twRighe.WriteLine(Stringa)

        Dim Condizione As String = ""

        Dim NumeroRegistrazioni As Integer


        NumeroRegistrazioni = 0

        Dim cmd As New OleDbCommand()

        If Val(Dd_RegistroDATA.SelectedValue) = 0 Then
            cmd.CommandText = "Select * From MovimentiContabiliTesta where (select count(*) from MovimentiContabiliRiga Where Numero = MovimentiContabiliTesta.NumeroRegistrazione  And RigaBollato = 1) =  0 And DataRegistrazione >= ? And DataRegistrazione <= ? And (RegistroIva = 1 Or RegistroIva = 0)"
        Else
            cmd.CommandText = "Select * From MovimentiContabiliTesta where (select count(*) from MovimentiContabiliRiga Where Numero = MovimentiContabiliTesta.NumeroRegistrazione  And RigaBollato = 1) =  0 And DataRegistrazione >= ? And DataRegistrazione <= ? And (RegistroIva = " & Val(Dd_RegistroDATA.SelectedValue) & " Or RegistroIva = 0)"
        End If
        cmd.Parameters.AddWithValue("@DataDal", Txt_DataDal.Text)
        cmd.Parameters.AddWithValue("@DataAl", Txt_DataAl.Text)
        cmd.Connection = cn


        Dim MyRsDB As OleDbDataReader = cmd.ExecuteReader()

        Do While MyRsDB.Read

            NumeroRegistrazioni = NumeroRegistrazioni + 1

            If Chk_Prova.Checked = False Then
                Dim cmdBollato As New OleDbCommand()
                cmdBollato.CommandText = "Update  MovimentiContabiliRiga Set RigaBollato =1  Where Numero = " & campodbN(MyRsDB.Item("NumeroRegistrazione"))
                cmdBollato.Connection = cn
                cmdBollato.ExecuteNonQuery()
            End If

            I = 0
            I = I + 1 : CampoTesta(I) = campodbN(MyRsDB.Item("NumeroRegistrazione")) '1
            I = I + 1 : CampoTesta(I) = Format(campodbD(MyRsDB.Item("DataRegistrazione")), "ddMMyyyy") '2
            I = I + 1 : CampoTesta(I) = 0 '3

            If campodbN(MyRsDB.Item("NumeroProtocollo")) > 0 Then
                I = I + 1 : CampoTesta(I) = "FE" '4
            Else
                I = I + 1 : CampoTesta(I) = "IC" '4
            End If
            I = I + 1 : CampoTesta(I) = -1 '5
            I = I + 1 : CampoTesta(I) = "" '6
            If campodbN(MyRsDB.Item("NumeroProtocollo")) > 0 Then
                I = I + 1 : CampoTesta(I) = "V" '7
            Else
                I = I + 1 : CampoTesta(I) = "" '7
            End If
            If campodbN(MyRsDB.Item("NumeroProtocollo")) > 0 Then
                If Dd_RegistroDATA.SelectedValue = 6 Then
                    I = I + 1 : CampoTesta(I) = "3" '8
                Else
                    I = I + 1 : CampoTesta(I) = "2" '8
                End If
            Else
                I = I + 1 : CampoTesta(I) = "" '8
            End If
            If campodbN(MyRsDB.Item("NumeroProtocollo")) > 0 Then
                I = I + 1 : CampoTesta(I) = campodbN(MyRsDB.Item("NumeroProtocollo")) '9
            Else
                I = I + 1 : CampoTesta(I) = "" '9
            End If

            I = I + 1 : CampoTesta(I) = "" '10
            I = I + 1 : CampoTesta(I) = "" '11
            I = I + 1 : CampoTesta(I) = "" '12
            I = I + 1 : CampoTesta(I) = "N" '13
            I = I + 1 : CampoTesta(I) = "." '14
            I = I + 1 : CampoTesta(I) = "" '15

            If campodbN(MyRsDB.Item("NumeroProtocollo")) > 0 Then
                I = I + 1 : CampoTesta(I) = "2" '16
                I = I + 1 : CampoTesta(I) = campodbN(MyRsDB.Item("NumeroProtocollo"))  '17
                I = I + 1 : CampoTesta(I) = Format(campodbD(MyRsDB.Item("DataRegistrazione")), "ddMMyyyy") '18
            Else
                I = I + 1 : CampoTesta(I) = "2" '16



                Dim NumeroProtocollo As Integer = 0
                Dim DataFattura As Date

                Dim cmd1 As New OleDbCommand()
                cmd1.CommandText = "Select * from TabellaLegami Where CodicePagamento = " & campodbN(MyRsDB.Item("NumeroRegistrazione"))
                cmd1.Connection = cn

                Dim rst As OleDbDataReader = cmd1.ExecuteReader()
                If rst.Read Then
                    Dim k As New Cls_MovimentoContabile

                    k.Leggi(Session("DC_GENERALE"), campodbN(rst.Item("CodiceDocumento")))
                    NumeroProtocollo = k.NumeroProtocollo
                    DataFattura = k.DataRegistrazione
                End If
                rst.Close()

                If Year(DataFattura) = Year(campodbD(MyRsDB.Item("DataRegistrazione"))) Then
                    DataFattura = campodbD(MyRsDB.Item("DataRegistrazione"))
                End If

                I = I + 1 : CampoTesta(I) = NumeroProtocollo '17
                I = I + 1 : CampoTesta(I) = Format(DataFattura, "ddMMyyyy") '18
            End If

            I = I + 1 : CampoTesta(I) = "" '19
            I = I + 1 : CampoTesta(I) = "" '20
            I = I + 1 : CampoTesta(I) = "N" '21
            I = I + 1 : CampoTesta(I) = "N" '22
            I = I + 1 : CampoTesta(I) = "N" '23
            I = I + 1 : CampoTesta(I) = "" '24
            I = I + 1 : CampoTesta(I) = "" '25
            I = I + 1 : CampoTesta(I) = "N" '26
            I = I + 1 : CampoTesta(I) = "" '27
            I = I + 1 : CampoTesta(I) = "" '28
            I = I + 1 : CampoTesta(I) = "" '29
            I = I + 1 : CampoTesta(I) = "" '30
            I = I + 1 : CampoTesta(I) = "" '31
            I = I + 1 : CampoTesta(I) = "S" '32
            I = I + 1 : CampoTesta(I) = "N" '33
            I = I + 1 : CampoTesta(I) = "N" '34
            I = I + 1 : CampoTesta(I) = 0 '35
            I = I + 1 : CampoTesta(I) = "" '36
            I = I + 1 : CampoTesta(I) = Format(Now, "ddMMyyy") '37
            I = I + 1 : CampoTesta(I) = Format(Now, "hh:mm") '38
            I = I + 1 : CampoTesta(I) = "" '39
            I = I + 1 : CampoTesta(I) = "N" '40
            I = I + 1 : CampoTesta(I) = 1 '41
            I = I + 1 : CampoTesta(I) = "" '42
            I = I + 1 : CampoTesta(I) = "N" '43
            I = I + 1 : CampoTesta(I) = "N" '44
            I = I + 1 : CampoTesta(I) = "" '45
            I = I + 1 : CampoTesta(I) = "N" '46
            I = I + 1 : CampoTesta(I) = "N" '47
            I = I + 1 : CampoTesta(I) = "N" '48
            I = I + 1 : CampoTesta(I) = "" '49
            I = I + 1 : CampoTesta(I) = campodbN(MyRsDB.Item("NumeroRegistrazione"))  '50
            I = I + 1 : CampoTesta(I) = Year(campodbD(MyRsDB.Item("DataRegistrazione"))) '51
            I = I + 1 : CampoTesta(I) = "" '52
            I = I + 1 : CampoTesta(I) = "N" '53
            I = I + 1 : CampoTesta(I) = "" '54

            Stringa = ""
            For Z = 1 To I
                Stringa = Stringa & CampoTesta(Z) & ";"
            Next Z

            tw.WriteLine(Stringa)
            Dim CONTOCASSABANCA As String

            If campodbN(MyRsDB.Item("NumeroProtocollo")) = 0 Then
                If Year(campodbD(MyRsDB.Item("DataRegistrazione"))) >= 2015 Then
                    CONTOCASSABANCA = "20200001"
                Else
                    CONTOCASSABANCA = "20200001"
                End If


                Dim cmd2 As New OleDbCommand()
                cmd2.CommandText = "Select * from MovimentiContabiliRiga Where Numero = " & campodbN(MyRsDB.Item("NumeroRegistrazione"))
                cmd2.Connection = cn
                Dim rst1 As OleDbDataReader = cmd2.ExecuteReader()

                Do While rst1.Read
                    If campodbN(rst1.Item("MastroPartita")) = 1 And campodbN(rst1.Item("ContoPartita")) = 1 And campodbN(rst1.Item("SottoContoPartita")) = 1 Then
                        If Year(campodbD(MyRsDB.Item("DataRegistrazione"))) >= 2015 Then
                            CONTOCASSABANCA = "20101000"
                        Else
                            CONTOCASSABANCA = "20100102"
                        End If
                    End If
                    If campodbN(rst1.Item("MastroPartita")) = 1 And campodbN(rst1.Item("ContoPartita")) = 1 And campodbN(rst1.Item("SottoContoPartita")) = 2 Then
                        If Year(campodbD(MyRsDB.Item("DataRegistrazione"))) >= 2015 Then
                            CONTOCASSABANCA = "20100102"
                        Else
                            CONTOCASSABANCA = "20100103"
                        End If
                    End If

                    If campodbN(rst1.Item("MastroPartita")) = 1 And campodbN(rst1.Item("ContoPartita")) = 2 And campodbN(rst1.Item("SottoContoPartita")) = 1 Then
                        CONTOCASSABANCA = "20201004" 'CASSA RISPARMIO LUCCA PISA LIVORNO
                    End If

                Loop
                rst1.Close()



                Dim cmd3 As New OleDbCommand()
                cmd3.CommandText = "Select * from MovimentiContabiliRiga Where Numero = " & campodbN(MyRsDB.Item("NumeroRegistrazione"))
                cmd3.Connection = cn
                Dim rst2 As OleDbDataReader = cmd3.ExecuteReader()

                Do While rst2.Read

                    I = 0
                    I = I + 1 : CampoRiga(I) = campodbN(MyRsDB.Item("NumeroRegistrazione")) '1
                    I = I + 1 : CampoRiga(I) = Format(campodbD(MyRsDB.Item("DataRegistrazione")), "ddMMyyyy") '2
                    If campodb(rst2.Item("Tipo")) = "CF" Then
                        I = I + 1 : CampoRiga(I) = UtilVb6.CampoAnagraficaComune(campodbN(rst2.Item("MastroPartita")), campodbN(rst2.Item("ContoPartita")), campodbN(rst2.Item("SottoContoPartita")), "C", "CONTOPERESATTO") '3
                    Else
                        I = I + 1 : CampoRiga(I) = CONTOCASSABANCA
                    End If
                    If campodb(rst2.Item("DareAvere")) = "D" Then
                        I = I + 1 : CampoRiga(I) = Modulo.MathRound(campodbN(rst2.Item("Importo")), 2) '4
                    Else
                        I = I + 1 : CampoRiga(I) = Modulo.MathRound(campodbN(rst2.Item("Importo")), 2) * -1 '4
                    End If
                    If campodb(rst2.Item("CodiceIva")) = "05" Then
                        I = I + 1 : CampoRiga(I) = " 5,00" '9
                    Else
                        I = I + 1 : CampoRiga(I) = "E10" '9
                    End If
                    I = I + 1 : CampoRiga(I) = "" '6
                    I = I + 1 : CampoRiga(I) = "" '7
                    I = I + 1 : CampoRiga(I) = ""  '8
                    I = I + 1 : CampoRiga(I) = "" '9
                    I = I + 1 : CampoRiga(I) = "" '10
                    I = I + 1 : CampoRiga(I) = "" '11
                    I = I + 1 : CampoRiga(I) = "" '12
                    I = I + 1 : CampoRiga(I) = "" '13
                    I = I + 1 : CampoRiga(I) = "" '14
                    I = I + 1 : CampoRiga(I) = "" '15
                    I = I + 1 : CampoRiga(I) = "" '16


                    I = I + 1 : CampoRiga(I) = "" '17
                    I = I + 1 : CampoRiga(I) = "" '18

                    I = I + 1 : CampoRiga(I) = "" '19
                    I = I + 1 : CampoRiga(I) = "" '20
                    I = I + 1 : CampoRiga(I) = "" '21
                    I = I + 1 : CampoRiga(I) = "" '22
                    I = I + 1 : CampoRiga(I) = "" '23
                    I = I + 1 : CampoRiga(I) = "" '24
                    I = I + 1 : CampoRiga(I) = "" '25
                    I = I + 1 : CampoRiga(I) = "" '26
                    I = I + 1 : CampoRiga(I) = "" '27
                    I = I + 1 : CampoRiga(I) = "" '28
                    I = I + 1 : CampoRiga(I) = "" '29
                    I = I + 1 : CampoRiga(I) = "" '30
                    I = I + 1 : CampoRiga(I) = "" '31
                    I = I + 1 : CampoRiga(I) = "" '32
                    I = I + 1 : CampoRiga(I) = "" '33
                    I = I + 1 : CampoRiga(I) = "" '34
                    I = I + 1 : CampoRiga(I) = "" '35
                    I = I + 1 : CampoRiga(I) = "" '36
                    I = I + 1 : CampoRiga(I) = "" '37
                    I = I + 1 : CampoRiga(I) = "" '38
                    I = I + 1 : CampoRiga(I) = "N" '39
                    I = I + 1 : CampoRiga(I) = "N" '40
                    I = I + 1 : CampoRiga(I) = "N" '41
                    I = I + 1 : CampoRiga(I) = "N" '42
                    I = I + 1 : CampoRiga(I) = "" '43
                    I = I + 1 : CampoRiga(I) = "" '44
                    I = I + 1 : CampoRiga(I) = "" '45
                    I = I + 1 : CampoRiga(I) = "N" '46
                    I = I + 1 : CampoRiga(I) = "N" '47
                    I = I + 1 : CampoRiga(I) = "" '48
                    I = I + 1 : CampoRiga(I) = "" '49
                    I = I + 1 : CampoRiga(I) = "" '50
                    I = I + 1 : CampoRiga(I) = "" '51



                    Stringa = ""
                    For Z = 1 To I
                        Stringa = Stringa & CampoRiga(Z) & ";"
                    Next Z

                    twRighe.WriteLine(Stringa)

                Loop
                rst2.Close()
            End If



            If campodbN(MyRsDB.Item("NumeroProtocollo")) > 0 Then
                Dim Imponibile As Double = 0
                Dim Imposta As Double = 0


                Dim cmd3 As New OleDbCommand()
                cmd3.CommandText = "Select * from MovimentiContabiliRiga WHERE RigaDaCausale = 2 And Numero = " & campodbN(MyRsDB.Item("NumeroRegistrazione"))
                cmd3.Connection = cn
                Dim rst2 As OleDbDataReader = cmd3.ExecuteReader()

                Do While rst2.Read
                    Imponibile = Imponibile + Modulo.MathRound(campodbN(rst2.Item("Imponibile")), 2)
                    Imposta = Imposta + Modulo.MathRound(campodbN(rst2.Item("Importo")), 2)
                Loop
                rst2.Close()

                Dim cmd4 As New OleDbCommand()
                cmd4.CommandText = "Select * from MovimentiContabiliRiga WHERE RigaDaCausale = 1 And Numero = " & campodbN(MyRsDB.Item("NumeroRegistrazione"))
                cmd4.Connection = cn
                Dim rst4 As OleDbDataReader = cmd4.ExecuteReader()

                Do While rst4.Read

                    I = 0
                    I = I + 1 : CampoRiga(I) = campodbN(MyRsDB.Item("NumeroRegistrazione")) '1
                    I = I + 1 : CampoRiga(I) = Format(campodbD(MyRsDB.Item("DataRegistrazione")), "ddMMyyyy") '2

                    I = I + 1 : CampoRiga(I) = UtilVb6.CampoAnagraficaComune(campodbN(rst4.Item("MastroPartita")), campodbN(rst4.Item("ContoPartita")), campodbN(rst4.Item("SottoContoPartita")), "C", "CONTOPERESATTO") '3
                    If campodb(rst4.Item("DareAvere")) = "D" Then
                        I = I + 1 : CampoRiga(I) = Modulo.MathRound(campodbN(rst4.Item("Importo")), 2) '4
                    Else
                        I = I + 1 : CampoRiga(I) = Modulo.MathRound(campodbN(rst4.Item("Importo")) * -1, 2) '4
                    End If
                    If campodb(rst4.Item("CodiceIva")) = "05" Then
                        I = I + 1 : CampoRiga(I) = " 5,00" '9
                    Else
                        If campodb(rst4.Item("CodiceIva")) = "71" Then
                            I = I + 1 : CampoRiga(I) = "S15" '9
                        Else
                            I = I + 1 : CampoRiga(I) = "E10" '9
                        End If
                    End If
                    I = I + 1 : CampoRiga(I) = "" '6
                    I = I + 1 : CampoRiga(I) = "" '7
                    I = I + 1 : CampoRiga(I) = ""  '8
                    I = I + 1 : CampoRiga(I) = "" '9
                    I = I + 1 : CampoRiga(I) = "" '10
                    I = I + 1 : CampoRiga(I) = "" '11
                    I = I + 1 : CampoRiga(I) = "" '12
                    I = I + 1 : CampoRiga(I) = "" '13
                    I = I + 1 : CampoRiga(I) = "" '14
                    I = I + 1 : CampoRiga(I) = "" '15
                    I = I + 1 : CampoRiga(I) = "" '16
                    I = I + 1 : CampoRiga(I) = "" '17
                    I = I + 1 : CampoRiga(I) = "" '18
                    I = I + 1 : CampoRiga(I) = "" '19
                    I = I + 1 : CampoRiga(I) = "" '20
                    I = I + 1 : CampoRiga(I) = "" '21
                    I = I + 1 : CampoRiga(I) = "" '22
                    I = I + 1 : CampoRiga(I) = "" '23
                    I = I + 1 : CampoRiga(I) = "" '24
                    I = I + 1 : CampoRiga(I) = "" '25
                    I = I + 1 : CampoRiga(I) = "" '26
                    I = I + 1 : CampoRiga(I) = "" '27
                    I = I + 1 : CampoRiga(I) = "" '28
                    I = I + 1 : CampoRiga(I) = "" '29
                    I = I + 1 : CampoRiga(I) = "" '30
                    I = I + 1 : CampoRiga(I) = "" '31
                    I = I + 1 : CampoRiga(I) = "" '32
                    I = I + 1 : CampoRiga(I) = "" '33
                    I = I + 1 : CampoRiga(I) = "" '34
                    I = I + 1 : CampoRiga(I) = "" '35
                    I = I + 1 : CampoRiga(I) = "" '36
                    I = I + 1 : CampoRiga(I) = "" '37
                    I = I + 1 : CampoRiga(I) = "" '38
                    I = I + 1 : CampoRiga(I) = "N" '39
                    I = I + 1 : CampoRiga(I) = "N" '40
                    I = I + 1 : CampoRiga(I) = "N" '41
                    I = I + 1 : CampoRiga(I) = "N" '42
                    I = I + 1 : CampoRiga(I) = "" '43
                    I = I + 1 : CampoRiga(I) = "" '44
                    I = I + 1 : CampoRiga(I) = "" '45
                    I = I + 1 : CampoRiga(I) = "N" '46
                    I = I + 1 : CampoRiga(I) = "N" '47
                    I = I + 1 : CampoRiga(I) = "" '48
                    I = I + 1 : CampoRiga(I) = "" '49
                    I = I + 1 : CampoRiga(I) = "" '50
                    I = I + 1 : CampoRiga(I) = "" '51


                    Stringa = ""
                    For Z = 1 To I
                        Stringa = Stringa & CampoRiga(Z) & ";"
                    Next Z

                    twRighe.WriteLine(Stringa)
                Loop
                rst4.Close()


                Dim cmd5 As New OleDbCommand()
                cmd5.CommandText = "Select * from MovimentiContabiliRiga Where (RigaDaCausale = 3 OR RigaDaCausale = 13 OR RigaDaCausale = 5 OR RigaDaCausale = 6) And  Numero = " & campodbN(MyRsDB.Item("NumeroRegistrazione"))
                cmd5.Connection = cn
                Dim rst5 As OleDbDataReader = cmd5.ExecuteReader()

                Do While rst5.Read
                    I = 0
                    I = I + 1 : CampoRiga(I) = campodbN(MyRsDB.Item("NumeroRegistrazione")) '1
                    I = I + 1 : CampoRiga(I) = Format(campodbD(MyRsDB.Item("DataRegistrazione")), "ddMMyyyy") '2

                    'campodbN(MyRsDB.Item("NumeroRegistrazione"))
                    If campodbN(rst5.Item("SottoContoPartita")) = 5 And campodbN(rst5.Item("ContoPartita")) = 1 And campodbN(rst5.Item("MastroPartita")) = 21 Then
                        I = I + 1 : CampoRiga(I) = "20800221"
                    Else
                        I = I + 1 : CampoRiga(I) = "80300001"
                    End If



                    'CampoAnagraficaComune(MoveFromDb(MyRs!MastroPartita), MoveFromDb(MyRs!ContoPartita), MoveFromDb(MyRs!SottoContoPartita), "C", "CONTOPERESATTO") '3
                    If campodb(rst5.Item("DareAvere")) = "D" Then
                        I = I + 1 : CampoRiga(I) = Modulo.MathRound(campodbN(rst5.Item("Importo")), 2) '4
                    Else
                        I = I + 1 : CampoRiga(I) = Modulo.MathRound(campodbN(rst5.Item("Importo")) * -1, 2) '4
                    End If
                    I = I + 1 : CampoRiga(I) = 10 '5
                    I = I + 1 : CampoRiga(I) = "" '6
                    'I = I + 1: CampoRiga(I) = Round(Imponibile, 2) '7
                    I = I + 1 : CampoRiga(I) = Modulo.MathRound(campodbN(rst5.Item("Importo")), 2) '7

                    Dim CodiceIVA As New Cls_IVA

                    CodiceIVA.Codice = campodb(rst5.Item("CodiceIva"))
                    CodiceIVA.Leggi(Session("DC_TABELLE"), CodiceIVA.Codice)
                    If CodiceIVA.Aliquota > 0 Then
                        I = I + 1 : CampoRiga(I) = Modulo.MathRound(campodbN(rst5.Item("Importo")) * CodiceIVA.Aliquota, 2) '7
                    Else
                        I = I + 1 : CampoRiga(I) = Imposta  '8
                    End If


                    If campodb(rst5.Item("CodiceIva")) = "05" Then
                        I = I + 1 : CampoRiga(I) = " 5,00" '9
                    Else
                        If campodb(rst5.Item("CodiceIva")) = "71" Then
                            I = I + 1 : CampoRiga(I) = "S15" '9
                        Else
                            I = I + 1 : CampoRiga(I) = "E10" '9
                        End If
                    End If
                    I = I + 1 : CampoRiga(I) = "" '10
                    I = I + 1 : CampoRiga(I) = "" '11
                    I = I + 1 : CampoRiga(I) = "" '12
                    I = I + 1 : CampoRiga(I) = "" '13
                    I = I + 1 : CampoRiga(I) = "" '14
                    I = I + 1 : CampoRiga(I) = "" '15
                    I = I + 1 : CampoRiga(I) = "" '16
                    I = I + 1 : CampoRiga(I) = "" '17
                    I = I + 1 : CampoRiga(I) = "" '18
                    I = I + 1 : CampoRiga(I) = "" '19
                    I = I + 1 : CampoRiga(I) = "" '20
                    I = I + 1 : CampoRiga(I) = "" '21
                    I = I + 1 : CampoRiga(I) = "" '22
                    I = I + 1 : CampoRiga(I) = "" '23
                    I = I + 1 : CampoRiga(I) = "" '24
                    I = I + 1 : CampoRiga(I) = "" '25
                    I = I + 1 : CampoRiga(I) = "" '26
                    I = I + 1 : CampoRiga(I) = "" '27
                    I = I + 1 : CampoRiga(I) = "" '28
                    I = I + 1 : CampoRiga(I) = "" '29
                    I = I + 1 : CampoRiga(I) = "" '30
                    I = I + 1 : CampoRiga(I) = "" '31
                    I = I + 1 : CampoRiga(I) = "" '32
                    I = I + 1 : CampoRiga(I) = "" '33
                    I = I + 1 : CampoRiga(I) = "" '34
                    I = I + 1 : CampoRiga(I) = "" '35
                    I = I + 1 : CampoRiga(I) = "" '36
                    I = I + 1 : CampoRiga(I) = "" '37
                    I = I + 1 : CampoRiga(I) = "" '38
                    I = I + 1 : CampoRiga(I) = "N" '39
                    I = I + 1 : CampoRiga(I) = "N" '40
                    I = I + 1 : CampoRiga(I) = "N" '41
                    I = I + 1 : CampoRiga(I) = "N" '42
                    I = I + 1 : CampoRiga(I) = "" '43
                    I = I + 1 : CampoRiga(I) = "" '44
                    I = I + 1 : CampoRiga(I) = "" '45
                    I = I + 1 : CampoRiga(I) = "N" '46
                    I = I + 1 : CampoRiga(I) = "N" '47
                    I = I + 1 : CampoRiga(I) = "" '48
                    I = I + 1 : CampoRiga(I) = "" '49
                    I = I + 1 : CampoRiga(I) = "" '50
                    I = I + 1 : CampoRiga(I) = "" '51

                    Stringa = ""
                    For Z = 1 To I
                        Stringa = Stringa & CampoRiga(Z) & ";"
                    Next Z

                    twRighe.WriteLine(Stringa)

                Loop
                rst5.Close()

                'RsSt.Open("Select * from MovimentiContabiliRiga WHERE RigaDaCausale = 2 And Numero = " & MoveFromDb(MyRs!NumeroRegistrazione), GeneraleDb, adOpenKeyset, adLockOptimistic)

                Dim cmd6 As New OleDbCommand()
                cmd6.CommandText = "Select * from MovimentiContabiliRiga WHERE RigaDaCausale = 2 And Numero = " & campodbN(MyRsDB.Item("NumeroRegistrazione"))
                cmd6.Connection = cn
                Dim rst6 As OleDbDataReader = cmd6.ExecuteReader()

                Do While rst6.Read
                    Imponibile = Imponibile + Modulo.MathRound(campodbN(rst6.Item("Imponibile")), 2)
                    Imposta = Imposta + Modulo.MathRound(campodbN(rst6.Item("Importo")), 2)

                    I = 0
                    I = I + 1 : CampoRiga(I) = campodbN(rst6.Item("Numero"))  '1
                    I = I + 1 : CampoRiga(I) = Format(campodbD(MyRsDB.Item("DataRegistrazione")), "ddMMyyyy") '2
                    I = I + 1 : CampoRiga(I) = "41500012" ' CONTO IVA
                    If campodb(rst6.Item("DareAvere")) = "D" Then
                        I = I + 1 : CampoRiga(I) = Modulo.MathRound(campodbN(rst6.Item("Importo")), 2) '4
                    Else
                        I = I + 1 : CampoRiga(I) = Modulo.MathRound(campodbN(rst6.Item("Importo")) * -1, 2) '4
                    End If
                    I = I + 1 : CampoRiga(I) = 10 '5
                    I = I + 1 : CampoRiga(I) = "" '6
                    I = I + 1 : CampoRiga(I) = 0 '7
                    I = I + 1 : CampoRiga(I) = 0  '8
                    If campodb(rst6.Item("CodiceIva")) = "05" Then
                        I = I + 1 : CampoRiga(I) = " 5,00" '9
                    Else
                        I = I + 1 : CampoRiga(I) = "E10" '9
                    End If
                    I = I + 1 : CampoRiga(I) = "" '10
                    I = I + 1 : CampoRiga(I) = "" '11
                    I = I + 1 : CampoRiga(I) = "" '12
                    I = I + 1 : CampoRiga(I) = "" '13
                    I = I + 1 : CampoRiga(I) = "" '14
                    I = I + 1 : CampoRiga(I) = "" '15
                    I = I + 1 : CampoRiga(I) = "" '16
                    I = I + 1 : CampoRiga(I) = "" '17
                    I = I + 1 : CampoRiga(I) = "" '18
                    I = I + 1 : CampoRiga(I) = "" '19
                    I = I + 1 : CampoRiga(I) = "" '20
                    I = I + 1 : CampoRiga(I) = "" '21
                    I = I + 1 : CampoRiga(I) = "" '22
                    I = I + 1 : CampoRiga(I) = "" '23
                    I = I + 1 : CampoRiga(I) = "" '24
                    I = I + 1 : CampoRiga(I) = "" '25
                    I = I + 1 : CampoRiga(I) = "" '26
                    I = I + 1 : CampoRiga(I) = "" '27
                    I = I + 1 : CampoRiga(I) = "" '28
                    I = I + 1 : CampoRiga(I) = "" '29
                    I = I + 1 : CampoRiga(I) = "" '30
                    I = I + 1 : CampoRiga(I) = "" '31
                    I = I + 1 : CampoRiga(I) = "" '32
                    I = I + 1 : CampoRiga(I) = "" '33
                    I = I + 1 : CampoRiga(I) = "" '34
                    I = I + 1 : CampoRiga(I) = "" '35
                    I = I + 1 : CampoRiga(I) = "" '36
                    I = I + 1 : CampoRiga(I) = "" '37
                    I = I + 1 : CampoRiga(I) = "" '38
                    I = I + 1 : CampoRiga(I) = "N" '39
                    I = I + 1 : CampoRiga(I) = "N" '40
                    I = I + 1 : CampoRiga(I) = "N" '41
                    I = I + 1 : CampoRiga(I) = "N" '42
                    I = I + 1 : CampoRiga(I) = "" '43
                    I = I + 1 : CampoRiga(I) = "" '44
                    I = I + 1 : CampoRiga(I) = "" '45
                    I = I + 1 : CampoRiga(I) = "N" '46
                    I = I + 1 : CampoRiga(I) = "N" '47
                    I = I + 1 : CampoRiga(I) = "" '48
                    I = I + 1 : CampoRiga(I) = "" '49
                    I = I + 1 : CampoRiga(I) = "" '50
                    I = I + 1 : CampoRiga(I) = "" '51


                    Stringa = ""
                    For Z = 1 To I
                        Stringa = Stringa & CampoRiga(Z) & ";"
                    Next Z

                    twRighe.WriteLine(Stringa)

                Loop
                rst6.Close()
            End If

        Loop

        tw.Close()
        twRighe.Close()


        Btn_Testa2.Visible = True
        Btn_Riga2.Visible = True


    End Sub


    Private Sub EseguiJS()
        Dim MyJs As String
        MyJs = "$(document).ready(function() {"

        MyJs = MyJs & "var els = document.getElementsByTagName(""*"");"

        MyJs = MyJs & "for (var i=0;i<els.length;i++)"
        MyJs = MyJs & "if ( els[i].id ) { "
        MyJs = MyJs & " var appoggio =els[i].id; "


        MyJs = MyJs & " if ((appoggio.match('Txt_Importo')!= null)) {  "
        MyJs = MyJs & " $(els[i]).keypress(function() { ForceNumericInput($(this).val(), true, true); } ); "
        MyJs = MyJs & " $(els[i]).blur(function() { var ap = formatNumber($(this).val(),2); $(this).val(ap); });"
        MyJs = MyJs & "    }"

        MyJs = MyJs & " if (appoggio.match('Txt_Data')!= null) {  "
        MyJs = MyJs & " $(els[i]).mask(""99/99/9999"");"
        MyJs = MyJs & " $(els[i]).datepicker({ changeMonth: true,changeYear: true,  yearRange: ""1990:" & Year(Now) + 5 & """},$.datepicker.regional[""it""]);"
        MyJs = MyJs & "    }"


        MyJs = MyJs & "} "



        MyJs = MyJs & "});"

        'MyJs = "$(document).ready(function() { $('.myClass').keypress(function() { handleEnter($('.myClass').val(), true, true); } );     $('#" & Txt_ImportoPagato.ClientID & "').blur(function() { var ap = formatNumber ($('#" & Txt_ImportoPagato.ClientID & "').val(),2); $('#" & Txt_ImportoPagato.ClientID & "').val(ap); }); });"
        ScriptManager.RegisterStartupScript(Me, Me.GetType(), "visualizzaRitIm", MyJs, True)
    End Sub

    Sub Export_Multi()

        Dim cn As OleDbConnection

        Dim MeseContr As Long
        Dim MySql As String
        Dim ProgressivoAssoluto As Integer = 0


        MeseContr = DD_Mese.SelectedValue


        MySql = "SELECT * " & _
                " FROM MovimentiContabiliTesta " & _
                " WHERE AnnoProtocollo = " & Txt_AnnoRif.Text

        If DD_Registro.SelectedValue <> "" Then
            MySql = MySql & " AND MovimentiContabiliTesta.RegistroIva = " & DD_Registro.SelectedValue
        End If

        If Tab_PeriodoData.ActiveTabIndex = 1 And IsDate(Txt_DataAl1.Text) And IsDate(Txt_DataDal1.Text) Then
            MySql = MySql & " AND (DataRegistrazione >= ? And DataRegistrazione <= ?) "
        Else
            MySql = MySql & " And MovimentiContabiliTesta.AnnoCompetenza = " & Txt_Anno.Text & " AND MovimentiContabiliTesta.MeseCompetenza = " & MeseContr & " "
        End If

        MySql = MySql & " AND (NumeroProtocollo >= " & Txt_DalDocumento.Text & " And NumeroProtocollo <= " & Txt_AlDocumento.Text & ") "


        cn = New Data.OleDb.OleDbConnection(Session("DC_GENERALE"))

        cn.Open()
        Dim cmd As New OleDbCommand()
        cmd.CommandText = MySql & " Order by NumeroProtocollo"
        cmd.Connection = cn
        If Tab_PeriodoData.ActiveTabIndex = 1 And IsDate(Txt_DataAl1.Text) And IsDate(Txt_DataDal1.Text) Then
            cmd.Parameters.AddWithValue("@DataDal", Txt_DataDal1.Text)
            cmd.Parameters.AddWithValue("@DataDal", Txt_DataAl1.Text)
        End If


        Dim CodiceInstallazione As Integer
        Dim CodiceDitta As Integer
        Dim ProgressivoTesta As Integer
        Dim RegistroIva As Integer
        Dim APPOGGIO As String

        CodiceInstallazione = 1
        CodiceDitta = 1

        RegistroIva = DD_Registro.SelectedValue
        Dim NomeFile As String
        'Dim NomeFileRiga As String
        NomeFile = HostingEnvironment.ApplicationPhysicalPath() & "\Public\Esportazione_" & Session("NomeEPersonam") & "_" & RegistroIva & "_" & Format(Now, "yyyyMMddHHmm") & ".Txt"
        'NomeFileRiga = HostingEnvironment.ApplicationPhysicalPath() & "\Riga_" & CodiceDitta & "_" & Format(Now, "yyyyMMddHHmm") & ".Txt"

        Dim tw As System.IO.TextWriter = System.IO.File.CreateText(NomeFile)
        'Dim twR As System.IO.TextWriter = System.IO.File.CreateText(NomeFileRiga)



        ProgressivoTesta = 0

        Dim myPOSTreader As OleDbDataReader = cmd.ExecuteReader()
        Do While myPOSTreader.Read
            ProgressivoTesta = ProgressivoTesta + 1


            APPOGGIO = ""
            Dim CausaleContabile As New Cls_CausaleContabile

            CausaleContabile.Codice = campodb(myPOSTreader.Item("CAUSALECONTABILE"))
            CausaleContabile.Leggi(Session("DC_TABELLE"), CausaleContabile.Codice)





            Dim Cliente As String = ""

            Dim Tipo As String = campodb(myPOSTreader.Item("Tipologia"))
            Dim CodiceOspite As Integer = 0
            Dim CodiceParente As Integer = 0

            Dim CodiceProvincia As String = ""
            Dim CodiceComune As String = ""
            Dim CodiceRegione As String = ""


            Dim cmdRd As New OleDbCommand()
            cmdRd.CommandText = "Select * From MovimentiContabiliRiga Where Numero = ?  And Tipo ='CF'   "
            cmdRd.Connection = cn
            cmdRd.Parameters.AddWithValue("@Numero", campodbN(myPOSTreader.Item("NumeroRegistrazione")))
            Dim MyReadSC As OleDbDataReader = cmdRd.ExecuteReader()
            If MyReadSC.Read Then
                Cliente = campodb(MyReadSC.Item("MastroPartita")) & "." & campodb(MyReadSC.Item("ContoPartita")) & "." & campodb(MyReadSC.Item("SottocontoPartita"))
                If Mid(Tipo & Space(10), 1, 1) = " " Then
                    CodiceOspite = Int(campodbN(MyReadSC.Item("SottocontoPartita")) / 100)
                    CodiceParente = campodbN(MyReadSC.Item("SottocontoPartita")) - (CodiceOspite * 100)

                    If CodiceParente = 0 And CodiceOspite > 0 Then
                        Tipo = "O" & Space(10)
                    End If
                    If CodiceParente > 0 And CodiceOspite > 0 Then
                        Tipo = "P" & Space(10)
                    End If
                End If
                If Mid(Tipo & Space(10), 1, 1) = "C" Then
                    CodiceProvincia = Mid(Tipo & Space(10), 2, 3)
                    CodiceComune = Mid(Tipo & Space(10), 5, 3)
                End If
                If Mid(Tipo & Space(10), 1, 1) = "J" Then
                    CodiceProvincia = Mid(Tipo & Space(10), 2, 3)
                    CodiceComune = Mid(Tipo & Space(10), 5, 3)
                End If
                If Mid(Tipo & Space(10), 1, 1) = "R" Then
                    CodiceRegione = Mid(Tipo & Space(10), 2, 4)
                End If

            End If
            MyReadSC.Close()


            Dim DO11_RAGIONESOCIALE As String = ""
            Dim DO11_PIVA As String = ""
            Dim DO11_CF As String = ""
            Dim DO11_INDIRIZZO As String = ""
            Dim DO11_CAP As String = ""
            Dim DO11_CITTA_NOME As String = ""
            Dim DO11_CITTA_ISTAT As String = ""
            Dim DO11_PROVINCIA As String = ""
            Dim DO11_CONDPAG_CODICE_SEN As String = ""
            Dim DO11_CONDPAG_NOME_SEN As String = ""
            Dim DO11_BANCA_CIN As String = ""
            Dim DO11_BANCA_ABI As String = ""
            Dim DO11_BANCA_CAB As String = ""
            Dim DO11_BANCA_CONTO As String = ""
            Dim DO11_BANCA_IBAN As String = ""
            Dim DO11_BANCA_IDMANDATO As String = ""
            Dim DO11_BANCA_DATAMANDATO As String = ""
            Dim TRFDIVIDE As Integer = 0


            Dim DO11_ATTENZIONE As String = ""
            Dim AddDescrizione As String
            Dim Quantita As Integer = 1

            AddDescrizione = ""
            If Mid(Tipo & Space(10), 1, 1) = "O" Then
                Dim Ospite As New ClsOspite

                Ospite.CodiceOspite = CodiceOspite
                Ospite.Leggi(Session("DC_OSPITE"), Ospite.CodiceOspite)
                DO11_RAGIONESOCIALE = Ospite.Nome
                DO11_PIVA = ""
                DO11_CF = Ospite.CODICEFISCALE
                DO11_INDIRIZZO = Ospite.RESIDENZAINDIRIZZO1
                DO11_CAP = Ospite.RESIDENZACAP1

                DO11_INDIRIZZO = DO11_INDIRIZZO.Replace("à", "a")
                DO11_INDIRIZZO = DO11_INDIRIZZO.Replace("ò", "o")
                DO11_INDIRIZZO = DO11_INDIRIZZO.Replace("è", "e")
                DO11_INDIRIZZO = DO11_INDIRIZZO.Replace("ì", "i")


                TRFDIVIDE = Ospite.CognomeOspite.Length + 1

                Dim DcCom As New ClsComune

                DcCom.Provincia = Ospite.RESIDENZAPROVINCIA1
                DcCom.Comune = Ospite.RESIDENZACOMUNE1
                DcCom.Leggi(Session("DC_OSPITE"))

                DO11_CITTA_NOME = DcCom.Descrizione
                DO11_CITTA_ISTAT = DcCom.Provincia & DcCom.Comune

                Dim DcProv As New ClsComune

                DcProv.Provincia = Ospite.RESIDENZAPROVINCIA1
                DcProv.Comune = ""
                DcProv.Leggi(Session("DC_OSPITE"))
                DO11_PROVINCIA = DcProv.CodificaProvincia

                If Len(Ospite.CognomeOspite) < 7 Then
                    DO11_ATTENZIONE = Ospite.CognomeOspite & " " & Mid(Ospite.NomeOspite & Space(1), 1, 1)
                Else
                    DO11_ATTENZIONE = Mid(Ospite.CognomeOspite, 1, 7) & " " & Mid(Ospite.NomeOspite & Space(1), 1, 1)
                End If


                Dim Kl As New Cls_DatiOspiteParenteCentroServizio

                Kl.CodiceOspite = CodiceOspite
                Kl.CodiceParente = 0
                Kl.CentroServizio = campodb(myPOSTreader.Item("CentroServizio"))
                Kl.Leggi(Session("DC_OSPITE"))
                If Kl.ModalitaPagamento <> "" Then
                    Ospite.MODALITAPAGAMENTO = Kl.ModalitaPagamento
                End If


                DO11_CONDPAG_CODICE_SEN = Ospite.MODALITAPAGAMENTO
                Dim MPAg As New ClsModalitaPagamento

                MPAg.Codice = Ospite.MODALITAPAGAMENTO
                MPAg.Leggi(Session("DC_OSPITE"))
                DO11_CONDPAG_NOME_SEN = MPAg.DESCRIZIONE


                Dim ModPag As New Cls_DatiPagamento

                ModPag.CodiceOspite = Ospite.CodiceOspite
                ModPag.CodiceParente = 0
                ModPag.LeggiUltimaDataData(Session("DC_OSPITE"), campodb(myPOSTreader.Item("DataDocumento")))

                DO11_BANCA_CIN = ModPag.Cin
                DO11_BANCA_ABI = ModPag.Abi
                DO11_BANCA_CAB = ModPag.Cin
                DO11_BANCA_CONTO = ModPag.CCBancario
                DO11_BANCA_IBAN = ModPag.CodiceInt & Format(Val(ModPag.NumeroControllo), "00") & ModPag.Cin & Format(Val(ModPag.Abi), "00000") & Format(Val(ModPag.Cab), "00000") & AdattaLunghezzaNumero(ModPag.CCBancario, 12)

                DO11_BANCA_IDMANDATO = ModPag.IdMandatoDebitore
                DO11_BANCA_DATAMANDATO = ModPag.DataMandatoDebitore


                Dim FatturaO As New Cls_MovimentoContabile

                FatturaO.Leggi(Session("DC_GENERALE"), campodb(myPOSTreader.Item("NumeroRegistrazione")))

                Quantita = 0
                For i = 0 To FatturaO.Righe.Length - 1
                    If Not IsNothing(FatturaO.Righe(i)) Then
                        Quantita = Quantita + FatturaO.Righe(i).Quantita
                    End If
                Next
            End If

            If Mid(Tipo & Space(10), 1, 1) = "P" Then
                Dim Ospite As New Cls_Parenti

                Ospite.CodiceOspite = CodiceOspite
                Ospite.CodiceParente = CodiceParente
                Ospite.Leggi(Session("DC_OSPITE"), Ospite.CodiceOspite, Ospite.CodiceParente)
                DO11_RAGIONESOCIALE = Ospite.Nome
                DO11_PIVA = ""
                DO11_CF = Ospite.CODICEFISCALE
                DO11_INDIRIZZO = Ospite.RESIDENZAINDIRIZZO1
                DO11_CAP = Ospite.RESIDENZACAP1

                DO11_INDIRIZZO = DO11_INDIRIZZO.Replace("à", "a")
                DO11_INDIRIZZO = DO11_INDIRIZZO.Replace("ò", "o")
                DO11_INDIRIZZO = DO11_INDIRIZZO.Replace("è", "e")
                DO11_INDIRIZZO = DO11_INDIRIZZO.Replace("ì", "i")


                TRFDIVIDE = Ospite.CognomeParente.Length + 1

                Dim DcCom As New ClsComune

                DcCom.Provincia = Ospite.RESIDENZAPROVINCIA1
                DcCom.Comune = Ospite.RESIDENZACOMUNE1
                DcCom.Leggi(Session("DC_OSPITE"))

                DO11_CITTA_NOME = DcCom.Descrizione
                DO11_CITTA_ISTAT = DcCom.Provincia & DcCom.Comune

                DO11_ATTENZIONE = Ospite.Nome

                Dim DcProv As New ClsComune

                DcProv.Provincia = Ospite.RESIDENZAPROVINCIA1
                DcProv.Comune = ""
                DcProv.Leggi(Session("DC_OSPITE"))
                DO11_PROVINCIA = DcProv.CodificaProvincia
                DO11_CONDPAG_CODICE_SEN = Ospite.MODALITAPAGAMENTO

                Dim Kl As New Cls_DatiOspiteParenteCentroServizio

                Kl.CodiceOspite = CodiceOspite
                Kl.CodiceParente = CodiceParente
                Kl.CentroServizio = campodb(myPOSTreader.Item("CentroServizio"))
                Kl.Leggi(Session("DC_OSPITE"))
                If Kl.ModalitaPagamento <> "" Then
                    Ospite.MODALITAPAGAMENTO = Kl.ModalitaPagamento
                End If

                Dim MPAg As New ClsModalitaPagamento

                MPAg.Codice = Ospite.MODALITAPAGAMENTO
                MPAg.Leggi(Session("DC_OSPITE"))
                DO11_CONDPAG_NOME_SEN = MPAg.DESCRIZIONE

                Dim ModPag As New Cls_DatiPagamento

                ModPag.CodiceOspite = Ospite.CodiceOspite
                ModPag.CodiceParente = Ospite.CodiceParente
                ModPag.LeggiUltimaDataData(Session("DC_OSPITE"), campodb(myPOSTreader.Item("DataDocumento")))

                DO11_BANCA_CIN = ModPag.Cin
                DO11_BANCA_ABI = ModPag.Abi
                DO11_BANCA_CAB = ModPag.Cin
                DO11_BANCA_CONTO = ModPag.CCBancario
                DO11_BANCA_IBAN = ModPag.CodiceInt & Format(Val(ModPag.NumeroControllo), "00") & ModPag.Cin & Format(Val(ModPag.Abi), "00000") & Format(Val(ModPag.Cab), "00000") & AdattaLunghezzaNumero(ModPag.CCBancario, 12)

                DO11_BANCA_IDMANDATO = ModPag.IdMandatoDebitore
                DO11_BANCA_DATAMANDATO = ModPag.DataMandatoDebitore


                Dim FatturaP As New Cls_MovimentoContabile

                FatturaP.Leggi(Session("DC_GENERALE"), campodb(myPOSTreader.Item("NumeroRegistrazione")))

                Quantita = 0
                For i = 0 To FatturaP.Righe.Length - 1
                    If Not IsNothing(FatturaP.Righe(i)) Then
                        Quantita = Quantita + FatturaP.Righe(i).Quantita
                    End If
                Next
            End If
            If Mid(Tipo & Space(10), 1, 1) = "C" Then
                Dim Ospite As New ClsComune

                Ospite.Provincia = CodiceProvincia
                Ospite.Comune = CodiceComune
                Ospite.Leggi(Session("DC_OSPITE"))
                DO11_RAGIONESOCIALE = Ospite.Descrizione
                DO11_PIVA = Ospite.PartitaIVA
                DO11_CF = Ospite.CodiceFiscale
                DO11_INDIRIZZO = Ospite.RESIDENZAINDIRIZZO1
                DO11_CAP = Ospite.RESIDENZACAP1

                DO11_ATTENZIONE = Ospite.Attenzione

                Dim DcCom As New ClsComune

                DcCom.Provincia = Ospite.RESIDENZAPROVINCIA1
                DcCom.Comune = Ospite.RESIDENZACOMUNE1
                DcCom.Leggi(Session("DC_OSPITE"))

                DO11_CITTA_NOME = DcCom.Descrizione
                DO11_CITTA_ISTAT = DcCom.Provincia & DcCom.Comune

                Dim DcProv As New ClsComune

                DcProv.Provincia = Ospite.RESIDENZAPROVINCIA1
                DcProv.Comune = ""
                DcProv.Leggi(Session("DC_OSPITE"))
                DO11_PROVINCIA = DcProv.CodificaProvincia
                DO11_CONDPAG_CODICE_SEN = Ospite.ModalitaPagamento
                Dim MPAg As New ClsModalitaPagamento

                MPAg.Codice = Ospite.ModalitaPagamento
                MPAg.Leggi(Session("DC_OSPITE"))
                DO11_CONDPAG_NOME_SEN = MPAg.DESCRIZIONE

                Dim cnOspiti As OleDbConnection

                cnOspiti = New Data.OleDb.OleDbConnection(Session("DC_OSPITE"))

                cnOspiti.Open()

                Dim cmdC As New OleDbCommand()
                cmdC.CommandText = ("select * from NoteFatture where CodiceProvincia = ? And CodiceComune = ?")
                cmdC.Parameters.AddWithValue("@CodiceProvincia", CodiceProvincia)
                cmdC.Parameters.AddWithValue("@CodiceComune", CodiceComune)
                cmdC.Connection = cnOspiti
                Dim RdCom As OleDbDataReader = cmdC.ExecuteReader()
                If RdCom.Read Then
                    AddDescrizione = campodb(RdCom.Item("NoteUp"))
                End If
                RdCom.Close()
                cnOspiti.Close()
            End If
            If Mid(Tipo & Space(10), 1, 1) = "J" Then
                Dim Ospite As New ClsComune

                Ospite.Provincia = CodiceProvincia
                Ospite.Comune = CodiceComune
                Ospite.Leggi(Session("DC_OSPITE"))
                DO11_RAGIONESOCIALE = Ospite.Descrizione
                DO11_PIVA = Ospite.PartitaIVA
                DO11_CF = Ospite.CodiceFiscale
                DO11_INDIRIZZO = Ospite.RESIDENZAINDIRIZZO1
                DO11_CAP = Ospite.RESIDENZACAP1
                DO11_ATTENZIONE = Ospite.Attenzione

                Dim DcCom As New ClsComune

                DcCom.Provincia = Ospite.RESIDENZAPROVINCIA1
                DcCom.Comune = Ospite.RESIDENZACOMUNE1
                DcCom.Leggi(Session("DC_OSPITE"))

                DO11_CITTA_NOME = DcCom.Descrizione
                DO11_CITTA_ISTAT = DcCom.Provincia & DcCom.Comune

                Dim DcProv As New ClsComune

                DcProv.Provincia = Ospite.RESIDENZAPROVINCIA1
                DcProv.Comune = ""
                DcProv.Leggi(Session("DC_OSPITE"))
                DO11_PROVINCIA = DcProv.CodificaProvincia
                DO11_CONDPAG_CODICE_SEN = Ospite.ModalitaPagamento
                Dim MPAg As New ClsModalitaPagamento

                MPAg.Codice = Ospite.ModalitaPagamento
                MPAg.Leggi(Session("DC_OSPITE"))
                DO11_CONDPAG_NOME_SEN = MPAg.DESCRIZIONE

                Dim cnOspiti As OleDbConnection

                cnOspiti = New Data.OleDb.OleDbConnection(Session("DC_OSPITE"))

                cnOspiti.Open()

                Dim cmdC As New OleDbCommand()
                cmdC.CommandText = ("select * from NoteFatture where CodiceProvincia = ? And CodiceComune = ?")
                cmdC.Parameters.AddWithValue("@CodiceProvincia", CodiceProvincia)
                cmdC.Parameters.AddWithValue("@CodiceComune", CodiceComune)
                cmdC.Connection = cnOspiti
                Dim RdCom As OleDbDataReader = cmdC.ExecuteReader()
                If RdCom.Read Then
                    AddDescrizione = campodb(RdCom.Item("NoteUp"))
                End If
                RdCom.Close()
                cnOspiti.Close()
            End If
            If Mid(Tipo & Space(10), 1, 1) = "R" Then
                Dim Ospite As New ClsUSL

                Ospite.CodiceRegione = CodiceRegione
                Ospite.Leggi(Session("DC_OSPITE"))
                DO11_RAGIONESOCIALE = Ospite.Nome
                DO11_PIVA = Ospite.PARTITAIVA
                DO11_CF = Ospite.CodiceFiscale
                DO11_INDIRIZZO = Ospite.RESIDENZAINDIRIZZO1
                DO11_CAP = Ospite.RESIDENZACAP1

                DO11_ATTENZIONE = Ospite.Attenzione
                Dim DcCom As New ClsComune

                DcCom.Provincia = Ospite.RESIDENZAPROVINCIA1
                DcCom.Comune = Ospite.RESIDENZACOMUNE1
                DcCom.Leggi(Session("DC_OSPITE"))

                DO11_CITTA_NOME = DcCom.Descrizione
                DO11_CITTA_ISTAT = DcCom.Provincia & DcCom.Comune

                Dim DcProv As New ClsComune

                DcProv.Provincia = Ospite.RESIDENZAPROVINCIA1
                DcProv.Comune = ""
                DcProv.Leggi(Session("DC_OSPITE"))
                DO11_PROVINCIA = DcProv.CodificaProvincia
                DO11_CONDPAG_CODICE_SEN = Ospite.ModalitaPagamento
                Dim MPAg As New ClsModalitaPagamento

                MPAg.Codice = Ospite.ModalitaPagamento
                MPAg.Leggi(Session("DC_OSPITE"))
                DO11_CONDPAG_NOME_SEN = MPAg.DESCRIZIONE
            End If


            Dim M As New Cls_TipoPagamento

            M.Codice = campodb(myPOSTreader.Item("ModalitaPagamento"))
            M.Leggi(Session("DC_TABELLE"))
            If M.Descrizione <> "" Then
                DO11_CONDPAG_CODICE_SEN = M.Codice
                DO11_CONDPAG_NOME_SEN = M.Descrizione
            End If


            If Session("DC_OSPITE").ToString.ToUpper.IndexOf("SANFRANCESCO") >= 0 Then
                APPOGGIO = APPOGGIO & AdattaLunghezzaNumero("306", 5) ' TRF_DITTA 
            End If
            If Session("DC_OSPITE").ToString.ToUpper.IndexOf("GIUSEPPE") >= 0 Then
                APPOGGIO = APPOGGIO & AdattaLunghezzaNumero("305", 5) ' TRF_DITTA 
            End If
            If Session("DC_OSPITE").ToString.ToUpper.IndexOf("CREA") >= 0 Then
                APPOGGIO = APPOGGIO & AdattaLunghezzaNumero("00001", 5) ' TRF_DITTA 
            End If
            If Session("DC_OSPITE").ToString.ToUpper.IndexOf("CHIA") >= 0 Then
                APPOGGIO = APPOGGIO & AdattaLunghezzaNumero("00068", 5) ' TRF_DITTA 
            End If

            If Session("DC_OSPITE").ToString.ToUpper.IndexOf("LIBERA") >= 0 Then
                APPOGGIO = APPOGGIO & AdattaLunghezzaNumero("00004", 5) ' TRF_DITTA 
            End If



            APPOGGIO = APPOGGIO & AdattaLunghezzaNumero("3", 1) 'TRF_VERSIONE 
            APPOGGIO = APPOGGIO & AdattaLunghezzaNumero("0", 1) 'TRF_TARC 
            APPOGGIO = APPOGGIO & AdattaLunghezzaNumero("0", 5) ' TRF_COD_CLIFOR 
            APPOGGIO = APPOGGIO & AdattaLunghezza(DO11_RAGIONESOCIALE, 32) 'TRF_RASO & 
            APPOGGIO = APPOGGIO & AdattaLunghezza(DO11_INDIRIZZO, 30) 'TRF_IND & 
            APPOGGIO = APPOGGIO & AdattaLunghezzaNumero(DO11_CAP, 5) ' TRF_CAP & 
            APPOGGIO = APPOGGIO & AdattaLunghezza(DO11_CITTA_NOME, 25) ' TRF_CITTA 
            APPOGGIO = APPOGGIO & AdattaLunghezza(DO11_PROVINCIA, 2) 'TRF_PROV 
            APPOGGIO = APPOGGIO & AdattaLunghezza(DO11_CF, 16) ' TRF_COFI MENT
            APPOGGIO = APPOGGIO & AdattaLunghezzaNumero(DO11_PIVA, 11) '  TRF-PIVA                        
            If Val(DO11_PIVA) <> 0 Then
                APPOGGIO = APPOGGIO & AdattaLunghezza("N", 1) '    TRF-PF                         
            Else
                APPOGGIO = APPOGGIO & AdattaLunghezza("S", 1) '    TRF-PF                         
            End If
            If Session("DC_OSPITE").ToString.ToUpper.IndexOf("LIBERA") >= 0 Then
                APPOGGIO = APPOGGIO & AdattaLunghezzaNumero(TRFDIVIDE, 2) 'TRF_DIVIDE
            Else
                APPOGGIO = APPOGGIO & AdattaLunghezzaNumero("0", 2) 'TRF_DIVIDE
            End If
            APPOGGIO = APPOGGIO & AdattaLunghezzaNumero("0", 4) 'TRF_PAESE 
            APPOGGIO = APPOGGIO & AdattaLunghezza("", 12) 'TRF_PIVA_ESTERO 
            APPOGGIO = APPOGGIO & AdattaLunghezza("", 20) ' TRF_COFI_ESTERO 
            APPOGGIO = APPOGGIO & AdattaLunghezza("", 1) 'TRF_SESSO 
            APPOGGIO = APPOGGIO & AdattaLunghezzaNumero("", 8) 'TRF_DTNAS 
            APPOGGIO = APPOGGIO & AdattaLunghezza("", 25) ' TRF_COMNA 
            APPOGGIO = APPOGGIO & AdattaLunghezza("", 2) 'TRF_PRVNA 
            APPOGGIO = APPOGGIO & AdattaLunghezza("", 4) 'TRF_PREF 
            APPOGGIO = APPOGGIO & AdattaLunghezza("", 20) 'TRF_NTELE_NUM 
            APPOGGIO = APPOGGIO & AdattaLunghezza("", 4) 'TRF_FAX_PREF
            APPOGGIO = APPOGGIO & AdattaLunghezza("", 9) ' TRF_FAX_NUM 
            APPOGGIO = APPOGGIO & AdattaLunghezzaNumero("", 7) ' TRF_CFCONTO

            'CONDIZIONE PAGA

            Dim TabTrasco As New Cls_TabellaTrascodificheEsportazioni

            TabTrasco.EXPORT = ""
            TabTrasco.SENIOR = DO11_CONDPAG_CODICE_SEN
            TabTrasco.TIPOTAB = "MP"
            TabTrasco.Leggi(Session("DC_TABELLE"))
            APPOGGIO = APPOGGIO & AdattaLunghezzaNumero(Val(TabTrasco.EXPORT), 4) 'TRF_CFCODPAG 

            'AGGIUNTA MODALITA PAGAMENTO PER CHIARALUCE

            APPOGGIO = APPOGGIO & AdattaLunghezzaNumero("", 5) 'TRF_CFBANCA 
            APPOGGIO = APPOGGIO & AdattaLunghezzaNumero("", 5) ' TRF_CFAGENZIA 
            APPOGGIO = APPOGGIO & AdattaLunghezzaNumero("", 1) ' TRF_CFINTERM 

            If CausaleContabile.TipoDocumento = "NC" Then
                APPOGGIO = APPOGGIO & AdattaLunghezzaNumero("002", 3) 'TRF_CAUSALE 
            Else
                APPOGGIO = APPOGGIO & AdattaLunghezzaNumero("001", 3) 'TRF_CAUSALE 
            End If
            If Session("DC_OSPITE").ToString.ToUpper.IndexOf("CREA") >= 0 Then
                Dim AbbreviazioneCserv As String = ""
                If campodb(myPOSTreader.Item("CentroServizio")) = "CD" Then
                    AbbreviazioneCserv = "CDNON"
                End If
                If campodb(myPOSTreader.Item("CentroServizio")) = "RSA" Then
                    AbbreviazioneCserv = "RSANON"
                End If
                If campodb(myPOSTreader.Item("CentroServizio")) = "CC" Then
                    AbbreviazioneCserv = "CDSDCAMA"
                End If
                If campodb(myPOSTreader.Item("CentroServizio")) = "CP" Then
                    AbbreviazioneCserv = "CDSDVG"
                End If
                If campodb(myPOSTreader.Item("CentroServizio")) = "CG" Then
                    AbbreviazioneCserv = "CDSDVG2"
                End If
                If campodb(myPOSTreader.Item("CentroServizio")) = "CA" Then
                    AbbreviazioneCserv = "C.A."
                End If
                If campodb(myPOSTreader.Item("CentroServizio")) = "CAP" Then
                    AbbreviazioneCserv = "CAPBEP"
                End If
                APPOGGIO = APPOGGIO & AdattaLunghezza("FATTURA VENDITE", 15) 'TRF_CAU_DES
                APPOGGIO = APPOGGIO & AdattaLunghezza(DO11_ATTENZIONE & " " & AbbreviazioneCserv & " " & DD_Mese.SelectedValue, 18) 'TRF_CAU_DES                
            Else
                If CausaleContabile.TipoDocumento = "NC" Then
                    APPOGGIO = APPOGGIO & AdattaLunghezza("NC VENDITE", 15) 'TRF_CAU_DESù
                Else
                    APPOGGIO = APPOGGIO & AdattaLunghezza("FATTURA VENDITE", 15) 'TRF_CAU_DESù
                End If

                If Session("DC_OSPITE").ToString.ToUpper.IndexOf("CHIA") >= 0 Then
                    APPOGGIO = APPOGGIO & AdattaLunghezza(campodb(myPOSTreader.Item("NumeroDocumento")), 18)  ' TRF_CAU_AGG 
                Else
                    APPOGGIO = APPOGGIO & AdattaLunghezza("", 18)  ' TRF_CAU_AGG 
                End If
            End If



            APPOGGIO = APPOGGIO & AdattaLunghezza("", 34) ' TRF-CAU-AGG-1
            APPOGGIO = APPOGGIO & AdattaLunghezza("", 34) ' TRF-CAU-AGG-2
            APPOGGIO = APPOGGIO & Format(campodbD(myPOSTreader.Item("DataRegistrazione")), "ddMMyyyy") ' TRF-DATA-REGISTRAZIONE
            APPOGGIO = APPOGGIO & Format(campodbD(myPOSTreader.Item("DataDocumento")), "ddMMyyyy") 'TRF_Data_DOC 

            APPOGGIO = APPOGGIO & AdattaLunghezzaNumero(campodb(myPOSTreader.Item("NumeroDocumento")), 8) 'TRF-NUM-DOC-FOR 


            APPOGGIO = APPOGGIO & AdattaLunghezzaNumero(campodb(myPOSTreader.Item("NumeroDocumento")), 5) 'TRF_NDOC 
            If Session("DC_OSPITE").ToString.ToUpper.IndexOf("LIBERA") >= 0 Then
                If RegistroIva = 1 Then
                    APPOGGIO = APPOGGIO & AdattaLunghezzaNumero("01", 2) 'TRF_SERIE 
                End If
                If RegistroIva = 2 Then
                    APPOGGIO = APPOGGIO & AdattaLunghezzaNumero("02", 2) 'TRF_SERIE 
                End If
                If RegistroIva = 3 Then
                    APPOGGIO = APPOGGIO & AdattaLunghezzaNumero("03", 2) 'TRF_SERIE 
                End If
                If RegistroIva = 4 Then
                    APPOGGIO = APPOGGIO & AdattaLunghezzaNumero("04", 2) 'TRF_SERIE 
                End If
                If RegistroIva = 5 Then
                    APPOGGIO = APPOGGIO & AdattaLunghezzaNumero("05", 2) 'TRF_SERIE 
                End If
                If RegistroIva = 6 Then
                    APPOGGIO = APPOGGIO & AdattaLunghezzaNumero("06", 2) 'TRF_SERIE 
                End If


            Else
                If Session("DC_OSPITE").ToString.ToUpper.IndexOf("CHIA") >= 0 Then
                    APPOGGIO = APPOGGIO & AdattaLunghezzaNumero("00", 2) 'TRF_SERIE 
                Else
                    If Session("DC_OSPITE").ToString.ToUpper.IndexOf("CREA") >= 0 Then
                        If RegistroIva = 1 Then
                            APPOGGIO = APPOGGIO & AdattaLunghezzaNumero("00", 2) 'TRF_SERIE 
                        Else
                            APPOGGIO = APPOGGIO & AdattaLunghezzaNumero("01", 2) 'TRF_SERIE 
                        End If
                    Else
                        If Session("DC_OSPITE").ToString.ToUpper.IndexOf("SANFRANCESCO") >= 0 Then
                            APPOGGIO = APPOGGIO & AdattaLunghezzaNumero("06", 2) 'TRF_SERIE 
                        Else
                            APPOGGIO = APPOGGIO & AdattaLunghezzaNumero("05", 2) 'TRF_SERIE 
                        End If
                    End If
                End If
            End If
            APPOGGIO = APPOGGIO & AdattaLunghezzaNumero("", 6) 'TRF_EC_PARTITA 
            APPOGGIO = APPOGGIO & AdattaLunghezzaNumero("", 4) 'TRF-EC-PARTITA-ANNO             
            APPOGGIO = APPOGGIO & AdattaLunghezzaNumero("", 3) 'TRF_EC_COD_Val
            APPOGGIO = APPOGGIO & AdattaLunghezzaNumero("", 13) 'TRF_EC_CAMBIO 
            APPOGGIO = APPOGGIO & AdattaLunghezzaNumero("", 8) 'TRF-EC-DATA-CAMBIO               
            APPOGGIO = APPOGGIO & AdattaLunghezzaNumero("", 16) 'TRF-EC-TOT-DOC-VAL               
            APPOGGIO = APPOGGIO & AdattaLunghezzaNumero("", 16) 'TRF-EC-TOT-IVA-VAL               
            APPOGGIO = APPOGGIO & AdattaLunghezzaNumero("", 6) 'TRF_PLAFOND




            Dim Progressivo As Integer = 0
            Dim Entrato As Boolean = False
            Dim Numero As Integer = 0

            Dim TotFat As Double = 0
            Dim TImposta As Double = 0

            Dim cmdRigaIVA As New OleDbCommand()
            cmdRigaIVA.CommandText = "Select * From MovimentiContabiliRiga Where Numero = ?  And Tipo = 'IV'  "
            cmdRigaIVA.Connection = cn
            cmdRigaIVA.Parameters.AddWithValue("@Numero", campodbN(myPOSTreader.Item("NumeroRegistrazione")))
            Dim MyRigaIVA As OleDbDataReader = cmdRigaIVA.ExecuteReader()
            Do While MyRigaIVA.Read

                APPOGGIO = APPOGGIO & AdattaLunghezzaNumero(Math.Round(campodbN(MyRigaIVA.Item("Imponibile")), 2) * 100, 11) & "+" 'TRF_IMPONIB 

                Dim MIva As New Cls_IVA

                MIva.Codice = campodb(MyRigaIVA.Item("CODICEIVA"))
                MIva.Leggi(Session("DC_TABELLE"), MIva.Codice)
                If MIva.Aliquota > 0 Then
                    APPOGGIO = APPOGGIO & AdattaLunghezzaNumero(Int(MIva.Aliquota * 100), 3) ' TRF_ALIQ 
                Else
                    If MIva.Codice = "10" Then
                        If Session("DC_OSPITE").ToString.ToUpper.IndexOf("CREA") >= 0 Then
                            APPOGGIO = APPOGGIO & "312"
                        Else
                            APPOGGIO = APPOGGIO & "310"
                        End If
                    End If
                    If MIva.Codice = "15" Then
                        APPOGGIO = APPOGGIO & "315"
                    End If
                    If MIva.Codice = "11" Then
                        APPOGGIO = APPOGGIO & "311"
                    End If
                End If
                APPOGGIO = APPOGGIO & AdattaLunghezzaNumero("", 3) 'TRF_ALIQ_AGRICOLA 
                APPOGGIO = APPOGGIO & AdattaLunghezzaNumero("", 2) ' TRF_IVA11 

                APPOGGIO = APPOGGIO & AdattaLunghezzaNumero(Math.Round(campodbN(MyRigaIVA.Item("Importo")), 2) * 100, 10) & "+" 'TRF_IMPOSTA 


                Numero = Numero + 1
                TotFat = TotFat + campodbN(MyRigaIVA.Item("Imponibile")) + campodbN(MyRigaIVA.Item("Importo"))
                TImposta = TImposta + campodbN(MyRigaIVA.Item("Importo"))
            Loop
            MyRigaIVA.Close()

            If Numero < 8 Then
                Dim I As Integer

                For I = Numero + 1 To 8
                    APPOGGIO = APPOGGIO & AdattaLunghezzaNumero("0", 11) & "+" 'TRF_IMPONIB  
                    APPOGGIO = APPOGGIO & AdattaLunghezzaNumero("", 3) ' TRF_ALIQ 
                    APPOGGIO = APPOGGIO & AdattaLunghezzaNumero("", 3) 'TRF_ALIQ_AGRICOLA 
                    APPOGGIO = APPOGGIO & AdattaLunghezzaNumero("", 2) ' TRF_IVA11 
                    APPOGGIO = APPOGGIO & AdattaLunghezzaNumero("0", 10) & "+"  'TRF_IMPOSTA 
                Next
            End If


            APPOGGIO = APPOGGIO & AdattaLunghezzaNumero(Math.Round(TotFat, 2) * 100, 12) 'TRF_TOT_FATT

            Dim CONTO1 As String = ""
            Dim CONTO2 As String = ""
            Dim CONTO3 As String = ""
            Dim Imponibile1 As Double = 0
            Dim Imponibile2 As Double = 0
            Dim Imponibile3 As Double = 0
            Dim Quantita1 As Double = 0
            Dim Quantita2 As Double = 0
            Dim Quantita3 As Double = 0
            Numero = 0

            Dim cmdRiga As New OleDbCommand()

            If Session("DC_OSPITE").ToString.ToUpper.IndexOf("CHIA") >= 0 Then
                cmdRiga.CommandText = "Select MastroPartita,ContoPartita,CASE WHEN SottocontoPartita = 6 THEN 5 ELSE SottocontoPartita END AS SottocontoPartita,sum(CASE WHEN DareAvere = 'A' THEN importo ELSE Importo *-1 END) as totoconto,sum(Quantita) as totquantita From MovimentiContabiliRiga Where Numero = ?  And (Tipo Is Null Or Tipo <> 'IV') And (Tipo Is Null Or Tipo <> 'CF') group by MastroPartita,ContoPartita,CASE WHEN SottocontoPartita = 6 THEN 5 ELSE SottocontoPartita END"
            Else
                cmdRiga.CommandText = "Select MastroPartita,ContoPartita,SottocontoPartita,sum(CASE WHEN DareAvere = 'A' THEN importo ELSE Importo *-1 END) as totoconto,sum(Quantita) as totquantita From MovimentiContabiliRiga Where Numero = ?  And (Tipo Is Null Or Tipo <> 'IV') And (Tipo Is Null Or Tipo <> 'CF') group by MastroPartita,ContoPartita,SottocontoPartita "
            End If

            cmdRiga.Connection = cn
            cmdRiga.Parameters.AddWithValue("@Numero", campodbN(myPOSTreader.Item("NumeroRegistrazione")))
            Dim MyRiga As OleDbDataReader = cmdRiga.ExecuteReader()
            Do While MyRiga.Read
                Dim Prova As String


                Prova = Format(campodbN(MyRiga.Item("MastroPartita")), "00") & Format(campodbN(MyRiga.Item("ContoPartita")), "00") & Format(campodbN(MyRiga.Item("SottocontoPartita")), "000")


                APPOGGIO = APPOGGIO & AdattaLunghezzaNumero(Prova, 7)  'TRF_CONTO_RIC

                'If CausaleContabile.TipoDocumento = "NC" Then
                '    APPOGGIO = APPOGGIO & AdattaLunghezzaNumero(Math.Round(campodbN(MyRiga.Item("totoconto")), 2) * 100, 11) & "-" 'TRF_IMP_RIC 
                'Else
                APPOGGIO = APPOGGIO & AdattaLunghezzaNumero(Math.Abs(Math.Round(campodbN(MyRiga.Item("totoconto")), 2)) * 100, 11) & "+" 'TRF_IMP_RIC 
                'End If


                If Imponibile1 = 0 Then
                    Imponibile1 = Math.Round(campodbN(MyRiga.Item("totoconto")), 2)
                    Quantita1 = 1 ' Math.Round(campodbN(MyRiga.Item("totquantita")), 2)
                    CONTO1 = Prova
                Else
                    If Imponibile2 = 0 Then
                        Imponibile2 = Math.Round(campodbN(MyRiga.Item("totoconto")), 2)
                        Quantita2 = 1 ' Math.Round(campodbN(MyRiga.Item("totquantita")), 2)
                        CONTO2 = Prova
                    Else
                        If Imponibile3 = 0 Then
                            Imponibile3 = Math.Round(campodbN(MyRiga.Item("totoconto")), 2)
                            Quantita3 = 1 ' Math.Round(campodbN(MyRiga.Item("totquantita")), 2)
                            CONTO3 = Prova
                        End If
                    End If
                End If
                Numero = Numero + 1
            Loop
            MyRiga.Close()

            If Numero < 8 Then
                Dim I As Integer

                For I = Numero + 1 To 8
                    APPOGGIO = APPOGGIO & AdattaLunghezzaNumero("0", 7)  'TRF_CONTO_RIC
                    APPOGGIO = APPOGGIO & AdattaLunghezzaNumero("0", 11) & "+" 'TRF_IMP_RIC 
                Next
            End If

            Dim TRF_CAU_PAGAM As String
            Dim TRF_CAU_DES_PAGAM As String

            APPOGGIO = APPOGGIO & AdattaLunghezzaNumero(TRF_CAU_PAGAM, 3) 'TRF_CAU_PAGAM
            APPOGGIO = APPOGGIO & AdattaLunghezza(TRF_CAU_DES_PAGAM, 15) ' TRF_CAU_DES_PAGAM 
            APPOGGIO = APPOGGIO & AdattaLunghezza("", 34) ' TRF_CAU_AGG_1_PAGAM
            APPOGGIO = APPOGGIO & AdattaLunghezza("", 34) ' TRF_CAU_AGG_2_PAGAM

            For i = 1 To 80
                APPOGGIO = APPOGGIO & AdattaLunghezzaNumero("", 7) 'TRF_CONTO 
                APPOGGIO = APPOGGIO & AdattaLunghezza("", 1) 'TRF_DA
                APPOGGIO = APPOGGIO & AdattaLunghezzaNumero("0", 11) & "+" 'TRF_IMPORTO
                APPOGGIO = APPOGGIO & AdattaLunghezza("", 18) 'TRF_CAU_AGGIUNT
                APPOGGIO = APPOGGIO & AdattaLunghezzaNumero("0", 6) 'TRF-EC-PARTITA-PAG
                APPOGGIO = APPOGGIO & AdattaLunghezzaNumero("0", 4) 'TRF-EC-PARTITA-ANNO-PAG          
                APPOGGIO = APPOGGIO & AdattaLunghezzaNumero("0", 15) & "0" 'TRF_EC_IMP_Val
            Next

            For i = 1 To 10
                APPOGGIO = APPOGGIO & AdattaLunghezza("", 1) 'TRF_RIFER_TAB
                APPOGGIO = APPOGGIO & AdattaLunghezzaNumero("0", 2) 'TRF_IND_RIGA 
                APPOGGIO = APPOGGIO & AdattaLunghezzaNumero("0", 8) 'TRF_DT_INI 
                APPOGGIO = APPOGGIO & AdattaLunghezzaNumero("0", 8) 'TRF_DT_FIN                 
            Next

            APPOGGIO = APPOGGIO & AdattaLunghezzaNumero("0", 6) ' TRF-DOC6  
            APPOGGIO = APPOGGIO & AdattaLunghezza("S", 1) 'TRF-AN-OMONIMI  
            APPOGGIO = APPOGGIO & AdattaLunghezzaNumero("0", 1) ' TRF_AN_Tipo_SOGG

            For i = 1 To 80
                APPOGGIO = APPOGGIO & AdattaLunghezzaNumero("0", 2) 'TRF-EC-PARTITA-SEZ-PAG
            Next

            APPOGGIO = APPOGGIO & AdattaLunghezzaNumero("0", 7) ' TRF-NUM-DOC-PAG-PROF  
            APPOGGIO = APPOGGIO & AdattaLunghezzaNumero("0", 8) 'TRF-DATA-DOC-PAG-PROF  
            APPOGGIO = APPOGGIO & AdattaLunghezzaNumero("0", 12) 'TRF-RIT-ACC  
            APPOGGIO = APPOGGIO & AdattaLunghezzaNumero("0", 12) 'TRF-RIT-PREV  
            APPOGGIO = APPOGGIO & AdattaLunghezzaNumero("0", 12) ' TRF-RIT-1
            APPOGGIO = APPOGGIO & AdattaLunghezzaNumero("0", 12) 'TRF-RIT-2
            APPOGGIO = APPOGGIO & AdattaLunghezzaNumero("0", 12) ' TRF-RIT-3            
            APPOGGIO = APPOGGIO & AdattaLunghezzaNumero("0", 12) 'TRF-RIT-4
            For i = 1 To 8
                APPOGGIO = APPOGGIO & AdattaLunghezzaNumero("0", 2) ' TRF_UNITA_RICAVI
            Next
            For i = 1 To 80
                APPOGGIO = APPOGGIO & AdattaLunghezzaNumero("0", 2) ' TRF_UNITA_PAGAM
            Next
            APPOGGIO = APPOGGIO & AdattaLunghezza("", 4) ' TRF_FAX_PREF_1
            APPOGGIO = APPOGGIO & AdattaLunghezza("", 20) ' TRF_FAX_NUM_1 
            APPOGGIO = APPOGGIO & AdattaLunghezza("", 1) ' TRF_SOLO_CLIFOR
            APPOGGIO = APPOGGIO & AdattaLunghezza("", 1) ' TRF_80_SEGUENTE
            APPOGGIO = APPOGGIO & AdattaLunghezzaNumero("0", 7) 'TRF_CONTO_RIT_ACC
            APPOGGIO = APPOGGIO & AdattaLunghezzaNumero("0", 7) ' TRF_CONTO_RIT_PREV
            APPOGGIO = APPOGGIO & AdattaLunghezzaNumero("0", 7) ' TRF-CONTO-RIT-1
            APPOGGIO = APPOGGIO & AdattaLunghezzaNumero("0", 7) ' TRF-CONTO-RIT-2
            APPOGGIO = APPOGGIO & AdattaLunghezzaNumero("0", 7) ' TRF-CONTO-RIT-3
            APPOGGIO = APPOGGIO & AdattaLunghezzaNumero("0", 7) ' TRF-CONTO-RIT-4
            APPOGGIO = APPOGGIO & AdattaLunghezza("", 1) 'TRF_DIFFERIMENTO_IVA
            APPOGGIO = APPOGGIO & AdattaLunghezza("", 1) 'TRF_STORICO
            APPOGGIO = APPOGGIO & AdattaLunghezzaNumero("0", 8) 'TRF_STORICO_DATA
            APPOGGIO = APPOGGIO & AdattaLunghezzaNumero("0", 3) 'TRF_CAUS_ORI
            APPOGGIO = APPOGGIO & AdattaLunghezza("", 1) 'TRF_PREV_TIPOMOV
            APPOGGIO = APPOGGIO & AdattaLunghezza("", 1) 'TRF_PREV_RATRIS
            APPOGGIO = APPOGGIO & AdattaLunghezzaNumero("0", 8) 'TRF_PREV_DTCOMP_INI
            APPOGGIO = APPOGGIO & AdattaLunghezzaNumero("0", 8) 'TRF_PREV_DTCOMP_FIN
            APPOGGIO = APPOGGIO & AdattaLunghezza("", 1) 'TRF_PREV_FLAG_CONT
            APPOGGIO = APPOGGIO & AdattaLunghezza("", 20) 'TRF_RIFERIMENTO
            APPOGGIO = APPOGGIO & AdattaLunghezzaNumero("0", 2) 'TRF_CAUS_PREST_ANA
            APPOGGIO = APPOGGIO & AdattaLunghezzaNumero("0", 1) 'TRF_EC_Tipo_PAGA
            APPOGGIO = APPOGGIO & AdattaLunghezzaNumero("0", 7) 'TRF_CONTO_IVA_VEN_ACQ
            APPOGGIO = APPOGGIO & AdattaLunghezzaNumero("0", 11) 'TRF_PIVA_VECCHIA
            APPOGGIO = APPOGGIO & AdattaLunghezza("", 12) 'TRF_PIVA_ESTERO_VECCHIA 
            APPOGGIO = APPOGGIO & AdattaLunghezza("", 32) 'TRF_RISERVATO
            APPOGGIO = APPOGGIO & AdattaLunghezzaNumero("0", 8) 'TRF_Data_IVA_AGVIAGGI
            APPOGGIO = APPOGGIO & AdattaLunghezza("", 1) ' TRF_DATI_AGG_ANA_REC4
            APPOGGIO = APPOGGIO & AdattaLunghezzaNumero("0", 6) 'TRF_RIF_IVA_NOTE_CRED
            APPOGGIO = APPOGGIO & AdattaLunghezza("", 1) 'TRF_RIF_IVA_ANNO_PREC
            APPOGGIO = APPOGGIO & AdattaLunghezzaNumero("0", 2) 'TRF_NATURA_GIURIDICA
            APPOGGIO = APPOGGIO & AdattaLunghezza("", 1) 'TRF_STAMPA_ELENCO

            For i = 1 To 8
                APPOGGIO = APPOGGIO & AdattaLunghezzaNumero("0", 3) 'TRF_PERC_FORF
            Next

            APPOGGIO = APPOGGIO & AdattaLunghezza("", 1) 'TRF_SOLO_MOV_IVA
            APPOGGIO = APPOGGIO & AdattaLunghezza("", 16) 'TRF_COFI_VECCHIO
            APPOGGIO = APPOGGIO & AdattaLunghezza("", 1) 'TRF_USA_PIVA_VECCHIA
            APPOGGIO = APPOGGIO & AdattaLunghezza("", 1) 'TRF_USA_PIVA_EST_VECCHIA
            APPOGGIO = APPOGGIO & AdattaLunghezza("", 1) 'TRF_USA_COFI_VECCHIO

            If Session("DC_OSPITE").ToString.ToUpper.IndexOf("LIBERA") >= 0 Then
                If TImposta > 0 And RegistroIva = 6 Then
                    APPOGGIO = APPOGGIO & AdattaLunghezzaNumero("4", 1) 'TRF_ESIGIBILITA_IVA
                Else
                    APPOGGIO = APPOGGIO & AdattaLunghezzaNumero("0", 1) 'TRF_ESIGIBILITA_IVA
                End If

            Else
                If TImposta > 0 And RegistroIva = 2 Then
                    APPOGGIO = APPOGGIO & AdattaLunghezzaNumero("4", 1) 'TRF_ESIGIBILITA_IVA
                Else
                    APPOGGIO = APPOGGIO & AdattaLunghezzaNumero("0", 1) 'TRF_ESIGIBILITA_IVA
                End If
            End If

            APPOGGIO = APPOGGIO & AdattaLunghezza("", 1) 'TRF_Tipo_MOV_RISCONTI
            APPOGGIO = APPOGGIO & AdattaLunghezza("", 1) 'TRF_AGGIORNA_EC
            APPOGGIO = APPOGGIO & AdattaLunghezza("", 1) 'TRF_BLACKLIST_ANAG
            APPOGGIO = APPOGGIO & AdattaLunghezza("", 1) 'TRF_BLACKLIST_IVA
            APPOGGIO = APPOGGIO & AdattaLunghezzaNumero("0", 6) 'TRF_BLACKLIST_IVA_ANA
            APPOGGIO = APPOGGIO & AdattaLunghezza("", 20) 'TRF_CONTEA_ESTERO
            If Session("DC_OSPITE").ToString.ToUpper.IndexOf("CREA") >= 0 Then
                APPOGGIO = APPOGGIO & AdattaLunghezza("S", 1) 'TRF_ART21_ANAG
                APPOGGIO = APPOGGIO & AdattaLunghezza("S", 1) 'TRF_ART21_IVA
            Else
                APPOGGIO = APPOGGIO & AdattaLunghezza("", 1) 'TRF_ART21_ANAG
                APPOGGIO = APPOGGIO & AdattaLunghezza("", 1) 'TRF_ART21_IVA
            End If

            APPOGGIO = APPOGGIO & AdattaLunghezza("", 1) 'TRF_RIF_FATTURA
            If Session("DC_OSPITE").ToString.ToUpper.IndexOf("CREA") >= 0 Then
                APPOGGIO = APPOGGIO & AdattaLunghezza("S", 1) 'TRF_RISERVATO_B
            Else
                APPOGGIO = APPOGGIO & AdattaLunghezza("", 1) 'TRF_RISERVATO_B
            End If

            APPOGGIO = APPOGGIO & AdattaLunghezza("", 1) 'TRF-MASTRO-CF 
            APPOGGIO = APPOGGIO & AdattaLunghezza("", 1) 'TRF_MOV_PRIVATO
            APPOGGIO = APPOGGIO & AdattaLunghezza("", 1) 'TRF-SPESE-MEDICHE
            APPOGGIO = APPOGGIO & AdattaLunghezza("", 2) ' FILLER

            REM nuovo formato

            tw.WriteLine(APPOGGIO)

            If Session("DC_OSPITE").ToString.ToUpper.IndexOf("LIBERA") >= 0 Then
                APPOGGIO = ""
                APPOGGIO = APPOGGIO & AdattaLunghezzaNumero("00004", 5) ' TRF_DITTA 
                APPOGGIO = APPOGGIO & "3" 'TRF1-VERSIONE
                APPOGGIO = APPOGGIO & "1" 'TRF1-TARC                        


                APPOGGIO = APPOGGIO & AdattaLunghezzaNumero(0, 5) 'TRF-NUM-AUTOFATT                 
                APPOGGIO = APPOGGIO & AdattaLunghezzaNumero(0, 2) 'TRF-SERIE-AUTOFATT                            
                APPOGGIO = APPOGGIO & Space(3) 'Codice Valuta
                APPOGGIO = APPOGGIO & AdattaLunghezzaNumero(0, 14) 'Totale importo in valuta

                For I = 1 To 20
                    APPOGGIO = APPOGGIO & Space(8) '  TRF-NOMENCLATURA                 	8
                    APPOGGIO = APPOGGIO & AdattaLunghezzaNumero(0, 12)  'TRF-IMP-LIRE                     	12
                    APPOGGIO = APPOGGIO & AdattaLunghezzaNumero(0, 12)  'TRF-IMP-VAL                      	12(10+2 DEC)
                    APPOGGIO = APPOGGIO & Space(1) 'TRF-NATURA                       	1
                    APPOGGIO = APPOGGIO & AdattaLunghezzaNumero(0, 12)  'TRF-MASSA                        	12(10+2 DEC)
                    APPOGGIO = APPOGGIO & AdattaLunghezzaNumero(0, 12)  'TRF-UN-SUPPL                     	12
                    APPOGGIO = APPOGGIO & AdattaLunghezzaNumero(0, 12)  'TRF-VAL-STAT                     	12
                    APPOGGIO = APPOGGIO & Space(1) 'TRF-REGIME                       	1
                    APPOGGIO = APPOGGIO & Space(1) 'TRF-TRASPORTO                    	1
                    APPOGGIO = APPOGGIO & AdattaLunghezzaNumero(0, 3) 'TRF-PAESE-PROV                   	3
                    APPOGGIO = APPOGGIO & AdattaLunghezzaNumero(0, 3) 'TRF-PAESE-ORIG                   	3
                    APPOGGIO = APPOGGIO & AdattaLunghezzaNumero(0, 3) 'TRF-PAESE-DEST                   	3
                    APPOGGIO = APPOGGIO & Space(2) 'TRF-PROV-DEST                    	2
                    APPOGGIO = APPOGGIO & Space(2) 'TRF-PROV-ORIG                    	2
                    APPOGGIO = APPOGGIO & Space(1) 'TRF-SEGNO-RET	1
                Next
                APPOGGIO = APPOGGIO & Space(1) 'TRF-INTRA-TIPO	1
                APPOGGIO = APPOGGIO & AdattaLunghezzaNumero(0, 6) 'TRF-MESE-ANNO-RIF	6
                APPOGGIO = APPOGGIO & Space(173) '173:


                APPOGGIO = APPOGGIO & AdattaLunghezzaNumero(0, 1) 'TRF-RITA-TIPO                    	1
                APPOGGIO = APPOGGIO & AdattaLunghezzaNumero(0, 11) 'TRF-RITA-IMPON                   	11
                APPOGGIO = APPOGGIO & AdattaLunghezzaNumero(0, 4) 'TRF-RITA-ALIQ                    	4(2+2dec)
                APPOGGIO = APPOGGIO & AdattaLunghezzaNumero(0, 10) 'TRF-RITA-IMPRA                   	10
                APPOGGIO = APPOGGIO & AdattaLunghezzaNumero(0, 11) 'TRF-RITA-PRONS                   	11
                APPOGGIO = APPOGGIO & AdattaLunghezzaNumero(0, 6) 'TRF-RITA-MESE                    	6
                APPOGGIO = APPOGGIO & AdattaLunghezzaNumero(0, 2) 'TRF-RITA-CAUSA                   	2
                APPOGGIO = APPOGGIO & Space(4) 'TRF-RITA-TRIBU                   	4
                APPOGGIO = APPOGGIO & AdattaLunghezzaNumero(0, 8) 'TRF-RITA-DTVERS                  	8
                APPOGGIO = APPOGGIO & AdattaLunghezzaNumero(0, 11) 'TRF-RITA-IMPAG                   	11
                APPOGGIO = APPOGGIO & AdattaLunghezzaNumero(0, 1) 'TRF-RITA-TPAG                    	1
                APPOGGIO = APPOGGIO & AdattaLunghezzaNumero(0, 4) 'TRF-RITA-SERIE                   	4
                APPOGGIO = APPOGGIO & AdattaLunghezzaNumero(0, 12) 'TRF-RITA-QUIETANZA               	12
                APPOGGIO = APPOGGIO & AdattaLunghezzaNumero(0, 12) 'TRF-RITA-NUM-BOLL                	12
                APPOGGIO = APPOGGIO & AdattaLunghezzaNumero(0, 5) 'TRF-RITA-ABI                     	5
                APPOGGIO = APPOGGIO & AdattaLunghezzaNumero(0, 5) 'TRF-RITA-CAB                     	5
                APPOGGIO = APPOGGIO & AdattaLunghezzaNumero(0, 4) 'TRF-RITA-AACOMP                  	4
                APPOGGIO = APPOGGIO & AdattaLunghezzaNumero(0, 11) 'TRF-RITA-CRED                    	11


                APPOGGIO = APPOGGIO & Space(1) 'TRF-RITA-SOGG                    	1
                APPOGGIO = APPOGGIO & AdattaLunghezzaNumero(0, 11) 'TRF-RITA-BASEIMP                 	11
                APPOGGIO = APPOGGIO & AdattaLunghezzaNumero(0, 11) 'TRF-RITA-FRANCHIGIA              	11
                APPOGGIO = APPOGGIO & AdattaLunghezzaNumero(0, 11) 'TRF-RITA-CTO-PERC                	11
                APPOGGIO = APPOGGIO & AdattaLunghezzaNumero(0, 11) 'TRF-RITA-CTO-DITT                	11
                APPOGGIO = APPOGGIO & Space(11) 'FILLER(11)
                APPOGGIO = APPOGGIO & AdattaLunghezzaNumero(0, 8) 'TRF-RITA-DATA                    	8
                APPOGGIO = APPOGGIO & AdattaLunghezzaNumero(0, 11) 'TRF-RITA-TOTDOC                  	11
                APPOGGIO = APPOGGIO & AdattaLunghezzaNumero(0, 11) 'TRF-RITA-IMPVERS                 	11
                APPOGGIO = APPOGGIO & AdattaLunghezzaNumero(0, 8) 'TRF-RITA-DATA-I                  	8
                APPOGGIO = APPOGGIO & AdattaLunghezzaNumero(0, 8) 'TRF-RITA-DATA-F                  	8
                APPOGGIO = APPOGGIO & AdattaLunghezzaNumero(0, 2) 'TRF-EMENS-ATT                	2
                APPOGGIO = APPOGGIO & AdattaLunghezzaNumero(0, 2) 'TRF-EMENS-RAP                	2
                APPOGGIO = APPOGGIO & AdattaLunghezzaNumero(0, 3) 'TRF-EMENS-ASS                	3
                APPOGGIO = APPOGGIO & AdattaLunghezzaNumero(0, 11) 'TRF-RITA-TOTIVA                  	11


                APPOGGIO = APPOGGIO & AdattaLunghezzaNumero(0, 3) 'TRF-CAUS-PREST-ANA-B	3	NU
                APPOGGIO = APPOGGIO & AdattaLunghezzaNumero(0, 3) 'TRF-RITA-CAUSA-B	3	NU
                APPOGGIO = APPOGGIO & Space(178) 'FILLER                           	178	AN


                TabTrasco.EXPORT = ""
                TabTrasco.SENIOR = DO11_CONDPAG_CODICE_SEN
                TabTrasco.TIPOTAB = "MP"
                TabTrasco.Leggi(Session("DC_TABELLE"))
                APPOGGIO = APPOGGIO & AdattaLunghezzaNumero(5, 3) 'TRF_CFCODPAG 

                APPOGGIO = APPOGGIO & AdattaLunghezzaNumero(0, 5) '  TRF-POR-BANCA                    
                APPOGGIO = APPOGGIO & AdattaLunghezzaNumero(0, 5) '  TRF-POR-AGENZIA                  
                APPOGGIO = APPOGGIO & Space(30) '  TRF-POR-DESAGENZIA               
                APPOGGIO = APPOGGIO & AdattaLunghezzaNumero(1, 2) 'TRF-POR-TOT-RATE                 

                If campodb(myPOSTreader.Item("RegistroIVA")) = 6 Then
                    APPOGGIO = APPOGGIO & AdattaLunghezzaNumero(Math.Round(Imponibile1 + Imponibile2 + Imponibile3, 2) * 100, 12) 'TRF-POR-TOTDOC                   
                Else
                    APPOGGIO = APPOGGIO & AdattaLunghezzaNumero(Math.Round(TotFat, 2) * 100, 12) 'TRF-POR-TOTDOC                   
                End If


                APPOGGIO = APPOGGIO & AdattaLunghezzaNumero(1, 2) '  TRF-POR-NUM-RATA                                    
                If Day(campodbD(myPOSTreader.Item("DataRegistrazione"))) < 5 Then
                    APPOGGIO = APPOGGIO & Format(DateSerial(Year(campodbD(myPOSTreader.Item("DataRegistrazione"))), Month(campodbD(myPOSTreader.Item("DataRegistrazione"))), 5), "ddMMyyyy") '  TRF-POR-DATASCAD                                    
                Else
                    If Month(campodbD(myPOSTreader.Item("DataRegistrazione"))) = 12 Then
                        APPOGGIO = APPOGGIO & Format(DateSerial(Year(campodbD(myPOSTreader.Item("DataRegistrazione"))) + 1, 1, 5), "ddMMyyyy") '  TRF-POR-DATASCAD                                    
                    Else
                        APPOGGIO = APPOGGIO & Format(DateSerial(Year(campodbD(myPOSTreader.Item("DataRegistrazione"))), Month(campodbD(myPOSTreader.Item("DataRegistrazione"))) + 1, 5), "ddMMyyyy") '  TRF-POR-DATASCAD                                    
                    End If
                End If


                APPOGGIO = APPOGGIO & AdattaLunghezzaNumero(3, 1) 'TRF-POR-TIPOEFF          

                If CausaleContabile.TipoDocumento = "NC" Then
                    If campodb(myPOSTreader.Item("RegistroIVA")) = 6 Then
                        APPOGGIO = APPOGGIO & AdattaLunghezzaNumero(Math.Round(Imponibile1 + Imponibile2 + Imponibile3, 2) * -1 * 100, 12) 'TRF-POR-TOTDOC 
                    Else
                        APPOGGIO = APPOGGIO & AdattaLunghezzaNumero(Math.Round(TotFat, 2) * -1 * 100, 12) 'TRF-POR-TOTDOC 
                    End If
                Else
                    If campodb(myPOSTreader.Item("RegistroIVA")) = 6 Then
                        APPOGGIO = APPOGGIO & AdattaLunghezzaNumero(Math.Round(Imponibile1 + Imponibile2 + Imponibile3, 2) * 100, 12) 'TRF-POR-TOTDOC 
                    Else
                        APPOGGIO = APPOGGIO & AdattaLunghezzaNumero(Math.Round(TotFat, 2) * 100, 12) 'TRF-POR-TOTDOC 
                    End If
                End If

                APPOGGIO = APPOGGIO & AdattaLunghezzaNumero(0, 15) 'TRF-POR-IMPORTO-EFFVAL           

                APPOGGIO = APPOGGIO & AdattaLunghezzaNumero(0, 12) 'TRF-POR-IMPORTO-BOLLI             
                APPOGGIO = APPOGGIO & AdattaLunghezzaNumero(0, 15) 'TRF-POR-IMPORTO-BOLIVAL         
                APPOGGIO = APPOGGIO & Space(1) 'TRF-POR-FLAG                     
                APPOGGIO = APPOGGIO & Space(1) 'TRF-POR-TIPO-RD        

                'rata
                For I = 1 To 11
                    APPOGGIO = APPOGGIO & AdattaLunghezzaNumero(0, 2) '  TRF-POR-NUM-RATA                                    
                    APPOGGIO = APPOGGIO & AdattaLunghezzaNumero(0, 8) '  TRF-POR-DATASCAD                                    

                    APPOGGIO = APPOGGIO & AdattaLunghezzaNumero(0, 1) 'TRF-POR-TIPOEFF                          
                    APPOGGIO = APPOGGIO & AdattaLunghezzaNumero(0, 12) 'TRF-POR-TOTDOC 

                    APPOGGIO = APPOGGIO & AdattaLunghezzaNumero(0, 15) 'TRF-POR-IMPORTO-EFFVAL           

                    APPOGGIO = APPOGGIO & AdattaLunghezzaNumero(0, 12) 'TRF-POR-IMPORTO-BOLLI             
                    APPOGGIO = APPOGGIO & AdattaLunghezzaNumero(0, 15) 'TRF-POR-IMPORTO-BOLIVAL         
                    APPOGGIO = APPOGGIO & Space(1) 'TRF-POR-FLAG                     
                    APPOGGIO = APPOGGIO & Space(1) 'TRF-POR-TIPO-RD                                      
                Next
                APPOGGIO = APPOGGIO & AdattaLunghezzaNumero(0, 4) 'TRF-POR-CODAGE                

                APPOGGIO = APPOGGIO & Space(12) 'TRF-POR-EFFETTO-SOSP  
                APPOGGIO = APPOGGIO & Space(15) 'TRF-POR-CIG  
                APPOGGIO = APPOGGIO & Space(15) 'TRF-POR-CUP 
                APPOGGIO = APPOGGIO & Space(294) 'FILLER


                For I = 1 To 20
                    APPOGGIO = APPOGGIO & Space(3) '  TRF-COD-VAL-IV                 	3
                    APPOGGIO = APPOGGIO & AdattaLunghezzaNumero(0, 16) '  TRF-IMP-VALUTA-IV                 	16(14+2DEC)
                Next

                For I = 1 To 20
                    APPOGGIO = APPOGGIO & Space(6) '  TRF-CODICE-SERVIZIO                 	6
                    APPOGGIO = APPOGGIO & AdattaLunghezzaNumero(0, 3) '  TRF-STATO-PAGAMENTO	3
                    APPOGGIO = APPOGGIO & AdattaLunghezzaNumero(0, 12) '  TRF-SERV-IMP-EURO                     	12
                    APPOGGIO = APPOGGIO & AdattaLunghezzaNumero(0, 12) '  TRF-SERV-IMP-VAL                      	12(10+2 DEC)
                    APPOGGIO = APPOGGIO & AdattaLunghezzaNumero(0, 8) '  TRF-DATA-DOC-ORIG                       	8
                    APPOGGIO = APPOGGIO & AdattaLunghezzaNumero(0, 1) '  TRF-MOD-EROGAZIONE                        	1
                    APPOGGIO = APPOGGIO & AdattaLunghezzaNumero(0, 1) '  TRF-MOD-INCASSO                     	1                
                    APPOGGIO = APPOGGIO & AdattaLunghezzaNumero(0, 6) '  TRF-PROT-REG                     	6
                    APPOGGIO = APPOGGIO & AdattaLunghezzaNumero(0, 6) '  TRF-PROG-REG                       	6
                    APPOGGIO = APPOGGIO & AdattaLunghezzaNumero(0, 6) '  TRF-COD-SEZ-DOG-RET                    	6
                    APPOGGIO = APPOGGIO & AdattaLunghezzaNumero(0, 2) '  TRF-ANNO-REG-RET                  	2
                    APPOGGIO = APPOGGIO & AdattaLunghezzaNumero(0, 15) '  TRF-NUM-DOC-ORIG                   	15
                    APPOGGIO = APPOGGIO & AdattaLunghezzaNumero(0, 1) '  TRF-SERV-SEGNO-RET                  	1
                    APPOGGIO = APPOGGIO & AdattaLunghezzaNumero(0, 3) '  TRF-SERV-COD-VAL-IV                 	3
                    APPOGGIO = APPOGGIO & AdattaLunghezzaNumero(0, 16) '  TRF-SERV-IMP-VALUTA-IV                 	16(14+2DEC)	
                Next

                APPOGGIO = APPOGGIO & AdattaLunghezzaNumero(0, 1) '  TRF-INTRA-TIPO-SERVIZIO	1
                APPOGGIO = APPOGGIO & AdattaLunghezzaNumero(0, 6) '  TRF-SERV-MESE-ANNO-RIF	6


                APPOGGIO = APPOGGIO & Space(8) '  TRF-CK-RCHARGE	1



                APPOGGIO = APPOGGIO & Space(15) '  TRF-XNUM-DOC-ORI	15                         

                APPOGGIO = APPOGGIO & Space(1) '  TRF-MEM-ESIGIB-IVA	1
                APPOGGIO = APPOGGIO & Space(2)  '  TRF-COD-IDENTIFICATIVO	2   
                APPOGGIO = APPOGGIO & Space(12)  '  TRF-ID-IMPORTAZIONE	12

                APPOGGIO = APPOGGIO & Space(1076)  '  1076
                APPOGGIO = APPOGGIO & Space(2)  '  FILLER	2


                tw.WriteLine(APPOGGIO)
            End If


            If Session("DC_OSPITE").ToString.ToUpper.IndexOf("CREA") >= 0 Then
                APPOGGIO = ""
                APPOGGIO = APPOGGIO & AdattaLunghezzaNumero("1", 5) ' TRF_DITTA 
                APPOGGIO = APPOGGIO & "3" 'TRF1-VERSIONE
                APPOGGIO = APPOGGIO & "1" 'TRF1-TARC                        


                APPOGGIO = APPOGGIO & AdattaLunghezzaNumero(0, 5) 'TRF-NUM-AUTOFATT                 
                APPOGGIO = APPOGGIO & AdattaLunghezzaNumero(0, 2) 'TRF-SERIE-AUTOFATT                            
                APPOGGIO = APPOGGIO & Space(3) 'Codice Valuta
                APPOGGIO = APPOGGIO & AdattaLunghezzaNumero(0, 14) 'Totale importo in valuta

                For I = 1 To 20
                    APPOGGIO = APPOGGIO & Space(8) '  TRF-NOMENCLATURA                 	8
                    APPOGGIO = APPOGGIO & AdattaLunghezzaNumero(0, 12)  'TRF-IMP-LIRE                     	12
                    APPOGGIO = APPOGGIO & AdattaLunghezzaNumero(0, 12)  'TRF-IMP-VAL                      	12(10+2 DEC)
                    APPOGGIO = APPOGGIO & Space(1) 'TRF-NATURA                       	1
                    APPOGGIO = APPOGGIO & AdattaLunghezzaNumero(0, 12)  'TRF-MASSA                        	12(10+2 DEC)
                    APPOGGIO = APPOGGIO & AdattaLunghezzaNumero(0, 12)  'TRF-UN-SUPPL                     	12
                    APPOGGIO = APPOGGIO & AdattaLunghezzaNumero(0, 12)  'TRF-VAL-STAT                     	12
                    APPOGGIO = APPOGGIO & Space(1) 'TRF-REGIME                       	1
                    APPOGGIO = APPOGGIO & Space(1) 'TRF-TRASPORTO                    	1
                    APPOGGIO = APPOGGIO & AdattaLunghezzaNumero(0, 3) 'TRF-PAESE-PROV                   	3
                    APPOGGIO = APPOGGIO & AdattaLunghezzaNumero(0, 3) 'TRF-PAESE-ORIG                   	3
                    APPOGGIO = APPOGGIO & AdattaLunghezzaNumero(0, 3) 'TRF-PAESE-DEST                   	3
                    APPOGGIO = APPOGGIO & Space(2) 'TRF-PROV-DEST                    	2
                    APPOGGIO = APPOGGIO & Space(2) 'TRF-PROV-ORIG                    	2
                    APPOGGIO = APPOGGIO & Space(1) 'TRF-SEGNO-RET	1
                Next
                APPOGGIO = APPOGGIO & Space(1) 'TRF-INTRA-TIPO	1
                APPOGGIO = APPOGGIO & AdattaLunghezzaNumero(0, 6) 'TRF-MESE-ANNO-RIF	6
                APPOGGIO = APPOGGIO & Space(173) '173:


                APPOGGIO = APPOGGIO & AdattaLunghezzaNumero(0, 1) 'TRF-RITA-TIPO                    	1
                APPOGGIO = APPOGGIO & AdattaLunghezzaNumero(0, 11) 'TRF-RITA-IMPON                   	11
                APPOGGIO = APPOGGIO & AdattaLunghezzaNumero(0, 4) 'TRF-RITA-ALIQ                    	4(2+2dec)
                APPOGGIO = APPOGGIO & AdattaLunghezzaNumero(0, 10) 'TRF-RITA-IMPRA                   	10
                APPOGGIO = APPOGGIO & AdattaLunghezzaNumero(0, 11) 'TRF-RITA-PRONS                   	11
                APPOGGIO = APPOGGIO & AdattaLunghezzaNumero(0, 6) 'TRF-RITA-MESE                    	6
                APPOGGIO = APPOGGIO & AdattaLunghezzaNumero(0, 2) 'TRF-RITA-CAUSA                   	2
                APPOGGIO = APPOGGIO & Space(4) 'TRF-RITA-TRIBU                   	4
                APPOGGIO = APPOGGIO & AdattaLunghezzaNumero(0, 8) 'TRF-RITA-DTVERS                  	8
                APPOGGIO = APPOGGIO & AdattaLunghezzaNumero(0, 11) 'TRF-RITA-IMPAG                   	11
                APPOGGIO = APPOGGIO & AdattaLunghezzaNumero(0, 1) 'TRF-RITA-TPAG                    	1
                APPOGGIO = APPOGGIO & AdattaLunghezzaNumero(0, 4) 'TRF-RITA-SERIE                   	4
                APPOGGIO = APPOGGIO & AdattaLunghezzaNumero(0, 12) 'TRF-RITA-QUIETANZA               	12
                APPOGGIO = APPOGGIO & AdattaLunghezzaNumero(0, 12) 'TRF-RITA-NUM-BOLL                	12
                APPOGGIO = APPOGGIO & AdattaLunghezzaNumero(0, 5) 'TRF-RITA-ABI                     	5
                APPOGGIO = APPOGGIO & AdattaLunghezzaNumero(0, 5) 'TRF-RITA-CAB                     	5
                APPOGGIO = APPOGGIO & AdattaLunghezzaNumero(0, 4) 'TRF-RITA-AACOMP                  	4
                APPOGGIO = APPOGGIO & AdattaLunghezzaNumero(0, 11) 'TRF-RITA-CRED                    	11


                APPOGGIO = APPOGGIO & Space(1) 'TRF-RITA-SOGG                    	1
                APPOGGIO = APPOGGIO & AdattaLunghezzaNumero(0, 11) 'TRF-RITA-BASEIMP                 	11
                APPOGGIO = APPOGGIO & AdattaLunghezzaNumero(0, 11) 'TRF-RITA-FRANCHIGIA              	11
                APPOGGIO = APPOGGIO & AdattaLunghezzaNumero(0, 11) 'TRF-RITA-CTO-PERC                	11
                APPOGGIO = APPOGGIO & AdattaLunghezzaNumero(0, 11) 'TRF-RITA-CTO-DITT                	11
                APPOGGIO = APPOGGIO & Space(11) 'FILLER(11)
                APPOGGIO = APPOGGIO & AdattaLunghezzaNumero(0, 8) 'TRF-RITA-DATA                    	8
                APPOGGIO = APPOGGIO & AdattaLunghezzaNumero(0, 11) 'TRF-RITA-TOTDOC                  	11
                APPOGGIO = APPOGGIO & AdattaLunghezzaNumero(0, 11) 'TRF-RITA-IMPVERS                 	11
                APPOGGIO = APPOGGIO & AdattaLunghezzaNumero(0, 8) 'TRF-RITA-DATA-I                  	8
                APPOGGIO = APPOGGIO & AdattaLunghezzaNumero(0, 8) 'TRF-RITA-DATA-F                  	8
                APPOGGIO = APPOGGIO & AdattaLunghezzaNumero(0, 2) 'TRF-EMENS-ATT                	2
                APPOGGIO = APPOGGIO & AdattaLunghezzaNumero(0, 2) 'TRF-EMENS-RAP                	2
                APPOGGIO = APPOGGIO & AdattaLunghezzaNumero(0, 3) 'TRF-EMENS-ASS                	3
                APPOGGIO = APPOGGIO & AdattaLunghezzaNumero(0, 11) 'TRF-RITA-TOTIVA                  	11


                APPOGGIO = APPOGGIO & AdattaLunghezzaNumero(0, 3) 'TRF-CAUS-PREST-ANA-B	3	NU
                APPOGGIO = APPOGGIO & AdattaLunghezzaNumero(0, 3) 'TRF-RITA-CAUSA-B	3	NU
                APPOGGIO = APPOGGIO & Space(178) 'FILLER                           	178	AN

                APPOGGIO = APPOGGIO & AdattaLunghezzaNumero(5, 3) 'TRF_CFCODPAG 

                APPOGGIO = APPOGGIO & AdattaLunghezzaNumero(0, 5) '  TRF-POR-BANCA                    
                APPOGGIO = APPOGGIO & AdattaLunghezzaNumero(0, 5) '  TRF-POR-AGENZIA                  
                APPOGGIO = APPOGGIO & Space(30) '  TRF-POR-DESAGENZIA               
                APPOGGIO = APPOGGIO & AdattaLunghezzaNumero(1, 2) 'TRF-POR-TOT-RATE                 
                APPOGGIO = APPOGGIO & AdattaLunghezzaNumero(0, 12) 'TRF-POR-TOTDOC                   

                APPOGGIO = APPOGGIO & AdattaLunghezzaNumero(1, 2) '  TRF-POR-NUM-RATA                                    
                APPOGGIO = APPOGGIO & AdattaLunghezzaNumero(1, 8) '  TRF-POR-DATASCAD                                    

                APPOGGIO = APPOGGIO & AdattaLunghezzaNumero(3, 1) 'TRF-POR-TIPOEFF                          
                APPOGGIO = APPOGGIO & AdattaLunghezzaNumero(0, 12) 'TRF-POR-TOTDOC 

                APPOGGIO = APPOGGIO & AdattaLunghezzaNumero(0, 15) 'TRF-POR-IMPORTO-EFFVAL           

                APPOGGIO = APPOGGIO & AdattaLunghezzaNumero(0, 12) 'TRF-POR-IMPORTO-BOLLI             
                APPOGGIO = APPOGGIO & AdattaLunghezzaNumero(0, 15) 'TRF-POR-IMPORTO-BOLIVAL         
                APPOGGIO = APPOGGIO & Space(1) 'TRF-POR-FLAG                     
                APPOGGIO = APPOGGIO & Space(1) 'TRF-POR-TIPO-RD        

                'rata
                For I = 1 To 11
                    APPOGGIO = APPOGGIO & AdattaLunghezzaNumero(0, 2) '  TRF-POR-NUM-RATA                                    
                    APPOGGIO = APPOGGIO & AdattaLunghezzaNumero(0, 8) '  TRF-POR-DATASCAD                                    

                    APPOGGIO = APPOGGIO & AdattaLunghezzaNumero(0, 1) 'TRF-POR-TIPOEFF                          
                    APPOGGIO = APPOGGIO & AdattaLunghezzaNumero(0, 12) 'TRF-POR-TOTDOC 

                    APPOGGIO = APPOGGIO & AdattaLunghezzaNumero(0, 15) 'TRF-POR-IMPORTO-EFFVAL           

                    APPOGGIO = APPOGGIO & AdattaLunghezzaNumero(0, 12) 'TRF-POR-IMPORTO-BOLLI             
                    APPOGGIO = APPOGGIO & AdattaLunghezzaNumero(0, 15) 'TRF-POR-IMPORTO-BOLIVAL         
                    APPOGGIO = APPOGGIO & Space(1) 'TRF-POR-FLAG                     
                    APPOGGIO = APPOGGIO & Space(1) 'TRF-POR-TIPO-RD                                      
                Next
                APPOGGIO = APPOGGIO & AdattaLunghezzaNumero(0, 4) 'TRF-POR-CODAGE                

                APPOGGIO = APPOGGIO & Space(12) 'TRF-POR-EFFETTO-SOSP  
                APPOGGIO = APPOGGIO & Space(15) 'TRF-POR-CIG  
                APPOGGIO = APPOGGIO & Space(15) 'TRF-POR-CUP 
                APPOGGIO = APPOGGIO & Space(294) 'FILLER


                For I = 1 To 20
                    APPOGGIO = APPOGGIO & Space(3) '  TRF-COD-VAL-IV                 	3
                    APPOGGIO = APPOGGIO & AdattaLunghezzaNumero(0, 16) '  TRF-IMP-VALUTA-IV                 	16(14+2DEC)
                Next

                For I = 1 To 20
                    APPOGGIO = APPOGGIO & Space(6) '  TRF-CODICE-SERVIZIO                 	6
                    APPOGGIO = APPOGGIO & AdattaLunghezzaNumero(0, 3) '  TRF-STATO-PAGAMENTO	3
                    APPOGGIO = APPOGGIO & AdattaLunghezzaNumero(0, 12) '  TRF-SERV-IMP-EURO                     	12
                    APPOGGIO = APPOGGIO & AdattaLunghezzaNumero(0, 12) '  TRF-SERV-IMP-VAL                      	12(10+2 DEC)
                    APPOGGIO = APPOGGIO & AdattaLunghezzaNumero(0, 8) '  TRF-DATA-DOC-ORIG                       	8
                    APPOGGIO = APPOGGIO & AdattaLunghezzaNumero(0, 1) '  TRF-MOD-EROGAZIONE                        	1
                    APPOGGIO = APPOGGIO & AdattaLunghezzaNumero(0, 1) '  TRF-MOD-INCASSO                     	1                
                    APPOGGIO = APPOGGIO & AdattaLunghezzaNumero(0, 6) '  TRF-PROT-REG                     	6
                    APPOGGIO = APPOGGIO & AdattaLunghezzaNumero(0, 6) '  TRF-PROG-REG                       	6
                    APPOGGIO = APPOGGIO & AdattaLunghezzaNumero(0, 6) '  TRF-COD-SEZ-DOG-RET                    	6
                    APPOGGIO = APPOGGIO & AdattaLunghezzaNumero(0, 2) '  TRF-ANNO-REG-RET                  	2
                    APPOGGIO = APPOGGIO & AdattaLunghezzaNumero(0, 15) '  TRF-NUM-DOC-ORIG                   	15
                    APPOGGIO = APPOGGIO & AdattaLunghezzaNumero(0, 1) '  TRF-SERV-SEGNO-RET                  	1
                    APPOGGIO = APPOGGIO & AdattaLunghezzaNumero(0, 3) '  TRF-SERV-COD-VAL-IV                 	3
                    APPOGGIO = APPOGGIO & AdattaLunghezzaNumero(0, 16) '  TRF-SERV-IMP-VALUTA-IV                 	16(14+2DEC)	
                Next

                APPOGGIO = APPOGGIO & AdattaLunghezzaNumero(0, 1) '  TRF-INTRA-TIPO-SERVIZIO	1
                APPOGGIO = APPOGGIO & AdattaLunghezzaNumero(0, 6) '  TRF-SERV-MESE-ANNO-RIF	6


                APPOGGIO = APPOGGIO & Space(8) '  TRF-CK-RCHARGE	1


                If campodb(myPOSTreader.Item("RegistroIVA")) = 1 Then
                    APPOGGIO = APPOGGIO & AdattaLunghezza(campodb(myPOSTreader.Item("NumeroProtocollo")) & "/00", 15)   '  TRF-XNUM-DOC-ORI	15                         
                Else
                    APPOGGIO = APPOGGIO & AdattaLunghezza(campodb(myPOSTreader.Item("NumeroProtocollo")) & "/01", 15)   '  TRF-XNUM-DOC-ORI	15                         
                End If

                APPOGGIO = APPOGGIO & Space(1) '  TRF-MEM-ESIGIB-IVA	1
                APPOGGIO = APPOGGIO & Space(2)  '  TRF-COD-IDENTIFICATIVO	2   
                APPOGGIO = APPOGGIO & Space(12)  '  TRF-ID-IMPORTAZIONE	12

                APPOGGIO = APPOGGIO & Space(1076)  '  1076
                APPOGGIO = APPOGGIO & Space(2)  '  FILLER	2


                tw.WriteLine(APPOGGIO)
            End If

            APPOGGIO = ""
            If Session("DC_OSPITE").ToString.ToUpper.IndexOf("LIBERA") >= 0 Then
                APPOGGIO = CentriCostoLibera(APPOGGIO, tw, myPOSTreader, CausaleContabile, CONTO2, CONTO3, Imponibile1, Imponibile2, Imponibile3, Quantita1, Quantita2, Quantita3)
            End If

            APPOGGIO = ""
            If Session("DC_OSPITE").ToString.ToUpper.IndexOf("CREA") >= 0 Then
                Dim FineFor As Integer = 20
                APPOGGIO = ""
                APPOGGIO = APPOGGIO & AdattaLunghezzaNumero("1", 5) ' TRF_DITTA 
                APPOGGIO = APPOGGIO & "3"
                APPOGGIO = APPOGGIO & "2"
                If Imponibile1 > 0 Then
                    If CausaleContabile.TipoDocumento = "NC" Then
                        APPOGGIO = APPOGGIO & AdattaLunghezzaNumero("002", 3) 'TRF_CAUSALE 
                    Else
                        APPOGGIO = APPOGGIO & AdattaLunghezzaNumero("001", 3) 'TRF_CAUSALE 
                    End If

                    If CONTO1 = 5851571 Or CONTO1 = 5851572 Or CONTO1 = 5851573 Then 'CD CASA NONNI
                        APPOGGIO = APPOGGIO & AdattaLunghezzaNumero("201002", 8) 'TRF-MCI-CDC-CONTO        
                    End If
                    If CONTO1 = 5851531 Then 'CIMBILIUM CC
                        APPOGGIO = APPOGGIO & AdattaLunghezzaNumero("101002", 8) 'TRF-MCI-CDC-CONTO        
                    End If
                    If CONTO1 = 5851511 Then  'CDD VIAREGGIO - IL CAPANNONE CP
                        APPOGGIO = APPOGGIO & AdattaLunghezzaNumero("101000", 8) 'TRF-MCI-CDC-CONTO        
                    End If
                    If CONTO1 = 5851541 Then   'GIOCORAGGIO CG
                        APPOGGIO = APPOGGIO & AdattaLunghezzaNumero("101001", 8) 'TRF-MCI-CDC-CONTO        
                    End If
                    If campodb(myPOSTreader.Item("CENTROSERVIZIO")) = "CA" Then
                        APPOGGIO = APPOGGIO & AdattaLunghezzaNumero("201001", 8) 'TRF-MCI-CDC-CONTO        
                    End If
                    If campodb(myPOSTreader.Item("CENTROSERVIZIO")) = "RSA" Then
                        APPOGGIO = APPOGGIO & AdattaLunghezzaNumero("201002", 8) 'TRF-MCI-CDC-CONTO        
                    End If

                    If campodb(myPOSTreader.Item("CENTROSERVIZIO")) = "CAP" Then
                        APPOGGIO = APPOGGIO & AdattaLunghezzaNumero("201004", 8) 'TRF-MCI-CDC-CONTO        
                    End If


                    If CONTO1 = 5851571 Or CONTO1 = 5851572 Or CONTO1 = 5851573 Then 'CD CASA NONNI
                        APPOGGIO = APPOGGIO & AdattaLunghezzaNumero("10001004", 8) 'TRF-MCI-CDC-CONTO        
                    End If
                    If CONTO1 = 5851531 Then 'CIMBILIUM CC
                        APPOGGIO = APPOGGIO & AdattaLunghezzaNumero("10001000", 8) 'TRF-MCI-CDC-CONTO        
                    End If
                    If CONTO1 = 5851511 Then  'CDD VIAREGGIO - IL CAPANNONE CP
                        APPOGGIO = APPOGGIO & AdattaLunghezzaNumero("10001000", 8) 'TRF-MCI-CDC-CONTO        
                    End If
                    If CONTO1 = 5851541 Then   'GIOCORAGGIO CG
                        APPOGGIO = APPOGGIO & AdattaLunghezzaNumero("10001000", 8) 'TRF-MCI-CDC-CONTO        
                    End If
                    If campodb(myPOSTreader.Item("CENTROSERVIZIO")) = "CA" Then
                        APPOGGIO = APPOGGIO & AdattaLunghezzaNumero("10001001", 8) 'TRF-MCI-CDC-CONTO        
                    End If
                    If campodb(myPOSTreader.Item("CENTROSERVIZIO")) = "RSA" Then
                        APPOGGIO = APPOGGIO & AdattaLunghezzaNumero("10001002", 8) 'TRF-MCI-CDC-CONTO        
                    End If

                    If campodb(myPOSTreader.Item("CENTROSERVIZIO")) = "CAP" Then
                        APPOGGIO = APPOGGIO & AdattaLunghezzaNumero("10001003", 8) 'TRF-MCI-CDC-CONTO        
                    End If


                    Dim AnnoData As Integer = campodbN(myPOSTreader.Item("ANNOCOMPETENZA"))
                    Dim MeseData As Integer = campodbN(myPOSTreader.Item("MESECOMPETENZA"))

                    APPOGGIO = APPOGGIO & Format(DateSerial(AnnoData, MeseData, 1), "ddMMyyyy")
                    APPOGGIO = APPOGGIO & Format(DateSerial(AnnoData, MeseData, GiorniMese(MeseData, AnnoData)), "ddMMyyyy")
                    APPOGGIO = APPOGGIO & AdattaLunghezzaNumero("0", 6)
                    APPOGGIO = APPOGGIO & AdattaLunghezzaNumero("0", 3)
                    APPOGGIO = APPOGGIO & AdattaLunghezza("", 20)

                    If CausaleContabile.TipoDocumento = "NC" Then
                        APPOGGIO = APPOGGIO & "A"
                    Else
                        APPOGGIO = APPOGGIO & "D"
                    End If

                    APPOGGIO = APPOGGIO & AdattaLunghezzaNumero(Quantita1 * 10000, 12) '  TRF-MCI-QUANTITA

                    APPOGGIO = APPOGGIO & AdattaLunghezzaNumero(((Imponibile1) / Quantita1) * 10000000, 17) '   TRF-MCI-COSTO-UNIT
                    APPOGGIO = APPOGGIO & AdattaLunghezzaNumero((Imponibile1) * 10000000, 18) '   TRF-MCI-COSTO-COMPLESS
                    APPOGGIO = APPOGGIO & AdattaLunghezzaNumero("0", 12) '   TRF-MCI-QTALAV
                    APPOGGIO = APPOGGIO & AdattaLunghezzaNumero("0", 10) '   TRF-MCI-TOTORE
                    APPOGGIO = APPOGGIO & AdattaLunghezzaNumero("0", 17) '   TRF-MCI-COSORA-CDC
                    APPOGGIO = APPOGGIO & AdattaLunghezzaNumero("0", 17) '   TRF-MCI-COSORA-VDS
                    APPOGGIO = APPOGGIO & AdattaLunghezza("", 18) '   TRF-MCI-DESCRIAGG
                    APPOGGIO = APPOGGIO & AdattaLunghezzaNumero("0", 3) '   TRF-MCI-DEP-PROD
                    APPOGGIO = APPOGGIO & AdattaLunghezzaNumero("0", 3) '   TRF-MCI-SEZ-PROD
                    APPOGGIO = APPOGGIO & AdattaLunghezzaNumero("0", 6) '   TRF-MCI-NUM-PROD
                    APPOGGIO = APPOGGIO & AdattaLunghezzaNumero("0", 3) '   TRF-MCI-LINEA
                    APPOGGIO = APPOGGIO & AdattaLunghezzaNumero("0", 1) '   TRF-MCI-TIPOCLFO                 
                    FineFor = FineFor - 1
                End If
                If Imponibile2 > 0 Then
                    If CausaleContabile.TipoDocumento = "NC" Then
                        APPOGGIO = APPOGGIO & AdattaLunghezzaNumero("002", 3) 'TRF_CAUSALE 
                    Else
                        APPOGGIO = APPOGGIO & AdattaLunghezzaNumero("001", 3) 'TRF_CAUSALE 
                    End If

                    If CONTO2 = 5851571 Or CONTO2 = 5851572 Or CONTO2 = 5851573 Then 'CD CASA NONNI
                        APPOGGIO = APPOGGIO & AdattaLunghezzaNumero("201002", 8) 'TRF-MCI-CDC-CONTO        
                    End If


                    If CONTO2 = 5851567 Or CONTO2 = 5851568 Or CONTO2 = 5851569 Then 'CAP CASA DON BEPPE SOCCI PRIVATI
                        APPOGGIO = APPOGGIO & AdattaLunghezzaNumero("201004", 8) 'TRF-MCI-CDC-CONTO        
                    End If



                    If CONTO2 = 5851531 Then 'CIMBILIUM CC
                        APPOGGIO = APPOGGIO & AdattaLunghezzaNumero("101002", 8) 'TRF-MCI-CDC-CONTO        
                    End If
                    If CONTO2 = 5851511 Then  'CDD VIAREGGIO - IL CAPANNONE CP
                        APPOGGIO = APPOGGIO & AdattaLunghezzaNumero("101000", 8) 'TRF-MCI-CDC-CONTO        
                    End If
                    If CONTO2 = 5851541 Then   'GIOCORAGGIO CG
                        APPOGGIO = APPOGGIO & AdattaLunghezzaNumero("101001", 8) 'TRF-MCI-CDC-CONTO        
                    End If
                    If campodb(myPOSTreader.Item("CENTROSERVIZIO")) = "CA" Then
                        APPOGGIO = APPOGGIO & AdattaLunghezzaNumero("201001", 8) 'TRF-MCI-CDC-CONTO        
                    End If
                    If campodb(myPOSTreader.Item("CENTROSERVIZIO")) = "RSA" Then
                        APPOGGIO = APPOGGIO & AdattaLunghezzaNumero("201002", 8) 'TRF-MCI-CDC-CONTO        
                    End If


                    If CONTO2 = 5851571 Or CONTO2 = 5851572 Or CONTO2 = 5851573 Then 'CD CASA NONNI
                        APPOGGIO = APPOGGIO & AdattaLunghezzaNumero("10001004", 8) 'TRF-MCI-CDC-CONTO        
                    End If

                    If CONTO2 = 5851567 Or CONTO2 = 5851568 Or CONTO2 = 5851569 Then 'CAP CASA DON BEPPE SOCCI PRIVATI
                        APPOGGIO = APPOGGIO & AdattaLunghezzaNumero("10001003", 8) 'TRF-MCI-CDC-CONTO        
                    End If

                    If CONTO2 = 5851531 Then 'CIMBILIUM CC
                        APPOGGIO = APPOGGIO & AdattaLunghezzaNumero("10001000", 8) 'TRF-MCI-CDC-CONTO        
                    End If
                    If CONTO2 = 5851511 Then  'CDD VIAREGGIO - IL CAPANNONE CP
                        APPOGGIO = APPOGGIO & AdattaLunghezzaNumero("10001000", 8) 'TRF-MCI-CDC-CONTO        
                    End If
                    If CONTO2 = 5851541 Then   'GIOCORAGGIO CG
                        APPOGGIO = APPOGGIO & AdattaLunghezzaNumero("10001000", 8) 'TRF-MCI-CDC-CONTO        
                    End If
                    If campodb(myPOSTreader.Item("CENTROSERVIZIO")) = "CA" Then
                        APPOGGIO = APPOGGIO & AdattaLunghezzaNumero("10001001", 8) 'TRF-MCI-CDC-CONTO        
                    End If
                    If campodb(myPOSTreader.Item("CENTROSERVIZIO")) = "RSA" Then
                        APPOGGIO = APPOGGIO & AdattaLunghezzaNumero("10001002", 8) 'TRF-MCI-CDC-CONTO        
                    End If


                    Dim AnnoData As Integer = campodbN(myPOSTreader.Item("ANNOCOMPETENZA"))
                    Dim MeseData As Integer = campodbN(myPOSTreader.Item("MESECOMPETENZA"))

                    APPOGGIO = APPOGGIO & Format(DateSerial(AnnoData, MeseData, 1), "ddMMyyyy")
                    APPOGGIO = APPOGGIO & Format(DateSerial(AnnoData, MeseData, GiorniMese(MeseData, AnnoData)), "ddMMyyyy")
                    APPOGGIO = APPOGGIO & AdattaLunghezzaNumero("0", 6)
                    APPOGGIO = APPOGGIO & AdattaLunghezzaNumero("0", 3)
                    APPOGGIO = APPOGGIO & AdattaLunghezza("", 20)

                    If CausaleContabile.TipoDocumento = "NC" Then
                        APPOGGIO = APPOGGIO & "A"
                    Else
                        APPOGGIO = APPOGGIO & "D"
                    End If

                    APPOGGIO = APPOGGIO & AdattaLunghezzaNumero(Quantita2 * 10000, 12) '  TRF-MCI-QUANTITA

                    APPOGGIO = APPOGGIO & AdattaLunghezzaNumero(((Imponibile2) / Quantita2) * 10000000, 17) '   TRF-MCI-COSTO-UNIT
                    APPOGGIO = APPOGGIO & AdattaLunghezzaNumero((Imponibile2) * 10000000, 18) '   TRF-MCI-COSTO-COMPLESS
                    APPOGGIO = APPOGGIO & AdattaLunghezzaNumero("0", 12) '   TRF-MCI-QTALAV
                    APPOGGIO = APPOGGIO & AdattaLunghezzaNumero("0", 10) '   TRF-MCI-TOTORE
                    APPOGGIO = APPOGGIO & AdattaLunghezzaNumero("0", 17) '   TRF-MCI-COSORA-CDC
                    APPOGGIO = APPOGGIO & AdattaLunghezzaNumero("0", 17) '   TRF-MCI-COSORA-VDS
                    APPOGGIO = APPOGGIO & AdattaLunghezza("", 18) '   TRF-MCI-DESCRIAGG
                    APPOGGIO = APPOGGIO & AdattaLunghezzaNumero("0", 3) '   TRF-MCI-DEP-PROD
                    APPOGGIO = APPOGGIO & AdattaLunghezzaNumero("0", 3) '   TRF-MCI-SEZ-PROD
                    APPOGGIO = APPOGGIO & AdattaLunghezzaNumero("0", 6) '   TRF-MCI-NUM-PROD
                    APPOGGIO = APPOGGIO & AdattaLunghezzaNumero("0", 3) '   TRF-MCI-LINEA
                    APPOGGIO = APPOGGIO & AdattaLunghezzaNumero("0", 1) '   TRF-MCI-TIPOCLFO                 
                    FineFor = FineFor - 1
                End If
                If Imponibile3 > 0 Then
                    If CausaleContabile.TipoDocumento = "NC" Then
                        APPOGGIO = APPOGGIO & AdattaLunghezzaNumero("002", 3) 'TRF_CAUSALE 
                    Else
                        APPOGGIO = APPOGGIO & AdattaLunghezzaNumero("001", 3) 'TRF_CAUSALE 
                    End If

                    If CONTO3 = 5851571 Or CONTO3 = 5851572 Or CONTO3 = 5851573 Then 'CD CASA NONNI
                        APPOGGIO = APPOGGIO & AdattaLunghezzaNumero("201002", 8) 'TRF-MCI-CDC-CONTO        
                    End If

                    If CONTO2 = 5851567 Or CONTO2 = 5851568 Or CONTO2 = 5851569 Then 'CAP CASA DON BEPPE SOCCI PRIVATI
                        APPOGGIO = APPOGGIO & AdattaLunghezzaNumero("201004", 8) 'TRF-MCI-CDC-CONTO        
                    End If

                    If CONTO3 = 5851531 Then 'CIMBILIUM CC
                        APPOGGIO = APPOGGIO & AdattaLunghezzaNumero("101002", 8) 'TRF-MCI-CDC-CONTO        
                    End If
                    If CONTO3 = 5851511 Then  'CDD VIAREGGIO - IL CAPANNONE CP
                        APPOGGIO = APPOGGIO & AdattaLunghezzaNumero("101000", 8) 'TRF-MCI-CDC-CONTO        
                    End If
                    If CONTO3 = 5851541 Then   'GIOCORAGGIO CG
                        APPOGGIO = APPOGGIO & AdattaLunghezzaNumero("101001", 8) 'TRF-MCI-CDC-CONTO        
                    End If
                    If campodb(myPOSTreader.Item("CENTROSERVIZIO")) = "CA" Then
                        APPOGGIO = APPOGGIO & AdattaLunghezzaNumero("201001", 8) 'TRF-MCI-CDC-CONTO        
                    End If
                    If campodb(myPOSTreader.Item("CENTROSERVIZIO")) = "RSA" Then
                        APPOGGIO = APPOGGIO & AdattaLunghezzaNumero("201002", 8) 'TRF-MCI-CDC-CONTO        
                    End If


                    If CONTO3 = 5851571 Or CONTO3 = 5851572 Or CONTO3 = 5851573 Then 'CD CASA NONNI
                        APPOGGIO = APPOGGIO & AdattaLunghezzaNumero("10001004", 8) 'TRF-MCI-CDC-CONTO        
                    End If
                    If CONTO3 = 5851531 Then 'CIMBILIUM CC
                        APPOGGIO = APPOGGIO & AdattaLunghezzaNumero("10001000", 8) 'TRF-MCI-CDC-CONTO        
                    End If

                    If CONTO2 = 5851567 Or CONTO2 = 5851568 Or CONTO2 = 5851569 Then 'CAP CASA DON BEPPE SOCCI PRIVATI
                        APPOGGIO = APPOGGIO & AdattaLunghezzaNumero("10001003", 8) 'TRF-MCI-CDC-CONTO        
                    End If

                    If CONTO3 = 5851511 Then  'CDD VIAREGGIO - IL CAPANNONE CP
                        APPOGGIO = APPOGGIO & AdattaLunghezzaNumero("10001000", 8) 'TRF-MCI-CDC-CONTO        
                    End If
                    If CONTO3 = 5851541 Then   'GIOCORAGGIO CG
                        APPOGGIO = APPOGGIO & AdattaLunghezzaNumero("10001000", 8) 'TRF-MCI-CDC-CONTO        
                    End If
                    If campodb(myPOSTreader.Item("CENTROSERVIZIO")) = "CA" Then
                        APPOGGIO = APPOGGIO & AdattaLunghezzaNumero("10001001", 8) 'TRF-MCI-CDC-CONTO        
                    End If
                    If campodb(myPOSTreader.Item("CENTROSERVIZIO")) = "RSA" Then
                        APPOGGIO = APPOGGIO & AdattaLunghezzaNumero("10001002", 8) 'TRF-MCI-CDC-CONTO        
                    End If


                    Dim AnnoData As Integer = campodbN(myPOSTreader.Item("ANNOCOMPETENZA"))
                    Dim MeseData As Integer = campodbN(myPOSTreader.Item("MESECOMPETENZA"))

                    APPOGGIO = APPOGGIO & Format(DateSerial(AnnoData, MeseData, 1), "ddMMyyyy")
                    APPOGGIO = APPOGGIO & Format(DateSerial(AnnoData, MeseData, GiorniMese(MeseData, AnnoData)), "ddMMyyyy")
                    APPOGGIO = APPOGGIO & AdattaLunghezzaNumero("0", 6)
                    APPOGGIO = APPOGGIO & AdattaLunghezzaNumero("0", 3)
                    APPOGGIO = APPOGGIO & AdattaLunghezza("", 20)

                    If CausaleContabile.TipoDocumento = "NC" Then
                        APPOGGIO = APPOGGIO & "A"
                    Else
                        APPOGGIO = APPOGGIO & "D"
                    End If

                    APPOGGIO = APPOGGIO & AdattaLunghezzaNumero(Quantita3 * 10000, 12) '  TRF-MCI-QUANTITA

                    APPOGGIO = APPOGGIO & AdattaLunghezzaNumero(((Imponibile3) / Quantita3) * 10000000, 17) '   TRF-MCI-COSTO-UNIT
                    APPOGGIO = APPOGGIO & AdattaLunghezzaNumero((Imponibile3) * 10000000, 18) '   TRF-MCI-COSTO-COMPLESS
                    APPOGGIO = APPOGGIO & AdattaLunghezzaNumero("0", 12) '   TRF-MCI-QTALAV
                    APPOGGIO = APPOGGIO & AdattaLunghezzaNumero("0", 10) '   TRF-MCI-TOTORE
                    APPOGGIO = APPOGGIO & AdattaLunghezzaNumero("0", 17) '   TRF-MCI-COSORA-CDC
                    APPOGGIO = APPOGGIO & AdattaLunghezzaNumero("0", 17) '   TRF-MCI-COSORA-VDS
                    APPOGGIO = APPOGGIO & AdattaLunghezza("", 18) '   TRF-MCI-DESCRIAGG
                    APPOGGIO = APPOGGIO & AdattaLunghezzaNumero("0", 3) '   TRF-MCI-DEP-PROD
                    APPOGGIO = APPOGGIO & AdattaLunghezzaNumero("0", 3) '   TRF-MCI-SEZ-PROD
                    APPOGGIO = APPOGGIO & AdattaLunghezzaNumero("0", 6) '   TRF-MCI-NUM-PROD
                    APPOGGIO = APPOGGIO & AdattaLunghezzaNumero("0", 3) '   TRF-MCI-LINEA
                    APPOGGIO = APPOGGIO & AdattaLunghezzaNumero("0", 1) '   TRF-MCI-TIPOCLFO                 
                    FineFor = FineFor - 1
                End If

                For X = 1 To FineFor
                    APPOGGIO = APPOGGIO & AdattaLunghezzaNumero("0", 3) 'TRF_CAUSALE                     
                    APPOGGIO = APPOGGIO & AdattaLunghezzaNumero("0", 8) 'TRF-MCI-CDC-CONTO        

                    APPOGGIO = APPOGGIO & AdattaLunghezzaNumero("0", 8) '  TRF-MCI-VDS-VDR




                    APPOGGIO = APPOGGIO & "00000000" '  TRF-MCI-DADATA
                    APPOGGIO = APPOGGIO & "00000000" '   TRF-MCI-ADATA
                    APPOGGIO = APPOGGIO & "000000" '  TRF-MCI-COMMESSA
                    APPOGGIO = APPOGGIO & "000" '  TRF-MCI-SCOMMESSA 
                    APPOGGIO = APPOGGIO & AdattaLunghezza("", 20) '  TRF-MCI-CODART

                    APPOGGIO = APPOGGIO & " "

                    APPOGGIO = APPOGGIO & AdattaLunghezzaNumero(0, 12) '  TRF-MCI-QUANTITA

                    APPOGGIO = APPOGGIO & AdattaLunghezzaNumero(0, 17) '   TRF-MCI-COSTO-UNIT
                    APPOGGIO = APPOGGIO & AdattaLunghezzaNumero(0, 18) '   TRF-MCI-COSTO-COMPLESS
                    APPOGGIO = APPOGGIO & AdattaLunghezzaNumero("0", 12) '   TRF-MCI-QTALAV
                    APPOGGIO = APPOGGIO & AdattaLunghezzaNumero("0", 10) '   TRF-MCI-TOTORE
                    APPOGGIO = APPOGGIO & AdattaLunghezzaNumero("0", 17) '   TRF-MCI-COSORA-CDC
                    APPOGGIO = APPOGGIO & AdattaLunghezzaNumero("0", 17) '   TRF-MCI-COSORA-VDS
                    APPOGGIO = APPOGGIO & AdattaLunghezza("0", 18) '   TRF-MCI-DESCRIAGG
                    APPOGGIO = APPOGGIO & AdattaLunghezzaNumero("0", 3) '   TRF-MCI-DEP-PROD
                    APPOGGIO = APPOGGIO & AdattaLunghezzaNumero("0", 3) '   TRF-MCI-SEZ-PROD
                    APPOGGIO = APPOGGIO & AdattaLunghezzaNumero("0", 6) '   TRF-MCI-NUM-PROD
                    APPOGGIO = APPOGGIO & AdattaLunghezzaNumero("0", 3) '   TRF-MCI-LINEA
                    APPOGGIO = APPOGGIO & AdattaLunghezzaNumero("0", 1) '   TRF-MCI-TIPOCLFO  


                Next
                APPOGGIO = APPOGGIO & Space(39)
                APPOGGIO = APPOGGIO & AdattaLunghezzaNumero("0", 2) '     TRF-MCI-IND-RIGA
                APPOGGIO = APPOGGIO & Space(2912)
                APPOGGIO = APPOGGIO & Space(2)


                '  TRF-MCI-DADATA


                '    TRF-MCI-VDS-VDR
                tw.WriteLine(APPOGGIO)
            End If



            REM **********************************CHIARA LUCE
            If Session("DC_OSPITE").ToString.ToUpper.IndexOf("CHIA") >= 0 Then
                Dim FineFor As Integer = 20
                APPOGGIO = ""
                APPOGGIO = APPOGGIO & AdattaLunghezzaNumero("00068", 5) ' TRF_DITTA 
                APPOGGIO = APPOGGIO & "3"
                APPOGGIO = APPOGGIO & "2"
                If CausaleContabile.TipoDocumento = "NC" Then
                    APPOGGIO = APPOGGIO & AdattaLunghezzaNumero("002", 3) 'TRF_CAUSALE 
                Else
                    APPOGGIO = APPOGGIO & AdattaLunghezzaNumero("001", 3) 'TRF_CAUSALE 
                End If

                APPOGGIO = APPOGGIO & AdattaLunghezzaNumero("01010003", 8) 'TRF-MCI-CDC-CONTO        


                APPOGGIO = APPOGGIO & AdattaLunghezzaNumero("58100005", 8) 'TRF-MCI-CDC-CONTO        


                Dim AnnoData As Integer = campodbN(myPOSTreader.Item("ANNOCOMPETENZA"))
                Dim MeseData As Integer = campodbN(myPOSTreader.Item("MESECOMPETENZA"))

                MeseData = MeseData + 1


                APPOGGIO = APPOGGIO & Format(DateSerial(AnnoData, MeseData, 1), "ddMMyyyy")
                APPOGGIO = APPOGGIO & Format(DateSerial(AnnoData, MeseData, GiorniMese(MeseData, AnnoData)), "ddMMyyyy")
                APPOGGIO = APPOGGIO & AdattaLunghezzaNumero("0", 6)
                APPOGGIO = APPOGGIO & AdattaLunghezzaNumero("0", 3)
                APPOGGIO = APPOGGIO & AdattaLunghezza("", 20)

                If CausaleContabile.TipoDocumento = "NC" Then
                    APPOGGIO = APPOGGIO & "A"
                Else
                    APPOGGIO = APPOGGIO & "D"
                End If

                APPOGGIO = APPOGGIO & AdattaLunghezzaNumero(Quantita1 * 10000, 12) '  TRF-MCI-QUANTITA

                APPOGGIO = APPOGGIO & AdattaLunghezzaNumero(((Imponibile1) / Quantita1) * 10000000, 17) '   TRF-MCI-COSTO-UNIT
                APPOGGIO = APPOGGIO & AdattaLunghezzaNumero((Imponibile1) * 10000000, 18) '   TRF-MCI-COSTO-COMPLESS
                APPOGGIO = APPOGGIO & AdattaLunghezzaNumero("0", 12) '   TRF-MCI-QTALAV
                APPOGGIO = APPOGGIO & AdattaLunghezzaNumero("0", 10) '   TRF-MCI-TOTORE
                APPOGGIO = APPOGGIO & AdattaLunghezzaNumero("0", 17) '   TRF-MCI-COSORA-CDC
                APPOGGIO = APPOGGIO & AdattaLunghezzaNumero("0", 17) '   TRF-MCI-COSORA-VDS
                APPOGGIO = APPOGGIO & AdattaLunghezza("", 18) '   TRF-MCI-DESCRIAGG
                APPOGGIO = APPOGGIO & AdattaLunghezzaNumero("0", 3) '   TRF-MCI-DEP-PROD
                APPOGGIO = APPOGGIO & AdattaLunghezzaNumero("0", 3) '   TRF-MCI-SEZ-PROD
                APPOGGIO = APPOGGIO & AdattaLunghezzaNumero("0", 6) '   TRF-MCI-NUM-PROD
                APPOGGIO = APPOGGIO & AdattaLunghezzaNumero("0", 3) '   TRF-MCI-LINEA
                APPOGGIO = APPOGGIO & AdattaLunghezzaNumero("0", 1) '   TRF-MCI-TIPOCLFO                 
                FineFor = FineFor - 1

                For X = 1 To 19
                    APPOGGIO = APPOGGIO & AdattaLunghezzaNumero("0", 3) 'TRF_CAUSALE                     
                    APPOGGIO = APPOGGIO & AdattaLunghezzaNumero("0", 8) 'TRF-MCI-CDC-CONTO        

                    APPOGGIO = APPOGGIO & AdattaLunghezzaNumero("0", 8) '  TRF-MCI-VDS-VDR




                    APPOGGIO = APPOGGIO & "00000000" '  TRF-MCI-DADATA
                    APPOGGIO = APPOGGIO & "00000000" '   TRF-MCI-ADATA
                    APPOGGIO = APPOGGIO & "000000" '  TRF-MCI-COMMESSA
                    APPOGGIO = APPOGGIO & "000" '  TRF-MCI-SCOMMESSA 
                    APPOGGIO = APPOGGIO & AdattaLunghezza("", 20) '  TRF-MCI-CODART

                    APPOGGIO = APPOGGIO & " "

                    APPOGGIO = APPOGGIO & AdattaLunghezzaNumero(0, 12) '  TRF-MCI-QUANTITA

                    APPOGGIO = APPOGGIO & AdattaLunghezzaNumero(0, 17) '   TRF-MCI-COSTO-UNIT
                    APPOGGIO = APPOGGIO & AdattaLunghezzaNumero(0, 18) '   TRF-MCI-COSTO-COMPLESS
                    APPOGGIO = APPOGGIO & AdattaLunghezzaNumero("0", 12) '   TRF-MCI-QTALAV
                    APPOGGIO = APPOGGIO & AdattaLunghezzaNumero("0", 10) '   TRF-MCI-TOTORE
                    APPOGGIO = APPOGGIO & AdattaLunghezzaNumero("0", 17) '   TRF-MCI-COSORA-CDC
                    APPOGGIO = APPOGGIO & AdattaLunghezzaNumero("0", 17) '   TRF-MCI-COSORA-VDS
                    APPOGGIO = APPOGGIO & AdattaLunghezza("0", 18) '   TRF-MCI-DESCRIAGG
                    APPOGGIO = APPOGGIO & AdattaLunghezzaNumero("0", 3) '   TRF-MCI-DEP-PROD
                    APPOGGIO = APPOGGIO & AdattaLunghezzaNumero("0", 3) '   TRF-MCI-SEZ-PROD
                    APPOGGIO = APPOGGIO & AdattaLunghezzaNumero("0", 6) '   TRF-MCI-NUM-PROD
                    APPOGGIO = APPOGGIO & AdattaLunghezzaNumero("0", 3) '   TRF-MCI-LINEA
                    APPOGGIO = APPOGGIO & AdattaLunghezzaNumero("0", 1) '   TRF-MCI-TIPOCLFO  


                Next

                APPOGGIO = APPOGGIO & AdattaLunghezzaNumero("1", 2) '     TRF-MCI-IND-RIGA
                APPOGGIO = APPOGGIO & AdattaLunghezzaNumero("0", 38)
                APPOGGIO = APPOGGIO & Space(2912)
                APPOGGIO = APPOGGIO & Space(2)


                '  TRF-MCI-DADATA


                '    TRF-MCI-VDS-VDR
                tw.WriteLine(APPOGGIO)
            End If



        Loop
        myPOSTreader.Close()
        cn.Close()



        tw.Close()

        Try
            Response.Clear()
            Response.Buffer = True
            Response.ContentType = "text/plain"
            Response.AppendHeader("content-disposition", "attachment;filename=TRAF2000.txt")
            Response.WriteFile(NomeFile)
            Response.Flush()
            Response.End()
        Finally
            Kill(NomeFile)
        End Try
        
    End Sub

    Private Function CentriCostoLibera(APPOGGIO As String, tw As TextWriter, myPOSTreader As OleDbDataReader, CausaleContabile As Cls_CausaleContabile, CONTO2 As String, CONTO3 As String, Imponibile1 As Double, Imponibile2 As Double, Imponibile3 As Double, Quantita1 As Double, Quantita2 As Double, Quantita3 As Double) As String
        Dim FineFor As Integer = 20
        APPOGGIO = ""
        APPOGGIO = APPOGGIO & AdattaLunghezzaNumero("4", 5) ' TRF_DITTA 
        APPOGGIO = APPOGGIO & "3"
        APPOGGIO = APPOGGIO & "2"
        If Imponibile1 > 0 Then
            If CausaleContabile.TipoDocumento = "NC" Then
                APPOGGIO = APPOGGIO & AdattaLunghezzaNumero("002", 3) 'TRF_CAUSALE 
            Else
                APPOGGIO = APPOGGIO & AdattaLunghezzaNumero("001", 3) 'TRF_CAUSALE 
            End If

            If campodb(myPOSTreader.Item("CENTROSERVIZIO")) = "0005" Then
                APPOGGIO = APPOGGIO & AdattaLunghezzaNumero("50000010", 8) 'TRF-MCI-CDC-CONTO        'SANTA MARIA
            End If
                
            If campodb(myPOSTreader.Item("CENTROSERVIZIO")) = "0001" Then
                APPOGGIO = APPOGGIO & AdattaLunghezzaNumero("50000010", 8) 'TRF-MCI-CDC-CONTO        'ULIVETO
            End If
                
            If campodb(myPOSTreader.Item("CENTROSERVIZIO")) = "0003" Then
                APPOGGIO = APPOGGIO & AdattaLunghezzaNumero("50000020", 8) 'TRF-MCI-CDC-CONTO        'MINI ALLOGGI
            End If

            If campodb(myPOSTreader.Item("CENTROSERVIZIO")) = "0005" Then
                APPOGGIO = APPOGGIO & AdattaLunghezzaNumero("58100602", 8) ' TRF-MCI-VDS-VDR        'SANTA MARIA
            End If
                
            If campodb(myPOSTreader.Item("CENTROSERVIZIO")) = "0001" Then
                APPOGGIO = APPOGGIO & AdattaLunghezzaNumero("58100601", 8) ' TRF-MCI-VDS-VDR        'ULIVETO
            End If
                
            If campodb(myPOSTreader.Item("CENTROSERVIZIO")) = "0003" Then
                APPOGGIO = APPOGGIO & AdattaLunghezzaNumero("58100600", 8) ' TRF-MCI-VDS-VDR        'MINI ALLOGGI
            End If




            Dim AnnoData As Integer = campodbN(myPOSTreader.Item("ANNOCOMPETENZA"))
            Dim MeseData As Integer = campodbN(myPOSTreader.Item("MESECOMPETENZA"))

            APPOGGIO = APPOGGIO & Format(DateSerial(AnnoData, MeseData, 1), "ddMMyyyy")
            APPOGGIO = APPOGGIO & Format(DateSerial(AnnoData, MeseData, GiorniMese(MeseData, AnnoData)), "ddMMyyyy")
            APPOGGIO = APPOGGIO & AdattaLunghezzaNumero("0", 6)
            APPOGGIO = APPOGGIO & AdattaLunghezzaNumero("0", 3)
            APPOGGIO = APPOGGIO & AdattaLunghezza("", 20)

            If CausaleContabile.TipoDocumento = "NC" Then
                APPOGGIO = APPOGGIO & "A"
            Else
                APPOGGIO = APPOGGIO & "D"
            End If

            APPOGGIO = APPOGGIO & AdattaLunghezzaNumero(Quantita1 * 10000, 12) '  TRF-MCI-QUANTITA

            APPOGGIO = APPOGGIO & AdattaLunghezzaNumero(((Imponibile1) / Quantita1) * 10000000, 17) '   TRF-MCI-COSTO-UNIT
            APPOGGIO = APPOGGIO & AdattaLunghezzaNumero((Imponibile1) * 10000000, 18) '   TRF-MCI-COSTO-COMPLESS
            APPOGGIO = APPOGGIO & AdattaLunghezzaNumero("0", 12) '   TRF-MCI-QTALAV
            APPOGGIO = APPOGGIO & AdattaLunghezzaNumero("0", 10) '   TRF-MCI-TOTORE
            APPOGGIO = APPOGGIO & AdattaLunghezzaNumero("0", 17) '   TRF-MCI-COSORA-CDC
            APPOGGIO = APPOGGIO & AdattaLunghezzaNumero("0", 17) '   TRF-MCI-COSORA-VDS
            APPOGGIO = APPOGGIO & AdattaLunghezza("", 18) '   TRF-MCI-DESCRIAGG
            APPOGGIO = APPOGGIO & AdattaLunghezzaNumero("0", 3) '   TRF-MCI-DEP-PROD
            APPOGGIO = APPOGGIO & AdattaLunghezzaNumero("0", 3) '   TRF-MCI-SEZ-PROD
            APPOGGIO = APPOGGIO & AdattaLunghezzaNumero("0", 6) '   TRF-MCI-NUM-PROD
            APPOGGIO = APPOGGIO & AdattaLunghezzaNumero("0", 3) '   TRF-MCI-LINEA
            APPOGGIO = APPOGGIO & AdattaLunghezzaNumero("0", 1) '   TRF-MCI-TIPOCLFO                 
            FineFor = FineFor - 1
        End If
        If Imponibile2 > 0 Then
            If CausaleContabile.TipoDocumento = "NC" Then
                APPOGGIO = APPOGGIO & AdattaLunghezzaNumero("002", 3) 'TRF_CAUSALE 
            Else
                APPOGGIO = APPOGGIO & AdattaLunghezzaNumero("001", 3) 'TRF_CAUSALE 
            End If

            If campodb(myPOSTreader.Item("CENTROSERVIZIO")) = "0005" Then
                APPOGGIO = APPOGGIO & AdattaLunghezzaNumero("50000010", 8) 'TRF-MCI-CDC-CONTO        'SANTA MARIA
            End If
                
            If campodb(myPOSTreader.Item("CENTROSERVIZIO")) = "0001" Then
                APPOGGIO = APPOGGIO & AdattaLunghezzaNumero("50000090", 8) 'TRF-MCI-CDC-CONTO        'ULIVETO
            End If
                
            If campodb(myPOSTreader.Item("CENTROSERVIZIO")) = "0003" Then
                APPOGGIO = APPOGGIO & AdattaLunghezzaNumero("50000020", 8) 'TRF-MCI-CDC-CONTO        'MINI ALLOGGI
            End If

            If campodb(myPOSTreader.Item("CENTROSERVIZIO")) = "0005" Then
                APPOGGIO = APPOGGIO & AdattaLunghezzaNumero("58100602", 8) ' TRF-MCI-VDS-VDR        'SANTA MARIA
            End If
                
            If campodb(myPOSTreader.Item("CENTROSERVIZIO")) = "0001" Then
                APPOGGIO = APPOGGIO & AdattaLunghezzaNumero("58100601", 8) ' TRF-MCI-VDS-VDR        'ULIVETO
            End If
                
            If campodb(myPOSTreader.Item("CENTROSERVIZIO")) = "0003" Then
                APPOGGIO = APPOGGIO & AdattaLunghezzaNumero("58100600", 8) ' TRF-MCI-VDS-VDR        'MINI ALLOGGI
            End If


            Dim AnnoData As Integer = campodbN(myPOSTreader.Item("ANNOCOMPETENZA"))
            Dim MeseData As Integer = campodbN(myPOSTreader.Item("MESECOMPETENZA"))

            APPOGGIO = APPOGGIO & Format(DateSerial(AnnoData, MeseData, 1), "ddMMyyyy")
            APPOGGIO = APPOGGIO & Format(DateSerial(AnnoData, MeseData, GiorniMese(MeseData, AnnoData)), "ddMMyyyy")
            APPOGGIO = APPOGGIO & AdattaLunghezzaNumero("0", 6)
            APPOGGIO = APPOGGIO & AdattaLunghezzaNumero("0", 3)
            APPOGGIO = APPOGGIO & AdattaLunghezza("", 20)

            If CausaleContabile.TipoDocumento = "NC" Then
                APPOGGIO = APPOGGIO & "A"
            Else
                APPOGGIO = APPOGGIO & "D"
            End If

            APPOGGIO = APPOGGIO & AdattaLunghezzaNumero(Quantita2 * 10000, 12) '  TRF-MCI-QUANTITA

            APPOGGIO = APPOGGIO & AdattaLunghezzaNumero(((Imponibile2) / Quantita2) * 10000000, 17) '   TRF-MCI-COSTO-UNIT
            APPOGGIO = APPOGGIO & AdattaLunghezzaNumero((Imponibile2) * 10000000, 18) '   TRF-MCI-COSTO-COMPLESS
            APPOGGIO = APPOGGIO & AdattaLunghezzaNumero("0", 12) '   TRF-MCI-QTALAV
            APPOGGIO = APPOGGIO & AdattaLunghezzaNumero("0", 10) '   TRF-MCI-TOTORE
            APPOGGIO = APPOGGIO & AdattaLunghezzaNumero("0", 17) '   TRF-MCI-COSORA-CDC
            APPOGGIO = APPOGGIO & AdattaLunghezzaNumero("0", 17) '   TRF-MCI-COSORA-VDS
            APPOGGIO = APPOGGIO & AdattaLunghezza("", 18) '   TRF-MCI-DESCRIAGG
            APPOGGIO = APPOGGIO & AdattaLunghezzaNumero("0", 3) '   TRF-MCI-DEP-PROD
            APPOGGIO = APPOGGIO & AdattaLunghezzaNumero("0", 3) '   TRF-MCI-SEZ-PROD
            APPOGGIO = APPOGGIO & AdattaLunghezzaNumero("0", 6) '   TRF-MCI-NUM-PROD
            APPOGGIO = APPOGGIO & AdattaLunghezzaNumero("0", 3) '   TRF-MCI-LINEA
            APPOGGIO = APPOGGIO & AdattaLunghezzaNumero("0", 1) '   TRF-MCI-TIPOCLFO                 
            FineFor = FineFor - 1
        End If
        If Imponibile3 > 0 Then
            If CausaleContabile.TipoDocumento = "NC" Then
                APPOGGIO = APPOGGIO & AdattaLunghezzaNumero("002", 3) 'TRF_CAUSALE 
            Else
                APPOGGIO = APPOGGIO & AdattaLunghezzaNumero("001", 3) 'TRF_CAUSALE 
            End If

            If campodb(myPOSTreader.Item("CENTROSERVIZIO")) = "0005" Then
                APPOGGIO = APPOGGIO & AdattaLunghezzaNumero("50000010", 8) 'TRF-MCI-CDC-CONTO        'SANTA MARIA
            End If
                
            If campodb(myPOSTreader.Item("CENTROSERVIZIO")) = "0001" Then
                APPOGGIO = APPOGGIO & AdattaLunghezzaNumero("50000090", 8) 'TRF-MCI-CDC-CONTO        'ULIVETO
            End If
                
            If campodb(myPOSTreader.Item("CENTROSERVIZIO")) = "0003" Then
                APPOGGIO = APPOGGIO & AdattaLunghezzaNumero("50000020", 8) 'TRF-MCI-CDC-CONTO        'MINI ALLOGGI
            End If

            If campodb(myPOSTreader.Item("CENTROSERVIZIO")) = "0005" Then
                APPOGGIO = APPOGGIO & AdattaLunghezzaNumero("58100602", 8) ' TRF-MCI-VDS-VDR        'SANTA MARIA
            End If
                
            If campodb(myPOSTreader.Item("CENTROSERVIZIO")) = "0001" Then
                APPOGGIO = APPOGGIO & AdattaLunghezzaNumero("58100601", 8) ' TRF-MCI-VDS-VDR        'ULIVETO
            End If
                
            If campodb(myPOSTreader.Item("CENTROSERVIZIO")) = "0003" Then
                APPOGGIO = APPOGGIO & AdattaLunghezzaNumero("58100600", 8) ' TRF-MCI-VDS-VDR        'MINI ALLOGGI
            End If



            Dim AnnoData As Integer = campodbN(myPOSTreader.Item("ANNOCOMPETENZA"))
            Dim MeseData As Integer = campodbN(myPOSTreader.Item("MESECOMPETENZA"))

            APPOGGIO = APPOGGIO & Format(DateSerial(AnnoData, MeseData, 1), "ddMMyyyy")
            APPOGGIO = APPOGGIO & Format(DateSerial(AnnoData, MeseData, GiorniMese(MeseData, AnnoData)), "ddMMyyyy")
            APPOGGIO = APPOGGIO & AdattaLunghezzaNumero("0", 6)
            APPOGGIO = APPOGGIO & AdattaLunghezzaNumero("0", 3)
            APPOGGIO = APPOGGIO & AdattaLunghezza("", 20)

            If CausaleContabile.TipoDocumento = "NC" Then
                APPOGGIO = APPOGGIO & "A"
            Else
                APPOGGIO = APPOGGIO & "D"
            End If

            APPOGGIO = APPOGGIO & AdattaLunghezzaNumero(Quantita3 * 10000, 12) '  TRF-MCI-QUANTITA

            APPOGGIO = APPOGGIO & AdattaLunghezzaNumero(((Imponibile3) / Quantita3) * 10000000, 17) '   TRF-MCI-COSTO-UNIT
            APPOGGIO = APPOGGIO & AdattaLunghezzaNumero((Imponibile3) * 10000000, 18) '   TRF-MCI-COSTO-COMPLESS
            APPOGGIO = APPOGGIO & AdattaLunghezzaNumero("0", 12) '   TRF-MCI-QTALAV
            APPOGGIO = APPOGGIO & AdattaLunghezzaNumero("0", 10) '   TRF-MCI-TOTORE
            APPOGGIO = APPOGGIO & AdattaLunghezzaNumero("0", 17) '   TRF-MCI-COSORA-CDC
            APPOGGIO = APPOGGIO & AdattaLunghezzaNumero("0", 17) '   TRF-MCI-COSORA-VDS
            APPOGGIO = APPOGGIO & AdattaLunghezza("", 18) '   TRF-MCI-DESCRIAGG
            APPOGGIO = APPOGGIO & AdattaLunghezzaNumero("0", 3) '   TRF-MCI-DEP-PROD
            APPOGGIO = APPOGGIO & AdattaLunghezzaNumero("0", 3) '   TRF-MCI-SEZ-PROD
            APPOGGIO = APPOGGIO & AdattaLunghezzaNumero("0", 6) '   TRF-MCI-NUM-PROD
            APPOGGIO = APPOGGIO & AdattaLunghezzaNumero("0", 3) '   TRF-MCI-LINEA
            APPOGGIO = APPOGGIO & AdattaLunghezzaNumero("0", 1) '   TRF-MCI-TIPOCLFO                 
            FineFor = FineFor - 1
        End If

        For X = 1 To FineFor
            APPOGGIO = APPOGGIO & AdattaLunghezzaNumero("0", 3) 'TRF_CAUSALE                     
            APPOGGIO = APPOGGIO & AdattaLunghezzaNumero("0", 8) 'TRF-MCI-CDC-CONTO        

            APPOGGIO = APPOGGIO & AdattaLunghezzaNumero("0", 8) '  TRF-MCI-VDS-VDR




            APPOGGIO = APPOGGIO & "00000000" '  TRF-MCI-DADATA
            APPOGGIO = APPOGGIO & "00000000" '   TRF-MCI-ADATA
            APPOGGIO = APPOGGIO & "000000" '  TRF-MCI-COMMESSA
            APPOGGIO = APPOGGIO & "000" '  TRF-MCI-SCOMMESSA 
            APPOGGIO = APPOGGIO & AdattaLunghezza("", 20) '  TRF-MCI-CODART

            APPOGGIO = APPOGGIO & " "

            APPOGGIO = APPOGGIO & AdattaLunghezzaNumero(0, 12) '  TRF-MCI-QUANTITA

            APPOGGIO = APPOGGIO & AdattaLunghezzaNumero(0, 17) '   TRF-MCI-COSTO-UNIT
            APPOGGIO = APPOGGIO & AdattaLunghezzaNumero(0, 18) '   TRF-MCI-COSTO-COMPLESS
            APPOGGIO = APPOGGIO & AdattaLunghezzaNumero("0", 12) '   TRF-MCI-QTALAV
            APPOGGIO = APPOGGIO & AdattaLunghezzaNumero("0", 10) '   TRF-MCI-TOTORE
            APPOGGIO = APPOGGIO & AdattaLunghezzaNumero("0", 17) '   TRF-MCI-COSORA-CDC
            APPOGGIO = APPOGGIO & AdattaLunghezzaNumero("0", 17) '   TRF-MCI-COSORA-VDS
            APPOGGIO = APPOGGIO & AdattaLunghezza("0", 18) '   TRF-MCI-DESCRIAGG
            APPOGGIO = APPOGGIO & AdattaLunghezzaNumero("0", 3) '   TRF-MCI-DEP-PROD
            APPOGGIO = APPOGGIO & AdattaLunghezzaNumero("0", 3) '   TRF-MCI-SEZ-PROD
            APPOGGIO = APPOGGIO & AdattaLunghezzaNumero("0", 6) '   TRF-MCI-NUM-PROD
            APPOGGIO = APPOGGIO & AdattaLunghezzaNumero("0", 3) '   TRF-MCI-LINEA
            APPOGGIO = APPOGGIO & AdattaLunghezzaNumero("0", 1) '   TRF-MCI-TIPOCLFO  


        Next
        APPOGGIO = APPOGGIO & Space(39)
        APPOGGIO = APPOGGIO & AdattaLunghezzaNumero("0", 2) '     TRF-MCI-IND-RIGA
        APPOGGIO = APPOGGIO & Space(2912)
        APPOGGIO = APPOGGIO & Space(2)


        '  TRF-MCI-DADATA


        '    TRF-MCI-VDS-VDR
        tw.WriteLine(APPOGGIO)

        Return APPOGGIO
    End Function

    Sub Export_Gamma()

        Dim cn As OleDbConnection

        Dim MeseContr As Long
        Dim MySql As String
        Dim ProgressivoAssoluto As Integer = 0


        MeseContr = DD_Mese.SelectedValue


        MySql = "SELECT * " & _
                " FROM MovimentiContabiliTesta " & _
                " WHERE MovimentiContabiliTesta.AnnoCompetenza = " & Txt_Anno.Text & " " & _
                " AND MovimentiContabiliTesta.MeseCompetenza = " & MeseContr & " " & _
                " AND AnnoProtocollo = " & Txt_AnnoRif.Text

        If DD_Registro.SelectedValue <> "" Then
            MySql = MySql & " AND MovimentiContabiliTesta.RegistroIva = " & DD_Registro.SelectedValue
        End If
        If Tab_PeriodoData.ActiveTabIndex = 1 And IsDate(Txt_DataAl1.Text) And IsDate(Txt_DataDal1.Text) Then
            MySql = MySql & " AND (DataRegistrazione >= ? And DataRegistrazione <= ?) "
        Else
            MySql = MySql & " And MovimentiContabiliTesta.AnnoCompetenza = " & Txt_Anno.Text & " AND MovimentiContabiliTesta.MeseCompetenza = " & MeseContr & " "
        End If

        MySql = MySql & " AND (NumeroProtocollo >= " & Txt_DalDocumento.Text & " And NumeroProtocollo <= " & Txt_AlDocumento.Text & ") "
        
        cn = New Data.OleDb.OleDbConnection(Session("DC_GENERALE"))

        cn.Open()
        Dim cmd As New OleDbCommand()
        cmd.CommandText = MySql & " Order by NumeroProtocollo"
        cmd.Connection = cn
        If Tab_PeriodoData.ActiveTabIndex = 1 And IsDate(Txt_DataAl1.Text) And IsDate(Txt_DataDal1.Text) Then
            cmd.Parameters.AddWithValue("@DataDal", Txt_DataDal1.Text)
            cmd.Parameters.AddWithValue("@DataDal", Txt_DataAl1.Text)
        End If


        Dim CodiceInstallazione As Integer
        Dim CodiceDitta As Integer
        Dim ProgressivoTesta As Integer
        Dim RegistroIva As Integer
        Dim APPOGGIO As String

        CodiceInstallazione = 1
        CodiceDitta = 1

        RegistroIva = DD_Registro.SelectedValue
        Dim NomeFile As String
        'Dim NomeFileRiga As String
        NomeFile = HostingEnvironment.ApplicationPhysicalPath() & "\Public\Esportazione_" & Session("NomeEPersonam") & "_" & RegistroIva & "_" & Format(Now, "yyyyMMddHHmm") & ".Txt"
        'NomeFileRiga = HostingEnvironment.ApplicationPhysicalPath() & "\Riga_" & CodiceDitta & "_" & Format(Now, "yyyyMMddHHmm") & ".Txt"

        Dim tw As System.IO.TextWriter = System.IO.File.CreateText(NomeFile)
        'Dim twR As System.IO.TextWriter = System.IO.File.CreateText(NomeFileRiga)



        ProgressivoTesta = 0

        Dim myPOSTreader As OleDbDataReader = cmd.ExecuteReader()
        Do While myPOSTreader.Read
            ProgressivoTesta = ProgressivoTesta + 1


            APPOGGIO = ""
            Dim CausaleContabile As New Cls_CausaleContabile

            CausaleContabile.Codice = campodb(myPOSTreader.Item("CAUSALECONTABILE"))
            CausaleContabile.Leggi(Session("DC_TABELLE"), CausaleContabile.Codice)





            Dim Cliente As String = ""

            Dim Tipo As String = campodb(myPOSTreader.Item("Tipologia"))
            Dim CodiceOspite As Integer = 0
            Dim CodiceParente As Integer = 0

            Dim CodiceProvincia As String = ""
            Dim CodiceComune As String = ""
            Dim CodiceRegione As String = ""


            Dim cmdRd As New OleDbCommand()
            cmdRd.CommandText = "Select * From MovimentiContabiliRiga Where Numero = ?  And Tipo ='CF'   "
            cmdRd.Connection = cn
            cmdRd.Parameters.AddWithValue("@Numero", campodbN(myPOSTreader.Item("NumeroRegistrazione")))
            Dim MyReadSC As OleDbDataReader = cmdRd.ExecuteReader()
            If MyReadSC.Read Then
                Cliente = campodb(MyReadSC.Item("MastroPartita")) & "." & campodb(MyReadSC.Item("ContoPartita")) & "." & campodb(MyReadSC.Item("SottocontoPartita"))
                If Mid(Tipo & Space(10), 1, 1) = " " Then
                    CodiceOspite = Int(campodbN(MyReadSC.Item("SottocontoPartita")) / 100)
                    CodiceParente = campodbN(MyReadSC.Item("SottocontoPartita")) - (CodiceOspite * 100)

                    If CodiceParente = 0 And CodiceOspite > 0 Then
                        Tipo = "O" & Space(10)
                    End If
                    If CodiceParente > 0 And CodiceOspite > 0 Then
                        Tipo = "P" & Space(10)
                    End If
                End If
                If Mid(Tipo & Space(10), 1, 1) = "C" Then
                    CodiceProvincia = Mid(Tipo & Space(10), 2, 3)
                    CodiceComune = Mid(Tipo & Space(10), 5, 3)
                End If
                If Mid(Tipo & Space(10), 1, 1) = "J" Then
                    CodiceProvincia = Mid(Tipo & Space(10), 2, 3)
                    CodiceComune = Mid(Tipo & Space(10), 5, 3)
                End If
                If Mid(Tipo & Space(10), 1, 1) = "R" Then
                    CodiceRegione = Mid(Tipo & Space(10), 2, 4)
                End If

            End If
            MyReadSC.Close()


            Dim DO11_RAGIONESOCIALE As String = ""
            Dim DO11_PIVA As String = ""
            Dim DO11_CF As String = ""
            Dim DO11_INDIRIZZO As String = ""
            Dim DO11_CAP As String = ""
            Dim DO11_CITTA_NOME As String = ""
            Dim DO11_CITTA_ISTAT As String = ""
            Dim DO11_PROVINCIA As String = ""
            Dim DO11_CONDPAG_CODICE_SEN As String = ""
            Dim DO11_CONDPAG_NOME_SEN As String = ""
            Dim DO11_BANCA_CIN As String = ""
            Dim DO11_BANCA_ABI As String = ""
            Dim DO11_BANCA_CAB As String = ""
            Dim DO11_BANCA_CONTO As String = ""
            Dim DO11_BANCA_IBAN As String = ""
            Dim DO11_BANCA_IDMANDATO As String = ""
            Dim DO11_BANCA_DATAMANDATO As String = ""
            Dim AddDescrizione As String

            AddDescrizione = ""
            If Mid(Tipo & Space(10), 1, 1) = "O" Then
                Dim Ospite As New ClsOspite

                Ospite.CodiceOspite = CodiceOspite
                Ospite.Leggi(Session("DC_OSPITE"), Ospite.CodiceOspite)
                DO11_RAGIONESOCIALE = Ospite.Nome
                DO11_PIVA = ""
                DO11_CF = Ospite.CODICEFISCALE
                DO11_INDIRIZZO = Ospite.RESIDENZAINDIRIZZO1
                DO11_CAP = Ospite.RESIDENZACAP1
                Dim DcCom As New ClsComune

                DcCom.Provincia = Ospite.RESIDENZAPROVINCIA1
                DcCom.Comune = Ospite.RESIDENZACOMUNE1
                DcCom.Leggi(Session("DC_OSPITE"))

                DO11_CITTA_NOME = DcCom.Descrizione
                DO11_CITTA_ISTAT = DcCom.Provincia & DcCom.Comune

                Dim DcProv As New ClsComune

                DcProv.Provincia = Ospite.RESIDENZAPROVINCIA1
                DcProv.Comune = ""
                DcProv.Leggi(Session("DC_OSPITE"))
                DO11_PROVINCIA = DcProv.CodificaProvincia


                Dim Kl As New Cls_DatiOspiteParenteCentroServizio

                Kl.CodiceOspite = CodiceOspite
                Kl.CodiceParente = 0
                Kl.CentroServizio = campodb(myPOSTreader.Item("CentroServizio"))
                Kl.Leggi(Session("DC_OSPITE"))
                If Kl.ModalitaPagamento <> "" Then
                    Ospite.MODALITAPAGAMENTO = Kl.ModalitaPagamento
                End If


                DO11_CONDPAG_CODICE_SEN = Ospite.MODALITAPAGAMENTO
                Dim MPAg As New ClsModalitaPagamento

                MPAg.Codice = Ospite.MODALITAPAGAMENTO
                MPAg.Leggi(Session("DC_OSPITE"))
                DO11_CONDPAG_NOME_SEN = MPAg.DESCRIZIONE


                Dim ModPag As New Cls_DatiPagamento

                ModPag.CodiceOspite = Ospite.CodiceOspite
                ModPag.CodiceParente = 0
                ModPag.LeggiUltimaDataData(Session("DC_OSPITE"), campodb(myPOSTreader.Item("DataDocumento")))

                DO11_BANCA_CIN = ModPag.Cin
                DO11_BANCA_ABI = ModPag.Abi
                DO11_BANCA_CAB = ModPag.Cab
                DO11_BANCA_CONTO = ModPag.CCBancario
                DO11_BANCA_IBAN = ModPag.CodiceInt & Format(Val(ModPag.NumeroControllo), "00") & ModPag.Cin & Format(Val(ModPag.Abi), "00000") & Format(Val(ModPag.Cab), "00000") & AdattaLunghezzaNumero(ModPag.CCBancario, 12)

                DO11_BANCA_IDMANDATO = ModPag.IdMandatoDebitore
                DO11_BANCA_DATAMANDATO = ModPag.DataMandatoDebitore
            End If

            If Mid(Tipo & Space(10), 1, 1) = "P" Then
                Dim Ospite As New Cls_Parenti

                Ospite.CodiceOspite = CodiceOspite
                Ospite.CodiceParente = CodiceParente
                Ospite.Leggi(Session("DC_OSPITE"), Ospite.CodiceOspite, Ospite.CodiceParente)
                DO11_RAGIONESOCIALE = Ospite.Nome
                DO11_PIVA = ""
                DO11_CF = Ospite.CODICEFISCALE
                DO11_INDIRIZZO = Ospite.RESIDENZAINDIRIZZO1
                DO11_CAP = Ospite.RESIDENZACAP1
                Dim DcCom As New ClsComune

                DcCom.Provincia = Ospite.RESIDENZAPROVINCIA1
                DcCom.Comune = Ospite.RESIDENZACOMUNE1
                DcCom.Leggi(Session("DC_OSPITE"))

                DO11_CITTA_NOME = DcCom.Descrizione
                DO11_CITTA_ISTAT = DcCom.Provincia & DcCom.Comune

                Dim DcProv As New ClsComune

                DcProv.Provincia = Ospite.RESIDENZAPROVINCIA1
                DcProv.Comune = ""
                DcProv.Leggi(Session("DC_OSPITE"))
                DO11_PROVINCIA = DcProv.CodificaProvincia
                DO11_CONDPAG_CODICE_SEN = Ospite.MODALITAPAGAMENTO

                Dim Kl As New Cls_DatiOspiteParenteCentroServizio

                Kl.CodiceOspite = CodiceOspite
                Kl.CodiceParente = CodiceParente
                Kl.CentroServizio = campodb(myPOSTreader.Item("CentroServizio"))
                Kl.Leggi(Session("DC_OSPITE"))
                If Kl.ModalitaPagamento <> "" Then
                    Ospite.MODALITAPAGAMENTO = Kl.ModalitaPagamento
                End If

                Dim MPAg As New ClsModalitaPagamento

                MPAg.Codice = Ospite.MODALITAPAGAMENTO
                MPAg.Leggi(Session("DC_OSPITE"))
                DO11_CONDPAG_NOME_SEN = MPAg.DESCRIZIONE

                Dim ModPag As New Cls_DatiPagamento

                ModPag.CodiceOspite = Ospite.CodiceOspite
                ModPag.CodiceParente = Ospite.CodiceParente
                ModPag.LeggiUltimaDataData(Session("DC_OSPITE"), campodb(myPOSTreader.Item("DataDocumento")))

                DO11_BANCA_CIN = ModPag.Cin
                DO11_BANCA_ABI = ModPag.Abi
                DO11_BANCA_CAB = ModPag.Cab
                DO11_BANCA_CONTO = ModPag.CCBancario
                DO11_BANCA_IBAN = ModPag.CodiceInt & Format(Val(ModPag.NumeroControllo), "00") & ModPag.Cin & Format(Val(ModPag.Abi), "00000") & Format(Val(ModPag.Cab), "00000") & AdattaLunghezzaNumero(ModPag.CCBancario, 12)

                DO11_BANCA_IDMANDATO = ModPag.IdMandatoDebitore
                DO11_BANCA_DATAMANDATO = ModPag.DataMandatoDebitore
            End If
            If Mid(Tipo & Space(10), 1, 1) = "C" Then
                Dim Ospite As New ClsComune

                Ospite.Provincia = CodiceProvincia
                Ospite.Comune = CodiceComune
                Ospite.Leggi(Session("DC_OSPITE"))
                DO11_RAGIONESOCIALE = Ospite.Descrizione
                DO11_PIVA = Ospite.PartitaIVA
                DO11_CF = Ospite.CodiceFiscale
                DO11_INDIRIZZO = Ospite.RESIDENZAINDIRIZZO1
                DO11_CAP = Ospite.RESIDENZACAP1
                Dim DcCom As New ClsComune

                DcCom.Provincia = Ospite.RESIDENZAPROVINCIA1
                DcCom.Comune = Ospite.RESIDENZACOMUNE1
                DcCom.Leggi(Session("DC_OSPITE"))

                DO11_CITTA_NOME = DcCom.Descrizione
                DO11_CITTA_ISTAT = DcCom.Provincia & DcCom.Comune

                Dim DcProv As New ClsComune

                DcProv.Provincia = Ospite.RESIDENZAPROVINCIA1
                DcProv.Comune = ""
                DcProv.Leggi(Session("DC_OSPITE"))
                DO11_PROVINCIA = DcProv.CodificaProvincia
                DO11_CONDPAG_CODICE_SEN = Ospite.ModalitaPagamento
                Dim MPAg As New ClsModalitaPagamento

                MPAg.Codice = Ospite.ModalitaPagamento
                MPAg.Leggi(Session("DC_OSPITE"))
                DO11_CONDPAG_NOME_SEN = MPAg.DESCRIZIONE

                Dim cnOspiti As OleDbConnection

                cnOspiti = New Data.OleDb.OleDbConnection(Session("DC_OSPITE"))

                cnOspiti.Open()

                Dim cmdC As New OleDbCommand()
                cmdC.CommandText = ("select * from NoteFatture where CodiceProvincia = ? And CodiceComune = ?")
                cmdC.Parameters.AddWithValue("@CodiceProvincia", CodiceProvincia)
                cmdC.Parameters.AddWithValue("@CodiceComune", CodiceComune)
                cmdC.Connection = cnOspiti
                Dim RdCom As OleDbDataReader = cmdC.ExecuteReader()
                If RdCom.Read Then
                    AddDescrizione = campodb(RdCom.Item("NoteUp"))
                End If
                RdCom.Close()
                cnOspiti.Close()
            End If
            If Mid(Tipo & Space(10), 1, 1) = "J" Then
                Dim Ospite As New ClsComune

                Ospite.Provincia = CodiceProvincia
                Ospite.Comune = CodiceComune
                Ospite.Leggi(Session("DC_OSPITE"))
                DO11_RAGIONESOCIALE = Ospite.Descrizione
                DO11_PIVA = Ospite.PartitaIVA
                DO11_CF = Ospite.CodiceFiscale
                DO11_INDIRIZZO = Ospite.RESIDENZAINDIRIZZO1
                DO11_CAP = Ospite.RESIDENZACAP1
                Dim DcCom As New ClsComune

                DcCom.Provincia = Ospite.RESIDENZAPROVINCIA1
                DcCom.Comune = Ospite.RESIDENZACOMUNE1
                DcCom.Leggi(Session("DC_OSPITE"))

                DO11_CITTA_NOME = DcCom.Descrizione
                DO11_CITTA_ISTAT = DcCom.Provincia & DcCom.Comune

                Dim DcProv As New ClsComune

                DcProv.Provincia = Ospite.RESIDENZAPROVINCIA1
                DcProv.Comune = ""
                DcProv.Leggi(Session("DC_OSPITE"))
                DO11_PROVINCIA = DcProv.CodificaProvincia
                DO11_CONDPAG_CODICE_SEN = Ospite.ModalitaPagamento
                Dim MPAg As New ClsModalitaPagamento

                MPAg.Codice = Ospite.ModalitaPagamento
                MPAg.Leggi(Session("DC_OSPITE"))
                DO11_CONDPAG_NOME_SEN = MPAg.DESCRIZIONE

                Dim cnOspiti As OleDbConnection

                cnOspiti = New Data.OleDb.OleDbConnection(Session("DC_OSPITE"))

                cnOspiti.Open()

                Dim cmdC As New OleDbCommand()
                cmdC.CommandText = ("select * from NoteFatture where CodiceProvincia = ? And CodiceComune = ?")
                cmdC.Parameters.AddWithValue("@CodiceProvincia", CodiceProvincia)
                cmdC.Parameters.AddWithValue("@CodiceComune", CodiceComune)
                cmdC.Connection = cnOspiti
                Dim RdCom As OleDbDataReader = cmdC.ExecuteReader()
                If RdCom.Read Then
                    AddDescrizione = campodb(RdCom.Item("NoteUp"))
                End If
                RdCom.Close()
                cnOspiti.Close()
            End If
            If Mid(Tipo & Space(10), 1, 1) = "R" Then
                Dim Ospite As New ClsUSL

                Ospite.CodiceRegione = CodiceRegione
                Ospite.Leggi(Session("DC_OSPITE"))
                DO11_RAGIONESOCIALE = Ospite.Nome
                DO11_PIVA = Ospite.PARTITAIVA
                DO11_CF = Ospite.CodiceFiscale
                DO11_INDIRIZZO = Ospite.RESIDENZAINDIRIZZO1
                DO11_CAP = Ospite.RESIDENZACAP1
                Dim DcCom As New ClsComune

                DcCom.Provincia = Ospite.RESIDENZAPROVINCIA1
                DcCom.Comune = Ospite.RESIDENZACOMUNE1
                DcCom.Leggi(Session("DC_OSPITE"))

                DO11_CITTA_NOME = DcCom.Descrizione
                DO11_CITTA_ISTAT = DcCom.Provincia & DcCom.Comune

                Dim DcProv As New ClsComune

                DcProv.Provincia = Ospite.RESIDENZAPROVINCIA1
                DcProv.Comune = ""
                DcProv.Leggi(Session("DC_OSPITE"))
                DO11_PROVINCIA = DcProv.CodificaProvincia
                DO11_CONDPAG_CODICE_SEN = Ospite.ModalitaPagamento
                Dim MPAg As New ClsModalitaPagamento

                MPAg.Codice = Ospite.ModalitaPagamento
                MPAg.Leggi(Session("DC_OSPITE"))
                DO11_CONDPAG_NOME_SEN = MPAg.DESCRIZIONE
            End If


            Dim M As New Cls_TipoPagamento

            M.Codice = campodb(myPOSTreader.Item("ModalitaPagamento"))
            M.Leggi(Session("DC_TABELLE"))
            If M.Descrizione <> "" Then
                DO11_CONDPAG_CODICE_SEN = M.Codice
                DO11_CONDPAG_NOME_SEN = M.Descrizione
            End If

            APPOGGIO = APPOGGIO & AdattaLunghezza("", 4) 'FAT-DITTANUM (numero ditta?)
            APPOGGIO = APPOGGIO & AdattaLunghezza("1", 1) 'FAT-TIPODOC
            APPOGGIO = APPOGGIO & Format(campodbD(myPOSTreader.Item("DataRegistrazione")), "ddMMyy") 'FAT-DATAFAT
            APPOGGIO = APPOGGIO & AdattaLunghezzaNumero("", 2) 'FAT-SERIE (numero serie ?)
            APPOGGIO = APPOGGIO & AdattaLunghezzaNumero(campodbN(myPOSTreader.Item("NumeroProtocollo")), 6) 'FAT-NUMFAT
            APPOGGIO = APPOGGIO & AdattaLunghezzaNumero("", 5) 'FAT-CLFO (se non ho il numero cliente, lo crea?)
            APPOGGIO = APPOGGIO & AdattaLunghezza(DO11_RAGIONESOCIALE, 32) 'FAT-RAGSOC
            APPOGGIO = APPOGGIO & AdattaLunghezza(DO11_INDIRIZZO, 32) 'FAT-INDIRIZZO
            APPOGGIO = APPOGGIO & AdattaLunghezzaNumero(DO11_CAP, 5) 'FAT-CAP
            APPOGGIO = APPOGGIO & AdattaLunghezza(DO11_CITTA_NOME, 25) 'FAT-LOCALITA
            APPOGGIO = APPOGGIO & AdattaLunghezza(DO11_PROVINCIA, 2) 'FAT-PROV
            APPOGGIO = APPOGGIO & AdattaLunghezzaNumero(DO11_PIVA, 11) 'FAT-PARTIVA
            APPOGGIO = APPOGGIO & AdattaLunghezza(DO11_CF, 16) 'FAT-CODFISC



            Dim Progressivo As Integer = 0
            Dim Entrato As Boolean = False
            Dim Numero As Integer = 0

            Dim TotFat As Double = 0
            Dim VtImponibile(100) As Double
            Dim VtImporto(100) As Double
            Dim VtCodiceIva(100) As String

            Dim cmdRigaIVA As New OleDbCommand()
            cmdRigaIVA.CommandText = "Select * From MovimentiContabiliRiga Where Numero = ?  And Tipo = 'IV'  "
            cmdRigaIVA.Connection = cn
            cmdRigaIVA.Parameters.AddWithValue("@Numero", campodbN(myPOSTreader.Item("NumeroRegistrazione")))
            Dim MyRigaIVA As OleDbDataReader = cmdRigaIVA.ExecuteReader()
            Do While MyRigaIVA.Read

                Numero = Numero + 1
                VtImponibile(Numero) = Int(campodbN(MyRigaIVA.Item("Imponibile")) * 100)
                VtImporto(Numero) = Int(campodbN(MyRigaIVA.Item("Importo")) * 100)
                Dim KIVA As New Cls_IVA

                KIVA.Codice = campodb(MyRigaIVA.Item("CodiceIva"))
                KIVA.Leggi(Session("DC_TABELLE"), KIVA.Codice)
                If KIVA.Aliquota > 0 Then
                    VtCodiceIva(Numero) = Int(KIVA.Aliquota * 100)
                Else
                    If KIVA.Codice = "10" Then
                        VtCodiceIva(Numero) = "310"
                    End If
                    If KIVA.Codice = "15" Then
                        VtCodiceIva(Numero) = "315"
                    End If
                End If
                TotFat = TotFat + campodbN(MyRigaIVA.Item("Imponibile")) + campodbN(MyRigaIVA.Item("Importo"))
            Loop
            MyRigaIVA.Close()

            Numero = 0

            Dim VtConto(10) As String
            Dim VtImpConto(10) As Double


            Dim cmdRiga As New OleDbCommand()
            cmdRiga.CommandText = "Select MastroPartita,ContoPartita,SottocontoPartita,sum(importo) as totoconto From MovimentiContabiliRiga Where Numero = ?  And (Tipo Is Null Or Tipo <> 'IV') group by MastroPartita,ContoPartita,SottocontoPartita  "
            cmdRiga.Connection = cn
            cmdRiga.Parameters.AddWithValue("@Numero", campodbN(myPOSTreader.Item("NumeroRegistrazione")))
            Dim MyRiga As OleDbDataReader = cmdRiga.ExecuteReader()
            Do While MyRiga.Read
                Dim Prova As String
                Numero = Numero + 1
                Prova = campodbN(MyRiga.Item("MastroPartita")) & campodbN(MyRiga.Item("ContoPartita")) & campodbN(MyRiga.Item("SottocontoPartita"))

                VtConto(Numero) = Prova
                VtImpConto(Numero) = Int(campodbN(MyRiga.Item("totoconto")) * 100)
            Loop
            MyRiga.Close()

            APPOGGIO = APPOGGIO & AdattaLunghezzaNumero(VtImponibile(1), 12) 'FAT-IMPONIBILE-1
            APPOGGIO = APPOGGIO & AdattaLunghezzaNumero(VtImponibile(2), 12) 'FAT-IMPONIBILE-2
            APPOGGIO = APPOGGIO & AdattaLunghezzaNumero(VtImponibile(3), 12) 'FAT-IMPONIBILE-3
            APPOGGIO = APPOGGIO & AdattaLunghezzaNumero(VtImponibile(4), 12) 'FAT-IMPONIBILE-4
            APPOGGIO = APPOGGIO & AdattaLunghezzaNumero(VtImponibile(5), 12) 'FAT-IMPONIBILE-5
            APPOGGIO = APPOGGIO & AdattaLunghezzaNumero("", 12) 'FAT-IMPONIBILE-6
            APPOGGIO = APPOGGIO & AdattaLunghezzaNumero("", 12) 'FAT-IMPONIBILE-7
            APPOGGIO = APPOGGIO & AdattaLunghezzaNumero("", 12) 'FAT-IMPONIBILE-8
            APPOGGIO = APPOGGIO & AdattaLunghezzaNumero(VtCodiceIva(1), 3) 'FAT-ALIVA-1
            APPOGGIO = APPOGGIO & AdattaLunghezzaNumero(VtCodiceIva(2), 3) 'FAT-ALIVA-2
            APPOGGIO = APPOGGIO & AdattaLunghezzaNumero(VtCodiceIva(3), 3) 'FAT-ALIVA-3
            APPOGGIO = APPOGGIO & AdattaLunghezzaNumero(VtCodiceIva(4), 3) 'FAT-ALIVA-4
            APPOGGIO = APPOGGIO & AdattaLunghezzaNumero(VtCodiceIva(5), 3) 'FAT-ALIVA-5
            APPOGGIO = APPOGGIO & AdattaLunghezzaNumero("", 3) 'FAT-ALIVA-6
            APPOGGIO = APPOGGIO & AdattaLunghezzaNumero("", 3) 'FAT-ALIVA-7
            APPOGGIO = APPOGGIO & AdattaLunghezzaNumero("", 3) 'FAT-ALIVA-8
            APPOGGIO = APPOGGIO & AdattaLunghezzaNumero(VtImporto(1), 10) 'FAT-IMPIVA-1
            APPOGGIO = APPOGGIO & AdattaLunghezzaNumero(VtImporto(2), 10) 'FAT-IMPIVA-2
            APPOGGIO = APPOGGIO & AdattaLunghezzaNumero(VtImporto(3), 10) 'FAT-IMPIVA-3
            APPOGGIO = APPOGGIO & AdattaLunghezzaNumero(VtImporto(4), 10) 'FAT-IMPIVA-4
            APPOGGIO = APPOGGIO & AdattaLunghezzaNumero(VtImporto(5), 10) 'FAT-IMPIVA-5
            APPOGGIO = APPOGGIO & AdattaLunghezzaNumero("", 10) 'FAT-IMPIVA-6
            APPOGGIO = APPOGGIO & AdattaLunghezzaNumero("", 10) 'FAT-IMPIVA-7
            APPOGGIO = APPOGGIO & AdattaLunghezzaNumero("", 10) 'FAT-IMPIVA-8
            APPOGGIO = APPOGGIO & AdattaLunghezzaNumero("", 3) 'FAT-TIPOIVA
            APPOGGIO = APPOGGIO & AdattaLunghezzaNumero("", 3) 'FAT-CODPAGIN
            APPOGGIO = APPOGGIO & AdattaLunghezzaNumero("", 12) 'FAT-COSTO
            APPOGGIO = APPOGGIO & AdattaLunghezzaNumero("", 10) 'FAT-ACCONTI
            APPOGGIO = APPOGGIO & AdattaLunghezzaNumero("", 12) 'FAT-IMPSCPIEDE
            APPOGGIO = APPOGGIO & AdattaLunghezzaNumero("", 8) 'FAT-RIFPARNUM
            APPOGGIO = APPOGGIO & AdattaLunghezzaNumero("", 2) 'FAT-RIFPARAA
            APPOGGIO = APPOGGIO & AdattaLunghezzaNumero("", 5) 'FAT-CODBANCA
            APPOGGIO = APPOGGIO & AdattaLunghezzaNumero("", 5) 'FAT-CODCAB
            APPOGGIO = APPOGGIO & AdattaLunghezza("", 1) 'FAT-FLAG-CORRISPETTIVI
            APPOGGIO = APPOGGIO & AdattaLunghezzaNumero("", 1) 'FAT-ESIGIBILITA-DIFFERITA
            APPOGGIO = APPOGGIO & AdattaLunghezza("", 14) 'FAT-PARTIVA-EST
            APPOGGIO = APPOGGIO & AdattaLunghezzaNumero("", 10) 'FAT-IMPSPINC
            APPOGGIO = APPOGGIO & AdattaLunghezzaNumero("", 10) 'FAT-IMPBOLLI
            APPOGGIO = APPOGGIO & AdattaLunghezzaNumero("", 10) 'FAT-IMPABBUONI
            APPOGGIO = APPOGGIO & AdattaLunghezzaNumero("", 10) 'FAT-IMPONIBPROV
            APPOGGIO = APPOGGIO & AdattaLunghezzaNumero("", 10) 'FAT-IMPORTOPROV
            APPOGGIO = APPOGGIO & AdattaLunghezzaNumero("", 6) 'FAT-PROVVMEDIA
            APPOGGIO = APPOGGIO & AdattaLunghezzaNumero("", 1) 'FAT-TIPO-EFF-01
            APPOGGIO = APPOGGIO & AdattaLunghezzaNumero("", 1) 'FAT-TIPO-EFF-02
            APPOGGIO = APPOGGIO & AdattaLunghezzaNumero("", 1) 'FAT-TIPO-EFF-03
            APPOGGIO = APPOGGIO & AdattaLunghezzaNumero("", 1) 'FAT-TIPO-EFF-04
            APPOGGIO = APPOGGIO & AdattaLunghezzaNumero("", 1) 'FAT-TIPO-EFF-05
            APPOGGIO = APPOGGIO & AdattaLunghezzaNumero("", 1) 'FAT-TIPO-EFF-06
            APPOGGIO = APPOGGIO & AdattaLunghezzaNumero("", 1) 'FAT-TIPO-EFF-07
            APPOGGIO = APPOGGIO & AdattaLunghezzaNumero("", 1) 'FAT-TIPO-EFF-08
            APPOGGIO = APPOGGIO & AdattaLunghezzaNumero("", 1) 'FAT-TIPO-EFF-09
            APPOGGIO = APPOGGIO & AdattaLunghezzaNumero("", 1) 'FAT-TIPO-EFF-10
            APPOGGIO = APPOGGIO & AdattaLunghezzaNumero("", 1) 'FAT-TIPO-EFF-11
            APPOGGIO = APPOGGIO & AdattaLunghezzaNumero("", 1) 'FAT-TIPO-EFF-12
            APPOGGIO = APPOGGIO & AdattaLunghezza("", 8) 'FAT-DATA-SCAD-01
            APPOGGIO = APPOGGIO & AdattaLunghezza("", 8) 'FAT-DATA-SCAD-02
            APPOGGIO = APPOGGIO & AdattaLunghezza("", 8) 'FAT-DATA-SCAD-03
            APPOGGIO = APPOGGIO & AdattaLunghezza("", 8) 'FAT-DATA-SCAD-04
            APPOGGIO = APPOGGIO & AdattaLunghezza("", 8) 'FAT-DATA-SCAD-05
            APPOGGIO = APPOGGIO & AdattaLunghezza("", 8) 'FAT-DATA-SCAD-06
            APPOGGIO = APPOGGIO & AdattaLunghezza("", 8) 'FAT-DATA-SCAD-07
            APPOGGIO = APPOGGIO & AdattaLunghezza("", 8) 'FAT-DATA-SCAD-08
            APPOGGIO = APPOGGIO & AdattaLunghezza("", 8) 'FAT-DATA-SCAD-09
            APPOGGIO = APPOGGIO & AdattaLunghezza("", 8) 'FAT-DATA-SCAD-10
            APPOGGIO = APPOGGIO & AdattaLunghezza("", 8) 'FAT-DATA-SCAD-11
            APPOGGIO = APPOGGIO & AdattaLunghezza("", 8) 'FAT-DATA-SCAD-12
            APPOGGIO = APPOGGIO & AdattaLunghezzaNumero("", 12) 'FAT-IMPO-SCAD-01
            APPOGGIO = APPOGGIO & AdattaLunghezzaNumero("", 12) 'FAT-IMPO-SCAD-02
            APPOGGIO = APPOGGIO & AdattaLunghezzaNumero("", 12) 'FAT-IMPO-SCAD-03
            APPOGGIO = APPOGGIO & AdattaLunghezzaNumero("", 12) 'FAT-IMPO-SCAD-04
            APPOGGIO = APPOGGIO & AdattaLunghezzaNumero("", 12) 'FAT-IMPO-SCAD-05
            APPOGGIO = APPOGGIO & AdattaLunghezzaNumero("", 12) 'FAT-IMPO-SCAD-06
            APPOGGIO = APPOGGIO & AdattaLunghezzaNumero("", 12) 'FAT-IMPO-SCAD-07
            APPOGGIO = APPOGGIO & AdattaLunghezzaNumero("", 12) 'FAT-IMPO-SCAD-08
            APPOGGIO = APPOGGIO & AdattaLunghezzaNumero("", 12) 'FAT-IMPO-SCAD-09
            APPOGGIO = APPOGGIO & AdattaLunghezzaNumero("", 12) 'FAT-IMPO-SCAD-10
            APPOGGIO = APPOGGIO & AdattaLunghezzaNumero("", 12) 'FAT-IMPO-SCAD-11
            APPOGGIO = APPOGGIO & AdattaLunghezzaNumero("", 12) 'FAT-IMPO-SCAD-12
            APPOGGIO = APPOGGIO & AdattaLunghezzaNumero("", 8) 'FAT-IMPO-BOLLO-01
            APPOGGIO = APPOGGIO & AdattaLunghezzaNumero("", 8) 'FAT-IMPO-BOLLO-02
            APPOGGIO = APPOGGIO & AdattaLunghezzaNumero("", 8) 'FAT-IMPO-BOLLO-03
            APPOGGIO = APPOGGIO & AdattaLunghezzaNumero("", 8) 'FAT-IMPO-BOLLO-04
            APPOGGIO = APPOGGIO & AdattaLunghezzaNumero("", 8) 'FAT-IMPO-BOLLO-05
            APPOGGIO = APPOGGIO & AdattaLunghezzaNumero("", 8) 'FAT-IMPO-BOLLO-06
            APPOGGIO = APPOGGIO & AdattaLunghezzaNumero("", 8) 'FAT-IMPO-BOLLO-07
            APPOGGIO = APPOGGIO & AdattaLunghezzaNumero("", 8) 'FAT-IMPO-BOLLO-08
            APPOGGIO = APPOGGIO & AdattaLunghezzaNumero("", 8) 'FAT-IMPO-BOLLO-09
            APPOGGIO = APPOGGIO & AdattaLunghezzaNumero("", 8) 'FAT-IMPO-BOLLO-10
            APPOGGIO = APPOGGIO & AdattaLunghezzaNumero("", 8) 'FAT-IMPO-BOLLO-11
            APPOGGIO = APPOGGIO & AdattaLunghezzaNumero("", 8) 'FAT-IMPO-BOLLO-12
            APPOGGIO = APPOGGIO & AdattaLunghezzaNumero(VtConto(1), 7) 'FAT-CONTO-01
            APPOGGIO = APPOGGIO & AdattaLunghezzaNumero(VtConto(2), 7) 'FAT-CONTO-02
            APPOGGIO = APPOGGIO & AdattaLunghezzaNumero(VtConto(3), 7) 'FAT-CONTO-03
            APPOGGIO = APPOGGIO & AdattaLunghezzaNumero(VtConto(4), 7) 'FAT-CONTO-04
            APPOGGIO = APPOGGIO & AdattaLunghezzaNumero(VtConto(5), 7) 'FAT-CONTO-05
            APPOGGIO = APPOGGIO & AdattaLunghezzaNumero("", 7) 'FAT-CONTO-06
            APPOGGIO = APPOGGIO & AdattaLunghezzaNumero("", 7) 'FAT-CONTO-07
            APPOGGIO = APPOGGIO & AdattaLunghezzaNumero("", 7) 'FAT-CONTO-08
            APPOGGIO = APPOGGIO & AdattaLunghezzaNumero("", 7) 'FAT-CONTO-09
            APPOGGIO = APPOGGIO & AdattaLunghezzaNumero("", 7) 'FAT-CONTO-10
            APPOGGIO = APPOGGIO & AdattaLunghezzaNumero("", 7) 'FAT-CONTO-11
            APPOGGIO = APPOGGIO & AdattaLunghezzaNumero("", 7) 'FAT-CONTO-12
            APPOGGIO = APPOGGIO & AdattaLunghezzaNumero("", 7) 'FAT-CONTO-13
            APPOGGIO = APPOGGIO & AdattaLunghezzaNumero("", 7) 'FAT-CONTO-14
            APPOGGIO = APPOGGIO & AdattaLunghezzaNumero("", 7) 'FAT-CONTO-15
            APPOGGIO = APPOGGIO & AdattaLunghezzaNumero("", 7) 'FAT-CONTO-16
            APPOGGIO = APPOGGIO & AdattaLunghezzaNumero("", 7) 'FAT-CONTO-17
            APPOGGIO = APPOGGIO & AdattaLunghezzaNumero("", 7) 'FAT-CONTO-18
            APPOGGIO = APPOGGIO & AdattaLunghezzaNumero("", 7) 'FAT-CONTO-19
            APPOGGIO = APPOGGIO & AdattaLunghezzaNumero("", 7) 'FAT-CONTO-20
            APPOGGIO = APPOGGIO & AdattaLunghezzaNumero(VtImpConto(1), 12) 'FAT-TOT-CONTO-01
            APPOGGIO = APPOGGIO & AdattaLunghezzaNumero(VtImpConto(2), 12) 'FAT-TOT-CONTO-02
            APPOGGIO = APPOGGIO & AdattaLunghezzaNumero(VtImpConto(3), 12) 'FAT-TOT-CONTO-03
            APPOGGIO = APPOGGIO & AdattaLunghezzaNumero(VtImpConto(4), 12) 'FAT-TOT-CONTO-04
            APPOGGIO = APPOGGIO & AdattaLunghezzaNumero(VtImpConto(5), 12) 'FAT-TOT-CONTO-05
            APPOGGIO = APPOGGIO & AdattaLunghezzaNumero("", 12) 'FAT-TOT-CONTO-06
            APPOGGIO = APPOGGIO & AdattaLunghezzaNumero("", 12) 'FAT-TOT-CONTO-07
            APPOGGIO = APPOGGIO & AdattaLunghezzaNumero("", 12) 'FAT-TOT-CONTO-08
            APPOGGIO = APPOGGIO & AdattaLunghezzaNumero("", 12) 'FAT-TOT-CONTO-09
            APPOGGIO = APPOGGIO & AdattaLunghezzaNumero("", 12) 'FAT-TOT-CONTO-10
            APPOGGIO = APPOGGIO & AdattaLunghezzaNumero("", 12) 'FAT-TOT-CONTO-11
            APPOGGIO = APPOGGIO & AdattaLunghezzaNumero("", 12) 'FAT-TOT-CONTO-12
            APPOGGIO = APPOGGIO & AdattaLunghezzaNumero("", 12) 'FAT-TOT-CONTO-13
            APPOGGIO = APPOGGIO & AdattaLunghezzaNumero("", 12) 'FAT-TOT-CONTO-14
            APPOGGIO = APPOGGIO & AdattaLunghezzaNumero("", 12) 'FAT-TOT-CONTO-15
            APPOGGIO = APPOGGIO & AdattaLunghezzaNumero("", 12) 'FAT-TOT-CONTO-16
            APPOGGIO = APPOGGIO & AdattaLunghezzaNumero("", 12) 'FAT-TOT-CONTO-17
            APPOGGIO = APPOGGIO & AdattaLunghezzaNumero("", 12) 'FAT-TOT-CONTO-18
            APPOGGIO = APPOGGIO & AdattaLunghezzaNumero("", 12) 'FAT-TOT-CONTO-19
            APPOGGIO = APPOGGIO & AdattaLunghezzaNumero("", 12) 'FAT-TOT-CONTO-20
            APPOGGIO = APPOGGIO & AdattaLunghezzaNumero("", 12) 'FAT-TOT-DOCUM
            APPOGGIO = APPOGGIO & AdattaLunghezzaNumero("", 1) 'FAT-TIPOFAT
            APPOGGIO = APPOGGIO & AdattaLunghezzaNumero("", 3) 'FAT-VALUTA
            APPOGGIO = APPOGGIO & AdattaLunghezzaNumero("", 9) 'FAT-CAMBIO
            APPOGGIO = APPOGGIO & AdattaLunghezzaNumero("", 12) 'FAT-IMPOMAG
            APPOGGIO = APPOGGIO & AdattaLunghezzaNumero("", 10) 'FAT-IVAOMAG
            APPOGGIO = APPOGGIO & AdattaLunghezza("", 40) 'FAT-CONTRATTO
            APPOGGIO = APPOGGIO & AdattaLunghezzaNumero("", 4) 'FAT-PAESE
            APPOGGIO = APPOGGIO & AdattaLunghezza("", 2) 'FAT-VERSIONE
            APPOGGIO = APPOGGIO & AdattaLunghezzaNumero("", 5) 'FAT-COMP-IVA
            APPOGGIO = APPOGGIO & AdattaLunghezzaNumero("", 4) 'FAT-REGIME-IVA
            APPOGGIO = APPOGGIO & AdattaLunghezzaNumero("", 4) 'FAT-REGIME-IVA-2
            APPOGGIO = APPOGGIO & AdattaLunghezzaNumero("", 4) 'FAT-REGIME-IVA-3
            APPOGGIO = APPOGGIO & AdattaLunghezzaNumero("", 4) 'FAT-REGIME-IVA-4
            APPOGGIO = APPOGGIO & AdattaLunghezzaNumero("", 4) 'FAT-REGIME-IVA-5
            APPOGGIO = APPOGGIO & AdattaLunghezzaNumero("", 4) 'FAT-REGIME-IVA-6
            APPOGGIO = APPOGGIO & AdattaLunghezzaNumero("", 4) 'FAT-REGIME-IVA-7
            APPOGGIO = APPOGGIO & AdattaLunghezzaNumero("", 4) 'FAT-REGIME-IVA-8
            APPOGGIO = APPOGGIO & AdattaLunghezzaNumero("", 3) 'FAT-PROG
            APPOGGIO = APPOGGIO & AdattaLunghezza("", 8) 'FAT-DATA-INVIO
            APPOGGIO = APPOGGIO & AdattaLunghezza("", 1) 'FAT-STATO-PA
            APPOGGIO = APPOGGIO & AdattaLunghezzaNumero("", 13) 'FAT-SCCASSA-IMP
            APPOGGIO = APPOGGIO & AdattaLunghezzaNumero("", 13) 'FAT-SCCASSA-PERC
            APPOGGIO = APPOGGIO & AdattaLunghezzaNumero("", 14) 'FAT-IMP-SCONTO-MERCE
            APPOGGIO = APPOGGIO & AdattaLunghezzaNumero("", 13) 'FAT-IMPONIBPROV-2
            APPOGGIO = APPOGGIO & AdattaLunghezzaNumero("", 13) 'FAT-IMPORTOPROV-2
            APPOGGIO = APPOGGIO & AdattaLunghezzaNumero("", 6) 'FAT-PROVVMEDIA-2
            APPOGGIO = APPOGGIO & AdattaLunghezzaNumero("", 4) 'FAT-CAPOZONA
            APPOGGIO = APPOGGIO & AdattaLunghezzaNumero("", 4) 'FAT-AGENTE
            APPOGGIO = APPOGGIO & AdattaLunghezzaNumero("", 13) 'FAT-ENAS
            APPOGGIO = APPOGGIO & AdattaLunghezzaNumero("", 1) 'FAT-MONETA-CONTO
            APPOGGIO = APPOGGIO & AdattaLunghezzaNumero("", 3) 'FAT-CAUT
            APPOGGIO = APPOGGIO & AdattaLunghezza("", 1) 'FAT-RICFIS-INCASSATA
            APPOGGIO = APPOGGIO & AdattaLunghezzaNumero("", 1) 'FAT-CREOFF
            APPOGGIO = APPOGGIO & AdattaLunghezzaNumero("", 1) 'FAT-CONTR
            APPOGGIO = APPOGGIO & AdattaLunghezza("", 8) 'FAT-DATA-CAMBIO
            APPOGGIO = APPOGGIO & AdattaLunghezzaNumero("", 1) 'FAT-MONETA-DOC
            APPOGGIO = APPOGGIO & AdattaLunghezzaNumero("", 6) 'FAT-NUMFATPRO
            APPOGGIO = APPOGGIO & AdattaLunghezzaNumero("", 4) 'FAT-SEZFATPRO
            APPOGGIO = APPOGGIO & AdattaLunghezzaNumero("", 13) 'FAT-CONTRIB
            APPOGGIO = APPOGGIO & AdattaLunghezzaNumero("", 13) 'FAT-RITAC
            APPOGGIO = APPOGGIO & AdattaLunghezzaNumero("", 10) 'FAT-NRDIST-POSTEL
            APPOGGIO = APPOGGIO & AdattaLunghezza("", 1) 'FAT-FLAG-230900
            APPOGGIO = APPOGGIO & AdattaLunghezzaNumero("", 4) 'FAT-CUP
            APPOGGIO = APPOGGIO & AdattaLunghezzaNumero("", 4) 'FAT-CIG
            APPOGGIO = APPOGGIO & AdattaLunghezza("", 1) 'FAT-TIPODOC-ART62
            APPOGGIO = APPOGGIO & AdattaLunghezza("", 8) 'FAT-DATA-DECORRENZA
            APPOGGIO = APPOGGIO & AdattaLunghezzaNumero("", 6) 'FAT-DIST-EMAIL
            APPOGGIO = APPOGGIO & AdattaLunghezzaNumero("", 6) 'FAT-DIST-PAGAMENTI
            APPOGGIO = APPOGGIO & AdattaLunghezza("", 8) 'FAT-PROG-DISTINTA-XML
            APPOGGIO = APPOGGIO & AdattaLunghezza("", 1) 'FAT-VAR-CONTI
            APPOGGIO = APPOGGIO & AdattaLunghezza("", 1) 'FAT-VAR-SCAD
            APPOGGIO = APPOGGIO & AdattaLunghezza("", 56) 'FILLER

            tw.WriteLine(APPOGGIO)


            If Entrato = False Then
                Entrato = False

            End If

        Loop
        myPOSTreader.Close()
        cn.Close()

        tw.Close()

        tw = Nothing
        'twR = Nothing

        Try
            Response.Clear()
            Response.Buffer = True
            Response.ContentType = "text/plain"
            Response.AppendHeader("content-disposition", "attachment;filename=output.txt")
            Response.WriteFile(NomeFile)
            Response.Flush()
            Response.End()
        Catch ex As Exception
            Kill(NomeFile)
        End Try

    End Sub




    Protected Sub Btn_Esci_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles Btn_Esci.Click
        Dim NomeFile As String

        If RB_AdHoc.Checked = True Then
            NomeFile = HostingEnvironment.ApplicationPhysicalPath() & "\Public\Esporta.Rfa"
            If System.IO.File.Exists(NomeFile) Then
                Kill(NomeFile)
            End If
        End If
        If RB_IPSOA.Checked = True Then
            NomeFile = HostingEnvironment.ApplicationPhysicalPath() & "\Public\Clienti_.TXT"

            If System.IO.File.Exists(NomeFile) Then
                Kill(NomeFile)
            End If
        End If
        If RB_Ats.Checked = True Then
            NomeFile = HostingEnvironment.ApplicationPhysicalPath() & "\Public\Farmaci_.txt"

            If System.IO.File.Exists(NomeFile) Then
                Kill(NomeFile)
            End If
        End If
        If RB_DocumentiIncassi.Checked = True Then
            NomeFile = HostingEnvironment.ApplicationPhysicalPath() & "\Public\DocumentiIncassi_" & Format(Session("MYDATE"), "yyyyMMddHHmmss") & ".Txt"

            If System.IO.File.Exists(NomeFile) Then
                Kill(NomeFile)
            End If

        End If

        If RB_PassePartout.Checked = True Then
            NomeFile = HostingEnvironment.ApplicationPhysicalPath() & "\Public\Righe_" & Format(Session("MYDATE"), "yyyyMMddHHmm") & ".Txt"

            If System.IO.File.Exists(NomeFile) Then
                Kill(NomeFile)
            End If
        End If


        If RB_POLIFARMA.Checked = True Then
            NomeFile = HostingEnvironment.ApplicationPhysicalPath() & "\Public\Polifarma_IN_" & Format(Session("MYDATE"), "yyyyMMddHHmmss") & ".txt"

            If System.IO.File.Exists(NomeFile) Then
                Kill(NomeFile)
            End If

        End If

        If RB_AdHoc.Checked = True Then
            NomeFile = HostingEnvironment.ApplicationPhysicalPath() & "\Public\Esporta.Tfa"
            If System.IO.File.Exists(NomeFile) Then
                Kill(NomeFile)
            End If
        End If

        If RB_IPSOA.Checked = True Then
            NomeFile = HostingEnvironment.ApplicationPhysicalPath() & "\Public\Movim_.Txt"

            If System.IO.File.Exists(NomeFile) Then
                Kill(NomeFile)
            End If
        End If




        If RB_Ats.Checked = True Then
            NomeFile = HostingEnvironment.ApplicationPhysicalPath() & "\Public\Movim_.Txt"

            If System.IO.File.Exists(NomeFile) Then
                Kill(NomeFile)
            End If
        End If



        If RB_PassePartout.Checked = True Then
            NomeFile = HostingEnvironment.ApplicationPhysicalPath() & "\Public\Testata_" & Format(Session("MYDATE"), "yyyyMMddHHmm") & ".Txt"

            If System.IO.File.Exists(NomeFile) Then
                Kill(NomeFile)
            End If


        End If

        If RB_POLIFARMA.Checked = True Then
            NomeFile = HostingEnvironment.ApplicationPhysicalPath() & "\Public\Polifarma_FT_" & Format(Session("MYDATE"), "yyyyMMddHHmmss") & ".Txt"

            If System.IO.File.Exists(NomeFile) Then
                Kill(NomeFile)
            End If


        End If


        If RB_DocumentiIncassi.Checked = True Then
            NomeFile = HostingEnvironment.ApplicationPhysicalPath() & "\Public\DocumentiIncassi_" & Format(Session("MYDATE"), "yyyyMMddHHmmss") & ".csv"

            If System.IO.File.Exists(NomeFile) Then
                Kill(NomeFile)
            End If

        End If


        NomeFile = HostingEnvironment.ApplicationPhysicalPath() & "\Public\Esporta.Tfa"
        If System.IO.File.Exists(NomeFile) Then
            If System.IO.File.GetLastWriteTime(NomeFile).ToString("yyyyMMdd") < Format(Now, "yyyyMMdd") Then
                Kill(NomeFile)
            End If
        End If

        NomeFile = HostingEnvironment.ApplicationPhysicalPath() & "\Public\Esporta.Rfa"
        If System.IO.File.Exists(NomeFile) Then
            If System.IO.File.GetLastWriteTime(NomeFile).ToString("yyyyMMdd") < Format(Now, "yyyyMMdd") Then
                Kill(NomeFile)
            End If
        End If

        NomeFile = HostingEnvironment.ApplicationPhysicalPath() & "\Public\Clienti_.TXT"
        If System.IO.File.Exists(NomeFile) Then
            If System.IO.File.GetLastWriteTime(NomeFile).ToString("yyyyMMdd") < Format(Now, "yyyyMMdd") Then
                Kill(NomeFile)
            End If
        End If

        NomeFile = HostingEnvironment.ApplicationPhysicalPath() & "\Public\Farmaci_.txt"
        If System.IO.File.Exists(NomeFile) Then
            If System.IO.File.GetLastWriteTime(NomeFile).ToString("yyyyMMdd") < Format(Now, "yyyyMMdd") Then
                Kill(NomeFile)
            End If
        End If


        NomeFile = HostingEnvironment.ApplicationPhysicalPath() & "\Public\Esporta.Tfa"
        If System.IO.File.Exists(NomeFile) Then
            If System.IO.File.GetLastWriteTime(NomeFile).ToString("yyyyMMdd") < Format(Now, "yyyyMMdd") Then
                Kill(NomeFile)
            End If
        End If

        NomeFile = HostingEnvironment.ApplicationPhysicalPath() & "\Public\Esporta.Rfa"
        If System.IO.File.Exists(NomeFile) Then
            If System.IO.File.GetLastWriteTime(NomeFile).ToString("yyyyMMdd") < Format(Now, "yyyyMMdd") Then
                Kill(NomeFile)
            End If
        End If


        NomeFile = HostingEnvironment.ApplicationPhysicalPath() & "\Public\Movim_.Txt"
        If System.IO.File.Exists(NomeFile) Then
            If System.IO.File.GetLastWriteTime(NomeFile).ToString("yyyyMMdd") < Format(Now, "yyyyMMdd") Then
                Kill(NomeFile)
            End If
        End If

        NomeFile = HostingEnvironment.ApplicationPhysicalPath() & "\Public\"

        For Each foundFile As String In My.Computer.FileSystem.GetFiles(NomeFile, FileIO.SearchOption.SearchTopLevelOnly, "Testata_*.Txt")
            If System.IO.File.GetLastWriteTime(foundFile).ToString("yyyyMMdd") < Format(Now, "yyyyMMdd") Then
                Kill(foundFile)
            End If
        Next


        NomeFile = HostingEnvironment.ApplicationPhysicalPath() & "\Public\"

        For Each foundFile As String In My.Computer.FileSystem.GetFiles(NomeFile, FileIO.SearchOption.SearchTopLevelOnly, "Polifarma_FT_*.Txt")
            If System.IO.File.GetLastWriteTime(foundFile).ToString("yyyyMMdd") < Format(Now, "yyyyMMdd") Then
                Kill(foundFile)
            End If
        Next

        NomeFile = HostingEnvironment.ApplicationPhysicalPath() & "\Public\"
        For Each foundFile As String In My.Computer.FileSystem.GetFiles(NomeFile, FileIO.SearchOption.SearchTopLevelOnly, "Testata_*.Txt")
            If System.IO.File.GetLastWriteTime(foundFile).ToString("yyyyMMdd") < Format(Now, "yyyyMMdd") Then
                Kill(foundFile)
            End If
        Next


        NomeFile = HostingEnvironment.ApplicationPhysicalPath() & "\Public\"

        For Each foundFile As String In My.Computer.FileSystem.GetFiles(NomeFile, FileIO.SearchOption.SearchTopLevelOnly, "DocumentiIncassi_*.Txt")
            If System.IO.File.GetLastWriteTime(foundFile).ToString("yyyyMMdd") < Format(Now, "yyyyMMdd") Then
                Kill(foundFile)
            End If
        Next


        NomeFile = HostingEnvironment.ApplicationPhysicalPath() & "\Public\"

        For Each foundFile As String In My.Computer.FileSystem.GetFiles(NomeFile, FileIO.SearchOption.SearchTopLevelOnly, "DocumentiIncassi_*.csv")
            If System.IO.File.GetLastWriteTime(foundFile).ToString("yyyyMMdd") < Format(Now, "yyyyMMdd") Then
                Kill(foundFile)
            End If
        Next

        NomeFile = HostingEnvironment.ApplicationPhysicalPath() & "\Public\"
        For Each foundFile As String In My.Computer.FileSystem.GetFiles(NomeFile, FileIO.SearchOption.SearchTopLevelOnly, "Righe_*.Txt")
            If System.IO.File.GetLastWriteTime(foundFile).ToString("yyyyMMdd") < Format(Now, "yyyyMMdd") Then
                Kill(foundFile)
            End If
        Next

        NomeFile = HostingEnvironment.ApplicationPhysicalPath() & "\Public\"

        For Each foundFile As String In My.Computer.FileSystem.GetFiles(NomeFile, FileIO.SearchOption.SearchTopLevelOnly, "Polifarma_IN_*.txt")
            If System.IO.File.GetLastWriteTime(foundFile).ToString("yyyyMMdd") < Format(Now, "yyyyMMdd") Then
                Kill(foundFile)
            End If
        Next


        If Request.Item("APPALTI") = "SI" Then
            Response.Redirect("../Appalti/Menu_Appalti.aspx")
        Else
            Response.Redirect("Menu_Export.aspx")
        End If
    End Sub



    Function Export_AlianteNuovaVersione() As Boolean

        Dim cn As OleDbConnection

        Dim MeseContr As Long
        Dim MySql As String
        Dim ProgressivoAssoluto As Integer = 0
        Dim Ragguppato As Boolean = False
        Dim Anno As Integer
        Dim Mese As Integer


        Anno = Txt_Anno.Text
        Mese = DD_Mese.SelectedValue

        MeseContr = DD_Mese.SelectedValue

        Export_AlianteNuovaVersione = True
        Dim TabellaSocieta As New Cls_TabellaSocieta


        TabellaSocieta.Leggi(Session("DC_TABELLE"))

        Dim LeggiParametri As New Cls_Parametri


        LeggiParametri.LeggiParametri(Session("DC_OSPITE"))

        If Txt_DalDocumento.Text.Trim = "" Then
            Lbl_Errori.Text = "Dal documento non indicato"
            Export_AlianteNuovaVersione = False
            Exit Function
        End If


        If Txt_AlDocumento.Text.Trim = "" Then
            Lbl_Errori.Text = "Al documento non indicato"
            Export_AlianteNuovaVersione = False
            Exit Function
        End If

        If Txt_Anno.Text.Trim = "" Then
            Lbl_Errori.Text = "Anno non indicato"
            Export_AlianteNuovaVersione = False
            Exit Function
        End If

        If Mese = 0 Then
            Lbl_Errori.Text = "Mese non indicato"
            Export_AlianteNuovaVersione = False
            Exit Function
        End If

        If TabellaSocieta.NomeFile.Trim = "" Then
            Lbl_Errori.Text = "Nome file non configurato non posso procedere all'esportazione"
            Export_AlianteNuovaVersione = False
            Exit Function
        End If

        If TabellaSocieta.CodiceInstallazione.Trim = "" Then
            Export_AlianteNuovaVersione = False
            Lbl_Errori.Text = "Codice Installazione non configurato non posso procedere all'esportazione"
            Exit Function
        End If

        If TabellaSocieta.CodiceDitta.Trim = "" Then
            Export_AlianteNuovaVersione = False
            Lbl_Errori.Text = "Codice Ditta non configurato non posso procedere all'esportazione"
            Exit Function
        End If

        Dim TabTrascodificaCausale As New Cls_TabellaTrascodificheEsportazioni





        MySql = "SELECT * " & _
                " FROM MovimentiContabiliTesta " & _
                " WHERE AnnoProtocollo = " & Txt_AnnoRif.Text

        If DD_Registro.SelectedValue <> "" Then
            MySql = MySql & " AND MovimentiContabiliTesta.RegistroIva = " & DD_Registro.SelectedValue
        End If
        If Tab_PeriodoData.ActiveTabIndex = 1 And IsDate(Txt_DataAl1.Text) And IsDate(Txt_DataDal1.Text) Then
            MySql = MySql & " AND (DataRegistrazione >= ? And DataRegistrazione <= ?) "
        Else
            MySql = MySql & " And MovimentiContabiliTesta.AnnoCompetenza = " & Txt_Anno.Text & " AND MovimentiContabiliTesta.MeseCompetenza = " & MeseContr & " "
        End If

        MySql = MySql & " AND (NumeroProtocollo >= " & Txt_DalDocumento.Text & " And NumeroProtocollo <= " & Txt_AlDocumento.Text & ") "

        If TabellaSocieta.NomeFile.Trim = "MDAZEGLIO" Then
            MySql = MySql & " AND CentroServizio like '13%'"
        End If
        If TabellaSocieta.NomeFile.Trim = "CHIABRERA" Then
            MySql = MySql & " AND CentroServizio like '14%'"
        End If


        cn = New Data.OleDb.OleDbConnection(Session("DC_GENERALE"))

        cn.Open()
        Dim cmd As New OleDbCommand()
        cmd.CommandText = MySql & " Order by NumeroProtocollo"
        cmd.Connection = cn
        If Tab_PeriodoData.ActiveTabIndex = 1 And IsDate(Txt_DataAl1.Text) And IsDate(Txt_DataDal1.Text) Then
            cmd.Parameters.AddWithValue("@DataDal", Txt_DataDal1.Text)
            cmd.Parameters.AddWithValue("@DataDal", Txt_DataAl1.Text)
        End If


        Dim CodiceInstallazione As String
        Dim CodiceDitta As String
        Dim ProgressivoTesta As Integer
        Dim RegistroIva As Integer
        CodiceInstallazione = 1
        CodiceDitta = 1

        RegistroIva = DD_Registro.SelectedValue
        Dim NomeFile As String

        NomeFile = HostingEnvironment.ApplicationPhysicalPath() & "\Public\" & TabellaSocieta.NomeFile & "\Esportazione_" & TabellaSocieta.NomeFile & "_" & RegistroIva & ".Txt"

        Dim tw As System.IO.TextWriter = System.IO.File.CreateText(NomeFile)

        Dim AppoggioRiga As String = ""
        Dim Appoggio As String = ""

        MyTable.Clear()
        MyTable.Columns.Clear()

        MyTable.Columns.Add("Numero Registrazione", GetType(String))
        MyTable.Columns.Add("Cliente", GetType(String))
        MyTable.Columns.Add("Segnalazione", GetType(String))

        Appoggio = ""
        Appoggio = Appoggio & "IT;"
        Appoggio = Appoggio & "PROGRESSIVO TESTA ;"
        Appoggio = Appoggio & "NUMERO REGISTRAZIONE (SENIOR);"
        Appoggio = Appoggio & "CODICE INSTALLAZIONE;"
        Appoggio = Appoggio & "CODICE DITTA;"
        Appoggio = Appoggio & "CODICE SEDE;"
        Appoggio = Appoggio & "REGISTRO IVA;"
        Appoggio = Appoggio & "TIPO DOCUMENTO SENIOR;"
        Appoggio = Appoggio & "CODICE DOCUMENTO ALYANTE;"
        Appoggio = Appoggio & "NUMERO DOCUMENTO;"
        Appoggio = Appoggio & "DATA DOCUMENTO;"
        Appoggio = Appoggio & "TIPO_CLIFORCG44;"
        Appoggio = Appoggio & "CLIFOR_CG44;"
        Appoggio = Appoggio & "TIPO ANAGRAFICA;"
        Appoggio = Appoggio & "MASTRO CONTO CLIENTE SENIOR;"
        Appoggio = Appoggio & "RAGIONE SOCIALE;"
        Appoggio = Appoggio & "PARTITA IVA;"
        Appoggio = Appoggio & "CODICE FISCALE;"
        Appoggio = Appoggio & "INDIRIZZO (VIA);"
        Appoggio = Appoggio & "CAP;"
        Appoggio = Appoggio & "COMUNE NOME;"
        Appoggio = Appoggio & "CITTA ISTAT;"
        Appoggio = Appoggio & "CODICE CATASTALE COMUNE;"
        Appoggio = Appoggio & "PROVINCIA;"
        Appoggio = Appoggio & "MODALITA PAGAMENTO;"
        Appoggio = Appoggio & "DECODIFICA MODALITA PAGAMENTO;"
        Appoggio = Appoggio & "BANCA CIN;"
        Appoggio = Appoggio & "BANCA ABI;"
        Appoggio = Appoggio & "BANCA CAB;"
        Appoggio = Appoggio & "BANCA CONTO;"
        Appoggio = Appoggio & "BANCA IBAN;"
        Appoggio = Appoggio & "ID MANDATO;"
        Appoggio = Appoggio & "DATA MANDATO;"
        Appoggio = Appoggio & "CIG;"
        Appoggio = Appoggio & "TIPOLOGIA IDDOC;"
        Appoggio = Appoggio & "IDDOCUMENTO (FATTURA ELETTRONICA);"
        Appoggio = Appoggio & "DATA DOCUMENTO (FATTURA ELETTRONICA);"
        Appoggio = Appoggio & "CODICE DESTINATARIO (FATTURA ELETTRONICA);"
        Appoggio = Appoggio & "RIFERIMENTO AMMINISTRAZIONE (FATTURA ELETTRONICA);"
        Appoggio = Appoggio & "PRESENZA DDT: FLAG si/no nel campo DDT inserire numero e data documento;"
        Appoggio = Appoggio & "INTESTATARIO SDD;"
        Appoggio = Appoggio & "CODICE FISCALE INTESTATARIO SDD;"
        Appoggio = Appoggio & "NOME OSPITE RIFERIMENTO;"
        Appoggio = Appoggio & "CODICE FISCALE OSPITE RIFERIMENTO;"
        Appoggio = Appoggio & "DATA NASCITA OSPITE RIFERIEMTNO;"
        Appoggio = Appoggio & "NOME DESTINATARIO FATTURA (CORRISPONDENZA);"
        Appoggio = Appoggio & "INDIRIZZO DESTINATARIO  (CORRISPONDENZA);"
        Appoggio = Appoggio & "CAP COMUNE PROVINCIA  (CORRISPONDENZA);"
        Appoggio = Appoggio & "MAIL DESTINATARIO  (CORRISPONDENZA);"
        Appoggio = Appoggio & "DESCRZIONE (NOTE);"
        Appoggio = Appoggio & "EXTRA1;"
        Appoggio = Appoggio & "EXTRA2;"
        Appoggio = Appoggio & "EXTRA3;"
        Appoggio = Appoggio & "EXTRA4;"
        Appoggio = Appoggio & "EXTRA5;"
        Appoggio = Appoggio & "EXTRA6;"
        Appoggio = Appoggio & "EXTRA7;"
        Appoggio = Appoggio & "EXTRA8;"
        Appoggio = Appoggio & "EXTRA9;"
        Appoggio = Appoggio & "EXTRA10;"
        Appoggio = Appoggio & "EXTRA11;"

        tw.WriteLine(Appoggio)

        Appoggio = ""
        Appoggio = Appoggio & "TC;"
        Appoggio = Appoggio & "ID TAB PROGRESSIVO RIGA;"
        Appoggio = Appoggio & "ID SENIOR;"
        Appoggio = Appoggio & "PROGRESSIVO TESTATA;"
        Appoggio = Appoggio & "CODICE INSTALLAZIONE;"
        Appoggio = Appoggio & "CODICE DITTA;"
        Appoggio = Appoggio & "CODICE SEDE;"
        Appoggio = Appoggio & "REGISTRO IVA;"
        Appoggio = Appoggio & "PROGRESSIVO RIGA SENIOR;"
        Appoggio = Appoggio & "TIPO RIGA ALYANTE;"
        Appoggio = Appoggio & "CODICE ARTICOLO ALYANTE;"
        Appoggio = Appoggio & "CONTO SENIOR ARTICOLO;"
        Appoggio = Appoggio & "DESCRIZIONE CONTO SENIOR;"
        Appoggio = Appoggio & "OPZIONE_MG5E;"
        Appoggio = Appoggio & "DESCRIZIONE RIGA;"
        Appoggio = Appoggio & "DO30_UM1;"
        Appoggio = Appoggio & "QUANTITA;"
        Appoggio = Appoggio & "DO30_COLLI;"
        Appoggio = Appoggio & "IMPORTO;"
        Appoggio = Appoggio & "CODICE IVA SENIOR;"
        Appoggio = Appoggio & "DESCRIZIONE IVA;"
        Appoggio = Appoggio & "CODICE CENTRO SERVIZIO SENIOR;"
        Appoggio = Appoggio & "DESCRIZIONE CENTRO SERVIZIO SENIOR;"
        Appoggio = Appoggio & "ANNO COMPETENZA;"
        Appoggio = Appoggio & "MESE COMPETENZA;"

        tw.WriteLine(Appoggio)

        Appoggio = ""
        Appoggio = Appoggio & "TP;"
        Appoggio = Appoggio & "ID TAB PROGRESSIVO RIGA;"
        Appoggio = Appoggio & "ID SENIOR;"
        Appoggio = Appoggio & "PROGRESSIVO TESTATA;"
        Appoggio = Appoggio & "CODICE INSTALLAZIONE;"
        Appoggio = Appoggio & "CODICE DITTA;"
        Appoggio = Appoggio & "CODICE SEDE;"
        Appoggio = Appoggio & "REGISTRO IVA;"
        Appoggio = Appoggio & "PROGRESSIVO RIGA SENIOR;"
        Appoggio = Appoggio & "TIPO RIGA ALYANTE;"
        Appoggio = Appoggio & "CODICE ARTICOLO ALYANTE;"
        Appoggio = Appoggio & "CONTO SENIOR CLIENTE;"
        Appoggio = Appoggio & "DESCRIZIONE SENIOR CONTO;"
        Appoggio = Appoggio & "OPZIONE_MG5E;"
        Appoggio = Appoggio & "DESCRIZIONE RIGA;"
        Appoggio = Appoggio & "DO30_UM1;"
        Appoggio = Appoggio & "QUANTITA;"
        Appoggio = Appoggio & "DO30_COLLI;"
        Appoggio = Appoggio & "IMPORTO;"
        Appoggio = Appoggio & "CODICE IVA SENIOR;"
        Appoggio = Appoggio & "DESCRIZIONE IVA;"
        Appoggio = Appoggio & "CODICE CENTRO SERVIZIO SENIOR;"
        Appoggio = Appoggio & "DESCRIZIONE CENTRO SERVIZIO SENIOR;"
        Appoggio = Appoggio & "ANNO COMPETENZA;"
        Appoggio = Appoggio & "MESE COMPETENZA;"
        tw.WriteLine(Appoggio)


        Dim Fnvb6 As New Cls_FunzioniVB6
        Fnvb6.StringaOspiti = Session("DC_OSPITE")
        Fnvb6.StringaTabelle = Session("DC_TABELLE")
        Fnvb6.StringaGenerale = Session("DC_GENERALE")
        Fnvb6.ApriDB()



        Dim CodiceRegistro As String = ""




        CodiceInstallazione = TabellaSocieta.CodiceInstallazione
        CodiceDitta = TabellaSocieta.CodiceDitta


        ProgressivoTesta = 0

        Dim myPOSTreader As OleDbDataReader = cmd.ExecuteReader()
        Do While myPOSTreader.Read
            ProgressivoTesta = ProgressivoTesta + 1

            Dim CausaleContabile As New Cls_CausaleContabile

            CausaleContabile.Codice = campodb(myPOSTreader.Item("CAUSALECONTABILE"))
            CausaleContabile.Leggi(Session("DC_TABELLE"), CausaleContabile.Codice)


            Appoggio = "T" & ";"  'DO_TIPO_RIGA
            Appoggio = Appoggio & ProgressivoTesta & ";"  'DO11_ID
            Appoggio = Appoggio & campodbN(myPOSTreader.Item("NumeroRegistrazione")) & ";" 'DO11_NUMREG_SEN


            Appoggio = Appoggio & CodiceInstallazione & ";" 'DO11_CODICE_INS

            Appoggio = Appoggio & CodiceDitta & ";" 'DO11_DITTA_CG18
            Appoggio = Appoggio & CausaleContabile.CodiceSede & ";" 'CODICE SEDE
            Appoggio = Appoggio & CausaleContabile.RegistroIVA & ";" 'REGISTRO IVA

            If CausaleContabile.TipoDocumento = "NC" Then
                Appoggio = Appoggio & "NC" & ";" 'TIPO DOCUMENTO SENIOR
            Else
                Appoggio = Appoggio & "FT" & ";" 'TIPO DOCUMENTO SENIOR
            End If
            Appoggio = Appoggio & ";" 'CODICE DOCUMENTO ALYANTE

            Appoggio = Appoggio & campodb(myPOSTreader.Item("NumeroDocumento")) & ";" 'DO11_NUMDOC
            Appoggio = Appoggio & campodb(myPOSTreader.Item("DataDocumento")) & ";" 'DO11_DATADOC
            Appoggio = Appoggio & "0" & ";" 'DO11_TIPOCF_CG44 -  0=  CLIENTE


            Dim Cliente As String = ""

            Dim Tipo As String = campodb(myPOSTreader.Item("Tipologia"))
            Dim CodiceOspite As Integer = 0
            Dim CodiceParente As Integer = 0

            Dim CodiceProvincia As String = ""
            Dim CodiceComune As String = ""
            Dim CodiceRegione As String = ""
            Dim MasCliente As String = ""
            Dim ConCliente As String = ""
            Dim SotCliente As String = ""

            Dim cmdRd As New OleDbCommand()
            cmdRd.CommandText = "Select * From MovimentiContabiliRiga Where Numero = ?  And Tipo ='CF'   "
            cmdRd.Connection = cn
            cmdRd.Parameters.AddWithValue("@Numero", campodbN(myPOSTreader.Item("NumeroRegistrazione")))
            Dim MyReadSC As OleDbDataReader = cmdRd.ExecuteReader()
            If MyReadSC.Read Then
                Cliente = campodb(MyReadSC.Item("MastroPartita")) & "." & campodb(MyReadSC.Item("ContoPartita")) & "." & campodb(MyReadSC.Item("SottocontoPartita"))
                MasCliente = campodb(MyReadSC.Item("MastroPartita"))
                ConCliente = campodb(MyReadSC.Item("ContoPartita"))
                SotCliente = campodb(MyReadSC.Item("SottocontoPartita"))
                If Mid(Tipo & Space(10), 1, 1) = " " Then
                    CodiceOspite = Int(campodbN(MyReadSC.Item("SottocontoPartita")) / 100)
                    CodiceParente = campodbN(MyReadSC.Item("SottocontoPartita")) - (CodiceOspite * 100)

                    If CodiceParente = 0 And CodiceOspite > 0 Then
                        Tipo = "O"
                    End If
                    If CodiceParente > 0 And CodiceOspite > 0 Then
                        Tipo = "P"
                    End If
                End If
                If Mid(Tipo & Space(10), 1, 1) = "C" Then
                    CodiceProvincia = Mid(Tipo & Space(10), 2, 3)
                    CodiceComune = Mid(Tipo & Space(10), 5, 3)
                    Tipo = "C"
                End If
                If Mid(Tipo & Space(10), 1, 1) = "J" Then
                    CodiceProvincia = Mid(Tipo & Space(10), 2, 3)
                    CodiceComune = Mid(Tipo & Space(10), 5, 3)
                    Tipo = "J"
                End If
                If Mid(Tipo & Space(10), 1, 1) = "R" Then
                    CodiceRegione = Mid(Tipo & Space(10), 2, 4)
                    Tipo = "R"
                End If

            End If
            MyReadSC.Close()

            Appoggio = Appoggio & "" & ";" 'CLIFOR_CG44 CODICE CLIENTE NON IN USO

            If Tipo = "O" Then
                Appoggio = Appoggio & "P" & ";" ''TIPO ANAGRAFICA
            End If
            If Tipo = "P" Then
                Appoggio = Appoggio & "P" & ";" ''TIPO ANAGRAFICA
            End If
            If Tipo = "C" Then
                Appoggio = Appoggio & "A" & ";" ''TIPO ANAGRAFICA
            End If
            If Tipo = "R" Then
                Appoggio = Appoggio & "A" & ";" ''TIPO ANAGRAFICA
            End If
            If Tipo = "J" Then
                Appoggio = Appoggio & "A" & ";" ''TIPO ANAGRAFICA
            End If




            TabTrascodificaCausale.TIPOTAB = "CC"
            TabTrascodificaCausale.SENIOR = MasCliente & "." & ConCliente
            TabTrascodificaCausale.EXPORT = ""
            TabTrascodificaCausale.Leggi(Session("DC_TABELLE"))

            If TabTrascodificaCausale.EXPORT <> "" Then
                Appoggio = Appoggio & TabTrascodificaCausale.EXPORT & "." & SotCliente & ";" 'DO30_CODIVA_CODICE_SEN
            Else
                Appoggio = Appoggio & Cliente & ";" 'CODICE CLIENTE DI SENIOR
            End If



            Dim DO11_RAGIONESOCIALE As String = ""
            Dim DO11_PIVA As String = ""
            Dim DO11_CF As String = ""
            Dim DO11_INDIRIZZO As String = ""
            Dim DO11_CAP As String = ""
            Dim DO11_CITTA_NOME As String = ""
            Dim DO11_CITTA_ISTAT As String = ""
            Dim DO11_CITTA_CATASTALE As String = ""
            Dim DO11_PROVINCIA As String = ""
            Dim DO11_CONDPAG_CODICE_SEN As String = ""
            Dim DO11_CONDPAG_NOME_SEN As String = ""
            Dim DO11_BANCA_CIN As String = ""
            Dim DO11_BANCA_ABI As String = ""
            Dim DO11_BANCA_CAB As String = ""
            Dim DO11_BANCA_CONTO As String = ""
            Dim DO11_BANCA_IBAN As String = ""
            Dim DO11_BANCA_IDMANDATO As String = ""
            Dim DO11_BANCA_DATAMANDATO As String = ""
            Dim DO11_INTESTATARIOSDD As String = ""
            Dim DO11_INTESTATARIOCFSDD As String = ""

            Dim DO11_COGNOMENOME_OSPITERIFERIMENO As String = ""
            Dim DO11_CODICEFISCALE_OSPITERIFERIMENO As String = ""
            Dim DO11_DATANASCITA_OSPITERIFERIMENO As String = ""

            Dim DO11_COGNOMENOME_DESTINATARIO As String = ""
            Dim DO11_INDIRIZZO_DESTINATARIO As String = ""
            Dim DO11_COMUNE_DESTINATARIO As String = ""
            Dim DO11_PROVINCIA_DESTINATARIO As String = ""
            Dim DO11_COMUNECOD_DESTINATARIO As String = ""
            Dim DO11_CAP_DESTINATARIO As String = ""
            Dim DO11_CITTA_DESTINATARIO As String = ""
            Dim DO11_MAIL_DESTINATARIO As String = ""
            Dim TIPOLOGIAIDDOC As String = "0"
            Dim PRESENZADDT As String

            Dim Raggruppa As String = ""


            Dim AddDescrizione As String


            Dim CIG As String = ""
            Dim IDDocumento As String = ""
            Dim IDDocumentoData As String = ""
            Dim CodiceDestinatario As String = ""
            Dim RiferimentoAmministrazione As String = ""

            AddDescrizione = ""
            If Mid(Tipo & Space(10), 1, 1) = "O" Then
                Dim Ospite As New ClsOspite

                Ospite.CodiceOspite = CodiceOspite
                Ospite.Leggi(Session("DC_OSPITE"), Ospite.CodiceOspite)
                DO11_RAGIONESOCIALE = Ospite.Nome
                DO11_PIVA = ""
                DO11_CF = Ospite.CODICEFISCALE

                Dim VerificaCodiceFiscale As New Cls_CodiceFiscale

                If VerificaCodiceFiscale.Check_CodiceFiscale(DO11_CF) = False Then
                    Dim myrigaerrore As System.Data.DataRow = MyTable.NewRow()
                    myrigaerrore.Item("Numero Registrazione") = campodbN(myPOSTreader.Item("NumeroRegistrazione"))
                    myrigaerrore.Item("Cliente") = DO11_RAGIONESOCIALE
                    myrigaerrore.Item("Segnalazione") = " Codice Fiscale formalmente errato per ospite "
                    MyTable.Rows.Add(myrigaerrore)
                End If

                DO11_INDIRIZZO = Ospite.RESIDENZAINDIRIZZO1
                DO11_CAP = Ospite.RESIDENZACAP1
                Dim DcCom As New ClsComune

                DcCom.Provincia = Ospite.RESIDENZAPROVINCIA1
                DcCom.Comune = Ospite.RESIDENZACOMUNE1
                DcCom.Leggi(Session("DC_OSPITE"))

                DO11_CITTA_NOME = DcCom.Descrizione
                DO11_CITTA_ISTAT = DcCom.Provincia & DcCom.Comune
                DO11_CITTA_CATASTALE = DcCom.CODXCODF

                Dim DcProv As New ClsComune

                DcProv.Provincia = Ospite.RESIDENZAPROVINCIA1
                DcProv.Comune = ""
                DcProv.Leggi(Session("DC_OSPITE"))
                DO11_PROVINCIA = DcProv.CodificaProvincia


                Dim Kl As New Cls_DatiOspiteParenteCentroServizio

                Kl.CodiceOspite = CodiceOspite
                Kl.CodiceParente = 0
                Kl.CentroServizio = campodb(myPOSTreader.Item("CentroServizio"))
                Kl.Leggi(Session("DC_OSPITE"))
                If Kl.ModalitaPagamento <> "" Then
                    Ospite.MODALITAPAGAMENTO = Kl.ModalitaPagamento
                End If


                DO11_CONDPAG_CODICE_SEN = Ospite.MODALITAPAGAMENTO
                Dim MPAg As New ClsModalitaPagamento

                MPAg.Codice = Ospite.MODALITAPAGAMENTO
                MPAg.Leggi(Session("DC_OSPITE"))
                DO11_CONDPAG_NOME_SEN = MPAg.DESCRIZIONE


                Dim ModPag As New Cls_DatiPagamento

                ModPag.CodiceOspite = Ospite.CodiceOspite
                ModPag.CodiceParente = 0
                ModPag.LeggiUltimaDataData(Session("DC_OSPITE"), campodb(myPOSTreader.Item("DataDocumento")))

                DO11_BANCA_CIN = ModPag.Cin
                DO11_BANCA_ABI = ModPag.Abi
                DO11_BANCA_CAB = ModPag.Cab
                DO11_BANCA_CONTO = ModPag.CCBancario
                DO11_BANCA_IBAN = ModPag.CodiceInt & Format(Val(ModPag.NumeroControllo), "00") & ModPag.Cin & Format(Val(ModPag.Abi), "00000") & Format(Val(ModPag.Cab), "00000") & AdattaLunghezzaNumero(ModPag.CCBancario, 12)

                TIPOLOGIAIDDOC = "0"

                DO11_BANCA_IDMANDATO = ModPag.IdMandatoDebitore
                DO11_BANCA_DATAMANDATO = ModPag.DataMandatoDebitore
                DO11_INTESTATARIOSDD = ModPag.IntestatarioConto
                DO11_INTESTATARIOCFSDD = ModPag.IntestatarioCFConto


                DO11_COGNOMENOME_OSPITERIFERIMENO = Ospite.Nome
                DO11_CODICEFISCALE_OSPITERIFERIMENO = Ospite.CODICEFISCALE
                DO11_DATANASCITA_OSPITERIFERIMENO = Ospite.DataNascita

                DO11_COGNOMENOME_DESTINATARIO = Ospite.RecapitoNome
                DO11_INDIRIZZO_DESTINATARIO = Ospite.RecapitoIndirizzo

                Dim MInt As New ClsComune

                MInt.Provincia = Ospite.RecapitoProvincia
                MInt.Comune = Ospite.RecapitoComune
                MInt.Leggi(Session("DC_OSPITE"))

                DO11_COMUNE_DESTINATARIO = MInt.Descrizione
                DO11_COMUNECOD_DESTINATARIO = Ospite.RecapitoProvincia & Ospite.RecapitoComune

                MInt.Provincia = Ospite.RecapitoProvincia
                MInt.Comune = ""
                MInt.Leggi(Session("DC_OSPITE"))
                DO11_PROVINCIA_DESTINATARIO = MInt.CodificaProvincia


                DO11_CAP_DESTINATARIO = Ospite.RESIDENZACAP4
                DO11_MAIL_DESTINATARIO = Ospite.TELEFONO4


                Dim cnOspiti As OleDbConnection

                cnOspiti = New Data.OleDb.OleDbConnection(Session("DC_OSPITE"))

                cnOspiti.Open()

                Dim cmdC As New OleDbCommand()
                cmdC.CommandText = ("select * from NoteFatture where [OspitiParentiComuniRegioni] = 'O'")
                cmdC.Connection = cnOspiti
                Dim RdCom As OleDbDataReader = cmdC.ExecuteReader()
                If RdCom.Read Then
                    If campodb(RdCom.Item("CODICIOSPITI")) = "" Or campodb(RdCom.Item("CODICIOSPITI")).IndexOf(CodiceOspite & ",") >= 0 Then
                        AddDescrizione = campodb(RdCom.Item("NoteUp"))
                    End If
                End If
                RdCom.Close()
                cnOspiti.Close()

            End If

            If Mid(Tipo & Space(10), 1, 1) = "P" Then
                Dim Ospite As New Cls_Parenti

                Ospite.CodiceOspite = CodiceOspite
                Ospite.CodiceParente = CodiceParente
                Ospite.Leggi(Session("DC_OSPITE"), Ospite.CodiceOspite, Ospite.CodiceParente)
                DO11_RAGIONESOCIALE = Ospite.Nome
                DO11_PIVA = ""
                DO11_CF = Ospite.CODICEFISCALE
                DO11_INDIRIZZO = Ospite.RESIDENZAINDIRIZZO1
                DO11_CAP = Ospite.RESIDENZACAP1

                Dim VerificaCodiceFiscale As New Cls_CodiceFiscale

                TIPOLOGIAIDDOC = "0"

                If VerificaCodiceFiscale.Check_CodiceFiscale(DO11_CF) = False Then
                    Dim myrigaerrore As System.Data.DataRow = MyTable.NewRow()
                    myrigaerrore.Item("Numero Registrazione") = campodbN(myPOSTreader.Item("NumeroRegistrazione"))
                    myrigaerrore.Item("Cliente") = DO11_RAGIONESOCIALE
                    myrigaerrore.Item("Segnalazione") = " Codice Fiscale formalmente errato per parente "
                    MyTable.Rows.Add(myrigaerrore)
                End If

                Dim DcCom As New ClsComune

                DcCom.Provincia = Ospite.RESIDENZAPROVINCIA1
                DcCom.Comune = Ospite.RESIDENZACOMUNE1
                DcCom.Leggi(Session("DC_OSPITE"))

                DO11_CITTA_NOME = DcCom.Descrizione
                DO11_CITTA_ISTAT = DcCom.Provincia & DcCom.Comune
                DO11_CITTA_CATASTALE = DcCom.CODXCODF

                Dim DcProv As New ClsComune

                DcProv.Provincia = Ospite.RESIDENZAPROVINCIA1
                DcProv.Comune = ""
                DcProv.Leggi(Session("DC_OSPITE"))
                DO11_PROVINCIA = DcProv.CodificaProvincia
                DO11_CONDPAG_CODICE_SEN = Ospite.MODALITAPAGAMENTO

                Dim Kl As New Cls_DatiOspiteParenteCentroServizio

                Kl.CodiceOspite = CodiceOspite
                Kl.CodiceParente = CodiceParente
                Kl.CentroServizio = campodb(myPOSTreader.Item("CentroServizio"))
                Kl.Leggi(Session("DC_OSPITE"))
                If Kl.ModalitaPagamento <> "" Then
                    Ospite.MODALITAPAGAMENTO = Kl.ModalitaPagamento
                End If

                Dim MPAg As New ClsModalitaPagamento

                MPAg.Codice = Ospite.MODALITAPAGAMENTO
                MPAg.Leggi(Session("DC_OSPITE"))
                DO11_CONDPAG_NOME_SEN = MPAg.DESCRIZIONE

                Dim ModPag As New Cls_DatiPagamento

                ModPag.CodiceOspite = Ospite.CodiceOspite
                ModPag.CodiceParente = Ospite.CodiceParente
                ModPag.LeggiUltimaDataData(Session("DC_OSPITE"), campodb(myPOSTreader.Item("DataDocumento")))

                DO11_BANCA_CIN = ModPag.Cin
                DO11_BANCA_ABI = ModPag.Abi
                DO11_BANCA_CAB = ModPag.Cab
                DO11_BANCA_CONTO = ModPag.CCBancario
                DO11_BANCA_IBAN = ModPag.CodiceInt & Format(Val(ModPag.NumeroControllo), "00") & ModPag.Cin & Format(Val(ModPag.Abi), "00000") & Format(Val(ModPag.Cab), "00000") & AdattaLunghezzaNumero(ModPag.CCBancario, 12)

                DO11_BANCA_IDMANDATO = ModPag.IdMandatoDebitore
                DO11_BANCA_DATAMANDATO = ModPag.DataMandatoDebitore
                DO11_INTESTATARIOSDD = ModPag.IntestatarioConto
                DO11_INTESTATARIOCFSDD = ModPag.IntestatarioCFConto



                Dim OspiteReale As New ClsOspite

                OspiteReale.CodiceOspite = Ospite.CodiceOspite
                OspiteReale.Leggi(Session("DC_OSPITE"), OspiteReale.CodiceOspite)


                DO11_COGNOMENOME_OSPITERIFERIMENO = OspiteReale.Nome
                DO11_CODICEFISCALE_OSPITERIFERIMENO = OspiteReale.CODICEFISCALE
                DO11_DATANASCITA_OSPITERIFERIMENO = OspiteReale.DataNascita

                DO11_COGNOMENOME_DESTINATARIO = Ospite.RECAPITONOME
                DO11_INDIRIZZO_DESTINATARIO = Ospite.RECAPITOINDIRIZZO


                Dim MInt As New ClsComune

                MInt.Provincia = Ospite.RECAPITOPROVINCIA
                MInt.Comune = Ospite.RECAPITOCOMUNE
                MInt.Leggi(Session("DC_OSPITE"))

                DO11_COMUNE_DESTINATARIO = MInt.Descrizione
                DO11_COMUNECOD_DESTINATARIO = Ospite.RECAPITOPROVINCIA & Ospite.RECAPITOCOMUNE

                MInt.Provincia = Ospite.RECAPITOPROVINCIA
                MInt.Comune = ""
                MInt.Leggi(Session("DC_OSPITE"))
                DO11_PROVINCIA_DESTINATARIO = MInt.CodificaProvincia


                DO11_CAP_DESTINATARIO = Ospite.RESIDENZACAP4
                DO11_MAIL_DESTINATARIO = Ospite.Telefono4

                Dim cnOspiti As OleDbConnection

                cnOspiti = New Data.OleDb.OleDbConnection(Session("DC_OSPITE"))

                cnOspiti.Open()

                Dim cmdC As New OleDbCommand()
                cmdC.CommandText = ("select * from NoteFatture where [OspitiParentiComuniRegioni] = 'O'")
                cmdC.Connection = cnOspiti
                Dim RdCom As OleDbDataReader = cmdC.ExecuteReader()
                If RdCom.Read Then
                    If campodb(RdCom.Item("CODICIOSPITI")) = "" Or campodb(RdCom.Item("CODICIOSPITI")).IndexOf(CodiceOspite & ",") >= 0 Then
                        AddDescrizione = campodb(RdCom.Item("NoteUp"))
                    End If
                End If
                RdCom.Close()
                cnOspiti.Close()

            End If
            If Mid(Tipo & Space(10), 1, 1) = "C" Then
                Dim Ospite As New ClsComune

                Ospite.Provincia = CodiceProvincia
                Ospite.Comune = CodiceComune
                Ospite.Leggi(Session("DC_OSPITE"))
                DO11_RAGIONESOCIALE = Ospite.Descrizione
                DO11_PIVA = AdattaLunghezzaNumero(Ospite.PartitaIVA, 11)
                DO11_CF = Ospite.CodiceFiscale
                DO11_INDIRIZZO = Ospite.RESIDENZAINDIRIZZO1
                DO11_CAP = Ospite.RESIDENZACAP1


                If Ospite.RaggruppaInExport = 1 Then
                    Raggruppa = "S"
                End If
                If Ospite.RaggruppaInExport = 2 Then
                    Raggruppa = "D"
                End If
                If Ospite.RaggruppaInExport = 0 Then
                    Raggruppa = ""
                End If

                '0 = non gestito
                '1 = ordine
                '2 = contratto
                '3 = convenzione"

                TIPOLOGIAIDDOC = "0"
                If Ospite.EGo = "O" Then
                    TIPOLOGIAIDDOC = "1"
                End If
                If Ospite.EGo = "C" Then
                    TIPOLOGIAIDDOC = "2"
                End If
                If Ospite.EGo = "V" Then
                    TIPOLOGIAIDDOC = "3"
                End If

                Dim VerificaPIVA As New Cls_CodiceFiscale

                If VerificaPIVA.CheckPartitaIva(AdattaLunghezzaNumero(DO11_PIVA, 11)) = False Then
                    Dim myrigaerrore As System.Data.DataRow = MyTable.NewRow()
                    myrigaerrore.Item("Numero Registrazione") = campodbN(myPOSTreader.Item("NumeroRegistrazione"))
                    myrigaerrore.Item("Cliente") = DO11_RAGIONESOCIALE
                    myrigaerrore.Item("Segnalazione") = " Partita Iva formalmente errato per ente  " & DO11_PIVA
                    MyTable.Rows.Add(myrigaerrore)
                End If


                Dim DcCom As New ClsComune

                DcCom.Provincia = Ospite.RESIDENZAPROVINCIA1
                DcCom.Comune = Ospite.RESIDENZACOMUNE1
                DcCom.Leggi(Session("DC_OSPITE"))

                DO11_CITTA_NOME = DcCom.Descrizione
                DO11_CITTA_ISTAT = DcCom.Provincia & DcCom.Comune
                DO11_CITTA_CATASTALE = DcCom.CODXCODF

                Dim DcProv As New ClsComune

                DcProv.Provincia = Ospite.RESIDENZAPROVINCIA1
                DcProv.Comune = ""
                DcProv.Leggi(Session("DC_OSPITE"))
                DO11_PROVINCIA = DcProv.CodificaProvincia
                DO11_CONDPAG_CODICE_SEN = Ospite.ModalitaPagamento
                Dim MPAg As New ClsModalitaPagamento

                MPAg.Codice = Ospite.ModalitaPagamento
                MPAg.Leggi(Session("DC_OSPITE"))
                DO11_CONDPAG_NOME_SEN = MPAg.DESCRIZIONE



                AddDescrizione = Trim(Fnvb6.LeggiNote(campodb(myPOSTreader.Item("CentroServizio")), CodiceProvincia, CodiceComune, "", True, campodb(myPOSTreader.Item("NumeroRegistrazione")), 0, Session("DC_OSPITE")))

                CIG = Ospite.CodiceCig
                IDDocumento = Ospite.IdDocumento
                IDDocumentoData = Ospite.IdDocumentoData
                CodiceDestinatario = Ospite.CodiceDestinatario
                RiferimentoAmministrazione = Ospite.RiferimentoAmministrazione

                If Ospite.NumeroFatturaDDT = 0 Then
                    PRESENZADDT = "S"
                Else
                    PRESENZADDT = ""
                End If
            End If
            If Mid(Tipo & Space(10), 1, 1) = "J" Then
                Dim Ospite As New ClsComune

                Ospite.Provincia = CodiceProvincia
                Ospite.Comune = CodiceComune
                Ospite.Leggi(Session("DC_OSPITE"))
                DO11_RAGIONESOCIALE = Ospite.Descrizione
                DO11_PIVA = AdattaLunghezzaNumero(Ospite.PartitaIVA, 11)
                DO11_CF = Ospite.CodiceFiscale
                DO11_INDIRIZZO = Ospite.RESIDENZAINDIRIZZO1
                DO11_CAP = Ospite.RESIDENZACAP1


                If Ospite.RaggruppaInExport = 1 Then
                    Raggruppa = "S"
                End If
                If Ospite.RaggruppaInExport = 2 Then
                    Raggruppa = "D"
                End If
                If Ospite.RaggruppaInExport = 0 Then
                    Raggruppa = ""
                End If

                TIPOLOGIAIDDOC = "0"
                If Ospite.EGo = "O" Then
                    TIPOLOGIAIDDOC = "1"
                End If
                If Ospite.EGo = "C" Then
                    TIPOLOGIAIDDOC = "2"
                End If
                If Ospite.EGo = "V" Then
                    TIPOLOGIAIDDOC = "3"
                End If

                Dim VerificaPIVA As New Cls_CodiceFiscale


                If VerificaPIVA.CheckPartitaIva(AdattaLunghezzaNumero(DO11_CF, 11)) = False Then
                    Dim myrigaerrore As System.Data.DataRow = MyTable.NewRow()
                    myrigaerrore.Item("Numero Registrazione") = campodbN(myPOSTreader.Item("NumeroRegistrazione"))
                    myrigaerrore.Item("Cliente") = DO11_RAGIONESOCIALE
                    myrigaerrore.Item("Segnalazione") = " Partita Iva formalmente errato per ente  " & DO11_PIVA
                    MyTable.Rows.Add(myrigaerrore)
                End If


                Dim DcCom As New ClsComune

                DcCom.Provincia = Ospite.RESIDENZAPROVINCIA1
                DcCom.Comune = Ospite.RESIDENZACOMUNE1
                DcCom.Leggi(Session("DC_OSPITE"))

                DO11_CITTA_NOME = DcCom.Descrizione
                DO11_CITTA_ISTAT = DcCom.Provincia & DcCom.Comune
                DO11_CITTA_CATASTALE = DcCom.CODXCODF


                Dim DcProv As New ClsComune

                DcProv.Provincia = Ospite.RESIDENZAPROVINCIA1
                DcProv.Comune = ""
                DcProv.Leggi(Session("DC_OSPITE"))
                DO11_PROVINCIA = DcProv.CodificaProvincia
                DO11_CONDPAG_CODICE_SEN = Ospite.ModalitaPagamento
                Dim MPAg As New ClsModalitaPagamento

                MPAg.Codice = Ospite.ModalitaPagamento
                MPAg.Leggi(Session("DC_OSPITE"))
                DO11_CONDPAG_NOME_SEN = MPAg.DESCRIZIONE

                AddDescrizione = Trim(Fnvb6.LeggiNote(campodb(myPOSTreader.Item("CentroServizio")), CodiceProvincia, CodiceComune, "", True, campodb(myPOSTreader.Item("NumeroRegistrazione")), 0, Session("DC_OSPITE")))

                CIG = Ospite.CodiceCig
                IDDocumento = Ospite.IdDocumento
                IDDocumentoData = Ospite.IdDocumentoData
                CodiceDestinatario = Ospite.CodiceDestinatario
                RiferimentoAmministrazione = Ospite.RiferimentoAmministrazione
                If Ospite.NumeroFatturaDDT = 0 Then
                    PRESENZADDT = "S"
                Else
                    PRESENZADDT = ""
                End If
            End If
            If Mid(Tipo & Space(10), 1, 1) = "R" Then
                Dim Ospite As New ClsUSL

                Ospite.CodiceRegione = CodiceRegione
                Ospite.Leggi(Session("DC_OSPITE"))
                DO11_RAGIONESOCIALE = Ospite.Nome
                DO11_PIVA = AdattaLunghezzaNumero(Ospite.PARTITAIVA, 11)
                DO11_CF = Ospite.CodiceFiscale
                DO11_INDIRIZZO = Ospite.RESIDENZAINDIRIZZO1
                DO11_CAP = Ospite.RESIDENZACAP1


                If Ospite.RaggruppaInExport = 1 Then
                    Raggruppa = "S"
                End If
                If Ospite.RaggruppaInExport = 2 Then
                    Raggruppa = "D"
                End If
                If Ospite.RaggruppaInExport = 0 Then
                    Raggruppa = ""
                End If


                TIPOLOGIAIDDOC = "0"
                If Ospite.EGo = "O" Then
                    TIPOLOGIAIDDOC = "1"
                End If
                If Ospite.EGo = "C" Then
                    TIPOLOGIAIDDOC = "2"
                End If
                If Ospite.EGo = "V" Then
                    TIPOLOGIAIDDOC = "3"
                End If

                Dim VerificaPIVA As New Cls_CodiceFiscale


                If VerificaPIVA.CheckPartitaIva(AdattaLunghezzaNumero(DO11_CF, 11)) = False Then
                    Dim myrigaerrore As System.Data.DataRow = MyTable.NewRow()
                    myrigaerrore.Item("Numero Registrazione") = campodbN(myPOSTreader.Item("NumeroRegistrazione"))
                    myrigaerrore.Item("Cliente") = DO11_RAGIONESOCIALE
                    myrigaerrore.Item("Segnalazione") = " Partita Iva formalmente errato per ente  " & DO11_CF
                    MyTable.Rows.Add(myrigaerrore)
                End If


                Dim DcCom As New ClsComune

                DcCom.Provincia = Ospite.RESIDENZAPROVINCIA1
                DcCom.Comune = Ospite.RESIDENZACOMUNE1
                DcCom.Leggi(Session("DC_OSPITE"))

                DO11_CITTA_NOME = DcCom.Descrizione
                DO11_CITTA_ISTAT = DcCom.Provincia & DcCom.Comune
                DO11_CITTA_CATASTALE = DcCom.CODXCODF

                Dim DcProv As New ClsComune

                DcProv.Provincia = Ospite.RESIDENZAPROVINCIA1
                DcProv.Comune = ""
                DcProv.Leggi(Session("DC_OSPITE"))
                DO11_PROVINCIA = DcProv.CodificaProvincia
                DO11_CONDPAG_CODICE_SEN = Ospite.ModalitaPagamento
                Dim MPAg As New ClsModalitaPagamento

                MPAg.Codice = Ospite.ModalitaPagamento
                MPAg.Leggi(Session("DC_OSPITE"))
                DO11_CONDPAG_NOME_SEN = MPAg.DESCRIZIONE

                CIG = Ospite.CodiceCig
                IDDocumento = Ospite.IdDocumento
                REM IDDocumentoData = Format(Ospite.IdDocumento, "dd/MM/yyyy")
                CodiceDestinatario = Ospite.CodiceDestinatario
                RiferimentoAmministrazione = Ospite.RiferimentoAmministrazione

                AddDescrizione = Trim(Fnvb6.LeggiNote(campodb(myPOSTreader.Item("CentroServizio")), "", "", CodiceRegione, True, campodb(myPOSTreader.Item("NumeroRegistrazione")), 0, Session("DC_OSPITE")))

                If Ospite.NumeroFatturaDDT = 0 Then
                    PRESENZADDT = "S"
                Else
                    PRESENZADDT = ""
                End If
            End If
            If campodbN(myPOSTreader.Item("IdProgettoODV")) > 0 Then
                Dim Appalto As New Cls_AppaltiAnagrafica

                Appalto.Id = campodbN(myPOSTreader.Item("IdProgettoODV"))
                Appalto.Leggi(Session("DC_TABELLE"))


                CIG = Appalto.Cig
            End If


            Dim M As New Cls_TipoPagamento

            M.Codice = campodb(myPOSTreader.Item("CodicePagamento"))
            M.Leggi(Session("DC_TABELLE"))
            If M.Descrizione <> "" Then
                DO11_CONDPAG_CODICE_SEN = M.Codice
                DO11_CONDPAG_NOME_SEN = M.Descrizione
            End If


            TabTrascodificaCausale.TIPOTAB = "MP"
            TabTrascodificaCausale.SENIOR = DO11_CONDPAG_CODICE_SEN
            TabTrascodificaCausale.EXPORT = ""
            TabTrascodificaCausale.Leggi(Session("DC_TABELLE"))

            If TabTrascodificaCausale.EXPORT <> "" Then
                DO11_CONDPAG_CODICE_SEN = TabTrascodificaCausale.EXPORT
            End If

            If DO11_CONDPAG_CODICE_SEN = "" Or DO11_CONDPAG_NOME_SEN = "" Then
                Dim myrigaerrore As System.Data.DataRow = MyTable.NewRow()
                myrigaerrore.Item("Numero Registrazione") = campodbN(myPOSTreader.Item("NumeroRegistrazione"))
                myrigaerrore.Item("Cliente") = DO11_RAGIONESOCIALE
                myrigaerrore.Item("Segnalazione") = " Modalita pagamento non inserita "
                MyTable.Rows.Add(myrigaerrore)
            End If

            If DO11_CONDPAG_NOME_SEN <> "" Then
                If DO11_CONDPAG_NOME_SEN.IndexOf("SEPA") >= 0 Or DO11_CONDPAG_NOME_SEN.IndexOf("RID") >= 0 Or DO11_CONDPAG_NOME_SEN.IndexOf("R.I.D") >= 0 Then
                    Dim MyCheckIban As New CheckIban

                    If Mid(DO11_BANCA_IBAN & Space(10), 1, 2) = "IT" Then
                        If MyCheckIban.ValidateIBAN(DO11_BANCA_IBAN) <> "Codice Corretto" Then

                            Dim myrigaerrore As System.Data.DataRow = MyTable.NewRow()
                            myrigaerrore.Item("Numero Registrazione") = campodbN(myPOSTreader.Item("NumeroRegistrazione"))
                            myrigaerrore.Item("Cliente") = DO11_RAGIONESOCIALE
                            myrigaerrore.Item("Segnalazione") = " Codice Iban formalmente errato  - " & MyCheckIban.ValidateIBAN(DO11_BANCA_IBAN)
                            MyTable.Rows.Add(myrigaerrore)
                        End If
                    End If


                    If DO11_BANCA_IDMANDATO = "" Then
                        Dim myrigaerrore As System.Data.DataRow = MyTable.NewRow()
                        myrigaerrore.Item("Numero Registrazione") = campodbN(myPOSTreader.Item("NumeroRegistrazione"))
                        myrigaerrore.Item("Cliente") = DO11_RAGIONESOCIALE
                        myrigaerrore.Item("Segnalazione") = " Specificare ID MANDATO  "
                        MyTable.Rows.Add(myrigaerrore)
                    End If


                    If Not IsDate(DO11_BANCA_DATAMANDATO) Or DO11_BANCA_DATAMANDATO = "00:00:00" Or DO11_BANCA_DATAMANDATO = "00.00.00" Then
                        Dim myrigaerrore As System.Data.DataRow = MyTable.NewRow()
                        myrigaerrore.Item("Numero Registrazione") = campodbN(myPOSTreader.Item("NumeroRegistrazione"))
                        myrigaerrore.Item("Cliente") = DO11_RAGIONESOCIALE
                        myrigaerrore.Item("Segnalazione") = " Specificare DATA MANDATO "
                        MyTable.Rows.Add(myrigaerrore)
                    End If
                End If
            End If

            If Session("DC_OSPITE").ToString.ToUpper.IndexOf("RISO") >= 0 Then

                DO11_CONDPAG_CODICE_SEN = "O"
                DO11_CONDPAG_NOME_SEN = "bonif.banc. a vista"
                If Mid(Tipo & Space(10), 1, 1) = "C" Or Mid(Tipo & Space(10), 1, 1) = "R" Or Mid(Tipo & Space(10), 1, 1) = "J" Then
                    DO11_CONDPAG_CODICE_SEN = "R"
                    DO11_CONDPAG_NOME_SEN = "bonif.bamc. 30 gg.df."
                End If
            End If


            If DO11_CITTA_CATASTALE = "" And DO11_CITTA_ISTAT <> "" Then

                Dim myrigaerrore As System.Data.DataRow = MyTable.NewRow()
                myrigaerrore.Item("Numero Registrazione") = campodbN(myPOSTreader.Item("NumeroRegistrazione"))
                myrigaerrore.Item("Cliente") = DO11_RAGIONESOCIALE
                myrigaerrore.Item("Segnalazione") = " Codice catastale non indicato per comune " & DO11_CITTA_NOME & " " & DO11_CITTA_ISTAT
                MyTable.Rows.Add(myrigaerrore)
            End If

            Appoggio = Appoggio & DO11_RAGIONESOCIALE.Replace("  ", " ") & ";" 'RAGIONE SOCIALE
            Appoggio = Appoggio & DO11_PIVA & ";" 'PARTITA IVA
            Appoggio = Appoggio & DO11_CF & ";" 'CODICE FISCALE
            Appoggio = Appoggio & DO11_INDIRIZZO & ";" 'INDIRIZZO (VIA)
            Appoggio = Appoggio & DO11_CAP & ";" 'CAP
            Appoggio = Appoggio & DO11_CITTA_NOME & ";" ' COMUNE NOME
            Appoggio = Appoggio & DO11_CITTA_ISTAT & ";" ' CITTA ISTAT

            Appoggio = Appoggio & DO11_CITTA_CATASTALE & ";" ' CITTA ISTAT
            Appoggio = Appoggio & DO11_PROVINCIA & ";" ' PROVINCIA

            If CodiceInstallazione = "07" Then
                If DO11_CONDPAG_CODICE_SEN = "04" Then
                    DO11_CONDPAG_CODICE_SEN = "05" 'verifica per incongruenza med servrice
                End If
            End If

            Appoggio = Appoggio & DO11_CONDPAG_CODICE_SEN & ";" 'MODALITA PAGAMENTO
            Appoggio = Appoggio & DO11_CONDPAG_NOME_SEN & ";" 'DECODIFICA MODALITA PAGAMENTO

            Appoggio = Appoggio & DO11_BANCA_CIN & ";" 'BANCA CIN
            Appoggio = Appoggio & DO11_BANCA_ABI & ";" 'BANCA ABI
            Appoggio = Appoggio & DO11_BANCA_CAB & ";" 'BANCA CAB
            Appoggio = Appoggio & DO11_BANCA_CONTO & ";" 'BANCA CONTO
            Appoggio = Appoggio & DO11_BANCA_IBAN & ";" 'BANCA IBAN
            Appoggio = Appoggio & DO11_BANCA_IDMANDATO & ";" 'ID MANDATO
            Appoggio = Appoggio & DO11_BANCA_DATAMANDATO & ";" 'DATA MANDATO


            Appoggio = Appoggio & CIG & ";" 'CIG
            Appoggio = Appoggio & TIPOLOGIAIDDOC & ";" 'TIPOLOGIA IDDOC DA GESTIRE
            Appoggio = Appoggio & IDDocumento & ";" 'IDDOCUMENTO (FATTURA ELETTRONICA)
            Appoggio = Appoggio & IDDocumentoData & ";" 'DATA DOCUMENTO (FATTURA ELETTRONICA)
            Appoggio = Appoggio & CodiceDestinatario & ";" ' CODICE DESTINATARIO (FATTURA ELETTRONICA)
            Appoggio = Appoggio & RiferimentoAmministrazione & ";" 'RIFERIMENTO AMMINISTRAZIONE (FATTURA ELETTRONICA)
            Appoggio = Appoggio & PRESENZADDT & ";" 'PRESENZA DDT: FLAG si/no nel campo DDT inserire numero e data documento

            Appoggio = Appoggio & DO11_INTESTATARIOSDD & ";" ' INTESTATARIO SDD
            Appoggio = Appoggio & DO11_INTESTATARIOCFSDD & ";" ' CODICE FISCALE INTESTATARIO SDD

            If DO11_COGNOMENOME_OSPITERIFERIMENO = "" Then
                Appoggio = Appoggio & "" & ";" 'NOME OSPITE RIFERIMENTO
            Else
                Appoggio = Appoggio & DO11_COGNOMENOME_OSPITERIFERIMENO.Replace("  ", " ") & ";" 'NOME OSPITE RIFERIMENTO
            End If

            Appoggio = Appoggio & DO11_CODICEFISCALE_OSPITERIFERIMENO & ";" ' CODICE FISCALE OSPITE RIFERIMENTO
            Appoggio = Appoggio & DO11_DATANASCITA_OSPITERIFERIMENO & ";" 'DATA NASCITA OSPITE RIFERIEMTNO


            If DO11_COGNOMENOME_DESTINATARIO = "" Then
                Appoggio = Appoggio & "" & ";" 'NOME DESTINATARIO FATTURA (CORRISPONDENZA)
            Else
                Appoggio = Appoggio & DO11_COGNOMENOME_DESTINATARIO.Replace("  ", " ") & ";" 'NOME DESTINATARIO FATTURA (CORRISPONDENZA)
            End If

            Appoggio = Appoggio & DO11_INDIRIZZO_DESTINATARIO & ";" 'INDIRIZZO DESTINATARIO  (CORRISPONDENZA)


            If Trim(DO11_PROVINCIA_DESTINATARIO) <> "" Then
                Appoggio = Appoggio & UCase(DO11_CAP_DESTINATARIO & " " & DO11_COMUNE_DESTINATARIO & " (" & DO11_PROVINCIA_DESTINATARIO & ")") & ";" 'CAP COMUNE PROVINCIA  (CORRISPONDENZA)
            Else
                Appoggio = Appoggio & UCase(DO11_CAP_DESTINATARIO & " " & DO11_COMUNE_DESTINATARIO) & ";"
            End If


            Appoggio = Appoggio & DO11_MAIL_DESTINATARIO & ";" 'MAIL DESTINATARIO  (CORRISPONDENZA)


            Appoggio = Appoggio & AddDescrizione & ";" 'DESCRZIONE (NOTE)


            If Session("DC_OSPITE").ToString.ToUpper.IndexOf("Lebetulle".ToUpper) >= 0 Then

                If Mid(Tipo & Space(10), 1, 1) = "O" Or Mid(Tipo & Space(10), 1, 1) = "P" Then

                    Dim MR As New Cls_rettatotale

                    MR.CODICEOSPITE = CodiceOspite
                    MR.UltimaData(Session("DC_OSPITE"), CodiceOspite, myPOSTreader.Item("CentroServizio"))

                    Dim GiorniPresenza As Integer
                    'RigaDaCausale = 3 And MoveFromDbWC(MyRs, "MastroPartita") = 58 And MoveFromDbWC(MyRs, "ContoPartita") = 20 And MoveFromDbWC(MyRs, "SottocontoPartita") = 2 

                    Dim cmdRd1 As New OleDbCommand()
                    cmdRd1.CommandText = "Select sum(Quantita) As Giorni From MovimentiContabiliRiga Where RigaDaCausale = 3  And (Not Descrizione like '%Sconto%' Or Descrizione is Null)  And SottocontoPartita <> 2 And Numero = " & campodbN(myPOSTreader.Item("NumeroRegistrazione"))
                    cmdRd1.Connection = cn
                    cmdRd1.Parameters.AddWithValue("@Numero", campodbN(myPOSTreader.Item("NumeroRegistrazione")))
                    Dim MyReadSC1 As OleDbDataReader = cmdRd1.ExecuteReader()
                    If MyReadSC1.Read Then
                        GiorniPresenza = campodbN(MyReadSC1.Item("Giorni"))
                    End If
                    MyReadSC1.Close()

                    If myPOSTreader.Item("CentroServizio") = "CD" Then
                    Else
                        Dim MeseComp As Integer = campodbN(myPOSTreader.Item("MeseCompetenza"))
                        Dim AnnoComp As Integer = campodbN(myPOSTreader.Item("AnnoCompetenza"))

                        If MeseComp < 12 Then
                            MeseComp = MeseComp + 1
                        Else
                            MeseComp = 1
                            AnnoComp = AnnoComp + 1
                        End If

                        Dim UltLiv As New Cls_rettatotale

                        UltLiv.CENTROSERVIZIO = campodb(myPOSTreader.Item("CentroServizio"))
                        UltLiv.CODICEOSPITE = CodiceOspite
                        UltLiv.UltimaData(Session("DC_OSPITE"), UltLiv.CODICEOSPITE, UltLiv.CENTROSERVIZIO)
                        Dim Xs As New Cls_TipoRetta

                        Xs.Tipo = UltLiv.TipoRetta
                        Xs.Leggi(Session("DC_OSPITE"), Xs.Tipo)
                        If Xs.Descrizione = "PROVVISORIO" Then
                            Dim UltAcc As New Cls_Movimenti

                            UltAcc.CENTROSERVIZIO = UltLiv.CENTROSERVIZIO
                            UltAcc.CodiceOspite = UltLiv.CODICEOSPITE
                            UltAcc.UltimaDataAccoglimento(Session("DC_OSPITE"))

                            Dim UltUsc As New Cls_Movimenti

                            UltUsc.CENTROSERVIZIO = UltLiv.CENTROSERVIZIO
                            UltUsc.CodiceOspite = UltLiv.CODICEOSPITE
                            UltUsc.UltimaDataUscitaDefinitiva(Session("DC_OSPITE"))

                            GiorniPresenza = DateDiff(DateInterval.Day, UltAcc.Data, UltUsc.Data)
                            If campodb(myPOSTreader.Item("CentroServizio")) = "NA" Then
                                Appoggio = Appoggio & Format(GiorniPresenza * 7.24, "0.00") & ";" ' EXTRA1
                            Else
                                If GiorniPresenza > 0 Then
                                    Appoggio = Appoggio & Format(GiorniPresenza * 14.14, "0.00") & ";"  ' EXTRA1
                                Else
                                    Appoggio = Appoggio & ";"  ' EXTRA1
                                End If
                            End If
                        Else
                            If campodb(myPOSTreader.Item("CentroServizio")) = "NA" Then
                                If GiorniPresenza = GiorniMese(MeseComp, AnnoComp) Then
                                    Appoggio = Appoggio & "220,00" & ";"  ' EXTRA1
                                Else
                                    If GiorniPresenza > 0 Then
                                        Appoggio = Appoggio & Format(GiorniPresenza * 7.24, "0.00") & ";"  ' EXTRA1
                                    Else
                                        Appoggio = Appoggio & ";"  ' EXTRA1
                                    End If
                                End If
                            Else
                                If GiorniPresenza = GiorniMese(MeseComp, AnnoComp) Then
                                    Appoggio = Appoggio & "430,00" & ";"  ' EXTRA1
                                Else
                                    If GiorniPresenza > 0 Then
                                        Appoggio = Appoggio & Format(GiorniPresenza * 14.14, "0.00") & ";"  ' EXTRA1
                                    Else
                                        Appoggio = Appoggio & ";"  ' EXTRA1
                                    End If
                                End If
                            End If
                        End If
                    End If
                End If
            Else
                Appoggio = Appoggio & ";"  ' EXTRA1
            End If

            Appoggio = Appoggio & ";"  ' EXTRA2
            Appoggio = Appoggio & ";"  ' EXTRA3
            Appoggio = Appoggio & ";"  ' EXTRA4
            Appoggio = Appoggio & ";"  ' EXTRA5
            Appoggio = Appoggio & ";"  ' EXTRA6
            Appoggio = Appoggio & ";"  ' EXTRA7
            Appoggio = Appoggio & ";"  ' EXTRA8
            Appoggio = Appoggio & ";"  ' EXTRA9
            Appoggio = Appoggio & ";"  ' EXTRA10
            Appoggio = Appoggio & ";"  ' EXTRA11



            tw.WriteLine(Appoggio.Replace(vbNewLine, " "))

            Dim Progressivo As Integer = 0
            Dim Entrato As Boolean = False


            Dim cmdRiga As New OleDbCommand()


            If Raggruppa = "S" Or Raggruppa = "D" Then
                cmdRiga.CommandText = "Select MastroPartita,ContoPartita,SottocontoPartita,DareAvere,Descrizione,CodiceIVA,RigaDaCausale,sum(Importo) As TImporto,sum(Quantita) As TQuantita From MovimentiContabiliRiga Where Numero = ?  And (Tipo Is Null Or Tipo <> 'IV') And (Importo > 0 OR (Importo = 0 And Imponibile =0))  Group by  MastroPartita,ContoPartita,SottocontoPartita,DareAvere,CodiceIVA,Descrizione,RigaDaCausale Order by RigaDaCausale"

                cmdRiga.Connection = cn
                cmdRiga.Parameters.AddWithValue("@Numero", campodbN(myPOSTreader.Item("NumeroRegistrazione")))
                Dim MyRiga As OleDbDataReader = cmdRiga.ExecuteReader()
                Do While MyRiga.Read
                    Progressivo = Progressivo + 1
                    ProgressivoAssoluto = ProgressivoAssoluto + 1
                    AppoggioRiga = ""

                    If campodbN(MyRiga.Item("RigaDaCausale")) = 1 Then
                        AppoggioRiga = "P" & ";"  'DO_TIPO_RIGA
                    Else
                        AppoggioRiga = "R" & ";"  'DO_TIPO_RIGA
                    End If

                    AppoggioRiga = AppoggioRiga & ProgressivoAssoluto & ";" 'progressivo assoluto nel file

                    AppoggioRiga = AppoggioRiga & 1 & ";" 'id riga di senior

                    AppoggioRiga = AppoggioRiga & ProgressivoTesta & ";" 'assoluto nel file

                    AppoggioRiga = AppoggioRiga & CodiceInstallazione & ";" 'installazione dl software senior

                    AppoggioRiga = AppoggioRiga & CodiceDitta & ";" ' 'il codice ditta in alyante

                    AppoggioRiga = AppoggioRiga & CausaleContabile.CodiceSede & ";" ' il codice sede in alyante
                    AppoggioRiga = AppoggioRiga & CausaleContabile.RegistroIVA & ";" ' il codice sede in alyante

                    AppoggioRiga = AppoggioRiga & Progressivo & ";" 'progressivo riga per ogni registrazione

                    AppoggioRiga = AppoggioRiga & "1" & ";" 'TIPO RIGA ALYANTE Valore Fisso 1


                    AppoggioRiga = AppoggioRiga & "" & ";" 'CODICE ARTICOLO ALYANTE


                    TabTrascodificaCausale.TIPOTAB = "CC"
                    TabTrascodificaCausale.SENIOR = campodbN(MyRiga.Item("MastroPartita")) & "." & campodbN(MyRiga.Item("ContoPartita"))
                    TabTrascodificaCausale.EXPORT = ""
                    TabTrascodificaCausale.Leggi(Session("DC_TABELLE"))

                    If TabTrascodificaCausale.EXPORT = "" Then
                        AppoggioRiga = AppoggioRiga & campodbN(MyRiga.Item("MastroPartita")) & "." & campodbN(MyRiga.Item("ContoPartita")) & "." & campodbN(MyRiga.Item("SottocontoPartita")) & ";" 'CONTO SENIOR CLIENTE
                    Else
                        AppoggioRiga = AppoggioRiga & TabTrascodificaCausale.EXPORT & "." & campodbN(MyRiga.Item("SottocontoPartita")) & ";" 'CONTO SENIOR CLIENTE
                    End If
                    

                    Dim DecConto As New Cls_Pianodeiconti

                    DecConto.Mastro = campodbN(MyRiga.Item("MastroPartita"))
                    DecConto.Conto = campodbN(MyRiga.Item("ContoPartita"))
                    DecConto.Sottoconto = campodbN(MyRiga.Item("SottocontoPartita"))
                    DecConto.Decodfica(Session("DC_GENERALE"))
                    AppoggioRiga = AppoggioRiga & DecConto.Descrizione.Replace(";", " ") 'DESCRIZIONE CONTO SENIRO

                    AppoggioRiga = AppoggioRiga & "" & ";" ' OPZIONE_MG5E vuoto





                    Dim Riferimento As String = ""
                    Dim DescrizioneAppoggio As String = ""

                    AppoggioRiga = AppoggioRiga & ";"

                    If Request.Item("APPALTI") = "SI" Then
                        DescrizioneAppoggio = campodb(MyRiga.Item("Descrizione"))
                    Else
                        If Raggruppa = "D" Then
                            DescrizioneAppoggio = campodb(MyRiga.Item("Descrizione"))
                        Else
                            If campodbN(MyRiga.Item("TQuantita")) > 0 Then
                                DescrizioneAppoggio = campodb(MyRiga.Item("Descrizione")) & " Giorni " & campodbN(MyRiga.Item("TQuantita"))
                            Else
                                DescrizioneAppoggio = campodb(MyRiga.Item("Descrizione"))
                            End If
                        End If
                    End If

                    AppoggioRiga = AppoggioRiga & DescrizioneAppoggio.Replace(";", "").Replace(vbNewLine, " ") & ";" 'DESCRIZIONE SENIOR CONTO




                    AppoggioRiga = AppoggioRiga & "" & ";" 'DO30_UM1 UNITA MISURA
                    If Request.Item("APPALTI") = "SI" Then
                        AppoggioRiga = AppoggioRiga & Math.Round(campodbN(MyRiga.Item("TQuantita")) / 100, 2) & ";"
                    Else
                        If Raggruppa = "D" Then
                            AppoggioRiga = AppoggioRiga & campodbN(MyRiga.Item("TQuantita")) & ";"
                        Else
                            AppoggioRiga = AppoggioRiga & "1" & ";"
                        End If
                    End If

                    AppoggioRiga = AppoggioRiga & 0 & ";" 'DO30_COLLI

                    If campodbN(MyRiga.Item("RigaDaCausale")) = 1 Then
                        If CausaleContabile.TipoDocumento = "NC" Then
                            If campodb(MyRiga.Item("DareAVere")) = "A" Then
                                AppoggioRiga = AppoggioRiga & campodbN(MyRiga.Item("TImporto")) & ";" 'DO30_PREZZO
                            Else
                                AppoggioRiga = AppoggioRiga & "-" & campodbN(MyRiga.Item("TImporto")) & ";" 'DO30_PREZZO
                            End If
                        Else
                            If campodb(MyRiga.Item("DareAVere")) = "D" Then
                                AppoggioRiga = AppoggioRiga & campodbN(MyRiga.Item("TImporto")) & ";" 'DO30_PREZZO
                            Else
                                AppoggioRiga = AppoggioRiga & "-" & campodbN(MyRiga.Item("TImporto")) & ";" 'DO30_PREZZO
                            End If
                        End If
                    Else
                        If CausaleContabile.TipoDocumento = "NC" Then
                            If campodb(MyRiga.Item("DareAVere")) = "D" Then
                                AppoggioRiga = AppoggioRiga & campodbN(MyRiga.Item("TImporto")) & ";" 'DO30_PREZZO
                            Else
                                AppoggioRiga = AppoggioRiga & "-" & campodbN(MyRiga.Item("TImporto")) & ";" 'DO30_PREZZO
                            End If
                        Else
                            If campodb(MyRiga.Item("DareAVere")) = "A" Then
                                AppoggioRiga = AppoggioRiga & campodbN(MyRiga.Item("TImporto")) & ";" 'DO30_PREZZO
                            Else
                                AppoggioRiga = AppoggioRiga & "-" & campodbN(MyRiga.Item("TImporto")) & ";" 'DO30_PREZZO
                            End If
                        End If
                    End If




                    TabTrascodificaCausale.TIPOTAB = "IV"
                    TabTrascodificaCausale.SENIOR = campodb(MyRiga.Item("CodiceIVA"))
                    TabTrascodificaCausale.EXPORT = ""
                    TabTrascodificaCausale.Leggi(Session("DC_TABELLE"))

                    If TabTrascodificaCausale.EXPORT <> "" Then
                        AppoggioRiga = AppoggioRiga & TabTrascodificaCausale.EXPORT
                    Else
                        AppoggioRiga = AppoggioRiga & campodb(MyRiga.Item("CodiceIVA")) & ";" 'DO30_CODIVA_CODICE_SEN
                    End If




                    Dim CIVA As New Cls_IVA

                    CIVA.Codice = campodb(MyRiga.Item("CodiceIVA"))
                    CIVA.Leggi(Session("DC_TABELLE"), CIVA.Codice)

                    AppoggioRiga = AppoggioRiga & CIVA.Descrizione & ";" 'DO30_CODIVA_NOME_SEN


                    AppoggioRiga = AppoggioRiga & campodb(myPOSTreader.Item("CentroServizio")) & ";" 'CODICE CENTRO SERVIZIO SENIOR

                    Dim MyCservR As New Cls_CentroServizio

                    MyCservR.CENTROSERVIZIO = campodb(myPOSTreader.Item("CentroServizio"))
                    MyCservR.Leggi(Session("DC_OSPITE"), MyCservR.CENTROSERVIZIO)

                    AppoggioRiga = AppoggioRiga & MyCservR.DESCRIZIONE & ";" 'DESCRIZIONE CENTRO SERVIZIO SENIOR


                    AppoggioRiga = AppoggioRiga & campodb(myPOSTreader.Item("AnnoCompetenza")) & ";"

                    AppoggioRiga = AppoggioRiga & campodb(myPOSTreader.Item("MeseCompetenza")) & ";"

                    Entrato = True
                    tw.WriteLine(AppoggioRiga.Replace(vbNewLine, " "))

                Loop



            Else


                If LeggiParametri.RaggruppaRicavi = 1 Then
                    cmdRiga.CommandText = "Select * From MovimentiContabiliRiga Where Numero = ?  And (Tipo Is Null Or Tipo <> 'IV') And (Importo > 0 OR (Importo = 0 And Imponibile =0))  Order by RigaDaCausale,Descrizione"
                Else
                    cmdRiga.CommandText = "Select * From MovimentiContabiliRiga Where Numero = ?  And (Tipo Is Null Or Tipo <> 'IV')  Order by RigaDaCausale,Descrizione"
                End If
                cmdRiga.Connection = cn
                cmdRiga.Parameters.AddWithValue("@Numero", campodbN(myPOSTreader.Item("NumeroRegistrazione")))
                Dim MyRiga As OleDbDataReader = cmdRiga.ExecuteReader()
                Do While MyRiga.Read
                    Progressivo = Progressivo + 1
                    ProgressivoAssoluto = ProgressivoAssoluto + 1
                    AppoggioRiga = ""

                    If campodbN(MyRiga.Item("RigaDaCausale")) = 1 Then
                        AppoggioRiga = "P" & ";"  'DO_TIPO_RIGA
                    Else
                        AppoggioRiga = "R" & ";"  'DO_TIPO_RIGA
                    End If

                    AppoggioRiga = AppoggioRiga & ProgressivoAssoluto & ";" 'progressivo assoluto nel file

                    AppoggioRiga = AppoggioRiga & campodbN(MyRiga.Item("Id")) & ";" 'id riga di senior

                    AppoggioRiga = AppoggioRiga & ProgressivoTesta & ";" 'assoluto nel file

                    AppoggioRiga = AppoggioRiga & CodiceInstallazione & ";" 'installazione dl software senior

                    AppoggioRiga = AppoggioRiga & CodiceDitta & ";" ' 'il codice ditta in alyante

                    AppoggioRiga = AppoggioRiga & CausaleContabile.CodiceSede & ";" ' il codice sede in alyante
                    AppoggioRiga = AppoggioRiga & CausaleContabile.RegistroIVA & ";" ' il codice sede in alyante

                    AppoggioRiga = AppoggioRiga & Progressivo & ";" 'progressivo riga per ogni registrazione

                    AppoggioRiga = AppoggioRiga & "1" & ";" 'TIPO RIGA ALYANTE Valore Fisso 1


                    AppoggioRiga = AppoggioRiga & "" & ";" 'CODICE ARTICOLO ALYANTE



                    TabTrascodificaCausale.TIPOTAB = "CC"
                    TabTrascodificaCausale.SENIOR = campodbN(MyRiga.Item("MastroPartita")) & "." & campodbN(MyRiga.Item("ContoPartita"))
                    TabTrascodificaCausale.EXPORT = ""
                    TabTrascodificaCausale.Leggi(Session("DC_TABELLE"))

                    If TabTrascodificaCausale.EXPORT = "" Then
                        AppoggioRiga = AppoggioRiga & campodbN(MyRiga.Item("MastroPartita")) & "." & campodbN(MyRiga.Item("ContoPartita")) & "." & campodbN(MyRiga.Item("SottocontoPartita")) & ";" 'CONTO SENIOR CLIENTE
                    Else
                        AppoggioRiga = AppoggioRiga & TabTrascodificaCausale.EXPORT & "." & campodbN(MyRiga.Item("SottocontoPartita")) & ";" 'CONTO SENIOR CLIENTE
                    End If


                    Dim DecConto As New Cls_Pianodeiconti

                    DecConto.Mastro = campodbN(MyRiga.Item("MastroPartita"))
                    DecConto.Conto = campodbN(MyRiga.Item("ContoPartita"))
                    DecConto.Sottoconto = campodbN(MyRiga.Item("SottocontoPartita"))
                    DecConto.Decodfica(Session("DC_GENERALE"))
                    AppoggioRiga = AppoggioRiga & DecConto.Descrizione.Replace(";", " ") 'DESCRIZIONE CONTO SENIRO

                    AppoggioRiga = AppoggioRiga & "" & ";" ' OPZIONE_MG5E vuoto



                    REM *************** CREA DESCRIZIONE RIGA CON PERSONALIZZAIONI

                    DecConto.Mastro = campodbN(MyRiga.Item("MastroControPartita"))
                    DecConto.Conto = campodbN(MyRiga.Item("ContoControPartita"))
                    DecConto.Sottoconto = campodbN(MyRiga.Item("SottocontoControPartita"))
                    If DecConto.Sottoconto / 100 <> Int(DecConto.Sottoconto / 100) Then
                        DecConto.Sottoconto = Int(DecConto.Sottoconto / 100) * 100
                    End If
                    DecConto.Decodfica(Session("DC_GENERALE"))

                    Dim Riferimento As String = ""
                    Dim DescrizioneAppoggio As String = ""
                    Dim Cserv As New Cls_CentroServizio


                    Cserv.CENTROSERVIZIO = campodb(myPOSTreader.Item("CentroServizio"))
                    Cserv.Leggi(Session("DC_OSPITE"), Cserv.CENTROSERVIZIO)
                    Riferimento = Cserv.DescrizioneRigaInFattura

                    If Riferimento = "Conto Retta - descrizione - Betulle" Then
                        If Mid(Tipo & Space(10), 1, 1) = "C" Or Mid(Tipo & Space(10), 1, 1) = "R" Or Mid(Tipo & Space(10), 1, 1) = "J" Then
                            If campodbN(MyRiga.Item("RigaDaCausale")) = 3 Then
                                DescrizioneAppoggio = Fnvb6.DecodificaCommentoConto(campodbN(MyRiga.Item("MastroPartita")), campodbN(MyRiga.Item("ContoPartita")), campodbN(MyRiga.Item("SottocontoPartita"))) & "  " & Fnvb6.DecodificaConto(campodbN(MyRiga.Item("MastroControPartita")), campodbN(MyRiga.Item("ContoControPartita")), campodbN(MyRiga.Item("SottocontoControPartita")))
                            Else
                                If campodbN(MyRiga.Item("RigaDaCausale")) = 9 Then
                                    DescrizioneAppoggio = Fnvb6.DecodificaCommentoConto(campodbN(MyRiga.Item("MastroPartita")), campodbN(MyRiga.Item("ContoPartita")), campodbN(MyRiga.Item("SottocontoPartita")))
                                Else
                                    DescrizioneAppoggio = Fnvb6.DecodificaCommentoConto(campodbN(MyRiga.Item("MastroPartita")), campodbN(MyRiga.Item("ContoPartita")), campodbN(MyRiga.Item("SottocontoPartita"))) & "  " & Fnvb6.DecodificaConto(campodbN(MyRiga.Item("MastroControPartita")), campodbN(MyRiga.Item("ContoControPartita")), campodbN(MyRiga.Item("SottocontoControPartita")))
                                End If
                            End If

                        Else
                            If campodbN(MyRiga.Item("RigaDaCausale")) = 3 Then
                                Dim MeseBetulle As Integer
                                Dim AnnoBetulle As Integer
                                MeseBetulle = campodb(MyRiga.Item("MeseRiferimento"))
                                AnnoBetulle = campodb(MyRiga.Item("AnnoRiferimento"))


                                Dim UltLiv As New Cls_rettatotale

                                UltLiv.CENTROSERVIZIO = campodb(myPOSTreader.Item("CentroServizio"))
                                UltLiv.CODICEOSPITE = Fnvb6.CampoOspiteParenteMCS("CodiceOspite", campodbN(MyRiga.Item("MastroControPartita")), campodbN(MyRiga.Item("ContoControPartita")), campodbN(MyRiga.Item("SottocontoControPartita")))
                                UltLiv.UltimaData(Session("DC_OSPITE"), UltLiv.CODICEOSPITE, UltLiv.CENTROSERVIZIO)
                                Dim Xs As New Cls_TipoRetta

                                Xs.Tipo = UltLiv.TipoRetta
                                Xs.Leggi(Session("DC_OSPITE"), Xs.Tipo)
                                If Xs.Descrizione = "PROVVISORIO" Then
                                    Dim MIng As New Cls_Movimenti

                                    MIng.CENTROSERVIZIO = campodb(myPOSTreader.Item("CentroServizio"))
                                    MIng.CodiceOspite = Fnvb6.CampoOspiteParenteMCS("CodiceOspite", campodbN(MyRiga.Item("MastroControPartita")), campodbN(MyRiga.Item("ContoControPartita")), campodbN(MyRiga.Item("SottocontoControPartita")))
                                    MIng.UltimaDataAccoglimento(Session("DC_OSPITE"))

                                    Dim MUsc As New Cls_Movimenti

                                    MUsc.CENTROSERVIZIO = campodb(myPOSTreader.Item("CentroServizio"))
                                    MUsc.CodiceOspite = Fnvb6.CampoOspiteParenteMCS("CodiceOspite", campodbN(MyRiga.Item("MastroControPartita")), campodbN(MyRiga.Item("ContoControPartita")), campodbN(MyRiga.Item("SottocontoControPartita")))
                                    MUsc.UltimaDataUscitaDefinitiva(Session("DC_OSPITE"))


                                    If campodbN(MyRiga.Item("RigaDaCausale")) = 3 And campodbN(MyRiga.Item("MastroPartita")) = 58 And (campodbN(MyRiga.Item("SottocontoPartita")) = 20 Or campodbN(MyRiga.Item("SottocontoPartita")) = 2) And (campodbN(MyRiga.Item("SottocontoPartita")) = 2 Or campodbN(MyRiga.Item("SottocontoPartita")) = 5) Then
                                        DescrizioneAppoggio = Fnvb6.DecodificaCommentoConto(campodbN(MyRiga.Item("MastroPartita")), campodbN(MyRiga.Item("ContoPartita")), campodbN(MyRiga.Item("SottocontoPartita"))) & "  " & campodb(MyRiga.Item("Descrizione")) & " Periodo Dal : " & Format(MIng.Data, "dd/MM/yyyy") & " al : " & Format(MUsc.Data, "dd/MM/yyyy")
                                    Else
                                        Dim aPPOGGIOVerificaAssistenza As String = Fnvb6.DecodificaCommentoConto(campodbN(MyRiga.Item("MastroPartita")), campodbN(MyRiga.Item("ContoPartita")), campodbN(MyRiga.Item("SottocontoPartita")))

                                        If aPPOGGIOVerificaAssistenza.IndexOf("Assistenza") > 0 Then
                                            DescrizioneAppoggio = Fnvb6.DecodificaCommentoConto(campodbN(MyRiga.Item("MastroPartita")), campodbN(MyRiga.Item("ContoPartita")), campodbN(MyRiga.Item("SottocontoPartita"))) & " " & campodb(MyRiga.Item("Descrizione")) & " Periodo Dal : " & Format(MIng.Data, "dd/MM/yyyy") & " al : " & Format(DateSerial(Anno, Mese, GiorniMese(Mese, Anno)), "dd/MM/yyyy")
                                        Else
                                            DescrizioneAppoggio = Fnvb6.DecodificaCommentoConto(campodbN(MyRiga.Item("MastroPartita")), campodbN(MyRiga.Item("ContoPartita")), campodbN(MyRiga.Item("SottocontoPartita"))) & " " & campodb(MyRiga.Item("Descrizione")) & " Periodo Dal : " & Format(MIng.Data, "dd/MM/yyyy") & " al : " & Format(MUsc.Data, "dd/MM/yyyy")
                                        End If

                                    End If
                                Else
                                    If campodbN(MyRiga.Item("RigaDaCausale")) = 3 And campodbN(MyRiga.Item("MastroPartita")) = 58 And (campodbN(MyRiga.Item("ContoPartita")) = 20 Or campodbN(MyRiga.Item("ContoPartita")) = 2) And (campodbN(MyRiga.Item("SottocontoPartita")) = 2 Or campodbN(MyRiga.Item("SottocontoPartita")) = 5) Then
                                        DescrizioneAppoggio = Fnvb6.DecodificaCommentoConto(campodbN(MyRiga.Item("MastroPartita")), campodbN(MyRiga.Item("ContoPartita")), campodbN(MyRiga.Item("SottocontoPartita"))) & campodb(MyRiga.Item("Descrizione")) & " Mese di " & DecodificaMese(campodb(myPOSTreader.Item("MeseCompetenza"))) & campodb(myPOSTreader.Item("AnnoCompetenza"))
                                    Else
                                        DescrizioneAppoggio = Fnvb6.DecodificaCommentoConto(campodbN(MyRiga.Item("MastroPartita")), campodbN(MyRiga.Item("ContoPartita")), campodbN(MyRiga.Item("SottocontoPartita"))) & campodb(MyRiga.Item("Descrizione")) & " Mese di " & DecodificaMese(MeseBetulle) & AnnoBetulle
                                        If CausaleContabile.TipoDocumento = "NC" Then
                                            If campodb(MyRiga.Item("DareAVere")) = "D" Then
                                                DescrizioneAppoggio = "Reso " & Fnvb6.DecodificaCommentoConto(campodbN(MyRiga.Item("MastroPartita")), campodbN(MyRiga.Item("ContoPartita")), campodbN(MyRiga.Item("SottocontoPartita"))) & "  " & campodb(MyRiga.Item("Descrizione")) & " Mese di " & DecodificaMese(MeseBetulle) & " / " & AnnoBetulle
                                            End If
                                        End If
                                    End If

                                End If


                            Else
                                Dim AppoggioDescrizione As String = campodb(MyRiga.Item("Descrizione"))
                                '58 20 2 
                                If campodbN(MyRiga.Item("RigaDaCausale")) = 5 And campodbN(MyRiga.Item("MastroPartita")) = 58 And campodbN(MyRiga.Item("ContoPartita")) = 20 And campodbN(MyRiga.Item("SottocontoPartita")) = 2 Then
                                    DescrizioneAppoggio = Fnvb6.DecodificaCommentoConto(campodbN(MyRiga.Item("MastroPartita")), campodbN(MyRiga.Item("ContoPartita")), campodbN(MyRiga.Item("SottocontoPartita"))) & "  " & campodb(MyRiga.Item("Descrizione")) & " Mese di " & DecodificaMese(campodb(myPOSTreader.Item("MeseCompetenza"))) & campodb(myPOSTreader.Item("AnnoCompetenza"))
                                Else
                                    DescrizioneAppoggio = Fnvb6.DecodificaCommentoConto(campodbN(MyRiga.Item("MastroPartita")), campodbN(MyRiga.Item("ContoPartita")), campodbN(MyRiga.Item("SottocontoPartita"))) & "  " & campodb(MyRiga.Item("Descrizione"))
                                End If
                            End If
                        End If
                    End If



                    'Dim UscitaDiSicurezza As String = ""
                    'If Session("DC_TABELLE").ToString.IndexOf("AppaltiDomiciliare") >= 0 Then
                    '    Dim tipoutente As New Cls_Appalti_TipoUtente

                    '    tipoutente.intypedescription = campodbN(MyRiga.Item("DestinatoVendita"))
                    '    tipoutente.LeggiDescrizione(Session("DC_TABELLE"), tipoutente.intypedescription)


                    '    UscitaDiSicurezza = tipoutente.intypecod
                    'End If

                    If Riferimento = "Residenziale Cittadella" Then
                        If campodbN(MyRiga.Item("RigaDaCausale")) >= 3 Then
                            If (Mid(Tipo & Space(10), 1, 1) = "C" Or Mid(Tipo & Space(10), 1, 1) = "R") Or campodbN(MyRiga.Item("RigaDaCausale")) = 5 Or campodbN(MyRiga.Item("RigaDaCausale")) = 6 Or campodbN(MyRiga.Item("RigaDaCausale")) = 9 Then
                                DescrizioneAppoggio = Fnvb6.DecodificaConto(campodbN(MyRiga.Item("MastroPartita")), campodbN(MyRiga.Item("ContoPartita")), campodbN(MyRiga.Item("SottocontoPartita"))) & " " & Fnvb6.DecodificaConto(campodbN(MyRiga.Item("MastroControPartita")), campodbN(MyRiga.Item("ContoControPartita")), campodbN(MyRiga.Item("SottocontoControPartita"))) & " " & campodb(MyRiga.Item("Descrizione"))
                            Else
                                Dim P As New Cls_CalcoloRette
                                CodiceOspite = CodiceOspite
                                Dim ComuneCittadella As New Cls_ImportoComune
                                ComuneCittadella.CODICEOSPITE = CodiceOspite
                                ComuneCittadella.CENTROSERVIZIO = Cserv.CENTROSERVIZIO
                                ComuneCittadella.UltimaData(Session("DC_OSPITE"), ComuneCittadella.CODICEOSPITE, ComuneCittadella.CENTROSERVIZIO)

                                If ComuneCittadella.PROV <> "" And ComuneCittadella.COMUNE <> "" Then
                                    Dim MyCodificaComumeCittadella As New ClsComune
                                    MyCodificaComumeCittadella.Provincia = ComuneCittadella.PROV
                                    MyCodificaComumeCittadella.Comune = ComuneCittadella.COMUNE
                                    MyCodificaComumeCittadella.Leggi(Session("DC_OSPITE"))

                                    If ComuneCittadella.Importo > 0 Then
                                        DescrizioneAppoggio = Fnvb6.DecodificaConto(campodbN(MyRiga.Item("MastroPartita")), campodbN(MyRiga.Item("ContoPartita")), campodbN(MyRiga.Item("SottocontoPartita"))) & " " & Fnvb6.DecodificaConto(campodbN(MyRiga.Item("MastroControPartita")), campodbN(MyRiga.Item("ContoControPartita")), campodbN(MyRiga.Item("SottocontoControPartita"))) & " " & campodb(MyRiga.Item("Descrizione")) & vbNewLine & "Compartecipazione comune " & MyCodificaComumeCittadella.Descrizione & " Euro " & Format(ComuneCittadella.Importo, "#,##0.00") & " " & ComuneCittadella.Tipo
                                    Else
                                        DescrizioneAppoggio = Fnvb6.DecodificaConto(campodbN(MyRiga.Item("MastroPartita")), campodbN(MyRiga.Item("ContoPartita")), campodbN(MyRiga.Item("SottocontoPartita"))) & " " & Fnvb6.DecodificaConto(campodbN(MyRiga.Item("MastroControPartita")), campodbN(MyRiga.Item("ContoControPartita")), campodbN(MyRiga.Item("SottocontoControPartita"))) & " " & campodb(MyRiga.Item("Descrizione")) & vbNewLine & "Compartecipazione comune " & MyCodificaComumeCittadella.Descrizione
                                    End If

                                Else
                                    DescrizioneAppoggio = Fnvb6.DecodificaConto(campodbN(MyRiga.Item("MastroPartita")), campodbN(MyRiga.Item("ContoPartita")), campodbN(MyRiga.Item("SottocontoPartita"))) & " " & Fnvb6.DecodificaConto(campodbN(MyRiga.Item("MastroControPartita")), campodbN(MyRiga.Item("ContoControPartita")), campodbN(MyRiga.Item("SottocontoControPartita"))) & " " & campodb(MyRiga.Item("Descrizione"))
                                End If
                            End If
                        End If
                    End If

                    If Riferimento = "Med Service" Then
                        If Mid(Tipo & Space(10), 1, 1) = "C" Or Mid(Tipo & Space(10), 1, 1) = "R" Or Mid(Tipo & Space(10), 1, 1) = "J" Then
                            If campodbN(MyRiga.Item("RigaDaCausale")) = 3 Then

                                DescrizioneAppoggio = "Retta " & Fnvb6.DecodificaConto(campodbN(MyRiga.Item("MastroControPartita")), campodbN(MyRiga.Item("ContoControPartita")), campodbN(MyRiga.Item("SottocontoControPartita"))) & " " & campodb(MyRiga.Item("Descrizione")) & " " & Fnvb6.DescrizioneRigaOspite(Int(campodbN(MyRiga.Item("SottocontoControPartita")) / 100), campodbN(myPOSTreader.Item("AnnoCompetenza")), campodbN(myPOSTreader.Item("MeseCompetenza")))
                            Else
                                DescrizioneAppoggio = "Retta " & Fnvb6.DecodificaConto(campodbN(MyRiga.Item("MastroControPartita")), campodbN(MyRiga.Item("ContoControPartita")), campodbN(MyRiga.Item("SottocontoControPartita")) & " " & Fnvb6.DecodificaConto(campodbN(MyRiga.Item("MastroPartita")), campodbN(MyRiga.Item("ContoPartita")), campodbN(MyRiga.Item("SottocontoPartita"))) & " " & campodb(MyRiga.Item("Descrizione")) & " " & Fnvb6.DescrizioneRigaOspite(Int(campodbN(MyRiga.Item("SottocontoControPartita")) / 100), campodbN(myPOSTreader.Item("AnnoCompetenza")), campodbN(myPOSTreader.Item("MeseCompetenza"))))
                            End If
                        Else
                            If campodbN(MyRiga.Item("RigaDaCausale")) = 3 Then
                                If campodbN(MyRiga.Item("SottocontoPartita")) = 5800400012 Then
                                    DescrizioneAppoggio = "Retta Alzheimer" & Fnvb6.DecodificaConto(campodbN(MyRiga.Item("MastroControPartita")), campodbN(MyRiga.Item("ContoControPartita")), Int(campodbN(MyRiga.Item("SottocontoControPartita")) / 100) * 100) & " " & campodb(MyRiga.Item("Descrizione"))
                                Else
                                    DescrizioneAppoggio = "Retta " & Fnvb6.DecodificaConto(campodbN(MyRiga.Item("MastroControPartita")), campodbN(MyRiga.Item("ContoControPartita")), Int(campodbN(MyRiga.Item("SottocontoControPartita")) / 100) * 100) & " " & campodb(MyRiga.Item("Descrizione"))
                                End If
                            Else
                                DescrizioneAppoggio = campodb(MyRiga.Item("Descrizione")) & " " & Fnvb6.DecodificaConto(campodbN(MyRiga.Item("MastroControPartita")), campodbN(MyRiga.Item("ContoControPartita")), campodbN(MyRiga.Item("SottocontoControPartita")))
                            End If
                        End If

                        '5800
                        Dim AppoggioSottoconto As String = campodb(MyRiga.Item("SottocontoPartita"))

                        If CodiceInstallazione = "04" And RegistroIva = 1 Then
                            If AppoggioSottoconto.IndexOf("580" & "03") < 0 And AppoggioSottoconto.IndexOf("580" & "04") < 0 And campodbN(MyRiga.Item("MastroPartita")) = 90 Then
                                Dim myrigaerrore As System.Data.DataRow = MyTable.NewRow()
                                myrigaerrore.Item("Numero Registrazione") = campodbN(myPOSTreader.Item("NumeroRegistrazione"))
                                myrigaerrore.Item("Cliente") = DO11_RAGIONESOCIALE
                                myrigaerrore.Item("Segnalazione") = "Sottoconto non conforme con codice installazione " & " CONTO " & AppoggioSottoconto
                                MyTable.Rows.Add(myrigaerrore)
                            End If
                        Else
                            If CodiceInstallazione = 10 Then
                                If AppoggioSottoconto.IndexOf("580" & "81") < 0 And campodbN(MyRiga.Item("MastroPartita")) = 90 Then
                                    Dim myrigaerrore As System.Data.DataRow = MyTable.NewRow()
                                    myrigaerrore.Item("Numero Registrazione") = campodbN(myPOSTreader.Item("NumeroRegistrazione"))
                                    myrigaerrore.Item("Cliente") = DO11_RAGIONESOCIALE
                                    myrigaerrore.Item("Segnalazione") = "Sottoconto non conforme con codice installazione " & " CONTO " & AppoggioSottoconto
                                    MyTable.Rows.Add(myrigaerrore)
                                End If
                            Else
                                If AppoggioSottoconto.IndexOf("580" & CodiceInstallazione) < 0 And campodbN(MyRiga.Item("MastroPartita")) = 90 And CodiceInstallazione <> "12" Then
                                    Dim myrigaerrore As System.Data.DataRow = MyTable.NewRow()
                                    myrigaerrore.Item("Numero Registrazione") = campodbN(myPOSTreader.Item("NumeroRegistrazione"))
                                    myrigaerrore.Item("Cliente") = DO11_RAGIONESOCIALE
                                    myrigaerrore.Item("Segnalazione") = "Sottoconto non conforme con codice installazione " & " CONTO " & AppoggioSottoconto
                                    MyTable.Rows.Add(myrigaerrore)
                                End If

                            End If
                        End If

                    End If

                    If Riferimento = "Diurno Soresina" Then
                        If campodbN(MyRiga.Item("RigaDaCausale")) <> 3 And campodbN(MyRiga.Item("RigaDaCausale")) <> 9 Then
                            DescrizioneAppoggio = Fnvb6.DecodificaConto(campodbN(MyRiga.Item("MastroPartita")), campodbN(MyRiga.Item("ContoPartita")), campodbN(MyRiga.Item("SottocontoPartita"))) & " " & Fnvb6.DecodificaConto(campodbN(MyRiga.Item("MastroControPartita")), campodbN(MyRiga.Item("ContoControPartita")), campodbN(MyRiga.Item("SottocontoControPartita"))) & " " & campodb(MyRiga.Item("Descrizione"))
                        End If
                        If campodbN(MyRiga.Item("RigaDaCausale")) = 3 Then
                            If (Mid(Tipo & Space(10), 1, 1) = "C" Or Mid(Tipo & Space(10), 1, 1) = "R") Then
                                DescrizioneAppoggio = Fnvb6.DecodificaConto(campodbN(MyRiga.Item("MastroPartita")), campodbN(MyRiga.Item("ContoPartita")), campodbN(MyRiga.Item("SottocontoPartita"))) & " " & Fnvb6.DecodificaConto(campodbN(MyRiga.Item("MastroControPartita")), campodbN(MyRiga.Item("ContoControPartita")), campodbN(MyRiga.Item("SottocontoControPartita")))
                            Else
                                Dim xAnno As Integer = campodbN(myPOSTreader.Item("AnnoCompetenza"))
                                Dim xMese As Integer = campodbN(myPOSTreader.Item("MeseCompetenza"))
                                Dim OMastro As Integer
                                Dim OConto As Integer
                                Dim OSottoconto As Integer
                                MySql = ""

                                OMastro = campodbN(MyRiga.Item("MastroControPartita"))
                                OConto = campodbN(MyRiga.Item("ContoControPartita"))
                                OSottoconto = campodbN(MyRiga.Item("SottocontoControPartita"))
                                CodiceOspite = Val(Fnvb6.CampoOspiteParenteMCS("CodiceOspite", OMastro, OConto, OSottoconto))


                                Dim DiurnoString As String = ""
                                Dim K As New Cls_Diurno

                                K.Anno = xAnno
                                K.Mese = xMese
                                K.CodiceOspite = CodiceOspite
                                K.CENTROSERVIZIO = campodb(myPOSTreader.Item("CentroServizio"))

                                Dim AppoggioMese As String = ""
                                AppoggioMese = K.LeggiMese(Session("DC_OSPITE"), K.CodiceOspite, K.CENTROSERVIZIO, K.Anno, K.Mese)

                                AppoggioMese = Mid(AppoggioMese, 1, GiorniMese(K.Mese, K.Anno) * 2)


                                Dim TrasportoDiurno1 As Integer = 0
                                Dim TrasportoDiurno2 As Integer = 0
                                Dim PresenzeFullTime As Integer = 0
                                Dim PresenzeMattina As Integer = 0
                                Dim PresenzePomeriggio As Integer = 0


                                TrasportoDiurno1 = quanteVolte(AppoggioMese, "t1") + quanteVolte(AppoggioMese, "M1") + quanteVolte(AppoggioMese, "P1")
                                TrasportoDiurno2 = quanteVolte(AppoggioMese, "t2")
                                PresenzeFullTime = quanteVolte(AppoggioMese, "  ") + quanteVolte(AppoggioMese, "t1") + quanteVolte(AppoggioMese, "t2")
                                PresenzeMattina = quanteVolte(AppoggioMese, "M1") + quanteVolte(AppoggioMese, "PM")
                                PresenzePomeriggio = quanteVolte(AppoggioMese, "P1") + quanteVolte(AppoggioMese, "PP")

                                If PresenzeFullTime > 0 Then

                                    DiurnoString = DiurnoString & vbNewLine

                                    DiurnoString = DiurnoString & "Presenze Full Time (20,19 Euro) :" & PresenzeFullTime
                                End If
                                If PresenzeMattina > 0 Then

                                    DiurnoString = DiurnoString & vbNewLine

                                    DiurnoString = DiurnoString & "Presenze Mattina (14,42 Euro) :" & PresenzeMattina
                                End If
                                If PresenzePomeriggio > 0 Then

                                    DiurnoString = DiurnoString & vbNewLine

                                    DiurnoString = DiurnoString & "Presenze Pomeriggio (8,65 Euro) :" & PresenzePomeriggio
                                End If

                                If TrasportoDiurno1 > 0 Then

                                    DiurnoString = DiurnoString & vbNewLine

                                    DiurnoString = DiurnoString & "1° Trasporto :" & TrasportoDiurno1
                                End If
                                If TrasportoDiurno2 > 0 Then

                                    DiurnoString = DiurnoString & vbNewLine

                                    DiurnoString = DiurnoString & "2° Trasporti :" & TrasportoDiurno2
                                End If


                                DescrizioneAppoggio = Fnvb6.DecodificaConto(campodbN(MyRiga.Item("MastroPartita")), campodbN(MyRiga.Item("ContoPartita")), campodbN(MyRiga.Item("SottocontoPartita"))) & " " & Fnvb6.DecodificaConto(campodbN(MyRiga.Item("MastroControPartita")), campodbN(MyRiga.Item("ContoControPartita")), campodbN(MyRiga.Item("SottocontoControPartita"))) & " " & DiurnoString
                            End If
                        End If
                    End If
                    If Riferimento = "Uscita Sicurezza" Then
                        If Mid(Tipo & Space(10), 1, 1) = "O" Then
                            If campodbN(MyRiga.Item("RigaDaCausale")) = 3 Then
                                DescrizioneAppoggio = Fnvb6.DecodificaConto(campodbN(MyRiga.Item("MastroPartita")), campodbN(MyRiga.Item("ContoPartita")), campodbN(MyRiga.Item("SottocontoPartita"))) & " " & Fnvb6.DecodificaConto(campodbN(MyRiga.Item("MastroControPartita")), campodbN(MyRiga.Item("ContoControPartita")), campodbN(MyRiga.Item("SottocontoControPartita"))) & " " & campodb(MyRiga.Item("Descrizione"))
                            End If
                        End If
                        If Mid(Tipo & Space(10), 1, 1) = "P" Then
                            If campodbN(MyRiga.Item("RigaDaCausale")) = 3 Then
                                DescrizioneAppoggio = Fnvb6.DecodificaConto(campodbN(MyRiga.Item("MastroPartita")), campodbN(MyRiga.Item("ContoPartita")), campodbN(MyRiga.Item("SottocontoPartita"))) & " " & Fnvb6.DecodificaConto(campodbN(MyRiga.Item("MastroControPartita")), campodbN(MyRiga.Item("ContoControPartita")), Int(campodbN(MyRiga.Item("SottocontoControPartita")) / 100) * 100) & " " & campodb(MyRiga.Item("Descrizione"))
                            End If
                        End If
                    End If



                    If DescrizioneAppoggio = "" Then
                        If campodb(MyRiga.Item("Descrizione")).ToUpper.IndexOf("PRESENZE") >= 0 Or campodb(MyRiga.Item("Descrizione")).ToUpper.IndexOf("ASSENZE") >= 0 Then
                            DescrizioneAppoggio = campodb(MyRiga.Item("Descrizione")) & " " & DecConto.Descrizione 'DO30_DESCART
                        Else
                            DescrizioneAppoggio = Trim(DecConto.Descrizione & " " & campodb(MyRiga.Item("Descrizione")))  'DO30_DESCART
                        End If
                    End If
                    If Riferimento = "ANNISERENI" Then
                        DescrizioneAppoggio = campodb(MyRiga.Item("Descrizione"))
                    End If

                    AppoggioRiga = AppoggioRiga & ";"

                    AppoggioRiga = AppoggioRiga & DescrizioneAppoggio.Replace(";", "").Replace(vbNewLine, " ") & ";" 'DESCRIZIONE SENIOR CONTO




                    AppoggioRiga = AppoggioRiga & "" & ";" 'DO30_UM1 UNITA MISURA
                    If Request.Item("APPALTI") = "SI" Then
                        AppoggioRiga = AppoggioRiga & Math.Round(campodbN(MyRiga.Item("Quantita")) / 100, 2) & ";"
                    Else
                        AppoggioRiga = AppoggioRiga & campodbN(MyRiga.Item("Quantita")) & ";"
                    End If
                    AppoggioRiga = AppoggioRiga & 0 & ";" 'DO30_COLLI

                    If campodbN(MyRiga.Item("RigaDaCausale")) = 1 Then
                        If CausaleContabile.TipoDocumento = "NC" Then
                            If campodb(MyRiga.Item("DareAVere")) = "A" Then
                                AppoggioRiga = AppoggioRiga & campodbN(MyRiga.Item("Importo")) & ";" 'DO30_PREZZO
                            Else
                                AppoggioRiga = AppoggioRiga & "-" & campodbN(MyRiga.Item("Importo")) & ";" 'DO30_PREZZO
                            End If
                        Else
                            If campodb(MyRiga.Item("DareAVere")) = "D" Then
                                AppoggioRiga = AppoggioRiga & campodbN(MyRiga.Item("Importo")) & ";" 'DO30_PREZZO
                            Else
                                AppoggioRiga = AppoggioRiga & "-" & campodbN(MyRiga.Item("Importo")) & ";" 'DO30_PREZZO
                            End If
                        End If
                    Else
                        If CausaleContabile.TipoDocumento = "NC" Then
                            If campodb(MyRiga.Item("DareAVere")) = "D" Then
                                AppoggioRiga = AppoggioRiga & campodbN(MyRiga.Item("Importo")) & ";" 'DO30_PREZZO
                            Else
                                AppoggioRiga = AppoggioRiga & "-" & campodbN(MyRiga.Item("Importo")) & ";" 'DO30_PREZZO
                            End If
                        Else
                            If campodb(MyRiga.Item("DareAVere")) = "A" Then
                                AppoggioRiga = AppoggioRiga & campodbN(MyRiga.Item("Importo")) & ";" 'DO30_PREZZO
                            Else
                                AppoggioRiga = AppoggioRiga & "-" & campodbN(MyRiga.Item("Importo")) & ";" 'DO30_PREZZO
                            End If
                        End If
                    End If


                    TabTrascodificaCausale.TIPOTAB = "IV"
                    TabTrascodificaCausale.SENIOR = campodb(MyRiga.Item("CodiceIVA"))
                    TabTrascodificaCausale.EXPORT = ""
                    TabTrascodificaCausale.Leggi(Session("DC_TABELLE"))

                    If TabTrascodificaCausale.EXPORT <> "" Then
                        AppoggioRiga = AppoggioRiga & TabTrascodificaCausale.EXPORT & ";" 'DO30_CODIVA_CODICE_SEN
                    Else
                        AppoggioRiga = AppoggioRiga & campodb(MyRiga.Item("CodiceIVA")) & ";" 'DO30_CODIVA_CODICE_SEN
                    End If



                    Dim CIVA As New Cls_IVA

                    CIVA.Codice = campodb(MyRiga.Item("CodiceIVA"))
                    CIVA.Leggi(Session("DC_TABELLE"), CIVA.Codice)

                    AppoggioRiga = AppoggioRiga & CIVA.Descrizione & ";" 'DO30_CODIVA_NOME_SEN


                    Dim MyCserv As New Cls_CentroServizio

                    If campodb(MyRiga.Item("CentroServizio")) = "" Then
                        MyCserv.CENTROSERVIZIO = campodb(myPOSTreader.Item("CentroServizio"))
                        MyCserv.Leggi(Session("DC_OSPITE"), MyCserv.CENTROSERVIZIO)
                        AppoggioRiga = AppoggioRiga & campodb(myPOSTreader.Item("CentroServizio")) & ";" 'CODICE CENTRO SERVIZIO SENIOR

                        AppoggioRiga = AppoggioRiga & MyCserv.DESCRIZIONE & ";" 'DESCRIZIONE CENTRO SERVIZIO SENIOR
                    Else
                        MyCserv.CENTROSERVIZIO = campodb(MyRiga.Item("CentroServizio"))
                        MyCserv.Leggi(Session("DC_OSPITE"), MyCserv.CENTROSERVIZIO)

                        AppoggioRiga = AppoggioRiga & campodb(MyRiga.Item("CentroServizio")) & ";" 'CODICE CENTRO SERVIZIO SENIOR
                        AppoggioRiga = AppoggioRiga & MyCserv.DESCRIZIONE & ";" 'DESCRIZIONE CENTRO SERVIZIO SENIOR
                    End If



                    If campodbN(MyRiga.Item("AnnoRiferimento")) = 0 Then
                        AppoggioRiga = AppoggioRiga & campodb(myPOSTreader.Item("AnnoCompetenza")) & ";"
                    Else
                        AppoggioRiga = AppoggioRiga & campodb(MyRiga.Item("AnnoRiferimento")) & ";"
                    End If

                    If campodbN(MyRiga.Item("MeseRiferimento")) = 0 Then
                        AppoggioRiga = AppoggioRiga & campodb(myPOSTreader.Item("MeseCompetenza")) & ";"
                    Else
                        AppoggioRiga = AppoggioRiga & campodb(MyRiga.Item("MeseRiferimento")) & ";"
                    End If



                    Entrato = True

                    tw.WriteLine(AppoggioRiga.Replace(vbNewLine, " "))

                Loop
                MyRiga.Close()
            End If



            If Entrato = False Then
                Entrato = False

            End If

        Loop
        myPOSTreader.Close()
        cn.Close()
        tw.Close()
        'twR.Close()
        Fnvb6.ChiudiDB()
        tw = Nothing
        'twR = Nothing

        Dim tr As System.IO.TextReader = System.IO.File.OpenText(NomeFile)
        Dim Appo As String
        Dim ImportoTotale As Double = 0
        Dim ImportoRighe As Double = 0
        Dim ImportoAppoggio As Double = 0
        Dim UltimaReg As String

        Do
            Dim VettoreCampi(100) As String


            Appo = tr.ReadLine()


            If Not IsNothing(Appo) Then
                VettoreCampi = Appo.Split(";")
                If VettoreCampi(0) = "T" Then
                    If Math.Abs(Math.Round(ImportoRighe, 2) - Math.Round(ImportoTotale, 2)) > 0.1 Then
                        Dim myrigaerrore As System.Data.DataRow = MyTable.NewRow()
                        myrigaerrore.Item("Numero Registrazione") = UltimaReg
                        Dim Registrazione As New Cls_MovimentoContabile

                        Registrazione.Leggi(Session("DC_GENERALE"), UltimaReg)

                        Dim ContoPianoConti As New Cls_Pianodeiconti
                        ContoPianoConti.Mastro = Registrazione.Righe(0).MastroPartita
                        ContoPianoConti.Conto = Registrazione.Righe(0).ContoPartita
                        ContoPianoConti.Sottoconto = Registrazione.Righe(0).SottocontoPartita
                        ContoPianoConti.Decodfica(Session("DC_GENERALE"))
                        myrigaerrore.Item("Cliente") = ContoPianoConti.Descrizione
                        myrigaerrore.Item("Segnalazione") = "Registrazione non quadra in esportazione  Diffrenza : " & Math.Round(ImportoRighe, 2) & "-" & Math.Round(ImportoTotale, 2)
                        MyTable.Rows.Add(myrigaerrore)
                    End If
                    UltimaReg = campodbN(VettoreCampi(2))
                    ImportoRighe = 0
                    ImportoTotale = 0
                Else
                    If VettoreCampi(0) = "P" Then
                        Try
                            ImportoTotale = campodbN(VettoreCampi(18))
                        Catch ex As Exception
                            ImportoTotale = 0
                        End Try
                    Else
                        Try
                            ImportoAppoggio = campodbN(VettoreCampi(18))
                        Catch ex As Exception
                            ImportoAppoggio = 0
                        End Try
                        Dim Iva As New Cls_IVA

                        TabTrascodificaCausale.SENIOR = ""
                        TabTrascodificaCausale.EXPORT = VettoreCampi(19)
                        TabTrascodificaCausale.TIPOTAB = "IV"
                        TabTrascodificaCausale.LeggiDaExport(Session("DC_TABELLE"))

                        If TabTrascodificaCausale.SENIOR = "" Then
                            Iva.Codice = VettoreCampi(19)
                        Else
                            Iva.Codice = TabTrascodificaCausale.SENIOR
                        End If

                        Iva.Leggi(Session("DC_TABELLE"), Iva.Codice)
                        If Iva.Aliquota > 0 Then
                            ImportoAppoggio = ImportoAppoggio + Math.Round(ImportoAppoggio * Iva.Aliquota, 2)
                        End If
                        'If VettoreCampi(19) = "05" Or VettoreCampi(19) = "25" Then                        

                        '    ImportoAppoggio = ImportoAppoggio + Math.Round(ImportoAppoggio * 5 / 100, 2)
                        'End If
                        'If VettoreCampi(19) = "04" Or VettoreCampi(19) = "24" Then
                        '    ImportoAppoggio = ImportoAppoggio + Math.Round(ImportoAppoggio * 4 / 100, 2)
                        'End If
                        'If VettoreCampi(19) = "11" Then
                        '    ImportoAppoggio = ImportoAppoggio + Math.Round(ImportoAppoggio * 10 / 100, 2)
                        'End If
                        'If VettoreCampi(19) = "22" Then
                        '    ImportoAppoggio = ImportoAppoggio + Math.Round(ImportoAppoggio * 22 / 100, 2)
                        'End If
                        ImportoRighe = ImportoRighe + ImportoAppoggio
                    End If
                End If
            End If
        Loop While Not IsNothing(Appo)
        If Math.Abs(Math.Round(ImportoRighe, 2) - Math.Round(ImportoTotale, 2)) > 0.1 Then
            Dim myrigaerrore As System.Data.DataRow = MyTable.NewRow()
            myrigaerrore.Item("Numero Registrazione") = UltimaReg
            Dim Registrazione As New Cls_MovimentoContabile

            Registrazione.Leggi(Session("DC_GENERALE"), UltimaReg)
            If UltimaReg > 0 Then
                Dim ContoPianoConti As New Cls_Pianodeiconti
                Try

                    ContoPianoConti.Mastro = Registrazione.Righe(0).MastroPartita
                    ContoPianoConti.Conto = Registrazione.Righe(0).ContoPartita
                    ContoPianoConti.Sottoconto = Registrazione.Righe(0).SottocontoPartita
                    ContoPianoConti.Decodfica(Session("DC_GENERALE"))

                Catch ex As Exception

                End Try
                myrigaerrore.Item("Cliente") = ContoPianoConti.Descrizione
                myrigaerrore.Item("Segnalazione") = "Registrazione  " & UltimaReg & " non quadra in esportazione  Diffrenza : " & Math.Round(ImportoRighe, 2) & "-" & Math.Round(ImportoTotale, 2)
                MyTable.Rows.Add(myrigaerrore)
            End If
        End If
        tr.Close()

        Lbl_Errori.Text = ""
        GridView1.AutoGenerateColumns = True
        GridView1.DataSource = MyTable
        GridView1.Font.Size = 10
        GridView1.DataBind()


    End Function




    Sub Export_ATS()
        Dim cn As OleDbConnection
        Dim Anno As Integer
        Dim Mese As Integer
        Dim MeseContr As Integer
        Dim MySql As String

        Dim NomeFileClienti As String
        Dim NomeFileMovim As String
        Dim Stringa As String

        Dim CampoClienti(500) As String
        Dim CampoMovimenti(500) As String

        NomeFileClienti = HostingEnvironment.ApplicationPhysicalPath() & "\Public\Clienti_.txt"
        NomeFileMovim = HostingEnvironment.ApplicationPhysicalPath() & "\Public\Movim_.txt"


        Dim FileClienti As System.IO.TextWriter = System.IO.File.CreateText(NomeFileClienti)

        Dim FileMovim As System.IO.TextWriter = System.IO.File.CreateText(NomeFileMovim)


        Anno = Txt_Anno.Text
        Mese = DD_Mese.SelectedValue

        MeseContr = DD_Mese.SelectedValue


        MySql = "SELECT * " & _
                " FROM MovimentiContabiliTesta " & _
                " WHERE AnnoProtocollo = " & Txt_AnnoRif.Text

        If DD_Registro.SelectedValue <> "" Then
            MySql = MySql & " AND MovimentiContabiliTesta.RegistroIva = " & DD_Registro.SelectedValue
        End If
        If Tab_PeriodoData.ActiveTabIndex = 1 And IsDate(Txt_DataAl1.Text) And IsDate(Txt_DataDal1.Text) Then
            MySql = MySql & " AND (DataRegistrazione >= ? And DataRegistrazione <= ?) "
        Else
            MySql = MySql & " And MovimentiContabiliTesta.AnnoCompetenza = " & Txt_Anno.Text & " AND MovimentiContabiliTesta.MeseCompetenza = " & MeseContr & " "
        End If

        MySql = MySql & " AND (NumeroProtocollo >= " & Txt_DalDocumento.Text & " And NumeroProtocollo <= " & Txt_AlDocumento.Text & ") "

        cn = New Data.OleDb.OleDbConnection(Session("DC_GENERALE"))

        cn.Open()
        Dim cmd As New OleDbCommand()
        cmd.CommandText = MySql & " Order by NumeroProtocollo"
        cmd.Connection = cn
        If Tab_PeriodoData.ActiveTabIndex = 1 And IsDate(Txt_DataAl1.Text) And IsDate(Txt_DataDal1.Text) Then
            cmd.Parameters.AddWithValue("@DataDal", Txt_DataDal1.Text)
            cmd.Parameters.AddWithValue("@DataDal", Txt_DataAl1.Text)
        End If




        Dim MyRsDB As OleDbDataReader = cmd.ExecuteReader()

        Do While MyRsDB.Read
            Dim Imponibile As Double
            Dim Registrazione As New Cls_MovimentoContabile
            Dim CausaleContabile As New Cls_CausaleContabile

            Registrazione.Leggi(Session("DC_GENERALE"), MyRsDB.Item("NumeroRegistrazione"))

            CausaleContabile.Codice = Registrazione.CausaleContabile
            CausaleContabile.Leggi(Session("DC_TABELLE"), CausaleContabile.Codice)


            Dim TipoAnagrafica As String
            Dim Ospiti As New ClsOspite
            Dim Parente As New Cls_Parenti
            Dim Regioni As New ClsUSL
            Dim Comune As New ClsComune

            If Registrazione.Tipologia = "" Then
                Dim CercaCf As Integer
                Dim SottocontoCf As Integer

                For CercaCf = 0 To Registrazione.Righe.Length - 1
                    If Not IsNothing(Registrazione.Righe(CercaCf)) Then
                        If Registrazione.Righe(CercaCf).Tipo = "CF" Then
                            SottocontoCf = Registrazione.Righe(CercaCf).SottocontoPartita
                        End If
                    End If
                Next

                If Int(SottocontoCf / 100) = Math.Round(SottocontoCf / 100, 2) Then
                    Ospiti.CodiceOspite = Int(SottocontoCf / 100)
                    Ospiti.Leggi(Session("DC_OSPITE"), Ospiti.CodiceOspite)
                    TipoAnagrafica = "O"
                Else
                    Parente.CodiceParente = SottocontoCf - (Int(SottocontoCf / 100) * 100)
                    Parente.CodiceOspite = Int(SottocontoCf / 100)
                    Parente.Leggi(Session("DC_OSPITE"), Parente.CodiceOspite, Parente.CodiceParente)
                    TipoAnagrafica = "P"
                End If
            Else
                If Mid(Registrazione.Tipologia & Space(10), 1, 1) = "C" Then
                    Comune.Provincia = Mid(Registrazione.Tipologia & Space(10), 2, 3)
                    Comune.Comune = Mid(Registrazione.Tipologia & Space(10), 5, 3)
                    Comune.Leggi(Session("DC_OSPITE"))

                    TipoAnagrafica = "C"
                End If
                If Mid(Registrazione.Tipologia & Space(10), 1, 1) = "J" Then
                    Comune.Provincia = Mid(Registrazione.Tipologia & Space(10), 2, 3)
                    Comune.Comune = Mid(Registrazione.Tipologia & Space(10), 5, 3)
                    Comune.Leggi(Session("DC_OSPITE"))
                    TipoAnagrafica = "C"
                End If
                If Mid(Registrazione.Tipologia & Space(10), 1, 1) = "R" Then
                    Regioni.CodiceRegione = Mid(Registrazione.Tipologia & Space(10), 2, 4)
                    Regioni.Leggi(Session("DC_OSPITE"))

                    TipoAnagrafica = "R"
                End If
            End If

            Dim Indice As Integer
            Dim I As Integer
            Dim CodiceIVA As String
            Dim Imposta As Double
            Dim Mastro As Integer = 0
            Dim Conto As Integer = 0
            Dim Sottoconto As Integer = 0
            Dim DescrizioneRiga As String = ""

            Imposta = 0
            Imponibile = 0
            CodiceIVA = ""
            For Indice = 0 To Registrazione.Righe.Length - 1
                If Not IsNothing(Registrazione.Righe(Indice)) Then
                    If Registrazione.Righe(Indice).Tipo = "IV" Then
                        Imponibile = Imponibile + Registrazione.Righe(Indice).Imponibile
                        Imposta = Imposta + Registrazione.Righe(Indice).Importo
                        CodiceIVA = Registrazione.Righe(Indice).CodiceIVA
                    End If

                    If Registrazione.Righe(Indice).RigaDaCausale = 3 Then
                        Mastro = Registrazione.Righe(Indice).MastroPartita
                        Conto = Registrazione.Righe(Indice).ContoPartita
                        Sottoconto = Registrazione.Righe(Indice).SottocontoPartita
                        DescrizioneRiga = Registrazione.Righe(Indice).Descrizione
                    End If
                End If
            Next
            Dim RagioneSociale As String = ""
            Dim Codice As String = ""
            Dim ResidenzaIndirizzo As String = ""
            Dim ComuneP As String = ""
            Dim ProvinciaP As String = ""
            Dim Cap As String = ""
            Dim PersonaFisica As String = "S"
            Dim PartitaIVA As String = ""
            Dim CodiceFiscale As String = ""
            Dim ContoRicavo As String = ""


            If TipoAnagrafica = "C" Then
                RagioneSociale = Comune.Descrizione
                Codice = Comune.Provincia & Comune.Comune
                ResidenzaIndirizzo = Comune.RESIDENZAINDIRIZZO1
                Cap = Comune.RESIDENZACAP1
                ComuneP = Comune.RESIDENZACOMUNE1
                ProvinciaP = Comune.RESIDENZAPROVINCIA1
                PersonaFisica = "N"

                PartitaIVA = Comune.PartitaIVA
                CodiceFiscale = Comune.CodiceFiscale

                If Conto = 2 Then
                    ContoRicavo = "51010305" 'non auto
                End If
                If Conto = 1 Then
                    ContoRicavo = "51010105" 'auto
                End If

            End If
            If TipoAnagrafica = "R" Then
                Codice = Regioni.CodiceRegione    'CoidceCliente
                RagioneSociale = Regioni.Nome
                ResidenzaIndirizzo = Regioni.RESIDENZAINDIRIZZO1
                Cap = Regioni.RESIDENZACAP1
                ComuneP = Regioni.RESIDENZACOMUNE1
                ProvinciaP = Regioni.RESIDENZAPROVINCIA1
                PersonaFisica = "N"

                PartitaIVA = Regioni.PARTITAIVA
                CodiceFiscale = Regioni.CodiceFiscale

                If Conto = 2 Then
                    ContoRicavo = "51010303" 'non auto
                End If
                If Conto = 1 Then
                    ContoRicavo = "51010103" 'auto
                End If

                If Conto = 20 Then
                    ContoRicavo = "51010303" 'non auto
                End If
            End If
            If TipoAnagrafica = "O" Then
                Codice = Ospiti.CodiceOspite * 100
                RagioneSociale = Ospiti.Nome
                ResidenzaIndirizzo = Ospiti.RESIDENZAINDIRIZZO1
                Cap = Ospiti.RESIDENZACAP1
                ComuneP = Ospiti.RESIDENZACOMUNE1
                ProvinciaP = Ospiti.RESIDENZAPROVINCIA1

                CodiceFiscale = Ospiti.CODICEFISCALE

                If Conto = 2 Then
                    ContoRicavo = "51010301" 'non auto
                End If
                If Conto = 1 Then
                    ContoRicavo = "51010101" 'auto
                End If
            End If
            If TipoAnagrafica = "P" Then
                Codice = (Parente.CodiceOspite * 100) + Parente.CodiceParente    'CoidceCliente
                RagioneSociale = Parente.Nome
                ResidenzaIndirizzo = Parente.RESIDENZAINDIRIZZO1
                Cap = Parente.RESIDENZACAP1
                ComuneP = Parente.RESIDENZACOMUNE1
                ProvinciaP = Parente.RESIDENZAPROVINCIA1

                CodiceFiscale = Parente.CODICEFISCALE

                If Conto = 2 Then
                    ContoRicavo = "51010301" 'non auto
                End If
                If Conto = 1 Then
                    ContoRicavo = "51010101" 'auto
                End If
            End If

            I = 0

            I = I + 1 : CampoClienti(I) = AccodaSpazio("", 8)   'CONTO
            I = I + 1 : CampoClienti(I) = AccodaSpazio("1", 1)   'TIPO RECORD
            I = I + 1 : CampoClienti(I) = AccodaSpazio("C", 1)   'TIPO CONTO
            I = I + 1 : CampoClienti(I) = AccodaSpazio(RagioneSociale, 50) 'RAGIONE SOCIALE
            I = I + 1 : CampoClienti(I) = Space(11) ' PARTITAIVA
            I = I + 1 : CampoClienti(I) = AccodaSpazio(CodiceFiscale, 16)
            I = I + 1 : CampoClienti(I) = AccodaSpazio("", 2) 'STATO_CEE
            I = I + 1 : CampoClienti(I) = AccodaSpazio("", 12) 'PARTITA_IVA_CEE	
            I = I + 1 : CampoClienti(I) = AccodaSpazio("", 40) 'INTERNET

            I = I + 1 : CampoClienti(I) = AccodaSpazio("", 6) 'CONTABILE
            I = I + 1 : CampoClienti(I) = AccodaSpazio("", 6) 'LISTINO
            I = I + 1 : CampoClienti(I) = AccodaSpazio("", 6) 'STATISTICA
            I = I + 1 : CampoClienti(I) = AccodaSpazio("", 6) 'ZONA
            I = I + 1 : CampoClienti(I) = AccodaSpazio("", 6) 'UTENTE


            I = I + 1 : CampoClienti(I) = AccodaSpazio("PF", 2) 'SOGGETTO
            I = I + 1 : CampoClienti(I) = AccodaSpazio("", 17) 'EORI
            I = I + 1 : CampoClienti(I) = AccodaSpazio("", 50) 'IPA_CODICE
            I = I + 1 : CampoClienti(I) = AccodaSpazio("", 6) 'IPA_CODICE_UNIVOCO_UFFICIO
            I = I + 1 : CampoClienti(I) = AccodaSpazio("", 100) 'NOTE_SOGGETTO
            I = I + 1 : CampoClienti(I) = AccodaSpazio("1", 1) 'SE_SPLIT_PAYMENT					NUMBER(1)		347-347


            Stringa = ""
            For Z = 1 To I
                Stringa = Stringa & CampoClienti(Z) & vbTab
            Next Z
            FileClienti.WriteLine(Stringa)



            I = 0
            I = I + 1 : CampoClienti(I) = AccodaSpazio("", 8)   'CONTO
            I = I + 1 : CampoClienti(I) = AccodaSpazio("2", 1)   'TIPO RECORD            

            I = I + 1 : CampoClienti(I) = AccodaSpazio(ResidenzaIndirizzo, 50) 'INDIRIZZO_1
            I = I + 1 : CampoClienti(I) = AccodaSpazio("", 50) 'INDIRIZZO_2
            I = I + 1 : CampoClienti(I) = AccodaSpazio("", 50) 'INDIRIZZO_3

            Dim DecoComune As New ClsComune

            DecoComune.Provincia = ComuneP
            DecoComune.Comune = ProvinciaP
            DecoComune.Leggi(Session("DC_OSPITE"))

            I = I + 1 : CampoClienti(I) = AccodaSpazio(Cap, 5)
            I = I + 1 : CampoClienti(I) = AccodaSpazio(DecoComune.Descrizione, 40)
            I = I + 1 : CampoClienti(I) = AccodaSpazio(DecoComune.CodificaProvincia, 2)  'ID_PROVINCIA
            I = I + 1 : CampoClienti(I) = Space(20) 'TELEFONO1_1
            I = I + 1 : CampoClienti(I) = Space(20) 'FAX 1
            I = I + 1 : CampoClienti(I) = Space(40) 'E_MAIL 
            I = I + 1 : CampoClienti(I) = AccodaSpazio(DecoComune.Provincia, 3)
            I = I + 1 : CampoClienti(I) = AccodaSpazio(DecoComune.Comune, 3)
            I = I + 1 : CampoClienti(I) = Space(60) 'PEC


            Stringa = ""
            For Z = 1 To I
                Stringa = Stringa & CampoClienti(Z) & vbTab
            Next Z
            FileClienti.WriteLine(Stringa)


            For Indice = 0 To Registrazione.Righe.Length - 1
                If Not IsNothing(Registrazione.Righe(Indice)) Then

                    I = 0

                    I = I + 1 : CampoMovimenti(I) = "C"
                    I = I + 1 : CampoMovimenti(I) = Year(Registrazione.DataRegistrazione)
                    I = I + 1 : CampoMovimenti(I) = "12" 'Tipo_Registro
                    I = I + 1 : CampoMovimenti(I) = "00" 'NUMERO_BOLLATO
                    I = I + 1 : CampoMovimenti(I) = Format(Registrazione.DataRegistrazione, "yyyyMMdd")
                    I = I + 1 : CampoMovimenti(I) = AdattaLunghezzaNumero(Registrazione.NumeroProtocollo, 8)
                    I = I + 1 : CampoMovimenti(I) = "02" 'COD_REC
                    I = I + 1 : CampoMovimenti(I) = "0" ' INTRA_CEE
                    I = I + 1 : CampoMovimenti(I) = Format(Registrazione.DataRegistrazione, "yyyyMMdd") 'DATA_RIF
                    I = I + 1 : CampoMovimenti(I) = AdattaLunghezza(Registrazione.NumeroProtocollo, 15) 'INTRA_CEE

                    I = I + 1 : CampoMovimenti(I) = Year(Registrazione.DataRegistrazione) 'ANNO_PARTITA
                    I = I + 1 : CampoMovimenti(I) = AdattaLunghezza(Registrazione.NumeroProtocollo, 15) 'NUMERO_PARTITA
                    I = I + 1 : CampoMovimenti(I) = Format(Registrazione.DataRegistrazione, "yyyyMMdd") 'DATA_LIQ
                    I = I + 1 : CampoMovimenti(I) = AdattaLunghezzaNumero(0, 8) 'numero


                    I = I + 1 : CampoMovimenti(I) = AdattaLunghezzaNumero(Registrazione.ImportoDocumento(Session("DC_TABELLE")), 8)
                    I = I + 1 : CampoMovimenti(I) = Space(3) ' MONETA
                    I = I + 1 : CampoMovimenti(I) = Space(20) ' CAUSALE

                    If CausaleContabile.TipoDocumento = "NC" Then
                        I = I + 1 : CampoMovimenti(I) = AdattaLunghezza("Nota Accr. Emessa", 20)  ' DESCRIZIONE 
                    Else
                        I = I + 1 : CampoMovimenti(I) = AdattaLunghezza("Fattura Emessa", 20) ' DESCRIZIONE 
                    End If

                    I = I + 1 : CampoMovimenti(I) = Space(80) ' DESCRIZIONE
                    I = I + 1 : CampoMovimenti(I) = Format(Registrazione.DataRegistrazione, "yyyyMMdd") 'DECORRENZA_PAGAMENTO		
                    I = I + 1 : CampoMovimenti(I) = Space(2) ' CODICE_PAGAMENTO
                    I = I + 1 : CampoMovimenti(I) = AdattaLunghezzaNumero("0", 8)

                    I = I + 1 : CampoMovimenti(I) = AccodaSpazio(RagioneSociale, 50) 'RAGIONE SOCIALE
                    I = I + 1 : CampoMovimenti(I) = AccodaSpazio(ResidenzaIndirizzo, 50) 'INDIRIZZO_1
                    I = I + 1 : CampoMovimenti(I) = AccodaSpazio(Cap, 5)
                    I = I + 1 : CampoMovimenti(I) = AccodaSpazio(ComuneP, 3)
                    I = I + 1 : CampoMovimenti(I) = AccodaSpazio(ProvinciaP, 3)
                    I = I + 1 : CampoMovimenti(I) = AdattaLunghezzaNumero(0, 11)
                    I = I + 1 : CampoMovimenti(I) = AccodaSpazio(CodiceFiscale, 16)
                    I = I + 1 : CampoMovimenti(I) = Space(20) 'COMPETENZA
                    I = I + 1 : CampoMovimenti(I) = Space(8) 'UTENTE ULTIMA VAR
                    I = I + 1 : CampoMovimenti(I) = "00000000" 'DATA ULTIMA VAR
                    I = I + 1 : CampoMovimenti(I) = Space(8) 'FASE
                    I = I + 1 : CampoMovimenti(I) = Space(6) 'UFFICIO
                    I = I + 1 : CampoMovimenti(I) = Space(15) 'CIG_O_CUP
                    I = I + 1 : CampoMovimenti(I) = "0" 'SPLIT
                    I = I + 1 : CampoMovimenti(I) = Space(3) 'CLASSE_NOTA_ARTICOLO
                    I = I + 1 : CampoMovimenti(I) = Space(2000) 'DE





                    Stringa = ""
                    For Z = 1 To I
                        Stringa = Stringa & CampoMovimenti(Z) & vbTab
                    Next Z
                    FileMovim.WriteLine(Stringa)



                    For IndRighe = 0 To Registrazione.Righe.Length - 1
                        If Not IsNothing(Registrazione.Righe(IndRighe)) Then
                            If Registrazione.Righe(IndRighe).Tipo = "" Then

                            End If
                        End If
                    Next
                End If
            Next


        Loop
        MyRsDB.Close()
        cn.Close()
        FileMovim.Close()
        FileClienti.Close()



        Btn_Testa.Text = "Movimenti"
        Btn_Riga.Text = "Clienti"
        Btn_Testa.Visible = True
        Btn_Riga.Visible = True


    End Sub



    Sub Export_ATS_Ver0()
        Dim cn As OleDbConnection
        Dim cnOsp As OleDbConnection
        Dim Anno As Integer
        Dim Mese As Integer
        Dim MeseContr As Integer
        Dim MySql As String

        Dim NomeFileClienti As String
        Dim NomeFileMovim As String
        Dim Stringa As String

        Dim CampoClienti(500) As String
        Dim CampoMovimenti(500) As String


        MyTable.Clear()
        MyTable.Columns.Clear()

        MyTable.Columns.Add("Numero Registrazione", GetType(String))
        MyTable.Columns.Add("Cliente", GetType(String))
        MyTable.Columns.Add("Segnalazione", GetType(String))

        NomeFileClienti = HostingEnvironment.ApplicationPhysicalPath() & "\Public\Farmaci_.txt"
        NomeFileMovim = HostingEnvironment.ApplicationPhysicalPath() & "\Public\Movim_.txt"


        Dim FileClienti As System.IO.TextWriter = System.IO.File.CreateText(NomeFileClienti)

        Dim FileMovim As System.IO.TextWriter = System.IO.File.CreateText(NomeFileMovim)


        Anno = Txt_Anno.Text
        Mese = DD_Mese.SelectedValue

        MeseContr = DD_Mese.SelectedValue


        MySql = "SELECT * " & _
                " FROM MovimentiContabiliTesta " & _
                " WHERE AnnoProtocollo = " & Txt_AnnoRif.Text

        If DD_Registro.SelectedValue <> "" Then
            MySql = MySql & " AND MovimentiContabiliTesta.RegistroIva = " & DD_Registro.SelectedValue
        End If
        If Tab_PeriodoData.ActiveTabIndex = 1 And IsDate(Txt_DataAl1.Text) And IsDate(Txt_DataDal1.Text) Then
            MySql = MySql & " AND (DataRegistrazione >= ? And DataRegistrazione <= ?) "
        Else
            MySql = MySql & " And MovimentiContabiliTesta.AnnoCompetenza = " & Txt_Anno.Text & " AND MovimentiContabiliTesta.MeseCompetenza = " & MeseContr & " "
        End If

        MySql = MySql & " AND (NumeroProtocollo >= " & Txt_DalDocumento.Text & " And NumeroProtocollo <= " & Txt_AlDocumento.Text & ") "

        cn = New Data.OleDb.OleDbConnection(Session("DC_GENERALE"))

        cn.Open()
        Dim cmd As New OleDbCommand()
        cmd.CommandText = MySql & " Order by NumeroProtocollo"
        cmd.Connection = cn
        If Tab_PeriodoData.ActiveTabIndex = 1 And IsDate(Txt_DataAl1.Text) And IsDate(Txt_DataDal1.Text) Then
            cmd.Parameters.AddWithValue("@DataDal", Txt_DataDal1.Text)
            cmd.Parameters.AddWithValue("@DataDal", Txt_DataAl1.Text)
        End If


        FileClienti.WriteLine("Matricola;CNome;Descrizione;DataMovimento; Importo")
        FileClienti.WriteLine(";;;;")

        FileMovim.WriteLine("NumeroFattura;DataFattura;SommadiImporto;MatricolaDebitore;IntFattura")


        Dim MyRsDB As OleDbDataReader = cmd.ExecuteReader()

        Do While MyRsDB.Read
            Dim Imponibile As Double
            Dim Registrazione As New Cls_MovimentoContabile
            Dim CausaleContabile As New Cls_CausaleContabile

            Registrazione.Leggi(Session("DC_GENERALE"), MyRsDB.Item("NumeroRegistrazione"))

            CausaleContabile.Codice = Registrazione.CausaleContabile
            CausaleContabile.Leggi(Session("DC_TABELLE"), CausaleContabile.Codice)


            Dim TipoAnagrafica As String
            Dim Ospiti As New ClsOspite
            Dim Parente As New Cls_Parenti
            Dim Regioni As New ClsUSL
            Dim Comune As New ClsComune

            If Registrazione.Tipologia = "" Then
                Dim CercaCf As Integer
                Dim SottocontoCf As Integer

                For CercaCf = 0 To Registrazione.Righe.Length - 1
                    If Not IsNothing(Registrazione.Righe(CercaCf)) Then
                        If Registrazione.Righe(CercaCf).Tipo = "CF" Then
                            SottocontoCf = Registrazione.Righe(CercaCf).SottocontoPartita
                        End If
                    End If
                Next

                If Int(SottocontoCf / 100) = Math.Round(SottocontoCf / 100, 2) Then
                    Ospiti.CodiceOspite = Int(SottocontoCf / 100)
                    Ospiti.Leggi(Session("DC_OSPITE"), Ospiti.CodiceOspite)
                    TipoAnagrafica = "O"
                Else
                    Parente.CodiceParente = SottocontoCf - (Int(SottocontoCf / 100) * 100)
                    Parente.CodiceOspite = Int(SottocontoCf / 100)
                    Parente.Leggi(Session("DC_OSPITE"), Parente.CodiceOspite, Parente.CodiceParente)
                    TipoAnagrafica = "P"
                End If
            Else
                If Mid(Registrazione.Tipologia & Space(10), 1, 1) = "C" Then
                    Comune.Provincia = Mid(Registrazione.Tipologia & Space(10), 2, 3)
                    Comune.Comune = Mid(Registrazione.Tipologia & Space(10), 5, 3)
                    Comune.Leggi(Session("DC_OSPITE"))

                    TipoAnagrafica = "C"
                End If
                If Mid(Registrazione.Tipologia & Space(10), 1, 1) = "J" Then
                    Comune.Provincia = Mid(Registrazione.Tipologia & Space(10), 2, 3)
                    Comune.Comune = Mid(Registrazione.Tipologia & Space(10), 5, 3)
                    Comune.Leggi(Session("DC_OSPITE"))
                    TipoAnagrafica = "C"
                End If
                If Mid(Registrazione.Tipologia & Space(10), 1, 1) = "R" Then
                    Regioni.CodiceRegione = Mid(Registrazione.Tipologia & Space(10), 2, 4)
                    Regioni.Leggi(Session("DC_OSPITE"))

                    TipoAnagrafica = "R"
                End If
            End If

            Dim Indice As Integer
            Dim I As Integer
            Dim CodiceIVA As String
            Dim Imposta As Double
            Dim Mastro As Integer = 0
            Dim Conto As Integer = 0
            Dim Sottoconto As Integer = 0
            Dim DescrizioneRiga As String = ""

            Imposta = 0
            Imponibile = 0
            CodiceIVA = ""
            For Indice = 0 To Registrazione.Righe.Length - 1
                If Not IsNothing(Registrazione.Righe(Indice)) Then
                    If Registrazione.Righe(Indice).Tipo = "IV" Then
                        Imponibile = Imponibile + Registrazione.Righe(Indice).Imponibile
                        Imposta = Imposta + Registrazione.Righe(Indice).Importo
                        CodiceIVA = Registrazione.Righe(Indice).CodiceIVA
                    End If

                    If Registrazione.Righe(Indice).RigaDaCausale = 3 Then
                        Mastro = Registrazione.Righe(Indice).MastroPartita
                        Conto = Registrazione.Righe(Indice).ContoPartita
                        Sottoconto = Registrazione.Righe(Indice).SottocontoPartita
                        DescrizioneRiga = Registrazione.Righe(Indice).Descrizione
                    End If
                End If
            Next
            Dim RagioneSociale As String = ""
            Dim Codice As String = ""
            Dim ResidenzaIndirizzo As String = ""
            Dim ComuneP As String = ""
            Dim ProvinciaP As String = ""
            Dim Cap As String = ""
            Dim PersonaFisica As String = "S"
            Dim PartitaIVA As String = ""
            Dim CodiceFiscale As String = ""
            Dim ContoEsportazione As String = ""



            If TipoAnagrafica = "O" Then
                Codice = Ospiti.CodiceOspite * 100
                RagioneSociale = Ospiti.Nome
                ResidenzaIndirizzo = Ospiti.RESIDENZAINDIRIZZO1
                Cap = Ospiti.RESIDENZACAP1
                ComuneP = Ospiti.RESIDENZACOMUNE1
                ProvinciaP = Ospiti.RESIDENZAPROVINCIA1

                CodiceFiscale = Ospiti.CODICEFISCALE

                ContoEsportazione = Ospiti.CONTOPERESATTO

            End If
            If TipoAnagrafica = "P" Then
                Codice = (Parente.CodiceOspite * 100) + Parente.CodiceParente    'CoidceCliente
                RagioneSociale = Parente.Nome
                ResidenzaIndirizzo = Parente.RESIDENZAINDIRIZZO1
                Cap = Parente.RESIDENZACAP1
                ComuneP = Parente.RESIDENZACOMUNE1
                ProvinciaP = Parente.RESIDENZAPROVINCIA1

                CodiceFiscale = Parente.CODICEFISCALE

                ContoEsportazione = Parente.CONTOPERESATTO
            End If


            If Val(ContoEsportazione) = 0 Then
                Dim myrigaerrore As System.Data.DataRow = MyTable.NewRow()
                myrigaerrore.Item("Numero Registrazione") = Registrazione.NumeroRegistrazione
                myrigaerrore.Item("Cliente") = RagioneSociale
                myrigaerrore.Item("Segnalazione") = " Matricola non indicata su questo utente"
                MyTable.Rows.Add(myrigaerrore)
            End If

            Dim cau As New Cls_CausaleContabile

            cau.Codice = Registrazione.CausaleContabile
            cau.Leggi(Session("DC_TABELLE"), cau.Codice)

            If cau.TipoDocumento <> "RE" Then
                Dim myrigaerrore As System.Data.DataRow = MyTable.NewRow()
                myrigaerrore.Item("Numero Registrazione") = Registrazione.NumeroRegistrazione
                myrigaerrore.Item("Cliente") = RagioneSociale
                myrigaerrore.Item("Segnalazione") = "Tipo Documento non esportabile"
                MyTable.Rows.Add(myrigaerrore)
            End If

            If cau.TipoDocumento = "RE" Then
                Dim PrimoFarmaco As Boolean = True
                cnOsp = New Data.OleDb.OleDbConnection(Session("DC_OSPITE"))

                cnOsp.Open()
                Dim cmdOsp As New OleDbCommand()
                cmdOsp.CommandText = " select * from addacr where NumeroRegistrazione = " & Registrazione.NumeroRegistrazione & " and (CodiceIva = '10' OR CodiceIva = '11')"
                cmdOsp.Connection = cnOsp
                Dim RsDB As OleDbDataReader = cmdOsp.ExecuteReader()
                Do While RsDB.Read
                    If PrimoFarmaco Then
                        I = 0
                        I = I + 1 : CampoClienti(I) = ContoEsportazione
                        I = I + 1 : CampoClienti(I) = RagioneSociale
                        I = I + 1 : CampoClienti(I) = ""
                        I = I + 1 : CampoClienti(I) = ""
                        Stringa = ""
                        For Z = 1 To I
                            Stringa = Stringa & CampoClienti(Z) & ";"
                        Next Z
                        FileClienti.WriteLine(Stringa)

                    End If
                    PrimoFarmaco = False
                    I = 0
                    I = I + 1
                    I = I + 1 : CampoClienti(I) = ""
                    I = I + 1 : CampoClienti(I) = ""
                    I = I + 1 : CampoClienti(I) = campodb(RsDB.Item("DESCRIZIONE"))
                    I = I + 1 : CampoClienti(I) = Format(campodbD(RsDB.Item("Data")), "dd/MM/yyyy")
                    I = I + 1 : CampoClienti(I) = Format(campodbN(RsDB.Item("Importo")), "#,##0.00")


                    Stringa = ""
                    For Z = 1 To I
                        Stringa = Stringa & CampoClienti(Z) & ";"
                    Next Z
                    FileClienti.WriteLine(Stringa)

                Loop
                RsDB.Close()


                Dim Totale As Double = 0

                For Indice = 0 To Registrazione.Righe.Length - 1
                    If Not IsNothing(Registrazione.Righe(Indice)) Then
                        If Registrazione.Righe(Indice).RigaDaCausale = 3 Then
                            Totale = Totale + Registrazione.Righe(Indice).Importo
                        End If
                        If Registrazione.Righe(Indice).RigaDaCausale = 4 Then
                            Totale = Totale + Registrazione.Righe(Indice).Importo
                        End If
                        If Registrazione.Righe(Indice).RigaDaCausale = 5 And Registrazione.Righe(Indice).TipoExtra <> "A10" And Registrazione.Righe(Indice).TipoExtra <> "A11" Then
                            Totale = Totale + Registrazione.Righe(Indice).Importo
                        End If
                        If Registrazione.Righe(Indice).RigaDaCausale = 6 Then
                            Totale = Totale - Registrazione.Righe(Indice).Importo
                        End If
                    End If
                Next

                I = 0

                I = I + 1 : CampoMovimenti(I) = Registrazione.NumeroRegistrazione
                I = I + 1 : CampoMovimenti(I) = Format(Registrazione.DataRegistrazione, "dd-MMM-yy")
                I = I + 1 : CampoMovimenti(I) = Format(Totale, "#,##0.00")
                I = I + 1 : CampoMovimenti(I) = ContoEsportazione
                I = I + 1 : CampoMovimenti(I) = RagioneSociale

                Stringa = ""
                For Z = 1 To I
                    Stringa = Stringa & CampoMovimenti(Z) & ";"
                Next Z
                FileMovim.WriteLine(Stringa)
            End If

        Loop
        MyRsDB.Close()
        cn.Close()
        FileMovim.Close()
        FileClienti.Close()



        Btn_Testa.Text = "Movimenti"
        Btn_Riga.Text = "Farmaci"
        Btn_Testa.Visible = True
        Btn_Riga.Visible = True

        GridView1.AutoGenerateColumns = True
        GridView1.DataSource = MyTable
        GridView1.Font.Size = 10
        GridView1.DataBind()
    End Sub

    Protected Sub Btn_Testa2_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles Btn_Testa2.Click

        Dim NomeFile As String

        If RB_PassePartout.Checked = True Then
            Try
                NomeFile = HostingEnvironment.ApplicationPhysicalPath() & "\Public\Testata_" & Format(Session("MYDATE"), "yyyyMMddHHmm") & ".Txt"

                If Dir(NomeFile) <> "" Then
                    Response.Clear()
                    Response.Buffer = True
                    Response.ContentType = "text/plain"
                    Response.AppendHeader("content-disposition", "attachment;filename=Testa.Csv")
                    Response.WriteFile(NomeFile)
                    Response.Flush()
                    Response.End()
                End If
            Catch ex As Exception
                If Dir(NomeFile) <> "" Then
                    Kill(NomeFile)
                End If
            End Try

  
        End If

        If RB_POLIFARMA.Checked = True Then
            Try
                NomeFile = HostingEnvironment.ApplicationPhysicalPath() & "\Public\Polifarma_FT_" & Format(Session("MYDATE"), "yyyyMMddHHmmss") & ".Txt"

                Response.Clear()
                Response.Buffer = True
                Response.ContentType = "text/plain"
                Response.AppendHeader("content-disposition", "attachment;filename=Fatture.txt")
                Response.WriteFile(NomeFile)
                Response.Flush()
                Response.End()

            Catch ex As Exception

            End Try

        End If


        If RB_DocumentiIncassi.Checked = True Then
            Try
                NomeFile = HostingEnvironment.ApplicationPhysicalPath() & "\Public\DocumentiIncassi_" & Format(Session("MYDATE"), "yyyyMMddHHmmss") & ".csv"

                Response.Clear()
                Response.Buffer = True
                Response.ContentType = "text/plain"
                Response.AppendHeader("content-disposition", "attachment;filename=DocumentiIncassi.csv")
                Response.WriteFile(NomeFile)
                Response.Flush()
                Response.End()

            Catch ex As Exception

            End Try

        End If


    End Sub

    Protected Sub Btn_Riga2_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles Btn_Riga2.Click
        Dim NomeFile As String
        Dim NomeFileRiga As String

        If RB_DocumentiIncassi.Checked = True Then
            Try
                NomeFile = HostingEnvironment.ApplicationPhysicalPath() & "\Public\DocumentiIncassi_" & Format(Session("MYDATE"), "yyyyMMddHHmmss") & ".Txt"

                Response.Clear()
                Response.Buffer = True
                Response.ContentType = "text/plain"
                Response.AppendHeader("content-disposition", "attachment;filename=DocumentiIncassi.csv")
                Response.WriteFile(NomeFile)
                Response.Flush()
                Response.End()

            Finally
                Kill(NomeFile)
            End Try

        End If

        If RB_PassePartout.Checked = True Then
            Try
                NomeFile = HostingEnvironment.ApplicationPhysicalPath() & "\Public\Righe_" & Format(Session("MYDATE"), "yyyyMMddHHmm") & ".Txt"

                If Dir(NomeFile) <> "" Then
                    Response.Clear()
                    Response.Buffer = True
                    Response.ContentType = "text/plain"
                    Response.AppendHeader("content-disposition", "attachment;filename=Righe.csv")
                    Response.WriteFile(NomeFile)
                    Response.Flush()
                    Response.End()
                End If
            Finally
                If Dir(NomeFile) <> "" Then
                    Kill(NomeFile)
                End If
            End Try



        End If


        If RB_POLIFARMA.Checked = True Then
            Try                
                'NomeFile = HostingEnvironment.ApplicationPhysicalPath() & "\Public\Polifarma_FT_" & Format(Session("MYDATE"), "yyyyMMddHHmmss") & ".Txt"
                NomeFile = HostingEnvironment.ApplicationPhysicalPath() & "\Public\Polifarma_IN_" & Format(Session("MYDATE"), "yyyyMMddHHmmss") & ".txt"


                Response.Clear()
                Response.Buffer = True
                Response.ContentType = "text/plain"
                Response.AppendHeader("content-disposition", "attachment;filename=Incassi.txt")
                Response.WriteFile(NomeFile)
                Response.Flush()
                Response.End()
            Finally
                Kill(NomeFile)
            End Try


        End If
    End Sub
    Function GetAnagrafica(ByVal Mastro As Long, ByVal Conto As Long, ByVal Sottoconto As Long) As String
        Dim cn As OleDbConnection




        cn = New Data.OleDb.OleDbConnection(Session("DC_OSPITE"))

        cn.Open()
        Dim cmd As New OleDbCommand()
        cmd.CommandText = ("select * from AnagraficaComune where " & _
                            "MastroCliente = ? And ContoCliente = ? And SottoContoCliente = ? And (TIPOLOGIA = 'D' or TIPOLOGIA = 'C' OR TIPOLOGIA = 'R')")
        cmd.Parameters.AddWithValue("@MastroCliente", Mastro)
        cmd.Parameters.AddWithValue("@ContoCliente", Conto)
        cmd.Parameters.AddWithValue("@SottoContoCliente", Sottoconto)
        cmd.Connection = cn


        Dim myPOSTreader As OleDbDataReader = cmd.ExecuteReader()
        If myPOSTreader.Read Then
            If campodb(myPOSTreader.Item("TIPOLOGIA")) = "C" Then

                GetAnagrafica = "C" & campodb(myPOSTreader.Item("CODICEPROVINCIA")) & campodb(myPOSTreader.Item("CODICECOMUNE"))
            End If
            If campodb(myPOSTreader.Item("TIPOLOGIA")) = "R" Then

                GetAnagrafica = "R" & campodb(myPOSTreader.Item("CODICEREGIONE"))
            End If
        End If


        cn.Close()


    End Function

    Function Export_polifarma() As Boolean
        Dim cn As OleDbConnection
        Dim MyRs As New ADODB.Recordset
        Dim RsIva As New ADODB.Recordset

        Dim RsSt As New ADODB.Recordset
        Dim UtilVb6 As New Cls_FunzioniVB6
        Dim Errori As String = ""
        Dim VerificaCodiceFiscale As New Cls_CodiceFiscale

        Dim NomeFile As String
        Dim NomeFileIN As String

        Session("MYDATE") = Now

        NomeFile = HostingEnvironment.ApplicationPhysicalPath() & "\Public\Polifarma_FT_" & Format(Session("MYDATE"), "yyyyMMddHHmmss") & ".Txt"
        NomeFileIN = HostingEnvironment.ApplicationPhysicalPath() & "\Public\Polifarma_IN_" & Format(Session("MYDATE"), "yyyyMMddHHmmss") & ".Txt"


        Dim twFatture As System.IO.TextWriter = System.IO.File.CreateText(NomeFile)
        Dim twIncassi As System.IO.TextWriter = System.IO.File.CreateText(NomeFileIN)

        cn = New Data.OleDb.OleDbConnection(Session("DC_GENERALE"))
        cn.Open()


        MyTable.Clear()
        MyTable.Columns.Clear()

        MyTable.Columns.Add("Numero Registrazione", GetType(String))
        MyTable.Columns.Add("Cliente", GetType(String))
        MyTable.Columns.Add("Segnalazione", GetType(String))

        Dim NumeroRegistrazioni As Integer


        NumeroRegistrazioni = 0
        Export_polifarma = True

        Dim cmd As New OleDbCommand()
        cmd.CommandText = "Select * From MovimentiContabiliTesta where (select count(*) from MovimentiContabiliRiga Where Numero = MovimentiContabiliTesta.NumeroRegistrazione  And RigaBollato = 1) =  0 And DataRegistrazione >= ? And DataRegistrazione <= ? "
        cmd.Parameters.AddWithValue("@DataDal", Txt_DataDal.Text)
        cmd.Parameters.AddWithValue("@DataAl", Txt_DataAl.Text)
        cmd.Connection = cn
        Dim MyRsDB As OleDbDataReader = cmd.ExecuteReader()
        Do While MyRsDB.Read

            Dim Riga As New Cls_CampiExport
            Dim Registrazione As New Cls_MovimentoContabile
            Dim RegistrazioneIncasso As New Cls_MovimentoContabile
            Dim CausaleContabile As New Cls_CausaleContabile

            Dim CausaleContabileDocumentoCollegato As New Cls_CausaleContabile

            Registrazione.NumeroRegistrazione = campodbN(MyRsDB.Item("NumeroRegistrazione"))
            Registrazione.Leggi(Session("DC_GENERALE"), Registrazione.NumeroRegistrazione)

            CausaleContabile.Leggi(Session("DC_TABELLE"), Registrazione.CausaleContabile)

            Dim Cliente As String = ""
            Dim CodiceOspite As Integer
            Dim CodiceParente As Integer = 0
            Dim Tipo As String
            Dim CodiceProvincia As String = ""
            Dim CodiceComune As String = ""
            Dim CodiceRegione As String = ""


            Tipo = Registrazione.Tipologia

            Dim cmdRd As New OleDbCommand()
            cmdRd.CommandText = "Select * From MovimentiContabiliRiga Where Numero = ?  And RigaDaCausale  = 1   "
            cmdRd.Connection = cn
            cmdRd.Parameters.AddWithValue("@Numero", Registrazione.NumeroRegistrazione)
            Dim MyReadSC As OleDbDataReader = cmdRd.ExecuteReader()
            If MyReadSC.Read Then
                Cliente = campodb(MyReadSC.Item("MastroPartita")) & "." & campodb(MyReadSC.Item("ContoPartita")) & "." & campodb(MyReadSC.Item("SottocontoPartita"))


                If CausaleContabile.Tipo = "P" Then
                    Dim PianoConti As New Cls_Pianodeiconti

                    PianoConti.Mastro = campodb(MyReadSC.Item("MastroPartita"))
                    PianoConti.Conto = campodb(MyReadSC.Item("ContoPartita"))
                    PianoConti.Sottoconto = campodb(MyReadSC.Item("SottocontoPartita"))
                    PianoConti.Decodfica(Session("DC_GENERALE"))

                    Tipo = GetAnagrafica(PianoConti.Mastro, PianoConti.Conto, PianoConti.Sottoconto)

                End If


                If Mid(Tipo & Space(10), 1, 1) = " " Then
                    CodiceOspite = Int(campodbN(MyReadSC.Item("SottocontoPartita")) / 100)
                    CodiceParente = campodbN(MyReadSC.Item("SottocontoPartita")) - (CodiceOspite * 100)

                    If CodiceParente = 0 And CodiceOspite > 0 Then
                        Tipo = "O" & Space(10)
                    End If
                    If CodiceParente > 0 And CodiceOspite > 0 Then
                        Tipo = "P" & Space(10)
                    End If
                End If

        

                If Mid(Tipo & Space(10), 1, 1) = "C" Then
                    CodiceProvincia = Mid(Tipo & Space(10), 2, 3)
                    CodiceComune = Mid(Tipo & Space(10), 5, 3)
                End If
                If Mid(Tipo & Space(10), 1, 1) = "J" Then
                    CodiceProvincia = Mid(Tipo & Space(10), 2, 3)
                    CodiceComune = Mid(Tipo & Space(10), 5, 3)
                End If
                If Mid(Tipo & Space(10), 1, 1) = "R" Then
                    CodiceRegione = Mid(Tipo & Space(10), 2, 4)
                End If

            End If
            MyReadSC.Close()

            Dim DO11_RAGIONESOCIALE As String = ""
            Dim DO11_PIVA As String = ""
            Dim DO11_CF As String = ""
            Dim DO11_INDIRIZZO As String = ""
            Dim DO11_CAP As String = ""
            Dim DO11_CITTA_NOME As String = ""
            Dim DO11_CITTA_ISTAT As String = ""
            Dim DO11_PROVINCIA As String = ""
            Dim DO11_CONDPAG_CODICE_SEN As String = ""
            Dim DO11_CONDPAG_NOME_SEN As String = ""
            Dim DO11_BANCA_CIN As String = ""
            Dim DO11_BANCA_ABI As String = ""
            Dim DO11_BANCA_CAB As String = ""
            Dim DO11_BANCA_CONTO As String = ""
            Dim DO11_BANCA_IBAN As String = ""
            Dim DO11_BANCA_IDMANDATO As String = ""
            Dim DO11_BANCA_DATAMANDATO As String = ""
            Dim AddDescrizione As String

            AddDescrizione = ""
            If Mid(Tipo & Space(10), 1, 1) = "O" Then
                Dim Ospite As New ClsOspite

                Ospite.CodiceOspite = CodiceOspite
                Ospite.Leggi(Session("DC_OSPITE"), Ospite.CodiceOspite)
                DO11_RAGIONESOCIALE = Ospite.CognomeOspite & ", " & Ospite.NomeOspite
                DO11_PIVA = ""
                DO11_CF = Ospite.CODICEFISCALE
                DO11_INDIRIZZO = Ospite.RESIDENZAINDIRIZZO1
                DO11_CAP = Ospite.RESIDENZACAP1
                Dim DcCom As New ClsComune

                DcCom.Provincia = Ospite.RESIDENZAPROVINCIA1
                DcCom.Comune = Ospite.RESIDENZACOMUNE1
                DcCom.Leggi(Session("DC_OSPITE"))

                DO11_CITTA_NOME = DcCom.Descrizione
                DO11_CITTA_ISTAT = DcCom.Provincia & DcCom.Comune

                Dim DcProv As New ClsComune

                DcProv.Provincia = Ospite.RESIDENZAPROVINCIA1
                DcProv.Comune = ""
                DcProv.Leggi(Session("DC_OSPITE"))
                DO11_PROVINCIA = DcProv.CodificaProvincia


                Dim Kl As New Cls_DatiOspiteParenteCentroServizio

                Kl.CodiceOspite = CodiceOspite
                Kl.CodiceParente = 0
                Kl.CentroServizio = Registrazione.CentroServizio
                Kl.Leggi(Session("DC_OSPITE"))
                If Kl.ModalitaPagamento <> "" Then
                    Ospite.MODALITAPAGAMENTO = Kl.ModalitaPagamento
                End If


                DO11_CONDPAG_CODICE_SEN = Ospite.MODALITAPAGAMENTO
                Dim MPAg As New ClsModalitaPagamento

                MPAg.Codice = Ospite.MODALITAPAGAMENTO
                MPAg.Leggi(Session("DC_OSPITE"))
                DO11_CONDPAG_NOME_SEN = MPAg.DESCRIZIONE


                Dim ModPag As New Cls_DatiPagamento

                ModPag.CodiceOspite = Ospite.CodiceOspite
                ModPag.CodiceParente = 0
                ModPag.LeggiUltimaDataData(Session("DC_OSPITE"), Registrazione.DataRegistrazione)

                DO11_BANCA_CIN = ModPag.Cin
                DO11_BANCA_ABI = ModPag.Abi
                DO11_BANCA_CAB = ModPag.Cab
                DO11_BANCA_CONTO = ModPag.CCBancario
                DO11_BANCA_IBAN = ModPag.CodiceInt & Format(Val(ModPag.NumeroControllo), "00") & ModPag.Cin & Format(Val(ModPag.Abi), "00000") & Format(Val(ModPag.Cab), "00000") & AdattaLunghezzaNumero(ModPag.CCBancario, 12)

                DO11_BANCA_IDMANDATO = ModPag.IdMandatoDebitore
                DO11_BANCA_DATAMANDATO = ModPag.DataMandatoDebitore
            End If

            If Mid(Tipo & Space(10), 1, 1) = "P" Then
                Dim Ospite As New Cls_Parenti

                Ospite.CodiceOspite = CodiceOspite
                Ospite.CodiceParente = CodiceParente
                Ospite.Leggi(Session("DC_OSPITE"), Ospite.CodiceOspite, Ospite.CodiceParente)
                DO11_RAGIONESOCIALE = Ospite.CognomeParente & ", " & Ospite.NomeParente
                DO11_PIVA = ""
                DO11_CF = Ospite.CODICEFISCALE
                DO11_INDIRIZZO = Ospite.RESIDENZAINDIRIZZO1
                DO11_CAP = Ospite.RESIDENZACAP1
                Dim DcCom As New ClsComune

                DcCom.Provincia = Ospite.RESIDENZAPROVINCIA1
                DcCom.Comune = Ospite.RESIDENZACOMUNE1
                DcCom.Leggi(Session("DC_OSPITE"))

                DO11_CITTA_NOME = DcCom.Descrizione
                DO11_CITTA_ISTAT = DcCom.Provincia & DcCom.Comune

                Dim DcProv As New ClsComune

                DcProv.Provincia = Ospite.RESIDENZAPROVINCIA1
                DcProv.Comune = ""
                DcProv.Leggi(Session("DC_OSPITE"))
                DO11_PROVINCIA = DcProv.CodificaProvincia
                DO11_CONDPAG_CODICE_SEN = Ospite.MODALITAPAGAMENTO

                Dim Kl As New Cls_DatiOspiteParenteCentroServizio

                Kl.CodiceOspite = CodiceOspite
                Kl.CodiceParente = CodiceParente
                Kl.CentroServizio = Registrazione.CentroServizio
                Kl.Leggi(Session("DC_OSPITE"))
                If Kl.ModalitaPagamento <> "" Then
                    Ospite.MODALITAPAGAMENTO = Kl.ModalitaPagamento
                End If

                Dim MPAg As New ClsModalitaPagamento

                MPAg.Codice = Ospite.MODALITAPAGAMENTO
                MPAg.Leggi(Session("DC_OSPITE"))
                DO11_CONDPAG_NOME_SEN = MPAg.DESCRIZIONE

                Dim ModPag As New Cls_DatiPagamento

                ModPag.CodiceOspite = Ospite.CodiceOspite
                ModPag.CodiceParente = Ospite.CodiceParente
                ModPag.LeggiUltimaDataData(Session("DC_OSPITE"), Registrazione.DataRegistrazione)

                DO11_BANCA_CIN = ModPag.Cin
                DO11_BANCA_ABI = ModPag.Abi
                DO11_BANCA_CAB = ModPag.Cab
                DO11_BANCA_CONTO = ModPag.CCBancario
                DO11_BANCA_IBAN = ModPag.CodiceInt & Format(Val(ModPag.NumeroControllo), "00") & ModPag.Cin & Format(Val(ModPag.Abi), "00000") & Format(Val(ModPag.Cab), "00000") & AdattaLunghezzaNumero(ModPag.CCBancario, 12)

                DO11_BANCA_IDMANDATO = ModPag.IdMandatoDebitore
                DO11_BANCA_DATAMANDATO = ModPag.DataMandatoDebitore
            End If
            If Mid(Tipo & Space(10), 1, 1) = "C" Then
                Dim Ospite As New ClsComune

                Ospite.Provincia = CodiceProvincia
                Ospite.Comune = CodiceComune
                Ospite.Leggi(Session("DC_OSPITE"))
                DO11_RAGIONESOCIALE = Ospite.Descrizione
                DO11_PIVA = campodbN(Ospite.PartitaIVA)
                DO11_CF = Ospite.CodiceFiscale
                DO11_INDIRIZZO = Ospite.RESIDENZAINDIRIZZO1
                DO11_CAP = Ospite.RESIDENZACAP1
                Dim DcCom As New ClsComune

                DcCom.Provincia = Ospite.RESIDENZAPROVINCIA1
                DcCom.Comune = Ospite.RESIDENZACOMUNE1
                DcCom.Leggi(Session("DC_OSPITE"))

                DO11_CITTA_NOME = DcCom.Descrizione
                DO11_CITTA_ISTAT = DcCom.Provincia & DcCom.Comune

                Dim DcProv As New ClsComune

                DcProv.Provincia = Ospite.RESIDENZAPROVINCIA1
                DcProv.Comune = ""
                DcProv.Leggi(Session("DC_OSPITE"))
                DO11_PROVINCIA = DcProv.CodificaProvincia
                DO11_CONDPAG_CODICE_SEN = Ospite.ModalitaPagamento
                Dim MPAg As New ClsModalitaPagamento

                MPAg.Codice = Ospite.ModalitaPagamento
                MPAg.Leggi(Session("DC_OSPITE"))
                DO11_CONDPAG_NOME_SEN = MPAg.DESCRIZIONE

                Dim cnOspiti As OleDbConnection

                cnOspiti = New Data.OleDb.OleDbConnection(Session("DC_OSPITE"))

                cnOspiti.Open()

                Dim cmdC As New OleDbCommand()
                cmdC.CommandText = ("select * from NoteFatture where CodiceProvincia = ? And CodiceComune = ?")
                cmdC.Parameters.AddWithValue("@CodiceProvincia", CodiceProvincia)
                cmdC.Parameters.AddWithValue("@CodiceComune", CodiceComune)
                cmdC.Connection = cnOspiti
                Dim RdCom As OleDbDataReader = cmdC.ExecuteReader()
                If RdCom.Read Then
                    AddDescrizione = campodb(RdCom.Item("NoteUp"))
                End If
                RdCom.Close()
                cnOspiti.Close()
            End If
            If Mid(Tipo & Space(10), 1, 1) = "J" Then
                Dim Ospite As New ClsComune

                Ospite.Provincia = CodiceProvincia
                Ospite.Comune = CodiceComune
                Ospite.Leggi(Session("DC_OSPITE"))
                DO11_RAGIONESOCIALE = Ospite.Descrizione
                DO11_PIVA = campodbN(Ospite.PartitaIVA)
                DO11_CF = Ospite.CodiceFiscale
                DO11_INDIRIZZO = Ospite.RESIDENZAINDIRIZZO1
                DO11_CAP = Ospite.RESIDENZACAP1
                Dim DcCom As New ClsComune

                DcCom.Provincia = Ospite.RESIDENZAPROVINCIA1
                DcCom.Comune = Ospite.RESIDENZACOMUNE1
                DcCom.Leggi(Session("DC_OSPITE"))

                DO11_CITTA_NOME = DcCom.Descrizione
                DO11_CITTA_ISTAT = DcCom.Provincia & DcCom.Comune

                Dim DcProv As New ClsComune

                DcProv.Provincia = Ospite.RESIDENZAPROVINCIA1
                DcProv.Comune = ""
                DcProv.Leggi(Session("DC_OSPITE"))
                DO11_PROVINCIA = DcProv.CodificaProvincia
                DO11_CONDPAG_CODICE_SEN = Ospite.ModalitaPagamento
                Dim MPAg As New ClsModalitaPagamento

                MPAg.Codice = Ospite.ModalitaPagamento
                MPAg.Leggi(Session("DC_OSPITE"))
                DO11_CONDPAG_NOME_SEN = MPAg.DESCRIZIONE

                Dim cnOspiti As OleDbConnection

                cnOspiti = New Data.OleDb.OleDbConnection(Session("DC_OSPITE"))

                cnOspiti.Open()

                Dim cmdC As New OleDbCommand()
                cmdC.CommandText = ("select * from NoteFatture where CodiceProvincia = ? And CodiceComune = ?")
                cmdC.Parameters.AddWithValue("@CodiceProvincia", CodiceProvincia)
                cmdC.Parameters.AddWithValue("@CodiceComune", CodiceComune)
                cmdC.Connection = cnOspiti
                Dim RdCom As OleDbDataReader = cmdC.ExecuteReader()
                If RdCom.Read Then
                    AddDescrizione = campodb(RdCom.Item("NoteUp"))
                End If
                RdCom.Close()
                cnOspiti.Close()
            End If
            If Mid(Tipo & Space(10), 1, 1) = "R" Then
                Dim Ospite As New ClsUSL

                Ospite.CodiceRegione = CodiceRegione
                Ospite.Leggi(Session("DC_OSPITE"))
                DO11_RAGIONESOCIALE = Ospite.Nome
                DO11_PIVA = campodbN(Ospite.PARTITAIVA)
                DO11_CF = Ospite.CodiceFiscale
                DO11_INDIRIZZO = Ospite.RESIDENZAINDIRIZZO1
                DO11_CAP = Ospite.RESIDENZACAP1
                Dim DcCom As New ClsComune

                DcCom.Provincia = Ospite.RESIDENZAPROVINCIA1
                DcCom.Comune = Ospite.RESIDENZACOMUNE1
                DcCom.Leggi(Session("DC_OSPITE"))

                DO11_CITTA_NOME = DcCom.Descrizione
                DO11_CITTA_ISTAT = DcCom.Provincia & DcCom.Comune

                Dim DcProv As New ClsComune

                DcProv.Provincia = Ospite.RESIDENZAPROVINCIA1
                DcProv.Comune = ""
                DcProv.Leggi(Session("DC_OSPITE"))
                DO11_PROVINCIA = DcProv.CodificaProvincia
                DO11_CONDPAG_CODICE_SEN = Ospite.ModalitaPagamento
                Dim MPAg As New ClsModalitaPagamento

                MPAg.Codice = Ospite.ModalitaPagamento
                MPAg.Leggi(Session("DC_OSPITE"))
                DO11_CONDPAG_NOME_SEN = MPAg.DESCRIZIONE
            End If



            If CausaleContabile.Tipo = "P" Then
                Riga.Tipodoc = 3

                RegistrazioneIncasso.NumeroRegistrazione = campodbN(MyRsDB.Item("NumeroRegistrazione"))
                RegistrazioneIncasso.Leggi(Session("DC_GENERALE"), Registrazione.NumeroRegistrazione)

                Dim lk As New Cls_Legami
                Dim RigaRecordIncasso As String = ""

                lk.Leggi(Session("DC_GENERALE"), 0, Registrazione.NumeroRegistrazione)

                Dim TabTrascodificaCausale As New Cls_TabellaTrascodificheEsportazioni

                Dim IndDocu As Integer
                For IndDocu = 0 To 100
                    If lk.NumeroDocumento(IndDocu) > 0 Then
                        Registrazione.Leggi(Session("DC_GENERALE"), lk.NumeroDocumento(IndDocu))

                        CausaleContabileDocumentoCollegato.Codice = Registrazione.CausaleContabile

                        CausaleContabileDocumentoCollegato.Leggi(Session("DC_TABELLE"), CausaleContabileDocumentoCollegato.Codice)


                        RigaRecordIncasso = ""

                        If IsNothing(DO11_CF) Then
                            DO11_CF = ""
                        End If
                        If campodb(DO11_CF).Trim = "" Then
                            If IsNothing(DO11_PIVA) Then
                                DO11_PIVA = 0
                            End If
                            RigaRecordIncasso = RigaRecordIncasso & AdattaLunghezzaNumero(Val(DO11_PIVA), 11) & Space(5)
                        Else
                            RigaRecordIncasso = RigaRecordIncasso & AdattaLunghezza(DO11_CF, 16)
                        End If

                        RigaRecordIncasso = RigaRecordIncasso & Format(RegistrazioneIncasso.DataRegistrazione, "yyyyMMdd")
                        Dim TipoCausale As String = "CRD"




                        TabTrascodificaCausale.TIPOTAB = "CS"
                        TabTrascodificaCausale.SENIOR = RegistrazioneIncasso.CausaleContabile
                        TabTrascodificaCausale.EXPORT = "0"
                        TabTrascodificaCausale.Leggi(Session("DC_TABELLE"))

                        If TabTrascodificaCausale.EXPORT = "0" Or TabTrascodificaCausale.EXPORT = "" Then
                            RigaRecordIncasso = RigaRecordIncasso & "CRD"
                        Else
                            RigaRecordIncasso = RigaRecordIncasso & TabTrascodificaCausale.EXPORT
                        End If
                        RigaRecordIncasso = RigaRecordIncasso & AdattaLunghezzaNumero(Registrazione.AnnoProtocollo, 4)
                        RigaRecordIncasso = RigaRecordIncasso & AdattaLunghezzaNumero(Registrazione.NumeroProtocollo, 8)

                        If lk.Importo(IndDocu) > 0 Then
                            RigaRecordIncasso = RigaRecordIncasso & AdattaLunghezzaNumero(Math.Round(lk.Importo(IndDocu), 2) * 100, 9)
                            RigaRecordIncasso = RigaRecordIncasso & RegistrazioneIncasso.Righe(0).DareAvere
                        Else
                            RigaRecordIncasso = RigaRecordIncasso & AdattaLunghezzaNumero(Math.Abs(Math.Round(lk.Importo(IndDocu), 2)) * 100, 9)
                            If RegistrazioneIncasso.Righe(0).DareAvere = "D" Then
                                RigaRecordIncasso = RigaRecordIncasso & "A"
                            Else
                                RigaRecordIncasso = RigaRecordIncasso & "D"
                            End If
                        End If


                        RigaRecordIncasso = RigaRecordIncasso & AdattaLunghezzaNumero("123015000001", 12)
                        RigaRecordIncasso = RigaRecordIncasso & Format(RegistrazioneIncasso.NumeroRegistrazione, "0000000000")

                        twIncassi.WriteLine(RigaRecordIncasso)



                    End If
                Next

                Dim TabTrascodificaCC As New Cls_TabellaTrascodificheEsportazioni

                If RegistrazioneIncasso.Righe(1).Importo > 0 Then
                    RigaRecordIncasso = ""

                    If IsNothing(DO11_CF) Then
                        DO11_CF = ""
                    End If
                    If campodb(DO11_CF).Trim = "" Then
                        If IsNothing(DO11_PIVA) Then
                            DO11_PIVA = 0
                        End If
                        RigaRecordIncasso = RigaRecordIncasso & AdattaLunghezzaNumero(Val(DO11_PIVA), 11) & Space(5)
                    Else
                        RigaRecordIncasso = RigaRecordIncasso & AdattaLunghezza(DO11_CF, 16)
                    End If

                    RigaRecordIncasso = RigaRecordIncasso & Format(RegistrazioneIncasso.DataRegistrazione, "yyyyMMdd")



                    TabTrascodificaCausale.TIPOTAB = "CS"
                    TabTrascodificaCausale.SENIOR = RegistrazioneIncasso.CausaleContabile
                    TabTrascodificaCausale.EXPORT = "0"
                    TabTrascodificaCausale.Leggi(Session("DC_TABELLE"))

                    If TabTrascodificaCausale.EXPORT = "0" Or TabTrascodificaCausale.EXPORT = "" Then
                        RigaRecordIncasso = RigaRecordIncasso & "CRD"
                    Else
                        RigaRecordIncasso = RigaRecordIncasso & TabTrascodificaCausale.EXPORT
                    End If
                    RigaRecordIncasso = RigaRecordIncasso & AdattaLunghezzaNumero(0, 4)
                    RigaRecordIncasso = RigaRecordIncasso & AdattaLunghezzaNumero(0, 8)
                    RigaRecordIncasso = RigaRecordIncasso & AdattaLunghezzaNumero(Math.Round(RegistrazioneIncasso.Righe(1).Importo, 2) * 100, 9)
                    RigaRecordIncasso = RigaRecordIncasso & RegistrazioneIncasso.Righe(1).DareAvere

                    TabTrascodificaCC.TIPOTAB = "CC"
                    TabTrascodificaCC.SENIOR = RegistrazioneIncasso.Righe(1).MastroPartita & "." & RegistrazioneIncasso.Righe(1).ContoPartita & "." & RegistrazioneIncasso.Righe(1).SottocontoPartita
                    TabTrascodificaCC.EXPORT = 0
                    TabTrascodificaCC.Leggi(Session("DC_TABELLE"))
                    RigaRecordIncasso = RigaRecordIncasso & AdattaLunghezzaNumero(TabTrascodificaCC.EXPORT, 12)
                    RigaRecordIncasso = RigaRecordIncasso & Format(RegistrazioneIncasso.NumeroRegistrazione, "0000000000")

                    twIncassi.WriteLine(RigaRecordIncasso)
                End If


                RigaRecordIncasso = ""


                Dim InRiga As Integer
                For InRiga = 2 To 100
                    If Not IsNothing(RegistrazioneIncasso.Righe(InRiga)) Then
                        If RegistrazioneIncasso.Righe(InRiga).RigaDaCausale = 3 Then
                            If RegistrazioneIncasso.Righe(InRiga).Importo <> 0 Then
                                RigaRecordIncasso = ""

                                If IsNothing(DO11_CF) Then
                                    DO11_CF = ""
                                End If
                                If campodb(DO11_CF).Trim = "" Then
                                    If IsNothing(DO11_PIVA) Then
                                        DO11_PIVA = 0
                                    End If
                                    RigaRecordIncasso = RigaRecordIncasso & AdattaLunghezzaNumero(Val(DO11_PIVA), 11) & Space(5)
                                Else
                                    RigaRecordIncasso = RigaRecordIncasso & AdattaLunghezza(DO11_CF, 16)
                                End If

                                RigaRecordIncasso = RigaRecordIncasso & Format(RegistrazioneIncasso.DataRegistrazione, "yyyyMMdd")

                                Dim TabTrascodificaCausale1 As New Cls_TabellaTrascodificheEsportazioni


                                TabTrascodificaCausale1.TIPOTAB = "CS"
                                TabTrascodificaCausale1.SENIOR = RegistrazioneIncasso.CausaleContabile
                                TabTrascodificaCausale1.EXPORT = "0"
                                TabTrascodificaCausale1.Leggi(Session("DC_TABELLE"))

                                If TabTrascodificaCausale1.EXPORT = "0" Or TabTrascodificaCausale1.EXPORT = "" Then
                                    RigaRecordIncasso = RigaRecordIncasso & "CRD"
                                Else
                                    RigaRecordIncasso = RigaRecordIncasso & TabTrascodificaCausale1.EXPORT
                                End If
                                RigaRecordIncasso = RigaRecordIncasso & AdattaLunghezzaNumero(Registrazione.AnnoProtocollo, 4)
                                RigaRecordIncasso = RigaRecordIncasso & AdattaLunghezzaNumero(Registrazione.NumeroProtocollo, 8)

                                RigaRecordIncasso = RigaRecordIncasso & AdattaLunghezzaNumero(Math.Round(RegistrazioneIncasso.Righe(InRiga).Importo, 2) * 100, 9)
                                RigaRecordIncasso = RigaRecordIncasso & RegistrazioneIncasso.Righe(InRiga).DareAvere


                                TabTrascodificaCC.TIPOTAB = "CC"
                                TabTrascodificaCC.SENIOR = RegistrazioneIncasso.Righe(InRiga).MastroPartita & "." & RegistrazioneIncasso.Righe(InRiga).ContoPartita & "." & RegistrazioneIncasso.Righe(InRiga).SottocontoPartita
                                TabTrascodificaCC.EXPORT = 0
                                TabTrascodificaCC.Leggi(Session("DC_TABELLE"))
                                RigaRecordIncasso = RigaRecordIncasso & AdattaLunghezzaNumero(TabTrascodificaCC.EXPORT, 11)
                                RigaRecordIncasso = RigaRecordIncasso & Format(RegistrazioneIncasso.NumeroRegistrazione, "0000000000")

                                twIncassi.WriteLine(RigaRecordIncasso)
                            End If
                        End If
                        If RegistrazioneIncasso.Righe(InRiga).RigaDaCausale = 4 Then
                            If RegistrazioneIncasso.Righe(InRiga).Importo > 0 Then
                                RigaRecordIncasso = ""

                                If IsNothing(DO11_CF) Then
                                    DO11_CF = ""
                                End If
                                If campodb(DO11_CF).Trim = "" Then
                                    If IsNothing(DO11_PIVA) Then
                                        DO11_PIVA = 0
                                    End If
                                    RigaRecordIncasso = RigaRecordIncasso & AdattaLunghezzaNumero(Val(DO11_PIVA), 11) & Space(5)
                                Else
                                    RigaRecordIncasso = RigaRecordIncasso & AdattaLunghezza(DO11_CF, 16)
                                End If

                                RigaRecordIncasso = RigaRecordIncasso & Format(RegistrazioneIncasso.DataRegistrazione, "yyyyMMdd")


                                Dim TabTrascodificaCausale1 As New Cls_TabellaTrascodificheEsportazioni


                                TabTrascodificaCausale1.TIPOTAB = "CS"
                                TabTrascodificaCausale1.SENIOR = RegistrazioneIncasso.CausaleContabile
                                TabTrascodificaCausale1.EXPORT = 0
                                TabTrascodificaCausale1.Leggi(Session("DC_TABELLE"))

                                If TabTrascodificaCausale1.EXPORT = "0" Or TabTrascodificaCausale1.EXPORT = "" Then
                                    RigaRecordIncasso = RigaRecordIncasso & "CRD"
                                Else
                                    RigaRecordIncasso = RigaRecordIncasso & TabTrascodificaCausale1.EXPORT
                                End If

                                RigaRecordIncasso = RigaRecordIncasso & AdattaLunghezzaNumero(Registrazione.AnnoProtocollo, 4)
                                RigaRecordIncasso = RigaRecordIncasso & AdattaLunghezzaNumero(Registrazione.NumeroProtocollo, 8)

                                RigaRecordIncasso = RigaRecordIncasso & AdattaLunghezzaNumero(Math.Round(RegistrazioneIncasso.Righe(InRiga).Importo, 2) * 100, 9)
                                RigaRecordIncasso = RigaRecordIncasso & RegistrazioneIncasso.Righe(InRiga).DareAvere


                                TabTrascodificaCC.TIPOTAB = "CC"
                                TabTrascodificaCC.SENIOR = RegistrazioneIncasso.Righe(InRiga).MastroPartita & "." & RegistrazioneIncasso.Righe(InRiga).ContoPartita & "." & RegistrazioneIncasso.Righe(InRiga).SottocontoPartita
                                TabTrascodificaCC.EXPORT = 0
                                TabTrascodificaCC.Leggi(Session("DC_TABELLE"))
                                RigaRecordIncasso = RigaRecordIncasso & AdattaLunghezzaNumero(TabTrascodificaCC.EXPORT, 11)
                                RigaRecordIncasso = RigaRecordIncasso & Format(RegistrazioneIncasso.NumeroRegistrazione, "0000000000")

                                twIncassi.WriteLine(RigaRecordIncasso)
                            End If
                        End If
                        If RegistrazioneIncasso.Righe(InRiga).RigaDaCausale = 6 Then
                            If RegistrazioneIncasso.Righe(InRiga).Importo > 0 Then
                                RigaRecordIncasso = ""

                                If IsNothing(DO11_CF) Then
                                    DO11_CF = ""
                                End If
                                If campodb(DO11_CF).Trim = "" Then
                                    If IsNothing(DO11_PIVA) Then
                                        DO11_PIVA = 0
                                    End If
                                    RigaRecordIncasso = RigaRecordIncasso & AdattaLunghezzaNumero(Val(DO11_PIVA), 11) & Space(5)
                                Else
                                    RigaRecordIncasso = RigaRecordIncasso & AdattaLunghezza(DO11_CF, 16)
                                End If

                                RigaRecordIncasso = RigaRecordIncasso & Format(RegistrazioneIncasso.DataRegistrazione, "yyyyMMdd")

                                Dim TabTrascodificaCausale1 As New Cls_TabellaTrascodificheEsportazioni


                                TabTrascodificaCausale1.TIPOTAB = "CS"
                                TabTrascodificaCausale1.SENIOR = RegistrazioneIncasso.CausaleContabile
                                TabTrascodificaCausale1.EXPORT = 0
                                TabTrascodificaCausale1.Leggi(Session("DC_TABELLE"))

                                If TabTrascodificaCausale1.EXPORT = "0" Or TabTrascodificaCausale1.EXPORT = "" Then
                                    RigaRecordIncasso = RigaRecordIncasso & "CRD"
                                Else
                                    RigaRecordIncasso = RigaRecordIncasso & TabTrascodificaCausale1.EXPORT
                                End If

                                RigaRecordIncasso = RigaRecordIncasso & AdattaLunghezzaNumero(Registrazione.AnnoProtocollo, 4)
                                RigaRecordIncasso = RigaRecordIncasso & AdattaLunghezzaNumero(Registrazione.NumeroProtocollo, 8)

                                RigaRecordIncasso = RigaRecordIncasso & AdattaLunghezzaNumero(Math.Round(RegistrazioneIncasso.Righe(InRiga).Importo, 2) * 100, 9)
                                RigaRecordIncasso = RigaRecordIncasso & RegistrazioneIncasso.Righe(InRiga).DareAvere


                                TabTrascodificaCC.TIPOTAB = "CC"
                                TabTrascodificaCC.SENIOR = RegistrazioneIncasso.Righe(InRiga).MastroPartita & "." & RegistrazioneIncasso.Righe(InRiga).ContoPartita & "." & RegistrazioneIncasso.Righe(InRiga).SottocontoPartita
                                TabTrascodificaCC.EXPORT = 0
                                TabTrascodificaCC.Leggi(Session("DC_TABELLE"))
                                RigaRecordIncasso = RigaRecordIncasso & AdattaLunghezzaNumero(TabTrascodificaCC.EXPORT, 11)
                                RigaRecordIncasso = RigaRecordIncasso & Format(RegistrazioneIncasso.NumeroRegistrazione, "0000000000")

                                twIncassi.WriteLine(RigaRecordIncasso)
                            End If
                        End If
                    End If
                Next


                If VerificaCodiceFiscale.Check_CodiceFiscale(DO11_CF) = False And (Mid(Tipo & Space(10), 1, 1) = "O" Or Mid(Tipo & Space(10), 1, 1) = "P") Then
                    Dim myrigaerrore As System.Data.DataRow = MyTable.NewRow()
                    myrigaerrore.Item("Numero Registrazione") = Registrazione.NumeroRegistrazione
                    myrigaerrore.Item("Cliente") = DO11_RAGIONESOCIALE
                    myrigaerrore.Item("Segnalazione") = " Codice Fiscale formalmente errato per ospite "
                    MyTable.Rows.Add(myrigaerrore)
                    Export_polifarma = False
                End If


            Else

                Dim RigaRecord As String

                RigaRecord = ""
                RigaRecord = RigaRecord & Registrazione.AnnoProtocollo
                RigaRecord = RigaRecord & Format(Registrazione.DataRegistrazione, "yyyyMMdd")
                RigaRecord = RigaRecord & AdattaLunghezzaNumero(Registrazione.NumeroProtocollo, 7)
                RigaRecord = RigaRecord & "01"
                If CausaleContabile.TipoDocumento = "NC" Then
                    RigaRecord = RigaRecord & "N"
                Else
                    RigaRecord = RigaRecord & "F"
                End If
                RigaRecord = RigaRecord & "0001"
                RigaRecord = RigaRecord & Registrazione.AnnoProtocollo
                RigaRecord = RigaRecord & AdattaLunghezzaNumero(Registrazione.NumeroProtocollo, 7)
                RigaRecord = RigaRecord & Format(Registrazione.DataRegistrazione, "dd/MM/yyyy")
                If CausaleContabile.TipoDocumento = "NC" Then
                    RigaRecord = RigaRecord & "N"
                Else
                    RigaRecord = RigaRecord & "F"
                End If



                If VerificaCodiceFiscale.Check_CodiceFiscale(DO11_CF) = False And (Mid(Tipo & Space(10), 1, 1) = "O" Or Mid(Tipo & Space(10), 1, 1) = "P") Then
                    Dim myrigaerrore As System.Data.DataRow = MyTable.NewRow()
                    myrigaerrore.Item("Numero Registrazione") = Registrazione.NumeroRegistrazione
                    myrigaerrore.Item("Cliente") = DO11_RAGIONESOCIALE
                    myrigaerrore.Item("Segnalazione") = " Codice Fiscale formalmente errato per ospite "
                    MyTable.Rows.Add(myrigaerrore)
                    Export_polifarma = False
                End If


                RigaRecord = RigaRecord & AdattaLunghezza(DO11_CF, 16)

                twFatture.WriteLine(RigaRecord)

                RigaRecord = ""

                Dim Imponibile As Double = 0
                Dim Imposta As Double = 0


                For Indice = 0 To Registrazione.Righe.Length - 1
                    If Not IsNothing(Registrazione.Righe(Indice)) Then
                        If Registrazione.Righe(Indice).Tipo = "IV" And (Registrazione.Righe(Indice).Imponibile > 0 Or Registrazione.Righe(Indice).Importo > 0) Then
                            Imponibile = Imponibile + Registrazione.Righe(Indice).Imponibile
                            Imposta = Imposta + Registrazione.Righe(Indice).Importo
                        End If
                    End If
                Next



                RigaRecord = ""
                RigaRecord = RigaRecord & Registrazione.AnnoProtocollo
                RigaRecord = RigaRecord & Format(Registrazione.DataRegistrazione, "yyyyMMdd")
                RigaRecord = RigaRecord & AdattaLunghezzaNumero(Registrazione.NumeroProtocollo, 7)
                RigaRecord = RigaRecord & "02"
                If CausaleContabile.TipoDocumento = "NC" Then
                    RigaRecord = RigaRecord & "N"
                Else
                    RigaRecord = RigaRecord & "F"
                End If
                RigaRecord = RigaRecord & "0001"
                RigaRecord = RigaRecord & AdattaLunghezzaNumero(Math.Round(Registrazione.ImportoDocumento(Session("DC_TABELLE")), 2) * 100, 15)
                RigaRecord = RigaRecord & AdattaLunghezzaNumero(Math.Round(Imponibile, 2) * 100, 15)
                RigaRecord = RigaRecord & AdattaLunghezzaNumero(Math.Round(Imposta, 2) * 100, 15)

                twFatture.WriteLine(RigaRecord)


                Dim Progressivo As Integer = 0
                Dim TotImponibile1 As Double = 0
                Dim TotImponibile2 As Double = 0

                Dim TotImposta1 As Double = 0
                Dim TotImposta2 As Double = 0

                For Indice = 0 To Registrazione.Righe.Length - 1
                    If Not IsNothing(Registrazione.Righe(Indice)) Then
                        If Registrazione.Righe(Indice).Tipo = "IV" And (Registrazione.Righe(Indice).Imponibile > 0 Or Registrazione.Righe(Indice).Importo > 0) Then
                            Progressivo = Progressivo + 1
                            RigaRecord = ""
                            RigaRecord = RigaRecord & Registrazione.AnnoProtocollo
                            RigaRecord = RigaRecord & Format(Registrazione.DataRegistrazione, "yyyyMMdd")
                            RigaRecord = RigaRecord & AdattaLunghezzaNumero(Registrazione.NumeroProtocollo, 7)
                            RigaRecord = RigaRecord & "03"
                            If CausaleContabile.TipoDocumento = "NC" Then
                                RigaRecord = RigaRecord & "N"
                            Else
                                RigaRecord = RigaRecord & "F"
                            End If
                            RigaRecord = RigaRecord & AdattaLunghezzaNumero(Progressivo, 4)

                            Dim TabTrascodifica As New Cls_TabellaTrascodificheEsportazioni


                            TabTrascodifica.TIPOTAB = "IV"
                            TabTrascodifica.SENIOR = Registrazione.Righe(Indice).CodiceIVA
                            TabTrascodifica.EXPORT = 0
                            TabTrascodifica.Leggi(Session("DC_TABELLE"))

                            RigaRecord = RigaRecord & TabTrascodifica.EXPORT

                            RigaRecord = RigaRecord & AdattaLunghezzaNumero(Math.Round(Registrazione.Righe(Indice).Imponibile, 2) * 100, 15)
                            RigaRecord = RigaRecord & AdattaLunghezzaNumero(Math.Round(Registrazione.Righe(Indice).Importo, 2) * 100, 15)
                            RigaRecord = RigaRecord & Registrazione.Righe(Indice).DareAvere

                            twFatture.WriteLine(RigaRecord)
                        End If

                        If Registrazione.Righe(Indice).RigaDaCausale >= 3 Then
                            If Mid(Tipo & Space(10), 1, 1) = "R" Or Mid(Tipo & Space(10), 1, 1) = "C" Then
                                Dim MR As New Cls_rettatotale

                                MR.CODICEOSPITE = Int(Registrazione.Righe(Indice).SottocontoContropartita / 100)
                                MR.RettaTotaleADataSenzaCSERV(Session("DC_OSPITE"), Int(Registrazione.Righe(Indice).SottocontoContropartita / 100), Registrazione.DataRegistrazione)
                                If MR.TipoRetta = "01" Then
                                    TotImponibile1 = TotImponibile1 + Registrazione.Righe(Indice).Importo
                                Else
                                    TotImponibile2 = TotImponibile2 + Registrazione.Righe(Indice).Importo
                                End If
                            End If
                        End If
                    End If
                Next


                REM CENTRO DI COSTO LEGATO AL CENTRO SERVIZIO?
                If Mid(Tipo & Space(10), 1, 1) = "R" Or Mid(Tipo & Space(10), 1, 1) = "C" Then
                    If TotImponibile1 > 0 Then
                        RigaRecord = ""
                        RigaRecord = RigaRecord & Registrazione.AnnoProtocollo
                        RigaRecord = RigaRecord & Format(Registrazione.DataRegistrazione, "yyyyMMdd")
                        RigaRecord = RigaRecord & AdattaLunghezzaNumero(Registrazione.NumeroProtocollo, 7)
                        RigaRecord = RigaRecord & "06"
                        If CausaleContabile.TipoDocumento = "NC" Then
                            RigaRecord = RigaRecord & "N"
                        Else
                            RigaRecord = RigaRecord & "F"
                        End If
                        RigaRecord = RigaRecord & "0001"

                        Dim TabTrascodificaCC As New Cls_TabellaTrascodificheEsportazioni


                        TabTrascodificaCC.TIPOTAB = "CC"
                        TabTrascodificaCC.SENIOR = "RSAA"
                        TabTrascodificaCC.EXPORT = 0
                        TabTrascodificaCC.Leggi(Session("DC_TABELLE"))
                        RigaRecord = RigaRecord & TabTrascodificaCC.EXPORT


                        RigaRecord = RigaRecord & AdattaLunghezzaNumero(Math.Round(TotImponibile1, 2) * 100, 9)
                        RigaRecord = RigaRecord & AdattaLunghezzaNumero(Math.Round(Imposta, 2) * 100, 9)
                        If TotImponibile2 > 0 Then
                            RigaRecord = RigaRecord & "S"
                        Else
                            RigaRecord = RigaRecord & "N"
                        End If


                        twFatture.WriteLine(RigaRecord)
                    End If
                    If TotImponibile2 > 0 Then
                        RigaRecord = ""
                        RigaRecord = RigaRecord & Registrazione.AnnoProtocollo
                        RigaRecord = RigaRecord & Format(Registrazione.DataRegistrazione, "yyyyMMdd")
                        RigaRecord = RigaRecord & AdattaLunghezzaNumero(Registrazione.NumeroProtocollo, 7)
                        RigaRecord = RigaRecord & "06"
                        If CausaleContabile.TipoDocumento = "NC" Then
                            RigaRecord = RigaRecord & "N"
                        Else
                            RigaRecord = RigaRecord & "F"
                        End If
                        If TotImponibile1 > 0 Then
                            RigaRecord = RigaRecord & "0002"
                        Else
                            RigaRecord = RigaRecord & "0001"
                        End If


                        Dim TabTrascodificaCC As New Cls_TabellaTrascodificheEsportazioni


                        TabTrascodificaCC.TIPOTAB = "CC"
                        TabTrascodificaCC.SENIOR = "RSAB"
                        TabTrascodificaCC.EXPORT = 0
                        TabTrascodificaCC.Leggi(Session("DC_TABELLE"))
                        RigaRecord = RigaRecord & TabTrascodificaCC.EXPORT


                        RigaRecord = RigaRecord & AdattaLunghezzaNumero(Math.Round(TotImponibile2, 2) * 100, 9)
                        RigaRecord = RigaRecord & AdattaLunghezzaNumero(Math.Round(Imposta, 2) * 100, 9)
                        RigaRecord = RigaRecord & "S"

                        twFatture.WriteLine(RigaRecord)
                    End If

                Else

                    RigaRecord = ""
                    RigaRecord = RigaRecord & Registrazione.AnnoProtocollo
                    RigaRecord = RigaRecord & Format(Registrazione.DataRegistrazione, "yyyyMMdd")
                    RigaRecord = RigaRecord & AdattaLunghezzaNumero(Registrazione.NumeroProtocollo, 7)
                    RigaRecord = RigaRecord & "06"
                    If CausaleContabile.TipoDocumento = "NC" Then
                        RigaRecord = RigaRecord & "N"
                    Else
                        RigaRecord = RigaRecord & "F"
                    End If
                    RigaRecord = RigaRecord & "0001"

                    Dim TabTrascodificaCC As New Cls_TabellaTrascodificheEsportazioni


                    TabTrascodificaCC.TIPOTAB = "CC"
                    TabTrascodificaCC.SENIOR = Registrazione.CentroServizio
                    TabTrascodificaCC.EXPORT = 0
                    TabTrascodificaCC.Leggi(Session("DC_TABELLE"))
                    RigaRecord = RigaRecord & TabTrascodificaCC.EXPORT

                    RigaRecord = RigaRecord & AdattaLunghezzaNumero(Math.Round(TotImponibile2, 2) * 100, 9)
                    RigaRecord = RigaRecord & AdattaLunghezzaNumero(Math.Round(Imposta, 2) * 100, 9)
                    RigaRecord = RigaRecord & "N"

                    twFatture.WriteLine(RigaRecord)
                End If

            End If
        Loop
        twIncassi.Close()
        twFatture.Close()


        Btn_Testa2.Text = "Fatture"
        Btn_Riga2.Text = "Incassi"
        Btn_Testa2.Visible = True
        Btn_Riga2.Visible = True

    End Function

    'Sub Export_ATS_Ver0()
    '    Dim cn As OleDbConnection
    '    Dim cnOsp As OleDbConnection

    '    Dim cn As OleDbConnection
    '    Dim MyRs As New ADODB.Recordset
    '    Dim RsIva As New ADODB.Recordset

    '    Dim RsSt As New ADODB.Recordset
    '    Dim UtilVb6 As New Cls_FunzioniVB6
    '    Dim Errori As String = ""
    '    Dim VerificaCodiceFiscale As New Cls_CodiceFiscale

    '    Dim NomeFile As String
    '    Dim NomeFileIN As String

    '    Session("MYDATE") = Now


    '    cn = New Data.OleDb.OleDbConnection(Session("DC_GENERALE"))
    '    cn.Open()


    '    MyTable.Clear()
    '    MyTable.Columns.Clear()

    '    MyTable.Columns.Add("Numero Registrazione", GetType(String))
    '    MyTable.Columns.Add("Cliente", GetType(String))
    '    MyTable.Columns.Add("Segnalazione", GetType(String))

    '    Dim NumeroRegistrazioni As Integer


    '    NumeroRegistrazioni = 0
    '    Export_polifarma = True

    '    Dim cmd As New OleDbCommand()
    '    cmd.CommandText = "Select * From MovimentiContabiliTesta where (select count(*) from MovimentiContabiliRiga Where Numero = MovimentiContabiliTesta.NumeroRegistrazione  And RigaBollato = 1) =  0 And DataRegistrazione >= ? And DataRegistrazione <= ? "
    '    cmd.Parameters.AddWithValue("@DataDal", Txt_DataDal.Text)
    '    cmd.Parameters.AddWithValue("@DataAl", Txt_DataAl.Text)
    '    cmd.Connection = cn
    '    Dim MyRsDB As OleDbDataReader = cmd.ExecuteReader()
    '    Do While MyRsDB.Read

    '        Dim Riga As New Cls_CampiExport
    '        Dim Registrazione As New Cls_MovimentoContabile
    '        Dim RegistrazioneIncasso As New Cls_MovimentoContabile
    '        Dim CausaleContabile As New Cls_CausaleContabile

    '        Dim CausaleContabileDocumentoCollegato As New Cls_CausaleContabile

    '        Registrazione.NumeroRegistrazione = campodbN(MyRsDB.Item("NumeroRegistrazione"))
    '        Registrazione.Leggi(Session("DC_GENERALE"), Registrazione.NumeroRegistrazione)

    '        CausaleContabile.Leggi(Session("DC_TABELLE"), Registrazione.CausaleContabile)

    '        Dim Cliente As String = ""
    '        Dim CodiceOspite As Integer
    '        Dim CodiceParente As Integer = 0
    '        Dim Tipo As String
    '        Dim CodiceProvincia As String = ""
    '        Dim CodiceComune As String = ""
    '        Dim CodiceRegione As String = ""


    '        Tipo = Registrazione.Tipologia

    '        Dim cmdRd As New OleDbCommand()
    '        cmdRd.CommandText = "Select * From MovimentiContabiliRiga Where Numero = ?  And Tipo ='CF'   "
    '        cmdRd.Connection = cn
    '        cmdRd.Parameters.AddWithValue("@Numero", Registrazione.NumeroRegistrazione)
    '        Dim MyReadSC As OleDbDataReader = cmdRd.ExecuteReader()
    '        If MyReadSC.Read Then
    '            Cliente = campodb(MyReadSC.Item("MastroPartita")) & "." & campodb(MyReadSC.Item("ContoPartita")) & "." & campodb(MyReadSC.Item("SottocontoPartita"))
    '            If Mid(Tipo & Space(10), 1, 1) = " " Then
    '                CodiceOspite = Int(campodbN(MyReadSC.Item("SottocontoPartita")) / 100)
    '                CodiceParente = campodbN(MyReadSC.Item("SottocontoPartita")) - (CodiceOspite * 100)

    '                If CodiceParente = 0 And CodiceOspite > 0 Then
    '                    Tipo = "O" & Space(10)
    '                End If
    '                If CodiceParente > 0 And CodiceOspite > 0 Then
    '                    Tipo = "P" & Space(10)
    '                End If
    '            End If
    '            If Mid(Tipo & Space(10), 1, 1) = "C" Then
    '                CodiceProvincia = Mid(Tipo & Space(10), 2, 3)
    '                CodiceComune = Mid(Tipo & Space(10), 5, 3)
    '            End If
    '            If Mid(Tipo & Space(10), 1, 1) = "J" Then
    '                CodiceProvincia = Mid(Tipo & Space(10), 2, 3)
    '                CodiceComune = Mid(Tipo & Space(10), 5, 3)
    '            End If
    '            If Mid(Tipo & Space(10), 1, 1) = "R" Then
    '                CodiceRegione = Mid(Tipo & Space(10), 2, 4)
    '            End If

    '        End If
    '        MyReadSC.Close()

    '        Dim DO11_RAGIONESOCIALE As String = ""
    '        Dim DO11_PIVA As String = ""
    '        Dim DO11_CF As String = ""
    '        Dim DO11_INDIRIZZO As String = ""
    '        Dim DO11_CAP As String = ""
    '        Dim DO11_CITTA_NOME As String = ""
    '        Dim DO11_CITTA_ISTAT As String = ""
    '        Dim DO11_PROVINCIA As String = ""
    '        Dim DO11_CONDPAG_CODICE_SEN As String = ""
    '        Dim DO11_CONDPAG_NOME_SEN As String = ""
    '        Dim DO11_BANCA_CIN As String = ""
    '        Dim DO11_BANCA_ABI As String = ""
    '        Dim DO11_BANCA_CAB As String = ""
    '        Dim DO11_BANCA_CONTO As String = ""
    '        Dim DO11_BANCA_IBAN As String = ""
    '        Dim DO11_BANCA_IDMANDATO As String = ""
    '        Dim DO11_BANCA_DATAMANDATO As String = ""
    '        Dim AddDescrizione As String

    '        AddDescrizione = ""
    '        If Mid(Tipo & Space(10), 1, 1) = "O" Then
    '            Dim Ospite As New ClsOspite

    '            Ospite.CodiceOspite = CodiceOspite
    '            Ospite.Leggi(Session("DC_OSPITE"), Ospite.CodiceOspite)
    '            DO11_RAGIONESOCIALE = Ospite.CognomeOspite & ", " & Ospite.NomeOspite
    '            DO11_PIVA = ""
    '            DO11_CF = Ospite.CODICEFISCALE
    '            DO11_INDIRIZZO = Ospite.RESIDENZAINDIRIZZO1
    '            DO11_CAP = Ospite.RESIDENZACAP1
    '            Dim DcCom As New ClsComune

    '            DcCom.Provincia = Ospite.RESIDENZAPROVINCIA1
    '            DcCom.Comune = Ospite.RESIDENZACOMUNE1
    '            DcCom.Leggi(Session("DC_OSPITE"))

    '            DO11_CITTA_NOME = DcCom.Descrizione
    '            DO11_CITTA_ISTAT = DcCom.Provincia & DcCom.Comune

    '            Dim DcProv As New ClsComune

    '            DcProv.Provincia = Ospite.RESIDENZAPROVINCIA1
    '            DcProv.Comune = ""
    '            DcProv.Leggi(Session("DC_OSPITE"))
    '            DO11_PROVINCIA = DcProv.CodificaProvincia


    '            Dim Kl As New Cls_DatiOspiteParenteCentroServizio

    '            Kl.CodiceOspite = CodiceOspite
    '            Kl.CodiceParente = 0
    '            Kl.CentroServizio = Registrazione.CentroServizio
    '            Kl.Leggi(Session("DC_OSPITE"))
    '            If Kl.ModalitaPagamento <> "" Then
    '                Ospite.MODALITAPAGAMENTO = Kl.ModalitaPagamento
    '            End If


    '            DO11_CONDPAG_CODICE_SEN = Ospite.MODALITAPAGAMENTO
    '            Dim MPAg As New ClsModalitaPagamento

    '            MPAg.Codice = Ospite.MODALITAPAGAMENTO
    '            MPAg.Leggi(Session("DC_OSPITE"))
    '            DO11_CONDPAG_NOME_SEN = MPAg.DESCRIZIONE


    '            Dim ModPag As New Cls_DatiPagamento

    '            ModPag.CodiceOspite = Ospite.CodiceOspite
    '            ModPag.CodiceParente = 0
    '            ModPag.LeggiUltimaDataData(Session("DC_OSPITE"), Registrazione.DataRegistrazione)

    '            DO11_BANCA_CIN = ModPag.Cin
    '            DO11_BANCA_ABI = ModPag.Abi
    '            DO11_BANCA_CAB = ModPag.Cab
    '            DO11_BANCA_CONTO = ModPag.CCBancario
    '            DO11_BANCA_IBAN = ModPag.CodiceInt & Format(Val(ModPag.NumeroControllo), "00") & ModPag.Cin & Format(Val(ModPag.Abi), "00000") & Format(Val(ModPag.Cab), "00000") & AdattaLunghezzaNumero(ModPag.CCBancario, 12)

    '            DO11_BANCA_IDMANDATO = ModPag.IdMandatoDebitore
    '            DO11_BANCA_DATAMANDATO = ModPag.DataMandatoDebitore
    '        End If

    '        If Mid(Tipo & Space(10), 1, 1) = "P" Then
    '            Dim Ospite As New Cls_Parenti

    '            Ospite.CodiceOspite = CodiceOspite
    '            Ospite.CodiceParente = CodiceParente
    '            Ospite.Leggi(Session("DC_OSPITE"), Ospite.CodiceOspite, Ospite.CodiceParente)
    '            DO11_RAGIONESOCIALE = Ospite.CognomeParente & ", " & Ospite.NomeParente
    '            DO11_PIVA = ""
    '            DO11_CF = Ospite.CODICEFISCALE
    '            DO11_INDIRIZZO = Ospite.RESIDENZAINDIRIZZO1
    '            DO11_CAP = Ospite.RESIDENZACAP1
    '            Dim DcCom As New ClsComune

    '            DcCom.Provincia = Ospite.RESIDENZAPROVINCIA1
    '            DcCom.Comune = Ospite.RESIDENZACOMUNE1
    '            DcCom.Leggi(Session("DC_OSPITE"))

    '            DO11_CITTA_NOME = DcCom.Descrizione
    '            DO11_CITTA_ISTAT = DcCom.Provincia & DcCom.Comune

    '            Dim DcProv As New ClsComune

    '            DcProv.Provincia = Ospite.RESIDENZAPROVINCIA1
    '            DcProv.Comune = ""
    '            DcProv.Leggi(Session("DC_OSPITE"))
    '            DO11_PROVINCIA = DcProv.CodificaProvincia
    '            DO11_CONDPAG_CODICE_SEN = Ospite.MODALITAPAGAMENTO

    '            Dim Kl As New Cls_DatiOspiteParenteCentroServizio

    '            Kl.CodiceOspite = CodiceOspite
    '            Kl.CodiceParente = CodiceParente
    '            Kl.CentroServizio = Registrazione.CentroServizio
    '            Kl.Leggi(Session("DC_OSPITE"))
    '            If Kl.ModalitaPagamento <> "" Then
    '                Ospite.MODALITAPAGAMENTO = Kl.ModalitaPagamento
    '            End If

    '            Dim MPAg As New ClsModalitaPagamento

    '            MPAg.Codice = Ospite.MODALITAPAGAMENTO
    '            MPAg.Leggi(Session("DC_OSPITE"))
    '            DO11_CONDPAG_NOME_SEN = MPAg.DESCRIZIONE

    '            Dim ModPag As New Cls_DatiPagamento

    '            ModPag.CodiceOspite = Ospite.CodiceOspite
    '            ModPag.CodiceParente = Ospite.CodiceParente
    '            ModPag.LeggiUltimaDataData(Session("DC_OSPITE"), Registrazione.DataRegistrazione)

    '            DO11_BANCA_CIN = ModPag.Cin
    '            DO11_BANCA_ABI = ModPag.Abi
    '            DO11_BANCA_CAB = ModPag.Cab
    '            DO11_BANCA_CONTO = ModPag.CCBancario
    '            DO11_BANCA_IBAN = ModPag.CodiceInt & Format(Val(ModPag.NumeroControllo), "00") & ModPag.Cin & Format(Val(ModPag.Abi), "00000") & Format(Val(ModPag.Cab), "00000") & AdattaLunghezzaNumero(ModPag.CCBancario, 12)

    '            DO11_BANCA_IDMANDATO = ModPag.IdMandatoDebitore
    '            DO11_BANCA_DATAMANDATO = ModPag.DataMandatoDebitore
    '        End If
    '        If Mid(Tipo & Space(10), 1, 1) = "C" Then
    '            Dim Ospite As New ClsComune

    '            Ospite.Provincia = CodiceProvincia
    '            Ospite.Comune = CodiceComune
    '            Ospite.Leggi(Session("DC_OSPITE"))
    '            DO11_RAGIONESOCIALE = Ospite.Descrizione
    '            DO11_PIVA = Ospite.PartitaIVA
    '            DO11_CF = Ospite.CodiceFiscale
    '            DO11_INDIRIZZO = Ospite.RESIDENZAINDIRIZZO1
    '            DO11_CAP = Ospite.RESIDENZACAP1
    '            Dim DcCom As New ClsComune

    '            DcCom.Provincia = Ospite.RESIDENZAPROVINCIA1
    '            DcCom.Comune = Ospite.RESIDENZACOMUNE1
    '            DcCom.Leggi(Session("DC_OSPITE"))

    '            DO11_CITTA_NOME = DcCom.Descrizione
    '            DO11_CITTA_ISTAT = DcCom.Provincia & DcCom.Comune

    '            Dim DcProv As New ClsComune

    '            DcProv.Provincia = Ospite.RESIDENZAPROVINCIA1
    '            DcProv.Comune = ""
    '            DcProv.Leggi(Session("DC_OSPITE"))
    '            DO11_PROVINCIA = DcProv.CodificaProvincia
    '            DO11_CONDPAG_CODICE_SEN = Ospite.ModalitaPagamento
    '            Dim MPAg As New ClsModalitaPagamento

    '            MPAg.Codice = Ospite.ModalitaPagamento
    '            MPAg.Leggi(Session("DC_OSPITE"))
    '            DO11_CONDPAG_NOME_SEN = MPAg.DESCRIZIONE

    '            Dim cnOspiti As OleDbConnection

    '            cnOspiti = New Data.OleDb.OleDbConnection(Session("DC_OSPITE"))

    '            cnOspiti.Open()

    '            Dim cmdC As New OleDbCommand()
    '            cmdC.CommandText = ("select * from NoteFatture where CodiceProvincia = ? And CodiceComune = ?")
    '            cmdC.Parameters.AddWithValue("@CodiceProvincia", CodiceProvincia)
    '            cmdC.Parameters.AddWithValue("@CodiceComune", CodiceComune)
    '            cmdC.Connection = cnOspiti
    '            Dim RdCom As OleDbDataReader = cmdC.ExecuteReader()
    '            If RdCom.Read Then
    '                AddDescrizione = campodb(RdCom.Item("NoteUp"))
    '            End If
    '            RdCom.Close()
    '            cnOspiti.Close()
    '        End If
    '        If Mid(Tipo & Space(10), 1, 1) = "J" Then
    '            Dim Ospite As New ClsComune

    '            Ospite.Provincia = CodiceProvincia
    '            Ospite.Comune = CodiceComune
    '            Ospite.Leggi(Session("DC_OSPITE"))
    '            DO11_RAGIONESOCIALE = Ospite.Descrizione
    '            DO11_PIVA = Ospite.PartitaIVA
    '            DO11_CF = Ospite.CodiceFiscale
    '            DO11_INDIRIZZO = Ospite.RESIDENZAINDIRIZZO1
    '            DO11_CAP = Ospite.RESIDENZACAP1
    '            Dim DcCom As New ClsComune

    '            DcCom.Provincia = Ospite.RESIDENZAPROVINCIA1
    '            DcCom.Comune = Ospite.RESIDENZACOMUNE1
    '            DcCom.Leggi(Session("DC_OSPITE"))

    '            DO11_CITTA_NOME = DcCom.Descrizione
    '            DO11_CITTA_ISTAT = DcCom.Provincia & DcCom.Comune

    '            Dim DcProv As New ClsComune

    '            DcProv.Provincia = Ospite.RESIDENZAPROVINCIA1
    '            DcProv.Comune = ""
    '            DcProv.Leggi(Session("DC_OSPITE"))
    '            DO11_PROVINCIA = DcProv.CodificaProvincia
    '            DO11_CONDPAG_CODICE_SEN = Ospite.ModalitaPagamento
    '            Dim MPAg As New ClsModalitaPagamento

    '            MPAg.Codice = Ospite.ModalitaPagamento
    '            MPAg.Leggi(Session("DC_OSPITE"))
    '            DO11_CONDPAG_NOME_SEN = MPAg.DESCRIZIONE

    '            Dim cnOspiti As OleDbConnection

    '            cnOspiti = New Data.OleDb.OleDbConnection(Session("DC_OSPITE"))

    '            cnOspiti.Open()

    '            Dim cmdC As New OleDbCommand()
    '            cmdC.CommandText = ("select * from NoteFatture where CodiceProvincia = ? And CodiceComune = ?")
    '            cmdC.Parameters.AddWithValue("@CodiceProvincia", CodiceProvincia)
    '            cmdC.Parameters.AddWithValue("@CodiceComune", CodiceComune)
    '            cmdC.Connection = cnOspiti
    '            Dim RdCom As OleDbDataReader = cmdC.ExecuteReader()
    '            If RdCom.Read Then
    '                AddDescrizione = campodb(RdCom.Item("NoteUp"))
    '            End If
    '            RdCom.Close()
    '            cnOspiti.Close()
    '        End If
    '        If Mid(Tipo & Space(10), 1, 1) = "R" Then
    '            Dim Ospite As New ClsUSL

    '            Ospite.CodiceRegione = CodiceRegione
    '            Ospite.Leggi(Session("DC_OSPITE"))
    '            DO11_RAGIONESOCIALE = Ospite.Nome
    '            DO11_PIVA = Ospite.PARTITAIVA
    '            DO11_CF = Ospite.CodiceFiscale
    '            DO11_INDIRIZZO = Ospite.RESIDENZAINDIRIZZO1
    '            DO11_CAP = Ospite.RESIDENZACAP1
    '            Dim DcCom As New ClsComune

    '            DcCom.Provincia = Ospite.RESIDENZAPROVINCIA1
    '            DcCom.Comune = Ospite.RESIDENZACOMUNE1
    '            DcCom.Leggi(Session("DC_OSPITE"))

    '            DO11_CITTA_NOME = DcCom.Descrizione
    '            DO11_CITTA_ISTAT = DcCom.Provincia & DcCom.Comune

    '            Dim DcProv As New ClsComune

    '            DcProv.Provincia = Ospite.RESIDENZAPROVINCIA1
    '            DcProv.Comune = ""
    '            DcProv.Leggi(Session("DC_OSPITE"))
    '            DO11_PROVINCIA = DcProv.CodificaProvincia
    '            DO11_CONDPAG_CODICE_SEN = Ospite.ModalitaPagamento
    '            Dim MPAg As New ClsModalitaPagamento

    '            MPAg.Codice = Ospite.ModalitaPagamento
    '            MPAg.Leggi(Session("DC_OSPITE"))
    '            DO11_CONDPAG_NOME_SEN = MPAg.DESCRIZIONE
    '        End If


    '        '<DOCVENTE>
    '        '<TIPDOC>FE</TIPDOC>
    '        '<RAGBOL>A</RAGBOL>
    '        '<ANNDOC>2018</ANNDOC>
    '        '<NUMDOC>85</NUMDOC>
    '        '<DATDOC>31/03/2018</DATDOC>
    '        '<CLFR>C</CLFR>
    '        '<CODCLI>AUSLRE</CODCLI>
    '        '<CAMBIO>0</CAMBIO>
    '        '<RIFCAMBIO>E</RIFCAMBIO>
    '        '<SCOFIN1>0</SCOFIN1>
    '        '<SCOFIN2>0</SCOFIN2>
    '        '<SCONTO>0</SCONTO>
    '        '<PDCSCO>6105</PDCSCO>
    '        '<CODMAG>02</CODMAG>
    '        '<CAUMAG>201</CAUMAG>
    '        '<CODPAG>BS11</CODPAG>
    '        '<PESO>0</PESO>
    '        '<COLLI>0</COLLI>
    '        '<CODCLI1>AUSLRE</CODCLI1>
    '        '<CODSPECL>06</CODSPECL>
    '        '<RAGSOC>A.U.S.L. di Reggio Emilia</RAGSOC>
    '        '<RAGSOC1>Ser.T di Reggio Emilia</RAGSOC1>
    '        '<INDIR>Via Amendola, 2</INDIR>
    '        '<CAP>42100</CAP>
    '        '<LOCAL>Reggio Emilia</LOCAL>
    '        '<PROV>RE</PROV>
    '        '<NUMBOL>0</NUMBOL>
    '        '<LISVEN>0</LISVEN>
    '        '<TOTIMP>16150,5</TOTIMP>
    '        '<TOTIVA>807,53</TOTIVA>
    '        '<TOT001>0</TOT001>
    '        '<TOT002>0</TOT002>
    '        '<TOT003>0</TOT003>
    '        '<IMPSCA01>0</IMPSCA01>
    '        '<IMPSCA02>0</IMPSCA02>
    '        '<IMPSCA03>0</IMPSCA03>
    '        '<IMPSCA04>0</IMPSCA04>
    '        '<IMPSCA05>0</IMPSCA05>
    '        '<IMPSCA06>0</IMPSCA06>
    '        '<IMPSCA07>0</IMPSCA07>
    '        '<IMPSCA08>0</IMPSCA08>
    '        '<IMPSCA09>0</IMPSCA09>
    '        '<ANNMAG>2018</ANNMAG>
    '        '<NUMMAG>159</NUMMAG>
    '        '<NUMMAG1>0</NUMMAG1>
    '        '<STATO>1</STATO>
    '        '<IVASOS>Y</IVASOS>
    '        '<TOTIMPCM>16150,5</TOTIMPCM>
    '        '<TOTIVACM>807,53</TOTIVACM>
    '        '<TOT002CM>0</TOT002CM>
    '        '<TOT003CM>0</TOT003CM>
    '        '<CENTRO>001</CENTRO>
    '        '<ANALIT>S</ANALIT>
    '        '<CODBAN>02</CODBAN>
    '        '<RTIMPNON>0</RTIMPNON>
    '        '<RTIMPNON2>0</RTIMPNON2>
    '        '<RTIMPNET>0</RTIMPNET>
    '        '<RTPERRIT>0</RTPERRIT>
    '        '<RTIMPRIT>0</RTIMPRIT>
    '        '<RTIMPESC>0</RTIMPESC>
    '        '<TRIMRET>0</TRIMRET>
    '        '<MESERET>0</MESERET>
    '        '<XDX01>3</XDX01>
    '        '<XDX02>43194,4759046</XDX02>
    '        '<STATOFE>1</STATOFE>
    '        '<XDX03>43194,5328927</XDX03>
    '        '<BOLVIRT>0</BOLVIRT>
    '        '<ADDINFO>&#60;FATEL&#62;&#60;MODRIF&#62;P&#60;/MODRIF&#62;&#60;MODDDT&#62;P&#60;/MODDDT&#62;&#60;MODFTCOL&#62;P&#60;/MODFTCOL&#62;&#60;MODPAG&#62;P&#60;/MODPAG&#62;&#60;MODCASPRE&#62;A&#60;/MODCASPRE&#62;&#60;MODTRASP&#62;A&#60;/MODTRASP&#62;&#60;MODESIG&#62;A&#60;/MODESIG&#62;&#60;RIFAMMMODE&#62;S&#60;/RIFAMMMODE&#62;&#60;ISCODIMPAMAN&#62;S&#60;/ISCODIMPAMAN&#62;&#60;DETTAGLIO&#62;&#60;RIGADETTAGLIO&#62;&#60;RIGA&#62;10&#60;/RIGA&#62;&#60;DGAUTO&#62;S&#60;/DGAUTO&#62;&#60;RFAUTO&#62;S&#60;/RFAUTO&#62;&#60;/RIGADETTAGLIO&#62;&#60;RIGADETTAGLIO&#62;&#60;RIGA&#62;20&#60;/RIGA&#62;&#60;DGAUTO&#62;S&#60;/DGAUTO&#62;&#60;RFAUTO&#62;S&#60;/RFAUTO&#62;&#60;/RIGADETTAGLIO&#62;&#60;RIGADETTAGLIO&#62;&#60;RIGA&#62;30&#60;/RIGA&#62;&#60;DGAUTO&#62;S&#60;/DGAUTO&#62;&#60;RFAUTO&#62;S&#60;/RFAUTO&#62;&#60;/RIGADETTAGLIO&#62;&#60;RIGADETTAGLIO&#62;&#60;RIGA&#62;40&#60;/RIGA&#62;&#60;DGAUTO&#62;S&#60;/DGAUTO&#62;&#60;RFAUTO&#62;S&#60;/RFAUTO&#62;&#60;/RIGADETTAGLIO&#62;&#60;RIGADETTAGLIO&#62;&#60;RIGA&#62;50&#60;/RIGA&#62;&#60;DGAUTO&#62;S&#60;/DGAUTO&#62;&#60;RFAUTO&#62;S&#60;/RFAUTO&#62;&#60;/RIGADETTAGLIO&#62;&#60;RIGADETTAGLIO&#62;&#60;RIGA&#62;60&#60;/RIGA&#62;&#60;DGAUTO&#62;S&#60;/DGAUTO&#62;&#60;RFAUTO&#62;S&#60;/RFAUTO&#62;&#60;/RIGADETTAGLIO&#62;&#60;RIGADETTAGLIO&#62;&#60;RIGA&#62;70&#60;/RIGA&#62;&#60;DGAUTO&#62;S&#60;/DGAUTO&#62;&#60;RFAUTO&#62;S&#60;/RFAUTO&#62;&#60;/RIGADETTAGLIO&#62;&#60;RIGADETTAGLIO&#62;&#60;RIGA&#62;80&#60;/RIGA&#62;&#60;DGAUTO&#62;S&#60;/DGAUTO&#62;&#60;RFAUTO&#62;S&#60;/RFAUTO&#62;&#60;/RIGADETTAGLIO&#62;&#60;RIGADETTAGLIO&#62;&#60;RIGA&#62;90&#60;/RIGA&#62;&#60;DGAUTO&#62;S&#60;/DGAUTO&#62;&#60;RFAUTO&#62;S&#60;/RFAUTO&#62;&#60;/RIGADETTAGLIO&#62;&#60;RIGADETTAGLIO&#62;&#60;RIGA&#62;100&#60;/RIGA&#62;&#60;DGAUTO&#62;S&#60;/DGAUTO&#62;&#60;RFAUTO&#62;S&#60;/RFAUTO&#62;&#60;/RIGADETTAGLIO&#62;&#60;RIGADETTAGLIO&#62;&#60;RIGA&#62;110&#60;/RIGA&#62;&#60;DGAUTO&#62;S&#60;/DGAUTO&#62;&#60;RFAUTO&#62;S&#60;/RFAUTO&#62;&#60;/RIGADETTAGLIO&#62;&#60;RIGADETTAGLIO&#62;&#60;RIGA&#62;120&#60;/RIGA&#62;&#60;DGAUTO&#62;S&#60;/DGAUTO&#62;&#60;RFAUTO&#62;S&#60;/RFAUTO&#62;&#60;/RIGADETTAGLIO&#62;&#60;/DETTAGLIO&#62;&#60;/FATEL&#62;</ADDINFO>
    '        '<NUMDIN>0</NUMDIN>
    '        '<DOCVENCO>
    '        '<TIPDOC>FE</TIPDOC>
    '        '<ANNDOC>2018</ANNDOC>
    '        '<NUMDOC>85</NUMDOC>
    '        '<NUMRIG>10</NUMRIG>
    '        '<LISVEN>0</LISVEN>
    '        '<CODMAG>02</CODMAG>
    '        '<CODART>MANFREDINIMARIL</CODART>
    '        '<DESART>M. M. dal 01/03/18 al 31/03/18</DESART>
    '        '<CODMER>02</CODMER>
    '        '<UNMIS>GG</UNMIS>
    '        '<QUANTI>31</QUANTI>
    '        '<QTA1>0</QTA1>
    '        '<QTA2>0</QTA2>
    '        '<QTA3>0</QTA3>
    '        '<NUMPEZ>0</NUMPEZ>
    '        '<CAMBIO>0</CAMBIO>
    '        '<RIFCAMBIO>E</RIFCAMBIO>
    '        '<PREZZO>54,75</PREZZO>
    '        '<SCONTO>0</SCONTO>
    '        '<SCONTO2>0</SCONTO2>
    '        '<SCONTO3>0</SCONTO3>
    '        '<PROVV>0</PROVV>
    '        '<CODIVA>05SP</CODIVA>
    '        '<CONTROP>7503</CONTROP>
    '        '<NUMORD>0</NUMORD>
    '        '<RIGORD>0</RIGORD>
    '        '<RIGMAG>0</RIGMAG>
    '        '<ALIESE>1</ALIESE>
    '        '<COLLI>0</COLLI>
    '        '<LIBER01>0</LIBER01>
    '        '<LIBER02>0</LIBER02>
    '        '<TIPRIG>0</TIPRIG>
    '        '<CENTRO>001</CENTRO>
    '        '<VOCE>905</VOCE>
    '        '<POSRIG>10</POSRIG>
    '        '<ESPLDIBA>P</ESPLDIBA>
    '        '<QTACAN>0</QTACAN>
    '        '<DIPRIG>0</DIPRIG>
    '        '<QTAUMSUP>0</QTAUMSUP>
    '        '<MASSANET>0</MASSANET>
    '        '<VALSTAT>0</VALSTAT>
    '        '<SEZDOGRET>0</SEZDOGRET>
    '        '<PROTDICH>0</PROTDICH>
    '        '<PROGRETT>0</PROGRETT>
    '        '<IMPRET>0</IMPRET>
    '        '<XDX01>3</XDX01>
    '        '<XDX02>43194,4769794</XDX02>
    '        '<LIBDAT1>01/03/2018</LIBDAT1>
    '        '<LIBDAT2>31/03/2018</LIBDAT2>
    '        '<XDX03>43194,4769794</XDX03>
    '        '<PROVAGC>0</PROVAGC>
    '        '<IMPPROAG>0</IMPPROAG>
    '        '<IMPPROAGC>0</IMPPROAGC>
    '        '<IMPONPRAG>0</IMPONPRAG>
    '        '<IMPONPRAC>0</IMPONPRAC>
    '        '<PROVIMPAG>0</PROVIMPAG>
    '        '<PROVIMPAC>0</PROVIMPAC>
    '        '</DOCVENCO>


    'End Sub

    Protected Sub ImgUpDate_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles ImgUpDate.Click
        Txt_DalDocumento.Text = MinimoDocumento()
        Txt_AlDocumento.Text = MassimoDocumento()
    End Sub



    Function Export_Zucchetti() As Boolean
        Dim cn As OleDbConnection
        Dim MyRs As New ADODB.Recordset
        Dim RsIva As New ADODB.Recordset

        Dim RsSt As New ADODB.Recordset
        Dim UtilVb6 As New Cls_FunzioniVB6
        Dim Errori As String = ""
        Dim VerificaCodiceFiscale As New Cls_CodiceFiscale

        Dim NomeFile As String
        Dim NomeFileIN As String

        Session("MYDATE") = Now

        NomeFile = HostingEnvironment.ApplicationPhysicalPath() & "\Public\Zucc_FT_" & Format(Session("MYDATE"), "yyyyMMddHHmmss") & ".Txt"


        Dim twFatture As System.IO.TextWriter = System.IO.File.CreateText(NomeFile)

        cn = New Data.OleDb.OleDbConnection(Session("DC_GENERALE"))
        cn.Open()

        twFatture.WriteLine("<?xml version=""1.0""?>")
        twFatture.WriteLine("<ImportExportG1>")

        MyTable.Clear()
        MyTable.Columns.Clear()

        MyTable.Columns.Add("Numero Registrazione", GetType(String))
        MyTable.Columns.Add("Cliente", GetType(String))
        MyTable.Columns.Add("Segnalazione", GetType(String))

        Dim NumeroRegistrazioni As Integer


        NumeroRegistrazioni = 0

        Dim cmd As New OleDbCommand()
        cmd.CommandText = "Select * From MovimentiContabiliTesta where (select count(*) from MovimentiContabiliRiga Where Numero = MovimentiContabiliTesta.NumeroRegistrazione  And RigaBollato = 1) =  0 And DataRegistrazione >= ? And DataRegistrazione <= ? And RegistroIVA = " & Val(Dd_RegistroDATA.SelectedValue)
        cmd.Parameters.AddWithValue("@DataDal", Txt_DataDal.Text)
        cmd.Parameters.AddWithValue("@DataAl", Txt_DataAl.Text)
        cmd.Connection = cn
        Dim MyRsDB As OleDbDataReader = cmd.ExecuteReader()
        Do While MyRsDB.Read

            Dim Riga As New Cls_CampiExport
            Dim Registrazione As New Cls_MovimentoContabile
            Dim RegistrazioneIncasso As New Cls_MovimentoContabile
            Dim CausaleContabile As New Cls_CausaleContabile

            Dim CausaleContabileDocumentoCollegato As New Cls_CausaleContabile

            Registrazione.NumeroRegistrazione = campodbN(MyRsDB.Item("NumeroRegistrazione"))
            Registrazione.Leggi(Session("DC_GENERALE"), Registrazione.NumeroRegistrazione)

            CausaleContabile.Leggi(Session("DC_TABELLE"), Registrazione.CausaleContabile)

            Dim Cliente As String = ""
            Dim CodiceOspite As Integer
            Dim CodiceParente As Integer = 0
            Dim Tipo As String
            Dim CodiceProvincia As String = ""
            Dim CodiceComune As String = ""
            Dim CodiceRegione As String = ""


            Tipo = Registrazione.Tipologia

            Dim cmdRd As New OleDbCommand()
            cmdRd.CommandText = "Select * From MovimentiContabiliRiga Where Numero = ?  And RigaDaCausale  = 1   "
            cmdRd.Connection = cn
            cmdRd.Parameters.AddWithValue("@Numero", Registrazione.NumeroRegistrazione)
            Dim MyReadSC As OleDbDataReader = cmdRd.ExecuteReader()
            If MyReadSC.Read Then
                Cliente = campodb(MyReadSC.Item("MastroPartita")) & "." & campodb(MyReadSC.Item("ContoPartita")) & "." & campodb(MyReadSC.Item("SottocontoPartita"))


                If CausaleContabile.Tipo = "P" Then
                    Dim PianoConti As New Cls_Pianodeiconti

                    PianoConti.Mastro = campodb(MyReadSC.Item("MastroPartita"))
                    PianoConti.Conto = campodb(MyReadSC.Item("ContoPartita"))
                    PianoConti.Sottoconto = campodb(MyReadSC.Item("SottocontoPartita"))
                    PianoConti.Decodfica(Session("DC_GENERALE"))

                    Tipo = GetAnagrafica(PianoConti.Mastro, PianoConti.Conto, PianoConti.Sottoconto)

                End If


                If Mid(Tipo & Space(10), 1, 1) = " " Then
                    CodiceOspite = Int(campodbN(MyReadSC.Item("SottocontoPartita")) / 100)
                    CodiceParente = campodbN(MyReadSC.Item("SottocontoPartita")) - (CodiceOspite * 100)

                    If CodiceParente = 0 And CodiceOspite > 0 Then
                        Tipo = "O" & Space(10)
                    End If
                    If CodiceParente > 0 And CodiceOspite > 0 Then
                        Tipo = "P" & Space(10)
                    End If
                End If



                If Mid(Tipo & Space(10), 1, 1) = "C" Then
                    CodiceProvincia = Mid(Tipo & Space(10), 2, 3)
                    CodiceComune = Mid(Tipo & Space(10), 5, 3)
                End If
                If Mid(Tipo & Space(10), 1, 1) = "J" Then
                    CodiceProvincia = Mid(Tipo & Space(10), 2, 3)
                    CodiceComune = Mid(Tipo & Space(10), 5, 3)
                End If
                If Mid(Tipo & Space(10), 1, 1) = "R" Then
                    CodiceRegione = Mid(Tipo & Space(10), 2, 4)
                End If

            End If
            MyReadSC.Close()

            Dim DO11_RAGIONESOCIALE As String = ""
            Dim DO11_PIVA As String = ""
            Dim DO11_CF As String = ""
            Dim DO11_INDIRIZZO As String = ""
            Dim DO11_CAP As String = ""
            Dim DO11_CITTA_NOME As String = ""
            Dim DO11_CITTA_ISTAT As String = ""
            Dim DO11_PROVINCIA As String = ""
            Dim DO11_CONDPAG_CODICE_SEN As String = ""
            Dim DO11_CONDPAG_NOME_SEN As String = ""
            Dim DO11_BANCA_CIN As String = ""
            Dim DO11_BANCA_ABI As String = ""
            Dim DO11_BANCA_CAB As String = ""
            Dim DO11_BANCA_CONTO As String = ""
            Dim DO11_BANCA_IBAN As String = ""
            Dim DO11_BANCA_IDMANDATO As String = ""
            Dim DO11_BANCA_DATAMANDATO As String = ""
            Dim DO11_EXPORT As String = ""
            Dim DO11_ORDINE As String = ""
            Dim DO11_CODART As String = ""
            Dim AddDescrizione As String
            Dim TabTrascodifica As New Cls_TabellaTrascodificheEsportazioni

            AddDescrizione = ""
            If Mid(Tipo & Space(10), 1, 1) = "O" Then
                Dim Ospite As New ClsOspite

                Ospite.CodiceOspite = CodiceOspite
                Ospite.Leggi(Session("DC_OSPITE"), Ospite.CodiceOspite)
                DO11_RAGIONESOCIALE = Ospite.CognomeOspite & " " & Ospite.NomeOspite
                DO11_PIVA = ""
                DO11_CF = Ospite.CODICEFISCALE
                DO11_INDIRIZZO = Ospite.RESIDENZAINDIRIZZO1
                DO11_CAP = Ospite.RESIDENZACAP1

                DO11_CODART = Ospite.CodiceCup

                DO11_EXPORT = Ospite.CONTOPERESATTO

                Dim DcCom As New ClsComune

                DcCom.Provincia = Ospite.RESIDENZAPROVINCIA1
                DcCom.Comune = Ospite.RESIDENZACOMUNE1
                DcCom.Leggi(Session("DC_OSPITE"))

                DO11_CITTA_NOME = DcCom.Descrizione
                DO11_CITTA_ISTAT = DcCom.Provincia & DcCom.Comune

                Dim DcProv As New ClsComune

                DcProv.Provincia = Ospite.RESIDENZAPROVINCIA1
                DcProv.Comune = ""
                DcProv.Leggi(Session("DC_OSPITE"))
                DO11_PROVINCIA = DcProv.CodificaProvincia


                Dim Kl As New Cls_DatiOspiteParenteCentroServizio

                Kl.CodiceOspite = CodiceOspite
                Kl.CodiceParente = 0
                Kl.CentroServizio = Registrazione.CentroServizio
                Kl.Leggi(Session("DC_OSPITE"))
                If Kl.ModalitaPagamento <> "" Then
                    Ospite.MODALITAPAGAMENTO = Kl.ModalitaPagamento
                End If


                DO11_CONDPAG_CODICE_SEN = Ospite.MODALITAPAGAMENTO
                Dim MPAg As New ClsModalitaPagamento

                MPAg.Codice = Ospite.MODALITAPAGAMENTO
                MPAg.Leggi(Session("DC_OSPITE"))
                DO11_CONDPAG_NOME_SEN = MPAg.DESCRIZIONE


                Dim ModPag As New Cls_DatiPagamento

                ModPag.CodiceOspite = Ospite.CodiceOspite
                ModPag.CodiceParente = 0
                ModPag.LeggiUltimaDataData(Session("DC_OSPITE"), Registrazione.DataRegistrazione)

                DO11_BANCA_CIN = ModPag.Cin
                DO11_BANCA_ABI = ModPag.Abi
                DO11_BANCA_CAB = ModPag.Cab
                DO11_BANCA_CONTO = ModPag.CCBancario
                DO11_BANCA_IBAN = ModPag.CodiceInt & Format(Val(ModPag.NumeroControllo), "00") & ModPag.Cin & Format(Val(ModPag.Abi), "00000") & Format(Val(ModPag.Cab), "00000") & AdattaLunghezzaNumero(ModPag.CCBancario, 12)

                DO11_BANCA_IDMANDATO = ModPag.IdMandatoDebitore
                DO11_BANCA_DATAMANDATO = ModPag.DataMandatoDebitore
            End If

            If Mid(Tipo & Space(10), 1, 1) = "P" Then
                Dim Ospite As New Cls_Parenti

                Ospite.CodiceOspite = CodiceOspite
                Ospite.CodiceParente = CodiceParente
                Ospite.Leggi(Session("DC_OSPITE"), Ospite.CodiceOspite, Ospite.CodiceParente)
                DO11_RAGIONESOCIALE = Ospite.CognomeParente & " " & Ospite.NomeParente
                DO11_PIVA = ""
                DO11_CF = Ospite.CODICEFISCALE
                DO11_INDIRIZZO = Ospite.RESIDENZAINDIRIZZO1
                DO11_CAP = Ospite.RESIDENZACAP1

                DO11_EXPORT = Ospite.CONTOPERESATTO

                DO11_CODART = Ospite.CodiceCup

                Dim DcCom As New ClsComune

                DcCom.Provincia = Ospite.RESIDENZAPROVINCIA1
                DcCom.Comune = Ospite.RESIDENZACOMUNE1
                DcCom.Leggi(Session("DC_OSPITE"))

                DO11_CITTA_NOME = DcCom.Descrizione
                DO11_CITTA_ISTAT = DcCom.Provincia & DcCom.Comune

                Dim DcProv As New ClsComune

                DcProv.Provincia = Ospite.RESIDENZAPROVINCIA1
                DcProv.Comune = ""
                DcProv.Leggi(Session("DC_OSPITE"))
                DO11_PROVINCIA = DcProv.CodificaProvincia
                DO11_CONDPAG_CODICE_SEN = Ospite.MODALITAPAGAMENTO

                Dim Kl As New Cls_DatiOspiteParenteCentroServizio

                Kl.CodiceOspite = CodiceOspite
                Kl.CodiceParente = CodiceParente
                Kl.CentroServizio = Registrazione.CentroServizio
                Kl.Leggi(Session("DC_OSPITE"))
                If Kl.ModalitaPagamento <> "" Then
                    Ospite.MODALITAPAGAMENTO = Kl.ModalitaPagamento
                End If

                Dim MPAg As New ClsModalitaPagamento

                MPAg.Codice = Ospite.MODALITAPAGAMENTO
                MPAg.Leggi(Session("DC_OSPITE"))
                DO11_CONDPAG_NOME_SEN = MPAg.DESCRIZIONE

                Dim ModPag As New Cls_DatiPagamento

                ModPag.CodiceOspite = Ospite.CodiceOspite
                ModPag.CodiceParente = Ospite.CodiceParente
                ModPag.LeggiUltimaDataData(Session("DC_OSPITE"), Registrazione.DataRegistrazione)

                DO11_BANCA_CIN = ModPag.Cin
                DO11_BANCA_ABI = ModPag.Abi
                DO11_BANCA_CAB = ModPag.Cab
                DO11_BANCA_CONTO = ModPag.CCBancario
                DO11_BANCA_IBAN = ModPag.CodiceInt & Format(Val(ModPag.NumeroControllo), "00") & ModPag.Cin & Format(Val(ModPag.Abi), "00000") & Format(Val(ModPag.Cab), "00000") & AdattaLunghezzaNumero(ModPag.CCBancario, 12)

                DO11_BANCA_IDMANDATO = ModPag.IdMandatoDebitore
                DO11_BANCA_DATAMANDATO = ModPag.DataMandatoDebitore
            End If
            If Mid(Tipo & Space(10), 1, 1) = "C" Then
                Dim Ospite As New ClsComune

                Ospite.Provincia = CodiceProvincia
                Ospite.Comune = CodiceComune
                Ospite.Leggi(Session("DC_OSPITE"))
                DO11_RAGIONESOCIALE = Ospite.Descrizione
                DO11_PIVA = campodbN(Ospite.PartitaIVA)
                DO11_CF = Ospite.CodiceFiscale
                DO11_INDIRIZZO = Ospite.RESIDENZAINDIRIZZO1
                DO11_CAP = Ospite.RESIDENZACAP1
                Dim DcCom As New ClsComune

                DcCom.Provincia = Ospite.RESIDENZAPROVINCIA1
                DcCom.Comune = Ospite.RESIDENZACOMUNE1
                DcCom.Leggi(Session("DC_OSPITE"))

                DO11_CITTA_NOME = DcCom.Descrizione
                DO11_CITTA_ISTAT = DcCom.Provincia & DcCom.Comune

                Dim DcProv As New ClsComune

                DcProv.Provincia = Ospite.RESIDENZAPROVINCIA1
                DcProv.Comune = ""
                DcProv.Leggi(Session("DC_OSPITE"))
                DO11_PROVINCIA = DcProv.CodificaProvincia
                DO11_CONDPAG_CODICE_SEN = Ospite.ModalitaPagamento
                Dim MPAg As New ClsModalitaPagamento

                MPAg.Codice = Ospite.ModalitaPagamento
                MPAg.Leggi(Session("DC_OSPITE"))
                DO11_CONDPAG_NOME_SEN = MPAg.DESCRIZIONE

                DO11_ORDINE = Ospite.CodiceCig

                DO11_EXPORT = Ospite.CONTOPERESATTO

                Dim cnOspiti As OleDbConnection

                cnOspiti = New Data.OleDb.OleDbConnection(Session("DC_OSPITE"))

                cnOspiti.Open()

                Dim cmdC As New OleDbCommand()
                cmdC.CommandText = ("select * from NoteFatture where CodiceProvincia = ? And CodiceComune = ?")
                cmdC.Parameters.AddWithValue("@CodiceProvincia", CodiceProvincia)
                cmdC.Parameters.AddWithValue("@CodiceComune", CodiceComune)
                cmdC.Connection = cnOspiti
                Dim RdCom As OleDbDataReader = cmdC.ExecuteReader()
                If RdCom.Read Then
                    AddDescrizione = campodb(RdCom.Item("NoteUp"))
                End If
                RdCom.Close()
                cnOspiti.Close()
            End If
            If Mid(Tipo & Space(10), 1, 1) = "J" Then
                Dim Ospite As New ClsComune

                Ospite.Provincia = CodiceProvincia
                Ospite.Comune = CodiceComune
                Ospite.Leggi(Session("DC_OSPITE"))
                DO11_RAGIONESOCIALE = Ospite.Descrizione
                DO11_PIVA = campodbN(Ospite.PartitaIVA)
                DO11_CF = Ospite.CodiceFiscale
                DO11_INDIRIZZO = Ospite.RESIDENZAINDIRIZZO1
                DO11_CAP = Ospite.RESIDENZACAP1

                DO11_EXPORT = Ospite.CONTOPERESATTO

                Dim DcCom As New ClsComune

                DcCom.Provincia = Ospite.RESIDENZAPROVINCIA1
                DcCom.Comune = Ospite.RESIDENZACOMUNE1
                DcCom.Leggi(Session("DC_OSPITE"))

                DO11_CITTA_NOME = DcCom.Descrizione
                DO11_CITTA_ISTAT = DcCom.Provincia & DcCom.Comune

                Dim DcProv As New ClsComune

                DcProv.Provincia = Ospite.RESIDENZAPROVINCIA1
                DcProv.Comune = ""
                DcProv.Leggi(Session("DC_OSPITE"))
                DO11_PROVINCIA = DcProv.CodificaProvincia
                DO11_CONDPAG_CODICE_SEN = Ospite.ModalitaPagamento
                Dim MPAg As New ClsModalitaPagamento

                MPAg.Codice = Ospite.ModalitaPagamento
                MPAg.Leggi(Session("DC_OSPITE"))
                DO11_CONDPAG_NOME_SEN = MPAg.DESCRIZIONE

                Dim cnOspiti As OleDbConnection

                cnOspiti = New Data.OleDb.OleDbConnection(Session("DC_OSPITE"))

                cnOspiti.Open()

                Dim cmdC As New OleDbCommand()
                cmdC.CommandText = ("select * from NoteFatture where CodiceProvincia = ? And CodiceComune = ?")
                cmdC.Parameters.AddWithValue("@CodiceProvincia", CodiceProvincia)
                cmdC.Parameters.AddWithValue("@CodiceComune", CodiceComune)
                cmdC.Connection = cnOspiti
                Dim RdCom As OleDbDataReader = cmdC.ExecuteReader()
                If RdCom.Read Then
                    AddDescrizione = campodb(RdCom.Item("NoteUp"))
                End If
                RdCom.Close()
                cnOspiti.Close()
            End If
            If Mid(Tipo & Space(10), 1, 1) = "R" Then
                Dim Ospite As New ClsUSL

                Ospite.CodiceRegione = CodiceRegione
                Ospite.Leggi(Session("DC_OSPITE"))
                DO11_RAGIONESOCIALE = Ospite.Nome
                DO11_PIVA = campodbN(Ospite.PARTITAIVA)
                DO11_CF = Ospite.CodiceFiscale
                DO11_INDIRIZZO = Ospite.RESIDENZAINDIRIZZO1
                DO11_CAP = Ospite.RESIDENZACAP1

                DO11_EXPORT = Ospite.CONTOPERESATTO

                Dim DcCom As New ClsComune

                DcCom.Provincia = Ospite.RESIDENZAPROVINCIA1
                DcCom.Comune = Ospite.RESIDENZACOMUNE1
                DcCom.Leggi(Session("DC_OSPITE"))

                DO11_CITTA_NOME = DcCom.Descrizione
                DO11_CITTA_ISTAT = DcCom.Provincia & DcCom.Comune

                Dim DcProv As New ClsComune

                DcProv.Provincia = Ospite.RESIDENZAPROVINCIA1
                DcProv.Comune = ""
                DcProv.Leggi(Session("DC_OSPITE"))
                DO11_PROVINCIA = DcProv.CodificaProvincia
                DO11_CONDPAG_CODICE_SEN = Ospite.ModalitaPagamento
                Dim MPAg As New ClsModalitaPagamento

                MPAg.Codice = Ospite.ModalitaPagamento
                MPAg.Leggi(Session("DC_OSPITE"))
                DO11_CONDPAG_NOME_SEN = MPAg.DESCRIZIONE
            End If



            If CausaleContabile.Tipo = "P" Then
                

            Else


              
                twFatture.WriteLine("<DOCVENTE>")

                If Dd_RegistroDATA.SelectedValue = 1 Then
                    If CausaleContabile.TipoDocumento = "NC" Then
                        twFatture.WriteLine("<TIPDOC>F4</TIPDOC>")
                    Else
                        twFatture.WriteLine("<TIPDOC>F4</TIPDOC>")
                    End If
                Else
                    If CausaleContabile.TipoDocumento = "NC" Then
                        twFatture.WriteLine("<TIPDOC>FE</TIPDOC>")
                    Else
                        twFatture.WriteLine("<TIPDOC>FE</TIPDOC>")
                    End If
                End If

                twFatture.WriteLine("<ANNDOC>" & Registrazione.AnnoProtocollo & "</ANNDOC>")
                twFatture.WriteLine("<NUMDOC>" & Registrazione.NumeroDocumento & "</NUMDOC>")
                twFatture.WriteLine("<DATDOC>" & Format(Registrazione.DataRegistrazione, "dd/MM/yyyy") & "</DATDOC>")
                twFatture.WriteLine("<CLFR>C</CLFR>")
                twFatture.WriteLine("<CODCLI>" & DO11_EXPORT & "</CODCLI>")
                twFatture.WriteLine("<CAMBIO>0</CAMBIO>")
                twFatture.WriteLine("<RIFCAMBIO>E</RIFCAMBIO>")
                twFatture.WriteLine("<SCOFIN1>0</SCOFIN1>")
                twFatture.WriteLine("<SCOFIN2>0</SCOFIN2>")
                twFatture.WriteLine("<SCONTO>0</SCONTO>")

                TabTrascodifica.TIPOTAB = "MP"
                TabTrascodifica.SENIOR = Registrazione.CodicePagamento
                TabTrascodifica.EXPORT = 0
                TabTrascodifica.Leggi(Session("DC_TABELLE"))


                twFatture.WriteLine("<CODMAG>02</CODMAG>")
                twFatture.WriteLine("<CAUMAG>201</CAUMAG>")
                If TabTrascodifica.EXPORT <> "0" Then

                    twFatture.WriteLine("<CODPAG>" & TabTrascodifica.EXPORT & "</CODPAG>")
                Else
                    twFatture.WriteLine("<CODPAG>" & TabTrascodifica.SENIOR & "</CODPAG>")
                End If

                twFatture.WriteLine("<VSORD>" & DO11_ORDINE & "</VSORD>")

                twFatture.WriteLine("<CODCLI1>" & DO11_EXPORT & "</CODCLI1>")
                twFatture.WriteLine("<RAGSOC>" & DO11_RAGIONESOCIALE & "</RAGSOC>")
                twFatture.WriteLine("<INDIR>" & DO11_INDIRIZZO & "</INDIR>")
                twFatture.WriteLine("<CAP>" & DO11_CAP & "</CAP>")
                twFatture.WriteLine("<LOCAL>" & DO11_CITTA_NOME & "</LOCAL>")
                twFatture.WriteLine("<PROV>" & DO11_PROVINCIA & "</PROV>")
                twFatture.WriteLine("<NUMBOL>0</NUMBOL>")



                twFatture.WriteLine("<LISVEN>0</LISVEN>")
                Dim Imponibile As Double = 0
                Dim Imposta As Double = 0
                Dim Quantita As Integer = 0


                For Indice = 0 To Registrazione.Righe.Length - 1
                    If Not IsNothing(Registrazione.Righe(Indice)) Then
                        If Registrazione.Righe(Indice).Tipo = "IV" And (Registrazione.Righe(Indice).Imponibile > 0 Or Registrazione.Righe(Indice).Importo > 0) Then
                            Imponibile = Imponibile + Registrazione.Righe(Indice).Imponibile
                            Imposta = Imposta + Registrazione.Righe(Indice).Importo
                        End If

                        Quantita = Quantita + Registrazione.Righe(Indice).Quantita
                    End If
                Next

                twFatture.WriteLine("<TOTIMP>" & Imponibile & "</TOTIMP>")
                twFatture.WriteLine("<TOTIVA>" & Imposta & "</TOTIVA>")



                twFatture.WriteLine("<TOT001>0</TOT001>")
                twFatture.WriteLine("<TOT002>0</TOT002>")
                twFatture.WriteLine("<TOT003>0</TOT003>")
                twFatture.WriteLine("<IMPSCA01>0</IMPSCA01>")
                twFatture.WriteLine("<IMPSCA02>0</IMPSCA02>")
                twFatture.WriteLine("<IMPSCA03>0</IMPSCA03>")
                twFatture.WriteLine("<IMPSCA04>0</IMPSCA04>")
                twFatture.WriteLine("<IMPSCA05>0</IMPSCA05>")
                twFatture.WriteLine("<IMPSCA06>0</IMPSCA06>")
                twFatture.WriteLine("<IMPSCA07>0</IMPSCA07>")
                twFatture.WriteLine("<IMPSCA08>0</IMPSCA08>")
                twFatture.WriteLine("<IMPSCA09>0</IMPSCA09>")
                If Dd_RegistroDATA.SelectedValue = 1 Then
                    twFatture.WriteLine("<STATO>1</STATO>")
                Else
                    twFatture.WriteLine("<STATO>1</STATO>")
                    twFatture.WriteLine("<IVASOS>Y</IVASOS>")
                End If
                twFatture.WriteLine("<TOTIMPCM>" & Imponibile & "</TOTIMPCM>")
                twFatture.WriteLine("<TOTIVACM>" & Imposta & "</TOTIVACM>")
                twFatture.WriteLine("<TOT002CM>0</TOT002CM>")
                twFatture.WriteLine("<TOT003CM>0</TOT003CM>")
                twFatture.WriteLine("<CENTRO>001</CENTRO>")
                twFatture.WriteLine("<ANALIT>S</ANALIT>")
                twFatture.WriteLine("<CODBAN>01</CODBAN>")
                twFatture.WriteLine("<RTIMPNON>0</RTIMPNON>")
                twFatture.WriteLine("<RTIMPNON2>0</RTIMPNON2>")
                twFatture.WriteLine("<RTIMPNET>0</RTIMPNET>")
                twFatture.WriteLine("<RTPERRIT>0</RTPERRIT>")
                twFatture.WriteLine("<RTIMPRIT>0</RTIMPRIT>")
                twFatture.WriteLine("<RTIMPESC>0</RTIMPESC>")
                'If Dd_RegistroDATA.SelectedValue > 1 Then
                twFatture.WriteLine("<STATOFE>1</STATOFE>")
                'End If

                twFatture.WriteLine("<BOLVIRT>0</BOLVIRT>")
                twFatture.WriteLine("<NUMDIN>0</NUMDIN>")




                Dim Codosp As Integer
                Dim Scorri As Integer

                For Scorri = 0 To 100
                    If Not IsNothing(Registrazione.Righe(Scorri)) Then
                        If Registrazione.Righe(Scorri).Importo > 0 And (Registrazione.Righe(Scorri).RigaDaCausale = 3 Or Registrazione.Righe(Scorri).RigaDaCausale = 6 Or Registrazione.Righe(Scorri).RigaDaCausale = 5) Then
                            twFatture.WriteLine("<DOCVENCO>")
                            If Dd_RegistroDATA.SelectedValue = 1 Then
                                twFatture.WriteLine("<TIPDOC>F4</TIPDOC>")
                            Else
                                twFatture.WriteLine("<TIPDOC>FE</TIPDOC>")
                            End If
                            twFatture.WriteLine("<ANNDOC>" & Registrazione.AnnoProtocollo & "</ANNDOC>")
                            twFatture.WriteLine("<NUMDOC>" & Registrazione.NumeroDocumento & "</NUMDOC>")
                            twFatture.WriteLine("<NUMRIG>" & Scorri & "</NUMRIG>")
                            twFatture.WriteLine("<LISVEN>0</LISVEN>")

                            If Dd_RegistroDATA.SelectedValue = 1 Then
                                twFatture.WriteLine("<CODART>" & DO11_CODART & "</CODART>")
                            Else
                                Codosp = Int(Registrazione.Righe(Scorri).SottocontoContropartita / 100)

                                Dim LeggiAnagrafica As New ClsOspite

                                LeggiAnagrafica.CodiceOspite = Codosp
                                LeggiAnagrafica.Leggi(Session("DC_OSPITE"), Codosp)

                                twFatture.WriteLine("<CODART>" & LeggiAnagrafica.CodiceCup & "</CODART>")
                            End If

                            If Dd_RegistroDATA.SelectedValue = 1 Then
                                twFatture.WriteLine("<DESART>" & DO11_RAGIONESOCIALE & " Periodo " & Registrazione.MeseCompetenza & "/" & Registrazione.AnnoCompetenza & "</DESART>")
                            Else
                                Codosp = Int(Registrazione.Righe(Scorri).SottocontoContropartita / 100)

                                Dim LeggiAnagrafica As New ClsOspite

                                LeggiAnagrafica.CodiceOspite = Codosp
                                LeggiAnagrafica.Leggi(Session("DC_OSPITE"), Codosp)

                                Dim UltMov As New Cls_Movimenti
                                Dim KAssezz As New Cls_Movimenti
                                Dim Uscita As String = ""

                                Dim NefeshMese As Integer
                                Dim NefeshAnno As Integer

                                NefeshMese = Registrazione.MeseCompetenza
                                NefeshAnno = Registrazione.AnnoCompetenza

                                If Registrazione.Righe(Scorri).Descrizione.IndexOf("Assenze") >= 0 Then
                                    KAssezz.CENTROSERVIZIO = Registrazione.CentroServizio
                                    KAssezz.CodiceOspite = Codosp
                                    KAssezz.UltimaDataUscita(Session("DC_OSPITE"), KAssezz.CodiceOspite, KAssezz.CENTROSERVIZIO, DateSerial(NefeshAnno, NefeshMese, GiorniMese(NefeshMese, NefeshAnno)))
                                    UltMov.CENTROSERVIZIO = KAssezz.CENTROSERVIZIO
                                    UltMov.CodiceOspite = KAssezz.CodiceOspite
                                    UltMov.UltimaMovimentoPrimaData(Session("DC_OSPITE"), UltMov.CodiceOspite, UltMov.CENTROSERVIZIO, DateSerial(NefeshAnno, NefeshMese, GiorniMese(NefeshMese, NefeshAnno)))

                                    If KAssezz.Causale <> "" And ((Month(KAssezz.Data) = NefeshMese And Year(KAssezz.Data) = NefeshAnno) Or UltMov.TipoMov = "03" Or (UltMov.TipoMov = "04" And Day(UltMov.Data) > 1 And Month(UltMov.Data) = NefeshMese And Year(UltMov.Data) = NefeshAnno)) Then

                                        Dim TipoCausale As New Cls_CausaliEntrataUscita


                                        TipoCausale.Codice = KAssezz.Causale
                                        TipoCausale.LeggiCausale(Session("DC_OSPITE"))

                                        Uscita = "(" & TipoCausale.Descrizione & ")"
                                    End If
                                End If


                                Dim ImpComune As New Cls_ImportoComune

                                ImpComune.DescrizioneRiga = ""
                                ImpComune.CENTROSERVIZIO = Registrazione.Righe(Scorri).CentroServizio
                                ImpComune.CODICEOSPITE = Codosp
                                ImpComune.UltimaData(Session("DC_OSPITE"), Codosp, ImpComune.CENTROSERVIZIO)



                                twFatture.WriteLine("<DESART>Retta " & Mid(LeggiAnagrafica.CognomeOspite, 1, 1) & ". " & Mid(LeggiAnagrafica.NomeOspite, 1, 1) & ". Periodo " & Registrazione.MeseCompetenza & "/" & Registrazione.AnnoCompetenza & " " & Uscita & " " & ImpComune.DescrizioneRiga & "</DESART>")
                            End If

                            If Registrazione.Righe(Scorri).Quantita = 0 Then
                                Registrazione.Righe(Scorri).Quantita = 1
                            End If

                            twFatture.WriteLine("<CODMER>02</CODMER>")
                            twFatture.WriteLine("<UNMIS>GG</UNMIS>")
                            twFatture.WriteLine("<QUANTI>" & Registrazione.Righe(Scorri).Quantita & "</QUANTI>")
                            twFatture.WriteLine("<QTA1>0</QTA1>")
                            twFatture.WriteLine("<QTA2>0</QTA2>")
                            twFatture.WriteLine("<QTA3>0</QTA3>")
                            twFatture.WriteLine("<NUMPEZ>0</NUMPEZ>")
                            twFatture.WriteLine("<CAMBIO>0</CAMBIO>")
                            twFatture.WriteLine("<RIFCAMBIO>E</RIFCAMBIO>")
                            If Registrazione.Righe(Scorri).RigaDaCausale = 6 Then                                
                                twFatture.WriteLine("<PREZZO>-" & Math.Round(Registrazione.Righe(Scorri).Importo / Registrazione.Righe(Scorri).Quantita, 10) & "</PREZZO>")
                            Else
                                twFatture.WriteLine("<PREZZO>" & Math.Round(Registrazione.Righe(Scorri).Importo / Registrazione.Righe(Scorri).Quantita, 10) & "</PREZZO>")
                            End If

                            twFatture.WriteLine("<SCONTO>0</SCONTO>")
                            twFatture.WriteLine("<SCONTO2>0</SCONTO2>")
                            twFatture.WriteLine("<SCONTO3>0</SCONTO3>")
                            twFatture.WriteLine("<PROVV>0</PROVV>")

                            If Dd_RegistroDATA.SelectedValue = 1 Then
                                twFatture.WriteLine("<CODIVA>05</CODIVA>")
                            Else
                                twFatture.WriteLine("<CODIVA>05SP</CODIVA>")
                            End If


                            TabTrascodifica.TIPOTAB = "CC"
                            TabTrascodifica.SENIOR = Registrazione.CentroServizio
                            TabTrascodifica.EXPORT = 0
                            TabTrascodifica.Leggi(Session("DC_TABELLE")) '7503

                            If Dd_RegistroDATA.SelectedValue = 1 Then

                                TabTrascodifica.TIPOTAB = "CA"
                                TabTrascodifica.SENIOR = Registrazione.CentroServizio
                                TabTrascodifica.EXPORT = 0
                                TabTrascodifica.Leggi(Session("DC_TABELLE")) '73031   privato
                            End If

                            twFatture.WriteLine("<CONTROP>" & TabTrascodifica.EXPORT & "</CONTROP>")


                            twFatture.WriteLine("<NUMORD>0</NUMORD>")
                            twFatture.WriteLine("<RIGORD>0</RIGORD>")


                            TabTrascodifica.TIPOTAB = "CT"
                            TabTrascodifica.SENIOR = Registrazione.CentroServizio
                            TabTrascodifica.EXPORT = 0
                            TabTrascodifica.Leggi(Session("DC_TABELLE")) '003


                            twFatture.WriteLine("<CENTRO>" & TabTrascodifica.EXPORT & "</CENTRO>")
                            twFatture.WriteLine("<POSRIG>" & Scorri & "</POSRIG>")
                            twFatture.WriteLine("<ESPLDIBA>P</ESPLDIBA>")
                            twFatture.WriteLine("<IMPPROAG>0</IMPPROAG>")
                            twFatture.WriteLine("<IMPPROAGC>0</IMPPROAGC>")
                            twFatture.WriteLine("<IMPONPRAG>0</IMPONPRAG>")
                            twFatture.WriteLine("<IMPONPRAC>0</IMPONPRAC>")
                            twFatture.WriteLine("<PROVIMPAG>0</PROVIMPAG>")
                            twFatture.WriteLine("<PROVIMPAC>0</PROVIMPAC>")
                            twFatture.WriteLine("</DOCVENCO>")
                        End If
                    End If
                Next
                twFatture.WriteLine("</DOCVENTE>")
            End If
        Loop

        twFatture.WriteLine("</ImportExportG1>")

        twFatture.Close()


        Try

            Response.Clear()
            Response.Buffer = True
            Response.ContentType = "text/plain"
            Response.AppendHeader("content-disposition", "attachment;filename=Fatture.xml")
            Response.WriteFile(NomeFile)
            Response.Flush()
            Response.End()

        Catch ex As Exception

        End Try

    End Function

End Class



