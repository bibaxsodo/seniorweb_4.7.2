﻿Imports Microsoft.VisualBasic
Imports System.Data.OleDb
Imports System.Web.Hosting
Imports System.Diagnostics
Imports System.ComponentModel
Imports System.Threading
Imports System.IO.Compression
Partial Class OspitiWeb_Export_Export_DocumentiBPoint
    Inherits System.Web.UI.Page
    Dim MyTable As New System.Data.DataTable("tabella")
    Dim Tabella As New System.Data.DataTable("EstraiTabella")
    Dim MyDataSet As New System.Data.DataSet()
    Dim Vt_Cliente(1000) As String

    Class Anagrafica
        Public CODICEEXPORT As String = ""
        Public DO11_RAGIONESOCIALE As String = ""
        Public DO11_PIVA As String = ""
        Public DO11_CF As String = ""
        Public DO11_INDIRIZZO As String = ""
        Public DO11_CAP As String = ""
        Public DO11_CITTA_NOME As String = ""
        Public DO11_CITTA_ISTAT As String = ""
        Public DO11_PROVINCIA As String = ""
        Public DO11_CONDPAG_CODICE_SEN As String = ""
        Public DO11_CONDPAG_NOME_SEN As String = ""
        Public DO11_BANCA_CIN As String = ""
        Public DO11_BANCA_ABI As String = ""
        Public DO11_BANCA_CAB As String = ""
        Public DO11_BANCA_CONTO As String = ""
        Public DO11_BANCA_IBAN As String = ""
        Public DO11_BANCA_IDMANDATO As String = ""
        Public DO11_BANCA_DATAMANDATO As String = ""
        Public DO11_OPPOSIZIONE As String = ""



        Public DES_RAGIONESOCIALE As String = ""
        Public DES_PIVA As String = ""
        Public DES_CF As String = ""
        Public DES_INDIRIZZO As String = ""
        Public DES_CAP As String = ""
        Public DES_CITTA_NOME As String = ""
        Public DES_CITTA_ISTAT As String = ""
        Public DES_PROVINCIA As String = ""

        Public AddDescrizione As String

        Public CodiceOspite As Integer
    End Class


    Class MOVPART
        Public Codicefiscale As String
        Public PartitaIVA As String
        Public Ragionesociale As String
        Public Argomento As String
        Public Competenza As String
        Public Numeropartita As String
        Public Numerointerno As String
        Public Dataoperazione As String
        Public Codiceconto As String
        Public CodiceCliFor As String
        Public TipoRegistro As String
        Public CodiceRegistro As String
        Public AnnoDocumento As String
        Public NumeroDocumento As String
        Public Numeroprotocollo As String
        Public DataDocumento As String
        Public Annopagamento As String
        Public Tipomovimento As String
        Public Codicecausale As String
        Public CodicePagamento As String
        Public Datascadenza As String
        Public Importodelmovimento As String
        Public Segnoimporto As String
        Public CodiceAgente As String
        Public CodiceValutaEstera As String
        Public Cambio As String
        Public CodiceABICli As String
        Public CodiceCABCli As String
        Public CodiceABIAz As String
        Public CodiceCABAz As String
        Public Numerodocumento1 As String
        Public Numeroprotocollo1 As String
        Public Datadocumento1 As String
        Public Annotazioni As String
        Public Tipopagamento As String
        Public Datanuovascadenza As String
        Public Importospese As String
        Public ContatoreUnivoco As String
        Public ImportoIVA As String
        Public TipoAnagrafica As String
        Public TipoValuta As String
        Public IVAperCassa As String
        Public TermineIVACassa As String
        Public CompetenzaIVA As String
        Public Libero As String
    End Class


    Class MOVIM
        Public Codicefiscale As String
        Public PartitaIVA As String
        Public Ragionesociale As String
        Public EsercizioIVA As String
        Public EsercizioCOGE As String
        Public Argomento As String
        Public Competenza As String
        Public Numeropartita As String
        Public Numerointerno As String
        Public Dataoperazione As String
        Public Codiceconto As String
        Public Tiporegistro As String
        Public Codiceregistro As String
        Public Numeroprotocollo As String
        Public Tipoarticolo As String
        Public Tipomovimento As String
        Public Codicecosto As String
        Public Codicecausale As String
        Public NumDescrizcausale As String
        Public Segnoimporto As String
        Public Importo As String
        Public Segnocontabileimporto As String
        Public Annotazioni As String
        Public Datadocumento As String
        Public Numerodocumento As String
        Public TipoAnagrafica As String
        Public FlagStorico As String
        Public Libero As String
    End Class

    Class IVAMOV
        Public Codicefiscale As String
        Public PartitaIVA As String
        Public Ragionesociale As String
        Public Numeropartita As String
        Public Numerointerno As String
        Public Segnoimponibile As String
        Public Imponibile As String
        Public SegnoImportoIVA As String
        Public Imposta As String
        Public Codicecausale As String
        Public CodiceIVA As String
        Public Filler As String
        Public Mercidestinateallarivendita As String
        Public Quadroa As String
        Public Codicecostiricavi As String
        Public Flagesercizio As String
        Public Percentualedetraibilita As String
        Public CodiceTabellaProdottiAgricoltori As String
        Public CodiceIVAcompensativoAgricoltori As String
        Public Datafatturasospeso As String
        Public Multipunto As String
        Public CausaleAnalitica As String
        Public Codicesottoconto As String
        Public Libero As String
    End Class
    Class CLISISP
        Public CodiceAnagrafico As String
        Public Codicefiscale As String
        Public PartitaIVA As String
        Public Tipoanagrafica As String
        Public Ragionesociale As String
        Public Indirizzo As String
        Public Numero As String
        Public Comune As String
        Public cap As String
        Public Statoestero As String
        Public Telefono As String
        Public Telefax As String
        Public Telex As String
        Public Prefisso As String
        Public Frazione As String
        Public Provincia As String
        Public Testaziendamunicipalizzata As String
        Public Codicecomune As String
        Public Codicefrazione As String
    End Class



    Private Sub ScriviMOVPART(ByVal Dati As MOVPART, ByVal File As System.IO.TextWriter)
        Dim Stringa As String = ""

        Stringa = ""
        Stringa = Stringa & AdattaLunghezza(Dati.Codicefiscale, 16)
        Stringa = Stringa & AdattaLunghezzaNumero(Dati.PartitaIVA, 11)
        Stringa = Stringa & AdattaLunghezza(Dati.Ragionesociale, 50)

        Stringa = Stringa & AdattaLunghezza(Dati.Argomento, 1)



        Stringa = Stringa & AdattaLunghezzaNumero(Dati.Numeropartita, 7)
        Stringa = Stringa & AdattaLunghezzaNumero(Dati.Numerointerno, 3)

        Stringa = Stringa & AdattaLunghezzaNumero(Dati.Dataoperazione, 8)

        Stringa = Stringa & AdattaLunghezza(Dati.Codiceconto, 6)
        Stringa = Stringa & AdattaLunghezza(Dati.CodiceCliFor, 6)
        Stringa = Stringa & AdattaLunghezzaNumero(Dati.TipoRegistro, 2)
        Stringa = Stringa & AdattaLunghezzaNumero(Dati.CodiceRegistro, 2)



        Stringa = Stringa & AdattaLunghezzaNumero(Dati.AnnoDocumento, 4)
        Stringa = Stringa & AdattaLunghezza(Dati.NumeroDocumento, 7)
        Stringa = Stringa & AdattaLunghezzaNumero(Dati.Numeroprotocollo, 7)
        Stringa = Stringa & AdattaLunghezzaNumero(Dati.DataDocumento, 8)
        Stringa = Stringa & AdattaLunghezzaNumero(Dati.Annopagamento, 4)
        Stringa = Stringa & AdattaLunghezza(Dati.Tipomovimento, 1)
        Stringa = Stringa & AdattaLunghezza(Dati.Codicecausale, 5)
        Stringa = Stringa & AdattaLunghezza(Dati.CodicePagamento, 6)
        Stringa = Stringa & AdattaLunghezzaNumero(Dati.Datascadenza, 8)
        Stringa = Stringa & AdattaLunghezzaNumero(Dati.Importodelmovimento, 12)
        Stringa = Stringa & AdattaLunghezza(Dati.Segnoimporto, 1)
        Stringa = Stringa & AdattaLunghezza(Dati.CodiceAgente, 3)
        Stringa = Stringa & AdattaLunghezza(Dati.CodiceValutaEstera, 3)
        Stringa = Stringa & AdattaLunghezzaNumero(Dati.Cambio, 9)
        Stringa = Stringa & AdattaLunghezzaNumero(Dati.CodiceABICli, 5)
        Stringa = Stringa & AdattaLunghezzaNumero(Dati.CodiceCABCli, 5)
        Stringa = Stringa & AdattaLunghezzaNumero(Dati.CodiceABIAz, 5)
        Stringa = Stringa & AdattaLunghezzaNumero(Dati.CodiceCABAz, 5)
        Stringa = Stringa & AdattaLunghezza(Dati.Numerodocumento1, 7)
        Stringa = Stringa & AdattaLunghezzaNumero(Dati.Numeroprotocollo1, 7)
        Stringa = Stringa & AdattaLunghezzaNumero(Dati.Datadocumento1, 8)
        Stringa = Stringa & AdattaLunghezza(Dati.Annotazioni, 30)
        Stringa = Stringa & AdattaLunghezzaNumero(Dati.Tipopagamento, 1)
        Stringa = Stringa & AdattaLunghezzaNumero(Dati.Datanuovascadenza, 8)
        Stringa = Stringa & AdattaLunghezzaNumero(Dati.Importospese, 12)
        Stringa = Stringa & AdattaLunghezzaNumero(Dati.ContatoreUnivoco, 8)
        Stringa = Stringa & AdattaLunghezzaNumero(Dati.ImportoIVA, 12)
        Stringa = Stringa & AdattaLunghezza(Dati.TipoAnagrafica, 1)
        Stringa = Stringa & AdattaLunghezza(Dati.TipoValuta, 1)
        Stringa = Stringa & AdattaLunghezza(Dati.IVAperCassa, 1)
        Stringa = Stringa & AdattaLunghezzaNumero(Dati.TermineIVACassa, 4)
        Stringa = Stringa & AdattaLunghezza(Dati.CompetenzaIVA, 1)
        Stringa = Stringa & AdattaLunghezza(Dati.Libero, 7)

        File.WriteLine(Stringa)

    End Sub



    Private Sub ScriviMOVIM(ByVal Dati As MOVIM, ByVal File As System.IO.TextWriter)
        Dim Stringa As String = ""

        Stringa = ""
        Stringa = Stringa & AdattaLunghezza(Dati.Codicefiscale, 16)
        Stringa = Stringa & AdattaLunghezzaNumero(Dati.PartitaIVA, 11)
        Stringa = Stringa & AdattaLunghezza(Dati.Ragionesociale, 50)

        Stringa = Stringa & AdattaLunghezzaNumero(Val(Dati.EsercizioIVA) - 2000, 2)

        Stringa = Stringa & AdattaLunghezzaNumero(Dati.EsercizioCOGE, 4)
        Stringa = Stringa & AdattaLunghezza(Dati.Argomento, 1)

        Stringa = Stringa & AdattaLunghezza(Dati.Competenza, 1)
        Stringa = Stringa & AdattaLunghezzaNumero(Dati.Numeropartita, 7)
        Stringa = Stringa & AdattaLunghezzaNumero(Dati.Numerointerno, 3)

        Stringa = Stringa & AdattaLunghezzaNumero(Dati.Dataoperazione, 6)
        Stringa = Stringa & AdattaLunghezza(Dati.Codiceconto, 12)
        Stringa = Stringa & AdattaLunghezzaNumero(Dati.Tiporegistro, 2)
        Stringa = Stringa & AdattaLunghezzaNumero(Dati.Codiceregistro, 2)
        Stringa = Stringa & AdattaLunghezzaNumero(Dati.Numeroprotocollo, 7)
        Stringa = Stringa & AdattaLunghezzaNumero(Dati.Tipoarticolo, 1)
        Stringa = Stringa & AdattaLunghezza(Dati.Tipomovimento, 1)
        Stringa = Stringa & AdattaLunghezzaNumero(Dati.Codicecosto, 3)

        Stringa = Stringa & AdattaLunghezzaNumero(Dati.Codicecausale, 5)
        Stringa = Stringa & AdattaLunghezzaNumero(Dati.NumDescrizcausale, 1)
        Stringa = Stringa & AdattaLunghezza(Dati.Segnoimporto, 1)
        Stringa = Stringa & AdattaLunghezzaNumero(Dati.Importo, 13)
        Stringa = Stringa & AdattaLunghezzaNumero(Dati.Segnocontabileimporto, 1)
        Stringa = Stringa & AdattaLunghezza(Dati.Annotazioni, 30)
        Stringa = Stringa & AdattaLunghezzaNumero(Dati.Datadocumento, 6)
        Stringa = Stringa & AdattaLunghezzaNumero(Dati.Numerodocumento, 7)
        Stringa = Stringa & AdattaLunghezza(Dati.TipoAnagrafica, 1)
        Stringa = Stringa & AdattaLunghezza(Dati.FlagStorico, 1)
        Stringa = Stringa & AdattaLunghezza(Dati.Libero, 20)

        File.WriteLine(Stringa)

    End Sub


    Private Sub ScriviIVAMOV(ByVal Dati As IVAMOV, ByVal File As System.IO.TextWriter)
        Dim Stringa As String = ""

        Stringa = ""
        Stringa = Stringa & AdattaLunghezza(Dati.Codicefiscale, 16)
        Stringa = Stringa & AdattaLunghezzaNumero(Dati.PartitaIVA, 11)
        Stringa = Stringa & AdattaLunghezza(Dati.Ragionesociale, 50)
        Stringa = Stringa & AdattaLunghezzaNumero(Dati.Numeropartita, 7)
        Stringa = Stringa & AdattaLunghezzaNumero(Dati.Numerointerno, 2)
        Stringa = Stringa & AdattaLunghezza(Dati.Segnoimponibile, 1)
        Stringa = Stringa & AdattaLunghezzaNumero(Dati.Imponibile, 13)
        Stringa = Stringa & AdattaLunghezza(Dati.SegnoImportoIVA, 1)
        Stringa = Stringa & AdattaLunghezzaNumero(Dati.Imposta, 13)


        Stringa = Stringa & AdattaLunghezzaNumero(Dati.Codicecausale, 5)
        Stringa = Stringa & AdattaLunghezza(Dati.CodiceIVA, 3)

        Stringa = Stringa & AdattaLunghezza(Dati.Filler, 2)
        Stringa = Stringa & AdattaLunghezza(Dati.Mercidestinateallarivendita, 1)
        Stringa = Stringa & AdattaLunghezza(Dati.Quadroa, 1)
        Stringa = Stringa & AdattaLunghezza(Dati.Codicecostiricavi, 6)
        Stringa = Stringa & AdattaLunghezza(Dati.Flagesercizio, 1)
        Stringa = Stringa & AdattaLunghezzaNumero(Dati.Percentualedetraibilita, 5)
        Stringa = Stringa & AdattaLunghezza(Dati.CodiceTabellaProdottiAgricoltori, 3)
        Stringa = Stringa & AdattaLunghezza(Dati.CodiceIVAcompensativoAgricoltori, 3)
        Stringa = Stringa & AdattaLunghezza(Dati.Datafatturasospeso, 1)
        Stringa = Stringa & AdattaLunghezza(Dati.Multipunto, 2)
        Stringa = Stringa & AdattaLunghezza(Dati.CausaleAnalitica, 2)
        Stringa = Stringa & AdattaLunghezzaNumero(Dati.Codicesottoconto, 4)
        Stringa = Stringa & AdattaLunghezza(Dati.Libero, 16)


        File.WriteLine(Stringa)

    End Sub


    Private Sub ScriviCLISISP(ByVal Dati As CLISISP, ByVal File As System.IO.TextWriter)
        Dim Stringa As String = ""

        Stringa = ""
        Stringa = Stringa & AdattaLunghezza(Dati.CodiceAnagrafico, 6)

        Stringa = Stringa & AdattaLunghezza(Dati.Codicefiscale, 16)

        If Val(Dati.PartitaIVA) = 0 Then
            Stringa = Stringa & AdattaLunghezza("", 11)
            Stringa = Stringa & AdattaLunghezza("P", 1)
        Else
            Stringa = Stringa & AdattaLunghezzaNumero(Dati.PartitaIVA, 11)
            Stringa = Stringa & AdattaLunghezza(Dati.Tipoanagrafica, 1)
        End If
        
        Stringa = Stringa & AdattaLunghezza(Dati.Ragionesociale.ToUpper, 50)
        Stringa = Stringa & AdattaLunghezza(Dati.Indirizzo.ToUpper, 28)
        Stringa = Stringa & AdattaLunghezza(Dati.Numero, 7)
        If isnothing(Dati.Comune) Then
            Dati.Comune=""
        End If

        Stringa = Stringa & AdattaLunghezza(Dati.Comune.ToUpper, 35)

        Stringa = Stringa & AdattaLunghezzaNumero(Dati.cap, 5)
        Stringa = Stringa & AdattaLunghezza(Dati.Statoestero, 35)
        Stringa = Stringa & AdattaLunghezza(Dati.Telefono, 20)
        Stringa = Stringa & AdattaLunghezza(Dati.Telefax, 20)
        Stringa = Stringa & AdattaLunghezza(Dati.Telex, 20)
        Stringa = Stringa & AdattaLunghezza(Dati.Prefisso, 4)
        Stringa = Stringa & AdattaLunghezza(Dati.Frazione, 35)


        Stringa = Stringa & AdattaLunghezza(Dati.Provincia, 2)
        Stringa = Stringa & AdattaLunghezza(Dati.Testaziendamunicipalizzata, 1)

        Stringa = Stringa & AdattaLunghezza(Dati.Codicecomune, 4)
        Stringa = Stringa & AdattaLunghezza(Dati.Codicefrazione, 2)

        File.WriteLine(Stringa)


    End Sub




    Private Sub DecodificaAnagrafica(ByRef Dati As Anagrafica, ByVal Registrazione As Cls_MovimentoContabile)
        Dim cn As OleDbConnection
        Dim Cliente As String
        Dim CodiceOspite As Integer
        Dim CodiceParente As Integer
        Dim CodiceProvincia As String
        Dim CodiceComune As String
        Dim CodiceRegione As String
        Dim Tipo As String

        cn = New Data.OleDb.OleDbConnection(Session("DC_GENERALE"))
        cn.Open()
        Tipo = Registrazione.Tipologia

        Dim cmdRd As New OleDbCommand()
        cmdRd.CommandText = "Select * From MovimentiContabiliRiga Where Numero = ?  And Tipo ='CF'   "
        cmdRd.Connection = cn
        cmdRd.Parameters.AddWithValue("@Numero", Registrazione.NumeroRegistrazione)
        Dim MyReadSC As OleDbDataReader = cmdRd.ExecuteReader()
        If MyReadSC.Read Then
            Cliente = campodb(MyReadSC.Item("MastroPartita")) & "." & campodb(MyReadSC.Item("ContoPartita")) & "." & campodb(MyReadSC.Item("SottocontoPartita"))
            If Mid(Tipo & Space(10), 1, 1) = " " Then
                CodiceOspite = Int(campodbN(MyReadSC.Item("SottocontoPartita")) / 100)
                CodiceParente = campodbN(MyReadSC.Item("SottocontoPartita")) - (CodiceOspite * 100)

                If CodiceParente = 0 And CodiceOspite > 0 Then
                    Tipo = "O" & Space(10)
                End If
                If CodiceParente > 0 And CodiceOspite > 0 Then
                    Tipo = "P" & Space(10)
                End If
            End If
            If Mid(Tipo & Space(10), 1, 1) = "C" Then
                CodiceProvincia = Mid(Tipo & Space(10), 2, 3)
                CodiceComune = Mid(Tipo & Space(10), 5, 3)
            End If
            If Mid(Tipo & Space(10), 1, 1) = "J" Then
                CodiceProvincia = Mid(Tipo & Space(10), 2, 3)
                CodiceComune = Mid(Tipo & Space(10), 5, 3)
            End If
            If Mid(Tipo & Space(10), 1, 1) = "R" Then
                CodiceRegione = Mid(Tipo & Space(10), 2, 4)
            End If

        End If
        MyReadSC.Close()
        cn.Close()

        Dim Servizio As New Cls_CentroServizio

        Servizio.CENTROSERVIZIO = Registrazione.CentroServizio
        Servizio.Leggi(Session("DC_OSPITE"), Servizio.CENTROSERVIZIO)


        Dati.AddDescrizione = ""
        If Mid(Tipo & Space(10), 1, 1) = "O" Then
            Dim Ospite As New ClsOspite

            Ospite.CodiceOspite = CodiceOspite
            Ospite.Leggi(Session("DC_OSPITE"), Ospite.CodiceOspite)

            Dati.CODICEEXPORT = Ospite.CodiceCup
            Dati.CodiceOspite = Ospite.CodiceOspite
            If Dati.CODICEEXPORT = "" Then
                Dim CodiceConto As Long

                CodiceConto = (Servizio.CONTO * 1000) + (Ospite.CodiceOspite * 10)
                Dati.CODICEEXPORT = "C" & CodiceConto
            End If
            Dati.DO11_RAGIONESOCIALE = Mid(Ospite.CognomeOspite & Space(30), 1, 30) & Ospite.NomeOspite
            Dati.DO11_PIVA = ""
            Dati.DO11_CF = Ospite.CODICEFISCALE
            Dati.DO11_INDIRIZZO = Ospite.RESIDENZAINDIRIZZO1
            Dati.DO11_CAP = Ospite.RESIDENZACAP1

            If Ospite.Opposizione730 = 1 Then
                Dati.DO11_OPPOSIZIONE = "S"
            Else
                Dati.DO11_OPPOSIZIONE = "N"
            End If
            Dim DcCom As New ClsComune

            DcCom.Provincia = Ospite.RESIDENZAPROVINCIA1
            DcCom.Comune = Ospite.RESIDENZACOMUNE1
            DcCom.Leggi(Session("DC_OSPITE"))

            Dati.DO11_CITTA_NOME = DcCom.Descrizione
            Dati.DO11_CITTA_ISTAT = DcCom.Provincia & DcCom.Comune

            Dim DcProv As New ClsComune

            DcProv.Provincia = Ospite.RESIDENZAPROVINCIA1
            DcProv.Comune = ""
            DcProv.Leggi(Session("DC_OSPITE"))
            Dati.DO11_PROVINCIA = DcProv.CodificaProvincia


            Dim Kl As New Cls_DatiOspiteParenteCentroServizio

            Kl.CodiceOspite = CodiceOspite
            Kl.CodiceParente = 0
            Kl.CentroServizio = Registrazione.CentroServizio
            Kl.Leggi(Session("DC_OSPITE"))
            If Kl.ModalitaPagamento <> "" Then
                Ospite.MODALITAPAGAMENTO = Kl.ModalitaPagamento
            End If


            Dati.DO11_CONDPAG_CODICE_SEN = Ospite.MODALITAPAGAMENTO
            Dim MPAg As New ClsModalitaPagamento

            MPAg.Codice = Ospite.MODALITAPAGAMENTO
            MPAg.Leggi(Session("DC_OSPITE"))
            Dati.DO11_CONDPAG_NOME_SEN = MPAg.DESCRIZIONE


            Dim ModPag As New Cls_DatiPagamento

            ModPag.CodiceOspite = Ospite.CodiceOspite
            ModPag.CodiceParente = 0
            ModPag.LeggiUltimaDataData(Session("DC_OSPITE"), Registrazione.DataRegistrazione)

            Dati.DO11_BANCA_CIN = ModPag.Cin
            Dati.DO11_BANCA_ABI = ModPag.Abi
            Dati.DO11_BANCA_CAB = ModPag.Cab
            Dati.DO11_BANCA_CONTO = ModPag.CCBancario
            Dati.DO11_BANCA_IBAN = ModPag.CodiceInt & Format(Val(ModPag.NumeroControllo), "00") & ModPag.Cin & Format(Val(ModPag.Abi), "00000") & Format(Val(ModPag.Cab), "00000") & AdattaLunghezzaNumero(ModPag.CCBancario, 12)

            Dati.DO11_BANCA_IDMANDATO = ModPag.IdMandatoDebitore
            Dati.DO11_BANCA_DATAMANDATO = ModPag.DataMandatoDebitore

            If Ospite.RecapitoNome <> "" Then
                Dati.DES_RAGIONESOCIALE = Ospite.RecapitoNome

                Dati.DES_INDIRIZZO = Ospite.RecapitoIndirizzo
                Dati.DES_CAP = Ospite.RESIDENZACAP4

                Dim DestCom As New ClsComune

                DestCom.Provincia = Ospite.RecapitoProvincia
                DestCom.Comune = Ospite.RecapitoComune
                DestCom.Leggi(Session("DC_OSPITE"))

                Dati.DES_CITTA_NOME = DestCom.Descrizione
                Dati.DES_CITTA_ISTAT = DestCom.Provincia & DcCom.Comune

                Dim DestProv As New ClsComune

                DestProv.Provincia = Ospite.RecapitoProvincia
                DestProv.Comune = ""
                DestProv.Leggi(Session("DC_OSPITE"))
                Dati.DES_PROVINCIA = DestProv.CodificaProvincia

            End If

        End If
        If Mid(Tipo & Space(10), 1, 1) = "P" Then
            Dim Ospite As New Cls_Parenti

            Ospite.CodiceOspite = CodiceOspite
            Ospite.CodiceParente = CodiceParente
            Ospite.Leggi(Session("DC_OSPITE"), Ospite.CodiceOspite, Ospite.CodiceParente)

            Dati.CodiceOspite = Ospite.CodiceOspite
            Dati.CODICEEXPORT = Ospite.CodiceCup
            If Dati.CODICEEXPORT = "" Then
                Dim CodiceConto As Long

                CodiceConto = (Servizio.CONTO * 1000) + (Ospite.CodiceOspite * 10) + Ospite.CodiceParente
                Dati.CODICEEXPORT = "C" & CodiceConto
            End If

            Dati.DO11_RAGIONESOCIALE = Mid(Ospite.CognomeParente & Space(30), 1, 30) & Ospite.NomeParente
            Dati.DO11_PIVA = ""
            Dati.DO11_CF = Ospite.CODICEFISCALE
            Dati.DO11_INDIRIZZO = Ospite.RESIDENZAINDIRIZZO1
            Dati.DO11_CAP = Ospite.RESIDENZACAP1


            If Ospite.Opposizione730 = 1 Then
                Dati.DO11_OPPOSIZIONE = "S"
            Else
                Dati.DO11_OPPOSIZIONE = "N"
            End If
            Dim DcCom As New ClsComune

            DcCom.Provincia = Ospite.RESIDENZAPROVINCIA1
            DcCom.Comune = Ospite.RESIDENZACOMUNE1
            DcCom.Leggi(Session("DC_OSPITE"))

            Dati.DO11_CITTA_NOME = DcCom.Descrizione
            Dati.DO11_CITTA_ISTAT = DcCom.Provincia & DcCom.Comune

            Dim DcProv As New ClsComune

            DcProv.Provincia = Ospite.RESIDENZAPROVINCIA1
            DcProv.Comune = ""
            DcProv.Leggi(Session("DC_OSPITE"))
            Dati.DO11_PROVINCIA = DcProv.CodificaProvincia
            Dati.DO11_CONDPAG_CODICE_SEN = Ospite.MODALITAPAGAMENTO

            Dim Kl As New Cls_DatiOspiteParenteCentroServizio

            Kl.CodiceOspite = CodiceOspite
            Kl.CodiceParente = CodiceParente
            Kl.CentroServizio = Registrazione.CentroServizio
            Kl.Leggi(Session("DC_OSPITE"))
            If Kl.ModalitaPagamento <> "" Then
                Ospite.MODALITAPAGAMENTO = Kl.ModalitaPagamento
            End If

            Dim MPAg As New ClsModalitaPagamento

            MPAg.Codice = Ospite.MODALITAPAGAMENTO
            MPAg.Leggi(Session("DC_OSPITE"))
            Dati.DO11_CONDPAG_NOME_SEN = MPAg.DESCRIZIONE

            Dim ModPag As New Cls_DatiPagamento

            ModPag.CodiceOspite = Ospite.CodiceOspite
            ModPag.CodiceParente = Ospite.CodiceParente
            ModPag.LeggiUltimaDataData(Session("DC_OSPITE"), Registrazione.DataRegistrazione)

            Dati.DO11_BANCA_CIN = ModPag.Cin
            Dati.DO11_BANCA_ABI = ModPag.Abi
            Dati.DO11_BANCA_CAB = ModPag.Cab
            Dati.DO11_BANCA_CONTO = ModPag.CCBancario
            Dati.DO11_BANCA_IBAN = ModPag.CodiceInt & Format(Val(ModPag.NumeroControllo), "00") & ModPag.Cin & Format(Val(ModPag.Abi), "00000") & Format(Val(ModPag.Cab), "00000") & AdattaLunghezzaNumero(ModPag.CCBancario, 12)

            Dati.DO11_BANCA_IDMANDATO = ModPag.IdMandatoDebitore
            Dati.DO11_BANCA_DATAMANDATO = ModPag.DataMandatoDebitore
        End If
        If Mid(Tipo & Space(10), 1, 1) = "C" Then
            Dim Ospite As New ClsComune

            Ospite.Provincia = CodiceProvincia
            Ospite.Comune = CodiceComune
            Ospite.Leggi(Session("DC_OSPITE"))

            Dati.CODICEEXPORT = Ospite.CONTOPERESATTO

            If Dati.CODICEEXPORT = "" Then
                Dim CodiceConto As Long

                CodiceConto = Ospite.Comune
                Dati.CODICEEXPORT = "CC" & CodiceConto
            End If


            Dati.DO11_RAGIONESOCIALE = Ospite.Descrizione
            Dati.DO11_PIVA = Ospite.PartitaIVA
            Dati.DO11_CF = Ospite.CodiceFiscale
            Dati.DO11_INDIRIZZO = Ospite.RESIDENZAINDIRIZZO1
            Dati.DO11_CAP = Ospite.RESIDENZACAP1
            Dim DcCom As New ClsComune

            DcCom.Provincia = Ospite.RESIDENZAPROVINCIA1
            DcCom.Comune = Ospite.RESIDENZACOMUNE1
            DcCom.Leggi(Session("DC_OSPITE"))

            Dati.DO11_CITTA_NOME = DcCom.Descrizione
            Dati.DO11_CITTA_ISTAT = DcCom.Provincia & DcCom.Comune

            Dim DcProv As New ClsComune

            DcProv.Provincia = Ospite.RESIDENZAPROVINCIA1
            DcProv.Comune = ""
            DcProv.Leggi(Session("DC_OSPITE"))
            Dati.DO11_PROVINCIA = DcProv.CodificaProvincia
            Dati.DO11_CONDPAG_CODICE_SEN = Ospite.ModalitaPagamento
            Dim MPAg As New ClsModalitaPagamento

            MPAg.Codice = Ospite.ModalitaPagamento
            MPAg.Leggi(Session("DC_OSPITE"))
            Dati.DO11_CONDPAG_NOME_SEN = MPAg.DESCRIZIONE

            Dim cnOspiti As OleDbConnection

            cnOspiti = New Data.OleDb.OleDbConnection(Session("DC_OSPITE"))

            cnOspiti.Open()

            Dim cmdC As New OleDbCommand()
            cmdC.CommandText = ("select * from NoteFatture where CodiceProvincia = ? And CodiceComune = ?")
            cmdC.Parameters.AddWithValue("@CodiceProvincia", CodiceProvincia)
            cmdC.Parameters.AddWithValue("@CodiceComune", CodiceComune)
            cmdC.Connection = cnOspiti
            Dim RdCom As OleDbDataReader = cmdC.ExecuteReader()
            If RdCom.Read Then
                Dati.AddDescrizione = campodb(RdCom.Item("NoteUp"))
            End If
            RdCom.Close()
            cnOspiti.Close()
        End If
        If Mid(Tipo & Space(10), 1, 1) = "J" Then
            Dim Ospite As New ClsComune

            Ospite.Provincia = CodiceProvincia
            Ospite.Comune = CodiceComune
            Ospite.Leggi(Session("DC_OSPITE"))


            Dati.CODICEEXPORT = Ospite.CONTOPERESATTO

            If Dati.CODICEEXPORT = "" Then
                Dim CodiceConto As Long

                CodiceConto = Ospite.Comune
                Dati.CODICEEXPORT = "CJ" & CodiceConto
            End If
            Dati.DO11_RAGIONESOCIALE = Ospite.Descrizione
            Dati.DO11_PIVA = Ospite.PartitaIVA
            Dati.DO11_CF = Ospite.CodiceFiscale
            Dati.DO11_INDIRIZZO = Ospite.RESIDENZAINDIRIZZO1
            Dati.DO11_CAP = Ospite.RESIDENZACAP1
            Dim DcCom As New ClsComune

            DcCom.Provincia = Ospite.RESIDENZAPROVINCIA1
            DcCom.Comune = Ospite.RESIDENZACOMUNE1
            DcCom.Leggi(Session("DC_OSPITE"))

            Dati.DO11_CITTA_NOME = DcCom.Descrizione
            Dati.DO11_CITTA_ISTAT = DcCom.Provincia & DcCom.Comune

            Dim DcProv As New ClsComune

            DcProv.Provincia = Ospite.RESIDENZAPROVINCIA1
            DcProv.Comune = ""
            DcProv.Leggi(Session("DC_OSPITE"))
            Dati.DO11_PROVINCIA = DcProv.CodificaProvincia
            Dati.DO11_CONDPAG_CODICE_SEN = Ospite.ModalitaPagamento
            Dim MPAg As New ClsModalitaPagamento

            MPAg.Codice = Ospite.ModalitaPagamento
            MPAg.Leggi(Session("DC_OSPITE"))
            Dati.DO11_CONDPAG_NOME_SEN = MPAg.DESCRIZIONE

            Dim cnOspiti As OleDbConnection

            cnOspiti = New Data.OleDb.OleDbConnection(Session("DC_OSPITE"))

            cnOspiti.Open()

            Dim cmdC As New OleDbCommand()
            cmdC.CommandText = ("select * from NoteFatture where CodiceProvincia = ? And CodiceComune = ?")
            cmdC.Parameters.AddWithValue("@CodiceProvincia", CodiceProvincia)
            cmdC.Parameters.AddWithValue("@CodiceComune", CodiceComune)
            cmdC.Connection = cnOspiti
            Dim RdCom As OleDbDataReader = cmdC.ExecuteReader()
            If RdCom.Read Then
                Dati.AddDescrizione = campodb(RdCom.Item("NoteUp"))
            End If
            RdCom.Close()
            cnOspiti.Close()
        End If
        If Mid(Tipo & Space(10), 1, 1) = "R" Then
            Dim Ospite As New ClsUSL

            Ospite.CodiceRegione = CodiceRegione
            Ospite.Leggi(Session("DC_OSPITE"))

            Dati.CODICEEXPORT = Ospite.CONTOPERESATTO


            If Dati.CODICEEXPORT = "" Then
                Dim CodiceConto As Long

                CodiceConto = Ospite.CodiceRegione
                Dati.CODICEEXPORT = "CR" & CodiceConto
            End If

            Dati.DO11_RAGIONESOCIALE = Ospite.Nome
            Dati.DO11_PIVA = Ospite.PARTITAIVA
            Dati.DO11_CF = Ospite.CodiceFiscale
            Dati.DO11_INDIRIZZO = Ospite.RESIDENZAINDIRIZZO1
            Dati.DO11_CAP = Ospite.RESIDENZACAP1
            Dim DcCom As New ClsComune

            DcCom.Provincia = Ospite.RESIDENZAPROVINCIA1
            DcCom.Comune = Ospite.RESIDENZACOMUNE1
            DcCom.Leggi(Session("DC_OSPITE"))

            Dati.DO11_CITTA_NOME = DcCom.Descrizione
            Dati.DO11_CITTA_ISTAT = DcCom.Provincia & DcCom.Comune

            Dim DcProv As New ClsComune

            DcProv.Provincia = Ospite.RESIDENZAPROVINCIA1
            DcProv.Comune = ""
            DcProv.Leggi(Session("DC_OSPITE"))
            Dati.DO11_PROVINCIA = DcProv.CodificaProvincia
            Dati.DO11_CONDPAG_CODICE_SEN = Ospite.ModalitaPagamento
            Dim MPAg As New ClsModalitaPagamento

            MPAg.Codice = Ospite.ModalitaPagamento
            MPAg.Leggi(Session("DC_OSPITE"))
            Dati.DO11_CONDPAG_NOME_SEN = MPAg.DESCRIZIONE
        End If

        cn.Close()
    End Sub

    Function Export_DocumentiIncassi() As Boolean
        Dim cn As OleDbConnection
        Session("MYDATE") = Now


        Dim TwMOVIM As System.IO.TextWriter = System.IO.File.CreateText(HostingEnvironment.ApplicationPhysicalPath() & "\Public\movim_" & Format(Session("MYDATE"), "yyyyMMddHHmmss") & ".Txt")
        Dim Twmovpart As System.IO.TextWriter = System.IO.File.CreateText(HostingEnvironment.ApplicationPhysicalPath() & "\Public\movpart_" & Format(Session("MYDATE"), "yyyyMMddHHmmss") & ".Txt")

        Dim TwIVAMOV As System.IO.TextWriter = System.IO.File.CreateText(HostingEnvironment.ApplicationPhysicalPath() & "\Public\ivamov_" & Format(Session("MYDATE"), "yyyyMMddHHmmss") & ".Txt")
        Dim TwCLISISP As System.IO.TextWriter = System.IO.File.CreateText(HostingEnvironment.ApplicationPhysicalPath() & "\Public\clisisp_" & Format(Session("MYDATE"), "yyyyMMddHHmmss") & ".Txt")

        Dim TwCSV As System.IO.TextWriter = System.IO.File.CreateText(HostingEnvironment.ApplicationPhysicalPath() & "\Public\CSV_" & Format(Session("MYDATE"), "yyyyMMddHHmmss") & ".Txt")

        cn = New Data.OleDb.OleDbConnection(Session("DC_GENERALE"))
        cn.Open()

        Dim Param As New Cls_DatiGenerali



        Param.LeggiDati(Session("DC_TABELLE"))

        Tabella = ViewState("App_AddebitiMultiplo")

        Dim NumeroRegistrazioni As Integer = 0
        Dim RigaEstratta As Integer = 0


        For RigaEstratta = 0 To Tabella.Rows.Count - 1
            Dim Chekc As CheckBox = DirectCast(GridView1.Rows(RigaEstratta).FindControl("ChkImporta"), CheckBox)

            If Chekc.Checked = True Then
                NumeroRegistrazioni = NumeroRegistrazioni + 1
            End If
        Next

        If NumeroRegistrazioni = 0 Then
            ScriptManager.RegisterClientScriptBlock(Me, Me.GetType(), "Stampa2", "alert('Attenzione non hai segnalato nessuna registrazione');", True)
            Exit Function
        End If

        NumeroRegistrazioni = 0

        Export_DocumentiIncassi = True

        For RigaEstratta = 0 To Tabella.Rows.Count - 1
            Dim Chekc As CheckBox = DirectCast(GridView1.Rows(RigaEstratta).FindControl("ChkImporta"), CheckBox)

            If Chekc.Checked = True Then
                Dim Registrazione As New Cls_MovimentoContabile
                Dim CausaleContabile As New Cls_CausaleContabile

                Registrazione.NumeroRegistrazione = campodbN(Tabella.Rows(RigaEstratta).Item(0))
                Registrazione.Leggi(Session("DC_GENERALE"), Registrazione.NumeroRegistrazione)

                CausaleContabile.Leggi(Session("DC_TABELLE"), Registrazione.CausaleContabile)

                Dim Anagrafiche As New Anagrafica


                DecodificaAnagrafica(Anagrafiche, Registrazione)

                If Chk_Prova.Checked = False Then
                    Dim cmdEff As New OleDbCommand()

                    cmdEff.CommandText = "UPDATE movimenticontabiliriga SET RigaBollato = 1 where Numero = ?"
                    cmdEff.Parameters.AddWithValue("@Numero", Registrazione.NumeroRegistrazione)
                    cmdEff.Connection = cn
                    cmdEff.ExecuteNonQuery()


                    Dim cmdEffT As New OleDbCommand()

                    cmdEffT.CommandText = "UPDATE movimenticontabiliTesta SET ExportDATA = GETDATE() where NumeroRegistrazione = ?"
                    cmdEffT.Parameters.AddWithValue("@Numero", Registrazione.NumeroRegistrazione)
                    cmdEffT.Connection = cn
                    cmdEffT.ExecuteNonQuery()
                End If

                If CausaleContabile.TipoDocumento = "FA" Or CausaleContabile.TipoDocumento = "RE" Or CausaleContabile.TipoDocumento = "NC" Then
                    Call FattureNC(Anagrafiche, Registrazione, CausaleContabile, TwMOVIM, TwIVAMOV, TwCLISISP)

                Else
                    Call CreaIncasso(Anagrafiche, Registrazione, CausaleContabile, Twmovpart)
                End If

                Dim Riga As String

                Riga = Registrazione.NumeroRegistrazione & ";" & Registrazione.DataRegistrazione & ";" & Registrazione.DataDocumento & ";" & Registrazione.NumeroDocumento & ";" & Registrazione.ImportoRegistrazioneDocumento(Session("DC_TABELLE"))

                TwCSV.WriteLine(Riga)
            End If
        Next
        cn.Close()


        TwCLISISP.Close()
        TwIVAMOV.Close()
        TwMOVIM.Close()
        TwCSV.Close()
        Twmovpart.Close()




        If Chk_Prova.Checked = False Then
            Dim IndiceTab As Integer


            For IndiceTab = 0 To GridView1.Rows.Count - 1

                Dim CheckBox As CheckBox = DirectCast(GridView1.Rows(IndiceTab).FindControl("ChkImporta"), CheckBox)

                CheckBox.Enabled = False
                CheckBox.Checked = False
            Next

            Btn_Export.Visible = False
        End If



        TabContainer1.ActiveTabIndex = 1
    End Function

    Private Sub CreaIncasso(ByRef Dati As Anagrafica, ByVal Reg As Cls_MovimentoContabile, ByRef CausaleContabile As Cls_CausaleContabile, ByVal FileMovPart As System.IO.TextWriter)
        Dim RigaMOVPART As New MOVPART
        Dim CentroServizio As New Cls_CentroServizio
        Dim I As Integer

        CentroServizio.CENTROSERVIZIO = Reg.CentroServizio
        CentroServizio.Leggi(Session("DC_OSPITE"), CentroServizio.CENTROSERVIZIO)

        Dim Legami As New Cls_Legami
        Dim TabTrascodifica As New Cls_TabellaTrascodificheEsportazioni

        Legami.Leggi(Session("DC_GENERALE"), 0, Reg.NumeroRegistrazione)



        Dim DatiSocieta As New Cls_TabellaSocieta

        DatiSocieta.Leggi(Session("DC_TABELLE"))

        For i = 0 To 100

            If Legami.Importo(i) > 0 Then
                Dim Documento As New Cls_MovimentoContabile

                Documento.NumeroRegistrazione = Legami.NumeroDocumento(I)
                Documento.Leggi(Session("DC_GENERALE"), Documento.NumeroRegistrazione)


                RigaMOVPART.PartitaIVA = DatiSocieta.PartitaIVA
                RigaMOVPART.Codicefiscale = DatiSocieta.CodiceFiscale
                RigaMOVPART.Ragionesociale = DatiSocieta.RagioneSociale
                RigaMOVPART.Argomento = "4"
                RigaMOVPART.Numeropartita = Legami.NumeroDocumento(I)
                RigaMOVPART.Numeropartita = Legami.NumeroPagamento(I)
                RigaMOVPART.Dataoperazione = Format(Reg.DataRegistrazione, "yyyyMMdd")
                RigaMOVPART.Codiceconto = "162001" ' ?                
                RigaMOVPART.CodiceCliFor = Dati.CODICEEXPORT
                RigaMOVPART.TipoRegistro = "03"


                TabTrascodifica.TIPOTAB = "RI"
                TabTrascodifica.SENIOR = Reg.RegistroIVA
                TabTrascodifica.EXPORT = 0
                TabTrascodifica.Leggi(Session("DC_TABELLE"))


                RigaMOVPART.CodiceRegistro = TabTrascodifica.EXPORT
                RigaMOVPART.AnnoDocumento = (Year(Documento.DataRegistrazione) - 2000) & (Year(Documento.DataRegistrazione) - 2000)
                RigaMOVPART.Numeroprotocollo = Documento.NumeroProtocollo
                RigaMOVPART.DataDocumento = Format(Documento.DataRegistrazione, "yyyyMMdd")


                RigaMOVPART.Annopagamento = (Year(Reg.DataRegistrazione) - 2000) & (Year(Reg.DataRegistrazione) - 2000)

                RigaMOVPART.Tipomovimento = "V"


                TabTrascodifica.TIPOTAB = "CT"
                TabTrascodifica.SENIOR = Reg.CausaleContabile
                TabTrascodifica.EXPORT = ""
                TabTrascodifica.Leggi(Session("DC_TABELLE"))

                RigaMOVPART.Codicecausale = TabTrascodifica.EXPORT


                TabTrascodifica.TIPOTAB = "PR"
                TabTrascodifica.SENIOR = Reg.CausaleContabile
                TabTrascodifica.EXPORT = ""
                TabTrascodifica.Leggi(Session("DC_TABELLE"))

                RigaMOVPART.CodicePagamento = TabTrascodifica.EXPORT



                RigaMOVPART.Datascadenza = Format(Documento.DataRegistrazione, "yyyyMMdd")
                RigaMOVPART.Importodelmovimento = Format(Documento.ImportoDocumento(Session("DC_TABELLE")) * 100, "000000000000")
                RigaMOVPART.Segnoimporto = "P"
                RigaMOVPART.CodiceAgente = ""
                RigaMOVPART.CodiceValutaEstera = "EUR"
                RigaMOVPART.Cambio = "1"



                TabTrascodifica.TIPOTAB = "CI"
                TabTrascodifica.SENIOR = Reg.CausaleContabile
                TabTrascodifica.EXPORT = ""
                TabTrascodifica.Leggi(Session("DC_TABELLE"))
                
                RigaMOVPART.Tipopagamento = TabTrascodifica.EXPORT



                If Mid(Reg.Tipologia & Space(10), 1, 1) = "C" Or Mid(Reg.Tipologia & Space(10), 1, 1) = "J" Or Mid(Reg.Tipologia & Space(10), 1, 1) = "R" Then
                    RigaMOVPART.TipoAnagrafica = "1"
                Else
                    RigaMOVPART.TipoAnagrafica = "2"
                End If
                RigaMOVPART.TipoValuta = "E"
                RigaMOVPART.IVAperCassa = ""
                ScriviMOVPART(RigaMOVPART, FileMovPart)
            End If
        Next



    End Sub

    Private Sub FattureNC(ByRef Dati As Anagrafica, ByVal Reg As Cls_MovimentoContabile, ByRef CausaleContabile As Cls_CausaleContabile, ByVal FileMOVIM As System.IO.TextWriter, ByVal FileIVAMOV As System.IO.TextWriter, ByVal FileCLISISP As System.IO.TextWriter)
        Dim RigaMOVIM As New MOVIM
        Dim RigaIVAMOV As New IVAMOV
        Dim RigaCLISISP As New CLISISP
        Dim ContoCliente As String = "162001"


        Dim TabTrascodifica As New Cls_TabellaTrascodificheEsportazioni


        Dim CentroServizio As New Cls_CentroServizio

        CentroServizio.CENTROSERVIZIO = Reg.CentroServizio
        CentroServizio.Leggi(Session("DC_OSPITE"), CentroServizio.CENTROSERVIZIO)

        Dim DatiSocieta As New Cls_TabellaSocieta

        DatiSocieta.Leggi(Session("DC_TABELLE"))

        RigaCLISISP.CodiceAnagrafico = Dati.CODICEEXPORT
        RigaCLISISP.PartitaIVA = Dati.DES_PIVA
        RigaCLISISP.Codicefiscale = Dati.DO11_CF
        If Mid(RigaMOVIM.TipoAnagrafica & Space(10), 1, 1) = "C" Or Mid(RigaMOVIM.TipoAnagrafica & Space(10), 1, 1) = "J" Or Mid(RigaMOVIM.TipoAnagrafica & Space(10), 1, 1) = "R" Then
            RigaCLISISP.Tipoanagrafica = "A"
        Else
            RigaCLISISP.Tipoanagrafica = "F"
        End If

        RigaCLISISP.Ragionesociale = Dati.DO11_RAGIONESOCIALE
        RigaCLISISP.Indirizzo = Dati.DO11_INDIRIZZO
        RigaCLISISP.Numero = ""
        RigaCLISISP.Comune = Dati.DO11_CITTA_NOME
        RigaCLISISP.cap = Dati.DO11_CAP
        RigaCLISISP.Statoestero = ""
        RigaCLISISP.Telefono = ""
        RigaCLISISP.Telefax = ""
        RigaCLISISP.Telex = ""
        RigaCLISISP.Prefisso = ""
        RigaCLISISP.Frazione = ""
        RigaCLISISP.Provincia = ""
        RigaCLISISP.Testaziendamunicipalizzata = ""
        RigaCLISISP.Codicecomune = ""
        RigaCLISISP.Codicefrazione = ""

        Dim Indice As Integer
        Dim TrovatoCliente As Boolean = False

        For Indice = 0 To 1000
            If Vt_Cliente(Indice) = RigaCLISISP.PartitaIVA & RigaCLISISP.Codicefiscale & Dati.CodiceOspite Then
                TrovatoCliente = True
                Exit For
            End If
            If Vt_Cliente(Indice) = "" Then
                TrovatoCliente = False
                Exit For
            End If
        Next

        If TrovatoCliente = False And Indice < 1000 Then
            Vt_Cliente(Indice) = RigaCLISISP.PartitaIVA & RigaCLISISP.Codicefiscale & Dati.CodiceOspite
        End If
        If TrovatoCliente = False Then
            ScriviCLISISP(RigaCLISISP, FileCLISISP)
        End If


        Dim RigaInterna As Integer = 0
        Dim RigaRicavo As Integer

        For RigaRicavo = 0 To 100
            If Not IsNothing(Reg.Righe(RigaRicavo)) Then
                If Reg.Righe(RigaRicavo).RigaDaCausale = 1 Or Reg.Righe(RigaRicavo).RigaDaCausale = 3 Or Reg.Righe(RigaRicavo).RigaDaCausale = 4 Or Reg.Righe(RigaRicavo).RigaDaCausale = 5 Or Reg.Righe(RigaRicavo).RigaDaCausale = 6 Then
                    RigaInterna = RigaInterna + 1
                    RigaMOVIM.Codicefiscale = DatiSocieta.CodiceFiscale
                    RigaMOVIM.PartitaIVA = DatiSocieta.PartitaIVA
                    RigaMOVIM.Ragionesociale = DatiSocieta.RagioneSociale

                    RigaMOVIM.EsercizioIVA = Reg.AnnoProtocollo
                    RigaMOVIM.EsercizioCOGE = Mid(Reg.AnnoProtocollo, 3, 2) & Mid(Reg.AnnoProtocollo, 3, 2)
                    RigaMOVIM.Argomento = "V"
                    RigaMOVIM.Competenza = "N"
                    RigaMOVIM.Numeropartita = Reg.NumeroRegistrazione
                    RigaMOVIM.Numerointerno = Format(RigaInterna, "000")

                    RigaMOVIM.Dataoperazione = Format(Reg.DataRegistrazione, "yyMMdd")


                    TabTrascodifica.TIPOTAB = "CC"
                    TabTrascodifica.SENIOR = Reg.Righe(RigaRicavo).MastroPartita & "." & Reg.Righe(RigaRicavo).ContoPartita & "." & Reg.Righe(RigaRicavo).SottocontoPartita
                    TabTrascodifica.EXPORT = 0
                    TabTrascodifica.Leggi(Session("DC_TABELLE"))

                    If Reg.Righe(RigaRicavo).RigaDaCausale = 1 Then
                        RigaMOVIM.Codiceconto = ContoCliente & Dati.CODICEEXPORT
                    Else
                        RigaMOVIM.Codiceconto = TabTrascodifica.EXPORT
                    End If

                    RigaMOVIM.Tiporegistro = "03"


                    TabTrascodifica.TIPOTAB = "RI"
                    TabTrascodifica.SENIOR = Reg.RegistroIVA
                    TabTrascodifica.EXPORT = 0
                    TabTrascodifica.Leggi(Session("DC_TABELLE"))



                    RigaMOVIM.Codiceregistro = TabTrascodifica.EXPORT

                    RigaMOVIM.Numeroprotocollo = Reg.NumeroProtocollo

                    If Reg.Righe(RigaRicavo).DareAvere = "D" Then
                        RigaMOVIM.Tipoarticolo = "1"
                    Else
                        RigaMOVIM.Tipoarticolo = "2"
                    End If


                    RigaMOVIM.Tipomovimento = "R"
                    RigaMOVIM.Codicecosto = "000"


                    TabTrascodifica.TIPOTAB = "CS"
                    TabTrascodifica.SENIOR = Reg.CentroServizio
                    TabTrascodifica.EXPORT = ""
                    TabTrascodifica.Leggi(Session("DC_TABELLE"))
                    If TabTrascodifica.EXPORT <> "" Then
                        RigaMOVIM.Codicecosto = TabTrascodifica.EXPORT
                    End If


                    If CausaleContabile.TipoDocumento = "NC" Then
                        RigaMOVIM.Codicecausale = "01200"
                    Else
                        RigaMOVIM.Codicecausale = "00200"
                    End If


                    RigaMOVIM.NumDescrizcausale = 0
                    RigaMOVIM.Segnoimporto = "P"

                    RigaMOVIM.Importo = Format(Reg.Righe(RigaRicavo).Importo * 100, "0000000000000")

                    RigaMOVIM.Segnocontabileimporto = Reg.Righe(RigaRicavo).DareAvere
                    RigaMOVIM.Annotazioni = ""
                    If Reg.RegistroIVA > 3 Then
                        RigaMOVIM.Annotazioni = "Scissione dei pagamenti"
                    End If
                    RigaMOVIM.Datadocumento = Format(Reg.DataDocumento, "yyMMdd")
                    RigaMOVIM.Numerodocumento = Reg.NumeroDocumento
                    If Reg.Righe(RigaRicavo).RigaDaCausale = 1 Then
                        If Mid(RigaMOVIM.TipoAnagrafica & Space(10), 1, 1) = "C" Or Mid(RigaMOVIM.TipoAnagrafica & Space(10), 1, 1) = "J" Or Mid(RigaMOVIM.TipoAnagrafica & Space(10), 1, 1) = "R" Then
                            RigaMOVIM.TipoAnagrafica = ""
                        Else
                            RigaMOVIM.TipoAnagrafica = ""
                        End If
                    Else
                        RigaMOVIM.TipoAnagrafica = ""
                    End If

                    RigaMOVIM.FlagStorico = ""
                    RigaMOVIM.Libero = ""

                    ScriviMOVIM(RigaMOVIM, FileMOVIM)

                End If
            End If
        Next


        For Riga = 0 To 100
            If Not IsNothing(Reg.Righe(Riga)) Then
                If Reg.Righe(Riga).Tipo = "IV" Then
                    RigaInterna = RigaInterna + 1
                    RigaMOVIM.Codicefiscale = DatiSocieta.CodiceFiscale
                    RigaMOVIM.PartitaIVA = DatiSocieta.PartitaIVA
                    RigaMOVIM.Ragionesociale = DatiSocieta.RagioneSociale

                    RigaMOVIM.EsercizioIVA = Reg.AnnoProtocollo
                    RigaMOVIM.EsercizioCOGE = Mid(Reg.AnnoProtocollo, 3, 2) & Mid(Reg.AnnoProtocollo, 3, 2)
                    RigaMOVIM.Argomento = "V"
                    RigaMOVIM.Competenza = "N"
                    RigaMOVIM.Numeropartita = Reg.NumeroRegistrazione
                    RigaMOVIM.Numerointerno = Format(RigaInterna, "000")

                    RigaMOVIM.Dataoperazione = Format(Reg.DataRegistrazione, "yyMMdd")



                    RigaMOVIM.Codiceconto = "245007" 'conto iva


                    RigaMOVIM.Tiporegistro = "03"


                    TabTrascodifica.TIPOTAB = "RI"
                    TabTrascodifica.SENIOR = Reg.RegistroIVA
                    TabTrascodifica.EXPORT = 0
                    TabTrascodifica.Leggi(Session("DC_TABELLE"))



                    RigaMOVIM.Codiceregistro = TabTrascodifica.EXPORT

                    RigaMOVIM.Numeroprotocollo = Reg.NumeroProtocollo

                    If Reg.Righe(Riga).DareAvere = "D" Then
                        RigaMOVIM.Tipoarticolo = "1"
                    Else
                        RigaMOVIM.Tipoarticolo = "2"
                    End If


                    RigaMOVIM.Tipomovimento = "R"
                    RigaMOVIM.Codicecosto = "000"


                    TabTrascodifica.TIPOTAB = "CS"
                    TabTrascodifica.SENIOR = Reg.CentroServizio
                    TabTrascodifica.EXPORT = ""
                    TabTrascodifica.Leggi(Session("DC_TABELLE"))
                    If TabTrascodifica.EXPORT <> "" Then
                        RigaMOVIM.Codicecosto = TabTrascodifica.EXPORT
                    End If


                    If CausaleContabile.TipoDocumento = "NC" Then
                        RigaMOVIM.Codicecausale = "01200"
                    Else
                        RigaMOVIM.Codicecausale = "00200"
                    End If


                    RigaMOVIM.NumDescrizcausale = 0
                    RigaMOVIM.Segnoimporto = "P"

                    RigaMOVIM.Importo = Format(Reg.Righe(Riga).Importo * 100, "0000000000000")

                    RigaMOVIM.Segnocontabileimporto = Reg.Righe(Riga).DareAvere
                    RigaMOVIM.Annotazioni = ""
                    If Reg.RegistroIVA > 3 Then
                        RigaMOVIM.Annotazioni = "Scissione dei pagamenti"
                    End If
                    RigaMOVIM.Datadocumento = Format(Reg.DataDocumento, "yyMMdd")
                    RigaMOVIM.Numerodocumento = Reg.NumeroDocumento
                    If Reg.Righe(Riga).RigaDaCausale = 1 Then
                        If Mid(RigaMOVIM.TipoAnagrafica & Space(10), 1, 1) = "C" Or Mid(RigaMOVIM.TipoAnagrafica & Space(10), 1, 1) = "J" Or Mid(RigaMOVIM.TipoAnagrafica & Space(10), 1, 1) = "R" Then
                            RigaMOVIM.TipoAnagrafica = ""
                        Else
                            RigaMOVIM.TipoAnagrafica = ""
                        End If
                    Else
                        RigaMOVIM.TipoAnagrafica = ""
                    End If

                    RigaMOVIM.FlagStorico = ""
                    RigaMOVIM.Libero = ""

                    ScriviMOVIM(RigaMOVIM, FileMOVIM)



                    RigaIVAMOV.Codicefiscale = DatiSocieta.CodiceFiscale
                    RigaIVAMOV.PartitaIVA = DatiSocieta.PartitaIVA
                    RigaIVAMOV.Ragionesociale = DatiSocieta.RagioneSociale
                    RigaIVAMOV.Numeropartita = Reg.NumeroRegistrazione
                    RigaIVAMOV.Numerointerno = Riga

                    Dim SegnoRigaRegistrazione As String = ""
                    Dim SegnoCausaleContabile As String = ""

                    SegnoRigaRegistrazione = Reg.Righe(Riga).DareAvere
                    SegnoCausaleContabile = CausaleContabile.Righe(2).DareAvere

                    If CausaleContabile.TipoDocumento = "NC" And Reg.Tipologia.Trim <> "" Then
                        If SegnoCausaleContabile = "D" Then
                            SegnoCausaleContabile = "A"
                        Else
                            SegnoCausaleContabile = "D"
                        End If
                    End If

                    If SegnoCausaleContabile = SegnoRigaRegistrazione Then
                        RigaIVAMOV.Segnoimponibile = "P"
                        RigaIVAMOV.Imponibile = Math.Round(Reg.Righe(Riga).Imponibile * 100, 0)

                        RigaIVAMOV.SegnoImportoIVA = "P"
                        RigaIVAMOV.Imposta = Math.Round(Reg.Righe(Riga).Importo * 100, 0)
                    Else
                        RigaIVAMOV.Segnoimponibile = "N"
                        RigaIVAMOV.Imponibile = Math.Round(Reg.Righe(Riga).Imponibile * 100, 0)

                        RigaIVAMOV.SegnoImportoIVA = "N"
                        RigaIVAMOV.Imposta = Math.Round(Reg.Righe(Riga).Importo * 100, 0)
                    End If



                    If CausaleContabile.TipoDocumento = "NC" Then
                        RigaIVAMOV.Codicecausale = "01200"
                    Else
                        RigaIVAMOV.Codicecausale = "00200"
                    End If

                    TabTrascodifica.TIPOTAB = "IV"
                    TabTrascodifica.SENIOR = Reg.Righe(Riga).CodiceIVA
                    TabTrascodifica.EXPORT = 0
                    TabTrascodifica.Leggi(Session("DC_TABELLE"))
                    RigaIVAMOV.CodiceIVA = TabTrascodifica.EXPORT

                    RigaIVAMOV.Filler = "00"
                    RigaIVAMOV.Mercidestinateallarivendita = "M"
                    RigaIVAMOV.Quadroa = ""
                    RigaIVAMOV.Codicecostiricavi = ""
                    RigaIVAMOV.Flagesercizio = ""
                    RigaIVAMOV.Percentualedetraibilita = "10000"
                    RigaIVAMOV.CodiceTabellaProdottiAgricoltori = ""

                    RigaIVAMOV.CodiceIVAcompensativoAgricoltori = ""
                    RigaIVAMOV.Datafatturasospeso = ""
                    RigaIVAMOV.Multipunto = ""
                    RigaIVAMOV.CausaleAnalitica = "00"
                    RigaIVAMOV.Codicesottoconto = ""
                    RigaIVAMOV.Libero = ""
                    ScriviIVAMOV(RigaIVAMOV, FileIVAMOV)
                    If Reg.RegistroIVA > 3 Then
                        RigaIVAMOV.Codicefiscale = DatiSocieta.CodiceFiscale
                        RigaIVAMOV.PartitaIVA = DatiSocieta.PartitaIVA
                        RigaIVAMOV.Ragionesociale = DatiSocieta.RagioneSociale
                        RigaIVAMOV.Numeropartita = Reg.NumeroRegistrazione
                        RigaIVAMOV.Numerointerno = Riga

                        If SegnoCausaleContabile = SegnoRigaRegistrazione Then
                            RigaIVAMOV.Segnoimponibile = "N"
                            RigaIVAMOV.Imponibile = Math.Round(Reg.Righe(Riga).Imponibile * 100, 0)

                            RigaIVAMOV.SegnoImportoIVA = "N"
                            RigaIVAMOV.Imposta = Math.Round(Reg.Righe(Riga).Importo * 100, 0)
                        Else
                            RigaIVAMOV.Segnoimponibile = "P"
                            RigaIVAMOV.Imponibile = Math.Round(Reg.Righe(Riga).Imponibile * 100, 0)

                            RigaIVAMOV.SegnoImportoIVA = "P"
                            RigaIVAMOV.Imposta = Math.Round(Reg.Righe(Riga).Importo * 100, 0)
                        End If


                        If CausaleContabile.TipoDocumento = "NC" Then
                            RigaIVAMOV.Codicecausale = "01200"
                        Else
                            RigaIVAMOV.Codicecausale = "00200"
                        End If

                        TabTrascodifica.TIPOTAB = "IV"
                        TabTrascodifica.SENIOR = Reg.Righe(Riga).CodiceIVA
                        TabTrascodifica.EXPORT = 0
                        TabTrascodifica.Leggi(Session("DC_TABELLE"))
                        RigaIVAMOV.CodiceIVA = TabTrascodifica.EXPORT

                        RigaIVAMOV.Filler = "00"
                        RigaIVAMOV.Mercidestinateallarivendita = "M"
                        RigaIVAMOV.Quadroa = ""
                        RigaIVAMOV.Codicecostiricavi = ""
                        RigaIVAMOV.Flagesercizio = ""
                        RigaIVAMOV.Percentualedetraibilita = "10000"
                        RigaIVAMOV.CodiceTabellaProdottiAgricoltori = ""

                        RigaIVAMOV.CodiceIVAcompensativoAgricoltori = ""
                        RigaIVAMOV.Datafatturasospeso = ""
                        RigaIVAMOV.Multipunto = ""
                        RigaIVAMOV.CausaleAnalitica = "00"
                        RigaIVAMOV.Codicesottoconto = ""
                        RigaIVAMOV.Libero = ""
                        ScriviIVAMOV(RigaIVAMOV, FileIVAMOV)
                    End If
                End If
            End If
        Next




    End Sub



    Private Sub Estrazione()
        Dim cn As OleDbConnection


        cn = New Data.OleDb.OleDbConnection(Session("DC_GENERALE"))
        cn.Open()
        Dim Param As New Cls_DatiGenerali


        Param.LeggiDati(Session("DC_TABELLE"))



        Tabella.Clear()
        Tabella.Columns.Clear()
        Tabella.Columns.Add("NumeroRegistrazione", GetType(String))
        Tabella.Columns.Add("DataRegistrazione", GetType(String))
        Tabella.Columns.Add("NumeroDocumento", GetType(String))
        Tabella.Columns.Add("Causale", GetType(String))
        Tabella.Columns.Add("Intestatario", GetType(String))
        Tabella.Columns.Add("Importo", GetType(String))




        Dim cmd As New OleDbCommand()
        '(select count(*) from MovimentiContabiliRiga Where Numero = MovimentiContabiliTesta.NumeroRegistrazione  And RigaBollato = 1) =  0 And

        If Param.CausaleRettifica = "" Then
            cmd.CommandText = "Select * From MovimentiContabiliTesta where CentroServizio = ? And DataRegistrazione >= ? And DataRegistrazione <= ? And ((Select COUNT(*) from movimenticontabiliriga where NUMERO = NUMEROREGISTRAZIONE AND  (RigaBollato = 0 or RigaBollato IS null)) > 0) And (RegistroIva =0 or RegistroIva =1  or RegistroIva =2  or RegistroIva is null) order by  (Select Top 1 CodiceDocumento  From TabellaLegami where CodicePagamento = NumeroRegistrazione   Order by CodiceDocumento),AnnoProtocollo,NumeroProtocollo,DataRegistrazione"
        Else
            cmd.CommandText = "Select * From MovimentiContabiliTesta where CentroServizio = ? And  CausaleContabile <> '" & Param.CausaleRettifica & "' And DataRegistrazione >= ? And DataRegistrazione <= ? And ((Select COUNT(*) from movimenticontabiliriga where NUMERO = NUMEROREGISTRAZIONE AND  (RigaBollato = 0 or RigaBollato IS null)) > 0) And (RegistroIva =0 or RegistroIva =1 or RegistroIva =2 or RegistroIva is null) order by  (Select Top 1 CodiceDocumento  From TabellaLegami where CodicePagamento = NumeroRegistrazione  Order by CodiceDocumento),AnnoProtocollo,NumeroProtocollo,DataRegistrazione"
        End If

        cmd.Parameters.AddWithValue("@CServ", DD_CServ.SelectedValue)
        cmd.Parameters.AddWithValue("@DataDal", Txt_DataDal.Text)
        cmd.Parameters.AddWithValue("@DataAl", Txt_DataAl.Text)
        cmd.Connection = cn
        Dim MyRsDB As OleDbDataReader = cmd.ExecuteReader()
        Do While MyRsDB.Read
            Dim myriga As System.Data.DataRow = Tabella.NewRow()

            Dim NReg As New Cls_MovimentoContabile

            NReg.NumeroRegistrazione = campodbN(MyRsDB.Item("NumeroRegistrazione"))
            NReg.Leggi(Session("DC_GENERALE"), NReg.NumeroRegistrazione)

            Dim Causale As New Cls_CausaleContabile

            Causale.Codice = NReg.CausaleContabile
            Causale.Leggi(Session("DC_TABELLE"), Causale.Codice)

            myriga(0) = NReg.NumeroRegistrazione
            myriga(1) = Format(NReg.DataRegistrazione, "dd/MM/yyyy")
            myriga(2) = NReg.NumeroDocumento
            myriga(3) = Causale.Descrizione

            Dim PInc As New Cls_Pianodeiconti

            PInc.Mastro = NReg.Righe(0).MastroPartita
            PInc.Conto = NReg.Righe(0).ContoPartita
            PInc.Sottoconto = NReg.Righe(0).SottocontoPartita
            PInc.Decodfica(Session("DC_GENERALE"))

            myriga(4) = PInc.Descrizione

            myriga(5) = Format(NReg.ImportoRegistrazioneDocumento(Session("DC_TABELLE")), "#,##0.00")



            Tabella.Rows.Add(myriga)
        Loop


        cn.Close()


        ViewState("App_AddebitiMultiplo") = Tabella

        GridView1.AutoGenerateColumns = False

        GridView1.DataSource = Tabella
        GridView1.DataBind()
        GridView1.Visible = True

        Dim ParamOsp As New Cls_Parametri


        ParamOsp.LeggiParametri(Session("DC_OSPITE"))

        If ParamOsp.TipoExport = "Documenti Incassi" And Chk_Prova.Checked = False Then
            Btn_Export.ImageUrl = "~/images/sendmail.png"
            Btn_Export.Visible = True
        Else
            Btn_Export.ImageUrl = "~/images/download.png"
            Btn_Export.Visible = True
        End If

    End Sub


    Function campodb(ByVal oggetto As Object) As String
        If IsDBNull(oggetto) Then
            Return ""
        Else
            Return oggetto
        End If
    End Function
    Function campodbN(ByVal oggetto As Object) As Double
        If IsDBNull(oggetto) Then
            Return 0
        Else
            Return oggetto
        End If
    End Function

    Function campodbd(ByVal oggetto As Object) As Date
        If IsDBNull(oggetto) Then
            Return Nothing
        Else
            Return oggetto
        End If
    End Function

    Protected Sub Btn_Estrai_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles Btn_Estrai.Click

        If DD_CServ.SelectedValue.Trim ="" Then
                ClientScript.RegisterClientScriptBlock(Me.GetType(), "Errore", "alert('Devi indicare il servizio');", True)
            Exit Sub
        End If

        Estrazione()
    End Sub

    Private Sub EseguiJS()
        Dim MyJs As String
        MyJs = "$(document).ready(function() {"

        MyJs = MyJs & "var els = document.getElementsByTagName(""*"");"

        MyJs = MyJs & "for (var i=0;i<els.length;i++)"
        MyJs = MyJs & "if ( els[i].id ) { "
        MyJs = MyJs & " var appoggio =els[i].id; "


        MyJs = MyJs & " if ((appoggio.match('Txt_Importo')!= null)) {  "
        MyJs = MyJs & " $(els[i]).keypress(function() { ForceNumericInput($(this).val(), true, true); } ); "
        MyJs = MyJs & " $(els[i]).blur(function() { var ap = formatNumber($(this).val(),2); $(this).val(ap); });"
        MyJs = MyJs & "    }"

        MyJs = MyJs & " if ((appoggio.match('Txt_Data')!= null) || (appoggio.match('Txt_Data')!= null)) {  "
        MyJs = MyJs & " $(els[i]).mask(""99/99/9999"");"
        MyJs = MyJs & " $(els[i]).datepicker({ changeMonth: true,changeYear: true,  yearRange: ""1990:" & Year(Now) + 5 & """},$.datepicker.regional[""it""]);"
        MyJs = MyJs & "    }"


        MyJs = MyJs & "} "



        MyJs = MyJs & "});"

        'MyJs = "$(document).ready(function() { $('.myClass').keypress(function() { handleEnter($('.myClass').val(), true, true); } );     $('#" & Txt_ImportoPagato.ClientID & "').blur(function() { var ap = formatNumber ($('#" & Txt_ImportoPagato.ClientID & "').val(),2); $('#" & Txt_ImportoPagato.ClientID & "').val(ap); }); });"
        ScriptManager.RegisterStartupScript(Me, Me.GetType(), "visualizzaRitIm", MyJs, True)
    End Sub
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If IsNothing(Session("DC_OSPITE")) Then
            Response.Redirect("..\Login.aspx")
            Exit Sub
        End If

        EseguiJS()

        If Page.IsPostBack = True Then
            Exit Sub
        End If

        Dim K1 As New Cls_SqlString

        Dim kVilla As New Cls_TabelleDescrittiveOspitiAccessori


        kVilla.UpDateDropBox(Session("DC_OSPITIACCESSORI"), "VIL", DD_Struttura)
        If DD_Struttura.Items.Count = 1 Then
            Call AggiornaCServ()
        End If



        Dim Barra As New Cls_BarraSenior

        If Not IsNothing(Session("RicercaAnagraficaSQLString")) Then
            K1 = Session("RicercaAnagraficaSQLString")
        End If

        Lbl_BarraSenior.Text = Barra.CodiceBarra(Application("SENIOR"), Session("UTENTE"), Page, K1)
    End Sub

    Protected Sub Btn_Seleziona_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles Btn_Seleziona.Click
        Dim Riga As Integer


        For Riga = 0 To GridView1.Rows.Count - 1

            Dim CheckBox As CheckBox = DirectCast(GridView1.Rows(Riga).FindControl("ChkImporta"), CheckBox)


            If CheckBox.Enabled = True Then
                CheckBox.Checked = True
            End If
        Next
    End Sub

   Function SostitusciCaratteriSpeciali(ByVal Testo As String) As String
        Testo = Testo.Replace("à","a'")
        Testo = Testo.Replace("è","e'")
        Testo = Testo.Replace("é","e'")
        Testo = Testo.Replace("ù","u'")
        Testo = Testo.Replace("ì","i'")
        Testo = Testo.Replace("ò","o'")
        Testo = Testo.Replace("À","A'")
        Testo = Testo.Replace("È","E'")
        Testo = Testo.Replace("É","E'")
        Testo = Testo.Replace("Ù","U'")
        Testo = Testo.Replace("Ì","I'")
        Testo = Testo.Replace("Ò","O'")
        Testo = Testo.Replace("°","")

        Return Testo
    End Function


    Function AdattaLunghezzaNumero(ByVal Testo As String, ByVal Lunghezza As Integer) As String
        If Len(Testo) > Lunghezza Then
            AdattaLunghezzaNumero = Mid(Testo, 1, Lunghezza)
            Exit Function
        End If
        AdattaLunghezzaNumero = Replace(Space(Lunghezza - Len(Testo)) & Testo, " ", "0")
        REM AdattaLunghezzaNumero = Space(Lunghezza - Len(Testo)) & Testo


    End Function

    Function AdattaLunghezza(ByVal Testo As String, ByVal Lunghezza As Integer) As String

        Testo = SostitusciCaratteriSpeciali(Testo)

        If Len(Testo) > Lunghezza Then
            Testo = Mid(Testo, 1, Lunghezza)
        End If

        AdattaLunghezza = Testo & Space(Lunghezza - Len(Testo) + 1)

        If Len(AdattaLunghezza) > Lunghezza Then
            AdattaLunghezza = Mid(AdattaLunghezza, 1, Lunghezza)
        End If

    End Function



    Protected Sub Btn_Export_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles Btn_Export.Click
        Call Export_DocumentiIncassi()

        Btn_DownloadMOVIM.Visible = True
    End Sub

    Protected Sub Btn_Esci_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles Btn_Esci.Click
        Response.Redirect("../Menu_Export.aspx")
    End Sub


    Protected Sub Btn_DownloadMOVIM_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles Btn_DownloadMOVIM.Click
        Dim NomeFile As String

        Try
            Kill(HostingEnvironment.ApplicationPhysicalPath() & "\Public\movim.")
            Kill(HostingEnvironment.ApplicationPhysicalPath() & "\Public\ivamov.")
            Kill(HostingEnvironment.ApplicationPhysicalPath() & "\Public\clisisp.")
            Kill(HostingEnvironment.ApplicationPhysicalPath() & "\Public\movpart.")

        Catch ex As Exception

        End Try


        Dim Zipfile As New Ionic.Zip.ZipFile

        NomeFile = HostingEnvironment.ApplicationPhysicalPath() & "\Public\MOVIM_" & Format(Session("MYDATE"), "yyyyMMddHHmmss") & ".Txt"

        Rename(NomeFile, HostingEnvironment.ApplicationPhysicalPath() & "\Public\movim.")

        Zipfile.AddFile(HostingEnvironment.ApplicationPhysicalPath() & "\Public\movim.", "\")


        NomeFile = HostingEnvironment.ApplicationPhysicalPath() & "\Public\IVAMOV_" & Format(Session("MYDATE"), "yyyyMMddHHmmss") & ".Txt"

        Rename(NomeFile, HostingEnvironment.ApplicationPhysicalPath() & "\Public\ivamov.")


        Zipfile.AddFile(HostingEnvironment.ApplicationPhysicalPath() & "\Public\ivamov.", "\")



        NomeFile = HostingEnvironment.ApplicationPhysicalPath() & "\Public\CLISISP_" & Format(Session("MYDATE"), "yyyyMMddHHmmss") & ".Txt"


        Rename(NomeFile, HostingEnvironment.ApplicationPhysicalPath() & "\Public\clisisp.")

        Zipfile.AddFile(HostingEnvironment.ApplicationPhysicalPath() & "\Public\clisisp.", "\")


        'NomeFile = HostingEnvironment.ApplicationPhysicalPath() & "\Public\movpart_" & Format(Session("MYDATE"), "yyyyMMddHHmmss") & ".Txt"


        'Rename(NomeFile, HostingEnvironment.ApplicationPhysicalPath() & "\Public\movpart.")

        'Zipfile.AddFile(HostingEnvironment.ApplicationPhysicalPath() & "\Public\movpart.", "\")

        Zipfile.Save(HostingEnvironment.ApplicationPhysicalPath() & "\Public\export.zip")

        Try

            Response.Clear()
            Response.Buffer = True
            Response.ContentType = "text/plain"
            Response.AppendHeader("content-disposition", "attachment;filename=documenti.zip")
            Response.WriteFile(HostingEnvironment.ApplicationPhysicalPath() & "\Public\export.zip")
            Response.Flush()
            Response.End()
        Catch ex As Exception

        Finally
            Try
                Kill(NomeFile)
            Catch ex As Exception

            End Try
        End Try
    End Sub


    Private Sub AggiornaCServ()
        Dim kCsrv As New Cls_CentroServizio

        If DD_Struttura.SelectedValue = "" Then
            kCsrv.UpDateDropBox(Session("DC_OSPITE"), DD_CServ)
        Else
            kCsrv.UpDateDropBoxStruttura(Session("DC_OSPITE"), DD_CServ, DD_Struttura.SelectedValue)
        End If
    End Sub

    Protected Sub DD_Struttura_TextChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles DD_Struttura.TextChanged
        AggiornaCServ()
    End Sub




End Class
