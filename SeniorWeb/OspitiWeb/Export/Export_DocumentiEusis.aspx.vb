﻿Imports Microsoft.VisualBasic
Imports System.Data.OleDb
Imports System.Web.Hosting
Imports System.Diagnostics
Imports System.ComponentModel
Imports System.Threading
Imports System.IO.Compression
Partial Class OspitiWeb_Export_Export_DocumentiEusis
    Inherits System.Web.UI.Page
    Dim MyTable As New System.Data.DataTable("tabella")
    Dim Tabella As New System.Data.DataTable("EstraiTabella")
    Dim MyDataSet As New System.Data.DataSet()

    Class Anagrafica
        Public DO11_RAGIONESOCIALE As String = ""
        Public DO11_PIVA As String = ""
        Public DO11_CF As String = ""
        Public DO11_INDIRIZZO As String = ""
        Public DO11_CAP As String = ""
        Public DO11_CITTA_NOME As String = ""
        Public DO11_CITTA_ISTAT As String = ""
        Public DO11_PROVINCIA As String = ""
        Public DO11_CONDPAG_CODICE_SEN As String = ""
        Public DO11_CONDPAG_NOME_SEN As String = ""
        Public DO11_BANCA_CIN As String = ""
        Public DO11_BANCA_ABI As String = ""
        Public DO11_BANCA_CAB As String = ""
        Public DO11_BANCA_CONTO As String = ""
        Public DO11_BANCA_IBAN As String = ""
        Public DO11_BANCA_IDMANDATO As String = ""
        Public DO11_BANCA_DATAMANDATO As String = ""
        Public DO11_OPPOSIZIONE As String = ""



        Public DES_RAGIONESOCIALE As String = ""
        Public DES_PIVA As String = ""
        Public DES_CF As String = ""
        Public DES_INDIRIZZO As String = ""
        Public DES_CAP As String = ""
        Public DES_CITTA_NOME As String = ""
        Public DES_CITTA_ISTAT As String = ""
        Public DES_PROVINCIA As String = ""

        Public AddDescrizione As String
    End Class

    Class Testa
        Public CODIMP As String
        Public TIPODOC As String
        Public ESERCIZIO As String
        Public NUMDOC As String
        Public DATADOC As String
        Public MODPAGAM As String
        Public DATAPAG As String
        Public IMPORTO As String
        Public IMPONIBILE As String
        Public IMPOSTA As String
        Public ESENTE As String
        Public FUORICAMPO As String
        Public BOLLO As String
        Public CLIENTE As String
        Public IPC As String
        Public DENOMIN1 As String
        Public DENOMIN2 As String
        Public INDIRIZZO As String
        Public CAP As String
        Public CITTA As String
        Public PROV As String
        Public PIVA As String
        Public CODFISC As String
        Public ABI As String
        Public CAB As String
        Public LINGUA As String
        Public CONTOCREDITO As String
        Public CASSABANCA As String
        Public STRUTTURA As String
        Public TIPOCDC As String
        Public CDCCRIT As String
        Public MESE_INI As String
        Public MESE_FIN As String
        Public ANNO_INI As String
        Public ANNO_FIN As String
        Public AREA_COM As String
        Public SERIE_COM As String
        Public COMMESSA As String
        Public FINANZIAMENTO As String
        Public DT_IMPORTA As String
        Public NOTE1 As String
        Public NOTE2 As String
        Public FL_IMPORTA As String
        Public DESTINATARIO As String
        Public DENOM_DES As String
        Public DENOM2_DES As String
        Public VIA_DES As String
        Public CAP_DES As String
        Public CITTA_DES As String
        Public PROV_DES As String
        Public PIVA_DES As String
        Public CODFISC_DES As String
        Public STATO As String
        Public NAZIONE As String
        Public NAZIONE_DES As String
        Public RIFDOCPART As String
        Public UFFRICH As String
        Public ISTITUTO_DEF As String
        Public SUBTSERIE_DEF As String
        Public CODSERIE_DEF As String
        Public NUMCAR_DEF As String
        Public CODIMP_COL As String
        Public TIPODOC_COL As String
        Public ESERCIZIO_COL As String
        Public NUMDOC_COL As String
        Public DATA_NASCITA As String
        Public FL_EXPORT As String
        Public MOD_INVIO As String
        Public CODICEIPA As String
        Public CODICEAMM As String
        Public CODPAG As String
        Public FL_BOLLO As String
        Public CODSEZ As String
        Public FLAGSPLIT As String
        Public NDOCIVA As String
        Public C_CORRENTE As String
        Public COD_ISO As String
        Public CHECK_DIGIT As String
        Public C_CIN As String
        Public DATASCAD As String
        Public CODIUV As String
        Public CODIUV_ANN As String

        Public FL_OPPOSIZIONE As String
        Public COD_REGIONE As String
        Public COD_ASL As String
        Public COD_SSA As String
        Public CODCONT As String
        Public CLASCONT As String
    End Class
    Class Riga
        Public CODIMP As String
        Public TIPODOC As String
        Public ESERCIZIO As String
        Public NUM_DOC As String
        Public RIGA As String
        Public CODPREST As String
        Public DES_PREST As String
        Public DES_PREST1 As String
        Public QUANTITA As String
        Public PREZZO As String
        Public SCONTO As String
        Public IMPOSTA As String
        Public DT_EFFETTUA As String
        Public TIPO_EQUIPE As String
        Public EQUIPE As String
        Public MEDICO As String
        Public FL_LP As String
        Public TIPO_LP As String
        Public ANNO_BOL As String
        Public NUMERO_BOL As String
        Public DATA_BOL As String
        Public NOSOLOGICO As String
        Public IMPORTO As String
        Public IVA As String
        Public CONTO As String
        Public CAPITOLO As String
        Public SIOPE As String
        Public TIPO_RIGA As String
        Public CODTAR As String
        Public DTCOMPINI As String
        Public DTCOMPFIN As String
        Public NUM_ORDINE As String
        Public DATA_ORDINE As String
        Public CUP_ORDINE As String
        Public CIG_ORDINE As String
        Public ANNOBDG As String
        Public BUDGET As String
        Public SUBBDG As String
        Public COD_COMUNICAZ As String
        Public BEF As String
        Public NUM_RICEZIONE As String
        Public LINEA_RICEZIONE As String
        Public CODPO As String
        Public TIPO_SPESA As String
    End Class


    Private Sub ScriviRiga(ByVal Dati As Riga, ByVal File As System.IO.TextWriter)
        Dim Stringa As String = ""

        Stringa = ""
        Stringa = Stringa & Dati.CODIMP & ";"
        Stringa = Stringa & Dati.TIPODOC & ";"
        Stringa = Stringa & Dati.ESERCIZIO & ";"
        Stringa = Stringa & Dati.NUM_DOC & ";"
        Stringa = Stringa & Dati.RIGA & ";"
        Stringa = Stringa & Dati.CODPREST & ";"
        Stringa = Stringa & Dati.DES_PREST & ";"
        Stringa = Stringa & Dati.DES_PREST1 & ";"
        Stringa = Stringa & Dati.QUANTITA & ";"
        Stringa = Stringa & Dati.PREZZO & ";"
        Stringa = Stringa & Dati.SCONTO & ";"
        Stringa = Stringa & Dati.IMPOSTA & ";"
        Stringa = Stringa & Dati.DT_EFFETTUA & ";"
        Stringa = Stringa & Dati.TIPO_EQUIPE & ";"
        Stringa = Stringa & Dati.EQUIPE & ";"
        Stringa = Stringa & Dati.MEDICO & ";"
        Stringa = Stringa & Dati.FL_LP & ";"
        Stringa = Stringa & Dati.TIPO_LP & ";"
        Stringa = Stringa & Dati.ANNO_BOL & ";"
        Stringa = Stringa & Dati.NUMERO_BOL & ";"
        Stringa = Stringa & Dati.DATA_BOL & ";"
        Stringa = Stringa & Dati.NOSOLOGICO & ";"
        Stringa = Stringa & Dati.IMPORTO & ";"
        Stringa = Stringa & Dati.IVA & ";"
        Stringa = Stringa & Dati.CONTO & ";"
        Stringa = Stringa & Dati.CAPITOLO & ";"
        Stringa = Stringa & Dati.SIOPE & ";"
        Stringa = Stringa & Dati.TIPO_RIGA & ";"
        Stringa = Stringa & Dati.CODTAR & ";"
        Stringa = Stringa & Dati.DTCOMPINI & ";"
        Stringa = Stringa & Dati.DTCOMPFIN & ";"
        Stringa = Stringa & Dati.NUM_ORDINE & ";"
        Stringa = Stringa & Dati.DATA_ORDINE & ";"
        Stringa = Stringa & Dati.CUP_ORDINE & ";"
        Stringa = Stringa & Dati.CIG_ORDINE & ";"
        Stringa = Stringa & Dati.ANNOBDG & ";"
        Stringa = Stringa & Dati.BUDGET & ";"
        Stringa = Stringa & Dati.SUBBDG & ";"
        Stringa = Stringa & Dati.COD_COMUNICAZ & ";"
        Stringa = Stringa & Dati.BEF & ";"
        Stringa = Stringa & Dati.NUM_RICEZIONE & ";"
        Stringa = Stringa & Dati.LINEA_RICEZIONE & ";"
        Stringa = Stringa & Dati.CODPO & ";"
        Stringa = Stringa & Dati.TIPO_SPESA  
        File.WriteLine(Stringa)

    End Sub
    Class Incasso
        Public CODIMP As String
        Public TIPODOC As String
        Public ESERCIZIO As String
        Public NUMDOC As String
        Public RIGA As String
        Public DATAACC As String
        Public IMPORTO As String
        Public TIPO_CHIUSRA As String
        Public CAU_CHIU As String
        Public DES_CHIU As String
        Public DATA_CHIU As String
        Public SERVIZIO As String
        Public ANNO As String
        Public CASSA As String
        Public ID_ESTERNO As String
    End Class


    Private Sub ScriviIncasso(ByVal Dati As Incasso, ByVal File As System.IO.TextWriter)
        Dim Stringa As String = ""

        Stringa = ""
        Stringa = Stringa & Dati.CODIMP & ";"
        Stringa = Stringa & Dati.TIPODOC & ";"
        Stringa = Stringa & Dati.ESERCIZIO & ";"
        Stringa = Stringa & Dati.NUMDOC & ";"
        Stringa = Stringa & Dati.RIGA & ";"
        Stringa = Stringa & Dati.DATAACC & ";"
        Stringa = Stringa & Dati.IMPORTO & ";"
        Stringa = Stringa & Dati.TIPO_CHIUSRA & ";"
        Stringa = Stringa & Dati.CAU_CHIU & ";"
        Stringa = Stringa & Dati.DES_CHIU & ";"
        Stringa = Stringa & Dati.DATA_CHIU & ";"
        Stringa = Stringa & Dati.SERVIZIO & ";"
        Stringa = Stringa & Dati.ANNO & ";"
        Stringa = Stringa & Dati.CASSA & ";"
        Stringa = Stringa & Dati.ID_ESTERNO 
        File.WriteLine(Stringa)

    End Sub


    Private Sub ScriviTesta(ByVal Dati As Testa, ByVal File As System.IO.TextWriter)
        Dim Stringa As String = ""

        Stringa = ""
        Stringa = Stringa & Dati.CODIMP & ";"
        Stringa = Stringa & Dati.TIPODOC & ";"
        Stringa = Stringa & Dati.ESERCIZIO & ";"
        Stringa = Stringa & Dati.NUMDOC & ";"
        Stringa = Stringa & Dati.DATADOC & ";"
        Stringa = Stringa & Dati.MODPAGAM & ";"
        Stringa = Stringa & Dati.DATAPAG & ";"
        Stringa = Stringa & Dati.IMPORTO & ";"
        Stringa = Stringa & Dati.IMPONIBILE & ";"
        Stringa = Stringa & Dati.IMPOSTA & ";"
        Stringa = Stringa & Dati.ESENTE & ";"
        Stringa = Stringa & Dati.FUORICAMPO & ";"
        Stringa = Stringa & Dati.BOLLO & ";"
        Stringa = Stringa & Dati.CLIENTE & ";"
        Stringa = Stringa & Dati.IPC & ";"
        Stringa = Stringa & Dati.DENOMIN1 & ";"
        Stringa = Stringa & Dati.DENOMIN2 & ";"
        Stringa = Stringa & Dati.INDIRIZZO & ";"
        Stringa = Stringa & Dati.CAP & ";"
        Stringa = Stringa & Dati.CITTA & ";"
        Stringa = Stringa & Dati.PROV & ";"
        Stringa = Stringa & Dati.PIVA & ";"
        Stringa = Stringa & Dati.CODFISC & ";"
        Stringa = Stringa & Dati.ABI & ";"
        Stringa = Stringa & Dati.CAB & ";"
        Stringa = Stringa & Dati.LINGUA & ";"
        Stringa = Stringa & Dati.CONTOCREDITO & ";"
        Stringa = Stringa & Dati.CASSABANCA & ";"
        Stringa = Stringa & Dati.STRUTTURA & ";"
        Stringa = Stringa & Dati.TIPOCDC & ";"
        Stringa = Stringa & Dati.CDCCRIT & ";"
        Stringa = Stringa & Dati.MESE_INI & ";"
        Stringa = Stringa & Dati.MESE_FIN & ";"
        Stringa = Stringa & Dati.ANNO_INI & ";"
        Stringa = Stringa & Dati.ANNO_FIN & ";"
        Stringa = Stringa & Dati.AREA_COM & ";"
        Stringa = Stringa & Dati.SERIE_COM & ";"
        Stringa = Stringa & Dati.COMMESSA & ";"
        Stringa = Stringa & Dati.FINANZIAMENTO & ";"
        Stringa = Stringa & Dati.DT_IMPORTA & ";"
        Stringa = Stringa & Dati.NOTE1 & ";"
        Stringa = Stringa & Dati.NOTE2 & ";"
        Stringa = Stringa & Dati.FL_IMPORTA & ";"
        Stringa = Stringa & Dati.DESTINATARIO & ";"
        Stringa = Stringa & Dati.DENOM_DES & ";"
        Stringa = Stringa & Dati.DENOM2_DES & ";"
        Stringa = Stringa & Dati.VIA_DES & ";"
        Stringa = Stringa & Dati.CAP_DES & ";"
        Stringa = Stringa & Dati.CITTA_DES & ";"
        Stringa = Stringa & Dati.PROV_DES & ";"
        Stringa = Stringa & Dati.PIVA_DES & ";"
        Stringa = Stringa & Dati.CODFISC_DES & ";"
        Stringa = Stringa & Dati.STATO & ";"
        Stringa = Stringa & Dati.NAZIONE & ";"
        Stringa = Stringa & Dati.NAZIONE_DES & ";"
        Stringa = Stringa & Dati.RIFDOCPART & ";"
        Stringa = Stringa & Dati.UFFRICH & ";"
        Stringa = Stringa & Dati.ISTITUTO_DEF & ";"
        Stringa = Stringa & Dati.SUBTSERIE_DEF & ";"
        Stringa = Stringa & Dati.CODSERIE_DEF & ";"
        Stringa = Stringa & Dati.NUMCAR_DEF & ";"
        Stringa = Stringa & Dati.CODIMP_COL & ";"
        Stringa = Stringa & Dati.TIPODOC_COL & ";"
        Stringa = Stringa & Dati.ESERCIZIO_COL & ";"
        Stringa = Stringa & Dati.NUMDOC_COL & ";"
        Stringa = Stringa & Dati.DATA_NASCITA & ";"
        Stringa = Stringa & Dati.FL_EXPORT & ";"
        Stringa = Stringa & Dati.MOD_INVIO & ";"
        Stringa = Stringa & Dati.CODICEIPA & ";"
        Stringa = Stringa & Dati.CODICEAMM & ";"
        Stringa = Stringa & Dati.CODPAG & ";"
        Stringa = Stringa & Dati.FL_BOLLO & ";"
        Stringa = Stringa & Dati.CODSEZ & ";"
        Stringa = Stringa & Dati.FLAGSPLIT & ";"
        Stringa = Stringa & Dati.NDOCIVA & ";"
        Stringa = Stringa & Dati.C_CORRENTE & ";"
        Stringa = Stringa & Dati.COD_ISO & ";"
        Stringa = Stringa & Dati.CHECK_DIGIT & ";"
        Stringa = Stringa & Dati.C_CIN & ";"
        Stringa = Stringa & Dati.DATASCAD & ";"
        Stringa = Stringa & Dati.CODIUV & ";"
        Stringa = Stringa & Dati.CODIUV_ANN & ";"
        Stringa = Stringa & Dati.FL_OPPOSIZIONE & ";"
        Stringa = Stringa & Dati.COD_REGIONE & ";"
        Stringa = Stringa & Dati.COD_ASL & ";"
        Stringa = Stringa & Dati.COD_SSA & ";"
        Stringa = Stringa & Dati.CODCONT & ";"
        Stringa = Stringa & Dati.CLASCONT


        File.WriteLine(Stringa)


    End Sub




    Private Sub DecodificaAnagrafica(ByRef Dati As Anagrafica, ByVal Registrazione As Cls_MovimentoContabile)
        Dim cn As OleDbConnection
        Dim Cliente As String
        Dim CodiceOspite As Integer
        Dim CodiceParente As Integer
        Dim CodiceProvincia As String
        Dim CodiceComune As String
        Dim CodiceRegione As String
        Dim Tipo As String

        cn = New Data.OleDb.OleDbConnection(Session("DC_GENERALE"))
        cn.Open()
        Tipo = Registrazione.Tipologia

        Dim cmdRd As New OleDbCommand()
        cmdRd.CommandText = "Select * From MovimentiContabiliRiga Where Numero = ?  And Tipo ='CF'   "
        cmdRd.Connection = cn
        cmdRd.Parameters.AddWithValue("@Numero", Registrazione.NumeroRegistrazione)
        Dim MyReadSC As OleDbDataReader = cmdRd.ExecuteReader()
        If MyReadSC.Read Then
            Cliente = campodb(MyReadSC.Item("MastroPartita")) & "." & campodb(MyReadSC.Item("ContoPartita")) & "." & campodb(MyReadSC.Item("SottocontoPartita"))
            If Mid(Tipo & Space(10), 1, 1) = " " Then
                CodiceOspite = Int(campodbN(MyReadSC.Item("SottocontoPartita")) / 100)
                CodiceParente = campodbN(MyReadSC.Item("SottocontoPartita")) - (CodiceOspite * 100)

                If CodiceParente = 0 And CodiceOspite > 0 Then
                    Tipo = "O" & Space(10)
                End If
                If CodiceParente > 0 And CodiceOspite > 0 Then
                    Tipo = "P" & Space(10)
                End If
            End If
            If Mid(Tipo & Space(10), 1, 1) = "C" Then
                CodiceProvincia = Mid(Tipo & Space(10), 2, 3)
                CodiceComune = Mid(Tipo & Space(10), 5, 3)
            End If
            If Mid(Tipo & Space(10), 1, 1) = "J" Then
                CodiceProvincia = Mid(Tipo & Space(10), 2, 3)
                CodiceComune = Mid(Tipo & Space(10), 5, 3)
            End If
            If Mid(Tipo & Space(10), 1, 1) = "R" Then
                CodiceRegione = Mid(Tipo & Space(10), 2, 4)
            End If

        End If
        MyReadSC.Close()
        cn.Close()


        Dati.AddDescrizione = ""
        If Mid(Tipo & Space(10), 1, 1) = "O" Then
            Dim Ospite As New ClsOspite

            Ospite.CodiceOspite = CodiceOspite
            Ospite.Leggi(Session("DC_OSPITE"), Ospite.CodiceOspite)
            Dati.DO11_RAGIONESOCIALE = Ospite.CognomeOspite & " " & Ospite.NomeOspite
            Dati.DO11_PIVA = ""
            Dati.DO11_CF = Ospite.CODICEFISCALE
            Dati.DO11_INDIRIZZO = Ospite.RESIDENZAINDIRIZZO1
            Dati.DO11_CAP = Ospite.RESIDENZACAP1

            If Ospite.Opposizione730 = 1 Then
                Dati.DO11_OPPOSIZIONE = "S"
            Else
                Dati.DO11_OPPOSIZIONE = "N"
            End If
            Dim DcCom As New ClsComune

            DcCom.Provincia = Ospite.RESIDENZAPROVINCIA1
            DcCom.Comune = Ospite.RESIDENZACOMUNE1
            DcCom.Leggi(Session("DC_OSPITE"))

            Dati.DO11_CITTA_NOME = DcCom.Descrizione
            Dati.DO11_CITTA_ISTAT = DcCom.Provincia & DcCom.Comune

            Dim DcProv As New ClsComune

            DcProv.Provincia = Ospite.RESIDENZAPROVINCIA1
            DcProv.Comune = ""
            DcProv.Leggi(Session("DC_OSPITE"))
            Dati.DO11_PROVINCIA = DcProv.CodificaProvincia


            Dim Kl As New Cls_DatiOspiteParenteCentroServizio

            Kl.CodiceOspite = CodiceOspite
            Kl.CodiceParente = 0
            Kl.CentroServizio = Registrazione.CentroServizio
            Kl.Leggi(Session("DC_OSPITE"))
            If Kl.ModalitaPagamento <> "" Then
                Ospite.MODALITAPAGAMENTO = Kl.ModalitaPagamento
            End If


            Dati.DO11_CONDPAG_CODICE_SEN = Ospite.MODALITAPAGAMENTO
            Dim MPAg As New ClsModalitaPagamento

            MPAg.Codice = Ospite.MODALITAPAGAMENTO
            MPAg.Leggi(Session("DC_OSPITE"))
            Dati.DO11_CONDPAG_NOME_SEN = MPAg.DESCRIZIONE


            Dim ModPag As New Cls_DatiPagamento

            ModPag.CodiceOspite = Ospite.CodiceOspite
            ModPag.CodiceParente = 0
            ModPag.LeggiUltimaDataData(Session("DC_OSPITE"), Registrazione.DataRegistrazione)

            Dati.DO11_BANCA_CIN = ModPag.Cin
            Dati.DO11_BANCA_ABI = ModPag.Abi
            Dati.DO11_BANCA_CAB = ModPag.Cab
            Dati.DO11_BANCA_CONTO = ModPag.CCBancario
            Dati.DO11_BANCA_IBAN = ModPag.CodiceInt & Format(Val(ModPag.NumeroControllo), "00") & ModPag.Cin & Format(Val(ModPag.Abi), "00000") & Format(Val(ModPag.Cab), "00000") & AdattaLunghezzaNumero(ModPag.CCBancario, 12)

            Dati.DO11_BANCA_IDMANDATO = ModPag.IdMandatoDebitore
            Dati.DO11_BANCA_DATAMANDATO = ModPag.DataMandatoDebitore

            If Ospite.RecapitoNome <> "" Then
                Dati.DES_RAGIONESOCIALE = Ospite.RecapitoNome

                Dati.DES_INDIRIZZO = Ospite.RecapitoIndirizzo
                Dati.DES_CAP = Ospite.RESIDENZACAP4

                Dim DestCom As New ClsComune

                DestCom.Provincia = Ospite.RecapitoProvincia
                DestCom.Comune = Ospite.RecapitoComune
                DestCom.Leggi(Session("DC_OSPITE"))

                Dati.DES_CITTA_NOME = DestCom.Descrizione
                Dati.DES_CITTA_ISTAT = DestCom.Provincia & DcCom.Comune

                Dim DestProv As New ClsComune

                DestProv.Provincia = Ospite.RecapitoProvincia
                DestProv.Comune = ""
                DestProv.Leggi(Session("DC_OSPITE"))
                Dati.DES_PROVINCIA = DestProv.CodificaProvincia

            End If

        End If
        If Mid(Tipo & Space(10), 1, 1) = "P" Then
            Dim Ospite As New Cls_Parenti

            Ospite.CodiceOspite = CodiceOspite
            Ospite.CodiceParente = CodiceParente
            Ospite.Leggi(Session("DC_OSPITE"), Ospite.CodiceOspite, Ospite.CodiceParente)
            Dati.DO11_RAGIONESOCIALE = Ospite.CognomeParente & " " & Ospite.NomeParente
            Dati.DO11_PIVA = ""
            Dati.DO11_CF = Ospite.CODICEFISCALE
            Dati.DO11_INDIRIZZO = Ospite.RESIDENZAINDIRIZZO1
            Dati.DO11_CAP = Ospite.RESIDENZACAP1


            If Ospite.Opposizione730 = 1 Then
                Dati.DO11_OPPOSIZIONE = "S"
            Else
                Dati.DO11_OPPOSIZIONE = "N"
            End If
            Dim DcCom As New ClsComune

            DcCom.Provincia = Ospite.RESIDENZAPROVINCIA1
            DcCom.Comune = Ospite.RESIDENZACOMUNE1
            DcCom.Leggi(Session("DC_OSPITE"))

            Dati.DO11_CITTA_NOME = DcCom.Descrizione
            Dati.DO11_CITTA_ISTAT = DcCom.Provincia & DcCom.Comune

            Dim DcProv As New ClsComune

            DcProv.Provincia = Ospite.RESIDENZAPROVINCIA1
            DcProv.Comune = ""
            DcProv.Leggi(Session("DC_OSPITE"))
            Dati.DO11_PROVINCIA = DcProv.CodificaProvincia
            Dati.DO11_CONDPAG_CODICE_SEN = Ospite.MODALITAPAGAMENTO

            Dim Kl As New Cls_DatiOspiteParenteCentroServizio

            Kl.CodiceOspite = CodiceOspite
            Kl.CodiceParente = CodiceParente
            Kl.CentroServizio = Registrazione.CentroServizio
            Kl.Leggi(Session("DC_OSPITE"))
            If Kl.ModalitaPagamento <> "" Then
                Ospite.MODALITAPAGAMENTO = Kl.ModalitaPagamento
            End If

            Dim MPAg As New ClsModalitaPagamento

            MPAg.Codice = Ospite.MODALITAPAGAMENTO
            MPAg.Leggi(Session("DC_OSPITE"))
            Dati.DO11_CONDPAG_NOME_SEN = MPAg.DESCRIZIONE

            Dim ModPag As New Cls_DatiPagamento

            ModPag.CodiceOspite = Ospite.CodiceOspite
            ModPag.CodiceParente = Ospite.CodiceParente
            ModPag.LeggiUltimaDataData(Session("DC_OSPITE"), Registrazione.DataRegistrazione)

            Dati.DO11_BANCA_CIN = ModPag.Cin
            Dati.DO11_BANCA_ABI = ModPag.Abi
            Dati.DO11_BANCA_CAB = ModPag.Cab
            Dati.DO11_BANCA_CONTO = ModPag.CCBancario
            Dati.DO11_BANCA_IBAN = ModPag.CodiceInt & Format(Val(ModPag.NumeroControllo), "00") & ModPag.Cin & Format(Val(ModPag.Abi), "00000") & Format(Val(ModPag.Cab), "00000") & AdattaLunghezzaNumero(ModPag.CCBancario, 12)

            Dati.DO11_BANCA_IDMANDATO = ModPag.IdMandatoDebitore
            Dati.DO11_BANCA_DATAMANDATO = ModPag.DataMandatoDebitore
        End If
        If Mid(Tipo & Space(10), 1, 1) = "C" Then
            Dim Ospite As New ClsComune

            Ospite.Provincia = CodiceProvincia
            Ospite.Comune = CodiceComune
            Ospite.Leggi(Session("DC_OSPITE"))
            Dati.DO11_RAGIONESOCIALE = Ospite.Descrizione
            Dati.DO11_PIVA = Ospite.PartitaIVA
            Dati.DO11_CF = Ospite.CodiceFiscale
            Dati.DO11_INDIRIZZO = Ospite.RESIDENZAINDIRIZZO1
            Dati.DO11_CAP = Ospite.RESIDENZACAP1
            Dim DcCom As New ClsComune

            DcCom.Provincia = Ospite.RESIDENZAPROVINCIA1
            DcCom.Comune = Ospite.RESIDENZACOMUNE1
            DcCom.Leggi(Session("DC_OSPITE"))

            Dati.DO11_CITTA_NOME = DcCom.Descrizione
            Dati.DO11_CITTA_ISTAT = DcCom.Provincia & DcCom.Comune

            Dim DcProv As New ClsComune

            DcProv.Provincia = Ospite.RESIDENZAPROVINCIA1
            DcProv.Comune = ""
            DcProv.Leggi(Session("DC_OSPITE"))
            Dati.DO11_PROVINCIA = DcProv.CodificaProvincia
            Dati.DO11_CONDPAG_CODICE_SEN = Ospite.ModalitaPagamento
            Dim MPAg As New ClsModalitaPagamento

            MPAg.Codice = Ospite.ModalitaPagamento
            MPAg.Leggi(Session("DC_OSPITE"))
            Dati.DO11_CONDPAG_NOME_SEN = MPAg.DESCRIZIONE

            Dim cnOspiti As OleDbConnection

            cnOspiti = New Data.OleDb.OleDbConnection(Session("DC_OSPITE"))

            cnOspiti.Open()

            Dim cmdC As New OleDbCommand()
            cmdC.CommandText = ("select * from NoteFatture where CodiceProvincia = ? And CodiceComune = ?")
            cmdC.Parameters.AddWithValue("@CodiceProvincia", CodiceProvincia)
            cmdC.Parameters.AddWithValue("@CodiceComune", CodiceComune)
            cmdC.Connection = cnOspiti
            Dim RdCom As OleDbDataReader = cmdC.ExecuteReader()
            If RdCom.Read Then
                Dati.AddDescrizione = campodb(RdCom.Item("NoteUp"))
            End If
            RdCom.Close()
            cnOspiti.Close()
        End If
        If Mid(Tipo & Space(10), 1, 1) = "J" Then
            Dim Ospite As New ClsComune

            Ospite.Provincia = CodiceProvincia
            Ospite.Comune = CodiceComune
            Ospite.Leggi(Session("DC_OSPITE"))
            Dati.DO11_RAGIONESOCIALE = Ospite.Descrizione
            Dati.DO11_PIVA = Ospite.PartitaIVA
            Dati.DO11_CF = Ospite.CodiceFiscale
            Dati.DO11_INDIRIZZO = Ospite.RESIDENZAINDIRIZZO1
            Dati.DO11_CAP = Ospite.RESIDENZACAP1
            Dim DcCom As New ClsComune

            DcCom.Provincia = Ospite.RESIDENZAPROVINCIA1
            DcCom.Comune = Ospite.RESIDENZACOMUNE1
            DcCom.Leggi(Session("DC_OSPITE"))

            Dati.DO11_CITTA_NOME = DcCom.Descrizione
            Dati.DO11_CITTA_ISTAT = DcCom.Provincia & DcCom.Comune

            Dim DcProv As New ClsComune

            DcProv.Provincia = Ospite.RESIDENZAPROVINCIA1
            DcProv.Comune = ""
            DcProv.Leggi(Session("DC_OSPITE"))
            Dati.DO11_PROVINCIA = DcProv.CodificaProvincia
            Dati.DO11_CONDPAG_CODICE_SEN = Ospite.ModalitaPagamento
            Dim MPAg As New ClsModalitaPagamento

            MPAg.Codice = Ospite.ModalitaPagamento
            MPAg.Leggi(Session("DC_OSPITE"))
            Dati.DO11_CONDPAG_NOME_SEN = MPAg.DESCRIZIONE

            Dim cnOspiti As OleDbConnection

            cnOspiti = New Data.OleDb.OleDbConnection(Session("DC_OSPITE"))

            cnOspiti.Open()

            Dim cmdC As New OleDbCommand()
            cmdC.CommandText = ("select * from NoteFatture where CodiceProvincia = ? And CodiceComune = ?")
            cmdC.Parameters.AddWithValue("@CodiceProvincia", CodiceProvincia)
            cmdC.Parameters.AddWithValue("@CodiceComune", CodiceComune)
            cmdC.Connection = cnOspiti
            Dim RdCom As OleDbDataReader = cmdC.ExecuteReader()
            If RdCom.Read Then
                Dati.AddDescrizione = campodb(RdCom.Item("NoteUp"))
            End If
            RdCom.Close()
            cnOspiti.Close()
        End If
        If Mid(Tipo & Space(10), 1, 1) = "R" Then
            Dim Ospite As New ClsUSL

            Ospite.CodiceRegione = CodiceRegione
            Ospite.Leggi(Session("DC_OSPITE"))
            Dati.DO11_RAGIONESOCIALE = Ospite.Nome
            Dati.DO11_PIVA = Ospite.PARTITAIVA
            Dati.DO11_CF = Ospite.CodiceFiscale
            Dati.DO11_INDIRIZZO = Ospite.RESIDENZAINDIRIZZO1
            Dati.DO11_CAP = Ospite.RESIDENZACAP1
            Dim DcCom As New ClsComune

            DcCom.Provincia = Ospite.RESIDENZAPROVINCIA1
            DcCom.Comune = Ospite.RESIDENZACOMUNE1
            DcCom.Leggi(Session("DC_OSPITE"))

            Dati.DO11_CITTA_NOME = DcCom.Descrizione
            Dati.DO11_CITTA_ISTAT = DcCom.Provincia & DcCom.Comune

            Dim DcProv As New ClsComune

            DcProv.Provincia = Ospite.RESIDENZAPROVINCIA1
            DcProv.Comune = ""
            DcProv.Leggi(Session("DC_OSPITE"))
            Dati.DO11_PROVINCIA = DcProv.CodificaProvincia
            Dati.DO11_CONDPAG_CODICE_SEN = Ospite.ModalitaPagamento
            Dim MPAg As New ClsModalitaPagamento

            MPAg.Codice = Ospite.ModalitaPagamento
            MPAg.Leggi(Session("DC_OSPITE"))
            Dati.DO11_CONDPAG_NOME_SEN = MPAg.DESCRIZIONE
        End If

        cn.Close()
    End Sub

    Function Export_DocumentiIncassi() As Boolean
        Dim cn As OleDbConnection
        Session("MYDATE") = Now


        Dim TwIncassi As System.IO.TextWriter = System.IO.File.CreateText(HostingEnvironment.ApplicationPhysicalPath() & "\Public\Incassi_" & Format(Session("MYDATE"), "yyyyMMddHHmmss") & ".Txt")
        Dim TwFatture As System.IO.TextWriter = System.IO.File.CreateText(HostingEnvironment.ApplicationPhysicalPath() & "\Public\Documenti_" & Format(Session("MYDATE"), "yyyyMMddHHmmss") & ".Txt")
        Dim TwRiga As System.IO.TextWriter = System.IO.File.CreateText(HostingEnvironment.ApplicationPhysicalPath() & "\Public\DocumentiRiga_" & Format(Session("MYDATE"), "yyyyMMddHHmmss") & ".Txt")

        Dim TwCSV As System.IO.TextWriter = System.IO.File.CreateText(HostingEnvironment.ApplicationPhysicalPath() & "\Public\CSV_" & Format(Session("MYDATE"), "yyyyMMddHHmmss") & ".Txt")

        cn = New Data.OleDb.OleDbConnection(Session("DC_GENERALE"))
        cn.Open()

        Dim Param As New Cls_DatiGenerali



        Param.LeggiDati(Session("DC_TABELLE"))

        Tabella = ViewState("App_AddebitiMultiplo")

        Dim NumeroRegistrazioni As Integer = 0
        Dim RigaEstratta As Integer = 0


        For RigaEstratta = 0 To Tabella.Rows.Count - 1
            Dim Chekc As CheckBox = DirectCast(GridView1.Rows(RigaEstratta).FindControl("ChkImporta"), CheckBox)

            If Chekc.Checked = True Then
                NumeroRegistrazioni = NumeroRegistrazioni + 1
            End If
        Next

        If NumeroRegistrazioni = 0 Then
            ScriptManager.RegisterClientScriptBlock(Me, Me.GetType(), "Stampa2", "alert('Attenzione non hai segnalato nessuna registrazione');", True)
            Exit Function
        End If

        NumeroRegistrazioni = 0

        Export_DocumentiIncassi = True

        For RigaEstratta = 0 To Tabella.Rows.Count - 1
            Dim Chekc As CheckBox = DirectCast(GridView1.Rows(RigaEstratta).FindControl("ChkImporta"), CheckBox)

            If Chekc.Checked = True Then
                Dim Registrazione As New Cls_MovimentoContabile
                Dim CausaleContabile As New Cls_CausaleContabile

                Registrazione.NumeroRegistrazione = campodbN(Tabella.Rows(RigaEstratta).Item(0))
                Registrazione.Leggi(Session("DC_GENERALE"), Registrazione.NumeroRegistrazione)

                CausaleContabile.Leggi(Session("DC_TABELLE"), Registrazione.CausaleContabile)

                Dim Anagrafiche As New Anagrafica


                DecodificaAnagrafica(Anagrafiche, Registrazione)

                If Chk_Prova.Checked = False Then
                    Dim cmdEff As New OleDbCommand()

                    cmdEff.CommandText = "UPDATE movimenticontabiliriga SET RigaBollato = 1 where Numero = ?"
                    cmdEff.Parameters.AddWithValue("@Numero", Registrazione.NumeroRegistrazione)
                    cmdEff.Connection = cn
                    cmdEff.ExecuteNonQuery()


                    Dim cmdEffT As New OleDbCommand()

                    cmdEffT.CommandText = "UPDATE movimenticontabiliTesta SET ExportDATA = GETDATE() where NumeroRegistrazione = ?"
                    cmdEffT.Parameters.AddWithValue("@Numero", Registrazione.NumeroRegistrazione)
                    cmdEffT.Connection = cn
                    cmdEffT.ExecuteNonQuery()
                End If

                If CausaleContabile.TipoDocumento = "FA" Or CausaleContabile.TipoDocumento = "RE" Or CausaleContabile.TipoDocumento = "NC" Then
                    Call FattureNC(Anagrafiche, Registrazione, CausaleContabile, TwFatture, TwRiga)
                End If
                If CausaleContabile.Tipo = "P" Then
                    Call Incassi(Anagrafiche, Registrazione, CausaleContabile, TwIncassi)
                End If
                Dim Riga As String

                Riga = Registrazione.NumeroRegistrazione & ";" & Registrazione.DataRegistrazione & ";" & Registrazione.DataDocumento & ";" & Registrazione.NumeroDocumento & ";" & Registrazione.ImportoRegistrazioneDocumento(Session("DC_TABELLE"))

                TwCSV.WriteLine(Riga)
            End If
        Next
        cn.Close()


        TwFatture.Close()
        TwRiga.Close()
        TwIncassi.Close()
        TwCSV.Close()




        If Chk_Prova.Checked = False Then
            Dim IndiceTab As Integer


            For IndiceTab = 0 To GridView1.Rows.Count - 1

                Dim CheckBox As CheckBox = DirectCast(GridView1.Rows(IndiceTab).FindControl("ChkImporta"), CheckBox)

                CheckBox.Enabled = False
                CheckBox.Checked = False
            Next

            Btn_Export.Visible = False
        End If

        Btn_DownloadIncassi.Visible = True

        Btn_DownloadRiga.Visible = True

        Btn_DownloadTesta.Visible = True

        Btn_DownloadCSV.Visible = True

        TabContainer1.ActiveTabIndex = 1
    End Function

    Private Sub Incassi(ByRef Dati As Anagrafica, ByVal Reg As Cls_MovimentoContabile, ByRef CausaleContabile As Cls_CausaleContabile, ByVal File As System.IO.TextWriter)
        Dim Inc As New Incasso
        Dim TabTrascodifica As New Cls_TabellaTrascodificheEsportazioni
        Dim Legami As New Cls_Legami
        Dim X As Integer

        Dim CentroServizio As New Cls_CentroServizio

        CentroServizio.CENTROSERVIZIO = Reg.CentroServizio
        CentroServizio.Leggi(Session("DC_OSPITE"), CentroServizio.CENTROSERVIZIO)


        Legami.Leggi(Session("DC_GENERALE"), 0, Reg.NumeroRegistrazione)

        For X = 0 To 300
            If Not IsNothing(Legami.NumeroDocumento(X)) Then
                If Legami.NumeroDocumento(X) > 0 Then
                    Dim RegistrazioneDocumento As New Cls_MovimentoContabile
                    Dim CausaleContabileDocumento As New Cls_CausaleContabile
                    RegistrazioneDocumento.Leggi(Session("DC_GENERALE"), Legami.NumeroDocumento(X))

                    CausaleContabileDocumento.Leggi(Session("DC_TABELLE"), RegistrazioneDocumento.CausaleContabile)


                    Inc.CODIMP = "14EP" 'SMARIAMONTE

                    If CausaleContabileDocumento.TipoDocumento = "FA" Or CausaleContabileDocumento.TipoDocumento = "RE" Then
                        Inc.TIPODOC = "FC"
                    End If
                    If CausaleContabileDocumento.TipoDocumento = "NC" Then
                        Inc.TIPODOC = "NA"
                    End If
                    Inc.ESERCIZIO = RegistrazioneDocumento.AnnoProtocollo

                    'If Session("DC_OSPITE").ToString.ToUpper.IndexOf("MARIA") >= 0 Then
                    '    Inc.NUMDOC = "17/" & RegistrazioneDocumento.NumeroProtocollo
                    'Else
                    '    Inc.NUMDOC = "88/" & RegistrazioneDocumento.NumeroProtocollo
                    'End If
                    Inc.NUMDOC = RegistrazioneDocumento.NumeroProtocollo

                    Inc.TIPO_CHIUSRA = "I"

                    Inc.RIGA = 1

                    TabTrascodifica.TIPOTAB = "CI"
                    TabTrascodifica.SENIOR = Reg.Righe(1).MastroPartita & "." & Reg.Righe(1).ContoPartita & "." & Reg.Righe(1).SottocontoPartita
                    TabTrascodifica.EXPORT = 0
                    TabTrascodifica.Leggi(Session("DC_TABELLE"))

                    Inc.DATAACC = Format(Reg.DataRegistrazione, "dd/MM/yyyy")
                    Inc.CAU_CHIU = TabTrascodifica.EXPORT 'tabella trascodifica CI 
                    Inc.DES_CHIU = Reg.Descrizione
                    Inc.DATA_CHIU = Format(Reg.DataRegistrazione, "dd/MM/yyyy")

                    Inc.IMPORTO = Format(Legami.Importo(X), "0.00")


                    TabTrascodifica.TIPOTAB = "PR"
                    TabTrascodifica.SENIOR = "1"
                    TabTrascodifica.EXPORT = 0
                    TabTrascodifica.Leggi(Session("DC_TABELLE"))



                    Inc.ID_ESTERNO = TabTrascodifica.EXPORT & Format(Reg.NumeroRegistrazione, "0000000")


                    ScriviIncasso(Inc, File)
                End If
            End If
        Next


    End Sub

    Private Sub FattureNC(ByRef Dati As Anagrafica, ByVal Reg As Cls_MovimentoContabile, ByRef CausaleContabile As Cls_CausaleContabile, ByVal File As System.IO.TextWriter, ByVal FileRiga As System.IO.TextWriter)
        Dim TR As New Testa
        Dim TabTrascodifica As New Cls_TabellaTrascodificheEsportazioni


        Dim CentroServizio As New Cls_CentroServizio

        CentroServizio.CENTROSERVIZIO = Reg.CentroServizio
        CentroServizio.Leggi(Session("DC_OSPITE"), CentroServizio.CENTROSERVIZIO)




        TR.CODIMP = "" 'CODICE CHE DOVREBBERO COMUNICARE
        If CausaleContabile.TipoDocumento = "FA" Or CausaleContabile.TipoDocumento = "RE" Then
            TR.TIPODOC = "FC"
        End If
        If CausaleContabile.TipoDocumento = "NC" Then
            TR.TIPODOC = "NA"
        End If
        TR.ESERCIZIO = Reg.AnnoProtocollo

        'If Session("DC_OSPITE").ToString.ToUpper.IndexOf("MARIA") >= 0 Then
        '    TR.NUMDOC = "17/" & Reg.NumeroProtocollo
        'Else
        '    TR.NUMDOC = "88/" & Reg.NumeroProtocollo
        'End If
        TR.NUMDOC = Reg.NumeroProtocollo

        TR.DATADOC = Format(Reg.DataDocumento, "dd/MM/yyyy")

        TabTrascodifica.TIPOTAB = "MP"
        TabTrascodifica.SENIOR = Reg.CodicePagamento
        TabTrascodifica.EXPORT = 0
        TabTrascodifica.Leggi(Session("DC_TABELLE"))
        TR.MODPAGAM = TabTrascodifica.EXPORT

        TR.DATAPAG = ""


        TR.IMPORTO = Format(Reg.ImportoDocumento(Session("DC_TABELLE")), "0.00")

        Dim Imponibile As Double = 0
        Dim Importo As Double = 0
        Dim Esente As Double = 0
        Dim FuoriCampo As Double = 0
        Dim Riga As Integer

        TR.BOLLO = "0,00"

        For Riga = 0 To 100
            If Not IsNothing(Reg.Righe(Riga)) Then
                If Reg.Righe(Riga).Tipo = "IV" Then
                    If Reg.Righe(Riga).Importo > 0 Then
                        Imponibile = Imponibile + Reg.Righe(Riga).Imponibile
                        Importo = Importo + Reg.Righe(Riga).Importo
                    End If
                    Dim CIva As New Cls_IVA

                    CIva.Codice = Reg.Righe(Riga).CodiceIVA
                    CIva.Leggi(Session("DC_TABELLE"), CIva.Codice)

                    If CIva.Tipo = "ES" Then
                        Esente = Esente + Reg.Righe(Riga).Imponibile
                    End If
                    If CIva.Tipo = "NS" Or CIva.Tipo = "NI" Then
                        FuoriCampo = FuoriCampo + Reg.Righe(Riga).Imponibile
                    End If
                End If
                If Reg.Righe(Riga).RigaDaCausale = 9 Then
                    TR.BOLLO = Format(Reg.Righe(Riga).Importo, "0.00")
                End If
            End If
        Next

        TR.IMPONIBILE = Format(Imponibile, "0.00")
        TR.IMPOSTA = Format(Importo, "0.00")
        TR.ESENTE = Format(Esente, "0.00")
        TR.FUORICAMPO = Format(FuoriCampo, "0.00")
        TR.CLIENTE = ""

        TR.IPC = ""

        TR.DENOMIN1 = Trim(Mid(Dati.DO11_RAGIONESOCIALE & Space(35), 1, 35))
        TR.DENOMIN2 = ""
        If Len(Dati.DO11_RAGIONESOCIALE) > 35 Then
            TR.DENOMIN2 = Trim(Mid(Dati.DO11_RAGIONESOCIALE & Space(100), 36, Len(Dati.DO11_RAGIONESOCIALE) - 35))
        End If
        TR.INDIRIZZO = Dati.DO11_INDIRIZZO
        TR.CITTA = Dati.DO11_CITTA_NOME
        TR.CAP = Dati.DO11_CAP
        TR.PROV = Dati.DO11_PROVINCIA
        TR.PIVA = Dati.DO11_PIVA
        TR.CODFISC = Dati.DO11_CF

        TR.ABI = Dati.DO11_BANCA_ABI
        TR.CAB = Dati.DO11_BANCA_CAB

        TR.LINGUA = ""
        TR.CONTOCREDITO = "" ' CONTO DA INSERIRE
        TR.CASSABANCA = ""
        TR.STRUTTURA = CentroServizio.CODSSA

        TR.NOTE1 = "Retta mensile " & Reg.MeseCompetenza & "/" & Reg.AnnoCompetenza
        TR.NOTE2 = Dati.DO11_RAGIONESOCIALE

        TR.DESTINATARIO = ""
        TR.DENOM_DES = Dati.DES_RAGIONESOCIALE
        TR.VIA_DES = Dati.DES_INDIRIZZO
        TR.CAP_DES = Dati.DES_CAP

        TR.CITTA_DES = Dati.DO11_CITTA_NOME
        TR.PROV_DES = Dati.DO11_PROVINCIA

        TR.PIVA_DES = Dati.DES_PIVA
        TR.CODFISC_DES = Dati.DES_CF
        TR.STATO = 2

        Try

        Catch ex As Exception

        End Try

        REM NAZIONE DI NASCITA O RESIDENZA??
        If Mid(Dati.DO11_CITTA_ISTAT & Space(10), 1, 3) <> "999" Then
            TR.NAZIONE = "99100"
            TR.NAZIONE_DES = "ITALIA"
        Else
            TR.NAZIONE = "99" & Mid(Dati.DO11_CITTA_ISTAT & Space(10), 4, 3)
            TR.NAZIONE_DES = Dati.DO11_CITTA_NOME
        End If

        If CausaleContabile.TipoDocumento = "NC" Then
            TR.CODIMP_COL = "" 'CODICE CHE DOVREBBERO COMUNICARE
            TR.TIPODOC_COL = "FA"
            TR.ESERCIZIO_COL = Year(Reg.RifData)
            TR.NUMDOC_COL = Reg.RifNumero

        End If

        TR.CODSEZ = ""
        If Session("DC_OSPITE").ToString.ToUpper.IndexOf("MARIA") >= 0 Then
            TR.CODSEZ = "17"
        Else
            TR.CODSEZ = "18"
        End If
        TR.NDOCIVA = Reg.NumeroProtocollo

        TR.FL_OPPOSIZIONE = Dati.DO11_OPPOSIZIONE
        TR.COD_ASL = CentroServizio.CODASL
        TR.COD_REGIONE = CentroServizio.CODREGIONE
        TR.COD_SSA = CentroServizio.CODSSA

        TR.UFFRICH = ""


        Dim MaxImporto As Double = 0
        Dim MaxRiga As Integer = 0

        For Riga = 0 To 100
            If Not IsNothing(Reg.Righe(Riga)) Then
                If Reg.Righe(Riga).Tipo = "" Then
                    If Reg.Righe(Riga).Importo > MaxImporto Then
                        MaxImporto = Reg.Righe(Riga).Importo
                        MaxRiga = Riga
                    End If
                End If

            End If
        Next

        TabTrascodifica.TIPOTAB = "CT"
        TabTrascodifica.SENIOR = Reg.Righe(MaxRiga).MastroPartita & "." & Reg.Righe(MaxRiga).ContoPartita & "." & Reg.Righe(MaxRiga).SottocontoPartita
        TabTrascodifica.EXPORT = ""
        TabTrascodifica.Leggi(Session("DC_TABELLE"))



        TR.CODCONT = TabTrascodifica.EXPORT
        If TR.CODCONT = "IS" Then
            TR.CLASCONT = "GE"
        Else
            TR.CLASCONT = "TT"
        End If

        TR.MOD_INVIO = "SDI_PEC"
        TR.CODICEIPA = "0000000"



        ScriviTesta(TR, File)


        Dim IndiceRiga As Integer = 0
        For Riga = 0 To 100
            If Not IsNothing(Reg.Righe(Riga)) Then
                If Reg.Righe(Riga).Tipo = "" Then
                    IndiceRiga = IndiceRiga + 1
                    Dim RigaFat As New Riga

                    RigaFat.CODIMP = TR.CODIMP
                    RigaFat.TIPODOC = TR.TIPODOC
                    RigaFat.ESERCIZIO = TR.ESERCIZIO
                    RigaFat.NUM_DOC = TR.NUMDOC
                    RigaFat.RIGA = IndiceRiga
                    RigaFat.CODPREST = ""

                    RigaFat.DES_PREST = Trim(Mid(Reg.Righe(Riga).Descrizione & Space(200), 190)).Replace(";", "")
                    If RigaFat.DES_PREST = "" Then
                        RigaFat.DES_PREST = "Retta mensile " & Reg.MeseCompetenza & "/" & Reg.AnnoCompetenza
                    End If
                    If Reg.Righe(Riga).Quantita = 0 Then
                        Reg.Righe(Riga).Quantita = 1
                    End If
                    RigaFat.QUANTITA = Reg.Righe(Riga).Quantita
                    RigaFat.PREZZO = Format(Math.Round(Reg.Righe(Riga).Importo / Reg.Righe(Riga).Quantita, 6), "0.00")
                    RigaFat.SCONTO = "0,00"


                    RigaFat.TIPO_RIGA = "T"
                    RigaFat.CODTAR = "99"

                    Dim IVA As New Cls_IVA

                    IVA.Codice = Reg.Righe(Riga).CodiceIVA
                    IVA.Leggi(Session("DC_TABELLE"), IVA.Codice)

                    If Math.Round(Reg.Righe(Riga).Importo * IVA.Aliquota, 6) = 0 Then
                        RigaFat.IMPOSTA = "0,00"
                    Else
                        RigaFat.IMPOSTA = Format(Math.Round(Reg.Righe(Riga).Importo * IVA.Aliquota, 6), "0.00")
                    End If
                    RigaFat.IMPORTO = Format(Reg.Righe(Riga).Importo, "0.00")
                    TabTrascodifica.TIPOTAB = "IV"
                    TabTrascodifica.SENIOR = Reg.Righe(Riga).CodiceIVA
                    TabTrascodifica.EXPORT = 0
                    TabTrascodifica.Leggi(Session("DC_TABELLE"))

                    RigaFat.IVA = TabTrascodifica.EXPORT

                    TabTrascodifica.TIPOTAB = "CC"
                    TabTrascodifica.SENIOR = Reg.Righe(Riga).MastroPartita & "." & Reg.Righe(Riga).ContoPartita & "." & Reg.Righe(Riga).SottocontoPartita
                    TabTrascodifica.EXPORT = 0
                    TabTrascodifica.Leggi(Session("DC_TABELLE"))

                    RigaFat.CONTO = TabTrascodifica.EXPORT

                    RigaFat.CAPITOLO = TabTrascodifica.EXPORT


                    TabTrascodifica.TIPOTAB = "CS"
                    TabTrascodifica.SENIOR = Reg.CausaleContabile
                    TabTrascodifica.EXPORT = 0
                    TabTrascodifica.Leggi(Session("DC_TABELLE"))

                    RigaFat.TIPO_SPESA = TabTrascodifica.EXPORT

                    ScriviRiga(RigaFat, FileRiga)


                End If
            End If
        Next



    End Sub

    Private Sub Estrazione()
        Dim cn As OleDbConnection


        cn = New Data.OleDb.OleDbConnection(Session("DC_GENERALE"))
        cn.Open()
        Dim Param As New Cls_DatiGenerali


        Param.LeggiDati(Session("DC_TABELLE"))



        Tabella.Clear()
        Tabella.Columns.Clear()
        Tabella.Columns.Add("NumeroRegistrazione", GetType(String))
        Tabella.Columns.Add("DataRegistrazione", GetType(String))
        Tabella.Columns.Add("NumeroDocumento", GetType(String))
        Tabella.Columns.Add("Causale", GetType(String))
        Tabella.Columns.Add("Intestatario", GetType(String))
        Tabella.Columns.Add("Importo", GetType(String))




        Dim cmd As New OleDbCommand()
        '(select count(*) from MovimentiContabiliRiga Where Numero = MovimentiContabiliTesta.NumeroRegistrazione  And RigaBollato = 1) =  0 And

        If Param.CausaleRettifica = "" Then
            cmd.CommandText = "Select * From MovimentiContabiliTesta where DataRegistrazione >= ? And DataRegistrazione <= ? And ((Select COUNT(*) from movimenticontabiliriga where NUMERO = NUMEROREGISTRAZIONE AND  (RigaBollato = 0 or RigaBollato IS null)) > 0) And (RegistroIva =0 or RegistroIva =1 or RegistroIva is null) order by  (Select Top 1 CodiceDocumento  From TabellaLegami where CodicePagamento = NumeroRegistrazione  Order by CodiceDocumento),AnnoProtocollo,NumeroProtocollo,DataRegistrazione"
        Else
            cmd.CommandText = "Select * From MovimentiContabiliTesta where CausaleContabile <> '" & Param.CausaleRettifica & "' And DataRegistrazione >= ? And DataRegistrazione <= ? And ((Select COUNT(*) from movimenticontabiliriga where NUMERO = NUMEROREGISTRAZIONE AND  (RigaBollato = 0 or RigaBollato IS null)) > 0) And (RegistroIva =0 or RegistroIva =1 or RegistroIva is null) order by  (Select Top 1 CodiceDocumento  From TabellaLegami where CodicePagamento = NumeroRegistrazione  Order by CodiceDocumento),AnnoProtocollo,NumeroProtocollo,DataRegistrazione"
        End If
        cmd.Parameters.AddWithValue("@DataDal", Txt_DataDal.Text)
        cmd.Parameters.AddWithValue("@DataAl", Txt_DataAl.Text)
        cmd.Connection = cn
        Dim MyRsDB As OleDbDataReader = cmd.ExecuteReader()
        Do While MyRsDB.Read
            Dim myriga As System.Data.DataRow = Tabella.NewRow()

            Dim NReg As New Cls_MovimentoContabile

            NReg.NumeroRegistrazione = campodbN(MyRsDB.Item("NumeroRegistrazione"))
            NReg.Leggi(Session("DC_GENERALE"), NReg.NumeroRegistrazione)

            Dim Causale As New Cls_CausaleContabile

            Causale.Codice = NReg.CausaleContabile
            Causale.Leggi(Session("DC_TABELLE"), Causale.Codice)

            myriga(0) = NReg.NumeroRegistrazione
            myriga(1) = Format(NReg.DataRegistrazione, "dd/MM/yyyy")
            myriga(2) = NReg.NumeroDocumento
            myriga(3) = Causale.Descrizione

            Dim PInc As New Cls_Pianodeiconti

            PInc.Mastro = NReg.Righe(0).MastroPartita
            PInc.Conto = NReg.Righe(0).ContoPartita
            PInc.Sottoconto = NReg.Righe(0).SottocontoPartita
            PInc.Decodfica(Session("DC_GENERALE"))

            myriga(4) = PInc.Descrizione

            myriga(5) = Format(NReg.ImportoRegistrazioneDocumento(Session("DC_TABELLE")), "#,##0.00")



            Tabella.Rows.Add(myriga)
        Loop


        cn.Close()


        ViewState("App_AddebitiMultiplo") = Tabella

        GridView1.AutoGenerateColumns = False

        GridView1.DataSource = Tabella
        GridView1.DataBind()
        GridView1.Visible = True

        Dim ParamOsp As New Cls_Parametri


        ParamOsp.LeggiParametri(Session("DC_OSPITE"))

        If ParamOsp.TipoExport = "Documenti Incassi" And Chk_Prova.Checked = False Then
            Btn_Export.ImageUrl = "~/images/sendmail.png"
            Btn_Export.Visible = True
        Else
            Btn_Export.ImageUrl = "~/images/download.png"
            Btn_Export.Visible = True
        End If

    End Sub


    Function campodb(ByVal oggetto As Object) As String
        If IsDBNull(oggetto) Then
            Return ""
        Else
            Return oggetto
        End If
    End Function
    Function campodbN(ByVal oggetto As Object) As Double
        If IsDBNull(oggetto) Then
            Return 0
        Else
            Return oggetto
        End If
    End Function

    Function campodbd(ByVal oggetto As Object) As Date
        If IsDBNull(oggetto) Then
            Return Nothing
        Else
            Return oggetto
        End If
    End Function

    Protected Sub Btn_Estrai_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles Btn_Estrai.Click
        Estrazione()
    End Sub

    Private Sub EseguiJS()
        Dim MyJs As String
        MyJs = "$(document).ready(function() {"

        MyJs = MyJs & "var els = document.getElementsByTagName(""*"");"

        MyJs = MyJs & "for (var i=0;i<els.length;i++)"
        MyJs = MyJs & "if ( els[i].id ) { "
        MyJs = MyJs & " var appoggio =els[i].id; "


        MyJs = MyJs & " if ((appoggio.match('Txt_Importo')!= null)) {  "
        MyJs = MyJs & " $(els[i]).keypress(function() { ForceNumericInput($(this).val(), true, true); } ); "
        MyJs = MyJs & " $(els[i]).blur(function() { var ap = formatNumber($(this).val(),2); $(this).val(ap); });"
        MyJs = MyJs & "    }"

        MyJs = MyJs & " if ((appoggio.match('Txt_Data')!= null) || (appoggio.match('Txt_Data')!= null)) {  "
        MyJs = MyJs & " $(els[i]).mask(""99/99/9999"");"
        MyJs = MyJs & " $(els[i]).datepicker({ changeMonth: true,changeYear: true,  yearRange: ""1990:" & Year(Now) + 5 & """},$.datepicker.regional[""it""]);"
        MyJs = MyJs & "    }"


        MyJs = MyJs & "} "



        MyJs = MyJs & "});"

        'MyJs = "$(document).ready(function() { $('.myClass').keypress(function() { handleEnter($('.myClass').val(), true, true); } );     $('#" & Txt_ImportoPagato.ClientID & "').blur(function() { var ap = formatNumber ($('#" & Txt_ImportoPagato.ClientID & "').val(),2); $('#" & Txt_ImportoPagato.ClientID & "').val(ap); }); });"
        ScriptManager.RegisterStartupScript(Me, Me.GetType(), "visualizzaRitIm", MyJs, True)
    End Sub
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If IsNothing(Session("DC_OSPITE")) Then
            Response.Redirect("..\Login.aspx")
            Exit Sub
        End If

        EseguiJS()

        Dim K1 As New Cls_SqlString


        Dim Barra As New Cls_BarraSenior

        If Not IsNothing(Session("RicercaAnagraficaSQLString")) Then
            K1 = Session("RicercaAnagraficaSQLString")
        End If

        Lbl_BarraSenior.Text = Barra.CodiceBarra(Application("SENIOR"), Session("UTENTE"), Page, K1)
    End Sub

    Protected Sub Btn_Seleziona_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles Btn_Seleziona.Click
        Dim Riga As Integer


        For Riga = 0 To GridView1.Rows.Count - 1

            Dim CheckBox As CheckBox = DirectCast(GridView1.Rows(Riga).FindControl("ChkImporta"), CheckBox)


            If CheckBox.Enabled = True Then
                CheckBox.Checked = True
            End If
        Next
    End Sub




    Function AdattaLunghezzaNumero(ByVal Testo As String, ByVal Lunghezza As Integer) As String
        If Len(Testo) > Lunghezza Then
            AdattaLunghezzaNumero = Mid(Testo, 1, Lunghezza)
            Exit Function
        End If
        AdattaLunghezzaNumero = Replace(Space(Lunghezza - Len(Testo)) & Testo, " ", "0")
        REM AdattaLunghezzaNumero = Space(Lunghezza - Len(Testo)) & Testo


    End Function

    Function AdattaLunghezza(ByVal Testo As String, ByVal Lunghezza As Integer) As String

        If Len(Testo) > Lunghezza Then
            Testo = Mid(Testo, 1, Lunghezza)
        End If

        AdattaLunghezza = Testo & Space(Lunghezza - Len(Testo) + 1)

        If Len(AdattaLunghezza) > Lunghezza Then
            AdattaLunghezza = Mid(AdattaLunghezza, 1, Lunghezza)
        End If

    End Function

  

    Protected Sub Btn_Export_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles Btn_Export.Click
        Call Export_DocumentiIncassi()
    End Sub

    Protected Sub Btn_Esci_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles Btn_Esci.Click
        Response.Redirect("../Menu_Export.aspx")
    End Sub


    Protected Sub Btn_DownloadIncassi_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles Btn_DownloadIncassi.Click
        Dim NomeFile As String

        NomeFile = HostingEnvironment.ApplicationPhysicalPath() & "\Public\Incassi_" & Format(Session("MYDATE"), "yyyyMMddHHmmss") & ".Txt"

        Try

            Response.Clear()
            Response.Buffer = True
            Response.ContentType = "text/plain"
            Response.AppendHeader("content-disposition", "attachment;filename=impfata.Txt")
            Response.WriteFile(NomeFile)
            Response.Flush()
            Response.End()
        Catch ex As Exception

        Finally
            Try
                Kill(NomeFile)
            Catch ex As Exception

            End Try
        End Try
    End Sub

    Protected Sub Btn_DownloadTesta_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles Btn_DownloadTesta.Click
        Dim NomeFile As String

        NomeFile = HostingEnvironment.ApplicationPhysicalPath() & "\Public\Documenti_" & Format(Session("MYDATE"), "yyyyMMddHHmmss") & ".Txt"

        Try

            Response.Clear()
            Response.Buffer = True
            Response.ContentType = "text/plain"
            Response.AppendHeader("content-disposition", "attachment;filename=impfat.Txt")
            Response.WriteFile(NomeFile)
            Response.Flush()
            Response.End()
        Catch ex As Exception

        Finally
            Try
                Kill(NomeFile)
            Catch ex As Exception

            End Try
        End Try
    End Sub

    Protected Sub Btn_DownloadRiga_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles Btn_DownloadRiga.Click
        Dim NomeFile As String

        NomeFile = HostingEnvironment.ApplicationPhysicalPath() & "\Public\DocumentiRiga_" & Format(Session("MYDATE"), "yyyyMMddHHmmss") & ".Txt"

        Try

            Response.Clear()
            Response.Buffer = True
            Response.ContentType = "text/plain"
            Response.AppendHeader("content-disposition", "attachment;filename=impfatd.Txt")
            Response.WriteFile(NomeFile)
            Response.Flush()
            Response.End()
        Catch ex As Exception

        Finally
            Try
                Kill(NomeFile)
            Catch ex As Exception

            End Try
        End Try
    End Sub

    Protected Sub Btn_DownloadCSV_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles Btn_DownloadCSV.Click
        Dim NomeFile As String

        NomeFile = HostingEnvironment.ApplicationPhysicalPath() & "\Public\CSV_" & Format(Session("MYDATE"), "yyyyMMddHHmmss") & ".Txt"

        Try

            Response.Clear()
            Response.Buffer = True
            Response.ContentType = "text/plain"
            Response.AppendHeader("content-disposition", "attachment;filename=EXPORT.CSV")
            Response.WriteFile(NomeFile)
            Response.Flush()
            Response.End()
        Catch ex As Exception

        Finally
            Try
                Kill(NomeFile)
            Catch ex As Exception

            End Try
        End Try
    End Sub


    Public Function FtpFileExists(ByVal Address As String, ByVal UserName As String, ByVal Password As String) As FtpResult
        Try
            Dim request As System.Net.FtpWebRequest = DirectCast(System.Net.WebRequest.Create(Address), System.Net.FtpWebRequest)
            request.Credentials = New System.Net.NetworkCredential(UserName, Password)
            request.GetResponse()
            Return FtpResult.Exists
        Catch ex As Exception
            If UserName = "" Then Return FtpResult.BlankUserName
            If Password = "" Then Return FtpResult.BlankPassword
            If ex.Message.IndexOf("530") > 0 Then Return FtpResult.InvalidLogin
            If Address.ToLower.IndexOf("ftp://ftp") = -1 Then Return FtpResult.InvalidUrl
            If Address = String.Empty Then Return FtpResult.InvalidUrl
            Return FtpResult.DoesNotExist
        End Try
    End Function

    Enum FtpResult
        Exists
        DoesNotExist
        InvalidLogin
        InvalidUrl
        BlankPassword
        BlankUserName
    End Enum


    Public Sub UploadFile(ByVal _FileName As String, ByVal _UploadPath As String, ByVal _FTPUser As String, ByVal _FTPPass As String)
        Dim _FileInfo As New System.IO.FileInfo(_FileName)

        ' Create FtpWebRequest object from the Uri provided
        Dim _FtpWebRequest As System.Net.FtpWebRequest = CType(System.Net.FtpWebRequest.Create(New Uri(_UploadPath)), System.Net.FtpWebRequest)

        ' Provide the WebPermission Credintials
        _FtpWebRequest.Credentials = New System.Net.NetworkCredential(_FTPUser, _FTPPass)

        ' By default KeepAlive is true, where the control connection is not closed
        ' after a command is executed.
        _FtpWebRequest.KeepAlive = False

        ' set timeout for 20 seconds
        _FtpWebRequest.Timeout = 20000

        ' Specify the command to be executed.
        _FtpWebRequest.Method = System.Net.WebRequestMethods.Ftp.UploadFile

        ' Specify the data transfer type.
        _FtpWebRequest.UseBinary = True

        ' Notify the server about the size of the uploaded file
        _FtpWebRequest.ContentLength = _FileInfo.Length

        ' The buffer size is set to 2kb
        Dim buffLength As Integer = 2048
        Dim buff(buffLength - 1) As Byte

        ' Opens a file stream (System.IO.FileStream) to read the file to be uploaded
        Dim _FileStream As System.IO.FileStream = _FileInfo.OpenRead()


        ' Stream to which the file to be upload is written
        Dim _Stream As System.IO.Stream = _FtpWebRequest.GetRequestStream()

        ' Read from the file stream 2kb at a time
        Dim contentLen As Integer = _FileStream.Read(buff, 0, buffLength)

        ' Till Stream content ends
        Do While contentLen <> 0
            ' Write Content from the file stream to the FTP Upload Stream
            _Stream.Write(buff, 0, contentLen)
            contentLen = _FileStream.Read(buff, 0, buffLength)
        Loop

        ' Close the file stream and the Request Stream
        _Stream.Close()
        _Stream.Dispose()
        _FileStream.Close()
        _FileStream.Dispose()

    End Sub

End Class
