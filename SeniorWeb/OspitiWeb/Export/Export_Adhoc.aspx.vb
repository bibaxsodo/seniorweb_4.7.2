﻿Imports Microsoft.VisualBasic
Imports System.Data.OleDb
Imports System.Web.Hosting
Imports System.Diagnostics
Imports System.ComponentModel
Imports System.Threading
Imports System.IO.Compression
Partial Class OspitiWeb_Export_Export_Adhoc
    Inherits System.Web.UI.Page
    Dim MyTable As New System.Data.DataTable("tabella")
    Dim Tabella As New System.Data.DataTable("EstraiTabella")
    Dim MyDataSet As New System.Data.DataSet()
    Dim Vt_Cliente(1000) As String
    'luca.destefani@saserviziassociati.it

    Class Anagrafica
        Public CODICEEXPORT As String = ""
        Public DO11_RAGIONESOCIALE As String = ""
        Public DO11_PIVA As String = ""
        Public DO11_CF As String = ""
        Public DO11_INDIRIZZO As String = ""
        Public DO11_CAP As String = ""
        Public DO11_CITTA_NOME As String = ""
        Public DO11_CITTA_ISTAT As String = ""
        Public DO11_PROVINCIA As String = ""
        Public DO11_CONDPAG_CODICE_SEN As String = ""
        Public DO11_CONDPAG_NOME_SEN As String = ""
        Public DO11_BANCA_CIN As String = ""
        Public DO11_BANCA_ABI As String = ""
        Public DO11_BANCA_CAB As String = ""
        Public DO11_BANCA_CONTO As String = ""
        Public DO11_BANCA_IBAN As String = ""
        Public DO11_BANCA_IDMANDATO As String = ""
        Public DO11_BANCA_DATAMANDATO As String = ""
        Public DO11_OPPOSIZIONE As String = ""



        Public DES_RAGIONESOCIALE As String = ""
        Public DES_PIVA As String = ""
        Public DES_CF As String = ""
        Public DES_INDIRIZZO As String = ""
        Public DES_CAP As String = ""
        Public DES_CITTA_NOME As String = ""
        Public DES_CITTA_ISTAT As String = ""
        Public DES_PROVINCIA As String = ""


        Public DES_COGNOME As String = ""
        Public DES_NOME As String = ""

        Public AddDescrizione As String
    End Class


    Class ADDOCUME
        Public TipoDocumento As String
        Public DataDocumento As String
        
        Public AlfaDocumento As String
        Public NumeroDocumento As Long


        Public FlagTipo As String
        Public Anno As String
        Public DataCompetenza As String
        Public CausaleDocumento As String
        Public filler As String
        Public CodiceMagazzino As String
        Public Magazzino As String
        Public Collegamento As String
        Public CodiceCliente As String
        Public CodiceFornitore As String
        Public FlagEsenzione As String
        Public CodiceAgente As String
        Public CodiceListino As String

        Public CodicePagamento As String

        Public DataDiversa As String

        Public CodiceBanca As String
        Public CodiceValuta As String

        Public CambioGiornaliero As String
        Public Spedizione As String
        Public Vettore As String
        Public PrimoSconto As String
        Public SecondoSconto As String
        Public ScontoSu As String
        Public SpeseIncasso As String
        Public CampiRiga As String
        Public Acconto As String
        Public TipoConto As String
        Public CodiceConto As String

        Public CodicePag As String
        Public FlagScorporo As String

    End Class


    Class ADDOCUMERiga
        Public TipoDocumento As String
        Public DataDocumento As String

        Public AlfaDocumento As String
        Public NumeroDocumento As String

        Public Filler As String

        Public CausaleDocument As String

        Public Filler2 As String

        Public CodiceMagazzino As String

        Public Filler3 As String

        Public CodiceCliente As String
        Public CodiceFornitore As String
        Public Filler4 As String
        Public Listino As String
        Public Filler5 As String
        Public CodiceValuta As String
        Public Filler6 As String
        Public TipoRiga As String

        Public CodiceArticolo As String
        Public CodArtXLink As String
        Public Contropartita As String
        Public UnitadiMisura As String
        Public Descrizione As String
        Public QtaMovimentata As String
        Public MVQTAUN1 As Double
        Public PrezzoUnitario As String
        Public PrimoSconto As String
        Public SecondoSconto As String
        Public ValoreUnitario As String
        Public FlagOmaggio As String
        Public CodiceIVA As String
        Public AliquotaIva As String
        Public TotaleNettoRIga As String
        Public TotaleinValuta As String
        Public FlagDate As String
        Public ArticoloFonitore As String
        Public FlagAggiornaDati As String
        Public Percentuale As String
        Public PesoNetto As String
        Public Nomenclatura As String
        Public um As String
        Public FlagSpesa As String
        Public TipoConto As String
        Public CodiceConto As String
        Public Centroricavo As String
        Public Classedocumento As String
        Public MVCODIVE As String
        Public MVFLVEAC As String
        Public MVVOCCEN As String
    End Class


    Class ADCLIFOR
        Public Tiporecord As String
        Public Indicatore As String
        Public Codice As String
        Public ANCODICE As String
        Public ANCODICE2 As String

        Public Descrizione As String
        Public Descrizione2 As String
        Public Indirizzo As String
        Public Indirizzoaggiuntivo As String
        Public cap As String
        Public Localita As String
        Public Provincia As String

        Public Telefono As String
        Public Cellulare As String

        Public Fax As String
        Public Personafisica As String
        Public Sesso As String
        Public Datanascita As String
        Public Luogodinascita As String
        Public Provinciadinascita As String
        Public Codicefiscale As String
        Public PartitaIva As String
        Public SottocontoClienti As String

        Public Scadenziario As String
        Public Codicepagamento As String
        Public Codicepagamento2 As String

        Public Codicezona As String
        Public Codicezona2 As String

        Public Codiceagente1 As String
        Public Codiceagente2 As String

        Public CodiceIvanon1 As String
        Public CodiceIvanon2 As String
        Public CodiceValuta1 As String
        Public CodiceValuta2 As String

        Public CodiceBanca As String
        Public PrimoMese As String
        Public PrimoggScadenza As String
        Public Miofiller As String
        Public Tipofatturazione As String
        Public BolliInFattura As String
        Public PrezziInBolla As String
        Public IndicatoreInvio As String
        Public TestScorporo As String
        Public CodiceLingua As String
        Public NonUtilizzo As String
        Public Annotazioni As String
        Public IndicatoreBlocco As String
        Public NonUtilizzato As String
        Public IndirizzoInternet As String
        Public IndirizzoMail As String
        Public Ritenute As String
        Public CategoriaContabili As String
        Public CodiceStudio As String
        Public FlagPrivato As String
        Public Cognome As String
        Public Nome As String
        Public Nazione As String
    End Class

    Private Sub ScriviADDOCUME(ByVal Dati As ADDOCUME, ByVal File As System.IO.TextWriter)
        Dim Stringa As String = ""
        Stringa = ""
        Stringa = Stringa & AdattaLunghezza(Dati.TipoDocumento, 2)

        Stringa = Stringa & AdattaLunghezza(Dati.DataDocumento, 8)

        Stringa = Stringa & AdattaLunghezza(Dati.AlfaDocumento, 3)
        Stringa = Stringa & AdattaLunghezzaNumero(Dati.NumeroDocumento, 6)


        Stringa = Stringa & AdattaLunghezza(Dati.FlagTipo, 1)
        Stringa = Stringa & AdattaLunghezza(Dati.Anno, 4)
        Stringa = Stringa & AdattaLunghezza(Dati.DataCompetenza, 8)
        Stringa = Stringa & AdattaLunghezza(Dati.CausaleDocumento, 5)
        Stringa = Stringa & AdattaLunghezza(Dati.filler, 14)
        Stringa = Stringa & AdattaLunghezza(Dati.CodiceMagazzino, 2)
        Stringa = Stringa & AdattaLunghezza(Dati.Magazzino, 2)
        Stringa = Stringa & AdattaLunghezza(Dati.Collegamento, 12)
        Stringa = Stringa & AdattaLunghezza(Dati.CodiceCliente, 10)
        Stringa = Stringa & AdattaLunghezza(Dati.CodiceFornitore, 7)
        Stringa = Stringa & AdattaLunghezza(Dati.FlagEsenzione, 1)
        Stringa = Stringa & AdattaLunghezza(Dati.CodiceAgente, 3)
        Stringa = Stringa & AdattaLunghezza(Dati.CodiceListino, 3)

        Stringa = Stringa & AdattaLunghezza(Dati.CodicePagamento, 3)

        Stringa = Stringa & AdattaLunghezza(Dati.DataDiversa, 8)

        Stringa = Stringa & AdattaLunghezza(Dati.CodiceBanca, 7)
        Stringa = Stringa & AdattaLunghezza(Dati.CodiceValuta, 3)

        Stringa = Stringa & AdattaLunghezzaNumero(Dati.CambioGiornaliero, 12)
        Stringa = Stringa & AdattaLunghezza(Dati.Spedizione, 3)
        Stringa = Stringa & AdattaLunghezza(Dati.Vettore, 3)
        Stringa = Stringa & AdattaLunghezzaNumero(Dati.PrimoSconto, 5)
        Stringa = Stringa & AdattaLunghezzaNumero(Dati.SecondoSconto, 5)
        Stringa = Stringa & AdattaLunghezzaNumero(Dati.ScontoSu, 5)
        Stringa = Stringa & AdattaLunghezzaNumero(Dati.SpeseIncasso, 15)
        Stringa = Stringa & AdattaLunghezza(Dati.CampiRiga, 212)
        Stringa = Stringa & AdattaLunghezza(Dati.Acconto, 15)
        Stringa = Stringa & AdattaLunghezza(Dati.TipoConto, 1)
        Stringa = Stringa & AdattaLunghezza(Dati.CodiceConto, 10)

        Stringa = Stringa & AdattaLunghezza(Dati.CodicePag, 5)
        Stringa = Stringa & AdattaLunghezza(Dati.FlagScorporo, 1)

        File.WriteLine(Stringa)

    End Sub



    Private Sub ScriviADDOCUMERiga(ByVal Dati As ADDOCUMERiga, ByVal DatiTesta As ADDOCUME, ByVal File As System.IO.TextWriter)
        Dim Stringa As String = ""

        Stringa = ""

        Stringa = Stringa & AdattaLunghezza(DatiTesta.TipoDocumento, 2)

        Stringa = Stringa & AdattaLunghezza(DatiTesta.DataDocumento, 8)

        Stringa = Stringa & AdattaLunghezza(DatiTesta.AlfaDocumento, 3)
        Stringa = Stringa & AdattaLunghezzaNumero(DatiTesta.NumeroDocumento, 6)


        Stringa = Stringa & AdattaLunghezza(DatiTesta.FlagTipo, 1)
        Stringa = Stringa & AdattaLunghezza(DatiTesta.Anno, 4)
        Stringa = Stringa & AdattaLunghezza(DatiTesta.DataCompetenza, 8)
        Stringa = Stringa & AdattaLunghezza(DatiTesta.CausaleDocumento, 5)
        Stringa = Stringa & AdattaLunghezza(DatiTesta.filler, 14)
        Stringa = Stringa & AdattaLunghezza(DatiTesta.CodiceMagazzino, 2)
        Stringa = Stringa & AdattaLunghezza(DatiTesta.Magazzino, 2)
        Stringa = Stringa & AdattaLunghezza(DatiTesta.Collegamento, 12)
        Stringa = Stringa & AdattaLunghezzaNumero(DatiTesta.CodiceCliente, 10)
        Stringa = Stringa & AdattaLunghezza(DatiTesta.CodiceFornitore, 7)
        Stringa = Stringa & AdattaLunghezza(DatiTesta.FlagEsenzione, 1)
        Stringa = Stringa & AdattaLunghezza(DatiTesta.CodiceAgente, 3)
        Stringa = Stringa & AdattaLunghezza(DatiTesta.CodiceListino, 3)

        Stringa = Stringa & AdattaLunghezza(DatiTesta.CodicePagamento, 3)

        Stringa = Stringa & AdattaLunghezza(DatiTesta.DataDiversa, 8)

        Stringa = Stringa & AdattaLunghezza(DatiTesta.CodiceBanca, 7)
        Stringa = Stringa & AdattaLunghezza(DatiTesta.CodiceValuta, 3)

        Stringa = Stringa & AdattaLunghezzaNumero(DatiTesta.CambioGiornaliero, 12)
        Stringa = Stringa & AdattaLunghezza(DatiTesta.Spedizione, 3)
        Stringa = Stringa & AdattaLunghezza(DatiTesta.Vettore, 3)
        Stringa = Stringa & AdattaLunghezzaNumero(DatiTesta.PrimoSconto, 5)
        Stringa = Stringa & AdattaLunghezzaNumero(DatiTesta.SecondoSconto, 5)
        Stringa = Stringa & AdattaLunghezzaNumero(DatiTesta.ScontoSu, 5)
        Stringa = Stringa & AdattaLunghezzaNumero(DatiTesta.SpeseIncasso, 15)



        Stringa = Stringa & AdattaLunghezza(Dati.TipoRiga, 1)

        Stringa = Stringa & AdattaLunghezza(Dati.CodiceArticolo, 15)
        Stringa = Stringa & AdattaLunghezza(Dati.CodArtXLink, 15)
        Stringa = Stringa & AdattaLunghezza(Dati.Contropartita, 10)
        Stringa = Stringa & AdattaLunghezza(Dati.UnitadiMisura, 2)
        Stringa = Stringa & AdattaLunghezza(Dati.Descrizione, 30)
        Stringa = Stringa & AdattaLunghezzaNumero(Dati.QtaMovimentata, 12)
        Stringa = Stringa & AdattaLunghezzaNumero(Dati.MVQTAUN1, 12)
        Stringa = Stringa & AdattaLunghezzaNumero(Dati.PrezzoUnitario, 15)
        Stringa = Stringa & AdattaLunghezzaNumero(Dati.PrimoSconto, 5)
        Stringa = Stringa & AdattaLunghezzaNumero(Dati.SecondoSconto, 5)
        Stringa = Stringa & AdattaLunghezzaNumero(Dati.ValoreUnitario, 15)
        Stringa = Stringa & AdattaLunghezza(Dati.FlagOmaggio, 1)
        Stringa = Stringa & AdattaLunghezza(Dati.CodiceIVA, 3)
        Stringa = Stringa & AdattaLunghezza(Dati.AliquotaIva, 5)
        Stringa = Stringa & AdattaLunghezzaNumero(Dati.TotaleNettoRIga, 15)
        Stringa = Stringa & AdattaLunghezzaNumero(Dati.TotaleinValuta, 15)
        Stringa = Stringa & AdattaLunghezza(Dati.FlagDate, 4)
        Stringa = Stringa & AdattaLunghezza(Dati.ArticoloFonitore, 15)
        Stringa = Stringa & AdattaLunghezza(Dati.FlagAggiornaDati, 1)
        Stringa = Stringa & AdattaLunghezzaNumero(Dati.Percentuale, 5)
        Stringa = Stringa & AdattaLunghezzaNumero(Dati.PesoNetto, 9)
        Stringa = Stringa & AdattaLunghezza(Dati.Nomenclatura, 8)
        Stringa = Stringa & AdattaLunghezza(Dati.um, 2)
        Stringa = Stringa & AdattaLunghezza(Dati.FlagSpesa, 1)
        Stringa = Stringa & AdattaLunghezzaNumero(DatiTesta.Acconto, 15)




        File.WriteLine(Stringa)

    End Sub


 


    Private Sub ScriviADCLIFOR(ByVal Dati As ADCLIFOR, ByVal File As System.IO.TextWriter)
        Dim Stringa As String = ""

        Stringa = ""
    
        Stringa = Stringa & AdattaLunghezza(Dati.Tiporecord, 1)
        Stringa = Stringa & AdattaLunghezza(Dati.Indicatore, 1)
        Stringa = Stringa & AdattaLunghezza(Dati.Codice, 20)
        
        Stringa = Stringa & AdattaLunghezza(Dati.Descrizione, 40)
        Stringa = Stringa & AdattaLunghezza(Dati.Descrizione2, 40)
        Stringa = Stringa & AdattaLunghezza(Dati.Indirizzo, 35)
        Stringa = Stringa & AdattaLunghezza(Dati.Indirizzoaggiuntivo, 35)
        Stringa = Stringa & AdattaLunghezza(Dati.cap, 5)
        Stringa = Stringa & AdattaLunghezza(Dati.Localita, 30)
        Stringa = Stringa & AdattaLunghezza(Dati.Provincia, 2)

        Stringa = Stringa & AdattaLunghezza(Dati.Telefono, 18)
        Stringa = Stringa & AdattaLunghezza(Dati.Cellulare, 18)

        Stringa = Stringa & AdattaLunghezza(Dati.Fax, 18)
        Stringa = Stringa & AdattaLunghezza(Dati.Personafisica, 1)
        Stringa = Stringa & AdattaLunghezza(Dati.Sesso, 1)
        Stringa = Stringa & AdattaLunghezza(Dati.Datanascita, 8)
        Stringa = Stringa & AdattaLunghezza(Dati.Luogodinascita, 30)
        Stringa = Stringa & AdattaLunghezza(Dati.Provinciadinascita, 2)
        Stringa = Stringa & AdattaLunghezza(Dati.Codicefiscale, 16)
        Stringa = Stringa & AdattaLunghezza(Dati.PartitaIva, 12)
        Stringa = Stringa & AdattaLunghezza(Dati.SottocontoClienti, 20)
        Stringa = Stringa & AdattaLunghezza(Dati.Scadenziario, 1)
        Stringa = Stringa & AdattaLunghezza(Dati.Scadenziario, 1)
        Stringa = Stringa & AdattaLunghezza(Dati.Codicepagamento, 20)
        REM Stringa = Stringa & AdattaLunghezza(DatiCodicepagamento2, 20)

        Stringa = Stringa & AdattaLunghezza(Dati.Codicezona, 20)
        REM Stringa = Stringa & AdattaLunghezza(Dati.Codicezona2, 20)

        Stringa = Stringa & AdattaLunghezza(Dati.Codiceagente1, 20)
        Stringa = Stringa & AdattaLunghezza(Dati.Codiceagente2, 20)

        Stringa = Stringa & AdattaLunghezza(Dati.CodiceIvanon1, 20)
        REM Stringa = Stringa & AdattaLunghezza(Dati.CodiceIvanon2, 20)
        Stringa = Stringa & AdattaLunghezza(Dati.CodiceValuta1, 20)
        REM Stringa = Stringa & AdattaLunghezza(Dati.CodiceValuta2, 20)

        Stringa = Stringa & AdattaLunghezza(Dati.CodiceBanca, 20)
        Stringa = Stringa & AdattaLunghezzaNumero(Dati.PrimoMese, 2)
        Stringa = Stringa & AdattaLunghezzaNumero(Dati.PrimoMese, 2)
        Stringa = Stringa & AdattaLunghezzaNumero(Dati.PrimoggScadenza, 2)
        Stringa = Stringa & AdattaLunghezza(Dati.Miofiller, 10)
        Stringa = Stringa & AdattaLunghezza(Dati.Tipofatturazione, 1)
        Stringa = Stringa & AdattaLunghezza(Dati.BolliInFattura, 1)
        Stringa = Stringa & AdattaLunghezza(Dati.PrezziInBolla, 1)
        Stringa = Stringa & AdattaLunghezza(Dati.IndicatoreInvio, 1)
        Stringa = Stringa & AdattaLunghezza(Dati.TestScorporo, 1)
        Stringa = Stringa & AdattaLunghezza(Dati.CodiceLingua, 20)
        Stringa = Stringa & AdattaLunghezza(Dati.NonUtilizzo, 15)
        Stringa = Stringa & AdattaLunghezza(Dati.Annotazioni, 255)
        Stringa = Stringa & AdattaLunghezza(Dati.IndicatoreBlocco, 1)
        Stringa = Stringa & AdattaLunghezza(Dati.NonUtilizzato, 1)
        Stringa = Stringa & AdattaLunghezza(Dati.IndirizzoInternet, 40)
        Stringa = Stringa & AdattaLunghezza(Dati.IndirizzoMail, 50)
        Stringa = Stringa & AdattaLunghezza(Dati.Ritenute, 1)
        'Stringa = Stringa & AdattaLunghezza(Dati.CategoriaContabili, 5)
        'Stringa = Stringa & AdattaLunghezza(Dati.CodiceStudio, 20)
        Stringa = Stringa & AdattaLunghezza(Dati.FlagPrivato, 1)
        Stringa = Stringa & AdattaLunghezza(Dati.Cognome, 50)
        Stringa = Stringa & AdattaLunghezza(Dati.Nome, 50)
        Stringa = Stringa & AdattaLunghezza(Dati.Nazione, 3)

        File.WriteLine(Stringa)


    End Sub




    Private Sub DecodificaAnagrafica(ByRef Dati As Anagrafica, ByVal Registrazione As Cls_MovimentoContabile)
        Dim cn As OleDbConnection
        Dim Cliente As String
        Dim CodiceOspite As Integer
        Dim CodiceParente As Integer
        Dim CodiceProvincia As String
        Dim CodiceComune As String
        Dim CodiceRegione As String
        Dim Tipo As String

        cn = New Data.OleDb.OleDbConnection(Session("DC_GENERALE"))
        cn.Open()
        Tipo = Registrazione.Tipologia

        Dim cmdRd As New OleDbCommand()
        cmdRd.CommandText = "Select * From MovimentiContabiliRiga Where Numero = ?  And Tipo ='CF'   "
        cmdRd.Connection = cn
        cmdRd.Parameters.AddWithValue("@Numero", Registrazione.NumeroRegistrazione)
        Dim MyReadSC As OleDbDataReader = cmdRd.ExecuteReader()
        If MyReadSC.Read Then
            Cliente = campodb(MyReadSC.Item("MastroPartita")) & "." & campodb(MyReadSC.Item("ContoPartita")) & "." & campodb(MyReadSC.Item("SottocontoPartita"))
            If Mid(Tipo & Space(10), 1, 1) = " " Then
                CodiceOspite = Int(campodbN(MyReadSC.Item("SottocontoPartita")) / 100)
                CodiceParente = campodbN(MyReadSC.Item("SottocontoPartita")) - (CodiceOspite * 100)

                If CodiceParente = 0 And CodiceOspite > 0 Then
                    Tipo = "O" & Space(10)
                End If
                If CodiceParente > 0 And CodiceOspite > 0 Then
                    Tipo = "P" & Space(10)
                End If
            End If
            If Mid(Tipo & Space(10), 1, 1) = "C" Then
                CodiceProvincia = Mid(Tipo & Space(10), 2, 3)
                CodiceComune = Mid(Tipo & Space(10), 5, 3)
            End If
            If Mid(Tipo & Space(10), 1, 1) = "J" Then
                CodiceProvincia = Mid(Tipo & Space(10), 2, 3)
                CodiceComune = Mid(Tipo & Space(10), 5, 3)
            End If
            If Mid(Tipo & Space(10), 1, 1) = "R" Then
                CodiceRegione = Mid(Tipo & Space(10), 2, 4)
            End If

        End If
        MyReadSC.Close()
        cn.Close()

        Dim Servizio As New Cls_CentroServizio

        Servizio.CENTROSERVIZIO = Registrazione.CentroServizio
        Servizio.Leggi(Session("DC_OSPITE"), Servizio.CENTROSERVIZIO)


        Dati.AddDescrizione = ""
        If Mid(Tipo & Space(10), 1, 1) = "O" Then
            Dim Ospite As New ClsOspite

            Ospite.CodiceOspite = CodiceOspite
            Ospite.Leggi(Session("DC_OSPITE"), Ospite.CodiceOspite)

            Dati.CODICEEXPORT = Ospite.CONTOPERESATTO
            If Dati.CODICEEXPORT = "" Then
                Dim CodiceConto As Long

                CodiceConto = (Servizio.CONTO * 1000) + (Ospite.CodiceOspite * 10)
                Dati.CODICEEXPORT = "C" & CodiceConto
            End If
            Dati.DO11_RAGIONESOCIALE = Mid(Ospite.CognomeOspite & Space(30), 1, 30) & Ospite.NomeOspite
            Dati.DO11_PIVA = ""
            Dati.DO11_CF = Ospite.CODICEFISCALE
            Dati.DO11_INDIRIZZO = Ospite.RESIDENZAINDIRIZZO1
            Dati.DO11_CAP = Ospite.RESIDENZACAP1
            Dati.DES_COGNOME = Ospite.CognomeOspite
            Dati.DES_NOME = Ospite.NomeOspite

            If Ospite.Opposizione730 = 1 Then
                Dati.DO11_OPPOSIZIONE = "S"
            Else
                Dati.DO11_OPPOSIZIONE = "N"
            End If
            Dim DcCom As New ClsComune

            DcCom.Provincia = Ospite.RESIDENZAPROVINCIA1
            DcCom.Comune = Ospite.RESIDENZACOMUNE1
            DcCom.Leggi(Session("DC_OSPITE"))

            Dati.DO11_CITTA_NOME = DcCom.Descrizione
            Dati.DO11_CITTA_ISTAT = DcCom.Provincia & DcCom.Comune

            Dim DcProv As New ClsComune

            DcProv.Provincia = Ospite.RESIDENZAPROVINCIA1
            DcProv.Comune = ""
            DcProv.Leggi(Session("DC_OSPITE"))
            Dati.DO11_PROVINCIA = DcProv.CodificaProvincia


            Dim Kl As New Cls_DatiOspiteParenteCentroServizio

            Kl.CodiceOspite = CodiceOspite
            Kl.CodiceParente = 0
            Kl.CentroServizio = Registrazione.CentroServizio
            Kl.Leggi(Session("DC_OSPITE"))
            If Kl.ModalitaPagamento <> "" Then
                Ospite.MODALITAPAGAMENTO = Kl.ModalitaPagamento
            End If


            Dati.DO11_CONDPAG_CODICE_SEN = Ospite.MODALITAPAGAMENTO
            Dim MPAg As New ClsModalitaPagamento

            MPAg.Codice = Ospite.MODALITAPAGAMENTO
            MPAg.Leggi(Session("DC_OSPITE"))
            Dati.DO11_CONDPAG_NOME_SEN = MPAg.DESCRIZIONE


            Dim ModPag As New Cls_DatiPagamento

            ModPag.CodiceOspite = Ospite.CodiceOspite
            ModPag.CodiceParente = 0
            ModPag.LeggiUltimaDataData(Session("DC_OSPITE"), Registrazione.DataRegistrazione)

            Dati.DO11_BANCA_CIN = ModPag.Cin
            Dati.DO11_BANCA_ABI = ModPag.Abi
            Dati.DO11_BANCA_CAB = ModPag.Cab
            Dati.DO11_BANCA_CONTO = ModPag.CCBancario
            Dati.DO11_BANCA_IBAN = ModPag.CodiceInt & Format(Val(ModPag.NumeroControllo), "00") & ModPag.Cin & Format(Val(ModPag.Abi), "00000") & Format(Val(ModPag.Cab), "00000") & AdattaLunghezzaNumero(ModPag.CCBancario, 12)

            Dati.DO11_BANCA_IDMANDATO = ModPag.IdMandatoDebitore
            Dati.DO11_BANCA_DATAMANDATO = ModPag.DataMandatoDebitore

            If Ospite.RecapitoNome <> "" Then
                Dati.DES_RAGIONESOCIALE = Ospite.RecapitoNome

                Dati.DES_INDIRIZZO = Ospite.RecapitoIndirizzo
                Dati.DES_CAP = Ospite.RESIDENZACAP4

                Dim DestCom As New ClsComune

                DestCom.Provincia = Ospite.RecapitoProvincia
                DestCom.Comune = Ospite.RecapitoComune
                DestCom.Leggi(Session("DC_OSPITE"))

                Dati.DES_CITTA_NOME = DestCom.Descrizione
                Dati.DES_CITTA_ISTAT = DestCom.Provincia & DcCom.Comune

                Dim DestProv As New ClsComune

                DestProv.Provincia = Ospite.RecapitoProvincia
                DestProv.Comune = ""
                DestProv.Leggi(Session("DC_OSPITE"))
                Dati.DES_PROVINCIA = DestProv.CodificaProvincia

            End If

        End If
        If Mid(Tipo & Space(10), 1, 1) = "P" Then
            Dim Ospite As New Cls_Parenti

            Ospite.CodiceOspite = CodiceOspite
            Ospite.CodiceParente = CodiceParente
            Ospite.Leggi(Session("DC_OSPITE"), Ospite.CodiceOspite, Ospite.CodiceParente)

            Dati.CODICEEXPORT = Ospite.CONTOPERESATTO
            If Dati.CODICEEXPORT = "" Then
                Dim CodiceConto As Long

                CodiceConto = (Servizio.CONTO * 1000) + (Ospite.CodiceOspite * 10)
                Dati.CODICEEXPORT = "C" & CodiceConto
            End If

            Dati.DO11_RAGIONESOCIALE = Mid(Ospite.CognomeParente & Space(30), 1, 30) & Ospite.NomeParente
            Dati.DO11_PIVA = ""
            Dati.DO11_CF = Ospite.CODICEFISCALE
            Dati.DO11_INDIRIZZO = Ospite.RESIDENZAINDIRIZZO1
            Dati.DO11_CAP = Ospite.RESIDENZACAP1


            If Ospite.Opposizione730 = 1 Then
                Dati.DO11_OPPOSIZIONE = "S"
            Else
                Dati.DO11_OPPOSIZIONE = "N"
            End If
            Dim DcCom As New ClsComune

            DcCom.Provincia = Ospite.RESIDENZAPROVINCIA1
            DcCom.Comune = Ospite.RESIDENZACOMUNE1
            DcCom.Leggi(Session("DC_OSPITE"))

            Dati.DO11_CITTA_NOME = DcCom.Descrizione
            Dati.DO11_CITTA_ISTAT = DcCom.Provincia & DcCom.Comune

            Dim DcProv As New ClsComune

            DcProv.Provincia = Ospite.RESIDENZAPROVINCIA1
            DcProv.Comune = ""
            DcProv.Leggi(Session("DC_OSPITE"))
            Dati.DO11_PROVINCIA = DcProv.CodificaProvincia
            Dati.DO11_CONDPAG_CODICE_SEN = Ospite.MODALITAPAGAMENTO

            Dim Kl As New Cls_DatiOspiteParenteCentroServizio

            Kl.CodiceOspite = CodiceOspite
            Kl.CodiceParente = CodiceParente
            Kl.CentroServizio = Registrazione.CentroServizio
            Kl.Leggi(Session("DC_OSPITE"))
            If Kl.ModalitaPagamento <> "" Then
                Ospite.MODALITAPAGAMENTO = Kl.ModalitaPagamento
            End If

            Dim MPAg As New ClsModalitaPagamento

            MPAg.Codice = Ospite.MODALITAPAGAMENTO
            MPAg.Leggi(Session("DC_OSPITE"))
            Dati.DO11_CONDPAG_NOME_SEN = MPAg.DESCRIZIONE

            Dim ModPag As New Cls_DatiPagamento

            ModPag.CodiceOspite = Ospite.CodiceOspite
            ModPag.CodiceParente = Ospite.CodiceParente
            ModPag.LeggiUltimaDataData(Session("DC_OSPITE"), Registrazione.DataRegistrazione)

            Dati.DO11_BANCA_CIN = ModPag.Cin
            Dati.DO11_BANCA_ABI = ModPag.Abi
            Dati.DO11_BANCA_CAB = ModPag.Cab
            Dati.DO11_BANCA_CONTO = ModPag.CCBancario
            Dati.DO11_BANCA_IBAN = ModPag.CodiceInt & Format(Val(ModPag.NumeroControllo), "00") & ModPag.Cin & Format(Val(ModPag.Abi), "00000") & Format(Val(ModPag.Cab), "00000") & AdattaLunghezzaNumero(ModPag.CCBancario, 12)

            Dati.DO11_BANCA_IDMANDATO = ModPag.IdMandatoDebitore
            Dati.DO11_BANCA_DATAMANDATO = ModPag.DataMandatoDebitore
        End If
        If Mid(Tipo & Space(10), 1, 1) = "C" Then
            Dim Ospite As New ClsComune

            Ospite.Provincia = CodiceProvincia
            Ospite.Comune = CodiceComune
            Ospite.Leggi(Session("DC_OSPITE"))

            Dati.CODICEEXPORT = Ospite.CONTOPERESATTO

            If Dati.CODICEEXPORT = "" Then
                Dim CodiceConto As Long

                CodiceConto = Ospite.Comune
                Dati.CODICEEXPORT = "CC" & CodiceConto
            End If


            Dati.DO11_RAGIONESOCIALE = Ospite.Descrizione
            Dati.DO11_PIVA = Ospite.PartitaIVA
            Dati.DO11_CF = Ospite.CodiceFiscale
            Dati.DO11_INDIRIZZO = Ospite.RESIDENZAINDIRIZZO1
            Dati.DO11_CAP = Ospite.RESIDENZACAP1
            Dim DcCom As New ClsComune

            DcCom.Provincia = Ospite.RESIDENZAPROVINCIA1
            DcCom.Comune = Ospite.RESIDENZACOMUNE1
            DcCom.Leggi(Session("DC_OSPITE"))

            Dati.DO11_CITTA_NOME = DcCom.Descrizione
            Dati.DO11_CITTA_ISTAT = DcCom.Provincia & DcCom.Comune

            Dim DcProv As New ClsComune

            DcProv.Provincia = Ospite.RESIDENZAPROVINCIA1
            DcProv.Comune = ""
            DcProv.Leggi(Session("DC_OSPITE"))
            Dati.DO11_PROVINCIA = DcProv.CodificaProvincia
            Dati.DO11_CONDPAG_CODICE_SEN = Ospite.ModalitaPagamento
            Dim MPAg As New ClsModalitaPagamento

            MPAg.Codice = Ospite.ModalitaPagamento
            MPAg.Leggi(Session("DC_OSPITE"))
            Dati.DO11_CONDPAG_NOME_SEN = MPAg.DESCRIZIONE

            Dim cnOspiti As OleDbConnection

            cnOspiti = New Data.OleDb.OleDbConnection(Session("DC_OSPITE"))

            cnOspiti.Open()

            Dim cmdC As New OleDbCommand()
            cmdC.CommandText = ("select * from NoteFatture where CodiceProvincia = ? And CodiceComune = ?")
            cmdC.Parameters.AddWithValue("@CodiceProvincia", CodiceProvincia)
            cmdC.Parameters.AddWithValue("@CodiceComune", CodiceComune)
            cmdC.Connection = cnOspiti
            Dim RdCom As OleDbDataReader = cmdC.ExecuteReader()
            If RdCom.Read Then
                Dati.AddDescrizione = campodb(RdCom.Item("NoteUp"))
            End If
            RdCom.Close()
            cnOspiti.Close()
        End If
        If Mid(Tipo & Space(10), 1, 1) = "J" Then
            Dim Ospite As New ClsComune

            Ospite.Provincia = CodiceProvincia
            Ospite.Comune = CodiceComune
            Ospite.Leggi(Session("DC_OSPITE"))


            Dati.CODICEEXPORT = Ospite.CONTOPERESATTO

            If Dati.CODICEEXPORT = "" Then
                Dim CodiceConto As Long

                CodiceConto = Ospite.Comune
                Dati.CODICEEXPORT = "CJ" & CodiceConto
            End If
            Dati.DO11_RAGIONESOCIALE = Ospite.Descrizione
            Dati.DO11_PIVA = Ospite.PartitaIVA
            Dati.DO11_CF = Ospite.CodiceFiscale
            Dati.DO11_INDIRIZZO = Ospite.RESIDENZAINDIRIZZO1
            Dati.DO11_CAP = Ospite.RESIDENZACAP1
            Dim DcCom As New ClsComune

            DcCom.Provincia = Ospite.RESIDENZAPROVINCIA1
            DcCom.Comune = Ospite.RESIDENZACOMUNE1
            DcCom.Leggi(Session("DC_OSPITE"))

            Dati.DO11_CITTA_NOME = DcCom.Descrizione
            Dati.DO11_CITTA_ISTAT = DcCom.Provincia & DcCom.Comune

            Dim DcProv As New ClsComune

            DcProv.Provincia = Ospite.RESIDENZAPROVINCIA1
            DcProv.Comune = ""
            DcProv.Leggi(Session("DC_OSPITE"))
            Dati.DO11_PROVINCIA = DcProv.CodificaProvincia
            Dati.DO11_CONDPAG_CODICE_SEN = Ospite.ModalitaPagamento
            Dim MPAg As New ClsModalitaPagamento

            MPAg.Codice = Ospite.ModalitaPagamento
            MPAg.Leggi(Session("DC_OSPITE"))
            Dati.DO11_CONDPAG_NOME_SEN = MPAg.DESCRIZIONE

            Dim cnOspiti As OleDbConnection

            cnOspiti = New Data.OleDb.OleDbConnection(Session("DC_OSPITE"))

            cnOspiti.Open()

            Dim cmdC As New OleDbCommand()
            cmdC.CommandText = ("select * from NoteFatture where CodiceProvincia = ? And CodiceComune = ?")
            cmdC.Parameters.AddWithValue("@CodiceProvincia", CodiceProvincia)
            cmdC.Parameters.AddWithValue("@CodiceComune", CodiceComune)
            cmdC.Connection = cnOspiti
            Dim RdCom As OleDbDataReader = cmdC.ExecuteReader()
            If RdCom.Read Then
                Dati.AddDescrizione = campodb(RdCom.Item("NoteUp"))
            End If
            RdCom.Close()
            cnOspiti.Close()
        End If
        If Mid(Tipo & Space(10), 1, 1) = "R" Then
            Dim Ospite As New ClsUSL

            Ospite.CodiceRegione = CodiceRegione
            Ospite.Leggi(Session("DC_OSPITE"))

            Dati.CODICEEXPORT = Ospite.CONTOPERESATTO


            If Dati.CODICEEXPORT = "" Then
                Dim CodiceConto As Long

                CodiceConto = Ospite.CodiceRegione
                Dati.CODICEEXPORT = "CR" & CodiceConto
            End If

            Dati.DO11_RAGIONESOCIALE = Ospite.Nome
            Dati.DO11_PIVA = Ospite.PARTITAIVA
            Dati.DO11_CF = Ospite.CodiceFiscale
            Dati.DO11_INDIRIZZO = Ospite.RESIDENZAINDIRIZZO1
            Dati.DO11_CAP = Ospite.RESIDENZACAP1
            Dim DcCom As New ClsComune

            DcCom.Provincia = Ospite.RESIDENZAPROVINCIA1
            DcCom.Comune = Ospite.RESIDENZACOMUNE1
            DcCom.Leggi(Session("DC_OSPITE"))

            Dati.DO11_CITTA_NOME = DcCom.Descrizione
            Dati.DO11_CITTA_ISTAT = DcCom.Provincia & DcCom.Comune

            Dim DcProv As New ClsComune

            DcProv.Provincia = Ospite.RESIDENZAPROVINCIA1
            DcProv.Comune = ""
            DcProv.Leggi(Session("DC_OSPITE"))
            Dati.DO11_PROVINCIA = DcProv.CodificaProvincia
            Dati.DO11_CONDPAG_CODICE_SEN = Ospite.ModalitaPagamento
            Dim MPAg As New ClsModalitaPagamento

            MPAg.Codice = Ospite.ModalitaPagamento
            MPAg.Leggi(Session("DC_OSPITE"))
            Dati.DO11_CONDPAG_NOME_SEN = MPAg.DESCRIZIONE
        End If

        cn.Close()
    End Sub

    Function Export_DocumentiIncassi() As Boolean
        Dim cn As OleDbConnection
        Session("MYDATE") = Now
        'ADCLIFOR.XXXXX

        'ADDOCUME.XXXXX
        Dim TwADDOCUME As System.IO.TextWriter = System.IO.File.CreateText(HostingEnvironment.ApplicationPhysicalPath() & "\Public\ADDOCUME_" & Format(Session("MYDATE"), "yyyyMMddHHmmss") & ".Txt")
        Dim TwADDOCUMERighe As System.IO.TextWriter = System.IO.File.CreateText(HostingEnvironment.ApplicationPhysicalPath() & "\Public\ADDOCUMER_" & Format(Session("MYDATE"), "yyyyMMddHHmmss") & ".Txt")
        Dim TwADCLIFOR As System.IO.TextWriter = System.IO.File.CreateText(HostingEnvironment.ApplicationPhysicalPath() & "\Public\ADCLIFOR_" & Format(Session("MYDATE"), "yyyyMMddHHmmss") & ".Txt")

        Dim TwCSV As System.IO.TextWriter = System.IO.File.CreateText(HostingEnvironment.ApplicationPhysicalPath() & "\Public\CSV_" & Format(Session("MYDATE"), "yyyyMMddHHmmss") & ".Txt")

        cn = New Data.OleDb.OleDbConnection(Session("DC_GENERALE"))
        cn.Open()

        Dim Param As New Cls_DatiGenerali



        Param.LeggiDati(Session("DC_TABELLE"))

        Tabella = ViewState("App_AddebitiMultiplo")

        Dim NumeroRegistrazioni As Integer = 0
        Dim RigaEstratta As Integer = 0


        For RigaEstratta = 0 To Tabella.Rows.Count - 1
            Dim Chekc As CheckBox = DirectCast(GridView1.Rows(RigaEstratta).FindControl("ChkImporta"), CheckBox)

            If Chekc.Checked = True Then
                NumeroRegistrazioni = NumeroRegistrazioni + 1
            End If
        Next

        If NumeroRegistrazioni = 0 Then
            ScriptManager.RegisterClientScriptBlock(Me, Me.GetType(), "Stampa2", "alert('Attenzione non hai segnalato nessuna registrazione');", True)
            Exit Function
        End If

        NumeroRegistrazioni = 0

        Export_DocumentiIncassi = True

        For RigaEstratta = 0 To Tabella.Rows.Count - 1
            Dim Chekc As CheckBox = DirectCast(GridView1.Rows(RigaEstratta).FindControl("ChkImporta"), CheckBox)

            If Chekc.Checked = True Then
                Dim Registrazione As New Cls_MovimentoContabile
                Dim CausaleContabile As New Cls_CausaleContabile

                Registrazione.NumeroRegistrazione = campodbN(Tabella.Rows(RigaEstratta).Item(0))
                Registrazione.Leggi(Session("DC_GENERALE"), Registrazione.NumeroRegistrazione)

                CausaleContabile.Leggi(Session("DC_TABELLE"), Registrazione.CausaleContabile)

                Dim Anagrafiche As New Anagrafica


                DecodificaAnagrafica(Anagrafiche, Registrazione)

                If Chk_Prova.Checked = False Then
                    Dim cmdEff As New OleDbCommand()

                    cmdEff.CommandText = "UPDATE movimenticontabiliriga SET RigaBollato = 1 where Numero = ?"
                    cmdEff.Parameters.AddWithValue("@Numero", Registrazione.NumeroRegistrazione)
                    cmdEff.Connection = cn
                    cmdEff.ExecuteNonQuery()


                    Dim cmdEffT As New OleDbCommand()

                    cmdEffT.CommandText = "UPDATE movimenticontabiliTesta SET ExportDATA = GETDATE() where NumeroRegistrazione = ?"
                    cmdEffT.Parameters.AddWithValue("@Numero", Registrazione.NumeroRegistrazione)
                    cmdEffT.Connection = cn
                    cmdEffT.ExecuteNonQuery()
                End If

                If CausaleContabile.TipoDocumento = "FA" Or CausaleContabile.TipoDocumento = "RE" Or CausaleContabile.TipoDocumento = "NC" Then
                    Call FattureNC(Anagrafiche, Registrazione, CausaleContabile, TwADDOCUME, TwADDOCUMERighe, TwADCLIFOR)

               
                End If

                Dim Riga As String

                Riga = Registrazione.NumeroRegistrazione & ";" & Registrazione.DataRegistrazione & ";" & Registrazione.DataDocumento & ";" & Registrazione.NumeroDocumento & ";" & Registrazione.ImportoRegistrazioneDocumento(Session("DC_TABELLE"))

                TwCSV.WriteLine(Riga)
            End If
        Next
        cn.Close()


        TwADCLIFOR.Close()
        TwADDOCUME.Close()
        TwADDOCUMERighe.Close()
        TwCSV.Close()




        If Chk_Prova.Checked = False Then
            Dim IndiceTab As Integer


            For IndiceTab = 0 To GridView1.Rows.Count - 1

                Dim CheckBox As CheckBox = DirectCast(GridView1.Rows(IndiceTab).FindControl("ChkImporta"), CheckBox)

                CheckBox.Enabled = False
                CheckBox.Checked = False
            Next

            Btn_Export.Visible = False
        End If



        TabContainer1.ActiveTabIndex = 1
    End Function


    'TwADDOCUME, TwADDOCUMERighe, TwADCLIFOR
    Private Sub FattureNC(ByRef Dati As Anagrafica, ByVal Reg As Cls_MovimentoContabile, ByRef CausaleContabile As Cls_CausaleContabile, ByVal TwADDOCUME As System.IO.TextWriter, ByVal TwADDOCUMERighe As System.IO.TextWriter, ByVal TwADCLIFOR As System.IO.TextWriter)
        Dim DocumentoTesta As New ADDOCUME
        Dim DocumentoRiga As New ADDOCUMERiga
        Dim RigaCLISISP As New ADCLIFOR


        Dim TabTrascodifica As New Cls_TabellaTrascodificheEsportazioni


        Dim CentroServizio As New Cls_CentroServizio

        CentroServizio.CENTROSERVIZIO = Reg.CentroServizio
        CentroServizio.Leggi(Session("DC_OSPITE"), CentroServizio.CENTROSERVIZIO)

        Dim DatiSocieta As New Cls_TabellaSocieta

        DatiSocieta.Leggi(Session("DC_TABELLE"))

        RigaCLISISP.Tiporecord = "C"
        RigaCLISISP.Indicatore = "V" ' ....
        RigaCLISISP.Codice = Dati.CODICEEXPORT 'come valorizzare

        RigaCLISISP.Nome = Dati.DES_NOME
        RigaCLISISP.Cognome = Dati.DES_COGNOME



        RigaCLISISP.Codicefiscale = Dati.DO11_CF
        RigaCLISISP.PartitaIva = Dati.DO11_PIVA



        RigaCLISISP.Descrizione = Dati.DO11_RAGIONESOCIALE

        RigaCLISISP.Indirizzo = Dati.DO11_INDIRIZZO
        RigaCLISISP.Indirizzoaggiuntivo = ""
        RigaCLISISP.Localita = Dati.DO11_CITTA_NOME
        RigaCLISISP.Provincia = Dati.DO11_PROVINCIA
        RigaCLISISP.cap = Dati.DO11_CAP

        If Val(Dati.DO11_PIVA) > 0 Then
            RigaCLISISP.Personafisica = "N"
            RigaCLISISP.FlagPrivato = ""
        Else
            RigaCLISISP.FlagPrivato = "S"
            RigaCLISISP.Personafisica = "S"
        End If

        RigaCLISISP.Cognome = Dati.DES_COGNOME
        RigaCLISISP.Nome = Dati.DES_NOME

        Dim Indice As Integer
        Dim TrovatoCliente As Boolean = False

        For Indice = 0 To 1000
            If Vt_Cliente(Indice) = RigaCLISISP.PartitaIva & RigaCLISISP.Codicefiscale Then
                TrovatoCliente = True
                Exit For
            End If
            If Vt_Cliente(Indice) = "" Then
                TrovatoCliente = False
                Exit For
            End If
        Next

        If TrovatoCliente = False And Indice < 1000 Then
            Vt_Cliente(Indice) = RigaCLISISP.PartitaIva & RigaCLISISP.Codicefiscale
        End If
        If TrovatoCliente = False Then
            ScriviADCLIFOR(RigaCLISISP, TwADCLIFOR)
        End If


        DocumentoTesta.CodiceCliente = RigaCLISISP.Codice
        DocumentoTesta.CodiceValuta = "EUR"


        DocumentoTesta.TipoDocumento = "DT"
        DocumentoTesta.DataDocumento = Format(Reg.DataRegistrazione, "yyyyMMdd")

        DocumentoTesta.AlfaDocumento = "001"


        DocumentoTesta.NumeroDocumento = Reg.NumeroProtocollo


        DocumentoTesta.FlagTipo = ""
        DocumentoTesta.Anno = Reg.AnnoProtocollo
        DocumentoTesta.DataCompetenza = Format(Reg.DataRegistrazione, "yyyyMMdd")

        TabTrascodifica.TIPOTAB = "CD"
        TabTrascodifica.SENIOR = Reg.CausaleContabile
        TabTrascodifica.EXPORT = 0
        TabTrascodifica.Leggi(Session("DC_TABELLE"))

        DocumentoTesta.CausaleDocumento = TabTrascodifica.EXPORT


        DocumentoTesta.CodiceCliente = RigaCLISISP.Codice


        TabTrascodifica.TIPOTAB = "MP"
        TabTrascodifica.SENIOR = Reg.CausaleContabile
        TabTrascodifica.EXPORT = 0
        TabTrascodifica.Leggi(Session("DC_TABELLE"))
        DocumentoTesta.CodicePagamento = TabTrascodifica.EXPORT

        DocumentoTesta.DataDiversa = ""

        DocumentoTesta.CodiceBanca = ""
        DocumentoTesta.CodiceValuta = ""

        DocumentoTesta.CambioGiornaliero = ""

        DocumentoTesta.Acconto = Reg.ImportoDocumento(Session("DC_TABELLE"))

        ScriviADDOCUME(DocumentoTesta, TwADDOCUME)


        Dim RigaInterna As Integer = 0
        Dim RigaRicavo As Integer

        For RigaRicavo = 0 To 100
            If Not IsNothing(Reg.Righe(RigaRicavo)) Then
                If Reg.Righe(RigaRicavo).RigaDaCausale = 3 Or Reg.Righe(RigaRicavo).RigaDaCausale = 4 Or Reg.Righe(RigaRicavo).RigaDaCausale = 5 Then
                    RigaInterna = RigaInterna + 1


                    DocumentoRiga.TipoRiga = "R" '?

                    DocumentoRiga.Descrizione = Reg.Righe(RigaRicavo).Descrizione


                    TabTrascodifica.TIPOTAB = "CA"
                    TabTrascodifica.SENIOR = Reg.Righe(RigaRicavo).MastroPartita & "." & Reg.Righe(RigaRicavo).ContoPartita & "." & Reg.Righe(RigaRicavo).SottocontoPartita
                    TabTrascodifica.EXPORT = 0
                    TabTrascodifica.Leggi(Session("DC_TABELLE"))
                    DocumentoRiga.CodiceArticolo = TabTrascodifica.EXPORT

                    'DocumentoRiga.UnitadiMisura = "GG"



                    DocumentoRiga.QtaMovimentata = Reg.Righe(RigaRicavo).Quantita

                    DocumentoRiga.PrezzoUnitario = Math.Round(Reg.Righe(RigaRicavo).Importo / Reg.Righe(RigaRicavo).Quantita, 5)



                    TabTrascodifica.TIPOTAB = "IV"
                    TabTrascodifica.SENIOR = Reg.Righe(RigaRicavo).CodiceIVA
                    TabTrascodifica.EXPORT = 0
                    TabTrascodifica.Leggi(Session("DC_TABELLE"))
                    DocumentoRiga.CodiceIVA = TabTrascodifica.EXPORT
                    Dim IVA As New Cls_IVA

                    IVA.Codice = Reg.Righe(RigaRicavo).CodiceIVA
                    IVA.Leggi(Session("DC_TABELLE"), IVA.Codice)

                    DocumentoRiga.AliquotaIva = IVA.Aliquota * 100


                    DocumentoRiga.TotaleNettoRIga = Reg.Righe(RigaRicavo).Importo


                    TabTrascodifica.TIPOTAB = "CC"
                    TabTrascodifica.SENIOR = Reg.Righe(RigaRicavo).MastroPartita & "." & Reg.Righe(RigaRicavo).ContoPartita & "." & Reg.Righe(RigaRicavo).SottocontoPartita
                    TabTrascodifica.EXPORT = 0
                    TabTrascodifica.Leggi(Session("DC_TABELLE"))
                    DocumentoRiga.CodiceConto = TabTrascodifica.EXPORT


                    TabTrascodifica.TIPOTAB = "CR"
                    TabTrascodifica.SENIOR = Reg.Righe(RigaRicavo).MastroPartita & "." & Reg.Righe(RigaRicavo).ContoPartita & "." & Reg.Righe(RigaRicavo).SottocontoPartita
                    TabTrascodifica.EXPORT = 0
                    TabTrascodifica.Leggi(Session("DC_TABELLE"))
                    DocumentoRiga.Centroricavo = TabTrascodifica.EXPORT

                    If CausaleContabile.TipoDocumento = "NC" Then
                        DocumentoRiga.Classedocumento = "NC" '?
                    Else
                        DocumentoRiga.Classedocumento = "FA" '?
                    End If

                    DocumentoRiga.MVFLVEAC = "V"


                    ScriviADDOCUMERiga(DocumentoRiga, DocumentoTesta, TwADDOCUMERighe)

                End If
            End If
        Next






    End Sub



    Private Sub Estrazione()
        Dim cn As OleDbConnection


        cn = New Data.OleDb.OleDbConnection(Session("DC_GENERALE"))
        cn.Open()
        Dim Param As New Cls_DatiGenerali


        Param.LeggiDati(Session("DC_TABELLE"))



        Tabella.Clear()
        Tabella.Columns.Clear()
        Tabella.Columns.Add("NumeroRegistrazione", GetType(String))
        Tabella.Columns.Add("DataRegistrazione", GetType(String))
        Tabella.Columns.Add("NumeroDocumento", GetType(String))
        Tabella.Columns.Add("Causale", GetType(String))
        Tabella.Columns.Add("Intestatario", GetType(String))
        Tabella.Columns.Add("Importo", GetType(String))




        Dim cmd As New OleDbCommand()
        '(select count(*) from MovimentiContabiliRiga Where Numero = MovimentiContabiliTesta.NumeroRegistrazione  And RigaBollato = 1) =  0 And

        If Param.CausaleRettifica = "" Then
            cmd.CommandText = "Select * From MovimentiContabiliTesta where DataRegistrazione >= ? And DataRegistrazione <= ? And ((Select COUNT(*) from movimenticontabiliriga where NUMERO = NUMEROREGISTRAZIONE AND  (RigaBollato = 0 or RigaBollato IS null)) > 0)  order by  (Select Top 1 CodiceDocumento  From TabellaLegami where CodicePagamento = NumeroRegistrazione  Order by CodiceDocumento),AnnoProtocollo,NumeroProtocollo,DataRegistrazione"
        Else
            cmd.CommandText = "Select * From MovimentiContabiliTesta where CausaleContabile <> '" & Param.CausaleRettifica & "' And DataRegistrazione >= ? And DataRegistrazione <= ? And ((Select COUNT(*) from movimenticontabiliriga where NUMERO = NUMEROREGISTRAZIONE AND  (RigaBollato = 0 or RigaBollato IS null)) > 0)  order by  (Select Top 1 CodiceDocumento  From TabellaLegami where CodicePagamento = NumeroRegistrazione  Order by CodiceDocumento),AnnoProtocollo,NumeroProtocollo,DataRegistrazione"
        End If
        cmd.Parameters.AddWithValue("@DataDal", Txt_DataDal.Text)
        cmd.Parameters.AddWithValue("@DataAl", Txt_DataAl.Text)
        cmd.Connection = cn
        Dim MyRsDB As OleDbDataReader = cmd.ExecuteReader()
        Do While MyRsDB.Read
            Dim myriga As System.Data.DataRow = Tabella.NewRow()

            Dim NReg As New Cls_MovimentoContabile

            NReg.NumeroRegistrazione = campodbN(MyRsDB.Item("NumeroRegistrazione"))
            NReg.Leggi(Session("DC_GENERALE"), NReg.NumeroRegistrazione)

            Dim Causale As New Cls_CausaleContabile

            Causale.Codice = NReg.CausaleContabile
            Causale.Leggi(Session("DC_TABELLE"), Causale.Codice)

            myriga(0) = NReg.NumeroRegistrazione
            myriga(1) = Format(NReg.DataRegistrazione, "dd/MM/yyyy")
            myriga(2) = NReg.NumeroDocumento
            myriga(3) = Causale.Descrizione

            Dim PInc As New Cls_Pianodeiconti

            PInc.Mastro = NReg.Righe(0).MastroPartita
            PInc.Conto = NReg.Righe(0).ContoPartita
            PInc.Sottoconto = NReg.Righe(0).SottocontoPartita
            PInc.Decodfica(Session("DC_GENERALE"))

            myriga(4) = PInc.Descrizione

            myriga(5) = Format(NReg.ImportoRegistrazioneDocumento(Session("DC_TABELLE")), "#,##0.00")



            Tabella.Rows.Add(myriga)
        Loop


        cn.Close()


        ViewState("App_AddebitiMultiplo") = Tabella

        GridView1.AutoGenerateColumns = False

        GridView1.DataSource = Tabella
        GridView1.DataBind()
        GridView1.Visible = True

        Dim ParamOsp As New Cls_Parametri


        ParamOsp.LeggiParametri(Session("DC_OSPITE"))

        If ParamOsp.TipoExport = "Documenti Incassi" And Chk_Prova.Checked = False Then
            Btn_Export.ImageUrl = "~/images/sendmail.png"
            Btn_Export.Visible = True
        Else
            Btn_Export.ImageUrl = "~/images/download.png"
            Btn_Export.Visible = True
        End If

    End Sub


    Function campodb(ByVal oggetto As Object) As String
        If IsDBNull(oggetto) Then
            Return ""
        Else
            Return oggetto
        End If
    End Function
    Function campodbN(ByVal oggetto As Object) As Double
        If IsDBNull(oggetto) Then
            Return 0
        Else
            Return oggetto
        End If
    End Function

    Function campodbd(ByVal oggetto As Object) As Date
        If IsDBNull(oggetto) Then
            Return Nothing
        Else
            Return oggetto
        End If
    End Function

    Protected Sub Btn_Estrai_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles Btn_Estrai.Click
        Estrazione()
    End Sub

    Private Sub EseguiJS()
        Dim MyJs As String
        MyJs = "$(document).ready(function() {"

        MyJs = MyJs & "var els = document.getElementsByTagName(""*"");"

        MyJs = MyJs & "for (var i=0;i<els.length;i++)"
        MyJs = MyJs & "if ( els[i].id ) { "
        MyJs = MyJs & " var appoggio =els[i].id; "


        MyJs = MyJs & " if ((appoggio.match('Txt_Importo')!= null)) {  "
        MyJs = MyJs & " $(els[i]).keypress(function() { ForceNumericInput($(this).val(), true, true); } ); "
        MyJs = MyJs & " $(els[i]).blur(function() { var ap = formatNumber($(this).val(),2); $(this).val(ap); });"
        MyJs = MyJs & "    }"

        MyJs = MyJs & " if ((appoggio.match('Txt_Data')!= null) || (appoggio.match('Txt_Data')!= null)) {  "
        MyJs = MyJs & " $(els[i]).mask(""99/99/9999"");"
        MyJs = MyJs & " $(els[i]).datepicker({ changeMonth: true,changeYear: true,  yearRange: ""1990:" & Year(Now) + 5 & """},$.datepicker.regional[""it""]);"
        MyJs = MyJs & "    }"


        MyJs = MyJs & "} "



        MyJs = MyJs & "});"

        'MyJs = "$(document).ready(function() { $('.myClass').keypress(function() { handleEnter($('.myClass').val(), true, true); } );     $('#" & Txt_ImportoPagato.ClientID & "').blur(function() { var ap = formatNumber ($('#" & Txt_ImportoPagato.ClientID & "').val(),2); $('#" & Txt_ImportoPagato.ClientID & "').val(ap); }); });"
        ScriptManager.RegisterStartupScript(Me, Me.GetType(), "visualizzaRitIm", MyJs, True)
    End Sub
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If IsNothing(Session("DC_OSPITE")) Then
            Response.Redirect("..\Login.aspx")
            Exit Sub
        End If

        EseguiJS()

        Dim K1 As New Cls_SqlString


        Dim Barra As New Cls_BarraSenior

        If Not IsNothing(Session("RicercaAnagraficaSQLString")) Then
            K1 = Session("RicercaAnagraficaSQLString")
        End If

        Lbl_BarraSenior.Text = Barra.CodiceBarra(Application("SENIOR"), Session("UTENTE"), Page, K1)
    End Sub

    Protected Sub Btn_Seleziona_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles Btn_Seleziona.Click
        Dim Riga As Integer


        For Riga = 0 To GridView1.Rows.Count - 1

            Dim CheckBox As CheckBox = DirectCast(GridView1.Rows(Riga).FindControl("ChkImporta"), CheckBox)


            If CheckBox.Enabled = True Then
                CheckBox.Checked = True
            End If
        Next
    End Sub


    Function SostitusciCaratteriSpeciali(ByVal Testo As String) As String
        Testo = Testo.Replace("à","a'")
        Testo = Testo.Replace("è","e'")
        Testo = Testo.Replace("é","e'")
        Testo = Testo.Replace("ù","u'")
        Testo = Testo.Replace("ì","i'")
        Testo = Testo.Replace("ò","o'")
        Testo = Testo.Replace("°","")

        Return Testo
    End Function



    Function AdattaLunghezzaNumero(ByVal Testo As String, ByVal Lunghezza As Integer) As String
        If Len(Testo) > Lunghezza Then
            AdattaLunghezzaNumero = Mid(Testo, 1, Lunghezza)
            Exit Function
        End If
        AdattaLunghezzaNumero = Replace(Space(Lunghezza - Len(Testo)) & Testo, " ", "0")
        REM AdattaLunghezzaNumero = Space(Lunghezza - Len(Testo)) & Testo


    End Function

    Function AdattaLunghezza(ByVal Testo As String, ByVal Lunghezza As Integer) As String
        Testo = SostitusciCaratteriSpeciali(Testo)
        If Len(Testo) > Lunghezza Then
            Testo = Mid(Testo, 1, Lunghezza)
        End If

        AdattaLunghezza = Testo & Space(Lunghezza - Len(Testo) + 1)

        If Len(AdattaLunghezza) > Lunghezza Then
            AdattaLunghezza = Mid(AdattaLunghezza, 1, Lunghezza)
        End If

    End Function



    Protected Sub Btn_Export_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles Btn_Export.Click
        Call Export_DocumentiIncassi()

        Btn_DownloadMOVIM.Visible = True
    End Sub

    Protected Sub Btn_Esci_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles Btn_Esci.Click
        Response.Redirect("../Menu_Export.aspx")
    End Sub


    Protected Sub Btn_DownloadMOVIM_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles Btn_DownloadMOVIM.Click
        Dim NomeFile As String

        Try

            Kill(HostingEnvironment.ApplicationPhysicalPath() & "\Public\ADDOCUME.")
            Kill(HostingEnvironment.ApplicationPhysicalPath() & "\Public\ADDOCUMER.")
            Kill(HostingEnvironment.ApplicationPhysicalPath() & "\Public\ADCLIFOR.")

        Catch ex As Exception

        End Try


        Dim Zipfile As New Ionic.Zip.ZipFile

        NomeFile = HostingEnvironment.ApplicationPhysicalPath() & "\Public\ADDOCUME_" & Format(Session("MYDATE"), "yyyyMMddHHmmss") & ".Txt"

        Rename(NomeFile, HostingEnvironment.ApplicationPhysicalPath() & "\Public\ADDOCUME.")

        Zipfile.AddFile(HostingEnvironment.ApplicationPhysicalPath() & "\Public\ADDOCUME.", "\")


        NomeFile = HostingEnvironment.ApplicationPhysicalPath() & "\Public\ADDOCUMER_" & Format(Session("MYDATE"), "yyyyMMddHHmmss") & ".Txt"

        Rename(NomeFile, HostingEnvironment.ApplicationPhysicalPath() & "\Public\ADDOCUMER.")


        Zipfile.AddFile(HostingEnvironment.ApplicationPhysicalPath() & "\Public\ADDOCUMER.", "\")



        NomeFile = HostingEnvironment.ApplicationPhysicalPath() & "\Public\ADCLIFOR_" & Format(Session("MYDATE"), "yyyyMMddHHmmss") & ".Txt"


        Rename(NomeFile, HostingEnvironment.ApplicationPhysicalPath() & "\Public\ADCLIFOR.")

        Zipfile.AddFile(HostingEnvironment.ApplicationPhysicalPath() & "\Public\ADCLIFOR.", "\")


        
        Zipfile.Save(HostingEnvironment.ApplicationPhysicalPath() & "\Public\export.zip")

        Try

            Response.Clear()
            Response.Buffer = True
            Response.ContentType = "text/plain"
            Response.AppendHeader("content-disposition", "attachment;filename=documenti.zip")
            Response.WriteFile(HostingEnvironment.ApplicationPhysicalPath() & "\Public\export.zip")
            Response.Flush()
            Response.End()
        Catch ex As Exception

        Finally
            Try
                Kill(NomeFile)
            Catch ex As Exception

            End Try
        End Try
    End Sub




End Class
