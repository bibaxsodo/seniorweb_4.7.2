﻿<%@ Page Language="VB" AutoEventWireup="false" Inherits="risultatoricalcolo" CodeFile="risultatoricalcolo.aspx.vb" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>Risultato Ricalcolo</title>
    <asp:PlaceHolder runat="server">
        <%: System.Web.Optimization.Styles.Render("~/Content/AjaxControlToolkit/Styles/Bundle") %>
    </asp:PlaceHolder>
    <link href="ospiti.css?ver=11" rel="stylesheet" type="text/css" />
    <link rel="shortcut icon" href="images/SENIOR.ico" />
</head>
<body>
    <form id="form1" runat="server">
        <asp:ScriptManager ID="ScriptManager1" runat="server" EnableScriptGlobalization="true">
            <Scripts>
                <asp:ScriptReference Path="~/Scripts/AjaxControlToolkit/Bundle" />
            </Scripts>
        </asp:ScriptManager>
        <div align="Left">
            <asp:Button ID="Btn_EliminaOspiti" runat="server" Text="Elimina Ad/Ac Ospiti" Width="160px" />&nbsp;
        <asp:Button ID="Button1" runat="server" Text="Elimina Ad/Ac Parenti" Width="160px" />
            <asp:Button ID="Button2" runat="server" Text="Elimina Ad/Ac Comuni" Width="160px" />
            <asp:Button ID="Button3" runat="server" Text="Elimina Ad/Ac Regioni" Width="160px" />
            <asp:GridView ID="Grid_AddAcr" runat="server" CellPadding="4" ForeColor="#333333"
                GridLines="None" Width="648px">
                <RowStyle BackColor="#EFF3FB" />
                <FooterStyle BackColor="#507CD1" Font-Bold="True" ForeColor="White" />
                <PagerStyle BackColor="#2461BF" ForeColor="White" HorizontalAlign="Center" />
                <SelectedRowStyle BackColor="#D1DDF1" Font-Bold="True" ForeColor="#333333" />
                <HeaderStyle BackColor="#507CD1" Font-Bold="True" ForeColor="White" />
                <EditRowStyle BackColor="#2461BF" />
                <AlternatingRowStyle BackColor="White" />
                <Columns>
                    <asp:CommandField ShowSelectButton="True" SelectText="Elimina" />
                </Columns>
            </asp:GridView>

        </div>
    </form>
</body>
</html>
