﻿Imports Microsoft.VisualBasic
Imports System.Data.OleDb
Imports System.Web.Hosting

Partial Class OspitiWeb_IncassoDaBollettino
    Inherits System.Web.UI.Page


    Private Sub EseguiJS()
        Dim MyJs As String
        MyJs = "$(document).ready(function() {"

        MyJs = MyJs & "var els = document.getElementsByTagName(""*"");"

        MyJs = MyJs & "for (var i=0;i<els.length;i++)"
        MyJs = MyJs & "if ( els[i].id ) { "
        MyJs = MyJs & " var appoggio =els[i].id; "
        MyJs = MyJs & " if ((appoggio.match('Txt_Data')!= null) || (appoggio.match('Txt_Decesso')!= null))  {  "
        MyJs = MyJs & " $(els[i]).mask(""99/99/9999"");"
        MyJs = MyJs & " $(els[i]).datepicker({ changeMonth: true,changeYear: true,  yearRange: ""1990:" & Year(Now) + 5 & """},$.datepicker.regional[""it""]);"
        MyJs = MyJs & "    }"
        MyJs = MyJs & " if ((appoggio.match('Txt_Importo')!= null) ) {  "
        MyJs = MyJs & " $(els[i]).keypress(function() { ForceNumericInput($(this).val(), true, true); } ); "
        MyJs = MyJs & " $(els[i]).blur(function() { var ap = formatNumber($(this).val(),2); $(this).val(ap); });"
        MyJs = MyJs & "    }"

        MyJs = MyJs & "} "
        MyJs = MyJs & "});"

        'MyJs = "$(document).ready(function() { $('.myClass').keypress(function() { handleEnter($('.myClass').val(), true, true); } );     $('#" & Txt_ImportoPagato.ClientID & "').blur(function() { var ap = formatNumber ($('#" & Txt_ImportoPagato.ClientID & "').val(),2); $('#" & Txt_ImportoPagato.ClientID & "').val(ap); }); });"
        ScriptManager.RegisterStartupScript(Me, Me.GetType(), "visualizzaRitIm", MyJs, True)
    End Sub



    Protected Sub OspitiWeb_IncassoDaBollettino_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Page.IsPostBack = True Then
            Exit Sub
        End If
        Dim K1 As New Cls_SqlString

        Dim Barra As New Cls_BarraSenior

        If Not IsNothing(Session("RicercaAnagraficaSQLString")) Then
            K1 = Session("RicercaAnagraficaSQLString")
        End If

        Lbl_BarraSenior.Text = Barra.CodiceBarra(Application("SENIOR"), Session("UTENTE"), Page, K1)


        Dim ParamOspiti As New Cls_Parametri

        ParamOspiti.LeggiParametri(Session("DC_OSPITE"))


        Dim k As New Cls_CausaleContabile
        Dim ConnectionString As String = Session("DC_TABELLE")

        k.UpDateDropBoxPag(ConnectionString, Dd_CausaleContabile)

        Txt_DataIncasso.Text = Format(Now, "dd/MM/yyyy")


        Call EseguiJS()

    End Sub

    Protected Sub Btn_Decodifica_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles Btn_Decodifica.Click
        If Val(Txt_CodiceOspite.Text) = 0 Then
            ScriptManager.RegisterClientScriptBlock(Me, Me.GetType(), "Errore", "VisualizzaErrore('<center>Inserisci codice ospite</center>');", True)
            Exit Sub
        End If
        If Val(Txt_Numero.Text) = 0 Then
            ScriptManager.RegisterClientScriptBlock(Me, Me.GetType(), "Errore", "VisualizzaErrore('<center>Inserisci numero</center>');", True)
            Exit Sub
        End If

        Dim l As New ClsOspite

        l.CodiceOspite = Val(Txt_CodiceOspite.Text)
        l.Leggi(Session("DC_OSPITE"), l.CodiceOspite)

        Lbl_DecodificaT.Text = "<div style=""width: 325px;height:74px;border: 2px #888888 solid;-moz-box-shadow: 3px 3px 3px 2px #888888;-webkit-box-shadow: 3px 3px 3px 2px #888888;box-shadow: 3px 3px 3px 2px #888888;"">Decodifica Ospite :" & l.Nome

        Dim cn As OleDbConnection


        cn = New Data.OleDb.OleDbConnection(Session("DC_GENERALE"))

        cn.Open()

        Dim MySql As String = "Select * From MovimentiContabiliRiga,MovimentiContabiliTesta  Where Numero = NumeroRegistrazione And NumeroProtocollo = " & Txt_Numero.Text & " And SottocontoPartita >= " & Txt_CodiceOspite.Text * 100 & " And SottocontoPartita  <= " & (Txt_CodiceOspite.Text * 100) + 99

        Dim cmd As New OleDbCommand()
        cmd.CommandText = MySql
        cmd.Connection = cn        
        Dim myPOSTreader As OleDbDataReader = cmd.ExecuteReader()
        If myPOSTreader.Read Then
            Dim M As New Cls_MovimentoContabile

            M.NumeroRegistrazione = campodbN(myPOSTreader.Item("NumeroRegistrazione"))
            M.Leggi(Session("DC_GENERALE"), M.NumeroRegistrazione)
            Lbl_DecodificaT.Text = Lbl_DecodificaT.Text & "<br/><br/>Importo Documento : " & M.ImportoDocumento(Session("DC_TABELLE")) & " del " & M.DataRegistrazione & "<br/>"
            Dim Legami As New Cls_Legami


            Legami.Leggi(Session("DC_GENERALE"), M.NumeroRegistrazione, 0)

            Dim I As Integer

            For I = 0 To Legami.Importo.Length - 1
                If Legami.Importo(I) > 0 Then
                    Lbl_DecodificaT.Text = Lbl_DecodificaT.Text & "<br/><br/><b>DOCUMENTO GIA' PAGATO</b>"
                End If
            Next
        End If
        myPOSTreader.Close()
        cn.Close()
        Lbl_DecodificaT.Text = Lbl_DecodificaT.Text & "</div>"

        Call EseguiJS()
    End Sub

    Function campodbN(ByVal oggetto As Object) As Long
        If IsDBNull(oggetto) Then
            Return 0
        Else
            Return oggetto
        End If
    End Function
    Function campodb(ByVal oggetto As Object) As String
        If IsDBNull(oggetto) Then
            Return ""
        Else
            Return oggetto
        End If
    End Function

    Protected Sub Btn_Modifica_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles Btn_Modifica.Click
        If Val(Txt_CodiceOspite.Text) = 0 Then
            ScriptManager.RegisterClientScriptBlock(Me, Me.GetType(), "Errore", "VisualizzaErrore('<center>Inserisci codice ospite</center>');", True)
            Exit Sub
        End If
        If Val(Txt_Numero.Text) = 0 Then
            ScriptManager.RegisterClientScriptBlock(Me, Me.GetType(), "Errore", "VisualizzaErrore('<center>Inserisci numero</center>');", True)
            Exit Sub
        End If

        If Not IsDate(Txt_DataIncasso.Text) Then
            ScriptManager.RegisterClientScriptBlock(Me, Me.GetType(), "Errore", "VisualizzaErrore('<center>Data Incasso formalmente errata</center>');", True)
            Exit Sub
        End If


        Dim NumeroRegistrazione As Integer = 0
        Dim ImportoDocumento As Double = 0
        Dim CodiceParente As Integer = 0
        Dim Mastro As Integer = 0
        Dim Conto As Integer = 0
        Dim Sottoconto As Integer = 0
        Dim cn As OleDbConnection


        cn = New Data.OleDb.OleDbConnection(Session("DC_GENERALE"))

        cn.Open()

        Dim MySql As String = "Select * From MovimentiContabiliRiga,MovimentiContabiliTesta  Where Numero = NumeroRegistrazione And NumeroProtocollo = " & Txt_Numero.Text & " And SottocontoPartita >= " & Txt_CodiceOspite.Text * 100 & " And SottocontoPartita  <= " & (Txt_CodiceOspite.Text * 100) + 99

        Dim cmd As New OleDbCommand()
        cmd.CommandText = MySql
        cmd.Connection = cn
        Dim myPOSTreader As OleDbDataReader = cmd.ExecuteReader()
        If myPOSTreader.Read Then
            Dim M As New Cls_MovimentoContabile

            M.NumeroRegistrazione = campodbN(myPOSTreader.Item("NumeroRegistrazione"))
            NumeroRegistrazione = M.NumeroRegistrazione
            M.Leggi(Session("DC_GENERALE"), M.NumeroRegistrazione)

            ImportoDocumento = M.ImportoDocumento(Session("DC_TABELLE"))

            Mastro = M.Righe(0).MastroPartita
            Conto = M.Righe(0).ContoPartita
            Sottoconto = M.Righe(0).SottocontoPartita


            Dim Legami As New Cls_Legami


            Legami.Leggi(Session("DC_GENERALE"), M.NumeroRegistrazione, 0)

            Dim I As Integer

            For I = 0 To Legami.Importo.Length - 1
                If Legami.Importo(I) > 0 Then
                    ScriptManager.RegisterClientScriptBlock(Me, Me.GetType(), "Errore", "VisualizzaErrore('<center>Vi sono legami non posso procedere</center>');", True)
                    Exit Sub
                End If
            Next
        End If
        myPOSTreader.Close()
        cn.Close()

        If NumeroRegistrazione = 0 Then
            ScriptManager.RegisterClientScriptBlock(Me, Me.GetType(), "Errore", "VisualizzaErrore('<center>Specifica numero registrazione</center>');", True)
            Exit Sub
        End If

        Call CreatoIncasso(NumeroRegistrazione, ImportoDocumento, Mastro, Conto, Sottoconto)


        Response.Redirect("IncassoDaBollettino.aspx")
    End Sub


    Private Sub CreatoIncasso(ByVal NumeroDocumento As Long, ByVal ImportoDocumento As Double, ByVal CMastro As Integer, ByVal CConto As Integer, ByVal CSottoconto As Long)

        If Not IsDate(Txt_DataBolletta.Text) Then
            ScriptManager.RegisterClientScriptBlock(Me, Me.GetType(), "Errore", "VisualizzaErrore('<center>Specifica data bollettino</center>');", True)
            Exit Sub
        End If

        Dim X As New Cls_MovimentoContabile
        Dim MyCau As New Cls_CausaleContabile

        X.Leggi(Session("DC_GENERALE"), 0)

        X.NumeroRegistrazione = 0
        X.DataRegistrazione = Txt_DataIncasso.Text
        X.DataBolletta = Txt_DataBolletta.Text
        X.NumeroBolletta = Txt_NumeroBolletta.Text
        X.Descrizione = Txt_Descrizione.Text

        X.CausaleContabile = Dd_CausaleContabile.SelectedValue
        X.Utente = Session("UTENTE")
        MyCau.Leggi(Session("DC_TABELLE"), Dd_CausaleContabile.SelectedValue)

        Dim Mastro As Long
        Dim Conto As Long
        Dim Sottoconto As Long
        Dim XCs As New Cls_CentroServizio

        Dim KoSP As New Cls_Parametri

        KoSP.LeggiParametri(Session("DC_OSPITE"))

        Mastro = CMastro
        Conto = CConto
        Sottoconto = CSottoconto
        Dim Riga As Long

        Riga = 0

        If IsNothing(X.Righe(Riga)) Then
            X.Righe(Riga) = New Cls_MovimentiContabiliRiga
        End If


        X.Righe(0).MastroPartita = Mastro
        X.Righe(0).ContoPartita = Conto
        X.Righe(0).SottocontoPartita = Sottoconto
        X.Righe(0).Importo = ImportoDocumento
        X.Righe(0).Segno = "+"
        X.Righe(0).Tipo = "CF"
        X.Righe(0).RigaDaCausale = 1
        X.Righe(0).DareAvere = MyCau.Righe(0).DareAvere
        X.Righe(Riga).Utente = Session("UTENTE")
        Riga = Riga + 1


        If IsNothing(X.Righe(Riga)) Then
            X.Righe(Riga) = New Cls_MovimentiContabiliRiga
        End If


        Dim X1 As New Cls_CausaleContabile

        X1.Leggi(Session("DC_TABELLE"), Dd_CausaleContabile.SelectedValue)

        Dim xS As New Cls_Pianodeiconti

        Mastro = X1.Righe(1).Mastro
        Conto = X1.Righe(1).Conto
        Sottoconto = X1.Righe(1).Sottoconto

        X.Righe(Riga).MastroPartita = Mastro
        X.Righe(Riga).ContoPartita = Conto
        X.Righe(Riga).SottocontoPartita = Sottoconto
        X.Righe(Riga).Importo = ImportoDocumento
        X.Righe(Riga).Segno = "+"
        X.Righe(Riga).Tipo = ""
        X.Righe(Riga).RigaDaCausale = 2
        X.Righe(Riga).DareAvere = MyCau.Righe(1).DareAvere
        X.Righe(Riga).Utente = Session("UTENTE")
        Riga = Riga + 1

        If X.Scrivi(Session("DC_GENERALE"), 0) = False Then
            ClientScript.RegisterClientScriptBlock(Me.GetType(), "Errore", "VisualizzaErrore('Errore nella registrazione');", True)
            Exit Sub
        End If

        Dim Legami As New Cls_Legami
        Legami.Importo(0) = ImportoDocumento
        Legami.NumeroDocumento(0) = NumeroDocumento
        Legami.NumeroPagamento(0) = X.NumeroRegistrazione
        Legami.Scrivi(Session("DC_GENERALE"), 0, X.NumeroRegistrazione)
    End Sub

    Protected Sub Btn_Esci_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles Btn_Esci.Click
        Response.Redirect("Menu_Strumenti.aspx")
    End Sub
End Class

