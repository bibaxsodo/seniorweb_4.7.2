﻿Imports Microsoft.VisualBasic
Imports System.Data.OleDb
Imports System.Web.Hosting
Imports System.Data.SqlTypes
Imports System.Net
Imports System.IO
Imports System.Security.Cryptography.X509Certificates
Imports System.Net.Security
Imports System.Web.Script.Serialization
Imports org.jivesoftware.util

Partial Class DatiAnagrafici
    Inherits System.Web.UI.Page

    Protected Sub Modifica()
        Dim x As New ClsOspite
        Dim ConnectionString As String = Session("DC_OSPITE")


        x.Leggi(ConnectionString, Session("CODICEOSPITE"))



        x.NOMEPADRE = Txt_Paternita.Text
        x.NOMEMADRE = Txt_Maternità.Text
        x.NOMECONIUGE = Txt_Coniuge.Text
        x.PROFCONIUGE = Txt_ProfessioneConiuge.Text

        x.Nazionalita = DD_Nazionalita.SelectedValue


        x.StrutturaProvenienza = Txt_StrutturaProvenienza.Text
        x.AssistenteSociale = Txt_AssistenteSociale.Text
        x.Distretto = Txt_Distretto.Text

        x.NumeroDocumento = Txt_Numero.Text
        x.DataDocumento = IIf(IsDate(Txt_Data.Text), Txt_Data.Text, Nothing)
        x.ScadenzaDocumento = IIf(IsDate(Txt_DataScadenza.Text), Txt_DataScadenza.Text, Nothing)
        x.TipoDocumento = DD_TipoDocumento.SelectedValue

        x.EmettitoreDocumento = Txt_Emettitore.Text

        x.DATAMORTE = IIf(Txt_Decesso.Text = "", Nothing, Txt_Decesso.Text)
        x.LuogoMorte = Txt_LuogoDecesso.Text
        x.USL = DD_CodiceUSL.SelectedValue
        x.CodiceCIG = Txt_CodiceCIG.Text

        x.StatoCivile = DD_StatoCivile.SelectedValue

        x.MastroCliente = 0
        x.ContoCliente = 0
        x.SottoContoCliente = 0
        If Txt_Sottoconto.Text.Trim <> "" Then
            Dim Vettore(100) As String
            Vettore = SplitWords(Txt_Sottoconto.Text)
            If Vettore.Length > 1 Then
                x.MastroCliente = Vettore(0)
                x.ContoCliente = Vettore(1)
                x.SottoContoCliente = Vettore(2)
            End If
        End If



        If chk_Opposizione730.Checked = True Then
            x.Opposizione730 = 1
        Else
            x.Opposizione730 = 0
        End If

        If RB_NOConsenso1.Checked = True Then
            x.CONSENSOINSERIMENTO = 2
        End If

        If RB_SIConsenso1.Checked = True Then
            x.CONSENSOINSERIMENTO = 1
        End If


        If RB_NOConsenso2.Checked = True Then
            x.CONSENSOMARKETING = 2
        End If

        If RB_SIConsenso2.Checked = True Then
            x.CONSENSOMARKETING = 1
        End If


        x.NOTE = Txt_Note.Text

        x.ScriviOspite(ConnectionString)

        ClientScript.RegisterClientScriptBlock(Me.GetType(), "Modifica", "alert('Modifica Effettuata');", True)
    End Sub

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Trim(Session("UTENTE")) = "" Then
            Response.Redirect("../Login.aspx")
            Exit Sub
        End If
        If Session("ABILITAZIONI").IndexOf("<PORTINERIA>") > 0 Then
            Response.Redirect("..\MainPortineria.aspx")
        End If

        If Page.IsPostBack = True Then Exit Sub


        If Val(Request.Item("CodiceOspite")) > 0 And Request.Item("CentroServizio") <> "" Then

            Session("CODICESERVIZIO") = Request.Item("CentroServizio")
            Session("CODICEOSPITE") = Val(Request.Item("CodiceOspite"))
        End If
        ViewState("CODICESERVIZIO") = Session("CODICESERVIZIO")
        ViewState("CODICEOSPITE") = Session("CODICEOSPITE")



        DD_TipoDocumento.Items.Clear()


        DD_TipoDocumento.Items.Add("CARTA DI IDENTITA'  ")
        DD_TipoDocumento.Items(DD_TipoDocumento.Items.Count - 1).Value = "IDENT"

        DD_TipoDocumento.Items.Add("CERTIFICATO D 'IDENTITA' ")
        DD_TipoDocumento.Items(DD_TipoDocumento.Items.Count - 1).Value = "CERID"

        DD_TipoDocumento.Items.Add("TESS. APP.TO AG.CUSTODIA    ")
        DD_TipoDocumento.Items(DD_TipoDocumento.Items.Count - 1).Value = "ACMIL"
        DD_TipoDocumento.Items.Add("TESS. SOTT.LI AG.CUSTODIA   ")
        DD_TipoDocumento.Items(DD_TipoDocumento.Items.Count - 1).Value = "ACSOT"
        DD_TipoDocumento.Items.Add("TESS. UFF.LI AG.CUSTODIA    ")
        DD_TipoDocumento.Items(DD_TipoDocumento.Items.Count - 1).Value = "ACUFF"
        DD_TipoDocumento.Items.Add("TESS. MILITARE TRUPPA A.M   ")
        DD_TipoDocumento.Items(DD_TipoDocumento.Items.Count - 1).Value = "AMMIL"
        DD_TipoDocumento.Items.Add("TESS.SOTTUFFICIALI a.M.")
        DD_TipoDocumento.Items(DD_TipoDocumento.Items.Count - 1).Value = "AMSOT"
        DD_TipoDocumento.Items.Add("TESS.UFFICIALI a.M.")
        DD_TipoDocumento.Items(DD_TipoDocumento.Items.Count - 1).Value = "AMUFF"
        DD_TipoDocumento.Items.Add("TESS. APP.TO CARABINIERI   ")
        DD_TipoDocumento.Items(DD_TipoDocumento.Items.Count - 1).Value = "CCMIL"
        DD_TipoDocumento.Items.Add("TESS. SOTTUFFICIALI CC  ")
        DD_TipoDocumento.Items(DD_TipoDocumento.Items.Count - 1).Value = "CCSOT"
        DD_TipoDocumento.Items.Add("TESS.UFFICIALE ")
        DD_TipoDocumento.Items(DD_TipoDocumento.Items.Count - 1).Value = "CCUFF"
        DD_TipoDocumento.Items.Add("TESS.AG.E AG.SC.C.f.s.")
        DD_TipoDocumento.Items(DD_TipoDocumento.Items.Count - 1).Value = "CFMIL"
        DD_TipoDocumento.Items.Add("TESS.SOTTUFICIALI C.f.s.")
        DD_TipoDocumento.Items(DD_TipoDocumento.Items.Count - 1).Value = "CFSOT"
        DD_TipoDocumento.Items.Add("TESS.UFFICIALI C.f.s.")
        DD_TipoDocumento.Items(DD_TipoDocumento.Items.Count - 1).Value = "CFUFF"
        DD_TipoDocumento.Items.Add("CARTA ID. DIPLOMATICA   ")
        DD_TipoDocumento.Items(DD_TipoDocumento.Items.Count - 1).Value = "CIDIP"
        DD_TipoDocumento.Items.Add("TESS.s.I.s.D.E.")
        DD_TipoDocumento.Items(DD_TipoDocumento.Items.Count - 1).Value = "DESIS"
        DD_TipoDocumento.Items.Add("TESS.MILITARE E.I.")
        DD_TipoDocumento.Items(DD_TipoDocumento.Items.Count - 1).Value = "EIMIL"
        DD_TipoDocumento.Items.Add("TESS.SOTTUFFICIALI E.I.")
        DD_TipoDocumento.Items(DD_TipoDocumento.Items.Count - 1).Value = "EISOT"
        DD_TipoDocumento.Items.Add("TESS.UFFICIALI E.I.")
        DD_TipoDocumento.Items(DD_TipoDocumento.Items.Count - 1).Value = "EIUFF"
        DD_TipoDocumento.Items.Add("TESS. APP.TO FINANZIERE ")
        DD_TipoDocumento.Items(DD_TipoDocumento.Items.Count - 1).Value = "GFMIL"
        DD_TipoDocumento.Items.Add("TESS.SOTT.LI g.D.f.")
        DD_TipoDocumento.Items(DD_TipoDocumento.Items.Count - 1).Value = "GFSOT"
        DD_TipoDocumento.Items.Add("TESS.POL.TRIB.g.D.f.")
        DD_TipoDocumento.Items(DD_TipoDocumento.Items.Count - 1).Value = "GFTRI"
        DD_TipoDocumento.Items.Add("TESS.UFFICIALI g.D.f.")
        DD_TipoDocumento.Items(DD_TipoDocumento.Items.Count - 1).Value = "GFUFF"
        DD_TipoDocumento.Items.Add("CARTA IDENTITA ' ELETTRONICA ")
        DD_TipoDocumento.Items(DD_TipoDocumento.Items.Count - 1).Value = "IDELE"
        DD_TipoDocumento.Items.Add("TESS.PERS.MAGISTRATI ")
        DD_TipoDocumento.Items(DD_TipoDocumento.Items.Count - 1).Value = "MAGIS"
        DD_TipoDocumento.Items.Add("TESS.MILIT.M.M.")
        DD_TipoDocumento.Items(DD_TipoDocumento.Items.Count - 1).Value = "MMMIL"
        DD_TipoDocumento.Items.Add("TESS.SOTTUFICIALI M.M.")
        DD_TipoDocumento.Items(DD_TipoDocumento.Items.Count - 1).Value = "MMSOT"
        DD_TipoDocumento.Items.Add("TESS.UFFICIALI M.M.")
        DD_TipoDocumento.Items(DD_TipoDocumento.Items.Count - 1).Value = "MMUFF"
        DD_TipoDocumento.Items.Add("TESS.PARLAMENTARI ")
        DD_TipoDocumento.Items(DD_TipoDocumento.Items.Count - 1).Value = "PARLA"
        DD_TipoDocumento.Items.Add("PASSAPORTO DIPLOMATICO  ")
        DD_TipoDocumento.Items(DD_TipoDocumento.Items.Count - 1).Value = "PASDI"
        DD_TipoDocumento.Items.Add("PASSAPORTO ORDINARIO    ")
        DD_TipoDocumento.Items(DD_TipoDocumento.Items.Count - 1).Value = "PASOR"
        DD_TipoDocumento.Items.Add("PASSAPORTO DI SERVIZIO  ")
        DD_TipoDocumento.Items(DD_TipoDocumento.Items.Count - 1).Value = "PASSE"
        DD_TipoDocumento.Items.Add("PATENTE DI GUIDA    ")
        DD_TipoDocumento.Items(DD_TipoDocumento.Items.Count - 1).Value = "PATEN"
        DD_TipoDocumento.Items.Add("PATENTE NAUTICA ")
        DD_TipoDocumento.Items(DD_TipoDocumento.Items.Count - 1).Value = "PATNA"
        DD_TipoDocumento.Items.Add("TESS. AGENTI/ASS.TI P.P.    ")
        DD_TipoDocumento.Items(DD_TipoDocumento.Items.Count - 1).Value = "PPAGE"
        DD_TipoDocumento.Items.Add("TESS.ISPETTORI P.P.")
        DD_TipoDocumento.Items(DD_TipoDocumento.Items.Count - 1).Value = "PPISP"
        DD_TipoDocumento.Items.Add("TESS.SOVRINTENDENTI P.P.")
        DD_TipoDocumento.Items(DD_TipoDocumento.Items.Count - 1).Value = "PPSOV"
        DD_TipoDocumento.Items.Add("TESS.UFFICIALI P.P.")
        DD_TipoDocumento.Items(DD_TipoDocumento.Items.Count - 1).Value = "PPUFF"
        DD_TipoDocumento.Items.Add("TESS. AGENTI/ASS.TI P.S.    ")
        DD_TipoDocumento.Items(DD_TipoDocumento.Items.Count - 1).Value = "PSAPP"
        DD_TipoDocumento.Items.Add("TESS. POLIZIA FEMMINILE ")
        DD_TipoDocumento.Items(DD_TipoDocumento.Items.Count - 1).Value = "PSFEM"
        DD_TipoDocumento.Items.Add("TESS.FUNZIONARI P.s.")
        DD_TipoDocumento.Items(DD_TipoDocumento.Items.Count - 1).Value = "PSFUN"
        DD_TipoDocumento.Items.Add("TESS.ISPETTORI P.s.")
        DD_TipoDocumento.Items(DD_TipoDocumento.Items.Count - 1).Value = "PSISP"
        DD_TipoDocumento.Items.Add("TESS.SOVRINTENDENTI P.s.")
        DD_TipoDocumento.Items(DD_TipoDocumento.Items.Count - 1).Value = "PSSOT"
        DD_TipoDocumento.Items.Add("TESS.UFFICIALI P.s.")
        DD_TipoDocumento.Items(DD_TipoDocumento.Items.Count - 1).Value = "PSUFF"
        DD_TipoDocumento.Items.Add("TESS. MILIT. TRUPPA SISMI   ")
        DD_TipoDocumento.Items(DD_TipoDocumento.Items.Count - 1).Value = "SDMIL"
        DD_TipoDocumento.Items.Add("TESS. SOTTUFFICIALI SISMI   ")
        DD_TipoDocumento.Items(DD_TipoDocumento.Items.Count - 1).Value = "SDSOT"
        DD_TipoDocumento.Items.Add("TESS. UFFICIALI SISMI   ")
        DD_TipoDocumento.Items(DD_TipoDocumento.Items.Count - 1).Value = "SDUFF"
        DD_TipoDocumento.Items.Add("TESS.ISCR.ALBO MED / CHI.")
        DD_TipoDocumento.Items(DD_TipoDocumento.Items.Count - 1).Value = "TEAMC"
        DD_TipoDocumento.Items.Add("TESS.ISCRIZ.ALBO ODONT.")
        DD_TipoDocumento.Items(DD_TipoDocumento.Items.Count - 1).Value = "TEAOD"
        DD_TipoDocumento.Items.Add("TES. UNICO PER LA CAMERA    ")
        DD_TipoDocumento.Items(DD_TipoDocumento.Items.Count - 1).Value = "TECAM"
        DD_TipoDocumento.Items.Add("TESS. CORTE DEI CONTI   ")
        DD_TipoDocumento.Items(DD_TipoDocumento.Items.Count - 1).Value = "TECOC"
        DD_TipoDocumento.Items.Add("TES.DOGANALE RIL.Min.FIN.")
        DD_TipoDocumento.Items(DD_TipoDocumento.Items.Count - 1).Value = "TEDOG"
        DD_TipoDocumento.Items.Add("TESS.FERROV.SENATO ")
        DD_TipoDocumento.Items(DD_TipoDocumento.Items.Count - 1).Value = "TEFSE"
        DD_TipoDocumento.Items.Add("TESS.Min.PUBB.ISTRUZIONE ")
        DD_TipoDocumento.Items(DD_TipoDocumento.Items.Count - 1).Value = "TEMPI"
        DD_TipoDocumento.Items.Add("TESS. MILITARE NATO ")
        DD_TipoDocumento.Items(DD_TipoDocumento.Items.Count - 1).Value = "TENAT"
        DD_TipoDocumento.Items.Add("TES. ENTE NAZ. ASSIS.VOLO   ")
        DD_TipoDocumento.Items(DD_TipoDocumento.Items.Count - 1).Value = "TENAV"
        DD_TipoDocumento.Items.Add("TESS.Min.POLIT.AGRIC.FOR.")
        DD_TipoDocumento.Items(DD_TipoDocumento.Items.Count - 1).Value = "TEPOL"
        DD_TipoDocumento.Items.Add("TESS. MIN. AFFARI ESTERI    ")
        DD_TipoDocumento.Items(DD_TipoDocumento.Items.Count - 1).Value = "TESAE"
        DD_TipoDocumento.Items.Add("TESS.ISCR.ALBO ARCHITETTI   ")
        DD_TipoDocumento.Items(DD_TipoDocumento.Items.Count - 1).Value = "TESAR"
        DD_TipoDocumento.Items.Add("TESSERA ISCR. ALBO AVVOC.   ")
        DD_TipoDocumento.Items(DD_TipoDocumento.Items.Count - 1).Value = "TESAV"
        DD_TipoDocumento.Items.Add("TESS.CORTE D 'APPELLO   ")
        DD_TipoDocumento.Items(DD_TipoDocumento.Items.Count - 1).Value = "TESCA"
        DD_TipoDocumento.Items.Add("TESS. CONSIGLIO DI STATO    ")
        DD_TipoDocumento.Items(DD_TipoDocumento.Items.Count - 1).Value = "TESCS"
        DD_TipoDocumento.Items.Add("TESSERA RICONOSC.D.I.a.")
        DD_TipoDocumento.Items(DD_TipoDocumento.Items.Count - 1).Value = "TESDI"
        DD_TipoDocumento.Items.Add("TESS. MEMBRO EQUIP. AEREO   ")
        DD_TipoDocumento.Items(DD_TipoDocumento.Items.Count - 1).Value = "TESEA"
        DD_TipoDocumento.Items.Add("TESS.ISCR. ALBO INGEGNERI   ")
        DD_TipoDocumento.Items(DD_TipoDocumento.Items.Count - 1).Value = "TESIN"
        DD_TipoDocumento.Items.Add("TESS. MINISTERO LAVORI PU   ")
        DD_TipoDocumento.Items(DD_TipoDocumento.Items.Count - 1).Value = "TESLP"
        DD_TipoDocumento.Items.Add("TESS.Min.BEN.E ATT.CULT.")
        DD_TipoDocumento.Items(DD_TipoDocumento.Items.Count - 1).Value = "TESMB"
        DD_TipoDocumento.Items.Add("TESS. MINISTERO DIFESA  ")
        DD_TipoDocumento.Items(DD_TipoDocumento.Items.Count - 1).Value = "TESMD"
        DD_TipoDocumento.Items.Add("TESS. MINISTERO FINANZE ")
        DD_TipoDocumento.Items(DD_TipoDocumento.Items.Count - 1).Value = "TESMF"
        DD_TipoDocumento.Items.Add("TESS. MINISTERO GIUSTIZIA   ")
        DD_TipoDocumento.Items(DD_TipoDocumento.Items.Count - 1).Value = "TESMG"
        DD_TipoDocumento.Items.Add("TESS. MINISTERO INTERNO ")
        DD_TipoDocumento.Items(DD_TipoDocumento.Items.Count - 1).Value = "TESMI"
        DD_TipoDocumento.Items.Add("TESS. MINIST. TRASP/NAVIG   ")
        DD_TipoDocumento.Items(DD_TipoDocumento.Items.Count - 1).Value = "TESMN"
        DD_TipoDocumento.Items.Add("TESS.MINISTERO SANITA ' ")
        DD_TipoDocumento.Items(DD_TipoDocumento.Items.Count - 1).Value = "TESMS"
        DD_TipoDocumento.Items.Add("TESS. MINISTERO TESORO  ")
        DD_TipoDocumento.Items(DD_TipoDocumento.Items.Count - 1).Value = "TESMT"
        DD_TipoDocumento.Items.Add("TESSERA DELL 'ORDINE NOTAI   ")
        DD_TipoDocumento.Items(DD_TipoDocumento.Items.Count - 1).Value = "TESNO"
        DD_TipoDocumento.Items.Add("TESS. ORDINE GIORNALISTI    ")
        DD_TipoDocumento.Items(DD_TipoDocumento.Items.Count - 1).Value = "TESOG"
        DD_TipoDocumento.Items.Add("TESS.PRES.ZA CONS.Min.")
        DD_TipoDocumento.Items(DD_TipoDocumento.Items.Count - 1).Value = "TESPC"
        DD_TipoDocumento.Items.Add("TESS. PUBBLICA ISTRUZIONE   ")
        DD_TipoDocumento.Items(DD_TipoDocumento.Items.Count - 1).Value = "TESPI"
        DD_TipoDocumento.Items.Add("TES. POSTE E TELECOMUNIC.   ")
        DD_TipoDocumento.Items(DD_TipoDocumento.Items.Count - 1).Value = "TESPT"
        DD_TipoDocumento.Items.Add("TESSERA U.N.U.C.I.")
        DD_TipoDocumento.Items(DD_TipoDocumento.Items.Count - 1).Value = "TESUN"
        DD_TipoDocumento.Items.Add("TESS.IDENTIF.TELECOM IT.")
        DD_TipoDocumento.Items(DD_TipoDocumento.Items.Count - 1).Value = "TETEL"
        DD_TipoDocumento.Items.Add("TES. FERROVIARIA DEPUTATI   ")
        DD_TipoDocumento.Items(DD_TipoDocumento.Items.Count - 1).Value = "TFERD"
        DD_TipoDocumento.Items.Add("TES. FERROV. EX DEPUTATI    ")
        DD_TipoDocumento.Items(DD_TipoDocumento.Items.Count - 1).Value = "TFEXD"
        DD_TipoDocumento.Items.Add("TESS. APP.TO/VIG. URBANO    ")
        DD_TipoDocumento.Items(DD_TipoDocumento.Items.Count - 1).Value = "VIMIL"
        DD_TipoDocumento.Items.Add("TESS. SOTT.LI VIG. URBANI   ")
        DD_TipoDocumento.Items(DD_TipoDocumento.Items.Count - 1).Value = "VISOT"
        DD_TipoDocumento.Items.Add("TESS. UFF.LI VIG.URBANI ")
        DD_TipoDocumento.Items(DD_TipoDocumento.Items.Count - 1).Value = "VIUFF"
        DD_TipoDocumento.Items.Add("TESS. APP.TO/VIG. VV.FF.    ")
        DD_TipoDocumento.Items(DD_TipoDocumento.Items.Count - 1).Value = "VVMIL"
        DD_TipoDocumento.Items.Add("TESS.SOTTUFF.LI VV.FF.")
        DD_TipoDocumento.Items(DD_TipoDocumento.Items.Count - 1).Value = "VVSOT"
        DD_TipoDocumento.Items.Add("TESS.UFFICIALI VV.FF.")
        DD_TipoDocumento.Items(DD_TipoDocumento.Items.Count - 1).Value = "VVUFF"
        DD_TipoDocumento.Items.Add("")
        DD_TipoDocumento.Items(DD_TipoDocumento.Items.Count - 1).Value = ""

        Dim K1 As New Cls_SqlString

        Dim Barra As New Cls_BarraSenior

        If Not IsNothing(Session("RicercaAnagraficaSQLString")) Then
            K1 = Session("RicercaAnagraficaSQLString")
        End If

        Lbl_BarraSenior.Text = Barra.CodiceBarra(Application("SENIOR"), Session("UTENTE"), Page, K1)


        Dim x As New ClsOspite
        Dim ConnectionString As String = Session("DC_OSPITE")

        x.Leggi(ConnectionString, Session("CODICEOSPITE"))

        Dim cn As OleDbConnection



        cn = New Data.OleDb.OleDbConnection(Session("DC_OSPITE"))

        cn.Open()
        Dim cmd As New OleDbCommand()

        cmd.CommandText = ("select * from TABELLE Where TipTab = 'NAZ' Order by Descrizione")
        cmd.Connection = cn
        Dim sb As StringBuilder = New StringBuilder


        DD_Nazionalita.Items.Clear()
        Dim myPOSTreader As OleDbDataReader = cmd.ExecuteReader()
        Do While myPOSTreader.Read
            DD_Nazionalita.Items.Add(myPOSTreader.Item("Descrizione"))
            DD_Nazionalita.Items(DD_Nazionalita.Items.Count - 1).Value = myPOSTreader.Item("CODtab")
        Loop
        myPOSTreader.Close()

        DD_Nazionalita.Items.Add("")
        DD_Nazionalita.Items(DD_Nazionalita.Items.Count - 1).Value = ""
        DD_Nazionalita.Items(DD_Nazionalita.Items.Count - 1).Selected = True



        Dim cmd1 As New OleDbCommand()

        cmd1.CommandText = ("select * from TABELLE Where TipTab = 'STC' Order by Descrizione")
        cmd1.Connection = cn


        DD_StatoCivile.Items.Clear()
        Dim myPOSTreader1 As OleDbDataReader = cmd1.ExecuteReader()
        Do While myPOSTreader1.Read
            DD_StatoCivile.Items.Add(myPOSTreader1.Item("Descrizione"))
            DD_StatoCivile.Items(DD_StatoCivile.Items.Count - 1).Value = myPOSTreader1.Item("CODtab")
        Loop
        myPOSTreader1.Close()
        cn.Close()
        DD_StatoCivile.Items.Add("")
        DD_StatoCivile.Items(DD_StatoCivile.Items.Count - 1).Value = ""
        DD_StatoCivile.Items(DD_StatoCivile.Items.Count - 1).Selected = True


        cn.Close()

        Dim p As New ClsUSL

        p.UpDateDropBox(ConnectionString, DD_CodiceUSL)

        Txt_Paternita.Text = x.NOMEPADRE
        Txt_Maternità.Text = x.NOMEMADRE
        Txt_Coniuge.Text = x.NOMECONIUGE
        Txt_ProfessioneConiuge.Text = x.PROFCONIUGE

        DD_Nazionalita.SelectedValue = x.Nazionalita

        Txt_Numero.Text = x.NumeroDocumento
        Txt_Data.Text = IIf(Year(x.DataDocumento) > 1900, Format(x.DataDocumento, "dd/MM/yyyy"), "")

        Txt_DataScadenza.Text = IIf(Year(x.ScadenzaDocumento) > 1900, Format(x.ScadenzaDocumento, "dd/MM/yyyy"), "")

        DD_TipoDocumento.SelectedValue = x.TipoDocumento


        Txt_Emettitore.Text = x.EmettitoreDocumento

        Txt_Decesso.Text = x.DATAMORTE
        Txt_LuogoDecesso.Text = x.LuogoMorte
        DD_CodiceUSL.SelectedValue = x.USL
        Txt_CodiceCIG.Text = x.CodiceCIG

        Txt_StrutturaProvenienza.Text = x.StrutturaProvenienza
        Txt_AssistenteSociale.Text = x.AssistenteSociale
        Txt_Distretto.Text = x.Distretto

        Dim XDex As New Cls_Pianodeiconti

        XDex.Mastro = x.MastroCliente
        XDex.Conto = x.ContoCliente
        XDex.Sottoconto = x.SottoContoCliente
        XDex.Decodfica(Session("DC_GENERALE"))
        Txt_Sottoconto.Text = x.MastroCliente & " " & x.ContoCliente & " " & x.SottoContoCliente & " " & XDex.Descrizione


        Txt_Note.Text = x.NOTE


        DD_StatoCivile.SelectedValue = x.StatoCivile

        Dim x1 As New ClsOspite

        x1.CodiceOspite = Session("CODICEOSPITE")
        x1.Leggi(Session("DC_OSPITE"), x.CodiceOspite)

        Dim cs As New Cls_CentroServizio

        cs.Leggi(Session("DC_OSPITE"), Session("CODICESERVIZIO"))

        Lbl_NomeOspite.Text = x1.Nome & " -  " & x1.DataNascita & " - " & cs.DESCRIZIONE & "<i style=""font-size:small;"">(" & x1.CodiceOspite & ")</i>"


        If x.Opposizione730 = 1 Then
            chk_Opposizione730.Checked = True
        Else
            chk_Opposizione730.Checked = False
        End If



        If x.CONSENSOINSERIMENTO = 2 Then
            RB_NOConsenso1.Checked = True
        End If

        If x.CONSENSOINSERIMENTO = 1 Then
            RB_SIConsenso1.Checked = True
        End If



        If x.CONSENSOMARKETING = 0 Or x.CONSENSOMARKETING = 2 Then
            RB_NOConsenso2.Checked = True
        End If

        If x.CONSENSOMARKETING = 1 Then
            RB_SIConsenso2.Checked = True
        End If


        If Val(Session("GDPRAttivo")) = 0 Then
            RB_NOConsenso1.Enabled = False
            RB_NOConsenso2.Enabled = False
            RB_SIConsenso1.Enabled = False
            RB_SIConsenso2.Enabled = False
            IB_Download.Enabled = False            
            DD_Report.Enabled = False
        Else
            DD_Report.Items.Add("Informativa Ospite Fornita da Ospite")

            Dim M As New Cls_Tabella_GDPR

            M.UpDropDownListInAggiunta(Session("DC_OSPITE"), DD_Report)

        End If

        


        Call EseguiJS()

    End Sub





    Protected Sub Btn_Modifica_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles Btn_Modifica.Click
        If Txt_Decesso.Text <> "" Then
            If Not IsDate(Txt_Decesso.Text) Then
                REM Lbl_Errore.Text = "Specificare data addebito"
                ClientScript.RegisterClientScriptBlock(Me.GetType(), "Errore", "VisualizzaErrore('Specificare data decesso');", True)
                Exit Sub
            End If
        End If


        If Txt_Data.Text <> "" Then
            If Not IsDate(Txt_Data.Text) Then
                REM Lbl_Errore.Text = "Specificare data addebito"
                ClientScript.RegisterClientScriptBlock(Me.GetType(), "Errore", "VisualizzaErrore('Specificare data documento');", True)
                Exit Sub
            End If
        End If

        If Not IsNothing(Session("ABILITAZIONI")) Then
            If Session("ABILITAZIONI").IndexOf("<EPERSONAM>") >= 0 Then
                'If Not ModificaOspite() Then
                '    ClientScript.RegisterClientScriptBlock(Me.GetType(), "Errore", "VisualizzaErrore('Errore nel dialogo con Epersonam');", True)
                '    Exit Sub
                'End If
            End If
        End If

        Session("CODICESERVIZIO") = ViewState("CODICESERVIZIO")
        Session("CODICEOSPITE") = ViewState("CODICEOSPITE")
        If Val(Session("CODICEOSPITE")) = 0 Then
            ClientScript.RegisterClientScriptBlock(Me.GetType(), "Errore", "VisualizzaErrore('Devi ricercare l ospite');", True)
            Exit Sub
        End If

        If Val(Session("GDPRAttivo")) = 1 Then
            If RB_SIConsenso1.Checked = False Then
                ClientScript.RegisterClientScriptBlock(Me.GetType(), "Errore", "VisualizzaErrore('Devi indicare il consenso all inserimento dei dati in Senior');", True)
                Exit Sub
            End If
        End If

        Dim Log1 As New Cls_LogPrivacy

        Log1.LogPrivacy(Application("SENIOR"), Session("CLIENTE"), Session("UTENTE"), Session("UTENTEIMPER"), Page.GetType.Name.ToUpper, Val(Session("CODICEOSPITE")), 0, "", "", 0, "", "M", "OSPITE", "")



        'Dim xPiano As New Cls_Pianodeiconti


        'If Txt_Sottoconto.Text.Trim = "" Then
        '    ClientScript.RegisterClientScriptBlock(Me.GetType(), "Errore", "VisualizzaErrore('Conto inesistente');", True)
        '    Exit Sub
        'End If

        'If Txt_Sottoconto.Text.Trim <> "" Then
        '    Dim Vettore(100) As String

        '    Vettore = SplitWords(Txt_Sottoconto.Text)
        '    If Vettore.Length > 1 Then               
        '        xPiano.Descrizione = ""
        '        xPiano.Mastro = Vettore(0)
        '        xPiano.Conto = Vettore(1)
        '        xPiano.Sottoconto = Vettore(2)
        '        xPiano.Decodfica(Session("DC_GENERALE"))
        '        If xPiano.Descrizione = "" Then
        '            ClientScript.RegisterClientScriptBlock(Me.GetType(), "Errore", "VisualizzaErrore('Conto inesistente');", True)
        '            Exit Sub
        '        End If
        '    End If
        'End If

        Call Modifica()

        Call EseguiJS()
    End Sub

    Private Function SplitWords(ByVal s As String) As String()
        '
        ' Call Regex.Split function from the imported namespace.
        ' Return the result array.
        '
        Return Regex.Split(s, "\W+")
    End Function
    Private Sub EseguiJS()
        Dim MyJs As String
        MyJs = "$(document).ready(function() {"

        MyJs = MyJs & "var els = document.getElementsByTagName(""*"");"




        MyJs = MyJs & "for (var i=0;i<els.length;i++)"
        MyJs = MyJs & "if ( els[i].id ) { "
        MyJs = MyJs & " var appoggio =els[i].id; "
        MyJs = MyJs & " if ((appoggio.match('Txt_Data')!= null) || (appoggio.match('Txt_Decesso')!= null))  {  "
        MyJs = MyJs & " $(els[i]).mask(""99/99/9999"");"
        MyJs = MyJs & " $(els[i]).datepicker({ changeMonth: true,changeYear: true,  yearRange: ""1990:" & Year(Now) + 11 & """},$.datepicker.regional[""it""]);"
        MyJs = MyJs & "    }"
        MyJs = MyJs & " if ((appoggio.match('Txt_Deposito')!= null) ) {  "
        MyJs = MyJs & " $(els[i]).keypress(function() { ForceNumericInput($(this).val(), true, true); } ); "
        MyJs = MyJs & " $(els[i]).blur(function() { var ap = formatNumber($(this).val(),2); $(this).val(ap); });"
        MyJs = MyJs & "    }"

        MyJs = MyJs & " if (appoggio.match('Sottoconto')!= null) {   "
        MyJs = MyJs & " $(els[i]).autocomplete('/WebHandler/GestioneConto.ashx?Utente=" & Session("UTENTE") & "', {delay:5,minChars:3});"
        MyJs = MyJs & "    }"

        MyJs = MyJs & " if ((appoggio.match('Txt_Comune')!= null) || (appoggio.match('Txt_ComRes')!= null))  {  "
        MyJs = MyJs & " $(els[i]).autocomplete('/WebHandler/AutocompleteComune.ashx?Utente=" & Session("UTENTE") & "', {delay:5,minChars:3});"
        MyJs = MyJs & "    }"

        MyJs = MyJs & "} "

 


        MyJs = MyJs & "});"

        'MyJs = "$(document).ready(function() { $('.myClass').keypress(function() { handleEnter($('.myClass').val(), true, true); } );     $('#" & Txt_ImportoPagato.ClientID & "').blur(function() { var ap = formatNumber ($('#" & Txt_ImportoPagato.ClientID & "').val(),2); $('#" & Txt_ImportoPagato.ClientID & "').val(ap); }); });"
        ScriptManager.RegisterStartupScript(Me, Me.GetType(), "visualizzaRitIm", MyJs, True)
    End Sub


    Protected Sub Btn_Esci_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles Btn_Esci.Click
        If Request.Item("PAGINA") = "" Then
            Response.Redirect("RicercaAnagrafica.aspx")
        Else
            Response.Redirect("RicercaAnagrafica.aspx?CHIAMATA=RITORNO&PAGINA=" & Request.Item("PAGINA"))
        End If
    End Sub


    Private Function ModificaOspite() As Boolean

        ModificaOspite = True
        Try

            LoginPersonam()

            Dim Data() As Byte

            Dim request As New NameValueCollection

            Dim JSon As String
            Dim Provincia As String = ""
            Dim Comune As String = ""




            JSon = "{""ass_soc_fullname"":""" & Txt_AssistenteSociale.Text & """}"

            
            'birthcity_id ...


            Data = System.Text.Encoding.ASCII.GetBytes(JSon)


            Dim M As New ClsOspite

            M.CodiceOspite = Session("CODICEOSPITE")
            M.Leggi(Session("DC_OSPITE"), M.CodiceOspite)


            System.Net.ServicePointManager.ServerCertificateValidationCallback = AddressOf CertificateHandler

            Dim Cs As New Cls_CentroServizio

            Cs.CENTROSERVIZIO = Session("CODICESERVIZIO")
            Cs.Leggi(Session("DC_OSPITE"), Cs.CENTROSERVIZIO)


            Dim client As HttpWebRequest = WebRequest.Create("https://api-v0.e-personam.com/v0/business_units/" & Cs.EPersonam & "/guests/" & M.CODICEFISCALE)

            client.Method = "PUT"
            client.Headers.Add("Authorization", "Bearer " & Session("access_token"))
            client.ContentType = "Content-Type: application/json"

            client.ContentLength = Data.Length

            Dim dataStream As Stream = client.GetRequestStream()


            dataStream.Write(Data, 0, Data.Length)
            dataStream.Close()

            Dim response As HttpWebResponse = client.GetResponse()

            Dim respStream As Stream = response.GetResponseStream()
            Dim reader As StreamReader = New StreamReader(respStream)
            Dim appoggio As String
            appoggio = reader.ReadToEnd()


        Catch ex As Exception
            ModificaOspite = False
        End Try

    End Function
    Private Sub LoginPersonam()
        'Dim request As WebRequest
        '-staging
        'request = HttpWebRequest.Create("https://api-v0.e-personam.com/v0/oauth/token")
        Dim request As New NameValueCollection

        Dim BlowFish As New Blowfish("advenias2014")



        'request.Method = "POST"
        request.Add("client_id", "98217ca6670c660e22f42d58e231d4e56c84fb34e216b0f66f1f30284fd3ec9d")
        request.Add("client_secret", "5570a684f69ca74d75d16bba669c156ec092eccb4111d0974adec3edb2fa4d6f")
        request.Add("grant_type", "authorization_code")


        Dim guarda As String

        If Trim(Session("EPersonamPSWCRYPT")) = "" Then
            request.Add("username", Session("UTENTE").ToString.Replace("<1>", "").Replace("<2>", "").Replace("<3>", "").Replace("<4>", "").Replace("<5>", "").Replace("<6>", ""))

            guarda = Session("ChiaveCr")
        Else
            request.Add("username", Session("EPersonamUser"))

            guarda = Session("EPersonamPSWCRYPT")
        End If

        request.Add("code", guarda)
        System.Net.ServicePointManager.ServerCertificateValidationCallback = AddressOf CertificateHandler
        'curl -i -k https://api-v0.e-personam.com/v0/oauth/token -F grant_type=password  -F client_id=98217ca6670c660e22f42d58e231d4e56c84fb34e216b0f66f1f30284fd3ec9d -F client_secret=5570a684f69ca74d75d16bba669c156ec092eccb4111d0974adec3edb2fa4d6f -F username=paolo -F password=cristina

        'Dim responsemia As System.Net.WebResponse
        'responsemia = request.GetResponse()


        Dim client As New WebClient()

        Dim result As Object = client.UploadValues("https://api-v0.e-personam.com/v0/oauth/token", "POST", request)


        Dim stringa As String = Encoding.Default.GetString(result)


        Dim serializer As JavaScriptSerializer


        serializer = New JavaScriptSerializer()


        Dim s As Autentificazione = serializer.Deserialize(Of Autentificazione)(stringa)


        Session("access_token") = s.access_token


    End Sub
    Public Class Autentificazione
        Public access_token As String
        Public expires_in As String
        Public scope As String
        Public token_type As String
        Public refresh_token As String
    End Class
    Private Shared Function CertificateHandler(ByVal sender As Object, ByVal certificate As X509Certificate, ByVal chain As X509Chain, ByVal SSLerror As SslPolicyErrors) As Boolean
        Return True
    End Function



    Private Sub ExportPDF(ByVal NomeFile As String)
        Dim contenuto As String
        If Val(NomeFile) > 0 Then
            Dim Personalizzazione As New Cls_Tabella_GDPR

            Personalizzazione.Id = Val(NomeFile)
            Personalizzazione.Leggi(Session("DC_OSPITE"))

            contenuto = Personalizzazione.Testo
        Else

            Dim objStreamReader As StreamReader


            objStreamReader = File.OpenText(Server.MapPath(NomeFile))

            ' Leggo il contenuto del file sino alla fine (ReadToEnd)
            contenuto = objStreamReader.ReadToEnd()

            objStreamReader.Close()
        End If


        Dim TabSoc As New Cls_TabellaSocieta

        TabSoc.Leggi(Session("DC_TABELLE"))

        Dim Osp As New ClsOspite

        Osp.CodiceOspite = Session("CODICEOSPITE")
        Osp.Leggi(Session("DC_OSPITE"), Osp.CodiceOspite)


        contenuto = contenuto.Replace("@NOMEOSPITE@", Osp.Nome)
        contenuto = contenuto.Replace("@NOMETITOLARE@", TabSoc.RagioneSociale)
        contenuto = contenuto.Replace("@INDIRIZZO@", TabSoc.Indirizzo)
        contenuto = contenuto.Replace("@CAP@", TabSoc.Cap)
        contenuto = contenuto.Replace("@CITTA@", TabSoc.Localita)
        contenuto = contenuto.Replace("@TEL@", TabSoc.Telefono)
        contenuto = contenuto.Replace("@EMAIL@", TabSoc.EMail)


        contenuto = contenuto.Replace("é", "&eacute;")
        contenuto = contenuto.Replace("è", "&egrave;")
        contenuto = contenuto.Replace("à", "&agrave;")
        contenuto = contenuto.Replace("ò", "&ograve;")
        contenuto = contenuto.Replace("ù", "&ugrave;")
        contenuto = contenuto.Replace("ì", "&igrave;")

        contenuto = contenuto.Replace("é", "&eacute;")
        contenuto = contenuto.Replace("È", "&Egrave;")
        contenuto = contenuto.Replace("À", "&Aacute;")
        contenuto = contenuto.Replace("Ò", "&Ograve;")
        contenuto = contenuto.Replace("Ù", "&Ugrave;")
        contenuto = contenuto.Replace("Ì", "&Igrave;")
        contenuto = contenuto.Replace("'", "&#39;")

        contenuto = contenuto.Replace("´", "&#180;")
        contenuto = contenuto.Replace("`", "&#96;")

        contenuto = Server.UrlEncode(contenuto.Replace(vbNewLine, ""))

        Session("PdfDaHTML") = contenuto
    End Sub



    Public Function SHA1(ByVal StringaDaConvertire As String) As String
        Dim sha2 As New System.Security.Cryptography.SHA1CryptoServiceProvider()
        Dim hash() As Byte
        Dim bytes() As Byte
        Dim output As String = ""
        Dim i As Integer

        bytes = System.Text.Encoding.UTF8.GetBytes(StringaDaConvertire)
        hash = sha2.ComputeHash(bytes)

        For i = 0 To hash.Length - 1
            output = output & hash(i).ToString("x2")
        Next

        Return output
    End Function

    Protected Sub IB_Download_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles IB_Download.Click
        If DD_Report.SelectedIndex = 0 Then
            ExportPDF("..\modulo\Informativa_GDPR_DatiOSPITEfornitidaOspite.html")
        Else
            ExportPDF(DD_Report.SelectedIndex)
        End If


    End Sub
End Class
