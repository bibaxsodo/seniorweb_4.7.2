﻿Imports Microsoft.VisualBasic
Imports System.Data.OleDb
Imports System.Web.Hosting

Partial Class OspitiWeb_CreaDocumentoRetta
    Inherits System.Web.UI.Page


    Private Sub Calcolo(ByVal Mese As Integer, ByVal Anno As Integer)
        Dim XS As New Cls_CalcoloRette
        Dim VarCentroServizio As String = Session("CODICESERVIZIO")
        Dim varcodiceospite As Long = Session("CODICEOSPITE")
        Dim MyCserv As New Cls_CentroServizio
        Dim f As New Cls_Parametri

        If Mese > 13 Or Mese < 1 Then
            Exit Sub
        End If

        If Anno > 2030 Or Anno < 2000 Then
            Exit Sub
        End If


        Dim MEm As New Cls_EmissioneRetta
        Dim MyStVb6 As New Cls_FunzioniVB6


        Dim CentroServizio As New Cls_CentroServizio
        Dim MastroPartita As Integer
        Dim ContoPartita As Integer
        Dim SottocontoPartita As Long

        CentroServizio.Leggi(Session("DC_OSPITE"), Session("CODICESERVIZIO"))

        MastroPartita = CentroServizio.MASTRO
        ContoPartita = CentroServizio.CONTO
        SottocontoPartita = Session("CODICEOSPITE") * 100
        If Val(Request.Item("CodiceParente")) < 99 And Val(Request.Item("CodiceParente")) > 0 Then
            SottocontoPartita = (Session("CODICEOSPITE") * 100) + Val(Request.Item("CodiceParente"))
        End If

        MEm.ConnessioneGenerale = Session("DC_GENERALE")
        MEm.ConnessioneOspiti = Session("DC_OSPITE")
        MEm.ConnessioneTabelle = Session("DC_TABELLE")

        MEm.ApriDB()
        lbl_Errore.Text = ""
        Chk_Conferma.Visible = False
        Chk_Conferma.Checked = False
        '0Registrazione.AnnoCompetenza = Val(Txt_Anno.Text)
        'Registrazione.MeseCompetenza = Val(Dd_Mese.SelectedValue)
        If MEm.EsisteDocumentoPerCreaDoc(MastroPartita, ContoPartita, SottocontoPartita, CentroServizio.CENTROSERVIZIO, Val(Txt_Anno.Text), Val(Dd_Mese.SelectedValue), "", Val(Txt_Anno.Text), Val(Dd_Mese.SelectedValue), 0) Then
            lbl_Errore.Text = "Già emessa fattura per questo periodo, metti il check per procedere"
            Chk_Conferma.Visible = True
            Chk_Conferma.Checked = False
        End If
        MEm.CloseDB()

        f.LeggiParametri(Session("DC_OSPITE"))


        Dim UsoExtraParente As Integer = 0

        Dim LeggiOspite As New ClsOspite

        LeggiOspite.Leggi(Session("DC_OSPITE"),Val(Session("CODICEOSPITE")))
        
        UsoExtraParente = 1
        
        Dim sc2 As New MSScriptControl.ScriptControl

        sc2.Language = "vbscript"


        XS.CampoParametriGIORNOUSCITA = f.GIORNOUSCITA
        XS.CampoParametriGIORNOENTRATA = f.GIORNOENTRATA
        XS.CampoParametriCAUSALEACCOGLIMENTO = f.CAUSALEACCOGLIMENTO
        XS.ConguaglioMinimo = f.ConguaglioMinimo
        XS.CampoParametriMastroAnticipo = f.MastroAnticipo
        XS.CampoParametriAddebitiAccreditiComulati = f.AddebitiAccreditiComulati
        XS.Prm_AzzeraSeNegativo = f.AzzeraSeNegativo
        XS.ApriDB(Session("DC_OSPITE"), Session("DC_OSPITIACCESSORI"))
        XS.STRINGACONNESSIONEDB = Session("DC_OSPITE")
        XS.ConnessioneGenerale = Session("DC_GENERALE")
        XS.CaricaCausali()

        MyCserv.Leggi(Session("DC_OSPITE"), VarCentroServizio)

        Call XS.AzzeraTabella()


        XS.EliminaDatiRette(Mese, Anno, VarCentroServizio, varcodiceospite)

        XS.DeleteChiave(VarCentroServizio, varcodiceospite, Mese, Anno)

        If LeggiOspite.PERIODO = "F" Then
            Dim DataUscita As Date
            Dim CalcFineMese As New Cls_CalcoloOspitiFineMese

            DataUscita = CalcFineMese.OspiteDimessonelMese(VarCentroServizio, varcodiceospite, Mese, Anno, Session("DC_OSPITE"))

            If Month(DataUscita) = Mese And Year(DataUscita) = Anno Then
                CalcFineMese.GeneraPeriodo(VarCentroServizio, varcodiceospite, Mese, Anno, DataUscita, Session("DC_OSPITE"), sc2)
            End If

            XS.StringaDegliErrori = XS.StringaDegliErrori & CalcFineMese.StringaDegliErrori
        Else
            XS.PrimoDataAccoglimentoOspite = XS.PrimaDataAccoglimento(VarCentroServizio, varcodiceospite, Mese, Anno)

            If UsoExtraParente = 1 Then
                XS.VerificaPrimaCalcolo(VarCentroServizio, varcodiceospite)
            End If

            If MyCserv.TIPOCENTROSERVIZIO = "D" Then
                XS.PresenzeAssenzeDiurno(VarCentroServizio, varcodiceospite, Mese, Anno)
            End If

            If MyCserv.TIPOCENTROSERVIZIO = "A" Then
                XS.PresenzeDomiciliare(VarCentroServizio, varcodiceospite, Mese, Anno)
            End If


            If Val(MyCserv.VerificaImpegnativa) = 1 Then
                Call XS.ModificaTabellaPresenze(VarCentroServizio, varcodiceospite, Mese, Anno)
            End If


            XS.CreaTabellaPresenze(VarCentroServizio, varcodiceospite, Mese, Anno)

            XS.CreaTabellaImportoOspite(VarCentroServizio, varcodiceospite, Mese, Anno)



            XS.CreaTabellaImportoComune(VarCentroServizio, varcodiceospite, Mese, Anno)
            XS.CreaTabellaUsl(VarCentroServizio, varcodiceospite, Mese, Anno)
            XS.CreaTabellaImportoRetta(VarCentroServizio, varcodiceospite, Mese, Anno)



            XS.CreaTabellaImportoJolly(VarCentroServizio, varcodiceospite, Mese, Anno)

            XS.CreaTabellaModalita(VarCentroServizio, varcodiceospite, Mese, Anno, True)

            XS.CreaTabellaImportoParente(VarCentroServizio, varcodiceospite, Mese, Anno)


            'Call CalcolaRettaDaTabella(Tabella, VarCentroServizio, VarCodiceOspite)
            XS.RiassociaComuni(VarCentroServizio, varcodiceospite, Mese, Anno)

            XS.CalcolaRettaDaTabella(VarCentroServizio, varcodiceospite, Anno, Mese, sc2)



            XS.CreaAddebitiAcrrediti(VarCentroServizio, varcodiceospite, Mese, Anno)

            XS.AllineaImportiMensili(VarCentroServizio, varcodiceospite, Mese, Anno)

            If Not XS.VerificaRette(VarCentroServizio, varcodiceospite, Mese, Anno) Then
                REM  Errori = True
            Else
                XS.SalvaDati(VarCentroServizio, varcodiceospite, Mese, Anno)
            End If
        End If

        Dim TipoOperazione As New Cls_TipoOperazione

        Dim cn As OleDbConnection


        cn = New Data.OleDb.OleDbConnection(Session("DC_OSPITE"))

        cn.Open()
        If Request.Item("CodiceParente") = 99 Then
            Dim Ospite As New ClsOspite

            Ospite.CodiceOspite = varcodiceospite
            Ospite.Leggi(Session("DC_OSPITE"), Ospite.CodiceOspite)


            TipoOperazione.Codice = Ospite.TIPOOPERAZIONE
            TipoOperazione.Leggi(Session("DC_OSPITE"), TipoOperazione.Codice)


            Dim cmd As New OleDbCommand()
            cmd.CommandText = ("select SUM(IMPORTO) AS Timporto, sum(GIORNI) As TGiorni from RETTEOSPITE Where Anno = ? And Mese = ? And CENTROSERVIZIO = ? And CODICEOSPITE = ? And (Elemento = 'RGP' or Elemento = 'RPX')")
            cmd.Connection = cn
            cmd.Parameters.AddWithValue("@Anno", Anno)
            cmd.Parameters.AddWithValue("@Mese", Mese)
            cmd.Parameters.AddWithValue("@CENTROSERVIZIO", VarCentroServizio)
            cmd.Parameters.AddWithValue("@CODICEOSPITE", varcodiceospite)
            Dim myPOSTreader As OleDbDataReader = cmd.ExecuteReader()
            If myPOSTreader.Read Then
                Txt_ImportoRettaPresenza.Text = CDbl(Txt_ImportoRettaPresenza.Text) + Format(campodbN(myPOSTreader.Item("Timporto")), "#,##0.00")
                Txt_GiorniPresenza.Text = CDbl(Txt_GiorniPresenza.Text) + campodbN(myPOSTreader.Item("TGiorni"))
            End If
            myPOSTreader.Close()

            Dim cmd1 As New OleDbCommand()
            cmd1.CommandText = ("select SUM(IMPORTO) AS Timporto, sum(GIORNI) As TGiorni from RETTEOSPITE Where Anno = ? And Mese = ? And CENTROSERVIZIO = ? And CODICEOSPITE = ? And (Elemento = 'R2P' or Elemento = 'R2X')")
            cmd1.Connection = cn
            cmd1.Parameters.AddWithValue("@Anno", Anno)
            cmd1.Parameters.AddWithValue("@Mese", Mese)
            cmd1.Parameters.AddWithValue("@CENTROSERVIZIO", VarCentroServizio)
            cmd1.Parameters.AddWithValue("@CODICEOSPITE", varcodiceospite)
            Dim myPOSTreader1 As OleDbDataReader = cmd1.ExecuteReader()
            If myPOSTreader1.Read Then
                Txt_ImportoRettaPresenza2.Text = CDbl(Txt_ImportoRettaPresenza2.Text) + Format(campodbN(myPOSTreader1.Item("Timporto")), "#,##0.00")
            End If
            myPOSTreader1.Close()

            If TipoOperazione.SCORPORAIVA = 1 Then
                Dim IVA As New Cls_IVA

                IVA.Codice = Ospite.CODICEIVA
                IVA.Leggi(Session("DC_TABELLE"), IVA.Codice)


                Txt_ImportoRettaPresenza.Text = Math.Round(CDbl(Txt_ImportoRettaPresenza.Text) * 100 / (100 + (IVA.Aliquota * 100)), 2)


                Txt_ImportoRettaPresenza2.Text = Math.Round(CDbl(Txt_ImportoRettaPresenza2.Text) * 100 / (100 + (IVA.Aliquota * 100)), 2)
            End If

            Dim cmd2 As New OleDbCommand()
            cmd2.CommandText = ("select SUM(IMPORTO) AS Timporto, sum(GIORNI) As TGiorni from RETTEOSPITE Where Anno = ? And Mese = ? And CENTROSERVIZIO = ? And CODICEOSPITE = ? And Elemento = 'RGA'")
            cmd2.Connection = cn
            cmd2.Parameters.AddWithValue("@Anno", Anno)
            cmd2.Parameters.AddWithValue("@Mese", Mese)
            cmd2.Parameters.AddWithValue("@CENTROSERVIZIO", VarCentroServizio)
            cmd2.Parameters.AddWithValue("@CODICEOSPITE", varcodiceospite)
            Dim myPOSTreader2 As OleDbDataReader = cmd2.ExecuteReader()
            If myPOSTreader2.Read Then
                Txt_ImportoRettaAssenza.Text = CDbl(Txt_ImportoRettaAssenza.Text) + Format(campodbN(myPOSTreader2.Item("Timporto")), "#,##0.00")
                Txt_GiorniAssenza.Text = CDbl(Txt_GiorniAssenza.Text) + campodbN(myPOSTreader2.Item("TGiorni"))
            End If
            myPOSTreader2.Close()

            Dim cmd3 As New OleDbCommand()
            cmd3.CommandText = ("select SUM(IMPORTO) AS Timporto, sum(GIORNI) As TGiorni from RETTEOSPITE Where Anno = ? And Mese = ? And CENTROSERVIZIO = ? And CODICEOSPITE = ? And Elemento Like 'E%'")
            cmd3.Connection = cn
            cmd3.Parameters.AddWithValue("@Anno", Anno)
            cmd3.Parameters.AddWithValue("@Mese", Mese)
            cmd3.Parameters.AddWithValue("@CENTROSERVIZIO", VarCentroServizio)
            cmd3.Parameters.AddWithValue("@CODICEOSPITE", varcodiceospite)
            Dim myPOSTreader3 As OleDbDataReader = cmd3.ExecuteReader()
            If myPOSTreader3.Read Then
                Txt_ExtraFissi.Text = CDbl(Txt_ExtraFissi.Text) + Format(campodbN(myPOSTreader3.Item("Timporto")), "#,##0.00")
            End If
            myPOSTreader3.Close()

            Dim cmd4 As New OleDbCommand()
            Dim CondizioneAddebiti As String = ""

            If RB_FatturaRetta.Visible = True Then
                If RB_FatturaRetta.Checked = True Then
                    CondizioneAddebiti = " And ((Select FuoriRetta From TabTipoAddebito Where  TabTipoAddebito.Codice = ADDACR.CodiceIVA) =0 or (Select FuoriRetta From TabTipoAddebito Where  TabTipoAddebito.Codice = ADDACR.CodiceIVA) Is Null) "
                Else
                    CondizioneAddebiti = " And (Select FuoriRetta From TabTipoAddebito Where  TabTipoAddebito.Codice = ADDACR.CodiceIVA) =1 "
                End If
            End If


            If Chk_SoloMeseCorrente.Checked = True Then
                cmd4.CommandText = ("SelecT * FROM ADDACR WHERE  TIPOMOV = 'AD' AND RIFERIMENTO = 'O' AND CENTROSERVIZIO = '" & Session("CODICESERVIZIO") & "' AND CODICEOSPITE  = " & Session("CODICEOSPITE") & " And (Elaborato = 0 or Elaborato Is Null) And year(Data) = " & Anno & " And month(data) = " & Mese & CondizioneAddebiti)
            Else
                cmd4.CommandText = ("SelecT * FROM ADDACR WHERE  TIPOMOV = 'AD' AND RIFERIMENTO = 'O' AND CENTROSERVIZIO = '" & Session("CODICESERVIZIO") & "' AND CODICEOSPITE  = " & Session("CODICEOSPITE") & " And (Elaborato = 0 or Elaborato Is Null) " & CondizioneAddebiti)
            End If

            Dim ImportoAddebiti As Double = 0
            cmd4.Connection = cn
            Dim myPOSTreader4 As OleDbDataReader = cmd4.ExecuteReader()
            Do While myPOSTreader4.Read
                If TipoOperazione.SCORPORAIVA = 1 Then
                    Dim TipoAddebito As New Cls_Addebito


                    TipoAddebito.Codice = campodb(myPOSTreader4.Item("CodiceIva"))
                    TipoAddebito.Leggi(Session("DC_OSPITE"), TipoAddebito.Codice)

                    Dim IVA As New Cls_IVA

                    IVA.Codice = TipoAddebito.CodiceIVA
                    IVA.Leggi(Session("DC_TABELLE"), IVA.Codice)

                    Dim ImportoScorporato As Double

                    ImportoScorporato = Math.Round(campodbN(myPOSTreader4.Item("importo")) * 100 / (100 + (IVA.Aliquota * 100)), 2)


                    ImportoAddebiti = ImportoAddebiti + Format(ImportoScorporato, "#,##0.00")
                Else
                    ImportoAddebiti = ImportoAddebiti + Format(campodbN(myPOSTreader4.Item("importo")), "#,##0.00")
                End If
            Loop
            myPOSTreader4.Close()
            Txt_ImportoAddibiti.Text = ImportoAddebiti




            Dim cmd5 As New OleDbCommand()

            cmd5.CommandText = ("SelecT sum(Importo) as Timporto FROM ADDACR WHERE  TIPOMOV = 'AC' AND RIFERIMENTO = 'O' AND CENTROSERVIZIO = '" & Session("CODICESERVIZIO") & "' AND CODICEOSPITE  = " & Session("CODICEOSPITE") & " And (Elaborato = 0 or Elaborato Is Null) " & CondizioneAddebiti)
            cmd5.Connection = cn
            Dim myPOSTreader5 As OleDbDataReader = cmd5.ExecuteReader()
            If myPOSTreader5.Read Then
                Txt_ImportoAccrediti.Text = Format(campodbN(myPOSTreader5.Item("Timporto")), "#,##0.00")
            End If
            myPOSTreader5.Close()
        Else
            Dim Parente As New Cls_Parenti

            Parente.CodiceOspite = varcodiceospite
            Parente.CodiceParente = Val(Request.Item("CodiceParente"))
            Parente.Leggi(Session("DC_OSPITE"), Parente.CodiceOspite, Parente.CodiceParente)


            TipoOperazione.Codice = Parente.TIPOOPERAZIONE
            TipoOperazione.Leggi(Session("DC_OSPITE"), TipoOperazione.Codice)


            Dim cmd As New OleDbCommand()
            cmd.CommandText = ("select SUM(IMPORTO) AS Timporto, sum(GIORNI) As TGiorni from RETTEPARENTE Where Anno = ? And Mese = ? And CENTROSERVIZIO = ? And CODICEOSPITE = ? AND CODICEPARENTE = ? And (Elemento = 'RGP' OR Elemento = 'RPX')")
            cmd.Connection = cn
            cmd.Parameters.AddWithValue("@Anno", Anno)
            cmd.Parameters.AddWithValue("@Mese", Mese)
            cmd.Parameters.AddWithValue("@CENTROSERVIZIO", VarCentroServizio)
            cmd.Parameters.AddWithValue("@CODICEOSPITE", varcodiceospite)
            cmd.Parameters.AddWithValue("@CODICEPARENTE", Val(Request.Item("CodiceParente")))
            Dim myPOSTreader As OleDbDataReader = cmd.ExecuteReader()
            If myPOSTreader.Read Then
                Txt_ImportoRettaPresenza.Text = CDbl(Txt_ImportoRettaPresenza.Text) + Format(campodbN(myPOSTreader.Item("Timporto")), "#,##0.00")
                Txt_GiorniPresenza.Text = CDbl(Txt_GiorniPresenza.Text) + campodbN(myPOSTreader.Item("TGiorni"))
            End If
            myPOSTreader.Close()


            Dim cmd1 As New OleDbCommand()
            cmd1.CommandText = ("select SUM(IMPORTO) AS Timporto, sum(GIORNI) As TGiorni from RETTEPARENTE Where Anno = ? And Mese = ? And CENTROSERVIZIO = ? And CODICEOSPITE = ? AND CODICEPARENTE = ? And (Elemento = 'R2P' OR Elemento = 'R2X')")
            cmd1.Connection = cn
            cmd1.Parameters.AddWithValue("@Anno", Anno)
            cmd1.Parameters.AddWithValue("@Mese", Mese)
            cmd1.Parameters.AddWithValue("@CENTROSERVIZIO", VarCentroServizio)
            cmd1.Parameters.AddWithValue("@CODICEOSPITE", varcodiceospite)
            cmd1.Parameters.AddWithValue("@CODICEPARENTE", Val(Request.Item("CodiceParente")))
            Dim myPOSTreader1 As OleDbDataReader = cmd1.ExecuteReader()
            If myPOSTreader1.Read Then
                Txt_ImportoRettaPresenza2.Text = CDbl(Txt_ImportoRettaPresenza.Text) + Format(campodbN(myPOSTreader1.Item("Timporto")), "#,##0.00")
            End If
            myPOSTreader1.Close()


            Dim cmd2 As New OleDbCommand()
            cmd2.CommandText = ("select SUM(IMPORTO) AS Timporto, sum(GIORNI) As TGiorni from RETTEPARENTE Where Anno = ? And Mese = ? And CENTROSERVIZIO = ? And CODICEOSPITE = ? AND CODICEPARENTE = ? And Elemento = 'RGA'")
            cmd2.Connection = cn
            cmd2.Parameters.AddWithValue("@Anno", Anno)
            cmd2.Parameters.AddWithValue("@Mese", Mese)
            cmd2.Parameters.AddWithValue("@CENTROSERVIZIO", VarCentroServizio)
            cmd2.Parameters.AddWithValue("@CODICEOSPITE", varcodiceospite)
            cmd2.Parameters.AddWithValue("@CODICEPARENTE", Val(Request.Item("CodiceParente")))
            Dim myPOSTreader2 As OleDbDataReader = cmd2.ExecuteReader()
            If myPOSTreader2.Read Then
                Txt_ImportoRettaAssenza.Text = CDbl(Txt_ImportoRettaAssenza.Text) + Format(campodbN(myPOSTreader2.Item("Timporto")), "#,##0.00")
                Txt_GiorniAssenza.Text = CDbl(Txt_GiorniAssenza.Text) + campodbN(myPOSTreader2.Item("TGiorni"))
            End If
            myPOSTreader2.Close()


            If TipoOperazione.SCORPORAIVA = 1 Then
                Dim IVA As New Cls_IVA

                IVA.Codice = Parente.CODICEIVA
                IVA.Leggi(Session("DC_TABELLE"), IVA.Codice)


                Txt_ImportoRettaPresenza.Text = Math.Round(CDbl(Txt_ImportoRettaPresenza.Text) * 100 / (100 + (IVA.Aliquota * 100)), 2)


                Txt_ImportoRettaPresenza2.Text = Math.Round(CDbl(Txt_ImportoRettaPresenza2.Text) * 100 / (100 + (IVA.Aliquota * 100)), 2)
            End If

            Dim cmd3 As New OleDbCommand()
            cmd3.CommandText = ("select SUM(IMPORTO) AS Timporto, sum(GIORNI) As TGiorni from RETTEPARENTE Where Anno = ? And Mese = ? And CENTROSERVIZIO = ? And CODICEOSPITE = ? AND CODICEPARENTE = ? And Elemento Like 'E%'")
            cmd3.Connection = cn
            cmd3.Parameters.AddWithValue("@Anno", Anno)
            cmd3.Parameters.AddWithValue("@Mese", Mese)
            cmd3.Parameters.AddWithValue("@CENTROSERVIZIO", VarCentroServizio)
            cmd3.Parameters.AddWithValue("@CODICEOSPITE", varcodiceospite)
            cmd3.Parameters.AddWithValue("@CODICEPARENTE", Val(Request.Item("CodiceParente")))

            Dim myPOSTreader3 As OleDbDataReader = cmd3.ExecuteReader()
            If myPOSTreader3.Read Then
                Txt_ExtraFissi.Text = CDbl(Txt_ExtraFissi.Text) + Format(campodbN(myPOSTreader3.Item("Timporto")), "#,##0.00")
            End If
            myPOSTreader3.Close()

            Dim cmd4 As New OleDbCommand()
            Dim CondizioneAddebiti As String = ""

            If RB_FatturaRetta.Visible = True Then
                If RB_FatturaRetta.Checked = True Then
                    CondizioneAddebiti = " And ((Select FuoriRetta From TabTipoAddebito Where  TabTipoAddebito.Codice = ADDACR.CodiceIVA) =0 or (Select FuoriRetta From TabTipoAddebito Where  TabTipoAddebito.Codice = ADDACR.CodiceIVA) Is Null ) "
                Else
                    CondizioneAddebiti = " And (Select FuoriRetta From TabTipoAddebito Where  TabTipoAddebito.Codice = ADDACR.CodiceIVA) =1 "
                End If
            End If

            If Chk_SoloMeseCorrente.Checked = True Then
                cmd4.CommandText = ("SelecT * FROM ADDACR WHERE  TIPOMOV = 'AD' AND RIFERIMENTO = 'P' AND CENTROSERVIZIO = '" & Session("CODICESERVIZIO") & "' AND CODICEOSPITE  = " & Session("CODICEOSPITE") & " AND PARENTE = " & Val(Request.Item("CodiceParente")) & " And (Elaborato = 0 or Elaborato Is Null) And year(Data) = " & Anno & " And month(data) = " & Mese & CondizioneAddebiti)
            Else
                cmd4.CommandText = ("SelecT * FROM ADDACR WHERE  TIPOMOV = 'AD' AND RIFERIMENTO = 'P' AND CENTROSERVIZIO = '" & Session("CODICESERVIZIO") & "' AND CODICEOSPITE  = " & Session("CODICEOSPITE") & " AND PARENTE = " & Val(Request.Item("CodiceParente")) & " And (Elaborato = 0 or Elaborato Is Null) " & CondizioneAddebiti)
            End If

            Dim ImportoAddebiti As Double = 0
            cmd4.Connection = cn
            Dim myPOSTreader4 As OleDbDataReader = cmd4.ExecuteReader()
            Do While myPOSTreader4.Read
                If TipoOperazione.SCORPORAIVA = 1 Then
                    Dim TipoAddebito As New Cls_Addebito


                    TipoAddebito.Codice = campodbN(myPOSTreader4.Item("CodiceIva"))
                    TipoAddebito.Leggi(Session("DC_OSPITE"), TipoAddebito.Codice)

                    Dim IVA As New Cls_IVA

                    IVA.Codice = TipoAddebito.CodiceIVA
                    IVA.Leggi(Session("DC_TABELLE"), IVA.Codice)

                    Dim ImportoScorporato As Double

                    ImportoScorporato = campodbN(myPOSTreader4.Item("importo")) * (1 - IVA.Aliquota)


                    ImportoAddebiti = ImportoAddebiti + Format(ImportoScorporato, "#,##0.00")
                Else
                    ImportoAddebiti = ImportoAddebiti + Format(campodbN(myPOSTreader4.Item("importo")), "#,##0.00")
                End If
            Loop
            myPOSTreader4.Close()
            Txt_ImportoAddibiti.Text = ImportoAddebiti

            Dim cmd5 As New OleDbCommand()



            cmd5.CommandText = ("SelecT sum(Importo) as Timporto FROM ADDACR WHERE  TIPOMOV = 'AC' AND RIFERIMENTO = 'P' AND CENTROSERVIZIO = '" & Session("CODICESERVIZIO") & "' AND CODICEOSPITE  = " & Session("CODICEOSPITE") & " AND PARENTE = " & Val(Request.Item("CodiceParente")) & " And (Elaborato = 0 or Elaborato Is Null) " & CondizioneAddebiti)
            cmd5.Connection = cn
            Dim myPOSTreader5 As OleDbDataReader = cmd5.ExecuteReader()
            If myPOSTreader5.Read Then
                Txt_ImportoAccrediti.Text = Format(campodbN(myPOSTreader5.Item("Timporto")), "#,##0.00")
            End If
            myPOSTreader5.Close()
        End If
        cn.Close()


        ViewState("Txt_ExtraFissi") = Txt_ExtraFissi.Text
        ViewState("Txt_ImportoAccrediti") = Txt_ImportoAccrediti.Text
        ViewState("Txt_ImportoAddibiti") = Txt_ImportoAddibiti.Text

        If CDbl(Txt_ExtraFissi.Text) > 0 Then
            Chk_ExtraFissi.Checked = True
        End If
        If CDbl(Txt_ImportoAccrediti.Text) > 0 Then
            Chk_ImportoAccrediti.Checked = True
        End If
        If CDbl(Txt_ImportoAddibiti.Text) > 0 Then
            Chk_ImportoAddibiti.Checked = True
        End If

         If Session("DC_OSPITE").ToString.ToUpper.IndexOf("MARINO") >= 0 Then
            'LeggiDocumento(CentroServizio.MASTRO, CentroServizio.CONTO, Session("CODICEOSPITE") * 100, Dd_Mese.SelectedValue, Txt_Anno.Text)
        End If

        Call CalcolaTotale()
        Call Detrazione()
    End Sub
    
    Private Sub LeggiDocumento(ByVal Mastro As Integer, ByVal Conto As Integer, ByVal SottoConto As Integer, ByVal Mese As Integer, ByVal Anno As Integer)
        Dim cn As OleDbConnection

        cn = New Data.OleDb.OleDbConnection(Session("DC_GENERALE"))

        cn.Open()

        Dim cmd As New OleDbCommand()
        cmd.CommandText = "Select * From MovimentiContabiliTesta Where (select count(*) From MovimentiContabiliRiga Where Numero = NumeroRegistrazione And MastroPartita = ? And ContoPartita = ? And SottocontoPartita = ? And RigaDaCausale = 1) > 0 And MeseCompetenza = ? And AnnoCompetenza = ? "
        cmd.Connection = cn
        cmd.Parameters.AddWithValue("@MastroPartita", Mastro)
        cmd.Parameters.AddWithValue("@ContoParitta", Conto)
        cmd.Parameters.AddWithValue("@SottoConto", SottoConto)
        cmd.Parameters.AddWithValue("@Mese", Mese)
        cmd.Parameters.AddWithValue("@Anno", Anno)
        Dim LeggiRegistrazione As OleDbDataReader = cmd.ExecuteReader()
        Do While LeggiRegistrazione.Read 
            Dim Registrazione As New Cls_MovimentoContabile

            Registrazione.NumeroRegistrazione = campodbN(LeggiRegistrazione.Item("NumeroRegistrazione"))
            Registrazione.Leggi(Session("DC_GENERALE"), Registrazione.NumeroRegistrazione)

            For i = 1 To 100
                If Not IsNothing(Registrazione.Righe(i)) Then
                    If Registrazione.Righe(i).RigaDaCausale = 3 Then
                        Txt_GiorniPresenza.Text = CDbl(Txt_GiorniPresenza.Text) - Registrazione.Righe(i).Quantita
                        Txt_ImportoRettaPresenza.Text = CDbl(Txt_ImportoRettaPresenza.Text) - Registrazione.Righe(i).Importo
                    End If
                    If Registrazione.Righe(i).RigaDaCausale = 13 Then
                        Txt_ImportoRettaPresenza2.Text = CDbl(Txt_ImportoRettaPresenza2.Text) - Registrazione.Righe(i).Importo
                    End If
                End If
            Next
        loop
        cn.Close()
    End Sub

    Private Sub Detrazione()
        Dim M As New Cls_CentroServizio

        M.CENTROSERVIZIO = Session("CODICESERVIZIO")
        M.Leggi(Session("DC_OSPITE"), M.CENTROSERVIZIO)

        If M.DescrizioneRigaInFattura = "Villa Jole" Then
            Dim UltModalita As New Cls_Modalita

            UltModalita.CentroServizio = M.CENTROSERVIZIO
            UltModalita.CodiceOspite = Session("CODICEOSPITE")
            UltModalita.UltimaData(Session("DC_OSPITE"), UltModalita.CodiceOspite, UltModalita.CentroServizio)

            Dim UltStato As New Cls_StatoAuto

            UltStato.CENTROSERVIZIO = M.CENTROSERVIZIO
            UltStato.CODICEOSPITE = Session("CODICEOSPITE")
            If Val(Txt_Anno.Text) = 0 Or Val(Dd_Mese.SelectedValue) = 0 Then
                UltStato.Data = Now
            Else
                UltStato.Data = DateSerial(Val(Txt_Anno.Text), Val(Dd_Mese.SelectedValue), GiorniMese(Val(Dd_Mese.SelectedValue), Val(Txt_Anno.Text)))
            End If
            UltStato.StatoPrimaData(Session("DC_OSPITE"), UltModalita.CodiceOspite, UltModalita.CentroServizio)


            Dim MRj As New Cls_rettatotale

            MRj.CODICEOSPITE = Session("CODICEOSPITE")
            MRj.UltimaData(Session("DC_OSPITE"), Session("CODICEOSPITE"), M.CENTROSERVIZIO)

            Dim Xs As New Cls_TipoRetta

            Xs.Tipo = MRj.TipoRetta
            Xs.Leggi(Session("DC_OSPITE"), Xs.Tipo)

            If Xs.Descrizione <> "HCP" Then
                If M.CENTROSERVIZIO = "2" Then
                    If Trim(UltStato.USL) <> "" Then
                        If UltModalita.MODALITA = "O" Or UltModalita.MODALITA = "P" Then
                            Txt_DescrizioneQD.Text = "Quota da detrarre per assistenza specifica : " & Math.Round(Txt_GiorniPresenza.Text * 5.84, 2)
                        Else
                            Txt_DescrizioneQD.Text = "Quota da detrarre per assistenza specifica : " & Math.Round(Txt_ImportoRettaPresenza.Text * 18.84 / 100, 2)
                        End If
                    Else
                        If UltStato.STATOAUTO = "N" Then
                            Txt_DescrizioneQD.Text = "Quota da detrarre per assistenza sanitaria e specifica : " & Math.Round(Txt_GiorniPresenza.Text * (5.84 + 30.93), 2)
                        End If
                    End If
                Else
                    If Trim(UltStato.USL) <> "" Then
                        If UltModalita.MODALITA = "O" Or UltModalita.MODALITA = "P" Then
                            Txt_DescrizioneQD.Text = "Quota da detrarre per assistenza specifica : " & Math.Round(Txt_GiorniPresenza.Text * 9.73, 2)
                        Else
                            Txt_DescrizioneQD.Text = "Quota da detrarre per assistenza specifica : " & Math.Round(Txt_ImportoRettaPresenza.Text * 18.35 / 100, 2)
                        End If
                    Else
                        If UltStato.STATOAUTO = "N" Then
                            Txt_DescrizioneQD.Text = "Quota da detrarre per assistenza sanitaria e specifica : " & Math.Round(Txt_GiorniPresenza.Text * (9.73 + 52.32), 2)
                        End If
                    End If
                End If
            End If
        End If

        If M.DescrizioneRigaInFattura = "Villa Amelia" Then

            Dim UltModalita As New Cls_Modalita

            UltModalita.CentroServizio = M.CENTROSERVIZIO
            UltModalita.CodiceOspite = Session("CODICEOSPITE")
            UltModalita.UltimaData(Session("DC_OSPITE"), UltModalita.CodiceOspite, UltModalita.CentroServizio)

            Dim UltStato As New Cls_StatoAuto

            UltStato.CENTROSERVIZIO = M.CENTROSERVIZIO
            UltStato.CODICEOSPITE = Session("CODICEOSPITE")
            UltStato.UltimaData(Session("DC_OSPITE"), UltModalita.CodiceOspite, UltModalita.CentroServizio)



            If M.CENTROSERVIZIO = "2" Then
                If Trim(UltStato.USL) <> "" Then
                    If UltModalita.MODALITA = "O" Or UltModalita.MODALITA = "P" Then
                        Txt_DescrizioneQD.Text = "Quota da detrarre per assistenza specifica : " & Math.Round(Txt_GiorniPresenza.Text * 5.83, 2)
                    Else
                        Txt_DescrizioneQD.Text = "Quota da detrarre per assistenza specifica : " & Math.Round(Txt_ImportoRettaPresenza.Text * 16.56 / 100, 2)
                    End If
                Else
                    If UltStato.STATOAUTO = "N" Then
                        Txt_DescrizioneQD.Text = "Quota da detrarre per assistenza sanitaria e specifica : " & Math.Round(Txt_GiorniPresenza.Text * (5.83 + 31.4), 2)
                    Else
                        Txt_DescrizioneQD.Text = "Quota da detrarre per assistenza specifica : " & Math.Round(Txt_GiorniPresenza.Text * 5.83, 2)
                    End If
                End If
            Else
                If Trim(UltStato.USL) <> "" Then
                    If UltModalita.MODALITA = "O" Or UltModalita.MODALITA = "P" Then
                        Txt_DescrizioneQD.Text = "Quota da detrarre per assistenza specifica : " & Math.Round(Txt_GiorniPresenza.Text * 9.73, 2)
                    Else
                        Txt_DescrizioneQD.Text = vbNewLine & "Quota da detrarre per assistenza specifica : " & Math.Round(Txt_ImportoRettaPresenza.Text * 18.47 / 100, 2)
                    End If
                Else
                    If UltStato.STATOAUTO = "N" Then
                        Txt_DescrizioneQD.Text = "Quota da detrarre per assistenza sanitaria e specifica : " & Math.Round(Txt_GiorniPresenza.Text * (9.73 + 52.32), 2)
                    Else
                        Txt_DescrizioneQD.Text = "Quota da detrarre per assistenza specifica : " & Math.Round(Txt_GiorniPresenza.Text * 9.73, 2)
                    End If
                End If
            End If
        End If



        If M.DescrizioneRigaInFattura = "San Martino" Then

            Dim UltModalita As New Cls_Modalita

            UltModalita.CentroServizio = M.CENTROSERVIZIO
            UltModalita.CodiceOspite = Session("CODICEOSPITE")
            UltModalita.UltimaData(Session("DC_OSPITE"), UltModalita.CodiceOspite, UltModalita.CentroServizio)

            Dim UltStato As New Cls_StatoAuto

            UltStato.CENTROSERVIZIO = M.CENTROSERVIZIO
            UltStato.CODICEOSPITE = Session("CODICEOSPITE")
            UltStato.UltimaData(Session("DC_OSPITE"), UltModalita.CodiceOspite, UltModalita.CentroServizio)



            If M.CENTROSERVIZIO = "2" Then
                If Trim(UltStato.USL) <> "" Then
                    If UltModalita.MODALITA = "O" Or UltModalita.MODALITA = "P" Then
                        Txt_DescrizioneQD.Text = "Quota da detrarre per assistenza specifica : " & Math.Round(Txt_GiorniPresenza.Text * 5.83, 2)
                    Else
                        Txt_DescrizioneQD.Text = "Quota da detrarre per assistenza specifica : " & Math.Round(Txt_ImportoRettaPresenza.Text * 16.56 / 100, 2)
                    End If
                Else
                    If UltStato.STATOAUTO = "N" Then
                        Txt_DescrizioneQD.Text = "Quota da detrarre per assistenza sanitaria e specifica : " & Math.Round(Txt_GiorniPresenza.Text * (5.83 + 31.4), 2)
                    Else
                        Txt_DescrizioneQD.Text = "Quota da detrarre per assistenza specifica : " & Math.Round(Txt_GiorniPresenza.Text * 5.83, 2)
                    End If
                End If
            Else
                If Trim(UltStato.USL) <> "" Then
                    If UltModalita.MODALITA = "O" Or UltModalita.MODALITA = "P" Then
                        Txt_DescrizioneQD.Text = "Quota da detrarre per assistenza specifica : " & Math.Round(Txt_GiorniPresenza.Text * 9.73, 2)
                    Else
                        Txt_DescrizioneQD.Text = vbNewLine & "Quota da detrarre per assistenza specifica : " & Math.Round(Txt_ImportoRettaPresenza.Text * 18.47 / 100, 2)
                    End If
                Else
                    If UltStato.STATOAUTO = "N" Then
                        Txt_DescrizioneQD.Text = "Quota da detrarre per assistenza sanitaria e specifica : " & Math.Round(Txt_GiorniPresenza.Text * (9.73 + 52.32), 2)
                    Else
                        Txt_DescrizioneQD.Text = "Quota da detrarre per assistenza specifica : " & Math.Round(Txt_GiorniPresenza.Text * 9.73, 2)
                    End If
                End If
            End If
        End If
    End Sub

    Private Sub CalcolaTotale()
        Dim Totale As Double
        Dim X As New ClsOspite
        Dim xPar As New Cls_Parenti

        Dim ConnectionString As String = Session("DC_OSPITE")
        Dim ConnectionStringTabelle As String = Session("DC_TABELLE")
        Dim ConnectionStringGenerale As String = Session("DC_GENERALE")

        If Request.Item("CodiceParente") = 99 Then
            X.Leggi(ConnectionString, Session("CODICEOSPITE"))

            Dim JK As New Cls_DatiOspiteParenteCentroServizio

            JK.CodiceOspite = X.CodiceOspite
            JK.CodiceParente = 0
            JK.CentroServizio = Session("CODICESERVIZIO")
            JK.Leggi(ConnectionString)

            If JK.CodiceOspite <> 0 Then
                X.TIPOOPERAZIONE = JK.TipoOperazione
            End If



        Else
            xPar.Leggi(ConnectionString, Session("CODICEOSPITE"), Val(Request.Item("CodiceParente")))

            Dim JK As New Cls_DatiOspiteParenteCentroServizio

            JK.CodiceOspite = X.CodiceOspite
            JK.CodiceParente = xPar.CodiceParente
            JK.CentroServizio = Session("CODICESERVIZIO")
            JK.Leggi(ConnectionString)

            If JK.CodiceOspite <> 0 Then
                xPar.TIPOOPERAZIONE = JK.TipoOperazione
            End If
        End If



        If Txt_ImportoRettaAssenza.Text.Trim = "" Then
            Txt_ImportoRettaAssenza.Text = "0"
        End If
        If Txt_ImportoRettaPresenza.Text.Trim = "" Then
            Txt_ImportoRettaPresenza.Text = "0"
        End If
        If Txt_ImportoRettaPresenza2.Text.Trim = "" Then
            Txt_ImportoRettaPresenza2.Text = "0"
        End If
        If Txt_ImportoAccrediti.Text.Trim = "" Then
            Txt_ImportoAccrediti.Text = "0"
        End If
        If Txt_ImportoAddibiti.Text.Trim = "" Then
            Txt_ImportoAddibiti.Text = "0"
        End If

        If Chk_ExtraFissi.Checked = False Then
            Txt_ExtraFissi.Text = "0,00"
        Else
            Txt_ExtraFissi.Text = ViewState("Txt_ExtraFissi")
        End If

        If Chk_ImportoAccrediti.Checked = False Then
            Txt_ImportoAccrediti.Text = "0,00"
        Else
            Txt_ImportoAccrediti.Text = ViewState("Txt_ImportoAccrediti")
        End If
        If Chk_ImportoAddibiti.Checked = False Then
            Txt_ImportoAddibiti.Text = "0,00"
        Else
            Txt_ImportoAddibiti.Text = ViewState("Txt_ImportoAddibiti")
        End If


        Dim TipoOperazione As New Cls_TipoOperazione


        If Request.Item("CodiceParente") = 99 Then
            TipoOperazione.Leggi(ConnectionString, X.TIPOOPERAZIONE)
        Else
            TipoOperazione.Leggi(ConnectionString, xPar.TIPOOPERAZIONE)
        End If


        Call CalcolaIVA()
        Totale = CDbl(Txt_ImportoRettaPresenza.Text)
        Totale = Totale + CDbl(Txt_ImportoRettaPresenza2.Text)
        Totale = Totale + CDbl(Txt_ImportoRettaAssenza.Text)
        Totale = Totale + CDbl(Txt_ImportoIVA.Text)

        Totale = Totale + CDbl(Txt_ExtraFissi.Text)

        Totale = Totale + CDbl(Txt_ImportoAddibiti.Text)
        Totale = Totale - CDbl(Txt_ImportoAccrediti.Text)

        Txt_Bollo.Text = "0"

        If TipoOperazione.SoggettaABollo = "S" Then
            Dim ClsBollo As New Cls_bolli

            ClsBollo.Leggi(Session("DC_TABELLE"), Now)


            If Math.Abs(CDbl(Txt_ImportoRettaPresenza.Text)) + Math.Abs(CDbl(Txt_ImportoRettaPresenza2.Text)) + CDbl(Txt_ImportoAddibiti.Text) + CDbl(Txt_ExtraFissi.Text) - CDbl(Txt_ImportoAccrediti.Text) + CDbl(Txt_ImportoRettaAssenza.Text) > Math.Abs(ClsBollo.ImportoApplicazione) Then
                Txt_Bollo.Text = Format(ClsBollo.ImportoBollo, "#,##0.00")
            End If
        End If

        If Txt_Bollo.Text = "" Then
            Txt_Bollo.Text = "0,00"
        End If

        Txt_Totale.Text = Format(Totale + CDbl(Txt_Bollo.Text), "#,##0.00")



        If Request.Item("CodiceParente") = 99 Then
            Dim JK As New Cls_DatiOspiteParenteCentroServizio

            JK.CodiceOspite = X.CodiceOspite
            JK.CodiceParente = 0
            JK.CentroServizio = Session("CODICESERVIZIO")
            JK.Leggi(ConnectionString)

            If JK.CodiceOspite <> 0 Then
                X.TIPOOPERAZIONE = JK.TipoOperazione
            End If
            Dim MyOpe As New Cls_TipoOperazione

            MyOpe.Codice = X.TIPOOPERAZIONE
            MyOpe.Leggi(ConnectionString, MyOpe.Codice)

            If Dd_CausaleContabile.SelectedValue <> "" Then
                If Txt_Totale.Text > 0 Then
                    Dd_CausaleContabile.SelectedValue = MyOpe.CausaleIncasso
                Else
                    Dd_CausaleContabile.SelectedValue = MyOpe.GirocontoAnticipo
                End If
            End If

        Else
            Dim JK As New Cls_DatiOspiteParenteCentroServizio

            JK.CodiceOspite = xPar.CodiceOspite
            JK.CodiceParente = xPar.CodiceParente
            JK.CentroServizio = Session("CODICESERVIZIO")
            JK.Leggi(ConnectionString)

            If JK.CodiceOspite <> 0 Then
                xPar.TIPOOPERAZIONE = JK.TipoOperazione
            End If

            Dim MyOpe As New Cls_TipoOperazione

            MyOpe.Codice = xPar.TIPOOPERAZIONE
            MyOpe.Leggi(ConnectionString, MyOpe.Codice)

            If Dd_CausaleContabile.SelectedValue <> "" Then
                If Txt_Totale.Text > 0 Then
                    Dd_CausaleContabile.SelectedValue = MyOpe.CausaleIncasso
                Else
                    Dd_CausaleContabile.SelectedValue = MyOpe.GirocontoAnticipo
                End If
            End If

        End If


        
    End Sub
    Private Sub CalcolaIVA()
        Dim x As New ClsOspite
        Dim xPar As New Cls_Parenti
        Dim MySql As String
        Dim Anno As Integer = Txt_Anno.Text
        Dim Mese As Integer = Val(Dd_Mese.SelectedValue)

        Dim ConnectionString As String = Session("DC_OSPITE")
        Dim ConnectionStringTabelle As String = Session("DC_TABELLE")
        Dim ConnectionStringGenerale As String = Session("DC_GENERALE")


        If Request.Item("CodiceParente") = 99 Then
            x.Leggi(ConnectionString, Session("CODICEOSPITE"))

            Dim OspDatCS As New Cls_DatiOspiteParenteCentroServizio

            OspDatCS.CodiceOspite = x.CodiceOspite
            OspDatCS.CentroServizio = Session("CODICESERVIZIO")
            OspDatCS.CodiceParente = 0
            OspDatCS.AliquotaIva = ""
            OspDatCS.Leggi(ConnectionString)

            If OspDatCS.AliquotaIva <> "" Then
                x.CODICEIVA = OspDatCS.AliquotaIva
                x.TIPOOPERAZIONE = OspDatCS.TipoOperazione
            End If
        Else
            xPar.Leggi(ConnectionString, Session("CODICEOSPITE"), Val(Request.Item("CodiceParente")))

            Dim OspDatCS As New Cls_DatiOspiteParenteCentroServizio

            OspDatCS.CodiceOspite = xPar.CodiceOspite
            OspDatCS.CentroServizio = Session("CODICESERVIZIO")
            OspDatCS.CodiceParente = xPar.CodiceParente
            OspDatCS.AliquotaIva = ""
            OspDatCS.Leggi(ConnectionString)

            If OspDatCS.AliquotaIva <> "" Then
                xPar.CODICEIVA = OspDatCS.AliquotaIva
                xPar.TIPOOPERAZIONE = OspDatCS.TipoOperazione
            End If
        End If

        Dim TipoOperazione As New Cls_TipoOperazione

        If Request.Item("CodiceParente") = 99 Then
            TipoOperazione.Leggi(ConnectionString, x.TIPOOPERAZIONE)
        Else
            TipoOperazione.Leggi(ConnectionString, xPar.TIPOOPERAZIONE)
        End If

        Dim ImportIVA As Double
        Dim Importo As Double
        Dim CodiceBollo As String
        Dim ImportoBollo As Double

        Dim CIva As New Cls_IVA

        If Request.Item("CodiceParente") = 99 Then
            CIva.Codice = x.CODICEIVA
        Else
            CIva.Codice = xPar.CODICEIVA
        End If


        CIva.Leggi(Session("DC_TABELLE"), CIva.Codice)

        ImportIVA = CDbl((CDbl(Txt_ImportoRettaPresenza.Text) + CDbl(Txt_ImportoRettaPresenza2.Text) + CDbl(Txt_ImportoRettaAssenza.Text)) * CIva.Aliquota)


        Dim cn As OleDbConnection

        cn = New Data.OleDb.OleDbConnection(Session("DC_OSPITE"))

        cn.Open()


        If CDbl(Txt_ImportoAddibiti.Text) > 0 Then

            Dim CondizioneAddebiti As String = ""

            If RB_FatturaRetta.Visible = True Then
                If RB_FatturaRetta.Checked = True Then
                    CondizioneAddebiti = " And ((Select FuoriRetta From TabTipoAddebito Where  TabTipoAddebito.Codice = ADDACR.CodiceIVA) =0 Or (Select FuoriRetta From TabTipoAddebito Where  TabTipoAddebito.Codice = ADDACR.CodiceIVA) Is Null) "
                Else
                    CondizioneAddebiti = " And (Select FuoriRetta From TabTipoAddebito Where  TabTipoAddebito.Codice = ADDACR.CodiceIVA) =1 "
                End If
            End If



            If Chk_SoloMeseCorrente.Checked = True Then
                If Request.Item("CodiceParente") = 99 Then
                    MySql = "SelecT * FROM ADDACR WHERE  TIPOMOV = 'AD' AND RIFERIMENTO = 'O' AND CENTROSERVIZIO = '" & Session("CODICESERVIZIO") & "' AND CODICEOSPITE  = " & Session("CODICEOSPITE") & " And (Elaborato = 0 or Elaborato Is Null) And year(Data) = " & Anno & " And month(data) = " & Mese & CondizioneAddebiti
                Else
                    MySql = "SelecT * FROM ADDACR WHERE  TIPOMOV = 'AD' AND  RIFERIMENTO = 'P' AND PARENTE = " & Request.Item("CodiceParente") & " AND CENTROSERVIZIO = '" & Session("CODICESERVIZIO") & "' AND CODICEOSPITE  = " & Session("CODICEOSPITE") & " And (Elaborato = 0 or Elaborato Is Null) And year(Data) = " & Anno & " And month(data) = " & Mese & CondizioneAddebiti
                End If
            Else
                If Request.Item("CodiceParente") = 99 Then
                    MySql = "SelecT * FROM ADDACR WHERE  TIPOMOV = 'AD' AND RIFERIMENTO = 'O' AND CENTROSERVIZIO = '" & Session("CODICESERVIZIO") & "' AND CODICEOSPITE  = " & Session("CODICEOSPITE") & " And (Elaborato = 0 or Elaborato Is Null) " & CondizioneAddebiti
                Else
                    MySql = "SelecT * FROM ADDACR WHERE  TIPOMOV = 'AD' AND  RIFERIMENTO = 'P' AND PARENTE = " & Request.Item("CodiceParente") & " AND CENTROSERVIZIO = '" & Session("CODICESERVIZIO") & "' AND CODICEOSPITE  = " & Session("CODICEOSPITE") & " And (Elaborato = 0 or Elaborato Is Null) " & CondizioneAddebiti
                End If
            End If

            Dim cmd As New OleDbCommand()
            cmd.CommandText = MySql
            cmd.Connection = cn

            Dim myPOSTreader As OleDbDataReader = cmd.ExecuteReader()
            Do While myPOSTreader.Read

                Dim KTipoAddebito As New Cls_Addebito
                KTipoAddebito.CodiceIVA = ""
                KTipoAddebito.Leggi(Session("DC_OSPITE"), campodb(myPOSTreader.Item("CODICEIVA")))

                If KTipoAddebito.CodiceIVA = "" Then
                    If Request.Item("CodiceParente") = 99 Then
                        KTipoAddebito.CodiceIVA = x.CODICEIVA
                    Else
                        KTipoAddebito.CodiceIVA = xPar.CODICEIVA
                    End If
                End If

                If TipoOperazione.SCORPORAIVA = 0 Then
                    Dim kIVA As New Cls_IVA

                    kIVA.Leggi(Session("DC_TABELLE"), KTipoAddebito.CodiceIVA)

                    ImportIVA = ImportIVA + Modulo.MathRound((campodbN(myPOSTreader.Item("IMPORTO")) * kIVA.Aliquota), 2)
                Else
                    Dim kIVA As New Cls_IVA

                    kIVA.Leggi(Session("DC_TABELLE"), KTipoAddebito.CodiceIVA)

                    Dim ImportoScorporato As Double

                    ImportoScorporato = campodbN(myPOSTreader.Item("importo")) * 100 / (100 + (kIVA.Aliquota * 100))
                    

                    ImportIVA = ImportIVA + Modulo.MathRound(ImportoScorporato * kIVA.Aliquota, 2)
                End If
            Loop
            myPOSTreader.Close()
        End If

        If CDbl(Txt_ImportoAccrediti.Text) > 0 Then
            Dim CondizioneAddebiti As String = ""

            If RB_FatturaRetta.Visible = True Then
                If RB_FatturaRetta.Checked = True Then
                    CondizioneAddebiti = " And ((Select FuoriRetta From TabTipoAddebito Where  TabTipoAddebito.Codice = ADDACR.CodiceIVA) =0 Or (Select FuoriRetta From TabTipoAddebito Where  TabTipoAddebito.Codice = ADDACR.CodiceIVA) Is Null) "
                Else
                    CondizioneAddebiti = " And (Select FuoriRetta From TabTipoAddebito Where  TabTipoAddebito.Codice = ADDACR.CodiceIVA) =1 "
                End If
            End If


            If Request.Item("CodiceParente") = 99 Then
                MySql = "SelecT * FROM ADDACR WHERE  TIPOMOV = 'AC' AND RIFERIMENTO = 'O' AND CENTROSERVIZIO = '" & Session("CODICESERVIZIO") & "' AND CODICEOSPITE  = " & Session("CODICEOSPITE") & " And  (Elaborato = 0 or Elaborato Is Null) " & CondizioneAddebiti
            Else
                MySql = "SelecT * FROM ADDACR WHERE  TIPOMOV = 'AC' AND  RIFERIMENTO = 'P' AND PARENTE = " & Request.Item("CodiceParente") & " AND CENTROSERVIZIO = '" & Session("CODICESERVIZIO") & "' AND CODICEOSPITE  = " & Session("CODICEOSPITE") & " And  (Elaborato = 0 or Elaborato Is Null) " & CondizioneAddebiti
            End If
            Dim cmd As New OleDbCommand()
            cmd.CommandText = MySql
            cmd.Connection = cn

            Dim myPOSTreader As OleDbDataReader = cmd.ExecuteReader()
            Do While myPOSTreader.Read

                Dim KTipoAddebito As New Cls_Addebito

                KTipoAddebito.CodiceIVA = ""
                KTipoAddebito.Leggi(Session("DC_OSPITE"), campodb(myPOSTreader.Item("CODICEIVA")))

                If KTipoAddebito.CodiceIVA = "" Then
                    If Request.Item("CodiceParente") = 99 Then
                        KTipoAddebito.CodiceIVA = x.CODICEIVA
                    Else
                        KTipoAddebito.CodiceIVA = xPar.CODICEIVA
                    End If
                End If

                Dim kIVA As New Cls_IVA

                kIVA.Leggi(Session("DC_TABELLE"), KTipoAddebito.CodiceIVA)


                ImportIVA = ImportIVA - Modulo.MathRound((campodbN(myPOSTreader.Item("IMPORTO")) * kIVA.Aliquota), 2)
            Loop
            myPOSTreader.Close()
        End If

        If CDbl(Txt_ExtraFissi.Text) > 0 Then
            If Request.Item("CodiceParente") = 99 Then
                MySql = "SelecT * FROM RETTEOSPITE WHERE ELEMENTO LIKE 'E%' AND  CENTROSERVIZIO = '" & Session("CODICESERVIZIO") & "' AND CODICEOSPITE  = " & Session("CODICEOSPITE") & " And MESE = " & Dd_Mese.SelectedValue & " And ANNO = " & Txt_Anno.Text
            Else
                MySql = "SelecT * FROM RETTEPARENTE WHERE ELEMENTO LIKE 'E%' AND  CENTROSERVIZIO = '" & Session("CODICESERVIZIO") & "' AND CODICEOSPITE  = " & Session("CODICEOSPITE") & " And MESE = " & Dd_Mese.SelectedValue & " And ANNO = " & Txt_Anno.Text & " And CodiceParente = " & Request.Item("CodiceParente")
            End If
            Dim cmd As New OleDbCommand()
            cmd.CommandText = MySql
            cmd.Connection = cn

            Dim myPOSTreader As OleDbDataReader = cmd.ExecuteReader()
            Do While myPOSTreader.Read

                Dim KExtra As New Cls_TipoExtraFisso

                KExtra.CODICEEXTRA = Mid(campodb(myPOSTreader.Item("CODICEEXTRA")), 1, 2)
                KExtra.Leggi(Session("DC_OSPITE"))

                Dim kIVA As New Cls_IVA

                kIVA.Leggi(Session("DC_TABELLE"), KExtra.CodiceIva)

                ImportIVA = ImportIVA + Modulo.MathRound((campodbN(myPOSTreader.Item("IMPORTO")) * kIVA.Aliquota), 2)
            Loop
            myPOSTreader.Close()

        End If
        cn.Close()

        Txt_ImportoIVA.Text = Format(ImportIVA, "#,##0.00")
    End Sub


    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If IsNothing(Session("UTENTE")) Then
            Response.Redirect("..\Login.aspx")
            Exit Sub
        End If
        If Trim(Session("UTENTE")) = "" Then
            Response.Redirect("..\Login.aspx")
            Exit Sub
        End If

        Call EseguiJS()

        If Page.IsPostBack = True Then
            Exit Sub
        End If



        Dim K1 As New Cls_SqlString

        Dim Barra As New Cls_BarraSenior

        If Not IsNothing(Session("RicercaAnagraficaSQLString")) Then
            K1 = Session("RicercaAnagraficaSQLString")
        End If

        Lbl_BarraSenior.Text = Barra.CodiceBarra(Application("SENIOR"), Session("UTENTE"), Page, K1)

        Dim x As New ClsOspite
        Dim xPar As New Cls_Parenti

        x.CodiceOspite = Session("CODICEOSPITE")
        x.Leggi(Session("DC_OSPITE"), x.CodiceOspite)

        Dim DatiCs As New Cls_DatiOspiteParenteCentroServizio


        DatiCs.CentroServizio = Session("CODICESERVIZIO")
        DatiCs.CodiceOspite = Session("CODICEOSPITE")

        If Val(Request.Item("CodiceParente")) = 99 Then
            DatiCs.CodiceParente = 0
        Else
            DatiCs.CodiceParente = Val(Request.Item("CodiceParente"))
        End If
        DatiCs.Leggi(Session("DC_OSPITE"))

        If DatiCs.TipoOperazione <> "" Then
            x.TIPOOPERAZIONE = DatiCs.TipoOperazione
            x.MODALITAPAGAMENTO = DatiCs.ModalitaPagamento
        End If

        Dim TipoOper As New Cls_TipoOperazione

        TipoOper.Codice = x.TIPOOPERAZIONE
        TipoOper.Leggi(Session("DC_OSPITE"), x.TIPOOPERAZIONE)

        If TipoOper.CausaleAddebito <> TipoOper.CausaleRetta And TipoOper.CausaleAddebito <> "" Then
            RB_FatturaRetta.Visible = True
            RB_FatturaFuoriRetta.Visible = True
            RB_FatturaRetta.Checked = True
            RB_FatturaFuoriRetta.Checked = False
        Else
            RB_FatturaRetta.Visible = False
            RB_FatturaFuoriRetta.Visible = False
        End If



        Dim cs As New Cls_CentroServizio

        cs.Leggi(Session("DC_OSPITE"), Session("CODICESERVIZIO"))

        If Val(Request.Item("CodiceParente")) <> 99 Then

            xPar.Leggi(Session("DC_OSPITE"), Session("CODICEOSPITE"), Val(Request.Item("CodiceParente")))
        End If

        If IsNothing(Session("CODICEOSPITE")) Then
            Response.Redirect("../Login.aspx")
            Exit Sub
        End If

        If Val(Session("CODICEOSPITE")) = 0 Then
            Response.Redirect("../Login.aspx")
            Exit Sub
        End If

        Dim DDModalita As New ClsModalitaPagamento
        DDModalita.UpDateDropBox(Session("DC_OSPITE"), DD_ModalitaPagamento)



        DD_ModalitaPagamento.SelectedValue = x.MODALITAPAGAMENTO


        Lbl_NomeOspite.Text = x.Nome & " -  " & x.DataNascita & " - " & cs.DESCRIZIONE & " " & xPar.Nome & "<i style=""font-size:small;"">(" & x.CodiceOspite & ")</i>"


        Dim ParamOspiti As New Cls_Parametri

        ParamOspiti.LeggiParametri(Session("DC_OSPITE"))

        Txt_Anno.Text = ParamOspiti.AnnoFatturazione
        Dd_Mese.SelectedValue = ParamOspiti.MeseFatturazione

        If ParamOspiti.DocumentiIncassiUltimaUscitaDefinitiva = 1 Then
            Dim Mov As New Cls_Movimenti

            Mov.CodiceOspite = Session("CODICEOSPITE")
            Mov.CENTROSERVIZIO = Session("CODICESERVIZIO")
            Mov.UltimaDataUscitaDefinitiva(Session("DC_OSPITE"))

            Txt_Anno.Text = Year(Mov.Data)
            Dd_Mese.SelectedValue = Month(Mov.Data)

        End If



        Dim k As New Cls_CausaleContabile
        Dim ConnectionString As String = Session("DC_TABELLE")

        k.UpDateDropBoxPag(ConnectionString, Dd_CausaleContabile)

        Txt_ImportoRettaPresenza.Text = "0"
        Txt_ImportoRettaPresenza2.Text = "0"
        Txt_GiorniPresenza.Text = "0"
        Txt_ImportoRettaAssenza.Text = "0"
        Txt_GiorniAssenza.Text = "0"
        Txt_ExtraFissi.Text = "0"
        Txt_ImportoAddibiti.Text = "0"
        Txt_ImportoAccrediti.Text = "0"


        'Calcolo(ParamOspiti.MeseFatturazione, ParamOspiti.AnnoFatturazione)
        Dim Param As New Cls_Parametri

        Param.LeggiParametri(Session("DC_OSPITE"))

        If Param.DocumentiIncassiMesiPrecedenti = 0 Then
            Call Calcolo(Dd_Mese.SelectedValue, Txt_Anno.Text)

            Txt_TotaleFatturato.Visible = False
            Txt_ImportoRettaPresenzaCalcolato.Visible = False
            Txt_GiorniCalcolato.Visible = False
            Txt_GiorniFattura.Visible = False
            lbl_fatturato.Visible = False
            Lbl_Calcolato.Visible = False
            Lbl_ImpFatturato.Visible = False
            Lbl_ImpCalcolato.Visible = False
        Else
            Txt_ImportoRettaPresenza.Text = CDbl(Txt_ImportoRettaPresenza.Text) + CDbl(Txt_ImportoRettaAssenza.Text) + CDbl(Txt_ImportoRettaPresenza2.Text)
            Txt_GiorniPresenza.Text = Int(Txt_GiorniPresenza.Text) + Int(Txt_GiorniAssenza.Text)

            Dim I As Integer
            Dim Mese As Integer = Dd_Mese.SelectedValue
            Dim Anno As Integer = Txt_Anno.Text
            Call Calcolo(Dd_Mese.SelectedValue, Txt_Anno.Text)
            For I = 1 To Param.DocumentiIncassiMesiPrecedenti
                If Mese = 1 Then
                    Mese = 12
                    Anno = Anno - 1
                Else
                    Mese = Mese - 1
                End If
                Call Calcolo(Mese, Anno)
            Next
            Call ConguagliaFatturato()


            If Param.DocumentiIncassiNonPeriodo = 1 Then
                Lbl_Anno.Visible = False
                Lbl_Mese.Visible = False
                Txt_Anno.Visible = False
                Dd_Mese.Visible = False
                btn_Calcolo.Visible = False
            End If

            Chk_Conferma.Checked = True
            Chk_Conferma.Visible = False
            lbl_Errore.Visible = False
            Lbl_ImportoRetteAssenza.Visible = False
            Lbl_GiorniAss.Visible = False
            Call btn_calcola_Click(sender, e)

            Txt_ImportoRettaAssenza.Visible = False
            Txt_GiorniAssenza.Visible = False
        End If




        Txt_DataRegistrazione.Text = Format(Now, "dd/MM/yyyy")
        Txt_DataIncasso.Text = Format(Now, "dd/MM/yyyy")


        If ParamOspiti.DocumentiIncassiBloccato = 1 Then
            Txt_Anno.Enabled = False
            Dd_Mese.Enabled = False
        Else
            Txt_Anno.Enabled = True
            Dd_Mese.Enabled = True
        End If
        Call EseguiJS()
    End Sub

    Function campodbN(ByVal oggetto As Object) As Double
        If IsDBNull(oggetto) Then
            Return 0
        Else
            Return oggetto
        End If
    End Function
    Function campodb(ByVal oggetto As Object) As String
        If IsDBNull(oggetto) Then
            Return ""
        Else
            Return oggetto
        End If
    End Function

    Function campodbd(ByVal oggetto As Object) As Date
        If IsDBNull(oggetto) Then
            Return Nothing
        Else
            Return oggetto
        End If
    End Function
    Private Sub EseguiJS()
        Dim MyJs As String
        MyJs = "$(document).ready(function() {"

        MyJs = MyJs & "var els = document.getElementsByTagName(""*"");"

        MyJs = MyJs & "for (var i=0;i<els.length;i++)"
        MyJs = MyJs & "if ( els[i].id ) { "
        MyJs = MyJs & " var appoggio =els[i].id; "
        MyJs = MyJs & " if ((appoggio.match('Txt_Data')!= null) || (appoggio.match('Txt_Decesso')!= null))  {  "
        MyJs = MyJs & " $(els[i]).mask(""99/99/9999"");"
        MyJs = MyJs & " $(els[i]).datepicker({ changeMonth: true,changeYear: true,  yearRange: ""1990:" & Year(Now) + 5 & """},$.datepicker.regional[""it""]);"
        MyJs = MyJs & "    }"
        MyJs = MyJs & " if ((appoggio.match('Txt_Importo')!= null) ) {  "
        MyJs = MyJs & " $(els[i]).keypress(function() { ForceNumericInput($(this).val(), true, true); } ); "
        MyJs = MyJs & " $(els[i]).blur(function() { var ap = formatNumber($(this).val(),2); $(this).val(ap); });"
        MyJs = MyJs & "    }"

        MyJs = MyJs & "} "
        MyJs = MyJs & "});"

        'MyJs = "$(document).ready(function() { $('.myClass').keypress(function() { handleEnter($('.myClass').val(), true, true); } );     $('#" & Txt_ImportoPagato.ClientID & "').blur(function() { var ap = formatNumber ($('#" & Txt_ImportoPagato.ClientID & "').val(),2); $('#" & Txt_ImportoPagato.ClientID & "').val(ap); }); });"
        ScriptManager.RegisterStartupScript(Me, Me.GetType(), "visualizzaRitIm", MyJs, True)
    End Sub

    Private Sub CreaDocumento(ByVal TipoMovimento As String)
        Call CalcolaTotale()
        Dim CodiceBollo As String
        Dim ImportoBollo As Double
        Dim CIva As New Cls_IVA


        If CDbl(Txt_Totale.Text) = 0 Then
            REM Lbl_Errore.Text = "Specificare importo deposito cauzionale"
            ClientScript.RegisterClientScriptBlock(Me.GetType(), "Errore", "VisualizzaErrore('Specificare importo');", True)
            Exit Sub
        End If
        If Txt_DataRegistrazione.Text = "" Then
            ClientScript.RegisterClientScriptBlock(Me.GetType(), "Errore", "VisualizzaErrore('Specificare data registrazione');", True)
            REM Lbl_Errore.Text = "Specificare data addebito"
            Exit Sub
        End If

 
        Dim Param As New Cls_Parametri

        Param.LeggiParametri(Session("DC_OSPITE"))



        Dim cn As OleDbConnection

        cn = New Data.OleDb.OleDbConnection(Session("DC_OSPITE"))

        cn.Open()


        Dim MySql As String
        Dim x As New ClsOspite
        Dim xPar As New Cls_Parenti

        Dim ConnectionString As String = Session("DC_OSPITE")
        Dim ConnectionStringTabelle As String = Session("DC_TABELLE")
        Dim ConnectionStringGenerale As String = Session("DC_GENERALE")



        If Request.Item("CodiceParente") = 99 Then
            x.Leggi(ConnectionString, Session("CODICEOSPITE"))
        Else
            xPar.Leggi(ConnectionString, Session("CODICEOSPITE"), Val(Request.Item("CodiceParente")))
        End If




        Dim KC2s As New Cls_DatiOspiteParenteCentroServizio

        KC2s.CentroServizio = Session("CODICESERVIZIO")
        KC2s.CodiceOspite = Session("CODICEOSPITE")
        If Request.Item("CodiceParente") = 99 Then
            KC2s.CodiceParente = 0
        Else
            KC2s.CodiceParente = Val(Request.Item("CodiceParente"))
        End If
        KC2s.Leggi(ConnectionString)

        REM Assegna la tipologia operazione e l'aliquota prendendo quella relativa al cserv
        If KC2s.CodiceOspite <> 0 Then
            If Request.Item("CodiceParente") = 99 Then
                x.TIPOOPERAZIONE = KC2s.TipoOperazione
                x.CODICEIVA = KC2s.AliquotaIva
                x.Compensazione = KC2s.Compensazione
            Else
                xPar.TIPOOPERAZIONE = KC2s.TipoOperazione
                xPar.CODICEIVA = KC2s.AliquotaIva
                xPar.Compensazione = KC2s.Compensazione
            End If

        End If

        If Request.Item("CodiceParente") = 99 Then
            If Trim(x.TIPOOPERAZIONE) = "" Then
                cn.Close()
                ClientScript.RegisterClientScriptBlock(Me.GetType(), "Errore", "VisualizzaErrore('Devi Specificare il tipo operazione');", True)
                Call EseguiJS()
                Exit Sub
            End If
            If Trim(x.CODICEIVA) = "" Then
                cn.Close()
                ClientScript.RegisterClientScriptBlock(Me.GetType(), "Errore", "VisualizzaErrore('Devi Specificare il CODICE IVA');", True)
                Call EseguiJS()
                Exit Sub
            End If
        Else
            If Trim(xPar.TIPOOPERAZIONE) = "" Then
                cn.Close()
                ClientScript.RegisterClientScriptBlock(Me.GetType(), "Errore", "VisualizzaErrore('Devi Specificare il tipo operazione');", True)
                Call EseguiJS()
                Exit Sub
            End If
            If Trim(xPar.CODICEIVA) = "" Then
                cn.Close()
                ClientScript.RegisterClientScriptBlock(Me.GetType(), "Errore", "VisualizzaErrore('Devi Specificare il CODICE IVA');", True)
                Call EseguiJS()
                Exit Sub
            End If
        End If



        Dim TipoOperazione As New Cls_TipoOperazione

        If Request.Item("CodiceParente") = 99 Then
            TipoOperazione.Leggi(ConnectionString, x.TIPOOPERAZIONE)
        Else
            TipoOperazione.Leggi(ConnectionString, xPar.TIPOOPERAZIONE)
        End If


        Dim CausaleContabile As New Cls_CausaleContabile
        Dim Registrazione As New Cls_MovimentoContabile


        If RB_FatturaRetta.Visible = True Then
            If RB_FatturaRetta.Checked = True Then
                If TipoMovimento = "DOC" Then
                    CausaleContabile.Leggi(ConnectionStringTabelle, TipoOperazione.CausaleRetta)
                Else
                    CausaleContabile.Leggi(ConnectionStringTabelle, TipoOperazione.CausaleAccredito)
                End If
            Else
                If TipoMovimento = "DOC" Then
                    CausaleContabile.Leggi(ConnectionStringTabelle, TipoOperazione.CausaleAddebito)
                Else
                    CausaleContabile.Leggi(ConnectionStringTabelle, TipoOperazione.CausaleStorno)
                End If
            End If
        Else
            If TipoMovimento = "DOC" Then
                CausaleContabile.Leggi(ConnectionStringTabelle, TipoOperazione.CausaleRetta)
            Else
                CausaleContabile.Leggi(ConnectionStringTabelle, TipoOperazione.CausaleAccredito)
            End If
        End If

        Dim MyCau As New Cls_CausaleContabile



        Dim RegIva As New Cls_RegistriIVAanno
        Dim DataAppo As Date

        Try
            DataAppo = Txt_DataRegistrazione.Text
        Catch ex As Exception
            ClientScript.RegisterClientScriptBlock(Me.GetType(), "Errore", "VisualizzaErrore('Errore nella data di registrazione');", True)
            Call EseguiJS()
            Exit Sub
        End Try



        RegIva.Tipo = CausaleContabile.RegistroIVA
        RegIva.Anno = Year(DataAppo)
        RegIva.Leggi(Session("DC_TABELLE"), RegIva.Tipo, RegIva.Anno)

        If Format(RegIva.DataStampa, "yyyyMMdd") >= Format(DataAppo, "yyyyMMdd") Then
            cn.Close()
            ClientScript.RegisterClientScriptBlock(Me.GetType(), "Errore", "VisualizzaErrore('Registro iva chiuso prima della dataregistrazione');", True)
            Call EseguiJS()
            Exit Sub
        End If

        If Format(Registrazione.MaxDataRegistroIVA(Session("DC_GENERALE"), Year(DataAppo), CausaleContabile.RegistroIVA), "yyyyMMdd") > Format(DataAppo, "yyyyMMdd") Then
            cn.Close()
            ClientScript.RegisterClientScriptBlock(Me.GetType(), "Errore", "VisualizzaErrore('Già registrato una documento con data successiva');", True)
            Call EseguiJS()
            Exit Sub
        End If



        'Dim cmd_DtReg As New OleDbCommand()
        'cmd_DtReg.CommandText = ("select max(DataRegistrazione) as DataRegSt from MovimentiContabiliTesta where  RegistroIVA = ? And AnnoProtocollo = ? ")

        'cmd_DtReg.Parameters.AddWithValue("@RegistroIva", MyCau.RegistroIVA)
        'cmd_DtReg.Parameters.AddWithValue("@AnnoProtocollo", Val(Txt_Anno.Text))
        'cmd_DtReg.Connection = cn
        'Dim VerReader As OleDbDataReader = cmd_DtReg.ExecuteReader()
        'If VerReader.Read Then
        '    If Format(campodbd(VerReader.Item("DataRegSt")), "yyyy-MM-dd") > Format(DataAppo, "yyyyMMdd") Then
        '        cn.Close()
        '        ClientScript.RegisterClientScriptBlock(Me.GetType(), "Errore", "VisualizzaErrore('Data Registrazione inferiore all'ultimo protocollo inserito');", True)
        '        Call EseguiJS()
        '        Exit Sub
        '    End If
        'End If
        'VerReader.Close()



        Dim ImportIVA As Double
        Dim Importo As Double



        If Request.Item("CodiceParente") = 99 Then
            CIva.Codice = x.CODICEIVA
        Else
            CIva.Codice = xPar.CODICEIVA
        End If


        If TipoMovimento = "DOCCAP" Or TipoMovimento = "NCCAP" Then
            CIva.Codice = TipoOperazione.CodiceIVACaparra
        End If
        CIva.Leggi(Session("DC_TABELLE"), CIva.Codice)

        ImportIVA = CDbl((CDbl(Txt_ImportoRettaPresenza.Text) + CDbl(Txt_ImportoRettaPresenza2.Text) + CDbl(Txt_ImportoRettaAssenza.Text)) * CIva.Aliquota)

        Importo = CDbl(Txt_ImportoRettaPresenza.Text) + CDbl(Txt_ImportoRettaPresenza2.Text) + CDbl(Txt_ImportoRettaAssenza.Text)

        If TipoOperazione.SoggettaABollo = "S" Then
            Dim ClsBollo As New Cls_bolli

            ClsBollo.Leggi(Session("DC_TABELLE"), Txt_DataRegistrazione.Text)

            If Math.Abs(Importo + CDbl(Txt_ImportoAddibiti.Text) + CDbl(Txt_ExtraFissi.Text) - CDbl(Txt_ImportoAccrediti.Text)) > ClsBollo.ImportoApplicazione Then
                If TipoMovimento = "NC" Then
                    ImportoBollo = ClsBollo.ImportoBollo
                    CodiceBollo = ClsBollo.CodiceIVA
                Else
                    ImportoBollo = ClsBollo.ImportoBollo
                    CodiceBollo = ClsBollo.CodiceIVA
                End If
            End If
        End If

        Registrazione.DataRegistrazione = Txt_DataRegistrazione.Text
        Registrazione.DataDocumento = Txt_DataRegistrazione.Text
        Registrazione.NumeroDocumento = Registrazione.MaxProtocollo(ConnectionStringGenerale, Year(Txt_DataRegistrazione.Text), CausaleContabile.RegistroIVA) + 1


        Dim ModalitaPagamentoOspiti As New ClsModalitaPagamento

        ModalitaPagamentoOspiti.Codice = DD_ModalitaPagamento.SelectedValue
        ModalitaPagamentoOspiti.Leggi(Session("DC_OSPITE"))

        Registrazione.CodicePagamento = ModalitaPagamentoOspiti.ModalitaPagamento        

        If TipoMovimento = "DOC" Then
            Registrazione.CausaleContabile = TipoOperazione.CausaleRetta
        Else
            Registrazione.CausaleContabile = TipoOperazione.CausaleAccredito
        End If

        Registrazione.AnnoCompetenza = Val(Txt_Anno.Text)
        Registrazione.MeseCompetenza = Val(Dd_Mese.SelectedValue)
        Registrazione.CentroServizio = Session("CODICESERVIZIO")

        Registrazione.RegistroIVA = CausaleContabile.RegistroIVA
        Registrazione.AnnoProtocollo = Year(Txt_DataRegistrazione.Text)
        Registrazione.NumeroProtocollo = Registrazione.NumeroDocumento

        If Txt_DescrizioneQD.Text.Trim <> "" Then
            Registrazione.Descrizione = Txt_Descrizione.Text & vbNewLine & Txt_DescrizioneQD.Text.Trim
        Else
            Registrazione.Descrizione = Txt_Descrizione.Text
        End If


        Registrazione.Utente = Session("UTENTE")


        Dim CentroServizio As New Cls_CentroServizio

        CentroServizio.Leggi(ConnectionString, Session("CODICESERVIZIO"))
        Registrazione.Righe(0) = New Cls_MovimentiContabiliRiga
        Registrazione.Righe(0).MastroPartita = CentroServizio.MASTRO
        Registrazione.Righe(0).ContoPartita = CentroServizio.CONTO
        Registrazione.Righe(0).SottocontoPartita = Session("CODICEOSPITE") * 100
        If Val(Request.Item("CodiceParente")) < 99 And Val(Request.Item("CodiceParente")) > 0 Then
            Registrazione.Righe(0).SottocontoPartita = (Session("CODICEOSPITE") * 100) + Val(Request.Item("CodiceParente"))
        End If
        Registrazione.Righe(0).DareAvere = CausaleContabile.Righe(0).DareAvere
        Registrazione.Righe(0).Segno = CausaleContabile.Righe(0).Segno


        Registrazione.Righe(0).Importo = Math.Abs(CDbl(Txt_Totale.Text))



        Registrazione.Righe(0).Tipo = "CF"
        Registrazione.Righe(0).RigaDaCausale = 1
        Registrazione.Righe(0).Utente = Session("UTENTE")


        Registrazione.Righe(1) = New Cls_MovimentiContabiliRiga
        Registrazione.Righe(1).MastroPartita = CausaleContabile.Righe(1).Mastro
        Registrazione.Righe(1).ContoPartita = CausaleContabile.Righe(1).Conto
        Registrazione.Righe(1).SottocontoPartita = CausaleContabile.Righe(1).Sottoconto

        If TipoMovimento = "DOC" Then
            Registrazione.Righe(1).DareAvere = CausaleContabile.Righe(1).DareAvere
        Else
            Registrazione.Righe(1).DareAvere = CausaleContabile.Righe(1).DareAvere
        End If


        Registrazione.Righe(1).Segno = CausaleContabile.Righe(1).Segno
        Registrazione.Righe(1).Importo = ImportIVA
        Registrazione.Righe(1).Imponibile = Importo
        Registrazione.Righe(1).CodiceIVA = CIva.Codice
        Registrazione.Righe(1).Tipo = "IV"
        Registrazione.Righe(1).RigaDaCausale = 2
        Registrazione.Righe(1).Utente = Session("UTENTE")


        Dim Riga As Integer = 1

        If Txt_GiorniPresenza.Text = "" Then
            Txt_GiorniPresenza.Text = "0"
        End If



        If CDbl(Txt_ImportoRettaPresenza.Text) > 0 Then
            Riga = Riga + 1
            Registrazione.Righe(Riga) = New Cls_MovimentiContabiliRiga
            Registrazione.Righe(Riga).MastroPartita = CausaleContabile.Righe(2).Mastro
            Registrazione.Righe(Riga).ContoPartita = CausaleContabile.Righe(2).Conto
            Registrazione.Righe(Riga).SottocontoPartita = CausaleContabile.Righe(2).Sottoconto
            Registrazione.Righe(Riga).DareAvere = CausaleContabile.Righe(2).DareAvere
            Registrazione.Righe(Riga).Segno = CausaleContabile.Righe(2).Segno
            Registrazione.Righe(Riga).Importo = CDbl(Txt_ImportoRettaPresenza.Text)
            Registrazione.Righe(Riga).Descrizione = "Importo Presenze"
            Registrazione.Righe(Riga).Quantita = Txt_GiorniPresenza.Text
            Registrazione.Righe(Riga).Imponibile = 0
            Registrazione.Righe(Riga).CodiceIVA = CIva.Codice
            Registrazione.Righe(Riga).Tipo = ""
            Registrazione.Righe(Riga).RigaDaCausale = 3
            Registrazione.Righe(Riga).Utente = Session("UTENTE")
        End If


        If CDbl(Txt_ImportoRettaAssenza.Text) > 0 Then
            Riga = Riga + 1
            Registrazione.Righe(Riga) = New Cls_MovimentiContabiliRiga
            Registrazione.Righe(Riga).MastroPartita = CausaleContabile.Righe(2).Mastro
            Registrazione.Righe(Riga).ContoPartita = CausaleContabile.Righe(2).Conto
            Registrazione.Righe(Riga).SottocontoPartita = CausaleContabile.Righe(2).Sottoconto
            Registrazione.Righe(Riga).DareAvere = CausaleContabile.Righe(2).DareAvere
            Registrazione.Righe(Riga).Segno = CausaleContabile.Righe(2).Segno
            Registrazione.Righe(Riga).Importo = CDbl(Txt_ImportoRettaAssenza.Text)
            Registrazione.Righe(Riga).Quantita = Txt_GiorniAssenza.Text
            Registrazione.Righe(Riga).Imponibile = 0
            Registrazione.Righe(Riga).Descrizione = "Importo Assenze"
            Registrazione.Righe(Riga).CodiceIVA = x.CODICEIVA
            Registrazione.Righe(Riga).Tipo = ""
            Registrazione.Righe(Riga).RigaDaCausale = 3
            Registrazione.Righe(Riga).Utente = Session("UTENTE")
        End If

        If CDbl(Txt_ImportoRettaPresenza2.text) > 0 Then
            Riga = Riga + 1
            Registrazione.Righe(Riga) = New Cls_MovimentiContabiliRiga
            Registrazione.Righe(Riga).MastroPartita = CausaleContabile.Righe(2).Mastro
            Registrazione.Righe(Riga).ContoPartita = CausaleContabile.Righe(2).Conto
            Registrazione.Righe(Riga).SottocontoPartita = CausaleContabile.Righe(2).Sottoconto
            Registrazione.Righe(Riga).DareAvere = CausaleContabile.Righe(2).DareAvere
            Registrazione.Righe(Riga).Segno = CausaleContabile.Righe(2).Segno
            Registrazione.Righe(Riga).Importo = CDbl(Txt_ImportoRettaPresenza2.Text)
            Registrazione.Righe(Riga).Descrizione = "Importo Presenze"
            Registrazione.Righe(Riga).Quantita = Txt_GiorniPresenza.Text
            Registrazione.Righe(Riga).Imponibile = 0
            Registrazione.Righe(Riga).CodiceIVA = CIva.Codice
            Registrazione.Righe(Riga).Tipo = ""
            Registrazione.Righe(Riga).RigaDaCausale = 13
            Registrazione.Righe(Riga).Utente = Session("UTENTE")

        End If

        If CDbl(Txt_ExtraFissi.Text) > 0 Then

            If Param.DocumentiIncassiMesiPrecedenti = 0 Then
                If Request.Item("CodiceParente") = 99 Then
                    MySql = "SelecT * FROM RETTEOSPITE WHERE Elemento Like 'E%' AND CENTROSERVIZIO = '" & Session("CODICESERVIZIO") & "' AND CODICEOSPITE  = " & Session("CODICEOSPITE") & " And MESE= " & Dd_Mese.SelectedValue & " And ANNO = " & Txt_Anno.Text
                Else
                    MySql = "SelecT * FROM RETTEPARENTE  WHERE  Elemento Like 'E%' AND CODICEPARENTE = " & Request.Item("CodiceParente") & " AND CENTROSERVIZIO = '" & Session("CODICESERVIZIO") & "' AND CODICEOSPITE  = " & Session("CODICEOSPITE") & " And MESE= " & Dd_Mese.SelectedValue & " And ANNO = " & Txt_Anno.Text
                End If
            Else

                Dim Mese As Integer = Dd_Mese.SelectedValue
                Dim Anno As Integer = Txt_Anno.Text
                Dim Condizione As String = ""

                Condizione = Condizione & " ( Anno = " & Anno & " And Mese = " & Mese & ") "

                For I = 1 To Param.DocumentiIncassiMesiPrecedenti
                    If Mese = 1 Then
                        Mese = 12
                        Anno = Anno - 1
                    Else
                        Mese = Mese - 1
                    End If
                    If Condizione <> "" Then
                        Condizione = Condizione & " OR "
                    End If
                    Condizione = Condizione & " ( Anno = " & Anno & " And Mese = " & Mese & ") "

                Next


                If Request.Item("CodiceParente") = 99 Then
                    MySql = "SelecT * FROM RETTEOSPITE WHERE Elemento Like 'E%' AND CENTROSERVIZIO = '" & Session("CODICESERVIZIO") & "' AND CODICEOSPITE  = " & Session("CODICEOSPITE") & " And (" & Condizione & ")"
                Else
                    MySql = "SelecT * FROM RETTEPARENTE  WHERE  Elemento Like 'E%' AND CODICEPARENTE = " & Request.Item("CodiceParente") & " AND CENTROSERVIZIO = '" & Session("CODICESERVIZIO") & "' AND CODICEOSPITE  = " & Session("CODICEOSPITE") & " And (" & Condizione & ")"
                End If
            End If

            Dim cmd As New OleDbCommand()
            cmd.CommandText = MySql
            cmd.Connection = cn

            Dim myPOSTreader As OleDbDataReader = cmd.ExecuteReader()
            Do While myPOSTreader.Read
                Riga = Riga + 1

                Dim KTipoAddebito As New Cls_TipoExtraFisso

                KTipoAddebito.CODICEEXTRA = campodb(myPOSTreader.Item("CODICEEXTRA"))
                KTipoAddebito.Leggi(Session("DC_OSPITE"))

                If IsNothing(KTipoAddebito.CodiceIva) Then
                    KTipoAddebito.CodiceIva = x.CODICEIVA
                End If

                Dim kIVA As New Cls_IVA

                kIVA.Leggi(Session("DC_TABELLE"), KTipoAddebito.CodiceIva)

                Registrazione.Righe(Riga) = New Cls_MovimentiContabiliRiga
                Registrazione.Righe(Riga).MastroPartita = CausaleContabile.Righe(1).Mastro
                Registrazione.Righe(Riga).ContoPartita = CausaleContabile.Righe(1).Conto
                Registrazione.Righe(Riga).SottocontoPartita = CausaleContabile.Righe(1).Sottoconto
                Registrazione.Righe(Riga).DareAvere = CausaleContabile.Righe(1).DareAvere

                Registrazione.Righe(Riga).Segno = CausaleContabile.Righe(1).Segno
                Registrazione.Righe(Riga).Descrizione = ""
                Registrazione.Righe(Riga).Importo = Modulo.MathRound((campodbN(myPOSTreader.Item("IMPORTO")) * kIVA.Aliquota), 2)
                Registrazione.Righe(Riga).Imponibile = campodbN(myPOSTreader.Item("IMPORTO"))
                Registrazione.Righe(Riga).CodiceIVA = KTipoAddebito.CodiceIva
                Registrazione.Righe(Riga).Tipo = "IV"
                Registrazione.Righe(Riga).RigaDaCausale = 2
                Registrazione.Righe(Riga).Utente = Session("UTENTE")

                Riga = Riga + 1

                Registrazione.Righe(Riga) = New Cls_MovimentiContabiliRiga
                Registrazione.Righe(Riga).MastroPartita = KTipoAddebito.Mastro
                Registrazione.Righe(Riga).ContoPartita = KTipoAddebito.Conto
                Registrazione.Righe(Riga).SottocontoPartita = KTipoAddebito.Sottoconto
                Registrazione.Righe(Riga).DareAvere = CausaleContabile.Righe(2).DareAvere
                Registrazione.Righe(Riga).Segno = CausaleContabile.Righe(2).Segno
                Registrazione.Righe(Riga).Importo = campodbN(myPOSTreader.Item("IMPORTO"))
                Registrazione.Righe(Riga).Imponibile = 0
                Registrazione.Righe(Riga).CodiceIVA = KTipoAddebito.CodiceIva
                Registrazione.Righe(Riga).Descrizione = KTipoAddebito.Descrizione
                Registrazione.Righe(Riga).Tipo = ""
                Registrazione.Righe(Riga).TipoExtra = "E" & campodb(myPOSTreader.Item("CODICEEXTRA"))
                Registrazione.Righe(Riga).RigaDaCausale = 3
                Registrazione.Righe(Riga).Utente = Session("UTENTE")



            Loop
            myPOSTreader.Close()
        End If


        If CDbl(Txt_ImportoAddibiti.Text) > 0 Then

            Dim CondizioneAddebiti As String = ""

            If RB_FatturaRetta.Visible = True Then
                If RB_FatturaRetta.Checked = True Then
                    CondizioneAddebiti = " And ((Select FuoriRetta From TabTipoAddebito Where  TabTipoAddebito.Codice = ADDACR.CodiceIVA) =0 Or (Select FuoriRetta From TabTipoAddebito Where  TabTipoAddebito.Codice = ADDACR.CodiceIVA) Is Null) "
                Else
                    CondizioneAddebiti = " And (Select FuoriRetta From TabTipoAddebito Where  TabTipoAddebito.Codice = ADDACR.CodiceIVA) =1 "
                End If
            End If

            If Chk_SoloMeseCorrente.Checked = True Then

                If Request.Item("CodiceParente") = 99 Then
                    MySql = "SelecT * FROM ADDACR WHERE  TIPOMOV = 'AD' AND RIFERIMENTO = 'O' AND CENTROSERVIZIO = '" & Session("CODICESERVIZIO") & "' AND CODICEOSPITE  = " & Session("CODICEOSPITE") & "  And (Elaborato = 0 or Elaborato Is Null) And year(Data) = " & Txt_Anno.Text & " And month(data) = " & Val(Dd_Mese.SelectedValue) & CondizioneAddebiti
                Else
                    MySql = "SelecT * FROM ADDACR WHERE  TIPOMOV = 'AD' AND  RIFERIMENTO = 'P' AND PARENTE = " & Request.Item("CodiceParente") & " AND CENTROSERVIZIO = '" & Session("CODICESERVIZIO") & "' AND CODICEOSPITE  = " & Session("CODICEOSPITE") & "  And (Elaborato = 0 or Elaborato Is Null) And year(Data) = " & Txt_Anno.Text & " And month(data) = " & Val(Dd_Mese.SelectedValue)
                End If
            Else
                If Request.Item("CodiceParente") = 99 Then
                    MySql = "SelecT * FROM ADDACR WHERE  TIPOMOV = 'AD' AND RIFERIMENTO = 'O' AND CENTROSERVIZIO = '" & Session("CODICESERVIZIO") & "' AND CODICEOSPITE  = " & Session("CODICEOSPITE") & " And (Year(DATA) = " & Txt_Anno.Text & " or Year(DATA) = " & Val(Txt_Anno.Text) - 1 & ") And (Elaborato = 0 or Elaborato Is Null) " & CondizioneAddebiti
                Else
                    MySql = "SelecT * FROM ADDACR WHERE  TIPOMOV = 'AD' AND  RIFERIMENTO = 'P' AND PARENTE = " & Request.Item("CodiceParente") & " AND CENTROSERVIZIO = '" & Session("CODICESERVIZIO") & "' AND CODICEOSPITE  = " & Session("CODICEOSPITE") & " And (Year(DATA) = " & Txt_Anno.Text & " or Year(DATA) = " & Val(Txt_Anno.Text) - 1 & ") And (Elaborato = 0 or Elaborato Is Null) " & CondizioneAddebiti
                End If
            End If

            Dim cmd As New OleDbCommand()
            cmd.CommandText = MySql
            cmd.Connection = cn

            Dim myPOSTreader As OleDbDataReader = cmd.ExecuteReader()
            Do While myPOSTreader.Read
                Riga = Riga + 1

                Dim KTipoAddebito As New Cls_Addebito

                KTipoAddebito.Leggi(Session("DC_OSPITE"), campodb(myPOSTreader.Item("CODICEIVA")))

                If KTipoAddebito.CodiceIVA = "" Then
                    If Request.Item("CodiceParente") = 99 Then
                        KTipoAddebito.CodiceIVA = x.CODICEIVA
                    Else
                        KTipoAddebito.CodiceIVA = xPar.CODICEIVA
                    End If
                End If

                Dim kIVA As New Cls_IVA

                kIVA.Leggi(Session("DC_TABELLE"), KTipoAddebito.CodiceIVA)



                Registrazione.Righe(Riga) = New Cls_MovimentiContabiliRiga
                Registrazione.Righe(Riga).MastroPartita = CausaleContabile.Righe(1).Mastro
                Registrazione.Righe(Riga).ContoPartita = CausaleContabile.Righe(1).Conto
                Registrazione.Righe(Riga).SottocontoPartita = CausaleContabile.Righe(1).Sottoconto
                Registrazione.Righe(Riga).DareAvere = CausaleContabile.Righe(1).DareAvere

                Registrazione.Righe(Riga).Segno = CausaleContabile.Righe(1).Segno
                Registrazione.Righe(Riga).Descrizione = ""


                If TipoOperazione.SCORPORAIVA = 0 Then
                    Registrazione.Righe(Riga).Importo = Modulo.MathRound((campodbN(myPOSTreader.Item("IMPORTO")) * kIVA.Aliquota), 2)
                    Registrazione.Righe(Riga).Imponibile = campodbN(myPOSTreader.Item("IMPORTO"))
                Else
                    Dim ImportoScorporato As Double

                    ImportoScorporato = campodbN(myPOSTreader.Item("importo")) * 100 / (100 + (kIVA.Aliquota * 100))

                    Registrazione.Righe(Riga).Importo = Modulo.MathRound(ImportoScorporato * kIVA.Aliquota, 2)
                    Registrazione.Righe(Riga).Imponibile = Modulo.MathRound(ImportoScorporato, 2)

                End If


                Registrazione.Righe(Riga).CodiceIVA = KTipoAddebito.CodiceIVA
                Registrazione.Righe(Riga).Tipo = "IV"
                Registrazione.Righe(Riga).RigaDaCausale = 2
                Registrazione.Righe(Riga).Utente = Session("UTENTE")

                Riga = Riga + 1

                Registrazione.Righe(Riga) = New Cls_MovimentiContabiliRiga
                Registrazione.Righe(Riga).MastroPartita = KTipoAddebito.Mastro
                Registrazione.Righe(Riga).ContoPartita = KTipoAddebito.Conto
                Registrazione.Righe(Riga).SottocontoPartita = KTipoAddebito.Sottoconto
                Registrazione.Righe(Riga).DareAvere = CausaleContabile.Righe(2).DareAvere
                Registrazione.Righe(Riga).Segno = CausaleContabile.Righe(2).Segno

                If TipoOperazione.SCORPORAIVA = 0 Then
                    Registrazione.Righe(Riga).Importo = campodbN(myPOSTreader.Item("IMPORTO"))
                Else
                    Dim ImportoScorporato As Double

                    ImportoScorporato = campodbN(myPOSTreader.Item("importo")) * 100 / (100 + (kIVA.Aliquota * 100))

                    Registrazione.Righe(Riga).Importo = Modulo.MathRound(ImportoScorporato, 2)
                End If

                Registrazione.Righe(Riga).Imponibile = 0
                Registrazione.Righe(Riga).CodiceIVA = KTipoAddebito.CodiceIVA
                Registrazione.Righe(Riga).Descrizione = campodb(myPOSTreader.Item("DESCRIZIONE")) ' KTipoAddebito.Descrizione  ' & " " &
                Registrazione.Righe(Riga).Tipo = ""
                Registrazione.Righe(Riga).TipoExtra = "A" & campodb(myPOSTreader.Item("CODICEIVA"))
                Registrazione.Righe(Riga).RigaDaCausale = 4
                Registrazione.Righe(Riga).Utente = Session("UTENTE")

                MySql = "UPDATE ADDACR SET Elaborato= 1,UTENTE = ?,DataAggiornamento =?   WHERE  CentroServizio = '" & Session("CODICESERVIZIO") & "' And  CodiceOspite = " & Session("CODICEOSPITE") & " And ID = " & campodbN(myPOSTreader.Item("ID"))
                Dim cmdw As New OleDbCommand()
                cmdw.Connection = cn
                cmdw.Parameters.AddWithValue("@UTENTE", Session("UTENTE"))
                cmdw.Parameters.AddWithValue("@DataAggiornamento", Now)
                cmdw.CommandText = MySql
                cmdw.ExecuteNonQuery()
            Loop
            myPOSTreader.Close()
        End If

        If Math.Abs(CDbl(Txt_ImportoAccrediti.Text)) > 0 Then

            Dim CondizioneAddebiti As String = ""

            If RB_FatturaRetta.Visible = True Then
                If RB_FatturaRetta.Checked = True Then
                    CondizioneAddebiti = " And ((Select FuoriRetta From TabTipoAddebito Where  TabTipoAddebito.Codice = ADDACR.CodiceIVA) =0 or (Select FuoriRetta From TabTipoAddebito Where  TabTipoAddebito.Codice = ADDACR.CodiceIVA) Is Null) "
                Else
                    CondizioneAddebiti = " And (Select FuoriRetta From TabTipoAddebito Where  TabTipoAddebito.Codice = ADDACR.CodiceIVA) =1 "
                End If
            End If

            If Request.Item("CodiceParente") = 99 Then
                MySql = "SelecT * FROM ADDACR WHERE  TIPOMOV = 'AC' AND RIFERIMENTO = 'O' And CENTROSERVIZIO = '" & Session("CODICESERVIZIO") & "' AND CODICEOSPITE  = " & Session("CODICEOSPITE") & " And (Year(DATA) = " & Txt_Anno.Text & " or Year(DATA) = " & Val(Txt_Anno.Text) - 1 & ") And (Elaborato = 0 or Elaborato Is Null) " & CondizioneAddebiti
            Else
                MySql = "SelecT * FROM ADDACR WHERE  TIPOMOV = 'AC' AND  RIFERIMENTO = 'P' AND PARENTE = " & Request.Item("CodiceParente") & " AND CENTROSERVIZIO = '" & Session("CODICESERVIZIO") & "' AND CODICEOSPITE  = " & Session("CODICEOSPITE") & " And (Year(DATA) = " & Txt_Anno.Text & " or Year(DATA) = " & Val(Txt_Anno.Text) - 1 & ") And (Elaborato = 0 or Elaborato Is Null) " & CondizioneAddebiti
            End If
            Dim cmd As New OleDbCommand()
            cmd.CommandText = MySql
            cmd.Connection = cn

            Dim myPOSTreader As OleDbDataReader = cmd.ExecuteReader()
            Do While myPOSTreader.Read


                Dim KTipoAddebito As New Cls_Addebito

                KTipoAddebito.Leggi(Session("DC_OSPITE"), campodb(myPOSTreader.Item("CODICEIVA")))

                If KTipoAddebito.CodiceIVA = "" Then
                    If Request.Item("CodiceParente") = 99 Then
                        KTipoAddebito.CodiceIVA = x.CODICEIVA
                    Else
                        KTipoAddebito.CodiceIVA = xPar.CODICEIVA
                    End If
                End If


                Dim kIVA As New Cls_IVA

                kIVA.Leggi(Session("DC_TABELLE"), KTipoAddebito.CodiceIVA)

                Riga = Riga + 1
                Registrazione.Righe(Riga) = New Cls_MovimentiContabiliRiga
                Registrazione.Righe(Riga).MastroPartita = CausaleContabile.Righe(1).Mastro
                Registrazione.Righe(Riga).ContoPartita = CausaleContabile.Righe(1).Conto
                Registrazione.Righe(Riga).SottocontoPartita = CausaleContabile.Righe(1).Sottoconto

                If TipoMovimento = "DOC" Then
                    Registrazione.Righe(Riga).DareAvere = IIf(CausaleContabile.Righe(1).DareAvere = "D", "A", "D")
                Else
                    Registrazione.Righe(Riga).DareAvere = IIf(CausaleContabile.Righe(1).DareAvere = "D", "A", "D")
                End If


                Registrazione.Righe(Riga).Segno = CausaleContabile.Righe(1).Segno
                Registrazione.Righe(Riga).Descrizione = ""
                Registrazione.Righe(Riga).Importo = Modulo.MathRound((campodbN(myPOSTreader.Item("IMPORTO")) * kIVA.Aliquota), 2)
                Registrazione.Righe(Riga).Imponibile = campodbN(myPOSTreader.Item("IMPORTO"))
                Registrazione.Righe(Riga).CodiceIVA = KTipoAddebito.CodiceIVA
                Registrazione.Righe(Riga).Tipo = "IV"
                Registrazione.Righe(Riga).RigaDaCausale = 2
                Registrazione.Righe(Riga).Utente = Session("UTENTE")

                Riga = Riga + 1
                Registrazione.Righe(Riga) = New Cls_MovimentiContabiliRiga
                Registrazione.Righe(Riga).MastroPartita = KTipoAddebito.Mastro
                Registrazione.Righe(Riga).ContoPartita = KTipoAddebito.Conto
                Registrazione.Righe(Riga).SottocontoPartita = KTipoAddebito.Sottoconto
                Registrazione.Righe(Riga).DareAvere = CausaleContabile.Righe(5).DareAvere
                Registrazione.Righe(Riga).Segno = CausaleContabile.Righe(5).Segno
                Registrazione.Righe(Riga).Importo = campodbN(myPOSTreader.Item("IMPORTO"))
                Registrazione.Righe(Riga).Imponibile = Modulo.MathRound((campodbN(myPOSTreader.Item("IMPORTO")) * kIVA.Aliquota) + campodbN(myPOSTreader.Item("IMPORTO")), 2)
                Registrazione.Righe(Riga).CodiceIVA = KTipoAddebito.CodiceIVA
                Registrazione.Righe(Riga).Descrizione = KTipoAddebito.Descrizione & " " & campodb(myPOSTreader.Item("DESCRIZIONE"))
                Registrazione.Righe(Riga).Tipo = ""
                Registrazione.Righe(Riga).TipoExtra = "A" & campodb(myPOSTreader.Item("CODICEIVA"))
                Registrazione.Righe(Riga).RigaDaCausale = 6
                Registrazione.Righe(Riga).Utente = Session("UTENTE")

                MySql = "UPDATE ADDACR SET Elaborato= 1,UTENTE = ?,DataAggiornamento =?   WHERE  CentroServizio = '" & Session("CODICESERVIZIO") & "' And  CodiceOspite = " & Session("CODICEOSPITE") & " And ID = " & campodbN(myPOSTreader.Item("ID"))
                Dim cmdw As New OleDbCommand()
                cmdw.Connection = cn
                cmdw.Parameters.AddWithValue("@UTENTE", Session("UTENTE"))
                cmdw.Parameters.AddWithValue("@DataAggiornamento", Now)
                cmdw.CommandText = MySql
                cmdw.ExecuteNonQuery()
            Loop
            myPOSTreader.Close()
        End If

        Dim RegistroIVA As New Cls_RegistroIVA

        RegistroIVA.RegistroCartaceo = 0
        RegistroIVA.Tipo = CausaleContabile.RegistroIVA
        RegistroIVA.Leggi(Session("DC_TABELLE"), RegistroIVA.Tipo)
        If (ImportoBollo > 0 Or TipoOperazione.BolloVirtuale = 1) And RegistroIVA.RegistroCartaceo = 0 Then
            Registrazione.BolloVirtuale = 1
        End If

        If ImportoBollo > 0 Then
            Riga = Riga + 1

            Registrazione.Righe(Riga) = New Cls_MovimentiContabiliRiga
            Registrazione.Righe(Riga).MastroPartita = CausaleContabile.Righe(1).Mastro
            Registrazione.Righe(Riga).ContoPartita = CausaleContabile.Righe(1).Conto
            Registrazione.Righe(Riga).SottocontoPartita = CausaleContabile.Righe(1).Sottoconto
            Registrazione.Righe(Riga).DareAvere = CausaleContabile.Righe(1).DareAvere
            Registrazione.Righe(Riga).Segno = CausaleContabile.Righe(1).Segno
            Registrazione.Righe(Riga).Descrizione = "Bollo"
            Registrazione.Righe(Riga).Importo = 0
            Registrazione.Righe(Riga).Imponibile = ImportoBollo
            Registrazione.Righe(Riga).CodiceIVA = CodiceBollo
            Registrazione.Righe(Riga).Tipo = "IV"
            Registrazione.Righe(Riga).RigaDaCausale = 2
            Registrazione.Righe(Riga).Utente = Session("UTENTE")


            Riga = Riga + 1
            Registrazione.Righe(Riga) = New Cls_MovimentiContabiliRiga
            Registrazione.Righe(Riga).MastroPartita = CausaleContabile.Righe(8).Mastro
            Registrazione.Righe(Riga).ContoPartita = CausaleContabile.Righe(8).Conto
            Registrazione.Righe(Riga).SottocontoPartita = CausaleContabile.Righe(8).Sottoconto
            Registrazione.Righe(Riga).DareAvere = CausaleContabile.Righe(8).DareAvere
            Registrazione.Righe(Riga).Segno = CausaleContabile.Righe(8).Segno
            Registrazione.Righe(Riga).Importo = ImportoBollo
            Registrazione.Righe(Riga).Imponibile = 0
            Registrazione.Righe(Riga).CodiceIVA = CodiceBollo
            Registrazione.Righe(Riga).Tipo = ""
            Registrazione.Righe(Riga).RigaDaCausale = 9
            Registrazione.Righe(Riga).Utente = Session("UTENTE")

        End If

        cn.Close()


        Dim VerificaValidita As String = Registrazione.VerificaValidita(Session("DC_GENERALE"), Session("DC_TABELLE"))

        If VerificaValidita <> "OK" Then
            ScriptManager.RegisterClientScriptBlock(Me, Me.GetType(), "Errore", "VisualizzaErrore('<center>" & VerificaValidita & "</center>');", True)
            Exit Sub
        End If


        If Registrazione.Scrivi(ConnectionStringGenerale, -1) = False Then
            ClientScript.RegisterClientScriptBlock(Me.GetType(), "Errore", "VisualizzaErrore('Errore nella registrazione');", True)
            Call EseguiJS()
            Exit Sub
        End If

        Dim M As New Cls_EmissioneRetta


        M.ConnessioneGenerale = Session("DC_GENERALE")
        M.ConnessioneOspiti = Session("DC_OSPITE")
        M.ConnessioneTabelle = Session("DC_TABELLE")
        M.ApriDB()

        M.VerificaRegistrazioneIVANew(Registrazione.NumeroRegistrazione, True)

        M.CloseDB()

        Registrazione.Leggi(M.ConnessioneGenerale, Registrazione.NumeroRegistrazione, False)
        OspizioMarino(Registrazione, M.ConnessioneOspiti, M.ConnessioneTabelle, M.ConnessioneGenerale)


        If Dd_CausaleContabile.SelectedValue <> "" Then
            CreatoIncasso(Registrazione.NumeroRegistrazione)
        End If


        If CentroServizio.TIPOCENTROSERVIZIO = "A" Then
            Dim OspitiCon As OleDbConnection

            OspitiCon = New Data.OleDb.OleDbConnection(Session("DC_OSPITE"))

            OspitiCon.Open()

            Dim Mese As Integer = Dd_Mese.SelectedValue
            Dim Anno As Integer = Txt_Anno.Text

            If Mese > Param.DocumentiIncassiMesiPrecedenti Then
                Mese = Mese - Param.DocumentiIncassiMesiPrecedenti
            Else
                Anno = Anno - 1
                Mese = 12 - (Mese - Param.DocumentiIncassiMesiPrecedenti)
            End If

            Dim Condizione As String = ""

            If RB_FatturaFuoriRetta.Visible = True Then
                If RB_FatturaRetta.Checked = True Then
                    Condizione = " And (Select count(*) From TipoDomiciliare_Addebiti Where TipoDomiciliare_Addebiti.CodiceTipoDomiciliare = MovimentiDomiciliare.Tipologia And (Select  FuoriRetta From TabTipoAddebito Where TabTipoAddebito.Codice  = TipoDomiciliare_Addebiti.TipoAddebito  ) = 0) > 0 "
                Else
                    Condizione = " And (Select count(*) From TipoDomiciliare_Addebiti Where TipoDomiciliare_Addebiti.CodiceTipoDomiciliare = MovimentiDomiciliare.Tipologia And (Select  FuoriRetta From TabTipoAddebito Where TabTipoAddebito.Codice  = TipoDomiciliare_Addebiti.TipoAddebito  ) = 1) > 0 "
                End If
            End If

            Dim cmdUpDom As New OleDbCommand()
            cmdUpDom.CommandText = "UPDATE MovimentiDomiciliare SET CreatoDocumentoOspiti = 1 Where CENTROSERVIZIO = '" & Session("CODICESERVIZIO") & "' And CODICEOSPITE = " & Session("CODICEOSPITE") & " And Data >= ? And Data <= ? " & Condizione
            cmdUpDom.Parameters.AddWithValue("@Data", DateSerial(Anno, Mese, 1))
            cmdUpDom.Parameters.AddWithValue("@Data", DateSerial(Txt_Anno.Text, Dd_Mese.SelectedValue, GiorniMese(Dd_Mese.SelectedValue, Txt_Anno.Text)))
            cmdUpDom.Connection = OspitiCon
            cmdUpDom.ExecuteNonQuery()



            OspitiCon.Close()
        End If

        Response.Redirect("ElencoRegistrazione.aspx?CodiceParente=" & Val(Request.Item("CodiceParente")))
        Call EseguiJS()

    End Sub

    Public Function GiorniMese(ByVal Mese As Byte, ByVal Anno As Integer) As Byte
        If Mese = 1 Or Mese = 3 Or Mese = 5 Or Mese = 7 Or Mese = 8 Or _
           Mese = 10 Or Mese = 12 Then
            GiorniMese = 31
        Else
            If Mese <> 2 Then
                GiorniMese = 30
            Else
                If Day(DateSerial(Anno, Mese, 29)) = 29 Then
                    GiorniMese = 29
                Else
                    GiorniMese = 28
                End If
            End If
        End If
    End Function


    Private Sub CreatoIncasso(ByVal NumeroDocumento As Long)
        Dim X As New Cls_MovimentoContabile
        Dim MyCau As New Cls_CausaleContabile

        X.Leggi(Session("DC_GENERALE"), 0)

        X.NumeroRegistrazione = 0
        X.DataRegistrazione = Txt_DataIncasso.Text
        X.DataBolletta = Nothing
        X.NumeroBolletta = ""
        X.Descrizione = Txt_Descrizione.Text
        X.CentroServizio = Session("CODICESERVIZIO")
        X.CausaleContabile = Dd_CausaleContabile.SelectedValue
        X.Utente = Session("UTENTE")
        MyCau.Leggi(Session("DC_TABELLE"), Dd_CausaleContabile.SelectedValue)

        Dim Mastro As Long
        Dim Conto As Long
        Dim Sottoconto As Long
        Dim XCs As New Cls_CentroServizio

        Dim KoSP As New Cls_Parametri

        KoSP.LeggiParametri(Session("DC_OSPITE"))

        XCs.Leggi(Session("DC_OSPITE"), Session("CODICESERVIZIO"))
        If Val(Request.Item("CodiceParente")) = 99 Then
            Mastro = XCs.MASTRO
            Conto = XCs.CONTO
            Sottoconto = Session("CODICEOSPITE") * 100
        Else
            Mastro = XCs.MASTRO
            Conto = XCs.CONTO
            Sottoconto = (Session("CODICEOSPITE") * 100) + Val(Request.Item("CodiceParente"))
        End If
        Dim Riga As Long

        Riga = 0

        If IsNothing(X.Righe(Riga)) Then
            X.Righe(Riga) = New Cls_MovimentiContabiliRiga
        End If


        X.Righe(0).MastroPartita = Mastro
        X.Righe(0).ContoPartita = Conto
        X.Righe(0).SottocontoPartita = Sottoconto
        X.Righe(0).Importo = Math.Abs(CDbl(Txt_Totale.Text))
        X.Righe(0).Descrizione = Txt_Descrizione.Text
        X.Righe(0).Segno = "+"
        X.Righe(0).Tipo = "CF"
        X.Righe(0).RigaDaCausale = 1
        X.Righe(0).DareAvere = MyCau.Righe(0).DareAvere
        X.Righe(Riga).Utente = Session("UTENTE")
        Riga = Riga + 1


        If IsNothing(X.Righe(Riga)) Then
            X.Righe(Riga) = New Cls_MovimentiContabiliRiga
        End If


        Dim X1 As New Cls_CausaleContabile

        X1.Leggi(Session("DC_TABELLE"), Dd_CausaleContabile.SelectedValue)

        Dim xS As New Cls_Pianodeiconti

        Mastro = X1.Righe(1).Mastro
        Conto = X1.Righe(1).Conto
        Sottoconto = X1.Righe(1).Sottoconto

        Dim DDModalita As New ClsModalitaPagamento

        If DD_ModalitaPagamento.SelectedValue <> "" Then
            DDModalita.Codice = DD_ModalitaPagamento.SelectedValue
            DDModalita.Leggi(Session("DC_OSPITE"))

            Mastro = DDModalita.MASTRO
            Conto = DDModalita.CONTO
            Sottoconto = DDModalita.SOTTOCONTO
        End If

        X.Righe(Riga).MastroPartita = Mastro
        X.Righe(Riga).ContoPartita = Conto
        X.Righe(Riga).SottocontoPartita = Sottoconto
        X.Righe(Riga).Importo = Math.Abs(CDbl(Txt_Totale.Text))
        X.Righe(Riga).Descrizione = Txt_Descrizione.Text
        X.Righe(Riga).Segno = "+"
        X.Righe(Riga).Tipo = ""
        X.Righe(Riga).RigaDaCausale = 2
        X.Righe(Riga).DareAvere = MyCau.Righe(1).DareAvere
        X.Righe(Riga).Utente = Session("UTENTE")
        Riga = Riga + 1

        If X.Scrivi(Session("DC_GENERALE"), 0) = False Then
            ClientScript.RegisterClientScriptBlock(Me.GetType(), "Errore", "VisualizzaErrore('Errore nella registrazione');", True)
            Exit Sub
        End If

        Dim Legami As New Cls_Legami
        Legami.Importo(0) = Math.Abs(CDbl(Txt_Totale.Text)) ' Txt_Importo.Text
        Legami.NumeroDocumento(0) = NumeroDocumento
        Legami.NumeroPagamento(0) = X.NumeroRegistrazione
        Legami.Scrivi(Session("DC_GENERALE"), 0, X.NumeroRegistrazione)
    End Sub


    Protected Sub Btn_Modifica_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles Btn_Modifica.Click
        If Chk_Conferma.Visible = True And Chk_Conferma.Checked = False Then
            ClientScript.RegisterClientScriptBlock(Me.GetType(), "Errore", "VisualizzaErrore('Devi mettere il check per proseguire nella creazione del documento');", True)
            Call EseguiJS()
            Exit Sub
        End If


        Btn_Modifica.Enabled = False

        Call CalcolaTotale()



        If CDbl(Txt_Totale.Text) > 0 Then
            Call CreaDocumento("DOC")
        Else

            Call CreaDocumento("NC")
        End If
        Btn_Modifica.Enabled = True
    End Sub

    Protected Sub btn_calcola_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btn_calcola.Click
        Call CalcolaTotale()
    End Sub

    Protected Sub btn_Calcolo_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btn_Calcolo.Click

        Txt_ImportoRettaPresenza.Text = "0"
        Txt_ImportoRettaPresenza2.Text = "0"
        Txt_GiorniPresenza.Text = "0"
        Txt_ImportoRettaAssenza.Text = "0"
        Txt_GiorniAssenza.Text = "0"
        Txt_ExtraFissi.Text = "0"
        Txt_ImportoAddibiti.Text = "0"
        Txt_ImportoAccrediti.Text = "0"
        Dim Param As New Cls_Parametri

        Param.LeggiParametri(Session("DC_OSPITE"))

        Dim XOspite As New ClsOspite

        Dim ConnectionString As String = Session("DC_OSPITE")
        

        XOspite.Leggi(ConnectionString, Session("CODICEOSPITE"))

        Dim JK As New Cls_DatiOspiteParenteCentroServizio

        JK.CodiceOspite = XOspite.CodiceOspite
        JK.CodiceParente = 0
        JK.CentroServizio = Session("CODICESERVIZIO")
        JK.Leggi(ConnectionString)

        If JK.CodiceOspite <> 0 Then
            XOspite.TIPOOPERAZIONE = JK.TipoOperazione
        End If


        If Param.DocumentiIncassiMesiPrecedenti = 0 Then

            Call Calcolo(Dd_Mese.SelectedValue, Txt_Anno.Text) 
        Else
            Txt_ImportoRettaPresenza.Text = CDbl(Txt_ImportoRettaPresenza.Text) + CDbl(Txt_ImportoRettaAssenza.Text)
            Txt_GiorniPresenza.Text = Int(Txt_GiorniPresenza.Text) + Int(Txt_GiorniAssenza.Text)

            Dim I As Integer
            Dim Mese As Integer = Dd_Mese.SelectedValue
            Dim Anno As Integer = Txt_Anno.Text
            'Call Calcolo(Dd_Mese.SelectedValue, Txt_Anno.Text)

            For I = 1 To Param.DocumentiIncassiMesiPrecedenti
                If Mese = 1 Then
                    Mese = 12
                    Anno = Anno - 1
                Else
                    Mese = Mese - 1
                End If
            Next

            For I = 1 To Param.DocumentiIncassiMesiPrecedenti
                If Mese = 12 Then
                    Mese = 1
                    Anno = Anno + 1
                Else
                    Mese = Mese + 1
                End If
                Call Calcolo(Mese, Anno)
            Next
            Call ConguagliaFatturato()


            If Param.DocumentiIncassiNonPeriodo = 1 Then
                Lbl_Anno.Visible = False
                Lbl_Mese.Visible = False
                Txt_Anno.Visible = False
                Dd_Mese.Visible = False
                btn_Calcolo.Visible = False
            End If

            Chk_Conferma.Checked = True
            Chk_Conferma.Visible = False
            lbl_Errore.Visible = False
            Lbl_ImportoRetteAssenza.Visible = False
            Lbl_GiorniAss.Visible = False
            Call btn_calcola_Click(sender, e)

            Txt_ImportoRettaAssenza.Visible = False
            Txt_GiorniAssenza.Visible = False

        End If

    End Sub


    Private Sub ConguagliaFatturato()
        Dim Param As New Cls_Parametri
        Dim SottoConto As Double
        Dim Emis As New Cls_EmissioneRetta
        Param.LeggiParametri(Session("DC_OSPITE"))
        Dim I As Integer
        Dim Mese As Integer = Dd_Mese.SelectedValue
        Dim Anno As Integer = Txt_Anno.Text
        Dim TotaleFatturato As Double = 0
        Dim TotaleFatturatoRetta2 As Double = 0
        Dim Giorni As Integer = 0

        Dim cn As OleDbConnection


        Dim MySql As String

        Dim Modifica As Boolean = False

        cn = New Data.OleDb.OleDbConnection(Session("DC_GENERALE"))

        cn.Open()



        Dim CentroServizio As New Cls_CentroServizio
        Dim Tipologia As String = "O"
        Dim NumeroRegistrazione As Long
        Dim Riga As Integer
        Dim TipOpe As New Cls_TipoOperazione


        CentroServizio.CENTROSERVIZIO = Session("CODICESERVIZIO")
        CentroServizio.Leggi(Session("DC_OSPITE"), CentroServizio.CENTROSERVIZIO)
        SottoConto = Session("CODICEOSPITE") * 100

        Dim Ospite As New ClsOspite

        Ospite.CodiceOspite = Session("CODICEOSPITE")
        Ospite.Leggi(Session("DC_OSPITE"), Ospite.CodiceOspite)

        TipOpe.Codice = Ospite.TIPOOPERAZIONE

        If Val(Request.Item("CodiceParente")) <> 99 Then
            SottoConto = SottoConto + Val(Request.Item("CodiceParente"))
            Tipologia = "P"

            Dim PRT As New Cls_Parenti

            PRT.CodiceOspite = Session("CODICEOSPITE")
            PRT.CodiceParente = Val(Request.Item("CodiceParente"))
            PRT.Leggi(Session("DC_OSPITE"), Ospite.CodiceOspite, Val(Request.Item("CodiceParente")))
            TipOpe.Codice = Ospite.TIPOOPERAZIONE
        End If

        TipOpe.Leggi(Session("DC_OSPITE"), TipOpe.Codice)



        MySql = "SELECT MovimentiContabiliTesta.FatturaDiAnticipo,NumeroRegistrazione " & _
                                " FROM MovimentiContabiliRiga INNER JOIN MovimentiContabiliTesta " & _
                                " ON MovimentiContabiliRiga.Numero = MovimentiContabiliTesta.NumeroRegistrazione " & _
                                " WHERE MovimentiContabiliRiga.MastroPartita = " & CentroServizio.MASTRO & _
                                " AND MovimentiContabiliRiga.ContoPartita = " & CentroServizio.CONTO & _
                                " AND MovimentiContabiliRiga.SottocontoPartita = " & SottoConto & _
                                " And MovimentiContabiliTesta.CentroServizio = '" & CentroServizio.CENTROSERVIZIO & "'" & _
                                " AND MovimentiContabiliTesta.AnnoCompetenza = " & Anno & _
                                " AND MovimentiContabiliTesta.MeseCompetenza = " & Mese
        
        Dim cmd As New OleDbCommand()


        cmd.CommandText = MySql
        cmd.Connection = cn

        Dim RdFatture As OleDbDataReader = cmd.ExecuteReader()


        Do While RdFatture.Read
            If campodb(RdFatture.Item("FatturaDiAnticipo")) = "" Then
                Dim cmdRd As New OleDbCommand()


                cmdRd.CommandText = "Select * From MovimentiContabiliRiga Where Numero = " & campodbN(RdFatture.Item("NumeroRegistrazione")) & " And AnnoRiferimento =  " & 0 & " And MeseRiferimento = " & 0
                cmdRd.Connection = cn
                Dim RdFatRiga As OleDbDataReader = cmdRd.ExecuteReader()
                If RdFatRiga.Read Then
                    Dim RegistrazioneContabile As New Cls_MovimentoContabile



                    RegistrazioneContabile.Leggi(Session("DC_GENERALE"), campodbN(RdFatRiga.Item("Numero")), False)

                    For Riga = 0 To RegistrazioneContabile.Righe.Length - 1
                        If Not IsNothing(RegistrazioneContabile.Righe(Riga)) Then
                            If RegistrazioneContabile.Righe(Riga).Tipo = "" Then
                                If RegistrazioneContabile.Righe(Riga).RigaDaCausale = 3 Then
                                    TotaleFatturato = TotaleFatturato + RegistrazioneContabile.Righe(Riga).Importo
                                    Giorni = Giorni + RegistrazioneContabile.Righe(Riga).Quantita
                                End If

                                If RegistrazioneContabile.Righe(Riga).RigaDaCausale = 6 And RegistrazioneContabile.Righe(Riga).TipoExtra = "A" & TipOpe.TipoAddebitoAccredito Then
                                    TotaleFatturato = TotaleFatturato - RegistrazioneContabile.Righe(Riga).Importo
                                End If
                                If RegistrazioneContabile.Righe(Riga).RigaDaCausale = 13 Then
                                    TotaleFatturatoRetta2= TotaleFatturatoRetta2 + RegistrazioneContabile.Righe(Riga).Importo                                    
                                End If
                            End If
                        End If
                    Next
                End If
                RdFatRiga.Close()                
            End If            
        Loop
        RdFatture.Close()


        

        For I = 1 To Param.DocumentiIncassiMesiPrecedenti
            If Mese = 1 Then
                Mese = 12
                Anno = Anno - 1
            Else
                Mese = Mese - 1
            End If
            
            
            MySql = "SELECT MovimentiContabiliTesta.FatturaDiAnticipo,NumeroRegistrazione " & _
                                " FROM MovimentiContabiliRiga INNER JOIN MovimentiContabiliTesta " & _
                                " ON MovimentiContabiliRiga.Numero = MovimentiContabiliTesta.NumeroRegistrazione " & _
                                " WHERE MovimentiContabiliRiga.MastroPartita = " & CentroServizio.MASTRO & _
                                " AND MovimentiContabiliRiga.ContoPartita = " & CentroServizio.CONTO & _
                                " AND MovimentiContabiliRiga.SottocontoPartita = " & SottoConto & _
                                " And MovimentiContabiliTesta.CentroServizio = '" & CentroServizio.CENTROSERVIZIO & "'" & _
                                " AND MovimentiContabiliTesta.AnnoCompetenza = " & Anno & _
                                " AND MovimentiContabiliTesta.MeseCompetenza = " & Mese

            Dim cmd1 As New OleDbCommand()


            cmd1.CommandText = MySql
            cmd1.Connection = cn

            Dim RdFatture2 As OleDbDataReader = cmd1.ExecuteReader()


            Do While RdFatture2.Read
                If campodb(RdFatture2.Item("FatturaDiAnticipo")) = "" Then
                    Dim cmdRd As New OleDbCommand()


                    cmdRd.CommandText = "Select * From MovimentiContabiliRiga Where Numero = " & campodbN(RdFatture2.Item("NumeroRegistrazione")) & " And AnnoRiferimento = 0 And MeseRiferimento = 0"
                    cmdRd.Connection = cn
                    Dim RdFatRiga As OleDbDataReader = cmdRd.ExecuteReader()
                    If RdFatRiga.Read Then
                        Dim RegistrazioneContabile As New Cls_MovimentoContabile



                        RegistrazioneContabile.Leggi(Session("DC_GENERALE"), campodbN(RdFatRiga.Item("Numero")), False)

                        For Riga = 0 To RegistrazioneContabile.Righe.Length - 1
                            If Not IsNothing(RegistrazioneContabile.Righe(Riga)) Then
                                If RegistrazioneContabile.Righe(Riga).Tipo = "" Then
                                    If RegistrazioneContabile.Righe(Riga).RigaDaCausale = 3 Then
                                        TotaleFatturato = TotaleFatturato + RegistrazioneContabile.Righe(Riga).Importo
                                        Giorni = Giorni + RegistrazioneContabile.Righe(Riga).Quantita
                                    End If

                                    If RegistrazioneContabile.Righe(Riga).RigaDaCausale = 6 And RegistrazioneContabile.Righe(Riga).TipoExtra = "A" & TipOpe.TipoAddebitoAccredito Then
                                        TotaleFatturato = TotaleFatturato - RegistrazioneContabile.Righe(Riga).Importo
                                    End If

                                    If RegistrazioneContabile.Righe(Riga).RigaDaCausale = 13 Then
                                        TotaleFatturatoRetta2= TotaleFatturatoRetta2 +  RegistrazioneContabile.Righe(Riga).Importo                                        
                                    End If
                                End If
                            End If
                        Next
                    End If
                    RdFatRiga.Close()
                End If
            Loop
            RdFatture.Close()


        Next


        cn.Close()

        Txt_TotaleFatturato.Text = Format(TotaleFatturato + TotaleFatturatoRetta2, "#,##0.00")

        Txt_ImportoRettaPresenzaCalcolato.Text = Format(CDbl(Txt_ImportoRettaPresenza.Text) + CDbl(Txt_ImportoRettaPresenza2.Text) , "#,##0.00")

        Txt_ImportoRettaPresenza.Text = Format(CDbl(Txt_ImportoRettaPresenza.Text) - TotaleFatturato, "#,##0.00")
        Txt_ImportoRettaPresenza2.Text = Format(CDbl(Txt_ImportoRettaPresenza2.Text) - TotaleFatturatoRetta2, "#,##0.00")

        Txt_GiorniCalcolato.Text = Txt_GiorniPresenza.Text

        Txt_GiorniFattura.Text = Giorni

        Txt_GiorniPresenza.Text = Val(Txt_GiorniPresenza.Text) - Giorni

    End Sub

    Protected Sub Btn_Esci_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles Btn_Esci.Click

        Response.Redirect("ElencoRegistrazione.aspx?CodiceParente=" & Val(Request.Item("CodiceParente")))
    End Sub

    Protected Sub Txt_Descrizione_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Txt_Descrizione.Load

    End Sub

    Protected Sub Chk_SoloMeseCorrente_CheckedChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles Chk_SoloMeseCorrente.CheckedChanged

        btn_Calcolo_Click(sender, e)
    End Sub


    Private Sub OspizioMarino(ByVal Documento As Cls_MovimentoContabile, ByVal ConnessioneOspite As String, ByVal ConnessioneTabelle As String, ByVal ConnessioneGenerale As String)
        Dim CodiceOspite As Integer = 0
        If Documento.Tipologia = "" Then
            If Documento.CentroServizio = "RIC" Then
                Dim Imponibile As Double = 0
                Dim Aliquota As String = ""
                Dim Listino As New Cls_Listino

                For Riga = 0 To 300
                    If Not IsNothing(Documento.Righe(Riga)) Then
                        If Documento.Righe(Riga).RigaDaCausale = 3 And Documento.Righe(Riga).Descrizione.ToUpper.IndexOf("SCONTO") < 0 Then
                            Documento.Righe(Riga).Descrizione = "Ricovero - Tariffa per assistenza medica infermieristica"
                            Imponibile = Imponibile + Documento.Righe(Riga).Importo
                            Aliquota = Documento.Righe(Riga).CodiceIVA
                            If Listino.CodiceListino = "11" Or Listino.CodiceListino = "03" Then
                                Documento.Righe(Riga).Descrizione = "RICOVERO PRIVATO – ACCOMPAGNATORI"
                            End If
                        End If
                        If Documento.Righe(Riga).RigaDaCausale = 13 Then
                            Documento.Righe(Riga).Descrizione = "Ricovero - Tariffa per spese generali e vitto"
                        End If

                        If Documento.Righe(Riga).RigaDaCausale = 5 And Documento.Righe(Riga).TipoExtra = "A01" Then
                            Documento.Righe(Riga).Descrizione = "Ricovero - Tariffa per assistenza medica infermieristica"
                            Imponibile = Imponibile + Documento.Righe(Riga).Importo
                            Aliquota = Documento.Righe(Riga).CodiceIVA
                        End If
                        If Documento.Righe(Riga).RigaDaCausale = 5 And Documento.Righe(Riga).TipoExtra = "A05" Then
                            Documento.Righe(Riga).Descrizione = "Ricovero - Tariffa per spese generali e vitto"
                        End If

                        If Documento.Righe(Riga).RigaDaCausale = 1 Then
                            CodiceOspite = Int(Documento.Righe(Riga).SottocontoPartita / 100)

                            Listino.CODICEOSPITE = CodiceOspite
                            Listino.CENTROSERVIZIO = Documento.CentroServizio
                            Listino.LeggiAData(ConnessioneOspite, Documento.DataRegistrazione)

                        End If
                    End If
                Next

                Dim DecIVa As New Cls_IVA
                DecIVa.Codice = Aliquota
                DecIVa.Leggi(ConnessioneTabelle, Aliquota)

                Dim Sconto As Double = 0

                If CodiceOspite > 0 Then
                    Dim Ospite As New ClsOspite

                    Ospite.CodiceOspite = CodiceOspite
                    Ospite.Leggi(ConnessioneOspite, CodiceOspite)

                    If Ospite.ImportoSconto > 0 Then
                        Dim DatiSconto As New Cls_Sconto

                        DatiSconto.Codice = Ospite.TipoSconto
                        DatiSconto.Leggi(ConnessioneOspite, DatiSconto.Codice)

                        If DatiSconto.Tipologia = "" Then
                            Sconto = Modulo.MathRound((Imponibile + (Imponibile * DecIVa.Aliquota)) * Ospite.ImportoSconto / 100, 2)

                        End If

                    End If
                End If


                Dim Detraibilita As String = ""
                Detraibilita= "Quota detraibile " & Format(Imponibile + (Imponibile * DecIVa.Aliquota) - Sconto, "#,##0.00")

                If CodiceOspite > 0 Then

                    Listino.CODICEOSPITE = CodiceOspite
                    Listino.CENTROSERVIZIO = Documento.CentroServizio
                    Listino.LeggiAData(ConnessioneOspite, Documento.DataRegistrazione)
                    If Listino.CodiceListino = "05" Then
                       Detraibilita = "RSA BASE 50% - " & Documento.Descrizione
                    End If
                    If Listino.CodiceListino = "03" Or Listino.CodiceListino = "11" Then
                        Detraibilita  = ""
                    End If
                End If

                Documento.Descrizione = Detraibilita & " " & Documento.Descrizione
            End If
            If Documento.CentroServizio = "RIC" Then


            End If
        Else
            For Riga = 0 To 300
                If Not IsNothing(Documento.Righe(Riga)) Then
                    If Documento.Righe(Riga).RigaDaCausale = 3 Then

                        Dim ContoControPartita As New Cls_Pianodeiconti

                        ContoControPartita.Mastro = Documento.Righe(Riga).MastroContropartita
                        ContoControPartita.Conto = Documento.Righe(Riga).ContoContropartita
                        ContoControPartita.Sottoconto = Documento.Righe(Riga).SottocontoContropartita
                        ContoControPartita.Decodfica(ConnessioneGenerale)

                        Dim Conto As New Cls_Pianodeiconti

                        Conto.Mastro = Documento.Righe(Riga).MastroPartita
                        Conto.Conto = Documento.Righe(Riga).ContoPartita
                        Conto.Sottoconto = Documento.Righe(Riga).SottocontoPartita
                        Conto.Decodfica(ConnessioneGenerale)


                        Documento.Righe(Riga).Descrizione = Conto.Descrizione & " " & ContoControPartita.Descrizione & " " & Documento.Righe(Riga).Descrizione
                    End If
                End If
            Next
        End If
        Documento.Scrivi(ConnessioneGenerale, Documento.NumeroRegistrazione, False)
    End Sub
End Class

