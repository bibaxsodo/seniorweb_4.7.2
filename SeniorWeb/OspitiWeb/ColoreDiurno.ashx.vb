﻿Imports System.Data.OleDb
Imports System.Web.Script.Serialization

Public Class ColoreDiurno
    Implements System.Web.IHttpHandler, IRequiresSessionState

    Public Sub ProcessRequest(ByVal context As HttpContext) Implements IHttpHandler.ProcessRequest
        context.Response.ContentType = "text/plain"
        Dim CSERV As String = context.Request.QueryString("CSERV")
        Dim CODOSP As String = context.Request.QueryString("CODOSP")

        Dim MESE As String = context.Request.QueryString("MESE")
        Dim GIORNO As String = context.Request.QueryString("GIORNO")

        Dim ANNO As String = context.Request.QueryString("ANNO")


        context.Response.Cache.SetCacheability(HttpCacheability.NoCache)
        Dim DbC As New Cls_Login


        If Trim(context.Session("UTENTE")) = "" Then
            Exit Sub
        End If


        DbC.Utente = context.Session("UTENTE")
        DbC.LeggiSP(context.Application("SENIOR"))

        Dim k As New CellaGiorno

        Dim XsDiurno As New Cls_Diurno


        XsDiurno.Leggi(DbC.Ospiti, CODOSP, CSERV, ANNO, MESE)
        If XsDiurno.Anno = 0 Then
            Dim TbCServ As New Cls_CentroServizio
            Dim SETTIMANA As String

            Dim Gg As Integer
            TbCServ.Leggi(DbC.Ospiti, CSERV)

            Dim AnaOsp As New ClsOspite

            AnaOsp.Leggi(DbC.Ospiti, CODOSP)


            SETTIMANA = AnaOsp.SETTIMANA

            Dim m As New Cls_DatiOspiteParenteCentroServizio

            m.CodiceOspite = CODOSP
            m.CodiceParente = 0
            m.CentroServizio = CSERV
            m.Leggi(DbC.Ospiti)
            If Trim(m.Settimana) <> "" Then
                SETTIMANA = m.Settimana
            End If


            Gg = Weekday(DateSerial(ANNO, MESE, GIORNO), vbMonday)
            If Trim(Mid(SETTIMANA, Gg, 1)) <> "" Then
                k.Causale = Mid(SETTIMANA, Gg, 1)
            End If


            SETTIMANA = TbCServ.SETTIMANA

            Gg = Weekday(DateSerial(ANNO, MESE, GIORNO), vbMonday)
            If Trim(Mid(SETTIMANA, Gg, 1)) <> "" Then
                k.Causale = "C"

            End If


            If MESE = 1 Then
                If TbCServ.GIORNO0101 <> "S" And GIORNO = 1 Then
                    k.Causale = "C"
                End If
                If TbCServ.GIORNO0601 = "S" And GIORNO = 6 Then
                    k.Causale = "C"
                End If
            End If
            If MESE = 3 Then
                If TbCServ.GIORNO1903 <> "S" And GIORNO = 19 Then
                    k.Causale = "C"
                End If
            End If
            If MESE = 4 Then
                If TbCServ.GIORNO2504 <> "S" And GIORNO = 25 Then
                    k.Causale = "C"
                End If
            End If
            If MESE = 5 Then
                If TbCServ.GIORNO0105 <> "S" And GIORNO = 1 Then
                    k.Causale = "C"
                End If
            End If

            If MESE = 6 Then
                If TbCServ.GIORNO0206 <> "S" And GIORNO = 2 Then
                    k.Causale = "C"
                End If
                If TbCServ.GIORNO2906 <> "S" And GIORNO = 29 Then
                    k.Causale = "C"
                End If
            End If
            If MESE = 8 Then
                If TbCServ.GIORNO1508 <> "S" And GIORNO = 29 Then
                    k.Causale = "C"
                End If
            End If
            If MESE = 11 Then
                If TbCServ.GIORNO0111 <> "S" And GIORNO = 1 Then
                    k.Causale = "C"
                End If
                If TbCServ.GIORNO0411 <> "S" And GIORNO = 4 Then
                    k.Causale = "C"
                End If
            End If
            If MESE = 12 Then
                If TbCServ.GIORNO0812 <> "S" And GIORNO = 8 Then
                    k.Causale = "C"
                End If
                If TbCServ.GIORNO2512 <> "S" And GIORNO = 25 Then
                    k.Causale = "C"
                End If
                If TbCServ.GIORNO2612 <> "S" And GIORNO = 26 Then
                    k.Causale = "C"
                End If
            End If
            If TbCServ.GIORNO1ATTIVO <> "S" Then
                If Not IsDBNull(TbCServ.GIORNO1) Then
                    If Val(Mid(TbCServ.GIORNO1, 4, 2)) = MESE Then
                        If Val(Mid(TbCServ.GIORNO1, 1, 2)) = GIORNO Then
                            k.Causale = "C"
                        End If
                    End If
                End If

                If TbCServ.GIORNO2ATTIVO <> "S" Then
                    If Not IsDBNull(TbCServ.GIORNO2) Then
                        If Val(Mid(TbCServ.GIORNO2, 4, 2)) = MESE Then
                            If Val(Mid(TbCServ.GIORNO2, 1, 2)) = GIORNO Then
                                k.Causale = "C"
                            End If
                        End If
                    End If
                End If


                If TbCServ.GIORNO3ATTIVO <> "S" Then
                    If Not IsDBNull(TbCServ.GIORNO3) Then
                        If Val(Mid(TbCServ.GIORNO3, 4, 2)) = MESE Then
                            If Val(Mid(TbCServ.GIORNO3, 1, 2)) = GIORNO Then
                                k.Causale = "C"
                            End If
                        End If
                    End If
                End If

                If TbCServ.GIORNO4ATTIVO <> "S" Then
                    If Not IsDBNull(TbCServ.GIORNO4) Then
                        If Val(Mid(TbCServ.GIORNO4, 4, 2)) = MESE Then
                            If Val(Mid(TbCServ.GIORNO4, 1, 2)) = GIORNO Then
                                k.Causale = "C"
                            End If
                        End If
                    End If
                End If

                If TbCServ.GIORNO5ATTIVO <> "S" Then
                    If Not IsDBNull(TbCServ.GIORNO5) Then
                        If Val(Mid(TbCServ.GIORNO5, 4, 2)) = MESE Then
                            If Val(Mid(TbCServ.GIORNO5, 1, 2)) = GIORNO Then
                                k.Causale = "C"
                            End If
                        End If
                    End If
                End If
            End If

        Else
            If GIORNO = 1 Then k.Causale = XsDiurno.Giorno1
            If GIORNO = 2 Then k.Causale = XsDiurno.Giorno2
            If GIORNO = 3 Then k.Causale = XsDiurno.Giorno3
            If GIORNO = 4 Then k.Causale = XsDiurno.Giorno4
            If GIORNO = 5 Then k.Causale = XsDiurno.Giorno5
            If GIORNO = 6 Then k.Causale = XsDiurno.Giorno6
            If GIORNO = 7 Then k.Causale = XsDiurno.Giorno7
            If GIORNO = 8 Then k.Causale = XsDiurno.Giorno8
            If GIORNO = 9 Then k.Causale = XsDiurno.Giorno9
            If GIORNO = 10 Then k.Causale = XsDiurno.Giorno10
            If GIORNO = 11 Then k.Causale = XsDiurno.Giorno11
            If GIORNO = 12 Then k.Causale = XsDiurno.Giorno12
            If GIORNO = 13 Then k.Causale = XsDiurno.Giorno13
            If GIORNO = 14 Then k.Causale = XsDiurno.Giorno14
            If GIORNO = 15 Then k.Causale = XsDiurno.Giorno15
            If GIORNO = 16 Then k.Causale = XsDiurno.Giorno16
            If GIORNO = 17 Then k.Causale = XsDiurno.Giorno17
            If GIORNO = 18 Then k.Causale = XsDiurno.Giorno18
            If GIORNO = 19 Then k.Causale = XsDiurno.Giorno19
            If GIORNO = 20 Then k.Causale = XsDiurno.Giorno20
            If GIORNO = 21 Then k.Causale = XsDiurno.Giorno21
            If GIORNO = 22 Then k.Causale = XsDiurno.Giorno22
            If GIORNO = 23 Then k.Causale = XsDiurno.Giorno23
            If GIORNO = 24 Then k.Causale = XsDiurno.Giorno24
            If GIORNO = 25 Then k.Causale = XsDiurno.Giorno25
            If GIORNO = 26 Then k.Causale = XsDiurno.Giorno26
            If GIORNO = 27 Then k.Causale = XsDiurno.Giorno27
            If GIORNO = 28 Then k.Causale = XsDiurno.Giorno28
            If GIORNO = 29 Then k.Causale = XsDiurno.Giorno29
            If GIORNO = 30 Then k.Causale = XsDiurno.Giorno30
            If GIORNO = 31 Then k.Causale = XsDiurno.Giorno31
        End If





        Dim cn As OleDbConnection



        cn = New Data.OleDb.OleDbConnection(DbC.Ospiti)

        cn.Open()

        Dim KCausale As New Cls_CausaliEntrataUscita
        Dim MovimentiTipo As String = ""
        Dim cmd As New OleDbCommand()
        cmd.CommandText = ("select * from MOVIMENTI where CodiceOspite = " & CODOSP & " And CENTROSERVIZIO = '" & CSERV & "' And Data = ? And (Causale <> '' and Not Causale is null) ")
        cmd.Parameters.AddWithValue("@Data", DateSerial(ANNO, MESE, GIORNO))
        cmd.Connection = cn

        Dim myPOSTreader As OleDbDataReader = cmd.ExecuteReader()
        If myPOSTreader.Read Then
            k.Causale = campodb(myPOSTreader.Item("Causale"))
            If campodb(myPOSTreader.Item("TipoMov")) = "13" Then
                MovimentiTipo = "- Uscita Definitiva"
            End If
            If campodb(myPOSTreader.Item("TipoMov")) = "05" Then
                MovimentiTipo = "- Accoglimento"
            End If
        End If
        myPOSTreader.Close()
        cn.Close()

        If k.Causale <> "" Then
            KCausale.Codice = k.Causale
            KCausale.LeggiCausale(DbC.Ospiti)

            k.Colore = KCausale.Diurno
            If MovimentiTipo = "" Then
                k.Descrizione = KCausale.Descrizione
            Else
                k.Descrizione = KCausale.Descrizione & " " & MovimentiTipo
            End If


        Else
            k.Colore = 0
        End If



        Dim serializer As JavaScriptSerializer
        serializer = New JavaScriptSerializer()

        Dim Serializzazione As String

        Serializzazione = serializer.Serialize(k)

        context.Response.Write(Serializzazione)
    End Sub

    Public Class CellaGiorno
        Public Causale As String
        Public Colore As Long
        Public Descrizione As String
    End Class

    Function campodb(ByVal oggetto As Object) As String
        If IsDBNull(oggetto) Then
            Return ""
        Else
            Return oggetto
        End If
    End Function

    Public ReadOnly Property IsReusable() As Boolean Implements IHttpHandler.IsReusable
        Get
            Return False
        End Get
    End Property

    Public Function GiorniMese(ByVal Mese As Byte, ByVal Anno As Integer) As Byte
        If Mese = 1 Or Mese = 3 Or Mese = 5 Or Mese = 7 Or Mese = 8 Or
           Mese = 10 Or Mese = 12 Then
            GiorniMese = 31
        Else
            If Mese <> 2 Then
                GiorniMese = 30
            Else
                If Day(DateSerial(Anno, Mese, 29)) = 29 Then
                    GiorniMese = 29
                Else
                    GiorniMese = 28
                End If
            End If
        End If
    End Function
End Class

