﻿Imports Microsoft.VisualBasic
Imports System.Data.OleDb
Imports System.Web.Hosting
Imports System.Data.SqlTypes

Partial Class OspitiWeb_ElencoExtraFissi
    Inherits System.Web.UI.Page


    Dim Tabella As New System.Data.DataTable("tabella")


    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Session("UTENTE") = "" Then
            Response.Redirect("..\Login.aspx")
            Exit Sub
        End If

        If Page.IsPostBack = True Then
            Exit Sub
        End If

        Dim K1 As New Cls_SqlString

        Dim Barra As New Cls_BarraSenior

        If Not IsNothing(Session("RicercaGeneraleSQLString")) Then
            K1 = Session("RicercaGeneraleSQLString")
        End If

        Lbl_BarraSenior.Text = Barra.CodiceBarra(Application("SENIOR"), Session("UTENTE"), Page, K1)


        Dim cn As New OleDbConnection


        Dim ConnectionString As String = Session("DC_OSPITE")

        cn = New Data.OleDb.OleDbConnection(ConnectionString)

        cn.Open()

        Dim cmd As New OleDbCommand

        cmd.CommandText = ("select top 50  * from TABELLAEXTRAFISSI  " & _
                               "  Order By Descrizione,DescrizioneInterna")
        cmd.Connection = cn



        Tabella.Clear()
        Tabella.Columns.Add("Codice", GetType(String))
        Tabella.Columns.Add("Struttura/Servizio", GetType(String))
        Tabella.Columns.Add("Descrizione", GetType(String))
        Tabella.Columns.Add("Descrizione Interna", GetType(String))

        Tabella.Columns.Add("Ripartizione", GetType(String))
        Tabella.Columns.Add("Importo", GetType(String))
        Tabella.Columns.Add("Scadenza", GetType(String))

        Dim myPOSTreader As OleDbDataReader = cmd.ExecuteReader()
        Do While myPOSTreader.Read
            Dim myriga As System.Data.DataRow = Tabella.NewRow()

            myriga(0) = campodb(myPOSTreader.Item("CODICEEXTRA"))

            Dim Cserv As New Cls_CentroServizio

            Cserv.CENTROSERVIZIO = campodb(myPOSTreader.Item("CENTROSERVIZIO"))
            Cserv.Leggi(Session("DC_OSPITE"), Cserv.CENTROSERVIZIO)


            Dim kVilla As New Cls_TabelleDescrittiveOspitiAccessori

            kVilla.TipoTabella = "VIL"
            kVilla.CodiceTabella = campodb(myPOSTreader.Item("struttura"))
            kVilla.Leggi(Session("DC_OSPITIACCESSORI"))

            If kVilla.Descrizione <> "" Then
                myriga(1) = kVilla.Descrizione
                If Cserv.DESCRIZIONE <> "" Then
                    myriga(1) = kVilla.Descrizione & " - " & Cserv.DESCRIZIONE
                End If
            Else
                myriga(1) = Cserv.DESCRIZIONE
            End If


            myriga(2) = campodb(myPOSTreader.Item("Descrizione"))
            myriga(3) = campodb(myPOSTreader.Item("DescrizioneInterna"))

            If campodb(myPOSTreader.Item("Ripartizione")) = "O" Then
                myriga(4) = "Ospite"
            End If
            If campodb(myPOSTreader.Item("Ripartizione")) = "P" Then
                myriga(4) = "Parente"
            End If
            If campodb(myPOSTreader.Item("Ripartizione")) = "C" Then
                myriga(4) = "Comune"
            End If

            myriga(5) = Format(campodbn(myPOSTreader.Item("importo")), "#,##0.00")
            myriga(6) = campodb(myPOSTreader.Item("Scadenza"))

            Tabella.Rows.Add(myriga)
        Loop

        myPOSTreader.Close()
        cn.Close()


        ViewState("Appoggio") = Tabella

        Grid.AutoGenerateColumns = True

        Grid.DataSource = Tabella

        Grid.DataBind()

        'Btn_Nuovo.Visible = True
    End Sub


    Function campodbn(ByVal oggetto As Object) As Double
        If IsDBNull(oggetto) Then
            Return 0
        Else
            Return oggetto
        End If
    End Function

    Protected Sub Grid_PageIndexChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewPageEventArgs) Handles Grid.PageIndexChanging
        Tabella = ViewState("Appoggio")
        Grid.PageIndex = e.NewPageIndex
        Grid.DataSource = Tabella
        Grid.DataBind()
    End Sub

    Protected Sub Grid_RowCommand(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewCommandEventArgs) Handles Grid.RowCommand
        If e.CommandName = "Seleziona" Then


            Dim d As Integer


            Tabella = ViewState("Appoggio")

            d = Val(e.CommandArgument)


            Dim Codice As String


            Codice = Tabella.Rows(d).Item(0).ToString


            Response.Redirect("Tabella_ExtraFissi.aspx?Codice=" & Codice)
        End If
    End Sub

    Protected Sub ImgRicerca_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles ImgRicerca.Click
        Response.Redirect("Tabella_ExtraFissi.aspx")
    End Sub




    Private Function SplitWords(ByVal s As String) As String()
        '
        ' Call Regex.Split function from the imported namespace.
        ' Return the result array.
        '
        Return Regex.Split(s, "\W+")
    End Function

    Protected Sub Btn_Esci_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles Btn_Esci.Click
        Response.Redirect("Menu_Tabelle.aspx")
    End Sub


    Protected Sub Lnk_ToExcel_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles Lnk_ToExcel.Click
        Dim cn As New OleDbConnection


        Dim ConnectionString As String = Session("DC_OSPITE")

        cn = New Data.OleDb.OleDbConnection(ConnectionString)

        cn.Open()

        Dim cmd As New OleDbCommand




        cmd.CommandText = "Select Top 100 * From TABELLAEXTRAFISSI Order By CODICEEXTRA"


        cmd.Connection = cn
        cmd.Connection = cn

        Tabella.Clear()
        Tabella.Columns.Add("CODICEEXTRA")
        Tabella.Columns.Add("Descrizione")
        Tabella.Columns.Add("CodiceIVA")
        Tabella.Columns.Add("Conto")
        Tabella.Columns.Add("TIPO IMPORTO")
        Tabella.Columns.Add("IMPORTO")
        Tabella.Columns.Add("RIPARTIZIONE")
        Tabella.Columns.Add("Anticipato")
        Tabella.Columns.Add("CENTROSERVIZIO")
        Tabella.Columns.Add("Struttura")
        Tabella.Columns.Add("Importo")

        Dim myPOSTreader As OleDbDataReader = cmd.ExecuteReader()
        Do While myPOSTreader.Read
            Dim myriga As System.Data.DataRow = Tabella.NewRow()

            myriga(0) = campodb(myPOSTreader.Item("CODICEEXTRA"))
            myriga(1) = campodb(myPOSTreader.Item("Descrizione"))

            Dim CSIva As New Cls_IVA


            CSIva.Codice = campodb(myPOSTreader.Item("CodiceIVA"))
            CSIva.Leggi(Session("DC_TABELLE"), CSIva.Codice)

            myriga(2) = CSIva.Descrizione

            Dim Ms As New Cls_Pianodeiconti

            Ms.Mastro = Val(campodb(myPOSTreader.Item("Mastro")))
            Ms.Conto = Val(campodb(myPOSTreader.Item("Conto")))
            Ms.Sottoconto = Val(campodb(myPOSTreader.Item("SottoConto")))
            Ms.Decodfica(Session("DC_GENERALE"))
            myriga(3) = Ms.Descrizione


            myriga(4) = campodb(myPOSTreader.Item("TipoImporto"))
            myriga(5) = campodb(myPOSTreader.Item("IMPORTO"))

            myriga(6) = campodb(myPOSTreader.Item("RIPARTIZIONE"))

            myriga(7) = campodb(myPOSTreader.Item("Anticipato"))

            Dim Cserv As New Cls_CentroServizio

            Cserv.CENTROSERVIZIO = campodb(myPOSTreader.Item("CENTROSERVIZIO"))
            Cserv.Leggi(Session("DC_OSPITE"), Cserv.CENTROSERVIZIO)

            myriga(8) = Cserv.DESCRIZIONE


            Dim kVilla As New Cls_TabelleDescrittiveOspitiAccessori

            kVilla.TipoTabella = "VIL"
            kVilla.CodiceTabella = campodb(myPOSTreader.Item("struttura"))
            kVilla.Leggi(Session("DC_OSPITIACCESSORI"))

            myriga(9) = kVilla.Descrizione

            myriga(10) = campodbn(myPOSTreader.Item("importo"))

            Tabella.Rows.Add(myriga)
        Loop

        myPOSTreader.Close()
        cn.Close()


        Session("GrigliaSoloStampa") = Tabella
        ScriptManager.RegisterClientScriptBlock(Me, Me.GetType(), "Stampa", "openPopUp('ExportExcel.aspx','Excel','width=800,height=600');", True)

    End Sub


    Function campodb(ByVal oggetto As Object) As String
        If IsDBNull(oggetto) Then
            Return ""
        Else
            Return oggetto
        End If
    End Function

    Protected Sub ImageButton1_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles ImageButton1.Click

        Dim cn As New OleDbConnection


        Dim ConnectionString As String = Session("DC_OSPITE")

        cn = New Data.OleDb.OleDbConnection(ConnectionString)

        cn.Open()

        Dim cmd As New OleDbCommand

        Dim Condizione As String = ""

        If RB_Comune.Checked = True Then
            Condizione = " Ripartizione = 'C'"
        End If

        If RB_Ospite.Checked = True Then
            Condizione = " Ripartizione = 'O'"
        End If

        If RB_Parente.Checked = True Then
            Condizione = " Ripartizione = 'P'"
        End If


        If Txt_Descrizione.Text.Trim = "" Then
            If Condizione = "" Then
                cmd.CommandText = ("select top 50  * from TABELLAEXTRAFISSI  " & _
                                       "  Order By Descrizione,DescrizioneInterna")
            Else
                cmd.CommandText = ("select top 50  * from TABELLAEXTRAFISSI Where " & Condizione & _
                                   "  Order By Descrizione,DescrizioneInterna")
            End If

        Else

            If Condizione = "" Then
                cmd.CommandText = ("select top 50  * from TABELLAEXTRAFISSI where descrizione like ? " & _
                                   "  Order By Descrizione,DescrizioneInterna")

            Else
                cmd.CommandText = ("select top 50  * from TABELLAEXTRAFISSI where descrizione like ? And " & Condizione & _
                                   "  Order By Descrizione,DescrizioneInterna")

            End If
            cmd.Parameters.AddWithValue("@Descrizione", Txt_Descrizione.Text)
        End If
        cmd.Connection = cn



        Tabella.Clear()
        Tabella.Columns.Add("Codice", GetType(String))
        Tabella.Columns.Add("Struttura/Servizio", GetType(String))
        Tabella.Columns.Add("Descrizione", GetType(String))
        Tabella.Columns.Add("Descrizione Interna", GetType(String))

        Tabella.Columns.Add("Ripartizione", GetType(String))
        Tabella.Columns.Add("Importo", GetType(String))
        Tabella.Columns.Add("Scadenza", GetType(String))

        Dim myPOSTreader As OleDbDataReader = cmd.ExecuteReader()
        Do While myPOSTreader.Read
            Dim myriga As System.Data.DataRow = Tabella.NewRow()

            myriga(0) = campodb(myPOSTreader.Item("CODICEEXTRA"))

            Dim Cserv As New Cls_CentroServizio

            Cserv.CENTROSERVIZIO = campodb(myPOSTreader.Item("CENTROSERVIZIO"))
            Cserv.Leggi(Session("DC_OSPITE"), Cserv.CENTROSERVIZIO)


            Dim kVilla As New Cls_TabelleDescrittiveOspitiAccessori

            kVilla.TipoTabella = "VIL"
            kVilla.CodiceTabella = campodb(myPOSTreader.Item("struttura"))
            kVilla.Leggi(Session("DC_OSPITIACCESSORI"))

            If kVilla.Descrizione <> "" Then
                myriga(1) = kVilla.Descrizione
                If Cserv.DESCRIZIONE <> "" Then
                    myriga(1) = kVilla.Descrizione & " - " & Cserv.DESCRIZIONE
                End If
            Else
                myriga(1) = Cserv.DESCRIZIONE
            End If


            myriga(2) = campodb(myPOSTreader.Item("Descrizione"))
            myriga(3) = campodb(myPOSTreader.Item("DescrizioneInterna"))

            If campodb(myPOSTreader.Item("Ripartizione")) = "O" Then
                myriga(4) = "Ospite"
            End If
            If campodb(myPOSTreader.Item("Ripartizione")) = "P" Then
                myriga(4) = "Parente"
            End If
            If campodb(myPOSTreader.Item("Ripartizione")) = "C" Then
                myriga(4) = "Comune"
            End If

            myriga(5) = Format(campodbn(myPOSTreader.Item("importo")), "#,##0.00")
            myriga(6) = campodb(myPOSTreader.Item("Scadenza"))

            Tabella.Rows.Add(myriga)
        Loop

        myPOSTreader.Close()
        cn.Close()


        ViewState("Appoggio") = Tabella

        Grid.AutoGenerateColumns = True

        Grid.DataSource = Tabella

        Grid.DataBind()

        'Btn_Nuovo.Visible = True
    End Sub
End Class
