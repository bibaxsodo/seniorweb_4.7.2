﻿Imports System.Web.Hosting
Imports System.Data.OleDb
Partial Class OspitiWeb_VisualizzaMovimenti
    Inherits System.Web.UI.Page
    Dim MyTable As New System.Data.DataTable("tabella")
    Dim MyDataSet As New System.Data.DataSet()

    Function campodb(ByVal oggetto As Object) As String
        If IsDBNull(oggetto) Then
            Return ""
        Else
            Return oggetto
        End If
    End Function
    Private Sub CaricaTabella()
        Dim ConnectionString As String = Session("DC_OSPITE")
        Dim cn As OleDbConnection
        Dim MySql As String
        Dim Condizione As String

        cn = New Data.OleDb.OleDbConnection(ConnectionString)

        cn.Open()

        MyTable.Clear()
        MyTable.Columns.Clear()

        MyTable.Columns.Add("Centro Servizio", GetType(String))
        MyTable.Columns.Add("Codice Ospite", GetType(Long))
        MyTable.Columns.Add("Nome", GetType(String))
        MyTable.Columns.Add("Data Nascita", GetType(String))
        MyTable.Columns.Add("Luogo Nascita", GetType(String))
        MyTable.Columns.Add("Tipo Movimento", GetType(String))
        MyTable.Columns.Add("Data", GetType(String))
        MyTable.Columns.Add("Causale", GetType(String))
        MyTable.Columns.Add("Descrizione", GetType(String))
        MyTable.Columns.Add("Medico", GetType(String))
        MyTable.Columns.Add("Impegnativa", GetType(String))
        MyTable.Columns.Add("Eta", GetType(String))


        Dim cmd As New OleDbCommand()

        Condizione = "("
        If Chk_Accoglimento.Checked = True Then
            If Condizione <> "(" Then
                Condizione = Condizione & " OR "
            End If
            Condizione = Condizione & " TipoMov = '05'"
        End If
        If Chk_UscitaTemporanea.Checked = True Then
            If Condizione <> "(" Then
                Condizione = Condizione & " OR "
            End If
            Condizione = Condizione & " TipoMov = '03'"
        End If
        If Chk_Entrata.Checked = True Then
            If Condizione <> "(" Then
                Condizione = Condizione & " OR "
            End If
            Condizione = Condizione & " TipoMov = '04'"
        End If
        If Chk_UscitaDefinitiva.Checked = True Then
            If Condizione <> "(" Then
                Condizione = Condizione & " OR "
            End If
            Condizione = Condizione & " TipoMov = '13'"
        End If
        Condizione = Condizione & ")"

        If Txt_Ospite.Text.Trim <> "" Then
            Dim Vettore(100) As String

            Vettore = SplitWords(Txt_Ospite.Text)
            If Not IsNothing(Vettore(0)) Then
                If Val(Vettore(0)) > 0 Then
                    If Condizione <> "" Then
                        Condizione = Condizione & " And "
                    Else
                        Condizione = Condizione & " "
                    End If
                    Condizione = Condizione & "  CodiceOspite = " & Val(Vettore(0))
                End If
            End If
        End If
        If DD_CServ.SelectedValue = "" Then
            If DD_Struttura.SelectedValue <> "" Then
                If Condizione <> "" Then
                    Condizione = Condizione & " And ("
                Else
                    Condizione = Condizione & " ("
                End If
                Dim Indice As Integer
                Call AggiornaCServ()
                For Indice = 0 To DD_CServ.Items.Count - 1
                    If Indice >= 1 Then
                        If Condizione <> "" Then
                            Condizione = Condizione & " Or "
                        End If
                    End If
                    Condizione = Condizione & "  MOVIMENTI.CentroServizio = '" & DD_CServ.Items(Indice).Value & "'"
                Next
                Condizione = Condizione & ") "
            End If
            MySql = "Select * From Movimenti Where Data >= ? And Data <= ? And " & Condizione & " Order By Data "
        Else
            MySql = "Select * From Movimenti Where Data >= ? And Data <= ? And " & Condizione & " And CENTROSERVIZIO = '" & DD_CServ.SelectedValue & "'" & " Order By Data "
        End If


        cmd.CommandText = MySql
        Dim DataDal As Date = Txt_DataDal.Text
        Dim DataAl As Date = Txt_DataAl.Text

        cmd.Parameters.AddWithValue("@DataDal", DataDal)
        cmd.Parameters.AddWithValue("@DataAl", DataAl)
        cmd.Connection = cn
        Dim myPOSTreader As OleDbDataReader = cmd.ExecuteReader()
        Do While myPOSTreader.Read
            Dim DecOsp As New ClsOspite

            DecOsp.Leggi(Session("DC_OSPITE"), Val(campodb(myPOSTreader.Item("CodiceOspite"))))

            If DecOsp.NonInUso <> "S" And ((DecOsp.Sesso = "F" And Chk_Donne.Checked = True) Or (DecOsp.Sesso = "M" And Chk_Uomini.Checked = True) Or DecOsp.Sesso = "") Then
                Dim myriga As System.Data.DataRow = MyTable.NewRow()
                myriga(0) = campodb(myPOSTreader.Item("CENTROSERVIZIO"))
                myriga(1) = Val(campodb(myPOSTreader.Item("CodiceOspite")))

                myriga(2) = DecOsp.Nome
                myriga(3) = Format(DecOsp.DataNascita, "dd/MM/yyyy")

                Dim MComuni As New ClsComune

                MComuni.Provincia = DecOsp.ProvinciaDiNascita
                MComuni.Comune = DecOsp.ComuneDiNascita
                MComuni.DecodficaComune(Session("DC_OSPITE"))

                myriga(4) = MComuni.Descrizione



                If myPOSTreader.Item("TIPOMOV") = "05" Then
                    myriga(5) = "Accoglimento"
                End If
                If myPOSTreader.Item("TIPOMOV") = "13" Then
                    myriga(5) = "Uscita Definitiva"
                End If
                If myPOSTreader.Item("TIPOMOV") = "03" Then
                    myriga(5) = "Uscita"
                End If
                If myPOSTreader.Item("TIPOMOV") = "04" Then
                    myriga(5) = "Entrata"
                End If


                myriga(6) = Format(myPOSTreader.Item("DATA"), "dd/MM/yyyy")

                Dim DecCau As New Cls_CausaliEntrataUscita

                DecCau.Codice = campodb(myPOSTreader.Item("CAUSALE"))
                DecCau.LeggiCausale(Session("DC_OSPITE"))
                myriga(7) = DecCau.Descrizione

                myriga(8) = campodb(myPOSTreader.Item("Descrizione"))

                Dim Medico As New Cls_Medici

                Medico.CodiceMedico = DecOsp.CodiceMedico
                Medico.Leggi(Session("DC_OSPITE"))

                myriga(9) = Medico.Nome

                Dim MyImpegnativa As New Cls_Impegnativa

                MyImpegnativa.CODICEOSPITE = DecOsp.CodiceOspite
                MyImpegnativa.CENTROSERVIZIO = campodb(myPOSTreader.Item("CENTROSERVIZIO"))
                MyImpegnativa.UltimaData(Session("DC_OSPITE"), MyImpegnativa.CODICEOSPITE, MyImpegnativa.CENTROSERVIZIO)

                myriga(10) = MyImpegnativa.DESCRIZIONE
                Dim Eta As Integer

                Eta = 0
                If IsDate(DecOsp.DataNascita) Then
                    Eta = DateDiff("M", DecOsp.DataNascita, myPOSTreader.Item("DATA"))
                End If

                myriga(11) = Int(Eta / 12)

               

                If DecOsp.CodiceMedico = "" Or Val(DecOsp.CodiceMedico) = 0 Then
                    myriga(9) = DecOsp.NomeMedico
                End If


                MyTable.Rows.Add(myriga)
            End If
        Loop
        cn.Close()

        Dim myriga1 As System.Data.DataRow = MyTable.NewRow()
        MyTable.Rows.Add(myriga1)

        GridView1.AutoGenerateColumns = True
        GridView1.DataSource = MyTable
        GridView1.Font.Size = 10
        GridView1.DataBind()


        Dim i As Integer

        For i = 0 To GridView1.Rows.Count - 1

            Dim jk As Integer
            jk = Val(GridView1.Rows(i).Cells(1).Text)

            Dim Csk As String
            Csk = GridView1.Rows(i).Cells(0).Text

            If jk > 0 Then
                GridView1.Rows(i).Cells(1).Text = "<a href=""#"" onclick=""DialogBox('Anagrafica.aspx?CodiceOspite=" & jk & "&CentroServizio=" & Csk & "');"" >" & GridView1.Rows(i).Cells(1).Text & "</a>"
            End If
        Next
    End Sub


    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Call EseguiJS()
        If Trim(Session("UTENTE")) = "" Then
            Response.Redirect("../Login.aspx")
            Exit Sub
        End If

        If Page.IsPostBack = True Then Exit Sub


        Dim K1 As New Cls_SqlString

        Dim Barra As New Cls_BarraSenior

        If Not IsNothing(Session("RicercaAnagraficaSQLString")) Then
            K1 = Session("RicercaAnagraficaSQLString")
        End If

        Lbl_BarraSenior.Text = Barra.CodiceBarra(Application("SENIOR"), Session("UTENTE"), Page, K1)

        Dim kVilla As New Cls_TabelleDescrittiveOspitiAccessori


        kVilla.UpDateDropBox(Session("DC_OSPITIACCESSORI"), "VIL", DD_Struttura)
        If DD_Struttura.Items.Count = 1 Then
            Call AggiornaCServ()
        End If

        Txt_DataAl.Text = Format(Now, "dd/MM/yyyy")
        Txt_DataDal.Text = Format(Now, "dd/MM/yyyy")

        Lbl_Utente.Text = Session("UTENTE")

        Call EseguiJS()
    End Sub

    Protected Sub ImageButton3_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles ImageButton3.Click
        If Not IsDate(Txt_DataDal.Text) Then
            REM ClientScript.RegisterClientScriptBlock(Me.GetType(), "Errore", "VisualizzaErrore('Data dal non corretta');", True)
            ScriptManager.RegisterStartupScript(Me, Me.GetType(), "visualizzaA", "VisualizzaErrore('Data dal non corretta');", True)
            Call EseguiJS()
            Exit Sub
        End If
        If Not IsDate(Txt_DataAl.Text) Then
            REM ClientScript.RegisterClientScriptBlock(Me.GetType(), "Errore", "VisualizzaErrore('Data al non corretta');", True)
            ScriptManager.RegisterStartupScript(Me, Me.GetType(), "visualizzaA", "VisualizzaErrore('Data al non corretta');", True)
            Call EseguiJS()
            Exit Sub
        End If
        If Chk_Accoglimento.Checked = False And Chk_Entrata.Checked = False And Chk_UscitaDefinitiva.Checked = False And Chk_UscitaTemporanea.Checked = False Then
            REM ClientScript.RegisterClientScriptBlock(Me.GetType(), "Errore", "VisualizzaErrore('Specificare almeno un tipo movimento');", True)
            ScriptManager.RegisterStartupScript(Me, Me.GetType(), "visualizzaA", "VisualizzaErrore('Specificare almeno un tipo movimento');", True)
            Call EseguiJS()
            Exit Sub
        End If

        Call CaricaTabella()
        Call EseguiJS()
    End Sub

    Protected Sub ImageButton4_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles ImageButton4.Click
        If Not IsDate(Txt_DataDal.Text) Then
            ClientScript.RegisterClientScriptBlock(Me.GetType(), "Errore", "VisualizzaErrore('Data dal non corretta');", True)
            Exit Sub
        End If
        If Not IsDate(Txt_DataAl.Text) Then
            ClientScript.RegisterClientScriptBlock(Me.GetType(), "Errore", "VisualizzaErrore('Data al non corretta');", True)
            Exit Sub
        End If
        If Chk_Accoglimento.Checked = False And Chk_Entrata.Checked = False And Chk_UscitaDefinitiva.Checked = False And Chk_UscitaTemporanea.Checked = False Then
            ClientScript.RegisterClientScriptBlock(Me.GetType(), "Errore", "VisualizzaErrore('Specificare almeno un tipo movimento');", True)
            Exit Sub
        End If

        Call CaricaTabella()
        If MyTable.Rows.Count > 1 Then
            Response.Clear()
            Response.AddHeader("content-disposition", "attachment;filename=Movimenti.xls")
            Response.Charset = String.Empty
            Response.Cache.SetCacheability(HttpCacheability.NoCache)
            Response.ContentType = "application/vnd.xls"
            Dim stringWrite As New System.IO.StringWriter
            Dim htmlWrite As New HtmlTextWriter(stringWrite)

            form1.Controls.Clear()
            form1.Controls.Add(GridView1)

            form1.RenderControl(htmlWrite)

            Response.Write(stringWrite.ToString())
            Response.End()
            UpdatePanel1.Update()
        End If
    End Sub


    Private Sub EseguiJS()
        Dim MyJs As String
        MyJs = "$(document).ready(function() {"

        MyJs = MyJs & "var els = document.getElementsByTagName(""*"");"

        MyJs = MyJs & "for (var i=0;i<els.length;i++)"
        MyJs = MyJs & "if ( els[i].id ) { "
        MyJs = MyJs & " var appoggio =els[i].id; "

        MyJs = MyJs & " if ((appoggio.match('Txt_Ospite')!= null) || (appoggio.match('Txt_Ospite')!= null))  {  "
        MyJs = MyJs & " $(els[i]).autocomplete('AutocompleteOspitiSenzaCserv.ashx?Utente=" & Session("UTENTE") & "', {delay:5,minChars:3});"
        MyJs = MyJs & "    }"

        MyJs = MyJs & " if (appoggio.match('Txt_Data')!= null) {  "
        MyJs = MyJs & " $(els[i]).mask(""99/99/9999"");"

        MyJs = MyJs & " $(els[i]).datepicker({ changeMonth: true,changeYear: true,  yearRange: ""1990:" & Year(Now) + 5 & """},$.datepicker.regional[""it""]);"

        MyJs = MyJs & "    }"
        MyJs = MyJs & "} "
        MyJs = MyJs & "if (window.innerHeight>0) { $(""#BarraLaterale"").css(""height"",(window.innerHeight - 94) + ""px""); } else"
        MyJs = MyJs & "{ $(""#BarraLaterale"").css(""height"",(document.documentElement.offsetHeight - 94) + ""px"");  }"
        MyJs = MyJs & "});"

        'MyJs = "$(document).ready(function() { $('.myClass').keypress(function() { handleEnter($('.myClass').val(), true, true); } );     $('#" & Txt_ImportoPagato.ClientID & "').blur(function() { var ap = formatNumber ($('#" & Txt_ImportoPagato.ClientID & "').val(),2); $('#" & Txt_ImportoPagato.ClientID & "').val(ap); }); });"
        ScriptManager.RegisterStartupScript(Me, Me.GetType(), "visualizzaRitIm", MyJs, True)
    End Sub

    Protected Sub Btn_Esci_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles Btn_Esci.Click
        Response.Redirect("Menu_Visualizzazioni.aspx")
    End Sub


    Private Sub AggiornaCServ()
        Dim kCsrv As New Cls_CentroServizio

        If DD_Struttura.SelectedValue = "" Then
            kCsrv.UpDateDropBox(Session("DC_OSPITE"), DD_CServ)
        Else
            kCsrv.UpDateDropBoxStruttura(Session("DC_OSPITE"), DD_CServ, DD_Struttura.SelectedValue)
        End If
    End Sub



    Protected Sub DD_Struttura_TextChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles DD_Struttura.TextChanged
        AggiornaCServ()
        Call EseguiJS()
    End Sub

    Protected Sub DD_CServ_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles DD_CServ.Load

    End Sub


    Private Function SplitWords(ByVal s As String) As String()
        '
        ' Call Regex.Split function from the imported namespace.
        ' Return the result array.
        '
        Return Regex.Split(s, "\W+")
    End Function

    Protected Sub Btn_Stampa_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles Btn_Stampa.Click
        If Not IsDate(Txt_DataDal.Text) Then
            ClientScript.RegisterClientScriptBlock(Me.GetType(), "Errore", "VisualizzaErrore('Data dal non corretta');", True)
            Exit Sub
        End If
        If Not IsDate(Txt_DataAl.Text) Then
            ClientScript.RegisterClientScriptBlock(Me.GetType(), "Errore", "VisualizzaErrore('Data al non corretta');", True)
            Exit Sub
        End If
        If Chk_Accoglimento.Checked = False And Chk_Entrata.Checked = False And Chk_UscitaDefinitiva.Checked = False And Chk_UscitaTemporanea.Checked = False Then
            ClientScript.RegisterClientScriptBlock(Me.GetType(), "Errore", "VisualizzaErrore('Specificare almeno un tipo movimento');", True)
            Exit Sub
        End If

        Call CaricaTabella()


        Session("GrigliaSoloStampa") = MyTable

        Session("ParametriSoloStampa") = ""

        ScriptManager.RegisterStartupScript(Me, Me.GetType(), "Stampa", "openPopUp('GrigliaSoloStampa.aspx','Stampe','width=800,height=600');", True)

        Call EseguiJS()
    End Sub

End Class

