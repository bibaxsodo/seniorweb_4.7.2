﻿Imports System.Data.OleDb

Public Class AutoCompleteOspitiTuttiCentriServizio
    Implements System.Web.IHttpHandler, IRequiresSessionState

    Public Sub ProcessRequest(ByVal context As HttpContext) Implements IHttpHandler.ProcessRequest
        context.Response.ContentType = "text/plain"
        Dim RICERCA As String = context.Request.QueryString("q")
        Dim UTENTE As String = context.Request.QueryString("UTENTE")
        Dim CSERV As String = context.Request.QueryString("CSERV")
        context.Response.Cache.SetCacheability(HttpCacheability.NoCache)

        Dim sb As StringBuilder = New StringBuilder

        Dim cn As OleDbConnection
        Dim DbC As New Cls_Login

        If UTENTE = "" Or IsNothing(UTENTE) Then
            Exit Sub
        End If

        DbC.Utente = UTENTE
        DbC.LeggiSP(context.Application("SENIOR"))

        If IsNothing(DbC.ABILITAZIONI) Then
            Exit Sub
        End If

        If IsDBNull(DbC.ABILITAZIONI) Then
            Exit Sub
        End If

        Dim Appoggio As String = DbC.ABILITAZIONI
        Dim CentroServizioAbilitato As String = ""
        Dim StrutturaAbilitata As String = ""

        If Appoggio.IndexOf("<STR=") > 0 Then
            Dim Posizione As Integer = Appoggio.IndexOf("<STR=") + 5

            StrutturaAbilitata = Appoggio.Substring(Posizione, Appoggio.IndexOf(">", Posizione) - Posizione)
        End If


        If Appoggio.IndexOf("<CSERV=") > 0 Then
            Dim Posizione As Integer = Appoggio.IndexOf("<CSERV=") + 7

            CentroServizioAbilitato = Appoggio.Substring(Posizione, Appoggio.IndexOf(">", Posizione) - Posizione)
        End If

        Try
            cn = New Data.OleDb.OleDbConnection(DbC.Ospiti)

            cn.Open()
            Dim cmd As New OleDbCommand()

            Dim Condizione As String = ""

            Condizione = " And (AnagraficaComune.CodiceOspite > 0 And Nome Like '%" & RICERCA & "%' OR CODICEFISCALE LIKE '" & RICERCA & "') And (NonInUso = '' Or NonInUso is Null) "
            cmd.CommandText = ("select AnagraficaComune.CodiceOspite as CodiceOSpite, AnagraficaComune.CodiceParente as CodiceParente,AnagraficaComune.DataNascita, MOVIMENTI.Data, MOVIMENTI.CentroServizio as CentroServizio, Nome  from AnagraficaComune INNER JOIN MOVIMENTI ON AnagraficaComune.CODICEOSPITE = MOVIMENTI.CODICEOSPITE where MOVIMENTI.DATA = (SELECT MAX(DATA) FROM MOVIMENTI AS K WHERE K.CODICEOSPITE = AnagraficaComune.CODICEOSPITE AND K.CentroServizio  = MOVIMENTI.CentroServizio) " & Condizione & "  ORDER BY NOME,Data ,CENTROSERVIZIO")

            cmd.Connection = cn

            Dim Counter As Integer = 0
            Dim myPOSTreader As OleDbDataReader = cmd.ExecuteReader()
            Do While myPOSTreader.Read
                Dim Inserimento As Boolean = True

                If StrutturaAbilitata <> "" Then
                    Dim M As New Cls_CentroServizio

                    M.CENTROSERVIZIO = campodb(myPOSTreader.Item("CentroServizio"))
                    M.Leggi(DbC.Ospiti, M.CENTROSERVIZIO)
                    If M.Villa <> StrutturaAbilitata Then
                        Inserimento = False
                    End If
                End If

                If CentroServizioAbilitato <> "" Then
                    If campodb(myPOSTreader.Item("CentroServizio")) <> CentroServizioAbilitato Then
                        Inserimento = False
                    End If
                End If

                If Inserimento = True Then
                    If Val(campodb(myPOSTreader.Item("CodiceParente"))) > 0 Then
                        Dim LgOspite As New ClsOspite

                        LgOspite.CodiceOspite = myPOSTreader.Item("CodiceOspite")
                        LgOspite.Leggi(DbC.Ospiti, LgOspite.CodiceOspite)

                        sb.Append(campodb(myPOSTreader.Item("CentroServizio")).Trim & " " & myPOSTreader.Item("CodiceOspite") & " " & myPOSTreader.Item("Nome") & " <br><font color=""red"">Parente di " & LgOspite.Nome & "</font>").Append(Environment.NewLine)
                    Else
                        sb.Append(campodb(myPOSTreader.Item("CentroServizio")).Trim & " " & myPOSTreader.Item("CodiceOspite") & " " & myPOSTreader.Item("Nome") & " " & campodb(myPOSTreader.Item("DataNascita"))).Append(Environment.NewLine)
                    End If
                    Counter = Counter + 1
                    If Counter > 10 Then
                        Exit Do
                    End If
                End If
            Loop
        Catch ex As Exception

        End Try

        context.Response.Write(sb.ToString)
    End Sub

    Public ReadOnly Property IsReusable() As Boolean Implements IHttpHandler.IsReusable
        Get
            Return False
        End Get
    End Property

    Function campodb(ByVal oggetto As Object) As String
        If IsDBNull(oggetto) Then
            Return ""
        Else
            Return oggetto
        End If
    End Function

    Private Function SplitWords(ByVal s As String) As String()
        '
        ' Call Regex.Split function from the imported namespace.
        ' Return the result array.
        '
        Return Regex.Split(s, "\W+")
    End Function
End Class