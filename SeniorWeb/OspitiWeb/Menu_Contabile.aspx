﻿<%@ Page Language="VB" AutoEventWireup="false" Inherits="OspitiWeb_Menu_Contabile" CodeFile="Menu_Contabile.aspx.vb" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
    <meta http-equiv="x-ua-compatible" content="IE=9" />
    <title>Menu Strumenti</title>
    <asp:PlaceHolder runat="server">
        <%: System.Web.Optimization.Styles.Render("~/Content/AjaxControlToolkit/Styles/Bundle") %>
    </asp:PlaceHolder>
    <link rel="stylesheet" href="ospiti.css?ver=11" type="text/css" />
    <link rel="shortcut icon" href="images/SENIOR.ico" />
    <link href="js/jquery.autocomplete.css" rel="stylesheet" type="text/css" />

    <script src="js/jquery-1.5.1.min.js" type="text/javascript"></script>
    <script src="js/jquery.autocomplete.js" type="text/javascript"></script>
    <script src="js/jquery.maskedinput-1.3.min.js" type="text/javascript"></script>
    <script src="js/NoEnter.js" type="text/javascript"></script>
    <script type="text/javascript">
        $(document).ready(function () {
            if (window.innerHeight > 0) { $("#BarraLaterale").css("height", (window.innerHeight - 105) + "px"); } else { $("#BarraLaterale").css("height", (document.documentElement.offsetHeight - 105) + "px"); }
        });
    </script>
</head>
<body>
    <form id="form1" runat="server">
        <asp:ScriptManager ID="ScriptManager1" runat="server" EnableScriptGlobalization="true">
            <Scripts>
                <asp:ScriptReference Path="~/Scripts/AjaxControlToolkit/Bundle" />
            </Scripts>
        </asp:ScriptManager>
        <asp:Label ID="Lbl_BarraSenior" runat="server" Text=""></asp:Label>
        <div>
            <table style="width: 100%;" cellpadding="0" cellspacing="0">
                <tr>
                    <td style="width: 160px; background-color: #F0F0F0;"></td>
                    <td>
                        <div class="Titolo">Ospiti - Strumenti</div>
                        <div class="SottoTitolo">
                            <br />
                            <br />
                        </div>
                    </td>

                    <td style="text-align: right; vertical-align: top;">
                        <span class="BenvenutoText"><font size="4">Benvenuto <%=Session("UTENTE")%></font></span>
                    </td>
                </tr>
                <tr>

                    <td style="width: 160px; background-color: #F0F0F0; text-align: center; vertical-align: top;" id="BarraLaterale">
                        <asp:ImageButton ID="Btn_Esci" runat="server" BackColor="Transparent" ImageUrl="../images/Menu_Indietro.png" ToolTip="Chiudi" class="Effetto" />
                    </td>
                    <td colspan="2" style="vertical-align: top;">
                        <table style="width: 60%;">
                            <tr>
                                <td style="text-align: center;">
                                    <a href="Calcolo.aspx" id="idcalcolo">
                                        <img alt="Calcolo" src="../images/Menu_Calcolo.png" class="Effetto" style="border-width: 0;"></a>
                                </td>
                                <td style="text-align: center; width: 20%;">
                                    <a href="Emissione.aspx">
                                        <img src="../images/Menu_Emissione.png" alt="Elimina Fatturazione" class="Effetto" style="border-width: 0;"></a>
                                </td>

                                <td style="text-align: center; width: 20%;">
                                    <a href="Emissione.aspx">
                                        <img src="../images/Menu_Emissione.png" alt="Elimina Fatturazione" class="Effetto" style="border-width: 0;"></a>
                                </td>
                                <td style="text-align: center; width: 20%;">
                                    <a href="StampaDocumenti.aspx">
                                        <img src="../images/Menu_StampaDocumenti.png" alt="Controlla Piano Dei Conti" class="Effetto" style="border-width: 0;"></a>
                                </td>

                            </tr>
                            <tr>
                                <td style="text-align: center; vertical-align: top;"><span class="MenuText">EMISSIONE</span></td>
                                <td style="text-align: center; vertical-align: top;"><span class="MenuText">STAMPA DOCUMENTI</span></td>
                                <td style="text-align: center; vertical-align: top;"><span class="MenuText">AGG. RETTA TOTALE</span></td>
                                <td style="text-align: center; vertical-align: top;"><span class="MenuText">AGG.LISTINO</span></td>
                                <td style="text-align: center; vertical-align: top;"><span class="MenuText">INCASSO RID</span></td>
                            </tr>
                            <tr>
                                <td></td>
                            </tr>
                            <tr>
                                <td style="text-align: center; width: 20%;">
                                    <a href="ImportAddebitiDaFileExcel.aspx">
                                        <img alt="Import Addebici Accrediti Excel" src="../images/Menu_Excel.png" class="Effetto" style="border-width: 0;"></a></td>
                                <td style="text-align: center; width: 20%;">
                                    <a href="FatturaDaAddebitiAccrediti.aspx">
                                        <img src="../images/fatturaaddebiticrediti.jpg" alt="Fatture da Addebiti Accrediti" class="Effetto" style="border-width: 0;"></a>
                                </td>
                                <td style="text-align: center; width: 20%;"><a href="IncassoDaBollettino.aspx">
                                    <img alt="Incassi Da bollettino" src="../images/Menu_IncassiAnticipi.png" class="Effetto" style="border-width: 0;"></a></td>
                                <td style="text-align: center; width: 20%;"><a href="RicercaAnagrafica.aspx?TIPO=MODIFICARETTA">
                                    <img alt="Modifica Retta" src="../images/Menu_ModificaRetta.jpg" class="Effetto" style="border-width: 0;"></a></td>
                                <td style="text-align: center; width: 20%;"><a href="RicercaAnagrafica.aspx?TIPO=MODIFICACSERV">
                                    <img alt="Modifica CentroServizio" src="../images/Menu_ModificaCserv.jpg" class="Effetto" style="border-width: 0;"></a></td>
                            </tr>
                            <tr>
                                <td style="text-align: center; vertical-align: top;"><span class="MenuText">IMPORT ADD.ACR</span></td>
                                <td style="text-align: center; vertical-align: top;"><span class="MenuText">FATT. AD/AC</span></td>
                                <td style="text-align: center; vertical-align: top;"><span class="MenuText">INC. DA BOLLETTINO</span></td>
                                <td style="text-align: center; vertical-align: top;"><span class="MenuText">MODIFICA RETTA</span></td>
                                <td style="text-align: center; vertical-align: top;"><span class="MenuText">MODIFICA CSERV</span></td>
                            </tr>
                            <tr>
                                <td style="text-align: center; width: 20%;">
                                    <a href="EliminaRidCreati.aspx">
                                        <img alt="Elimina Rid Creati" src="../images/Menu_GestioneIncassiPagamenti.png" class="Effetto" style="border-width: 0;"></a></td>
                                <td style="text-align: center; width: 20%;">
                                    <a href="Elimina_flagexport.aspx">
                                        <img alt="Elimina flag Export" src="../images/Menu_Strumenti.png" class="Effetto" style="border-width: 0;"></a></td>

                                <td style="text-align: center; width: 20%;">
                                    <a href="ForzaNonInUso.aspx">
                                        <img alt="Imposta Non In Uso" src="../images/Menu_Strumenti.png" class="Effetto" style="border-width: 0;">
                                    </a>
                                </td>
                                <td style="text-align: center; width: 20%;">
                                    <a href="StampaFattureDettagliate.aspx">
                                        <img alt="Fatture Dettagliate" src="../images/Menu_FattureDettagliate.png" class="Effetto" style="border-width: 0;"></a>
                                </td>
                                <td style="text-align: center; width: 20%;">
                                    <a href="AssegnaListinoOspiti.aspx">
                                        <img src="../images/Menu_AggiornaRette.jpg" alt="Assegna Listino" class="Effetto" style="border-width: 0;"></a>
                                </td>
                            </tr>
                            <tr>
                                <td style="text-align: center; vertical-align: top;"><span class="MenuText">ELIMINA RID CREATI</span></td>
                                <td style="text-align: center; vertical-align: top;"><span class="MenuText">ELIMINA FLAG EXPORT</span></td>
                                <td style="text-align: center; vertical-align: top;"><span class="MenuText">NON IN USO</span></td>
                                <td style="text-align: center; vertical-align: top;"><span class="MenuText">FATTURE DETTAGLIATE</span></td>
                                <td style="text-align: center; vertical-align: top;"><span class="MenuText">ASSEGNA LISTINO</span></td>
                            </tr>
                            <asp:Label ID="Lbl_test" runat="server" Text=""></asp:Label>
                        </table>
                    </td>
                </tr>
            </table>
        </div>
    </form>
</body>
</html>
