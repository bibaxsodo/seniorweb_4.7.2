﻿Imports System.Web.Hosting
Imports System.Data.OleDb

Partial Class OspitiWeb_EliminaFatturazione
    Inherits System.Web.UI.Page

    Protected Sub Btn_Elimina_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles Btn_Elimina.Click
        Dim ConnectionString As String = Session("DC_GENERALE")
        Dim cn As OleDbConnection
        Dim cnOspiti As OleDbConnection
        Dim MySql As String
        Dim Condizione As String
        Dim UltimoNumero As Long




        Dim Txt_DataEmissioneText As Date



        If Not IsDate(Txt_DataEmissione.Text) Then
            ClientScript.RegisterClientScriptBlock(Me.GetType(), "visualizzaA", "VisualizzaErrore('Data formalmente errata');", True)
            Exit Sub
        End If



        If Cmb_CServ.SelectedValue = "" Then
            ClientScript.RegisterClientScriptBlock(Me.GetType(), "visualizzaA", "VisualizzaErrore('Devi indicare il centro servizio');", True)
            Exit Sub
        End If





        Txt_DataEmissioneText = Txt_DataEmissione.Text
        cn = New Data.OleDb.OleDbConnection(ConnectionString)

        cn.Open()

        cnOspiti = New Data.OleDb.OleDbConnection(Session("DC_OSPITE"))

        cnOspiti.Open()

        Dim cmd As New OleDbCommand()
        cmd.Connection = cn
        cmd.CommandText = "Select * From MovimentiContabilitesta Where Utente = 'EMISSIONE' AND  DataAggiornamento > {ts '" & Format(Txt_DataEmissioneText, "yyyy-MM-dd") & " 23:59:59'}  "
        Dim myPOSTreader As OleDbDataReader = cmd.ExecuteReader()
        If myPOSTreader.Read Then            
            ClientScript.RegisterClientScriptBlock(Me.GetType(), "visualizzaB", "VisualizzaErrore('Non posso eliminare, non si tratta dell ultima emissione rette effettuata');", True)
            Exit Sub
        End If
        myPOSTreader.Close()


        Dim cmd1 As New OleDbCommand()
        cmd1.Connection = cn
        cmd1.CommandText = "Select Max(NumeroRegistrazione) From MovimentiContabilitesta Where Utente = 'EMISSIONE' AND  DataAggiornamento > {ts '" & Format(Txt_DataEmissioneText, "yyyy-MM-dd") & " 00:00:00'}  And DataAggiornamento <= {ts '" & Format(Txt_DataEmissioneText, "yyyy-MM-dd") & " 23:59:59'}  "
        Dim myPOSTreader1 As OleDbDataReader = cmd1.ExecuteReader()
        If myPOSTreader1.Read Then
            If IsDBNull(myPOSTreader1.Item(0)) Then
                UltimoNumero = 0
            Else
                UltimoNumero = myPOSTreader1.Item(0)
            End If
        End If


        If UltimoNumero > 0 Then            

            Dim cmd2 As New OleDbCommand()
            cmd2.Connection = cn
            cmd2.CommandText = "Select * From MovimentiContabilitesta Where NumeroRegistrazione > " & UltimoNumero
            Dim myPOSTreader2 As OleDbDataReader = cmd2.ExecuteReader()
            If myPOSTreader2.Read Then
                ClientScript.RegisterClientScriptBlock(Me.GetType(), "visualizzaC", "VisualizzaErrore('Non posso eliminare, ci registrazioni dopo l emissione retta');", True)
                Exit Sub
            End If
        End If



        Dim MySqlCond As String = ""

        MySqlCond = " And (Select Top 1 CentroServizio From MovimentiContabilitesta Where NumeroRegistrazione = Numero) = '" & Cmb_CServ.SelectedValue & "'"

        Dim cmdDelete2 As New OleDbCommand()
        cmdDelete2.Connection = cn
        cmdDelete2.CommandText = "Delete From MovimentiContabiliRiga Where Utente = 'EMISSIONE' AND  DataAggiornamento >= {ts '" & Format(Txt_DataEmissioneText, "yyyy-MM-dd") & " 00:00:00'}  And DataAggiornamento <= {ts '" & Format(Txt_DataEmissioneText, "yyyy-MM-dd") & " 23:59:59'} " & MySqlCond
        cmdDelete2.ExecuteNonQuery()


        MySqlCond = " And (Select Top 1 CentroServizio From MovimentiContabilitesta Where NumeroRegistrazione = CodiceDocumento) = '" & Cmb_CServ.SelectedValue & "'"


        Dim cmdDelete3 As New OleDbCommand()
        cmdDelete3.Connection = cn
        cmdDelete3.CommandText = "Delete From TabellaLegami Where Utente = 'EMISSIONE' AND  DataAggiornamento >= {ts '" & Format(Txt_DataEmissioneText, "yyyy-MM-dd") & " 00:00:00'}  And DataAggiornamento <= {ts '" & Format(Txt_DataEmissioneText, "yyyy-MM-dd") & " 23:59:59'} " & MySqlCond
        cmdDelete3.ExecuteNonQuery()


        MySqlCond = " And (Select Top 1 CentroServizio From MovimentiContabilitesta Where NumeroRegistrazione = NumeroRegistrazioneContabile) = '" & Cmb_CServ.SelectedValue & "'"


        Dim cmdDelete4 As New OleDbCommand()
        cmdDelete4.Connection = cn
        cmdDelete4.CommandText = "Delete From Scadenzario Where Utente = 'EMISSIONE' AND  DataAggiornamento >= {ts '" & Format(Txt_DataEmissioneText, "yyyy-MM-dd") & " 00:00:00'}  And DataAggiornamento <= {ts '" & Format(Txt_DataEmissioneText, "yyyy-MM-dd") & " 23:59:59'}"
        cmdDelete4.ExecuteNonQuery()



        Dim cmdUpdate As New OleDbCommand()
        cmdUpdate.Connection = cnOspiti
        cmdUpdate.CommandText = "UPDATE ADDACR Set ADDACR.NumeroRegistrazione = 0,ADDACR.Elaborato = 0 Where DataModifica >= {ts '" & Format(Txt_DataEmissioneText, "yyyy-MM-dd") & " 00:00:00'}  And DataModifica <= {ts '" & Format(Txt_DataEmissioneText, "yyyy-MM-dd") & " 23:59:59'} and CENTROSERVIZIO  = '" & Cmb_CServ.SelectedValue & "'"
        cmdUpdate.ExecuteNonQuery()



        Dim cmdDelete1 As New OleDbCommand()
        cmdDelete1.Connection = cn
        cmdDelete1.CommandText = "Delete From MovimentiContabilitesta Where Utente = 'EMISSIONE' AND  DataAggiornamento >= {ts '" & Format(Txt_DataEmissioneText, "yyyy-MM-dd") & " 00:00:00'}  And DataAggiornamento <= {ts '" & Format(Txt_DataEmissioneText, "yyyy-MM-dd") & " 23:59:59'}  and CENTROSERVIZIO  = '" & Cmb_CServ.SelectedValue & "'"
        cmdDelete1.ExecuteNonQuery()


        

        cn.Close()
        cnOspiti.Close()

        ScriptManager.RegisterClientScriptBlock(Me, Me.GetType(), "ESEGUITO", "alert('Eliminazione effettuata');", True)
    End Sub

    Protected Sub Btn_Esci_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles Btn_Esci.Click
        Response.Redirect("Menu_Strumenti.aspx")
    End Sub


    Protected Sub DD_Struttura_TextChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles DD_Struttura.TextChanged
        AggiornaCServ()
        Call EseguiJS()
    End Sub

    Private Sub AggiornaCServ()
        Dim kCsrv As New Cls_CentroServizio

        If DD_Struttura.SelectedValue = "" Then
            kCsrv.UpDateDropBox(Session("DC_OSPITE"), Cmb_CServ)
        Else
            kCsrv.UpDateDropBoxStruttura(Session("DC_OSPITE"), Cmb_CServ, DD_Struttura.SelectedValue)
        End If

    End Sub


    Private Sub EseguiJS()
        Dim MyJs As String
        MyJs = "$(document).ready(function() {"

        MyJs = MyJs & "var els = document.getElementsByTagName(""*"");"

        MyJs = MyJs & "for (var i=0;i<els.length;i++)"
        MyJs = MyJs & "if ( els[i].id ) { "
        MyJs = MyJs & " var appoggio =els[i].id; "
        MyJs = MyJs & " if (appoggio.match('Txt_Data')!= null) {  "
        MyJs = MyJs & " $(els[i]).mask(""99/99/9999"");"
        MyJs = MyJs & " $(els[i]).datepicker({ changeMonth: true,changeYear: true,  yearRange: ""1990:" & Year(Now) + 5 & """},$.datepicker.regional[""it""]);"

        MyJs = MyJs & "    }"


        MyJs = MyJs & "} "
        MyJs = MyJs & "});"

        'MyJs = "$(document).ready(function() { $('.myClass').keypress(function() { handleEnter($('.myClass').val(), true, true); } );     $('#" & Txt_ImportoPagato.ClientID & "').blur(function() { var ap = formatNumber ($('#" & Txt_ImportoPagato.ClientID & "').val(),2); $('#" & Txt_ImportoPagato.ClientID & "').val(ap); }); });"
        ScriptManager.RegisterStartupScript(Me, Me.GetType(), "visualizzaRitIm", MyJs, True)
    End Sub

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load

        Call EseguiJS()

        If Page.IsPostBack = True Then
            Exit Sub
        End If

        Dim K1 As New Cls_SqlString

        Dim Barra As New Cls_BarraSenior

        If Not IsNothing(Session("RicercaAnagraficaSQLString")) Then
            K1 = Session("RicercaAnagraficaSQLString")
        End If

        Lbl_BarraSenior.Text = Barra.CodiceBarra(Application("SENIOR"), Session("UTENTE"), Page, K1)


        Dim kVilla As New Cls_TabelleDescrittiveOspitiAccessori


        kVilla.UpDateDropBox(Session("DC_OSPITIACCESSORI"), "VIL", DD_Struttura)
        If DD_Struttura.Items.Count = 1 Then
            Call AggiornaCServ()
        End If
    End Sub
End Class
