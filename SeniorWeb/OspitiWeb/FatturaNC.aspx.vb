﻿Imports System.Web.Hosting
Imports System.Diagnostics
Imports System.ComponentModel
Imports System
Imports System.Xml
Imports System.Xml.Schema
Imports System.IO
Imports System.Data
Imports System.Data.OleDb


Partial Class OspitiWeb_FatturaNC
    Inherits System.Web.UI.Page
    Public Emr_DesExtra(40) As String
    Public Emr_ImpExtra(40) As Double
    Public Emr_GGExtra(40) As Integer
    Public Emr_CodivaExtra(40) As String
    Public Emr_CodiceExtra(40) As String
    Public Emr_ExtraMastro(40) As Long
    Public Emr_ExtraConto(40) As Long
    Public Emr_ExtraSottoConto(40) As Double

    Public Emr_P_DesExtra(10, 40) As String
    Public Emr_P_ImpExtra(10, 40) As Double
    Public Emr_P_GGExtra(10, 40) As Integer
    Public Emr_P_CodiceExtra(10, 40) As String
    Public Emr_P_CodivaExtra(10, 40) As String
    Public Emr_P_ExtraMastro(10, 40) As Long
    Public Emr_P_ExtraConto(10, 40) As Long
    Public Emr_P_ExtraSottoConto(10, 40) As Double

    Dim Tabella As New System.Data.DataTable("tabella")

    Public Function GiorniMese(ByVal Mese As Byte, ByVal Anno As Integer) As Byte
        If Mese = 1 Or Mese = 3 Or Mese = 5 Or Mese = 7 Or Mese = 8 Or _
           Mese = 10 Or Mese = 12 Then
            GiorniMese = 31
        Else
            If Mese <> 2 Then
                GiorniMese = 30
            Else
                If Day(DateSerial(Anno, Mese, 29)) = 29 Then
                    GiorniMese = 29
                Else
                    GiorniMese = 28
                End If
            End If
        End If
    End Function


    Private Function OspitePresente(ByVal Cserv As String, ByVal CodOsp As Long, ByVal Mese As Integer, ByVal Anno As Integer, ByVal ConnessioniOspiti As String) As Boolean
        Dim RsMovimenti As New ADODB.Recordset
        Dim MySql As String
        Dim OspitiDb As New ADODB.Connection

        OspitiDb.Open(ConnessioniOspiti)

        MySql = "Select top 1 * From Movimenti Where  CENTROSERVIZIO = '" & Cserv & "' And CODICEOSPITE =" & CodOsp & " And Data <= {ts '" & Format(DateSerial(Anno, Mese, GiorniMese(Mese, Anno)), "yyyy-MM-dd") & " 00:00:00'}  Order By DATA Desc,PROGRESSIVO Desc"


        RsMovimenti.Open(MySql, OspitiDb, ADODB.CursorTypeEnum.adOpenKeyset, ADODB.LockTypeEnum.adLockOptimistic)
        If RsMovimenti.EOF Then
            OspitePresente = False
        Else
            OspitePresente = False
            If MoveFromDb(RsMovimenti.Fields("TipoMov")) = "13" And Format(MoveFromDb(RsMovimenti.Fields("Data")), "yyyyMMdd") >= Format(DateSerial(Anno, Mese, 1), "yyyyMMdd") Then
                OspitePresente = True
            End If
            If MoveFromDb(RsMovimenti.Fields("TipoMov")) <> "13" Then
                OspitePresente = True
            End If
        End If
        RsMovimenti.Close()
        OspitiDb.Close()
    End Function


    Private Sub CalcolaRetta(ByRef XS As Cls_CalcoloRette)
        Dim Mese As Integer, Anno As Integer
        Dim xMese1 As Integer, xAnno1 As Integer
        Dim MyRs As New ADODB.Recordset
        Dim Errori As Boolean
        Dim GGMESE As Long
        Dim ImpRpxParente(10) As Double
        Dim GGRpxParente(10) As Long
        Dim ImpRpxOspite As Double
        Dim GGRpxOspite As Long
        Dim OspitiDb As New ADODB.Connection
        Dim RDataINI As String
        Dim RDataFine As String
        Dim StDataCond As String
        Dim tempoazzera As Long
        Dim VarCentroServizio As String
        Dim varcodiceospite As Long
        Dim OldCserv As String = ""
        Dim TipoCentro As String = ""
        Dim xMese As Integer = 0
        Dim xAnno As Integer = 0
        Dim xMese2 As Integer = 0
        Dim xAnno2 As Integer = 0
        Dim M2GiornoCalcolati As Integer = 0
        Dim M1GiornoCalcolati As Integer = 0
        Dim GiornoCalcolati As Integer = 0
        Dim OspitiCon As New OleDb.OleDbConnection


        OspitiCon.ConnectionString = Session("DC_OSPITE")
        OspitiCon.Open()


        Dim sc2 As New MSScriptControl.ScriptControl
        sc2.Language = "vbscript"

        For Par = 1 To 10
            ImpRpxParente(Par) = 0
            GGRpxParente(Par) = 0
        Next Par
        ImpRpxOspite = 0
        GGRpxOspite = 0

        sc2.Reset()

        Mese = Dd_Mese.SelectedValue
        XS.ApriDB(Session("DC_OSPITE"), Session("DC_OSPITIACCESSORI"))
        OspitiDb.Open(Session("DC_OSPITE"))
        MyRs.Open("SELECT MOVIMENTI.CENTROSERVIZIO, AnagraficaComune.CODICEOSPITE FROM AnagraficaComune INNER JOIN MOVIMENTI ON AnagraficaComune.CODICEOSPITE = MOVIMENTI.CODICEOSPITE WHERE (((AnagraficaComune.TIPOLOGIA)='O')) And AnagraficaComune.CodiceOspite = " & Val(Session("CODICEOSPITE")) & " And MOVIMENTI.TIPOMOV = '05'  AND MOVIMENTI.CENTROSERVIZIO = '" & Session("CODICESERVIZIO") & "'  GROUP BY MOVIMENTI.CENTROSERVIZIO, AnagraficaComune.CODICEOSPITE ", OspitiDb, ADODB.CursorTypeEnum.adOpenKeyset, ADODB.LockTypeEnum.adLockOptimistic)

        Anno = Txt_Anno.Text
        GGMESE = XS.GiorniMese(Mese, Anno)


        Dim f As New Cls_Parametri

        f.LeggiParametri(Session("DC_OSPITE"))

        XS.CampoParametriGIORNOUSCITA = f.GIORNOUSCITA
        XS.CampoParametriGIORNOENTRATA = f.GIORNOENTRATA
        XS.CampoParametriCAUSALEACCOGLIMENTO = f.CAUSALEACCOGLIMENTO
        XS.STRINGACONNESSIONEDB = Session("DC_OSPITE")
        XS.CaricaCausali()

        RDataINI = Format(DateSerial(Anno, Mese, 1), "yyyyMMdd")
        RDataFine = Format(DateSerial(Anno, Mese, GGMESE), "yyyyMMdd")

        StDataCond = " Data <= {ts '" & Format(DateSerial(Anno, Mese, GiorniMese(Mese, Anno)), "yyyy-MM-dd") & " 00:00:00'} AND DATA >= {ts '" & Format(DateSerial(Anno, Mese, 1), "yyyy-MM-dd") & " 00:00:00'} "

        tempoazzera = 0
        Do While Not MyRs.EOF
            VarCentroServizio = MoveFromDbWC(MyRs, "CentroServizio")
            varcodiceospite = MoveFromDbWC(MyRs, "CodiceOspite")

            Dim X As New ClsOspite
            X.Leggi(Session("DC_OSPITE"), varcodiceospite)

            Dim KCs As New Cls_DatiOspiteParenteCentroServizio

            KCs.CentroServizio = VarCentroServizio
            KCs.CodiceOspite = varcodiceospite
            KCs.CodiceParente = 0
            KCs.Leggi(Session("DC_OSPITE"))

            REM Assegna la tipologia operazione e l'aliquota prendendo quella relativa al cserv
            If KCs.CodiceOspite <> 0 Then
                X.TIPOOPERAZIONE = KCs.TipoOperazione
                X.CODICEIVA = KCs.AliquotaIva
                X.FattAnticipata = KCs.Anticipata
                X.MODALITAPAGAMENTO = KCs.ModalitaPagamento
                X.Compensazione = KCs.Compensazione
                X.SETTIMANA = KCs.Settimana
            End If

            If X.FattAnticipata = "S" Then
                xMese1 = Mese + 1
                xAnno1 = Anno
                If xMese1 = 13 Then
                    xMese1 = 1
                    xAnno1 = Anno + 1
                End If

                If OspitePresente(VarCentroServizio, varcodiceospite, xMese1, xAnno1, Session("DC_OSPITE")) = True Then


                    If Chk_SoloMese.Checked = False Then
                        If X.MesiAnticipo < 2 Then


                            Call XS.AzzeraTabella()

                            If OldCserv <> VarCentroServizio Then
                                Dim lc As New Cls_CentroServizio

                                lc.Leggi(Session("DC_OSPITE"), VarCentroServizio)
                                TipoCentro = lc.TIPOCENTROSERVIZIO
                                OldCserv = VarCentroServizio
                            End If
                            XS.DeleteChiave(VarCentroServizio, varcodiceospite, xMese1, xAnno1)

                            If TipoCentro = "D" Then
                                Call XS.PresenzeAssenzeDiurno(VarCentroServizio, varcodiceospite, xMese1, xAnno1)
                            End If
                            If TipoCentro = "A" Then
                                XS.PresenzeDomiciliare(VarCentroServizio, varcodiceospite, xMese1, xAnno1)
                            End If
                            Call XS.CreaTabellaPresenze(VarCentroServizio, varcodiceospite, xMese1, xAnno1)
                            Call XS.CreaTabellaImportoOspite(VarCentroServizio, varcodiceospite, xMese1, Anno)
                            Call XS.CreaTabellaImportoComune(VarCentroServizio, varcodiceospite, xMese1, Anno)
                            Call XS.CreaTabellaImportoJolly(VarCentroServizio, varcodiceospite, xMese1, Anno)
                            Call XS.CreaTabellaUsl(VarCentroServizio, varcodiceospite, xMese1, xAnno1)
                            Call XS.CreaTabellaImportoRetta(VarCentroServizio, varcodiceospite, xMese1, xAnno1)

                            Call XS.CreaTabellaModalita(VarCentroServizio, varcodiceospite, xMese1, xAnno1, True)
                            Call XS.CreaTabellaImportoParente(VarCentroServizio, varcodiceospite, xMese1, xAnno1)

                            Call XS.CalcolaRettaDaTabella(VarCentroServizio, varcodiceospite, xAnno1, xMese1, sc2)


                            Call XS.CreaAddebitiAcrreditiProgrammati(VarCentroServizio, varcodiceospite, xMese1, xAnno1)
                            Call XS.CreaAddebitiAcrrediti(VarCentroServizio, varcodiceospite, xMese1, xAnno1)
                            Call XS.AllineaImportiMensili(VarCentroServizio, varcodiceospite, xMese1, xAnno1)


                        End If
                        For Par = 1 To 10
                            Dim XP As New Cls_Parenti
                            XP.Leggi(Session("DC_OSPITE"), varcodiceospite, Par)
                            Dim KCsP As New Cls_DatiOspiteParenteCentroServizio

                            KCsP.CentroServizio = VarCentroServizio
                            KCsP.CodiceOspite = varcodiceospite
                            KCsP.CodiceParente = Par
                            KCsP.Leggi(Session("DC_OSPITE"))

                            REM Assegna la tipologia operazione e l'aliquota prendendo quella relativa al cserv
                            If KCsP.CodiceOspite <> 0 Then
                                XP.TIPOOPERAZIONE = KCsP.TipoOperazione
                                XP.CODICEIVA = KCsP.AliquotaIva
                                XP.FattAnticipata = KCsP.Anticipata
                                XP.MODALITAPAGAMENTO = KCsP.ModalitaPagamento
                                XP.Compensazione = KCsP.Compensazione
                            End If

                            If XP.FattAnticipata = "S" Then
                                If OspitePresente(VarCentroServizio, varcodiceospite, xMese1, xAnno1, Session("DC_OSPITE")) = True Then
                                    If X.MesiAnticipo < 2 Then
                                        Call XS.AzzeraTabella()

                                        If OldCserv <> VarCentroServizio Then
                                            Dim lc As New Cls_CentroServizio

                                            lc.Leggi(Session("DC_TABELLE"), VarCentroServizio)
                                            TipoCentro = lc.TIPOCENTROSERVIZIO

                                            OldCserv = VarCentroServizio
                                        End If

                                        XS.DeleteChiave(VarCentroServizio, varcodiceospite, Mese, Anno)

                                        If TipoCentro = "D" Then
                                            Call XS.PresenzeAssenzeDiurno(VarCentroServizio, varcodiceospite, xMese1, xAnno1)
                                        End If
                                        If TipoCentro = "A" Then
                                            XS.PresenzeDomiciliare(VarCentroServizio, varcodiceospite, xMese1, xAnno1)
                                        End If
                                        Call XS.CreaTabellaPresenze(VarCentroServizio, varcodiceospite, xMese1, xAnno1)
                                        Call XS.CreaTabellaImportoOspite(VarCentroServizio, varcodiceospite, xMese1, Anno)
                                        Call XS.CreaTabellaImportoComune(VarCentroServizio, varcodiceospite, xMese1, Anno)
                                        Call XS.CreaTabellaImportoJolly(VarCentroServizio, varcodiceospite, xMese1, Anno)
                                        Call XS.CreaTabellaUsl(VarCentroServizio, varcodiceospite, xMese1, xAnno1)
                                        Call XS.CreaTabellaImportoRetta(VarCentroServizio, varcodiceospite, xMese1, xAnno1)

                                        Call XS.CreaTabellaModalita(VarCentroServizio, varcodiceospite, xMese1, xAnno1, True)
                                        Call XS.CreaTabellaImportoParente(VarCentroServizio, varcodiceospite, xMese1, xAnno1)

                                        'Call CalcolaRettaDaTabella(Tabella, VarCentroServizio, VarCodiceOspite)
                                        Call XS.CalcolaRettaDaTabella(VarCentroServizio, varcodiceospite, xAnno1, xMese1, sc2)

                                        Call XS.CreaAddebitiAcrreditiProgrammati(VarCentroServizio, varcodiceospite, xMese1, xAnno1)

                                        Call XS.CreaAddebitiAcrrediti(VarCentroServizio, varcodiceospite, xMese1, xAnno1)
                                        Call XS.AllineaImportiMensili(VarCentroServizio, varcodiceospite, xMese1, xAnno1)



                                    End If
                                End If
                            End If
                        Next
                    End If
                End If
            End If


            Call XS.AzzeraTabella()

            If OldCserv <> VarCentroServizio Then
                Dim XCserv As New Cls_CentroServizio
                XCserv.Leggi(Session("DC_OSPITE"), VarCentroServizio)
                TipoCentro = XCserv.TIPOCENTROSERVIZIO
                OldCserv = VarCentroServizio
            End If

            XS.DeleteChiave(VarCentroServizio, varcodiceospite, Mese, Anno)

            If TipoCentro = "D" Then
                Call XS.PresenzeAssenzeDiurno(VarCentroServizio, varcodiceospite, Mese, Anno)
            End If

            If TipoCentro = "A" Then
                XS.PresenzeDomiciliare(VarCentroServizio, varcodiceospite, Mese, Anno)
            End If
            Call XS.CreaTabellaPresenze(VarCentroServizio, varcodiceospite, Mese, Anno)
            Call XS.CreaTabellaImportoOspite(VarCentroServizio, varcodiceospite, Mese, Anno)
            Call XS.CreaTabellaImportoComune(VarCentroServizio, varcodiceospite, Mese, Anno)
            Call XS.CreaTabellaImportoJolly(VarCentroServizio, varcodiceospite, Mese, Anno)
            Call XS.CreaTabellaUsl(VarCentroServizio, varcodiceospite, Mese, Anno)
            Call XS.CreaTabellaImportoRetta(VarCentroServizio, varcodiceospite, Mese, Anno)

            Call XS.CreaTabellaModalita(VarCentroServizio, varcodiceospite, Mese, Anno, True)
            Call XS.CreaTabellaImportoParente(VarCentroServizio, varcodiceospite, Mese, Anno)

            Call XS.CreaAddebitiAcrreditiProgrammati(VarCentroServizio, varcodiceospite, Mese, Anno)
            Call XS.CreaAddebitiAcrrediti(VarCentroServizio, varcodiceospite, Mese, Anno)

            'Call CalcolaRettaDaTabella(Tabella, VarCentroServizio, VarCodiceOspite)
            Call XS.CalcolaRettaDaTabella(VarCentroServizio, varcodiceospite, Anno, Mese, sc2)


            Call XS.AllineaImportiMensili(VarCentroServizio, varcodiceospite, Mese, Anno)


            xMese = Mese + 1
            xAnno = Anno
            If xMese = 13 Then
                xMese = 1
                xAnno = Anno + 1
            End If

            xMese1 = xMese + 1
            xAnno1 = xAnno
            If xMese1 = 13 Then
                xMese1 = 1
                xAnno1 = xAnno + 1
            End If

            xMese2 = xMese1 + 1
            xAnno2 = xAnno1
            If xMese2 = 13 Then
                xMese2 = 1
                xAnno2 = xAnno1 + 1
            End If



            M2GiornoCalcolati = XS.GiorniNelMese(VarCentroServizio, varcodiceospite, xMese2, xAnno2)
            M1GiornoCalcolati = XS.GiorniNelMese(VarCentroServizio, varcodiceospite, xMese1, xAnno1)
            GiornoCalcolati = XS.GiorniNelMese(VarCentroServizio, varcodiceospite, xMese, xAnno)


            If Val(DD_OspiteParenti.SelectedValue) > 0 Then
                XS.ConnessioneGenerale = Session("DC_GENERALE")
                XS.AnticipoExtraFissiParente(varcodiceospite, Val(DD_OspiteParenti.SelectedValue), VarCentroServizio, Anno, Mese, xAnno, xMese, 0, GiornoCalcolati, False, 0, False)
            Else
                XS.ConnessioneGenerale = Session("DC_GENERALE")
                XS.AnticipoExtraFissi(varcodiceospite, VarCentroServizio, Anno, Mese, xAnno, xMese, 0, GiornoCalcolati, False, False)
            End If

            'Dim Totale As New Cls_rettatotale

            'Totale.CODICEOSPITE = varcodiceospite
            'Totale.CENTROSERVIZIO = VarCentroServizio
            'Totale.RettaTotaleAData(Session("DC_OSPITE"), Totale.CODICEOSPITE, Totale.CENTROSERVIZIO, DateSerial(xAnno, xMese, 1))

            'If Year(Totale.Data) > 1900 Then

            '    Dim Extra As New Cls_ExtraFisso

            '    Extra.CENTROSERVIZIO = Totale.CENTROSERVIZIO
            '    Extra.CODICEOSPITE = Totale.CODICEOSPITE
            '    Extra.Data = Totale.Data
            '    Extra.UltimaDataAData(Session("DC_OSPITE"))

            '    Dim TipoExtra As New Cls_TipoExtraFisso

            '    TipoExtra.CODICEEXTRA = Extra.CODICEEXTRA
            '    TipoExtra.Leggi(Session("DC_OSPITE"))

            '    If X.FattAnticipata = "S" Then

            '        If TipoExtra.Ripartizione = "O" And TipoExtra.Anticipato = "S" Then


            '            If TipoExtra.TipoImporto = "G" Then

            '                Dim VerificaExtra As Double
            '                Dim cmdRpxO1 As New OleDbCommand()
            '                cmdRpxO1.CommandText = "Select * From RetteOspite Where CodiceOspite = " & varcodiceospite & " And CentroServizio = '" & VarCentroServizio & "' And  Elemento Like 'E%' And MeseCompetenza = " & Mese & " And AnnoCompetenza = " & Anno
            '                cmdRpxO1.Connection = OspitiCon
            '                Dim RDRettaRpxO1 As OleDbDataReader = cmdRpxO1.ExecuteReader()
            '                If RDRettaRpxO1.Read() Then
            '                    VerificaExtra = campodb(RDRettaRpxO1.Item("Importo"))
            '                End If
            '                RDRettaRpxO1.Close()

            '                If VerificaExtra <> XS.ImpExtraOspite(0) + XS.NonImpExtraOspite(0) Then
            '                    If XS.NonImpExtraOspite(0) = 0 And XS.ImpExtraOspite(0) = 0 Then
            '                        Dim KStato As New Cls_StatoAuto

            '                        KStato.CODICEOSPITE = Totale.CODICEOSPITE
            '                        KStato.CENTROSERVIZIO = Totale.CENTROSERVIZIO
            '                        KStato.UltimaData(Session("DC_OSPITE"), KStato.CODICEOSPITE, KStato.CENTROSERVIZIO)
            '                        If KStato.STATOAUTO = "A" Then
            '                            XS.ImpExtraOspite(0) = VerificaExtra * -1
            '                        Else
            '                            XS.NonImpExtraOspite(0) = VerificaExtra * -1
            '                        End If
            '                    Else
            '                        If XS.ImpExtraOspite(0) > 0 Then
            '                            XS.ImpExtraOspite(0) = XS.ImpExtraOspite(0) + XS.ImpExtraOspite(1) - VerificaExtra
            '                        End If
            '                        If XS.NonImpExtraOspite(0) > 0 Then
            '                            XS.NonImpExtraOspite(0) = XS.NonImpExtraOspite(0) + XS.NonImpExtraOspite(1) - VerificaExtra
            '                        End If
            '                        XS.ImpExtraOspite(1) = 0
            '                        XS.NonImpExtraOspite(1) = 0
            '                    End If
            '                Else
            '                    XS.ImpExtraOspite(0) = 0
            '                    XS.NonImpExtraOspite(0) = 0
            '                    XS.ImpExtraOspite(1) = 0
            '                    XS.NonImpExtraOspite(1) = 0
            '                End If
            '            End If
            '            If TipoExtra.TipoImporto = "M" Then
            '                Dim VerificaExtra As Double
            '                Dim cmdRpxO1 As New OleDbCommand()
            '                cmdRpxO1.CommandText = "Select * From RetteOspite Where CodiceOspite = " & varcodiceospite & " And CentroServizio = '" & VarCentroServizio & "' And  Elemento Like 'E%' And MeseCompetenza = " & Mese & " And AnnoCompetenza = " & Anno
            '                cmdRpxO1.Connection = OspitiCon
            '                Dim RDRettaRpxO1 As OleDbDataReader = cmdRpxO1.ExecuteReader()
            '                If RDRettaRpxO1.Read() Then
            '                    VerificaExtra = campodb(RDRettaRpxO1.Item("Importo"))
            '                End If
            '                RDRettaRpxO1.Close()
            '                If VerificaExtra > 0 Then
            '                    XS.ImpExtraOspite(0) = 0
            '                    XS.NonImpExtraOspite(0) = 0
            '                End If
            '            End If
            '        End If
            '    End If


            'If Val(DD_OspiteParenti.SelectedValue) > 0 Then
            '    Dim Par As Integer
            '    Par = Val(DD_OspiteParenti.SelectedValue)
            '    Dim XKPar As New Cls_Parenti
            '    XKPar.CodiceOspite = X.CodiceOspite
            '    XKPar.CodiceParente = Par
            '    XKPar.Leggi(Session("DC_OSPITE"), XKPar.CodiceOspite, XKPar.CodiceParente)


            '    Dim KCsP As New Cls_DatiOspiteParenteCentroServizio

            '    KCsP.CentroServizio = VarCentroServizio
            '    KCsP.CodiceOspite = varcodiceospite
            '    KCsP.CodiceParente = Par
            '    KCsP.Leggi(Session("DC_OSPITE"))

            '    REM Assegna la tipologia operazione e l'aliquota prendendo quella relativa al cserv
            '    If KCsP.CodiceOspite <> 0 Then
            '        XKPar.TIPOOPERAZIONE = KCsP.TipoOperazione
            '        XKPar.CODICEIVA = KCsP.AliquotaIva
            '        XKPar.FattAnticipata = KCsP.Anticipata
            '        XKPar.MODALITAPAGAMENTO = KCs.ModalitaPagamento
            '        XKPar.Compensazione = KCsP.Compensazione
            '    End If


            '    If XKPar.FattAnticipata = "S" Then
            '        If TipoExtra.Ripartizione = "P" And TipoExtra.Anticipato = "S" Then


            '            If TipoExtra.TipoImporto = "G" Then

            '                Dim VerificaExtra As Double
            '                Dim cmdRpxO1 As New OleDbCommand()
            '                cmdRpxO1.CommandText = "Select * From RETTEPARENTE Where CodiceOspite = " & varcodiceospite & " And CentroServizio = '" & VarCentroServizio & "' And  CODICEPARENTE =  " & Par & " And Elemento Like 'E%' And MeseCompetenza = " & Mese & " And AnnoCompetenza = " & Anno
            '                cmdRpxO1.Connection = OspitiCon
            '                Dim RDRettaRpxO1 As OleDbDataReader = cmdRpxO1.ExecuteReader()
            '                If RDRettaRpxO1.Read() Then
            '                    VerificaExtra = campodb(RDRettaRpxO1.Item("Importo"))
            '                End If
            '                RDRettaRpxO1.Close()

            '                If VerificaExtra <> XS.ImpExtraParente(Par, 0) + XS.NonImpExtraParente(Par, 0) + XS.ImpExtraParente(Par, 1) + XS.NonImpExtraParente(Par, 1) Then

            '                    If XS.ImpExtraParente(Par, 0) = 0 And XS.NonImpExtraParente(Par, 0) = 0 And XS.ImpExtraParente(Par, 1) = 0 And XS.NonImpExtraParente(Par, 1) = 0 Then
            '                        Dim KStato As New Cls_StatoAuto

            '                        KStato.CODICEOSPITE = Totale.CODICEOSPITE
            '                        KStato.CENTROSERVIZIO = Totale.CENTROSERVIZIO
            '                        KStato.UltimaData(Session("DC_OSPITE"), KStato.CODICEOSPITE, KStato.CENTROSERVIZIO)
            '                        If KStato.STATOAUTO = "A" Then
            '                            XS.ImpExtraParente(Par, 0) = VerificaExtra * -1
            '                        Else
            '                            XS.NonImpExtraParente(Par, 0) = VerificaExtra * -1
            '                        End If
            '                    Else

            '                        If XS.ImpExtraParente(Par, 0) > 0 Then
            '                            XS.ImpExtraParente(Par, 0) = XS.ImpExtraParente(Par, 0) + XS.ImpExtraParente(Par, 1) - VerificaExtra
            '                        End If
            '                        If XS.NonImpExtraParente(Par, 0) > 0 Then
            '                            XS.NonImpExtraParente(Par, 0) = XS.NonImpExtraParente(Par, 0) + XS.NonImpExtraParente(Par, 1) - VerificaExtra
            '                        End If
            '                        If XS.ImpExtraParente(Par, 1) > 0 Then
            '                            XS.ImpExtraParente(Par, 1) = XS.ImpExtraParente(Par, 0) + XS.ImpExtraParente(Par, 1) - VerificaExtra
            '                        End If
            '                        If XS.NonImpExtraParente(Par, 1) > 0 Then
            '                            XS.NonImpExtraParente(Par, 1) = XS.NonImpExtraParente(Par, 0) + XS.NonImpExtraParente(Par, 1) - VerificaExtra
            '                        End If

            '                    End If

            '                Else
            '                    XS.ImpExtraParente(Par, 0) = 0
            '                    XS.NonImpExtraParente(Par, 0) = 0
            '                    XS.ImpExtraParente(Par, 1) = 0
            '                    XS.NonImpExtraParente(Par, 1) = 0
            '                    XS.ImpExtraParente(Par, 2) = 0
            '                    XS.NonImpExtraParente(Par, 2) = 0
            '                End If
            '            End If
            '            If TipoExtra.TipoImporto = "M" Then
            '                Dim VerificaExtra As Double
            '                Dim cmdRpxO1 As New OleDbCommand()
            '                cmdRpxO1.CommandText = "Select * From RETTEPARENTE Where CodiceOspite = " & varcodiceospite & " And CentroServizio = '" & VarCentroServizio & "' And  CODICEPARENTE =  " & Par & " And Elemento Like 'E%' And MeseCompetenza = " & Mese & " And AnnoCompetenza = " & Anno
            '                cmdRpxO1.Connection = OspitiCon
            '                Dim RDRettaRpxO1 As OleDbDataReader = cmdRpxO1.ExecuteReader()
            '                If RDRettaRpxO1.Read() Then
            '                    VerificaExtra = campodb(RDRettaRpxO1.Item("Importo"))
            '                End If
            '                RDRettaRpxO1.Close()
            '                If VerificaExtra > 0 Then
            '                    XS.ImpExtraParente(Par, 0) = 0
            '                    XS.NonImpExtraParente(Par, 0) = 0
            '                    XS.ImpExtraParente(Par, 1) = 0
            '                    XS.NonImpExtraParente(Par, 1) = 0
            '                End If
            '            End If
            '        End If
            '    End If
            'End If
            'End If
            MyRs.MoveNext()
        Loop
        OspitiDb.Close()
        XS.ChiudiDB()
        OspitiCon.Close()
    End Sub

    Function campodbd(ByVal oggetto As Object) As Date
        If IsDBNull(oggetto) Then
            Return Nothing
        Else
            Return oggetto
        End If
    End Function


    Function campodb(ByVal oggetto As Object) As String
        If IsDBNull(oggetto) Then
            Return ""
        Else
            Return oggetto
        End If
    End Function

    Function campodbn(ByVal oggetto As Object) As Double
        If IsDBNull(oggetto) Then
            Return 0
        Else
            Return oggetto
        End If
    End Function



    Protected Sub EstraiFTNC()

        If Val(Txt_Anno.Text) = 0 Then Exit Sub
        If Not IsDate(Txt_DataRegistrazione.Text) Then
            Exit Sub
        End If

        Dim ConnectionString As String = Session("DC_OSPITE")

        Dim GiorniPres As Long
        Dim NonGiorniPres As Long
        Dim i As Long

        Dim MyRs As New ADODB.Recordset
        Dim SalvaImportoDocumento As Double
        Dim SalvaImportoDocumento2 As Double
        Dim SalvaGiorniDocumento As Integer
        Dim MyRsSrc As New ADODB.Recordset
        Dim TotRetta As Double
        Dim xMese As Integer
        Dim xAnno As Integer
        Dim Grid As New Cls_Grid
        Dim Mese As Integer
        Dim ImportoRpx As Double
        Dim GiornoCalcolati As Integer
        Dim XS As New Cls_CalcoloRette
        Dim XSPX As New Cls_CalcoloRette
        Dim XGiorniAssenza As Long
        Dim Calcola As Double
        Dim Calcola2 As Double
        Dim sc2 As New MSScriptControl.ScriptControl
        Dim GGRpxOspite As Long
        Dim ImpRpxOspite As Double
        Dim ImpRpxOspite2 As Double

        Dim OspitiAccessoriCon As New OleDbConnection




        OspitiAccessoriCon = New Data.OleDb.OleDbConnection(Session("DC_OSPITIACCESSORI"))
        OspitiAccessoriCon.Open()

        Grid.Rows = 1

        sc2.Language = "vbscript"

        Grid.Col = 1 : Grid.Text("Quota a carico del ")
        Grid.Col = 2 : Grid.Text("Importo")
        Grid.Col = 3 : Grid.Text("Giorni")
        Grid.Col = 4 : Grid.Text("Importo Mese Successivo")
        Grid.Col = 5 : Grid.Text("Giorni Mese Successivo")
        Grid.Col = 6 : Grid.Text("Importo 2")
        Grid.Col = 7 : Grid.Text("Importo 2 Mese Successivo")
        Grid.Rows = 0


        Mese = Dd_Mese.SelectedValue


        If Mese = 12 Then
            xMese = 1
            xAnno = Val(Txt_Anno.Text) + 1
        Else
            xMese = Mese + 1
            xAnno = Val(Txt_Anno.Text)
        End If

        Call CalcolaRetta(XS)
        XS.ApriDB(Session("DC_OSPITE"), Session("DC_OSPITIACCESSORI"))
        XSPX.ApriDB(Session("DC_OSPITE"), Session("DC_OSPITIACCESSORI"))

        XSPX.STRINGACONNESSIONEDB = Session("DC_OSPITE")

        ImportoRpx = 0
        Dim CampoOspite As New ClsOspite

        CampoOspite.Leggi(Session("DC_OSPITE"), Session("CODICEOSPITE"))
        Dim KCs As New Cls_DatiOspiteParenteCentroServizio

        KCs.CentroServizio = Session("CODICESERVIZIO")
        KCs.CodiceOspite = Session("CODICEOSPITE")
        KCs.CodiceParente = 0
        KCs.Leggi(Session("DC_OSPITE"))

        REM Assegna la tipologia operazione e l'aliquota prendendo quella relativa al cserv
        If KCs.CodiceOspite <> 0 Then
            CampoOspite.TIPOOPERAZIONE = KCs.TipoOperazione
            CampoOspite.CODICEIVA = KCs.AliquotaIva
            CampoOspite.FattAnticipata = KCs.Anticipata
            CampoOspite.MODALITAPAGAMENTO = KCs.ModalitaPagamento
            CampoOspite.Compensazione = KCs.Compensazione
            CampoOspite.SETTIMANA = KCs.Settimana
        End If


        If CampoOspite.FattAnticipata = "S" Then
            GiornoCalcolati = XSPX.GiorniNelMese(Session("CODICESERVIZIO"), Session("CODICEOSPITE"), xMese, xAnno)

            Call XSPX.CreaTabellaPresenze(Session("CODICESERVIZIO"), Session("CODICEOSPITE"), xMese, xAnno)
            XGiorniAssenza = 0
            Calcola = 0
            For i = 1 To GiornoCalcolati
                If XSPX.MyTabCausale(i) <> "*" And XSPX.MyTabCausale(i) <> "" Then
                    XGiorniAssenza = XGiorniAssenza + 1
                    Calcola = Calcola + XSPX.QuoteGiornaliereAssenzaCausale(Session("CODICESERVIZIO"), Session("CODICEOSPITE"), "O1", 0, sc2, XSPX.MyTabCausale(i), XGiorniAssenza, DateSerial(xAnno, xMese, i))
                    Calcola2 = Calcola2 + XSPX.QuoteGiornaliereAssenzaCausale(Session("CODICESERVIZIO"), Session("CODICEOSPITE"), "O2", 0, sc2, XSPX.MyTabCausale(i), XGiorniAssenza, DateSerial(xAnno, xMese, i))
                Else
                    Calcola = Calcola + XSPX.QuoteGiornaliere(Session("CODICESERVIZIO"), Session("CODICEOSPITE"), "O1", 0, DateSerial(xAnno, xMese, i))
                    Calcola2 = Calcola2 + XSPX.QuoteGiornaliere(Session("CODICESERVIZIO"), Session("CODICEOSPITE"), "O2", 0, DateSerial(xAnno, xMese, i))
                End If
            Next i

            If XS.QuoteMensile(Session("CODICESERVIZIO"), Session("CODICEOSPITE"), "O1", 0, DateSerial(xAnno, xMese, i - 1)) > 0 And Calcola > 0 Then
                Calcola = XS.QuoteMensile(Session("CODICESERVIZIO"), Session("CODICEOSPITE"), "O1", 0, DateSerial(xAnno, xMese, i - 1))
                Calcola2 = XS.QuoteMensile(Session("CODICESERVIZIO"), Session("CODICEOSPITE"), "O2", 0, DateSerial(xAnno, xMese, i - 1))
            Else
                If XS.ImportoMensileOspite = 0 And Calcola > 0 Then
                    If XS.QuoteMensile(Session("CODICESERVIZIO"), Session("CODICEOSPITE"), "O1", 0, DateSerial(xAnno, xMese, i)) > 0 Then
                        Calcola = XS.QuoteMensile(Session("CODICESERVIZIO"), Session("CODICEOSPITE"), "O1", 0, DateSerial(xAnno, xMese, i))
                        Calcola2 = XS.QuoteMensile(Session("CODICESERVIZIO"), Session("CODICEOSPITE"), "O2", 0, DateSerial(xAnno, xMese, i))
                    End If
                End If
            End If
            GGRpxOspite = GiornoCalcolati
            ImpRpxOspite = Modulo.MathRound(Calcola, 2)
            ImpRpxOspite2 = Modulo.MathRound(Calcola2, 2)
        End If



        Dim Mastro As Long
        Dim Conto As Long
        Dim Sottoconto As Long
        Dim CentroServizio As String
        Dim MyCs As New Cls_CentroServizio
        Dim Anno As Long
        Dim PredxMese As Integer
        Dim PredxAnno As Integer
        Dim MeseDopoMese As Integer
        Dim MeseDopoAnno As Integer
        Dim AnnoRiferimento As Integer
        Dim MeseRiferimento As Integer
        Dim NumeroRegOccorre As Integer
        Dim Tipologia As String
        Dim MySql As String
        Dim ConAddebiti As Boolean
        Dim SoloRigaRetta As Integer
        Dim GeneraleDb As New ADODB.Connection
        Dim OspitiDb As New ADODB.Connection
        Dim xImporto As Double

        GeneraleDb.Open(Session("DC_GENERALE"))
        OspitiDb.Open(Session("DC_OSPITE"))

        MyCs.Leggi(Session("DC_OSPITE"), Session("CODICESERVIZIO"))


        Mastro = MyCs.MASTRO
        Conto = MyCs.CONTO
        Sottoconto = Session("CODICEOSPITE") * 100
        CentroServizio = Session("CODICESERVIZIO")


        Anno = Txt_Anno.Text
        Mese = Dd_Mese.SelectedValue


        PredxMese = Mese
        PredxAnno = Txt_Anno.Text
        If PredxMese = 1 Then
            PredxAnno = PredxAnno - 1
        Else
            PredxMese = PredxMese - 1
        End If


        MeseDopoMese = Mese
        MeseDopoAnno = Txt_Anno.Text
        If MeseDopoMese = 12 Then
            MeseDopoAnno = MeseDopoAnno + 1
            MeseDopoMese = 1
        Else
            MeseDopoMese = MeseDopoMese + 1
        End If




        AnnoRiferimento = Txt_Anno.Text
        MeseRiferimento = Mese
        NumeroRegOccorre = 0
        SalvaImportoDocumento = 0
        SalvaGiorniDocumento = 0
        Tipologia = ""
        Dim XOsp As New ClsOspite

        XOsp.Leggi(Session("DC_OSPITE"), Session("CODICEOSPITE"))

        Dim KCs2 As New Cls_DatiOspiteParenteCentroServizio

        KCs2.CentroServizio = CentroServizio
        KCs2.CodiceOspite = Session("CODICEOSPITE")
        KCs2.CodiceParente = 0
        KCs2.Leggi(Session("DC_OSPITE"))

        REM Assegna la tipologia operazione e l'aliquota prendendo quella relativa al cserv
        If KCs2.CodiceOspite <> 0 Then
            XOsp.TIPOOPERAZIONE = KCs2.TipoOperazione
            XOsp.CODICEIVA = KCs2.AliquotaIva
            XOsp.FattAnticipata = KCs2.Anticipata
            XOsp.MODALITAPAGAMENTO = KCs2.ModalitaPagamento
            XOsp.Compensazione = KCs2.Compensazione
            XOsp.SETTIMANA = KCs2.Settimana
        End If

        If XOsp.FattAnticipata = "S" Then

            MySql = "SELECT MovimentiContabiliTesta.FatturaDiAnticipo,NumeroRegistrazione " & _
                    " FROM MovimentiContabiliRiga INNER JOIN MovimentiContabiliTesta " & _
                    " ON MovimentiContabiliRiga.Numero = MovimentiContabiliTesta.NumeroRegistrazione " & _
                    " WHERE MovimentiContabiliRiga.MastroPartita = " & Mastro & _
                    " AND MovimentiContabiliRiga.ContoPartita = " & Conto & _
                    " AND MovimentiContabiliRiga.SottocontoPartita = " & Sottoconto & _
                    " And MovimentiContabiliTesta.CentroServizio = '" & CentroServizio & "'"
            If Trim(Tipologia) <> "" Then
                MySql = MySql & " AND MovimentiContabiliTesta.Tipologia = '" & Tipologia & "'"
            End If


            ConAddebiti = False
            NumeroRegOccorre = 0
            SalvaImportoDocumento = 0
            SalvaGiorniDocumento = 0
            SalvaImportoDocumento2 = 0

            SoloRigaRetta = 0
            MyRs.Open(MySql, GeneraleDb, ADODB.CursorTypeEnum.adOpenKeyset, ADODB.LockTypeEnum.adLockOptimistic)
            Do While Not MyRs.EOF

                MyRsSrc.Open("Select * From MovimentiContabiliRiga Where Numero = " & MoveFromDb(MyRs.Fields("NumeroRegistrazione")) & " And (RigaDaCausale = 3 OR RigaDaCausale =5)  And ((AnnoRiferimento =  " & Anno & " And MeseRiferimento = " & Mese & ") OR (AnnoRiferimento =  " & MeseDopoAnno & " And MeseRiferimento = " & MeseDopoMese & ")) ", GeneraleDb, ADODB.CursorTypeEnum.adOpenKeyset, ADODB.LockTypeEnum.adLockOptimistic)
                Do While Not MyRsSrc.EOF

                    If Val(MoveFromDb(MyRsSrc.Fields("Quantita"))) = 0 Then
                        ConAddebiti = True
                        NumeroRegOccorre = MoveFromDb(MyRsSrc.Fields("Numero"))
                    End If
                    If MoveFromDb(MyRsSrc.Fields("DareAvere")) = "A" Then
                        SalvaImportoDocumento = SalvaImportoDocumento + MoveFromDb(MyRsSrc.Fields("Importo"))
                        SalvaGiorniDocumento = SalvaGiorniDocumento + MoveFromDb(MyRsSrc.Fields("Quantita"))
                    Else
                        SalvaImportoDocumento = SalvaImportoDocumento - MoveFromDb(MyRsSrc.Fields("Importo"))
                        SalvaGiorniDocumento = SalvaGiorniDocumento - MoveFromDb(MyRsSrc.Fields("Quantita"))
                    End If
                    If Val(MoveFromDb(MyRsSrc.Fields("Quantita"))) = 0 Then
                        SoloRigaRetta = 1
                    End If
                    'ImportoDocumento(MoveFromDb(MyRs.Fields("NumeroRegistrazione))
                    MyRsSrc.MoveNext()
                Loop
                MyRsSrc.Close()


                MyRs.MoveNext()
            Loop
            MyRs.Close()


            MyRs.Open(MySql, GeneraleDb, ADODB.CursorTypeEnum.adOpenKeyset, ADODB.LockTypeEnum.adLockOptimistic)
            Do While Not MyRs.EOF

                MyRsSrc.Open("Select * From MovimentiContabiliRiga Where Numero = " & MoveFromDb(MyRs.Fields("NumeroRegistrazione")) & " And RigaDaCausale = 13  And ((AnnoRiferimento =  " & Anno & " And MeseRiferimento = " & Mese & ") OR (AnnoRiferimento =  " & MeseDopoAnno & " And MeseRiferimento = " & MeseDopoMese & ")) ", GeneraleDb, ADODB.CursorTypeEnum.adOpenKeyset, ADODB.LockTypeEnum.adLockOptimistic)
                Do While Not MyRsSrc.EOF

                    If MoveFromDb(MyRsSrc.Fields("DareAvere")) = "A" Then
                        SalvaImportoDocumento2 = SalvaImportoDocumento2 + MoveFromDb(MyRsSrc.Fields("Importo"))
                    Else
                        SalvaImportoDocumento2 = SalvaImportoDocumento2 - MoveFromDb(MyRsSrc.Fields("Importo"))
                    End If

                    MyRsSrc.MoveNext()
                Loop
                MyRsSrc.Close()
                MyRs.MoveNext()
            Loop
            MyRs.Close()
        End If


        Dim ImportoAddebiti As Double = 0
        xImporto = 0
        MyRsSrc.Open("SelecT * FROM ADDACR WHERE CENTROSERVIZIO = '" & Session("CODICESERVIZIO") & "' AND CODICEOSPITE  = " & Session("CODICEOSPITE") & " And month(DATA) = " & Mese & " And Year(DATA) = " & Anno & " And (Elaborato Is null or Elaborato =0)", OspitiDb, ADODB.CursorTypeEnum.adOpenKeyset, ADODB.LockTypeEnum.adLockPessimistic)

        Do While Not MyRsSrc.EOF
            If MoveFromDb(MyRsSrc.Fields("TipoMov")) = "AD" And MoveFromDb(MyRsSrc.Fields("Riferimento")) = "O" Then
                xImporto = xImporto + MoveFromDb(MyRsSrc.Fields("Importo"))
                ImportoAddebiti = ImportoAddebiti + MoveFromDb(MyRsSrc.Fields("Importo"))
            End If
            If MoveFromDb(MyRsSrc.Fields("TipoMov")) = "AC" And MoveFromDb(MyRsSrc.Fields("Riferimento")) = "O" Then
                xImporto = xImporto - MoveFromDb(MyRsSrc.Fields("Importo"))
                ImportoAddebiti = ImportoAddebiti - MoveFromDb(MyRsSrc.Fields("Importo"))
            End If

            MyRsSrc.MoveNext()
        Loop
        MyRsSrc.Close()




        xImporto = xImporto + XS.ImpExtrOspMan1 + XS.ImpExtrOspMan2 + XS.ImpExtrOspMan3 + XS.ImpExtrOspMan4


        Session("IMPORTOOSPMAN1") = XS.ImpExtrOspMan1
        Session("CODICEOSPMAN1") = XS.TipoAddExtrOspMan1

        Session("IMPORTOOSPMAN2") = XS.ImpExtrOspMan2
        Session("CODICEOSPMAN2") = XS.TipoAddExtrOspMan2

        Session("IMPORTOOSPMAN2") = XS.ImpExtrOspMan3
        Session("CODICEOSPMAN2") = XS.TipoAddExtrOspMan3

        Session("IMPORTOOSPMAN2") = XS.ImpExtrOspMan4
        Session("CODICEOSPMAN2") = XS.TipoAddExtrOspMan4


        Dim xImportoAddPrec As Long
        Dim SoloRetta As Double
        Dim SoloRetta2 As Double
        Dim gaientrato As Boolean

        xImportoAddPrec = 0
        MyRsSrc.Open("SelecT * FROM ADDACR WHERE NumeroRegistrazione = " & NumeroRegOccorre & " And CENTROSERVIZIO = '" & Session("CODICESERVIZIO") & "' AND CODICEOSPITE  = " & Session("CODICEOSPITE") & " And ((month(DATA) = " & PredxMese & " And Year(DATA) = " & PredxAnno & ") OR  (month(DATA) = " & Mese & " And Year(DATA) = " & Anno & ")) And (Elaborato =1)", OspitiDb, ADODB.CursorTypeEnum.adOpenKeyset, ADODB.LockTypeEnum.adLockPessimistic)

        Do While Not MyRsSrc.EOF
            If MoveFromDb(MyRsSrc.Fields("TipoMov")) = "AD" And MoveFromDb(MyRsSrc.Fields("Riferimento")) = "O" Then
                xImportoAddPrec = xImportoAddPrec + MoveFromDb(MyRsSrc.Fields("Importo"))
            End If
            If MoveFromDb(MyRsSrc.Fields("TipoMov")) = "AC" And MoveFromDb(MyRsSrc.Fields("Riferimento")) = "O" Then
                xImportoAddPrec = xImportoAddPrec - MoveFromDb(MyRsSrc.Fields("Importo"))
            End If

            MyRsSrc.MoveNext()
        Loop
        MyRsSrc.Close()





        If Chk_SoloMese.Checked = True Then
            TotRetta = XS.NonImportoPresOspite + XS.NonImportoAssOspite + XS.ImportoPresOspite + XS.ImportoAssOspite + xImporto '+ xImportoAddPrec
            SoloRetta = XS.NonImportoPresOspite + XS.NonImportoAssOspite + XS.ImportoPresOspite + XS.ImportoAssOspite
            SoloRetta2 = XS.NonImportoPresOspite2 + XS.NonImportoAssOspite2 + XS.ImportoPresOspite2 + XS.ImportoAssOspite2
            GGRpxOspite = 0
            ImpRpxOspite = 0
            ImpRpxOspite2 = 0
        Else
            TotRetta = XS.NonImportoPresOspite + XS.NonImportoAssOspite + XS.ImportoPresOspite + XS.ImportoAssOspite + ImpRpxOspite + xImporto '+ xImportoAddPrec
            SoloRetta = XS.NonImportoPresOspite + XS.NonImportoAssOspite + XS.ImportoPresOspite + XS.ImportoAssOspite + ImpRpxOspite
            SoloRetta2 = XS.NonImportoPresOspite2 + XS.NonImportoAssOspite2 + XS.ImportoPresOspite2 + XS.ImportoAssOspite2 + ImpRpxOspite2
        End If


        Dim AnagOspite As New ClsOspite

        AnagOspite.CodiceOspite = Session("CODICEOSPITE")
        AnagOspite.Leggi(Session("DC_OSPITE"), AnagOspite.CodiceOspite)

        Dim MDatiOspite As New Cls_DatiOspiteParenteCentroServizio

        MDatiOspite.CentroServizio = Session("CODICESERVIZIO")
        MDatiOspite.CodiceOspite = AnagOspite.CodiceOspite
        MDatiOspite.CodiceParente = 0
        MDatiOspite.Leggi(Session("DC_OSPITE"))

        If MDatiOspite.TipoOperazione <> "" Then
            AnagOspite.TIPOOPERAZIONE = MDatiOspite.TipoOperazione
            AnagOspite.CODICEIVA = MDatiOspite.AliquotaIva
        End If



        Dim XOperazione As New Cls_TipoOperazione

        XOperazione.Leggi(Session("DC_OSPITE"), AnagOspite.TIPOOPERAZIONE)

        If XOperazione.SCORPORAIVA = 1 Then
            Dim MyIva As New Cls_IVA

            MyIva.Codice = AnagOspite.CODICEIVA
            MyIva.Leggi(Session("DC_TABELLE"), MyIva.Codice)

            'TotRetta = Math.Round(TotRetta * 100 / (100 + (MyIva.Aliquota * 100)), 2)
            'SoloRetta2 = Math.Round(SoloRetta2 * 100 / (100 + (MyIva.Aliquota * 100)), 2)

            SalvaImportoDocumento = Math.Round(SalvaImportoDocumento + (SalvaImportoDocumento * MyIva.Aliquota), 2)
            SalvaImportoDocumento2 = Math.Round(SalvaImportoDocumento2 + (SalvaImportoDocumento2 * MyIva.Aliquota), 2)
        End If



        If SoloRigaRetta = 1 Then
            SalvaImportoDocumento = SalvaImportoDocumento - xImportoAddPrec
        Else
            SalvaImportoDocumento = SalvaImportoDocumento
        End If

        Dim Totale As Double = 0


        For i = 0 To 10
            Dim FlagAggiungiExtra As Boolean = True

            If RdRetta.Visible = True Then
                Dim DecExtr As New Cls_TipoExtraFisso

                DecExtr.CODICEEXTRA = XS.MyTabCodiceExtrOspite(i)
                DecExtr.Leggi(Session("DC_OSPITE"))
                FlagAggiungiExtra = False
                If DecExtr.FuoriRetta = 1 And RdFuoriRetta.Checked = True Then
                    FlagAggiungiExtra = True
                End If
                If DecExtr.FuoriRetta = 0 And RdRetta.Checked = True Then
                    FlagAggiungiExtra = True
                End If
            End If

            If XS.ImpExtraOspite(i) <> 0 And FlagAggiungiExtra Then
                Emr_DesExtra(i) = CampoExtraFisso(XS.MyTabCodiceExtrOspite(i), "Descrizione")
                Emr_ImpExtra(i) = XS.ImpExtraOspite(i)
                Emr_GGExtra(i) = XS.MyTabCodiceExtrOspiteGIORNI(i)
                Totale = Totale + XS.ImpExtraOspite(i)
                Emr_CodivaExtra(i) = CampoExtraFisso(XS.MyTabCodiceExtrOspite(i), "CodiceIva")
                Emr_CodiceExtra(i) = XS.MyTabCodiceExtrOspite(i)
                Emr_ExtraMastro(i) = CampoExtraFisso(XS.MyTabCodiceExtrOspite(i), "Mastro")
                Emr_ExtraConto(i) = CampoExtraFisso(XS.MyTabCodiceExtrOspite(i), "Conto")
                Emr_ExtraSottoConto(i) = CampoExtraFisso(XS.MyTabCodiceExtrOspite(i), "SottoConto")
            End If
            If XS.NonImpExtraOspite(i) <> 0 And FlagAggiungiExtra Then

                Emr_DesExtra(i) = CampoExtraFisso(XS.MyTabCodiceExtrOspite(i), "Descrizione")
                Emr_ImpExtra(i) = XS.NonImpExtraOspite(i)
                Emr_GGExtra(i) = XS.MyTabCodiceExtrOspiteGIORNI(i)
                Totale = Totale + XS.NonImpExtraOspite(i)
                Emr_CodivaExtra(i) = CampoExtraFisso(XS.MyTabCodiceExtrOspite(i), "CodiceIva")
                Emr_CodiceExtra(i) = XS.MyTabCodiceExtrOspite(i)
                Emr_ExtraMastro(i) = CampoExtraFisso(XS.MyTabCodiceExtrOspite(i), "Mastro")
                Emr_ExtraConto(i) = CampoExtraFisso(XS.MyTabCodiceExtrOspite(i), "Conto")
                Emr_ExtraSottoConto(i) = CampoExtraFisso(XS.MyTabCodiceExtrOspite(i), "SottoConto")
            End If

        Next
        Session("Emr_ImpExtra") = Emr_ImpExtra
        Session("Emr_GGExtra") = Emr_GGExtra
        Session("Emr_CodivaExtra") = Emr_CodivaExtra
        Session("Emr_CodiceExtra") = Emr_CodiceExtra
        Session("Emr_ExtraMastro") = Emr_ExtraMastro
        Session("Emr_ExtraConto") = Emr_ExtraConto
        Session("Emr_ExtraSottoConto") = Emr_ExtraSottoConto
        Session("Emr_DesExtra") = Emr_DesExtra




        If Val(DD_OspiteParenti.SelectedValue) = 0 Then
            If Modulo.MathRound(SalvaImportoDocumento + SalvaImportoDocumento2, 2) <> Modulo.MathRound(TotRetta + SoloRetta2 + Totale, 2) Then

                If SalvaImportoDocumento + SalvaImportoDocumento2 > TotRetta + SoloRetta2 Then

                    If Modulo.MathRound(xImporto, 2) > Modulo.MathRound(TotRetta - SalvaImportoDocumento, 2) Then


                        ' modifica per variazione retta
                        If Modulo.MathRound((TotRetta + SoloRetta2) - (SalvaImportoDocumento + SalvaImportoDocumento2), 2) - xImporto > 0 Then
                            Grid.AddItem(vbTab & "Quota ospite (RETTA): " & vbTab & Format(Modulo.MathRound((TotRetta + SoloRetta2) - (SalvaImportoDocumento + SalvaImportoDocumento2), 2) - xImporto, "#,##0.00") & vbTab & SalvaGiorniDocumento - XS.GiorniPres - XS.NonGiorniPres & vbTab & ImpRpxOspite + ImpRpxOspite2 & vbTab & GGRpxOspite & vbTab & SoloRetta2 - SalvaImportoDocumento2 & vbTab & ImpRpxOspite2)
                        Else
                            Grid.AddItem(vbTab & "Quota ospite (RETTA): " & vbTab & Format(Modulo.MathRound((TotRetta + SoloRetta2 - ImpRpxOspite - ImpRpxOspite2) - (SalvaImportoDocumento + SalvaImportoDocumento2), 2) - xImporto - ImpRpxOspite - ImpRpxOspite2, "#,##0.00") & vbTab & SalvaGiorniDocumento - XS.GiorniPres - XS.NonGiorniPres & vbTab & ImpRpxOspite + ImpRpxOspite2 & vbTab & GGRpxOspite & vbTab & SoloRetta2 - SalvaImportoDocumento2 & vbTab & ImpRpxOspite2)
                        End If


                        Grid.AddItem(vbTab & "Quota ospite (AD-AC): " & vbTab & Format(xImporto, "#,##0.00") & vbTab & 0)
                    Else
                        Grid.AddItem(vbTab & "Quota ospite (RETTA): " & vbTab & Format(Modulo.MathRound((TotRetta + SoloRetta2) - (SalvaImportoDocumento - SalvaImportoDocumento2), 2), "#,##0.00") & vbTab & SalvaGiorniDocumento - XS.GiorniPres - XS.NonGiorniPres & vbTab & ImpRpxOspite + ImpRpxOspite2 & vbTab & GGRpxOspite & vbTab & SoloRetta2 - SalvaImportoDocumento2 & vbTab & ImpRpxOspite2)
                    End If

                Else

                    gaientrato = False
                    If Modulo.MathRound(TotRetta - SalvaImportoDocumento, 2) = Modulo.MathRound(xImporto, 2) Then
                        Grid.AddItem(vbTab & "Quota ospite (AD-AC): " & vbTab & Format((TotRetta + SoloRetta2) - (SalvaImportoDocumento2 - SalvaImportoDocumento), "#,##0.00") & vbTab & 0 & vbTab & ImpRpxOspite + ImpRpxOspite2 & vbTab & 0 & vbTab & SoloRetta2 - SalvaImportoDocumento2 & vbTab & ImpRpxOspite2)
                        Grid.Row = Grid.Rows - 1

                        gaientrato = True
                    End If

                    If SalvaImportoDocumento = 0 And gaientrato = False Then
                        Grid.AddItem(vbTab & "Quota ospite (RETTA): " & vbTab & Format(SoloRetta + SoloRetta2, "#,##0.00") & vbTab & XS.GiorniPres + XS.NonGiorniPres & vbTab & ImpRpxOspite + ImpRpxOspite2 & vbTab & GGRpxOspite & vbTab & SoloRetta2 - ImpRpxOspite2 & vbTab & ImpRpxOspite2)


                        Grid.AddItem(vbTab & "Quota ospite (AD-AC): " & vbTab & Format(xImporto, "#,##0.00") & vbTab & 0)

                        gaientrato = True
                    End If
                    If gaientrato = False Then
                        If Modulo.MathRound(xImporto, 2) < Modulo.MathRound(TotRetta - SalvaImportoDocumento, 2) Then
                            Grid.AddItem(vbTab & "Quota ospite (RETTA): " & vbTab & Format(Modulo.MathRound((TotRetta + SoloRetta2) - (SalvaImportoDocumento + SalvaImportoDocumento2), 2) - xImporto, "#,##0.00") & vbTab & XS.GiorniPres + XS.NonGiorniPres - SalvaGiorniDocumento & vbTab & ImpRpxOspite + ImpRpxOspite2 & vbTab & GGRpxOspite & vbTab & SoloRetta2 - SalvaImportoDocumento2 - ImpRpxOspite2 & vbTab & ImpRpxOspite2)

                            Grid.AddItem(vbTab & "Quota ospite (AD-AC): " & vbTab & Format(xImporto, "#,##0.00") & vbTab & 0)

                        Else
                            If ImportoAddebiti > 0 Then
                                Grid.AddItem(vbTab & "Quota ospite (RETTA): " & vbTab & Format((Modulo.MathRound((TotRetta + SoloRetta2) - (SalvaImportoDocumento + SalvaImportoDocumento2), 2) - ImportoAddebiti), "#,##0.00") & vbTab & XS.GiorniPres + XS.NonGiorniPres - SalvaGiorniDocumento & vbTab & ImpRpxOspite + ImpRpxOspite2 & vbTab & GGRpxOspite & vbTab & SoloRetta2 - SalvaImportoDocumento2 - ImpRpxOspite2 & vbTab & ImpRpxOspite2)

                                Grid.AddItem(vbTab & "Quota ospite (AD-AC): " & vbTab & Format(ImportoAddebiti, "#,##0.00") & vbTab & 0)

                            Else
                                Grid.AddItem(vbTab & "Quota ospite (RETTA): " & vbTab & Format(Modulo.MathRound((TotRetta + SoloRetta2 + ImpRpxOspite2) - (SalvaImportoDocumento - SalvaImportoDocumento2), 2), "#,##0.00") & vbTab & XS.GiorniPres + XS.NonGiorniPres - SalvaGiorniDocumento & vbTab & ImpRpxOspite + ImpRpxOspite2 & vbTab & GGRpxOspite & vbTab & SoloRetta2 - SalvaImportoDocumento2 & vbTab & ImpRpxOspite2)
                            End If
                        End If
                    End If


                End If
            End If
        End If




        If Val(DD_OspiteParenti.SelectedValue) > 0 Then
            ' Parenti Estrazione Dati
            Dim RsParenti As New ADODB.Recordset
            Dim GGRpxParente(10) As Double
            Dim ImpRpxParente(10) As Double
            Dim ImpRpxParente2(10) As Double

            RsParenti.Open("Select * From AnagraficaComune Where CodiceParente = " & Val(DD_OspiteParenti.SelectedValue) & " And CodiceOspite = " & Session("CODICEOSPITE"), OspitiDb, ADODB.CursorTypeEnum.adOpenKeyset, ADODB.LockTypeEnum.adLockPessimistic)
            Do While Not RsParenti.EOF
                Dim kp As New Cls_Parenti

                kp.Leggi(Session("DC_OSPITE"), Session("CODICEOSPITE"), MoveFromDb(RsParenti.Fields("CODICEPARENTE")))

                Dim KCsP As New Cls_DatiOspiteParenteCentroServizio

                KCsP.CentroServizio = Session("CODICESERVIZIO")
                KCsP.CodiceOspite = Session("CODICEOSPITE")
                KCsP.CodiceParente = MoveFromDb(RsParenti.Fields("CODICEPARENTE"))
                KCsP.Leggi(Session("DC_OSPITE"))

                REM Assegna la tipologia operazione e l'aliquota prendendo quella relativa al cserv
                If KCsP.CodiceOspite <> 0 Then
                    kp.TIPOOPERAZIONE = KCsP.TipoOperazione
                    kp.CODICEIVA = KCsP.AliquotaIva
                    kp.FattAnticipata = KCsP.Anticipata
                    kp.MODALITAPAGAMENTO = KCsP.ModalitaPagamento
                    kp.Compensazione = KCsP.Compensazione
                End If


                If kp.FattAnticipata = "S" Then
                    Calcola = 0
                    Calcola2 = 0
                    GiornoCalcolati = XS.GiorniNelMese(Session("CODICESERVIZIO"), Session("CODICEOSPITE"), xMese, xAnno)

                    For i = 1 To 31 : XS.MyTabCausale(i) = "" : Next

                    Call XS.CreaTabellaPresenze(Session("CODICESERVIZIO"), Session("CODICEOSPITE"), xMese, xAnno)
                    XGiorniAssenza = 0
                    For i = 1 To GiornoCalcolati
                        If XS.MyTabCausale(i) <> "*" And XS.MyTabCausale(i) <> "" Then
                            XGiorniAssenza = XGiorniAssenza + 1
                            Calcola = Calcola + XS.QuoteGiornaliereAssenzaCausale(Session("CODICESERVIZIO"), Session("CODICEOSPITE"), "P1", MoveFromDb(RsParenti.Fields("CODICEPARENTE")), sc2, XS.MyTabCausale(i), XGiorniAssenza, DateSerial(xAnno, xMese, i))
                            Calcola2 = Calcola2 + XS.QuoteGiornaliereAssenzaCausale(Session("CODICESERVIZIO"), Session("CODICEOSPITE"), "P2", MoveFromDb(RsParenti.Fields("CODICEPARENTE")), sc2, XS.MyTabCausale(i), XGiorniAssenza, DateSerial(xAnno, xMese, i))
                        Else
                            Calcola = Calcola + XS.QuoteGiornaliere(Session("CODICESERVIZIO"), Session("CODICEOSPITE"), "P1", MoveFromDb(RsParenti.Fields("CODICEPARENTE")), DateSerial(xAnno, xMese, i))
                            Calcola2 = Calcola2 + XS.QuoteGiornaliere(Session("CODICESERVIZIO"), Session("CODICEOSPITE"), "P2", MoveFromDb(RsParenti.Fields("CODICEPARENTE")), DateSerial(xAnno, xMese, i))
                        End If
                    Next i
                    If XS.QuoteMensile(Session("CODICESERVIZIO"), Session("CODICEOSPITE"), "P1", MoveFromDb(RsParenti.Fields("CODICEPARENTE")), DateSerial(xAnno, xMese, i)) > 0 And Calcola > 0 Then
                        Calcola = XS.QuoteMensile(Session("CODICESERVIZIO"), Session("CODICEOSPITE"), "P1", MoveFromDb(RsParenti.Fields("CODICEPARENTE")), DateSerial(xAnno, xMese, i))  'ImportoMensileParente(Par)
                        Calcola2 = XS.QuoteMensile(Session("CODICESERVIZIO"), Session("CODICEOSPITE"), "P2", MoveFromDb(RsParenti.Fields("CODICEPARENTE")), DateSerial(xAnno, xMese, i))  'ImportoMensileParente(Par)
                    Else
                        If XS.ImportoMensileParente(MoveFromDb(RsParenti.Fields("CODICEPARENTE"))) = 0 And Calcola = 0 Then
                            If XS.QuoteMensile(Session("CODICESERVIZIO"), Session("CODICEOSPITE"), "P1", MoveFromDb(RsParenti.Fields("CODICEPARENTE")), DateSerial(xAnno, xMese, i)) > 0 Then
                                Calcola = XS.QuoteMensile(Session("CODICESERVIZIO"), Session("CODICEOSPITE"), "P1", MoveFromDb(RsParenti.Fields("CODICEPARENTE")), DateSerial(xAnno, xMese, i))   'ImportoMensileParente(Par)
                                Calcola2 = XS.QuoteMensile(Session("CODICESERVIZIO"), Session("CODICEOSPITE"), "P2", MoveFromDb(RsParenti.Fields("CODICEPARENTE")), DateSerial(xAnno, xMese, i))   'ImportoMensileParente(Par)
                            End If
                        End If
                    End If
                    ImpRpxParente(MoveFromDb(RsParenti.Fields("CODICEPARENTE"))) = Modulo.MathRound(Calcola, 2)
                    ImpRpxParente2(MoveFromDb(RsParenti.Fields("CODICEPARENTE"))) = Modulo.MathRound(Calcola2, 2)
                    GGRpxParente(MoveFromDb(RsParenti.Fields("CODICEPARENTE"))) = GiornoCalcolati
                End If

                MyCs.Leggi(Session("DC_OSPITE"), Session("CODICESERVIZIO"))


                Mastro = MyCs.MASTRO
                Conto = MyCs.CONTO
                Sottoconto = Session("CODICEOSPITE") * 100 + MoveFromDb(RsParenti.Fields("CODICEPARENTE"))


                CentroServizio = Session("CODICESERVIZIO")


                Tipologia = ""
                SalvaImportoDocumento = 0
                SalvaImportoDocumento2 = 0
                SalvaGiorniDocumento = 0

                If kp.FattAnticipata = "S" Then
                    MySql = "SELECT MovimentiContabiliTesta.FatturaDiAnticipo,NumeroRegistrazione " & _
                            " FROM MovimentiContabiliRiga INNER JOIN MovimentiContabiliTesta " & _
                            " ON MovimentiContabiliRiga.Numero = MovimentiContabiliTesta.NumeroRegistrazione " & _
                            " WHERE MovimentiContabiliRiga.MastroPartita = " & Mastro & _
                            " AND MovimentiContabiliRiga.ContoPartita = " & Conto & _
                            " AND MovimentiContabiliRiga.SottocontoPartita = " & Sottoconto & _
                            " And MovimentiContabiliTesta.CentroServizio = '" & CentroServizio & "'"
                    If Trim(Tipologia) <> "" Then
                        MySql = MySql & " AND MovimentiContabiliTesta.Tipologia = '" & Tipologia & "'"
                    End If

                    SalvaImportoDocumento = 0
                    SalvaImportoDocumento2 = 0
                    SoloRigaRetta = 0
                    MyRs.Open(MySql, GeneraleDb, ADODB.CursorTypeEnum.adOpenKeyset, ADODB.LockTypeEnum.adLockOptimistic)
                    Do While Not MyRs.EOF
                        MyRsSrc.Open("Select * From MovimentiContabiliRiga Where Numero = " & MoveFromDb(MyRs.Fields("NumeroRegistrazione")) & "  And RigaDaCausale <> 13 And ((AnnoRiferimento =  " & Anno & " And MeseRiferimento = " & Mese & ") OR (AnnoRiferimento =  " & MeseDopoAnno & " And MeseRiferimento = " & MeseDopoMese & ")) ", GeneraleDb, ADODB.CursorTypeEnum.adOpenKeyset, )
                        Do While Not MyRsSrc.EOF
                            If MoveFromDb(MyRsSrc.Fields("DareAvere")) = "A" Then
                                SalvaImportoDocumento = SalvaImportoDocumento + MoveFromDb(MyRsSrc.Fields("Importo"))
                                SalvaGiorniDocumento = SalvaGiorniDocumento + MoveFromDb(MyRsSrc.Fields("Quantita"))
                            Else
                                SalvaImportoDocumento = SalvaImportoDocumento - MoveFromDb(MyRsSrc.Fields("Importo"))
                                SalvaGiorniDocumento = SalvaGiorniDocumento - MoveFromDb(MyRsSrc.Fields("Quantita"))
                            End If

                            If Val(MoveFromDb(MyRsSrc.Fields("Quantita"))) = 0 Then
                                SoloRigaRetta = 1
                            End If
                            'ImportoDocumento(MoveFromDb(MyRs.Fields("NumeroRegistrazione))
                            MyRsSrc.MoveNext()
                        Loop
                        MyRsSrc.Close()
                        MyRs.MoveNext()
                    Loop
                    MyRs.Close()
                    If RdRetta.Visible = True Then
                        If RdFuoriRetta.Checked = False Then
                            SalvaGiorniDocumento = 0
                            SalvaImportoDocumento = 0
                        End If
                    End If

                    MyRs.Open(MySql, GeneraleDb, ADODB.CursorTypeEnum.adOpenKeyset, ADODB.LockTypeEnum.adLockOptimistic)
                    Do While Not MyRs.EOF

                        MyRsSrc.Open("Select * From MovimentiContabiliRiga Where Numero = " & MoveFromDb(MyRs.Fields("NumeroRegistrazione")) & " And RigaDaCausale = 13  And ((AnnoRiferimento =  " & Anno & " And MeseRiferimento = " & Mese & ") OR (AnnoRiferimento =  " & MeseDopoAnno & " And MeseRiferimento = " & MeseDopoMese & ")) ", GeneraleDb, ADODB.CursorTypeEnum.adOpenKeyset, ADODB.LockTypeEnum.adLockOptimistic)
                        Do While Not MyRsSrc.EOF

                            If MoveFromDb(MyRsSrc.Fields("DareAvere")) = "A" Then
                                SalvaImportoDocumento2 = SalvaImportoDocumento2 + MoveFromDb(MyRsSrc.Fields("Importo"))
                            Else
                                SalvaImportoDocumento2 = SalvaImportoDocumento2 - MoveFromDb(MyRsSrc.Fields("Importo"))
                            End If

                            MyRsSrc.MoveNext()
                        Loop
                        MyRsSrc.Close()
                        MyRs.MoveNext()
                    Loop
                    MyRs.Close()
                End If

                If RdRetta.Visible = True Then
                    If RdRetta.Checked = False Then
                        SalvaImportoDocumento2 = 0
                    End If
                End If

                xImporto = 0

                MyRsSrc.Open("SelecT * FROM ADDACR WHERE CENTROSERVIZIO = '" & Session("CODICESERVIZIO") & "' AND CODICEOSPITE  = " & Session("CODICEOSPITE") & " And month(DATA) = " & Mese & " And Year(DATA) = " & Anno & " And (Elaborato Is null or Elaborato =0)", OspitiDb, ADODB.CursorTypeEnum.adOpenKeyset, ADODB.LockTypeEnum.adLockOptimistic)

                Do While Not MyRsSrc.EOF
                    If MoveFromDb(MyRsSrc.Fields("TipoMov")) = "AD" And MoveFromDb(MyRsSrc.Fields("Riferimento")) = "P" And MoveFromDb(MyRsSrc.Fields("Parente")) = MoveFromDb(RsParenti.Fields("CODICEPARENTE")) Then
                        xImporto = xImporto + MoveFromDb(MyRsSrc.Fields("Importo"))
                    End If
                    If MoveFromDb(MyRsSrc.Fields("TipoMov")) = "AC" And MoveFromDb(MyRsSrc.Fields("Riferimento")) = "P" And MoveFromDb(MyRsSrc.Fields("Parente")) = MoveFromDb(RsParenti.Fields("CODICEPARENTE")) Then
                        xImporto = xImporto - MoveFromDb(MyRsSrc.Fields("Importo"))
                    End If

                    MyRsSrc.MoveNext()
                Loop
                MyRsSrc.Close()

                Dim Indice As Integer

                For Indice = 0 To 10

                    Session("IMPORTOPARMAN1-" & Indice) = XS.ImpExtrParMan1(Indice)
                    Session("CODICEPARMAN1-" & Indice) = XS.TipoExtrParMan1(Indice)

                    Session("IMPORTOPARMAN2-" & Indice) = XS.ImpExtrParMan2(Indice)
                    Session("CODICEPARMAN2-" & Indice) = XS.TipoExtrParMan2(Indice)

                    Session("IMPORTOPARMAN3-" & Indice) = XS.ImpExtrParMan3(Indice)
                    Session("CODICEPARMAN3-" & Indice) = XS.TipoExtrParMan3(Indice)

                    Session("IMPORTOPARMAN4-" & Indice) = XS.ImpExtrParMan3(Indice)
                    Session("CODICEPARMAN4-" & Indice) = XS.TipoExtrParMan4(Indice)

                    xImporto = xImporto + XS.ImpExtrParMan1(Indice) + XS.ImpExtrParMan2(Indice) + XS.ImpExtrParMan3(Indice) + XS.ImpExtrParMan4(Indice)

                    For i = 0 To 10
                        Dim FlagAggiungiExtra As Boolean = True

                        If RdRetta.Visible = True Then
                            Dim DecExtr As New Cls_TipoExtraFisso

                            DecExtr.CODICEEXTRA = XS.MyTabCodiceExtrParent(Indice)
                            DecExtr.Leggi(Session("DC_OSPITE"))
                            FlagAggiungiExtra = False
                            If DecExtr.FuoriRetta = 1 And RdFuoriRetta.Checked = True Then
                                FlagAggiungiExtra = True
                            End If
                            If DecExtr.FuoriRetta = 0 And RdRetta.Checked = True Then
                                FlagAggiungiExtra = True
                            End If
                        End If


                        If XS.ImpExtraParente(i, Indice) <> 0 And FlagAggiungiExtra Then

                            Emr_P_DesExtra(i, Indice) = CampoExtraFisso(XS.MyTabCodiceExtrParent(Indice), "Descrizione")
                            Emr_P_ImpExtra(i, Indice) = XS.ImpExtraParente(i, Indice)

                            Emr_P_GGExtra(i, Indice) = XS.MyTabCodiceExtrParentGIORNI(MoveFromDb(RsParenti.Fields("CODICEPARENTE")), Indice)

                            Totale = Totale + XS.ImpExtraParente(i, Indice)
                            Emr_P_CodiceExtra(i, Indice) = XS.MyTabCodiceExtrParent(Indice)
                            Emr_P_CodivaExtra(i, Indice) = CampoExtraFisso(XS.MyTabCodiceExtrParent(Indice), "CodiceIva")
                            Emr_P_ExtraMastro(i, Indice) = CampoExtraFisso(XS.MyTabCodiceExtrParent(Indice), "Mastro")
                            Emr_P_ExtraConto(i, Indice) = CampoExtraFisso(XS.MyTabCodiceExtrParent(Indice), "Conto")
                            Emr_P_ExtraSottoConto(i, Indice) = CampoExtraFisso(XS.MyTabCodiceExtrParent(Indice), "SottoConto")
                        End If
                        If XS.NonImpExtraParente(i, Indice) <> 0 And FlagAggiungiExtra Then
                            Emr_P_DesExtra(i, Indice) = CampoExtraFisso(XS.MyTabCodiceExtrParent(Indice), "Descrizione")
                            Emr_P_ImpExtra(i, Indice) = XS.NonImpExtraParente(i, Indice)
                            Emr_P_GGExtra(i, Indice) = XS.MyTabCodiceExtrParentGIORNI(MoveFromDb(RsParenti.Fields("CODICEPARENTE")), Indice)
                            Totale = Totale + XS.NonImpExtraParente(i, Indice)
                            Emr_P_CodiceExtra(i, Indice) = XS.MyTabCodiceExtrParent(Indice)
                            Emr_P_CodivaExtra(i, Indice) = CampoExtraFisso(XS.MyTabCodiceExtrParent(Indice), "CodiceIva")
                            Emr_P_ExtraMastro(i, Indice) = CampoExtraFisso(XS.MyTabCodiceExtrParent(Indice), "Mastro")
                            Emr_P_ExtraConto(i, Indice) = CampoExtraFisso(XS.MyTabCodiceExtrParent(Indice), "Conto")
                            Emr_P_ExtraSottoConto(i, Indice) = CampoExtraFisso(XS.MyTabCodiceExtrParent(Indice), "SottoConto")
                        End If
                    Next
                Next

                Session("Emr_P_ImpExtra") = Emr_P_ImpExtra
                Session("Emr_P_GGExtra") = Emr_P_GGExtra
                Session("Emr_P_CodivaExtra") = Emr_P_CodivaExtra
                Session("Emr_P_CodiceExtra") = Emr_P_CodiceExtra
                Session("Emr_P_ExtraMastro") = Emr_P_ExtraMastro
                Session("Emr_P_ExtraConto") = Emr_P_ExtraConto
                Session("Emr_P_ExtraSottoConto") = Emr_P_ExtraSottoConto
                Session("Emr_P_DesExtra") = Emr_P_DesExtra

                Dim AppoggioImpRpx As Double
                Dim AppoggioImpRpx2 As Double

                xImportoAddPrec = 0



                MyRsSrc.Open("SelecT * FROM ADDACR WHERE CENTROSERVIZIO = '" & Session("CODICESERVIZIO") & "' AND CODICEOSPITE  = " & Session("CODICEOSPITE") & " And ((month(DATA) = " & PredxMese & " And Year(DATA) = " & PredxAnno & ") OR  (month(DATA) = " & Mese & " And Year(DATA) = " & Anno & ")) And (Elaborato =1)", OspitiDb, ADODB.CursorTypeEnum.adOpenKeyset, ADODB.LockTypeEnum.adLockPessimistic)
                Do While Not MyRsSrc.EOF
                    Dim ConsideraAddebito As Boolean = True

                    If RdFuoriRetta.Visible = True Then
                        Dim TipoAdd As New Cls_Addebito

                        TipoAdd.Codice = MoveFromDb(MyRsSrc.Fields("CodiceIva"))
                        TipoAdd.Leggi(Session("DC_OSPITE"), TipoAdd.Codice)
                        ConsideraAddebito = False
                        If RdRetta.Checked = True And TipoAdd.FuoriRetta = 0 Then
                            ConsideraAddebito = True
                        End If
                        If RdFuoriRetta.Checked = True And TipoAdd.FuoriRetta = 1 Then
                            ConsideraAddebito = True
                        End If
                    End If


                    If ConsideraAddebito = True Then

                        If MoveFromDb(MyRsSrc.Fields("TipoMov")) = "AD" And MoveFromDb(MyRsSrc.Fields("Riferimento")) = "P" And MoveFromDb(MyRsSrc.Fields("Parente")) = MoveFromDb(RsParenti.Fields("CODICEPARENTE")) Then
                            xImportoAddPrec = xImportoAddPrec + MoveFromDb(MyRsSrc.Fields("Importo"))
                        End If
                        If MoveFromDb(MyRsSrc.Fields("TipoMov")) = "AC" And MoveFromDb(MyRsSrc.Fields("Riferimento")) = "P" And MoveFromDb(MyRsSrc.Fields("Parente")) = MoveFromDb(RsParenti.Fields("CODICEPARENTE")) Then
                            xImportoAddPrec = xImportoAddPrec - MoveFromDb(MyRsSrc.Fields("Importo"))
                        End If
                    End If
                    MyRsSrc.MoveNext()
                Loop
                MyRsSrc.Close()



                If Chk_SoloMese.Checked = True Then
                    TotRetta = XS.NonImportoPresParente(MoveFromDb(RsParenti.Fields("CODICEPARENTE"))) + XS.NonImportoAssParente(MoveFromDb(RsParenti.Fields("CODICEPARENTE"))) + XS.ImportoPresParente(MoveFromDb(RsParenti.Fields("CODICEPARENTE"))) + XS.ImportoAssParente(MoveFromDb(RsParenti.Fields("CODICEPARENTE"))) + xImporto ' + xImportoAddPrec
                    SoloRetta = XS.NonImportoPresParente(MoveFromDb(RsParenti.Fields("CODICEPARENTE"))) + XS.NonImportoAssParente(MoveFromDb(RsParenti.Fields("CODICEPARENTE"))) + XS.ImportoPresParente(MoveFromDb(RsParenti.Fields("CODICEPARENTE"))) + XS.ImportoAssParente(MoveFromDb(RsParenti.Fields("CODICEPARENTE")))
                    SoloRetta2 = XS.NonImportoPresParente2(MoveFromDb(RsParenti.Fields("CODICEPARENTE"))) + XS.NonImportoAssParente2(MoveFromDb(RsParenti.Fields("CODICEPARENTE"))) + XS.ImportoPresParente2(MoveFromDb(RsParenti.Fields("CODICEPARENTE"))) + XS.ImportoAssParente2(MoveFromDb(RsParenti.Fields("CODICEPARENTE")))
                    ImpRpxParente(MoveFromDb(RsParenti.Fields("CODICEPARENTE"))) = 0
                    ImpRpxParente2(MoveFromDb(RsParenti.Fields("CODICEPARENTE"))) = 0
                    GGRpxParente(MoveFromDb(RsParenti.Fields("CODICEPARENTE"))) = 0
                Else
                    TotRetta = XS.NonImportoPresParente(MoveFromDb(RsParenti.Fields("CODICEPARENTE"))) + XS.NonImportoAssParente(MoveFromDb(RsParenti.Fields("CODICEPARENTE"))) + XS.ImportoPresParente(MoveFromDb(RsParenti.Fields("CODICEPARENTE"))) + XS.ImportoAssParente(MoveFromDb(RsParenti.Fields("CODICEPARENTE"))) + xImporto + ImpRpxParente(MoveFromDb(RsParenti.Fields("CODICEPARENTE"))) '+ xImportoAddPrec
                    SoloRetta = XS.NonImportoPresParente(MoveFromDb(RsParenti.Fields("CODICEPARENTE"))) + XS.NonImportoAssParente(MoveFromDb(RsParenti.Fields("CODICEPARENTE"))) + XS.ImportoPresParente(MoveFromDb(RsParenti.Fields("CODICEPARENTE"))) + XS.ImportoAssParente(MoveFromDb(RsParenti.Fields("CODICEPARENTE"))) + ImpRpxParente(MoveFromDb(RsParenti.Fields("CODICEPARENTE")))
                    SoloRetta2 = XS.NonImportoPresParente2(MoveFromDb(RsParenti.Fields("CODICEPARENTE"))) + XS.NonImportoAssParente2(MoveFromDb(RsParenti.Fields("CODICEPARENTE"))) + XS.ImportoPresParente2(MoveFromDb(RsParenti.Fields("CODICEPARENTE"))) + XS.ImportoAssParente2(MoveFromDb(RsParenti.Fields("CODICEPARENTE"))) + ImpRpxParente2(MoveFromDb(RsParenti.Fields("CODICEPARENTE")))
                End If
                If RdRetta.Visible = True Then
                    If RdFuoriRetta.Checked = True Then
                        TotRetta = SoloRetta2
                        SoloRetta = 0
                    End If
                End If




                Dim AnagParente As New Cls_Parenti

                AnagParente.CodiceOspite = Session("CODICEOSPITE")
                AnagParente.Leggi(Session("DC_OSPITE"), AnagParente.CodiceOspite, MoveFromDb(RsParenti.Fields("CODICEPARENTE")))

                Dim MDatiParente As New Cls_DatiOspiteParenteCentroServizio

                MDatiParente.CentroServizio = Session("CODICESERVIZIO")
                MDatiParente.CodiceOspite = AnagOspite.CodiceOspite
                MDatiParente.CodiceParente = MoveFromDb(RsParenti.Fields("CODICEPARENTE"))
                MDatiParente.Leggi(Session("DC_OSPITE"))

                If MDatiParente.TipoOperazione <> "" Then
                    AnagParente.TIPOOPERAZIONE = MDatiParente.TipoOperazione
                    AnagParente.CODICEIVA = MDatiParente.AliquotaIva
                End If



                Dim XOperazioneP As New Cls_TipoOperazione

                XOperazioneP.Leggi(Session("DC_OSPITE"), AnagOspite.TIPOOPERAZIONE)

                If XOperazioneP.SCORPORAIVA = 1 Then
                    Dim MyIvaP As New Cls_IVA

                    MyIvaP.Codice = AnagOspite.CODICEIVA
                    MyIvaP.Leggi(Session("DC_TABELLE"), MyIvaP.Codice)

                    TotRetta = Math.Round(TotRetta * 100 / (100 + (MyIvaP.Aliquota * 100)), 2)
                End If

                'SalvaImportoDocumento = SalvaImportoDocumento - xImportoAddPrec

                If SoloRigaRetta = 1 Then
                    SalvaImportoDocumento = SalvaImportoDocumento - xImportoAddPrec
                Else
                    SalvaImportoDocumento = SalvaImportoDocumento
                End If
                If SalvaImportoDocumento + SalvaImportoDocumento2 <> TotRetta + SoloRetta2 Then
                    If SalvaImportoDocumento > TotRetta Then

                        If Modulo.MathRound(xImporto, 2) > Modulo.MathRound(TotRetta - SalvaImportoDocumento, 2) Then

                            AppoggioImpRpx = ImpRpxParente(MoveFromDb(RsParenti.Fields("CODICEPARENTE")))
                            AppoggioImpRpx2 = ImpRpxParente2(MoveFromDb(RsParenti.Fields("CODICEPARENTE")))

                            ' modifica per variazione retta
                            If Modulo.MathRound((TotRetta + SoloRetta2) - (SalvaImportoDocumento + SalvaImportoDocumento2), 2) > 0 Then
                                Grid.AddItem(vbTab & "Quota Parente " & MoveFromDb(RsParenti.Fields("CODICEPARENTE")) & " - " & MoveFromDb(RsParenti.Fields("Nome")) & " (RETTA): " & vbTab & Format(Modulo.MathRound((TotRetta + SoloRetta2) - (SalvaImportoDocumento + SalvaImportoDocumento2), 2) - xImporto, "#,##0.00") & vbTab & SalvaGiorniDocumento - (XS.GiorniPresParente(MoveFromDb(RsParenti.Fields("CODICEPARENTE"))) + XS.NonGiorniPresParente(MoveFromDb(RsParenti.Fields("CODICEPARENTE")))) & vbTab & AppoggioImpRpx + AppoggioImpRpx2 & vbTab & GGRpxParente(MoveFromDb(RsParenti.Fields("CODICEPARENTE"))) & vbTab & SoloRetta2 - SalvaImportoDocumento2 & vbTab & AppoggioImpRpx2)
                            Else
                                Grid.AddItem(vbTab & "Quota Parente " & MoveFromDb(RsParenti.Fields("CODICEPARENTE")) & " - " & MoveFromDb(RsParenti.Fields("Nome")) & " (RETTA): " & vbTab & Format(Modulo.MathRound((TotRetta + SoloRetta2 - AppoggioImpRpx - AppoggioImpRpx2) - (SalvaImportoDocumento + SalvaImportoDocumento2), 2) - xImporto - AppoggioImpRpx - AppoggioImpRpx2, "#,##0.00") & vbTab & SalvaGiorniDocumento - (XS.GiorniPresParente(MoveFromDb(RsParenti.Fields("CODICEPARENTE"))) + XS.NonGiorniPresParente(MoveFromDb(RsParenti.Fields("CODICEPARENTE")))) & vbTab & AppoggioImpRpx + AppoggioImpRpx2 & vbTab & GGRpxParente(MoveFromDb(RsParenti.Fields("CODICEPARENTE"))) & vbTab & SoloRetta2 - SalvaImportoDocumento2 & vbTab & AppoggioImpRpx2)
                            End If

                            Grid.AddItem(vbTab & "Quota Parente " & MoveFromDb(RsParenti.Fields("CODICEPARENTE")) & " - " & MoveFromDb(RsParenti.Fields("Nome")) & " (AD-AC): " & vbTab & Format(xImporto, "#,##0.00") & vbTab & 0)


                        Else
                            Grid.AddItem(vbTab & "Quota Parente " & MoveFromDb(RsParenti.Fields("CODICEPARENTE")) & " - " & MoveFromDb(RsParenti.Fields("Nome")) & " (RETTA): " & vbTab & Format(Modulo.MathRound((TotRetta + SoloRetta2) - (SalvaImportoDocumento + SalvaImportoDocumento2), 2), "#,##0.00") & vbTab & SalvaGiorniDocumento - (XS.GiorniPresParente(MoveFromDb(RsParenti.Fields("CODICEPARENTE"))) + XS.NonGiorniPresParente(MoveFromDb(RsParenti.Fields("CODICEPARENTE")))) & vbTab & ImpRpxParente(MoveFromDb(RsParenti.Fields("CODICEPARENTE"))) + ImpRpxParente2(MoveFromDb(RsParenti.Fields("CODICEPARENTE"))) & vbTab & GGRpxParente(MoveFromDb(RsParenti.Fields("CODICEPARENTE"))) & vbTab & SoloRetta2 - SalvaImportoDocumento2 & vbTab & ImpRpxParente2(MoveFromDb(RsParenti.Fields("CODICEPARENTE"))))
                        End If

                    Else
                        gaientrato = False
                        If Modulo.MathRound(TotRetta - SalvaImportoDocumento, 2) = Modulo.MathRound(xImporto, 2) Then
                            Grid.AddItem(vbTab & "Quota Parente " & MoveFromDb(RsParenti.Fields("CODICEPARENTE")) & " - " & MoveFromDb(RsParenti.Fields("Nome")) & " (AD-AC): " & vbTab & Format((TotRetta + SoloRetta2) - (SalvaImportoDocumento + SalvaImportoDocumento2), "#,##0.00") & vbTab & 0)


                            gaientrato = True
                        End If

                        If SalvaImportoDocumento = 0 And gaientrato = False Then

                            AppoggioImpRpx = ImpRpxParente(MoveFromDb(RsParenti.Fields("CODICEPARENTE")))
                            AppoggioImpRpx2 = ImpRpxParente2(MoveFromDb(RsParenti.Fields("CODICEPARENTE")))
                            Grid.AddItem(vbTab & "Quota Parente " & MoveFromDb(RsParenti.Fields("CODICEPARENTE")) & " - " & MoveFromDb(RsParenti.Fields("Nome")) & " (RETTA): " & vbTab & Format(SoloRetta + SoloRetta2, "#,##0.00") & vbTab & XS.GiorniPresParente(MoveFromDb(RsParenti.Fields("CODICEPARENTE"))) + XS.NonGiorniPresParente(MoveFromDb(RsParenti.Fields("CODICEPARENTE"))) & vbTab & AppoggioImpRpx + AppoggioImpRpx2 & vbTab & GGRpxParente(MoveFromDb(RsParenti.Fields("CODICEPARENTE"))) & vbTab & SoloRetta2 - SalvaImportoDocumento2 - AppoggioImpRpx2 & vbTab & AppoggioImpRpx2)

                            Grid.AddItem(vbTab & "Quota Parente " & MoveFromDb(RsParenti.Fields("CODICEPARENTE")) & " - " & MoveFromDb(RsParenti.Fields("Nome")) & " (AD-AC): " & vbTab & Format(xImporto, "#,##0.00") & vbTab & 0)

                            gaientrato = True
                        End If
                        If gaientrato = False Then
                            If Modulo.MathRound(xImporto, 2) < Modulo.MathRound(TotRetta - SalvaImportoDocumento, 2) Then
                                Grid.AddItem(vbTab & "Quota Parente " & MoveFromDb(RsParenti.Fields("CODICEPARENTE")) & " - " & MoveFromDb(RsParenti.Fields("Nome")) & " (RETTA): " & vbTab & Format(Modulo.MathRound(TotRetta - SalvaImportoDocumento, 2) - xImporto, "#,##0.00") & vbTab & SalvaGiorniDocumento - (XS.GiorniPresParente(MoveFromDb(RsParenti.Fields("CODICEPARENTE"))) + XS.NonGiorniPresParente(MoveFromDb(RsParenti.Fields("CODICEPARENTE")))) & vbTab & ImpRpxParente(MoveFromDb(RsParenti.Fields("CODICEPARENTE"))) & vbTab & GGRpxParente(MoveFromDb(RsParenti.Fields("CODICEPARENTE"))) & vbTab & SoloRetta2 - SalvaImportoDocumento2 & vbTab & ImpRpxParente2(MoveFromDb(RsParenti.Fields("CODICEPARENTE"))))
                                Grid.AddItem(vbTab & "Quota Parente " & MoveFromDb(RsParenti.Fields("CODICEPARENTE")) & " - " & MoveFromDb(RsParenti.Fields("Nome")) & " (AD-AC): " & vbTab & Format(xImporto, "#,##0.00") & vbTab & 0)
                            Else
                                Grid.AddItem(vbTab & "Quota Parente " & MoveFromDb(RsParenti.Fields("CODICEPARENTE")) & " - " & MoveFromDb(RsParenti.Fields("Nome")) & " (RETTA): " & vbTab & Format(Modulo.MathRound(TotRetta - SalvaImportoDocumento, 2), "#,##0.00") & vbTab & SalvaGiorniDocumento - (XS.GiorniPresParente(MoveFromDb(RsParenti.Fields("CODICEPARENTE"))) + XS.NonGiorniPresParente(MoveFromDb(RsParenti.Fields("CODICEPARENTE")))) & vbTab & ImpRpxParente(MoveFromDb(RsParenti.Fields("CODICEPARENTE"))) & vbTab & GGRpxParente(MoveFromDb(RsParenti.Fields("CODICEPARENTE"))) & vbTab & SoloRetta2 - SalvaImportoDocumento2 & vbTab & ImpRpxParente2(MoveFromDb(RsParenti.Fields("CODICEPARENTE"))))

                            End If
                        End If
                    End If
                End If
                RsParenti.MoveNext()
            Loop
            RsParenti.Close()
        End If




        Tabella.Clear()
        Tabella.Columns.Clear()
        Tabella.Columns.Add("Quota", GetType(String))
        Tabella.Columns.Add("Importo", GetType(String))
        Tabella.Columns.Add("Giorni", GetType(Long))
        Tabella.Columns.Add("Importo Mese Successivo", GetType(String))
        Tabella.Columns.Add("Giorni Mese Successivo", GetType(Long))
        Tabella.Columns.Add("Importo2", GetType(String))
        Tabella.Columns.Add("Importo2 Mese Successivo", GetType(String))
        Dim p As Integer

        For p = 1 To Grid.Rows
            Dim myriga As System.Data.DataRow = Tabella.NewRow()
            myriga(0) = Grid.TextMatrix(p, 1)
            If Grid.TextMatrix(p, 2) = "" Then
                myriga(1) = "0,00"
            Else
                Dim Db As Double
                Db = Grid.TextMatrix(p, 2)
                myriga(1) = Format(Db, "#,##0.00")
            End If
            myriga(2) = Val(Grid.TextMatrix(p, 3))
            If Grid.TextMatrix(p, 4) = "" Then
                myriga(3) = "0,00"
            Else
                Dim Db As Double
                Db = Grid.TextMatrix(p, 4)
                myriga(3) = Format(Db, "#,##0.00")
            End If
            myriga(4) = Val(Grid.TextMatrix(p, 5))

            If Grid.TextMatrix(p, 6) = "" Then
                myriga(5) = "0,00"
            Else
                myriga(5) = Grid.TextMatrix(p, 6)
            End If

            If Grid.TextMatrix(p, 7) = "" Then
                myriga(6) = "0,00"
            Else
                myriga(6) = Grid.TextMatrix(p, 7)
            End If


            Tabella.Rows.Add(myriga)
        Next


        Session("Fn_Table") = Tabella

        Call CreaDocumento()


        Tabella.Clear()
        Tabella.Columns.Clear()
        Tabella.Columns.Add("DescrizioneRiga", GetType(String))
        Tabella.Columns.Add("Importo", GetType(String))
        Tabella.Columns.Add("Giorni", GetType(String))
        Tabella.Columns.Add("IVA", GetType(String))
        Tabella.Columns.Add("Descrizione", GetType(String))
        Tabella.Columns.Add("ID", GetType(String))
        Tabella.Columns.Add("ContoRicavo", GetType(String))

        Dim GeneraleCon As New OleDb.OleDbConnection
        Dim Causale As String = ""

        GeneraleCon.ConnectionString = Session("DC_GENERALE")
        GeneraleCon.Open()

        Dim cmdREAD As New OleDbCommand()
        cmdREAD.CommandText = ("select * from Temp_MovimentiContabiliTesta")
        cmdREAD.Connection = GeneraleCon

        Dim ReadTesta As OleDbDataReader = cmdREAD.ExecuteReader()
        If ReadTesta.Read Then
            Causale = campodb(ReadTesta.Item("CausaleContabile"))
        End If
        ReadTesta.Close()

        Dim MyCausale As New Cls_CausaleContabile

        MyCausale.Codice = Causale
        MyCausale.Leggi(Session("DC_TABELLE"), MyCausale.Codice)

        Lbl_DecodificaCausale.Text = "<br><br>Causale Contabile " & MyCausale.Descrizione & "<br/>"

        If MyCausale.TipoDocumento = "NC" Then
            Lbl_Riferimento730.Visible = True
            Lbl_RiferimentoNumero.Visible = True
            Txt_DataRif.Visible = True
            Txt_RifNumero.Visible = True
            Call NumeraRiferimentoFattura()
        End If

        Dim cmd As New OleDbCommand()
        cmd.CommandText = ("select * from Temp_MovimentiContabiliRiga where ( TIPO = '' OR TIPO IS NULL) ORDER BY RIGADACAUSALE")
        cmd.Connection = GeneraleCon

        Dim myPOSTreader As OleDbDataReader = cmd.ExecuteReader()
        Do While myPOSTreader.Read

            Dim myriga As System.Data.DataRow = Tabella.NewRow()
            If campodb(myPOSTreader.Item("RigaDaCausale")) = 3 Then
                myriga(0) = "Retta"
            End If
            If campodb(myPOSTreader.Item("RigaDaCausale")) = 4 Then
                myriga(0) = "Extra Fissi"
            End If
            If campodb(myPOSTreader.Item("RigaDaCausale")) = 5 Then
                myriga(0) = "Addebiti"
            End If
            If campodb(myPOSTreader.Item("RigaDaCausale")) = 6 Then
                myriga(0) = "Accrediti"
            End If
            If campodb(myPOSTreader.Item("RigaDaCausale")) = 9 Then
                myriga(0) = "Bollo"
            End If
            If campodb(myPOSTreader.Item("RigaDaCausale")) = 12 Then
                myriga(0) = "Sconto"
            End If
            If campodb(myPOSTreader.Item("RigaDaCausale")) = 13 Then
                myriga(0) = "Retta 2"
            End If
            If campodb(myPOSTreader.Item("RigaDaCausale")) = 14 Then
                myriga(0) = "Retta Percentuale"
            End If

            If MyCausale.TipoDocumento = "NC" Then
                If campodb(myPOSTreader.Item("DareAvere")) = "A" Then
                    myriga(1) = Modulo.MathRound(campodbn(myPOSTreader.Item("Importo")), 2)
                Else
                    myriga(1) = Modulo.MathRound(campodbn(myPOSTreader.Item("Importo")) * -1, 2)
                End If
            Else
                If campodb(myPOSTreader.Item("DareAvere")) = "A" Then
                    myriga(1) = Modulo.MathRound(campodbn(myPOSTreader.Item("Importo")), 2)
                Else
                    myriga(1) = Modulo.MathRound(campodbn(myPOSTreader.Item("Importo")) * -1, 2)
                End If
            End If

            myriga(2) = campodb(myPOSTreader.Item("Quantita"))

            If campodbn(myPOSTreader.Item("Quantita")) < 0 And Math.Abs(Math.Round(campodbn(myPOSTreader.Item("Importo")), 2)) = 0 Then
                myriga(2) = 0
            End If

            myriga(3) = campodb(myPOSTreader.Item("CodiceIva"))
            myriga(4) = campodb(myPOSTreader.Item("Descrizione"))
            myriga(5) = campodb(myPOSTreader.Item("id"))

            Dim MS As New Cls_Pianodeiconti
            MS.Mastro = campodb(myPOSTreader.Item("MastroPartita"))
            MS.Conto = campodb(myPOSTreader.Item("ContoPartita"))
            MS.Sottoconto = campodb(myPOSTreader.Item("SottocontoPartita"))
            MS.Decodfica(Session("DC_GENERALE"))

            myriga(6) = MS.Mastro & "  " & MS.Conto & " " & MS.Sottoconto & " " & MS.Descrizione

            Tabella.Rows.Add(myriga)
        Loop

        GeneraleCon.Close()


        GridV.AutoGenerateColumns = False

        GridV.DataSource = Tabella

        GridV.DataBind()



    End Sub

    Private Sub CreaDocumento()
        Dim d As Integer
        Dim Tabella As New System.Data.DataTable("tabella")
        Dim TipoClick As String
        Dim CodiceParente As Long
        Dim Giorni As Long
        Dim MyRetta As Double
        Dim MyRetta2 As Double
        Dim MyRettaRpx As Double
        Dim MyRettaRpx2 As Double
        Dim MyGiorniRpx As Long
        Dim MyAddebiti As Double
        Dim Compensazione As String

        Dim Appoggio As String
        Dim RegIVA As Integer


        Session("Doc_creato_Numero") = 0
        Tabella = Session("Fn_Table")




        CodiceParente = Val(DD_OspiteParenti.SelectedValue)




        Dim x As New ClsOspite

        x.CodiceOspite = Session("CODICEOSPITE")
        x.Leggi(Session("DC_OSPITE"), x.CodiceOspite)

        Dim KCs As New Cls_DatiOspiteParenteCentroServizio

        KCs.CentroServizio = Session("CODICESERVIZIO")
        KCs.CodiceOspite = x.CodiceOspite
        KCs.CodiceParente = CodiceParente
        KCs.Leggi(Session("DC_OSPITE"))

        REM Assegna la tipologia operazione e l'aliquota prendendo quella relativa al cserv
        If KCs.CodiceOspite <> 0 Then
            x.TIPOOPERAZIONE = KCs.TipoOperazione
            x.CODICEIVA = KCs.AliquotaIva
            x.FattAnticipata = KCs.Anticipata
            x.MODALITAPAGAMENTO = KCs.ModalitaPagamento
            x.Compensazione = KCs.Compensazione
            x.SETTIMANA = KCs.Settimana
        End If


        Dim TipoOperazione As New Cls_TipoOperazione

        TipoOperazione.Leggi(Session("DC_OSPITE"), x.TIPOOPERAZIONE)



        If TipoOperazione.CausaleRetta = "" And CodiceParente = 0 Then
            Dim kP As New Cls_EmissioneRetta


            kP.ConnessioneGenerale = Session("DC_GENERALE")
            kP.EliminaTemporanei()

            Exit Sub
        End If





        Dim k As New Cls_EmissioneRetta


        k.ConnessioneGenerale = Session("DC_GENERALE")
        k.EliminaTemporanei()


        Try
            Appoggio = Tabella.Rows(d).Item(0).ToString
        Catch ex As Exception
            Exit Sub
        End Try


        If Tabella.Rows(d).Item(0).ToString.IndexOf("(RETTA)") > 0 Then
            TipoClick = "R"
            If Tabella.Rows(d).Item(0).ToString.IndexOf("Parente") > 0 Then
                CodiceParente = Val(Mid(Appoggio, 15, InStr(1, Appoggio, "-") - 15))
            End If            
            
            Giorni = Val(Tabella.Rows(d).Item(2).ToString)
            MyRetta = Tabella.Rows(d).Item(1).ToString
            MyRettaRpx = Tabella.Rows(d).Item(3).ToString
            MyGiorniRpx = Tabella.Rows(d).Item(4).ToString
            MyRetta2 = Tabella.Rows(d).Item(5).ToString
            MyRettaRpx2 = Tabella.Rows(d).Item(6).ToString
            If d < Tabella.Rows.Count - 1 Then        
                If Tabella.Rows(d + 1).Item(0).ToString.IndexOf("AD-AC") > 0 Then
                    MyAddebiti = Tabella.Rows(d + 1).Item(1).ToString
                End If
            End If
        Else
            TipoClick = "A"

            CodiceParente = Val(DD_OspiteParenti.SelectedValue)

            MyAddebiti = Tabella.Rows(d).Item(1).ToString
            If d > 1 Then
                If Tabella.Rows(d - 1).Item(0).ToString.IndexOf("RETTA") > 0 Then
                    Giorni = Tabella.Rows(d - 1).Item(2).ToString
                    MyRetta = Tabella.Rows(d - 1).Item(1).ToString
                    MyRettaRpx = Tabella.Rows(d - 1).Item(3).ToString
                    MyGiorniRpx = Tabella.Rows(d - 1).Item(4).ToString
                    MyRetta2 = Tabella.Rows(d - 1).Item(5).ToString
                    MyRettaRpx2 = Tabella.Rows(d - 1).Item(6).ToString
                End If
            End If
        End If
        'Session("CODICESERVIZIO") & "' AND CODICEOSPITE  = " & Session("CODICEOSPITE") & 
        Compensazione = ""
        If CodiceParente > 0 Then
            Dim XCP As New Cls_Parenti
            XCP.CodiceOspite = Session("CODICEOSPITE")
            XCP.CodiceParente = CodiceParente
            XCP.Leggi(Session("DC_OSPITE"), Session("CODICEOSPITE"), CodiceParente)
            Dim KCsP As New Cls_DatiOspiteParenteCentroServizio

            KCsP.CentroServizio = Session("CODICESERVIZIO")
            KCsP.CodiceOspite = Session("CODICEOSPITE")
            KCsP.CodiceParente = CodiceParente
            KCsP.Leggi(Session("DC_OSPITE"))

            REM Assegna la tipologia operazione e l'aliquota prendendo quella relativa al cserv
            If KCsP.CodiceOspite <> 0 Then
                XCP.TIPOOPERAZIONE = KCsP.TipoOperazione
                XCP.CODICEIVA = KCsP.AliquotaIva
                XCP.FattAnticipata = KCsP.Anticipata
                XCP.MODALITAPAGAMENTO = KCsP.ModalitaPagamento
                XCP.Compensazione = KCsP.Compensazione
            End If

            Compensazione = XCP.Compensazione


            TipoOperazione.Leggi(Session("DC_OSPITE"), XCP.TIPOOPERAZIONE)


            If TipoOperazione.CausaleRetta = "" And CodiceParente > 0 Then
                Dim kP As New Cls_EmissioneRetta


                kP.ConnessioneGenerale = Session("DC_GENERALE")
                kP.EliminaTemporanei()

                Exit Sub
            End If

        Else
            'Dim XCO As New ClsOspite
            'XCO.CodiceOspite = Session("CODICEOSPITE")
            'XCO.Leggi(Session("DC_OSPITE"), Session("CODICEOSPITE"))
            Compensazione = x.Compensazione
        End If


        Dim XCC As New Cls_CausaleContabile

        XCC.Leggi(Session("DC_TABELLE"), TipoOperazione.CausaleRetta)
        RegIVA = XCC.RegistroIVA

        Dim DataRegistrazione As Date
        DataRegistrazione = Txt_DataRegistrazione.Text

        Dim cn As OleDbConnection



        cn = New Data.OleDb.OleDbConnection(Session("DC_GENERALE"))

        cn.Open()
        Dim cmd As New OleDbCommand()
        cmd.CommandText = "Select * From MovimentiContabiliTesta Where RegistroIVA = " & RegIVA & " And Annoprotocollo = " & Year(DataRegistrazione) & " And DataRegistrazione > {ts '" & Format(DataRegistrazione, "yyyy-MM-dd") & " 00:00:00'}"
        cmd.Connection = cn

        Dim myPOSTreader As OleDbDataReader = cmd.ExecuteReader()
        If myPOSTreader.Read Then

            ScriptManager.RegisterStartupScript(Me, Me.GetType(), "DataInfUlProts", "$(document).ready(function() {  VisualizzaErrore('Data inferiore ultimo protocollo inserito,non posso procedere'); } );", True)
            REM ClientScript.RegisterClientScriptBlock(Me.GetType(), "Error_Segn", "$(document).ready(function() {  VisualizzaErrore('Data inferiore ultimo protocollo inserito,non posso procedere'); } );", True)
            Call EseguiJS()
            Exit Sub
        End If
        myPOSTreader.Close()



        If Val(Txt_NumeroProtocollo.Text) > 0 Then
            Dim cmdL As New OleDbCommand()
            cmdL.CommandText = "Select * From MovimentiContabiliTesta Where RegistroIVA = " & RegIVA & " And Annoprotocollo = " & Year(DataRegistrazione) & " And NumeroProtocollo = " & Val(Txt_NumeroProtocollo.Text)
            cmdL.Connection = cn

            Dim ReadL As OleDbDataReader = cmdL.ExecuteReader()
            If ReadL.Read Then

                ScriptManager.RegisterStartupScript(Me, Me.GetType(), "DataInfUlProts", "$(document).ready(function() {  VisualizzaErrore('Protocollo già presente'); } );", True)
                Call EseguiJS()
                Exit Sub
            End If
            ReadL.Close()
        End If
        cn.Close()


        MyRetta = MyRetta - MyRetta2

        MyRettaRpx = MyRettaRpx - MyRettaRpx2



        If Compensazione = "S" Or Compensazione = "D" Then
            If RdFuoriRetta.Visible = False Then
                Call CreaDocumento(MyRetta, MyRetta2, MyAddebiti, CodiceParente, Giorni, MyRettaRpx, MyRettaRpx2, MyGiorniRpx)
            Else
                If RdRetta.Checked = True Then
                    Call CreaDocumento(MyRetta, 0, MyAddebiti, CodiceParente, Giorni, MyRettaRpx, 0, MyGiorniRpx)
                Else
                    Call CreaDocumento(0, MyRetta2, MyAddebiti, CodiceParente, Giorni, 0, MyRettaRpx2, MyGiorniRpx)
                End If
            End If


        Else
            If TipoClick = "R" Then
                Call CreaDocumento(MyRetta, MyRetta2, 0, CodiceParente, Giorni, MyRettaRpx, MyRettaRpx2, MyGiorniRpx)
            Else
                Call CreaDocumento(0, 0, MyAddebiti, CodiceParente, Giorni, MyRettaRpx, MyRettaRpx2, MyGiorniRpx)
            End If
        End If




    End Sub



    Public Function MoveFromDbWC(ByVal MyRs As ADODB.Recordset, ByVal Campo As String) As Object
        Dim MyField As ADODB.Field

#If FLAG_DEBUG = False Then
        On Error GoTo ErroreCampoInesistente
#End If
        MyField = MyRs.Fields(Campo)

        Select Case MyField.Type
            Case ADODB.DataTypeEnum.adDBDate
                If IsDBNull(MyField.Value) Then
                    MoveFromDbWC = "__/__/____"
                Else
                    If Not IsDate(MyField.Value) Or MyField.Value = "0.00.00" Then
                        MoveFromDbWC = "__/__/____"
                    Else
                        MoveFromDbWC = MyField.Value
                    End If
                End If
            Case ADODB.DataTypeEnum.adDBTimeStamp
                If IsDBNull(MyField.Value) Then
                    MoveFromDbWC = "__/__/____"
                Else
                    If Not IsDate(MyField.Value) Or MyField.Value = "0.00.00" Then
                        MoveFromDbWC = "__/__/____"
                    Else
                        MoveFromDbWC = MyField.Value
                    End If
                End If
            Case ADODB.DataTypeEnum.adSmallInt
                If IsDBNull(MyField.Value) Then
                    MoveFromDbWC = 0
                Else
                    MoveFromDbWC = MyField.Value
                End If
            Case ADODB.DataTypeEnum.adInteger
                If IsDBNull(MyField.Value) Then
                    MoveFromDbWC = 0
                Else
                    MoveFromDbWC = MyField.Value
                End If
            Case ADODB.DataTypeEnum.adNumeric
                If IsDBNull(MyField.Value) Then
                    MoveFromDbWC = 0
                Else
                    MoveFromDbWC = MyField.Value
                End If
            Case ADODB.DataTypeEnum.adTinyInt
                If IsDBNull(MyField.Value) Then
                    MoveFromDbWC = 0
                Else
                    MoveFromDbWC = MyField.Value
                End If
            Case ADODB.DataTypeEnum.adSingle
                If IsDBNull(MyField.Value) Then
                    MoveFromDbWC = 0
                Else
                    MoveFromDbWC = MyField.Value
                End If
            Case ADODB.DataTypeEnum.adLongVarBinary
                If IsDBNull(MyField.Value) Then
                    MoveFromDbWC = 0
                Else
                    MoveFromDbWC = MyField.Value
                End If
            Case ADODB.DataTypeEnum.adDouble
                If IsDBNull(MyField.Value) Then
                    MoveFromDbWC = 0
                Else
                    MoveFromDbWC = MyField.Value
                End If
            Case ADODB.DataTypeEnum.adVarChar
                If IsDBNull(MyField.Value) Then
                    MoveFromDbWC = vbNullString
                Else
                    MoveFromDbWC = CStr(MyField.Value)
                End If
            Case ADODB.DataTypeEnum.adUnsignedTinyInt
                If IsDBNull(MyField.Value) Then
                    MoveFromDbWC = 0
                Else
                    MoveFromDbWC = MyField.Value
                End If
            Case Else
                If IsDBNull(MyField.Value) Then
                    MoveFromDbWC = ""
                End If
                If IsDBNull(MyField.Value) Then
                    MoveFromDbWC = ""
                Else
                    MoveFromDbWC = MyField.Value
                End If
        End Select

        On Error GoTo 0
        Exit Function
ErroreCampoInesistente:

        On Error GoTo 0
    End Function
    Private Function MoveFromDb(ByVal MyField As ADODB.Field, Optional ByVal Tipo As Integer = 0) As Object
        If Tipo = 0 Then
            Select Case MyField.Type
                Case ADODB.DataTypeEnum.adDBDate Or ADODB.DataTypeEnum.adDBTimeStamp
                    If IsDBNull(MyField.Value) Then
                        MoveFromDb = "__/__/____"
                    Else
                        If Not IsDate(MyField.Value) Then
                            MoveFromDb = "__/__/____"
                        Else
                            MoveFromDb = MyField.Value
                        End If
                    End If
                Case ADODB.DataTypeEnum.adSmallInt
                    If IsDBNull(MyField.Value) Then
                        MoveFromDb = 0
                    Else
                        MoveFromDb = MyField.Value
                    End If
                Case 20 ' INTERO PER MYSQL
                    If IsDBNull(MyField.Value) Then
                        MoveFromDb = 0
                    Else
                        MoveFromDb = MyField.Value
                    End If
                Case ADODB.DataTypeEnum.adInteger
                    If IsDBNull(MyField.Value) Then
                        MoveFromDb = 0
                    Else
                        MoveFromDb = MyField.Value
                    End If
                Case ADODB.DataTypeEnum.adNumeric
                    If IsDBNull(MyField.Value) Then
                        MoveFromDb = 0
                    Else
                        MoveFromDb = MyField.Value
                    End If
                Case ADODB.DataTypeEnum.adTinyInt
                    If IsDBNull(MyField.Value) Then
                        MoveFromDb = 0
                    Else
                        MoveFromDb = MyField.Value
                    End If
                Case ADODB.DataTypeEnum.adSingle
                    If IsDBNull(MyField.Value) Then
                        MoveFromDb = 0
                    Else
                        MoveFromDb = MyField.Value
                    End If
                Case ADODB.DataTypeEnum.adLongVarBinary
                    If IsDBNull(MyField.Value) Then
                        MoveFromDb = 0
                    Else
                        MoveFromDb = MyField.Value
                    End If
                Case ADODB.DataTypeEnum.adDouble
                    If IsDBNull(MyField.Value) Then
                        MoveFromDb = 0
                    Else
                        MoveFromDb = MyField.Value
                    End If
                Case 139
                    If IsDBNull(MyField.Value) Then
                        MoveFromDb = 0
                    Else
                        MoveFromDb = MyField.Value
                    End If
                Case ADODB.DataTypeEnum.adVarChar
                    If IsDBNull(MyField.Value) Then
                        MoveFromDb = vbNullString
                    Else
                        MoveFromDb = CStr(MyField.Value)
                    End If
                Case ADODB.DataTypeEnum.adUnsignedTinyInt
                    If IsDBNull(MyField.Value) Then
                        MoveFromDb = 0
                    Else
                        MoveFromDb = MyField.Value
                    End If
                Case Else
                    If IsDBNull(MyField.Value) Then
                        MoveFromDb = ""
                    End If
                    If IsDBNull(MyField.Value) Then
                        MoveFromDb = ""
                    Else
                        'MoveFromDb = StringaSqlS(MyField.Value)
                        MoveFromDb = RTrim(MyField.Value)
                    End If
            End Select
        End If
    End Function

    Protected Sub GridV_DataBound(ByVal sender As Object, ByVal e As System.EventArgs) Handles GridV.DataBound

    End Sub







    Private Sub creadocumento(ByVal ImportoRetta As Double, ByVal ImportoRetta2 As Double, ByVal AddebitoAccredito As Double, ByVal CODICEPARENTE As Integer, ByVal Giorni As Integer, ByVal MyRettaRpx As Double, ByVal MyRettaRpx2 As Double, ByVal GiorniRpx As Double)
        Dim MyRs As New ADODB.Recordset
        Dim MyRsSrc As New ADODB.Recordset
        Dim IffNC As String

        Dim ImostaBollo As Double
        Dim Anno As Long
        Dim TipoOperazione As String
        Dim CausaleIVA As String
        Dim CausaleContabile As String
        Dim Mese As Integer
        Dim RegIVA As Long
        Dim ImpBollo As Double
        Dim ImportoSconto As Double
        Dim FissoSconto As Double
        Dim TipoSconto As String
        Dim MaxReg As Long
        Dim MaxProtoccollo As Long
        Dim Numero As Long
        Dim GeneraleDb As New ADODB.Connection
        Dim PercAliquotaIVA As Double
        Dim ImpostaBollo As Double
        Dim OspitiDb As New ADODB.Connection
        Dim xMese As Integer
        Dim xAnno As Integer
        Dim MTipoSconto As New Cls_Sconto
        Dim ModalitaPagamento As String
        Dim TotaleExtra As Double = 0
        Dim RettaNegativa As Boolean = False

        REM ImportoRetta = ImportoRetta - MyRettaRpx2

        Emr_ImpExtra = Session("Emr_ImpExtra")
        Emr_GGExtra = Session("Emr_GGExtra")
        Emr_CodivaExtra = Session("Emr_CodivaExtra")
        Emr_CodiceExtra = Session("Emr_CodiceExtra")
        Emr_ExtraMastro = Session("Emr_ExtraMastro")
        Emr_ExtraConto = Session("Emr_ExtraConto")
        Emr_ExtraSottoConto = Session("Emr_ExtraSottoConto")
        Emr_DesExtra = Session("Emr_DesExtra")


        Dim Parametri As New Cls_Parametri

        Parametri.LeggiParametri(Session("DC_OSPITE"))


        Dim DatiOspiteCS As New Cls_DatiOspiteParenteCentroServizio



        DatiOspiteCS.CentroServizio = Session("CODICESERVIZIO")
        DatiOspiteCS.CodiceOspite = Session("CODICEOSPITE")
        DatiOspiteCS.CodiceParente = CODICEPARENTE
        DatiOspiteCS.Leggi(Session("DC_OSPITE"))

        Dim ModPagOp As New ClsModalitaPagamento

        ModPagOp.Codice = DatiOspiteCS.ModalitaPagamento
        ModPagOp.Leggi(Session("DC_OSPITE"))

        If ModPagOp.ExtraFisso <> "" And ImportoRetta + ImportoRetta2 + AddebitoAccredito > 0 Then
            For i = 1 To 40
                Dim FlagAggiungiExtra As Boolean = True

                If RdRetta.Visible = True Then
                    Dim DecExtr As New Cls_TipoExtraFisso

                    DecExtr.CODICEEXTRA = ModPagOp.ExtraFisso
                    DecExtr.Leggi(Session("DC_OSPITE"))
                    FlagAggiungiExtra = False
                    If DecExtr.FuoriRetta = 1 And RdFuoriRetta.Checked = True Then
                        FlagAggiungiExtra = True
                    End If
                    If DecExtr.FuoriRetta = 0 And RdRetta.Checked = True Then
                        FlagAggiungiExtra = True
                    End If
                End If

                If Modulo.MathRound(Emr_ImpExtra(i), 2) = 0 And CODICEPARENTE = 0 And FlagAggiungiExtra Then
                    Dim Extra As New Cls_TipoExtraFisso

                    Extra.CODICEEXTRA = ModPagOp.ExtraFisso
                    Extra.Leggi(Session("DC_OSPITE"))

                    If Extra.IMPORTO > 0 Then
                        Emr_DesExtra(i) = Extra.Descrizione
                        Emr_ImpExtra(i) = Extra.IMPORTO
                        Emr_GGExtra(i) = 1
                        Emr_CodivaExtra(i) = Extra.CodiceIva
                        Emr_CodiceExtra(i) = Extra.CODICEEXTRA
                        Emr_ExtraMastro(i) = Extra.Mastro
                        Emr_ExtraConto(i) = Extra.Conto
                        Emr_ExtraSottoConto(i) = Extra.Sottoconto

                    End If

                    Exit For
                End If
                If Modulo.MathRound(Emr_P_ImpExtra(CODICEPARENTE, i), 2) = 0 And CODICEPARENTE > 0 And FlagAggiungiExtra Then
                    Dim Extra As New Cls_TipoExtraFisso

                    Extra.CODICEEXTRA = ModPagOp.ExtraFisso
                    Extra.Leggi(Session("DC_OSPITE"))

                    If Extra.IMPORTO > 0 Then
                        Emr_P_DesExtra(CODICEPARENTE, i) = Extra.Descrizione
                        Emr_P_ImpExtra(CODICEPARENTE, i) = Extra.IMPORTO
                        Emr_P_GGExtra(CODICEPARENTE, i) = 1
                        Emr_P_CodivaExtra(CODICEPARENTE, i) = Extra.CodiceIva
                        Emr_P_CodiceExtra(CODICEPARENTE, i) = Extra.CODICEEXTRA
                        Emr_P_ExtraMastro(CODICEPARENTE, i) = Extra.Mastro
                        Emr_P_ExtraConto(CODICEPARENTE, i) = Extra.Conto
                        Emr_P_ExtraSottoConto(CODICEPARENTE, i) = Extra.Sottoconto

                    End If

                    Exit For
                End If
            Next
        End If

        If CODICEPARENTE = 0 Then
            For i = 0 To 40
                If Modulo.MathRound(Emr_ImpExtra(i), 2) <> 0 Then
                    TotaleExtra = TotaleExtra + Modulo.MathRound(Emr_ImpExtra(i), 2)
                End If
            Next
        Else
            For i = 0 To 40
                If Modulo.MathRound(Emr_P_ImpExtra(CODICEPARENTE, i), 2) <> 0 Then
                    TotaleExtra = TotaleExtra + Modulo.MathRound(Emr_P_ImpExtra(CODICEPARENTE, i), 2)
                End If
            Next
        End If

        If CODICEPARENTE = 0 Then
            Dim OspiteAnticipato As New ClsOspite
            OspiteAnticipato.Leggi(Session("DC_OSPITE"), Session("CODICEOSPITE"))

            ImportoSconto = OspiteAnticipato.ImportoSconto
            TipoSconto = OspiteAnticipato.TipoSconto
            FissoSconto = 0
            MTipoSconto.Tipologia = ""
            MTipoSconto.Codice = TipoSconto
            MTipoSconto.Leggi(Session("DC_OSPITE"), TipoSconto)
            If MTipoSconto.Tipologia = "F" Then
                FissoSconto = ImportoSconto
            End If
            If MTipoSconto.Tipologia = "G" Then
                FissoSconto = (ImportoSconto * (Giorni + GiorniRpx))
            End If

            ModalitaPagamento = OspiteAnticipato.MODALITAPAGAMENTO

            Dim KCs As New Cls_DatiOspiteParenteCentroServizio

            KCs.CentroServizio = Session("CODICESERVIZIO")
            KCs.CodiceOspite = Session("CODICEOSPITE")
            KCs.CodiceParente = 0
            KCs.Leggi(Session("DC_OSPITE"))


            REM Assegna la tipologia operazione e l'aliquota prendendo quella relativa al cserv
            If KCs.CodiceOspite <> 0 Then
                OspiteAnticipato.TIPOOPERAZIONE = KCs.TipoOperazione
                OspiteAnticipato.CODICEIVA = KCs.AliquotaIva
                OspiteAnticipato.FattAnticipata = KCs.Anticipata
                OspiteAnticipato.MODALITAPAGAMENTO = KCs.ModalitaPagamento
                OspiteAnticipato.Compensazione = KCs.Compensazione
                OspiteAnticipato.SETTIMANA = KCs.Settimana
                ModalitaPagamento = KCs.ModalitaPagamento
            End If
            If OspiteAnticipato.FattAnticipata = "S" Then
                
                If Parametri.MeseFatturazione = Val(Dd_Mese.SelectedValue) + 1 And Parametri.AnnoFatturazione = Val(Txt_Anno.Text) Or (Parametri.MeseFatturazione = 12 And Parametri.AnnoFatturazione = Val(Txt_Anno.Text) + 1) Then
                    If MyRettaRpx > 0 Then
                        Dim ConDB As New OleDbConnection


                        ConDB.ConnectionString = Session("DC_OSPITE")
                        ConDB.Open()


                        Dim CmdInsertRow As New OleDbCommand


                        CmdInsertRow.CommandText = "INSERT INTO RETTEOSPITE ([CENTROSERVIZIO],[CODICEOSPITE],[MESE],[ANNO],[ELEMENTO],[GIORNI],[IMPORTO],[MESECOMPETENZA],[ANNOCOMPETENZA])  VALUES (?,?,?,?,?,?,?,?,?) "
                        CmdInsertRow.Parameters.AddWithValue("@CENTROSERVIZIO", Session("CODICESERVIZIO"))
                        CmdInsertRow.Parameters.AddWithValue("@CODICEOSPITE", Session("CODICEOSPITE"))

                        CmdInsertRow.Parameters.AddWithValue("@MESE", Dd_Mese.SelectedValue)
                        CmdInsertRow.Parameters.AddWithValue("@ANNO", Txt_Anno.Text)
                        CmdInsertRow.Parameters.AddWithValue("@ELEMENTO", "RPX")
                        CmdInsertRow.Parameters.AddWithValue("@GIORNI", GiorniRpx)
                        CmdInsertRow.Parameters.AddWithValue("@IMPORTO", MyRettaRpx)

                        If Dd_Mese.SelectedValue = 12 Then
                            CmdInsertRow.Parameters.AddWithValue("@MESECOMP", 1)
                            CmdInsertRow.Parameters.AddWithValue("@ANNOCOMP", Val(Txt_Anno.Text) + 1)
                        Else
                            CmdInsertRow.Parameters.AddWithValue("@MESECOMP", Dd_Mese.SelectedValue + 1)
                            CmdInsertRow.Parameters.AddWithValue("@ANNOCOMP", Val(Txt_Anno.Text))
                        End If
                        CmdInsertRow.Connection = ConDB
                        CmdInsertRow.ExecuteNonQuery()
                        ConDB.Close()
                    End If
                End If
            End If
        Else
            Dim OspiteAnticipato As New Cls_Parenti

            OspiteAnticipato.Leggi(Session("DC_OSPITE"), Session("CODICEOSPITE"), CODICEPARENTE)

            ImportoSconto = OspiteAnticipato.ImportoSconto
            TipoSconto = OspiteAnticipato.TipoSconto
            FissoSconto = 0
            MTipoSconto.Tipologia = ""
            MTipoSconto.Codice = TipoSconto
            MTipoSconto.Leggi(Session("DC_OSPITE"), TipoSconto)
            If MTipoSconto.Tipologia = "F" Then
                FissoSconto = ImportoSconto
            End If
            If MTipoSconto.Tipologia = "G" Then
                FissoSconto = (ImportoSconto * (Giorni + GiorniRpx))
            End If


            ModalitaPagamento = OspiteAnticipato.MODALITAPAGAMENTO

            Dim KCs As New Cls_DatiOspiteParenteCentroServizio

            KCs.CentroServizio = Session("CODICESERVIZIO")
            KCs.CodiceOspite = Session("CODICEOSPITE")
            KCs.CodiceParente = CODICEPARENTE
            KCs.Leggi(Session("DC_OSPITE"))

            REM Assegna la tipologia operazione e l'aliquota prendendo quella relativa al cserv
            If KCs.CodiceOspite <> 0 Then
                OspiteAnticipato.TIPOOPERAZIONE = KCs.TipoOperazione
                OspiteAnticipato.CODICEIVA = KCs.AliquotaIva
                OspiteAnticipato.FattAnticipata = KCs.Anticipata
                OspiteAnticipato.MODALITAPAGAMENTO = KCs.ModalitaPagamento
                OspiteAnticipato.Compensazione = KCs.Compensazione

                ModalitaPagamento = OspiteAnticipato.MODALITAPAGAMENTO
            End If
            If OspiteAnticipato.FattAnticipata = "S" Then
                
                If Parametri.MeseFatturazione = Val(Dd_Mese.SelectedValue) + 1 And Parametri.AnnoFatturazione = Val(Txt_Anno.Text) Or (Parametri.MeseFatturazione = 12 And Parametri.AnnoFatturazione = Val(Txt_Anno.Text) + 1) Then
                    If MyRettaRpx > 0 Then
                        Dim ConDB As New OleDbConnection


                        ConDB.ConnectionString = Session("DC_OSPITE")
                        ConDB.Open()


                        Dim CmdInsertRow As New OleDbCommand


                        CmdInsertRow.CommandText = "INSERT INTO RETTEPARENTE ([CENTROSERVIZIO],[CODICEOSPITE],CODICEPARENTE,[MESE],[ANNO],[ELEMENTO],[GIORNI],[IMPORTO],[MESECOMPETENZA],[ANNOCOMPETENZA])  VALUES (?,?,?,?,?,?,?,?,?,?) "
                        CmdInsertRow.Parameters.AddWithValue("@CENTROSERVIZIO", Session("CODICESERVIZIO"))
                        CmdInsertRow.Parameters.AddWithValue("@CODICEOSPITE", Session("CODICEOSPITE"))
                        CmdInsertRow.Parameters.AddWithValue("@CODICEPARENTE", CODICEPARENTE)

                        CmdInsertRow.Parameters.AddWithValue("@MESE", Dd_Mese.SelectedValue)
                        CmdInsertRow.Parameters.AddWithValue("@ANNO", Txt_Anno.Text)
                        CmdInsertRow.Parameters.AddWithValue("@ELEMENTO", "RPX")
                        CmdInsertRow.Parameters.AddWithValue("@GIORNI", GiorniRpx)
                        CmdInsertRow.Parameters.AddWithValue("@IMPORTO", MyRettaRpx)

                        If Dd_Mese.SelectedValue = 12 Then
                            CmdInsertRow.Parameters.AddWithValue("@MESECOMP", 1)
                            CmdInsertRow.Parameters.AddWithValue("@ANNOCOMP", Val(Txt_Anno.Text) + 1)
                        Else
                            CmdInsertRow.Parameters.AddWithValue("@MESECOMP", Dd_Mese.SelectedValue + 1)
                            CmdInsertRow.Parameters.AddWithValue("@ANNOCOMP", Val(Txt_Anno.Text))
                        End If
                        CmdInsertRow.Connection = ConDB
                        CmdInsertRow.ExecuteNonQuery()
                        ConDB.Close()
                    End If
                End If
            End If
        End If



        GeneraleDb.Open(Session("DC_GENERALE"))
        OspitiDb.Open(Session("DC_OSPITE"))

        '  ImportoRetta = 0
        '  PercAliquotaIVA = 0
        ImostaBollo = 0

        Mese = Dd_Mese.SelectedValue
        Anno = Txt_Anno.Text

        If CODICEPARENTE = 0 Then
            Dim xO As New ClsOspite
            xO.Leggi(Session("DC_OSPITE"), Session("CODICEOSPITE"))

            Dim KCs As New Cls_DatiOspiteParenteCentroServizio

            KCs.CentroServizio = Session("CODICESERVIZIO")
            KCs.CodiceOspite = Session("CODICEOSPITE")
            KCs.CodiceParente = 0
            KCs.Leggi(Session("DC_OSPITE"))

            REM Assegna la tipologia operazione e l'aliquota prendendo quella relativa al cserv
            If KCs.CodiceOspite <> 0 Then
                xO.TIPOOPERAZIONE = KCs.TipoOperazione
                xO.CODICEIVA = KCs.AliquotaIva
                xO.FattAnticipata = KCs.Anticipata
                xO.MODALITAPAGAMENTO = KCs.ModalitaPagamento
                xO.Compensazione = KCs.Compensazione
                xO.SETTIMANA = KCs.Settimana
            End If


            TipoOperazione = xO.TIPOOPERAZIONE
            CausaleIVA = xO.CODICEIVA
            ModalitaPagamento = xO.MODALITAPAGAMENTO
        Else
            Dim xP As New Cls_Parenti
            xP.Leggi(Session("DC_OSPITE"), Session("CODICEOSPITE"), CODICEPARENTE)

            Dim KCs As New Cls_DatiOspiteParenteCentroServizio

            KCs.CentroServizio = Session("CODICESERVIZIO")
            KCs.CodiceOspite = Session("CODICEOSPITE")
            KCs.CodiceParente = CODICEPARENTE
            KCs.Leggi(Session("DC_OSPITE"))

            REM Assegna la tipologia operazione e l'aliquota prendendo quella relativa al cserv
            If KCs.CodiceOspite <> 0 Then
                xP.TIPOOPERAZIONE = KCs.TipoOperazione
                xP.CODICEIVA = KCs.AliquotaIva
                xP.FattAnticipata = KCs.Anticipata
                xP.MODALITAPAGAMENTO = KCs.ModalitaPagamento
                xP.Compensazione = KCs.Compensazione
            End If

            TipoOperazione = xP.TIPOOPERAZIONE
            CausaleIVA = xP.CODICEIVA
            ModalitaPagamento = xP.MODALITAPAGAMENTO
        End If

        Dim Tpo As New Cls_TipoOperazione
        IffNC = ""
        If ImportoRetta + AddebitoAccredito + TotaleExtra > 0 Then


            Tpo.Leggi(Session("DC_OSPITE"), TipoOperazione)

            If RdRetta.visible = False Then
                CausaleContabile = Tpo.CausaleRetta
            Else
                If RdRetta.checked = True Then
                    CausaleContabile = Tpo.CausaleRetta
                Else
                    CausaleContabile = Tpo.CausaleAddebito
                End If
            End If
        Else

            Tpo.Leggi(Session("DC_OSPITE"), TipoOperazione)


            If RdRetta.visible = False Then
                CausaleContabile = Tpo.CausaleAccredito
            Else
                If RdRetta.checked = True Then
                    CausaleContabile = Tpo.CausaleAccredito
                Else
                    CausaleContabile = Tpo.CausaleStorno
                End If
            End If

            IffNC = "NC"
        End If
        If ImportoRetta - AddebitoAccredito = 0 Then
            Tpo.Leggi(Session("DC_OSPITE"), TipoOperazione)
            If Tpo.DocumentoZero <> "" Then
                CausaleContabile = Tpo.DocumentoZero
                IffNC = ""
            End If
        End If

        Dim XCC As New Cls_CausaleContabile

        XCC.Leggi(Session("DC_TABELLE"), CausaleContabile)
        RegIVA = XCC.RegistroIVA


        If XCC.Descrizione = "" Then Exit Sub

        Dim DataRegistrazione As Date
        DataRegistrazione = Txt_DataRegistrazione.Text



        If ImportoRetta < 0 Then
            RettaNegativa = True
        End If
        ImportoRetta = Math.Abs(ImportoRetta)




        Dim ImportoAddAccBollo As Double
        If AddebitoAccredito <> 0 Then
            MyRsSrc.Open("SelecT * FROM ADDACR WHERE CENTROSERVIZIO = '" & Session("CODICESERVIZIO") & "' AND CODICEOSPITE  = " & Session("CODICEOSPITE") & " And month(DATA) = " & Mese & " And Year(DATA) = " & Anno & " And (Elaborato Is null or Elaborato =0)", OspitiDb, ADODB.CursorTypeEnum.adOpenKeyset, ADODB.LockTypeEnum.adLockOptimistic)
            Do While Not MyRsSrc.EOF

                Dim ConsideraAddebito As Boolean = True

                If RdFuoriRetta.Visible = True Then
                    Dim TipoAdd As New Cls_Addebito

                    TipoAdd.Codice = MoveFromDb(MyRsSrc.Fields("CodiceIva"))
                    TipoAdd.Leggi(Session("DC_OSPITE"), TipoAdd.Codice)
                    ConsideraAddebito = False
                    If RdRetta.Checked = True And TipoAdd.FuoriRetta = 0 Then
                        ConsideraAddebito = True
                    End If
                    If RdFuoriRetta.Checked = True And TipoAdd.FuoriRetta = 1 Then
                        ConsideraAddebito = True
                    End If
                End If

                If ConsideraAddebito = True Then
                    If MoveFromDb(MyRsSrc.Fields("TipoMov")) = "AD" Then
                        ImportoAddAccBollo = ImportoAddAccBollo + MoveFromDb(MyRsSrc.Fields("Importo"))
                    Else
                        ImportoAddAccBollo = ImportoAddAccBollo - MoveFromDb(MyRsSrc.Fields("Importo"))
                    End If
                End If

                MyRsSrc.MoveNext()
            Loop
            MyRsSrc.Close()
        End If


        If Tpo.SoggettaABollo = "S" Or (IffNC = "" And Tpo.SoggettaABollo = "F") Then
            ImpBollo = ImportoBollo(DataRegistrazione, ImportoRetta + ImportoAddAccBollo)
            If IffNC = "NC" Then
                ImpBollo = ImportoBollo(DataRegistrazione, ImportoRetta - ImportoAddAccBollo)
            End If
        End If


        MaxReg = MaxRegistrazione()

        Dim XCau As New Cls_CausaleContabile
        Dim MySql As String


        XCau.Leggi(Session("DC_TABELLE"), CausaleContabile)

        RegIVA = XCau.RegistroIVA

        MaxProtoccollo = CalcolaProgressivo(Year(DataRegistrazione), RegIVA) + 1

        If Val(Txt_NumeroProtocollo.Text) > 0 Then
            MaxProtoccollo = Val(Txt_NumeroProtocollo.Text)
        End If


        MySql = "SELECT * FROM Temp_MovimentiContabiliTesta WHERE NumeroRegistrazione = 0"
        MyRs.Open(MySql, GeneraleDb, ADODB.CursorTypeEnum.adOpenKeyset, ADODB.LockTypeEnum.adLockOptimistic)
        MyRs.AddNew()
        MoveToDb(MyRs.Fields("DataRegistrazione"), DataRegistrazione)
        MoveToDb(MyRs.Fields("Competenza"), "")
        MoveToDb(MyRs.Fields("IVASospesa"), "")
        MoveToDb(MyRs.Fields("Descrizione"), "")



        Dim ModPag As New ClsModalitaPagamento

        ModPag.Codice = ModalitaPagamento
        ModPag.Leggi(Session("DC_OSPITE"))

        MoveToDb(MyRs.Fields("CodicePagamento"), ModPag.ModalitaPagamento)

        'MoveToDb MyRs.Fields("CodicePagamento, Func_GetChiaveCombo(Cmb_Pagamento, Vt_Pagamento)

        RegIVA = XCau.RegistroIVA
        MoveToDb(MyRs.Fields("RegistroIva"), RegIVA)
        If RegIVA > 0 Then
            MoveToDb(MyRs.Fields("NumeroProtocollo"), MaxProtoccollo)
            MoveToDb(MyRs.Fields("AnnoProtocollo"), Year(DataRegistrazione))
        End If
        MoveToDb(MyRs.Fields("NumeroRegistrazione"), MaxReg)

        MoveToDb(MyRs.Fields("DataDocumento"), DataRegistrazione)
        MoveToDb(MyRs.Fields("NumeroDocumento"), MoveFromDbWC(MyRs, "NumeroProtocollo"))

        MoveToDb(MyRs.Fields("CentroServizio"), Session("CODICESERVIZIO"))
        MoveToDb(MyRs.Fields("ANNOCOMPETENZA"), Txt_Anno.Text)
        MoveToDb(MyRs.Fields("MESECOMPETENZA"), Mese)

        MoveToDb(MyRs.Fields("CausaleContabile"), CausaleContabile)
        MoveToDb(MyRs.Fields("UTENTE"), Session("UTENTE"))
        MoveToDb(MyRs.Fields("DataAggiornamento"), Now)
        MyRs.Update()
        Numero = MoveFromDbWC(MyRs, "NumeroRegistrazione")
        MyRs.Close()
        '
        ' RigaDaCausale è stata impostata a 999 per fargli leggere un recordset vuoto
        '
        MySql = "Select * from Temp_MovimentiContabiliRiga where Numero = " & Numero & " And RigaDaCausale = 999"
        MyRs.Open(MySql, GeneraleDb, ADODB.CursorTypeEnum.adOpenKeyset, ADODB.LockTypeEnum.adLockOptimistic)
        ' Cliente
        MyRs.AddNew()
        MoveToDb(MyRs.Fields("Numero"), Numero)
        MoveToDb(MyRs.Fields("RigaDaCausale"), 1)

        If CODICEPARENTE = 0 Then
            MoveToDb(MyRs.Fields("MastroPartita"), MastroOspite(Session("CODICESERVIZIO"), Session("CODICEOSPITE")))
            MoveToDb(MyRs.Fields("ContoPartita"), ContoOspite(Session("CODICESERVIZIO"), Session("CODICEOSPITE")))
            MoveToDb(MyRs.Fields("SottoContoPartita"), SottoContoOspite(Session("CODICESERVIZIO"), Session("CODICEOSPITE")))
        Else
            MoveToDb(MyRs.Fields("MastroPartita"), MastroParente(Session("CODICESERVIZIO"), Session("CODICEOSPITE"), CODICEPARENTE))
            MoveToDb(MyRs.Fields("ContoPartita"), ContoParente(Session("CODICESERVIZIO"), Session("CODICEOSPITE"), CODICEPARENTE))
            MoveToDb(MyRs.Fields("SottoContoPartita"), SottoContoParente(Session("CODICESERVIZIO"), Session("CODICEOSPITE"), CODICEPARENTE))
        End If


        MoveToDb(MyRs.Fields("Segno"), XCau.Righe(0).Segno)
        MoveToDb(MyRs.Fields("DareAvere"), XCau.Righe(0).DareAvere)
        MoveToDb(MyRs.Fields("Tipo"), "CF")

        '
        If ImpBollo > 0 Then
            Dim XIvaBollo As New Cls_IVA
            XIvaBollo.Leggi(Session("DC_TABELLE"), CodiceIVABollo(DataRegistrazione))
            PercAliquotaIVA = XIvaBollo.Aliquota
            ImpostaBollo = Modulo.MathRound(Math.Abs(ImpBollo) * Modulo.MathRound(PercAliquotaIVA, 2), 2)
        End If

        Dim XIva As New Cls_IVA
        Dim IvaInclusaNelTotale As Integer
        Dim XOperazione As New Cls_TipoOperazione
        Dim ValoreImponibile As Double

        XOperazione.Leggi(Session("DC_OSPITE"), TipoOperazione)
        XIva.Leggi(Session("DC_TABELLE"), CausaleIVA)
        PercAliquotaIVA = XIva.Aliquota

        IvaInclusaNelTotale = XOperazione.SCORPORAIVA
        ImportoRetta = Math.Abs(ImportoRetta)

        If IvaInclusaNelTotale = 1 Then
            ValoreImponibile = Modulo.MathRound((Math.Abs(ImportoRetta) + Math.Abs(ImportoRetta2)) / Modulo.MathRound(PercAliquotaIVA + 1, 2), 2)
        Else
            ValoreImponibile = Modulo.MathRound(Math.Abs(ImportoRetta) + Math.Abs(ImportoRetta2), 2)
        End If
        If IvaInclusaNelTotale = 1 Then
            If XCau.Righe(0).DareAvere = XCau.Righe(8).DareAvere Then
                MoveToDb(MyRs.Fields("Importo"), Modulo.MathRound(ImportoRetta + ImportoRetta2, 2) - ImpBollo)
            Else
                MoveToDb(MyRs.Fields("Importo"), Modulo.MathRound(ImportoRetta + ImportoRetta2, 2) + ImpBollo)
            End If
        Else
            If XCau.Righe(0).DareAvere = XCau.Righe(8).DareAvere Then
                MoveToDb(MyRs.Fields("Importo"), Modulo.MathRound(ImportoRetta + ImportoRetta2 + ((Math.Abs(ImportoRetta) + Math.Abs(ImportoRetta2)) * Modulo.MathRound(PercAliquotaIVA, 2)), 2) - ImpBollo)
            Else
                MoveToDb(MyRs.Fields("Importo"), Modulo.MathRound(ImportoRetta + ImportoRetta2 + ((Math.Abs(ImportoRetta) + Math.Abs(ImportoRetta2)) * Modulo.MathRound(PercAliquotaIVA, 2)), 2) + ImpBollo)
            End If
        End If

        If ImportoSconto > 0 And FissoSconto = 0 Then
            If IffNC = "" Then
                MoveToDb(MyRs.Fields("Importo"), MoveFromDb(MyRs.Fields("Importo")) - Modulo.MathRound(ImportoRetta * ImportoSconto / 100, 2))
            End If
        End If
        If ImportoSconto > 0 And FissoSconto > 0 Then
            If IffNC = "" Then
                MoveToDb(MyRs.Fields("Importo"), FissoSconto)
            End If
        End If

        MoveToDb(MyRs.Fields("UTENTE"), Session("UTENTE"))
        MoveToDb(MyRs.Fields("DataAggiornamento"), Now)
        MyRs.Update()


        ' IVA Retta

        MyRs.AddNew()
        MoveToDb(MyRs.Fields("Numero"), Numero)
        MoveToDb(MyRs.Fields("RigaDaCausale"), 2)
        MoveToDb(MyRs.Fields("MastroPartita"), XCau.Righe(2 - 1).Mastro)
        MoveToDb(MyRs.Fields("ContoPartita"), XCau.Righe(2 - 1).Conto)
        MoveToDb(MyRs.Fields("SottoContoPartita"), XCau.Righe(2 - 1).Sottoconto)


        If CODICEPARENTE = 0 Then
            MoveToDb(MyRs.Fields("MastroControPartita"), MastroOspite(Session("CODICESERVIZIO"), Session("CODICEOSPITE")))
            MoveToDb(MyRs.Fields("ContoControPartita"), ContoOspite(Session("CODICESERVIZIO"), Session("CODICEOSPITE")))
            MoveToDb(MyRs.Fields("SottocontoControPartita"), SottoContoOspite(Session("CODICESERVIZIO"), Session("CODICEOSPITE")))
        Else
            MoveToDb(MyRs.Fields("MastroControPartita"), MastroParente(Session("CODICESERVIZIO"), Session("CODICEOSPITE"), CODICEPARENTE))
            MoveToDb(MyRs.Fields("ContoControPartita"), ContoParente(Session("CODICESERVIZIO"), Session("CODICEOSPITE"), CODICEPARENTE))
            MoveToDb(MyRs.Fields("SottocontoControPartita"), SottoContoParente(Session("CODICESERVIZIO"), Session("CODICEOSPITE"), CODICEPARENTE))
        End If

        MoveToDb(MyRs.Fields("Segno"), XCau.Righe(2 - 1).Segno)
        If IffNC = "" Then
            MoveToDb(MyRs.Fields("DareAvere"), XCau.Righe(2 - 1).DareAvere)
            If RettaNegativa = True Then
                If XCau.Righe(2 - 1).DareAvere = "D" Then
                    MoveToDb(MyRs.Fields("DareAvere"), "A")
                Else
                    MoveToDb(MyRs.Fields("DareAvere"), "D")
                End If
            End If
        Else
            If XCau.Righe(2 - 1).DareAvere = "D" Then
                MoveToDb(MyRs.Fields("DareAvere"), "A")
            Else
                MoveToDb(MyRs.Fields("DareAvere"), "D")
            End If
        End If


        MoveToDb(MyRs.Fields("Tipo"), "IV")
        MoveToDb(MyRs.Fields("CodiceIva"), CausaleIVA)
        If IvaInclusaNelTotale = 1 Then
            MoveToDb(MyRs.Fields("Importo"), Math.Abs(ImportoRetta) + Math.Abs(ImportoRetta2) - ValoreImponibile)
            MoveToDb(MyRs.Fields("Imponibile"), ValoreImponibile)
        Else
            MoveToDb(MyRs.Fields("Importo"), Modulo.MathRound((Math.Abs(ValoreImponibile) * Modulo.MathRound(PercAliquotaIVA, 2)), 2))
            MoveToDb(MyRs.Fields("Imponibile"), ValoreImponibile)
        End If
        MoveToDb(MyRs.Fields("UTENTE"), Session("UTENTE"))
        MoveToDb(MyRs.Fields("DataAggiornamento"), Now)
        MyRs.Update()

        If IffNC = "" Then
            If ImportoSconto > 0 Then
                ' Sconto 
                MyRs.AddNew()
                MoveToDb(MyRs.Fields("Numero"), Numero)
                MoveToDb(MyRs.Fields("RigaDaCausale"), 3)
                MoveToDb(MyRs.Fields("MastroPartita"), XCau.Righe(3 - 1).Mastro)
                MoveToDb(MyRs.Fields("ContoPartita"), XCau.Righe(3 - 1).Conto)
                MoveToDb(MyRs.Fields("SottoContoPartita"), XCau.Righe(3 - 1).Sottoconto)


                If CODICEPARENTE = 0 Then
                    MoveToDb(MyRs.Fields("MastroControPartita"), MastroOspite(Session("CODICESERVIZIO"), Session("CODICEOSPITE")))
                    MoveToDb(MyRs.Fields("ContoControPartita"), ContoOspite(Session("CODICESERVIZIO"), Session("CODICEOSPITE")))
                    MoveToDb(MyRs.Fields("SottocontoControPartita"), SottoContoOspite(Session("CODICESERVIZIO"), Session("CODICEOSPITE")))
                Else
                    MoveToDb(MyRs.Fields("MastroControPartita"), MastroParente(Session("CODICESERVIZIO"), Session("CODICEOSPITE"), CODICEPARENTE))
                    MoveToDb(MyRs.Fields("ContoControPartita"), ContoParente(Session("CODICESERVIZIO"), Session("CODICEOSPITE"), CODICEPARENTE))
                    MoveToDb(MyRs.Fields("SottocontoControPartita"), SottoContoParente(Session("CODICESERVIZIO"), Session("CODICEOSPITE"), CODICEPARENTE))
                End If


                MoveToDb(MyRs.Fields("Segno"), XCau.Righe(3 - 1).Segno)
                If XCau.Righe(3 - 1).DareAvere = "D" Then
                    MoveToDb(MyRs.Fields("DareAvere"), "A")
                Else
                    MoveToDb(MyRs.Fields("DareAvere"), "D")
                End If

                If TipoSconto <> "" Then
                    Dim DatiScotno As New Cls_Sconto

                    DatiScotno.Codice = TipoSconto
                    DatiScotno.Leggi(Session("DC_OSPITE"), DatiScotno.Codice)

                    If DatiScotno.Mastro > 0 Then
                        MoveToDb(MyRs.Fields("MastroPartita"), DatiScotno.Mastro)
                        MoveToDb(MyRs.Fields("ContoPartita"), DatiScotno.Conto)
                        MoveToDb(MyRs.Fields("SottoContoPartita"), DatiScotno.Sottoconto)
                    End If
                    MoveToDb(MyRs.Fields("Descrizione"), DatiScotno.Descrizione)
                End If

                MoveToDb(MyRs.Fields("Tipo"), "")
                MoveToDb(MyRs.Fields("CodiceIva"), CausaleIVA)
                If FissoSconto > 0 Then
                    MoveToDb(MyRs.Fields("Importo"), FissoSconto)
                Else
                    MoveToDb(MyRs.Fields("Importo"), Modulo.MathRound(ImportoRetta * ImportoSconto / 100, 2))
                End If

                MoveToDb(MyRs.Fields("Imponibile"), 0)
                MoveToDb(MyRs.Fields("UTENTE"), Session("UTENTE"))
                MoveToDb(MyRs.Fields("DataAggiornamento"), Now)
                MyRs.Update()

                ' IVA Sconto 
                MyRs.AddNew()
                MoveToDb(MyRs.Fields("Numero"), Numero)
                MoveToDb(MyRs.Fields("RigaDaCausale"), 2)
                MoveToDb(MyRs.Fields("MastroPartita"), XCau.Righe(2 - 1).Mastro)
                MoveToDb(MyRs.Fields("ContoPartita"), XCau.Righe(2 - 1).Conto)
                MoveToDb(MyRs.Fields("SottoContoPartita"), XCau.Righe(2 - 1).Sottoconto)


                If CODICEPARENTE = 0 Then
                    MoveToDb(MyRs.Fields("MastroControPartita"), MastroOspite(Session("CODICESERVIZIO"), Session("CODICEOSPITE")))
                    MoveToDb(MyRs.Fields("ContoControPartita"), ContoOspite(Session("CODICESERVIZIO"), Session("CODICEOSPITE")))
                    MoveToDb(MyRs.Fields("SottocontoControPartita"), SottoContoOspite(Session("CODICESERVIZIO"), Session("CODICEOSPITE")))
                Else
                    MoveToDb(MyRs.Fields("MastroControPartita"), MastroParente(Session("CODICESERVIZIO"), Session("CODICEOSPITE"), CODICEPARENTE))
                    MoveToDb(MyRs.Fields("ContoControPartita"), ContoParente(Session("CODICESERVIZIO"), Session("CODICEOSPITE"), CODICEPARENTE))
                    MoveToDb(MyRs.Fields("SottocontoControPartita"), SottoContoParente(Session("CODICESERVIZIO"), Session("CODICEOSPITE"), CODICEPARENTE))
                End If


                MoveToDb(MyRs.Fields("Segno"), XCau.Righe(2 - 1).Segno)
                If XCau.Righe(2 - 1).DareAvere = "D" Then
                    MoveToDb(MyRs.Fields("DareAvere"), "A")
                Else
                    MoveToDb(MyRs.Fields("DareAvere"), "D")
                End If


                MoveToDb(MyRs.Fields("Tipo"), "IV")
                MoveToDb(MyRs.Fields("CodiceIva"), CausaleIVA)
                If FissoSconto > 0 Then
                    MoveToDb(MyRs.Fields("Importo"), Modulo.MathRound(FissoSconto * Modulo.MathRound(PercAliquotaIVA, 2), 2))
                    MoveToDb(MyRs.Fields("Imponibile"), Modulo.MathRound(FissoSconto, 2))
                Else
                    MoveToDb(MyRs.Fields("Importo"), Modulo.MathRound((Math.Abs(Modulo.MathRound(ImportoRetta * ImportoSconto / 100, 2)) * Modulo.MathRound(PercAliquotaIVA, 2)), 2))
                    MoveToDb(MyRs.Fields("Imponibile"), Modulo.MathRound(ImportoRetta * ImportoSconto / 100, 2))
                End If


                MoveToDb(MyRs.Fields("UTENTE"), Session("UTENTE"))
                MoveToDb(MyRs.Fields("DataAggiornamento"), Now)
                MyRs.Update()
            End If
        End If

  
        Dim RegistroIVA As New Cls_RegistroIVA

        RegistroIVA.RegistroCartaceo = 0
        RegistroIVA.Tipo = XCau.RegistroIVA
        RegistroIVA.Leggi(Session("DC_TABELLE"), RegistroIVA.Tipo)

        

        If (ImpBollo > 0 Or Tpo.BolloVirtuale = 1) And RegistroIVA.RegistroCartaceo = 0 Then

            Dim ConDB As New OleDbConnection


            ConDB.ConnectionString = Session("DC_GENERALE")
            ConDB.Open()


            Dim M As New OleDbCommand


            M.Parameters.Clear()
            M.Connection = ConDB
            M.CommandText = "UPDATE Temp_MovimentiContabiliTesta Set BolloVirtuale = 1 Where NumeroRegistrazione = ?"
            M.Parameters.AddWithValue("NumeroRegistrazione", Numero)
            M.ExecuteNonQuery()

            ConDB.Close()
        End If


        If ImpBollo > 0 Then
            ' IVA Bollo
            MyRs.AddNew()
            MoveToDb(MyRs.Fields("Numero"), Numero)
            MoveToDb(MyRs.Fields("RigaDaCausale"), 2)
            MoveToDb(MyRs.Fields("MastroPartita"), XCau.Righe(2 - 1).Mastro)
            MoveToDb(MyRs.Fields("ContoPartita"), XCau.Righe(2 - 1).Conto)
            MoveToDb(MyRs.Fields("SottoContoPartita"), XCau.Righe(2 - 1).Sottoconto)


            If CODICEPARENTE = 0 Then
                MoveToDb(MyRs.Fields("MastroControPartita"), MastroOspite(Session("CODICESERVIZIO"), Session("CODICEOSPITE")))
                MoveToDb(MyRs.Fields("ContoControPartita"), ContoOspite(Session("CODICESERVIZIO"), Session("CODICEOSPITE")))
                MoveToDb(MyRs.Fields("SottocontoControPartita"), SottoContoOspite(Session("CODICESERVIZIO"), Session("CODICEOSPITE")))
            Else
                MoveToDb(MyRs.Fields("MastroControPartita"), MastroParente(Session("CODICESERVIZIO"), Session("CODICEOSPITE"), CODICEPARENTE))
                MoveToDb(MyRs.Fields("ContoControPartita"), ContoParente(Session("CODICESERVIZIO"), Session("CODICEOSPITE"), CODICEPARENTE))
                MoveToDb(MyRs.Fields("SottocontoControPartita"), SottoContoParente(Session("CODICESERVIZIO"), Session("CODICEOSPITE"), CODICEPARENTE))
            End If


            MoveToDb(MyRs.Fields("Segno"), XCau.Righe(2 - 1).Segno)
            'If CampoRigaCausaleContabile(CausaleContabile, "DareAvere", 2) = "D" Then
            '   MoveToDb MyRs.Fields("DareAvere"), "A"
            '  Else
            '   MoveToDb MyRs.Fields("DareAvere"), "D"
            'End If
            MoveToDb(MyRs.Fields("DareAvere"), XCau.Righe(2 - 1).DareAvere)

            MoveToDb(MyRs.Fields("Tipo"), "IV")
            MoveToDb(MyRs.Fields("CodiceIva"), CodiceIVABollo(DataRegistrazione))
            MoveToDb(MyRs.Fields("Importo"), ImpostaBollo)
            MoveToDb(MyRs.Fields("Imponibile"), ImpBollo)
            MoveToDb(MyRs.Fields("UTENTE"), Session("UTENTE"))
            MoveToDb(MyRs.Fields("DataAggiornamento"), Now)
            MyRs.Update()
            ' Ricavo Bollo
            MyRs.AddNew()
            MoveToDb(MyRs.Fields("Numero"), Numero)
            MoveToDb(MyRs.Fields("RigaDaCausale"), 9)
            MoveToDb(MyRs.Fields("MastroPartita"), XCau.Righe(9 - 1).Mastro)
            MoveToDb(MyRs.Fields("ContoPartita"), XCau.Righe(9 - 1).Conto)
            MoveToDb(MyRs.Fields("SottoContoPartita"), XCau.Righe(9 - 1).Sottoconto)

            If CODICEPARENTE = 0 Then
                MoveToDb(MyRs.Fields("MastroControPartita"), MastroOspite(Session("CODICESERVIZIO"), Session("CODICEOSPITE")))
                MoveToDb(MyRs.Fields("ContoControPartita"), ContoOspite(Session("CODICESERVIZIO"), Session("CODICEOSPITE")))
                MoveToDb(MyRs.Fields("SottocontoControPartita"), SottoContoOspite(Session("CODICESERVIZIO"), Session("CODICEOSPITE")))
            Else
                MoveToDb(MyRs.Fields("MastroControPartita"), MastroParente(Session("CODICESERVIZIO"), Session("CODICEOSPITE"), CODICEPARENTE))
                MoveToDb(MyRs.Fields("ContoControPartita"), ContoParente(Session("CODICESERVIZIO"), Session("CODICEOSPITE"), CODICEPARENTE))
                MoveToDb(MyRs.Fields("SottocontoControPartita"), SottoContoParente(Session("CODICESERVIZIO"), Session("CODICEOSPITE"), CODICEPARENTE))
            End If


            MoveToDb(MyRs.Fields("Segno"), XCau.Righe(9 - 1).Segno)
            'MoveToDb MyRs.Fields("DareAvere"), CampoRigaCausaleContabile(CausaleContabile, "DareAvere", 9)
            MoveToDb(MyRs.Fields("DareAvere"), XCau.Righe(9 - 1).DareAvere)

            MoveToDb(MyRs.Fields("Tipo"), "")
            MoveToDb(MyRs.Fields("CodiceIva"), CodiceIVABollo(DataRegistrazione))
            MoveToDb(MyRs.Fields("Importo"), ImpBollo)
            MoveToDb(MyRs.Fields("UTENTE"), Session("UTENTE"))
            MoveToDb(MyRs.Fields("DataAggiornamento"), Now)
            MyRs.Update()
        End If



        If AddebitoAccredito <> 0 Then


            MyRsSrc.Open("SelecT * FROM ADDACR WHERE CENTROSERVIZIO = '" & Session("CODICESERVIZIO") & "' AND CODICEOSPITE  = " & Session("CODICEOSPITE") & " And month(DATA) = " & Mese & " And Year(DATA) = " & Anno & " And (Elaborato Is null or Elaborato =0)", OspitiDb, ADODB.CursorTypeEnum.adOpenKeyset, ADODB.LockTypeEnum.adLockOptimistic)
            Do While Not MyRsSrc.EOF

                Dim ConsideraAddebito As Boolean = True

                If RdFuoriRetta.Visible = True Then
                    Dim TipoAdd As New Cls_Addebito

                    TipoAdd.Codice = MoveFromDb(MyRsSrc.Fields("CodiceIva"))
                    TipoAdd.Leggi(Session("DC_OSPITE"), TipoAdd.Codice)
                    ConsideraAddebito = False
                    If RdRetta.Checked = True And TipoAdd.FuoriRetta = 0 Then
                        ConsideraAddebito = True
                    End If
                    If RdFuoriRetta.Checked = True And TipoAdd.FuoriRetta = 1 Then
                        ConsideraAddebito = True
                    End If
                End If

                If ConsideraAddebito = True Then
                    If MoveFromDb(MyRsSrc.Fields("TipoMov")) = "AD" And MoveFromDb(MyRsSrc.Fields("Riferimento")) = "O" And Val(CODICEPARENTE) = 0 Then
                        MyRs.AddNew()
                        MoveToDb(MyRs.Fields("Utente"), "NEWAUTONCFAT")
                        MoveToDb(MyRs.Fields("DataAggiornamento"), Now)
                        MoveToDb(MyRs.Fields("Numero"), Numero)
                        MoveToDb(MyRs.Fields("RigaDaCausale"), 2)
                        MoveToDb(MyRs.Fields("MastroPartita"), XCau.Righe(2 - 1).Mastro)
                        MoveToDb(MyRs.Fields("ContoPartita"), XCau.Righe(2 - 1).Conto)
                        MoveToDb(MyRs.Fields("SottoContoPartita"), XCau.Righe(2 - 1).Sottoconto)

                        MoveToDb(MyRs.Fields("MastroControPartita"), MastroOspite(Session("CODICESERVIZIO"), Session("CODICEOSPITE")))
                        MoveToDb(MyRs.Fields("ContoControPartita"), ContoOspite(Session("CODICESERVIZIO"), Session("CODICEOSPITE")))
                        MoveToDb(MyRs.Fields("SottocontoControPartita"), SottoContoOspite(Session("CODICESERVIZIO"), Session("CODICEOSPITE")))

                        MoveToDb(MyRs.Fields("Segno"), XCau.Righe(2 - 1).Segno)
                        MoveToDb(MyRs.Fields("DareAvere"), XCau.Righe(2 - 1).DareAvere)
                        MoveToDb(MyRs.Fields("Tipo"), "IV")
                        Dim CodiceIvaAddebito As String = CampoTipoAddebito(MoveFromDb(MyRsSrc.Fields("CodiceIva")), "CodiceIVA")

                        If CodiceIvaAddebito = "" Then
                            CodiceIvaAddebito = CausaleIVA
                        End If

                        MoveToDb(MyRs.Fields("CodiceIva"), CodiceIvaAddebito)

                        If IvaInclusaNelTotale = 1 Then
                            Dim MyIva As New Cls_IVA

                            MyIva.Leggi(Session("DC_TABELLE"), CampoTipoAddebito(MoveFromDb(MyRsSrc.Fields("CodiceIva")), "CodiceIVA"))

                            MoveToDb(MyRs.Fields("Imponibile"), Modulo.MathRound((MoveFromDb(MyRsSrc.Fields("Importo")) * 100) / ((MyIva.Aliquota * 100) + 100), 2))
                            MoveToDb(MyRs.Fields("Importo"), MoveFromDb(MyRsSrc.Fields("Importo")) - MoveFromDb(MyRs.Fields("Imponibile")))
                        Else
                            Dim MyIva As New Cls_IVA

                            MyIva.Leggi(Session("DC_TABELLE"), CampoTipoAddebito(MoveFromDb(MyRsSrc.Fields("CodiceIva")), "CodiceIVA"))

                            MoveToDb(MyRs.Fields("Imponibile"), MoveFromDb(MyRsSrc.Fields("Importo")))
                            MoveToDb(MyRs.Fields("Importo"), MyIva.Aliquota * MoveFromDb(MyRsSrc.Fields("Importo")))
                        End If

                        MoveToDb(MyRs.Fields("AnnoRiferimento"), 0)
                        MoveToDb(MyRs.Fields("MeseRiferimento"), 0)
                        MoveToDb(MyRs.Fields("Quantita"), 0)

                        If Parametri.TipoAddebitoFatturePrivati = 1 Then
                            MoveToDb(MyRs.Fields("Descrizione"), CampoTipoAddebito(MoveFromDb(MyRsSrc.Fields("CodiceIva")), "Descrizione") & MoveFromDb(MyRsSrc.Fields("Descrizione")))
                        Else
                            MoveToDb(MyRs.Fields("Descrizione"), MoveFromDb(MyRsSrc.Fields("Descrizione")))
                        End If
                        MoveToDb(MyRs.Fields("TipoExtra"), "A" & MoveFromDb(MyRsSrc.Fields("CodiceIva")))

                        MoveToDb(MyRs.Fields("UTENTE"), Session("UTENTE"))
                        MoveToDb(MyRs.Fields("DataAggiornamento"), Now)
                        MyRs.Update()

                        MyRs.AddNew()
                        MoveToDb(MyRs.Fields("Utente"), "AUTONCFAT")
                        MoveToDb(MyRs.Fields("DataAggiornamento"), Now)
                        MoveToDb(MyRs.Fields("Numero"), Numero)
                        MoveToDb(MyRs.Fields("RigaDaCausale"), 5)
                        MoveToDb(MyRs.Fields("MastroPartita"), XCau.Righe(3 - 1).Mastro)
                        MoveToDb(MyRs.Fields("ContoPartita"), XCau.Righe(3 - 1).Conto)
                        MoveToDb(MyRs.Fields("SottoContoPartita"), XCau.Righe(3 - 1).Sottoconto)


                        If MoveFromDbWC(MyRsSrc, "CODICEIVA") <> "" Then
                            If Val(CampoTipoAddebito(MoveFromDbWC(MyRsSrc, "CODICEIVA"), "Mastro")) > 0 Then
                                MoveToDb(MyRs.Fields("MastroPartita"), CampoTipoAddebito(MoveFromDbWC(MyRsSrc, "CODICEIVA"), "Mastro"))
                                MoveToDb(MyRs.Fields("ContoPartita"), CampoTipoAddebito(MoveFromDbWC(MyRsSrc, "CODICEIVA"), "Conto"))
                                MoveToDb(MyRs.Fields("SottoContoPartita"), CampoTipoAddebito(MoveFromDbWC(MyRsSrc, "CODICEIVA"), "SottoConto"))
                            End If
                        End If

                        MoveToDb(MyRs.Fields("MastroControPartita"), 0) 'MastroOspite(Session("CODICESERVIZIO"), Session("CODICEOSPITE")))
                        MoveToDb(MyRs.Fields("ContoControPartita"), 0) 'ContoOspite(Session("CODICESERVIZIO"), Session("CODICEOSPITE")))
                        MoveToDb(MyRs.Fields("SottocontoControPartita"), 0) 'SottoContoOspite(Session("CODICESERVIZIO"), Session("CODICEOSPITE")))

                        MoveToDb(MyRs.Fields("Segno"), XCau.Righe(3 - 1).Segno)
                        MoveToDb(MyRs.Fields("DareAvere"), XCau.Righe(3 - 1).DareAvere)
                        MoveToDb(MyRs.Fields("Tipo"), "")


                        MoveToDb(MyRs.Fields("CodiceIva"), CodiceIvaAddebito)



                        If IvaInclusaNelTotale = 1 Then
                            Dim MyIva As New Cls_IVA

                            MyIva.Leggi(Session("DC_TABELLE"), CampoTipoAddebito(MoveFromDb(MyRsSrc.Fields("CodiceIva")), "CodiceIVA"))

                            MoveToDb(MyRs.Fields("Importo"), Modulo.MathRound((MoveFromDb(MyRsSrc.Fields("Importo")) * 100) / ((MyIva.Aliquota * 100) + 100), 2))
                        Else
                            MoveToDb(MyRs.Fields("Importo"), MoveFromDb(MyRsSrc.Fields("Importo")))
                        End If


                        MoveToDb(MyRs.Fields("AnnoRiferimento"), Txt_Anno.Text)
                        MoveToDb(MyRs.Fields("MeseRiferimento"), Mese)
                        MoveToDb(MyRs.Fields("Quantita"), 0)
                        If Parametri.TipoAddebitoFatturePrivati = 1 Then
                            MoveToDb(MyRs.Fields("Descrizione"), CampoTipoAddebito(MoveFromDb(MyRsSrc.Fields("CodiceIva")), "Descrizione") & MoveFromDb(MyRsSrc.Fields("Descrizione")))
                        Else
                            MoveToDb(MyRs.Fields("Descrizione"), MoveFromDb(MyRsSrc.Fields("Descrizione")))
                        End If
                        MoveToDb(MyRs.Fields("UTENTE"), Session("UTENTE"))
                        MoveToDb(MyRs.Fields("DataAggiornamento"), Now)
                        MyRs.Update()




                        REM MoveToDb(MyRsSrc.Fields("Elaborato"), 1)
                        REM MoveToDb(MyRsSrc.Fields("NumeroRegistrazione"), Numero)
                        REM MoveToDb(MyRsSrc.Fields("UTENTE"), Session("UTENTE"))
                        REM MoveToDb(MyRsSrc.Fields("DataAggiornamento"), Now)
                        REM MyRsSrc.Update()
                    End If
                    If MoveFromDb(MyRsSrc.Fields("TipoMov")) = "AC" And MoveFromDb(MyRsSrc.Fields("Riferimento")) = "O" And Val(CODICEPARENTE) = 0 Then
                        MyRs.AddNew()
                        MoveToDb(MyRs.Fields("Utente"), "NEWAUTONCFAT")
                        MoveToDb(MyRs.Fields("DataAggiornamento"), Now)
                        MoveToDb(MyRs.Fields("Numero"), Numero)
                        MoveToDb(MyRs.Fields("RigaDaCausale"), 2)
                        MoveToDb(MyRs.Fields("MastroPartita"), XCau.Righe(2 - 1).Mastro)
                        MoveToDb(MyRs.Fields("ContoPartita"), XCau.Righe(2 - 1).Conto)
                        MoveToDb(MyRs.Fields("SottoContoPartita"), XCau.Righe(2 - 1).Sottoconto)

                        MoveToDb(MyRs.Fields("MastroControPartita"), MastroOspite(Session("CODICESERVIZIO"), Session("CODICEOSPITE")))
                        MoveToDb(MyRs.Fields("ContoControPartita"), ContoOspite(Session("CODICESERVIZIO"), Session("CODICEOSPITE")))
                        MoveToDb(MyRs.Fields("SottocontoControPartita"), SottoContoOspite(Session("CODICESERVIZIO"), Session("CODICEOSPITE")))

                        MoveToDb(MyRs.Fields("Segno"), XCau.Righe(2 - 1).Segno)
                        If XCau.Righe(2 - 1).DareAvere = "A" Then
                            MoveToDb(MyRs.Fields("DareAvere"), "D")
                        Else
                            MoveToDb(MyRs.Fields("DareAvere"), "A")
                        End If
                        MoveToDb(MyRs.Fields("Tipo"), "IV")

                        Dim CodiceIvaAddebito As String = CampoTipoAddebito(MoveFromDb(MyRsSrc.Fields("CodiceIva")), "CodiceIVA")

                        If CodiceIvaAddebito = "" Then
                            CodiceIvaAddebito = CausaleIVA
                        End If

                        MoveToDb(MyRs.Fields("CodiceIva"), CodiceIvaAddebito)

                        If IvaInclusaNelTotale = 1 Then
                            Dim MyIva As New Cls_IVA

                            MyIva.Leggi(Session("DC_TABELLE"), CampoTipoAddebito(MoveFromDb(MyRsSrc.Fields("CodiceIva")), "CodiceIVA"))


                            MoveToDb(MyRs.Fields("Imponibile"), Modulo.MathRound((MoveFromDb(MyRsSrc.Fields("Importo")) * 100) / ((MyIva.Aliquota * 100) + 100), 2))
                            MoveToDb(MyRs.Fields("Importo"), MoveFromDb(MyRsSrc.Fields("Importo")) - MoveFromDb(MyRs.Fields("Imponibile")))
                        Else
                            MoveToDb(MyRs.Fields("Imponibile"), MoveFromDb(MyRsSrc.Fields("Importo")))
                            Dim MyIva As New Cls_IVA

                            MyIva.Leggi(Session("DC_TABELLE"), CampoTipoAddebito(MoveFromDb(MyRsSrc.Fields("CodiceIva")), "CodiceIVA"))

                            MoveToDb(MyRs.Fields("Importo"), MyIva.Aliquota * MoveFromDb(MyRsSrc.Fields("Importo")))
                        End If

                        MoveToDb(MyRs.Fields("AnnoRiferimento"), 0)
                        MoveToDb(MyRs.Fields("MeseRiferimento"), 0)
                        MoveToDb(MyRs.Fields("Quantita"), 0)
                        MoveToDb(MyRs.Fields("Descrizione"), MoveFromDb(MyRsSrc.Fields("Descrizione")))
                        MoveToDb(MyRs.Fields("UTENTE"), Session("UTENTE"))
                        MoveToDb(MyRs.Fields("DataAggiornamento"), Now)
                        MyRs.Update()

                        MyRs.AddNew()
                        MoveToDb(MyRs.Fields("Utente"), "AUTONCFAT")
                        MoveToDb(MyRs.Fields("DataAggiornamento"), Now)
                        MoveToDb(MyRs.Fields("Numero"), Numero)
                        MoveToDb(MyRs.Fields("RigaDaCausale"), 6)
                        MoveToDb(MyRs.Fields("MastroPartita"), XCau.Righe(3 - 1).Mastro)
                        MoveToDb(MyRs.Fields("ContoPartita"), XCau.Righe(3 - 1).Conto)
                        MoveToDb(MyRs.Fields("SottoContoPartita"), XCau.Righe(3 - 1).Sottoconto)

                        MoveToDb(MyRs.Fields("MastroControPartita"), 0) 'MastroOspite(Session("CODICESERVIZIO"), Session("CODICEOSPITE")))
                        MoveToDb(MyRs.Fields("ContoControPartita"), 0) 'ContoOspite(Session("CODICESERVIZIO"), Session("CODICEOSPITE")))
                        MoveToDb(MyRs.Fields("SottocontoControPartita"), 0) 'SottoContoOspite(Session("CODICESERVIZIO"), Session("CODICEOSPITE")))

                        If MoveFromDbWC(MyRsSrc, "CODICEIVA") <> "" Then
                            If Val(CampoTipoAddebito(MoveFromDbWC(MyRsSrc, "CODICEIVA"), "Mastro")) > 0 Then
                                MoveToDb(MyRs.Fields("MastroPartita"), CampoTipoAddebito(MoveFromDbWC(MyRsSrc, "CODICEIVA"), "Mastro"))
                                MoveToDb(MyRs.Fields("ContoPartita"), CampoTipoAddebito(MoveFromDbWC(MyRsSrc, "CODICEIVA"), "Conto"))
                                MoveToDb(MyRs.Fields("SottoContoPartita"), CampoTipoAddebito(MoveFromDbWC(MyRsSrc, "CODICEIVA"), "SottoConto"))
                            End If
                        End If

                        MoveToDb(MyRs.Fields("Segno"), XCau.Righe(3 - 1).Segno)
                        If XCau.Righe(3 - 1).DareAvere = "A" Then
                            MoveToDb(MyRs.Fields("DareAvere"), "D")
                        Else
                            MoveToDb(MyRs.Fields("DareAvere"), "A")
                        End If
                        MoveToDb(MyRs.Fields("Tipo"), "")
                        MoveToDb(MyRs.Fields("CodiceIva"), CodiceIvaAddebito)
                        REM MoveToDb MyRs.Fields("Importo"), MoveFromDb(MyRsSrc.Fields("Importo"))


                        If IvaInclusaNelTotale = 1 Then
                            Dim MyIva As New Cls_IVA

                            MyIva.Leggi(Session("DC_TABELLE"), CampoTipoAddebito(MoveFromDb(MyRsSrc.Fields("CodiceIva")), "CodiceIVA"))

                            MoveToDb(MyRs.Fields("Importo"), Modulo.MathRound((MoveFromDb(MyRsSrc.Fields("Importo")) * 100) / ((MyIva.Aliquota * 100) + 100), 2))
                        Else
                            MoveToDb(MyRs.Fields("Importo"), MoveFromDb(MyRsSrc.Fields("Importo")))
                        End If

                        MoveToDb(MyRs.Fields("AnnoRiferimento"), Txt_Anno.Text)
                        MoveToDb(MyRs.Fields("MeseRiferimento"), Mese)
                        MoveToDb(MyRs.Fields("Quantita"), 0)
                        If Parametri.TipoAddebitoFatturePrivati = 1 Then
                            MoveToDb(MyRs.Fields("Descrizione"), CampoTipoAddebito(MoveFromDb(MyRsSrc.Fields("CodiceIva")), "Descrizione") & MoveFromDb(MyRsSrc.Fields("Descrizione")))
                        Else
                            MoveToDb(MyRs.Fields("Descrizione"), MoveFromDb(MyRsSrc.Fields("Descrizione")))
                        End If
                        MoveToDb(MyRs.Fields("TipoExtra"), "A" & MoveFromDb(MyRsSrc.Fields("CodiceIva")))

                        MoveToDb(MyRs.Fields("UTENTE"), Session("UTENTE"))
                        MoveToDb(MyRs.Fields("DataAggiornamento"), Now)
                        MyRs.Update()


                        REM MoveToDb(MyRsSrc.Fields("NumeroRegistrazione"), Numero)
                        REM MoveToDb(MyRsSrc.Fields("Elaborato"), 1)
                        REM MoveToDb(MyRsSrc.Fields("UTENTE"), Session("UTENTE"))
                        REM MoveToDb(MyRsSrc.Fields("DataAggiornamento"), Now)
                        REM MyRsSrc.Update()
                    End If

                    If MoveFromDb(MyRsSrc.Fields("TipoMov")) = "AD" And MoveFromDb(MyRsSrc.Fields("Riferimento")) = "P" And Val(CODICEPARENTE) > 0 Then
                        MyRs.AddNew()
                        MoveToDb(MyRs.Fields("Utente"), "NEWAUTONCFAT")
                        MoveToDb(MyRs.Fields("DataAggiornamento"), Now)
                        MoveToDb(MyRs.Fields("Numero"), Numero)
                        MoveToDb(MyRs.Fields("RigaDaCausale"), 2)
                        MoveToDb(MyRs.Fields("MastroPartita"), XCau.Righe(2 - 1).Mastro)
                        MoveToDb(MyRs.Fields("ContoPartita"), XCau.Righe(2 - 1).Conto)
                        MoveToDb(MyRs.Fields("SottoContoPartita"), XCau.Righe(2 - 1).Sottoconto)

                        MoveToDb(MyRs.Fields("MastroControPartita"), MastroParente(Session("CODICESERVIZIO"), Session("CODICEOSPITE"), MoveFromDb(MyRsSrc.Fields("Parente"))))
                        MoveToDb(MyRs.Fields("ContoControPartita"), ContoParente(Session("CODICESERVIZIO"), Session("CODICEOSPITE"), MoveFromDb(MyRsSrc.Fields("Parente"))))
                        MoveToDb(MyRs.Fields("SottocontoControPartita"), SottoContoParente(Session("CODICESERVIZIO"), Session("CODICEOSPITE"), MoveFromDb(MyRsSrc.Fields("Parente"))))

                        MoveToDb(MyRs.Fields("Segno"), XCau.Righe(2 - 1).Segno)
                        MoveToDb(MyRs.Fields("DareAvere"), XCau.Righe(2 - 1).DareAvere)
                        MoveToDb(MyRs.Fields("Tipo"), "IV")

                        Dim CodiceIvaAddebito As String = CampoTipoAddebito(MoveFromDb(MyRsSrc.Fields("CodiceIva")), "CodiceIVA")

                        If CodiceIvaAddebito = "" Then
                            CodiceIvaAddebito = CausaleIVA
                        End If


                        MoveToDb(MyRs.Fields("CodiceIva"), CodiceIvaAddebito)

                        If IvaInclusaNelTotale = 1 Then
                            Dim MyIva As New Cls_IVA

                            MyIva.Leggi(Session("DC_TABELLE"), CampoTipoAddebito(MoveFromDb(MyRsSrc.Fields("CodiceIva")), "CodiceIVA"))

                            MoveToDb(MyRs.Fields("Imponibile"), Modulo.MathRound((MoveFromDb(MyRsSrc.Fields("Importo")) * 100) / ((MyIva.Aliquota * 100) + 100), 2))
                            MoveToDb(MyRs.Fields("Importo"), MoveFromDb(MyRsSrc.Fields("Importo")) - MoveFromDb(MyRs.Fields("Imponibile")))
                        Else
                            Dim MyIva As New Cls_IVA

                            MyIva.Leggi(Session("DC_TABELLE"), CampoTipoAddebito(MoveFromDb(MyRsSrc.Fields("CodiceIva")), "CodiceIVA"))

                            MoveToDb(MyRs.Fields("Imponibile"), MoveFromDb(MyRsSrc.Fields("Importo")))
                            MoveToDb(MyRs.Fields("Importo"), MyIva.Aliquota * MoveFromDb(MyRsSrc.Fields("Importo")))
                        End If

                        MoveToDb(MyRs.Fields("AnnoRiferimento"), 0)
                        MoveToDb(MyRs.Fields("MeseRiferimento"), 0)
                        MoveToDb(MyRs.Fields("Quantita"), 0)
                        MoveToDb(MyRs.Fields("Descrizione"), MoveFromDb(MyRsSrc.Fields("Descrizione")))
                        MoveToDb(MyRs.Fields("UTENTE"), Session("UTENTE"))
                        MoveToDb(MyRs.Fields("DataAggiornamento"), Now)
                        MyRs.Update()

                        MyRs.AddNew()
                        MoveToDb(MyRs.Fields("Utente"), "AUTONCFAT")
                        MoveToDb(MyRs.Fields("DataAggiornamento"), Now)
                        MoveToDb(MyRs.Fields("Numero"), Numero)
                        MoveToDb(MyRs.Fields("RigaDaCausale"), 5)
                        MoveToDb(MyRs.Fields("MastroPartita"), XCau.Righe(3 - 1).Mastro)
                        MoveToDb(MyRs.Fields("ContoPartita"), XCau.Righe(3 - 1).Conto)
                        MoveToDb(MyRs.Fields("SottoContoPartita"), XCau.Righe(3 - 1).Sottoconto)

                        MoveToDb(MyRs.Fields("MastroControPartita"), MastroParente(Session("CODICESERVIZIO"), Session("CODICEOSPITE"), MoveFromDb(MyRsSrc.Fields("Parente"))))
                        MoveToDb(MyRs.Fields("ContoControPartita"), ContoParente(Session("CODICESERVIZIO"), Session("CODICEOSPITE"), MoveFromDb(MyRsSrc.Fields("Parente"))))
                        MoveToDb(MyRs.Fields("SottocontoControPartita"), SottoContoParente(Session("CODICESERVIZIO"), Session("CODICEOSPITE"), MoveFromDb(MyRsSrc.Fields("Parente"))))

                        If MoveFromDbWC(MyRsSrc, "CODICEIVA") <> "" Then
                            MoveToDb(MyRs.Fields("MastroPartita"), CampoTipoAddebito(MoveFromDbWC(MyRsSrc, "CODICEIVA"), "Mastro"))
                            MoveToDb(MyRs.Fields("ContoPartita"), CampoTipoAddebito(MoveFromDbWC(MyRsSrc, "CODICEIVA"), "Conto"))
                            MoveToDb(MyRs.Fields("SottoContoPartita"), CampoTipoAddebito(MoveFromDbWC(MyRsSrc, "CODICEIVA"), "SottoConto"))
                        End If

                        MoveToDb(MyRs.Fields("Segno"), XCau.Righe(3 - 1).Segno)
                        MoveToDb(MyRs.Fields("DareAvere"), XCau.Righe(3 - 1).DareAvere)
                        MoveToDb(MyRs.Fields("Tipo"), "")
                        MoveToDb(MyRs.Fields("CodiceIva"), CodiceIvaAddebito)


                        If IvaInclusaNelTotale = 1 Then
                            Dim MyIva As New Cls_IVA

                            MyIva.Leggi(Session("DC_TABELLE"), CampoTipoAddebito(MoveFromDb(MyRsSrc.Fields("CodiceIva")), "CodiceIVA"))

                            MoveToDb(MyRs.Fields("Importo"), Modulo.MathRound((MoveFromDb(MyRsSrc.Fields("Importo")) * 100) / ((MyIva.Aliquota * 100) + 100), 2))
                        Else
                            MoveToDb(MyRs.Fields("Importo"), MoveFromDb(MyRsSrc.Fields("Importo")))
                        End If

                        MoveToDb(MyRs.Fields("AnnoRiferimento"), Txt_Anno.Text)
                        MoveToDb(MyRs.Fields("MeseRiferimento"), Mese)
                        MoveToDb(MyRs.Fields("Quantita"), 0)
                        MoveToDb(MyRs.Fields("Descrizione"), MoveFromDb(MyRsSrc.Fields("Descrizione")))
                        MoveToDb(MyRs.Fields("UTENTE"), Session("UTENTE"))
                        MoveToDb(MyRs.Fields("DataAggiornamento"), Now)
                        MyRs.Update()


                        REM MoveToDb(MyRsSrc.Fields("NumeroRegistrazione"), Numero)
                        REM MoveToDb(MyRsSrc.Fields("Elaborato"), 1)
                        REM MoveToDb(MyRsSrc.Fields("UTENTE"), Session("UTENTE"))
                        REM MoveToDb(MyRsSrc.Fields("DataAggiornamento"), Now)
                        REM MyRsSrc.Update()
                    End If
                    If MoveFromDb(MyRsSrc.Fields("TipoMov")) = "AC" And MoveFromDb(MyRsSrc.Fields("Riferimento")) = "P" And Val(CODICEPARENTE) > 0 Then
                        MyRs.AddNew()
                        MoveToDb(MyRs.Fields("Utente"), "NEWAUTONCFAT")
                        MoveToDb(MyRs.Fields("DataAggiornamento"), Now)
                        MoveToDb(MyRs.Fields("Numero"), Numero)
                        MoveToDb(MyRs.Fields("RigaDaCausale"), 2)
                        MoveToDb(MyRs.Fields("MastroPartita"), XCau.Righe(2 - 1).Mastro)
                        MoveToDb(MyRs.Fields("ContoPartita"), XCau.Righe(2 - 1).Conto)
                        MoveToDb(MyRs.Fields("SottoContoPartita"), XCau.Righe(2 - 1).Sottoconto)

                        MoveToDb(MyRs.Fields("MastroControPartita"), MastroParente(Session("CODICESERVIZIO"), Session("CODICEOSPITE"), MoveFromDb(MyRsSrc.Fields("Parente"))))
                        MoveToDb(MyRs.Fields("ContoControPartita"), ContoParente(Session("CODICESERVIZIO"), Session("CODICEOSPITE"), MoveFromDb(MyRsSrc.Fields("Parente"))))
                        MoveToDb(MyRs.Fields("SottocontoControPartita"), SottoContoParente(Session("CODICESERVIZIO"), Session("CODICEOSPITE"), MoveFromDb(MyRsSrc.Fields("Parente"))))

                        MoveToDb(MyRs.Fields("Segno"), XCau.Righe(2 - 1).Segno)
                        If XCau.Righe(2 - 1).DareAvere = "A" Then
                            MoveToDb(MyRs.Fields("DareAvere"), "D")
                        Else
                            MoveToDb(MyRs.Fields("DareAvere"), "A")
                        End If
                        MoveToDb(MyRs.Fields("Tipo"), "IV")

                        Dim CodiceIvaAddebito As String = CampoTipoAddebito(MoveFromDb(MyRsSrc.Fields("CodiceIva")), "CodiceIVA")

                        If CodiceIvaAddebito = "" Then
                            CodiceIvaAddebito = CausaleIVA
                        End If

                        MoveToDb(MyRs.Fields("CodiceIva"), CodiceIvaAddebito)

                        If IvaInclusaNelTotale = 1 Then
                            Dim MyIva As New Cls_IVA

                            MyIva.Leggi(Session("DC_TABELLE"), CampoTipoAddebito(MoveFromDb(MyRsSrc.Fields("CodiceIva")), "CodiceIVA"))

                            MoveToDb(MyRs.Fields("Imponibile"), Modulo.MathRound((MoveFromDb(MyRsSrc.Fields("Importo")) * 100) / ((MyIva.Aliquota * 100) + 100), 2))
                            MoveToDb(MyRs.Fields("Importo"), MoveFromDb(MyRsSrc.Fields("Importo")) - MoveFromDb(MyRs.Fields("Imponibile")))
                        Else
                            Dim MyIva As New Cls_IVA

                            MyIva.Leggi(Session("DC_TABELLE"), CampoTipoAddebito(MoveFromDb(MyRsSrc.Fields("CodiceIva")), "CodiceIVA"))

                            MoveToDb(MyRs.Fields("Imponibile"), MoveFromDb(MyRsSrc.Fields("Importo")))
                            MoveToDb(MyRs.Fields("Importo"), MyIva.Aliquota * MoveFromDb(MyRsSrc.Fields("Importo")))
                        End If

                        MoveToDb(MyRs.Fields("AnnoRiferimento"), 0)
                        MoveToDb(MyRs.Fields("MeseRiferimento"), 0)
                        MoveToDb(MyRs.Fields("Quantita"), 0)
                        MoveToDb(MyRs.Fields("Descrizione"), MoveFromDb(MyRsSrc.Fields("Descrizione")))
                        MoveToDb(MyRs.Fields("UTENTE"), Session("UTENTE"))
                        MoveToDb(MyRs.Fields("DataAggiornamento"), Now)
                        MyRs.Update()

                        MyRs.AddNew()
                        MoveToDb(MyRs.Fields("Utente"), "AUTONCFAT")
                        MoveToDb(MyRs.Fields("DataAggiornamento"), Now)
                        MoveToDb(MyRs.Fields("Numero"), Numero)
                        MoveToDb(MyRs.Fields("RigaDaCausale"), 6)
                        MoveToDb(MyRs.Fields("MastroPartita"), XCau.Righe(3 - 1).Mastro)
                        MoveToDb(MyRs.Fields("ContoPartita"), XCau.Righe(3 - 1).Conto)
                        MoveToDb(MyRs.Fields("SottoContoPartita"), XCau.Righe(3 - 1).Sottoconto)

                        MoveToDb(MyRs.Fields("MastroControPartita"), MastroParente(Session("CODICESERVIZIO"), Session("CODICEOSPITE"), MoveFromDb(MyRsSrc.Fields("Parente"))))
                        MoveToDb(MyRs.Fields("ContoControPartita"), ContoParente(Session("CODICESERVIZIO"), Session("CODICEOSPITE"), MoveFromDb(MyRsSrc.Fields("Parente"))))
                        MoveToDb(MyRs.Fields("SottocontoControPartita"), SottoContoParente(Session("CODICESERVIZIO"), Session("CODICEOSPITE"), MoveFromDb(MyRsSrc.Fields("Parente"))))

                        If MoveFromDbWC(MyRsSrc, "CODICEIVA") <> "" Then
                            MoveToDb(MyRs.Fields("MastroPartita"), CampoTipoAddebito(MoveFromDbWC(MyRsSrc, "CODICEIVA"), "Mastro"))
                            MoveToDb(MyRs.Fields("ContoPartita"), CampoTipoAddebito(MoveFromDbWC(MyRsSrc, "CODICEIVA"), "Conto"))
                            MoveToDb(MyRs.Fields("SottoContoPartita"), CampoTipoAddebito(MoveFromDbWC(MyRsSrc, "CODICEIVA"), "SottoConto"))
                        End If

                        MoveToDb(MyRs.Fields("Segno"), XCau.Righe(3 - 1).Segno)
                        If XCau.Righe(3 - 1).DareAvere = "A" Then
                            MoveToDb(MyRs.Fields("DareAvere"), "D")
                        Else
                            MoveToDb(MyRs.Fields("DareAvere"), "A")
                        End If
                        MoveToDb(MyRs.Fields("Tipo"), "")
                        MoveToDb(MyRs.Fields("CodiceIva"), CodiceIvaAddebito)

                        If IvaInclusaNelTotale = 1 Then
                            Dim MyIva As New Cls_IVA

                            MyIva.Leggi(Session("DC_TABELLE"), CampoTipoAddebito(MoveFromDb(MyRsSrc.Fields("CodiceIva")), "CodiceIVA"))

                            MoveToDb(MyRs.Fields("Importo"), Modulo.MathRound((MoveFromDb(MyRsSrc.Fields("Importo")) * 100) / ((MyIva.Aliquota * 100) + 100), 2))
                        Else
                            MoveToDb(MyRs.Fields("Importo"), MoveFromDb(MyRsSrc.Fields("Importo")))
                        End If

                        MoveToDb(MyRs.Fields("AnnoRiferimento"), Txt_Anno.Text)
                        MoveToDb(MyRs.Fields("MeseRiferimento"), Mese)
                        MoveToDb(MyRs.Fields("Quantita"), 0)
                        MoveToDb(MyRs.Fields("Descrizione"), MoveFromDb(MyRsSrc.Fields("Descrizione")))
                        MoveToDb(MyRs.Fields("UTENTE"), Session("UTENTE"))
                        MoveToDb(MyRs.Fields("DataAggiornamento"), Now)
                        MyRs.Update()

                        REM MoveToDb(MyRsSrc.Fields("NumeroRegistrazione"), Numero)
                        REM MoveToDb(MyRsSrc.Fields("Elaborato"), 1)
                        REM MoveToDb(MyRsSrc.Fields("UTENTE"), Session("UTENTE"))
                        REM MoveToDb(MyRsSrc.Fields("DataAggiornamento"), Now)
                        REM MyRsSrc.Update()
                    End If
                End If
                MyRsSrc.MoveNext()
            Loop
            MyRsSrc.Close()
        End If

        Dim Rs_MyRegistrazioneRiga As New ADODB.Recordset


        If CODICEPARENTE = 0 Then
            For i = 0 To 40
                If Modulo.MathRound(Emr_ImpExtra(i), 2) <> 0 Then
                    MySql = "Select * from Temp_MovimentiContabiliRiga where Numero = " & Numero & " And RigaDaCausale = 999"
                    Rs_MyRegistrazioneRiga.Open(MySql, GeneraleDb, ADODB.CursorTypeEnum.adOpenKeyset, ADODB.LockTypeEnum.adLockOptimistic)

                    Rs_MyRegistrazioneRiga.AddNew()
                    MoveToDb(Rs_MyRegistrazioneRiga.Fields("Numero"), Numero)
                    If Emr_ExtraMastro(i) = 0 Then
                        MoveToDb(Rs_MyRegistrazioneRiga.Fields("MastroPartita"), XCau.Righe(3).Mastro)
                        MoveToDb(Rs_MyRegistrazioneRiga.Fields("ContoPartita"), XCau.Righe(3).Conto)
                        MoveToDb(Rs_MyRegistrazioneRiga.Fields("SottoContoPartita"), XCau.Righe(3).Sottoconto)
                    Else
                        MoveToDb(Rs_MyRegistrazioneRiga.Fields("MastroPartita"), Emr_ExtraMastro(i))
                        MoveToDb(Rs_MyRegistrazioneRiga.Fields("ContoPartita"), Emr_ExtraConto(i))
                        MoveToDb(Rs_MyRegistrazioneRiga.Fields("SottoContoPartita"), Emr_ExtraSottoConto(i))
                    End If

                    If Modulo.MathRound(Emr_ImpExtra(i), 2) > 0 Then
                        MoveToDb(Rs_MyRegistrazioneRiga.Fields("DareAvere"), XCau.Righe(3).DareAvere)
                    Else
                        If XCau.Righe(3).DareAvere = "D" Then
                            MoveToDb(Rs_MyRegistrazioneRiga.Fields("DareAvere"), "A")
                        Else
                            MoveToDb(Rs_MyRegistrazioneRiga.Fields("DareAvere"), "D")
                        End If
                    End If



                    If IvaInclusaNelTotale = 1 Then
                        Dim MyIva As New Cls_IVA

                        MyIva.Codice = Emr_CodivaExtra(i)
                        MyIva.Leggi(Session("DC_TABELLE"), MyIva.Codice)


                        MoveToDb(Rs_MyRegistrazioneRiga.Fields("Importo"), Modulo.MathRound(Math.Abs(Emr_ImpExtra(i)) / (MyIva.Aliquota + 1), 2))
                    Else
                        MoveToDb(Rs_MyRegistrazioneRiga.Fields("Importo"), Math.Abs(Modulo.MathRound(Emr_ImpExtra(i), 2)))
                    End If

                    'MoveToDb Rs_MyRegistrazioneRiga.Fields("Importo"), math.Abs(Modulo.MathRound(Emr_ImpExtra(I), DecEuro))

                    MoveToDb(Rs_MyRegistrazioneRiga.Fields("CodiceIva"), Emr_CodivaExtra(i))
                    MoveToDb(Rs_MyRegistrazioneRiga.Fields("Segno"), "+")
                    MoveToDb(Rs_MyRegistrazioneRiga.Fields("RigaDaCausale"), 4)
                    MoveToDb(Rs_MyRegistrazioneRiga.Fields("Quantita"), Emr_GGExtra(i))
                    If IffNC = "NC" Then
                        If Chk_SoloMese.Checked = True Then
                            MoveToDb(Rs_MyRegistrazioneRiga.Fields("Quantita"), Giorni)
                        Else
                            MoveToDb(Rs_MyRegistrazioneRiga.Fields("Quantita"), Giorni + GiorniRpx)
                        End If
                    End If

                    MoveToDb(Rs_MyRegistrazioneRiga.Fields("TipoExtra"), "E" & Emr_CodiceExtra(i))

                    MoveToDb(Rs_MyRegistrazioneRiga.Fields("CENTROSERVIZIO"), Session("CODICESERVIZIO"))
                    MoveToDb(Rs_MyRegistrazioneRiga.Fields("Utente"), "CREAFAT")

                    MoveToDb(Rs_MyRegistrazioneRiga.Fields("DataAggiornamento"), Now)
                    MoveToDb(Rs_MyRegistrazioneRiga.Fields("Descrizione"), Emr_DesExtra(i))

                    If MoveFromDb(Rs_MyRegistrazioneRiga.Fields("SottoContoPartita")) = 0 And MoveFromDb(Rs_MyRegistrazioneRiga.Fields("ContoPartita")) And _
                       MoveFromDb(Rs_MyRegistrazioneRiga.Fields("MastroPartita")) = 0 Then
                        'StringaErrore = StringaErrore & " Errore mastro,conto,sottoconto partitata causale " & CausaleRetta & " inesistente"
                    End If

                    Rs_MyRegistrazioneRiga.Update()

                    Rs_MyRegistrazioneRiga.AddNew()
                    MoveToDb(Rs_MyRegistrazioneRiga.Fields("Numero"), Numero)

                    MoveToDb(Rs_MyRegistrazioneRiga.Fields("MastroPartita"), XCau.Righe(1).Mastro)
                    MoveToDb(Rs_MyRegistrazioneRiga.Fields("ContoPartita"), XCau.Righe(1).Conto)
                    MoveToDb(Rs_MyRegistrazioneRiga.Fields("SottoContoPartita"), XCau.Righe(1).Sottoconto)

                    Dim MyIvaAP As New Cls_IVA

                    MyIvaAP.Codice = Emr_CodivaExtra(i)
                    MyIvaAP.Leggi(Session("DC_TABELLE"), MyIvaAP.Codice)
                    MoveToDb(Rs_MyRegistrazioneRiga.Fields("Importo"), Math.Abs(Modulo.MathRound(MyIvaAP.Aliquota * Math.Abs(Emr_ImpExtra(i)), 2)))


                    If Modulo.MathRound(Emr_ImpExtra(i), 2) > 0 Then
                        MoveToDb(Rs_MyRegistrazioneRiga.Fields("DareAvere"), XCau.Righe(2).DareAvere)
                    Else
                        If XCau.Righe(2).DareAvere = "D" Then
                            MoveToDb(Rs_MyRegistrazioneRiga.Fields("DareAvere"), "A")
                        Else
                            MoveToDb(Rs_MyRegistrazioneRiga.Fields("DareAvere"), "D")
                        End If
                    End If

                    MoveToDb(Rs_MyRegistrazioneRiga.Fields("Tipo"), "IV")
                    MoveToDb(Rs_MyRegistrazioneRiga.Fields("CodiceIva"), Emr_CodivaExtra(i))

                    If IvaInclusaNelTotale = 1 Then
                        MoveToDb(Rs_MyRegistrazioneRiga.Fields("Imponibile"), Math.Abs(Modulo.MathRound((Math.Abs(Emr_ImpExtra(i)) / (MyIvaAP.Aliquota + 1)), 2)))
                    Else
                        MoveToDb(Rs_MyRegistrazioneRiga.Fields("Imponibile"), Math.Abs(Emr_ImpExtra(i)))
                    End If

                    MoveToDb(Rs_MyRegistrazioneRiga.Fields("Segno"), "+")
                    MoveToDb(Rs_MyRegistrazioneRiga.Fields("RigaDaCausale"), 2)

                    If MoveFromDb(Rs_MyRegistrazioneRiga.Fields("SottoContoPartita")) = 0 And MoveFromDb(Rs_MyRegistrazioneRiga.Fields("ContoPartita")) And _
                       MoveFromDb(Rs_MyRegistrazioneRiga.Fields("MastroPartita")) = 0 Then
                        'StringaErrore = StringaErrore & " Errore mastro,conto,sottoconto partitata causale " & CausaleRetta & " inesistente"
                    End If

                    'If ImportoRetta + ImportoExtra + ImportoAddebiti - ImportoAccrediti < 0 Then
                    '    If MoveFromDbWC(Rs_CausaleRiga, "DareAvere") = "D" Then
                    '        MoveToDb(Rs_MyRegistrazioneRiga.Fields("DareAvere"), "A")
                    '    Else
                    '        MoveToDb(Rs_MyRegistrazioneRiga.Fields("DareAvere"), "D")
                    '    End If
                    'End If



                    MoveToDb(Rs_MyRegistrazioneRiga.Fields("Utente"), "CREAFAT")

                    MoveToDb(Rs_MyRegistrazioneRiga.Fields("DataAggiornamento"), Now)
                    Rs_MyRegistrazioneRiga.Update()

                    Rs_MyRegistrazioneRiga.Close()
                End If
            Next i


            If Session("IMPORTOOSPMAN1") > 0 Then
                Dim CODICE_MANUALE As String = Session("CODICEOSPMAN1")
                Dim IMPORTO_MANUALE As Double = Session("IMPORTOOSPMAN1")
                MyRs.AddNew()
                MoveToDb(MyRs.Fields("Utente"), "NEWAUTONCFAT")
                MoveToDb(MyRs.Fields("DataAggiornamento"), Now)
                MoveToDb(MyRs.Fields("Numero"), Numero)
                MoveToDb(MyRs.Fields("RigaDaCausale"), 2)
                MoveToDb(MyRs.Fields("MastroPartita"), XCau.Righe(2 - 1).Mastro)
                MoveToDb(MyRs.Fields("ContoPartita"), XCau.Righe(2 - 1).Conto)
                MoveToDb(MyRs.Fields("SottoContoPartita"), XCau.Righe(2 - 1).Sottoconto)

                MoveToDb(MyRs.Fields("MastroControPartita"), MastroOspite(Session("CODICESERVIZIO"), Session("CODICEOSPITE")))
                MoveToDb(MyRs.Fields("ContoControPartita"), ContoOspite(Session("CODICESERVIZIO"), Session("CODICEOSPITE")))
                MoveToDb(MyRs.Fields("SottocontoControPartita"), SottoContoOspite(Session("CODICESERVIZIO"), Session("CODICEOSPITE")))

                MoveToDb(MyRs.Fields("Segno"), XCau.Righe(2 - 1).Segno)
                MoveToDb(MyRs.Fields("DareAvere"), XCau.Righe(2 - 1).DareAvere)
                MoveToDb(MyRs.Fields("Tipo"), "IV")
                MoveToDb(MyRs.Fields("CodiceIva"), CampoTipoAddebito(CODICE_MANUALE, "CodiceIVA"))

                If IvaInclusaNelTotale = 1 Then
                    Dim MyIva As New Cls_IVA

                    MyIva.Leggi(Session("DC_TABELLE"), CampoTipoAddebito(CODICE_MANUALE, "CodiceIVA"))

                    MoveToDb(MyRs.Fields("Imponibile"), Modulo.MathRound((IMPORTO_MANUALE * 100) / ((MyIva.Aliquota * 100) + 100), 2))
                    MoveToDb(MyRs.Fields("Importo"), IMPORTO_MANUALE - MoveFromDb(MyRs.Fields("Imponibile")))
                Else
                    Dim MyIva As New Cls_IVA

                    MyIva.Leggi(Session("DC_TABELLE"), CampoTipoAddebito(CODICE_MANUALE, "CodiceIVA"))

                    MoveToDb(MyRs.Fields("Imponibile"), IMPORTO_MANUALE)
                    MoveToDb(MyRs.Fields("Importo"), MyIva.Aliquota * IMPORTO_MANUALE)
                End If

                MoveToDb(MyRs.Fields("AnnoRiferimento"), 0)
                MoveToDb(MyRs.Fields("MeseRiferimento"), 0)
                MoveToDb(MyRs.Fields("Quantita"), 0)
                MoveToDb(MyRs.Fields("Descrizione"), "")
                MoveToDb(MyRs.Fields("UTENTE"), Session("UTENTE"))
                MoveToDb(MyRs.Fields("DataAggiornamento"), Now)
                MyRs.Update()

                MyRs.AddNew()
                MoveToDb(MyRs.Fields("Utente"), "AUTONCFAT")
                MoveToDb(MyRs.Fields("DataAggiornamento"), Now)
                MoveToDb(MyRs.Fields("Numero"), Numero)
                MoveToDb(MyRs.Fields("RigaDaCausale"), 3)
                MoveToDb(MyRs.Fields("MastroPartita"), XCau.Righe(3 - 1).Mastro)
                MoveToDb(MyRs.Fields("ContoPartita"), XCau.Righe(3 - 1).Conto)
                MoveToDb(MyRs.Fields("SottoContoPartita"), XCau.Righe(3 - 1).Sottoconto)


                If CODICE_MANUALE <> "" Then
                    MoveToDb(MyRs.Fields("MastroPartita"), CampoTipoAddebito(CODICE_MANUALE, "Mastro"))
                    MoveToDb(MyRs.Fields("ContoPartita"), CampoTipoAddebito(CODICE_MANUALE, "Conto"))
                    MoveToDb(MyRs.Fields("SottoContoPartita"), CampoTipoAddebito(CODICE_MANUALE, "SottoConto"))
                End If

                MoveToDb(MyRs.Fields("MastroControPartita"), MastroOspite(Session("CODICESERVIZIO"), Session("CODICEOSPITE")))
                MoveToDb(MyRs.Fields("ContoControPartita"), ContoOspite(Session("CODICESERVIZIO"), Session("CODICEOSPITE")))
                MoveToDb(MyRs.Fields("SottocontoControPartita"), SottoContoOspite(Session("CODICESERVIZIO"), Session("CODICEOSPITE")))

                MoveToDb(MyRs.Fields("Segno"), XCau.Righe(3 - 1).Segno)
                MoveToDb(MyRs.Fields("DareAvere"), XCau.Righe(3 - 1).DareAvere)
                MoveToDb(MyRs.Fields("Tipo"), "")
                MoveToDb(MyRs.Fields("CodiceIva"), CampoTipoAddebito(CODICE_MANUALE, "CodiceIVA"))


                If IvaInclusaNelTotale = 1 Then
                    Dim MyIva As New Cls_IVA

                    MyIva.Leggi(Session("DC_TABELLE"), CampoTipoAddebito(CODICE_MANUALE, "CodiceIVA"))

                    MoveToDb(MyRs.Fields("Importo"), Modulo.MathRound((IMPORTO_MANUALE * 100) / ((MyIva.Aliquota * 100) + 100), 2))
                Else
                    MoveToDb(MyRs.Fields("Importo"), IMPORTO_MANUALE)
                End If

                MoveToDb(MyRs.Fields("AnnoRiferimento"), Txt_Anno.Text)
                MoveToDb(MyRs.Fields("MeseRiferimento"), Mese)
                MoveToDb(MyRs.Fields("Quantita"), 0)
                MoveToDb(MyRs.Fields("Descrizione"), "")
                MoveToDb(MyRs.Fields("UTENTE"), Session("UTENTE"))
                MoveToDb(MyRs.Fields("DataAggiornamento"), Now)
                MyRs.Update()
            End If

            If Session("IMPORTOOSPMAN2") > 0 Then
                Dim CODICE_MANUALE As String = Session("CODICEOSPMAN2")
                Dim IMPORTO_MANUALE As Double = Session("IMPORTOOSPMAN2")
                MyRs.AddNew()
                MoveToDb(MyRs.Fields("Utente"), "NEWAUTONCFAT")
                MoveToDb(MyRs.Fields("DataAggiornamento"), Now)
                MoveToDb(MyRs.Fields("Numero"), Numero)
                MoveToDb(MyRs.Fields("RigaDaCausale"), 2)
                MoveToDb(MyRs.Fields("MastroPartita"), XCau.Righe(2 - 1).Mastro)
                MoveToDb(MyRs.Fields("ContoPartita"), XCau.Righe(2 - 1).Conto)
                MoveToDb(MyRs.Fields("SottoContoPartita"), XCau.Righe(2 - 1).Sottoconto)

                MoveToDb(MyRs.Fields("MastroControPartita"), MastroOspite(Session("CODICESERVIZIO"), Session("CODICEOSPITE")))
                MoveToDb(MyRs.Fields("ContoControPartita"), ContoOspite(Session("CODICESERVIZIO"), Session("CODICEOSPITE")))
                MoveToDb(MyRs.Fields("SottocontoControPartita"), SottoContoOspite(Session("CODICESERVIZIO"), Session("CODICEOSPITE")))

                MoveToDb(MyRs.Fields("Segno"), XCau.Righe(2 - 1).Segno)
                MoveToDb(MyRs.Fields("DareAvere"), XCau.Righe(2 - 1).DareAvere)
                MoveToDb(MyRs.Fields("Tipo"), "IV")
                MoveToDb(MyRs.Fields("CodiceIva"), CampoTipoAddebito(CODICE_MANUALE, "CodiceIVA"))

                If IvaInclusaNelTotale = 1 Then
                    Dim MyIva As New Cls_IVA

                    MyIva.Leggi(Session("DC_TABELLE"), CampoTipoAddebito(CODICE_MANUALE, "CodiceIVA"))

                    MoveToDb(MyRs.Fields("Imponibile"), Modulo.MathRound((IMPORTO_MANUALE * 100) / ((MyIva.Aliquota * 100) + 100), 2))
                    MoveToDb(MyRs.Fields("Importo"), IMPORTO_MANUALE - MoveFromDb(MyRs.Fields("Imponibile")))
                Else
                    Dim MyIva As New Cls_IVA

                    MyIva.Leggi(Session("DC_TABELLE"), CampoTipoAddebito(CODICE_MANUALE, "CodiceIVA"))

                    MoveToDb(MyRs.Fields("Imponibile"), IMPORTO_MANUALE)
                    MoveToDb(MyRs.Fields("Importo"), MyIva.Aliquota * IMPORTO_MANUALE)
                End If

                MoveToDb(MyRs.Fields("AnnoRiferimento"), 0)
                MoveToDb(MyRs.Fields("MeseRiferimento"), 0)
                MoveToDb(MyRs.Fields("Quantita"), 0)
                MoveToDb(MyRs.Fields("Descrizione"), "")
                MoveToDb(MyRs.Fields("UTENTE"), Session("UTENTE"))
                MoveToDb(MyRs.Fields("DataAggiornamento"), Now)
                MyRs.Update()

                MyRs.AddNew()
                MoveToDb(MyRs.Fields("Utente"), "AUTONCFAT")
                MoveToDb(MyRs.Fields("DataAggiornamento"), Now)
                MoveToDb(MyRs.Fields("Numero"), Numero)
                MoveToDb(MyRs.Fields("RigaDaCausale"), 3)
                MoveToDb(MyRs.Fields("MastroPartita"), XCau.Righe(3 - 1).Mastro)
                MoveToDb(MyRs.Fields("ContoPartita"), XCau.Righe(3 - 1).Conto)
                MoveToDb(MyRs.Fields("SottoContoPartita"), XCau.Righe(3 - 1).Sottoconto)


                If CODICE_MANUALE <> "" Then
                    MoveToDb(MyRs.Fields("MastroPartita"), CampoTipoAddebito(CODICE_MANUALE, "Mastro"))
                    MoveToDb(MyRs.Fields("ContoPartita"), CampoTipoAddebito(CODICE_MANUALE, "Conto"))
                    MoveToDb(MyRs.Fields("SottoContoPartita"), CampoTipoAddebito(CODICE_MANUALE, "SottoConto"))
                End If

                MoveToDb(MyRs.Fields("MastroControPartita"), MastroOspite(Session("CODICESERVIZIO"), Session("CODICEOSPITE")))
                MoveToDb(MyRs.Fields("ContoControPartita"), ContoOspite(Session("CODICESERVIZIO"), Session("CODICEOSPITE")))
                MoveToDb(MyRs.Fields("SottocontoControPartita"), SottoContoOspite(Session("CODICESERVIZIO"), Session("CODICEOSPITE")))

                MoveToDb(MyRs.Fields("Segno"), XCau.Righe(3 - 1).Segno)
                MoveToDb(MyRs.Fields("DareAvere"), XCau.Righe(3 - 1).DareAvere)
                MoveToDb(MyRs.Fields("Tipo"), "")
                MoveToDb(MyRs.Fields("CodiceIva"), CampoTipoAddebito(CODICE_MANUALE, "CodiceIVA"))


                If IvaInclusaNelTotale = 1 Then
                    Dim MyIva As New Cls_IVA

                    MyIva.Leggi(Session("DC_TABELLE"), CampoTipoAddebito(CODICE_MANUALE, "CodiceIVA"))

                    MoveToDb(MyRs.Fields("Importo"), Modulo.MathRound((IMPORTO_MANUALE * 100) / ((MyIva.Aliquota * 100) + 100), 2))
                Else
                    MoveToDb(MyRs.Fields("Importo"), IMPORTO_MANUALE)
                End If

                MoveToDb(MyRs.Fields("AnnoRiferimento"), Txt_Anno.Text)
                MoveToDb(MyRs.Fields("MeseRiferimento"), Mese)
                MoveToDb(MyRs.Fields("Quantita"), 0)
                MoveToDb(MyRs.Fields("Descrizione"), "")
                MoveToDb(MyRs.Fields("UTENTE"), Session("UTENTE"))
                MoveToDb(MyRs.Fields("DataAggiornamento"), Now)
                MyRs.Update()
            End If
        Else

            Emr_P_ImpExtra = Session("Emr_P_ImpExtra")
            Emr_P_GGExtra = Session("Emr_P_GGExtra")
            Emr_P_CodivaExtra = Session("Emr_P_CodivaExtra")
            Emr_P_CodiceExtra = Session("Emr_P_CodiceExtra")
            Emr_P_ExtraMastro = Session("Emr_P_ExtraMastro")
            Emr_P_ExtraConto = Session("Emr_P_ExtraConto")
            Emr_P_ExtraSottoConto = Session("Emr_P_ExtraSottoConto")
            Emr_P_DesExtra = Session("Emr_P_DesExtra")


            For i = 0 To 40
                If Modulo.MathRound(Emr_P_ImpExtra(CODICEPARENTE, i), 2) <> 0 Then
                    MySql = "Select * from Temp_MovimentiContabiliRiga where Numero = " & Numero & " And RigaDaCausale = 999"
                    Rs_MyRegistrazioneRiga.Open(MySql, GeneraleDb, ADODB.CursorTypeEnum.adOpenKeyset, ADODB.LockTypeEnum.adLockOptimistic)

                    Rs_MyRegistrazioneRiga.AddNew()
                    MoveToDb(Rs_MyRegistrazioneRiga.Fields("Numero"), Numero)
                    If Emr_P_ExtraMastro(CODICEPARENTE, i) = 0 Then
                        MoveToDb(Rs_MyRegistrazioneRiga.Fields("MastroPartita"), XCau.Righe(3).Mastro)
                        MoveToDb(Rs_MyRegistrazioneRiga.Fields("ContoPartita"), XCau.Righe(3).Conto)
                        MoveToDb(Rs_MyRegistrazioneRiga.Fields("SottoContoPartita"), XCau.Righe(3).Sottoconto)
                    Else
                        MoveToDb(Rs_MyRegistrazioneRiga.Fields("MastroPartita"), Emr_P_ExtraMastro(CODICEPARENTE, i))
                        MoveToDb(Rs_MyRegistrazioneRiga.Fields("ContoPartita"), Emr_P_ExtraConto(CODICEPARENTE, i))
                        MoveToDb(Rs_MyRegistrazioneRiga.Fields("SottoContoPartita"), Emr_P_ExtraSottoConto(CODICEPARENTE, i))
                    End If

                    If Modulo.MathRound(Emr_P_ImpExtra(CODICEPARENTE, i), 2) > 0 Then
                        MoveToDb(Rs_MyRegistrazioneRiga.Fields("DareAvere"), XCau.Righe(3).DareAvere)
                    Else
                        If XCau.Righe(3).DareAvere = "D" Then
                            MoveToDb(Rs_MyRegistrazioneRiga.Fields("DareAvere"), "A")
                        Else
                            MoveToDb(Rs_MyRegistrazioneRiga.Fields("DareAvere"), "D")
                        End If
                    End If



                    If IvaInclusaNelTotale = 1 Then
                        Dim MyIva As New Cls_IVA

                        MyIva.Codice = Emr_P_CodivaExtra(CODICEPARENTE, i)
                        MyIva.Leggi(Session("DC_TABELLE"), MyIva.Codice)


                        MoveToDb(Rs_MyRegistrazioneRiga.Fields("Importo"), Modulo.MathRound(Math.Abs(Emr_P_ImpExtra(CODICEPARENTE, i)) / (MyIva.Aliquota + 1), 2))
                    Else
                        MoveToDb(Rs_MyRegistrazioneRiga.Fields("Importo"), Math.Abs(Modulo.MathRound(Emr_P_ImpExtra(CODICEPARENTE, i), 2)))
                    End If

                    'MoveToDb Rs_MyRegistrazioneRiga.Fields("Importo"), math.Abs(Modulo.MathRound(Emr_ImpExtra(I), DecEuro))

                    MoveToDb(Rs_MyRegistrazioneRiga.Fields("CodiceIva"), Emr_P_CodivaExtra(CODICEPARENTE, i))
                    MoveToDb(Rs_MyRegistrazioneRiga.Fields("Segno"), "+")
                    MoveToDb(Rs_MyRegistrazioneRiga.Fields("RigaDaCausale"), 4)
                    MoveToDb(Rs_MyRegistrazioneRiga.Fields("Quantita"), Emr_P_GGExtra(CODICEPARENTE, i))
                    If IffNC = "NC" Then
                        If Chk_SoloMese.Checked = True Then
                            MoveToDb(Rs_MyRegistrazioneRiga.Fields("Quantita"), Giorni)
                        Else
                            MoveToDb(Rs_MyRegistrazioneRiga.Fields("Quantita"), Giorni + GiorniRpx)
                        End If
                    End If
                    MoveToDb(Rs_MyRegistrazioneRiga.Fields("TipoExtra"), "E" & Emr_P_CodiceExtra(CODICEPARENTE, i))
                    MoveToDb(Rs_MyRegistrazioneRiga.Fields("CENTROSERVIZIO"), Session("CODICESERVIZIO"))


                    MoveToDb(Rs_MyRegistrazioneRiga.Fields("Utente"), "CREAFAT")

                    MoveToDb(Rs_MyRegistrazioneRiga.Fields("DataAggiornamento"), Now)
                    MoveToDb(Rs_MyRegistrazioneRiga.Fields("Descrizione"), Emr_P_DesExtra(CODICEPARENTE, i))

                    If MoveFromDb(Rs_MyRegistrazioneRiga.Fields("SottoContoPartita")) = 0 And MoveFromDb(Rs_MyRegistrazioneRiga.Fields("ContoPartita")) And _
                       MoveFromDb(Rs_MyRegistrazioneRiga.Fields("MastroPartita")) = 0 Then
                        'StringaErrore = StringaErrore & " Errore mastro,conto,sottoconto partitata causale " & CausaleRetta & " inesistente"
                    End If

                    Rs_MyRegistrazioneRiga.Update()

                    Rs_MyRegistrazioneRiga.AddNew()
                    MoveToDb(Rs_MyRegistrazioneRiga.Fields("Numero"), Numero)

                    MoveToDb(Rs_MyRegistrazioneRiga.Fields("MastroPartita"), XCau.Righe(1).Mastro)
                    MoveToDb(Rs_MyRegistrazioneRiga.Fields("ContoPartita"), XCau.Righe(1).Conto)
                    MoveToDb(Rs_MyRegistrazioneRiga.Fields("SottoContoPartita"), XCau.Righe(1).Sottoconto)

                    Dim MyIvaAP As New Cls_IVA

                    MyIvaAP.Codice = Emr_P_CodivaExtra(CODICEPARENTE, i)
                    MyIvaAP.Leggi(Session("DC_TABELLE"), MyIvaAP.Codice)
                    MoveToDb(Rs_MyRegistrazioneRiga.Fields("Importo"), Math.Abs(Modulo.MathRound(MyIvaAP.Aliquota * Math.Abs(Emr_P_ImpExtra(CODICEPARENTE, i)), 2)))




                    If Modulo.MathRound(Emr_P_ImpExtra(CODICEPARENTE, i), 2) > 0 Then
                        MoveToDb(Rs_MyRegistrazioneRiga.Fields("DareAvere"), XCau.Righe(2).DareAvere)
                    Else
                        If XCau.Righe(2).DareAvere = "D" Then
                            MoveToDb(Rs_MyRegistrazioneRiga.Fields("DareAvere"), "A")
                        Else
                            MoveToDb(Rs_MyRegistrazioneRiga.Fields("DareAvere"), "D")
                        End If
                    End If

                    MoveToDb(Rs_MyRegistrazioneRiga.Fields("Tipo"), "IV")
                    MoveToDb(Rs_MyRegistrazioneRiga.Fields("CodiceIva"), Emr_P_CodivaExtra(CODICEPARENTE, i))

                    If IvaInclusaNelTotale = 1 Then
                        MoveToDb(Rs_MyRegistrazioneRiga.Fields("Imponibile"), Math.Abs(Modulo.MathRound((Math.Abs(Emr_P_ImpExtra(CODICEPARENTE, i)) / (MyIvaAP.Aliquota + 1)), 2)))
                    Else
                        MoveToDb(Rs_MyRegistrazioneRiga.Fields("Imponibile"), Math.Abs(Emr_P_ImpExtra(CODICEPARENTE, i)))
                    End If

                    MoveToDb(Rs_MyRegistrazioneRiga.Fields("Segno"), "+")
                    MoveToDb(Rs_MyRegistrazioneRiga.Fields("RigaDaCausale"), 2)

                    If MoveFromDb(Rs_MyRegistrazioneRiga.Fields("SottoContoPartita")) = 0 And MoveFromDb(Rs_MyRegistrazioneRiga.Fields("ContoPartita")) And _
                       MoveFromDb(Rs_MyRegistrazioneRiga.Fields("MastroPartita")) = 0 Then
                        'StringaErrore = StringaErrore & " Errore mastro,conto,sottoconto partitata causale " & CausaleRetta & " inesistente"
                    End If

                    'If ImportoRetta + ImportoExtra + ImportoAddebiti - ImportoAccrediti < 0 Then
                    '    If MoveFromDbWC(Rs_CausaleRiga, "DareAvere") = "D" Then
                    '        MoveToDb(Rs_MyRegistrazioneRiga.Fields("DareAvere"), "A")
                    '    Else
                    '        MoveToDb(Rs_MyRegistrazioneRiga.Fields("DareAvere"), "D")
                    '    End If
                    'End If



                    MoveToDb(Rs_MyRegistrazioneRiga.Fields("Utente"), "CREAFAT")

                    MoveToDb(Rs_MyRegistrazioneRiga.Fields("DataAggiornamento"), Now)
                    Rs_MyRegistrazioneRiga.Update()

                    Rs_MyRegistrazioneRiga.Close()
                End If
            Next i

            If Session("IMPORTOPARMAN1-" & CODICEPARENTE) > 0 Then
                Dim CODICE_MANUALE As String = Session("CODICEPARMAN1-" & CODICEPARENTE)
                Dim IMPORTO_MANUALE As Double = Session("IMPORTOPARMAN1-" & CODICEPARENTE)
                MyRs.AddNew()
                MoveToDb(MyRs.Fields("Utente"), "NEWAUTONCFAT")
                MoveToDb(MyRs.Fields("DataAggiornamento"), Now)
                MoveToDb(MyRs.Fields("Numero"), Numero)
                MoveToDb(MyRs.Fields("RigaDaCausale"), 2)
                MoveToDb(MyRs.Fields("MastroPartita"), XCau.Righe(2 - 1).Mastro)
                MoveToDb(MyRs.Fields("ContoPartita"), XCau.Righe(2 - 1).Conto)
                MoveToDb(MyRs.Fields("SottoContoPartita"), XCau.Righe(2 - 1).Sottoconto)

                MoveToDb(MyRs.Fields("MastroControPartita"), MastroOspite(Session("CODICESERVIZIO"), Session("CODICEOSPITE")))
                MoveToDb(MyRs.Fields("ContoControPartita"), ContoOspite(Session("CODICESERVIZIO"), Session("CODICEOSPITE")))
                MoveToDb(MyRs.Fields("SottocontoControPartita"), SottoContoOspite(Session("CODICESERVIZIO"), Session("CODICEOSPITE")))

                MoveToDb(MyRs.Fields("Segno"), XCau.Righe(2 - 1).Segno)
                MoveToDb(MyRs.Fields("DareAvere"), XCau.Righe(2 - 1).DareAvere)
                MoveToDb(MyRs.Fields("Tipo"), "IV")
                MoveToDb(MyRs.Fields("CodiceIva"), CampoTipoAddebito(CODICE_MANUALE, "CodiceIVA"))

                If IvaInclusaNelTotale = 1 Then
                    Dim MyIva As New Cls_IVA

                    MyIva.Leggi(Session("DC_TABELLE"), CampoTipoAddebito(CODICE_MANUALE, "CodiceIVA"))

                    MoveToDb(MyRs.Fields("Imponibile"), Modulo.MathRound((IMPORTO_MANUALE * 100) / ((MyIva.Aliquota * 100) + 100), 2))
                    MoveToDb(MyRs.Fields("Importo"), IMPORTO_MANUALE - MoveFromDb(MyRs.Fields("Imponibile")))
                Else
                    Dim MyIva As New Cls_IVA

                    MyIva.Leggi(Session("DC_TABELLE"), CampoTipoAddebito(CODICE_MANUALE, "CodiceIVA"))

                    MoveToDb(MyRs.Fields("Imponibile"), IMPORTO_MANUALE)
                    MoveToDb(MyRs.Fields("Importo"), MyIva.Aliquota * IMPORTO_MANUALE)
                End If

                MoveToDb(MyRs.Fields("AnnoRiferimento"), 0)
                MoveToDb(MyRs.Fields("MeseRiferimento"), 0)
                MoveToDb(MyRs.Fields("Quantita"), 0)
                MoveToDb(MyRs.Fields("Descrizione"), "")
                MoveToDb(MyRs.Fields("UTENTE"), Session("UTENTE"))
                MoveToDb(MyRs.Fields("DataAggiornamento"), Now)
                MyRs.Update()

                MyRs.AddNew()
                MoveToDb(MyRs.Fields("Utente"), "AUTONCFAT")
                MoveToDb(MyRs.Fields("DataAggiornamento"), Now)
                MoveToDb(MyRs.Fields("Numero"), Numero)
                MoveToDb(MyRs.Fields("RigaDaCausale"), 3)
                MoveToDb(MyRs.Fields("MastroPartita"), XCau.Righe(3 - 1).Mastro)
                MoveToDb(MyRs.Fields("ContoPartita"), XCau.Righe(3 - 1).Conto)
                MoveToDb(MyRs.Fields("SottoContoPartita"), XCau.Righe(3 - 1).Sottoconto)


                If CODICE_MANUALE <> "" Then
                    MoveToDb(MyRs.Fields("MastroPartita"), CampoTipoAddebito(CODICE_MANUALE, "Mastro"))
                    MoveToDb(MyRs.Fields("ContoPartita"), CampoTipoAddebito(CODICE_MANUALE, "Conto"))
                    MoveToDb(MyRs.Fields("SottoContoPartita"), CampoTipoAddebito(CODICE_MANUALE, "SottoConto"))
                End If

                MoveToDb(MyRs.Fields("MastroControPartita"), MastroOspite(Session("CODICESERVIZIO"), Session("CODICEOSPITE")))
                MoveToDb(MyRs.Fields("ContoControPartita"), ContoOspite(Session("CODICESERVIZIO"), Session("CODICEOSPITE")))
                MoveToDb(MyRs.Fields("SottocontoControPartita"), SottoContoOspite(Session("CODICESERVIZIO"), Session("CODICEOSPITE")))

                MoveToDb(MyRs.Fields("Segno"), XCau.Righe(3 - 1).Segno)
                MoveToDb(MyRs.Fields("DareAvere"), XCau.Righe(3 - 1).DareAvere)
                MoveToDb(MyRs.Fields("Tipo"), "")
                MoveToDb(MyRs.Fields("CodiceIva"), CampoTipoAddebito(CODICE_MANUALE, "CodiceIVA"))


                If IvaInclusaNelTotale = 1 Then
                    Dim MyIva As New Cls_IVA

                    MyIva.Leggi(Session("DC_TABELLE"), CampoTipoAddebito(CODICE_MANUALE, "CodiceIVA"))

                    MoveToDb(MyRs.Fields("Importo"), Modulo.MathRound((IMPORTO_MANUALE * 100) / ((MyIva.Aliquota * 100) + 100), 2))
                Else
                    MoveToDb(MyRs.Fields("Importo"), IMPORTO_MANUALE)
                End If

                MoveToDb(MyRs.Fields("AnnoRiferimento"), Txt_Anno.Text)
                MoveToDb(MyRs.Fields("MeseRiferimento"), Mese)
                MoveToDb(MyRs.Fields("Quantita"), 0)
                MoveToDb(MyRs.Fields("Descrizione"), "")
                MoveToDb(MyRs.Fields("UTENTE"), Session("UTENTE"))
                MoveToDb(MyRs.Fields("DataAggiornamento"), Now)
                MyRs.Update()
            End If

            If Session("IMPORTOPARMAN2-" & CODICEPARENTE) > 0 Then
                Dim CODICE_MANUALE As String = Session("CODICEPARMAN1-" & CODICEPARENTE)
                Dim IMPORTO_MANUALE As Double = Session("IMPORTOPARMAN2-" & CODICEPARENTE)
                MyRs.AddNew()
                MoveToDb(MyRs.Fields("Utente"), "NEWAUTONCFAT")
                MoveToDb(MyRs.Fields("DataAggiornamento"), Now)
                MoveToDb(MyRs.Fields("Numero"), Numero)
                MoveToDb(MyRs.Fields("RigaDaCausale"), 2)
                MoveToDb(MyRs.Fields("MastroPartita"), XCau.Righe(2 - 1).Mastro)
                MoveToDb(MyRs.Fields("ContoPartita"), XCau.Righe(2 - 1).Conto)
                MoveToDb(MyRs.Fields("SottoContoPartita"), XCau.Righe(2 - 1).Sottoconto)

                MoveToDb(MyRs.Fields("MastroControPartita"), MastroOspite(Session("CODICESERVIZIO"), Session("CODICEOSPITE")))
                MoveToDb(MyRs.Fields("ContoControPartita"), ContoOspite(Session("CODICESERVIZIO"), Session("CODICEOSPITE")))
                MoveToDb(MyRs.Fields("SottocontoControPartita"), SottoContoOspite(Session("CODICESERVIZIO"), Session("CODICEOSPITE")))

                MoveToDb(MyRs.Fields("Segno"), XCau.Righe(2 - 1).Segno)
                MoveToDb(MyRs.Fields("DareAvere"), XCau.Righe(2 - 1).DareAvere)
                MoveToDb(MyRs.Fields("Tipo"), "IV")
                MoveToDb(MyRs.Fields("CodiceIva"), CampoTipoAddebito(CODICE_MANUALE, "CodiceIVA"))

                If IvaInclusaNelTotale = 1 Then
                    Dim MyIva As New Cls_IVA

                    MyIva.Leggi(Session("DC_TABELLE"), CampoTipoAddebito(CODICE_MANUALE, "CodiceIVA"))

                    MoveToDb(MyRs.Fields("Imponibile"), Modulo.MathRound((IMPORTO_MANUALE * 100) / ((MyIva.Aliquota * 100) + 100), 2))
                    MoveToDb(MyRs.Fields("Importo"), IMPORTO_MANUALE - MoveFromDb(MyRs.Fields("Imponibile")))
                Else
                    Dim MyIva As New Cls_IVA

                    MyIva.Leggi(Session("DC_TABELLE"), CampoTipoAddebito(CODICE_MANUALE, "CodiceIVA"))

                    MoveToDb(MyRs.Fields("Imponibile"), IMPORTO_MANUALE)
                    MoveToDb(MyRs.Fields("Importo"), MyIva.Aliquota * IMPORTO_MANUALE)
                End If

                MoveToDb(MyRs.Fields("AnnoRiferimento"), 0)
                MoveToDb(MyRs.Fields("MeseRiferimento"), 0)
                MoveToDb(MyRs.Fields("Quantita"), 0)
                MoveToDb(MyRs.Fields("Descrizione"), "")
                MoveToDb(MyRs.Fields("UTENTE"), Session("UTENTE"))
                MoveToDb(MyRs.Fields("DataAggiornamento"), Now)
                MyRs.Update()

                MyRs.AddNew()
                MoveToDb(MyRs.Fields("Utente"), "AUTONCFAT")
                MoveToDb(MyRs.Fields("DataAggiornamento"), Now)
                MoveToDb(MyRs.Fields("Numero"), Numero)
                MoveToDb(MyRs.Fields("RigaDaCausale"), 3)
                MoveToDb(MyRs.Fields("MastroPartita"), XCau.Righe(3 - 1).Mastro)
                MoveToDb(MyRs.Fields("ContoPartita"), XCau.Righe(3 - 1).Conto)
                MoveToDb(MyRs.Fields("SottoContoPartita"), XCau.Righe(3 - 1).Sottoconto)


                If CODICE_MANUALE <> "" Then
                    MoveToDb(MyRs.Fields("MastroPartita"), CampoTipoAddebito(CODICE_MANUALE, "Mastro"))
                    MoveToDb(MyRs.Fields("ContoPartita"), CampoTipoAddebito(CODICE_MANUALE, "Conto"))
                    MoveToDb(MyRs.Fields("SottoContoPartita"), CampoTipoAddebito(CODICE_MANUALE, "SottoConto"))
                End If

                MoveToDb(MyRs.Fields("MastroControPartita"), MastroOspite(Session("CODICESERVIZIO"), Session("CODICEOSPITE")))
                MoveToDb(MyRs.Fields("ContoControPartita"), ContoOspite(Session("CODICESERVIZIO"), Session("CODICEOSPITE")))
                MoveToDb(MyRs.Fields("SottocontoControPartita"), SottoContoOspite(Session("CODICESERVIZIO"), Session("CODICEOSPITE")))

                MoveToDb(MyRs.Fields("Segno"), XCau.Righe(3 - 1).Segno)
                MoveToDb(MyRs.Fields("DareAvere"), XCau.Righe(3 - 1).DareAvere)
                MoveToDb(MyRs.Fields("Tipo"), "")
                MoveToDb(MyRs.Fields("CodiceIva"), CampoTipoAddebito(CODICE_MANUALE, "CodiceIVA"))


                If IvaInclusaNelTotale = 1 Then
                    Dim MyIva As New Cls_IVA

                    MyIva.Leggi(Session("DC_TABELLE"), CampoTipoAddebito(CODICE_MANUALE, "CodiceIVA"))

                    MoveToDb(MyRs.Fields("Importo"), Modulo.MathRound((IMPORTO_MANUALE * 100) / ((MyIva.Aliquota * 100) + 100), 2))
                Else
                    MoveToDb(MyRs.Fields("Importo"), IMPORTO_MANUALE)
                End If

                MoveToDb(MyRs.Fields("AnnoRiferimento"), Txt_Anno.Text)
                MoveToDb(MyRs.Fields("MeseRiferimento"), Mese)
                MoveToDb(MyRs.Fields("Quantita"), 0)
                MoveToDb(MyRs.Fields("Descrizione"), "")
                MoveToDb(MyRs.Fields("UTENTE"), Session("UTENTE"))
                MoveToDb(MyRs.Fields("DataAggiornamento"), Now)
                MyRs.Update()
            End If
        End If





        ' Ricavo Retta
        MyRs.AddNew()
        MoveToDb(MyRs.Fields("Numero"), Numero)
        MoveToDb(MyRs.Fields("RigaDaCausale"), 3)
        MoveToDb(MyRs.Fields("MastroPartita"), XCau.Righe(3 - 1).Mastro)
        MoveToDb(MyRs.Fields("ContoPartita"), XCau.Righe(3 - 1).Conto)
        MoveToDb(MyRs.Fields("SottoContoPartita"), XCau.Righe(3 - 1).Sottoconto)


        If CODICEPARENTE = 0 Then
            MoveToDb(MyRs.Fields("MastroControPartita"), MastroOspite(Session("CODICESERVIZIO"), Session("CODICEOSPITE")))
            MoveToDb(MyRs.Fields("ContoControPartita"), ContoOspite(Session("CODICESERVIZIO"), Session("CODICEOSPITE")))
            MoveToDb(MyRs.Fields("SottocontoControPartita"), SottoContoOspite(Session("CODICESERVIZIO"), Session("CODICEOSPITE")))
        Else
            MoveToDb(MyRs.Fields("MastroControPartita"), MastroParente(Session("CODICESERVIZIO"), Session("CODICEOSPITE"), CODICEPARENTE))
            MoveToDb(MyRs.Fields("ContoControPartita"), ContoParente(Session("CODICESERVIZIO"), Session("CODICEOSPITE"), CODICEPARENTE))
            MoveToDb(MyRs.Fields("SottocontoControPartita"), SottoContoParente(Session("CODICESERVIZIO"), Session("CODICEOSPITE"), CODICEPARENTE))
        End If


        MoveToDb(MyRs.Fields("Segno"), XCau.Righe(3 - 1).Segno)

        If IffNC = "" Then
            MoveToDb(MyRs.Fields("DareAvere"), XCau.Righe(3 - 1).DareAvere)
            If RettaNegativa = True Then
                If XCau.Righe(2 - 1).DareAvere = "D" Then
                    MoveToDb(MyRs.Fields("DareAvere"), "A")
                Else
                    MoveToDb(MyRs.Fields("DareAvere"), "D")
                End If
            End If
        Else
            If XCau.Righe(3 - 1).DareAvere = "A" Then
                MoveToDb(MyRs.Fields("DareAvere"), "D")
            Else
                MoveToDb(MyRs.Fields("DareAvere"), "A")
            End If
        End If

        MoveToDb(MyRs.Fields("Tipo"), "")
        MoveToDb(MyRs.Fields("CodiceIva"), CausaleIVA)


        REM TOGLIE IL SECONDO MESE SUCCESSIVO CHE APPARE A VIDEO

        If IvaInclusaNelTotale = 1 Then
            Dim AliquotaIVA As New Cls_IVA

            AliquotaIVA.Codice = CausaleIVA
            AliquotaIVA.Leggi(Session("DC_TABELLE"), AliquotaIVA.Codice)

            ImportoRetta = Modulo.MathRound((ImportoRetta - Modulo.MathRound(MyRettaRpx2, 2) - Modulo.MathRound(MyRettaRpx, 2)) / (AliquotaIVA.Aliquota + 1), 2) + Modulo.MathRound(MyRettaRpx, 2)
        Else
            ImportoRetta = Modulo.MathRound(ImportoRetta - Modulo.MathRound(MyRettaRpx2, 2), 2)
        End If

        If Tpo.PercentualeDivisione = 0 Then
            If IffNC = "" Then
                If Modulo.MathRound(ImportoRetta - Modulo.MathRound(MyRettaRpx, 2), 2) < 0 Then
                    If XCau.Righe(3 - 1).DareAvere = "A" Then
                        MoveToDb(MyRs.Fields("DareAvere"), "D")
                    Else
                        MoveToDb(MyRs.Fields("DareAvere"), "A")
                    End If
                End If

                MoveToDb(MyRs.Fields("Importo"), Math.Abs(Modulo.MathRound(ImportoRetta - Modulo.MathRound(MyRettaRpx, 2), 2)))
            Else
                If Modulo.MathRound(ImportoRetta - Modulo.MathRound(MyRettaRpx, 2), 2) < 0 Then
                    If MoveFromDb(MyRs.Fields("DareAvere")) = "A" Then
                        MoveToDb(MyRs.Fields("Importo"), Modulo.MathRound(Modulo.MathRound(MyRettaRpx, 2) - ImportoRetta, 2))
                        MoveToDb(MyRs.Fields("DareAvere"), "D")
                    Else
                        MoveToDb(MyRs.Fields("Importo"), Modulo.MathRound(Modulo.MathRound(MyRettaRpx, 2) - ImportoRetta, 2))
                        MoveToDb(MyRs.Fields("DareAvere"), "A")
                    End If

                Else
                    MoveToDb(MyRs.Fields("Importo"), Modulo.MathRound(ImportoRetta - Modulo.MathRound(MyRettaRpx, 2), 2))
                End If
            End If
        Else
            If IffNC = "" Then
                MoveToDb(MyRs.Fields("Importo"), Modulo.MathRound(Modulo.MathRound(ImportoRetta - Modulo.MathRound(MyRettaRpx, 2), 2) * Tpo.PercentualeDivisione / 100, 2))
            Else
                If Modulo.MathRound(ImportoRetta - Modulo.MathRound(MyRettaRpx, 2), 2) < 0 Then
                    If MoveFromDb(MyRs.Fields("DareAvere")) = "A" Then
                        MoveToDb(MyRs.Fields("Importo"), Modulo.MathRound(Modulo.MathRound(Modulo.MathRound(MyRettaRpx, 2) - ImportoRetta, 2) * Tpo.PercentualeDivisione / 100, 2))
                        MoveToDb(MyRs.Fields("DareAvere"), "D")
                    Else
                        MoveToDb(MyRs.Fields("Importo"), Modulo.MathRound(Modulo.MathRound(Modulo.MathRound(MyRettaRpx, 2) - ImportoRetta, 2) * Tpo.PercentualeDivisione / 100, 2))
                        MoveToDb(MyRs.Fields("DareAvere"), "A")
                    End If

                Else
                    MoveToDb(MyRs.Fields("Importo"), Modulo.MathRound(Modulo.MathRound(ImportoRetta - Modulo.MathRound(MyRettaRpx, 2), 2) * Tpo.PercentualeDivisione / 100, 2))
                End If
            End If
        End If

        MoveToDb(MyRs.Fields("AnnoRiferimento"), Txt_Anno.Text)
        MoveToDb(MyRs.Fields("MeseRiferimento"), Mese)

        MoveToDb(MyRs.Fields("CENTROSERVIZIO"), Session("CODICESERVIZIO"))
        MoveToDb(MyRs.Fields("Quantita"), Math.Abs(Giorni))
        MoveToDb(MyRs.Fields("UTENTE"), Session("UTENTE"))
        MoveToDb(MyRs.Fields("DataAggiornamento"), Now)
        MyRs.Update()

        If Tpo.PercentualeDivisione > 0 Then

            ' Ricavo Retta
            MyRs.AddNew()
            MoveToDb(MyRs.Fields("Numero"), Numero)
            MoveToDb(MyRs.Fields("RigaDaCausale"), 3)
            MoveToDb(MyRs.Fields("MastroPartita"), XCau.Righe(14 - 1).Mastro)
            MoveToDb(MyRs.Fields("ContoPartita"), XCau.Righe(14 - 1).Conto)
            MoveToDb(MyRs.Fields("SottoContoPartita"), XCau.Righe(14 - 1).Sottoconto)


            If CODICEPARENTE = 0 Then
                MoveToDb(MyRs.Fields("MastroControPartita"), MastroOspite(Session("CODICESERVIZIO"), Session("CODICEOSPITE")))
                MoveToDb(MyRs.Fields("ContoControPartita"), ContoOspite(Session("CODICESERVIZIO"), Session("CODICEOSPITE")))
                MoveToDb(MyRs.Fields("SottocontoControPartita"), SottoContoOspite(Session("CODICESERVIZIO"), Session("CODICEOSPITE")))
            Else
                MoveToDb(MyRs.Fields("MastroControPartita"), MastroParente(Session("CODICESERVIZIO"), Session("CODICEOSPITE"), CODICEPARENTE))
                MoveToDb(MyRs.Fields("ContoControPartita"), ContoParente(Session("CODICESERVIZIO"), Session("CODICEOSPITE"), CODICEPARENTE))
                MoveToDb(MyRs.Fields("SottocontoControPartita"), SottoContoParente(Session("CODICESERVIZIO"), Session("CODICEOSPITE"), CODICEPARENTE))
            End If


            MoveToDb(MyRs.Fields("Segno"), XCau.Righe(14 - 1).Segno)

            If IffNC = "" Then
                MoveToDb(MyRs.Fields("DareAvere"), XCau.Righe(14 - 1).DareAvere)
            Else
                If XCau.Righe(3 - 1).DareAvere = "A" Then
                    MoveToDb(MyRs.Fields("DareAvere"), "D")
                Else
                    MoveToDb(MyRs.Fields("DareAvere"), "A")
                End If
            End If

            MoveToDb(MyRs.Fields("Tipo"), "")
            MoveToDb(MyRs.Fields("CodiceIva"), CausaleIVA)

            REM TOGLIE IL SECONDO MESE SUCCESSIVO CHE APPARE A VIDEO
            ImportoRetta = Modulo.MathRound(ImportoRetta - Modulo.MathRound(MyRettaRpx2, 2), 2)
            If Tpo.PercentualeDivisione = 0 Then
                If IffNC = "" Then
                    MoveToDb(MyRs.Fields("Importo"), Modulo.MathRound(ImportoRetta - Modulo.MathRound(MyRettaRpx, 2), 2))
                Else
                    If Modulo.MathRound(ImportoRetta - Modulo.MathRound(MyRettaRpx, 2), 2) < 0 Then
                        If MoveFromDb(MyRs.Fields("DareAvere")) = "A" Then
                            MoveToDb(MyRs.Fields("Importo"), Modulo.MathRound(Modulo.MathRound(MyRettaRpx, 2) - ImportoRetta, 2))
                            MoveToDb(MyRs.Fields("DareAvere"), "D")
                        Else
                            MoveToDb(MyRs.Fields("Importo"), Modulo.MathRound(Modulo.MathRound(MyRettaRpx, 2) - ImportoRetta, 2))
                            MoveToDb(MyRs.Fields("DareAvere"), "A")
                        End If

                    Else
                        MoveToDb(MyRs.Fields("Importo"), Modulo.MathRound(ImportoRetta - Modulo.MathRound(MyRettaRpx, 2), 2))
                    End If
                End If
            Else
                If IffNC = "" Then
                    MoveToDb(MyRs.Fields("Importo"), Modulo.MathRound(Modulo.MathRound(ImportoRetta - Modulo.MathRound(MyRettaRpx, 2), 2) * (100 - Tpo.PercentualeDivisione) / 100, 2))
                Else
                    If Modulo.MathRound(ImportoRetta - Modulo.MathRound(MyRettaRpx, 2), 2) < 0 Then
                        If MoveFromDb(MyRs.Fields("DareAvere")) = "A" Then
                            MoveToDb(MyRs.Fields("Importo"), Modulo.MathRound(Modulo.MathRound(Modulo.MathRound(MyRettaRpx, 2) - ImportoRetta, 2) * (100 - Tpo.PercentualeDivisione) / 100, 2))
                            MoveToDb(MyRs.Fields("DareAvere"), "D")
                        Else
                            MoveToDb(MyRs.Fields("Importo"), Modulo.MathRound(Modulo.MathRound(Modulo.MathRound(MyRettaRpx, 2) - ImportoRetta, 2) * (100 - Tpo.PercentualeDivisione) / 100, 2))
                            MoveToDb(MyRs.Fields("DareAvere"), "A")
                        End If

                    Else
                        MoveToDb(MyRs.Fields("Importo"), Modulo.MathRound(Modulo.MathRound(ImportoRetta - Modulo.MathRound(MyRettaRpx, 2), 2) * (100 - Tpo.PercentualeDivisione) / 100, 2))
                    End If
                End If
            End If

            MoveToDb(MyRs.Fields("AnnoRiferimento"), Txt_Anno.Text)
            MoveToDb(MyRs.Fields("MeseRiferimento"), Mese)

            MoveToDb(MyRs.Fields("CENTROSERVIZIO"), Session("CODICESERVIZIO"))
            MoveToDb(MyRs.Fields("Quantita"), Math.Abs(Giorni))
            MoveToDb(MyRs.Fields("UTENTE"), Session("UTENTE"))
            MoveToDb(MyRs.Fields("DataAggiornamento"), Now)
            MyRs.Update()

        End If




        If ImportoRetta2 <> 0 And Not IsNothing(XCau.Righe(13 - 1)) Then
            MyRs.AddNew()
            MoveToDb(MyRs.Fields("Numero"), Numero)
            MoveToDb(MyRs.Fields("RigaDaCausale"), 13)
            MoveToDb(MyRs.Fields("MastroPartita"), XCau.Righe(13 - 1).Mastro)
            MoveToDb(MyRs.Fields("ContoPartita"), XCau.Righe(13 - 1).Conto)
            MoveToDb(MyRs.Fields("SottoContoPartita"), XCau.Righe(13 - 1).Sottoconto)


            If CODICEPARENTE = 0 Then
                MoveToDb(MyRs.Fields("MastroControPartita"), MastroOspite(Session("CODICESERVIZIO"), Session("CODICEOSPITE")))
                MoveToDb(MyRs.Fields("ContoControPartita"), ContoOspite(Session("CODICESERVIZIO"), Session("CODICEOSPITE")))
                MoveToDb(MyRs.Fields("SottocontoControPartita"), SottoContoOspite(Session("CODICESERVIZIO"), Session("CODICEOSPITE")))
            Else
                MoveToDb(MyRs.Fields("MastroControPartita"), MastroParente(Session("CODICESERVIZIO"), Session("CODICEOSPITE"), CODICEPARENTE))
                MoveToDb(MyRs.Fields("ContoControPartita"), ContoParente(Session("CODICESERVIZIO"), Session("CODICEOSPITE"), CODICEPARENTE))
                MoveToDb(MyRs.Fields("SottocontoControPartita"), SottoContoParente(Session("CODICESERVIZIO"), Session("CODICEOSPITE"), CODICEPARENTE))
            End If


            MoveToDb(MyRs.Fields("Segno"), XCau.Righe(13 - 1).Segno)

            If IffNC = "" Then
                MoveToDb(MyRs.Fields("DareAvere"), XCau.Righe(13 - 1).DareAvere)
            Else
                If XCau.Righe(13 - 1).DareAvere = "A" Then
                    MoveToDb(MyRs.Fields("DareAvere"), "D")
                Else
                    MoveToDb(MyRs.Fields("DareAvere"), "A")
                End If
            End If

            If IvaInclusaNelTotale = 1 Then
                Dim AliquotaIVA As New Cls_IVA

                AliquotaIVA.Codice = CausaleIVA
                AliquotaIVA.Leggi(Session("DC_TABELLE"), AliquotaIVA.Codice)

                ImportoRetta2 = Modulo.MathRound(ImportoRetta2 / (AliquotaIVA.Aliquota + 1), 2)
            Else
                ImportoRetta2 = Modulo.MathRound(ImportoRetta2, 2)
            End If

            MoveToDb(MyRs.Fields("Tipo"), "")
            MoveToDb(MyRs.Fields("CodiceIva"), CausaleIVA)
            If IffNC = "" Then
                MoveToDb(MyRs.Fields("Importo"), Math.Abs(Modulo.MathRound(ImportoRetta2, 2)))
            Else
                MoveToDb(MyRs.Fields("Importo"), Math.Abs(Modulo.MathRound(ImportoRetta2, 2)))
            End If
            MoveToDb(MyRs.Fields("AnnoRiferimento"), Txt_Anno.Text)
            MoveToDb(MyRs.Fields("MeseRiferimento"), Mese)

            MoveToDb(MyRs.Fields("CENTROSERVIZIO"), Session("CODICESERVIZIO"))
            MoveToDb(MyRs.Fields("Quantita"), Math.Abs(Giorni))
            MoveToDb(MyRs.Fields("UTENTE"), Session("UTENTE"))
            MoveToDb(MyRs.Fields("DataAggiornamento"), Now)
            MyRs.Update()

        End If

        If MyRettaRpx > 0 Then
            ' Ricavo Retta
            MyRs.AddNew()
            MoveToDb(MyRs.Fields("Numero"), Numero)
            MoveToDb(MyRs.Fields("RigaDaCausale"), 3)
            MoveToDb(MyRs.Fields("MastroPartita"), XCau.Righe(3 - 1).Mastro)
            MoveToDb(MyRs.Fields("ContoPartita"), XCau.Righe(3 - 1).Conto)
            MoveToDb(MyRs.Fields("SottoContoPartita"), XCau.Righe(3 - 1).Sottoconto)


            If CODICEPARENTE = 0 Then
                MoveToDb(MyRs.Fields("MastroControPartita"), MastroOspite(Session("CODICESERVIZIO"), Session("CODICEOSPITE")))
                MoveToDb(MyRs.Fields("ContoControPartita"), ContoOspite(Session("CODICESERVIZIO"), Session("CODICEOSPITE")))
                MoveToDb(MyRs.Fields("SottocontoControPartita"), SottoContoOspite(Session("CODICESERVIZIO"), Session("CODICEOSPITE")))
            Else
                MoveToDb(MyRs.Fields("MastroControPartita"), MastroParente(Session("CODICESERVIZIO"), Session("CODICEOSPITE"), CODICEPARENTE))
                MoveToDb(MyRs.Fields("ContoControPartita"), ContoParente(Session("CODICESERVIZIO"), Session("CODICEOSPITE"), CODICEPARENTE))
                MoveToDb(MyRs.Fields("SottocontoControPartita"), SottoContoParente(Session("CODICESERVIZIO"), Session("CODICEOSPITE"), CODICEPARENTE))
            End If


            MoveToDb(MyRs.Fields("Segno"), XCau.Righe(3 - 1).Segno)

            If IffNC = "" Then
                MoveToDb(MyRs.Fields("DareAvere"), XCau.Righe(3 - 1).DareAvere)
            Else
                If XCau.Righe(3 - 1).DareAvere = "A" Then
                    MoveToDb(MyRs.Fields("DareAvere"), "D")
                Else
                    MoveToDb(MyRs.Fields("DareAvere"), "A")
                End If
            End If

            MoveToDb(MyRs.Fields("Tipo"), "")
            MoveToDb(MyRs.Fields("CodiceIva"), CausaleIVA)

            If Tpo.PercentualeDivisione = 0 Then
                MoveToDb(MyRs.Fields("Importo"), MyRettaRpx)
            Else
                MoveToDb(MyRs.Fields("Importo"), Modulo.MathRound(MyRettaRpx * Tpo.PercentualeDivisione / 100, 2))
            End If

            xMese = Mese
            xAnno = Txt_Anno.Text
            If xMese = 12 Then
                xAnno = Anno + 1
                xMese = 1
            Else
                xMese = xMese + 1
            End If

            MoveToDb(MyRs.Fields("AnnoRiferimento"), xAnno)
            MoveToDb(MyRs.Fields("MeseRiferimento"), xMese)
            MoveToDb(MyRs.Fields("Quantita"), GiorniRpx)
            MoveToDb(MyRs.Fields("CENTROSERVIZIO"), Session("CODICESERVIZIO"))
            MoveToDb(MyRs.Fields("UTENTE"), Session("UTENTE"))
            MoveToDb(MyRs.Fields("DataAggiornamento"), Now)
            MyRs.Update()
            If Tpo.PercentualeDivisione > 0 Then
                ' Ricavo Retta
                MyRs.AddNew()
                MoveToDb(MyRs.Fields("Numero"), Numero)
                MoveToDb(MyRs.Fields("RigaDaCausale"), 3)
                MoveToDb(MyRs.Fields("MastroPartita"), XCau.Righe(3 - 1).Mastro)
                MoveToDb(MyRs.Fields("ContoPartita"), XCau.Righe(3 - 1).Conto)
                MoveToDb(MyRs.Fields("SottoContoPartita"), XCau.Righe(3 - 1).Sottoconto)


                If CODICEPARENTE = 0 Then
                    MoveToDb(MyRs.Fields("MastroControPartita"), MastroOspite(Session("CODICESERVIZIO"), Session("CODICEOSPITE")))
                    MoveToDb(MyRs.Fields("ContoControPartita"), ContoOspite(Session("CODICESERVIZIO"), Session("CODICEOSPITE")))
                    MoveToDb(MyRs.Fields("SottocontoControPartita"), SottoContoOspite(Session("CODICESERVIZIO"), Session("CODICEOSPITE")))
                Else
                    MoveToDb(MyRs.Fields("MastroControPartita"), MastroParente(Session("CODICESERVIZIO"), Session("CODICEOSPITE"), CODICEPARENTE))
                    MoveToDb(MyRs.Fields("ContoControPartita"), ContoParente(Session("CODICESERVIZIO"), Session("CODICEOSPITE"), CODICEPARENTE))
                    MoveToDb(MyRs.Fields("SottocontoControPartita"), SottoContoParente(Session("CODICESERVIZIO"), Session("CODICEOSPITE"), CODICEPARENTE))
                End If


                MoveToDb(MyRs.Fields("Segno"), XCau.Righe(3 - 1).Segno)

                If IffNC = "" Then
                    MoveToDb(MyRs.Fields("DareAvere"), XCau.Righe(3 - 1).DareAvere)
                Else
                    If XCau.Righe(3 - 1).DareAvere = "A" Then
                        MoveToDb(MyRs.Fields("DareAvere"), "D")
                    Else
                        MoveToDb(MyRs.Fields("DareAvere"), "A")
                    End If
                End If

                MoveToDb(MyRs.Fields("Tipo"), "")
                MoveToDb(MyRs.Fields("CodiceIva"), CausaleIVA)

                If Tpo.PercentualeDivisione = 0 Then
                    MoveToDb(MyRs.Fields("Importo"), MyRettaRpx)
                Else
                    MoveToDb(MyRs.Fields("Importo"), Modulo.MathRound(MyRettaRpx * (100 - Tpo.PercentualeDivisione) / 100, 2))
                End If

                xMese = Mese
                xAnno = Txt_Anno.Text
                If xMese = 12 Then
                    xAnno = Anno + 1
                    xMese = 1
                Else
                    xMese = xMese + 1
                End If

                MoveToDb(MyRs.Fields("AnnoRiferimento"), xAnno)
                MoveToDb(MyRs.Fields("MeseRiferimento"), xMese)
                MoveToDb(MyRs.Fields("Quantita"), GiorniRpx)
                MoveToDb(MyRs.Fields("CENTROSERVIZIO"), Session("CODICESERVIZIO"))
                MoveToDb(MyRs.Fields("UTENTE"), Session("UTENTE"))
                MoveToDb(MyRs.Fields("DataAggiornamento"), Now)
                MyRs.Update()
            End If
        End If
        If MyRettaRpx2 > 0 And Not IsNothing(XCau.Righe(13 - 1)) Then
            ' Ricavo Retta
            MyRs.AddNew()
            MoveToDb(MyRs.Fields("Numero"), Numero)
            MoveToDb(MyRs.Fields("RigaDaCausale"), 13)
            MoveToDb(MyRs.Fields("MastroPartita"), XCau.Righe(13 - 1).Mastro)
            MoveToDb(MyRs.Fields("ContoPartita"), XCau.Righe(13 - 1).Conto)
            MoveToDb(MyRs.Fields("SottoContoPartita"), XCau.Righe(13 - 1).Sottoconto)


            If CODICEPARENTE = 0 Then
                MoveToDb(MyRs.Fields("MastroControPartita"), MastroOspite(Session("CODICESERVIZIO"), Session("CODICEOSPITE")))
                MoveToDb(MyRs.Fields("ContoControPartita"), ContoOspite(Session("CODICESERVIZIO"), Session("CODICEOSPITE")))
                MoveToDb(MyRs.Fields("SottocontoControPartita"), SottoContoOspite(Session("CODICESERVIZIO"), Session("CODICEOSPITE")))
            Else
                MoveToDb(MyRs.Fields("MastroControPartita"), MastroParente(Session("CODICESERVIZIO"), Session("CODICEOSPITE"), CODICEPARENTE))
                MoveToDb(MyRs.Fields("ContoControPartita"), ContoParente(Session("CODICESERVIZIO"), Session("CODICEOSPITE"), CODICEPARENTE))
                MoveToDb(MyRs.Fields("SottocontoControPartita"), SottoContoParente(Session("CODICESERVIZIO"), Session("CODICEOSPITE"), CODICEPARENTE))
            End If


            MoveToDb(MyRs.Fields("Segno"), XCau.Righe(13 - 1).Segno)

            If IffNC = "" Then
                MoveToDb(MyRs.Fields("DareAvere"), XCau.Righe(13 - 1).DareAvere)
            Else
                If XCau.Righe(13 - 1).DareAvere = "A" Then
                    MoveToDb(MyRs.Fields("DareAvere"), "D")
                Else
                    MoveToDb(MyRs.Fields("DareAvere"), "A")
                End If
            End If

            MoveToDb(MyRs.Fields("Tipo"), "")
            MoveToDb(MyRs.Fields("CodiceIva"), CausaleIVA)
            MoveToDb(MyRs.Fields("Importo"), MyRettaRpx2)
            xMese = Mese
            xAnno = Txt_Anno.Text
            If xMese = 12 Then
                xAnno = Anno + 1
                xMese = 1
            Else
                xMese = xMese + 1
            End If

            MoveToDb(MyRs.Fields("AnnoRiferimento"), xAnno)
            MoveToDb(MyRs.Fields("MeseRiferimento"), xMese)
            MoveToDb(MyRs.Fields("Quantita"), GiorniRpx)
            MoveToDb(MyRs.Fields("CENTROSERVIZIO"), Session("CODICESERVIZIO"))
            MoveToDb(MyRs.Fields("UTENTE"), Session("UTENTE"))
            MoveToDb(MyRs.Fields("DataAggiornamento"), Now)
            MyRs.Update()
        End If
        MyRs.Close()

        Dim ImportoApp As Double


        MyRs.Open("Select Sum(Importo) + Sum(Imponibile) As Totale from Temp_MovimentiContabiliRiga where Numero = " & Numero & " And  Tipo = 'IV' And DareAvere = 'D'", GeneraleDb, ADODB.CursorTypeEnum.adOpenKeyset, ADODB.LockTypeEnum.adLockOptimistic)
        'If IffNC = "NC" Then
        ' ImportoApp = MoveFromDb(MyRs.Fields("Totale")) * -1
        'Else
        ImportoApp = MoveFromDb(MyRs.Fields("Totale"))
        'End If
        MyRs.Close()


        MyRs.Open("Select Sum(Importo) + Sum(Imponibile) As Totale from Temp_MovimentiContabiliRiga where Numero = " & Numero & " And  Tipo = 'IV' And  DareAvere = 'A'", GeneraleDb, ADODB.CursorTypeEnum.adOpenKeyset, ADODB.LockTypeEnum.adLockOptimistic)
        If IffNC = "NC" Then
            ImportoApp = ImportoApp - MoveFromDb(MyRs.Fields("Totale"))
        Else
            ImportoApp = MoveFromDb(MyRs.Fields("Totale")) - ImportoApp
        End If
        MyRs.Close()

        MyRs.Open("Select * from Temp_MovimentiContabiliRiga where Numero = " & Numero & " And  RigaDaCausale =1", GeneraleDb, ADODB.CursorTypeEnum.adOpenKeyset, ADODB.LockTypeEnum.adLockOptimistic)
        MoveToDb(MyRs.Fields("Importo"), Math.Abs(ImportoApp))
        MyRs.Update()
        MyRs.Close()



        Dim KEm As New Cls_EmissioneRetta

        KEm.ConnessioneGenerale = Session("DC_GENERALE")
        KEm.ConnessioneOspiti = Session("DC_OSPITE")
        KEm.ConnessioneTabelle = Session("DC_TABELLE")

        KEm.ApriDB()

        KEm.VerificaRegistrazioneIVA(Numero, False)

        KEm.Commit()
        KEm.CloseDB()

        Session("Doc_creato_Numero") = Numero
    End Sub

    Public Function ImportoBollo(ByVal Data As Date, ByVal Importo As Double) As Double
        Dim MySql As String
        Dim MyRs As New ADODB.Recordset
        Dim TabelleDB As New ADODB.Connection
        Dim LeggiData As Date

        TabelleDB.Open(Session("DC_TABELLE"))
        MySql = "Select top 1 DataValidita From Bolli Where DataValidita <= {ts '" & Format(Data, "yyyy-MM-dd") & " 00:00:00'} Order by DataValidita DESC"

        MyRs.Open(MySql, TabelleDB, ADODB.CursorTypeEnum.adOpenKeyset, ADODB.LockTypeEnum.adLockOptimistic)
        If Not MyRs.EOF Then
            LeggiData = MoveFromDb(MyRs.Fields("DataValidita"))
        End If
        MyRs.Close()


        MySql = "Select * From Bolli Where DataValidita = {ts '" & Format(LeggiData, "yyyy-MM-dd") & " 00:00:00'} Order by ImportoApplicazione DESC"

        MyRs.Open(MySql, TabelleDB, ADODB.CursorTypeEnum.adOpenKeyset, ADODB.LockTypeEnum.adLockOptimistic)
        Do While Not MyRs.EOF
            If Math.Abs(Importo) > MoveFromDb(MyRs.Fields("ImportoApplicazione")) Then
                ImportoBollo = MoveFromDb(MyRs.Fields("ImportoBollo"))
                MyRs.Close()
                Exit Function
            End If
            MyRs.MoveNext()
        Loop
        MyRs.Close()
        TabelleDB.Close()
    End Function

    Public Function MaxRegistrazione() As Long
        Dim Rs_MaxF As New ADODB.Recordset
        Dim GeneraleDB As New ADODB.Connection

        GeneraleDB.Open(Session("DC_GENERALE"))

        Rs_MaxF.Open("Select Max(NumeroRegistrazione) From MovimentiContabiliTesta", GeneraleDB, ADODB.CursorTypeEnum.adOpenKeyset, ADODB.LockTypeEnum.adLockOptimistic)
        MaxRegistrazione = MoveFromDb(Rs_MaxF.Fields(0)) + 1
        Rs_MaxF.Close()


        Rs_MaxF.Open("Select Max(NumeroRegistrazione) From Temp_MovimentiContabiliTesta", GeneraleDB, ADODB.CursorTypeEnum.adOpenKeyset, ADODB.LockTypeEnum.adLockOptimistic)
        If MaxRegistrazione < MoveFromDb(Rs_MaxF.Fields(0)) + 1 Then
            MaxRegistrazione = MoveFromDb(Rs_MaxF.Fields(0)) + 1
        End If
        Rs_MaxF.Close()

        Rs_MaxF = Nothing
        GeneraleDB.Close()
    End Function

    Public Function CalcolaProgressivo(ByVal Anno As Integer, ByVal RegistroIva As Integer) As Long
        Dim RsMaxProg As New ADODB.Recordset
        Dim GeneraleDB As New ADODB.Connection

        GeneraleDB.Open(Session("DC_GENERALE"))


        RsMaxProg.Open("Select Max(NumeroProtocollo) From MovimentiContabiliTesta Where AnnoProtocollo = " & Anno & " And RegistroIVA = " & RegistroIva, GeneraleDB, ADODB.CursorTypeEnum.adOpenKeyset, ADODB.LockTypeEnum.adLockOptimistic)
        CalcolaProgressivo = Val(MoveFromDb(RsMaxProg.Fields(0)))
        RsMaxProg.Close()

        RsMaxProg.Open("Select Max(NumeroProtocollo) From Temp_MovimentiContabiliTesta Where AnnoProtocollo = " & Anno & " And RegistroIVA = " & RegistroIva, GeneraleDB, ADODB.CursorTypeEnum.adOpenKeyset, ADODB.LockTypeEnum.adLockOptimistic)
        If CalcolaProgressivo < Val(MoveFromDb(RsMaxProg.Fields(0))) Then
            CalcolaProgressivo = Val(MoveFromDb(RsMaxProg.Fields(0)))
        End If
        RsMaxProg.Close()

        GeneraleDB.Close()
    End Function


    Public Sub MoveToDb(ByRef MyField As ADODB.Field, ByVal MyVal As Object)
        Select Case Val(MyField.Type)
            Case ADODB.DataTypeEnum.adDBDate Or ADODB.DataTypeEnum.adDBTimeStamp
                If IsDate(MyVal) Then
                    MyField.Value = MyVal
                Else
                    MyField.Value = 0
                End If
            Case ADODB.DataTypeEnum.adDouble
                If IsNumeric(MyVal) Then
                    MyField.Value = MyVal
                Else
                    MyField.Value = 0
                End If
            Case ADODB.DataTypeEnum.adSmallInt
                If IsNumeric(MyVal) Then
                    MyField.Value = MyVal
                Else : MyField.Value = 0
                End If
            Case ADODB.DataTypeEnum.adInteger
                If IsNumeric(MyVal) Then
                    MyField.Value = MyVal
                Else
                    MyField.Value = 0
                End If
            Case ADODB.DataTypeEnum.adNumeric
                If IsNumeric(MyVal) Then
                    MyField.Value = MyVal
                Else
                    MyField.Value = 0
                End If
            Case ADODB.DataTypeEnum.adTinyInt
                If IsNumeric(MyVal) Then
                    MyField.Value = MyVal
                Else
                    MyField.Value = 0
                End If
            Case ADODB.DataTypeEnum.adSingle
                If IsNumeric(MyVal) Then
                    MyField.Value = MyVal
                Else
                    MyField.Value = 0
                End If
            Case ADODB.DataTypeEnum.adLongVarBinary
                If IsNumeric(MyVal) Then
                    MyField.Value = MyVal
                Else
                    MyField.Value = 0
                End If
            Case ADODB.DataTypeEnum.adUnsignedTinyInt
                If IsNumeric(MyVal) Then
                    MyField.Value = MyVal
                Else
                    MyField.Value = 0
                End If
            Case ADODB.DataTypeEnum.adVarChar Or ADODB.DataTypeEnum.adVarWChar Or ADODB.DataTypeEnum.adWChar Or ADODB.DataTypeEnum.adBSTR
                If IsDBNull(MyVal) Then
                    MyField.Value = ""
                Else
                    If Len(MyVal) > MyField.DefinedSize Then
                        MyField.Value = Mid(MyVal, 1, MyField.DefinedSize)
                    Else
                        MyField.Value = MyVal
                    End If
                End If
            Case Else
                If Val(MyField.Type) = ADODB.DataTypeEnum.adVarChar Or Val(MyField.Type) = ADODB.DataTypeEnum.adVarWChar Or _
                   Val(MyField.Type) = ADODB.DataTypeEnum.adWChar Or Val(MyField.Type) = ADODB.DataTypeEnum.adBSTR Then
                    If IsDBNull(MyVal) Then
                        MyField.Value = ""
                    Else
                        If Len(MyVal) > MyField.DefinedSize Then
                            MyField.Value = Mid(MyVal, 1, MyField.DefinedSize)
                        Else
                            MyField.Value = MyVal
                        End If
                    End If
                Else
                    MyField.Value = MyVal
                End If
        End Select

    End Sub





    Public Function MastroOspite(ByVal Cserv As String, ByVal CodiceOspite As Long) As Long
        Dim MyRs As New ADODB.Recordset
        Dim OspitiDb As New ADODB.Connection

        OspitiDb.Open(Session("DC_OSPITE"))
        MastroOspite = 0
        MyRs.Open("Select * From AnagraficaComune Where CodiceOspite = " & CodiceOspite & " And TIPOLOGIA = 'O'", OspitiDb, ADODB.CursorTypeEnum.adOpenKeyset, ADODB.LockTypeEnum.adLockOptimistic)
        If Not MyRs.EOF Then
            MastroOspite = MoveFromDb(MyRs.Fields("MastroCliente"))
            If MastroOspite = 0 Then
                Dim XCs As New Cls_CentroServizio

                XCs.Leggi(Session("DC_OSPITE"), Cserv)
                MastroOspite = XCs.MASTRO
            End If
        End If
        MyRs.Close()
        OspitiDb.Close()
    End Function

    Public Function ContoOspite(ByVal Cserv As String, ByVal CodiceOspite As Long) As Long
        Dim MyRs As New ADODB.Recordset
        Dim OspitiDb As New ADODB.Connection

        OspitiDb.Open(Session("DC_OSPITE"))

        ContoOspite = 0
        MyRs.Open("Select * From AnagraficaComune Where CodiceOspite = " & CodiceOspite & " And TIPOLOGIA = 'O'", OspitiDb, ADODB.CursorTypeEnum.adOpenKeyset, ADODB.LockTypeEnum.adLockOptimistic)
        If Not MyRs.EOF Then
            ContoOspite = MoveFromDb(MyRs.Fields("ContoCliente"))
            If ContoOspite = 0 Then
                Dim XCs As New Cls_CentroServizio

                XCs.Leggi(Session("DC_OSPITE"), Cserv)
                ContoOspite = XCs.CONTO
            End If
        End If
        MyRs.Close()
        OspitiDb.Close()
    End Function


    Public Function SottoContoOspite(ByVal Cserv As String, ByVal CodiceOspite As Long) As Long
        Dim MyRs As New ADODB.Recordset
        Dim OspitiDb As New ADODB.Connection

        OspitiDb.Open(Session("DC_OSPITE"))

        SottoContoOspite = 0
        MyRs.Open("Select * From AnagraficaComune Where CodiceOspite = " & CodiceOspite & " And TIPOLOGIA = 'O'", OspitiDb, ADODB.CursorTypeEnum.adOpenKeyset, ADODB.LockTypeEnum.adLockOptimistic)
        If Not MyRs.EOF Then
            SottoContoOspite = MoveFromDb(MyRs.Fields("SottoContoCliente"))
            If SottoContoOspite = 0 Then
                SottoContoOspite = CodiceOspite * 100
            End If
        End If
        MyRs.Close()
        OspitiDb.Close()
    End Function

    Public Function MastroParente(ByVal Cserv As String, ByVal CodiceOspite As Long, ByVal CODICEPARENTE As Long) As Long
        Dim MyRs As New ADODB.Recordset
        Dim OspitiDb As New ADODB.Connection

        OspitiDb.Open(Session("DC_OSPITE"))
        MastroParente = 0
        MyRs.Open("Select * From AnagraficaComune Where CodiceOspite  = " & CodiceOspite & " And CodiceParente = " & CODICEPARENTE & " And TIPOLOGIA = 'P'", OspitiDb, ADODB.CursorTypeEnum.adOpenKeyset, ADODB.LockTypeEnum.adLockOptimistic)
        If Not MyRs.EOF Then
            MastroParente = MoveFromDb(MyRs.Fields("MastroCliente"))
            If MastroParente = 0 Then
                Dim XCs As New Cls_CentroServizio

                XCs.Leggi(Session("DC_OSPITE"), Cserv)
                MastroParente = XCs.MASTRO
            End If
        End If
        MyRs.Close()
        OspitiDb.Close()
    End Function

    Public Function ContoParente(ByVal Cserv As String, ByVal CodiceOspite As Long, ByVal CODICEPARENTE As Long) As Long
        Dim MyRs As New ADODB.Recordset
        Dim OspitiDb As New ADODB.Connection

        OspitiDb.Open(Session("DC_OSPITE"))
        ContoParente = 0
        MyRs.Open("Select * From AnagraficaComune Where CodiceOspite = " & CodiceOspite & " And CodiceParente = " & CODICEPARENTE & " And TIPOLOGIA = 'P'", OspitiDb, ADODB.CursorTypeEnum.adOpenKeyset, ADODB.LockTypeEnum.adLockOptimistic)
        If Not MyRs.EOF Then
            ContoParente = MoveFromDb(MyRs.Fields("ContoCliente"))
            If ContoParente = 0 Then
                Dim XCs As New Cls_CentroServizio

                XCs.Leggi(Session("DC_OSPITE"), Cserv)
                ContoParente = XCs.CONTO
            End If
        End If
        MyRs.Close()
        OspitiDb.Close()
    End Function

    Public Function SottoContoParente(ByVal Cserv As String, ByVal CodiceOspite As Long, ByVal CODICEPARENTE As Long) As Long
        Dim MyRs As New ADODB.Recordset
        Dim OspitiDb As New ADODB.Connection

        OspitiDb.Open(Session("DC_OSPITE"))
        SottoContoParente = 0
        MyRs.Open("Select * From AnagraficaComune Where CodiceOspite = " & CodiceOspite & " And CodiceParente = " & CODICEPARENTE & " And TIPOLOGIA = 'P'", OspitiDb, ADODB.CursorTypeEnum.adOpenKeyset, ADODB.LockTypeEnum.adLockOptimistic)
        If Not MyRs.EOF Then
            SottoContoParente = MoveFromDb(MyRs.Fields("SottoContoCliente"))
            If SottoContoParente = 0 Then
                SottoContoParente = CodiceOspite * 100 + CODICEPARENTE
            End If
        End If
        MyRs.Close()
        OspitiDb.Close()
    End Function

    Public Function CampoTipoAddebito(ByVal Codice As String, ByVal Campo As String) As Object
        Dim MyRs As New ADODB.Recordset
        Dim OspitiDb As New ADODB.Connection

        OspitiDb.Open(Session("DC_OSPITE"))
        MyRs.Open("Select * From TabTipoAddebito Where Codice = '" & Codice & "' ", OspitiDb, ADODB.CursorTypeEnum.adOpenKeyset, ADODB.LockTypeEnum.adLockOptimistic)
        If Not MyRs.EOF Then
            CampoTipoAddebito = MyRs.Fields(Campo).Value
        End If
        MyRs.Close()
        OspitiDb.Close()
    End Function
    Public Function CodiceIVABollo(ByVal Data As Date) As String
        Dim MySql As String
        Dim MyRs As New ADODB.Recordset
        Dim TabelleDb As New ADODB.Connection

        TabelleDb.Open(Session("DC_TABELLE"))
        MySql = "Select top 1 CodiceIVA From Bolli Where DataValidita <= {ts '" & Format(Data, "yyyy-MM-dd") & " 00:00:00'} Order by DataValidita DESC"
        MyRs.Open(MySql, TabelleDb, ADODB.CursorTypeEnum.adOpenKeyset, ADODB.LockTypeEnum.adLockOptimistic)
        If Not MyRs.EOF Then
            CodiceIVABollo = MoveFromDbWC(MyRs, "CodiceIva")
        End If
        MyRs.Close()
        TabelleDb.Close()
    End Function


    Protected Sub ImageButton1_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles ImageButton1.Click
        If Not IsDate(Txt_DataRegistrazione.Text) Then
            REM Lbl_Errori.Text = "Data formalmente errata"
            
            ScriptManager.RegisterClientScriptBlock(Me.Page, Me.GetType(), "Errore", "VisualizzaErrore('Data formalmente errata');", True)
            Call EseguiJS()
            Exit Sub
        End If
        If Val(Txt_Anno.Text) = 0 Then
            ScriptManager.RegisterClientScriptBlock(Me.Page, Me.GetType(), "Errore", "VisualizzaErrore('Digitare Anno');", True)
            REM Lbl_Errori.Text = "Digitare Anno"
            Call EseguiJS()
            Exit Sub
        End If

        
        Dim m As New Cls_Parametri



        If m.InElaborazione(Session("DC_OSPITE")) Then
            ScriptManager.RegisterClientScriptBlock(Me.Page, Me.GetType(), "Errore", "VisualizzaErrore('Elaborazione in esecuzione non posso procedere');", True)
            Call EseguiJS()
            Exit Sub
        End If


        Dim ParametriGenerale As New Cls_ParametriGenerale

        If ParametriGenerale.InElaborazione(Session("DC_GENERALE")) Then
            ClientScript.RegisterClientScriptBlock(Me.GetType(), "Errore", "alert('Elaborazione in esecuzione non posso procedere');", True)
            Exit Sub
        End If


        Session("CODICESERVIZIO") = ViewState("CODICESERVIZIO")
        Session("CODICEOSPITE") = ViewState("CODICEOSPITE")


        If Session("CODICESERVIZIO") = "" Then
            ScriptManager.RegisterClientScriptBlock(Me.Page, Me.GetType(), "Errore", "VisualizzaErrore('Specificare codice servizio');", True)
            REM Lbl_Errori.Text = "Specificare codice servizio "
            Call EseguiJS()
            Exit Sub
        End If
        If Val(Session("CODICEOSPITE")) = 0 Then
            ScriptManager.RegisterClientScriptBlock(Me.Page, Me.GetType(), "Errore", "VisualizzaErrore('Specificare codice ospite');", True)
            REM Lbl_Errori.Text = "Specificare codice ospite"
            Call EseguiJS()
            Exit Sub
        End If

        


        


        Dim cn As OleDbConnection



        cn = New Data.OleDb.OleDbConnection(Session("DC_OSPITE"))

        cn.Open()
        Dim cmd As New OleDbCommand()
        cmd.CommandText = ("Delete  from notifiche ")
        cmd.Connection = cn
        cmd.ExecuteNonQuery()
        cn.Close()

        Lbl_Errori.Text = ""
        'Call CalcoloEmissione(Dd_Mese.SelectedValue, Txt_Anno.Text)        
        Call EstraiFTNC()

        Call EseguiJS()

    End Sub


    Protected Sub CalcoloEmissione(ByVal Mese As Integer, ByVal Anno As Integer)


        Dim ConnectionString As String = Session("DC_OSPITE")
        Dim ConnessioneOspiti As String = Session("DC_OSPITE")
        Dim TipoCentro As String
        Dim VarCentroServizio As String
        Dim varcodiceospite As Long

        Dim xMese As Integer
        Dim xAnno As Integer
        Dim cn As OleDbConnection
        Dim SessioneTP As System.Web.SessionState.HttpSessionState = Session



        cn = New Data.OleDb.OleDbConnection(ConnessioneOspiti)

        cn.Open()


        Dim UsoExtraParente As Integer = 0



        Dim cmdParenteRetta1 As New OleDbCommand()
        cmdParenteRetta1.CommandText = "SELECT count(*) as usati FROM [EXTRAOSPITE] where RIPARTIZIONE = 'P' "
        cmdParenteRetta1.Connection = cn
        Dim RDParenteRetta1 As OleDbDataReader = cmdParenteRetta1.ExecuteReader()
        If RDParenteRetta1.Read() Then
            If Val(campodb(RDParenteRetta1.Item(0))) > 0 Then
                UsoExtraParente = 1
            End If
        End If
        RDParenteRetta1.Close()


        Dim f As New Cls_Parametri

        f.LeggiParametri(ConnectionString)


        Dim sc2 As New MSScriptControl.ScriptControl

        sc2.Language = "vbscript"
        Dim XS As New Cls_CalcoloRette
        SyncLock (SessioneTP.SyncRoot())
            SessioneTP("CampoErrori") = ""
            SessioneTP("CampoProgressBar") = 0
        End SyncLock

        Dim MyCserv As New Cls_CentroServizio

        MyCserv.Leggi(ConnessioneOspiti, Session("CODICESERVIZIO"))

        
        XS.CampoParametriGIORNOUSCITA = f.GIORNOUSCITA
        XS.CampoParametriGIORNOENTRATA = f.GIORNOENTRATA
        XS.CampoParametriCAUSALEACCOGLIMENTO = f.CAUSALEACCOGLIMENTO
        XS.ConguaglioMinimo = f.ConguaglioMinimo
        XS.CampoParametriMastroAnticipo = f.MastroAnticipo
        XS.CampoParametriAddebitiAccreditiComulati = f.AddebitiAccreditiComulati
        XS.Prm_AzzeraSeNegativo = f.AzzeraSeNegativo
        XS.ApriDB(ConnessioneOspiti, Session("DC_OSPITIACCESSORI"))
        XS.STRINGACONNESSIONEDB = ConnessioneOspiti
        XS.ConnessioneGenerale = Session("DC_GENERALE")
        XS.CaricaCausali()

        XS.EliminaDatiRette(Mese, Anno, "", 0)

        

        VarCentroServizio = Session("CODICESERVIZIO")
        varcodiceospite = Session("CODICEOSPITE")

        Call XS.AzzeraTabella()


        XS.PrimoDataAccoglimentoOspite = XS.PrimaDataAccoglimento(VarCentroServizio, varcodiceospite, Mese, Anno)

        If UsoExtraParente = 1 Then
            XS.VerificaPrimaCalcolo(VarCentroServizio, varcodiceospite)
        End If


        XS.DeleteChiave(VarCentroServizio, varcodiceospite, Mese, Anno)

        XS.CampoParametriGIORNOUSCITA = f.GIORNOUSCITA
        XS.CampoParametriGIORNOENTRATA = f.GIORNOENTRATA
        XS.CampoParametriCAUSALEACCOGLIMENTO = f.CAUSALEACCOGLIMENTO
        XS.CampoParametriMastroAnticipo = f.MastroAnticipo
        XS.CampoParametriAddebitiAccreditiComulati = f.AddebitiAccreditiComulati
        XS.Prm_AzzeraSeNegativo = f.AzzeraSeNegativo

        If MyCserv.TIPOCENTROSERVIZIO = "D" Then
            XS.PresenzeAssenzeDiurno(VarCentroServizio, varcodiceospite, Mese, Anno)
        End If


        XS.CreaTabellaPresenze(VarCentroServizio, varcodiceospite, Mese, Anno)

        If MyCserv.TIPOCENTROSERVIZIO = "A" Then
            XS.PresenzeDomiciliare(VarCentroServizio, varcodiceospite, Mese, Anno)
        End If


        If Val(MyCserv.VerificaImpegnativa) = 1 Then
            Call XS.ModificaTabellaPresenze(VarCentroServizio, varcodiceospite, Mese, Anno)
        End If


        XS.CreaAddebitiAcrreditiProgrammati(VarCentroServizio, varcodiceospite, Mese, Anno)


        XS.CreaTabellaImportoOspite(VarCentroServizio, varcodiceospite, Mese, Anno)



        XS.CreaTabellaImportoComune(VarCentroServizio, varcodiceospite, Mese, Anno)
        XS.CreaTabellaUsl(VarCentroServizio, varcodiceospite, Mese, Anno)
        XS.CreaTabellaImportoRetta(VarCentroServizio, varcodiceospite, Mese, Anno)



        XS.CreaTabellaImportoJolly(VarCentroServizio, varcodiceospite, Mese, Anno)

        XS.CreaTabellaModalita(VarCentroServizio, varcodiceospite, Mese, Anno, True)

        XS.CreaTabellaImportoParente(VarCentroServizio, varcodiceospite, Mese, Anno)


        'Call CalcolaRettaDaTabella(Tabella, VarCentroServizio, VarCodiceOspite)
        XS.RiassociaComuni(VarCentroServizio, varcodiceospite, Mese, Anno)

        XS.CalcolaRettaDaTabella(VarCentroServizio, varcodiceospite, Anno, Mese, sc2)



        XS.CreaAddebitiAcrrediti(VarCentroServizio, varcodiceospite, Mese, Anno)

        XS.AllineaImportiMensili(VarCentroServizio, varcodiceospite, Mese, Anno)

        If Not XS.VerificaRette(VarCentroServizio, varcodiceospite, Mese, Anno) Then
            REM  Errori = True
        Else
            XS.SalvaDati(VarCentroServizio, varcodiceospite, Mese, Anno)
        End If

        If Chk_SoloMese.Checked = True Then
            If DD_OspiteParenti.SelectedValue = "0" Then
                Dim cmdDeleteRettaOspite As New OleDbCommand()
                'CENTROSERVIZIO, CODICEOSPITE, MESE, ANNO, STATOCONTABILE, ELEMENTO
                cmdDeleteRettaOspite.CommandText = "DELETE FROM RETTEOSPITE wHERE CENTROSERVIZIO =  ? aND CODICEOSPITE =  ? aND MESE =  ? aND ANNO =  ? aND ELEMENTO = ?"
                cmdDeleteRettaOspite.Connection = cn
                cmdDeleteRettaOspite.Parameters.AddWithValue("@CENTROSERVIZIO", VarCentroServizio)
                cmdDeleteRettaOspite.Parameters.AddWithValue("@CODICEOSPITE", varcodiceospite)
                cmdDeleteRettaOspite.Parameters.AddWithValue("@Mese", Mese)
                cmdDeleteRettaOspite.Parameters.AddWithValue("@Anno", Anno)
                cmdDeleteRettaOspite.Parameters.AddWithValue("@ELEMENTO", "RPX")
                cmdDeleteRettaOspite.ExecuteNonQuery()
            Else
                Dim cmdDeleteRettaParente As New OleDbCommand()
                'CENTROSERVIZIO, CODICEOSPITE, MESE, ANNO, STATOCONTABILE, ELEMENTO
                cmdDeleteRettaParente.CommandText = "DELETE FROM RETTEPARENTE wHERE CENTROSERVIZIO =  ? aND CODICEOSPITE =  ? aND CODICEPARENTE =  ? aND MESE =  ? aND ANNO =  ? aND ELEMENTO = ?"
                cmdDeleteRettaParente.Connection = cn
                cmdDeleteRettaParente.Parameters.AddWithValue("@CENTROSERVIZIO", VarCentroServizio)
                cmdDeleteRettaParente.Parameters.AddWithValue("@CODICEOSPITE", varcodiceospite)
                cmdDeleteRettaParente.Parameters.AddWithValue("@CODICEPARENTE", DD_OspiteParenti.SelectedValue)
                cmdDeleteRettaParente.Parameters.AddWithValue("@Mese", Mese)
                cmdDeleteRettaParente.Parameters.AddWithValue("@Anno", Anno)
                cmdDeleteRettaParente.Parameters.AddWithValue("@ELEMENTO", "RPX")
                cmdDeleteRettaParente.ExecuteNonQuery()
            End If
        End If



        Dim EmC As New Cls_EmissioneRetta

        Dim StringaErrore As String
        Dim StringaWarning As String
        Dim MeseContr As Integer

        Dim LeggiParametri As New Cls_Parametri

        LeggiParametri.LeggiParametri(ConnessioneOspiti)

        Mese = LeggiParametri.MeseFatturazione
        MeseContr = LeggiParametri.MeseFatturazione
        Anno = LeggiParametri.AnnoFatturazione
        EmC.ConnessioneGenerale = Session("DC_GENERALE")
        EmC.ConnessioneOspiti = ConnessioneOspiti
        EmC.ConnessioneTabelle = Session("DC_TABELLE")

        EmC.ApriDB()

        EmC.EliminaTemporanei()
        EmC.EmissioneDiProva = ""

        If DD_OspiteParenti.SelectedValue = "0" Then
            If LeggiParametri.EmissioneOspite = 0 Then
                Call EmC.CreaRettaOspite(MeseContr, Anno, "", Txt_DataRegistrazione.Text, Txt_DataRegistrazione.Text, Session)
            End If
        Else
            If LeggiParametri.EmissioneParente = 0 Then
                Call EmC.CreaRettaParente(MeseContr, Anno, "", Txt_DataRegistrazione.Text, Txt_DataRegistrazione.Text, Session)
            End If
        End If



        Tabella.Clear()
        Tabella.Columns.Clear()
        Tabella.Columns.Add("DescrizioneRiga", GetType(String))
        Tabella.Columns.Add("Importo", GetType(String))
        Tabella.Columns.Add("Giorni", GetType(String))
        Tabella.Columns.Add("IVA", GetType(String))
        Tabella.Columns.Add("Descrizione", GetType(String))
        Tabella.Columns.Add("ID", GetType(String))
        Tabella.Columns.Add("ContoRicavo", GetType(String))

        Dim GeneraleCon As New OleDb.OleDbConnection
        Dim Causale As String = ""

        GeneraleCon.ConnectionString = Session("DC_GENERALE")
        GeneraleCon.Open()

        Dim cmdREAD As New OleDbCommand()
        cmdREAD.CommandText = ("select * from Temp_MovimentiContabiliTesta")
        cmdREAD.Connection = GeneraleCon

        Dim ReadTesta As OleDbDataReader = cmdREAD.ExecuteReader()
        If ReadTesta.Read Then
            Causale = campodb(ReadTesta.Item("CausaleContabile"))
        End If
        ReadTesta.Close()

        Dim MyCausale As New Cls_CausaleContabile

        MyCausale.Codice = Causale
        MyCausale.Leggi(Session("DC_TABELLE"), MyCausale.Codice)

        Lbl_DecodificaCausale.Text = "<br><br>Causale Contabile " & MyCausale.Descrizione & "<br/>"

        Dim cmd As New OleDbCommand()
        cmd.CommandText = ("select * from Temp_MovimentiContabiliRiga where ( TIPO = '' OR TIPO IS NULL) ORDER BY RIGADACAUSALE")
        cmd.Connection = GeneraleCon

        Dim myPOSTreader As OleDbDataReader = cmd.ExecuteReader()
        Do While myPOSTreader.Read

            Dim myriga As System.Data.DataRow = Tabella.NewRow()
            If campodb(myPOSTreader.Item("RigaDaCausale")) = 3 Then
                myriga(0) = "Retta"
            End If
            If campodb(myPOSTreader.Item("RigaDaCausale")) = 4 Then
                myriga(0) = "Extra Fissi"
            End If
            If campodb(myPOSTreader.Item("RigaDaCausale")) = 5 Then
                myriga(0) = "Addebiti"
            End If
            If campodb(myPOSTreader.Item("RigaDaCausale")) = 6 Then
                myriga(0) = "Accrediti"
            End If
            If campodb(myPOSTreader.Item("RigaDaCausale")) = 9 Then
                myriga(0) = "Bollo"
            End If
            If campodb(myPOSTreader.Item("RigaDaCausale")) = 12 Then
                myriga(0) = "Sconto"
            End If
            If campodb(myPOSTreader.Item("RigaDaCausale")) = 13 Then
                myriga(0) = "Retta 2"
            End If
            If campodb(myPOSTreader.Item("RigaDaCausale")) = 14 Then
                myriga(0) = "Retta Percentuale"
            End If

            If MyCausale.TipoDocumento = "NC" Then
                If campodb(myPOSTreader.Item("DareAvere")) = "A" Then
                    myriga(1) = Modulo.MathRound(campodbn(myPOSTreader.Item("Importo")), 2)
                Else
                    myriga(1) = Modulo.MathRound(campodbn(myPOSTreader.Item("Importo")) * -1, 2)
                End If
            Else
                If campodb(myPOSTreader.Item("DareAvere")) = "A" Then
                    myriga(1) = Modulo.MathRound(campodbn(myPOSTreader.Item("Importo")), 2)
                Else
                    myriga(1) = Modulo.MathRound(campodbn(myPOSTreader.Item("Importo")) * -1, 2)
                End If
            End If

            myriga(2) = campodb(myPOSTreader.Item("Quantita"))

            If campodbn(myPOSTreader.Item("Quantita")) < 0 And Math.Abs(Math.Round(campodbn(myPOSTreader.Item("Importo")), 2)) = 0 Then
                myriga(2) = 0
            End If

            myriga(3) = campodb(myPOSTreader.Item("CodiceIva"))
            myriga(4) = campodb(myPOSTreader.Item("Descrizione"))
            myriga(5) = campodb(myPOSTreader.Item("id"))

            Dim MS As New Cls_Pianodeiconti
            MS.Mastro = campodb(myPOSTreader.Item("MastroPartita"))
            MS.Conto = campodb(myPOSTreader.Item("ContoPartita"))
            MS.Sottoconto = campodb(myPOSTreader.Item("SottocontoPartita"))
            MS.Decodfica(Session("DC_GENERALE"))

            myriga(6) = MS.Mastro & "  " & MS.Conto & " " & MS.Sottoconto & " " & MS.Descrizione

            Tabella.Rows.Add(myriga)
        Loop

        GeneraleCon.Close()


        GridV.AutoGenerateColumns = False

        GridV.DataSource = Tabella

        GridV.DataBind()


    End Sub

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Trim(Session("UTENTE")) = "" Then
            Response.Redirect("../Login.aspx")
            Exit Sub
        End If
        If Session("ABILITAZIONI").IndexOf("<PORTINERIA>") > 0 Then
            Response.Redirect("..\MainPortineria.aspx")
        End If

        If Page.IsPostBack = True Then
            Exit Sub
        End If

        Session("CampoWr") = ""
        Session("CampoErrori") = ""
        If Val(Request.Item("CodiceOspite")) > 0 And Request.Item("CentroServizio") <> "" Then

            Session("CODICESERVIZIO") = Request.Item("CentroServizio")
            Session("CODICEOSPITE") = Val(Request.Item("CodiceOspite"))

        End If


        ViewState("CODICESERVIZIO") = Session("CODICESERVIZIO")
        ViewState("CODICEOSPITE") = Session("CODICEOSPITE")

        Dim CParenti As New Cls_Parenti
        Dim COspite As New ClsOspite

        COspite.Leggi(Session("DC_OSPITE"), Session("CODICEOSPITE"))
        CParenti.UpDateDropBox(Session("DC_OSPITE"), Session("CODICEOSPITE"), DD_OspiteParenti)

        Dim K1 As New Cls_SqlString

        Dim Barra As New Cls_BarraSenior

        If Not IsNothing(Session("RicercaAnagraficaSQLString")) Then
            K1 = Session("RicercaAnagraficaSQLString")
        End If

        Lbl_BarraSenior.Text = Barra.CodiceBarra(Application("SENIOR"), Session("UTENTE"), Page, K1)

        Lbl_Errori.Text = ""
        Dim ConnectionString As String = Session("DC_OSPITE")

        Txt_DataRegistrazione.Text = Format(Now, "dd/MM/yyyy")

        Dim f As New Cls_Parametri

        f.LeggiParametri(ConnectionString)
        Dd_Mese.SelectedValue = f.MeseFatturazione
        Txt_Anno.Text = f.AnnoFatturazione


        If f.FattureNCNuovo = 1 Then
            Response.Redirect("FatturaNCNew.aspx??CodiceOspite=" & Session("CODICEOSPITE") & "&CentroServizio=" & Session("CODICESERVIZIO"))
            Exit Sub

        End If

        Dim x As New ClsOspite

        x.CodiceOspite = Session("CODICEOSPITE")
        x.Leggi(Session("DC_OSPITE"), x.CodiceOspite)

        If x.Compensazione = "D" Then
            RdFuoriRetta.Visible = True
            RdRetta.Visible = True
            RdRetta.Checked = True
        End If

        Dim cs As New Cls_CentroServizio

        cs.Leggi(Session("DC_OSPITE"), Session("CODICESERVIZIO"))

        Lbl_NomeOspite.Text = x.Nome & " -  " & x.DataNascita & " - " & cs.DESCRIZIONE & "<i style=""font-size:small;"">(" & x.CodiceOspite & ")</i>"

        Lb_GestioneDocumento.Visible = False
        Lb_StampaDocumento.Visible = False

        Lbl_Riferimento730.Visible = False
        Lbl_RiferimentoNumero.Visible = False
        Txt_DataRif.Visible = False
        Txt_RifNumero.Visible = False


        Txt_Anno.Enabled = False

        If f.ModificaPeriodoFatturaNC = 0 Then
            Dd_Mese.Enabled = False
        End If

        Call EseguiJS()

        Dim m As New Cls_Notifica


        m.Delete(Session("DC_OSPITE"))
    End Sub



    Private Sub EseguiJS()
        Dim MyJs As String


        MyJs = "$(document).ready(function() {"
        MyJs = MyJs & "var els = document.getElementsByTagName(""*"");"
        MyJs = MyJs & "if ($('#MENUDIV').html().length < 50) { ImpostaMenuAgraficaOspiti(); }"
        MyJs = MyJs & "for (var i=0;i<els.length;i++)"
        MyJs = MyJs & "if ( els[i].id ) { "
        MyJs = MyJs & " var appoggio =els[i].id; "
        MyJs = MyJs & " if ((appoggio.match('TxtData')!= null) || (appoggio.match('Txt_Data')!= null)) {  "
        MyJs = MyJs & " $(els[i]).mask(""99/99/9999"");"
        MyJs = MyJs & " $(els[i]).datepicker({ changeMonth: true,changeYear: true,  yearRange: ""1990:" & Year(Now) + 5 & """},$.datepicker.regional[""it""]);"
        MyJs = MyJs & "    }"
        MyJs = MyJs & " if ((appoggio.match('TxtImporto')!= null) || (appoggio.match('TxtPercentuale')!= null) ) {  "
        MyJs = MyJs & " $(els[i]).keypress(function() { ForceNumericInput($(this).val(), true, true); } ); "
        MyJs = MyJs & " $(els[i]).blur(function() { var ap = formatNumber($(this).val(),2); $(this).val(ap); });"
        MyJs = MyJs & "    }"

        MyJs = MyJs & " if (appoggio.match('ContoRicavo')!= null) {   "
        MyJs = MyJs & " $(els[i]).autocomplete('/WebHandler/GestioneConto.ashx?Utente=" & Session("UTENTE") & "', {delay:5,minChars:3});"
        MyJs = MyJs & "    }"

        MyJs = MyJs & " if ((appoggio.match('Txt_Comune')!= null) || (appoggio.match('Txt_ComRes')!= null))  {  "
        MyJs = MyJs & " $(els[i]).autocomplete('/WebHandler/AutocompleteComune.ashx?Utente=" & Session("UTENTE") & "', {delay:5,minChars:3});"
        MyJs = MyJs & "    }"

        MyJs = MyJs & "} "
        MyJs = MyJs & "});"


        'MyJs = "$(document).ready(function() { $('.myClass').keypress(function() { handleEnter($('.myClass').val(), true, true); } );     $('#" & Txt_ImportoPagato.ClientID & "').blur(function() { var ap = formatNumber ($('#" & Txt_ImportoPagato.ClientID & "').val(),2); $('#" & Txt_ImportoPagato.ClientID & "').val(ap); }); });"
        ScriptManager.RegisterStartupScript(Me, Me.GetType(), "visualizzaRitIm", MyJs, True)

    End Sub

    Protected Sub Btn_Esci_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles Btn_Esci.Click
        Response.Redirect("RicercaAnagrafica.aspx?TIPO=FATTURANC")
    End Sub

    Protected Sub GridV_RowDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs) Handles GridV.RowDataBound

        If e.Row.RowType = DataControlRowType.DataRow Then


            Dim LblID As Label = DirectCast(e.Row.FindControl("LblID"), Label)

            LblID.Text = Tabella.Rows(e.Row.RowIndex).Item(5).ToString


            Dim LblDescrizione As Label = DirectCast(e.Row.FindControl("LblDescrizioneRiga"), Label)

            LblDescrizione.Text = Tabella.Rows(e.Row.RowIndex).Item(0).ToString


            Dim TxtImporto As TextBox = DirectCast(e.Row.FindControl("TxtImporto"), TextBox)

            TxtImporto.Text = Tabella.Rows(e.Row.RowIndex).Item(1).ToString


            Dim TxtGiorni As TextBox = DirectCast(e.Row.FindControl("TxtGiorni"), TextBox)


            TxtGiorni.Text = Tabella.Rows(e.Row.RowIndex).Item(2).ToString

            Dim DDIva As DropDownList = DirectCast(e.Row.FindControl("DDIva"), DropDownList)

            Dim MyIva As New Cls_IVA

            MyIva.UpDateDropBox(Session("DC_TABELLE"), DDIva)
            DDIva.SelectedValue = Tabella.Rows(e.Row.RowIndex).Item(3).ToString



            Dim TxtDescrizione As TextBox = DirectCast(e.Row.FindControl("TxtDescrizione"), TextBox)

            TxtDescrizione.Text = Tabella.Rows(e.Row.RowIndex).Item(4).ToString

            Dim TxtContoRicavo As TextBox = DirectCast(e.Row.FindControl("TxtContoRicavo"), TextBox)

            TxtContoRicavo.Text = Tabella.Rows(e.Row.RowIndex).Item(6).ToString


            Dim AppImporto As Double = 0
            Dim AppQuantita As Double = 0
            If TxtImporto.Text = "" Then
                AppImporto = 0
            Else
                AppImporto = TxtImporto.Text
            End If
            If TxtGiorni.Text = "" Then
                AppQuantita = 0
            Else
                AppQuantita = TxtGiorni.Text
            End If
            If AppImporto = 0 And AppQuantita = 0 Then
                TxtImporto.Enabled = False
                TxtGiorni.Enabled = False
                TxtDescrizione.Enabled = False
                LblID.Enabled = False
                LblDescrizione.Enabled = False
                DDIva.Enabled = False
                TxtContoRicavo.Enabled = False
                e.Row.Visible = False
            End If




            Call EseguiJS()

        End If
    End Sub

    Protected Sub Lb_GestioneDocumento_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles Lb_GestioneDocumento.Click
        Dim MyJS As String

        MyJS = "window.open('../GeneraleWeb/Documenti.aspx?NumeroRegistrazione=" & Session("Doc_creato_Numero") & "','Documenti','');"

        ScriptManager.RegisterStartupScript(Me, Me.GetType(), "visualizzaRitIm", MyJS, True)
    End Sub

    Protected Sub Lb_StampaDocumento_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles Lb_StampaDocumento.Click
        Dim MyJS As String

        Dim MyDoc As New Cls_MovimentoContabile

        MyDoc.Leggi(Session("DC_GENERALE"), Session("Doc_creato_Numero"))


        MyJS = "window.open('StampaDocumenti.aspx?Anno=" & Year(MyDoc.DataRegistrazione) & "&AnnoCompetenza=" & MyDoc.AnnoCompetenza & "&MeseDocumento=" & MyDoc.MeseCompetenza & "&NumeroDal=" & MyDoc.NumeroProtocollo & "&RegistroIVA=" & MyDoc.RegistroIVA & "','Documenti','');"

        ScriptManager.RegisterStartupScript(Me, Me.GetType(), "visualizzaRitIm", MyJS, True)
    End Sub

    Protected Overrides Sub Finalize()
        MyBase.Finalize()
    End Sub


    Public Function CampoExtraFisso(ByVal Codice As String, ByVal Campo As String) As Object
        Dim MyRs As New ADODB.Recordset
        Dim OspitiDB As New ADODB.Connection


        OspitiDB.Open(Session("DC_OSPITE"))

        MyRs.Open("Select * From TABELLAEXTRAFISSI Where CodiceExtra = '" & Codice & "'", OspitiDB, ADODB.CursorTypeEnum.adOpenKeyset, ADODB.LockTypeEnum.adLockOptimistic)
        If MyRs.EOF Then
            CampoExtraFisso = 0
        Else
            CampoExtraFisso = MoveFromDb(MyRs.Fields(Campo))
        End If
        MyRs.Close()
        OspitiDB.Close()
    End Function




    Protected Sub Btn_Modifica_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles Btn_Modifica.Click
        Dim I As Integer
        Dim NumeroRegistrazione As Long
        Dim Causale As String = ""
        Dim GeneraleCon As New OleDb.OleDbConnection
        Dim MastroCF As Long
        Dim ContoCF As Long
        Dim SottoContoCF As Long



        Dim m1 As New Cls_Parametri

        Dim ParametriGenerale As New Cls_ParametriGenerale



        If m1.InElaborazione(Session("DC_OSPITE")) Then
            ClientScript.RegisterClientScriptBlock(Me.GetType(), "Errore", "VisualizzaErrore('Elaborazione in esecuzione non posso procedere');", True)
            Call EseguiJS()
            Exit Sub
        End If

        If ParametriGenerale.InElaborazione(Session("DC_GENERALE")) Then
            ClientScript.RegisterClientScriptBlock(Me.GetType(), "Errore", "VisualizzaErrore('Elaborazione in esecuzione non posso procedere');", True)
            Call EseguiJS()
            Exit Sub
        End If


        m1.Elaborazione(Session("DC_OSPITE"))

        ParametriGenerale.Elaborazione(Session("DC_GENERALE"))

        Session("CODICESERVIZIO") = ViewState("CODICESERVIZIO")
        Session("CODICEOSPITE") = ViewState("CODICEOSPITE")

        GeneraleCon.ConnectionString = Session("DC_GENERALE")
        GeneraleCon.Open()

        Dim cmdREAD As New OleDbCommand()
        cmdREAD.CommandText = ("select * from Temp_MovimentiContabiliTesta")
        cmdREAD.Connection = GeneraleCon


        Dim myPOSTreader As OleDbDataReader = cmdREAD.ExecuteReader()
        If myPOSTreader.Read Then
            NumeroRegistrazione = campodb(myPOSTreader.Item("NumeroRegistrazione"))
            Causale = campodb(myPOSTreader.Item("CausaleContabile"))
        End If
        myPOSTreader.Close()


        Dim CausaleContabile As New Cls_CausaleContabile

        CausaleContabile.Codice = Causale
        CausaleContabile.Leggi(Session("DC_TABELLE"), Causale)
        If CausaleContabile.TipoDocumento = "NC" Then
            Dim cmdWrite As New OleDbCommand()
            cmdWrite.CommandText = ("UPDATE Temp_MovimentiContabiliTesta set RifNumero = ?,RifData = ? Where NumeroRegistrazione =?  ")
            cmdWrite.Connection = GeneraleCon
            cmdWrite.Parameters.AddWithValue("RifNumero", Txt_RifNumero.Text)
            Dim DataRiferimento As Date

            DataRiferimento = Txt_DataRif.Text
            cmdWrite.Parameters.AddWithValue("RifData", DataRiferimento)
            cmdWrite.Parameters.AddWithValue("NumeroRegistrazione", NumeroRegistrazione)
            cmdWrite.ExecuteNonQuery()            
        End If




        Dim cmdREADCF As New OleDbCommand()
        cmdREADCF.CommandText = ("select * from Temp_MovimentiContabiliRiga where TIPO = 'CF'")
        cmdREADCF.Connection = GeneraleCon

        Dim myPOSTreaderCF As OleDbDataReader = cmdREADCF.ExecuteReader()
        If myPOSTreaderCF.Read Then
            MastroCF = campodb(myPOSTreaderCF.Item("MastroPartita"))
            ContoCF = campodb(myPOSTreaderCF.Item("ContoPartita"))
            SottoContoCF = campodb(myPOSTreaderCF.Item("SottocontoPartita"))
        End If
        myPOSTreaderCF.Close()


        Dim MyCausale As New Cls_CausaleContabile

        MyCausale.Leggi(Session("DC_TABELLE"), Causale)

        Dim cmd As New OleDbCommand()
        cmd.CommandText = ("DELETE from Temp_MovimentiContabiliRiga where TIPO = 'IV' ")
        cmd.Connection = GeneraleCon
        cmd.ExecuteNonQuery()


        For I = 0 To GridV.Rows.Count - 1

            Dim TxtImporto As TextBox = DirectCast(GridV.Rows(I).FindControl("TxtImporto"), TextBox)
            Dim TxtGiorni As TextBox = DirectCast(GridV.Rows(I).FindControl("TxtGiorni"), TextBox)
            Dim DDIva As DropDownList = DirectCast(GridV.Rows(I).FindControl("DDIva"), DropDownList)
            Dim TxtDescrizione As TextBox = DirectCast(GridV.Rows(I).FindControl("TxtDescrizione"), TextBox)
            Dim LblDescrizione As Label = DirectCast(GridV.Rows(I).FindControl("LblDescrizioneRiga"), Label)
            Dim LblID As Label = DirectCast(GridV.Rows(I).FindControl("LblID"), Label)
            Dim TxtContoRicavo As TextBox = DirectCast(GridV.Rows(I).FindControl("TxtContoRicavo"), TextBox)

            Dim Imponibile As Double
            Dim Imposta As Double

            If TxtImporto.Text = "" Then
                TxtImporto.Text = "0"
            End If
            If TxtGiorni.Text = "" Then
                TxtGiorni.Text = "0"
            End If


            If TxtGiorni.Text = "0" And TxtImporto.Text = "0" Then
                Dim cmdMod As New OleDbCommand()
                cmdMod.CommandText = ("DELETE Temp_MovimentiContabiliRiga where id = ? ")
                cmdMod.Connection = GeneraleCon
                cmdMod.Parameters.AddWithValue("@ID", Val(LblID.Text))
                cmdMod.ExecuteNonQuery()
            Else
                Imponibile = TxtImporto.Text
                Dim MyIva As New Cls_IVA
                MyIva.Codice = DDIva.SelectedValue
                MyIva.Leggi(Session("DC_TABELLE"), MyIva.Codice)

                Imposta = Modulo.MathRound(Imponibile * MyIva.Aliquota, 2)


                Dim cmdIns As New OleDbCommand()
                cmdIns.CommandText = ("INSERT INTO Temp_MovimentiContabiliRiga (Utente,DataAggiornamento,Numero,MastroPartita,ContoPartita,SottocontoPartita,MastroContropartita,ContoContropartita,SottocontoContropartita,Importo,DareAvere,Segno,Tipo,CodiceIVA,Imponibile,RigaDaCausale) VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?) ")
                cmdIns.Connection = GeneraleCon
                cmdIns.Parameters.AddWithValue("@Utente", Session("UTENTE"))
                cmdIns.Parameters.AddWithValue("@DataAggiornamento", Now)
                cmdIns.Parameters.AddWithValue("@Numero", NumeroRegistrazione)
                cmdIns.Parameters.AddWithValue("@MastroPartita", MyCausale.Righe(1).Mastro)
                cmdIns.Parameters.AddWithValue("@ContoPartita", MyCausale.Righe(1).Conto)
                cmdIns.Parameters.AddWithValue("@SottocontoPartita", MyCausale.Righe(1).Sottoconto)

                cmdIns.Parameters.AddWithValue("@MastroContropartita", MastroCF)
                cmdIns.Parameters.AddWithValue("@ContoContropartita", ContoCF)
                cmdIns.Parameters.AddWithValue("@SottocontoContropartita", SottoContoCF)

                cmdIns.Parameters.AddWithValue("@Importo", Math.Abs(Imposta))
                If MyCausale.TipoDocumento = "NC" Then
                    If CDbl(TxtImporto.Text) < 0 Then
                        cmdIns.Parameters.AddWithValue("@DareAvere", "D")
                    Else
                        cmdIns.Parameters.AddWithValue("@DareAvere", "A")
                    End If
                Else
                    If CDbl(TxtImporto.Text) < 0 Then
                        cmdIns.Parameters.AddWithValue("@DareAvere", "D")
                    Else
                        cmdIns.Parameters.AddWithValue("@DareAvere", "A")
                    End If
                End If
                cmdIns.Parameters.AddWithValue("@Segno", "+")
                cmdIns.Parameters.AddWithValue("@Tipo", "IV")
                cmdIns.Parameters.AddWithValue("@CodiceIVA", DDIva.SelectedValue)
                cmdIns.Parameters.AddWithValue("@Imponibile", Math.Abs(Imponibile))
                cmdIns.Parameters.AddWithValue("@RigaDaCausale", 2)


                cmdIns.ExecuteNonQuery()

                Dim Vettore(100) As String





                If TxtContoRicavo.Text = "" Then
                    Dim cmdMod As New OleDbCommand()
                    cmdMod.CommandText = ("UPDATE Temp_MovimentiContabiliRiga SET   Importo = ?,Quantita = ?,CodiceIVA=?,Descrizione  = ?,DareAvere = ?  where id = ? ")
                    cmdMod.Connection = GeneraleCon

                    cmdMod.Parameters.AddWithValue("@Importo", Math.Abs(CDbl(TxtImporto.Text)))
                    cmdMod.Parameters.AddWithValue("@Quantita", Val(TxtGiorni.Text))
                    cmdMod.Parameters.AddWithValue("@CodiceIVA", DDIva.SelectedValue)
                    cmdMod.Parameters.AddWithValue("@Descrizione", TxtDescrizione.Text)
                    If MyCausale.TipoDocumento = "NC" Then
                        If CDbl(TxtImporto.Text) < 0 Then
                            cmdMod.Parameters.AddWithValue("@DareAvere", "D")
                        Else
                            cmdMod.Parameters.AddWithValue("@DareAvere", "A")
                        End If
                    Else
                        If CDbl(TxtImporto.Text) < 0 Then
                            cmdMod.Parameters.AddWithValue("@DareAvere", "D")
                        Else
                            cmdMod.Parameters.AddWithValue("@DareAvere", "A")
                        End If
                    End If


                    cmdMod.Parameters.AddWithValue("@ID", Val(LblID.Text))
                    cmdMod.ExecuteNonQuery()
                Else

                    Vettore = SplitWords(TxtContoRicavo.Text)


                    Dim cmdMod As New OleDbCommand()
                    cmdMod.CommandText = ("UPDATE Temp_MovimentiContabiliRiga SET   Importo = ?,Quantita = ?,CodiceIVA=?,Descrizione  = ?,DareAvere = ?,MastroPartita = ?,ContoPartita =?,SottocontoPartita =?  where id = ? ")
                    cmdMod.Connection = GeneraleCon

                    cmdMod.Parameters.AddWithValue("@Importo", Math.Abs(CDbl(TxtImporto.Text)))
                    cmdMod.Parameters.AddWithValue("@Quantita", Val(TxtGiorni.Text))
                    cmdMod.Parameters.AddWithValue("@CodiceIVA", DDIva.SelectedValue)
                    cmdMod.Parameters.AddWithValue("@Descrizione", TxtDescrizione.Text)
                    If MyCausale.TipoDocumento = "NC" Then
                        If CDbl(TxtImporto.Text) < 0 Then
                            cmdMod.Parameters.AddWithValue("@DareAvere", "D")
                        Else
                            cmdMod.Parameters.AddWithValue("@DareAvere", "A")
                        End If
                    Else
                        If CDbl(TxtImporto.Text) < 0 Then
                            cmdMod.Parameters.AddWithValue("@DareAvere", "D")
                        Else
                            cmdMod.Parameters.AddWithValue("@DareAvere", "A")
                        End If
                    End If

                    cmdMod.Parameters.AddWithValue("@MastroPartita", Vettore(0))
                    cmdMod.Parameters.AddWithValue("@ContoPartita", Vettore(1))
                    cmdMod.Parameters.AddWithValue("@SottoContoPartita", Vettore(2))

                    cmdMod.Parameters.AddWithValue("@ID", Val(LblID.Text))
                    cmdMod.ExecuteNonQuery()
                End If
            End If

        Next
        Dim DareAvere As Double = 0

        Dim cmdSUM As New OleDbCommand()
        cmdSUM.CommandText = ("select SUM(IMPORTO) from Temp_MovimentiContabiliRiga where DareAvere  = 'D' And  (TIPO <> 'CF' or TIPO IS NULL) ")
        cmdSUM.Connection = GeneraleCon

        Dim RDSUM As OleDbDataReader = cmdSUM.ExecuteReader()
        If RDSUM.Read Then
            DareAvere = campodbn(RDSUM.Item(0))
        End If
        RDSUM.Close()


        Dim cmdSUMA As New OleDbCommand()
        cmdSUMA.CommandText = ("select SUM(IMPORTO) from Temp_MovimentiContabiliRiga where DareAvere  = 'A' And  (TIPO <> 'CF' or TIPO IS NULL) ")
        cmdSUMA.Connection = GeneraleCon

        Dim RDSUMA As OleDbDataReader = cmdSUMA.ExecuteReader()
        If RDSUMA.Read Then
            DareAvere = DareAvere - campodbn(RDSUMA.Item(0))
        End If
        RDSUMA.Close()

        Dim cmdModcf As New OleDbCommand()
        cmdModcf.CommandText = ("UPDATE Temp_MovimentiContabiliRiga SET   Importo = ?   where TIPO= 'CF' ")
        cmdModcf.Connection = GeneraleCon
        cmdModcf.Parameters.AddWithValue("@Importo", Math.Abs(Modulo.MathRound(DareAvere, 2)))
        cmdModcf.ExecuteNonQuery()


        GeneraleCon.Close()


        Dim Em As New Cls_EmissioneRetta

        Em.ConnessioneGenerale = Session("DC_GENERALE")
        Em.ConnessioneOspiti = Session("DC_OSPITE")
        Em.ConnessioneTabelle = Session("DC_TABELLE")
        Em.ApriDB()

        Call Em.VerificaRegistrazioneIVA(NumeroRegistrazione, False)

        Dim LeggiParametri As New Cls_Parametri


        LeggiParametri.LeggiParametri(Em.ConnessioneOspiti)

        If LeggiParametri.RaggruppaRicavi = 1 Then
            Call Em.RaggruppaContiRicavo()
        End If


        Em.CloseDB()

        Dim Documento As New Cls_MovimentoContabile

        Documento.NumeroRegistrazione = NumeroRegistrazione
        Documento.Leggi(Session("DC_GENERALE"), Documento.NumeroRegistrazione, True)

        Dim M As New Cls_DescrizioneSuDocumento

        M.ModificaDescrizioneDocumento(Documento, Session("DC_OSPITE"), Session("DC_TABELLE"), Session("DC_GENERALE"))


        Response.Redirect("StampaFattureProva.aspx?TIPO=NONEMISSIONE&URL=FatturaNC.aspx")
    End Sub


    Private Function SplitWords(ByVal s As String) As String()
        '
        ' Call Regex.Split function from the imported namespace.
        ' Return the result array.
        '
        Return Regex.Split(s, "\W+")
    End Function

    Private Sub NumeraRiferimentoFattura()
        Dim Mastro As Long
        Dim Conto As Long
        Dim Sottoconto As Long
        Dim MySql As String


        Txt_DataRif.Text = ""
        Txt_RifNumero.Text = ""

        Dim cn As OleDbConnection

        cn = New Data.OleDb.OleDbConnection(Session("DC_GENERALE"))

        cn.Open()


        Dim XCs As New Cls_CentroServizio
        XCs.Leggi(Session("DC_OSPITE"), Session("CODICESERVIZIO"))
        If DD_OspiteParenti.SelectedValue = 99 Then
            Mastro = XCs.MASTRO
            Conto = XCs.CONTO
            Sottoconto = Session("CODICEOSPITE") * 100
        Else
            Mastro = XCs.MASTRO
            Conto = XCs.CONTO
            Sottoconto = (Session("CODICEOSPITE") * 100) + DD_OspiteParenti.SelectedValue
        End If


        If Mastro = 0 Or Sottoconto = 0 Then Exit Sub



        MySql = "SELECT NumeroRegistrazione,DataRegistrazione,NumeroDocumento,DataDocumento,NumeroProtocollo,AnnoProtocollo,CausaleContabile,MovimentiContabiliRiga.Descrizione,DareAvere,Importo,RigaDaCausale,MastroContropartita,ContoContropartita,SottocontoContropartita,MovimentiContabiliTesta.NumeroBolletta,MovimentiContabiliTesta.DataBolletta " & _
                " FROM MovimentiContabiliRiga , MovimentiContabiliTesta " & _
                " WHERE MovimentiContabiliRiga.Numero = MovimentiContabiliTesta.NumeroRegistrazione " & _
                " AND MastroPartita = " & Mastro & _
                " AND  ContoPartita = " & Conto & _
                " AND SottoContoPartita = " & Sottoconto
        MySql = MySql & " ORDER BY  DataRegistrazione DESC,NumeroRegistrazione Desc, RigaDaCausale, MastroPartita, ContoPartita, SottocontoPartita"

        Dim cmd As New OleDbCommand()

        cmd.CommandText = (MySql)
        cmd.Connection = cn
        Dim Saldo As Double
        Saldo = 0
        Dim myPOSTreader As OleDbDataReader = cmd.ExecuteReader()
        Do While myPOSTreader.Read
            Dim Causale As New Cls_CausaleContabile

            Causale.Codice = campodb(myPOSTreader.Item("CausaleContabile"))
            Causale.Leggi(Session("DC_TABELLE"), Causale.Codice)
            If Causale.TipoDocumento = "FA" Or Causale.TipoDocumento = "RE" Then
                Txt_DataRif.Text = Format(campodbd(myPOSTreader.Item("DataRegistrazione")), "dd/MM/yyyy")
                Txt_RifNumero.Text = campodb(myPOSTreader.Item("NumeroProtocollo"))
                Exit Sub
            End If


        Loop
        myPOSTreader.Close()
        cn.Close()
    End Sub

    Protected Sub DD_OspiteParenti_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles DD_OspiteParenti.SelectedIndexChanged
        If DD_OspiteParenti.SelectedValue = "0" Or DD_OspiteParenti.SelectedValue = "" Then
            Dim DeAna As New ClsOspite

            DeAna.CodiceOspite = Session("CODICEOSPITE")
            DeAna.Leggi(Session("DC_OSPITE"), DeAna.CodiceOspite)

            RdFuoriRetta.Visible = False
            RdRetta.Visible = False


            If DeAna.Compensazione = "D" Then
                RdFuoriRetta.Visible = True
                RdRetta.Visible = True
                RdRetta.Checked = True
            End If
        Else
            Dim DeAna As New Cls_Parenti

            DeAna.CodiceOspite = Session("CODICEOSPITE")
            DeAna.CodiceParente = Val(DD_OspiteParenti.SelectedValue)
            DeAna.Leggi(Session("DC_OSPITE"), DeAna.CodiceOspite, DeAna.CodiceParente)

            Dim DatiCserv As New Cls_DatiOspiteParenteCentroServizio

            DatiCserv.CodiceOspite = DeAna.CodiceOspite
            DatiCserv.CodiceParente = DeAna.CodiceParente
            DatiCserv.CentroServizio = Session("CODICESERVIZIO")
            DatiCserv.Leggi(Session("DC_OSPITE"))
            If DatiCserv.Compensazione <> "" Then

                DeAna.Compensazione = DatiCserv.Compensazione
            End If


            RdFuoriRetta.Visible = False
            RdRetta.Visible = False

            If DeAna.Compensazione = "D" Then
                RdFuoriRetta.Visible = True
                RdRetta.Visible = True
                RdRetta.Checked = True
            End If
        End If
        Call EseguiJS()
    End Sub
End Class


