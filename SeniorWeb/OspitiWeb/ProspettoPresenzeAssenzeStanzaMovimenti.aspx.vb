﻿Imports System
Imports System.Web
Imports Microsoft.VisualBasic
Imports System.Data.OleDb
Imports System.Web.Hosting
Imports System.Data.SqlTypes
Imports System.Net
Imports System.Collections.Generic
Imports System.IO
Imports System.Security.Cryptography.X509Certificates
Imports System.Net.Security
Imports System.Web.Script.Serialization
Imports org.jivesoftware.util
Imports Newtonsoft.Json
Imports Newtonsoft.Json.Linq
Imports System.Threading

Partial Class OspitiWeb_ProspettoPresenzeAssenzeStanzaMovimenti
    Inherits System.Web.UI.Page
    Dim MyTable As New System.Data.DataTable("tabella")
    Dim MyDataSet As New System.Data.DataSet()

    Private Sub Estrai()
        If Txt_DataDal.Text = "" Then
            Exit Sub
        End If
        If Txt_DataAl.Text = "" Then
            Exit Sub
        End If

        If Not IsDate(Txt_DataDal.Text) Then
            Exit Sub
        End If

        If Not IsDate(Txt_DataAl.Text) Then
            Exit Sub
        End If

        Dim Presente As Boolean
        Dim Stanza As String
        Dim CentroServizio As String
        Dim MySql As String
        Dim PresenteNelRange As Boolean
        Dim MovimentoInserito As Integer

        Dim cn As OleDbConnection
        Dim cnOspitiAccessori As OleDbConnection



        cn = New Data.OleDb.OleDbConnection(Session("DC_OSPITE"))

        cn.Open()

        cnOspitiAccessori = New Data.OleDb.OleDbConnection(Session("DC_OSPITIACCESSORI"))

        cnOspitiAccessori.Open()


        Dim VSession As New VariabiliSesssione


        VSession.DC_OSPITE = Session("DC_OSPITE")
        VSession.DC_OSPITIACCESSORI = Session("DC_OSPITIACCESSORI")
        VSession.DC_TABELLE = Session("DC_TABELLE")
        VSession.DC_GENERALE = Session("DC_GENERALE")
        VSession.UTENTE = Session("EPersonamUser")
        VSession.PASSWORD = Session("EPersonamPSW")
        VSession.RICERCAOSPITI = Request.Item("RICERCA")

        Dim Token As String = ""

        Try
            Token = LoginPersonam(VSession)
        Catch ex As Exception

        End Try

        If Token <> "" Then
            AssegnaStanze(Token, VSession, Txt_DataAl.Text)
        End If


        Dim DataDal As Date = Txt_DataDal.Text
        Dim DataAl As Date = Txt_DataAl.Text

        MyTable.Clear()
        MyTable.Columns.Clear()

        MyTable.Columns.Add("CodiceOspite", GetType(String))
        MyTable.Columns.Add("Nome Ospite", GetType(String))
        MyTable.Columns.Add("Stanza", GetType(String))
        MyTable.Columns.Add("Movimento Data", GetType(String))
        MyTable.Columns.Add("Tipo", GetType(String))
        MyTable.Columns.Add("Tipologia", GetType(String))
        MyTable.Columns.Add("Centro Servizio", GetType(String))



        MySql = "SELECT * FROM  AnagraficaComune Where CodiceOspite > 0 And CodiceParente =0 and (NonInUso is null or NonInUso = '')  ORDER BY CodiceOspite"
        Dim cmd As New OleDbCommand()


        cmd.CommandText = MySql

        cmd.Connection = cn
        Dim MyRs As OleDbDataReader = cmd.ExecuteReader()
        Do While MyRs.Read

            Presente = False
            Stanza = ""
            CentroServizio = ""

            Dim cmdMovimenti As New OleDbCommand()
            If MyRs.Item("codiceospite") = 170 Then

                Stanza = ""
            End If

            cmdMovimenti.CommandText = "Select * From Movimenti Where Data < {ts '" & Format(DataDal, "yyyy-MM-dd") & " 00:00:00'} And CodiceOspite = " & MyRs.Item("codiceospite") & " Order By Data Desc,TipoMov Desc"

            cmdMovimenti.Connection = cn
            Dim RsMovimenti As OleDbDataReader = cmdMovimenti.ExecuteReader()
            If RsMovimenti.Read Then
                If RsMovimenti.Item("TipoMov") = "13" Then
                    Presente = False
                Else
                    Presente = True
                    CentroServizio = RsMovimenti.Item("CentroServizio")
                End If
            End If
            RsMovimenti.Close()

            If Presente = True Then
                Dim cmdStanze As New OleDbCommand()


                cmdStanze.CommandText = "Select * From Movimenti Where Data <= ? And  CodiceOspite = " & MyRs.Item("codiceospite") & " Order By Data Desc"
                cmdStanze.Parameters.AddWithValue("@Data", DataDal)

                cmdStanze.Connection = cnOspitiAccessori
                Dim RsStanze As OleDbDataReader = cmdStanze.ExecuteReader()
                If RsStanze.Read Then

                    Stanza = RsStanze.Item("Letto")
                End If
                RsStanze.Close()
            End If


            PresenteNelRange = False
            If Presente = False Then
                Dim cmdMovimentiEU As New OleDbCommand()


                cmdMovimentiEU.CommandText = "Select * From Movimenti Where Data >= {ts '" & Format(DataDal, "yyyy-MM-dd") & " 00:00:00'} And Data <= {ts '" & Format(DataAl, "yyyy-MM-dd") & " 00:00:00'} And  CodiceOspite = " & MyRs.Item("CodiceOspite") & " Order By Data Desc"

                cmdMovimentiEU.Connection = cn
                Dim RsMovimentiEU As OleDbDataReader = cmdMovimentiEU.ExecuteReader()
                If RsMovimentiEU.Read Then
                    PresenteNelRange = True
                End If
                RsMovimentiEU.Close()

            End If

            If PresenteNelRange = True Or Presente = True Then
                MovimentoInserito = 0
                If Presente = True And PresenteNelRange = False Then
                    MovimentoInserito = 1
                    Dim myriga As System.Data.DataRow = MyTable.NewRow()

                    myriga(0) = MyRs.Item("CodiceOspite")
                    myriga(1) = MyRs.Item("Nome")
                    myriga(2) = Stanza
                    myriga(3) = Format(DataDal, "dd/MM/yyyy")
                    myriga(5) = CentroServizio

                    MyTable.Rows.Add(myriga)
                End If

                Dim VettoreData(100) As Date
                Dim VettoreStanza(100) As String
                Dim VettoreTipoMovimento(100) As String
                Dim VettoreCentroServizio(100) As String
                Dim Indice As Integer

                Indice = 0

                Dim cmdMovimentiEUV As New OleDbCommand()

                cmdMovimentiEUV.CommandText = "Select * From Movimenti Where Data > {ts '" & Format(DataDal, "yyyy-MM-dd") & " 00:00:00'} And Data <= {ts '" & Format(DataAl, "yyyy-MM-dd") & " 00:00:00'} And  CodiceOspite = " & MyRs.Item("CodiceOspite") & " Order By Data "

                cmdMovimentiEUV.Connection = cn
                Dim RsMovimentiEUV As OleDbDataReader = cmdMovimentiEUV.ExecuteReader()

                Do While RsMovimentiEUV.Read
                    VettoreData(Indice) = RsMovimentiEUV.Item("Data")
                    If RsMovimentiEUV.Item("TipoMov") = "13" Then
                        VettoreTipoMovimento(Indice) = "Uscita Definitiva"
                    End If
                    If RsMovimentiEUV.Item("TipoMov") = "05" Then
                        VettoreTipoMovimento(Indice) = "Accoglimento"
                    End If
                    If RsMovimentiEUV.Item("TipoMov") = "04" Then
                        VettoreTipoMovimento(Indice) = "Entrata"
                    End If
                    If RsMovimentiEUV.Item("TipoMov") = "03" Then
                        VettoreTipoMovimento(Indice) = "Uscita"
                    End If
                    VettoreStanza(Indice) = ""
                    VettoreCentroServizio(Indice) = RsMovimentiEUV.Item("CentroServizio")
                    Indice = Indice + 1
                Loop
                RsMovimentiEUV.Close()


                Dim cmdStanzeV As New OleDbCommand()

                cmdStanzeV.CommandText = "Select * From Movimenti Where Data > {ts '" & Format(DataDal, "yyyy-MM-dd") & " 00:00:00'} And Data <= {ts '" & Format(DataAl, "yyyy-MM-dd") & " 00:00:00'} And  CodiceOspite = " & MyRs.Item("CodiceOspite") & " Order By Data "

                cmdStanzeV.Connection = cnOspitiAccessori
                Dim RsStanzeV As OleDbDataReader = cmdStanzeV.ExecuteReader()

                Do While RsStanzeV.Read
                    'Stanza = DecodificaTabellaAccessori("VIL", MoveFromDbWC(RsStanze, "Villa")) & " " & DecodificaTabellaAccessori("REP", MoveFromDbWC(RsStanze, "Reparto")) & " " & MoveFromDbWC(RsStanze, "Piano") & " " & MoveFromDbWC(RsStanze, "Stanza") & " " & MoveFromDbWC(RsStanze, "Letto")
                    Stanza = RsStanzeV.Item("Letto")
                    VettoreData(Indice) = RsStanzeV.Item("Data")
                    VettoreStanza(Indice) = Stanza

                    If RsStanzeV.Item("Tipologia") = "OC" Then
                        VettoreTipoMovimento(Indice) = "Occupazione stanza"
                    End If
                    If RsStanzeV.Item("Tipologia") = "LI" Then
                        VettoreTipoMovimento(Indice) = "Liberazione stanza"
                    End If

                    Indice = Indice + 1
                Loop
                RsStanzeV.Close()

                For x = 0 To Indice - 2
                    For y = x + 1 To Indice - 1
                        If Format(VettoreData(x), "yyyyMMdd") > Format(VettoreData(y), "yyyyMMdd") Then
                            Dim AppoData As Date
                            Dim AppoStanza As String
                            Dim AppoTipoMov As String
                            Dim AppoTipoCS As String

                            AppoData = VettoreData(x)
                            AppoStanza = VettoreStanza(x)
                            AppoTipoMov = VettoreTipoMovimento(x)
                            AppoTipoCS = VettoreCentroServizio(x)

                            VettoreData(x) = VettoreData(y)
                            VettoreStanza(x) = VettoreStanza(y)
                            VettoreTipoMovimento(x) = VettoreTipoMovimento(y)
                            VettoreCentroServizio(x) = VettoreCentroServizio(y)

                            VettoreData(y) = AppoData
                            VettoreStanza(y) = AppoStanza
                            VettoreTipoMovimento(y) = AppoTipoMov
                            VettoreCentroServizio(x) = AppoTipoCS
                        End If
                    Next
                Next

                If Indice > 0 Then
                    For o = 0 To Indice - 1
                        If MovimentoInserito = 0 Then


                            Dim myriga As System.Data.DataRow = MyTable.NewRow()

                            myriga(0) = MyRs.Item("CodiceOspite")
                            myriga(1) = MyRs.Item("Nome")
                            myriga(2) = VettoreStanza(o)
                            myriga(3) = Format(VettoreData(o), "dd/MM/yyyy")
                            myriga(4) = VettoreTipoMovimento(o)
                            myriga(5) = VettoreCentroServizio(o)

                            MyTable.Rows.Add(myriga)

                            MovimentoInserito = 1

                        Else
                            Dim myriga As System.Data.DataRow = MyTable.NewRow()

                            myriga(0) = MyRs.Item("CodiceOspite")
                            myriga(1) = MyRs.Item("Nome")
                            myriga(2) = VettoreStanza(o)
                            myriga(3) = Format(VettoreData(o), "dd/MM/yyyy")
                            myriga(4) = VettoreTipoMovimento(o)
                            myriga(5) = VettoreCentroServizio(o)

                            MyTable.Rows.Add(myriga)

                        End If
                    Next
                End If
            End If
        Loop
        MyRs.Close()

        Dim myriga1 As System.Data.DataRow = MyTable.NewRow()
        MyTable.Rows.Add(myriga1)

        GridView1.AutoGenerateColumns = True
        GridView1.DataSource = MyTable
        GridView1.Font.Size = 10
        GridView1.DataBind()

    End Sub


    Protected Sub ImageButton3_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles ImageButton3.Click
        Call Estrai()
    End Sub

    Function campodb(ByVal oggetto As Object) As String
        If IsDBNull(oggetto) Then
            Return ""
        Else
            Return oggetto
        End If
    End Function

    Protected Sub OspitiWeb_ProspettoPresenzeAssenzeStanzaMovimenti_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load

        Call EseguiJS()
        If Page.IsPostBack = True Then Exit Sub

        Dim K1 As New Cls_SqlString

        Dim Barra As New Cls_BarraSenior

        If Not IsNothing(Session("RicercaAnagraficaSQLString")) Then
            K1 = Session("RicercaAnagraficaSQLString")
        End If

        Lbl_BarraSenior.Text = Barra.CodiceBarra(Application("SENIOR"), Session("UTENTE"), Page, K1)
    End Sub

    Private Sub AssegnaStanze(ByVal Token As String, ByVal VSession As VariabiliSesssione, ByVal DataDal As Date)

        Dim cn As OleDbConnection
        Dim cnOspitiAccessori As OleDbConnection


        cnOspitiAccessori = New Data.OleDb.OleDbConnection(Session("DC_OSPITIACCESSORI"))

        cnOspitiAccessori.Open()

        Dim UrlConnessione As String
        Dim rawresp As String

        UrlConnessione = "https://api.e-personam.com/api/v1.0/business_units/" & VSession.UTENTE.Replace("senioruser_", "") & "/guests?per_page=500"




        Dim client As HttpWebRequest = WebRequest.Create(UrlConnessione)

        client.Method = "GET"
        client.Headers.Add("Authorization", "Bearer " & Token)
        client.ContentType = "Content-Type: application/json"

        Dim reader As StreamReader
        Dim response As HttpWebResponse = Nothing
        client.KeepAlive = True
        client.ReadWriteTimeout = 62000
        client.MaximumResponseHeadersLength = 262144
        client.Timeout = 62000

        response = DirectCast(client.GetResponse(), HttpWebResponse)

        reader = New StreamReader(response.GetResponseStream())



        rawresp = reader.ReadToEnd()
        Dim jResults As JArray = JArray.Parse(rawresp)

        For Each jTok As JToken In jResults
            Dim M As New ClsOspite
            M.CodiceOspite = 0
            M.CODICEFISCALE = jTok.Item("cf").ToString()
            M.LeggiPerCodiceFiscale(Session("DC_OSPITE"), M.CODICEFISCALE)

            If M.CodiceOspite > 0 Then
                Dim tipo As String = ""

                Try
                    tipo = jTok.Item("current_status").Item("room").ToString
                Catch ex As Exception

                End Try

                Dim Mov As New Cls_Movimenti

                Mov.CodiceOspite = M.CodiceOspite
                Mov.UltimaDataAccoglimentoSenzaCserv(Session("DC_OSPITE"))


                Dim cmdStanze As New OleDbCommand()
                Dim Stanza As String = ""

                cmdStanze.CommandText = "Select * From Movimenti Where Data <= ? And  CodiceOspite = " & M.CodiceOspite & " Order By Data Desc"
                cmdStanze.Parameters.AddWithValue("@Data", DataDal)

                cmdStanze.Connection = cnOspitiAccessori
                Dim RsStanze As OleDbDataReader = cmdStanze.ExecuteReader()
                If RsStanze.Read Then

                    Stanza = RsStanze.Item("Letto")
                End If
                RsStanze.Close()



                If Stanza = "" Then
                    Dim MovimentoStanza As New Cls_MovimentiStanze


                    MovimentoStanza.CentroServizio = Mov.CENTROSERVIZIO
                    MovimentoStanza.CodiceOspite = M.CodiceOspite
                    MovimentoStanza.Data = Mov.Data
                    MovimentoStanza.Villa = "01"
                    MovimentoStanza.Reparto = "0" & Int(Val(tipo) / 100)
                    MovimentoStanza.Stanza = ""
                    MovimentoStanza.Piano = "0"
                    MovimentoStanza.Letto = tipo
                    MovimentoStanza.Tipologia = "OC"
                    MovimentoStanza.AggiornaDB(Session("DC_OSPITIACCESSORI"))
                End If

            End If
        Next

        cnOspitiAccessori.Close()
    End Sub

    Private Function RichiestaGuests(ByVal Token As String, ByVal IdEpersonam As String) As String

        
        Try

            Dim UrlConnessione As String



            'curl -k -H "Authorization: Bearer $TOKEN" -H "Content-Type: application/json" https://api.e-personam.com/api/v1.0/business_units/1771/guests?from=2018-02-04T00:00:00+00:00

            'curl -k -H "Authorization: Bearer $TOKEN" -H "Content-Type: application/json" https://api.e-personam.com/api/v1.0/business_units/3302/guests/updated/2018-04-10T00:00:00+00:00

         
            UrlConnessione = "https://api.e-personam.com/api/v1.0/guests/" & IdEpersonam

            Dim client As HttpWebRequest = WebRequest.Create(UrlConnessione)

            client.Method = "GET"
            client.Headers.Add("Authorization", "Bearer " & Token)
            client.ContentType = "Content-Type: application/json"

            Dim reader As StreamReader
            Dim response As HttpWebResponse = Nothing

            response = DirectCast(client.GetResponse(), HttpWebResponse)

            reader = New StreamReader(response.GetResponseStream())


            Dim rawresp As String
            rawresp = reader.ReadToEnd()

            If rawresp.ToUpper.IndexOf("BNCCRL22P18F205O") > 0 Then
                rawresp = rawresp
            End If

            Return rawresp

        Catch ex As Exception
            Return "ERRORE :[" & ex.Message & "]"
        End Try

    End Function

    Private Sub EseguiJS()
        Dim MyJs As String
        MyJs = "$(document).ready(function() {"

        MyJs = MyJs & "var els = document.getElementsByTagName(""*"");"

        MyJs = MyJs & "for (var i=0;i<els.length;i++)"
        MyJs = MyJs & "if ( els[i].id ) { "
        MyJs = MyJs & " var appoggio =els[i].id; "
        MyJs = MyJs & " if (appoggio.match('Txt_Data')!= null) {  "
        MyJs = MyJs & " $(els[i]).mask(""99/99/9999"");"
        MyJs = MyJs & " $(els[i]).datepicker({ changeMonth: true,changeYear: true,  yearRange: ""1990:" & Year(Now) + 5 & """},$.datepicker.regional[""it""]);"

        MyJs = MyJs & "    }"
        MyJs = MyJs & "} "
        MyJs = MyJs & "if (window.innerHeight>0) { $(""#BarraLaterale"").css(""height"",(window.innerHeight - 94) + ""px""); } else"
        MyJs = MyJs & "{ $(""#BarraLaterale"").css(""height"",(document.documentElement.offsetHeight - 94) + ""px"");  }"
        MyJs = MyJs & "});"

        'MyJs = "$(document).ready(function() { $('.myClass').keypress(function() { handleEnter($('.myClass').val(), true, true); } );     $('#" & Txt_ImportoPagato.ClientID & "').blur(function() { var ap = formatNumber ($('#" & Txt_ImportoPagato.ClientID & "').val(),2); $('#" & Txt_ImportoPagato.ClientID & "').val(ap); }); });"
        ScriptManager.RegisterStartupScript(Me, Me.GetType(), "visualizzaRitIm", MyJs, True)
    End Sub

    Protected Sub ImageButton4_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles ImageButton4.Click
        Call Estrai()
        If MyTable.Rows.Count > 1 Then
            Response.Clear()
            Response.AddHeader("content-disposition", "attachment;filename=AddebitiAccrediti.xls")
            Response.Charset = String.Empty
            Response.Cache.SetCacheability(HttpCacheability.NoCache)
            Response.ContentType = "application/vnd.xls"
            Dim stringWrite As New System.IO.StringWriter
            Dim htmlWrite As New HtmlTextWriter(stringWrite)

            form1.Controls.Clear()
            form1.Controls.Add(GridView1)

            form1.RenderControl(htmlWrite)

            Response.Write(stringWrite.ToString())
            Response.End()
        End If
    End Sub

    Protected Sub Btn_Esci_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles Btn_Esci.Click
        Response.Redirect("Menu_Visualizzazioni.aspx")
    End Sub

    Private Function LoginPersonam(ByVal data As VariabiliSesssione) As String
        Try


            Dim request As New NameValueCollection


            request.Add("client_id", "98217ca6670c660e22f42d58e231d4e56c84fb34e216b0f66f1f30284fd3ec9d")
            request.Add("client_secret", "5570a684f69ca74d75d16bba669c156ec092eccb4111d0974adec3edb2fa4d6f")


            request.Add("username", data.UTENTE)



            request.Add("password", data.PASSWORD)
            System.Net.ServicePointManager.ServerCertificateValidationCallback = AddressOf CertificateHandler


            Dim client As New WebClient()

            Dim result As Object = client.UploadValues("https://api.e-personam.com/api/v1.0/token/password", "POST", request)


            Dim stringa As String = Encoding.Default.GetString(result)


            Dim serializer As JavaScriptSerializer


            serializer = New JavaScriptSerializer()




            Dim s As Autentificazione = serializer.Deserialize(Of Autentificazione)(stringa)


            Return s.access_token
        Catch ex As Exception
            Return ""
        End Try
    End Function


    Public Class Autentificazione
        Public access_token As String
        Public expires_in As String
        Public scope As String
        Public token_type As String
        Public refresh_token As String
    End Class
    Public Class VariabiliSesssione
        Public DC_OSPITE As String
        Public DC_OSPITIACCESSORI As String
        Public DC_TABELLE As String
        Public DC_GENERALE As String
        Public UTENTE As String
        Public PASSWORD As String
        Public RICERCAOSPITI As String
    End Class


    Private Shared Function CertificateHandler(ByVal sender As Object, ByVal certificate As X509Certificate, ByVal chain As X509Chain, ByVal SSLerror As SslPolicyErrors) As Boolean
        Return True
    End Function
End Class
