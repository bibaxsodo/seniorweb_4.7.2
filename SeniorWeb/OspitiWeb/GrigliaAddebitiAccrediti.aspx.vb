﻿Imports System.Web.Hosting
Imports System.Data.OleDb
Imports ThoughtWorks.QRCode.Codec
Imports ThoughtWorks.QRCode.Codec.Data
Imports ThoughtWorks.QRCode.Codec.Util

Partial Class OspitiWeb_GrigliaAddebitiAccrediti
    Inherits System.Web.UI.Page
    Dim MyTable As New System.Data.DataTable("tabella")
    Dim MyDataSet As New System.Data.DataSet()

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Trim(Session("UTENTE")) = "" Then
            Response.Redirect("../Login.aspx")
            Exit Sub
        End If
        If Session("ABILITAZIONI").IndexOf("<PORTINERIA>") > 0 Then
            Response.Redirect("..\MainPortineria.aspx")
            Exit Sub
        End If
        If Session("ABILITAZIONI").IndexOf("<ADDEBITI>") < 1 Then
            Response.Redirect("Menu_Ospiti.aspx")
            Exit Sub
        End If

        If Page.IsPostBack = True Then Exit Sub

        If Val(Request.Item("CodiceOspite")) > 0 And Request.Item("CentroServizio") <> "" Then

            Session("CODICESERVIZIO") = Request.Item("CentroServizio")
            Session("CODICEOSPITE") = Val(Request.Item("CodiceOspite"))
        End If


        ViewState("CODICESERVIZIO") = Session("CODICESERVIZIO")
        ViewState("CODICEOSPITE") = Session("CODICEOSPITE")




        Dim K1 As New Cls_SqlString

        Dim Barra As New Cls_BarraSenior

        If Not IsNothing(Session("RicercaAnagraficaSQLString")) Then
            K1 = Session("RicercaAnagraficaSQLString")
        End If

        Lbl_BarraSenior.Text = Barra.CodiceBarra(Application("SENIOR"), Session("UTENTE"), Page, K1)

        Dim ConnectionString As String = Session("DC_OSPITE")
        Dim z As New Cls_AddebitiAccrediti



        Dim DataDal As Date = DateSerial(Year(Now), 1, 1)

        If Month(DataDal) <= 1 Then
            DataDal = DateSerial(Year(Now) - 1, 11, 1)
        End If
        Dim DataAl As Date = DateSerial(Year(Now), 12, 31)

        Txt_DataDal.Text = Format(DataDal, "dd/MM/yyyy")
        Txt_DataAl.Text = Format(DataAl, "dd/MM/yyyy")

        z.loaddati(ConnectionString, Session("CODICEOSPITE"), Session("CODICESERVIZIO"), MyTable, DataDal, DataAl, "")
        ViewState("Appoggio") = MyTable

        Grd_AddebitiAccrediti.AutoGenerateColumns = False
        Grd_AddebitiAccrediti.DataSource = MyTable
        Grd_AddebitiAccrediti.DataBind()



        Dim x As New ClsOspite

        x.CodiceOspite = Session("CODICEOSPITE")
        x.Leggi(Session("DC_OSPITE"), x.CodiceOspite)

        Dim cs As New Cls_CentroServizio

        cs.Leggi(Session("DC_OSPITE"), Session("CODICESERVIZIO"))

        Lbl_NomeOspite.Text = x.Nome & " -  " & x.DataNascita & " - " & cs.DESCRIZIONE & "<i style=""font-size:small;"">(" & x.CodiceOspite & ")</i>"

        Call EseguiJS()


        Call ColoreGriglia()



        
    End Sub

    Protected Sub Grd_AddebitiAccrediti_PageIndexChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewPageEventArgs) Handles Grd_AddebitiAccrediti.PageIndexChanging
        MyTable = ViewState("Appoggio")
        Grd_AddebitiAccrediti.PageIndex = e.NewPageIndex
        Grd_AddebitiAccrediti.DataSource = MyTable
        Grd_AddebitiAccrediti.DataBind()

        Call ColoreGriglia()
    End Sub



    Private Sub GenerateQrCode(ByVal Testo As String)
        Dim barcode As New QRCodeEncoder()

        Dim u As Integer



        Dim NomeSocieta As String

        Dim k2 As New Cls_Login

        k2.Utente = Session("UTENTE")
        k2.LeggiSP(Application("SENIOR"))

        NomeSocieta = k2.RagioneSociale



        Dim Appo As String

        Appo = Dir(HostingEnvironment.ApplicationPhysicalPath() & "\Public\" & NomeSocieta)
        If Dir(HostingEnvironment.ApplicationPhysicalPath() & "\Public\" & NomeSocieta, FileAttribute.Directory) = "" Then
            MkDir(HostingEnvironment.ApplicationPhysicalPath() & "\Public\" & NomeSocieta)
        End If


        If Dir(HostingEnvironment.ApplicationPhysicalPath() & "\Public\" & NomeSocieta & "\Ospite_" & Val(Session("CODICEOSPITE")), FileAttribute.Directory) = "" Then
            MkDir(HostingEnvironment.ApplicationPhysicalPath() & "\Public\" & NomeSocieta & "\Ospite_" & Val(Session("CODICEOSPITE")))
        End If

        If Dir(HostingEnvironment.ApplicationPhysicalPath() & "\Public\" & NomeSocieta & "\Ospite_" & Val(Session("CODICEOSPITE")) & "\" & Testo, FileAttribute.Directory) = "" Then
            MkDir(HostingEnvironment.ApplicationPhysicalPath() & "\Public\" & NomeSocieta & "\Ospite_" & Val(Session("CODICEOSPITE")) & "\" & Testo)
        End If

        Dim Ospite As New ClsOspite

        Ospite.CodiceOspite = Val(Session("CODICEOSPITE"))
        Ospite.Leggi(Session("DC_OSPITE"), Ospite.CodiceOspite)


        Dim Token As String = ""
        Dim i As Integer

        Token = Token & Session("CLIENTE")
        For i = 0 To 24
            Token = Token & Chr(Int(Rnd(1) * 20) + 65)
        Next


        Dim cnSenior As OleDbConnection

        cnSenior = New System.Data.OleDb.OleDbConnection(Application("SENIOR"))


        cnSenior.Open()
        Dim cmd As New OleDbCommand()
        cmd.CommandText = "Delete From Token where DataScadenza < ?"
        cmd.Parameters.AddWithValue("@DataScadenza", Now)
        cmd.Connection = cnSenior
        cmd.ExecuteNonQuery()




        Dim cmdInserimento As New OleDbCommand()
        cmdInserimento.CommandText = "INSERT  INTO Token (Token,Value1, Value2, DataScadenza) VALUES (?,?,?,?)"
        cmdInserimento.Parameters.AddWithValue("@Token", Token)
        cmdInserimento.Parameters.AddWithValue("@Value1", NomeSocieta & "\Ospite_" & Val(Session("CODICEOSPITE")) & "\" & Testo & "\1.jpg")
        cmdInserimento.Parameters.AddWithValue("@Value2", Ospite.Nome)
        cmdInserimento.Parameters.AddWithValue("@DataScadenza", Now.AddMinutes(10))
        cmdInserimento.Connection = cnSenior
        cmdInserimento.ExecuteNonQuery()


        cnSenior.Close()


        Dim ks As New Image
        Dim originalimg As System.Drawing.Image

        If Request.Url.ToString.ToUpper.IndexOf("seniorweb.".ToUpper) >= 0 Then
            originalimg = barcode.Encode("https://seniorweb.e-personam.com/seniorweb/qrcodeupload.aspx?DEST=" & Token)
        End If


        If Request.Url.ToString.ToUpper.IndexOf("senior.e-personam".ToUpper) >= 0 Then
            originalimg = barcode.Encode("https://senior.e-personam.com/seniorweb/qrcodeupload.aspx?DEST=" & Token)
        End If

        If Request.Url.ToString.ToUpper.IndexOf("senior.medservices".ToUpper) >= 0 Then
            originalimg = barcode.Encode("https://senior.medservices.cloud:3450/seniorweb/qrcodeupload.aspx?DEST=" & Token)
        End If


        originalimg.Save(HostingEnvironment.ApplicationPhysicalPath() & "\Public\" & NomeSocieta & ".jpg")
        ImgQrCode.ImageUrl = "..\Public\" & NomeSocieta & ".jpg"


    End Sub


    Protected Sub Grd_AddebitiAccrediti_RowCommand(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewCommandEventArgs) Handles Grd_AddebitiAccrediti.RowCommand
        If e.CommandName = "QrCode" Then

            Dim d As Integer



            MyTable = ViewState("Appoggio")

            d = Val(e.CommandArgument)

            Dim k As Long

            k = MyTable.Rows(d).Item(0).ToString

            If Val(Request.Item("CodiceOspite")) > 0 And Request.Item("CentroServizio") <> "" Then

                Session("CODICESERVIZIO") = Request.Item("CentroServizio")
                Session("CODICEOSPITE") = Val(Request.Item("CodiceOspite"))
            End If


            GenerateQrCode("AdAC_" & k)
            ScriptManager.RegisterStartupScript(Me, Me.GetType(), "visualizzaBoxQR", "VisualizzaDivRegistrazione();", True)

            'VisualizzaDivRegistrazione()
        End If

        If e.CommandName = "Seleziona" Then

            Session("CODICESERVIZIO") = ViewState("CODICESERVIZIO")
            Session("CODICEOSPITE") = ViewState("CODICEOSPITE")
            If Val(Session("CODICEOSPITE")) = 0 Then
                ClientScript.RegisterClientScriptBlock(Me.GetType(), "Errore", "VisualizzaErrore('Devi ricercare l ospite');", True)
                Exit Sub
            End If

            Dim d As Integer



            MyTable = ViewState("Appoggio")

            d = Val(e.CommandArgument)

            Dim k As Long

            k = MyTable.Rows(d).Item(0).ToString

            If Val(Request.Item("CodiceOspite")) > 0 And Request.Item("CentroServizio") <> "" Then

                Session("CODICESERVIZIO") = Request.Item("CentroServizio")
                Session("CODICEOSPITE") = Val(Request.Item("CodiceOspite"))
            End If


            Response.Redirect("AddebitoAccredito.aspx?ID=" & k & "&CentroServizio=" & Session("CODICESERVIZIO") & "&CodiceOspite=" & Session("CODICEOSPITE"))
        End If
    End Sub

    Protected Sub Grd_AddebitiAccrediti_RowDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs) Handles Grd_AddebitiAccrediti.RowDataBound
        If e.Row.RowType = DataControlRowType.DataRow Then



        End If
    End Sub

    Protected Sub Grd_AddebitiAccrediti_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles Grd_AddebitiAccrediti.SelectedIndexChanged
        


    End Sub

    Protected Sub Btn_Nuovo_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles Btn_Nuovo.Click

        Session("CODICESERVIZIO") = ViewState("CODICESERVIZIO")
        Session("CODICEOSPITE") = ViewState("CODICEOSPITE")
        If Val(Session("CODICEOSPITE")) = 0 Then
            ClientScript.RegisterClientScriptBlock(Me.GetType(), "Errore", "VisualizzaErrore('Devi ricercare l ospite');", True)
            Exit Sub
        End If

        Response.Redirect("AddebitoAccredito.aspx?ID=0&CentroServizio=" & Session("CODICESERVIZIO") & "&CodiceOspite=" & Session("CODICEOSPITE"))
    End Sub

    Protected Sub Btn_Esci_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles Btn_Esci.Click


        If Val(Request.Item("CodiceOspite")) > 0 And Request.Item("CentroServizio") <> "" Then

            Session("CODICESERVIZIO") = Request.Item("CentroServizio")
            Session("CODICEOSPITE") = Val(Request.Item("CodiceOspite"))
        End If

        If Request.Item("PAGINA") = "" Then
            Response.Redirect("RicercaAnagrafica.aspx?TIPO=ADDACR" & "&CentroServizio=" & Session("CODICESERVIZIO") & "&CodiceOspite=" & Session("CODICEOSPITE"))
        Else
            Response.Redirect("RicercaAnagrafica.aspx?TIPO=ADDACR&CHIAMATA=RITORNO&PAGINA=" & Request.Item("PAGINA") & "&CentroServizio=" & Session("CODICESERVIZIO") & "&CodiceOspite=" & Session("CODICEOSPITE"))
        End If
    End Sub

    Private Sub ColoreGriglia()

        Dim k As Integer
        Dim i As Integer
        Dim CodiceMenu As String = ""

        For i = 0 To Grd_AddebitiAccrediti.Rows.Count - 1
            '<div id="menutastodestro">  

            Dim IdAddebito As Long

            IdAddebito = Grd_AddebitiAccrediti.Rows(i).Cells(1).Text

            If Grd_AddebitiAccrediti.Rows(i).Cells(11).Text = "0" Then

                Grd_AddebitiAccrediti.Rows(i).Cells(1).Text = "<div id=""menutastodestro" & IdAddebito & """><p title=""Tasto destro per creare il documento"">" & IdAddebito & "</p></div><ul class=""custom-menu"" id=""rightmenu" & IdAddebito & """><a href=""CreaDocumentoDaAddebito.aspx?ID=" & IdAddebito & """ id=""A1"" ><li data-action=""first"">CREA DOCUMENTO</li></a></ul>"


                CodiceMenu = CodiceMenu & "$(document).ready(function() {  " & vbNewLine
                CodiceMenu = CodiceMenu & "$(""#menutastodestro" & IdAddebito & """).bind(""contextmenu"", function(event) {  " & vbNewLine
                CodiceMenu = CodiceMenu & "event.preventDefault();" & vbNewLine
                CodiceMenu = CodiceMenu & "$("".custom-menu"").css(""display"",""none"");"
                CodiceMenu = CodiceMenu & "$(""#rightmenu" & IdAddebito & """).toggle(100).css({" & vbNewLine
                CodiceMenu = CodiceMenu & "top: event.pageY + ""px""," & vbNewLine
                CodiceMenu = CodiceMenu & "left: event.pageX + ""px""" & vbNewLine
                CodiceMenu = CodiceMenu & "}); " & vbNewLine
                CodiceMenu = CodiceMenu & "});" & vbNewLine
                CodiceMenu = CodiceMenu & "});" & vbNewLine

            Else
                Grd_AddebitiAccrediti.Rows(i).Cells(1).Text = IdAddebito
            End If


            If Grd_AddebitiAccrediti.Rows(i).Cells(3).Text = "Accredito" Then
                For k = 0 To 11
                    Grd_AddebitiAccrediti.Rows(i).Cells(k).ForeColor = Drawing.Color.Green
                Next
            End If
            If Grd_AddebitiAccrediti.Rows(i).Cells(3).Text = "Addebito" Then

                For k = 0 To 11
                    Grd_AddebitiAccrediti.Rows(i).Cells(k).ForeColor = Drawing.Color.Red
                Next k
            End If

            Dim kAC As New Cls_AddebitiAccrediti

            kAC.CENTROSERVIZIO = Session("CODICESERVIZIO")
            kAC.CodiceOspite = Session("CODICEOSPITE")
            kAC.ID = Val(Grd_AddebitiAccrediti.Rows(i).Cells(1).Text)
            kAC.chiaveselezione = ""
            kAC.leggi(Session("DC_OSPITE"), kAC.CodiceOspite, kAC.CENTROSERVIZIO, kAC.ID)
            If kAC.chiaveselezione <> "" Then
                For k = 0 To 11
                    Grd_AddebitiAccrediti.Rows(i).Cells(k).BackColor = Drawing.Color.Yellow
                Next k
            End If


        Next


        CodiceMenu = CodiceMenu & "$(document).ready(function() {  " & vbNewLine
        CodiceMenu = CodiceMenu & " $(""body"").click(function(){ $("".custom-menu"").css(""display"",""none""); });" & vbNewLine
        CodiceMenu = CodiceMenu & "});" & vbNewLine

        ScriptManager.RegisterStartupScript(Me, Me.GetType(), "CodiceMenu", CodiceMenu, True)

        Dim Param As New Cls_Parametri

        Param.LeggiParametri(Session("DC_OSPITE"))

        If Param.AbilitaQrCodeAddAcc = 0 Then

            Grd_AddebitiAccrediti.Columns(12).Visible = False
        End If
    End Sub

    Protected Sub ImageButton1_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles ImageButton1.Click
        Dim ConnectionString As String = Session("DC_OSPITE")
        Dim z As New Cls_AddebitiAccrediti



        Session("CODICESERVIZIO") = ViewState("CODICESERVIZIO")
        Session("CODICEOSPITE") = ViewState("CODICEOSPITE")

        If IsDate(Txt_DataDal.Text) And IsDate(Txt_DataAl.Text) Then
            Dim DataDal As Date = Txt_DataDal.Text
            Dim DataAl As Date = Txt_DataAl.Text
            Dim Tipo As String = ""

            If RB_Ospite.Checked = True Then
                Tipo = "O"
            End If
            If RB_Perente.Checked = True Then
                Tipo = "P"
            End If
            If RB_Comune.Checked = True Then
                Tipo = "C"
            End If
            If RB_Jolly.Checked = True Then
                Tipo = "J"
            End If
            If RB_Regione.Checked = True Then
                Tipo = "R"
            End If

            z.loaddati(ConnectionString, Session("CODICEOSPITE"), Session("CODICESERVIZIO"), MyTable, DataDal, DataAl, Tipo)
        Else
            z.loaddati(ConnectionString, Session("CODICEOSPITE"), Session("CODICESERVIZIO"), MyTable)
        End If
        ViewState("Appoggio") = MyTable

        REM Grd_AddebitiAccrediti.AutoGenerateColumns = True
        Grd_AddebitiAccrediti.DataSource = MyTable
        Grd_AddebitiAccrediti.DataBind()



        Dim x As New ClsOspite

        x.CodiceOspite = Session("CODICEOSPITE")
        x.Leggi(Session("DC_OSPITE"), x.CodiceOspite)

        Dim cs As New Cls_CentroServizio

        cs.Leggi(Session("DC_OSPITE"), Session("CODICESERVIZIO"))

        Lbl_NomeOspite.Text = x.Nome & " -  " & x.DataNascita & " - " & cs.DESCRIZIONE & "<i style=""font-size:small;"">(" & x.CodiceOspite & ")</i>"

        Call ColoreGriglia()
    End Sub




    Private Sub EseguiJS()
        Dim MyJs As String


        MyJs = "$(document).ready(function() {"
        MyJs = MyJs & "var els = document.getElementsByTagName(""*"");"        
        MyJs = MyJs & "for (var i=0;i<els.length;i++)"
        MyJs = MyJs & "if ( els[i].id ) { "
        MyJs = MyJs & " var appoggio =els[i].id; "
        MyJs = MyJs & " if ((appoggio.match('Txt_Data')!= null) || (appoggio.match('Txt_Data')!= null)) {  "
        MyJs = MyJs & " $(els[i]).mask(""99/99/9999"");"
        MyJs = MyJs & " $(els[i]).datepicker({ changeMonth: true,changeYear: true,  yearRange: ""1990:" & Year(Now) + 5 & """},$.datepicker.regional[""it""]);"
        MyJs = MyJs & "    }"

        MyJs = MyJs & "} "
        MyJs = MyJs & "});"


        'MyJs = "$(document).ready(function() { $('.myClass').keypress(function() { handleEnter($('.myClass').val(), true, true); } );     $('#" & Txt_ImportoPagato.ClientID & "').blur(function() { var ap = formatNumber ($('#" & Txt_ImportoPagato.ClientID & "').val(),2); $('#" & Txt_ImportoPagato.ClientID & "').val(ap); }); });"
        ScriptManager.RegisterStartupScript(Me, Me.GetType(), "visualizzaRitIm", MyJs, True)
    End Sub

    Protected Sub Btn_Excel_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles Btn_Excel.Click


        Session("CODICESERVIZIO") = ViewState("CODICESERVIZIO")
        Session("CODICEOSPITE") = ViewState("CODICEOSPITE")

        Dim ConnectionString As String = Session("DC_OSPITE")
        Dim z As New Cls_AddebitiAccrediti

        If IsDate(Txt_DataDal.Text) And IsDate(Txt_DataAl.Text) Then
            Dim DataDal As Date = Txt_DataDal.Text
            Dim DataAl As Date = Txt_DataAl.Text
            Dim Tipo As String = ""

            If RB_Ospite.Checked = True Then
                Tipo = "O"
            End If
            If RB_Perente.Checked = True Then
                Tipo = "P"
            End If
            If RB_Comune.Checked = True Then
                Tipo = "C"
            End If
            If RB_Regione.Checked = True Then
                Tipo = "R"
            End If

            z.loaddati(ConnectionString, Session("CODICEOSPITE"), Session("CODICESERVIZIO"), MyTable, DataDal, DataAl, Tipo)
        Else
            z.loaddati(ConnectionString, Session("CODICEOSPITE"), Session("CODICESERVIZIO"), MyTable)
        End If

        Session("GrigliaSoloStampa") = MyTable
        ScriptManager.RegisterClientScriptBlock(Me, Me.GetType(), "Stampa", "window.open('ExportExcel.aspx','Excel','width=800,height=600');", True)

    End Sub
End Class

