﻿Imports Microsoft.VisualBasic
Imports System.Data.OleDb
Imports System.Web.Hosting

Partial Class OspitiWeb_ModificaCentroServizio
    Inherits System.Web.UI.Page


    Private Sub AggiornaCServ()
        Dim kCsrv As New Cls_CentroServizio

        If DD_Struttura.SelectedValue = "" Then
            kCsrv.UpDateDropBox(Session("DC_OSPITE"), Cmb_CServ)
        Else
            kCsrv.UpDateDropBoxStruttura(Session("DC_OSPITE"), Cmb_CServ, DD_Struttura.SelectedValue)
        End If

    End Sub



    Protected Sub DD_Struttura_TextChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles DD_Struttura.TextChanged
        AggiornaCServ()       
    End Sub

    Protected Sub OspitiWeb_ModificaCentroServizio_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Trim(Session("UTENTE")) = "" Then
            Response.Redirect("../Login.aspx")
            Exit Sub
        End If
        If Page.IsPostBack = True Then
            Exit Sub
        End If



        Dim x As New ClsOspite

        x.CodiceOspite = Session("CODICEOSPITE")
        x.Leggi(Session("DC_OSPITE"), x.CodiceOspite)

        Dim cs As New Cls_CentroServizio

        cs.Leggi(Session("DC_OSPITE"), Session("CODICESERVIZIO"))


        Lbl_NomeOspite.Text = x.Nome & " -  " & x.DataNascita & " - " & cs.DESCRIZIONE & "<i style=""font-size:small;"">(" & x.CodiceOspite & ")</i>"

        Dim K1 As New Cls_SqlString

        Dim Barra As New Cls_BarraSenior

        If Not IsNothing(Session("RicercaAnagraficaSQLString")) Then
            K1 = Session("RicercaAnagraficaSQLString")
        End If

        Lbl_BarraSenior.Text = Barra.CodiceBarra(Application("SENIOR"), Session("UTENTE"), Page, K1)

        Dim ConnectionString As String = Session("DC_OSPITE")
        Dim kVilla As New Cls_TabelleDescrittiveOspitiAccessori


        kVilla.UpDateDropBox(Session("DC_OSPITIACCESSORI"), "VIL", DD_Struttura)
        If DD_Struttura.Items.Count = 1 Then
            Call AggiornaCServ()
        End If


        
    End Sub

    Protected Sub Btn_Modifica_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles Btn_Modifica.Click
        Dim cnOspiti As OleDbConnection
        Dim cnGenerale As OleDbConnection

        Dim MyMastro As Long
        Dim MyConto As Long
        Dim MySottoConto As Long


        Dim xMyMastro As Long
        Dim xMyConto As Long
        Dim xMySottoConto As Long


        Dim Cserv As New Cls_CentroServizio

        Cserv.Leggi(Session("DC_OSPITE"), Session("CODICESERVIZIO"))

        MyMastro = Cserv.MASTRO
        MyConto = Cserv.CONTO
        MySottoConto = Int(Session("CODICEOSPITE") * 100)

        Dim CservNew As New Cls_CentroServizio


        CservNew.Leggi(Session("DC_OSPITE"), Cmb_CServ.SelectedValue)
        xMyMastro = CservNew.MASTRO
        xMyConto = CservNew.CONTO
        xMySottoConto = Int(Session("CODICEOSPITE") * 100)

        cnOspiti = New Data.OleDb.OleDbConnection(Session("DC_OSPITE"))

        cnOspiti.Open()

        cnGenerale = New Data.OleDb.OleDbConnection(Session("DC_GENERALE"))

        cnGenerale.Open()


        Dim Cmd As New OleDbCommand

        Dim Transan As OleDbTransaction = cnOspiti.BeginTransaction()


        Cmd.Connection = cnOspiti
        Cmd.Transaction = Transan
        Cmd.CommandText = "Update EXTRAOSPITE Set CENTROSERVIZIO = '" & Cmb_CServ.SelectedValue & "' Where CodiceOspite = " & Session("CODICEOSPITE") & " And CENTROSERVIZIO = '" & Session("CODICESERVIZIO") & "'"
        Cmd.ExecuteNonQuery()

        Cmd.CommandText = "Update IMPEGNATIVE Set CENTROSERVIZIO = '" & Cmb_CServ.SelectedValue & "' Where CodiceOspite = " & Session("CODICEOSPITE") & " And CENTROSERVIZIO = '" & Session("CODICESERVIZIO") & "'"
        Cmd.ExecuteNonQuery()
        Cmd.CommandText = "Update IMPORTOCOMUNE Set CENTROSERVIZIO = '" & Cmb_CServ.SelectedValue & "' Where CodiceOspite = " & Session("CODICEOSPITE") & " And CENTROSERVIZIO = '" & Session("CODICESERVIZIO") & "'"
        Cmd.ExecuteNonQuery()
        Cmd.CommandText = "Update IMPORTOJOLLY Set CENTROSERVIZIO = '" & Cmb_CServ.SelectedValue & "' Where CodiceOspite = " & Session("CODICEOSPITE") & " And CENTROSERVIZIO = '" & Session("CODICESERVIZIO") & "'"
        Cmd.ExecuteNonQuery()
        Cmd.CommandText = "Update IMPORTOOSPITE Set CENTROSERVIZIO = '" & Cmb_CServ.SelectedValue & "' Where CodiceOspite = " & Session("CODICEOSPITE") & " And CENTROSERVIZIO = '" & Session("CODICESERVIZIO") & "'"
        Cmd.ExecuteNonQuery()
        Cmd.CommandText = "Update IMPORTOPARENTI Set CENTROSERVIZIO = '" & Cmb_CServ.SelectedValue & "' Where CodiceOspite = " & Session("CODICEOSPITE") & " And CENTROSERVIZIO = '" & Session("CODICESERVIZIO") & "'"
        Cmd.ExecuteNonQuery()
        Cmd.CommandText = "Update IMPORTORETTA Set CENTROSERVIZIO = '" & Cmb_CServ.SelectedValue & "' Where CodiceOspite = " & Session("CODICEOSPITE") & " And CENTROSERVIZIO = '" & Session("CODICESERVIZIO") & "'"
        Cmd.ExecuteNonQuery()
        Cmd.CommandText = "Update MODALITA Set CENTROSERVIZIO = '" & Cmb_CServ.SelectedValue & "' Where CodiceOspite = " & Session("CODICEOSPITE") & " And CENTROSERVIZIO = '" & Session("CODICESERVIZIO") & "'"
        Cmd.ExecuteNonQuery()
        Cmd.CommandText = "Update MOVIMENTI Set CENTROSERVIZIO = '" & Cmb_CServ.SelectedValue & "' Where CodiceOspite = " & Session("CODICEOSPITE") & " And CENTROSERVIZIO = '" & Session("CODICESERVIZIO") & "'"
        Cmd.ExecuteNonQuery()
        Cmd.CommandText = "Update RETTECOMUNE Set CENTROSERVIZIO = '" & Cmb_CServ.SelectedValue & "' Where CodiceOspite = " & Session("CODICEOSPITE") & " And CENTROSERVIZIO = '" & Session("CODICESERVIZIO") & "'"
        Cmd.ExecuteNonQuery()
        Cmd.CommandText = "Update RETTEENTE Set CENTROSERVIZIO = '" & Cmb_CServ.SelectedValue & "' Where CodiceOspite = " & Session("CODICEOSPITE") & " And CENTROSERVIZIO = '" & Session("CODICESERVIZIO") & "'"
        Cmd.ExecuteNonQuery()
        Cmd.CommandText = "Update RETTEJOLLY Set CENTROSERVIZIO = '" & Cmb_CServ.SelectedValue & "' Where CodiceOspite = " & Session("CODICEOSPITE") & " And CENTROSERVIZIO = '" & Session("CODICESERVIZIO") & "'"
        Cmd.ExecuteNonQuery()
        Cmd.CommandText = "Update RETTEOSPITE Set CENTROSERVIZIO = '" & Cmb_CServ.SelectedValue & "' Where CodiceOspite = " & Session("CODICEOSPITE") & " And CENTROSERVIZIO = '" & Session("CODICESERVIZIO") & "'"
        Cmd.ExecuteNonQuery()
        Cmd.CommandText = "Update RETTEPARENTE Set CENTROSERVIZIO = '" & Cmb_CServ.SelectedValue & "' Where CodiceOspite = " & Session("CODICEOSPITE") & " And CENTROSERVIZIO = '" & Session("CODICESERVIZIO") & "'"
        Cmd.ExecuteNonQuery()
        Cmd.CommandText = "Update RETTEREGIONE Set CENTROSERVIZIO = '" & Cmb_CServ.SelectedValue & "' Where CodiceOspite = " & Session("CODICEOSPITE") & " And CENTROSERVIZIO = '" & Session("CODICESERVIZIO") & "'"
        Cmd.ExecuteNonQuery()
        Cmd.CommandText = "Update STATOAUTO Set CENTROSERVIZIO = '" & Cmb_CServ.SelectedValue & "' Where CodiceOspite = " & Session("CODICEOSPITE") & " And CENTROSERVIZIO = '" & Session("CODICESERVIZIO") & "'"
        Cmd.ExecuteNonQuery()
        Cmd.CommandText = "Update STATOAUTO Set CENTROSERVIZIO = '" & Cmb_CServ.SelectedValue & "' Where CodiceOspite = " & Session("CODICEOSPITE") & " And CENTROSERVIZIO = '" & Session("CODICESERVIZIO") & "'"
        Cmd.ExecuteNonQuery()

        Cmd.CommandText = "Update ADDACR Set CENTROSERVIZIO = '" & Cmb_CServ.SelectedValue & "' Where CodiceOspite = " & Session("CODICEOSPITE") & " And CENTROSERVIZIO = '" & Session("CODICESERVIZIO") & "'"
        Cmd.ExecuteNonQuery()

        Cmd.CommandText = "Update ADDACR Set CENTROSERVIZIO = '" & Cmb_CServ.SelectedValue & "' Where CodiceOspite = " & Session("CODICEOSPITE") & " And CENTROSERVIZIO = '" & Session("CODICESERVIZIO") & "'"
        Cmd.ExecuteNonQuery()

        Cmd.CommandText = "Update Listino Set CENTROSERVIZIO = '" & Cmb_CServ.SelectedValue & "' Where CodiceOspite = " & Session("CODICEOSPITE") & " And CENTROSERVIZIO = '" & Session("CODICESERVIZIO") & "'"
        Cmd.ExecuteNonQuery()


        Cmd.CommandText = "Update DatiOspiteParenteCentroServizio Set CENTROSERVIZIO = '" & Cmb_CServ.SelectedValue & "' Where CodiceOspite = " & Session("CODICEOSPITE") & " And CENTROSERVIZIO = '" & Session("CODICESERVIZIO") & "'"
        Cmd.ExecuteNonQuery()



        ' DOCUMENTI

        If Chk_MovimentiContabili.Checked = True Then


            Dim CmdGen As New OleDbCommand

            Dim TransanGen As OleDbTransaction = cnGenerale.BeginTransaction()


            CmdGen.Transaction = TransanGen


            CmdGen.Connection = cnGenerale
            CmdGen.CommandText = "UPDATE MovimentiContabiliTesta " & _
                            " SET MovimentiContabiliTesta.CentroServizio =  '" & Cmb_CServ.SelectedValue & "'" & _
                            " WHERE MovimentiContabiliTesta.CentroServizio =  '" & Session("CODICESERVIZIO") & "' " & _
                            " And (select count(MovimentiContabiliRiga.Numero) From MovimentiContabiliRiga where MovimentiContabiliRiga.Mastropartita = " & MyMastro & _
                            " AND MovimentiContabiliRiga.Contopartita =  " & MyConto & _
                            " AND MovimentiContabiliRiga.Sottocontopartita = " & MySottoConto & _
                            " AND MovimentiContabiliRiga.Numero  = MovimentiContabiliTesta.NumeroRegistrazione) > 0  "
            CmdGen.ExecuteNonQuery()





            CmdGen.CommandText = "UPDATE MovimentiContabiliRiga  " & _
                              " SET MovimentiContabiliRiga.Mastropartita = " & xMyMastro & _
                              " , MovimentiContabiliRiga.Contopartita =  " & xMyConto & _
                              " , MovimentiContabiliRiga.Sottocontopartita = " & xMySottoConto & _
                              " WHERE MovimentiContabiliRiga.Mastropartita = " & MyMastro & _
                              " AND MovimentiContabiliRiga.Contopartita =  " & MyConto & _
                              " AND MovimentiContabiliRiga.Sottocontopartita = " & MySottoConto
            CmdGen.ExecuteNonQuery()

            TransanGen.Commit()
        End If

        Transan.Commit()
        cnOspiti.Close()
        cnGenerale.Close()

        Dim Osp As New ClsOspite

        Osp.CodiceOspite = Session("CODICEOSPITE")
        Osp.Leggi(Session("DC_OSPITE"), Osp.CodiceOspite)

        
        Dim M As New Cls_Pianodeiconti

        M.Mastro = Cserv.MASTRO
        M.Conto = Cserv.CONTO
        M.Sottoconto = Int(Session("CODICEOSPITE") * 100)
        M.Descrizione = Osp.Nome
        M.TipoAnagrafica = "O"
        M.Scrivi(Session("DC_GENERALE"))

        Dim i As Integer
        For i = 1 To 10
            Dim Par As New Cls_Parenti

            Par.Nome = ""
            Par.TIPOOPERAZIONE = ""
            Par.CodiceOspite = Session("CODICEOSPITE")
            Par.CodiceOspite = i
            Par.Leggi(Session("DC_OSPITE"), Par.CodiceOspite, Par.CodiceParente)
            If Par.Nome <> "" And Par.TIPOOPERAZIONE <> "" Then
                Dim MPar As New Cls_Pianodeiconti

                MPar.Mastro = Cserv.MASTRO
                MPar.Conto = Cserv.CONTO
                MPar.Sottoconto = Int(Par.CodiceOspite * 100) + Par.CodiceParente
                MPar.Descrizione = Par.Nome
                MPar.TipoAnagrafica = "O"
                MPar.Scrivi(Session("DC_GENERALE"))

            End If

        Next i

        ClientScript.RegisterClientScriptBlock(Me.GetType(), "Modifica", "alert('Centro Servizio Modificato');", True)

        DD_Struttura.Enabled = False
        Cmb_CServ.Enabled = False
        Chk_MovimentiContabili.Enabled = False
    End Sub

    Protected Sub Btn_Esci_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles Btn_Esci.Click
        If Request.Item("PAGINA") = "" Then
            Response.Redirect("RicercaAnagrafica.aspx?TIPO=MODIFICACSERV")
        Else
            Response.Redirect("RicercaAnagrafica.aspx?CHIAMATA=RITORNO&TIPO=MODIFICACSERV&PAGINA=" & Request.Item("PAGINA"))
        End If
    End Sub
End Class
