﻿<%@ Page Language="VB" AutoEventWireup="false" Inherits="OspitiWeb_ModificaRetta" CodeFile="ModificaRetta.aspx.vb" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="xasp" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
    <meta http-equiv="x-ua-compatible" content="IE=9" />
    <title>Modifica Retta</title>
    <asp:PlaceHolder runat="server">
        <%: System.Web.Optimization.Styles.Render("~/Content/AjaxControlToolkit/Styles/Bundle") %>
    </asp:PlaceHolder>
    <link href="ospiti.css?ver=11" rel="stylesheet" type="text/css" />
    <link rel="shortcut icon" href="images/SENIOR.ico" />
    <link href="js/jquery.autocomplete.css" rel="stylesheet" type="text/css" />

    <script src="js/jquery-1.5.1.min.js" type="text/javascript"></script>
    <script src="js/jquery.autocomplete.js" type="text/javascript"></script>
    <script src="js/soapclient.js" type="text/javascript"></script>
    <script src="/js/convertinumero.js" type="text/javascript"></script>

    <script src="js/jquery.maskedinput-1.3.min.js" type="text/javascript"></script>
    <script src="/js/formatnumer.js" type="text/javascript"></script>

    <script src="/js/pianoconti.js" type="text/javascript"></script>
    <script src="js/JSErrore.js" type="text/javascript"></script>
    <link rel="stylesheet" href="dialogbox/redips-dialog.css" type="text/css" media="screen" />
    <script type="text/javascript" src="dialogbox/redips-dialog-min.js"></script>
    <script type="text/javascript" src="dialogbox/script.js"></script>
    <script type="text/javascript" src="../js/menuanagraficaospiti.js?ver=7"></script>

    <script type="text/javascript">                 

        $(document).ready(function () {
            if (window.innerHeight > 0) { $("#BarraLaterale").css("height", (window.innerHeight - 94) + "px"); } else { $("#BarraLaterale").css("height", (document.documentElement.offsetHeight - 94) + "px"); }
        });
    </script>
</head>
<body>
    <form id="form1" runat="server">
        <asp:ScriptManager ID="ScriptManager1" runat="server" AsyncPostBackTimeout="900" EnableScriptGlobalization="true">
            <Scripts>
                <asp:ScriptReference Path="~/Scripts/AjaxControlToolkit/Bundle" />
            </Scripts>
        </asp:ScriptManager>
        <asp:Label ID="Lbl_BarraSenior" runat="server" Text=""></asp:Label>
        <div style="text-align: left;">
            <table style="width: 100%;" cellpadding="0" cellspacing="0">
                <tr>
                    <td style="width: 160px; background-color: #F0F0F0;"></td>
                    <td>
                        <div class="Titolo">Ospiti - Strumenti - Modifica Retta</div>
                        <div class="SottoTitoloOSPITE">
                            <asp:Label ID="Lbl_NomeOspite" runat="server"></asp:Label><br />
                            <br />
                        </div>
                    </td>
                    <td style="text-align: right; vertical-align: top;">

                        <div class="DivTastiOspite">
                            <asp:ImageButton ID="Btn_Visualizza" runat="server" Height="38px" ImageUrl="~/images/esegui.png" CssClass="EffettoBottoniTondi" Width="38px" />&nbsp;
		<asp:ImageButton ID="Btn_Modifica" runat="server" Height="38px" ImageUrl="~/images/salva.jpg" CssClass="EffettoBottoniTondi" Width="38px" ToolTip="Modifica / Inserisci (F2)" />
                        </div>
                    </td>
                </tr>
                <tr>
                    <td style="width: 160px; background-color: #F0F0F0; vertical-align: top; text-align: center;" id="BarraLaterale">
                        <asp:ImageButton ID="Btn_Esci" runat="server" BackColor="Transparent" ImageUrl="../images/Menu_Indietro.png" CssClass="Effetto" ToolTip="Chiudi" />


                    </td>
                    <td colspan="2" style="background-color: #FFFFFF;" valign="top">
                        <xasp:TabContainer ID="TabContainer1" runat="server" ActiveTabIndex="0" CssClass="TabSenior" Width="100%" BorderStyle="None" Style="margin-right: 39px">
                            <xasp:TabPanel runat="server" HeaderText="Modifica Retta" ID="Tab_Anagrafica">
                                <HeaderTemplate>Modifica Retta</HeaderTemplate>
                                <ContentTemplate>


                                    <label class="LabelCampo">Anno :</label><asp:TextBox ID="Txt_Anno" onkeypress="return soloNumeri(event);" MaxLength="4" runat="server" Width="64px"></asp:TextBox><br />
                                    <br />
                                    <label class="LabelCampo">Mese Da :</label><asp:DropDownList ID="Dd_MeseDa" runat="server" Width="128px">
                                        <asp:ListItem Value="1">Gennaio</asp:ListItem>
                                        <asp:ListItem Value="2">Febbraio</asp:ListItem>
                                        <asp:ListItem Value="3">Marzo</asp:ListItem>
                                        <asp:ListItem Value="4">Aprile</asp:ListItem>
                                        <asp:ListItem Value="5">Maggio</asp:ListItem>
                                        <asp:ListItem Value="6">Giugno</asp:ListItem>
                                        <asp:ListItem Value="7">Luglio</asp:ListItem>
                                        <asp:ListItem Value="8">Agosto</asp:ListItem>
                                        <asp:ListItem Value="9">Settembre</asp:ListItem>
                                        <asp:ListItem Value="10">Ottobre</asp:ListItem>
                                        <asp:ListItem Value="11">Novembre</asp:ListItem>
                                        <asp:ListItem Value="12">Dicembre</asp:ListItem>
                                    </asp:DropDownList>
                                    <br />
                                    <br />

                                    <label class="LabelCampo">Tipo :</label>
                                    <asp:DropDownList ID="DD_Tipo" runat="server">
                                        <asp:ListItem Value="O">Ospite</asp:ListItem>
                                        <asp:ListItem Value="P">Parente</asp:ListItem>
                                        <asp:ListItem Value="C">Comune</asp:ListItem>
                                        <asp:ListItem Value="R">Regione</asp:ListItem>
                                        <asp:ListItem Value="J">Jolly</asp:ListItem>
                                        <asp:ListItem></asp:ListItem>
                                    </asp:DropDownList>
                                    <br />
                                    <br />

                                    <asp:GridView ID="Grd_Retta" runat="server" CellPadding="4" Height="60px"
                                        ShowFooter="True" BackColor="White" BorderColor="#6FA7D1"
                                        BorderStyle="Solid" BorderWidth="1px">
                                        <RowStyle ForeColor="#6FA7D1" BackColor="White" />
                                        <Columns>
                                            <asp:TemplateField>
                                                <ItemTemplate>
                                                    <asp:ImageButton ID="IB_Delete" CommandName="Delete" runat="Server" ImageUrl="~/images/cancella.png" />
                                                </ItemTemplate>
                                                <FooterTemplate>
                                                    <div style="text-align: right">
                                                        <asp:ImageButton ID="ImageButton1" ImageUrl="~/images/inserisci.png" CommandName="Inserisci" runat="server" />
                                                    </div>
                                                </FooterTemplate>
                                            </asp:TemplateField>


                                            <asp:TemplateField HeaderText="Tipo">
                                                <ItemTemplate>
                                                    <asp:TextBox ID="TxtTipo" Style="text-align: right;" Width="100px" runat="server"></asp:TextBox>
                                                </ItemTemplate>
                                                <HeaderStyle Font-Bold="False" Font-Italic="False" />
                                                <ItemStyle Width="30px" />
                                            </asp:TemplateField>

                                            <asp:TemplateField HeaderText="Importo">
                                                <ItemTemplate>
                                                    <asp:TextBox ID="TxtImporto" Style="text-align: right;" Width="100px" runat="server"></asp:TextBox>
                                                </ItemTemplate>
                                                <HeaderStyle Font-Bold="False" Font-Italic="False" />
                                                <ItemStyle Width="30px" />
                                            </asp:TemplateField>
                                            <asp:TemplateField HeaderText="Giorni">
                                                <ItemTemplate>
                                                    <asp:TextBox ID="TxtGiorni" Style="text-align: right;" Width="60px" runat="server"></asp:TextBox>
                                                </ItemTemplate>
                                                <HeaderStyle Font-Bold="False" Font-Italic="False" />
                                                <ItemStyle Width="30px" />
                                            </asp:TemplateField>
                                            <asp:TemplateField HeaderText="Manuale">
                                                <ItemTemplate>
                                                    <asp:DropDownList ID="DD_Tipo" runat="server">
                                                        <asp:ListItem Value="M">Manuale</asp:ListItem>
                                                        <asp:ListItem Value=""></asp:ListItem>
                                                    </asp:DropDownList>
                                                </ItemTemplate>
                                                <HeaderStyle Font-Bold="False" Font-Italic="False" />
                                                <ItemStyle Width="30px" />
                                            </asp:TemplateField>

                                            <asp:TemplateField HeaderText="CodiceExtra">
                                                <ItemTemplate>
                                                    <asp:TextBox ID="TxtExtra" Style="text-align: right;" Width="100px" runat="server"></asp:TextBox>
                                                </ItemTemplate>
                                                <HeaderStyle Font-Bold="False" Font-Italic="False" />
                                                <ItemStyle Width="30px" />
                                            </asp:TemplateField>


                                            <asp:TemplateField HeaderText="Riferimento">
                                                <ItemTemplate>
                                                    <asp:TextBox ID="TxtRiferimento" Style="text-align: right;" Width="50px" runat="server"></asp:TextBox>
                                                    <asp:TextBox ID="TxtRiferimentob" Style="text-align: right;" Width="50px" runat="server"></asp:TextBox>
                                                </ItemTemplate>
                                                <HeaderStyle Font-Bold="False" Font-Italic="False" Width="210px " />
                                                <ItemStyle Width="30px" />
                                            </asp:TemplateField>

                                            <asp:TemplateField HeaderText="Anno">
                                                <ItemTemplate>
                                                    <asp:TextBox ID="TxtAnno" Style="text-align: right;" Width="100px" runat="server"></asp:TextBox>
                                                </ItemTemplate>
                                                <HeaderStyle Font-Bold="False" Font-Italic="False" />
                                                <ItemStyle Width="30px" />
                                            </asp:TemplateField>

                                            <asp:TemplateField HeaderText="Mese">
                                                <ItemTemplate>
                                                    <asp:TextBox ID="TxtMese" Style="text-align: right;" Width="100px" runat="server"></asp:TextBox>
                                                </ItemTemplate>
                                                <HeaderStyle Font-Bold="False" Font-Italic="False" />
                                                <ItemStyle Width="30px" />
                                            </asp:TemplateField>

                                        </Columns>
                                        <FooterStyle BackColor="White" ForeColor="#6FA7D1" />
                                        <HeaderStyle BackColor="#A6C9E2" Font-Bold="False" ForeColor="White"
                                            BorderColor="#6FA7D1" BorderWidth="1px" />
                                        <PagerStyle BackColor="#336666" ForeColor="White" HorizontalAlign="Center" BorderColor="#6FA7D1" />
                                        <SelectedRowStyle BackColor="#339966" Font-Bold="True" ForeColor="White" />
                                    </asp:GridView>


                                </ContentTemplate>
                            </xasp:TabPanel>
                        </xasp:TabContainer>
                    </td>
                </tr>
            </table>
        </div>
    </form>
</body>
</html>

