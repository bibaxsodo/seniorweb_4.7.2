﻿Imports Microsoft.VisualBasic
Imports System.Data.OleDb
Imports System.Web.Hosting
Partial Class OspitiWeb_WizardDomiciliare
    Inherits System.Web.UI.Page



    Private Sub EseguiJS()
        Dim MyJs As String
        MyJs = "$(document).ready(function() {"

        MyJs = MyJs & "var els = document.getElementsByTagName(""*"");"

        MyJs = MyJs & "for (var i=0;i<els.length;i++)"
        MyJs = MyJs & "if ( els[i].id ) { "
        MyJs = MyJs & " var appoggio =els[i].id; "
        MyJs = MyJs & " if (appoggio.match('Txt_DataRegistrazione')!= null) {  "
        MyJs = MyJs & " $(els[i]).mask(""99/99/9999"");"
        MyJs = MyJs & "    }"
        MyJs = MyJs & " if (appoggio.match('Txt_Ora')!= null) {  "
        MyJs = MyJs & " $(els[i]).mask(""99:99"");"
        MyJs = MyJs & "    }"
        MyJs = MyJs & " if ((appoggio.match('Txt_Importo')!= null) || (appoggio.match('TxtPercentuale')!= null) ) {  "
        MyJs = MyJs & " $(els[i]).keypress(function() { ForceNumericInput($(this).val(), true, true); } ); "
        MyJs = MyJs & " $(els[i]).blur(function() { var ap = formatNumber($(this).val(),2); $(this).val(ap); });"
        MyJs = MyJs & "    }"




        MyJs = MyJs & "} "
        MyJs = MyJs & "});"

        'MyJs = "$(document).ready(function() { $('.myClass').keypress(function() { handleEnter($('.myClass').val(), true, true); } );     $('#" & Txt_ImportoPagato.ClientID & "').blur(function() { var ap = formatNumber ($('#" & Txt_ImportoPagato.ClientID & "').val(),2); $('#" & Txt_ImportoPagato.ClientID & "').val(ap); }); });"
        ScriptManager.RegisterStartupScript(Me, Me.GetType(), "visualizzaRitIm", MyJs, True)
    End Sub
    Protected Sub Btn_Modifica_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles Btn_Modifica.Click
        Dim k As New Cls_MovimentiDomiciliare

        'cmd.CommandText = ("select * from MovimentiDomiciliare where centroservizio = '" & Request.Item("CENTROSERVIZIO") & "' And CodiceOspite = " & Request.Item("CODICEOSPITE") & " And Data  = ? Order by ID")
        'cmd.Parameters.AddWithValue("@DATA", DateSerial(Request.Item("ANNO"), Request.Item("MESE"), Request.Item("GIORNO")))

        Dim OraFine As String
        Dim MinutiFine As String
        Dim OraInizio As String
        Dim MinutiInizio As String



        If Txt_OraInizio.Text = "" Then
            ClientScript.RegisterClientScriptBlock(Me.GetType(), "Errore", "VisualizzaErrore('Ora Inizio formalmente errata');", True)
            Exit Sub
        End If

        If Txt_OraFine.Text = "" Then
            ClientScript.RegisterClientScriptBlock(Me.GetType(), "Errore", "VisualizzaErrore('Ora Fine formalmente errata');", True)
            Exit Sub
        End If
        If Txt_OraInizio.Text.Length < 5 Then
            ClientScript.RegisterClientScriptBlock(Me.GetType(), "Errore", "VisualizzaErrore('Ora Inizio formalmente errata');", True)
            Exit Sub
        End If

        If Txt_OraFine.Text.Length < 5 Then
            ClientScript.RegisterClientScriptBlock(Me.GetType(), "Errore", "VisualizzaErrore('Ora Fine formalmente errata');", True)
            Exit Sub
        End If


        OraFine = Mid(Txt_OraFine.Text, 1, 2)
        If Val(OraFine) > 24 Then
            ClientScript.RegisterClientScriptBlock(Me.GetType(), "Errore", "VisualizzaErrore('Ora Fine formalmente errata');", True)
            Exit Sub
        End If

        MinutiFine = Mid(Txt_OraFine.Text, 4, 2)
        If Val(MinutiFine) >= 60 Then
            ClientScript.RegisterClientScriptBlock(Me.GetType(), "Errore", "VisualizzaErrore('Ora Fine formalmente errata');", True)
            Exit Sub
        End If


        OraInizio = Mid(Txt_OraInizio.Text, 1, 2)
        If Val(OraInizio) > 24 Then
            ClientScript.RegisterClientScriptBlock(Me.GetType(), "Errore", "VisualizzaErrore('Ora Inizio formalmente errata');", True)
            Exit Sub
        End If

        MinutiInizio = Mid(Txt_OraInizio.Text, 4, 2)
        If Val(MinutiInizio) >= 60 Then
            ClientScript.RegisterClientScriptBlock(Me.GetType(), "Errore", "VisualizzaErrore('Ora Inizio formalmente errata');", True)
            Exit Sub
        End If

        Dim Giorno As Integer


        For Giorno = 1 To 31

            Dim CheckGiorno As CheckBox = DirectCast(Tab_Anagrafica.FindControl("GG_" & Giorno), CheckBox)

            If CheckGiorno.Checked = True Then


                k.CodiceOspite = Val(Request.Item("CODICEOSPITE"))
                k.CENTROSERVIZIO = Request.Item("CENTROSERVIZIO")
                k.Data = DateSerial(Request.Item("ANNO"), Request.Item("MESE"), Giorno)

                k.ID = 0

                k.Descrizione = Txt_Descrizione.Text



                k.OraFine = TimeSerial(OraFine, MinutiFine, 0).AddYears(1899).AddMonths(12).AddDays(30)
                k.OraInizio = TimeSerial(OraInizio, MinutiInizio, 0).AddYears(1899).AddMonths(12).AddDays(30)
                k.Operatore = DD_Operatore.SelectedValue
                k.Tipologia = DD_Tipologia.SelectedValue


                k.Utente = Session("UTENTE")

                k.AggiornaDB(Session("DC_OSPITE"))
            End If
        Next
        ClientScript.RegisterClientScriptBlock(Me.GetType(), "Errore", "alert('Operazione Effettuata');", True)
    End Sub

    Protected Sub OspitiWeb_WizardDomiciliare_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Call EseguiJS()

        If Page.IsPostBack = True Then
            Exit Sub
        End If


        Dim Numero As Integer = 0


        Dim Ktipo As New Cls_TipoDomiciliare


        Ktipo.UpDateDropBox(Session("DC_OSPITE"), DD_Tipologia)
        Dim KOperatore As New Cls_Operatore


        KOperatore.UpDateDropBox(Session("DC_OSPITE"), DD_Operatore)


        Dim Ospite As New ClsOspite

        Ospite.CodiceOspite = Val(Request.Item("CODICEOSPITE"))
        Ospite.Leggi(Session("DC_OSPITE"), Val(Request.Item("CODICEOSPITE")))

        Lbl_NomeOspite.Text = Ospite.Nome

        If GiorniMese(Request.Item("MESE"), Request.Item("ANNO")) = 30 Then
            GG_31.Enabled = False
        End If

        If GiorniMese(Request.Item("MESE"), Request.Item("ANNO")) = 29 Then
            GG_30.Enabled = False
            GG_31.Enabled = False
        End If
        If GiorniMese(Request.Item("MESE"), Request.Item("ANNO")) = 28 Then
            GG_29.Enabled = False
            GG_30.Enabled = False
            GG_31.Enabled = False
        End If

        Dim Mov As New Cls_Movimenti

        Mov.Data = Nothing
        Mov.CodiceOspite = Ospite.CodiceOspite
        Mov.CENTROSERVIZIO = Request.Item("CENTROSERVIZIO")
        Mov.PrimoAccoglimentoDefinitivaMese(Session("DC_OSPITE"), Request.Item("ANNO"), Request.Item("MESE"))
        If Not IsNothing(Mov.Data) And Month(Mov.Data) = Request.Item("MESE") And Year(Mov.Data) = Request.Item("ANNO") Then
            If Day(Mov.Data) > 1 Then GG_1.Enabled = False
            If Day(Mov.Data) > 2 Then GG_2.Enabled = False
            If Day(Mov.Data) > 3 Then GG_3.Enabled = False
            If Day(Mov.Data) > 4 Then GG_4.Enabled = False
            If Day(Mov.Data) > 5 Then GG_5.Enabled = False
            If Day(Mov.Data) > 6 Then GG_6.Enabled = False
            If Day(Mov.Data) > 7 Then GG_7.Enabled = False
            If Day(Mov.Data) > 8 Then GG_8.Enabled = False
            If Day(Mov.Data) > 9 Then GG_9.Enabled = False
            If Day(Mov.Data) > 10 Then GG_10.Enabled = False
            If Day(Mov.Data) > 11 Then GG_11.Enabled = False
            If Day(Mov.Data) > 12 Then GG_12.Enabled = False
            If Day(Mov.Data) > 13 Then GG_13.Enabled = False
            If Day(Mov.Data) > 14 Then GG_14.Enabled = False
            If Day(Mov.Data) > 15 Then GG_15.Enabled = False
            If Day(Mov.Data) > 16 Then GG_16.Enabled = False
            If Day(Mov.Data) > 17 Then GG_17.Enabled = False
            If Day(Mov.Data) > 18 Then GG_18.Enabled = False
            If Day(Mov.Data) > 19 Then GG_19.Enabled = False
            If Day(Mov.Data) > 20 Then GG_20.Enabled = False
            If Day(Mov.Data) > 21 Then GG_21.Enabled = False
            If Day(Mov.Data) > 22 Then GG_22.Enabled = False
            If Day(Mov.Data) > 23 Then GG_23.Enabled = False
            If Day(Mov.Data) > 24 Then GG_24.Enabled = False
            If Day(Mov.Data) > 25 Then GG_25.Enabled = False
            If Day(Mov.Data) > 26 Then GG_26.Enabled = False
            If Day(Mov.Data) > 27 Then GG_27.Enabled = False
            If Day(Mov.Data) > 28 Then GG_28.Enabled = False
            If Day(Mov.Data) > 29 Then GG_29.Enabled = False
            If Day(Mov.Data) > 30 Then GG_30.Enabled = False
            If Day(Mov.Data) > 31 Then GG_31.Enabled = False
        End If

        Mov.Data = Nothing
        Mov.CodiceOspite = Ospite.CodiceOspite
        Mov.CENTROSERVIZIO = Request.Item("CENTROSERVIZIO")
        Mov.UltimaDataUscitaDefinitivaMese(Session("DC_OSPITE"), Request.Item("ANNO"), Request.Item("MESE"))
        If Not IsNothing(Mov.Data) And Month(Mov.Data) = Request.Item("MESE") And Year(Mov.Data) = Request.Item("ANNO") Then
            If Day(Mov.Data) < 1 Then GG_1.Enabled = False
            If Day(Mov.Data) < 2 Then GG_2.Enabled = False
            If Day(Mov.Data) < 3 Then GG_3.Enabled = False
            If Day(Mov.Data) < 4 Then GG_4.Enabled = False
            If Day(Mov.Data) < 5 Then GG_5.Enabled = False
            If Day(Mov.Data) < 6 Then GG_6.Enabled = False
            If Day(Mov.Data) < 7 Then GG_7.Enabled = False
            If Day(Mov.Data) < 8 Then GG_8.Enabled = False
            If Day(Mov.Data) < 9 Then GG_9.Enabled = False
            If Day(Mov.Data) < 10 Then GG_10.Enabled = False
            If Day(Mov.Data) < 11 Then GG_11.Enabled = False
            If Day(Mov.Data) < 12 Then GG_12.Enabled = False
            If Day(Mov.Data) < 13 Then GG_13.Enabled = False
            If Day(Mov.Data) < 14 Then GG_14.Enabled = False
            If Day(Mov.Data) < 15 Then GG_15.Enabled = False
            If Day(Mov.Data) < 16 Then GG_16.Enabled = False
            If Day(Mov.Data) < 17 Then GG_17.Enabled = False
            If Day(Mov.Data) < 18 Then GG_18.Enabled = False
            If Day(Mov.Data) < 19 Then GG_19.Enabled = False
            If Day(Mov.Data) < 20 Then GG_20.Enabled = False
            If Day(Mov.Data) < 21 Then GG_21.Enabled = False
            If Day(Mov.Data) < 22 Then GG_22.Enabled = False
            If Day(Mov.Data) < 23 Then GG_23.Enabled = False
            If Day(Mov.Data) < 24 Then GG_24.Enabled = False
            If Day(Mov.Data) < 25 Then GG_25.Enabled = False
            If Day(Mov.Data) < 26 Then GG_26.Enabled = False
            If Day(Mov.Data) < 27 Then GG_27.Enabled = False
            If Day(Mov.Data) < 28 Then GG_28.Enabled = False
            If Day(Mov.Data) < 29 Then GG_29.Enabled = False
            If Day(Mov.Data) < 30 Then GG_30.Enabled = False
            If Day(Mov.Data) < 31 Then GG_31.Enabled = False
        End If

    End Sub

    Public Function GiorniMese(ByVal Mese As Byte, ByVal Anno As Integer) As Byte
        If Mese = 1 Or Mese = 3 Or Mese = 5 Or Mese = 7 Or Mese = 8 Or _
           Mese = 10 Or Mese = 12 Then
            GiorniMese = 31
        Else
            If Mese <> 2 Then
                GiorniMese = 30
            Else
                If Day(DateSerial(Anno, Mese, 29)) = 29 Then
                    GiorniMese = 29
                Else
                    GiorniMese = 28
                End If
            End If
        End If
    End Function
End Class
