﻿Imports Microsoft.VisualBasic
Imports System.Data.OleDb
Imports System.Web.Hosting


Partial Class SelezionaSocieta
    Inherits System.Web.UI.Page

    Protected Sub SelezionaSocieta_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load


        If Session("USER_SODO") <> True Then

            Response.Redirect("login.aspx")
            Exit Sub
        End If

        If Page.IsPostBack = True Then
            Exit Sub
        End If

        Dim Barra As New Cls_BarraSenior

        Lbl_BarraSenior.Text = Barra.CodiceBarra(Application("SENIOR"), "", Page)


        Dim cn As OleDbConnection



        cn = New Data.OleDb.OleDbConnection(Application("SENIOR"))

        cn.Open()
        Dim cmd As New OleDbCommand()
        cmd.CommandText = ("select * from ArchivioClienti order by RAGIONESOCIALE")
        cmd.Connection = cn
        Dim sb As StringBuilder = New StringBuilder

        DD_Societa.Items.Clear()
        DD_Societa.Items.Add("")
        DD_Societa.Items(DD_Societa.Items.Count - 1).Value = ""
        DD_Societa.Items(DD_Societa.Items.Count - 1).Selected = True
        Dim myPOSTreader As OleDbDataReader = cmd.ExecuteReader()
        Do While myPOSTreader.Read
            DD_Societa.Items.Add(myPOSTreader.Item("RAGIONESOCIALE"))
            DD_Societa.Items(DD_Societa.Items.Count - 1).Value = myPOSTreader.Item("CLIENTE")
        Loop
        myPOSTreader.Close()
        cn.Close()
    End Sub

    Protected Sub IB_ProssimaPagina_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles IB_ProssimaPagina.Click

        Response.Redirect("CreaUtenti.aspx?IDCLIENTE=" & DD_Societa.SelectedItem.Value)
    End Sub


    Private Function UpDateDBIndex(ByVal condizione As String, ByVal nome As String) As String
        Dim cn As OleDbConnection
        Dim MySql As String = ""
        Dim Segnalazione As String = "OK"

        Try

            cn = New Data.OleDb.OleDbConnection(condizione)

            cn.Open()
        Catch ex As Exception
            Segnalazione = "Erorre in connessione"
            Return Segnalazione
        End Try



        Try
            Dim tr As System.IO.TextReader = System.IO.File.OpenText(HostingEnvironment.ApplicationPhysicalPath() & "\SqlDataBase\Index" & nome & ".sql")

            MySql = tr.ReadToEnd()

            Dim cmdI As New OleDbCommand()
            cmdI.CommandText = MySql
            cmdI.Connection = cn
            cmdI.ExecuteNonQuery()
        Catch ex As Exception
            Segnalazione = "Erorre in allineamento " & ex.Message
            Return Segnalazione
        End Try

        cn.Close()

        Return Segnalazione

    End Function


    Private Function UpDateDB(ByVal condizione As String, ByVal nome As String) As String
        Dim cn As OleDbConnection
        Dim MySql As String = ""
        Dim Segnalazione As String = "OK"

        Try

            cn = New Data.OleDb.OleDbConnection(condizione)

            cn.Open()
        Catch ex As Exception
            Segnalazione = "Erorre in connessione"
            Return Segnalazione
        End Try



        Try
            Dim tr As System.IO.TextReader = System.IO.File.OpenText(HostingEnvironment.ApplicationPhysicalPath() & "\SqlDataBase\" & nome & ".sql")

            MySql = tr.ReadToEnd()

            Dim cmdI As New OleDbCommand()
            cmdI.CommandText = MySql
            cmdI.Connection = cn
            cmdI.ExecuteNonQuery()
        Catch ex As Exception
            Segnalazione = "Erorre in allineamento " & ex.Message
            Return Segnalazione
        End Try


        cn.Close()

        Return Segnalazione

    End Function

    Protected Sub Btn_AllineaDB_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles Btn_AllineaDB.Click


        Dim cn As OleDbConnection

        LblSegnalazioni.Text = ""

        cn = New Data.OleDb.OleDbConnection(Application("SENIOR"))

        cn.Open()
        Dim cmd As New OleDbCommand()
        cmd.CommandText = ("select * from ArchivioClienti order by RAGIONESOCIALE")
        cmd.Connection = cn
        Dim sb As StringBuilder = New StringBuilder

        DD_Societa.Items.Clear()
        DD_Societa.Items.Add("")
        DD_Societa.Items(DD_Societa.Items.Count - 1).Value = ""
        DD_Societa.Items(DD_Societa.Items.Count - 1).Selected = True
        Dim myPOSTreader As OleDbDataReader = cmd.ExecuteReader()
        Do While myPOSTreader.Read
            If campodb(myPOSTreader.Item("RAGIONESOCIALE")) <> "" Then
                If campodb(myPOSTreader.Item("OSPITI")) <> "" Then
                    LblSegnalazioni.Text = LblSegnalazioni.Text & campodb(myPOSTreader.Item("RAGIONESOCIALE")) & " - OSPITI - " & UpDateDB(campodb(myPOSTreader.Item("OSPITI")), "OSPITE")
                End If
                If campodb(myPOSTreader.Item("TABELLE")) <> "" Then
                    LblSegnalazioni.Text = LblSegnalazioni.Text & campodb(myPOSTreader.Item("RAGIONESOCIALE")) & " - TABELLE - " & UpDateDB(campodb(myPOSTreader.Item("TABELLE")), "TABELLE")
                End If
                If campodb(myPOSTreader.Item("GENERALE")) <> "" Then
                    LblSegnalazioni.Text = LblSegnalazioni.Text & campodb(myPOSTreader.Item("RAGIONESOCIALE")) & " - GENERALE - " & UpDateDB(campodb(myPOSTreader.Item("GENERALE")), "GENERALE")
                End If
                If campodb(myPOSTreader.Item("OSPITIACCESSORI")) <> "" Then
                    LblSegnalazioni.Text = LblSegnalazioni.Text & campodb(myPOSTreader.Item("RAGIONESOCIALE")) & " - OSPITIACCESSORI - " & UpDateDB(campodb(myPOSTreader.Item("OSPITIACCESSORI")), "OSPITIACCESSORI")
                End If
                LblSegnalazioni.Text = LblSegnalazioni.Text & "<br/>"

                If campodb(myPOSTreader.Item("OSPITI")) <> "" Then
                    LblSegnalazioni.Text = LblSegnalazioni.Text & campodb(myPOSTreader.Item("RAGIONESOCIALE")) & " - INDICI OSPITI - " & UpDateDBIndex(campodb(myPOSTreader.Item("OSPITI")), "OSPITE")
                End If
                If campodb(myPOSTreader.Item("TABELLE")) <> "" Then
                    LblSegnalazioni.Text = LblSegnalazioni.Text & campodb(myPOSTreader.Item("RAGIONESOCIALE")) & " - INDICI TABELLE - " & UpDateDBIndex(campodb(myPOSTreader.Item("TABELLE")), "TABELLE")
                End If
                If campodb(myPOSTreader.Item("GENERALE")) <> "" Then
                    LblSegnalazioni.Text = LblSegnalazioni.Text & campodb(myPOSTreader.Item("RAGIONESOCIALE")) & " - INDICI GENERALE - " & UpDateDBIndex(campodb(myPOSTreader.Item("GENERALE")), "GENERALE")
                End If                
                LblSegnalazioni.Text = LblSegnalazioni.Text & "<br/>"
            End If
        Loop
        myPOSTreader.Close()
        cn.Close()
    End Sub

    Function campodb(ByVal oggetto As Object) As String
        If IsDBNull(oggetto) Then
            Return ""
        Else
            Return oggetto
        End If
    End Function

    Protected Sub Btn_StatoFatturazione_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles Btn_StatoFatturazione.Click
        Dim cn As OleDbConnection

        LblSegnalazioni.Text = ""

        cn = New Data.OleDb.OleDbConnection(Application("SENIOR"))

        cn.Open()
        Dim cmd As New OleDbCommand()
        cmd.CommandText = ("select * from ArchivioClienti order by RAGIONESOCIALE")
        cmd.Connection = cn
        Dim sb As StringBuilder = New StringBuilder

        DD_Societa.Items.Clear()
        DD_Societa.Items.Add("")
        DD_Societa.Items(DD_Societa.Items.Count - 1).Value = ""
        DD_Societa.Items(DD_Societa.Items.Count - 1).Selected = True
        Dim myPOSTreader As OleDbDataReader = cmd.ExecuteReader()
        Do While myPOSTreader.Read
            If campodb(myPOSTreader.Item("RAGIONESOCIALE")) <> "" Then
                If campodb(myPOSTreader.Item("OSPITI")) <> "" Then
                    Dim M As New Cls_Parametri
                    Try

                        M.LeggiParametri(campodb(myPOSTreader.Item("OSPITI")))


                        LblSegnalazioni.Text = LblSegnalazioni.Text & campodb(myPOSTreader.Item("RAGIONESOCIALE")) & " - Periodo Fatturazione - " & M.MeseFatturazione & "/" & M.AnnoFatturazione
                    Catch ex As Exception
                        LblSegnalazioni.Text = LblSegnalazioni.Text & campodb(myPOSTreader.Item("RAGIONESOCIALE")) & " - Errore in lettura parametri"
                    End Try
                End If
                
                LblSegnalazioni.Text = LblSegnalazioni.Text & "<br/>"
            End If
        Loop
        myPOSTreader.Close()
        cn.Close()
    End Sub

    Protected Sub Btn_Home_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles Btn_Home.Click
        Response.Redirect("Login.aspx")
    End Sub

    
    Protected Sub Lnk_AbilitaProcedure_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles Lnk_AbilitaProcedure.Click
        Response.Redirect("AbilitaProcedure.aspx")

    End Sub

    Protected Sub Btn_Attivo_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles Btn_Attivo.Click
        Dim cn As OleDbConnection

        LblSegnalazioni.Text = "Società attive negli ultimi 30 minuti<br/>"

        cn = New Data.OleDb.OleDbConnection(Application("SENIOR"))

        cn.Open()
        Dim cmd As New OleDbCommand()
        cmd.CommandText = ("select * from ArchivioClienti order by RAGIONESOCIALE")
        cmd.Connection = cn

        DD_Societa.Items.Clear()
        DD_Societa.Items.Add("")
        DD_Societa.Items(DD_Societa.Items.Count - 1).Value = ""
        DD_Societa.Items(DD_Societa.Items.Count - 1).Selected = True
        Dim myPOSTreader As OleDbDataReader = cmd.ExecuteReader()
        Do While myPOSTreader.Read
            If campodb(myPOSTreader.Item("RAGIONESOCIALE")) <> "" Then
                If campodb(myPOSTreader.Item("TABELLE")) <> "" Then
                    Try
                        Dim cnOsp As OleDbConnection

                        cnOsp = New Data.OleDb.OleDbConnection(campodb(myPOSTreader.Item("TABELLE")))

                        cnOsp.Open()

                        Dim cmdA As New OleDbCommand()
                        cmdA.CommandText = ("select Top 1 DataOra from LogPrivacy Where DataOra > ? Order by DataOra")
                        cmdA.Connection = cnOsp
                        cmdA.Parameters.AddWithValue("@DataOra", Now.AddMinutes(-30))
                        Dim ReadCmdA As OleDbDataReader = cmdA.ExecuteReader()
                        If ReadCmdA.Read Then
                            LblSegnalazioni.Text = LblSegnalazioni.Text & campodb(myPOSTreader.Item("RAGIONESOCIALE")) & " - Ultima Operazione - " & campodb(ReadCmdA.Item("DataOra")) & "<BR>"
                        End If
                        ReadCmdA.Close()
                        cnOsp.Close()

                    Catch ex As Exception
                        LblSegnalazioni.Text = LblSegnalazioni.Text & campodb(myPOSTreader.Item("RAGIONESOCIALE")) & " - Errore in lettura dati privacy <BR>"
                    End Try
                End If
            End If
        Loop
        myPOSTreader.Close()
        cn.Close()


    End Sub
End Class
