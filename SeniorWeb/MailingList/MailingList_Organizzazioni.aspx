﻿<%@ Page Language="VB" AutoEventWireup="false" Inherits="MailingList_MailingList_Organizzazioni" CodeFile="MailingList_Organizzazioni.aspx.vb" %>


<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="AJAX" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
    <title>Elenco Anagrafiche</title>
    <asp:PlaceHolder runat="server">
        <%: System.Web.Optimization.Styles.Render("~/Content/AjaxControlToolkit/Styles/Bundle") %>
    </asp:PlaceHolder>
    <link rel="stylesheet" href="css/csv.css?Versione=8" type="text/css" />
    <link href="js/jquery.autocomplete.css" rel="stylesheet" type="text/css" />
    <script src="js/jquery-1.5.1.min.js" type="text/javascript"></script>
    <script src="js/jquery.maskedinput-1.3.min.js" type="text/javascript"></script>
    <script src="js/jquery.autocomplete.js" type="text/javascript"></script>
    <script src="/js/formatnumer.js" type="text/javascript"></script>
    <script src="js/JSErrore.js" type="text/javascript"></script>
    <script src="js/NoEnter.js" type="text/javascript"></script>


    <script type="text/javascript">
        $(document).ready(function () {
            if (window.innerHeight > 0) { $("#BarraLaterale").css("height", (window.innerHeight - 94) + "px"); } else { $("#BarraLaterale").css("height", (document.documentElement.offsetHeight - 94) + "px"); }
        });
    </script>
</head>
<body>
    <form id="form1" runat="server">
        <asp:ScriptManager ID="ScriptManager1" runat="server" EnableScriptGlobalization="true">
            <Scripts>
                <asp:ScriptReference Path="~/Scripts/AjaxControlToolkit/Bundle" />
            </Scripts>
        </asp:ScriptManager>
        <div>

            <table width="100%">
                <tr>
                    <td>
                        <div class="Titolo">Mailing List</div>
                        <div class="SottoTitolo">
                            <br />
                            <br />
                        </div>
                    </td>
                    <td align="right">
                        <asp:ImageButton ID="Imb_Exporta" runat="server" ImageUrl="images/export.jpg" />
                    </td>
                </tr>
            </table>
            <asp:CheckBox ID="Chk_SoloPresenti" runat="server" Text="Escludi Usciti Definitivi" Checked />
            <br />
            <asp:RadioButton ID="Rb_Ospiti" runat="server" Text="Ospiti" GroupName="TIPOANA" Checked="true" />
            <asp:RadioButton ID="Rb_Parenti" runat="server" Text="Parenti" GroupName="TIPOANA" Checked="false" />
            <asp:RadioButton ID="Rb_Comuni" runat="server" Text="Comuni" GroupName="TIPOANA" Checked="false" />
            <asp:RadioButton ID="Rb_Regioni" runat="server" Text="Regioni" GroupName="TIPOANA" Checked="false" />
            <asp:RadioButton ID="Rb_Medici" runat="server" Text="Medici" GroupName="TIPOANA" Checked="false" />
            <asp:RadioButton ID="Rb_ClientiFornitori" runat="server" Text="Clienti Fornitori" GroupName="TIPOANA" Checked="false" /><br />
            <table width="100%">
                <tr>
                    <td style="color: #565151;">Ragione sociale/Nome :</td>
                    <td>
                        <asp:TextBox ID="Txt_Cognome" onkeypress="return handleEnter(this, event)" MaxLength="30" Width="350px" runat="server"></asp:TextBox></td>
                    <td style="color: #565151;"></td>
                    <td></td>
                    <td align="right">
                        <asp:ImageButton ID="Btn_Ricerca" runat="server" ImageUrl="~/images/ricerca.png" BackColor="Transparent" /></td>
                </tr>
            </table>

            <asp:GridView ID="Grd_Visualizzazione" runat="server" CellPadding="3"
                Width="100%" BackColor="White" BorderColor="#999999" BorderStyle="None"
                BorderWidth="1px" GridLines="Vertical">
                <Columns>
                    <asp:TemplateField HeaderText="">
                        <ItemTemplate>
                            <asp:CheckBox ID="Chk_Selezionato" runat="server" Text="" />
                        </ItemTemplate>
                    </asp:TemplateField>
                </Columns>
                <FooterStyle BackColor="#CCCCCC" ForeColor="Black" />
                <PagerStyle BackColor="#999999" ForeColor="Black" HorizontalAlign="Center" />
                <SelectedRowStyle BackColor="#008A8C" Font-Bold="True" ForeColor="White" />
                <HeaderStyle BackColor="#565151" Font-Bold="False" Font-Size="Small"
                    ForeColor="White" />
                <AlternatingRowStyle BackColor="Gainsboro" />
            </asp:GridView>
            <br />

            <asp:LinkButton ID="Btn_SelezionaAll" runat="server">Seleziona Tutti</asp:LinkButton>
            <asp:LinkButton ID="Btn_InvertiSelezione" runat="server">Inverti Selezione</asp:LinkButton>

            <br />

            <br />



        </div>
    </form>
</body>
</html>
