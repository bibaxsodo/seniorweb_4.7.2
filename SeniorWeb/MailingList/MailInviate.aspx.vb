﻿Imports System.IO
Imports System.Net.Mail
Imports System.Web.Hosting
Imports System.Data.OleDb

Partial Class MailingList_MailInviate
    Inherits System.Web.UI.Page
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load

        If Trim(Session("UTENTE")) = "" Then
            Response.Redirect("../Login.aspx")
            Exit Sub
        End If

        If Page.IsPostBack = True Then
            Exit Sub
        End If



        Dim kBarra As New Cls_BarraSenior

        Lbl_BarraSenior.Text = kBarra.CodiceBarra(Application("SENIOR"), Session("UTENTE"), Page)


        Lbl_Testo.Text = "<table class=""tabella"">" & Session("LOGMAIL") & "</table>"


    End Sub

    
    Protected Sub Btn_Excel_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles Btn_Excel.Click

        Response.Clear()
        Response.ClearContent()
        Response.ClearHeaders()
        Response.Charset = String.Empty
        Response.Cache.SetCacheability(HttpCacheability.NoCache)        
        Response.AddHeader("content-disposition", "attachment;filename=MailInviate.xls")
        Response.ContentType = "application/vnd.xls"
        Response.Write("<table>" & Session("LOGMAIL") & "</table>")
        Response.Flush()
        Response.End()
    End Sub
End Class
