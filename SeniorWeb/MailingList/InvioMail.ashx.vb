﻿Imports System.IO
Imports System.Net.Mail
Imports System.Web.Hosting

Public Class InvioMail
    Implements System.Web.IHttpHandler

    Public Sub ProcessRequest(ByVal context As HttpContext) Implements IHttpHandler.ProcessRequest
        Dim UTENTE As String = context.Request.QueryString("UTENTE")
        Dim MsgFrom As String = context.Request.QueryString("MsgFrom")
        Dim MsgTo As String = context.Request.QueryString("MsgTo")
        Dim MsgSubject As String = context.Request.QueryString("MsgSubject")
        Dim Allegato As String = context.Request.QueryString("Allegato")
        Dim MsgBody As String = context.Request.Item("MsgBody")
        context.Response.ContentType = "text/plain"
        context.Response.Cache.SetCacheability(HttpCacheability.NoCache)
        Dim Appoggio As String



        Appoggio = SendAnEmail(MsgFrom, MsgTo, MsgSubject, MsgBody, Allegato, context)
        context.Response.Write(Appoggio)
    End Sub

    Public ReadOnly Property IsReusable() As Boolean Implements IHttpHandler.IsReusable
        Get
            Return False
        End Get
    End Property


    Public Function SendAnEmail(ByVal MsgFrom As String, ByVal MsgTo As String, ByVal MsgSubject As String, ByVal MsgBody As String, ByVal Allegato As String, ByVal context As HttpContext) As String


        Dim Kx As New Cls_Login

        Kx.Utente = context.Session("UTENTE")
        Kx.LeggiSP(context.Application("SENIOR"))

        Dim NomeSocieta As String


        NomeSocieta = Kx.RagioneSociale

        Try

            Dim msg As New Net.Mail.MailMessage(MsgFrom, MsgTo, MsgSubject, MsgBody)


            Dim mailClient As New SmtpClient(Kx.SMTP)  ' 


            mailClient.Credentials = New System.Net.NetworkCredential(Kx.UserName, Kx.Passwordsmtp)


            Dim Appo As String

            Appo = Dir(HostingEnvironment.ApplicationPhysicalPath() & "\Public\" & NomeSocieta & "_Allegati")
            If Dir(HostingEnvironment.ApplicationPhysicalPath() & "\Public\" & NomeSocieta & "_Allegati", FileAttribute.Directory) = "" Then
                MkDir(HostingEnvironment.ApplicationPhysicalPath() & "\Public\" & NomeSocieta & "_Allegati")
            End If


            If Dir(HostingEnvironment.ApplicationPhysicalPath() & "\Public\" & NomeSocieta & "_Allegati" & "\MAIL_" & context.Session("SALVAMAILID"), FileAttribute.Directory) = "" Then
                MkDir(HostingEnvironment.ApplicationPhysicalPath() & "\Public\" & NomeSocieta & "_Allegati" & "\MAIL_" & context.Session("SALVAMAILID"))
            End If


            Dim objDI As New DirectoryInfo(HostingEnvironment.ApplicationPhysicalPath() & "\Public\" & NomeSocieta & "_Allegati" & "\MAIL_" & context.Session("SALVAMAILID") & "\")
            Dim aryItemsInfo() As FileSystemInfo
            Dim objItem As FileSystemInfo


            aryItemsInfo = objDI.GetFileSystemInfos()
            For Each objItem In aryItemsInfo
                Dim MyAllegato As String

                MyAllegato = HostingEnvironment.ApplicationPhysicalPath() & "\Public\" & NomeSocieta & "_Allegati" & "\MAIL_" & context.Session("SALVAMAILID") & "\" & objItem.Name

                Dim at1 As New Attachment(MyAllegato)
                msg.Attachments.Add(at1)

            Next


            Dim at As New Attachment(Allegato)
            msg.Attachments.Add(at)

            msg.IsBodyHtml = True

            mailClient.Send(msg)



            '  Housekeeping
            msg.Dispose()
            Return "OK"

        Catch ex As FormatException
            Return (ex.Message & " :Format Exception")
        Catch ex As SmtpException
            Return (ex.Message & " :SMTP Exception")
        End Try

    End Function


End Class