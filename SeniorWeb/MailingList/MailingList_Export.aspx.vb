﻿
Partial Class MailingList_MailingList_Export
    Inherits System.Web.UI.Page

    Protected Sub MailingList_MailingList_Export_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Dim TabExport As New System.Data.DataTable("TabExport")


        TabExport = Session("Export")

        Grd_Visualizzazione.AutoGenerateColumns = True
        Grd_Visualizzazione.DataSource = TabExport
        Grd_Visualizzazione.DataBind()
    End Sub
End Class
