﻿<%@ Page Language="VB" AutoEventWireup="false" Inherits="Menu_MailList" CodeFile="Menu_MailList.aspx.vb" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
    <title>Menu MailingList</title>
    <asp:PlaceHolder runat="server">
        <%: System.Web.Optimization.Styles.Render("~/Content/AjaxControlToolkit/Styles/Bundle") %>
    </asp:PlaceHolder>
    <link rel="stylesheet" href="ospiti.css?ver=11" type="text/css" />
    <link rel="shortcut icon" href="images/SENIOR.ico" />
    <link href="js/jquery.autocomplete.css" rel="stylesheet" type="text/css" />

    <script src="js/jquery-1.5.1.min.js" type="text/javascript"></script>
    <script src="js/jquery.autocomplete.js?ver=1" type="text/javascript"></script>
    <script src="js/jquery.maskedinput-1.3.min.js" type="text/javascript"></script>
    <script src="js/NoEnter.js" type="text/javascript"></script>
    <script type="text/javascript">
        $(document).ready(function () {
            if (window.innerHeight > 0) { $("#BarraLaterale").css("height", (window.innerHeight - 94) + "px"); } else { $("#BarraLaterale").css("height", (document.documentElement.offsetHeight - 94) + "px"); }
        });
        function chiudinews() {

            $("#news").css('visibility', 'hidden');
        }
    </script>
    <style>
        #news {
            border: solid 1px #2d2d2d;
            text-align: left;
            vertical-align: top;
            background: white;
            width: 400px;
            height: 470px;
            position: absolute;
            -moz-border-radius: 5px;
            -webkit-border-radius: 5px;
            border-radius: 5px;
            box-shadow: 0px 0px 10px 4px rgba(0, 125, 196, 0.75);
            -moz-box-shadow: 0px 0px 10px 4px rgba(0, 125, 196, 0.75);
            -webkit-box-shadow: 0px 0px 10px 4px rgba(0, 125, 196, 0.75);
            z-index: 133;
        }
    </style>
</head>
<body>
    <form id="form1" runat="server">
        <asp:ScriptManager ID="ScriptManager1" runat="server" EnableScriptGlobalization="true">
            <Scripts>
                <asp:ScriptReference Path="~/Scripts/AjaxControlToolkit/Bundle" />
            </Scripts>
        </asp:ScriptManager>
        <div>
            <asp:Label ID="Lbl_BarraSenior" runat="server" Text=""></asp:Label>
            <table style="width: 100%;" cellpadding="0" cellspacing="0">
                <tr>
                    <td style="width: 140px; background-color: #F0F0F0; text-align: center;"></td>
                    <td>
                        <div class="Titolo">Mailing List - Principale</div>
                        <div class="SottoTitolo">
                            <br />
                        </div>
                    </td>

                </tr>

                <tr>

                    <td style="width: 140px; background-color: #F0F0F0; text-align: center; vertical-align: top;" id="BarraLaterale">
                        <asp:ImageButton ID="ImageButton1" runat="server" BackColor="Transparent" ImageUrl="../images/Menu_Indietro.png" ToolTip="Chiudi" />

                    </td>
                    <td colspan="2" style="vertical-align: top;">
                        <table style="width: 900px;">

                            <tr>

                                <td style="text-align: center; width: 150px;">
                                    <a href="Elenco_Mail.aspx">
                                        <img src="../images/Mail.jpg" style="border-width: 0;"></a></td>
                                <td style="text-align: center; width: 150px;">
                                    <a href="Elenco_MailingList.aspx">
                                        <img src="../images/MailingList.jpg" style="border-width: 0;"></a></td>
                                <td style="text-align: center; width: 150px;">
                                    <a href="InvioMail.aspx">
                                        <img src="images/SendMail.jpg" style="border-width: 0;"></a></td>
                                <td style="text-align: center; width: 150px;">
                                    <a href="InviaFatture.aspx">
                                        <img src="images/SendMail.jpg" style="border-width: 0;"></a></td>
                                <td style="text-align: center; width: 150px;">
                                    <a href="StatisticheMailInviate.aspx">
                                        <img src="../images/bottonestatistica.jpg" style="border-width: 0;"></a></td>
                                <td style="text-align: center; width: 150px;">&nbsp;</td>
                            </tr>
                            <tr>
                                <td style="text-align: center; vertical-align: top;"><span class="MenuText">MAIL</span></td>
                                <td style="text-align: center; vertical-align: top;"><span class="MenuText">MAIL LIST</span></td>
                                <td style="text-align: center; vertical-align: top;"><span class="MenuText">INVIO MAIL</span></td>
                                <td style="text-align: center; vertical-align: top;"><span class="MenuText">INVIO FATTURE</span></td>
                                <td style="text-align: center; vertical-align: top;"><span class="MenuText">STAT. INVIO MAIL</span></td>
                                <td style="text-align: center; vertical-align: top;"></td>
                            </tr>


                            <tr>
                                <td></td>
                                <td></td>
                            </tr>


                        </table>
                    </td>
                </tr>

            </table>


        </div>


        <div id="news" style="position: absolute; right: 50px; top: 100px;">
            <h1>Aggiornamenti Mailing List - Senior</h1>
            <br />
            1) Invio Fatture, possibilità di selezionare quali fatture inviare prima di procedere all'invio<br />
            <br />
            2) Invio Mail non collegate alle fatture, possibilità di selezionare quali mail inviare prima dell'invio stesso<br />
            <br />
            3) In entrambe le funzionalità è possibile esportare in excel l'esito dell'invio<br />
            <br />
            <br />
            <input type="button" style="position: inherit; bottom: 10px; left: 40%;" onclick="chiudinews();" value="CHIUDI" title="CHIUDI">
        </div>

    </form>
</body>
</html>
