﻿Imports Microsoft.VisualBasic
Imports System.Data.OleDb
Imports System.Web.Hosting

Partial Class Elenco_Mail
    Inherits System.Web.UI.Page



    Dim Tab_RicercaCorsi As New System.Data.DataTable("Tab_RicercaSoci")

    Protected Sub Btn_Ricerca_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles Btn_Ricerca.Click

        Tab_RicercaCorsi.Clear()
        Tab_RicercaCorsi.Columns.Clear()
        Tab_RicercaCorsi.Columns.Add("ID", GetType(Long))
        Tab_RicercaCorsi.Columns.Add("Data", GetType(String))
        Tab_RicercaCorsi.Columns.Add("Descrizione", GetType(String))



        Dim cn As OleDbConnection



        cn = New Data.OleDb.OleDbConnection(Session("DC_OSPITE"))

        cn.Open()
        Dim cmd As New OleDbCommand()

        If Val(Txt_ID.Text) > 0 Then
            cmd.CommandText = ("select * from Tabella_Mail where " & _
                               "ID = ? ")
            cmd.Parameters.AddWithValue("@ID", Txt_ID.Text)
        Else
            Dim MySql As String
            MySql = "select * from Tabella_Mail where "
            If Txt_Descrizione.Text <> "" Then
                MySql = MySql & " Descrizione Like ?"
                cmd.Parameters.AddWithValue("@Descrizione", Txt_Descrizione.Text & "%")
            Else
                ClientScript.RegisterClientScriptBlock(Me.GetType(), "Errore", "VisualizzaErrore('<center>Specificare un criterio di ricerca</center>');", True)
                Exit Sub
            End If

            cmd.CommandText = MySql
        End If
        cmd.Connection = cn
        Dim sb As StringBuilder = New StringBuilder

        Dim myPOSTreader As OleDbDataReader = cmd.ExecuteReader()
        Do While myPOSTreader.Read

            Dim myrigaT As System.Data.DataRow = Tab_RicercaCorsi.NewRow()
            myrigaT(0) = campodb(myPOSTreader.Item("ID"))
            myrigaT(1) = campodb(myPOSTreader.Item("Data"))
            myrigaT(2) = campodb(myPOSTreader.Item("Descrizione"))
            Tab_RicercaCorsi.Rows.Add(myrigaT)
        Loop
        myPOSTreader.Close()
        cn.Close()

        ViewState("RicercaCorsi") = Tab_RicercaCorsi

        Grd_Visualizzazione.AutoGenerateColumns = True
        Grd_Visualizzazione.DataSource = Tab_RicercaCorsi
        Grd_Visualizzazione.DataBind()

    End Sub

    Function campodb(ByVal oggetto As Object) As String
        If IsDBNull(oggetto) Then
            Return ""
        Else
            Return oggetto
        End If
    End Function

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Trim(Session("UTENTE")) = "" Then
            Response.Redirect("Login.aspx")
            Exit Sub
        End If

        If Page.IsPostBack = True Then Exit Sub



        Dim kBarra As New Cls_BarraSenior

        Lbl_BarraSenior.Text = kBarra.CodiceBarra(Application("SENIOR"), Session("UTENTE"), Page)


        Tab_RicercaCorsi.Clear()
        Tab_RicercaCorsi.Columns.Clear()
        Tab_RicercaCorsi.Columns.Add("ID", GetType(Long))
        Tab_RicercaCorsi.Columns.Add("Data", GetType(String))
        Tab_RicercaCorsi.Columns.Add("Descrizione", GetType(String))



        Dim cn As OleDbConnection



        cn = New Data.OleDb.OleDbConnection(Session("DC_OSPITE"))

        cn.Open()
        Dim cmd As New OleDbCommand()


        cmd.CommandText = ("select TOP 20 * from Tabella_Mail " & _
                           "ORDER BY Data Desc, ID  DESC")

        cmd.Connection = cn
        Dim sb As StringBuilder = New StringBuilder

        Dim myPOSTreader As OleDbDataReader = cmd.ExecuteReader()
        Do While myPOSTreader.Read

            Dim myrigaT As System.Data.DataRow = Tab_RicercaCorsi.NewRow()
            myrigaT(0) = campodb(myPOSTreader.Item("ID"))
            myrigaT(1) = campodb(myPOSTreader.Item("Data"))
            myrigaT(2) = campodb(myPOSTreader.Item("Descrizione"))
            Tab_RicercaCorsi.Rows.Add(myrigaT)
        Loop
        myPOSTreader.Close()
        cn.Close()

        ViewState("RicercaCorsi") = Tab_RicercaCorsi

        Grd_Visualizzazione.AutoGenerateColumns = True
        Grd_Visualizzazione.DataSource = Tab_RicercaCorsi
        Grd_Visualizzazione.DataBind()


    End Sub

    Protected Sub Grd_Visualizzazione_RowCommand(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewCommandEventArgs) Handles Grd_Visualizzazione.RowCommand
        If (e.CommandName = "Select" Or e.CommandName = "Seleziona") Then

            Tab_RicercaCorsi = ViewState("RicercaCorsi")

            If Val(Tab_RicercaCorsi.Rows(e.CommandArgument).Item(0)) > 0 Then
                Response.Redirect("GestioneMail.aspx?ID=" & Tab_RicercaCorsi.Rows(e.CommandArgument).Item(0))
            End If
        End If
    End Sub

    Protected Sub Grd_Visualizzazione_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles Grd_Visualizzazione.SelectedIndexChanged

    End Sub



    Protected Sub Btn_Nuovo_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles Btn_Nuovo.Click
        Response.Redirect("GestioneMail.aspx?ID=0")
    End Sub
End Class
