﻿Imports System.IO
Imports System.Net.Mail
Imports System.Web.Hosting
Imports System.Data.OleDb
Imports System
Imports System.Net
Imports System.Net.Mime
Imports System.Threading
Imports System.ComponentModel

Partial Class MailingList_InvioMail
    Inherits System.Web.UI.Page

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Page.IsPostBack = True Then
            Exit Sub
        End If

        Dim kBarra As New Cls_BarraSenior

        Lbl_BarraSenior.Text = kBarra.CodiceBarra(Application("SENIOR"), Session("UTENTE"), Page)

        Dim k As New Cls_Mail

        k.UpDropDownList(Session("DC_OSPITE"), DD_Mail)

        Dim L As New Cls_Tabella_MailingListTesta

        L.UpDropDownList(Session("DC_OSPITE"), DD_MailingList)
    End Sub

    Protected Sub Btn_Invia_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles Btn_Invia.Click
        If Val(DD_MailingList.SelectedValue) = 0 Then
            ClientScript.RegisterClientScriptBlock(Me.GetType(), "Errore", "VisualizzaErrore('<center>Specificare Mailing List</center>');", True)
            Exit Sub
        End If
        If Val(DD_Mail.SelectedValue) = 0 Then
            ClientScript.RegisterClientScriptBlock(Me.GetType(), "Errore", "VisualizzaErrore('<center>Specificare Mail</center>');", True)
            Exit Sub
        End If

        Session("DD_MailingList") = DD_MailingList.SelectedValue
        Session("DD_Mail") = DD_Mail.SelectedValue
        Response.Redirect("InvioMail_Step2.aspx")

    End Sub

    Protected Sub Btn_Esci_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles Btn_Esci.Click
        Response.Redirect("Menu_MailList.aspx")
    End Sub

End Class
