﻿Imports System.IO
Imports System.Net.Mail
Imports System.Web.Hosting
Imports System.Data.OleDb
Imports System
Imports System.Net
Imports System.Net.Mime
Imports System.Threading
Imports System.ComponentModel
Imports System.Web.Mail


Partial Class MailingList_InvioMail_Step2
    Inherits System.Web.UI.Page
    Dim Tabella As New System.Data.DataTable("tabella")

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Dim XS As New Cls_Login
        Dim I As Integer
        Dim Stampa As New StampeGenerale

        Try
            Tabella.Clear()
            Tabella.Columns.Clear()

            Tabella.Columns.Add("EMail", GetType(String))
            Tabella.Columns.Add("Esito", GetType(String))
            Tabella.Columns.Add("SndEmail", GetType(String))
            Tabella.Columns.Add("SndOggetto", GetType(String))
            Tabella.Columns.Add("SndTesto", GetType(String))
            Tabella.Columns.Add("SndPath", GetType(String))
            Tabella.Columns.Add("SndIdMail", GetType(String))
            Tabella.Columns.Add("SndCodiceOspite", GetType(String))
            Tabella.Columns.Add("SndCodiceParente", GetType(String))
            Tabella.Columns.Add("SndCodicePorvincia", GetType(String))
            Tabella.Columns.Add("SndCodiceComune", GetType(String))
            Tabella.Columns.Add("SndRegione", GetType(String))
            Tabella.Columns.Add("SndCodiceDebitoreCreditore", GetType(String))
            Tabella.Columns.Add("SndCodiceMedico", GetType(String))
            Tabella.Columns.Add("SndIdMailingList", GetType(String))
            Tabella.Columns.Add("SndMail", GetType(String))
            Tabella.Columns.Add("SndCheck", GetType(String))

        Catch ex As Exception

        End Try


        Dim K2 As New Cls_SqlString

        Dim Barra As New Cls_BarraSenior

        If Not IsNothing(Session("RicercaAnagraficaSQLString")) Then
            Try
                K2 = Session("RicercaAnagraficaSQLString")
            Catch ex As Exception
                K2 = Nothing
            End Try
        End If

        Lbl_BarraSenior.Text = Barra.CodiceBarra(Application("SENIOR"), Session("UTENTE"), Page, K2)

        If Page.IsPostBack = True Then Exit Sub

        Dim Kx As New Cls_Login

        Kx.Utente = Session("UTENTE")
        Kx.LeggiSP(Application("SENIOR"))


        Dim k As New Cls_Tabella_MailingListTesta
        Dim IndirizzoMail As String
        Dim Appoggio As String = ""

        k.Id = Session("DD_MailingList")
        k.Leggi(Session("DC_OSPITE"))

        Dim Indice As Integer

        Dim Mail As New Cls_Mail

        Mail.Id = Session("DD_Mail")
        Mail.Leggi(Session("DC_OSPITE"))


        Dim k1 As New Cls_Login
        Dim NomeSocieta As String

        k1.Utente = Session("UTENTE")
        k1.LeggiSP(Application("SENIOR"))

        NomeSocieta = k1.RagioneSociale

        Dim Vettore(100) As String
        Dim IndiceAllegati As Integer = 0
        Dim CognomeNome As String

        Dim Appo As String

        Appo = Dir(HostingEnvironment.ApplicationPhysicalPath() & "\Public\" & NomeSocieta & "_Allegati")
        If Dir(HostingEnvironment.ApplicationPhysicalPath() & "\Public\" & NomeSocieta & "_Allegati", FileAttribute.Directory) = "" Then
            MkDir(HostingEnvironment.ApplicationPhysicalPath() & "\Public\" & NomeSocieta & "_Allegati")
        End If


        If Dir(HostingEnvironment.ApplicationPhysicalPath() & "\Public\" & NomeSocieta & "_Allegati" & "\MAIL_" & Mail.Id, FileAttribute.Directory) = "" Then
            MkDir(HostingEnvironment.ApplicationPhysicalPath() & "\Public\" & NomeSocieta & "_Allegati" & "\MAIL_" & Mail.Id)
        End If


        Dim objDI As New DirectoryInfo(HostingEnvironment.ApplicationPhysicalPath() & "\Public\" & NomeSocieta & "_Allegati" & "\MAIL_" & Mail.Id & "\")
        Dim aryItemsInfo() As FileSystemInfo
        Dim objItem As FileSystemInfo


        aryItemsInfo = objDI.GetFileSystemInfos()
        For Each objItem In aryItemsInfo
            Console.WriteLine(objItem.Name)
            IndiceAllegati = IndiceAllegati + 1
            Vettore(IndiceAllegati) = HostingEnvironment.ApplicationPhysicalPath() & "\Public\" & NomeSocieta & "_Allegati" & "\MAIL_" & Mail.Id & "\" & objItem.Name

        Next

        Session("OGGETTOMAIL") = Mail.Oggetto
        Session("TESTOMAIL") = Mail.Testo
        Session("LOGMAIL") = ""

        Dim Inizio As Integer = 0

        For Indice = 0 To k.Righe.Length - 1
            If Not IsNothing(k.Righe(Indice)) Then
                Inizio = Inizio + 1
                IndirizzoMail = ""
                CognomeNome = ""

                If k.Righe(Indice).CodiceOspite > 0 And k.Righe(Indice).CodiceParente = 0 Then

                    Dim NomeOspiti As New ClsOspite

                    NomeOspiti.CodiceOspite = k.Righe(Indice).CodiceOspite
                    NomeOspiti.Leggi(Session("DC_OSPITE"), NomeOspiti.CodiceOspite)

                    IndirizzoMail = NomeOspiti.RESIDENZATELEFONO3

                    CognomeNome = NomeOspiti.Nome
                End If


                If k.Righe(Indice).CodiceOspite > 0 And k.Righe(Indice).CodiceParente > 0 Then

                    Dim NomeParenti As New Cls_Parenti

                    NomeParenti.CodiceOspite = k.Righe(Indice).CodiceOspite
                    NomeParenti.CodiceParente = k.Righe(Indice).CodiceParente
                    NomeParenti.Leggi(Session("DC_OSPITE"), NomeParenti.CodiceOspite, NomeParenti.CodiceParente)

                    IndirizzoMail = NomeParenti.Telefono3


                    Dim NomeOspiti As New ClsOspite

                    NomeOspiti.CodiceOspite = k.Righe(Indice).CodiceOspite
                    NomeOspiti.Leggi(Session("DC_OSPITE"), NomeOspiti.CodiceOspite)

                    CognomeNome = NomeOspiti.Nome
                End If


                If k.Righe(Indice).CodiceMedico <> "" And Val(k.Righe(Indice).CodiceMedico) <> 0 Then

                    Dim NomeParenti As New Cls_Medici

                    NomeParenti.CodiceMedico = k.Righe(Indice).CodiceMedico
                    NomeParenti.Leggi(Session("DC_OSPITE"))

                    IndirizzoMail = NomeParenti.Telefono2
                End If


                If k.Righe(Indice).CodicePorvincia <> "" And k.Righe(Indice).CodiceComune <> "" Then

                    Dim NomeParenti As New ClsComune

                    NomeParenti.Provincia = k.Righe(Indice).CodicePorvincia
                    NomeParenti.Comune = k.Righe(Indice).CodiceComune
                    NomeParenti.Leggi(Session("DC_OSPITE"))

                    IndirizzoMail = NomeParenti.RESIDENZATELEFONO3
                End If

                If k.Righe(Indice).Regione <> "" Then

                    Dim NomeParenti As New ClsUSL

                    NomeParenti.CodiceRegione = k.Righe(Indice).Regione
                    NomeParenti.Leggi(Session("DC_OSPITE"))

                    IndirizzoMail = NomeParenti.RESIDENZATELEFONO3
                End If


                If k.Righe(Indice).CodiceOspite = 0 And k.Righe(Indice).CodiceParente = 0 And k.Righe(Indice).CodicePorvincia = "" And k.Righe(Indice).CodiceComune = "" And k.Righe(Indice).CodiceMedico = "" And k.Righe(Indice).CodiceDebitoreCreditore = 0 Then
                    IndirizzoMail = k.Righe(Indice).Mail
                End If


                If IndirizzoMail <> "" Then

                    Dim cn As OleDbConnection


                    cn = New Data.OleDb.OleDbConnection(Session("DC_OSPITE"))

                    cn.Open()

                    Dim cmdVerifica As New OleDbCommand()

                    cmdVerifica.Connection = cn
                    cmdVerifica.CommandText = "Select * From Log_MailInviate Where IdMail = ? And  Mail = ? And DataOra >= ? "
                    cmdVerifica.Parameters.AddWithValue("@IdMail", Mail.Id)
                    cmdVerifica.Parameters.AddWithValue("@Mail", IndirizzoMail)
                    cmdVerifica.Parameters.AddWithValue("@DataOra", DateAdd(DateInterval.Hour, 0, DateSerial(Year(Now), Month(Now), Day(Now))))

                    Dim myPOSTreader As OleDbDataReader = cmdVerifica.ExecuteReader()
                    If Not myPOSTreader.Read Then

                        Dim KIndice As Integer
                        Dim MyMAil(1000) As String

                        If InStr(IndirizzoMail, ";") > 0 Then
                            MyMAil = Split(IndirizzoMail, ";")
                            For KIndice = 0 To MyMAil.Length - 1
                                If IsNothing(MyMAil(KIndice)) Then
                                    Exit For
                                End If
                                If InStr(MyMAil(KIndice), "@") > 0 And InStr(MyMAil(KIndice), ".") > 0 Then
                                    Dim myriga As System.Data.DataRow = Tabella.NewRow()

                                    myriga(0) = MyMAil(KIndice)
                                    myriga(1) = ""
                                    myriga(2) = MyMAil(KIndice)
                                    If Not IsNothing(Mail.Oggetto) Then
                                        myriga(3) = campodb(Mail.Oggetto.Replace("@COGNOMENOME", CognomeNome))
                                    End If
                                    If Not IsNothing(Mail.Testo) Then
                                        myriga(4) = campodb(Mail.Testo.Replace("@COGNOMENOME", CognomeNome))
                                    End If
                                    Dim Separa As Integer
                                    Dim AppoggioVettore As String = ""
                                    For Separa = 0 To Vettore.Length - 1
                                        If Not IsNothing(Vettore(Separa)) Then
                                            AppoggioVettore = AppoggioVettore & Vettore(Separa) & ";"
                                        End If
                                    Next
                                    myriga(5) = AppoggioVettore
                                    myriga(6) = Mail.Id
                                    myriga(7) = k.Righe(Indice).CodiceOspite
                                    myriga(8) = k.Righe(Indice).CodiceParente
                                    myriga(9) = k.Righe(Indice).CodicePorvincia
                                    myriga(10) = k.Righe(Indice).CodiceComune
                                    myriga(11) = k.Righe(Indice).Regione
                                    myriga(12) = k.Righe(Indice).CodiceDebitoreCreditore
                                    myriga(13) = k.Righe(Indice).CodiceMedico
                                    myriga(14) = k.Id
                                    myriga(15) = IndirizzoMail
                                    myriga(16) = ""

                                    Tabella.Rows.Add(myriga)
                                End If
                            Next
                        Else
                            Dim myriga As System.Data.DataRow = Tabella.NewRow()

                            myriga(0) = IndirizzoMail
                            myriga(1) = ""
                            myriga(2) = IndirizzoMail
                            If Not IsNothing(Mail.Oggetto) Then
                                myriga(3) = campodb(Mail.Oggetto.Replace("@COGNOMENOME", CognomeNome))
                            End If
                            If Not IsNothing(Mail.Testo) Then
                                myriga(4) = campodb(Mail.Testo.Replace("@COGNOMENOME", CognomeNome))
                            End If
                            Dim Separa As Integer
                            Dim AppoggioVettore As String = ""
                            For Separa = 0 To Vettore.Length - 1
                                If Not IsNothing(Vettore(Separa)) Then
                                    AppoggioVettore = AppoggioVettore & Vettore(Separa) & ";"
                                End If
                            Next
                            myriga(5) = AppoggioVettore
                            myriga(6) = Mail.Id
                            myriga(7) = k.Righe(Indice).CodiceOspite
                            myriga(8) = k.Righe(Indice).CodiceParente
                            myriga(9) = k.Righe(Indice).CodicePorvincia
                            myriga(10) = k.Righe(Indice).CodiceComune
                            myriga(11) = k.Righe(Indice).Regione
                            myriga(12) = k.Righe(Indice).CodiceDebitoreCreditore
                            myriga(13) = k.Righe(Indice).CodiceMedico
                            myriga(14) = k.Id
                            myriga(15) = IndirizzoMail
                            myriga(16) = ""

                            Tabella.Rows.Add(myriga)
                        End If
                    End If

                    cn.Close()
                End If
            End If
        Next



        'SendAnEmail("segreteria@csv-vicenza.org", "segreteria@csv-vicenza.org", Mail.Oggetto, Mail.Testo & Firma.Testo & "<br><b>Report Invio :</b><br/><table>" & Appoggio & "</table>", Vettore)

        'Session("LOGMAIL") = Appoggio

        'Session("OGGETTOMAIL") = Mail.Oggetto
        'Session("TESTOMAIL") = Mail.Testo

        'Response.Redirect("MailInviate.aspx")


        Session("InvioMail") = Tabella

        GridView1.AutoGenerateColumns = False

        GridView1.DataSource = Tabella
        GridView1.DataBind()
        GridView1.Visible = True

        Timer1.Enabled = False
    End Sub


    Public Function SendAnEmail(ByVal MsgFrom As String, ByVal MsgTo As String, ByVal MsgSubject As String, ByVal MsgBody As String, ByVal ParamArray Vetttore() As String) As String
        Dim Kx As New Cls_Login
        Dim mailClient As SmtpClient = Nothing

        Kx.Utente = Session("UTENTE")
        Kx.LeggiSP(Application("SENIOR"))

        MsgFrom = Kx.EMail

        Try

            Using msg As New Net.Mail.MailMessage(MsgFrom, MsgTo, MsgSubject, MsgBody)
                msg.IsBodyHtml = True
                If Kx.Usa_SMTP_Default Then
                    mailClient = New SmtpClient("smtp.sendgrid.net")
                    mailClient.Credentials = New System.Net.NetworkCredential(ConfigurationManager.AppSettings("Senior_DefaultSmtpUser"), ConfigurationManager.AppSettings("Senior_DefaultSmtpPassword"))
                    mailClient.EnableSsl = False
                    mailClient.Port = 587
                Else
                    If Kx.Porta587 = 1 Or Kx.SMTP = "smtp.gmail.com" Or Kx.SMTP = "smtps.aruba.it" Or Kx.SMTP = "smtp.office365.com" Or Kx.SMTP = "ssl0.ovh.net" Or Kx.SMTP = "sicuro.nephila.it" Or Kx.SMTP = "smtps.aruba.it" Then

                        mailClient = New SmtpClient(Kx.SMTP, 587)  '  = local machine IP Address
                        mailClient.EnableSsl = True
                        mailClient.Credentials = New System.Net.NetworkCredential(Kx.UserName, Kx.Passwordsmtp)

                        If Kx.SSL = 1 Then
                            mailClient.EnableSsl = True
                        End If
                    Else
                        mailClient = New SmtpClient(Kx.SMTP)  '  = local machine IP Address
                        mailClient.Credentials = New System.Net.NetworkCredential(Kx.UserName, Kx.Passwordsmtp)
                    End If
                End If
                Dim I As Integer
                For I = 0 To Vetttore.Length - 1
                    If Not IsNothing(Vetttore(I)) Then
                        If Trim(Vetttore(I)) <> "" Then
                            Dim at As New Attachment(Vetttore(I))
                            msg.Attachments.Add(at)
                        End If
                    End If
                Next
                If Not mailClient Is Nothing Then
                    mailClient.Send(msg)
                    Return "OK"
                End If
            End Using

        Catch ex As FormatException
            ' ScriviLog(ex.Message & " :Format Exception")
            Return ex.Message
        Catch ex As SmtpException
            ' ScriviLog(ex.Message & " :SMTP Exception")
            Return ex.Message
        End Try
    End Function

    Function campodb(ByVal oggetto As Object) As String
        If IsDBNull(oggetto) Then
            Return ""
        Else
            Return oggetto
        End If
    End Function

    Protected Sub Btn_InviaMail_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles Btn_InviaMail.Click
        Tabella = Session("InvioMail")

        For Riga = 0 To GridView1.Rows.Count - 1
            Dim CheckBox As CheckBox = DirectCast(GridView1.Rows(Riga).FindControl("ChkInvia"), CheckBox)
            If CheckBox.Checked = True Then
                Tabella.Rows(Riga).Item(14) = "Checked"
            End If
        Next

        lblWaiting.Text = "<center><img src=""../images/loading.gif"" /></center>"

        Session("InvioMail") = Tabella
        Timer1.Interval = 2000
        Timer1.Enabled = True
    End Sub

    Protected Sub Timer1_Tick(ByVal sender As Object, ByVal e As System.EventArgs) Handles Timer1.Tick
        Dim Riga As Integer
        Dim Entratato As Boolean = False
        Dim XS As New Cls_Login
        XS.Utente = Session("UTENTE")
        XS.LeggiSP(Application("SENIOR"))

        Tabella = Session("InvioMail")

        For Riga = 0 To GridView1.Rows.Count - 1
            Dim SenzaEsito As Boolean = True
            If Not IsNothing(Tabella.Rows(Riga).Item(1)) Then
                If campodb(Tabella.Rows(Riga).Item(1)) <> "" Then
                    SenzaEsito = False
                End If
            Else
                SenzaEsito = False
            End If
            If Tabella.Rows(Riga).Item(14) = "Checked" And SenzaEsito = True Then
                Dim Vettore(100) As String

                Vettore = Regex.Split(Tabella.Rows(Riga).Item(5), ";")
                Tabella.Rows(Riga).Item(1) = SendAnEmail(XS.EMail, campodb(Tabella.Rows(Riga).Item(0)), campodb(Tabella.Rows(Riga).Item(3)), campodb(Tabella.Rows(Riga).Item(4)), Vettore)


                Session("LOGMAIL") = Session("LOGMAIL") & "<tr><td>" & Tabella.Rows(Riga).Item(1) & "<td><td>" & campodb(Tabella.Rows(Riga).Item(0)) & "</td><td>" & campodb(Tabella.Rows(Riga).Item(3)) & "</td><td>" & Format(Now, "dd/MM/yyyy hh:mm:ss") & "</td>"

                Dim cn As OleDbConnection


                cn = New Data.OleDb.OleDbConnection(Session("DC_OSPITE"))

                cn.Open()

                Dim cmdIns As New OleDbCommand()


                cmdIns.CommandText = ("Insert Into Log_MailInviate ([IdMail],[CodiceOspite],[CodiceParente],[CodicePorvincia],[CodiceComune],[Regione],[CodiceDebitoreCreditore],[CodiceMedico],[IdMailingList],[Mail],[DataOra],[Utente],[Esito],[Oggetto],[CorpoMail],[MailInvio])  VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?) ")
                cmdIns.Parameters.AddWithValue("@IdMail", Val(Tabella.Rows(Riga).Item(6)))
                cmdIns.Parameters.AddWithValue("@CodiceOspite", Val(Tabella.Rows(Riga).Item(7)))
                cmdIns.Parameters.AddWithValue("@CodiceParente", Val(Tabella.Rows(Riga).Item(8)))
                cmdIns.Parameters.AddWithValue("@CodicePorvincia", Tabella.Rows(Riga).Item(9))
                cmdIns.Parameters.AddWithValue("@CodiceComune", Tabella.Rows(Riga).Item(10))
                cmdIns.Parameters.AddWithValue("@Regione", Tabella.Rows(Riga).Item(11))
                cmdIns.Parameters.AddWithValue("@CodiceDebitoreCreditore", Tabella.Rows(Riga).Item(12))
                cmdIns.Parameters.AddWithValue("@CodiceMedico", Tabella.Rows(Riga).Item(13))
                cmdIns.Parameters.AddWithValue("@IdMailingList", Val(Tabella.Rows(Riga).Item(14)))
                cmdIns.Parameters.AddWithValue("@Mail", Tabella.Rows(Riga).Item(15))
                cmdIns.Parameters.AddWithValue("@DataOra", Now)
                cmdIns.Parameters.AddWithValue("@Utente", Session("UTENTE"))
                cmdIns.Parameters.AddWithValue("@Esito", Tabella.Rows(Riga).Item(1))
                cmdIns.Parameters.AddWithValue("@Oggetto", Tabella.Rows(Riga).Item(3))
                cmdIns.Parameters.AddWithValue("@CorpoMail", Tabella.Rows(Riga).Item(4))
                cmdIns.Parameters.AddWithValue("@MailInvio", XS.EMail)
                cmdIns.Connection = cn
                cmdIns.ExecuteNonQuery()

                cn.Close()

                Entratato = True

                GridView1.DataSource = Tabella
                GridView1.DataBind()
                Session("InvioMail") = Tabella
                Exit For
            End If
        Next
        If Not Entratato Then
            Response.Redirect("MailInviate.aspx")

            lblWaiting.Text = ""
            Timer1.Enabled = False
        End If

    End Sub

    Protected Sub Btn_Seleziona_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles Btn_Seleziona.Click
        For Riga = 0 To GridView1.Rows.Count - 1
            Dim CheckBox As CheckBox = DirectCast(GridView1.Rows(Riga).FindControl("ChkInvia"), CheckBox)
            CheckBox.Checked = True
        Next
    End Sub

    Protected Sub Btn_Esci_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles Btn_Esci.Click
        Response.Redirect("InvioMail.aspx")
    End Sub
End Class

