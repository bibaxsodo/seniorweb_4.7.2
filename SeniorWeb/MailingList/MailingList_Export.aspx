﻿<%@ Page Language="VB" AutoEventWireup="false" Inherits="MailingList_MailingList_Export" CodeFile="MailingList_Export.aspx.vb" %>

<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
    <title>Export</title>
    <asp:PlaceHolder runat="server">
        <%: System.Web.Optimization.Styles.Render("~/Content/AjaxControlToolkit/Styles/Bundle") %>
    </asp:PlaceHolder>
    <link rel="stylesheet" href="css/csv.css?Versione=8" type="text/css" />
</head>
<body>
    <form id="form1" runat="server">
        <asp:ScriptManager ID="ScriptManager1" runat="server" EnableScriptGlobalization="true">
            <Scripts>
                <asp:ScriptReference Path="~/Scripts/AjaxControlToolkit/Bundle" />
            </Scripts>
        </asp:ScriptManager>
        <div>
            <div class="Titolo">CSV - Dati Exportati</div>
            <div class="SottoTitolo">
                <br />
                <br />
            </div>
            <asp:GridView ID="Grd_Visualizzazione" runat="server" CellPadding="3"
                Width="100%" BackColor="White" BorderColor="#999999" BorderStyle="None"
                BorderWidth="1px" GridLines="Vertical">
                <Columns>
                </Columns>
                <FooterStyle BackColor="#CCCCCC" ForeColor="Black" />
                <PagerStyle BackColor="#999999" ForeColor="Black" HorizontalAlign="Center" />
                <SelectedRowStyle BackColor="#008A8C" Font-Bold="True" ForeColor="White" />
                <HeaderStyle BackColor="#565151" Font-Bold="False" Font-Size="Small"
                    ForeColor="White" />
                <AlternatingRowStyle BackColor="Gainsboro" />
            </asp:GridView>
        </div>
    </form>
</body>
</html>
