﻿<%@ Page Language="VB" AutoEventWireup="false" Inherits="MailingList_InvioMail" CodeFile="InvioMail.aspx.vb" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit.HTMLEditor"
    TagPrefix="cc2" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="AJAX" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
    <title>Invio Mail</title>
    <asp:PlaceHolder runat="server">
        <%: System.Web.Optimization.Styles.Render("~/Content/AjaxControlToolkit/Styles/Bundle") %>
    </asp:PlaceHolder>
    <link rel="stylesheet" href="ospiti.css?ver=11" type="text/css" />
    <link rel="shortcut icon" href="../images/SENIOR.ico" />
    <link href="js/jquery.autocomplete.css" rel="stylesheet" type="text/css" />
    <script src="js/jquery-1.5.1.min.js" type="text/javascript"></script>
    <script src="js/jquery.maskedinput-1.3.min.js" type="text/javascript"></script>
    <script src="js/jquery.autocomplete.js" type="text/javascript"></script>
    <script src="/js/formatnumer.js" type="text/javascript"></script>
    <script src="js/JSErrore.js" type="text/javascript"></script>
    <script src="js/NoEnter.js" type="text/javascript"></script>
    <style>
        th {
            font-weight: normal;
        }
    </style>
    <script type="text/javascript">
        $(document).ready(function () {
            if (window.innerHeight > 0) { $("#BarraLaterale").css("height", (window.innerHeight - 94) + "px"); } else { $("#BarraLaterale").css("height", (document.documentElement.offsetHeight - 94) + "px"); }
        });
    </script>
</head>
<body>
    <form id="form1" runat="server">
        <asp:ScriptManager ID="ScriptManager1" runat="server" EnableScriptGlobalization="true">
            <Scripts>
                <asp:ScriptReference Path="~/Scripts/AjaxControlToolkit/Bundle" />
            </Scripts>
        </asp:ScriptManager>
        <asp:Label ID="Lbl_BarraSenior" runat="server" Text=""></asp:Label>
        <div>
            <asp:Button ID="Btn_RefreshPostali" runat="server" Text="" Visible="false" />
            <table style="width: 100%;" cellpadding="0" cellspacing="0">
                <tr>
                    <td style="width: 140px; background-color: #F0F0F0;"></td>
                    <td>
                        <div class="Titolo">Mailing List - Invio Mail</div>
                        <div class="SottoTitolo">
                            <br />
                            <br />
                        </div>
                    </td>
                    <td style="text-align: right; vertical-align: top;">
                        <div class="DivTasti">
                            <asp:ImageButton ID="Btn_Invia" Height="38px" runat="server" ImageUrl="~/images/sendmail.png" ToolTip="Invia Mail" />&nbsp;                     
                        </div>
                    </td>
                </tr>

                <tr>
                    <td style="width: 140px; background-color: #F0F0F0; vertical-align: top; text-align: center;" id="BarraLaterale">
                        <asp:ImageButton ID="Btn_Esci" runat="server" ImageUrl="../images/Menu_Indietro.png" />
                    </td>
                    <td colspan="2" style="background-color: #FFFFFF; vertical-align: top;">
                        <AJAX:TabContainer ID="TabContainer1" runat="server" ActiveTabIndex="0" Height="665px" Width="100%" CssClass="TabSenior">
                            <AJAX:TabPanel runat="server" HeaderText="Invio Mail" ID="TabPanel1">
                                <ContentTemplate>
                                    <br />
                                    <label style="display: block; float: left; width: 150px;">Mail :<font color="red">*</font></label>
                                    <asp:DropDownList ID="DD_Mail" runat="server"></asp:DropDownList>
                                    <br />
                                    <br />
                                    <br />
                                    <label style="display: block; float: left; width: 150px;">Mailing list :<font color="red">*</font></label>
                                    <asp:DropDownList ID="DD_MailingList" runat="server"></asp:DropDownList>
                                    <br />
                                    <br />
                                    <asp:CheckBoxList ID="Chk_Lista" runat="server">
                                    </asp:CheckBoxList>
                                    <br />
                                    <br />
                                    <br />

                                </ContentTemplate>
                            </AJAX:TabPanel>

                        </AJAX:TabContainer>
                    </td>
                </tr>
            </table>


        </div>
    </form>
</body>
</html>
