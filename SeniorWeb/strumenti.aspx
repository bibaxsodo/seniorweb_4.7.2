﻿<%@ Page Language="VB" EnableEventValidation="False" AutoEventWireup="false" Inherits="strumenti" CodeFile="strumenti.aspx.vb" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="xasp" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
    <title>Impostazioni</title>
    <asp:PlaceHolder runat="server">
        <%: System.Web.Optimization.Styles.Render("~/Content/AjaxControlToolkit/Styles/Bundle") %>
    </asp:PlaceHolder>
    <link href="Content/bootstrap.min.css" rel="stylesheet" />
    <script src="js/JSErrore.js" type="text/javascript"></script>
    <link href="ospiti.css?ver=10" rel="stylesheet" type="text/css" />
    <link rel="shortcut icon" href="images/SENIOR.ico" />
    <script src="/js/formatnumer.js" type="text/javascript"></script>
    <script src="js/JSErrore.js" type="text/javascript"></script>
    <script src="js/jquery-1.5.1.min.js" type="text/javascript"></script>
    <script type="text/javascript">     
        $(document).ready(function () {
            if (window.innerHeight > 0) { $("#BarraLaterale").css("height", (window.innerHeight - 94) + "px"); } else { $("#BarraLaterale").css("height", (document.documentElement.offsetHeight - 94) + "px"); }
        });
    </script>
</head>
<body>
    <form id="form1" runat="server">
        <asp:ScriptManager ID="ScriptManager1" runat="server" EnableScriptGlobalization="true">
            <Scripts>
                <asp:ScriptReference Path="~/Scripts/AjaxControlToolkit/Bundle" />
            </Scripts>
        </asp:ScriptManager>
        <asp:Label ID="Lbl_BarraSenior" runat="server" Text=""></asp:Label>
        <div>
            <table style="width: 100%;" cellpadding="0" cellspacing="0">
                <tr>
                    <td style="width: 160px; background-color: #F0F0F0;"></td>
                    <td>
                        <h1 class="Titolo">Impostazioni</h1>
                        <div class="SottoTitoloOSPITE">
                            <asp:Label ID="Lbl_NomeOspite" runat="server"></asp:Label><br />
                            <br />
                        </div>
                    </td>
                    <td style="text-align: right;">
                        <div class="DivTasti">
                            <asp:ImageButton runat="server" ImageUrl="~/images/salva.jpg" class="EffettoBottoniTondi" Height="38px" ToolTip="Modifica" ID="BTN_Modifica"></asp:ImageButton>
                        </div>
                    </td>
                </tr>
                <tr>
                    <td style="width: 160px; background-color: #F0F0F0; vertical-align: top; text-align: center;" id="BarraLaterale">
                        <a href="MainMenu.aspx" style="border-width: 0px;">
                            <img src="images/Home.jpg" alt="Menù" class="Effetto" /></a><br />
                        <asp:ImageButton ID="Btn_Esci" runat="server" BackColor="Transparent" ImageUrl="images/Menu_Indietro.png" ToolTip="Chiudi" /><br />
                        <br />
                        <label class="MenuDestra">&nbsp;&nbsp;<a href="Ospitiweb/UpDateComuni.aspx">Up Date Comuni</a></label>
                        <br />
                        <label class="MenuDestra">&nbsp;&nbsp;<a href="SqlDatabase/UpDateSqlDb.aspx">Up Date DB</a></label>
                        <br />
                        <label class="MenuDestra">&nbsp;&nbsp;<asp:Label runat="server" ID="lbl_RidXML" Text="Import Rid"></asp:Label></label>
                    </td>
                    <td colspan="2" style="background-color: #FFFFFF;" valign="top">
                        <xasp:TabContainer ID="TabContainer1" runat="server" ActiveTabIndex="0" CssClass="TabSenior" Width="100%" BorderStyle="None" Style="margin-right: 39px">
                            <xasp:TabPanel runat="server" HeaderText="Strumenti" ID="Tab_Anagrafica">
                                <HeaderTemplate>
                                    Dati Personali                 
                                </HeaderTemplate>
                                <ContentTemplate>
                                    <p>
                                        <label class="LabelCampo">Indirizzo Mail :</label>
                                        <asp:TextBox ID="Txt_EMailUtente" runat="server" Height="28px" Width="232px" Visible="true"></asp:TextBox><br />
                                        <br />
                                        <asp:Button ID="Btn_ModificaEMail" CssClass="SeniorButton" runat="server" Text="Conferma Mail" Width="232px" Visible="true" />
                                    </p>
                                    <br />
                                    <p>
                                        <label class="LabelCampo">Nuova Password :</label>
                                        <asp:TextBox ID="Txt_Password" TextMode="Password" runat="server" Height="28px" Width="132px" Visible="true"></asp:TextBox>
                                        (Lunghezza minima 6 caratteri)
            <br />
                                        <br />
                                        <label class="LabelCampo">Conferma Password :</label>
                                        <asp:TextBox ID="Txt_Password2" TextMode="Password" runat="server" Height="28px" Width="132px" Visible="true"></asp:TextBox><br />
                                        <!--(Lunghezza Minima 6 caratteri)<br /> -->
                                        <br />
                                        <br />
                                        <asp:Button ID="btn_DatiPersonali_ModificaPassword" CssClass="SeniorButton" runat="server" Text="Modifica Password" Width="232px" Visible="true" />
                                    </p>
                                    <p>
                                        <asp:Label ID="Lbl_Errori" runat="server" ForeColor="Red" Width="472px"></asp:Label>&nbsp;
                                    </p>
                                    <p>
                                    </p>
                                </ContentTemplate>
                            </xasp:TabPanel>
                            <xasp:TabPanel runat="server" HeaderText="Report Ospiti" ID="Tb_Ospiti">
                                <HeaderTemplate>
                                    Report Ospiti                 
                                </HeaderTemplate>
                                <ContentTemplate>
                                    <asp:GridView ID="Grid" runat="server" CellPadding="4" ForeColor="#333333" GridLines="None"
                                        Height="160px" ShowFooter="True" Width="912px">
                                        <RowStyle BackColor="#F7F6F3" ForeColor="#333333" />
                                        <Columns>
                                            <asp:CommandField ButtonType="Image" CancelImageUrl="~/images/annulla.png" EditImageUrl="~/images/modifica.png"
                                                ShowEditButton="True" UpdateImageUrl="~/images/aggiorna.png" />
                                            <asp:TemplateField HeaderText="Report">
                                                <EditItemTemplate>
                                                    <asp:Label ID="LblReport" runat="server" Text='<%# Eval("Report") %>' Width="136px"></asp:Label>
                                                </EditItemTemplate>
                                                <ItemTemplate>
                                                    <asp:Label ID="LblReport" runat="server" Text='<%# Eval("Report") %>' Width="136px"></asp:Label>
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                            <asp:TemplateField HeaderText="Personalizzazione">
                                                <EditItemTemplate>
                                                    <asp:TextBox ID="TxtPersonalizzazione" runat="server" Width="300px"></asp:TextBox>
                                                </EditItemTemplate>
                                                <ItemTemplate>
                                                    <asp:Label ID="LblPersonalizzazione" runat="server" Text='<%# Eval("Personalizzazione") %>' Width="136px"></asp:Label>
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                        </Columns>
                                        <FooterStyle BackColor="#5D7B9D" Font-Bold="True" ForeColor="White" />
                                        <PagerStyle BackColor="#284775" ForeColor="White" HorizontalAlign="Center" />
                                        <SelectedRowStyle BackColor="#E2DED6" Font-Bold="True" ForeColor="#333333" />
                                        <HeaderStyle BackColor="#5D7B9D" Font-Bold="True" ForeColor="White" />
                                        <EditRowStyle BackColor="#999999" />
                                        <AlternatingRowStyle BackColor="White" ForeColor="#284775" />
                                    </asp:GridView>
                                </ContentTemplate>
                            </xasp:TabPanel>
                            <xasp:TabPanel runat="server" HeaderText="Report Ospiti" ID="TabPanel1">
                                <HeaderTemplate>
                                    Report Generale                 
                                </HeaderTemplate>
                                <ContentTemplate>
                                    <asp:GridView ID="GridGenerale" runat="server" CellPadding="4" ForeColor="#333333" GridLines="None"
                                        Height="160px" ShowFooter="True" Width="912px">
                                        <RowStyle BackColor="#F7F6F3" ForeColor="#333333" />
                                        <Columns>
                                            <asp:CommandField ButtonType="Image" CancelImageUrl="~/images/annulla.png" EditImageUrl="~/images/modifica.png"
                                                ShowEditButton="True" UpdateImageUrl="~/images/aggiorna.png" />
                                            <asp:TemplateField HeaderText="Report">
                                                <EditItemTemplate>
                                                    <asp:Label ID="LblReport" runat="server" Text='<%# Eval("Report") %>' Width="136px"></asp:Label>
                                                </EditItemTemplate>
                                                <ItemTemplate>
                                                    <asp:Label ID="LblReport" runat="server" Text='<%# Eval("Report") %>' Width="136px"></asp:Label>
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                            <asp:TemplateField HeaderText="Personalizzazione">
                                                <EditItemTemplate>
                                                    <asp:TextBox ID="TxtPersonalizzazione" runat="server" Width="300px"></asp:TextBox>
                                                </EditItemTemplate>
                                                <ItemTemplate>
                                                    <asp:Label ID="LblPersonalizzazione" runat="server" Text='<%# Eval("Personalizzazione") %>' Width="136px"></asp:Label>
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                        </Columns>
                                        <FooterStyle BackColor="#5D7B9D" Font-Bold="True" ForeColor="White" />
                                        <PagerStyle BackColor="#284775" ForeColor="White" HorizontalAlign="Center" />
                                        <SelectedRowStyle BackColor="#E2DED6" Font-Bold="True" ForeColor="#333333" />
                                        <HeaderStyle BackColor="#5D7B9D" Font-Bold="True" ForeColor="White" />
                                        <EditRowStyle BackColor="#999999" />
                                        <AlternatingRowStyle BackColor="White" ForeColor="#284775" />
                                    </asp:GridView>
                                </ContentTemplate>
                            </xasp:TabPanel>
                            <xasp:TabPanel runat="server" HeaderText="Dati Societa" ID="TabPanel3">
                                <HeaderTemplate>
                                    Dati Societa
                                </HeaderTemplate>
                                <ContentTemplate>
                                    <label class="LabelCampo">Ragione Sociale: </label>
                                    <asp:TextBox ID="Txt_RagioneSociale" runat="server" Width="350px" MaxLength="100"></asp:TextBox><br />
                                    <br />
                                    <label class="LabelCampo">Indirizzo: </label>
                                    <asp:TextBox ID="Txt_Indirizzo" runat="server" Width="300px" MaxLength="50"></asp:TextBox><br />
                                    <br />
                                    <label class="LabelCampo">Cap: </label>
                                    <asp:TextBox ID="Txt_Cap" runat="server" Width="100px" onkeypress="return soloNumeri(event);"></asp:TextBox><br />
                                    <br />
                                    <label class="LabelCampo">Localita : </label>
                                    <asp:TextBox ID="Txt_Localita" runat="server" Width="250px" MaxLength="50"></asp:TextBox><br />
                                    <br />
                                    <label class="LabelCampo">Provincia: </label>
                                    <asp:TextBox ID="Txt_Provincia" runat="server" Width="30px" MaxLength="2"></asp:TextBox><br />
                                    <br />
                                    <label class="LabelCampo">Codice Fiscale: </label>
                                    <asp:TextBox ID="Txt_CodiceFiscale" runat="server" Width="100px" MaxLength="16"></asp:TextBox><br />
                                    <br />
                                    <label class="LabelCampo">Partita Iva: </label>
                                    <asp:TextBox ID="Txt_PartitaIva" runat="server" Width="100px" onkeypress="return soloNumeri(event);"></asp:TextBox><br />
                                    <br />
                                    <label class="LabelCampo">Telefono : </label>
                                    <asp:TextBox ID="Txt_Telefono" runat="server" Width="150px" MaxLength="15"></asp:TextBox><br />
                                    <br />
                                    <label class="LabelCampo">E-Mail : </label>
                                    <asp:TextBox ID="Txt_EMailStruttura" runat="server" Width="150px" MaxLength="50"></asp:TextBox><br />
                                    <br />
                                    <label class="LabelCampo">Banca : </label>
                                    <asp:TextBox ID="Txt_Banca" runat="server" Width="250px" MaxLength="50"></asp:TextBox><br />
                                    <br />
                                    <label class="LabelCampo">Iban : </label>
                                    <asp:TextBox ID="Txt_Iban" runat="server" Width="220px" MaxLength="27"></asp:TextBox><br />
                                    <br />
                                    <hr />
                                    <label class="LabelCampo">BOLLO VIRTUALE : </label>
                                    <asp:CheckBox ID="Chk_BolloVirtuale" runat="server" Text="" />
                                    <br />
                                    <br />
                                    <label class="LabelCampo">REGIME FISCALE : </label>
                                    <asp:DropDownList ID="DD_RegimeFiscale" runat="server">
                                        <asp:ListItem Value="RF01" Text="Regime Ordinario"></asp:ListItem>
                                        <asp:ListItem Value="RF02" Text="Regime Contribuenti minimi (art.1, c.96-117, L.244/07)"></asp:ListItem>
                                        <asp:ListItem Value="RF04" Text="Regime Agricoltura e attività connesse e pesca (art.34 e 34-bis, D.P.R. 633/72)"></asp:ListItem>
                                        <asp:ListItem Value="RF05" Text="Regime Vendita sali e tabacchi(art.74, c.1, D.P.R. 633/72)"></asp:ListItem>
                                        <asp:ListItem Value="RF06" Text="Regime Commercio fiammiferi (art.74, c.1, D.P.R. 633/72)"></asp:ListItem>
                                        <asp:ListItem Value="RF07" Text="Regime Editoria (art.74, c.1, D.P.R. 633/72)"></asp:ListItem>
                                        <asp:ListItem Value="RF08" Text="Regime Gestione servizi telefonia pubblica (art.74, c.1, D.P.R. 633/72)"></asp:ListItem>
                                        <asp:ListItem Value="RF09" Text="Regime Rivendita documenti di trasporto pubblico e di sosta (art.74, c.1, D.P.R. 633/72)"></asp:ListItem>
                                        <asp:ListItem Value="RF10" Text="Regime Intrattenimenti, giochi e altre attività di cui alla tariffa allegata al DPR 640/72 (art.74, c.6, D.P.R. 633/72)"></asp:ListItem>
                                        <asp:ListItem Value="RF11" Text="Regime Agenzie viaggi e turismo (art.74-ter, D.P.R. 633/72)"></asp:ListItem>
                                        <asp:ListItem Value="RF12" Text="Regime Agriturismo (art.5, c.2, L. 413/91)"></asp:ListItem>
                                        <asp:ListItem Value="RF13" Text="Regime Vendite a domicilio (art.25-bis, c.6, D.P.R. 600/73)"></asp:ListItem>

                                        <asp:ListItem Value="RF14" Text="Regime Rivendita beni usati, oggetti d'arte, d'antiquariato o da collezione (art.36, D.L. 41/95)"></asp:ListItem>
                                        <asp:ListItem Value="RF15" Text="Regime Agenzie di vendite all'asta di oggetti d'arte, antiquariato o da collezione (art.40-bis, D.L. 41/95)"></asp:ListItem>
                                        <asp:ListItem Value="RF16" Text="IVA per cassa P.A. (art. 6, c.5, D.P.R. 633/1972)"></asp:ListItem>
                                        <asp:ListItem Value="RF17" Text="IVA per cassa (art. 32-bis, D.L. 83/2012)"></asp:ListItem>
                                        <asp:ListItem Value="RF18" Text="Altro"></asp:ListItem>
                                        <asp:ListItem Value="RF19" Text="Regime forfettario"></asp:ListItem>
                                        <asp:ListItem Value="" Text="Seleziona"></asp:ListItem>
                                    </asp:DropDownList>
                                    <br />
                                    <br />

                                    <hr />

                                    <table style="width: 100%;">
                                        <tr>
                                            <td style="width: 50%; text-align: center;"><b>RID</b></td>
                                            <td style="width: 50%; text-align: center;"><b>MAV</b></td>
                                        </tr>
                                        <tr>
                                            <td>
                                                <label class="LabelCampo">Iban Rid : </label>
                                                <asp:TextBox ID="Txt_IBANRID" runat="server" Width="220px" MaxLength="27"></asp:TextBox><br />
                                                <br />
                                                <label class="LabelCampo">End To End Id : </label>
                                                <asp:TextBox ID="Txt_EndToEndId" runat="server" Width="220px" MaxLength="27"></asp:TextBox><i>(Facoltativo)</i><br />
                                                <br />

                                                <label class="LabelCampo">Prvt Id : </label>
                                                <asp:TextBox ID="Txt_PrvtId" runat="server" Width="220px" MaxLength="27"></asp:TextBox><i>(Identificativo creditore es. IT320010000003053920165)</i><br />
                                                <br />

                                                <label class="LabelCampo">Mmb Id : </label>
                                                <asp:TextBox ID="Txt_MmbId" runat="server" Width="220px" MaxLength="27"></asp:TextBox><i>(Codice ABI )</i><br />
                                                <br />

                                                <label class="LabelCampo">OrgId : </label>
                                                <asp:TextBox ID="Txt_OrgId" runat="server" Width="220px" MaxLength="27"></asp:TextBox><i>(Codice CUC azienda mittente)</i><br />
                                                <br />
                                            </td>
                                            <td>

                                                <label class="LabelCampo">Codice Mittente : </label>
                                                <asp:TextBox ID="Txt_CodiceMittente" runat="server" Width="220px" MaxLength="27"></asp:TextBox><br />
                                                <br />

                                                <label class="LabelCampo">Codice Ricevente : </label>
                                                <asp:TextBox ID="Txt_CodiceRicevente" runat="server" Width="220px" MaxLength="27"></asp:TextBox><br />
                                                <br />

                                                <label class="LabelCampo">Codice ABI : </label>
                                                <asp:TextBox ID="Txt_CodiceABi" runat="server" Width="220px" MaxLength="27"></asp:TextBox><br />
                                                <br />

                                                <label class="LabelCampo">Codice CAB : </label>
                                                <asp:TextBox ID="Txt_CodiceCab" runat="server" Width="220px" MaxLength="27"></asp:TextBox><br />
                                                <br />

                                                <label class="LabelCampo">Conto : </label>
                                                <asp:TextBox ID="Txt_CodiceConto" runat="server" Width="220px" MaxLength="27"></asp:TextBox><br />
                                                <br />


                                            </td>
                                        </tr>
                                    </table>

                                    <hr />

                                    <label class="LabelCampo">Codice Installazione : </label>
                                    <asp:TextBox ID="Txt_CodiceInstallazione" runat="server" Width="220px" MaxLength="20"></asp:TextBox><br />
                                    <br />
                                    <label class="LabelCampo">Codice Ditta : </label>
                                    <asp:TextBox ID="Txt_CodiceDitta" runat="server" Width="220px" MaxLength="20"></asp:TextBox><br />
                                    <br />
                                    <label class="LabelCampo">Nome File : </label>
                                    <asp:TextBox ID="Txt_NomeFile" runat="server" Width="220px" MaxLength="50"></asp:TextBox><br />
                                    <br />

                                </ContentTemplate>
                            </xasp:TabPanel>
                            <xasp:TabPanel runat="server" HeaderText="Report Ospiti" ID="TabPanel4">
                                <HeaderTemplate>
                                    Mailing List
                                </HeaderTemplate>
                                <ContentTemplate>
                                    <asp:UpdatePanel ID="UpdatePanel1" runat="server">
                                        <ContentTemplate>
                                            <label style="display: block; float: left; width: 150px;">E Mail :</label>
                                            <asp:TextBox ID="Txt_EMail" runat="server" Height="28px"></asp:TextBox>
                                            <br />
                                            <br />
                                            <label style="display: block; float: left; width: 150px;">Server SMTP:</label>
                                            <asp:TextBox ID="Txt_Smtp" runat="server" Height="28px"></asp:TextBox>
                                            <br />
                                            <br />
                                            <label style="display: block; float: left; width: 150px;">User Name :</label>
                                            <asp:TextBox ID="Txt_UserName" runat="server" Height="28px"></asp:TextBox>
                                            <br />
                                            <br />
                                            <label style="display: block; float: left; width: 150px;">Password :</label>
                                            <asp:TextBox ID="Txt_PasswordSmtp" TextMode="Password" Height="28px" runat="server"></asp:TextBox>
                                            <br />
                                            <br />
                                            <label style="display: block; float: left; width: 150px;">Porta :</label>
                                            <asp:CheckBox ID="Chk_Porta587" runat="server" Text="Porta 587" />
                                            <br />
                                            <br />
                                            <label style="display: block; float: left; width: 150px;">Uso SSL :</label>
                                            <asp:CheckBox ID="Chk_UsoSSl" runat="server" Text="" />
                                            <br />
                                            <br />
                                            <asp:CheckBox ID="chk_UsaServerDefault" runat="server" Text="Usa il server di invio di Senior" AutoPostBack="true" OnCheckedChanged="chk_UsaServerDefault_CheckedChanged" />
                                            <br />
                                            <br />
                                            <asp:Button ID="btn_SaveMailSettings" CssClass="SeniorButton" runat="server" Text="Conferma" Width="228px" />
                                            <br />
                                        </ContentTemplate>
                                    </asp:UpdatePanel>
                                </ContentTemplate>
                            </xasp:TabPanel>


                        </xasp:TabContainer>
                    </td>
                </tr>
            </table>
        </div>
    </form>
    <script src="Scripts/bootstrap.bundle.min.js"></script>
</body>
</html>
