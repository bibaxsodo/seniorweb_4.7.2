﻿<%@ Page Language="VB" AutoEventWireup="false" Inherits="MainMenu" CodeFile="MainMenu.aspx.vb" %>

<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
    <title>Menu Senior</title>
    <asp:PlaceHolder runat="server">
        <%: System.Web.Optimization.Styles.Render("~/Content/AjaxControlToolkit/Styles/Bundle") %>
    </asp:PlaceHolder>
    <link rel="stylesheet" href="ospiti.css?ver=11" type="text/css" />
    <link rel="shortcut icon" href="images/SENIOR.ico" />
    <link href="js/jquery.autocomplete.css" rel="stylesheet" type="text/css" />

    <script src="js/jquery-1.5.1.min.js" type="text/javascript"></script>
    <script src="js/jquery.autocomplete.js" type="text/javascript"></script>
    <script src="js/jquery.maskedinput-1.3.min.js" type="text/javascript"></script>
    <script src="js/NoEnter.js" type="text/javascript"></script>
    <script type="text/javascript">
        function removeElement(divNum) {
            var d = document.getElementById(divNum).parentNode;
            var d2 = document.getElementById(divNum);
            d.removeChild(d2);
        }
    </script>
    <script type="text/javascript">
        $(document).ready(function () {
            if (window.innerHeight > 0) { $("#BarraLaterale").css("height", (window.innerHeight - 105) + "px"); } else { $("#BarraLaterale").css("height", (document.documentElement.offsetHeight - 105) + "px"); }
        });
    </script>
</head>
<body>
    <form id="form1" runat="server">
        <asp:ScriptManager ID="ScriptManager1" runat="server" EnableScriptGlobalization="true">
            <Scripts>
                <asp:ScriptReference Path="~/Scripts/AjaxControlToolkit/Bundle" />
            </Scripts>
        </asp:ScriptManager>
        <div>
            <asp:Label ID="Lbl_BarraSenior" runat="server" Text=""></asp:Label>

            <table width="100%" cellpadding="0" cellspacing="0">
                <tr>
                    <td style="width: 160px; background-color: #F0F0F0;"></td>
                    <td>
                        <div class="Titolo">Menu principale</div>
                        <div class="SottoTitolo">
                            <br />
                            <br />
                        </div>
                    </td>
                    <td style="text-align: right; vertical-align: top;"></td>
                </tr>
                <tr>



                    <td style="width: 160px; background-color: #F0F0F0; text-align: center; vertical-align: top;" id="BarraLaterale">
                        <asp:ImageButton ID="Btn_Home" runat="server" BackColor="Transparent" class="Effetto" ImageUrl="images/Menu_Indietro.png" ToolTip="Home" />
                    </td>
                    <td colspan="2" style="vertical-align: top;">
                        <table>
                            <tr>
                                <td style="text-align: center; width: 150px; vertical-align: top;" id="ColomOspiti">
                                    <a href="OspitiWeb/Menu_Ospiti.aspx">
                                        <img src="images/Menu_Ospiti.jpg" alt="GESTIONE OSPITI" id="ospitiimg" class="Effetto" style="border-width: 0;"></a>
                                </td>
                                <td style="text-align: center; width: 150px;" id="ColomGenerale">
                                    <a href="GeneraleWeb/Menu_Generale.aspx">
                                        <img src="images/Contabilita.jpg" alt="CONTABILITA' GENERALE" id="generaleimg" class="Effetto" style="border-width: 0;"></a>
                                </td>
                                <td style="text-align: center; width: 150px;" id="ColomMailinglist">
                                    <a href="MailingList/Menu_MailList.aspx">
                                        <img src="images/Mail.jpg" alt="MAILING LIST" class="Effetto" id="mailinglistimg" style="border-width: 0;"></a>
                                </td>
                                <td style="text-align: center; width: 150px;" id="ColomComunicazioni">
                                    <a href="Comunicazioni/Menu_Comunicazioni.aspx">
                                        <img src="images/Menu_Contratto.jpg" alt="Comunicazioni" class="Effetto" id="comunicazioniimg" style="border-width: 0;"></a>
                                </td>
                                <td style="text-align: center; width: 150px;" id="ColomProtocollo">
                                    <a href="Protocollo/Menu_Protocollo.aspx">
                                        <img src="images/Menu_protocollo.jpg" alt="GESTIONE PROTOCOLLO" id="protocolloimg" class="Effetto" style="border-width: 0;"></a>
                                </td>
                                <td style="text-align: center; width: 150px;" id="ColomAppalti">
                                    <a href="Appalti/Menu_Appalti.aspx">
                                        <img src="images/Menu_Appalti.jpg" alt="Commesse" class="Effetto" id="appaltiimg" style="border-width: 0;"></a>
                                </td>
                                <td style="text-align: center; width: 150px;" id="ColomAmbulatoriale">
                                    <a href="Ambulatoriale/Menu_Ambulatoriale.aspx">
                                        <img src="images/Menu_InserimentoPrenotazione.png" alt="Ambulatoriale" class="Effetto" id="Ambulatorialeimg" style="border-width: 0;"></a>
                                </td>
                                <td style="text-align: center; width: 150px;" id="ColomRicoveri">
                                    <a href="Ricoveri/Menu_Ricoveri.aspx">
                                        <img src="images/Menu_InserimentoPrenotazione.png" alt="Ricoveri" class="Effetto" id="Ricoveriimg" style="border-width: 0;"></a>
                                </td>
                            </tr>
                            <tr>
                                <td style="text-align: center; vertical-align: top;"><span class="MenuText" id="ospititext">OSPITI</span></td>
                                <td style="text-align: center; vertical-align: top;"><span class="MenuText" id="generaletext">GENERALE</span></td>
                                <td style="text-align: center; vertical-align: top;"><span class="MenuText" id="mailinglisttext">MAILING LIST</span></td>
                                <td style="text-align: center; vertical-align: top;"><span class="MenuText" id="comunicazionitext">COMUNICAZIONI</span></td>
                                <td style="text-align: center; vertical-align: top;"><span class="MenuText" id="protocollotext">PROTOCOLLO</span></td>
                                <td style="text-align: center; vertical-align: top;"><span class="MenuText" id="appaltitext">COMMESSE</span></td>
                                <td style="text-align: center; vertical-align: top;"><span class="MenuText" id="Ambulatorialetext">AMBULATORIALE</span></td>
                                <td style="text-align: center; vertical-align: top;"><span class="MenuText" id="Ricoveritext">RICOVERI</span></td>
                            </tr>

                            <tr>
                                <td>
                                    <asp:Label ID="Lbl_PostIT" runat="server" Text=""></asp:Label></td>
                                <td></td>
                            </tr>


                            <tr>
                                <td style="text-align: center;">&nbsp;</td>
                                <td style="text-align: center;">&nbsp;</td>
                                <td style="text-align: center;">&nbsp;</td>
                                <td style="text-align: center;">&nbsp;</td>
                                <td style="text-align: center;"></td>
                            </tr>
                            <tr>
                                <td style="text-align: center; vertical-align: top;">&nbsp;</td>
                                <td style="text-align: center; vertical-align: top;">&nbsp;</td>
                                <td style="text-align: center; vertical-align: top;">&nbsp;</td>
                                <td style="text-align: center; vertical-align: top;">&nbsp;</td>
                                <td style="text-align: center; vertical-align: top;"><span class="MenuText"></span></td>
                            </tr>

                            <tr>
                                <td></td>
                                <td></td>
                            </tr>

                            <tr>
                                <td style="text-align: center;"></td>
                                <td style="text-align: center;"></td>
                                <td style="text-align: center;"></td>
                                <td style="text-align: center;"></td>
                                <td style="text-align: center;"></td>
                            </tr>

                            <tr>
                                <td style="text-align: center; vertical-align: top;"><span class="MenuText"></span></td>
                                <td style="text-align: center; vertical-align: top;"><span class="MenuText"></span></td>
                                <td style="text-align: center; vertical-align: top;"><span class="MenuText"></span></td>
                                <td style="text-align: center; vertical-align: top;"><span class="MenuText"></span></td>
                                <td style="text-align: center; vertical-align: top;"><span class="MenuText"></span></td>
                            </tr>

                        </table>

                    </td>
                </tr>

                <tr>
                    <td style="width: 160px; background-color: #F0F0F0;">&nbsp;<br />
                        &nbsp;<br />
                        &nbsp;<br />
                        &nbsp;<br />
                        &nbsp;<br />
                        &nbsp;<br />
                        &nbsp;<br />
                        &nbsp;<br />
                        &nbsp;<br />
                    </td>
                    <td></td>
                    <td></td>
                </tr>
            </table>


        </div>
    </form>
</body>
</html>
