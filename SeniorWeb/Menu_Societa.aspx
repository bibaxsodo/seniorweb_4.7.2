﻿<%@ Page Language="VB" AutoEventWireup="false" Inherits="Menu_Societa" CodeFile="Menu_Societa.aspx.vb" %>

<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
    <title>Menu Senior</title>
    <asp:PlaceHolder runat="server">
        <%: System.Web.Optimization.Styles.Render("~/Content/AjaxControlToolkit/Styles/Bundle") %>
    </asp:PlaceHolder>
    <link rel="stylesheet" href="ospiti.css?ver=11" type="text/css" />
    <link href="js/jquery.autocomplete.css" rel="stylesheet" type="text/css" />

    <script src="js/jquery-1.5.1.min.js" type="text/javascript"></script>
    <script src="js/jquery.autocomplete.js" type="text/javascript"></script>
    <script src="js/jquery.maskedinput-1.3.min.js" type="text/javascript"></script>
    <script src="js/NoEnter.js" type="text/javascript"></script>
    <script type="text/javascript">
        function removeElement(divNum) {
            var d = document.getElementById(divNum).parentNode;
            var d2 = document.getElementById(divNum);
            d.removeChild(d2);
        }
    </script>
    <script type="text/javascript">
        $(document).ready(function () {
            if (window.innerHeight > 0) { $("#BarraLaterale").css("height", (window.innerHeight - 105) + "px"); } else { $("#BarraLaterale").css("height", (document.documentElement.offsetHeight - 105) + "px"); }
        });
    </script>
</head>
<body>
    <form id="form1" runat="server">
        <asp:ScriptManager ID="ScriptManager1" runat="server" EnableScriptGlobalization="true">
            <Scripts>
                <asp:ScriptReference Path="~/Scripts/AjaxControlToolkit/Bundle" />
            </Scripts>
        </asp:ScriptManager>
        <div>
            <asp:Label ID="Lbl_BarraSenior" runat="server" Text=""></asp:Label>

            <table width="100%" cellpadding="0" cellspacing="0">
                <tr>
                    <td style="width: 160px; background-color: #F0F0F0;"></td>
                    <td>
                        <div class="Titolo">Menu Senior</div>
                        <div class="SottoTitolo">
                            <br />
                            <br />
                        </div>
                    </td>
                    <td style="text-align: right; vertical-align: top;"></td>
                </tr>
                <tr>



                    <td style="width: 160px; background-color: #F0F0F0; text-align: center; vertical-align: top;" id="BarraLaterale">
                        <a href="Login.aspx" style="border-width: 0px;">
                            <img src="images/Menu_Indietro.png" class="Effetto" alt="Menù" /></a><br />
                    </td>
                    <td colspan="2" style="vertical-align: top;">
                        <table>
                            <tr>
                                <td style="text-align: center; width: 150px; vertical-align: top;">
                                    <asp:ImageButton ID="IB_SOCIETA1" runat="server" ImageUrl="images/Menu_Villa1.jpg" class="Effetto" Visible="false" />
                                </td>
                                <td style="text-align: center; width: 150px;">
                                    <asp:ImageButton ID="IB_SOCIETA2" runat="server" ImageUrl="images/Menu_Villa2.jpg" class="Effetto" Visible="false" />
                                </td>
                                <td style="text-align: center; width: 150px;">
                                    <asp:ImageButton ID="IB_SOCIETA3" runat="server" ImageUrl="images/Menu_Villa3.jpg" class="Effetto" Visible="false" />
                                </td>
                                <td style="text-align: center; width: 112px;">
                                    <asp:ImageButton ID="IB_SOCIETA4" runat="server" ImageUrl="images/Menu_Villa4.jpg" class="Effetto" Visible="false" />
                                </td>
                                <td style="text-align: center; width: 112px;">
                                    <asp:ImageButton ID="IB_SOCIETA5" runat="server" ImageUrl="images/Menu_Villa5.jpg" class="Effetto" Visible="false" />
                                </td>
                            </tr>
                            <tr>
                                <td style="text-align: center; vertical-align: top;"><span class="MenuText">
                                    <asp:Label ID="Lbl_Testo1" runat="server" Text=""></asp:Label></span></td>
                                <td style="text-align: center; vertical-align: top;"><span class="MenuText">
                                    <asp:Label ID="Lbl_Testo2" runat="server" Text=""></asp:Label></span></td>
                                <td style="text-align: center; vertical-align: top;"><span class="MenuText">
                                    <asp:Label ID="Lbl_Testo3" runat="server" Text=""></asp:Label></span></td>
                                <td style="text-align: center; vertical-align: top;"><span class="MenuText">
                                    <asp:Label ID="Lbl_Testo4" runat="server" Text=""></asp:Label></span></td>
                                <td style="text-align: center; vertical-align: top;"><span class="MenuText">
                                    <asp:Label ID="Lbl_Testo5" runat="server" Text=""></asp:Label></span></td>
                            </tr>

                            <tr>
                                <td style="text-align: center; width: 150px; vertical-align: top;">
                                    <asp:ImageButton ID="IB_SOCIETA6" runat="server" ImageUrl="images/Menu_Villa1.jpg" class="Effetto" Visible="false" />
                                </td>
                                <td style="text-align: center; width: 150px;">
                                    <asp:ImageButton ID="IB_SOCIETA7" runat="server" ImageUrl="images/Menu_Villa2.jpg" class="Effetto" Visible="false" />
                                </td>
                                <td style="text-align: center; width: 150px;">
                                    <asp:ImageButton ID="IB_SOCIETA8" runat="server" ImageUrl="images/Menu_Villa3.jpg" class="Effetto" Visible="false" />
                                </td>
                                <td style="text-align: center; width: 112px;">
                                    <asp:ImageButton ID="IB_SOCIETA9" runat="server" ImageUrl="images/Menu_Villa4.jpg" class="Effetto" Visible="false" />
                                </td>
                                <td style="text-align: center; width: 112px;">
                                    <asp:ImageButton ID="IB_SOCIETA10" runat="server" ImageUrl="images/Menu_Villa5.jpg" class="Effetto" Visible="false" />
                                </td>
                            </tr>
                            <tr>
                                <td style="text-align: center; vertical-align: top;"><span class="MenuText">
                                    <asp:Label ID="Lbl_Testo6" runat="server" Text=""></asp:Label></span></td>
                                <td style="text-align: center; vertical-align: top;"><span class="MenuText">
                                    <asp:Label ID="Lbl_Testo7" runat="server" Text=""></asp:Label></span></td>
                                <td style="text-align: center; vertical-align: top;"><span class="MenuText">
                                    <asp:Label ID="Lbl_Testo8" runat="server" Text=""></asp:Label></span></td>
                                <td style="text-align: center; vertical-align: top;"><span class="MenuText">
                                    <asp:Label ID="Lbl_Testo9" runat="server" Text=""></asp:Label></span></td>
                                <td style="text-align: center; vertical-align: top;"><span class="MenuText">
                                    <asp:Label ID="Lbl_Testo10" runat="server" Text=""></asp:Label></span></td>
                            </tr>


                            <tr>
                                <td style="text-align: center; width: 150px; vertical-align: top;">
                                    <asp:ImageButton ID="IB_SOCIETA11" runat="server" ImageUrl="images/Menu_Villa1.jpg" class="Effetto" Visible="false" />
                                </td>
                                <td style="text-align: center; width: 150px;">
                                    <asp:ImageButton ID="IB_SOCIETA12" runat="server" ImageUrl="images/Menu_Villa2.jpg" class="Effetto" Visible="false" />
                                </td>
                                <td style="text-align: center; width: 150px;">
                                    <asp:ImageButton ID="IB_SOCIETA13" runat="server" ImageUrl="images/Menu_Villa3.jpg" class="Effetto" Visible="false" />
                                </td>
                                <td style="text-align: center; width: 112px;">
                                    <asp:ImageButton ID="IB_SOCIETA14" runat="server" ImageUrl="images/Menu_Villa4.jpg" class="Effetto" Visible="false" />
                                </td>
                                <td style="text-align: center; width: 112px;">
                                    <asp:ImageButton ID="IB_SOCIETA15" runat="server" ImageUrl="images/Menu_Villa5.jpg" class="Effetto" Visible="false" />
                                </td>
                            </tr>
                            <tr>
                                <td style="text-align: center; vertical-align: top;"><span class="MenuText">
                                    <asp:Label ID="Lbl_Testo11" runat="server" Text=""></asp:Label></span></td>
                                <td style="text-align: center; vertical-align: top;"><span class="MenuText">
                                    <asp:Label ID="Lbl_Testo12" runat="server" Text=""></asp:Label></span></td>
                                <td style="text-align: center; vertical-align: top;"><span class="MenuText">
                                    <asp:Label ID="Lbl_Testo13" runat="server" Text=""></asp:Label></span></td>
                                <td style="text-align: center; vertical-align: top;"><span class="MenuText">
                                    <asp:Label ID="Lbl_Testo14" runat="server" Text=""></asp:Label></span></td>
                                <td style="text-align: center; vertical-align: top;"><span class="MenuText">
                                    <asp:Label ID="Lbl_Testo15" runat="server" Text=""></asp:Label></span></td>
                            </tr>



                            <tr>
                                <td style="text-align: center; width: 150px; vertical-align: top;">
                                    <asp:ImageButton ID="IB_SOCIETA16" runat="server" ImageUrl="images/Menu_Villa1.jpg" class="Effetto" Visible="false" />
                                </td>
                                <td style="text-align: center; width: 150px;">
                                    <asp:ImageButton ID="IB_SOCIETA17" runat="server" ImageUrl="images/Menu_Villa2.jpg" class="Effetto" Visible="false" />
                                </td>
                                <td style="text-align: center; width: 150px;">
                                    <asp:ImageButton ID="IB_SOCIETA18" runat="server" ImageUrl="images/Menu_Villa3.jpg" class="Effetto" Visible="false" />
                                </td>
                                <td style="text-align: center; width: 112px;">
                                    <asp:ImageButton ID="IB_SOCIETA19" runat="server" ImageUrl="images/Menu_Villa4.jpg" class="Effetto" Visible="false" />
                                </td>
                                <td style="text-align: center; width: 112px;">
                                    <asp:ImageButton ID="IB_SOCIETA20" runat="server" ImageUrl="images/Menu_Villa5.jpg" class="Effetto" Visible="false" />
                                </td>
                            </tr>
                            <tr>
                                <td style="text-align: center; vertical-align: top;"><span class="MenuText">
                                    <asp:Label ID="Lbl_Testo16" runat="server" Text=""></asp:Label></span></td>
                                <td style="text-align: center; vertical-align: top;"><span class="MenuText">
                                    <asp:Label ID="Lbl_Testo17" runat="server" Text=""></asp:Label></span></td>
                                <td style="text-align: center; vertical-align: top;"><span class="MenuText">
                                    <asp:Label ID="Lbl_Testo18" runat="server" Text=""></asp:Label></span></td>
                                <td style="text-align: center; vertical-align: top;"><span class="MenuText">
                                    <asp:Label ID="Lbl_Testo19" runat="server" Text=""></asp:Label></span></td>
                                <td style="text-align: center; vertical-align: top;"><span class="MenuText">
                                    <asp:Label ID="Lbl_Testo20" runat="server" Text=""></asp:Label></span></td>
                            </tr>
                        </table>

                    </td>
                </tr>

                <tr>
                    <td style="width: 160px; background-color: #F0F0F0;">&nbsp;<br />
                        &nbsp;<br />
                        &nbsp;<br />
                        &nbsp;<br />
                        &nbsp;<br />
                        &nbsp;<br />
                        &nbsp;<br />
                        &nbsp;<br />
                        &nbsp;<br />
                    </td>
                    <td></td>
                    <td></td>
                </tr>
            </table>


        </div>
    </form>
</body>
</html>
