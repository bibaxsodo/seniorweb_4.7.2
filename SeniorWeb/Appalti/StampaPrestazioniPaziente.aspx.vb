﻿Imports System.Web.Hosting
Imports System.Diagnostics
Imports System.ComponentModel
Imports System.Threading
Imports System.Data.OleDb
Imports SeniorWebApplication

Partial Class Appalti_StampaPrestazioniPaziente
    Inherits System.Web.UI.Page


    Dim Festivita(100) As MeseGiorno

    Structure MeseGiorno
        Public Mese As Integer
        Public Giorno As Integer
    End Structure

    Private Sub CaricaFestivita(ByVal Anno As Integer)
        Dim cn As OleDbConnection
        Dim Indice As Integer = 0
        cn = New Data.OleDb.OleDbConnection(Session("DC_TABELLE"))
        cn.Open()

        Dim cmd As New OleDbCommand()
        cmd.CommandText = "Select * From Appalti_Festivita Where  (Anno = 0 or Anno Is Null)"
        cmd.Connection = cn
        Dim myPOSTreader As OleDbDataReader = cmd.ExecuteReader()
        Do While myPOSTreader.Read
            Festivita(Indice).Giorno = campodbN(myPOSTreader.Item("Giorno"))
            Festivita(Indice).Mese = campodbN(myPOSTreader.Item("Mese"))

            Indice = Indice + 1
        Loop
        myPOSTreader.Close()


        Dim cmdAnno As New OleDbCommand()
        cmdAnno.CommandText = "Select * From Appalti_Festivita Where  Anno = ? "
        cmdAnno.Connection = cn
        cmdAnno.Parameters.AddWithValue("@Anno", Anno)
        Dim ReadAnno As OleDbDataReader = cmdAnno.ExecuteReader()
        Do While ReadAnno.Read
            Festivita(Indice).Giorno = campodbN(myPOSTreader.Item("Giorno"))
            Festivita(Indice).Mese = campodbN(myPOSTreader.Item("Mese"))

            Indice = Indice + 1
        Loop
        ReadAnno.Close()

        cn.Close()

    End Sub
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Trim(Session("UTENTE")) = "" Then
            Response.Redirect("../Login.aspx")
            Exit Sub
        End If

        If Page.IsPostBack = True Then Exit Sub

        Dim ConnessioneOspiti As String

        Dim K1 As New Cls_SqlString

        Dim Barra As New Cls_BarraSenior

        If Not IsNothing(Session("RicercaAnagraficaSQLString")) Then
            Try
                K1 = Session("RicercaAnagraficaSQLString")
            Catch ex As Exception
                K1 = Nothing
            End Try
        End If

        Lbl_BarraSenior.Text = Barra.CodiceBarra(Application("SENIOR"), Session("UTENTE"), Page, K1)

        ConnessioneOspiti = Session("DC_OSPITE")

        Dim f As New Cls_Parametri


        f.LeggiParametri(ConnessioneOspiti)
        Dd_Mese.SelectedValue = f.MeseFatturazione
        Txt_Anno.Text = f.AnnoFatturazione

        Session("CampoWr") = ""
        Session("CampoErrori") = ""
        Call EseguiJS()


        Dim LS As New Cls_AppaltiAnagrafica


        LS.UpDateDropBox(Session("DC_TABELLE"), DD_Appalto)

        Dim XS As New Cls_Login

        XS.Utente = Session("UTENTE")
        XS.LeggiSP(Application("SENIOR"))

        CaricaFestivita(Txt_Anno.Text)
    End Sub



    Private Sub EseguiJS()
        Dim MyJs As String
        MyJs = "$(document).ready(function() {"

        MyJs = MyJs & "var els = document.getElementsByTagName(""*"");"

        MyJs = MyJs & "for (var i=0;i<els.length;i++)"
        MyJs = MyJs & "if ( els[i].id ) { "
        MyJs = MyJs & " var appoggio =els[i].id; "
        MyJs = MyJs & " if (appoggio.match('Txt_Data')!= null) {  "
        MyJs = MyJs & " $(els[i]).mask(""99/99/9999"");"

        MyJs = MyJs & " $(els[i]).datepicker({ changeMonth: true,changeYear: true,  yearRange: ""1990:" & Year(Now) + 5 & """},$.datepicker.regional[""it""]);"
        MyJs = MyJs & "    }"
        MyJs = MyJs & " if (appoggio.match('Txt_Scadenza')!= null) {  "
        MyJs = MyJs & " $(els[i]).mask(""99/99/9999"");"

        MyJs = MyJs & " $(els[i]).datepicker({ changeMonth: true,changeYear: true,  yearRange: ""1990:" & Year(Now) + 5 & """},$.datepicker.regional[""it""]);"
        MyJs = MyJs & "    }"



        MyJs = MyJs & "} "
        MyJs = MyJs & "});"

        'MyJs = "$(document).ready(function() { $('.myClass').keypress(function() { handleEnter($('.myClass').val(), true, true); } );     $('#" & Txt_ImportoPagato.ClientID & "').blur(function() { var ap = formatNumber ($('#" & Txt_ImportoPagato.ClientID & "').val(),2); $('#" & Txt_ImportoPagato.ClientID & "').val(ap); }); });"
        ScriptManager.RegisterStartupScript(Me, Me.GetType(), "visualizzaRitIm", MyJs, True)
    End Sub


    Protected Sub Btn_Esci_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles Btn_Esci.Click
        Response.Redirect("Menu_Appalti.aspx")
    End Sub

    Protected Sub ImageButton1_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles ImageButton1.Click
        ElaboraXsd()


        ScriptManager.RegisterClientScriptBlock(Me, Me.GetType(), "Stampa4", "openPopUp('Stampa_ReportXSD.aspx?REPORT=PRESTAZIONEPAZIENTE','Stampe4','width=800,height=600');", True)
    End Sub



    Private Sub ElaboraXsd()
        Dim ConnessioneTabelle As String

        ConnessioneTabelle = Session("DC_TABELLE")

        Dim cn As OleDbConnection

        cn = New Data.OleDb.OleDbConnection(ConnessioneTabelle)
        cn.Open()


        Dim Imposta As Double = 0
        Dim cmd As New OleDbCommand()

        If Val(DD_Appalto.SelectedValue) = 0 Then
            cmd.CommandText = "Select *  From Appalti_PrestazioniElaborate Where year(Data) = ? And month(Data) = ? And CalcoloPassivo = 0 "
        Else
            cmd.CommandText = "Select *  From Appalti_PrestazioniElaborate Where year(Data) = ? And month(Data) = ? And IdAppalto = " & DD_Appalto.SelectedValue & "  And CalcoloPassivo = 0 "
        End If

        cmd.Connection = cn
        cmd.Parameters.AddWithValue("@Anno", Val(Txt_Anno.Text))
        cmd.Parameters.AddWithValue("@Mese", Val(Dd_Mese.SelectedValue))

        Dim Stampa As New Cls_Appalti_AppaltiStrutture

        Dim Scheda As New AppaltiXSD
        Dim myPOSTreader As OleDbDataReader = cmd.ExecuteReader()
        Do While myPOSTreader.Read
            Dim Riga As System.Data.DataRow = Scheda.Tables("PrestazioniPazienti").NewRow

            Dim Ospite As New ClsOspite

            Ospite.CodiceOspite = campodbN(myPOSTreader.Item("CodiceOspite"))
            Ospite.Leggi(Session("DC_OSPITE"), Ospite.CodiceOspite)

            Dim Operatore As New Cls_Operatore

            Operatore.CodiceMedico = campodb(myPOSTreader.Item("CodiceOperatore"))

            Operatore.Leggi(Session("DC_OSPITE"))

            Dim TipologiaOperatore As New Cls_TipoOperatore

            TipologiaOperatore.Codice = Operatore.TipoOperatore
            TipologiaOperatore.Leggi(Session("DC_OSPITE"), TipologiaOperatore.Codice)





            Riga.Item("DataPresaInCarico") = Ospite.DataAccreditoDeposito 'Data Presa in Carico
            Riga.Item("DataAccesso") = campodbD(myPOSTreader.Item("Data"))


            If TipologiaOperatore.Descrizione = "" Then
                Riga.Item("FiguraProfessionale") = "Infermiere"
            Else
                Riga.Item("FiguraProfessionale") = TipologiaOperatore.Descrizione
            End If


            Riga.Item("Importo") = campodbN(myPOSTreader.Item("Imponibile"))

            Riga.Item("Durata") = campodbN(myPOSTreader.Item("Ore"))

            Dim Mansione As New Cls_AppaltiTabellaMansione

            Mansione.ID = campodbN(myPOSTreader.Item("IdMansione"))
            Mansione.Leggi(Session("DC_TABELLE"), Mansione.ID)

            Riga.Item("TipoAccesso") = Mansione.Descrizione
            If campodbN(myPOSTreader.Item("Done")) = 2 Then
                Riga.Item("TipoAccesso") = Mansione.Descrizione & " (non eseguito)"
            End If


            If GiornFestivo(campodbD(myPOSTreader.Item("Data"))) = 0 Then

                Riga.Item("TipoGiorno") = "Feriale"
            Else
                Riga.Item("TipoGiorno") = "Festivo"
            End If

            Riga.Item("IdAccesso") = campodbN(myPOSTreader.Item("Id"))


            Riga.Item("NomePaziente") = Ospite.Nome
            Riga.Item("DataNascita") = Ospite.DataNascita

            Dim Comune As New ClsComune
            Comune.Descrizione = ""
            Comune.Provincia = Ospite.RESIDENZAPROVINCIA1
            Comune.Comune = Ospite.RESIDENZACOMUNE1
            Comune.Leggi(Session("DC_OSPITE"))

            Riga.Item("ResidenzaComune") = Comune.Descrizione

            Riga.Item("ResidenzaIndirizzo") = Ospite.RESIDENZAINDIRIZZO1

            Riga.Item("ResidenzaCap") = Ospite.RESIDENZACAP1



            Scheda.Tables("PrestazioniPazienti").Rows.Add(Riga)
        Loop
        cn.Close()

        Session("stampa") = Scheda
    End Sub

    Function GiornFestivo(ByVal Data As Date) As Integer
        Dim Mese As Integer = Month(Data)
        Dim Giorno As Integer = Day(Data)
        Dim Indice As Integer = 0

        If Data.DayOfWeek = DayOfWeek.Sunday Then Return 1 : Exit Function


        For Indice = 0 To 100
            If Festivita(Indice).Mese = Mese And Festivita(Indice).Giorno = Giorno Then
                Return 1
                Exit Function
            End If
        Next

        Return 0
    End Function

    Function campodb(ByVal oggetto As Object) As String
        If IsDBNull(oggetto) Then
            Return ""
        Else
            Return oggetto
        End If
    End Function

    Function campodbN(ByVal oggetto As Object) As Double
        If IsDBNull(oggetto) Then
            Return 0
        Else
            Return oggetto
        End If
    End Function

    Function campodbD(ByVal oggetto As Object) As Date
        If IsDBNull(oggetto) Then
            Return Nothing
        Else
            Return oggetto
        End If
    End Function


End Class
