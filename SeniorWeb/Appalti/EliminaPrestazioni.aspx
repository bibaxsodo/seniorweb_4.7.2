﻿<%@ Page Language="VB" AutoEventWireup="false" Inherits="Appalti_EliminaPrestazioni" CodeFile="EliminaPrestazioni.aspx.vb" %>


<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="xasp" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
    <meta http-equiv="x-ua-compatible" content="IE=9" />
    <title>Elimina Prestazioni</title>
    <asp:PlaceHolder runat="server">
        <%: System.Web.Optimization.Styles.Render("~/Content/AjaxControlToolkit/Styles/Bundle") %>
    </asp:PlaceHolder>
    <link href="ospiti.css?ver=11" rel="stylesheet" type="text/css" />
    <link rel="shortcut icon" href="images/SENIOR.ico" />
    <script src="/js/formatnumer.js" type="text/javascript"></script>
    <script src="js/jquery-1.5.1.min.js" type="text/javascript"></script>
    <link href="js/jquery.autocomplete.css" rel="stylesheet" type="text/css" />
    <script src="js/jquery.autocomplete.js" type="text/javascript"></script>

    <script src="js/chosen/chosen.jquery.js" type="text/javascript"></script>
    <script src="js/chosen/docsupport/prism.js" type="text/javascript" charset="utf-8"></script>
    <link rel="stylesheet" href="js/chosen/docsupport/style.css">
    <link rel="stylesheet" href="js/chosen/docsupport/prism.css">
    <link rel="stylesheet" href="js/chosen/chosen.css">
    <style>
        .chosen-container-single .chosen-single {
            height: 30px;
            background: white;
            color: Black;
            font-family: "Cuprum","Roboto","Calibri",Lucida Grande,"Helvetica Neue",Helvetica,Arial,sans-serif;
            font-size: 16px;
        }

        .chosen-container {
            font-family: "Cuprum","Roboto","Calibri",Lucida Grande,"Helvetica Neue",Helvetica,Arial,sans-serif;
            font-size: 16px;
        }
    </style>
    <script type="text/javascript">
        var GlbCodiceOperatore = 0;
        var GlbGiorno = 0;
        var GlbMese = 0;
        var GlbAnno = 0;
        var GlbAppalto = 0;
        var GlbStruttura = 0;


        $(document).ready(function () {
            if (window.innerHeight > 0) { $("#BarraLaterale").css("height", (window.innerHeight - 94) + "px"); } else { $("#BarraLaterale").css("height", (document.documentElement.offsetHeight - 94) + "px"); }
        });


    </script>
    <style>
        .wait {
            border: solid 1px #2d2d2d;
            text-align: left;
            vertical-align: top;
            background: white;
            width: 850px;
            height: 350px;
            position: absolute;
            top: 290px;
            left: 20%;
            -moz-border-radius: 5px;
            -webkit-border-radius: 5px;
            border-radius: 5px;
            box-shadow: 0px 0px 10px 4px rgba(0, 125, 196, 0.75);
            -moz-box-shadow: 0px 0px 10px 4px rgba(0, 125, 196, 0.75);
            -webkit-box-shadow: 0px 0px 10px 4px rgba(0, 125, 196, 0.75);
            z-index: 133;
        }

        #blur {
            width: 100%;
            background-color: black;
            moz-opacity: 0.5;
            khtml-opacity: .5;
            opacity: .5;
            filter: alpha(opacity=50);
            z-index: 120;
            height: 100%;
            position: absolute;
            top: 0;
            left: 0;
        }

        .Cella {
            border-style: solid;
            border-width: 1px;
            width: 45px;
            height: 70px;
            font-size: small;
            font-weight: bold;
        }

        .tabella {
            border-style: solid;
            border-width: 1px;
        }
    </style>
</head>
<body>
    <form id="form1" runat="server">
        <asp:ScriptManager ID="ScriptManager1" runat="server" AsyncPostBackTimeout="900" EnableScriptGlobalization="true">
            <Scripts>
                <asp:ScriptReference Path="~/Scripts/AjaxControlToolkit/Bundle" />
            </Scripts>
        </asp:ScriptManager>
        <asp:Label ID="Lbl_BarraSenior" runat="server" Text=""></asp:Label>
        <div>
            <table style="width: 100%;" cellpadding="0" cellspacing="0">
                <tr>
                    <td style="width: 160px; background-color: #F0F0F0;"></td>
                    <td>
                        <div class="Titolo">Appalti - Elimina Prestazioni</div>
                        <div class="SottoTitolo">
                            <br />
                            <br />
                        </div>
                    </td>
                    <td style="text-align: right; vertical-align: top;">

                        <span class="BenvenutoText">Benvenuto <%=Session("UTENTE")%>&nbsp;&nbsp;&nbsp;&nbsp;</span>

                    </td>
                </tr>
                <tr>
                    <td style="width: 160px; background-color: #F0F0F0;"></td>
                    <td colspan="2" style="border: 2px solid #9C9C9C; background-color: #DCDCDC;">
                        <table width="100%">
                            <tr>
                                <td align="left">Commessa
                                <asp:DropDownList ID="DD_Appalto" AutoPostBack="true" runat="server" CssClass="chosen-select" Width="300px"></asp:DropDownList>
                                </td>
                                <td align="left">Struttura
                                <asp:DropDownList ID="DD_Struttura" runat="server" CssClass="chosen-select" Width="300px"></asp:DropDownList>
                                </td>
                                <td align="left">Cooperativa
                                <asp:DropDownList ID="DD_Cooperativa" runat="server" CssClass="chosen-select" Width="300px"></asp:DropDownList>
                                </td>
                                <td align="left">Anno :
     <asp:TextBox ID="Txt_Anno" onkeypress="return soloNumeri(event);" runat="server" Width="64px"></asp:TextBox>
                                </td>
                                <td align="left">Mese :
      <asp:DropDownList ID="Dd_Mese" runat="server" Width="128px">
          <asp:ListItem Value="1">Gennaio</asp:ListItem>
          <asp:ListItem Value="2">Febbraio</asp:ListItem>
          <asp:ListItem Value="3">Marzo</asp:ListItem>
          <asp:ListItem Value="4">Aprile</asp:ListItem>
          <asp:ListItem Value="5">Maggio</asp:ListItem>
          <asp:ListItem Value="6">Giugno</asp:ListItem>
          <asp:ListItem Value="7">Luglio</asp:ListItem>
          <asp:ListItem Value="8">Agosto</asp:ListItem>
          <asp:ListItem Value="9">Settembre</asp:ListItem>
          <asp:ListItem Value="10">Ottobre</asp:ListItem>
          <asp:ListItem Value="11">Novembre</asp:ListItem>
          <asp:ListItem Value="12">Dicembre</asp:ListItem>
          <asp:ListItem Value="" Selected></asp:ListItem>
      </asp:DropDownList>
                                </td>
                                <td align="right">
                                    <asp:ImageButton ID="Img_EliminaMese" runat="server" BackColor="Transparent" ImageUrl="~/images/elimina.jpg" OnClientClick="return window.confirm('Eliminare?');" class="EffettoBottoniTondi" />
                                </td>
                            </tr>
                        </table>

                    </td>
                </tr>

                <tr>
                    <td style="width: 160px; background-color: #F0F0F0; vertical-align: top; text-align: center;" id="BarraLaterale">
                        <a href="Menu_Appalti.aspx" style="border-width: 0px;">
                            <img src="images/Home.jpg" alt="Menù" class="Effetto" /></a><br />
                        <asp:ImageButton ID="Btn_Esci" runat="server" BackColor="Transparent" ImageUrl="../images/Menu_Indietro.png" class="Effetto" ToolTip="Chiudi" />
                    </td>
                    <td colspan="2" style="background-color: #FFFFFF;" valign="top">
                        <asp:Label ID="Lbl_Sinottico" runat="server" Text=""></asp:Label>
                        <br />
                        <div style="height: 40px; vertical-align: middle;">
                            <div style="float: left; padding-top: 7px;">
                                <asp:DropDownList ID="DD_Operatore" runat="server" class="chosen-select" Visible="false"></asp:DropDownList>
                            </div>
                            <div>
                                <asp:ImageButton ID="BtnAddOperatore" runat="server" ImageUrl="~/images/inserisci.png" Visible="false" />
                            </div>
                        </div>
                        <br />

                        <div id="blur" style="visibility: hidden;">&nbsp;</div>
                        <div id="IdModificaOre" style="visibility: hidden;" class="wait">
                            <img src="../images/close.png" style="padding-left: 95%; top: 2px;" onclick="Close();" /><br />
                            <iframe src="CellaGriglia.aspx" id="IdIframe" style="border-width: 0px; width: 850px; height: 350px;"></iframe>
                        </div>

                    </td>
                </tr>
            </table>

        </div>
    </form>
</body>
</html>
