﻿Imports Microsoft.VisualBasic
Imports System.Data.Odbc
Imports System.Web.Hosting
Imports System.Data.OleDb
Imports System.IO
Imports System.Text
Imports System.Data


Partial Class Appalti_ImportCus
    Inherits System.Web.UI.Page
    Dim Tabella As New System.Data.DataTable("tabella")
    Private Function ImportFileEpersonam(ByVal Appoggio As String) As String
        Dim cn As OdbcConnection
        Dim Riga As Integer

        Dim NomeSocieta As String


        If FileUpload1.FileName = "" Then
            ClientScript.RegisterClientScriptBlock(Me.GetType(), "Errore", "VisualizzaErrore('File non indicato');", True)
            Exit Function
        End If

        Dim k As New Cls_Login

        k.Utente = Session("UTENTE")
        k.LeggiSP(Application("SENIOR"))

        NomeSocieta = k.NomeEPersonam



        FileUpload1.SaveAs(HostingEnvironment.ApplicationPhysicalPath() & "\Public\Turni_Import_" & Appoggio & ".txt")

        If Dir(HostingEnvironment.ApplicationPhysicalPath() & "\Public\Turni_Import_" & Appoggio & ".txt") = "" Then
            ClientScript.RegisterClientScriptBlock(Me.GetType(), "Errore", "VisualizzaErrore('File txt non trovata');", True)
            Exit Function
        End If


        Tabella.Clear()

        Tabella.Columns.Add("IdAppalto", GetType(String))
        Tabella.Columns.Add("IdStruttura", GetType(String))
        Tabella.Columns.Add("IdMansione", GetType(String))

        Tabella.Columns.Add("Appalto", GetType(String))
        Tabella.Columns.Add("CentroDiCosto", GetType(String))
        Tabella.Columns.Add("CodiceOperatore", GetType(String))
        Tabella.Columns.Add("CodiceFiscale", GetType(String))
        Tabella.Columns.Add("CognomeNome", GetType(String))
        Tabella.Columns.Add("Mansione", GetType(String))
        Tabella.Columns.Add("Data", GetType(String))
        Tabella.Columns.Add("Ore", GetType(String))
        Tabella.Columns.Add("Segnalazioni", GetType(String))
        Dim objStreamReader As StreamReader
        objStreamReader = File.OpenText(HostingEnvironment.ApplicationPhysicalPath() & "\Public\Turni_Import_" & Appoggio & ".TXT")


        Dim Testo As String = ""
        Dim CodiceFiscale As String = ""
        Dim Cognome As String = ""
        Dim Nome As String = ""
        Dim Matricola As String = ""
        Dim CentroDiCosto As String = ""
        Dim Mansione As String = ""


        Riga = 0
        Do While Not objStreamReader.EndOfStream
            Riga = Riga + 1
            Testo = objStreamReader.ReadLine

            Dim Data As String
            Dim Ora As String
            Dim Tipo As String
            CodiceFiscale = ""
            Dim Attivita As String
            Dim Struttura As String

            '201812101030eNRNRNRVJDIDKBB31000293440092600000
            '1234567812341123456789012345612345678
            '1234567890123456789012345678901234567890



            Data = Mid(Testo & Space(100), 1, 8)
            Ora = Mid(Testo & Space(100), 9, 4)
            Tipo = Mid(Testo & Space(100), 13, 1)
            CodiceFiscale = Mid(Testo & Space(100), 14, 16)
            Attivita = Mid(Testo & Space(100), 30, 8)
            Struttura = Mid(Testo & Space(100), 38, 5)


            Dim StrutturaCls As New Cls_AppaltiTabellaStruttura

            StrutturaCls.LeggiCentroDiCosto(Session("DC_TABELLE"), Struttura)

            Dim AppaltoStruttureCls As New Cls_Appalti_AppaltiStrutture

            AppaltoStruttureCls.IdStrutture = StrutturaCls.ID
            AppaltoStruttureCls.LeggiAppaltoDaStruttura(Session("DC_TABELLE"))

            Dim MansioneCls As New Cls_AppaltiTabellaMansione


            MansioneCls.LeggiImport(Session("DC_TABELLE"), Attivita)

            Dim AppaltoCls As New Cls_AppaltiAnagrafica


            AppaltoCls.Id = AppaltoStruttureCls.IdAppalto
            AppaltoCls.Leggi(Session("DC_TABELLE"))

            Dim Operatore As New Cls_Operatore

            Operatore.CodiceFiscale = CodiceFiscale
            Operatore.LeggiPerCF(Session("DC_TABELLE"))

            Dim myriga As System.Data.DataRow = Tabella.NewRow()

            myriga(1) = AppaltoStruttureCls.IdAppalto
            myriga(2) = StrutturaCls.ID
            myriga(3) = MansioneCls.ID
            myriga(4) = AppaltoCls.Descrizione
            myriga(5) = Attivita

            myriga(6) = Operatore.CodiceMedico
            myriga(7) = Operatore.CodiceFiscale
            myriga(8) = Operatore.Nome
            myriga(9) = MansioneCls.Descrizione
            myriga(10) = Data
            myriga(11) = Ora




            'Tabella.Columns.Add("IdAppalto", GetType(String))
            'Tabella.Columns.Add("IdStruttura", GetType(String))
            'Tabella.Columns.Add("IdMansione", GetType(String))

            'Tabella.Columns.Add("Appalto", GetType(String))
            'Tabella.Columns.Add("CentroDiCosto", GetType(String))
            'Tabella.Columns.Add("CodiceOperatore", GetType(String))
            'Tabella.Columns.Add("CodiceFiscale", GetType(String))
            'Tabella.Columns.Add("CognomeNome", GetType(String))
            'Tabella.Columns.Add("Mansione", GetType(String))
            'Tabella.Columns.Add("Data", GetType(String))
            'Tabella.Columns.Add("Ore", GetType(String))
            'Tabella.Columns.Add("Segnalazioni", GetType(String))

            Tabella.Rows.Add(myriga)
        Loop



        ViewState("Appoggio") = Tabella

        Grd_ImportTurni.AutoGenerateColumns = False

        Grd_ImportTurni.DataSource = Tabella

        Grd_ImportTurni.DataBind()
        Grd_ImportTurni.Visible = True

    End Function





    Private Function ImportFile(ByVal Appoggio As String) As String
        Dim cn As OleDbConnection

        Dim NomeSocieta As String


        If FileUpload1.FileName = "" Then
            ClientScript.RegisterClientScriptBlock(Me.GetType(), "Errore", "VisualizzaErrore('File non indicato');", True)
            Exit Function
        End If

        Dim k As New Cls_Login

        k.Utente = Session("UTENTE")
        k.LeggiSP(Application("SENIOR"))

        NomeSocieta = k.NomeEPersonam



        FileUpload1.SaveAs(HostingEnvironment.ApplicationPhysicalPath() & "\Public\Turni_Import_" & Appoggio & ".txt")

        If Dir(HostingEnvironment.ApplicationPhysicalPath() & "\Public\Turni_Import_" & Appoggio & ".txt") = "" Then
            ClientScript.RegisterClientScriptBlock(Me.GetType(), "Errore", "VisualizzaErrore('File txt non trovata');", True)
            Exit Function
        End If


        Dim CreateSQL As String


        CreateSQL = "Create table #ImportCUS"
        CreateSQL = CreateSQL & "("
        CreateSQL = CreateSQL & " centroprofitto varchar(50),"
        CreateSQL = CreateSQL & " nominativo varchar(250),"
        CreateSQL = CreateSQL & " codicefiscale varchar(50),"
        CreateSQL = CreateSQL & " contabilita varchar(50),"
        CreateSQL = CreateSQL & " ore float,"
        CreateSQL = CreateSQL & " dataevento varchar(50)"
        CreateSQL = CreateSQL & ")"


        cn = New Data.OleDb.OleDbConnection(Session("DC_TABELLE"))

        cn.Open()


        Dim cmd1 As New OleDbCommand()
        cmd1.CommandText = CreateSQL
        cmd1.Connection = cn
        cmd1.ExecuteReader()


        Dim Riga As Integer = 0

        Dim objStreamReader As StreamReader
        objStreamReader = File.OpenText(HostingEnvironment.ApplicationPhysicalPath() & "\Public\Turni_Import_" & Appoggio & ".TXT")
        Do While Not objStreamReader.EndOfStream
            Dim Testo As String
            Testo = objStreamReader.ReadLine

            Riga = Riga + 1

            If Riga > 1 Then
                Dim Vettore(100) As String

                Vettore = Split(Testo, ";")



                Dim cmdIns As New OleDbCommand()
                cmdIns.CommandText = "Insert Into #ImportCUS (centroprofitto,nominativo,codicefiscale,contabilita,ORE,dataevento) VALUES (?,?,?,?,?,?)"
                cmdIns.Connection = cn
                cmdIns.Parameters.AddWithValue("@centroprofitto", Vettore(0))
                cmdIns.Parameters.AddWithValue("@nominativo", Vettore(1))
                cmdIns.Parameters.AddWithValue("@codicefiscale", Vettore(2))
                cmdIns.Parameters.AddWithValue("@contabilita", Vettore(3))
                cmdIns.Parameters.AddWithValue("@ORE", CDbl(Vettore(4)))
                cmdIns.Parameters.AddWithValue("@dataevento", Vettore(5))
                cmdIns.ExecuteReader()

            End If
        Loop





        Tabella.Clear()

        Tabella.Columns.Add("IdAppalto", GetType(String))
        Tabella.Columns.Add("IdStruttura", GetType(String))
        Tabella.Columns.Add("IdMansione", GetType(String))

        Tabella.Columns.Add("Appalto", GetType(String))
        Tabella.Columns.Add("CentroDiCosto", GetType(String))
        Tabella.Columns.Add("Mansione", GetType(String))
        Tabella.Columns.Add("Data", GetType(String))
        Tabella.Columns.Add("Ore", GetType(String))
        Tabella.Columns.Add("Segnalazioni", GetType(String))
        Dim cmd As New OleDbCommand()
        cmd.CommandText = ("select centroprofitto,contabilita,dataevento,sum(Ore) as TotOre from #ImportCUS Group by centroprofitto,contabilita,dataevento")
        cmd.Connection = cn

        Dim myrd As OleDbDataReader = cmd.ExecuteReader()
        Do While myrd.Read

            Dim DataStr As String

            DataStr = Trim(campodb(myrd.Item("dataevento")))

            Dim Anno As String = Mid(DataStr, 1, 2)
            Dim Mese As String = Mid(DataStr, 3, 2)
            Dim Giorno As String = Mid(DataStr, 5, 2)




            Dim Segnalazione As String = ""
            Dim myriga As System.Data.DataRow = Tabella.NewRow()


            Dim Struttura As New Cls_AppaltiTabellaStruttura

            Struttura.ID = 0
            Struttura.CodificaImport = campodb(myrd.Item("centroprofitto"))
            Struttura.LeggiCentroDiCosto(Session("DC_TABELLE"), Struttura.CodificaImport)

            If Struttura.ID = 0 Then
                Segnalazione = Segnalazione & "Struttura " & Struttura.CodificaImport & " non trovata "
            End If


            Dim StruttuaAppalto As New Cls_Appalti_AppaltiStrutture

            StruttuaAppalto.IdStrutture = Struttura.ID
            StruttuaAppalto.LeggiAppaltoDaStruttura(Session("DC_TABELLE"))



            If StruttuaAppalto.IdAppalto = 0 And Struttura.ID > 0 Then
                Segnalazione = Segnalazione & "Appalto per struttura " & Struttura.Descrizione & " non trovato "
            End If

            Dim Mansione As New Cls_AppaltiTabellaMansione

            If Giorno = 2 Then
                Mansione.LeggiImport(Session("DC_TABELLE"), campodb(myrd.Item("contabilita")))
            Else
                Mansione.LeggiImport(Session("DC_TABELLE"), "F_" & campodb(myrd.Item("contabilita")))
            End If
            
            If Mansione.ID = 0 Then
                If Giorno = 2 Then
                    Segnalazione = Segnalazione & "Mansione F_" & campodb(myrd.Item("contabilita")) & " non trovata "
                Else
                    Segnalazione = Segnalazione & "Mansione " & campodb(myrd.Item("contabilita")) & " non trovata "
                End If

            End If


            myriga("IdAppalto") = StruttuaAppalto.IdAppalto
            myriga("IdStruttura") = StruttuaAppalto.IdStrutture
            myriga("IdMansione") = Mansione.ID

            Dim Appalto As New Cls_AppaltiAnagrafica

            Appalto.Id = StruttuaAppalto.IdAppalto
            Appalto.Leggi(Session("DC_TABELLE"))

            myriga("Appalto") = Appalto.Descrizione

            myriga("CentroDiCosto") = campodb(myrd.Item("centroprofitto"))
            myriga("Mansione") = Mansione.Descrizione


            myriga("Mansione") = Mansione.Descrizione


            myriga("Data") = DateSerial(Anno + 2000, Mese, Giorno)

            myriga("Ore") = campodbn(myrd.Item("TotOre"))


            myriga("Segnalazioni") = Segnalazione

            Tabella.Rows.Add(myriga)
        Loop
        myrd.Close()



        ViewState("Appoggio") = Tabella

        Grd_ImportTurni.AutoGenerateColumns = False

        Grd_ImportTurni.DataSource = Tabella

        Grd_ImportTurni.DataBind()
        Grd_ImportTurni.Visible = True

        'Dim Indice As Integer

        'For Indice = 0 To Grd_ImportTurni.Rows.Count - 1
        '    Dim Chekc As CheckBox = DirectCast(Grd_ImportTurni.Rows(Indice).FindControl("Chk_Selezionato"), CheckBox)
        '    If Tabella.Rows(Indice).Item(11) = "" Then
        '        Chekc.Checked = True
        '    End If
        'Next



    End Function




    Protected Sub Btn_Modifica_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles Btn_Modifica.Click
        Dim Indice As Integer

        Tabella = ViewState("Appoggio")

        For Indice = 0 To Grd_ImportTurni.Rows.Count - 1
            Dim Chekc As CheckBox = DirectCast(Grd_ImportTurni.Rows(Indice).FindControl("Chk_Selezionato"), CheckBox)
            If Tabella.Rows(Indice).Item("Segnalazioni") <> "" Then
                If Chekc.Checked = True Then
                    ClientScript.RegisterClientScriptBlock(Me.GetType(), "Errore", "VisualizzaErrore('Non puoi selezionare un orario con errori');", True)

                    Exit Sub
                End If
            End If
        Next




        For Indice = 0 To Grd_ImportTurni.Rows.Count - 1
            Dim Chekc As CheckBox = DirectCast(Grd_ImportTurni.Rows(Indice).FindControl("Chk_Selezionato"), CheckBox)

            If Chekc.Checked = True Then
                Dim CodiceOperatore As Integer = 0

     

                Dim Prestazione As New Cls_Appalti_Prestazioni



                Prestazione.IdAppalto = Val(Tabella.Rows(Indice).Item(0))
                Prestazione.IdStrutture = Val(Tabella.Rows(Indice).Item(1))
                Prestazione.IdMansione = Val(Tabella.Rows(Indice).Item(2))
                Prestazione.CodiceOperatore = CodiceOperatore
                Prestazione.Data = Tabella.Rows(Indice).Item("Data")
                Prestazione.Ore = CDbl(Tabella.Rows(Indice).Item("Ore"))

                Prestazione.Scrivi(Session("DC_TABELLE"))

                Chekc.Enabled = False

                Tabella.Rows(Indice).Item("Segnalazioni") = "Importato"
            End If
        Next

        ViewState("Appoggio") = Tabella

        Grd_ImportTurni.AutoGenerateColumns = False

        Grd_ImportTurni.DataSource = Tabella

        Grd_ImportTurni.DataBind()
        Grd_ImportTurni.Visible = True



    End Sub

    Function campodb(ByVal oggetto As Object) As String
        If IsDBNull(oggetto) Then
            Return ""
        Else
            Return oggetto
        End If
    End Function

    Function campodbn(ByVal oggetto As Object) As Double
        If IsDBNull(oggetto) Then
            Return 0
        Else
            Return oggetto
        End If
    End Function



    Protected Sub ImageButton3_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles ImageButton3.Click

        Dim Appoggio As String = Format(Now, "yyyyMMddHHmmss")
        Try
            Dim NomeFile As String

            If ChkEstrazioneEpersonam.Checked = False Then
                NomeFile = ImportFile(Appoggio)
            Else
                NomeFile = ImportFileEpersonam(Appoggio)
            End If
        Finally
            Try

                Kill(HostingEnvironment.ApplicationPhysicalPath() & "Public\Turni_Import_" & Appoggio & ".txt")
            Catch ex As Exception

            End Try
        End Try
    End Sub

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Trim(Session("UTENTE")) = "" Then
            Response.Redirect("..\Login.aspx")
            Exit Sub
        End If

        If Page.IsPostBack = True Then Exit Sub


        Dim K1 As New Cls_SqlString

        Dim Barra As New Cls_BarraSenior

        If Not IsNothing(Session("RicercaAnagraficaSQLString")) Then
            Try
                K1 = Session("RicercaAnagraficaSQLString")
            Catch ex As Exception
                K1 = Nothing
            End Try
        End If

        Lbl_BarraSenior.Text = Barra.CodiceBarra(Application("SENIOR"), Session("UTENTE"), Page, K1)


    End Sub

    Protected Sub Btn_Seleziona_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles Btn_Seleziona.Click
        Dim Indice As Integer

        Tabella = ViewState("Appoggio")

        For Indice = 0 To Grd_ImportTurni.Rows.Count - 1
            Dim Chekc As CheckBox = DirectCast(Grd_ImportTurni.Rows(Indice).FindControl("Chk_Selezionato"), CheckBox)
            If Tabella.Rows(Indice).Item("Segnalazioni") = "" Then
                Chekc.Checked = True
            End If
        Next
    End Sub

    Protected Sub Btn_Esci_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles Btn_Esci.Click
        Response.Redirect("Menu_ImportExport.aspx")

    End Sub

End Class
