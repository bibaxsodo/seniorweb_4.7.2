﻿Imports Microsoft.VisualBasic
Imports System.Data.OleDb
Imports System.Web.Hosting
Imports System.Data.SqlTypes
Partial Class Appalti_Elenco_Anagraficautenti
    Inherits System.Web.UI.Page
    Dim Tabella As New System.Data.DataTable("tabella")


    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Session("UTENTE") = "" Then
            Response.Redirect("..\Login.aspx")
            Exit Sub
        End If

        If Page.IsPostBack = True Then
            Exit Sub
        End If


        Dim K1 As New Cls_SqlString

        Dim Barra As New Cls_BarraSenior

        If Not IsNothing(Session("RicercaAnagraficaSQLString")) Then
            Try
                K1 = Session("RicercaAnagraficaSQLString")
            Catch ex As Exception
                K1 = Nothing
            End Try
        End If

        Lbl_BarraSenior.Text = Barra.CodiceBarra(Application("SENIOR"), Session("UTENTE"), Page, K1)


        Dim f As New Cls_CausaliEntrataUscita

        Dim cn As New OleDbConnection

        Dim ConnectionString As String = Session("DC_OSPITE")

        cn = New Data.OleDb.OleDbConnection(ConnectionString)

        cn.Open()

        Dim cmd As New OleDbCommand

        cmd.CommandText = "Select * From AnagraficaComune Where CodiceOspite > 0 And CodiceParente = 0 ORDER BY NOME"

        cmd.Connection = cn



        Tabella.Clear()
        Tabella.Columns.Add("CodiceOspite", GetType(String))
        Tabella.Columns.Add("Nome", GetType(String))


        Dim myPOSTreader As OleDbDataReader = cmd.ExecuteReader()
        Do While myPOSTreader.Read
            Dim myriga As System.Data.DataRow = Tabella.NewRow()

            myriga(0) = myPOSTreader.Item("CodiceOspite")
            myriga(1) = myPOSTreader.Item("Nome")

            If IsNothing(myPOSTreader.Item("DataNascita")) Then
                myriga(1) = myPOSTreader.Item("Nome")
            Else
                If Year(campodbd(myPOSTreader.Item("DataNascita"))) < 1900 Then
                    myriga(1) = myPOSTreader.Item("Nome")
                Else
                    myriga(1) = myPOSTreader.Item("Nome") & " " & Format(myPOSTreader.Item("DataNascita"), "dd/MM/yyyy")
                End If
            End If

            Tabella.Rows.Add(myriga)
        Loop

        myPOSTreader.Close()
        cn.Close()

        ViewState("Appoggio") = Tabella

        Grd_ImportoOspite.AutoGenerateColumns = True

        Grd_ImportoOspite.DataSource = Tabella

        Grd_ImportoOspite.DataBind()

        Btn_Nuovo.Visible = True
    End Sub

    Function campodbd(ByVal oggetto As Object) As Date
        If IsDBNull(oggetto) Then
            Return Nothing
        Else
            Return oggetto
        End If
    End Function

    Protected Sub Grd_ImportoOspite_PageIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles Grd_ImportoOspite.PageIndexChanged

    End Sub

    Protected Sub Grd_ImportoOspite_PageIndexChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewPageEventArgs) Handles Grd_ImportoOspite.PageIndexChanging
        Tabella = ViewState("Appoggio")
        Grd_ImportoOspite.PageIndex = e.NewPageIndex
        Grd_ImportoOspite.DataSource = Tabella
        Grd_ImportoOspite.DataBind()
    End Sub

    Protected Sub Grd_ImportoOspite_RowCommand(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewCommandEventArgs) Handles Grd_ImportoOspite.RowCommand

        If e.CommandName = "Seleziona" Then


            Dim d As Integer


            Tabella = ViewState("Appoggio")

            d = Val(e.CommandArgument)

            Dim Codice As String

            Codice = Tabella.Rows(d).Item(0).ToString

            Response.Redirect("AnagraficaUtenti.aspx?id=" & Codice)
        End If
    End Sub

    Protected Sub Grd_ImportoOspite_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles Grd_ImportoOspite.SelectedIndexChanged

    End Sub

    Protected Sub Btn_Esci_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles Btn_Esci.Click
        Response.Redirect("Menu_Appalti.aspx")
    End Sub

    Protected Sub ImageButton1_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles ImageButton1.Click

        Dim f As New Cls_CausaliEntrataUscita

        Dim cn As New OleDbConnection


        Dim ConnectionString As String = Session("DC_OSPITE")

        cn = New Data.OleDb.OleDbConnection(ConnectionString)

        cn.Open()

        Dim cmd As New OleDbCommand
        Dim Condizione As String = ""
        If Txt_Descrizione.Text.Trim <> "" Then
            Condizione = " And  nome like ?"

            cmd.Parameters.AddWithValue("@Nome", Txt_Descrizione.Text)
        End If
        If Val(Txt_Codice.Text) > 0 Then
            Condizione = " And  CodiceOspite = ?"

            cmd.Parameters.AddWithValue("@CodiceOspite", Val(Txt_Codice.Text))
        End If

        cmd.CommandText = "Select * From AnagraficaComune Where CodiceOspite > 0 And CodiceParente = 0 " & Condizione & "  ORDER BY NOME"


        cmd.Connection = cn



        Tabella.Clear()
        Tabella.Columns.Add("CodiceOspite", GetType(String))
        Tabella.Columns.Add("Nome", GetType(String))


        Dim myPOSTreader As OleDbDataReader = cmd.ExecuteReader()
        Do While myPOSTreader.Read
            Dim myriga As System.Data.DataRow = Tabella.NewRow()

            myriga(0) = myPOSTreader.Item("CodiceOspite")
            myriga(1) = myPOSTreader.Item("Nome")

            If IsNothing(myPOSTreader.Item("DataNascita")) Then
                myriga(1) = myPOSTreader.Item("Nome")
            Else
                If Year(campodbd(myPOSTreader.Item("DataNascita"))) < 1900 Then
                    myriga(1) = myPOSTreader.Item("Nome")
                Else
                    myriga(1) = myPOSTreader.Item("Nome") & " " & Format(myPOSTreader.Item("DataNascita"), "dd/MM/yyyy")
                End If
            End If

            Tabella.Rows.Add(myriga)
        Loop

        myPOSTreader.Close()
        cn.Close()


        ViewState("Appoggio") = Tabella

        Grd_ImportoOspite.AutoGenerateColumns = True

        Grd_ImportoOspite.DataSource = Tabella

        Grd_ImportoOspite.DataBind()

        Btn_Nuovo.Visible = True
    End Sub

    Protected Sub Btn_Nuovo_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles Btn_Nuovo.Click
        Response.Redirect("AnagraficaUtenti.aspx")
    End Sub
End Class
