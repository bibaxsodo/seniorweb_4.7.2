﻿Imports Microsoft.VisualBasic
Imports System.Data.OleDb
Imports System.Web.Hosting
Imports System.Data.SqlTypes
Partial Class Appalti_Elenco_Prestazioni
    Inherits System.Web.UI.Page
    Dim Tabella As New System.Data.DataTable("tabella")
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Session("UTENTE") = "" Then
            Call Ricerca()
            Response.Redirect("..\Login.aspx")
            Exit Sub
        End If
        EseguiJS()
        If Page.IsPostBack = True Then
            Exit Sub
        End If


        Dim K1 As New Cls_SqlString

        Dim Barra As New Cls_BarraSenior

        If Not IsNothing(Session("RicercaAnagraficaSQLString")) Then
            Try
                K1 = Session("RicercaAnagraficaSQLString")
            Catch ex As Exception
                K1 = Nothing
            End Try
        End If

        Lbl_BarraSenior.Text = Barra.CodiceBarra(Application("SENIOR"), Session("UTENTE"), Page, K1)


        Dim MAppalto As New Cls_AppaltiAnagrafica

        MAppalto.UpDateDropBox(Session("DC_TABELLE"), DD_Appalto)


        Dim f As New Cls_Parametri

        f.LeggiParametri(Session("DC_OSPITE"))
        Dd_Mese.SelectedValue = f.MeseFatturazione
        Txt_Anno.Text = f.AnnoFatturazione
    End Sub


    Private Sub Ricerca()

        Dim cn As New OleDbConnection


        Dim ConnectionString As String = Session("DC_TABELLE")

        cn = New Data.OleDb.OleDbConnection(ConnectionString)

        cn.Open()

        Dim cmd As New OleDbCommand
        Dim Condizione As String = ""

        If Val(DD_Appalto.SelectedValue) > 0 Then
            Condizione = " IdAppalto = " & Val(DD_Appalto.SelectedValue)
        End If
        If Val(Txt_Anno.Text) > 0 And Val(Dd_Mese.SelectedValue) > 0 Then
            If Condizione <> "" Then
                Condizione = Condizione & " And "
            End If
            Condizione = Condizione & " month(Data)  = " & Val(Dd_Mese.SelectedValue) & " And year(Data) = " & Val(Txt_Anno.Text)
        End If
        If Condizione = "" Then
            cmd.CommandText = "Select * From Appalti_Prestazioni Order By Data,IdAppalto,IdStrutture,IdMansione,CodiceOperatore"
        Else
            cmd.CommandText = "Select * From Appalti_Prestazioni where " & Condizione & " Order By Data,IdAppalto,IdStrutture,IdMansione,CodiceOperatore"
        End If
        cmd.Connection = cn




        Tabella.Clear()
        Tabella.Columns.Add("Id", GetType(String))
        Tabella.Columns.Add("Data", GetType(String))
        Tabella.Columns.Add("Appalto", GetType(String))
        Tabella.Columns.Add("Struttura", GetType(String))
        Tabella.Columns.Add("Mansione", GetType(String))
        Tabella.Columns.Add("Operatore", GetType(String))
        Tabella.Columns.Add("Ore", GetType(String))


        Dim myPOSTreader As OleDbDataReader = cmd.ExecuteReader()
        Do While myPOSTreader.Read
            Dim myriga As System.Data.DataRow = Tabella.NewRow()

            myriga(0) = campodbn(myPOSTreader.Item("id"))
            myriga(1) = Format(campodbd(myPOSTreader.Item("Data")), "dd/MM/yyyy")

            Dim Appalti As New Cls_AppaltiAnagrafica

            Appalti.Id = campodbn(myPOSTreader.Item("IdAppalto"))
            Appalti.Leggi(Session("DC_TABELLE"))


            Dim TabOpe As New Cls_Operatore

            TabOpe.CodiceMedico = campodb(myPOSTreader.Item("CodiceOperatore"))
            TabOpe.Leggi(Session("DC_OSPITE"))



            Dim MS As New Cls_AppaltiTabellaStruttura

            MS.ID = campodbn(myPOSTreader.Item("IdStrutture"))
            MS.Leggi(Session("DC_TABELLE"), MS.ID)


            Dim Mans As New Cls_AppaltiTabellaMansione

            Mans.ID = campodbn(myPOSTreader.Item("IdMansione"))
            Mans.Leggi(Session("DC_TABELLE"), Mans.ID)

            myriga(2) = Appalti.Descrizione
            myriga(3) = MS.Descrizione
            myriga(4) = Mans.Descrizione
            myriga(5) = TabOpe.Nome
            myriga(6) = Format(campodbn(myPOSTreader.Item("Ore")), "#,##0.00")



            Tabella.Rows.Add(myriga)
        Loop

        myPOSTreader.Close()
        cn.Close()


        ViewState("Appoggio") = Tabella

        Grd_ImportoOspite.AutoGenerateColumns = True

        Grd_ImportoOspite.DataSource = Tabella

        Grd_ImportoOspite.DataBind()

        Btn_Nuovo.Visible = True
    End Sub

    Protected Sub Grd_ImportoOspite_PageIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles Grd_ImportoOspite.PageIndexChanged

    End Sub

    Protected Sub Grd_ImportoOspite_PageIndexChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewPageEventArgs) Handles Grd_ImportoOspite.PageIndexChanging
        Tabella = ViewState("Appoggio")
        Grd_ImportoOspite.PageIndex = e.NewPageIndex
        Grd_ImportoOspite.DataSource = Tabella
        Grd_ImportoOspite.DataBind()
    End Sub

    Protected Sub Grd_ImportoOspite_RowCommand(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewCommandEventArgs) Handles Grd_ImportoOspite.RowCommand

        If e.CommandName = "Seleziona" Then


            Dim d As Integer


            Tabella = ViewState("Appoggio")

            d = Val(e.CommandArgument)

            Dim Codice As String

            Codice = Tabella.Rows(d).Item(0).ToString

            Response.Redirect("Prestazione.aspx?id=" & Codice)
        End If
    End Sub

    Protected Sub Grd_ImportoOspite_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles Grd_ImportoOspite.SelectedIndexChanged

    End Sub

    Protected Sub Btn_Esci_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles Btn_Esci.Click
        Response.Redirect("Menu_Appalti.aspx")
    End Sub

    Protected Sub ImageButton1_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles ImageButton1.Click
        Ricerca()
    End Sub

    Protected Sub Btn_Nuovo_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles Btn_Nuovo.Click
        Response.Redirect("Prestazione.aspx")
    End Sub


    Function campodb(ByVal oggetto As Object) As String
        If IsDBNull(oggetto) Then
            Return ""
        Else
            Return oggetto
        End If
    End Function

    Function campodbn(ByVal oggetto As Object) As Double
        If IsDBNull(oggetto) Then
            Return 0
        Else
            Return oggetto
        End If
    End Function

    Function campodbd(ByVal oggetto As Object) As Date
        If IsDBNull(oggetto) Then
            Return Nothing
        Else
            Return oggetto
        End If
    End Function

    Protected Sub Lnk_ToExcel_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles Lnk_ToExcel.Click

        Session("GrigliaSoloStampa") = ViewState("Appoggio")
        ScriptManager.RegisterClientScriptBlock(Me, Me.GetType(), "Stampa", "openPopUp('../Ospitiweb/ExportExcel.aspx','Excel','width=800,height=600');", True)
    End Sub



    Private Sub EseguiJS()
        Dim MyJs As String
        MyJs = "$(document).ready(function() {"

        MyJs = MyJs & "var els = document.getElementsByTagName(""*"");"

        MyJs = MyJs & "for (var i=0;i<els.length;i++)"
        MyJs = MyJs & "if ( els[i].id ) { "
        MyJs = MyJs & " var appoggio =els[i].id; "
        MyJs = MyJs & " if (appoggio.match('Txt_Data')!= null) {  "
        MyJs = MyJs & " $(els[i]).mask(""99/99/9999"");"
        MyJs = MyJs & " $(els[i]).datepicker({ changeMonth: true,changeYear: true,  yearRange: ""2000:" & Year(Now) + 12 & """},$.datepicker.regional[""it""]);"
        MyJs = MyJs & "    }"
        MyJs = MyJs & " if ((appoggio.match('Txt_Importo')!= null) || (appoggio.match('Txt_Percentuale')!= null) ) {  "
        MyJs = MyJs & " $(els[i]).keypress(function() { ForceNumericInput($(this).val(), true, true); } ); "
        MyJs = MyJs & " $(els[i]).blur(function() { var ap = formatNumber($(this).val(),2); $(this).val(ap); });"
        MyJs = MyJs & "    }"

        MyJs = MyJs & " if (appoggio.match('Txt_Conto')!= null) {   "
        MyJs = MyJs & " $(els[i]).autocomplete('/WebHandler/GestioneConto.ashx?Utente=" & Session("UTENTE") & "', {delay:5,minChars:3});"
        MyJs = MyJs & "    }"

        MyJs = MyJs & " if (appoggio.match('Txt_ComRes')!= null)  {  "
        MyJs = MyJs & " $(els[i]).autocomplete('/WebHandler/AutocompleteComune.ashx?Utente=" & Session("UTENTE") & "', {delay:5,minChars:3});"
        MyJs = MyJs & "    }"

        MyJs = MyJs & "} "


        MyJs = MyJs & "$("".chosen-select"").chosen({"
        MyJs = MyJs & "no_results_text: ""Comune non trovato"","
        MyJs = MyJs & "display_disabled_options: true,"
        MyJs = MyJs & "allow_single_deselect: true,"
        MyJs = MyJs & "width: ""400px"","
        MyJs = MyJs & "placeholder_text_single: ""Seleziona """
        MyJs = MyJs & "});"

        MyJs = MyJs & "});"
        'MyJs = "$(document).ready(function() { $('.myClass').keypress(function() { handleEnter($('.myClass').val(), true, true); } );     $('#" & Txt_ImportoPagato.ClientID & "').blur(function() { var ap = formatNumber ($('#" & Txt_ImportoPagato.ClientID & "').val(),2); $('#" & Txt_ImportoPagato.ClientID & "').val(ap); }); });"
        ScriptManager.RegisterStartupScript(Me, Me.GetType(), "visualizzaRitIm", MyJs, True)
    End Sub
End Class
