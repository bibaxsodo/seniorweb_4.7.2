﻿
Partial Class Appalti_Tabella_ModalitaPagamento
    Inherits System.Web.UI.Page

    Protected Sub Btn_Modifica_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles Btn_Modifica.Click
        If Txt_Codice.Text.Trim = "" Then
            ClientScript.RegisterClientScriptBlock(Me.GetType(), "Errore", "VisualizzaErrore('Digitare codice');", True)
            Exit Sub
        End If
        If Txt_Descrizione.Text.Trim = "" Then
            ClientScript.RegisterClientScriptBlock(Me.GetType(), "Errore", "VisualizzaErrore('Digitare descrizione');", True)
            Exit Sub
        End If

        If Txt_Descrizione.Text.Length > 50 Then
            ClientScript.RegisterClientScriptBlock(Me.GetType(), "Errore", "VisualizzaErrore('Descrizione troppo lunga');", True)
            Exit Sub
        End If

        If Txt_Codice.Text.Length > 2 Then
            ClientScript.RegisterClientScriptBlock(Me.GetType(), "Errore", "VisualizzaErrore('Codice troppo lunga');", True)
            Exit Sub
        End If


        Dim K As New ClsModalitaPagamento

        K.Codice = Request.Item("CODICE")

        K.Leggi(Session("DC_OSPITE"))

        K.Codice = Txt_Codice.Text

        If Len(Txt_Descrizione.Text) > 50 Then
            Txt_Descrizione.Text = Mid(Txt_Descrizione.Text, 1, 50)
        End If
        K.DESCRIZIONE = Txt_Descrizione.Text

        Dim Vettore(100) As String
        If Txt_Sottoconto.Text <> "" Then
            Vettore = SplitWords(Txt_Sottoconto.Text)
            If Vettore.Length > 1 Then
                K.MASTRO = Vettore(0)
                K.CONTO = Vettore(1)
                K.SOTTOCONTO = Vettore(2)
            End If
        Else
            K.MASTRO = 0
            K.CONTO = 0
            K.SOTTOCONTO = 0
        End If

        K.ModalitaPagamento = DD_ModalitaPagamento.SelectedValue

        K.DescrizioneEstesa = Txt_DescrizioneEstea.Text

     

        K.Utente = Session("UTENTE")
        K.Scrivi(Session("DC_OSPITE"))

        Response.Redirect("Elenco_ModalitaPagamento.aspx")

    End Sub

    Private Function SplitWords(ByVal s As String) As String()
        '
        ' Call Regex.Split function from the imported namespace.
        ' Return the result array.
        '
        Return Regex.Split(s, "\W+")
    End Function

    Protected Sub OspitiWeb_Tabella_ModalitaPagamento_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load

        If Page.IsPostBack = True Then
            Exit Sub
        End If

        Dim kTP As New Cls_TipoPagamento

        kTP.UpDateDropBox(Session("DC_TABELLE"), DD_ModalitaPagamento)




        If Request.Item("CODICE") <> "" Then
            Dim K As New ClsModalitaPagamento

            K.Codice = Request.Item("CODICE")

            K.Leggi(Session("DC_OSPITE"))

            Txt_Codice.Text = K.Codice
            Txt_Descrizione.Text = K.DESCRIZIONE

            Dim PianoConti As New Cls_Pianodeiconti

            PianoConti.Mastro = K.MASTRO
            PianoConti.Conto = K.CONTO
            PianoConti.Sottoconto = K.SOTTOCONTO
            PianoConti.Decodfica(Session("DC_GENERALE"))

            Txt_Sottoconto.Text = K.MASTRO & " " & K.CONTO & " " & K.SOTTOCONTO & " " & PianoConti.Descrizione

            DD_ModalitaPagamento.SelectedValue = K.ModalitaPagamento


            Txt_DescrizioneEstea.Text = K.DescrizioneEstesa

        
        Else
            Dim TipoPag As New ClsModalitaPagamento


            Txt_Codice.Text = TipoPag.MaxModalitaPagamento(Session("DC_OSPITE"))

        End If

        Call EseguiJS()
    End Sub


    Private Sub EseguiJS()
        Dim MyJs As String
        MyJs = "$(document).ready(function() {"

        MyJs = MyJs & "var els = document.getElementsByTagName(""*"");"

        MyJs = MyJs & "for (var i=0;i<els.length;i++)"
        MyJs = MyJs & "if ( els[i].id ) { "
        MyJs = MyJs & " var appoggio =els[i].id; "
        MyJs = MyJs & " if (appoggio.match('TxtData')!= null) {  "
        MyJs = MyJs & " $(els[i]).mask(""99/99/9999"");"
        MyJs = MyJs & "    }"

        MyJs = MyJs & " if ((appoggio.match('TxtImporto')!= null) ) {  "
        MyJs = MyJs & " $(els[i]).keypress(function() { ForceNumericInput($(this).val(), true, true); } ); "
        MyJs = MyJs & " $(els[i]).blur(function() { var ap = formatNumber($(this).val(),2); $(this).val(ap); });"
        MyJs = MyJs & "    }"

        MyJs = MyJs & " if (appoggio.match('Txt_Sottoconto')!= null) {   "
        MyJs = MyJs & " $(els[i]).autocomplete('/WebHandler/GestioneConto.ashx?Utente=" & Session("UTENTE") & "', {delay:5,minChars:3});"
        MyJs = MyJs & "    }"

        MyJs = MyJs & "} "
        MyJs = MyJs & "});"

        'MyJs = "$(document).ready(function() { $('.myClass').keypress(function() { handleEnter($('.myClass').val(), true, true); } );     $('#" & Txt_ImportoPagato.ClientID & "').blur(function() { var ap = formatNumber ($('#" & Txt_ImportoPagato.ClientID & "').val(),2); $('#" & Txt_ImportoPagato.ClientID & "').val(ap); }); });"
        ScriptManager.RegisterStartupScript(Me, Me.GetType(), "visualizzaRitIm", MyJs, True)
    End Sub

    Protected Sub ImageButton1_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles ImageButton1.Click
        If Txt_Codice.Text.Trim = "" Then
            ClientScript.RegisterClientScriptBlock(Me.GetType(), "Errore", "VisualizzaErrore('Digitare codice');", True)
            Exit Sub
        End If
        If Txt_Descrizione.Text.Trim = "" Then
            ClientScript.RegisterClientScriptBlock(Me.GetType(), "Errore", "VisualizzaErrore('Digitare descrizione');", True)
            Exit Sub
        End If

        Dim K As New ClsModalitaPagamento

        K.Codice = Request.Item("CODICE")

        K.Elimina(Session("DC_OSPITE"))

        Response.Redirect("Elenco_ModalitaPagamento.aspx")
    End Sub

    Protected Sub Btn_Esci_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles Btn_Esci.Click
        Response.Redirect("Elenco_ModalitaPagamento.aspx")
    End Sub

    Protected Sub Btn_Modifica_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Btn_Modifica.Load

        Dim K1 As New Cls_SqlString

        Dim Barra As New Cls_BarraSenior

        If Not IsNothing(Session("RicercaAnagraficaSQLString")) Then
            K1 = Session("RicercaAnagraficaSQLString")
        End If

        Lbl_BarraSenior.Text = Barra.CodiceBarra(Application("SENIOR"), Session("UTENTE"), Page, K1)
    End Sub

    Protected Sub Imb_Duplica_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles Imb_Duplica.Click
        Dim MaxMogPag As New ClsModalitaPagamento

        Txt_Codice.Text = MaxMogPag.MaxModalitaPagamento(Session("DC_OSPITE"))

        Txt_Codice.Enabled = True

        Call Txt_Codice_TextChanged(sender, e)
        Call Txt_Descrizione_TextChanged(sender, e)


        Call EseguiJS()
    End Sub


    Protected Sub Txt_Codice_TextChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles Txt_Codice.TextChanged
        If Txt_Codice.Enabled = True Then
            Img_VerificaCod.ImageUrl = "~/images/corretto.gif"

            Dim x As New ClsModalitaPagamento

            x.Codice = Txt_Codice.Text
            x.Leggi(Session("DC_OSPITE"))

            If x.Descrizione <> "" Then
                Img_VerificaCod.ImageUrl = "~/images/errore.gif"
            End If
        End If
        Call EseguiJS()
    End Sub


    Protected Sub Txt_Descrizione_TextChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles Txt_Descrizione.TextChanged
        If Txt_Codice.Enabled = True Then
            Img_VerificaDes.ImageUrl = "~/images/corretto.gif"

            Dim x As New ClsModalitaPagamento

            x.DESCRIZIONE = Txt_Descrizione.Text
            x.LeggiDescrizione(Session("DC_OSPITE"))

            If x.Codice <> "" Then
                Img_VerificaDes.ImageUrl = "~/images/errore.gif"
            End If
        End If
        Call EseguiJS()
    End Sub
End Class
