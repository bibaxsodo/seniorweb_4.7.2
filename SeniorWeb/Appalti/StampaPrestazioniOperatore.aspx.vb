﻿Imports Microsoft.VisualBasic
Imports System.Data.OleDb
Imports System.Web.Hosting
Imports System.Data.SqlTypes
Imports SeniorWebApplication

Partial Class Appalti_StampaPrestazioniOperatore
    Inherits System.Web.UI.Page
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Session("UTENTE") = "" Then
            Response.Redirect("..\Login.aspx")
            Exit Sub
        End If

        If Page.IsPostBack = True Then
            Exit Sub
        End If


        Dim K1 As New Cls_SqlString

        Dim Barra As New Cls_BarraSenior

        If Not IsNothing(Session("RicercaAnagraficaSQLString")) Then
            Try
                K1 = Session("RicercaAnagraficaSQLString")
            Catch ex As Exception
                K1 = Nothing
            End Try
        End If

        Lbl_BarraSenior.Text = Barra.CodiceBarra(Application("SENIOR"), Session("UTENTE"), Page, K1)


        Dim MAppalto As New Cls_AppaltiAnagrafica

        MAppalto.UpDateDropBox(Session("DC_TABELLE"), DD_Appalto)

        Dim MStruttura As New Cls_AppaltiTabellaStruttura


        MStruttura.UpDateDropBox(Session("DC_TABELLE"), DD_Struttura)


        Dim f As New Cls_Parametri

        f.LeggiParametri(Session("DC_OSPITE"))
        Dd_Mese.SelectedValue = f.MeseFatturazione
        Txt_Anno.Text = f.AnnoFatturazione
    End Sub


    Protected Sub Btn_Esci_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles Btn_Esci.Click
        Response.Redirect("Menu_Appalti.aspx")
    End Sub

    Protected Sub DD_Appalto_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles DD_Appalto.SelectedIndexChanged

        Dim MStruttura As New Cls_AppaltiTabellaStruttura


        MStruttura.UpDateDropBoxAppalto(Session("DC_TABELLE"), DD_Struttura, Val(DD_Appalto.SelectedValue))
    End Sub

    Protected Sub Img_Stampa_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles Img_Stampa.Click
        If Val(Txt_Anno.Text) = 0 Then
            Exit Sub
        End If

        If Val(Dd_Mese.SelectedValue) = 0 Then
            Exit Sub
        End If

        If Val(DD_Appalto.SelectedValue) = 0 Then
            Exit Sub
        End If

        If Val(DD_Struttura.SelectedValue) = 0 Then
            Exit Sub
        End If

        Dim cn As New OleDbConnection


        Dim ConnectionString As String = Session("DC_TABELLE")

        cn = New Data.OleDb.OleDbConnection(ConnectionString)

        cn.Open()

        Dim cmd As New OleDbCommand
        Dim Condizione As String = ""

        If Val(DD_Appalto.SelectedValue) > 0 Then
            Condizione = " IdAppalto = " & Val(DD_Appalto.SelectedValue)
        End If
        If Val(Txt_Anno.Text) > 0 And Val(Dd_Mese.SelectedValue) > 0 Then
            If Condizione <> "" Then
                Condizione = Condizione & " And "
            End If
            Condizione = Condizione & " month(Data)  = " & Val(Dd_Mese.SelectedValue) & " And year(Data) = " & Val(Txt_Anno.Text)
        End If
        If Val(DD_Struttura.SelectedValue) > 0 Then
            If Condizione <> "" Then
                Condizione = Condizione & " And "
            End If
            Condizione = Condizione & " IdStruttura  = " & Val(DD_Struttura.SelectedValue)
        End If

        If RB_CicloAttivo.Checked = True Then
            If Condizione <> "" Then
                Condizione = Condizione & " And "
            End If
            Condizione = Condizione & " (CalcoloPassivo = 0 or CalcoloPassivo  is null) "
        End If
        If RB_CicloPassivo.Checked = True Then
            If Condizione <> "" Then
                Condizione = Condizione & " And "
            End If
            Condizione = Condizione & " CalcoloPassivo > 0 "
        End If

        If Condizione = "" Then
            cmd.CommandText = "Select CodiceOperatore From Appalti_PrestazioniElaborate  Group By CodiceOperatore "
        Else
            cmd.CommandText = "Select CodiceOperatore From Appalti_PrestazioniElaborate where " & Condizione & " Group By CodiceOperatore Order by CodiceOperatore"
        End If
        cmd.Connection = cn
        Dim Scheda As New AppaltiXSD


        Dim myPOSTreader As OleDbDataReader = cmd.ExecuteReader()
        Do While myPOSTreader.Read

            For Giorni = 1 To GiorniMese(Val(Dd_Mese.SelectedValue), Val(Txt_Anno.Text))
                Dim CmdP As New OleDbCommand
                CmdP.Connection = cn
                CmdP.CommandText = "Select * From Appalti_PrestazioniElaborate where " & Condizione & " And Data = ? And CodiceOperatore = ?"
                CmdP.Parameters.AddWithValue("@Data", DateSerial(Val(Txt_Anno.Text), Val(Dd_Mese.SelectedValue), Giorni))
                CmdP.Parameters.AddWithValue("@CodiceOperatore", campodb(myPOSTreader.Item("CodiceOperatore")))


                Dim MovimentiGiorno As OleDbDataReader = CmdP.ExecuteReader()
                Do While MovimentiGiorno.Read
                    Dim Riga As System.Data.DataRow = Scheda.Tables("PrestazioniMese").NewRow

                    Dim Appalto As New Cls_AppaltiAnagrafica


                    Appalto.Id = campodbn(MovimentiGiorno.Item("IdAppalto"))
                    Appalto.Leggi(Session("DC_TABELLE"))

                    Riga.Item("Appalto") = Appalto.Descrizione

                    Dim Struttura As New Cls_AppaltiTabellaStruttura


                    Struttura.ID = campodbn(MovimentiGiorno.Item("IdStruttura"))
                    Struttura.Leggi(Session("DC_TABELLE"), Struttura.ID)

                    Riga.Item("Struttura") = Struttura.Descrizione


                    Dim Operatore As New Cls_Operatore

                    Operatore.CodiceMedico = campodbn(MovimentiGiorno.Item("CodiceOperatore"))
                    Operatore.Leggi(Session("DC_OSPITE"))

                    Riga.Item("Operatore") = Operatore.Nome

                    Dim TipoOperatore As New Cls_TipoOperatore

                    TipoOperatore.Codice = Operatore.CodiceMedico
                    TipoOperatore.Leggi(Session("DC_OSPITE"), TipoOperatore.Codice)

                    Riga.Item("Specializzazione") = TipoOperatore.Descrizione



                    Riga.Item("Anno") = Year(campodbd(MovimentiGiorno.Item("Data")))
                    Riga.Item("Mese") = DecodificaMese(Month(campodbd(MovimentiGiorno.Item("Data"))))
                    Riga.Item("Giorno") = Day(campodbd(MovimentiGiorno.Item("Data")))

                    Riga.Item("Ore") = campodbn(MovimentiGiorno.Item("Ore"))

                    Dim Mansione As New Cls_AppaltiTabellaMansione


                    Mansione.ID = campodbn(MovimentiGiorno.Item("IdMansione"))
                    Mansione.Leggi(Session("DC_TABELLE"), Mansione.ID)

                    Riga.Item("Mansione") = Mansione.Descrizione


                    Dim Ospite As New ClsOspite

                    Ospite.CodiceOspite = campodbn(MovimentiGiorno.Item("codiceospite"))
                    Ospite.Leggi(Session("DC_OSPITE"), Ospite.CodiceOspite)

                    Riga.Item("Utente") = Ospite.CognomeOspite & " " & Ospite.NomeOspite

                    Riga.Item("Importo") = campodbn(MovimentiGiorno.Item("Imponibile"))

                    If campodbn(MovimentiGiorno.Item("DONE")) = 1 Or campodbn(MovimentiGiorno.Item("DONE")) = 5 Then
                        Riga.Item("Eseguito") = "Eseguito"
                    Else
                        Riga.Item("Eseguito") = "Non Eseguito"
                    End If

                    Scheda.Tables("PrestazioniMese").Rows.Add(Riga)
                Loop
            Next
        Loop
        myPOSTreader.Close()
        cn.Close()

        Session("stampa") = Scheda


        ScriptManager.RegisterClientScriptBlock(Me, Me.GetType(), "Stampa4", "openPopUp('Stampa_ReportXSD.aspx?REPORT=APPALTISTAMPAPRESTAZIONI','Stampe4','width=800,height=600');", True)

    End Sub
    Public Function GiorniMese(ByVal Mese As Byte, ByVal Anno As Integer) As Byte
        If Mese = 1 Or Mese = 3 Or Mese = 5 Or Mese = 7 Or Mese = 8 Or
           Mese = 10 Or Mese = 12 Then
            GiorniMese = 31
        Else
            If Mese <> 2 Then
                GiorniMese = 30
            Else
                If Day(DateSerial(Anno, Mese, 29)) = 29 Then
                    GiorniMese = 29
                Else
                    GiorniMese = 28
                End If
            End If
        End If
    End Function

    Public Function DecodificaMese(ByVal Mese As Long) As String
        Select Case Mese
            Case 1
                DecodificaMese = "Gennaio"
            Case 2
                DecodificaMese = "Febbraio"
            Case 3
                DecodificaMese = "Marzo"
            Case 4
                DecodificaMese = "Aprile"
            Case 5
                DecodificaMese = "Maggio"
            Case 6
                DecodificaMese = "Giugno"
            Case 7
                DecodificaMese = "Luglio"
            Case 8
                DecodificaMese = "Agosto"
            Case 9
                DecodificaMese = "Settembre"
            Case 10
                DecodificaMese = "Ottobre"
            Case 11
                DecodificaMese = "Novembre"
            Case 12
                DecodificaMese = "Dicembre"
        End Select
    End Function

    Function campodb(ByVal oggetto As Object) As String
        If IsDBNull(oggetto) Then
            Return ""
        Else
            Return oggetto
        End If
    End Function

    Function campodbn(ByVal oggetto As Object) As Double
        If IsDBNull(oggetto) Then
            Return 0
        Else
            Return oggetto
        End If
    End Function

    Function campodbd(ByVal oggetto As Object) As Date
        If IsDBNull(oggetto) Then
            Return Nothing
        Else
            Return oggetto
        End If
    End Function




End Class
