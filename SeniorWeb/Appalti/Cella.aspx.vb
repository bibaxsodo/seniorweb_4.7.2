﻿Imports Microsoft.VisualBasic
Imports System.Data.OleDb
Imports System.Web.Hosting
Imports System.Data.SqlTypes
Partial Class Appalti_Cella
    Inherits System.Web.UI.Page

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load

        If Session("UTENTE") = "" Then
            Response.Redirect("..\Login.aspx")
            Exit Sub
        End If

        If Page.IsPostBack = True Then
            Exit Sub
        End If


        Dim Anno As Integer
        Dim Mese As Integer
        Dim Giorno As Integer
        Dim CodiceOperatore As Integer
        Dim Appalto As Integer
        Dim Struttura As Integer

        Giorno = Request.Item("Giorno")
        CodiceOperatore = Request.Item("CodiceOperatore")

        Anno = Request.Item("Anno")


        Mese = Request.Item("Mese")

        Appalto = Request.Item("Appalto")
        Struttura = Request.Item("Struttura")


        Dim Mc As New Cls_AppaltiTabellaMansione

        Mc.UpDateDropBox(Session("DC_TABELLE"), DD_Mansione)

        Mc.UpDateDropBox(Session("DC_TABELLE"), DD_Mansione1)


        Mc.UpDateDropBox(Session("DC_TABELLE"), DD_Mansione2)

        Mc.UpDateDropBox(Session("DC_TABELLE"), DD_Mansione3)

        Dim Ospite As New ClsOspite

        Ospite.UpDateDropBox(Session("DC_OSPITE"), DD_Utente)


        Ospite.UpDateDropBox(Session("DC_OSPITE"), DD_Utente1)

        Ospite.UpDateDropBox(Session("DC_OSPITE"), DD_Utente2)

        Ospite.UpDateDropBox(Session("DC_OSPITE"), DD_Utente3)



        Dim cn As New OleDbConnection


        Dim ConnectionString As String = Session("DC_TABELLE")

        cn = New Data.OleDb.OleDbConnection(ConnectionString)

        cn.Open()


        Dim CmdP As New OleDbCommand
        CmdP.Connection = cn
        CmdP.CommandText = "Select * From Appalti_Prestazioni where IdAppalto =  ? And IdStrutture  = ? And  Data = ? And CodiceOperatore = ?"

        CmdP.Parameters.AddWithValue("@Appalto", Appalto)
        CmdP.Parameters.AddWithValue("@Struttura", Struttura)
        CmdP.Parameters.AddWithValue("@Data", DateSerial(Anno, Mese, Giorno))
        CmdP.Parameters.AddWithValue("@CodiceOperatore", CodiceOperatore)
        Dim NonFlag As Boolean = False

        LblMansione1.Visible = False
        lblOre1.Visible = False
        Txt_Ore1.Visible = False
        DD_Mansione1.Visible = False
        Lbl_Utente1.Visible = False
        DD_Utente1.Visible = False
        Btn_Second.Visible = True


        LblMansione2.Visible = False
        lblOre2.Visible = False
        Txt_Ore2.Visible = False
        DD_Mansione2.Visible = False
        Lbl_Utente2.Visible = False
        DD_Utente2.Visible = False
        Btn_Terzo.Visible = False

        LblMansione3.Visible = False
        lblOre3.Visible = False
        Txt_Ore3.Visible = False
        DD_Mansione3.Visible = False
        Lbl_Utente3.Visible = False
        DD_Utente3.Visible = False
        Btn_Quarto.Visible = False




        Dim MovimentiGiorno As OleDbDataReader = CmdP.ExecuteReader()
        If MovimentiGiorno.Read Then
            Txt_Ore.Text = campodbn(MovimentiGiorno.Item("Ore"))
            DD_Mansione.SelectedValue = campodbn(MovimentiGiorno.Item("IdMansione"))

            DD_Utente.SelectedValue = campodbn(MovimentiGiorno.Item("codiceospite"))
        End If
        If MovimentiGiorno.Read Then
            Txt_Ore1.Text = campodbn(MovimentiGiorno.Item("Ore"))
            DD_Mansione1.SelectedValue = campodbn(MovimentiGiorno.Item("IdMansione"))
            DD_Utente1.SelectedValue = campodbn(MovimentiGiorno.Item("codiceospite"))

            Btn_Second.Visible = False

            LblMansione1.Visible = True
            lblOre1.Visible = True
            Txt_Ore1.Visible = True
            DD_Mansione1.Visible = True
            DD_Utente1.Visible = True
            Lbl_Utente1.Visible = True

            Btn_Terzo.Visible = True
        End If
        CmdP.Clone()

        cn.Close()
    End Sub

    Function campodb(ByVal oggetto As Object) As String
        If IsDBNull(oggetto) Then
            Return ""
        Else
            Return oggetto
        End If
    End Function

    Function campodbn(ByVal oggetto As Object) As Double
        If IsDBNull(oggetto) Then
            Return 0
        Else
            Return oggetto
        End If
    End Function

    Function campodbd(ByVal oggetto As Object) As Date
        If IsDBNull(oggetto) Then
            Return Nothing
        Else
            Return oggetto
        End If
    End Function

    Protected Sub Txt_Ore_TextChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles Txt_Ore.TextChanged
        Scrivi()
    End Sub

    Private Sub Scrivi()
        Dim Anno As Integer
        Dim Mese As Integer
        Dim Giorno As Integer
        Dim CodiceOperatore As Integer
        Dim Appalto As Integer
        Dim Struttura As Integer

        Giorno = Request.Item("Giorno")
        CodiceOperatore = Request.Item("CodiceOperatore")

        Anno = Request.Item("Anno")


        Mese = Request.Item("Mese")

        Appalto = Request.Item("Appalto")
        Struttura = Request.Item("Struttura")


        Dim cn As New OleDbConnection


        Dim ConnectionString As String = Session("DC_TABELLE")

        cn = New Data.OleDb.OleDbConnection(ConnectionString)

        cn.Open()


        Dim CmdP As New OleDbCommand
        CmdP.Connection = cn
        CmdP.CommandText = "DELETE From Appalti_Prestazioni where IdAppalto =  ? And IdStrutture  = ? And  Data = ? And CodiceOperatore = ?"

        CmdP.Parameters.AddWithValue("@Appalto", Appalto)
        CmdP.Parameters.AddWithValue("@Struttura", Struttura)
        CmdP.Parameters.AddWithValue("@Data", DateSerial(Anno, Mese, Giorno))
        CmdP.Parameters.AddWithValue("@CodiceOperatore", CodiceOperatore)

        CmdP.ExecuteNonQuery()

        Dim CmdIns As New OleDbCommand
        If Val(DD_Mansione.SelectedValue) <> 0 And CDbl(Txt_Ore.Text) <> 0 Then

            CmdIns.Connection = cn

            CmdIns.CommandText = "INSERT INTO Appalti_Prestazioni (IdAppalto,IdStrutture,Data,CodiceOperatore,IdMansione,Ore,CodiceOspite) VALUES (?,?,?,?,?,?,?)"

            CmdIns.Parameters.AddWithValue("@Appalto", Appalto)
            CmdIns.Parameters.AddWithValue("@Struttura", Struttura)
            CmdIns.Parameters.AddWithValue("@Data", DateSerial(Anno, Mese, Giorno))
            CmdIns.Parameters.AddWithValue("@CodiceOperatore", CodiceOperatore)
            CmdIns.Parameters.AddWithValue("@IdMansione", Val(DD_Mansione.SelectedValue))
            CmdIns.Parameters.AddWithValue("@Ore", CDbl(Txt_Ore.Text))
            CmdIns.Parameters.AddWithValue("@CodiceOspite", Val(DD_Utente.SelectedValue))
            CmdIns.ExecuteNonQuery()
        End If

        If Txt_Ore1.Text <> "" Then
            If CDbl(Txt_Ore1.Text) > 0 Then
                CmdIns.Parameters.Clear()

                CmdIns.Connection = cn

                CmdIns.CommandText = "INSERT INTO Appalti_Prestazioni (IdAppalto,IdStrutture,Data,CodiceOperatore,IdMansione,Ore,CodiceOspite) VALUES (?,?,?,?,?,?,?)"

                CmdIns.Parameters.AddWithValue("@Appalto", Appalto)
                CmdIns.Parameters.AddWithValue("@Struttura", Struttura)
                CmdIns.Parameters.AddWithValue("@Data", DateSerial(Anno, Mese, Giorno))
                CmdIns.Parameters.AddWithValue("@CodiceOperatore", CodiceOperatore)
                CmdIns.Parameters.AddWithValue("@IdMansione", Val(DD_Mansione1.SelectedValue))
                CmdIns.Parameters.AddWithValue("@Ore", CDbl(Txt_Ore1.Text))
                CmdIns.Parameters.AddWithValue("@CodiceOspite", Val(DD_Utente1.SelectedValue))
                CmdIns.ExecuteNonQuery()
            End If
        End If


        If Txt_Ore2.Text <> "" Then
            If CDbl(Txt_Ore2.Text) > 0 Then
                CmdIns.Parameters.Clear()

                CmdIns.Connection = cn

                CmdIns.CommandText = "INSERT INTO Appalti_Prestazioni (IdAppalto,IdStrutture,Data,CodiceOperatore,IdMansione,Ore,CodiceOspite) VALUES (?,?,?,?,?,?,?)"

                CmdIns.Parameters.AddWithValue("@Appalto", Appalto)
                CmdIns.Parameters.AddWithValue("@Struttura", Struttura)
                CmdIns.Parameters.AddWithValue("@Data", DateSerial(Anno, Mese, Giorno))
                CmdIns.Parameters.AddWithValue("@CodiceOperatore", CodiceOperatore)
                CmdIns.Parameters.AddWithValue("@IdMansione", Val(DD_Mansione2.SelectedValue))
                CmdIns.Parameters.AddWithValue("@Ore", CDbl(Txt_Ore2.Text))
                CmdIns.Parameters.AddWithValue("@CodiceOspite", Val(DD_Utente2.SelectedValue))
                CmdIns.ExecuteNonQuery()
            End If
        End If

        If Txt_Ore3.Text <> "" Then
            If CDbl(Txt_Ore3.Text) > 0 Then
                CmdIns.Parameters.Clear()

                CmdIns.Connection = cn

                CmdIns.CommandText = "INSERT INTO Appalti_Prestazioni (IdAppalto,IdStrutture,Data,CodiceOperatore,IdMansione,Ore,CodiceOspite) VALUES (?,?,?,?,?,?,?)"

                CmdIns.Parameters.AddWithValue("@Appalto", Appalto)
                CmdIns.Parameters.AddWithValue("@Struttura", Struttura)
                CmdIns.Parameters.AddWithValue("@Data", DateSerial(Anno, Mese, Giorno))
                CmdIns.Parameters.AddWithValue("@CodiceOperatore", CodiceOperatore)
                CmdIns.Parameters.AddWithValue("@IdMansione", Val(DD_Mansione3.SelectedValue))
                CmdIns.Parameters.AddWithValue("@Ore", CDbl(Txt_Ore3.Text))
                CmdIns.Parameters.AddWithValue("@CodiceOspite", Val(DD_Utente3.SelectedValue))
                CmdIns.ExecuteNonQuery()
            End If
        End If


        cn.Close()
    End Sub

    Protected Sub DD_Mansione_TextChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles DD_Mansione.TextChanged
        Scrivi()
    End Sub

    Protected Sub Txt_Ore1_TextChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles Txt_Ore1.TextChanged
        Scrivi()
    End Sub

    Protected Sub DD_Mansione1_TextChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles DD_Mansione1.TextChanged
        Scrivi()
    End Sub

    Protected Sub Btn_Second_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles Btn_Second.Click
        Btn_Second.Visible = False

        LblMansione1.Visible = True
        lblOre1.Visible = True
        Txt_Ore1.Visible = True
        DD_Mansione1.Visible = True

        DD_Utente1.Visible = True
        Lbl_Utente1.Visible = True
        Btn_Terzo.Visible = True

    End Sub

    Protected Sub Btn_Terzo_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles Btn_Terzo.Click
        Btn_Terzo.Visible = False

        LblMansione2.Visible = True
        lblOre2.Visible = True
        Txt_Ore2.Visible = True
        DD_Mansione2.Visible = True

        DD_Utente2.Visible = True
        Lbl_Utente2.Visible = True
        Btn_Quarto.Visible = True

    End Sub

    Protected Sub Btn_Quarto_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles Btn_Quarto.Click
        Btn_Quarto.Visible = False

        LblMansione3.Visible = True
        lblOre3.Visible = True
        Txt_Ore3.Visible = True
        DD_Mansione3.Visible = True

        DD_Utente3.Visible = True
        Lbl_Utente3.Visible = True
        
    End Sub
End Class
