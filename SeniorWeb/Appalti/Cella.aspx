﻿<%@ Page Language="VB" AutoEventWireup="false" Inherits="Appalti_Cella" CodeFile="Cella.aspx.vb" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <asp:PlaceHolder runat="server">
        <%: System.Web.Optimization.Styles.Render("~/Content/AjaxControlToolkit/Styles/Bundle") %>
    </asp:PlaceHolder>
    <link href="ospiti.css?ver=11" rel="stylesheet" type="text/css" />
    <link rel="shortcut icon" href="images/SENIOR.ico" />
    <link href="js/jquery.autocomplete.css" rel="stylesheet" type="text/css" />


    <script src="js/jquery-1.5.1.min.js" type="text/javascript"></script>
    <script src="js/jquery.autocomplete.js" type="text/javascript"></script>
    <script src="js/soapclient.js" type="text/javascript"></script>
    <script src="/js/convertinumero.js" type="text/javascript"></script>


    <script src="/js/listacomuni.js" type="text/javascript"></script>
    <script src="js/jquery.maskedinput-1.3.min.js" type="text/javascript"></script>
    <script src="/js/formatnumer.js" type="text/javascript"></script>
    <script src="js/JSErrore.js" type="text/javascript"></script>
    <link rel="stylesheet" href="jqueryui/jquery-ui.css" type="text/css">
    <script src="jqueryui/jquery-ui.js" type="text/javascript"></script>
    <script src="jqueryui/datapicker-it.js" type="text/javascript"></script>
    <title></title>
</head>
<body>
    <form id="form1" runat="server">
        <asp:ScriptManager ID="ScriptManager1" runat="server">
            <Scripts>
                <asp:ScriptReference Path="~/Scripts/AjaxControlToolkit/Bundle" />
            </Scripts>
        </asp:ScriptManager>
        <div>
            <asp:UpdatePanel ID="UpdatePanel1" runat="server">
                <ContentTemplate>
                    Ore :<br />
                    <asp:TextBox ID="Txt_Ore" onkeypress="return soloNumeri(event);" Width="50px" runat="server" AutoPostBack="true"></asp:TextBox>
                    <br />
                    Mansione :<br />
                    <asp:DropDownList ID="DD_Mansione" AutoPostBack="true" runat="server"></asp:DropDownList>
                    Utente :<br />
                    <asp:DropDownList ID="DD_Utente" AutoPostBack="true" runat="server"></asp:DropDownList>
                    <br />
                    <asp:Button ID="Btn_Second" runat="server" Text="+" />
                    <br />
                    <br />
                    <asp:Label ID="lblOre1" runat="server" Text="Ore 2"></asp:Label><br />
                    <asp:TextBox ID="Txt_Ore1" onkeypress="return soloNumeri(event);" Width="50px" runat="server" AutoPostBack="true"></asp:TextBox>
                    <br />
                    <asp:Label ID="LblMansione1" runat="server" Text="Mansione 2"></asp:Label>
                    <asp:DropDownList ID="DD_Mansione1" AutoPostBack="true" runat="server"></asp:DropDownList><br />
                    <asp:Label ID="Lbl_Utente1" runat="server" Text="Utente 2"></asp:Label>
                    <asp:DropDownList ID="DD_Utente1" AutoPostBack="true" runat="server"></asp:DropDownList><br />
                    <br />
                    <asp:Button ID="Btn_Terzo" runat="server" Text="+" />
                    <br />
                    <br />
                    <asp:Label ID="lblOre2" runat="server" Text="Ore 3"></asp:Label><br />
                    <asp:TextBox ID="Txt_Ore2" onkeypress="return soloNumeri(event);" Width="50px" runat="server" AutoPostBack="true"></asp:TextBox>
                    <br />
                    <asp:Label ID="LblMansione2" runat="server" Text="Mansione 3"></asp:Label>
                    <asp:DropDownList ID="DD_Mansione2" AutoPostBack="true" runat="server"></asp:DropDownList><br />
                    <asp:Label ID="Lbl_Utente2" runat="server" Text="Utente 3"></asp:Label>
                    <asp:DropDownList ID="DD_Utente2" AutoPostBack="true" runat="server"></asp:DropDownList><br />
                    <br />
                    <asp:Button ID="Btn_Quarto" runat="server" Text="+" />
                    <br />
                    <br />
                    <asp:Label ID="lblOre3" runat="server" Text="Ore 4"></asp:Label><br />
                    <asp:TextBox ID="Txt_Ore3" onkeypress="return soloNumeri(event);" Width="50px" runat="server" AutoPostBack="true"></asp:TextBox>
                    <br />
                    <asp:Label ID="LblMansione3" runat="server" Text="Mansione 4"></asp:Label>
                    <asp:DropDownList ID="DD_Mansione3" AutoPostBack="true" runat="server"></asp:DropDownList><br />
                    <asp:Label ID="Lbl_Utente3" runat="server" Text="Utente 4"></asp:Label>
                    <asp:DropDownList ID="DD_Utente3" AutoPostBack="true" runat="server"></asp:DropDownList><br />
                </ContentTemplate>
            </asp:UpdatePanel>
        </div>

    </form>
</body>
</html>
