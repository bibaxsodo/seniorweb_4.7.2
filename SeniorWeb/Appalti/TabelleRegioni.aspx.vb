﻿Imports System.Web.Hosting
Imports System.Data.OleDb
Imports System.Data.DataView
Partial Class Appalti_TabelleRegioni
    Inherits System.Web.UI.Page
    Dim MyTable As New System.Data.DataTable("tabella")
    Dim MyDataSet As New System.Data.DataSet()

    Dim MyTableImp As New System.Data.DataTable("tabella")
    Dim MyDataSetImp As New System.Data.DataSet()


    Dim MyEccezioni As New System.Data.DataTable("Eccezioni")
    Dim DSEccezioni As New System.Data.DataSet()


    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Session("UTENTE") = "" Then
            Response.Redirect("..\Login.aspx")
            Exit Sub
        End If

        EseguiJS()

        If Page.IsPostBack = True Then
            Call EseguiJS()
            Exit Sub
        End If



        Dim K1 As New Cls_SqlString

        Dim Barra As New Cls_BarraSenior

        If Not IsNothing(Session("RicercaAnagraficaSQLString")) Then
            Try
                K1 = Session("RicercaAnagraficaSQLString")
            Catch ex As Exception
                K1 = Nothing
            End Try
        End If

        Lbl_BarraSenior.Text = Barra.CodiceBarra(Application("SENIOR"), Session("UTENTE"), Page, K1)

        Dim ConnectionString As String = Session("DC_OSPITE")
        Dim ConnectionStringTabelle As String = Session("DC_TABELLE")
        Dim k As New ClsUSL


        If Request.Item("CODICE") = "" Then

            Txt_Codice.Text = k.MaxRegione(Session("DC_OSPITE"))


            Exit Sub
        End If


        Dim DDModalita As New ClsModalitaPagamento
        DDModalita.UpDateDropBox(ConnectionString, DD_ModalitaPagamento)

        k.CodiceRegione = Request.Item("CODICE")
        k.Leggi(ConnectionString)
        Txt_Codice.Text = k.CodiceRegione

        Txt_Codice.Enabled = False

        Txt_Descrizione.Text = k.Nome
        Txt_NomeConiuge.Text = k.NonInVisualizzazione
        Txt_Indirizzo.Text = k.RESIDENZAINDIRIZZO1
        Txt_Cap.Text = k.RESIDENZACAP1
        Txt_Attenzione.Text = k.Attenzione
        Txt_Piva.Text = k.PARTITAIVA
        Txt_CodiceFiscale.Text = k.CodiceFiscale

        DD_ModalitaPagamento.SelectedValue = k.ModalitaPagamento


        Dim MyRes As New ClsComune

        MyRes.Descrizione = ""
        MyRes.Provincia = k.RESIDENZAPROVINCIA1
        MyRes.Comune = k.RESIDENZACOMUNE1
        MyRes.Leggi(Session("DC_OSPITE"))
        If k.RESIDENZAPROVINCIA1 <> "" Then
            Txt_ComRes.Text = k.RESIDENZAPROVINCIA1 & " " & k.RESIDENZACOMUNE1 & " " & MyRes.Descrizione
        Else
            Txt_ComRes.Text = ""
        End If



        TxT_Mail.Text = k.RESIDENZATELEFONO3


        Txt_Referente.Text = k.RecapitoNome
        Txt_TelefonoReferente.Text = k.RESIDENZATELEFONO4



        Txt_CodiceDestinatario.Text = k.CodiceDestinatario




        Dim ks As New Cls_Pianodeiconti

        ks.Mastro = k.MastroCliente
        ks.Conto = k.ContoCliente
        ks.Sottoconto = k.SottoContoCliente
        ks.Decodfica(Session("DC_GENERALE"))

        Txt_Sottoconto.Text = ks.Mastro & " " & ks.Conto & " " & ks.Sottoconto & " " & ks.Descrizione





        'Dim kSA As String
        'kSA = m(0)(1).ToString

        If Val(Txt_Piva.Text) > 0 Then
            Dim Stringa As String = Format(Val(Txt_Piva.Text), "00000000000")

            Dim VerCf As New Cls_CodiceFiscale

            If VerCf.CheckPartitaIva(Stringa) = False Then
                Img_VerPIVA.ImageUrl = "~/images/errore.gif"
            Else
                Img_VerPIVA.ImageUrl = "~/images/Blanco.png"
            End If
        End If

    End Sub




    Protected Sub ModificaRegione()
        Dim k As New ClsUSL
        Dim ConnectionString As String = Session("DC_OSPITE")

        k.CodiceRegione = Txt_Codice.Text
        k.CodiceRegione = Txt_Codice.Text
        k.Nome = Txt_Descrizione.Text
        k.NonInVisualizzazione = Txt_NomeConiuge.Text
        k.RESIDENZAINDIRIZZO1 = Txt_Indirizzo.Text
        k.RESIDENZACAP1 = Txt_Cap.Text
        k.Attenzione = Txt_Attenzione.Text
        k.PARTITAIVA = Val(Txt_Piva.Text)
        k.CodiceFiscale = Txt_CodiceFiscale.Text

        k.ModalitaPagamento = DD_ModalitaPagamento.SelectedValue

        k.CodiceDestinatario = Txt_CodiceDestinatario.Text

        Dim Vettore(100) As String

        If Txt_ComRes.Text <> "" Then
            Vettore = SplitWords(Txt_ComRes.Text)
            If Vettore.Length > 1 Then
                k.RESIDENZAPROVINCIA1 = Vettore(0)
                k.RESIDENZACOMUNE1 = Vettore(1)
            End If
        Else
            k.RESIDENZAPROVINCIA1 = ""
            k.RESIDENZACOMUNE1 = ""
        End If

 


        If Txt_Sottoconto.Text <> "" Then
            Vettore = SplitWords(Txt_Sottoconto.Text)
            If Vettore.Length > 1 Then
                k.MastroCliente = Vettore(0)
                k.ContoCliente = Vettore(1)
                k.SottoContoCliente = Vettore(2)
            End If
        Else
            k.MastroCliente = 0
            k.ContoCliente = 0
            k.SottoContoCliente = 0
        End If

        If k.SottoContoCliente = 0 Then
            Dim TabellaParametri As New Cls_Parametri

            TabellaParametri.LeggiParametri(Session("DC_OSPITE"))

            Dim PianoConti As New Cls_Pianodeiconti

            PianoConti.Mastro = TabellaParametri.Mastro
            PianoConti.Conto = TabellaParametri.ContoRegione
            PianoConti.Sottoconto = k.CodiceRegione
            PianoConti.Decodfica(Session("DC_GENERALE"))

            If PianoConti.Descrizione = "" Then
                PianoConti.Mastro = TabellaParametri.Mastro
                PianoConti.Conto = TabellaParametri.ContoRegione
                PianoConti.Sottoconto = k.CodiceRegione
                PianoConti.Descrizione = Txt_Descrizione.Text
                PianoConti.Tipo = "A"
                PianoConti.TipoAnagrafica = "R"
                PianoConti.Scrivi(Session("DC_GENERALE"))

                k.MastroCliente = PianoConti.Mastro
                k.ContoCliente = PianoConti.Conto
                k.SottoContoCliente = PianoConti.Sottoconto
            End If
        End If



        k.RecapitoNome = Txt_Referente.Text
        k.RESIDENZATELEFONO4 = Txt_TelefonoReferente.Text



        k.RESIDENZATELEFONO3 = TxT_Mail.Text



        k.ScriviRegione(ConnectionString)





    End Sub



    Private Function SplitWords(ByVal s As String) As String()
        '
        ' Call Regex.Split function from the imported namespace.
        ' Return the result array.
        '
        Return Regex.Split(s, "\W+")
    End Function

    Protected Sub Btn_Modifica_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles Btn_Modifica.Click
        If Txt_Codice.Text.Trim = "" Then
            ClientScript.RegisterClientScriptBlock(Me.GetType(), "Errore", "VisualizzaErrore('Digitare codice');", True)
            Exit Sub
        End If
        If Txt_Descrizione.Text.Trim = "" Then
            ClientScript.RegisterClientScriptBlock(Me.GetType(), "Errore", "VisualizzaErrore('Digitare descrizione');", True)
            Exit Sub
        End If

        If Txt_Codice.Enabled = True Then
            Dim k As New ClsUSL
            Dim ConnectionString As String = Session("DC_OSPITE")

            k.CodiceRegione = Txt_Codice.Text
            k.Leggi(ConnectionString)
            If k.Nome <> "" Then
                ClientScript.RegisterClientScriptBlock(Me.GetType(), "Errore", "VisualizzaErrore('Regione già presente con questo codice');", True)
                Exit Sub
            End If
        End If




        Call ModificaRegione()

        Call PaginaPrecedente()
    End Sub

    Protected Sub Btn_Elimina_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles Btn_Elimina.Click
        If Txt_Codice.Text.Trim = "" Then
            ClientScript.RegisterClientScriptBlock(Me.GetType(), "Errore", "VisualizzaErrore('Digitare codice');", True)
            Exit Sub
        End If

        Dim X As New ClsUSL

        X.CodiceRegione = Txt_Codice.Text
        X.EliminaRegione(Session("DC_OSPITE"))

        Call PaginaPrecedente()
    End Sub





    Private Sub EseguiJS()
        Dim MyJs As String
        MyJs = "$(document).ready(function() {"

        MyJs = MyJs & "var els = document.getElementsByTagName(""*"");"

        MyJs = MyJs & "for (var i=0;i<els.length;i++)"
        MyJs = MyJs & "if ( els[i].id ) { "
        MyJs = MyJs & " var appoggio =els[i].id; "
        MyJs = MyJs & " if (appoggio.match('TxtData')!= null) {  "
        MyJs = MyJs & " $(els[i]).mask(""99/99/9999"");"

        MyJs = MyJs & " $(els[i]).datepicker({ changeMonth: true,changeYear: true,  yearRange: ""1990:" & Year(Now) + 5 & """},$.datepicker.regional[""it""]);"

        MyJs = MyJs & "    }"

        MyJs = MyJs & " if ((appoggio.match('TxtImporto')!= null) ) {  "
        MyJs = MyJs & " $(els[i]).keypress(function() { ForceNumericInput($(this).val(), true, true); } ); "
        MyJs = MyJs & " $(els[i]).blur(function() { var ap = formatNumber($(this).val(),2); $(this).val(ap); });"
        MyJs = MyJs & "    }"

        MyJs = MyJs & " if (appoggio.match('Txt_ComRes')!= null)  {  "
        MyJs = MyJs & " $(els[i]).autocomplete('/WebHandler/AutocompleteComune.ashx?Utente=" & Session("UTENTE") & "', {delay:5,minChars:3});"
        MyJs = MyJs & "    }"

        MyJs = MyJs & " if (appoggio.match('Txt_Sottoconto')!= null) {   "
        MyJs = MyJs & " $(els[i]).autocomplete('/WebHandler/GestioneConto.ashx?Utente=" & Session("UTENTE") & "', {delay:5,minChars:3});"
        MyJs = MyJs & "    }"

        MyJs = MyJs & "} "
        MyJs = MyJs & "});"

        'MyJs = "$(document).ready(function() { $('.myClass').keypress(function() { handleEnter($('.myClass').val(), true, true); } );     $('#" & Txt_ImportoPagato.ClientID & "').blur(function() { var ap = formatNumber ($('#" & Txt_ImportoPagato.ClientID & "').val(),2); $('#" & Txt_ImportoPagato.ClientID & "').val(ap); }); });"
        ScriptManager.RegisterStartupScript(Me, Me.GetType(), "visualizzaRitIm", MyJs, True)
    End Sub



    Protected Sub Btn_Esci_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles Btn_Esci.Click

        Call PaginaPrecedente()
    End Sub


    Private Sub PaginaPrecedente()
        If Request.Item("PAGINA") = "" Then
            Response.Redirect("ElencoRegione.aspx")
        Else
            Response.Redirect("ElencoRegione.aspx?CHIAMATA=RITORNO&PAGINA=" & Request.Item("PAGINA"))
        End If
    End Sub

    Private Function MaxCodReg() As String

        MaxCodReg = ""
        Dim cn As New OleDbConnection


        Dim ConnectionString As String = Session("DC_OSPITE")

        cn = New Data.OleDb.OleDbConnection(ConnectionString)

        cn.Open()

        Dim cmd As New OleDbCommand
        cmd.CommandText = "Select max(CodiceRegione) From AnagraficaComune Where Tipologia ='R' "



        Dim myPOSTreader As OleDbDataReader = cmd.ExecuteReader()
        If myPOSTreader.Read Then
            MaxCodReg = campodbn(myPOSTreader.Item(0))
        End If
        myPOSTreader.Close()
        cn.Close()

        If MaxCodReg = "0" Then
            MaxCodReg = ""
        End If
    End Function
    Function campodbn(ByVal oggetto As Object) As Double
        If IsDBNull(oggetto) Then
            Return 0
        Else
            Return oggetto
        End If
    End Function



    Protected Sub Btn_Duplica_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles Btn_Duplica.Click

        Txt_Codice.Text = ""
        Txt_Codice.Enabled = True

    End Sub



    Protected Sub Txt_Piva_TextChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles Txt_Piva.TextChanged
        If Val(Txt_Piva.Text) > 0 Then
            Dim Stringa As String = Format(Val(Txt_Piva.Text), "00000000000")

            Dim VerCf As New Cls_CodiceFiscale

            If VerCf.CheckPartitaIva(Stringa) = False Then
                Img_VerPIVA.ImageUrl = "~/images/errore.gif"
            Else
                Img_VerPIVA.ImageUrl = "~/images/Blanco.png"
            End If
        End If
    End Sub
End Class
