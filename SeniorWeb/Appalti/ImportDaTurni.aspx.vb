﻿Imports Microsoft.VisualBasic
Imports System.Data.Odbc
Imports System.Web.Hosting
Imports System.Data.OleDb
Imports System.IO
Imports System.Text
Imports System.Data


Partial Class Appalti_ImportDaTurni
    Inherits System.Web.UI.Page
    Dim Tabella As New System.Data.DataTable("tabella")
    Private Function ImportFileEpersonam(ByVal Appoggio As String) As String
        Dim cn As OdbcConnection
        Dim Riga As Integer

        Dim NomeSocieta As String


        If FileUpload1.FileName = "" Then
            ClientScript.RegisterClientScriptBlock(Me.GetType(), "Errore", "VisualizzaErrore('File non indicato');", True)
            Exit Function
        End If

        Dim k As New Cls_Login

        k.Utente = Session("UTENTE")
        k.LeggiSP(Application("SENIOR"))

        NomeSocieta = k.NomeEPersonam



        FileUpload1.SaveAs(HostingEnvironment.ApplicationPhysicalPath() & "\Public\Turni_Import_" & Appoggio & ".txt")

        If Dir(HostingEnvironment.ApplicationPhysicalPath() & "\Public\Turni_Import_" & Appoggio & ".txt") = "" Then
            ClientScript.RegisterClientScriptBlock(Me.GetType(), "Errore", "VisualizzaErrore('File txt non trovata');", True)
            Exit Function
        End If


        Tabella.Clear()

        Tabella.Columns.Add("IdAppalto", GetType(String))
        Tabella.Columns.Add("IdStruttura", GetType(String))
        Tabella.Columns.Add("IdMansione", GetType(String))

        Tabella.Columns.Add("Appalto", GetType(String))
        Tabella.Columns.Add("CentroDiCosto", GetType(String))
        Tabella.Columns.Add("CodiceOperatore", GetType(String))
        Tabella.Columns.Add("CodiceFiscale", GetType(String))
        Tabella.Columns.Add("CognomeNome", GetType(String))
        Tabella.Columns.Add("Mansione", GetType(String))
        Tabella.Columns.Add("Data", GetType(String))
        Tabella.Columns.Add("Ore", GetType(String))
        Tabella.Columns.Add("Segnalazioni", GetType(String))
        Dim objStreamReader As StreamReader
        objStreamReader = File.OpenText(HostingEnvironment.ApplicationPhysicalPath() & "\Public\Turni_Import_" & Appoggio & ".TXT")


        Dim Testo As String = ""
        Dim CodiceFiscale As String = ""
        Dim Cognome As String = ""
        Dim Nome As String = ""
        Dim Matricola As String = ""
        Dim CentroDiCosto As String = ""
        Dim Mansione As String = ""


        Riga = 0
        Do While Not objStreamReader.EndOfStream
            Riga = Riga + 1
            Testo = objStreamReader.ReadLine

            Dim Data As String
            Dim Ora As String
            Dim Tipo As String
            CodiceFiscale = ""
            Dim Attivita As String
            Dim Struttura As String

            '201812101030eNRNRNRVJDIDKBB31000293440092600000
            '1234567812341123456789012345612345678
            '1234567890123456789012345678901234567890



            Data = Mid(Testo & Space(100), 1, 8)
            Ora = Mid(Testo & Space(100), 9, 4)
            Tipo = Mid(Testo & Space(100), 13, 1)
            CodiceFiscale = Mid(Testo & Space(100), 14, 16)
            Attivita = Mid(Testo & Space(100), 30, 8)
            Struttura = Mid(Testo & Space(100), 38, 5)



            Dim StrutturaCls As New Cls_AppaltiTabellaStruttura

            StrutturaCls.LeggiCentroDiCosto(Session("DC_TABELLE"), Struttura)



            Dim AppaltoStruttureCls As New Cls_Appalti_AppaltiStrutture

            AppaltoStruttureCls.IdStrutture = StrutturaCls.ID
            AppaltoStruttureCls.LeggiAppaltoDaStruttura(Session("DC_TABELLE"))

            Dim MansioneCls As New Cls_AppaltiTabellaMansione


            MansioneCls.LeggiImport(Session("DC_TABELLE"), Attivita)

            Dim AppaltoCls As New Cls_AppaltiAnagrafica


            AppaltoCls.Id = AppaltoStruttureCls.IdAppalto
            AppaltoCls.Leggi(Session("DC_TABELLE"))

            Dim Operatore As New Cls_Operatore

            Operatore.CodiceFiscale = CodiceFiscale
            Operatore.LeggiPerCF(Session("DC_TABELLE"))

            Dim myriga As System.Data.DataRow = Tabella.NewRow()

            myriga(1) = AppaltoStruttureCls.IdAppalto
            myriga(2) = StrutturaCls.ID
            myriga(3) = MansioneCls.ID
            myriga(4) = AppaltoCls.Descrizione
            myriga(5) = Attivita

            myriga(6) = Operatore.CodiceMedico
            myriga(7) = Operatore.CodiceFiscale
            myriga(8) = Operatore.Nome
            myriga(9) = MansioneCls.Descrizione
            myriga(10) = Data
            myriga(11) = Ora




            'Tabella.Columns.Add("IdAppalto", GetType(String))
            'Tabella.Columns.Add("IdStruttura", GetType(String))
            'Tabella.Columns.Add("IdMansione", GetType(String))

            'Tabella.Columns.Add("Appalto", GetType(String))
            'Tabella.Columns.Add("CentroDiCosto", GetType(String))
            'Tabella.Columns.Add("CodiceOperatore", GetType(String))
            'Tabella.Columns.Add("CodiceFiscale", GetType(String))
            'Tabella.Columns.Add("CognomeNome", GetType(String))
            'Tabella.Columns.Add("Mansione", GetType(String))
            'Tabella.Columns.Add("Data", GetType(String))
            'Tabella.Columns.Add("Ore", GetType(String))
            'Tabella.Columns.Add("Segnalazioni", GetType(String))

            Tabella.Rows.Add(myriga)
        Loop



        ViewState("Appoggio") = Tabella

        Grd_ImportTurni.AutoGenerateColumns = False

        Grd_ImportTurni.DataSource = Tabella

        Grd_ImportTurni.DataBind()
        Grd_ImportTurni.Visible = True

    End Function




    Private Function ImportFile(ByVal Appoggio As String) As String
        Dim cn As OdbcConnection
        Dim Riga As Integer

        Dim NomeSocieta As String


        If FileUpload1.FileName = "" Then
            ClientScript.RegisterClientScriptBlock(Me.GetType(), "Errore", "VisualizzaErrore('File non indicato');", True)
            Exit Function
        End If

        Dim k As New Cls_Login

        k.Utente = Session("UTENTE")
        k.LeggiSP(Application("SENIOR"))

        NomeSocieta = k.NomeEPersonam



        FileUpload1.SaveAs(HostingEnvironment.ApplicationPhysicalPath() & "\Public\Turni_Import_" & Appoggio & ".txt")

        If Dir(HostingEnvironment.ApplicationPhysicalPath() & "\Public\Turni_Import_" & Appoggio & ".txt") = "" Then
            ClientScript.RegisterClientScriptBlock(Me.GetType(), "Errore", "VisualizzaErrore('File txt non trovata');", True)
            Exit Function
        End If


        Tabella.Clear()

        Tabella.Columns.Add("IdAppalto", GetType(String))
        Tabella.Columns.Add("IdStruttura", GetType(String))
        Tabella.Columns.Add("IdMansione", GetType(String))

        Tabella.Columns.Add("Appalto", GetType(String))
        Tabella.Columns.Add("CentroDiCosto", GetType(String))
        Tabella.Columns.Add("CodiceOperatore", GetType(String))
        Tabella.Columns.Add("CodiceFiscale", GetType(String))
        Tabella.Columns.Add("CognomeNome", GetType(String))
        Tabella.Columns.Add("Mansione", GetType(String))
        Tabella.Columns.Add("Data", GetType(String))
        Tabella.Columns.Add("Ore", GetType(String))
        Tabella.Columns.Add("Segnalazioni", GetType(String))

        Tabella.Columns.Add("intypecod", GetType(String))
        Tabella.Columns.Add("intypedescription", GetType(String))

        Dim objStreamReader As StreamReader
        objStreamReader = File.OpenText(HostingEnvironment.ApplicationPhysicalPath() & "\Public\Turni_Import_" & Appoggio & ".TXT")


        Dim Testo As String = ""
        Dim CodiceFiscale As String = ""
        Dim Cognome As String = ""
        Dim Nome As String = ""
        Dim Matricola As String = ""
        Dim CentroDiCosto As String = ""
        Dim Mansione As String = ""
        Dim intypecod As String = ""
        Dim intypedescription As String = ""

        Riga = 0
        Do While Not objStreamReader.EndOfStream
            Riga = Riga + 1
            Testo = objStreamReader.ReadLine

            If Mid(Testo, 1, 2) = "00" Then
                CodiceFiscale = Mid(Testo, 3, 16)
                Cognome = Mid(Testo, 19, 16)
                Nome = Mid(Testo, 34, 16)
                Matricola = Mid(Testo, 51, 10)
                CentroDiCosto = Mid(Testo, 71, 16)
                Mansione = Mid(Testo, 61, 10)
                If Chk_Cus.Checked = True Then
                    Dim Trascodifica As New Cls_TabellaTrascodificheEsportazioni

                    intypecod = Mid(Mansione & Space(10), 6, 4)
                    intypedescription = ""

                    Trascodifica.EXPORT = intypecod
                    Trascodifica.TIPOTAB = "ST"
                    Trascodifica.LeggiDaExport(Session("DC_TABELLE"))
                    If Trascodifica.SENIOR <> "" Then
                        intypecod = Trascodifica.SENIOR

                        Dim TipoUtente As New Cls_Appalti_TipoUtente

                        TipoUtente.intypecod = intypecod
                        TipoUtente.Leggi(Session("DC_OSPITE"), TipoUtente.intypecod)
                        intypedescription = TipoUtente.intypedescription
                    End If

                    Mansione = Mid(Mansione & Space(10), 1, 4)
                End If
            End If

            If Mid(Testo, 1, 2) = "01" Then


          
                'If Chk_Cus.Checked = True Then
                '    Dim Trascodifica As New Cls_TabellaTrascodificheEsportazioni


                '    'Trascodifica.EXPORT = CentroDiCosto.Trim
                '    'Trascodifica.TIPOTAB = "ST"
                '    'Trascodifica.LeggiDaExport(Session("DC_TABELLE"))
                '    'If Trascodifica.SENIOR <> "" Then


                '    '    Dim StrutturaLet As New Cls_AppaltiTabellaStruttura

                '    '    StrutturaLet.ID = Val(Trascodifica.SENIOR)
                '    '    StrutturaLet.Leggi(Session("DC_TABELLE"), StrutturaLet.ID)

                '    '    If StrutturaLet.CodificaImport = "" Then
                '    '        StrutturaLet.CodificaImport = ""
                '    '    End If
                '    '    CentroDiCosto = StrutturaLet.CodificaImport

                '    'End If


                '    Dim TrascodificaMansione As New Cls_TabellaTrascodificheEsportazioni
                '    If Mansione = "5800100018" Then
                '        intypecod = "MIN"
                '        intypedescription = "MINORI"
                '    End If
                '    If Mansione = "5800100012" Then
                '        intypecod = "ANA"
                '        intypedescription = "ANZIANI NON AUTO"
                '    End If

                '    TrascodificaMansione.EXPORT = Mansione
                '    TrascodificaMansione.TIPOTAB = "MN"
                '    TrascodificaMansione.LeggiDaExport(Session("DC_TABELLE"))
                '    If TrascodificaMansione.SENIOR <> "" Then

                '        Dim MansioneLet As New Cls_AppaltiTabellaMansione

                '        MansioneLet.ID = Val(TrascodificaMansione.SENIOR)
                '        MansioneLet.Leggi(Session("DC_TABELLE"), MansioneLet.ID)
                '        Mansione = MansioneLet.CodificaImport
                '    End If
                'End If

                Dim Segnalazioni As String = ""

                Dim myriga As System.Data.DataRow = Tabella.NewRow()

                Dim Struttura As New Cls_AppaltiTabellaStruttura


                If IsNothing(CentroDiCosto) Then CentroDiCosto = ""
           

                Struttura.ID = 0
                Struttura.LeggiCentroDiCosto(Session("DC_TABELLE"), CentroDiCosto)


                If Struttura.ID = 0 Then
                    Segnalazioni = Segnalazioni & "[Centro di costo non trovato " & CentroDiCosto & "]"
                End If

                Dim AppaltiStrutture As New Cls_Appalti_AppaltiStrutture



                AppaltiStrutture.IdStrutture = Struttura.ID
                AppaltiStrutture.LeggiAppaltoDaStruttura(Session("DC_TABELLE"))

                If AppaltiStrutture.IdAppalto = 0 Then
                    Segnalazioni = Segnalazioni & "[Appalto non trovato per il centro di costo]"
                End If


                Dim MansioneCls As New Cls_AppaltiTabellaMansione


                If IsNothing(Mansione) Then Mansione = ""

                MansioneCls.ID = 0
                MansioneCls.LeggiImport(Session("DC_TABELLE"), Mansione.Trim)


                If MansioneCls.ID = 0 Then
                    Segnalazioni = Segnalazioni & "[Mansione non trovata " & Mansione & "]"
                End If




                Dim StrData As String
                Dim Gironi As Integer
                Dim Mese As Integer
                Dim Anno As Integer

                StrData = Mid(Testo, 19, 8)
                '20180801

                Anno = Mid(StrData, 1, 4)
                Mese = Mid(StrData, 5, 2)
                Gironi = Mid(StrData, 7, 2)

                Dim Ore As String
                Dim OreInCentesimi As Double

                Ore = Mid(Testo, 27, 5).Replace(".", ",").Replace(":", ",")

                Dim Minuti As Integer
                Dim MinutiInCentesimi As Double
                Minuti = (Ore - Int(Ore)) * 100

                MinutiInCentesimi = Math.Round(100 / (60 / Minuti), 2)

                OreInCentesimi = Int(Ore) + (MinutiInCentesimi / 100)



                myriga(0) = AppaltiStrutture.IdAppalto

                myriga(1) = Struttura.ID
                myriga(2) = MansioneCls.ID

                Dim AppaltoAnagr As New Cls_AppaltiAnagrafica


                AppaltoAnagr.Id = AppaltiStrutture.IdAppalto
                AppaltoAnagr.Leggi(Session("DC_TABELLE"))


                myriga(3) = AppaltoAnagr.Descrizione
                myriga(4) = CentroDiCosto

                Dim CercaOpe As New Cls_Operatore


                CercaOpe.CodiceFiscale = CodiceFiscale
                CercaOpe.LeggiPerCF(Session("DC_OSPITE"))


                myriga(5) = CercaOpe.CodiceMedico


                myriga(6) = CodiceFiscale.Trim
                myriga(7) = Cognome.Trim & " " & Nome.Trim

                myriga(8) = MansioneCls.Descrizione

                myriga(9) = Format(DateSerial(Anno, Mese, Gironi), "dd/MM/yyyy")

                myriga(10) = Format(OreInCentesimi, "#,##0.00")

                myriga(11) = intypecod
                myriga(12) = intypedescription


                If MansioneCls.ID = 0 Then
                    Segnalazioni = Segnalazioni & "[Mansione non trovata]"
                End If

                If Segnalazioni = "" Then
                    Dim Prestazioni As New Cls_Appalti_Prestazioni


                    Prestazioni.IdAppalto = AppaltiStrutture.IdAppalto
                    Prestazioni.IdMansione = MansioneCls.ID
                    Prestazioni.IdStrutture = Struttura.ID
                    Prestazioni.CodiceOperatore = Val(CercaOpe.CodiceMedico)
                    Prestazioni.Data = DateSerial(Anno, Mese, Gironi)
                    Prestazioni.LeggiPrestazioneGiorno(Session("DC_TABELLE"))

                    If Prestazioni.ID > 0 Then
                        Segnalazioni = Segnalazioni & "[già presnete ID " & Prestazioni.ID & "]"

                        If OreInCentesimi <> Prestazioni.Ore Then
                            Segnalazioni = Segnalazioni & " Ore diverse, ore senior " & Prestazioni.Ore
                        End If
                    End If

                End If
                myriga(11) = Segnalazioni


                Tabella.Rows.Add(myriga)
            End If

        Loop




        ViewState("Appoggio") = Tabella

        Grd_ImportTurni.AutoGenerateColumns = False

        Grd_ImportTurni.DataSource = Tabella

        Grd_ImportTurni.DataBind()
        Grd_ImportTurni.Visible = True

        'Dim Indice As Integer

        'For Indice = 0 To Grd_ImportTurni.Rows.Count - 1
        '    Dim Chekc As CheckBox = DirectCast(Grd_ImportTurni.Rows(Indice).FindControl("Chk_Selezionato"), CheckBox)
        '    If Tabella.Rows(Indice).Item(11) = "" Then
        '        Chekc.Checked = True
        '    End If
        'Next



    End Function




    Protected Sub Btn_Modifica_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles Btn_Modifica.Click
        Dim Indice As Integer

        Tabella = ViewState("Appoggio")

        For Indice = 0 To Grd_ImportTurni.Rows.Count - 1
            Dim Chekc As CheckBox = DirectCast(Grd_ImportTurni.Rows(Indice).FindControl("Chk_Selezionato"), CheckBox)
            If Tabella.Rows(Indice).Item(11) <> "" Then
                If Chekc.Checked = True Then
                    ClientScript.RegisterClientScriptBlock(Me.GetType(), "Errore", "VisualizzaErrore('Non puoi selezionare un orario con errori');", True)

                    Exit Sub
                End If
            End If
        Next




        For Indice = 0 To Grd_ImportTurni.Rows.Count - 1
            Dim Chekc As CheckBox = DirectCast(Grd_ImportTurni.Rows(Indice).FindControl("Chk_Selezionato"), CheckBox)

            If Chekc.Checked = True Then
                Dim CodiceOperatore As Integer = 0

                CodiceOperatore = campodbn(Tabella.Rows(Indice).Item(5))
                If Val(CodiceOperatore) = 0 Then
                    Dim Operatore As New Cls_Operatore
                    Operatore.CodiceMedico = 0
                    Operatore.CodiceFiscale = Tabella.Rows(Indice).Item(6)
                    Operatore.LeggiPerCF(Session("DC_OSPITE"))
                    If Operatore.CodiceMedico = 0 Then


                        Operatore.Nome = Tabella.Rows(Indice).Item(7)
                        Operatore.CodiceFiscale = Tabella.Rows(Indice).Item(6)

                        Operatore.CodiceMedico = CDbl(Operatore.MaxOperatore(Session("DC_OSPITE")))
                        Operatore.Scrivi(Session("DC_OSPITE"))
                    End If

                    CodiceOperatore = Operatore.CodiceMedico
                End If

                Dim Prestazione As New Cls_Appalti_Prestazioni



                Prestazione.IdAppalto = Val(Tabella.Rows(Indice).Item(0))
                Prestazione.IdStrutture = Val(Tabella.Rows(Indice).Item(1))
                Prestazione.IdMansione = Val(Tabella.Rows(Indice).Item(2))
                Prestazione.CodiceOperatore = CodiceOperatore
                Prestazione.Data = Tabella.Rows(Indice).Item(9)
                Prestazione.Ore = CDbl(Tabella.Rows(Indice).Item(10))

                Prestazione.intypecod = campodb(Tabella.Rows(Indice).Item(11))
                Prestazione.intypedescription = campodb(Tabella.Rows(Indice).Item(12))


                Prestazione.Scrivi(Session("DC_TABELLE"))



                Chekc.Enabled = False

                Tabella.Rows(Indice).Item(11) = "Importato"
            End If
        Next

        ViewState("Appoggio") = Tabella

        Grd_ImportTurni.AutoGenerateColumns = False

        Grd_ImportTurni.DataSource = Tabella

        Grd_ImportTurni.DataBind()
        Grd_ImportTurni.Visible = True



    End Sub

    Function campodb(ByVal oggetto As Object) As String
        If IsDBNull(oggetto) Then
            Return ""
        Else
            Return oggetto
        End If
    End Function

    Function campodbn(ByVal oggetto As Object) As Double
        If IsDBNull(oggetto) Then
            Return 0
        Else
            Return oggetto
        End If
    End Function



    Protected Sub ImageButton3_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles ImageButton3.Click

        Dim Appoggio As String = Format(Now, "yyyyMMddHHmmss")
        Try
            Dim NomeFile As String

            If ChkEstrazioneEpersonam.Checked = False Then
                NomeFile = ImportFile(Appoggio)
            Else
                NomeFile = ImportFileEpersonam(Appoggio)
            End If
        Finally
            Try

                Kill(HostingEnvironment.ApplicationPhysicalPath() & "Public\Turni_Import_" & Appoggio & ".txt")
            Catch ex As Exception

            End Try
        End Try
    End Sub

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Trim(Session("UTENTE")) = "" Then
            Response.Redirect("..\Login.aspx")
            Exit Sub
        End If

        If Page.IsPostBack = True Then Exit Sub


        Dim K1 As New Cls_SqlString

        Dim Barra As New Cls_BarraSenior

        If Not IsNothing(Session("RicercaAnagraficaSQLString")) Then
            Try
                K1 = Session("RicercaAnagraficaSQLString")
            Catch ex As Exception
                K1 = Nothing
            End Try
        End If

        Lbl_BarraSenior.Text = Barra.CodiceBarra(Application("SENIOR"), Session("UTENTE"), Page, K1)


    End Sub

    Protected Sub Btn_Seleziona_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles Btn_Seleziona.Click
        Dim Indice As Integer

        Tabella = ViewState("Appoggio")

        For Indice = 0 To Grd_ImportTurni.Rows.Count - 1
            Dim Chekc As CheckBox = DirectCast(Grd_ImportTurni.Rows(Indice).FindControl("Chk_Selezionato"), CheckBox)
            If Tabella.Rows(Indice).Item(11) = "" Then
                Chekc.Checked = True
            End If
        Next
    End Sub

    Protected Sub Btn_Esci_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles Btn_Esci.Click
        Response.Redirect("Menu_ImportExport.aspx")

    End Sub
End Class
