﻿<%@ Page Language="VB" AutoEventWireup="false" Inherits="Appalti_Tabella_Operatore" CodeFile="Tabella_Operatore.aspx.vb" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="xasp" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
    <meta http-equiv="x-ua-compatible" content="IE=9" />
    <title>Gestione Operatori</title>
    <asp:PlaceHolder runat="server">
        <%: System.Web.Optimization.Styles.Render("~/Content/AjaxControlToolkit/Styles/Bundle") %>
    </asp:PlaceHolder>
    <link href="ospiti.css?ver=11" rel="stylesheet" type="text/css" />
    <link rel="shortcut icon" href="images/SENIOR.ico" />
    <link href="js/jquery.autocomplete.css" rel="stylesheet" type="text/css" />


    <script src="js/jquery-1.5.1.min.js" type="text/javascript"></script>
    <script src="js/jquery.autocomplete.js" type="text/javascript"></script>
    <script src="js/soapclient.js" type="text/javascript"></script>
    <script src="/js/convertinumero.js" type="text/javascript"></script>

    <script src="js/JSErrore.js" type="text/javascript"></script>

    <script src="js/jquery.maskedinput-1.3.min.js" type="text/javascript"></script>
    <script src="/js/formatnumer.js" type="text/javascript"></script>
    <link rel="stylesheet" href="dialogbox/redips-dialog.css" type="text/css" media="screen" />
    <script type="text/javascript" src="dialogbox/redips-dialog-min.js"></script>
    <script type="text/javascript" src="dialogbox/script.js"></script>


    <script src="js/chosen/chosen.jquery.js" type="text/javascript"></script>
    <script src="js/chosen/docsupport/prism.js" type="text/javascript" charset="utf-8"></script>
    <link rel="stylesheet" href="js/chosen/docsupport/style.css">
    <link rel="stylesheet" href="js/chosen/docsupport/prism.css">
    <link rel="stylesheet" href="js/chosen/chosen.css">

    <link rel="stylesheet" href="jqueryui/jquery-ui.css" type="text/css">
    <script src="jqueryui/jquery-ui.js" type="text/javascript"></script>
    <script src="jqueryui/datapicker-it.js" type="text/javascript"></script>
    <script type="text/javascript">         
        $(document).ready(function () {
            $('html').keyup(function (event) {

                if (event.keyCode == 113) {
                    __doPostBack("Btn_Modifica", "0");
                }

            });
        });

        $(document).ready(function () {
            if (window.innerHeight > 0) { $("#BarraLaterale").css("height", (window.innerHeight - 94) + "px"); } else { $("#BarraLaterale").css("height", (document.documentElement.offsetHeight - 94) + "px"); }
        });
    </script>
</head>
<body>
    <form id="form1" runat="server">
        <asp:ScriptManager ID="ScriptManager1" runat="server" EnableScriptGlobalization="true">
            <Scripts>
                <asp:ScriptReference Path="~/Scripts/AjaxControlToolkit/Bundle" />
            </Scripts>
        </asp:ScriptManager>
        <asp:Label ID="Lbl_BarraSenior" runat="server" Text=""></asp:Label>
        <div style="text-align: left;">
            <table style="width: 100%;" cellpadding="0" cellspacing="0">
                <tr>
                    <td style="width: 160px; background-color: #F0F0F0;"></td>
                    <td>
                        <div class="Titolo">Commesse -  Anagrafica Operatori</div>
                        <div class="SottoTitoloOSPITE">
                            <asp:Label ID="Lbl_NomeOspite" runat="server" Style="text-indent: 120px;"></asp:Label><br />
                            <br />
                        </div>
                    </td>
                    <td style="text-align: right;">
                        <div class="DivTasti">
                            <asp:ImageButton runat="server" ImageUrl="~/images/salva.jpg" class="EffettoBottoniTondi" Height="38px" ToolTip="Modifica / Inserisci" ID="Btn_Modifica"></asp:ImageButton>
                            <asp:ImageButton runat="server" OnClientClick="return window.confirm('Eliminare?');" ImageUrl="~/images/elimina.jpg" class="EffettoBottoniTondi" Height="38px" ToolTip="Elimina" ID="ImageButton1"></asp:ImageButton>
                        </div>
                    </td>
                </tr>

                <tr>
                    <td style="width: 160px; background-color: #F0F0F0; vertical-align: top; text-align: center;" id="BarraLaterale">
                        <a href="Menu_Appalti.aspx" style="border-width: 0px;">
                            <img src="images/Home.jpg" alt="Menù" class="Effetto" /></a><br />
                        <asp:ImageButton ID="Btn_Esci" runat="server" BackColor="Transparent" ImageUrl="../images/Menu_Indietro.png" ToolTip="Chiudi" />
                        <br />
                        <br />
                        <br />
                        <br />
                        <br />
                        <br />
                        <br />
                        <br />
                        <br />
                    </td>
                    <td colspan="2" style="background-color: #FFFFFF;" valign="top">
                        <xasp:TabContainer ID="TabContainer1" runat="server" ActiveTabIndex="0" CssClass="TabSenior" Width="100%" BorderStyle="None" Style="margin-right: 39px">
                            <xasp:TabPanel runat="server" HeaderText="Prima Nota" ID="Tab_Anagrafica">
                                <HeaderTemplate>
                                    Anagrafica Operatori
         
                                </HeaderTemplate>
                                <ContentTemplate>


                                    <label class="LabelCampo">Codice Operatore</label>
                                    <asp:TextBox ID="Txt_CodiceMedico" runat="server" MaxLength="8"></asp:TextBox><br />
                                    <br />
                                    <label class="LabelCampo">Cognome Nome</label>
                                    <asp:TextBox ID="Txt_Nome" runat="server" MaxLength="30" Width="440px"></asp:TextBox><br />
                                    <br />


                                    <label class="LabelCampo">Codice Fiscale </label>
                                    <asp:TextBox ID="Txt_CodiceFiscale" runat="server" MaxLength="16"></asp:TextBox><br />
                                    <br />


                                    <label class="LabelCampo">Tipo Operatore: </label>
                                    <asp:DropDownList ID="DD_TipoOperatore" runat="server"></asp:DropDownList><br />
                                    <br />


                                    <label class="LabelCampo">Indirizzo :</label>
                                    <asp:TextBox ID="Txt_Indirizzo" runat="server" MaxLength="30" Width="453px"></asp:TextBox><br />
                                    <br />

                                    <label class="LabelCampo">Comune :</label>
                                    <asp:TextBox ID="Txt_ComRes" MaxLength="60" runat="server" Width="350px"></asp:TextBox><br />
                                    <br />


                                    <label class="LabelCampo">Telefono :</label>
                                    <asp:TextBox ID="Txt_Telefono" runat="server" MaxLength="16" Width="108px"></asp:TextBox><br />
                                    <br />
                                    <label class="LabelCampo">Telefono Cellulare :</label>
                                    <asp:TextBox ID="Txt_TelefonoCellulare" runat="server" MaxLength="30" Width="98px"></asp:TextBox><br />
                                    <br />
                                    <label class="LabelCampo">Specializzazione :</label>
                                    <asp:TextBox ID="Txt_Specializzazione" runat="server" MaxLength="30" Width="297px"></asp:TextBox><br />

                                    <br />
                                    <label class="LabelCampo">Note :</label><br />
                                    <asp:TextBox ID="Txt_Note" runat="server" Height="118px" TextMode="MultiLine" Width="724px" MaxLength="2000"></asp:TextBox><br />
                                    <br />


                                </ContentTemplate>

                            </xasp:TabPanel>

                            <xasp:TabPanel runat="server" HeaderText="Prima Nota" ID="TabPanel1">
                                <HeaderTemplate>
                                    Presenze Settimana
         
                                </HeaderTemplate>
                                <ContentTemplate>

                                    <asp:UpdatePanel ID="UpdatePanel1" runat="server">
                                        <ContentTemplate>

                                            <asp:GridView ID="Grd_Cella" runat="server" CellPadding="4" Height="60px"
                                                ShowFooter="True" BackColor="White" BorderColor="#6FA7D1"
                                                BorderStyle="Dotted" BorderWidth="1px">
                                                <RowStyle ForeColor="#333333" BackColor="White" />
                                                <Columns>
                                                    <asp:TemplateField>
                                                        <ItemTemplate>
                                                            <asp:ImageButton ID="IB_Delete" CommandName="Delete" runat="Server" ImageUrl="~/images/cancella.png" />
                                                        </ItemTemplate>
                                                        <FooterTemplate>
                                                            <div style="text-align: right">
                                                                <asp:ImageButton ID="ImageButton1" ImageUrl="~/images/inserisci.png" CommandName="Inserisci" runat="server" />
                                                            </div>
                                                        </FooterTemplate>
                                                    </asp:TemplateField>



                                                    <asp:TemplateField HeaderText="Regola Ripetizione">
                                                        <ItemTemplate>
                                                            <asp:DropDownList ID="DD_Regola" runat="server" class="chosen-select" AutoPostBack="true" OnSelectedIndexChanged="DD_Giorno_SelectedIndexChanged">

                                                                <asp:ListItem Value="0" Text="Settimanale"></asp:ListItem>
                                                                <asp:ListItem Value="1" Text="Ripetizione Settimanale"></asp:ListItem>
                                                                <asp:ListItem Value="2" Text="Ripetizione Intervalo"></asp:ListItem>
                                                                <asp:ListItem Value="3" Text="Mensile"></asp:ListItem>
                                                                <asp:ListItem Value="4" Text="Data"></asp:ListItem>
                                                            </asp:DropDownList>
                                                            <br />
                                                            <asp:TextBox ID="Txt_Data" runat="server" placeholder="Data Inizio" Visible="false" Width="100px"></asp:TextBox>

                                                            <asp:TextBox ID="Txt_Numero" runat="server" placeholder="Numero Ripetizioni" Visible="false" Style="text-align: right;" Width="100px"></asp:TextBox>


                                                            <asp:TextBox ID="Txt_Intervallo" runat="server" placeholder="Intervallo" Visible="false" Style="text-align: right;" Width="100px"></asp:TextBox>
                                                            <table>
                                                                <tr>
                                                                    <td>
                                                                        <asp:CheckBox ID="Chk_Lunedi" runat="server" Text="Lunedi" /></td>
                                                                    <td>
                                                                        <asp:CheckBox ID="Chk_Martedi" runat="server" Text="Martedì" /></td>
                                                                </tr>
                                                                <tr>
                                                                    <td>
                                                                        <asp:CheckBox ID="Chk_Mercoledi" runat="server" Text="Mercoledì" /></td>
                                                                    <td>
                                                                        <asp:CheckBox ID="Chk_Giovedi" runat="server" Text="Giovedì" /></td>
                                                                    <tr>
                                                                <tr>
                                                                    <td>
                                                                        <asp:CheckBox ID="Chk_Venerdì" runat="server" Text="Venerdì" /></td>
                                                                    <td>
                                                                        <asp:CheckBox ID="Chk_Sabato" runat="server" Text="Sabato" /></td>
                                                                </tr>
                                                                <tr>
                                                                    <td>
                                                                        <asp:CheckBox ID="Chk_Domenica" runat="server" Text="Domenica" /></td>
                                                                    <td></td>
                                                                </tr>
                                                            </table>

                                                        </ItemTemplate>
                                                        <HeaderStyle Font-Bold="False" Font-Italic="False" />
                                                        <ItemStyle Width="350px" />
                                                    </asp:TemplateField>

                                                    <asp:TemplateField HeaderText="Mansione">
                                                        <ItemTemplate>
                                                            <asp:DropDownList ID="DD_Mansione" runat="server" class="chosen-select" Width="300px"></asp:DropDownList>
                                                        </ItemTemplate>
                                                        <HeaderStyle Font-Bold="False" Font-Italic="False" />
                                                        <ItemStyle Width="320px" />
                                                    </asp:TemplateField>

                                                    <asp:TemplateField HeaderText="Commesse">
                                                        <ItemTemplate>
                                                            <asp:DropDownList ID="DD_Appalto" runat="server" class="chosen-select" Width="300px" AutoPostBack="true" OnSelectedIndexChanged="DD_Appalto_SelectedIndexChanged"></asp:DropDownList>
                                                        </ItemTemplate>
                                                        <HeaderStyle Font-Bold="False" Font-Italic="False" />
                                                        <ItemStyle Width="320px" />
                                                    </asp:TemplateField>

                                                    <asp:TemplateField HeaderText="Struttura">
                                                        <ItemTemplate>
                                                            <asp:DropDownList ID="DD_Struttura" runat="server" class="chosen-select" Width="300px"></asp:DropDownList>
                                                        </ItemTemplate>
                                                        <HeaderStyle Font-Bold="False" Font-Italic="False" />
                                                        <ItemStyle Width="320px" />
                                                    </asp:TemplateField>

                                                    <asp:TemplateField HeaderText="Utente">
                                                        <ItemTemplate>
                                                            <asp:DropDownList ID="DD_Utente" runat="server" class="chosen-select" Width="300px"></asp:DropDownList>
                                                        </ItemTemplate>
                                                        <HeaderStyle Font-Bold="False" Font-Italic="False" />
                                                        <ItemStyle Width="320px" />
                                                    </asp:TemplateField>


                                                    <asp:TemplateField HeaderText="Ore">
                                                        <ItemTemplate>
                                                            <asp:TextBox ID="Txt_Ore" runat="server" Style="text-align: right;" Width="100px"></asp:TextBox>
                                                        </ItemTemplate>
                                                        <HeaderStyle Font-Bold="False" Font-Italic="False" />
                                                        <ItemStyle Width="100px" />
                                                    </asp:TemplateField>





                                                </Columns>

                                                <FooterStyle BackColor="White" ForeColor="#023102" />

                                                <HeaderStyle BackColor="#A6C9E2" Font-Bold="False" ForeColor="White" BorderColor="#6FA7D1" BorderWidth="1" />

                                                <PagerStyle BackColor="#336666" ForeColor="White" HorizontalAlign="Center" />

                                                <SelectedRowStyle BackColor="#339966" Font-Bold="True" ForeColor="White" />
                                            </asp:GridView>
                                        </ContentTemplate>
                                    </asp:UpdatePanel>



                                </ContentTemplate>

                            </xasp:TabPanel>
                        </xasp:TabContainer>
                    </td>
                </tr>
            </table>
        </div>
    </form>
</body>
</html>
