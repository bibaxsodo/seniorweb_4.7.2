﻿<%@ Page Language="VB" AutoEventWireup="false" Inherits="Appalti_StruttureMansioni" CodeFile="StruttureMansioni.aspx.vb" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="xasp" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
    <meta http-equiv="x-ua-compatible" content="IE=9" />
    <title>Strutture Mansioni</title>    
    <link href="ospiti.css?ver=11" rel="stylesheet" type="text/css" /> <link rel="shortcut icon" href="images/SENIOR.ico"/>
    <link href="js/jquery.autocomplete.css" rel="stylesheet" type="text/css" />
       

    <script src="js/jquery-1.5.1.min.js" type="text/javascript"></script>
    <script src="js/jquery.autocomplete.js" type="text/javascript"></script>
    <script src="js/soapclient.js" type="text/javascript"></script>
    <script src="js/convertinumero.js" type="text/javascript"></script>
    
    <script src="js/JSErrore.js" type="text/javascript"></script>
    
    <script src="js/jquery.maskedinput-1.3.min.js" type="text/javascript"></script>
    <script src="js/formatnumer.js" type="text/javascript"></script>
    <link rel="stylesheet" href="dialogbox/redips-dialog.css" type="text/css" media="screen" />
	<script type="text/javascript" src="dialogbox/redips-dialog-min.js"></script>
    <script type="text/javascript" src="dialogbox/script.js"></script>    
        
     <script src="js/chosen/chosen.jquery.js" type="text/javascript"></script>
  <script src="js/chosen/docsupport/prism.js" type="text/javascript" charset="utf-8"></script>
    <link rel="stylesheet" href="js/chosen/docsupport/style.css">
    <link rel="stylesheet" href="js/chosen/docsupport/prism.css">
    <link rel="stylesheet" href="js/chosen/chosen.css">
        
        <link rel="stylesheet" href="jqueryui/jquery-ui.css"  type="text/css">
    <script src="jqueryui/jquery-ui.js" type="text/javascript"></script>
    <script src="jqueryui/datapicker-it.js" type="text/javascript"></script> 
    <script type="text/javascript">         
        $(document).ready(function () {
            $('html').keyup(function (event) {

                if (event.keyCode == 113) {
                    __doPostBack("Btn_Modifica", "0");
                }

            });
        });

        $(document).ready(function () {
            if (window.innerHeight > 0) { $("#BarraLaterale").css("height", (window.innerHeight - 94) + "px"); } else { $("#BarraLaterale").css("height", (document.documentElement.offsetHeight - 94) + "px"); }
        });
    </script>     
</head>
<body>
    <form id="form1" runat="server">
    <asp:Label ID="Lbl_BarraSenior" runat="server" Text=""></asp:Label>    
    <div style="text-align:left;">    
    <asp:ScriptManager ID="ScriptManager1" runat="server" EnableScriptGlobalization="true"></asp:ScriptManager>
    <table style="width:100%;" cellpadding="0" cellspacing="0">
    <tr>
    <td style="width:160px; background-color:#F0F0F0;"></td>
    <td>
    <div class="Titolo">Appalti - Strutture Mansioni</div>
    <div class="SottoTitolo">
        <br />
        <asp:Label ID="lbl_StrutturaMansione" runat="server" Text=""></asp:Label>
        <br />
        <br />
    </div>
    </td>
    <td style="text-align:right;">
         <div class="DivTasti"> 
             <asp:ImageButton runat="server" ImageUrl="~/images/salva.jpg"  class="EffettoBottoniTondi"  Height="38px" ToolTip="Modifica / Inserisci"  ID="Btn_Modifica"></asp:ImageButton>
             <asp:ImageButton runat="server" OnClientClick="return window.confirm('Eliminare?');" ImageUrl="~/images/elimina.jpg"  class="EffettoBottoniTondi"  Height="38px" ToolTip="Elimina"  ID="ImageButton1"></asp:ImageButton>                    
         </div>
	 </td>
    </tr>
   
    <tr>
    <td style="width:160px; background-color:#F0F0F0; vertical-align:top; text-align:center;" id="BarraLaterale">       
    <a href="Menu_Appalti.aspx" style="border-width:0px;"><img src="images/Home.jpg" alt="Menù" class="Effetto" /></a><br />    
     <asp:ImageButton ID="Btn_Esci" runat="server" BackColor="Transparent" ImageUrl="../images/Menu_Indietro.png" ToolTip="Chiudi"  />
     <br />                
     <br />
     <br />
     <br />
     <br />
     <br />
     <br />
     <br />
     <br />
    </td>    
    <td colspan="2" style="background-color: #FFFFFF;" valign="top" > 
     <xasp:TabContainer ID="TabContainer1" runat="server" ActiveTabIndex="0" CssClass="TabSenior"  
            Width="100%" BorderStyle="None" style="margin-right: 39px">               
     <xasp:TabPanel runat="server" HeaderText="Prima Nota" ID="Tab_Anagrafica"><HeaderTemplate>
            Strutture Mansioni
         
         </HeaderTemplate>
     <ContentTemplate> 

  <asp:GridView ID="Grd_Retta" runat="server" CellPadding="4" Height="60px"  
ShowFooter="True" BackColor="White"  BorderColor="#6FA7D1"
               BorderStyle="Dotted" BorderWidth="1px"  >
<RowStyle ForeColor="#333333" BackColor="White" />
<Columns>
    <asp:TemplateField>
    <ItemTemplate>
    <asp:ImageButton ID="IB_Delete" CommandName="Delete" Runat="server" 
                 ImageUrl="~/images/cancella.png" />
    </ItemTemplate>
    <FooterTemplate>
    <div style="text-align:right"   >
    <asp:ImageButton ID="ImageButton1" ImageUrl="~/images/inserisci.png" CommandName="Inserisci"  runat="server" />
    </div>
    </FooterTemplate>
    </asp:TemplateField>   
    
    <asp:TemplateField HeaderText="Mansione">
    <ItemTemplate>        
    <asp:DropDownList id="DD_Mansione" runat="server" class="chosen-select" Width="540px" ></asp:DropDownList>
    </ItemTemplate>
    <HeaderStyle Font-Bold="False" Font-Italic="False" />
    <ItemStyle Width="540px" />
    </asp:TemplateField>

        
    <asp:TemplateField HeaderText="Regola">
    <ItemTemplate >        
    <asp:DropDownList id="DD_Regola" runat="server"  class="chosen-select"    >
    <asp:ListItem Value="F" Text="Importo Fisso"></asp:ListItem>
    <asp:ListItem Value="O" Text="Importo Orario/Unita"></asp:ListItem>    
    <asp:ListItem Value="G" Text="Importo Giornaliero"></asp:ListItem>
    <asp:ListItem Value="A" Text="Importo Accesso"></asp:ListItem>
    <asp:ListItem Value="a" Text="Importo Accesso Parziali"></asp:ListItem>
    <asp:ListItem Value="P" Text="Accesso Fatturazione Passiva"></asp:ListItem>
    <asp:ListItem Value="p" Text="Accesso Fatt. Passiva Parziali"></asp:ListItem>
    <asp:ListItem Value="o" Text="Importo Orario Fatturazione Passiva"></asp:ListItem>
    </asp:DropDownList>
    </ItemTemplate>
    <HeaderStyle Font-Bold="False" Font-Italic="False" />
    <ItemStyle Width="310px" />
    </asp:TemplateField>
    
    
        <asp:TemplateField HeaderText="Importo">
    <ItemTemplate>        
    <asp:TextBox ID="TxtImporto" runat="server" Text="" style="text-align:right;" Width="100px" ></asp:TextBox>

    </ItemTemplate>
    <HeaderStyle Font-Bold="False" Font-Italic="False" />
    <ItemStyle Width="110px" />
    </asp:TemplateField>

    
     <asp:TemplateField HeaderText="Conto">
    <ItemTemplate>        
    <asp:TextBox ID="TxtConto" runat="server" Text="" Width="300px" ></asp:TextBox>

    </ItemTemplate>
    <HeaderStyle Font-Bold="False" Font-Italic="False" />
    <ItemStyle Width="310px" />
    </asp:TemplateField>
    
    
    
     <asp:TemplateField HeaderText="IVA">
    <ItemTemplate>        
    <asp:DropDownList ID="DDIVA" runat="server" class="chosen-select"  ></asp:DropDownList>
    </ItemTemplate>
    <HeaderStyle Font-Bold="False" Font-Italic="False" />
    <ItemStyle Width="310px" />
    </asp:TemplateField>
    
    
    
     <asp:TemplateField HeaderText="TIPO UTENTE">
    <ItemTemplate>        
    <asp:DropDownList ID="DDTIPOUTENTE" runat="server" class="chosen-select"  ></asp:DropDownList>
    </ItemTemplate>
    <HeaderStyle Font-Bold="False" Font-Italic="False" />
    <ItemStyle Width="310px" />
    </asp:TemplateField>
    </Columns>
    <FooterStyle BackColor="White" ForeColor="#023102" />
    <HeaderStyle BackColor="#A6C9E2" Font-Bold="False"  ForeColor="White" BorderColor="#6FA7D1"  BorderWidth="1px"  />
    <PagerStyle BackColor="#336666" ForeColor="White" HorizontalAlign="Center" />
    <SelectedRowStyle BackColor="#339966" Font-Bold="True" ForeColor="White" />
    </asp:GridView>
    <br /><br />
    </ContentTemplate>
    </xasp:TabPanel>
    </xasp:TabContainer>         
     </td>
     </tr>     
     </table>   
    </div>
    </form>
</body>
</html>
