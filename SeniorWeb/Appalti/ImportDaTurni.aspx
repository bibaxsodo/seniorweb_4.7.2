﻿<%@ Page Language="VB" AutoEventWireup="false" Inherits="Appalti_ImportDaTurni" CodeFile="ImportDaTurni.aspx.vb" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="sau" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
    <meta http-equiv="x-ua-compatible" content="IE=9" />
    <title>Import Da Turni</title>
    <asp:PlaceHolder runat="server">
        <%: System.Web.Optimization.Styles.Render("~/Content/AjaxControlToolkit/Styles/Bundle") %>
    </asp:PlaceHolder>
    <link href="ospiti.css?ver=11" rel="stylesheet" type="text/css" />
    <link rel="shortcut icon" href="images/SENIOR.ico" />
    <link href="js/jquery.autocomplete.css" rel="stylesheet" type="text/css" />


    <script src="js/jquery-1.5.1.min.js" type="text/javascript"></script>
    <script src="js/jquery.autocomplete.js" type="text/javascript"></script>
    <script src="js/soapclient.js" type="text/javascript"></script>
    <script src="/js/convertinumero.js" type="text/javascript"></script>



    <script src="js/jquery.maskedinput-1.3.min.js" type="text/javascript"></script>
    <script src="/js/formatnumer.js" type="text/javascript"></script>
    <script src="js/JSErrore.js" type="text/javascript"></script>

    <link rel="stylesheet" href="dialogbox/redips-dialog.css" type="text/css" media="screen" />
    <script type="text/javascript" src="dialogbox/redips-dialog-min.js"></script>
    <script type="text/javascript" src="dialogbox/script.js"></script>

    <link rel="stylesheet" href="jqueryui/jquery-ui.css" type="text/css">
    <script src="jqueryui/jquery-ui.js" type="text/javascript"></script>
    <script src="jqueryui/datapicker-it.js" type="text/javascript"></script>
    <script type="text/javascript">
        $(document).ready(function () {
            if (window.innerHeight > 0) { $("#BarraLaterale").css("height", (window.innerHeight - 105) + "px"); } else { $("#BarraLaterale").css("height", (document.documentElement.offsetHeight - 105) + "px"); }
        });
    </script>
</head>
<body>
    <form id="form1" runat="server">
        <asp:ScriptManager ID="ScriptManager1" runat="server" EnableScriptGlobalization="true">
            <Scripts>
                <asp:ScriptReference Path="~/Scripts/AjaxControlToolkit/Bundle" />
            </Scripts>
        </asp:ScriptManager>
        <asp:Label ID="Lbl_BarraSenior" runat="server" Text=""></asp:Label>
        <div style="text-align: left;">

            <table style="width: 100%;" cellpadding="0" cellspacing="0">
                <tr>
                    <td style="width: 160px; background-color: #F0F0F0;"></td>
                    <td>
                        <div class="Titolo">Commesse - Import Da Turni</div>
                        <div class="SottoTitolo">
                            <asp:Label ID="Lbl_NomeOspite" runat="server"></asp:Label><br />
                            <br />
                        </div>
                    </td>
                    <td style="text-align: right;">
                        <span class="BenvenutoText">Benvenuto
                            <asp:Label ID="Lbl_Utente" runat="server"></asp:Label></span>
                        <div class="DivTasti">
                            <asp:ImageButton ID="Btn_Modifica" runat="server" Height="38px" ImageUrl="~/images/salva.jpg" CssClass="EffettoBottoniTondi" Width="38px" ToolTip="Modifica" />
                            <asp:ImageButton ID="ImageButton3" runat="server" Height="38px" ImageUrl="~/images/esegui.png" CssClass="EffettoBottoniTondi" Width="38px" />
                        </div>
                    </td>
                </tr>

                <tr>
                    <td style="width: 160px; background-color: #F0F0F0; vertical-align: top; text-align: center;" id="BarraLaterale">
                        <a href="Menu_ImportExport.aspx" style="border-width: 0px;">
                            <img src="images/Home.jpg" class="Effetto" alt="Menù" /></a><br />
                        <asp:ImageButton ID="Btn_Esci" runat="server" BackColor="Transparent" ImageUrl="../images/Menu_Indietro.png" CssClass="Effetto" ToolTip="Chiudi" />

                        <br />

                        <div id="MENUDIV"></div>
                        <br />
                        <br />
                        <br />
                        <br />
                        <br />
                        <br />
                        <br />
                        <br />
                        <br />
                        <br />
                        <br />
                        <br />
                    </td>
                    <td colspan="2" style="background-color: #FFFFFF; vertical-align: top;">
                        <sau:TabContainer ID="TabContainer1" runat="server" ActiveTabIndex="0" CssClass="TabSenior"
                            Width="100%" BorderStyle="None" Style="margin-right: 39px">
                            <sau:TabPanel runat="server" HeaderText="Prima Nota" ID="Tab_Anagrafica">
                                <HeaderTemplate>
                                    Import Da Turni              
                                </HeaderTemplate>
                                <ContentTemplate>
                                    <label style="display: block; float: left; width: 160px;">File  :</label>
                                    <asp:FileUpload ID="FileUpload1" runat="server" />
                                    <br />
                                    <br />
                                    <label style="display: block; float: left; width: 160px;">E-Personam  :</label>
                                    <asp:CheckBox ID="ChkEstrazioneEpersonam" runat="server" Text="" />

                                    <br />
                                    <br />

                                    <label style="display: block; float: left; width: 160px;">Import CUS  :</label>
                                    <asp:CheckBox ID="Chk_Cus" runat="server" Text="" />

                                    <br />
                                    <br />

                                    <asp:Button ID="Btn_Seleziona" runat="server" Text="Seleziona Tutto" />
                                    <br />
                                    <br />


                                    <asp:GridView ID="Grd_ImportTurni" runat="server" CellPadding="3"
                                        Width="100%" BackColor="White" BorderColor="#999999" BorderStyle="None"
                                        BorderWidth="1px" GridLines="Vertical" AllowPaging="False"
                                        ShowFooter="True" PageSize="20000">
                                        <RowStyle ForeColor="#565151" BackColor="#EEEEEE" />
                                        <AlternatingRowStyle BackColor="White" />
                                        <Columns>
                                            <asp:TemplateField ItemStyle-Width="10px" HeaderText="Selezionato">
                                                <ItemTemplate>
                                                    <asp:CheckBox ID="Chk_Selezionato" runat="server" Text="" />
                                                </ItemTemplate>
                                            </asp:TemplateField>

                                            <asp:BoundField HeaderText="Appalto" ItemStyle-Width="15%" DataField="Appalto" />
                                            <asp:BoundField HeaderText="Centro Di Costo" ItemStyle-Width="10%" DataField="CentroDiCosto" />
                                            <asp:BoundField HeaderText="Codice Operatore" ItemStyle-Width="10%" DataField="CodiceOperatore" />
                                            <asp:BoundField HeaderText="Codice Fiscale" ItemStyle-Width="10%" DataField="CodiceFiscale" />
                                            <asp:BoundField HeaderText="Cognome Nome" ItemStyle-Width="15%" DataField="CognomeNome" />
                                            <asp:BoundField HeaderText="Mansione" ItemStyle-Width="10%" DataField="Mansione" />
                                            <asp:BoundField HeaderText="Data" ItemStyle-Width="5%" DataField="Data" />
                                            <asp:BoundField HeaderText="Ore" ItemStyle-Width="5%" DataField="Ore" />
                                            <asp:BoundField HeaderText="Segnalazioni" ItemStyle-Width="20%" DataField="Segnalazioni" />

                                        </Columns>
                                        <FooterStyle BackColor="#CCCCCC" ForeColor="Black" />
                                        <PagerStyle BackColor="#999999" ForeColor="Black" HorizontalAlign="Center" />
                                        <SelectedRowStyle BackColor="#008A8C" Font-Bold="True" ForeColor="White" />
                                        <HeaderStyle BackColor="#565151" Font-Bold="false" Font-Size="Small" ForeColor="White" />
                                        <AlternatingRowStyle BackColor="#DCDCDC" />
                                    </asp:GridView>

                                    <br />
                                    <br />


                                </ContentTemplate>
                            </sau:TabPanel>
                        </sau:TabContainer>
                    </td>
                </tr>
            </table>


        </div>
    </form>
</body>
</html>
