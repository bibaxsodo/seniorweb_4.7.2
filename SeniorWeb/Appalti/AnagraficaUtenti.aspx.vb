﻿Imports Microsoft.VisualBasic
Imports System.Data.OleDb
Imports System.Web.Hosting
Imports System.Data.SqlTypes

Partial Class Appalti_AnagraficaUtenti
    Inherits System.Web.UI.Page


    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Trim(Session("UTENTE")) = "" Then
            Response.Redirect("../Login.aspx")
            Exit Sub
        End If

        Call EseguiJS()

        If Page.IsPostBack = True Then
            Exit Sub
        End If

        Dim K1 As New Cls_SqlString

        Dim Barra As New Cls_BarraSenior

        If Not IsNothing(Session("RicercaAnagraficaSQLString")) Then
            Try
                K1 = Session("RicercaAnagraficaSQLString")
            Catch ex As Exception
                K1 = Nothing
            End Try
        End If

        Lbl_BarraSenior.Text = Barra.CodiceBarra(Application("SENIOR"), Session("UTENTE"), Page, K1)


        If Val(Request.Item("id")) = 0 Then
            Dim CodOpe As New ClsOspite

            Exit Sub
        End If


        Dim Med As New ClsOspite

        Med.CodiceOspite = Request.Item("id")
        Med.Leggi(Session("DC_OSPITE"), Med.CodiceOspite)
        Txt_CodiceUtente.Text = Med.CodiceOspite
        Txt_Nome.Text = Med.NomeOspite
        Txt_CogNome.Text = Med.CognomeOspite

        Txt_CodiceFiscale.Text = Med.CodiceFiscale
        Txt_Indirizzo.Text = Med.RESIDENZAINDIRIZZO1

        Dim DeCom As New ClsComune
        DeCom.Comune = Med.RESIDENZACOMUNE1
        DeCom.Provincia = Med.RESIDENZAPROVINCIA1
        DeCom.DecodficaComune(Session("DC_OSPITE"))
        Txt_ComRes.Text = DeCom.Comune & " " & DeCom.Provincia & " " & DeCom.Descrizione

        Txt_Telefono.Text = Med.Telefono1
        Txt_TelefonoCellulare.Text = Med.Telefono2
        Txt_DataNascita.Text = Format(Med.DataNascita, "dd/MM/yyyy")

        Dim TipoUtente As New Cls_Appalti_TipoUtente

        TipoUtente.UpDateDropBox(Session("DC_TABELLE"), DD_TipoUtente)


        DD_TipoUtente.SelectedValue = Med.TipoOspite

        Dim Log As New Cls_LogPrivacy
        Log.LogPrivacy(Application("SENIOR"), Session("CLIENTE"), Session("UTENTE"), Session("UTENTEIMPER"), Page.GetType.Name.ToUpper, 0, 0, "", "", 0, Txt_Nome.Text, "V", "OPERATORE", "")





        Call EseguiJS()
    End Sub



    Protected Sub Btn_Modifica_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles Btn_Modifica.Click
        Dim Ospite As New ClsOspite


        If Txt_Nome.Text.Trim = "" Then
            ClientScript.RegisterClientScriptBlock(Me.GetType(), "Errore", "VisualizzaErrore('Specificare cognome nome');", True)
            Exit Sub
        End If


        If Request.Item("id") = "" Then
            Dim Log As New Cls_LogPrivacy
            Log.LogPrivacy(Application("SENIOR"), Session("CLIENTE"), Session("UTENTE"), Session("UTENTEIMPER"), Page.GetType.Name.ToUpper, 0, 0, "", "", 0, Txt_Nome.Text, "I", "OPERATORE", "")
        Else

            Dim Log As New Cls_LogPrivacy
            Dim ConvT As New Cls_DataTableToJson
            Dim OldTable As New System.Data.DataTable("tabellaOld")


            Dim OldDatiPar As New Cls_Operatore

            OldDatiPar.CodiceMedico = Txt_CodiceUtente.Text
            OldDatiPar.Leggi(Session("DC_OSPITE"))
            Dim AppoggioJS As String


            AppoggioJS = ConvT.SerializeObject(OldDatiPar)

            Log.LogPrivacy(Application("SENIOR"), Session("CLIENTE"), Session("UTENTE"), Session("UTENTEIMPER"), Page.GetType.Name.ToUpper, Session("CODICEOSPITE"), 0, "", "", 0, Txt_Nome.Text, "M", "OPERATORE", AppoggioJS)

        End If


        Ospite.CodiceOspite = Val(Txt_CodiceUtente.Text)
        Ospite.Nome = Txt_CogNome.Text & "  " & Txt_Nome.Text
        Ospite.CognomeOspite = Txt_CogNome.Text
        Ospite.NomeOspite = Txt_Nome.Text
        Ospite.CODICEFISCALE = Txt_CodiceFiscale.Text
        If Val(Txt_CodiceUtente.Text) = 0 Then
            Ospite.InserisciOspite(Session("DC_OSPITE"), Txt_CogNome.Text, Txt_Nome.Text, Nothing)
        End If

        Ospite.RESIDENZAINDIRIZZO1 = Txt_Indirizzo.Text
        Ospite.Nome = Txt_CogNome.Text & "  " & Txt_Nome.Text
        Ospite.CognomeOspite = Txt_CogNome.Text
        Ospite.NomeOspite = Txt_Nome.Text
        Ospite.CODICEFISCALE = Txt_CodiceFiscale.Text
        If Txt_DataNascita.Text.Trim = "" Then
            Ospite.DataNascita = Nothing
        Else
            If IsDate(Txt_DataNascita.Text) Then
                Ospite.DataNascita = Txt_DataNascita.Text
            Else
                Ospite.DataNascita = Nothing
            End If
        End If

        Dim Vettore(100) As String

        If Txt_ComRes.Text <> "" Then
            Vettore = SplitWords(Txt_ComRes.Text)
            If Vettore.Length > 1 Then
                Ospite.RESIDENZAPROVINCIA1 = Vettore(0)
                Ospite.RESIDENZACOMUNE1 = Vettore(1)
            End If
        End If


        Ospite.TELEFONO1 = Txt_Telefono.Text
        Ospite.TELEFONO2 = Txt_TelefonoCellulare.Text
        Ospite.TIPOLOGIA = "O"

        Ospite.TipoOspite = DD_TipoUtente.SelectedValue


        Ospite.ScriviOspite(Session("DC_OSPITE"))

        Call PaginaPrecedente()
    End Sub



    Private Function SplitWords(ByVal s As String) As String()
        '
        ' Call Regex.Split function from the imported namespace.
        ' Return the result array.
        '
        Return Regex.Split(s, "\W+")
    End Function

    Protected Sub ImageButton1_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles ImageButton1.Click
        If Txt_CodiceUtente.Text.Trim = "" Then
            ClientScript.RegisterClientScriptBlock(Me.GetType(), "Errore", "VisualizzaErrore('Specificare codice');", True)
            Exit Sub
        End If

        Dim MyMed As New Cls_Operatore
        MyMed.CodiceMedico = Txt_CodiceUtente.Text

        If MyMed.VerificaElimina(Session("DC_OSPITE")) = False Then
            ClientScript.RegisterClientScriptBlock(Me.GetType(), "Errore", "VisualizzaErrore('Non posso utilizzare codice usate');", True)
            Exit Sub
        End If


        Dim Log As New Cls_LogPrivacy
        Dim ConvT As New Cls_DataTableToJson
        Dim OldTable As New System.Data.DataTable("tabellaOld")


        Dim OldDatiPar As New Cls_Operatore

        OldDatiPar.CodiceMedico = Txt_CodiceUtente.Text
        OldDatiPar.Leggi(Session("DC_OSPITE"))
        Dim AppoggioJS As String


        AppoggioJS = ConvT.SerializeObject(OldDatiPar)

        Log.LogPrivacy(Application("SENIOR"), Session("CLIENTE"), Session("UTENTE"), Session("UTENTEIMPER"), Page.GetType.Name.ToUpper, Session("CODICEOSPITE"), 0, "", "", 0, Txt_Nome.Text, "D", "OPERATORE", AppoggioJS)

        MyMed.Elimina(Session("DC_OSPITE"))

        Call PaginaPrecedente()
    End Sub

    Protected Sub Btn_Esci_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles Btn_Esci.Click

        Call PaginaPrecedente()
    End Sub
    Private Sub PaginaPrecedente()
        Response.Redirect("Elenco_Anagraficautenti.aspx")
    End Sub



    Private Sub EseguiJS()
        Dim MyJs As String
        MyJs = "$(document).ready(function() {"

        MyJs = MyJs & "var els = document.getElementsByTagName(""*"");"

        MyJs = MyJs & "for (var i=0;i<els.length;i++)"
        MyJs = MyJs & "if ( els[i].id ) { "
        MyJs = MyJs & " var appoggio =els[i].id; "
        MyJs = MyJs & " if (appoggio.match('Txt_Data')!= null) {  "
        MyJs = MyJs & " $(els[i]).mask(""99/99/9999"");"
        MyJs = MyJs & " $(els[i]).datepicker({ changeMonth: true,changeYear: true,  yearRange: ""2000:" & Year(Now) + 12 & """},$.datepicker.regional[""it""]);"
        MyJs = MyJs & "    }"
        MyJs = MyJs & " if ((appoggio.match('TxtImporto')!= null) || (appoggio.match('Txt_Percentuale')!= null) ) {  "
        MyJs = MyJs & " $(els[i]).keypress(function() { ForceNumericInput($(this).val(), true, true); } ); "
        MyJs = MyJs & " $(els[i]).blur(function() { var ap = formatNumber($(this).val(),2); $(this).val(ap); });"
        MyJs = MyJs & "    }"

        MyJs = MyJs & " if (appoggio.match('Txt_Conto')!= null) {   "
        MyJs = MyJs & " $(els[i]).autocomplete('/WebHandler/GestioneConto.ashx?Utente=" & Session("UTENTE") & "', {delay:5,minChars:3});"
        MyJs = MyJs & "    }"

        MyJs = MyJs & " if (appoggio.match('Txt_ComRes')!= null)  {  "
        MyJs = MyJs & " $(els[i]).autocomplete('/WebHandler/AutocompleteComune.ashx?Utente=" & Session("UTENTE") & "', {delay:5,minChars:3});"
        MyJs = MyJs & "    }"

        MyJs = MyJs & "} "
        MyJs = MyJs & "});"

        'MyJs = "$(document).ready(function() { $('.myClass').keypress(function() { handleEnter($('.myClass').val(), true, true); } );     $('#" & Txt_ImportoPagato.ClientID & "').blur(function() { var ap = formatNumber ($('#" & Txt_ImportoPagato.ClientID & "').val(),2); $('#" & Txt_ImportoPagato.ClientID & "').val(ap); }); });"
        ScriptManager.RegisterStartupScript(Me, Me.GetType(), "visualizzaRitIm", MyJs, True)
    End Sub


    
End Class
