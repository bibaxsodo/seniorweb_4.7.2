﻿Imports Microsoft.VisualBasic
Imports System.Data.OleDb
Imports System.Web.Hosting
Imports System.Data.SqlTypes
Partial Class Appalti_Elenco_TabellaTrascodifica
    Inherits System.Web.UI.Page
    Dim Tabella As New System.Data.DataTable("tabella")


    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Session("UTENTE") = "" Then
            Response.Redirect("..\Login.aspx")
            Exit Sub
        End If

        If Page.IsPostBack = True Then
            Exit Sub
        End If
        Dim K1 As New Cls_SqlString

        Dim Barra As New Cls_BarraSenior

        If Not IsNothing(Session("RicercaGeneraleSQLString")) Then
            K1 = Session("RicercaGeneraleSQLString")
        End If

        Lbl_BarraSenior.Text = Barra.CodiceBarra(Application("SENIOR"), Session("UTENTE"), Page, K1)



        Dim cn As New OleDbConnection


        Dim ConnectionString As String = Session("DC_TABELLE")

        cn = New Data.OleDb.OleDbConnection(ConnectionString)

        cn.Open()

        Dim cmd As New OleDbCommand

        cmd.CommandText = ("select top 50  * from TabellaTrascodificheEsportazioni  " & _
                               "  Order By TIPOTAB,ID")
        cmd.Connection = cn



        Tabella.Clear()
        Tabella.Columns.Add("ID", GetType(String))
        Tabella.Columns.Add("TIPOTAB", GetType(String))
        Tabella.Columns.Add("TIPOTAB DECODIFICA", GetType(String))
        Tabella.Columns.Add("SENIOR", GetType(String))
        Tabella.Columns.Add("DECODIFICA", GetType(String))
        Tabella.Columns.Add("EXPORT", GetType(String))

        Dim myPOSTreader As OleDbDataReader = cmd.ExecuteReader()
        Do While myPOSTreader.Read
            Dim myriga As System.Data.DataRow = Tabella.NewRow()

            myriga(0) = campodbn(myPOSTreader.Item("ID"))
            myriga(1) = campodb(myPOSTreader.Item("TIPOTAB"))

            If campodb(myPOSTreader.Item("TIPOTAB")) = "RI" Then
                myriga(2) = "REGISTRO IVA"

                Try
                    Dim TipoReg As New Cls_RegistroIVA

                    TipoReg.Tipo = Val(campodb(myPOSTreader.Item("SENIOR")))
                    TipoReg.Leggi(Session("DC_TABELLE"), TipoReg.Tipo)

                    myriga(4) = TipoReg.Descrizione
                Catch ex As Exception

                End Try
            End If
            If campodb(myPOSTreader.Item("TIPOTAB")) = "CC" Then
                myriga(2) = "CONTO CONTABILE"

                Dim Vettore(100) As String

                Vettore = SplitWords(campodb(myPOSTreader.Item("SENIOR")))

                Try
                    Dim Decodifica As New Cls_Pianodeiconti


                    Decodifica.Mastro = Vettore(0)
                    Decodifica.Conto = Vettore(1)
                    Decodifica.Sottoconto = Vettore(2)
                    Decodifica.Decodfica(Session("DC_GENERALE"))

                    myriga(4) = Decodifica.Descrizione
                Catch ex As Exception

                End Try
            End If
            If campodb(myPOSTreader.Item("TIPOTAB")) = "MP" Then
                myriga(2) = "MODALITA PAGAMENTO"

                Try
                    Dim ModPag As New Cls_TipoPagamento

                    ModPag.Codice = campodb(myPOSTreader.Item("SENIOR"))
                    ModPag.Leggi(Session("DC_TABELLE"))

                    myriga(4) = ModPag.Descrizione
                Catch ex As Exception

                End Try
            End If
            If campodb(myPOSTreader.Item("TIPOTAB")) = "IV" Then
                myriga(2) = "CODICE IVA"

                Try
                    Dim ModPag As New Cls_IVA

                    ModPag.Codice = campodb(myPOSTreader.Item("SENIOR"))
                    ModPag.Leggi(Session("DC_TABELLE"), ModPag.Codice)

                    myriga(4) = ModPag.Descrizione
                Catch ex As Exception

                End Try
            End If
            If campodb(myPOSTreader.Item("TIPOTAB")) = "CS" Then
                myriga(2) = "CAUSALE-SPESA"


                Try
                    Dim ModPag As New Cls_CausaleContabile

                    ModPag.Codice = campodb(myPOSTreader.Item("SENIOR"))
                    ModPag.Leggi(Session("DC_TABELLE"), ModPag.Codice)

                    myriga(4) = ModPag.Descrizione
                Catch ex As Exception

                End Try
            End If
            If campodb(myPOSTreader.Item("TIPOTAB")) = "SC" Then
                myriga(2) = "MODALITA-SCADENZA"
                Try
                    Dim ModPag As New Cls_TipoPagamento

                    ModPag.Codice = campodb(myPOSTreader.Item("SENIOR"))
                    ModPag.Leggi(Session("DC_TABELLE"))

                    myriga(4) = ModPag.Descrizione
                Catch ex As Exception

                End Try
            End If
            If campodb(myPOSTreader.Item("TIPOTAB")) = "CI" Then
                myriga(2) = "CONTO CONTABILE-CAUSALE INCASSO"
                Dim Vettore(100) As String

                Vettore = SplitWords(campodb(myPOSTreader.Item("SENIOR")))

                Try
                    Dim Decodifica As New Cls_Pianodeiconti


                    Decodifica.Mastro = Vettore(0)
                    Decodifica.Conto = Vettore(1)
                    Decodifica.Sottoconto = Vettore(2)
                    Decodifica.Decodfica(Session("DC_GENERALE"))

                    myriga(4) = Decodifica.Descrizione
                Catch ex As Exception

                End Try

            End If
            If campodb(myPOSTreader.Item("TIPOTAB")) = "CT" Then
                myriga(2) = "CONTO CONTABILE-TIPO CONTO"
                Dim Vettore(100) As String

                Vettore = SplitWords(campodb(myPOSTreader.Item("SENIOR")))

                Try
                    Dim Decodifica As New Cls_Pianodeiconti


                    Decodifica.Mastro = Vettore(0)
                    Decodifica.Conto = Vettore(1)
                    Decodifica.Sottoconto = Vettore(2)
                    Decodifica.Decodfica(Session("DC_GENERALE"))

                    myriga(4) = Decodifica.Descrizione
                Catch ex As Exception

                End Try

            End If
            myriga(3) = campodb(myPOSTreader.Item("SENIOR"))


            myriga(5) = campodb(myPOSTreader.Item("EXPORT"))


            Tabella.Rows.Add(myriga)
        Loop

        myPOSTreader.Close()
        cn.Close()


        ViewState("Appoggio") = Tabella

        Grid.AutoGenerateColumns = True

        Grid.DataSource = Tabella

        Grid.DataBind()

        'Btn_Nuovo.Visible = True
    End Sub

    Function campodb(ByVal oggetto As Object) As String
        If IsDBNull(oggetto) Then
            Return ""
        Else
            Return oggetto
        End If
    End Function
    Function campodbn(ByVal oggetto As Object) As Double
        If IsDBNull(oggetto) Then
            Return 0
        Else
            Return oggetto
        End If
    End Function

    Protected Sub Grid_PageIndexChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewPageEventArgs) Handles Grid.PageIndexChanging
        Tabella = ViewState("Appoggio")
        Grid.PageIndex = e.NewPageIndex
        Grid.DataSource = Tabella
        Grid.DataBind()
    End Sub

    Protected Sub Grid_RowCommand(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewCommandEventArgs) Handles Grid.RowCommand
        If e.CommandName = "Seleziona" Then


            Dim d As Integer


            Tabella = ViewState("Appoggio")

            d = Val(e.CommandArgument)



            Dim Codice As String


            Codice = Tabella.Rows(d).Item(0).ToString


            Response.Redirect("TabellaTrascodifica.aspx?Id=" & Codice)
        End If
    End Sub

    Protected Sub ImgRicerca_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles ImgRicerca.Click
        Response.Redirect("TabellaTrascodifica.aspx")
    End Sub




    Private Function SplitWords(ByVal s As String) As String()
        '
        ' Call Regex.Split function from the imported namespace.
        ' Return the result array.
        '
        Return Regex.Split(s, "\W+")
    End Function

    Protected Sub Btn_Esci_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles Btn_Esci.Click
        Response.Redirect("Menu_Tabelle.aspx")
    End Sub

    Protected Sub ImgHome_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles ImgHome.Click
        Response.Redirect("Menu_Appalti.aspx")
    End Sub
End Class
