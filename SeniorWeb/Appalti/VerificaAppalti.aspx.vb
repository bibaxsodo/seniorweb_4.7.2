﻿Imports System.Web.Hosting
Imports System.Data.OleDb
Partial Class Appalti_VerificaAppalti
    Inherits System.Web.UI.Page

    Dim MyTable As New System.Data.DataTable("tabella")
    Dim MyDataSet As New System.Data.DataSet()

    Function campodb(ByVal oggetto As Object) As String
        If IsDBNull(oggetto) Then
            Return ""
        Else
            Return oggetto
        End If
    End Function


    Private Sub CaricaTabella()
        Dim ConnectionString As String = Session("DC_TABELLE")
        Dim cn As OleDbConnection
        Dim MySql As String
        Dim Condizione As String

        cn = New Data.OleDb.OleDbConnection(ConnectionString)

        cn.Open()

        MyTable.Clear()
        MyTable.Columns.Clear()

        MyTable.Columns.Add("IdAppalto", GetType(String))
        MyTable.Columns.Add("Descrizione", GetType(String))
        MyTable.Columns.Add("Committente", GetType(String))
        MyTable.Columns.Add("Data Inizio", GetType(String))
        MyTable.Columns.Add("Data Fine", GetType(String))

        MyTable.Columns.Add("Tipo   ", GetType(String))

        MyTable.Columns.Add("IdStruttura", GetType(String))
        MyTable.Columns.Add("Struttura", GetType(String))
        
        Dim cmd As New OleDbCommand()

        MySQl = "SELECT [Appalti_Anagrafica].[ID] as IdAppalto,[Descrizione],[Gruppo],[Cig],[Committente],[DataInizio],[DataFine] ,[ImportoTeoricoMensile]" & _
                ",[ImportoTeoricoGiornaliero],[ImportoTeoricoMensileInaddGiornaliero],[Appalti_Anagrafica].[ImportoForfait]" & _
                ",[CausaleContabile]      ,[CodiceIVA],[Tipo],[RotturaStruttura],[RaggruppaRicavi],[Mastro],[Conto],[Sottoconto],[Appalti_AppaltiStrutture].[ID],[Appalti_AppaltiStrutture].[IdAppalto]" & _
                ",[Appalti_AppaltiStrutture].[IdStrutture],[Appalti_AppaltiStrutture].[ImportoForfait],[Appalti_AppaltiStrutture].[KM] ,[Appalti_AppaltiStrutture].[KMImporto] " & _
                " FROM [Appalti_Anagrafica] left join [Appalti_AppaltiStrutture] on [Appalti_Anagrafica].id = [Appalti_AppaltiStrutture].IdAppalto "

        cmd.CommandText = MySql
        
        cmd.Connection = cn
        Dim myPOSTreader As OleDbDataReader = cmd.ExecuteReader()
        Do While myPOSTreader.Read

            Dim myriga As System.Data.DataRow = MyTable.NewRow()

            myriga(0) = campodb(myPOSTreader.Item("IdAppalto"))
            myriga(1) = campodb(myPOSTreader.Item("Descrizione"))
            Dim MCom As New ClsUSL

            MCom.CodiceRegione = campodb(myPOSTreader.Item("Committente"))
            MCom.Leggi(Session("DC_OSPITE"))


            myriga(2) = MCom.Nome

            myriga(3) = campodb(myPOSTreader.Item("DataInizio"))
            myriga(4) = campodb(myPOSTreader.Item("DataFine"))

            myriga(5) = ""
            If campodb(myPOSTreader.Item("TIPO")) = "F" Then
                myriga(5) = "Forfait"
            End If

            If campodb(myPOSTreader.Item("TIPO")) = "F" Then
                myriga(5) = "Variabile"
            End If


            myriga(6) = campodb(myPOSTreader.Item("IdStrutture"))

            Dim Strutture As New Cls_AppaltiTabellaStruttura

            Strutture.ID = Val(campodb(myPOSTreader.Item("IdStrutture")))

            Strutture.Leggi(Session("DC_TABELLE"), Strutture.ID)


            myriga(7) = Strutture.Descrizione


            MyTable.Rows.Add(myriga)

        Loop
        cn.Close()

        Dim myriga1 As System.Data.DataRow = MyTable.NewRow()
        MyTable.Rows.Add(myriga1)

        GridView1.AutoGenerateColumns = True
        GridView1.DataSource = MyTable
        GridView1.Font.Size = 10
        GridView1.DataBind()


        Dim i As Integer

        For i = 0 To GridView1.Rows.Count - 1

            Dim jk As Integer
            jk = Val(GridView1.Rows(i).Cells(1).Text)

            Dim Csk As String
            Csk = GridView1.Rows(i).Cells(0).Text

            If jk > 0 Then
                GridView1.Rows(i).Cells(1).Text = "<a href=""#"" onclick=""DialogBox('Anagrafica.aspx?CodiceOspite=" & jk & "&CentroServizio=" & Csk & "');"" >" & GridView1.Rows(i).Cells(1).Text & "</a>"
            End If
        Next
    End Sub

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Call EseguiJS()
        If Trim(Session("UTENTE")) = "" Then
            Response.Redirect("../Login.aspx")
            Exit Sub
        End If

        If Page.IsPostBack = True Then Exit Sub


        Dim K1 As New Cls_SqlString

        Dim Barra As New Cls_BarraSenior

        If Not IsNothing(Session("RicercaAnagraficaSQLString")) Then
            Try
                K1 = Session("RicercaAnagraficaSQLString")
            Catch ex As Exception
                K1 = Nothing
            End Try
        End If

        Lbl_BarraSenior.Text = Barra.CodiceBarra(Application("SENIOR"), Session("UTENTE"), Page, K1)

    End Sub


    Private Sub EseguiJS()
        Dim MyJs As String
        MyJs = "$(document).ready(function() {"

        MyJs = MyJs & "var els = document.getElementsByTagName(""*"");"

        MyJs = MyJs & "for (var i=0;i<els.length;i++)"
        MyJs = MyJs & "if ( els[i].id ) { "
        MyJs = MyJs & " var appoggio =els[i].id; "

        MyJs = MyJs & " if ((appoggio.match('Txt_Ospite')!= null) || (appoggio.match('Txt_Ospite')!= null))  {  "
        MyJs = MyJs & " $(els[i]).autocomplete('AutocompleteOspitiSenzaCserv.ashx?Utente=" & Session("UTENTE") & "', {delay:5,minChars:3});"
        MyJs = MyJs & "    }"

        MyJs = MyJs & " if (appoggio.match('Txt_Data')!= null) {  "
        MyJs = MyJs & " $(els[i]).mask(""99/99/9999"");"

        MyJs = MyJs & " $(els[i]).datepicker({ changeMonth: true,changeYear: true,  yearRange: ""1990:" & Year(Now) + 5 & """},$.datepicker.regional[""it""]);"

        MyJs = MyJs & "    }"
        MyJs = MyJs & "} "
        MyJs = MyJs & "if (window.innerHeight>0) { $(""#BarraLaterale"").css(""height"",(window.innerHeight - 94) + ""px""); } else"
        MyJs = MyJs & "{ $(""#BarraLaterale"").css(""height"",(document.documentElement.offsetHeight - 94) + ""px"");  }"
        MyJs = MyJs & "});"

        'MyJs = "$(document).ready(function() { $('.myClass').keypress(function() { handleEnter($('.myClass').val(), true, true); } );     $('#" & Txt_ImportoPagato.ClientID & "').blur(function() { var ap = formatNumber ($('#" & Txt_ImportoPagato.ClientID & "').val(),2); $('#" & Txt_ImportoPagato.ClientID & "').val(ap); }); });"
        ScriptManager.RegisterStartupScript(Me, Me.GetType(), "visualizzaRitIm", MyJs, True)
    End Sub

    Protected Sub ImageButton3_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles ImageButton3.Click

        Call CaricaTabella()
        Call EseguiJS()
    End Sub


    Protected Sub ImageButton4_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles ImageButton4.Click

        Call CaricaTabella()
        If MyTable.Rows.Count > 1 Then
            Response.Clear()
            Response.AddHeader("content-disposition", "attachment;filename=Movimenti.xls")
            Response.Charset = String.Empty
            Response.Cache.SetCacheability(HttpCacheability.NoCache)
            Response.ContentType = "application/vnd.xls"
            Dim stringWrite As New System.IO.StringWriter
            Dim htmlWrite As New HtmlTextWriter(stringWrite)

            form1.Controls.Clear()
            form1.Controls.Add(GridView1)

            form1.RenderControl(htmlWrite)

            Response.Write(stringWrite.ToString())
            Response.End()
            UpdatePanel1.Update()
        End If
    End Sub

    Protected Sub Btn_Esci_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles Btn_Esci.Click
        Response.Redirect("Menu_Appalti.aspx")

    End Sub

    Protected Sub Btn_Stampa_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles Btn_Stampa.Click

        Call CaricaTabella()


        Session("GrigliaSoloStampa") = MyTable

        Session("ParametriSoloStampa") = ""

        ScriptManager.RegisterStartupScript(Me, Me.GetType(), "Stampa", "openPopUp('GrigliaSoloStampa.aspx','Stampe','width=800,height=600');", True)

        Call EseguiJS()
    End Sub
End Class
