﻿Imports System
Imports System.Web
Imports Microsoft.VisualBasic
Imports System.Data.OleDb
Imports System.Web.Hosting
Imports System.Data.SqlTypes
Imports System.Net
Imports System.Collections.Generic
Imports System.IO
Imports System.Security.Cryptography.X509Certificates
Imports System.Net.Security
Imports System.Web.Script.Serialization
Imports org.jivesoftware.util
Imports Newtonsoft.Json
Imports Newtonsoft.Json.Linq

Partial Class Appalti_Strutture
    Inherits System.Web.UI.Page
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Page.IsPostBack = True Then
            Exit Sub
        End If




        Dim K1 As New Cls_SqlString

        Dim Barra As New Cls_BarraSenior

        If Not IsNothing(Session("RicercaAnagraficaSQLString")) Then
            Try
                K1 = Session("RicercaAnagraficaSQLString")
            Catch ex As Exception
                K1 = Nothing
            End Try
        End If

        Lbl_BarraSenior.Text = Barra.CodiceBarra(Application("SENIOR"), Session("UTENTE"), Page, K1)


        Dim CentroServizio As New Cls_CentroServizio



        CentroServizio.UpDateDropBox(Session("DC_OSPITE"), DD_Cserv)



        If Request.Item("CODICE") = "" Then
            Call EseguiJS()
            Try
                DecodificaCodiceEpersonam()

            Catch ex As Exception

            End Try


            Exit Sub
        End If


        
        Dim Struttura As New Cls_AppaltiTabellaStruttura


        Struttura.ID = Request.Item("CODICE")
        Struttura.Leggi(Session("DC_TABELLE"), Struttura.ID)


        Txt_Codice.Text = Struttura.ID

        Txt_Codice.Enabled = False
        Txt_Descrizione.Text = Struttura.Descrizione
        Txt_CentroDiCosto.Text = Struttura.CodificaImport
        DD_Cserv.SelectedValue = Struttura.CentroServizio
        Txt_CodiceImport2.Text = Struttura.CodificaImport2

        Try
            DecodificaCodiceEpersonam()

        Catch ex As Exception

        End Try
        


        Call EseguiJS()

    End Sub



    Private Function SplitWords(ByVal s As String) As String()
        '
        ' Call Regex.Split function from the imported namespace.
        ' Return the result array.
        '
        Return Regex.Split(s, "\W+")
    End Function

    Private Sub DecodificaCodiceEpersonam()
        Dim Token As String

        Token = LoginPersonam(Context)
        Dim Url As String
        Dim rawresp As String

        Url = "https://api.e-personam.com/api/v1.0/business_units"

        Dim client As HttpWebRequest = WebRequest.Create(Url)

        client.Method = "GET"
        client.Headers.Add("Authorization", "Bearer " & Token)
        client.ContentType = "Content-Type: application/json"

        client.KeepAlive = True
        client.ReadWriteTimeout = 62000
        client.MaximumResponseHeadersLength = 262144
        client.Timeout = 62000


        Dim reader As StreamReader
        Dim response As HttpWebResponse = Nothing



        response = DirectCast(client.GetResponse(), HttpWebResponse)

        reader = New StreamReader(response.GetResponseStream())

        rawresp = reader.ReadToEnd()


        
        Dim jResults As JArray = JArray.Parse(rawresp)

        Dim IndiceRighe As Integer = 0
        Dim VtTipo(1000) As String
        Dim VtTipoCodice(1000) As String
        Dim VtPadre(1000) As Integer



        For Each jTok1 As JToken In jResults

            If jTok1.Item("structure").ToString.ToUpper = "true".ToUpper Then
                Dim Cerca As Integer
                Dim trovato As Boolean = False

                For Cerca = 0 To IndiceRighe - 1
                    If VtTipoCodice(Cerca) = jTok1.Item("id") Then                        
                        trovato = True
                    End If
                Next

                If Not trovato Then
                    IndiceRighe = IndiceRighe + 1
                    VtTipo(IndiceRighe) = jTok1.Item("name").ToString
                    VtTipoCodice(IndiceRighe) = jTok1.Item("id")
                    VtPadre(IndiceRighe) = 0
                End If
            End If


            If Not IsNothing(jTok1.Item("substructures")) Then
                For Each jTok2 As JToken In jTok1.Item("substructures")

                    Dim Cerca As Integer
                    Dim trovato As Boolean = False

                    For Cerca = 0 To IndiceRighe - 1
                        If VtTipoCodice(Cerca) = jTok2.Item("id") Then
                            VtPadre(Cerca) = jTok1.Item("id")
                            trovato = True
                        End If
                    Next
                    If Not trovato Then
                        IndiceRighe = IndiceRighe + 1
                        VtTipo(IndiceRighe) = jTok2.Item("name").ToString
                        VtTipoCodice(IndiceRighe) = jTok2.Item("id")
                        VtPadre(IndiceRighe) = jTok1.Item("id")
                    End If

                    If Not IsNothing(jTok2.Item("units")) Then
                        For Each jTok3 As JToken In jTok2.Item("units")
                            '5214
                            If Val(jTok3.Item("id").ToString) = 5214 Then
                                trovato = False

                            End If

                            trovato = False
                            For Cerca = 0 To IndiceRighe - 1
                                If VtTipoCodice(Cerca) = jTok3.Item("id") Then
                                    VtPadre(Cerca) = jTok3.Item("id")
                                    trovato = True
                                End If
                            Next
                            If Not trovato Then
                                IndiceRighe = IndiceRighe + 1
                                VtTipo(IndiceRighe) = jTok3.Item("name").ToString
                                VtTipoCodice(IndiceRighe) = jTok3.Item("id")
                                VtPadre(IndiceRighe) = jTok2.Item("id")
                            End If

                        Next
                    End If
                Next
            Else
                If Not IsNothing(jTok1.Item("units")) Then
                    For Each jTok2 As JToken In jTok1.Item("units")

                        Dim Cerca As Integer
                        Dim trovato As Boolean = False

                        For Cerca = 0 To IndiceRighe - 1
                            If VtTipoCodice(Cerca) = jTok2.Item("id") Then
                                VtPadre(Cerca) = jTok1.Item("id")
                                trovato = True
                            End If
                        Next
                        If Not trovato Then
                            IndiceRighe = IndiceRighe + 1
                            VtTipo(IndiceRighe) = jTok2.Item("name").ToString
                            VtTipoCodice(IndiceRighe) = jTok2.Item("id")
                            VtPadre(IndiceRighe) = jTok1.Item("id")
                        End If
                    Next
                End If
            End If
        Next

        Dim Ordina As Integer
        Dim OrdinaX As Integer

        For Ordina = 1 To IndiceRighe
            If VtPadre(Ordina) = 0 Then
                DD_CentriCosto.Items.Add(VtTipo(Ordina))
                DD_CentriCosto.Items(DD_CentriCosto.Items.Count - 1).Value = VtTipoCodice(Ordina)
                Dim Indicex As Integer

                For Indicex = 0 To IndiceRighe
                    If VtPadre(Indicex) = VtTipoCodice(Ordina) Then
                        DD_CentriCosto.Items.Add("---" & VtTipo(Indicex))
                        DD_CentriCosto.Items(DD_CentriCosto.Items.Count - 1).Value = VtTipoCodice(Indicex)
                        Dim Indicey As Integer

                        For Indicey = 0 To IndiceRighe
                            If VtPadre(Indicey) = VtTipoCodice(Indicex) Then
                                DD_CentriCosto.Items.Add("------" & VtTipo(Indicey))
                                DD_CentriCosto.Items(DD_CentriCosto.Items.Count - 1).Value = VtTipoCodice(Indicey)

                                Dim Indicez As Integer

                                For Indicez = 0 To IndiceRighe
                                    If VtPadre(Indicez) = VtTipoCodice(Indicey) Then
                                        DD_CentriCosto.Items.Add("---------" & VtTipo(Indicez))
                                        DD_CentriCosto.Items(DD_CentriCosto.Items.Count - 1).Value = VtTipoCodice(Indicez)

                                    End If
                                Next

                            End If
                        Next
                    End If
                Next

            End If

        Next


        DD_CentriCosto.SelectedValue = Txt_CentroDiCosto.Text
    End Sub



    Private Function LoginPersonam(ByVal context As HttpContext) As String
        Try


            Dim request As New NameValueCollection


            request.Add("client_id", "98217ca6670c660e22f42d58e231d4e56c84fb34e216b0f66f1f30284fd3ec9d")
            request.Add("client_secret", "5570a684f69ca74d75d16bba669c156ec092eccb4111d0974adec3edb2fa4d6f")


            request.Add("username", context.Session("EPersonamUser"))



            request.Add("password", context.Session("EPersonamPSW"))
            System.Net.ServicePointManager.ServerCertificateValidationCallback = AddressOf CertificateHandler


            Dim client As New WebClient()

            Dim result As Object = client.UploadValues("https://api.e-personam.com/api/v1.0/token/password", "POST", request)


            Dim stringa As String = Encoding.Default.GetString(result)


            Dim serializer As JavaScriptSerializer


            serializer = New JavaScriptSerializer()




            Dim s As Autentificazione = serializer.Deserialize(Of Autentificazione)(stringa)


            Return s.access_token
        Catch ex As Exception
            Return ""
        End Try
    End Function

    Public Class Autentificazione
        Public access_token As String
        Public expires_in As String
        Public scope As String
        Public token_type As String
        Public refresh_token As String
    End Class



    Private Shared Function CertificateHandler(ByVal sender As Object, ByVal certificate As X509Certificate, ByVal chain As X509Chain, ByVal SSLerror As SslPolicyErrors) As Boolean
        Return True
    End Function



    Protected Sub Imb_Modifica_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles Imb_Modifica.Click
        If Txt_Descrizione.Text.Trim = "" Then
            ClientScript.RegisterClientScriptBlock(Me.GetType(), "Errore", "VisualizzaErrore('Descrizione obbligatoria');", True)
            Exit Sub
        End If

        'If Txt_Codice.Text.Trim = "" Then
        '    ClientScript.RegisterClientScriptBlock(Me.GetType(), "Errore", "VisualizzaErrore('Codice obbligatorio');", True)
        '    Exit Sub
        'End If

        Dim Struttura As New Cls_AppaltiTabellaStruttura
        Struttura.ID = Val(Txt_Codice.Text)


        Struttura.Descrizione = Txt_Descrizione.Text
        Struttura.CodificaImport = Txt_CentroDiCosto.Text
        Struttura.CodificaImport2 = Txt_CodiceImport2.Text
        Struttura.CentroServizio = DD_Cserv.SelectedValue
        Struttura.Scrivi(Session("DC_TABELLE"))

        If Val(Txt_Codice.Text) = 0 And DD_Cserv.SelectedValue = "" Then
            Dim Mc As New Cls_CentroServizio


            Mc.CENTROSERVIZIO = Struttura.ID
            Mc.Leggi(Session("DC_OSPITE"), Struttura.ID)

            Mc.CENTROSERVIZIO = Struttura.ID
            Mc.DESCRIZIONE = Txt_Descrizione.Text
            Mc.MASTRO = 14
            Mc.CONTO = 1
            Mc.DescrizioneRigaInFattura = "Conto Retta - descrizione"
            Mc.ScriviCentroServizio(Session("DC_OSPITE"))

            Struttura.CentroServizio = Struttura.ID
            Struttura.Scrivi(Session("DC_TABELLE"))



        End If


        Call PaginaPrecedente()
    End Sub

    Protected Sub ImageButton2_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles ImageButton2.Click
        If Txt_Descrizione.Text.Trim = "" Then
            ClientScript.RegisterClientScriptBlock(Me.GetType(), "Errore", "VisualizzaErrore('Descrizione obbligatoria');", True)
            Exit Sub
        End If

        If Txt_Codice.Text.Trim = "" Then
            ClientScript.RegisterClientScriptBlock(Me.GetType(), "Errore", "VisualizzaErrore('Codice obbligatorio');", True)
            Exit Sub
        End If
        Dim TpAd As New Cls_AppaltiTabellaStruttura

        TpAd.ID = Txt_Codice.Text
        TpAd.Elimina(Session("DC_TABELLE"))

        Call PaginaPrecedente()
    End Sub



    Private Sub EseguiJS()
        Dim MyJs As String
        MyJs = "$(document).ready(function() {"

        MyJs = MyJs & "var els = document.getElementsByTagName(""*"");"

        MyJs = MyJs & "for (var i=0;i<els.length;i++)"
        MyJs = MyJs & "if ( els[i].id ) { "
        MyJs = MyJs & " var appoggio =els[i].id; "


        MyJs = MyJs & " if (appoggio.match('Txt_Conto')!= null) {   "
        MyJs = MyJs & " $(els[i]).autocomplete('/WebHandler/GestioneConto.ashx?Utente=" & Session("UTENTE") & "', {delay:5,minChars:3});"
        MyJs = MyJs & "    }"


        MyJs = MyJs & " if ((appoggio.match('Txt_MasimaleDomiciliari')!= null) ) {  " & vbNewLine
        MyJs = MyJs & " $(els[i]).keypress(function() { ForceNumericInput($(this).val(), true, true); } ); " & vbNewLine
        MyJs = MyJs & " $(els[i]).blur(function() { var ap = formatNumber($(this).val(),2); $(this).val(ap); });" & vbNewLine
        MyJs = MyJs & "    }" & vbNewLine


        'Txt_MasimaleDomiciliari

        MyJs = MyJs & "} "
        MyJs = MyJs & "});"

        'MyJs = "$(document).ready(function() { $('.myClass').keypress(function() { handleEnter($('.myClass').val(), true, true); } );     $('#" & Txt_ImportoPagato.ClientID & "').blur(function() { var ap = formatNumber ($('#" & Txt_ImportoPagato.ClientID & "').val(),2); $('#" & Txt_ImportoPagato.ClientID & "').val(ap); }); });"
        ScriptManager.RegisterStartupScript(Me, Me.GetType(), "visualizzaRitIm", MyJs, True)
    End Sub

    Protected Sub Btn_Esci_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles Btn_Esci.Click
        Call PaginaPrecedente()
    End Sub

    Private Sub PaginaPrecedente()
        Response.Redirect("Elenco_Struttura.aspx")
    End Sub

    Protected Sub ImageButton1_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles ImageButton1.Click
        Txt_Codice.Text = ""
        Txt_Codice.Enabled = True

        Call EseguiJS()
    End Sub

    Protected Sub Txt_Codice_TextChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles Txt_Codice.TextChanged
        If Txt_Codice.Enabled = True Then
            Img_VerificaCodice.ImageUrl = "~/images/corretto.gif"

            Dim x As New Cls_AppaltiTabellaStruttura

            x.ID = Txt_Codice.Text.Trim
            x.Descrizione = ""
            x.Leggi(Session("DC_TABELLE"), x.ID)

            If x.Descrizione <> "" Then
                Img_VerificaCodice.ImageUrl = "~/images/errore.gif"
            End If
        End If
    End Sub

    Protected Sub Txt_Descrizione_TextChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles Txt_Descrizione.TextChanged
        If Txt_Codice.Enabled = True Then
            If Txt_Descrizione.Text <> "" Then
                Img_VerificaDescrizione.ImageUrl = "~/images/corretto.gif"

                Dim x As New Cls_AppaltiTabellaStruttura

                x.ID = 0
                x.Descrizione = Txt_Descrizione.Text.Trim
                x.LeggiDescrizione(Session("DC_TABELLE"), x.ID)

                If x.ID <> 0 Then
                    Img_VerificaDescrizione.ImageUrl = "~/images/errore.gif"
                End If
            End If
        End If
    End Sub

    Protected Sub DD_CentriCosto_TextChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles DD_CentriCosto.TextChanged
        Txt_CentroDiCosto.Text = DD_CentriCosto.SelectedValue
    End Sub
End Class
