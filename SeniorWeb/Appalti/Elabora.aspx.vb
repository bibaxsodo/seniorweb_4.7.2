﻿Imports System.Web.Hosting
Imports System.Diagnostics
Imports System.ComponentModel
Imports System.Threading
Imports System.Data.OleDb

Partial Class Appalti_Elabora
    Inherits System.Web.UI.Page
    Private myRequest As System.Net.WebRequest
    Public StDataCond As String

    Private DataP As Date
    Private DataA As Date
    Private AttivaEnte As Date
    Private DataEnte As Date
    Private ScadenzaEnte As Date
    Private ConnessioneGenerale As String
    Private ConnessioneOspiti As String
    Private ConnessioneTabelle As String
    Private CampoTimer As String
    Private CampoProgressBar As String
    Private CampoEstrazione As String
    Private CampoProva As String
    Private CampoErrori As String
    Private CampoWr As String
    Dim Vt_List2(200) As String
    Public DiProva As Boolean

    Dim TabRegistri As New System.Data.DataTable("TabRegistri")




    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Trim(Session("UTENTE")) = "" Then
            Response.Redirect("../Login.aspx")
            Exit Sub
        End If

        ConnessioneOspiti = Session("DC_OSPITE")
        ConnessioneTabelle = Session("DC_TABELLE")
        ConnessioneGenerale = Session("DC_GENERALE")


        If Page.IsPostBack = True Then
            Exit Sub
        End If

        
        Dim NumeroRegistrazioniIni As Integer = 0
        Dim NumeroRegistrazioniFin As Integer = 0
        Dim cn As OleDbConnection

        cn = New Data.OleDb.OleDbConnection(Session("DC_GENERALE"))
        cn.Open()

        Dim cmd As New OleDbCommand()
        cmd.CommandText = "Select count(*) as NumeroRegistrazioni From Temp_MovimentiContabiliTesta "
        cmd.Connection = cn
        Dim myPOSTreader As OleDbDataReader = cmd.ExecuteReader()
        If myPOSTreader.Read Then
            NumeroRegistrazioniIni = campodbN(myPOSTreader.Item("NumeroRegistrazioni"))
        End If
        myPOSTreader.Close()


        Threading.Thread.Sleep(1500)


        Dim cmdV As New OleDbCommand()
        cmdV.CommandText = "Select count(*) as NumeroRegistrazioni From Temp_MovimentiContabiliTesta"
        cmdV.Connection = cn
        Dim ReadV1 As OleDbDataReader = cmdV.ExecuteReader()
        If ReadV1.Read Then
            NumeroRegistrazioniFin = campodbN(ReadV1.Item("NumeroRegistrazioni"))
        End If
        ReadV1.Close()
        cn.Close()

        If NumeroRegistrazioniFin <> NumeroRegistrazioniIni Then
            Response.Redirect("Menu_Ospiti.aspx")
            Exit Sub
        End If


        Session("CampoProgressBar") = ""


        Cache("CampoProgressBar" + Session.SessionID) = ""
        System.Web.HttpRuntime.Cache("CampoProgressBar" + Session.SessionID) = ""


        Dim K1 As New Cls_SqlString

        Dim Barra As New Cls_BarraSenior

        If Not IsNothing(Session("RicercaAnagraficaSQLString")) Then            
            Try
                K1 = Session("RicercaAnagraficaSQLString")
            Catch ex As Exception
                K1 = Nothing
            End Try
        End If

        Lbl_BarraSenior.Text = Barra.CodiceBarra(Application("SENIOR"), Session("UTENTE"), Page, K1)


        Txt_Data.Text = Format(Now, "dd/MM/yyyy")


        Call CaricaTabellaRegistri()

        Application(CampoProgressBar) = ""
        Application(CampoProva) = ""
        Dim ConnectionString As String = Session("DC_OSPITE")

        Dim f As New Cls_Parametri

        f.LeggiParametri(ConnectionString)
        Dd_Mese.SelectedValue = f.MeseFatturazione
        Txt_Anno.Text = f.AnnoFatturazione

        Session("CampoWr") = ""
        Session("CampoErrori") = ""
        Call EseguiJS()


        Dim LS As New Cls_AppaltiAnagrafica


        LS.UpDateDropBox(Session("DC_TABELLE"), DD_Appalto)
        
    End Sub


    Private Sub CaricaTabellaRegistri()

        TabRegistri.Clear()
        TabRegistri.Columns.Clear()
        TabRegistri.Columns.Add("Numero", GetType(String))
        TabRegistri.Columns.Add("Registro", GetType(String))
        TabRegistri.Columns.Add("Protocollo1", GetType(String))
        TabRegistri.Columns.Add("Protocollo2", GetType(String))


        Dim cn As OleDbConnection
        Dim cnGen As OleDbConnection



        cn = New Data.OleDb.OleDbConnection(Session("DC_TABELLE"))

        cnGen = New Data.OleDb.OleDbConnection(Session("DC_GENERALE"))

        cn.Open()

        cnGen.Open()

        Dim cmd As New OleDbCommand()
        cmd.CommandText = "Select * From TipoRegistro Where TipoIVA = 'V' OR TipoIVA = 'C'"

        cmd.Connection = cn
        Dim myPOSTreader As OleDbDataReader = cmd.ExecuteReader()

        Do While myPOSTreader.Read

            Dim myriga As System.Data.DataRow = TabRegistri.NewRow()
            myriga(0) = campodbN(myPOSTreader.Item("Tipo"))
            myriga(1) = campodb(myPOSTreader.Item("Descrizione"))

            Dim cmdProg As New OleDbCommand()
            cmdProg.CommandText = "Select Max(NumeroProtocollo) From MovimentiContabiliTesta Where AnnoProtocollo = " & Year(Txt_Data.Text) & " And RegistroIVA = " & campodbN(myPOSTreader.Item("Tipo"))
            cmdProg.Connection = cnGen
            Dim RdProg As OleDbDataReader = cmdProg.ExecuteReader()
            If RdProg.Read Then
                myriga(2) = campodbN(RdProg.Item(0))
            End If
            RdProg.Close()

            Try
                'Provider=SQLOLEDB;Data Source=localhost\SQLEXPRESS;Initial Catalog=Senior_AppaltiUscita_Generale;User Id=sa;Password=advenias2012;
                If cnGen.ConnectionString.IndexOf("Senior_AppaltiUscita_Generale") >= 0 And campodbN(myPOSTreader.Item("Tipo")) = 12 Then
                    Dim NumeroX As Integer = MaxInt(Year(Txt_Data.Text), campodbN(myPOSTreader.Item("Tipo")))



                    If Val(myriga(2)) < NumeroX Then
                        myriga(2) = NumeroX

                    End If

                End If

            Catch ex As Exception

            End Try


            If IsDate(Txt_Data.Text) Then
                If Year(Txt_Data.Text) <> Year(Txt_Data.Text) Then
                    Dim cmdProgA As New OleDbCommand()
                    cmdProgA.CommandText = "Select Max(NumeroProtocollo) From MovimentiContabiliTesta Where AnnoProtocollo = " & Year(Txt_Data.Text) & " And RegistroIVA = " & campodbN(myPOSTreader.Item("Tipo"))
                    cmdProgA.Connection = cnGen
                    Dim RdProgA As OleDbDataReader = cmdProgA.ExecuteReader()
                    If RdProgA.Read Then
                        myriga(3) = campodbN(RdProgA.Item(0))
                    End If
                    RdProgA.Close()
                Else
                    myriga(3) = 0
                End If
            Else
                myriga(3) = 0
            End If


            TabRegistri.Rows.Add(myriga)
        Loop
        myPOSTreader.Close()
        cn.Close()
        cnGen.Close()

        Session("TabRegistri") = TabRegistri
        BindGen()
    End Sub
    Function MaxInt(ByVal Anno As Integer, ByVal registroiva As Integer) As Long
        Dim cn As OleDbConnection

        'Provider=SQLOLEDB;Data Source=localhost\sqlexpress;Initial Catalog=Senior_UsciteSicurezza_Generale;User Id=sa;Password=advenias2012;

        cn = New Data.OleDb.OleDbConnection("Provider=SQLOLEDB;Data Source=localhost\sqlexpress;Initial Catalog=Senior_UsciteSicurezza_Generale;User Id=sa;Password=advenias2012;")

        cn.Open()

        Dim cmd As New OleDbCommand()
        cmd.CommandText = "Select max(NumeroProtocollo) As NmProcollo From MovimentiContabiliTesta Where AnnoProtocollo = ? And RegistroIVA = " & registroiva
        cmd.Parameters.AddWithValue("@Anno", Anno)
        cmd.Connection = cn
        Dim myPOSTreader As OleDbDataReader = cmd.ExecuteReader()

        If myPOSTreader.Read Then
            MaxInt = campodbN(myPOSTreader.Item("NmProcollo"))
        End If

        cn.Close()
    End Function

    Private Sub BindGen()
        TabRegistri = Session("TabRegistri")
        Grd_Registri.AutoGenerateColumns = False
        Grd_Registri.DataSource = TabRegistri


        Grd_Registri.Columns(2).HeaderText = "Protocollo " & Year(Txt_Data.Text)
        If IsDate(Txt_Data.Text) Then
            If Year(Txt_Data.Text) <> Year(Txt_Data.Text) Then
                Grd_Registri.Columns(3).HeaderText = "Protocollo " & Year(Txt_Data.Text)
                Grd_Registri.Columns(3).Visible = True
            Else
                Grd_Registri.Columns(3).Visible = False
            End If
        Else
            Grd_Registri.Columns(3).Visible = False
        End If

        Grd_Registri.DataBind()
    End Sub


    Private Sub GeneraScadenza(ByVal NumeroRegistrazione As Long, ByVal Pag As String, ByVal Importo As Double, ByVal DataRegistrazione As Date)
        Dim MyRs As New ADODB.Recordset
        Dim MySql As String
        Dim NumScadenze As Long
        Dim GiorniPrima As Long
        Dim GiorniSeconda As Long
        Dim GiorniALtre As Long
        Dim Tipo As String
        Dim MaxScad As Long
        Dim DataScadenza As Date
        Dim GIORNIDA As Long
        Dim DecEuro As Double = 2

        Dim OspitiDB As New ADODB.Connection
        Dim GeneraleDB As New ADODB.Connection
        Dim TabelleDB As New ADODB.Connection

        TabelleDB.Open(Session("DC_TABELLE"))
        GeneraleDB.Open(Session("DC_GENERALE"))


        MySql = "Select * From TipoPagamento Where Codice = '" & Pag & "'"
        MyRs.Open(MySql, TabelleDB, ADODB.CursorTypeEnum.adOpenKeyset, ADODB.LockTypeEnum.adLockOptimistic)
        If MyRs.EOF Then MyRs.Close() : Exit Sub
        NumScadenze = MoveFromDbWC(MyRs, "Scadenze")
        GiorniPrima = MoveFromDbWC(MyRs, "GiorniPrima")
        GiorniSeconda = MoveFromDbWC(MyRs, "GiorniSeconda")
        GiorniAltre = MoveFromDbWC(MyRs, "GiorniAltre")
        Tipo = MoveFromDbWC(MyRs, "Tipo")
        MyRs.Close()
        If NumScadenze = 0 Then Exit Sub


        MySql = "Select maX(Numero) AS XNumero From Scadenzario Where NumeroRegistrazioneContabile = " & NumeroRegistrazione
        MyRs.Open(MySql, GeneraleDB, ADODB.CursorTypeEnum.adOpenKeyset, ADODB.LockTypeEnum.adLockOptimistic)
        If Not MyRs.EOF Then
            MaxScad = MoveFromDbWC(MyRs, "XNumero") + 1
        End If
        MyRs.Close()
        MySql = "Select maX(Numero) AS XNumero From Temp_Scadenzario Where NumeroRegistrazioneContabile = " & NumeroRegistrazione
        MyRs.Open(MySql, GeneraleDB, ADODB.CursorTypeEnum.adOpenKeyset, ADODB.LockTypeEnum.adLockOptimistic)
        If Not MyRs.EOF Then
            If MaxScad < Val(MoveFromDbWC(MyRs, "XNumero")) + 1 Then
                MaxScad = Val(MoveFromDbWC(MyRs, "XNumero")) + 1
            End If
        End If
        MyRs.Close()


        MySql = "Select * From Temp_Scadenzario Where NumeroRegistrazioneContabile = " & NumeroRegistrazione
        MyRs.Open(MySql, GeneraleDB, ADODB.CursorTypeEnum.adOpenKeyset, ADODB.LockTypeEnum.adLockOptimistic)
        MyRs.AddNew()
        MoveToDb(MyRs.Fields("DataAggiornamento"), Now)
        MoveToDb(MyRs.Fields("Tipo"), " ")
        MoveToDb(MyRs.Fields("Numero"), MaxScad)
        MoveToDb(MyRs.Fields("NumeroRegistrazioneContabile"), NumeroRegistrazione)
        DataScadenza = DateAdd("d", GiorniPrima, DataRegistrazione)
        If Tipo = "F" Then DataScadenza = DateSerial(Year(DataScadenza), Month(DataScadenza), GiorniMese(Month(DataScadenza), Year(DataScadenza)))
        If Month(DataScadenza) = 12 Then
            DataScadenza = DateSerial(2019, 11, 30)
        End If

        MoveToDb(MyRs.Fields("DataScadenza"), DataScadenza)
        MoveToDb(MyRs.Fields("Importo"), Modulo.MathRound(Importo / NumScadenze, DecEuro))
        MoveToDb(MyRs.Fields("Descrizione"), "")
        MoveToDb(MyRs.Fields("Chiusa"), 0)
        MoveToDb(MyRs.Fields("Utente"), "EMISSIONE")
        MoveToDb(MyRs.Fields("DataAggiornamento"), Now)
        MyRs.Update()
        MyRs.Close()

        If NumScadenze > 1 Then
            MySql = "Select * From Temp_Scadenzario Where NumeroRegistrazioneContabile = " & NumeroRegistrazione
            MyRs.Open(MySql, GeneraleDB, ADODB.CursorTypeEnum.adOpenKeyset, ADODB.LockTypeEnum.adLockOptimistic)
            MyRs.AddNew()
            MoveToDb(MyRs.Fields("DataAggiornamento"), Now)
            MoveToDb(MyRs.Fields("Tipo"), " ")
            MoveToDb(MyRs.Fields("Numero"), MaxScad)
            MoveToDb(MyRs.Fields("NumeroRegistrazioneContabile"), NumeroRegistrazione)
            DataScadenza = DateAdd("d", GiorniSeconda + GiorniPrima, DataRegistrazione)
            If Tipo = "F" Then DataScadenza = DateSerial(Year(DataScadenza), Month(DataScadenza), GiorniMese(Month(DataScadenza), Year(DataScadenza)))
            MoveToDb(MyRs.Fields("DataScadenza"), DataScadenza)
            MoveToDb(MyRs.Fields("Importo"), Modulo.MathRound(Importo / NumScadenze, DecEuro))
            MoveToDb(MyRs.Fields("Descrizione"), "")
            MoveToDb(MyRs.Fields("Chiusa"), 0)
            MoveToDb(MyRs.Fields("Utente"), "EMISSIONE")
            MoveToDb(MyRs.Fields("DataAggiornamento"), Now)
            MyRs.Update()
            MyRs.Close()
        End If

        If NumScadenze > 2 Then
            GIORNIDA = GiorniSeconda + GiorniPrima
            For I = 3 To NumScadenze
                GIORNIDA = GIORNIDA + GiorniALtre
                MySql = "Select * From Temp_Scadenzario Where NumeroRegistrazioneContabile = " & NumeroRegistrazione
                MyRs.Open(MySql, GeneraleDB, ADODB.CursorTypeEnum.adOpenKeyset, ADODB.LockTypeEnum.adLockOptimistic)
                MyRs.AddNew()
                MoveToDb(MyRs.Fields("DataAggiornamento"), Now)
                MoveToDb(MyRs.Fields("Tipo"), " ")
                MoveToDb(MyRs.Fields("Numero"), MaxScad)
                MoveToDb(MyRs.Fields("NumeroRegistrazioneContabile"), NumeroRegistrazione)
                DataScadenza = DateAdd("d", GIORNIDA, DataRegistrazione)
                If Tipo = "F" Then DataScadenza = DateSerial(Year(DataScadenza), Month(DataScadenza), GiorniMese(Month(DataScadenza), Year(DataScadenza)))
                MoveToDb(MyRs.Fields("DataScadenza"), DataScadenza)
                MoveToDb(MyRs.Fields("Importo"), Modulo.MathRound(Importo / NumScadenze, DecEuro))
                MoveToDb(MyRs.Fields("Descrizione"), "")
                MoveToDb(MyRs.Fields("Chiusa"), 0)
                MoveToDb(MyRs.Fields("Utente"), "EMISSIONE")
                MoveToDb(MyRs.Fields("DataAggiornamento"), Now)
                MyRs.Update()
                MyRs.Close()
            Next I
        End If



    End Sub


    Public Sub MoveToDb(ByRef MyField As ADODB.Field, ByVal MyVal As Object)
        Select Case Val(MyField.Type)
            Case ADODB.DataTypeEnum.adDBDate Or ADODB.DataTypeEnum.adDBTimeStamp
                If IsDate(MyVal) Then
                    MyField.Value = MyVal
                Else
                    MyField.Value = 0
                End If
            Case ADODB.DataTypeEnum.adDouble
                If IsNumeric(MyVal) Then
                    MyField.Value = MyVal
                Else
                    MyField.Value = 0
                End If
            Case ADODB.DataTypeEnum.adSmallInt
                If IsNumeric(MyVal) Then
                    MyField.Value = MyVal
                Else : MyField.Value = 0
                End If
            Case ADODB.DataTypeEnum.adInteger
                If IsNumeric(MyVal) Then
                    MyField.Value = MyVal
                Else
                    MyField.Value = 0
                End If
            Case ADODB.DataTypeEnum.adNumeric
                If IsNumeric(MyVal) Then
                    MyField.Value = MyVal
                Else
                    MyField.Value = 0
                End If
            Case ADODB.DataTypeEnum.adTinyInt
                If IsNumeric(MyVal) Then
                    MyField.Value = MyVal
                Else
                    MyField.Value = 0
                End If
            Case ADODB.DataTypeEnum.adSingle
                If IsNumeric(MyVal) Then
                    MyField.Value = MyVal
                Else
                    MyField.Value = 0
                End If
            Case ADODB.DataTypeEnum.adLongVarBinary
                If IsNumeric(MyVal) Then
                    MyField.Value = MyVal
                Else
                    MyField.Value = 0
                End If
            Case ADODB.DataTypeEnum.adUnsignedTinyInt
                If IsNumeric(MyVal) Then
                    MyField.Value = MyVal
                Else
                    MyField.Value = 0
                End If
            Case ADODB.DataTypeEnum.adVarChar Or ADODB.DataTypeEnum.adVarWChar Or ADODB.DataTypeEnum.adWChar Or ADODB.DataTypeEnum.adBSTR
                If IsDBNull(MyVal) Then
                    MyField.Value = ""
                Else
                    If Len(MyVal) > MyField.DefinedSize Then
                        MyField.Value = Mid(MyVal, 1, MyField.DefinedSize)
                    Else
                        MyField.Value = MyVal
                    End If
                End If
            Case Else
                If Val(MyField.Type) = ADODB.DataTypeEnum.adVarChar Or Val(MyField.Type) = ADODB.DataTypeEnum.adVarWChar Or _
                   Val(MyField.Type) = ADODB.DataTypeEnum.adWChar Or Val(MyField.Type) = ADODB.DataTypeEnum.adBSTR Then
                    If IsDBNull(MyVal) Then
                        MyField.Value = ""
                    Else
                        If Len(MyVal) > MyField.DefinedSize Then
                            MyField.Value = Mid(MyVal, 1, MyField.DefinedSize)
                        Else
                            MyField.Value = MyVal
                        End If
                    End If
                Else
                    MyField.Value = MyVal
                End If
        End Select

    End Sub
    Public Function MoveFromDbWC(ByVal MyRs As ADODB.Recordset, ByVal Campo As String) As Object
        Dim MyField As ADODB.Field

#If FLAG_DEBUG = False Then
        On Error GoTo ErroreCampoInesistente
#End If
        MyField = MyRs.Fields(Campo)

        Select Case MyField.Type
            Case ADODB.DataTypeEnum.adDBDate
                If IsDBNull(MyField.Value) Then
                    MoveFromDbWC = "__/__/____"
                Else
                    If Not IsDate(MyField.Value) Or MyField.Value = "0.00.00" Then
                        MoveFromDbWC = "__/__/____"
                    Else
                        MoveFromDbWC = MyField.Value
                    End If
                End If
            Case ADODB.DataTypeEnum.adDBTimeStamp
                If IsDBNull(MyField.Value) Then
                    MoveFromDbWC = "__/__/____"
                Else
                    If Not IsDate(MyField.Value) Or MyField.Value = "0.00.00" Then
                        MoveFromDbWC = "__/__/____"
                    Else
                        MoveFromDbWC = MyField.Value
                    End If
                End If
            Case ADODB.DataTypeEnum.adSmallInt
                If IsDBNull(MyField.Value) Then
                    MoveFromDbWC = 0
                Else
                    MoveFromDbWC = MyField.Value
                End If
            Case ADODB.DataTypeEnum.adInteger
                If IsDBNull(MyField.Value) Then
                    MoveFromDbWC = 0
                Else
                    MoveFromDbWC = MyField.Value
                End If
            Case ADODB.DataTypeEnum.adNumeric
                If IsDBNull(MyField.Value) Then
                    MoveFromDbWC = 0
                Else
                    MoveFromDbWC = MyField.Value
                End If
            Case ADODB.DataTypeEnum.adTinyInt
                If IsDBNull(MyField.Value) Then
                    MoveFromDbWC = 0
                Else
                    MoveFromDbWC = MyField.Value
                End If
            Case ADODB.DataTypeEnum.adSingle
                If IsDBNull(MyField.Value) Then
                    MoveFromDbWC = 0
                Else
                    MoveFromDbWC = MyField.Value
                End If
            Case ADODB.DataTypeEnum.adLongVarBinary
                If IsDBNull(MyField.Value) Then
                    MoveFromDbWC = 0
                Else
                    MoveFromDbWC = MyField.Value
                End If
            Case ADODB.DataTypeEnum.adDouble
                If IsDBNull(MyField.Value) Then
                    MoveFromDbWC = 0
                Else
                    MoveFromDbWC = MyField.Value
                End If
            Case ADODB.DataTypeEnum.adVarChar
                If IsDBNull(MyField.Value) Then
                    MoveFromDbWC = vbNullString
                Else
                    MoveFromDbWC = CStr(MyField.Value)
                End If
            Case ADODB.DataTypeEnum.adUnsignedTinyInt
                If IsDBNull(MyField.Value) Then
                    MoveFromDbWC = 0
                Else
                    MoveFromDbWC = MyField.Value
                End If
            Case Else
                If IsDBNull(MyField.Value) Then
                    MoveFromDbWC = ""
                End If
                If IsDBNull(MyField.Value) Then
                    MoveFromDbWC = ""
                Else
                    MoveFromDbWC = MyField.Value
                End If
        End Select

        On Error GoTo 0
        Exit Function
ErroreCampoInesistente:

        On Error GoTo 0
    End Function
    Private Function MoveFromDb(ByVal MyField As ADODB.Field, Optional ByVal Tipo As Integer = 0) As Object
        If Tipo = 0 Then
            Select Case MyField.Type
                Case ADODB.DataTypeEnum.adDBDate Or ADODB.DataTypeEnum.adDBTimeStamp
                    If IsDBNull(MyField.Value) Then
                        MoveFromDb = "__/__/____"
                    Else
                        If Not IsDate(MyField.Value) Then
                            MoveFromDb = "__/__/____"
                        Else
                            MoveFromDb = MyField.Value
                        End If
                    End If
                Case ADODB.DataTypeEnum.adSmallInt
                    If IsDBNull(MyField.Value) Then
                        MoveFromDb = 0
                    Else
                        MoveFromDb = MyField.Value
                    End If
                Case 20 ' INTERO PER MYSQL
                    If IsDBNull(MyField.Value) Then
                        MoveFromDb = 0
                    Else
                        MoveFromDb = MyField.Value
                    End If
                Case ADODB.DataTypeEnum.adInteger
                    If IsDBNull(MyField.Value) Then
                        MoveFromDb = 0
                    Else
                        MoveFromDb = MyField.Value
                    End If
                Case ADODB.DataTypeEnum.adNumeric
                    If IsDBNull(MyField.Value) Then
                        MoveFromDb = 0
                    Else
                        MoveFromDb = MyField.Value
                    End If
                Case ADODB.DataTypeEnum.adTinyInt
                    If IsDBNull(MyField.Value) Then
                        MoveFromDb = 0
                    Else
                        MoveFromDb = MyField.Value
                    End If
                Case ADODB.DataTypeEnum.adSingle
                    If IsDBNull(MyField.Value) Then
                        MoveFromDb = 0
                    Else
                        MoveFromDb = MyField.Value
                    End If
                Case ADODB.DataTypeEnum.adLongVarBinary
                    If IsDBNull(MyField.Value) Then
                        MoveFromDb = 0
                    Else
                        MoveFromDb = MyField.Value
                    End If
                Case ADODB.DataTypeEnum.adDouble
                    If IsDBNull(MyField.Value) Then
                        MoveFromDb = 0
                    Else
                        MoveFromDb = MyField.Value
                    End If
                Case 139
                    If IsDBNull(MyField.Value) Then
                        MoveFromDb = 0
                    Else
                        MoveFromDb = MyField.Value
                    End If
                Case ADODB.DataTypeEnum.adVarChar
                    If IsDBNull(MyField.Value) Then
                        MoveFromDb = vbNullString
                    Else
                        MoveFromDb = CStr(MyField.Value)
                    End If
                Case ADODB.DataTypeEnum.adUnsignedTinyInt
                    If IsDBNull(MyField.Value) Then
                        MoveFromDb = 0
                    Else
                        MoveFromDb = MyField.Value
                    End If
                Case Else
                    If IsDBNull(MyField.Value) Then
                        MoveFromDb = ""
                    End If
                    If IsDBNull(MyField.Value) Then
                        MoveFromDb = ""
                    Else
                        'MoveFromDb = StringaSqlS(MyField.Value)
                        MoveFromDb = RTrim(MyField.Value)
                    End If
            End Select
        End If
    End Function

    Protected Sub Grd_Registri_RowCancelingEdit(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewCancelEditEventArgs) Handles Grd_Registri.RowCancelingEdit
        Grd_Registri.EditIndex = -1
        Call BindGen()
        Call EseguiJS()
    End Sub

    Protected Sub Grd_Registri_RowDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs) Handles Grd_Registri.RowDataBound
        If e.Row.RowType = DataControlRowType.DataRow Then


            Dim LblProtocollo1 As Label = DirectCast(e.Row.FindControl("LblProtocollo1"), Label)
            If Not IsNothing(LblProtocollo1) Then

            Else
                Dim TxtProtocollo1 As TextBox = DirectCast(e.Row.FindControl("TxtProtcollo1"), TextBox)
                TxtProtocollo1.Text = TabRegistri.Rows(e.Row.RowIndex).Item(2).ToString
            End If


            Dim LblProtocollo2 As Label = DirectCast(e.Row.FindControl("LblProtocollo2"), Label)
            If Not IsNothing(LblProtocollo2) Then

            Else
                Dim TxtProtocollo2 As TextBox = DirectCast(e.Row.FindControl("TxtProtcollo2"), TextBox)
                TxtProtocollo2.Text = TabRegistri.Rows(e.Row.RowIndex).Item(3).ToString
            End If
        End If
    End Sub
    Protected Sub Grd_Registri_RowEditing(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewEditEventArgs) Handles Grd_Registri.RowEditing
        Grd_Registri.EditIndex = e.NewEditIndex
        Call BindGen()
        Call EseguiJS()
    End Sub

    Protected Sub Grd_Registri_RowUpdated(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewUpdatedEventArgs) Handles Grd_Registri.RowUpdated

    End Sub

    Protected Sub Grd_Registri_RowUpdating(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewUpdateEventArgs) Handles Grd_Registri.RowUpdating
        Dim TxtProtcollo1 As TextBox = DirectCast(Grd_Registri.Rows(e.RowIndex).FindControl("TxtProtcollo1"), TextBox)
        Dim TxtProtcollo2 As TextBox = DirectCast(Grd_Registri.Rows(e.RowIndex).FindControl("TxtProtcollo2"), TextBox)


        TabRegistri = Session("TabRegistri")
        Dim row = Grd_Registri.Rows(e.RowIndex)

        TabRegistri.Rows(row.DataItemIndex).Item(2) = TxtProtcollo1.Text
        TabRegistri.Rows(row.DataItemIndex).Item(3) = TxtProtcollo2.Text



        Session("TabRegistri") = TabRegistri

        Grd_Registri.EditIndex = -1
        Call BindGen()
        Call EseguiJS()

    End Sub

    Function campodb(ByVal oggetto As Object) As String
        If IsDBNull(oggetto) Then
            Return ""
        Else
            Return oggetto
        End If
    End Function

    Function campodbN(ByVal oggetto As Object) As Double
        If IsDBNull(oggetto) Then
            Return 0
        Else
            Return oggetto
        End If
    End Function

    Function campodbD(ByVal oggetto As Object) As Date
        If IsDBNull(oggetto) Then
            Return Nothing
        Else
            Return oggetto
        End If
    End Function




    Private Sub EseguiJS()
        Dim MyJs As String
        MyJs = "$(document).ready(function() {"

        MyJs = MyJs & "var els = document.getElementsByTagName(""*"");"

        MyJs = MyJs & "for (var i=0;i<els.length;i++)"
        MyJs = MyJs & "if ( els[i].id ) { "
        MyJs = MyJs & " var appoggio =els[i].id; "
        MyJs = MyJs & " if (appoggio.match('Txt_Data')!= null) {  "
        MyJs = MyJs & " $(els[i]).mask(""99/99/9999"");"

        MyJs = MyJs & " $(els[i]).datepicker({ changeMonth: true,changeYear: true,  yearRange: ""1990:" & Year(Now) + 5 & """},$.datepicker.regional[""it""]);"
        MyJs = MyJs & "    }"
        MyJs = MyJs & " if (appoggio.match('Txt_Scadenza')!= null) {  "
        MyJs = MyJs & " $(els[i]).mask(""99/99/9999"");"

        MyJs = MyJs & " $(els[i]).datepicker({ changeMonth: true,changeYear: true,  yearRange: ""1990:" & Year(Now) + 5 & """},$.datepicker.regional[""it""]);"
        MyJs = MyJs & "    }"



        MyJs = MyJs & "} "
        MyJs = MyJs & "});"

        'MyJs = "$(document).ready(function() { $('.myClass').keypress(function() { handleEnter($('.myClass').val(), true, true); } );     $('#" & Txt_ImportoPagato.ClientID & "').blur(function() { var ap = formatNumber ($('#" & Txt_ImportoPagato.ClientID & "').val(),2); $('#" & Txt_ImportoPagato.ClientID & "').val(ap); }); });"
        ScriptManager.RegisterStartupScript(Me, Me.GetType(), "visualizzaRitIm", MyJs, True)
    End Sub


    Protected Sub Btn_Esci_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles Btn_Esci.Click
        Response.Redirect("Menu_Appalti.aspx")
    End Sub

    Protected Sub Btn_Aggiorna_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles Btn_Aggiorna.Click

        Call CaricaTabellaRegistri()
        BindGen()
    End Sub


    Sub Scrivi(ByVal Data As Date, ByVal IdAppalto As Integer, ByVal IdStruttura As Integer, ByVal IdMansione As Integer, ByVal CodiceOperatore As Integer, ByVal Ore As Double, ByVal Numero As Integer, ByVal Importo As Double, ByVal CodiceIVA As String, ByVal intypecod As String, ByVal intypedescription As String, ByVal CodiceOspite As Integer, ByVal CodiceCooperativa As Integer, ByVal CalcoloPassivo As Integer, Optional ByVal Done As Integer = 0)
        Dim cnGenerale As New OleDbConnection


        Dim ConnectionStringGenerale As String = Session("DC_TABELLE")

        cnGenerale = New Data.OleDb.OleDbConnection(ConnectionStringGenerale)

        cnGenerale.Open()

        Dim cmdDel As New OleDbCommand
        cmdDel.CommandText = "INSERT INTO Appalti_PrestazioniElaborate (Data,[IdAppalto],[IdStruttura],[IdMansione],[CodiceOperatore],[Ore],[Imponibile],[CodiceIVA],intypecod,intypedescription,CodiceOspite,CodiceCooperativa,CalcoloPassivo,Done) VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?,?)"
        cmdDel.Connection = cnGenerale
        cmdDel.Parameters.AddWithValue("@Data", Data)
        cmdDel.Parameters.AddWithValue("@IdAppalto", IdAppalto)
        cmdDel.Parameters.AddWithValue("@IdStruttura", IdStruttura)
        cmdDel.Parameters.AddWithValue("@IdMansione", IdMansione)
        cmdDel.Parameters.AddWithValue("@CodiceOperatore", CodiceOperatore)
        cmdDel.Parameters.AddWithValue("@Ore", Ore)
        cmdDel.Parameters.AddWithValue("@Imponibile", Importo)
        cmdDel.Parameters.AddWithValue("@CodiceIVA", CodiceIVA)

        cmdDel.Parameters.AddWithValue("@intypecod", intypecod)

        cmdDel.Parameters.AddWithValue("@intypedescription", intypedescription)
        cmdDel.Parameters.AddWithValue("@CodiceOspite", CodiceOspite)

        cmdDel.Parameters.AddWithValue("@CodiceCooperativa", CodiceCooperativa)

        cmdDel.Parameters.AddWithValue("@CalcoloPassivo", CalcoloPassivo)

        cmdDel.Parameters.AddWithValue("@Done", Done)

        'CalcoloPassivo

        'CodiceCooperativa

        cmdDel.ExecuteNonQuery()


        cnGenerale.Close()

    End Sub


    Private Sub ElaboraFatture()
        Dim cnGenerale As New OleDbConnection


        Dim ConnectionStringGenerale As String = Session("DC_GENERALE")

        cnGenerale = New Data.OleDb.OleDbConnection(ConnectionStringGenerale)

        cnGenerale.Open()

        Dim cmdDel As New OleDbCommand
        cmdDel.CommandText = "Delete From Temp_MovimentiContabiliTesta"
        cmdDel.Connection = cnGenerale
        cmdDel.ExecuteNonQuery()



        cmdDel.CommandText = "Delete From Temp_MovimentiContabiliRiga"
        cmdDel.Connection = cnGenerale
        cmdDel.ExecuteNonQuery()



        cmdDel.CommandText = "Delete From Temp_Scadenzario"
        cmdDel.Connection = cnGenerale
        cmdDel.ExecuteNonQuery()
        cnGenerale.Close()




        Dim cn As New OleDbConnection


        Dim ConnectionString As String = Session("DC_TABELLE")

        cn = New Data.OleDb.OleDbConnection(ConnectionString)

        cn.Open()

        Dim cmdDelElaborato As New OleDbCommand

        If Val(DD_Appalto.SelectedValue) = 0 Then
            cmdDelElaborato.CommandText = "Delete From Appalti_PrestazioniElaborate where  year(Data) = " & Txt_Anno.Text & " And Month(Data) = " & Val(Dd_Mese.SelectedValue)
        Else
            cmdDelElaborato.CommandText = "Delete From Appalti_PrestazioniElaborate where  year(Data) = " & Txt_Anno.Text & " And Month(Data) = " & Val(Dd_Mese.SelectedValue) & " And IdAppalto = " & Val(DD_Appalto.SelectedValue)
        End If

        cmdDelElaborato.Connection = cn
        cmdDelElaborato.ExecuteNonQuery()


        Dim cmd As New OleDbCommand
        Dim Condizione As String = ""
        Dim IdAppalto As Integer = 0
        Dim IdAppaltoUlt As Integer = 0
        Dim Movimenti As New Cls_MovimentoContabile

        If Val(DD_Appalto.SelectedValue) = 0 Then
            cmd.CommandText = "Select IdAppalto From Appalti_StruttureMansioni Where (select DataInizio From Appalti_Anagrafica Where Appalti_Anagrafica.Id = IdAppalto) <= ? And   (select DataFine From Appalti_Anagrafica Where Appalti_Anagrafica.Id = IdAppalto) >= ?  group by IdAppalto"
        Else
            cmd.CommandText = "Select IdAppalto From Appalti_StruttureMansioni Where (select DataInizio From Appalti_Anagrafica Where Appalti_Anagrafica.Id = IdAppalto) <= ? And   (select DataFine From Appalti_Anagrafica Where Appalti_Anagrafica.Id = IdAppalto) >= ? And IdAppalto = " & Val(DD_Appalto.SelectedValue) & "  group by IdAppalto"
        End If

        cmd.Connection = cn
        cmd.Parameters.AddWithValue("@DataFine", DateSerial(Txt_Anno.Text, Dd_Mese.SelectedValue, GiorniMese(Dd_Mese.SelectedValue, Txt_Anno.Text)))
        cmd.Parameters.AddWithValue("@DataInizio", DateSerial(Txt_Anno.Text, Dd_Mese.SelectedValue, 1))


        Dim StruttureMansioni As OleDbDataReader = cmd.ExecuteReader()
        Do While StruttureMansioni.Read
            Dim Appalto As New Cls_AppaltiAnagrafica


            Appalto.Id = campodbN(StruttureMansioni.Item("IdAppalto"))
            Appalto.Leggi(Session("DC_TABELLE"))

            If Appalto.RotturaStruttura = 1 Then
                FattureStrutturaAppalto(Appalto.Id)
            Else
                FattureAppalto(Appalto.Id)
            End If

        Loop

        StruttureMansioni.Close()



        Dim cmdScadenza As New OleDbCommand
        cmdScadenza.CommandText = "Select * From Appalti_Anagrafica Where DataFine >= ? and DataFine <= ?"
        cmdScadenza.Connection = cn
        cmdScadenza.Parameters.AddWithValue("@DataInizio", DateSerial(Txt_Anno.Text, Dd_Mese.SelectedValue, 1))
        cmdScadenza.Parameters.AddWithValue("@DataFine", DateSerial(Txt_Anno.Text, Dd_Mese.SelectedValue, GiorniMese(Dd_Mese.SelectedValue, Txt_Anno.Text)))
        Dim ReadScadenza As OleDbDataReader = cmdScadenza.ExecuteReader()
        Do While ReadScadenza.Read
            Session("CampoWr") = Session("CampoWr") & vbNewLine & "(W) Appalto " & campodb(ReadScadenza.Item("Descrizione")) & " in scadenza "

        Loop
        ReadScadenza.Close()

        cn.Close()

    End Sub

    Private Sub FattureStrutturaAppalto(ByVal CodiceAppalto As Integer)
        Dim TotaleAppalto As Double = 0
        Dim cn As New OleDbConnection


        Dim ConnectionString As String = Session("DC_TABELLE")

        cn = New Data.OleDb.OleDbConnection(ConnectionString)

        cn.Open()


        Dim cmd As New OleDbCommand
        Dim Indice As Integer = 1

        Dim Appalto As New Cls_AppaltiAnagrafica


        Appalto.Id = CodiceAppalto
        Appalto.Leggi(Session("DC_TABELLE"))



        cmd.CommandText = "Select IdStrutture From Appalti_StruttureMansioni  where IdAppalto = " & CodiceAppalto & "   group by IdStrutture"
        cmd.Connection = cn
        Dim StruttureMansioni As OleDbDataReader = cmd.ExecuteReader()
        Do While StruttureMansioni.Read
            FattureStruttura(CodiceAppalto, campodbN(StruttureMansioni.Item("IdStrutture")), TotaleAppalto)
        Loop
        StruttureMansioni.Close()


        cn.Close()


        If Appalto.ImportoTeoricoMensile > 0 Then
            If Math.Round(TotaleAppalto, 2) <> Math.Round(Appalto.ImportoTeoricoMensile, 2) Then
                Session("CampoWr") = Session("CampoWr") & vbNewLine & "(W) Importo teorico " & Format(Appalto.ImportoTeoricoMensile, "#,##0.00") & " è diverso da importo calcolato " & Format(TotaleAppalto, "#,##0.00") & " per appalto " & Appalto.Descrizione
            End If
        End If
        If Appalto.ImportoTeoricoGiornaliero > 0 Then
            Dim TotAppoggio As Double
            TotAppoggio = (GiorniMese(Dd_Mese.SelectedValue, Txt_Anno.Text) * Appalto.ImportoTeoricoGiornaliero) + Appalto.ImportoTeoricoMensileInaddGiornaliero

            If Math.Round(TotaleAppalto, 2) <> Math.Round(TotAppoggio, 2) Then
                Session("CampoWr") = Session("CampoWr") & vbNewLine & "(W) Importo teorico " & Format(TotAppoggio, "#,##0.00") & " è diverso da importo calcolato " & Format(TotaleAppalto, "#,##0.00") & " per appalto " & Appalto.Descrizione
            End If
        End If


    End Sub

    Private Sub FattureStruttura(ByVal CodiceAppalto As Integer, ByVal Struttura As Integer, ByRef TotaleAppalto As Double)

        If Struttura = 183 Then
            Struttura = 183
        End If

        Dim cn As New OleDbConnection


        Dim ConnectionString As String = Session("DC_TABELLE")

        cn = New Data.OleDb.OleDbConnection(ConnectionString)

        cn.Open()


        Dim cmd As New OleDbCommand
        Dim Indice As Integer = 1
        Dim Appalto As New Cls_AppaltiAnagrafica
        Dim CausaleContabile As New Cls_CausaleContabile
        Dim Committente As New ClsUSL

        Appalto.Id = CodiceAppalto
        Appalto.Leggi(Session("DC_TABELLE"))


        Committente.CodiceRegione = Appalto.Committente
        Committente.Leggi(Session("DC_OSPITE"))


        Dim StrutturaAppalto As New Cls_Appalti_AppaltiStrutture


        StrutturaAppalto.IdAppalto = CodiceAppalto
        StrutturaAppalto.IdStrutture = Struttura
        StrutturaAppalto.Leggi(Session("DC_TABELLE"))


        CausaleContabile.Leggi(Session("DC_TABELLE"), Appalto.CausaleContabile)

        Dim Movimenti As New Cls_MovimentoContabile

        Movimenti.DataRegistrazione = Txt_Data.Text
        Movimenti.MeseCompetenza = Dd_Mese.SelectedValue
        Movimenti.AnnoCompetenza = Txt_Anno.Text
        Movimenti.AnnoProtocollo = Year(Txt_Data.Text)
        Movimenti.CausaleContabile = Appalto.CausaleContabile

        Movimenti.DataDocumento = Txt_Data.Text


        Movimenti.RegistroIVA = CausaleContabile.RegistroIVA
        Movimenti.Tipologia = "R" & Appalto.Committente
        Movimenti.IdProgettoODV = Appalto.Id

        Dim ModPag As New ClsModalitaPagamento

        ModPag.Codice = Committente.ModalitaPagamento
        ModPag.Leggi(Session("DC_OSPITE"))

        Movimenti.CodicePagamento = ModPag.ModalitaPagamento


        cmd.CommandText = "Select * From Appalti_StruttureMansioni  where IdAppalto = " & CodiceAppalto & " And IdStrutture = " & Struttura & " Order by IdMansione"
        cmd.Connection = cn
        Dim StruttureMansioni As OleDbDataReader = cmd.ExecuteReader()
        Do While StruttureMansioni.Read
            If campodb(StruttureMansioni.Item("Regola")) = "O" Then
                RigaMansioneOra(Movimenti, CodiceAppalto, campodbN(StruttureMansioni.Item("IdStrutture")), campodbN(StruttureMansioni.Item("IdMansione")), campodbN(StruttureMansioni.Item("Importo")), Indice)
            End If



            '

            If campodb(StruttureMansioni.Item("Regola")) = "A" Then
                RigaMansioneAccessi(Movimenti, CodiceAppalto, campodbN(StruttureMansioni.Item("IdStrutture")), campodbN(StruttureMansioni.Item("IdMansione")), campodbN(StruttureMansioni.Item("Importo")), Indice, 1)
            End If

            If campodb(StruttureMansioni.Item("Regola")) = "a" Then
                RigaMansioneAccessi(Movimenti, CodiceAppalto, campodbN(StruttureMansioni.Item("IdStrutture")), campodbN(StruttureMansioni.Item("IdMansione")), campodbN(StruttureMansioni.Item("Importo")), Indice, 2)
            End If

            If campodb(StruttureMansioni.Item("Regola")) = "G" Then
                RigaMansioneGiorno(Movimenti, CodiceAppalto, campodbN(StruttureMansioni.Item("IdStrutture")), campodbN(StruttureMansioni.Item("IdMansione")), campodbN(StruttureMansioni.Item("Importo")), Indice)
            End If

            If campodb(StruttureMansioni.Item("Regola")) = "F" Then
                RigaMansioneFisso(Movimenti, CodiceAppalto, campodbN(StruttureMansioni.Item("IdStrutture")), campodbN(StruttureMansioni.Item("IdMansione")), campodbN(StruttureMansioni.Item("Importo")), Indice)
            End If

            If campodb(StruttureMansioni.Item("Regola")) = "P" Then
                RigaMansioneAccessiPassivi(Movimenti, CodiceAppalto, campodbN(StruttureMansioni.Item("IdStrutture")), campodbN(StruttureMansioni.Item("IdMansione")), campodbN(StruttureMansioni.Item("Importo")), Indice, 1)
            End If

            If campodb(StruttureMansioni.Item("Regola")) = "p" Then
                RigaMansioneAccessiPassivi(Movimenti, CodiceAppalto, campodbN(StruttureMansioni.Item("IdStrutture")), campodbN(StruttureMansioni.Item("IdMansione")), campodbN(StruttureMansioni.Item("Importo")), Indice, 2)
            End If

            If campodb(StruttureMansioni.Item("Regola")) = "o" Then
                RigaMansioneOraPassivi(Movimenti, CodiceAppalto, campodbN(StruttureMansioni.Item("IdStrutture")), campodbN(StruttureMansioni.Item("IdMansione")), campodbN(StruttureMansioni.Item("Importo")), Indice)
            End If

        Loop

        StruttureMansioni.Close()



        cmd.CommandText = "Select * From Appalti_AppaltiStrutture  where IdAppalto = " & CodiceAppalto & " And IdStrutture = " & Struttura & " Order by IdStrutture"
        cmd.Connection = cn
        Dim StruttureAppalti As OleDbDataReader = cmd.ExecuteReader()
        Do While StruttureAppalti.Read
            If campodbN(StruttureAppalti.Item("KMImporto")) > 0 Then
                RigaKM(Movimenti, CodiceAppalto, campodbN(StruttureAppalti.Item("IdStrutture")), campodbN(StruttureAppalti.Item("KM")), campodbN(StruttureAppalti.Item("KMImporto")), Indice)
            End If
        Loop
        StruttureAppalti.Close()


        cn.Close()


        Call RigheExtra(Movimenti, Appalto.Id, Struttura, Indice)



        Dim TotaleDocumento As Double = 0

        RigheIVA(Movimenti, TotaleDocumento, Indice, CausaleContabile)


        RigheCliente(Movimenti, Modulo.MathRound(TotaleDocumento, 2), Committente, CausaleContabile)

        TabRegistri = Session("TabRegistri")
        For I = 0 To TabRegistri.Rows.Count - 1
            If Val(TabRegistri.Rows(I).Item(0)) = Movimenti.RegistroIVA Then
                'TabRegistri.Clear()
                'TabRegistri.Columns.Clear()
                'TabRegistri.Columns.Add("Numero", GetType(String))
                'TabRegistri.Columns.Add("Registro", GetType(String))
                'TabRegistri.Columns.Add("Protocollo1", GetType(String))
                'TabRegistri.Columns.Add("Protocollo2", GetType(String))
                If Movimenti.MaxProtocolloTemp(Session("DC_GENERALE"), Movimenti.AnnoProtocollo, Movimenti.RegistroIVA) > Movimenti.NumeroProtocollo Then
                    Movimenti.NumeroProtocollo = Movimenti.MaxProtocolloTemp(Session("DC_GENERALE"), Movimenti.AnnoProtocollo, Movimenti.RegistroIVA) + 1
                End If
            End If
        Next


        TabRegistri = Session("TabRegistri")
        For I = 0 To TabRegistri.Rows.Count - 1
            If Val(TabRegistri.Rows(I).Item(0)) = Movimenti.RegistroIVA Then
                'TabRegistri.Clear()
                'TabRegistri.Columns.Clear()
                'TabRegistri.Columns.Add("Numero", GetType(String))
                'TabRegistri.Columns.Add("Registro", GetType(String))
                'TabRegistri.Columns.Add("Protocollo1", GetType(String))
                'TabRegistri.Columns.Add("Protocollo2", GetType(String))
                If Movimenti.MaxProtocolloTemp(Session("DC_GENERALE"), Movimenti.AnnoProtocollo, Movimenti.RegistroIVA) > Val(TabRegistri.Rows(I).Item(2)) Then
                    Movimenti.NumeroProtocollo = Movimenti.MaxProtocolloTemp(Session("DC_GENERALE"), Movimenti.AnnoProtocollo, Movimenti.RegistroIVA) + 1
                Else
                    Movimenti.NumeroProtocollo = Val(TabRegistri.Rows(I).Item(2)) + 1
                End If
            End If
        Next

        Movimenti.NumeroDocumento = Movimenti.NumeroProtocollo

        
        If Movimenti.Scrivi(Session("DC_GENERALE"), 0, True) = False Then
            Session("CampoErrori") = Session("CampoErrori") & vbNewLine & "Errori creazione documento appalto " & Appalto.Descrizione & " Struttura " & Struttura
        End If

        TotaleAppalto = TotaleAppalto + TotaleDocumento


        If Appalto.Tipo = "F" Then
            RaggruppaRegistrazione(Movimenti.NumeroRegistrazione)

            
            RegistrazioneForfait(Movimenti.NumeroRegistrazione, Appalto, CausaleContabile, Movimenti, StrutturaAppalto.ImportoForfait, StrutturaAppalto.IdStrutture)
        Else
            If Appalto.RaggruppaRicavi = 1 Or Appalto.RaggruppaSenzaPresaInCarico = 1 Then
                RaggruppaRegistrazione(Movimenti.NumeroRegistrazione)
            End If
        End If
        Movimenti.Leggi(Session("DC_GENERALE"), Movimenti.NumeroRegistrazione, True)


        If Movimenti.ImportoDocumento(Session("DC_TABELLE")) = 0 Then
            EliminaRegistrazione(Session("DC_GENERALE"), Movimenti.NumeroRegistrazione)
        Else
            GeneraScadenza(Movimenti.NumeroRegistrazione, Movimenti.CodicePagamento, Movimenti.ImportoDocumento(Session("DC_TABELLE")), Movimenti.DataRegistrazione)
        End If
    End Sub



    Private Sub FattureAppalto(ByVal CodiceAppalto As Integer)

        Dim cn As New OleDbConnection


        Dim ConnectionString As String = Session("DC_TABELLE")

        cn = New Data.OleDb.OleDbConnection(ConnectionString)

        cn.Open()


        Dim cmd As New OleDbCommand
        Dim Indice As Integer = 1
        Dim Appalto As New Cls_AppaltiAnagrafica
        Dim CausaleContabile As New Cls_CausaleContabile
        Dim Committente As New ClsUSL

        Appalto.Id = CodiceAppalto
        Appalto.Leggi(Session("DC_TABELLE"))


        Committente.CodiceRegione = Appalto.Committente
        Committente.Leggi(Session("DC_OSPITE"))


        CausaleContabile.Leggi(Session("DC_TABELLE"), Appalto.CausaleContabile)

        Dim Movimenti As New Cls_MovimentoContabile

        Movimenti.DataRegistrazione = Txt_Data.Text
        Movimenti.MeseCompetenza = Dd_Mese.SelectedValue
        Movimenti.AnnoCompetenza = Txt_Anno.Text
        Movimenti.AnnoProtocollo = Year(Txt_Data.Text)
        Movimenti.CausaleContabile = Appalto.CausaleContabile
        Movimenti.RegistroIVA = CausaleContabile.RegistroIVA
        Movimenti.Tipologia = "R" & Appalto.Committente
        Movimenti.IdProgettoODV = Appalto.Id

        Dim ModPag As New ClsModalitaPagamento

        ModPag.Codice = Committente.ModalitaPagamento
        ModPag.Leggi(Session("DC_OSPITE"))

        Movimenti.CodicePagamento = ModPag.ModalitaPagamento
        Movimenti.DataDocumento = Txt_Data.Text


        If Movimenti.MaxProtocolloTemp(Session("DC_GENERALE"), Movimenti.AnnoProtocollo, Movimenti.RegistroIVA) > Movimenti.MaxProtocollo(Session("DC_GENERALE"), Movimenti.AnnoProtocollo, Movimenti.RegistroIVA) Then
            Movimenti.NumeroProtocollo = Movimenti.MaxProtocolloTemp(Session("DC_GENERALE"), Movimenti.AnnoProtocollo, Movimenti.RegistroIVA) + 1
        Else
            Movimenti.NumeroProtocollo = Movimenti.MaxProtocollo(Session("DC_GENERALE"), Movimenti.AnnoProtocollo, Movimenti.RegistroIVA) + 1
        End If




        cmd.CommandText = "Select * From Appalti_StruttureMansioni  where IdAppalto = " & CodiceAppalto & " Order by IdStrutture,IdMansione"
        cmd.Connection = cn
        Dim StruttureMansioni As OleDbDataReader = cmd.ExecuteReader()
        Do While StruttureMansioni.Read
            If campodb(StruttureMansioni.Item("Regola")) = "O" Then
                RigaMansioneOra(Movimenti, CodiceAppalto, campodbN(StruttureMansioni.Item("IdStrutture")), campodbN(StruttureMansioni.Item("IdMansione")), campodbN(StruttureMansioni.Item("Importo")), Indice)
            End If

            If campodb(StruttureMansioni.Item("Regola")) = "A" Then
                RigaMansioneAccessi(Movimenti, CodiceAppalto, campodbN(StruttureMansioni.Item("IdStrutture")), campodbN(StruttureMansioni.Item("IdMansione")), campodbN(StruttureMansioni.Item("Importo")), Indice, 1)
            End If

            If campodb(StruttureMansioni.Item("Regola")) = "a" Then
                RigaMansioneAccessi(Movimenti, CodiceAppalto, campodbN(StruttureMansioni.Item("IdStrutture")), campodbN(StruttureMansioni.Item("IdMansione")), campodbN(StruttureMansioni.Item("Importo")), Indice, 2)
            End If

            If campodb(StruttureMansioni.Item("Regola")) = "G" Then
                RigaMansioneGiorno(Movimenti, CodiceAppalto, campodbN(StruttureMansioni.Item("IdStrutture")), campodbN(StruttureMansioni.Item("IdMansione")), campodbN(StruttureMansioni.Item("Importo")), Indice)
            End If

            If campodb(StruttureMansioni.Item("Regola")) = "F" Then
                RigaMansioneFisso(Movimenti, CodiceAppalto, campodbN(StruttureMansioni.Item("IdStrutture")), campodbN(StruttureMansioni.Item("IdMansione")), campodbN(StruttureMansioni.Item("Importo")), Indice)
            End If

            If campodb(StruttureMansioni.Item("Regola")) = "P" Then
                RigaMansioneAccessiPassivi(Movimenti, CodiceAppalto, campodbN(StruttureMansioni.Item("IdStrutture")), campodbN(StruttureMansioni.Item("IdMansione")), campodbN(StruttureMansioni.Item("Importo")), Indice, 1)
            End If

            If campodb(StruttureMansioni.Item("Regola")) = "p" Then
                RigaMansioneAccessiPassivi(Movimenti, CodiceAppalto, campodbN(StruttureMansioni.Item("IdStrutture")), campodbN(StruttureMansioni.Item("IdMansione")), campodbN(StruttureMansioni.Item("Importo")), Indice, 2)
            End If

            If campodb(StruttureMansioni.Item("Regola")) = "o" Then
                RigaMansioneOraPassivi(Movimenti, CodiceAppalto, campodbN(StruttureMansioni.Item("IdStrutture")), campodbN(StruttureMansioni.Item("IdMansione")), campodbN(StruttureMansioni.Item("Importo")), Indice)
            End If
        Loop
        StruttureMansioni.Close()


        cmd.CommandText = "Select * From Appalti_AppaltiStrutture  where IdAppalto = " & CodiceAppalto & " Order by IdStrutture"
        cmd.Connection = cn
        Dim StruttureAppalti As OleDbDataReader = cmd.ExecuteReader()
        Do While StruttureAppalti.Read
            If campodbN(StruttureAppalti.Item("KMImporto")) > 0 Then
                RigaKM(Movimenti, CodiceAppalto, campodbN(StruttureAppalti.Item("IdStrutture")), campodbN(StruttureAppalti.Item("KM")), campodbN(StruttureAppalti.Item("KMImporto")), Indice)
            End If
        Loop
        StruttureAppalti.Close()

        cn.Close()

        Call RigheExtra(Movimenti, Appalto.Id, 0, Indice)




        Dim TotaleDocumento As Double = 0

        RigheIVA(Movimenti, TotaleDocumento, Indice, CausaleContabile)


        RigheCliente(Movimenti, Modulo.MathRound(TotaleDocumento, 2), Committente, CausaleContabile)



        TabRegistri = Session("TabRegistri")
        For I = 0 To TabRegistri.Rows.Count - 1
            If Val(TabRegistri.Rows(I).Item(0)) = Movimenti.RegistroIVA Then
                'TabRegistri.Clear()
                'TabRegistri.Columns.Clear()
                'TabRegistri.Columns.Add("Numero", GetType(String))
                'TabRegistri.Columns.Add("Registro", GetType(String))
                'TabRegistri.Columns.Add("Protocollo1", GetType(String))
                'TabRegistri.Columns.Add("Protocollo2", GetType(String))
                If Movimenti.MaxProtocolloTemp(Session("DC_GENERALE"), Movimenti.AnnoProtocollo, Movimenti.RegistroIVA) > Val(TabRegistri.Rows(I).Item(2)) Then
                    Movimenti.NumeroProtocollo = Movimenti.MaxProtocolloTemp(Session("DC_GENERALE"), Movimenti.AnnoProtocollo, Movimenti.RegistroIVA) + 1
                Else
                    Movimenti.NumeroProtocollo = Val(TabRegistri.Rows(I).Item(2)) + 1
                End If
            End If
        Next

        Movimenti.NumeroDocumento = Movimenti.NumeroProtocollo



        If Movimenti.Scrivi(Session("DC_GENERALE"), 0, True) = False Then
            Session("CampoErrori") = Session("CampoErrori") & vbNewLine & "Errori creazione documento appalto " & Appalto.Descrizione
        End If


        If Appalto.Tipo = "F" Then
            If Math.Round(TotaleDocumento, 2) <> Math.Round(Appalto.ImportoForfait, 2) Then
                Session("CampoWr") = Session("CampoWr") & vbNewLine & "(W) Importo forfait " & Format(Appalto.ImportoForfait, "#,##0.00") & " è diverso da importo calcolato " & Format(TotaleDocumento, "#,##0.00") & " per appalto " & Appalto.Descrizione
            End If

            RaggruppaRegistrazione(Movimenti.NumeroRegistrazione)
            RegistrazioneForfait(Movimenti.NumeroRegistrazione, Appalto, CausaleContabile, Movimenti)
        Else
            If Appalto.ImportoTeoricoMensile > 0 Then
                If Math.Round(TotaleDocumento, 2) <> Math.Round(Appalto.ImportoTeoricoMensile, 2) Then
                    Session("CampoWr") = Session("CampoWr") & vbNewLine & "(W) Importo teorico " & Format(Appalto.ImportoTeoricoMensile, "#,##0.00") & " è diverso da importo calcolato " & Format(TotaleDocumento, "#,##0.00") & " per appalto " & Appalto.Descrizione
                End If
            End If
            If Appalto.ImportoTeoricoGiornaliero > 0 Then
                Dim TotAppoggio As Double
                TotAppoggio = (GiorniMese(Dd_Mese.SelectedValue, Txt_Anno.Text) * Appalto.ImportoTeoricoGiornaliero) + Appalto.ImportoTeoricoMensileInaddGiornaliero

                If Math.Round(TotaleDocumento, 2) <> Math.Round(TotAppoggio, 2) Then
                    Session("CampoWr") = Session("CampoWr") & vbNewLine & "(W) Importo teorico " & Format(TotAppoggio, "#,##0.00") & " è diverso da importo calcolato " & Format(TotaleDocumento, "#,##0.00") & " per appalto " & Appalto.Descrizione
                End If
            End If

            If Appalto.RaggruppaRicavi = 1 Or Appalto.RaggruppaSenzaPresaInCarico = 1 Then
                RaggruppaRegistrazione(Movimenti.NumeroRegistrazione)
            End If
        End If
        Movimenti.Leggi(Session("DC_GENERALE"), Movimenti.NumeroRegistrazione, True)
        If Movimenti.ImportoDocumento(Session("DC_TABELLE")) = 0 Then
            EliminaRegistrazione(Session("DC_GENERALE"), Movimenti.NumeroRegistrazione)

        Else
            
            GeneraScadenza(Movimenti.NumeroRegistrazione, Movimenti.CodicePagamento, Movimenti.ImportoDocumento(Session("DC_TABELLE")), Movimenti.DataRegistrazione)
        End If
    End Sub

    Sub EliminaRegistrazione(ByVal StringaConnessione As String, ByVal NumeroRegistrazione As Long)
        Dim cn As OleDbConnection


        cn = New Data.OleDb.OleDbConnection(StringaConnessione)

        cn.Open()
        Dim cmd As New OleDbCommand()
        cmd.CommandText = ("Delete  from temp_MovimentiContabiliTesta where " & _
                           "NumeroRegistrazione = ? ")
        cmd.Parameters.AddWithValue("@NumeroRegistrazione", NumeroRegistrazione)
        cmd.Connection = cn
        cmd.ExecuteNonQuery()

        Dim cmdR As New OleDbCommand()
        cmdR.CommandText = ("Delete  from temp_MovimentiContabiliRiga where " & _
                           "Numero = ? ")
        cmdR.Parameters.AddWithValue("@Numero", NumeroRegistrazione)
        cmdR.Connection = cn
        cmdR.ExecuteNonQuery()

        cn.Close()
    End Sub

    Public Function GiorniMese(ByVal Mese As Byte, ByVal Anno As Integer) As Byte
        If Mese = 1 Or Mese = 3 Or Mese = 5 Or Mese = 7 Or Mese = 8 Or _
           Mese = 10 Or Mese = 12 Then
            GiorniMese = 31
        Else
            If Mese <> 2 Then
                GiorniMese = 30
            Else
                If Day(DateSerial(Anno, Mese, 29)) = 29 Then
                    GiorniMese = 29
                Else
                    GiorniMese = 28
                End If
            End If
        End If
    End Function
    Private Sub RigheCliente(ByRef Movimenti As Cls_MovimentoContabile, ByRef TotaleDocumento As Double, ByVal Committente As ClsUSL, ByVal CausaleContabile As Cls_CausaleContabile)

        Movimenti.Righe(0) = New Cls_MovimentiContabiliRiga

        Movimenti.Righe(0).Numero = Movimenti.NumeroRegistrazione
        Movimenti.Righe(0).RigaDaCausale = 1
        Movimenti.Righe(0).MastroPartita = Committente.MastroCliente
        Movimenti.Righe(0).ContoPartita = Committente.ContoCliente
        Movimenti.Righe(0).SottocontoPartita = Committente.SottoContoCliente
        Movimenti.Righe(0).Tipo = "CF"

        Movimenti.Righe(0).Importo = TotaleDocumento

        Movimenti.Righe(0).Quantita = 0
        Movimenti.Righe(0).DareAvere = CausaleContabile.Righe(0).DareAvere
    End Sub

    Private Sub RigheIVA(ByRef Movimenti As Cls_MovimentoContabile, ByRef TotaleDocumento As Double, ByVal Indice As Integer, ByVal CausaleContabile As Cls_CausaleContabile)
        TotaleDocumento = 0
        Dim I As Integer
        Dim VettoreAliquote(100) As String
        Dim VettoreImponibile(100) As Double
        Dim VettoreImposta(100) As Double

        For I = 1 To Indice + 1
            If Not IsNothing(Movimenti.Righe(I)) Then
                If Movimenti.Righe(I).Tipo = "" Then
                    Dim CercaAliquota As Integer

                    If I = 131 Then
                        CercaAliquota = 0
                    End If

                    For CercaAliquota = 0 To 100
                        If Movimenti.Righe(I).CodiceIVA = VettoreAliquote(CercaAliquota) Then
                            VettoreImponibile(CercaAliquota) = VettoreImponibile(CercaAliquota) + Movimenti.Righe(I).Importo
                            Exit For
                        End If
                    Next
                    If CercaAliquota >= 100 Then
                        For CercaAliquota = 0 To 100
                            If VettoreAliquote(CercaAliquota) = "" Then
                                Exit For
                            End If
                        Next
                        VettoreAliquote(CercaAliquota) = Movimenti.Righe(I).CodiceIVA
                        VettoreImponibile(CercaAliquota) = VettoreImponibile(CercaAliquota) + Movimenti.Righe(I).Importo
                    End If

                End If
            End If
        Next
        Dim ScorriAliquota As Integer

        For ScorriAliquota = 0 To 100
            If VettoreAliquote(ScorriAliquota) = "" Then
                Exit For
            End If
            Dim CodIva As New Cls_IVA

            CodIva.Codice = VettoreAliquote(ScorriAliquota)
            CodIva.Leggi(Session("DC_TABELLE"), CodIva.Codice)

            VettoreImposta(ScorriAliquota) = Modulo.MathRound(CodIva.Aliquota * Modulo.MathRound(VettoreImponibile(ScorriAliquota), 2), 2)


            Movimenti.Righe(Indice) = New Cls_MovimentiContabiliRiga

            Movimenti.Righe(Indice).Numero = Movimenti.NumeroRegistrazione
            Movimenti.Righe(Indice).RigaDaCausale = 2
            Movimenti.Righe(Indice).MastroPartita = CausaleContabile.Righe(1).Mastro
            Movimenti.Righe(Indice).ContoPartita = CausaleContabile.Righe(1).Conto
            Movimenti.Righe(Indice).SottocontoPartita = CausaleContabile.Righe(1).Sottoconto
            Movimenti.Righe(Indice).CodiceIVA = CodIva.Codice
            Movimenti.Righe(Indice).Tipo = "IV"



            Movimenti.Righe(Indice).Importo = VettoreImposta(ScorriAliquota)
            Movimenti.Righe(Indice).Imponibile = Modulo.MathRound(VettoreImponibile(ScorriAliquota), 2)

            Movimenti.Righe(Indice).Quantita = 0
            Movimenti.Righe(Indice).DareAvere = CausaleContabile.Righe(1).DareAvere

            TotaleDocumento = TotaleDocumento + VettoreImposta(ScorriAliquota) + VettoreImponibile(ScorriAliquota)


            Indice = Indice + 1
        Next
    End Sub
    Private Sub RigaMansioneOraPassivi(ByRef MovimentiContabilie As Cls_MovimentoContabile, ByVal IdAppalto As Integer, ByVal IdStruttura As Integer, ByVal IdMansione As String, ByVal ImportoOrario As Double, ByRef Indice As Integer, Optional ByVal Filtro As Integer = 1)

        Dim cn As New OleDbConnection
        Dim CausaleContabile As New Cls_CausaleContabile

        Dim ConnectionString As String = Session("DC_TABELLE")

        cn = New Data.OleDb.OleDbConnection(ConnectionString)

        cn.Open()



        CausaleContabile.Leggi(Session("DC_TABELLE"), MovimentiContabilie.CausaleContabile)


        Dim Condizione As String

        If Filtro = 1 Then
            Condizione = " (done is null or Done =1 or Done =5)"
        Else
            Condizione = " (Done =2)"
        End If


        Dim cmd As New OleDbCommand


        cmd.CommandText = "Select * From Appalti_Prestazioni  where year(Data) = " & Txt_Anno.Text & " And Month(Data) = " & Val(Dd_Mese.SelectedValue) & " And  IdAppalto = " & IdAppalto & " And  IdStrutture = " & IdStruttura & " And IdMansione =" & IdMansione & " And " & Condizione & " Order by Data,CodiceOperatore"
        cmd.Connection = cn
        Dim StruttureMansioni As OleDbDataReader = cmd.ExecuteReader()
        Do While StruttureMansioni.Read
            Dim ImportoIntervento As Double
            Dim OreMinuti As Double
            Dim Minuti As Double

            OreMinuti = Math.Round(campodbN(StruttureMansioni.Item("Ore")), 4)

            Minuti = Math.Abs(Int(OreMinuti) - OreMinuti)
            If Minuti <= 0.25 Then
                OreMinuti = Int(OreMinuti)
            End If
            If Minuti >= 0.75 Then
                OreMinuti = Int(OreMinuti) + 1
            End If



            ImportoIntervento = Modulo.MathRound(OreMinuti * Math.Round(ImportoOrario, 4), 15)


            Dim Mansione As New Cls_AppaltiTabellaMansione


            Mansione.ID = IdMansione
            Mansione.Leggi(Session("DC_TABELLE"), Mansione.ID)

            Dim Operatore As New Cls_Operatore


            Operatore.CodiceMedico = campodbN(StruttureMansioni.Item("CodiceOperatore"))
            Operatore.Leggi(Session("DC_OSPITE"))


            Dim StrutturaMansione As New Cls_Appalti_StruttureMansioni


            StrutturaMansione.IdAppalto = IdAppalto
            StrutturaMansione.Struttura = IdStruttura
            StrutturaMansione.Mansione = Mansione.ID
            StrutturaMansione.Leggi(Session("DC_TABELLE"))
            If campodb(StruttureMansioni.Item("intypecod")) <> "" Then
                StrutturaMansione.IdAppalto = IdAppalto
                StrutturaMansione.Struttura = IdStruttura
                StrutturaMansione.Mansione = Mansione.ID
                StrutturaMansione.intypecod = campodb(StruttureMansioni.Item("intypecod"))
                StrutturaMansione.Leggiintypecod(Session("DC_TABELLE"))
            End If


            'MovimentiContabilie.Righe(Indice).Quantita = Math.Round(campodbN(StruttureMansioni.Item("Ore")), 4) * 10000
            Scrivi(campodbD(StruttureMansioni.Item("Data")), IdAppalto, IdStruttura, IdMansione, Operatore.CodiceMedico, OreMinuti, 1, ImportoIntervento, StrutturaMansione.CodiceIVA, campodb(StruttureMansioni.Item("intypecod")), campodb(StruttureMansioni.Item("intypedescription")), campodbN(StruttureMansioni.Item("CodiceOspite")), campodbN(StruttureMansioni.Item("codicecooperativa")), 1)
        Loop
        StruttureMansioni.Close()


        cn.Close()

    End Sub


    Private Sub RigaMansioneOra(ByRef MovimentiContabilie As Cls_MovimentoContabile, ByVal IdAppalto As Integer, ByVal IdStruttura As Integer, ByVal IdMansione As String, ByVal ImportoOrario As Double, ByRef Indice As Integer)

        Dim cn As New OleDbConnection
        Dim CausaleContabile As New Cls_CausaleContabile

        Dim ConnectionString As String = Session("DC_TABELLE")

        cn = New Data.OleDb.OleDbConnection(ConnectionString)

        cn.Open()

        If IdStruttura = 189 And IdMansione = 276 Then
            IdStruttura = 189
        End If

        CausaleContabile.Leggi(Session("DC_TABELLE"), MovimentiContabilie.CausaleContabile)



        Dim cmd As New OleDbCommand



        cmd.CommandText = "Select * From Appalti_Prestazioni  where year(Data) = " & Txt_Anno.text & " And Month(Data) = " & Val(Dd_Mese.SelectedValue) & " And  IdAppalto = " & IdAppalto & " And  IdStrutture = " & IdStruttura & " And IdMansione =" & IdMansione & " Order by Data,CodiceOperatore"
        cmd.Connection = cn
        Dim StruttureMansioni As OleDbDataReader = cmd.ExecuteReader()
        Do While StruttureMansioni.Read
            Dim ImportoIntervento As Double

            If IdMansione = 66 Then
                IdMansione = 66
            End If
            ImportoIntervento = Modulo.MathRound(Math.Round(campodbN(StruttureMansioni.Item("Ore")), 4) * Math.Round(ImportoOrario, 4), 15)


            Dim Mansione As New Cls_AppaltiTabellaMansione


            Mansione.ID = IdMansione
            Mansione.Leggi(Session("DC_TABELLE"), Mansione.ID)

            Dim StrutturaMansione As New Cls_Appalti_StruttureMansioni


            StrutturaMansione.IdAppalto = IdAppalto
            StrutturaMansione.Struttura = IdStruttura
            StrutturaMansione.Mansione = Mansione.ID
            StrutturaMansione.Leggi(Session("DC_TABELLE"))
            If campodb(StruttureMansioni.Item("intypecod")) <> "" Then
                StrutturaMansione.IdAppalto = IdAppalto
                StrutturaMansione.Struttura = IdStruttura
                StrutturaMansione.Mansione = Mansione.ID
                StrutturaMansione.intypecod = campodb(StruttureMansioni.Item("intypecod"))
                StrutturaMansione.Leggiintypecod(Session("DC_TABELLE"))
            End If


            Dim Operatore As New Cls_Operatore


            Operatore.CodiceMedico = campodbN(StruttureMansioni.Item("CodiceOperatore"))
            Operatore.Leggi(Session("DC_OSPITE"))




            MovimentiContabilie.Righe(Indice) = New Cls_MovimentiContabiliRiga

            MovimentiContabilie.Righe(Indice).Numero = MovimentiContabilie.NumeroRegistrazione
            MovimentiContabilie.Righe(Indice).RigaDaCausale = 3
            MovimentiContabilie.Righe(Indice).MastroPartita = StrutturaMansione.Mastro
            MovimentiContabilie.Righe(Indice).ContoPartita = StrutturaMansione.Conto
            MovimentiContabilie.Righe(Indice).SottocontoPartita = StrutturaMansione.Sottoconto
            MovimentiContabilie.Righe(Indice).CodiceIVA = StrutturaMansione.CodiceIVA
            MovimentiContabilie.Righe(Indice).Tipo = ""

            MovimentiContabilie.Righe(Indice).DestinatoVendita = campodb(StruttureMansioni.Item("intypedescription"))

            Dim Struttura As New Cls_AppaltiTabellaStruttura

            Struttura.ID = IdStruttura
            Struttura.Leggi(Session("DC_TABELLE"), IdStruttura)

            MovimentiContabilie.Righe(Indice).CentroServizio = Struttura.CentroServizio

            MovimentiContabilie.CentroServizio = Struttura.CentroServizio

            MovimentiContabilie.Righe(Indice).Importo = Modulo.MathRound(ImportoIntervento, 15)

            MovimentiContabilie.Righe(Indice).Quantita = Math.Round(campodbN(StruttureMansioni.Item("Ore")), 4) * 10000
            MovimentiContabilie.Righe(Indice).DareAvere = CausaleContabile.Righe(2).DareAvere


            MovimentiContabilie.Righe(Indice).Descrizione = Operatore.Nome & " " & Format(campodbD(StruttureMansioni.Item("Data")), "dd/MM/yyyy")
            MovimentiContabilie.Righe(Indice).Invisibile = Mansione.ID



            Scrivi(campodbD(StruttureMansioni.Item("Data")), IdAppalto, IdStruttura, IdMansione, Operatore.CodiceMedico, Math.Round(campodbN(StruttureMansioni.Item("Ore")), 6), 0, ImportoIntervento, StrutturaMansione.CodiceIVA, campodb(StruttureMansioni.Item("intypecod")), campodb(StruttureMansioni.Item("intypedescription")), campodbN(StruttureMansioni.Item("CodiceOspite")), campodbN(StruttureMansioni.Item("codicecooperativa")), 0)

            Indice = Indice + 1
        Loop
        StruttureMansioni.Close()


        cn.Close()

    End Sub



    Private Sub RigaKM(ByRef MovimentiContabilie As Cls_MovimentoContabile, ByVal IdAppalto As Integer, ByVal IdStruttura As Integer, ByVal KM As Integer, ByVal ImportoKM As Double, ByRef Indice As Integer)

        Dim cn As New OleDbConnection
        Dim CausaleContabile As New Cls_CausaleContabile

        Dim ConnectionString As String = Session("DC_TABELLE")

        cn = New Data.OleDb.OleDbConnection(ConnectionString)

        cn.Open()

        Dim Appalti As New Cls_AppaltiAnagrafica

        Appalti.Id = IdAppalto

        Appalti.Leggi(ConnectionString)




        CausaleContabile.Leggi(Session("DC_TABELLE"), MovimentiContabilie.CausaleContabile)



        MovimentiContabilie.Righe(Indice) = New Cls_MovimentiContabiliRiga

        MovimentiContabilie.Righe(Indice).Numero = MovimentiContabilie.NumeroRegistrazione
        MovimentiContabilie.Righe(Indice).RigaDaCausale = 3
        MovimentiContabilie.Righe(Indice).MastroPartita = Appalti.MastroKM
        MovimentiContabilie.Righe(Indice).ContoPartita = Appalti.ContoKM
        MovimentiContabilie.Righe(Indice).SottocontoPartita = Appalti.SottocontoKM
        MovimentiContabilie.Righe(Indice).CodiceIVA = Appalti.CodiceIvaKM
        MovimentiContabilie.Righe(Indice).Tipo = ""

        Dim Struttura As New Cls_AppaltiTabellaStruttura

        Struttura.ID = IdStruttura
        Struttura.Leggi(Session("DC_TABELLE"), IdStruttura)

        MovimentiContabilie.Righe(Indice).CentroServizio = Struttura.CentroServizio

        MovimentiContabilie.CentroServizio = Struttura.CentroServizio

        MovimentiContabilie.Righe(Indice).Importo = ImportoKM

        MovimentiContabilie.Righe(Indice).Invisibile = -1

        MovimentiContabilie.Righe(Indice).Quantita = KM * 10000
        MovimentiContabilie.Righe(Indice).DareAvere = CausaleContabile.Righe(2).DareAvere

        MovimentiContabilie.Righe(Indice).Descrizione = "KM"

        Scrivi(DateSerial(Txt_Anno.Text, Dd_Mese.SelectedValue, 1), IdAppalto, IdStruttura, 0, 0, KM, 0, ImportoKM, Appalti.CodiceIva, "", "", 0, 0, 0)

        Indice = Indice + 1

        'MovimentiContabilie.Righe(Indice) = New Cls_MovimentiContabiliRiga

        'MovimentiContabilie.Righe(Indice).Numero = MovimentiContabilie.NumeroRegistrazione
        'MovimentiContabilie.Righe(Indice).RigaDaCausale = 2
        'MovimentiContabilie.Righe(Indice).MastroPartita = CausaleContabile.Righe(1).Mastro
        'MovimentiContabilie.Righe(Indice).ContoPartita = CausaleContabile.Righe(1).Conto
        'MovimentiContabilie.Righe(Indice).SottocontoPartita = CausaleContabile.Righe(1).Sottoconto
        'MovimentiContabilie.Righe(Indice).CodiceIVA = Appalti.CodiceIvaKM
        'MovimentiContabilie.Righe(Indice).Tipo = "IV"



        'MovimentiContabilie.Righe(Indice).CentroServizio = Struttura.CentroServizio

        'MovimentiContabilie.CentroServizio = Struttura.CentroServizio


        'Dim Iva As New Cls_IVA

        'Iva.Codice = Appalti.CodiceIvaKM
        'Iva.Leggi(Session("DC_TABELLE"), Iva.Codice)

        'MovimentiContabilie.Righe(Indice).Importo = Modulo.MathRound(ImportoKM * Iva.Aliquota, 2)

        'MovimentiContabilie.Righe(Indice).Quantita = 0
        'MovimentiContabilie.Righe(Indice).DareAvere = CausaleContabile.Righe(1).DareAvere


        'MovimentiContabilie.Righe(Indice).Descrizione = ""
        'MovimentiContabilie.Righe(Indice).Imponibile = ImportoKM



        'Indice = Indice + 1

        cn.Close()

    End Sub


    Private Sub RigaMansioneAccessi(ByRef MovimentiContabilie As Cls_MovimentoContabile, ByVal IdAppalto As Integer, ByVal IdStruttura As Integer, ByVal IdMansione As String, ByVal ImportoAccesso As Double, ByRef Indice As Integer, ByVal Filtro As Integer)

        Dim cn As New OleDbConnection
        Dim CausaleContabile As New Cls_CausaleContabile

        Dim ConnectionString As String = Session("DC_TABELLE")

        cn = New Data.OleDb.OleDbConnection(ConnectionString)

        cn.Open()



        CausaleContabile.Leggi(Session("DC_TABELLE"), MovimentiContabilie.CausaleContabile)

        Dim Condizione As String

        If Filtro = 1 Then
            Condizione = " (done is null or Done =1 or Done =5) "
        Else
            Condizione = " (Done =2) "
        End If


        Dim cmd As New OleDbCommand

        cmd.CommandText = "Select CodiceOperatore,intypecod,intypedescription,Data,CodiceOspite,codicecooperativa,done From Appalti_Prestazioni  where  year(Data) = " & Txt_Anno.Text & " And Month(Data) = " & Val(Dd_Mese.SelectedValue) & " And  IdAppalto = " & IdAppalto & " And  IdStrutture = " & IdStruttura & " And IdMansione =" & IdMansione & " And " & Condizione & " Group by  CodiceOperatore,intypecod,intypedescription,Data,CodiceOspite,codicecooperativa,done"
        cmd.Connection = cn
        Dim StruttureMansioni As OleDbDataReader = cmd.ExecuteReader()
        Do While StruttureMansioni.Read
            Dim ImportoIntervento As Double

            ImportoIntervento = ImportoAccesso


            Dim Mansione As New Cls_AppaltiTabellaMansione


            Mansione.ID = IdMansione
            Mansione.Leggi(Session("DC_TABELLE"), Mansione.ID)

            Dim Operatore As New Cls_Operatore


            Operatore.CodiceMedico = campodbN(StruttureMansioni.Item("CodiceOperatore"))
            Operatore.Leggi(Session("DC_OSPITE"))


            Dim StrutturaMansione As New Cls_Appalti_StruttureMansioni


            StrutturaMansione.IdAppalto = IdAppalto
            StrutturaMansione.Struttura = IdStruttura
            StrutturaMansione.Mansione = Mansione.ID
            StrutturaMansione.Leggi(Session("DC_TABELLE"))
            If campodb(StruttureMansioni.Item("intypecod")) <> "" Then
                StrutturaMansione.IdAppalto = IdAppalto
                StrutturaMansione.Struttura = IdStruttura
                StrutturaMansione.Mansione = Mansione.ID
                StrutturaMansione.intypecod = campodb(StruttureMansioni.Item("intypecod"))
                StrutturaMansione.Leggiintypecod(Session("DC_TABELLE"))
            End If


            MovimentiContabilie.Righe(Indice) = New Cls_MovimentiContabiliRiga

            MovimentiContabilie.Righe(Indice).Numero = MovimentiContabilie.NumeroRegistrazione
            MovimentiContabilie.Righe(Indice).RigaDaCausale = 3
            MovimentiContabilie.Righe(Indice).MastroPartita = StrutturaMansione.Mastro
            MovimentiContabilie.Righe(Indice).ContoPartita = StrutturaMansione.Conto
            MovimentiContabilie.Righe(Indice).SottocontoPartita = StrutturaMansione.Sottoconto
            MovimentiContabilie.Righe(Indice).CodiceIVA = StrutturaMansione.CodiceIVA

            MovimentiContabilie.Righe(Indice).DestinatoVendita = campodb(StruttureMansioni.Item("intypedescription"))

            Dim Struttura As New Cls_AppaltiTabellaStruttura

            Struttura.ID = IdStruttura
            Struttura.Leggi(Session("DC_TABELLE"), IdStruttura)

            MovimentiContabilie.Righe(Indice).CentroServizio = Struttura.CentroServizio

            MovimentiContabilie.CentroServizio = Struttura.CentroServizio

            MovimentiContabilie.Righe(Indice).Importo = ImportoIntervento

            MovimentiContabilie.Righe(Indice).Quantita = 10000
            MovimentiContabilie.Righe(Indice).DareAvere = CausaleContabile.Righe(2).DareAvere

            MovimentiContabilie.Righe(Indice).Descrizione = Operatore.Nome & " " & Format(campodbD(StruttureMansioni.Item("Data")), "dd/MM/yyyy")


            Scrivi(campodbD(StruttureMansioni.Item("Data")), IdAppalto, IdStruttura, IdMansione, Operatore.CodiceMedico, 1, 1, ImportoAccesso, StrutturaMansione.CodiceIVA, campodb(StruttureMansioni.Item("intypecod")), campodb(StruttureMansioni.Item("intypedescription")), campodbN(StruttureMansioni.Item("CodiceOspite")), campodbN(StruttureMansioni.Item("codicecooperativa")), 0, campodbN(StruttureMansioni.Item("Done")))
            Indice = Indice + 1
        Loop
        StruttureMansioni.Close()


        cn.Close()

    End Sub



    Private Sub RigaMansioneAccessiPassivi(ByRef MovimentiContabilie As Cls_MovimentoContabile, ByVal IdAppalto As Integer, ByVal IdStruttura As Integer, ByVal IdMansione As String, ByVal ImportoAccesso As Double, ByRef Indice As Integer, ByVal Filtro As Integer)

        Dim cn As New OleDbConnection
        Dim CausaleContabile As New Cls_CausaleContabile

        Dim ConnectionString As String = Session("DC_TABELLE")

        cn = New Data.OleDb.OleDbConnection(ConnectionString)

        cn.Open()



        CausaleContabile.Leggi(Session("DC_TABELLE"), MovimentiContabilie.CausaleContabile)


        Dim Condizione As String

        If Filtro = 1 Then
            Condizione = " (done is null or Done =1 or Done =5)"
        Else
            Condizione = " (Done =2)"
        End If


        Dim cmd As New OleDbCommand

        cmd.CommandText = "Select CodiceOperatore,intypecod,intypedescription,Data,CodiceOspite,codicecooperativa From Appalti_Prestazioni  where  year(Data) = " & Txt_Anno.Text & " And Month(Data) = " & Val(Dd_Mese.SelectedValue) & " And  IdAppalto = " & IdAppalto & " And  IdStrutture = " & IdStruttura & " And IdMansione =" & IdMansione & " And " & Condizione  '& " Group by  CodiceOperatore,intypecod,intypedescription,Data,CodiceOspite,codicecooperativa"
        cmd.Connection = cn
        Dim StruttureMansioni As OleDbDataReader = cmd.ExecuteReader()
        Do While StruttureMansioni.Read
            Dim ImportoIntervento As Double

            ImportoIntervento = ImportoAccesso


            Dim Mansione As New Cls_AppaltiTabellaMansione


            Mansione.ID = IdMansione
            Mansione.Leggi(Session("DC_TABELLE"), Mansione.ID)

            Dim Operatore As New Cls_Operatore


            Operatore.CodiceMedico = campodbN(StruttureMansioni.Item("CodiceOperatore"))
            Operatore.Leggi(Session("DC_OSPITE"))


            Dim StrutturaMansione As New Cls_Appalti_StruttureMansioni


            StrutturaMansione.IdAppalto = IdAppalto
            StrutturaMansione.Struttura = IdStruttura
            StrutturaMansione.Mansione = Mansione.ID
            StrutturaMansione.Leggi(Session("DC_TABELLE"))
            If campodb(StruttureMansioni.Item("intypecod")) <> "" Then
                StrutturaMansione.IdAppalto = IdAppalto
                StrutturaMansione.Struttura = IdStruttura
                StrutturaMansione.Mansione = Mansione.ID
                StrutturaMansione.intypecod = campodb(StruttureMansioni.Item("intypecod"))
                StrutturaMansione.Leggiintypecod(Session("DC_TABELLE"))
            End If



            Scrivi(campodbD(StruttureMansioni.Item("Data")), IdAppalto, IdStruttura, IdMansione, Operatore.CodiceMedico, 1, 1, ImportoAccesso, StrutturaMansione.CodiceIVA, campodb(StruttureMansioni.Item("intypecod")), campodb(StruttureMansioni.Item("intypedescription")), campodbN(StruttureMansioni.Item("CodiceOspite")), campodbN(StruttureMansioni.Item("codicecooperativa")), 1)
        Loop
        StruttureMansioni.Close()


        cn.Close()

    End Sub

    Private Sub RigaMansioneGiorno(ByRef MovimentiContabilie As Cls_MovimentoContabile, ByVal IdAppalto As Integer, ByVal IdStruttura As Integer, ByVal IdMansione As String, ByVal ImportoGiorno As Double, ByRef Indice As Integer)

        Dim cn As New OleDbConnection
        Dim CausaleContabile As New Cls_CausaleContabile

        Dim ConnectionString As String = Session("DC_TABELLE")

        cn = New Data.OleDb.OleDbConnection(ConnectionString)

        cn.Open()



        CausaleContabile.Leggi(Session("DC_TABELLE"), MovimentiContabilie.CausaleContabile)



        Dim cmd As New OleDbCommand

        cmd.CommandText = "Select Data,CodiceOperatore,intypecod,intypedescription,codiceospite,count(*) as quantita From Appalti_Prestazioni  where  year(Data) = " & Txt_Anno.Text & " And Month(Data) = " & Val(Dd_Mese.SelectedValue) & " And  IdAppalto = " & IdAppalto & " And  IdStrutture = " & IdStruttura & " And IdMansione =" & IdMansione & " group by Data,CodiceOperatore,intypecod,intypedescription,codiceospite Order by Data,CodiceOperatore,intypecod,intypedescription,codiceospite"
        cmd.Connection = cn
        Dim StruttureMansioni As OleDbDataReader = cmd.ExecuteReader()
        Do While StruttureMansioni.Read
            Dim ImportoIntervento As Double

            ImportoIntervento = ImportoGiorno


            Dim Mansione As New Cls_AppaltiTabellaMansione


            Mansione.ID = IdMansione
            Mansione.Leggi(Session("DC_TABELLE"), Mansione.ID)

            Dim Operatore As New Cls_Operatore


            Operatore.CodiceMedico = campodbN(StruttureMansioni.Item("CodiceOperatore"))
            Operatore.Leggi(Session("DC_OSPITE"))


            Dim StrutturaMansione As New Cls_Appalti_StruttureMansioni


            StrutturaMansione.IdAppalto = IdAppalto
            StrutturaMansione.Struttura = IdStruttura
            StrutturaMansione.Mansione = Mansione.ID
            StrutturaMansione.Leggi(Session("DC_TABELLE"))
            If campodb(StruttureMansioni.Item("intypecod")) <> "" Then
                StrutturaMansione.IdAppalto = IdAppalto
                StrutturaMansione.Struttura = IdStruttura
                StrutturaMansione.Mansione = Mansione.ID
                StrutturaMansione.intypecod = campodb(StruttureMansioni.Item("intypecod"))
                StrutturaMansione.Leggiintypecod(Session("DC_TABELLE"))
            End If


            MovimentiContabilie.Righe(Indice) = New Cls_MovimentiContabiliRiga

            MovimentiContabilie.Righe(Indice).Numero = MovimentiContabilie.NumeroRegistrazione
            MovimentiContabilie.Righe(Indice).RigaDaCausale = 3
            MovimentiContabilie.Righe(Indice).MastroPartita = StrutturaMansione.Mastro
            MovimentiContabilie.Righe(Indice).ContoPartita = StrutturaMansione.Conto
            MovimentiContabilie.Righe(Indice).SottocontoPartita = StrutturaMansione.Sottoconto
            MovimentiContabilie.Righe(Indice).CodiceIVA = StrutturaMansione.CodiceIVA

            MovimentiContabilie.Righe(Indice).DestinatoVendita = campodb(StruttureMansioni.Item("intypedescription"))

            'DestinatoVendita

            Dim Struttura As New Cls_AppaltiTabellaStruttura

            Struttura.ID = IdStruttura
            Struttura.Leggi(Session("DC_TABELLE"), IdStruttura)

            MovimentiContabilie.Righe(Indice).CentroServizio = Struttura.CentroServizio

            MovimentiContabilie.CentroServizio = Struttura.CentroServizio

            MovimentiContabilie.Righe(Indice).Importo = ImportoIntervento

            MovimentiContabilie.Righe(Indice).Quantita = campodbN(StruttureMansioni.Item("quantita")) * 10000

            MovimentiContabilie.Righe(Indice).DareAvere = CausaleContabile.Righe(2).DareAvere

            MovimentiContabilie.Righe(Indice).Descrizione = Operatore.Nome & " " & Format(campodbD(StruttureMansioni.Item("Data")), "dd/MM/yyyy")


            Scrivi(campodbD(StruttureMansioni.Item("Data")), IdAppalto, IdStruttura, IdMansione, Operatore.CodiceMedico, campodbN(StruttureMansioni.Item("quantita")), 1, ImportoIntervento, StrutturaMansione.CodiceIVA, campodb(StruttureMansioni.Item("intypecod")), campodb(StruttureMansioni.Item("intypedescription")), campodbN(StruttureMansioni.Item("CodiceOspite")), campodbN(StruttureMansioni.Item("CodiceCooperativa")), 0)

            Indice = Indice + 1
        Loop
        StruttureMansioni.Close()


        cn.Close()

    End Sub


    Private Sub RigaMansioneFisso(ByRef MovimentiContabilie As Cls_MovimentoContabile, ByVal IdAppalto As Integer, ByVal IdStruttura As Integer, ByVal IdMansione As String, ByVal ImportoFisso As Double, ByRef Indice As Integer)

        Dim cn As New OleDbConnection
        Dim CausaleContabile As New Cls_CausaleContabile

        Dim ConnectionString As String = Session("DC_TABELLE")

        cn = New Data.OleDb.OleDbConnection(ConnectionString)

        cn.Open()



        CausaleContabile.Leggi(Session("DC_TABELLE"), MovimentiContabilie.CausaleContabile)



        Dim cmd As New OleDbCommand

        cmd.CommandText = "Select CodiceOperatore,intypecod,intypedescription,CodiceOspite,codicecooperativa From Appalti_Prestazioni  where  year(Data) = " & Txt_Anno.Text & " And Month(Data) = " & Val(Dd_Mese.SelectedValue) & " And  IdAppalto = " & IdAppalto & " And  IdStrutture = " & IdStruttura & " And IdMansione =" & IdMansione & " group by CodiceOperatore,intypecod,intypedescription,CodiceOspite,codicecooperativa"
        cmd.Connection = cn
        Dim StruttureMansioni As OleDbDataReader = cmd.ExecuteReader()
        Do While StruttureMansioni.Read
            Dim ImportoIntervento As Double

            ImportoIntervento = ImportoFisso


            Dim Mansione As New Cls_AppaltiTabellaMansione


            Mansione.ID = IdMansione
            Mansione.Leggi(Session("DC_TABELLE"), Mansione.ID)

            Dim Operatore As New Cls_Operatore


            Operatore.CodiceMedico = campodbN(StruttureMansioni.Item("CodiceOperatore"))
            Operatore.Leggi(Session("DC_OSPITE"))

            Dim StrutturaMansione As New Cls_Appalti_StruttureMansioni


            StrutturaMansione.IdAppalto = IdAppalto
            StrutturaMansione.Struttura = IdStruttura
            StrutturaMansione.Mansione = Mansione.ID
            StrutturaMansione.Leggi(Session("DC_TABELLE"))
            If campodb(StruttureMansioni.Item("intypecod")) <> "" Then
                StrutturaMansione.IdAppalto = IdAppalto
                StrutturaMansione.Struttura = IdStruttura
                StrutturaMansione.Mansione = Mansione.ID
                StrutturaMansione.intypecod = campodb(StruttureMansioni.Item("intypecod"))
                StrutturaMansione.Leggiintypecod(Session("DC_TABELLE"))
            End If


            MovimentiContabilie.Righe(Indice) = New Cls_MovimentiContabiliRiga

            MovimentiContabilie.Righe(Indice).Numero = MovimentiContabilie.NumeroRegistrazione
            MovimentiContabilie.Righe(Indice).RigaDaCausale = 3
            MovimentiContabilie.Righe(Indice).MastroPartita = StrutturaMansione.Mastro
            MovimentiContabilie.Righe(Indice).ContoPartita = StrutturaMansione.Conto
            MovimentiContabilie.Righe(Indice).SottocontoPartita = StrutturaMansione.Sottoconto
            MovimentiContabilie.Righe(Indice).CodiceIVA = StrutturaMansione.CodiceIVA
            MovimentiContabilie.Righe(Indice).DestinatoVendita = campodb(StruttureMansioni.Item("intypedescription"))


            Dim Struttura As New Cls_AppaltiTabellaStruttura

            Struttura.ID = IdStruttura
            Struttura.Leggi(Session("DC_TABELLE"), IdStruttura)

            MovimentiContabilie.Righe(Indice).CentroServizio = Struttura.CentroServizio


            MovimentiContabilie.CentroServizio = Struttura.CentroServizio

            MovimentiContabilie.Righe(Indice).Importo = ImportoIntervento

            MovimentiContabilie.Righe(Indice).Quantita = 10000
            MovimentiContabilie.Righe(Indice).DareAvere = CausaleContabile.Righe(2).DareAvere

            MovimentiContabilie.Righe(Indice).Descrizione = Operatore.Nome


            Scrivi(DateSerial(Txt_Anno.Text, Val(Dd_Mese.SelectedValue), 1), IdAppalto, IdStruttura, IdMansione, Operatore.CodiceMedico, 0, 1, ImportoIntervento, StrutturaMansione.CodiceIVA, campodb(StruttureMansioni.Item("intypecod")), campodb(StruttureMansioni.Item("intypedescription")), campodbN(StruttureMansioni.Item("CodiceOspite")), campodbN(StruttureMansioni.Item("codicecooperativa")), 0)
            Indice = Indice + 1
        Loop
        StruttureMansioni.Close()


        cn.Close()

    End Sub


    Private Sub RegistrazioneForfait(ByVal NumeroRegistrazione As Integer, ByVal Appalto As Cls_AppaltiAnagrafica, ByVal CausaleContabile As Cls_CausaleContabile, ByVal Movienti As Cls_MovimentoContabile, Optional ByVal ImportoForfaitStruttura As Double = 0, Optional ByVal IdStruttura As Double = 0)

        Dim cn As New OleDbConnection

        Dim ConnectionString As String = Session("DC_GENERALE")

        cn = New Data.OleDb.OleDbConnection(ConnectionString)

        cn.Open()




        Dim cmdDelete As New OleDbCommand

        cmdDelete.CommandText = "delete From temp_MovimentiContabiliRiga Where RigaDaCausale = 2 And Numero = ?"
        cmdDelete.Connection = cn
        cmdDelete.Parameters.AddWithValue("@Numero", NumeroRegistrazione)
        cmdDelete.ExecuteNonQuery()


        Dim cmdModifichi As New OleDbCommand

        cmdModifichi.CommandText = "UPDATE temp_MovimentiContabiliRiga SET QUANTITA =0,IMPORTO = 0 Where RigaDaCausale = 3 And Numero = ?"
        cmdModifichi.Connection = cn
        cmdModifichi.Parameters.AddWithValue("@Numero", NumeroRegistrazione)
        cmdModifichi.ExecuteNonQuery()



        Dim TempRigaNumero As Long
        Dim TempRigaMastroPartita As Long
        Dim TempRigaContoPartita As Long
        Dim TempRigaSottoContoPartita As Double
        Dim TempRigaImporto As Double
        Dim TempRigaTipo As String
        Dim TempRigaDareAvere As String
        Dim TempRigaSegno As String
        Dim TempRigaDaCausale As String
        Dim TempRigaUtente As String = Session("UTENTE")
        Dim TempDataAggiornamento As Date = Now
        Dim TempRigaImponibile As Double
        Dim TempRigaCodiceIva As String
        Dim M As New OleDbCommand




        TempRigaNumero = NumeroRegistrazione
        TempRigaMastroPartita = Appalto.Mastro
        TempRigaContoPartita = Appalto.Conto
        TempRigaSottoContoPartita = Appalto.Sottoconto
        If ImportoForfaitStruttura = 0 Then
            TempRigaImporto = Appalto.ImportoForfait
        Else
            TempRigaImporto = ImportoForfaitStruttura
        End If
        TempRigaTipo = ""
        TempRigaDareAvere = CausaleContabile.Righe(2).DareAvere
        TempRigaSegno = "+"
        TempRigaDaCausale = 3
        TempRigaImponibile = 0
        TempRigaCodiceIva = Appalto.CodiceIva


        M.Parameters.Clear()
        M.Connection = cn
        M.CommandText = "INSERT INTO Temp_MovimentiContabiliRiga (Numero,MastroPartita,ContoPartita,SottoContoPartita,Importo,Tipo,DareAvere,Segno,RigaDaCausale,CodiceIva,Imponibile,CENTROSERVIZIO,Utente,DataAggiornamento) values " & _
                               " (?,?,?,?,?,?,?,?,?,?,?,?,?,?)"
        M.Parameters.AddWithValue("Numero", TempRigaNumero)
        M.Parameters.AddWithValue("MastroPartita", TempRigaMastroPartita)
        M.Parameters.AddWithValue("ContoPartita", TempRigaContoPartita)
        M.Parameters.AddWithValue("SottoContoPartita", TempRigaSottoContoPartita)
        M.Parameters.AddWithValue("Importo", TempRigaImporto)
        M.Parameters.AddWithValue("Tipo", TempRigaTipo)
        M.Parameters.AddWithValue("DareAvere", TempRigaDareAvere)
        M.Parameters.AddWithValue("Segno", TempRigaSegno)
        M.Parameters.AddWithValue("RigaDaCausale", TempRigaDaCausale)
        M.Parameters.AddWithValue("CodiceIva", TempRigaCodiceIva)
        M.Parameters.AddWithValue("Imponibile", TempRigaImponibile)
        M.Parameters.AddWithValue("CENTROSERVIZIO", Movienti.CentroServizio)
        M.Parameters.AddWithValue("Utente", TempRigaUtente)
        M.Parameters.AddWithValue("DataAggiornamento", TempDataAggiornamento)
        M.ExecuteNonQuery()



        TempRigaNumero = NumeroRegistrazione
        TempRigaMastroPartita = CausaleContabile.Righe(1).Mastro
        TempRigaContoPartita = CausaleContabile.Righe(1).Conto
        TempRigaSottoContoPartita = CausaleContabile.Righe(1).Sottoconto


        Dim CodIva As New Cls_IVA

        CodIva.Codice = Appalto.CodiceIva
        CodIva.Leggi(Session("DC_TABELLE"), CodIva.Codice)

        If ImportoForfaitStruttura = 0 Then
            TempRigaImponibile = Appalto.ImportoForfait
            TempRigaImporto = Modulo.MathRound(CodIva.Aliquota * Appalto.ImportoForfait, 2)
        Else
            TempRigaImponibile = ImportoForfaitStruttura
            TempRigaImporto = Modulo.MathRound(CodIva.Aliquota * ImportoForfaitStruttura, 2)
        End If

        TempRigaTipo = "IV"
        TempRigaDareAvere = CausaleContabile.Righe(1).DareAvere
        TempRigaSegno = "+"
        TempRigaDaCausale = 2

        TempRigaCodiceIva = Appalto.CodiceIva


        M.Parameters.Clear()
        M.Connection = cn
        M.CommandText = "INSERT INTO Temp_MovimentiContabiliRiga (Numero,MastroPartita,ContoPartita,SottoContoPartita,Importo,Tipo,DareAvere,Segno,RigaDaCausale,CodiceIva,Imponibile,CENTROSERVIZIO,Utente,DataAggiornamento) values " & _
                               " (?,?,?,?,?,?,?,?,?,?,?,?,?,?)"
        M.Parameters.AddWithValue("Numero", TempRigaNumero)
        M.Parameters.AddWithValue("MastroPartita", TempRigaMastroPartita)
        M.Parameters.AddWithValue("ContoPartita", TempRigaContoPartita)
        M.Parameters.AddWithValue("SottoContoPartita", TempRigaSottoContoPartita)
        M.Parameters.AddWithValue("Importo", TempRigaImporto)
        M.Parameters.AddWithValue("Tipo", TempRigaTipo)
        M.Parameters.AddWithValue("DareAvere", TempRigaDareAvere)
        M.Parameters.AddWithValue("Segno", TempRigaSegno)
        M.Parameters.AddWithValue("RigaDaCausale", TempRigaDaCausale)
        M.Parameters.AddWithValue("CodiceIva", TempRigaCodiceIva)
        M.Parameters.AddWithValue("Imponibile", TempRigaImponibile)
        M.Parameters.AddWithValue("CENTROSERVIZIO", Movienti.CentroServizio)
        M.Parameters.AddWithValue("Utente", TempRigaUtente)
        M.Parameters.AddWithValue("DataAggiornamento", TempDataAggiornamento)
        M.ExecuteNonQuery()


        Dim cmdModifichiCli As New OleDbCommand

        cmdModifichiCli.CommandText = "UPDATE temp_MovimentiContabiliRiga SET IMPORTO = ? Where RigaDaCausale = 1 And Numero = ?"
        cmdModifichiCli.Connection = cn
        cmdModifichiCli.Parameters.AddWithValue("@IMPORTO", TempRigaImponibile + TempRigaImporto)
        cmdModifichiCli.Parameters.AddWithValue("@Numero", NumeroRegistrazione)
        cmdModifichiCli.ExecuteNonQuery()

        cn.Close()


        Scrivi(DateSerial(Txt_Anno.Text, Val(Dd_Mese.SelectedValue), 1), Appalto.Id, IdStruttura, 99999, 0, 0, 0, TempRigaImponibile, TempRigaCodiceIva, "", "", 0, 0, 0)


    End Sub
    Private Sub RaggruppaRegistrazione(ByVal NumeroRegistrazione As Integer)

        Dim cn As New OleDbConnection

        Dim ConnectionString As String = Session("DC_GENERALE")


        Dim Registrazione As New Cls_MovimentoContabile


        Registrazione.Leggi(Session("DC_GENERALE"), NumeroRegistrazione, True)


        'IdProgettoODV

        Dim Appalto As New Cls_AppaltiAnagrafica

        Appalto.Id = Registrazione.IdProgettoODV
        Appalto.Leggi(Session("DC_TABELLE"))




        cn = New Data.OleDb.OleDbConnection(ConnectionString)

        cn.Open()


        Dim cmd As New OleDbCommand

        If Appalto.RaggruppaSenzaPresaInCarico = 0 Then
            cmd.CommandText = "Select MastroPartita,ContoPartita,SottocontoPartita,Invisibile,DestinatoVendita,CodiceIVA,CentroServizio,DareAvere,Sum(Importo) AS Totrighe,Sum(Quantita) AS TotQTY From temp_MovimentiContabiliRiga Where RigaDaCausale = 3 And Numero  =? group by MastroPartita,ContoPartita,SottocontoPartita,Invisibile,DestinatoVendita,CodiceIVA,CentroServizio,DareAvere"
        Else
            cmd.CommandText = "Select MastroPartita,ContoPartita,SottocontoPartita,Invisibile,CodiceIVA,DareAvere,Sum(Importo) AS Totrighe,Sum(Quantita) AS TotQTY From temp_MovimentiContabiliRiga Where RigaDaCausale = 3 And Numero  =? group by MastroPartita,ContoPartita,SottocontoPartita,Invisibile,CodiceIVA,DareAvere"
        End If


        cmd.Connection = cn
        cmd.Parameters.AddWithValue("@Numero", NumeroRegistrazione)
        Dim LeggiRegistrazione As OleDbDataReader = cmd.ExecuteReader()

        Dim cmdDelete As New OleDbCommand

        cmdDelete.CommandText = "delete From temp_MovimentiContabiliRiga Where RigaDaCausale = 3 And Numero =?"
        cmdDelete.Connection = cn
        cmdDelete.Parameters.AddWithValue("@Numero", NumeroRegistrazione)
        cmdDelete.ExecuteNonQuery()

        Do While LeggiRegistrazione.Read

            Dim TempRigaNumero As Long
            Dim TempRigaMastroPartita As Long
            Dim TempRigaContoPartita As Long
            Dim TempRigaSottoContoPartita As Double
            Dim TempRigaImporto As Double
            Dim TempRigaTipo As String
            Dim TempRigaDareAvere As String
            Dim TempRigaSegno As String
            Dim TempRigaDaCausale As String
            Dim TempRigaUtente As String = Session("UTENTE")
            Dim TempDataAggiornamento As Date = Now
            Dim TempRigaImponibile As Double
            Dim TempRigaCodiceIva As String
            Dim TempRigaCENTROSERVIZIO As String
            Dim TempRigaDescrizione As String
            Dim TempRigaQuantita As Double
            Dim TempRigaDestinatoVendita As String

            Dim M As New OleDbCommand

            Dim TotRighe As Double = 0
            TotRighe = Modulo.MathRound(Modulo.MathRound(campodbN(LeggiRegistrazione.Item("Totrighe")) / (campodbN(LeggiRegistrazione.Item("TotQTY")) / 10000), 6) * (campodbN(LeggiRegistrazione.Item("TotQTY")) / 10000), 2)



            TempRigaNumero = NumeroRegistrazione
            TempRigaMastroPartita = campodbN(LeggiRegistrazione.Item("MastroPartita"))
            TempRigaContoPartita = campodbN(LeggiRegistrazione.Item("ContoPartita"))
            TempRigaSottoContoPartita = campodbN(LeggiRegistrazione.Item("SottocontoPartita"))
            TempRigaImporto = TotRighe
            TempRigaTipo = ""
            TempRigaDareAvere = campodb(LeggiRegistrazione.Item("DareAvere"))
            TempRigaSegno = "+"
            TempRigaDaCausale = 3
            TempRigaImponibile = 0
            TempRigaCodiceIva = campodb(LeggiRegistrazione.Item("CodiceIVA"))

            If Appalto.RaggruppaSenzaPresaInCarico = 0 Then
                TempRigaCENTROSERVIZIO = campodb(LeggiRegistrazione.Item("CentroServizio"))
            Else
                TempRigaCENTROSERVIZIO = ""
            End If
            TempRigaQuantita = campodbN(LeggiRegistrazione.Item("TotQTY")) / 100

            If Appalto.RaggruppaSenzaPresaInCarico = 0 Then
                TempRigaDestinatoVendita = campodb(LeggiRegistrazione.Item("DestinatoVendita"))
            Else
                TempRigaDestinatoVendita = ""
            End If

            Dim Mansione As New Cls_AppaltiTabellaMansione

            Mansione.ID = campodbN(LeggiRegistrazione.Item("Invisibile"))
            Mansione.Leggi(Session("DC_TABELLE"), Mansione.ID)


            If campodbN(LeggiRegistrazione.Item("Invisibile")) = -1 Then
                Mansione.Descrizione = "KM"
            End If



            TempRigaDescrizione = Mansione.Descrizione

            If TempRigaDestinatoVendita <> "" Then
                TempRigaDescrizione = TempRigaDescrizione & " " & TempRigaDestinatoVendita

            End If


            If TempRigaCodiceIva <> "" Then

                M.Parameters.Clear()
                M.Connection = cn
                M.CommandText = "INSERT INTO Temp_MovimentiContabiliRiga (Numero,MastroPartita,ContoPartita,SottoContoPartita,Importo,Tipo,DareAvere,Segno,RigaDaCausale,CodiceIva,Imponibile,CENTROSERVIZIO,Utente,DataAggiornamento,Quantita,Descrizione, DestinatoVendita) values " & _
                                       " (?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)"
                M.Parameters.AddWithValue("Numero", TempRigaNumero)
                M.Parameters.AddWithValue("MastroPartita", TempRigaMastroPartita)
                M.Parameters.AddWithValue("ContoPartita", TempRigaContoPartita)
                M.Parameters.AddWithValue("SottoContoPartita", TempRigaSottoContoPartita)
                M.Parameters.AddWithValue("Importo", TempRigaImporto)
                M.Parameters.AddWithValue("Tipo", TempRigaTipo)
                M.Parameters.AddWithValue("DareAvere", TempRigaDareAvere)
                M.Parameters.AddWithValue("Segno", TempRigaSegno)
                M.Parameters.AddWithValue("RigaDaCausale", TempRigaDaCausale)
                M.Parameters.AddWithValue("CodiceIva", TempRigaCodiceIva)
                M.Parameters.AddWithValue("Imponibile", TempRigaImponibile)

                'CENTROSERVIZIO
                M.Parameters.AddWithValue("CENTROSERVIZIO", TempRigaCENTROSERVIZIO)
                M.Parameters.AddWithValue("Utente", TempRigaUtente)
                M.Parameters.AddWithValue("DataAggiornamento", TempDataAggiornamento)
                M.Parameters.AddWithValue("Quantita", TempRigaQuantita)
                M.Parameters.AddWithValue("TempRigaDescrizione", TempRigaDescrizione)

                M.Parameters.AddWithValue("TempRigaDestinatoVendita", TempRigaDestinatoVendita)

                'TempRigaDestinatoVendita

                M.ExecuteNonQuery()
            End If

        Loop
        LeggiRegistrazione.Close()




        cn.Close()

        VerificaRegistrazioneIVA(Session("DC_GENERALE"), Session("DC_TABELLE"), NumeroRegistrazione, Session("UTENTE"), False)

    End Sub
    '       If IdAppalto <> campodbN(Prestazioni.Item("IdAppalto")) Then

    '            If IdAppalto > 0 Then
    '                Movimenti.Scrivi(Session("DC_TABELLE"), 0)
    '            End If
    'Dim Appalto As New Cls_AppaltiAnagrafica
    'Dim CausaleContabile As New Cls_CausaleContabile


    '            Appalto.Id = campodbN(Prestazioni.Item("IdAppalto"))
    '            Appalto.Leggi(Session("DC_TABELLE"))

    '            Movimenti = New Cls_MovimentoContabile

    '            Movimenti.DataRegistrazione = Txt_Data.Text
    '            Movimenti.DataDocumento = Txt_Data.Text

    '            Movimenti.CausaleContabile = Appalto.CausaleContabile

    '            CausaleContabile.Leggi(Session("DC_TABELLE"), Movimenti.CausaleContabile)

    '            Movimenti.AnnoProtocollo = Year(Movimenti.DataRegistrazione)
    '            Movimenti.RegistroIVA = CausaleContabile.RegistroIVA
    '            Movimenti.NumeroProtocollo = 0
    '            Riga = 0
    '        End If
    '        If IdAppalto = campodbN(Prestazioni.Item("IdAppalto")) Then
    'Dim Sm As New Cls_Appalti_StruttureMansioni

    '            Sm.IdAppalto = campodbN(Prestazioni.Item("IdAppalto"))
    '            Sm.Struttura = campodbN(Prestazioni.Item("IdStrutture"))
    '            Sm.Mansione = campodbN(Prestazioni.Item("IdMansione"))
    '            Sm.Leggi(Session("DC_TABELLE"))

    'Dim CalcoloImporto As Double


    '            CalcoloImporto =



    'Dim xAppalto As New Cls_AppaltiAnagrafica
    '            xAppalto.Id = campodbN(Prestazioni.Item("IdAppalto"))
    '            xAppalto.Leggi(Session("DC_TABELLE"))

    'Dim RigaCausaleContabile As New Cls_CausaleContabile
    '            RigaCausaleContabile.Leggi(Session("DC_TABELLE"), xAppalto.CausaleContabile)
    'Dim RigaRegistrazione As New Cls_MovimentiContabiliRiga


    '            RigaRegistrazione.MastroPartita = RigaCausaleContabile.Righe(2).Mastro
    '            RigaRegistrazione.ContoPartita = RigaCausaleContabile.Righe(2).Conto
    '            RigaRegistrazione.SottocontoPartita = RigaCausaleContabile.Righe(2).Sottoconto

    '            RigaRegistrazione.Importo()

    '        End If




    Protected Sub Btn_Modifica_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles Btn_Modifica.Click
        ElaboraFatture()


        Dim cn As New OleDbConnection
        Dim Min As Integer
        Dim Max As Integer

        Dim ConnectionString As String = Session("DC_GENERALE")

        cn = New Data.OleDb.OleDbConnection(ConnectionString)

        cn.Open()


        Dim cmd As New OleDbCommand

        cmd.CommandText = "Select min(NumeroRegistrazione) As MinReg, Max(NumeroRegistrazione) AS MaxReg from temp_Movimenticontabilitesta"
        cmd.Connection = cn

        Dim LeggiRegistrazione As OleDbDataReader = cmd.ExecuteReader()
        If LeggiRegistrazione.Read Then
            Min = campodbN(LeggiRegistrazione.Item("MinReg"))
            Max = campodbN(LeggiRegistrazione.Item("MaxReg"))
        End If


        Response.Redirect("StampaFattureProva.aspx?RegistrazioneDal=" & Min & "&RegistrazioneAl=" & Max)
    End Sub



    Public Sub VerificaRegistrazioneIVA(ByVal ConnessioneGenerale As String, ByVal ConnessioneTabelle As String, ByVal NumeroRegistrazione As Long, ByVal Utente As String, Optional ByVal ArchivioEffettivo As Boolean = False)

        Dim Vt_CodiceIva(100) As String
        Dim Vt_ImportoIva(100) As Double
        Dim Vt_ImponibileIva(100) As Double
        Dim Vt_MastroIva(100) As Long
        Dim Vt_ContoIva(100) As Long
        Dim Vt_SottoContoIva(100) As Long
        Dim Vt_DareAvere(100) As String
        Dim Imponibile As Double
        Dim Trovato As Boolean
        Dim MaxIT As Long
        Dim TotImporto As Double
        Dim TotImporto2 As Double

        Dim ConnectionString As String = ConnessioneGenerale
        Dim cn As New OleDbConnection
        cn = New Data.OleDb.OleDbConnection(ConnectionString)

        cn.Open()

        Dim cmd As New OleDbCommand

        cmd.Connection = cn


        Imponibile = 0
        If ArchivioEffettivo = True Then

            cmd.CommandText = "Select * From MovimentiContabiliRiga Where Numero = " & NumeroRegistrazione & " And (Tipo IS NULL  OR TIPO = '') And DareAvere = 'D' Order by CodiceIVA"
        Else
            cmd.CommandText = "Select * From Temp_MovimentiContabiliRiga Where Numero = " & NumeroRegistrazione & " And (Tipo IS NULL  OR TIPO = '') And DareAvere = 'D' Order by CodiceIVA"
        End If


        Dim MyRs As OleDbDataReader = cmd.ExecuteReader()
        Do While MyRs.Read
            Trovato = False
            For I = 0 To MaxIT
                If Vt_CodiceIva(I) = campodb(MyRs.Item("CodiceIva")) Then
                    Vt_ImportoIva(I) = 0
                    Vt_ImponibileIva(I) = Modulo.MathRound(Vt_ImponibileIva(I) + campodbN(MyRs.Item("Importo")), 2)
                    Vt_MastroIva(I) = campodbN(MyRs.Item("MastroPartita"))
                    Vt_ContoIva(I) = campodbN(MyRs.Item("ContoPartita"))
                    Vt_SottoContoIva(I) = campodbN(MyRs.Item("SottoContoPartita"))
                    Vt_DareAvere(I) = "D"
                    Trovato = True
                    Exit For
                End If
            Next
            If Trovato = False Then
                MaxIT = MaxIT + 1
                Vt_CodiceIva(MaxIT) = campodb(MyRs.Item("CodiceIva"))
                Vt_ImportoIva(MaxIT) = Modulo.MathRound(campodbN(MyRs.Item("Importo")), 2)
                Vt_ImponibileIva(MaxIT) = Modulo.MathRound(Vt_ImponibileIva(MaxIT) + campodbN(MyRs.Item("Importo")), 2)
                Vt_MastroIva(MaxIT) = campodbN(MyRs.Item("MastroPartita"))
                Vt_ContoIva(MaxIT) = campodbN(MyRs.Item("ContoPartita"))
                Vt_SottoContoIva(MaxIT) = campodbN(MyRs.Item("SottoContoPartita"))
                Vt_DareAvere(MaxIT) = "D"
            End If

        Loop
        MyRs.Close()


        If ArchivioEffettivo = True Then
            Dim cmdDelete As New OleDbCommand

            cmdDelete.Connection = cn

            cmdDelete.CommandText = "Delete From MovimentiContabiliRiga Where Numero = " & NumeroRegistrazione & " And Tipo = 'IV' And DareAvere = 'D' "
            cmdDelete.ExecuteNonQuery()
        Else

            Dim cmdDelete As New OleDbCommand

            cmdDelete.Connection = cn

            cmdDelete.CommandText = "Delete From Temp_MovimentiContabiliRiga Where Numero = " & NumeroRegistrazione & " And Tipo = 'IV' And DareAvere = 'D' "
            cmdDelete.ExecuteNonQuery()
        End If
        Imponibile = 0

        Dim cmdAvere As New OleDbCommand

        cmdAvere.Connection = cn



        If ArchivioEffettivo = True Then
            cmdAvere.CommandText = "Select * From MovimentiContabiliRiga Where Numero = " & NumeroRegistrazione & " And (Tipo IS NULL  OR TIPO = '') And DareAvere = 'A' Order by CodiceIVA"
        Else
            cmdAvere.CommandText = "Select * From Temp_MovimentiContabiliRiga Where Numero = " & NumeroRegistrazione & " And (Tipo IS NULL OR TIPO = '') And DareAvere = 'A' Order by CodiceIVA"
        End If

        Dim RsAvere As OleDbDataReader = cmdAvere.ExecuteReader()
        Do While RsAvere.Read
            Trovato = False
            For I = 0 To MaxIT
                If Vt_CodiceIva(I) = campodb(RsAvere.Item("CodiceIva")) Then
                    Vt_ImportoIva(I) = 0
                    Vt_ImponibileIva(I) = Modulo.MathRound(Vt_ImponibileIva(I) - campodbN(RsAvere.Item("Importo")), 2)
                    Vt_MastroIva(I) = campodbN(RsAvere.Item("MastroPartita"))
                    Vt_ContoIva(I) = campodbN(RsAvere.Item("ContoPartita"))
                    Vt_SottoContoIva(I) = campodbN(RsAvere.Item("SottoContoPartita"))
                    Vt_DareAvere(I) = "A"
                    Trovato = True
                    Exit For
                End If
            Next
            If Trovato = False Then
                MaxIT = MaxIT + 1
                Vt_CodiceIva(MaxIT) = campodb(RsAvere.Item("CodiceIva"))
                Vt_ImportoIva(MaxIT) = Modulo.MathRound(campodbN(RsAvere.Item("Importo")) * -1, 2)
                Vt_ImponibileIva(MaxIT) = Modulo.MathRound(campodbN(RsAvere.Item("Importo")) * -1, 2)
                Vt_MastroIva(MaxIT) = campodbN(RsAvere.Item("MastroPartita"))
                Vt_ContoIva(MaxIT) = campodbN(RsAvere.Item("ContoPartita"))
                Vt_SottoContoIva(MaxIT) = campodbN(RsAvere.Item("SottoContoPartita"))
                Vt_DareAvere(MaxIT) = "A"
            End If

        Loop
        RsAvere.Close()


        If ArchivioEffettivo = True Then
            Dim cmdDelete As New OleDbCommand

            cmdDelete.Connection = cn

            cmdDelete.CommandText = "Delete From MovimentiContabiliRiga Where Numero = " & NumeroRegistrazione & " And Tipo = 'IV' And DareAvere = 'A' "
            cmdDelete.ExecuteNonQuery()
        Else
            Dim cmdDelete As New OleDbCommand

            cmdDelete.Connection = cn

            cmdDelete.CommandText = "Delete From Temp_MovimentiContabiliRiga Where Numero = " & NumeroRegistrazione & " And Tipo = 'IV' And DareAvere = 'A' "
            cmdDelete.ExecuteNonQuery()
        End If

        Dim Registrazione As New Cls_MovimentoContabile

        Registrazione.NumeroRegistrazione = NumeroRegistrazione
        Registrazione.Leggi(ConnessioneGenerale, Registrazione.NumeroRegistrazione, True)

        Dim CausaleContabile As New Cls_CausaleContabile

        CausaleContabile.Leggi(ConnessioneTabelle, Registrazione.CausaleContabile)



        For I = 0 To MaxIT
            If Not IsNothing(Vt_CodiceIva(I)) Then
                Dim TempRigaNumero As Long
                Dim TempRigaMastroPartita As Long
                Dim TempRigaContoPartita As Long
                Dim TempRigaSottoContoPartita As Double
                Dim TempRigaImporto As Double
                Dim TempRigaTipo As String
                Dim TempRigaDareAvere As String
                Dim TempRigaSegno As String
                Dim TempRigaDaCausale As String
                Dim TempRigaUtente As String
                Dim TempRigaDataAggiornamento As Date

                Dim TempRigaCodiceIva As String
                Dim TempRigaImponibile As Double

                Dim TempDataAggiornamento As Date

                TempRigaNumero = NumeroRegistrazione
                TempRigaTipo = "IV"
                If Vt_ImponibileIva(I) > 0 Then
                    TempRigaDareAvere = "D"
                Else
                    TempRigaDareAvere = "A"
                End If

                Try
                    Dim Iva As New Cls_IVA

                    Iva.Codice = Vt_CodiceIva(I)
                    Iva.Leggi(ConnessioneTabelle, Iva.Codice)
                    TempRigaImporto = Math.Abs(Modulo.MathRound(Vt_ImponibileIva(I) * Iva.Aliquota, 2))
                Catch ex As Exception

                End Try

                Try
                    TempRigaMastroPartita = CausaleContabile.Righe(1).Mastro
                    TempRigaContoPartita = CausaleContabile.Righe(1).Conto
                    TempRigaSottoContoPartita = CausaleContabile.Righe(1).Sottoconto
                Catch ex As Exception

                End Try

                TempRigaCodiceIva = Vt_CodiceIva(I)
                TempRigaImponibile = Math.Abs(Modulo.MathRound(Vt_ImponibileIva(I), 2))
                TempRigaDaCausale = 2
                TempRigaSegno = "+"

                TempRigaUtente = Utente
                TempDataAggiornamento = Now

                Dim cmdIva As New OleDbCommand

                cmdIva.Connection = cn
                cmdIva.Connection = cn
                cmdIva.CommandText = "INSERT INTO Temp_MovimentiContabiliRiga (Numero,MastroPartita,ContoPartita,SottoContoPartita,Importo,Tipo,DareAvere,Segno,RigaDaCausale,CodiceIva,Imponibile,Utente,DataAggiornamento) values " & _
                                       " (?,?,?,?,?,?,?,?,?,?,?,?,?)"
                cmdIva.Parameters.AddWithValue("Numero", TempRigaNumero)
                cmdIva.Parameters.AddWithValue("MastroPartita", TempRigaMastroPartita)
                cmdIva.Parameters.AddWithValue("ContoPartita", TempRigaContoPartita)
                cmdIva.Parameters.AddWithValue("SottoContoPartita", TempRigaSottoContoPartita)
                cmdIva.Parameters.AddWithValue("Importo", TempRigaImporto)
                cmdIva.Parameters.AddWithValue("Tipo", TempRigaTipo)
                cmdIva.Parameters.AddWithValue("DareAvere", TempRigaDareAvere)
                cmdIva.Parameters.AddWithValue("Segno", TempRigaSegno)
                cmdIva.Parameters.AddWithValue("RigaDaCausale", TempRigaDaCausale)
                cmdIva.Parameters.AddWithValue("CodiceIva", TempRigaCodiceIva)
                cmdIva.Parameters.AddWithValue("Imponibile", TempRigaImponibile)
                cmdIva.Parameters.AddWithValue("Utente", Utente)
                cmdIva.Parameters.AddWithValue("DataAggiornamento", TempDataAggiornamento)
                cmdIva.ExecuteNonQuery()

            End If
        Next




        Dim cmdTotDare As New OleDbCommand

        cmdTotDare.Connection = cn
        If ArchivioEffettivo = True Then
            cmdTotDare.CommandText = "Select sum(Importo) AS TotaleDare From MovimentiContabiliRiga Where Numero = " & NumeroRegistrazione & " And RigaDaCausale <> 1 And DareAvere = 'D' "
        Else
            cmdTotDare.CommandText = "Select sum(Importo) AS TotaleDare From Temp_MovimentiContabiliRiga Where Numero = " & NumeroRegistrazione & " And RigaDaCausale <> 1 And DareAvere = 'D' "
        End If
        Dim RsTotDare As OleDbDataReader = cmdTotDare.ExecuteReader()
        If RsTotDare.Read Then

            TotImporto = Modulo.MathRound(campodbN(RsTotDare.Item("TotaleDare")), 2)
        End If
        RsTotDare.Close()


        Dim cmdTotAvere As New OleDbCommand

        cmdTotAvere.Connection = cn

        If ArchivioEffettivo = True Then
            cmdTotAvere.CommandText = "Select sum(Importo) AS TotaleAvere From MovimentiContabiliRiga Where Numero = " & NumeroRegistrazione & " And RigaDaCausale <> 1 And DareAvere = 'A' "
        Else
            cmdTotAvere.CommandText = "Select sum(Importo) AS TotaleAvere From Temp_MovimentiContabiliRiga Where Numero = " & NumeroRegistrazione & " And RigaDaCausale <> 1 And DareAvere = 'A' "
        End If
        Dim RsTotAvere As OleDbDataReader = cmdTotAvere.ExecuteReader()
        If RsTotAvere.Read Then

            TotImporto2 = Modulo.MathRound(campodbN(RsTotAvere.Item("TotaleAvere")), 2)
        End If
        RsTotAvere.Close()


        Dim cmdCliente As New OleDbCommand

        cmdCliente.Connection = cn

        If ArchivioEffettivo = True Then
            cmdCliente.CommandText = "Select *  From MovimentiContabiliRiga Where Numero = " & NumeroRegistrazione & " And RigaDaCausale = 1 "
        Else
            cmdCliente.CommandText = "Select *  From Temp_MovimentiContabiliRiga Where Numero = " & NumeroRegistrazione & " And RigaDaCausale = 1 "
        End If
        Dim RsCliente As OleDbDataReader = cmdCliente.ExecuteReader()
        If RsCliente.Read Then
            If campodb(RsCliente.Item("DareAvere")) = "D" Then
                Dim cmdModCliente As New OleDbCommand

                cmdModCliente.Connection = cn
                If ArchivioEffettivo = True Then
                    cmdModCliente.CommandText = "UPDATE MovimentiContabiliRiga SET Importo = ? Where Numero = " & NumeroRegistrazione & " And RigaDaCausale = 1 "
                Else
                    cmdModCliente.CommandText = "UPDATE Temp_MovimentiContabiliRiga SET Importo = ? Where Numero = " & NumeroRegistrazione & " And RigaDaCausale = 1 "
                End If
                cmdModCliente.Parameters.AddWithValue("@Importo", Modulo.MathRound(TotImporto2 - TotImporto, 2))
                cmdModCliente.ExecuteNonQuery()


            Else
                Dim cmdModCliente As New OleDbCommand

                cmdModCliente.Connection = cn
                If ArchivioEffettivo = True Then
                    cmdModCliente.CommandText = "UPDATE MovimentiContabiliRiga SET Importo = ? Where Numero = " & NumeroRegistrazione & " And RigaDaCausale = 1 "
                Else
                    cmdModCliente.CommandText = "UPDATE Temp_MovimentiContabiliRiga SET Importo = ? Where Numero = " & NumeroRegistrazione & " And RigaDaCausale = 1 "
                End If
                cmdModCliente.Parameters.AddWithValue("@Importo", Modulo.MathRound(TotImporto - TotImporto2, 2))
                cmdModCliente.ExecuteNonQuery()

            End If

        End If

        RsCliente.Close()
    End Sub



    Private Sub RigheExtra(ByRef MovimentiContabilie As Cls_MovimentoContabile, ByVal IdAppalto As Integer, ByVal IdStruttura As Integer, ByRef Indice As Integer)

        Dim cn As New OleDbConnection
        Dim CausaleContabile As New Cls_CausaleContabile

        Dim ConnectionString As String = Session("DC_TABELLE")

        cn = New Data.OleDb.OleDbConnection(ConnectionString)

        cn.Open()



        CausaleContabile.Leggi(Session("DC_TABELLE"), MovimentiContabilie.CausaleContabile)



        Dim cmd As New OleDbCommand

        cmd.CommandText = "Select * From Appalti_AppaltiRigheAggiuntive  where  IdAppalto = " & IdAppalto & " And (Struttura IS null OR Struttura = 0 OR Struttura = ?) Order  by ID"
        cmd.Connection = cn
        cmd.Parameters.AddWithValue("@Struttura", IdStruttura)

        Dim RigheExtraInFattura As OleDbDataReader = cmd.ExecuteReader()

        Do While RigheExtraInFattura.Read

            Dim Regola As String
            Dim Importo As Double
            Dim Quantita As Integer = 100
            Regola = campodb(RigheExtraInFattura.Item("Regola"))
            Importo = campodbN(RigheExtraInFattura.Item("Importo"))

            Quantita = campodbN(RigheExtraInFattura.Item("Quantita")) * 100
            If Regola = "O" Then
                Dim I As Integer
                Dim OreTotali As Integer = 0

                For I = 0 To Indice
                    If Not IsNothing(MovimentiContabilie.Righe(I)) Then

                        If MovimentiContabilie.Righe(I).Quantita > 0 And MovimentiContabilie.Righe(I).CentroServizio <> "" Then
                            OreTotali = OreTotali + (MovimentiContabilie.Righe(I).Quantita / 10000)
                        End If
                    End If
                Next

                Quantita = OreTotali
                Importo = Math.Round(Importo * OreTotali, 2)
                Quantita = Math.Round(Quantita * 100, 2)
            End If




            MovimentiContabilie.Righe(Indice) = New Cls_MovimentiContabiliRiga

            MovimentiContabilie.Righe(Indice).Numero = MovimentiContabilie.NumeroRegistrazione
            MovimentiContabilie.Righe(Indice).RigaDaCausale = 3
            MovimentiContabilie.Righe(Indice).MastroPartita = campodbN(RigheExtraInFattura.Item("Mastro"))
            MovimentiContabilie.Righe(Indice).ContoPartita = campodbN(RigheExtraInFattura.Item("Conto"))
            MovimentiContabilie.Righe(Indice).SottocontoPartita = campodbN(RigheExtraInFattura.Item("Sottoconto"))
            MovimentiContabilie.Righe(Indice).CodiceIVA = campodb(RigheExtraInFattura.Item("IVA"))
            MovimentiContabilie.Righe(Indice).DestinatoVendita = ""



            MovimentiContabilie.Righe(Indice).CentroServizio = ""

            
            MovimentiContabilie.Righe(Indice).Importo = Importo

            MovimentiContabilie.Righe(Indice).Quantita = Quantita
            MovimentiContabilie.Righe(Indice).DareAvere = CausaleContabile.Righe(2).DareAvere

            MovimentiContabilie.Righe(Indice).Descrizione = campodb(RigheExtraInFattura.Item("Descrizione"))
            MovimentiContabilie.Righe(Indice).DestinatoVendita = campodb(RigheExtraInFattura.Item("Descrizione"))
            MovimentiContabilie.Righe(Indice).Invisibile = 0


            Indice = Indice + 1
        Loop
        RigheExtraInFattura.Close()


        cn.Close()

    End Sub

End Class
