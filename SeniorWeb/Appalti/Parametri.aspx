﻿<%@ Page Language="VB" AutoEventWireup="false" Inherits="Appalti_Parametri" CodeFile="Parametri.aspx.vb" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="xasp" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
    <meta http-equiv="x-ua-compatible" content="IE=9" />
    <title>Appalti - Parametri</title>
    <asp:PlaceHolder runat="server">
        <%: System.Web.Optimization.Styles.Render("~/Content/AjaxControlToolkit/Styles/Bundle") %>
    </asp:PlaceHolder>
    <link href="ospiti.css?ver=11" rel="stylesheet" type="text/css" />
    <link rel="shortcut icon" href="images/SENIOR.ico" />
    <link href="js/jquery.autocomplete.css" rel="stylesheet" type="text/css" />

    <script src="js/jquery-1.5.1.min.js" type="text/javascript"></script>
    <script src="js/jquery.autocomplete.js" type="text/javascript"></script>
    <script src="js/soapclient.js" type="text/javascript"></script>
    <script src="/js/convertinumero.js" type="text/javascript"></script>



    <script src="js/jquery.maskedinput-1.3.min.js" type="text/javascript"></script>
    <script src="/js/formatnumer.js" type="text/javascript"></script>
    <script src="/js/pianoconti.js" type="text/javascript"></script>
    <script src="js/JSErrore.js" type="text/javascript"></script>
    <script type="text/javascript">         
        $(document).ready(function () {
            $('html').keyup(function (event) {

                if (event.keyCode == 113) {
                    __doPostBack("Imb_Modifica", "0");
                }


            });
        });

        $(document).ready(function () {
            if (window.innerHeight > 0) { $("#BarraLaterale").css("height", (window.innerHeight - 94) + "px"); } else { $("#BarraLaterale").css("height", (document.documentElement.offsetHeight - 94) + "px"); }
        });
    </script>
</head>
<body>
    <form id="form1" runat="server">
        <asp:ScriptManager ID="ScriptManager1" runat="server" EnableScriptGlobalization="true">
            <Scripts>
                <asp:ScriptReference Path="~/Scripts/AjaxControlToolkit/Bundle" />
            </Scripts>
        </asp:ScriptManager>
        <asp:Label ID="Lbl_BarraSenior" runat="server" Text=""></asp:Label>
        <div style="text-align: left;">
            <table style="width: 100%;" cellpadding="0" cellspacing="0">
                <tr>
                    <td style="width: 160px; background-color: #F0F0F0;"></td>
                    <td>
                        <div class="Titolo">Appalti - Parametri</div>
                        <div class="SottoTitolo">
                            <br />
                            <br />
                        </div>
                    </td>
                    <td style="text-align: right;">
                        <div class="DivTasti">
                            <asp:ImageButton runat="server" ImageUrl="~/images/salva.jpg" class="EffettoBottoniTondi" Height="38px" ToolTip="Modifica / Inserisci" ID="Imb_Modifica"></asp:ImageButton>
                        </div>
                    </td>
                </tr>

                <tr>
                    <td style="width: 160px; background-color: #F0F0F0; vertical-align: top; text-align: center;" id="BarraLaterale">
                        <a href="Menu_Appalti.aspx" style="border-width: 0px;">
                            <img src="images/Home.jpg" class="Effetto" alt="Menù" /></a><br />
                        <asp:ImageButton ID="Btn_Esci" runat="server" BackColor="Transparent" ImageUrl="../images/Menu_Indietro.png" class="Effetto" ToolTip="Chiudi" />
                        <br />

                        <br />
                        <br />
                        <br />
                        <br />
                        <br />
                        <br />
                        <br />
                        <br />
                    </td>
                    <td colspan="2" style="background-color: #FFFFFF;" valign="top">
                        <xasp:TabContainer ID="TabContainer1" runat="server" ActiveTabIndex="0" CssClass="TabSenior" Width="100%" BorderStyle="None" Style="margin-right: 39px">
                            <xasp:TabPanel runat="server" HeaderText="Prima Nota" ID="Tab_Anagrafica">
                                <HeaderTemplate>
                                    Parametri
                                </HeaderTemplate>
                                <ContentTemplate>

                                    <br />
                                    <label class="LabelCampo">Periodo Fatturazione : Anno : </label>
                                    <asp:TextBox ID="Txt_Anno" onkeypress="return soloNumeri(event);" MaxLength="4" runat="server"></asp:TextBox>
                                    Mese : 
       <asp:DropDownList ID="Dd_Mese" runat="server" Width="128px">
           <asp:ListItem Value="1">Gennaio</asp:ListItem>
           <asp:ListItem Value="2">Febbraio</asp:ListItem>
           <asp:ListItem Value="3">Marzo</asp:ListItem>
           <asp:ListItem Value="4">Aprile</asp:ListItem>
           <asp:ListItem Value="5">Maggio</asp:ListItem>
           <asp:ListItem Value="6">Giugno</asp:ListItem>
           <asp:ListItem Value="7">Luglio</asp:ListItem>
           <asp:ListItem Value="8">Agosto</asp:ListItem>
           <asp:ListItem Value="9">Settembre</asp:ListItem>
           <asp:ListItem Value="10">Ottobre</asp:ListItem>
           <asp:ListItem Value="11">Novembre</asp:ListItem>
           <asp:ListItem Value="12">Dicembre</asp:ListItem>
       </asp:DropDownList><br />
                                    <br />
                                    <label class="LabelCampo">Mastro :</label>
                                    <asp:TextBox ID="Txt_Mastro" onkeypress="return soloNumeri(event);" MaxLength="8" runat="server" Width="97px"></asp:TextBox><br />
                                    <br />
                                    <label class="LabelCampo">Conto  :</label>
                                    <asp:TextBox ID="Txt_ContoRegione" onkeypress="return soloNumeri(event);" MaxLength="8" runat="server"></asp:TextBox><br />
                                    <br />

                                    <label class="LabelCampo">Operatore CUS  :</label>
                                    <asp:TextBox ID="Txt_CodiceOperatore" onkeypress="return soloNumeri(event);" MaxLength="8" runat="server"></asp:TextBox><br />
                                    <br />

                                    <label class="LabelCampo">Mansione KM  :</label>
                                    <asp:TextBox ID="Txt_MansioneKM" onkeypress="return soloNumeri(event);" MaxLength="8" runat="server"></asp:TextBox><br />
                                    <br />

                                    <label class="LabelCampo">Mansione Accessi  :</label>
                                    <asp:TextBox ID="Txt_MansioneAccesssi" onkeypress="return soloNumeri(event);" MaxLength="8" runat="server"></asp:TextBox><br />
                                    <br />


                                </ContentTemplate>
                            </xasp:TabPanel>
                        </xasp:TabContainer>
                    </td>
                </tr>
            </table>
        </div>
    </form>
</body>
</html>
