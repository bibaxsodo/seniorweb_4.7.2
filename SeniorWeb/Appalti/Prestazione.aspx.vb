﻿Imports Microsoft.VisualBasic
Imports System.Data.OleDb
Imports System.Web.Hosting
Imports System.Data.SqlTypes
Partial Class Appalti_Prestazione
    Inherits System.Web.UI.Page

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Trim(Session("UTENTE")) = "" Then
            Response.Redirect("../Login.aspx")
            Exit Sub
        End If

        EseguiJS()

        If Page.IsPostBack = True Then
            Exit Sub
        End If

        Dim K1 As New Cls_SqlString

        Dim Barra As New Cls_BarraSenior

        If Not IsNothing(Session("RicercaAnagraficaSQLString")) Then
            Try
                K1 = Session("RicercaAnagraficaSQLString")
            Catch ex As Exception
                K1 = Nothing
            End Try
        End If

        Lbl_BarraSenior.Text = Barra.CodiceBarra(Application("SENIOR"), Session("UTENTE"), Page, K1)

        Dim AnagApp As New Cls_AppaltiAnagrafica

        AnagApp.UpDateDropBox(Session("DC_TABELLE"), DD_Appalto)

        Dim Struttura As New Cls_AppaltiTabellaStruttura

        Struttura.UpDateDropBox(Session("DC_TABELLE"), DD_Struttura)

        Dim Mansione As New Cls_AppaltiTabellaMansione

        Mansione.UpDateDropBox(Session("DC_TABELLE"), DD_Mansione)

        Dim Opreatore As New Cls_Operatore

        Opreatore.UpDateDropBox(Session("DC_OSPITE"), DD_Operatore)


        Dim Ospite As New ClsOspite

        Ospite.UpDateDropBox(Session("DC_OSPITE"), DD_Ospite)


        If Val(Request.Item("id")) > 0 Then
            Dim Prestazione As New Cls_Appalti_Prestazioni

            Prestazione.ID = Val(Request.Item("id"))
            Prestazione.Leggi(Session("DC_TABELLE"))

            Txt_Id.Text = Prestazione.ID
            Txt_Ore.Text = Format(Prestazione.Ore, "#,##0.00")
            Txt_DataInizio.Text = Format(Prestazione.Data, "dd/MM/yyyy")

            DD_Appalto.SelectedValue = Prestazione.IdAppalto
            DD_Mansione.SelectedValue = Prestazione.IdMansione
            DD_Struttura.SelectedValue = Prestazione.IdStrutture
            DD_Operatore.SelectedValue = Prestazione.CodiceOperatore


            DD_Ospite.SelectedValue = Prestazione.CodiceOspite
        End If

    End Sub


    Private Sub EseguiJS()
        Dim MyJs As String
        MyJs = "$(document).ready(function() {"

        MyJs = MyJs & "var els = document.getElementsByTagName(""*"");"

        MyJs = MyJs & "for (var i=0;i<els.length;i++)"
        MyJs = MyJs & "if ( els[i].id ) { "
        MyJs = MyJs & " var appoggio =els[i].id; "
        MyJs = MyJs & " if (appoggio.match('Txt_Data')!= null) {  "
        MyJs = MyJs & " $(els[i]).mask(""99/99/9999"");"
        MyJs = MyJs & " $(els[i]).datepicker({ changeMonth: true,changeYear: true,  yearRange: ""1890:" & Year(Now) & """},$.datepicker.regional[""it""]);"
        MyJs = MyJs & "    }"
        MyJs = MyJs & " if ((appoggio.match('Txt_Importo')!= null) || (appoggio.match('Txt_Percentuale')!= null) ) {  "
        MyJs = MyJs & " $(els[i]).keypress(function() { ForceNumericInput($(this).val(), true, true); } ); "
        MyJs = MyJs & " $(els[i]).blur(function() { var ap = formatNumber($(this).val(),2); $(this).val(ap); });"
        MyJs = MyJs & "    }"

        MyJs = MyJs & " if (appoggio.match('Txt_Sottoconto')!= null) {   "
        MyJs = MyJs & " $(els[i]).autocomplete('/WebHandler/GestioneConto.ashx?Utente=" & Session("UTENTE") & "', {delay:5,minChars:3});"
        MyJs = MyJs & "    }"

        MyJs = MyJs & " if (appoggio.match('Txt_ComRes')!= null)  {  "
        MyJs = MyJs & " $(els[i]).autocomplete('/WebHandler/AutocompleteComune.ashx?Utente=" & Session("UTENTE") & "', {delay:5,minChars:3});"
        MyJs = MyJs & "    }"

        MyJs = MyJs & "} "


        MyJs = MyJs & "$("".chosen-select"").chosen({"
        MyJs = MyJs & "no_results_text: ""Comune non trovato"","
        MyJs = MyJs & "display_disabled_options: true,"
        MyJs = MyJs & "allow_single_deselect: true,"
        MyJs = MyJs & "width: ""400px"","
        MyJs = MyJs & "placeholder_text_single: ""Seleziona Committente"""
        MyJs = MyJs & "});"

        MyJs = MyJs & "});"
        'MyJs = "$(document).ready(function() { $('.myClass').keypress(function() { handleEnter($('.myClass').val(), true, true); } );     $('#" & Txt_ImportoPagato.ClientID & "').blur(function() { var ap = formatNumber ($('#" & Txt_ImportoPagato.ClientID & "').val(),2); $('#" & Txt_ImportoPagato.ClientID & "').val(ap); }); });"
        ScriptManager.RegisterStartupScript(Me, Me.GetType(), "visualizzaRitIm", MyJs, True)
    End Sub

    Protected Sub Btn_Modifica_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles Btn_Modifica.Click
        Dim Prestazione As New Cls_Appalti_Prestazioni

        Prestazione.ID = Val(Request.Item("id"))


        Prestazione.Ore = Txt_Ore.Text
        Prestazione.Data = Txt_DataInizio.Text

        Prestazione.IdAppalto = DD_Appalto.SelectedValue
        Prestazione.IdMansione = DD_Mansione.SelectedValue
        Prestazione.IdStrutture = DD_Struttura.SelectedValue
        Prestazione.CodiceOperatore = DD_Operatore.SelectedValue

        Prestazione.CodiceOspite = DD_Ospite.SelectedValue

        Prestazione.Scrivi(Session("DC_TABELLE"))

        Response.Redirect("Elenco_Prestazioni.aspx")
    End Sub

    Protected Sub Btn_Esci_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles Btn_Esci.Click
        Response.Redirect("Elenco_Prestazioni.aspx")
    End Sub
End Class
