﻿<%@ Page Language="VB" AutoEventWireup="false" Inherits="Appalti_CellaGriglia" CodeFile="CellaGriglia.aspx.vb" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <asp:PlaceHolder runat="server">
        <%: System.Web.Optimization.Styles.Render("~/Content/AjaxControlToolkit/Styles/Bundle") %>
    </asp:PlaceHolder>
    <link href="ospiti.css?ver=11" rel="stylesheet" type="text/css" />
    <link rel="shortcut icon" href="images/SENIOR.ico" />
    <link href="js/jquery.autocomplete.css" rel="stylesheet" type="text/css" />


    <script src="js/jquery-1.5.1.min.js" type="text/javascript"></script>
    <script src="js/jquery.autocomplete.js" type="text/javascript"></script>
    <script src="js/soapclient.js" type="text/javascript"></script>
    <script src="/js/convertinumero.js" type="text/javascript"></script>


    <script src="/js/listacomuni.js" type="text/javascript"></script>
    <script src="js/jquery.maskedinput-1.3.min.js" type="text/javascript"></script>
    <script src="/js/formatnumer.js" type="text/javascript"></script>
    <script src="js/JSErrore.js" type="text/javascript"></script>
    <link rel="stylesheet" href="jqueryui/jquery-ui.css" type="text/css">
    <script src="jqueryui/jquery-ui.js" type="text/javascript"></script>
    <script src="jqueryui/datapicker-it.js" type="text/javascript"></script>
    <title></title>
</head>
<body>
    <form id="form1" runat="server">
        <div>
            <div style="right: 40px; width: 100%; position: static; vertical-align: top;">
                <asp:ImageButton runat="server" ImageUrl="~/images/salva.jpg" CssClass="EffettoBottoniTondi" Height="38px" ToolTip="Modifica / Inserisci" ID="Btn_Modifica"></asp:ImageButton>

                <asp:Label ID="lblDatiCella" runat="server" Text="" Style="vertical-align: top;"></asp:Label>
            </div>
            <asp:GridView ID="Grd_Cella" runat="server" CellPadding="4" Height="60px"
                ShowFooter="True" BackColor="White" BorderColor="#6FA7D1"
                BorderStyle="Dotted" BorderWidth="1px">
                <RowStyle ForeColor="#333333" BackColor="White" />
                <Columns>
                    <asp:TemplateField>
                        <ItemTemplate>
                            <asp:ImageButton ID="IB_Delete" CommandName="Delete" runat="Server" ImageUrl="~/images/cancella.png" />
                        </ItemTemplate>
                        <FooterTemplate>
                            <div style="text-align: right">
                                <asp:ImageButton ID="ImageButton1" ImageUrl="~/images/inserisci.png" CommandName="Inserisci" runat="server" />
                            </div>
                        </FooterTemplate>
                    </asp:TemplateField>



                    <asp:TemplateField HeaderText="Mansione">
                        <ItemTemplate>
                            <asp:DropDownList ID="DD_Mansione" runat="server" CssClass="chosen-select"></asp:DropDownList>
                        </ItemTemplate>
                        <HeaderStyle Font-Bold="False" Font-Italic="False" />
                        <ItemStyle Width="350px" />
                    </asp:TemplateField>


                    <asp:TemplateField HeaderText="Utente">
                        <ItemTemplate>
                            <asp:DropDownList ID="DD_Utente" runat="server" CssClass="chosen-select" AutoPostBack="true" OnSelectedIndexChanged="DD_Utentee_SelectedIndexChanged"></asp:DropDownList>
                        </ItemTemplate>
                        <HeaderStyle Font-Bold="False" Font-Italic="False" />
                        <ItemStyle Width="350px" />
                    </asp:TemplateField>


                    <asp:TemplateField HeaderText="Ore">
                        <ItemTemplate>
                            <asp:TextBox ID="Txt_Ore" runat="server" Style="text-align: right;" Width="100px"></asp:TextBox>
                        </ItemTemplate>
                        <HeaderStyle Font-Bold="False" Font-Italic="False" />
                        <ItemStyle Width="100px" />
                    </asp:TemplateField>


                    <asp:TemplateField HeaderText="intypecod">
                        <ItemTemplate>
                            <asp:TextBox ID="Txt_intypecod" runat="server" Style="text-align: right;" Width="100px"></asp:TextBox>
                        </ItemTemplate>
                        <HeaderStyle Font-Bold="False" Font-Italic="False" />
                        <ItemStyle Width="100px" />
                    </asp:TemplateField>



                    <asp:TemplateField HeaderText="intypedescription">
                        <ItemTemplate>
                            <asp:TextBox ID="Txt_intypedescription" runat="server" Style="text-align: right;" Width="100px"></asp:TextBox>
                        </ItemTemplate>
                        <HeaderStyle Font-Bold="False" Font-Italic="False" />
                        <ItemStyle Width="200px" />
                    </asp:TemplateField>



                </Columns>

                <FooterStyle BackColor="White" ForeColor="#023102" />

                <HeaderStyle BackColor="#A6C9E2" Font-Bold="False" ForeColor="White" BorderColor="#6FA7D1" BorderWidth="1" />

                <PagerStyle BackColor="#336666" ForeColor="White" HorizontalAlign="Center" />

                <SelectedRowStyle BackColor="#339966" Font-Bold="True" ForeColor="White" />
            </asp:GridView>

        </div>
    </form>
</body>
</html>
