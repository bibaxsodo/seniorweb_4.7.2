﻿Imports System.Data.OleDb
Imports System.Web.Hosting
Imports System.IO
Imports System.Security.Cryptography

Partial Class Appalti_Appalti
    Inherits System.Web.UI.Page
    Dim MyTable As New System.Data.DataTable("tabella")
    Dim TabellaBudget As New System.Data.DataTable("TabellaBudget")
    Dim MyDataSet As New System.Data.DataSet()
    Dim Tabella As New System.Data.DataTable("TabellaAllegati")
    Dim TabellaRigheExtra As New System.Data.DataTable("TabellaRigheExtra")

    Protected Sub EncryptFile(ByVal sender As Object, ByVal e As EventArgs)
        'Get the Input File Name and Extension.  
        Dim fileName As String = Path.GetFileNameWithoutExtension(FileUpload1.PostedFile.FileName)
        Dim fileExtension As String = Path.GetExtension(FileUpload1.PostedFile.FileName)

        'Build the File Path for the original (input) and the encrypted (output) file.  
        Dim input As String = Convert.ToString(Server.MapPath("~/Files/") & fileName) & fileExtension
        Dim output As String = Convert.ToString((Server.MapPath("~/Files/") & fileName) + "_enc") & fileExtension

        'Save the Input File, Encrypt it and save the encrypted file in output path.  
        FileUpload1.SaveAs(input)
        Me.Encrypt(input, output)

        'Download the Encrypted File.  
        Response.ContentType = FileUpload1.PostedFile.ContentType
        Response.Clear()
        Response.AppendHeader("Content-Disposition", "attachment; filename=" + Path.GetFileName(output))
        Response.WriteFile(output)
        Response.Flush()

        'Delete the original (input) and the encrypted (output) file.  
        File.Delete(input)
        File.Delete(output)

        Response.End()
    End Sub

    Protected Sub DecryptFile(ByVal sender As Object, ByVal e As EventArgs)
        'Get the Input File Name and Extension  
        Dim fileName As String = Path.GetFileNameWithoutExtension(FileUpload1.PostedFile.FileName)
        Dim fileExtension As String = Path.GetExtension(FileUpload1.PostedFile.FileName)

        'Build the File Path for the original (input) and the decrypted (output) file  
        Dim input As String = Convert.ToString(Server.MapPath("~/Files/") & fileName) & fileExtension
        Dim output As String = Convert.ToString((Server.MapPath("~/Files/") & fileName) + "_dec") & fileExtension

        'Save the Input File, Decrypt it and save the decrypted file in output path.  
        FileUpload1.SaveAs(input)
        Me.Decrypt(input, output)

        'Download the Decrypted File.  
        Response.Clear()
        Response.ContentType = FileUpload1.PostedFile.ContentType
        Response.AppendHeader("Content-Disposition", "attachment; filename=" + Path.GetFileName(output))
        Response.WriteFile(output)
        Response.Flush()

        'Delete the original (input) and the decrypted (output) file.  
        File.Delete(input)
        File.Delete(output)

        Response.End()
    End Sub

    Private Shared Function Assign(Of T)(ByRef source As T, ByVal value As T) As T
        source = value
        Return value
    End Function

    Private Sub Encrypt(ByVal inputFilePath As String, ByVal outputfilePath As String)
        Dim EncryptionKey As String = "SENIOR1714"
        Using encryptor As Aes = Aes.Create()
            Dim pdb As New Rfc2898DeriveBytes(EncryptionKey, New Byte() {&H49, &H76, &H61, &H6E, &H20, &H4D, _
             &H65, &H64, &H76, &H65, &H64, &H65, _
             &H76})
            encryptor.Key = pdb.GetBytes(32)
            encryptor.IV = pdb.GetBytes(16)
            Using fs As New FileStream(outputfilePath, FileMode.Create)
                Using cs As New CryptoStream(fs, encryptor.CreateEncryptor(), CryptoStreamMode.Write)
                    Using fsInput As New FileStream(inputFilePath, FileMode.Open)
                        Dim data As Integer
                        While (Assign(data, fsInput.ReadByte())) <> -1
                            cs.WriteByte(CByte(data))
                        End While
                    End Using
                End Using
            End Using
        End Using
    End Sub

    Private Sub Decrypt(ByVal inputFilePath As String, ByVal outputfilePath As String)
        Dim EncryptionKey As String = "SENIOR1714"
        Using encryptor As Aes = Aes.Create()
            Dim pdb As New Rfc2898DeriveBytes(EncryptionKey, New Byte() {&H49, &H76, &H61, &H6E, &H20, &H4D, _
             &H65, &H64, &H76, &H65, &H64, &H65, _
             &H76})
            encryptor.Key = pdb.GetBytes(32)
            encryptor.IV = pdb.GetBytes(16)
            Using fs As New FileStream(inputFilePath, FileMode.Open)
                Using cs As New CryptoStream(fs, encryptor.CreateDecryptor(), CryptoStreamMode.Read)
                    Using fsOutput As New FileStream(outputfilePath, FileMode.Create)
                        Dim data As Integer
                        While (Assign(data, cs.ReadByte())) <> -1
                            fsOutput.WriteByte(CByte(data))
                        End While
                    End Using
                End Using
            End Using
        End Using
    End Sub
    Private Sub LeggiDirectory()
        Dim i As Long = 0

        Tabella.Clear()
        Tabella.Columns.Clear()
        Tabella.Columns.Add("NomeFile", GetType(String))
        Tabella.Columns.Add("Descrizione", GetType(String))
        Tabella.Columns.Add("Data", GetType(String))

        Dim NomeSocieta As String


        Dim k As New Cls_Login

        k.Utente = Session("UTENTE")
        k.LeggiSP(Application("SENIOR"))

        NomeSocieta = k.RagioneSociale


        Dim Appo As String

        Appo = Dir(HostingEnvironment.ApplicationPhysicalPath() & "\Public\" & NomeSocieta)
        If Dir(HostingEnvironment.ApplicationPhysicalPath() & "\Public\" & NomeSocieta, FileAttribute.Directory) = "" Then
            MkDir(HostingEnvironment.ApplicationPhysicalPath() & "\Public\" & NomeSocieta)
        End If


        If Dir(HostingEnvironment.ApplicationPhysicalPath() & "\Public\" & NomeSocieta & "\Appalti_" & Val(Request.Item("id")), FileAttribute.Directory) = "" Then
            MkDir(HostingEnvironment.ApplicationPhysicalPath() & "\Public\" & NomeSocieta & "\Appalti_" & Val(Request.Item("id")))
        End If


        Dim objDI As New DirectoryInfo(HostingEnvironment.ApplicationPhysicalPath() & "\Public\" & NomeSocieta & "\Appalti_" & Val(Request.Item("id")) & "\")
        Dim aryItemsInfo() As FileSystemInfo
        Dim objItem As FileSystemInfo

        aryItemsInfo = objDI.GetFileSystemInfos()
        For Each objItem In aryItemsInfo
            Console.WriteLine(objItem.Name)

            Dim myriga As System.Data.DataRow = Tabella.NewRow()

            myriga(0) = objItem.Name




            myriga(0) = objItem.Name



            Tabella.Rows.Add(myriga)
            i = i + 1
        Next

        If i = 0 Then
            Dim myriga As System.Data.DataRow = Tabella.NewRow()
            Tabella.Rows.Add(myriga)
        End If
        Session("Documentazione") = Tabella
        Grid.AutoGenerateColumns = False
        Grid.DataSource = Tabella
        Grid.DataBind()
    End Sub


    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Trim(Session("UTENTE")) = "" Then
            Response.Redirect("../Login.aspx")
            Exit Sub
        End If

        EseguiJS()

        If Page.IsPostBack = True Then
            Exit Sub
        End If

        Dim K1 As New Cls_SqlString

        Dim Barra As New Cls_BarraSenior

        If Not IsNothing(Session("RicercaAnagraficaSQLString")) Then            
            Try
                K1 = Session("RicercaAnagraficaSQLString")
            Catch ex As Exception
                K1 = Nothing
            End Try
        End If

        Lbl_BarraSenior.Text = Barra.CodiceBarra(Application("SENIOR"), Session("UTENTE"), Page, K1)


        Dim MyE As New ClsUSL

        MyE.UpDateDropBox(Session("DC_OSPITE"), DD_Usl)

        Dim CauCon As New Cls_CausaleContabile

        CauCon.UpDateDropBox(Session("DC_TABELLE"), DD_CausaleContabile)

        Dim IVA As New Cls_IVA

        IVA.UpDateDropBox(Session("DC_TABELLE"), DD_CodiceIVA)
        IVA.UpDateDropBox(Session("DC_TABELLE"), DD_CodiceIVAKM)

        Dim Appalto As New Cls_AppaltiAnagrafica

        Appalto.Id = Val(Request.Item("id"))
        Appalto.Leggi(Session("DC_TABELLE"))
        If Appalto.Descrizione <> "" Then

            Txt_Descrizione.Text = Appalto.Descrizione
            Txt_Cig.Text = Appalto.Cig
            Txt_DataInizio.Text = Format(Appalto.DataInizio, "dd/MM/yyyy")
            Txt_DataFine.Text = Format(Appalto.DataFine, "dd/MM/yyyy")
            Txt_Gruppo.Text = Appalto.Gruppo
            Txt_ImportoForfait.Text = Format(Appalto.ImportoForfait, "#,##0.00")
            Txt_ImportoTeoricoGiornaliero.Text = Format(Appalto.ImportoTeoricoGiornaliero, "#,##0.00")
            Txt_ImportoTeoricoMensile.Text = Format(Appalto.ImportoTeoricoMensile, "#,##0.00")
            Txt_ImportoTeoricoMensileInaddGiornaliero.Text = Format(Appalto.ImportoTeoricoMensileInaddGiornaliero, "#,##0.00")

            DD_CausaleContabile.SelectedValue = Appalto.CausaleContabile

            DD_Usl.SelectedValue = Appalto.Committente
            Txt_Id.Text = Val(Request.Item("id"))

            If Appalto.RotturaStruttura = 1 Then
                Chk_ModFatt.Checked = True
            Else
                Chk_ModFatt.Checked = False
            End If


            If Appalto.RaggruppaRicavi = 1 Then
                Chk_RaggruppaRicavi.Checked = True
            Else
                Chk_RaggruppaRicavi.Checked = False
            End If

            If Appalto.Tipo = "F" Then
                RB_Forfait.Checked = True
                RB_ImportoVaribile.Checked = False
            Else
                RB_Forfait.Checked = False
                RB_ImportoVaribile.Checked = True
            End If

            DD_CodiceIVA.SelectedValue = Appalto.CodiceIva

            Dim Conto As New Cls_Pianodeiconti


            Conto.Mastro = Appalto.Mastro
            Conto.Conto = Appalto.Conto
            Conto.Sottoconto = Appalto.Sottoconto
            Conto.Decodfica(Session("DC_GENERALE"))

            Txt_Conto.Text = Conto.Mastro & " " & Conto.Conto & " " & Conto.Sottoconto & " " & Conto.Descrizione


            Conto.Descrizione = ""
            Conto.Mastro = Appalto.MastroKM
            Conto.Conto = Appalto.ContoKM
            Conto.Sottoconto = Appalto.SottocontoKM
            Conto.Decodfica(Session("DC_GENERALE"))

            Txt_ContoKM.Text = Conto.Mastro & " " & Conto.Conto & " " & Conto.Sottoconto & " " & Conto.Descrizione


            DD_CodiceIVAKM.SelectedValue = Appalto.CodiceIvaKM

            LeggiDirectory()
        End If
        loadpagina(Appalto.Id)

        CambioCheck()
    End Sub


    Sub loadpagina(ByVal id As Integer)
        Dim x As New ClsOspite
        Dim d As New Cls_Appalti_AppaltiStrutture




        Dim ConnectionString As String = Session("DC_TABELLE")





        d.loaddati(ConnectionString, id, MyTable)

        ViewState("App_Retta") = MyTable

        Grd_Retta.AutoGenerateColumns = False

        Grd_Retta.DataSource = MyTable

        Grd_Retta.DataBind()



        Dim Budget As New Cls_Appalti_AppaltiBudget

        Budget.loaddati(ConnectionString, id, TabellaBudget)

        ViewState("App_Budget") = TabellaBudget

        Grid_Budget.AutoGenerateColumns = False

        Grid_Budget.DataSource = TabellaBudget

        Grid_Budget.DataBind()



        Dim RigheAggiuntive As New Cls_Appalti_AppaltiRigheAggiuntive

        RigheAggiuntive.loaddati(ConnectionString, Session("DC_GENERALE"), id, TabellaRigheExtra)

        ViewState("App_TabellaRigheExtra") = TabellaRigheExtra

        Grd_RigheAggiuntive.AutoGenerateColumns = False

        Grd_RigheAggiuntive.DataSource = TabellaRigheExtra

        Grd_RigheAggiuntive.DataBind()



    End Sub

    Private Sub EseguiJS()
        Dim MyJs As String
        MyJs = "$(document).ready(function() {"

        MyJs = MyJs & "var els = document.getElementsByTagName(""*"");"

        MyJs = MyJs & "for (var i=0;i<els.length;i++)"
        MyJs = MyJs & "if ( els[i].id ) { "
        MyJs = MyJs & " var appoggio =els[i].id; "
        MyJs = MyJs & " if (appoggio.match('Txt_Data')!= null) {  "
        MyJs = MyJs & " $(els[i]).mask(""99/99/9999"");"
        MyJs = MyJs & " $(els[i]).datepicker({ changeMonth: true,changeYear: true,  yearRange: ""2000:" & Year(Now) + 12 & """},$.datepicker.regional[""it""]);"
        MyJs = MyJs & "    }"
        MyJs = MyJs & " if ((appoggio.match('Txt_Importo')!= null) || (appoggio.match('Txt_Percentuale')!= null) ) {  "
        MyJs = MyJs & " $(els[i]).keypress(function() { ForceNumericInput($(this).val(), true, true); } ); "
        MyJs = MyJs & " $(els[i]).blur(function() { var ap = formatNumber($(this).val(),2); $(this).val(ap); });"
        MyJs = MyJs & "    }"

        MyJs = MyJs & " if ((appoggio.match('TxtImporto')!= null) ) {  "
        MyJs = MyJs & " $(els[i]).keypress(function() { ForceNumericInput($(this).val(), true, true); } ); "
        MyJs = MyJs & " $(els[i]).blur(function() { var ap = formatNumber($(this).val(),4); $(this).val(ap); });"
        MyJs = MyJs & "    }"

        'TxtImporto

        MyJs = MyJs & " if ((appoggio.match('Txt_Conto')!= null) || (appoggio.match('TxtConto')!= null))  {   "
        MyJs = MyJs & " $(els[i]).autocomplete('/WebHandler/GestioneConto.ashx?Utente=" & Session("UTENTE") & "', {delay:5,minChars:3});"
        MyJs = MyJs & "    }"

        MyJs = MyJs & " if (appoggio.match('Txt_ComRes')!= null)  {  "
        MyJs = MyJs & " $(els[i]).autocomplete('/WebHandler/AutocompleteComune.ashx?Utente=" & Session("UTENTE") & "', {delay:5,minChars:3});"
        MyJs = MyJs & "    }"

        MyJs = MyJs & "} "


        MyJs = MyJs & "$("".chosen-select"").chosen({"
        MyJs = MyJs & "no_results_text: ""Non trovato"","
        MyJs = MyJs & "display_disabled_options: true,"
        MyJs = MyJs & "allow_single_deselect: true,"
        MyJs = MyJs & "width: ""400px"","
        MyJs = MyJs & "placeholder_text_single: ""Seleziona """
        MyJs = MyJs & "});"


        MyJs = MyJs & "$("".chosen-select2"").chosen({"
        MyJs = MyJs & "no_results_text: ""Non trovato"","
        MyJs = MyJs & "display_disabled_options: true,"
        MyJs = MyJs & "allow_single_deselect: true,"
        MyJs = MyJs & "width: ""250px"","
        MyJs = MyJs & "placeholder_text_single: ""Seleziona """
        MyJs = MyJs & "});"

        MyJs = MyJs & "});"
        'MyJs = "$(document).ready(function() { $('.myClass').keypress(function() { handleEnter($('.myClass').val(), true, true); } );     $('#" & Txt_ImportoPagato.ClientID & "').blur(function() { var ap = formatNumber ($('#" & Txt_ImportoPagato.ClientID & "').val(),2); $('#" & Txt_ImportoPagato.ClientID & "').val(ap); }); });"
        ScriptManager.RegisterStartupScript(Me, Me.GetType(), "visualizzaRitIm", MyJs, True)
    End Sub


    Protected Sub Btn_Esci_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles Btn_Esci.Click
        Response.Redirect("Elenco_Appalti.aspx")
    End Sub

    Private Sub InserisciRigaBudget()
        UpDateTableBudget()
        TabellaBudget = ViewState("App_Budget")
        Dim myriga As System.Data.DataRow = TabellaBudget.NewRow()
        myriga(0) = ""
        myriga(1) = 0
        myriga(2) = 0

        TabellaBudget.Rows.Add(myriga)



        ViewState("App_Budget") = TabellaBudget

        Grid_Budget.AutoGenerateColumns = False

        Grid_Budget.DataSource = TabellaBudget

        Grid_Budget.DataBind()




        Call EseguiJS()
    End Sub

    Private Sub InserisciRigaRigheExtra()
        UpDateTableRigheAggiuntive()

        TabellaRigheExtra = ViewState("App_TabellaRigheExtra")
        Dim myriga As System.Data.DataRow = TabellaRigheExtra.NewRow()
        myriga(0) = ""
        myriga(1) = ""

        myriga(2) = ""
        myriga(3) = 0
        myriga(4) = 0

        TabellaRigheExtra.Rows.Add(myriga)

        ViewState("App_TabellaRigheExtra") = TabellaRigheExtra
        Grd_RigheAggiuntive.AutoGenerateColumns = False

        Grd_RigheAggiuntive.DataSource = TabellaRigheExtra

        Grd_RigheAggiuntive.DataBind()




        Call EseguiJS()
    End Sub


    Private Sub InserisciRiga()
        UpDateTable()
        MyTable = ViewState("App_Retta")
        Dim myriga As System.Data.DataRow = MyTable.NewRow()
        myriga(0) = 0
        myriga(1) = 0
    
        MyTable.Rows.Add(myriga)

        ViewState("App_Retta") = MyTable
        Grd_Retta.AutoGenerateColumns = False

        Grd_Retta.DataSource = MyTable

        Grd_Retta.DataBind()


       

        Call EseguiJS()
    End Sub

    Protected Sub Grd_Retta_RowCommand(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewCommandEventArgs) Handles Grd_Retta.RowCommand
        If (e.CommandName = "Inserisci") Then
            Call InserisciRiga()
        End If


    End Sub




    Function campodb(ByVal oggetto As Object) As String
        If IsDBNull(oggetto) Then
            Return ""
        Else
            Return oggetto
        End If
    End Function



    Function campodbn(ByVal oggetto As Object) As Double
        If IsDBNull(oggetto) Then
            Return 0
        Else
            Return oggetto
        End If
    End Function

    Protected Sub Grd_Retta_RowDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs) Handles Grd_Retta.RowDataBound
        If e.Row.RowType = DataControlRowType.DataRow Then




            Dim DD_Struttura As DropDownList = DirectCast(e.Row.FindControl("DD_Struttura"), DropDownList)
            Dim Struttura As New Cls_AppaltiTabellaStruttura

            Struttura.UpDateDropBox(Session("DC_TABELLE"), DD_Struttura)
            DD_Struttura.SelectedValue = MyTable.Rows(e.Row.RowIndex).Item(0).ToString


            If RB_Forfait.Checked = False Then
                Dim Txt_ImportoForait As TextBox = DirectCast(e.Row.FindControl("Txt_ImportoForait"), TextBox)

                If campodb(MyTable.Rows(e.Row.RowIndex).Item(1).ToString) = "" Then
                    MyTable.Rows(e.Row.RowIndex).Item(1) = "0"
                End If
                Txt_ImportoForait.Text = Format(campodbn(MyTable.Rows(e.Row.RowIndex).Item(1).ToString), "#,##0.00")

                Txt_ImportoForait.Enabled = False
                Txt_ImportoForait.BackColor = Drawing.Color.Gray
            Else
                Dim Txt_ImportoForait As TextBox = DirectCast(e.Row.FindControl("Txt_ImportoForait"), TextBox)

                Txt_ImportoForait.Enabled = True
                Txt_ImportoForait.BackColor = Drawing.Color.White

                Txt_ImportoForait.Text = Format(campodbn(MyTable.Rows(e.Row.RowIndex).Item(1).ToString), "#,##0.00")
            End If


            Dim Txt_KM As TextBox = DirectCast(e.Row.FindControl("Txt_KM"), TextBox)


            If campodb(MyTable.Rows(e.Row.RowIndex).Item(2).ToString) = "" Then
                Txt_KM.Text = 0
            Else
                Txt_KM.Text = Format(campodbn(MyTable.Rows(e.Row.RowIndex).Item(2).ToString), "#,##0.00")
            End If

            Dim Txt_ImportoKM As TextBox = DirectCast(e.Row.FindControl("Txt_ImportoKM"), TextBox)

            If MyTable.Rows(e.Row.RowIndex).Item(3).ToString = "" Then
                Txt_ImportoKM.Text = 0
            Else
                Txt_ImportoKM.Text = Format(campodbn(MyTable.Rows(e.Row.RowIndex).Item(3).ToString), "#,##0.00")
            End If

            'Dim Mansione As New Cls_AppaltiTabellaMansione

            'Mansione.UpDateDropBox(Session("DC_TABELLE"), DD_Incarico)

            'Dim DD_Incarico As DropDownList = DirectCast(e.Row.FindControl("DD_Incarico"), DropDownList)
            'DD_Incarico.SelectedValue = MyTable.Rows(e.Row.RowIndex).Item(1).ToString




            'Dim TxtImportoOrario As TextBox = DirectCast(e.Row.FindControl("TxtImportoOrario"), TextBox)

            'TxtImportoOrario.Text = MyTable.Rows(e.Row.RowIndex).Item(2).ToString


            'Dim TxtImportoIntervento As TextBox = DirectCast(e.Row.FindControl("TxtImportoIntervento"), TextBox)

            'TxtImportoIntervento.Text = MyTable.Rows(e.Row.RowIndex).Item(3).ToString


            'Dim TxtImportoFisso As TextBox = DirectCast(e.Row.FindControl("TxtImportoFisso"), TextBox)

            'TxtImportoFisso.Text = MyTable.Rows(e.Row.RowIndex).Item(4).ToString



            Call EseguiJS()

        End If
    End Sub


    Private Sub UpDateTableRigheAggiuntive()



        Dim i As Integer

        TabellaRigheExtra.Clear()
        TabellaRigheExtra.Columns.Clear()
        
        TabellaRigheExtra.Columns.Add("Conto", GetType(String))
        TabellaRigheExtra.Columns.Add("Regola", GetType(String))
        TabellaRigheExtra.Columns.Add("IVA", GetType(String))
        TabellaRigheExtra.Columns.Add("Quantita", GetType(String))
        TabellaRigheExtra.Columns.Add("Importo", GetType(String))
        TabellaRigheExtra.Columns.Add("Descrizione", GetType(String))

        TabellaRigheExtra.Columns.Add("Struttura", GetType(String))



        For i = 0 To Grd_RigheAggiuntive.Rows.Count - 1


            Dim Txt_Conto As TextBox = DirectCast(Grd_RigheAggiuntive.Rows(i).FindControl("TxtConto"), TextBox)

            Dim DD_Regola As DropDownList = DirectCast(Grd_RigheAggiuntive.Rows(i).FindControl("DD_Regola"), DropDownList)

            Dim DD_IVA As DropDownList = DirectCast(Grd_RigheAggiuntive.Rows(i).FindControl("DD_IVA"), DropDownList)

            Dim Txt_Importo As TextBox = DirectCast(Grd_RigheAggiuntive.Rows(i).FindControl("Txt_Importo"), TextBox)


            Dim Txt_Quantita As TextBox = DirectCast(Grd_RigheAggiuntive.Rows(i).FindControl("Txt_Quantita"), TextBox)

            Dim Txt_Descrizione As TextBox = DirectCast(Grd_RigheAggiuntive.Rows(i).FindControl("Txt_Descrizione"), TextBox)


            Dim DD_Struttura As DropDownList = DirectCast(Grd_RigheAggiuntive.Rows(i).FindControl("DD_Struttura"), DropDownList)



            Dim Vettore(100) As String
            Dim Appoggio As String



            Dim myrigaR As System.Data.DataRow = TabellaRigheExtra.NewRow()



            myrigaR(0) = Txt_Conto.Text

            myrigaR(1) = DD_Regola.SelectedValue

            myrigaR(2) = DD_IVA.SelectedValue

            myrigaR(3) = Txt_Quantita.Text

            myrigaR(4) = Txt_Importo.Text

            myrigaR(5) = Txt_Descrizione.Text

            myrigaR(6) = DD_Struttura.SelectedValue



            TabellaRigheExtra.Rows.Add(myrigaR)
        Next

        ViewState("App_TabellaRigheExtra") = TabellaRigheExtra

    End Sub


    Private Sub UpDateTable()



        Dim i As Integer

        MyTable.Clear()
        MyTable.Columns.Clear()
        MyTable.Columns.Add("Struttura", GetType(String))
        MyTable.Columns.Add("Mansione", GetType(String))
        MyTable.Columns.Add("ImportoOrario", GetType(String))
        MyTable.Columns.Add("KM", GetType(String))
        MyTable.Columns.Add("KMImporto", GetType(String))


        For i = 0 To Grd_Retta.Rows.Count - 1

            Dim DD_Struttura As DropDownList = DirectCast(Grd_Retta.Rows(i).FindControl("DD_Struttura"), DropDownList)



            'Dim DD_Incarico As DropDownList = DirectCast(Grd_Retta.Rows(i).FindControl("DD_Incarico"), DropDownList)

            Dim Txt_ImportoForait As TextBox = DirectCast(Grd_Retta.Rows(i).FindControl("Txt_ImportoForait"), TextBox)
            Dim Txt_KM As TextBox = DirectCast(Grd_Retta.Rows(i).FindControl("Txt_KM"), TextBox)
            Dim Txt_ImportoKM As TextBox = DirectCast(Grd_Retta.Rows(i).FindControl("Txt_ImportoKM"), TextBox)



            Dim Vettore(100) As String
            Dim Appoggio As String



            Dim myrigaR As System.Data.DataRow = MyTable.NewRow()



            myrigaR(0) = DD_Struttura.SelectedValue

            myrigaR(1) = Txt_ImportoForait.Text

            myrigaR(2) = Txt_KM.Text

            myrigaR(3) = Txt_ImportoKM.Text


            MyTable.Rows.Add(myrigaR)
        Next
        ViewState("App_Retta") = MyTable
    End Sub
    Private Sub UpDateTableBudget()



        Dim i As Integer

        TabellaBudget.Clear()
        TabellaBudget.Columns.Clear()
        TabellaBudget.Columns.Add("IdTipoOperatore", GetType(String))
        TabellaBudget.Columns.Add("Ore", GetType(String))
        TabellaBudget.Columns.Add("Importo", GetType(String))


        For i = 0 To Grid_Budget.Rows.Count - 1

            Dim DD_TipoOperatore As DropDownList = DirectCast(Grid_Budget.Rows(i).FindControl("DD_TipoOperatore"), DropDownList)

            Dim Txt_Ore As TextBox = DirectCast(Grid_Budget.Rows(i).FindControl("Txt_Ore"), TextBox)
            Dim Txt_Importo As TextBox = DirectCast(Grid_Budget.Rows(i).FindControl("TxtImporto"), TextBox)



            Dim Vettore(100) As String
            Dim Appoggio As String



            Dim myrigaR As System.Data.DataRow = TabellaBudget.NewRow()



            myrigaR(0) = DD_TipoOperatore.SelectedValue

            myrigaR(1) = Txt_Ore.Text

            myrigaR(2) = Txt_Importo.Text



            TabellaBudget.Rows.Add(myrigaR)
        Next
        ViewState("App_Budget") = TabellaBudget
    End Sub


    Protected Sub Grd_Retta_RowDeleted(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewDeletedEventArgs) Handles Grd_Retta.RowDeleted

    End Sub


    Protected Sub Grd_Retta_RowDeleting(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewDeleteEventArgs) Handles Grd_Retta.RowDeleting

        UpDateTable()
        MyTable = ViewState("App_Retta")
        Try
            If Request.UserAgent.Contains("Chrome") Then
                If IsDBNull(Session("RI_TIMER")) Or DateDiff("s", Session("RI_TIMER"), Now) > 1 Then
                    MyTable.Rows.RemoveAt(e.RowIndex)
                    If MyTable.Rows.Count = 0 Then
                        Dim myriga As System.Data.DataRow = MyTable.NewRow()
                        myriga(1) = 0
                        myriga(2) = 0
                        MyTable.Rows.Add(myriga)
                    End If
                    Session("RI_TIMER") = Now
                End If
            Else
                MyTable.Rows.RemoveAt(e.RowIndex)
                If MyTable.Rows.Count = 0 Then
                    Dim myriga As System.Data.DataRow = MyTable.NewRow()
                    myriga(1) = 0
                    myriga(2) = 0
                    MyTable.Rows.Add(myriga)
                End If
            End If
        Catch ex As Exception

        End Try


        ViewState("App_Retta") = MyTable

        Grd_Retta.AutoGenerateColumns = False

        Grd_Retta.DataSource = MyTable        
        Grd_Retta.DataBind()
        Call EseguiJS()

        e.Cancel = True
    End Sub

    Protected Sub Btn_Modifica_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles Btn_Modifica.Click

        If Not IsDate(Txt_DataInizio.Text) Then
            ClientScript.RegisterClientScriptBlock(Me.GetType(), "Errore", "VisualizzaErrore('Data Inizio formalmente errata');", True)
            Exit Sub
        End If

        If Not IsDate(Txt_DataFine.Text) Then
            ClientScript.RegisterClientScriptBlock(Me.GetType(), "Errore", "VisualizzaErrore('Data Fine formalmente errata');", True)
            Exit Sub
        End If


        If Year(Txt_DataInizio.Text) < 1900 Then
            ClientScript.RegisterClientScriptBlock(Me.GetType(), "Errore", "VisualizzaErrore('Data Inizio formalmente errata');", True)
            Exit Sub
        End If

        If Year(Txt_DataFine.Text) < 1900 Then
            ClientScript.RegisterClientScriptBlock(Me.GetType(), "Errore", "VisualizzaErrore('Data Fine formalmente errata');", True)
            Exit Sub
        End If

        If Val(DD_Usl.SelectedValue) = 0 Then
            ClientScript.RegisterClientScriptBlock(Me.GetType(), "Errore", "VisualizzaErrore('Indica committente');", True)
            Exit Sub
        End If

        If Val(DD_CausaleContabile.SelectedValue) = 0 Then
            ClientScript.RegisterClientScriptBlock(Me.GetType(), "Errore", "VisualizzaErrore('Indica causale contabile');", True)
            Exit Sub
        End If

        If RB_Forfait.Checked = True Then
            Dim I As Integer
            Dim Appoggio As Double = 0
            Dim AppoggioForfait As Double

            AppoggioForfait = Txt_ImportoForfait.Text

            For I = 0 To Grd_Retta.Rows.Count - 1
                Dim TxtImportoForait As TextBox = DirectCast(Grd_Retta.Rows(I).FindControl("Txt_ImportoForait"), TextBox)
                Dim DD_Struttura As DropDownList = DirectCast(Grd_Retta.Rows(I).FindControl("DD_Struttura"), DropDownList)

                For x = 0 To Grd_Retta.Rows.Count - 1
                    Dim DD_St2 As DropDownList = DirectCast(Grd_Retta.Rows(x).FindControl("DD_Struttura"), DropDownList)

                    If x <> I Then
                        If DD_Struttura.SelectedValue = DD_St2.SelectedValue Then
                            ClientScript.RegisterClientScriptBlock(Me.GetType(), "Errore", "VisualizzaErrore('Non puoi indicare due volte la stessa struttura per lo stesso appalto');", True)
                            Exit Sub
                        End If
                    End If
                Next


                Appoggio = Appoggio + campodbn(TxtImportoForait.Text)
            Next
            If Math.Round(Appoggio, 2) <> Math.Round(AppoggioForfait, 2) Then
                ClientScript.RegisterClientScriptBlock(Me.GetType(), "Errore", "VisualizzaErrore('Importo Forfait appalto non coincide con importo appalto strutture');", True)
                Exit Sub
            End If



        End If


        Dim Appalto As New Cls_AppaltiAnagrafica

        Appalto.Id = Val(Txt_Id.Text)
        Appalto.Descrizione = Txt_Descrizione.Text

        Appalto.Gruppo = Txt_Gruppo.Text

        Appalto.Cig = Txt_Cig.Text

        If Txt_ImportoForfait.Text = "" Then
            Txt_ImportoForfait.Text = "0"
        End If

        If Txt_ImportoTeoricoGiornaliero.Text = "" Then
            Txt_ImportoTeoricoGiornaliero.Text = "0"
        End If

        If Txt_ImportoForfait.Text = "" Then
            Txt_ImportoForfait.Text = "0"
        End If

        Appalto.DataInizio = Txt_DataInizio.Text
        Appalto.DataFine = Txt_DataFine.Text

        If Txt_ImportoForfait.Text = "" Then
            Txt_ImportoForfait.Text = "0"
        End If
        If Txt_ImportoTeoricoGiornaliero.Text = "" Then
            Txt_ImportoTeoricoGiornaliero.Text = "0"
        End If
        If Txt_ImportoTeoricoMensile.Text = "" Then
            Txt_ImportoTeoricoMensile.Text = "0"
        End If
        If Txt_ImportoTeoricoMensileInaddGiornaliero.Text = "" Then
            Txt_ImportoTeoricoMensileInaddGiornaliero.Text = "0"
        End If

        Appalto.Committente = DD_Usl.SelectedValue
        Appalto.ImportoForfait = CDbl(Txt_ImportoForfait.Text)
        Appalto.ImportoTeoricoGiornaliero = CDbl(Txt_ImportoTeoricoGiornaliero.Text)
        Appalto.ImportoTeoricoMensile = CDbl(Txt_ImportoTeoricoMensile.Text)
        Appalto.ImportoTeoricoMensileInaddGiornaliero = CDbl(Txt_ImportoTeoricoMensileInaddGiornaliero.Text)


        Appalto.CausaleContabile = DD_CausaleContabile.SelectedValue

        If RB_Forfait.Checked = True Then
            Appalto.Tipo = "F"
        Else
            Appalto.Tipo = "V"
        End If

        If Chk_ModFatt.Checked = True Then
            Appalto.RotturaStruttura = 1
        Else
            Appalto.RotturaStruttura = 0
        End If


        If Chk_RaggruppaRicavi.Checked = True Then
            Appalto.RaggruppaRicavi = 1
        Else
            Appalto.RaggruppaRicavi = 0
        End If


        Appalto.CodiceIva = DD_CodiceIVA.SelectedValue


        Dim Vettore(100) As String

        Vettore = SplitWords(Txt_Conto.Text)

        Appalto.Mastro = 0
        Appalto.Conto = 0
        Appalto.Sottoconto = 0
        If Vettore.Length >= 3 Then
            Appalto.Mastro = Val(Vettore(0))
            Appalto.Conto = Val(Vettore(1))
            Appalto.Sottoconto = Val(Vettore(2))
        End If


        Vettore = SplitWords(Txt_ContoKM.Text)

        Appalto.MastroKM = 0
        Appalto.ContoKM = 0
        Appalto.SottocontoKM = 0
        If Vettore.Length >= 3 Then
            Appalto.MastroKM = Val(Vettore(0))
            Appalto.ContoKM = Val(Vettore(1))
            Appalto.SottocontoKM = Val(Vettore(2))
        End If

        Appalto.CodiceIvaKM = DD_CodiceIVAKM.SelectedValue


        Appalto.Scrivi(Session("DC_TABELLE"))

        Txt_Id.Text = Appalto.Id

        Call UpDateTable()
        MyTable = ViewState("App_Retta")
        Dim X1 As New Cls_Appalti_AppaltiStrutture


        X1.IdAppalto = Val(Txt_Id.Text)

        X1.AggiornaDaTabella(Session("DC_TABELLE"), MyTable)


        Call UpDateTableBudget()
        TabellaBudget = ViewState("App_Budget")
        Dim X2 As New Cls_Appalti_AppaltiBudget


        X2.IdAppalto = Val(Txt_Id.Text)

        X2.AggiornaDaTabella(Session("DC_TABELLE"), TabellaBudget)




        Call UpDateTableRigheAggiuntive()
        TabellaRigheExtra = ViewState("App_TabellaRigheExtra")
        Dim X3 As New Cls_Appalti_AppaltiRigheAggiuntive


        X3.IdAppalto = Val(Txt_Id.Text)

        X3.AggiornaDaTabella(Session("DC_TABELLE"), TabellaRigheExtra)



        Response.Redirect("Elenco_Appalti.aspx")
    End Sub


    Private Function SplitWords(ByVal s As String) As String()
        '
        ' Call Regex.Split function from the imported namespace.
        ' Return the result array.
        '
        Return Regex.Split(s, "\W+")
    End Function


    Private Sub CambioCheck()
        Dim I As Integer = 0

        If RB_Forfait.Checked = True Then
            Txt_ImportoForfait.Enabled = True

            Txt_ImportoForfait.BackColor = Drawing.Color.White

            Txt_Conto.BackColor = Drawing.Color.White
            Txt_Conto.Enabled = True

            DD_CodiceIVA.Enabled = True
            DD_CodiceIVA.BackColor = Drawing.Color.White


            Txt_ImportoTeoricoGiornaliero.BackColor = Drawing.Color.Gray
            Txt_ImportoTeoricoGiornaliero.Enabled = False

            Txt_ImportoTeoricoMensile.BackColor = Drawing.Color.Gray
            Txt_ImportoTeoricoMensile.Enabled = False

            Txt_ImportoTeoricoMensileInaddGiornaliero.BackColor = Drawing.Color.Gray
            Txt_ImportoTeoricoMensileInaddGiornaliero.Enabled = False

            For I = 0 To Grd_Retta.Rows.Count - 1
                Dim TxtImportoForait As TextBox = DirectCast(Grd_Retta.Rows(I).FindControl("Txt_ImportoForait"), TextBox)

                TxtImportoForait.Enabled = True

                TxtImportoForait.BackColor = Drawing.Color.White
            Next

            Chk_RaggruppaRicavi.Checked = True
            Chk_RaggruppaRicavi.Enabled = False
        Else


            Txt_ImportoForfait.Enabled = False

            Txt_ImportoForfait.BackColor = Drawing.Color.Gray


            Txt_Conto.BackColor = Drawing.Color.Gray
            Txt_Conto.Enabled = False

            DD_CodiceIVA.Enabled = False
            DD_CodiceIVA.BackColor = Drawing.Color.Gray


            Txt_ImportoTeoricoGiornaliero.BackColor = Drawing.Color.White
            Txt_ImportoTeoricoGiornaliero.Enabled = True

            Txt_ImportoTeoricoMensile.BackColor = Drawing.Color.White
            Txt_ImportoTeoricoMensile.Enabled = True

            Txt_ImportoTeoricoMensileInaddGiornaliero.BackColor = Drawing.Color.White
            Txt_ImportoTeoricoMensileInaddGiornaliero.Enabled = True


            For I = 0 To Grd_Retta.Rows.Count - 1
                Dim TxtImportoForait As TextBox = DirectCast(Grd_Retta.Rows(I).FindControl("Txt_ImportoForait"), TextBox)

                TxtImportoForait.Enabled = False

                TxtImportoForait.BackColor = Drawing.Color.Gray
            Next


            Chk_RaggruppaRicavi.Enabled = True
        End If

    End Sub

    Protected Sub RB_Forfait_CheckedChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles RB_Forfait.CheckedChanged
        Call CambioCheck()
    End Sub

    Protected Sub RB_ImportoVaribile_CheckedChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles RB_ImportoVaribile.CheckedChanged
        Call CambioCheck()
    End Sub

    Protected Sub Grid_RowCommand(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewCommandEventArgs) Handles Grid.RowCommand
        If e.CommandName = "DOWNLOAD" Then

            Dim NomeSocieta As String


            Dim k As New Cls_Login

            k.Utente = Session("UTENTE")
            k.LeggiSP(Application("SENIOR"))

            NomeSocieta = k.RagioneSociale

            If e.CommandArgument.ToString.IndexOf("Crypt_") >= 0 Then
                Decrypt(HostingEnvironment.ApplicationPhysicalPath() & "\Public\" & NomeSocieta & "\Appalti_" & Val(Request.Item("id")) & "\" & e.CommandArgument, HostingEnvironment.ApplicationPhysicalPath() & "\Public\" & NomeSocieta & "\Appalti_" & Val(Request.Item("id")) & "\" & e.CommandArgument.ToString.Replace("Crypt_", ""))
            End If

            Try
                HttpContext.Current.Response.Clear()
                HttpContext.Current.Response.ClearHeaders()
                HttpContext.Current.Response.ClearContent()

                Response.Clear()
                Response.ContentType = "application/octect-stream"
                Response.AddHeader("Content-Disposition", _
                                    "attachment; filename=""" & "Copia_" & e.CommandArgument & """")
                Response.TransmitFile(HostingEnvironment.ApplicationPhysicalPath() & "\Public\" & NomeSocieta & "\Appalti_" & Val(Request.Item("id")) & "\" & e.CommandArgument.ToString.Replace("Crypt_", ""))

                Response.Flush()

            Finally
                Kill(HostingEnvironment.ApplicationPhysicalPath() & "\Public\" & NomeSocieta & "\Appalti_" & Val(Request.Item("id")) & "\" & e.CommandArgument.ToString.Replace("Crypt_", ""))
            End Try
        End If
        If e.CommandName = "CANCELLAFILE" Then

            Dim NomeSocieta As String


            Dim k As New Cls_Login

            k.Utente = Session("UTENTE")
            k.LeggiSP(Application("SENIOR"))

            NomeSocieta = k.RagioneSociale

            MyTable = Session("Documentazione")


            Try
                Kill(HostingEnvironment.ApplicationPhysicalPath() & "\Public\" & NomeSocieta & "\Appalti_" & Val(Request.Item("id")) & "\" & MyTable.Rows(e.CommandArgument).Item(0))
            Catch ex As Exception

            End Try


            MyTable.Rows.RemoveAt(e.CommandArgument)

            If MyTable.Rows.Count = 0 Then
                Dim myriga As System.Data.DataRow = MyTable.NewRow()

                MyTable.Rows.Add(myriga)
            End If

            Session("Documentazione") = MyTable

            Call BindDocumentazione()

        End If
    End Sub

    Protected Sub Grid_RowDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs) Handles Grid.RowDataBound
        If e.Row.RowType = DataControlRowType.DataRow Then

            Dim LblDescrizione As Label = DirectCast(e.Row.FindControl("LblDescrizione"), Label)
            If Not IsNothing(LblDescrizione) Then

            Else
                Dim TxtDescrizione As TextBox = DirectCast(e.Row.FindControl("TxtDescrizione"), TextBox)
                Dim MyDv As System.Data.DataRowView = e.Row.DataItem                
            End If
            Dim LblData As Label = DirectCast(e.Row.FindControl("LblData"), Label)
            If Not IsNothing(LblData) Then

            Else
   
            End If
        End If
    End Sub

    Protected Sub Grid_RowEditing(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewEditEventArgs) Handles Grid.RowEditing
        Grid.EditIndex = e.NewEditIndex
        Call BindDocumentazione()
    End Sub



    Protected Sub Grid_RowUpdating(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewUpdateEventArgs) Handles Grid.RowUpdating
        Dim TxtDescrizione As TextBox = DirectCast(Grid.Rows(e.RowIndex).FindControl("TxtDescrizione"), TextBox)
        Dim TxtData As TextBox = DirectCast(Grid.Rows(e.RowIndex).FindControl("TxtData"), TextBox)

        If Not IsDate(TxtData.Text) Then
            ClientScript.RegisterClientScriptBlock(Me.GetType(), "Errore", "VisualizzaErrore('<center>Data formalmente errata</center>');", True)
            Dim MyJs As String

            MyJs = "$(document).ready(function() { $('#" & TxtData.ClientID & "').mask(""99/99/9999""); "


            MyJs = MyJs & " $('#" & TxtData.ClientID & "').datepicker({ changeMonth: true,changeYear: true,  yearRange: ""1990:" & Year(Now) + 5 & """},$.datepicker.regional[""it""]); });"

            ClientScript.RegisterClientScriptBlock(Me.GetType(), "ControlloData", MyJs, True)
            Exit Sub
        End If

        Tabella = Session("Documentazione")

        Session("Documentazione") = Tabella

        Grid.EditIndex = -1
        Call BindDocumentazione()
    End Sub



    Private Sub BindDocumentazione()
        Tabella = Session("Documentazione")
        Grid.AutoGenerateColumns = False
        Grid.DataSource = Tabella
        Grid.DataBind()
    End Sub

    Protected Sub UpLoadFile_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles UpLoadFile.Click
        Session("CODICESERVIZIO") = ViewState("CODICESERVIZIO")
        Session("CODICEOSPITE") = ViewState("CODICEOSPITE")
        If Val(Request.Item("id")) = 0 Then
            ClientScript.RegisterClientScriptBlock(Me.GetType(), "Errore", "VisualizzaErrore('Devi ricercare l ospite');", True)
            Exit Sub
        End If
        Dim NomeSocieta As String


        Dim k As New Cls_Login

        k.Utente = Session("UTENTE")
        k.LeggiSP(Application("SENIOR"))

        NomeSocieta = k.RagioneSociale

        If FileUpload1.FileName.Trim = "" Then
            ScriptManager.RegisterClientScriptBlock(Me, Me.GetType(), "DocumentazioneErroreFileNonSpecificato", "VisualizzaErrore('Specificare un file.');", True)
            Exit Sub
        End If

        Dim Appo As String

        Appo = Dir(HostingEnvironment.ApplicationPhysicalPath() & "\Public\" & NomeSocieta)
        If Dir(HostingEnvironment.ApplicationPhysicalPath() & "\Public\" & NomeSocieta, FileAttribute.Directory) = "" Then
            MkDir(HostingEnvironment.ApplicationPhysicalPath() & "\Public\" & NomeSocieta)
        End If


        If Dir(HostingEnvironment.ApplicationPhysicalPath() & "\Public\" & NomeSocieta & "\Appalti_" & Val(Request.Item("id")), FileAttribute.Directory) = "" Then
            MkDir(HostingEnvironment.ApplicationPhysicalPath() & "\Public\" & NomeSocieta & "\Appalti_" & Val(Request.Item("id")))
        End If

        FileUpload1.SaveAs(HostingEnvironment.ApplicationPhysicalPath() & "\Public\" & NomeSocieta & "\Appalti_" & Val(Request.Item("id")) & "\" & FileUpload1.FileName)

        Encrypt(HostingEnvironment.ApplicationPhysicalPath() & "\Public\" & NomeSocieta & "\Appalti_" & Val(Request.Item("id")) & "\" & FileUpload1.FileName, HostingEnvironment.ApplicationPhysicalPath() & "\Public\" & NomeSocieta & "\Appalti_" & Val(Request.Item("id")) & "\Crypt_" & FileUpload1.FileName)

        Kill(HostingEnvironment.ApplicationPhysicalPath() & "\Public\" & NomeSocieta & "\Appalti_" & Val(Request.Item("id")) & "\" & FileUpload1.FileName)
        Call LeggiDirectory()
    End Sub

    Protected Sub Grid_Budget_RowCommand(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewCommandEventArgs) Handles Grid_Budget.RowCommand
        If (e.CommandName = "Inserisci") Then
            Call InserisciRigaBudget()
        End If

    End Sub

    Protected Sub Grid_Budget_RowDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs) Handles Grid_Budget.RowDataBound
        If e.Row.RowType = DataControlRowType.DataRow Then




            Dim DD_TipoOperatore As DropDownList = DirectCast(e.Row.FindControl("DD_TipoOperatore"), DropDownList)
            Dim TipoOperatore As New Cls_TipoOperatore

            TipoOperatore.UpDateDropBox(Session("DC_OSPITE"), DD_TipoOperatore)
            DD_TipoOperatore.SelectedValue = TabellaBudget.Rows(e.Row.RowIndex).Item(0).ToString





            Dim Txt_Ore As TextBox = DirectCast(e.Row.FindControl("Txt_Ore"), TextBox)


            If campodb(TabellaBudget.Rows(e.Row.RowIndex).Item(1).ToString) = "" Then
                Txt_Ore.Text = 0
            Else
                Txt_Ore.Text = Format(campodbn(TabellaBudget.Rows(e.Row.RowIndex).Item(1).ToString), "#,##0.00")
            End If

            Dim Txt_Importo As TextBox = DirectCast(e.Row.FindControl("TxtImporto"), TextBox)

            If TabellaBudget.Rows(e.Row.RowIndex).Item(2).ToString = "" Then
                Txt_Importo.Text = 0
            Else
                Txt_Importo.Text = Format(campodbn(TabellaBudget.Rows(e.Row.RowIndex).Item(2).ToString), "#,##0.0000")
            End If



            Call EseguiJS()

        End If
    End Sub

    Protected Sub Grid_Budget_RowDeleted(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewDeletedEventArgs) Handles Grid_Budget.RowDeleted

    End Sub

    Protected Sub Grid_Budget_RowDeleting(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewDeleteEventArgs) Handles Grid_Budget.RowDeleting
        UpDateTable()
        TabellaBudget = ViewState("App_Budget")
        Try
            If Request.UserAgent.Contains("Chrome") Then
                If IsDBNull(Session("RI_TIMER")) Or DateDiff("s", Session("RI_TIMER"), Now) > 1 Then
                    TabellaBudget.Rows.RemoveAt(e.RowIndex)
                    If MyTable.Rows.Count = 0 Then
                        Dim myriga As System.Data.DataRow = TabellaBudget.NewRow()
                        myriga(0) = ""
                        myriga(1) = 0
                        myriga(2) = 0
                        MyTable.Rows.Add(myriga)
                    End If
                    Session("RI_TIMER") = Now
                End If
            Else
                MyTable.Rows.RemoveAt(e.RowIndex)
                If MyTable.Rows.Count = 0 Then
                    Dim myriga As System.Data.DataRow = TabellaBudget.NewRow()
                    myriga(0) = ""
                    myriga(1) = 0
                    myriga(2) = 0
                    TabellaBudget.Rows.Add(myriga)
                End If
            End If
        Catch ex As Exception

        End Try


        ViewState("App_Budget") = TabellaBudget

        Grid_Budget.AutoGenerateColumns = False

        Grid_Budget.DataSource = MyTable
        Grid_Budget.DataBind()
        Call EseguiJS()

        e.Cancel = True
    End Sub

    Protected Sub Grd_RigheAggiuntive_RowCommand(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewCommandEventArgs) Handles Grd_RigheAggiuntive.RowCommand
        If (e.CommandName = "Inserisci") Then
            Call InserisciRigaRigheExtra()
        End If

    End Sub

    Protected Sub Grd_RigheAggiuntive_RowDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs) Handles Grd_RigheAggiuntive.RowDataBound
        If e.Row.RowType = DataControlRowType.DataRow Then

            Dim TxtConto As TextBox = DirectCast(e.Row.FindControl("TxtConto"), TextBox)

            TxtConto.Text = campodb(TabellaRigheExtra.Rows(e.Row.RowIndex).Item(0).ToString)



            Dim DD_Regola As DropDownList = DirectCast(e.Row.FindControl("DD_Regola"), DropDownList)
            
            DD_Regola.SelectedValue = campodb(TabellaRigheExtra.Rows(e.Row.RowIndex).Item(1).ToString)



            Dim DD_IVA As DropDownList = DirectCast(e.Row.FindControl("DD_IVA"), DropDownList)

            Dim IVA As New Cls_IVA


            IVA.UpDateDropBox(Session("DC_TABELLE"), DD_IVA)



            DD_IVA.SelectedValue = campodb(TabellaRigheExtra.Rows(e.Row.RowIndex).Item(2).ToString)


            Dim Txt_Quantita As TextBox = DirectCast(e.Row.FindControl("Txt_Quantita"), TextBox)

            If TabellaRigheExtra.Rows(e.Row.RowIndex).Item(3).ToString = "" Then
                Txt_Quantita.Text = 0
            Else
                Txt_Quantita.Text = Format(campodbn(TabellaRigheExtra.Rows(e.Row.RowIndex).Item(3).ToString), "#,##0.00")
            End If



            Dim Txt_Importo As TextBox = DirectCast(e.Row.FindControl("Txt_Importo"), TextBox)


            If campodb(TabellaRigheExtra.Rows(e.Row.RowIndex).Item(4).ToString) = "" Then
                Txt_Importo.Text = 0
            Else
                Txt_Importo.Text = Format(campodbn(TabellaRigheExtra.Rows(e.Row.RowIndex).Item(4).ToString), "#,##0.00")
            End If


            Dim Txt_Descrizione As TextBox = DirectCast(e.Row.FindControl("Txt_Descrizione"), TextBox)

            Txt_Descrizione.Text = campodb(TabellaRigheExtra.Rows(e.Row.RowIndex).Item(5).ToString)


            Dim DD_Struttura As DropDownList = DirectCast(e.Row.FindControl("DD_Struttura"), DropDownList)

            Dim Struttura As New Cls_AppaltiTabellaStruttura


            Struttura.UpDateDropBoxAppaltoNoLinea(Session("DC_TABELLE"), DD_Struttura, Val(Txt_Id.Text))



            DD_Struttura.SelectedValue = Val(campodb(TabellaRigheExtra.Rows(e.Row.RowIndex).Item(6).ToString))

            

            Call EseguiJS()

        End If
    End Sub

    Protected Sub Grd_RigheAggiuntive_RowDeleted(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewDeletedEventArgs) Handles Grd_RigheAggiuntive.RowDeleted

    End Sub

    Protected Sub Grd_RigheAggiuntive_RowDeleting(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewDeleteEventArgs) Handles Grd_RigheAggiuntive.RowDeleting

        UpDateTableRigheAggiuntive()
        
        TabellaRigheExtra = ViewState("App_TabellaRigheExtra")
        Try
            
            TabellaRigheExtra.Rows.RemoveAt(e.RowIndex)
            If TabellaRigheExtra.Rows.Count = 0 Then
                Dim myriga As System.Data.DataRow = TabellaRigheExtra.NewRow()
                myriga(1) = 0
                myriga(2) = 0
                TabellaRigheExtra.Rows.Add(myriga)
            End If
        Catch ex As Exception

        End Try


        ViewState("App_TabellaRigheExtra") = TabellaRigheExtra


        Grd_RigheAggiuntive.AutoGenerateColumns = False
        Grd_RigheAggiuntive.DataSource = TabellaRigheExtra
        Grd_RigheAggiuntive.DataBind()
        Call EseguiJS()

        e.Cancel = True
    End Sub
End Class
