﻿<%@ Page Language="VB" AutoEventWireup="false" Inherits="Appalti_Menu_Appalti" CodeFile="Menu_Appalti.aspx.vb" %>


<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
    <title>Menu Commesse</title>
    <asp:PlaceHolder runat="server">
        <%: System.Web.Optimization.Styles.Render("~/Content/AjaxControlToolkit/Styles/Bundle") %>
    </asp:PlaceHolder>
    <link rel="stylesheet" href="ospiti.css?ver=11" type="text/css" />
    <link rel="shortcut icon" href="images/SENIOR.ico" />
    <link href="js/jquery.autocomplete.css" rel="stylesheet" type="text/css" />

    <script src="js/jquery-1.5.1.min.js" type="text/javascript"></script>
    <script src="js/jquery.autocomplete.js?ver=1" type="text/javascript"></script>
    <script src="js/jquery.maskedinput-1.3.min.js" type="text/javascript"></script>
    <script src="js/NoEnter.js" type="text/javascript"></script>
    <script type="text/javascript">
        $(document).ready(function () {
            if (window.innerHeight > 0) { $("#BarraLaterale").css("height", (window.innerHeight - 94) + "px"); } else { $("#BarraLaterale").css("height", (document.documentElement.offsetHeight - 94) + "px"); }
        });
        function chiudinews() {

            $("#news").css('visibility', 'hidden');
        }
    </script>
    <style>
        #news {
            border: solid 1px #2d2d2d;
            text-align: left;
            vertical-align: top;
            background: white;
            width: 400px;
            height: 470px;
            position: absolute;
            -moz-border-radius: 5px;
            -webkit-border-radius: 5px;
            border-radius: 5px;
            box-shadow: 0px 0px 10px 4px rgba(0, 125, 196, 0.75);
            -moz-box-shadow: 0px 0px 10px 4px rgba(0, 125, 196, 0.75);
            -webkit-box-shadow: 0px 0px 10px 4px rgba(0, 125, 196, 0.75);
            z-index: 133;
        }
    </style>
</head>
<body>
    <form id="form1" runat="server">
        <div>
            <asp:Label ID="Lbl_BarraSenior" runat="server" Text=""></asp:Label>
            <table style="width: 100%;" cellpadding="0" cellspacing="0">
                <tr>
                    <td style="width: 140px; background-color: #F0F0F0; text-align: center;"></td>
                    <td>
                        <div class="Titolo">Appalti - Principale</div>
                        <div class="SottoTitolo">
                            <br />
                        </div>
                    </td>

                </tr>

                <tr>

                    <td style="width: 140px; background-color: #F0F0F0; text-align: center; vertical-align: top;" id="BarraLaterale">
                        <asp:ImageButton ID="ImageButton1" runat="server" BackColor="Transparent" ImageUrl="../images/Menu_Indietro.png" ToolTip="Chiudi" />

                    </td>
                    <td colspan="2" style="vertical-align: top;">
                        <table style="width: 900px;">

                            <tr>

                                <td style="text-align: center; width: 150px;">
                                    <a href="Elenco_Appalti.aspx">
                                        <img src="../images/Menu_Appalti.jpg" class="Effetto" style="border-width: 0;"></a></td>

                                <td style="text-align: center; width: 150px;">
                                    <a href="Elenco_Struttura.aspx">
                                        <img src="../images/Menu_Villa5.jpg" class="Effetto" style="border-width: 0;"></a></td>

                                <td style="text-align: center; width: 150px;">
                                    <a href="Elenco_StruttureMansioni.aspx">
                                        <img src="../images/Menu_Appalti.jpg" class="Effetto" style="border-width: 0;"></a></td>
                                <td style="text-align: center; width: 150px;">
                                    <a href="Elenco_Prestazioni.aspx">
                                        <img src="../images/Menu_ModificaProfili.png" alt="Prestazione" class="Effetto" style="border-width: 0;"></a>
                                </td>

                                <td style="text-align: center; width: 150px;">
                                    <a href="SinotticoPrestazioni.aspx">
                                        <img src="../images/Menu_ModificaProfili.png" alt="Sinottico Prestazioni" class="Effetto" style="border-width: 0;"></a>
                                </td>


                                <td style="text-align: center; width: 150px;">
                                    <a href="StampaPrestazioniOperatore.aspx">
                                        <img alt="Fatture" class="Effetto" src="../images/Menu_StampaDocumenti.png" style="border-width: 0;"></a>

                                </td>

                                <td style="text-align: center; width: 150px;">
                                    <a href="StampaPrestazioniPaziente.aspx">
                                        <img alt="Fatture" class="Effetto" src="../images/Menu_StampaDocumenti.png" style="border-width: 0;"></a>

                                </td>

                            </tr>
                            <tr>
                                <td style="text-align: center; vertical-align: top;"><span class="MenuText">COMMESSE</span></td>
                                <td style="text-align: center; vertical-align: top;"><span class="MenuText">STRUTTURE</span></td>
                                <td style="text-align: center; vertical-align: top;"><span class="MenuText">STRUTTURE MANSIONI</span></td>
                                <td style="text-align: center; vertical-align: top;"><span class="MenuText">PRESTAZIONI</span></td>
                                <td style="text-align: center; vertical-align: top;"><span class="MenuText">PROSPETTO PRESTAZIONI</span></td>

                                <td style="text-align: center; vertical-align: top;"><span class="MenuText">PRESTAZIONI OPERATORI</span></td>
                                <td style="text-align: center; vertical-align: top;"><span class="MenuText">PRESTAZIONI UTENTI</span></td>


                            </tr>


                            <tr>
                                <td style="text-align: center; width: 150px;"></td>
                                <td style="text-align: center; width: 150px;"></td>
                                <td style="text-align: center; width: 150px;"></td>
                                <td style="text-align: center; width: 150px;"></td>
                                <td style="text-align: center; width: 150px;"></td>
                                <td style="text-align: center; width: 150px;"></td>
                            </tr>

                            <tr>
                                <td style="text-align: center; width: 150px;"></td>
                                <td style="text-align: center; width: 150px;"></td>
                                <td style="text-align: center; width: 150px;"></td>
                                <td style="text-align: center; width: 150px;"></td>
                                <td style="text-align: center; width: 150px;"></td>
                                <td style="text-align: center; width: 150px;"></td>
                            </tr>

                            <tr>
                                <td style="text-align: center; width: 150px;">
                                    <a href="Elenco_Operatore.aspx">
                                        <img src="../images/Menu_Ospiti.jpg" class="Effetto" style="border-width: 0;"></a>
                                </td>
                                <td style="text-align: center; width: 150px;">
                                    <a href="Elenco_Anagraficautenti.aspx">
                                        <img src="../images/Menu_Ospiti.jpg" class="Effetto" style="border-width: 0;"></a>
                                </td>
                                <td style="text-align: center; width: 150px;">
                                    <a href="ElencoRegione.aspx">
                                        <img src="../images/Menu_Ospiti.jpg" class="Effetto" style="border-width: 0;"></a>
                                </td>
                                <td style="text-align: center; width: 150px;">
                                    <a href="VerificaAppalti.aspx">
                                        <img src="../images/bottonestatistica.jpg" class="Effetto" style="border-width: 0;"></a>
                                </td>
                                <td style="text-align: center; width: 150px;"></td>
                                <td style="text-align: center; width: 150px;"></td>
                                <td style="text-align: center; width: 150px;">
                                    <a href="Menu_Tabelle.aspx">
                                        <img src="../images/Menu_Tabelle.png" class="Effetto" style="border-width: 0;"></a>
                                </td>
                            </tr>

                            <tr>
                                <td style="text-align: center; vertical-align: top;"><span class="MenuText">OPERATORI</span></td>
                                <td style="text-align: center; width: 150px;"><span class="MenuText">UTENTI</span></td>
                                <td style="text-align: center; width: 150px;"><span class="MenuText">COMMITTENTI</span></td>
                                <td style="text-align: center; width: 150px;"><span class="MenuText">VISUALIZZA COMMESSE </span>
                                </td>
                                <td style="text-align: center; width: 150px;"><span class="MenuText"></span>
                                </td>
                                <td style="text-align: center; width: 150px;"><span class="MenuText"></span>
                                </td>
                                <td style="text-align: center; width: 150px;"><span class="MenuText">TABELLE</span>
                                </td>
                            </tr>



                            <tr>
                                <td style="text-align: center; width: 150px;"></td>
                                <td style="text-align: center; width: 150px;"></td>
                                <td style="text-align: center; width: 150px;"></td>
                                <td style="text-align: center; width: 150px;"></td>
                                <td style="text-align: center; width: 150px;"></td>
                                <td style="text-align: center; width: 150px;"></td>
                            </tr>

                            <tr>
                                <td style="text-align: center; width: 150px;"></td>
                                <td style="text-align: center; width: 150px;"></td>
                                <td style="text-align: center; width: 150px;"></td>
                                <td style="text-align: center; width: 150px;"></td>
                                <td style="text-align: center; width: 150px;"></td>
                                <td style="text-align: center; width: 150px;"></td>
                            </tr>

                            <tr>
                                <td style="text-align: center; width: 150px;">
                                    <a href="Elabora.aspx">
                                        <img src="../images/Menu_Riclassificato.png" alt="Comuni" class="Effetto" style="border-width: 0;"></a>
                                </td>

                                <td style="text-align: center; width: 150px;">
                                    <a href="StampaAllegati.aspx">
                                        <img src="../images/Menu_Movimenti.png" alt="Comuni" class="Effetto" style="border-width: 0;"></a>

                                </td>
                                <td style="text-align: center; width: 150px;">
                                    <a href="StampaDocumenti.aspx">
                                        <img alt="Fatture" class="Effetto" src="../images/Menu_StampaDocumenti.png" style="border-width: 0;"></a>

                                </td>
                                <td style="text-align: center; width: 150px;"></td>
                                <td style="text-align: center; width: 150px;"></td>

                                <td style="text-align: center; width: 150px;"></td>

                                <td style="text-align: center; width: 150px;">
                                    <a href="Menu_ImportExport.aspx">
                                        <img alt="EXPORT" class="Effetto" src="../images/Menu_Export_1.png" style="border-width: 0;"></a></td>
                            </tr>

                            <tr>
                                <td style="text-align: center; vertical-align: top;"><span class="MenuText">ELABORA</span></td>
                                <td style="text-align: center; vertical-align: top;"><span class="MenuText">ALLEGATI</span></td>
                                <td style="text-align: center; vertical-align: top;"><span class="MenuText">STAMPA DOC.</span></td>
                                <td style="text-align: center; vertical-align: top;"><span class="MenuText"></span></td>
                                <td style="text-align: center; vertical-align: top;"><span class="MenuText"></span></td>
                                <td style="text-align: center; vertical-align: top;"><span class="MenuText"></span></td>
                                <td style="text-align: center; vertical-align: top;"><span class="MenuText">IMP.EXP.</span></td>
                            </tr>



                        </table>
                    </td>
                </tr>

            </table>


        </div>


    </form>
</body>
</html>
