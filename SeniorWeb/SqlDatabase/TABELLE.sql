IF NOT EXISTS(SELECT name FROM sys.sysobjects  WHERE Name = N'CausaliCassa' AND xtype = N'U')
BEGIN
CREATE TABLE CausaliCassa([ID] [int] IDENTITY(1,1) NOT NULL) ON [PRIMARY];
END

IF NOT EXISTS(SELECT 1 FROM sys.columns WHERE Name = N'Utente' AND Object_ID = Object_ID(N'CausaliCassa'))
BEGIN
ALTER TABLE CausaliCassa ADD [Utente] nvarchar (50);
END

IF NOT EXISTS(SELECT 1 FROM sys.columns WHERE Name = N'DataAggiornamento' AND Object_ID = Object_ID(N'CausaliCassa'))
BEGIN
ALTER TABLE CausaliCassa ADD [DataAggiornamento] datetime;
END

IF NOT EXISTS(SELECT 1 FROM sys.columns WHERE Name = N'Codice' AND Object_ID = Object_ID(N'CausaliCassa'))
BEGIN
ALTER TABLE CausaliCassa ADD [Codice] nvarchar (2);
END

IF NOT EXISTS(SELECT 1 FROM sys.columns WHERE Name = N'Descrizione' AND Object_ID = Object_ID(N'CausaliCassa'))
BEGIN
ALTER TABLE CausaliCassa ADD [Descrizione] nvarchar (50);
END

IF NOT EXISTS(SELECT 1 FROM sys.columns WHERE Name = N'EU' AND Object_ID = Object_ID(N'CausaliCassa'))
BEGIN
ALTER TABLE CausaliCassa ADD [EU] nvarchar (1);
END

IF NOT EXISTS(SELECT 1 FROM sys.columns WHERE Name = N'Corrispettivo' AND Object_ID = Object_ID(N'CausaliCassa'))
BEGIN
ALTER TABLE CausaliCassa ADD [Corrispettivo] nvarchar (1);
END

IF NOT EXISTS(SELECT 1 FROM sys.columns WHERE Name = N'CodiceIVA' AND Object_ID = Object_ID(N'CausaliCassa'))
BEGIN
ALTER TABLE CausaliCassa ADD [CodiceIVA] nvarchar (2);
END

IF NOT EXISTS(SELECT 1 FROM sys.columns WHERE Name = N'CausaleContabile' AND Object_ID = Object_ID(N'CausaliCassa'))
BEGIN
ALTER TABLE CausaliCassa ADD [CausaleContabile] nvarchar (3);
END

IF NOT EXISTS(SELECT 1 FROM sys.columns WHERE Name = N'Capitolo' AND Object_ID = Object_ID(N'CausaliCassa'))
BEGIN
ALTER TABLE CausaliCassa ADD [Capitolo] smallint;
END

IF NOT EXISTS(SELECT 1 FROM sys.columns WHERE Name = N'Articolo' AND Object_ID = Object_ID(N'CausaliCassa'))
BEGIN
ALTER TABLE CausaliCassa ADD [Articolo] smallint;
END

IF NOT EXISTS(SELECT 1 FROM sys.columns WHERE Name = N'TipoDocumento' AND Object_ID = Object_ID(N'CausaliCassa'))
BEGIN
ALTER TABLE CausaliCassa ADD [TipoDocumento] nvarchar (2);
END

IF NOT EXISTS(SELECT 1 FROM sys.columns WHERE Name = N'CAS-TIPO-ST' AND Object_ID = Object_ID(N'CausaliCassa'))
BEGIN
ALTER TABLE CausaliCassa ADD [CAS-TIPO-ST] nvarchar (50);
END

IF NOT EXISTS(SELECT 1 FROM sys.columns WHERE Name = N'CAS-STP-DOC' AND Object_ID = Object_ID(N'CausaliCassa'))
BEGIN
ALTER TABLE CausaliCassa ADD [CAS-STP-DOC] nvarchar (50);
END

IF NOT EXISTS(SELECT 1 FROM sys.columns WHERE Name = N'Bollo' AND Object_ID = Object_ID(N'CausaliCassa'))
BEGIN
ALTER TABLE CausaliCassa ADD [Bollo] nvarchar (1);
END

IF NOT EXISTS(SELECT 1 FROM sys.columns WHERE Name = N'CAP' AND Object_ID = Object_ID(N'CausaliCassa'))
BEGIN
ALTER TABLE CausaliCassa ADD [CAP] nvarchar (1);
END

IF NOT EXISTS(SELECT 1 FROM sys.columns WHERE Name = N'DebitoreCreditore' AND Object_ID = Object_ID(N'CausaliCassa'))
BEGIN
ALTER TABLE CausaliCassa ADD [DebitoreCreditore] nvarchar (1);
END

IF NOT EXISTS(SELECT 1 FROM sys.columns WHERE Name = N'CAS-RIDISP' AND Object_ID = Object_ID(N'CausaliCassa'))
BEGIN
ALTER TABLE CausaliCassa ADD [CAS-RIDISP] nvarchar (50);
END

IF NOT EXISTS(SELECT 1 FROM sys.columns WHERE Name = N'PassaggioContabilita' AND Object_ID = Object_ID(N'CausaliCassa'))
BEGIN
ALTER TABLE CausaliCassa ADD [PassaggioContabilita] nvarchar (1);
END

IF NOT EXISTS(SELECT 1 FROM sys.columns WHERE Name = N'CodiceDescrizione' AND Object_ID = Object_ID(N'CausaliCassa'))
BEGIN
ALTER TABLE CausaliCassa ADD [CodiceDescrizione] nvarchar (3);
END

IF NOT EXISTS(SELECT name FROM sys.sysobjects  WHERE Name = N'CausaliContabiliRiga' AND xtype = N'U')
BEGIN
CREATE TABLE CausaliContabiliRiga([ID] [int] IDENTITY(1,1) NOT NULL) ON [PRIMARY];
END

IF NOT EXISTS(SELECT 1 FROM sys.columns WHERE Name = N'Utente' AND Object_ID = Object_ID(N'CausaliContabiliRiga'))
BEGIN
ALTER TABLE CausaliContabiliRiga ADD [Utente] nvarchar (50);
END

IF NOT EXISTS(SELECT 1 FROM sys.columns WHERE Name = N'DataAggiornamento' AND Object_ID = Object_ID(N'CausaliContabiliRiga'))
BEGIN
ALTER TABLE CausaliContabiliRiga ADD [DataAggiornamento] datetime;
END

IF NOT EXISTS(SELECT 1 FROM sys.columns WHERE Name = N'Codice' AND Object_ID = Object_ID(N'CausaliContabiliRiga'))
BEGIN
ALTER TABLE CausaliContabiliRiga ADD [Codice] nvarchar (3);
END

IF NOT EXISTS(SELECT 1 FROM sys.columns WHERE Name = N'Riga' AND Object_ID = Object_ID(N'CausaliContabiliRiga'))
BEGIN
ALTER TABLE CausaliContabiliRiga ADD [Riga] smallint;
END

IF NOT EXISTS(SELECT 1 FROM sys.columns WHERE Name = N'Mastro' AND Object_ID = Object_ID(N'CausaliContabiliRiga'))
BEGIN
ALTER TABLE CausaliContabiliRiga ADD [Mastro] smallint;
END

IF NOT EXISTS(SELECT 1 FROM sys.columns WHERE Name = N'Conto' AND Object_ID = Object_ID(N'CausaliContabiliRiga'))
BEGIN
ALTER TABLE CausaliContabiliRiga ADD [Conto] smallint;
END

IF NOT EXISTS(SELECT 1 FROM sys.columns WHERE Name = N'Sottoconto' AND Object_ID = Object_ID(N'CausaliContabiliRiga'))
BEGIN
ALTER TABLE CausaliContabiliRiga ADD [Sottoconto] float;
END

IF NOT EXISTS(SELECT 1 FROM sys.columns WHERE Name = N'Segno' AND Object_ID = Object_ID(N'CausaliContabiliRiga'))
BEGIN
ALTER TABLE CausaliContabiliRiga ADD [Segno] nvarchar (1);
END

IF NOT EXISTS(SELECT 1 FROM sys.columns WHERE Name = N'DareAvere' AND Object_ID = Object_ID(N'CausaliContabiliRiga'))
BEGIN
ALTER TABLE CausaliContabiliRiga ADD [DareAvere] nvarchar (1);
END

IF NOT EXISTS(SELECT name FROM sys.sysobjects  WHERE Name = N'CausaliContabiliTesta' AND xtype = N'U')
BEGIN
CREATE TABLE CausaliContabiliTesta([ID] [int] IDENTITY(1,1) NOT NULL) ON [PRIMARY];
END

IF NOT EXISTS(SELECT 1 FROM sys.columns WHERE Name = N'Utente' AND Object_ID = Object_ID(N'CausaliContabiliTesta'))
BEGIN
ALTER TABLE CausaliContabiliTesta ADD [Utente] nvarchar (50);
END

IF NOT EXISTS(SELECT 1 FROM sys.columns WHERE Name = N'DataAggiornamento' AND Object_ID = Object_ID(N'CausaliContabiliTesta'))
BEGIN
ALTER TABLE CausaliContabiliTesta ADD [DataAggiornamento] datetime;
END

IF NOT EXISTS(SELECT 1 FROM sys.columns WHERE Name = N'Codice' AND Object_ID = Object_ID(N'CausaliContabiliTesta'))
BEGIN
ALTER TABLE CausaliContabiliTesta ADD [Codice] nvarchar (3);
END

IF NOT EXISTS(SELECT 1 FROM sys.columns WHERE Name = N'Tipo' AND Object_ID = Object_ID(N'CausaliContabiliTesta'))
BEGIN
ALTER TABLE CausaliContabiliTesta ADD [Tipo] nvarchar (1);
END

IF NOT EXISTS(SELECT 1 FROM sys.columns WHERE Name = N'Descrizione' AND Object_ID = Object_ID(N'CausaliContabiliTesta'))
BEGIN
ALTER TABLE CausaliContabiliTesta ADD [Descrizione] nvarchar (50);
END

IF NOT EXISTS(SELECT 1 FROM sys.columns WHERE Name = N'RegistroIVA' AND Object_ID = Object_ID(N'CausaliContabiliTesta'))
BEGIN
ALTER TABLE CausaliContabiliTesta ADD [RegistroIVA] tinyint;
END

IF NOT EXISTS(SELECT 1 FROM sys.columns WHERE Name = N'TipoDocumento' AND Object_ID = Object_ID(N'CausaliContabiliTesta'))
BEGIN
ALTER TABLE CausaliContabiliTesta ADD [TipoDocumento] nvarchar (2);
END

IF NOT EXISTS(SELECT 1 FROM sys.columns WHERE Name = N'DataObbligatoria' AND Object_ID = Object_ID(N'CausaliContabiliTesta'))
BEGIN
ALTER TABLE CausaliContabiliTesta ADD [DataObbligatoria] nvarchar (1);
END

IF NOT EXISTS(SELECT 1 FROM sys.columns WHERE Name = N'NumeroObbligatorio' AND Object_ID = Object_ID(N'CausaliContabiliTesta'))
BEGIN
ALTER TABLE CausaliContabiliTesta ADD [NumeroObbligatorio] nvarchar (1);
END

IF NOT EXISTS(SELECT 1 FROM sys.columns WHERE Name = N'CausaleIncasso' AND Object_ID = Object_ID(N'CausaliContabiliTesta'))
BEGIN
ALTER TABLE CausaliContabiliTesta ADD [CausaleIncasso] nvarchar (3);
END

IF NOT EXISTS(SELECT 1 FROM sys.columns WHERE Name = N'AllegatoFineAnno' AND Object_ID = Object_ID(N'CausaliContabiliTesta'))
BEGIN
ALTER TABLE CausaliContabiliTesta ADD [AllegatoFineAnno] nvarchar (1);
END

IF NOT EXISTS(SELECT 1 FROM sys.columns WHERE Name = N'VenditaAcquisti' AND Object_ID = Object_ID(N'CausaliContabiliTesta'))
BEGIN
ALTER TABLE CausaliContabiliTesta ADD [VenditaAcquisti] nvarchar (1);
END

IF NOT EXISTS(SELECT 1 FROM sys.columns WHERE Name = N'CodiceIva' AND Object_ID = Object_ID(N'CausaliContabiliTesta'))
BEGIN
ALTER TABLE CausaliContabiliTesta ADD [CodiceIva] nvarchar (3);
END

IF NOT EXISTS(SELECT 1 FROM sys.columns WHERE Name = N'Detraibilita' AND Object_ID = Object_ID(N'CausaliContabiliTesta'))
BEGIN
ALTER TABLE CausaliContabiliTesta ADD [Detraibilita] nvarchar (3);
END

IF NOT EXISTS(SELECT 1 FROM sys.columns WHERE Name = N'Prorata' AND Object_ID = Object_ID(N'CausaliContabiliTesta'))
BEGIN
ALTER TABLE CausaliContabiliTesta ADD [Prorata] nvarchar (1);
END

IF NOT EXISTS(SELECT 1 FROM sys.columns WHERE Name = N'CentroServizio' AND Object_ID = Object_ID(N'CausaliContabiliTesta'))
BEGIN
ALTER TABLE CausaliContabiliTesta ADD [CentroServizio] nvarchar (4);
END

IF NOT EXISTS(SELECT 1 FROM sys.columns WHERE Name = N'Report' AND Object_ID = Object_ID(N'CausaliContabiliTesta'))
BEGIN
ALTER TABLE CausaliContabiliTesta ADD [Report] nvarchar (255);
END

IF NOT EXISTS(SELECT 1 FROM sys.columns WHERE Name = N'CodiceCIG' AND Object_ID = Object_ID(N'CausaliContabiliTesta'))
BEGIN
ALTER TABLE CausaliContabiliTesta ADD [CodiceCIG] int;
END

IF NOT EXISTS(SELECT 1 FROM sys.columns WHERE Name = N'Ritenuta' AND Object_ID = Object_ID(N'CausaliContabiliTesta'))
BEGIN
ALTER TABLE CausaliContabiliTesta ADD [Ritenuta] varchar (2);
END

IF NOT EXISTS(SELECT 1 FROM sys.columns WHERE Name = N'DocumentoReverse' AND Object_ID = Object_ID(N'CausaliContabiliTesta'))
BEGIN
ALTER TABLE CausaliContabiliTesta ADD [DocumentoReverse] varchar (3);
END

IF NOT EXISTS(SELECT 1 FROM sys.columns WHERE Name = N'GirocontoReverse' AND Object_ID = Object_ID(N'CausaliContabiliTesta'))
BEGIN
ALTER TABLE CausaliContabiliTesta ADD [GirocontoReverse] varchar (3);
END

IF NOT EXISTS(SELECT 1 FROM sys.columns WHERE Name = N'CodiceSede' AND Object_ID = Object_ID(N'CausaliContabiliTesta'))
BEGIN
ALTER TABLE CausaliContabiliTesta ADD [CodiceSede] varchar (20);
END

IF NOT EXISTS(SELECT 1 FROM sys.columns WHERE Name = N'Registro' AND Object_ID = Object_ID(N'CausaliContabiliTesta'))
BEGIN
ALTER TABLE CausaliContabiliTesta ADD [Registro] varchar (20);
END

IF NOT EXISTS(SELECT name FROM sys.sysobjects  WHERE Name = N'CausaliDescrittive' AND xtype = N'U')
BEGIN
CREATE TABLE CausaliDescrittive([ID] [int] IDENTITY(1,1) NOT NULL) ON [PRIMARY];
END

IF NOT EXISTS(SELECT 1 FROM sys.columns WHERE Name = N'Utente' AND Object_ID = Object_ID(N'CausaliDescrittive'))
BEGIN
ALTER TABLE CausaliDescrittive ADD [Utente] nvarchar (50);
END

IF NOT EXISTS(SELECT 1 FROM sys.columns WHERE Name = N'DataAggiornamento' AND Object_ID = Object_ID(N'CausaliDescrittive'))
BEGIN
ALTER TABLE CausaliDescrittive ADD [DataAggiornamento] datetime;
END

IF NOT EXISTS(SELECT 1 FROM sys.columns WHERE Name = N'Codice' AND Object_ID = Object_ID(N'CausaliDescrittive'))
BEGIN
ALTER TABLE CausaliDescrittive ADD [Codice] nvarchar (4);
END

IF NOT EXISTS(SELECT 1 FROM sys.columns WHERE Name = N'Descrizione' AND Object_ID = Object_ID(N'CausaliDescrittive'))
BEGIN
ALTER TABLE CausaliDescrittive ADD [Descrizione] nvarchar (50);
END

IF NOT EXISTS(SELECT name FROM sys.sysobjects  WHERE Name = N'CausaliMagazzino' AND xtype = N'U')
BEGIN
CREATE TABLE CausaliMagazzino([ID] [int] IDENTITY(1,1) NOT NULL) ON [PRIMARY];
END

IF NOT EXISTS(SELECT 1 FROM sys.columns WHERE Name = N'Utente' AND Object_ID = Object_ID(N'CausaliMagazzino'))
BEGIN
ALTER TABLE CausaliMagazzino ADD [Utente] nvarchar (50);
END

IF NOT EXISTS(SELECT 1 FROM sys.columns WHERE Name = N'DataAggiornamento' AND Object_ID = Object_ID(N'CausaliMagazzino'))
BEGIN
ALTER TABLE CausaliMagazzino ADD [DataAggiornamento] datetime;
END

IF NOT EXISTS(SELECT 1 FROM sys.columns WHERE Name = N'Codice' AND Object_ID = Object_ID(N'CausaliMagazzino'))
BEGIN
ALTER TABLE CausaliMagazzino ADD [Codice] nvarchar (3);
END

IF NOT EXISTS(SELECT 1 FROM sys.columns WHERE Name = N'Descrizione' AND Object_ID = Object_ID(N'CausaliMagazzino'))
BEGIN
ALTER TABLE CausaliMagazzino ADD [Descrizione] nvarchar (50);
END

IF NOT EXISTS(SELECT 1 FROM sys.columns WHERE Name = N'Tipo' AND Object_ID = Object_ID(N'CausaliMagazzino'))
BEGIN
ALTER TABLE CausaliMagazzino ADD [Tipo] nvarchar (1);
END

IF NOT EXISTS(SELECT 1 FROM sys.columns WHERE Name = N'Deposito' AND Object_ID = Object_ID(N'CausaliMagazzino'))
BEGIN
ALTER TABLE CausaliMagazzino ADD [Deposito] nvarchar (2);
END

IF NOT EXISTS(SELECT 1 FROM sys.columns WHERE Name = N'AQuantita' AND Object_ID = Object_ID(N'CausaliMagazzino'))
BEGIN
ALTER TABLE CausaliMagazzino ADD [AQuantita] nvarchar (1);
END

IF NOT EXISTS(SELECT 1 FROM sys.columns WHERE Name = N'AValore' AND Object_ID = Object_ID(N'CausaliMagazzino'))
BEGIN
ALTER TABLE CausaliMagazzino ADD [AValore] nvarchar (1);
END

IF NOT EXISTS(SELECT 1 FROM sys.columns WHERE Name = N'VariaPrezzo' AND Object_ID = Object_ID(N'CausaliMagazzino'))
BEGIN
ALTER TABLE CausaliMagazzino ADD [VariaPrezzo] nvarchar (1);
END

IF NOT EXISTS(SELECT name FROM sys.sysobjects  WHERE Name = N'ChiusuraContabile' AND xtype = N'U')
BEGIN
CREATE TABLE ChiusuraContabile([ID] [int] IDENTITY(1,1) NOT NULL) ON [PRIMARY];
END

IF NOT EXISTS(SELECT 1 FROM sys.columns WHERE Name = N'Utente' AND Object_ID = Object_ID(N'ChiusuraContabile'))
BEGIN
ALTER TABLE ChiusuraContabile ADD [Utente] nvarchar (50);
END

IF NOT EXISTS(SELECT 1 FROM sys.columns WHERE Name = N'DataAggiornamento' AND Object_ID = Object_ID(N'ChiusuraContabile'))
BEGIN
ALTER TABLE ChiusuraContabile ADD [DataAggiornamento] datetime;
END

IF NOT EXISTS(SELECT 1 FROM sys.columns WHERE Name = N'Anno' AND Object_ID = Object_ID(N'ChiusuraContabile'))
BEGIN
ALTER TABLE ChiusuraContabile ADD [Anno] smallint;
END

IF NOT EXISTS(SELECT 1 FROM sys.columns WHERE Name = N'Tipo' AND Object_ID = Object_ID(N'ChiusuraContabile'))
BEGIN
ALTER TABLE ChiusuraContabile ADD [Tipo] nvarchar (1);
END

IF NOT EXISTS(SELECT 1 FROM sys.columns WHERE Name = N'DataClienti' AND Object_ID = Object_ID(N'ChiusuraContabile'))
BEGIN
ALTER TABLE ChiusuraContabile ADD [DataClienti] datetime;
END

IF NOT EXISTS(SELECT 1 FROM sys.columns WHERE Name = N'DataFornitori' AND Object_ID = Object_ID(N'ChiusuraContabile'))
BEGIN
ALTER TABLE ChiusuraContabile ADD [DataFornitori] datetime;
END

IF NOT EXISTS(SELECT 1 FROM sys.columns WHERE Name = N'DataEconomica' AND Object_ID = Object_ID(N'ChiusuraContabile'))
BEGIN
ALTER TABLE ChiusuraContabile ADD [DataEconomica] datetime;
END

IF NOT EXISTS(SELECT 1 FROM sys.columns WHERE Name = N'DataPatrimoniale' AND Object_ID = Object_ID(N'ChiusuraContabile'))
BEGIN
ALTER TABLE ChiusuraContabile ADD [DataPatrimoniale] datetime;
END

IF NOT EXISTS(SELECT name FROM sys.sysobjects  WHERE Name = N'DatiGenerali' AND xtype = N'U')
BEGIN
CREATE TABLE DatiGenerali([ID] [int] IDENTITY(1,1) NOT NULL) ON [PRIMARY];
END

IF NOT EXISTS(SELECT 1 FROM sys.columns WHERE Name = N'Utente' AND Object_ID = Object_ID(N'DatiGenerali'))
BEGIN
ALTER TABLE DatiGenerali ADD [Utente] nvarchar (50);
END

IF NOT EXISTS(SELECT 1 FROM sys.columns WHERE Name = N'DataAggiornamento' AND Object_ID = Object_ID(N'DatiGenerali'))
BEGIN
ALTER TABLE DatiGenerali ADD [DataAggiornamento] datetime;
END

IF NOT EXISTS(SELECT 1 FROM sys.columns WHERE Name = N'EsercizioMese' AND Object_ID = Object_ID(N'DatiGenerali'))
BEGIN
ALTER TABLE DatiGenerali ADD [EsercizioMese] tinyint;
END

IF NOT EXISTS(SELECT 1 FROM sys.columns WHERE Name = N'EsercizioGiorno' AND Object_ID = Object_ID(N'DatiGenerali'))
BEGIN
ALTER TABLE DatiGenerali ADD [EsercizioGiorno] tinyint;
END

IF NOT EXISTS(SELECT 1 FROM sys.columns WHERE Name = N'ApprovazioneBilancio' AND Object_ID = Object_ID(N'DatiGenerali'))
BEGIN
ALTER TABLE DatiGenerali ADD [ApprovazioneBilancio] datetime;
END

IF NOT EXISTS(SELECT 1 FROM sys.columns WHERE Name = N'CausaleChiusura' AND Object_ID = Object_ID(N'DatiGenerali'))
BEGIN
ALTER TABLE DatiGenerali ADD [CausaleChiusura] nvarchar (3);
END

IF NOT EXISTS(SELECT 1 FROM sys.columns WHERE Name = N'CausaleApertura' AND Object_ID = Object_ID(N'DatiGenerali'))
BEGIN
ALTER TABLE DatiGenerali ADD [CausaleApertura] nvarchar (3);
END

IF NOT EXISTS(SELECT 1 FROM sys.columns WHERE Name = N'BilancioChiusuraMastro' AND Object_ID = Object_ID(N'DatiGenerali'))
BEGIN
ALTER TABLE DatiGenerali ADD [BilancioChiusuraMastro] smallint;
END

IF NOT EXISTS(SELECT 1 FROM sys.columns WHERE Name = N'BilancioChiusuraConto' AND Object_ID = Object_ID(N'DatiGenerali'))
BEGIN
ALTER TABLE DatiGenerali ADD [BilancioChiusuraConto] smallint;
END

IF NOT EXISTS(SELECT 1 FROM sys.columns WHERE Name = N'BilancioChiusuraSottoconto' AND Object_ID = Object_ID(N'DatiGenerali'))
BEGIN
ALTER TABLE DatiGenerali ADD [BilancioChiusuraSottoconto] float;
END

IF NOT EXISTS(SELECT 1 FROM sys.columns WHERE Name = N'ProfittiPerditeMastro' AND Object_ID = Object_ID(N'DatiGenerali'))
BEGIN
ALTER TABLE DatiGenerali ADD [ProfittiPerditeMastro] smallint;
END

IF NOT EXISTS(SELECT 1 FROM sys.columns WHERE Name = N'ProfittiPerditeConto' AND Object_ID = Object_ID(N'DatiGenerali'))
BEGIN
ALTER TABLE DatiGenerali ADD [ProfittiPerditeConto] smallint;
END

IF NOT EXISTS(SELECT 1 FROM sys.columns WHERE Name = N'ProfittiPerditeSottoconto' AND Object_ID = Object_ID(N'DatiGenerali'))
BEGIN
ALTER TABLE DatiGenerali ADD [ProfittiPerditeSottoconto] float;
END

IF NOT EXISTS(SELECT 1 FROM sys.columns WHERE Name = N'BilancioAperturaMastro' AND Object_ID = Object_ID(N'DatiGenerali'))
BEGIN
ALTER TABLE DatiGenerali ADD [BilancioAperturaMastro] smallint;
END

IF NOT EXISTS(SELECT 1 FROM sys.columns WHERE Name = N'BilancioAperturaConto' AND Object_ID = Object_ID(N'DatiGenerali'))
BEGIN
ALTER TABLE DatiGenerali ADD [BilancioAperturaConto] smallint;
END

IF NOT EXISTS(SELECT 1 FROM sys.columns WHERE Name = N'BilancioAperturaSottoconto' AND Object_ID = Object_ID(N'DatiGenerali'))
BEGIN
ALTER TABLE DatiGenerali ADD [BilancioAperturaSottoconto] float;
END

IF NOT EXISTS(SELECT 1 FROM sys.columns WHERE Name = N'UtilePatrimonialeMastro' AND Object_ID = Object_ID(N'DatiGenerali'))
BEGIN
ALTER TABLE DatiGenerali ADD [UtilePatrimonialeMastro] smallint;
END

IF NOT EXISTS(SELECT 1 FROM sys.columns WHERE Name = N'UtilePatrimonialeConto' AND Object_ID = Object_ID(N'DatiGenerali'))
BEGIN
ALTER TABLE DatiGenerali ADD [UtilePatrimonialeConto] smallint;
END

IF NOT EXISTS(SELECT 1 FROM sys.columns WHERE Name = N'UtilePatrimonialeSottoconto' AND Object_ID = Object_ID(N'DatiGenerali'))
BEGIN
ALTER TABLE DatiGenerali ADD [UtilePatrimonialeSottoconto] float;
END

IF NOT EXISTS(SELECT 1 FROM sys.columns WHERE Name = N'PerditaPatrimonialeMastro' AND Object_ID = Object_ID(N'DatiGenerali'))
BEGIN
ALTER TABLE DatiGenerali ADD [PerditaPatrimonialeMastro] smallint;
END

IF NOT EXISTS(SELECT 1 FROM sys.columns WHERE Name = N'PerditaPatrimonialeConto' AND Object_ID = Object_ID(N'DatiGenerali'))
BEGIN
ALTER TABLE DatiGenerali ADD [PerditaPatrimonialeConto] smallint;
END

IF NOT EXISTS(SELECT 1 FROM sys.columns WHERE Name = N'PerditaPatrimonialeSottoconto' AND Object_ID = Object_ID(N'DatiGenerali'))
BEGIN
ALTER TABLE DatiGenerali ADD [PerditaPatrimonialeSottoconto] float;
END

IF NOT EXISTS(SELECT 1 FROM sys.columns WHERE Name = N'UtileEconomicoMastro' AND Object_ID = Object_ID(N'DatiGenerali'))
BEGIN
ALTER TABLE DatiGenerali ADD [UtileEconomicoMastro] smallint;
END

IF NOT EXISTS(SELECT 1 FROM sys.columns WHERE Name = N'UtileEconomicoConto' AND Object_ID = Object_ID(N'DatiGenerali'))
BEGIN
ALTER TABLE DatiGenerali ADD [UtileEconomicoConto] smallint;
END

IF NOT EXISTS(SELECT 1 FROM sys.columns WHERE Name = N'UtileEconomicoSottoconto' AND Object_ID = Object_ID(N'DatiGenerali'))
BEGIN
ALTER TABLE DatiGenerali ADD [UtileEconomicoSottoconto] float;
END

IF NOT EXISTS(SELECT 1 FROM sys.columns WHERE Name = N'PerditaEconomicoMastro' AND Object_ID = Object_ID(N'DatiGenerali'))
BEGIN
ALTER TABLE DatiGenerali ADD [PerditaEconomicoMastro] smallint;
END

IF NOT EXISTS(SELECT 1 FROM sys.columns WHERE Name = N'PerditaEconomicoConto' AND Object_ID = Object_ID(N'DatiGenerali'))
BEGIN
ALTER TABLE DatiGenerali ADD [PerditaEconomicoConto] smallint;
END

IF NOT EXISTS(SELECT 1 FROM sys.columns WHERE Name = N'PerditaEconomicoSottoconto' AND Object_ID = Object_ID(N'DatiGenerali'))
BEGIN
ALTER TABLE DatiGenerali ADD [PerditaEconomicoSottoconto] float;
END

IF NOT EXISTS(SELECT 1 FROM sys.columns WHERE Name = N'CausaleRettifica' AND Object_ID = Object_ID(N'DatiGenerali'))
BEGIN
ALTER TABLE DatiGenerali ADD [CausaleRettifica] nvarchar (3);
END

IF NOT EXISTS(SELECT 1 FROM sys.columns WHERE Name = N'ArrotondaIVA' AND Object_ID = Object_ID(N'DatiGenerali'))
BEGIN
ALTER TABLE DatiGenerali ADD [ArrotondaIVA] nvarchar (1);
END

IF NOT EXISTS(SELECT 1 FROM sys.columns WHERE Name = N'ClientiMastro' AND Object_ID = Object_ID(N'DatiGenerali'))
BEGIN
ALTER TABLE DatiGenerali ADD [ClientiMastro] smallint;
END

IF NOT EXISTS(SELECT 1 FROM sys.columns WHERE Name = N'FornitoriMastro' AND Object_ID = Object_ID(N'DatiGenerali'))
BEGIN
ALTER TABLE DatiGenerali ADD [FornitoriMastro] smallint;
END

IF NOT EXISTS(SELECT 1 FROM sys.columns WHERE Name = N'IVAMastro' AND Object_ID = Object_ID(N'DatiGenerali'))
BEGIN
ALTER TABLE DatiGenerali ADD [IVAMastro] smallint;
END

IF NOT EXISTS(SELECT 1 FROM sys.columns WHERE Name = N'RegistroLiquidazioni' AND Object_ID = Object_ID(N'DatiGenerali'))
BEGIN
ALTER TABLE DatiGenerali ADD [RegistroLiquidazioni] tinyint;
END

IF NOT EXISTS(SELECT 1 FROM sys.columns WHERE Name = N'LiquidazioneTrimestrale' AND Object_ID = Object_ID(N'DatiGenerali'))
BEGIN
ALTER TABLE DatiGenerali ADD [LiquidazioneTrimestrale] nvarchar (1);
END

IF NOT EXISTS(SELECT 1 FROM sys.columns WHERE Name = N'CodiceDetraibilita' AND Object_ID = Object_ID(N'DatiGenerali'))
BEGIN
ALTER TABLE DatiGenerali ADD [CodiceDetraibilita] nvarchar (1);
END

IF NOT EXISTS(SELECT 1 FROM sys.columns WHERE Name = N'CodiceScadenza' AND Object_ID = Object_ID(N'DatiGenerali'))
BEGIN
ALTER TABLE DatiGenerali ADD [CodiceScadenza] nvarchar (1);
END

IF NOT EXISTS(SELECT 1 FROM sys.columns WHERE Name = N'CodiceRitenute' AND Object_ID = Object_ID(N'DatiGenerali'))
BEGIN
ALTER TABLE DatiGenerali ADD [CodiceRitenute] nvarchar (1);
END

IF NOT EXISTS(SELECT 1 FROM sys.columns WHERE Name = N'NumeroAutomaticoClienti' AND Object_ID = Object_ID(N'DatiGenerali'))
BEGIN
ALTER TABLE DatiGenerali ADD [NumeroAutomaticoClienti] nvarchar (1);
END

IF NOT EXISTS(SELECT 1 FROM sys.columns WHERE Name = N'ValoreEURO' AND Object_ID = Object_ID(N'DatiGenerali'))
BEGIN
ALTER TABLE DatiGenerali ADD [ValoreEURO] real;
END

IF NOT EXISTS(SELECT 1 FROM sys.columns WHERE Name = N'TipoApertura' AND Object_ID = Object_ID(N'DatiGenerali'))
BEGIN
ALTER TABLE DatiGenerali ADD [TipoApertura] nvarchar (1);
END

IF NOT EXISTS(SELECT 1 FROM sys.columns WHERE Name = N'EconomoMastro' AND Object_ID = Object_ID(N'DatiGenerali'))
BEGIN
ALTER TABLE DatiGenerali ADD [EconomoMastro] smallint;
END

IF NOT EXISTS(SELECT 1 FROM sys.columns WHERE Name = N'EconomoConto' AND Object_ID = Object_ID(N'DatiGenerali'))
BEGIN
ALTER TABLE DatiGenerali ADD [EconomoConto] smallint;
END

IF NOT EXISTS(SELECT 1 FROM sys.columns WHERE Name = N'EconomoSottoconto' AND Object_ID = Object_ID(N'DatiGenerali'))
BEGIN
ALTER TABLE DatiGenerali ADD [EconomoSottoconto] real;
END

IF NOT EXISTS(SELECT 1 FROM sys.columns WHERE Name = N'EconomoCapitolo' AND Object_ID = Object_ID(N'DatiGenerali'))
BEGIN
ALTER TABLE DatiGenerali ADD [EconomoCapitolo] int;
END

IF NOT EXISTS(SELECT 1 FROM sys.columns WHERE Name = N'EconomoArticolo' AND Object_ID = Object_ID(N'DatiGenerali'))
BEGIN
ALTER TABLE DatiGenerali ADD [EconomoArticolo] smallint;
END

IF NOT EXISTS(SELECT 1 FROM sys.columns WHERE Name = N'ImportoMassimo' AND Object_ID = Object_ID(N'DatiGenerali'))
BEGIN
ALTER TABLE DatiGenerali ADD [ImportoMassimo] float;
END

IF NOT EXISTS(SELECT 1 FROM sys.columns WHERE Name = N'CausaleGirocontoRitenute' AND Object_ID = Object_ID(N'DatiGenerali'))
BEGIN
ALTER TABLE DatiGenerali ADD [CausaleGirocontoRitenute] nvarchar (3);
END

IF NOT EXISTS(SELECT 1 FROM sys.columns WHERE Name = N'DataChiusura' AND Object_ID = Object_ID(N'DatiGenerali'))
BEGIN
ALTER TABLE DatiGenerali ADD [DataChiusura] datetime;
END

IF NOT EXISTS(SELECT 1 FROM sys.columns WHERE Name = N'MastroBollo' AND Object_ID = Object_ID(N'DatiGenerali'))
BEGIN
ALTER TABLE DatiGenerali ADD [MastroBollo] int;
END

IF NOT EXISTS(SELECT 1 FROM sys.columns WHERE Name = N'ContoBollo' AND Object_ID = Object_ID(N'DatiGenerali'))
BEGIN
ALTER TABLE DatiGenerali ADD [ContoBollo] int;
END

IF NOT EXISTS(SELECT 1 FROM sys.columns WHERE Name = N'SottoContoBollo' AND Object_ID = Object_ID(N'DatiGenerali'))
BEGIN
ALTER TABLE DatiGenerali ADD [SottoContoBollo] float;
END

IF NOT EXISTS(SELECT 1 FROM sys.columns WHERE Name = N'FornitoriConto' AND Object_ID = Object_ID(N'DatiGenerali'))
BEGIN
ALTER TABLE DatiGenerali ADD [FornitoriConto] int;
END

IF NOT EXISTS(SELECT 1 FROM sys.columns WHERE Name = N'ClientiConto' AND Object_ID = Object_ID(N'DatiGenerali'))
BEGIN
ALTER TABLE DatiGenerali ADD [ClientiConto] int;
END


IF NOT EXISTS(SELECT 1 FROM sys.columns WHERE Name = N'TestAgyo' AND Object_ID = Object_ID(N'DatiGenerali'))
BEGIN
ALTER TABLE DatiGenerali ADD TestAgyo varchar(1);
END


IF NOT EXISTS(SELECT 1 FROM sys.columns WHERE Name = N'IdAgyo' AND Object_ID = Object_ID(N'DatiGenerali'))
BEGIN
ALTER TABLE DatiGenerali ADD IdAgyo varchar(250);
END


IF NOT EXISTS(SELECT 1 FROM sys.columns WHERE Name = N'PasswordAgyo' AND Object_ID = Object_ID(N'DatiGenerali'))
BEGIN
ALTER TABLE DatiGenerali ADD PasswordAgyo varchar(250);
END

IF NOT EXISTS(SELECT 1 FROM sys.columns WHERE Name = N'TokenAgyo' AND Object_ID = Object_ID(N'DatiGenerali'))
BEGIN
ALTER TABLE DatiGenerali ADD TokenAgyo varchar(MAX);
END



IF NOT EXISTS(SELECT 1 FROM sys.columns WHERE Name = N'ModalitaPagamentoOblDoc' AND Object_ID = Object_ID(N'DatiGenerali'))
BEGIN
ALTER TABLE DatiGenerali ADD ModalitaPagamentoOblDoc int;
END

IF NOT EXISTS(SELECT name FROM sys.sysobjects  WHERE Name = N'Depositi' AND xtype = N'U')
BEGIN
CREATE TABLE Depositi([ID] [int] IDENTITY(1,1) NOT NULL) ON [PRIMARY];
END

IF NOT EXISTS(SELECT 1 FROM sys.columns WHERE Name = N'Utente' AND Object_ID = Object_ID(N'Depositi'))
BEGIN
ALTER TABLE Depositi ADD [Utente] nvarchar (50);
END

IF NOT EXISTS(SELECT 1 FROM sys.columns WHERE Name = N'DataAggiornamento' AND Object_ID = Object_ID(N'Depositi'))
BEGIN
ALTER TABLE Depositi ADD [DataAggiornamento] datetime;
END

IF NOT EXISTS(SELECT 1 FROM sys.columns WHERE Name = N'Codice' AND Object_ID = Object_ID(N'Depositi'))
BEGIN
ALTER TABLE Depositi ADD [Codice] nvarchar (2);
END

IF NOT EXISTS(SELECT 1 FROM sys.columns WHERE Name = N'Descrizione' AND Object_ID = Object_ID(N'Depositi'))
BEGIN
ALTER TABLE Depositi ADD [Descrizione] nvarchar (50);
END

IF NOT EXISTS(SELECT 1 FROM sys.columns WHERE Name = N'Ubicazione' AND Object_ID = Object_ID(N'Depositi'))
BEGIN
ALTER TABLE Depositi ADD [Ubicazione] nvarchar (8);
END

IF NOT EXISTS(SELECT 1 FROM sys.columns WHERE Name = N'Categoria' AND Object_ID = Object_ID(N'Depositi'))
BEGIN
ALTER TABLE Depositi ADD [Categoria] nvarchar (2);
END

IF NOT EXISTS(SELECT 1 FROM sys.columns WHERE Name = N'Tipo' AND Object_ID = Object_ID(N'Depositi'))
BEGIN
ALTER TABLE Depositi ADD [Tipo] nvarchar (1);
END

IF NOT EXISTS(SELECT name FROM sys.sysobjects  WHERE Name = N'Detraibilita' AND xtype = N'U')
BEGIN
CREATE TABLE Detraibilita([ID] [int] IDENTITY(1,1) NOT NULL) ON [PRIMARY];
END

IF NOT EXISTS(SELECT 1 FROM sys.columns WHERE Name = N'Utente' AND Object_ID = Object_ID(N'Detraibilita'))
BEGIN
ALTER TABLE Detraibilita ADD [Utente] nvarchar (50);
END

IF NOT EXISTS(SELECT 1 FROM sys.columns WHERE Name = N'DataAggiornamento' AND Object_ID = Object_ID(N'Detraibilita'))
BEGIN
ALTER TABLE Detraibilita ADD [DataAggiornamento] datetime;
END

IF NOT EXISTS(SELECT 1 FROM sys.columns WHERE Name = N'Codice' AND Object_ID = Object_ID(N'Detraibilita'))
BEGIN
ALTER TABLE Detraibilita ADD [Codice] nvarchar (1);
END

IF NOT EXISTS(SELECT 1 FROM sys.columns WHERE Name = N'Descrizione' AND Object_ID = Object_ID(N'Detraibilita'))
BEGIN
ALTER TABLE Detraibilita ADD [Descrizione] nvarchar (50);
END

IF NOT EXISTS(SELECT 1 FROM sys.columns WHERE Name = N'Aliquota' AND Object_ID = Object_ID(N'Detraibilita'))
BEGIN
ALTER TABLE Detraibilita ADD [Aliquota] real;
END

IF NOT EXISTS(SELECT 1 FROM sys.columns WHERE Name = N'Automatico' AND Object_ID = Object_ID(N'Detraibilita'))
BEGIN
ALTER TABLE Detraibilita ADD [Automatico] nvarchar (1);
END

IF NOT EXISTS(SELECT 1 FROM sys.columns WHERE Name = N'GiroACosto' AND Object_ID = Object_ID(N'Detraibilita'))
BEGIN
ALTER TABLE Detraibilita ADD [GiroACosto] nvarchar (1);
END

IF NOT EXISTS(SELECT 1 FROM sys.columns WHERE Name = N'IVAGiroACostoMastro' AND Object_ID = Object_ID(N'Detraibilita'))
BEGIN
ALTER TABLE Detraibilita ADD [IVAGiroACostoMastro] smallint;
END

IF NOT EXISTS(SELECT 1 FROM sys.columns WHERE Name = N'IVAGiroACostoConto' AND Object_ID = Object_ID(N'Detraibilita'))
BEGIN
ALTER TABLE Detraibilita ADD [IVAGiroACostoConto] smallint;
END

IF NOT EXISTS(SELECT 1 FROM sys.columns WHERE Name = N'IVAGiroACostoSottoconto' AND Object_ID = Object_ID(N'Detraibilita'))
BEGIN
ALTER TABLE Detraibilita ADD [IVAGiroACostoSottoconto] float;
END

IF NOT EXISTS(SELECT 1 FROM sys.columns WHERE Name = N'IVAGiroACostoAliquota' AND Object_ID = Object_ID(N'Detraibilita'))
BEGIN
ALTER TABLE Detraibilita ADD [IVAGiroACostoAliquota] real;
END

IF NOT EXISTS(SELECT 1 FROM sys.columns WHERE Name = N'Prorata' AND Object_ID = Object_ID(N'Detraibilita'))
BEGIN
ALTER TABLE Detraibilita ADD [Prorata] int;
END

IF NOT EXISTS(SELECT name FROM sys.sysobjects  WHERE Name = N'GruppiMerceologici' AND xtype = N'U')
BEGIN
CREATE TABLE GruppiMerceologici([ID] [int] IDENTITY(1,1) NOT NULL) ON [PRIMARY];
END

IF NOT EXISTS(SELECT 1 FROM sys.columns WHERE Name = N'Utente' AND Object_ID = Object_ID(N'GruppiMerceologici'))
BEGIN
ALTER TABLE GruppiMerceologici ADD [Utente] nvarchar (50);
END

IF NOT EXISTS(SELECT 1 FROM sys.columns WHERE Name = N'DataAggiornamento' AND Object_ID = Object_ID(N'GruppiMerceologici'))
BEGIN
ALTER TABLE GruppiMerceologici ADD [DataAggiornamento] datetime;
END

IF NOT EXISTS(SELECT 1 FROM sys.columns WHERE Name = N'Codice' AND Object_ID = Object_ID(N'GruppiMerceologici'))
BEGIN
ALTER TABLE GruppiMerceologici ADD [Codice] nvarchar (2);
END

IF NOT EXISTS(SELECT 1 FROM sys.columns WHERE Name = N'Descrizione' AND Object_ID = Object_ID(N'GruppiMerceologici'))
BEGIN
ALTER TABLE GruppiMerceologici ADD [Descrizione] nvarchar (50);
END

IF NOT EXISTS(SELECT name FROM sys.sysobjects  WHERE Name = N'GruppoRegistriRiga' AND xtype = N'U')
BEGIN
CREATE TABLE GruppoRegistriRiga([ID] [int] IDENTITY(1,1) NOT NULL) ON [PRIMARY];
END

IF NOT EXISTS(SELECT 1 FROM sys.columns WHERE Name = N'UTENTE' AND Object_ID = Object_ID(N'GruppoRegistriRiga'))
BEGIN
ALTER TABLE GruppoRegistriRiga ADD [UTENTE] nvarchar (255);
END

IF NOT EXISTS(SELECT 1 FROM sys.columns WHERE Name = N'DATAAGGIORNAMENTO' AND Object_ID = Object_ID(N'GruppoRegistriRiga'))
BEGIN
ALTER TABLE GruppoRegistriRiga ADD [DATAAGGIORNAMENTO] datetime;
END

IF NOT EXISTS(SELECT 1 FROM sys.columns WHERE Name = N'CODICE' AND Object_ID = Object_ID(N'GruppoRegistriRiga'))
BEGIN
ALTER TABLE GruppoRegistriRiga ADD [CODICE] nvarchar (2);
END

IF NOT EXISTS(SELECT 1 FROM sys.columns WHERE Name = N'REGISTRO' AND Object_ID = Object_ID(N'GruppoRegistriRiga'))
BEGIN
ALTER TABLE GruppoRegistriRiga ADD [REGISTRO] int;
END

IF NOT EXISTS(SELECT name FROM sys.sysobjects  WHERE Name = N'GruppoRegistriTesta' AND xtype = N'U')
BEGIN
CREATE TABLE GruppoRegistriTesta([ID] [int] IDENTITY(1,1) NOT NULL) ON [PRIMARY];
END

IF NOT EXISTS(SELECT 1 FROM sys.columns WHERE Name = N'UTENTE' AND Object_ID = Object_ID(N'GruppoRegistriTesta'))
BEGIN
ALTER TABLE GruppoRegistriTesta ADD [UTENTE] nvarchar (255);
END

IF NOT EXISTS(SELECT 1 FROM sys.columns WHERE Name = N'DATAAGGIORNAMENTO' AND Object_ID = Object_ID(N'GruppoRegistriTesta'))
BEGIN
ALTER TABLE GruppoRegistriTesta ADD [DATAAGGIORNAMENTO] datetime;
END

IF NOT EXISTS(SELECT 1 FROM sys.columns WHERE Name = N'CODICE' AND Object_ID = Object_ID(N'GruppoRegistriTesta'))
BEGIN
ALTER TABLE GruppoRegistriTesta ADD [CODICE] nvarchar (2);
END

IF NOT EXISTS(SELECT 1 FROM sys.columns WHERE Name = N'DESCRIZIONE' AND Object_ID = Object_ID(N'GruppoRegistriTesta'))
BEGIN
ALTER TABLE GruppoRegistriTesta ADD [DESCRIZIONE] nvarchar (255);
END

IF NOT EXISTS(SELECT 1 FROM sys.columns WHERE Name = N'TipoReport' AND Object_ID = Object_ID(N'GruppoRegistriTesta'))
BEGIN
ALTER TABLE GruppoRegistriTesta ADD [TipoReport] int;
END

IF NOT EXISTS(SELECT 1 FROM sys.columns WHERE Name = N'ConsideraInLiquidazioni' AND Object_ID = Object_ID(N'GruppoRegistriTesta'))
BEGIN
ALTER TABLE GruppoRegistriTesta ADD [ConsideraInLiquidazioni] int;
END

IF NOT EXISTS(SELECT name FROM sys.sysobjects  WHERE Name = N'IVA' AND xtype = N'U')
BEGIN
CREATE TABLE IVA([ID] [int] IDENTITY(1,1) NOT NULL) ON [PRIMARY];
END

IF NOT EXISTS(SELECT 1 FROM sys.columns WHERE Name = N'Utente' AND Object_ID = Object_ID(N'IVA'))
BEGIN
ALTER TABLE IVA ADD [Utente] nvarchar (50);
END

IF NOT EXISTS(SELECT 1 FROM sys.columns WHERE Name = N'DataAggiornamento' AND Object_ID = Object_ID(N'IVA'))
BEGIN
ALTER TABLE IVA ADD [DataAggiornamento] datetime;
END

IF NOT EXISTS(SELECT 1 FROM sys.columns WHERE Name = N'Codice' AND Object_ID = Object_ID(N'IVA'))
BEGIN
ALTER TABLE IVA ADD [Codice] nvarchar (2);
END

IF NOT EXISTS(SELECT 1 FROM sys.columns WHERE Name = N'Descrizione' AND Object_ID = Object_ID(N'IVA'))
BEGIN
ALTER TABLE IVA ADD [Descrizione] nvarchar (50);
END

IF NOT EXISTS(SELECT 1 FROM sys.columns WHERE Name = N'Aliquota' AND Object_ID = Object_ID(N'IVA'))
BEGIN
ALTER TABLE IVA ADD [Aliquota] real;
END

IF NOT EXISTS(SELECT 1 FROM sys.columns WHERE Name = N'AllegatoFornitori' AND Object_ID = Object_ID(N'IVA'))
BEGIN
ALTER TABLE IVA ADD [AllegatoFornitori] smallint;
END

IF NOT EXISTS(SELECT 1 FROM sys.columns WHERE Name = N'AllegatoClienti' AND Object_ID = Object_ID(N'IVA'))
BEGIN
ALTER TABLE IVA ADD [AllegatoClienti] smallint;
END

IF NOT EXISTS(SELECT 1 FROM sys.columns WHERE Name = N'Tipo' AND Object_ID = Object_ID(N'IVA'))
BEGIN
ALTER TABLE IVA ADD [Tipo] nvarchar (2);
END

IF NOT EXISTS(SELECT 1 FROM sys.columns WHERE Name = N'InLiquidazione' AND Object_ID = Object_ID(N'IVA'))
BEGIN
ALTER TABLE IVA ADD [InLiquidazione] int;
END

IF NOT EXISTS(SELECT 1 FROM sys.columns WHERE Name = N'DetraibilitaDefault' AND Object_ID = Object_ID(N'IVA'))
BEGIN
ALTER TABLE IVA ADD [DetraibilitaDefault] nvarchar (2);
END

IF NOT EXISTS(SELECT 1 FROM sys.columns WHERE Name = N'InSpesometro' AND Object_ID = Object_ID(N'IVA'))
BEGIN
ALTER TABLE IVA ADD [InSpesometro] int;
END

IF NOT EXISTS(SELECT 1 FROM sys.columns WHERE Name = N'Natura' AND Object_ID = Object_ID(N'IVA'))
BEGIN
ALTER TABLE IVA ADD [Natura] nvarchar (50);
END

IF NOT EXISTS(SELECT name FROM sys.sysobjects  WHERE Name = N'ModalitaPagamento' AND xtype = N'U')
BEGIN
CREATE TABLE ModalitaPagamento([ID] [int] IDENTITY(1,1) NOT NULL) ON [PRIMARY];
END

IF NOT EXISTS(SELECT 1 FROM sys.columns WHERE Name = N'Utente' AND Object_ID = Object_ID(N'ModalitaPagamento'))
BEGIN
ALTER TABLE ModalitaPagamento ADD [Utente] nvarchar (50);
END

IF NOT EXISTS(SELECT 1 FROM sys.columns WHERE Name = N'DataAggiornamento' AND Object_ID = Object_ID(N'ModalitaPagamento'))
BEGIN
ALTER TABLE ModalitaPagamento ADD [DataAggiornamento] datetime;
END

IF NOT EXISTS(SELECT 1 FROM sys.columns WHERE Name = N'Codice' AND Object_ID = Object_ID(N'ModalitaPagamento'))
BEGIN
ALTER TABLE ModalitaPagamento ADD [Codice] nvarchar (2);
END

IF NOT EXISTS(SELECT 1 FROM sys.columns WHERE Name = N'Descrizione' AND Object_ID = Object_ID(N'ModalitaPagamento'))
BEGIN
ALTER TABLE ModalitaPagamento ADD [Descrizione] nvarchar (60);
END

IF NOT EXISTS(SELECT 1 FROM sys.columns WHERE Name = N'Per' AND Object_ID = Object_ID(N'ModalitaPagamento'))
BEGIN
ALTER TABLE ModalitaPagamento ADD [Per] nvarchar (1);
END

IF NOT EXISTS(SELECT 1 FROM sys.columns WHERE Name = N'Mastro' AND Object_ID = Object_ID(N'ModalitaPagamento'))
BEGIN
ALTER TABLE ModalitaPagamento ADD [Mastro] smallint;
END

IF NOT EXISTS(SELECT 1 FROM sys.columns WHERE Name = N'Conto' AND Object_ID = Object_ID(N'ModalitaPagamento'))
BEGIN
ALTER TABLE ModalitaPagamento ADD [Conto] smallint;
END

IF NOT EXISTS(SELECT 1 FROM sys.columns WHERE Name = N'Sottoconto' AND Object_ID = Object_ID(N'ModalitaPagamento'))
BEGIN
ALTER TABLE ModalitaPagamento ADD [Sottoconto] float;
END

IF NOT EXISTS(SELECT 1 FROM sys.columns WHERE Name = N'CodiceBolletta' AND Object_ID = Object_ID(N'ModalitaPagamento'))
BEGIN
ALTER TABLE ModalitaPagamento ADD [CodiceBolletta] nvarchar (1);
END

IF NOT EXISTS(SELECT name FROM sys.sysobjects  WHERE Name = N'ProgressiviClientiFornitori' AND xtype = N'U')
BEGIN
CREATE TABLE ProgressiviClientiFornitori([ID] [int] IDENTITY(1,1) NOT NULL) ON [PRIMARY];
END

IF NOT EXISTS(SELECT 1 FROM sys.columns WHERE Name = N'Utente' AND Object_ID = Object_ID(N'ProgressiviClientiFornitori'))
BEGIN
ALTER TABLE ProgressiviClientiFornitori ADD [Utente] nvarchar (50);
END

IF NOT EXISTS(SELECT 1 FROM sys.columns WHERE Name = N'DataAggiornamento' AND Object_ID = Object_ID(N'ProgressiviClientiFornitori'))
BEGIN
ALTER TABLE ProgressiviClientiFornitori ADD [DataAggiornamento] datetime;
END

IF NOT EXISTS(SELECT 1 FROM sys.columns WHERE Name = N'Tipo' AND Object_ID = Object_ID(N'ProgressiviClientiFornitori'))
BEGIN
ALTER TABLE ProgressiviClientiFornitori ADD [Tipo] nvarchar (1);
END

IF NOT EXISTS(SELECT 1 FROM sys.columns WHERE Name = N'Conto' AND Object_ID = Object_ID(N'ProgressiviClientiFornitori'))
BEGIN
ALTER TABLE ProgressiviClientiFornitori ADD [Conto] smallint;
END

IF NOT EXISTS(SELECT 1 FROM sys.columns WHERE Name = N'Progressivo' AND Object_ID = Object_ID(N'ProgressiviClientiFornitori'))
BEGIN
ALTER TABLE ProgressiviClientiFornitori ADD [Progressivo] float;
END

IF NOT EXISTS(SELECT name FROM sys.sysobjects  WHERE Name = N'ProgressiviIVA' AND xtype = N'U')
BEGIN
CREATE TABLE ProgressiviIVA([ID] [int] IDENTITY(1,1) NOT NULL) ON [PRIMARY];
END

IF NOT EXISTS(SELECT 1 FROM sys.columns WHERE Name = N'Anno' AND Object_ID = Object_ID(N'ProgressiviIVA'))
BEGIN
ALTER TABLE ProgressiviIVA ADD [Anno] int;
END

IF NOT EXISTS(SELECT 1 FROM sys.columns WHERE Name = N'Mese' AND Object_ID = Object_ID(N'ProgressiviIVA'))
BEGIN
ALTER TABLE ProgressiviIVA ADD [Mese] int;
END

IF NOT EXISTS(SELECT 1 FROM sys.columns WHERE Name = N'Importo' AND Object_ID = Object_ID(N'ProgressiviIVA'))
BEGIN
ALTER TABLE ProgressiviIVA ADD [Importo] float;
END

IF NOT EXISTS(SELECT 1 FROM sys.columns WHERE Name = N'Gruppo' AND Object_ID = Object_ID(N'ProgressiviIVA'))
BEGIN
ALTER TABLE ProgressiviIVA ADD [Gruppo] nvarchar (2);
END

IF NOT EXISTS(SELECT name FROM sys.sysobjects  WHERE Name = N'Prorata' AND xtype = N'U')
BEGIN
CREATE TABLE Prorata([ID] [int] IDENTITY(1,1) NOT NULL) ON [PRIMARY];
END

IF NOT EXISTS(SELECT 1 FROM sys.columns WHERE Name = N'Utente' AND Object_ID = Object_ID(N'Prorata'))
BEGIN
ALTER TABLE Prorata ADD [Utente] nvarchar (50);
END

IF NOT EXISTS(SELECT 1 FROM sys.columns WHERE Name = N'DataAggiornamento' AND Object_ID = Object_ID(N'Prorata'))
BEGIN
ALTER TABLE Prorata ADD [DataAggiornamento] datetime;
END

IF NOT EXISTS(SELECT 1 FROM sys.columns WHERE Name = N'RegistroIVA' AND Object_ID = Object_ID(N'Prorata'))
BEGIN
ALTER TABLE Prorata ADD [RegistroIVA] int;
END

IF NOT EXISTS(SELECT 1 FROM sys.columns WHERE Name = N'Anno' AND Object_ID = Object_ID(N'Prorata'))
BEGIN
ALTER TABLE Prorata ADD [Anno] smallint;
END

IF NOT EXISTS(SELECT 1 FROM sys.columns WHERE Name = N'PresuntoEsenti' AND Object_ID = Object_ID(N'Prorata'))
BEGIN
ALTER TABLE Prorata ADD [PresuntoEsenti] float;
END

IF NOT EXISTS(SELECT 1 FROM sys.columns WHERE Name = N'PresuntoAffari' AND Object_ID = Object_ID(N'Prorata'))
BEGIN
ALTER TABLE Prorata ADD [PresuntoAffari] float;
END

IF NOT EXISTS(SELECT 1 FROM sys.columns WHERE Name = N'PresuntoPercentuale' AND Object_ID = Object_ID(N'Prorata'))
BEGIN
ALTER TABLE Prorata ADD [PresuntoPercentuale] real;
END

IF NOT EXISTS(SELECT 1 FROM sys.columns WHERE Name = N'DefinitivoEsenti' AND Object_ID = Object_ID(N'Prorata'))
BEGIN
ALTER TABLE Prorata ADD [DefinitivoEsenti] float;
END

IF NOT EXISTS(SELECT 1 FROM sys.columns WHERE Name = N'DefinitivoAffari' AND Object_ID = Object_ID(N'Prorata'))
BEGIN
ALTER TABLE Prorata ADD [DefinitivoAffari] float;
END

IF NOT EXISTS(SELECT 1 FROM sys.columns WHERE Name = N'DefinitivoPercentuale' AND Object_ID = Object_ID(N'Prorata'))
BEGIN
ALTER TABLE Prorata ADD [DefinitivoPercentuale] real;
END

IF NOT EXISTS(SELECT name FROM sys.sysobjects  WHERE Name = N'Registri' AND xtype = N'U')
BEGIN
CREATE TABLE Registri([ID] [int] IDENTITY(1,1) NOT NULL) ON [PRIMARY];
END

IF NOT EXISTS(SELECT 1 FROM sys.columns WHERE Name = N'Utente' AND Object_ID = Object_ID(N'Registri'))
BEGIN
ALTER TABLE Registri ADD [Utente] nvarchar (50);
END

IF NOT EXISTS(SELECT 1 FROM sys.columns WHERE Name = N'DataAggiornamento' AND Object_ID = Object_ID(N'Registri'))
BEGIN
ALTER TABLE Registri ADD [DataAggiornamento] datetime;
END

IF NOT EXISTS(SELECT 1 FROM sys.columns WHERE Name = N'Anno' AND Object_ID = Object_ID(N'Registri'))
BEGIN
ALTER TABLE Registri ADD [Anno] smallint;
END

IF NOT EXISTS(SELECT 1 FROM sys.columns WHERE Name = N'Tipo' AND Object_ID = Object_ID(N'Registri'))
BEGIN
ALTER TABLE Registri ADD [Tipo] tinyint;
END

IF NOT EXISTS(SELECT 1 FROM sys.columns WHERE Name = N'DataStampa' AND Object_ID = Object_ID(N'Registri'))
BEGIN
ALTER TABLE Registri ADD [DataStampa] datetime;
END

IF NOT EXISTS(SELECT 1 FROM sys.columns WHERE Name = N'UltimaPaginaStampata' AND Object_ID = Object_ID(N'Registri'))
BEGIN
ALTER TABLE Registri ADD [UltimaPaginaStampata] int;
END

IF NOT EXISTS(SELECT name FROM sys.sysobjects  WHERE Name = N'Ritenute' AND xtype = N'U')
BEGIN
CREATE TABLE Ritenute([ID] [int] IDENTITY(1,1) NOT NULL) ON [PRIMARY];
END

IF NOT EXISTS(SELECT 1 FROM sys.columns WHERE Name = N'Utente' AND Object_ID = Object_ID(N'Ritenute'))
BEGIN
ALTER TABLE Ritenute ADD [Utente] nvarchar (50);
END

IF NOT EXISTS(SELECT 1 FROM sys.columns WHERE Name = N'DataAggiornamento' AND Object_ID = Object_ID(N'Ritenute'))
BEGIN
ALTER TABLE Ritenute ADD [DataAggiornamento] datetime;
END

IF NOT EXISTS(SELECT 1 FROM sys.columns WHERE Name = N'Codice' AND Object_ID = Object_ID(N'Ritenute'))
BEGIN
ALTER TABLE Ritenute ADD [Codice] nvarchar (2);
END

IF NOT EXISTS(SELECT 1 FROM sys.columns WHERE Name = N'Descrizione' AND Object_ID = Object_ID(N'Ritenute'))
BEGIN
ALTER TABLE Ritenute ADD [Descrizione] nvarchar (50);
END


IF NOT EXISTS(SELECT 1 FROM sys.columns WHERE Name = N'ACarico' AND Object_ID = Object_ID(N'Ritenute'))
BEGIN
ALTER TABLE Ritenute ADD ACarico nvarchar (1);
END


IF NOT EXISTS(SELECT 1 FROM sys.columns WHERE Name = N'Percentuale' AND Object_ID = Object_ID(N'Ritenute'))
BEGIN
ALTER TABLE Ritenute ADD [Percentuale] real;
END

IF NOT EXISTS(SELECT 1 FROM sys.columns WHERE Name = N'Mastro' AND Object_ID = Object_ID(N'Ritenute'))
BEGIN
ALTER TABLE Ritenute ADD [Mastro] smallint;
END

IF NOT EXISTS(SELECT 1 FROM sys.columns WHERE Name = N'Conto' AND Object_ID = Object_ID(N'Ritenute'))
BEGIN
ALTER TABLE Ritenute ADD [Conto] smallint;
END

IF NOT EXISTS(SELECT 1 FROM sys.columns WHERE Name = N'Sottoconto' AND Object_ID = Object_ID(N'Ritenute'))
BEGIN
ALTER TABLE Ritenute ADD [Sottoconto] float;
END

IF NOT EXISTS(SELECT name FROM sys.sysobjects  WHERE Name = N'Societa' AND xtype = N'U')
BEGIN
CREATE TABLE Societa([ID] [int] IDENTITY(1,1) NOT NULL) ON [PRIMARY];
END

IF NOT EXISTS(SELECT 1 FROM sys.columns WHERE Name = N'Utente' AND Object_ID = Object_ID(N'Societa'))
BEGIN
ALTER TABLE Societa ADD [Utente] nvarchar (50);
END

IF NOT EXISTS(SELECT 1 FROM sys.columns WHERE Name = N'DataAggiornamento' AND Object_ID = Object_ID(N'Societa'))
BEGIN
ALTER TABLE Societa ADD [DataAggiornamento] datetime;
END

IF NOT EXISTS(SELECT 1 FROM sys.columns WHERE Name = N'RagioneSociale' AND Object_ID = Object_ID(N'Societa'))
BEGIN
ALTER TABLE Societa ADD [RagioneSociale] nvarchar (100);
END

IF NOT EXISTS(SELECT 1 FROM sys.columns WHERE Name = N'Indirizzo' AND Object_ID = Object_ID(N'Societa'))
BEGIN
ALTER TABLE Societa ADD [Indirizzo] nvarchar (50);
END

IF NOT EXISTS(SELECT 1 FROM sys.columns WHERE Name = N'CAP' AND Object_ID = Object_ID(N'Societa'))
BEGIN
ALTER TABLE Societa ADD [CAP] int;
END

IF NOT EXISTS(SELECT 1 FROM sys.columns WHERE Name = N'Localita' AND Object_ID = Object_ID(N'Societa'))
BEGIN
ALTER TABLE Societa ADD [Localita] nvarchar (50);
END

IF NOT EXISTS(SELECT 1 FROM sys.columns WHERE Name = N'Provincia' AND Object_ID = Object_ID(N'Societa'))
BEGIN
ALTER TABLE Societa ADD [Provincia] nvarchar (2);
END

IF NOT EXISTS(SELECT 1 FROM sys.columns WHERE Name = N'Pasword' AND Object_ID = Object_ID(N'Societa'))
BEGIN
ALTER TABLE Societa ADD [Pasword] int;
END

IF NOT EXISTS(SELECT 1 FROM sys.columns WHERE Name = N'DataScadenza' AND Object_ID = Object_ID(N'Societa'))
BEGIN
ALTER TABLE Societa ADD [DataScadenza] datetime;
END

IF NOT EXISTS(SELECT 1 FROM sys.columns WHERE Name = N'OspitiAbilitati' AND Object_ID = Object_ID(N'Societa'))
BEGIN
ALTER TABLE Societa ADD [OspitiAbilitati] int;
END

IF NOT EXISTS(SELECT 1 FROM sys.columns WHERE Name = N'OspitiPresenti' AND Object_ID = Object_ID(N'Societa'))
BEGIN
ALTER TABLE Societa ADD [OspitiPresenti] int;
END

IF NOT EXISTS(SELECT 1 FROM sys.columns WHERE Name = N'Attivita' AND Object_ID = Object_ID(N'Societa'))
BEGIN
ALTER TABLE Societa ADD [Attivita] nvarchar (50);
END

IF NOT EXISTS(SELECT 1 FROM sys.columns WHERE Name = N'CodiceFiscale' AND Object_ID = Object_ID(N'Societa'))
BEGIN
ALTER TABLE Societa ADD [CodiceFiscale] nvarchar (16);
END

IF NOT EXISTS(SELECT 1 FROM sys.columns WHERE Name = N'PartitaIVA' AND Object_ID = Object_ID(N'Societa'))
BEGIN
ALTER TABLE Societa ADD [PartitaIVA] float;
END

IF NOT EXISTS(SELECT 1 FROM sys.columns WHERE Name = N'Telefono' AND Object_ID = Object_ID(N'Societa'))
BEGIN
ALTER TABLE Societa ADD [Telefono] nvarchar (15);
END

IF NOT EXISTS(SELECT 1 FROM sys.columns WHERE Name = N'PaswordAttiva' AND Object_ID = Object_ID(N'Societa'))
BEGIN
ALTER TABLE Societa ADD [PaswordAttiva] nvarchar (1);
END

IF NOT EXISTS(SELECT 1 FROM sys.columns WHERE Name = N'Prompt' AND Object_ID = Object_ID(N'Societa'))
BEGIN
ALTER TABLE Societa ADD [Prompt] nvarchar (15);
END

IF NOT EXISTS(SELECT 1 FROM sys.columns WHERE Name = N'CodiceEffettivo' AND Object_ID = Object_ID(N'Societa'))
BEGIN
ALTER TABLE Societa ADD [CodiceEffettivo] nvarchar (10);
END

IF NOT EXISTS(SELECT 1 FROM sys.columns WHERE Name = N'RagioneSocialeEffettivo' AND Object_ID = Object_ID(N'Societa'))
BEGIN
ALTER TABLE Societa ADD [RagioneSocialeEffettivo] nvarchar (100);
END

IF NOT EXISTS(SELECT 1 FROM sys.columns WHERE Name = N'ABI' AND Object_ID = Object_ID(N'Societa'))
BEGIN
ALTER TABLE Societa ADD [ABI] int;
END

IF NOT EXISTS(SELECT 1 FROM sys.columns WHERE Name = N'CAB' AND Object_ID = Object_ID(N'Societa'))
BEGIN
ALTER TABLE Societa ADD [CAB] int;
END

IF NOT EXISTS(SELECT 1 FROM sys.columns WHERE Name = N'ContoCorrente' AND Object_ID = Object_ID(N'Societa'))
BEGIN
ALTER TABLE Societa ADD [ContoCorrente] nvarchar (20);
END

IF NOT EXISTS(SELECT 1 FROM sys.columns WHERE Name = N'Commento' AND Object_ID = Object_ID(N'Societa'))
BEGIN
ALTER TABLE Societa ADD [Commento] nvarchar (50);
END

IF NOT EXISTS(SELECT 1 FROM sys.columns WHERE Name = N'DecEuro' AND Object_ID = Object_ID(N'Societa'))
BEGIN
ALTER TABLE Societa ADD [DecEuro] tinyint;
END

IF NOT EXISTS(SELECT 1 FROM sys.columns WHERE Name = N'ProcedureAbilitate' AND Object_ID = Object_ID(N'Societa'))
BEGIN
ALTER TABLE Societa ADD [ProcedureAbilitate] nvarchar (250);
END

IF NOT EXISTS(SELECT 1 FROM sys.columns WHERE Name = N'Banca' AND Object_ID = Object_ID(N'Societa'))
BEGIN
ALTER TABLE Societa ADD [Banca] varchar (150);
END

IF NOT EXISTS(SELECT 1 FROM sys.columns WHERE Name = N'IBAN' AND Object_ID = Object_ID(N'Societa'))
BEGIN
ALTER TABLE Societa ADD [IBAN] varchar (50);
END

IF NOT EXISTS(SELECT 1 FROM sys.columns WHERE Name = N'CodiceInstallazione' AND Object_ID = Object_ID(N'Societa'))
BEGIN
ALTER TABLE Societa ADD [CodiceInstallazione] varchar (20);
END

IF NOT EXISTS(SELECT 1 FROM sys.columns WHERE Name = N'CodiceDitta' AND Object_ID = Object_ID(N'Societa'))
BEGIN
ALTER TABLE Societa ADD [CodiceDitta] varchar (20);
END

IF NOT EXISTS(SELECT 1 FROM sys.columns WHERE Name = N'NomeFile' AND Object_ID = Object_ID(N'Societa'))
BEGIN
ALTER TABLE Societa ADD [NomeFile] varchar (250);
END


IF NOT EXISTS(SELECT 1 FROM sys.columns WHERE Name = N'EMail' AND Object_ID = Object_ID(N'Societa'))
BEGIN
ALTER TABLE Societa ADD EMail varchar(150);
END


IF NOT EXISTS(SELECT name FROM sys.sysobjects  WHERE Name = N'TABELLAUTENTI' AND xtype = N'U')
BEGIN
CREATE TABLE TABELLAUTENTI([ID] [int] IDENTITY(1,1) NOT NULL) ON [PRIMARY];
END

IF NOT EXISTS(SELECT 1 FROM sys.columns WHERE Name = N'NOMEUTENTE' AND Object_ID = Object_ID(N'TABELLAUTENTI'))
BEGIN
ALTER TABLE TABELLAUTENTI ADD [NOMEUTENTE] nvarchar (50);
END

IF NOT EXISTS(SELECT 1 FROM sys.columns WHERE Name = N'PASSWORD' AND Object_ID = Object_ID(N'TABELLAUTENTI'))
BEGIN
ALTER TABLE TABELLAUTENTI ADD [PASSWORD] nvarchar (50);
END

IF NOT EXISTS(SELECT name FROM sys.sysobjects  WHERE Name = N'TipoAtto' AND xtype = N'U')
BEGIN
CREATE TABLE TipoAtto([ID] [int] IDENTITY(1,1) NOT NULL) ON [PRIMARY];
END

IF NOT EXISTS(SELECT 1 FROM sys.columns WHERE Name = N'Utente' AND Object_ID = Object_ID(N'TipoAtto'))
BEGIN
ALTER TABLE TipoAtto ADD [Utente] nvarchar (50);
END

IF NOT EXISTS(SELECT 1 FROM sys.columns WHERE Name = N'DataAggiornamento' AND Object_ID = Object_ID(N'TipoAtto'))
BEGIN
ALTER TABLE TipoAtto ADD [DataAggiornamento] datetime;
END

IF NOT EXISTS(SELECT 1 FROM sys.columns WHERE Name = N'Codice' AND Object_ID = Object_ID(N'TipoAtto'))
BEGIN
ALTER TABLE TipoAtto ADD [Codice] nvarchar (2);
END

IF NOT EXISTS(SELECT 1 FROM sys.columns WHERE Name = N'Descrizione' AND Object_ID = Object_ID(N'TipoAtto'))
BEGIN
ALTER TABLE TipoAtto ADD [Descrizione] nvarchar (50);
END

IF NOT EXISTS(SELECT name FROM sys.sysobjects  WHERE Name = N'TipoBene' AND xtype = N'U')
BEGIN
CREATE TABLE TipoBene([ID] [int] IDENTITY(1,1) NOT NULL) ON [PRIMARY];
END

IF NOT EXISTS(SELECT 1 FROM sys.columns WHERE Name = N'Utente' AND Object_ID = Object_ID(N'TipoBene'))
BEGIN
ALTER TABLE TipoBene ADD [Utente] nvarchar (50);
END

IF NOT EXISTS(SELECT 1 FROM sys.columns WHERE Name = N'DataAggiornamento' AND Object_ID = Object_ID(N'TipoBene'))
BEGIN
ALTER TABLE TipoBene ADD [DataAggiornamento] datetime;
END

IF NOT EXISTS(SELECT 1 FROM sys.columns WHERE Name = N'Codice' AND Object_ID = Object_ID(N'TipoBene'))
BEGIN
ALTER TABLE TipoBene ADD [Codice] nvarchar (2);
END

IF NOT EXISTS(SELECT 1 FROM sys.columns WHERE Name = N'Descrizione' AND Object_ID = Object_ID(N'TipoBene'))
BEGIN
ALTER TABLE TipoBene ADD [Descrizione] nvarchar (50);
END

IF NOT EXISTS(SELECT 1 FROM sys.columns WHERE Name = N'Tipo' AND Object_ID = Object_ID(N'TipoBene'))
BEGIN
ALTER TABLE TipoBene ADD [Tipo] nvarchar (4);
END

IF NOT EXISTS(SELECT name FROM sys.sysobjects  WHERE Name = N'TipoPagamento' AND xtype = N'U')
BEGIN
CREATE TABLE TipoPagamento([ID] [int] IDENTITY(1,1) NOT NULL) ON [PRIMARY];
END

IF NOT EXISTS(SELECT 1 FROM sys.columns WHERE Name = N'Utente' AND Object_ID = Object_ID(N'TipoPagamento'))
BEGIN
ALTER TABLE TipoPagamento ADD [Utente] nvarchar (50);
END

IF NOT EXISTS(SELECT 1 FROM sys.columns WHERE Name = N'DataAggiornamento' AND Object_ID = Object_ID(N'TipoPagamento'))
BEGIN
ALTER TABLE TipoPagamento ADD [DataAggiornamento] datetime;
END

IF NOT EXISTS(SELECT 1 FROM sys.columns WHERE Name = N'Codice' AND Object_ID = Object_ID(N'TipoPagamento'))
BEGIN
ALTER TABLE TipoPagamento ADD [Codice] nvarchar (2);
END

IF NOT EXISTS(SELECT 1 FROM sys.columns WHERE Name = N'Descrizione' AND Object_ID = Object_ID(N'TipoPagamento'))
BEGIN
ALTER TABLE TipoPagamento ADD [Descrizione] nvarchar (60);
END

IF NOT EXISTS(SELECT 1 FROM sys.columns WHERE Name = N'Scadenze' AND Object_ID = Object_ID(N'TipoPagamento'))
BEGIN
ALTER TABLE TipoPagamento ADD [Scadenze] tinyint;
END

IF NOT EXISTS(SELECT 1 FROM sys.columns WHERE Name = N'Tipo' AND Object_ID = Object_ID(N'TipoPagamento'))
BEGIN
ALTER TABLE TipoPagamento ADD [Tipo] nvarchar (1);
END

IF NOT EXISTS(SELECT 1 FROM sys.columns WHERE Name = N'GiorniPrima' AND Object_ID = Object_ID(N'TipoPagamento'))
BEGIN
ALTER TABLE TipoPagamento ADD [GiorniPrima] smallint;
END

IF NOT EXISTS(SELECT 1 FROM sys.columns WHERE Name = N'GiorniSeconda' AND Object_ID = Object_ID(N'TipoPagamento'))
BEGIN
ALTER TABLE TipoPagamento ADD [GiorniSeconda] smallint;
END

IF NOT EXISTS(SELECT 1 FROM sys.columns WHERE Name = N'GiorniAltre' AND Object_ID = Object_ID(N'TipoPagamento'))
BEGIN
ALTER TABLE TipoPagamento ADD [GiorniAltre] smallint;
END

IF NOT EXISTS(SELECT 1 FROM sys.columns WHERE Name = N'Spese' AND Object_ID = Object_ID(N'TipoPagamento'))
BEGIN
ALTER TABLE TipoPagamento ADD [Spese] float;
END

IF NOT EXISTS(SELECT 1 FROM sys.columns WHERE Name = N'Per' AND Object_ID = Object_ID(N'TipoPagamento'))
BEGIN
ALTER TABLE TipoPagamento ADD [Per] nvarchar (1);
END

IF NOT EXISTS(SELECT 1 FROM sys.columns WHERE Name = N'CampoFE' AND Object_ID = Object_ID(N'TipoPagamento'))
BEGIN
ALTER TABLE TipoPagamento ADD [CampoFE] nvarchar (50);
END

IF NOT EXISTS(SELECT name FROM sys.sysobjects  WHERE Name = N'TipoRegistro' AND xtype = N'U')
BEGIN
CREATE TABLE TipoRegistro([ID] [int] IDENTITY(1,1) NOT NULL) ON [PRIMARY];
END

IF NOT EXISTS(SELECT 1 FROM sys.columns WHERE Name = N'Utente' AND Object_ID = Object_ID(N'TipoRegistro'))
BEGIN
ALTER TABLE TipoRegistro ADD [Utente] nvarchar (50);
END

IF NOT EXISTS(SELECT 1 FROM sys.columns WHERE Name = N'DataAggiornamento' AND Object_ID = Object_ID(N'TipoRegistro'))
BEGIN
ALTER TABLE TipoRegistro ADD [DataAggiornamento] datetime;
END

IF NOT EXISTS(SELECT 1 FROM sys.columns WHERE Name = N'Tipo' AND Object_ID = Object_ID(N'TipoRegistro'))
BEGIN
ALTER TABLE TipoRegistro ADD [Tipo] tinyint;
END

IF NOT EXISTS(SELECT 1 FROM sys.columns WHERE Name = N'Descrizione' AND Object_ID = Object_ID(N'TipoRegistro'))
BEGIN
ALTER TABLE TipoRegistro ADD [Descrizione] nvarchar (50);
END

IF NOT EXISTS(SELECT 1 FROM sys.columns WHERE Name = N'TipoIva' AND Object_ID = Object_ID(N'TipoRegistro'))
BEGIN
ALTER TABLE TipoRegistro ADD [TipoIva] nvarchar (1);
END

IF NOT EXISTS(SELECT 1 FROM sys.columns WHERE Name = N'TipoAcqVnt' AND Object_ID = Object_ID(N'TipoRegistro'))
BEGIN
ALTER TABLE TipoRegistro ADD [TipoAcqVnt] tinyint;
END

IF NOT EXISTS(SELECT 1 FROM sys.columns WHERE Name = N'Protocollo' AND Object_ID = Object_ID(N'TipoRegistro'))
BEGIN
ALTER TABLE TipoRegistro ADD [Protocollo] int;
END

IF NOT EXISTS(SELECT 1 FROM sys.columns WHERE Name = N'IndicatoreRegistro' AND Object_ID = Object_ID(N'TipoRegistro'))
BEGIN
ALTER TABLE TipoRegistro ADD [IndicatoreRegistro] nvarchar (1);
END

IF NOT EXISTS(SELECT name FROM sys.sysobjects  WHERE Name = N'TipoScadenza' AND xtype = N'U')
BEGIN
CREATE TABLE TipoScadenza([ID] [int] IDENTITY(1,1) NOT NULL) ON [PRIMARY];
END

IF NOT EXISTS(SELECT 1 FROM sys.columns WHERE Name = N'Utente' AND Object_ID = Object_ID(N'TipoScadenza'))
BEGIN
ALTER TABLE TipoScadenza ADD [Utente] nvarchar (50);
END

IF NOT EXISTS(SELECT 1 FROM sys.columns WHERE Name = N'DataAggiornamento' AND Object_ID = Object_ID(N'TipoScadenza'))
BEGIN
ALTER TABLE TipoScadenza ADD [DataAggiornamento] datetime;
END

IF NOT EXISTS(SELECT 1 FROM sys.columns WHERE Name = N'Codice' AND Object_ID = Object_ID(N'TipoScadenza'))
BEGIN
ALTER TABLE TipoScadenza ADD [Codice] nvarchar (1);
END

IF NOT EXISTS(SELECT 1 FROM sys.columns WHERE Name = N'Descrizione' AND Object_ID = Object_ID(N'TipoScadenza'))
BEGIN
ALTER TABLE TipoScadenza ADD [Descrizione] nvarchar (50);
END

IF NOT EXISTS(SELECT 1 FROM sys.columns WHERE Name = N'CausaleContabile' AND Object_ID = Object_ID(N'TipoScadenza'))
BEGIN
ALTER TABLE TipoScadenza ADD [CausaleContabile] nvarchar (3);
END

IF NOT EXISTS(SELECT 1 FROM sys.columns WHERE Name = N'Giorno' AND Object_ID = Object_ID(N'TipoScadenza'))
BEGIN
ALTER TABLE TipoScadenza ADD [Giorno] tinyint;
END

IF NOT EXISTS(SELECT name FROM sys.sysobjects  WHERE Name = N'UnitaMisura' AND xtype = N'U')
BEGIN
CREATE TABLE UnitaMisura([ID] [int] IDENTITY(1,1) NOT NULL) ON [PRIMARY];
END

IF NOT EXISTS(SELECT 1 FROM sys.columns WHERE Name = N'Utente' AND Object_ID = Object_ID(N'UnitaMisura'))
BEGIN
ALTER TABLE UnitaMisura ADD [Utente] nvarchar (50);
END

IF NOT EXISTS(SELECT 1 FROM sys.columns WHERE Name = N'DataAggiornamento' AND Object_ID = Object_ID(N'UnitaMisura'))
BEGIN
ALTER TABLE UnitaMisura ADD [DataAggiornamento] datetime;
END

IF NOT EXISTS(SELECT 1 FROM sys.columns WHERE Name = N'Codice' AND Object_ID = Object_ID(N'UnitaMisura'))
BEGIN
ALTER TABLE UnitaMisura ADD [Codice] nvarchar (2);
END

IF NOT EXISTS(SELECT 1 FROM sys.columns WHERE Name = N'Descrizione' AND Object_ID = Object_ID(N'UnitaMisura'))
BEGIN
ALTER TABLE UnitaMisura ADD [Descrizione] nvarchar (50);
END

IF NOT EXISTS(SELECT name FROM sys.sysobjects  WHERE Name = N'ProtocolloAnagrafiche' AND xtype = N'U')
BEGIN
CREATE TABLE ProtocolloAnagrafiche([ID] [int] IDENTITY(1,1) NOT NULL) ON [PRIMARY];
END

IF NOT EXISTS(SELECT 1 FROM sys.columns WHERE Name = N'Anno' AND Object_ID = Object_ID(N'ProtocolloAnagrafiche'))
BEGIN
ALTER TABLE ProtocolloAnagrafiche ADD [Anno] int;
END

IF NOT EXISTS(SELECT 1 FROM sys.columns WHERE Name = N'Numero' AND Object_ID = Object_ID(N'ProtocolloAnagrafiche'))
BEGIN
ALTER TABLE ProtocolloAnagrafiche ADD [Numero] int;
END

IF NOT EXISTS(SELECT 1 FROM sys.columns WHERE Name = N'Tipo' AND Object_ID = Object_ID(N'ProtocolloAnagrafiche'))
BEGIN
ALTER TABLE ProtocolloAnagrafiche ADD [Tipo] varchar (1);
END

IF NOT EXISTS(SELECT 1 FROM sys.columns WHERE Name = N'IdRubrica' AND Object_ID = Object_ID(N'ProtocolloAnagrafiche'))
BEGIN
ALTER TABLE ProtocolloAnagrafiche ADD [IdRubrica] int;
END

IF NOT EXISTS(SELECT 1 FROM sys.columns WHERE Name = N'IdClienteFornitore' AND Object_ID = Object_ID(N'ProtocolloAnagrafiche'))
BEGIN
ALTER TABLE ProtocolloAnagrafiche ADD [IdClienteFornitore] int;
END

IF NOT EXISTS(SELECT 1 FROM sys.columns WHERE Name = N'IdAnagrafica' AND Object_ID = Object_ID(N'ProtocolloAnagrafiche'))
BEGIN
ALTER TABLE ProtocolloAnagrafiche ADD [IdAnagrafica] int;
END

IF NOT EXISTS(SELECT 1 FROM sys.columns WHERE Name = N'IdOspite' AND Object_ID = Object_ID(N'ProtocolloAnagrafiche'))
BEGIN
ALTER TABLE ProtocolloAnagrafiche ADD [IdOspite] int;
END

IF NOT EXISTS(SELECT 1 FROM sys.columns WHERE Name = N'IdParente' AND Object_ID = Object_ID(N'ProtocolloAnagrafiche'))
BEGIN
ALTER TABLE ProtocolloAnagrafiche ADD [IdParente] int;
END

IF NOT EXISTS(SELECT name FROM sys.sysobjects  WHERE Name = N'Rubrica' AND xtype = N'U')
BEGIN
CREATE TABLE Rubrica([ID] [int] IDENTITY(1,1) NOT NULL) ON [PRIMARY];
END

IF NOT EXISTS(SELECT 1 FROM sys.columns WHERE Name = N'Appellativo' AND Object_ID = Object_ID(N'Rubrica'))
BEGIN
ALTER TABLE Rubrica ADD [Appellativo] int;
END

IF NOT EXISTS(SELECT 1 FROM sys.columns WHERE Name = N'Denominazione' AND Object_ID = Object_ID(N'Rubrica'))
BEGIN
ALTER TABLE Rubrica ADD [Denominazione] varchar (150);
END

IF NOT EXISTS(SELECT 1 FROM sys.columns WHERE Name = N'Cognome' AND Object_ID = Object_ID(N'Rubrica'))
BEGIN
ALTER TABLE Rubrica ADD [Cognome] varchar (100);
END

IF NOT EXISTS(SELECT 1 FROM sys.columns WHERE Name = N'Nome' AND Object_ID = Object_ID(N'Rubrica'))
BEGIN
ALTER TABLE Rubrica ADD [Nome] varchar (100);
END

IF NOT EXISTS(SELECT 1 FROM sys.columns WHERE Name = N'DataNascita' AND Object_ID = Object_ID(N'Rubrica'))
BEGIN
ALTER TABLE Rubrica ADD [DataNascita] datetime;
END

IF NOT EXISTS(SELECT 1 FROM sys.columns WHERE Name = N'Indirizzo' AND Object_ID = Object_ID(N'Rubrica'))
BEGIN
ALTER TABLE Rubrica ADD [Indirizzo] varchar (250);
END

IF NOT EXISTS(SELECT 1 FROM sys.columns WHERE Name = N'Provincia' AND Object_ID = Object_ID(N'Rubrica'))
BEGIN
ALTER TABLE Rubrica ADD [Provincia] varchar (3);
END

IF NOT EXISTS(SELECT 1 FROM sys.columns WHERE Name = N'Comune' AND Object_ID = Object_ID(N'Rubrica'))
BEGIN
ALTER TABLE Rubrica ADD [Comune] varchar (3);
END

IF NOT EXISTS(SELECT 1 FROM sys.columns WHERE Name = N'Cap' AND Object_ID = Object_ID(N'Rubrica'))
BEGIN
ALTER TABLE Rubrica ADD [Cap] varchar (6);
END

IF NOT EXISTS(SELECT 1 FROM sys.columns WHERE Name = N'Localita' AND Object_ID = Object_ID(N'Rubrica'))
BEGIN
ALTER TABLE Rubrica ADD [Localita] varchar (150);
END

IF NOT EXISTS(SELECT 1 FROM sys.columns WHERE Name = N'Ragruppamento' AND Object_ID = Object_ID(N'Rubrica'))
BEGIN
ALTER TABLE Rubrica ADD [Ragruppamento] varchar (50);
END

IF NOT EXISTS(SELECT 1 FROM sys.columns WHERE Name = N'Note' AND Object_ID = Object_ID(N'Rubrica'))
BEGIN
ALTER TABLE Rubrica ADD [Note] text;
END

IF NOT EXISTS(SELECT 1 FROM sys.columns WHERE Name = N'DataModifica' AND Object_ID = Object_ID(N'Rubrica'))
BEGIN
ALTER TABLE Rubrica ADD [DataModifica] datetime;
END

IF NOT EXISTS(SELECT 1 FROM sys.columns WHERE Name = N'Mail' AND Object_ID = Object_ID(N'Rubrica'))
BEGIN
ALTER TABLE Rubrica ADD [Mail] varchar (250);
END

IF NOT EXISTS(SELECT 1 FROM sys.columns WHERE Name = N'Utente' AND Object_ID = Object_ID(N'Rubrica'))
BEGIN
ALTER TABLE Rubrica ADD [Utente] varchar (50);
END

IF NOT EXISTS(SELECT 1 FROM sys.columns WHERE Name = N'EMail1' AND Object_ID = Object_ID(N'Rubrica'))
BEGIN
ALTER TABLE Rubrica ADD [EMail1] varchar (50);
END

IF NOT EXISTS(SELECT 1 FROM sys.columns WHERE Name = N'EMail2' AND Object_ID = Object_ID(N'Rubrica'))
BEGIN
ALTER TABLE Rubrica ADD [EMail2] varchar (50);
END

IF NOT EXISTS(SELECT 1 FROM sys.columns WHERE Name = N'Telefono1' AND Object_ID = Object_ID(N'Rubrica'))
BEGIN
ALTER TABLE Rubrica ADD [Telefono1] varchar (50);
END

IF NOT EXISTS(SELECT 1 FROM sys.columns WHERE Name = N'Telefono2' AND Object_ID = Object_ID(N'Rubrica'))
BEGIN
ALTER TABLE Rubrica ADD [Telefono2] varchar (50);
END

IF NOT EXISTS(SELECT 1 FROM sys.columns WHERE Name = N'InvioComunicazione' AND Object_ID = Object_ID(N'Rubrica'))
BEGIN
ALTER TABLE Rubrica ADD [InvioComunicazione] int;
END

IF NOT EXISTS(SELECT name FROM sys.sysobjects  WHERE Name = N'RaggruppamentoRubrica' AND xtype = N'U')
BEGIN
CREATE TABLE RaggruppamentoRubrica([ID] [int] IDENTITY(1,1) NOT NULL) ON [PRIMARY];
END

IF NOT EXISTS(SELECT 1 FROM sys.columns WHERE Name = N'IdRaggruppamento' AND Object_ID = Object_ID(N'RaggruppamentoRubrica'))
BEGIN
ALTER TABLE RaggruppamentoRubrica ADD [IdRaggruppamento] int;
END

IF NOT EXISTS(SELECT 1 FROM sys.columns WHERE Name = N'Descrizione' AND Object_ID = Object_ID(N'RaggruppamentoRubrica'))
BEGIN
ALTER TABLE RaggruppamentoRubrica ADD [Descrizione] varchar (150);
END

IF NOT EXISTS(SELECT name FROM sys.sysobjects  WHERE Name = N'TabellaMezzo' AND xtype = N'U')
BEGIN
CREATE TABLE TabellaMezzo([ID] [int] IDENTITY(1,1) NOT NULL) ON [PRIMARY];
END

IF NOT EXISTS(SELECT 1 FROM sys.columns WHERE Name = N'IdMezzo' AND Object_ID = Object_ID(N'TabellaMezzo'))
BEGIN
ALTER TABLE TabellaMezzo ADD [IdMezzo] int;
END

IF NOT EXISTS(SELECT 1 FROM sys.columns WHERE Name = N'Descrizione' AND Object_ID = Object_ID(N'TabellaMezzo'))
BEGIN
ALTER TABLE TabellaMezzo ADD [Descrizione] varchar (50);
END

IF NOT EXISTS(SELECT name FROM sys.sysobjects  WHERE Name = N'TabellaUfficioDestinatario' AND xtype = N'U')
BEGIN
CREATE TABLE TabellaUfficioDestinatario([ID] [int] IDENTITY(1,1) NOT NULL) ON [PRIMARY];
END

IF NOT EXISTS(SELECT 1 FROM sys.columns WHERE Name = N'IdUfficioDestinatario' AND Object_ID = Object_ID(N'TabellaUfficioDestinatario'))
BEGIN
ALTER TABLE TabellaUfficioDestinatario ADD [IdUfficioDestinatario] int;
END

IF NOT EXISTS(SELECT 1 FROM sys.columns WHERE Name = N'Descrizione' AND Object_ID = Object_ID(N'TabellaUfficioDestinatario'))
BEGIN
ALTER TABLE TabellaUfficioDestinatario ADD [Descrizione] varchar (50);
END

IF NOT EXISTS(SELECT name FROM sys.sysobjects  WHERE Name = N'TabellaTitolo' AND xtype = N'U')
BEGIN
CREATE TABLE TabellaTitolo([ID] [int] IDENTITY(1,1) NOT NULL) ON [PRIMARY];
END

IF NOT EXISTS(SELECT 1 FROM sys.columns WHERE Name = N'idTitolo' AND Object_ID = Object_ID(N'TabellaTitolo'))
BEGIN
ALTER TABLE TabellaTitolo ADD [idTitolo] int;
END

IF NOT EXISTS(SELECT 1 FROM sys.columns WHERE Name = N'Descrizione' AND Object_ID = Object_ID(N'TabellaTitolo'))
BEGIN
ALTER TABLE TabellaTitolo ADD [Descrizione] varchar (50);
END

IF NOT EXISTS(SELECT 1 FROM sys.columns WHERE Name = N'Codice' AND Object_ID = Object_ID(N'TabellaTitolo'))
BEGIN
ALTER TABLE TabellaTitolo ADD [Codice] varchar (50);
END

IF NOT EXISTS(SELECT name FROM sys.sysobjects  WHERE Name = N'Allegati' AND xtype = N'U')
BEGIN
CREATE TABLE Allegati([ID] [int] IDENTITY(1,1) NOT NULL) ON [PRIMARY];
END

IF NOT EXISTS(SELECT 1 FROM sys.columns WHERE Name = N'Anno' AND Object_ID = Object_ID(N'Allegati'))
BEGIN
ALTER TABLE Allegati ADD [Anno] int;
END

IF NOT EXISTS(SELECT 1 FROM sys.columns WHERE Name = N'NumeroProtocollo' AND Object_ID = Object_ID(N'Allegati'))
BEGIN
ALTER TABLE Allegati ADD [NumeroProtocollo] int;
END

IF NOT EXISTS(SELECT 1 FROM sys.columns WHERE Name = N'Tipo' AND Object_ID = Object_ID(N'Allegati'))
BEGIN
ALTER TABLE Allegati ADD [Tipo] varchar (2);
END

IF NOT EXISTS(SELECT 1 FROM sys.columns WHERE Name = N'NomeFile' AND Object_ID = Object_ID(N'Allegati'))
BEGIN
ALTER TABLE Allegati ADD [NomeFile] varchar (150);
END

IF NOT EXISTS(SELECT 1 FROM sys.columns WHERE Name = N'DATA' AND Object_ID = Object_ID(N'Allegati'))
BEGIN
ALTER TABLE Allegati ADD [DATA] datetime;
END

IF NOT EXISTS(SELECT 1 FROM sys.columns WHERE Name = N'DESCRIZIONE' AND Object_ID = Object_ID(N'Allegati'))
BEGIN
ALTER TABLE Allegati ADD [DESCRIZIONE] varchar (250);
END

IF NOT EXISTS(SELECT name FROM sys.sysobjects  WHERE Name = N'Tabella_Appellativo' AND xtype = N'U')
BEGIN
CREATE TABLE Tabella_Appellativo([ID] [int] IDENTITY(1,1) NOT NULL) ON [PRIMARY];
END

IF NOT EXISTS(SELECT 1 FROM sys.columns WHERE Name = N'idTitolo' AND Object_ID = Object_ID(N'Tabella_Appellativo'))
BEGIN
ALTER TABLE Tabella_Appellativo ADD [idTitolo] int;
END

IF NOT EXISTS(SELECT 1 FROM sys.columns WHERE Name = N'Descrizione' AND Object_ID = Object_ID(N'Tabella_Appellativo'))
BEGIN
ALTER TABLE Tabella_Appellativo ADD [Descrizione] varchar (50);
END

IF NOT EXISTS(SELECT name FROM sys.sysobjects  WHERE Name = N'ParametriProtocollo' AND xtype = N'U')
BEGIN
CREATE TABLE ParametriProtocollo([ID] [int] IDENTITY(1,1) NOT NULL) ON [PRIMARY];
END

IF NOT EXISTS(SELECT 1 FROM sys.columns WHERE Name = N'Pagina' AND Object_ID = Object_ID(N'ParametriProtocollo'))
BEGIN
ALTER TABLE ParametriProtocollo ADD [Pagina] int;
END

IF NOT EXISTS(SELECT 1 FROM sys.columns WHERE Name = N'NumeroRiga' AND Object_ID = Object_ID(N'ParametriProtocollo'))
BEGIN
ALTER TABLE ParametriProtocollo ADD [NumeroRiga] int;
END

IF NOT EXISTS(SELECT 1 FROM sys.columns WHERE Name = N'UltimaStampa' AND Object_ID = Object_ID(N'ParametriProtocollo'))
BEGIN
ALTER TABLE ParametriProtocollo ADD [UltimaStampa] date;
END

IF NOT EXISTS(SELECT name FROM sys.sysobjects  WHERE Name = N'TabellaClasse' AND xtype = N'U')
BEGIN
CREATE TABLE TabellaClasse([ID] [int] IDENTITY(1,1) NOT NULL) ON [PRIMARY];
END

IF NOT EXISTS(SELECT 1 FROM sys.columns WHERE Name = N'idClasse' AND Object_ID = Object_ID(N'TabellaClasse'))
BEGIN
ALTER TABLE TabellaClasse ADD [idClasse] int;
END

IF NOT EXISTS(SELECT 1 FROM sys.columns WHERE Name = N'Descrizione' AND Object_ID = Object_ID(N'TabellaClasse'))
BEGIN
ALTER TABLE TabellaClasse ADD [Descrizione] varchar (100);
END

IF NOT EXISTS(SELECT 1 FROM sys.columns WHERE Name = N'idTitolo' AND Object_ID = Object_ID(N'TabellaClasse'))
BEGIN
ALTER TABLE TabellaClasse ADD [idTitolo] int;
END

IF NOT EXISTS(SELECT name FROM sys.sysobjects  WHERE Name = N'TabellaIndice' AND xtype = N'U')
BEGIN
CREATE TABLE TabellaIndice([ID] [int] IDENTITY(1,1) NOT NULL) ON [PRIMARY];
END

IF NOT EXISTS(SELECT 1 FROM sys.columns WHERE Name = N'idIndice' AND Object_ID = Object_ID(N'TabellaIndice'))
BEGIN
ALTER TABLE TabellaIndice ADD [idIndice] int;
END

IF NOT EXISTS(SELECT 1 FROM sys.columns WHERE Name = N'Descrizione' AND Object_ID = Object_ID(N'TabellaIndice'))
BEGIN
ALTER TABLE TabellaIndice ADD [Descrizione] varchar (100);
END

IF NOT EXISTS(SELECT 1 FROM sys.columns WHERE Name = N'idTitolo' AND Object_ID = Object_ID(N'TabellaIndice'))
BEGIN
ALTER TABLE TabellaIndice ADD [idTitolo] int;
END

IF NOT EXISTS(SELECT 1 FROM sys.columns WHERE Name = N'idClasse' AND Object_ID = Object_ID(N'TabellaIndice'))
BEGIN
ALTER TABLE TabellaIndice ADD [idClasse] int;
END

IF NOT EXISTS(SELECT name FROM sys.sysobjects  WHERE Name = N'Protocollo' AND xtype = N'U')
BEGIN
CREATE TABLE Protocollo([ID] [int] IDENTITY(1,1) NOT NULL) ON [PRIMARY];
END

IF NOT EXISTS(SELECT 1 FROM sys.columns WHERE Name = N'Numero' AND Object_ID = Object_ID(N'Protocollo'))
BEGIN
ALTER TABLE Protocollo ADD [Numero] int;
END

IF NOT EXISTS(SELECT 1 FROM sys.columns WHERE Name = N'Tipo' AND Object_ID = Object_ID(N'Protocollo'))
BEGIN
ALTER TABLE Protocollo ADD [Tipo] varchar (1);
END

IF NOT EXISTS(SELECT 1 FROM sys.columns WHERE Name = N'Anno' AND Object_ID = Object_ID(N'Protocollo'))
BEGIN
ALTER TABLE Protocollo ADD [Anno] int;
END

IF NOT EXISTS(SELECT 1 FROM sys.columns WHERE Name = N'Titolo' AND Object_ID = Object_ID(N'Protocollo'))
BEGIN
ALTER TABLE Protocollo ADD [Titolo] int;
END

IF NOT EXISTS(SELECT 1 FROM sys.columns WHERE Name = N'NumeroProtocolloPrecedente' AND Object_ID = Object_ID(N'Protocollo'))
BEGIN
ALTER TABLE Protocollo ADD [NumeroProtocolloPrecedente] int;
END

IF NOT EXISTS(SELECT 1 FROM sys.columns WHERE Name = N'AnnoProtocolloPrecedente' AND Object_ID = Object_ID(N'Protocollo'))
BEGIN
ALTER TABLE Protocollo ADD [AnnoProtocolloPrecedente] int;
END

IF NOT EXISTS(SELECT 1 FROM sys.columns WHERE Name = N'NumeroProtocolloSuccessivo' AND Object_ID = Object_ID(N'Protocollo'))
BEGIN
ALTER TABLE Protocollo ADD [NumeroProtocolloSuccessivo] int;
END

IF NOT EXISTS(SELECT 1 FROM sys.columns WHERE Name = N'AnnoProtocolloSuccessivo' AND Object_ID = Object_ID(N'Protocollo'))
BEGIN
ALTER TABLE Protocollo ADD [AnnoProtocolloSuccessivo] int;
END

IF NOT EXISTS(SELECT 1 FROM sys.columns WHERE Name = N'DataDocumento' AND Object_ID = Object_ID(N'Protocollo'))
BEGIN
ALTER TABLE Protocollo ADD [DataDocumento] datetime;
END

IF NOT EXISTS(SELECT 1 FROM sys.columns WHERE Name = N'DataArrivo' AND Object_ID = Object_ID(N'Protocollo'))
BEGIN
ALTER TABLE Protocollo ADD [DataArrivo] datetime;
END

IF NOT EXISTS(SELECT 1 FROM sys.columns WHERE Name = N'NumeroDocumento' AND Object_ID = Object_ID(N'Protocollo'))
BEGIN
ALTER TABLE Protocollo ADD [NumeroDocumento] varchar (50);
END

IF NOT EXISTS(SELECT 1 FROM sys.columns WHERE Name = N'RagioneSociale' AND Object_ID = Object_ID(N'Protocollo'))
BEGIN
ALTER TABLE Protocollo ADD [RagioneSociale] varchar (150);
END

IF NOT EXISTS(SELECT 1 FROM sys.columns WHERE Name = N'Indirizzo' AND Object_ID = Object_ID(N'Protocollo'))
BEGIN
ALTER TABLE Protocollo ADD [Indirizzo] varchar (100);
END

IF NOT EXISTS(SELECT 1 FROM sys.columns WHERE Name = N'Citta' AND Object_ID = Object_ID(N'Protocollo'))
BEGIN
ALTER TABLE Protocollo ADD [Citta] varchar (50);
END

IF NOT EXISTS(SELECT 1 FROM sys.columns WHERE Name = N'Cap' AND Object_ID = Object_ID(N'Protocollo'))
BEGIN
ALTER TABLE Protocollo ADD [Cap] varchar (6);
END

IF NOT EXISTS(SELECT 1 FROM sys.columns WHERE Name = N'Provincia' AND Object_ID = Object_ID(N'Protocollo'))
BEGIN
ALTER TABLE Protocollo ADD [Provincia] varchar (2);
END

IF NOT EXISTS(SELECT 1 FROM sys.columns WHERE Name = N'Stato' AND Object_ID = Object_ID(N'Protocollo'))
BEGIN
ALTER TABLE Protocollo ADD [Stato] varchar (50);
END

IF NOT EXISTS(SELECT 1 FROM sys.columns WHERE Name = N'Mezzo' AND Object_ID = Object_ID(N'Protocollo'))
BEGIN
ALTER TABLE Protocollo ADD [Mezzo] int;
END

IF NOT EXISTS(SELECT 1 FROM sys.columns WHERE Name = N'Allegati' AND Object_ID = Object_ID(N'Protocollo'))
BEGIN
ALTER TABLE Protocollo ADD [Allegati] int;
END

IF NOT EXISTS(SELECT 1 FROM sys.columns WHERE Name = N'UfficioDestinatario' AND Object_ID = Object_ID(N'Protocollo'))
BEGIN
ALTER TABLE Protocollo ADD [UfficioDestinatario] int;
END

IF NOT EXISTS(SELECT 1 FROM sys.columns WHERE Name = N'Oggetto' AND Object_ID = Object_ID(N'Protocollo'))
BEGIN
ALTER TABLE Protocollo ADD [Oggetto] ntext;
END

IF NOT EXISTS(SELECT 1 FROM sys.columns WHERE Name = N'Note' AND Object_ID = Object_ID(N'Protocollo'))
BEGIN
ALTER TABLE Protocollo ADD [Note] ntext;
END

IF NOT EXISTS(SELECT 1 FROM sys.columns WHERE Name = N'Utente' AND Object_ID = Object_ID(N'Protocollo'))
BEGIN
ALTER TABLE Protocollo ADD [Utente] varchar (50);
END

IF NOT EXISTS(SELECT 1 FROM sys.columns WHERE Name = N'DataModifica' AND Object_ID = Object_ID(N'Protocollo'))
BEGIN
ALTER TABLE Protocollo ADD [DataModifica] datetime;
END

IF NOT EXISTS(SELECT 1 FROM sys.columns WHERE Name = N'TipoProtocolloSuccessivo' AND Object_ID = Object_ID(N'Protocollo'))
BEGIN
ALTER TABLE Protocollo ADD [TipoProtocolloSuccessivo] varchar (1);
END

IF NOT EXISTS(SELECT 1 FROM sys.columns WHERE Name = N'TipoProtocolloPrecedente' AND Object_ID = Object_ID(N'Protocollo'))
BEGIN
ALTER TABLE Protocollo ADD [TipoProtocolloPrecedente] varchar (1);
END

IF NOT EXISTS(SELECT 1 FROM sys.columns WHERE Name = N'idProtTop' AND Object_ID = Object_ID(N'Protocollo'))
BEGIN
ALTER TABLE Protocollo ADD [idProtTop] int;
END

IF NOT EXISTS(SELECT 1 FROM sys.columns WHERE Name = N'Classe' AND Object_ID = Object_ID(N'Protocollo'))
BEGIN
ALTER TABLE Protocollo ADD [Classe] int;
END

IF NOT EXISTS(SELECT 1 FROM sys.columns WHERE Name = N'Indice' AND Object_ID = Object_ID(N'Protocollo'))
BEGIN
ALTER TABLE Protocollo ADD [Indice] int;
END

IF NOT EXISTS(SELECT 1 FROM sys.columns WHERE Name = N'Fascicolo' AND Object_ID = Object_ID(N'Protocollo'))
BEGIN
ALTER TABLE Protocollo ADD [Fascicolo] varchar (10);
END

IF NOT EXISTS(SELECT 1 FROM sys.columns WHERE Name = N'OraArrivo' AND Object_ID = Object_ID(N'Protocollo'))
BEGIN
ALTER TABLE Protocollo ADD [OraArrivo] datetime;
END

IF NOT EXISTS(SELECT 1 FROM sys.columns WHERE Name = N'FascicoloId' AND Object_ID = Object_ID(N'Protocollo'))
BEGIN
ALTER TABLE Protocollo ADD [FascicoloId] int;
END

IF NOT EXISTS(SELECT name FROM sys.sysobjects  WHERE Name = N'TabellaFascicolo' AND xtype = N'U')
BEGIN
CREATE TABLE TabellaFascicolo([ID] [int] IDENTITY(1,1) NOT NULL) ON [PRIMARY];
END

IF NOT EXISTS(SELECT 1 FROM sys.columns WHERE Name = N'IdFascicolo' AND Object_ID = Object_ID(N'TabellaFascicolo'))
BEGIN
ALTER TABLE TabellaFascicolo ADD [IdFascicolo] int;
END

IF NOT EXISTS(SELECT 1 FROM sys.columns WHERE Name = N'IdUfficioDestinatario' AND Object_ID = Object_ID(N'TabellaFascicolo'))
BEGIN
ALTER TABLE TabellaFascicolo ADD [IdUfficioDestinatario] int;
END

IF NOT EXISTS(SELECT 1 FROM sys.columns WHERE Name = N'Descrizione' AND Object_ID = Object_ID(N'TabellaFascicolo'))
BEGIN
ALTER TABLE TabellaFascicolo ADD [Descrizione] varchar (50);
END

IF NOT EXISTS(SELECT 1 FROM sys.columns WHERE Name = N'Chiuso' AND Object_ID = Object_ID(N'TabellaFascicolo'))
BEGIN
ALTER TABLE TabellaFascicolo ADD [Chiuso] int;
END

IF NOT EXISTS(SELECT 1 FROM sys.columns WHERE Name = N'DataScadenza' AND Object_ID = Object_ID(N'TabellaFascicolo'))
BEGIN
ALTER TABLE TabellaFascicolo ADD [DataScadenza] date;
END

IF NOT EXISTS(SELECT 1 FROM sys.columns WHERE Name = N'CodiceFascicolo' AND Object_ID = Object_ID(N'TabellaFascicolo'))
BEGIN
ALTER TABLE TabellaFascicolo ADD [CodiceFascicolo] varchar (50);
END

IF NOT EXISTS(SELECT name FROM sys.sysobjects  WHERE Name = N'Bolli' AND xtype = N'U')
BEGIN
CREATE TABLE Bolli([ID] [int] IDENTITY(1,1) NOT NULL) ON [PRIMARY];
END

IF NOT EXISTS(SELECT 1 FROM sys.columns WHERE Name = N'Utente' AND Object_ID = Object_ID(N'Bolli'))
BEGIN
ALTER TABLE Bolli ADD [Utente] nvarchar (50);
END

IF NOT EXISTS(SELECT 1 FROM sys.columns WHERE Name = N'DataAggiornamento' AND Object_ID = Object_ID(N'Bolli'))
BEGIN
ALTER TABLE Bolli ADD [DataAggiornamento] datetime;
END

IF NOT EXISTS(SELECT 1 FROM sys.columns WHERE Name = N'DataValidita' AND Object_ID = Object_ID(N'Bolli'))
BEGIN
ALTER TABLE Bolli ADD [DataValidita] datetime;
END

IF NOT EXISTS(SELECT 1 FROM sys.columns WHERE Name = N'ImportoBollo' AND Object_ID = Object_ID(N'Bolli'))
BEGIN
ALTER TABLE Bolli ADD [ImportoBollo] float;
END

IF NOT EXISTS(SELECT 1 FROM sys.columns WHERE Name = N'ImportoApplicazione' AND Object_ID = Object_ID(N'Bolli'))
BEGIN
ALTER TABLE Bolli ADD [ImportoApplicazione] float;
END

IF NOT EXISTS(SELECT 1 FROM sys.columns WHERE Name = N'CodiceIVA' AND Object_ID = Object_ID(N'Bolli'))
BEGIN
ALTER TABLE Bolli ADD [CodiceIVA] nvarchar (2);
END

IF NOT EXISTS(SELECT name FROM sys.sysobjects  WHERE Name = N'CategoriaProdotto' AND xtype = N'U')
BEGIN
CREATE TABLE CategoriaProdotto([ID] [int] IDENTITY(1,1) NOT NULL) ON [PRIMARY];
END

IF NOT EXISTS(SELECT 1 FROM sys.columns WHERE Name = N'Utente' AND Object_ID = Object_ID(N'CategoriaProdotto'))
BEGIN
ALTER TABLE CategoriaProdotto ADD [Utente] nvarchar (50);
END

IF NOT EXISTS(SELECT 1 FROM sys.columns WHERE Name = N'DataAggiornamento' AND Object_ID = Object_ID(N'CategoriaProdotto'))
BEGIN
ALTER TABLE CategoriaProdotto ADD [DataAggiornamento] datetime;
END

IF NOT EXISTS(SELECT 1 FROM sys.columns WHERE Name = N'Codice' AND Object_ID = Object_ID(N'CategoriaProdotto'))
BEGIN
ALTER TABLE CategoriaProdotto ADD [Codice] nvarchar (2);
END

IF NOT EXISTS(SELECT 1 FROM sys.columns WHERE Name = N'Descrizione' AND Object_ID = Object_ID(N'CategoriaProdotto'))
BEGIN
ALTER TABLE CategoriaProdotto ADD [Descrizione] nvarchar (50);
END

IF NOT EXISTS(SELECT 1 FROM sys.columns WHERE Name = N'Elimina' AND Object_ID = Object_ID(N'CategoriaProdotto'))
BEGIN
ALTER TABLE CategoriaProdotto ADD [Elimina] nvarchar (1);
END

IF NOT EXISTS(SELECT 1 FROM sys.columns WHERE Name = N'GruppoMerceologico' AND Object_ID = Object_ID(N'CategoriaProdotto'))
BEGIN
ALTER TABLE CategoriaProdotto ADD [GruppoMerceologico] nvarchar (2);
END

IF NOT EXISTS(SELECT 1 FROM sys.columns WHERE Name = N'GiorniScorta' AND Object_ID = Object_ID(N'CategoriaProdotto'))
BEGIN
ALTER TABLE CategoriaProdotto ADD [GiorniScorta] smallint;
END

IF NOT EXISTS(SELECT name FROM sys.sysobjects  WHERE Name = N'CausaliAutomatiche' AND xtype = N'U')
BEGIN
CREATE TABLE CausaliAutomatiche([ID] [int] IDENTITY(1,1) NOT NULL) ON [PRIMARY];
END

IF NOT EXISTS(SELECT 1 FROM sys.columns WHERE Name = N'Utente' AND Object_ID = Object_ID(N'CausaliAutomatiche'))
BEGIN
ALTER TABLE CausaliAutomatiche ADD [Utente] nvarchar (50);
END

IF NOT EXISTS(SELECT 1 FROM sys.columns WHERE Name = N'DataAggiornamento' AND Object_ID = Object_ID(N'CausaliAutomatiche'))
BEGIN
ALTER TABLE CausaliAutomatiche ADD [DataAggiornamento] datetime;
END

IF NOT EXISTS(SELECT 1 FROM sys.columns WHERE Name = N'CaricoBolla' AND Object_ID = Object_ID(N'CausaliAutomatiche'))
BEGIN
ALTER TABLE CausaliAutomatiche ADD [CaricoBolla] nvarchar (3);
END

IF NOT EXISTS(SELECT 1 FROM sys.columns WHERE Name = N'CaricoFattura' AND Object_ID = Object_ID(N'CausaliAutomatiche'))
BEGIN
ALTER TABLE CausaliAutomatiche ADD [CaricoFattura] nvarchar (3);
END

IF NOT EXISTS(SELECT 1 FROM sys.columns WHERE Name = N'CaricoMedicinali' AND Object_ID = Object_ID(N'CausaliAutomatiche'))
BEGIN
ALTER TABLE CausaliAutomatiche ADD [CaricoMedicinali] nvarchar (3);
END

IF NOT EXISTS(SELECT 1 FROM sys.columns WHERE Name = N'ScaricoMedicinali' AND Object_ID = Object_ID(N'CausaliAutomatiche'))
BEGIN
ALTER TABLE CausaliAutomatiche ADD [ScaricoMedicinali] nvarchar (3);
END

IF NOT EXISTS(SELECT 1 FROM sys.columns WHERE Name = N'MovimentoApertura' AND Object_ID = Object_ID(N'CausaliAutomatiche'))
BEGIN
ALTER TABLE CausaliAutomatiche ADD [MovimentoApertura] nvarchar (3);
END

IF NOT EXISTS(SELECT 1 FROM sys.columns WHERE Name = N'ScaricoMultiplo' AND Object_ID = Object_ID(N'CausaliAutomatiche'))
BEGIN
ALTER TABLE CausaliAutomatiche ADD [ScaricoMultiplo] nvarchar (3);
END

IF NOT EXISTS(SELECT 1 FROM sys.columns WHERE Name = N'CaricoMultiplo' AND Object_ID = Object_ID(N'CausaliAutomatiche'))
BEGIN
ALTER TABLE CausaliAutomatiche ADD [CaricoMultiplo] nvarchar (3);
END

IF NOT EXISTS(SELECT 1 FROM sys.columns WHERE Name = N'ScaricoMenu' AND Object_ID = Object_ID(N'CausaliAutomatiche'))
BEGIN
ALTER TABLE CausaliAutomatiche ADD [ScaricoMenu] nvarchar (3);
END

IF NOT EXISTS(SELECT 1 FROM sys.columns WHERE Name = N'ScaricoNotaCredito' AND Object_ID = Object_ID(N'CausaliAutomatiche'))
BEGIN
ALTER TABLE CausaliAutomatiche ADD [ScaricoNotaCredito] nvarchar (3);
END

IF NOT EXISTS(SELECT 1 FROM sys.columns WHERE Name = N'GruppoMerceologicoAlimenti' AND Object_ID = Object_ID(N'CausaliAutomatiche'))
BEGIN
ALTER TABLE CausaliAutomatiche ADD [GruppoMerceologicoAlimenti] nvarchar (2);
END

IF NOT EXISTS(SELECT 1 FROM sys.columns WHERE Name = N'GruppoMerceologicoMedicinali' AND Object_ID = Object_ID(N'CausaliAutomatiche'))
BEGIN
ALTER TABLE CausaliAutomatiche ADD [GruppoMerceologicoMedicinali] nvarchar (2);
END

IF NOT EXISTS(SELECT 1 FROM sys.columns WHERE Name = N'CausaleIncasso' AND Object_ID = Object_ID(N'CausaliAutomatiche'))
BEGIN
ALTER TABLE CausaliAutomatiche ADD [CausaleIncasso] nvarchar (2);
END

IF NOT EXISTS(SELECT 1 FROM sys.columns WHERE Name = N'CausaleDescrittiva' AND Object_ID = Object_ID(N'CausaliAutomatiche'))
BEGIN
ALTER TABLE CausaliAutomatiche ADD [CausaleDescrittiva] nvarchar (4);
END

IF NOT EXISTS(SELECT 1 FROM sys.columns WHERE Name = N'IBANRID' AND Object_ID = Object_ID(N'Societa'))
BEGIN
ALTER TABLE Societa ADD [IBANRID] nvarchar (100);
END

IF NOT EXISTS(SELECT 1 FROM sys.columns WHERE Name = N'EndToEndId' AND Object_ID = Object_ID(N'Societa'))
BEGIN
ALTER TABLE Societa ADD [EndToEndId] nvarchar (100);
END

IF NOT EXISTS(SELECT 1 FROM sys.columns WHERE Name = N'PrvtId' AND Object_ID = Object_ID(N'Societa'))
BEGIN
ALTER TABLE Societa ADD [PrvtId] nvarchar (100);
END

IF NOT EXISTS(SELECT 1 FROM sys.columns WHERE Name = N'MmbId' AND Object_ID = Object_ID(N'Societa'))
BEGIN
ALTER TABLE Societa ADD [MmbId] nvarchar (100);
END

IF NOT EXISTS(SELECT 1 FROM sys.columns WHERE Name = N'OrgId' AND Object_ID = Object_ID(N'Societa'))
BEGIN
ALTER TABLE Societa ADD [OrgId] nvarchar (100);
END

IF NOT EXISTS(SELECT 1 FROM sys.columns WHERE Name = N'CodiceMittente' AND Object_ID = Object_ID(N'Societa'))
BEGIN
ALTER TABLE Societa ADD [CodiceMittente] nvarchar (100);
END

IF NOT EXISTS(SELECT 1 FROM sys.columns WHERE Name = N'CodiceRicevente' AND Object_ID = Object_ID(N'Societa'))
BEGIN
ALTER TABLE Societa ADD [CodiceRicevente] nvarchar (100);
END  


IF NOT EXISTS(SELECT 1 FROM sys.columns WHERE Name = N'CODICEABI' AND Object_ID = Object_ID(N'Societa'))
BEGIN
ALTER TABLE Societa ADD [CODICEABI] nvarchar (100);
END  


IF NOT EXISTS(SELECT 1 FROM sys.columns WHERE Name = N'CODICECAB' AND Object_ID = Object_ID(N'Societa'))
BEGIN
ALTER TABLE Societa ADD [CODICECAB] nvarchar (100);
END  


IF NOT EXISTS(SELECT 1 FROM sys.columns WHERE Name = N'Conto' AND Object_ID = Object_ID(N'Societa'))
BEGIN
ALTER TABLE Societa ADD [Conto] nvarchar (100);
END  



IF NOT EXISTS(SELECT name FROM sys.sysobjects  WHERE Name = N'TabellaTrascodificheEsportazioni' AND xtype = N'U')
BEGIN
CREATE TABLE TabellaTrascodificheEsportazioni([ID] [int] IDENTITY(1,1) NOT NULL) ON [PRIMARY];
END


IF NOT EXISTS(SELECT 1 FROM sys.columns WHERE Name = N'TIPOTAB' AND Object_ID = Object_ID(N'TabellaTrascodificheEsportazioni'))
BEGIN
ALTER TABLE TabellaTrascodificheEsportazioni ADD [TIPOTAB] varchar (2);
END  


IF NOT EXISTS(SELECT 1 FROM sys.columns WHERE Name = N'SENIOR' AND Object_ID = Object_ID(N'TabellaTrascodificheEsportazioni'))
BEGIN
ALTER TABLE TabellaTrascodificheEsportazioni ADD [SENIOR] varchar (50);
END  


IF NOT EXISTS(SELECT 1 FROM sys.columns WHERE Name = N'EXPORT' AND Object_ID = Object_ID(N'TabellaTrascodificheEsportazioni'))
BEGIN
ALTER TABLE TabellaTrascodificheEsportazioni ADD [EXPORT] varchar (50);
END  


IF NOT EXISTS(SELECT name FROM sys.sysobjects  WHERE Name = N'LogPrivacy' AND xtype = N'U')
BEGIN
CREATE TABLE LogPrivacy([ID] [int] IDENTITY(1,1) NOT NULL) ON [PRIMARY];
END


IF NOT EXISTS(SELECT 1 FROM sys.columns WHERE Name = N'Utente' AND Object_ID = Object_ID(N'LogPrivacy'))
BEGIN
ALTER TABLE LogPrivacy ADD Utente varchar (100);
END  

IF NOT EXISTS(SELECT 1 FROM sys.columns WHERE Name = N'Pagina' AND Object_ID = Object_ID(N'LogPrivacy'))
BEGIN
ALTER TABLE LogPrivacy ADD Pagina varchar (100);
END  

IF NOT EXISTS(SELECT 1 FROM sys.columns WHERE Name = N'CodiceOspite' AND Object_ID = Object_ID(N'LogPrivacy'))
BEGIN
ALTER TABLE LogPrivacy ADD CodiceOspite int;
END  

IF NOT EXISTS(SELECT 1 FROM sys.columns WHERE Name = N'CodiceParente' AND Object_ID = Object_ID(N'LogPrivacy'))
BEGIN
ALTER TABLE LogPrivacy ADD CodiceParente int;
END  

IF NOT EXISTS(SELECT 1 FROM sys.columns WHERE Name = N'NumeroRegistrazione' AND Object_ID = Object_ID(N'LogPrivacy'))
BEGIN
ALTER TABLE LogPrivacy ADD NumeroRegistrazione int;
END  


IF NOT EXISTS(SELECT 1 FROM sys.columns WHERE Name = N'STRUTTURA' AND Object_ID = Object_ID(N'LogPrivacy'))
BEGIN
ALTER TABLE LogPrivacy ADD STRUTTURA varchar(2);
END


IF NOT EXISTS(SELECT 1 FROM sys.columns WHERE Name = N'CSERV' AND Object_ID = Object_ID(N'LogPrivacy'))
BEGIN
ALTER TABLE LogPrivacy ADD CSERV varchar(4);
END

IF NOT EXISTS(SELECT 1 FROM sys.columns WHERE Name = N'CODIFICA' AND Object_ID = Object_ID(N'LogPrivacy'))
BEGIN
ALTER TABLE LogPrivacy ADD CODIFICA varchar(100);
END  

IF NOT EXISTS(SELECT 1 FROM sys.columns WHERE Name = N'DATAORA' AND Object_ID = Object_ID(N'LogPrivacy'))
BEGIN
ALTER TABLE LogPrivacy ADD DATAORA DateTime;
END 

IF NOT EXISTS(SELECT 1 FROM sys.columns WHERE Name = N'Tipo' AND Object_ID = Object_ID(N'LogPrivacy'))
BEGIN
ALTER TABLE LogPrivacy ADD Tipo varchar(1);
END  

IF NOT EXISTS(SELECT 1 FROM sys.columns WHERE Name = N'Classe' AND Object_ID = Object_ID(N'LogPrivacy'))
BEGIN
ALTER TABLE LogPrivacy ADD Classe varchar(100);
END  

IF NOT EXISTS(SELECT 1 FROM sys.columns WHERE Name = N'JsonOggetto' AND Object_ID = Object_ID(N'LogPrivacy'))
BEGIN
ALTER TABLE LogPrivacy ADD JsonOggetto varchar(Max);
END

IF NOT EXISTS(SELECT 1 FROM sys.columns WHERE Name = N'Nome' AND Object_ID = Object_ID(N'LogPrivacy'))
BEGIN
ALTER TABLE LogPrivacy ADD Nome varchar(150);
END  

IF NOT EXISTS(SELECT 1 FROM sys.columns WHERE Name = N'Send' AND Object_ID = Object_ID(N'LogPrivacy'))
BEGIN
ALTER TABLE LogPrivacy ADD Send int;
END  

IF NOT EXISTS(SELECT 1 FROM sys.columns WHERE Name = N'StampaDaGestioneDocumenti' AND Object_ID = Object_ID(N'DatiGenerali'))
BEGIN
ALTER TABLE DatiGenerali ADD [StampaDaGestioneDocumenti] int;
END

IF NOT EXISTS(SELECT 1 FROM sys.columns WHERE Name = N'GirocontoRitenuta' AND Object_ID = Object_ID(N'DatiGenerali'))
BEGIN
ALTER TABLE DatiGenerali ADD GirocontoRitenuta int;
END


IF NOT EXISTS(SELECT 1 FROM sys.columns WHERE Name = N'CONSENSOINSERIMENTO' AND Object_ID = Object_ID(N'Protocollo'))
BEGIN
ALTER TABLE Protocollo ADD [CONSENSOINSERIMENTO] int;
END



IF NOT EXISTS(SELECT 1 FROM sys.columns WHERE Name = N'CONSENSOMARKETING' AND Object_ID = Object_ID(N'Protocollo'))
BEGIN
ALTER TABLE Protocollo ADD [CONSENSOMARKETING] int;
END

IF NOT EXISTS(SELECT 1 FROM sys.columns WHERE Name = N'CONSENSOINSERIMENTO' AND Object_ID = Object_ID(N'Rubrica'))
BEGIN
ALTER TABLE Rubrica ADD [CONSENSOINSERIMENTO] int;
END


IF NOT EXISTS(SELECT 1 FROM sys.columns WHERE Name = N'CONSENSOMARKETING' AND Object_ID = Object_ID(N'Rubrica'))
BEGIN
ALTER TABLE Rubrica ADD [CONSENSOMARKETING] int;
END

IF NOT EXISTS(SELECT 1 FROM sys.columns WHERE Name = N'MovimentiRitenute' AND Object_ID = Object_ID(N'DatiGenerali'))
BEGIN
ALTER TABLE DatiGenerali ADD MovimentiRitenute int;
END


IF NOT EXISTS(SELECT name FROM sys.sysobjects  WHERE Name = N'TabellaTributo' AND xtype = N'U')
BEGIN
CREATE TABLE TabellaTributo([ID] [int] IDENTITY(1,1) NOT NULL) ON [PRIMARY];
END


IF NOT EXISTS(SELECT 1 FROM sys.columns WHERE Name = N'Codice' AND Object_ID = Object_ID(N'TabellaTributo'))
BEGIN
ALTER TABLE TabellaTributo ADD Codice varchar (2);
END  


IF NOT EXISTS(SELECT 1 FROM sys.columns WHERE Name = N'Descrizione' AND Object_ID = Object_ID(N'TabellaTributo'))
BEGIN
ALTER TABLE TabellaTributo ADD Descrizione varchar (200);
END  


IF NOT EXISTS(SELECT 1 FROM sys.columns WHERE Name = N'Tributo' AND Object_ID = Object_ID(N'Ritenute'))
BEGIN
ALTER TABLE Ritenute ADD Tributo varchar (2);
END



IF NOT EXISTS(SELECT 1 FROM sys.columns WHERE Name = N'BOLLOVIRTUALE' AND Object_ID = Object_ID(N'Societa'))
BEGIN
ALTER TABLE Societa ADD BOLLOVIRTUALE int;
END



IF NOT EXISTS(SELECT 1 FROM sys.columns WHERE Name = N'REGIMEFISCALE' AND Object_ID = Object_ID(N'Societa'))
BEGIN
ALTER TABLE Societa ADD REGIMEFISCALE varchar (50);
END

IF NOT EXISTS(SELECT 1 FROM sys.columns WHERE Name = N'FatturazionePrivati' AND Object_ID = Object_ID(N'DatiGenerali'))
BEGIN
ALTER TABLE DatiGenerali ADD FatturazionePrivati int;
END



IF NOT EXISTS(SELECT 1 FROM sys.columns WHERE Name = N'BudgetAnalitica' AND Object_ID = Object_ID(N'DatiGenerali'))
BEGIN
ALTER TABLE DatiGenerali ADD BudgetAnalitica int;
END


IF NOT EXISTS(SELECT 1 FROM sys.columns WHERE Name = N'CausaleContabileDefault' AND Object_ID = Object_ID(N'DatiGenerali'))
BEGIN
ALTER TABLE DatiGenerali ADD CausaleContabileDefault varchar(3);
END

IF NOT EXISTS(SELECT 1 FROM sys.columns WHERE Name = N'CausaleContabileNCDefault' AND Object_ID = Object_ID(N'DatiGenerali'))
BEGIN
ALTER TABLE DatiGenerali ADD CausaleContabileNCDefault varchar(3);
END


IF NOT EXISTS(SELECT name FROM sys.sysobjects  WHERE Name = N'FEALIQUOTAIVA' AND xtype = N'U')
BEGIN
CREATE TABLE FEALIQUOTAIVA([ID] [int] IDENTITY(1,1) NOT NULL) ON [PRIMARY];
END


IF NOT EXISTS(SELECT 1 FROM sys.columns WHERE Name = N'ALIQUOTA' AND Object_ID = Object_ID(N'FEALIQUOTAIVA'))
BEGIN
ALTER TABLE FEALIQUOTAIVA ADD ALIQUOTA varchar(5);
END


IF NOT EXISTS(SELECT 1 FROM sys.columns WHERE Name = N'NATURA' AND Object_ID = Object_ID(N'FEALIQUOTAIVA'))
BEGIN
ALTER TABLE FEALIQUOTAIVA ADD NATURA varchar(2);
END


IF NOT EXISTS(SELECT 1 FROM sys.columns WHERE Name = N'CODICE' AND Object_ID = Object_ID(N'FEALIQUOTAIVA'))
BEGIN
ALTER TABLE FEALIQUOTAIVA ADD CODICE varchar(2);
END


IF NOT EXISTS(SELECT name FROM sys.sysobjects  WHERE Name = N'FEARTICOLO' AND xtype = N'U')
BEGIN
CREATE TABLE FEARTICOLO([ID] [int] IDENTITY(1,1) NOT NULL) ON [PRIMARY];
END




IF NOT EXISTS(SELECT 1 FROM sys.columns WHERE Name = N'CODICETIPO' AND Object_ID = Object_ID(N'FEARTICOLO'))
BEGIN
ALTER TABLE FEARTICOLO ADD CODICETIPO varchar(10);
END


IF NOT EXISTS(SELECT 1 FROM sys.columns WHERE Name = N'CODICEVALORE' AND Object_ID = Object_ID(N'FEARTICOLO'))
BEGIN
ALTER TABLE FEARTICOLO ADD CODICEVALORE varchar(10);
END



IF NOT EXISTS(SELECT 1 FROM sys.columns WHERE Name = N'MASTRO' AND Object_ID = Object_ID(N'FEARTICOLO'))
BEGIN
ALTER TABLE FEARTICOLO ADD MASTRO int ;
END


IF NOT EXISTS(SELECT 1 FROM sys.columns WHERE Name = N'CONTO' AND Object_ID = Object_ID(N'FEARTICOLO'))
BEGIN
ALTER TABLE FEARTICOLO ADD CONTO int;
END


IF NOT EXISTS(SELECT 1 FROM sys.columns WHERE Name = N'SOTTOCONTO' AND Object_ID = Object_ID(N'FEARTICOLO'))
BEGIN
ALTER TABLE FEARTICOLO ADD SOTTOCONTO INT;
END


IF NOT EXISTS(SELECT 1 FROM sys.columns WHERE Name = N'AttivaCServPrimanoIncassi' AND Object_ID = Object_ID(N'DatiGenerali'))
BEGIN
ALTER TABLE DatiGenerali ADD AttivaCServPrimanoIncassi int;
END


IF NOT EXISTS(SELECT name FROM sys.sysobjects  WHERE Name = N'Appalti_Anagrafica' AND xtype = N'U')
BEGIN
CREATE TABLE Appalti_Anagrafica([ID] [int] IDENTITY(1,1) NOT NULL) ON [PRIMARY];
END

IF NOT EXISTS(SELECT 1 FROM sys.columns WHERE Name = N'Descrizione' AND Object_ID = Object_ID(N'Appalti_Anagrafica'))
BEGIN
ALTER TABLE Appalti_Anagrafica ADD Descrizione varchar(250);
END


IF NOT EXISTS(SELECT 1 FROM sys.columns WHERE Name = N'Gruppo' AND Object_ID = Object_ID(N'Appalti_Anagrafica'))
BEGIN
ALTER TABLE Appalti_Anagrafica ADD Gruppo varchar(250);
END

IF NOT EXISTS(SELECT 1 FROM sys.columns WHERE Name = N'Cig' AND Object_ID = Object_ID(N'Appalti_Anagrafica'))
BEGIN
ALTER TABLE Appalti_Anagrafica ADD Cig varchar(250);
END


IF NOT EXISTS(SELECT 1 FROM sys.columns WHERE Name = N'Committente' AND Object_ID = Object_ID(N'Appalti_Anagrafica'))
BEGIN
ALTER TABLE Appalti_Anagrafica ADD Committente varchar(4);
END


IF NOT EXISTS(SELECT 1 FROM sys.columns WHERE Name = N'DataInizio' AND Object_ID = Object_ID(N'Appalti_Anagrafica'))
BEGIN
ALTER TABLE Appalti_Anagrafica ADD DataInizio date;
END

IF NOT EXISTS(SELECT 1 FROM sys.columns WHERE Name = N'DataFine' AND Object_ID = Object_ID(N'Appalti_Anagrafica'))
BEGIN
ALTER TABLE Appalti_Anagrafica ADD DataFine date;
END


IF NOT EXISTS(SELECT 1 FROM sys.columns WHERE Name = N'ImportoTeoricoMensile' AND Object_ID = Object_ID(N'Appalti_Anagrafica'))
BEGIN
ALTER TABLE Appalti_Anagrafica ADD ImportoTeoricoMensile float;
END

IF NOT EXISTS(SELECT 1 FROM sys.columns WHERE Name = N'ImportoTeoricoGiornaliero' AND Object_ID = Object_ID(N'Appalti_Anagrafica'))
BEGIN
ALTER TABLE Appalti_Anagrafica ADD ImportoTeoricoGiornaliero float;
END

IF NOT EXISTS(SELECT 1 FROM sys.columns WHERE Name = N'ImportoTeoricoMensileInaddGiornaliero' AND Object_ID = Object_ID(N'Appalti_Anagrafica'))
BEGIN
ALTER TABLE Appalti_Anagrafica ADD ImportoTeoricoMensileInaddGiornaliero float;
END


IF NOT EXISTS(SELECT 1 FROM sys.columns WHERE Name = N'ImportoForfait' AND Object_ID = Object_ID(N'Appalti_Anagrafica'))
BEGIN
ALTER TABLE Appalti_Anagrafica ADD ImportoForfait float;
END


IF NOT EXISTS(SELECT 1 FROM sys.columns WHERE Name = N'Committente' AND Object_ID = Object_ID(N'Appalti_Anagrafica'))
BEGIN
ALTER TABLE Appalti_Anagrafica ADD Committente varchar(4);
END

IF NOT EXISTS(SELECT 1 FROM sys.columns WHERE Name = N'CausaleContabile' AND Object_ID = Object_ID(N'Appalti_Anagrafica'))
BEGIN
ALTER TABLE Appalti_Anagrafica ADD CausaleContabile varchar(3);
END

IF NOT EXISTS(SELECT 1 FROM sys.columns WHERE Name = N'CodiceIVA' AND Object_ID = Object_ID(N'Appalti_Anagrafica'))
BEGIN
ALTER TABLE Appalti_Anagrafica ADD CodiceIVA varchar(2);
END



IF NOT EXISTS(SELECT 1 FROM sys.columns WHERE Name = N'Tipo' AND Object_ID = Object_ID(N'Appalti_Anagrafica'))
BEGIN
ALTER TABLE Appalti_Anagrafica ADD Tipo varchar(1);
END

IF NOT EXISTS(SELECT 1 FROM sys.columns WHERE Name = N'RotturaStruttura' AND Object_ID = Object_ID(N'Appalti_Anagrafica'))
BEGIN
ALTER TABLE Appalti_Anagrafica ADD RotturaStruttura integer;
END


IF NOT EXISTS(SELECT 1 FROM sys.columns WHERE Name = N'RaggruppaRicavi' AND Object_ID = Object_ID(N'Appalti_Anagrafica'))
BEGIN
ALTER TABLE Appalti_Anagrafica ADD RaggruppaRicavi integer;
END

IF NOT EXISTS(SELECT 1 FROM sys.columns WHERE Name = N'Mastro' AND Object_ID = Object_ID(N'Appalti_Anagrafica'))
BEGIN
ALTER TABLE Appalti_Anagrafica ADD Mastro integer;
END

IF NOT EXISTS(SELECT 1 FROM sys.columns WHERE Name = N'Conto' AND Object_ID = Object_ID(N'Appalti_Anagrafica'))
BEGIN
ALTER TABLE Appalti_Anagrafica ADD Conto integer;
END

IF NOT EXISTS(SELECT 1 FROM sys.columns WHERE Name = N'Sottoconto' AND Object_ID = Object_ID(N'Appalti_Anagrafica'))
BEGIN
ALTER TABLE Appalti_Anagrafica ADD Sottoconto integer;
END



IF NOT EXISTS(SELECT name FROM sys.sysobjects  WHERE Name = N'Appalti_Strutture' AND xtype = N'U')
BEGIN
CREATE TABLE Appalti_Strutture([ID] [int] IDENTITY(1,1) NOT NULL) ON [PRIMARY];
END

IF NOT EXISTS(SELECT 1 FROM sys.columns WHERE Name = N'Descrizione' AND Object_ID = Object_ID(N'Appalti_Strutture'))
BEGIN
ALTER TABLE Appalti_Strutture ADD Descrizione varchar(250);
END

IF NOT EXISTS(SELECT 1 FROM sys.columns WHERE Name = N'CodificaImport' AND Object_ID = Object_ID(N'Appalti_Strutture'))
BEGIN
ALTER TABLE Appalti_Strutture ADD CodificaImport varchar(50);
END

IF NOT EXISTS(SELECT 1 FROM sys.columns WHERE Name = N'CentroServizio' AND Object_ID = Object_ID(N'Appalti_Strutture'))
BEGIN
ALTER TABLE Appalti_Strutture ADD CentroServizio varchar(4);
END


IF NOT EXISTS(SELECT 1 FROM sys.columns WHERE Name = N'MoltiplicatoreKM' AND Object_ID = Object_ID(N'Appalti_Strutture'))
BEGIN
ALTER TABLE Appalti_Strutture ADD MoltiplicatoreKM  float;
END



IF NOT EXISTS(SELECT name FROM sys.sysobjects  WHERE Name = N'Appalti_Mansione' AND xtype = N'U')
BEGIN
CREATE TABLE Appalti_Mansione([ID] [int] IDENTITY(1,1) NOT NULL) ON [PRIMARY];
END

IF NOT EXISTS(SELECT 1 FROM sys.columns WHERE Name = N'Descrizione' AND Object_ID = Object_ID(N'Appalti_Mansione'))
BEGIN
ALTER TABLE Appalti_Mansione ADD Descrizione varchar(250);
END

IF NOT EXISTS(SELECT 1 FROM sys.columns WHERE Name = N'CodificaImport' AND Object_ID = Object_ID(N'Appalti_Mansione'))
BEGIN
ALTER TABLE Appalti_Mansione ADD CodificaImport varchar(50);
END

IF NOT EXISTS(SELECT 1 FROM sys.columns WHERE Name = N'Struttura' AND Object_ID = Object_ID(N'Appalti_Mansione'))
BEGIN
ALTER TABLE Appalti_Mansione ADD Struttura int;
END



IF NOT EXISTS(SELECT 1 FROM sys.columns WHERE Name = N'Festivo' AND Object_ID = Object_ID(N'Appalti_Mansione'))
BEGIN
ALTER TABLE Appalti_Mansione ADD Festivo int;
END


IF NOT EXISTS(SELECT name FROM sys.sysobjects  WHERE Name = N'Appalti_AppaltiStrutture' AND xtype = N'U')
BEGIN
CREATE TABLE Appalti_AppaltiStrutture([ID] [int] IDENTITY(1,1) NOT NULL) ON [PRIMARY];
END

IF NOT EXISTS(SELECT 1 FROM sys.columns WHERE Name = N'IdAppalto' AND Object_ID = Object_ID(N'Appalti_AppaltiStrutture'))
BEGIN
ALTER TABLE Appalti_AppaltiStrutture ADD IdAppalto integer;
END


IF NOT EXISTS(SELECT 1 FROM sys.columns WHERE Name = N'IdStrutture' AND Object_ID = Object_ID(N'Appalti_AppaltiStrutture'))
BEGIN
ALTER TABLE Appalti_AppaltiStrutture ADD IdStrutture integer;
END

IF NOT EXISTS(SELECT 1 FROM sys.columns WHERE Name = N'ImportoForfait' AND Object_ID = Object_ID(N'Appalti_AppaltiStrutture'))
BEGIN
ALTER TABLE Appalti_AppaltiStrutture ADD ImportoForfait float;
END



IF NOT EXISTS(SELECT name FROM sys.sysobjects  WHERE Name = N'Appalti_StruttureMansioni' AND xtype = N'U')
BEGIN
CREATE TABLE Appalti_StruttureMansioni([ID] [int] IDENTITY(1,1) NOT NULL) ON [PRIMARY];
END

IF NOT EXISTS(SELECT 1 FROM sys.columns WHERE Name = N'IdAppalto' AND Object_ID = Object_ID(N'Appalti_StruttureMansioni'))
BEGIN
ALTER TABLE Appalti_StruttureMansioni ADD IdAppalto integer;
END


IF NOT EXISTS(SELECT 1 FROM sys.columns WHERE Name = N'IdStrutture' AND Object_ID = Object_ID(N'Appalti_StruttureMansioni'))
BEGIN
ALTER TABLE Appalti_StruttureMansioni ADD IdStrutture integer;
END


IF NOT EXISTS(SELECT 1 FROM sys.columns WHERE Name = N'IdMansione' AND Object_ID = Object_ID(N'Appalti_StruttureMansioni'))
BEGIN
ALTER TABLE Appalti_StruttureMansioni ADD IdMansione integer;
END


IF NOT EXISTS(SELECT 1 FROM sys.columns WHERE Name = N'Regola' AND Object_ID = Object_ID(N'Appalti_StruttureMansioni'))
BEGIN
ALTER TABLE Appalti_StruttureMansioni ADD Regola varchar(2);
END


IF NOT EXISTS(SELECT 1 FROM sys.columns WHERE Name = N'Importo' AND Object_ID = Object_ID(N'Appalti_StruttureMansioni'))
BEGIN
ALTER TABLE Appalti_StruttureMansioni ADD Importo float;
END


IF NOT EXISTS(SELECT 1 FROM sys.columns WHERE Name = N'Mastro' AND Object_ID = Object_ID(N'Appalti_StruttureMansioni'))
BEGIN
ALTER TABLE Appalti_StruttureMansioni ADD Mastro integer;
END

IF NOT EXISTS(SELECT 1 FROM sys.columns WHERE Name = N'Conto' AND Object_ID = Object_ID(N'Appalti_StruttureMansioni'))
BEGIN
ALTER TABLE Appalti_StruttureMansioni ADD Conto integer;
END


IF NOT EXISTS(SELECT 1 FROM sys.columns WHERE Name = N'Sottoconto' AND Object_ID = Object_ID(N'Appalti_StruttureMansioni'))
BEGIN
ALTER TABLE Appalti_StruttureMansioni ADD Sottoconto integer;
END

IF NOT EXISTS(SELECT 1 FROM sys.columns WHERE Name = N'CodiceIVA' AND Object_ID = Object_ID(N'Appalti_StruttureMansioni'))
BEGIN
ALTER TABLE Appalti_StruttureMansioni ADD CodiceIVA varchar(2);
END




IF NOT EXISTS(SELECT 1 FROM sys.columns WHERE Name = N'intypecod' AND Object_ID = Object_ID(N'Appalti_StruttureMansioni'))
BEGIN
ALTER TABLE Appalti_StruttureMansioni ADD intypecod varchar(250);
END



IF NOT EXISTS(SELECT name FROM sys.sysobjects  WHERE Name = N'Appalti_Prestazioni' AND xtype = N'U')
BEGIN
CREATE TABLE Appalti_Prestazioni([ID] [int] IDENTITY(1,1) NOT NULL) ON [PRIMARY];
END

IF NOT EXISTS(SELECT 1 FROM sys.columns WHERE Name = N'Data' AND Object_ID = Object_ID(N'Appalti_Prestazioni'))
BEGIN
ALTER TABLE Appalti_Prestazioni ADD Data date;
END


IF NOT EXISTS(SELECT 1 FROM sys.columns WHERE Name = N'IdAppalto' AND Object_ID = Object_ID(N'Appalti_Prestazioni'))
BEGIN
ALTER TABLE Appalti_Prestazioni ADD IdAppalto integer;
END


IF NOT EXISTS(SELECT 1 FROM sys.columns WHERE Name = N'IdStrutture' AND Object_ID = Object_ID(N'Appalti_Prestazioni'))
BEGIN
ALTER TABLE Appalti_Prestazioni ADD IdStrutture integer;
END

IF NOT EXISTS(SELECT 1 FROM sys.columns WHERE Name = N'IdMansione' AND Object_ID = Object_ID(N'Appalti_Prestazioni'))
BEGIN
ALTER TABLE Appalti_Prestazioni ADD IdMansione integer;
END

IF NOT EXISTS(SELECT 1 FROM sys.columns WHERE Name = N'Ore' AND Object_ID = Object_ID(N'Appalti_Prestazioni'))
BEGIN
ALTER TABLE Appalti_Prestazioni ADD Ore  float;
END


IF NOT EXISTS(SELECT 1 FROM sys.columns WHERE Name = N'CodiceOperatore' AND Object_ID = Object_ID(N'Appalti_Prestazioni'))
BEGIN
ALTER TABLE Appalti_Prestazioni ADD CodiceOperatore  varchar(6);
END





IF NOT EXISTS(SELECT name FROM sys.sysobjects  WHERE Name = N'Appalti_PrestazioniElaborate' AND xtype = N'U')
BEGIN
CREATE TABLE Appalti_PrestazioniElaborate([ID] [int] IDENTITY(1,1) NOT NULL) ON [PRIMARY];
END

IF NOT EXISTS(SELECT 1 FROM sys.columns WHERE Name = N'Data' AND Object_ID = Object_ID(N'Appalti_PrestazioniElaborate'))
BEGIN
ALTER TABLE Appalti_PrestazioniElaborate ADD Data  date;
END

IF NOT EXISTS(SELECT 1 FROM sys.columns WHERE Name = N'IdAppalto' AND Object_ID = Object_ID(N'Appalti_PrestazioniElaborate'))
BEGIN
ALTER TABLE Appalti_PrestazioniElaborate ADD IdAppalto  integer;
END


IF NOT EXISTS(SELECT 1 FROM sys.columns WHERE Name = N'IdStruttura' AND Object_ID = Object_ID(N'Appalti_PrestazioniElaborate'))
BEGIN
ALTER TABLE Appalti_PrestazioniElaborate ADD IdStruttura  integer;
END


IF NOT EXISTS(SELECT 1 FROM sys.columns WHERE Name = N'IdMansione' AND Object_ID = Object_ID(N'Appalti_PrestazioniElaborate'))
BEGIN
ALTER TABLE Appalti_PrestazioniElaborate ADD IdMansione  integer;
END


IF NOT EXISTS(SELECT 1 FROM sys.columns WHERE Name = N'CodiceOperatore' AND Object_ID = Object_ID(N'Appalti_PrestazioniElaborate'))
BEGIN
ALTER TABLE Appalti_PrestazioniElaborate ADD CodiceOperatore  varchar(6);
END


IF NOT EXISTS(SELECT 1 FROM sys.columns WHERE Name = N'Ore' AND Object_ID = Object_ID(N'Appalti_PrestazioniElaborate'))
BEGIN
ALTER TABLE Appalti_PrestazioniElaborate ADD Ore  float;
END

IF NOT EXISTS(SELECT 1 FROM sys.columns WHERE Name = N'Accessi' AND Object_ID = Object_ID(N'Appalti_PrestazioniElaborate'))
BEGIN
ALTER TABLE Appalti_PrestazioniElaborate ADD Accessi  integer;
END

IF NOT EXISTS(SELECT 1 FROM sys.columns WHERE Name = N'Imponibile' AND Object_ID = Object_ID(N'Appalti_PrestazioniElaborate'))
BEGIN
ALTER TABLE Appalti_PrestazioniElaborate ADD Imponibile  float;
END

IF NOT EXISTS(SELECT 1 FROM sys.columns WHERE Name = N'CodiceIVA' AND Object_ID = Object_ID(N'Appalti_PrestazioniElaborate'))
BEGIN
ALTER TABLE Appalti_PrestazioniElaborate ADD CodiceIVA  varchar(2);
END



IF NOT EXISTS(SELECT 1 FROM sys.columns WHERE Name = N'KM' AND Object_ID = Object_ID(N'Appalti_AppaltiStrutture'))
BEGIN
ALTER TABLE Appalti_AppaltiStrutture ADD KM int;
END


IF NOT EXISTS(SELECT 1 FROM sys.columns WHERE Name = N'KMImporto' AND Object_ID = Object_ID(N'Appalti_AppaltiStrutture'))
BEGIN
ALTER TABLE Appalti_AppaltiStrutture ADD KMImporto float;
END



IF NOT EXISTS(SELECT 1 FROM sys.columns WHERE Name = N'Attivita' AND Object_ID = Object_ID(N'Prorata'))
BEGIN
ALTER TABLE Prorata ADD Attivita int;
END


IF NOT EXISTS(SELECT 1 FROM sys.columns WHERE Name = N'CodiceIVAKM' AND Object_ID = Object_ID(N'Appalti_Anagrafica'))
BEGIN
ALTER TABLE Appalti_Anagrafica ADD CodiceIVAKM varchar(2);
END



IF NOT EXISTS(SELECT 1 FROM sys.columns WHERE Name = N'MastroKM' AND Object_ID = Object_ID(N'Appalti_Anagrafica'))
BEGIN
ALTER TABLE Appalti_Anagrafica ADD MastroKM integer;
END

IF NOT EXISTS(SELECT 1 FROM sys.columns WHERE Name = N'ContoKM' AND Object_ID = Object_ID(N'Appalti_Anagrafica'))
BEGIN
ALTER TABLE Appalti_Anagrafica ADD ContoKM integer;
END

IF NOT EXISTS(SELECT 1 FROM sys.columns WHERE Name = N'SottocontoKM' AND Object_ID = Object_ID(N'Appalti_Anagrafica'))
BEGIN
ALTER TABLE Appalti_Anagrafica ADD SottocontoKM integer;
END



IF NOT EXISTS(SELECT 1 FROM sys.columns WHERE Name = N'RegistroCartaceo' AND Object_ID = Object_ID(N'TipoRegistro'))
BEGIN
ALTER TABLE TipoRegistro ADD RegistroCartaceo int;
END


IF NOT EXISTS(SELECT 1 FROM sys.columns WHERE Name = N'BloccaLegameScadenzario' AND Object_ID = Object_ID(N'DatiGenerali'))
BEGIN
ALTER TABLE DatiGenerali ADD BloccaLegameScadenzario int;
END


IF NOT EXISTS(SELECT 1 FROM sys.columns WHERE Name = N'ScadenziarioCheckChiuso' AND Object_ID = Object_ID(N'DatiGenerali'))
BEGIN
ALTER TABLE DatiGenerali ADD ScadenziarioCheckChiuso int;
END


ALTER TABLE SOCIETA ALTER COLUMN  ProcedureAbilitate VARCHAR(250);


IF NOT EXISTS(SELECT 1 FROM sys.columns WHERE Name = N'ModificaDaAppalto' AND Object_ID = Object_ID(N'Appalti_StruttureMansioni'))
BEGIN
ALTER TABLE Appalti_StruttureMansioni ADD ModificaDaAppalto integer;
END


IF NOT EXISTS(SELECT name FROM sys.sysobjects  WHERE Name = N'Appalti_Festivita' AND xtype = N'U')
BEGIN
CREATE TABLE Appalti_Festivita([ID] [int] IDENTITY(1,1) NOT NULL) ON [PRIMARY];
END

IF NOT EXISTS(SELECT 1 FROM sys.columns WHERE Name = N'Utente' AND Object_ID = Object_ID(N'Appalti_Festivita'))
BEGIN
ALTER TABLE Appalti_Festivita ADD [Utente] nvarchar (50);
END


IF NOT EXISTS(SELECT 1 FROM sys.columns WHERE Name = N'Giorno' AND Object_ID = Object_ID(N'Appalti_Festivita'))
BEGIN
ALTER TABLE Appalti_Festivita ADD Giorno int;
END


IF NOT EXISTS(SELECT 1 FROM sys.columns WHERE Name = N'Mese' AND Object_ID = Object_ID(N'Appalti_Festivita'))
BEGIN
ALTER TABLE Appalti_Festivita ADD Mese int;
END



IF NOT EXISTS(SELECT 1 FROM sys.columns WHERE Name = N'Anno' AND Object_ID = Object_ID(N'Appalti_Festivita'))
BEGIN
ALTER TABLE Appalti_Festivita ADD Anno int;
END


IF NOT EXISTS(SELECT 1 FROM sys.columns WHERE Name = N'NonInUso' AND Object_ID = Object_ID(N'IVA'))
BEGIN
ALTER TABLE IVA ADD NonInUso int;
END



IF NOT EXISTS(SELECT 1 FROM sys.columns WHERE Name = N'TipoVisualizzazione' AND Object_ID = Object_ID(N'DatiGenerali'))
BEGIN
ALTER TABLE DatiGenerali ADD TipoVisualizzazione int;
END



IF NOT EXISTS(SELECT 1 FROM sys.columns WHERE Name = N'AnaliticaObbligatoria' AND Object_ID = Object_ID(N'CausaliContabiliTesta'))
BEGIN
ALTER TABLE CausaliContabiliTesta ADD AnaliticaObbligatoria tinyint;
END



IF NOT EXISTS(SELECT 1 FROM sys.columns WHERE Name = N'CompetenzaObbligatiroAnalitica' AND Object_ID = Object_ID(N'DatiGenerali'))
BEGIN
ALTER TABLE DatiGenerali ADD CompetenzaObbligatiroAnalitica int;
END




IF NOT EXISTS(SELECT 1 FROM sys.columns WHERE Name = N'GestioneDocumentiNew' AND Object_ID = Object_ID(N'DatiGenerali'))
BEGIN
ALTER TABLE DatiGenerali ADD GestioneDocumentiNew int;
END




IF NOT EXISTS(SELECT 1 FROM sys.columns WHERE Name = N'AperturaGestioneScadenze' AND Object_ID = Object_ID(N'TipoPagamento'))
BEGIN
ALTER TABLE TipoPagamento ADD AperturaGestioneScadenze tinyint;
END


IF NOT EXISTS(SELECT 1 FROM sys.columns WHERE Name = N'intypecod' AND Object_ID = Object_ID(N'Appalti_Prestazioni'))
BEGIN
ALTER TABLE Appalti_Prestazioni ADD intypecod varchar(250);
END


IF NOT EXISTS(SELECT 1 FROM sys.columns WHERE Name = N'intypedescription' AND Object_ID = Object_ID(N'Appalti_Prestazioni'))
BEGIN
ALTER TABLE Appalti_Prestazioni ADD intypedescription varchar(250);
END





IF NOT EXISTS(SELECT 1 FROM sys.columns WHERE Name = N'intypecod' AND Object_ID = Object_ID(N'Appalti_PrestazioniElaborate'))
BEGIN
ALTER TABLE Appalti_PrestazioniElaborate ADD intypecod varchar(250);
END


IF NOT EXISTS(SELECT 1 FROM sys.columns WHERE Name = N'intypedescription' AND Object_ID = Object_ID(N'Appalti_PrestazioniElaborate'))
BEGIN
ALTER TABLE Appalti_PrestazioniElaborate ADD intypedescription varchar(250);
END



IF NOT EXISTS(SELECT name FROM sys.sysobjects  WHERE Name = N'Appalti_TipoUtente' AND xtype = N'U')
BEGIN
CREATE TABLE Appalti_TipoUtente([ID] [int] IDENTITY(1,1) NOT NULL) ON [PRIMARY];
END



IF NOT EXISTS(SELECT 1 FROM sys.columns WHERE Name = N'intypecod' AND Object_ID = Object_ID(N'Appalti_TipoUtente'))
BEGIN
ALTER TABLE Appalti_TipoUtente ADD intypecod varchar(250);
END


IF NOT EXISTS(SELECT 1 FROM sys.columns WHERE Name = N'intypedescription' AND Object_ID = Object_ID(N'Appalti_TipoUtente'))
BEGIN
ALTER TABLE Appalti_TipoUtente ADD intypedescription varchar(250);
END



IF NOT EXISTS(SELECT 1 FROM sys.columns WHERE Name = N'codiceospite' AND Object_ID = Object_ID(N'Appalti_Prestazioni'))
BEGIN
ALTER TABLE Appalti_Prestazioni ADD codiceospite int;
END



IF NOT EXISTS(SELECT 1 FROM sys.columns WHERE Name = N'CodiceOspite' AND Object_ID = Object_ID(N'Appalti_PrestazioniElaborate'))
BEGIN
ALTER TABLE Appalti_PrestazioniElaborate ADD CodiceOspite  integer;
END





IF NOT EXISTS(SELECT name FROM sys.sysobjects  WHERE Name = N'Appalti_Programmazione' AND xtype = N'U')
BEGIN
CREATE TABLE Appalti_Programmazione([ID] [int] IDENTITY(1,1) NOT NULL) ON [PRIMARY];
END

IF NOT EXISTS(SELECT 1 FROM sys.columns WHERE Name = N'IdOperatore' AND Object_ID = Object_ID(N'Appalti_Programmazione'))
BEGIN
ALTER TABLE Appalti_Programmazione ADD IdOperatore  integer;
END

IF NOT EXISTS(SELECT 1 FROM sys.columns WHERE Name = N'Regola' AND Object_ID = Object_ID(N'Appalti_Programmazione'))
BEGIN
ALTER TABLE Appalti_Programmazione ADD Regola  integer;
END



IF NOT EXISTS(SELECT 1 FROM sys.columns WHERE Name = N'DataInizio' AND Object_ID = Object_ID(N'Appalti_Programmazione'))
BEGIN
ALTER TABLE Appalti_Programmazione ADD DataInizio  date;
END


IF NOT EXISTS(SELECT 1 FROM sys.columns WHERE Name = N'NumeroRipetizioni' AND Object_ID = Object_ID(N'Appalti_Programmazione'))
BEGIN
ALTER TABLE Appalti_Programmazione ADD NumeroRipetizioni  integer;
END

IF NOT EXISTS(SELECT 1 FROM sys.columns WHERE Name = N'Giorni' AND Object_ID = Object_ID(N'Appalti_Programmazione'))
BEGIN
ALTER TABLE Appalti_Programmazione ADD Giorni  varchar(250);
END

IF NOT EXISTS(SELECT 1 FROM sys.columns WHERE Name = N'idMansione' AND Object_ID = Object_ID(N'Appalti_Programmazione'))
BEGIN
ALTER TABLE Appalti_Programmazione ADD idMansione  integer;
END

IF NOT EXISTS(SELECT 1 FROM sys.columns WHERE Name = N'IdAppalto' AND Object_ID = Object_ID(N'Appalti_Programmazione'))
BEGIN
ALTER TABLE Appalti_Programmazione ADD IdAppalto  integer;
END

IF NOT EXISTS(SELECT 1 FROM sys.columns WHERE Name = N'IdStruttura' AND Object_ID = Object_ID(N'Appalti_Programmazione'))
BEGIN
ALTER TABLE Appalti_Programmazione ADD IdStruttura  integer;
END

IF NOT EXISTS(SELECT 1 FROM sys.columns WHERE Name = N'CodiceOspite' AND Object_ID = Object_ID(N'Appalti_Programmazione'))
BEGIN
ALTER TABLE Appalti_Programmazione ADD CodiceOspite  integer;
END

IF NOT EXISTS(SELECT 1 FROM sys.columns WHERE Name = N'Ore' AND Object_ID = Object_ID(N'Appalti_Programmazione'))
BEGIN
ALTER TABLE Appalti_Programmazione ADD Ore float;
END


IF NOT EXISTS(SELECT name FROM sys.sysobjects  WHERE Name = N'Appalti_AppaltiBudget' AND xtype = N'U')
BEGIN
CREATE TABLE Appalti_AppaltiBudget([ID] [int] IDENTITY(1,1) NOT NULL) ON [PRIMARY];
END

IF NOT EXISTS(SELECT 1 FROM sys.columns WHERE Name = N'IdAppalto' AND Object_ID = Object_ID(N'Appalti_AppaltiBudget'))
BEGIN
ALTER TABLE Appalti_AppaltiBudget ADD IdAppalto integer;
END


IF NOT EXISTS(SELECT 1 FROM sys.columns WHERE Name = N'IdTipoOperatore' AND Object_ID = Object_ID(N'Appalti_AppaltiBudget'))
BEGIN
ALTER TABLE Appalti_AppaltiBudget ADD IdTipoOperatore  varchar(2);
END


IF NOT EXISTS(SELECT 1 FROM sys.columns WHERE Name = N'Ore' AND Object_ID = Object_ID(N'Appalti_AppaltiBudget'))
BEGIN
ALTER TABLE Appalti_AppaltiBudget ADD Ore float;
END


IF NOT EXISTS(SELECT 1 FROM sys.columns WHERE Name = N'Importo' AND Object_ID = Object_ID(N'Appalti_AppaltiBudget'))
BEGIN
ALTER TABLE Appalti_AppaltiBudget ADD Importo float;
END




IF NOT EXISTS(SELECT name FROM sys.sysobjects  WHERE Name = N'Appalti_AppaltiRigheAggiuntive' AND xtype = N'U')
BEGIN
CREATE TABLE Appalti_AppaltiRigheAggiuntive([ID] [int] IDENTITY(1,1) NOT NULL) ON [PRIMARY];
END

IF NOT EXISTS(SELECT 1 FROM sys.columns WHERE Name = N'IdAppalto' AND Object_ID = Object_ID(N'Appalti_AppaltiRigheAggiuntive'))
BEGIN
ALTER TABLE Appalti_AppaltiRigheAggiuntive ADD IdAppalto integer;
END


IF NOT EXISTS(SELECT 1 FROM sys.columns WHERE Name = N'Mastro' AND Object_ID = Object_ID(N'Appalti_AppaltiRigheAggiuntive'))
BEGIN
ALTER TABLE Appalti_AppaltiRigheAggiuntive ADD Mastro integer;
END


IF NOT EXISTS(SELECT 1 FROM sys.columns WHERE Name = N'Conto' AND Object_ID = Object_ID(N'Appalti_AppaltiRigheAggiuntive'))
BEGIN
ALTER TABLE Appalti_AppaltiRigheAggiuntive ADD Conto integer;
END


IF NOT EXISTS(SELECT 1 FROM sys.columns WHERE Name = N'Sottoconto' AND Object_ID = Object_ID(N'Appalti_AppaltiRigheAggiuntive'))
BEGIN
ALTER TABLE Appalti_AppaltiRigheAggiuntive ADD Sottoconto integer;
END

IF NOT EXISTS(SELECT 1 FROM sys.columns WHERE Name = N'Regola' AND Object_ID = Object_ID(N'Appalti_AppaltiRigheAggiuntive'))
BEGIN
ALTER TABLE Appalti_AppaltiRigheAggiuntive ADD Regola  varchar(2);
END


IF NOT EXISTS(SELECT 1 FROM sys.columns WHERE Name = N'IVA' AND Object_ID = Object_ID(N'Appalti_AppaltiRigheAggiuntive'))
BEGIN
ALTER TABLE Appalti_AppaltiRigheAggiuntive ADD IVA  varchar(2);
END


IF NOT EXISTS(SELECT 1 FROM sys.columns WHERE Name = N'Quantita' AND Object_ID = Object_ID(N'Appalti_AppaltiRigheAggiuntive'))
BEGIN
ALTER TABLE Appalti_AppaltiRigheAggiuntive ADD Quantita  integer;
END


IF NOT EXISTS(SELECT 1 FROM sys.columns WHERE Name = N'Importo' AND Object_ID = Object_ID(N'Appalti_AppaltiRigheAggiuntive'))
BEGIN
ALTER TABLE Appalti_AppaltiRigheAggiuntive ADD Importo float;
END

IF NOT EXISTS(SELECT 1 FROM sys.columns WHERE Name = N'Descrizione' AND Object_ID = Object_ID(N'Appalti_AppaltiRigheAggiuntive'))
BEGIN
ALTER TABLE Appalti_AppaltiRigheAggiuntive ADD Descrizione  varchar(150);
END


IF NOT EXISTS(SELECT 1 FROM sys.columns WHERE Name = N'Struttura' AND Object_ID = Object_ID(N'Appalti_AppaltiRigheAggiuntive'))
BEGIN
ALTER TABLE Appalti_AppaltiRigheAggiuntive ADD Struttura  int;
END


IF NOT EXISTS(SELECT 1 FROM sys.columns WHERE Name = N'CodificaImport2' AND Object_ID = Object_ID(N'Appalti_Mansione'))
BEGIN
ALTER TABLE Appalti_Mansione ADD CodificaImport2 varchar(50);
END


IF NOT EXISTS(SELECT 1 FROM sys.columns WHERE Name = N'CodificaImport2' AND Object_ID = Object_ID(N'Appalti_Strutture'))
BEGIN
ALTER TABLE Appalti_Strutture ADD CodificaImport2 varchar(50);
END



IF NOT EXISTS(SELECT 1 FROM sys.columns WHERE Name = N'IvaImponibile' AND Object_ID = Object_ID(N'TipoPagamento'))
BEGIN
ALTER TABLE TipoPagamento ADD IvaImponibile nvarchar (1);
END





IF NOT EXISTS(SELECT 1 FROM sys.columns WHERE Name = N'codicecooperativa' AND Object_ID = Object_ID(N'Appalti_Prestazioni'))
BEGIN
ALTER TABLE Appalti_Prestazioni ADD codicecooperativa int;
END



IF NOT EXISTS(SELECT 1 FROM sys.columns WHERE Name = N'codicecooperativa' AND Object_ID = Object_ID(N'Appalti_PrestazioniElaborate'))
BEGIN
ALTER TABLE Appalti_PrestazioniElaborate ADD codicecooperativa  integer;
END


 
 

IF NOT EXISTS(SELECT name FROM sys.sysobjects  WHERE Name = N'Appalti_Cooperative' AND xtype = N'U')
BEGIN
CREATE TABLE Appalti_Cooperative([ID] [int] IDENTITY(1,1) NOT NULL) ON [PRIMARY];
END

IF NOT EXISTS(SELECT 1 FROM sys.columns WHERE Name = N'RagioneSociale' AND Object_ID = Object_ID(N'Appalti_Cooperative'))
BEGIN
ALTER TABLE Appalti_Cooperative ADD RagioneSociale varchar(250);
END



IF NOT EXISTS(SELECT 1 FROM sys.columns WHERE Name = N'CalcoloPassivo' AND Object_ID = Object_ID(N'Appalti_Prestazioni'))
BEGIN
ALTER TABLE Appalti_Prestazioni ADD CalcoloPassivo int;
END



IF NOT EXISTS(SELECT 1 FROM sys.columns WHERE Name = N'CalcoloPassivo' AND Object_ID = Object_ID(N'Appalti_PrestazioniElaborate'))
BEGIN
ALTER TABLE Appalti_PrestazioniElaborate ADD CalcoloPassivo  integer;
END

IF NOT EXISTS(SELECT 1 FROM sys.columns WHERE Name = N'GestioneCespiti' AND Object_ID = Object_ID(N'CausaliContabiliTesta'))
BEGIN
ALTER TABLE CausaliContabiliTesta ADD GestioneCespiti tinyint;
END


IF NOT EXISTS(SELECT 1 FROM sys.columns WHERE Name = N'AbilitaGirocontoReverse' AND Object_ID = Object_ID(N'CausaliContabiliTesta'))
BEGIN
ALTER TABLE CausaliContabiliTesta ADD AbilitaGirocontoReverse tinyint;
END

IF NOT EXISTS(SELECT 1 FROM sys.columns WHERE Name = N'CausaleContabileNCDefaultAttivita2' AND Object_ID = Object_ID(N'DatiGenerali'))
BEGIN
ALTER TABLE DatiGenerali ADD CausaleContabileNCDefaultAttivita2 varchar(3);
END


IF NOT EXISTS(SELECT 1 FROM sys.columns WHERE Name = N'CausaleContabileDefaultAttivita2' AND Object_ID = Object_ID(N'DatiGenerali'))
BEGIN
ALTER TABLE DatiGenerali ADD CausaleContabileDefaultAttivita2 varchar(3);
END



IF NOT EXISTS(SELECT 1 FROM sys.columns WHERE Name = N'DONE' AND Object_ID = Object_ID(N'Appalti_Prestazioni'))
BEGIN
ALTER TABLE Appalti_Prestazioni ADD DONE int;
END



IF NOT EXISTS(SELECT 1 FROM sys.columns WHERE Name = N'DONE' AND Object_ID = Object_ID(N'Appalti_PrestazioniElaborate'))
BEGIN
ALTER TABLE Appalti_PrestazioniElaborate ADD DONE  integer;
END


IF NOT EXISTS(SELECT name FROM sys.sysobjects  WHERE Name = N'Prestazioni_Tipologie' AND xtype = N'U')
BEGIN
CREATE TABLE [dbo].[Prestazioni_Tipologie](
	[ID_Prestazione_Tipologia] [int] IDENTITY(1,1) NOT NULL,
	[Codice_Prestazione] [nvarchar](50) NULL,
	[Chiave_Import] [nvarchar](200) NULL,	
	[Descrizione] [nvarchar](500) NULL,
	[Note] [text] NULL,
 CONSTRAINT [PK_Prestazioni_Tipologie] PRIMARY KEY CLUSTERED 
(
	[ID_Prestazione_Tipologia] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

END


ALTER TABLE Prestazioni_Tipologie ALTER COLUMN Note nvarchar(500)


IF NOT EXISTS(SELECT name FROM sys.sysobjects  WHERE Name = N'Amb_Appuntamento' AND xtype = N'U')
BEGIN
CREATE TABLE Amb_Appuntamento([ID] [int] IDENTITY(1,1) NOT NULL) ON [PRIMARY];
END


IF NOT EXISTS(SELECT 1 FROM sys.columns WHERE Name = N'CodiceOperazione' AND Object_ID = Object_ID(N'Amb_Appuntamento'))
BEGIN
ALTER TABLE Amb_Appuntamento ADD CodiceOperazione integer;
END

IF NOT EXISTS(SELECT 1 FROM sys.columns WHERE Name = N'Data' AND Object_ID = Object_ID(N'Amb_Appuntamento'))
BEGIN
ALTER TABLE Amb_Appuntamento ADD Data date;
END


IF NOT EXISTS(SELECT 1 FROM sys.columns WHERE Name = N'Ora' AND Object_ID = Object_ID(N'Amb_Appuntamento'))
BEGIN
ALTER TABLE Amb_Appuntamento ADD Ora  time;
END

IF NOT EXISTS(SELECT 1 FROM sys.columns WHERE Name = N'idUtente' AND Object_ID = Object_ID(N'Amb_Appuntamento'))
BEGIN
ALTER TABLE Amb_Appuntamento ADD idUtente integer;
END


IF NOT EXISTS(SELECT 1 FROM sys.columns WHERE Name = N'DataDomanda' AND Object_ID = Object_ID(N'Amb_Appuntamento'))
BEGIN
ALTER TABLE Amb_Appuntamento ADD DataDomanda date;
END

IF NOT EXISTS(SELECT 1 FROM sys.columns WHERE Name = N'Operatore' AND Object_ID = Object_ID(N'Amb_Appuntamento'))
BEGIN
ALTER TABLE Amb_Appuntamento ADD Operatore integer;
END

IF NOT EXISTS(SELECT 1 FROM sys.columns WHERE Name = N'TipoContatto' AND Object_ID = Object_ID(N'Amb_Appuntamento'))
BEGIN
ALTER TABLE Amb_Appuntamento ADD TipoContatto varchar(250);
END


IF NOT EXISTS(SELECT 1 FROM sys.columns WHERE Name = N'CodiceSanitario' AND Object_ID = Object_ID(N'Amb_Appuntamento'))
BEGIN
ALTER TABLE Amb_Appuntamento ADD CodiceSanitario varchar(250);
END

IF NOT EXISTS(SELECT 1 FROM sys.columns WHERE Name = N'Provenienza' AND Object_ID = Object_ID(N'Amb_Appuntamento'))
BEGIN
ALTER TABLE Amb_Appuntamento ADD Provenienza varchar(250);
END

IF NOT EXISTS(SELECT 1 FROM sys.columns WHERE Name = N'ModalitaConsegna' AND Object_ID = Object_ID(N'Amb_Appuntamento'))
BEGIN
ALTER TABLE Amb_Appuntamento ADD ModalitaConsegna  varchar(250);
END

IF NOT EXISTS(SELECT 1 FROM sys.columns WHERE Name = N'CodiceImpegnativa' AND Object_ID = Object_ID(N'Amb_Appuntamento'))
BEGIN
ALTER TABLE Amb_Appuntamento ADD CodiceImpegnativa  varchar(250);
END

IF NOT EXISTS(SELECT 1 FROM sys.columns WHERE Name = N'Contratto' AND Object_ID = Object_ID(N'Amb_Appuntamento'))
BEGIN
ALTER TABLE Amb_Appuntamento ADD Contratto varchar(250);
END


IF NOT EXISTS(SELECT 1 FROM sys.columns WHERE Name = N'Priorita' AND Object_ID = Object_ID(N'Amb_Appuntamento'))
BEGIN
ALTER TABLE Amb_Appuntamento ADD Priorita varchar(250);
END

IF NOT EXISTS(SELECT 1 FROM sys.columns WHERE Name = N'TestoQuesito' AND Object_ID = Object_ID(N'Amb_Appuntamento'))
BEGIN
ALTER TABLE Amb_Appuntamento ADD TestoQuesito varchar(250);
END


IF NOT EXISTS(SELECT 1 FROM sys.columns WHERE Name = N'OperatoreMedico' AND Object_ID = Object_ID(N'Amb_Appuntamento'))
BEGIN
ALTER TABLE Amb_Appuntamento ADD OperatoreMedico int;
END

IF NOT EXISTS(SELECT 1 FROM sys.columns WHERE Name = N'IDModalitaInvioReferto' AND Object_ID = Object_ID(N'Amb_Appuntamento'))
BEGIN
ALTER TABLE Amb_Appuntamento ADD IDModalitaInvioReferto int;
END

IF NOT EXISTS(SELECT 1 FROM sys.columns WHERE Name = N'Eliminato' AND Object_ID = Object_ID(N'Amb_Appuntamento'))
BEGIN
ALTER TABLE Amb_Appuntamento ADD Eliminato int;
END


IF NOT EXISTS(SELECT 1 FROM sys.columns WHERE Name = N'MastroExtra' AND Object_ID = Object_ID(N'Amb_Appuntamento'))
BEGIN
ALTER TABLE Amb_Appuntamento ADD MastroExtra int;
END


IF NOT EXISTS(SELECT 1 FROM sys.columns WHERE Name = N'ContoExtra' AND Object_ID = Object_ID(N'Amb_Appuntamento'))
BEGIN
ALTER TABLE Amb_Appuntamento ADD ContoExtra int;
END

IF NOT EXISTS(SELECT 1 FROM sys.columns WHERE Name = N'SottoContoExtra' AND Object_ID = Object_ID(N'Amb_Appuntamento'))
BEGIN
ALTER TABLE Amb_Appuntamento ADD SottoContoExtra int;
END


IF NOT EXISTS(SELECT 1 FROM sys.columns WHERE Name = N'CodiceIVA' AND Object_ID = Object_ID(N'Amb_Appuntamento'))
BEGIN
ALTER TABLE Amb_Appuntamento ADD CodiceIVA varchar(3);
END


IF NOT EXISTS(SELECT 1 FROM sys.columns WHERE Name = N'DareAvere' AND Object_ID = Object_ID(N'Amb_Appuntamento'))
BEGIN
ALTER TABLE Amb_Appuntamento ADD DareAvere varchar(1);
END


IF NOT EXISTS(SELECT 1 FROM sys.columns WHERE Name = N'Importo' AND Object_ID = Object_ID(N'Amb_Appuntamento'))
BEGIN
ALTER TABLE Amb_Appuntamento ADD Importo float;
END


IF NOT EXISTS(SELECT 1 FROM sys.columns WHERE Name = N'Descrizione' AND Object_ID = Object_ID(N'Amb_Appuntamento'))
BEGIN
ALTER TABLE Amb_Appuntamento ADD Descrizione varchar(250);
END



IF NOT EXISTS(SELECT name FROM sys.sysobjects  WHERE Name = N'Amb_Prestazione' AND xtype = N'U')
BEGIN
CREATE TABLE Amb_Prestazione([ID] [int] IDENTITY(1,1) NOT NULL) ON [PRIMARY];
END


IF NOT EXISTS(SELECT 1 FROM sys.columns WHERE Name = N'ID_Appuntamento' AND Object_ID = Object_ID(N'Amb_Prestazione'))
BEGIN
ALTER TABLE Amb_Prestazione ADD ID_Appuntamento int;
END

IF NOT EXISTS(SELECT 1 FROM sys.columns WHERE Name = N'ID_Cliente' AND Object_ID = Object_ID(N'Amb_Prestazione'))
BEGIN
ALTER TABLE Amb_Prestazione ADD ID_Cliente int;
END

IF NOT EXISTS(SELECT 1 FROM sys.columns WHERE Name = N'ID_Operatore' AND Object_ID = Object_ID(N'Amb_Prestazione'))
BEGIN
ALTER TABLE Amb_Prestazione ADD ID_Operatore int;
END

IF NOT EXISTS(SELECT 1 FROM sys.columns WHERE Name = N'ID_ePersonam' AND Object_ID = Object_ID(N'Amb_Prestazione'))
BEGIN
ALTER TABLE Amb_Prestazione ADD ID_ePersonam  int;
END


IF NOT EXISTS(SELECT 1 FROM sys.columns WHERE Name = N'Codice_Prestazione' AND Object_ID = Object_ID(N'Amb_Prestazione'))
BEGIN
ALTER TABLE Amb_Prestazione ADD Codice_Prestazione varchar(250);
END

IF NOT EXISTS(SELECT 1 FROM sys.columns WHERE Name = N'Descrizione' AND Object_ID = Object_ID(N'Amb_Prestazione'))
BEGIN
ALTER TABLE Amb_Prestazione ADD Descrizione  varchar(250);
END


IF NOT EXISTS(SELECT 1 FROM sys.columns WHERE Name = N'CheckIn' AND Object_ID = Object_ID(N'Amb_Prestazione'))
BEGIN
ALTER TABLE Amb_Prestazione ADD CheckIn   int;
END

IF NOT EXISTS(SELECT 1 FROM sys.columns WHERE Name = N'Fatturata' AND Object_ID = Object_ID(N'Amb_Prestazione'))
BEGIN
ALTER TABLE Amb_Prestazione ADD Fatturata  int;
END

IF NOT EXISTS(SELECT 1 FROM sys.columns WHERE Name = N'OscuramentoPrestazione' AND Object_ID = Object_ID(N'Amb_Prestazione'))
BEGIN
ALTER TABLE Amb_Prestazione ADD OscuramentoPrestazione int;
END

IF NOT EXISTS(SELECT 1 FROM sys.columns WHERE Name = N'ConsensoFirmato' AND Object_ID = Object_ID(N'Amb_Prestazione'))
BEGIN
ALTER TABLE Amb_Prestazione ADD ConsensoFirmato int;
END

IF NOT EXISTS(SELECT 1 FROM sys.columns WHERE Name = N'PathConsensoFirmato' AND Object_ID = Object_ID(N'Amb_Prestazione'))
BEGIN
ALTER TABLE Amb_Prestazione ADD PathConsensoFirmato varchar(250);
END






IF NOT EXISTS(SELECT name FROM sys.sysobjects  WHERE Name = N'Amb_Assistito' AND xtype = N'U')
BEGIN
CREATE TABLE Amb_Assistito([ID] [int] IDENTITY(1,1) NOT NULL) ON [PRIMARY];
END



IF NOT EXISTS(SELECT 1 FROM sys.columns WHERE Name = N'Cognome' AND Object_ID = Object_ID(N'Amb_Assistito'))
BEGIN
ALTER TABLE Amb_Assistito ADD Cognome varchar(250);
END

IF NOT EXISTS(SELECT 1 FROM sys.columns WHERE Name = N'Nome' AND Object_ID = Object_ID(N'Amb_Assistito'))
BEGIN
ALTER TABLE Amb_Assistito ADD Nome varchar(250);
END

IF NOT EXISTS(SELECT 1 FROM sys.columns WHERE Name = N'CodiceFiscale' AND Object_ID = Object_ID(N'Amb_Assistito'))
BEGIN
ALTER TABLE Amb_Assistito ADD CodiceFiscale varchar(250);
END

IF NOT EXISTS(SELECT 1 FROM sys.columns WHERE Name = N'Sesso' AND Object_ID = Object_ID(N'Amb_Assistito'))
BEGIN
ALTER TABLE Amb_Assistito ADD Sesso varchar(250);
END

IF NOT EXISTS(SELECT 1 FROM sys.columns WHERE Name = N'Telefono' AND Object_ID = Object_ID(N'Amb_Assistito'))
BEGIN
ALTER TABLE Amb_Assistito ADD Telefono varchar(250);
END


IF NOT EXISTS(SELECT 1 FROM sys.columns WHERE Name = N'Indirizzo' AND Object_ID = Object_ID(N'Amb_Assistito'))
BEGIN
ALTER TABLE Amb_Assistito ADD Indirizzo varchar(250);
END

IF NOT EXISTS(SELECT 1 FROM sys.columns WHERE Name = N'CAP' AND Object_ID = Object_ID(N'Amb_Assistito'))
BEGIN
ALTER TABLE Amb_Assistito ADD CAP varchar(250);
END

IF NOT EXISTS(SELECT 1 FROM sys.columns WHERE Name = N'Comune' AND Object_ID = Object_ID(N'Amb_Assistito'))
BEGIN
ALTER TABLE Amb_Assistito ADD Comune varchar(250);
END


IF NOT EXISTS(SELECT 1 FROM sys.columns WHERE Name = N'IDFasciaIsee' AND Object_ID = Object_ID(N'Amb_Assistito'))
BEGIN
ALTER TABLE Amb_Assistito ADD IDFasciaIsee  int;
END


IF NOT EXISTS(SELECT 1 FROM sys.columns WHERE Name = N'DataNascita' AND Object_ID = Object_ID(N'Amb_Assistito'))
BEGIN
ALTER TABLE Amb_Assistito ADD DataNascita  date;
END




IF NOT EXISTS(SELECT name FROM sys.sysobjects  WHERE Name = N'Tabella_FasciaISE' AND xtype = N'U')
BEGIN
CREATE TABLE Tabella_FasciaISE(ID_FasciaISEE [int] IDENTITY(1,1) NOT NULL) ON [PRIMARY];
END



IF NOT EXISTS(SELECT 1 FROM sys.columns WHERE Name = N'Descrizione' AND Object_ID = Object_ID(N'Tabella_FasciaISE'))
BEGIN
ALTER TABLE Tabella_FasciaISE ADD Descrizione varchar(250);
END

IF NOT EXISTS(SELECT name FROM sys.sysobjects  WHERE Name = N'Prestazioni_Listino' AND xtype = N'U')
BEGIN
CREATE TABLE [dbo].[Prestazioni_Listino] (
    [ID_Listino]               INT   IDENTITY (1, 1) NOT NULL,
    [ID_Prestazione_Tipologia] INT   NOT NULL,
    [ID_ContrattoDiServizio]   INT   NOT NULL,
    [Importo]                  MONEY NOT NULL,
    [ID_FasciaISE]             INT   NULL,
    CONSTRAINT [PK_Prestazioni_Listino] PRIMARY KEY CLUSTERED ([ID_Listino] ASC)
);

END

IF NOT EXISTS(SELECT name FROM sys.sysobjects  WHERE Name = N'Branche' AND xtype = N'U')
BEGIN

CREATE TABLE [dbo].[Branche] (
    [ID_Branca]   INT            IDENTITY (1, 1) NOT NULL,
    [Descrizione] NVARCHAR (200) NOT NULL,
    CONSTRAINT [PK_Branche] PRIMARY KEY CLUSTERED ([ID_Branca] ASC)
);
END

IF NOT EXISTS(SELECT name FROM sys.sysobjects  WHERE Name = N'Prestazioni_Tipologie__Branche' AND xtype = N'U')
BEGIN
CREATE TABLE [dbo].[Prestazioni_Tipologie__Branche] (
    [ID_Prestazione_Tipologia] INT NOT NULL,
    [ID_Branca]                INT NOT NULL,
    CONSTRAINT [PK_Prestazioni_Tipologie__Branche] PRIMARY KEY CLUSTERED ([ID_Prestazione_Tipologia] ASC, [ID_Branca] ASC)
);
END


IF NOT EXISTS(SELECT name FROM sys.sysobjects  WHERE Name = N'Prestazioni_Tipologie__ePersonam_exams' AND xtype = N'U')
BEGIN

CREATE TABLE [dbo].[Prestazioni_Tipologie__ePersonam_exams](
	[ID_Prestazione_Tipologia__ePersonam_exams] [int] IDENTITY(1,1) NOT NULL,
	[ID_Prestazione_Tipologia] [int] NOT NULL,
	[Codice_ePersonam] [nvarchar](200) NOT NULL,
	[Descrizione_ePersonam] [nvarchar](500) NULL,
	[Tipologia_ePersonam] [nvarchar](50) NULL,
 CONSTRAINT [PK_Prestazioni_Tipologie_ePersonam] PRIMARY KEY CLUSTERED 
(
	[ID_Prestazione_Tipologia__ePersonam_exams] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

END



IF NOT EXISTS(SELECT name FROM sys.sysobjects  WHERE Name = N'Amb_Parametri' AND xtype = N'U')
BEGIN
CREATE TABLE Amb_Parametri([ID] [int] IDENTITY(1,1) NOT NULL) ON [PRIMARY];
END


IF NOT EXISTS(SELECT 1 FROM sys.columns WHERE Name = N'MastroRicavo' AND Object_ID = Object_ID(N'Amb_Parametri'))
BEGIN
ALTER TABLE Amb_Parametri ADD MastroRicavo  int;
END


IF NOT EXISTS(SELECT 1 FROM sys.columns WHERE Name = N'ContoRicavo' AND Object_ID = Object_ID(N'Amb_Parametri'))
BEGIN
ALTER TABLE Amb_Parametri ADD ContoRicavo  int;
END


IF NOT EXISTS(SELECT 1 FROM sys.columns WHERE Name = N'SottoContoRicavo' AND Object_ID = Object_ID(N'Amb_Parametri'))
BEGIN
ALTER TABLE Amb_Parametri ADD SottoContoRicavo  int;
END


IF NOT EXISTS(SELECT 1 FROM sys.columns WHERE Name = N'CodiceIVA' AND Object_ID = Object_ID(N'Amb_Parametri'))
BEGIN
ALTER TABLE Amb_Parametri ADD CodiceIVA  varchar(3);
END


IF NOT EXISTS(SELECT 1 FROM sys.columns WHERE Name = N'IdEpersonamStruttura' AND Object_ID = Object_ID(N'Amb_Parametri'))
BEGIN
ALTER TABLE Amb_Parametri ADD IdEpersonamStruttura  int;
END

IF NOT EXISTS(SELECT name FROM sys.sysobjects  WHERE Name = N'Ric_Contratti' AND xtype = N'U')
BEGIN
CREATE TABLE Ric_Contratti([ID] [int] IDENTITY(1,1) NOT NULL) ON [PRIMARY];
END


IF NOT EXISTS(SELECT 1 FROM sys.columns WHERE Name = N'Codice' AND Object_ID = Object_ID(N'Ric_Contratti'))
BEGIN
ALTER TABLE Ric_Contratti ADD Codice   varchar(250);
END

IF NOT EXISTS(SELECT 1 FROM sys.columns WHERE Name = N'Descrizione' AND Object_ID = Object_ID(N'Ric_Contratti'))
BEGIN
ALTER TABLE Ric_Contratti ADD Descrizione varchar(250);
END


IF NOT EXISTS(SELECT 1 FROM sys.columns WHERE Name = N'PercentualePrimoOperatore' AND Object_ID = Object_ID(N'Ric_Contratti'))
BEGIN
ALTER TABLE Ric_Contratti ADD PercentualePrimoOperatore float;
END

IF NOT EXISTS(SELECT 1 FROM sys.columns WHERE Name = N'PercentualeSuccessivoOperatore' AND Object_ID = Object_ID(N'Ric_Contratti'))
BEGIN
ALTER TABLE Ric_Contratti ADD PercentualeSuccessivoOperatore float;
END


IF NOT EXISTS(SELECT name FROM sys.sysobjects  WHERE Name = N'Ric_Drg' AND xtype = N'U')
BEGIN
CREATE TABLE Ric_Drg([ID] [int] IDENTITY(1,1) NOT NULL) ON [PRIMARY];
END

IF NOT EXISTS(SELECT 1 FROM sys.columns WHERE Name = N'Codice' AND Object_ID = Object_ID(N'Ric_Drg'))
BEGIN
ALTER TABLE Ric_Drg ADD Codice   varchar(250);
END

IF NOT EXISTS(SELECT 1 FROM sys.columns WHERE Name = N'Descrizione' AND Object_ID = Object_ID(N'Ric_Drg'))
BEGIN
ALTER TABLE Ric_Drg ADD Descrizione varchar(250);
END

IF NOT EXISTS(SELECT 1 FROM sys.columns WHERE Name = N'ordinari' AND Object_ID = Object_ID(N'Ric_Drg'))
BEGIN
ALTER TABLE Ric_Drg ADD ordinari float;
END

IF NOT EXISTS(SELECT 1 FROM sys.columns WHERE Name = N'Giorno1' AND Object_ID = Object_ID(N'Ric_Drg'))
BEGIN
ALTER TABLE Ric_Drg ADD Giorno1 float;
END

IF NOT EXISTS(SELECT 1 FROM sys.columns WHERE Name = N'DayHospital' AND Object_ID = Object_ID(N'Ric_Drg'))
BEGIN
ALTER TABLE Ric_Drg ADD DayHospital float;
END

IF NOT EXISTS(SELECT 1 FROM sys.columns WHERE Name = N'Soglia' AND Object_ID = Object_ID(N'Ric_Drg'))
BEGIN
ALTER TABLE Ric_Drg ADD Soglia integer;
END

IF NOT EXISTS(SELECT 1 FROM sys.columns WHERE Name = N'IncrementoProDie' AND Object_ID = Object_ID(N'Ric_Drg'))
BEGIN
ALTER TABLE Ric_Drg ADD IncrementoProDie float;
END