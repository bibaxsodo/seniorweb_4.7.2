﻿Imports Microsoft.VisualBasic
Imports System.Data.OleDb
Imports System.Web.Hosting

Partial Class SqlDatabase_UpDateSqlDb
    Inherits System.Web.UI.Page

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        UpDateDB(Session("DC_OSPITE"), "OSPITE")
        UpDateDB(Session("DC_GENERALE"), "GENERALE")
        UpDateDB(Session("DC_OSPITIACCESSORI"), "OSPITIACCESSORI")
        UpDateDB(Session("DC_TABELLE"), "TABELLE")


        UpDateStoredProcedure(Session("DC_OSPITE"), "OSPITE")
        UpDateStoredProcedure(Session("DC_GENERALE"), "GENERALE")
        UpDateStoredProcedure(Session("DC_OSPITIACCESSORI"), "OSPITIACCESSORI")
        UpDateStoredProcedure(Session("DC_TABELLE"), "TABELLE")


        UpDateDBIndex(Session("DC_OSPITE"), "OSPITE")
        UpDateDBIndex(Session("DC_GENERALE"), "GENERALE")

        UpDateDBIndex(Session("DC_TABELLE"), "TABELLE")


        Response.Redirect("..\Strumenti.aspx")

    End Sub

    Private Sub UpDateDBIndex(ByVal condizione As String, ByVal nome As String)
        Dim cn As OleDbConnection
        Dim MySql As String = ""

        cn = New Data.OleDb.OleDbConnection(condizione)

        cn.Open()


        Dim tr As System.IO.TextReader = System.IO.File.OpenText(HostingEnvironment.ApplicationPhysicalPath() & "\SqlDataBase\Index" & nome & ".sql")

        MySql = tr.ReadToEnd()

        Dim cmdI As New OleDbCommand()
        cmdI.CommandText = MySql
        cmdI.Connection = cn
        cmdI.ExecuteNonQuery()


        tr.Close()


        cn.Close()

    End Sub

    Private Sub UpDateDB(ByVal condizione As String, ByVal nome As String)
        Dim cn As OleDbConnection
        Dim MySql As String = ""

        cn = New Data.OleDb.OleDbConnection(condizione)

        cn.Open()


        Dim tr As System.IO.TextReader = System.IO.File.OpenText(HostingEnvironment.ApplicationPhysicalPath() & "\SqlDataBase\" & nome & ".sql")

        MySql = tr.ReadToEnd()

        Dim cmdI As New OleDbCommand()
        cmdI.CommandText = MySql
        cmdI.Connection = cn
        cmdI.ExecuteNonQuery()


        tr.Close()


        cn.Close()

    End Sub

    Private Sub UpDateStoredProcedure(ByVal condizione As String, ByVal nome As String)
        Dim cn As SqlClient.SqlConnection
        Dim MySql As String = ""

        cn = New Data.SqlClient.SqlConnection(condizione.Replace("Provider=SQLOLEDB;",""))

        cn.Open()


        Dim tr As System.IO.TextReader = System.IO.File.OpenText(HostingEnvironment.ApplicationPhysicalPath() & "\SqlDataBase\StoredProcedure" & nome & ".sql")

        MySql = tr.ReadToEnd()

        Dim cmdI As New SqlClient.SqlCommand()
        cmdI.CommandText = MySql
        cmdI.Connection = cn        
        cmdI.ExecuteNonQuery()


        tr.Close()


        cn.Close()

    End Sub
End Class
