IF NOT EXISTS(SELECT name FROM sys.sysobjects  WHERE Name = N'MovimentiLibretto' AND xtype = N'U')
BEGIN
CREATE TABLE MovimentiLibretto([ID] [int] IDENTITY(1,1) NOT NULL) ON [PRIMARY];
END

IF NOT EXISTS(SELECT 1 FROM sys.columns WHERE Name = N'Utente' AND Object_ID = Object_ID(N'MovimentiLibretto'))
BEGIN
ALTER TABLE MovimentiLibretto ADD [Utente] nvarchar (50);
END

IF NOT EXISTS(SELECT 1 FROM sys.columns WHERE Name = N'DataAggiornamento' AND Object_ID = Object_ID(N'MovimentiLibretto'))
BEGIN
ALTER TABLE MovimentiLibretto ADD [DataAggiornamento] datetime;
END

IF NOT EXISTS(SELECT 1 FROM sys.columns WHERE Name = N'CentroServizio' AND Object_ID = Object_ID(N'MovimentiLibretto'))
BEGIN
ALTER TABLE MovimentiLibretto ADD [CentroServizio] nvarchar (4);
END

IF NOT EXISTS(SELECT 1 FROM sys.columns WHERE Name = N'CodiceOspite' AND Object_ID = Object_ID(N'MovimentiLibretto'))
BEGIN
ALTER TABLE MovimentiLibretto ADD [CodiceOspite] int;
END

IF NOT EXISTS(SELECT 1 FROM sys.columns WHERE Name = N'DataValidita' AND Object_ID = Object_ID(N'MovimentiLibretto'))
BEGIN
ALTER TABLE MovimentiLibretto ADD [DataValidita] datetime;
END

IF NOT EXISTS(SELECT 1 FROM sys.columns WHERE Name = N'Descrizione' AND Object_ID = Object_ID(N'MovimentiLibretto'))
BEGIN
ALTER TABLE MovimentiLibretto ADD [Descrizione] nvarchar (50);
END

IF NOT EXISTS(SELECT 1 FROM sys.columns WHERE Name = N'DatiLibretto' AND Object_ID = Object_ID(N'MovimentiLibretto'))
BEGIN
ALTER TABLE MovimentiLibretto ADD [DatiLibretto] nvarchar (2);
END

IF NOT EXISTS(SELECT 1 FROM sys.columns WHERE Name = N'Saldo' AND Object_ID = Object_ID(N'MovimentiLibretto'))
BEGIN
ALTER TABLE MovimentiLibretto ADD [Saldo] float;
END

IF NOT EXISTS(SELECT name FROM sys.sysobjects  WHERE Name = N'Parametri' AND xtype = N'U')
BEGIN
CREATE TABLE Parametri([ID] [int] IDENTITY(1,1) NOT NULL) ON [PRIMARY];
END

IF NOT EXISTS(SELECT 1 FROM sys.columns WHERE Name = N'Mastro' AND Object_ID = Object_ID(N'Parametri'))
BEGIN
ALTER TABLE Parametri ADD [Mastro] int;
END

IF NOT EXISTS(SELECT 1 FROM sys.columns WHERE Name = N'CausaleRettaMedicinale' AND Object_ID = Object_ID(N'Parametri'))
BEGIN
ALTER TABLE Parametri ADD [CausaleRettaMedicinale] nvarchar (3);
END

IF NOT EXISTS(SELECT name FROM sys.sysobjects  WHERE Name = N'Stanze' AND xtype = N'U')
BEGIN
CREATE TABLE Stanze([ID] [int] IDENTITY(1,1) NOT NULL) ON [PRIMARY];
END

IF NOT EXISTS(SELECT 1 FROM sys.columns WHERE Name = N'Utente' AND Object_ID = Object_ID(N'Stanze'))
BEGIN
ALTER TABLE Stanze ADD [Utente] nvarchar (50);
END

IF NOT EXISTS(SELECT 1 FROM sys.columns WHERE Name = N'DataAggiornamento' AND Object_ID = Object_ID(N'Stanze'))
BEGIN
ALTER TABLE Stanze ADD [DataAggiornamento] datetime;
END

IF NOT EXISTS(SELECT 1 FROM sys.columns WHERE Name = N'Villa' AND Object_ID = Object_ID(N'Stanze'))
BEGIN
ALTER TABLE Stanze ADD [Villa] nvarchar (2);
END

IF NOT EXISTS(SELECT 1 FROM sys.columns WHERE Name = N'Reparto' AND Object_ID = Object_ID(N'Stanze'))
BEGIN
ALTER TABLE Stanze ADD [Reparto] nvarchar (2);
END

IF NOT EXISTS(SELECT 1 FROM sys.columns WHERE Name = N'Piano' AND Object_ID = Object_ID(N'Stanze'))
BEGIN
ALTER TABLE Stanze ADD [Piano] nvarchar (2);
END

IF NOT EXISTS(SELECT 1 FROM sys.columns WHERE Name = N'Stanza' AND Object_ID = Object_ID(N'Stanze'))
BEGIN
ALTER TABLE Stanze ADD [Stanza] nvarchar (2);
END

IF NOT EXISTS(SELECT 1 FROM sys.columns WHERE Name = N'Letto' AND Object_ID = Object_ID(N'Stanze'))
BEGIN
ALTER TABLE Stanze ADD [Letto] nvarchar (5);
END

IF NOT EXISTS(SELECT 1 FROM sys.columns WHERE Name = N'Tipologia' AND Object_ID = Object_ID(N'Stanze'))
BEGIN
ALTER TABLE Stanze ADD [Tipologia] nvarchar (6);
END

IF NOT EXISTS(SELECT 1 FROM sys.columns WHERE Name = N'NumeroTelefono' AND Object_ID = Object_ID(N'Stanze'))
BEGIN
ALTER TABLE Stanze ADD [NumeroTelefono] nvarchar (50);
END

IF NOT EXISTS(SELECT 1 FROM sys.columns WHERE Name = N'NumeroBiancheria' AND Object_ID = Object_ID(N'Stanze'))
BEGIN
ALTER TABLE Stanze ADD [NumeroBiancheria] nvarchar (50);
END

IF NOT EXISTS(SELECT 1 FROM sys.columns WHERE Name = N'TipoArredo' AND Object_ID = Object_ID(N'Stanze'))
BEGIN
ALTER TABLE Stanze ADD [TipoArredo] nvarchar (2);
END

IF NOT EXISTS(SELECT 1 FROM sys.columns WHERE Name = N'StanzaInterno' AND Object_ID = Object_ID(N'Stanze'))
BEGIN
ALTER TABLE Stanze ADD [StanzaInterno] nvarchar (5);
END

IF NOT EXISTS(SELECT name FROM sys.sysobjects  WHERE Name = N'StrutturaXOggetti' AND xtype = N'U')
BEGIN
CREATE TABLE StrutturaXOggetti([ID] [int] IDENTITY(1,1) NOT NULL) ON [PRIMARY];
END

IF NOT EXISTS(SELECT 1 FROM sys.columns WHERE Name = N'Utente' AND Object_ID = Object_ID(N'StrutturaXOggetti'))
BEGIN
ALTER TABLE StrutturaXOggetti ADD [Utente] nvarchar (50);
END

IF NOT EXISTS(SELECT 1 FROM sys.columns WHERE Name = N'DataAggiornamento' AND Object_ID = Object_ID(N'StrutturaXOggetti'))
BEGIN
ALTER TABLE StrutturaXOggetti ADD [DataAggiornamento] datetime;
END

IF NOT EXISTS(SELECT 1 FROM sys.columns WHERE Name = N'Codice' AND Object_ID = Object_ID(N'StrutturaXOggetti'))
BEGIN
ALTER TABLE StrutturaXOggetti ADD [Codice] nvarchar (5);
END

IF NOT EXISTS(SELECT 1 FROM sys.columns WHERE Name = N'Descrizione' AND Object_ID = Object_ID(N'StrutturaXOggetti'))
BEGIN
ALTER TABLE StrutturaXOggetti ADD [Descrizione] nvarchar (50);
END

IF NOT EXISTS(SELECT 1 FROM sys.columns WHERE Name = N'TipoOperazione' AND Object_ID = Object_ID(N'StrutturaXOggetti'))
BEGIN
ALTER TABLE StrutturaXOggetti ADD [TipoOperazione] nvarchar (1);
END

IF NOT EXISTS(SELECT 1 FROM sys.columns WHERE Name = N'Testo' AND Object_ID = Object_ID(N'StrutturaXOggetti'))
BEGIN
ALTER TABLE StrutturaXOggetti ADD [Testo] nvarchar (Max);
END

IF NOT EXISTS(SELECT name FROM sys.sysobjects  WHERE Name = N'TabellaFarmacie' AND xtype = N'U')
BEGIN
CREATE TABLE TabellaFarmacie([ID] [int] IDENTITY(1,1) NOT NULL) ON [PRIMARY];
END

IF NOT EXISTS(SELECT 1 FROM sys.columns WHERE Name = N'Codice' AND Object_ID = Object_ID(N'TabellaFarmacie'))
BEGIN
ALTER TABLE TabellaFarmacie ADD [Codice] int;
END

IF NOT EXISTS(SELECT 1 FROM sys.columns WHERE Name = N'RagioneSociale' AND Object_ID = Object_ID(N'TabellaFarmacie'))
BEGIN
ALTER TABLE TabellaFarmacie ADD [RagioneSociale] nvarchar (64);
END

IF NOT EXISTS(SELECT 1 FROM sys.columns WHERE Name = N'Indirizzo' AND Object_ID = Object_ID(N'TabellaFarmacie'))
BEGIN
ALTER TABLE TabellaFarmacie ADD [Indirizzo] nvarchar (64);
END

IF NOT EXISTS(SELECT 1 FROM sys.columns WHERE Name = N'Cap' AND Object_ID = Object_ID(N'TabellaFarmacie'))
BEGIN
ALTER TABLE TabellaFarmacie ADD [Cap] nvarchar (5);
END

IF NOT EXISTS(SELECT 1 FROM sys.columns WHERE Name = N'Provincia' AND Object_ID = Object_ID(N'TabellaFarmacie'))
BEGIN
ALTER TABLE TabellaFarmacie ADD [Provincia] nvarchar (3);
END

IF NOT EXISTS(SELECT 1 FROM sys.columns WHERE Name = N'Comune' AND Object_ID = Object_ID(N'TabellaFarmacie'))
BEGIN
ALTER TABLE TabellaFarmacie ADD [Comune] nvarchar (3);
END

IF NOT EXISTS(SELECT name FROM sys.sysobjects  WHERE Name = N'TabellaLibretto' AND xtype = N'U')
BEGIN
CREATE TABLE TabellaLibretto([ID] [int] IDENTITY(1,1) NOT NULL) ON [PRIMARY];
END

IF NOT EXISTS(SELECT 1 FROM sys.columns WHERE Name = N'Utente' AND Object_ID = Object_ID(N'TabellaLibretto'))
BEGIN
ALTER TABLE TabellaLibretto ADD [Utente] nvarchar (50);
END

IF NOT EXISTS(SELECT 1 FROM sys.columns WHERE Name = N'DataAggiornamento' AND Object_ID = Object_ID(N'TabellaLibretto'))
BEGIN
ALTER TABLE TabellaLibretto ADD [DataAggiornamento] datetime;
END

IF NOT EXISTS(SELECT 1 FROM sys.columns WHERE Name = N'CentroServizio' AND Object_ID = Object_ID(N'TabellaLibretto'))
BEGIN
ALTER TABLE TabellaLibretto ADD [CentroServizio] nvarchar (4);
END

IF NOT EXISTS(SELECT 1 FROM sys.columns WHERE Name = N'CodiceOspite' AND Object_ID = Object_ID(N'TabellaLibretto'))
BEGIN
ALTER TABLE TabellaLibretto ADD [CodiceOspite] int;
END

IF NOT EXISTS(SELECT 1 FROM sys.columns WHERE Name = N'Codice' AND Object_ID = Object_ID(N'TabellaLibretto'))
BEGIN
ALTER TABLE TabellaLibretto ADD [Codice] nvarchar (2);
END

IF NOT EXISTS(SELECT 1 FROM sys.columns WHERE Name = N'Descrizione' AND Object_ID = Object_ID(N'TabellaLibretto'))
BEGIN
ALTER TABLE TabellaLibretto ADD [Descrizione] nvarchar (50);
END

IF NOT EXISTS(SELECT name FROM sys.sysobjects  WHERE Name = N'TabellaMedicinali' AND xtype = N'U')
BEGIN
CREATE TABLE TabellaMedicinali([ID] [int] IDENTITY(1,1) NOT NULL) ON [PRIMARY];
END

IF NOT EXISTS(SELECT 1 FROM sys.columns WHERE Name = N'Codice' AND Object_ID = Object_ID(N'TabellaMedicinali'))
BEGIN
ALTER TABLE TabellaMedicinali ADD [Codice] int;
END

IF NOT EXISTS(SELECT 1 FROM sys.columns WHERE Name = N'Descrizione' AND Object_ID = Object_ID(N'TabellaMedicinali'))
BEGIN
ALTER TABLE TabellaMedicinali ADD [Descrizione] nvarchar (50);
END

IF NOT EXISTS(SELECT 1 FROM sys.columns WHERE Name = N'UnitaMisura' AND Object_ID = Object_ID(N'TabellaMedicinali'))
BEGIN
ALTER TABLE TabellaMedicinali ADD [UnitaMisura] nvarchar (2);
END

IF NOT EXISTS(SELECT 1 FROM sys.columns WHERE Name = N'Quantita' AND Object_ID = Object_ID(N'TabellaMedicinali'))
BEGIN
ALTER TABLE TabellaMedicinali ADD [Quantita] float;
END

IF NOT EXISTS(SELECT 1 FROM sys.columns WHERE Name = N'Note' AND Object_ID = Object_ID(N'TabellaMedicinali'))
BEGIN
ALTER TABLE TabellaMedicinali ADD [Note] nvarchar (Max);
END

IF NOT EXISTS(SELECT name FROM sys.sysobjects  WHERE Name = N'Tabelle' AND xtype = N'U')
BEGIN
CREATE TABLE Tabelle([ID] [int] IDENTITY(1,1) NOT NULL) ON [PRIMARY];
END

IF NOT EXISTS(SELECT 1 FROM sys.columns WHERE Name = N'Utente' AND Object_ID = Object_ID(N'Tabelle'))
BEGIN
ALTER TABLE Tabelle ADD [Utente] nvarchar (50);
END

IF NOT EXISTS(SELECT 1 FROM sys.columns WHERE Name = N'DataAggiornamento' AND Object_ID = Object_ID(N'Tabelle'))
BEGIN
ALTER TABLE Tabelle ADD [DataAggiornamento] datetime;
END

IF NOT EXISTS(SELECT 1 FROM sys.columns WHERE Name = N'TipoTabella' AND Object_ID = Object_ID(N'Tabelle'))
BEGIN
ALTER TABLE Tabelle ADD [TipoTabella] nvarchar (3);
END

IF NOT EXISTS(SELECT 1 FROM sys.columns WHERE Name = N'CodiceTabella' AND Object_ID = Object_ID(N'Tabelle'))
BEGIN
ALTER TABLE Tabelle ADD [CodiceTabella] nvarchar (4);
END

IF NOT EXISTS(SELECT 1 FROM sys.columns WHERE Name = N'Descrizione' AND Object_ID = Object_ID(N'Tabelle'))
BEGIN
ALTER TABLE Tabelle ADD [Descrizione] nvarchar (50);
END

IF NOT EXISTS(SELECT name FROM sys.sysobjects  WHERE Name = N'Movimenti' AND xtype = N'U')
BEGIN
CREATE TABLE Movimenti([ID] [int] IDENTITY(1,1) NOT NULL) ON [PRIMARY];
END

IF NOT EXISTS(SELECT 1 FROM sys.columns WHERE Name = N'Utente' AND Object_ID = Object_ID(N'Movimenti'))
BEGIN
ALTER TABLE Movimenti ADD [Utente] nvarchar (50);
END

IF NOT EXISTS(SELECT 1 FROM sys.columns WHERE Name = N'DataAggiornamento' AND Object_ID = Object_ID(N'Movimenti'))
BEGIN
ALTER TABLE Movimenti ADD [DataAggiornamento] datetime;
END

IF NOT EXISTS(SELECT 1 FROM sys.columns WHERE Name = N'CentroServizio' AND Object_ID = Object_ID(N'Movimenti'))
BEGIN
ALTER TABLE Movimenti ADD [CentroServizio] nvarchar (4);
END

IF NOT EXISTS(SELECT 1 FROM sys.columns WHERE Name = N'CodiceOspite' AND Object_ID = Object_ID(N'Movimenti'))
BEGIN
ALTER TABLE Movimenti ADD [CodiceOspite] int;
END

IF NOT EXISTS(SELECT 1 FROM sys.columns WHERE Name = N'Villa' AND Object_ID = Object_ID(N'Movimenti'))
BEGIN
ALTER TABLE Movimenti ADD [Villa] nvarchar (2);
END

IF NOT EXISTS(SELECT 1 FROM sys.columns WHERE Name = N'Reparto' AND Object_ID = Object_ID(N'Movimenti'))
BEGIN
ALTER TABLE Movimenti ADD [Reparto] nvarchar (2);
END

IF NOT EXISTS(SELECT 1 FROM sys.columns WHERE Name = N'Piano' AND Object_ID = Object_ID(N'Movimenti'))
BEGIN
ALTER TABLE Movimenti ADD [Piano] nvarchar (2);
END

IF NOT EXISTS(SELECT 1 FROM sys.columns WHERE Name = N'Stanza' AND Object_ID = Object_ID(N'Movimenti'))
BEGIN
ALTER TABLE Movimenti ADD [Stanza] nvarchar (10);
END

IF NOT EXISTS(SELECT 1 FROM sys.columns WHERE Name = N'Letto' AND Object_ID = Object_ID(N'Movimenti'))
BEGIN
ALTER TABLE Movimenti ADD [Letto] nvarchar (10);
END

IF NOT EXISTS(SELECT 1 FROM sys.columns WHERE Name = N'Data' AND Object_ID = Object_ID(N'Movimenti'))
BEGIN
ALTER TABLE Movimenti ADD [Data] datetime;
END

IF NOT EXISTS(SELECT 1 FROM sys.columns WHERE Name = N'Tipologia' AND Object_ID = Object_ID(N'Movimenti'))
BEGIN
ALTER TABLE Movimenti ADD [Tipologia] nvarchar (2);
END

IF NOT EXISTS(SELECT name FROM sys.sysobjects  WHERE Name = N'Denaro' AND xtype = N'U')
BEGIN
CREATE TABLE Denaro([ID] [int] IDENTITY(1,1) NOT NULL) ON [PRIMARY];
END

IF NOT EXISTS(SELECT 1 FROM sys.columns WHERE Name = N'Utente' AND Object_ID = Object_ID(N'Denaro'))
BEGIN
ALTER TABLE Denaro ADD [Utente] nvarchar (50);
END

IF NOT EXISTS(SELECT 1 FROM sys.columns WHERE Name = N'DataAggiornamento' AND Object_ID = Object_ID(N'Denaro'))
BEGIN
ALTER TABLE Denaro ADD [DataAggiornamento] datetime;
END

IF NOT EXISTS(SELECT 1 FROM sys.columns WHERE Name = N'CodiceOspite' AND Object_ID = Object_ID(N'Denaro'))
BEGIN
ALTER TABLE Denaro ADD [CodiceOspite] float;
END

IF NOT EXISTS(SELECT 1 FROM sys.columns WHERE Name = N'AnnoMovimento' AND Object_ID = Object_ID(N'Denaro'))
BEGIN
ALTER TABLE Denaro ADD [AnnoMovimento] smallint;
END

IF NOT EXISTS(SELECT 1 FROM sys.columns WHERE Name = N'NumeroMovimento' AND Object_ID = Object_ID(N'Denaro'))
BEGIN
ALTER TABLE Denaro ADD [NumeroMovimento] int;
END

IF NOT EXISTS(SELECT 1 FROM sys.columns WHERE Name = N'TipoOperazione' AND Object_ID = Object_ID(N'Denaro'))
BEGIN
ALTER TABLE Denaro ADD [TipoOperazione] nvarchar (1);
END

IF NOT EXISTS(SELECT 1 FROM sys.columns WHERE Name = N'Importo' AND Object_ID = Object_ID(N'Denaro'))
BEGIN
ALTER TABLE Denaro ADD [Importo] float;
END

IF NOT EXISTS(SELECT 1 FROM sys.columns WHERE Name = N'Causale' AND Object_ID = Object_ID(N'Denaro'))
BEGIN
ALTER TABLE Denaro ADD [Causale] nvarchar (2);
END

IF NOT EXISTS(SELECT 1 FROM sys.columns WHERE Name = N'CausaleDescrittiva' AND Object_ID = Object_ID(N'Denaro'))
BEGIN
ALTER TABLE Denaro ADD [CausaleDescrittiva] nvarchar (4);
END

IF NOT EXISTS(SELECT 1 FROM sys.columns WHERE Name = N'Descrizione' AND Object_ID = Object_ID(N'Denaro'))
BEGIN
ALTER TABLE Denaro ADD [Descrizione] nvarchar (50);
END

IF NOT EXISTS(SELECT 1 FROM sys.columns WHERE Name = N'DataRegistrazione' AND Object_ID = Object_ID(N'Denaro'))
BEGIN
ALTER TABLE Denaro ADD [DataRegistrazione] datetime;
END

IF NOT EXISTS(SELECT 1 FROM sys.columns WHERE Name = N'NumeroDocumento' AND Object_ID = Object_ID(N'Denaro'))
BEGIN
ALTER TABLE Denaro ADD [NumeroDocumento] int;
END

IF NOT EXISTS(SELECT 1 FROM sys.columns WHERE Name = N'Ripartizione' AND Object_ID = Object_ID(N'Denaro'))
BEGIN
ALTER TABLE Denaro ADD [Ripartizione] nvarchar (1);
END

IF NOT EXISTS(SELECT 1 FROM sys.columns WHERE Name = N'Parente' AND Object_ID = Object_ID(N'Denaro'))
BEGIN
ALTER TABLE Denaro ADD [Parente] tinyint;
END

IF NOT EXISTS(SELECT 1 FROM sys.columns WHERE Name = N'TipoAddebito' AND Object_ID = Object_ID(N'Denaro'))
BEGIN
ALTER TABLE Denaro ADD [TipoAddebito] varchar (2);
END

IF NOT EXISTS(SELECT name FROM sys.sysobjects  WHERE Name = N'Medicinali' AND xtype = N'U')
BEGIN
CREATE TABLE Medicinali([ID] [int] IDENTITY(1,1) NOT NULL) ON [PRIMARY];
END

IF NOT EXISTS(SELECT 1 FROM sys.columns WHERE Name = N'CSERV' AND Object_ID = Object_ID(N'Medicinali'))
BEGIN
ALTER TABLE Medicinali ADD [CSERV] nvarchar (4);
END

IF NOT EXISTS(SELECT 1 FROM sys.columns WHERE Name = N'CODICEOSPITE' AND Object_ID = Object_ID(N'Medicinali'))
BEGIN
ALTER TABLE Medicinali ADD [CODICEOSPITE] int;
END

IF NOT EXISTS(SELECT 1 FROM sys.columns WHERE Name = N'DATAREGISTRAZIONE' AND Object_ID = Object_ID(N'Medicinali'))
BEGIN
ALTER TABLE Medicinali ADD [DATAREGISTRAZIONE] datetime;
END

IF NOT EXISTS(SELECT 1 FROM sys.columns WHERE Name = N'CODICEFARMACIA' AND Object_ID = Object_ID(N'Medicinali'))
BEGIN
ALTER TABLE Medicinali ADD [CODICEFARMACIA] int;
END

IF NOT EXISTS(SELECT 1 FROM sys.columns WHERE Name = N'IMPORTO' AND Object_ID = Object_ID(N'Medicinali'))
BEGIN
ALTER TABLE Medicinali ADD [IMPORTO] float;
END

IF NOT EXISTS(SELECT 1 FROM sys.columns WHERE Name = N'CODICEMEDICINALE' AND Object_ID = Object_ID(N'Medicinali'))
BEGIN
ALTER TABLE Medicinali ADD [CODICEMEDICINALE] int;
END

IF NOT EXISTS(SELECT 1 FROM sys.columns WHERE Name = N'DESCRIZIONE' AND Object_ID = Object_ID(N'Medicinali'))
BEGIN
ALTER TABLE Medicinali ADD [DESCRIZIONE] nvarchar (100);
END

IF NOT EXISTS(SELECT 1 FROM sys.columns WHERE Name = N'NumeroRegistrazione' AND Object_ID = Object_ID(N'Medicinali'))
BEGIN
ALTER TABLE Medicinali ADD [NumeroRegistrazione] int;
END

IF NOT EXISTS(SELECT 1 FROM sys.columns WHERE Name = N'Scadenza' AND Object_ID = Object_ID(N'Medicinali'))
BEGIN
ALTER TABLE Medicinali ADD [Scadenza] datetime;
END


IF NOT EXISTS(SELECT 1 FROM sys.columns WHERE Name = N'AnnoCompetenza' AND Object_ID = Object_ID(N'Denaro'))
BEGIN
ALTER TABLE Denaro ADD AnnoCompetenza int;
END


IF NOT EXISTS(SELECT 1 FROM sys.columns WHERE Name = N'MeseCompetenza' AND Object_ID = Object_ID(N'Denaro'))
BEGIN
ALTER TABLE Denaro ADD MeseCompetenza int;
END
