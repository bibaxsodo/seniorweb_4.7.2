
/****** STORED PROCEDURE ******/

IF OBJECT_ID('Operatore_Update') IS NOT NULL DROP PROCEDURE  [Operatore_Update]
IF OBJECT_ID('Operatore_List') IS NOT NULL DROP PROCEDURE  [Operatore_List]

IF OBJECT_ID('Operatore_Get') IS NOT NULL DROP PROCEDURE  Operatore_Get


IF OBJECT_ID('Operatore_FindByName') IS NOT NULL DROP PROCEDURE  Operatore_FindByName


IF OBJECT_ID('Operatore_Delete') IS NOT NULL DROP PROCEDURE  Operatore_Delete


IF OBJECT_ID('Operatore_Add') IS NOT NULL DROP PROCEDURE  Operatore_Add

IF OBJECT_ID('Cliente_Update') IS NOT NULL DROP PROCEDURE  Cliente_Update


IF OBJECT_ID('Cliente_Update') IS NOT NULL DROP PROCEDURE  Cliente_Update


IF OBJECT_ID('Cliente_List') IS NOT NULL DROP PROCEDURE  Cliente_List


IF OBJECT_ID('Cliente_Get') IS NOT NULL DROP PROCEDURE  Cliente_Get



IF OBJECT_ID('Cliente_Delete') IS NOT NULL DROP PROCEDURE  Cliente_Delete


IF OBJECT_ID('Cliente_Add') IS NOT NULL DROP PROCEDURE  Cliente_Add


exec('CREATE PROCEDURE [dbo].[Cliente_Add]
(
    @ID                      [Int]
   ,@CODICEDEBITORECREDITORE [Int]
   ,@RESIDENZAINDIRIZZO1     [NVarchar](250)
   ,@RESIDENZACOMUNE1        [NVarchar](3)
   ,@RESIDENZAPROVINCIA1     [NVarchar](3)
   ,@RESIDENZACAP1           [NVarchar](5)
   ,@RESIDENZATELEFONO1      [NVarchar](250)
   ,@RESIDENZATELEFONO2      [NVarchar](250)
   ,@RESIDENZATELEFONO3      [NVarchar](250)
   ,@RESIDENZATELEFONO4      [NVarchar](250)
   ,@CODICEFISCALE           [NVarchar](16)
   ,@MastroCliente           [SmallInt]
   ,@ContoCliente            [SmallInt]
   ,@SottoContoCliente       [Float]
   ,@Nome              [NVarchar](200)
   ,@NomeOspite              [NVarchar](50)
   ,@CognomeOspite           [NVarchar](50)
   ,@Sesso                   [NVarchar](1)
   ,@CodiceSanitario         [NVarchar](50)
   ,@CodiceTicket            [NVarchar](10)
   ,@DataNascita             [DateTime]
   ,@DescrizioneCliente      [NVarchar](255)
   ,@EsenzioneTicket         [NVarchar](2)
   ,@IdEpersonam             [Int]
   ,@PEC                     [VarChar](250)
)
AS
    SET NOCOUNT ON
    SET XACT_ABORT ON
    BEGIN TRANSACTION
    INSERT INTO dbo.AnagraficaComune
    (
       TIPOLOGIA
       ,CODICEDEBITORECREDITORE       
       ,RESIDENZAINDIRIZZO1
       ,RESIDENZACOMUNE1
       ,RESIDENZAPROVINCIA1
       ,RESIDENZACAP1
       ,RESIDENZATELEFONO1
       ,RESIDENZATELEFONO2
       ,RESIDENZATELEFONO3
       ,RESIDENZATELEFONO4
       ,CODICEFISCALE
       ,MastroCliente
       ,ContoCliente
       ,SottoContoCliente
	   ,Nome
       ,NomeOspite
       ,CognomeOspite
       ,Sesso
       ,CodiceSanitario
       ,CodiceTicket
       ,DataNascita
       ,DescrizioneCliente
       ,EsenzioneTicket
       ,IdEpersonam
       ,PEC
    )
    VALUES
    (
        ''D''
       ,
    (
        SELECT   
            MAX(CODICEDEBITORECREDITORE) + 1
        FROM   
            AnagraficaComune
        WHERE TIPOLOGIA = ''D''
    )
       ,@RESIDENZAINDIRIZZO1
       ,@RESIDENZACOMUNE1
       ,@RESIDENZAPROVINCIA1
       ,@RESIDENZACAP1
       ,@RESIDENZATELEFONO1
       ,@RESIDENZATELEFONO2
       ,@RESIDENZATELEFONO3
       ,@RESIDENZATELEFONO4
       ,@CODICEFISCALE
       ,@MastroCliente
       ,@ContoCliente
       ,@SottoContoCliente
	   ,@Nome
       ,@NomeOspite
       ,@CognomeOspite
       ,@Sesso
       ,@CodiceSanitario
       ,@CodiceTicket
       ,@DataNascita
       ,@DescrizioneCliente
       ,@EsenzioneTicket
       ,@IdEpersonam
       ,@PEC
    )
    SELECT  
        ID
       ,CODICEDEBITORECREDITORE
    FROM  
        dbo.AnagraficaComune
    WHERE(ID = SCOPE_IDENTITY())
    COMMIT
')


exec('
CREATE PROCEDURE [dbo].[Cliente_Delete] 
    @ID [Int]
AS
    SET NOCOUNT ON
    SET XACT_ABORT ON
    BEGIN TRANSACTION
    DELETE FROM dbo.AnagraficaComune
    WHERE        
        (ID = @ID)
    COMMIT
')

exec('
CREATE PROCEDURE [dbo].[Cliente_Get] 
    @ID [Int]
AS
    SELECT  
        ID
       ,CODICEDEBITORECREDITORE
       ,RESIDENZAINDIRIZZO1
       ,RESIDENZACOMUNE1
       ,RESIDENZAPROVINCIA1
       ,RESIDENZACAP1
       ,RESIDENZATELEFONO1
       ,RESIDENZATELEFONO2
       ,RESIDENZATELEFONO3
       ,RESIDENZATELEFONO4
       ,CODICEFISCALE
       ,MastroCliente
       ,ContoCliente
       ,SottoContoCliente
	   ,Nome
       ,NomeOspite
       ,CognomeOspite
       ,Sesso
       ,CodiceSanitario
       ,CodiceTicket
       ,DataNascita
       ,DescrizioneCliente
       ,EsenzioneTicket
       ,IdEpersonam
       ,PEC
    FROM  
        dbo.AnagraficaComune
    WHERE(ID = @ID)
')

exec('CREATE PROCEDURE [dbo].[Cliente_List]
AS
    SELECT   
        ID
       ,CODICEDEBITORECREDITORE
       ,RESIDENZAINDIRIZZO1
       ,RESIDENZACOMUNE1
       ,RESIDENZAPROVINCIA1
       ,RESIDENZACAP1
       ,RESIDENZATELEFONO1
       ,RESIDENZATELEFONO2
       ,RESIDENZATELEFONO3
       ,RESIDENZATELEFONO4
       ,CODICEFISCALE
       ,MastroCliente
       ,ContoCliente
       ,SottoContoCliente
       ,Nome
       ,NomeOspite
       ,CognomeOspite
       ,Sesso
       ,CodiceSanitario
       ,CodiceTicket
       ,DataNascita
       ,DescrizioneCliente
       ,EsenzioneTicket
       ,IdEpersonam
       ,PEC
    FROM   
        dbo.AnagraficaComune
    WHERE TIPOLOGIA = ''D''
          AND MastroCliente <> 0
')

exec('
CREATE PROCEDURE [dbo].[Cliente_Update]
(
    @ID                      [Int]
   ,@CODICEDEBITORECREDITORE [Int]
   ,@RECAPITOCOMUNE          [NVarchar](3)
   ,@RECAPITOPROVINCIA       [NVarchar](3)
   ,@RECAPITOINDIRIZZO       [NVarchar](150)
   ,@RECAPITONOME            [NVarchar](250)
   ,@RESIDENZAINDIRIZZO1     [NVarchar](250)
   ,@RESIDENZACOMUNE1        [NVarchar](3)
   ,@RESIDENZAPROVINCIA1     [NVarchar](3)
   ,@RESIDENZACAP1           [NVarchar](5)
   ,@RESIDENZATELEFONO1      [NVarchar](250)
   ,@RESIDENZATELEFONO2      [NVarchar](250)
   ,@RESIDENZATELEFONO3      [NVarchar](250)
   ,@RESIDENZATELEFONO4      [NVarchar](250)
   ,@CODICEFISCALE           [NVarchar](16)
   ,@MastroCliente           [SmallInt]
   ,@ContoCliente            [SmallInt]
   ,@SottoContoCliente       [Float]
   ,@Nome              [NVarchar](200)
   ,@NomeOspite              [NVarchar](50)
   ,@CognomeOspite           [NVarchar](50)
   ,@Sesso                   [NVarchar](1)
   ,@CodiceSanitario         [NVarchar](50)
   ,@CodiceTicket            [NVarchar](10)
   ,@DataNascita             [DateTime]
   ,@DescrizioneCliente      [NVarchar](255)
   ,@EsenzioneTicket         [NVarchar](2)
   ,@IdEpersonam             [Int]
   ,@PEC                     [VarChar](250)
)
AS
    SET NOCOUNT ON
    SET XACT_ABORT ON
    BEGIN TRANSACTION
    UPDATE  dbo.AnagraficaComune
        SET 
            RECAPITOCOMUNE = @RECAPITOCOMUNE,
            RECAPITOPROVINCIA = @RECAPITOPROVINCIA,
            RECAPITOINDIRIZZO = @RECAPITOINDIRIZZO,
            RECAPITONOME = @RECAPITONOME,
            RESIDENZAINDIRIZZO1 = @RESIDENZAINDIRIZZO1,
            RESIDENZACOMUNE1 = @RESIDENZACOMUNE1,
            RESIDENZAPROVINCIA1 = @RESIDENZAPROVINCIA1,
            RESIDENZACAP1 = @RESIDENZACAP1,
            RESIDENZATELEFONO1 = @RESIDENZATELEFONO1,
            RESIDENZATELEFONO2 = @RESIDENZATELEFONO2,
            RESIDENZATELEFONO3 = @RESIDENZATELEFONO3,
            RESIDENZATELEFONO4 = @RESIDENZATELEFONO4,
            CODICEFISCALE = @CODICEFISCALE,
            MastroCliente = @MastroCliente,
            ContoCliente = @ContoCliente,
            SottoContoCliente = @SottoContoCliente,
			Nome = @Nome,
            NomeOspite = @NomeOspite,
            CognomeOspite = @CognomeOspite,
            Sesso = @Sesso,
            CodiceSanitario = @CodiceSanitario,
            CodiceTicket = @CodiceTicket,
            DataNascita = @DataNascita,
            DescrizioneCliente = @DescrizioneCliente,
            EsenzioneTicket = @EsenzioneTicket,
            IdEpersonam = @IdEpersonam,
            PEC = @PEC
    WHERE    
        (ID = @ID)
    COMMIT
')

exec('
CREATE PROCEDURE [dbo].[Operatore_Add]
(
    @ID                      [Int]
   ,@CODICEDEBITORECREDITORE [Int]
   ,@RESIDENZAINDIRIZZO1     [NVarchar](250)
   ,@RESIDENZACOMUNE1        [NVarchar](3)
   ,@RESIDENZAPROVINCIA1     [NVarchar](3)
   ,@RESIDENZACAP1           [NVarchar](5)
   ,@RESIDENZATELEFONO1      [NVarchar](250)
   ,@RESIDENZATELEFONO2      [NVarchar](250)
   ,@RESIDENZATELEFONO3      [NVarchar](250)
   ,@RESIDENZATELEFONO4      [NVarchar](250)
   ,@CODICEFISCALE           [NVarchar](16)
   ,@PARTITAIVA              [Float]
   ,@MastroFornitore         [SmallInt]
   ,@ContoFornitore          [SmallInt]
   ,@SottoContoFornitore     [Float]
   ,@Nome                    [NVarchar](200)
   ,@NomeOspite              [NVarchar](50)
   ,@CognomeOspite           [NVarchar](50)
   ,@DescrizioneFornitore    [NVarchar](255)
   ,@IdEpersonam             [Int]
   ,@PEC                     [VarChar](250)
)
AS
    SET NOCOUNT ON
    SET XACT_ABORT ON
    BEGIN TRANSACTION
    INSERT INTO dbo.AnagraficaComune
    (
        CODICEDEBITORECREDITORE
       ,RESIDENZAINDIRIZZO1
       ,RESIDENZACOMUNE1
       ,RESIDENZAPROVINCIA1
       ,RESIDENZACAP1
       ,RESIDENZATELEFONO1
       ,RESIDENZATELEFONO2
       ,RESIDENZATELEFONO3
       ,RESIDENZATELEFONO4
       ,CODICEFISCALE
       ,PARTITAIVA
       ,MastroFornitore
       ,ContoFornitore
       ,SottoContoFornitore
       ,Nome
       ,NomeOspite
       ,CognomeOspite
       ,DescrizioneFornitore
       ,IdEpersonam
       ,PEC
    )
    VALUES
    (
    (
        SELECT   
            MAX(CODICEDEBITORECREDITORE)
        FROM   
            AnagraficaComune
        WHERE TIPOLOGIA = ''D''
    )
   ,@RESIDENZAINDIRIZZO1
   ,@RESIDENZACOMUNE1
   ,@RESIDENZAPROVINCIA1
   ,@RESIDENZACAP1
   ,@RESIDENZATELEFONO1
   ,@RESIDENZATELEFONO2
   ,@RESIDENZATELEFONO3
   ,@RESIDENZATELEFONO4
   ,@CODICEFISCALE
   ,@PARTITAIVA
   ,@MastroFornitore
   ,@ContoFornitore
   ,@SottoContoFornitore
   ,@Nome
   ,@NomeOspite
   ,@CognomeOspite
   ,@DescrizioneFornitore
   ,@IdEpersonam
   ,@PEC
    )
    SELECT  
        ID
       ,CODICEDEBITORECREDITORE
    FROM  
        dbo.AnagraficaComune
    WHERE(ID = SCOPE_IDENTITY())
    COMMIT
')

exec('
CREATE PROCEDURE [dbo].[Operatore_Delete] 
    @ID [Int]
AS
    SET NOCOUNT ON
    SET XACT_ABORT ON
    BEGIN TRANSACTION
    DELETE FROM dbo.AnagraficaComune
    WHERE        
        (ID = @ID)
    COMMIT
')

exec('CREATE PROCEDURE [dbo].[Operatore_FindByName] 
    @NameToFind nvarchar(50)
AS
    SELECT   
        CODICEDEBITORECREDITORE
       ,RESIDENZAINDIRIZZO1
       ,RESIDENZACOMUNE1
       ,RESIDENZAPROVINCIA1
       ,RESIDENZACAP1
       ,RESIDENZATELEFONO1
       ,RESIDENZATELEFONO2
       ,RESIDENZATELEFONO3
       ,RESIDENZATELEFONO4
       ,CODICEFISCALE
       ,PARTITAIVA
       ,MastroFornitore
       ,ContoFornitore
       ,SottoContoFornitore
       ,Nome
       ,NomeOspite
       ,CognomeOspite
       ,DescrizioneFornitore
       ,IdEpersonam
       ,PEC
    FROM   
        dbo.AnagraficaComune
    WHERE TIPOLOGIA = ''D''
          AND Nome like ''%'' + @NameToFind + ''%''
')

exec('CREATE PROCEDURE [dbo].[Operatore_Get] 
    @ID [Int]
AS
    SELECT  
        CODICEDEBITORECREDITORE
       ,RESIDENZAINDIRIZZO1
       ,RESIDENZACOMUNE1
       ,RESIDENZAPROVINCIA1
       ,RESIDENZACAP1
       ,RESIDENZATELEFONO1
       ,RESIDENZATELEFONO2
       ,RESIDENZATELEFONO3
       ,RESIDENZATELEFONO4
       ,CODICEFISCALE
       ,PARTITAIVA
       ,MastroFornitore
       ,ContoFornitore
       ,SottoContoFornitore
       ,Nome
	   ,NomeOspite
	   ,CognomeOspite
       ,DescrizioneFornitore
       ,IdEpersonam
       ,PEC
    FROM  
        dbo.AnagraficaComune
    WHERE(ID = @ID)
')

exec('CREATE PROCEDURE [dbo].[Operatore_List] 
    
AS
    SELECT   
        CODICEDEBITORECREDITORE
       ,RESIDENZAINDIRIZZO1
       ,RESIDENZACOMUNE1
       ,RESIDENZAPROVINCIA1
       ,RESIDENZACAP1
       ,RESIDENZATELEFONO1
       ,RESIDENZATELEFONO2
       ,RESIDENZATELEFONO3
       ,RESIDENZATELEFONO4
       ,CODICEFISCALE
       ,PARTITAIVA
       ,MastroFornitore
       ,ContoFornitore
       ,SottoContoFornitore
       ,Nome
       ,NomeOspite
       ,CognomeOspite
	   ,DataNascita
       ,DescrizioneFornitore
       ,IdEpersonam
       ,PEC
    FROM   
        dbo.AnagraficaComune
    WHERE TIPOLOGIA = ''D''
          AND MastroFornitore <> 0
')

exec('CREATE PROCEDURE [dbo].[Operatore_Update]
(
    @ID                      [Int]
   ,@CODICEDEBITORECREDITORE [Int]
   ,@RESIDENZAINDIRIZZO1     [NVarchar](250)
   ,@RESIDENZACOMUNE1        [NVarchar](3)
   ,@RESIDENZAPROVINCIA1     [NVarchar](3)
   ,@RESIDENZACAP1           [NVarchar](5)
   ,@RESIDENZATELEFONO1      [NVarchar](250)
   ,@RESIDENZATELEFONO2      [NVarchar](250)
   ,@RESIDENZATELEFONO3      [NVarchar](250)
   ,@RESIDENZATELEFONO4      [NVarchar](250)
   ,@CODICEFISCALE           [NVarchar](16)
   ,@PARTITAIVA              [Float]
   ,@MastroFornitore         [SmallInt]
   ,@ContoFornitore          [SmallInt]
   ,@SottoContoFornitore     [Float]
   ,@Nome                    [NVarchar](200)
   ,@NomeOspite              [NVarchar](50)
   ,@CognomeOspite           [NVarchar](50)
   ,@DescrizioneFornitore    [NVarchar](255)
   ,@IdEpersonam             [Int]
   ,@PEC                     [VarChar](250)
)
AS
    SET NOCOUNT ON
    SET XACT_ABORT ON
    BEGIN TRANSACTION
    UPDATE  dbo.AnagraficaComune
        SET 
            RESIDENZAINDIRIZZO1 = @RESIDENZAINDIRIZZO1,
            RESIDENZACOMUNE1 = @RESIDENZACOMUNE1,
            RESIDENZAPROVINCIA1 = @RESIDENZAPROVINCIA1,
            RESIDENZACAP1 = @RESIDENZACAP1,
            RESIDENZATELEFONO1 = @RESIDENZATELEFONO1,
            RESIDENZATELEFONO2 = @RESIDENZATELEFONO2,
            RESIDENZATELEFONO3 = @RESIDENZATELEFONO3,
            RESIDENZATELEFONO4 = @RESIDENZATELEFONO4,
            CODICEFISCALE = @CODICEFISCALE,
            PARTITAIVA = @PARTITAIVA,
            MastroFornitore = @MastroFornitore,
            ContoFornitore = @ContoFornitore,
            SottoContoFornitore = @SottoContoFornitore,
            Nome = @Nome,
            NomeOspite = @NomeOspite,
            CognomeOspite = @CognomeOspite,
            DescrizioneFornitore = @DescrizioneFornitore,
            IdEpersonam = @IdEpersonam,
            PEC = @PEC
    WHERE    
        (ID = @ID)
    COMMIT
')


/****** FINE --- STORED PROCEDURE ******/