﻿<%@ Page Language="VB" AutoEventWireup="false" Inherits="AbilitaProcedure" CodeFile="AbilitaProcedure.aspx.vb" %>

<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
    <title>Menu Senior</title>
    <asp:PlaceHolder runat="server">
        <%: System.Web.Optimization.Styles.Render("~/Content/AjaxControlToolkit/Styles/Bundle") %>
    </asp:PlaceHolder>
    <link rel="stylesheet" href="ospiti.css?ver=10" type="text/css" />
    <link href="js/jquery.autocomplete.css" rel="stylesheet" type="text/css" />

    <script src="js/jquery-1.5.1.min.js" type="text/javascript"></script>
    <script src="js/jquery.autocomplete.js" type="text/javascript"></script>
    <script src="js/jquery.maskedinput-1.3.min.js" type="text/javascript"></script>
    <script src="js/NoEnter.js" type="text/javascript"></script>

    <script src="Ospitiweb/js/chosen/chosen.jquery.js" type="text/javascript"></script>
    <script src="Ospitiweb/js/chosen/docsupport/prism.js" type="text/javascript" charset="utf-8"></script>
    <link rel="stylesheet" href="Ospitiweb/js/chosen/docsupport/style.css">
    <link rel="stylesheet" href="Ospitiweb/js/chosen/docsupport/prism.css">
    <link rel="stylesheet" href="Ospitiweb/js/chosen/chosen.css">
    <script type="text/javascript">
        function removeElement(divNum) {
            var d = document.getElementById(divNum).parentNode;
            var d2 = document.getElementById(divNum);
            d.removeChild(d2);
        }
    </script>
    <script type="text/javascript">
        $(document).ready(function () {

            $(".chosen-select").chosen({
                no_results_text: "Non trovata",
                display_disabled_options: true,
                allow_single_deselect: true,
                width: "350px",
                placeholder_text_single: "Seleziona Società"
            });
            if (window.innerHeight > 0) { $("#BarraLaterale").css("height", (window.innerHeight - 105) + "px"); } else { $("#BarraLaterale").css("height", (document.documentElement.offsetHeight - 105) + "px"); }
        });
    </script>
</head>
<body>
    <form id="form1" runat="server">
        <asp:ScriptManager ID="ScriptManager1" runat="server" EnableScriptGlobalization="true">
            <Scripts>
                <asp:ScriptReference Path="~/Scripts/AjaxControlToolkit/Bundle" />
            </Scripts>
        </asp:ScriptManager>
        <div>
            <asp:Label ID="Lbl_BarraSenior" runat="server" Text=""></asp:Label>

            <table width="100%" cellpadding="0" cellspacing="0">
                <tr>
                    <td style="width: 160px; background-color: #F0F0F0;"></td>
                    <td>
                        <div class="Titolo">Menu Senior</div>
                        <div class="SottoTitolo">
                            <br />
                            <br />
                        </div>
                    </td>
                    <td style="text-align: right; vertical-align: top;">
                        <div class="DivTasti">
                            <asp:ImageButton ID="Btn_Modifica" runat="server" Height="38px" ImageUrl="~/images/salva.jpg" CssClass="EffettoBottoniTondi" Width="38px" ToolTip="Modifica" />
                            <asp:ImageButton ID="IB_ProssimaPagina" runat="server" ImageUrl="~/images/avanti.jpg" />
                        </div>
                    </td>
                </tr>
                <tr>

                    <td style="width: 160px; background-color: #F0F0F0; text-align: center; vertical-align: top;" id="BarraLaterale">
                        <asp:ImageButton ID="Btn_Home" runat="server" BackColor="Transparent" ImageUrl="images/Menu_Indietro.png" ToolTip="Home" /><br />

                    </td>
                    <td colspan="2" style="vertical-align: top;">


                        <br />
                        <br />
                        <label class="LabelCampo">Ospiti :</label>
                        <asp:CheckBox ID="Chk_Ospiti" runat="server" Text="" />

                        <br />
                        <br />
                        <label class="LabelCampo">Generale :</label>
                        <asp:CheckBox ID="Chk_Generale" runat="server" Text="" />

                        <br />
                        <br />
                        <label class="LabelCampo">Protocollo :</label>
                        <asp:CheckBox ID="Chk_Protocollo" runat="server" Text="" />

                        <br />
                        <br />
                        <label class="LabelCampo">Mailing List :</label>
                        <asp:CheckBox ID="Chk_MailingList" runat="server" Text="" />

                        <br />
                        <br />
                        <label class="LabelCampo">Appalti :</label>
                        <asp:CheckBox ID="Chk_Appalti" runat="server" Text="" />

                        <br />
                        <br />
                        <label class="LabelCampo">Comunicazione :</label>
                        <asp:CheckBox ID="Chk_Comunicazioni" runat="server" Text="" />
                        <br />
                        <br />
                        <label class="LabelCampo">Allegati XML :</label>
                        <asp:CheckBox ID="Chk_AllegatiXML" runat="server" Text="" />
                        <br />
                        <br />
                        <label class="LabelCampo">Ambulatoriale :</label>
                        <asp:CheckBox ID="Chk_Ambulatoriale" runat="server" Text="" />
                        <br />
                        <br />
                        <label class="LabelCampo">Ricoveri :</label>
                        <asp:CheckBox ID="Chk_Ricoveri" runat="server" Text="" />
                        <br />
                        <br />


                        <br />
                        <asp:Button ID="Btn_Seleziona" runat="server" Text="Seleziona Tutto" />
                        <br />
                        <asp:GridView ID="GridView1" runat="server" CellPadding="3"
                            Width="100%" BackColor="White" BorderColor="#999999" BorderStyle="None"
                            BorderWidth="1px" GridLines="Vertical">
                            <RowStyle ForeColor="#565151" BackColor="#EEEEEE" />
                            <AlternatingRowStyle BackColor="Gainsboro" />

                            <Columns>



                                <asp:TemplateField HeaderText="Importa">
                                    <ItemTemplate>
                                        <asp:CheckBox ID="ChkImporta" runat="server" Text="" />
                                    </ItemTemplate>
                                    <HeaderStyle Font-Bold="False" Font-Italic="False" />
                                    <ItemStyle Width="30px" />
                                </asp:TemplateField>
                                <asp:BoundField DataField="RagioneSociale" HeaderText="Ragione Sociale" />
                                <asp:BoundField DataField="ProcedureAbilitate" HeaderText="Procedure Abilitate" />

                            </Columns>

                            <FooterStyle BackColor="#CCCCCC" ForeColor="Black" />
                            <PagerStyle BackColor="#999999" ForeColor="Black" HorizontalAlign="Center" />
                            <SelectedRowStyle BackColor="#008A8C" Font-Bold="True" ForeColor="White" />
                            <HeaderStyle BackColor="#565151" Font-Bold="False" Font-Size="Small"
                                ForeColor="White" />
                        </asp:GridView>


                    </td>
                </tr>

                <tr>
                    <td style="width: 160px; background-color: #F0F0F0;">&nbsp;<br />
                        &nbsp;<br />
                        &nbsp;<br />
                        &nbsp;<br />
                        &nbsp;<br />
                        &nbsp;<br />
                        &nbsp;<br />
                        &nbsp;<br />
                        &nbsp;<br />
                    </td>
                    <td></td>
                    <td></td>
                </tr>
            </table>


        </div>
    </form>
</body>
</html>
