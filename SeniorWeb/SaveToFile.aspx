<%@ Page Language="vb" %>

<%
   

    Dim cnSenior As System.Data.OleDb.OleDbConnection
    Dim Percorso As String = ""
    
    cnSenior = New System.Data.OleDb.OleDbConnection(Application("SENIOR"))


    cnSenior.Open()
    Dim cmd As New System.Data.OleDb.OleDbCommand()
    cmd.CommandText = "SELECT * From Token where tOKEN = ? And DataScadenza >= ?"
    cmd.Parameters.AddWithValue("@Token", HttpContext.Current.Request.QueryString("DEST"))
    cmd.Parameters.AddWithValue("@DataScadenza", Now)
    cmd.Connection = cnSenior

    Dim Read As System.Data.OleDb.OleDbDataReader = cmd.ExecuteReader()

    If Read.Read() Then
        If IsDBNull(Read.Item("Value1")) Then
            Percorso = ""
        Else
            Percorso = Read.Item("Value1")
        End If        
    End If
    Read.Close()
   
    cnSenior.Close()
    
    If Percorso <> "" Then
    
   
        Dim files As HttpFileCollection
    
        files = HttpContext.Current.Request.Files
    
        Dim Index As Integer
    
        For Index = 0 To files.Count - 1
            Dim uploadfile As HttpPostedFile
        
            uploadfile = files(Index)
        
            Try
                'uploadfile.SaveAs(System.Web.Hosting.HostingEnvironment.ApplicationPhysicalPath() & "\Public\" & Percorso.Replace("1.jpg","2.jpg"))
                
                
                        
            Catch ex As Exception
    
            Finally
                    
                Const Lx = 640
                Const Ly = 480

                Dim newWidth, newHeight As Integer
                Dim originalimg As System.Drawing.Image
                Dim msg As String
                Dim upload_ok As Boolean
                Dim L2 As Integer

                Try
                        
                     originalimg = System.Drawing.Image.FromStream(uploadfile.InputStream)
                    'originalimg = System.Drawing.Image.FromFile(System.Web.Hosting.HostingEnvironment.ApplicationPhysicalPath() & "\Public\" & Percorso.Replace("1.jpg","2.jpg"))

                    If (originalimg.Width / Lx) > (originalimg.Width / Ly) Then
                        L2 = originalimg.Width
                        newWidth = Lx
                        newHeight = originalimg.Height * (Lx / L2)
                        If newHeight > Ly Then
                            newWidth = newWidth * (Ly / newHeight)
                            newHeight = Ly
                        End If
                    Else
                        L2 = originalimg.Height
                        newHeight = Ly
                        newWidth = originalimg.Width * (Ly / L2)
                        If newWidth > Lx Then
                            newHeight = newHeight * (Lx / newWidth)
                            newWidth = Lx
                        End If
                    End If
                    
                    
                    Dim thumb As New System.Drawing.Bitmap(newWidth, newHeight)
                    Dim gr_dest As System.Drawing.Graphics = System.Drawing.Graphics.FromImage(thumb)
                    Dim sb = New System.Drawing.SolidBrush(System.Drawing.Color.White)
                    gr_dest.FillRectangle(sb, 0, 0, thumb.Width, thumb.Height)
                    gr_dest.DrawImage(originalimg, 0, 0, thumb.Width, thumb.Height)
                    Try
                        rem fileExt = System.IO.Path.GetExtension(sOrgFileName).ToLower()
                        thumb.Save(System.Web.Hosting.HostingEnvironment.ApplicationPhysicalPath() & "\Public\" & Percorso) 

                        
                        upload_ok = True
                    Catch ex As Exception
                        msg = "Sorry, there was a problem saving the image."
                    End Try
                    If Not thumb Is Nothing Then
                        thumb.Dispose()
                        thumb = Nothing
                    End If

                Catch ex As Exception

                End Try
                                    
        End Try
                 
        Next
    
        HttpContext.Current.Response.Write("Foto Caricata su Senior!")
    Else
        HttpContext.Current.Response.Write("Token Scaduto")
    End If
    
%>
