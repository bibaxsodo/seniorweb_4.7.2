﻿<%@ Page Language="VB" AutoEventWireup="false" Inherits="ModificaPassword" CodeFile="ModificaPassword.aspx.vb" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">

<head runat="server">
    <meta content="text/html; charset=utf-8" http-equiv="Content-Type" />
    <title>Modifica Password</title>
    <asp:PlaceHolder runat="server">
        <%: System.Web.Optimization.Styles.Render("~/Content/AjaxControlToolkit/Styles/Bundle") %>
    </asp:PlaceHolder>
    <link href="ospiti.css?ver=11" rel="stylesheet" type="text/css" />
    <link rel="shortcut icon" href="images/SENIOR.ico" />
    <link rel="shortcut icon" href="images/SENIOR.ico" />
    <script src="js/JSErrore.js" type="text/javascript"></script>
    <link href="ospiti.css?ver=11" rel="stylesheet" type="text/css" />
    <script src="/js/formatnumer.js" type="text/javascript"></script>
    <script src="js/JSErrore.js" type="text/javascript"></script>
    <script src="js/jquery-1.7.1.min.js" type="text/javascript"></script>
    <script src="js/bjqs-1.3.min.js" type="text/javascript"></script>
    <link type="text/css" rel="Stylesheet" href="css/bjqs.css" />

    <link rel="stylesheet" media="all" href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,500" />

    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.1/css/bootstrap.min.css"
        integrity="sha384-WskhaSGFgHYWDcbwN70/dfYBj47jz9qbsMId/iRN3ewGhXQFZCSftd1LZCfmhktB" crossorigin="anonymous">

    <meta content='width=device-width, initial-scale=1.0' name='viewport'>

    <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.3/umd/popper.min.js" integrity="sha384-ZMP7rVo3mIykV+2+9J3UJ46jBk0WLaUAdn689aCwoqbBJiSnjAK/l8WvCWPIPm49" crossorigin="anonymous"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.1.1/js/bootstrap.min.js" integrity="sha384-smHYKdLADwkXOn1EmN1qk/HfnUcbVRZyYmZ4qpPea6sjB/pTJ0euyQp0Mk8ck+5T" crossorigin="anonymous"></script>




    <style>
        .footer {
            position: absolute;
            bottom: 0;
            width: 98%;
            height: 70px; /* Set the fixed height of the footer here */
            line-height: 60px; /* Vertically center the text there */
            background-color: White;
        }

        .jumbotron {
            background-color: #007DC4;
            height: 70px;
            padding-top: 20px;
        }


        .inputbox {
            box-sizing: border-box;
            background: #F8F8F8;
            box-shadow: inset 0 1px 3px 0 rgba(0,0,0,0.3);
            border-radius: 5px !important;
            height: 46px;
            font-size: 18px;
            width: 100%;
            border: none;
            padding: 0 0 0 10px;
        }

        input[type="text"] {
            height: 46px;
        }

        body {
            font-family: 'Source Sans Pro', sans-serif;
            font-size: 16px;
        }

        h2 {
            left: 40px;
            color: #666;
            font-family: 'Source Sans Pro', sans-serif;
            font-weight: 300;
            margin-bottom: 20px;
        }

        h1 {
            font-family: 'Source Sans Pro', sans-serif;
            font-weight: 300;
            font-size: 26px;
            color: #282828;
            letter-spacing: 0;
            line-height: 13px;
            font-weight: 300;
            margin-bottom: 34px;
        }

        @media only screen and (min-device-width : 1024px) {
            #divsfondo {
                top: 20px;
                background-image: url('images/sfondoprova.jpg');
                background-size: 100% 100%;
            }
        }
    </style>
    <script>
        jQuery(document).ready(function ($) {
            $('#my-slideshow').bjqs({
                'height': 372,
                'width': 520,
                'responsive': true,
                randomstart: true,
                showcontrols: false,
                animspeed: 8000,
                showmarkers: false
            });
        });
    </script>
</head>

<body>
    <form id="form2" runat="server">
        <asp:ScriptManager ID="ScriptManager1" runat="server" EnableScriptGlobalization="true">
            <Scripts>
                <asp:ScriptReference Path="~/Scripts/AjaxControlToolkit/Bundle" />
            </Scripts>
        </asp:ScriptManager>
        <div class="jumbotron">
            <div class="well">
                <div class="container">
                    <div class="row">
                        <div class="col-lg-6">
                            <img src="images/melograno.png" />
                            <img src="images/senior.png" style="margin-right: 15px;" />
                        </div>
                        <div class="col-lg-6" style="top: 10px; text-align: right;">
                            <a href="https://advenias.it/" target="_blank" style="color: White; font-style: normal;">Scopri gli altri servizi di Advenias</a>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <div class="container">
            <div class="well">
                <div class="row">
                </div>
                <br />
                <br />
            </div>
        </div>
        <div class="container">
            <div class="well">
                <div class="row">
                    <div class="col-lg-8" id="divsfondo">
                        <div class="hero-cont" style="max-width: 80%; padding-top: 10%;">
                        </div>
                    </div>

                    <div class="col-lg-4" style="top: 10px; text-align: left; padding-top: 10px;">
                        <h1>Modifica Password</h1>
                        <br />
                        <b>Password scaduta, inserire una nuova Password</b>
                        <br />
                        <br />
                        <label style="font-size: 14px;">Password</label><br />
                        <div class="inpt-control">
                            <asp:TextBox ID="Txt_Password" required class="inputbox" TabIndex="2" Width="311px" runat="server" TextMode="Password" value="******" ForeColor="#888888" onclick="this.value=''; this.style.color='#000000';" onblur="if (this.value=='') { this.value='******'; this.style.color='#888888'; }"></asp:TextBox><br />
                        </div>
                        <br />
                        <label style="font-size: 14px;">Conferma Password</label><br />
                        <div class="inpt-control">
                            <asp:TextBox ID="Txt_PasswordConferma" required class="inputbox" TabIndex="2" Width="311px" runat="server" TextMode="Password" value="******" ForeColor="#888888" onclick="this.value=''; this.style.color='#000000';" onblur="if (this.value=='') { this.value='******'; this.style.color='#888888'; }"></asp:TextBox><br />
                        </div>
                        <br />
                        <br />
                        <center>		
		<asp:Button ID="Button1" runat="server"  OnClientClick="xxlogin();"  tabindex="3" Text="Modifica Password" style='height: 2.142em;min-width: 6em;font-family: "Segoe UI Semibold", "Segoe UI Web Semibold", "Segoe UI Web Regular", "Segoe UI", "Segoe UI Symbol","HelveticaNeue-Medium", "Helvetica Neue", Arial, sans-serif;font-weight: normal;font-size: 100%;background-color: rgba(182,182,182,0.7);color: #212121;padding: 3px 12px 5px;border: 0px;' />		
		</center>
                        <br />
                        <br />
                        <br />


                        <br />
                    </div>
                </div>
            </div>
        </div>

        <div class="footer">
            <div class="container">
                <div class="row">
                    <div class="col-lg-1" style="text-align: center; padding-top: 0px; color: White;">
                        <a href="https://advenias.it/" target="_blank">
                            <img style="border-radius: 0 !important;" src="https://cdn.e-personam.com/assets/loghi/Advenias_logo-6ee032a37e4bc7001c2af205ff957b4574c873c75689742d2ffdd7f756029e5b.png" alt="Advenias logo" width="117" height="27">
                        </a>
                    </div>
                    <div class="col-lg-8" style="text-align: center; padding-top: 0px; color: White;">
                        <div style="color: #4B4B4B; line-height: 1.2;">
                            Senior è un marchio di Advenias Srl<br>
                            via Lercaro 3 - Casalecchio di Reno (BO) - PI 03210661207   
                        </div>
                    </div>
                    <div class="col-lg-1" style="text-align: right; padding-top: 0px; padding-left: 10px; color: White;">
                        <a href="mailto:info@sodo.it">senior@advenias.it</a>
                    </div>

                </div>
            </div>
        </div>



    </form>
    </div>
</body>
</html>

