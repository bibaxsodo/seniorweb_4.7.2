﻿Imports System.Security.Cryptography.SHA1Managed

Partial Class Login_generale
    Inherits System.Web.UI.Page

    Protected Sub ImageButton1_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles ImageButton1.Click
        Dim k As New Cls_Login

        k.Utente = UCase(Txt_Login.Text)
        k.Chiave = Txt_Password.Text
        k.Leggi(Application("SENIOR"))

        Session("DC_OSPITE") = k.Ospiti
        Session("DC_OSPITIACCESSORI") = k.OspitiAccessori
        Session("DC_TABELLE") = k.TABELLE
        Session("DC_GENERALE") = k.Generale
        Session("STAMPEOSPITI") = k.STAMPEOSPITI
        Session("STAMPEFINANZIARIA") = k.STAMPEFINANZIARIA
        Session("ProgressBar") = ""


        If k.Ospiti <> "" Then
            Session("ABILITAZIONI") = k.ABILITAZIONI
            Session("UTENTE") = Txt_Login.Text

            
            Response.Redirect("GeneraleWeb\Menu_Generale.aspx")
        Else
            ClientScript.RegisterClientScriptBlock(Me.GetType(), "Errore", "VisualizzaErrore('<center><b>Nome utente o password errati</b></center>');", True)
        End If
    End Sub

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Page.IsPostBack = True Then Exit Sub

        If Request.Item("Logout") = "TRUE" Then
            Session.Clear()
            Session.Abandon()
        End If
        Session("UTENTE") = ""
        Session("ODV") = ""
        If Request.Item("USER") <> "" Then
            If SHA1("GABELLINNI" & Request.Item("USER") & "SAURO") = Request.Item("CodiceSicurezza") Then
                Dim k As New Cls_Login

                k.Utente = Request.Item("USER")                
                k.LeggiSP(Application("SENIOR"))

                Session("DC_OSPITE") = k.Ospiti
                Session("DC_OSPITIACCESSORI") = k.OspitiAccessori
                Session("DC_TABELLE") = k.TABELLE
                Session("DC_GENERALE") = k.Generale
                Session("STAMPEOSPITI") = k.STAMPEOSPITI
                Session("STAMPEFINANZIARIA") = k.STAMPEFINANZIARIA
                Session("ProgressBar") = ""

                Session("ABILITAZIONI") = k.ABILITAZIONI
                Session("UTENTE") = Request.Item("USER")
                Session("ODV") = "ODV"

                Session("TIPOAPP") = "ODV"

                Dim k1 As New Cls_TabellaSocieta

                Response.Redirect("GeneraleWeb\Menu_Generale.aspx?" & Session("TIPOAPP"))

            End If
        End If
    End Sub

    Public Function SHA1(ByVal StringaDaConvertire As String) As String
        Dim sha2 As New System.Security.Cryptography.SHA1CryptoServiceProvider()
        Dim hash() As Byte
        Dim bytes() As Byte
        Dim output As String = ""
        Dim i As Integer

        bytes = System.Text.Encoding.UTF8.GetBytes(StringaDaConvertire)
        hash = sha2.ComputeHash(bytes)

        For i = 0 To hash.Length - 1
            output = output & hash(i).ToString("x2")
        Next

        Return output
    End Function
End Class
