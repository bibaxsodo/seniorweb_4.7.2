﻿<%@ Page Language="vb" AutoEventWireup="false" CodeFile="Menu_Ricoveri.aspx.vb" Inherits="Menu_Ricoveri" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
    <title>Menu Ricoveri</title>
    <asp:PlaceHolder runat="server">
        <%: System.Web.Optimization.Styles.Render("~/Content/AjaxControlToolkit/Styles/Bundle") %>
    </asp:PlaceHolder>
    <link rel="stylesheet" href="ospiti.css" type="text/css" />
    <link rel="shortcut icon" href="../images/SENIOR.ico" />
    <script src="js/jquery-1.5.1.min.js" type="text/javascript"></script>

    <script type="text/javascript">
        $(document).ready(function () {
            if (window.innerHeight > 0) { $("#BarraLaterale").css("height", (window.innerHeight - 94) + "px"); } else { $("#BarraLaterale").css("height", (document.documentElement.offsetHeight - 94) + "px"); }
        });
    </script>
    <script src="https://kit.fontawesome.com/2d6fbbbf2d.js" crossorigin="anonymous"></script>
</head>
<body>
    <form id="form1" runat="server">
        <asp:ScriptManager ID="ScriptManager1" runat="server" EnableScriptGlobalization="true">
            <Scripts>
                <asp:ScriptReference Path="~/Scripts/AjaxControlToolkit/Bundle" />
            </Scripts>
        </asp:ScriptManager>
        <div>
            <asp:Label ID="Lbl_BarraSenior" runat="server" Text=""></asp:Label>
            <table style="width: 100%;" cellpadding="0" cellspacing="0">
                <tr>
                    <td style="width: 140px; background-color: #F0F0F0; text-align: center;">
                    <td>
                        <div class="Titolo">Ricoveri</div>
                        <div class="SottoTitolo">
                            <br />
                        </div>
                    </td>

                    <td style="text-align: right; vertical-align: top;">
                        <span class="BenvenutoText"><font size="4">Benvenuto <%=Session("UTENTE")%></font></span>
                    </td>
                </tr>

                <tr>

                    <td style="width: 140px; background-color: #F0F0F0; text-align: center; vertical-align: top;" id="BarraLaterale">
                        <asp:ImageButton ID="ImageButton1" runat="server" BackColor="Transparent" ImageUrl="../images/Menu_Indietro.png" ToolTip="Chiudi" />
                    </td>
                    <td colspan="2" style="vertical-align: top;">
                        <table style="width: 1024px;">
                            <tr>
                                <td style="text-align: center; width: 150px;"><a href="Elenco_Operatore.aspx" id="A1"><i class="fas fa-user-md fa-border" style="padding: 0; font-size: 52px; width: 104px; height: 75px; padding-top: 21px; color: #1960ab; border-color: #bbbbbb; border-radius: 9px; border-width: 3px;"></i></a></td>
                                <td style="text-align: center; width: 150px;"><a href="Elenco_Contratti.aspx" id="A1"><i class="far fa-handshake fa-border" style="padding: 0; font-size: 52px; width: 104px; height: 75px; padding-top: 21px; color: #1960ab; border-color: #bbbbbb; border-radius: 9px; border-width: 3px;"></i></a></td>
                                <td style="text-align: center; width: 150px;"><a href="Elenco_DRG.aspx" id="A1"><i class="fas fa-clinic-medical fa-border" style="padding: 0; font-size: 52px; width: 104px; height: 75px; padding-top: 21px; color: #1960ab; border-color: #bbbbbb; border-radius: 9px; border-width: 3px;"></i></a></td>
                                <td style="text-align: center; width: 150px;" id="ColomProtocollo"><a href="ImportEpersonam.aspx"><img src="../images/Menu_EPersonam.PNG" alt="GESTIONE PROTOCOLLO" id="protocolloimg" class="Effetto" style="border-width: 0;"></a>
                                <td style="text-align: center; width: 150px;"><a href="RendicontoPassivoOperatori.aspx" id="A1"><i class="fas fa-file-invoice fa-border" style="padding: 0; font-size: 52px; width: 104px; height: 75px; padding-top: 21px; color: #1960ab; border-color: #bbbbbb; border-radius: 9px; border-width: 3px;"></i></a></td>
                            </tr>
                            <tr>
                                <td style="text-align: center; vertical-align: top;"><span class="MenuText">OPERATORI</span></td>
                                <td style="text-align: center; vertical-align: top;"><span class="MenuText">CONTRATTI</span></td>
                                <td style="text-align: center; vertical-align: top;"><span class="MenuText">DRG</span></td>
                                <td style="text-align: center; vertical-align: top;"><span class="MenuText">ePersonam</span></td>
                                <td style="text-align: center; vertical-align: top;"><span class="MenuText">RENDICONTI</span></td>
                            </tr>

                            <tr>
                                <td style="text-align: center; vertical-align: top;"><span class="MenuText"></span></td>
                                <td style="text-align: center; vertical-align: top;"><span class="MenuText"></span></td>
                                <td style="text-align: center; vertical-align: top;"><span class="MenuText"></span></td>
                                <td style="text-align: center; vertical-align: top;"><span class="MenuText"></span></td>
                                <td style="text-align: center; vertical-align: top;"><span class="MenuText"></span></td>
                                <td style="text-align: center; vertical-align: top;"></td>
                            </tr>
                            <tr>
                                <td style="text-align: center;">&nbsp;</td>
                                <td style="text-align: center;">&nbsp;</td>
                                <td style="text-align: center;">&nbsp;</td>
                                <td style="text-align: center;">&nbsp;</td>
                                <td style="text-align: center;">&nbsp;</td>
                                <td style="text-align: center;">&nbsp;</td>
                            </tr>
                            <tr>
                                <td style="text-align: center; vertical-align: top;"><span class="MenuText"></span></td>
                                <td style="text-align: center; vertical-align: top;"><span class="MenuText"></span></td>
                                <td style="text-align: center; vertical-align: top;"></td>
                                <td style="text-align: center; vertical-align: top;"></td>
                                <td style="text-align: center; vertical-align: top;"></td>
                                <td style="text-align: center; vertical-align: top;"></td>
                            </tr>


                        </table>
                    </td>
                </tr>

                <tr>
                    <td style="width: 140px; background-color: #F0F0F0;">&nbsp;<br />
                        &nbsp;<br />
                        &nbsp;<br />
                        &nbsp;<br />
                        &nbsp;<br />
                        &nbsp;<br />
                        &nbsp;<br />
                        &nbsp;<br />
                        &nbsp;<br />
                    </td>
                    <td></td>
                    <td></td>
                </tr>
            </table>



        </div>
    </form>
</body>
</html>

