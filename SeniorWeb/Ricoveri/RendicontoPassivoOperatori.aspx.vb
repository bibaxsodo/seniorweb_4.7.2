﻿Imports Microsoft.VisualBasic
Imports System.Data.OleDb
Imports System.Web.Hosting
Imports System.Data.SqlTypes

Public Class RendicontoPassivoOperatori
    Inherits System.Web.UI.Page

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
                If Session("UTENTE") = "" Then
            Response.Redirect("..\Login.aspx")
            Exit Sub
        End If

        If Page.IsPostBack = True Then
            Exit Sub
        End If


        Dim K1 As New Cls_SqlString

        Dim Barra As New Cls_BarraSenior

        If Not IsNothing(Session("RicercaAnagraficaSQLString")) Then
            Try
                K1 = Session("RicercaAnagraficaSQLString")
            Catch ex As Exception
                K1 = Nothing
            End Try
        End If

        Lbl_BarraSenior.Text = Barra.CodiceBarra(Application("SENIOR"), Session("UTENTE"), Page, K1)


        Dim MAppalto As New Cls_Operatore

        MAppalto.UpDateDropBox(Session("DC_OSPITE"), DD_Operatore)

        
        Dim f As New Cls_Parametri

        f.LeggiParametri(Session("DC_OSPITE"))
        Dd_Mese.SelectedValue = f.MeseFatturazione
        Txt_Anno.Text = f.AnnoFatturazione
    End Sub



    Private Sub Btn_Esci_Click(sender As Object, e As ImageClickEventArgs) Handles Btn_Esci.Click
        Response.Redirect("Menu_Ricoveri.aspx")
    End Sub

    Private Sub Img_Stampa_Click(sender As Object, e As ImageClickEventArgs) Handles Img_Stampa.Click
        If Val(Txt_Anno.Text) = 0 Then
            Exit Sub
        End If

        If Val(Dd_Mese.SelectedValue) = 0 Then
            Exit Sub
        End If

        Dim cn As New OleDbConnection

        Dim ConnectionString As String = Session("DC_TABELLE")

        cn = New Data.OleDb.OleDbConnection(ConnectionString)

        cn.Open()

        cn.Close
    End Sub
End Class