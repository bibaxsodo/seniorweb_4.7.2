﻿Imports Senior.Entities
Imports Senior.BIZ

Public Class ElencoAppuntameti
    Inherits System.Web.UI.Page

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
                If Session("UTENTE") = "" Then
            Response.Redirect("..\Login.aspx")
            Exit Sub
        End If

        If Page.IsPostBack = True Then
            Exit Sub
        End If

        Dim K1 As New Cls_SqlString

        Dim Barra As New Cls_BarraSenior

        If Not IsNothing(Session("RicercaAnagraficaSQLString")) Then
            K1 = Session("RicercaAnagraficaSQLString")
        End If

        Lbl_BarraSenior.Text = Barra.CodiceBarra(Application("SENIOR"), Session("UTENTE"), Page, K1)

        

        grd_ElencoAppuntamenti.DataSource = GetAppuntamenti()
        grd_ElencoAppuntamenti.DataBind()        
    End Sub
    
    Private Function GetAppuntamenti() As List(Of DettaglioAppuntamento)
        Dim MyListaAppuntamenti = CType(Session("AppuntamentiFilePDF"), List(Of DettaglioAppuntamento))
        
        Return MyListaAppuntamenti        
    End Function
End Class