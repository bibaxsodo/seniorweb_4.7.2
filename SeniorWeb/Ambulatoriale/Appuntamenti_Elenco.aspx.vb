﻿Imports Microsoft.VisualBasic
Imports System.Data.OleDb
Imports System.Web.Hosting
Imports System.Data.SqlTypes
Imports Senior.Entities
Imports Senior.BIZ

Partial Class Ambulatoriale_Appuntamenti_Elenco
    Inherits System.Web.UI.Page
    
    Private Function LeggiFattura(ByVal Numero As Integer)  As Integer
        Dim cn As New OleDbConnection


        Dim ConnectionString As String = Session("DC_GENERALE")

        cn = New Data.OleDb.OleDbConnection(ConnectionString)

        cn.Open()

        LeggiFattura=0

        Dim cmd As New OleDbCommand

        cmd.CommandText = "Select * From MovimentiContabiliTesta where IdProgettoODV = ? And NumeroProtocollo > 0"
        cmd.Connection = cn
        cmd.Parameters.AddWithValue("@Numero", numero)
        
        Dim NumeroRiga As Integer = 0

        Dim myPOSTreader As OleDbDataReader = cmd.ExecuteReader()
        if myPOSTreader.Read then
            LeggiFattura = campodbn(myPOSTreader.Item("NumeroRegistrazione"))
        End if
        myPOSTreader.Close()
        cn.Close()
    End Function

    Function campodbn(ByVal oggetto As Object) As Double
        If IsDBNull(oggetto) Then
            Return 0
        Else
            Return oggetto
        End If
    End Function
   Function campodbD(ByVal oggetto As Object) As Date
        If IsDBNull(oggetto) Then
            Return Nothing
        Else
            Return oggetto
        End If
    End Function
   Function campodb(ByVal oggetto As Object) As String
        If IsDBNull(oggetto) Then
            Return ""
        Else
            Return oggetto
        End If
    End Function
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Session("UTENTE") = "" Then
            Response.Redirect("..\Login.aspx")
            Exit Sub
        End If


        Call EseguiJS()

        If Page.IsPostBack = True Then
            Exit Sub
        End If

        Dim K1 As New Cls_SqlString

        Dim Barra As New Cls_BarraSenior

        If Not IsNothing(Session("RicercaAnagraficaSQLString")) Then
            K1 = Session("RicercaAnagraficaSQLString")
        End If

        Lbl_BarraSenior.Text = Barra.CodiceBarra(Application("SENIOR"), Session("UTENTE"), Page, K1)

        Btn_Nuovo.Visible = True

        Txt_Data.Text = Format(Now,"dd/MM/yyyy")


        Dim DettagliAppuntamenti  As new Cls_DettaglioAppuntamentoSave

        Dim AppuntamentiFilePDF As List(Of DettaglioAppuntamento) = New List(Of DettaglioAppuntamento)

        DettagliAppuntamenti.CaricaPrestazioni(Session("DC_TABELLE"),AppuntamentiFilePDF)

        Session("AppuntamentiFilePDF") = AppuntamentiFilePDF

        Call  EstraAppuntamenti()
    End Sub


    Protected Sub grd_ElencoOperatori_PageIndexChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewPageEventArgs) Handles grd_ElencoAppuntamenti.PageIndexChanging
        grd_ElencoAppuntamenti.PageIndex = e.NewPageIndex
        grd_ElencoAppuntamenti.DataSource = CType(Session("AppuntamentiFilePDF"), List(Of DettaglioAppuntamento))
        grd_ElencoAppuntamenti.DataBind()
    End Sub

    Protected Sub grd_ElencoOperatori_RowCommand(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewCommandEventArgs) Handles grd_ElencoAppuntamenti.RowCommand
         Select Case e.CommandName
            Case "Seleziona"
                Response.Redirect("Appuntamento_Dettaglio.aspx?ID_Operatore=" & e.CommandArgument)
            Case "EmissioneFattura"
                Response.Redirect("EmissioneFattura.aspx?CodiceOperazione=" & e.CommandArgument)
        End Select
    End Sub

    Private Sub EstraAppuntamenti()
               Dim MyFilteredList As List(Of DettaglioAppuntamento)

        MyFilteredList=GetAppuntamenti()
        
        For Each Appuntamento In MyFilteredList

            Dim Registrazione As New Cls_MovimentoContabile
        
            Registrazione.NumeroRegistrazione = LeggiFattura(Appuntamento.CodiceOperazione)

            If Registrazione.NumeroRegistrazione > 0 then                
               Registrazione.Leggi(Session("DC_GENERALE"),Registrazione.NumeroRegistrazione)
 
               Appuntamento.Fattura = Registrazione.NumeroProtocollo & "/" & Registrazione.AnnoProtocollo            
            Else
                Appuntamento.Fattura = ""
            End IF
        Next

        grd_ElencoAppuntamenti.DataSource = MyFilteredList
        grd_ElencoAppuntamenti.DataBind()
    End Sub


    Protected Sub ImageButton1_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles ImageButton1.Click
            Call  EstraAppuntamenti()
    End Sub

    Protected Sub Btn_Nuovo_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles Btn_Nuovo.Click

        Response.Redirect("NuovoAppuntamentoManuale.aspx")

    End Sub

    Private Function GetAppuntamenti() As List(Of DettaglioAppuntamento)
        Dim MyListaAppuntamenti = CType(Session("AppuntamentiFilePDF"), List(Of DettaglioAppuntamento))
        Dim MyFilteredList As List(Of DettaglioAppuntamento)
        If String.IsNullOrEmpty(Txt_Ricerca.Text) And String.IsNullOrEmpty(txt_CF.Text) And String.IsNullOrEmpty(txt_Data.Text) Then
            Return MyListaAppuntamenti
        Else
            MyFilteredList = MyListaAppuntamenti.Where(Function(appuntamento) (If(String.IsNullOrEmpty(txt_Data.Text), True, appuntamento.Data = txt_Data.Text)) And (appuntamento.Utente.Cognome.ToLower.Contains(Txt_Ricerca.Text.ToLower) Or appuntamento.Utente.Nome.ToLower.Contains(Txt_Ricerca.Text.ToLower) Or String.IsNullOrEmpty(Txt_Ricerca.Text)) And (appuntamento.Utente.CodiceFiscale.ToLower.Contains(txt_CF.Text.ToLower.Trim) Or String.IsNullOrEmpty(txt_CF.Text.ToLower))).ToList
            Return MyFilteredList
        End If
    End Function


    Private Sub EseguiJS()
        Dim MyJs As String
        MyJs = "$(document).ready(function() {"

        MyJs = MyJs & "var els = document.getElementsByTagName(""*"");"

        MyJs = MyJs & "for (var i=0;i<els.length;i++)"
        MyJs = MyJs & "if ( els[i].id ) { "
        MyJs = MyJs & " var appoggio =els[i].id; "
        MyJs = MyJs & " if (appoggio.match('Txt_Data')!= null) {  "
        MyJs = MyJs & " $(els[i]).mask(""99/99/9999"");"
        MyJs = MyJs & " $(els[i]).datepicker({ changeMonth: true,changeYear: true,  yearRange: ""1990:" & Year(Now) + 5 & """},$.datepicker.regional[""it""]);"
        MyJs = MyJs & "    }"
        MyJs = MyJs & " if ((appoggio.match('TxtImporto')!= null) ) {  "
        MyJs = MyJs & " $(els[i]).keypress(function() { ForceNumericInput($(this).val(), true, true); } ); "
        MyJs = MyJs & " $(els[i]).blur(function() { var ap = formatNumber($(this).val(),2); $(this).val(ap); });"
        MyJs = MyJs & "    }"

        MyJs = MyJs & " if ((appoggio.match('TxtComune')!= null) ) {  "
        MyJs = MyJs & " $(els[i]).autocomplete('/WebHandler/AutocompleteComune.ashx?Utente=" & Session("UTENTE") & "', {delay:5,minChars:3});"
        MyJs = MyJs & "    }"

        MyJs = MyJs & "} " & vbNewLine


        'MyJs = MyJs & "$("".chosen-select"").chosen({"
        'MyJs = MyJs & "no_results_text: ""Comune non trovato"","
        'MyJs = MyJs & "display_disabled_options: true,"
        'MyJs = MyJs & "allow_single_deselect: true,"
        'MyJs = MyJs & "width: ""400px"","
        'MyJs = MyJs & "placeholder_text_single: ""Seleziona Comune"""
        'MyJs = MyJs & "});"


        MyJs = MyJs & "});"

        'MyJs = "$(document).ready(function() { $('.myClass').keypress(function() { handleEnter($('.myClass').val(), true, true); } );     $('#" & Txt_ImportoPagato.ClientID & "').blur(function() { var ap = formatNumber ($('#" & Txt_ImportoPagato.ClientID & "').val(),2); $('#" & Txt_ImportoPagato.ClientID & "').val(ap); }); });"
        ScriptManager.RegisterStartupScript(Me, Me.GetType(), "visualizzaRitIm", MyJs, True)
    End Sub

End Class
