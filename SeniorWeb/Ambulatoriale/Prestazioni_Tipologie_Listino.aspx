﻿<%@ Page Language="VB" AutoEventWireup="false" Inherits="Ambulatoriale_Prestazioni_Tipologie_Listino" CodeFile="Prestazioni_Tipologie_Listino.aspx.vb" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="xasp" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
    <title>Listino Tipologie Prestazioni</title>
    <asp:PlaceHolder runat="server">
        <%: System.Web.Optimization.Styles.Render("~/Content/AjaxControlToolkit/Styles/Bundle") %>
    </asp:PlaceHolder>
    <link href="ospiti.css?ver=1" rel="stylesheet" type="text/css" />
    <link rel="shortcut icon" href="images/SENIOR.ico" />
    <link href="js/jquery.autocomplete.css" rel="stylesheet" type="text/css" />

    <script src="js/jquery-1.5.1.min.js" type="text/javascript"></script>
    <script src="js/jquery.autocomplete.js" type="text/javascript"></script>
    <script src="js/soapclient.js" type="text/javascript"></script>
    <script src="/js/convertinumero.js" type="text/javascript"></script>

    <script src="js/jquery.maskedinput-1.3.min.js" type="text/javascript"></script>
    <script src="/js/formatnumer.js" type="text/javascript"></script>

    <script src="/js/pianoconti.js" type="text/javascript"></script>
    <script src="js/JSErrore.js" type="text/javascript"></script>
    <link rel="stylesheet" href="dialogbox/redips-dialog.css" type="text/css" media="screen" />
    <script type="text/javascript" src="dialogbox/redips-dialog-min.js"></script>
    <script type="text/javascript" src="dialogbox/script.js"></script>
    <link rel="stylesheet" href="jqueryui/jquery-ui.css" type="text/css" />
    <script src="jqueryui/jquery-ui.js" type="text/javascript"></script>
    <script src="jqueryui/datapicker-it.js" type="text/javascript"></script>

    <%--    <script src="js/chosen/chosen.jquery.js" type="text/javascript"></script>
    <script src="js/chosen/docsupport/prism.js" type="text/javascript" charset="utf-8"></script>
    <link rel="stylesheet" href="js/chosen/docsupport/style.css">
    <link rel="stylesheet" href="js/chosen/docsupport/prism.css">
    <link rel="stylesheet" href="js/chosen/chosen.css">--%>
    <script type="text/javascript">  
        $(document).ready(function () {
            if (window.innerHeight > 0) { $("#BarraLaterale").css("height", (window.innerHeight - 94) + "px"); } else { $("#BarraLaterale").css("height", (document.documentElement.offsetHeight - 94) + "px"); }
        });
        function openPopUp(urlToOpen, nome, parametri) {
            var popup_window = window.open(urlToOpen, '_blank');
            try {
                popup_window.focus();
            } catch (e) {
                alert("E' stato bloccato l'aperutra del popup da parte del browser");
            }
        }
    </script>
    <style type="text/css">
        <style >
        th {
            font-weight: normal;
        }

        .tabella {
            border: 1px solid #ccc;
            border-collapse: collapse;
            margin: 0;
            padding: 0;
            width: 100%;
            table-layout: fixed;
        }

        .miotr {
            background: #f8f8f8;
            border: 1px solid #ddd;
            padding: .35em;
        }

        .miacella {
            padding: .625em;
            text-align: center;
        }

        .miaintestazione {
            padding: .625em;
            text-align: center;
            font-size: .85em;
            letter-spacing: .1em;
            text-transform: uppercase;
        }

        .Row {
            display: table;
            width: 100%; /*Optional*/
            table-layout: fixed; /*Optional*/
            border-spacing: 10px; /*Optional*/
        }

        .Column {
            display: table-cell;
            vertical-align: top;
        }

        .cornicegrigia {
            border-bottom-color: rgb(227, 227, 227);
            border-bottom-left-radius: 4px;
            border-bottom-right-radius: 4px;
            border-bottom-style: solid;
            border-bottom-width: 1px;
            border-image-outset: 0px;
            border-image-repeat: stretch;
            border-image-slice: 100%;
            border-image-source: none;
            border-image-width: 1;
            border-left-color: rgb(227, 227, 227);
            border-left-style: solid;
            border-left-width: 1px;
            border-right-color: rgb(227, 227, 227);
            border-right-style: solid;
            border-right-width: 1px;
            border-top-color: rgb(227, 227, 227);
            border-top-left-radius: 4px;
            border-top-right-radius: 4px;
            border-top-style: solid;
            border-top-width: 1px;
            box-shadow: rgba(0, 0, 0, 0.0470588) 0px 1px 1px 0px inset;
            color: rgb(75, 75, 75);
            display: block;
            font-family: "Source Sans Pro",sans-serif;
            font-size: 16px;
            height: 460px;
            line-height: 18px;
            margin-bottom: 4.80000019073486px;
            margin-left: 0px;
            margin-right: 0px;
            margin-top: 4.80000019073486px;
            min-height: 20px;
            padding-bottom: 8px;
            padding-left: 8px;
            padding-right: 8px;
            padding-top: 8px;
        }
    </style>
</head>
<body>
    <form id="form1" runat="server">
        <asp:ScriptManager ID="ScriptManager1" runat="server" AsyncPostBackTimeout="900" EnableScriptGlobalization="true">
            <Scripts>
                <asp:ScriptReference Path="~/Scripts/AjaxControlToolkit/Bundle" />
            </Scripts>
        </asp:ScriptManager>
        <asp:Label ID="Lbl_BarraSenior" runat="server" Text=""></asp:Label>
        <div>
            <table style="width: 100%;" cellpadding="0" cellspacing="0">
                <tr>
                    <td style="width: 160px; background-color: #F0F0F0;"></td>
                    <td>
                        <div class="Titolo">Ambulatoriale - Elenco Tipologie Prestazioni</div>
                        <div class="SottoTitolo">
                            <br />
                            <br />
                        </div>
                    </td>
                    <td style="text-align: right; vertical-align: top;">
                        <span class="BenvenutoText">Benvenuto <%=Session("UTENTE")%>&nbsp;&nbsp;&nbsp;&nbsp;</span>
                    </td>
                </tr>
                <tr>
                    <td style="width: 160px; background-color: #F0F0F0;"></td>
                    <td colspan="2" style="border: 2px solid #9C9C9C; background-color: #DCDCDC;"></td>
                </tr>
                <tr>
                    <td style="width: 160px; background-color: #F0F0F0; vertical-align: top; text-align: center;" id="BarraLaterale">
                        <a href="Prestazioni_Tipologie_Elenco" style="border-width: 0px;">
                            <img src="../images/Menu_Indietro.png" alt="Torna ad Elenco" class="Effetto" />
                        </a><br />
                        <br />
                        <br />
                    </td>
                    <td colspan="2" style="background-color: #FFFFFF;" valign="top">
                        <div style="text-align: right; width: 100%; position: relative; width: auto; margin-right: 5px;" align="right">
                            <asp:ImageButton ID="btn_Salva" runat="server" ImageUrl="../images/save.png" CssClass="EffettoBottoniTondi" ToolTip="Aggiungi" />
                        </div>
                        <xasp:TabContainer ID="TabContainer1" runat="server" ActiveTabIndex="0" CssClass="TabSenior"
                            Width="100%" BorderStyle="None" Style="margin-right: 39px">
                            <xasp:TabPanel runat="server" HeaderText="Prima Nota" ID="Tab_Anagrafica">
                                <HeaderTemplate>
                                    <asp:Label runat="server" ID="lbl_TitoloTab"></asp:Label>
                                </HeaderTemplate>
                                <ContentTemplate>
                                    <div class="Row">
                                        <div class="Column">
                                            <div class="cornicegrigia">
                                                <label class="LabelCampo">Nome Prestazione :</label>
                                                <asp:TextBox ID="txt_Descrizione" runat="server" Width="60%"></asp:TextBox>
                                                <br />
                                                <br />
                                                <br />
                                                <label class="LabelCampo">Codice Prestazione:</label>
                                                <asp:TextBox ID="txt_Codice_Prestazione" runat="server" Width="60%" Style="border-color: Red;"></asp:TextBox>
                                                <br />
                                                <br />
                                                <label class="LabelCampo">Nome per Importazione da pdf:</label>
                                                <asp:TextBox ID="txt_Chiave_Import" runat="server" Width="60%" Style="border-color: Red;"></asp:TextBox>
                                                <br />
                                                <br />
                                                <label class="LabelCampo">Note :</label>
                                                <asp:TextBox ID="txt_Note" runat="server" Width="60%" TextMode="MultiLine" Height="80px"></asp:TextBox>
                                                <br />
                                                <br />
                                                <label class="LabelCampo">Branca 1:</label>
                                                <asp:DropDownList ID="ddl_Branche1" Width="350px" runat="server" AppendDataBoundItems="true" DataTextField="Descrizione" DataValueField="ID_Branca">
                                                    <asp:ListItem Text="Seleziona" Value="0"></asp:ListItem>
                                                </asp:DropDownList>
                                                <br />
                                                <br />
                                                <label class="LabelCampo">Branca 2:</label>
                                                <asp:DropDownList ID="ddl_Branche2" Width="350px" runat="server" AppendDataBoundItems="true" DataTextField="Descrizione" DataValueField="ID_Branca">
                                                    <asp:ListItem Text="Seleziona" Value="0"></asp:ListItem>
                                                </asp:DropDownList>
                                                <br />
                                                <br />
                                                <label class="LabelCampo">Branca 3:</label>
                                                <asp:DropDownList ID="ddl_Branche3" Width="350px" runat="server" AppendDataBoundItems="true" DataTextField="Descrizione" DataValueField="ID_Branca">
                                                    <asp:ListItem Text="Seleziona" Value="0"></asp:ListItem>
                                                </asp:DropDownList>
                                                <br />
                                                <br />
                                                <label class="LabelCampo">Branca 4:</label>
                                                <asp:DropDownList ID="ddl_Branche4" Width="350px" runat="server" AppendDataBoundItems="true" DataTextField="Descrizione" DataValueField="ID_Branca">
                                                    <asp:ListItem Text="Seleziona" Value="0"></asp:ListItem>
                                                </asp:DropDownList>
                                                <br />
                                                <br />
                                                <label class="LabelCampo">Branca 5:</label>
                                                <asp:DropDownList ID="ddl_Branche5" Width="350px" runat="server" AppendDataBoundItems="true" DataTextField="Descrizione" DataValueField="ID_Branca">
                                                    <asp:ListItem Text="Seleziona" Value="0"></asp:ListItem>
                                                </asp:DropDownList>
                                                <br />
                                                <br />
                                            </div>
                                        </div>
                                        <div class="Column">
                                            <div class="cornicegrigia">
                                                <label class="LabelCampo">Contratto di Servizio :</label>
                                                <asp:DropDownList runat="server" ID="ddl_ContrattoDiServizio" DataValueField="ID_ContrattoDiServizio" DataTextField="Descrizione">                                                </asp:DropDownList>
                                                <br />
                                                <br />
                                                <label class="LabelCampo">Fascia ISEE:</label>
                                                <asp:DropDownList runat="server" ID="ddl_FasciaIse" AppendDataBoundItems="true" DataValueField="ID_FasciaISEE" DataTextField="Descrizione">
                                                    <asp:ListItem Text="Selezionare" Value="0" Selected="True"></asp:ListItem>
                                                </asp:DropDownList>
                                                <br />
                                                <br />
                                                <label class="LabelCampo">Importo Prestazione:</label>
                                                <asp:TextBox runat="server" ID="txt_Importo"></asp:TextBox>
                                                <br />
                                                <br />
                                                <asp:Button runat="server" ID="btn_AggiungiListino" Text="Aggiungi Importo" />
                                                <br />
                                                <br />
                                                <asp:GridView ID="grd_ListinoTipologiePrestazioni" runat="server" CellPadding="3" ItemType="Senior.Entities.Prestazione_Listino"
                                                    Width="100%" BackColor="White" BorderColor="#999999" BorderStyle="None" AutoGenerateColumns="false"
                                                    BorderWidth="1px" GridLines="Vertical" AllowPaging="True"
                                                    ShowFooter="True" PageSize="20">
                                                    <Columns>
                                                        <asp:TemplateField ItemStyle-Width="40px">
                                                            <ItemTemplate>
                                                                <asp:ImageButton ID="btn_EliminaListino" CommandName="Elimina" runat="Server" ImageUrl="~/images/cancella.png" CssClass="EffettoBottoniTondi" BackColor="Transparent" CommandArgument='<%# Item.ID_Listino  %>' />
                                                            </ItemTemplate>
                                                        </asp:TemplateField>
                                                        <asp:BoundField DataField="ID_Listino" Visible="false" />
                                                        <asp:BoundField DataField="ID_Prestazione_Tipologia" Visible="false" />
                                                        <asp:BoundField DataField="ID_ContrattoDiServizio" Visible="false" />
                                                        <asp:BoundField DataField="ContrattoDiServizio" />
                                                        <asp:BoundField DataField="ID_FasciaISEE" Visible="false" />
                                                        <asp:BoundField DataField="FasciaISEE" />
                                                        <asp:BoundField DataField="Importo" DataFormatString = "{0:#,##0.00}"  ItemStyle-HorizontalAlign="Right" />
                                                    </Columns>
                                                    <RowStyle ForeColor="#565151" BackColor="#EEEEEE" />
                                                    <AlternatingRowStyle BackColor="White" />
                                                    <FooterStyle BackColor="#CCCCCC" ForeColor="Black" />
                                                    <PagerStyle BackColor="#999999" ForeColor="Black" HorizontalAlign="Center" />
                                                    <SelectedRowStyle BackColor="#008A8C" Font-Bold="True" ForeColor="White" />
                                                    <HeaderStyle BackColor="#565151" Font-Bold="false" Font-Size="Small" ForeColor="White" />
                                                    <AlternatingRowStyle BackColor="#DCDCDC" />
                                                </asp:GridView>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="Row">
                                        <div class="Column">
                                            <div class="cornicegrigia">
                                                <h3>Prestazioni ePersonam Collegate</h3>
                                                <label class="LabelCampo">Prestazioni ePersonam :</label>
                                                <asp:DropDownList runat="server" ID="ddl_ePersonam_exams" DataValueField="Codice_ePersonam" DataTextField="Descrizione_ePersonam" ></asp:DropDownList>
                                                <br />
                                                <br />
                                                <asp:Button runat="server" ID="btn_AddPrestazione_ePersonam" Text="Collega Prestazione ePersonam" />
                                                <br />
                                                <br />
                                                <asp:GridView ID="grid_Prestazioni_ePersonam" runat="server" CellPadding="3" ItemType="Senior.Entities.Prestazione_Tipologia__ePersonam_exams"
                                                    Width="100%" BackColor="White" BorderColor="#999999" BorderStyle="None" AutoGenerateColumns="false"
                                                    BorderWidth="1px" GridLines="Horizontal" AllowPaging="True"
                                                    ShowFooter="false">
                                                    <Columns>
                                                        <asp:TemplateField ItemStyle-Width="40px">
                                                            <ItemTemplate>
                                                                <asp:ImageButton ID="btn_Elimina_ePersonamExam" CommandName="Elimina" runat="Server" ImageUrl="~/images/cancella.png" CssClass="EffettoBottoniTondi" BackColor="Transparent" CommandArgument='<%# Item.ID_Prestazione_Tipologia__ePersonam_exams %>' />
                                                            </ItemTemplate>
                                                        </asp:TemplateField>
                                                        <asp:BoundField DataField="ID_Prestazione_Tipologia__ePersonam" Visible="false" />
                                                        <asp:BoundField DataField="ID_Prestazione_Tipologia" Visible="false" />
                                                        <asp:BoundField DataField="Codice_ePersonam" Visible="true" />
                                                        <asp:BoundField DataField="Descrizione_ePersonam" />
                                                    </Columns>
                                                    <RowStyle ForeColor="#565151" BackColor="#EEEEEE" />
                                                    <AlternatingRowStyle BackColor="White" />
                                                    <SelectedRowStyle BackColor="#008A8C" Font-Bold="True" ForeColor="White" />
                                                    <HeaderStyle BackColor="#565151" Font-Bold="false" Font-Size="Small" ForeColor="White" />
                                                    <AlternatingRowStyle BackColor="#DCDCDC" />
                                                </asp:GridView>
                                                </div>
                                            </div>
                                        </div>
                                </ContentTemplate>
                            </xasp:TabPanel>
                        </xasp:TabContainer>
                    </td>
                </tr>
            </table>
        </div>
    </form>
    
</body>
</html>
