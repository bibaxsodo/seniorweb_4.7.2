﻿Imports Microsoft.VisualBasic
Imports System.Data.OleDb
Imports System.Web.Hosting
Imports System.Data.SqlTypes
Imports Senior.Entities
Imports Senior.BIZ

Partial Class Ambulatoriale_Operatori_Elenco
    Inherits System.Web.UI.Page

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Session("UTENTE") = "" Then
            Response.Redirect("..\Login.aspx")
            Exit Sub
        End If

        If Page.IsPostBack = True Then
            Exit Sub
        End If

        Dim K1 As New Cls_SqlString

        Dim Barra As New Cls_BarraSenior

        If Not IsNothing(Session("RicercaAnagraficaSQLString")) Then
            K1 = Session("RicercaAnagraficaSQLString")
        End If

        Lbl_BarraSenior.Text = Barra.CodiceBarra(Application("SENIOR"), Session("UTENTE"), Page, K1)

        Btn_Nuovo.Visible = True
        If Not Page.IsPostBack Then
            Session("Ambulatoriale_ListaOperatori") = Nothing
            grd_ElencoOperatori.DataSource = GetOperatori()
            grd_ElencoOperatori.DataBind()
        End If
    End Sub


    Protected Sub grd_ElencoOperatori_PageIndexChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewPageEventArgs) Handles grd_ElencoOperatori.PageIndexChanging
        grd_ElencoOperatori.PageIndex = e.NewPageIndex
        grd_ElencoOperatori.DataSource = CType(Session("Ambulatoriale_ListaOperatori"), Operatore_List)
        grd_ElencoOperatori.DataBind()
    End Sub

    Protected Sub grd_ElencoOperatori_RowCommand(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewCommandEventArgs) Handles grd_ElencoOperatori.RowCommand

        If e.CommandName = "Seleziona" Then
            Response.Redirect("OperatoreDettaglio.aspx?ID_ContrattoDiServizio=" & e.CommandArgument)
        End If

    End Sub

    Protected Sub Btn_Esci_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles Btn_Esci.Click
        Response.Redirect("Menu_Ambulatoriale.aspx")
    End Sub

    Protected Sub ImageButton1_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles ImageButton1.Click
        grd_ElencoOperatori.DataSource = GetOperatori()
        grd_ElencoOperatori.DataBind()

    End Sub

    Protected Sub Btn_Nuovo_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles Btn_Nuovo.Click

        Response.Redirect("Operatore_Dettaglio.aspx")

    End Sub

    Private Function GetOperatori() As Operatore_List
        Dim OperatoreManager As New OperatoreBiz(Session("DC_Ospite"))
        Dim MyListaOperatori As New Operatore_List
        If String.IsNullOrEmpty(Txt_Ricerca.Text) Then
            MyListaOperatori = OperatoreManager.List()
        Else
            MyListaOperatori = OperatoreManager.FindByName(Txt_Ricerca.Text)
        End If
        Session("Ambulatoriale_ListaOperatori") = MyListaOperatori
        Return MyListaOperatori
    End Function





    'Protected Sub Lnk_ToExcel_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles Lnk_ToExcel.Click
    'Dim cn As New OleDbConnection


    '    Dim ConnectionString As String = Session("DC_OSPITE")

    '    cn = New Data.OleDb.OleDbConnection(ConnectionString)

    '    cn.Open()

    '    Dim cmd As New OleDbCommand




    '    cmd.CommandText = "Select * From AnagraficaComune Where Tipologia ='A' Order By Nome"

    '    cmd.Connection = cn
    '    cmd.Connection = cn


    '    Tabella.Clear()
    '    Tabella.Columns.Add("Codice", GetType(String))
    '    Tabella.Columns.Add("Nome", GetType(String))
    '    Tabella.Columns.Add("RESIDENZAINDIRIZZO1", GetType(String))
    '    Tabella.Columns.Add("RESIDENZACOMUNE1", GetType(String))
    '    Tabella.Columns.Add("RESIDENZAPROVINCIA1", GetType(String))
    '    Tabella.Columns.Add("Telefono1", GetType(String))
    '    Tabella.Columns.Add("Telefono2", GetType(String))
    '    Tabella.Columns.Add("Specializazione", GetType(String))
    '    Tabella.Columns.Add("Note", GetType(String))
    '    Tabella.Columns.Add("CodiceFiscale", GetType(String))

    '    Dim myPOSTreader As OleDbDataReader = cmd.ExecuteReader()
    '    Do While myPOSTreader.Read
    '        Dim myriga As System.Data.DataRow = Tabella.NewRow()

    '        myriga(0) = campodb(myPOSTreader.Item("CodiceMedico"))
    '        myriga(1) = campodb(myPOSTreader.Item("Nome"))
    '        myriga(2) = campodb(myPOSTreader.Item("RESIDENZAINDIRIZZO1"))
    '        myriga(3) = campodb(myPOSTreader.Item("RESIDENZACOMUNE1"))
    '        myriga(4) = campodb(myPOSTreader.Item("RESIDENZAPROVINCIA1"))
    '        myriga(5) = campodb(myPOSTreader.Item("Telefono1"))
    '        myriga(6) = campodb(myPOSTreader.Item("Telefono2"))
    '        myriga(7) = campodb(myPOSTreader.Item("Specializazione"))
    '        myriga(8) = campodb(myPOSTreader.Item("Note"))
    '        myriga(9) = campodb(myPOSTreader.Item("CodiceFiscale"))

    '        Tabella.Rows.Add(myriga)
    '    Loop

    '    myPOSTreader.Close()
    '    cn.Close()


    '    Session("GrigliaSoloStampa") = Tabella
    '    ScriptManager.RegisterClientScriptBlock(Me, Me.GetType(), "Stampa", "openPopUp('ExportExcel.aspx','Excel','width=800,height=600');", True)

    'End Sub
End Class
