﻿Imports Senior.Entities
Imports Senior.BIZ
Imports System.Data.OleDb
Imports System.Web.Script.Serialization
Public Class EliminaRegistrazioni
    Inherits System.Web.UI.Page
    Dim MyTable As New System.Data.DataTable("tabella")
    Dim MyDataSet As New System.Data.DataSet()

     Function campodbn(ByVal oggetto As Object) As Double
        If IsDBNull(oggetto) Then
            Return 0
        Else
            Return oggetto
        End If
    End Function
   Function campodbD(ByVal oggetto As Object) As Date
        If IsDBNull(oggetto) Then
            Return Nothing
        Else
            Return oggetto
        End If
    End Function
   Function campodb(ByVal oggetto As Object) As String
        If IsDBNull(oggetto) Then
            Return ""
        Else
            Return oggetto
        End If
    End Function
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
       If Session("UTENTE") = "" Then
            Response.Redirect("..\Login.aspx")
            Exit Sub
        End If

        If Page.IsPostBack = True Then
            Exit Sub
        End If

        Dim K1 As New Cls_SqlString

        Dim Barra As New Cls_BarraSenior

        If Not IsNothing(Session("RicercaAnagraficaSQLString")) Then
            K1 = Session("RicercaAnagraficaSQLString")
        End If

        Lbl_BarraSenior.Text = Barra.CodiceBarra(Application("SENIOR"), Session("UTENTE"), Page, K1)

        Call CaricaTabella
    End Sub

    Private Sub CaricaTabella
        Dim cn As New OleDbConnection


        Dim ConnectionString As String = Session("DC_GENERALE")

        cn = New Data.OleDb.OleDbConnection(ConnectionString)

        cn.Open()

        MyTable.Clear()
        MyTable.Columns.Clear()

        MyTable.Columns.Add("Numero Registrazione", GetType(String))
        MyTable.Columns.Add("Data Registrazione", GetType(String))
        MyTable.Columns.Add("Causale Contabile", GetType(String))
        MyTable.Columns.Add("Importo", GetType(String))

        
        Dim CodiceOperazione As  String

        CodiceOperazione = Request.Item("CodiceOperazione")

        Dim cmd As New OleDbCommand

        cmd.CommandText = "Select * From MovimentiContabiliTesta where IdProgettoODV = ? Order By DataRegistrazione"
        cmd.Connection = cn
        cmd.Parameters.AddWithValue("@CodiceOperazione", CodiceOperazione)
                
        Dim myPOSTreader As OleDbDataReader = cmd.ExecuteReader()
        Do While myPOSTreader.Read 
            Dim myriga As System.Data.DataRow = MyTable.NewRow()

            myriga.Item(0) = campodb(myPOSTreader.Item("NumeroRegistrazione"))
            myriga.Item(1) = campodbD(myPOSTreader.Item("DataRegistrazione"))

            Dim CausaleContabile As New Cls_CausaleContabile

            CausaleContabile.Codice =campodb(myPOSTreader.Item("CausaleContabile"))
            CausaleContabile.Leggi(Session("DC_TABELLE"),CausaleContabile.Codice)


            myriga.Item(2) = CausaleContabile.Descrizione

            Dim Registrazione As New Cls_MovimentoContabile

            Registrazione.NumeroRegistrazione =  campodbn(myPOSTreader.Item("NumeroRegistrazione"))
            Registrazione.Leggi(Session("DC_GENERALE"), Registrazione.NumeroRegistrazione)
            myriga.Item(3) = Registrazione.ImportoRegistrazioneDocumento(Session("DC_TABELLE"))


            MyTable.Rows.Add(myriga)
            
        loop
        myPOSTreader.Close()

        grd_EliminaRegistrazioni.AutoGenerateColumns = True
        grd_EliminaRegistrazioni.DataSource = MyTable
        grd_EliminaRegistrazioni.Font.Size = 10
        grd_EliminaRegistrazioni.DataBind()

        ViewState("MyTable") =MyTable
        cn.Close()
    End Sub


    Private Sub grd_EliminaRegistrazioni_RowCommand(sender As Object, e As GridViewCommandEventArgs) Handles grd_EliminaRegistrazioni.RowCommand
        If e.CommandName = "Seleziona" Then


            Dim d As Integer


            MyTable = ViewState("MyTable")

            d = Val(e.CommandArgument)


            Dim NumeroRegistrazione As String

            NumeroRegistrazione= MyTable.Rows(d).Item(0).ToString

            Dim Registrazione As New Cls_MovimentoContabile

            Registrazione.NumeroRegistrazione = NumeroRegistrazione
            Registrazione.Leggi(Session("DC_GENERALE"), Registrazione.NumeroRegistrazione)

            If Registrazione.NumeroProtocollo > 0 Then
                Dim TabelleLegami As New Cls_Legami

                If TabelleLegami.TotaleLegame(Session("DC_GENERALE"),Registrazione.NumeroRegistrazione) > 0 Then
                    ClientScript.RegisterClientScriptBlock(Me.GetType(), "Errore", "VisualizzaErrore('Legami presenti non posso eliminare');", True)
                    Exit Sub
                End If

                Dim UltimoProtocollo As  Integer
                UltimoProtocollo =Registrazione.MaxProtocollo(Session("DC_GENERALE"),Registrazione.AnnoProtocollo,Registrazione.RegistroIVA) 

                If UltimoProtocollo > Registrazione.NumeroProtocollo Then
                    ClientScript.RegisterClientScriptBlock(Me.GetType(), "Errore", "VisualizzaErrore('Vi sono registrazioni con numero protocollo successivo');", True)
                    Exit Sub
                End If


                Dim Log As New Cls_LogPrivacy

                Dim serializer As JavaScriptSerializer
                serializer = New JavaScriptSerializer()
                Log.LogPrivacy(Application("SENIOR"), Session("CLIENTE"), Session("UTENTE"), Session("UTENTEIMPER"), Page.GetType.Name.ToUpper, 0, 0, "", "", Registrazione.NumeroRegistrazione, "", "D", "ELLIMINAREGISTRAZIONE", serializer.Serialize(Registrazione))


                Registrazione.EliminaRegistrazione(Session("DC_GENERALE"))
            Else
                Dim TabelleLegami As New Cls_Legami


                Dim Log As New Cls_LogPrivacy

                Dim serializer As JavaScriptSerializer
                serializer = New JavaScriptSerializer()
                Log.LogPrivacy(Application("SENIOR"), Session("CLIENTE"), Session("UTENTE"), Session("UTENTEIMPER"), Page.GetType.Name.ToUpper, 0, 0, "", "", Registrazione.NumeroRegistrazione, "", "D", "ELLIMINAREGISTRAZIONE", serializer.Serialize(Registrazione))


                Registrazione.EliminaRegistrazione(Session("DC_GENERALE"))

                TabelleLegami.Leggi(Session("DC_GENERALE"),0, Registrazione.NumeroRegistrazione)

                For i =0 To 300
                    If TabelleLegami.NumeroPagamento(i) = Registrazione.NumeroRegistrazione Then
                        TabelleLegami.NumeroPagamento(i) =0
                        TabelleLegami.NumeroDocumento(i) =0
                        TabelleLegami.Importo(i) = 0
                    End If
                Next
                TabelleLegami.Scrivi(Session("DC_GENERALE"),0, Registrazione.NumeroRegistrazione)
            End If


        End If

        Call CaricaTabella
    End Sub

    Private Sub ImgMenu_Click(sender As Object, e As ImageClickEventArgs) Handles ImgMenu.Click        
        Dim CodiceOperazione As  String

        CodiceOperazione = Request.Item("CodiceOperazione")

        Response.Redirect("EmissioneFattura.aspx?CodiceOperazione=" & CodiceOperazione)        
    End Sub
End Class