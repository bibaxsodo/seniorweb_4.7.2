﻿Imports Microsoft.VisualBasic
Imports System.Data.OleDb
Imports System.Web.Hosting


Public Class Cls_Budget
    Public Anno As Long
    Public Colonna As Long
    Public Livello1 As Long
    Public Livello2 As Long
    Public Livello3 As Long    
    Public Importo As Double
    Public Utente As String


    Sub Decodfica(ByVal StringaConnessione As String)
        Dim cn As OleDbConnection


        cn = New Data.OleDb.OleDbConnection(StringaConnessione)

        cn.Open()
        Dim cmd As New OleDbCommand()
        cmd.CommandText = ("select * from Budget where " & _
                             "Anno = ? And Livello1 = ? And Livello2 = ? And Livello3 = ? And Colonna = ?")

        cmd.Parameters.AddWithValue("@Anno", Anno)
        cmd.Parameters.AddWithValue("@Livello1", Livello1)
        cmd.Parameters.AddWithValue("@Livello2", Livello2)
        cmd.Parameters.AddWithValue("@Livello3", Livello3)
        cmd.Parameters.AddWithValue("@Colonna", Colonna)
        cmd.Connection = cn

        Dim myPOSTreader As OleDbDataReader = cmd.ExecuteReader()
        If myPOSTreader.Read Then
            Anno = campodb(myPOSTreader.Item("Anno"))
            Colonna = campodb(myPOSTreader.Item("Colonna"))
            Livello1 = campodb(myPOSTreader.Item("Livello1"))
            Livello2 = campodb(myPOSTreader.Item("Livello2"))
            Livello3 = campodb(myPOSTreader.Item("Livello3"))
            Importo = campodb(myPOSTreader.Item("Importo"))
            Utente = campodb(myPOSTreader.Item("Utente"))

        End If
        myPOSTreader.Close()
        cn.Close()
    End Sub


    Function campodb(ByVal oggetto As Object) As String
        If IsDBNull(oggetto) Then
            Return ""
        Else
            Return oggetto
        End If
    End Function


    Sub Scrivi(ByVal StringaConnessione As String)
        Dim cn As OleDbConnection
        Dim MySql As String

        Dim Modifica As Boolean = False

        cn = New Data.OleDb.OleDbConnection(StringaConnessione)

        cn.Open()
        Dim cmd As New OleDbCommand()
        cmd.CommandText = ("select * from Budget where " & _
                           "Anno = ? And Livello1 = ? And Livello2 = ? And Livello3 = ? And Colonna = ?")

        cmd.Parameters.AddWithValue("@Anno", Anno)
        cmd.Parameters.AddWithValue("@Livello1", Livello1)
        cmd.Parameters.AddWithValue("@Livello2", Livello2)
        cmd.Parameters.AddWithValue("@Livello3", Livello3)
        cmd.Parameters.AddWithValue("@Colonna", Colonna)
        cmd.Connection = cn
        Dim sb As StringBuilder = New StringBuilder

        Dim myPOSTreader As OleDbDataReader = cmd.ExecuteReader()
        If myPOSTreader.Read Then
            Modifica = True
        End If
        myPOSTreader.Close()

        If Modifica = False Then
            Dim APPOGGIO As String = ""
            Dim I As Integer
            For I = 1 To 10
                APPOGGIO = APPOGGIO & Int(Rnd(1) * 10)
            Next
            Dim cmdIns As New OleDbCommand()
            MySql = "INSERT INTO Budget (Anno,Livello1,Livello2,Livello3,Colonna,Importo,Utente) VALUES (?,?,?,?,?,?,?)"
            cmdIns.CommandText = MySql
            cmdIns.Parameters.AddWithValue("@Anno", Anno)
            cmdIns.Parameters.AddWithValue("@Livello1", Livello1)
            cmdIns.Parameters.AddWithValue("@Livello2", Livello2)
            cmdIns.Parameters.AddWithValue("@Livello3", Livello3)
            cmdIns.Parameters.AddWithValue("@Colonna", Colonna)
            cmdIns.Parameters.AddWithValue("@Importo", Importo)
            cmdIns.Parameters.AddWithValue("@Utente", Utente)
            cmdIns.Connection = cn
            cmdIns.ExecuteNonQuery()
        End If

        MySql = "UPDATE Budget  SET " & _
                "Importo = ?, " & _
                "Utente = ? " & _
                " Where Anno = ? And Livello1 = ? And Livello2 = ? And Livello3 = ? And Colonna = ?"
        Dim cmd1 As New OleDbCommand()
        cmd1.Parameters.AddWithValue("@Importo", Importo)
        cmd1.Parameters.AddWithValue("@Utente", Utente)
        cmd1.Parameters.AddWithValue("@Anno", Anno)
        cmd1.Parameters.AddWithValue("@Livello1", Livello1)
        cmd1.Parameters.AddWithValue("@Livello2", Livello2)
        cmd1.Parameters.AddWithValue("@Livello3", Livello3)
        cmd1.Parameters.AddWithValue("@Colonna", Colonna)

        cmd1.CommandText = MySql
        cmd1.Connection = cn
        cmd1.ExecuteNonQuery()

        cn.Close()
    End Sub

    Function campodbN(ByVal oggetto As Object) As Double
        If IsDBNull(oggetto) Then
            Return 0
        Else
            Return oggetto
        End If
    End Function

    Public Function DisponibilitaBudget(ByVal StringaConnessione As String, ByVal Anno As Integer, ByVal Mastro As Long, ByVal Conto As Long, ByVal Sottoconto As Long, ByVal Colonna As Long, ByVal NumeroRegistrazione As Long) As Double
        Dim RsLeggi As New ADODB.Recordset
        Dim RsImportoContabile As New ADODB.Recordset

        Dim TotImporto As Double
        Dim XImp As Double

        Dim MySql As String        
        Dim XImporto As Double
        Dim cn As OleDbConnection


        cn = New Data.OleDb.OleDbConnection(StringaConnessione)

        cn.Open()



        XImporto = 0
        MySql = "Select sum(Importo) , (select sum(Importo) From VariazioniBudget Where Anno = " & Anno & " And Livello1 = " & Mastro & " And Livello2 = " & Conto & " And Livello3 = " & Sottoconto & " And (Colonna =0 Or Colonna = " & Colonna & ")) From Budget Where Anno = " & Anno & " And Livello1 = " & Mastro & " And Livello2 = " & Conto & " And Livello3 = " & Sottoconto & " And (Colonna =0 Or Colonna = " & Colonna & ")"
        Dim cmdBudget As New OleDbCommand()
        cmdBudget.CommandText = MySql
        cmdBudget.Connection = cn
        Dim RdBudget As OleDbDataReader = cmdBudget.ExecuteReader()
        If RdBudget.Read Then
            XImporto = campodbN(RdBudget.Item(0)) + campodbN(RdBudget.Item(1))
        End If
        RdBudget.Close()

        If XImporto = 0 Then

            MySql = "Select sum(Importo) , (select sum(Importo) From VariazioniBudget Where Anno = " & Anno & " And Livello1 = " & Mastro & " And Livello2 = " & Conto & " And Livello3 = 0 And (Colonna =0 Or Colonna = " & Colonna & ")) From Budget Where Anno = " & Anno & " And Livello1 = " & Mastro & " And Livello2 = " & Conto & " And Livello3 = 0 And (Colonna =0 Or Colonna = " & Colonna & ")"
            Dim cmdBudget1 As New OleDbCommand()
            cmdBudget1.CommandText = MySql
            cmdBudget1.Connection = cn
            Dim RdBudget1 As OleDbDataReader = cmdBudget1.ExecuteReader()
            If RdBudget1.Read Then
                XImporto = campodbN(RdBudget1.Item(0)) + campodbN(RdBudget1.Item(1))
            End If
            RdBudget1.Close()
            If XImporto = 0 Then
                MySql = "Select sum(Importo) , (select sum(Importo) From VariazioniBudget Where Anno = " & Anno & " And Livello1 = " & Mastro & " And Livello2 = 0 And Livello3 = 0 And (Colonna =0 Or Colonna = " & Colonna & ")) From Budget Where Anno = " & Anno & " And Livello1 = " & Mastro & " And Livello2 = 0 And Livello3 = 0 And (Colonna =0 Or Colonna = " & Colonna & ")"
                Dim cmdBudget2 As New OleDbCommand()
                cmdBudget2.CommandText = MySql
                cmdBudget2.Connection = cn
                Dim RdBudget2 As OleDbDataReader = cmdBudget2.ExecuteReader()

                If RdBudget2.Read Then
                    XImporto = campodbN(RdBudget2.Item(0)) + campodbN(RdBudget2.Item(1))
                End If
                RdBudget2.Close()
            End If
        End If



        TotImporto = 0
        MySql = "Select sum(Importo) From PrenotazioniRiga Where Anno = " & Anno & " And Livello1 = " & Mastro & " And Livello2 = " & Conto & " And Livello3 = " & Sottoconto & " And Colonna = " & Colonna
        Dim cmdB As New OleDbCommand()
        cmdB.CommandText = MySql
        cmdB.Connection = cn
        Dim RdB As OleDbDataReader = cmdB.ExecuteReader()
        If RdB.Read Then
            TotImporto = campodbN(RdB.Item(0))
        End If
        RdB.Close()


        MySql = "SELECT sum(VariazioniPrenotazioni.Importo) FROM VariazioniPrenotazioni INNER JOIN PrenotazioniRiga ON (VariazioniPrenotazioni.RigaPrenotazione = PrenotazioniRiga.Riga) AND (VariazioniPrenotazioni.NumeroPrenotazione = PrenotazioniRiga.Numero) AND (VariazioniPrenotazioni.AnnoPrenotazione = PrenotazioniRiga.Anno) Where  PrenotazioniRiga.Anno = " & Anno & " And Livello1 = " & Mastro & " And Livello2 = " & Conto & " And Livello3 = " & Sottoconto & " And Colonna = " & Colonna
        Dim cmdB1 As New OleDbCommand()
        cmdB1.CommandText = MySql
        cmdB1.Connection = cn
        Dim RdB1 As OleDbDataReader = cmdB1.ExecuteReader()
        If RdB1.Read Then
            TotImporto = TotImporto - campodbN(RdB1.Item(0))
        End If
        RdB1.Close()

        If Sottoconto = 0 Then
            MySql = "Select sum(Importo) From PrenotazioniRiga Where Anno = " & Anno & " And Livello1 = " & Mastro & " And Livello2 = " & Conto & " And Livello3 > 0 And Colonna = " & Colonna
            Dim cmdB2 As New OleDbCommand()
            cmdB2.CommandText = MySql
            cmdB2.Connection = cn
            Dim RdB2 As OleDbDataReader = cmdB2.ExecuteReader()
            If RdB2.Read Then
                TotImporto = TotImporto + campodbN(RdB2.Item(0))
            End If
            RdB2.Close()

            MySql = "SELECT sum(VariazioniPrenotazioni.Importo) FROM VariazioniPrenotazioni INNER JOIN PrenotazioniRiga ON (VariazioniPrenotazioni.RigaPrenotazione = PrenotazioniRiga.Riga) AND (VariazioniPrenotazioni.NumeroPrenotazione = PrenotazioniRiga.Numero) AND (VariazioniPrenotazioni.AnnoPrenotazione = PrenotazioniRiga.Anno) Where Numero <> " & NumeroRegistrazione & " And PrenotazioniRiga.Anno = " & Anno & " And Livello1 = " & Mastro & " And Livello2 = " & Conto & " And Livello3 > 0 And Colonna = " & Colonna
            Dim cmdB3 As New OleDbCommand()
            cmdB3.CommandText = MySql
            cmdB3.Connection = cn
            Dim RdB3 As OleDbDataReader = cmdB3.ExecuteReader()
            If RdB3.Read Then
                TotImporto = TotImporto - campodbN(RdB3.Item(0))
            End If
            RdB3.Close()

            If Conto = 0 Then

                MySql = "Select sum(Importo) From PrenotazioniRiga Where  Anno = " & Anno & " And Livello1 = " & Mastro & " And Livello2 > 0 And Livello3 > 0 And Colonna = " & Colonna
                Dim cmdB4 As New OleDbCommand()
                cmdB4.CommandText = MySql
                cmdB4.Connection = cn
                Dim RdB4 As OleDbDataReader = cmdB4.ExecuteReader()
                If RdB4.Read Then
                    TotImporto = TotImporto + campodbN(RdB4.Item(0))
                End If
                RdB4.Close()

                MySql = "SELECT sum(VariazioniPrenotazioni.Importo) FROM VariazioniPrenotazioni INNER JOIN PrenotazioniRiga ON (VariazioniPrenotazioni.RigaPrenotazione = PrenotazioniRiga.Riga) AND (VariazioniPrenotazioni.NumeroPrenotazione = PrenotazioniRiga.Numero) AND (VariazioniPrenotazioni.AnnoPrenotazione = PrenotazioniRiga.Anno) Where Numero <> " & NumeroRegistrazione & " And PrenotazioniRiga.Anno = " & Anno & " And Livello1 = " & Mastro & " And Livello2  > 0 And Livello3 > 0 And Colonna = " & Colonna
                Dim cmdB5 As New OleDbCommand()
                cmdB5.CommandText = MySql
                cmdB5.Connection = cn
                Dim RdB5 As OleDbDataReader = cmdB5.ExecuteReader()
                If RdB5.Read Then
                    TotImporto = TotImporto - campodbN(RdB5.Item(0))
                End If
                RdB5.Close()
            End If
        End If


        XImp = 0
        MySql = "Select sum(Importo) From LegameBudgetRegistrazione Where NumeroRegistrazione <> " & NumeroRegistrazione & " And Anno = " & Anno & " And Livello1 = " & Mastro & " And Livello2 = " & Conto & " And Livello3 = " & Sottoconto & " And Colonna = " & Colonna
        Dim cmdB6 As New OleDbCommand()
        cmdB6.CommandText = MySql
        cmdB6.Connection = cn
        Dim RdB6 As OleDbDataReader = cmdB6.ExecuteReader()
        If RdB6.Read Then
            XImp = campodbN(RdB6.Item(0))
        End If
        RdB6.Close()

        If XImp = 0 Then
            MySql = "Select sum(Importo) From LegameBudgetRegistrazione Where NumeroRegistrazione <> " & NumeroRegistrazione & " And Anno = " & Anno & " And Livello1 = " & Mastro & " And Livello2 = " & Conto & " And Livello3 = 0 And Colonna = " & Colonna
            Dim cmdB7 As New OleDbCommand()
            cmdB7.CommandText = MySql
            cmdB7.Connection = cn
            Dim RdB7 As OleDbDataReader = cmdB7.ExecuteReader()
            If RdB7.Read Then
                XImp = campodbN(RdB7.Item(0))
            End If
            RdB7.Close()

            If XImp = 0 Then
                MySql = "Select sum(Importo) From LegameBudgetRegistrazione Where NumeroRegistrazione <> " & NumeroRegistrazione & " And Anno = " & Anno & " And Livello1 = " & Mastro & " And Livello2 = 0 And Livello3 = 0 And Colonna = " & Colonna
                Dim cmdB8 As New OleDbCommand()
                cmdB8.CommandText = MySql
                cmdB8.Connection = cn
                Dim RdB8 As OleDbDataReader = cmdB8.ExecuteReader()
                If RdB8.Read Then
                    XImp = campodbN(RdB8.Item(0))
                End If
                RdB8.Close()
            End If
        End If
        DisponibilitaBudget = Math.Round(XImporto - (TotImporto + XImp), 2)

        cn.Close()
    End Function

End Class
