Imports Microsoft.VisualBasic
Imports System.Data.OleDb
Imports System.Web.Hosting

Public Class Cls_ExtraFisso
    Public CENTROSERVIZIO As String
    Public CODICEOSPITE As Long
    Public Data As Date
    Public CODICEEXTRA As String
    Public RIPARTIZIONE As String

    Function campodb(ByVal oggetto As Object) As String
        If IsDBNull(oggetto) Then
            Return ""
        Else
            Return oggetto
        End If
    End Function
    Public Sub EliminaAll(ByVal StringaConnessione As String)
        Dim cn As OleDbConnection
        Dim MySql As String

        MySql = ""

        cn = New Data.OleDb.OleDbConnection(StringaConnessione)

        cn.Open()
        Dim cmd As New OleDbCommand()
        cmd.CommandText = "Delete from EXTRAOSPITE where CentroServizio = '" & CENTROSERVIZIO & "' And  CodiceOspite = " & CODICEOSPITE
        cmd.Connection = cn
        cmd.ExecuteNonQuery()
        cn.Close()
    End Sub

    Public Sub Elimina(ByVal StringaConnessione As String)
        Dim cn As OleDbConnection
        Dim MySql As String

        MySql = ""

        cn = New Data.OleDb.OleDbConnection(StringaConnessione)

        cn.Open()
        Dim cmd As New OleDbCommand()
        cmd.CommandText = "Delete from EXTRAOSPITE where CentroServizio = '" & CENTROSERVIZIO & "' And  CodiceOspite = " & CODICEOSPITE & "  And Data = ?"
        cmd.Parameters.AddWithValue("@Data", Data)
        cmd.Connection = cn
        cmd.ExecuteNonQuery()
        cn.Close()
    End Sub


    Public Sub AggiornaDB(ByVal StringaConnessione As String)
        Dim cn As OleDbConnection
        Dim MySql As String

        MySql = ""


        cn = New Data.OleDb.OleDbConnection(StringaConnessione)

        cn.Open()
        Dim cmd As New OleDbCommand()
        cmd.CommandText = ("select * from EXTRAOSPITE where CentroServizio = '" & CENTROSERVIZIO & "' And  CodiceOspite = " & CODICEOSPITE & "  And Data = ?")
        cmd.Parameters.AddWithValue("@Data", Data)
        cmd.Connection = cn
        Dim myPOSTreader As OleDbDataReader = cmd.ExecuteReader()
        If myPOSTreader.Read Then
            MySql = "UPDATE EXTRAOSPITE SET CODICEEXTRA = ?,RIPARTIZIONE=? " & _
                    " WHERE  CentroServizio = '" & CENTROSERVIZIO & "' And  CodiceOspite = " & CODICEOSPITE & " And Data = ?"
            Dim cmdw As New OleDbCommand()
            cmdw.CommandText = (MySql)
            cmdw.Parameters.AddWithValue("@CODICEEXTRA", CODICEEXTRA)
            cmdw.Parameters.AddWithValue("@RIPARTIZIONE", RIPARTIZIONE)
            cmdw.Parameters.AddWithValue("@DATA", Data)
            cmdw.Connection = cn
            cmdw.ExecuteNonQuery()
        Else
            MySql = "INSERT INTO EXTRAOSPITE (CentroServizio,CodiceOspite,Data,CODICEEXTRA,RIPARTIZIONE) VALUES (?,?,?,?,?)"
            Dim cmdw As New OleDbCommand()
            cmdw.CommandText = (MySql)
            cmdw.Parameters.AddWithValue("@CentroServizio", CentroServizio)
            cmdw.Parameters.AddWithValue("@CodiceOspite", CODICEOSPITE)
            cmdw.Parameters.AddWithValue("@DATA", Data)
            cmdw.Parameters.AddWithValue("@CODICEEXTRA", CODICEEXTRA)
            cmdw.Parameters.AddWithValue("@RIPARTIZIONE", RIPARTIZIONE)


            cmdw.Connection = cn
            cmdw.ExecuteNonQuery()
        End If
        myPOSTreader.Close()
        cn.Close()
    End Sub


    Public Sub AggiornaDaTabella(ByVal StringaConnessione As String, ByVal Tabella As System.Data.DataTable)
        Dim cn As OleDbConnection
        Dim MySql As String

        MySql = ""

        cn = New Data.OleDb.OleDbConnection(StringaConnessione)

        cn.Open()


        Dim Transan As OleDbTransaction = cn.BeginTransaction()

        Dim cmd As New OleDbCommand()
        cmd.CommandText = "Delete from EXTRAOSPITE where CentroServizio = '" & CENTROSERVIZIO & "' And  CodiceOspite = " & CODICEOSPITE
        cmd.Connection = cn
        cmd.Transaction = Transan
        cmd.ExecuteNonQuery()

        Dim i As Long

        For i = 0 To Tabella.Rows.Count - 1
            If IsDate(Tabella.Rows(i).Item(0)) And Tabella.Rows(i).Item(0) <> "" Then
                MySql = "INSERT INTO EXTRAOSPITE (CentroServizio,CodiceOspite,Data,CODICEEXTRA,RIPARTIZIONE) VALUES (?,?,?,?,?)"
                Dim cmdw As New OleDbCommand()
                cmdw.CommandText = (MySql)
                cmdw.Parameters.AddWithValue("@CentroServizio", CENTROSERVIZIO)
                cmdw.Parameters.AddWithValue("@CodiceOspite", CODICEOSPITE)
                Dim xData As Date

                xData = Tabella.Rows(i).Item(0)
                cmdw.Parameters.AddWithValue("@Data", xData)
                cmdw.Parameters.AddWithValue("@CODICEEXTRA", Tabella.Rows(i).Item(1))
                cmdw.Parameters.AddWithValue("@RIPARTIZIONE", Tabella.Rows(i).Item(2))
                cmdw.Transaction = Transan
                cmdw.Connection = cn
                cmdw.ExecuteNonQuery()
            End If
        Next
        Transan.Commit()
        cn.Close()

    End Sub
    Function ElencaExtrafissiTesto(ByVal StringaConnessione As String) As String

        If Not IsDate(Data) Then Exit Function
        If Val(CODICEOSPITE) = 0 Then Exit Function
        If Year(Data) < 1990 Then Exit Function

        Dim cn As OleDbConnection


        cn = New Data.OleDb.OleDbConnection(StringaConnessione)

        cn.Open()
        ElencaExtrafissiTesto = ""
        Dim cmd As New OleDbCommand()
        cmd.CommandText = "select * from EXTRAOSPITE where centroservizio = '" & CENTROSERVIZIO & "' And CodiceOspite = " & CODICEOSPITE & " And Data = ?"
        cmd.Parameters.AddWithValue("@Data", Data)
        cmd.Connection = cn
        Dim myPOSTreader As OleDbDataReader = cmd.ExecuteReader()
        Do While myPOSTreader.Read
            Dim DecExtra As New Cls_TipoExtraFisso

            DecExtra.CODICEEXTRA = campodb(myPOSTreader.Item("CODICEEXTRA"))
            DecExtra.Leggi(StringaConnessione)
            ElencaExtrafissiTesto = ElencaExtrafissiTesto & DecExtra.Descrizione & " � " & Format(DecExtra.IMPORTO, "#,##0.00")
        Loop
        myPOSTreader.Close()
        cn.Close()

    End Function
    Function ElencaExtrafissia(ByVal StringaConnessione As String) As String

        Dim cn As OleDbConnection


        cn = New Data.OleDb.OleDbConnection(StringaConnessione)

        cn.Open()
        ElencaExtrafissia = ""
        Dim cmd As New OleDbCommand()
        cmd.CommandText = "select * from EXTRAOSPITE where centroservizio = '" & CENTROSERVIZIO & "' And CodiceOspite = " & CODICEOSPITE & " And Data = ?"
        cmd.Parameters.AddWithValue("@Data", Data)
        cmd.Connection = cn
        Dim myPOSTreader As OleDbDataReader = cmd.ExecuteReader()
        Do While myPOSTreader.Read
            Dim DecExtra As New Cls_TipoExtraFisso

            DecExtra.CODICEEXTRA = campodb(myPOSTreader.Item("CODICEEXTRA"))
            DecExtra.Leggi(StringaConnessione)

            Dim Ripartizione As String = ""
            If campodb(myPOSTreader.Item("RIPARTIZIONE")) = "O" Then
                Ripartizione = "(O)"
            End If
            If campodb(myPOSTreader.Item("RIPARTIZIONE")) = "P" Then
                Ripartizione = "(P)"
            End If
            If campodb(myPOSTreader.Item("RIPARTIZIONE")) = "C" Then
                Ripartizione = "(C)"
            End If

            ElencaExtrafissia = ElencaExtrafissia & DecExtra.Descrizione & " " & DecExtra.DescrizioneInterna & " <b><font codiceiva=""" & campodb(myPOSTreader.Item("CodiceExtra")) & """ color=""#000000"">� " & Format(DecExtra.IMPORTO, "#,##0.00") & "</font> " & DecExtra.TipoImporto & " " & Ripartizione & "</b>|"
        Loop
        myPOSTreader.Close()
        cn.Close()

    End Function
    Public Function CodiceExtraUsato(ByVal StringaConnessione As String, ByVal CodiceExtra As String) As Boolean
        Dim cn As OleDbConnection



        cn = New Data.OleDb.OleDbConnection(StringaConnessione)

        cn.Open()
        Dim cmd As New OleDbCommand()
        cmd.CommandText = ("select * from EXTRAOSPITE where CODICEEXTRA = '" & CodiceExtra & "'")
        cmd.Connection = cn
        CodiceExtraUsato = False

        Dim myPOSTreader As OleDbDataReader = cmd.ExecuteReader()
        If myPOSTreader.Read Then
            CodiceExtraUsato = True
        End If
        myPOSTreader.Close()
        cn.Close()

    End Function

    Sub loaddati(ByVal StringaConnessione As String, ByVal codiceospite As Integer, ByVal centroservizio As String, ByVal Tabella As System.Data.DataTable)
        Dim cn As OleDbConnection



        cn = New Data.OleDb.OleDbConnection(StringaConnessione)

        cn.Open()
        Dim cmd As New OleDbCommand()
        cmd.CommandText = ("select * from EXTRAOSPITE where centroservizio = '" & centroservizio & "' And CodiceOspite = " & codiceospite)
        cmd.Connection = cn
        Dim sb As StringBuilder = New StringBuilder

        Tabella.Clear()
        Tabella.Columns.Add("Data", GetType(String))
        Tabella.Columns.Add("CODICEEXTRA", GetType(String))
        Tabella.Columns.Add("RIPARTIZIONE", GetType(String))

        Dim myPOSTreader As OleDbDataReader = cmd.ExecuteReader()
        Do While myPOSTreader.Read

            Dim myriga As System.Data.DataRow = Tabella.NewRow()
            myriga(0) = Format(myPOSTreader.Item("DATA"), "dd/MM/yyyy")
            myriga(1) = myPOSTreader.Item("CODICEEXTRA")
            myriga(2) = myPOSTreader.Item("RIPARTIZIONE")

            Tabella.Rows.Add(myriga)
        Loop
        myPOSTreader.Close()

        If Tabella.Rows.Count = 0 Then
            Dim myriga As System.Data.DataRow = Tabella.NewRow()
            Tabella.Rows.Add(myriga)
        End If
        cn.Close()
    End Sub


    Public Sub DuplicaExtraAdata(ByVal StringaConnessione As String, ByVal NewData As Date)
        Dim cn As OleDbConnection



        cn = New Data.OleDb.OleDbConnection(StringaConnessione)

        cn.Open()
        Dim cmd As New OleDbCommand()
        cmd.CommandText = ("select * from EXTRAOSPITE where CentroServizio = '" & CENTROSERVIZIO & "' And  CodiceOspite = " & CODICEOSPITE & " And Data = ?")
        cmd.Parameters.AddWithValue("@Data", Data)
        cmd.Connection = cn
        Dim sb As StringBuilder = New StringBuilder

        Dim myPOSTreader As OleDbDataReader = cmd.ExecuteReader()
        Do While myPOSTreader.Read

            CODICEOSPITE = campodb(myPOSTreader.Item("CodiceOspite"))
            CENTROSERVIZIO = campodb(myPOSTreader.Item("CentroServizio"))
            Data = NewData 'campodb(myPOSTreader.Item("data"))
            CODICEEXTRA = campodb(myPOSTreader.Item("CODICEEXTRA"))
            RIPARTIZIONE = campodb(myPOSTreader.Item("RIPARTIZIONE"))

            AggiornaDB_Extra(StringaConnessione)
        Loop
        myPOSTreader.Close()
        cn.Close()

    End Sub





    Public Sub UltimaDataAData(ByVal StringaConnessione As String)
        Dim cn As OleDbConnection
        If CENTROSERVIZIO = "" Then Exit Sub
        If CODICEOSPITE = 0 Then Exit Sub
        If Not IsDate(Data) Then Exit Sub
        If Year(Data) < 1900 Then Exit Sub


        cn = New Data.OleDb.OleDbConnection(StringaConnessione)

        cn.Open()
        Dim cmd As New OleDbCommand()
        cmd.CommandText = ("select * from EXTRAOSPITE where CentroServizio = '" & CENTROSERVIZIO & "' And  CodiceOspite = " & CODICEOSPITE & " And Data = ?")
        cmd.Parameters.AddWithValue("@Data", Data)
        cmd.Connection = cn
        Dim sb As StringBuilder = New StringBuilder

        Dim myPOSTreader As OleDbDataReader = cmd.ExecuteReader()
        If myPOSTreader.Read Then

            CODICEOSPITE = campodb(myPOSTreader.Item("CodiceOspite"))
            CENTROSERVIZIO = campodb(myPOSTreader.Item("CentroServizio"))
            Data = campodb(myPOSTreader.Item("data"))
            CODICEEXTRA = campodb(myPOSTreader.Item("CODICEEXTRA"))
            RIPARTIZIONE = campodb(myPOSTreader.Item("RIPARTIZIONE"))
        End If
        myPOSTreader.Close()
        cn.Close()

    End Sub

    Public Sub UltimaDataAData_Multi(ByVal StringaConnessione As String, ByRef VettoreCodiceExtra() As String, ByRef VettoreRipartizione() As String)
        Dim cn As OleDbConnection
        Dim Indice As Integer = 0
        If CENTROSERVIZIO = "" Then Exit Sub
        If CODICEOSPITE = 0 Then Exit Sub
        If Not IsDate(Data) Then Exit Sub
        If Year(Data) < 1900 Then Exit Sub




        cn = New Data.OleDb.OleDbConnection(StringaConnessione)

        cn.Open()
        Dim cmd As New OleDbCommand()
        cmd.CommandText = ("select * from EXTRAOSPITE where CentroServizio = '" & CENTROSERVIZIO & "' And  CodiceOspite = " & CODICEOSPITE & " And Data = ?")
        cmd.Parameters.AddWithValue("@Data", Data)
        cmd.Connection = cn
        Dim sb As StringBuilder = New StringBuilder

        Dim myPOSTreader As OleDbDataReader = cmd.ExecuteReader()
        Do While myPOSTreader.Read

            VettoreCodiceExtra(Indice) = campodb(myPOSTreader.Item("CODICEEXTRA"))
            VettoreRipartizione(Indice) = campodb(myPOSTreader.Item("RIPARTIZIONE"))
            Indice = Indice + 1
        Loop

        myPOSTreader.Close()
        cn.Close()

    End Sub

    Public Sub UltimaData(ByVal StringaConnessione As String)
        Dim cn As OleDbConnection



        cn = New Data.OleDb.OleDbConnection(StringaConnessione)

        cn.Open()
        Dim cmd As New OleDbCommand()
        cmd.CommandText = ("select * from EXTRAOSPITE where CentroServizio = '" & CentroServizio & "' And  CodiceOspite = " & CodiceOspite & " Order by Data Desc")
        cmd.Connection = cn
        Dim sb As StringBuilder = New StringBuilder

        Dim myPOSTreader As OleDbDataReader = cmd.ExecuteReader()
        If myPOSTreader.Read Then

            CodiceOspite = campodb(myPOSTreader.Item("CodiceOspite"))
            CENTROSERVIZIO = campodb(myPOSTreader.Item("CentroServizio"))
            Data = campodb(myPOSTreader.Item("data"))
            CODICEEXTRA = campodb(myPOSTreader.Item("CODICEEXTRA"))
            RIPARTIZIONE = campodb(myPOSTreader.Item("RIPARTIZIONE"))
        End If
        myPOSTreader.Close()
        cn.Close()

    End Sub


    Public Sub AggiornaDB_Extra(ByVal StringaConnessione As String)
        Dim cn As OleDbConnection
        Dim MySql As String

        MySql = ""


        cn = New Data.OleDb.OleDbConnection(StringaConnessione)

        cn.Open()
        Dim cmd As New OleDbCommand()
        cmd.CommandText = ("select * from EXTRAOSPITE where CentroServizio = '" & CENTROSERVIZIO & "' And  CodiceOspite = " & CODICEOSPITE & "  And Data = ? And CODICEEXTRA = ?")
        cmd.Parameters.AddWithValue("@Data", Data)
        cmd.Parameters.AddWithValue("@CODICEEXTRA", CODICEEXTRA)
        cmd.Connection = cn
        Dim myPOSTreader As OleDbDataReader = cmd.ExecuteReader()
        If myPOSTreader.Read Then
            MySql = "UPDATE EXTRAOSPITE SET RIPARTIZIONE=? " & _
                    " WHERE  CentroServizio = '" & CENTROSERVIZIO & "' And  CodiceOspite = " & CODICEOSPITE & " And Data = ? And CODICEEXTRA = ?"
            Dim cmdw As New OleDbCommand()
            cmdw.CommandText = (MySql)
            cmdw.Parameters.AddWithValue("@RIPARTIZIONE", RIPARTIZIONE)
            cmdw.Parameters.AddWithValue("@DATA", Data)
            cmdw.Parameters.AddWithValue("@CODICEEXTRA", CODICEEXTRA)
            cmdw.Connection = cn
            cmdw.ExecuteNonQuery()
        Else
            MySql = "INSERT INTO EXTRAOSPITE (CentroServizio,CodiceOspite,Data,CODICEEXTRA,RIPARTIZIONE) VALUES (?,?,?,?,?)"
            Dim cmdw As New OleDbCommand()
            cmdw.CommandText = (MySql)
            cmdw.Parameters.AddWithValue("@CentroServizio", CENTROSERVIZIO)
            cmdw.Parameters.AddWithValue("@CodiceOspite", CODICEOSPITE)
            cmdw.Parameters.AddWithValue("@DATA", Data)
            cmdw.Parameters.AddWithValue("@CODICEEXTRA", CODICEEXTRA)
            cmdw.Parameters.AddWithValue("@RIPARTIZIONE", RIPARTIZIONE)


            cmdw.Connection = cn
            cmdw.ExecuteNonQuery()
        End If
        myPOSTreader.Close()
        cn.Close()
    End Sub
End Class
