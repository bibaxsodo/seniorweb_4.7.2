﻿Imports System.Text
''' <summary>
''' Determina il controllo della validità dell'IBAN per italia
''' e San Marino
''' </summary>
Public NotInheritable Class CheckIban
    ' costanti
    Private Const MOD_NUM As Integer = 97
    Private Const STATI As String = "IT,SM"
    Private Const L_IBAN As Integer = 27
    ' membri privati
    Private Shared mIBAN As String = String.Empty

    ' rendiamo privato il costruttore per impedire
    ' l'istanziazione "privata" della classe
    Public Sub New()
    End Sub
    ''' <summary>
    ''' Membro Esposto IBAN
    ''' </summary>
    Public Property IBAN() As String
        Get
            Return mIBAN
        End Get
        Set(value As String)
            mIBAN = value
        End Set
    End Property

    ''' <summary>
    ''' Validazione IBAN
    ''' </summary>
    ''' <param name="pIBAN">Codice IBAN da validare</param>
    ''' <returns>Testo</returns>
    Public Function ValidateIBAN(ByVal pIBAN As String) As String
        Dim retValue As String = String.Empty
        ' trasformiamo in maiuscolo il codice depurato degli spazi
        Dim codiceIBAN As String = pIBAN.Trim().ToUpper()
        ' se si ottiene una stringa vuota sono dolori! :)
        If codiceIBAN = String.Empty Then
            Return "IBAN vuoto"
        End If
        ' Controllo lungehzza
        If codiceIBAN.Length <> L_IBAN Then
            Return String.Format("Verificare lunghezza IBAN dovrebbbe essere {0}", L_IBAN)
        End If
        ' test codice paese
        If Not CheckPaese(codiceIBAN.Substring(0, 2)) Then
            Return "Codice Stato diverso da [IT,SM]"
        End If
        Try
            ' trasformazione in stringa numerica
            Dim codiceNumerico As String = CalcolaCodiceNumerico(codiceIBAN)
            Dim i As Integer
            ' applichiamo il modulo 97 su 6 caratteri alla volta
            While codiceNumerico.Length > 6
                i = Integer.Parse(codiceNumerico.Substring(0, 6)) Mod MOD_NUM
                codiceNumerico = i.ToString() + codiceNumerico.Substring(6)
            End While
            ' la stringa restante con lunghezza <= 6 può essere
            ' convertita agevolmente
            i = Integer.Parse(codiceNumerico) Mod MOD_NUM
            ' se il valore modulo 97 non è uno il check digit
            ' dell'IBAN è errato
            If i <> 1 Then
                retValue = "Codice Errato"
            Else
                retValue = "Codice Corretto"
            End If
        Catch ex As Exception
            retValue = ex.Message
        End Try
        Return retValue
    End Function
    ''' <summary>
    ''' Validazione IBAN
    ''' </summary>
    ''' <returns>Testo</returns>
    Public Function ValidateIBAN() As String
        Return ValidateIBAN(IBAN)
    End Function
    ''' <summary>
    ''' Controllo del codice dello Stato
    ''' </summary>
    ''' <param name="codicePaese"></param>
    ''' <returns>Corretto/Errato</returns>
    Private Function CheckPaese(ByVal codicePaese As String) As Boolean
        Return (STATI.IndexOf(codicePaese) <> -1)
    End Function
    ''' <summary>
    ''' Generazione stringa numerica a partire dal codice IBAN
    ''' Secondo le norme vengono spostati in fondo i primi 4
    ''' caratteri dell'IBAN
    ''' </summary>
    ''' <param name="codiceIBAN"></param>
    ''' <returns></returns>
    Private Function CalcolaCodiceNumerico(ByVal codiceIBAN As String) As String
        Dim sb As New StringBuilder()
        Const lettere As String = "0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZ"
        ' per ogni carattere contenuto nella stringa riformattata per il calcolo
        ' troviamo la posizione relativa nella stringa lettere e
        ' aggiungiamo il valore ottenuto sull'oggetto stringbuilder
        For Each c As Char In codiceIBAN.Substring(4) + codiceIBAN.Substring(0, 4)
            Dim x As Integer = lettere.IndexOf(c)
            If x = -1 Then
                Throw (New Exception("Caratteri non validi nella stringa"))
            End If
            sb.Append(x)
        Next
        Return sb.ToString()
    End Function
End Class
