﻿Imports Microsoft.VisualBasic
Imports System.Data.OleDb
Imports System.Web.Hosting
Imports System.Web.Script.Serialization


Public Class Cls_ReverseCharge

    Public Function Reverse(ByVal NumeroRegistrazione As Integer, ByVal CausaleContabileDoc As String, ByVal CausaleContabileGiroconto As String, ByVal ConnessioneGenerale As String, ByVal ConnessioneTabelle As String) As String

        Dim Imponibile As Double
        Dim Importo As Double
        Dim CodiceIva As String = ""
        Dim DareAvere As String = ""

        Dim MastroCliente As Integer
        Dim ContoCliente As Integer
        Dim SottoContoCliente As Long

        Dim Registrazione As New Cls_MovimentoContabile
        Dim caufv As String


        Registrazione.NumeroRegistrazione = NumeroRegistrazione
        Registrazione.Leggi(ConnessioneGenerale, NumeroRegistrazione)

        Dim Indice As Integer

        For Indice = 0 To Registrazione.Righe.Length - 1
            If IsNothing(Registrazione.Righe(Indice)) Then
            Else
                If Registrazione.Righe(Indice).Tipo = "CF" Then
                    MastroCliente = Registrazione.Righe(Indice).MastroPartita
                    ContoCliente = Registrazione.Righe(Indice).ContoPartita
                    SottoContoCliente = Registrazione.Righe(Indice).SottocontoPartita
                End If
                If Registrazione.Righe(Indice).Tipo = "IV" Then
                    If Registrazione.Righe(Indice).Importo > 0 Then
                        If DareAvere <> Registrazione.Righe(Indice).DareAvere And Trim(DareAvere) <> "" Then
                            Return ("Non posso procedere, la procedura automatica funziona solo iva nello stesso segno")
                            Exit Function
                        End If
                        Imponibile = Imponibile + Registrazione.Righe(Indice).Imponibile
                        Importo = Importo + Registrazione.Righe(Indice).Importo
                        CodiceIva = Registrazione.Righe(Indice).CodiceIVA
                        DareAvere = Registrazione.Righe(Indice).DareAvere
                    End If
                End If
            End If
        Next

        If Importo = 0 Then
            Return ""
            Exit Function
        End If

        Dim NumeroFatVendita As Long

        caufv = CausaleContabileDoc  ' Func_GetChiaveCombo(Cmb_CausaleContabileFV, Vt_CausaleContabileFV)
        NumeroFatVendita = 0

        Dim FatturaEvidente As New Cls_MovimentoContabile
        Dim CausaliContabili As New Cls_CausaleContabile


        FatturaEvidente.NumeroRegistrazione = 0
        FatturaEvidente.Leggi(ConnessioneGenerale, FatturaEvidente.NumeroRegistrazione)

        CausaliContabili.Codice = CausaleContabileDoc
        CausaliContabili.Leggi(ConnessioneTabelle, CausaliContabili.Codice)




        FatturaEvidente.DataRegistrazione = Registrazione.DataRegistrazione
        FatturaEvidente.Competenza = Registrazione.Competenza
        FatturaEvidente.IVASospesa = Registrazione.IVASospesa


        FatturaEvidente.Descrizione = Registrazione.Descrizione

        FatturaEvidente.CodicePagamento = Registrazione.CodicePagamento


        Dim RegIVA As String

        RegIVA = CausaliContabili.RegistroIVA
        FatturaEvidente.RegistroIVA = RegIVA
        If RegIVA > 0 Then

            FatturaEvidente.NumeroProtocollo = FatturaEvidente.MaxProtocollo(ConnessioneGenerale, Year(Registrazione.DataRegistrazione), RegIVA) + 1
        End If



        'MoveToDb(MyRs!NumeroRegistrazione, NumeroFatVendita)



        FatturaEvidente.AnnoProtocollo = Year(Registrazione.DataRegistrazione)


        FatturaEvidente.DataDocumento = Registrazione.DataRegistrazione
        FatturaEvidente.NumeroDocumento = FatturaEvidente.NumeroProtocollo

        FatturaEvidente.CentroServizio = ""
        FatturaEvidente.AnnoCompetenza = 0

        FatturaEvidente.MeseCompetenza = 0
        FatturaEvidente.Tipologia = ""


        FatturaEvidente.PeriodoRiferimentoRettaMese = 0
        FatturaEvidente.PeriodoRiferimentoRettaAnno = 0

        FatturaEvidente.CausaleContabile = caufv
        FatturaEvidente.Utente = "REVERSE"

        FatturaEvidente.RegistrazioneRiferimento = Registrazione.NumeroRegistrazione


        If IsNothing(FatturaEvidente.Righe(0)) Then
            FatturaEvidente.Righe(0) = New Cls_MovimentiContabiliRiga
        End If

        FatturaEvidente.Righe(0).Numero = 0

        FatturaEvidente.Righe(0).RigaDaCausale = 1
        FatturaEvidente.Righe(0).MastroPartita = CausaliContabili.Righe(0).Mastro
        FatturaEvidente.Righe(0).ContoPartita = CausaliContabili.Righe(0).Conto
        FatturaEvidente.Righe(0).SottocontoPartita = CausaliContabili.Righe(0).Sottoconto


        FatturaEvidente.Righe(0).Segno = CausaliContabili.Righe(0).Segno
        FatturaEvidente.Righe(0).DareAvere = CausaliContabili.Righe(0).DareAvere
        FatturaEvidente.Righe(0).Tipo = "CF"
        FatturaEvidente.Righe(0).Importo = Imponibile + Importo
        FatturaEvidente.Righe(0).Descrizione = ""
        FatturaEvidente.Righe(0).Utente = "REVERSE"
        FatturaEvidente.Righe(0).RigaRegistrazione = 1


        If IsNothing(FatturaEvidente.Righe(1)) Then
            FatturaEvidente.Righe(1) = New Cls_MovimentiContabiliRiga
        End If


        FatturaEvidente.Righe(1).Numero = 0

        FatturaEvidente.Righe(1).RigaDaCausale = 2
        FatturaEvidente.Righe(1).MastroPartita = CausaliContabili.Righe(1).Mastro
        FatturaEvidente.Righe(1).ContoPartita = CausaliContabili.Righe(1).Conto
        FatturaEvidente.Righe(1).SottocontoPartita = CausaliContabili.Righe(1).Sottoconto


        FatturaEvidente.Righe(1).CodiceIVA = CodiceIva
        FatturaEvidente.Righe(1).Segno = CausaliContabili.Righe(1).Segno
        FatturaEvidente.Righe(1).DareAvere = CausaliContabili.Righe(1).DareAvere
        FatturaEvidente.Righe(1).Tipo = ""
        FatturaEvidente.Righe(1).Importo = Imponibile
        FatturaEvidente.Righe(1).Descrizione = ""
        FatturaEvidente.Righe(1).Utente = "REVERSE"
        FatturaEvidente.Righe(1).RigaRegistrazione = 2



        Dim RigheIva As Integer = 1

        For Indice = 0 To Registrazione.Righe.Length - 1
            If Not IsNothing(Registrazione.Righe(Indice)) Then
                If Registrazione.Righe(Indice).Tipo = "IV" Then

                    RigheIva = RigheIva + 1
                    If IsNothing(FatturaEvidente.Righe(RigheIva)) Then
                        FatturaEvidente.Righe(RigheIva) = New Cls_MovimentiContabiliRiga
                    End If

                    FatturaEvidente.Righe(RigheIva).Numero = 0

                    FatturaEvidente.Righe(RigheIva).RigaDaCausale = 3
                    FatturaEvidente.Righe(RigheIva).MastroPartita = CausaliContabili.Righe(2).Mastro
                    FatturaEvidente.Righe(RigheIva).ContoPartita = CausaliContabili.Righe(2).Conto
                    FatturaEvidente.Righe(RigheIva).SottocontoPartita = CausaliContabili.Righe(2).Sottoconto


                    FatturaEvidente.Righe(RigheIva).Segno = CausaliContabili.Righe(2).Segno
                    FatturaEvidente.Righe(RigheIva).DareAvere = CausaliContabili.Righe(2).DareAvere
                    FatturaEvidente.Righe(RigheIva).Tipo = "IV"
                    FatturaEvidente.Righe(RigheIva).Importo = Registrazione.Righe(Indice).Importo
                    FatturaEvidente.Righe(RigheIva).Imponibile = Registrazione.Righe(Indice).Imponibile
                    FatturaEvidente.Righe(RigheIva).CodiceIVA = Registrazione.Righe(Indice).CodiceIVA
                    FatturaEvidente.Righe(RigheIva).Detraibile = CausaliContabili.Detraibilita
                    FatturaEvidente.Righe(RigheIva).Descrizione = ""
                    FatturaEvidente.Righe(RigheIva).Utente = "REVERSE"
                    FatturaEvidente.Righe(RigheIva).RigaRegistrazione = 3



                End If
            End If
        Next
        FatturaEvidente.Scrivi(ConnessioneGenerale, 0)

        Registrazione.RegistrazioneRiferimento = FatturaEvidente.NumeroRegistrazione
        Registrazione.Scrivi(ConnessioneGenerale, Registrazione.NumeroRegistrazione)


        REM *******************************************************************************************************************
        REM * giroconto
        REM *******************************************************************************************************************


        Dim caugr As String = CausaleContabileGiroconto
        Dim NumeroGiroconto As Long

        Dim Giroconto As New Cls_MovimentoContabile
        Dim CC_G As New Cls_CausaleContabile


        caugr = CausaleContabileGiroconto
        NumeroGiroconto = 0

        CC_G.Leggi(ConnessioneTabelle, caugr)


        Giroconto.NumeroRegistrazione = 0
        Giroconto.Leggi(ConnessioneGenerale, Giroconto.NumeroRegistrazione)




        Giroconto.DataRegistrazione = Registrazione.DataRegistrazione
        Giroconto.Competenza = Registrazione.Competenza
        Giroconto.IVASospesa = 0
        Giroconto.Descrizione = Registrazione.Descrizione
        Giroconto.CodicePagamento = ""
        Giroconto.RegistroIVA = 0
        Giroconto.NumeroProtocollo = 0

        Giroconto.AnnoProtocollo = 0
        Giroconto.CentroServizio = ""
        Giroconto.AnnoCompetenza = 0
        Giroconto.MeseCompetenza = 0
        Giroconto.Tipologia = ""
        Giroconto.PeriodoRiferimentoRettaMese = 0
        Giroconto.PeriodoRiferimentoRettaAnno = 0
        Giroconto.CausaleContabile = caugr
        Giroconto.Utente = "REVERSE"




        If IsNothing(Giroconto.Righe(0)) Then
            Giroconto.Righe(0) = New Cls_MovimentiContabiliRiga
        End If

        Giroconto.Righe(0).Numero = 0

        Giroconto.Righe(0).RigaDaCausale = 1
        Giroconto.Righe(0).MastroPartita = CC_G.Righe(0).Mastro
        Giroconto.Righe(0).ContoPartita = CC_G.Righe(0).Conto
        Giroconto.Righe(0).SottocontoPartita = CC_G.Righe(0).Sottoconto


        Giroconto.Righe(0).Segno = CC_G.Righe(0).Segno
        Giroconto.Righe(0).DareAvere = CC_G.Righe(0).DareAvere
        Giroconto.Righe(0).Tipo = ""
        Giroconto.Righe(0).Importo = Importo + Imponibile
        Giroconto.Righe(0).Descrizione = ""
        Giroconto.Righe(0).Utente = "REVERSE"
        Giroconto.Righe(0).RigaRegistrazione = 1


        If IsNothing(Giroconto.Righe(1)) Then
            Giroconto.Righe(1) = New Cls_MovimentiContabiliRiga
        End If

        Giroconto.Righe(1).Numero = 0

        Giroconto.Righe(1).RigaDaCausale = 2
        Giroconto.Righe(1).MastroPartita = CC_G.Righe(1).Mastro
        Giroconto.Righe(1).ContoPartita = CC_G.Righe(1).Conto
        Giroconto.Righe(1).SottocontoPartita = CC_G.Righe(1).Sottoconto


        Giroconto.Righe(1).Segno = CC_G.Righe(1).Segno
        Giroconto.Righe(1).DareAvere = CC_G.Righe(1).DareAvere
        Giroconto.Righe(1).Tipo = ""
        Giroconto.Righe(1).Importo = Imponibile
        Giroconto.Righe(1).Descrizione = ""
        Giroconto.Righe(1).Utente = "REVERSE"
        Giroconto.Righe(1).RigaRegistrazione = 2


        If IsNothing(Giroconto.Righe(2)) Then
            Giroconto.Righe(2) = New Cls_MovimentiContabiliRiga
        End If


        Giroconto.Righe(2).Numero = 0

        Giroconto.Righe(2).RigaDaCausale = 3
        Giroconto.Righe(2).MastroPartita = MastroCliente
        Giroconto.Righe(2).ContoPartita = ContoCliente
        Giroconto.Righe(2).SottocontoPartita = SottoContoCliente


        Giroconto.Righe(2).Segno = CC_G.Righe(2).Segno
        Giroconto.Righe(2).DareAvere = CC_G.Righe(2).DareAvere
        Giroconto.Righe(2).Tipo = ""
        Giroconto.Righe(2).Importo = Importo
        Giroconto.Righe(2).Descrizione = ""
        Giroconto.Righe(2).Utente = "REVERSE"
        Giroconto.Righe(2).RigaRegistrazione = 3

        Giroconto.Scrivi(ConnessioneGenerale, 0)


        Dim Legami As New Cls_Legami


        Legami.NumeroDocumento(0) = Registrazione.NumeroRegistrazione
        Legami.NumeroPagamento(0) = Giroconto.NumeroRegistrazione
        Legami.Importo(0) = Importo

        Legami.Scrivi(ConnessioneGenerale, Registrazione.NumeroRegistrazione, Giroconto.NumeroRegistrazione)


        REM Aggiorna Scadenziario
        AggiornaScadenze(Registrazione, Imponibile, ConnessioneGenerale, ConnessioneTabelle)


        Return "Fattura Vendita " & FatturaEvidente.NumeroRegistrazione & " e Giroconto Creato " & Giroconto.NumeroRegistrazione
    End Function

    Private Sub AggiornaScadenze(ByVal x As Cls_MovimentoContabile, ByVal ImportoNetto As Double, ByVal ConnessioneGenerale As String, ByVal ConnessioneTabelle As String)
        Dim XS As New Cls_Scadenziario
        Dim DataScadenza As New Date
        Dim xModalita As New Cls_TipoPagamento
        Dim CausaleContabile As New Cls_CausaleContabile


        EliminaScadenza(x.NumeroRegistrazione, ConnessioneGenerale)


        XS.NumeroRegistrazioneContabile = x.NumeroRegistrazione

        xModalita.Codice = x.ModalitaPagamento
        xModalita.Leggi(ConnessioneTabelle)



        CausaleContabile.Codice = x.CausaleContabile
        CausaleContabile.Leggi(ConnessioneTabelle, CausaleContabile.Codice)


        If Year(x.DataDocumento) < 1900 Then
            DataScadenza = DateAdd(DateInterval.Day, xModalita.GiorniPrima, x.DataDocumento)
            If xModalita.GiorniPrima > 59 And Month(DataScadenza) And Day(DataScadenza) And xModalita.GiorniPrima < 5 And xModalita.Tipo = "F" Then
                DataScadenza = DateAdd(DateInterval.Day, xModalita.GiorniPrima, x.DataDocumento)
            End If
            If xModalita.Tipo = "F" Then
                If Month(x.DataRegistrazione) = 1 And xModalita.GiorniPrima >= 30 And xModalita.GiorniPrima <= 40 Then
                    DataScadenza = DateSerial(Year(DataScadenza), 2, GiorniMese(2, Year(DataScadenza)))
                Else
                    DataScadenza = DateSerial(Year(DataScadenza), Month(DataScadenza), GiorniMese(Month(DataScadenza), Year(DataScadenza)))
                End If
            End If
        Else
            DataScadenza = DateAdd(DateInterval.Day, xModalita.GiorniPrima, x.DataDocumento)
            If xModalita.GiorniPrima > 59 And Month(DataScadenza) And Day(DataScadenza) And xModalita.GiorniPrima < 5 And xModalita.Tipo = "F" Then
                DataScadenza = DateAdd(DateInterval.Day, xModalita.GiorniPrima, x.DataDocumento)
            End If
            If xModalita.Tipo = "F" Then
                If Month(x.DataDocumento) = 1 And xModalita.GiorniPrima >= 30 And xModalita.GiorniPrima <= 40 Then
                    DataScadenza = DateSerial(Year(DataScadenza), 2, GiorniMese(2, Year(DataScadenza)))
                Else
                    DataScadenza = DateSerial(Year(DataScadenza), Month(DataScadenza), GiorniMese(Month(DataScadenza), Year(DataScadenza)))
                End If
            End If
        End If

        XS.DataScadenza = DataScadenza
        Dim ImportoScadenza As Double = 0

        If xModalita.Scadenze > 0 Then
            XS.Importo = Modulo.MathRound(ImportoNetto / xModalita.Scadenze, 2)

            ImportoScadenza = Modulo.MathRound(ImportoNetto / xModalita.Scadenze, 2)
            XS.Chiusa = 0
            XS.Descrizione = ""
            If CausaleContabile.TipoDocumento = "NC" Then
                XS.Importo = XS.Importo * -1
            End If
            XS.ScriviScadenza(ConnessioneGenerale)
        End If

        If xModalita.Scadenze > 1 Then
            Dim XS1 As New Cls_Scadenziario
            XS1.NumeroRegistrazioneContabile = x.NumeroRegistrazione
            If ImportoScadenza + Modulo.MathRound(ImportoNetto / xModalita.Scadenze, 2) > ImportoNetto Then
                XS1.Importo = Modulo.MathRound(ImportoNetto - ImportoScadenza, 2)
                ImportoScadenza = ImportoScadenza + XS1.Importo
            Else
                XS1.Importo = Modulo.MathRound(ImportoNetto / xModalita.Scadenze, 2)
                ImportoScadenza = ImportoScadenza + Modulo.MathRound(ImportoNetto / xModalita.Scadenze, 2)
            End If



            DataScadenza = DateAdd("d", xModalita.GiorniSeconda + xModalita.GiorniPrima, x.DataDocumento)
            If xModalita.Tipo = "F" Then DataScadenza = DateSerial(Year(DataScadenza), Month(DataScadenza), GiorniMese(Month(DataScadenza), Year(DataScadenza)))
            XS1.DataScadenza = DataScadenza
            If CausaleContabile.TipoDocumento = "NC" Then
                XS.Importo = XS.Importo * -1
            End If
            XS1.Chiusa = 0
            XS1.Descrizione = ""
            XS1.ScriviScadenza(ConnessioneGenerale)
        End If

        If xModalita.Scadenze > 2 Then
            Dim GIORNIDA As Long
            GIORNIDA = xModalita.GiorniSeconda + xModalita.GiorniPrima
            For i = 3 To xModalita.Scadenze
                GIORNIDA = GIORNIDA + xModalita.GiorniAltre
                Dim XS1 As New Cls_Scadenziario
                XS1.NumeroRegistrazioneContabile = x.NumeroRegistrazione
                DataScadenza = DateAdd("d", GIORNIDA, x.DataDocumento)
                If xModalita.Tipo = "F" Then DataScadenza = DateSerial(Year(DataScadenza), Month(DataScadenza), GiorniMese(Month(DataScadenza), Year(DataScadenza)))
                XS1.DataScadenza = DataScadenza

                If ImportoScadenza + Modulo.MathRound(ImportoNetto / xModalita.Scadenze, 2) > ImportoNetto Then
                    XS1.Importo = Modulo.MathRound(ImportoNetto - ImportoScadenza, 2)
                    ImportoScadenza = ImportoScadenza + XS1.Importo
                Else
                    XS1.Importo = Modulo.MathRound(ImportoNetto / xModalita.Scadenze, 2)
                    ImportoScadenza = ImportoScadenza + Modulo.MathRound(ImportoNetto / xModalita.Scadenze, 2)
                End If
                'XS1.Importo = Modulo.MathRound(ImportoNetto / xModalita.Scadenze, 2)
                If CausaleContabile.TipoDocumento = "NC" Then
                    XS.Importo = XS.Importo * -1
                End If
                XS1.Chiusa = 0
                XS1.Descrizione = ""
                XS1.ScriviScadenza(ConnessioneGenerale)
            Next i
        End If

    End Sub


    Private Sub EliminaScadenza(ByVal Numero As Integer, ByVal ConnessioneGenerale As String)
        Dim MySql As String
        Dim cn As OleDbConnection

        cn = New Data.OleDb.OleDbConnection(ConnessioneGenerale)

        cn.Open()

        Dim P As New Cls_Scadenziario

        P.NumeroRegistrazioneContabile = Numero
        P.Leggi(ConnessioneGenerale)



        MySql = "Delete From Scadenzario Where NumeroRegistrazioneContabile = ?"

        Dim cmd As New OleDbCommand()

        cmd.CommandText = MySql
        cmd.Parameters.AddWithValue("@NumeroRegistrazioneContabile", Numero)
        cmd.Connection = cn
        cmd.ExecuteNonQuery()



        cn.Close()
    End Sub
    Public Function GiorniMese(ByVal Mese As Byte, ByVal Anno As Integer) As Byte
        If Mese = 1 Or Mese = 3 Or Mese = 5 Or Mese = 7 Or Mese = 8 Or _
           Mese = 10 Or Mese = 12 Then
            GiorniMese = 31
        Else
            If Mese <> 2 Then
                GiorniMese = 30
            Else
                If Day(DateSerial(Anno, Mese, 29)) = 29 Then
                    GiorniMese = 29
                Else
                    GiorniMese = 28
                End If
            End If
        End If
    End Function
End Class
