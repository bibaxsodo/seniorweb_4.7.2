Imports Microsoft.VisualBasic
Imports System.Data.OleDb
Imports System.Web.Hosting

Public Class Cls_Pianodeiconti
    Public Mastro As Long
    Public Conto As Long
    Public Sottoconto As Long
    Public Descrizione As String
    Public Tipo As String
    Public Sezione As String
    Public Classe As String
    Public TipoBene As String
    Public TipoAnagrafica As String
    Public EntrataUscita As String    
    Public NonInUso As String
    Public Commento As String
    Public ContoDiAmmortamento As Long

    Function campodb(ByVal oggetto As Object) As String
        If IsDBNull(oggetto) Then
            Return ""
        Else
            Return oggetto
        End If
    End Function
    Sub New()
        Mastro = 0
        Conto = 0
        Sottoconto = 0
        Descrizione = ""
        Tipo = ""
        Sezione = ""
        Classe = ""
        TipoBene = ""
        TipoAnagrafica = ""
        EntrataUscita = ""
        NonInUso = ""
        Commento = ""
        ContoDiAmmortamento = 0
    End Sub

    Sub Elimina(ByVal StringaConnessione As String)
        Dim cn As OleDbConnection

        Descrizione = ""
        If IsNothing(Mastro) Or IsNothing(Conto) Or IsNothing(Sottoconto) Then
            Exit Sub
        End If

        cn = New Data.OleDb.OleDbConnection(StringaConnessione)

        cn.Open()
        Dim cmd As New OleDbCommand()
        cmd.CommandText = ("Delete from PianoConti where " & _
                           "Mastro = ? And Conto = ? And SottoConto = ?")

        cmd.Parameters.AddWithValue("@Mastro", Mastro)
        cmd.Parameters.AddWithValue("@Conto", Conto)
        cmd.Parameters.AddWithValue("@Sottoconto", Sottoconto)
        cmd.Connection = cn
        cmd.ExecuteNonQuery()
              
        cn.Close()
    End Sub


    Function MaxSottoconto(ByVal StringaConnessione As String) As Long
        Dim cn As OleDbConnection

        MaxSottoconto = 0
        If IsNothing(Mastro) Or IsNothing(Conto) Or IsNothing(Sottoconto) Then
            Exit Function
        End If

        cn = New Data.OleDb.OleDbConnection(StringaConnessione)

        cn.Open()
        Dim cmd As New OleDbCommand()
        cmd.CommandText = ("select max(Sottoconto) as Totale from PianoConti where " & _
                           "Mastro = ? And Conto = ? ")

        cmd.Parameters.AddWithValue("@Mastro", Mastro)
        cmd.Parameters.AddWithValue("@Conto", Conto)
        cmd.Connection = cn
        Dim sb As StringBuilder = New StringBuilder

        Dim myPOSTreader As OleDbDataReader = cmd.ExecuteReader()
        If myPOSTreader.Read Then
            MaxSottoconto = Val(campodb(myPOSTreader.Item("Totale"))) + 1
        End If
        myPOSTreader.Close()
        cn.Close()

    End Function
    Sub Decodfica(ByVal StringaConnessione As String)
        Dim cn As OleDbConnection

        Descrizione = ""
        If IsNothing(Mastro) Or IsNothing(Conto) Or IsNothing(Sottoconto) Then
            Exit Sub
        End If

        cn = New Data.OleDb.OleDbConnection(StringaConnessione)

        cn.Open()
        Dim cmd As New OleDbCommand()
        cmd.CommandText = ("select * from PianoConti where " & _
                           "Mastro = ? And Conto = ? And SottoConto = ?")

        cmd.Parameters.AddWithValue("@Mastro", Mastro)
        cmd.Parameters.AddWithValue("@Conto", Conto)
        cmd.Parameters.AddWithValue("@Sottoconto", Sottoconto)
        cmd.Connection = cn
        Dim sb As StringBuilder = New StringBuilder

        Dim myPOSTreader As OleDbDataReader = cmd.ExecuteReader()
        If myPOSTreader.Read Then
            Descrizione = campodb(myPOSTreader.Item("Descrizione"))
            Tipo = campodb(myPOSTreader.Item("Tipo"))
            Sezione = campodb(myPOSTreader.Item("Sezione"))
            Classe = campodb(myPOSTreader.Item("Classe"))
            TipoBene = campodb(myPOSTreader.Item("TipoBene"))
            TipoAnagrafica = campodb(myPOSTreader.Item("TipoAnagrafica"))
            EntrataUscita = campodb(myPOSTreader.Item("EntrataUscita"))
            NonInUso = campodb(myPOSTreader.Item("NonInUso"))
            Commento = campodb(myPOSTreader.Item("Commento"))
            ContoDiAmmortamento = Val(campodb(myPOSTreader.Item("ContoDiAmmortamento")))
        End If
        myPOSTreader.Close()
        cn.Close()
    End Sub

    Sub Scrivi(ByVal StringaConnessione As String)
        Dim cn As OleDbConnection
        Dim MySql As String

        Dim Modifica As Boolean = False

        cn = New Data.OleDb.OleDbConnection(StringaConnessione)

        cn.Open()
        Dim cmd As New OleDbCommand()
        cmd.CommandText = ("select * from PianoConti where " & _
                           "Mastro = ? And Conto = ? And SottoConto = ?")

        cmd.Parameters.AddWithValue("@Mastro", Mastro)
        cmd.Parameters.AddWithValue("@Conto", Conto)
        cmd.Parameters.AddWithValue("@Sottoconto", Sottoconto)
        cmd.Connection = cn
        Dim sb As StringBuilder = New StringBuilder

        Dim myPOSTreader As OleDbDataReader = cmd.ExecuteReader()
        If myPOSTreader.Read Then
            Modifica = True
        End If
        myPOSTreader.Close()

        If Modifica = False Then
            Dim APPOGGIO As String = ""
            Dim I As Integer
            For I = 1 To 10
                APPOGGIO = APPOGGIO & Int(Rnd(1) * 10)
            Next
            Dim cmdIns As New OleDbCommand()
            MySql = "INSERT INTO PianoConti (Mastro,Conto,SottoConto) VALUES (?,?,?)"
            cmdIns.CommandText = MySql
            cmdIns.Parameters.AddWithValue("@Mastro", Mastro)
            cmdIns.Parameters.AddWithValue("@Conto", Conto)
            cmdIns.Parameters.AddWithValue("@SottoConto", Sottoconto)
            cmdIns.Connection = cn
            cmdIns.ExecuteNonQuery()
        End If

        MySql = "UPDATE PianoConti SET " & _
                "Descrizione = ?, " & _
                "Tipo = ?, " & _
                "Sezione = ?, " & _
                "Classe = ?, " & _
                "TipoBene = ?, " & _
                "TipoAnagrafica = ?, " & _
                "EntrataUscita = ?, " & _
                "NonInUso = ?, " & _
                "Commento = ?, " & _
                "ContoDiAmmortamento = ?" & _
                " Where Mastro = ? And Conto = ? And Sottoconto = ?"
        Dim cmd1 As New OleDbCommand()
        cmd1.Parameters.AddWithValue("@Descrizione", Descrizione)
        cmd1.Parameters.AddWithValue("@Tipo", Tipo)
        cmd1.Parameters.AddWithValue("@Sezione", Sezione)
        cmd1.Parameters.AddWithValue("@Classe", Classe)
        cmd1.Parameters.AddWithValue("@TipoBene", TipoBene)
        cmd1.Parameters.AddWithValue("@TipoAnagrafica", TipoAnagrafica)
        cmd1.Parameters.AddWithValue("@EntrataUscita", EntrataUscita)
        cmd1.Parameters.AddWithValue("@NonInUso", NonInUso)
        cmd1.Parameters.AddWithValue("@Commento", Commento)
        cmd1.Parameters.AddWithValue("@ContoDiAmmortamento", ContoDiAmmortamento)
        cmd1.Parameters.AddWithValue("@Mastro", Mastro)
        cmd1.Parameters.AddWithValue("@Conto", Conto)
        cmd1.Parameters.AddWithValue("@Sottoconto", Sottoconto)
        cmd1.CommandText = MySql
        cmd1.Connection = cn
        cmd1.ExecuteNonQuery()

        cn.Close()
    End Sub

End Class
