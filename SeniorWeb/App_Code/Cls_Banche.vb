﻿Imports Microsoft.VisualBasic
Imports System.Data.OleDb
Imports System.Web.Hosting

Public Class Cls_Banche
    Public Codice As String
    Public Descrizione As String
    Public Agenzia As String
    Public Indirizzo As String
    Public Provincia As String
    Public Comune As String
    Public Cap As String
    Public IBANRID As String
    Public EndToEndId As String
    Public PrvtId As String
    Public MmbId As String
    Public OrgId As String

    Function campodbn(ByVal oggetto As Object) As Double
        If IsDBNull(oggetto) Then
            Return 0
        Else
            Return oggetto
        End If
    End Function


    Function campodb(ByVal oggetto As Object) As String
        If IsDBNull(oggetto) Then
            Return ""
        Else
            Return oggetto
        End If
    End Function



    Sub UpDateDropBox(ByVal StringaConnessione As String, ByRef appoggio As DropDownList)
        Dim cn As OleDbConnection



        cn = New Data.OleDb.OleDbConnection(StringaConnessione)

        cn.Open()
        Dim cmd As New OleDbCommand()
        cmd.CommandText = ("select * from Banche")
        cmd.Connection = cn
        Dim sb As StringBuilder = New StringBuilder

        appoggio.Items.Clear()
        Dim myPOSTreader As OleDbDataReader = cmd.ExecuteReader()
        Do While myPOSTreader.Read
            appoggio.Items.Add(myPOSTreader.Item("Descrizione"))
            appoggio.Items(appoggio.Items.Count - 1).Value = myPOSTreader.Item("Codice")
        Loop
        myPOSTreader.Close()
        cn.Close()
        appoggio.Items.Add("")
        appoggio.Items(appoggio.Items.Count - 1).Value = ""
        appoggio.Items(appoggio.Items.Count - 1).Selected = True

    End Sub

    Sub Delete(ByVal StringaConnessione As String)
        Dim cn As OleDbConnection



        cn = New Data.OleDb.OleDbConnection(StringaConnessione)

        cn.Open()
        Dim cmd As New OleDbCommand()
        cmd.CommandText = ("Delete from Banche where " & _
                           "CODICE = ? ")
        cmd.Parameters.AddWithValue("@CODICE", Codice)
        cmd.Connection = cn
        cmd.ExecuteNonQuery()
        cn.Close()
    End Sub

    Sub Scrivi(ByVal StringaConnessione As String)
        Dim cn As OleDbConnection



        cn = New Data.OleDb.OleDbConnection(StringaConnessione)

        cn.Open()
        Dim cmd As New OleDbCommand()
        cmd.CommandText = ("select * from Banche where " & _
                           "CODICE = ? ")
        cmd.Parameters.AddWithValue("@CODICE", Codice)
        cmd.Connection = cn
        Dim myPOSTreader As OleDbDataReader = cmd.ExecuteReader()
        If Not myPOSTreader.Read Then
            Dim XSql As String
            XSql = "INSERT INTO Banche (CODICE,DESCRIZIONE) values (?,?)"
            Dim cmdIns As New OleDbCommand()
            cmdIns.CommandText = XSql
            cmdIns.Parameters.AddWithValue("@Codice", Codice)
            cmdIns.Parameters.AddWithValue("@Descrizione", Descrizione)
            cmdIns.Connection = cn
            cmdIns.ExecuteNonQuery()
        End If
        myPOSTreader.Close()

        Dim MySql As String

        MySql = "UPDATE Banche SET " & _
                "  Descrizione = ?," & _
                "  Agenzia  = ?," & _
                "  Indirizzo  = ?," & _
                "  Provincia  = ?," & _
                "  Comune  = ?," & _
                "  Cap  = ?," & _
                "  IBANRID  = ?," & _
                "  EndToEndId  = ?," & _
                "  PrvtId  = ?," & _
                "  MmbId  = ?," & _
                "  OrgId  = ? " & _
                " Where Codice = ? "
        Dim cmd1 As New OleDbCommand()
        cmd1.Connection = cn
        cmd1.CommandText = MySql
        cmd1.Parameters.AddWithValue("@Descrizione", Descrizione)
        cmd1.Parameters.AddWithValue("@Agenzia", Agenzia)
        cmd1.Parameters.AddWithValue("@Indirizzo", Indirizzo)
        cmd1.Parameters.AddWithValue("@Provincia", Provincia)
        cmd1.Parameters.AddWithValue("@Comune", Comune)
        cmd1.Parameters.AddWithValue("@Cap", Cap)
        cmd1.Parameters.AddWithValue("@IBANRID", IBANRID)
        cmd1.Parameters.AddWithValue("@EndToEndId", EndToEndId)
        cmd1.Parameters.AddWithValue("@PrvtId", PrvtId)
        cmd1.Parameters.AddWithValue("@MmbId", MmbId)
        cmd1.Parameters.AddWithValue("@OrgId", OrgId)
        cmd1.Parameters.AddWithValue("@Codice", Codice)
        cmd1.ExecuteNonQuery()

        cn.Close()
    End Sub

    Sub Leggi(ByVal StringaConnessione As String, ByVal XCodice As String)
        Dim cn As OleDbConnection



        cn = New Data.OleDb.OleDbConnection(StringaConnessione)

        cn.Open()
        Dim cmd As New OleDbCommand()
        cmd.CommandText = ("select * from Banche where " & _
                           " Codice = ?")
        cmd.Parameters.AddWithValue("@Codice", XCodice)
        cmd.Connection = cn
        Dim sb As StringBuilder = New StringBuilder

        Dim myPOSTreader As OleDbDataReader = cmd.ExecuteReader()
        If myPOSTreader.Read Then
            Codice = campodb(myPOSTreader.Item("Codice"))
            Descrizione = campodb(myPOSTreader.Item("Descrizione"))
            Agenzia = campodb(myPOSTreader.Item("Agenzia"))
            Indirizzo = campodb(myPOSTreader.Item("Indirizzo"))
            Provincia = campodb(myPOSTreader.Item("Provincia"))
            Comune = campodb(myPOSTreader.Item("Comune"))
            Cap = campodb(myPOSTreader.Item("Cap"))
            IBANRID = campodb(myPOSTreader.Item("IBANRID"))
            EndToEndId = campodb(myPOSTreader.Item("EndToEndId"))
            PrvtId = campodb(myPOSTreader.Item("PrvtId"))
            MmbId = campodb(myPOSTreader.Item("MmbId"))
            OrgId = campodb(myPOSTreader.Item("OrgId"))
        End If
        cn.Close()
    End Sub


    Function MaxBanche(ByVal StringaConnessione As String) As String
        Dim cn As OleDbConnection


        MaxBanche = ""

        cn = New Data.OleDb.OleDbConnection(StringaConnessione)

        cn.Open()
        Dim cmd As New OleDbCommand()
        cmd.CommandText = ("select max(CASE WHEN (len(Codice) = 1) THEN '0' + CODICE ELSE Codice END) from Banche ")
        cmd.Connection = cn
        Dim MaxAddebito As OleDbDataReader = cmd.ExecuteReader()
        If MaxAddebito.Read Then
            Dim MassimoLetto As String

            MassimoLetto = campodb(MaxAddebito.Item(0))

            If MassimoLetto = "Z9" Then
                MassimoLetto = "a0"
                cn.Close()
                Return MassimoLetto
            End If

            If MassimoLetto = "99" Then
                MassimoLetto = "A0"
            Else
                If Mid(MassimoLetto & Space(10), 1, 1) > "0" And Mid(MassimoLetto & Space(10), 1, 1) > "9" Then
                    If Mid(MassimoLetto & Space(10), 2, 1) < "9" Then
                        MassimoLetto = Mid(MassimoLetto & Space(10), 1, 1) & Val(Mid(MassimoLetto & Space(10), 2, 1)) + 1
                    Else
                        Dim CodiceAscii As String
                        CodiceAscii = Asc(Mid(MassimoLetto & Space(10), 1, 1))

                        MassimoLetto = Chr(CodiceAscii + 1) & "0"
                    End If
                Else
                    MassimoLetto = Format(Val(MassimoLetto) + 1, "00")
                End If
            End If

            Return MassimoLetto
        Else
            Return "01"
        End If
        cn.Close()

    End Function
End Class
