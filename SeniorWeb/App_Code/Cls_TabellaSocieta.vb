﻿Imports Microsoft.VisualBasic
Imports System.Data.OleDb
Imports System.Web.Hosting


Public Class Cls_TabellaSocieta
    Public RagioneSociale As String
    Public Indirizzo As String
    Public Cap As String
    Public Localita As String
    Public Provincia As String
    Public Attivita As String
    Public CodiceFiscale As String
    Public PartitaIVA As String
    Public Telefono As String
    Public EMail As String
    'Public ABI As String
    'Public CAB As String
    'Public ContoCorrente As String
    Public Commento As String

    Public IBAN As String
    Public BANCA As String
    Public CodiceInstallazione As String
    Public CodiceDitta As String
    Public NomeFile As String
    Public IBANRID As String
    Public EndToEndId As String
    Public PrvtId As String
    Public MmbId As String
    Public OrgId As String

    Public CodiceMittente As String
    Public CodiceRicevente As String
    Public CODICEABI As String
    Public CODICECAB As String
    Public Conto As String


    Public BOLLOVIRTUALE As Integer
    Public REGIMEFISCALE As String

    Sub Leggi(ByVal StringaConnessione As String)
        Dim cn As OleDbConnection

        cn = New Data.OleDb.OleDbConnection(StringaConnessione)

        cn.Open()
        Dim cmd As New OleDbCommand()
        cmd.CommandText = ("select * from Societa ")
        cmd.Connection = cn        
        Dim myPOSTreader As OleDbDataReader = cmd.ExecuteReader()
        If myPOSTreader.Read Then
            RagioneSociale = campodb(myPOSTreader.Item("RagioneSociale"))
            Indirizzo = campodb(myPOSTreader.Item("Indirizzo"))
            Cap = campodb(myPOSTreader.Item("Cap"))



            Localita = campodb(myPOSTreader.Item("Localita"))
            Provincia = campodb(myPOSTreader.Item("Provincia"))
            Attivita = campodb(myPOSTreader.Item("Attivita"))
            CodiceFiscale = campodb(myPOSTreader.Item("CodiceFiscale"))
            PartitaIVA = campodb(myPOSTreader.Item("PartitaIVA"))

            Telefono = campodb(myPOSTreader.Item("Telefono"))           

            EMail = campodb(myPOSTreader.Item("EMail"))

            'ABI = campodb(myPOSTreader.Item("ABI"))
            'CAB = campodb(myPOSTreader.Item("CAB"))
            'ContoCorrente = campodb(myPOSTreader.Item("ContoCorrente"))
            Commento = campodb(myPOSTreader.Item("Commento"))
            IBAN = campodb(myPOSTreader.Item("IBAN"))
            BANCA = campodb(myPOSTreader.Item("BANCA"))
            CodiceInstallazione = campodb(myPOSTreader.Item("CodiceInstallazione"))
            CodiceDitta = campodb(myPOSTreader.Item("CodiceDitta"))

            NomeFile = campodb(myPOSTreader.Item("NomeFile"))




            IBANRID = campodb(myPOSTreader.Item("IBANRID"))
            EndToEndId = campodb(myPOSTreader.Item("EndToEndId"))
            PrvtId = campodb(myPOSTreader.Item("PrvtId"))
            MmbId = campodb(myPOSTreader.Item("MmbId"))
            OrgId = campodb(myPOSTreader.Item("OrgId"))



            CodiceMittente = campodb(myPOSTreader.Item("CodiceMittente"))
            CodiceRicevente = campodb(myPOSTreader.Item("CodiceRicevente"))
            CODICEABI = campodb(myPOSTreader.Item("CODICEABI"))
            CODICECAB = campodb(myPOSTreader.Item("CODICECAB"))
            Conto = campodb(myPOSTreader.Item("Conto"))



            BOLLOVIRTUALE = Val(campodb(myPOSTreader.Item("BOLLOVIRTUALE")))
            REGIMEFISCALE = campodb(myPOSTreader.Item("REGIMEFISCALE"))
        End If
        myPOSTreader.Close()
        cn.Close()
    End Sub



    Function campodbD(ByVal oggetto As Object) As Date
        If IsDBNull(oggetto) Then
            Return Nothing
        Else
            Return oggetto
        End If
    End Function


    Function DecodificaSocieta(ByVal ConnectionString As String) As String
        Dim cn As OleDbConnection

        cn = New Data.OleDb.OleDbConnection(ConnectionString)

        DecodificaSocieta = ""

        cn.Open()
        Dim cmd As New OleDbCommand()
        cmd.CommandText = ("Select * From Societa ")
        cmd.Connection = cn
        Dim myPOSTreader As OleDbDataReader = cmd.ExecuteReader()
        If myPOSTreader.Read Then
            RagioneSociale = campodb(myPOSTreader.Item("RagioneSociale"))
            Indirizzo = campodb(myPOSTreader.Item("Indirizzo"))
            Cap = campodb(myPOSTreader.Item("Cap"))

            Localita = campodb(myPOSTreader.Item("Localita"))
            Provincia = campodb(myPOSTreader.Item("Provincia"))
            CodiceFiscale = campodb(myPOSTreader.Item("CodiceFiscale"))
            PartitaIVA = campodb(myPOSTreader.Item("PartitaIVA"))

            DecodificaSocieta = RagioneSociale & "  " & Indirizzo & "  " & Format(Cap, "00000") & "  " & Localita & "  " & Provincia
            If CodiceFiscale <> "" Then
                DecodificaSocieta = DecodificaSocieta & "  C.F. " & CodiceFiscale
            End If
            If PartitaIVA <> "" Then
                DecodificaSocieta = DecodificaSocieta & "  P.I. " & Format(PartitaIVA, "00000000000")
            End If
        End If
        myPOSTreader.Close()
        cn.Close()

    End Function

    Function campodb(ByVal oggetto As Object) As String
        If IsDBNull(oggetto) Then
            Return ""
        Else
            Return oggetto
        End If
    End Function



    Sub Scrivi(ByVal StringaConnessione As String)
        Dim cn As OleDbConnection


        cn = New Data.OleDb.OleDbConnection(StringaConnessione)

        cn.Open()

        '            CodiceInstallazione = campodb(myPOSTreader.Item("CodiceInstallazione"))
        'CodiceDitta = campodb(myPOSTreader.Item("CodiceDitta"))

        Dim cmd As New OleDbCommand()
        cmd.CommandText = ("UPDATE Societa SET   RagioneSociale = ?, Indirizzo = ?, Cap = ?, Localita = ?, Provincia= ?, Attivita = ?, CodiceFiscale = ?, PartitaIVA = ?, Telefono = ?,  Commento = ?,IBAN= ?, BANCA = ?,CodiceInstallazione = ?, CodiceDitta =?,NomeFile = ?,IBANRID = ?,EndToEndId = ?,PrvtId  = ?,MmbId = ?,OrgId = ?,CodiceMittente =?, CodiceRicevente = ?,CODICEABI = ?, CODICECAB =?,Conto = ?,Email = ?,BOLLOVIRTUALE = ?,REGIMEFISCALE =? ")




        If RagioneSociale.Length() > 99 Then
            RagioneSociale = Mid(RagioneSociale, 1, 99)
        End If
        If Indirizzo.Length() > 50 Then
            Indirizzo = Mid(Indirizzo, 1, 50)
        End If

        cmd.Parameters.AddWithValue("@RagioneSociale", RagioneSociale)
        cmd.Parameters.AddWithValue("@Indirizzo", Indirizzo)
        cmd.Parameters.AddWithValue("@Cap", Cap)
        cmd.Parameters.AddWithValue("@Localita", Localita)
        cmd.Parameters.AddWithValue("@Provincia", Provincia)
        cmd.Parameters.AddWithValue("@Attivita", Attivita)
        cmd.Parameters.AddWithValue("@CodiceFiscale", CodiceFiscale)
        cmd.Parameters.AddWithValue("@PartitaIVA", PartitaIVA)
        cmd.Parameters.AddWithValue("@Telefono", Telefono)


        'cmd.Parameters.AddWithValue("@ABI", ABI)
        'cmd.Parameters.AddWithValue("@CAB", CAB)
        'cmd.Parameters.AddWithValue("@ContoCorrente", ContoCorrente)
        cmd.Parameters.AddWithValue("@Commento", Commento)
        cmd.Parameters.AddWithValue("@IBAN", IBAN)
        cmd.Parameters.AddWithValue("@BANCA", BANCA)
        cmd.Parameters.AddWithValue("@CodiceInstallazione", CodiceInstallazione)
        cmd.Parameters.AddWithValue("@CodiceDitta", CodiceDitta)
        cmd.Parameters.AddWithValue("@NomeFile", NomeFile)

        cmd.Parameters.AddWithValue("@IBANRID", IBANRID)
        cmd.Parameters.AddWithValue("@EndToEndId", EndToEndId)
        cmd.Parameters.AddWithValue("@PrvtId", PrvtId)
        cmd.Parameters.AddWithValue("@MmbId", MmbId)
        cmd.Parameters.AddWithValue("@OrgId", OrgId)


        cmd.Parameters.AddWithValue("@CodiceMittente", CodiceMittente)
        cmd.Parameters.AddWithValue("@CodiceRicevente", CodiceRicevente)
        cmd.Parameters.AddWithValue("@CODICEABI", CODICEABI)
        cmd.Parameters.AddWithValue("@CODICECAB", CODICECAB)
        cmd.Parameters.AddWithValue("@Conto", Conto)

        cmd.Parameters.AddWithValue("@Email", EMail)

        cmd.Parameters.AddWithValue("@BOLLOVIRTUALE", BOLLOVIRTUALE)
        cmd.Parameters.AddWithValue("@REGIMEFISCALE", REGIMEFISCALE)

        cmd.Connection = cn
        cmd.ExecuteNonQuery()

        
        cn.Close()
    End Sub
End Class

