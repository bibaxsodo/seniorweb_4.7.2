﻿Imports Microsoft.VisualBasic
Imports System.Data.OleDb
Imports System.Web.Hosting

Public Class Cls_bolli
    Public DataValidita As Date
    Public ImportoBollo As Double
    Public ImportoApplicazione As Double
    Public CodiceIVA As String

    Function campodb(ByVal oggetto As Object) As String
        If IsDBNull(oggetto) Then
            Return ""
        Else
            Return oggetto
        End If
    End Function

    Sub Leggi(ByVal StringaConnessione As String, ByVal Data As Date)
        Dim cn As OleDbConnection

        cn = New Data.OleDb.OleDbConnection(StringaConnessione)

        cn.Open()
        Dim cmd As New OleDbCommand()
        cmd.CommandText = ("select * from Bolli where " & _
                           " DataValidita <= ? Order by DataValidita Desc")
        cmd.Parameters.AddWithValue("@Data", Data)
        cmd.Connection = cn
        Dim sb As StringBuilder = New StringBuilder

        Dim myPOSTreader As OleDbDataReader = cmd.ExecuteReader()
        If myPOSTreader.Read Then
            DataValidita = campodb(myPOSTreader.Item("DataValidita"))
            ImportoBollo = campodb(myPOSTreader.Item("ImportoBollo"))
            ImportoApplicazione = campodb(myPOSTreader.Item("ImportoApplicazione"))
            CodiceIVA = campodb(myPOSTreader.Item("CodiceIVA"))
        End If
        cn.Close()
    End Sub

End Class
