﻿Imports Microsoft.VisualBasic
Imports System.Data.OleDb
Imports System.Web.Hosting

Public Class Cls_Operatore
    Public Nome As String
    Public RESIDENZAINDIRIZZO1 As String
    Public RESIDENZACOMUNE1 As String
    Public RESIDENZAPROVINCIA1 As String
    Public Telefono1 As String
    Public Telefono2 As String
    Public Specializazione As String
    Public Note As String
    Public CodiceMedico As String
    Public CodiceFiscale As String
    Public TipoOperatore As String
    Public MastroFornitore As Long
    Public ContoFornitore As Long
    Public SottoContoFornitore As Long
    Public EmOspiteParente As Long
    Public RaggruppamentoOperatori As String


    Sub Pulisci()
        Nome = ""
        RESIDENZAINDIRIZZO1 = ""
        RESIDENZACOMUNE1 = ""
        RESIDENZAPROVINCIA1 = ""
        Telefono1 = ""
        Telefono2 = ""
        Specializazione = ""
        Note = ""
        CodiceMedico = ""
        CodiceFiscale = ""
        TipoOperatore = ""

        MastroFornitore = 0
        ContoFornitore = 0
        SottoContoFornitore = 0
        EmOspiteParente = 0
        RaggruppamentoOperatori = ""
    End Sub


    Function campodb(ByVal oggetto As Object) As String
        If IsDBNull(oggetto) Then
            Return ""
        Else
            Return oggetto
        End If
    End Function

    Public Sub UpDateDropBox2(ByVal StringaConnessione As String, ByRef appoggio As DropDownList)
        Dim cn As OleDbConnection



        cn = New Data.OleDb.OleDbConnection(StringaConnessione)

        cn.Open()
        Dim cmd As New OleDbCommand()

        cmd.CommandText = ("select CodiceMedico,  Nome from AnagraficaComune where AnagraficaComune.Tipologia  = 'A' ORDER BY Nome")

        cmd.Connection = cn
        Dim sb As StringBuilder = New StringBuilder

        appoggio.Items.Clear()
        Dim myPOSTreader As OleDbDataReader = cmd.ExecuteReader()
        Do While myPOSTreader.Read
            appoggio.Items.Add(myPOSTreader.Item("CodiceMedico") & " " & myPOSTreader.Item("NOME"))
            appoggio.Items(appoggio.Items.Count - 1).Value = myPOSTreader.Item("CodiceMedico")
        Loop
        myPOSTreader.Close()
        cn.Close()
        appoggio.Items.Add("")
        appoggio.Items(appoggio.Items.Count - 1).Value = ""
        appoggio.Items(appoggio.Items.Count - 1).Selected = True
    End Sub

    Public Function MaxOperatore(ByVal StringaConnessione As String) As Integer
        Dim cn As OleDbConnection
        MaxOperatore = 1


        cn = New Data.OleDb.OleDbConnection(StringaConnessione)

        cn.Open()
        Dim cmd As New OleDbCommand()

        cmd.CommandText = ("select MAX(CAST(CODICEMEDICO AS INT))  as MAXOPE from AnagraficaComune where AnagraficaComune.Tipologia  = 'A'")

        cmd.Connection = cn

        Dim myPOSTreader As OleDbDataReader = cmd.ExecuteReader()
        If myPOSTreader.Read Then
            MaxOperatore = Val(campodb(myPOSTreader.Item(0))) + 1
        End If
        myPOSTreader.Close()
        cn.Close()

    End Function


    Public Sub UpDateDropBox(ByVal StringaConnessione As String, ByRef appoggio As DropDownList)
        Dim cn As OleDbConnection



        cn = New Data.OleDb.OleDbConnection(StringaConnessione)

        cn.Open()
        Dim cmd As New OleDbCommand()

        cmd.CommandText = ("select CodiceMedico,  Nome from AnagraficaComune where AnagraficaComune.Tipologia  = 'A' ORDER BY Nome")

        cmd.Connection = cn
        Dim sb As StringBuilder = New StringBuilder

        appoggio.Items.Clear()
        Dim myPOSTreader As OleDbDataReader = cmd.ExecuteReader()
        Do While myPOSTreader.Read
            appoggio.Items.Add(myPOSTreader.Item("NOME"))
            appoggio.Items(appoggio.Items.Count - 1).Value = myPOSTreader.Item("CodiceMedico")
        Loop
        myPOSTreader.Close()
        cn.Close()
        appoggio.Items.Add("")
        appoggio.Items(appoggio.Items.Count - 1).Value = ""
        appoggio.Items(appoggio.Items.Count - 1).Selected = True

    End Sub

    Sub Scrivi(ByVal StringaConnessione As String)
        Dim cn As OleDbConnection
        Dim MySql As String

        cn = New Data.OleDb.OleDbConnection(StringaConnessione)

        cn.Open()
        Dim cmd As New OleDbCommand()


        cmd.Connection = cn

        Dim cmdLeggi As New OleDbCommand()
        cmdLeggi.CommandText = ("select * from AnagraficaComune where " & _
                           "CodiceMedico = ? And " & _
                           "  AnagraficaComune.Tipologia  = 'A' ")
        cmdLeggi.Parameters.AddWithValue("@CodiceOspite", CodiceMedico)
        cmdLeggi.Connection = cn

        Dim myPOSTreader As OleDbDataReader = cmdLeggi.ExecuteReader()
        If Not myPOSTreader.Read Then


            MySql = "INSERT INTO AnagraficaComune " & _
                    " (Nome,CodiceMedico,TIPOLOGIA ) Values " & _
                    " (?,?,'A')"
            Dim cmd2 As New OleDbCommand()
            cmd2.CommandText = MySql
            cmd2.Parameters.AddWithValue("@Nome", Nome)
            cmd2.Parameters.AddWithValue("@CodiceMedico", CodiceMedico)
            cmd2.Connection = cn
            cmd2.ExecuteNonQuery()
        End If
        myPOSTreader.Close()

        If CodiceMedico = "" Then
            Exit Sub
        End If

        MySql = "UPDATE AnagraficaComune SET " & _
                "Nome = ?, " & _
                "RESIDENZAINDIRIZZO1 = ?, " & _
                "RESIDENZACOMUNE1 = ?, " & _
                "RESIDENZAPROVINCIA1= ?," & _
                "Telefono1 = ?, " & _
                "Telefono2 = ?, " & _
                "Specializazione = ?, " & _
                "Note = ?," & _
                "CodiceFiscale = ?, " & _
                "TIPOOPERAZIONE = ?, " & _
                "MastroFornitore = ?, " & _
                "ContoFornitore = ?, " & _
                "SottoContoFornitore = ?, " & _
                " EmOspiteParente = ?, " & _
                " TipoOspite = ? " & _
                 " Where CodiceMedico = ? "


        'PERIODO
        '" NOTE  = @NOTE, " & _
        cmd.CommandText = MySql

        cmd.Parameters.AddWithValue("@Nome", Nome)
        cmd.Parameters.AddWithValue("@RESIDENZAINDIRIZZO1", RESIDENZAINDIRIZZO1)
        cmd.Parameters.AddWithValue("@RESIDENZACOMUNE1", RESIDENZACOMUNE1)
        cmd.Parameters.AddWithValue("@RESIDENZAPROVINCIA1", RESIDENZAPROVINCIA1)
        cmd.Parameters.AddWithValue("@Telefono1", Telefono1)
        cmd.Parameters.AddWithValue("@Telefono2", Telefono2)
        cmd.Parameters.AddWithValue("@Specializazione", Specializazione)
        cmd.Parameters.AddWithValue("@Note", Note)
        cmd.Parameters.AddWithValue("@CodiceFiscale", CodiceFiscale)
        cmd.Parameters.AddWithValue("@TipoOperatore", TipoOperatore)
        cmd.Parameters.AddWithValue("@MastroFornitore", MastroFornitore)
        cmd.Parameters.AddWithValue("@ContoFornitore", ContoFornitore)
        cmd.Parameters.AddWithValue("@SottoContoFornitore", SottoContoFornitore)
        cmd.Parameters.AddWithValue("@EmOspiteParente", EmOspiteParente)
        cmd.Parameters.AddWithValue("@TipoOspite", RaggruppamentoOperatori)
        cmd.Parameters.AddWithValue("@CodiceMedico", CodiceMedico)

        cmd.ExecuteNonQuery()


        cn.Close()
    End Sub
    Sub Leggi(ByVal StringaConnessione As String)
        Dim cn As OleDbConnection



        cn = New Data.OleDb.OleDbConnection(StringaConnessione)

        cn.Open()
        Dim cmd As New OleDbCommand()
        cmd.CommandText = ("select * from AnagraficaComune where " & _
                           "CodiceMedico = ? And " & _
                           "  AnagraficaComune.Tipologia  = 'A' ")
        cmd.Parameters.AddWithValue("@CodiceOspite", CodiceMedico)
        cmd.Connection = cn
        Dim sb As StringBuilder = New StringBuilder

        Dim myPOSTreader As OleDbDataReader = cmd.ExecuteReader()
        If myPOSTreader.Read Then
            RESIDENZAINDIRIZZO1 = campodb(myPOSTreader.Item("RESIDENZAINDIRIZZO1"))
            RESIDENZACOMUNE1 = campodb(myPOSTreader.Item("RESIDENZACOMUNE1"))
            RESIDENZAPROVINCIA1 = campodb(myPOSTreader.Item("RESIDENZAPROVINCIA1"))
            Telefono1 = campodb(myPOSTreader.Item("Telefono1"))
            Telefono2 = campodb(myPOSTreader.Item("Telefono2"))
            Specializazione = campodb(myPOSTreader.Item("Specializazione"))
            Note = campodb(myPOSTreader.Item("Note"))
            CodiceMedico = campodb(myPOSTreader.Item("CodiceMedico"))
            CodiceFiscale = campodb(myPOSTreader.Item("CodiceFiscale"))
            Nome = campodb(myPOSTreader.Item("Nome"))
            TipoOperatore = campodb(myPOSTreader.Item("TIPOOPERAZIONE"))
            MastroFornitore = Val(campodb(myPOSTreader.Item("MastroFornitore")))
            ContoFornitore = Val(campodb(myPOSTreader.Item("ContoFornitore")))
            SottoContoFornitore = Val(campodb(myPOSTreader.Item("SottoContoFornitore")))
            EmOspiteParente = Val(campodb(myPOSTreader.Item("EmOspiteParente")))
            RaggruppamentoOperatori = campodb(myPOSTreader.Item("TipoOspite"))

        End If
        cn.Close()
    End Sub


    Sub LeggiPerCF(ByVal StringaConnessione As String)
        Dim cn As OleDbConnection



        cn = New Data.OleDb.OleDbConnection(StringaConnessione)

        cn.Open()
        Dim cmd As New OleDbCommand()
        cmd.CommandText = ("select * from AnagraficaComune where " & _
                           "CodiceFiscale = ? And " & _
                           "  AnagraficaComune.Tipologia  = 'A' ")
        cmd.Parameters.AddWithValue("@CodiceFiscale", CodiceFiscale)
        cmd.Connection = cn
        Dim sb As StringBuilder = New StringBuilder

        Dim myPOSTreader As OleDbDataReader = cmd.ExecuteReader()
        If myPOSTreader.Read Then
            RESIDENZAINDIRIZZO1 = campodb(myPOSTreader.Item("RESIDENZAINDIRIZZO1"))
            RESIDENZACOMUNE1 = campodb(myPOSTreader.Item("RESIDENZACOMUNE1"))
            RESIDENZAPROVINCIA1 = campodb(myPOSTreader.Item("RESIDENZAPROVINCIA1"))
            Telefono1 = campodb(myPOSTreader.Item("Telefono1"))
            Telefono2 = campodb(myPOSTreader.Item("Telefono2"))
            Specializazione = campodb(myPOSTreader.Item("Specializazione"))
            Note = campodb(myPOSTreader.Item("Note"))
            CodiceMedico = campodb(myPOSTreader.Item("CodiceMedico"))
            CodiceFiscale = campodb(myPOSTreader.Item("CodiceFiscale"))
            Nome = campodb(myPOSTreader.Item("Nome"))
            TipoOperatore = campodb(myPOSTreader.Item("TIPOOPERAZIONE"))
            MastroFornitore = Val(campodb(myPOSTreader.Item("MastroFornitore")))
            ContoFornitore = Val(campodb(myPOSTreader.Item("ContoFornitore")))
            SottoContoFornitore = Val(campodb(myPOSTreader.Item("SottoContoFornitore")))
            EmOspiteParente = Val(campodb(myPOSTreader.Item("EmOspiteParente")))
            RaggruppamentoOperatori = campodb(myPOSTreader.Item("TipoOspite"))

        End If
        cn.Close()
    End Sub

    Sub LeggiPerNOME(ByVal StringaConnessione As String)
        Dim cn As OleDbConnection



        cn = New Data.OleDb.OleDbConnection(StringaConnessione)

        cn.Open()
        Dim cmd As New OleDbCommand()
        cmd.CommandText = ("select * from AnagraficaComune where " & _
                           "Nome Like ? And " & _
                           "  AnagraficaComune.Tipologia  = 'A' ")
        cmd.Parameters.AddWithValue("@Nome", Nome)
        cmd.Connection = cn
        Dim sb As StringBuilder = New StringBuilder

        Dim myPOSTreader As OleDbDataReader = cmd.ExecuteReader()
        If myPOSTreader.Read Then
            RESIDENZAINDIRIZZO1 = campodb(myPOSTreader.Item("RESIDENZAINDIRIZZO1"))
            RESIDENZACOMUNE1 = campodb(myPOSTreader.Item("RESIDENZACOMUNE1"))
            RESIDENZAPROVINCIA1 = campodb(myPOSTreader.Item("RESIDENZAPROVINCIA1"))
            Telefono1 = campodb(myPOSTreader.Item("Telefono1"))
            Telefono2 = campodb(myPOSTreader.Item("Telefono2"))
            Specializazione = campodb(myPOSTreader.Item("Specializazione"))
            Note = campodb(myPOSTreader.Item("Note"))
            CodiceMedico = campodb(myPOSTreader.Item("CodiceMedico"))
            CodiceFiscale = campodb(myPOSTreader.Item("CodiceFiscale"))
            Nome = campodb(myPOSTreader.Item("Nome"))
            TipoOperatore = campodb(myPOSTreader.Item("TIPOOPERAZIONE"))
            MastroFornitore = Val(campodb(myPOSTreader.Item("MastroFornitore")))
            ContoFornitore = Val(campodb(myPOSTreader.Item("ContoFornitore")))
            SottoContoFornitore = Val(campodb(myPOSTreader.Item("SottoContoFornitore")))
            EmOspiteParente = Val(campodb(myPOSTreader.Item("EmOspiteParente")))
            RaggruppamentoOperatori = campodb(myPOSTreader.Item("TipoOspite"))

        End If
        cn.Close()
    End Sub

    Function VerificaElimina(ByVal StringaConnessione As String) As Boolean
        Dim cn As OleDbConnection



        cn = New Data.OleDb.OleDbConnection(StringaConnessione)
        VerificaElimina = True
        cn.Open()
        Dim cmd As New OleDbCommand()
        cmd.CommandText = ("Select * from MovimentiDomiciliare_Operatori where " & _
                           "CodiceOperatore = ? ")
        cmd.Parameters.AddWithValue("@CodiceOperatore", CodiceMedico)
        cmd.Connection = cn
        Dim myPOSTreader As OleDbDataReader = cmd.ExecuteReader()
        If myPOSTreader.Read Then
            VerificaElimina = False
        End If
        cn.Close()
    End Function
    Sub Elimina(ByVal StringaConnessione As String)
        Dim cn As OleDbConnection



        cn = New Data.OleDb.OleDbConnection(StringaConnessione)

        cn.Open()
        Dim cmd As New OleDbCommand()
        cmd.CommandText = ("Delete from AnagraficaComune where " & _
                           "CodiceMedico = ? And " & _
                           "  AnagraficaComune.Tipologia  = 'A' ")
        cmd.Parameters.AddWithValue("@CodiceOspite", CodiceMedico)
        cmd.Connection = cn
        cmd.ExecuteNonQuery()
        cn.Close()
    End Sub


End Class
