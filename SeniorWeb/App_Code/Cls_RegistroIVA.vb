Imports Microsoft.VisualBasic
Imports System.Data.OleDb
Imports System.Web.Hosting

Public Class Cls_RegistroIVA
    Public Tipo As Long
    Public Descrizione As String
    Public TipoIVA As String
    Public TipoAcqVnt As String
    Public Protocollo As Long
    Public IndicatoreRegistro As String
    Public RegistroCartaceo As Integer

    Function campodb(ByVal oggetto As Object) As String
        If IsDBNull(oggetto) Then
            Return ""
        Else
            Return oggetto
        End If
    End Function
    Function campodbn(ByVal oggetto As Object) As Double
        If IsDBNull(oggetto) Then
            Return 0
        Else
            Return oggetto
        End If
    End Function
    Sub Leggi(ByVal StringaConnessione As String, ByVal Codice As Long)
        Dim cn As OleDbConnection

        cn = New Data.OleDb.OleDbConnection(StringaConnessione)

        cn.Open()
        Dim cmd As New OleDbCommand()
        cmd.CommandText = ("select * from TipoRegistro Where  Tipo = " & Codice)
        cmd.Connection = cn
        Dim sb As StringBuilder = New StringBuilder

        Dim myPOSTreader As OleDbDataReader = cmd.ExecuteReader()
        If myPOSTreader.Read Then
            Tipo = campodb(myPOSTreader.Item("Tipo"))
            Descrizione = campodb(myPOSTreader.Item("Descrizione"))
            TipoIVA = campodb(myPOSTreader.Item("TipoIVA"))
            TipoAcqVnt = campodb(myPOSTreader.Item("TipoAcqVnt"))
            Protocollo = Val(campodb(myPOSTreader.Item("Protocollo")))
            IndicatoreRegistro = campodb(myPOSTreader.Item("IndicatoreRegistro"))
            RegistroCartaceo = campodbn(myPOSTreader.Item("RegistroCartaceo"))
        End If
        myPOSTreader.Close()
        cn.Close()
    End Sub

    Function InUso(ByVal StringaConnessione As String) As Boolean
        Dim cn As OleDbConnection
        InUso = False


        cn = New Data.OleDb.OleDbConnection(StringaConnessione)

        cn.Open()
        Dim cmd As New OleDbCommand()
        cmd.CommandText = ("select *  from MovimentiContabiliTesta Where RegistroIVA = ? ")
        cmd.Parameters.AddWithValue("@Tipo", Tipo)
        cmd.Connection = cn
        Dim InUsoRd As OleDbDataReader = cmd.ExecuteReader()
        If InUsoRd.Read Then
            InUso = True
        End If
        InUsoRd.Close()
        cn.Close()


    End Function

    Function MaxRegistro(ByVal StringaConnessione As String) As Long
        Dim cn As OleDbConnection
        Dim MaxRegistroVar As Integer = 1
        cn = New Data.OleDb.OleDbConnection(StringaConnessione)

        cn.Open()
        Dim cmd As New OleDbCommand()
        cmd.CommandText = ("select max(Tipo) from TipoRegistro ")
        cmd.Connection = cn
        Dim sb As StringBuilder = New StringBuilder

        Dim myPOSTreader As OleDbDataReader = cmd.ExecuteReader()
        If myPOSTreader.Read Then
            MaxRegistroVar = campodb(myPOSTreader.Item(0)) + 1
        End If
        myPOSTreader.Close()
        cn.Close()

        Return MaxRegistroVar
    End Function


    Sub LeggiDescrizione(ByVal StringaConnessione As String, ByVal Descrizione As String)
        Dim cn As OleDbConnection

        cn = New Data.OleDb.OleDbConnection(StringaConnessione)

        cn.Open()
        Dim cmd As New OleDbCommand()
        cmd.CommandText = ("select * from TipoRegistro Where  Descrizione Like ?")
        cmd.Parameters.AddWithValue("@Descrizione", Descrizione)
        cmd.Connection = cn
        Dim sb As StringBuilder = New StringBuilder

        Dim myPOSTreader As OleDbDataReader = cmd.ExecuteReader()
        If myPOSTreader.Read Then
            Tipo = campodb(myPOSTreader.Item("Tipo"))
            Call Leggi(StringaConnessione, Tipo)
        End If
        myPOSTreader.Close()
        cn.Close()
    End Sub

    Sub Delete(ByVal StringaConnessione As String)
        Dim cn As OleDbConnection

        cn = New Data.OleDb.OleDbConnection(StringaConnessione)

        cn.Open()
        Dim cmd As New OleDbCommand()
        cmd.CommandText = ("Delete from TipoRegistro Where  Tipo = " & Tipo)
        cmd.Connection = cn
        cmd.ExecuteNonQuery()


        cn.Close()
    End Sub
    Sub LeggiGiornale(ByVal StringaConnessione As String)
        Dim cn As OleDbConnection

        cn = New Data.OleDb.OleDbConnection(StringaConnessione)

        cn.Open()
        Dim cmd As New OleDbCommand()
        cmd.CommandText = ("select * from TipoRegistro Where  TipoIVA = 'G'")
        cmd.Connection = cn
        Dim sb As StringBuilder = New StringBuilder

        Dim myPOSTreader As OleDbDataReader = cmd.ExecuteReader()
        If myPOSTreader.Read Then
            Tipo = campodb(myPOSTreader.Item("Tipo"))
            Descrizione = campodb(myPOSTreader.Item("Descrizione"))
            TipoIVA = campodb(myPOSTreader.Item("TipoIVA"))
            TipoAcqVnt = campodb(myPOSTreader.Item("TipoAcqVnt"))
            Protocollo = Val(campodb(myPOSTreader.Item("Protocollo")))
            IndicatoreRegistro = campodb(myPOSTreader.Item("IndicatoreRegistro"))
            RegistroCartaceo = campodbn(myPOSTreader.Item("RegistroCartaceo"))
        End If
        myPOSTreader.Close()
        cn.Close()
    End Sub

    Sub UpDateDropBox(ByVal StringaConnessione As String, ByRef appoggio As DropDownList)
        Dim cn As OleDbConnection



        cn = New Data.OleDb.OleDbConnection(StringaConnessione)

       cn.Open()
        Dim cmd As New OleDbCommand()
        cmd.CommandText = ("select * from TipoRegistro")
        cmd.Connection = cn
        Dim sb As StringBuilder = New StringBuilder

        appoggio.Items.Clear()
        Dim myPOSTreader As OleDbDataReader = cmd.ExecuteReader()
        Do While myPOSTreader.Read
            appoggio.Items.Add(myPOSTreader.Item("Descrizione"))
            appoggio.Items(appoggio.Items.Count - 1).Value = myPOSTreader.Item("Tipo")
        Loop
        myPOSTreader.Close()
        cn.Close()
        appoggio.Items.Add("")
        appoggio.Items(appoggio.Items.Count - 1).Value = 0
        appoggio.Items(appoggio.Items.Count - 1).Selected = True
    End Sub


    Sub Scrivi(ByVal StringaConnessione As String)
        Dim cn As OleDbConnection

        cn = New Data.OleDb.OleDbConnection(StringaConnessione)

        cn.Open()
        Dim cmd As New OleDbCommand()
        cmd.CommandText = ("select * from TipoRegistro Where  Tipo = " & Tipo)
        cmd.Connection = cn
        
        Dim myPOSTreader As OleDbDataReader = cmd.ExecuteReader()
        If Not myPOSTreader.Read Then
            Dim XSql As String
            XSql = "INSERT INTO TipoRegistro (Tipo,DESCRIZIONE) values (?,?)"
            Dim cmdIns As New OleDbCommand()
            cmdIns.Parameters.AddWithValue("@Tipo", Tipo)
            cmdIns.Parameters.AddWithValue("@Descrizione", Descrizione)
            cmdIns.CommandText = XSql
            cmdIns.Connection = cn
            cmdIns.ExecuteNonQuery()
        End If        
        myPOSTreader.Close()

        Dim MySql As String

        MySql = "UPDATE TipoRegistro SET " & _
                " Descrizione  = ?, " & _
                " TipoIVA  = ?, " & _
                " TipoAcqVnt  = ?, " & _
                " Protocollo  = ?, " & _
                " IndicatoreRegistro  = ?, " & _
                " RegistroCartaceo =  ? " & _
                " Where Tipo = ? "
        Dim cmd1 As New OleDbCommand()
        cmd1.Connection = cn
        cmd1.CommandText = MySql
        cmd1.Parameters.AddWithValue("@Descrizione", Descrizione)
        cmd1.Parameters.AddWithValue("@TipoIVA", TipoIVA)
        cmd1.Parameters.AddWithValue("@TipoAcqVnt", TipoAcqVnt)
        cmd1.Parameters.AddWithValue("@Protocollo", Protocollo)
        cmd1.Parameters.AddWithValue("@IndicatoreRegistro", IndicatoreRegistro)
        cmd1.Parameters.AddWithValue("@RegistroCartaceo", RegistroCartaceo)
        cmd1.Parameters.AddWithValue("@Tipo", Tipo)

        cmd1.ExecuteNonQuery()

        cn.Close()
    End Sub


    Sub UpDateDropBoxCodice(ByVal StringaConnessione As String, ByRef appoggio As DropDownList)
        Dim cn As OleDbConnection



        cn = New Data.OleDb.OleDbConnection(StringaConnessione)

        cn.Open()
        Dim cmd As New OleDbCommand()
        cmd.CommandText = ("select * from TipoRegistro")
        cmd.Connection = cn
        Dim sb As StringBuilder = New StringBuilder

        appoggio.Items.Clear()
        Dim myPOSTreader As OleDbDataReader = cmd.ExecuteReader()
        Do While myPOSTreader.Read
            appoggio.Items.Add(myPOSTreader.Item("Tipo") & " " & myPOSTreader.Item("Descrizione"))
            appoggio.Items(appoggio.Items.Count - 1).Value = myPOSTreader.Item("Tipo")
        Loop
        myPOSTreader.Close()
        cn.Close()
        appoggio.Items.Add("")
        appoggio.Items(appoggio.Items.Count - 1).Value = 0
        appoggio.Items(appoggio.Items.Count - 1).Selected = True
    End Sub
End Class

