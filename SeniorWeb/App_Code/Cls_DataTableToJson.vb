﻿Imports Microsoft.VisualBasic
Imports System.Data
Imports System.Web.Script.Serialization

Public Class Cls_DataTableToJson

    Public Function SerializeObject(ByRef Oggetto As Object) As String
        Dim serializer As JavaScriptSerializer
        serializer = New JavaScriptSerializer()


        Return serializer.Serialize(Oggetto)
    End Function

    Public Function DataTableToJsonObj(ByVal dt As DataTable) As String
        Dim ds As DataSet = New DataSet()
        ds.Merge(dt)
        Dim JsonString As StringBuilder = New StringBuilder()
        If ds IsNot Nothing AndAlso ds.Tables(0).Rows.Count > 0 Then
            JsonString.Append("[")
            For i As Integer = 0 To ds.Tables(0).Rows.Count - 1
                JsonString.Append("{")
                For j As Integer = 0 To ds.Tables(0).Columns.Count - 1
                    If j < ds.Tables(0).Columns.Count - 1 Then
                        JsonString.Append("""" & ds.Tables(0).Columns(j).ColumnName.ToString() & """:" & """" + ds.Tables(0).Rows(i)(j).ToString() & """,")
                    ElseIf j = ds.Tables(0).Columns.Count - 1 Then
                        JsonString.Append("""" & ds.Tables(0).Columns(j).ColumnName.ToString() & """:" & """" + ds.Tables(0).Rows(i)(j).ToString() & """")
                    End If
                Next

                If i = ds.Tables(0).Rows.Count - 1 Then
                    JsonString.Append("}")
                Else
                    JsonString.Append("},")
                End If
            Next

            JsonString.Append("]")
            Return JsonString.ToString()
        Else
            Return Nothing
        End If
    End Function
End Class
