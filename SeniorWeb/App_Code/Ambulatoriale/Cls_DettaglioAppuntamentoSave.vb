﻿Imports Senior.Entities
Imports Senior.BIZ
Imports System.Data.OleDb
Imports System.Web.Script.Serialization

Public Class Cls_DettaglioAppuntamentoSave
   
    Public Sub CaricaPrestazioni(ByVal StringaConnessione As String,ByRef  Dati As  List(Of DettaglioAppuntamento))
        Dim cn As OleDbConnection
        

        cn = New Data.OleDb.OleDbConnection(StringaConnessione)

        cn.Open()
        Dim cmd As New OleDbCommand()
        cmd.CommandText = ("select * from Amb_Appuntamento ")        
        cmd.Connection = cn

        Dim ReadaAppuntamento As OleDbDataReader = cmd.ExecuteReader()
        Do While ReadaAppuntamento.Read 
            Dim Appuntamento As New DettaglioAppuntamento
            
            Appuntamento.CodiceOperazione = campodb(ReadaAppuntamento.Item("CodiceOperazione"))
            Appuntamento.Data = campodbd(ReadaAppuntamento.Item("Data"))
            Appuntamento.Ora= campodbd(ReadaAppuntamento.Item("Ora"))                     
            Appuntamento.DataDomanda= campodb(ReadaAppuntamento.Item("DataDomanda"))
            Appuntamento.Operatore= campodb(ReadaAppuntamento.Item("Operatore"))
            Appuntamento.TipoContatto= campodb(ReadaAppuntamento.Item("TipoContatto"))
            Appuntamento.CodiceSanitario= campodb(ReadaAppuntamento.Item("CodiceSanitario"))
            Appuntamento.Provenienza= campodb(ReadaAppuntamento.Item("Provenienza"))
            Appuntamento.ModalitaConsegna= campodb(ReadaAppuntamento.Item("ModalitaConsegna"))
            Appuntamento.CodiceImpegnativa= campodb(ReadaAppuntamento.Item("CodiceImpegnativa"))
            Appuntamento.Contratto= campodb(ReadaAppuntamento.Item("Contratto"))
            Appuntamento.Priorita= campodb(ReadaAppuntamento.Item("Priorita"))
            Appuntamento.TestoQuesito= campodb(ReadaAppuntamento.Item("TestoQuesito"))
            Appuntamento.OperatoreMedico= campodb(ReadaAppuntamento.Item("OperatoreMedico"))
            Appuntamento.IDModalitaInvioReferto= campodb(ReadaAppuntamento.Item("IDModalitaInvioReferto"))
            Appuntamento.Eliminato= campodb(ReadaAppuntamento.Item("Eliminato"))

            Appuntamento.MastroExtra= campodbN(ReadaAppuntamento.Item("MastroExtra"))
            Appuntamento.ContoExtra= campodbN(ReadaAppuntamento.Item("ContoExtra"))
            Appuntamento.SottoContoExtra= campodbN(ReadaAppuntamento.Item("SottoContoExtra"))

            Appuntamento.CodiceIVA= campodb(ReadaAppuntamento.Item("CodiceIVA"))
            Appuntamento.Descrizione= campodb(ReadaAppuntamento.Item("Descrizione"))
            Appuntamento.DareAvere= campodb(ReadaAppuntamento.Item("DareAvere"))
            Appuntamento.Importo= campodbn(ReadaAppuntamento.Item("Importo"))


            Dim cmdUtente As New OleDbCommand()
            cmdUtente.CommandText = ("select * from Amb_Assistito  Where id  = " & campodb(ReadaAppuntamento.Item("idUtente")))      
            cmdUtente.Connection = cn

            Dim ReadaUtente As OleDbDataReader = cmdUtente.ExecuteReader()
            Do While ReadaUtente.Read 
                Appuntamento.Utente = New Cliente

                Appuntamento.Utente.Cognome =campodb(ReadaUtente.Item("Cognome"))
                Appuntamento.Utente.Nome=campodb(ReadaUtente.Item("Nome"))
                Appuntamento.Utente.CodiceFiscale=campodb(ReadaUtente.Item("CodiceFiscale"))
                Appuntamento.Utente.Sesso=campodb(ReadaUtente.Item("Sesso"))
                Appuntamento.Utente.Telefono1=campodb(ReadaUtente.Item("Telefono"))
                Appuntamento.Utente.Indirizzo=campodb(ReadaUtente.Item("Indirizzo"))
                Appuntamento.Utente.CAP=campodb(ReadaUtente.Item("CAP"))
                Appuntamento.Utente.Comune=campodb(ReadaUtente.Item("Comune"))
                Appuntamento.Utente.IDFasciaIsee =campodbn(ReadaUtente.Item("IDFasciaIsee"))
                Appuntamento.Utente.DataNascita = campodbd(ReadaUtente.Item("DataNascita"))
            Loop
            ReadaUtente.Close


            
            Dim cmdPrestazione As New OleDbCommand()
            cmdPrestazione.CommandText = ("select * from Amb_Prestazione  Where ID_Appuntamento  = " & campodbn(ReadaAppuntamento.Item("ID")))      
            cmdPrestazione.Connection = cn

            Dim ReadaPrestazione As OleDbDataReader = cmdPrestazione.ExecuteReader()
            Do While ReadaPrestazione.Read 
                Dim MyPrestazione As  New  Prestazione

                MyPrestazione.ID =campodbn(ReadaPrestazione.Item("ID")) 
                MyPrestazione.ID_Cliente = campodbn(ReadaPrestazione.Item("ID_Cliente")) 
                MyPrestazione.ID_Operatore= campodbn(ReadaPrestazione.Item("ID_Operatore")) 
                MyPrestazione.ID_ePersonam= campodbn(ReadaPrestazione.Item("ID_ePersonam")) 
                MyPrestazione.Codice_Prestazione= campodb(ReadaPrestazione.Item("Codice_Prestazione")) 
                MyPrestazione.Descrizione= campodb(ReadaPrestazione.Item("Descrizione")) 
                If campodbn(ReadaPrestazione.Item("CheckIn"))  = 1 then
                    MyPrestazione.CheckIn  =True
                Else
                    MyPrestazione.CheckIn  =False
                End If

                If campodbn(ReadaPrestazione.Item("Fatturata"))  =1 then
                    MyPrestazione.Fatturata   =True
                Else
                    MyPrestazione.Fatturata   =False
                End If
                
                If campodbn(ReadaPrestazione.Item("OscuramentoPrestazione"))  =1 then
                    MyPrestazione.OscuramentoPrestazione =True
                Else
                    MyPrestazione.OscuramentoPrestazione =False
                End if
                If campodbn(ReadaPrestazione.Item("ConsensoFirmato")) =  1 Then
                    MyPrestazione.ConsensoFirmato = True
                Else
                    MyPrestazione.ConsensoFirmato = False
                End if

                MyPrestazione.PathConsensoFirmato= campodb(ReadaPrestazione.Item("PathConsensoFirmato")) 
                
                Appuntamento.Prestazioni.Add(MyPrestazione)
            Loop
            ReadaPrestazione.Close


            Dati.Add(Appuntamento)
        loop
        ReadaAppuntamento.Close


        cn.Close()

    End Sub


   Public Function CreaCliente(ByVal StringaConnessione As String, ByVal MyAppuntamento As DettaglioAppuntamento)  As Integer
        Dim cn As OleDbConnection
        Dim IDCliente As Integer =0


        cn = New Data.OleDb.OleDbConnection(StringaConnessione)

        cn.Open()
        Dim cmd As New OleDbCommand()
        cmd.CommandText = ("select * from Amb_Assistito where  CodiceFiscale= ? ")
        cmd.Parameters.AddWithValue("@CodiceFiscale", MyAppuntamento.Utente.CodiceFiscale)
        cmd.Connection = cn

        Dim myPOSTreader As OleDbDataReader = cmd.ExecuteReader()
        If myPOSTreader.Read Then
            IDCliente= campodbn(myPOSTreader.Item("ID"))             
        End If
        myPOSTreader.Close

        
        If IDCliente = 0 then
           Dim cmdInsert As New OleDbCommand()
           Dim Transan As OleDbTransaction = cn.BeginTransaction()
           cmdInsert.CommandText = "INSERT INTO Amb_Assistito (CodiceFiscale) VALUES (?) "
           cmdInsert.Parameters.AddWithValue("@CodiceFiscale", MyAppuntamento.Utente.CodiceFiscale)
           cmdInsert.Connection = cn
           cmdInsert.Transaction = Transan
           cmdInsert.ExecuteNonQuery

            Dim cmdRead As New OleDbCommand()
            cmdRead.CommandText = ("select * from Amb_Assistito where  CodiceFiscale= ? ")
            cmdRead.Parameters.AddWithValue("@CodiceFiscale", MyAppuntamento.Utente.CodiceFiscale)
            cmdRead.Connection = cn
            cmdRead.Transaction = Transan
            Dim ReadID As OleDbDataReader = cmdRead.ExecuteReader()
            If ReadID.Read Then
                IDCliente= campodbn(ReadID.Item("ID"))             
            End If
            ReadID.Close
            Transan.Commit
            Transan.Dispose
        End If

        Dim cmdUpdate As New OleDbCommand()
        Dim MySQl As String =""

        

        MySql ="Update Amb_Assistito SET Cognome = ?, " &  _
               " Nome = ?, " & _ 
               " CodiceFiscale = ?, " & _ 
               " Sesso = ?, " & _ 
               " Telefono = ?, " & _ 
               " Indirizzo = ?, " & _ 
               " CAP = ?, " & _ 
               " Comune = ?, " & _  
               " DataNascita = ?, " & _
               " IDFasciaIsee = ? " & _  
               " Where Id = ?" 

        cmdUpdate.CommandText = MySQl
        cmdUpdate.Parameters.AddWithValue("@Cognome", MyAppuntamento.Utente.Cognome)
        cmdUpdate.Parameters.AddWithValue("@Nome", MyAppuntamento.Utente.Nome)
        cmdUpdate.Parameters.AddWithValue("@CodiceFiscale", MyAppuntamento.Utente.CodiceFiscale)
        cmdUpdate.Parameters.AddWithValue("@Sesso", MyAppuntamento.Utente.Sesso)
        cmdUpdate.Parameters.AddWithValue("@Telefono", MyAppuntamento.Utente.Telefono1)
        cmdUpdate.Parameters.AddWithValue("@Indirizzo", MyAppuntamento.Utente.Indirizzo)
        cmdUpdate.Parameters.AddWithValue("@CAP", MyAppuntamento.Utente.CAP)
        cmdUpdate.Parameters.AddWithValue("@Comune", MyAppuntamento.Utente.Comune)
        cmdUpdate.Parameters.AddWithValue("@DataNascita", MyAppuntamento.Utente.DataNascita)
        cmdUpdate.Parameters.AddWithValue("@IDFasciaIsee", MyAppuntamento.Utente.IDFasciaIsee)
        cmdUpdate.Parameters.AddWithValue("@Id", IDCliente)
        cmdUpdate.Connection = cn
        cmdUpdate.ExecuteNonQuery()


        cn.Close

        CreaCliente = IDCliente

    End Function


    Public Sub UpDateSingolaPrestazione(ByVal StringaConnessione As String, ByVal Prestazione As  Prestazione,ByVal IDOperazione  As Integer,ByVal IDCliente  As Integer) 
        Dim cn As OleDbConnection

        cn = New Data.OleDb.OleDbConnection(StringaConnessione)

        cn.Open()
        Dim cmdUpDate As New OleDbCommand()
           
        cmdUpDate.CommandText = "UPDATE Amb_Prestazione SET ID_Appuntamento = ?,ID_Cliente = ? ,ID_Operatore  = ?,Codice_Prestazione  = ?,Descrizione  = ?,CheckIn  = ?,Fatturata  = ?,OscuramentoPrestazione  = ?,ConsensoFirmato  = ?,PathConsensoFirmato  = ?"
        cmdUpDate.Parameters.AddWithValue("@ID_Appuntamento", IDOperazione)
        cmdUpDate.Parameters.AddWithValue("@ID_Cliente", IDCliente)
        cmdUpDate.Parameters.AddWithValue("@ID_Operatore", Prestazione.ID_Operatore)
        cmdUpDate.Parameters.AddWithValue("@Codice_Prestazione", Prestazione.Codice_Prestazione)
        cmdUpDate.Parameters.AddWithValue("@Descrizione", Prestazione.Descrizione)

        cmdUpDate.Parameters.AddWithValue("@CheckIn", Prestazione.CheckIn)
        cmdUpDate.Parameters.AddWithValue("@Fatturata", Prestazione.Fatturata)
        cmdUpDate.Parameters.AddWithValue("@OscuramentoPrestazione", Prestazione.OscuramentoPrestazione)
        cmdUpDate.Parameters.AddWithValue("@ConsensoFirmato", Prestazione.ConsensoFirmato)
        cmdUpDate.Connection = cn           
        cmdUpDate.ExecuteNonQuery

        cn.Close

    End Sub
    Public Sub CreaPrestazione(ByVal StringaConnessione As String, ByVal MyAppuntamento As DettaglioAppuntamento,ByVal IDOperazione  As Integer,ByVal IDCliente  As Integer) 
        Dim cn As OleDbConnection

        cn = New Data.OleDb.OleDbConnection(StringaConnessione)

        cn.Open()

        
        For Each Dettaglio In MyAppuntamento.Prestazioni
           If val(Dettaglio.Codice_Prestazione) > 0 Then
               If Dettaglio.ID =0 then
                   Dim cmdInsert As New OleDbCommand()
           
                   cmdInsert.CommandText = "INSERT INTO Amb_Prestazione (ID_Appuntamento,ID_Cliente,ID_Operatore,Codice_Prestazione,Descrizione,CheckIn,Fatturata,OscuramentoPrestazione,ConsensoFirmato,PathConsensoFirmato, ID_ePersonam) VALUES (?,?,?,?,?,?,?,?,?,?,?) "
                                                                    
                   cmdInsert.Parameters.AddWithValue("@ID_Appuntamento", IDOperazione)
                   cmdInsert.Parameters.AddWithValue("@ID_Cliente", IDCliente)
                   cmdInsert.Parameters.AddWithValue("@ID_Operatore", Dettaglio.ID_Operatore)
                   cmdInsert.Parameters.AddWithValue("@Codice_Prestazione", Dettaglio.Codice_Prestazione)
                   cmdInsert.Parameters.AddWithValue("@Descrizione", Dettaglio.Descrizione)

                
                    cmdInsert.Parameters.AddWithValue("@CheckIn", Dettaglio.CheckIn)
                    cmdInsert.Parameters.AddWithValue("@Fatturata", Dettaglio.Fatturata)
                    cmdInsert.Parameters.AddWithValue("@OscuramentoPrestazione", Dettaglio.OscuramentoPrestazione)
                    cmdInsert.Parameters.AddWithValue("@ConsensoFirmato", Dettaglio.ConsensoFirmato)
                    cmdInsert.Parameters.AddWithValue("@PathConsensoFirmato", Dettaglio.PathConsensoFirmato)
                    cmdInsert.Parameters.AddWithValue("@ID_ePersonam", Dettaglio.ID_ePersonam)
                   cmdInsert.Connection = cn           
                   cmdInsert.ExecuteNonQuery
                Else
                   Dim cmdUpDate As New OleDbCommand()
           
                   cmdUpDate.CommandText = "UPDATE Amb_Prestazione SET ID_Appuntamento = ?,ID_Cliente = ? ,ID_Operatore  = ?,Codice_Prestazione  = ?,Descrizione  = ?,CheckIn  = ?,Fatturata  = ?,OscuramentoPrestazione  = ?,ConsensoFirmato  = ?,PathConsensoFirmato  = ?,ID_ePersonam = ?  where id =  ?"
                   cmdUpDate.Parameters.AddWithValue("@ID_Appuntamento", IDOperazione)
                   cmdUpDate.Parameters.AddWithValue("@ID_Cliente", IDCliente)
                   cmdUpDate.Parameters.AddWithValue("@ID_Operatore", Dettaglio.ID_Operatore)
                   cmdUpDate.Parameters.AddWithValue("@Codice_Prestazione", Dettaglio.Codice_Prestazione)
                   cmdUpDate.Parameters.AddWithValue("@Descrizione", Dettaglio.Descrizione)

                    cmdUpDate.Parameters.AddWithValue("@CheckIn", Dettaglio.CheckIn)
                    cmdUpDate.Parameters.AddWithValue("@Fatturata", Dettaglio.Fatturata)
                    cmdUpDate.Parameters.AddWithValue("@OscuramentoPrestazione", Dettaglio.OscuramentoPrestazione)
                    cmdUpDate.Parameters.AddWithValue("@ConsensoFirmato", Dettaglio.ConsensoFirmato)
                    cmdUpDate.Parameters.AddWithValue("@PathConsensoFirmato", Dettaglio.PathConsensoFirmato)
                    cmdUpDate.Parameters.AddWithValue("@ID_ePersonam", Dettaglio.ID_ePersonam)
                    cmdUpDate.Parameters.AddWithValue("@ID", Dettaglio.ID)
                    cmdUpDate.Connection = cn           
                    cmdUpDate.ExecuteNonQuery
               End If
            End if
        Next

        cn.Close
    End Sub


    Public Function GetIDAppuntamento(ByVal StringaConnessione As String,ByVal CodiceOperazione As  String)  As Integer
        Dim cn As OleDbConnection

        cn = New Data.OleDb.OleDbConnection(StringaConnessione)

        cn.Open()

        GetIDAppuntamento=0

        Dim cmd As New OleDbCommand()
        cmd.CommandText = ("select * from Amb_Appuntamento where  CodiceOperazione= ? ")
        cmd.Parameters.AddWithValue("@CodiceOperazione", CodiceOperazione)
        cmd.Connection = cn

        Dim myPOSTreader As OleDbDataReader = cmd.ExecuteReader()
        If myPOSTreader.Read Then
            GetIDAppuntamento= campodbn(myPOSTreader.Item("ID"))             
        End If
        myPOSTreader.Close
        cn.Close
    End Function

    Public Sub CancellaPrestazioni(ByVal StringaConnessione As String,ByVal IdAppuntamento As  Integer) 
        Dim cn As OleDbConnection

        cn = New Data.OleDb.OleDbConnection(StringaConnessione)

        cn.Open()

        Dim cmdDelete As New OleDbCommand()
           
        cmdDelete.CommandText = "DELETE  FROM Amb_Prestazione WHERE ID_Appuntamento = ?"
        cmdDelete.Parameters.AddWithValue("@ID_Appuntamento", IdAppuntamento)             
        cmdDelete.Connection = cn           
        cmdDelete.ExecuteNonQuery

        cn.Close
    End Sub


    Public Sub ScriviDB(ByVal StringaConnessione As String, ByVal MyAppuntamento As DettaglioAppuntamento, ByVal SeniorDB As String, ByVal Cliente As String, ByVal Utente As String,ByVal UtenteImpert As String, ByVal MyPage As Page )
        Dim cn As OleDbConnection
        Dim IDOperazione As Integer =0
        Dim idUtente As  Integer =0


        Dim Log As New Cls_LogPrivacy

        Dim serializer As JavaScriptSerializer
        serializer = New JavaScriptSerializer()
        Log.LogPrivacy(SeniorDB, Cliente, Utente, UtenteImpert, MyPage.GetType.Name.ToUpper, 0, 0, "", "", 0, "", "I", "EMISSIONEFATTURA", serializer.Serialize(MyAppuntamento))



        idUtente  =CreaCliente(StringaConnessione,MyAppuntamento)


        cn = New Data.OleDb.OleDbConnection(StringaConnessione)

        cn.Open()
        Dim cmd As New OleDbCommand()
        cmd.CommandText = ("select * from Amb_Appuntamento where  CodiceOperazione= ? ")
        cmd.Parameters.AddWithValue("@CodiceOperazione", MyAppuntamento.CodiceOperazione)
        cmd.Connection = cn

        Dim myPOSTreader As OleDbDataReader = cmd.ExecuteReader()
        If myPOSTreader.Read Then
            IDOperazione= campodbn(myPOSTreader.Item("ID"))             
        End If
        myPOSTreader.Close


        Dim MySql  As String

        If IDOperazione = 0 then
           Dim cmdInsert As New OleDbCommand()
           Dim Transan As OleDbTransaction = cn.BeginTransaction()
           cmdInsert.CommandText = "INSERT INTO Amb_Appuntamento (CodiceOperazione) VALUES (?) "
           cmdInsert.Parameters.AddWithValue("@CodiceOperazione",  MyAppuntamento.CodiceOperazione)
           cmdInsert.Connection = cn
           cmdInsert.Transaction = Transan
           cmdInsert.ExecuteNonQuery

            Dim cmdRead As New OleDbCommand()
            cmdRead.CommandText = ("select * from Amb_Appuntamento where  CodiceOperazione= ? ")
            cmdRead.Parameters.AddWithValue("@CodiceOperazione",  MyAppuntamento.CodiceOperazione)
            cmdRead.Connection = cn
            cmdRead.Transaction = Transan
            Dim ReadID As OleDbDataReader = cmdRead.ExecuteReader()
            If ReadID.Read Then
                IDOperazione= campodbn(ReadID.Item("ID"))             
            End If
            ReadID.Close
            Transan.Commit
        End If


        Dim cmdUpdate As New OleDbCommand()

        MySql ="Update Amb_Appuntamento SET Data = ?, " &  _
               " Ora = ?, " & _ 
               " idUtente = ?, " & _ 
               " DataDomanda = ?, " & _ 
               " Operatore = ?, " & _ 
               " TipoContatto = ?, " & _ 
               " CodiceSanitario = ?, " & _ 
               " Provenienza = ?, " & _ 
               " ModalitaConsegna = ?, " &  _
               " CodiceImpegnativa= ?, " &  _
               " Contratto = ?, " &  _
               " Priorita = ?, " &  _
               " TestoQuesito = ?, " &  _
               " OperatoreMedico = ?, " & _
               " IDModalitaInvioReferto = ?, " & _
               " MastroExtra = ?, " & _
               " ContoExtra = ?, " & _
               " SottocontoExtra = ?, " & _
               " CodiceIVA = ?, " & _
               " DareAvere = ?, " & _
               " Importo = ?, " & _
               " Descrizione = ?, " & _
               " Eliminato = ?  Where Id = ?" 

        cmdUpdate.CommandText = MySql
        cmdUpdate.Parameters.AddWithValue("@Data", IIf(Year(MyAppuntamento.Data) > 1, MyAppuntamento.Data, System.DBNull.Value))
        cmdUpdate.Parameters.AddWithValue("@Ora", IIf(hour(MyAppuntamento.Ora) > 1,  DateSerial(1899,12,30).AddHours(Hour(MyAppuntamento.Ora)).AddMinutes(Minute(MyAppuntamento.Ora)), System.DBNull.Value))
        cmdUpdate.Parameters.AddWithValue("@idUtente", idUtente)
        cmdUpdate.Parameters.AddWithValue("@DataDomanda",  IIf(Year(MyAppuntamento.DataDomanda) > 1, MyAppuntamento.DataDomanda, System.DBNull.Value))
        cmdUpdate.Parameters.AddWithValue("@Operatore", MyAppuntamento.Operatore)
        cmdUpdate.Parameters.AddWithValue("@TipoContatto", MyAppuntamento.TipoContatto)
        cmdUpdate.Parameters.AddWithValue("@CodiceSanitario", MyAppuntamento.CodiceSanitario)
        cmdUpdate.Parameters.AddWithValue("@Provenienza", MyAppuntamento.Provenienza)
        cmdUpdate.Parameters.AddWithValue("@ModalitaConsegna", MyAppuntamento.ModalitaConsegna)
        cmdUpdate.Parameters.AddWithValue("@CodiceImpegnativa", MyAppuntamento.CodiceImpegnativa)
        cmdUpdate.Parameters.AddWithValue("@Contratto", MyAppuntamento.Contratto)
        cmdUpdate.Parameters.AddWithValue("@Priorita", MyAppuntamento.Priorita)
        cmdUpdate.Parameters.AddWithValue("@TestoQuesito", MyAppuntamento.TestoQuesito)
        cmdUpdate.Parameters.AddWithValue("@OperatoreMedico", MyAppuntamento.OperatoreMedico)
        cmdUpdate.Parameters.AddWithValue("@IDModalitaInvioReferto", MyAppuntamento.IDModalitaInvioReferto)

        cmdUpdate.Parameters.AddWithValue("@MastroExtra", MyAppuntamento.MastroExtra)
        cmdUpdate.Parameters.AddWithValue("@ContoExtra", MyAppuntamento.ContoExtra)
        cmdUpdate.Parameters.AddWithValue("@SottocontoExtra", MyAppuntamento.SottocontoExtra)

        cmdUpdate.Parameters.AddWithValue("@CodiceIVA", MyAppuntamento.CodiceIVA)
        cmdUpdate.Parameters.AddWithValue("@DareAvere", MyAppuntamento.DareAvere)
        cmdUpdate.Parameters.AddWithValue("@Importo", MyAppuntamento.Importo)
        cmdUpdate.Parameters.AddWithValue("@Descrizione", MyAppuntamento.Descrizione)

        cmdUpdate.Parameters.AddWithValue("@Eliminato", MyAppuntamento.Eliminato)
        cmdUpdate.Parameters.AddWithValue("@Id", IDOperazione)
        cmdUpdate.Connection = cn
        cmdUpdate.ExecuteNonQuery()
        

        CreaPrestazione (StringaConnessione,MyAppuntamento,IDOperazione, idUtente)
        cn.Close()
    End Sub

    Function campodb(ByVal oggetto As Object) As String
        If IsDBNull(oggetto) Then
            Return ""
        Else
            Return oggetto
        End If
    End Function

    Function campodbD(ByVal oggetto As Object) As Date
        If IsDBNull(oggetto) Then
            Return Nothing
        Else
            Return oggetto
        End If
    End Function
    Function campodbn(ByVal oggetto As Object) As Double
        If IsDBNull(oggetto) Then
            Return 0
        Else
            Return oggetto
        End If
    End Function

End Class
