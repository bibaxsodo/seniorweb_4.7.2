﻿Imports Senior.Entities
Imports Senior.BIZ
Imports System.Data.OleDb
Imports System.Web.Script.Serialization

Public Class Cls_Amb_Parametri

    Public MastroRicavo As Long
    Public ContoRicavo As Long
    Public SottoContoRicavo As Long
    Public CodiceIVA As String
    Public IdEpersonamStruttura As Integer


    Public Sub LeggiParametri(ByVal StringaConnessione As String)
        Dim cn As OleDbConnection
        

        cn = New Data.OleDb.OleDbConnection(StringaConnessione)

        cn.Open()
        Dim cmd As New OleDbCommand()
        cmd.CommandText = ("select * from Amb_Parametri ")        
        cmd.Connection = cn

        Dim ReadaAppuntamento As OleDbDataReader = cmd.ExecuteReader()
        if ReadaAppuntamento.Read  Then                        
            MastroRicavo = campodbn(ReadaAppuntamento.Item("MastroRicavo"))
            ContoRicavo= campodbn(ReadaAppuntamento.Item("ContoRicavo"))
            SottoContoRicavo = campodbn(ReadaAppuntamento.Item("SottoContoRicavo"))     
            CodiceIVA = campodb(ReadaAppuntamento.Item("CodiceIVA"))     
            IdEpersonamStruttura = campodbn(ReadaAppuntamento.Item("IdEpersonamStruttura"))     
        End If
        ReadaAppuntamento.Close

        cn.Close
    End Sub

    
    Function campodb(ByVal oggetto As Object) As String
        If IsDBNull(oggetto) Then
            Return ""
        Else
            Return oggetto
        End If
    End Function

    Function campodbD(ByVal oggetto As Object) As Date
        If IsDBNull(oggetto) Then
            Return Nothing
        Else
            Return oggetto
        End If
    End Function
    Function campodbn(ByVal oggetto As Object) As Double
        If IsDBNull(oggetto) Then
            Return 0
        Else
            Return oggetto
        End If
    End Function

End Class
