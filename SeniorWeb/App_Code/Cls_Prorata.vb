﻿Imports Microsoft.VisualBasic
Imports System.Data.OleDb
Imports System.Web.Hosting

Public Class Cls_Prorata
    Public Anno As Long
    Public PresuntoEsenti As Double
    Public PresuntoAffari As Double
    Public PresuntoPercentuale As Double
    Public DefinitivoEsenti As Double
    Public DefinitivoAffari As Double
    Public DefinitivoPercentuale As Double
    Public RegistroIVA As Integer
    Public Attivita As Integer

    Function campodbN(ByVal oggetto As Object) As Double
        If IsDBNull(oggetto) Then
            Return 0
        Else
            Return oggetto
        End If
    End Function

    Public Sub Delete(ByVal StringaConnessione As String)
        Dim cn As OleDbConnection


        cn = New Data.OleDb.OleDbConnection(StringaConnessione)

        cn.Open()
        Dim cmd As New OleDbCommand()
        cmd.CommandText = ("DELETE from Prorata Where Anno = ? And  RegistroIVA =?")
        cmd.Parameters.AddWithValue("@Anno", Anno)
        cmd.Parameters.AddWithValue("@RegistroIVA", RegistroIVA)
        cmd.Connection = cn
        cmd.ExecuteNonQuery()

        cn.Close()
    End Sub

    Public Sub Leggi(ByVal StringaConnessione As String)
        Dim cn As OleDbConnection


        cn = New Data.OleDb.OleDbConnection(StringaConnessione)

        cn.Open()
        Dim cmd As New OleDbCommand()
        cmd.CommandText = ("select * from Prorata Where Anno = ? And  RegistroIVA =?")
        cmd.Parameters.AddWithValue("@Anno", Anno)
        cmd.Parameters.AddWithValue("@RegistroIVA", RegistroIVA)
        cmd.Connection = cn
        Dim sb As StringBuilder = New StringBuilder


        Dim myPOSTreader As OleDbDataReader = cmd.ExecuteReader()
        If myPOSTreader.Read Then
            Anno = campodbN(myPOSTreader.Item("Anno"))
            PresuntoEsenti = campodbN(myPOSTreader.Item("PresuntoEsenti"))
            PresuntoAffari = campodbN(myPOSTreader.Item("PresuntoAffari"))
            PresuntoPercentuale = campodbN(myPOSTreader.Item("PresuntoPercentuale"))
            DefinitivoEsenti = campodbN(myPOSTreader.Item("DefinitivoEsenti"))
            DefinitivoAffari = campodbN(myPOSTreader.Item("DefinitivoAffari"))
            DefinitivoPercentuale = campodbN(myPOSTreader.Item("DefinitivoPercentuale"))
            RegistroIVA = campodbN(myPOSTreader.Item("RegistroIVA"))
            Attivita = campodbN(myPOSTreader.Item("Attivita"))
        End If
        myPOSTreader.Close()

        cn.Close()
    End Sub


    Public Sub Scrivi(ByVal StringaConnessione As String)
        Dim cn As OleDbConnection
        Dim MySql As String = ""

        cn = New Data.OleDb.OleDbConnection(StringaConnessione)

        cn.Open()
        Dim cmd As New OleDbCommand()
        cmd.CommandText = ("select * from Prorata Where Anno = ? And  RegistroIVA =?")
        cmd.Parameters.AddWithValue("@Anno", Anno)
        cmd.Parameters.AddWithValue("@RegistroIVA", RegistroIVA)
        cmd.Connection = cn


        Dim myPOSTreader As OleDbDataReader = cmd.ExecuteReader()
        If myPOSTreader.Read Then
            MySql = "Update Prorata Set Anno = ?,PresuntoEsenti= ?,PresuntoAffari= ?,PresuntoPercentuale= ?,DefinitivoEsenti= ?,DefinitivoAffari= ?,DefinitivoPercentuale= ?,RegistroIVA= ?,Attivita = ? Where Anno = " & Anno & " And  RegistroIVA = " & RegistroIVA

        Else
            MySql = "INSERT INTO Prorata (Anno,PresuntoEsenti,PresuntoAffari ,PresuntoPercentuale,DefinitivoEsenti,DefinitivoAffari,DefinitivoPercentuale,RegistroIVA,Attivita) VALUES (?,?,?,?,?,?,?,?,?)"
        End If

        Dim cmdIns As New OleDbCommand()
        cmdIns.CommandText = MySql
        cmdIns.Parameters.AddWithValue("@Anno", Anno)
        cmdIns.Parameters.AddWithValue("@PresuntoEsenti", PresuntoEsenti)

        cmdIns.Parameters.AddWithValue("@PresuntoAffari", PresuntoAffari)

        cmdIns.Parameters.AddWithValue("@PresuntoPercentuale", PresuntoPercentuale)
        cmdIns.Parameters.AddWithValue("@DefinitivoEsenti", DefinitivoEsenti)

        cmdIns.Parameters.AddWithValue("@DefinitivoAffari", DefinitivoAffari)
        cmdIns.Parameters.AddWithValue("@DefinitivoPercentuale", DefinitivoPercentuale)
        cmdIns.Parameters.AddWithValue("@RegistroIVA", RegistroIVA)
        cmdIns.Parameters.AddWithValue("@Attivita", Attivita)
        cmdIns.Connection = cn

        cmdIns.ExecuteNonQuery()
        myPOSTreader.Close()




        cn.Close()
    End Sub

End Class
