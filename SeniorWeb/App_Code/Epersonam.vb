﻿
Imports Microsoft.VisualBasic
Imports System
Imports System.Collections.Generic
Imports System.Web
Imports System.Web.Services
Imports System.Web.Services.Protocols
Imports System.Data.OleDb
Imports System.Web.Hosting
Imports BCrypt.Net
Imports System.Web.Script.Serialization

' <System.Web.Script.Services.ScriptService()> _
<WebService(Namespace:="http://tempuri.org/")> _
<WebServiceBinding(ConformsTo:=WsiProfiles.BasicProfile1_1)> _
<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Public Class Epersonam
    Inherits System.Web.Services.WebService


    ' ER1 utente o password errata
    ' ER2 CodiceFiscale non trovato
    ' ER3 CodiceFiscale Parente non trovato
    ' ER4 CodiceFiscale già presente
    ' ER5 Data errata
    ' ER6 Tipo movimento errato
    ' ER7 Centro Servizio non corretto
    ' WR1 Movimento non modificato
    <WebMethod()> _
    Public Function ModificaPassword(ByVal Utente As String, ByVal Password As String, ByVal NuovaPassword As String) As String
        If Trim(Utente) = "" Then
            Return "FALSE"
            Exit Function
        End If

        Dim cn As OleDbConnection
        Dim MySql As String



        cn = New Data.OleDb.OleDbConnection(Context.Application("SENIOR"))

        cn.Open()
        Dim cmd As New OleDbCommand()

        MySql = "UPDATE  UTENTE SET  ChiaveCr = ?, DataModifica = ?, Motivo = ? WHERE UTENTE Like ? "

        cmd.CommandText = MySql
        cmd.Parameters.AddWithValue("@ChiaveCr", NuovaPassword)
        cmd.Parameters.AddWithValue("@DataModifica", Now)
        cmd.Parameters.AddWithValue("@Motivo", "CambioChiave")
        cmd.Parameters.AddWithValue("@UTENTE", Utente)
        cmd.Connection = cn
        cmd.ExecuteNonQuery()



        Dim cmd1 As New OleDbCommand()

        MySql = "UPDATE  UTENTE SET  ChiaveCr = ?, DataModifica = ?, Motivo = ? WHERE UTENTE Like ? "

        cmd1.CommandText = MySql
        cmd1.Parameters.AddWithValue("@ChiaveCr", NuovaPassword)
        cmd1.Parameters.AddWithValue("@DataModifica", Now)
        cmd1.Parameters.AddWithValue("@Motivo", "CambioChiave")
        cmd1.Parameters.AddWithValue("@UTENTE", Utente & "<%>")
        cmd1.Connection = cn
        cmd1.ExecuteNonQuery()

        cn.Close()


        'Dim DbC As New Cls_Login

        'DbC.Utente = Utente
        'DbC.Chiave = Password
        'DbC.LeggiSP(Context.Application("SENIOR"))



        'If DbC.Ospiti = "" Or IsNothing(DbC.Ospiti) Then
        '    'Context.Response.StatusCode = 401
        '    Return "ER1"
        'End If

        'REM   DbC.Chiave = NuovaPassword
        'DbC.ChiaveCr = NuovaPassword

        'DbC.Scrivi(Context.Application("SENIOR"), "CambioChiave")
        Return "OK"
    End Function



    <WebMethod()> _
    Public Function ModificaOspite(ByVal Utente As String, ByVal Password As String, ByVal CodiceFiscale As String, ByVal Cognome As String, ByVal Nome As String, ByVal DataNascita As String, ByVal provincianascita As String, ByVal comunenascita As String, ByVal CognomeNomePadre As String, ByVal CognomeNomeMadre As String, ByVal CognomeNomeConiuge As String, ByVal ProfessioneConiuge As String, ByVal Nazionalita As String, ByVal NumeroDocumento As String, ByVal DataDocumento As String, ByVal TipoDocumento As String, ByVal Emettitore As String, ByVal Scadenza As String, ByVal DataDecesso As String, ByVal LuogoDecesso As String, ByVal StrutturaProvenienza As String, ByVal AssistenteSociale As String, ByVal Distretto As String, ByVal CodiceMedico As String, ByVal codicesanitario As String, ByVal codiceesenzioneticket As String, ByVal datadalesenzioneticket As String, ByVal dataalesenzioneticket As String, ByVal IndirizzoRecapito As String, ByVal CapRecapito As String, ByVal ProvinciaRecapito As String, ByVal ComuneRecapito As String, ByVal NewCodiceFiscale As String, ByVal Telefono1 As String, ByVal Telefono2 As String, ByVal Sesso As String) As String



        Dim DbC As New Cls_Login

        DbC.Utente = Utente
        DbC.Chiave = Password
        DbC.Leggi(Context.Application("SENIOR"))

        If DbC.Ospiti = "" Or IsNothing(DbC.Ospiti) Then
            Context.Response.StatusCode = 401
            Return "ER1"
        End If

        Dim Anag As New ClsOspite


        Anag.LeggiPerCodiceFiscale(DbC.Ospiti, CodiceFiscale)

        If Anag.CODICEFISCALE <> CodiceFiscale Then
            Context.Response.StatusCode = 404
            Return "ER2"
        End If
        If Anag.CodiceOspite = 0 Then
            Context.Response.StatusCode = 404
            Return "ER2"
        End If


        If NewCodiceFiscale <> "*" Then
            Anag.CODICEFISCALE = NewCodiceFiscale
        End If

        If Cognome <> "*" Then
            Anag.CognomeOspite = Cognome
        End If
        If Nome <> "*" Then
            Anag.NomeOspite = Nome
        End If

        Anag.Nome = Anag.CognomeOspite & " " & Anag.NomeOspite
        If DataNascita <> "*" Then
            If IsDate(DataNascita) Then
                Anag.DataNascita = DataNascita
            End If
        End If

        If provincianascita <> "*" Then
            Anag.ProvinciaDiNascita = provincianascita
        End If
        If comunenascita <> "*" Then
            Anag.ComuneDiNascita = comunenascita
        End If

        If Sesso <> "*" Then
            If Sesso = "1" Then
                Anag.Sesso = "M"
            End If
            If Sesso = "2" Then
                Anag.Sesso = "F"
            End If
        End If

        If CognomeNomePadre <> "*" Then
            Anag.NOMEPADRE = CognomeNomePadre
        End If
        If CognomeNomeMadre <> "*" Then
            Anag.NOMEMADRE = CognomeNomeMadre
        End If
        If CognomeNomeConiuge <> "*" Then
            Anag.NOMECONIUGE = CognomeNomeConiuge
        End If
        If CognomeNomeConiuge <> "*" Then
            Anag.NOMECONIUGE = CognomeNomeConiuge
        End If

        If ProfessioneConiuge <> "*" Then
            Anag.PROFCONIUGE = ProfessioneConiuge
        End If
        If Nazionalita <> "*" Then
            Anag.Nazionalita = Nazionalita
        End If
        If NumeroDocumento <> "*" Then
            Anag.NumeroDocumento = NumeroDocumento
        End If
        If DataDocumento <> "*" Then
            If IsDate(DataDocumento) Then
                Anag.DataDocumento = DataDocumento
            End If
        End If

        If Telefono1 <> "*" Then
            Anag.TELEFONO1 = Telefono1
        End If
        If Telefono2 <> "*" Then
            Anag.TELEFONO2 = Telefono2
        End If


        If TipoDocumento <> "*" Then
            Anag.TipoDocumento = TipoDocumento
        End If
        If Emettitore <> "*" Then
            Anag.EmettitoreDocumento = Emettitore
        End If
        If Scadenza <> "*" Then
            If IsDate(Scadenza) Then
                Anag.ScadenzaDocumento = Scadenza
            End If
        End If
        If DataDecesso <> "*" Then
            If IsDate(DataDecesso) Then
                Anag.DATAMORTE = DataDecesso
            End If
        End If
        If LuogoDecesso <> "*" Then
            Anag.LuogoMorte = LuogoDecesso
        End If
        If StrutturaProvenienza <> "*" Then
            Anag.StrutturaProvenienza = StrutturaProvenienza
        End If
        If AssistenteSociale <> "*" Then
            Anag.AssistenteSociale = AssistenteSociale
        End If
        If Distretto <> "*" Then
            Anag.Distretto = Distretto
        End If
        If CodiceMedico <> "*" Then
            Anag.CodiceMedico = CodiceMedico
        End If
        If codicesanitario <> "*" Then
            Anag.CodiceSanitario = codicesanitario
        End If
        If codiceesenzioneticket <> "*" Then
            Anag.CodiceTicket = codiceesenzioneticket
        End If
        If datadalesenzioneticket <> "*" Then
            If IsDate(datadalesenzioneticket) Then
                Anag.DataRilascio = datadalesenzioneticket
            End If
        End If
        If dataalesenzioneticket <> "*" Then
            If IsDate(dataalesenzioneticket) Then
                Anag.DataScadenza = dataalesenzioneticket
            End If
        End If
        If IndirizzoRecapito <> "*" Then
            Anag.RESIDENZAINDIRIZZO1 = IndirizzoRecapito
        End If
        If CapRecapito <> "*" Then
            Anag.RESIDENZACAP1 = CapRecapito
        End If
        If ProvinciaRecapito <> "*" Then
            Anag.RESIDENZAPROVINCIA1 = ProvinciaRecapito
        End If
        If ComuneRecapito <> "*" Then
            Anag.RESIDENZACOMUNE1 = ComuneRecapito
        End If

        Anag.UTENTE = Utente
        Anag.ScriviOspite(DbC.Ospiti)

        Return "OK"
    End Function



    <WebMethod()> _
    Public Function Importi(ByVal Utente As String, ByVal Password As String, ByVal CodiceFiscale As String, ByVal CentroServizio As String, ByVal DataDal As String, ByVal DataAl As String) As String
        Dim DataInizio As Date
        Dim DataFine As Date
        Dim i As Integer



        Dim DbC As New Cls_Login

        DbC.Utente = Utente
        DbC.Chiave = Password
        DbC.Leggi(Context.Application("SENIOR"))

        If DbC.Ospiti = "" Or IsNothing(DbC.Ospiti) Then
            Context.Response.StatusCode = 401
            Return "ER1"
        End If

        Dim CS As New Cls_CentroServizio

        CS.EPersonam = CentroServizio
        CS.LeggiEpersonam(DbC.Ospiti, CS.EPersonam, True)
        If CS.DESCRIZIONE.Trim = "" Then
            Context.Response.StatusCode = 404
            Return "ER7"
        End If

        Dim Anag As New ClsOspite


        Anag.LeggiPerCodiceFiscale(DbC.Ospiti, CodiceFiscale)

        If Anag.CODICEFISCALE <> CodiceFiscale Then
            Context.Response.StatusCode = 404
            Return "ER2"
        End If
        If Anag.CodiceOspite = 0 Then
            Context.Response.StatusCode = 404
            Return "ER2"
        End If


        If DataDal <> "*" Then
            If IsDate(DataDal) Then
                DataInizio = DataDal
            End If
        End If
        If DataAl <> "*" Then
            If IsDate(DataAl) Then
                DataFine = DataAl
            End If
        End If
        Dim V As New VImportiGiornalieri
        Dim k As New Cls_CalcoloRette

        k.STRINGACONNESSIONEDB = DbC.Ospiti
        k.ApriDB(DbC.Ospiti, DbC.OspitiAccessori)

        For i = 0 To DateDiff(DateInterval.Day, DataInizio, DataFine)
            V.QuotaOspite(i) = k.QuoteGiornaliere(CS.CENTROSERVIZIO, Anag.CodiceOspite, "O", 0, DateAdd(DateInterval.Day, i, DataInizio))
            V.QuotaParenti(i) = k.QuoteGiornaliere(CS.CENTROSERVIZIO, Anag.CodiceOspite, "P", 1, DateAdd(DateInterval.Day, i, DataInizio)) + k.QuoteGiornaliere(CentroServizio, Anag.CodiceOspite, "P", 2, DateAdd(DateInterval.Day, i, DataInizio)) + k.QuoteGiornaliere(CentroServizio, Anag.CodiceOspite, "P", 3, DateAdd(DateInterval.Day, i, DataInizio))
            V.QuotaComune(i) = k.QuoteGiornaliere(CS.CENTROSERVIZIO, Anag.CodiceOspite, "C", 0, DateAdd(DateInterval.Day, i, DataInizio))
            V.QuotaUsl(i) = k.QuoteGiornaliere(CS.CENTROSERVIZIO, Anag.CodiceOspite, "R", 0, DateAdd(DateInterval.Day, i, DataInizio))
            V.QuotaAltriEnti(i) = k.QuoteGiornaliere(CS.CENTROSERVIZIO, Anag.CodiceOspite, "J", 0, DateAdd(DateInterval.Day, i, DataInizio))
        Next

        Dim serializer As JavaScriptSerializer
        serializer = New JavaScriptSerializer()

        Dim Serializzazione As String

        Serializzazione = serializer.Serialize(V)
        Me.Context.Response.StatusCode = 200
        Return Serializzazione
    End Function

    Public Function GiorniMese(ByVal Mese As Byte, ByVal Anno As Integer) As Byte
        If Mese = 1 Or Mese = 3 Or Mese = 5 Or Mese = 7 Or Mese = 8 Or _
           Mese = 10 Or Mese = 12 Then
            GiorniMese = 31
        Else
            If Mese <> 2 Then
                GiorniMese = 30
            Else
                If Day(DateSerial(Anno, Mese, 29)) = 29 Then
                    GiorniMese = 29
                Else
                    GiorniMese = 28
                End If
            End If
        End If
    End Function


    <WebMethod()> _
    Public Function ImportoMedio_ArrayCF(ByVal Utente As String, ByVal Password As String, ByVal CentroServizio As String, ByVal CodiceFiscale As String, ByVal Anno As Integer, ByVal Trimestre As Integer) As String
        Dim CodiceFiscali(200) As String
        Dim MeseDa As Integer
        Dim MeseA As Integer

        If Trimestre = 1 Then
            MeseDa = 1
            MeseA = 3
        End If
        If Trimestre = 2 Then
            MeseDa = 4
            MeseA = 6
        End If
        If Trimestre = 3 Then
            MeseDa = 7
            MeseA = 9
        End If
        If Trimestre = 4 Then
            MeseDa = 10
            MeseA = 12
        End If

        CodiceFiscali = Split(CodiceFiscale, ",")

        Dim DbC As New Cls_Login

        DbC.Utente = Utente
        DbC.Chiave = Password
        DbC.Leggi(Context.Application("SENIOR"))

        If DbC.Ospiti = "" Or IsNothing(DbC.Ospiti) Then
            Context.Response.StatusCode = 401
            Return "ER1"
        End If




        Dim cn As OleDbConnection
        Dim Trovato As Boolean = False
        Dim V As New QuoteMedieOspitiCF


        Dim k As New Cls_CalcoloRette

        cn = New Data.OleDb.OleDbConnection(DbC.Ospiti)

        cn.Open()

        Dim AppoggioImportoOspite As Double
        Dim AppoggioNumeroOspite As Double

        Dim AppoggioImportoParenti As Double
        Dim AppoggioNumeroParenti As Double

        Dim AppoggioImportoComune As Double
        Dim AppoggioNumeroComune As Double

        Dim AppoggioImportoUsl As Double
        Dim AppoggioNumeroUsl As Double

        Dim AppoggioImportoQuotaAltriEnti As Double
        Dim AppoggioNumeroQuotaAltriEnti As Double

        Dim I As Integer = 0
        Dim Indice As Integer = 0

        k.STRINGACONNESSIONEDB = DbC.Ospiti
        k.ApriDB(DbC.Ospiti, DbC.OspitiAccessori)

        Dim cmdRs As New OleDbCommand()
        Dim KS As New Cls_Epersonam

        Dim Centro1 As String = ""
        Dim Centro2 As String = ""

        cmdRs.CommandText = ("select * from TABELLACENTROSERVIZIO Where  EPersonam = " & KS.RendiWardid(CentroServizio, DbC.Ospiti) & " or EPersonamN = " & KS.RendiWardid(CentroServizio, DbC.Ospiti))

        cmdRs.Connection = cn

        I = 0
        Dim RsRead As OleDbDataReader = cmdRs.ExecuteReader()
        If RsRead.Read Then
            Centro1 = campodb(RsRead.Item("CENTROSERVIZIO"))
            Centro2 = Centro1
            If RsRead.Read Then
                Centro2 = campodb(RsRead.Item("CENTROSERVIZIO"))
            End If
        End If
        RsRead.Close()
        If Centro1 = Centro2 Then
            Centro2 = ""
        End If

        For Indice = 0 To CodiceFiscali.Length - 1


            Dim Anag As New ClsOspite
            Dim TCodiceFiscale As String = CodiceFiscali(Indice)


            If TCodiceFiscale <> "" Then

                Anag.LeggiPerCodiceFiscale(DbC.Ospiti, TCodiceFiscale)

                If Anag.CODICEFISCALE <> TCodiceFiscale Then
                    Context.Response.StatusCode = 404
                    Return "ER2"
                End If
                If Anag.CodiceOspite = 0 Then
                    Context.Response.StatusCode = 404
                    Return "ER2"
                End If
                V.Singolo(Indice) = New QuoteMedieOspiti
                V.Singolo(Indice).CF = TCodiceFiscale


                Dim ImportoOspite As Double = 0
                Dim GiorniOspite As Integer = 0

                Dim ImportoParente As Double = 0
                Dim GiorniParente As Integer = 0

                Dim ImportoRegione As Double = 0
                Dim GiorniRegione As Integer = 0

                Dim ImportoComune As Double = 0
                Dim GiorniComune As Integer = 0

                Dim ImportoJolly As Double = 0
                Dim GiorniJolly As Integer = 0




                Dim cmdO As New OleDbCommand()
                cmdO.CommandText = ("select sum(Importo) as importo, sum(giorni) as xgiorni from RetteOspite where  CodiceOspite = " & Anag.CodiceOspite & " And (CentroServizio = ? Or CentroServizio = ?) And Anno = ? And Mese >= ? And Mese <= ?  And (Elemento = 'RGP' OR Elemento = 'RGA' OR Elemento = 'RPX')  ")
                cmdO.Connection = cn
                cmdO.Parameters.AddWithValue("@CENTROSERVIZIO", Centro1)
                cmdO.Parameters.AddWithValue("@CENTROSERVIZIO2", Centro2)
                cmdO.Parameters.AddWithValue("@Anno", Anno)
                cmdO.Parameters.AddWithValue("@Mese", MeseDa)
                cmdO.Parameters.AddWithValue("@Mese", MeseA)
                Dim ReadO As OleDbDataReader = cmdO.ExecuteReader()
                If ReadO.Read Then
                    ImportoOspite = campodbN(ReadO.Item("importo"))
                    GiorniOspite = campodbN(ReadO.Item("xgiorni"))
                End If
                ReadO.Close()



                Dim cmdP As New OleDbCommand()
                cmdP.CommandText = ("select sum(Importo) as importo, sum(giorni) as xgiorni from RetteParente where  CodiceOspite = " & Anag.CodiceOspite & " And (CentroServizio = ? Or CentroServizio = ?) And Anno = ? And Mese >= ? And Mese <= ? And (Elemento = 'RGP' OR Elemento = 'RGA' OR Elemento = 'RPX')  ")
                cmdP.Connection = cn
                cmdP.Parameters.AddWithValue("@CENTROSERVIZIO1", Centro1)
                cmdP.Parameters.AddWithValue("@CENTROSERVIZIO2", Centro2)
                cmdP.Parameters.AddWithValue("@Anno", Anno)
                cmdP.Parameters.AddWithValue("@Mese", MeseDa)
                cmdP.Parameters.AddWithValue("@Mese", MeseA)
                Dim ReadP As OleDbDataReader = cmdP.ExecuteReader()
                If ReadP.Read Then
                    ImportoParente = campodbN(ReadP.Item("importo"))
                    GiorniParente = campodbN(ReadP.Item("xgiorni"))
                End If
                ReadP.Close()



                Dim cmdC As New OleDbCommand()
                cmdC.CommandText = ("select sum(Importo) as importo, sum(giorni) as xgiorni from RettecOMUNE where  CodiceOspite = " & Anag.CodiceOspite & " And (CentroServizio = ? Or CentroServizio = ?) And Anno = ? And Mese >= ? And Mese <= ? And (Elemento = 'RGP' OR Elemento = 'RGA' OR Elemento = 'RPX')  ")
                cmdC.Connection = cn
                cmdC.Parameters.AddWithValue("@CENTROSERVIZIO1", Centro1)
                cmdC.Parameters.AddWithValue("@CENTROSERVIZIO1", Centro2)
                cmdC.Parameters.AddWithValue("@Anno", Anno)
                cmdC.Parameters.AddWithValue("@Mese", MeseDa)
                cmdC.Parameters.AddWithValue("@Mese", MeseA)
                Dim ReadC As OleDbDataReader = cmdC.ExecuteReader()
                If ReadC.Read Then
                    ImportoComune = campodbN(ReadC.Item("importo"))
                    GiorniComune = campodbN(ReadC.Item("xgiorni"))
                End If
                ReadC.Close()

                Dim cmdJ As New OleDbCommand()
                cmdJ.CommandText = ("select sum(Importo) as importo, sum(giorni) as xgiorni from RettejOLLY where  CodiceOspite = " & Anag.CodiceOspite & " And (CentroServizio = ? Or CentroServizio = ?) And Anno = ? And Mese >= ? And Mese <= ? And (Elemento = 'RGP' OR Elemento = 'RGA' OR Elemento = 'RPX')  ")
                cmdJ.Connection = cn
                cmdJ.Parameters.AddWithValue("@CENTROSERVIZIO1", Centro1)
                cmdJ.Parameters.AddWithValue("@CENTROSERVIZIO2", Centro2)
                cmdJ.Parameters.AddWithValue("@Anno", Anno)
                cmdJ.Parameters.AddWithValue("@Mese", MeseDa)
                cmdJ.Parameters.AddWithValue("@Mese", MeseA)
                Dim ReadJ As OleDbDataReader = cmdJ.ExecuteReader()
                If ReadJ.Read Then
                    ImportoJolly = campodbN(ReadJ.Item("importo"))
                    GiorniJolly = campodbN(ReadJ.Item("xgiorni"))
                End If
                ReadJ.Close()


                Dim cmdR As New OleDbCommand()
                cmdR.CommandText = ("select sum(Importo) as importo, sum(giorni) as xgiorni from RetteRegione where  CodiceOspite = " & Anag.CodiceOspite & " And (CentroServizio = ? OR CentroServizio = ? ) And Anno = ? And Mese >= ? And Mese <= ? And (Elemento = 'RGP' OR Elemento = 'RGA' OR Elemento = 'RPX')  ")
                cmdR.Connection = cn
                cmdR.Parameters.AddWithValue("@CENTROSERVIZIO1", Centro1)
                cmdR.Parameters.AddWithValue("@CENTROSERVIZIO2", Centro2)
                cmdR.Parameters.AddWithValue("@Anno", Anno)
                cmdR.Parameters.AddWithValue("@Mese", MeseDa)
                cmdR.Parameters.AddWithValue("@Mese", MeseA)
                Dim ReadR As OleDbDataReader = cmdR.ExecuteReader()
                If ReadR.Read Then
                    ImportoRegione = campodbN(ReadR.Item("importo"))
                    GiorniRegione = campodbN(ReadR.Item("xgiorni"))
                End If
                ReadR.Close()



                If ImportoOspite <> 0 Or ImportoParente <> 0 Or ImportoComune <> 0 Or ImportoRegione <> 0 Or ImportoJolly <> 0 Then
                    If ImportoOspite > 0 Then
                        V.Singolo(Indice).QuotaOspite = Math.Round(ImportoOspite / GiorniOspite, 2)
                    Else
                        V.Singolo(Indice).QuotaOspite = 0
                    End If
                    If ImportoParente > 0 Then
                        V.Singolo(Indice).QuotaParenti = Math.Round(ImportoParente / GiorniParente, 2)
                    Else
                        V.Singolo(Indice).QuotaParenti = 0
                    End If
                    If ImportoComune > 0 Then
                        V.Singolo(Indice).QuotaComune = Math.Round(ImportoComune / GiorniComune, 2)
                    Else
                        V.Singolo(Indice).QuotaComune = 0
                    End If
                    If ImportoRegione > 0 Then
                        V.Singolo(Indice).QuotaUsl = Math.Round(ImportoRegione / GiorniRegione, 2)
                    Else
                        V.Singolo(Indice).QuotaUsl = 0
                    End If
                    If ImportoJolly > 0 Then
                        V.Singolo(Indice).QuotaAltriEnti = Math.Round(ImportoJolly / GiorniJolly, 2)
                    Else
                        V.Singolo(Indice).QuotaAltriEnti = 0
                    End If
                Else
                    For I = 1 To DateDiff(DateInterval.Day, DateSerial(Anno, MeseDa, 1), DateSerial(Anno, MeseDa, GiorniMese(MeseDa, Anno)))
                        Dim Appoggio As Double


                        Appoggio = k.QuoteGiornaliere(Centro1, Anag.CodiceOspite, "O", 0, DateSerial(Anno, MeseDa, I)) + k.QuoteGiornaliere(Centro2, Anag.CodiceOspite, "O", 0, DateSerial(Anno, MeseDa, I))
                        AppoggioImportoOspite = AppoggioImportoOspite + Appoggio
                        If Appoggio > 0 Then
                            AppoggioNumeroOspite = AppoggioNumeroOspite + 1
                        End If

                        Appoggio = k.QuoteGiornaliere(Centro1, Anag.CodiceOspite, "P", 1, DateSerial(Anno, MeseDa, I)) + k.QuoteGiornaliere(Centro1, Anag.CodiceOspite, "P", 2, DateSerial(Anno, MeseDa, I)) + k.QuoteGiornaliere(Centro1, Anag.CodiceOspite, "P", 3, DateSerial(Anno, MeseDa, I))
                        Appoggio = Appoggio + k.QuoteGiornaliere(Centro2, Anag.CodiceOspite, "P", 1, DateSerial(Anno, MeseDa, I)) + k.QuoteGiornaliere(Centro2, Anag.CodiceOspite, "P", 2, DateSerial(Anno, MeseDa, I)) + k.QuoteGiornaliere(Centro2, Anag.CodiceOspite, "P", 3, DateSerial(Anno, MeseDa, I))
                        AppoggioImportoParenti = AppoggioImportoParenti + Appoggio
                        If Appoggio > 0 Then
                            AppoggioNumeroParenti = AppoggioNumeroParenti + 1
                        End If


                        Appoggio = k.QuoteGiornaliere(Centro1, Anag.CodiceOspite, "C", 0, DateSerial(Anno, MeseDa, I)) + k.QuoteGiornaliere(Centro2, Anag.CodiceOspite, "C", 0, DateSerial(Anno, MeseDa, I))
                        AppoggioImportoComune = AppoggioImportoComune + Appoggio
                        If Appoggio > 0 Then
                            AppoggioNumeroComune = AppoggioNumeroComune + 1
                        End If


                        Appoggio = k.QuoteGiornaliere(Centro1, Anag.CodiceOspite, "R", 0, DateSerial(Anno, MeseDa, I)) + k.QuoteGiornaliere(Centro2, Anag.CodiceOspite, "R", 0, DateSerial(Anno, MeseDa, I))
                        AppoggioImportoUsl = AppoggioImportoUsl + Appoggio
                        If Appoggio > 0 Then
                            AppoggioNumeroUsl = AppoggioNumeroUsl + 1
                        End If

                        Appoggio = k.QuoteGiornaliere(Centro1, Anag.CodiceOspite, "J", 0, DateSerial(Anno, MeseDa, I)) + k.QuoteGiornaliere(Centro2, Anag.CodiceOspite, "J", 0, DateSerial(Anno, MeseDa, I))
                        AppoggioImportoQuotaAltriEnti = AppoggioImportoQuotaAltriEnti + Appoggio
                        If Appoggio > 0 Then
                            AppoggioNumeroQuotaAltriEnti = AppoggioNumeroQuotaAltriEnti + 1
                        End If
                    Next
                    If AppoggioNumeroOspite = 0 Then
                        V.Singolo(Indice).QuotaOspite = 0
                    Else
                        V.Singolo(Indice).QuotaOspite = Math.Round(AppoggioImportoOspite / AppoggioNumeroOspite, 2)
                    End If
                    If AppoggioNumeroParenti = 0 Then
                        V.Singolo(Indice).QuotaParenti = 0
                    Else
                        V.Singolo(Indice).QuotaParenti = Math.Round(AppoggioImportoParenti / AppoggioNumeroParenti, 2)
                    End If
                    If AppoggioNumeroComune = 0 Then
                        V.Singolo(Indice).QuotaComune = 0
                    Else
                        V.Singolo(Indice).QuotaComune = Math.Round(AppoggioImportoComune / AppoggioNumeroComune, 2)
                    End If
                    If AppoggioNumeroUsl = 0 Then
                        V.Singolo(Indice).QuotaUsl = 0
                    Else
                        V.Singolo(Indice).QuotaUsl = Math.Round(AppoggioImportoUsl / AppoggioNumeroUsl, 2)
                    End If
                    If AppoggioNumeroQuotaAltriEnti = 0 Then
                        V.Singolo(Indice).QuotaAltriEnti = 0
                    Else
                        V.Singolo(Indice).QuotaAltriEnti = Math.Round(AppoggioImportoQuotaAltriEnti / AppoggioNumeroQuotaAltriEnti, 2)
                    End If
                End If
            End If
        Next

        cn.Close()
        Dim serializer As JavaScriptSerializer
        serializer = New JavaScriptSerializer()

        Dim Serializzazione As String

        Serializzazione = serializer.Serialize(V)
        Me.Context.Response.StatusCode = 200
        Return Serializzazione
    End Function


    <WebMethod()> _
    Public Function ImportoMedio(ByVal Utente As String, ByVal Password As String, ByVal CodiceFiscale As String, ByVal CentroServizio As String, ByVal DataDal As String, ByVal DataAl As String) As String
        Dim DataInizio As Date
        Dim DataFine As Date
        Dim i As Integer



        Dim DbC As New Cls_Login

        DbC.Utente = Utente
        DbC.Chiave = Password
        DbC.Leggi(Context.Application("SENIOR"))

        If DbC.Ospiti = "" Or IsNothing(DbC.Ospiti) Then
            Context.Response.StatusCode = 401
            Return "ER1"
        End If

        Dim CS As New Cls_CentroServizio

        CS.EPersonam = CentroServizio
        CS.LeggiEpersonam(DbC.Ospiti, CS.EPersonam, True)
        If CS.DESCRIZIONE.Trim = "" Then
            Context.Response.StatusCode = 404
            Return "ER7"
        End If

        Dim Anag As New ClsOspite


        Anag.LeggiPerCodiceFiscale(DbC.Ospiti, CodiceFiscale)

        If Anag.CODICEFISCALE <> CodiceFiscale Then
            Context.Response.StatusCode = 404
            Return "ER2"
        End If
        If Anag.CodiceOspite = 0 Then
            Context.Response.StatusCode = 404
            Return "ER2"
        End If


        If DataDal <> "*" Then
            If IsDate(DataDal) Then
                DataInizio = DataDal
            End If
        End If
        If DataAl <> "*" Then
            If IsDate(DataAl) Then
                DataFine = DataAl
            End If
        End If
        Dim V As New QuoteMedie
        Dim k As New Cls_CalcoloRette
        Dim AppoggioImportoOspite As Double
        Dim AppoggioNumeroOspite As Double

        Dim AppoggioImportoParenti As Double
        Dim AppoggioNumeroParenti As Double

        Dim AppoggioImportoComune As Double
        Dim AppoggioNumeroComune As Double

        Dim AppoggioImportoUsl As Double
        Dim AppoggioNumeroUsl As Double

        Dim AppoggioImportoQuotaAltriEnti As Double
        Dim AppoggioNumeroQuotaAltriEnti As Double


        k.STRINGACONNESSIONEDB = DbC.Ospiti
        k.ApriDB(DbC.Ospiti, DbC.OspitiAccessori)

        For i = 0 To DateDiff(DateInterval.Day, DataInizio, DataFine)
            Dim Appoggio As Double


            Appoggio = k.QuoteGiornaliere(CS.CENTROSERVIZIO, Anag.CodiceOspite, "O", 0, DateAdd(DateInterval.Day, i, DataInizio))
            AppoggioImportoOspite = AppoggioImportoOspite + Appoggio
            If Appoggio > 0 Then
                AppoggioNumeroOspite = AppoggioNumeroOspite + 1
            End If

            Appoggio = k.QuoteGiornaliere(CS.CENTROSERVIZIO, Anag.CodiceOspite, "P", 1, DateAdd(DateInterval.Day, i, DataInizio)) + k.QuoteGiornaliere(CentroServizio, Anag.CodiceOspite, "P", 2, DateAdd(DateInterval.Day, i, DataInizio)) + k.QuoteGiornaliere(CentroServizio, Anag.CodiceOspite, "P", 3, DateAdd(DateInterval.Day, i, DataInizio))
            AppoggioImportoParenti = AppoggioImportoParenti + Appoggio
            If Appoggio > 0 Then
                AppoggioNumeroParenti = AppoggioNumeroParenti + 1
            End If


            Appoggio = k.QuoteGiornaliere(CS.CENTROSERVIZIO, Anag.CodiceOspite, "C", 0, DateAdd(DateInterval.Day, i, DataInizio))
            AppoggioImportoComune = AppoggioImportoComune + Appoggio
            If Appoggio > 0 Then
                AppoggioNumeroComune = AppoggioNumeroComune + 1
            End If


            Appoggio = k.QuoteGiornaliere(CS.CENTROSERVIZIO, Anag.CodiceOspite, "R", 0, DateAdd(DateInterval.Day, i, DataInizio))
            AppoggioImportoUsl = AppoggioImportoUsl + Appoggio
            If Appoggio > 0 Then
                AppoggioNumeroUsl = AppoggioNumeroUsl + 1
            End If

            Appoggio = k.QuoteGiornaliere(CS.CENTROSERVIZIO, Anag.CodiceOspite, "J", 0, DateAdd(DateInterval.Day, i, DataInizio))
            AppoggioImportoQuotaAltriEnti = AppoggioImportoQuotaAltriEnti + Appoggio
            If Appoggio > 0 Then
                AppoggioNumeroQuotaAltriEnti = AppoggioNumeroQuotaAltriEnti + 1
            End If
        Next

        V.QuotaOspite = Math.Round(AppoggioImportoOspite / AppoggioNumeroOspite, 2)
        V.QuotaParenti = Math.Round(AppoggioImportoParenti / AppoggioNumeroParenti, 2)
        V.QuotaComune = Math.Round(AppoggioImportoComune / AppoggioNumeroComune, 2)
        V.QuotaUsl = Math.Round(AppoggioImportoUsl / AppoggioNumeroUsl, 2)
        V.QuotaAltriEnti = Math.Round(AppoggioImportoQuotaAltriEnti / AppoggioNumeroQuotaAltriEnti, 2)


        Dim serializer As JavaScriptSerializer
        serializer = New JavaScriptSerializer()

        Dim Serializzazione As String

        Serializzazione = serializer.Serialize(V)
        Me.Context.Response.StatusCode = 200
        Return Serializzazione
    End Function


    <WebMethod()> _
    Public Function ImportoMedioAltriEnti(ByVal Utente As String, ByVal Password As String, ByVal CodiceFiscale As String, ByVal CentroServizio As String, ByVal DataDal As String, ByVal DataAl As String) As Double
        Dim DataInizio As Date
        Dim DataFine As Date
        Dim i As Integer



        Dim DbC As New Cls_Login

        DbC.Utente = Utente
        DbC.Chiave = Password
        DbC.Leggi(Context.Application("SENIOR"))

        If DbC.Ospiti = "" Or IsNothing(DbC.Ospiti) Then
            Context.Response.StatusCode = 401
            Return "ER1"
        End If

        Dim CS As New Cls_CentroServizio

        CS.EPersonam = CentroServizio
        CS.LeggiEpersonam(DbC.Ospiti, CS.EPersonam, True)
        If CS.DESCRIZIONE.Trim = "" Then
            Context.Response.StatusCode = 404
            Return "ER7"
        End If

        Dim Anag As New ClsOspite


        Anag.LeggiPerCodiceFiscale(DbC.Ospiti, CodiceFiscale)

        If Anag.CODICEFISCALE <> CodiceFiscale Then
            Context.Response.StatusCode = 404
            Return "ER2"
        End If
        If Anag.CodiceOspite = 0 Then
            Context.Response.StatusCode = 404
            Return "ER2"
        End If


        If DataDal <> "*" Then
            If IsDate(DataDal) Then
                DataInizio = DataDal
            End If
        End If
        If DataAl <> "*" Then
            If IsDate(DataAl) Then
                DataFine = DataAl
            End If
        End If
        Dim V As New VImportiGiornalieri
        Dim k As New Cls_CalcoloRette

        k.STRINGACONNESSIONEDB = DbC.Ospiti
        k.ApriDB(DbC.Ospiti, DbC.OspitiAccessori)

        Dim Importo As Double = 0
        Dim Numero As Integer = 0

        For i = 0 To DateDiff(DateInterval.Day, DataInizio, DataFine)
            Dim Appoggio As Double = 0

            Appoggio = k.QuoteGiornaliere(CS.CENTROSERVIZIO, Anag.CodiceOspite, "J", 0, DateAdd(DateInterval.Day, i, DataInizio))

            If Appoggio > 0 Then
                Importo = Importo + Appoggio
                Numero = Numero + 1
            End If

            'V.QuotaComune(i) = 
            'V.QuotaUsl(i) = k.QuoteGiornaliere(CS.CENTROSERVIZIO, Anag.CodiceOspite, "R", 0, DateAdd(DateInterval.Day, i, DataInizio))
            'V.QuotaAltriEnti(i) = k.QuoteGiornaliere(CS.CENTROSERVIZIO, Anag.CodiceOspite, "J", 0, DateAdd(DateInterval.Day, i, DataInizio))
        Next


        Return Math.Round(Importo / Numero, 2)
    End Function



    <WebMethod()> _
    Public Function ImportoMedioRegione(ByVal Utente As String, ByVal Password As String, ByVal CodiceFiscale As String, ByVal CentroServizio As String, ByVal DataDal As String, ByVal DataAl As String) As Double
        Dim DataInizio As Date
        Dim DataFine As Date
        Dim i As Integer



        Dim DbC As New Cls_Login

        DbC.Utente = Utente
        DbC.Chiave = Password
        DbC.Leggi(Context.Application("SENIOR"))

        If DbC.Ospiti = "" Or IsNothing(DbC.Ospiti) Then
            Context.Response.StatusCode = 401
            Return "ER1"
        End If

        Dim CS As New Cls_CentroServizio

        CS.EPersonam = CentroServizio
        CS.LeggiEpersonam(DbC.Ospiti, CS.EPersonam, True)
        If CS.DESCRIZIONE.Trim = "" Then
            Context.Response.StatusCode = 404
            Return "ER7"
        End If

        Dim Anag As New ClsOspite


        Anag.LeggiPerCodiceFiscale(DbC.Ospiti, CodiceFiscale)

        If Anag.CODICEFISCALE <> CodiceFiscale Then
            Context.Response.StatusCode = 404
            Return "ER2"
        End If
        If Anag.CodiceOspite = 0 Then
            Context.Response.StatusCode = 404
            Return "ER2"
        End If


        If DataDal <> "*" Then
            If IsDate(DataDal) Then
                DataInizio = DataDal
            End If
        End If
        If DataAl <> "*" Then
            If IsDate(DataAl) Then
                DataFine = DataAl
            End If
        End If
        Dim V As New VImportiGiornalieri
        Dim k As New Cls_CalcoloRette

        k.STRINGACONNESSIONEDB = DbC.Ospiti
        k.ApriDB(DbC.Ospiti, DbC.OspitiAccessori)

        Dim Importo As Double = 0
        Dim Numero As Integer = 0

        For i = 0 To DateDiff(DateInterval.Day, DataInizio, DataFine)
            Dim Appoggio As Double = 0

            Appoggio = k.QuoteGiornaliere(CS.CENTROSERVIZIO, Anag.CodiceOspite, "R", 0, DateAdd(DateInterval.Day, i, DataInizio))

            If Appoggio > 0 Then
                Importo = Importo + Appoggio
                Numero = Numero + 1
            End If

            'V.QuotaComune(i) = 
            'V.QuotaUsl(i) = k.QuoteGiornaliere(CS.CENTROSERVIZIO, Anag.CodiceOspite, "R", 0, DateAdd(DateInterval.Day, i, DataInizio))
            'V.QuotaAltriEnti(i) = k.QuoteGiornaliere(CS.CENTROSERVIZIO, Anag.CodiceOspite, "J", 0, DateAdd(DateInterval.Day, i, DataInizio))
        Next


        Return Math.Round(Importo / Numero, 2)
    End Function


    <WebMethod()> _
    Public Function ImportoMedioComune(ByVal Utente As String, ByVal Password As String, ByVal CodiceFiscale As String, ByVal CentroServizio As String, ByVal DataDal As String, ByVal DataAl As String) As Double
        Dim DataInizio As Date
        Dim DataFine As Date
        Dim i As Integer



        Dim DbC As New Cls_Login

        DbC.Utente = Utente
        DbC.Chiave = Password
        DbC.Leggi(Context.Application("SENIOR"))

        If DbC.Ospiti = "" Or IsNothing(DbC.Ospiti) Then
            Context.Response.StatusCode = 401
            Return "ER1"
        End If

        Dim CS As New Cls_CentroServizio

        CS.EPersonam = CentroServizio
        CS.LeggiEpersonam(DbC.Ospiti, CS.EPersonam, True)
        If CS.DESCRIZIONE.Trim = "" Then
            Context.Response.StatusCode = 404
            Return "ER7"
        End If

        Dim Anag As New ClsOspite


        Anag.LeggiPerCodiceFiscale(DbC.Ospiti, CodiceFiscale)

        If Anag.CODICEFISCALE <> CodiceFiscale Then
            Context.Response.StatusCode = 404
            Return "ER2"
        End If
        If Anag.CodiceOspite = 0 Then
            Context.Response.StatusCode = 404
            Return "ER2"
        End If


        If DataDal <> "*" Then
            If IsDate(DataDal) Then
                DataInizio = DataDal
            End If
        End If
        If DataAl <> "*" Then
            If IsDate(DataAl) Then
                DataFine = DataAl
            End If
        End If
        Dim V As New VImportiGiornalieri
        Dim k As New Cls_CalcoloRette

        k.STRINGACONNESSIONEDB = DbC.Ospiti
        k.ApriDB(DbC.Ospiti, DbC.OspitiAccessori)

        Dim Importo As Double = 0
        Dim Numero As Integer = 0

        For i = 0 To DateDiff(DateInterval.Day, DataInizio, DataFine)
            Dim Appoggio As Double = 0

            Appoggio = k.QuoteGiornaliere(CS.CENTROSERVIZIO, Anag.CodiceOspite, "C", 0, DateAdd(DateInterval.Day, i, DataInizio))

            If Appoggio > 0 Then
                Importo = Importo + Appoggio
                Numero = Numero + 1
            End If

            'V.QuotaComune(i) = 
            'V.QuotaUsl(i) = k.QuoteGiornaliere(CS.CENTROSERVIZIO, Anag.CodiceOspite, "R", 0, DateAdd(DateInterval.Day, i, DataInizio))
            'V.QuotaAltriEnti(i) = k.QuoteGiornaliere(CS.CENTROSERVIZIO, Anag.CodiceOspite, "J", 0, DateAdd(DateInterval.Day, i, DataInizio))
        Next


        Return Math.Round(Importo / Numero, 2)
    End Function

    <WebMethod()> _
    Public Function ImportoMedioOspiteParente(ByVal Utente As String, ByVal Password As String, ByVal CodiceFiscale As String, ByVal CentroServizio As String, ByVal DataDal As String, ByVal DataAl As String) As Double
        Dim DataInizio As Date
        Dim DataFine As Date
        Dim i As Integer



        Dim DbC As New Cls_Login

        DbC.Utente = Utente
        DbC.Chiave = Password
        DbC.Leggi(Context.Application("SENIOR"))

        If DbC.Ospiti = "" Or IsNothing(DbC.Ospiti) Then
            Context.Response.StatusCode = 401
            Return "ER1"
        End If

        Dim CS As New Cls_CentroServizio

        CS.EPersonam = CentroServizio
        CS.LeggiEpersonam(DbC.Ospiti, CS.EPersonam, True)
        If CS.DESCRIZIONE.Trim = "" Then
            Context.Response.StatusCode = 404
            Return "ER7"
        End If


        Dim Anag As New ClsOspite


        Anag.LeggiPerCodiceFiscale(DbC.Ospiti, CodiceFiscale)

        If Anag.CODICEFISCALE <> CodiceFiscale Then
            Context.Response.StatusCode = 404
            Return "ER2"
        End If
        If Anag.CodiceOspite = 0 Then
            Context.Response.StatusCode = 404
            Return "ER2"
        End If


        Dim KM As New Cls_Movimenti

        KM.CodiceOspite = Anag.CodiceOspite
        KM.CENTROSERVIZIO = CentroServizio
        If KM.CServizioUsato(DbC.Ospiti) = False Then
            CS.EPersonam = CentroServizio
            CS.LeggiEpersonam(DbC.Ospiti, CS.EPersonam, False)
            If CS.DESCRIZIONE.Trim = "" Then
                Context.Response.StatusCode = 404
                Return "ER7"
            End If
        End If


        If DataDal <> "*" Then
            If IsDate(DataDal) Then
                DataInizio = DataDal
            End If
        End If
        If DataAl <> "*" Then
            If IsDate(DataAl) Then
                DataFine = DataAl
            End If
        End If
        Dim V As New VImportiGiornalieri
        Dim k As New Cls_CalcoloRette




        k.STRINGACONNESSIONEDB = DbC.Ospiti
        k.ApriDB(DbC.Ospiti, DbC.OspitiAccessori)

        Dim Importo As Double = 0
        Dim Numero As Integer = 0

        For i = 0 To DateDiff(DateInterval.Day, DataInizio, DataFine)
            Dim Appoggio As Double = 0

            Appoggio = k.QuoteGiornaliere(CS.CENTROSERVIZIO, Anag.CodiceOspite, "O", 0, DateAdd(DateInterval.Day, i, DataInizio))
            Appoggio = Appoggio + k.QuoteGiornaliere(CS.CENTROSERVIZIO, Anag.CodiceOspite, "P", 1, DateAdd(DateInterval.Day, i, DataInizio)) + k.QuoteGiornaliere(CentroServizio, Anag.CodiceOspite, "P", 2, DateAdd(DateInterval.Day, i, DataInizio)) + k.QuoteGiornaliere(CentroServizio, Anag.CodiceOspite, "P", 3, DateAdd(DateInterval.Day, i, DataInizio))

            If Appoggio > 0 Then
                Importo = Importo + Appoggio
                Numero = Numero + 1
            End If

            'V.QuotaComune(i) = k.QuoteGiornaliere(CS.CENTROSERVIZIO, Anag.CodiceOspite, "C", 0, DateAdd(DateInterval.Day, i, DataInizio))
            'V.QuotaUsl(i) = k.QuoteGiornaliere(CS.CENTROSERVIZIO, Anag.CodiceOspite, "R", 0, DateAdd(DateInterval.Day, i, DataInizio))
            'V.QuotaAltriEnti(i) = k.QuoteGiornaliere(CS.CENTROSERVIZIO, Anag.CodiceOspite, "J", 0, DateAdd(DateInterval.Day, i, DataInizio))
        Next


        Return Math.Round(Importo / Numero, 2)
    End Function

    <WebMethod()> _
    Public Function CancellaOspite(ByVal Utente As String, ByVal Password As String, ByVal CodiceFiscale As String) As String

        Dim DbC As New Cls_Login

        DbC.Utente = Utente
        DbC.Chiave = Password
        DbC.Leggi(Context.Application("SENIOR"))

        If DbC.Ospiti = "" Or IsNothing(DbC.Ospiti) Then
            Context.Response.StatusCode = 401
            Return "ER1"
        End If


        Dim AnagOsp As New ClsOspite


        AnagOsp.LeggiPerCodiceFiscale(DbC.Ospiti, CodiceFiscale)

        If AnagOsp.CodiceOspite = 0 Then
            Context.Response.StatusCode = 404
            Return "ER2"
        End If

        Dim cn As OleDbConnection
        Dim Trovato As Boolean = False

        cn = New Data.OleDb.OleDbConnection(DbC.Ospiti)

        cn.Open()

        Dim cmd As New OleDbCommand()
        cmd.CommandText = ("select * from Movimenti  where CodiceOspite = " & AnagOsp.CodiceOspite)


        cmd.Connection = cn
        Dim myPOSTreader As OleDbDataReader = cmd.ExecuteReader()
        If myPOSTreader.Read Then
            Context.Response.StatusCode = 404
            Return "Presenti Movimenti"
        End If
        myPOSTreader.Close()


        Dim cmdR As New OleDbCommand()
        cmdR.CommandText = ("select * from RetteOspite  where CodiceOspite = " & AnagOsp.CodiceOspite)
        cmdR.Connection = cn
        Dim Read1 As OleDbDataReader = cmdR.ExecuteReader()
        If Read1.Read Then
            Context.Response.StatusCode = 404
            Return "Presenti Rette"
        End If
        Read1.Close()
        cn.Close()



        AnagOsp.Elimina(DbC.Ospiti, AnagOsp.CodiceOspite)

        Return "OK"
    End Function

    <WebMethod()> _
    Public Function CancellaParente(ByVal Utente As String, ByVal Password As String, ByVal CodiceFiscaleOspite As String, ByVal CodiceFiscale As String) As String

        Dim DbC As New Cls_Login

        DbC.Utente = Utente
        DbC.Chiave = Password
        DbC.Leggi(Context.Application("SENIOR"))

        If DbC.Ospiti = "" Or IsNothing(DbC.Ospiti) Then
            Context.Response.StatusCode = 401
            Return "ER1"
        End If


        Dim AnagOsp As New ClsOspite


        AnagOsp.LeggiPerCodiceFiscale(DbC.Ospiti, CodiceFiscaleOspite)

        If AnagOsp.CODICEFISCALE <> CodiceFiscaleOspite Then
            Context.Response.StatusCode = 404
            Return "ER2"
        End If
        If AnagOsp.CodiceOspite = 0 Then
            Context.Response.StatusCode = 404
            Return "ER2"
        End If

        Dim Anag As New Cls_Parenti


        Anag.CodiceOspite = AnagOsp.CodiceOspite
        Anag.LeggiPerCodiceFiscale(DbC.Ospiti, CodiceFiscale)


        Dim cn As OleDbConnection
        Dim Trovato As Boolean = False

        cn = New Data.OleDb.OleDbConnection(DbC.Ospiti)

        cn.Open()

        Dim cmd As New OleDbCommand()
        cmd.CommandText = ("select * from Movimenti  where CodiceOspite = " & Anag.CodiceOspite)


        Dim cmdR As New OleDbCommand()
        cmdR.CommandText = ("select * from RetteParente where CodiceOspite = " & AnagOsp.CodiceOspite & " And CodiceParente = " & Anag.CodiceParente)
        cmdR.Connection = cn
        Dim Read1 As OleDbDataReader = cmdR.ExecuteReader()
        If Read1.Read Then
            Context.Response.StatusCode = 404
            Return "Presenti Rette"
        End If
        Read1.Close()
        cn.Close()

        Anag.Elimina(DbC.Ospiti, Anag.CodiceOspite, Anag.CodiceParente)

        Return "OK"
    End Function

    <WebMethod()> _
    Public Function ModificaParente(ByVal Utente As String, ByVal Password As String, ByVal CodiceFiscaleOspite As String, ByVal CodiceFiscale As String, ByVal Cognome As String, ByVal Nome As String, ByVal DataNascita As String, ByVal provincianascita As String, ByVal comunenascita As String, ByVal Telefono1 As String, ByVal Telefono2 As String, ByVal CodGradoParentela As String, ByVal DesGradoParentela As String, ByVal IndirizzoRecapito As String, ByVal CapRecapito As String, ByVal ProvinciaRecapito As String, ByVal ComuneRecapito As String, ByVal NewCodiceFiscale As String) As String

        Dim DbC As New Cls_Login

        DbC.Utente = Utente
        DbC.Chiave = Password
        DbC.Leggi(Context.Application("SENIOR"))

        If DbC.Ospiti = "" Or IsNothing(DbC.Ospiti) Then
            Context.Response.StatusCode = 401
            Return "ER1"
        End If


        Dim AnagOsp As New ClsOspite


        AnagOsp.LeggiPerCodiceFiscale(DbC.Ospiti, CodiceFiscaleOspite)

        If AnagOsp.CODICEFISCALE <> CodiceFiscaleOspite Then
            Context.Response.StatusCode = 404
            Return "ER2"
        End If
        If AnagOsp.CodiceOspite = 0 Then
            Context.Response.StatusCode = 404
            Return "ER2"
        End If

        Dim Anag As New Cls_Parenti


        Anag.CodiceOspite = AnagOsp.CodiceOspite
        Anag.LeggiPerCodiceFiscale(DbC.Ospiti, CodiceFiscale)

        If Anag.CODICEFISCALE <> CodiceFiscale Then
            Context.Response.StatusCode = 404
            Return "ER3"
        End If
        If Anag.CodiceOspite = 0 Then
            Context.Response.StatusCode = 404
            Return "ER3"
        End If
        If Anag.CodiceParente = 0 Then
            Context.Response.StatusCode = 404
            Return "ER3"
        End If


        If NewCodiceFiscale <> "*" Then
            Anag.CODICEFISCALE = NewCodiceFiscale
        End If
        If Cognome <> "*" Or Nome <> "*" Then
            Anag.Nome = Cognome & " " & Nome
        End If
        If DataNascita <> "*" Then
            If IsDate(DataNascita) Then
                Anag.DataNascita = DataNascita
            End If
        End If

        If provincianascita <> "*" Then
            Anag.ProvinciaDiNascita = provincianascita
        End If
        If comunenascita <> "*" Then
            Anag.ComuneDiNascita = comunenascita
        End If
        If Telefono1 <> "*" Then
            Anag.Telefono1 = Telefono1
        End If
        If Telefono2 <> "*" Then
            Anag.Telefono2 = Telefono2
        End If
        If CodGradoParentela <> "*" Then
            Anag.GradoParentela = CodGradoParentela
        End If
        If IndirizzoRecapito <> "*" Then
            Anag.RESIDENZAINDIRIZZO1 = IndirizzoRecapito
        End If
        If CapRecapito <> "*" Then
            Anag.RESIDENZACAP1 = CapRecapito
        End If
        If ProvinciaRecapito <> "*" Then
            Anag.RESIDENZAPROVINCIA1 = ProvinciaRecapito
        End If
        If ComuneRecapito <> "*" Then
            Anag.RESIDENZACOMUNE1 = ComuneRecapito
        End If
        Anag.Utente = Utente

        Anag.ScriviParente(DbC.Ospiti)


        Return "OK"
    End Function

    <WebMethod()> _
   Public Function ModificaCodiceFiscaleOspite(ByVal Utente As String, ByVal Password As String, ByVal CodiceFiscale As String, ByVal NewCodiceFiscale As String) As String

        Dim DbC As New Cls_Login

        DbC.Utente = Utente
        DbC.Chiave = Password
        DbC.Leggi(Context.Application("SENIOR"))

        If DbC.Ospiti = "" Or IsNothing(DbC.Ospiti) Then
            Context.Response.StatusCode = 401
            Return "ER1"
        End If

        Dim Anag As New ClsOspite


        Anag.LeggiPerCodiceFiscale(DbC.Ospiti, CodiceFiscale)

        If Trim(Anag.CODICEFISCALE) <> Trim(CodiceFiscale) Then
            Context.Response.StatusCode = 404
            Return "ER2"
        End If
        If Anag.CodiceOspite = 0 Then
            Context.Response.StatusCode = 404
            Return "ER2"
        End If


        Dim VDes As New ClsOspite

        VDes.LeggiPerCodiceFiscale(DbC.Ospiti, NewCodiceFiscale)
        If VDes.CodiceOspite <> Anag.CodiceOspite And VDes.CodiceOspite > 0 Then
            Context.Response.StatusCode = 404
            Return "ER4"
        End If

        Anag.UTENTE = Utente

        Anag.CODICEFISCALE = NewCodiceFiscale

        Anag.ScriviOspite(DbC.Ospiti)

        Return "OK"
    End Function



    <WebMethod()> _
   Public Function ModificaCodiceFiscaleParente(ByVal Utente As String, ByVal Password As String, ByVal CodiceFiscaleOspite As String, ByVal CodiceFiscale As String, ByVal NewCodiceFiscale As String) As String

        Dim DbC As New Cls_Login

        DbC.Utente = Utente
        DbC.Chiave = Password
        DbC.Leggi(Context.Application("SENIOR"))

        If DbC.Ospiti = "" Or IsNothing(DbC.Ospiti) Then
            Context.Response.StatusCode = 401
            Return "ER1"
        End If


        Dim AnagOsp As New ClsOspite


        AnagOsp.LeggiPerCodiceFiscale(DbC.Ospiti, CodiceFiscaleOspite)

        If Trim(AnagOsp.CODICEFISCALE) <> Trim(CodiceFiscale) Then
            Context.Response.StatusCode = 404
            Return "ER2"
        End If


        Dim Anag As New Cls_Parenti


        Anag.CodiceOspite = AnagOsp.CodiceOspite
        Anag.LeggiPerCodiceFiscale(DbC.Ospiti, CodiceFiscale)

        If Anag.CODICEFISCALE <> CodiceFiscale Then
            Context.Response.StatusCode = 404
            Return "ER3"
        End If


        Dim VDes As New Cls_Parenti

        VDes.CodiceOspite = AnagOsp.CodiceOspite
        VDes.LeggiPerCodiceFiscale(DbC.Ospiti, NewCodiceFiscale)
        If VDes.CodiceParente > 0 Then
            Context.Response.StatusCode = 404
            Return "ER4"
        End If

        Anag.Utente = Utente

        Anag.CODICEFISCALE = NewCodiceFiscale

        Anag.ScriviParente(DbC.Ospiti)

        Return "OK"
    End Function



    <WebMethod()> _
    Public Function InserisciOspite(ByVal Utente As String, ByVal Password As String, ByVal CodiceFiscale As String, ByVal Cognome As String, ByVal Nome As String, ByVal DataNascita As String, ByVal provincianascita As String, ByVal comunenascita As String, ByVal CognomeNomePadre As String, ByVal CognomeNomeMadre As String, ByVal CognomeNomeConiuge As String, ByVal ProfessioneConiuge As String, ByVal Nazionalita As String, ByVal NumeroDocumento As String, ByVal DataDocumento As String, ByVal TipoDocumento As String, ByVal Emettitore As String, ByVal Scadenza As String, ByVal DataDecesso As String, ByVal LuogoDecesso As String, ByVal StrutturaProvenienza As String, ByVal AssistenteSociale As String, ByVal Distretto As String, ByVal CodiceMedico As String, ByVal codicesanitario As String, ByVal codiceesenzioneticket As String, ByVal datadalesenzioneticket As String, ByVal dataalesenzioneticket As String, ByVal IndirizzoRecapito As String, ByVal CapRecapito As String, ByVal ProvinciaRecapito As String, ByVal ComuneRecapito As String, ByVal DataAccoglimento As String, ByVal Descrizione As String, ByVal CentroServizio As Long, ByVal IdEPersonam As Long, ByVal Telefono1 As String, ByVal Telefono2 As String, ByVal Sesso As String, ByVal conv As Boolean) As String



        Dim DbC As New Cls_Login

        DbC.Utente = Utente
        DbC.Chiave = Password
        DbC.Leggi(Context.Application("SENIOR"))

        If DbC.Ospiti = "" Or IsNothing(DbC.Ospiti) Then
            Context.Response.StatusCode = 401
            Return "ER1"
        End If

        Dim Anag As New ClsOspite

        Anag.CodiceOspite = 0
        Anag.LeggiPerCodiceFiscale(DbC.Ospiti, CodiceFiscale)

        If Anag.CodiceOspite > 0 Then
            Context.Response.StatusCode = 404
            Return "ER4"
        End If

        If DataNascita = "" Then
            Context.Response.StatusCode = 404
            Return "ER10"
        End If
        If DataAccoglimento = "" Then
            Context.Response.StatusCode = 404
            Return "ER11"
        End If

        If Not IsDate(DataNascita) Then
            Context.Response.StatusCode = 404
            Return "ER10"
        End If
        If Not IsDate(DataAccoglimento) Then
            Context.Response.StatusCode = 404
            Return "ER11"
        End If


        Dim CS As New Cls_CentroServizio
        CS.DESCRIZIONE = ""
        CS.EPersonam = CentroServizio
        CS.LeggiEpersonam(DbC.Ospiti, CS.EPersonam, conv)
        If CS.DESCRIZIONE.Trim = "" Then
            Context.Response.StatusCode = 404
            Return "ER7"
        End If


        Anag.CognomeOspite = Cognome
        Anag.NomeOspite = Nome
        Anag.Nome = Anag.CognomeOspite & " " & Anag.NomeOspite
        Anag.DataNascita = DataNascita
        Anag.UTENTE = Utente
        Anag.InserisciOspite(DbC.Ospiti, Anag.CognomeOspite, Anag.NomeOspite, Anag.DataNascita)
        Anag.Leggi(DbC.Ospiti, Anag.CodiceOspite)

        Anag.CODICEFISCALE = CodiceFiscale


        If Sesso <> "*" Then
            If Sesso = "1" Then
                Anag.Sesso = "M"
            End If
            If Sesso = "2" Then
                Anag.Sesso = "F"
            End If
        End If
        If provincianascita <> "*" Then
            Anag.ProvinciaDiNascita = provincianascita
        End If
        If comunenascita <> "*" Then
            Anag.ComuneDiNascita = comunenascita
        End If

        If CognomeNomePadre <> "*" Then
            Anag.NOMEPADRE = CognomeNomePadre
        End If
        If CognomeNomeMadre <> "*" Then
            Anag.NOMEMADRE = CognomeNomeMadre
        End If
        If CognomeNomeConiuge <> "*" Then
            Anag.NOMECONIUGE = CognomeNomeConiuge
        End If
        If CognomeNomeConiuge <> "*" Then
            Anag.NOMECONIUGE = CognomeNomeConiuge
        End If

        If ProfessioneConiuge <> "*" Then
            Anag.PROFCONIUGE = ProfessioneConiuge
        End If
        If Nazionalita <> "*" Then
            Anag.Nazionalita = Nazionalita
        End If
        If NumeroDocumento <> "*" Then
            Anag.NumeroDocumento = NumeroDocumento
        End If
        If DataDocumento <> "*" Then
            If IsDate(DataDocumento) Then
                Anag.DataDocumento = DataDocumento
            End If
        End If
        If TipoDocumento <> "*" Then
            Anag.TipoDocumento = TipoDocumento
        End If
        If Emettitore <> "*" Then
            Anag.EmettitoreDocumento = Emettitore
        End If
        If Scadenza <> "*" Then
            If IsDate(Scadenza) Then
                Anag.ScadenzaDocumento = Scadenza
            End If
        End If
        If Telefono1 <> "*" Then
            Anag.RESIDENZATELEFONO1 = Telefono1
        End If
        If Telefono2 <> "*" Then
            Anag.RESIDENZATELEFONO2 = Telefono2
        End If

        If DataDecesso <> "*" Then
            If IsDate(DataDecesso) Then
                Anag.DATAMORTE = DataDecesso
            End If
        End If
        If LuogoDecesso <> "*" Then
            Anag.LuogoMorte = LuogoDecesso
        End If
        If StrutturaProvenienza <> "*" Then
            Anag.StrutturaProvenienza = StrutturaProvenienza
        End If
        If AssistenteSociale <> "*" Then
            Anag.AssistenteSociale = AssistenteSociale
        End If
        If Distretto <> "*" Then
            Anag.Distretto = Distretto
        End If
        If CodiceMedico <> "*" Then
            Anag.CodiceMedico = CodiceMedico
        End If
        If codicesanitario <> "*" Then
            Anag.CodiceSanitario = codicesanitario
        End If
        If codiceesenzioneticket <> "*" Then
            Anag.CodiceTicket = codiceesenzioneticket
        End If
        If datadalesenzioneticket <> "*" Then
            If IsDate(datadalesenzioneticket) Then
                Anag.DataRilascio = datadalesenzioneticket
            End If
        End If
        If dataalesenzioneticket <> "*" Then
            If IsDate(dataalesenzioneticket) Then
                Anag.DataScadenza = dataalesenzioneticket
            End If
        End If
        If IndirizzoRecapito <> "*" Then
            Anag.RESIDENZAINDIRIZZO1 = IndirizzoRecapito
        End If
        If CapRecapito <> "*" Then
            Anag.RESIDENZACAP1 = CapRecapito
        End If
        If ProvinciaRecapito <> "*" Then
            Anag.RESIDENZAPROVINCIA1 = ProvinciaRecapito
        End If
        If ComuneRecapito <> "*" Then
            Anag.RESIDENZACOMUNE1 = ComuneRecapito
        End If


        Anag.ScriviOspite(DbC.Ospiti)





        Dim Pc As New Cls_Pianodeiconti
        Pc.Mastro = CS.MASTRO
        Pc.Conto = CS.CONTO
        Pc.Sottoconto = Anag.CodiceOspite * 100
        Pc.Decodfica(DbC.Generale)
        Pc.Mastro = CS.MASTRO
        Pc.Conto = CS.CONTO
        Pc.Sottoconto = Anag.CodiceOspite * 100
        Pc.Descrizione = Anag.Nome
        Pc.Tipo = "A"
        Pc.TipoAnagrafica = "O"
        Pc.Scrivi(DbC.Generale)



        Dim cn As OleDbConnection
        Dim Trovato As Boolean = False

        cn = New Data.OleDb.OleDbConnection(DbC.Ospiti)

        cn.Open()



        Dim TipoMovimentoSenior As String = ""
        Dim Progressivo As Long
        Dim MySql As String


        TipoMovimentoSenior = "05"

        Dim xtr As OleDbTransaction = cn.BeginTransaction()



        Dim cmdIns As New OleDbCommand()
        cmdIns.CommandText = ("INSERT INTO Movimenti_EPersonam  (IdEpersonam,CodiceFiscale,CentroServizio,CodiceOspite,Data,TipoMovimento,Descrizione,DataModifica) VALUES (?,?,?,?,?,?,?,?)")
        cmdIns.Connection = cn
        cmdIns.Transaction = xtr
        cmdIns.Parameters.AddWithValue("@IdEpersonam", IdEPersonam)
        cmdIns.Parameters.AddWithValue("@CodiceFiscale", CodiceFiscale)
        cmdIns.Parameters.AddWithValue("@CentroServizio", CS.CENTROSERVIZIO)
        cmdIns.Parameters.AddWithValue("@CodiceOspite", Anag.CodiceOspite)
        cmdIns.Parameters.AddWithValue("@Data", DataAccoglimento)
        cmdIns.Parameters.AddWithValue("@TipoMovimento", "A")
        cmdIns.Parameters.AddWithValue("@Descrizione", Descrizione)
        cmdIns.Parameters.AddWithValue("@DataModifica", Now)
        cmdIns.ExecuteNonQuery()

        Dim cmd1 As New OleDbCommand()
        cmd1.CommandText = ("select MAX(Progressivo) from Movimenti where CentroServizio = '" & CS.CENTROSERVIZIO & "' And  CodiceOspite = " & Anag.CodiceOspite)
        cmd1.Connection = cn
        cmd1.Transaction = xtr
        Dim myPOSTreader1 As OleDbDataReader = cmd1.ExecuteReader()
        If myPOSTreader1.Read Then
            If Not IsDBNull(myPOSTreader1.Item(0)) Then
                Progressivo = myPOSTreader1.Item(0) + 1
            Else
                Progressivo = 1
            End If
        Else
            Progressivo = 1
        End If


        MySql = "INSERT INTO Movimenti (Utente,DataAggiornamento,CentroServizio,CodiceOspite,Data,TIPOMOV,PROGRESSIVO,CAUSALE,DESCRIZIONE,EPersonam) VALUES (?,?,?,?,?,?,?,?,?,?)"
        Dim cmdw As New OleDbCommand()
        cmdw.CommandText = (MySql)
        cmdw.Transaction = xtr
        cmdw.Parameters.AddWithValue("@Utente", Utente)
        cmdw.Parameters.AddWithValue("@DataAggiornamento", Now)
        cmdw.Parameters.AddWithValue("@CentroServizio", CS.CENTROSERVIZIO)
        cmdw.Parameters.AddWithValue("@CodiceOspite", Anag.CodiceOspite)
        cmdw.Parameters.AddWithValue("@Data", DataAccoglimento)
        cmdw.Parameters.AddWithValue("@TIPOMOV", TipoMovimentoSenior)
        cmdw.Parameters.AddWithValue("@PROGRESSIVO", Progressivo)
        cmdw.Parameters.AddWithValue("@CAUSALE", "")
        cmdw.Parameters.AddWithValue("@Descrizione", Descrizione)
        cmdw.Parameters.AddWithValue("@EPersonam", 1)
        cmdw.Connection = cn
        cmdw.ExecuteNonQuery()

        xtr.Commit()

        cn.Close()
        Return "OK"
    End Function


    <WebMethod()> _
     Public Function InserisciParente(ByVal Utente As String, ByVal Password As String, ByVal CodiceFiscaleOspite As String, ByVal CodiceFiscale As String, ByVal Cognome As String, ByVal Nome As String, ByVal DataNascita As String, ByVal provincianascita As String, ByVal comunenascita As String, ByVal Telefono1 As String, ByVal Telefono2 As String, ByVal CodGradoParentela As String, ByVal DesGradoParentela As String, ByVal IndirizzoRecapito As String, ByVal CapRecapito As String, ByVal ProvinciaRecapito As String, ByVal ComuneRecapito As String) As String

        Dim DbC As New Cls_Login

        DbC.Utente = Utente
        DbC.Chiave = Password
        DbC.Leggi(Context.Application("SENIOR"))

        If DbC.Ospiti = "" Or IsNothing(DbC.Ospiti) Then
            Context.Response.StatusCode = 401
            Return "ER1"
        End If




        Dim AnagOsp As New ClsOspite


        AnagOsp.LeggiPerCodiceFiscale(DbC.Ospiti, CodiceFiscaleOspite)

        If AnagOsp.CODICEFISCALE = CodiceFiscale Then
            Context.Response.StatusCode = 404
            Return "ER2"
        End If
        If AnagOsp.CodiceOspite = 0 Then
            Context.Response.StatusCode = 404
            Return "ER2"
        End If


        Dim VDes As New Cls_Parenti

        VDes.CodiceOspite = AnagOsp.CodiceOspite
        VDes.LeggiPerCodiceFiscale(DbC.Ospiti, CodiceFiscale)
        If VDes.CodiceParente > 0 Then
            Context.Response.StatusCode = 404
            Return "ER4"
        End If


        Dim Anag As New Cls_Parenti


        Anag.CodiceOspite = VDes.CodiceOspite
        Anag.CodiceParente = 0
        Anag.Nome = Cognome & " " & Nome

        If DataNascita <> "*" Then
            Anag.DataNascita = DataNascita
        End If
        Anag.CODICEFISCALE = CodiceFiscale

        If provincianascita <> "*" Then
            Anag.ProvinciaDiNascita = provincianascita
        End If
        If comunenascita <> "*" Then
            Anag.ComuneDiNascita = comunenascita
        End If
        If Telefono1 <> "*" Then
            Anag.Telefono1 = Telefono1
        End If
        If Telefono2 <> "*" Then
            Anag.Telefono2 = Telefono2
        End If

        If CodGradoParentela <> "*" Then
            Anag.GradoParentela = CodGradoParentela
        End If
        If IndirizzoRecapito <> "*" Then
            Anag.RESIDENZAINDIRIZZO1 = IndirizzoRecapito
        End If
        If CapRecapito <> "*" Then
            Anag.RESIDENZACAP1 = CapRecapito
        End If
        If ProvinciaRecapito <> "*" Then
            Anag.RESIDENZAPROVINCIA1 = ProvinciaRecapito
        End If
        If ComuneRecapito <> "*" Then
            Anag.RESIDENZACOMUNE1 = ComuneRecapito
        End If

        Anag.ScriviParente(DbC.Ospiti)


        Dim cn As OleDbConnection
        Dim Trovato As Boolean = False

        cn = New Data.OleDb.OleDbConnection(DbC.Ospiti)

        cn.Open()

        Dim cmd As New OleDbCommand()
        cmd.CommandText = ("select CentroServizio from Movimenti  where CodiceOspite = " & Anag.CodiceOspite & " Group by CentroServizio")


        cmd.Connection = cn
        Dim myPOSTreader As OleDbDataReader = cmd.ExecuteReader()
        Do While myPOSTreader.Read

            Dim CS As New Cls_CentroServizio

            CS.CENTROSERVIZIO = campodb(myPOSTreader.Item("CentroServizio"))
            CS.Leggi(DbC.Ospiti, CS.CENTROSERVIZIO)

            Dim Pc As New Cls_Pianodeiconti
            Pc.Mastro = CS.MASTRO
            Pc.Conto = CS.CONTO
            Pc.Sottoconto = (Anag.CodiceOspite * 100) + Anag.CodiceParente
            Pc.Decodfica(DbC.Generale)
            Pc.Mastro = CS.MASTRO
            Pc.Conto = CS.CONTO
            Pc.Sottoconto = (Anag.CodiceOspite * 100) + Anag.CodiceParente
            Pc.Descrizione = Anag.Nome
            Pc.Tipo = "A"
            Pc.TipoAnagrafica = "P"
            Pc.Scrivi(DbC.Generale)

        Loop
        myPOSTreader.Close()



        cn.Close()

        Return "OK"
    End Function



    <WebMethod()> _
    Public Function ImportaMovimenti(ByVal Utente As String, ByVal Password As String, ByVal IdEPersonam As String, ByVal CodiceFiscale As String, ByVal CentroServizio As Long, ByVal Data As String, ByVal TipoMovimento As String, ByVal Descrizione As String) As String
        Dim DbC As New Cls_Login

        DbC.Utente = Utente
        DbC.Chiave = Password
        DbC.Leggi(Context.Application("SENIOR"))

        If DbC.Ospiti = "" Or IsNothing(DbC.Ospiti) Then
            Context.Response.StatusCode = 401
            Return "ER1"
        End If
        Dim Anag As New ClsOspite


        Anag.LeggiPerCodiceFiscale(DbC.Ospiti, CodiceFiscale)

        If Anag.CODICEFISCALE = "" Then
            Context.Response.StatusCode = 404
            Return "ER2"
        End If


        If Data = "" Then
            Context.Response.StatusCode = 404
            Return "ER5"
        End If

        If Not IsDate(Data) Then
            Context.Response.StatusCode = 404
            Return "ER5"
        End If

        Dim CS As New Cls_CentroServizio

        CS.EPersonam = CentroServizio
        CS.LeggiEpersonam(DbC.Ospiti, CS.EPersonam, True)
        If CS.DESCRIZIONE.Trim = "" Then
            Context.Response.StatusCode = 404
            Return "ER7"
        End If



        Dim cn As OleDbConnection
        Dim Trovato As Boolean = False

        cn = New Data.OleDb.OleDbConnection(DbC.Ospiti)

        cn.Open()



        Dim TipoMovimentoSenior As String = ""

        If TipoMovimento = "A" Then
            TipoMovimentoSenior = "05"
        End If
        If TipoMovimento = "U" Then
            TipoMovimentoSenior = "03"
        End If
        If TipoMovimento = "E" Then
            TipoMovimentoSenior = "04"
        End If
        If TipoMovimento = "D" Then
            TipoMovimentoSenior = "13"
        End If
        If TipoMovimentoSenior = "" Then
            Context.Response.StatusCode = 404
            Return "ER6"
        End If

        Dim Progressivo As Integer = 0
        Dim MySql As String

        Dim xtr As OleDbTransaction = cn.BeginTransaction()

        Dim cmd As New OleDbCommand()
        cmd.CommandText = ("select * from Movimenti  where EPersonam = " & IdEPersonam)

        cmd.Transaction = xtr

        cmd.Connection = cn
        Dim myPOSTreader As OleDbDataReader = cmd.ExecuteReader()
        If myPOSTreader.Read Then
            Progressivo = campodbN(myPOSTreader.Item("Progressivo"))
        End If
        myPOSTreader.Close()


        Dim cmdLog As New OleDbCommand()
        cmdLog.CommandText = ("select * from Movimenti_EPersonam  where IdEpersonam = " & IdEPersonam)

        cmdLog.Transaction = xtr
        cmdLog.Connection = cn
        Dim RDLog As OleDbDataReader = cmdLog.ExecuteReader()
        If RDLog.Read Then
            Trovato = True
        End If
        RDLog.Close()


        If Progressivo = 0 And Trovato Then
            Context.Response.StatusCode = 401
            Return "WR1"
        End If

        Dim cmdIns As New OleDbCommand()
        cmdIns.CommandText = ("INSERT INTO Movimenti_EPersonam  (IdEpersonam,CodiceFiscale,CentroServizio,CodiceOspite,Data,TipoMovimento,Descrizione,DataModifica) VALUES (?,?,?,?,?,?,?,?)")

        cmdIns.Connection = cn
        cmdIns.Transaction = xtr
        cmdIns.Parameters.AddWithValue("@IdEpersonam", IdEPersonam)
        cmdIns.Parameters.AddWithValue("@CodiceFiscale", CodiceFiscale)
        cmdIns.Parameters.AddWithValue("@CentroServizio", CS.CENTROSERVIZIO)
        cmdIns.Parameters.AddWithValue("@CodiceOspite", Anag.CodiceOspite)
        cmdIns.Parameters.AddWithValue("@Data", Data)
        cmdIns.Parameters.AddWithValue("@TipoMovimento", TipoMovimento)
        cmdIns.Parameters.AddWithValue("@Descrizione", Descrizione)
        cmdIns.Parameters.AddWithValue("@DataModifica", Now)
        cmdIns.ExecuteNonQuery()

        If Progressivo = 0 Then
            Dim cmd1 As New OleDbCommand()
            cmd1.CommandText = ("select MAX(Progressivo) from Movimenti where CentroServizio = '" & CS.CENTROSERVIZIO & "' And  CodiceOspite = " & Anag.CodiceOspite)
            cmd1.Connection = cn
            Dim myPOSTreader1 As OleDbDataReader = cmd1.ExecuteReader()
            If myPOSTreader1.Read Then
                If Not IsDBNull(myPOSTreader1.Item(0)) Then
                    Progressivo = myPOSTreader1.Item(0) + 1
                Else
                    Progressivo = 1
                End If
            Else
                Progressivo = 1
            End If


            MySql = "INSERT INTO Movimenti (Utente,DataAggiornamento,CentroServizio,CodiceOspite,Data,TIPOMOV,PROGRESSIVO,CAUSALE,DESCRIZIONE,EPersonam) VALUES (?,?,?,?,?,?,?,?,?,?)"
            Dim cmdw As New OleDbCommand()
            cmdw.CommandText = (MySql)
            cmdw.Parameters.AddWithValue("@Utente", Utente)
            cmdw.Parameters.AddWithValue("@DataAggiornamento", Now)
            cmdw.Parameters.AddWithValue("@CentroServizio", CS.CENTROSERVIZIO)
            cmdw.Parameters.AddWithValue("@CodiceOspite", Anag.CodiceOspite)
            cmdw.Parameters.AddWithValue("@Data", Data)
            cmdw.Parameters.AddWithValue("@TIPOMOV", TipoMovimentoSenior)
            cmdw.Parameters.AddWithValue("@PROGRESSIVO", Progressivo)
            cmdw.Parameters.AddWithValue("@CAUSALE", "")
            cmdw.Parameters.AddWithValue("@Descrizione", Descrizione)
            cmdw.Parameters.AddWithValue("@EPersonam", 1)
            cmdw.Transaction = xtr
            cmdw.Connection = cn
            cmdw.ExecuteNonQuery()
        Else
            MySql = "UPDATE Movimenti SET Utente = ?,DataAggiornamento = ?,TIPOMOV = ?,DATA=?,CAUSALE=?,DESCRIZIONE=?,EPersonam =? WHERE  CentroServizio = '" & CS.CENTROSERVIZIO & "' And  CodiceOspite = " & Anag.CodiceOspite & " And PROGRESSIVO=?"
            Dim cmdw As New OleDbCommand()
            cmdw.CommandText = (MySql)
            cmdw.Parameters.AddWithValue("@Utente", Utente)
            cmdw.Parameters.AddWithValue("@DataAggiornamento", Now)
            cmdw.Parameters.AddWithValue("@TIPOMOV", TipoMovimentoSenior)
            cmdw.Parameters.AddWithValue("@Data", Data)
            cmdw.Parameters.AddWithValue("@CAUSALE", "")
            cmdw.Parameters.AddWithValue("@Descrizione", Descrizione)
            cmdw.Parameters.AddWithValue("@EPersonam", 1)
            cmdw.Parameters.AddWithValue("@PROGRESSIVO", Progressivo)
            cmdw.Transaction = xtr
            cmdw.Connection = cn
            cmdw.ExecuteNonQuery()
        End If

        xtr.Commit()

        If TipoMovimentoSenior = "05" Then
            Dim Pc As New Cls_Pianodeiconti
            Pc.Mastro = CS.MASTRO
            Pc.Conto = CS.CONTO
            Pc.Sottoconto = Anag.CodiceOspite * 100
            Pc.Decodfica(DbC.Generale)
            Pc.Mastro = CS.MASTRO
            Pc.Conto = CS.CONTO
            Pc.Sottoconto = Anag.CodiceOspite * 100
            Pc.Descrizione = Anag.Nome
            Pc.Tipo = "A"
            Pc.TipoAnagrafica = "O"
            Pc.Scrivi(DbC.Generale)

            Dim cmdP As New OleDbCommand()
            cmdP.CommandText = ("select * from AnagraficaComune where CodiceOspite = " & Anag.CodiceOspite & " And CodiceParente > 0")


            cmdP.Connection = cn
            Dim ReadP As OleDbDataReader = cmdP.ExecuteReader()
            Do While ReadP.Read

                Dim PcP As New Cls_Pianodeiconti
                PcP.Mastro = CS.MASTRO
                PcP.Conto = CS.CONTO
                PcP.Sottoconto = (Anag.CodiceOspite * 100) + campodbN(ReadP.Item("CodiceParente"))
                PcP.Decodfica(DbC.Generale)
                PcP.Mastro = CS.MASTRO
                PcP.Conto = CS.CONTO
                PcP.Sottoconto = (Anag.CodiceOspite * 100) + campodbN(ReadP.Item("CodiceParente"))
                PcP.Descrizione = Anag.Nome
                PcP.Tipo = "A"
                PcP.TipoAnagrafica = "P"
                PcP.Scrivi(DbC.Generale)
            Loop
            myPOSTreader.Close()
        End If
        cn.Close()
        Return "OK"
    End Function

    Function campodbN(ByVal oggetto As Object) As Double
        If IsDBNull(oggetto) Then
            Return 0
        Else
            Return oggetto
        End If
    End Function

    Function campodb(ByVal oggetto As Object) As String
        If IsDBNull(oggetto) Then
            Return ""
        Else
            Return oggetto
        End If
    End Function

    Function campodbd(ByVal oggetto As Object) As Date
        If IsDBNull(oggetto) Then
            Return Nothing
        Else
            Return oggetto
        End If
    End Function

    ' tabella Tipo Documento
    'IDENT;CARTA DI IDENTITA'  
    'CERID;CERTIFICATO D 'IDENTITA' 
    'ACMIL;TESS. APP.TO AG.CUSTODIA    
    'ACSOT;TESS. SOTT.LI AG.CUSTODIA   
    'ACUFF;TESS. UFF.LI AG.CUSTODIA    
    'AMMIL;TESS. MILITARE TRUPPA A.M   
    'AMSOT;TESS.SOTTUFFICIALI a.M.
    'AMUFF;TESS.UFFICIALI a.M.
    'CCMIL;TESS. APP.TO CARABINIERI   
    'CCSOT;TESS. SOTTUFFICIALI CC  
    'CCUFF;TESS.UFFICIALE 
    'CFMIL;TESS.AG.E AG.SC.C.f.s.
    'CFSOT;TESS.SOTTUFICIALI C.f.s.
    'CFUFF;TESS.UFFICIALI C.f.s.
    'CIDIP;CARTA ID. DIPLOMATICA   
    'DESIS;TESS.s.I.s.D.E.
    'EIMIL;TESS.MILITARE E.I.
    'EISOT;TESS.SOTTUFFICIALI E.I.
    'EIUFF;TESS.UFFICIALI E.I.
    'GFMIL;TESS. APP.TO FINANZIERE 
    'GFSOT;TESS.SOTT.LI g.D.f.
    'GFTRI;TESS.POL.TRIB.g.D.f.
    'GFUFF;TESS.UFFICIALI g.D.f.
    'IDELE;CARTA IDENTITA ' ELETTRONICA 
    'MAGIS;TESS.PERS.MAGISTRATI 
    'MMMIL;TESS.MILIT.M.M.
    'MMSOT;TESS.SOTTUFICIALI M.M.
    'MMUFF;TESS.UFFICIALI M.M.
    'PARLA;TESS.PARLAMENTARI 
    'PASDI;PASSAPORTO DIPLOMATICO  
    'PASOR;PASSAPORTO ORDINARIO    
    'PASSE;PASSAPORTO DI SERVIZIO  
    'PATEN;PATENTE DI GUIDA    
    'PATNA;PATENTE NAUTICA 
    'PPAGE;TESS. AGENTI/ASS.TI P.P.    
    'PPISP;TESS.ISPETTORI P.P.
    'PPSOV;TESS.SOVRINTENDENTI P.P.
    'PPUFF;TESS.UFFICIALI P.P.
    'PSAPP;TESS. AGENTI/ASS.TI P.S.    
    'PSFEM;TESS. POLIZIA FEMMINILE 
    'PSFUN;TESS.FUNZIONARI P.s.
    'PSISP;TESS.ISPETTORI P.s.
    'PSSOT;TESS.SOVRINTENDENTI P.s.
    'PSUFF;TESS.UFFICIALI P.s.
    'SDMIL;TESS. MILIT. TRUPPA SISMI   
    'SDSOT;TESS. SOTTUFFICIALI SISMI   
    'SDUFF;TESS. UFFICIALI SISMI   
    'TEAMC;TESS.ISCR.ALBO MED / CHI.
    'TEAOD;TESS.ISCRIZ.ALBO ODONT.
    'TECAM;TES. UNICO PER LA CAMERA    
    'TECOC;TESS. CORTE DEI CONTI   
    'TEDOG;TES.DOGANALE RIL.Min.FIN.
    'TEFSE;TESS.FERROV.SENATO 
    'TEMPI;TESS.Min.PUBB.ISTRUZIONE 
    'TENAT;TESS. MILITARE NATO 
    'TENAV;TES. ENTE NAZ. ASSIS.VOLO   
    'TEPOL;TESS.Min.POLIT.AGRIC.FOR.
    'TESAE;TESS. MIN. AFFARI ESTERI    
    'TESAR;TESS.ISCR.ALBO ARCHITETTI   
    'TESAV;TESSERA ISCR. ALBO AVVOC.   
    'TESCA;TESS.CORTE D 'APPELLO   
    'TESCS;TESS. CONSIGLIO DI STATO    
    'TESDI;TESSERA RICONOSC.D.I.a.
    'TESEA;TESS. MEMBRO EQUIP. AEREO   
    'TESIN;TESS.ISCR. ALBO INGEGNERI   
    'TESLP;TESS. MINISTERO LAVORI PU   
    'TESMB;TESS.Min.BEN.E ATT.CULT.
    'TESMD;TESS. MINISTERO DIFESA  
    'TESMF;TESS. MINISTERO FINANZE 
    'TESMG;TESS. MINISTERO GIUSTIZIA   
    'TESMI;TESS. MINISTERO INTERNO 
    'TESMN;TESS. MINIST. TRASP/NAVIG   
    'TESMS;TESS.MINISTERO SANITA ' 
    'TESMT;TESS. MINISTERO TESORO  
    'TESNO;TESSERA DELL 'ORDINE NOTAI   
    'TESOG;TESS. ORDINE GIORNALISTI    
    'TESPC;TESS.PRES.ZA CONS.Min.
    'TESPI;TESS. PUBBLICA ISTRUZIONE   
    'TESPT;TES. POSTE E TELECOMUNIC.   
    'TESUN;TESSERA U.N.U.C.I.
    'TETEL;TESS.IDENTIF.TELECOM IT.
    'TFERD;TES. FERROVIARIA DEPUTATI   
    'TFEXD;TES. FERROV. EX DEPUTATI    
    'VIMIL;TESS. APP.TO/VIG. URBANO    
    'VISOT;TESS. SOTT.LI VIG. URBANI   
    'VIUFF;TESS. UFF.LI VIG.URBANI 
    'VVMIL;TESS. APP.TO/VIG. VV.FF.    
    'VVSOT;TESS.SOTTUFF.LI VV.FF.
    'VVUFF;TESS.UFFICIALI VV.FF.

    Private Class VImportiGiornalieri
        Public QuotaOspite(366) As Double
        Public QuotaParenti(366) As Double
        Public QuotaComune(366) As Double
        Public QuotaUsl(366) As Double
        Public QuotaAltriEnti(366) As Double
    End Class


    Private Class QuoteMedie
        Public QuotaOspite As Double
        Public QuotaParenti As Double
        Public QuotaComune As Double
        Public QuotaUsl As Double
        Public QuotaAltriEnti As Double
    End Class

    Private Class QuoteMedieOspitiCF
        Public Singolo(100) As QuoteMedieOspiti
    End Class
    Private Class QuoteMedieOspiti
        Public CF As String
        Public QuotaOspite As Double
        Public QuotaParenti As Double
        Public QuotaComune As Double
        Public QuotaUsl As Double
        Public QuotaAltriEnti As Double
    End Class



    Private Class QuoteOspitiPeriodoCFs
        Public Singolo As New List(Of QuoteOspitiPeriodo)
    End Class
    Private Class QuoteOspitiPeriodo
        Public CF As String
        Public Data As String
        Public Comune As String
        Public Usl As String
        Public AltroEnte As String
        Public QuotaOspite As Double
        Public QuotaParenti As Double
        Public QuotaComune As Double
        Public QuotaUsl As Double
        Public QuotaAltriEnti As Double
        Public AssegnoAccompagnamento As String
        Public QuotaAltroEnteSanitario As Double
    End Class

    <WebMethod()> _
    Public Function ImportiStati(ByVal Utente As String, ByVal Password As String, ByVal CodiceFiscale As String, ByVal CentroServizio As String, ByVal DataDal As String, ByVal DataAl As String, ByVal Sosia As String) As String


        If Utente = "" And Password = "" Then


            Me.Context.Response.StatusCode = 200
            Return ""
        End If

        Dim DbC As New Cls_Login


        DbC.Utente = Utente
        DbC.Chiave = Password
        DbC.LeggiSP(Context.Application("SENIOR"))

        If DbC.Ospiti = "" Or IsNothing(DbC.Ospiti) Then
            Context.Response.StatusCode = 401
            Return "ER1"
        End If


        Dim Anag As New ClsOspite


        Anag.LeggiPerCodiceFiscale(DbC.Ospiti, CodiceFiscale)

        If Anag.CODICEFISCALE <> CodiceFiscale Then
            Context.Response.StatusCode = 404
            Return "ER2"
        End If
        If Anag.CodiceOspite = 0 Then
            Context.Response.StatusCode = 404
            Return "ER2"
        End If

        Dim XS As New QuoteOspitiPeriodoCFs

        XS = ImportiStatiOspite(Utente, Password, CodiceFiscale, CentroServizio, DataDal, DataAl, Sosia)

        Dim serializer As JavaScriptSerializer
        serializer = New JavaScriptSerializer()

        Dim Serializzazione As String

        Serializzazione = serializer.Serialize(XS)
        Me.Context.Response.StatusCode = 200

        Return Serializzazione
    End Function
    Private Function ImportiStatiOspite(ByVal Utente As String, ByVal Password As String, ByVal CodiceFiscale As String, ByVal CentroServizio As String, ByVal DataDal As String, ByVal DataAl As String, Optional ByVal Sosia As String = "") As QuoteOspitiPeriodoCFs
        Dim DataInizio As Date
        Dim DataFine As Date
        Dim i As Integer
        Dim XS As New QuoteOspitiPeriodoCFs


        Dim DbC As New Cls_Login

        DbC.Utente = Utente
        DbC.Chiave = Password
        DbC.Leggi(Context.Application("SENIOR"))


        Dim Anag As New ClsOspite

        Try
            Anag.LeggiPerCodiceFiscale(DbC.Ospiti, CodiceFiscale)

        Catch ex As Exception
            ImportiStatiOspite = Nothing
            Exit Function
        End Try




        If Anag.CODICEFISCALE <> CodiceFiscale Then
            Return XS
        End If
        If Anag.CodiceOspite = 0 Then

            Return XS
        End If

        Dim CS As New Cls_CentroServizio
        Dim KS As New Cls_Epersonam

        'CS.EPersonam = KS.RendiWardid(CentroServizio)
        'CS.LeggiEpersonam(DbC.Ospiti, CS.EPersonam, True)
        'If CS.DESCRIZIONE.Trim = "" Then
        '    CS.EPersonam = KS.RendiWardid(CentroServizio)
        '    CS.LeggiEpersonam(DbC.Ospiti, CS.EPersonam, False)
        '    If CS.DESCRIZIONE.Trim = "" Then
        '        Dim kCS As New Cls_Movimenti

        '        kCS.CodiceOspite = Anag.CodiceOspite
        '        kCS.UltimaDataAccoglimento(DbC.Ospiti)
        '        If kCS.CENTROSERVIZIO.Trim = "" Then
        '            Context.Response.StatusCode = 404
        '            Return "ER7"
        '        End If
        '    End If
        'End If



        If DataDal <> "*" Then
            If IsDate(DataDal) Then
                DataInizio = DataDal
            End If
        End If
        If DataAl <> "*" Then
            If IsDate(DataAl) Then
                DataFine = DataAl
            End If
        End If
        Dim V As New VImportiGiornalieri
        Dim k As New Cls_CalcoloRette

        k.STRINGACONNESSIONEDB = DbC.Ospiti
        k.ApriDB(DbC.Ospiti, DbC.OspitiAccessori)


        If Sosia = "S" Then
            k.QuoteDaSoia = 1024
        End If


        Dim cn As OleDbConnection
        Dim Trovato As Boolean = False
        Dim UltimeQuoteO As Double = 0
        Dim UltimeQuoteP As Double = 0
        Dim UltimeQuoteC As Double = 0
        Dim UltimeQuoteR As Double = 0
        Dim UltimeQuoteJ As Double = 0
        Dim UltimeQuoteJSanitario As Double = 0
        Dim UltimeAccompagnamento As String = ""
        Dim UltimaData As Date
        Dim Entrato As Boolean = False

        cn = New Data.OleDb.OleDbConnection(DbC.Ospiti)

        cn.Open()

        Dim cmdRs As New OleDbCommand()

        Dim Centro1 As String = ""
        Dim Centro2 As String = ""

        cmdRs.CommandText = ("select * from TABELLACENTROSERVIZIO Where  EPersonam = " & KS.RendiWardid(CentroServizio, DbC.Ospiti) & " or EPersonamN = " & KS.RendiWardid(CentroServizio, DbC.Ospiti))

        cmdRs.Connection = cn

        i = 0
        Dim RsRead As OleDbDataReader = cmdRs.ExecuteReader()
        If RsRead.Read Then
            Centro1 = campodb(RsRead.Item("CENTROSERVIZIO"))
            Centro2 = Centro1
            If RsRead.Read Then
                Centro2 = campodb(RsRead.Item("CENTROSERVIZIO"))
            End If
        End If
        RsRead.Close()

        If Centro1 = "" And Centro2 = "" Then
            Dim cmd2 As New OleDbCommand()

            cmd2.CommandText = "SELECT CENTROSERVIZIO From  MOVIMENTI WHERE CODICEOSPITE = ? GROUP BY CENTROSERVIZIO"
            cmd2.Connection = cn
            cmd2.Parameters.AddWithValue("@CodiceOSpite", Anag.CodiceOspite)
            Dim Reader2 As OleDbDataReader = cmd2.ExecuteReader()
            If Reader2.Read Then
                Centro1 = campodb(Reader2.Item("CENTROSERVIZIO"))
                If Reader2.Read Then
                    Centro2 = campodb(Reader2.Item("CENTROSERVIZIO"))
                End If
            End If
        End If

        'TIPOMOV = '13' OR

        Dim cmd1 As New OleDbCommand()
        If k.QuoteDaSoia = 1024 Then
            cmd1.CommandText = ("SELECT  CENTROSERVIZIO, CODICEOSPITE, DATA  From  [IMPORTORETTA]  where codiceospite = ? and (centroservizio = ? Or centroservizio = ? )  union " & _
                                "SELECT  CENTROSERVIZIO, CODICEOSPITE, DATA  From  [IMPORTOOSPITE] where codiceospite = ? and (centroservizio = ? Or centroservizio = ? ) union  " & _
                                "SELECT  CENTROSERVIZIO, CODICEOSPITE, DATASOSIA  From  [IMPORTOCOMUNE] where codiceospite = ? and (centroservizio = ? Or centroservizio = ? ) union  " & _
                                "SELECT  CENTROSERVIZIO, CODICEOSPITE, DATA  From  [IMPORTOJOLLY] where codiceospite = ? and (centroservizio = ? Or centroservizio = ? ) union  " & _
                                "SELECT  CENTROSERVIZIO, CODICEOSPITE, DATA  From  IMPORTOPARENTI where codiceospite = ? and (centroservizio = ? Or centroservizio = ? ) union  " & _
                                "SELECT  CENTROSERVIZIO, CODICEOSPITE, DATA  From  IMPORTORETTA  where codiceospite = ? and (centroservizio = ? Or centroservizio = ? ) union  " & _
                                "SELECT  CENTROSERVIZIO, CODICEOSPITE, DATA  From  STATOAUTO  where codiceospite = ? and (centroservizio = ? Or centroservizio = ? ) union  " & _
                                "SELECT  CENTROSERVIZIO, CODICEOSPITE, DATA  From  MOVIMENTI  where codiceospite = ? and (centroservizio = ? Or centroservizio = ? ) AND ( TIPOMOV = '05') union  " & _
                                "SELECT  CENTROSERVIZIO, CODICEOSPITE, DATA  From  MODALITA where codiceospite = ? and (centroservizio = ? Or centroservizio = ? )  union  " & _
                                "SELECT  CENTROSERVIZIO, CODICEOSPITE, DATA  From  Accompagnamento where codiceospite = ? and (centroservizio = ? Or centroservizio = ? )  ORDER BY DATA ")
        Else
            cmd1.CommandText = ("SELECT  CENTROSERVIZIO, CODICEOSPITE, DATA  From  [IMPORTORETTA]  where codiceospite = ? and (centroservizio = ? Or centroservizio = ? )  union " & _
                            "SELECT  CENTROSERVIZIO, CODICEOSPITE, DATA  From  [IMPORTOOSPITE] where codiceospite = ? and (centroservizio = ? Or centroservizio = ? ) union  " & _
                            "SELECT  CENTROSERVIZIO, CODICEOSPITE, DATA  From  [IMPORTOCOMUNE] where codiceospite = ? and (centroservizio = ? Or centroservizio = ? ) union  " & _
                            "SELECT  CENTROSERVIZIO, CODICEOSPITE, DATA  From  [IMPORTOJOLLY] where codiceospite = ? and (centroservizio = ? Or centroservizio = ? ) union  " & _
                            "SELECT  CENTROSERVIZIO, CODICEOSPITE, DATA  From  IMPORTOPARENTI where codiceospite = ? and (centroservizio = ? Or centroservizio = ? ) union  " & _
                            "SELECT  CENTROSERVIZIO, CODICEOSPITE, DATA  From  IMPORTORETTA  where codiceospite = ? and (centroservizio = ? Or centroservizio = ? ) union  " & _
                            "SELECT  CENTROSERVIZIO, CODICEOSPITE, DATA  From  STATOAUTO  where codiceospite = ? and (centroservizio = ? Or centroservizio = ? ) union  " & _
                            "SELECT  CENTROSERVIZIO, CODICEOSPITE, DATA  From  MOVIMENTI  where codiceospite = ? and (centroservizio = ? Or centroservizio = ? ) AND ( TIPOMOV = '05') union  " & _
                            "SELECT  CENTROSERVIZIO, CODICEOSPITE, DATA  From  MODALITA where codiceospite = ? and (centroservizio = ? Or centroservizio = ? )  union  " & _
                            "SELECT  CENTROSERVIZIO, CODICEOSPITE, DATA  From  Accompagnamento where codiceospite = ? and (centroservizio = ? Or centroservizio = ? )  ORDER BY DATA ")

        End If

        cmd1.Connection = cn
        cmd1.Parameters.AddWithValue("@CodiceOSpite", Anag.CodiceOspite)
        cmd1.Parameters.AddWithValue("@CodiceOSpite", Centro1)
        cmd1.Parameters.AddWithValue("@CodiceOSpite", Centro2)
        cmd1.Parameters.AddWithValue("@CodiceOSpite", Anag.CodiceOspite)
        cmd1.Parameters.AddWithValue("@CodiceOSpite", Centro1)
        cmd1.Parameters.AddWithValue("@CodiceOSpite", Centro2)
        cmd1.Parameters.AddWithValue("@CodiceOSpite", Anag.CodiceOspite)
        cmd1.Parameters.AddWithValue("@CodiceOSpite", Centro1)
        cmd1.Parameters.AddWithValue("@CodiceOSpite", Centro2)
        cmd1.Parameters.AddWithValue("@CodiceOSpite", Anag.CodiceOspite)
        cmd1.Parameters.AddWithValue("@CodiceOSpite", Centro1)
        cmd1.Parameters.AddWithValue("@CodiceOSpite", Centro2)
        cmd1.Parameters.AddWithValue("@CodiceOSpite", Anag.CodiceOspite)
        cmd1.Parameters.AddWithValue("@CodiceOSpite", Centro1)
        cmd1.Parameters.AddWithValue("@CodiceOSpite", Centro2)
        cmd1.Parameters.AddWithValue("@CodiceOSpite", Anag.CodiceOspite)
        cmd1.Parameters.AddWithValue("@CodiceOSpite", Centro1)
        cmd1.Parameters.AddWithValue("@CodiceOSpite", Centro2)
        cmd1.Parameters.AddWithValue("@CodiceOSpite", Anag.CodiceOspite)
        cmd1.Parameters.AddWithValue("@CodiceOSpite", Centro1)
        cmd1.Parameters.AddWithValue("@CodiceOSpite", Centro2)
        cmd1.Parameters.AddWithValue("@CodiceOSpite", Anag.CodiceOspite)
        cmd1.Parameters.AddWithValue("@CodiceOSpite", Centro1)
        cmd1.Parameters.AddWithValue("@CodiceOSpite", Centro2)
        cmd1.Parameters.AddWithValue("@CodiceOSpite", Anag.CodiceOspite)
        cmd1.Parameters.AddWithValue("@CodiceOSpite", Centro1)
        cmd1.Parameters.AddWithValue("@CodiceOSpite", Centro2)
        cmd1.Parameters.AddWithValue("@CodiceOSpite", Anag.CodiceOspite)
        cmd1.Parameters.AddWithValue("@CodiceOSpite", Centro1)
        cmd1.Parameters.AddWithValue("@CodiceOSpite", Centro2)


        Dim PassatoData As String = ""
        Dim PassatoCentroServizio As String = ""
        Dim UltimaCentroServizio As String = ""

        Entrato = True
        Dim Reader As OleDbDataReader = cmd1.ExecuteReader()
        Do While Reader.Read
            CS.CENTROSERVIZIO = campodb(Reader.Item("CENTROSERVIZIO"))

            If Format(UltimaData, "yyyyMMdd") < Format(DataInizio, "yyyyMMdd") And Format(Reader.Item("Data"), "yyyyMMdd") > Format(DataFine, "yyyyMMdd") And Entrato = False Then
                Dim Appoggio As New QuoteOspitiPeriodo


                Appoggio.CF = CodiceFiscale
                Appoggio.Data = Format(UltimaData, "dd/MM/yyyy")
                Appoggio.QuotaOspite = UltimeQuoteO
                Appoggio.QuotaUsl = UltimeQuoteR
                Appoggio.QuotaComune = UltimeQuoteC
                Appoggio.QuotaAltriEnti = UltimeQuoteJ
                Appoggio.QuotaAltroEnteSanitario = UltimeQuoteJSanitario
                Appoggio.QuotaParenti = UltimeQuoteP
                Appoggio.AssegnoAccompagnamento = UltimeAccompagnamento
                XS.Singolo.Add(Appoggio)
                i = i + 1
                Entrato = True
            End If

            If Format(Reader.Item("Data"), "yyyyMMdd") > Format(DataInizio, "yyyyMMdd") And Format(Reader.Item("Data"), "yyyyMMdd") <= Format(DataFine, "yyyyMMdd") And Entrato = False Then
                If PassatoData = Format(Reader.Item("Data"), "yyyyMMdd") And PassatoCentroServizio = campodb(Reader.Item("CENTROSERVIZIO")) Then
                Else
                    Dim Appoggio As New QuoteOspitiPeriodo


                    Appoggio.CF = CodiceFiscale
                    Appoggio.Data = Format(UltimaData, "dd/MM/yyyy")
                    Appoggio.QuotaOspite = UltimeQuoteO
                    Appoggio.QuotaUsl = UltimeQuoteR
                    Appoggio.QuotaComune = UltimeQuoteC
                    Appoggio.QuotaAltriEnti = UltimeQuoteJ
                    Appoggio.QuotaAltroEnteSanitario = UltimeQuoteJSanitario
                    Appoggio.QuotaParenti = UltimeQuoteP
                    Appoggio.AssegnoAccompagnamento = UltimeAccompagnamento
                    XS.Singolo.Add(Appoggio)
                    i = i + 1
                    Entrato = True
                End If
            End If

            Entrato = False
            UltimaData = campodb(Reader.Item("Data"))
            UltimaCentroServizio = campodb(Reader.Item("CENTROSERVIZIO"))

            Dim MAccom As New Cls_Accompagnamento

            MAccom.CODICEOSPITE = Anag.CodiceOspite
            MAccom.CENTROSERVIZIO = CS.CENTROSERVIZIO
            MAccom.LeggiAData(DbC.Ospiti, UltimaData)

            If MAccom.Accompagnamento = "1" Then
                UltimeAccompagnamento = "S"
            Else
                UltimeAccompagnamento = ""
            End If

            UltimeQuoteO = Math.Round(k.QuoteGiornaliere(CS.CENTROSERVIZIO, Anag.CodiceOspite, "O", 0, Reader.Item("Data")), 2)

            If Anag.ImportoSconto > 0 Then
                UltimeQuoteO = UltimeQuoteO - Modulo.MathRound(UltimeQuoteO * Anag.ImportoSconto / 100, 2)
            End If

            If k.QuoteMensile(CS.CENTROSERVIZIO, Anag.CodiceOspite, "O", 0, Reader.Item("Data")) > 0 Then
                Dim ImportoMensile As Double = Math.Round(k.QuoteMensile(CS.CENTROSERVIZIO, Anag.CodiceOspite, "O", 0, Reader.Item("Data")), 2)

                Dim Parente As New Cls_Parenti

                Parente.CodiceOspite = Anag.CodiceOspite
                Parente.Leggi(DbC.Ospiti, Anag.CodiceOspite, 1)

                ImportoMensile = ImportoMensile - (ImportoMensile * Anag.ImportoSconto / 100)

                UltimeQuoteO = Modulo.MathRound(k.Mensile(ImportoMensile, Month(Reader.Item("Data")), Year(Reader.Item("Data")), UltimaCentroServizio), 2)
            End If

            UltimeQuoteP = 0

            Dim TotaleMensile As Double = 0
            Dim Par As New Cls_Parenti

            Par.CodiceOspite = Anag.CodiceOspite
            Par.CodiceParente = 1
            Par.Leggi(DbC.Ospiti, Par.CodiceOspite, Par.CodiceParente)

            If Not IsNothing(Par.Nome) And Par.Nome <> "" Then
                UltimeQuoteP = Math.Round(k.QuoteGiornaliere(CS.CENTROSERVIZIO, Anag.CodiceOspite, "P", 1, Reader.Item("Data")), 2)
                If k.QuoteMensile(CS.CENTROSERVIZIO, Anag.CodiceOspite, "P", 1, Reader.Item("Data")) > 0 Then
                    Dim ImportoMensile As Double = Math.Round(k.QuoteMensile(CS.CENTROSERVIZIO, Anag.CodiceOspite, "P", 1, Reader.Item("Data")), 2)

                    Dim Parente As New Cls_Parenti

                    Parente.CodiceOspite = Anag.CodiceOspite
                    Parente.Leggi(DbC.Ospiti, Anag.CodiceOspite, 1)

                    ImportoMensile = ImportoMensile - (ImportoMensile * Parente.ImportoSconto / 100)

                    TotaleMensile = TotaleMensile + Modulo.MathRound(k.Mensile(ImportoMensile, Month(Reader.Item("Data")), Year(Reader.Item("Data")), UltimaCentroServizio), 2)
                Else

                    If Par.ImportoSconto > 0 Then
                        UltimeQuoteP = UltimeQuoteP - Modulo.MathRound(UltimeQuoteP * Par.ImportoSconto / 100, 2)
                        If TotaleMensile > 0 Then
                            TotaleMensile = TotaleMensile - Modulo.MathRound(TotaleMensile * Par.ImportoSconto / 100, 2)
                        End If
                    End If
                End If
            End If

            Par.Nome = ""
            Par.CodiceOspite = Anag.CodiceOspite
            Par.CodiceParente = 2
            Par.Leggi(DbC.Ospiti, Par.CodiceOspite, Par.CodiceParente)

            If Not IsNothing(Par.Nome) And Par.Nome <> "" Then

                UltimeQuoteP = UltimeQuoteP + Math.Round(k.QuoteGiornaliere(CS.CENTROSERVIZIO, Anag.CodiceOspite, "P", 2, Reader.Item("Data")), 2)

                If k.QuoteMensile(CS.CENTROSERVIZIO, Anag.CodiceOspite, "P", 2, Reader.Item("Data")) > 0 Then
                    Dim ImportoMensile As Double = Math.Round(k.QuoteMensile(CS.CENTROSERVIZIO, Anag.CodiceOspite, "P", 2, Reader.Item("Data")), 2)

                    ImportoMensile = ImportoMensile - (ImportoMensile * Anag.ImportoSconto / 100)

                    TotaleMensile = TotaleMensile + Modulo.MathRound(k.Mensile(ImportoMensile, Month(Reader.Item("Data")), Year(Reader.Item("Data")), UltimaCentroServizio), 2)
                Else
                    If Par.ImportoSconto > 0 Then
                        UltimeQuoteP = UltimeQuoteP - Modulo.MathRound(UltimeQuoteP * Par.ImportoSconto / 100, 2)
                        If TotaleMensile > 0 Then
                            TotaleMensile = TotaleMensile - Modulo.MathRound(TotaleMensile * Par.ImportoSconto / 100, 2)
                        End If
                    End If
                End If
            End If

            Par.Nome = ""
            Par.CodiceOspite = Anag.CodiceOspite
            Par.CodiceParente = 3
            Par.Leggi(DbC.Ospiti, Par.CodiceOspite, Par.CodiceParente)

            If Not IsNothing(Par.Nome) And Par.Nome <> "" Then
                UltimeQuoteP = UltimeQuoteP + Math.Round(k.QuoteGiornaliere(CS.CENTROSERVIZIO, Anag.CodiceOspite, "P", 3, Reader.Item("Data")), 2)

                If k.QuoteMensile(CS.CENTROSERVIZIO, Anag.CodiceOspite, "P", 3, Reader.Item("Data")) > 0 Then
                    Dim ImportoMensile As Double = Math.Round(k.QuoteMensile(CS.CENTROSERVIZIO, Anag.CodiceOspite, "P", 3, Reader.Item("Data")), 2)

                    ImportoMensile = ImportoMensile - (ImportoMensile * Anag.ImportoSconto / 100)

                    TotaleMensile = TotaleMensile + Modulo.MathRound(k.Mensile(ImportoMensile, Month(Reader.Item("Data")), Year(Reader.Item("Data")), UltimaCentroServizio), 2)
                Else
                    If Par.ImportoSconto > 0 Then
                        UltimeQuoteP = UltimeQuoteP - Modulo.MathRound(UltimeQuoteP * Par.ImportoSconto / 100, 2)
                        If TotaleMensile > 0 Then
                            TotaleMensile = TotaleMensile - Modulo.MathRound(TotaleMensile * Par.ImportoSconto / 100, 2)
                        End If
                    End If
                End If
            End If

            Par.Nome = ""
            Par.CodiceOspite = Anag.CodiceOspite
            Par.CodiceParente = 4
            Par.Leggi(DbC.Ospiti, Par.CodiceOspite, Par.CodiceParente)

            If Not IsNothing(Par.Nome) And Par.Nome <> "" Then
                UltimeQuoteP = UltimeQuoteP + Math.Round(k.QuoteGiornaliere(CS.CENTROSERVIZIO, Anag.CodiceOspite, "P", 4, Reader.Item("Data")), 2)

                If k.QuoteMensile(CS.CENTROSERVIZIO, Anag.CodiceOspite, "P", 4, Reader.Item("Data")) > 0 Then
                    Dim ImportoMensile As Double = Math.Round(k.QuoteMensile(CS.CENTROSERVIZIO, Anag.CodiceOspite, "P", 4, Reader.Item("Data")), 2)

                    ImportoMensile = ImportoMensile - (ImportoMensile * Anag.ImportoSconto / 100)

                    TotaleMensile = TotaleMensile + Modulo.MathRound(k.Mensile(ImportoMensile, Month(Reader.Item("Data")), Year(Reader.Item("Data")), UltimaCentroServizio), 2)
                Else
                    If Par.ImportoSconto > 0 Then
                        UltimeQuoteP = UltimeQuoteP - Modulo.MathRound(UltimeQuoteP * Par.ImportoSconto / 100, 2)
                        If TotaleMensile > 0 Then
                            TotaleMensile = TotaleMensile - Modulo.MathRound(TotaleMensile * Par.ImportoSconto / 100, 2)
                        End If
                    End If
                End If
            End If
                  
            Dim IndiceParente As Integer    

            For IndiceParente = 5 To 10

                Par.Nome = ""
                Par.CodiceOspite = Anag.CodiceOspite
                Par.CodiceParente = IndiceParente
                Par.Leggi(DbC.Ospiti, Par.CodiceOspite, Par.CodiceParente)

                If Not IsNothing(Par.Nome) And Par.Nome <> "" Then
                    UltimeQuoteP = UltimeQuoteP + Math.Round(k.QuoteGiornaliere(CS.CENTROSERVIZIO, Anag.CodiceOspite, "P", IndiceParente, Reader.Item("Data")), 2)

                    If k.QuoteMensile(CS.CENTROSERVIZIO, Anag.CodiceOspite, "P", IndiceParente, Reader.Item("Data")) > 0 Then
                        Dim ImportoMensile As Double = Math.Round(k.QuoteMensile(CS.CENTROSERVIZIO, Anag.CodiceOspite, "P", IndiceParente, Reader.Item("Data")), 2)

                        ImportoMensile = ImportoMensile - (ImportoMensile * Anag.ImportoSconto / 100)

                        TotaleMensile = TotaleMensile + Modulo.MathRound(k.Mensile(ImportoMensile, Month(Reader.Item("Data")), Year(Reader.Item("Data")), UltimaCentroServizio), 2)
                    Else
                        If Par.ImportoSconto > 0 Then
                            UltimeQuoteP = UltimeQuoteP - Modulo.MathRound(UltimeQuoteP * Par.ImportoSconto / 100, 2)
                            If TotaleMensile > 0 Then
                                TotaleMensile = TotaleMensile - Modulo.MathRound(TotaleMensile * Par.ImportoSconto / 100, 2)
                            End If
                        End If
                    End If
                End If                  
            Next


            If TotaleMensile > 0 Then
                UltimeQuoteP = TotaleMensile
            End If

            UltimeQuoteC = Math.Round(k.QuoteGiornaliere(CS.CENTROSERVIZIO, Anag.CodiceOspite, "C", 0, Reader.Item("Data")), 2)
            UltimeQuoteR = Math.Round(k.QuoteGiornaliere(CS.CENTROSERVIZIO, Anag.CodiceOspite, "R", 0, Reader.Item("Data")), 2)
            If UltimeQuoteR = 0 Then
                UltimeQuoteR = Math.Round(k.QuoteGiornaliere(CS.CENTROSERVIZIO, Anag.CodiceOspite, "R", 0, DataInizio), 2)
            End If
            UltimeQuoteJ = Math.Round(k.QuoteGiornaliere(CS.CENTROSERVIZIO, Anag.CodiceOspite, "J", 0, Reader.Item("Data")), 2)
            UltimeQuoteJSanitario = 0
            If UltimeQuoteJ > 0 Then
                Dim Jolly As New Cls_ImportoJolly

                Jolly.CODICEOSPITE = Anag.CodiceOspite
                Jolly.CENTROSERVIZIO = CS.CENTROSERVIZIO
                Jolly.UltimaAData(DbC.Ospiti, Jolly.CODICEOSPITE, Jolly.CENTROSERVIZIO, Reader.Item("Data"))
                If Jolly.PROV = "" Then
                    Jolly.UltimaData(DbC.Ospiti, Jolly.CODICEOSPITE, Jolly.CENTROSERVIZIO)
                End If

                Dim Comune As New ClsComune

                Comune.Provincia = Jolly.PROV
                Comune.Comune = Jolly.COMUNE
                Comune.Leggi(DbC.Ospiti)
                If Comune.TIPOENTE = "S" Then
                    UltimeQuoteJSanitario = UltimeQuoteJ
                    UltimeQuoteJ = 0
                End If
            End If

            If DbC.Ospiti.ToLower.IndexOf("betulle") > 0 Then
                Dim M As New Cls_AddebitoAccreditiProgrammati

                M.CentroServizio = CS.CENTROSERVIZIO
                M.CodiceOspite = Anag.CodiceOspite
                M.UltimaData(DbC.Ospiti)

                UltimeQuoteJ = M.Importo
                If k.QuoteMensile(CS.CENTROSERVIZIO, Anag.CodiceOspite, "O", 0, Reader.Item("Data")) > 0 Then
                    UltimeQuoteO = Math.Round(k.QuoteMensile(CS.CENTROSERVIZIO, Anag.CodiceOspite, "O", 0, Reader.Item("Data")), 2)
                End If
                If UltimeQuoteP > 0 Then
                    Dim TotParentiMensile As Double
                    TotParentiMensile = Math.Round(k.QuoteMensile(CS.CENTROSERVIZIO, Anag.CodiceOspite, "P", 1, Reader.Item("Data")), 2)
                    TotParentiMensile = TotParentiMensile + Math.Round(k.QuoteMensile(CS.CENTROSERVIZIO, Anag.CodiceOspite, "P", 2, Reader.Item("Data")), 2)
                    TotParentiMensile = TotParentiMensile + Math.Round(k.QuoteMensile(CS.CENTROSERVIZIO, Anag.CodiceOspite, "P", 3, Reader.Item("Data")), 2)
                    If TotParentiMensile > 0 Then
                        UltimeQuoteP = TotParentiMensile
                    End If
                End If
            End If

            If Format(Reader.Item("Data"), "yyyyMMdd") > Format(DataInizio, "yyyyMMdd") And Format(Reader.Item("Data"), "yyyyMMdd") <= Format(DataFine, "yyyyMMdd") Then
                If PassatoData = Format(Reader.Item("Data"), "yyyyMMdd") And PassatoCentroServizio = campodb(Reader.Item("CENTROSERVIZIO")) Then
                Else
                    Dim Appoggio As New QuoteOspitiPeriodo

                    Appoggio.CF = CodiceFiscale
                    Appoggio.Data = Format(UltimaData, "dd/MM/yyyy")
                    Appoggio.QuotaOspite = UltimeQuoteO
                    Appoggio.QuotaUsl = UltimeQuoteR
                    Appoggio.QuotaComune = UltimeQuoteC
                    Appoggio.QuotaAltriEnti = UltimeQuoteJ
                    Appoggio.QuotaAltroEnteSanitario = UltimeQuoteJSanitario
                    Appoggio.QuotaParenti = UltimeQuoteP
                    Appoggio.AssegnoAccompagnamento = UltimeAccompagnamento

                    XS.Singolo.Add(Appoggio)
                    i = i + 1

                    PassatoData = Format(Reader.Item("Data"), "yyyyMMdd")
                    PassatoCentroServizio = campodb(Reader.Item("CENTROSERVIZIO"))


                    Entrato = True
                End If
            End If


            If Format(Reader.Item("Data"), "yyyyMMdd") = Format(DataInizio, "yyyyMMdd") Then
                If PassatoData = Format(Reader.Item("Data"), "yyyyMMdd") And PassatoCentroServizio = campodb(Reader.Item("CENTROSERVIZIO")) Then
                Else
                    If Format(UltimaData, "yyyyMMdd") <= Format(DataFine, "yyyyMMdd") Then
                        Dim Appoggio As New QuoteOspitiPeriodo

                        Appoggio.CF = CodiceFiscale
                        Appoggio.Data = Format(UltimaData, "dd/MM/yyyy")
                        Appoggio.QuotaOspite = UltimeQuoteO
                        Appoggio.QuotaUsl = UltimeQuoteR
                        Appoggio.QuotaComune = UltimeQuoteC
                        Appoggio.QuotaAltriEnti = UltimeQuoteJ
                        Appoggio.QuotaAltroEnteSanitario = UltimeQuoteJSanitario
                        Appoggio.QuotaParenti = UltimeQuoteP
                        Appoggio.AssegnoAccompagnamento = UltimeAccompagnamento

                        XS.Singolo.Add(Appoggio)
                        i = i + 1

                        PassatoData = Format(Reader.Item("Data"), "yyyyMMdd")
                        PassatoCentroServizio = campodb(Reader.Item("CENTROSERVIZIO"))

                        Entrato = True
                    End If
                End If
            End If

        Loop
        Reader.Close()
        cn.Close()



        If Entrato = False Then

            If PassatoData = Format(UltimaData, "yyyyMMdd") And PassatoCentroServizio = UltimaCentroServizio Then
            Else
                If Format(UltimaData, "yyyyMMdd") <= Format(DataFine, "yyyyMMdd") Then
                    Dim Appoggio As New QuoteOspitiPeriodo

                    Appoggio.CF = CodiceFiscale  '& campodb(Reader.Item("CENTROSERVIZIO"))
                    Appoggio.Data = Format(UltimaData, "dd/MM/yyyy")
                    Appoggio.QuotaOspite = UltimeQuoteO
                    Appoggio.QuotaUsl = UltimeQuoteR
                    Appoggio.QuotaComune = UltimeQuoteC
                    Appoggio.QuotaAltriEnti = UltimeQuoteJ
                    Appoggio.QuotaAltroEnteSanitario = UltimeQuoteJSanitario
                    Appoggio.QuotaParenti = UltimeQuoteP
                    Appoggio.AssegnoAccompagnamento = UltimeAccompagnamento

                    XS.Singolo.Add(Appoggio)
                    i = i + 1
                End If
            End If
        End If


        'Dim XSCpy As New QuoteOspitiPeriodoCFs

        'For i = XS.Singolo.Count - 1 To 0 Step -1
        '    Dim VerificaAttualeComune As Double
        '    Dim VerificaPrecedenteComune As Double

        '    Dim VerificaAttualeOP As Double
        '    Dim VerificaPrecedenteOP As Double

        '    VerificaAttualeComune = XS.Singolo(i).QuotaComune
        '    If i > 0 Then
        '        VerificaPrecedenteComune = XS.Singolo(i - 1).QuotaComune
        '    Else
        '        VerificaPrecedenteComune = 0
        '    End If


        '    VerificaAttualeOP = XS.Singolo(i).QuotaOspite + XS.Singolo(i).QuotaParenti
        '    If i > 0 Then
        '        VerificaPrecedenteOP = XS.Singolo(i - 1).QuotaOspite + XS.Singolo(i - 1).QuotaParenti
        '    Else
        '        VerificaPrecedenteOP = 0
        '    End If


        '    If Math.Round(VerificaAttualeComune, 2) <> Math.Round(VerificaPrecedenteComune, 2) Or _
        '       Math.Round(VerificaAttualeOP, 2) <> Math.Round(VerificaPrecedenteOP, 2) Then

        '        Dim Appoggio As New QuoteOspitiPeriodo

        '        Appoggio.CF = XS.Singolo(i).CF
        '        Appoggio.Data = XS.Singolo(i).Data
        '        Appoggio.QuotaOspite = XS.Singolo(i).QuotaOspite
        '        Appoggio.QuotaUsl = XS.Singolo(i).QuotaUsl
        '        Appoggio.QuotaComune = XS.Singolo(i).QuotaComune
        '        Appoggio.QuotaAltriEnti = XS.Singolo(i).QuotaAltriEnti
        '        Appoggio.QuotaParenti = XS.Singolo(i).QuotaParenti

        '        XSCpy.Singolo.Add(Appoggio)

        '    End If
        'Next

        Return XS
    End Function


    <WebMethod()> _
    Public Function ImportiStati_ArrayCF(ByVal Utente As String, ByVal Password As String, ByVal CodiceFiscali As String, ByVal CentroServizio As String, ByVal DataDal As String, ByVal DataAl As String, ByVal Sosia As String) As String

        Dim i As Integer
        Dim XS As New QuoteOspitiPeriodoCFs
        Dim CodiceFiscale As String

        Dim DbC As New Cls_Login
        Dim vtCodiceFiscali(300) As String


        vtCodiceFiscali = Split(CodiceFiscali, ",")

        If CentroServizio = 2550 Then
            Utente = Utente & "<2>" ' controllo per parco cave
        End If

        DbC.Utente = Utente
        DbC.Chiave = Password
        DbC.Leggi(Context.Application("SENIOR"))

        If DbC.Ospiti = "" Or IsNothing(DbC.Ospiti) Then
            Context.Response.StatusCode = 401
            Return "ER1"
        End If

        Dim CS As New Cls_CentroServizio

        CS.EPersonam = CentroServizio
        CS.LeggiEpersonam(DbC.Ospiti, CS.EPersonam, True)
        If CS.DESCRIZIONE.Trim = "" Then
            Context.Response.StatusCode = 404
            Return "ER7"
        End If
        i = 0

        For x = 0 To vtCodiceFiscali.Length - 1
            If vtCodiceFiscali(x) <> "" Then
                CodiceFiscale = vtCodiceFiscali(x)

                Dim XSAppoggio As New QuoteOspitiPeriodoCFs
                XSAppoggio = ImportiStatiOspite(Utente, Password, CodiceFiscale, CentroServizio, DataDal, DataAl, Sosia)

                XS.Singolo.AddRange(XSAppoggio.Singolo)
            End If
        Next



        Dim serializer As JavaScriptSerializer
        serializer = New JavaScriptSerializer()

        Dim Serializzazione As String

        Serializzazione = serializer.Serialize(XS)
        Me.Context.Response.StatusCode = 200
        Return Serializzazione
    End Function


    <WebMethod()> _
    Public Function TipoAddebito(ByVal Utente As String, ByVal Password As String) As String
        Dim cnMaster As New OleDbConnection
        Dim ConnessioneOspite As String

        Dim DbC As New Cls_Login

        If Utente = "senioruser_1461" Or Utente = "senioruser_1455" Then
            Utente = "senioruser_1447"
            Password = "$3N!0R_secret!"
        End If


        DbC.Utente = Utente
        DbC.Chiave = Password
        DbC.Leggi(Context.Application("SENIOR"))
        ConnessioneOspite = DbC.Ospiti


        If ConnessioneOspite = "" Then
            Context.Response.StatusCode = 401
            Return "ER1"
        End If

        


        Dim cn As OleDbConnection
        Dim TipoAddebiti As New List(Of MyTipoAddebito)


        cn = New Data.OleDb.OleDbConnection(ConnessioneOspite)

        cn.Open()
        Dim cmd As New OleDbCommand()
        cmd.CommandText = ("select * from TabTipoAddebito  " & _
                           "Order By Descrizione")
        cmd.Connection = cn
        Dim sb As StringBuilder = New StringBuilder

        Dim myPOSTreader As OleDbDataReader = cmd.ExecuteReader()
        Do While myPOSTreader.Read
            Dim x As New MyTipoAddebito


            x.Codice = campodb(myPOSTreader.Item("Codice"))
            x.Descrizione = campodb(myPOSTreader.Item("Descrizione"))
            TipoAddebiti.Add(x)
        Loop
        myPOSTreader.Close()
        cn.Close()



        Dim serializer As JavaScriptSerializer
        serializer = New JavaScriptSerializer()

        Dim Serializzazione As String

        Serializzazione = serializer.Serialize(TipoAddebiti)
        Me.Context.Response.StatusCode = 200



        Dim NomeFile As String

        NomeFile = HostingEnvironment.ApplicationPhysicalPath() & "\Public\TipoADD_" & Format(Now, "yyyyMMddhhmmss") & ".txt"
        Dim tw As System.IO.TextWriter = System.IO.File.CreateText(NomeFile)
        tw.Write(Utente & " " & Now.ToString & " " & Serializzazione)
        tw.Close()


        Return Serializzazione
    End Function

    Public Class MyTipoAddebito
        Public Codice As String
        Public Descrizione As String
    End Class

End Class



