Imports Microsoft.VisualBasic
Imports System.Data.OleDb
Imports System.Web.Hosting

Public Class ClsRitenuta
    Public Codice As String
    Public Descrizione As String
    Public Percentuale As Double
    Public ACarico As String
    Public Mastro As Long
    Public Conto As Long
    Public Sottoconto As Double
    Public Tributo As String


    Sub UpDateDropBox(ByVal StringaConnessione As String, ByRef appoggio As DropDownList)
        Dim cn As OleDbConnection



        cn = New Data.OleDb.OleDbConnection(StringaConnessione)

        cn.Open()
        Dim cmd As New OleDbCommand()
        cmd.CommandText = ("select * from Ritenute order by Descrizione")
        cmd.Connection = cn
        Dim sb As StringBuilder = New StringBuilder

        appoggio.Items.Clear()
        Dim myPOSTreader As OleDbDataReader = cmd.ExecuteReader()
        Do While myPOSTreader.Read
            appoggio.Items.Add(myPOSTreader.Item("Descrizione"))
            appoggio.Items(appoggio.Items.Count - 1).Value = myPOSTreader.Item("Codice")
        Loop
        myPOSTreader.Close()
        cn.Close()
        appoggio.Items.Add("")
        appoggio.Items(appoggio.Items.Count - 1).Value = ""
        appoggio.Items(appoggio.Items.Count - 1).Selected = True

    End Sub

    Function campodb(ByVal oggetto As Object) As String


        If IsDBNull(oggetto) Then
            Return ""
        Else
            Return oggetto
        End If
    End Function

    Function campodbN(ByVal oggetto As Object) As Double


        If IsDBNull(oggetto) Then
            Return 0
        Else
            Return oggetto
        End If
    End Function



    Function InUso(ByVal StringaConnessione As String) As Boolean
        Dim cn As OleDbConnection
        InUso = False


        cn = New Data.OleDb.OleDbConnection(StringaConnessione)

        cn.Open()
        Dim cmd As New OleDbCommand()
        cmd.CommandText = ("select *  from MovimentiContabiliRiga Where CodiceRitenuta = ? ")
        cmd.Parameters.AddWithValue("@Codice", Codice)
        cmd.Connection = cn
        Dim InUsoRd As OleDbDataReader = cmd.ExecuteReader()
        If InUsoRd.Read Then
            InUso = True
        End If
        InUsoRd.Close()
        cn.Close()


    End Function


    Function MaxTipoRitenute(ByVal StringaConnessione As String) As String
        Dim cn As OleDbConnection


        MaxTipoRitenute = ""

        cn = New Data.OleDb.OleDbConnection(StringaConnessione)

        cn.Open()
        Dim cmd As New OleDbCommand()
        cmd.CommandText = ("select max(CASE WHEN (len(Codice) = 1) THEN '0' + CODICE ELSE Codice END) from Ritenute ")
        cmd.Connection = cn
        Dim MaxAddebito As OleDbDataReader = cmd.ExecuteReader()
        If MaxAddebito.Read Then
            Dim MassimoLetto As String

            MassimoLetto = campodb(MaxAddebito.Item(0))

            If MassimoLetto = "Z9" Then
                MassimoLetto = "a0"
                cn.Close()
                Return MassimoLetto
            End If


            If MassimoLetto = "99" Then
                MassimoLetto = "A0"
            Else
                If Mid(MassimoLetto & Space(10), 1, 1) > "0" And Mid(MassimoLetto & Space(10), 1, 1) > "9" Then
                    If Mid(MassimoLetto & Space(10), 2, 1) < "9" Then
                        MassimoLetto = Mid(MassimoLetto & Space(10), 1, 1) & Val(Mid(MassimoLetto & Space(10), 2, 1)) + 1
                    Else
                        Dim CodiceAscii As String
                        CodiceAscii = Asc(Mid(MassimoLetto & Space(10), 1, 1))

                        MassimoLetto = Chr(CodiceAscii + 1) & "0"
                    End If
                Else
                    MassimoLetto = Format(Val(MassimoLetto) + 1, "00")
                End If
            End If

            Return MassimoLetto
        Else
            Return "01"
        End If
        cn.Close()

    End Function


    Sub LeggiDescrizione(ByVal StringaConnessione As String)
        Dim cn As OleDbConnection
        Dim Max As Long

        cn = New Data.OleDb.OleDbConnection(StringaConnessione)

        cn.Open()
        Dim cmd As New OleDbCommand()

        cmd.CommandText = ("select * from Ritenute where " & _
                           "Descrizione  Like ? ")
        cmd.Parameters.AddWithValue("@Descrizione", Descrizione)


        cmd.Connection = cn
        Max = 0
        Dim VerReader As OleDbDataReader = cmd.ExecuteReader()
        If VerReader.Read Then
            Codice = campodb(VerReader.Item("Codice"))
            Call Leggi(StringaConnessione)
        End If
        VerReader.Close()
        cn.Close()
    End Sub

    Sub Leggi(ByVal StringaConnessione As String)
        Dim cn As OleDbConnection
        Dim Max As Long

        cn = New Data.OleDb.OleDbConnection(StringaConnessione)

        cn.Open()
        Dim cmd As New OleDbCommand()

        cmd.CommandText = ("select * from Ritenute where " & _
                           "Codice  = ? ")
        cmd.Parameters.AddWithValue("@Codice", Codice)


        cmd.Connection = cn
        Max = 0
        Dim VerReader As OleDbDataReader = cmd.ExecuteReader()
        If VerReader.Read Then
            Codice = campodb(VerReader.Item("Codice"))
            Descrizione = campodb(VerReader.Item("Descrizione"))
    
            Percentuale = campodbN(VerReader.Item("Percentuale"))
            ACarico = campodb(VerReader.Item("ACarico"))
            Mastro = campodbN(VerReader.Item("Mastro"))
            Conto = campodbN(VerReader.Item("Conto"))
            Sottoconto = campodbN(VerReader.Item("Sottoconto"))

            Tributo = campodb(VerReader.Item("Tributo"))

        End If
        VerReader.Close()
        cn.Close()
    End Sub

    Sub Delete(ByVal StringaConnessione As String)
        Dim cn As OleDbConnection

        cn = New Data.OleDb.OleDbConnection(StringaConnessione)

        cn.Open()
        Dim cmd As New OleDbCommand()
        cmd.CommandText = ("Delete from Ritenute Where Codice = ? ")
        cmd.Connection = cn
        cmd.Parameters.AddWithValue("@Codice", Codice)
        cmd.ExecuteNonQuery()
        cn.Close()
    End Sub

    Sub Scrivi(ByVal StringaConnessione As String)
        Dim cn As OleDbConnection
        Dim Esiste As Boolean

        cn = New Data.OleDb.OleDbConnection(StringaConnessione)

        cn.Open()
        Dim cmd As New OleDbCommand()

        cmd.CommandText = ("select * from Ritenute where " & _
                           "Codice  = ? ")
        cmd.Parameters.AddWithValue("@Codice", Codice)


        cmd.Connection = cn

        Esiste = False
        Dim VerReader As OleDbDataReader = cmd.ExecuteReader()
        If VerReader.Read Then
            Esiste = True
        End If
        VerReader.Close()

        Dim MySql As String


        If Esiste = True Then

            
            MySql = "UPDATE Ritenute SET " & _
                    " Descrizione  = ?, " & _
                    " Percentuale  = ?, " & _
                    " ACarico  = ?, " & _
                    " Mastro  = ?, " & _
                    " Conto  = ?, " & _
                    " Sottoconto  = ?, " & _
                    " Tributo = ? " & _
                    " Where Codice = ? "
        Else
            MySql = "INSERT INTO Ritenute (Descrizione,Percentuale,ACarico,Mastro,Conto,Sottoconto,Tributo,Codice)  VALUES " & _
                                             "(?,?,?,?,?,?,?,?)"
        End If


        Dim cmd1 As New OleDbCommand()
        cmd1.Connection = cn
        cmd1.CommandText = MySql
        cmd1.Parameters.AddWithValue("@Descrizione", Descrizione)
        cmd1.Parameters.AddWithValue("@Percentuale", Percentuale)
        cmd1.Parameters.AddWithValue("@ACarico", IIf(IsNothing(ACarico), "", ACarico))
        cmd1.Parameters.AddWithValue("@Mastro", Mastro)
        cmd1.Parameters.AddWithValue("@Conto", Conto)
        cmd1.Parameters.AddWithValue("@Sottoconto", Sottoconto)
        cmd1.Parameters.AddWithValue("@Tributo", Tributo)
        cmd1.Parameters.AddWithValue("@Codice", Codice)
        cmd1.ExecuteNonQuery()

        cn.Close()
    End Sub


End Class
