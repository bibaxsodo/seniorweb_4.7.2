Imports Microsoft.VisualBasic
Imports System.Data.OleDb
Imports System.Web.Hosting
Imports System.Data.SqlTypes
Imports System.Web.Script
Imports System.Web.Script.Serialization

Public Class ClsOspite
    Public CodiceOspite As Integer
    Public CodiceMedico As String
    Public RecapitoNome As String
    Public RecapitoIndirizzo As String
    Public RecapitoProvincia As String
    Public RecapitoComune As String

    Public RESIDENZADATA1 As Date
    Public RESIDENZADATA2 As Date
    Public RESIDENZADATA3 As Date
    Public RESIDENZADATA4 As Date

    Public RESIDENZAINDIRIZZO1 As String
    Public RESIDENZAINDIRIZZO2 As String
    Public RESIDENZAINDIRIZZO3 As String
    Public RESIDENZAINDIRIZZO4 As String

    Public RESIDENZACOMUNE1 As String
    Public RESIDENZACOMUNE2 As String
    Public RESIDENZACOMUNE3 As String
    Public RESIDENZACOMUNE4 As String

    Public RESIDENZAPROVINCIA1 As String
    Public RESIDENZAPROVINCIA2 As String
    Public RESIDENZAPROVINCIA3 As String
    Public RESIDENZAPROVINCIA4 As String

    Public RESIDENZACAP1 As String
    Public RESIDENZACAP2 As String
    Public RESIDENZACAP3 As String
    Public RESIDENZACAP4 As String

    Public RESIDENZATELEFONO1 As String
    Public RESIDENZATELEFONO2 As String
    Public RESIDENZATELEFONO3 As String
    Public RESIDENZATELEFONO4 As String
    Public CODICEFISCALE As String

    Public CODICEIVA As String
    Public TIPOOPERAZIONE As String
    Public MODALITAPAGAMENTO As String

    Public MastroCliente As Integer
    Public ContoCliente As Integer
    Public SottoContoCliente As Integer

    Public Nome As String
    Public Sesso As String

    Public CodiceSanitario As String
    Public Compensazione As String
    Public ComuneDiNascita As String
    Public ProvinciaDiNascita As String
    Public DataDomanda As Date
    Public DataNascita As Date
    Public EsenzioneTicket As String
    Public NOMECONIUGE As String
    Public NOMEPADRE As String
    Public NOMEMADRE As String
    Public PROFCONIUGE As String

    Public SETTIMANA As String
    Public TELEFONO1 As String
    Public TELEFONO2 As String
    Public TELEFONO3 As String
    Public TELEFONO4 As String
    Public USL As String

    Public PERIODO As String
    Public NOTE As String
    Public TIPOLOGIA As String
    Public UTENTE As String
    Public DATAAGGIORNAMENTO As String
    Public FattAnticipata As String
    Public GradoParentela As String

    Public TipoOspite As String
    Public DataDeposito As Date
    Public ImportoDeposito As Double
    Public NonInUso As String

    Public CodiceTicket As String
    Public DataRilascio As Date
    Public DataScadenza As Date
    Public CodiceTicket2 As String
    Public DataRilascio2 As Date
    Public DataScadenza2 As Date

    Public NoteTicket As String
    Public PathImmagine As String
    Public NumeroDocumento As String
    Public DataDocumento As Date
    Public EmettitoreDocumento As String
    Public Nazionalita As String
    Public MesiAnticipo As Long
    Public CodiceCIG As String
    Public EmOspiteParente As Long
    Public DataAccreditoDeposito As Date
    Public DATAMORTE As Date
    Public LuogoMorte As String
    Public ExtraFuoriRetta As Long


    Public AssistenteSociale As String
    Public CodiceLavanderia As String
    Public Distretto As String
    Public ParenteIndirizzo As Integer


    Public CodiceDestinatario As String
    Public CodificaProvincia As String
    Public LetteraProvincia As String
    Public ProvinciaRilascio As String
    Public TipoDocumento As String
    Public ComuneRilascio As String
    Public CodiceQuestura As String

    Public CognomeOspite As String
    Public NomeOspite As String
    Public TesseraSanitaria As String
    Public StatoCivile As String
    Public StrutturaProvenienza As String
    Public ScadenzaDocumento As Date


    Public CABCLIENTE As String
    Public ABICLIENTE As String
    Public IntCliente As String
    Public NumeroControlloCliente As String
    Public CINCLIENTE As String
    Public CCBANCARIOCLIENTE As String
    Public BancaCliente As String

    Public IntestatarioCC As String
    Public CodiceFiscaleCC As String
    Public RotturaOspite As String
    Public Vincolo As String
    Public ImportoSconto As Double
    Public IdEpersonam As Integer
    Public TipoSconto As String
    Public CONTOPERESATTO As String
    Public Opposizione730 As Integer

    Public NomeMedico As String
    Public CONSENSOINSERIMENTO As Integer
    Public CONSENSOMARKETING As Integer
    Public CartellaClinica As String
    Public CodiceCup As String 'usato come export

    ' Codice Servizio non in anagrafica comune
    Public CodiceCentroServizio As String


    Function MaxTesseraSanitaria(ByVal StringaConnessione As String) As String
        Dim cn As OleDbConnection
        Dim MySql As String

        cn = New Data.OleDb.OleDbConnection(StringaConnessione)

        cn.Open()

        Dim cmd As New OleDbCommand()
        cmd.Connection = cn

        If CodiceCentroServizio = "" Then
            MySql = "Select max(case when isnumeric(CartellaClinica) =1  THEN CAST(CartellaClinica AS Int) else 0 end) from AnagraficaComune where codiceospite > 0 and CartellaClinica like '" & Year(Now) & "%'"
        Else
            MySql = "Select max(case when isnumeric(CartellaClinica) =1  THEN CAST(CartellaClinica AS Int) else 0 end) from AnagraficaComune where codiceospite > 0  And (select top 1 CentroServizio From Movimenti Where Movimenti.CodiceOspite = AnagraficaComune.Codiceospite Order by DATA desc) = '" & CodiceCentroServizio & "' and CartellaClinica like '" & Year(Now) & "%'"
        End If
        cmd.CommandText = MySql
        Dim myPOSTreader As OleDbDataReader = cmd.ExecuteReader()
        If myPOSTreader.Read() Then
            MaxTesseraSanitaria = campodbn(myPOSTreader.Item(0)) + 1
            If MaxTesseraSanitaria = 1 Then
                MaxTesseraSanitaria = Year(Now) & "00001"
            End If
        Else
            MaxTesseraSanitaria = Year(Now) & "00001"
        End If
        myPOSTreader.Close()


        cn.Close()
    End Function


    Sub InserisciOspiteSeCFNONPresente(ByVal StringaConnessione As String, ByVal Cognome As String, ByVal Nome As String, ByVal DataNascita As Date, ByVal CodiceFiscale As String)
        Dim cn As OleDbConnection
        Dim MySql As String

        cn = New Data.OleDb.OleDbConnection(StringaConnessione)

        Call Pulisci()
        cn.Open()
        Dim cmd As New OleDbCommand()
        cmd.Connection = cn

        MySql = "Select max(CodiceOspite) from AnagraficaComune "
        cmd.CommandText = MySql
        Dim myPOSTreader As OleDbDataReader = cmd.ExecuteReader()
        If myPOSTreader.Read() Then
            CodiceOspite = campodbn(myPOSTreader.Item(0)) + 1
        End If
        myPOSTreader.Close()


        MySql = "IF NOT EXISTS(SELECT * FROM [AnagraficaComune] WHERE CODICEFISCALE = ? And TIPOLOGIA = 'O') BEGIN "
        MySql = MySql & " IF NOT EXISTS(SELECT * FROM [AnagraficaComune] WHERE CodiceOspite = ? And CodiceParente = 0) BEGIN "
        MySql = MySql & " INSERT INTO AnagraficaComune (CodiceOspite,CognomeOspite,NomeOspite,Nome,DataNascita,CODICEFISCALE,TIPOLOGIA,CodiceParente) Values (?,?,?,?,?,?,'O',0)"
        MySql = MySql & " END "
        MySql = MySql & " END"

        cmd.CommandText = MySql
        cmd.Parameters.AddWithValue("@CODICEFISCALE", CodiceFiscale)
        cmd.Parameters.AddWithValue("@CodiceOspite", CodiceOspite)
        cmd.Parameters.AddWithValue("@CodiceOspite", CodiceOspite)
        cmd.Parameters.AddWithValue("@Cognome", Cognome)
        cmd.Parameters.AddWithValue("@Nome", Nome)
        cmd.Parameters.AddWithValue("@CognomeNome", Cognome & " " & Nome)
        If Not IsDate(DataNascita) Then
            cmd.Parameters.AddWithValue("@DataNascita", Now)
        Else
            If Year(DataNascita) < 1900 Then
                cmd.Parameters.AddWithValue("@DataNascita", Now)
            Else
                cmd.Parameters.AddWithValue("@DataNascita", DataNascita)
            End If
        End If
        cmd.Parameters.AddWithValue("@CODICEFISCALE", CodiceFiscale)

        If cmd.ExecuteNonQuery() = -1 Then
            Dim cmdcf As New OleDbCommand()
            cmdcf.Connection = cn

            MySql = "Select CodiceOspite from AnagraficaComune Where CodiceFiscale = ? And TIPOLOGIA = 'O'"
            cmdcf.CommandText = MySql
            cmdcf.Parameters.AddWithValue("@CODICEFISCALE", CodiceFiscale)
            Dim ReadCF As OleDbDataReader = cmdcf.ExecuteReader()
            If ReadCF.Read() Then
                CodiceOspite = campodbn(ReadCF.Item(0))
            Else
                CodiceOspite = 0
            End If
            ReadCF.Close()
        End If
        Nome = Cognome & " " & Nome
        cn.Close()

        'Dim serializer As JavaScriptSerializer
        'serializer = New JavaScriptSerializer()

        'Dim Serializzazione As String

        'Serializzazione = serializer.Serialize(Me)
    End Sub

    Public Sub UpDateDropBox(ByVal StringaConnessione As String, ByRef appoggio As DropDownList)
        Dim cn As OleDbConnection



        cn = New Data.OleDb.OleDbConnection(StringaConnessione)

        cn.Open()
        Dim cmd As New OleDbCommand()

        cmd.CommandText = ("select * from AnagraficaComune Where  TIPOLOGIA = 'O' and  (AnagraficaComune.NonInUso  is null Or AnagraficaComune.NonInUso = '')  Order By Nome,NomeConiuge")
        cmd.Connection = cn
        Dim sb As StringBuilder = New StringBuilder

        appoggio.Items.Clear()
        appoggio.Items.Add("")
        appoggio.Items(appoggio.Items.Count - 1).Value = ""
        appoggio.Items(appoggio.Items.Count - 1).Selected = True
        Dim myPOSTreader As OleDbDataReader = cmd.ExecuteReader()
        Do While myPOSTreader.Read
            If IsNothing(myPOSTreader.Item("DataNascita")) Then
                appoggio.Items.Add(campodb(myPOSTreader.Item("Nome")))
            Else
                If Year(campodbd(myPOSTreader.Item("DataNascita"))) < 1900 Then
                    appoggio.Items.Add(campodb(myPOSTreader.Item("Nome")))
                Else
                    appoggio.Items.Add(campodb(myPOSTreader.Item("Nome")) & " " & Format(campodbd(myPOSTreader.Item("DataNascita")), "dd/MM/yyyy"))
                End If
            End If
            appoggio.Items(appoggio.Items.Count - 1).Value = campodbn(myPOSTreader.Item("CodiceOspite"))
        Loop
        myPOSTreader.Close()
        cn.Close()

    End Sub

    Sub InserisciOspite(ByVal StringaConnessione As String, ByVal Cognome As String, ByVal Nome As String, ByVal DataNascita As Date)
        Dim cn As OleDbConnection
        Dim MySql As String

        cn = New Data.OleDb.OleDbConnection(StringaConnessione)

        Call Pulisci()
        cn.Open()
        Dim cmd As New OleDbCommand()
        cmd.Connection = cn

        MySql = "Select max(CodiceOspite) from AnagraficaComune "
        cmd.CommandText = MySql
        Dim myPOSTreader As OleDbDataReader = cmd.ExecuteReader()
        If myPOSTreader.Read() Then
            CodiceOspite = campodbn(myPOSTreader.Item(0)) + 1
        End If
        myPOSTreader.Close()

        MySql = "INSERT INTO AnagraficaComune (CodiceOspite,CognomeOspite,NomeOspite,Nome,DataNascita,TIPOLOGIA,CodiceParente) Values (?,?,?,?,?,'O',0)"
        cmd.CommandText = MySql
        cmd.Parameters.AddWithValue("@CodiceOspite", CodiceOspite)
        cmd.Parameters.AddWithValue("@Cognome", Cognome)
        cmd.Parameters.AddWithValue("@Nome", Nome)
        cmd.Parameters.AddWithValue("@CognomeNome", Cognome & " " & Nome)
        If Not IsDate(DataNascita) Then
            cmd.Parameters.AddWithValue("@DataNascita", Now)
        Else
            If Year(DataNascita) < 1900 Then
                cmd.Parameters.AddWithValue("@DataNascita", Now)
            Else
                cmd.Parameters.AddWithValue("@DataNascita", DataNascita)
            End If
        End If

        cmd.ExecuteNonQuery()
        Nome = Cognome & " " & Nome
        cn.Close()

        'Dim serializer As JavaScriptSerializer
        'serializer = New JavaScriptSerializer()

        'Dim Serializzazione As String

        'Serializzazione = serializer.Serialize(Me)
    End Sub

    Public Function CentroServizio(ByVal StringaConnessione As String) As String
        Dim cn As OleDbConnection
        Dim AssegnaCServ As String


        cn = New Data.OleDb.OleDbConnection(StringaConnessione)
        AssegnaCServ = ""
        cn.Open()
        Dim cmd As New OleDbCommand()
        cmd.CommandText = ("Select CENTROSERVIZIO From Movimenti Where  TipoMov = '05' And CODICEOSPITE = " & CodiceOspite & " ORDER by DATA Desc ")
        cmd.Connection = cn
        Dim sb As StringBuilder = New StringBuilder

        Dim myPOSTreader As OleDbDataReader = cmd.ExecuteReader()
        If myPOSTreader.Read Then
            AssegnaCServ = campodb(myPOSTreader.Item("CentroServizio"))
        End If
        cn.Close()
        Return AssegnaCServ
    End Function

    Sub ScriviOspite(ByVal StringaConnessione As String)

        If CodiceOspite = 0 Then
            Exit Sub
        End If

        If Len(Nome) > 150 Then
            Nome = Mid(Nome, 1, 150)
        End If
        If Len(RESIDENZAINDIRIZZO1) > 150 Then
            RESIDENZAINDIRIZZO1 = Mid(RESIDENZAINDIRIZZO1, 1, 150)
        End If
        If Len(RecapitoIndirizzo) > 50 Then
            RecapitoIndirizzo = Mid(RecapitoIndirizzo, 1, 50)
        End If

        If Len(RecapitoNome) > 50 Then
            RecapitoNome = Mid(RecapitoNome, 1, 50)
        End If

        If Len(RESIDENZACAP1) > 50 Then
            RESIDENZACAP1 = Mid(RESIDENZACAP1, 1, 5)
        End If

        DATAAGGIORNAMENTO = Now

        Dim cn As OleDbConnection
        Dim MySql As String

        cn = New Data.OleDb.OleDbConnection(StringaConnessione)

        cn.Open()
        Dim cmd As New OleDbCommand()


        cmd.Connection = cn

        MySql = "UPDATE AnagraficaComune SET " & _
                " CodiceMedico = ?, " & _
                " RecapitoNome  = ?," & _
                " RecapitoIndirizzo = ?, " & _
                " RecapitoProvincia = ?, " & _
        " RecapitoComune  = ?, " & _
        " RESIDENZAINDIRIZZO1 = ?, " & _
        " RESIDENZAINDIRIZZO2 = ?, " & _
        " RESIDENZAINDIRIZZO3 = ?, " & _
        " RESIDENZAINDIRIZZO4 = ?, " & _
        " RESIDENZACOMUNE1 = ?, " & _
        " RESIDENZACOMUNE2 = ?, " & _
        " RESIDENZACOMUNE3 = ?, " & _
        " RESIDENZACOMUNE4 = ?, " & _
        " RESIDENZAPROVINCIA1 = ?, " & _
        " RESIDENZAPROVINCIA2 = ?, " & _
        " RESIDENZAPROVINCIA3 = ?, " & _
        " RESIDENZAPROVINCIA4 = ?, " & _
        " RESIDENZACAP1 = ?, " & _
        " RESIDENZACAP2 = ?, " & _
        " RESIDENZACAP3 = ?, " & _
        " RESIDENZACAP4 = ?, " & _
        " RESIDENZATELEFONO1 = ?, " & _
        " RESIDENZATELEFONO2 = ?, " & _
        " RESIDENZATELEFONO3 = ?, " & _
        " RESIDENZATELEFONO4 = ?, " & _
        " CODICEFISCALE = ?, " & _
        " CODICEIVA = ?, " & _
        " TIPOOPERAZIONE = ?, " & _
        " MODALITAPAGAMENTO = ?, " & _
        " MastroCliente = ?, " & _
        " ContoCliente = ?, " & _
        " SottoContoCliente = ?, " & _
        " Nome = ?, " & _
        " Sesso = ?, " & _
        " CodiceSanitario = ?, " & _
        " Compensazione = ?, " & _
        " ComuneDiNascita = ?, " & _
        " ProvinciaDiNascita = ?, " & _
        " DataDomanda = ?, " & _
        " DataNascita = ?, " & _
        " EsenzioneTicket = ?,  " & _
        " NOMECONIUGE  = ?, " & _
        " NOMEPADRE  = ?, " & _
        " NOMEMADRE  = ?,  " & _
        " SETTIMANA  = ?,  " & _
        " TELEFONO1  = ?,  " & _
        " TELEFONO2  = ?,  " & _
        " TELEFONO3  = ?,  " & _
        " TELEFONO4  = ?,  " & _
        " USL  = ?,  " & _
        " PERIODO  = ?,  " & _
        " TIPOLOGIA = ?,  " & _
        " UTENTE  = ?,  " & _
        " DATAAGGIORNAMENTO = ?,  " & _
        " FattAnticipata  = ?,  " & _
        " GradoParentela  = ?,  " & _
        " TipoOspite  = ?,  " & _
        " DataDeposito = ?,  " & _
        " ImportoDeposito = ?,  " & _
        " NonInUso  = ?,   " & _
        " CodiceTicket = ?, " & _
        " DataRilascio  = ?,  " & _
        " DataScadenza = ?,  " & _
        " CodiceTicket2 = ?,  " & _
        " DataRilascio2 = ?, " & _
        " DataScadenza2 = ?,  " & _
        " NoteTicket = ?,  " & _
        " PathImmagine  = ?, " & _
        " NumeroDocumento  = ?,  " & _
        " DataDocumento  = ?, " & _
        " EmettitoreDocumento  = ?,  " & _
        " Nazionalita  = ?,  " & _
        " MesiAnticipo = ?,  " & _
        " CodiceCIG  = ?,  " & _
        " EmOspiteParente  = ?,  " & _
        " DataAccreditoDeposito  = ?,  " & _
        " PROFCONIUGE = ?, " & _
        " DATAMORTE = ?,  " & _
        " LuogoMorte = ?,  " & _
        " ExtraFuoriRetta=?, " & _
        " [Note] = ?, " & _
        " RESIDENZADATA1 = ?, " & _
        " RESIDENZADATA2 = ?, " & _
        " RESIDENZADATA3 = ?, " & _
        " CodiceDestinatario = ?, " & _
        " CodificaProvincia = ?, " & _
        " LetteraProvincia = ?, " & _
        " ProvinciaRilascio = ?, " & _
        " TipoDocumento = ?, " & _
        " ComuneRilascio = ?, " & _
        " CodiceQuestura = ?, " & _
        " CognomeOspite = ?, " & _
        " NomeOspite = ?, " & _
        " TesseraSanitaria = ?, " & _
        " StatoCivile = ?, " & _
        " StrutturaProvenienza = ?, " & _
        " ScadenzaDocumento = ?,  " & _
        " AssistenteSociale = ?, " & _
        " CodiceLavanderia = ?," & _
        " Distretto = ?," & _
        " ParenteIndirizzo = ?, " & _
        " CABCLIENTE = ?, " & _
        " ABICLIENTE = ?, " & _
        " IntCliente = ?, " & _
        " NumeroControlloCliente = ?, " & _
        " CINCLIENTE = ?, " & _
        " CCBANCARIOCLIENTE = ?, " & _
        " BancaCliente = ?, " & _
        " IntestatarioCC = ?, " & _
        " CodiceFiscaleCC = ?, " & _
        " RotturaOspite = ?, " & _
        " Vincolo = ?, " & _
        " ImportoSconto  = ?, " & _
        " IdEpersonam = ?, " & _
        " TipoSconto = ?, " & _
        " CONTOPERESATTO = ?, " & _
        " Opposizione730 = ?, " & _
        " NomeMedico = ?, " & _
        " CONSENSOINSERIMENTO = ?, " & _
        " CONSENSOMARKETING = ?, " & _
        " CartellaClinica = ?, " & _
        " CodiceCup = ? " & _
        " Where CodiceOSpite = ? And CodiceParente = 0"




        '" Note = @XNote " & _ 
        'PERIODO
        '" NOTE  = @NOTE, " & _



        cmd.CommandText = MySql
        cmd.Parameters.AddWithValue("@CodiceMedico", CodiceMedico)
        cmd.Parameters.AddWithValue("@RecapitoNome ", RecapitoNome)
        cmd.Parameters.AddWithValue("@RecapitoIndirizzo", RecapitoIndirizzo)
        cmd.Parameters.AddWithValue("@RecapitoProvincia", RecapitoProvincia)
        cmd.Parameters.AddWithValue("@RecapitoComune ", RecapitoComune)
        cmd.Parameters.AddWithValue("@RESIDENZAINDIRIZZO1", RESIDENZAINDIRIZZO1)
        cmd.Parameters.AddWithValue("@RESIDENZAINDIRIZZO2", RESIDENZAINDIRIZZO2)
        cmd.Parameters.AddWithValue("@RESIDENZAINDIRIZZO3", RESIDENZAINDIRIZZO3)
        cmd.Parameters.AddWithValue("@RESIDENZAINDIRIZZO4", RESIDENZAINDIRIZZO4)
        cmd.Parameters.AddWithValue("@RESIDENZACOMUNE1", RESIDENZACOMUNE1)
        cmd.Parameters.AddWithValue("@RESIDENZACOMUNE2", RESIDENZACOMUNE2)
        cmd.Parameters.AddWithValue("@RESIDENZACOMUNE3", RESIDENZACOMUNE3)
        cmd.Parameters.AddWithValue("@RESIDENZACOMUNE4", RESIDENZACOMUNE4)
        cmd.Parameters.AddWithValue("@RESIDENZAPROVINCIA1", RESIDENZAPROVINCIA1)
        cmd.Parameters.AddWithValue("@RESIDENZAPROVINCIA2", RESIDENZAPROVINCIA2)
        cmd.Parameters.AddWithValue("@RESIDENZAPROVINCIA3", RESIDENZAPROVINCIA3)
        cmd.Parameters.AddWithValue("@RESIDENZAPROVINCIA4", RESIDENZAPROVINCIA4)
        cmd.Parameters.AddWithValue("@RESIDENZACAP1", RESIDENZACAP1)
        cmd.Parameters.AddWithValue("@RESIDENZACAP2", RESIDENZACAP2)
        cmd.Parameters.AddWithValue("@RESIDENZACAP3", RESIDENZACAP3)
        cmd.Parameters.AddWithValue("@RESIDENZACAP4", RESIDENZACAP4)
        cmd.Parameters.AddWithValue("@RESIDENZATELEFONO1", RESIDENZATELEFONO1)
        cmd.Parameters.AddWithValue("@RESIDENZATELEFONO2", RESIDENZATELEFONO2)
        cmd.Parameters.AddWithValue("@RESIDENZATELEFONO3", RESIDENZATELEFONO3)
        cmd.Parameters.AddWithValue("@RESIDENZATELEFONO4", RESIDENZATELEFONO4)
        cmd.Parameters.AddWithValue("@CODICEFISCALE", Trim(Mid(CODICEFISCALE & Space(18), 1, 16)))
        cmd.Parameters.AddWithValue("@CODICEIVA", CODICEIVA)
        cmd.Parameters.AddWithValue("@TIPOOPERAZIONE", TIPOOPERAZIONE)
        cmd.Parameters.AddWithValue("@MODALITAPAGAMENTO", MODALITAPAGAMENTO)
        cmd.Parameters.AddWithValue("@MastroCliente", MastroCliente)
        cmd.Parameters.AddWithValue("@ContoCliente", ContoCliente)
        cmd.Parameters.AddWithValue("@SottoContoCliente", SottoContoCliente)
        cmd.Parameters.AddWithValue("@Nome", Nome)
        cmd.Parameters.AddWithValue("@Sesso", Sesso)
        cmd.Parameters.AddWithValue("@CodiceSanitario", CodiceSanitario)

        cmd.Parameters.AddWithValue("@Compensazione", Compensazione)
        cmd.Parameters.AddWithValue("@ComuneDiNascita", ComuneDiNascita)
        cmd.Parameters.AddWithValue("@ProvinciaDiNascita", ProvinciaDiNascita)
        If Year(DataDomanda) > 1 Then
            cmd.Parameters.AddWithValue("@DataDomanda", DataDomanda)
        Else
            cmd.Parameters.AddWithValue("@DataDomanda", System.DBNull.Value)
        End If
        If Year(DataNascita) > 1 Then
            cmd.Parameters.AddWithValue("@DataNascita", DataNascita)
        Else
            cmd.Parameters.AddWithValue("@DataNascita", System.DBNull.Value)
        End If
        cmd.Parameters.AddWithValue("@EsenzioneTicket", EsenzioneTicket)
        cmd.Parameters.AddWithValue("@NOMECONIUGE", NOMECONIUGE)
        cmd.Parameters.AddWithValue("@NOMEPADRE", NOMEPADRE)
        cmd.Parameters.AddWithValue("@NOMEMADRE", NOMEMADRE)
        cmd.Parameters.AddWithValue("@SETTIMANA", SETTIMANA)
        cmd.Parameters.AddWithValue("@TELEFONO1", TELEFONO1)
        cmd.Parameters.AddWithValue("@TELEFONO2", TELEFONO2)
        cmd.Parameters.AddWithValue("@TELEFONO3", TELEFONO3)
        cmd.Parameters.AddWithValue("@TELEFONO4", TELEFONO4)
        cmd.Parameters.AddWithValue("@USL", USL)
        cmd.Parameters.AddWithValue("@PERIODO", PERIODO)

        cmd.Parameters.AddWithValue("@TIPOLOGIA", TIPOLOGIA)
        cmd.Parameters.AddWithValue("@UTENTE", UTENTE)
        REM If Year(DATAAGGIORNAMENTO) > 1 Then
        cmd.Parameters.AddWithValue("@DATAAGGIORNAMENTO", DATAAGGIORNAMENTO)
        REM Else
        REM cmd.Parameters.AddWithValue("@DATAAGGIORNAMENTO", System.DBNull.Value)
        REM End If
        cmd.Parameters.AddWithValue("@FattAnticipata", FattAnticipata)
        cmd.Parameters.AddWithValue("@GradoParentela", GradoParentela)
        cmd.Parameters.AddWithValue("@TipoOspite", TipoOspite)

        If Year(DataDeposito) > 1 Then
            cmd.Parameters.AddWithValue("@DataDeposito", DataDeposito)
        Else
            cmd.Parameters.AddWithValue("@DataDeposito", System.DBNull.Value)
        End If
        cmd.Parameters.AddWithValue("@ImportoDeposito", ImportoDeposito)
        cmd.Parameters.AddWithValue("@NonInUso", NonInUso)
        cmd.Parameters.AddWithValue("@CodiceTicket", CodiceTicket)
        If Year(DataRilascio) > 1 Then
            cmd.Parameters.AddWithValue("@DataRilascio", DataRilascio)
        Else
            cmd.Parameters.AddWithValue("@DataRilascio", System.DBNull.Value)
        End If

        If Year(DataScadenza) > 1 Then
            cmd.Parameters.AddWithValue("@DataScadenza1", DataScadenza)
        Else
            cmd.Parameters.AddWithValue("@DataScadenza1", System.DBNull.Value)
        End If


        cmd.Parameters.AddWithValue("@CodiceTicket2", CodiceTicket2)

        If Year(DataRilascio2) > 1 Then
            cmd.Parameters.AddWithValue("@DataRilascio2", DataRilascio2)
        Else
            cmd.Parameters.AddWithValue("@DataRilascio2", System.DBNull.Value)
        End If

        If Year(DataScadenza2) > 1 Then
            cmd.Parameters.AddWithValue("@DataScadenza2", DataScadenza2)
        Else
            cmd.Parameters.AddWithValue("@DataScadenza2", System.DBNull.Value)
        End If

        cmd.Parameters.AddWithValue("@NoteTicket", NoteTicket)
        cmd.Parameters.AddWithValue("@PathImmagine", PathImmagine)
        cmd.Parameters.AddWithValue("@NumeroDocumento", NumeroDocumento)

        If Year(DataDocumento) > 1 Then
            cmd.Parameters.AddWithValue("@DataDocumento", DataDocumento)
        Else
            cmd.Parameters.AddWithValue("@DataDocumento", System.DBNull.Value)
        End If


        cmd.Parameters.AddWithValue("@EmettitoreDocumento", EmettitoreDocumento)
        cmd.Parameters.AddWithValue("@Nazionalita", Nazionalita)
        cmd.Parameters.AddWithValue("@MesiAnticipo", MesiAnticipo)
        cmd.Parameters.AddWithValue("@CodiceCIG", CodiceCIG)
        cmd.Parameters.AddWithValue("@EmOspiteParente", Val(EmOspiteParente))

        If Year(DataAccreditoDeposito) > 1 Then
            cmd.Parameters.AddWithValue("@DataAccreditoDeposito", DataAccreditoDeposito)
        Else
            cmd.Parameters.AddWithValue("@DataAccreditoDeposito", System.DBNull.Value)
        End If

        cmd.Parameters.AddWithValue("@PROFCONIUGE", PROFCONIUGE)

        If Year(DATAMORTE) > 1 Then
            cmd.Parameters.AddWithValue("@DATAMORTE", DATAMORTE)
        Else
            cmd.Parameters.AddWithValue("@DATAMORTE", System.DBNull.Value)
        End If

        cmd.Parameters.AddWithValue("@LuogoMorte", LuogoMorte)
        cmd.Parameters.AddWithValue("@ExtraFuoriRetta", ExtraFuoriRetta)
        cmd.Parameters.AddWithValue("@XNOTE", NOTE)


        If Year(RESIDENZADATA1) > 1 Then
            cmd.Parameters.AddWithValue("@RESIDENZADATA1", RESIDENZADATA1)
        Else
            cmd.Parameters.AddWithValue("@RESIDENZADATA1", System.DBNull.Value)
        End If

        If Year(RESIDENZADATA2) > 1 Then
            cmd.Parameters.AddWithValue("@RESIDENZADATA2", RESIDENZADATA2)
        Else
            cmd.Parameters.AddWithValue("@RESIDENZADATA2", System.DBNull.Value)
        End If

        If Year(RESIDENZADATA3) > 1 Then
            cmd.Parameters.AddWithValue("@RESIDENZADATA3", RESIDENZADATA3)
        Else
            cmd.Parameters.AddWithValue("@RESIDENZADATA3", System.DBNull.Value)
        End If

        cmd.Parameters.AddWithValue("@CodiceDestinatario", CodiceDestinatario)
        cmd.Parameters.AddWithValue("@CodificaProvincia", CodificaProvincia)
        cmd.Parameters.AddWithValue("@LetteraProvincia", LetteraProvincia)
        cmd.Parameters.AddWithValue("@ProvinciaRilascio", ProvinciaRilascio)
        cmd.Parameters.AddWithValue("@TipoDocumento", TipoDocumento)
        cmd.Parameters.AddWithValue("@ComuneRilascio", ComuneRilascio)
        cmd.Parameters.AddWithValue("@CodiceQuestura", CodiceQuestura)
        cmd.Parameters.AddWithValue("@CognomeOspite", CognomeOspite)
        cmd.Parameters.AddWithValue("@NomeOspite", NomeOspite)
        cmd.Parameters.AddWithValue("@TesseraSanitaria", TesseraSanitaria)
        cmd.Parameters.AddWithValue("@StatoCivile", StatoCivile)
        cmd.Parameters.AddWithValue("@StrutturaProvenienza", StrutturaProvenienza)

        If Year(ScadenzaDocumento) > 1 Then
            cmd.Parameters.AddWithValue("@ScadenzaDocumento", ScadenzaDocumento)
        Else
            cmd.Parameters.AddWithValue("@ScadenzaDocumento", System.DBNull.Value)
        End If



        cmd.Parameters.AddWithValue("@AssistenteSociale", AssistenteSociale)
        cmd.Parameters.AddWithValue("@CodiceLavanderia", CodiceLavanderia)
        cmd.Parameters.AddWithValue("@Distretto", Distretto)
        cmd.Parameters.AddWithValue("@ParenteIndirizzo", ParenteIndirizzo)

        cmd.Parameters.AddWithValue("@CABCLIENTE", CABCLIENTE)
        cmd.Parameters.AddWithValue("@ABICLIENTE", ABICLIENTE)
        cmd.Parameters.AddWithValue("@IntCliente", IntCliente)
        cmd.Parameters.AddWithValue("@NumeroControlloCliente", NumeroControlloCliente)
        cmd.Parameters.AddWithValue("@CINCLIENTE", CINCLIENTE)
        cmd.Parameters.AddWithValue("@CCBANCARIOCLIENTE", CCBANCARIOCLIENTE)
        cmd.Parameters.AddWithValue("@BancaCliente", BancaCliente)
        cmd.Parameters.AddWithValue("@IntestatarioCC", IntestatarioCC)
        cmd.Parameters.AddWithValue("@CodiceFiscaleCC", CodiceFiscaleCC)
        cmd.Parameters.AddWithValue("@RotturaOspite", RotturaOspite)
        cmd.Parameters.AddWithValue("@Vincolo", Vincolo)
        cmd.Parameters.AddWithValue("@ImportoSconto", ImportoSconto)
        cmd.Parameters.AddWithValue("@IdEpersonam", IdEpersonam)
        cmd.Parameters.AddWithValue("@TipoSconto", TipoSconto)
        cmd.Parameters.AddWithValue("@CONTOPERESATTO", CONTOPERESATTO)
        cmd.Parameters.AddWithValue("@Opposizione730", Opposizione730)
        cmd.Parameters.AddWithValue("@NomeMedico", NomeMedico)
        cmd.Parameters.AddWithValue("@CONSENSOINSERIMENTO", CONSENSOINSERIMENTO)
        cmd.Parameters.AddWithValue("@CONSENSOMARKETING", CONSENSOMARKETING)
        cmd.Parameters.AddWithValue("@CartellaClinica", CartellaClinica)
        cmd.Parameters.AddWithValue("@CodiceCup", CodiceCup)

        cmd.Parameters.AddWithValue("@CodiceOSpite", CodiceOspite)

        cmd.ExecuteNonQuery()

        cn.Close()
    End Sub
    Sub loaddati(ByVal StringaConnessione As String, ByVal codiceospite As Integer, ByVal Nome As String, ByVal Tabella As System.Data.DataTable, Optional ByVal PrimiDieci As Boolean = False)
        Dim cn As OleDbConnection
        Dim Riga As Long = 0

        cn = New Data.OleDb.OleDbConnection(StringaConnessione)

        cn.Open()
        Dim cmd As New OleDbCommand()

        If PrimiDieci = False Then
            If codiceospite > 0 Then
                cmd.CommandText = ("select AnagraficaComune.CodiceOspite as CodiceOSpite,AnagraficaComune.DataNascita, MOVIMENTI.Data, MOVIMENTI.CentroServizio as CentroServizio, Nome from AnagraficaComune INNER JOIN MOVIMENTI ON AnagraficaComune.CODICEOSPITE = MOVIMENTI.CODICEOSPITE where TIPOLOGIA = 'O' And AnagraficaComune.CodiceOspite = " & codiceospite & " ORDER BY NOME,Data ,CENTROSERVIZIO")
            Else
                cmd.CommandText = ("select AnagraficaComune.CodiceOspite as CodiceOSpite,AnagraficaComune.DataNascita, MOVIMENTI.Data, MOVIMENTI.CentroServizio as CentroServizio, Nome  from AnagraficaComune INNER JOIN MOVIMENTI ON AnagraficaComune.CODICEOSPITE = MOVIMENTI.CODICEOSPITE where TIPOLOGIA = 'O' And AnagraficaComune.CodiceOspite > 0 And Nome Like '" & Nome & "' ORDER BY NOME,Data ,CENTROSERVIZIO")
            End If
        Else
            cmd.CommandText = ("select AnagraficaComune.CodiceOspite as CodiceOSpite,AnagraficaComune.DataNascita, MOVIMENTI.Data, MOVIMENTI.CentroServizio as CentroServizio, Nome  from AnagraficaComune INNER JOIN MOVIMENTI ON AnagraficaComune.CODICEOSPITE = MOVIMENTI.CODICEOSPITE where TIPOLOGIA = 'O'  ORDER BY CodiceOspite Desc,CENTROSERVIZIO")
        End If

        cmd.Connection = cn
        Dim sb As StringBuilder = New StringBuilder


        Tabella.Clear()
        Tabella.Columns.Add("Centro Servizio", GetType(String))
        Tabella.Columns.Add("Codice Ospite", GetType(String))
        Tabella.Columns.Add("Nome", GetType(String))
        Tabella.Columns.Add("Data Nascita", GetType(String))
        Tabella.Columns.Add("Data Accoglimento", GetType(String))
        Tabella.Columns.Add("Data Uscita Definitiva", GetType(String))

        Dim oldCodiceServizio As String
        Dim OldCodiceOspite As Long
        oldCodiceServizio = ""
        OldCodiceOspite = 0

        Dim myPOSTreader As OleDbDataReader = cmd.ExecuteReader()
        Do While myPOSTreader.Read
            If oldCodiceServizio <> myPOSTreader.Item("CentroServizio") Or _
               OldCodiceOspite <> myPOSTreader.Item("CodiceOSpite") Then

                Dim myriga As System.Data.DataRow = Tabella.NewRow()
                myriga(0) = myPOSTreader.Item("CentroServizio")
                myriga(1) = myPOSTreader.Item("CodiceOspite")
                myriga(2) = myPOSTreader.Item("Nome")
                If Not IsDBNull(myPOSTreader.Item("DataNascita")) Then
                    myriga(3) = Format(myPOSTreader.Item("DataNascita"), "dd/MM/yyyy")
                End If
                Dim XS As New Cls_Movimenti

                XS.CENTROSERVIZIO = myPOSTreader.Item("CentroServizio")
                XS.CodiceOspite = myPOSTreader.Item("CodiceOspite")
                XS.UltimaDataAccoglimento(StringaConnessione)

                If Not IsDBNull(XS.Data) Then
                    myriga(4) = Format(XS.Data, "dd/MM/yyyy")
                End If

                XS.Data = Nothing
                XS.CENTROSERVIZIO = myPOSTreader.Item("CentroServizio")
                XS.CodiceOspite = myPOSTreader.Item("CodiceOspite")

                XS.UltimaDataUscitaDefinitiva(StringaConnessione)

                If Not IsDBNull(XS.Data) Then
                    If Year(XS.Data) > 1900 Then
                        myriga(5) = Format(XS.Data, "dd/MM/yyyy")
                    End If
                End If
                Tabella.Rows.Add(myriga)
                Riga = Riga + 1

                If Riga = 20 Then
                    Exit Do
                End If
            End If
            oldCodiceServizio = myPOSTreader.Item("CentroServizio")
            OldCodiceOspite = myPOSTreader.Item("CodiceOspite")


        Loop
        myPOSTreader.Close()
        cn.Close()
    End Sub
    Function campodbd(ByVal oggetto As Object) As Date
        If IsDBNull(oggetto) Then
            Return Nothing
        Else
            Return oggetto
        End If
    End Function
    Function campodb(ByVal oggetto As Object) As String
        If IsDBNull(oggetto) Then
            Return ""
        Else
            Return oggetto
        End If
    End Function
    Function campodbn(ByVal oggetto As Object) As Double
        If IsDBNull(oggetto) Then
            Return 0
        Else
            Return oggetto
        End If
    End Function
    Sub Elimina(ByVal StringaConnessione As String, ByVal xCodiceOspite As Long)
        Dim cn As OleDbConnection



        cn = New Data.OleDb.OleDbConnection(StringaConnessione)

        cn.Open()
        Dim cmd As New OleDbCommand()
        cmd.CommandText = ("Delete from AnagraficaComune where " & _
                           "CodiceOspite = ? And TIPOLOGIA = 'O'")
        cmd.Parameters.AddWithValue("@CodiceOspite", xCodiceOspite)
        cmd.Connection = cn
        cmd.ExecuteNonQuery()

        Dim cmd1 As New OleDbCommand()
        cmd1.CommandText = ("Delete from IMPORTOOSPITE where " & _
                                   "CodiceOspite = ? ")
        cmd1.Parameters.AddWithValue("@CodiceOspite", xCodiceOspite)
        cmd1.Connection = cn
        cmd1.ExecuteNonQuery()

        Dim cmd2 As New OleDbCommand()
        cmd2.CommandText = ("Delete from IMPORTOCOMUNE where " & _
                                   "CodiceOspite = ? ")
        cmd2.Parameters.AddWithValue("@CodiceOspite", xCodiceOspite)
        cmd2.Connection = cn
        cmd2.ExecuteNonQuery()

        Dim cmd3 As New OleDbCommand()
        cmd3.CommandText = ("Delete from IMPORTOJOLLY where " & _
                           "CodiceOspite = ? ")
        cmd3.Parameters.AddWithValue("@CodiceOspite", xCodiceOspite)
        cmd3.Connection = cn
        cmd3.ExecuteNonQuery()

        Dim cmd4 As New OleDbCommand()
        cmd4.CommandText = ("Delete from IMPORTOPARENTI where " & _
                   "CodiceOspite = ? ")
        cmd4.Parameters.AddWithValue("@CodiceOspite", xCodiceOspite)
        cmd4.Connection = cn
        cmd4.ExecuteNonQuery()




        Dim cmd6 As New OleDbCommand()
        cmd6.CommandText = ("Delete from IMPORTORETTA where " & _
                "CodiceOspite = ? ")
        cmd6.Parameters.AddWithValue("@CodiceOspite", xCodiceOspite)
        cmd6.Connection = cn
        cmd6.ExecuteNonQuery()

        Dim cmd7 As New OleDbCommand()
        cmd7.CommandText = ("Delete from Movimenti where " & _
                "CodiceOspite = ? ")
        cmd7.Parameters.AddWithValue("@CodiceOspite", xCodiceOspite)
        cmd7.Connection = cn
        cmd7.ExecuteNonQuery()


        Dim cmd8 As New OleDbCommand()
        cmd8.CommandText = ("Delete from DATIISE where " & _
                "CodiceOspite = ? ")
        cmd8.Parameters.AddWithValue("@CodiceOspite", xCodiceOspite)
        cmd8.Connection = cn
        cmd8.ExecuteNonQuery()

        Dim cmd9 As New OleDbCommand()
        cmd9.CommandText = ("Delete from Listino where " & _
                "CodiceOspite = ? ")
        cmd9.Parameters.AddWithValue("@CodiceOspite", xCodiceOspite)
        cmd9.Connection = cn
        cmd9.ExecuteNonQuery()
        cn.Close()
    End Sub


    Sub LeggiPerIdEpersonam(ByVal StringaConnessione As String)
        Dim cn As OleDbConnection



        cn = New Data.OleDb.OleDbConnection(StringaConnessione)

        cn.Open()
        Dim cmd As New OleDbCommand()
        cmd.CommandText = ("select * from AnagraficaComune where " & _
                           "IdEpersonam= ? And TIPOLOGIA = 'O'")
        cmd.Parameters.AddWithValue("@IdEpersonam", IdEpersonam)
        cmd.Connection = cn
        Dim sb As StringBuilder = New StringBuilder

        Dim myPOSTreader As OleDbDataReader = cmd.ExecuteReader()
        If myPOSTreader.Read Then
            CodiceOspite = Val(campodb(myPOSTreader.Item("CodiceOspite")))
            Leggi(StringaConnessione, CodiceOspite)
        End If
        myPOSTreader.Close()
    End Sub

    Sub LeggiPerCodiceFiscale(ByVal StringaConnessione As String, ByVal CodiceFiscale As String)
        Dim cn As OleDbConnection



        cn = New Data.OleDb.OleDbConnection(StringaConnessione)

        cn.Open()
        Dim cmd As New OleDbCommand()
        cmd.CommandText = ("select * from AnagraficaComune where " & _
                           "CODICEFISCALE = ? And TIPOLOGIA = 'O'")
        cmd.Parameters.AddWithValue("@CODICEFISCALE", CodiceFiscale)
        cmd.Connection = cn
        Dim sb As StringBuilder = New StringBuilder

        Dim myPOSTreader As OleDbDataReader = cmd.ExecuteReader()
        If myPOSTreader.Read Then
            CodiceOspite = Val(campodb(myPOSTreader.Item("CodiceOspite")))
            Leggi(StringaConnessione, CodiceOspite)
        End If
        myPOSTreader.Close()
    End Sub

    Sub Leggi(ByVal StringaConnessione As String, ByVal xCodiceOspite As Long)
        Dim cn As OleDbConnection



        cn = New Data.OleDb.OleDbConnection(StringaConnessione)

        cn.Open()
        Dim cmd As New OleDbCommand()
        cmd.CommandText = ("select * from AnagraficaComune where " & _
                           "CodiceOspite = ? And TIPOLOGIA = 'O'")
        cmd.Parameters.AddWithValue("@CodiceOspite", xCodiceOspite)
        cmd.Connection = cn
        Dim sb As StringBuilder = New StringBuilder

        Dim myPOSTreader As OleDbDataReader = cmd.ExecuteReader()
        If myPOSTreader.Read Then
            CodiceOspite = xCodiceOspite

            CodiceMedico = campodb(myPOSTreader.Item("CodiceMedico"))
            RecapitoNome = campodb(myPOSTreader.Item("RecapitoNome"))

            RecapitoIndirizzo = campodb(myPOSTreader.Item("RecapitoIndirizzo"))
            RecapitoProvincia = campodb(myPOSTreader.Item("RecapitoProvincia"))
            RecapitoComune = campodb(myPOSTreader.Item("RecapitoComune"))
            RESIDENZAINDIRIZZO1 = campodb(myPOSTreader.Item("RESIDENZAINDIRIZZO1"))
            RESIDENZAINDIRIZZO2 = campodb(myPOSTreader.Item("RESIDENZAINDIRIZZO2"))
            RESIDENZAINDIRIZZO3 = campodb(myPOSTreader.Item("RESIDENZAINDIRIZZO3"))
            RESIDENZAINDIRIZZO4 = campodb(myPOSTreader.Item("RESIDENZAINDIRIZZO4"))

            RESIDENZACOMUNE1 = campodb(myPOSTreader.Item("RESIDENZACOMUNE1"))
            RESIDENZACOMUNE2 = campodb(myPOSTreader.Item("RESIDENZACOMUNE2"))
            RESIDENZACOMUNE3 = campodb(myPOSTreader.Item("RESIDENZACOMUNE3"))
            RESIDENZACOMUNE4 = campodb(myPOSTreader.Item("RESIDENZACOMUNE4"))

            RESIDENZAPROVINCIA1 = campodb(myPOSTreader.Item("RESIDENZAPROVINCIA1"))
            RESIDENZAPROVINCIA2 = campodb(myPOSTreader.Item("RESIDENZAPROVINCIA2"))
            RESIDENZAPROVINCIA3 = campodb(myPOSTreader.Item("RESIDENZAPROVINCIA3"))
            RESIDENZAPROVINCIA4 = campodb(myPOSTreader.Item("RESIDENZAPROVINCIA4"))

            RESIDENZACAP1 = campodb(myPOSTreader.Item("RESIDENZACAP1"))
            RESIDENZACAP2 = campodb(myPOSTreader.Item("RESIDENZACAP2"))
            RESIDENZACAP3 = campodb(myPOSTreader.Item("RESIDENZACAP3"))
            RESIDENZACAP4 = campodb(myPOSTreader.Item("RESIDENZACAP4"))

            RESIDENZATELEFONO1 = campodb(myPOSTreader.Item("RESIDENZATELEFONO1"))
            RESIDENZATELEFONO2 = campodb(myPOSTreader.Item("RESIDENZATELEFONO2"))
            RESIDENZATELEFONO3 = campodb(myPOSTreader.Item("RESIDENZATELEFONO3"))
            RESIDENZATELEFONO4 = campodb(myPOSTreader.Item("RESIDENZATELEFONO4"))
            CODICEFISCALE = campodb(myPOSTreader.Item("CODICEFISCALE"))

            CODICEIVA = campodb(myPOSTreader.Item("CODICEIVA"))
            TIPOOPERAZIONE = campodb(myPOSTreader.Item("TIPOOPERAZIONE"))
            MODALITAPAGAMENTO = campodb(myPOSTreader.Item("MODALITAPAGAMENTO"))

            MastroCliente = Val(campodb(myPOSTreader.Item("MastroCliente")))
            ContoCliente = Val(campodb(myPOSTreader.Item("ContoCliente")))
            SottoContoCliente = Val(campodb(myPOSTreader.Item("SottoContoCliente")))

            Nome = campodb(myPOSTreader.Item("Nome"))
            Sesso = campodb(myPOSTreader.Item("Sesso"))




            CodiceSanitario = campodb(myPOSTreader.Item("CodiceSanitario"))

            Compensazione = campodb(myPOSTreader.Item("Compensazione"))
            ComuneDiNascita = campodb(myPOSTreader.Item("ComuneDiNascita"))
            ProvinciaDiNascita = campodb(myPOSTreader.Item("ProvinciaDiNascita"))

            If campodb(myPOSTreader.Item("DataDomanda")) <> "" Then
                DataDomanda = campodb(myPOSTreader.Item("DataDomanda"))
            Else
                DataDomanda = Nothing
            End If

            If campodb(myPOSTreader.Item("DataNascita")) <> "" Then
                DataNascita = campodb(myPOSTreader.Item("DataNascita"))
            Else
                DataNascita = Nothing
            End If

            EsenzioneTicket = campodb(myPOSTreader.Item("EsenzioneTicket"))
            NOMECONIUGE = campodb(myPOSTreader.Item("NOMECONIUGE"))
            NOMEPADRE = campodb(myPOSTreader.Item("NOMEPADRE"))
            NOMEMADRE = campodb(myPOSTreader.Item("NOMEMADRE"))
            SETTIMANA = campodb(myPOSTreader.Item("SETTIMANA"))
            TELEFONO1 = campodb(myPOSTreader.Item("TELEFONO1"))
            TELEFONO2 = campodb(myPOSTreader.Item("TELEFONO2"))
            TELEFONO3 = campodb(myPOSTreader.Item("TELEFONO3"))
            TELEFONO4 = campodb(myPOSTreader.Item("TELEFONO4"))
            USL = campodb(myPOSTreader.Item("USL"))

            PERIODO = campodb(myPOSTreader.Item("PERIODO"))
            NOTE = campodb(myPOSTreader.Item("NOTE"))
            TIPOLOGIA = campodb(myPOSTreader.Item("TIPOLOGIA"))
            UTENTE = campodb(myPOSTreader.Item("UTENTE"))
            If campodb(myPOSTreader.Item("DATAAGGIORNAMENTO")) <> "" Then
                DATAAGGIORNAMENTO = myPOSTreader.Item("DATAAGGIORNAMENTO")
            Else
                DATAAGGIORNAMENTO = Nothing
            End If
            FattAnticipata = campodb(myPOSTreader.Item("FattAnticipata"))
            GradoParentela = campodb(myPOSTreader.Item("GradoParentela"))

            TipoOspite = campodb(myPOSTreader.Item("TipoOspite"))
            If campodb(myPOSTreader.Item("DataDeposito")) = "" Then
                DataDeposito = Nothing
            Else
                DataDeposito = campodb(myPOSTreader.Item("DataDeposito"))
            End If
            If campodb(myPOSTreader.Item("ImportoDeposito")) = "" Then
                ImportoDeposito = 0
            Else
                ImportoDeposito = campodb(myPOSTreader.Item("ImportoDeposito"))
            End If
            NonInUso = campodb(myPOSTreader.Item("NonInUso"))

            CodiceTicket = campodb(myPOSTreader.Item("CodiceTicket"))

            If campodb(myPOSTreader.Item("DataRilascio")) <> "" Then
                DataRilascio = campodb(myPOSTreader.Item("DataRilascio"))
            Else
                DataRilascio = Nothing
            End If
            If campodb(myPOSTreader.Item("DataScadenza")) <> "" Then
                DataScadenza = campodb(myPOSTreader.Item("DataScadenza"))
            Else
                DataScadenza = Nothing
            End If
            CodiceTicket2 = campodb(myPOSTreader.Item("CodiceTicket2"))
            If campodb(myPOSTreader.Item("DataRilascio2")) <> "" Then
                DataRilascio2 = campodb(myPOSTreader.Item("DataRilascio2"))
            Else
                DataRilascio2 = Nothing
            End If
            If campodb(myPOSTreader.Item("DataScadenza2")) <> "" Then
                DataScadenza2 = campodb(myPOSTreader.Item("DataScadenza2"))
            Else
                DataScadenza2 = Nothing
            End If

            NoteTicket = campodb(myPOSTreader.Item("NoteTicket"))
            PathImmagine = campodb(myPOSTreader.Item("PathImmagine"))
            NumeroDocumento = campodb(myPOSTreader.Item("NumeroDocumento"))
            If campodb(myPOSTreader.Item("DataDocumento")) <> "" Then
                DataDocumento = campodb(myPOSTreader.Item("DataDocumento"))
            Else
                DataDocumento = Nothing
            End If

            EmettitoreDocumento = campodb(myPOSTreader.Item("EmettitoreDocumento"))
            Nazionalita = campodb(myPOSTreader.Item("Nazionalita"))

            If campodb(myPOSTreader.Item("MesiAnticipo")) = "" Then
                MesiAnticipo = 0
            Else
                MesiAnticipo = campodb(myPOSTreader.Item("MesiAnticipo"))
            End If
            CodiceCIG = campodb(myPOSTreader.Item("CodiceCIG"))
            EmOspiteParente = Val(campodb(myPOSTreader.Item("EmOspiteParente")))
            If campodb(myPOSTreader.Item("DataAccreditoDeposito")) <> "" Then
                DataAccreditoDeposito = campodb(myPOSTreader.Item("DataAccreditoDeposito"))
            Else
                DataAccreditoDeposito = Nothing
            End If

            PROFCONIUGE = campodb(myPOSTreader.Item("PROFCONIUGE"))



            If campodb(myPOSTreader.Item("DATAMORTE")) <> "" Then
                DATAMORTE = campodb(myPOSTreader.Item("DATAMORTE"))
            Else
                DATAMORTE = Nothing
            End If

            LuogoMorte = campodb(myPOSTreader.Item("LuogoMorte"))
            ExtraFuoriRetta = Val(campodb(myPOSTreader.Item("ExtraFuoriRetta")))

            RESIDENZADATA1 = campodbd(myPOSTreader.Item("RESIDENZADATA1"))
            RESIDENZADATA2 = campodbd(myPOSTreader.Item("RESIDENZADATA2"))
            RESIDENZADATA3 = campodbd(myPOSTreader.Item("RESIDENZADATA3"))

            CodiceDestinatario = campodb(myPOSTreader.Item("CodiceDestinatario"))
            CodificaProvincia = campodb(myPOSTreader.Item("CodificaProvincia"))
            LetteraProvincia = campodb(myPOSTreader.Item("LetteraProvincia"))
            ProvinciaRilascio = campodb(myPOSTreader.Item("ProvinciaRilascio"))
            TipoDocumento = campodb(myPOSTreader.Item("TipoDocumento"))
            ComuneRilascio = campodb(myPOSTreader.Item("ComuneRilascio"))
            CodiceQuestura = campodb(myPOSTreader.Item("CodiceQuestura"))
            CognomeOspite = campodb(myPOSTreader.Item("CognomeOspite"))
            NomeOspite = campodb(myPOSTreader.Item("NomeOspite"))
            TesseraSanitaria = campodb(myPOSTreader.Item("TesseraSanitaria"))
            StatoCivile = campodb(myPOSTreader.Item("StatoCivile"))
            StrutturaProvenienza = campodb(myPOSTreader.Item("StrutturaProvenienza"))
            ScadenzaDocumento = campodbd(myPOSTreader.Item("ScadenzaDocumento"))

            AssistenteSociale = campodb(myPOSTreader.Item("AssistenteSociale"))
            CodiceLavanderia = campodb(myPOSTreader.Item("CodiceLavanderia"))
            Distretto = campodb(myPOSTreader.Item("Distretto"))
            ParenteIndirizzo = Val(campodb(myPOSTreader.Item("ParenteIndirizzo")))


            CABCLIENTE = campodb(myPOSTreader.Item("CABCLIENTE"))
            ABICLIENTE = campodb(myPOSTreader.Item("ABICLIENTE"))
            IntCliente = campodb(myPOSTreader.Item("IntCliente"))
            NumeroControlloCliente = campodb(myPOSTreader.Item("NumeroControlloCliente"))
            CINCLIENTE = campodb(myPOSTreader.Item("CINCLIENTE"))
            CCBANCARIOCLIENTE = campodb(myPOSTreader.Item("CCBANCARIOCLIENTE"))
            BancaCliente = campodb(myPOSTreader.Item("BancaCliente"))
            IntestatarioCC = campodb(myPOSTreader.Item("IntestatarioCC"))
            CodiceFiscaleCC = campodb(myPOSTreader.Item("CodiceFiscaleCC"))
            RotturaOspite = Val(campodb(myPOSTreader.Item("RotturaOspite")))
            Vincolo = campodb(myPOSTreader.Item("Vincolo"))
            ImportoSconto = campodbn(myPOSTreader.Item("ImportoSconto"))

            IdEpersonam = Val(campodb(myPOSTreader.Item("IdEpersonam")))

            TipoSconto = campodb(myPOSTreader.Item("TipoSconto"))
            CONTOPERESATTO = campodb(myPOSTreader.Item("CONTOPERESATTO"))

            Opposizione730 = campodbn(myPOSTreader.Item("Opposizione730"))

            NomeMedico = campodb(myPOSTreader.Item("NomeMedico"))

            CONSENSOINSERIMENTO = campodbn(myPOSTreader.Item("CONSENSOINSERIMENTO"))

            CONSENSOMARKETING = campodbn(myPOSTreader.Item("CONSENSOMARKETING"))


            CartellaClinica = campodb(myPOSTreader.Item("CartellaClinica"))
            CodiceCup = campodb(myPOSTreader.Item("CodiceCup"))
        End If
        cn.Close()
    End Sub

    Sub LeggiNome(ByVal StringaConnessione As String)
        Dim cn As OleDbConnection



        cn = New Data.OleDb.OleDbConnection(StringaConnessione)

        cn.Open()
        Dim cmd As New OleDbCommand()
        cmd.CommandText = ("select * from AnagraficaComune where " & _
                           "Nome like ? And TIPOLOGIA = 'O'")
        cmd.Parameters.AddWithValue("@Nome", Nome)
        cmd.Connection = cn
        Dim sb As StringBuilder = New StringBuilder

        Dim myPOSTreader As OleDbDataReader = cmd.ExecuteReader()
        If myPOSTreader.Read Then
            CodiceOspite = Val(campodb(myPOSTreader.Item("CodiceOspite")))

            CodiceMedico = campodb(myPOSTreader.Item("CodiceMedico"))
            RecapitoNome = campodb(myPOSTreader.Item("RecapitoNome"))

            RecapitoIndirizzo = campodb(myPOSTreader.Item("RecapitoIndirizzo"))
            RecapitoProvincia = campodb(myPOSTreader.Item("RecapitoProvincia"))
            RecapitoComune = campodb(myPOSTreader.Item("RecapitoComune"))
            RESIDENZAINDIRIZZO1 = campodb(myPOSTreader.Item("RESIDENZAINDIRIZZO1"))
            RESIDENZAINDIRIZZO2 = campodb(myPOSTreader.Item("RESIDENZAINDIRIZZO2"))
            RESIDENZAINDIRIZZO3 = campodb(myPOSTreader.Item("RESIDENZAINDIRIZZO3"))
            RESIDENZAINDIRIZZO4 = campodb(myPOSTreader.Item("RESIDENZAINDIRIZZO4"))

            RESIDENZACOMUNE1 = campodb(myPOSTreader.Item("RESIDENZACOMUNE1"))
            RESIDENZACOMUNE2 = campodb(myPOSTreader.Item("RESIDENZACOMUNE2"))
            RESIDENZACOMUNE3 = campodb(myPOSTreader.Item("RESIDENZACOMUNE3"))
            RESIDENZACOMUNE4 = campodb(myPOSTreader.Item("RESIDENZACOMUNE4"))

            RESIDENZAPROVINCIA1 = campodb(myPOSTreader.Item("RESIDENZAPROVINCIA1"))
            RESIDENZAPROVINCIA2 = campodb(myPOSTreader.Item("RESIDENZAPROVINCIA2"))
            RESIDENZAPROVINCIA3 = campodb(myPOSTreader.Item("RESIDENZAPROVINCIA3"))
            RESIDENZAPROVINCIA4 = campodb(myPOSTreader.Item("RESIDENZAPROVINCIA4"))

            RESIDENZACAP1 = campodb(myPOSTreader.Item("RESIDENZACAP1"))
            RESIDENZACAP2 = campodb(myPOSTreader.Item("RESIDENZACAP2"))
            RESIDENZACAP3 = campodb(myPOSTreader.Item("RESIDENZACAP3"))
            RESIDENZACAP4 = campodb(myPOSTreader.Item("RESIDENZACAP4"))

            RESIDENZATELEFONO1 = campodb(myPOSTreader.Item("RESIDENZATELEFONO1"))
            RESIDENZATELEFONO2 = campodb(myPOSTreader.Item("RESIDENZATELEFONO2"))
            RESIDENZATELEFONO3 = campodb(myPOSTreader.Item("RESIDENZATELEFONO3"))
            RESIDENZATELEFONO4 = campodb(myPOSTreader.Item("RESIDENZATELEFONO4"))
            CODICEFISCALE = campodb(myPOSTreader.Item("CODICEFISCALE"))

            CODICEIVA = campodb(myPOSTreader.Item("CODICEIVA"))
            TIPOOPERAZIONE = campodb(myPOSTreader.Item("TIPOOPERAZIONE"))
            MODALITAPAGAMENTO = campodb(myPOSTreader.Item("MODALITAPAGAMENTO"))

            MastroCliente = Val(campodb(myPOSTreader.Item("MastroCliente")))
            ContoCliente = Val(campodb(myPOSTreader.Item("ContoCliente")))
            SottoContoCliente = Val(campodb(myPOSTreader.Item("SottoContoCliente")))

            Nome = campodb(myPOSTreader.Item("Nome"))
            Sesso = campodb(myPOSTreader.Item("Sesso"))




            CodiceSanitario = campodb(myPOSTreader.Item("CodiceSanitario"))

            Compensazione = campodb(myPOSTreader.Item("Compensazione"))
            ComuneDiNascita = campodb(myPOSTreader.Item("ComuneDiNascita"))
            ProvinciaDiNascita = campodb(myPOSTreader.Item("ProvinciaDiNascita"))

            If campodb(myPOSTreader.Item("DataDomanda")) <> "" Then
                DataDomanda = campodb(myPOSTreader.Item("DataDomanda"))
            Else
                DataDomanda = Nothing
            End If

            If campodb(myPOSTreader.Item("DataNascita")) <> "" Then
                DataNascita = campodb(myPOSTreader.Item("DataNascita"))
            Else
                DataNascita = Nothing
            End If

            EsenzioneTicket = campodb(myPOSTreader.Item("EsenzioneTicket"))
            NOMECONIUGE = campodb(myPOSTreader.Item("NOMECONIUGE"))
            NOMEPADRE = campodb(myPOSTreader.Item("NOMEPADRE"))
            NOMEMADRE = campodb(myPOSTreader.Item("NOMEMADRE"))
            SETTIMANA = campodb(myPOSTreader.Item("SETTIMANA"))
            TELEFONO1 = campodb(myPOSTreader.Item("TELEFONO1"))
            TELEFONO2 = campodb(myPOSTreader.Item("TELEFONO2"))
            TELEFONO3 = campodb(myPOSTreader.Item("TELEFONO3"))
            TELEFONO4 = campodb(myPOSTreader.Item("TELEFONO4"))
            USL = campodb(myPOSTreader.Item("USL"))

            PERIODO = campodb(myPOSTreader.Item("PERIODO"))
            NOTE = campodb(myPOSTreader.Item("NOTE"))
            TIPOLOGIA = campodb(myPOSTreader.Item("TIPOLOGIA"))
            UTENTE = campodb(myPOSTreader.Item("UTENTE"))
            If campodb(myPOSTreader.Item("DATAAGGIORNAMENTO")) <> "" Then
                DATAAGGIORNAMENTO = myPOSTreader.Item("DATAAGGIORNAMENTO")
            Else
                DATAAGGIORNAMENTO = Nothing
            End If
            FattAnticipata = campodb(myPOSTreader.Item("FattAnticipata"))
            GradoParentela = campodb(myPOSTreader.Item("GradoParentela"))

            TipoOspite = campodb(myPOSTreader.Item("TipoOspite"))
            If campodb(myPOSTreader.Item("DataDeposito")) = "" Then
                DataDeposito = Nothing
            Else
                DataDeposito = campodb(myPOSTreader.Item("DataDeposito"))
            End If
            If campodb(myPOSTreader.Item("ImportoDeposito")) = "" Then
                ImportoDeposito = 0
            Else
                ImportoDeposito = campodb(myPOSTreader.Item("ImportoDeposito"))
            End If
            NonInUso = campodb(myPOSTreader.Item("NonInUso"))

            CodiceTicket = campodb(myPOSTreader.Item("CodiceTicket"))

            If campodb(myPOSTreader.Item("DataRilascio")) <> "" Then
                DataRilascio = campodb(myPOSTreader.Item("DataRilascio"))
            Else
                DataRilascio = Nothing
            End If
            If campodb(myPOSTreader.Item("DataScadenza")) <> "" Then
                DataScadenza = campodb(myPOSTreader.Item("DataScadenza"))
            Else
                DataScadenza = Nothing
            End If
            CodiceTicket2 = campodb(myPOSTreader.Item("CodiceTicket2"))
            If campodb(myPOSTreader.Item("DataRilascio2")) <> "" Then
                DataRilascio2 = campodb(myPOSTreader.Item("DataRilascio2"))
            Else
                DataRilascio2 = Nothing
            End If
            If campodb(myPOSTreader.Item("DataScadenza2")) <> "" Then
                DataScadenza2 = campodb(myPOSTreader.Item("DataScadenza2"))
            Else
                DataScadenza2 = Nothing
            End If

            NoteTicket = campodb(myPOSTreader.Item("NoteTicket"))
            PathImmagine = campodb(myPOSTreader.Item("PathImmagine"))
            NumeroDocumento = campodb(myPOSTreader.Item("NumeroDocumento"))
            If campodb(myPOSTreader.Item("DataDocumento")) <> "" Then
                DataDocumento = campodb(myPOSTreader.Item("DataDocumento"))
            Else
                DataDocumento = Nothing
            End If

            EmettitoreDocumento = campodb(myPOSTreader.Item("EmettitoreDocumento"))
            Nazionalita = campodb(myPOSTreader.Item("Nazionalita"))

            If campodb(myPOSTreader.Item("MesiAnticipo")) = "" Then
                MesiAnticipo = 0
            Else
                MesiAnticipo = campodb(myPOSTreader.Item("MesiAnticipo"))
            End If
            CodiceCIG = campodb(myPOSTreader.Item("CodiceCIG"))
            EmOspiteParente = Val(campodb(myPOSTreader.Item("EmOspiteParente")))
            If campodb(myPOSTreader.Item("DataAccreditoDeposito")) <> "" Then
                DataAccreditoDeposito = campodb(myPOSTreader.Item("DataAccreditoDeposito"))
            Else
                DataAccreditoDeposito = Nothing
            End If

            PROFCONIUGE = campodb(myPOSTreader.Item("PROFCONIUGE"))



            If campodb(myPOSTreader.Item("DATAMORTE")) <> "" Then
                DATAMORTE = campodb(myPOSTreader.Item("DATAMORTE"))
            Else
                DATAMORTE = Nothing
            End If

            LuogoMorte = campodb(myPOSTreader.Item("LuogoMorte"))
            ExtraFuoriRetta = Val(campodb(myPOSTreader.Item("ExtraFuoriRetta")))

            RESIDENZADATA1 = campodbd(myPOSTreader.Item("RESIDENZADATA1"))
            RESIDENZADATA2 = campodbd(myPOSTreader.Item("RESIDENZADATA2"))
            RESIDENZADATA3 = campodbd(myPOSTreader.Item("RESIDENZADATA3"))

            CodiceDestinatario = campodb(myPOSTreader.Item("CodiceDestinatario"))
            CodificaProvincia = campodb(myPOSTreader.Item("CodificaProvincia"))
            LetteraProvincia = campodb(myPOSTreader.Item("LetteraProvincia"))
            ProvinciaRilascio = campodb(myPOSTreader.Item("ProvinciaRilascio"))
            TipoDocumento = campodb(myPOSTreader.Item("TipoDocumento"))
            ComuneRilascio = campodb(myPOSTreader.Item("ComuneRilascio"))
            CodiceQuestura = campodb(myPOSTreader.Item("CodiceQuestura"))
            CognomeOspite = campodb(myPOSTreader.Item("CognomeOspite"))
            NomeOspite = campodb(myPOSTreader.Item("NomeOspite"))
            TesseraSanitaria = campodb(myPOSTreader.Item("TesseraSanitaria"))
            StatoCivile = campodb(myPOSTreader.Item("StatoCivile"))
            StrutturaProvenienza = campodb(myPOSTreader.Item("StrutturaProvenienza"))
            ScadenzaDocumento = campodbd(myPOSTreader.Item("ScadenzaDocumento"))

            AssistenteSociale = campodb(myPOSTreader.Item("AssistenteSociale"))
            CodiceLavanderia = campodb(myPOSTreader.Item("CodiceLavanderia"))
            Distretto = campodb(myPOSTreader.Item("Distretto"))
            ParenteIndirizzo = Val(campodb(myPOSTreader.Item("ParenteIndirizzo")))


            CABCLIENTE = campodb(myPOSTreader.Item("CABCLIENTE"))
            ABICLIENTE = campodb(myPOSTreader.Item("ABICLIENTE"))
            IntCliente = campodb(myPOSTreader.Item("IntCliente"))
            NumeroControlloCliente = campodb(myPOSTreader.Item("NumeroControlloCliente"))
            CINCLIENTE = campodb(myPOSTreader.Item("CINCLIENTE"))
            CCBANCARIOCLIENTE = campodb(myPOSTreader.Item("CCBANCARIOCLIENTE"))
            BancaCliente = campodb(myPOSTreader.Item("BancaCliente"))

            IntestatarioCC = campodb(myPOSTreader.Item("IntestatarioCC"))
            CodiceFiscaleCC = campodb(myPOSTreader.Item("CodiceFiscaleCC"))
            RotturaOspite = Val(campodb(myPOSTreader.Item("RotturaOspite")))
            Vincolo = campodb(myPOSTreader.Item("Vincolo"))

            ImportoSconto = campodbn(myPOSTreader.Item("ImportoSconto"))

            IdEpersonam = Val(campodb(myPOSTreader.Item("IdEpersonam")))

            TipoSconto = campodb(myPOSTreader.Item("IdEpersonam"))

            CONTOPERESATTO = campodb(myPOSTreader.Item("CONTOPERESATTO"))

            Opposizione730 = campodbn(myPOSTreader.Item("Opposizione730"))
            NomeMedico = campodb(myPOSTreader.Item("NomeMedico"))

            CONSENSOINSERIMENTO = campodbn(myPOSTreader.Item("CONSENSOINSERIMENTO"))

            CONSENSOMARKETING = campodbn(myPOSTreader.Item("CONSENSOMARKETING"))
            CartellaClinica = campodb(myPOSTreader.Item("CartellaClinica"))

            CodiceCup = campodb(myPOSTreader.Item("CodiceCup"))
        End If
        cn.Close()
    End Sub

    Public Sub Pulisci()
        CodiceOspite = 0

        CodiceMedico = 0
        RecapitoNome = ""

        RecapitoIndirizzo = ""
        RecapitoProvincia = ""
        RecapitoComune = ""
        RESIDENZAINDIRIZZO1 = ""
        RESIDENZAINDIRIZZO2 = ""
        RESIDENZAINDIRIZZO3 = ""
        RESIDENZAINDIRIZZO4 = ""

        RESIDENZACOMUNE1 = ""
        RESIDENZACOMUNE2 = ""
        RESIDENZACOMUNE3 = ""
        RESIDENZACOMUNE4 = ""

        RESIDENZAPROVINCIA1 = ""
        RESIDENZAPROVINCIA2 = ""
        RESIDENZAPROVINCIA3 = ""
        RESIDENZAPROVINCIA4 = ""

        RESIDENZACAP1 = ""
        RESIDENZACAP2 = ""
        RESIDENZACAP3 = ""
        RESIDENZACAP4 = ""

        RESIDENZATELEFONO1 = ""
        RESIDENZATELEFONO2 = ""
        RESIDENZATELEFONO3 = ""
        RESIDENZATELEFONO4 = ""
        CODICEFISCALE = ""

        CODICEIVA = ""
        TIPOOPERAZIONE = ""
        MODALITAPAGAMENTO = ""

        MastroCliente = 0
        ContoCliente = 0
        SottoContoCliente = 0

        Nome = ""
        Sesso = ""

        CodiceSanitario = ""

        Compensazione = ""
        ComuneDiNascita = ""
        ProvinciaDiNascita = ""


        DataDomanda = Nothing

        DataNascita = Nothing

        EsenzioneTicket = ""
        NOMECONIUGE = ""
        NOMEPADRE = ""
        NOMEMADRE = ""
        SETTIMANA = ""
        TELEFONO1 = ""
        TELEFONO2 = ""
        TELEFONO3 = ""
        TELEFONO4 = ""
        USL = ""

        PERIODO = ""
        NOTE = ""
        TIPOLOGIA = ""
        UTENTE = ""

        DATAAGGIORNAMENTO = Nothing

        FattAnticipata = ""
        GradoParentela = ""
        TipoOspite = ""

        DataDeposito = Nothing


        ImportoDeposito = 0

        NonInUso = ""

        CodiceTicket = ""

        DataRilascio = Nothing
        DataScadenza = Nothing

        CodiceTicket2 = ""

        DataRilascio2 = Nothing

        DataScadenza2 = Nothing

        NoteTicket = ""
        PathImmagine = ""
        NumeroDocumento = ""

        DataDocumento = Nothing


        EmettitoreDocumento = ""
        Nazionalita = ""


        MesiAnticipo = 0


        CodiceCIG = ""
        EmOspiteParente = 0

        DataAccreditoDeposito = Nothing


        PROFCONIUGE = ""
        DATAMORTE = Nothing


        LuogoMorte = ""
        ExtraFuoriRetta = 0

        RESIDENZADATA1 = Nothing
        RESIDENZADATA2 = Nothing
        RESIDENZADATA3 = Nothing
        RESIDENZADATA4 = Nothing
        CodiceDestinatario = ""
        CodificaProvincia = ""
        LetteraProvincia = ""
        ProvinciaRilascio = ""
        TipoDocumento = ""
        ComuneRilascio = ""
        CodiceQuestura = ""
        CognomeOspite = ""
        NomeOspite = ""
        TesseraSanitaria = ""
        StatoCivile = ""
        StrutturaProvenienza = ""
        ScadenzaDocumento = Nothing

        AssistenteSociale = ""
        CodiceLavanderia = ""
        Distretto = ""
        ParenteIndirizzo = 0


        CABCLIENTE = ""
        ABICLIENTE = ""
        IntCliente = ""
        NumeroControlloCliente = ""
        CINCLIENTE = ""
        CCBANCARIOCLIENTE = ""
        BancaCliente = ""


        IntestatarioCC = ""
        CodiceFiscaleCC = ""
        RotturaOspite = 0
        Vincolo = ""
        ImportoSconto = 0

        IdEpersonam = 0
        TipoSconto = ""
        CONTOPERESATTO = ""
        Opposizione730 = 0
        NomeMedico = ""

        CONSENSOINSERIMENTO = 0
        CONSENSOMARKETING = 0

        CartellaClinica = ""

        CodiceCup = ""
    End Sub

    Public Function Func_ImportoDeposito(ByVal ConnessioneOspiti As String, ByVal ConnessioneGenerale As String, ByVal ConnessioneTabelle As String) As Double
        Dim cn As OleDbConnection
        Dim Mastro As Long
        Dim Conto As Long
        Dim Sottoconto As Long
        Dim MySql As String

        cn = New Data.OleDb.OleDbConnection(ConnessioneGenerale)

        cn.Open()

        Dim TipoOper As New Cls_TipoOperazione


        TipoOper.Leggi(ConnessioneOspiti, TIPOOPERAZIONE)


        Dim XCs As New Cls_CentroServizio
        XCs.Leggi(ConnessioneOspiti, CodiceCentroServizio)

        Mastro = XCs.MASTRO
        Conto = XCs.CONTO
        Sottoconto = CodiceOspite * 100



        If Mastro = 0 Or Conto = 0 Or Sottoconto = 0 Then Exit Function



        MySql = "SELECT NumeroRegistrazione,DataRegistrazione " & _
                " FROM MovimentiContabiliRiga , MovimentiContabiliTesta " & _
                " WHERE MovimentiContabiliRiga.Numero = MovimentiContabiliTesta.NumeroRegistrazione " & _
                " AND MastroPartita = " & Mastro & _
                " AND  ContoPartita = " & Conto & _
                " AND SottoContoPartita = " & Sottoconto & _
                " AND CausaleContabile = '" & TipoOper.CausaleDeposito & "'"
        MySql = MySql & " ORDER BY  DataRegistrazione DESC,NumeroRegistrazione Desc, RigaDaCausale, MastroPartita, ContoPartita, SottocontoPartita"

        Dim cmd As New OleDbCommand()

        cmd.CommandText = (MySql)
        cmd.Connection = cn

        Dim myPOSTreader As OleDbDataReader = cmd.ExecuteReader()
        If myPOSTreader.Read Then
            Dim kDoc As New Cls_MovimentoContabile


            kDoc.NumeroRegistrazione = campodb(myPOSTreader.Item("NumeroRegistrazione"))
            kDoc.Leggi(ConnessioneGenerale, campodb(myPOSTreader.Item("NumeroRegistrazione")))
            myPOSTreader.Close()
            cn.Close()
            Return kDoc.ImportoDocumento(ConnessioneTabelle)
            Exit Function
        End If
        myPOSTreader.Close()
        cn.Close()
        Return 0
    End Function

    Public Function Func_DataDeposito(ByVal ConnessioneOspiti As String, ByVal ConnessioneGenerale As String) As Date
        Dim cn As OleDbConnection
        Dim Mastro As Long
        Dim Conto As Long
        Dim Sottoconto As Long
        Dim MySql As String

        cn = New Data.OleDb.OleDbConnection(ConnessioneGenerale)

        cn.Open()

        Dim TipoOper As New Cls_TipoOperazione


        TipoOper.Leggi(ConnessioneOspiti, TIPOOPERAZIONE)


        Dim XCs As New Cls_CentroServizio
        XCs.Leggi(ConnessioneOspiti, CodiceCentroServizio)

        Mastro = XCs.MASTRO
        Conto = XCs.CONTO
        Sottoconto = CodiceOspite * 100



        If Mastro = 0 Or Conto = 0 Or Sottoconto = 0 Then Exit Function



        MySql = "SELECT NumeroRegistrazione,DataRegistrazione " & _
                " FROM MovimentiContabiliRiga , MovimentiContabiliTesta " & _
                " WHERE MovimentiContabiliRiga.Numero = MovimentiContabiliTesta.NumeroRegistrazione " & _
                " AND MastroPartita = " & Mastro & _
                " AND  ContoPartita = " & Conto & _
                " AND SottoContoPartita = " & Sottoconto & _
                " AND CausaleContabile = '" & TipoOper.CausaleDeposito & "'"
        MySql = MySql & " ORDER BY  DataRegistrazione DESC,NumeroRegistrazione Desc, RigaDaCausale, MastroPartita, ContoPartita, SottocontoPartita"

        Dim cmd As New OleDbCommand()

        cmd.CommandText = (MySql)
        cmd.Connection = cn

        Dim myPOSTreader As OleDbDataReader = cmd.ExecuteReader()
        If myPOSTreader.Read Then
            Return campodbd(myPOSTreader.Item("DataRegistrazione"))
            myPOSTreader.Close()
            cn.Close()

            Exit Function
        End If
        myPOSTreader.Close()
        cn.Close()
        Return Nothing
    End Function
End Class

