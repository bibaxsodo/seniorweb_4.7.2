Imports Microsoft.VisualBasic

Public Class Cls_ControllaSottoconto
    Public Function ControllaSottoconto(ByVal SottoConto As String) As Boolean
        If Len(SottoConto) < 5 Then
            ControllaSottoconto = False
            Exit Function
        End If
        Dim appo As String
        appo = Replace(Replace(Replace(Mid(SottoConto, 1, 5), ",", ""), ".", ""), " ", "")
        If Not IsNumeric(appo) Then
            ControllaSottoconto = False
            Exit Function
        End If
        ControllaSottoconto = True
    End Function

    Public Function DecodificaSottoconto(ByVal stringaconnessione As String, ByVal MySottoConto As String) As String
        If ControllaSottoconto(MySottoConto) = False Then
            Return ""
            Exit Function
        End If
        Dim Mastro As Long
        Dim Conto As Long
        Dim Sottoconto As Long
        Dim Vettore(100) As String
        Vettore = SplitWords(MySottoConto)

        Mastro = Val(Vettore(0))
        Conto = Val(Vettore(1))
        Sottoconto = Val(Vettore(2))
        Dim XPianoConti As New Cls_Pianodeiconti

        XPianoConti.Mastro = Mastro
        XPianoConti.Conto = Conto
        XPianoConti.Sottoconto = Sottoconto
        XPianoConti.Decodfica(stringaconnessione)

        Return Mastro & " " & Conto & " " & Sottoconto & " " & XPianoConti.Descrizione
    End Function


    Private Function SplitWords(ByVal s As String) As String()
        '
        ' Call Regex.Split function from the imported namespace.
        ' Return the result array.
        '
        Return Regex.Split(s, "\W+")
    End Function
End Class
