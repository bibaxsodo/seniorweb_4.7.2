Imports Microsoft.VisualBasic
Imports System.Data.OleDb
Imports System.Web.Hosting

Public Class Cls_ImportiPastiTrasporti
    Public Codice As String
    Public Descrizione As String

    Sub Leggi(ByVal StringaConnessione As String, ByVal MCodice As String)
        Dim cn As OleDbConnection

        Codice = MCodice
        Descrizione = ""

        cn = New Data.OleDb.OleDbConnection(StringaConnessione)

        cn.Open()
        Dim cmd As New OleDbCommand()
        cmd.CommandText = ("select * from Tabelle Where TIPTAB = 'IME' AND CODTAB = ?")
        cmd.Parameters.AddWithValue("@Codice", MCodice)
        cmd.Connection = cn
        Dim sb As StringBuilder = New StringBuilder

        Dim myPOSTreader As OleDbDataReader = cmd.ExecuteReader()
        If myPOSTreader.Read Then
            Codice = MCodice
            Descrizione = myPOSTreader.Item("DESCRIZIONE")

        End If
        cn.Close()
    End Sub
End Class
