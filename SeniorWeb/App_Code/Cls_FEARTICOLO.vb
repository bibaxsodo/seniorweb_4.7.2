﻿Imports Microsoft.VisualBasic
Imports System.Data.OleDb
Imports System.Web.Hosting

Public Class Cls_FEARTICOLO
    Public CODICETIPO As String
    Public CODICEVALORE As String
    Public MASTRO As Integer
    Public CONTO As Integer
    Public SOTTOCONTO As Long


    Function campodbN(ByVal oggetto As Object) As Double
        If IsDBNull(oggetto) Then
            Return 0
        Else
            Return oggetto
        End If
    End Function

    Public Sub Delete(ByVal StringaConnessione As String, ByVal ID As Integer)
        Dim cn As OleDbConnection


        cn = New Data.OleDb.OleDbConnection(StringaConnessione)

        cn.Open()
        Dim cmd As New OleDbCommand()
        cmd.CommandText = ("select * from FEARTICOLO Where ID = ?")
        cmd.Parameters.AddWithValue("@ID", id)
        cmd.Connection = cn
        cmd.ExecuteNonQuery()

        cn.Close()
    End Sub

    Function campodb(ByVal oggetto As Object) As String
        If IsDBNull(oggetto) Then
            Return ""
        Else
            Return oggetto
        End If
    End Function

    Public Sub Leggi(ByVal StringaConnessione As String)
        Dim cn As OleDbConnection


        cn = New Data.OleDb.OleDbConnection(StringaConnessione)

        cn.Open()
        Dim cmd As New OleDbCommand()
        cmd.CommandText = ("select * from FEARTICOLO Where CODICETIPO = ? and  CODICEVALORE =?")
        cmd.Parameters.AddWithValue("@CODICETIPO", CODICETIPO)
        cmd.Parameters.AddWithValue("@CODICEVALORE", CODICEVALORE)
        cmd.Connection = cn
        Dim sb As StringBuilder = New StringBuilder


        Dim myPOSTreader As OleDbDataReader = cmd.ExecuteReader()
        If myPOSTreader.Read Then
            MASTRO = campodbN(myPOSTreader.Item("MASTRO"))
            CONTO = campodbN(myPOSTreader.Item("CONTO"))
            SOTTOCONTO = campodbN(myPOSTreader.Item("SOTTOCONTO"))

        End If
        myPOSTreader.Close()

        cn.Close()
    End Sub


    Sub Scrivi(ByVal StringaConnessione As String)
        Dim cn As OleDbConnection



        cn = New Data.OleDb.OleDbConnection(StringaConnessione)

        cn.Open()
        Dim cmd As New OleDbCommand()
        cmd.CommandText = ("select * from FEARTICOLO Where CODICETIPO = ? and  CODICEVALORE =?")
        cmd.Parameters.AddWithValue("@CODICETIPO", CODICETIPO)
        cmd.Parameters.AddWithValue("@CODICEVALORE", CODICEVALORE)
        cmd.Connection = cn
        Dim sb As StringBuilder = New StringBuilder


        Dim myPOSTreader As OleDbDataReader = cmd.ExecuteReader()
        If myPOSTreader.Read Then

            Dim CmqUP As New OleDbCommand

            CmqUP.Connection = cn
            CmqUP.CommandText = "UPDATE FEARTICOLO SET MASTRO = ?,CONTO=?,SOTTOCONTO=? WHERE  CODICETIPO = ? and  CODICEVALORE =?"


            CmqUP.Parameters.AddWithValue("@MASTRO", MASTRO)
            CmqUP.Parameters.AddWithValue("@CONTO", CONTO)
            CmqUP.Parameters.AddWithValue("@SOTTOCONTO", SOTTOCONTO)

            CmqUP.Parameters.AddWithValue("@CODICETIPO", CODICETIPO)
            CmqUP.Parameters.AddWithValue("@CODICEVALORE", CODICEVALORE)


            CmqUP.ExecuteNonQuery()
        Else
            Dim CmqADD As New OleDbCommand

            CmqADD.Connection = cn
            CmqADD.CommandText = "INSERT INTO FEARTICOLO  (CODICETIPO, CODICEVALORE,MASTRO, CONTO,SOTTOCONTO) VALUES (?,?,?,?,?)"
            CmqADD.Parameters.AddWithValue("@CODICETIPO", CODICETIPO)
            CmqADD.Parameters.AddWithValue("@CODICEVALORE", CODICEVALORE)

            CmqADD.Parameters.AddWithValue("@MASTRO", MASTRO)
            CmqADD.Parameters.AddWithValue("@CONTO", CONTO)
            CmqADD.Parameters.AddWithValue("@SOTTOCONTO", SOTTOCONTO)


            CmqADD.ExecuteNonQuery()


        End If
        myPOSTreader.Close()

        cn.Close()
    End Sub




End Class
