﻿Imports Microsoft.VisualBasic
Imports System.Data.OleDb
Imports System.Web.Hosting


Public Class Cls_DiminuzionePrenotazione    
    Public Anno As Long
    Public Numero As Long
    Public Riga As Long
    Public Data As Date
    Public Importo As Double
    Public Descrizione As String

    Function campodbN(ByVal oggetto As Object) As Double


        If IsDBNull(oggetto) Then
            Return 0
        Else
            Return oggetto
        End If
    End Function
    Function campodb(ByVal oggetto As Object) As String


        If IsDBNull(oggetto) Then
            Return ""
        Else
            Return oggetto
        End If
    End Function



    Sub loaddati(ByVal StringaConnessione As String, ByVal Tabella As System.Data.DataTable)
        Dim cn As OleDbConnection
        Dim Xps As Boolean

        cn = New Data.OleDb.OleDbConnection(StringaConnessione)

        cn.Open()
        Dim cmd As New OleDbCommand()

        cmd.CommandText = ("select * from VariazioniPrenotazioni where " & _
                             "AnnoPrenotazione = ? and NumeroPrenotazione = ? and RigaPrenotazione = ? ")
        cmd.Parameters.AddWithValue("@Anno", Anno)
        cmd.Parameters.AddWithValue("@Numero", Numero)
        cmd.Parameters.AddWithValue("@Riga", Riga)
        cmd.Connection = cn
        Dim sb As StringBuilder = New StringBuilder


        Tabella.Clear()
        Tabella.Columns.Clear()

        Tabella.Columns.Add("Anno", GetType(Integer))        
        Tabella.Columns.Add("Data", GetType(String))
        Tabella.Columns.Add("Importo", GetType(String))
        Tabella.Columns.Add("Descrizione", GetType(String))
        Xps = False

        Dim myPOSTreader As OleDbDataReader = cmd.ExecuteReader()
        Do While myPOSTreader.Read
            Dim myriga As System.Data.DataRow = Tabella.NewRow()

            myriga(0) = campodbN(myPOSTreader.Item("Anno"))            
            myriga(1) = Format(myPOSTreader.Item("DataRegistrazione"), "dd/MM/yyyy")
            myriga(2) = campodbN(myPOSTreader.Item("Importo"))
            myriga(3) = campodb(myPOSTreader.Item("Descrizione"))
            
            Tabella.Rows.Add(myriga)
            Xps = True
        Loop
        myPOSTreader.Close()
        cn.Close()
        If Xps = False Then
            Dim myriga As System.Data.DataRow = Tabella.NewRow()
            Tabella.Rows.Add(myriga)
        End If
    End Sub


    Sub Scrivi(ByVal StringaConnessione As String, ByVal Tabella As System.Data.DataTable)
        Dim cn As OleDbConnection
        Dim Max As Long
        Dim MySql As String


        cn = New Data.OleDb.OleDbConnection(StringaConnessione)

        cn.Open()
        Dim cmd As New OleDbCommand()

        cmd.CommandText = ("Delete from VariazioniPrenotazioni where " & _
                            " AnnoPrenotazione = ? and NumeroPrenotazione = ? and RigaPrenotazione = ? ")
        cmd.Parameters.AddWithValue("@Anno", Anno)
        cmd.Parameters.AddWithValue("@Numero", Numero)
        cmd.Parameters.AddWithValue("@Riga", Riga)
        cmd.Connection = cn

        cmd.ExecuteNonQuery()

        For Max = 0 To Tabella.Rows.Count - 1
            If Not IsDBNull(Tabella.Rows(Max).Item(1)) Then
                MySql = "INSERT INTO VariazioniPrenotazioni (AnnoPrenotazione,NumeroPrenotazione,RigaPrenotazione,Anno,DataRegistrazione,Importo,Descrizione) VALUES (?,?,?,?,?,?,?)"
                Dim cmdw As New OleDbCommand()
                cmdw.CommandText = (MySql)

                cmdw.Parameters.AddWithValue("@AnnoPrenotazione", Anno)
                cmdw.Parameters.AddWithValue("@Numero", Numero)
                cmdw.Parameters.AddWithValue("@Riga", Riga)

                cmdw.Parameters.AddWithValue("@Anno", CDbl(Tabella.Rows(Max).Item(0)))
                Dim DataAppoggio As Date = Tabella.Rows(Max).Item(1)

                cmdw.Parameters.AddWithValue("@DataRegistrazione", DataAppoggio)
                cmdw.Parameters.AddWithValue("@Importo", CDbl(Tabella.Rows(Max).Item(2)))
                cmdw.Parameters.AddWithValue("@Descrizione", Tabella.Rows(Max).Item(3))
          


                cmdw.Connection = cn
                cmdw.ExecuteNonQuery()
            End If
        Next

        cn.Close()
    End Sub

End Class
