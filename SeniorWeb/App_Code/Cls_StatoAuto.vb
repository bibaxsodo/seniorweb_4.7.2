Imports Microsoft.VisualBasic
Imports System.Data.OleDb
Imports System.Web.Hosting


Public Class Cls_StatoAuto
    Public CENTROSERVIZIO As String
    Public CODICEOSPITE As Long
    Public Data As Date
    Public STATOAUTO As String
    Public USL As String
    Public TipoRetta As String

    Public AtsCodice As String
    Public Sosia As String

    Public Sub EliminaAll(ByVal StringaConnessione As String)
        Dim cn As OleDbConnection
        Dim MySql As String

        MySql = ""

        cn = New Data.OleDb.OleDbConnection(StringaConnessione)

        cn.Open()
        Dim cmd As New OleDbCommand()
        cmd.CommandText = "Delete STATOAUTO where CentroServizio = '" & CENTROSERVIZIO & "' And  CodiceOspite = " & CODICEOSPITE
        cmd.Connection = cn
        cmd.ExecuteNonQuery()
        cn.Close()
    End Sub
    Public Sub Elimina(ByVal StringaConnessione As String)
        Dim cn As OleDbConnection
        Dim MySql As String

        MySql = ""

        cn = New Data.OleDb.OleDbConnection(StringaConnessione)

        cn.Open()
        Dim cmd As New OleDbCommand()
        cmd.CommandText = "Delete STATOAUTO where CentroServizio = '" & CENTROSERVIZIO & "' And  CodiceOspite = " & CODICEOSPITE & " And Data = ?"
        cmd.Parameters.AddWithValue("@Data", Data)
        cmd.Connection = cn
        cmd.ExecuteNonQuery()
        cn.Close()
    End Sub

    Public Sub AggiornaDB(ByVal StringaConnessione As String)
        Dim cn As OleDbConnection
        Dim MySql As String

        MySql = ""


        cn = New Data.OleDb.OleDbConnection(StringaConnessione)

        cn.Open()
        Dim cmd As New OleDbCommand()
        cmd.CommandText = ("select * from STATOAUTO where CentroServizio = '" & CENTROSERVIZIO & "' And  CodiceOspite = " & CODICEOSPITE & " And Data = ?")
        cmd.Parameters.AddWithValue("@Data", Data)
        cmd.Connection = cn
        Dim myPOSTreader As OleDbDataReader = cmd.ExecuteReader()
        If myPOSTreader.Read Then
            MySql = "UPDATE STATOAUTO SET STATOAUTO = ?,USL = ?,TipoRetta = ?,AtsCodice = ?,Sosia = ?  WHERE  CentroServizio = '" & CENTROSERVIZIO & "' And  CodiceOspite = " & CODICEOSPITE & " And Data = ?"
            Dim cmdw As New OleDbCommand()
            cmdw.CommandText = (MySql)
            cmdw.Parameters.AddWithValue("@STATOAUTO", STATOAUTO)
            cmdw.Parameters.AddWithValue("@USL", USL)
            cmdw.Parameters.AddWithValue("@TipoRetta", TipoRetta)
            cmdw.Parameters.AddWithValue("@AtsCodice", AtsCodice)
            cmdw.Parameters.AddWithValue("@Sosia", Sosia)
            cmdw.Parameters.AddWithValue("@Data", Data)
            cmdw.Connection = cn
            cmdw.ExecuteNonQuery()
        Else
            MySql = "INSERT INTO STATOAUTO (CentroServizio,CodiceOspite,Data,STATOAUTO,USL,TipoRetta,AtsCodice,Sosia) VALUES (?,?,?,?,?,?,?,?)"
            Dim cmdw As New OleDbCommand()
            cmdw.CommandText = (MySql)
            cmdw.Parameters.AddWithValue("@CentroServizio", CentroServizio)
            cmdw.Parameters.AddWithValue("@CodiceOspite", CodiceOspite)            
            cmdw.Parameters.AddWithValue("@Data", Data)
            cmdw.Parameters.AddWithValue("@STATOAUTO", STATOAUTO)
            cmdw.Parameters.AddWithValue("@USL", USL)
            cmdw.Parameters.AddWithValue("@TipoRetta", TipoRetta)
            cmdw.Parameters.AddWithValue("@AtsCodice", AtsCodice)
            cmdw.Parameters.AddWithValue("@Sosia", Sosia)

            cmdw.Connection = cn
            cmdw.ExecuteNonQuery()
        End If
        myPOSTreader.Close()
        cn.Close()
    End Sub


    Public Sub AggiornaDaTabella(ByVal StringaConnessione As String, ByVal Tabella As System.Data.DataTable)
        Dim cn As OleDbConnection
        Dim MySql As String

        MySql = ""

        cn = New Data.OleDb.OleDbConnection(StringaConnessione)

        cn.Open()


        Dim Transan As OleDbTransaction = cn.BeginTransaction()

        Dim cmd As New OleDbCommand()
        cmd.CommandText = "Delete from STATOAUTO where CentroServizio = '" & CENTROSERVIZIO & "' And  CodiceOspite = " & CODICEOSPITE
        cmd.Connection = cn
        cmd.Transaction = Transan
        cmd.ExecuteNonQuery()

        Dim i As Long

        For i = 0 To Tabella.Rows.Count - 1
            If IsDate(Tabella.Rows(i).Item(0)) And Tabella.Rows(i).Item(0) <> "" Then
                MySql = "INSERT INTO STATOAUTO (CentroServizio,CodiceOspite,Data,STATOAUTO,USL,TipoRetta,AtsCodice,Sosia) VALUES (?,?,?,?,?,?,?,?)"
                Dim cmdw As New OleDbCommand()
                cmdw.CommandText = (MySql)
                cmdw.Parameters.AddWithValue("@CentroServizio", CENTROSERVIZIO)
                cmdw.Parameters.AddWithValue("@CodiceOspite", CODICEOSPITE)
                Dim xData As Date

                xData = Tabella.Rows(i).Item(0)
                cmdw.Parameters.AddWithValue("@Data", xData)
                cmdw.Parameters.AddWithValue("@STATOAUTO", Tabella.Rows(i).Item(1))
                cmdw.Parameters.AddWithValue("@USL", Tabella.Rows(i).Item(2))
                cmdw.Parameters.AddWithValue("@TipoRetta", Tabella.Rows(i).Item(3))
                cmdw.Parameters.AddWithValue("@AtsCodice", "") 'Tabella.Rows(i).Item(4))
                cmdw.Parameters.AddWithValue("@Sosia", "") ' Tabella.Rows(i).Item(5))
                cmdw.Transaction = Transan
                cmdw.Connection = cn
                cmdw.ExecuteNonQuery()
            End If
        Next
        Transan.Commit()
        cn.Close()

    End Sub

    Public Sub StatoDopoData(ByVal StringaConnessione As String, ByVal CodiceOspite As Long, ByVal CentroServizio As String)
        Dim cn As OleDbConnection



        cn = New Data.OleDb.OleDbConnection(StringaConnessione)

        cn.Open()
        Dim cmd As New OleDbCommand()
        cmd.CommandText = ("select * from STATOAUTO where CentroServizio = '" & CentroServizio & "' And  CodiceOspite = " & CodiceOspite & " And Data > ? Order by Data Desc")
        cmd.Parameters.AddWithValue("@Data", Data)
        cmd.Connection = cn
        
        Dim myPOSTreader As OleDbDataReader = cmd.ExecuteReader()
        If myPOSTreader.Read Then
            CodiceOspite = campodb(myPOSTreader.Item("CodiceOspite"))
            CentroServizio = campodb(myPOSTreader.Item("CentroServizio"))
            Data = campodbd(myPOSTreader.Item("Data"))
            STATOAUTO = campodb(myPOSTreader.Item("STATOAUTO"))
            USL = campodb(myPOSTreader.Item("USL"))
            TipoRetta = campodb(myPOSTreader.Item("TipoRetta"))
            AtsCodice = campodb(myPOSTreader.Item("AtsCodice"))
            Sosia = campodb(myPOSTreader.Item("Sosia"))
        Else
            Data = Nothing
        End If
        myPOSTreader.Close()
        cn.Close()

    End Sub

    Public Sub StatoPrimaDataSenzaCSERV(ByVal StringaConnessione As String, ByVal CodiceOspite As Long)
        Dim cn As OleDbConnection

        cn = New Data.OleDb.OleDbConnection(StringaConnessione)

        cn.Open()
        Dim cmd As New OleDbCommand()
        cmd.CommandText = ("select * from STATOAUTO where CodiceOspite = " & CodiceOspite & " And Data <= ? Order by Data Desc")
        cmd.Parameters.AddWithValue("@Data", Data)
        cmd.Connection = cn

        Dim myPOSTreader As OleDbDataReader = cmd.ExecuteReader()
        If myPOSTreader.Read Then
            CodiceOspite = campodb(myPOSTreader.Item("CodiceOspite"))
            CentroServizio = campodb(myPOSTreader.Item("CentroServizio"))
            Data = campodbd(myPOSTreader.Item("Data"))
            STATOAUTO = campodb(myPOSTreader.Item("STATOAUTO"))
            USL = campodb(myPOSTreader.Item("USL"))
            TipoRetta = campodb(myPOSTreader.Item("TipoRetta"))
            AtsCodice = campodb(myPOSTreader.Item("AtsCodice"))
            Sosia = campodb(myPOSTreader.Item("Sosia"))
        Else
            Data = Nothing
        End If
        myPOSTreader.Close()
        cn.Close()
    End Sub

    Public Sub StatoPrimaData(ByVal StringaConnessione As String, ByVal CodiceOspite As Long, ByVal CentroServizio As String)
        Dim cn As OleDbConnection

        cn = New Data.OleDb.OleDbConnection(StringaConnessione)

        cn.Open()
        Dim cmd As New OleDbCommand()
        cmd.CommandText = ("select * from STATOAUTO where CentroServizio = '" & CentroServizio & "' And  CodiceOspite = " & CodiceOspite & " And Data <= ? Order by Data Desc")
        cmd.Parameters.AddWithValue("@Data", Data)
        cmd.Connection = cn

        Dim myPOSTreader As OleDbDataReader = cmd.ExecuteReader()
        If myPOSTreader.Read Then
            CodiceOspite = campodb(myPOSTreader.Item("CodiceOspite"))
            CentroServizio = campodb(myPOSTreader.Item("CentroServizio"))
            Data = campodbd(myPOSTreader.Item("Data"))
            STATOAUTO = campodb(myPOSTreader.Item("STATOAUTO"))
            USL = campodb(myPOSTreader.Item("USL"))
            TipoRetta = campodb(myPOSTreader.Item("TipoRetta"))
            AtsCodice = campodb(myPOSTreader.Item("AtsCodice"))
            Sosia = campodb(myPOSTreader.Item("Sosia"))
        Else
            Data = Nothing
        End If
        myPOSTreader.Close()
        cn.Close()
    End Sub

    Public Sub UltimaDataConUslTipoExtra(ByVal StringaConnessione As String, ByVal CodiceOspite As Long, ByVal CentroServizio As String)
        Dim cn As OleDbConnection



        cn = New Data.OleDb.OleDbConnection(StringaConnessione)

        cn.Open()
        Dim cmd As New OleDbCommand()
        cmd.CommandText = ("select * from STATOAUTO where CentroServizio = '" & CentroServizio & "' And  CodiceOspite = " & CodiceOspite & " And USL = ? And  TipoRetta = ? Order by Data Desc")
        cmd.Parameters.AddWithValue("@USL", USL)
        cmd.Parameters.AddWithValue("@TipoRetta", TipoRetta)
        cmd.Connection = cn
        Dim sb As StringBuilder = New StringBuilder

        Dim myPOSTreader As OleDbDataReader = cmd.ExecuteReader()
        If myPOSTreader.Read Then
            CodiceOspite = campodb(myPOSTreader.Item("CodiceOspite"))
            CentroServizio = campodb(myPOSTreader.Item("CentroServizio"))
            Data = campodbd(myPOSTreader.Item("Data"))
            STATOAUTO = campodb(myPOSTreader.Item("STATOAUTO"))
            USL = campodb(myPOSTreader.Item("USL"))
            TipoRetta = campodb(myPOSTreader.Item("TipoRetta"))
            AtsCodice = campodb(myPOSTreader.Item("AtsCodice"))
            Sosia = campodb(myPOSTreader.Item("Sosia"))
        End If
        myPOSTreader.Close()
        cn.Close()

    End Sub
    Public Sub UltimaDataConUslIndicata(ByVal StringaConnessione As String, ByVal CodiceOspite As Long, ByVal CentroServizio As String, ByVal USLIndicata As String)
        Dim cn As OleDbConnection



        cn = New Data.OleDb.OleDbConnection(StringaConnessione)

        cn.Open()
        Dim cmd As New OleDbCommand()
        cmd.CommandText = ("select * from STATOAUTO where CentroServizio = '" & CentroServizio & "' And  CodiceOspite = " & CodiceOspite & " And  USL = ? Order by Data Desc")
        cmd.Connection = cn
        cmd.Parameters.AddWithValue("@usl", USLIndicata)

        Dim myPOSTreader As OleDbDataReader = cmd.ExecuteReader()
        If myPOSTreader.Read Then
            CodiceOspite = campodb(myPOSTreader.Item("CodiceOspite"))
            CentroServizio = campodb(myPOSTreader.Item("CentroServizio"))
            Data = campodbd(myPOSTreader.Item("Data"))
            STATOAUTO = campodb(myPOSTreader.Item("STATOAUTO"))
            USL = campodb(myPOSTreader.Item("USL"))
            TipoRetta = campodb(myPOSTreader.Item("TipoRetta"))
            AtsCodice = campodb(myPOSTreader.Item("AtsCodice"))
            Sosia = campodb(myPOSTreader.Item("Sosia"))
        End If
        myPOSTreader.Close()
        cn.Close()

    End Sub

    Public Sub UltimaDataConUsl(ByVal StringaConnessione As String, ByVal CodiceOspite As Long, ByVal CentroServizio As String)
        Dim cn As OleDbConnection



        cn = New Data.OleDb.OleDbConnection(StringaConnessione)

        cn.Open()
        Dim cmd As New OleDbCommand()
        cmd.CommandText = ("select * from STATOAUTO where CentroServizio = '" & CentroServizio & "' And  CodiceOspite = " & CodiceOspite & " And Not USL Is Null And USL <> '' Order by Data Desc")
        cmd.Connection = cn
        Dim sb As StringBuilder = New StringBuilder

        Dim myPOSTreader As OleDbDataReader = cmd.ExecuteReader()
        If myPOSTreader.Read Then
            CodiceOspite = campodb(myPOSTreader.Item("CodiceOspite"))
            CentroServizio = campodb(myPOSTreader.Item("CentroServizio"))
            Data = campodbd(myPOSTreader.Item("Data"))
            STATOAUTO = campodb(myPOSTreader.Item("STATOAUTO"))
            USL = campodb(myPOSTreader.Item("USL"))
            TipoRetta = campodb(myPOSTreader.Item("TipoRetta"))
            AtsCodice = campodb(myPOSTreader.Item("AtsCodice"))
            Sosia = campodb(myPOSTreader.Item("Sosia"))
        End If
        myPOSTreader.Close()
        cn.Close()

    End Sub


    Public Sub UltimaData(ByVal StringaConnessione As String, ByVal CodiceOspite As Long, ByVal CentroServizio As String)
        Dim cn As OleDbConnection



        cn = New Data.OleDb.OleDbConnection(StringaConnessione)

        cn.Open()
        Dim cmd As New OleDbCommand()
        cmd.CommandText = ("select * from STATOAUTO where CentroServizio = '" & CentroServizio & "' And  CodiceOspite = " & CodiceOspite & " Order by Data Desc")
        cmd.Connection = cn
        Dim sb As StringBuilder = New StringBuilder

        Dim myPOSTreader As OleDbDataReader = cmd.ExecuteReader()
        If myPOSTreader.Read Then
            CodiceOspite = campodb(myPOSTreader.Item("CodiceOspite"))
            CentroServizio = campodb(myPOSTreader.Item("CentroServizio"))
            Data = campodbd(myPOSTreader.Item("Data"))
            STATOAUTO = campodb(myPOSTreader.Item("STATOAUTO"))
            USL = campodb(myPOSTreader.Item("USL"))
            TipoRetta = campodb(myPOSTreader.Item("TipoRetta"))
            AtsCodice = campodb(myPOSTreader.Item("AtsCodice"))
            Sosia = campodb(myPOSTreader.Item("Sosia"))
        End If
        myPOSTreader.Close()
        cn.Close()

    End Sub
    Function campodbd(ByVal oggetto As Object) As Date
        If IsDBNull(oggetto) Then
            Return Nothing
        Else
            Return oggetto
        End If
    End Function
    Function campodb(ByVal oggetto As Object) As String
        If IsDBNull(oggetto) Then
            Return ""
        Else
            Return oggetto
        End If
    End Function

    Sub loaddati(ByVal StringaConnessione As String, ByVal codiceospite As Integer, ByVal centroservizio As String, ByVal Tabella As System.Data.DataTable)
        Dim cn As OleDbConnection

        cn = New Data.OleDb.OleDbConnection(StringaConnessione)

        cn.Open()
        Dim cmd As New OleDbCommand()
        cmd.CommandText = ("select * from STATOAUTO where centroservizio = '" & centroservizio & "' And CodiceOspite = " & codiceospite & " order by Data")
        cmd.Connection = cn
        Dim sb As StringBuilder = New StringBuilder

        Tabella.Clear()
        Tabella.Columns.Clear()
        Tabella.Columns.Add("Data", GetType(String))
        Tabella.Columns.Add("STATOAUTO", GetType(String))
        Tabella.Columns.Add("USL", GetType(String))
        Tabella.Columns.Add("TipoRetta", GetType(String))
        Tabella.Columns.Add("AtsCodice", GetType(String))
        Tabella.Columns.Add("Sosia", GetType(String))

        Dim myPOSTreader As OleDbDataReader = cmd.ExecuteReader()
        Do While myPOSTreader.Read

            Dim myriga As System.Data.DataRow = Tabella.NewRow()
            myriga(0) = Format(campodbd(myPOSTreader.Item("DATA")), "dd/MM/yyyy")
            myriga(1) = campodb(myPOSTreader.Item("STATOAUTO"))
            myriga(2) = campodb(myPOSTreader.Item("USL"))
            myriga(3) = campodb(myPOSTreader.Item("TipoRetta"))
            myriga(4) = campodb(myPOSTreader.Item("AtsCodice"))
            myriga(5) = campodb(myPOSTreader.Item("Sosia"))

            
            Tabella.Rows.Add(myriga)
        Loop
        myPOSTreader.Close()
        If Tabella.Rows.Count = 0 Then
            Dim myrigaV As System.Data.DataRow = Tabella.NewRow()

            Dim Mov As New Cls_Movimenti

            Mov.CENTROSERVIZIO = centroservizio
            Mov.CodiceOspite = codiceospite
            Mov.UltimaDataAccoglimento(StringaConnessione)
            myrigaV(0) = Mov.Data

            Tabella.Rows.Add(myrigaV)
        End If
        cn.Close()
    End Sub
End Class

