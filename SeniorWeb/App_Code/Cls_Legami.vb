Imports Microsoft.VisualBasic
Imports System.Data.OleDb
Imports System.Web.Hosting

Public Class Cls_Legami
    Public NumeroDocumento(300) As Long
    Public NumeroPagamento(300) As Long
    Public Importo(300) As Double
    Public IdDistinta(300)  as Long
    Public Utente as String


    Public Sub New()
        Dim I As Integer

        For I = 0 To 100
            NumeroDocumento(I) = 0
            NumeroPagamento(I) = 0
            Importo(I) = 0
        Next
    End Sub
    Function campodb(ByVal oggetto As Object) As String


        If IsDBNull(oggetto) Then
            Return ""
        Else
            Return oggetto
        End If
    End Function


    Function TotaleLegame(ByVal StringaConnessione As String, ByVal NumeroRegistrazione As Long) As Double

        If NumeroRegistrazione = 0 Then
            TotaleLegame = 0
            Exit Function
        End If

        Dim cn As OleDbConnection


        cn = New Data.OleDb.OleDbConnection(StringaConnessione)

        cn.Open()
        Dim cmd As New OleDbCommand()


        cmd.CommandText = ("select sum(Importo) from TabellaLegami where " & _
                           "CodiceDocumento = ? Or CodicePagamento = ? ")
        cmd.Parameters.AddWithValue("@NumeroRegistrazione", NumeroRegistrazione)
        cmd.Parameters.AddWithValue("@NumeroRegistrazione", NumeroRegistrazione)

        cmd.Connection = cn
        Dim VerReader As OleDbDataReader = cmd.ExecuteReader()
        If VerReader.Read Then
            If campodb(VerReader.Item(0)) = "" Then
                TotaleLegame = 0
            Else
                TotaleLegame = VerReader.Item(0)
            End If
        End If
        VerReader.Close()
        cn.Close()


    End Function
    Sub Leggi(ByVal StringaConnessione As String, ByVal MyNumeroDocumento As Long, ByVal MyNumeroPagamento As Long)
        Dim cn As OleDbConnection
        Dim Max As Long

        cn = New Data.OleDb.OleDbConnection(StringaConnessione)

        cn.Open()
        Dim cmd As New OleDbCommand()
        If MyNumeroDocumento > 0 Then
            cmd.CommandText = ("select * from TabellaLegami where " & _
                               "CodiceDocumento = ? ")
            cmd.Parameters.AddWithValue("@NumeroRegistrazione", MyNumeroDocumento)
        Else
            cmd.CommandText = ("select * from TabellaLegami where " & _
                               "CodicePagamento = ? ")
            cmd.Parameters.AddWithValue("@NumeroRegistrazione", MyNumeroPagamento)
        End If

        cmd.Connection = cn
        Max = 0
        Dim VerReader As OleDbDataReader = cmd.ExecuteReader()
        Do While VerReader.Read
            NumeroDocumento(Max) = VerReader.Item("CodiceDocumento")
            NumeroPagamento(Max) = VerReader.Item("CodicePagamento")
            Importo(Max) = VerReader.Item("Importo")
            Max = Max + 1
        Loop
        VerReader.Close()
        cn.Close()
    End Sub

    Sub Scrivi(ByVal StringaConnessione As String, ByVal MyNumeroDocumento As Long, ByVal MyNumeroPagamento As Long, Optional ByVal ArchivioTemp As Boolean = False)
        Dim cn As OleDbConnection
        Dim Max As Long
        Dim MySql As String

        cn = New Data.OleDb.OleDbConnection(StringaConnessione)

        cn.Open()

        Dim cmddel As New OleDbCommand()


        If MyNumeroDocumento > 0 Then
            If ArchivioTemp = False Then
                cmddel.CommandText = ("DELETE from TabellaLegami where " & _
                                   "CodiceDocumento = ? ")
            Else
                cmddel.CommandText = ("DELETE from Temp_TabellaLegami where " & _
                               "CodiceDocumento = ? ")

            End If

            cmddel.Parameters.AddWithValue("@NumeroRegistrazione", MyNumeroDocumento)
            cmddel.Connection = cn
            cmddel.ExecuteNonQuery()
        Else
            If ArchivioTemp = False Then
                cmddel.CommandText = ("DELETE from TabellaLegami where " & _
                                   "CodicePagamento = ? ")
            Else
                cmddel.CommandText = ("DELETE from Temp_TabellaLegami where " & _
                                   "CodicePagamento = ? ")
            End If
            cmddel.Parameters.AddWithValue("@NumeroRegistrazione", MyNumeroPagamento)
            cmddel.Connection = cn
            cmddel.ExecuteNonQuery()
            End If

            For Max = 0 To 300
                If IsDBNull(Importo(Max)) Then
                    Importo(Max) = 0
                End If

                If CDbl(Importo(Max)) <> 0 Then
                If ArchivioTemp = False Then
                    MySql = "INSERT INTO TabellaLegami (CodiceDocumento,CodicePagamento,Importo,IdDistinta,Utente,DataAggiornamento) VALUES (?,?,?,?,?,?)"
                Else
                    MySql = "INSERT INTO Temp_TabellaLegami (CodiceDocumento,CodicePagamento,Importo,IdDistinta,Utente,DataAggiornamento) VALUES (?,?,?,?,?,?)"
                End If
                Dim cmdINS As New OleDbCommand()
                cmdINS.CommandText = MySql
                cmdINS.Connection = cn
                cmdINS.Parameters.AddWithValue("@CodiceDocumento", NumeroDocumento(Max))
                cmdINS.Parameters.AddWithValue("@CodicePagamento", NumeroPagamento(Max))
                cmdINS.Parameters.AddWithValue("@Importo", Importo(Max)) ' Math.Abs(Importo(Max)))
                cmdINS.Parameters.AddWithValue("@IdDistinta", IdDistinta(Max))
                cmdINS.Parameters.AddWithValue("@Utente", Utente)
                cmdINS.Parameters.AddWithValue("@DataAggiornamento", now)
                cmdINS.ExecuteNonQuery()
            End If
        Next

            cn.Close()
    End Sub
End Class
