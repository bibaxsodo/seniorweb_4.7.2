﻿Imports Microsoft.VisualBasic
Imports System.Data.OleDb
Imports System.Web.Hosting
Public Class Cls_MovimentiStanze
    Public CentroServizio As String
    Public CodiceOspite As Long
    Public Id As Long
    Public Villa As String
    Public Reparto As String
    Public Piano As String
    Public Stanza As String
    Public Letto As String
    Public Data As Date
    Public Tipologia As String
    Public Utente As String


    Public Sub AggiornaDB(ByVal StringaConnessione As String)
        Dim cn As OleDbConnection
        Dim MySql As String

        MySql = ""


        cn = New Data.OleDb.OleDbConnection(StringaConnessione)

        cn.Open()
        Dim cmd As New OleDbCommand()
        cmd.CommandText = ("select * from Movimenti where  Id = " & Id)        
        cmd.Connection = cn
        Dim myPOSTreader As OleDbDataReader = cmd.ExecuteReader()
        If myPOSTreader.Read Then
            MySql = "UPDATE Movimenti SET CentroServizio = ?,CodiceOspite = ?,Villa = ?,Reparto = ?,Piano = ?,Stanza = ?,Letto = ?,Data = ?,Tipologia = ?,Utente = ?,DataAggiornamento = ? " & _
                    " WHERE  ID = " & Id
            Dim cmdw As New OleDbCommand()
            cmdw.CommandText = (MySql)

            cmdw.Parameters.AddWithValue("@CentroServizio", CentroServizio)
            cmdw.Parameters.AddWithValue("@CodiceOspite", CodiceOspite)
            cmdw.Parameters.AddWithValue("@Villa", Villa)
            cmdw.Parameters.AddWithValue("@Reparto", Reparto)
            cmdw.Parameters.AddWithValue("@Piano", Piano)
            cmdw.Parameters.AddWithValue("@Stanza", Stanza)
            cmdw.Parameters.AddWithValue("@Letto", Letto)
            cmdw.Parameters.AddWithValue("@Data", Data)
            cmdw.Parameters.AddWithValue("@Tipologia", Tipologia)
            cmdw.Parameters.AddWithValue("@Utente", Utente)
            cmdw.Parameters.AddWithValue("@DataAggiornamento", Now)            
            cmdw.Connection = cn
            cmdw.ExecuteNonQuery()
        Else

            MySql = "INSERT INTO Movimenti (CentroServizio,CodiceOspite,Villa ,Reparto, Piano  ,Stanza  ,Letto ,Data ,Tipologia ,Utente ,DataAggiornamento) VALUES (?,?,?, ?,?,?,?,?,?,?,?)"
            Dim cmdw As New OleDbCommand()
            cmdw.CommandText = (MySql)
            cmdw.Parameters.AddWithValue("@CentroServizio", CentroServizio)
            cmdw.Parameters.AddWithValue("@CodiceOspite", CodiceOspite)
            cmdw.Parameters.AddWithValue("@Villa", Villa)
            cmdw.Parameters.AddWithValue("@Reparto", Reparto)
            cmdw.Parameters.AddWithValue("@Piano", Piano)
            cmdw.Parameters.AddWithValue("@Stanza", Stanza)
            cmdw.Parameters.AddWithValue("@Letto", Letto)
            cmdw.Parameters.AddWithValue("@Data", Data)
            cmdw.Parameters.AddWithValue("@Tipologia", Tipologia)
            cmdw.Parameters.AddWithValue("@Utente", Utente)
            cmdw.Parameters.AddWithValue("@DataAggiornamento", Now)

            cmdw.Connection = cn
            cmdw.ExecuteNonQuery()
        End If
        myPOSTreader.Close()
        cn.Close()
    End Sub

    Public Sub Leggi(ByVal StringaConnessione As String)
        Dim cn As OleDbConnection


        cn = New Data.OleDb.OleDbConnection(StringaConnessione)

        cn.Open()
        Dim cmd As New OleDbCommand()
        cmd.CommandText = ("select * from Movimenti where CentroServizio = '" & CentroServizio & "' And CodiceOspite = " & CodiceOspite & " AND  Id =  " & Id)
        cmd.Connection = cn


        Dim myPOSTreader As OleDbDataReader = cmd.ExecuteReader()
        If myPOSTreader.Read Then

            CodiceOspite = campodbn(myPOSTreader.Item("codiceospite"))
            Villa = campodb(myPOSTreader.Item("Villa"))
            Reparto = campodb(myPOSTreader.Item("Reparto"))
            Piano = campodb(myPOSTreader.Item("Piano"))
            Stanza = campodb(myPOSTreader.Item("Stanza"))
            Letto = campodb(myPOSTreader.Item("Letto"))
            Data = campodbd(myPOSTreader.Item("Data"))
            Tipologia = campodb(myPOSTreader.Item("Tipologia"))
        End If
        myPOSTreader.Close()

    End Sub


    Public Sub UltimoMovimentoOspiteData(ByVal StringaConnessione As String, ByVal DataLimite As Date)
        Dim cn As OleDbConnection


        cn = New Data.OleDb.OleDbConnection(StringaConnessione)

        cn.Open()
        Dim cmd As New OleDbCommand()
        cmd.CommandText = ("select * from Movimenti where CentroServizio = '" & CentroServizio & "' And CodiceOspite = " & CodiceOspite & " And Data <= ? Order by Data Desc, id Desc")
        cmd.Parameters.AddWithValue("@DataLimite", DataLimite)
        cmd.Connection = cn

        Dim myPOSTreader As OleDbDataReader = cmd.ExecuteReader()
        If myPOSTreader.Read Then

            CodiceOspite = campodbn(myPOSTreader.Item("codiceospite"))
            Villa = campodb(myPOSTreader.Item("Villa"))
            Reparto = campodb(myPOSTreader.Item("Reparto"))
            Piano = campodb(myPOSTreader.Item("Piano"))
            Stanza = campodb(myPOSTreader.Item("Stanza"))
            Letto = campodb(myPOSTreader.Item("Letto"))
            Data = campodbd(myPOSTreader.Item("Data"))
            Tipologia = campodb(myPOSTreader.Item("Tipologia"))
        End If
        myPOSTreader.Close()

    End Sub

    Public Sub UltimoMovimentoOspite(ByVal StringaConnessione As String)
        Dim cn As OleDbConnection


        cn = New Data.OleDb.OleDbConnection(StringaConnessione)

        cn.Open()
        Dim cmd As New OleDbCommand()
        cmd.CommandText = ("select * from Movimenti where CentroServizio = '" & CentroServizio & "' And CodiceOspite = " & CodiceOspite & " Order by Data Desc, id Desc")

        cmd.Connection = cn

        Dim myPOSTreader As OleDbDataReader = cmd.ExecuteReader()
        If myPOSTreader.Read Then

            CodiceOspite = campodbn(myPOSTreader.Item("codiceospite"))
            Villa = campodb(myPOSTreader.Item("Villa"))
            Reparto = campodb(myPOSTreader.Item("Reparto"))
            Piano = campodb(myPOSTreader.Item("Piano"))
            Stanza = campodb(myPOSTreader.Item("Stanza"))
            Letto = campodb(myPOSTreader.Item("Letto"))
            Data = campodbd(myPOSTreader.Item("Data"))
            Tipologia = campodb(myPOSTreader.Item("Tipologia"))
        End If
        myPOSTreader.Close()

    End Sub


    Public Sub UltimoMovimentoOspiteSenza(ByVal StringaConnessione As String)
        Dim cn As OleDbConnection


        cn = New Data.OleDb.OleDbConnection(StringaConnessione)

        cn.Open()
        Dim cmd As New OleDbCommand()
        cmd.CommandText = ("select * from Movimenti where CodiceOspite = " & CodiceOspite & " Order by Data Desc, id Desc")

        cmd.Connection = cn

        Dim myPOSTreader As OleDbDataReader = cmd.ExecuteReader()
        If myPOSTreader.Read Then

            CodiceOspite = campodbn(myPOSTreader.Item("codiceospite"))
            Villa = campodb(myPOSTreader.Item("Villa"))
            Reparto = campodb(myPOSTreader.Item("Reparto"))
            Piano = campodb(myPOSTreader.Item("Piano"))
            Stanza = campodb(myPOSTreader.Item("Stanza"))
            Letto = campodb(myPOSTreader.Item("Letto"))
            Data = campodbd(myPOSTreader.Item("Data"))
            Tipologia = campodb(myPOSTreader.Item("Tipologia"))
        End If
        myPOSTreader.Close()

    End Sub

    Public Sub StanzaOspite(ByVal StringaConnessione As String)
        Dim cn As OleDbConnection


        cn = New Data.OleDb.OleDbConnection(StringaConnessione)

        cn.Open()
        Dim cmd As New OleDbCommand()
        cmd.CommandText = ("select * from Movimenti where CentroServizio = '" & CentroServizio & "' And CodiceOspite = " & CodiceOspite & " AND  Data <= ? Order by Data Desc")
        cmd.Parameters.AddWithValue("@Data", Now)
        cmd.Connection = cn

        Dim myPOSTreader As OleDbDataReader = cmd.ExecuteReader()
        If myPOSTreader.Read Then

            CodiceOspite = campodbn(myPOSTreader.Item("codiceospite"))
            Villa = campodb(myPOSTreader.Item("Villa"))
            Reparto = campodb(myPOSTreader.Item("Reparto"))
            Piano = campodb(myPOSTreader.Item("Piano"))
            Stanza = campodb(myPOSTreader.Item("Stanza"))
            Letto = campodb(myPOSTreader.Item("Letto"))
            Data = campodbd(myPOSTreader.Item("Data"))
            Tipologia = campodb(myPOSTreader.Item("Tipologia"))
        End If
        myPOSTreader.Close()

    End Sub

    Function campodb(ByVal oggetto As Object) As String
        If IsDBNull(oggetto) Then
            Return ""
        Else
            Return oggetto
        End If
    End Function
    Function campodbn(ByVal oggetto As Object) As Double
        If IsDBNull(oggetto) Then
            Return 0
        Else
            Return oggetto
        End If
    End Function
    Function campodbd(ByVal oggetto As Object) As Date
        If IsDBNull(oggetto) Then
            Return Nothing
        Else
            Return oggetto
        End If
    End Function


    Public Sub Elimina(ByVal StringaConnessione As String)
        Dim cn As OleDbConnection
        Dim MySql As String

        MySql = ""

        cn = New Data.OleDb.OleDbConnection(StringaConnessione)

        cn.Open()
        Dim cmd As New OleDbCommand()
        cmd.CommandText = ("Delete from Movimenti where   CentroServizio = '" & CentroServizio & "' And CodiceOspite = " & CodiceOspite & " AND  Id =  " & Id)        
        cmd.Connection = cn
        cmd.ExecuteNonQuery()
        cn.Close()
    End Sub

    

    Sub loaddati(ByVal StringaConnessione As String, ByVal codiceospite As Integer, ByVal AnnoMovimento As Long, ByVal Tabella As System.Data.DataTable)
        Dim cn As OleDbConnection


        cn = New Data.OleDb.OleDbConnection(StringaConnessione)

        cn.Open()
        Dim cmd As New OleDbCommand()
        cmd.CommandText = ("select * from Movimenti where CentroServizio = '" & CentroServizio & "' And CodiceOspite = " & codiceospite & " Order by Data Desc,ID Desc")
        cmd.Connection = cn
        Dim sb As StringBuilder = New StringBuilder

        Tabella.Clear()
        Tabella.Columns.Clear()
        Tabella.Columns.Add("ID", GetType(String))
        Tabella.Columns.Add("Data", GetType(String))
        Tabella.Columns.Add("TipoMovimento", GetType(String))
        Tabella.Columns.Add("Stanza", GetType(String))
        

        Dim myPOSTreader As OleDbDataReader = cmd.ExecuteReader()
        Do While myPOSTreader.Read

            Dim myriga As System.Data.DataRow = Tabella.NewRow()
            myriga(0) = campodbn(myPOSTreader.Item("ID"))
            myriga(1) = Format(campodbd(myPOSTreader.Item("Data")), "dd/MM/yyyy")
            If campodb(myPOSTreader.Item("Tipologia")) = "OC" Then
                myriga(2) = "Occupazione"
            Else
                myriga(2) = "Liberazione"
            End If
            Dim TOCVilla As New Cls_TabelleDescrittiveOspitiAccessori
            Dim TOCReparto As New Cls_TabelleDescrittiveOspitiAccessori

            TOCVilla.TipoTabella = "VIL"
            TOCVilla.CodiceTabella = campodb(myPOSTreader.Item("VILLA"))
            TOCVilla.Leggi(StringaConnessione)

            TOCReparto.TipoTabella = "REP"
            TOCReparto.CodiceTabella = campodb(myPOSTreader.Item("REPARTO"))
            TOCReparto.Leggi(StringaConnessione)

            myriga(3) = TOCVilla.Descrizione & " " & TOCReparto.Descrizione & " " & campodb(myPOSTreader.Item("PIANO")) & " " & campodb(myPOSTreader.Item("STANZA")) & " " & campodb(myPOSTreader.Item("LETTO"))
            
            Tabella.Rows.Add(myriga)
        Loop
        myPOSTreader.Close()
        cn.Close()

    End Sub
End Class
