﻿Imports Microsoft.VisualBasic
Imports System.Web
Imports System.Web.Services
Imports System.Web.Services.Protocols
Imports System.Data.OleDb
Imports System.Web.Hosting
Imports System.Data.SqlTypes
Imports System.Net
Imports System.IO
Imports System.Security.Cryptography.X509Certificates
Imports System.Net.Security
Imports System.Web.Script.Serialization

Public Class Cls_CodiceFiscale

    Public Nome As String
    Public Cognome As String
    Public DataNascita As Date
    Public Sesso As String
    Public Comune As String
    Public Provincia As String

    Public StringaConnessione As String


    Private Function CampoCodFisComune(ByVal Comune As String, ByVal Provincia As String) As String
        Dim k As New ClsComune
        If IsNothing(Provincia) Then
            Provincia = ""
        End If
        If IsNothing(Comune) Then
            Comune = ""
        End If
        k.Provincia = Provincia
        k.Comune = Comune
        k.Leggi(StringaConnessione)

        CampoCodFisComune = k.CODXCODF
    End Function

    Public Function Make_CodiceFiscale() As String
        Dim Anno As String
        Dim i As Integer
        Dim Mese As String = ""
        Dim Giorno As String
        Dim Cgm As String
        Dim wConsonanti As String
        Dim pNome As String
        Dim wVocali As String
        Dim wCodFis As String
        Dim pCognome As String

        Dim Stringa As String
        Dim Nm As String
        Dim Somma As Integer
        Dim ChEl As String
        Dim NumEl As String
        Dim cars As String

        Dim MySomma As Integer        
        Dim LastLettera As String

        If Not IsDate(DataNascita) Then
            DataNascita = Now
        Else
            If Year(DataNascita) < 1900 Then
                DataNascita = Now
            End If
        End If
        Anno = Mid(Year(DataNascita), 3, 2)

        If Month(DataNascita) = 1 Then
            Mese = "A"
        End If
        If Month(DataNascita) = 2 Then
            Mese = "B"
        End If
        If Month(DataNascita) = 3 Then
            Mese = "C"
        End If
        If Month(DataNascita) = 4 Then
            Mese = "D"
        End If
        If Month(DataNascita) = 5 Then
            Mese = "E"
        End If
        If Month(DataNascita) = 6 Then
            Mese = "H"
        End If
        If Month(DataNascita) = 7 Then
            Mese = "L"
        End If
        If Month(DataNascita) = 8 Then
            Mese = "M"
        End If
        If Month(DataNascita) = 9 Then
            Mese = "P"
        End If
        If Month(DataNascita) = 10 Then
            Mese = "R"
        End If
        If Month(DataNascita) = 11 Then
            Mese = "S"
        End If
        If Month(DataNascita) = 12 Then
            Mese = "T"
        End If
        Giorno = Day(DataNascita)
        If Giorno < 10 Then
            Giorno = "0" & Day(DataNascita)
        End If
        If Sesso = "F" Then
            Giorno = Day(DataNascita) + 40
        End If


        wCodFis = ""

        '
        ' RICAVO IL COGNOME (123)
        '
        pCognome = UCase(Cognome)
        wVocali = ""
        wConsonanti = ""
        For i = 1 To Len(pCognome)
            If InStr("AEIOU", Mid(pCognome, i, 1)) Then
                wVocali = wVocali + Mid(pCognome, i, 1)
            ElseIf InStr("BCDFGHJKLMNPQRSTVWXYZ", Mid(pCognome, i, 1)) Then
                wConsonanti = wConsonanti + Mid(pCognome, i, 1)
            Else
                ' E' uno spazio, un apostrfo o altro che non va considerato
            End If
            If Len(wConsonanti) = 3 Then Exit For
        Next
        If Len(wConsonanti) < 3 Then wConsonanti = wConsonanti + Left(wVocali, 3 - Len(wConsonanti))
        If Len(wConsonanti) < 3 Then wConsonanti = wConsonanti + Space(3 - Len(wConsonanti)).Replace(" ", "X")
        Cgm = wConsonanti

        '
        ' RICAVO IL NOME (456)
        '
        pNome = UCase(Nome)
        wVocali = ""
        wConsonanti = ""
        For i = 1 To Len(pNome)
            If InStr("AEIOU", Mid(pNome, i, 1)) Then
                wVocali = wVocali + Mid(pNome, i, 1)
            ElseIf InStr("BCDFGHJKLMNPQRSTVWXYZ", Mid(pNome, i, 1)) Then
                wConsonanti = wConsonanti + Mid(pNome, i, 1)
            Else
                ' E' uno spazio, un apostrfo o altro che non va considerato
            End If
        Next i
        If Len(wConsonanti) >= 4 Then
            ' solo la prima, terza e quarta consonante
            wConsonanti = Left$(wConsonanti, 1) & Mid$(wConsonanti, 3, 2)
        ElseIf Len(wConsonanti) = 3 Then
            ' Va bene cosi'
        Else
            ' Aggiungo le wvocali
            wConsonanti = Left$(wConsonanti & wVocali, 3)
            ' se non basta, aggiungo le X
            If Len(wConsonanti) < 3 Then wConsonanti = Left$(wConsonanti & "XXX", 3)
        End If
        Nm = wConsonanti


        'Cgm = Cdf_CONSONANTI(Cognome)
        'Nm = Cdf_CONSONANTI(Nome)
        Dim Appoggio As String

        Stringa = Cgm & Nm & Anno & Mese & Giorno & CampoCodFisComune(Comune, Provincia) & "            "
        Somma = 0
        For i = 2 To 14 Step 2
            Appoggio = Mid(Stringa, i, 1)
            If Mid(Stringa, i, 1) <= "9" And Mid(Stringa, i, 1) >= "0" Then
                Somma = Somma + Mid(Stringa, i, 1)
            Else
                Somma = Somma + Asc(Mid(Stringa, i, 1)) - 65
            End If
        Next i

        ChEl = "A01B00C05D07E09F13G15H17I19J21K02L04M18N20O11P03Q06R08S12T14U16V10W22X25Y24Z23"
        NumEl = "000507091315171921"

        For i = 1 To 15 Step 2
            If Mid(Stringa, i, 1) <= "9" And Mid(Stringa, i, 1) >= "0" Then
                If Mid(Stringa, i, 1) = 0 Then
                    Somma = Somma + 1
                Else
                    Somma = Somma + Mid(NumEl, (Val(Mid(Stringa, i, 1)) * 2) - 1, 2)
                End If
            Else
                cars = InStr(1, ChEl, Mid(Stringa, i, 1))
                If cars <> 0 Then
                    Somma = Somma + Val(Mid(ChEl, cars + 1, 2))
                End If
            End If
        Next i
        MySomma = Int(Somma / 26)
        MySomma = Somma - (MySomma * 26)
        LastLettera = Chr(MySomma + 65)
        Make_CodiceFiscale = Mid(Stringa, 1, 15) & LastLettera

    End Function



    Public Function Check_CodiceFiscale(ByVal CodiceFiscale As String) As Boolean

        Dim Stringa As String
        Dim Nm As String
        Dim Somma As Integer
        Dim ChEl As String
        Dim NumEl As String

        Dim MySomma As Integer
        Dim LastLettera As String
        Dim cars As String

        Check_CodiceFiscale = True
        Stringa = CodiceFiscale & Space(16)
        Somma = 0
        For I = 2 To 14 Step 2
            If Trim(Mid(Stringa, I, 1)) <> "" Then
                Dim Appoggio As String = Mid(Stringa, I, 1)
                Dim Numero As Integer = Val(Appoggio)
                

                If Numero >= 1 Or Appoggio = "0" Then
                    Somma = Somma + Mid(Stringa, I, 1)
                Else
                    Somma = Somma + Asc(Mid(Stringa, I, 1)) - 65
                End If
            End If
        Next I

        ChEl = "A01B00C05D07E09F13G15H17I19J21K02L04M18N20O11P03Q06R08S12T14U16V10W22X25Y24Z23"
        NumEl = "000507091315171921"

        For I = 1 To 15 Step 2
            Dim Appoggio As String = Mid(Stringa, I, 1)
            Dim Numero As Integer = Val(Appoggio)

            If Numero >= 1 Or Appoggio = "0" Then
                If Mid(Stringa, I, 1) = 0 Then
                    Somma = Somma + 1
                Else
                    Somma = Somma + Mid(NumEl, (Val(Mid(Stringa, I, 1)) * 2) - 1, 2)
                End If
            Else
                cars = InStr(1, ChEl, Mid(Stringa, I, 1))
                If cars <> 0 Then
                    Somma = Somma + Val(Mid(ChEl, cars + 1, 2))
                End If
            End If
        Next I
        MySomma = Int(Somma / 26)
        MySomma = Somma - (MySomma * 26)
        LastLettera = Chr(MySomma + 65)

        If LastLettera <> Mid(CodiceFiscale, 16, 1) Then
            Check_CodiceFiscale = False
        End If
    End Function


    Public Function CheckPartitaIva(ByVal PIva As String) As Boolean
        Dim result As Boolean = False
        Const caratteri As Integer = 11        
        Dim partitaiva As String = PIva
        Dim pregex As Regex = New Regex("^\d{" + caratteri.ToString + "}$")
        If partitaiva Is Nothing Then
            Return result
        End If
        'If partitaiva.Length = codfisc Then
        ' Return CodiceFiscale.CheckCodiceFiscale(PIva)
        'End If
        If Not (partitaiva.Length = caratteri) Then
            If partitaiva.Length < caratteri Then
                partitaiva.PadLeft(caratteri, "0"c)
            Else
                partitaiva = partitaiva.Substring(2)
            End If
        End If
        Dim m As Match = pregex.Match(partitaiva)
        result = m.Success
        If result Then
            result = ((Not (Integer.Parse(partitaiva.Substring(0, 7)) = 0)) AndAlso (Integer.Parse(partitaiva.Substring(7, 3)) >= 0) AndAlso (Integer.Parse(partitaiva.Substring(7, 3)) < 201))
        End If
        If result Then
            Dim somma As Integer = 0
            Dim i As Integer = 0
            For i = 0 To caratteri - 2
                Dim j As Integer = Integer.Parse(partitaiva.Substring(i, 1))
                If (i + 1) Mod 2 = 0 Then
                    j *= 2
                    Dim c As Char() = j.ToString("00").ToCharArray
                    somma += Integer.Parse(c(0).ToString)
                    somma += Integer.Parse(c(1).ToString)
                Else
                    somma += j
                End If
            Next
            If (somma.ToString("00").Substring(1, 1) = "0") AndAlso (Not (partitaiva.Substring(10, 1) = "0")) Then
                result = False
            End If
            somma = Integer.Parse(partitaiva.Substring(10, 1)) + Integer.Parse(somma.ToString("00").Substring(1, 1))
            If result Then
                result = (somma.ToString("00").Substring(1, 1) = "0")
            End If
        End If
        Return result
    End Function


    Public Function VerificaPIVA(ByVal Piva As String) As Boolean

        Dim request As New NameValueCollection
        Dim client As New WebClient()

        Dim reqparm As New Specialized.NameValueCollection
        reqparm.Add("piva", Piva)

        Dim Tresult As Object = client.UploadValues("https://www.guidamonaci.it/gmbig/risultati/", "POST", reqparm)
        Dim Risultato As String = Encoding.Default.GetString(Tresult)


        Dim Appoggio As String = Risultato

        If Appoggio.IndexOf("Nessun riscontro nel DB") < 1 Then

            Appoggio = Mid(Risultato, Risultato.IndexOf("<div class=""lista_risultati"">") + 1, Len(Risultato) - Risultato.IndexOf("<div class=""lista_risultati"">") + 1)

            Appoggio = Mid(Appoggio, 1, Appoggio.IndexOf("</div>"))

            Return True
        Else
            Appoggio = "NON TROVATO"
            Return False
        End If
    End Function
End Class
