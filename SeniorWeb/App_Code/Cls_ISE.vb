Imports Microsoft.VisualBasic
Imports System.Data.OleDb
Imports System.Web.Hosting

Public Class Cls_Ise

    Public CENTROSERVIZIO As String
    Public CODICEOSPITE As Long
    Public DATAINIZIO As Date
    Public DATAFINE As Date
    Public DESCRIZIONE As String
    Public ISE As String
    Public ISEE As String
    Public UTENTE As String

    Function campodbD(ByVal oggetto As Object) As Date
        If IsDBNull(oggetto) Then
            Return Nothing
        Else
            Return oggetto
        End If
    End Function
    Function campodb(ByVal oggetto As Object) As String
        If IsDBNull(oggetto) Then
            Return ""
        Else
            Return oggetto
        End If
    End Function

    Public Sub EliminaAll(ByVal StringaConnessione As String)
        Dim cn As OleDbConnection
        Dim MySql As String

        MySql = ""

        cn = New Data.OleDb.OleDbConnection(StringaConnessione)

        cn.Open()
        Dim cmd As New OleDbCommand()
        cmd.CommandText = ("Delete from DATIISE Where CentroServizio = '" & CENTROSERVIZIO & "' And  CodiceOspite = " & CODICEOSPITE)
        cmd.Connection = cn
        cmd.ExecuteNonQuery()
        cn.Close()
    End Sub

    Public Sub Elimina(ByVal StringaConnessione As String)
        Dim cn As OleDbConnection
        Dim MySql As String

        MySql = ""

        cn = New Data.OleDb.OleDbConnection(StringaConnessione)

        cn.Open()
        Dim cmd As New OleDbCommand()
        cmd.CommandText = ("Delete from DATIISE Where CentroServizio = '" & CENTROSERVIZIO & "' And  CodiceOspite = " & CODICEOSPITE & "  And DATAINIZIO = ?")
        cmd.Parameters.AddWithValue("@DATAINIZIO", DATAINIZIO)
        cmd.Connection = cn
        cmd.ExecuteNonQuery()
        cn.Close()
    End Sub


    Public Sub AggiornaDaTabella(ByVal StringaConnessione As String, ByVal Tabella As System.Data.DataTable)
        Dim cn As OleDbConnection
        Dim MySql As String

        MySql = ""

        cn = New Data.OleDb.OleDbConnection(StringaConnessione)

        cn.Open()


        Dim Transan As OleDbTransaction = cn.BeginTransaction()

        Dim cmd As New OleDbCommand()
        cmd.CommandText = "Delete from DATIISE where CentroServizio = '" & CENTROSERVIZIO & "' And  CodiceOspite = " & CODICEOSPITE
        cmd.Connection = cn
        cmd.Transaction = Transan
        cmd.ExecuteNonQuery()

        Dim i As Long

        For i = 0 To Tabella.Rows.Count - 1
            If IsDate(Tabella.Rows(i).Item(0)) And Tabella.Rows(i).Item(0) <> "" Then
                MySql = "INSERT INTO DATIISE (CentroServizio,CodiceOspite,DATAINIZIO,DATAFINE,ISE,ISEE,DESCRIZIONE,UTENTE,DATAAGGIORNAMENTO) VALUES (?,?,?,?,?,?,?,?,?)"
                Dim cmdw As New OleDbCommand()
                cmdw.CommandText = (MySql)
                cmdw.Parameters.AddWithValue("@CentroServizio", CENTROSERVIZIO)
                cmdw.Parameters.AddWithValue("@CodiceOspite", CODICEOSPITE)
                Dim xData As Date

                If IsDate(Tabella.Rows(i).Item(0)) Then
                    xData = Tabella.Rows(i).Item(0)
                    cmdw.Parameters.AddWithValue("@DATAINIZIO", xData)
                Else
                    cmdw.Parameters.AddWithValue("@DATAINIZIO", System.DBNull.Value)
                End If
                If IsDate(Tabella.Rows(i).Item(1)) Then
                    xData = Tabella.Rows(i).Item(1)
                    cmdw.Parameters.AddWithValue("@DATAFINE", xData)
                Else
                    cmdw.Parameters.AddWithValue("@DATAFINE", System.DBNull.Value)
                End If

                cmdw.Parameters.AddWithValue("@ISE", Tabella.Rows(i).Item(2))
                cmdw.Parameters.AddWithValue("@ISEE", Tabella.Rows(i).Item(3))
                cmdw.Parameters.AddWithValue("@DESCRIZIONE", Tabella.Rows(i).Item(4))

                cmdw.Parameters.AddWithValue("@UTENTE", UTENTE)
                cmdw.Parameters.AddWithValue("@AGGIORNAMENTO", Now)

                cmdw.Transaction = Transan
                cmdw.Connection = cn
                cmdw.ExecuteNonQuery()
            End If
        Next
        Transan.Commit()
        cn.Close()

    End Sub

    Public Sub AggiornaDB(ByVal StringaConnessione As String)
        Dim cn As OleDbConnection
        Dim MySql As String

        MySql = ""


        cn = New Data.OleDb.OleDbConnection(StringaConnessione)

        cn.Open()
        Dim cmd As New OleDbCommand()
        cmd.CommandText = ("select * from DATIISE where CentroServizio = '" & CENTROSERVIZIO & "' And  CodiceOspite = " & CODICEOSPITE & "  And DATAINIZIO = ?")
        cmd.Parameters.AddWithValue("@DATAINIZIO", DATAINIZIO)
        cmd.Connection = cn
        Dim myPOSTreader As OleDbDataReader = cmd.ExecuteReader()
        If myPOSTreader.Read Then
            If DATAFINE = Nothing Then
                MySql = "UPDATE DATIISE SET DATAINIZIO = ?,DESCRIZIONE=?,ISE = ?,ISEE = ?,UTENTE = ?,DATAAGGIORNAMENTO = ? " & _
                        " WHERE  CentroServizio = '" & CENTROSERVIZIO & "' And  CodiceOspite = " & CODICEOSPITE & " And DATAINIZIO = ? "
            Else
                MySql = "UPDATE DATIISE SET DATAINIZIO = ?,DATAFINE=?,DESCRIZIONE=?,ISE = ?,ISEE = ?,UTENTE = ?,DATAAGGIORNAMENTO = ? " & _
                        " WHERE  CentroServizio = '" & CENTROSERVIZIO & "' And  CodiceOspite = " & CODICEOSPITE & " And DATAINIZIO = ? "
            End If
            Dim cmdw As New OleDbCommand()
            cmdw.CommandText = (MySql)
            cmdw.Parameters.AddWithValue("@DATAINIZIO", DATAINIZIO)
            If Not DATAFINE = Nothing Then
                cmdw.Parameters.AddWithValue("@DATAFINE", DATAFINE)
            End If
            cmdw.Parameters.AddWithValue("@DESCRIZIONE", DESCRIZIONE)
            cmdw.Parameters.AddWithValue("@ISE", ISE)
            cmdw.Parameters.AddWithValue("@ISEE", ISEE)
            cmdw.Parameters.AddWithValue("@DATAINIZIO", DATAINIZIO)
            cmdw.Parameters.AddWithValue("@UTENTE", UTENTE)
            cmdw.Parameters.AddWithValue("@DATAAGGIORNAMENTO", Now)
            cmdw.Connection = cn
            cmdw.ExecuteNonQuery()
        Else
            If Not DATAFINE = Nothing Then
                MySql = "INSERT INTO DATIISE (CentroServizio,CodiceOspite,DATAINIZIO,DATAFINE,DESCRIZIONE,ISE,ISEE) VALUES (?,?,?,?,?,?,?)"
            Else
                MySql = "INSERT INTO DATIISE (CentroServizio,CodiceOspite,DATAINIZIO,DESCRIZIONE,ISE,ISEE) VALUES (?,?,?,?,?,?)"
            End If
            Dim cmdw As New OleDbCommand()
            cmdw.CommandText = (MySql)
            cmdw.Parameters.AddWithValue("@CentroServizio", CENTROSERVIZIO)
            cmdw.Parameters.AddWithValue("@CodiceOspite", CODICEOSPITE)
            cmdw.Parameters.AddWithValue("@DATAINIZIO", DATAINIZIO)
            If Not DATAFINE = Nothing Then
                cmdw.Parameters.AddWithValue("@DATAFINE", DATAFINE)
            End If
            cmdw.Parameters.AddWithValue("@ISE", ISE)
            cmdw.Parameters.AddWithValue("@ISEE", ISEE)
            cmdw.Parameters.AddWithValue("@DESCRIZIONE", DESCRIZIONE)
            cmdw.Parameters.AddWithValue("@UTENTE", UTENTE)
            cmdw.Parameters.AddWithValue("@DATAAGGIORNAMENTO", Now)
            cmdw.Connection = cn
            cmdw.ExecuteNonQuery()
        End If
        myPOSTreader.Close()
        cn.Close()
    End Sub

    Public Sub UltimaData(ByVal StringaConnessione As String, ByVal CodiceOspite As Long, ByVal CentroServizio As String)
        Dim cn As OleDbConnection



        cn = New Data.OleDb.OleDbConnection(StringaConnessione)

        cn.Open()
        Dim cmd As New OleDbCommand()
        cmd.CommandText = ("select * from DATIISE where CentroServizio = '" & CentroServizio & "' And  CodiceOspite = " & CodiceOspite & " Order by DATAINIZIO Desc")
        cmd.Connection = cn
        Dim sb As StringBuilder = New StringBuilder

        Dim myPOSTreader As OleDbDataReader = cmd.ExecuteReader()
        If myPOSTreader.Read Then


            CentroServizio = campodb(myPOSTreader.Item("CentroServizio"))
            CodiceOspite = campodb(myPOSTreader.Item("CodiceOspite"))
            DATAINIZIO = campodbD(myPOSTreader.Item("DATAINIZIO"))
            If campodb(myPOSTreader.Item("DATAFINE")) <> "" Then
                DATAFINE = campodb(myPOSTreader.Item("DATAFINE"))
            Else
                DATAFINE = Nothing
            End If
            DESCRIZIONE = campodb(myPOSTreader.Item("DESCRIZIONE"))
            ISE = campodb(myPOSTreader.Item("ISE"))
            ISEE = campodb(myPOSTreader.Item("ISEE"))
        End If
        myPOSTreader.Close()
        cn.Close()

    End Sub


    Public Sub IseeAdata(ByVal StringaConnessione As String, ByVal CodiceOspite As Long, ByVal CentroServizio As String, ByVal Data As Date)
        Dim cn As OleDbConnection



        cn = New Data.OleDb.OleDbConnection(StringaConnessione)

        cn.Open()
        Dim cmd As New OleDbCommand()
        cmd.CommandText = ("select * from DATIISE where CentroServizio = '" & CentroServizio & "' And  CodiceOspite = " & CodiceOspite & " And DATAINIZIO <= ? And (DATAFINE >= ? OR  DATAFINE IS NULL) Order by DATAINIZIO Desc")
        cmd.Connection = cn
        cmd.Parameters.AddWithValue("@DataInizio", Data)
        cmd.Parameters.AddWithValue("@DATAFINE", Data)
        Dim myPOSTreader As OleDbDataReader = cmd.ExecuteReader()
        If myPOSTreader.Read Then


            CentroServizio = campodb(myPOSTreader.Item("CentroServizio"))
            CodiceOspite = campodb(myPOSTreader.Item("CodiceOspite"))
            DATAINIZIO = campodbD(myPOSTreader.Item("DATAINIZIO"))
            If campodb(myPOSTreader.Item("DATAFINE")) <> "" Then
                DATAFINE = campodb(myPOSTreader.Item("DATAFINE"))
            Else
                DATAFINE = Nothing
            End If
            DESCRIZIONE = campodb(myPOSTreader.Item("DESCRIZIONE"))
            ISE = campodb(myPOSTreader.Item("ISE"))
            ISEE = campodb(myPOSTreader.Item("ISEE"))
        End If
        myPOSTreader.Close()
        cn.Close()

    End Sub

    Sub loaddati(ByVal StringaConnessione As String, ByVal codiceospite As Integer, ByVal centroservizio As String, ByVal Tabella As System.Data.DataTable)
        Dim cn As OleDbConnection



        cn = New Data.OleDb.OleDbConnection(StringaConnessione)

        cn.Open()
        Dim cmd As New OleDbCommand()
        cmd.CommandText = ("select * from DATIISE where centroservizio = '" & centroservizio & "' And CodiceOspite = " & codiceospite & " Order By DATAINIZIO")
        cmd.Connection = cn
        Dim sb As StringBuilder = New StringBuilder

        Tabella.Clear()
        Tabella.Columns.Clear()
        Tabella.Columns.Add("DataInizio", GetType(String))
        Tabella.Columns.Add("DataFine", GetType(String))
        Tabella.Columns.Add("Descrizione", GetType(String))
        Tabella.Columns.Add("ISE", GetType(String))
        Tabella.Columns.Add("ISEE", GetType(String))

        Dim myPOSTreader As OleDbDataReader = cmd.ExecuteReader()
        Do While myPOSTreader.Read

            Dim myriga As System.Data.DataRow = Tabella.NewRow()
            myriga(0) = Format(myPOSTreader.Item("DATAINIZIO"), "dd/MM/yyyy")
            If IsDBNull(myPOSTreader.Item("DATAFINE")) Then
                myriga(1) = ""
            Else
                myriga(1) = Format(myPOSTreader.Item("DATAFINE"), "dd/MM/yyyy")
            End If
            myriga(2) = myPOSTreader.Item("ISE")
            myriga(3) = myPOSTreader.Item("ISEE")
            myriga(4) = myPOSTreader.Item("DESCRIZIONE")

            Tabella.Rows.Add(myriga)
        Loop
        myPOSTreader.Close()
        If Tabella.Rows.Count = 0 Then
            Dim myriga As System.Data.DataRow = Tabella.NewRow()
            Tabella.Rows.Add(myriga)
        End If
        cn.Close()
    End Sub
End Class
