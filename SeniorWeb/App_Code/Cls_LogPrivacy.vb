﻿Imports Microsoft.VisualBasic
Imports SeniorWebApplication
Imports System.Data.OleDb
Imports System.Threading
Imports System.Web.Script.Serialization

Public Class Cls_LogPrivacy
    Private Delegate Sub DoWorkDelegate(ByRef data As Object)

    Public Sub LogPrivacy(ByVal Connessione As String, ByVal Cliente As Integer, ByVal Utente As String, ByVal UTENTEIMPER As String, ByVal Pagina As String, ByVal CodiceOspite As Integer, ByVal CodiceParente As Integer, ByVal Struttura As String, ByVal Cserv As String, ByVal NumeroRegistrazione As Integer, ByVal Codifica As String, ByVal Tipo As String, ByVal Classe As String, ByVal JsonOggetto As String)

        Dim LK As New Cls_Login

        LK.Cliente = Cliente
        LK.LeggiClienteDB(Connessione)



        Dim VSession As New VariabiliSesssione

        VSession.DC_TABELLE = LK.TABELLE
        VSession.DC_OSPITE = LK.Ospiti
        VSession.DC_GENERALE = LK.Generale
        VSession.DC_OSPITIACCESSORI = LK.OspitiAccessori

        If UTENTEIMPER <> "" Then
            VSession.UTENTE = UTENTEIMPER
        Else
            VSession.UTENTE = Utente
        End If
        VSession.Pagina = Pagina
        VSession.CodiceOspite = CodiceOspite
        VSession.CodiceParente = CodiceParente
        VSession.NumeroRegistrazione = NumeroRegistrazione
        VSession.Struttura = Struttura
        VSession.Cserv = Cserv
        VSession.Codifica = Codifica
        VSession.Tipo = Tipo
        VSession.Classe = Classe
        VSession.JsonOggetto = JsonOggetto

        If VSession.Tipo = "M" Or VSession.Tipo = "D" Then
            DoWork(VSession)
        Else
            Dim t As New Thread(New ParameterizedThreadStart(AddressOf DoWork))

            t.Start(VSession)
        End If


    End Sub

    Public Sub DoWork(ByVal data As VariabiliSesssione)
        Dim Nome As String = ""

        Try
            If data.CodiceOspite > 0 And data.CodiceParente = 0 Then
                Dim CodOsp As New ClsOspite

                CodOsp.CodiceOspite = data.CodiceOspite
                CodOsp.Leggi(data.DC_OSPITE, CodOsp.CodiceOspite)

                Nome = CodOsp.Nome

                If data.Tipo = "M" Or data.Tipo = "D" Then
                    If data.Classe = "OSPITE" Then
                        Dim serializer As JavaScriptSerializer
                        serializer = New JavaScriptSerializer()

                        data.JsonOggetto = serializer.Serialize(CodOsp)
                    End If
                End If
            End If
            If data.CodiceOspite > 0 And data.CodiceParente > 0 Then
                Dim CodPar As New Cls_Parenti

                CodPar.CodiceOspite = data.CodiceOspite
                CodPar.CodiceParente = data.CodiceParente
                CodPar.Leggi(data.DC_OSPITE, CodPar.CodiceOspite, CodPar.CodiceParente)

                Nome = CodPar.Nome
                If data.Tipo = "M" Or data.Tipo = "D" Then
                    If data.Classe = "PARENTE" Then
                        Dim serializer As JavaScriptSerializer
                        serializer = New JavaScriptSerializer()

                        data.JsonOggetto = serializer.Serialize(CodPar)
                    End If
                End If
            End If
            If data.NumeroRegistrazione > 0 And data.CodiceOspite = 0 And data.CodiceParente = 0 Then
                Dim MCont As New Cls_MovimentoContabile

                MCont.NumeroRegistrazione = data.NumeroRegistrazione
                MCont.Leggi(data.DC_GENERALE, MCont.NumeroRegistrazione)

                If MCont.NumeroRegistrazione > 0 Then

                    Dim MDc As New Cls_Pianodeiconti

                    MDc.Mastro = MCont.Righe(0).MastroPartita
                    MDc.Conto = MCont.Righe(0).ContoPartita
                    MDc.Sottoconto = MCont.Righe(0).SottocontoPartita
                    MDc.Decodfica(data.DC_GENERALE)

                    Nome = MDc.Descrizione

                    If data.Tipo = "M" Or data.Tipo = "D" Then
                        If data.Classe = "DOCUMENTO" Then
                            Dim serializer As JavaScriptSerializer
                            serializer = New JavaScriptSerializer()

                            data.JsonOggetto = serializer.Serialize(MCont)
                        End If
                        If data.Classe = "INCASSO" Then
                            Dim serializer As JavaScriptSerializer
                            serializer = New JavaScriptSerializer()

                            data.JsonOggetto = serializer.Serialize(MCont)
                        End If
                    End If
                End If
            End If
        Catch ex As Exception

        End Try

        Try
            Dim K As New ValidaJwtws.WebService

            'K.LogInsert("jkjkkj")





            Dim cn As OleDbConnection

            cn = New Data.OleDb.OleDbConnection(data.DC_TABELLE)
            cn.Open()

            Dim cmdOspiti As New OleDbCommand()
            cmdOspiti.CommandText = ("INSERT INTO LogPrivacy (Utente,Pagina ,CodiceOspite ,CodiceParente, NumeroRegistrazione , STRUTTURA, CSERV, CODIFICA,Tipo,Classe, JsonOggetto ,  Nome, DATAORA  ) VALUES (?,?,?,?,?,?,?,?,?,?,?,?, ?)")
            cmdOspiti.Parameters.AddWithValue("@Utente", data.UTENTE)
            cmdOspiti.Parameters.AddWithValue("@Pagina", data.Pagina)
            cmdOspiti.Parameters.AddWithValue("@CodiceOspite", data.CodiceOspite)
            cmdOspiti.Parameters.AddWithValue("@CodiceParente", data.CodiceParente)
            cmdOspiti.Parameters.AddWithValue("@NumeroRegistrazione", data.NumeroRegistrazione)
            cmdOspiti.Parameters.AddWithValue("@Struttura", data.Struttura)
            cmdOspiti.Parameters.AddWithValue("@Cserv", data.Cserv)
            cmdOspiti.Parameters.AddWithValue("@Codifica", data.Codifica)

            cmdOspiti.Parameters.AddWithValue("@Tipo", data.Tipo)

            cmdOspiti.Parameters.AddWithValue("@Classe", data.Classe)
            cmdOspiti.Parameters.AddWithValue("@JsonOggetto", data.JsonOggetto)

            cmdOspiti.Parameters.AddWithValue("@Nome", Nome)

            cmdOspiti.Parameters.AddWithValue("@DATAORA", Now)
            cmdOspiti.Connection = cn
            cmdOspiti.ExecuteNonQuery()

            cn.Close()
        Catch ex As Exception

        End Try
    End Sub


    Public Class VariabiliSesssione
        Public DC_OSPITE As String
        Public DC_OSPITIACCESSORI As String
        Public DC_TABELLE As String
        Public DC_GENERALE As String
        Public UTENTE As String
        Public Pagina As String
        Public CodiceOspite As Integer
        Public CodiceParente As Integer
        Public NumeroRegistrazione As Integer
        Public Nome As String
        Public Tipo As String
        Public Classe As String
        Public JsonOggetto As String

        Public Struttura As String
        Public Cserv As String
        Public Codifica As String

    End Class
End Class

