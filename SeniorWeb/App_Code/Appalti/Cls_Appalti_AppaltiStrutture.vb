﻿Imports Microsoft.VisualBasic
Imports System.Data.OleDb
Imports System.Web.Hosting
Public Class Cls_Appalti_AppaltiStrutture
    Public IdAppalto As Integer
    Public IdStrutture As Integer
    Public ImportoForfait As Double
    Public KM As Integer
    Public KMImporto As Double

    Sub Leggi(ByVal StringaConnessione As String)
        Dim cn As OleDbConnection


        cn = New Data.OleDb.OleDbConnection(StringaConnessione)

        cn.Open()
        Dim cmd As New OleDbCommand()


        cmd.CommandText = ("select * from Appalti_AppaltiStrutture where " & _
                           "IdAppalto = ? AND IdStrutture = ?")
        cmd.Parameters.AddWithValue("@IdAppalto", IdAppalto)
        cmd.Parameters.AddWithValue("@IdStrutture", IdStrutture)
        cmd.Connection = cn
        Dim sb As StringBuilder = New StringBuilder

        Dim myPOSTreader As OleDbDataReader = cmd.ExecuteReader()
        If myPOSTreader.Read Then
            IdAppalto = campodb(myPOSTreader.Item("IdAppalto"))
            IdStrutture = campodb(myPOSTreader.Item("IdStrutture"))
            ImportoForfait = campodb(myPOSTreader.Item("ImportoForfait"))
            KM = campodbn(myPOSTreader.Item("KM"))
            KMImporto = campodbn(myPOSTreader.Item("KMImporto"))

        End If
        myPOSTreader.Close()
        cn.Close()
    End Sub


    Sub LeggiID(ByVal StringaConnessione As String, ByVal ID As Integer)
        Dim cn As OleDbConnection


        cn = New Data.OleDb.OleDbConnection(StringaConnessione)

        cn.Open()
        Dim cmd As New OleDbCommand()


        cmd.CommandText = ("select * from Appalti_AppaltiStrutture where " & _
                           "ID = ? ")
        cmd.Parameters.AddWithValue("@IdAppalto", ID)
        cmd.Connection = cn
        Dim sb As StringBuilder = New StringBuilder

        Dim myPOSTreader As OleDbDataReader = cmd.ExecuteReader()
        If myPOSTreader.Read Then
            IdAppalto = campodb(myPOSTreader.Item("IdAppalto"))
            IdStrutture = campodb(myPOSTreader.Item("IdStrutture"))
        End If
        myPOSTreader.Close()
        cn.Close()
    End Sub


    Sub LeggiAppaltoDaStruttura(ByVal StringaConnessione As String)
        Dim cn As OleDbConnection


        cn = New Data.OleDb.OleDbConnection(StringaConnessione)

        cn.Open()
        Dim cmd As New OleDbCommand()


        cmd.CommandText = ("select * from Appalti_AppaltiStrutture where " & _
                           "IdStrutture = ? ")
        cmd.Parameters.AddWithValue("@IdAppalto", IdStrutture)
        cmd.Connection = cn
        Dim sb As StringBuilder = New StringBuilder

        Dim myPOSTreader As OleDbDataReader = cmd.ExecuteReader()
        If myPOSTreader.Read Then
            IdAppalto = campodb(myPOSTreader.Item("IdAppalto"))
            IdStrutture = campodb(myPOSTreader.Item("IdStrutture"))
        End If
        myPOSTreader.Close()
        cn.Close()
    End Sub


    Sub loaddati(ByVal StringaConnessione As String, ByVal IdAppalti As Integer, ByVal Tabella As System.Data.DataTable)
        Dim cn As OleDbConnection


        cn = New Data.OleDb.OleDbConnection(StringaConnessione)

        cn.Open()
        Dim cmd As New OleDbCommand()
        cmd.CommandText = ("select * from Appalti_AppaltiStrutture where IdAppalto = " & IdAppalti)
        cmd.Connection = cn
        Dim sb As StringBuilder = New StringBuilder

        Tabella.Clear()
        Tabella.Columns.Clear()
        Tabella.Columns.Add("Struttura", GetType(String))
        Tabella.Columns.Add("ImportoForfait", GetType(String))
        Tabella.Columns.Add("KM", GetType(String))
        Tabella.Columns.Add("KMImporto", GetType(String))


        Dim myPOSTreader As OleDbDataReader = cmd.ExecuteReader()
        Do While myPOSTreader.Read

            Dim myriga As System.Data.DataRow = Tabella.NewRow()



            myriga(0) = campodbn(myPOSTreader.Item("IdStrutture"))

            myriga(1) = campodbn(myPOSTreader.Item("ImportoForfait"))

            myriga(2) = campodbn(myPOSTreader.Item("KM"))

            myriga(3) = campodbn(myPOSTreader.Item("KMImporto"))


            Tabella.Rows.Add(myriga)
        Loop
        myPOSTreader.Close()
        If Tabella.Rows.Count = 0 Then
            Dim myriga As System.Data.DataRow = Tabella.NewRow()


            myriga(0) = 0

            myriga(1) = 0

            myriga(2) = 0
            myriga(3) = 0

            Tabella.Rows.Add(myriga)
        End If
        cn.Close()
    End Sub

    Public Sub AggiornaDaTabella(ByVal StringaConnessione As String, ByVal Tabella As System.Data.DataTable)
        Dim cn As OleDbConnection
        Dim MySql As String

        MySql = ""

        cn = New Data.OleDb.OleDbConnection(StringaConnessione)

        cn.Open()


        Dim Transan As OleDbTransaction = cn.BeginTransaction()

        Dim cmd As New OleDbCommand()
        cmd.CommandText = "Delete from Appalti_AppaltiStrutture where IdAppalto = " & IdAppalto
        cmd.Connection = cn
        cmd.Transaction = Transan
        cmd.ExecuteNonQuery()

        Dim i As Long

        For i = 0 To Tabella.Rows.Count - 1
            'If IsDate(Tabella.Rows(i).Item(0)) And Tabella.Rows(i).Item(0) <> "" Then
            MySql = "INSERT INTO Appalti_AppaltiStrutture ([IdAppalto],[IdStrutture],ImportoForfait,KM,KMImporto) VALUES (?,?,?,?,?)"
            Dim cmdw As New OleDbCommand()
            cmdw.CommandText = (MySql)
            cmdw.Parameters.AddWithValue("@IdAppalto", IdAppalto)
            cmdw.Parameters.AddWithValue("@IdStrutture", Convert.ToDouble(Tabella.Rows(i).Item(0)))
            cmdw.Parameters.AddWithValue("@ImportoForfait", Convert.ToDouble(Tabella.Rows(i).Item(1)))
            If Val(campodb(Tabella.Rows(i).Item(2))) = 0 Then
                Tabella.Rows(i).Item(2) = 0
            End If

            cmdw.Parameters.AddWithValue("@KM", Convert.ToDouble(Tabella.Rows(i).Item(2)))

            If Val(campodb(Tabella.Rows(i).Item(3))) = 0 Then
                Tabella.Rows(i).Item(3) = 0
            End If
            cmdw.Parameters.AddWithValue("@KMImporto", Convert.ToDouble(Tabella.Rows(i).Item(3)))

            cmdw.Transaction = Transan
            cmdw.Connection = cn
            cmdw.ExecuteNonQuery()
            'End If
        Next
        Transan.Commit()
        cn.Close()

    End Sub


    Function campodb(ByVal oggetto As Object) As String
        If IsDBNull(oggetto) Then
            Return ""
        Else
            Return oggetto
        End If
    End Function

    Function campodbn(ByVal oggetto As Object) As Double
        If IsDBNull(oggetto) Then
            Return 0
        Else
            Return oggetto
        End If
    End Function

End Class
