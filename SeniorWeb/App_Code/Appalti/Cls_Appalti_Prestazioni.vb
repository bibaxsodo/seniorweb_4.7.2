﻿Imports Microsoft.VisualBasic
Imports System.Data.OleDb
Imports System.Web.Hosting

Public Class Cls_Appalti_Prestazioni
    Public ID As Integer
    Public Data As Date
    Public IdAppalto As Integer
    Public IdStrutture As Integer
    Public IdMansione As Integer
    Public Ore As Double
    Public CodiceOperatore As String
    Public intypedescription As String
    Public intypecod As String
    Public CodiceOspite As String
    Public codicecooperativa As Integer
    Public CalcoloPassivo As Integer
    Public Done As Integer

    Sub Leggi(ByVal StringaConnessione As String)
        Dim cn As OleDbConnection


        cn = New Data.OleDb.OleDbConnection(StringaConnessione)

        cn.Open()
        Dim cmd As New OleDbCommand()


        cmd.CommandText = ("select * from Appalti_Prestazioni where " & _
                           "ID = ? ")
        cmd.Parameters.AddWithValue("@Id", ID)
        cmd.Connection = cn
        Dim sb As StringBuilder = New StringBuilder

        Dim myPOSTreader As OleDbDataReader = cmd.ExecuteReader()
        If myPOSTreader.Read Then
            ID = campodb(myPOSTreader.Item("ID"))
            Data = campodbd(myPOSTreader.Item("Data"))
            IdAppalto = campodbn(myPOSTreader.Item("IdAppalto"))
            IdStrutture = campodbn(myPOSTreader.Item("IdStrutture"))
            IdMansione = campodbn(myPOSTreader.Item("IdMansione"))
            Ore = campodbn(myPOSTreader.Item("Ore"))
            CodiceOperatore = campodb(myPOSTreader.Item("CodiceOperatore"))

            intypecod = campodb(myPOSTreader.Item("intypecod"))
            intypedescription = campodb(myPOSTreader.Item("intypedescription"))

            CodiceOspite = campodbn(myPOSTreader.Item("CodiceOspite"))

            CalcoloPassivo = campodbn(myPOSTreader.Item("CalcoloPassivo"))

            Done = campodbn(myPOSTreader.Item("Done"))
        End If
        myPOSTreader.Close()
        cn.Close()
    End Sub

    Sub LeggiPrestazioneGiorno(ByVal StringaConnessione As String)
        Dim cn As OleDbConnection


        cn = New Data.OleDb.OleDbConnection(StringaConnessione)

        cn.Open()
        Dim cmd As New OleDbCommand()


        cmd.CommandText = ("select * from Appalti_Prestazioni where " & _
                           " IdAppalto = ? And IdStrutture = ? And  IdMansione = ? And CodiceOperatore = ?  And Data = ?")
        cmd.Parameters.AddWithValue("@IdAppalto", IdAppalto)
        cmd.Parameters.AddWithValue("@IdStrutture", IdStrutture)
        cmd.Parameters.AddWithValue("@IdMansione", IdMansione)
        cmd.Parameters.AddWithValue("@CodiceOperatore", CodiceOperatore)
        cmd.Parameters.AddWithValue("@Data", Data)
        cmd.Connection = cn

        Dim myPOSTreader As OleDbDataReader = cmd.ExecuteReader()
        If myPOSTreader.Read Then
            ID = campodb(myPOSTreader.Item("ID"))
            Data = campodbd(myPOSTreader.Item("Data"))
            IdAppalto = campodbn(myPOSTreader.Item("IdAppalto"))
            IdStrutture = campodbn(myPOSTreader.Item("IdStrutture"))
            IdMansione = campodbn(myPOSTreader.Item("IdMansione"))
            Ore = campodbn(myPOSTreader.Item("Ore"))
            CodiceOperatore = campodb(myPOSTreader.Item("CodiceOperatore"))


            intypecod = campodb(myPOSTreader.Item("intypecod"))
            intypedescription = campodb(myPOSTreader.Item("intypedescription"))

            CodiceOspite = campodbn(myPOSTreader.Item("CodiceOspite"))

            codicecooperativa = campodbn(myPOSTreader.Item("codicecooperativa"))

            CalcoloPassivo = campodbn(myPOSTreader.Item("CalcoloPassivo"))

            Done = campodbn(myPOSTreader.Item("Done"))
        End If
        myPOSTreader.Close()
        cn.Close()
    End Sub

    Sub LeggiPrestazioneGiornoTipoUtente(ByVal StringaConnessione As String)
        Dim cn As OleDbConnection


        cn = New Data.OleDb.OleDbConnection(StringaConnessione)

        cn.Open()
        Dim cmd As New OleDbCommand()


        cmd.CommandText = ("select * from Appalti_Prestazioni where " & _
                           " IdAppalto = ? And IdStrutture = ? And  IdMansione = ? And CodiceOperatore = ?  And Data = ? And  intypecod = ?")
        cmd.Parameters.AddWithValue("@IdAppalto", IdAppalto)
        cmd.Parameters.AddWithValue("@IdStrutture", IdStrutture)
        cmd.Parameters.AddWithValue("@IdMansione", IdMansione)
        cmd.Parameters.AddWithValue("@CodiceOperatore", CodiceOperatore)
        cmd.Parameters.AddWithValue("@Data", Data)
        cmd.Parameters.AddWithValue("@intypecod", intypecod)
        'intypecod
        cmd.Connection = cn

        Dim myPOSTreader As OleDbDataReader = cmd.ExecuteReader()
        If myPOSTreader.Read Then
            ID = campodb(myPOSTreader.Item("ID"))
            Data = campodbd(myPOSTreader.Item("Data"))
            IdAppalto = campodbn(myPOSTreader.Item("IdAppalto"))
            IdStrutture = campodbn(myPOSTreader.Item("IdStrutture"))
            IdMansione = campodbn(myPOSTreader.Item("IdMansione"))
            Ore = campodbn(myPOSTreader.Item("Ore"))
            CodiceOperatore = campodb(myPOSTreader.Item("CodiceOperatore"))

            intypecod = campodb(myPOSTreader.Item("intypecod"))
            intypedescription = campodb(myPOSTreader.Item("intypedescription"))

            CodiceOspite = campodbn(myPOSTreader.Item("CodiceOspite"))

            codicecooperativa = campodbn(myPOSTreader.Item("codicecooperativa"))

            CalcoloPassivo = campodbn(myPOSTreader.Item("CalcoloPassivo"))

            Done = campodbn(myPOSTreader.Item("Done"))
        End If
        myPOSTreader.Close()
        cn.Close()
    End Sub


    Sub Scrivi(ByVal StringaConnessione As String)
        Dim cn As OleDbConnection



        cn = New Data.OleDb.OleDbConnection(StringaConnessione)

        cn.Open()
        Dim cmd As New OleDbCommand()


        Dim cmdScrivi As New OleDbCommand()
        cmdScrivi.Connection = cn


        cmd.CommandText = ("select * from Appalti_Prestazioni where " & _
                           "ID = ? ")
        cmd.Parameters.AddWithValue("@Id", ID)
        cmd.Connection = cn
        Dim sb As StringBuilder = New StringBuilder

        Dim myPOSTreader As OleDbDataReader = cmd.ExecuteReader()
        If myPOSTreader.Read Then

            cmdScrivi.CommandText = "Update Appalti_Prestazioni set Data=?,IdAppalto=?,IdStrutture=?,IdMansione=?,Ore=?,CodiceOperatore=?,intypecod = ?,intypedescription = ?,CodiceOspite = ?,codicecooperativa = ?,CalcoloPassivo = ?,Done = ? Where id = " & ID
        Else
            cmdScrivi.CommandText = "INSERT INTO Appalti_Prestazioni (Data,IdAppalto,IdStrutture,IdMansione,Ore,CodiceOperatore,intypecod,intypedescription,CodiceOspite,codicecooperativa,CalcoloPassivo,Done) vaLUES (?,?,?,?,?,?,?,?,?,?,?,?) "
        End If

        cmdScrivi.Parameters.AddWithValue("@Data", Data)
        cmdScrivi.Parameters.AddWithValue("@IdAppalto", IdAppalto)
        cmdScrivi.Parameters.AddWithValue("@IdStrutture", IdStrutture)
        cmdScrivi.Parameters.AddWithValue("@IdMansione", IdMansione)
        cmdScrivi.Parameters.AddWithValue("@Ore", Ore)
        cmdScrivi.Parameters.AddWithValue("@CodiceOperatore", CodiceOperatore)

        cmdScrivi.Parameters.AddWithValue("@intypecod", intypecod)
        cmdScrivi.Parameters.AddWithValue("@intypedescription", intypedescription)

        cmdScrivi.Parameters.AddWithValue("@CodiceOspite", CodiceOspite)

        cmdScrivi.Parameters.AddWithValue("@codicecooperativa", codicecooperativa)


        cmdScrivi.Parameters.AddWithValue("@CalcoloPassivo", CalcoloPassivo)


        cmdScrivi.Parameters.AddWithValue("@Done", Done)


        'codicecooperativa

        cmdScrivi.ExecuteNonQuery()

        myPOSTreader.Close()
        cn.Close()

    End Sub


    Function campodb(ByVal oggetto As Object) As String
        If IsDBNull(oggetto) Then
            Return ""
        Else
            Return oggetto
        End If
    End Function

    Function campodbn(ByVal oggetto As Object) As Double
        If IsDBNull(oggetto) Then
            Return 0
        Else
            Return oggetto
        End If
    End Function
    Function campodbd(ByVal oggetto As Object) As Date
        If IsDBNull(oggetto) Then
            Return Nothing
        Else
            Return oggetto
        End If
    End Function

End Class
