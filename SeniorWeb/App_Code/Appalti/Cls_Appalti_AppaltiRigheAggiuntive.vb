﻿Imports Microsoft.VisualBasic
Imports System.Data.OleDb
Imports System.Web.Hosting

Public Class Cls_Appalti_AppaltiRigheAggiuntive
    Public IdAppalto As Integer
    Public Regola As String

    Public Mastro As Integer

    Public Conto As Integer

    Public Sottoconto As String

    Public IVA As String
    Public Quantita As Double
    Public Importo As Double
    Public Struttura As Integer



    Sub loaddati(ByVal StringaConnessione As String, ByVal StringaConnessioneGenerale As String, ByVal IdAppalti As Integer, ByVal Tabella As System.Data.DataTable)
        Dim cn As OleDbConnection


        cn = New Data.OleDb.OleDbConnection(StringaConnessione)

        cn.Open()
        Dim cmd As New OleDbCommand()
        cmd.CommandText = ("select * from Appalti_AppaltiRigheAggiuntive  where IdAppalto = " & IdAppalti)
        cmd.Connection = cn
        Dim sb As StringBuilder = New StringBuilder

        Tabella.Clear()
        Tabella.Columns.Clear()
        Tabella.Columns.Add("Conto", GetType(String))
        Tabella.Columns.Add("Regola", GetType(String))
        Tabella.Columns.Add("IVA", GetType(String))
        Tabella.Columns.Add("Quantita", GetType(String))
        Tabella.Columns.Add("Importo", GetType(String))
        Tabella.Columns.Add("Descrizione", GetType(String))
        Tabella.Columns.Add("Struttura", GetType(String))

        Dim myPOSTreader As OleDbDataReader = cmd.ExecuteReader()
        Do While myPOSTreader.Read

            Dim myriga As System.Data.DataRow = Tabella.NewRow()

            Dim PDC As New Cls_Pianodeiconti

            PDC.Mastro = campodbn(myPOSTreader.Item("Mastro"))
            PDC.Conto = campodbn(myPOSTreader.Item("Conto"))
            PDC.Sottoconto = campodbn(myPOSTreader.Item("Sottoconto"))
            PDC.Decodfica(StringaConnessioneGenerale)


            myriga(0) = PDC.Mastro & " " & PDC.Conto & " " & PDC.Sottoconto & " " & PDC.Descrizione

            myriga(1) = campodb(myPOSTreader.Item("Regola"))

            myriga(2) = campodb(myPOSTreader.Item("IVA"))

            myriga(3) = campodbn(myPOSTreader.Item("Quantita"))

            myriga(4) = Format(campodbn(myPOSTreader.Item("Importo")), "#,##0.00")

            myriga(5) = campodb(myPOSTreader.Item("Descrizione"))


            myriga(6) = campodbn(myPOSTreader.Item("Struttura"))

            Tabella.Rows.Add(myriga)
        Loop
        myPOSTreader.Close()
        If Tabella.Rows.Count = 0 Then
            Dim myriga As System.Data.DataRow = Tabella.NewRow()


            myriga(0) = ""
            myriga(1) = ""
            myriga(2) = ""
            myriga(3) = 0
            myriga(4) = 0
            myriga(5) = ""
            Tabella.Rows.Add(myriga)
        End If
        cn.Close()
    End Sub

    Public Sub AggiornaDaTabella(ByVal StringaConnessione As String, ByVal Tabella As System.Data.DataTable)
        Dim cn As OleDbConnection
        Dim MySql As String

        MySql = ""

        cn = New Data.OleDb.OleDbConnection(StringaConnessione)

        cn.Open()


        Dim Transan As OleDbTransaction = cn.BeginTransaction()

        Dim cmd As New OleDbCommand()
        cmd.CommandText = "Delete from Appalti_AppaltiRigheAggiuntive where IdAppalto = " & IdAppalto
        cmd.Connection = cn
        cmd.Transaction = Transan
        cmd.ExecuteNonQuery()

        Dim i As Long

        For i = 0 To Tabella.Rows.Count - 1
            'If IsDate(Tabella.Rows(i).Item(0)) And Tabella.Rows(i).Item(0) <> "" Then
            MySql = "INSERT INTO Appalti_AppaltiRigheAggiuntive ([IdAppalto],Mastro,Conto,Sottoconto,Regola,IVA,Quantita,Importo,Descrizione,Struttura) VALUES (?,?,?,?,?,?,?,?,?,?)"
            Dim cmdw As New OleDbCommand()
            cmdw.CommandText = (MySql)
            cmdw.Parameters.AddWithValue("@IdAppalto", IdAppalto)


            Dim CampoConto As String
            CampoConto = campodb(Tabella.Rows(i).Item(0))



            Dim Vettore(100) As String

            Vettore = SplitWords(CampoConto)

            Mastro = 0
            Conto = 0
            Sottoconto = 0
            If Vettore.Length >= 3 Then
                Mastro = Val(Vettore(0))
                Conto = Val(Vettore(1))
                Sottoconto = Val(Vettore(2))
            End If

            cmdw.Parameters.AddWithValue("@Mastro", Mastro)
            cmdw.Parameters.AddWithValue("@Conto", Conto)
            cmdw.Parameters.AddWithValue("@Sottoconto", Sottoconto)


            cmdw.Parameters.AddWithValue("@Regola", campodb(Tabella.Rows(i).Item(1)))

            cmdw.Parameters.AddWithValue("@IVA", campodb(Tabella.Rows(i).Item(2)))

            cmdw.Parameters.AddWithValue("@Quantita", campodbn(Tabella.Rows(i).Item(3)))

            cmdw.Parameters.AddWithValue("@Importo", campodbn(Tabella.Rows(i).Item(4)))

            cmdw.Parameters.AddWithValue("@Descrizione", campodb(Tabella.Rows(i).Item(5)))

            cmdw.Parameters.AddWithValue("@Struttura", campodbn(Tabella.Rows(i).Item(6)))

            cmdw.Transaction = Transan
            cmdw.Connection = cn
            cmdw.ExecuteNonQuery()
            'End If
        Next
        Transan.Commit()
        cn.Close()

    End Sub


    Function campodb(ByVal oggetto As Object) As String
        If IsDBNull(oggetto) Then
            Return ""
        Else
            Return oggetto
        End If
    End Function

    Function campodbn(ByVal oggetto As Object) As Double
        If IsDBNull(oggetto) Then
            Return 0
        Else
            Return oggetto
        End If
    End Function

    Private Function SplitWords(ByVal s As String) As String()
        '
        ' Call Regex.Split function from the imported namespace.
        ' Return the result array.
        '
        Return Regex.Split(s, "\W+")
    End Function


End Class
