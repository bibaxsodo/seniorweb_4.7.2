﻿Imports Microsoft.VisualBasic


Public Class Modulo

    Public Shared Function MathRound(ByVal Valore As Double, ByVal Cifre As Integer) As Double
        Return Math.Round(Valore, Cifre, MidpointRounding.AwayFromZero)
    End Function


End Class
