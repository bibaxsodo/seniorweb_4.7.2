﻿Imports Microsoft.VisualBasic
Imports System.Data.OleDb
Imports System.Web.Hosting



Public Class Cls_Rubrica
    Public Id As Long
    Public Appellativo As Long
    Public Denominazione As String
    Public Cognome As String
    Public Nome As String
    Public Indirizzo As String

    Public Provincia As String
    Public Comune As String
    Public Cap As String
    Public Localita As String
    Public Note As String

    Public EMail1 As String
    Public EMail2 As String

    Public Telefono1 As String
    Public Telefono2 As String

    Public Ragruppamento As String
    Public Utente As String
    Public InvioComunicazione As Integer
    Public CONSENSOINSERIMENTO As Integer
    Public CONSENSOMARKETING As Integer



    Sub Scrivi(ByVal StringaConnessione As String)
        Dim cn As OleDbConnection


        cn = New Data.OleDb.OleDbConnection(StringaConnessione)

        cn.Open()

        Dim cmd As New OleDbCommand()
        cmd.CommandText = ("select * from Rubrica where ID = " & Id)
        cmd.Connection = cn
        Dim cmdIns As New OleDbCommand()

        Dim myPOSTreader As OleDbDataReader = cmd.ExecuteReader()
        If Not myPOSTreader.Read Then
            cmdIns.CommandText = ("Insert Into Rubrica (Appellativo,Denominazione,Cognome,Nome, Indirizzo, Provincia, Comune, Cap, Localita, Ragruppamento,Note,DataModifica,Utente,EMail1,EMail2,Telefono1,Telefono2,InvioComunicazione,CONSENSOINSERIMENTO,CONSENSOMARKETING)  VALUES (?,?,?,?, ?, ?, ?, ?, ?, ?,?,?,?,?,?,?,?,?,?,?) ")
        Else
            cmdIns.CommandText = ("UPDATE Rubrica SET Appellativo =?,Denominazione= ?,Cognome = ?,Nome = ? , Indirizzo = ?, Provincia = ?, Comune = ?, Cap = ?, Localita = ?, Ragruppamento = ?,Note = ?,DataModifica =?,Utente=?,EMail1 = ?,EMail2 = ?,Telefono1 = ?,Telefono2 = ?,InvioComunicazione = ?,CONSENSOINSERIMENTO = ?,CONSENSOMARKETING = ?  Where ID = " & Id)
        End If


        cmdIns.Parameters.AddWithValue("@Appellativo", Appellativo)
        cmdIns.Parameters.AddWithValue("@Denominazone", Trim(Mid(Denominazione & Space(160), 1, 149)))
        cmdIns.Parameters.AddWithValue("@Cognome", Trim(Mid(Cognome & Space(160), 1, 99)))

        cmdIns.Parameters.AddWithValue("@Nome", Trim(Mid(Nome & Space(160), 1, 99)))

        cmdIns.Parameters.AddWithValue("@Indirizzo", Trim(Mid(Indirizzo & Space(240), 1, 240)))
        cmdIns.Parameters.AddWithValue("@Provincia", Trim(Mid(Provincia & Space(3), 1, 3)))
        cmdIns.Parameters.AddWithValue("@Comune", Trim(Mid(Comune & Space(3), 1, 3)))
        cmdIns.Parameters.AddWithValue("@Cap", Trim(Mid(Cap & Space(6), 1, 6)))
        cmdIns.Parameters.AddWithValue("@Localita", Trim(Mid(Localita & Space(150), 1, 150)))
        cmdIns.Parameters.AddWithValue("@Ragruppamento", Ragruppamento)
        cmdIns.Parameters.AddWithValue("@Note", Note)



        cmdIns.Parameters.AddWithValue("@DataModifica", Now)

        cmdIns.Parameters.AddWithValue("@Utente", Utente)


        cmdIns.Parameters.AddWithValue("@EMail1", EMail1)
        cmdIns.Parameters.AddWithValue("@EMail2", EMail2)
        cmdIns.Parameters.AddWithValue("@Telefono1", Telefono1)
        cmdIns.Parameters.AddWithValue("@Telefono2", Telefono2)
        cmdIns.Parameters.AddWithValue("@InvioComunicazione", InvioComunicazione)

        cmdIns.Parameters.AddWithValue("@CONSENSOINSERIMENTO", CONSENSOINSERIMENTO)
        cmdIns.Parameters.AddWithValue("@CONSENSOMARKETING", CONSENSOMARKETING)
      
        cmdIns.Connection = cn
        cmdIns.ExecuteNonQuery()

        cn.Close()
    End Sub

    Sub Leggi(ByVal StringaConnessione As String)
        Dim cn As OleDbConnection


        cn = New Data.OleDb.OleDbConnection(StringaConnessione)

        cn.Open()
        Dim cmd As New OleDbCommand()
        cmd.CommandText = ("select * from Rubrica where ID = " & Id)
        cmd.Connection = cn


        Dim Entrato As Boolean = False

        Dim myPOSTreader As OleDbDataReader = cmd.ExecuteReader()
        If myPOSTreader.Read Then
            Appellativo = campodb(myPOSTreader.Item("Appellativo"))
            Denominazione = campodb(myPOSTreader.Item("Denominazione"))
            Cognome = campodb(myPOSTreader.Item("Cognome"))
            Nome = campodb(myPOSTreader.Item("Nome"))
            Indirizzo = campodb(myPOSTreader.Item("Indirizzo"))
            Provincia = campodb(myPOSTreader.Item("Provincia"))
            Comune = campodb(myPOSTreader.Item("Comune"))
            Cap = campodb(myPOSTreader.Item("Cap"))
            Localita = campodb(myPOSTreader.Item("Localita"))
            Note = campodb(myPOSTreader.Item("Note"))
            Ragruppamento = campodb(myPOSTreader.Item("Ragruppamento"))


            EMail1 = campodb(myPOSTreader.Item("EMail1"))
            EMail2 = campodb(myPOSTreader.Item("EMail2"))

            Telefono1 = campodb(myPOSTreader.Item("Telefono1"))
            Telefono2 = campodb(myPOSTreader.Item("Telefono2"))

            InvioComunicazione = Val(campodb(myPOSTreader.Item("InvioComunicazione")))

            CONSENSOINSERIMENTO = Val(campodb(myPOSTreader.Item("CONSENSOINSERIMENTO")))
            CONSENSOMARKETING = Val(campodb(myPOSTreader.Item("CONSENSOMARKETING")))
        End If
        myPOSTreader.Close()
        cn.Close()

        
    End Sub

    Function campodb(ByVal oggetto As Object) As String
        If IsDBNull(oggetto) Then
            Return ""
        Else
            Return oggetto
        End If
    End Function



    Sub Delete(ByVal StringaConnessione As String)
        Dim cn As OleDbConnection


        cn = New Data.OleDb.OleDbConnection(StringaConnessione)

        cn.Open()
        Dim cmd As New OleDbCommand()
        cmd.CommandText = ("Delete from Rubrica where ID = " & Id)
        cmd.Connection = cn
        cmd.ExecuteNonQuery()

        cn.Close()


    End Sub



    Sub UpCheckBoxList(ByVal StringaConnessione As String, ByVal Raggruppamento As Integer, ByRef appoggio As CheckBoxList)
        Dim cn As OleDbConnection

        cn = New Data.OleDb.OleDbConnection(StringaConnessione)

        cn.Open()
        Dim cmd As New OleDbCommand()
        If Raggruppamento = 0 Then
            cmd.CommandText = ("select * from Rubrica Order By Denominazione,Cognome,Nome")
        Else
            cmd.CommandText = ("select * from Rubrica Where Ragruppamento Like '%<" & Raggruppamento & ">%' Order By Denominazione,Cognome,Nome")
        End If

        cmd.Connection = cn

        Dim Entrato As Boolean = False

        appoggio.Items.Clear()
        Dim myPOSTreader As OleDbDataReader = cmd.ExecuteReader()
        Do While myPOSTreader.Read
            appoggio.Items.Add(campodb(myPOSTreader.Item("Denominazione")) & "  -  " & campodb(myPOSTreader.Item("Cognome")) & " " & campodb(myPOSTreader.Item("Nome")))
            appoggio.Items(appoggio.Items.Count - 1).Value = myPOSTreader.Item("ID")
        Loop
        myPOSTreader.Close()
        cn.Close()
 
    End Sub


    Sub UpDropDownList(ByVal StringaConnessione As String, ByVal Raggruppamento As Integer, ByRef appoggio As DropDownList)
        Dim cn As OleDbConnection

        cn = New Data.OleDb.OleDbConnection(StringaConnessione)

        cn.Open()
        Dim cmd As New OleDbCommand()
        If Raggruppamento = 0 Then
            cmd.CommandText = ("select * from Rubrica Order By Denominazione,Cognome,Nome")
        Else
            cmd.CommandText = ("select * from Rubrica Where Ragruppamento Like '%<" & Raggruppamento & ">%' Order By Denominazione,Cognome,Nome")
        End If

        cmd.Connection = cn

        Dim Entrato As Boolean = False

        appoggio.Items.Clear()
        Dim myPOSTreader As OleDbDataReader = cmd.ExecuteReader()
        Do While myPOSTreader.Read
            appoggio.Items.Add(campodb(myPOSTreader.Item("Denominazione")) & "  -  " & campodb(myPOSTreader.Item("Cognome")) & " " & campodb(myPOSTreader.Item("Nome")))
            appoggio.Items(appoggio.Items.Count - 1).Value = myPOSTreader.Item("ID")
        Loop
        myPOSTreader.Close()
        cn.Close()

    End Sub
End Class
