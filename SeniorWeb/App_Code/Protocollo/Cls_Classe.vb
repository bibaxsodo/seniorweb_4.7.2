﻿Imports Microsoft.VisualBasic
Imports System.Data.OleDb
Imports System.Web.Hosting


Public Class Cls_Classe
    Public idClasse As Long
    Public Descrizione As String
    Public idTitolo As Long

    Sub loaddati(ByVal StringaConnessione As String, ByVal Tabella As System.Data.DataTable)
        Dim cn As OleDbConnection

        cn = New Data.OleDb.OleDbConnection(StringaConnessione)
        cn.Open()
        Dim cmd As New OleDbCommand()
        cmd.CommandText = ("select * from TabellaClasse Order by idTitolo,idClasse")
        cmd.Connection = cn

        Tabella.Clear()
        Tabella.Columns.Clear()
        Tabella.Columns.Add("idClasse", GetType(String))
        Tabella.Columns.Add("Descrizione", GetType(String))
        Tabella.Columns.Add("idTitolo", GetType(String))


        Dim Entrato As Boolean = False

        Dim myPOSTreader As OleDbDataReader = cmd.ExecuteReader()
        Do While myPOSTreader.Read

            Dim myriga As System.Data.DataRow = Tabella.NewRow()
            myriga(0) = myPOSTreader.Item("idClasse")

            myriga(1) = myPOSTreader.Item("Descrizione")
            myriga(2) = Val(campodb(myPOSTreader.Item("idTitolo")))
            Tabella.Rows.Add(myriga)

            Entrato = True
        Loop
        myPOSTreader.Close()
        cn.Close()

        If Entrato = False Then
            Dim myriga As System.Data.DataRow = Tabella.NewRow()
            myriga(0) = 0
            myriga(1) = ""
            Tabella.Rows.Add(myriga)
        End If
    End Sub

    Sub ScriviTabella(ByVal StringaConnessione As String, ByVal Tabella As System.Data.DataTable)
        Dim cn As OleDbConnection
        Dim transaction As OleDbTransaction

        cn = New Data.OleDb.OleDbConnection(StringaConnessione)

        cn.Open()

        transaction = cn.BeginTransaction()

        Try

            Dim cmd As New OleDbCommand()
            cmd.CommandText = ("delete from TabellaClasse")
            cmd.Connection = cn
            cmd.Transaction = transaction
            cmd.ExecuteNonQuery()

            Dim i As Long

            For i = 0 To Tabella.Rows.Count - 1
                If Tabella.Rows(i).Item(1).ToString.Trim <> "" Then
                    Dim cmdIns As New OleDbCommand()
                    cmdIns.CommandText = ("Insert Into TabellaClasse (idClasse,Descrizione,IdTitolo)  VALUES (?,?,?) ")

                    If Val(campodb(Tabella.Rows(i).Item(0))) > 0 Then
                        cmdIns.Parameters.AddWithValue("@IdGruppo", Val(Tabella.Rows(i).Item(0)))
                    Else
                        Dim cmdMax As New OleDbCommand()
                        cmdMax.CommandText = ("select MAX(idClasse) from TabellaClasse")
                        cmdMax.Connection = cn
                        cmdMax.Transaction = transaction
                        Dim RDMax As OleDbDataReader = cmdMax.ExecuteReader()
                        If RDMax.Read Then
                            cmdIns.Parameters.AddWithValue("@idClasse", Val(campodb(RDMax.Item(0))) + 1)
                        End If
                        RDMax.Close()
                    End If
                    cmdIns.Parameters.AddWithValue("@Descrizione", Tabella.Rows(i).Item(1))
                    cmdIns.Parameters.AddWithValue("@IdTitolo", Tabella.Rows(i).Item(2))
                    cmdIns.Transaction = transaction
                    cmdIns.Connection = cn
                    cmdIns.ExecuteNonQuery()
                End If
            Next
            transaction.Commit()
        Catch ex As Exception
            transaction.Rollback()
        End Try

        cn.Close()
    End Sub


    Function campodb(ByVal oggetto As Object) As String
        If IsDBNull(oggetto) Then
            Return ""
        Else
            Return oggetto
        End If
    End Function
    Sub UpDropDownListClasse(ByVal StringaConnessione As String, ByRef appoggio As DropDownList, ByVal IdTtitolo As Integer)
        Dim cn As OleDbConnection

        cn = New Data.OleDb.OleDbConnection(StringaConnessione)

        cn.Open()
        Dim cmd As New OleDbCommand()
        cmd.CommandText = ("select * from TabellaClasse Where IdTitolo = ? order by  idClasse")
        cmd.Parameters.AddWithValue("@IdTtitolo", IdTtitolo)
        cmd.Connection = cn

        Dim Entrato As Boolean = False

        appoggio.Items.Clear()
        Dim myPOSTreader As OleDbDataReader = cmd.ExecuteReader()
        Do While myPOSTreader.Read
            appoggio.Items.Add(myPOSTreader.Item("idClasse") & " - " & myPOSTreader.Item("Descrizione"))
            appoggio.Items(appoggio.Items.Count - 1).Value = myPOSTreader.Item("idClasse")
        Loop
        myPOSTreader.Close()
        cn.Close()

        If appoggio.Items.Count > 0 Then
            appoggio.Items(0).Selected = True
        End If
        'appoggio.Items.Add("")
        'appoggio.Items(appoggio.Items.Count - 1).Value = ""
        'appoggio.Items(appoggio.Items.Count - 1).Selected = True
    End Sub

    Sub UpDropDownList(ByVal StringaConnessione As String, ByRef appoggio As DropDownList)
        Dim cn As OleDbConnection

        cn = New Data.OleDb.OleDbConnection(StringaConnessione)

        cn.Open()
        Dim cmd As New OleDbCommand()
        cmd.CommandText = ("select * from TabellaClasse")
        cmd.Connection = cn

        Dim Entrato As Boolean = False

        appoggio.Items.Clear()
        Dim myPOSTreader As OleDbDataReader = cmd.ExecuteReader()
        Do While myPOSTreader.Read
            If myPOSTreader.Item("idClasse") > 0 Then
                appoggio.Items.Add(myPOSTreader.Item("idClasse") & " - " & myPOSTreader.Item("Descrizione"))
            Else
                appoggio.Items.Add(myPOSTreader.Item("Descrizione"))
            End If
            appoggio.Items(appoggio.Items.Count - 1).Value = myPOSTreader.Item("idClasse")
        Loop
        myPOSTreader.Close()
        cn.Close()
        If appoggio.Items.Count > 0 Then
            appoggio.Items(0).Selected = True
        End If
        'appoggio.Items.Add("")
        'appoggio.Items(appoggio.Items.Count - 1).Value = ""
        'appoggio.Items(appoggio.Items.Count - 1).Selected = True
    End Sub


    Sub Leggi(ByVal StringaConnessione As String)
        Dim cn As OleDbConnection


        cn = New Data.OleDb.OleDbConnection(StringaConnessione)

        cn.Open()
        Dim cmd As New OleDbCommand()
        cmd.CommandText = ("select * from TabellaClasse where idClasse = " & idClasse & " And IdTitolo = " & idTitolo)
        cmd.Connection = cn


        Dim Entrato As Boolean = False

        Dim myPOSTreader As OleDbDataReader = cmd.ExecuteReader()
        If myPOSTreader.Read Then
            Descrizione = campodb(myPOSTreader.Item("Descrizione"))
        End If
        myPOSTreader.Close()
        cn.Close()
    End Sub


End Class
