﻿Imports System.Web
Imports System.Web.Services
Imports System.Web.Services.Protocols
Imports System.Web.Hosting

' To allow this Web Service to be called from script, using ASP.NET AJAX, uncomment the following line.
' <System.Web.Script.Services.ScriptService()> _
<WebService(Namespace:="http://tempuri.org/")> _
<WebServiceBinding(ConformsTo:=WsiProfiles.BasicProfile1_1)> _
<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Public Class Uploadprotocollo_allegati
     Inherits System.Web.Services.WebService

    <WebMethod()> _
    Public Function VerificaCodice(ByVal SecurCode As String, ByVal Anno As Integer, ByVal Tipo As String, ByVal Codice As Integer) As String
        If SecurCode = "Sepiolle76" Then
            VerificaCodice = "NF"
            Exit Function
        End If

        Dim Verifica As New Cls_Protocollo

        Verifica.Oggetto = ""
        Verifica.Anno = Anno
        Verifica.Tipo = Tipo
        Verifica.Numero = Codice
        Verifica.Leggi(Session("DC_TABELLE"))

        If Verifica.Oggetto = "" Then
            VerificaCodice = "NF"
            Exit Function
        End If

        VerificaCodice = "OK"




    End Function


End Class
