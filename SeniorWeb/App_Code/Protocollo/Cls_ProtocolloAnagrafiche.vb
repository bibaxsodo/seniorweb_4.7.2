﻿Imports Microsoft.VisualBasic
Imports System.Data.OleDb
Imports System.Web.Hosting

Public Class Cls_ProtocolloAnagrafiche
    Public Anno As Long
    Public Numero As Long
    Public Tipo As String
    Public IdRubrica(1000) As Long
    Public IdClienteFornitore(1000) As Long
    Public IdAnagrafica(1000) As Long
    Public IdOspite(1000) As Long
    Public IdParente(1000) As Long

    Function campodb(ByVal oggetto As Object) As String
        If IsDBNull(oggetto) Then
            Return ""
        Else
            Return oggetto
        End If
    End Function





    Sub Leggi(ByVal StringaConnessione As String)
        Dim cn As OleDbConnection
        Dim Max As Long = 0

        For Max = 0 To 1000
            IdRubrica(Max) = 0
            IdClienteFornitore(Max) = 0
            IdAnagrafica(Max) = 0
        Next

        cn = New Data.OleDb.OleDbConnection(StringaConnessione)

        cn.Open()
        Dim cmd As New OleDbCommand()
        cmd.CommandText = ("select * from ProtocolloAnagrafiche where Anno = " & Anno & " And Numero = " & Numero & " And Tipo = '" & Tipo & "'")
        cmd.Connection = cn


        Dim Entrato As Boolean = False

        Max = 0
        Dim myPOSTreader As OleDbDataReader = cmd.ExecuteReader()
        Do While myPOSTreader.Read

            IdRubrica(Max) = Val(campodb(myPOSTreader.Item("IdRubrica")))
            IdClienteFornitore(Max) = Val(campodb(myPOSTreader.Item("IdClienteFornitore")))
            IdOspite(Max) = Val(campodb(myPOSTreader.Item("IdOspite")))
            IdParente(Max) = Val(campodb(myPOSTreader.Item("IdParente")))

            Max = Max + 1
        Loop
        myPOSTreader.Close()
        cn.Close()
    End Sub

    Sub Scrivi(ByVal StringaConnessione As String)
        Dim cn As OleDbConnection
        Dim Max As Long = 0


        cn = New Data.OleDb.OleDbConnection(StringaConnessione)

        cn.Open()

        Dim Transan As OleDbTransaction = cn.BeginTransaction()



        Dim cmdDelete As New OleDbCommand()
        cmdDelete.CommandText = ("Delete from ProtocolloAnagrafiche where Anno = " & Anno & " And Numero = " & Numero & " And Tipo = '" & Tipo & "'")
        cmdDelete.Connection = cn
        cmdDelete.Transaction = Transan
        cmdDelete.ExecuteNonQuery()




        For Max = 0 To 1000
            If IdRubrica(Max) > 0 Or IdClienteFornitore(Max) > 0 Or IdAnagrafica(Max) > 0 Or IdOspite(Max) > 0 Or IdParente(Max) > 0 Then
                Dim cmd As New OleDbCommand()
                cmd.CommandText = ("INSERT INTO ProtocolloAnagrafiche (Anno,Numero,Tipo,IdRubrica,IdClienteFornitore,IdAnagrafica,IdOspite,IdParente) VALUES (?,?,?,?,?,?,?,?)")
                cmd.Connection = cn
                cmd.Transaction = Transan
                cmd.Parameters.AddWithValue("@Anno", Anno)
                cmd.Parameters.AddWithValue("@Anno", Numero)
                cmd.Parameters.AddWithValue("@Anno", Tipo)
                cmd.Parameters.AddWithValue("@IdRubrica", IdRubrica(Max))
                cmd.Parameters.AddWithValue("@IdClienteFornitore", IdClienteFornitore(Max))
                cmd.Parameters.AddWithValue("@IdAnagrafica", IdAnagrafica(Max))
                cmd.Parameters.AddWithValue("@IdOspite", IdOspite(Max))
                cmd.Parameters.AddWithValue("@IdParente", IdParente(Max))
                cmd.ExecuteNonQuery()
            End If
        Next
        Transan.Commit()
        cn.Close()
    End Sub

End Class
