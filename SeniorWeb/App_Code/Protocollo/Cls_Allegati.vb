﻿Imports Microsoft.VisualBasic
Imports System.Data.OleDb
Imports System.Web.Hosting

Public Class Cls_Allegati
    Public Anno As Long
    Public NumeroProtocollo As Long
    Public Tipo As String
    Public NomeFile As String
    Public DATA As Date
    Public DESCRIZIONE As String


    Function campodb(ByVal oggetto As Object) As String
        If IsDBNull(oggetto) Then
            Return ""
        Else
            Return oggetto
        End If
    End Function

    Sub Leggi(ByVal StringaConnessione As String)
        Dim cn As OleDbConnection



        DATA = Nothing
        DESCRIZIONE = ""
        cn = New Data.OleDb.OleDbConnection(StringaConnessione)

        cn.Open()
        Dim cmd As New OleDbCommand()
        cmd.CommandText = ("select * from Allegati where " & _
                           "Anno = ? And NumeroProtocollo = ? And Tipo = ? And NomeFile = ?")
        cmd.Parameters.AddWithValue("@Anno", Anno)
        cmd.Parameters.AddWithValue("@NumeroProtocollo", NumeroProtocollo)
        cmd.Parameters.AddWithValue("@Tipo", Tipo)
        cmd.Parameters.AddWithValue("@NomeFile", NomeFile)
        cmd.Connection = cn
        Dim sb As StringBuilder = New StringBuilder

        Dim myPOSTreader As OleDbDataReader = cmd.ExecuteReader()
        If myPOSTreader.Read Then            
            Anno = Val(campodb(myPOSTreader.Item("Anno")))
            NumeroProtocollo = Val(campodb(myPOSTreader.Item("NumeroProtocollo")))
            Tipo = campodb(myPOSTreader.Item("Tipo"))
            NomeFile = campodb(myPOSTreader.Item("NomeFile"))
            DESCRIZIONE = campodb(myPOSTreader.Item("Descrizione"))
            DATA = Nothing
            If campodb(myPOSTreader.Item("Data")) <> "" Then
                DATA = campodb(myPOSTreader.Item("Data"))
            End If
        End If
        myPOSTreader.Close()
        cn.Close()
    End Sub


    Sub Scrivi(ByVal StringaConnessione As String, ByVal MyTable As System.Data.DataTable)
        Dim cn As OleDbConnection
        Dim I As Long


        cn = New Data.OleDb.OleDbConnection(StringaConnessione)

        cn.Open()

        Dim cmdDelete As New OleDbCommand()

        cmdDelete.CommandText = ("delete  from Allegati where " & _
                   "Anno = ? And NumeroProtocollo = ? And Tipo = ?")
        cmdDelete.Parameters.AddWithValue("@Anno", Anno)
        cmdDelete.Parameters.AddWithValue("@NumeroProtocollo", NumeroProtocollo)
        cmdDelete.Parameters.AddWithValue("@Tipo", Tipo)
        cmdDelete.Connection = cn
        cmdDelete.ExecuteNonQuery()


        For I = 0 To MyTable.Rows.Count - 1
            If Not IsDBNull(MyTable.Rows(I).Item(0)) Then
                If Trim(MyTable.Rows(I).Item(0)) <> "" Then
                    Dim cmd As New OleDbCommand()



                    cmd.CommandText = ("INSERT INTO Allegati (Anno,NumeroProtocollo,Tipo,NomeFile,DESCRIZIONE,DATA)" & _
                                       " VALUES (?,?,?,?,?,?)")
                    cmd.Parameters.AddWithValue("@Anno", Anno)
                    cmd.Parameters.AddWithValue("@NumeroProtocollo", NumeroProtocollo)
                    cmd.Parameters.AddWithValue("@Tipo", Tipo)
                    cmd.Parameters.AddWithValue("@NomeFile", MyTable.Rows(I).Item(0))
                    cmd.Parameters.AddWithValue("@Descrizione", MyTable.Rows(I).Item(1))

                    If IsDate(MyTable.Rows(I).Item(2)) Then
                        Dim DataDoc As Date = MyTable.Rows(I).Item(2)
                        cmd.Parameters.AddWithValue("@Data", DataDoc)
                    Else
                        cmd.Parameters.AddWithValue("@Data", Nothing)
                    End If
                    cmd.Connection = cn
                    cmd.ExecuteNonQuery()
                End If
            End If
        Next

        cn.Close()
    End Sub
End Class
