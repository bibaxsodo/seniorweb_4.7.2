Imports Microsoft.VisualBasic
Imports System.Data.OleDb
Imports System.Web.Hosting

Public Class Cls_Movimenti


    Public CENTROSERVIZIO As String
    Public CodiceOspite As Long
    Public TipoMov As String
    Public Progressivo As Long
    Public Data As Date
    Public Causale As String
    Public Descrizione As String
    Public Utente As String
    Public EPersonam As Long
    Public DataOra As DateTime
    
    Function campodbN(ByVal oggetto As Object) As Double
        If IsDBNull(oggetto) Then
            Return 0
        Else
            Return oggetto
        End If
    End Function
    Function campodb(ByVal oggetto As Object) As String
        If IsDBNull(oggetto) Then
            Return ""
        Else
            Return oggetto
        End If
    End Function
    Function campodbD(ByVal oggetto As Object) As Date
        If IsDBNull(oggetto) Then
            Return Nothing
        Else
            Return oggetto
        End If
    End Function

    Function CServizioUsato(ByVal StringaConnessione As String) As Boolean
        Dim cn As OleDbConnection


        cn = New Data.OleDb.OleDbConnection(StringaConnessione)

        CServizioUsato = False
        cn.Open()
        Dim cmd As New OleDbCommand()
        cmd.CommandText = ("select * from MOVIMENTI where centroservizio = '" & CENTROSERVIZIO & "' And CodiceOspite = " & CodiceOspite)
        cmd.Connection = cn

        Dim myPOSTreader As OleDbDataReader = cmd.ExecuteReader()
        If myPOSTreader.Read Then
            CServizioUsato = True
        End If
        myPOSTreader.Close()
        cn.Close()

    End Function

    Sub loaddati(ByVal StringaConnessione As String, ByVal codiceospite As Integer, ByVal centroservizio As String, ByVal Tabella As System.Data.DataTable, Optional ByVal NumeroMovimenti As Integer = 0)
        Dim cn As OleDbConnection


        cn = New Data.OleDb.OleDbConnection(StringaConnessione)

        cn.Open()
        Dim cmd As New OleDbCommand()


        If NumeroMovimenti > 100 Then
            cmd.CommandText = ("select * from MOVIMENTI where centroservizio = '" & centroservizio & "' And CodiceOspite = " & codiceospite & " And Data >= ? ORDER BY DATA,Progressivo ")
            cmd.Parameters.AddWithValue("@DataFiltro", DateSerial(Year(Now) - 1, 1, 1))
        Else
            cmd.CommandText = ("select * from MOVIMENTI where centroservizio = '" & centroservizio & "' And CodiceOspite = " & codiceospite & " ORDER BY DATA,Progressivo ")
        End If

        cmd.Connection = cn
        Dim sb As StringBuilder = New StringBuilder

        Tabella.Clear()
        Tabella.Columns.Clear()
        Tabella.Columns.Add("Data", GetType(String))
        Tabella.Columns.Add("Tipo", GetType(String))
        Tabella.Columns.Add("Causale", GetType(String))
        Tabella.Columns.Add("Descrizione", GetType(String))
        Tabella.Columns.Add("PROGRESSIVO", GetType(Long))
        Tabella.Columns.Add("DataOra", GetType(Date))

        Dim myPOSTreader As OleDbDataReader = cmd.ExecuteReader()
        Do While myPOSTreader.Read

            Dim myriga As System.Data.DataRow = Tabella.NewRow()
            myriga(0) = Format(campodbD(myPOSTreader.Item("DATA")), "dd/MM/yyyy")

            If campodb(myPOSTreader.Item("TIPOMOV")) = "05" Then
                myriga(1) = "Accoglimento"
            End If
            If campodb(myPOSTreader.Item("TIPOMOV")) = "13" Then
                myriga(1) = "Uscita Definitiva"
            End If
            If campodb(myPOSTreader.Item("TIPOMOV")) = "03" Then
                myriga(1) = "Uscita"
            End If
            If campodb(myPOSTreader.Item("TIPOMOV")) = "04" Then
                myriga(1) = "Entrata"
            End If
            myriga(1) = campodb(myPOSTreader.Item("TIPOMOV"))


            myriga(2) = campodb(myPOSTreader.Item("CAUSALE"))
            myriga(3) = campodb(myPOSTreader.Item("DESCRIZIONE"))
            myriga(4) = campodbN(myPOSTreader.Item("PROGRESSIVO"))

            myriga(5) = campodbD(myPOSTreader.Item("DataOra"))



            Tabella.Rows.Add(myriga)
        Loop
        myPOSTreader.Close()
        cn.Close()
    End Sub

    Public Sub EliminaAll(ByVal StringaConnessione As String, Optional ByVal NumeroMovimenti As Integer = 0)
        Dim cn As OleDbConnection
        Dim MySql As String

        MySql = ""

        cn = New Data.OleDb.OleDbConnection(StringaConnessione)

        cn.Open()
        Dim cmd As New OleDbCommand()


        If NumeroMovimenti > 100 Then
            cmd.CommandText = "Delete from Movimenti where CentroServizio = '" & CENTROSERVIZIO & "' And  CodiceOspite = " & CodiceOspite & " And Data >= ?"
            cmd.Parameters.AddWithValue("@DataFiltro", DateSerial(Year(Now) - 1, 1, 1))
        Else
            cmd.CommandText = "Delete from Movimenti where CentroServizio = '" & CENTROSERVIZIO & "' And  CodiceOspite = " & CodiceOspite
        End If


        cmd.Connection = cn
        cmd.ExecuteNonQuery()
        cn.Close()
    End Sub

    Public Sub Elimina(ByVal StringaConnessione As String)
        Dim cn As OleDbConnection
        Dim MySql As String

        MySql = ""

        cn = New Data.OleDb.OleDbConnection(StringaConnessione)

        cn.Open()
        Dim cmd As New OleDbCommand()
        cmd.CommandText = ("Delete from Movimenti where CentroServizio = '" & CENTROSERVIZIO & "' And  CodiceOspite = " & CodiceOspite & " And Data = ?")
        cmd.Parameters.AddWithValue("@Data", Data)
        cmd.Connection = cn
        cmd.ExecuteNonQuery()
        cn.Close()
    End Sub

    Public Sub InserisciDB(ByVal StringaConnessione As String)
        Dim cn As OleDbConnection
        Dim MySql As String

        MySql = ""

        cn = New Data.OleDb.OleDbConnection(StringaConnessione)

        cn.Open()

        If Progressivo = 0 Then
            Dim cmd1 As New OleDbCommand()
            cmd1.CommandText = ("select MAX(Progressivo) from Movimenti where CentroServizio = '" & CENTROSERVIZIO & "' And  CodiceOspite = " & CodiceOspite)
            cmd1.Connection = cn
            Dim myPOSTreader1 As OleDbDataReader = cmd1.ExecuteReader()
            If myPOSTreader1.Read Then
                If Not IsDBNull(myPOSTreader1.Item(0)) Then
                    Progressivo = myPOSTreader1.Item(0) + 1
                Else
                    Progressivo = 1
                End If
            Else
                Progressivo = 1
            End If
        End If

        MySql = "INSERT INTO Movimenti (Utente,DataAggiornamento,CentroServizio,CodiceOspite,Data,TIPOMOV,PROGRESSIVO,CAUSALE,DESCRIZIONE,EPersonam,DataOra) VALUES (?,?,?,?,?,?,?,?,?,?,?)"
        Dim cmdw As New OleDbCommand()
        cmdw.CommandText = (MySql)
        cmdw.Parameters.AddWithValue("@Utente", Utente)
        cmdw.Parameters.AddWithValue("@DataAggiornamento", Now)
        cmdw.Parameters.AddWithValue("@CentroServizio", CENTROSERVIZIO)
        cmdw.Parameters.AddWithValue("@CodiceOspite", CodiceOspite)
        cmdw.Parameters.AddWithValue("@Data", Data)
        cmdw.Parameters.AddWithValue("@TIPOMOV", TipoMov)
        cmdw.Parameters.AddWithValue("@PROGRESSIVO", Progressivo)
        cmdw.Parameters.AddWithValue("@CAUSALE", Causale)
        cmdw.Parameters.AddWithValue("@Descrizione", Trim(Mid(Descrizione & Space(70),1,50)))
        cmdw.Parameters.AddWithValue("@EPersonam", 0)
        cmdw.Parameters.AddWithValue("@DataOra", IIf(Year(DataOra) > 1, DataOra, System.DBNull.Value))
        cmdw.Connection = cn
        cmdw.ExecuteNonQuery()


        cn.Close()
    End Sub
    Public Sub AggiornaDB(ByVal StringaConnessione As String)
        Dim cn As OleDbConnection
        Dim MySql As String

        MySql = ""

        cn = New Data.OleDb.OleDbConnection(StringaConnessione)

        cn.Open()
        Dim cmd As New OleDbCommand()
        cmd.CommandText = ("select * from Movimenti where CentroServizio = '" & CENTROSERVIZIO & "' And  CodiceOspite = " & CodiceOspite & " And Data = ?")
        cmd.Parameters.AddWithValue("@Data", Data)
        cmd.Connection = cn
        Dim myPOSTreader As OleDbDataReader = cmd.ExecuteReader()
        If myPOSTreader.Read Then
            MySql = "UPDATE Movimenti SET Utente = ?,DataAggiornamento = ?,TIPOMOV = ?,PROGRESSIVO=?,CAUSALE=?,DESCRIZIONE=?,EPersonam =?,DataOra = ? WHERE  CentroServizio = '" & CENTROSERVIZIO & "' And  CodiceOspite = " & CodiceOspite & " And Data = ?"
            Dim cmdw As New OleDbCommand()
            cmdw.CommandText = (MySql)
            cmdw.Parameters.AddWithValue("@Utente", Utente)
            cmdw.Parameters.AddWithValue("@DataAggiornamento", Now)
            cmdw.Parameters.AddWithValue("@TIPOMOV", TipoMov)
            cmdw.Parameters.AddWithValue("@PROGRESSIVO", Progressivo)
            cmdw.Parameters.AddWithValue("@CAUSALE", Causale)
            cmdw.Parameters.AddWithValue("@Descrizione", Descrizione)
            cmdw.Parameters.AddWithValue("@EPersonam", 0)
            cmdw.Parameters.AddWithValue("@DataOra", IIf(Year(DataOra) > 1, DataOra, System.DBNull.Value))
            cmdw.Parameters.AddWithValue("@Data", Data)
            cmdw.Connection = cn
            cmdw.ExecuteNonQuery()
        Else
            If Progressivo = 0 Then
                Dim cmd1 As New OleDbCommand()
                cmd1.CommandText = ("select MAX(Progressivo) from Movimenti where CentroServizio = '" & CENTROSERVIZIO & "' And  CodiceOspite = " & CodiceOspite)
                cmd1.Connection = cn
                Dim myPOSTreader1 As OleDbDataReader = cmd1.ExecuteReader()
                If myPOSTreader1.Read Then
                    If Not IsDBNull(myPOSTreader1.Item(0)) Then
                        Progressivo = myPOSTreader1.Item(0) + 1
                    Else
                        Progressivo = 1
                    End If
                Else
                    Progressivo = 1
                End If
            End If

            MySql = "INSERT INTO Movimenti (Utente,DataAggiornamento,CentroServizio,CodiceOspite,Data,TIPOMOV,PROGRESSIVO,CAUSALE,DESCRIZIONE,EPersonam,DataOra) VALUES (?,?,?,?,?,?,?,?,?,?,?)"
            Dim cmdw As New OleDbCommand()
            cmdw.CommandText = (MySql)
            cmdw.Parameters.AddWithValue("@Utente", Utente)
            cmdw.Parameters.AddWithValue("@DataAggiornamento", Now)
            cmdw.Parameters.AddWithValue("@CentroServizio", CENTROSERVIZIO)
            cmdw.Parameters.AddWithValue("@CodiceOspite", CodiceOspite)
            cmdw.Parameters.AddWithValue("@Data", Data)
            cmdw.Parameters.AddWithValue("@TIPOMOV", TipoMov)
            cmdw.Parameters.AddWithValue("@PROGRESSIVO", Progressivo)
            cmdw.Parameters.AddWithValue("@CAUSALE", Causale)
            cmdw.Parameters.AddWithValue("@Descrizione", Descrizione)
            cmdw.Parameters.AddWithValue("@EPersonam", 0)
            cmdw.Parameters.AddWithValue("@DataOra", IIf(Year(DataOra) > 1, DataOra, System.DBNull.Value))
            cmdw.Connection = cn
            cmdw.ExecuteNonQuery()
        End If
        myPOSTreader.Close()
        cn.Close()
    End Sub

   Public Sub UltimaDataAccoglimentoConQuotaSanitaria(ByVal StringaConnessione As String)
        Dim cn As OleDbConnection



        cn = New Data.OleDb.OleDbConnection(StringaConnessione)

        cn.Open()
        Dim cmd As New OleDbCommand()

        Dim SubSelect As String

        SubSelect = "(Select top 1 Usl from STATOAUTO Where STATOAUTO.CODICEOSPITE = MOVIMENTI.CODICEOSPITE and STATOAUTO.CENTROSERVIZIO = MOVIMENTI.CENTROSERVIZIO and STATOAUTO.DATA <= MOVIMENTI.DATA order by STATOAUTO.DATA desc)"
        cmd.CommandText = "select * from MOVIMENTI where CodiceOspite = " & CodiceOspite & " And CENTROSERVIZIO = '" & CENTROSERVIZIO & "' And TipoMov = '05' And " & SubSelect & " <> '' Order by Data Desc"
        cmd.Connection = cn
        Dim sb As StringBuilder = New StringBuilder

        Dim myPOSTreader As OleDbDataReader = cmd.ExecuteReader()
        If myPOSTreader.Read Then
            CENTROSERVIZIO = campodb(myPOSTreader.Item("CENTROSERVIZIO"))
            CodiceOspite = campodbN(myPOSTreader.Item("CodiceOspite"))
            TipoMov = campodb(myPOSTreader.Item("TipoMov"))
            Progressivo = campodbN(myPOSTreader.Item("Progressivo"))
            Data = campodbD(myPOSTreader.Item("Data"))
            Causale = campodb(myPOSTreader.Item("Causale"))
            Descrizione = campodb(myPOSTreader.Item("Descrizione"))
            EPersonam = campodbN(myPOSTreader.Item("EPersonam"))
            DataOra = campodbD(myPOSTreader.Item("DataOra"))
        End If
        myPOSTreader.Close()
        cn.Close()
    End Sub

    Public Sub UltimaDataUscitaDefinitivaQuotaSanitaria(ByVal StringaConnessione As String)
        Dim cn As OleDbConnection

        
        cn = New Data.OleDb.OleDbConnection(StringaConnessione)

        cn.Open()
        Dim cmd As New OleDbCommand()
        Dim SubSelect As String

        SubSelect = "(Select top 1 Usl from STATOAUTO Where STATOAUTO.CODICEOSPITE = MOVIMENTI.CODICEOSPITE and STATOAUTO.CENTROSERVIZIO = MOVIMENTI.CENTROSERVIZIO and STATOAUTO.DATA <  MOVIMENTI.DATA order by STATOAUTO.DATA desc)"
        cmd.CommandText = ("select * from MOVIMENTI where CodiceOspite = " & CodiceOspite & " And CENTROSERVIZIO = '" & CENTROSERVIZIO & "' And TipoMov = '13' And " & SubSelect & " <> '' Order by Data Desc")
        cmd.Connection = cn
        Dim sb As StringBuilder = New StringBuilder

        Dim myPOSTreader As OleDbDataReader = cmd.ExecuteReader()
        If myPOSTreader.Read Then
            CENTROSERVIZIO = campodb(myPOSTreader.Item("CENTROSERVIZIO"))
            CodiceOspite = campodbN(myPOSTreader.Item("CodiceOspite"))
            TipoMov = campodb(myPOSTreader.Item("TipoMov"))
            Progressivo = campodbN(myPOSTreader.Item("Progressivo"))
            Data = campodb(myPOSTreader.Item("Data"))
            Causale = campodb(myPOSTreader.Item("Causale"))
            Descrizione = campodb(myPOSTreader.Item("Descrizione"))
            EPersonam = campodbN(myPOSTreader.Item("EPersonam"))
            DataOra = campodbD(myPOSTreader.Item("DataOra"))
        End If
        myPOSTreader.Close()
        cn.Close()
    End Sub


    Public Sub PrimoAccoglimentoDefinitivaMese(ByVal StringaConnessione As String, ByVal Anno As Integer, ByVal Mese As Integer)
        Dim cn As OleDbConnection



        cn = New Data.OleDb.OleDbConnection(StringaConnessione)

        cn.Open()
        Dim cmd As New OleDbCommand()
        cmd.CommandText = ("select * from MOVIMENTI where CodiceOspite = " & CodiceOspite & " And CENTROSERVIZIO = '" & CENTROSERVIZIO & "' And TipoMov = '05' And year(Data) = " & Anno & " and month(data) = " & Mese & "  Order by Data ")
        cmd.Connection = cn
        Dim sb As StringBuilder = New StringBuilder

        Dim myPOSTreader As OleDbDataReader = cmd.ExecuteReader()
        If myPOSTreader.Read Then
            CENTROSERVIZIO = campodb(myPOSTreader.Item("CENTROSERVIZIO"))
            CodiceOspite = campodbN(myPOSTreader.Item("CodiceOspite"))
            TipoMov = campodb(myPOSTreader.Item("TipoMov"))
            Progressivo = campodbN(myPOSTreader.Item("Progressivo"))
            Data = campodb(myPOSTreader.Item("Data"))
            Causale = campodb(myPOSTreader.Item("Causale"))
            Descrizione = campodb(myPOSTreader.Item("Descrizione"))
            EPersonam = campodbN(myPOSTreader.Item("EPersonam"))
            DataOra = campodbD(myPOSTreader.Item("DataOra"))
        End If
        myPOSTreader.Close()
        cn.Close()
    End Sub

    Public Sub UltimaDataUscitaDefinitivaMese(ByVal StringaConnessione As String, ByVal Anno As Integer, ByVal Mese As Integer)
        Dim cn As OleDbConnection



        cn = New Data.OleDb.OleDbConnection(StringaConnessione)

        cn.Open()
        Dim cmd As New OleDbCommand()
        cmd.CommandText = ("select * from MOVIMENTI where CodiceOspite = " & CodiceOspite & " And CENTROSERVIZIO = '" & CENTROSERVIZIO & "' And TipoMov = '13' And year(Data) = " & Anno & " and month(data) = " & Mese & "  Order by Data Desc")
        cmd.Connection = cn
        Dim sb As StringBuilder = New StringBuilder

        Dim myPOSTreader As OleDbDataReader = cmd.ExecuteReader()
        If myPOSTreader.Read Then
            CENTROSERVIZIO = campodb(myPOSTreader.Item("CENTROSERVIZIO"))
            CodiceOspite = campodbN(myPOSTreader.Item("CodiceOspite"))
            TipoMov = campodb(myPOSTreader.Item("TipoMov"))
            Progressivo = campodbN(myPOSTreader.Item("Progressivo"))
            Data = campodb(myPOSTreader.Item("Data"))
            Causale = campodb(myPOSTreader.Item("Causale"))
            Descrizione = campodb(myPOSTreader.Item("Descrizione"))
            EPersonam = campodbN(myPOSTreader.Item("EPersonam"))
            DataOra = campodbD(myPOSTreader.Item("DataOra"))
        End If
        myPOSTreader.Close()
        cn.Close()
    End Sub

    Public Sub UltimaDataUscitaDefinitiva(ByVal StringaConnessione As String)
        Dim cn As OleDbConnection



        cn = New Data.OleDb.OleDbConnection(StringaConnessione)

        cn.Open()
        Dim cmd As New OleDbCommand()
        cmd.CommandText = ("select * from MOVIMENTI where CodiceOspite = " & CodiceOspite & " And CENTROSERVIZIO = '" & CENTROSERVIZIO & "' And TipoMov = '13' Order by Data Desc")
        cmd.Connection = cn
        Dim sb As StringBuilder = New StringBuilder

        Dim myPOSTreader As OleDbDataReader = cmd.ExecuteReader()
        If myPOSTreader.Read Then
            CENTROSERVIZIO = campodb(myPOSTreader.Item("CENTROSERVIZIO"))
            CodiceOspite = campodbN(myPOSTreader.Item("CodiceOspite"))
            TipoMov = campodb(myPOSTreader.Item("TipoMov"))
            Progressivo = campodbN(myPOSTreader.Item("Progressivo"))
            Data = campodb(myPOSTreader.Item("Data"))
            Causale = campodb(myPOSTreader.Item("Causale"))
            Descrizione = campodb(myPOSTreader.Item("Descrizione"))
            EPersonam = campodbN(myPOSTreader.Item("EPersonam"))
            DataOra = campodbD(myPOSTreader.Item("DataOra"))
        End If
        myPOSTreader.Close()
        cn.Close()
    End Sub

    Public Sub UltimaDataAccoglimentoSenzaCserv(ByVal StringaConnessione As String)
        Dim cn As OleDbConnection



        cn = New Data.OleDb.OleDbConnection(StringaConnessione)

        cn.Open()
        Dim cmd As New OleDbCommand()
        cmd.CommandText = ("select * from MOVIMENTI where CodiceOspite = " & CodiceOspite & " And TipoMov = '05' Order by Data Desc")
        cmd.Connection = cn
        Dim sb As StringBuilder = New StringBuilder

        Dim myPOSTreader As OleDbDataReader = cmd.ExecuteReader()
        If myPOSTreader.Read Then
            CENTROSERVIZIO = campodb(myPOSTreader.Item("CENTROSERVIZIO"))
            CodiceOspite = campodbN(myPOSTreader.Item("CodiceOspite"))
            TipoMov = campodb(myPOSTreader.Item("TipoMov"))
            Progressivo = campodbN(myPOSTreader.Item("Progressivo"))
            Data = campodb(myPOSTreader.Item("Data"))
            Causale = campodb(myPOSTreader.Item("Causale"))
            Descrizione = campodb(myPOSTreader.Item("Descrizione"))
            EPersonam = campodbN(myPOSTreader.Item("EPersonam"))
            DataOra = campodbD(myPOSTreader.Item("DataOra"))
        End If
        myPOSTreader.Close()
        cn.Close()
    End Sub
    Public Sub UltimaDataAccoglimento(ByVal StringaConnessione As String)
        Dim cn As OleDbConnection



        cn = New Data.OleDb.OleDbConnection(StringaConnessione)

        cn.Open()
        Dim cmd As New OleDbCommand()
        cmd.CommandText = ("select * from MOVIMENTI where CodiceOspite = " & CodiceOspite & " And CENTROSERVIZIO = '" & CENTROSERVIZIO & "' And TipoMov = '05' Order by Data Desc")
        cmd.Connection = cn
        Dim sb As StringBuilder = New StringBuilder

        Dim myPOSTreader As OleDbDataReader = cmd.ExecuteReader()
        If myPOSTreader.Read Then
            CENTROSERVIZIO = campodb(myPOSTreader.Item("CENTROSERVIZIO"))
            CodiceOspite = campodbN(myPOSTreader.Item("CodiceOspite"))
            TipoMov = campodb(myPOSTreader.Item("TipoMov"))
            Progressivo = campodbN(myPOSTreader.Item("Progressivo"))
            Data = campodbD(myPOSTreader.Item("Data"))
            Causale = campodb(myPOSTreader.Item("Causale"))
            Descrizione = campodb(myPOSTreader.Item("Descrizione"))
            EPersonam = campodbN(myPOSTreader.Item("EPersonam"))
            DataOra = campodbD(myPOSTreader.Item("DataOra"))
        End If
        myPOSTreader.Close()
        cn.Close()
    End Sub

    Public Sub UltimaDataAccoglimentoAData(ByVal StringaConnessione As String)
        Dim cn As OleDbConnection



        cn = New Data.OleDb.OleDbConnection(StringaConnessione)

        cn.Open()
        Dim cmd As New OleDbCommand()
        cmd.CommandText = ("select * from MOVIMENTI where CodiceOspite = " & CodiceOspite & " And CENTROSERVIZIO = '" & CENTROSERVIZIO & "' And TipoMov = '05' And Data <= ? Order by Data Desc")
        cmd.Connection = cn
        cmd.Parameters.AddWithValue("@Data", Data)

        Dim myPOSTreader As OleDbDataReader = cmd.ExecuteReader()
        If myPOSTreader.Read Then
            CENTROSERVIZIO = campodb(myPOSTreader.Item("CENTROSERVIZIO"))
            CodiceOspite = campodbN(myPOSTreader.Item("CodiceOspite"))
            TipoMov = campodb(myPOSTreader.Item("TipoMov"))
            Progressivo = campodbN(myPOSTreader.Item("Progressivo"))
            Data = campodbD(myPOSTreader.Item("Data"))
            Causale = campodb(myPOSTreader.Item("Causale"))
            Descrizione = campodb(myPOSTreader.Item("Descrizione"))
            EPersonam = campodbN(myPOSTreader.Item("EPersonam"))
            DataOra = campodbD(myPOSTreader.Item("DataOra"))

            
        End If
        myPOSTreader.Close()
        cn.Close()
    End Sub



    Public Sub UltimaData(ByVal StringaConnessione As String, ByVal CodiceOspite As Long)
        Dim cn As OleDbConnection



        cn = New Data.OleDb.OleDbConnection(StringaConnessione)

        cn.Open()
        Dim cmd As New OleDbCommand()
        cmd.CommandText = ("select * from MOVIMENTI where CodiceOspite = " & CodiceOspite & " Order by Data Desc")
        cmd.Connection = cn
        Dim sb As StringBuilder = New StringBuilder

        Dim myPOSTreader As OleDbDataReader = cmd.ExecuteReader()
        If myPOSTreader.Read Then
            CENTROSERVIZIO = campodb(myPOSTreader.Item("CENTROSERVIZIO"))
            CodiceOspite = campodbN(myPOSTreader.Item("CodiceOspite"))
            TipoMov = campodb(myPOSTreader.Item("TipoMov"))
            Progressivo = campodbN(myPOSTreader.Item("Progressivo"))
            Data = campodb(myPOSTreader.Item("Data"))
            Causale = campodb(myPOSTreader.Item("Causale"))
            Descrizione = campodb(myPOSTreader.Item("Descrizione"))
            EPersonam = campodbN(myPOSTreader.Item("EPersonam"))

            DataOra = campodbD(myPOSTreader.Item("DataOra"))
        End If
        myPOSTreader.Close()
        cn.Close()
    End Sub
    Public Sub UltimaDataCserv(ByVal StringaConnessione As String, ByVal CodiceOspite As Long, ByVal xCENTROSERVIZIO As String)
        Dim cn As OleDbConnection



        cn = New Data.OleDb.OleDbConnection(StringaConnessione)

        cn.Open()
        Dim cmd As New OleDbCommand()
        cmd.CommandText = ("select * from MOVIMENTI where CodiceOspite = " & CodiceOspite & " And CENTROSERVIZIO = '" & xCENTROSERVIZIO & "' Order by Data Desc")
        cmd.Connection = cn
        Dim sb As StringBuilder = New StringBuilder

        Dim myPOSTreader As OleDbDataReader = cmd.ExecuteReader()
        If myPOSTreader.Read Then
            CENTROSERVIZIO = campodb(myPOSTreader.Item("CENTROSERVIZIO"))
            CodiceOspite = campodbN(myPOSTreader.Item("CodiceOspite"))
            TipoMov = campodb(myPOSTreader.Item("TipoMov"))
            Progressivo = campodbN(myPOSTreader.Item("Progressivo"))
            Data = campodb(myPOSTreader.Item("Data"))
            Causale = campodb(myPOSTreader.Item("Causale"))
            Descrizione = campodb(myPOSTreader.Item("Descrizione"))
            EPersonam = campodbN(myPOSTreader.Item("EPersonam"))
            DataOra = campodbD(myPOSTreader.Item("DataOra"))
        End If
        myPOSTreader.Close()
        cn.Close()
    End Sub


    Public Sub UltimaMovimentoPrimaData(ByVal StringaConnessione As String, ByVal CodiceOspite As Long, ByVal CENTROSERVIZIO As String, ByVal MyData As Date)
        Dim cn As OleDbConnection



        cn = New Data.OleDb.OleDbConnection(StringaConnessione)

        cn.Open()
        Dim cmd As New OleDbCommand()
        If CENTROSERVIZIO = "" Then
            cmd.CommandText = ("select * from MOVIMENTI where CodiceOspite = " & CodiceOspite & " And (Data < ? OR (Data <= ? aND TipoMov = '05')) Order by Data Desc, ID DESC")
        Else
            cmd.CommandText = ("select * from MOVIMENTI where CodiceOspite = " & CodiceOspite & " And CENTROSERVIZIO = ? And (Data < ? OR (Data <= ? aND TipoMov = '05')) Order by Data Desc, ID DESC")
        End If

        cmd.Connection = cn
        If CENTROSERVIZIO <> "" Then
            cmd.Parameters.AddWithValue("@CENTROSERVIZIO", CENTROSERVIZIO)
        End If
        cmd.Parameters.AddWithValue("@Data", MyData)
        cmd.Parameters.AddWithValue("@Data", MyData)

        Dim myPOSTreader As OleDbDataReader = cmd.ExecuteReader()
        If myPOSTreader.Read Then
            CENTROSERVIZIO = campodb(myPOSTreader.Item("CENTROSERVIZIO"))
            CodiceOspite = campodbN(myPOSTreader.Item("CodiceOspite"))
            TipoMov = campodb(myPOSTreader.Item("TipoMov"))
            Progressivo = campodbN(myPOSTreader.Item("Progressivo"))
            Data = campodb(myPOSTreader.Item("Data"))
            Causale = campodb(myPOSTreader.Item("Causale"))
            Descrizione = campodb(myPOSTreader.Item("Descrizione"))
            EPersonam = campodbN(myPOSTreader.Item("EPersonam"))
            DataOra = campodbD(myPOSTreader.Item("DataOra"))
        End If
        myPOSTreader.Close()
        cn.Close()
    End Sub

    Public Function PresenzaInSAD(ByVal StringaConnessione As String, ByVal CodiceOspite As Long, ByVal Anno As Long, ByVal Mese As Long) As Boolean

        Dim cn As OleDbConnection
        Dim CentroServizio(100) As String
        Dim Presente As Boolean
        Dim CentroServizioApp As String = ""
        cn = New Data.OleDb.OleDbConnection(StringaConnessione)

        PresenzaInSAD = False
        cn.Open()
        Dim cmd As New OleDbCommand()
        cmd.CommandText = ("select * from MOVIMENTI where CodiceOspite = " & CodiceOspite & " Order by CENTROSERVIZIO,Data Asc")
        cmd.Connection = cn
        cmd.Parameters.AddWithValue("@CENTROSERVIZIO", CentroServizio)

        Dim myPOSTreader As OleDbDataReader = cmd.ExecuteReader()

        If Anno = 0 Then
            If myPOSTreader.Read Then
                PresenzaInSAD = True
            Else
                PresenzaInSAD = False
            End If
            Exit Function
        Else

            Do While myPOSTreader.Read
                If CentroServizioApp <> campodb(myPOSTreader.Item("CENTROSERVIZIO")) And CentroServizioApp <> "" Then
                    If Presente = True Then
                        Dim Cs As New Cls_CentroServizio

                        Cs.CENTROSERVIZIO = CentroServizioApp
                        Cs.Leggi(StringaConnessione, Cs.CENTROSERVIZIO)
                        If Cs.TIPOCENTROSERVIZIO = "A" Then
                            myPOSTreader.Close()
                            cn.Close()
                            PresenzaInSAD = True
                            Exit Function
                        End If
                    End If
                End If
                CentroServizioApp = campodb(myPOSTreader.Item("CENTROSERVIZIO"))
                If campodb(myPOSTreader.Item("TipoMov")) = "13" And Format(campodbD(myPOSTreader.Item("Data")), "yyyyMMdd") < Format(DateSerial(Anno, Mese, 1), "yyyyMMdd") Then
                    Presente = False
                End If
                If campodb(myPOSTreader.Item("TipoMov")) = "05" And Format(campodbD(myPOSTreader.Item("Data")), "yyyyMMdd") <= Format(DateSerial(Anno, Mese, GiorniMese(Mese, Anno)), "yyyyMMdd") Then
                    Presente = True
                End If
            Loop
            myPOSTreader.Close()
            cn.Close()


            Dim Cs1 As New Cls_CentroServizio

            Cs1.CENTROSERVIZIO = CentroServizioApp
            Cs1.Leggi(StringaConnessione, Cs1.CENTROSERVIZIO)
            If Cs1.TIPOCENTROSERVIZIO = "A" Then
                PresenzaInSAD = True
                Exit Function
            End If
        End If

    End Function

    Public Function GiorniMese(ByVal Mese As Byte, ByVal Anno As Integer) As Byte
        If Mese = 1 Or Mese = 3 Or Mese = 5 Or Mese = 7 Or Mese = 8 Or _
           Mese = 10 Or Mese = 12 Then
            GiorniMese = 31
        Else
            If Mese <> 2 Then
                GiorniMese = 30
            Else
                If Day(DateSerial(Anno, Mese, 29)) = 29 Then
                    GiorniMese = 29
                Else
                    GiorniMese = 28
                End If
            End If
        End If
    End Function



    Public Sub UltimaDataUscita(ByVal StringaConnessione As String, ByVal CodiceOspite As Integer, ByVal CENTROSERVIZIO As String, ByVal DataInterrogazione As Date)
        Dim cn As OleDbConnection



        cn = New Data.OleDb.OleDbConnection(StringaConnessione)

        cn.Open()
        Dim cmd As New OleDbCommand()
        cmd.CommandText = ("select * from MOVIMENTI where CodiceOspite = " & CodiceOspite & " And CENTROSERVIZIO = '" & CENTROSERVIZIO & "' And  Data <= ? And TipoMov = '03' Order by Data Desc")
        cmd.Connection = cn
        cmd.Parameters.AddWithValue("@Data", DataInterrogazione)

        Dim myPOSTreader As OleDbDataReader = cmd.ExecuteReader()
        If myPOSTreader.Read Then
            CENTROSERVIZIO = campodb(myPOSTreader.Item("CENTROSERVIZIO"))
            CodiceOspite = campodbN(myPOSTreader.Item("CodiceOspite"))
            TipoMov = campodb(myPOSTreader.Item("TipoMov"))
            Progressivo = campodbN(myPOSTreader.Item("Progressivo"))
            Data = campodb(myPOSTreader.Item("Data"))
            Causale = campodb(myPOSTreader.Item("Causale"))
            Descrizione = campodb(myPOSTreader.Item("Descrizione"))
            EPersonam = campodbN(myPOSTreader.Item("EPersonam"))
            DataOra = campodbD(myPOSTreader.Item("DataOra"))
        End If
        myPOSTreader.Close()
        cn.Close()
    End Sub


    Public Sub PrimaData(ByVal StringaConnessione As String, ByVal CodiceOspite As Long)
        Dim cn As OleDbConnection



        cn = New Data.OleDb.OleDbConnection(StringaConnessione)

        cn.Open()
        Dim cmd As New OleDbCommand()
        cmd.CommandText = ("select * from MOVIMENTI where CodiceOspite = " & CodiceOspite & " Order by Data")
        cmd.Connection = cn
        Dim sb As StringBuilder = New StringBuilder

        Dim myPOSTreader As OleDbDataReader = cmd.ExecuteReader()
        If myPOSTreader.Read Then
            CENTROSERVIZIO = campodb(myPOSTreader.Item("CENTROSERVIZIO"))
            CodiceOspite = campodbN(myPOSTreader.Item("CodiceOspite"))
            TipoMov = campodb(myPOSTreader.Item("TipoMov"))
            Progressivo = campodbN(myPOSTreader.Item("Progressivo"))
            Data = campodb(myPOSTreader.Item("Data"))
            Causale = campodb(myPOSTreader.Item("Causale"))
            Descrizione = campodb(myPOSTreader.Item("Descrizione"))
            EPersonam = campodbN(myPOSTreader.Item("EPersonam"))
            DataOra = campodbD(myPOSTreader.Item("DataOra"))
        End If
        myPOSTreader.Close()
        cn.Close()
    End Sub
End Class
