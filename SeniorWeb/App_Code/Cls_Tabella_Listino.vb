﻿Imports Microsoft.VisualBasic
Imports System.Data.OleDb
Imports System.Web.Hosting


Public Class Cls_Tabella_Listino

    Public Codice As String
    Public CENTROSERVIZIO As String
    Public Struttura As String
    Public Descrizione As String
    Public SETTIMANA As String
    Public DATA As Date
    Public IMPORTORETTATOTALE As Double
    Public TIPORETTATOTALE As String
    Public TipoRetta As String
    Public MODALITA As String
    Public CODICEEXTRA1 As String
    Public CODICEEXTRA2 As String
    Public CODICEEXTRA3 As String
    Public CODICEEXTRA4 As String
    Public CODICEEXTRA5 As String
    Public CODICEEXTRA6 As String
    Public CODICEEXTRA7 As String
    Public CODICEEXTRA8 As String
    Public CODICEEXTRA9 As String
    Public CODICEEXTRA10 As String
    Public TIPOOPERAIZONE As String
    Public CODICEIVA As String
    Public MODALITAPAGAMENTO As String
    Public COMPENSAZIONE As String
    Public ANTICIPATA As String
    Public IMPORTORETTTAOSPITE1 As Double
    Public IMPORTORETTTAOSPITE2 As Double
    Public TIPORETTTAOSPITE As String
    Public USL As String
    Public TIPORETTAUSL As String
    Public SOCIALECOMUNE As String
    Public SOCIALEPROVINCIA As String
    Public IMPORTORETTTASOCIALE As Double
    Public TIPORETTASOCIALE As String
    Public JOLLYCOMUNE As String
    Public JOLLYPROVINCIA As String
    Public IMPORTORETTTAJOLLY As Double
    Public TIPORETTAJOLLY As String
    Public TIPOLISTINO As String
    Public CodiceEpersonam As Integer
    Public PeriodoFatturazione As String
    Public USLDACOMUNERESIDENZA As Integer
    Public ImportoRicoveriMaggioreZero As Double
    Public GiornateSoglia As Integer
    Public ImportoIncremento As Double
    Public ImportoDiurno As Double


    Function EsisteListino(ByVal StringaConnessione As String) As Boolean
        Dim cn As OleDbConnection


        EsisteListino = False

        cn = New Data.OleDb.OleDbConnection(StringaConnessione)

        cn.Open()
        Dim cmd As New OleDbCommand()
        cmd.CommandText = ("select * from [Tabella_Listino] ")


        cmd.Connection = cn


        Dim myPOSTreader As OleDbDataReader = cmd.ExecuteReader()
        If myPOSTreader.Read Then
            EsisteListino = True
        End If
        myPOSTreader.Close()

        cn.Close()
    End Function


    Sub UpDateDropBoxConCodice(ByVal StringaConnessione As String, ByRef appoggio As DropDownList, Optional ByVal CENTROSERVIZIO As String = "", Optional ByVal TIPO As String = "")
        Dim cn As OleDbConnection



        cn = New Data.OleDb.OleDbConnection(StringaConnessione)

        cn.Open()
        Dim cmd As New OleDbCommand()

        Dim CS As New Cls_CentroServizio

        CS.CENTROSERVIZIO = CENTROSERVIZIO
        CS.Leggi(StringaConnessione, CS.CENTROSERVIZIO)

        If TIPO = "" Then
            cmd.CommandText = ("select * from Tabella_Listino where (Data  Is Null Or Data > GETDATE()) AND (((CentroServizio Is Null or CentroServizio  = '') and (Struttura Is Null or Struttura  = '')) Or CentroServizio  = ?  Or ((CentroServizio Is Null or CentroServizio  = '') and  Struttura = ?)) And ((TIPOLISTINO Is null Or TIPOLISTINO = '') OR TIPOLISTINO = 'S')  order by Descrizione")
        End If
        If TIPO = "P" Then
            cmd.CommandText = ("select * from Tabella_Listino where (Data  Is Null Or Data > GETDATE()) AND (((CentroServizio Is Null or CentroServizio  = '') and (Struttura Is Null or Struttura  = '')) Or CentroServizio  = ?  Or ((CentroServizio Is Null or CentroServizio  = '') and  Struttura = ?)) And TIPOLISTINO = 'P' order by Descrizione")
        End If
        If TIPO = "R" Then
            cmd.CommandText = ("select * from Tabella_Listino where (Data  Is Null Or Data > GETDATE()) AND (((CentroServizio Is Null or CentroServizio  = '') and (Struttura Is Null or Struttura  = '')) Or CentroServizio  = ?  Or ((CentroServizio Is Null or CentroServizio  = '') and  Struttura = ?)) And TIPOLISTINO = 'R' order by Descrizione")
        End If
        If TIPO = "C" Then
            cmd.CommandText = ("select * from Tabella_Listino where (Data  Is Null Or Data > GETDATE()) AND (((CentroServizio Is Null or CentroServizio  = '') and (Struttura Is Null or Struttura  = '')) Or CentroServizio  = ?  Or ((CentroServizio Is Null or CentroServizio  = '') and  Struttura = ?)) And TIPOLISTINO = 'C' order by Descrizione")
        End If
        If TIPO = "J" Then
            cmd.CommandText = ("select * from Tabella_Listino where (Data  Is Null Or Data > GETDATE()) AND (((CentroServizio Is Null or CentroServizio  = '') and (Struttura Is Null or Struttura  = '')) Or CentroServizio  = ?  Or ((CentroServizio Is Null or CentroServizio  = '') and  Struttura = ?)) And TIPOLISTINO = 'J' order by Descrizione")
        End If
        If TIPO = "A" Then
            cmd.CommandText = ("select * from Tabella_Listino where (Data  Is Null Or Data > GETDATE())   order by Descrizione")
        End If
        cmd.Connection = cn
        If TIPO <> "A" Then
            cmd.Parameters.AddWithValue("@CentroServizio", CENTROSERVIZIO)
            cmd.Parameters.AddWithValue("@Struttura", CS.Villa)
        End If


        appoggio.Items.Clear()
        Dim myPOSTreader As OleDbDataReader = cmd.ExecuteReader()
        Do While myPOSTreader.Read

            If campodb(myPOSTreader.Item("DATA")) = "" Then
                appoggio.Items.Add(myPOSTreader.Item("Codice") & " " & myPOSTreader.Item("Descrizione"))
            Else
                appoggio.Items.Add(myPOSTreader.Item("Codice") & " " & myPOSTreader.Item("Descrizione") & " (" & campodb(myPOSTreader.Item("DATA")) & ")")
            End If

            appoggio.Items(appoggio.Items.Count - 1).Value = myPOSTreader.Item("Codice")
        Loop
        myPOSTreader.Close()
        cn.Close()
        appoggio.Items.Add("")
        appoggio.Items(appoggio.Items.Count - 1).Value = ""
        appoggio.Items(appoggio.Items.Count - 1).Selected = True

    End Sub




    Sub Elimina(ByVal StringaConnessione As String)
        Dim cn As OleDbConnection


        cn = New Data.OleDb.OleDbConnection(StringaConnessione)

        cn.Open()
        Dim cmd As New OleDbCommand()
        cmd.CommandText = ("Delete from [Tabella_Listino] Where codice = ?")

        cmd.Parameters.AddWithValue("@codice", Codice)
        cmd.Connection = cn
        cmd.ExecuteNonQuery()

        cn.Close()
    End Sub

    Function MaxTipoListino(ByVal StringaConnessione As String) As String
        Dim cn As OleDbConnection


        MaxTipoListino = ""

        cn = New Data.OleDb.OleDbConnection(StringaConnessione)

        cn.Open()
        Dim cmd As New OleDbCommand()
        cmd.CommandText = ("select max(CASE WHEN (len(Codice) = 1) THEN '0' + CODICE ELSE Codice END) from Tabella_Listino ")
        cmd.Connection = cn
        Dim MaxAddebito As OleDbDataReader = cmd.ExecuteReader()
        If MaxAddebito.Read Then
            Dim MassimoLetto As String

            MassimoLetto = campodb(MaxAddebito.Item(0))

            If MassimoLetto = "Z9" Then
                MassimoLetto = "a0"
                cn.Close()
                Return MassimoLetto
            End If

            If MassimoLetto = "99" Then
                MassimoLetto = "A0"
            Else
                If Mid(MassimoLetto & Space(10), 1, 1) > "0" And Mid(MassimoLetto & Space(10), 1, 1) > "9" Then
                    If Mid(MassimoLetto & Space(10), 2, 1) < "9" Then
                        MassimoLetto = Mid(MassimoLetto & Space(10), 1, 1) & Val(Mid(MassimoLetto & Space(10), 2, 1)) + 1
                    Else
                        Dim CodiceAscii As String
                        CodiceAscii = Asc(Mid(MassimoLetto & Space(10), 1, 1))

                        MassimoLetto = Chr(CodiceAscii + 1) & "0"
                    End If
                Else
                    MassimoLetto = Format(Val(MassimoLetto) + 1, "00")
                End If
            End If

            Return MassimoLetto
        Else
            Return "01"
        End If
        cn.Close()

    End Function
    Sub LeggiDescrizione(ByVal StringaConnessione As String)
        Dim cn As OleDbConnection


        cn = New Data.OleDb.OleDbConnection(StringaConnessione)

        cn.Open()
        Dim cmd As New OleDbCommand()
        cmd.CommandText = ("select * from [Tabella_Listino] Where Descrizione like ?")

        cmd.Parameters.AddWithValue("@Descrizione", Descrizione)
        cmd.Connection = cn


        Dim myPOSTreader As OleDbDataReader = cmd.ExecuteReader()
        If myPOSTreader.Read Then
            Codice = campodb(myPOSTreader.Item("Codice"))
            LeggiCausale(StringaConnessione)
        End If
        myPOSTreader.Close()
        cn.Close()
    End Sub

    Sub LeggiCodiceEpersonam(ByVal StringaConnessione As String)
        Dim cn As OleDbConnection


        cn = New Data.OleDb.OleDbConnection(StringaConnessione)

        cn.Open()
        Dim cmd As New OleDbCommand()
        cmd.CommandText = ("select * from [Tabella_Listino] Where CodiceEpersonam like ?")

        cmd.Parameters.AddWithValue("@CodiceEpersonam", CodiceEpersonam)
        cmd.Connection = cn


        Dim myPOSTreader As OleDbDataReader = cmd.ExecuteReader()
        If myPOSTreader.Read Then
            Codice = campodb(myPOSTreader.Item("Codice"))
            LeggiCausale(StringaConnessione)
        End If
        myPOSTreader.Close()
        cn.Close()
    End Sub

    Sub LeggiCausale(ByVal StringaConnessione As String)
        Dim cn As OleDbConnection


        cn = New Data.OleDb.OleDbConnection(StringaConnessione)

        cn.Open()
        Dim cmd As New OleDbCommand()
        cmd.CommandText = ("select * from [Tabella_Listino] Where codice = ?")

        cmd.Parameters.AddWithValue("@codice", Codice)
        cmd.Connection = cn


        Dim myPOSTreader As OleDbDataReader = cmd.ExecuteReader()
        If myPOSTreader.Read Then



            Codice = campodb(myPOSTreader.Item("Codice"))
            Descrizione = campodb(myPOSTreader.Item("Descrizione"))
            SETTIMANA = campodb(myPOSTreader.Item("SETTIMANA"))
            IMPORTORETTATOTALE = campodbn(myPOSTreader.Item("IMPORTORETTATOTALE"))
            TIPORETTATOTALE = campodb(myPOSTreader.Item("TIPORETTATOTALE"))
            TipoRetta = campodb(myPOSTreader.Item("TipoRetta"))
            MODALITA = campodb(myPOSTreader.Item("MODALITA"))
            CODICEEXTRA1 = campodb(myPOSTreader.Item("CODICEEXTRA1"))
            CODICEEXTRA2 = campodb(myPOSTreader.Item("CODICEEXTRA2"))
            CODICEEXTRA3 = campodb(myPOSTreader.Item("CODICEEXTRA3"))
            CODICEEXTRA4 = campodb(myPOSTreader.Item("CODICEEXTRA4"))
            CODICEEXTRA5 = campodb(myPOSTreader.Item("CODICEEXTRA5"))
            CODICEEXTRA6 = campodb(myPOSTreader.Item("CODICEEXTRA6"))
            CODICEEXTRA7 = campodb(myPOSTreader.Item("CODICEEXTRA7"))
            CODICEEXTRA8 = campodb(myPOSTreader.Item("CODICEEXTRA8"))
            CODICEEXTRA9 = campodb(myPOSTreader.Item("CODICEEXTRA9"))
            CODICEEXTRA10 = campodb(myPOSTreader.Item("CODICEEXTRA10"))
            TIPOOPERAIZONE = campodb(myPOSTreader.Item("TIPOOPERAIZONE"))
            CODICEIVA = campodb(myPOSTreader.Item("CODICEIVA"))
            MODALITAPAGAMENTO = campodb(myPOSTreader.Item("MODALITAPAGAMENTO"))
            COMPENSAZIONE = campodb(myPOSTreader.Item("COMPENSAZIONE"))
            ANTICIPATA = campodb(myPOSTreader.Item("ANTICIPATA"))
            IMPORTORETTTAOSPITE1 = campodbn(myPOSTreader.Item("IMPORTORETTTAOSPITE1"))
            IMPORTORETTTAOSPITE2 = campodbn(myPOSTreader.Item("IMPORTORETTTAOSPITE2"))
            TIPORETTTAOSPITE = campodb(myPOSTreader.Item("TIPORETTTAOSPITE"))
            USL = campodb(myPOSTreader.Item("USL"))
            TIPORETTAUSL = campodb(myPOSTreader.Item("TIPORETTAUSL"))
            SOCIALECOMUNE = campodb(myPOSTreader.Item("SOCIALECOMUNE"))
            SOCIALEPROVINCIA = campodb(myPOSTreader.Item("SOCIALEPROVINCIA"))
            IMPORTORETTTASOCIALE = campodbn(myPOSTreader.Item("IMPORTORETTTASOCIALE"))
            TIPORETTASOCIALE = campodb(myPOSTreader.Item("TIPORETTASOCIALE"))
            JOLLYCOMUNE = campodb(myPOSTreader.Item("JOLLYCOMUNE"))
            JOLLYPROVINCIA = campodb(myPOSTreader.Item("JOLLYPROVINCIA"))
            IMPORTORETTTAJOLLY = campodbn(myPOSTreader.Item("IMPORTORETTTAJOLLY"))
            TIPORETTAJOLLY = campodb(myPOSTreader.Item("TIPORETTAJOLLY"))
            TIPOLISTINO = campodb(myPOSTreader.Item("TIPOLISTINO"))
            DATA = campodbD(myPOSTreader.Item("DATA"))


            CENTROSERVIZIO = campodb(myPOSTreader.Item("CENTROSERVIZIO"))
            Struttura = campodb(myPOSTreader.Item("Struttura"))

            PeriodoFatturazione = campodb(myPOSTreader.Item("PeriodoFatturazione"))


            CodiceEpersonam = campodbn(myPOSTreader.Item("CodiceEpersonam"))

            USLDACOMUNERESIDENZA = campodbn(myPOSTreader.Item("USLDACOMUNERESIDENZA"))



            ImportoRicoveriMaggioreZero = campodbn(myPOSTreader.Item("ImportoRicoveriMaggioreZero"))
            GiornateSoglia  = campodbn(myPOSTreader.Item("GiornateSoglia"))
            ImportoIncremento  = campodbn(myPOSTreader.Item("ImportoIncremento"))
            ImportoDiurno  = campodbn(myPOSTreader.Item("ImportoDiurno"))
        End If
        myPOSTreader.Close()
        cn.Close()
    End Sub

    Function campodbD(ByVal oggetto As Object) As Date
        If IsDBNull(oggetto) Then
            Return Nothing
        Else
            Return oggetto
        End If
    End Function


    Function campodb(ByVal oggetto As Object) As String
        If IsDBNull(oggetto) Then
            Return ""
        Else
            Return oggetto
        End If
    End Function


    Function campodbn(ByVal oggetto As Object) As Double
        If IsDBNull(oggetto) Then
            Return 0
        Else
            Return oggetto
        End If
    End Function


    Sub Scrivi(ByVal StringaConnessione As String)
        Dim cn As OleDbConnection



        cn = New Data.OleDb.OleDbConnection(StringaConnessione)

        cn.Open()
        Dim cmd As New OleDbCommand()
        cmd.CommandText = ("select * from [Tabella_Listino] where " & _
                           "CODICE = ? ")
        cmd.Parameters.AddWithValue("@CODICE", Codice)
        cmd.Connection = cn
        Dim myPOSTreader As OleDbDataReader = cmd.ExecuteReader()
        If Not myPOSTreader.Read Then
            Dim XSql As String
            XSql = "INSERT INTO [Tabella_Listino] (CODICE,DESCRIZIONE) values (?,?)"
            Dim cmdIns As New OleDbCommand()
            cmdIns.CommandText = XSql
            cmdIns.Parameters.AddWithValue("@Codice", Codice)
            cmdIns.Parameters.AddWithValue("@Descrizione", Descrizione)
            cmdIns.Connection = cn
            cmdIns.ExecuteNonQuery()
        End If
        myPOSTreader.Close()

        Dim MySql As String

        MySql = "UPDATE [Tabella_Listino] SET " & _
              "[Descrizione] = ?, " & _
              "DATA = ?, " & _
              "[SETTIMANA] = ?, " & _
              "[IMPORTORETTATOTALE] = ?, " & _
              "[TIPORETTATOTALE] = ?, " & _
              "[TipoRetta] =  ?, " & _
              "[MODALITA] = ?, " & _
              "[CODICEEXTRA1] = ?, " & _
              "[CODICEEXTRA2] = ?, " & _
              "[CODICEEXTRA3] = ?, " & _
              "[CODICEEXTRA4] = ?, " & _
              "[CODICEEXTRA5] = ?, " & _
              "[CODICEEXTRA6] = ?, " & _
              "[CODICEEXTRA7] = ?, " & _
              "[CODICEEXTRA8] = ?, " & _
              "[CODICEEXTRA9] = ?, " & _
              "[CODICEEXTRA10] = ?, " & _
              "[TIPOOPERAIZONE] = ?, " & _
              "[CODICEIVA] = ?, " & _
              "[MODALITAPAGAMENTO] = ?, " & _
              "[COMPENSAZIONE] = ?, " & _
              "[ANTICIPATA] = ?, " & _
              "[IMPORTORETTTAOSPITE1] = ?, " & _
              "[IMPORTORETTTAOSPITE2] = ?, " & _
              "[TIPORETTTAOSPITE] = ?, " & _
              "[USL] = ?, " & _
              "[TIPORETTAUSL] = ?, " & _
              "[SOCIALECOMUNE] = ?, " & _
              "[SOCIALEPROVINCIA] = ?, " & _
              "[IMPORTORETTTASOCIALE] = ?, " & _
              "[TIPORETTASOCIALE] = ?, " & _
              "[JOLLYCOMUNE] = ?, " & _
              "[JOLLYPROVINCIA] = ?, " & _
              "[IMPORTORETTTAJOLLY] = ?, " & _
              "[TIPORETTAJOLLY] = ?, " & _
              "TIPOLISTINO = ?, " & _
              "CENTROSERVIZIO = ?, " & _
              "Struttura = ?, " & _
              "CodiceEpersonam  = ?, " & _
              "PeriodoFatturazione = ?, " & _
              "USLDACOMUNERESIDENZA = ?, " & _              
              "ImportoRicoveriMaggioreZero  = ?, " & _              
              "GiornateSoglia  = ?, " & _              
              "ImportoIncremento  = ?, " & _              
              "ImportoDiurno  = ? " & _              
              " Where Codice = ? "
        Dim cmd1 As New OleDbCommand()
        cmd1.Connection = cn
        cmd1.CommandText = MySql
        cmd1.Parameters.AddWithValue("@Descrizione", Descrizione)
        cmd1.Parameters.AddWithValue("@DATA", IIf(Year(DATA) > 1, DATA, System.DBNull.Value))
        cmd1.Parameters.AddWithValue("@SETTIMANA", SETTIMANA)
        cmd1.Parameters.AddWithValue("@IMPORTORETTATOTALE", IMPORTORETTATOTALE)
        cmd1.Parameters.AddWithValue("@TIPORETTATOTALE", TIPORETTATOTALE)
        cmd1.Parameters.AddWithValue("@TipoRetta", TipoRetta)
        cmd1.Parameters.AddWithValue("@MODALITA", MODALITA)
        cmd1.Parameters.AddWithValue("@CODICEEXTRA1", CODICEEXTRA1)
        cmd1.Parameters.AddWithValue("@CODICEEXTRA2", CODICEEXTRA2)
        cmd1.Parameters.AddWithValue("@CODICEEXTRA3", CODICEEXTRA3)
        cmd1.Parameters.AddWithValue("@CODICEEXTRA4", CODICEEXTRA4)
        cmd1.Parameters.AddWithValue("@CODICEEXTRA5", CODICEEXTRA5)
        cmd1.Parameters.AddWithValue("@CODICEEXTRA6", CODICEEXTRA6)
        cmd1.Parameters.AddWithValue("@CODICEEXTRA7", CODICEEXTRA7)
        cmd1.Parameters.AddWithValue("@CODICEEXTRA8", CODICEEXTRA8)
        cmd1.Parameters.AddWithValue("@CODICEEXTRA9", CODICEEXTRA9)
        cmd1.Parameters.AddWithValue("@CODICEEXTRA10", CODICEEXTRA10)
        cmd1.Parameters.AddWithValue("@TIPOOPERAIZONE", TIPOOPERAIZONE)
        cmd1.Parameters.AddWithValue("@CODICEIVA", CODICEIVA)
        cmd1.Parameters.AddWithValue("@MODALITAPAGAMENTO", MODALITAPAGAMENTO)
        cmd1.Parameters.AddWithValue("@COMPENSAZIONE", COMPENSAZIONE)
        cmd1.Parameters.AddWithValue("@ANTICIPATA", ANTICIPATA)
        cmd1.Parameters.AddWithValue("@IMPORTORETTTAOSPITE1", IMPORTORETTTAOSPITE1)
        cmd1.Parameters.AddWithValue("@IMPORTORETTTAOSPITE2", IMPORTORETTTAOSPITE2)
        cmd1.Parameters.AddWithValue("@TIPORETTTAOSPITE", TIPORETTTAOSPITE)
        cmd1.Parameters.AddWithValue("@USL", USL)
        cmd1.Parameters.AddWithValue("@TIPORETTAUSL", TIPORETTAUSL)
        cmd1.Parameters.AddWithValue("@SOCIALECOMUNE", SOCIALECOMUNE)
        cmd1.Parameters.AddWithValue("@SOCIALEPROVINCIA", SOCIALEPROVINCIA)
        cmd1.Parameters.AddWithValue("@IMPORTORETTTASOCIALE", IMPORTORETTTASOCIALE)
        cmd1.Parameters.AddWithValue("@TIPORETTASOCIALE", TIPORETTASOCIALE)
        cmd1.Parameters.AddWithValue("@JOLLYCOMUNE", JOLLYCOMUNE)
        cmd1.Parameters.AddWithValue("@JOLLYPROVINCIA", JOLLYPROVINCIA)
        cmd1.Parameters.AddWithValue("@IMPORTORETTTAJOLLY", IMPORTORETTTAJOLLY)
        cmd1.Parameters.AddWithValue("@TIPORETTAJOLLY", TIPORETTAJOLLY)
        cmd1.Parameters.AddWithValue("@TIPOLISTINO", TIPOLISTINO)

        cmd1.Parameters.AddWithValue("@CENTROSERVIZIO", CENTROSERVIZIO)
        cmd1.Parameters.AddWithValue("@Struttura", Struttura)

        cmd1.Parameters.AddWithValue("@CodiceEpersonam", CodiceEpersonam)
        cmd1.Parameters.AddWithValue("@PeriodoFatturazione", PeriodoFatturazione)
        cmd1.Parameters.AddWithValue("@USLDACOMUNERESIDENZA", USLDACOMUNERESIDENZA)


        cmd1.Parameters.AddWithValue("@ImportoRicoveriMaggioreZero", ImportoRicoveriMaggioreZero)
        cmd1.Parameters.AddWithValue("@GiornateSoglia", GiornateSoglia)
        cmd1.Parameters.AddWithValue("@ImportoIncremento", ImportoIncremento)
        cmd1.Parameters.AddWithValue("@ImportoDiurno", ImportoDiurno)
        'USLDACOMUNERESIDENZA


        'PeriodoFatturazione

        'CodiceEpersonam
        'TIPOLISTINO
        cmd1.Parameters.AddWithValue("@Codice", Codice)
        cmd1.ExecuteNonQuery()

        cn.Close()
    End Sub


End Class
