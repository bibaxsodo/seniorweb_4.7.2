Imports Microsoft.VisualBasic
Imports System.Data.OleDb
Imports System.Web.Hosting

Public Class Cls_rettatotale
    Public CENTROSERVIZIO As String
    Public CODICEOSPITE As Long
    Public Data As Date
    Public Importo As Double
    Public Tipo As String
    Public TipoRetta As String
    Public Utente As String
    Public DataAggiornamento As String

    Function campodb(ByVal oggetto As Object) As String
        If IsDBNull(oggetto) Then
            Return ""
        Else
            Return oggetto
        End If
    End Function
    Function campodbN(ByVal oggetto As Object) As Double
        If IsDBNull(oggetto) Then
            Return 0
        Else
            Return oggetto
        End If
    End Function


    Public Sub EliminaAll(ByVal StringaConnessione As String)
        Dim cn As OleDbConnection
        Dim MySql As String

        MySql = ""

        cn = New Data.OleDb.OleDbConnection(StringaConnessione)

        cn.Open()
        Dim cmd As New OleDbCommand()
        cmd.CommandText = "Delete from IMPORTORETTA where CentroServizio = '" & CENTROSERVIZIO & "' And  CodiceOspite = " & CODICEOSPITE
        cmd.Connection = cn
        cmd.ExecuteNonQuery()
        cn.Close()
    End Sub
    Public Sub Elimina(ByVal StringaConnessione As String)
        Dim cn As OleDbConnection
        Dim MySql As String

        MySql = ""

        cn = New Data.OleDb.OleDbConnection(StringaConnessione)

        cn.Open()
        Dim cmd As New OleDbCommand()
        cmd.CommandText = "Delete from IMPORTORETTA where CentroServizio = '" & CENTROSERVIZIO & "' And  CodiceOspite = " & CODICEOSPITE & "  And Data = ?"
        cmd.Parameters.AddWithValue("@Data", Data)
        cmd.Connection = cn
        cmd.ExecuteNonQuery()
        cn.Close()
    End Sub


    Public Sub AggiornaDB(ByVal StringaConnessione As String)
        Dim cn As OleDbConnection
        Dim MySql As String

        MySql = ""


        cn = New Data.OleDb.OleDbConnection(StringaConnessione)

        cn.Open()
        Dim cmd As New OleDbCommand()
        cmd.CommandText = ("select * from IMPORTORETTA where CentroServizio = '" & CENTROSERVIZIO & "' And  CodiceOspite = " & CODICEOSPITE & "  And Data = ?")
        cmd.Parameters.AddWithValue("@Data", Data)
        cmd.Connection = cn
        Dim myPOSTreader As OleDbDataReader = cmd.ExecuteReader()
        If myPOSTreader.Read Then
            MySql = "UPDATE IMPORTORETTA SET IMPORTO = ?,TIPOIMPORTO=?,TipoRetta=?,Utente = ?,DataAggiornamento = ? " & _
                    " WHERE  CentroServizio = '" & CENTROSERVIZIO & "' And  CodiceOspite = " & CODICEOSPITE & " And Data = ?"
            Dim cmdw As New OleDbCommand()
            cmdw.CommandText = (MySql)
            cmdw.Parameters.AddWithValue("@IMPORTO", Importo)
            cmdw.Parameters.AddWithValue("@TIPOIMPORTO", Tipo)
            cmdw.Parameters.AddWithValue("@TipoRetta", TipoRetta)
            cmdw.Parameters.AddWithValue("@Data", Data)
            cmdw.Parameters.AddWithValue("@Utente", Utente)
            cmdw.Parameters.AddWithValue("@DataAggiornamento", Now)
            cmdw.Connection = cn
            cmdw.ExecuteNonQuery()
        Else
            MySql = "INSERT INTO IMPORTORETTA (CentroServizio,CodiceOspite,Data,IMPORTO,TIPOIMPORTO,TipoRetta,Utente,DataAggiornamento) VALUES (?,?,?,?,?,?,?,?)"
            Dim cmdw As New OleDbCommand()
            cmdw.CommandText = (MySql)
            cmdw.Parameters.AddWithValue("@CentroServizio", CentroServizio)
            cmdw.Parameters.AddWithValue("@CodiceOspite", CODICEOSPITE)            
            cmdw.Parameters.AddWithValue("@Data", Data)
            cmdw.Parameters.AddWithValue("@IMPORTO", Importo)
            cmdw.Parameters.AddWithValue("@TIPOIMPORTO", Tipo)
            cmdw.Parameters.AddWithValue("@TipoRetta", TipoRetta)
            cmdw.Parameters.AddWithValue("@Utente", Utente)
            cmdw.Parameters.AddWithValue("@DataAggiornamento", Now)
            cmdw.Connection = cn
            cmdw.ExecuteNonQuery()
        End If
        myPOSTreader.Close()
        cn.Close()
    End Sub

    Public Sub AggiornaDaTabella(ByVal StringaConnessione As String, ByVal Tabella As System.Data.DataTable)
        Dim cn As OleDbConnection
        Dim MySql As String

        MySql = ""

        cn = New Data.OleDb.OleDbConnection(StringaConnessione)

        cn.Open()


        Dim Transan As OleDbTransaction = cn.BeginTransaction()

        Dim cmd As New OleDbCommand()
        cmd.CommandText = "Delete from IMPORTORETTA where CentroServizio = '" & CENTROSERVIZIO & "' And  CodiceOspite = " & CODICEOSPITE
        cmd.Connection = cn
        cmd.Transaction = Transan
        cmd.ExecuteNonQuery()

        Dim cmdE As New OleDbCommand()
        cmdE.CommandText = "Delete from EXTRAOSPITE where CentroServizio = '" & CENTROSERVIZIO & "' And  CodiceOspite = " & CODICEOSPITE
        cmdE.Connection = cn
        cmdE.Transaction = Transan
        cmdE.ExecuteNonQuery()

        Dim i As Long

        For i = 0 To Tabella.Rows.Count - 1
            If IsDate(Tabella.Rows(i).Item(0)) And Tabella.Rows(i).Item(0) <> "" Then
                MySql = "INSERT INTO IMPORTORETTA (CentroServizio,CodiceOspite,Data,IMPORTO,TIPOIMPORTO,TipoRetta,Utente,DataAggiornamento) VALUES (?,?,?,?,?,?,?,?)"
                Dim cmdw As New OleDbCommand()
                cmdw.CommandText = (MySql)
                cmdw.Parameters.AddWithValue("@CentroServizio", CENTROSERVIZIO)
                cmdw.Parameters.AddWithValue("@CodiceOspite", CODICEOSPITE)
                Dim xData As Date

                xData = Tabella.Rows(i).Item(0)
                cmdw.Parameters.AddWithValue("@Data", xData)
                cmdw.Parameters.AddWithValue("@IMPORTO", CDbl(Tabella.Rows(i).Item(1)))
                cmdw.Parameters.AddWithValue("@TIPOIMPORTO", Tabella.Rows(i).Item(2))

                cmdw.Parameters.AddWithValue("@TipoRetta", Tabella.Rows(i).Item(3))

                cmdw.Parameters.AddWithValue("@Utente", Utente)

                cmdw.Parameters.AddWithValue("@DataAggiornamento", DataAggiornamento)
                cmdw.Transaction = Transan
                cmdw.Connection = cn
                cmdw.ExecuteNonQuery()

                Dim Stringa As String = Tabella.Rows(i).Item(4)
                Dim Vettore(100) As String
                Dim x As Long
                Vettore = Split(Stringa, "|")
                For x = 0 To Vettore.Length - 1
                    If Vettore(x) <> "" Then
                        Dim DecExtra As New Cls_TipoExtraFisso

                        If Vettore(x).IndexOf("codiceiva=") > 0 Then
                            DecExtra.CODICEEXTRA = Trim(Mid(Vettore(x), Vettore(x).IndexOf("codiceiva=") + 12, 2))
                        End If
                        If Vettore(x).IndexOf("(P)") > 0 Then
                            DecExtra.Ripartizione = "P"
                        End If
                        If Vettore(x).IndexOf("(O)") > 0 Then
                            DecExtra.Ripartizione = "O"
                        End If
                        If Vettore(x).IndexOf("(C)") > 0 Then
                            DecExtra.Ripartizione = "C"
                        End If

                        DecExtra.Leggi(StringaConnessione)
                        MySql = "INSERT INTO EXTRAOSPITE (CentroServizio,CodiceOspite,Data,CODICEEXTRA,RIPARTIZIONE) VALUES (?,?,?,?,?)"
                        Dim cmdwE As New OleDbCommand()
                        cmdwE.CommandText = (MySql)
                        cmdwE.Parameters.AddWithValue("@CentroServizio", CENTROSERVIZIO)
                        cmdwE.Parameters.AddWithValue("@CodiceOspite", CODICEOSPITE)


                        xData = Tabella.Rows(i).Item(0)
                        cmdwE.Parameters.AddWithValue("@Data", xData)
                        cmdwE.Parameters.AddWithValue("@CODICEEXTRA", DecExtra.CODICEEXTRA)
                        cmdwE.Parameters.AddWithValue("@RIPARTIZIONE", DecExtra.Ripartizione)
                        cmdwE.Transaction = Transan
                        cmdwE.Connection = cn
                        cmdwE.ExecuteNonQuery()
                    End If
                Next


            End If
        Next
        Transan.Commit()
        cn.Close()

    End Sub


    Public Sub RettaTotaleADataSenzaCSERV(ByVal StringaConnessione As String, ByVal CodiceOspite As Long, ByVal DDate As Date)
        Dim cn As OleDbConnection



        cn = New Data.OleDb.OleDbConnection(StringaConnessione)

        cn.Open()
        Dim cmd As New OleDbCommand()
        cmd.CommandText = ("select * from importoretta where CodiceOspite = " & CodiceOspite & " And Data <= ? Order by Data Desc")
        cmd.Connection = cn
        cmd.Parameters.AddWithValue("@Data", DDate)

        Dim myPOSTreader As OleDbDataReader = cmd.ExecuteReader()
        If myPOSTreader.Read Then

            CodiceOspite = campodb(myPOSTreader.Item("CodiceOspite"))
            Importo = campodb(myPOSTreader.Item("Importo"))
            Tipo = campodb(myPOSTreader.Item("TIPOIMPORTO"))
            Data = campodb(myPOSTreader.Item("Data"))
            TipoRetta = campodb(myPOSTreader.Item("TipoRetta"))
        End If
        myPOSTreader.Close()
        cn.Close()

    End Sub


    Public Sub RettaTotaleAData(ByVal StringaConnessione As String, ByVal CodiceOspite As Long, ByVal CentroServizio As String, ByVal DDate As Date)
        Dim cn As OleDbConnection



        cn = New Data.OleDb.OleDbConnection(StringaConnessione)

        cn.Open()
        Dim cmd As New OleDbCommand()
        cmd.CommandText = ("select * from importoretta where CentroServizio = '" & CentroServizio & "' And  CodiceOspite = " & CodiceOspite & " And Data <= ? Order by Data Desc")
        cmd.Connection = cn
        cmd.Parameters.AddWithValue("@Data", DDate)

        Dim myPOSTreader As OleDbDataReader = cmd.ExecuteReader()
        If myPOSTreader.Read Then

            CodiceOspite = campodb(myPOSTreader.Item("CodiceOspite"))
            Importo = campodb(myPOSTreader.Item("Importo"))
            Tipo = campodb(myPOSTreader.Item("TIPOIMPORTO"))
            Data = campodb(myPOSTreader.Item("Data"))
            TipoRetta = campodb(myPOSTreader.Item("TipoRetta"))
        End If
        myPOSTreader.Close()
        cn.Close()

    End Sub

    Public Sub PrimaDataDopoData(ByVal StringaConnessione As String, ByVal CodiceOspite As Long, ByVal CentroServizio As String, ByVal DDate As Date)
        Dim cn As OleDbConnection



        cn = New Data.OleDb.OleDbConnection(StringaConnessione)

        cn.Open()
        Dim cmd As New OleDbCommand()
        cmd.CommandText = ("select * from importoretta where CentroServizio = '" & CentroServizio & "' And  CodiceOspite = " & CodiceOspite & " And Data > ?")
        cmd.Connection = cn        
        cmd.Parameters.AddWithValue("@Data", DDate)

        Dim myPOSTreader As OleDbDataReader = cmd.ExecuteReader()
        If myPOSTreader.Read Then

            CodiceOspite = campodb(myPOSTreader.Item("CodiceOspite"))
            Importo = campodb(myPOSTreader.Item("Importo"))
            Tipo = campodb(myPOSTreader.Item("TIPOIMPORTO"))
            Data = campodb(myPOSTreader.Item("Data"))
            TipoRetta = campodb(myPOSTreader.Item("TipoRetta"))
        End If
        myPOSTreader.Close()
        cn.Close()

    End Sub

    Public Sub UltimaData(ByVal StringaConnessione As String, ByVal CodiceOspite As Long, ByVal CentroServizio As String)
        Dim cn As OleDbConnection



        cn = New Data.OleDb.OleDbConnection(StringaConnessione)

        cn.Open()
        Dim cmd As New OleDbCommand()
        cmd.CommandText = ("select * from importoretta where CentroServizio = '" & CentroServizio & "' And  CodiceOspite = " & CodiceOspite & " Order by Data Desc")
        cmd.Connection = cn
        Dim sb As StringBuilder = New StringBuilder

        Dim myPOSTreader As OleDbDataReader = cmd.ExecuteReader()
        If myPOSTreader.Read Then

            CodiceOspite = campodb(myPOSTreader.Item("CodiceOspite"))
            Importo = campodb(myPOSTreader.Item("Importo"))
            Tipo = campodb(myPOSTreader.Item("TIPOIMPORTO"))
            Data = campodb(myPOSTreader.Item("Data"))
            TipoRetta = campodb(myPOSTreader.Item("TipoRetta"))
        End If
        myPOSTreader.Close()
        cn.Close()

    End Sub

    Sub loaddati(ByVal StringaConnessione As String, ByVal codiceospite As Integer, ByVal centroservizio As String, ByVal Tabella As System.Data.DataTable)
        Dim cn As OleDbConnection

        cn = New Data.OleDb.OleDbConnection(StringaConnessione)

        cn.Open()
        Dim cmd As New OleDbCommand()
        cmd.CommandText = ("select * from importoretta where centroservizio = '" & centroservizio & "' And CodiceOspite = " & codiceospite & " Order By Data")
        cmd.Connection = cn
        Dim sb As StringBuilder = New StringBuilder


        Tabella.Clear()
        Tabella.Columns.Clear()
        Tabella.Columns.Add("Data", GetType(String))
        Tabella.Columns.Add("Importo", GetType(String))
        Tabella.Columns.Add("Tipo", GetType(String))
        Tabella.Columns.Add("TipoRetta", GetType(String))
        Tabella.Columns.Add("Extra", GetType(String))

        Dim myPOSTreader As OleDbDataReader = cmd.ExecuteReader()
        Do While myPOSTreader.Read

            Try

            
                Dim myriga As System.Data.DataRow = Tabella.NewRow()
                myriga(0) = Format(myPOSTreader.Item("DATA"), "dd/MM/yyyy")
                myriga(1) = Format(Math.Round(campodbN(myPOSTreader.Item("Importo")), 2), "#,##0.00")
                myriga(2) = myPOSTreader.Item("TIPOIMPORTO")
                myriga(3) = myPOSTreader.Item("TipoRetta")


                Dim kE As New Cls_ExtraFisso

                kE.CODICEOSPITE = codiceospite
                kE.CENTROSERVIZIO = centroservizio
                kE.Data = myPOSTreader.Item("DATA")
                myriga(4) = kE.ElencaExtrafissia(StringaConnessione)
                Tabella.Rows.Add(myriga)
            Catch ex As Exception

            End Try
        Loop
        myPOSTreader.Close()
        If Tabella.Rows.Count = 0 Then
            Dim myriga1 As System.Data.DataRow = Tabella.NewRow()
            Dim Mov As New Cls_Movimenti

            Mov.CENTROSERVIZIO = centroservizio
            Mov.CodiceOspite = codiceospite
            Mov.UltimaDataAccoglimento(StringaConnessione)
            myriga1(0) = Mov.Data
            myriga1(1) = 0
            Tabella.Rows.Add(myriga1)
        End If
        cn.Close()
    End Sub
End Class
