Imports Microsoft.VisualBasic
Imports System.Data.OleDb
Imports System.Web.Hosting

Public Class Cls_Modalita
    Public Data As Date    
    Public MODALITA As String
    Public CodiceOspite As Long
    Public CentroServizio As String

    Function campodb(ByVal oggetto As Object) As String
        If IsDBNull(oggetto) Then
            Return ""
        Else
            Return oggetto
        End If
    End Function
    
    Function campodbD(ByVal oggetto As Object) As Date
        If IsDBNull(oggetto) Then
            Return Nothing
        Else
            Return oggetto
        End If
    End Function
    Public Sub EliminaAll(ByVal StringaConnessione As String)
        Dim cn As OleDbConnection
        Dim MySql As String

        MySql = ""

        cn = New Data.OleDb.OleDbConnection(StringaConnessione)

        cn.Open()
        Dim cmd As New OleDbCommand()
        cmd.CommandText = "Delete from MODALITA where CentroServizio = '" & CentroServizio & "' And  CodiceOspite = " & CodiceOspite
        cmd.Connection = cn
        cmd.ExecuteNonQuery()
        cn.Close()
    End Sub
    Public Sub Elimina(ByVal StringaConnessione As String)
        Dim cn As OleDbConnection
        Dim MySql As String

        MySql = ""

        cn = New Data.OleDb.OleDbConnection(StringaConnessione)

        cn.Open()
        Dim cmd As New OleDbCommand()
        cmd.CommandText = "Delete from MODALITA where CentroServizio = '" & CentroServizio & "' And  CodiceOspite = " & CodiceOspite & " And Data = ?"
        cmd.Parameters.AddWithValue("@Data", Data)
        cmd.Connection = cn
        cmd.ExecuteNonQuery()
        cn.Close()
    End Sub

    Public Sub AggiornaDB(ByVal StringaConnessione As String)
        Dim cn As OleDbConnection
        Dim MySql As String

        MySql = ""


        cn = New Data.OleDb.OleDbConnection(StringaConnessione)

        cn.Open()
        Dim cmd As New OleDbCommand()
        cmd.CommandText = ("select * from MODALITA where CentroServizio = '" & CentroServizio & "' And  CodiceOspite = " & CodiceOspite & " And Data = ?")
        cmd.Parameters.AddWithValue("@Data", Data)
        cmd.Connection = cn
        Dim myPOSTreader As OleDbDataReader = cmd.ExecuteReader()
        If myPOSTreader.Read Then
            MySql = "UPDATE MODALITA SET MODALITA = '" & MODALITA & "' WHERE  CentroServizio = '" & CentroServizio & "' And  CodiceOspite = " & CodiceOspite & " And Data = ?"
            Dim cmdw As New OleDbCommand()
            cmdw.CommandText = (MySql)
            REM cmdw.Parameters.AddWithValue("@MODALITA", MODALITA)
            cmdw.Parameters.AddWithValue("@Data", Data)
            cmdw.Connection = cn
            cmdw.ExecuteNonQuery()
        Else
            MySql = "INSERT INTO MODALITA (CentroServizio,CodiceOspite,Data,MODALITA) VALUES (?,?,?,?)"
            Dim cmdw As New OleDbCommand()
            cmdw.CommandText = (MySql)
            cmdw.Parameters.AddWithValue("@CentroServizio", CentroServizio)
            cmdw.Parameters.AddWithValue("@CodiceOspite", CodiceOspite)
            cmdw.Parameters.AddWithValue("@Data", Data)
            cmdw.Parameters.AddWithValue("@MODALITA", MODALITA)
            cmdw.Connection = cn
            cmdw.ExecuteNonQuery()
        End If
        myPOSTreader.Close()
        cn.Close()
    End Sub

    Public Sub UltimaData(ByVal StringaConnessione As String, ByVal CodiceOspite As Long, ByVal CentroServizio As String)
        Dim cn As OleDbConnection



        cn = New Data.OleDb.OleDbConnection(StringaConnessione)

        cn.Open()
        Dim cmd As New OleDbCommand()
        cmd.CommandText = ("select * from MODALITA where CentroServizio = '" & CentroServizio & "' And  CodiceOspite = " & CodiceOspite & " Order by Data Desc")
        cmd.Connection = cn
        Dim sb As StringBuilder = New StringBuilder

        Dim myPOSTreader As OleDbDataReader = cmd.ExecuteReader()
        If myPOSTreader.Read Then

            CodiceOspite = campodb(myPOSTreader.Item("CodiceOspite"))

            MODALITA = campodb(myPOSTreader.Item("MODALITA"))
            Data = campodbD(myPOSTreader.Item("DATA"))

        End If
        myPOSTreader.Close()
        cn.Close()

    End Sub

    Sub loaddati(ByVal StringaConnessione As String, ByVal codiceospite As Integer, ByVal centroservizio As String, ByVal Tabella As System.Data.DataTable)
        Dim cn As OleDbConnection


        cn = New Data.OleDb.OleDbConnection(StringaConnessione)

        cn.Open()
        Dim cmd As New OleDbCommand()
        cmd.CommandText = ("select * from MODALITA where centroservizio = '" & centroservizio & "' And CodiceOspite = " & codiceospite & " Order by Data")
        cmd.Connection = cn
        Dim sb As StringBuilder = New StringBuilder

        Tabella.Clear()
        Tabella.Columns.Clear()

        Tabella.Columns.Add("Data", GetType(String))
        Tabella.Columns.Add("MODALITA", GetType(String))


        Dim myPOSTreader As OleDbDataReader = cmd.ExecuteReader()
        Do While myPOSTreader.Read

            Dim myriga As System.Data.DataRow = Tabella.NewRow()
            myriga(0) = Format(campodbD(myPOSTreader.Item("DATA")), "dd/MM/yyyy")
            myriga(1) = campodb(myPOSTreader.Item("MODALITA"))

            Tabella.Rows.Add(myriga)
        Loop
        myPOSTreader.Close()
        If Tabella.Rows.Count = 0 Then
            Dim myrigaV As System.Data.DataRow = Tabella.NewRow()

            Dim Mov As New Cls_Movimenti

            Mov.CENTROSERVIZIO = centroservizio
            Mov.CodiceOspite = codiceospite
            Mov.UltimaDataAccoglimento(StringaConnessione)
            myrigaV(0) = Mov.Data

            Tabella.Rows.Add(myrigaV)
        End If
        cn.Close()
    End Sub
End Class
