Imports Microsoft.VisualBasic
Imports System.Data.OleDb

Public Class Cls_CalcoloRette
    REM C:\WINDOWS\Microsoft.NET\Framework\v2.0.50727>REGASM D:\Inetpub\wwwroot\provascipt\Bin\Interop.MSScriptControl.dll
    REM Microsoft (R) .NET Framework Assembly Registration Utility 2.0.50727.3053
    REM Copyright (C) Microsoft Corporation 1998-2004.  All rights reserved.4

    REM Types registered successfully

    REM E' necessario definire un applicazione per poter usare scriptcontrol e questa operazione "REGASM" non so se � necessaria.

    Public MyTabCausale(31) As String
    Public MyTabGiornoAssenza(31) As Integer
    Public MyTabGiorniRipetizioneCausale(31) As Integer
    Public MyTabGiorniRipetizioneCausaleNC(31) As Integer
    Public MyTabGiorniRipetizioneCausaleACSum(31) As Integer

    Public MyTabCodiceProv(31) As String
    Public MyTabCodiceComune(31) As String


    Public MyTabCodiceProvJolly(31) As String
    Public MyTabCodiceJolly(31) As String

    Public MyTabCodiceRegione(31) As String
    Public MyTabRegioneTipoImporto(31) As String

    Public MyTabAutoSufficente(31) As String

    Public MyTabImportoOspite(31) As Single
    Public MyTabImportoOspite2(31) As Single
    Public MyTabImportoComune(31) As Single

    Public MyTabImportoJolly(31) As Single

    Public MyTabImportoRegione(31) As Single
    Public MyTabImportoRetta(31) As Single
    Public MyTabImportoParente(31, 10) As Single
    Public MyTabImportoParente2(31, 10) As Single
    Public MyTabPercentuale(31, 10) As Single
    Public MyTabModalita(31) As String
    Public MyTabImpExtrOspite(31, 20) As Single
    Public MyTabImpExtrParent(31, 20) As Single
    Public MyTabImpExtrComune(31, 20) As Single

    Public MyTabImpExtrJolly(31, 20) As Single

    Public MyTabGiornalieroExtraImporto1(31) As Double
    Public MyTabGiornalieroExtraImporto2(31) As Double
    Public MyTabGiornalieroExtraImporto3(31) As Double
    Public MyTabGiornalieroExtraImporto4(31) As Double


    Public MyTabGiornalieroExtraImporto1J(31) As Double
    Public MyTabGiornalieroExtraImporto2J(31) As Double
    Public MyTabGiornalieroExtraImporto3J(31) As Double
    Public MyTabGiornalieroExtraImporto4J(31) As Double


    Public MyTabGiornalieroExtraImporto1C(31) As Double
    Public MyTabGiornalieroExtraImporto2C(31) As Double
    Public MyTabGiornalieroExtraImporto3C(31) As Double
    Public MyTabGiornalieroExtraImporto4C(31) As Double

    Public MyTabGiornalieroExtraImporto1P(31, 10) As Double
    Public MyTabGiornalieroExtraImporto2P(31, 10) As Double
    Public MyTabGiornalieroExtraImporto3P(31, 10) As Double
    Public MyTabGiornalieroExtraImporto4P(31, 10) As Double

    Public MyTabCodiceExtrOspite(20) As String
    Public MyTabCodiceExtrOspiteGIORNI(20) As Integer
    Public MyTabCodiceNonExtrOspiteGIORNI(20) As Integer

    Public MyTabCodiceExtrParent(20) As String

    Public MyTabCodiceExtrParentGIORNI(10, 20) As Integer
    Public MyTabCodiceExtrParentNonGIORNI(10, 20) As Integer

    Public MyTabCodiceExtrComune(10) As String

    Public MyTabCodiceExtrComuneGIORNI(10) As Integer

    Public MyTabCodiceExtrJolly(10) As String


    Public MyTabAddebitoOspite(40) As Single
    Public MyTabAccreditoOspite(40) As Single



    Public MyTabAddebitoMeseCompetenza(40) As Integer
    Public MyTabAddebitoAnnoCompetenza(40) As Integer
    Public MyTabAccreditoMeseCompetenza(40) As Integer
    Public MyTabAccreditoAnnoCompetenza(40) As Integer



    Public MyTabAddebitoOspiteRegistrazione(40) As Long
    Public MyTabAccreditoOspiteRegistrazione(40) As Long

    Public MyTabAddebitoParente(10, 40) As Single
    Public MyTabAccreditoParente(10, 40) As Single


    Public MyTabAddebitoParenteMeseCompetenza(10, 40) As Integer
    Public MyTabAddebitoParenteAnnoCompetenza(10, 40) As Integer
    Public MyTabAccreditoParenteMeseCompetenza(10, 40) As Integer
    Public MyTabAccreditoParenteAnnoCompetenza(10, 40) As Integer

    Public MyTabAddebitoParenteRegistrazione(10, 40) As Long
    Public MyTabAccreditoParenteRegistrazione(10, 40) As Long

    Public MyTabAddebitoComune(10, 40) As Single
    Public MyTabAccreditoComune(10, 40) As Single

    Public MyTabAddebitoOspiteQty(40) As Single
    Public MyTabAddebitoParenteQty(10, 40) As Single


    Public MyTabAddebitoQty(10, 40) As Single

    Public MyTabAddebitoJolly(10, 40) As Single
    Public MyTabAccreditoJolly(10, 40) As Single


    Public MyTabAddebitoRegione(10, 40) As Single
    Public MyTabAccreditoRegione(10, 40) As Single

    Public MyTabDescrizioneAddebitoOspite(40) As String
    Public MyTabDescrizioneAccreditoOspite(40) As String

    Public MyTabDescrizioneAddebitoParente(10, 40) As String
    Public MyTabDescrizioneAccreditoParente(10, 40) As String


    Public MyTabCodiceIvaAddebitoOspite(40) As String
    Public MyTabCodiceIvaAccreditoOspite(40) As String

    Public MyTabCodiceIvaAddebitoParente(10, 40) As String
    Public MyTabCodiceIvaAccreditoParente(10, 40) As String


    Public MyTabDescrizioneAddebitoComune(10, 32) As String
    Public MyTabDescrizioneAccreditoComune(10, 32) As String

    Public MyTabCodiceIvaAddebitoComune(10, 32) As String
    Public MyTabCodiceIvaAccreditoComune(10, 32) As String

    Public MyTabDescrizioneAddebitoJolly(10, 40) As String
    Public MyTabDescrizioneAccreditoJolly(10, 40) As String

    Public MyTabCodiceIvaAddebitoJolly(10, 40) As String
    Public MyTabCodiceIvaAccreditoJolly(10, 40) As String

    Public MyTabDescrizioneAddebitoRegione(10, 40) As String
    Public MyTabDescrizioneAccreditoRegione(10, 40) As String


    Public MyTabCodiceIvaAddebitoRegione(10, 40) As String
    Public MyTabCodiceIvaAccreditoRegione(10, 40) As String

    Public MyTabOspitiMensile As Single
    Public MyTabParenteMensile(10) As Single


    Public PrimoDataAccoglimentoOspite As Date

    Public ImportoAddebitoPrimoComune As Double

    Public ImportoAddebitoPrimoJolly As Double

    Public ImportoAddebitoPrimoRegione As Double
    Public TipoAddebitoPrimoRegione As String


    Public TipoAddebitoPrimoComune As String

    Public TipoAddebitoPrimoJolly As String

    Public ForzaRPXOspite As Double

    Public ForzaRPXParente(10) As Double

    Public Accolto As Boolean
    Public Dimesso As Boolean
    Public Prm_AzzeraSeNegativo As Long
    Public CampoParametriGIORNOUSCITA As String
    Public CampoParametriGIORNOENTRATA As String
    Public CampoParametriCAUSALEACCOGLIMENTO As String
    Public CampoParametriMastroAnticipo As Long

    Public CampoParametriAddebitiAccreditiComulati As Long

    Public XCodicRegione(410) As String
    Public XTipoRegione(410) As String
    Public XImportoRegione(410, 31) As Double
    Public UltimoCodiceRegione As Integer

    Public StringaDegliErrori As String
    Public ImportoMensileOspite As Double

    Public ImportoMensileComune As Double

    Public ImportoMensileJolly As Double

    Public ImportoMensileRetta As Double
    Public ImportoMensileParente(10) As Double
    Public ImportoMensileParente2(10) As Double

    Public ImportoExtrOspite(10) As Double
    Public ImportoExtrParent(10) As Double
    Public ImportoExtrComune(10) As Double

    Public MensileImportoExtrOspite(20) As Double
    Public MensileImportoExtrParent(20) As Double
    Public MensileImportoExtrComune(20) As Double

    Public ImportoExtrJolly(10) As Double


    Public ImpExtrComMan1(10) As Double
    Public ImpExtrComMan2(10) As Double
    Public ImpExtrComMan3(10) As Double
    Public ImpExtrComMan4(10) As Double

    Public NumExtrComMan1(10) As Integer
    Public NumExtrComMan2(10) As Integer
    Public NumExtrComMan3(10) As Integer
    Public NumExtrComMan4(10) As Integer

    Public TipoExtrComMan1(10) As String
    Public TipoExtrComMan2(10) As String
    Public TipoExtrComMan3(10) As String
    Public TipoExtrComMan4(10) As String


    Public ImpExtrParMan1(10) As Double
    Public ImpExtrParMan2(10) As Double
    Public ImpExtrParMan3(10) As Double
    Public ImpExtrParMan4(10) As Double


    Public TipoExtrParMan1(10) As String
    Public TipoExtrParMan2(10) As String
    Public TipoExtrParMan3(10) As String
    Public TipoExtrParMan4(10) As String


    Public NumExtrParMan1(10) As Integer
    Public NumExtrParMan2(10) As Integer
    Public NumExtrParMan3(10) As Integer
    Public NumExtrParMan4(10) As Integer

    Public ImpExtrOspMan1 As Double
    Public ImpExtrOspMan2 As Double
    Public ImpExtrOspMan3 As Double
    Public ImpExtrOspMan4 As Double

    Public NumExtrOspMan1 As Integer
    Public NumExtrOspMan2 As Integer
    Public NumExtrOspMan3 As Integer
    Public NumExtrOspMan4 As Integer

    Public TipoAddExtrOspMan1 As String
    Public TipoAddExtrOspMan2 As String
    Public TipoAddExtrOspMan3 As String
    Public TipoAddExtrOspMan4 As String



    Public MaxAddebitoOspite As Integer
    Public MaxAccreditoOspite As Integer
    Public MaxAddebitoParente(10) As Integer
    Public MaxAccreditoParente(10) As Integer

    Public MaxAddebitoComune(10) As Integer
    Public MaxAccreditoComune(10) As Integer

    Public MaxAddebitoJolly(10) As Integer
    Public MaxAccreditoJolly(10) As Integer

    Public MaxAddebitoRegione(10) As Integer
    Public MaxAccreditoRegione(10) As Integer

    Public MaxParenti As Integer

    Public MaxRegione As Integer


    Public SalvaImpExtrOspite(20) As Double
    Public SalvaImpExtrParent(20) As Double
    Public SalvaImpExtrComune(20) As Double


    Public SalvaImpExtrJolly(20) As Double


    Public ImpExtraOspite(20) As Single, NonImpExtraOspite(20) As Single
    Public ImpExtraParente(10, 20) As Single, NonImpExtraParente(10, 20) As Single
    Public ImpExtraComune(10, 20) As Single, NonImpExtraComune(10, 20) As Single

    Public ImpExtraJolly(10, 20) As Single, NonImpExtraJolly(10, 20) As Single



    Public ImportoPresParente(10) As Single, ImportoAssParente(10) As Single
    Public ImportoPresParente2(10) As Single, ImportoAssParente2(10) As Single

    Public ImportoPresComune(10) As Single, ImportoAssComune(10) As Single

    Public ImportoPresJolly(10) As Single, ImportoAssJolly(10) As Single


    Public ImportoPresRegione(10) As Single, ImportoAssRegione(10) As Single
    Public TipoImportoPresRegione(10) As String, TipoImportoAssRegione(10) As String
    Public ImportoPresOspite As Single, ImportoAssOspite As Single

    Public ImportoPresOspite2 As Single, ImportoAssOspite2 As Single

    Public ImportoPresEnte As Single, ImportoAssEnte As Single
    Public GiornoRientro As Byte
    Public NonImportoPresParente(10) As Single, NonImportoAssParente(10) As Single

    Public NonImportoPresParente2(10) As Single, NonImportoAssParente2(10) As Single

    Public NonImportoPresComune(10) As Single, NonImportoAssComune(10) As Single

    Public NonImportoPresJolly(10) As Single, NonImportoAssJolly(10) As Single


    Public NonImportoPresRegione(10) As Single, NonImportoAssRegione(10) As Single
    Public NonImportoPresOspite As Single, NonImportoAssOspite As Single

    Public NonImportoPresOspite2 As Single, NonImportoAssOspite2 As Single

    Public NonImportoPresEnte As Single, NonImportoAssEnte As Single

    Public GiorniPres As Integer, GiorniAss As Integer
    Public GiorniPresEnte As Integer, GiorniAssEnte As Integer
    Public GiorniPresParente(10) As Integer, GiorniAssParente(10) As Integer

    Public GiorniPresComune(10) As Integer, GiorniAssComune(10) As Integer

    Public GiorniPresJolly(10) As Integer, GiorniAssJolly(10) As Integer

    Public GiorniPresRegione(10) As Integer, GiorniAssRegione(10) As Integer
    Public NonGiorniPres As Integer, NonGiorniAss As Integer
    Public NonGiorniPresEnte As Integer, NonGiorniAssEnte As Integer
    Public NonGiorniPresParente(10) As Integer, NonGiorniAssParente(10) As Integer

    Public NonGiorniPresComune(10) As Integer, NonGiorniAssComune(10) As Integer

    Public NonGiorniPresJolly(10) As Integer, NonGiorniAssJolly(10) As Integer


    Public NonGiorniPresRegione(10) As Integer, NonGiorniAssRegione(10) As Integer
    Public SalvaImportoRegione As Double, SalvaImportoComune As Double, SalvaImportoOspite As Double, SalvaImportoTotale As Double, SalvaPercentuale As Double, SalvaDifferenza As Double, SalvaImportoJolly As Double

    Public XSO As Single, XSP As Single, XSC As Single

    Public mGiorniPres As Long, mGiorniAss As Long

    Public GiorniPresExtraC(10) As Long

    Public CodiceComune(10) As String, CodiceRegione(10) As String, TipoRetta(10) As String
    Public CodiceJolly(10) As String


    Public RicalcoloGriglia_CServ(5000) As String
    Public RicalcoloGriglia_CodiceOspite(5000) As Long
    Public RicalcoloGriglia_NumeroMovimento(5000) As Long
    Public RicalcoloGriglia_Anno(5000) As Long
    Public RicalcoloGriglia_Mese(5000) As Long
    Public RicalcoloGriglia_Ospite(5000) As Double
    Public RicalcoloGriglia_Parente_id(5000) As Integer
    Public RicalcoloGriglia_Parente(5000) As Double
    Public RicalcoloGriglia_Comune(5000) As Double
    Public RicalcoloGriglia_Jolly(5000) As Double
    Public RicalcoloGriglia_Regione(5000) As Double
    Public RicalcoloGriglia_Descrizione(5000) As String
    Public IndiceVariazione As Long

    Private OspitiDb As New ADODB.Connection
    Private OspitiAccessoriDb As New ADODB.Connection


    Private OspitiCon As OleDbConnection
    Private OspitiAccessoriCon As OleDbConnection

    Public STRINGACONNESSIONEDB As String

    Public ConnessioneGenerale As String

    Public ConnessioneTabelle As String

    Public ConguaglioMinimo As Double = 0


    Public QuoteDaSoia As Integer = 0

    Private Vt_CausaleCodice(1000) As String
    Private Vt_CausaleRegola(1000) As String
    Private Vt_CausaleCheck(1000) As String
    Private Vt_CausaleTimpoMovimento(1000) As String
    Private Vt_GiornoUscita(1000) As String
    Private MaxCausali As Integer

    Private GiornoCambioRettaOspite As Integer = 0
    Private GiornoCambioRettaParenti(100) As Integer

    Private BloccaConguaglioDaRegola As Integer

    Dim VtFestivita(10000, 31) As Integer


    Public Sub CaricaCausali()
        Dim cmdCausale As New OleDbCommand()
        MaxCausali = 0
        cmdCausale.CommandText = "Select * From Causali Order by Codice"
        cmdCausale.Connection = OspitiCon
        Dim RDCausale As OleDbDataReader = cmdCausale.ExecuteReader()
        Do While RDCausale.Read()
            MaxCausali = MaxCausali + 1
            Vt_CausaleCodice(MaxCausali) = campodb(RDCausale.Item("Codice"))
            Vt_CausaleRegola(MaxCausali) = campodb(RDCausale.Item("Regole"))
            Vt_CausaleCheck(MaxCausali) = campodb(RDCausale.Item("ChekCausale"))

            Vt_GiornoUscita(MaxCausali) = campodb(RDCausale.Item("GiornoUscita"))
            Vt_CausaleTimpoMovimento(MaxCausali) = campodb(RDCausale.Item("TIPOMOVIMENTO"))
        Loop
        RDCausale.Close()
    End Sub

    Private Function NumeroDaCodice(ByVal codice As String) As Integer
        Dim i As Integer

        For i = 0 To MaxCausali
            If Vt_CausaleCodice(i) = codice Then
                Return i
                Exit For
            End If
        Next
    End Function


    Public Sub ChiudiDB()
        OspitiDb.Close()
        OspitiAccessoriDb.Close()

        OspitiCon.Close()
        OspitiAccessoriCon.Close()
    End Sub

    Public Sub ApriDB(ByVal stringaconnessione As String, ByVal stringaconnessioneOspitiAccessori As String)
        OspitiDb.Open(stringaconnessione)
        OspitiAccessoriDb.Open(stringaconnessioneOspitiAccessori)

        OspitiCon = New Data.OleDb.OleDbConnection(stringaconnessione)

        OspitiCon.Open()


        OspitiAccessoriCon = New Data.OleDb.OleDbConnection(stringaconnessioneOspitiAccessori)

        OspitiAccessoriCon.Open()
    End Sub
    Public Function PrimaDataAccoglimento(ByVal Cserv As String, ByVal CodOsp As Long, ByVal Mese As Integer, ByVal Anno As Integer) As Date
        Dim MySql As String




        MySql = "Select top 1 * From Movimenti Where CENTROSERVIZIO = '" & Cserv & "' And CODICEOSPITE =" & CodOsp & " And TIPOMOV =  '05' And  Data <= {ts '" & Format(DateSerial(Anno, Mese, GiorniMese(Mese, Anno)), "yyyy-MM-dd") & " 00:00:00'} And  Data >= {ts '" & Format(DateSerial(Anno, Mese, 1), "yyyy-MM-dd") & " 00:00:00'} Order By DATA "

        Dim cmd As New OleDbCommand()
        cmd.CommandText = MySql
        cmd.Connection = OspitiCon

        Dim myPOSTreader As OleDbDataReader = cmd.ExecuteReader()
        If myPOSTreader.Read Then
            PrimaDataAccoglimento = campodbd(myPOSTreader.Item("Data"))
            MySql = "Select top 1 * From Movimenti Where CENTROSERVIZIO = '" & Cserv & "' And CODICEOSPITE =" & CodOsp & " And TIPOMOV =  '13' And  Data <= {ts '" & Format(DateSerial(Anno, Mese, GiorniMese(Mese, Anno)), "yyyy-MM-dd") & " 00:00:00'} And  Data >= {ts '" & Format(DateSerial(Anno, Mese, 1), "yyyy-MM-dd") & " 00:00:00'} Order By DATA "

            Dim cmdUscita As New OleDbCommand()
            cmdUscita.CommandText = MySql
            cmdUscita.Connection = OspitiCon

            Dim ReadUscita As OleDbDataReader = cmdUscita.ExecuteReader()
            If ReadUscita.Read Then
                'If Format(PrimaDataAccoglimento, "yyyyMMdd") = Format(campodbd(ReadUscita.Item("Data")).AddDays(1), "yyyyMMdd") Then
                If Format(PrimaDataAccoglimento, "yyyyMMdd") > Format(campodbd(ReadUscita.Item("Data")), "yyyyMMdd") Then
                    MySql = "Select top 1 * From Movimenti Where CENTROSERVIZIO = '" & Cserv & "' And CODICEOSPITE =" & CodOsp & " And TIPOMOV =  '05' And  Data < {ts '" & Format(PrimaDataAccoglimento, "yyyy-MM-dd") & " 00:00:00'} Order By DATA Desc"

                    Dim cmdPrimoAcco As New OleDbCommand()
                    cmdPrimoAcco.CommandText = MySql
                    cmdPrimoAcco.Connection = OspitiCon

                    Dim ReadAcco As OleDbDataReader = cmdPrimoAcco.ExecuteReader()
                    If ReadAcco.Read Then
                        PrimaDataAccoglimento = campodbd(ReadAcco.Item("Data"))
                    End If
                    ReadAcco.Close()
                End If
            End If
            ReadUscita.Close()
        End If
        myPOSTreader.Close()



    End Function

    Public Function DataAccoglimento(ByVal Cserv As String, ByVal CodOsp As Long, ByVal Mese As Integer, ByVal Anno As Integer) As Date
        Dim MySql As String




        MySql = "Select top 1 * From Movimenti Where CENTROSERVIZIO = '" & Cserv & "' And CODICEOSPITE =" & CodOsp & " And TIPOMOV =  '05' And  Data < {ts '" & Format(DateSerial(Anno, Mese, GiorniMese(Mese, Anno)), "yyyy-MM-dd") & " 00:00:00'} Order By DATA Desc"

        Dim cmd As New OleDbCommand()
        cmd.CommandText = MySql
        cmd.Connection = OspitiCon

        Dim myPOSTreader As OleDbDataReader = cmd.ExecuteReader()
        If myPOSTreader.Read Then
            DataAccoglimento = campodbd(myPOSTreader.Item("Data"))
        Else
            myPOSTreader.Close()
            MySql = "Select top 1 * From Movimenti Where CENTROSERVIZIO = '" & Cserv & "' And CODICEOSPITE =" & CodOsp & " And TIPOMOV =  '05' Order By DATA Desc"

            Dim cmd1 As New OleDbCommand()
            cmd1.CommandText = MySql
            cmd1.Connection = OspitiCon

            Dim myPOSTreader1 As OleDbDataReader = cmd1.ExecuteReader()
            If myPOSTreader1.Read Then
                DataAccoglimento = campodbd(myPOSTreader1.Item("Data"))
            End If
        End If
        myPOSTreader.Close()
    End Function


    Private Function ImportoRegione(ByVal MiaRegione As String, ByVal TipoRetta As String, ByVal Giorno As Long, ByVal Mese As Long, ByVal Anno As Long, ByVal Cserv As String) As Double
        Dim I As Integer
        Dim MySql As String
        Dim Fine As Integer
        Dim ImportoAppoggio As Double = 0

        For I = 1 To UltimoCodiceRegione
            If XCodicRegione(I) = MiaRegione And XTipoRegione(I) = TipoRetta Then
                ImportoRegione = XImportoRegione(I, Giorno)
                Exit Function
            End If
        Next I
        UltimoCodiceRegione = UltimoCodiceRegione + 1
        XCodicRegione(UltimoCodiceRegione) = MiaRegione
        XTipoRegione(UltimoCodiceRegione) = TipoRetta

        If Trim(TipoRetta) = "" Then
            MySql = "Select Importo,TIPOIMPORTO,Data From ImportoRegioni Where CODICEREGIONE = '" & MiaRegione & "' and  DATA <= {ts '" & Format(DateSerial(Anno, Mese, 31), "yyyy-MM-dd") & " 00:00:00'} Order by Data DESC"
        Else
            MySql = "Select Importo,TIPOIMPORTO,Data From ImportoRegioni Where CODICEREGIONE = '" & MiaRegione & "' and  TipoRetta = '" & TipoRetta & "' And DATA <= {ts '" & Format(DateSerial(Anno, Mese, 31), "yyyy-MM-dd") & " 00:00:00'} Order by Data DESC"
        End If
        Dim Entrato As Boolean = False

        Fine = 31
        Dim cmd As New OleDbCommand()
        cmd.CommandText = MySql
        cmd.Connection = OspitiCon
        Dim myPOSTreader As OleDbDataReader = cmd.ExecuteReader()
        Do While myPOSTreader.Read
            If Format(campodbd(myPOSTreader.Item("Data")), "yyyyMMdd") <= Format(DateSerial(Anno, Mese, 1), "yyyyMMdd") Then

                If campodb(myPOSTreader.Item("TIPOIMPORTO")) = "G" Then
                    ImportoAppoggio = campodbN(myPOSTreader.Item("Importo"))
                End If

                If campodb(myPOSTreader.Item("TIPOIMPORTO")) = "M" Then
                    ImportoAppoggio = Mensile(campodbN(myPOSTreader.Item("Importo")), Mese, Anno, Cserv)
                End If


                For I = 1 To Fine
                    If XImportoRegione(UltimoCodiceRegione, I) = 0 Then
                        XImportoRegione(UltimoCodiceRegione, I) = ImportoAppoggio
                    End If
                Next
                Entrato = True
                Exit Do
            End If
            If Format(campodbd(myPOSTreader.Item("Data")), "yyyyMMdd") > Format(DateSerial(Anno, Mese, 1), "yyyyMMdd") Then

                If campodb(myPOSTreader.Item("TIPOIMPORTO")) = "G" Then
                    ImportoAppoggio = campodbN(myPOSTreader.Item("Importo"))
                End If

                If campodb(myPOSTreader.Item("TIPOIMPORTO")) = "M" Then
                    ImportoAppoggio = Mensile(campodbN(myPOSTreader.Item("Importo")), Mese, Anno, Cserv)
                End If

                'ImportoAppoggio = campodbN(myPOSTreader.Item("Importo"))
                For I = Day(campodbd(myPOSTreader.Item("Data"))) To Fine
                    XImportoRegione(UltimoCodiceRegione, I) = ImportoAppoggio
                Next
                Entrato = True
                Fine = 31
            End If
        Loop
        myPOSTreader.Close()
        If Entrato = False Then
            Dim Regione As New ClsUSL

            Regione.CodiceRegione = MiaRegione
            Regione.Leggi(STRINGACONNESSIONEDB)
            StringaDegliErrori = StringaDegliErrori & "Regione non trovata o importo regione non indicato per il periodo : " & MiaRegione & " " & Regione.Nome & " - " & TipoRetta & vbNewLine
        End If

        'For I = 1 To 31

        '    If Trim(TipoRetta) = "" Then
        '        MySql = "Select top 1 Importo From ImportoRegioni Where CODICEREGIONE = '" & MiaRegione & "' and  DATA <= {ts '" & Format(DateSerial(Anno, Mese, I), "yyyy-MM-dd") & " 00:00:00'} Order by Data DESC"
        '    Else
        '        MySql = "Select top 1 Importo From ImportoRegioni Where CODICEREGIONE = '" & MiaRegione & "' and  TipoRetta = '" & TipoRetta & "' And DATA <= {ts '" & Format(DateSerial(Anno, Mese, I), "yyyy-MM-dd") & " 00:00:00'} Order by Data DESC"
        '    End If

        '    Dim cmd As New OleDbCommand()
        '    cmd.CommandText = MySql
        '    cmd.Connection = OspitiCon
        '    Dim myPOSTreader As OleDbDataReader = cmd.ExecuteReader()
        '    If myPOSTreader.Read Then
        '        XImportoRegione(UltimoCodiceRegione, I) = campodbN(myPOSTreader.Item("Importo"))
        '    Else
        '        StringaDegliErrori = StringaDegliErrori & "Regione non trovata : " & MiaRegione & "-" & TipoRetta & vbNewLine
        '    End If
        '    myPOSTreader.Close()
        'Next I
        If XCodicRegione(UltimoCodiceRegione) = MiaRegione Then
            ImportoRegione = XImportoRegione(UltimoCodiceRegione, Giorno)
            Exit Function
        End If
    End Function


    Public Sub DeleteChiave(ByVal Cserv As String, ByVal CodOsp As Long, ByVal Mese As Integer, ByVal Anno As Integer)

        Dim DelAdb As New Cls_AddebitiAccrediti

        DelAdb.DeleteChiave(STRINGACONNESSIONEDB, CodOsp, Cserv, Mese, Anno)
    End Sub

    Public Function GiornoFestivo(ByVal Cserv As String, ByVal Data As Date) As Boolean

        Dim CservCls As New Cls_CentroServizio

        CservCls.Leggi(STRINGACONNESSIONEDB, Cserv)

        'Or Data.DayOfWeek = DayOfWeek.Saturday 
        If Data.DayOfWeek = DayOfWeek.Sunday Then
            GiornoFestivo = True
            Exit Function
        End If

        Dim Mese As Integer = Month(Data)
        Dim Giorno As Integer = Day(Data)


        If Mese = 1 And (Giorno = 1 Or Giorno = 6) Then
            If CservCls.GIORNO0101 <> "S" Then
                If Giorno = 1 Then
                    GiornoFestivo = True
                    Exit Function
                End If
            End If
            If CservCls.GIORNO0601 <> "S" Then
                If Giorno = 6 Then
                    GiornoFestivo = True
                    Exit Function
                End If
            End If
        End If
        If Mese = 3 Then
            If CservCls.GIORNO1903 <> "S" Then
                If Giorno = 3 Then
                    GiornoFestivo = True
                    Exit Function
                End If
            End If
        End If
        If Mese = 4 Then
            If CservCls.GIORNO2504 <> "S" Then
                If Giorno = 25 Then
                    GiornoFestivo = True
                    Exit Function
                End If
            End If
        End If
        If Mese = 5 Then
            If CservCls.GIORNO0105 <> "S" Then
                If Giorno = 25 Then
                    GiornoFestivo = True
                    Exit Function
                End If
            End If
        End If

        If Mese = 6 Then
            If CservCls.GIORNO0206 <> "S" Then
                If Giorno = 2 Then
                    GiornoFestivo = True
                    Exit Function
                End If
            End If
            If CservCls.GIORNO2906 <> "S" Then
                If Giorno = 29 Then
                    GiornoFestivo = True
                    Exit Function
                End If
            End If
        End If
        If Mese = 8 Then
            If CservCls.GIORNO1508 <> "S" Then
                If Giorno = 15 Then
                    GiornoFestivo = True
                    Exit Function
                End If
            End If
        End If
        If Mese = 11 Then
            If CservCls.GIORNO0111 <> "S" Then
                If Giorno = 1 Then
                    GiornoFestivo = True
                    Exit Function
                End If
            End If
            If CservCls.GIORNO0411 <> "S" Then
                If Giorno = 4 Then
                    GiornoFestivo = True
                    Exit Function
                End If
            End If
        End If
        If Mese = 12 Then
            If CservCls.GIORNO0812 <> "S" Then
                If Giorno = 8 Then
                    GiornoFestivo = True
                    Exit Function
                End If
            End If
            If CservCls.GIORNO2512 <> "S" Then
                If Giorno = 25 Then
                    GiornoFestivo = True
                    Exit Function
                End If
            End If
            If CservCls.GIORNO2612 <> "S" Then
                If Giorno = 26 Then
                    GiornoFestivo = True
                    Exit Function
                End If
            End If
        End If


        If CservCls.GIORNO1ATTIVO <> "S" Then
            If Not IsDBNull(CservCls.GIORNO1) Then
                If Val(Mid(CservCls.GIORNO1, 4, 2)) = Mese Then
                    If Giorno = Val(Mid(CservCls.GIORNO1, 1, 2)) Then
                        GiornoFestivo = True
                        Exit Function
                    End If
                End If
            End If
        End If

        If CservCls.GIORNO2ATTIVO <> "S" Then
            If Not IsDBNull(CservCls.GIORNO2) Then
                If Val(Mid(CservCls.GIORNO2, 4, 2)) = Mese Then
                    If Giorno = Val(Mid(CservCls.GIORNO2, 1, 2)) Then
                        GiornoFestivo = True
                        Exit Function
                    End If
                End If
            End If
        End If

        'If CampoCentroServizio(Cserv, "GIORNO3ATTIVO") <> "S" Then
        '    If Not IsDBNull(CampoCentroServizio(Cserv, "GIORNO3")) Then
        '        If Val(Mid(CampoCentroServizio(Cserv, "GIORNO3"), 4, 2)) = Mese Then
        '            If Giorno = Val(Mid(CampoCentroServizio(Cserv, "GIORNO3"), 1, 2)) Then
        '                GiornoFestivo = True
        '                Exit Function
        '            End If
        '        End If
        '    End If
        'End If

        'If CampoCentroServizio(Cserv, "GIORNO4ATTIVO") <> "S" Then
        '    If Not IsDBNull(CampoCentroServizio(Cserv, "GIORNO4")) Then
        '        If Val(Mid(CampoCentroServizio(Cserv, "GIORNO4"), 4, 2)) = Mese Then
        '            If Giorno = Val(Mid(CampoCentroServizio(Cserv, "GIORNO4"), 1, 2)) Then
        '                GiornoFestivo = True
        '                Exit Function
        '            End If
        '        End If
        '    End If
        'End If

        'If CampoCentroServizio(Cserv, "GIORNO5ATTIVO") <> "S" Then
        '    If Not IsDBNull(CampoCentroServizio(Cserv, "GIORNO5")) Then
        '        If Val(Mid(CampoCentroServizio(Cserv, "GIORNO5"), 4, 2)) = Mese Then
        '            If Giorno = Val(Mid(CampoCentroServizio(Cserv, "GIORNO5"), 1, 2)) Then
        '                GiornoFestivo = True
        '                Exit Function
        '            End If
        '        End If
        '    End If
        'End If

    End Function

    Public Sub CreaAddebitiAcrreditiProgrammati(ByVal Cserv As String, ByVal CodOsp As Long, ByVal Mese As Integer, ByVal Anno As Integer)

        Dim cmd As New OleDbCommand()
        cmd.CommandText = "Select * From AddebitoAccreditiProgrammati Where DataInizio <= ? And DataFine >= ? And CodiceOspite = ? And CentroServizio = ? "
        cmd.Connection = OspitiCon
        cmd.Parameters.AddWithValue("@DataInizio", DateSerial(Anno, Mese, GiorniMese(Mese, Anno)))
        cmd.Parameters.AddWithValue("@DataFine", DateSerial(Anno, Mese, 1))
        cmd.Parameters.AddWithValue("@CodiceOspite", CodOsp)
        cmd.Parameters.AddWithValue("@Cserv", Cserv)

        Dim myRd As OleDbDataReader = cmd.ExecuteReader()
        Do While myRd.Read
            If campodb(myRd.Item("Ripetizione")) = "M" Then

                Dim AddM As New Cls_AddebitiAccrediti

                AddM.CENTROSERVIZIO = Cserv
                AddM.CodiceOspite = CodOsp
                AddM.chiaveselezione = campodb(myRd.Item("ID"))
                AddM.MESECOMPETENZA = Mese
                AddM.ANNOCOMPETENZA = Anno
                AddM.Elaborato = 0
                AddM.LeggiChiave(STRINGACONNESSIONEDB, AddM.CodiceOspite, AddM.CENTROSERVIZIO, AddM.chiaveselezione)

                If AddM.Elaborato = 0 Then
                    AddM.MESECOMPETENZA = Mese
                    AddM.ANNOCOMPETENZA = Anno
                    AddM.TipoMov = campodb(myRd.Item("Tipo"))

                    If campodb(myRd.Item("Rendiconto")) = "1" Then
                        AddM.RETTA = "0"
                    End If

                    AddM.CodiceIva = campodb(myRd.Item("TipoAddebito"))
                    AddM.Data = DateSerial(Anno, Mese, GiorniMese(Mese, Anno))
                    AddM.Descrizione = campodb(myRd.Item("Descrizione"))
                    AddM.IMPORTO = campodb(myRd.Item("Importo"))
                    AddM.PARENTE = Val(campodb(myRd.Item("Parente")))
                    AddM.RIFERIMENTO = campodb(myRd.Item("Riferimento"))
                    AddM.PROVINCIA = campodb(myRd.Item("Provincia"))
                    AddM.COMUNE = campodb(myRd.Item("Comune"))
                    AddM.REGIONE = campodb(myRd.Item("CodiceRegione"))
                    AddM.AggiornaDB(STRINGACONNESSIONEDB)
                End If

            End If

            If campodb(myRd.Item("Ripetizione")) = "N" Then

                Dim Indice As Integer
                Dim GiornoInzio As Integer
                Dim GiornoFine As Integer
                Dim Presenze As Integer = 0

                GiornoInzio = 1
                If Month(campodbd(myRd.Item("DataInizio"))) = Mese And Month(campodbd(myRd.Item("DataInizio"))) = Anno Then
                    GiornoInzio = Day(campodbd(myRd.Item("DataInizio")))
                End If

                GiornoFine = GiorniMese(Mese, Anno)
                If Month(campodbd(myRd.Item("DataFine"))) = Mese And Month(campodbd(myRd.Item("DataFine"))) = Anno Then
                    GiornoFine = Day(campodbd(myRd.Item("DataFine")))
                End If

                For Indice = GiornoInzio To GiornoFine
                    If MyTabCausale(Indice) <> "" Then
                        Dim CauS As New Cls_CausaliEntrataUscita

                        CauS.Codice = MyTabCausale(Indice)
                        CauS.LeggiCausale(STRINGACONNESSIONEDB)
                        If CauS.TIPOMOVIMENTO = "P" Then
                            Presenze = Presenze + 1
                        End If
                    Else
                        Presenze = Presenze + 1
                    End If
                Next


                Dim AddM As New Cls_AddebitiAccrediti

                AddM.CENTROSERVIZIO = Cserv
                AddM.CodiceOspite = CodOsp
                AddM.chiaveselezione = campodb(myRd.Item("ID"))
                AddM.MESECOMPETENZA = Mese
                AddM.ANNOCOMPETENZA = Anno
                AddM.Elaborato = 0
                AddM.LeggiChiave(STRINGACONNESSIONEDB, AddM.CodiceOspite, AddM.CENTROSERVIZIO, AddM.chiaveselezione)

                If AddM.Elaborato = 0 Then
                    AddM.MESECOMPETENZA = Mese
                    AddM.ANNOCOMPETENZA = Anno
                    AddM.TipoMov = campodb(myRd.Item("Tipo"))

                    AddM.CodiceIva = campodb(myRd.Item("TipoAddebito"))
                    AddM.Data = DateSerial(Anno, Mese, GiorniMese(Mese, Anno))
                    AddM.Descrizione = campodb(myRd.Item("Descrizione"))
                    AddM.IMPORTO = campodb(myRd.Item("Importo"))

                    If Presenze < GiorniMese(Mese, Anno) Then
                        AddM.IMPORTO = Modulo.MathRound(Mensile(campodbN(myRd.Item("Importo")), Mese, Anno, Cserv) * Presenze, 0)

                    End If

                    If campodb(myRd.Item("Rendiconto")) = "1" Then
                        AddM.RETTA = "0"
                    End If

                    AddM.PARENTE = Val(campodb(myRd.Item("Parente")))
                    AddM.RIFERIMENTO = campodb(myRd.Item("Riferimento"))
                    AddM.PROVINCIA = campodb(myRd.Item("Provincia"))
                    AddM.COMUNE = campodb(myRd.Item("Comune"))
                    AddM.REGIONE = campodb(myRd.Item("CodiceRegione"))
                    AddM.AggiornaDB(STRINGACONNESSIONEDB)
                End If
            End If

            If campodb(myRd.Item("Ripetizione")) = "G" Then

                Dim AddM As New Cls_AddebitiAccrediti

                AddM.CENTROSERVIZIO = Cserv
                AddM.CodiceOspite = CodOsp
                AddM.chiaveselezione = campodb(myRd.Item("ID"))
                AddM.MESECOMPETENZA = Mese
                AddM.ANNOCOMPETENZA = Anno
                AddM.Elaborato = 0
                AddM.LeggiChiave(STRINGACONNESSIONEDB, AddM.CodiceOspite, AddM.CENTROSERVIZIO, AddM.chiaveselezione)

                If AddM.Elaborato = 0 Then
                    AddM.MESECOMPETENZA = Mese
                    AddM.ANNOCOMPETENZA = Anno
                    AddM.TipoMov = campodb(myRd.Item("Tipo"))
                    AddM.CodiceIva = campodb(myRd.Item("TipoAddebito"))
                    AddM.Data = DateSerial(Anno, Mese, GiorniMese(Mese, Anno))
                    AddM.Descrizione = campodb(myRd.Item("Descrizione"))
                    AddM.IMPORTO = campodb(myRd.Item("Importo")) * GiorniMese(Mese, Anno)
                    AddM.PARENTE = Val(campodb(myRd.Item("Parente")))
                    AddM.RIFERIMENTO = campodb(myRd.Item("Riferimento"))
                    AddM.PROVINCIA = campodb(myRd.Item("Provincia"))
                    AddM.COMUNE = campodb(myRd.Item("Comune"))
                    AddM.REGIONE = campodb(myRd.Item("CodiceRegione"))
                    If campodb(myRd.Item("Rendiconto")) = "1" Then
                        AddM.RETTA = "0"
                    End If

                    AddM.AggiornaDB(STRINGACONNESSIONEDB)
                End If
            End If


            If campodb(myRd.Item("Ripetizione")) = "A" Then

                Dim Indice As Integer
                Dim GiornoInzio As Integer
                Dim GiornoFine As Integer
                Dim Assente As Integer = 0

                GiornoInzio = 1
                If Month(campodbd(myRd.Item("DataInizio"))) = Mese And Month(campodbd(myRd.Item("DataInizio"))) = Anno Then
                    GiornoInzio = Day(campodbd(myRd.Item("DataInizio")))
                End If

                GiornoFine = GiorniMese(Mese, Anno)
                If Month(campodbd(myRd.Item("DataFine"))) = Mese And Month(campodbd(myRd.Item("DataFine"))) = Anno Then
                    GiornoFine = Day(campodbd(myRd.Item("DataFine")))
                End If

                For Indice = GiornoInzio To GiornoFine
                    If MyTabCausale(Indice) <> "" Then
                        Dim CauS As New Cls_CausaliEntrataUscita

                        CauS.Codice = campodb(MyTabCausale(Indice))
                        CauS.LeggiCausale(STRINGACONNESSIONEDB)
                        If CauS.TIPOMOVIMENTO = "A" Then
                            Assente = Assente + 1
                        End If
                    End If
                Next


                Dim AddM As New Cls_AddebitiAccrediti

                AddM.CENTROSERVIZIO = Cserv
                AddM.CodiceOspite = CodOsp
                AddM.chiaveselezione = campodb(myRd.Item("ID"))
                AddM.Elaborato = 0
                AddM.MESECOMPETENZA = Mese
                AddM.ANNOCOMPETENZA = Anno
                AddM.LeggiChiave(STRINGACONNESSIONEDB, AddM.CodiceOspite, AddM.CENTROSERVIZIO, AddM.chiaveselezione)

                If AddM.Elaborato = 0 Then
                    AddM.MESECOMPETENZA = Mese
                    AddM.ANNOCOMPETENZA = Anno
                    AddM.CodiceIva = campodb(myRd.Item("TipoAddebito"))
                    AddM.TipoMov = campodb(myRd.Item("Tipo"))
                    AddM.Data = DateSerial(Anno, Mese, GiorniMese(Mese, Anno))
                    AddM.Descrizione = campodb(myRd.Item("Descrizione"))
                    AddM.IMPORTO = Modulo.MathRound(campodb(myRd.Item("Importo")) * Assente, 2)
                    AddM.PARENTE = Val(campodb(myRd.Item("Parente")))
                    AddM.RIFERIMENTO = campodb(myRd.Item("Riferimento"))
                    AddM.PROVINCIA = campodb(myRd.Item("Provincia"))
                    AddM.COMUNE = campodb(myRd.Item("Comune"))
                    AddM.REGIONE = campodb(myRd.Item("CodiceRegione"))
                    If campodb(myRd.Item("Rendiconto")) = "1" Then
                        AddM.RETTA = "0"
                    End If

                    AddM.AggiornaDB(STRINGACONNESSIONEDB)
                End If
            End If

            If campodb(myRd.Item("Ripetizione")) = "P" Then

                Dim Indice As Integer
                Dim GiornoInzio As Integer
                Dim GiornoFine As Integer
                Dim Presenze As Integer = 0

                GiornoInzio = 1
                If Month(campodbd(myRd.Item("DataInizio"))) = Mese And Month(campodbd(myRd.Item("DataInizio"))) = Anno Then
                    GiornoInzio = Day(campodbd(myRd.Item("DataInizio")))
                End If

                GiornoFine = GiorniMese(Mese, Anno)
                If Month(campodbd(myRd.Item("DataFine"))) = Mese And Month(campodbd(myRd.Item("DataFine"))) = Anno Then
                    GiornoFine = Day(campodbd(myRd.Item("DataFine")))
                End If

                For Indice = GiornoInzio To GiornoFine
                    If MyTabCausale(Indice) <> "" Then
                        Dim CauS As New Cls_CausaliEntrataUscita

                        CauS.Codice = MyTabCausale(Indice)
                        CauS.LeggiCausale(STRINGACONNESSIONEDB)
                        If CauS.TIPOMOVIMENTO = "P" Then
                            Presenze = Presenze + 1
                        End If
                    Else
                        Presenze = Presenze + 1
                    End If
                Next


                Dim AddM As New Cls_AddebitiAccrediti

                AddM.CENTROSERVIZIO = Cserv
                AddM.CodiceOspite = CodOsp
                AddM.chiaveselezione = campodb(myRd.Item("ID"))
                AddM.Elaborato = 0
                AddM.MESECOMPETENZA = Mese
                AddM.ANNOCOMPETENZA = Anno
                AddM.LeggiChiave(STRINGACONNESSIONEDB, AddM.CodiceOspite, AddM.CENTROSERVIZIO, AddM.chiaveselezione)

                If AddM.Elaborato = 0 Then
                    AddM.MESECOMPETENZA = Mese
                    AddM.ANNOCOMPETENZA = Anno
                    AddM.CodiceIva = campodb(myRd.Item("TipoAddebito"))
                    AddM.TipoMov = campodb(myRd.Item("Tipo"))
                    AddM.Data = DateSerial(Anno, Mese, GiorniMese(Mese, Anno))
                    AddM.Descrizione = campodb(myRd.Item("Descrizione"))
                    AddM.IMPORTO = Modulo.MathRound(campodb(myRd.Item("Importo")) * Presenze, 2)
                    AddM.PARENTE = Val(campodb(myRd.Item("Parente")))
                    AddM.RIFERIMENTO = campodb(myRd.Item("Riferimento"))
                    AddM.PROVINCIA = campodb(myRd.Item("Provincia"))
                    AddM.COMUNE = campodb(myRd.Item("Comune"))
                    AddM.REGIONE = campodb(myRd.Item("CodiceRegione"))
                    If campodb(myRd.Item("Rendiconto")) = "1" Then
                        AddM.RETTA = "0"
                    End If

                    AddM.AggiornaDB(STRINGACONNESSIONEDB)
                End If
            End If

            If campodb(myRd.Item("Ripetizione")) = "L" Then

                Dim Indice As Integer
                Dim GiornoInzio As Integer
                Dim GiornoFine As Integer
                Dim Presenze As Integer = 0

                GiornoInzio = 1
                If Month(campodbd(myRd.Item("DataInizio"))) = Mese And Month(campodbd(myRd.Item("DataInizio"))) = Anno Then
                    GiornoInzio = Day(campodbd(myRd.Item("DataInizio")))
                End If

                GiornoFine = GiorniMese(Mese, Anno)
                If Month(campodbd(myRd.Item("DataFine"))) = Mese And Month(campodbd(myRd.Item("DataFine"))) = Anno Then
                    GiornoFine = Day(campodbd(myRd.Item("DataFine")))
                End If

                For Indice = GiornoInzio To GiornoFine
                    If MyTabCausale(Indice) <> "" Then
                        Dim CauS As New Cls_CausaliEntrataUscita

                        CauS.Codice = MyTabCausale(Indice)
                        CauS.LeggiCausale(STRINGACONNESSIONEDB)
                        If CauS.TIPOMOVIMENTO = "P" Then
                            If Not GiornoFestivo(Cserv, DateSerial(Anno, Mese, Indice)) Then
                                Presenze = Presenze + 1
                            End If
                        End If
                    Else
                        If Not GiornoFestivo(Cserv, DateSerial(Anno, Mese, Indice)) Then
                            Presenze = Presenze + 1
                        End If

                    End If
                Next


                Dim AddM As New Cls_AddebitiAccrediti

                AddM.CENTROSERVIZIO = Cserv
                AddM.CodiceOspite = CodOsp
                AddM.chiaveselezione = campodb(myRd.Item("ID"))
                AddM.Elaborato = 0
                AddM.MESECOMPETENZA = Mese
                AddM.ANNOCOMPETENZA = Anno
                AddM.LeggiChiave(STRINGACONNESSIONEDB, AddM.CodiceOspite, AddM.CENTROSERVIZIO, AddM.chiaveselezione)

                If AddM.Elaborato = 0 Then
                    AddM.MESECOMPETENZA = Mese
                    AddM.ANNOCOMPETENZA = Anno
                    AddM.CodiceIva = campodb(myRd.Item("TipoAddebito"))
                    AddM.TipoMov = campodb(myRd.Item("Tipo"))
                    AddM.Data = DateSerial(Anno, Mese, GiorniMese(Mese, Anno))
                    AddM.Descrizione = campodb(myRd.Item("Descrizione"))
                    AddM.IMPORTO = Modulo.MathRound(campodb(myRd.Item("Importo")) * Presenze, 2)
                    AddM.PARENTE = Val(campodb(myRd.Item("Parente")))
                    AddM.RIFERIMENTO = campodb(myRd.Item("Riferimento"))
                    AddM.PROVINCIA = campodb(myRd.Item("Provincia"))
                    AddM.COMUNE = campodb(myRd.Item("Comune"))
                    AddM.REGIONE = campodb(myRd.Item("CodiceRegione"))
                    If campodb(myRd.Item("Rendiconto")) = "1" Then
                        AddM.RETTA = "0"
                    End If

                    AddM.AggiornaDB(STRINGACONNESSIONEDB)
                End If
            End If

            If campodb(myRd.Item("Ripetizione")) = "F" Then

                Dim Indice As Integer
                Dim GiornoInzio As Integer
                Dim GiornoFine As Integer
                Dim Presenze As Integer = 0

                GiornoInzio = 1
                If Month(campodbd(myRd.Item("DataInizio"))) = Mese And Month(campodbd(myRd.Item("DataInizio"))) = Anno Then
                    GiornoInzio = Day(campodbd(myRd.Item("DataInizio")))
                End If

                GiornoFine = GiorniMese(Mese, Anno)
                If Month(campodbd(myRd.Item("DataFine"))) = Mese And Month(campodbd(myRd.Item("DataFine"))) = Anno Then
                    GiornoFine = Day(campodbd(myRd.Item("DataFine")))
                End If

                For Indice = GiornoInzio To GiornoFine
                    If MyTabCausale(Indice) <> "" Then
                        Dim CauS As New Cls_CausaliEntrataUscita

                        CauS.Codice = MyTabCausale(Indice)
                        CauS.LeggiCausale(STRINGACONNESSIONEDB)
                        If CauS.TIPOMOVIMENTO = "P" Then
                            If GiornoFestivo(Cserv, DateSerial(Anno, Mese, Indice)) Then
                                Presenze = Presenze + 1
                            End If
                        End If
                    Else
                        If GiornoFestivo(Cserv, DateSerial(Anno, Mese, Indice)) Then
                            Presenze = Presenze + 1
                        End If

                    End If
                Next


                Dim AddM As New Cls_AddebitiAccrediti

                AddM.CENTROSERVIZIO = Cserv
                AddM.CodiceOspite = CodOsp
                AddM.chiaveselezione = campodb(myRd.Item("ID"))
                AddM.Elaborato = 0
                AddM.MESECOMPETENZA = Mese
                AddM.ANNOCOMPETENZA = Anno
                AddM.LeggiChiave(STRINGACONNESSIONEDB, AddM.CodiceOspite, AddM.CENTROSERVIZIO, AddM.chiaveselezione)

                If AddM.Elaborato = 0 Then
                    AddM.MESECOMPETENZA = Mese
                    AddM.ANNOCOMPETENZA = Anno
                    AddM.CodiceIva = campodb(myRd.Item("TipoAddebito"))
                    AddM.TipoMov = campodb(myRd.Item("Tipo"))
                    AddM.Data = DateSerial(Anno, Mese, GiorniMese(Mese, Anno))
                    AddM.Descrizione = campodb(myRd.Item("Descrizione"))
                    AddM.IMPORTO = Modulo.MathRound(campodb(myRd.Item("Importo")) * Presenze, 2)
                    AddM.PARENTE = Val(campodb(myRd.Item("Parente")))
                    AddM.RIFERIMENTO = campodb(myRd.Item("Riferimento"))
                    AddM.PROVINCIA = campodb(myRd.Item("Provincia"))
                    AddM.COMUNE = campodb(myRd.Item("Comune"))
                    AddM.REGIONE = campodb(myRd.Item("CodiceRegione"))
                    If campodb(myRd.Item("Rendiconto")) = "1" Then
                        AddM.RETTA = "0"
                    End If

                    AddM.AggiornaDB(STRINGACONNESSIONEDB)
                End If
            End If

        Loop
        myRd.Close()


        Dim NonEntrare As Boolean = False

        Dim cmd2 As New OleDbCommand()
        cmd2.CommandText = "SELECT * FROM MOVIMENTI WHERE  CENTROSERVIZIO <> '" & Cserv & "' AND CODICEOSPITE  = " & CodOsp & " And DATA >= ? And  DATA <= ? AND [TIPOMOV] = '05'"
        cmd2.Parameters.AddWithValue("@Data1", DateSerial(Anno, Mese, 1))
        cmd2.Parameters.AddWithValue("@Data2", DateSerial(Anno, Mese, GiorniMese(Mese, Anno)))
        cmd2.Connection = OspitiCon
        Dim myRd2 As OleDbDataReader = cmd2.ExecuteReader()
        If myRd2.Read Then
            NonEntrare = True
        End If
        myRd2.Close()

        Dim cmd3 As New OleDbCommand()
        cmd3.CommandText = "SELECT * FROM MOVIMENTI WHERE  CENTROSERVIZIO = '" & Cserv & "' AND CODICEOSPITE  = " & CodOsp & " Order by Data Desc "
        cmd3.Connection = OspitiCon
        Dim myRd3 As OleDbDataReader = cmd3.ExecuteReader()
        If myRd3.Read Then
            If campodb(myRd3.Item("TIPOMOV")) = "13" And Format(campodb(myRd3.Item("DATA")), "yyyy-MM-dd") < Format(DateSerial(Anno, Mese, 1), "yyyy-MM-dd") Then
                NonEntrare = True
            End If
        End If
        myRd3.Close()
        If NonEntrare Then
            Dim cmd4 As New OleDbCommand()
            cmd4.CommandText = "SELECT * FROM MOVIMENTI WHERE  CODICEOSPITE  = " & CodOsp & " And DATA >= ? AND [TIPOMOV] = '13' order by Data Desc"
            cmd4.Parameters.AddWithValue("@Data1", DateSerial(Anno, Mese, 1))            
            cmd4.Connection = OspitiCon
            Dim myRd4 As OleDbDataReader = cmd4.ExecuteReader()
            If myRd4.Read Then
                If campodb(myRd4.Item("CENTROSERVIZIO")) = Cserv Then
                    Dim cmd5 As New OleDbCommand()
                    cmd5.CommandText = "SELECT * FROM MOVIMENTI WHERE  CENTROSERVIZIO <> '" & Cserv & "' AND CODICEOSPITE  = " & CodOsp & " And DATA >= ? And  DATA <= ? AND [TIPOMOV] = '13' order by Data Desc"
                    cmd5.Parameters.AddWithValue("@Data1", DateSerial(Anno, Mese, 1))
                    cmd5.Parameters.AddWithValue("@Data2", DateSerial(Anno, Mese, GiorniMese(Mese, Anno)))
                    cmd5.Connection = OspitiCon
                    Dim myRd5 As OleDbDataReader = cmd5.ExecuteReader()
                    If myRd5.Read Then
                        NonEntrare = False
                    End If
                End If
            End If
            myRd4.Close()
        End If

        If Not NonEntrare Then
            Dim Param As New Cls_Parametri

            Param.LeggiParametri(STRINGACONNESSIONEDB)



            Dim cmd1 As New OleDbCommand()

            If Param.DenaroDaPeriodo = 0 Then
                cmd1.CommandText = "SELECT * FROM DENARO WHERE CODICEOSPITE  = " & CodOsp & " And DATAREGISTRAZIONE >= ? And  DATAREGISTRAZIONE <= ? AND (Ripartizione = 'O' OR Ripartizione = 'P') AND TipoOperazione = 'P'"
                cmd1.Parameters.AddWithValue("@Data1", DateSerial(Anno, Mese, 1))
                cmd1.Parameters.AddWithValue("@Data2", DateSerial(Anno, Mese, GiorniMese(Mese, Anno)))
                cmd1.Connection = OspitiAccessoriCon
            Else
                cmd1.CommandText = "SELECT * FROM DENARO WHERE CODICEOSPITE  = " & CodOsp & " And AnnoCompetenza = ? And  MeseCompetenza = ? AND (Ripartizione = 'O' OR Ripartizione = 'P') AND TipoOperazione = 'P'"
                cmd1.Parameters.AddWithValue("@AnnoCompetenza", Anno)
                cmd1.Parameters.AddWithValue("@MeseCompetenza", Mese)
                cmd1.Connection = OspitiAccessoriCon
            End If
            Dim myRd1 As OleDbDataReader = cmd1.ExecuteReader()

            Do While myRd1.Read
                If campodb(myRd1.Item("Ripartizione")) = "O" Then
                    Dim AddM As New Cls_AddebitiAccrediti

                    AddM.CENTROSERVIZIO = Cserv
                    AddM.CodiceOspite = CodOsp
                    AddM.chiaveselezione = campodb(myRd1.Item("ID"))


                    AddM.MESECOMPETENZA = Mese
                    AddM.ANNOCOMPETENZA = Anno
                    AddM.CodiceIva = campodb(myRd1.Item("TipoAddebito"))
                    AddM.TipoMov = "AD"
                    AddM.Data = DateSerial(Anno, Mese, GiorniMese(Mese, Anno))
                    AddM.Descrizione = campodb(myRd1.Item("Descrizione"))
                    AddM.IMPORTO = campodbN(myRd1.Item("Importo"))
                    AddM.PARENTE = 0
                    AddM.RIFERIMENTO = "O"
                    AddM.PROVINCIA = ""
                    AddM.COMUNE = ""
                    AddM.REGIONE = ""

                    If AddM.CodiceOspite = 526 Then
                        AddM.CodiceOspite = 526
                    End If


                    AddM.AggiornaDB(STRINGACONNESSIONEDB)

                End If
                If campodb(myRd1.Item("Ripartizione")) = "P" Then
                    Dim AddM As New Cls_AddebitiAccrediti

                    AddM.CENTROSERVIZIO = Cserv
                    AddM.CodiceOspite = CodOsp
                    AddM.chiaveselezione = campodb(myRd1.Item("ID"))

                    AddM.MESECOMPETENZA = Mese
                    AddM.ANNOCOMPETENZA = Anno
                    AddM.CodiceIva = campodb(myRd1.Item("TipoAddebito"))
                    AddM.TipoMov = "AD"
                    AddM.Data = DateSerial(Anno, Mese, GiorniMese(Mese, Anno))
                    AddM.Descrizione = campodb(myRd1.Item("Descrizione"))
                    AddM.IMPORTO = campodbN(myRd1.Item("Importo"))
                    AddM.PARENTE = campodbN(myRd1.Item("Parente"))
                    AddM.RIFERIMENTO = "P"
                    AddM.PROVINCIA = ""
                    AddM.COMUNE = ""
                    AddM.REGIONE = ""


                    AddM.AggiornaDB(STRINGACONNESSIONEDB)
                End If

            Loop
            myRd1.Close()
        End If
    End Sub


    Public Sub CreaAddebitiAcrrediti(ByVal Cserv As String, ByVal CodOsp As Long, ByVal Mese As Integer, ByVal Anno As Integer, Optional ByVal VERIFICADATA As Boolean = True)
        Dim MyRs As New ADODB.Recordset
        Dim RsDenaro As New ADODB.Recordset
        Dim MyRs2 As New ADODB.Recordset
        Dim MySql As String = ""
        Dim Param As New Cls_Parametri


        Param.LeggiParametri(STRINGACONNESSIONEDB)

        Dim X As Integer

        If VERIFICADATA = True Then

            MySql = "SelecT * FROM ADDACR WHERE CENTROSERVIZIO = '" & Cserv & "' AND CODICEOSPITE  = " & CodOsp & " And month(DATA) = " & Mese & " And Year(DATA) = " & Anno & " And (Elaborato = 0 or Elaborato Is Null) "
        Else
            MySql = "SelecT * FROM ADDACR WHERE CENTROSERVIZIO = '" & Cserv & "' AND CODICEOSPITE  = " & CodOsp & " And month(DATA) = " & Mese & " And Year(DATA) = " & Anno & " And (Elaborato = 0 or Elaborato Is Null) "
        End If

        Dim cmd As New OleDbCommand()
        cmd.CommandText = MySql
        cmd.Connection = OspitiCon

        Dim myRd As OleDbDataReader = cmd.ExecuteReader()
        
        Do While myRd.Read

            If campodb(myRd.Item("Retta")) <> "O" Then
                If campodb(myRd.Item("TipoMov")) = "AD" And campodb(myRd.Item("Riferimento")) = "O" Then
                    If Val(campodb(myRd.Item("raggruppa"))) = 1 Then
                        Dim Trovato As Boolean = False
                        For X = 1 To MaxAddebitoOspite
                            If MyTabCodiceIvaAddebitoOspite(X) = campodb(myRd.Item("CodiceIva")) Then
                                MyTabAddebitoOspite(X) = MyTabAddebitoOspite(X) + campodbN(myRd.Item("Importo"))
                                MyTabAddebitoOspiteQty(X) = MyTabAddebitoOspiteQty(X) + campodbN(myRd.Item("Quantita"))
                                MyTabAddebitoOspiteRegistrazione(X) = Val(campodb(myRd.Item("NumeroRegistrazione")))

                                If Param.TipoAddebitoFatturePrivati = 0 Then
                                    Dim MyTipoAdd As New Cls_Addebito

                                    MyTipoAdd.Codice = campodb(myRd.Item("CodiceIva"))
                                    MyTipoAdd.Leggi(STRINGACONNESSIONEDB, MyTipoAdd.Codice)

                                    MyTabDescrizioneAddebitoOspite(X) = MyTipoAdd.Descrizione
                                End If

                                MyTabCodiceIvaAddebitoOspite(X) = campodb(myRd.Item("CodiceIva"))
                                Trovato = True
                            End If
                        Next
                        If Trovato = False Then
                            MaxAddebitoOspite = MaxAddebitoOspite + 1
                            MyTabAddebitoOspite(MaxAddebitoOspite) = MyTabAddebitoOspite(MaxAddebitoOspite) + campodbN(myRd.Item("Importo"))
                            MyTabAddebitoOspiteRegistrazione(MaxAddebitoOspite) = Val(campodb(myRd.Item("NumeroRegistrazione")))
                            MyTabAddebitoOspiteQty(MaxAddebitoOspite) = MyTabAddebitoOspiteQty(MaxAddebitoOspite) + campodbN(myRd.Item("Quantita"))
                            If Param.TipoAddebitoFatturePrivati = 0 Then
                                Dim MyTipoAdd As New Cls_Addebito

                                MyTipoAdd.Codice = campodb(myRd.Item("CodiceIva"))
                                MyTipoAdd.Leggi(STRINGACONNESSIONEDB, MyTipoAdd.Codice)

                                MyTabDescrizioneAddebitoOspite(MaxAddebitoOspite) = MyTipoAdd.Descrizione
                            End If

                            MyTabCodiceIvaAddebitoOspite(MaxAddebitoOspite) = campodb(myRd.Item("CodiceIva"))
                        End If
                    Else
                        MaxAddebitoOspite = MaxAddebitoOspite + 1
                        MyTabAddebitoOspite(MaxAddebitoOspite) = MyTabAddebitoOspite(MaxAddebitoOspite) + campodbN(myRd.Item("Importo"))
                        MyTabAddebitoOspiteRegistrazione(MaxAddebitoOspite) = Val(campodb(myRd.Item("NumeroRegistrazione")))
                        MyTabDescrizioneAddebitoOspite(MaxAddebitoOspite) = campodb(myRd.Item("Descrizione"))
                        MyTabCodiceIvaAddebitoOspite(MaxAddebitoOspite) = campodb(myRd.Item("CodiceIva"))
                        MyTabAddebitoOspiteQty(MaxAddebitoOspite) = MyTabAddebitoOspiteQty(MaxAddebitoOspite) + campodbN(myRd.Item("Quantita"))
                    End If
                End If
                If campodb(myRd.Item("TipoMov")) = "AD" And campodb(myRd.Item("Riferimento")) = "P" Then
                    If Val(campodb(myRd.Item("raggruppa"))) = 1 Then
                        Dim Trovato As Boolean = False
                        For X = 1 To MaxAddebitoParente(campodbN(myRd.Item("Parente")))
                            If MyTabCodiceIvaAddebitoParente(campodbN(myRd.Item("Parente")), X) = campodb(myRd.Item("CodiceIva")) Then
                                MyTabAddebitoParente(campodbN(myRd.Item("Parente")), X) = MyTabAddebitoParente(campodbN(myRd.Item("Parente")), X) + campodbN(myRd.Item("Importo"))
                                MyTabAddebitoParenteRegistrazione(campodbN(myRd.Item("Parente")), X) = campodbN(myRd.Item("NumeroRegistrazione"))
                                MyTabAddebitoParenteQty(campodbN(myRd.Item("Parente")), X) = MyTabAddebitoParenteQty(campodbN(myRd.Item("Parente")), X) + campodbN(myRd.Item("Quantita"))

                                If Param.TipoAddebitoFatturePrivati = 0 Then
                                    Dim MyTipoAdd As New Cls_Addebito

                                    MyTipoAdd.Codice = campodb(myRd.Item("CodiceIva"))
                                    MyTipoAdd.Leggi(STRINGACONNESSIONEDB, MyTipoAdd.Codice)

                                    MyTabDescrizioneAddebitoParente(campodbN(myRd.Item("Parente")), X) = MyTipoAdd.Descrizione
                                End If
                                MyTabCodiceIvaAddebitoParente(campodbN(myRd.Item("Parente")), X) = campodb(myRd.Item("CodiceIva"))
                                Trovato = True
                            End If
                        Next
                        If Trovato = False Then
                            MaxAddebitoParente(campodbN(myRd.Item("Parente"))) = MaxAddebitoParente(campodbN(myRd.Item("Parente"))) + 1
                            MyTabAddebitoParente(campodbN(myRd.Item("Parente")), MaxAddebitoParente(campodbN(myRd.Item("Parente")))) = MyTabAddebitoParente(campodbN(myRd.Item("Parente")), MaxAddebitoParente(campodbN(myRd.Item("Parente")))) + campodbN(myRd.Item("Importo"))
                            MyTabAddebitoParenteRegistrazione(campodbN(myRd.Item("Parente")), MaxAddebitoParente(campodbN(myRd.Item("Parente")))) = campodbN(myRd.Item("NumeroRegistrazione"))
                            MyTabAddebitoParenteQty(campodbN(myRd.Item("Parente")), MaxAddebitoParente(campodbN(myRd.Item("Parente")))) = MyTabAddebitoParenteQty(campodbN(myRd.Item("Parente")), MaxAddebitoParente(campodbN(myRd.Item("Parente")))) + campodbN(myRd.Item("Quantita"))


                            If Param.TipoAddebitoFatturePrivati = 0 Then
                                Dim MyTipoAdd As New Cls_Addebito

                                MyTipoAdd.Codice = campodb(myRd.Item("CodiceIva"))
                                MyTipoAdd.Leggi(STRINGACONNESSIONEDB, MyTipoAdd.Codice)

                                MyTabDescrizioneAddebitoParente(campodbN(myRd.Item("Parente")), MaxAddebitoParente(campodbN(myRd.Item("Parente")))) = MyTipoAdd.Descrizione
                            End If
                            MyTabCodiceIvaAddebitoParente(campodbN(myRd.Item("Parente")), MaxAddebitoParente(campodbN(myRd.Item("Parente")))) = campodb(myRd.Item("CodiceIva"))
                            If MaxParenti < campodbN(myRd.Item("Parente")) Then
                                MaxParenti = campodbN(myRd.Item("Parente"))
                            End If
                        End If
                    Else
                        MaxAddebitoParente(campodbN(myRd.Item("Parente"))) = MaxAddebitoParente(campodbN(myRd.Item("Parente"))) + 1
                        MyTabAddebitoParente(campodbN(myRd.Item("Parente")), MaxAddebitoParente(campodbN(myRd.Item("Parente")))) = MyTabAddebitoParente(campodbN(myRd.Item("Parente")), MaxAddebitoParente(campodbN(myRd.Item("Parente")))) + campodbN(myRd.Item("Importo"))
                        MyTabAddebitoParenteRegistrazione(campodbN(myRd.Item("Parente")), MaxAddebitoParente(campodbN(myRd.Item("Parente")))) = campodbN(myRd.Item("NumeroRegistrazione"))
                        MyTabDescrizioneAddebitoParente(campodbN(myRd.Item("Parente")), MaxAddebitoParente(campodbN(myRd.Item("Parente")))) = campodb(myRd.Item("Descrizione"))
                        MyTabCodiceIvaAddebitoParente(campodbN(myRd.Item("Parente")), MaxAddebitoParente(campodbN(myRd.Item("Parente")))) = campodb(myRd.Item("CodiceIva"))
                        MyTabAddebitoParenteQty(campodbN(myRd.Item("Parente")), MaxAddebitoParente(campodbN(myRd.Item("Parente")))) = campodbN(myRd.Item("Quantita"))

                        If MaxParenti < campodbN(myRd.Item("Parente")) Then
                            MaxParenti = campodbN(myRd.Item("Parente"))
                        End If
                    End If
                End If
                If campodb(myRd.Item("TipoMov")) = "AD" And campodb(myRd.Item("Riferimento")) = "C" Then
                    X = TrovaComune(campodb(myRd.Item("Provincia")) & campodb(myRd.Item("Comune")))
                    If X > 10 Then
                        Call InserisciComune(campodb(myRd.Item("Provincia")) & campodb(myRd.Item("Comune")))
                        X = TrovaComune(campodb(myRd.Item("Provincia")) & campodb(myRd.Item("Comune")))
                    End If
                    If X > 10 Then
                        StringaDegliErrori = StringaDegliErrori & "Comune non trovato in Addebito per Ospite " & CodOsp & " - " & CampoOspite(CodOsp, "NOME", False) & vbNewLine
                    Else
                        If CampoParametriAddebitiAccreditiComulati = 1 Then
                            MaxAddebitoComune(X) = MaxAddebitoComune(X)
                        Else
                            MaxAddebitoComune(X) = MaxAddebitoComune(X) + 1
                        End If
                        MyTabAddebitoComune(X, MaxAddebitoComune(X)) = MyTabAddebitoComune(X, MaxAddebitoComune(X)) + campodbN(myRd.Item("Importo"))
                        MyTabDescrizioneAddebitoComune(X, MaxAddebitoComune(X)) = campodb(myRd.Item("Descrizione"))
                        MyTabCodiceIvaAddebitoComune(X, MaxAddebitoComune(X)) = campodb(myRd.Item("CodiceIva"))
                        MyTabAddebitoQty(X, MaxAddebitoComune(X)) = campodb(myRd.Item("Quantita"))
                    End If

                End If
                If campodb(myRd.Item("TipoMov")) = "AD" And campodb(myRd.Item("Riferimento")) = "J" Then
                    X = TrovaJolly(campodb(myRd.Item("Provincia")) & campodb(myRd.Item("Comune")))
                    If X > 10 Then
                        Call InserisciJolly(campodb(myRd.Item("Provincia")) & campodb(myRd.Item("Comune")))
                        X = TrovaJolly(campodb(myRd.Item("Provincia")) & campodb(myRd.Item("Comune")))
                    End If
                    If X > 10 Then
                        StringaDegliErrori = StringaDegliErrori & "Jolly non trovato in Addebito per Ospite " & CodOsp & " - " & CampoOspite(CodOsp, "NOME", False) & vbNewLine
                    Else
                        If CampoParametriAddebitiAccreditiComulati = 1 Then
                            MaxAddebitoJolly(X) = MaxAddebitoJolly(X)
                        Else
                            MaxAddebitoJolly(X) = MaxAddebitoJolly(X) + 1
                        End If
                        MyTabAddebitoJolly(X, MaxAddebitoJolly(X)) = MyTabAddebitoJolly(X, MaxAddebitoJolly(X)) + campodbN(myRd.Item("Importo"))
                        MyTabDescrizioneAddebitoJolly(X, MaxAddebitoJolly(X)) = campodb(myRd.Item("Descrizione"))
                        MyTabCodiceIvaAddebitoJolly(X, MaxAddebitoJolly(X)) = campodb(myRd.Item("CodiceIva"))
                    End If

                End If

                If campodb(myRd.Item("TipoMov")) = "AD" And campodb(myRd.Item("Riferimento")) = "R" Then
                    X = TrovaRegione(campodb(myRd.Item("Regione")), "")
                    If X > 10 Then
                        Call InserisciRegione(campodb(myRd.Item("Regione")), "")
                        X = TrovaRegione(campodb(myRd.Item("Regione")), "")
                    End If

                    If X > 10 Then
                        StringaDegliErrori = StringaDegliErrori & "Asl non trovato in Addebito per Ospite " & CodOsp & " - " & CampoOspite(CodOsp, "NOME", False) & vbNewLine
                    Else
                        If CampoParametriAddebitiAccreditiComulati = 1 Then
                            MaxAddebitoRegione(X) = MaxAddebitoRegione(X)
                        Else
                            MaxAddebitoRegione(X) = MaxAddebitoRegione(X) + 1
                        End If
                        MyTabAddebitoRegione(X, MaxAddebitoRegione(X)) = MyTabAddebitoRegione(X, MaxAddebitoRegione(X)) + campodbN(myRd.Item("Importo"))
                        MyTabDescrizioneAddebitoRegione(X, MaxAddebitoRegione(X)) = campodb(myRd.Item("Descrizione"))
                        MyTabCodiceIvaAddebitoRegione(X, MaxAddebitoRegione(X)) = campodb(myRd.Item("CodiceIva"))
                    End If
                End If
                If campodb(myRd.Item("TipoMov")) = "AC" And campodb(myRd.Item("Riferimento")) = "O" Then
                    MaxAccreditoOspite = MaxAccreditoOspite + 1
                    MyTabAccreditoOspite(MaxAccreditoOspite) = MyTabAccreditoOspite(MaxAccreditoOspite) + campodbN(myRd.Item("Importo"))
                    MyTabDescrizioneAccreditoOspite(MaxAccreditoOspite) = campodb(myRd.Item("Descrizione"))
                    MyTabCodiceIvaAccreditoOspite(MaxAccreditoOspite) = campodb(myRd.Item("CodiceIva"))
                End If
                If campodb(myRd.Item("TipoMov")) = "AC" And campodb(myRd.Item("Riferimento")) = "P" Then
                    MaxAccreditoParente(campodbN(myRd.Item("Parente"))) = MaxAccreditoParente(campodbN(myRd.Item("Parente"))) + 1
                    MyTabAccreditoParente(campodbN(myRd.Item("Parente")), MaxAccreditoParente(campodbN(myRd.Item("Parente")))) = MyTabAccreditoParente(campodbN(myRd.Item("Parente")), MaxAccreditoParente(campodbN(myRd.Item("Parente")))) + campodbN(myRd.Item("Importo"))
                    MyTabDescrizioneAccreditoParente(campodbN(myRd.Item("Parente")), MaxAccreditoParente(campodbN(myRd.Item("Parente")))) = campodb(myRd.Item("Descrizione"))
                    MyTabCodiceIvaAccreditoParente(campodbN(myRd.Item("Parente")), MaxAccreditoParente(campodbN(myRd.Item("Parente")))) = campodb(myRd.Item("CodiceIva"))
                    If MaxParenti < campodbN(myRd.Item("Parente")) Then
                        MaxParenti = campodbN(myRd.Item("Parente"))
                    End If
                End If
                If campodb(myRd.Item("TipoMov")) = "AC" And campodb(myRd.Item("Riferimento")) = "C" Then
                    X = TrovaComune(campodb(myRd.Item("Provincia")) & campodb(myRd.Item("Comune")))
                    If X > 10 Then
                        If CodiceComune(0) = "" And CodiceComune(1) = "" And CodiceComune(2) = "" Then
                            CodiceComune(1) = campodb(myRd.Item("Provincia")) & campodb(myRd.Item("Comune"))
                            X = 1
                        End If
                    End If
                    If X > 10 Then
                        InserisciComune(campodb(myRd.Item("Provincia")) & campodb(myRd.Item("Comune")))
                        X = TrovaComune(campodb(myRd.Item("Provincia")) & campodb(myRd.Item("Comune")))
                        MaxAccreditoComune(X) = MaxAccreditoComune(X) + 1
                        MyTabAccreditoComune(X, MaxAccreditoComune(X)) = MyTabAccreditoComune(X, MaxAccreditoComune(X)) + campodbN(myRd.Item("Importo"))
                        MyTabDescrizioneAccreditoComune(X, MaxAccreditoComune(X)) = campodb(myRd.Item("Descrizione"))
                        MyTabCodiceIvaAccreditoComune(X, MaxAccreditoComune(X)) = campodb(myRd.Item("CodiceIva"))

                        'StringaDegliErrori = StringaDegliErrori & "Comune non trovato in Accredito per Ospite " & CodOsp & " - " & CampoOspite(CodOsp, "NOME", False) & vbNewLine
                    Else
                        MaxAccreditoComune(X) = MaxAccreditoComune(X) + 1
                        MyTabAccreditoComune(X, MaxAccreditoComune(X)) = MyTabAccreditoComune(X, MaxAccreditoComune(X)) + campodbN(myRd.Item("Importo"))
                        MyTabDescrizioneAccreditoComune(X, MaxAccreditoComune(X)) = campodb(myRd.Item("Descrizione"))

                        MyTabCodiceIvaAccreditoComune(X, MaxAccreditoComune(X)) = campodb(myRd.Item("CodiceIva"))
                    End If
                End If
                If campodb(myRd.Item("TipoMov")) = "AC" And campodb(myRd.Item("Riferimento")) = "J" Then
                    X = TrovaJolly(campodb(myRd.Item("Provincia")) & campodb(myRd.Item("Comune")))
                    If X > 10 Then
                        If CodiceJolly(0) = "" And CodiceJolly(1) = "" And CodiceJolly(2) = "" Then
                            CodiceJolly(1) = campodb(myRd.Item("Provincia")) & campodb(myRd.Item("Comune"))
                            X = 1
                        End If
                    End If
                    If X > 10 Then
                        InserisciJolly(campodb(myRd.Item("Provincia")) & campodb(myRd.Item("Comune")))
                        X = TrovaJolly(campodb(myRd.Item("Provincia")) & campodb(myRd.Item("Comune")))
                        MaxAccreditoJolly(X) = MaxAccreditoJolly(X) + 1
                        MyTabAccreditoJolly(X, MaxAccreditoJolly(X)) = MyTabAccreditoJolly(X, MaxAccreditoJolly(X)) + campodbN(myRd.Item("Importo"))
                        MyTabDescrizioneAccreditoJolly(X, MaxAccreditoJolly(X)) = campodb(myRd.Item("Descrizione"))
                        MyTabCodiceIvaAccreditoJolly(X, MaxAccreditoJolly(X)) = campodb(myRd.Item("CodiceIva"))
                    Else
                        MaxAccreditoJolly(X) = MaxAccreditoJolly(X) + 1
                        MyTabAccreditoJolly(X, MaxAccreditoJolly(X)) = MyTabAccreditoJolly(X, MaxAccreditoJolly(X)) + campodbN(myRd.Item("Importo"))
                        MyTabDescrizioneAccreditoJolly(X, MaxAccreditoJolly(X)) = campodb(myRd.Item("Descrizione"))
                        MyTabCodiceIvaAccreditoJolly(X, MaxAccreditoJolly(X)) = campodb(myRd.Item("CodiceIva"))
                    End If
                End If

                If campodb(myRd.Item("TipoMov")) = "AC" And campodb(myRd.Item("Riferimento")) = "R" Then
                    X = TrovaRegione(campodb(myRd.Item("Regione")), "")
                    If X > 10 Then
                        If CodiceRegione(0) = "" And CodiceRegione(1) = "" And CodiceRegione(2) = "" Then
                            CodiceRegione(1) = campodb(myRd.Item("Regione"))
                            X = 1
                        End If
                    End If
                    If X > 10 Then
                        InserisciRegione(campodb(myRd.Item("Regione")), "")
                        X = TrovaRegione(campodb(myRd.Item("Regione")), "")
                        MaxAccreditoRegione(X) = MaxAccreditoRegione(X) + 1
                        MyTabAccreditoRegione(X, MaxAccreditoRegione(X)) = MyTabAccreditoRegione(X, MaxAccreditoRegione(X)) + campodbN(myRd.Item("Importo"))
                        MyTabDescrizioneAccreditoRegione(X, MaxAccreditoRegione(X)) = campodb(myRd.Item("Descrizione"))
                        MyTabCodiceIvaAccreditoRegione(X, MaxAccreditoRegione(X)) = campodb(myRd.Item("CodiceIva"))

                        'StringaDegliErrori = StringaDegliErrori & "Asl non trovato in Accredito per Ospite " & CodOsp & " - " & CampoOspite(CodOsp, "NOME", False) & vbNewLine
                    Else
                        MaxAccreditoRegione(X) = MaxAccreditoRegione(X) + 1
                        MyTabAccreditoRegione(X, MaxAccreditoRegione(X)) = MyTabAccreditoRegione(X, MaxAccreditoRegione(X)) + campodbN(myRd.Item("Importo"))
                        MyTabDescrizioneAccreditoRegione(X, MaxAccreditoRegione(X)) = campodb(myRd.Item("Descrizione"))
                        MyTabCodiceIvaAccreditoRegione(X, MaxAccreditoRegione(X)) = campodb(myRd.Item("CodiceIva"))
                    End If
                End If
            End If
        Loop
        myRd.Close()



        'Dim cmd1 As New OleDbCommand()
        'cmd1.CommandText = "SELECT * FROM DENARO WHERE CODICEOSPITE  = " & CodOsp & " And DATAREGISTRAZIONE >= ? And  DATAREGISTRAZIONE <= ? AND (Ripartizione = 'O' OR Ripartizione = 'P') AND TipoOperazione = 'P'"
        'cmd1.Parameters.AddWithValue("@Data1", DateSerial(Anno, Mese, 1))
        'cmd1.Parameters.AddWithValue("@Data2", DateSerial(Anno, Mese, GiorniMese(Mese, Anno)))
        'cmd1.Connection = OspitiAccessoriCon

        'Dim myRd1 As OleDbDataReader = cmd1.ExecuteReader()

        'Do While myRd1.Read
        '    If campodb(myRd1.Item("Ripartizione")) = "O" Then
        '        MaxAddebitoOspite = MaxAddebitoOspite + 1
        '        MyTabAddebitoOspite(MaxAddebitoOspite) = MyTabAddebitoOspite(MaxAddebitoOspite) + campodbN(myRd1.Item("Importo"))
        '        MyTabDescrizioneAddebitoOspite(MaxAddebitoOspite) = campodb(myRd1.Item("Descrizione"))
        '        MyTabCodiceIvaAddebitoOspite(MaxAddebitoOspite) = campodb(myRd1.Item("TipoAddebito"))
        '    End If
        '    If campodb(myRd1.Item("Ripartizione")) = "P" Then
        '        MaxAddebitoParente(campodbN(myRd1.Item("Parente"))) = MaxAddebitoParente(campodbN(myRd1.Item("Parente"))) + 1
        '        MyTabAddebitoParente(campodbN(myRd1.Item("Parente")), MaxAddebitoParente(campodbN(myRd1.Item("Parente")))) = MyTabAddebitoParente(campodb(myRd1.Item("Parente")), MaxAddebitoParente(campodbN(myRd1.Item("Parente")))) + campodbN(myRd1.Item("Importo"))
        '        MyTabDescrizioneAddebitoParente(campodbN(myRd1.Item("Parente")), MaxAddebitoParente(campodbN(myRd1.Item("Parente")))) = campodb(myRd1.Item("Descrizione"))
        '        MyTabCodiceIvaAddebitoParente(campodbN(myRd1.Item("Parente")), MaxAddebitoOspite) = campodb(myRd1.Item("TipoAddebito"))
        '    End If

        'Loop
        'myRd1.Close()



    End Sub

    Public Function VerificaRette(ByVal Cserv As String, ByVal CodOsp As Long, ByVal Mese As Integer, ByVal Anno As Integer) As Boolean
        Dim I As Byte
        VerificaRette = True
        For I = 1 To 10
            If ImportoPresParente(I) < 0 Or NonImportoPresParente(I) < 0 Then
                StringaDegliErrori = StringaDegliErrori + "Importo Parente Negativo Ospite :" & CodOsp & " - " & CampoOspite(CodOsp, "NOME", False) & Chr(13)
                VerificaRette = False
            End If
            If ImportoAssParente(I) < 0 Or NonImportoAssParente(I) < 0 Then
                StringaDegliErrori = StringaDegliErrori + "Importo Parente Negativo Ospite :" & CodOsp & " - " & CampoOspite(CodOsp, "NOME", False) & Chr(13)
                VerificaRette = False
            End If
            If Modulo.MathRound(ImportoPresComune(I), 2) < 0 Or Modulo.MathRound(NonImportoPresComune(I), 2) < 0 Then
                'StringaDegliErrori = StringaDegliErrori + "Importo Comune Negativo Ospite :" & CodOsp & " - " & CampoOspite(CodOsp, "NOME", False) & Chr(13)
                'VerificaRette = False
            End If
            If Modulo.MathRound(ImportoAssComune(I), 2) < 0 Or Modulo.MathRound(NonImportoAssComune(I), 2) < 0 Then
                'StringaDegliErrori = StringaDegliErrori + "Importo Comune Negativo Ospite :" & CodOsp & " - " & CampoOspite(CodOsp, "NOME", False) & Chr(13)
                'VerificaRette = False
            End If

            If Modulo.MathRound(ImportoPresRegione(I), 2) < 0 Or Modulo.MathRound(NonImportoPresRegione(I), 2) < 0 Then
                StringaDegliErrori = StringaDegliErrori + "Importo Regione Negativo Ospite :" & CodOsp & " - " & CampoOspite(CodOsp, "NOME", False) & Chr(13)
                VerificaRette = False
            End If

            If Modulo.MathRound(ImportoAssRegione(I), 2) < 0 Or Modulo.MathRound(NonImportoAssRegione(I), 2) < 0 Then
                StringaDegliErrori = StringaDegliErrori + "Importo Regione Negativo Ospite :" & CodOsp & " - " & CampoOspite(CodOsp, "NOME", False) & Chr(13)
                VerificaRette = False
            End If

        Next I


        If Modulo.MathRound(ImportoPresOspite, 2) < 0 Or Modulo.MathRound(NonImportoPresOspite, 2) < 0 Then
            StringaDegliErrori = StringaDegliErrori + "Importo Ospite Negativo Ospite :" & CodOsp & " - " & CampoOspite(CodOsp, "NOME", False) & Chr(13)
            VerificaRette = False
        End If

        If Modulo.MathRound(ImportoAssOspite, 2) < 0 Or Modulo.MathRound(NonImportoAssOspite, 2) < 0 Then
            StringaDegliErrori = StringaDegliErrori + "Importo Ospite Negativo Ospite :" & CodOsp & " - " & CampoOspite(CodOsp, "NOME", False) & Chr(13)
            VerificaRette = False
        End If

        Dim MyRs As New ADODB.Recordset
        Dim xk As Integer
        Dim Provincia As String
        Dim Comune As String
        Dim Vincolo As String
        Dim DataNascita As Object
        Dim Sesso As String
        Dim VerificoComuneOspite As Boolean
        Dim xModalita As String
        Dim xImportoComune As Double
        Dim XImportoRegione As Double
        Dim VerificoRegioneOspite As Boolean
        Dim xRegione As String
        Dim VincoloRegione As String = ""

        Dim UltimaRegione As String = ""

        Dim UltimoComune As String = ""


        VerificoComuneOspite = True
        VerificoRegioneOspite = True


        Provincia = ""
        Comune = ""
        xModalita = ""
        xImportoComune = 0
        Vincolo = ""
        For xk = 1 To 31

            If MyTabCodiceProv(xk) <> "" And MyTabCodiceComune(xk) <> "" Then
                If UltimoComune <> MyTabCodiceProv(xk) & MyTabCodiceComune(xk) Then
                    Provincia = MyTabCodiceProv(xk)
                    Comune = MyTabCodiceComune(xk)
                    UltimoComune = MyTabCodiceProv(xk) & MyTabCodiceComune(xk)
                    Vincolo = ""

                    Dim cmd As New OleDbCommand()
                    cmd.CommandText = "Select Vincolo From AnagraficaComune Where CodiceProvincia = '" & Provincia & "' And CodiceComune = '" & Comune & "' And Tipologia  ='C'"
                    cmd.Connection = OspitiCon

                    Dim myRd As OleDbDataReader = cmd.ExecuteReader()

                    If myRd.Read Then
                        If campodb(myRd.Item("Vincolo")) <> "" Then
                            Vincolo = campodb(myRd.Item("Vincolo")) & Space(10)
                        End If
                    End If
                    myRd.Close()   
                End If
            End If

            If MyTabCodiceRegione(xk) <> "" Then
                If UltimaRegione <> MyTabCodiceRegione(xk) Then
                    xRegione = MyTabCodiceRegione(xk)
                    UltimaRegione = MyTabCodiceRegione(xk)
                    VincoloRegione = ""

                    Dim cmd As New OleDbCommand()
                    cmd.CommandText = "Select Vincolo From AnagraficaComune Where CodiceRegione = '" & xRegione & "' And Tipologia  ='R'"
                    cmd.Connection = OspitiCon

                    Dim myRd As OleDbDataReader = cmd.ExecuteReader()

                    If myRd.Read Then
                        If campodb(myRd.Item("Vincolo")) <> "" Then
                            VincoloRegione = campodb(myRd.Item("Vincolo")) & Space(10)
                        End If
                    End If
                    myRd.Close()
                End If
            End If


            If MyTabModalita(xk) <> "" Then
                xModalita = MyTabModalita(xk)
            End If
            If MyTabImportoComune(xk) > -1 Then
                xImportoComune = MyTabImportoComune(xk)
            End If

            If MyTabImportoRegione(xk) > -1 Then
                XImportoRegione = MyTabImportoRegione(xk)
            End If

            If Vincolo <> "" And (xModalita = "C" Or xImportoComune > 0) Then
                If Val(Mid(Vincolo, 1, 3)) > 0 Or Val(Mid(Vincolo, 4, 3)) > 0 Then
                    DataNascita = CampoOspite(CodOsp, "DataNascita")
                    If IsDate(DataNascita) Then
                        If DateDiff("d", DataNascita, DateSerial(Anno, Mese, xk)) < DateDiff("d", DataNascita, DateAdd("yyyy", Val(Mid(Vincolo, 1, 3)), DataNascita)) Then
                            VerificoComuneOspite = False
                        End If
                        If DateDiff("d", DataNascita, DateSerial(Anno, Mese, xk)) > DateDiff("d", DataNascita, DateAdd("yyyy", Val(Mid(Vincolo, 4, 3)), DataNascita)) Then
                            VerificoComuneOspite = False
                        End If
                    End If
                End If

                If Trim(Mid(Vincolo, 7, 1)) <> "" Then
                    Sesso = CampoOspite(CodOsp, "Sesso")
                    If Mid(Vincolo, 7, 1) <> Sesso Then
                        VerificoComuneOspite = False
                    End If
                End If

                If Trim(Mid(Vincolo, 8, 1)) <> "" Then
                    If Mid(Vincolo, 8, 1) <> StatoAutoOspite(CodOsp, DateSerial(Anno, Mese, xk)) Then
                        VerificoComuneOspite = False
                    End If
                End If
            End If
            If VincoloRegione <> "" And (xModalita = "R" Or XImportoRegione > 0) Then
                If Val(Mid(VincoloRegione, 1, 3)) > 0 Or Val(Mid(VincoloRegione, 4, 3)) > 0 Then
                    DataNascita = CampoOspite(CodOsp, "DataNascita")
                    If IsDate(DataNascita) Then
                        If DateDiff("d", DataNascita, DateSerial(Anno, Mese, xk)) < DateDiff("d", DataNascita, DateAdd("yyyy", Val(Mid(VincoloRegione, 1, 3)), DataNascita)) Then
                            VerificoRegioneOspite = False
                        End If
                        If DateDiff("d", DataNascita, DateSerial(Anno, Mese, xk)) > DateDiff("d", DataNascita, DateAdd("yyyy", Val(Mid(VincoloRegione, 4, 3)), DataNascita)) Then
                            VerificoRegioneOspite = False
                        End If
                    End If
                End If

                If Trim(Mid(VincoloRegione, 7, 1)) <> "" Then
                    Sesso = CampoOspite(CodOsp, "Sesso")
                    If Mid(VincoloRegione, 7, 1) <> Sesso Then
                        VerificoRegioneOspite = False
                    End If
                End If

                If Trim(Mid(VincoloRegione, 8, 1)) <> "" Then
                    If Mid(VincoloRegione, 8, 1) <> StatoAutoOspite(CodOsp, DateSerial(Anno, Mese, xk)) Then
                        VerificoRegioneOspite = False
                    End If
                End If
            End If

        Next

        If VerificoComuneOspite = False Then
            StringaDegliErrori = StringaDegliErrori & "Validita su Comune non valida per ospite " & CodOsp & vbNewLine
        End If
        If VerificoRegioneOspite = False Then
            StringaDegliErrori = StringaDegliErrori & "Validita su Regione non valida per ospite " & CodOsp & vbNewLine
        End If

        'If ImportoPresEnte < 0 Or NonImportoPresEnte < 0 Then
        '   StringaDegliErrori = StringaDegliErrori + "Importo Ente Negativo Ospite :" & CodOsp & " - " & CampoOspite(CodOsp, "NOME") & Chr(13)
        '   VerificaRette = False
        'End If

        'If ImportoAssEnte < 0 Or NonImportoAssEnte < 0 Then
        '   StringaDegliErrori = StringaDegliErrori + "Importo Ente Negativo Ospite :" & CodOsp & " - " & CampoOspite(CodOsp, "NOME") & Chr(13)
        '   VerificaRette = False
        'End If
    End Function

    Public Function Annuale(ByVal Importo As Double, ByVal Anno As Integer) As Double
        If Day(DateSerial(Anno, 2, 29)) = 29 Then
            Annuale = Modulo.MathRound(Importo / 366, 2)
        Else
            Annuale = Modulo.MathRound(Importo / 365, 2)
        End If
    End Function

    Public Function Mensile(ByVal Importo As Double, ByVal Mese As Byte, ByVal Anno As Integer, ByVal CentroServizio As String) As Double
        Dim Parametri As New Cls_Parametri

        Parametri.LeggiParametri(STRINGACONNESSIONEDB)
        If Parametri.ImportoMensile = 0 Then
            If Mese = 1 Or Mese = 3 Or Mese = 5 Or Mese = 7 Or Mese = 8 Or _
               Mese = 10 Or Mese = 12 Then
                Mensile = Modulo.MathRound(Importo / 31, 10) '2)
            Else
                If Mese <> 2 Then
                    Mensile = Modulo.MathRound(Importo / 30, 10) ' 2)
                Else
                    If Day(DateSerial(Anno, Mese, 29)) = 29 Then
                        Mensile = Modulo.MathRound(Importo / 29, 10) '2)
                    Else
                        Mensile = Modulo.MathRound(Importo / 28, 10) '2)
                    End If
                End If
            End If
        End If
        If Parametri.ImportoMensile = 30 Then
            Mensile = Modulo.MathRound(Importo / 30, 10)  '2)
        End If
        If Parametri.ImportoMensile = 12 Then
            Mensile = Modulo.MathRound(Importo * 12 / 365, 2)  '2)
        End If
        If Parametri.ImportoMensile = 34 Then
            Mensile = Modulo.MathRound(Importo / 30.4, 10)  '2)
        End If

        If Parametri.ImportoMensile = 365 Then
            If Day(DateSerial(Anno, 2, 29)) = 29 Then
                Mensile = Modulo.MathRound(Importo / (366 / 12), 2)   '2)
            Else
                Mensile = Modulo.MathRound(Importo / (365 / 12), 2)  '2)
            End If
        End If

        If CentroServizio <> "" Then
            Dim Cserv As New Cls_CentroServizio

            Cserv.Leggi(STRINGACONNESSIONEDB, CentroServizio)
            If Cserv.ImportoMensile = 0 Then
                If Mese = 1 Or Mese = 3 Or Mese = 5 Or Mese = 7 Or Mese = 8 Or _
                   Mese = 10 Or Mese = 12 Then
                    Mensile = Modulo.MathRound(Importo / 31, 10) '2)
                Else
                    If Mese <> 2 Then
                        Mensile = Modulo.MathRound(Importo / 30, 10) ' 2)
                    Else
                        If Day(DateSerial(Anno, Mese, 29)) = 29 Then
                            Mensile = Modulo.MathRound(Importo / 29, 10) '2)
                        Else
                            Mensile = Modulo.MathRound(Importo / 28, 10) '2)
                        End If
                    End If
                End If
            End If
            If Cserv.ImportoMensile = 30 Then
                Mensile = Modulo.MathRound(Importo / 30, 10)  '2)
            End If
            If Cserv.ImportoMensile = 34 Then
                Mensile = Modulo.MathRound(Importo / 30.4, 10)  '2)
            End If
            If Cserv.ImportoMensile = 12 Then
                Mensile = Modulo.MathRound(Importo * 12 / 365, 2)  '2)
            End If
            If Cserv.ImportoMensile = 365 Then
                If Day(DateSerial(Anno, 2, 29)) = 29 Then
                    Mensile = Modulo.MathRound(Importo / (366 / 12), 2)   '2)
                Else
                    Mensile = Modulo.MathRound(Importo / (365 / 12), 2)  '2)
                End If
            End If

            If Cserv.ImportoMensile = 7 Then
                Dim Giorni As Integer
                Dim I As Integer

                Giorni = 0
                For I = 0 To GiorniMese(Mese, Anno)
                    If Weekday(DateSerial(Anno, Mese, I)) <> vbSaturday And Weekday(DateSerial(Anno, Mese, I)) <> vbSunday Then
                        Giorni = Giorni + 1
                    End If
                Next
                Mensile = Modulo.MathRound(Importo / Giorni, 10)  ' DecEuro)
            End If

            If Cserv.ImportoMensile = -1 Then
                Exit Function
            End If
        End If

        
    End Function

    Public Sub AzzeraTabella()
        Dim I As Integer, Par As Integer
        Dim Ext As Integer

        For Ext = 0 To 310
            XCodicRegione(Ext) = 0
            For I = 0 To 10
                XImportoRegione(Ext, I) = 0
            Next
        Next



        For I = 1 To 31
            MyTabCausale(I) = ""
            MyTabModalita(I) = ""
            MyTabGiornoAssenza(I) = 0
            MyTabCodiceComune(I) = ""
            MyTabGiorniRipetizioneCausale(I) = 0
            MyTabGiorniRipetizioneCausaleNC(I) = 0
            MyTabGiorniRipetizioneCausaleACSum(I) = 0


            MyTabCodiceProvJolly(I) = ""
            MyTabCodiceJolly(I) = ""

            MyTabCodiceProv(I) = ""
            MyTabAutoSufficente(I) = ""
            MyTabCodiceRegione(I) = "****"
            MyTabImportoComune(I) = -1
            MyTabModalita(I) = ""
            MyTabImportoJolly(I) = -1

            MyTabImportoOspite(I) = -1
            MyTabImportoOspite2(I) = -1

            MyTabImportoRegione(I) = -1
            MyTabImportoRetta(I) = -1
            For Par = 0 To 10
                MyTabImportoParente(I, Par) = -1
                MyTabImportoParente2(I, Par) = -1
                MyTabPercentuale(I, Par) = -1
                MyTabImpExtrOspite(I, Par) = -1
                MyTabImpExtrParent(I, Par) = -1

                MyTabImpExtrComune(I, Par) = -1

                MyTabImpExtrJolly(I, Par) = -1


            Next Par



            MyTabGiornalieroExtraImporto1(I) = 0
            MyTabGiornalieroExtraImporto2(I) = 0
            MyTabGiornalieroExtraImporto3(I) = 0
            MyTabGiornalieroExtraImporto4(I) = 0


            MyTabGiornalieroExtraImporto1J(I) = 0
            MyTabGiornalieroExtraImporto2J(I) = 0
            MyTabGiornalieroExtraImporto3J(I) = 0
            MyTabGiornalieroExtraImporto4J(I) = 0


            MyTabGiornalieroExtraImporto1C(I) = 0
            MyTabGiornalieroExtraImporto2C(I) = 0
            MyTabGiornalieroExtraImporto3C(I) = 0
            MyTabGiornalieroExtraImporto4C(I) = 0


        Next I


        BloccaConguaglioDaRegola = 0
        ImpExtrOspMan1 = 0
        ImpExtrOspMan2 = 0
        ImpExtrOspMan3 = 0
        ImpExtrOspMan4 = 0

        NumExtrOspMan1 = 0
        NumExtrOspMan2 = 0
        NumExtrOspMan3 = 0
        NumExtrOspMan4 = 0

        TipoAddExtrOspMan1 = ""
        TipoAddExtrOspMan2 = ""
        TipoAddExtrOspMan3 = ""
        TipoAddExtrOspMan4 = ""

        ' Azzera Importi Mensile
        ImportoMensileOspite = 0
        ImportoMensileComune = 0

        ImportoMensileJolly = 0

        ImportoMensileRetta = 0

        ImportoPresEnte = 0 : ImportoAssEnte = 0
        NonImportoPresEnte = 0 : NonImportoAssEnte = 0
        ImportoPresOspite = 0 : ImportoAssOspite = 0
        NonImportoPresOspite = 0 : NonImportoAssOspite = 0

        ImportoPresOspite2 = 0 : ImportoAssOspite2 = 0
        NonImportoPresOspite2 = 0 : NonImportoAssOspite2 = 0

        ImportoMensileOspite = 0


        ImportoAddebitoPrimoComune = 0

        ImportoAddebitoPrimoJolly = 0

        ImportoAddebitoPrimoRegione = 0
        TipoAddebitoPrimoRegione = ""
        TipoAddebitoPrimoComune = ""


        TipoAddebitoPrimoJolly = ""


        Accolto = False
        Dimesso = False
        Prm_AzzeraSeNegativo = 1
        'CampoParametriGIORNOUSCITA = ""
        'CampoParametriGIORNOENTRATA = ""
        'CampoParametriCAUSALEACCOGLIMENTO = ""
        'CampoParametriMastroAnticipo = 0

        'CampoParametriAddebitiAccreditiComulati = 0


        For I = 1 To 410
            XCodicRegione(I) = ""
            For Ext = 1 To 31
                XImportoRegione(I, Ext) = 0
            Next
        Next

        GiornoCambioRettaOspite = 0




        UltimoCodiceRegione = 0

        ImportoMensileOspite = 0

        ImportoMensileComune = 0

        ImportoMensileJolly = 0

        ImportoMensileRetta = 0


        For I = 0 To 10
            ImportoMensileParente(I) = 0
            ImportoMensileParente2(I) = 0
            ImportoExtrOspite(I) = 0
            ImportoExtrParent(I) = 0
            ImportoExtrComune(I) = 0
            ImportoExtrJolly(I) = 0
            ImpExtrComMan1(I) = 0
            ImpExtrComMan2(I) = 0
            ImpExtrComMan3(I) = 0
            ImpExtrComMan4(I) = 0
            NumExtrComMan1(I) = 0
            NumExtrComMan2(I) = 0
            NumExtrComMan3(I) = 0
            NumExtrComMan4(I) = 0

            TipoExtrComMan1(I) = ""
            TipoExtrComMan2(I) = ""
            TipoExtrComMan3(I) = ""
            TipoExtrComMan4(I) = ""
            MensileImportoExtrOspite(I) = 0
            MensileImportoExtrParent(I) = 0
            MensileImportoExtrComune(I) = 0

            ImpExtrParMan1(I) = 0
            ImpExtrParMan2(I) = 0
            ImpExtrParMan3(I) = 0
            ImpExtrParMan4(I) = 0
            NumExtrParMan1(I) = 0
            NumExtrParMan2(I) = 0
            NumExtrParMan3(I) = 0
            NumExtrParMan4(I) = 0
            TipoExtrParMan1(I) = ""
            TipoExtrParMan2(I) = ""
            TipoExtrParMan3(I) = ""
            TipoExtrParMan4(I) = ""


            MaxAddebitoParente(I) = 0
            MaxAccreditoParente(I) = 0
            MaxAddebitoComune(I) = 0
            MaxAccreditoComune(I) = 0
            MaxAddebitoJolly(I) = 0
            MaxAccreditoJolly(I) = 0
            MaxAddebitoRegione(I) = 0
            MaxAccreditoRegione(I) = 0
            SalvaImpExtrOspite(I) = 0
            SalvaImpExtrParent(I) = 0
            SalvaImpExtrComune(I) = 0
            SalvaImpExtrJolly(I) = 0

            CodiceComune(I) = 0
            CodiceRegione(I) = 0

            CodiceJolly(I) = 0
            ImportoPresParente(I) = 0
            ImportoAssParente(I) = 0
            ImportoPresParente2(I) = 0
            ImportoAssParente2(I) = 0


            ImportoPresComune(I) = 0
            ImportoAssComune(I) = 0

            ImportoPresJolly(I) = 0
            ImportoAssJolly(I) = 0

            ImportoPresRegione(I) = 0
            ImportoAssRegione(I) = 0

            ImpExtraOspite(I) = 0
            NonImpExtraOspite(I) = 0

            NonImportoPresParente(I) = 0
            NonImportoAssParente(I) = 0

            NonImportoPresParente2(I) = 0
            NonImportoAssParente2(I) = 0

            NonImportoPresComune(I) = 0
            NonImportoAssComune(I) = 0

            NonImportoPresJolly(I) = 0
            NonImportoAssJolly(I) = 0


            NonImportoPresRegione(I) = 0
            NonImportoAssRegione(I) = 0

            NonGiorniPresParente(I) = 0
            NonGiorniAssParente(I) = 0

            NonGiorniPresComune(I) = 0
            NonGiorniAssComune(I) = 0

            NonGiorniPresJolly(I) = 0
            NonGiorniAssJolly(I) = 0
            GiorniPresParente(I) = 0
            GiorniAssParente(I) = 0

            GiorniPresComune(I) = 0
            GiorniAssComune(I) = 0

            GiorniPresJolly(I) = 0
            GiorniAssJolly(I) = 0

            GiorniPresRegione(I) = 0
            GiorniAssRegione(I) = 0

            NonGiorniPresRegione(I) = 0
            NonGiorniAssRegione(I) = 0
            GiorniPresExtraC(I) = 0


            For Ext = 1 To 10
                ImpExtraParente(I, Ext) = 0
                NonImpExtraParente(I, Ext) = 0
                ImpExtraComune(I, Ext) = 0
                NonImpExtraComune(I, Ext) = 0

                ImpExtraJolly(I, Ext) = 0
                NonImpExtraJolly(I, Ext) = 0
            Next
        Next

        ImpExtrOspMan1 = 0
        ImpExtrOspMan2 = 0
        ImpExtrOspMan3 = 0
        ImpExtrOspMan4 = 0

        NumExtrOspMan1 = 0
        NumExtrOspMan2 = 0
        NumExtrOspMan3 = 0
        NumExtrOspMan4 = 0

        TipoAddExtrOspMan1 = ""
        TipoAddExtrOspMan2 = ""
        TipoAddExtrOspMan3 = ""
        TipoAddExtrOspMan4 = ""

        MaxAddebitoOspite = 0
        MaxAccreditoOspite = 0

        MaxParenti = 0

        MaxRegione = 0

        ImportoPresOspite = 0
        ImportoAssOspite = 0
        ImportoPresOspite2 = 0
        ImportoAssOspite2 = 0

        ImportoPresEnte = 0
        ImportoAssEnte = 0
        GiornoRientro = 0
        NonImportoPresOspite = 0
        NonImportoAssOspite = 0
        NonImportoPresOspite2 = 0
        NonImportoAssOspite2 = 0
        NonImportoPresEnte = 0
        NonImportoAssEnte = 0

        GiorniPres = 0
        GiorniAss = 0
        GiorniPresEnte = 0
        GiorniAssEnte = 0
        NonGiorniPres = 0
        NonGiorniAss = 0
        NonGiorniPresEnte = 0
        NonGiorniAssEnte = 0


        SalvaImportoRegione = 0
        SalvaImportoComune = 0
        SalvaImportoOspite = 0
        SalvaImportoTotale = 0
        SalvaPercentuale = 0
        SalvaDifferenza = 0
        SalvaImportoJolly = 0

        XSO = 0
        XSP = 0
        XSC = 0

        mGiorniPres = 0
        mGiorniAss = 0

        For I = 0 To 10
            ImportoPresComune(I) = 0 : ImportoAssComune(I) = 0

            ImportoPresJolly(I) = 0 : ImportoAssJolly(I) = 0

            ImportoPresRegione(I) = 0 : ImportoAssRegione(I) = 0

            NonImportoPresComune(I) = 0 : NonImportoAssComune(I) = 0

            NonImportoPresJolly(I) = 0 : NonImportoAssJolly(I) = 0

            NonImportoPresRegione(I) = 0 : NonImportoAssRegione(I) = 0
            GiorniPresParente(I) = 0 : GiorniAssParente(I) = 0

            GiorniPresComune(I) = 0 : GiorniAssComune(I) = 0

            GiorniPresJolly(I) = 0 : GiorniAssJolly(I) = 0

            GiorniPresRegione(I) = 0 : GiorniAssRegione(I) = 0
            NonGiorniPresParente(I) = 0 : NonGiorniAssParente(I) = 0

            NonGiorniPresComune(I) = 0 : NonGiorniAssComune(I) = 0

            NonGiorniPresJolly(I) = 0 : NonGiorniAssJolly(I) = 0

            NonGiorniPresRegione(I) = 0 : NonGiorniAssRegione(I) = 0

            NonImpExtraOspite(I) = 0 : ImpExtraOspite(I) = 0
            ImpExtrComMan1(I) = 0
            ImpExtrComMan2(I) = 0
            ImpExtrComMan3(I) = 0
            ImpExtrComMan4(I) = 0

            NumExtrComMan1(I) = 0
            NumExtrComMan2(I) = 0
            NumExtrComMan3(I) = 0
            NumExtrComMan4(I) = 0

            TipoExtrComMan1(I) = ""
            TipoExtrComMan2(I) = ""
            TipoExtrComMan3(I) = ""
            TipoExtrComMan4(I) = ""

            ImpExtrParMan1(I) = 0
            ImpExtrParMan2(I) = 0
            ImpExtrParMan3(I) = 0
            ImpExtrParMan4(I) = 0

            NumExtrParMan1(I) = 0
            NumExtrParMan2(I) = 0
            NumExtrParMan3(I) = 0
            NumExtrParMan4(I) = 0

            TipoExtrParMan1(I) = ""
            TipoExtrParMan2(I) = ""
            TipoExtrParMan3(I) = ""
            TipoExtrParMan4(I) = ""

            For Ext = 0 To 10
                NonImpExtraParente(I, Ext) = 0 : ImpExtraParente(I, Ext) = 0

                NonImpExtraComune(I, Ext) = 0 : ImpExtraComune(I, Ext) = 0

                NonImpExtraJolly(I, Ext) = 0 : ImpExtraJolly(I, Ext) = 0
                MyTabGiornalieroExtraImporto1P(I, Ext) = 0
                MyTabGiornalieroExtraImporto2P(I, Ext) = 0
                MyTabGiornalieroExtraImporto3P(I, Ext) = 0
                MyTabGiornalieroExtraImporto4P(I, Ext) = 0

                MyTabCodiceExtrParentGIORNI(I, Ext) = 0
                MyTabCodiceExtrParentNonGIORNI(I, Ext) = 0

            Next Ext

            ImportoMensileParente(I) = 0
            ImportoMensileParente2(I) = 0
            ImportoPresParente(I) = 0 : ImportoAssParente(I) = 0
            NonImportoPresParente(I) = 0 : NonImportoAssParente(I) = 0

            ImportoPresParente2(I) = 0 : ImportoAssParente2(I) = 0
            NonImportoPresParente2(I) = 0 : NonImportoAssParente2(I) = 0
            SalvaImpExtrOspite(I) = 0
            SalvaImpExtrParent(I) = 0
            SalvaImpExtrComune(I) = 0

            SalvaImpExtrJolly(I) = 0

            MyTabCodiceExtrComuneGIORNI(I) = 0
            MyTabCodiceExtrOspiteGIORNI(I) = 0
            MyTabCodiceNonExtrOspiteGIORNI(I) = 0


            
            MyTabCodiceExtrOspite(I) = 0
            MyTabCodiceExtrParent(I) = 0

            MyTabCodiceExtrComune(I) = 0

            MyTabCodiceExtrJolly(I) = 0



            MyTabParenteMensile(I) = 0

        Next I


        MyTabOspitiMensile = 0

        GiorniPres = 0 : GiorniAss = 0

        GiorniPresEnte = 0 : GiorniAssEnte = 0
        NonGiorniPres = 0 : NonGiorniAss = 0
        NonGiorniPresEnte = 0 : NonGiorniAssEnte = 0

        MaxAddebitoOspite = 0
        MaxAccreditoOspite = 0

        For I = 0 To 40
            MyTabAddebitoOspite(I) = 0
            MyTabAccreditoOspite(I) = 0
            MyTabAddebitoMeseCompetenza(I) = 0
            MyTabAddebitoAnnoCompetenza(I) = 0
            MyTabAccreditoMeseCompetenza(I) = 0
            MyTabAccreditoAnnoCompetenza(I) = 0
            GiornoCambioRettaParenti(I) = 0
            MyTabAddebitoOspiteQty(I) = 0
        Next
        For I = 0 To 10



            MaxAccreditoParente(I) = 0
            MaxAddebitoParente(I) = 0
            MaxAddebitoComune(I) = 0

            MaxAddebitoJolly(I) = 0

            MaxAccreditoComune(I) = 0

            MaxAccreditoJolly(I) = 0

            MaxAddebitoRegione(I) = 0
            MaxAccreditoRegione(I) = 0

            ImportoMensileParente(I) = 0
            ImportoMensileParente2(I) = 0

            For Ext = 0 To 40

                MyTabAddebitoParenteQty(I, Ext) = 0
                MyTabAddebitoQty(I, Ext) = 0
                MyTabAddebitoParenteMeseCompetenza(I, Ext) = 0
                MyTabAddebitoParenteAnnoCompetenza(I, Ext) = 0
                MyTabAccreditoParenteMeseCompetenza(I, Ext) = 0
                MyTabAccreditoParenteAnnoCompetenza(I, Ext) = 0

                MyTabAddebitoParente(I, Ext) = 0
                MyTabAccreditoParente(I, Ext) = 0

                MyTabAddebitoComune(I, Ext) = 0
                MyTabAccreditoComune(I, Ext) = 0

                MyTabAddebitoJolly(I, Ext) = 0
                MyTabAccreditoJolly(I, Ext) = 0

                MyTabAddebitoRegione(I, Ext) = 0
                MyTabAccreditoRegione(I, Ext) = 0


                MyTabDescrizioneAddebitoOspite(Ext) = ""
                MyTabDescrizioneAccreditoOspite(Ext) = ""

                MyTabDescrizioneAddebitoParente(I, Ext) = ""
                MyTabDescrizioneAccreditoParente(I, Ext) = ""


                MyTabCodiceIvaAddebitoOspite(Ext) = ""
                MyTabCodiceIvaAccreditoOspite(Ext) = ""

                MyTabCodiceIvaAddebitoParente(I, Ext) = ""
                MyTabCodiceIvaAccreditoParente(I, Ext) = ""


                If Ext < 32 Then MyTabDescrizioneAddebitoComune(I, Ext) = ""
                If Ext < 32 Then MyTabDescrizioneAccreditoComune(I, Ext) = ""

                If Ext < 32 Then MyTabCodiceIvaAddebitoComune(I, Ext) = ""
                If Ext < 32 Then MyTabCodiceIvaAccreditoComune(I, Ext) = ""


                MyTabDescrizioneAddebitoJolly(I, Ext) = ""
                MyTabDescrizioneAccreditoJolly(I, Ext) = ""

                MyTabDescrizioneAddebitoRegione(I, Ext) = ""
                MyTabDescrizioneAccreditoRegione(I, Ext) = ""

            Next
            CodiceRegione(I) = ""
            CodiceComune(I) = ""

            CodiceJolly(I) = ""
        Next I
    End Sub
    Public Sub CreaTabellaImportoParente(ByVal Cserv As String, ByVal CodOsp As Long, ByVal Mese As Integer, ByVal Anno As Integer)

        Dim MyDataini As Date, MyDatafine As Date
        Dim MySql As String
        Dim CodiceParenti(100) As Boolean
        Dim I As Integer
        Dim ParCod As Integer
        Dim ImportoLetto(100) As Integer


        For I = 1 To 10 : ImportoLetto(I) = 0 : Next

        MaxParenti = 0

        MyDataini = DateSerial(Anno, Mese, 1)
        MyDatafine = DateSerial(Anno, Mese, GiorniMese(Mese, Anno))


        MySql = "Select * From ImportoParenti Where CENTROSERVIZIO = '" & Cserv & "' and CODICEOSPITE = " & CodOsp & " And "
        MySql = MySql & " Data <= {ts '" & Format(MyDatafine, "yyyy-MM-dd") & " 00:00:00'}  Order by Data Desc"


        Dim cmd As New OleDbCommand()
        cmd.CommandText = MySql
        cmd.Connection = OspitiCon

        Dim Rs_ImportoParente As OleDbDataReader = cmd.ExecuteReader()

        Do While Rs_ImportoParente.Read()
            If Format(campodbd(Rs_ImportoParente.Item("Data")), "yyyy-MM-dd") > Format(MyDataini, "yyyy-MM-dd") Then
                ParCod = campodbN(Rs_ImportoParente.Item("CODICEPARENTE"))

                If Format(campodbd(Rs_ImportoParente.Item("Data")), "yyyyMMdd") <> Format(PrimoDataAccoglimentoOspite, "yyyyMMdd") Then
                    GiornoCambioRettaParenti(ParCod) = Day(campodbd(Rs_ImportoParente.Item("Data")))
                End If

                If campodb(Rs_ImportoParente.Item("TipoImporto")) = "G" Or campodb(Rs_ImportoParente.Item("TipoImporto")) = "" Then
                    If campodb(Rs_ImportoParente.Item("Data")) = "" Then
                        StringaDegliErrori = StringaDegliErrori & " Errore in importo parenti per Ospite " & CodOsp & " - " & CampoOspite(CodOsp, "NOME", False)
                        Exit Sub
                    Else
                        MyTabImportoParente(Day(campodbd(Rs_ImportoParente.Item("Data"))), ParCod) = campodbN(Rs_ImportoParente.Item("Importo"))
                        MyTabImportoParente2(Day(campodbd(Rs_ImportoParente.Item("Data"))), ParCod) = campodbN(Rs_ImportoParente.Item("Importo_2"))
                        MyTabGiornalieroExtraImporto1P(Day(campodbd(Rs_ImportoParente.Item("Data"))), ParCod) = campodbN(Rs_ImportoParente.Item("Importo1"))
                        MyTabGiornalieroExtraImporto2P(Day(campodbd(Rs_ImportoParente.Item("Data"))), ParCod) = campodbN(Rs_ImportoParente.Item("Importo2"))
                        MyTabGiornalieroExtraImporto3P(Day(campodbd(Rs_ImportoParente.Item("Data"))), ParCod) = campodbN(Rs_ImportoParente.Item("Importo3"))
                        MyTabGiornalieroExtraImporto4P(Day(campodbd(Rs_ImportoParente.Item("Data"))), ParCod) = campodbN(Rs_ImportoParente.Item("Importo4"))
                    End If
                Else
                    If campodb(Rs_ImportoParente.Item("TipoImporto")) = "M" Then
                        MyTabParenteMensile(ParCod) = campodbN(Rs_ImportoParente.Item("Importo"))
                        MyTabImportoParente(Day(campodbd(Rs_ImportoParente.Item("Data"))), ParCod) = Mensile(campodbN(Rs_ImportoParente.Item("Importo")), Mese, Anno, Cserv)
                        MyTabImportoParente2(Day(campodbd(Rs_ImportoParente.Item("Data"))), ParCod) = Mensile(campodbN(Rs_ImportoParente.Item("Importo_2")), Mese, Anno, Cserv)
                        MyTabGiornalieroExtraImporto1P(Day(campodbd(Rs_ImportoParente.Item("Data"))), ParCod) = campodbN(Rs_ImportoParente.Item("Importo1"))
                        MyTabGiornalieroExtraImporto2P(Day(campodbd(Rs_ImportoParente.Item("Data"))), ParCod) = campodbN(Rs_ImportoParente.Item("Importo2"))
                        MyTabGiornalieroExtraImporto3P(Day(campodbd(Rs_ImportoParente.Item("Data"))), ParCod) = campodbN(Rs_ImportoParente.Item("Importo3"))
                        MyTabGiornalieroExtraImporto4P(Day(campodbd(Rs_ImportoParente.Item("Data"))), ParCod) = campodbN(Rs_ImportoParente.Item("Importo4"))
                        ImportoMensileParente(ParCod) = campodbN(Rs_ImportoParente.Item("Importo"))
                        ImportoMensileParente2(ParCod) = campodbN(Rs_ImportoParente.Item("Importo_2"))
                    Else
                        MyTabImportoParente(Day(campodbd(Rs_ImportoParente.Item("Data"))), ParCod) = Annuale(campodbN(Rs_ImportoParente.Item("Importo")), Anno)
                        MyTabImportoParente2(Day(campodbd(Rs_ImportoParente.Item("Data"))), ParCod) = Annuale(campodbN(Rs_ImportoParente.Item("Importo_2")), Anno)
                        MyTabGiornalieroExtraImporto1P(Day(campodbd(Rs_ImportoParente.Item("Data"))), ParCod) = campodbN(Rs_ImportoParente.Item("Importo1"))
                        MyTabGiornalieroExtraImporto2P(Day(campodbd(Rs_ImportoParente.Item("Data"))), ParCod) = campodbN(Rs_ImportoParente.Item("Importo2"))
                        MyTabGiornalieroExtraImporto3P(Day(campodbd(Rs_ImportoParente.Item("Data"))), ParCod) = campodbN(Rs_ImportoParente.Item("Importo3"))
                        MyTabGiornalieroExtraImporto4P(Day(campodbd(Rs_ImportoParente.Item("Data"))), ParCod) = campodbN(Rs_ImportoParente.Item("Importo4"))
                    End If
                End If
                MyTabPercentuale(Day(campodbd(Rs_ImportoParente.Item("Data"))), ParCod) = campodbN(Rs_ImportoParente.Item("Percentuale")) * 100
            Else
                ParCod = campodbN(Rs_ImportoParente.Item("CODICEPARENTE"))

                If Not CodiceParenti(ParCod) Then
                    CodiceParenti(ParCod) = True
                    If campodb(Rs_ImportoParente.Item("TipoImporto")) = "G" Or campodb(Rs_ImportoParente.Item("TipoImporto")) = "" Then
                        MyTabImportoParente(1, ParCod) = campodbN(Rs_ImportoParente.Item("Importo"))
                        MyTabImportoParente2(1, ParCod) = campodbN(Rs_ImportoParente.Item("Importo_2"))
                        MyTabGiornalieroExtraImporto1P(1, ParCod) = campodbN(Rs_ImportoParente.Item("Importo1"))
                        MyTabGiornalieroExtraImporto2P(1, ParCod) = campodbN(Rs_ImportoParente.Item("Importo2"))
                        MyTabGiornalieroExtraImporto3P(1, ParCod) = campodbN(Rs_ImportoParente.Item("Importo3"))
                        MyTabGiornalieroExtraImporto4P(1, ParCod) = campodbN(Rs_ImportoParente.Item("Importo4"))
                    Else
                        If campodb(Rs_ImportoParente.Item("TipoImporto")) = "M" Then
                            MyTabParenteMensile(ParCod) = campodbN(Rs_ImportoParente.Item("Importo"))
                            MyTabImportoParente(1, ParCod) = Mensile(campodbN(Rs_ImportoParente.Item("Importo")), Mese, Anno, Cserv)
                            MyTabImportoParente2(1, ParCod) = Mensile(campodbN(Rs_ImportoParente.Item("Importo_2")), Mese, Anno, Cserv)
                            MyTabGiornalieroExtraImporto1P(1, ParCod) = campodbN(Rs_ImportoParente.Item("Importo1"))
                            MyTabGiornalieroExtraImporto2P(1, ParCod) = campodbN(Rs_ImportoParente.Item("Importo2"))
                            MyTabGiornalieroExtraImporto3P(1, ParCod) = campodbN(Rs_ImportoParente.Item("Importo3"))
                            MyTabGiornalieroExtraImporto4P(1, ParCod) = campodbN(Rs_ImportoParente.Item("Importo4"))
                            ImportoMensileParente(ParCod) = campodbN(Rs_ImportoParente.Item("Importo"))
                            ImportoMensileParente2(ParCod) = campodbN(Rs_ImportoParente.Item("Importo_2"))
                        Else
                            MyTabImportoParente(1, ParCod) = Annuale(campodbN(Rs_ImportoParente.Item("Importo")), Anno)
                            MyTabImportoParente2(1, ParCod) = Annuale(campodbN(Rs_ImportoParente.Item("Importo_2")), Anno)
                            MyTabGiornalieroExtraImporto1P(1, ParCod) = campodbN(Rs_ImportoParente.Item("Importo1"))
                            MyTabGiornalieroExtraImporto2P(1, ParCod) = campodbN(Rs_ImportoParente.Item("Importo2"))
                            MyTabGiornalieroExtraImporto3P(1, ParCod) = campodbN(Rs_ImportoParente.Item("Importo3"))
                            MyTabGiornalieroExtraImporto4P(1, ParCod) = campodbN(Rs_ImportoParente.Item("Importo4"))
                        End If
                    End If
                    MyTabPercentuale(1, ParCod) = campodbN(Rs_ImportoParente.Item("Percentuale")) * 100
                End If
            End If
            If MaxParenti < ParCod Then
                MaxParenti = ParCod
            End If
        Loop
        Rs_ImportoParente.Close()


    End Sub
    Public Function DecodificaExtra(ByVal Tipo As String, ByVal Codice As String, ByVal Mese As Long, ByVal Anno As Long, ByVal Numero As Long, ByVal Cserv As String) As Double
        Dim Rs_DecodificaExtra As New ADODB.Recordset
        Dim MySql As String


        MySql = "Select * From TabellaExtraFissi Where CODICEEXTRA = '" & Codice & "'"
        Rs_DecodificaExtra.Open(MySql, OspitiDb, ADODB.CursorTypeEnum.adOpenKeyset)
        If Not Rs_DecodificaExtra.EOF Then
            If MoveFromDb(Rs_DecodificaExtra.Fields("TipoImporto")) = "M" Then
                If Tipo = "O" Then
                    If MensileImportoExtrOspite(Numero) = 0 Then MensileImportoExtrOspite(Numero) = MoveFromDb(Rs_DecodificaExtra.Fields("Importo"))
                End If
                If Tipo = "P" Then
                    If MensileImportoExtrParent(Numero) = 0 Then MensileImportoExtrParent(Numero) = MoveFromDb(Rs_DecodificaExtra.Fields("Importo"))
                End If
                If Tipo = "C" Then
                    If MensileImportoExtrComune(Numero) = 0 Then MensileImportoExtrComune(Numero) = MoveFromDb(Rs_DecodificaExtra.Fields("Importo"))
                End If

                If Tipo = "J" Then
                    If ImportoExtrJolly(Numero) = 0 Then ImportoExtrJolly(Numero) = MoveFromDb(Rs_DecodificaExtra.Fields("Importo"))
                End If

                If Trim(MoveFromDb(Rs_DecodificaExtra.Fields("CodiceIva"))) = "" Then
                    StringaDegliErrori = StringaDegliErrori & "Specificare il codice iva sull extra fisso : " & Codice & vbNewLine
                End If

                DecodificaExtra = Mensile(MoveFromDb(Rs_DecodificaExtra.Fields("Importo")), Mese, Anno, Cserv)
            Else
                If Tipo = "O" Then
                    MensileImportoExtrOspite(Numero) = 0
                End If
                If Tipo = "P" Then
                    MensileImportoExtrParent(Numero) = 0
                End If
                If Tipo = "C" Then
                    MensileImportoExtrComune(Numero) = 0
                End If

                If Tipo = "J" Then
                    ImportoExtrJolly(Numero) = 0
                End If
                If Trim(MoveFromDb(Rs_DecodificaExtra.Fields("CodiceIva"))) = "" Then
                    StringaDegliErrori = StringaDegliErrori & "Specificare il codice iva sull extra fisso : " & Codice & vbNewLine
                End If

                DecodificaExtra = MoveFromDb(Rs_DecodificaExtra.Fields("Importo"))
            End If
        End If
        Rs_DecodificaExtra.Close()
    End Function

    Public Sub LeggiExtra(ByVal Cserv As String, ByVal CodOsp As Long, ByVal DataRet As Date, ByVal Mese As Long, ByVal Anno As Long)
        Dim Rs_ExtraRetta As New ADODB.Recordset
        Dim I As Integer
        Dim MySql As String
        Dim ExtraPresente As Integer
        MySql = "Select * From EXTRAOSPITE Where CENTROSERVIZIO = '" & Cserv & "' AND  CODICEOSPITE = " & CodOsp & " AND"
        MySql = MySql + " Data = {ts '" & Format(DataRet, "yyyy-MM-dd") & " 00:00:00'} "
        Rs_ExtraRetta.Open(MySql, OspitiDb, ADODB.CursorTypeEnum.adOpenKeyset, ADODB.LockTypeEnum.adLockOptimistic)


        For I = 0 To 10
            If (Month(DataRet) < Mese And Year(DataRet) = Anno) Or Year(DataRet) < Anno Then
                MyTabImpExtrOspite(1, I) = 0
                MyTabImpExtrParent(1, I) = 0
                MyTabImpExtrComune(1, I) = 0

                MyTabImpExtrJolly(1, I) = 0
            Else
                MyTabImpExtrOspite(Day(DataRet), I) = 0
                MyTabImpExtrParent(Day(DataRet), I) = 0
                MyTabImpExtrComune(Day(DataRet), I) = 0

                MyTabImpExtrJolly(Day(DataRet), I) = 0
            End If
        Next I

        Dim Trovato As Boolean = False

        On Error Resume Next
        Rs_ExtraRetta.MoveFirst()
        On Error GoTo 0
        Do While Not Rs_ExtraRetta.EOF
            Dim TabExtra As New Cls_TipoExtraFisso

            Trovato = False
            TabExtra.CODICEEXTRA = MoveFromDb(Rs_ExtraRetta.Fields("CODICEEXTRA"))
            TabExtra.Leggi(STRINGACONNESSIONEDB)


            If MoveFromDb(Rs_ExtraRetta.Fields("Ripartizione")) = "O" Then
                If (Month(DataRet) < Mese And Year(DataRet) = Anno) Or Year(DataRet) < Anno Then

                    If TabExtra.TipoImporto = "M" Then
                        Dim Cerca As Integer
                        For Cerca = 0 To XSO
                            If MyTabCodiceExtrOspite(Cerca) = MoveFromDb(Rs_ExtraRetta.Fields("CODICEEXTRA")) Then
                                Trovato = True
                            End If
                        Next
                    End If
                    If Trovato = False Then
                        Trovato = False
                        For ExtraPresente = 0 To XSO
                            If MyTabCodiceExtrOspite(ExtraPresente) = MoveFromDb(Rs_ExtraRetta.Fields("CODICEEXTRA")) Then
                                Trovato = True
                                Exit For
                            End If
                        Next

                        If Trovato = False Then
                            MyTabImpExtrOspite(1, XSO) = DecodificaExtra("O", MoveFromDb(Rs_ExtraRetta.Fields("CODICEEXTRA")), Mese, Anno, XSO, Cserv)
                            MyTabCodiceExtrOspite(XSO) = MoveFromDb(Rs_ExtraRetta.Fields("CODICEEXTRA"))

                            XSO = XSO + 1
                        Else
                            MyTabImpExtrOspite(1, ExtraPresente) = DecodificaExtra("O", MoveFromDb(Rs_ExtraRetta.Fields("CODICEEXTRA")), Mese, Anno, XSO, Cserv)
                            MyTabCodiceExtrOspite(ExtraPresente) = MoveFromDb(Rs_ExtraRetta.Fields("CODICEEXTRA"))
                        End If
                    End If
                Else

                    If TabExtra.TipoImporto = "M" Then
                        Dim Cerca As Integer
                        For Cerca = 0 To XSO
                            If MyTabCodiceExtrOspite(Cerca) = MoveFromDb(Rs_ExtraRetta.Fields("CODICEEXTRA")) Then
                                Trovato = True
                            End If
                        Next
                    End If
                    If Trovato = False Then
                        Trovato = False
                        For ExtraPresente = 0 To XSO
                            If MyTabCodiceExtrOspite(ExtraPresente) = MoveFromDb(Rs_ExtraRetta.Fields("CODICEEXTRA")) Then
                                Trovato = True
                                Exit For
                            End If
                        Next

                        If Trovato = False Then
                            MyTabImpExtrOspite(Day(DataRet), XSO) = DecodificaExtra("O", MoveFromDb(Rs_ExtraRetta.Fields("CODICEEXTRA")), Mese, Anno, XSO, Cserv)
                            MyTabCodiceExtrOspite(XSO) = MoveFromDb(Rs_ExtraRetta.Fields("CODICEEXTRA"))
                            XSO = XSO + 1
                        Else
                            MyTabImpExtrOspite(Day(DataRet), ExtraPresente) = DecodificaExtra("O", MoveFromDb(Rs_ExtraRetta.Fields("CODICEEXTRA")), Mese, Anno, XSO, Cserv)
                            MyTabCodiceExtrOspite(ExtraPresente) = MoveFromDb(Rs_ExtraRetta.Fields("CODICEEXTRA"))
                        End If
                    End If
                End If
            End If
            If MoveFromDb(Rs_ExtraRetta.Fields("Ripartizione")) = "P" Then
                If (Month(DataRet) < Mese And Year(DataRet) = Anno) Or Year(DataRet) < Anno Then

                    If TabExtra.TipoImporto = "M" Then
                        Dim Cerca As Integer
                        For Cerca = 0 To XSP
                            If MyTabCodiceExtrParent(Cerca) = MoveFromDb(Rs_ExtraRetta.Fields("CODICEEXTRA")) Then
                                Trovato = True
                            End If
                        Next
                    End If
                    If Trovato = False Then
                        Trovato = False
                        For ExtraPresente = 0 To XSP
                            If MyTabCodiceExtrOspite(ExtraPresente) = MoveFromDb(Rs_ExtraRetta.Fields("CODICEEXTRA")) Then
                                Trovato = True
                                Exit For
                            End If
                        Next
                        If Trovato = False Then
                            MyTabImpExtrParent(1, XSP) = DecodificaExtra("P", MoveFromDb(Rs_ExtraRetta.Fields("CODICEEXTRA")), Mese, Anno, XSP, Cserv)
                            MyTabCodiceExtrParent(XSP) = MoveFromDb(Rs_ExtraRetta.Fields("CODICEEXTRA"))
                            XSP = XSP + 1
                        Else
                            MyTabImpExtrParent(1, ExtraPresente) = DecodificaExtra("P", MoveFromDb(Rs_ExtraRetta.Fields("CODICEEXTRA")), Mese, Anno, XSP, Cserv)
                            MyTabCodiceExtrParent(ExtraPresente) = MoveFromDb(Rs_ExtraRetta.Fields("CODICEEXTRA"))
                        End If
                    End If
                Else
                    If TabExtra.TipoImporto = "M" Then
                        Dim Cerca As Integer
                        For Cerca = 0 To XSP
                            If MyTabCodiceExtrParent(Cerca) = MoveFromDb(Rs_ExtraRetta.Fields("CODICEEXTRA")) Then
                                Trovato = True
                            End If
                        Next
                    End If
                    If Trovato = False Then
                        Trovato = False
                        For ExtraPresente = 0 To XSP
                            If MyTabCodiceExtrOspite(ExtraPresente) = MoveFromDb(Rs_ExtraRetta.Fields("CODICEEXTRA")) Then
                                Trovato = True
                                Exit For
                            End If
                        Next
                        If Trovato = False Then
                            MyTabImpExtrParent(Day(DataRet), XSP) = DecodificaExtra("P", MoveFromDb(Rs_ExtraRetta.Fields("CODICEEXTRA")), Mese, Anno, XSP, Cserv)
                            MyTabCodiceExtrParent(XSP) = MoveFromDb(Rs_ExtraRetta.Fields("CODICEEXTRA"))
                            XSP = XSP + 1
                        Else
                            MyTabImpExtrParent(Day(DataRet), ExtraPresente) = DecodificaExtra("P", MoveFromDb(Rs_ExtraRetta.Fields("CODICEEXTRA")), Mese, Anno, XSP, Cserv)
                            MyTabCodiceExtrParent(ExtraPresente) = MoveFromDb(Rs_ExtraRetta.Fields("CODICEEXTRA"))
                        End If
                    End If
                End If
            End If
            If MoveFromDb(Rs_ExtraRetta.Fields("Ripartizione")) = "C" Then
                If (Month(DataRet) < Mese And Year(DataRet) = Anno) Or Year(DataRet) < Anno Then
                    If TabExtra.TipoImporto = "M" Then
                        Dim Cerca As Integer
                        For Cerca = 0 To XSP
                            If MyTabCodiceExtrParent(Cerca) = MoveFromDb(Rs_ExtraRetta.Fields("CODICEEXTRA")) Then
                                Trovato = True
                            End If
                        Next
                    End If
                    If Trovato = False Then
                        MyTabImpExtrComune(1, XSC) = DecodificaExtra("C", MoveFromDb(Rs_ExtraRetta.Fields("CODICEEXTRA")), Mese, Anno, XSC, Cserv)

                        MyTabImpExtrJolly(1, XSC) = DecodificaExtra("C", MoveFromDb(Rs_ExtraRetta.Fields("CODICEEXTRA")), Mese, Anno, XSC, Cserv)

                        MyTabCodiceExtrComune(XSC) = MoveFromDb(Rs_ExtraRetta.Fields("CODICEEXTRA"))

                        MyTabCodiceExtrJolly(XSC) = MoveFromDb(Rs_ExtraRetta.Fields("CODICEEXTRA"))
                        XSC = XSC + 1
                    End If
                Else
                    If TabExtra.TipoImporto = "M" Then
                        Dim Cerca As Integer
                        For Cerca = 0 To XSP
                            If MyTabCodiceExtrParent(Cerca) = MoveFromDb(Rs_ExtraRetta.Fields("CODICEEXTRA")) Then
                                Trovato = True
                            End If
                        Next
                    End If
                    If Trovato = False Then
                        MyTabImpExtrComune(Day(DataRet), XSC) = DecodificaExtra("C", MoveFromDb(Rs_ExtraRetta.Fields("CODICEEXTRA")), Mese, Anno, XSC, Cserv)

                        MyTabImpExtrJolly(Day(DataRet), XSC) = DecodificaExtra("C", MoveFromDb(Rs_ExtraRetta.Fields("CODICEEXTRA")), Mese, Anno, XSC, Cserv)

                        MyTabCodiceExtrComune(XSC) = MoveFromDb(Rs_ExtraRetta.Fields("CODICEEXTRA"))

                        MyTabCodiceExtrJolly(XSC) = MoveFromDb(Rs_ExtraRetta.Fields("CODICEEXTRA"))
                        XSC = XSC + 1
                    End If
                End If
            End If
            Rs_ExtraRetta.MoveNext()
        Loop
        Rs_ExtraRetta.Close()
    End Sub
    Public Sub CreaTabellaImportoRetta(ByVal Cserv As String, ByVal CodOsp As Long, ByVal Mese As Integer, ByVal Anno As Integer)
        Dim Rs_ImportoRetta As New ADODB.Recordset
        Dim MyDataini As Date, MyDatafine As Date
        Dim MySql As String, I As Long

        XSO = 0 : XSP = 1 : XSC = 0
        For I = 0 To 10
            ImportoExtrOspite(I) = 0
            ImportoExtrComune(I) = 0

            ImportoExtrJolly(I) = 0

            ImportoExtrParent(I) = 0
        Next I
        MyDataini = DateSerial(Anno, Mese, 1)
        MyDatafine = DateSerial(Anno, Mese, GiorniMese(Mese, Anno))

        MySql = "Select * From ImportoRetta Where CENTROSERVIZIO = '" & Cserv & "' and CODICEOSPITE = " & CodOsp & " and"
        MySql = MySql & " Data <= {ts '" & Format(MyDatafine, "yyyy-MM-dd") & " 00:00:00'}  Order by Data"
        Rs_ImportoRetta.Open(MySql, OspitiDb, ADODB.CursorTypeEnum.adOpenKeyset, ADODB.LockTypeEnum.adLockOptimistic)
        On Error Resume Next
        Rs_ImportoRetta.MoveLast()
        On Error GoTo 0
        Do While Not Rs_ImportoRetta.BOF
            If MoveFromDb(Rs_ImportoRetta.Fields("Data")) > MyDataini Then
                If MoveFromDb(Rs_ImportoRetta.Fields("TipoImporto")) = "G" Or MoveFromDb(Rs_ImportoRetta.Fields("TipoImporto")) = "" Or IsDBNull(Rs_ImportoRetta.Fields("TipoImporto").Value) Then
                    MyTabImportoRetta(Day(MoveFromDb(Rs_ImportoRetta.Fields("Data")))) = MoveFromDb(Rs_ImportoRetta.Fields("Importo"))
                Else
                    If MoveFromDb(Rs_ImportoRetta.Fields("TipoImporto")) = "M" Then
                        MyTabImportoRetta(Day(MoveFromDb(Rs_ImportoRetta.Fields("Data")))) = Mensile(MoveFromDb(Rs_ImportoRetta.Fields("Importo")), Mese, Anno, Cserv)
                        ImportoMensileRetta = MoveFromDb(Rs_ImportoRetta.Fields("Importo"))
                    Else
                        MyTabImportoRetta(Day(MoveFromDb(Rs_ImportoRetta.Fields("Data")))) = Annuale(MoveFromDb(Rs_ImportoRetta.Fields("Importo")), Anno)
                    End If
                End If                
                Call LeggiExtra(Cserv, CodOsp, MoveFromDb(Rs_ImportoRetta.Fields("Data")), Mese, Anno)
            Else
                If MoveFromDb(Rs_ImportoRetta.Fields("TipoImporto")) = "G" Or MoveFromDb(Rs_ImportoRetta.Fields("TipoImporto")) = "" Or IsDBNull(Rs_ImportoRetta.Fields("TipoImporto").Value) Then
                    MyTabImportoRetta(1) = MoveFromDb(Rs_ImportoRetta.Fields("Importo"))
                Else
                    If MoveFromDb(Rs_ImportoRetta.Fields("TipoImporto")) = "M" Then
                        MyTabImportoRetta(1) = Mensile(MoveFromDb(Rs_ImportoRetta.Fields("Importo")), Mese, Anno, Cserv)
                        ImportoMensileRetta = MoveFromDb(Rs_ImportoRetta.Fields("Importo"))
                    Else
                        MyTabImportoRetta(1) = Annuale(MoveFromDb(Rs_ImportoRetta.Fields("Importo")), Anno)
                    End If
                End If                
                Call LeggiExtra(Cserv, CodOsp, MoveFromDb(Rs_ImportoRetta.Fields("Data")), Mese, Anno)
                Exit Do
            End If
            Rs_ImportoRetta.MovePrevious()
        Loop
        Rs_ImportoRetta.Close()
    End Sub

    Public Sub CreaTabellaModalita(ByVal Cserv As String, ByVal CodOsp As Long, ByVal Mese As Integer, ByVal Anno As Integer, ByVal Verifica As Boolean)
        Dim Rs_Modalita As New ADODB.Recordset
        Dim MyDataini As Date, MyDatafine As Date
        Dim MySql As String

        MyDataini = DateSerial(Anno, Mese, 1)
        MyDatafine = DateSerial(Anno, Mese, GiorniMese(Mese, Anno))

        MySql = "Select * From Modalita Where CENTROSERVIZIO = '" & Cserv & "' and CODICEOSPITE = " & CodOsp & " and"
        MySql = MySql & " Data <= {ts '" & Format(MyDatafine, "yyyy-MM-dd") & " 00:00:00'}  Order by Data Desc"





        Dim Flag As Boolean = False
        Dim cmd As New OleDbCommand()
        cmd.CommandText = MySql
        cmd.Connection = OspitiCon

        Dim myPOSTreader As OleDbDataReader = cmd.ExecuteReader()
        Do While myPOSTreader.Read
            Flag = True
            If campodb(myPOSTreader.Item("Data")) > MyDataini Then
                MyTabModalita(Day(campodb(myPOSTreader.Item("Data")))) = campodb(myPOSTreader.Item("Modalita"))
            Else
                MyTabModalita(1) = campodb(myPOSTreader.Item("Modalita"))
                Exit Do
            End If
        Loop
        If Flag = False Then
            StringaDegliErrori = StringaDegliErrori & " Errore Modalita Calcolo Assente " & CodOsp & "," & CampoOspite(CodOsp, "NOME", False) & "  Centro Servizio " & Cserv & vbNewLine
        End If
        myPOSTreader.Close()

    End Sub







    Public Sub CreaTabellaUsl(ByVal Cserv As String, ByVal CodOsp As Long, ByVal Mese As Integer, ByVal Anno As Integer)
        'Dim Rs_ImportoUsl As New ADODB.Recordset
        Dim Rs_Regioni As New ADODB.Recordset
        Dim MyDataini As Date, MyDatafine As Date
        Dim MySql As String, MiaRegione As String, MioTipoRetta As String
        Dim I As Integer
        Dim OldRegione As String


        MyDataini = DateSerial(Anno, Mese, 1)
        MyDatafine = DateSerial(Anno, Mese, GiorniMese(Mese, Anno))

        MySql = "Select * From StatoAuto Where CENTROSERVIZIO = '" & Cserv & "' and CODICEOSPITE = " & CodOsp & " and"
        MySql = MySql & " Data <= {ts '" & Format(MyDatafine, "yyyy-MM-dd") & " 00:00:00'} Order by Data Desc"

        'Rs_ImportoUsl.Open(MySql, OspitiDb, ADODB.CursorTypeEnum.adOpenKeyset, ADODB.LockTypeEnum.adLockOptimistic)
        'If Rs_ImportoUsl.EOF Then

        'End If

        Dim cmd As New OleDbCommand()
        cmd.CommandText = MySql
        cmd.Connection = OspitiCon

        Dim Rs_ImportoUsl As OleDbDataReader = cmd.ExecuteReader()

        If Not Rs_ImportoUsl.Read Then
            MyTabAutoSufficente(1) = "A"
        Else
            Do
                If Format(campodbd(Rs_ImportoUsl.Item("Data")), "yyyyMMdd") > Format(MyDataini, "yyyyMMdd") Then
                    MyTabCodiceRegione(Day(campodbd(Rs_ImportoUsl.Item("Data")))) = campodb(Rs_ImportoUsl.Item("Usl"))                    
                    MyTabAutoSufficente(Day(campodbd(Rs_ImportoUsl.Item("Data")))) = campodb(Rs_ImportoUsl.Item("StatoAuto"))
                    MyTabRegioneTipoImporto(Day(campodbd(Rs_ImportoUsl.Item("Data")))) = campodb(Rs_ImportoUsl.Item("TipoRetta"))
                Else
                    MyTabCodiceRegione(1) = campodb(Rs_ImportoUsl.Item("Usl"))
                    MyTabAutoSufficente(1) = campodb(Rs_ImportoUsl.Item("StatoAuto"))
                    MyTabRegioneTipoImporto(1) = campodb(Rs_ImportoUsl.Item("TipoRetta"))
                    Exit Do
                End If

            Loop While Rs_ImportoUsl.Read
        End If
        Rs_ImportoUsl.Close()




        OldRegione = ""
        MiaRegione = "****"
        MioTipoRetta = ""
        For I = 1 To 31
            If MiaRegione <> MyTabCodiceRegione(I) And MyTabCodiceRegione(I) <> "" And MyTabCodiceRegione(I) <> "****" Then
                If CampoRegioneN(MyTabCodiceRegione(I), "MASTROCLIENTE", False) = 0 Then
                    StringaDegliErrori = StringaDegliErrori & "Mastro Non presente per Usl " & CampoRegioneT(MyTabCodiceRegione(I), "NOME") & " " & MyTabCodiceRegione(I) & vbNewLine
                End If
                If CampoRegioneN(MyTabCodiceRegione(I), "CONTOCLIENTE", False) = 0 Then
                    StringaDegliErrori = StringaDegliErrori & "Conto Non presente per Usl " & CampoRegioneT(MyTabCodiceRegione(I), "NOME") & " " & MyTabCodiceRegione(I) & vbNewLine
                End If
                If CampoRegioneN(MyTabCodiceRegione(I), "SOTTOCONTOCLIENTE", False) = 0 Then
                    StringaDegliErrori = StringaDegliErrori & "SottoConto Non presente per Usl " & CampoRegioneT(MyTabCodiceRegione(I), "NOME") & " " & MyTabCodiceRegione(I) & vbNewLine
                End If
                If CampoRegioneT(MyTabCodiceRegione(I), "TIPOOPERAZIONE", False) = "" Then
                    StringaDegliErrori = StringaDegliErrori & "Tipo operazione Non presente per Usl " & CampoRegioneT(MyTabCodiceRegione(I), "NOME") & " " & MyTabCodiceRegione(I) & vbNewLine
                Else
                    If Trim(CampoRegioneT(MyTabCodiceRegione(I), "PERIODO", False)) = "" Then
                        StringaDegliErrori = StringaDegliErrori & "Periodo Fatturazione Non Presente Per USL : " & CampoRegioneT(MyTabCodiceRegione(I), "NOME") & " " & MyTabCodiceRegione(I) & vbNewLine
                    End If
                End If
                If CampoRegioneT(MyTabCodiceRegione(I), "CODICEIVA", False) = "" Then
                    StringaDegliErrori = StringaDegliErrori & "Codice IVA Non presente per Usl " & CampoRegioneT(MyTabCodiceRegione(I), "NOME") & " " & MyTabCodiceRegione(I) & vbNewLine
                End If
            End If
            If MyTabCodiceRegione(I) <> "****" Then
                MiaRegione = MyTabCodiceRegione(I)
                MioTipoRetta = MyTabRegioneTipoImporto(I)
            End If
            If MiaRegione <> "" And MiaRegione <> "****" Then
                MyTabImportoRegione(I) = ImportoRegione(MiaRegione, MioTipoRetta, I, Mese, Anno, Cserv)
            End If
            If MiaRegione = "" Then
                MyTabImportoRegione(I) = 0
            End If
        Next I
    End Sub

    Public Sub CreaTabellaImportoComune(ByVal Cserv As String, ByVal CodOp As Long, ByVal Mese As Integer, ByVal Anno As Integer)
        Dim Rs_ImportoComune As New ADODB.Recordset
        Dim MyDataini As Date
        Dim MyDatafine As Date
        Dim MySql As String

        MyDataini = DateSerial(Anno, Mese, 1)
        MyDatafine = DateSerial(Anno, Mese, GiorniMese(Mese, Anno))

        MySql = "Select * From ImportoComune Where CENTROSERVIZIO = '" & Cserv & "' and CODICEOSPITE = " & CodOp & " and"
        MySql = MySql & " Data <= {ts '" & Format(MyDatafine, "yyyy-MM-dd") & " 00:00:00'}  Order by Data desc"



        Dim cmd As New OleDbCommand()
        cmd.CommandText = MySql
        cmd.Connection = OspitiCon

        Dim ReaderComuni As OleDbDataReader = cmd.ExecuteReader()
        Do While ReaderComuni.Read


            If campodbd(ReaderComuni.Item("Data")) > MyDataini Then
                If campodb(ReaderComuni.Item("TIPORETTA")) = "G" Or campodb(ReaderComuni.Item("TIPORETTA")) = "" Or IsDBNull(ReaderComuni.Item("TIPORETTA")) Then
                    MyTabImportoComune(Day(campodbd(ReaderComuni.Item("Data")))) = campodbN(ReaderComuni.Item("Importo"))
                    If DecodificaComune(campodb(ReaderComuni.Item("Prov")), campodb(ReaderComuni.Item("Comune"))) = "" And campodb(ReaderComuni.Item("Prov")) <> "" Then
                        StringaDegliErrori = StringaDegliErrori & "Errore comune non presente, o non decodificato per ospite " & CodOp & " " & CampoOspite(CodOp, "NOME", False) & vbNewLine
                    End If
                    'If Not VerificoComuneOspite(CodOp, campodb(ReaderComuni.Item("Prov")), campodb(ReaderComuni.Item("Comune")), Anno, Mese) Then
                    '   StringaDegliErrori = StringaDegliErrori & "Validita su Comune : " & DecodificaComune(campodb(ReaderComuni.Item("Prov")), campodb(ReaderComuni.Item("Comune"))) & " per ospite :" & CodOp & " " & CampoOspite(CodOp, "NOME", False) & " non valida " & vbNewLine
                    'End If

                    MyTabGiornalieroExtraImporto1C(Day(campodbd(ReaderComuni.Item("Data")))) = campodbN(ReaderComuni.Item("Importo1"))
                    MyTabGiornalieroExtraImporto2C(Day(campodbd(ReaderComuni.Item("Data")))) = campodbN(ReaderComuni.Item("Importo2"))
                    MyTabGiornalieroExtraImporto3C(Day(campodbd(ReaderComuni.Item("Data")))) = campodbN(ReaderComuni.Item("Importo3"))
                    MyTabGiornalieroExtraImporto4C(Day(campodbd(ReaderComuni.Item("Data")))) = campodbN(ReaderComuni.Item("Importo4"))
                Else
                    If campodb(ReaderComuni.Item("TIPORETTA")) = "M" Then
                        MyTabImportoComune(Day(campodb(ReaderComuni.Item("Data")))) = Mensile(campodbN(ReaderComuni.Item("Importo")), Mese, Anno, Cserv)

                        If DecodificaComune(campodb(ReaderComuni.Item("Prov")), campodb(ReaderComuni.Item("Comune"))) = "" And campodb(ReaderComuni.Item("Prov")) <> "" Then
                            StringaDegliErrori = StringaDegliErrori & "Errore comune non presente, o non decodificato per ospite " & CodOp & " " & CampoOspite(CodOp, "NOME", False) & vbNewLine
                        End If
                        'If Not VerificoComuneOspite(CodOp, campodb(ReaderComuni.Item("Prov")), campodb(ReaderComuni.Item("Comune")), Anno, Mese) Then
                        '  StringaDegliErrori = StringaDegliErrori & "Validita su Comune : " & DecodificaComune(campodb(ReaderComuni.Item("Prov")), campodb(ReaderComuni.Item("Comune"))) & " per ospite :" & CodOp & " " & CampoOspite(CodOp, "NOME", False) & " non valida " & vbNewLine
                        'End If
                        MyTabGiornalieroExtraImporto1C(Day(campodbd(ReaderComuni.Item("Data")))) = campodbN(ReaderComuni.Item("Importo1"))
                        MyTabGiornalieroExtraImporto2C(Day(campodbd(ReaderComuni.Item("Data")))) = campodbN(ReaderComuni.Item("Importo2"))
                        MyTabGiornalieroExtraImporto3C(Day(campodbd(ReaderComuni.Item("Data")))) = campodbN(ReaderComuni.Item("Importo3"))
                        MyTabGiornalieroExtraImporto4C(Day(campodbd(ReaderComuni.Item("Data")))) = campodbN(ReaderComuni.Item("Importo4"))

                        ImportoMensileComune = campodbN(ReaderComuni.Item("Importo"))
                    Else
                        MyTabImportoComune(Day(campodbd(ReaderComuni.Item("Data")))) = Annuale(campodbN(ReaderComuni.Item("Importo")), Anno)
                        If DecodificaComune(campodb(ReaderComuni.Item("Prov")), campodb(ReaderComuni.Item("Comune"))) = "" And campodb(ReaderComuni.Item("Prov")) <> "" Then
                            StringaDegliErrori = StringaDegliErrori & "Errore comune non presente, o non decodificato per ospite " & CodOp & " " & CampoOspite(CodOp, "NOME", False) & vbNewLine
                        End If
                        'If Not VerificoComuneOspite(CodOp, campodb(ReaderComuni.Item("Prov")), campodb(ReaderComuni.Item("Comune")), Anno, Mese) Then
                        '   StringaDegliErrori = StringaDegliErrori & "Validita su Comune : " & DecodificaComune(campodb(ReaderComuni.Item("Prov")), campodb(ReaderComuni.Item("Comune"))) & " per ospite :" & CodOp & " " & CampoOspite(CodOp, "NOME", False) & " non valida " & vbNewLine
                        'End If
                        MyTabGiornalieroExtraImporto1C(Day(campodbd(ReaderComuni.Item("Data")))) = campodbN(ReaderComuni.Item("Importo1"))
                        MyTabGiornalieroExtraImporto2C(Day(campodbd(ReaderComuni.Item("Data")))) = campodbN(ReaderComuni.Item("Importo2"))
                        MyTabGiornalieroExtraImporto3C(Day(campodbd(ReaderComuni.Item("Data")))) = campodbN(ReaderComuni.Item("Importo3"))
                        MyTabGiornalieroExtraImporto4C(Day(campodbd(ReaderComuni.Item("Data")))) = campodbN(ReaderComuni.Item("Importo4"))

                    End If
                End If
                MyTabCodiceComune(Day(campodbd(ReaderComuni.Item("Data")))) = campodb(ReaderComuni.Item("Comune"))
                MyTabCodiceProv(Day(campodbd(ReaderComuni.Item("Data")))) = campodb(ReaderComuni.Item("Prov"))
            Else
                If campodb(ReaderComuni.Item("TIPORETTA")) = "G" Or campodb(ReaderComuni.Item("TIPORETTA")) = "" Or IsDBNull(ReaderComuni.Item("TIPORETTA")) Then
                    MyTabImportoComune(1) = campodb(ReaderComuni.Item("Importo"))

                    If DecodificaComune(campodb(ReaderComuni.Item("Prov")), campodb(ReaderComuni.Item("Comune"))) = "" And campodb(ReaderComuni.Item("Prov")) <> "" Then
                        StringaDegliErrori = StringaDegliErrori & "Errore comune non presente, o non decodificato per ospite " & CodOp & " " & CampoOspite(CodOp, "NOME", False) & vbNewLine
                    End If
                    'If Not VerificoComuneOspite(CodOp, campodb(ReaderComuni.Item("Prov")), campodb(ReaderComuni.Item("Comune")), Anno, Mese) Then
                    '   StringaDegliErrori = StringaDegliErrori & "Validita su Comune : " & DecodificaComune(campodb(ReaderComuni.Item("Prov")), campodb(ReaderComuni.Item("Comune"))) & " per ospite :" & CodOp & " " & CampoOspite(CodOp, "NOME", False) & " non valida " & vbNewLine
                    'End If
                    MyTabGiornalieroExtraImporto1C(1) = campodbN(ReaderComuni.Item("Importo1"))
                    MyTabGiornalieroExtraImporto2C(1) = campodbN(ReaderComuni.Item("Importo2"))
                    MyTabGiornalieroExtraImporto3C(1) = campodbN(ReaderComuni.Item("Importo3"))
                    MyTabGiornalieroExtraImporto4C(1) = campodbN(ReaderComuni.Item("Importo4"))

                Else
                    If campodb(ReaderComuni.Item("TIPORETTA")) = "M" Then
                        MyTabImportoComune(1) = Mensile(campodbN(ReaderComuni.Item("Importo")), Mese, Anno, Cserv)

                        If DecodificaComune(campodb(ReaderComuni.Item("Prov")), campodb(ReaderComuni.Item("Comune"))) = "" And campodb(ReaderComuni.Item("Prov")) <> "" Then
                            StringaDegliErrori = StringaDegliErrori & "Errore comune non presente, o non decodificato per ospite " & CodOp & " " & CampoOspite(CodOp, "NOME", False) & vbNewLine
                        End If
                        'If Not VerificoComuneOspite(CodOp, campodb(ReaderComuni.Item("Prov")), campodb(ReaderComuni.Item("Comune")), Anno, Mese) Then
                        '   StringaDegliErrori = StringaDegliErrori & "Validita su Comune : " & DecodificaComune(campodb(ReaderComuni.Item("Prov")), campodb(ReaderComuni.Item("Comune"))) & " per ospite :" & CodOp & " " & CampoOspite(CodOp, "NOME", False) & " non valida " & vbNewLine
                        'End If
                        MyTabGiornalieroExtraImporto1C(1) = campodbN(ReaderComuni.Item("Importo1"))
                        MyTabGiornalieroExtraImporto2C(1) = campodbN(ReaderComuni.Item("Importo2"))
                        MyTabGiornalieroExtraImporto3C(1) = campodbN(ReaderComuni.Item("Importo3"))
                        MyTabGiornalieroExtraImporto4C(1) = campodbN(ReaderComuni.Item("Importo4"))

                        ImportoMensileComune = campodb(ReaderComuni.Item("Importo"))
                    Else
                        MyTabImportoComune(1) = Annuale(campodb(ReaderComuni.Item("Importo")), Anno)

                        If DecodificaComune(campodb(ReaderComuni.Item("Prov")), campodb(ReaderComuni.Item("Comune"))) = "" And campodb(ReaderComuni.Item("Prov")) <> "" Then
                            StringaDegliErrori = StringaDegliErrori & "Errore comune non presente, o non decodificato per ospite " & CodOp & " " & CampoOspite(CodOp, "NOME", False) & vbNewLine
                        End If
                        'If Not VerificoComuneOspite(CodOp, campodb(ReaderComuni.Item("Prov")), campodb(ReaderComuni.Item("Comune")), Anno, Mese) Then
                        '   StringaDegliErrori = StringaDegliErrori & "Validita su Comune : " & DecodificaComune(campodb(ReaderComuni.Item("Prov")), campodb(ReaderComuni.Item("Comune"))) & " per ospite :" & CodOp & " " & CampoOspite(CodOp, "NOME", False) & " non valida " & vbNewLine
                        'End If
                        MyTabGiornalieroExtraImporto1C(1) = campodbN(ReaderComuni.Item("Importo1"))
                        MyTabGiornalieroExtraImporto2C(1) = campodbN(ReaderComuni.Item("Importo2"))
                        MyTabGiornalieroExtraImporto3C(1) = campodbN(ReaderComuni.Item("Importo3"))
                        MyTabGiornalieroExtraImporto4C(1) = campodbN(ReaderComuni.Item("Importo4"))

                    End If
                End If
                MyTabCodiceComune(1) = campodb(ReaderComuni.Item("Comune"))
                MyTabCodiceProv(1) = campodb(ReaderComuni.Item("Prov"))
                Exit Do
            End If            
        Loop
        ReaderComuni.Close()

    End Sub


    Public Sub CreaTabellaImportoJolly(ByVal Cserv As String, ByVal CodOp As Long, ByVal Mese As Integer, ByVal Anno As Integer)
        Dim Rs_ImportoJolly As New ADODB.Recordset
        Dim MyDataini As Date
        Dim MyDatafine As Date
        Dim MySql As String

        MyDataini = DateSerial(Anno, Mese, 1)
        MyDatafine = DateSerial(Anno, Mese, GiorniMese(Mese, Anno))

        MySql = "Select * From ImportoJolly Where CENTROSERVIZIO = '" & Cserv & "' and CODICEOSPITE = " & CodOp & " and"
        MySql = MySql & " Data <= {ts '" & Format(MyDatafine, "yyyy-MM-dd") & " 00:00:00'}  Order by Data desc"

        Dim cmd As New OleDbCommand()
        cmd.CommandText = MySql
        cmd.Connection = OspitiCon

        Dim ReaderJolly As OleDbDataReader = cmd.ExecuteReader()

        Do While ReaderJolly.Read
            If campodbd(ReaderJolly.Item("Data")) > MyDataini Then
                If campodb(ReaderJolly.Item("TIPORETTA")) = "G" Or campodb(ReaderJolly.Item("TIPORETTA")) = "" Or IsDBNull(ReaderJolly.Item("TIPORETTA")) Then
                    MyTabImportoJolly(Day(campodbd(ReaderJolly.Item("Data")))) = campodbN(ReaderJolly.Item("Importo"))
                    If DecodificaComune(campodb(ReaderJolly.Item("Prov")), campodb(ReaderJolly.Item("Comune"))) = "" Then
                        StringaDegliErrori = StringaDegliErrori & "Errore Jolly non presente, o non decodificato per ospite " & CodOp & " " & CampoOspite(CodOp, "NOME", False) & vbNewLine
                    End If
                    MyTabGiornalieroExtraImporto1J(Day(campodbd(ReaderJolly.Item("Data")))) = campodbN(ReaderJolly.Item("Importo1"))
                    MyTabGiornalieroExtraImporto2J(Day(campodbd(ReaderJolly.Item("Data")))) = campodbN(ReaderJolly.Item("Importo2"))
                    MyTabGiornalieroExtraImporto3J(Day(campodbd(ReaderJolly.Item("Data")))) = campodbN(ReaderJolly.Item("Importo3"))
                    MyTabGiornalieroExtraImporto4J(Day(campodbd(ReaderJolly.Item("Data")))) = campodbN(ReaderJolly.Item("Importo4"))
                Else
                    If campodb(ReaderJolly.Item("TIPORETTA")) = "M" Then
                        MyTabImportoJolly(Day(campodbd(ReaderJolly.Item("Data")))) = Mensile(campodbN(ReaderJolly.Item("Importo")), Mese, Anno, Cserv)

                        If DecodificaComune(campodb(ReaderJolly.Item("Prov")), campodb(ReaderJolly.Item("Comune"))) = "" Then
                            StringaDegliErrori = StringaDegliErrori & "Errore Jolly non presente, o non decodificato per ospite " & CodOp & " " & CampoOspite(CodOp, "NOME", False) & vbNewLine
                        End If
                        MyTabGiornalieroExtraImporto1J(Day(campodbd(ReaderJolly.Item("Data")))) = campodbN(ReaderJolly.Item("Importo1"))
                        MyTabGiornalieroExtraImporto2J(Day(campodbd(ReaderJolly.Item("Data")))) = campodbN(ReaderJolly.Item("Importo2"))
                        MyTabGiornalieroExtraImporto3J(Day(campodbd(ReaderJolly.Item("Data")))) = campodbN(ReaderJolly.Item("Importo3"))
                        MyTabGiornalieroExtraImporto4J(Day(campodbd(ReaderJolly.Item("Data")))) = campodbN(ReaderJolly.Item("Importo4"))

                        ImportoMensileJolly = campodbN(ReaderJolly.Item("Importo"))
                    Else
                        MyTabImportoJolly(Day(campodbd(ReaderJolly.Item("Data")))) = Annuale(campodbN(ReaderJolly.Item("Importo")), Anno)
                        If DecodificaComune(campodb(ReaderJolly.Item("Prov")), campodb(ReaderJolly.Item("Comune"))) = "" Then
                            StringaDegliErrori = StringaDegliErrori & "Errore Jolly non presente, o non decodificato per ospite " & CodOp & " " & CampoOspite(CodOp, "NOME", False) & vbNewLine
                        End If
                        MyTabGiornalieroExtraImporto1J(Day(campodbd(ReaderJolly.Item("Data")))) = campodbN(ReaderJolly.Item("Importo1"))
                        MyTabGiornalieroExtraImporto2J(Day(campodbd(ReaderJolly.Item("Data")))) = campodbN(ReaderJolly.Item("Importo2"))
                        MyTabGiornalieroExtraImporto3J(Day(campodbd(ReaderJolly.Item("Data")))) = campodbN(ReaderJolly.Item("Importo3"))
                        MyTabGiornalieroExtraImporto4J(Day(campodbd(ReaderJolly.Item("Data")))) = campodbN(ReaderJolly.Item("Importo4"))

                    End If
                End If
                MyTabCodiceJolly(Day(campodbd(ReaderJolly.Item("Data")))) = campodb(ReaderJolly.Item("Comune"))
                MyTabCodiceProvJolly(Day(campodbd(ReaderJolly.Item("Data")))) = campodb(ReaderJolly.Item("Prov"))
            Else
                If campodb(ReaderJolly.Item("TIPORETTA")) = "G" Or campodb(ReaderJolly.Item("TIPORETTA")) = "" Or IsDBNull(ReaderJolly.Item("TIPORETTA")) Then
                    MyTabImportoJolly(1) = campodb(ReaderJolly.Item("Importo"))

                    If DecodificaComune(campodb(ReaderJolly.Item("Prov")), campodb(ReaderJolly.Item("Comune"))) = "" Then
                        StringaDegliErrori = StringaDegliErrori & "Errore Jolly non presente, o non decodificato per ospite " & CodOp & " " & CampoOspite(CodOp, "NOME", False) & vbNewLine
                    End If
                    MyTabGiornalieroExtraImporto1J(1) = campodbN(ReaderJolly.Item("Importo1"))
                    MyTabGiornalieroExtraImporto2J(1) = campodbN(ReaderJolly.Item("Importo2"))
                    MyTabGiornalieroExtraImporto3J(1) = campodbN(ReaderJolly.Item("Importo3"))
                    MyTabGiornalieroExtraImporto4J(1) = campodbN(ReaderJolly.Item("Importo4"))

                Else
                    If campodb(ReaderJolly.Item("TIPORETTA")) = "M" Then
                        MyTabImportoJolly(1) = Mensile(campodbN(ReaderJolly.Item("Importo")), Mese, Anno, Cserv)

                        If DecodificaComune(campodb(ReaderJolly.Item("Prov")), campodb(ReaderJolly.Item("Comune"))) = "" Then
                            StringaDegliErrori = StringaDegliErrori & "Errore Jolly non presente, o non decodificato per ospite " & CodOp & " " & CampoOspite(CodOp, "NOME", False) & vbNewLine
                        End If

                        MyTabGiornalieroExtraImporto1J(1) = campodbN(ReaderJolly.Item("Importo1"))
                        MyTabGiornalieroExtraImporto2J(1) = campodbN(ReaderJolly.Item("Importo2"))
                        MyTabGiornalieroExtraImporto3J(1) = campodbN(ReaderJolly.Item("Importo3"))
                        MyTabGiornalieroExtraImporto4J(1) = campodbN(ReaderJolly.Item("Importo4"))

                        ImportoMensileJolly = campodbN(ReaderJolly.Item("Importo"))
                    Else
                        MyTabImportoJolly(1) = Annuale(campodbN(ReaderJolly.Item("Importo")), Anno)

                        If DecodificaComune(campodb(ReaderJolly.Item("Prov")), campodb(ReaderJolly.Item("Comune"))) = "" Then
                            StringaDegliErrori = StringaDegliErrori & "Errore Jolly non presente, o non decodificato per ospite " & CodOp & " " & CampoOspite(CodOp, "NOME", False) & vbNewLine
                        End If

                        MyTabGiornalieroExtraImporto1J(1) = campodbN(ReaderJolly.Item("Importo1"))
                        MyTabGiornalieroExtraImporto2J(1) = campodbN(ReaderJolly.Item("Importo2"))
                        MyTabGiornalieroExtraImporto3J(1) = campodbN(ReaderJolly.Item("Importo3"))
                        MyTabGiornalieroExtraImporto4J(1) = campodbN(ReaderJolly.Item("Importo4"))

                    End If
                End If
                MyTabCodiceJolly(1) = campodb(ReaderJolly.Item("Comune"))
                MyTabCodiceProvJolly(1) = campodb(ReaderJolly.Item("Prov"))
                Exit Do
            End If
        Loop
        ReaderJolly.Close()
    End Sub


    Public Sub CreaTabellaImportoOspite(ByVal Cserv As String, ByVal CodOsp As Long, ByVal Mese As Integer, ByVal Anno As Integer)
        Dim Rs_ImportoOspite As New ADODB.Recordset
        Dim MyDataini As Date, MyDatafine As Date
        Dim MySql As String, TipoOperazione As String
        MyDataini = DateSerial(Anno, Mese, 1)
        MyDatafine = DateSerial(Anno, Mese, GiorniMese(Mese, Anno))
        ImportoMensileOspite = 0
        MyTabOspitiMensile = 0

        Dim ImportoLetto As Integer = 0


        Dim LO_CodiceOspite As New ClsOspite

        LO_CodiceOspite.Leggi(STRINGACONNESSIONEDB, CodOsp)



        Dim KCs As New Cls_DatiOspiteParenteCentroServizio

        KCs.CentroServizio = Cserv
        KCs.CodiceOspite = CodOsp
        KCs.CodiceParente = 0
        KCs.Leggi(STRINGACONNESSIONEDB)

        REM Assegna la tipologia operazione e l'aliquota prendendo quella relativa al cserv
        If KCs.CodiceOspite <> 0 Then
            LO_CodiceOspite.TIPOOPERAZIONE = KCs.TipoOperazione
            LO_CodiceOspite.CODICEIVA = KCs.AliquotaIva
            LO_CodiceOspite.FattAnticipata = KCs.Anticipata
            LO_CodiceOspite.MODALITAPAGAMENTO = KCs.ModalitaPagamento
            LO_CodiceOspite.Compensazione = KCs.Compensazione
            LO_CodiceOspite.SETTIMANA = KCs.Settimana

        End If

        TipoOperazione = LO_CodiceOspite.TIPOOPERAZIONE ' CampoOspite(CodOsp, "TipoOperazione", False)

        MySql = "Select * From ImportoOspite Where CENTROSERVIZIO = '" & Cserv & "' and CODICEOSPITE = " & CodOsp & " and"
        MySql = MySql & " Data <= {ts '" & Format(MyDatafine, "yyyy-MM-dd") & " 00:00:00'}  Order by Data desc"


        Dim cmd As New OleDbCommand()
        cmd.CommandText = MySql
        cmd.Connection = OspitiCon

        Dim ReaderOspite As OleDbDataReader = cmd.ExecuteReader()

        Do While ReaderOspite.Read
            If campodbd(ReaderOspite.Item("Data")) > MyDataini Then
                If LO_CodiceOspite.Compensazione = "" Then
                    StringaDegliErrori = StringaDegliErrori & "Compensazione mancante per Ospite " & CodOsp & " - " & CampoOspite(CodOsp, "NOME", False) & Chr(13)
                End If
                If LO_CodiceOspite.PERIODO = "" Then
                    StringaDegliErrori = StringaDegliErrori & "PERIODO mancante per Ospite " & CodOsp & " - " & CampoOspite(CodOsp, "NOME", False) & Chr(13)
                End If
                If TipoOperazione = "" Then
                    StringaDegliErrori = StringaDegliErrori & "Tipo Operazione mancante per Ospite " & CodOsp & " - " & CampoOspite(CodOsp, "NOME", False) & Chr(13)
                Else
                    If Trim(CampoOspite(CodOsp, "PERIODO", False)) = "" Then
                        StringaDegliErrori = StringaDegliErrori & "Periodo Fatturazione Non Presente Per Ospite : " & CodOsp & " - " & CampoOspite(CodOsp, "NOME", False) & Chr(13)
                    End If
                End If

                If Format(campodbd(ReaderOspite.Item("Data")), "yyyyMMdd") <> Format(PrimoDataAccoglimentoOspite, "yyyyMMdd") Then
                    GiornoCambioRettaOspite = Day(campodbd(ReaderOspite.Item("Data")))
                End If

                If campodb(ReaderOspite.Item("TIPORETTA")) = "M" Then
                    ImportoMensileOspite = campodbN(ReaderOspite.Item("Importo"))
                    MyTabOspitiMensile = campodbN(ReaderOspite.Item("Importo"))
                    MyTabImportoOspite(Day(campodbd(ReaderOspite.Item("Data")))) = Mensile(campodbN(ReaderOspite.Item("Importo")), Mese, Anno, Cserv)
                    MyTabImportoOspite2(Day(campodbd(ReaderOspite.Item("Data")))) = Mensile(campodbN(ReaderOspite.Item("Importo_2")), Mese, Anno, Cserv)

                    MyTabGiornalieroExtraImporto1(Day(campodbd(ReaderOspite.Item("Data")))) = campodbN(ReaderOspite.Item("Importo1"))
                    MyTabGiornalieroExtraImporto2(Day(campodbd(ReaderOspite.Item("Data")))) = campodbN(ReaderOspite.Item("Importo2"))
                    MyTabGiornalieroExtraImporto3(Day(campodbd(ReaderOspite.Item("Data")))) = campodbN(ReaderOspite.Item("Importo3"))
                    MyTabGiornalieroExtraImporto4(Day(campodbd(ReaderOspite.Item("Data")))) = campodbN(ReaderOspite.Item("Importo4"))
                Else
                    If campodb(ReaderOspite.Item("TIPORETTA")) = "A" Then
                        MyTabImportoOspite(Day(campodbd(ReaderOspite.Item("Data")))) = Annuale(campodbN(ReaderOspite.Item("Importo")), Anno)
                        MyTabImportoOspite2(Day(campodbd(ReaderOspite.Item("Data")))) = Annuale(campodbN(ReaderOspite.Item("Importo_2")), Anno)
                        MyTabGiornalieroExtraImporto1(Day(campodbd(ReaderOspite.Item("Data")))) = campodbN(ReaderOspite.Item("Importo1"))
                        MyTabGiornalieroExtraImporto2(Day(campodbd(ReaderOspite.Item("Data")))) = campodbN(ReaderOspite.Item("Importo2"))
                        MyTabGiornalieroExtraImporto3(Day(campodbd(ReaderOspite.Item("Data")))) = campodbN(ReaderOspite.Item("Importo3"))
                        MyTabGiornalieroExtraImporto4(Day(campodbd(ReaderOspite.Item("Data")))) = campodbN(ReaderOspite.Item("Importo4"))
                    Else
                        MyTabImportoOspite(Day(campodbd(ReaderOspite.Item("Data")))) = campodbN(ReaderOspite.Item("Importo"))
                        MyTabImportoOspite2(Day(campodbd(ReaderOspite.Item("Data")))) = campodbN(ReaderOspite.Item("Importo_2"))
                        MyTabGiornalieroExtraImporto1(Day(campodbd(ReaderOspite.Item("Data")))) = campodbN(ReaderOspite.Item("Importo1"))
                        MyTabGiornalieroExtraImporto2(Day(campodbd(ReaderOspite.Item("Data")))) = campodbN(ReaderOspite.Item("Importo2"))
                        MyTabGiornalieroExtraImporto3(Day(campodbd(ReaderOspite.Item("Data")))) = campodbN(ReaderOspite.Item("Importo3"))
                        MyTabGiornalieroExtraImporto4(Day(campodbd(ReaderOspite.Item("Data")))) = campodbN(ReaderOspite.Item("Importo4"))
                    End If
                End If
            Else
                If LO_CodiceOspite.Compensazione = "" Then
                    StringaDegliErrori = StringaDegliErrori & "Compensazione mancante per Ospite " & CodOsp & " - " & CampoOspite(CodOsp, "NOME", False) & Chr(13)
                End If
                If LO_CodiceOspite.PERIODO = "" Then
                    StringaDegliErrori = StringaDegliErrori & "PERIODO mancante per Ospite " & CodOsp & " - " & CampoOspite(CodOsp, "NOME", False) & Chr(13)
                End If
                If TipoOperazione = "" Then
                    StringaDegliErrori = StringaDegliErrori & "Tipo Operazione mancante per Ospite " & CodOsp & " - " & CampoOspite(CodOsp, "NOME", False) & Chr(13)
                Else
                    If Trim(CampoOspite(CodOsp, "PERIODO", False)) = "" Then
                        StringaDegliErrori = StringaDegliErrori & "Periodo Fatturazione Non Presente Per Ospite : " & CodOsp & " - " & CampoOspite(CodOsp, "NOME", False) & Chr(13)
                    End If
                End If

                If campodb(ReaderOspite.Item("TIPORETTA")) = "M" Then
                    ImportoMensileOspite = campodbN(ReaderOspite.Item("Importo"))
                    MyTabOspitiMensile = campodbN(ReaderOspite.Item("Importo"))
                    MyTabImportoOspite(1) = Mensile(campodbN(ReaderOspite.Item("Importo")), Mese, Anno, Cserv)
                    MyTabImportoOspite2(1) = Mensile(campodbN(ReaderOspite.Item("Importo_2")), Mese, Anno, Cserv)

                    MyTabGiornalieroExtraImporto1(1) = campodbN(ReaderOspite.Item("Importo1"))
                    MyTabGiornalieroExtraImporto2(1) = campodbN(ReaderOspite.Item("Importo2"))
                    MyTabGiornalieroExtraImporto3(1) = campodbN(ReaderOspite.Item("Importo3"))
                    MyTabGiornalieroExtraImporto4(1) = campodbN(ReaderOspite.Item("Importo4"))
                Else
                    If campodb(ReaderOspite.Item("TIPORETTA")) = "A" Then
                        MyTabImportoOspite(1) = Annuale(campodbN(ReaderOspite.Item("Importo")), Anno)
                        MyTabImportoOspite2(1) = Annuale(campodbN(ReaderOspite.Item("Importo_2")), Anno)
                        MyTabGiornalieroExtraImporto1(1) = campodbN(ReaderOspite.Item("Importo1"))
                        MyTabGiornalieroExtraImporto2(1) = campodbN(ReaderOspite.Item("Importo2"))
                        MyTabGiornalieroExtraImporto3(1) = campodbN(ReaderOspite.Item("Importo3"))
                        MyTabGiornalieroExtraImporto4(1) = campodbN(ReaderOspite.Item("Importo4"))
                    Else
                        MyTabImportoOspite(1) = campodbN(ReaderOspite.Item("Importo"))
                        MyTabImportoOspite2(1) = campodbN(ReaderOspite.Item("Importo_2"))
                        MyTabGiornalieroExtraImporto1(1) = campodbN(ReaderOspite.Item("Importo1"))
                        MyTabGiornalieroExtraImporto2(1) = campodbN(ReaderOspite.Item("Importo2"))
                        MyTabGiornalieroExtraImporto3(1) = campodbN(ReaderOspite.Item("Importo3"))
                        MyTabGiornalieroExtraImporto4(1) = campodbN(ReaderOspite.Item("Importo4"))
                    End If
                End If
                Exit Do
            End If
        Loop
        ReaderOspite.Close()
    End Sub

    Public Sub PresenzeDomiciliare(ByVal Cserv As String, ByVal CodOsp As Long, ByVal Mese As Integer, ByVal Anno As Integer, Optional ByVal Ricalcolo As Boolean = False, Optional ByVal CreoAddAccr As Integer = 0, Optional ByVal DataMov As Date = Nothing, Optional ByVal Tdesc As String = "")
        Dim Rs_AssenzeCentroDiurno As New ADODB.Recordset
        Dim Sql As String

        If CodOsp = 1416 Then
            CodOsp = 1416
        End If
        For i = 1 To GiorniMese(Mese, Anno)
            MyTabCausale(i) = "dA"
        Next


        Dim Acco As New Cls_Movimenti

        Acco.CENTROSERVIZIO = Cserv
        Acco.CodiceOspite = CodOsp

        Dim VettoreCodice(50) As String
        Dim VettoreImporto(50) As Double
        Dim VettoreDescrizione(50) As String
        Dim VettoreTipo(50) As String
        Dim VettoreQuantita(50) As Double

        Dim MaxAddebito As Integer
        Dim OldConsideraIngresso As Integer

        Sql = "Select * From MovimentiDomiciliare Where CENTROSERVIZIO = '" & Cserv & "' And CODICEOSPITE = " & CodOsp & " And Data >= ? And Data <= ? Order by Data,OraInizio"
        Dim OldData As Date

        Dim cmd As New OleDbCommand()
        cmd.CommandText = Sql
        cmd.Parameters.AddWithValue("@Data", DateSerial(Anno, Mese, 1))
        cmd.Parameters.AddWithValue("@Data", DateSerial(Anno, Mese, GiorniMese(Mese, Anno)))
        cmd.Connection = OspitiCon
        Dim ReaderAssenzeCentroDiurno As OleDbDataReader = cmd.ExecuteReader()
        Do While ReaderAssenzeCentroDiurno.Read

            Acco.TipoMov = ""
            Acco.UltimaMovimentoPrimaData(STRINGACONNESSIONEDB, Acco.CodiceOspite, Acco.CENTROSERVIZIO, DateSerial(Anno, Mese, Day(campodbd(ReaderAssenzeCentroDiurno.Item("Data")))))
            If Acco.TipoMov = "13" Or Acco.TipoMov = "" Then
                StringaDegliErrori = StringaDegliErrori & "Errore movimento domiciliare per utente non presente :" & CampoOspite(CodOsp, "NOME") & vbNewLine
            End If

            MyTabCausale(Day(campodbd(ReaderAssenzeCentroDiurno.Item("Data")))) = "dP"

            Dim Minuti As Integer
            Minuti = DateDiff("n", campodbd(ReaderAssenzeCentroDiurno.Item("OraInizio")), campodbd(ReaderAssenzeCentroDiurno.Item("OraFine")))

            Dim FasciaOraria As Integer = 1

            If Hour(campodbd(ReaderAssenzeCentroDiurno.Item("OraInizio"))) >= 22 Or Hour(campodbd(ReaderAssenzeCentroDiurno.Item("OraInizio"))) < 7 Then
                FasciaOraria = 2
            End If
            Dim PrimoIngresso As Boolean = True
            If OldData = campodbd(ReaderAssenzeCentroDiurno.Item("Data")) And OldConsideraIngresso = 1 Then
                PrimoIngresso = False
            End If

            Call CreaAddebitiDomicliari(campodb(ReaderAssenzeCentroDiurno.Item("Tipologia")), Minuti, VettoreTipo, VettoreCodice, VettoreDescrizione, VettoreImporto, MaxAddebito, campodbN(ReaderAssenzeCentroDiurno.Item("CreatoDocumentoOspiti")), Cserv, CodOsp, campodbd(ReaderAssenzeCentroDiurno.Item("Data")), Val(campodb(ReaderAssenzeCentroDiurno.Item("Operatore"))), PrimoIngresso, Val(campodb(ReaderAssenzeCentroDiurno.Item("ID"))), VettoreQuantita, FasciaOraria)
            OldData = campodbd(ReaderAssenzeCentroDiurno.Item("Data"))

            Dim MP As New Cls_TipoDomiciliare

            MP.Codice = campodb(ReaderAssenzeCentroDiurno.Item("Tipologia"))
            MP.Leggi(STRINGACONNESSIONEDB, MP.Codice)
            OldConsideraIngresso = MP.ConsideraIngresso
        Loop
        ReaderAssenzeCentroDiurno.Close()


        Dim X As Double
        Dim Comune As String = ""
        Dim Jolly As String = ""
        Dim Regione As String = ""
        For X = 1 To MaxAddebito
            If VettoreTipo(X) = "C" Then
                Dim ComuneS As New Cls_ImportoComune

                ComuneS.CENTROSERVIZIO = Cserv
                ComuneS.CODICEOSPITE = CodOsp
                ComuneS.UltimaDataAData(STRINGACONNESSIONEDB, ComuneS.CODICEOSPITE, ComuneS.CENTROSERVIZIO, DateSerial(Anno, Mese, 1))

                Comune = ComuneS.PROV & ComuneS.COMUNE

                If Comune = "" Then
                    ComuneS.CENTROSERVIZIO = Cserv
                    ComuneS.CODICEOSPITE = CodOsp
                    ComuneS.UltimaDataAData(STRINGACONNESSIONEDB, ComuneS.CODICEOSPITE, ComuneS.CENTROSERVIZIO, DateSerial(Anno, Mese, GiorniMese(Mese, Anno)))

                    Comune = ComuneS.PROV & ComuneS.COMUNE

                End If
            End If
            If VettoreTipo(X) = "J" Then
                Dim ComuneS As New Cls_ImportoJolly

                ComuneS.CENTROSERVIZIO = Cserv
                ComuneS.CODICEOSPITE = CodOsp
                ComuneS.UltimaData(STRINGACONNESSIONEDB, ComuneS.CODICEOSPITE, ComuneS.CENTROSERVIZIO)

                Jolly = ComuneS.PROV & ComuneS.COMUNE
            End If
            If VettoreTipo(X) = "R" Then
                Dim RegS As New Cls_StatoAuto

                RegS.CENTROSERVIZIO = Cserv
                RegS.CODICEOSPITE = CodOsp
                RegS.UltimaData(STRINGACONNESSIONEDB, RegS.CODICEOSPITE, RegS.CENTROSERVIZIO)

                Regione = RegS.USL
            End If
        Next
        If Ricalcolo = False Then
            For X = 1 To MaxAddebito

                If VettoreTipo(X) = "O" Or VettoreTipo(X) = "P" Then
                    Dim RettaTotale As New Cls_rettatotale

                    RettaTotale.CODICEOSPITE = CodOsp
                    RettaTotale.CENTROSERVIZIO = Cserv
                    RettaTotale.Data = DateSerial(Anno, Mese, GiorniMese(Mese, Anno))
                    RettaTotale.RettaTotaleAData(STRINGACONNESSIONEDB, CodOsp, Cserv, RettaTotale.Data)

                    If RettaTotale.TipoRetta <> "" Then
                        Dim TipoRetta As New Cls_TipoRetta

                        TipoRetta.Codice = RettaTotale.TipoRetta
                        TipoRetta.Leggi(STRINGACONNESSIONEDB, TipoRetta.Codice)

                        If TipoRetta.MassimaleDomiciliari > 0 Then

                            If TipoRetta.MassimaleDomiciliari < VettoreImporto(X) And (TipoRetta.TipoAddebitoMassimale = "" Or (TipoRetta.TipoAddebitoMassimale = VettoreCodice(X))) Then
                                VettoreImporto(X) = TipoRetta.MassimaleDomiciliari
                            End If
                        End If

                    End If

                End If

                Dim AddM As New Cls_AddebitiAccrediti

                AddM.CENTROSERVIZIO = Cserv
                AddM.CodiceOspite = CodOsp
                AddM.chiaveselezione = X
                AddM.MESECOMPETENZA = Mese
                AddM.ANNOCOMPETENZA = Anno
                AddM.TipoMov = "AD"

                AddM.CodiceIva = VettoreCodice(X)
                AddM.Data = DateSerial(Anno, Mese, GiorniMese(Mese, Anno))
                AddM.Descrizione = VettoreDescrizione(X)
                AddM.IMPORTO = VettoreImporto(X)
                AddM.Quantita = VettoreQuantita(X)
                AddM.PARENTE = 0
                AddM.PROVINCIA = ""
                AddM.COMUNE = ""
                AddM.REGIONE = ""
                AddM.RIFERIMENTO = VettoreTipo(X)
                If AddM.RIFERIMENTO = "C" Then
                    AddM.PROVINCIA = Mid(Comune & Space(6), 1, 3)
                    AddM.COMUNE = Mid(Comune & Space(6), 4, 3)
                End If
                If AddM.RIFERIMENTO = "J" Then
                    AddM.PROVINCIA = Mid(Jolly & Space(6), 1, 3)
                    AddM.COMUNE = Mid(Jolly & Space(6), 4, 3)
                End If
                If AddM.RIFERIMENTO = "R" Then
                    AddM.REGIONE = Mid(Regione & Space(6), 1, 4)
                End If
                AddM.AggiornaDB(STRINGACONNESSIONEDB)
            Next
        Else
            For X = 1 To MaxAddebito
                If VettoreTipo(X) = "O" Or VettoreTipo(X) = "P" Then
                    Dim RettaTotale As New Cls_rettatotale

                    RettaTotale.CODICEOSPITE = CodOsp
                    RettaTotale.CENTROSERVIZIO = Cserv
                    RettaTotale.Data = DateSerial(Anno, Mese, GiorniMese(Mese, Anno))
                    RettaTotale.RettaTotaleAData(STRINGACONNESSIONEDB, CodOsp, Cserv, RettaTotale.Data)

                    If RettaTotale.TipoRetta <> "" Then
                        Dim TipoRetta As New Cls_TipoRetta

                        TipoRetta.Codice = RettaTotale.TipoRetta
                        TipoRetta.Leggi(STRINGACONNESSIONEDB, TipoRetta.Codice)

                        If TipoRetta.MassimaleDomiciliari > 0 Then

                            If TipoRetta.MassimaleDomiciliari < VettoreImporto(X) And (TipoRetta.TipoAddebitoMassimale = "" Or (TipoRetta.TipoAddebitoMassimale = VettoreCodice(X))) Then
                                VettoreImporto(X) = TipoRetta.MassimaleDomiciliari
                            End If
                        End If

                    End If

                End If

                Dim AddM As New Cls_AddebitiAccrediti
                Dim ImpPresente As Double = 0

                ImpPresente = AddM.ImportoAddebitoMese(STRINGACONNESSIONEDB, CodOsp, Cserv, VettoreCodice(X), VettoreTipo(X), DateSerial(Anno, Mese, 1), DateSerial(Anno, Mese, GiorniMese(Mese, Anno)))

                If ImpPresente <> VettoreImporto(X) Then

                    If VettoreTipo(X) = "O" Then
                        IndiceVariazione = IndiceVariazione + 1

                        RicalcoloGriglia_CServ(IndiceVariazione) = Cserv
                        RicalcoloGriglia_CodiceOspite(IndiceVariazione) = CodOsp
                        RicalcoloGriglia_Anno(IndiceVariazione) = Anno
                        RicalcoloGriglia_Mese(IndiceVariazione) = Mese
                        RicalcoloGriglia_Ospite(IndiceVariazione) = Modulo.MathRound(VettoreImporto(X) - ImpPresente, 2)
                        RicalcoloGriglia_Parente_id(IndiceVariazione) = 0
                        RicalcoloGriglia_Parente(IndiceVariazione) = 0
                        RicalcoloGriglia_Comune(IndiceVariazione) = 0
                        RicalcoloGriglia_Jolly(IndiceVariazione) = 0
                        RicalcoloGriglia_Regione(IndiceVariazione) = 0
                        RicalcoloGriglia_Descrizione(IndiceVariazione) = ""

                        If CreoAddAccr = 1 Then
                            Dim MyAddAcc As New Cls_AddebitiAccrediti


                            MyAddAcc.CENTROSERVIZIO = Cserv
                            MyAddAcc.CodiceOspite = CodOsp
                            MyAddAcc.Descrizione = Tdesc
                            MyAddAcc.ANNOCOMPETENZA = Anno
                            MyAddAcc.MESECOMPETENZA = Mese
                            If Modulo.MathRound(VettoreImporto(X) - ImpPresente, 2) > 0 Then
                                MyAddAcc.TipoMov = "AD"
                            Else
                                MyAddAcc.TipoMov = "AC"
                            End If

                            MyAddAcc.CodiceIva = CampoParametri("NCDeposito")
                            Dim TRegione As New ClsOspite

                            TRegione.CodiceOspite = CodOsp
                            TRegione.Leggi(STRINGACONNESSIONEDB, CodOsp)


                            Dim TipoOperazione As New Cls_TipoOperazione

                            TipoOperazione.Codice = TRegione.TipoOperazione
                            TipoOperazione.Leggi(STRINGACONNESSIONEDB, TipoOperazione.Codice)

                            If TipoOperazione.TipoAddebitoAccredito <> "" Then
                                MyAddAcc.CodiceIva = TipoOperazione.TipoAddebitoAccredito
                            End If

                            MyAddAcc.RETTA = "S"
                            MyAddAcc.IMPORTO = Math.Abs(Modulo.MathRound(VettoreImporto(X) - ImpPresente, 2))
                            MyAddAcc.PROVINCIA = ""
                            MyAddAcc.COMUNE = ""
                            MyAddAcc.REGIONE = ""
                            MyAddAcc.PARENTE = 0
                            MyAddAcc.RIFERIMENTO = "O"
                            MyAddAcc.Quantita = VettoreQuantita(X)
                            MyAddAcc.Data = Format(DataMov, "dd/MM/yyyy")
                            MyAddAcc.Progressivo = ProgressivoADDACC()
                            MyAddAcc.Utente = "EmissioneAddebitiAccrediti"

                            MyAddAcc.InserisciAddebito(STRINGACONNESSIONEDB)

                            StringaDegliErrori = StringaDegliErrori & "Periodo " & Mese & "/" & Anno & "  Creato Movimento numero : " & MyAddAcc.Progressivo & vbNewLine

                            RicalcoloGriglia_NumeroMovimento(IndiceVariazione) = MyAddAcc.Progressivo
                        End If
                    End If

                    If VettoreTipo(X) = "C" Then
                        IndiceVariazione = IndiceVariazione + 1

                        RicalcoloGriglia_CServ(IndiceVariazione) = Cserv
                        RicalcoloGriglia_CodiceOspite(IndiceVariazione) = CodOsp
                        RicalcoloGriglia_Anno(IndiceVariazione) = Anno
                        RicalcoloGriglia_Mese(IndiceVariazione) = Mese
                        RicalcoloGriglia_Ospite(IndiceVariazione) = 0
                        RicalcoloGriglia_Parente_id(IndiceVariazione) = 0
                        RicalcoloGriglia_Parente(IndiceVariazione) = 0
                        RicalcoloGriglia_Comune(IndiceVariazione) = Modulo.MathRound(VettoreImporto(X) - ImpPresente, 2)
                        RicalcoloGriglia_Jolly(IndiceVariazione) = 0
                        RicalcoloGriglia_Regione(IndiceVariazione) = 0
                        RicalcoloGriglia_Descrizione(IndiceVariazione) = ""


                        If CreoAddAccr = 1 Then
                            Dim MyAddAcc As New Cls_AddebitiAccrediti


                            MyAddAcc.CENTROSERVIZIO = Cserv
                            MyAddAcc.CodiceOspite = CodOsp
                            MyAddAcc.Descrizione = Tdesc
                            MyAddAcc.ANNOCOMPETENZA = Anno
                            MyAddAcc.MESECOMPETENZA = Mese
                            If Modulo.MathRound(VettoreImporto(X) - ImpPresente, 2) > 0 Then
                                MyAddAcc.TipoMov = "AD"
                            Else
                                MyAddAcc.TipoMov = "AC"
                            End If

                            MyAddAcc.CodiceIva = CampoParametri("NCDeposito")
                            Dim TRegione As New ClsComune

                            TRegione.Provincia = Mid(Comune & Space(10), 1, 3)
                            TRegione.Comune = Mid(Comune & Space(10), 4, 3)
                            TRegione.Leggi(STRINGACONNESSIONEDB)


                            Dim TipoOperazione As New Cls_TipoOperazione

                            TipoOperazione.Codice = TRegione.TipoOperazione
                            TipoOperazione.Leggi(STRINGACONNESSIONEDB, TipoOperazione.Codice)

                            If TipoOperazione.TipoAddebitoAccredito <> "" Then
                                MyAddAcc.CodiceIva = TipoOperazione.TipoAddebitoAccredito
                            End If

                            MyAddAcc.RETTA = "S"
                            MyAddAcc.IMPORTO = Math.Abs(Modulo.MathRound(VettoreImporto(X) - ImpPresente, 2))
                            MyAddAcc.PROVINCIA = Mid(Comune & Space(10), 1, 3)
                            MyAddAcc.COMUNE = Mid(Comune & Space(10), 4, 3)
                            MyAddAcc.REGIONE = ""
                            MyAddAcc.PARENTE = 0
                            MyAddAcc.RIFERIMENTO = "C"
                            MyAddAcc.Data = Format(DataMov, "dd/MM/yyyy")
                            MyAddAcc.Progressivo = ProgressivoADDACC()
                            MyAddAcc.Utente = "EmissioneAddebitiAccrediti"
                            MyAddAcc.Quantita = VettoreQuantita(X)
                            MyAddAcc.InserisciAddebito(STRINGACONNESSIONEDB)

                            StringaDegliErrori = StringaDegliErrori & "Periodo " & Mese & "/" & Anno & "  Creato Movimento numero : " & MyAddAcc.Progressivo & vbNewLine

                            RicalcoloGriglia_NumeroMovimento(IndiceVariazione) = MyAddAcc.Progressivo
                        End If
                    End If
                    If VettoreTipo(X) = "R" Then
                        IndiceVariazione = IndiceVariazione + 1

                        RicalcoloGriglia_CServ(IndiceVariazione) = Cserv
                        RicalcoloGriglia_CodiceOspite(IndiceVariazione) = CodOsp
                        RicalcoloGriglia_Anno(IndiceVariazione) = Anno
                        RicalcoloGriglia_Mese(IndiceVariazione) = Mese
                        RicalcoloGriglia_Ospite(IndiceVariazione) = 0
                        RicalcoloGriglia_Parente_id(IndiceVariazione) = 0
                        RicalcoloGriglia_Parente(IndiceVariazione) = 0
                        RicalcoloGriglia_Comune(IndiceVariazione) = 0
                        RicalcoloGriglia_Jolly(IndiceVariazione) = 0
                        RicalcoloGriglia_Regione(IndiceVariazione) = Modulo.MathRound(VettoreImporto(X) - ImpPresente, 2)
                        RicalcoloGriglia_Descrizione(IndiceVariazione) = ""

                        If CreoAddAccr = 1 Then
                            Dim MyAddAcc As New Cls_AddebitiAccrediti


                            MyAddAcc.CENTROSERVIZIO = Cserv
                            MyAddAcc.CodiceOspite = CodOsp
                            MyAddAcc.Descrizione = Tdesc
                            MyAddAcc.ANNOCOMPETENZA = Anno
                            MyAddAcc.MESECOMPETENZA = Mese
                            If Modulo.MathRound(VettoreImporto(X) - ImpPresente, 2) > 0 Then
                                MyAddAcc.TipoMov = "AD"
                            Else
                                MyAddAcc.TipoMov = "AC"
                            End If

                            MyAddAcc.CodiceIva = CampoParametri("NCDeposito")
                            Dim TRegione As New ClsUSL

                            TRegione.CodiceRegione = Regione
                            TRegione.Leggi(STRINGACONNESSIONEDB)


                            Dim TipoOperazione As New Cls_TipoOperazione

                            TipoOperazione.Codice = TRegione.TipoOperazione
                            TipoOperazione.Leggi(STRINGACONNESSIONEDB, TipoOperazione.Codice)

                            If TipoOperazione.TipoAddebitoAccredito <> "" Then
                                MyAddAcc.CodiceIva = TipoOperazione.TipoAddebitoAccredito
                            End If

                            MyAddAcc.RETTA = "S"
                            MyAddAcc.IMPORTO = Math.Abs(Modulo.MathRound(VettoreImporto(X) - ImpPresente, 2))
                            MyAddAcc.PROVINCIA = ""
                            MyAddAcc.COMUNE = ""
                            MyAddAcc.REGIONE = Regione
                            MyAddAcc.PARENTE = 0
                            MyAddAcc.RIFERIMENTO = "R"
                            MyAddAcc.Data = Format(DataMov, "dd/MM/yyyy")
                            MyAddAcc.Progressivo = ProgressivoADDACC()
                            MyAddAcc.Utente = "EmissioneAddebitiAccrediti"
                            MyAddAcc.Quantita = VettoreQuantita(X)
                            MyAddAcc.InserisciAddebito(STRINGACONNESSIONEDB)

                            StringaDegliErrori = StringaDegliErrori & "Periodo " & Mese & "/" & Anno & "  Creato Movimento numero : " & MyAddAcc.Progressivo & vbNewLine

                            RicalcoloGriglia_NumeroMovimento(IndiceVariazione) = MyAddAcc.Progressivo
                        End If
                    End If
                End If
            Next
        End If

    End Sub

    Public Sub PresenzeAssenzeDiurno(ByVal Cserv As String, ByVal CodOsp As Long, ByVal Mese As Integer, ByVal Anno As Integer)
        Dim Rs_AssenzeCentroDiurno As New ADODB.Recordset
        Dim Sql As String, SETTIMANA As String
        Dim I As Integer, Gg As Integer
        Dim YOldCau As String = "", OldCau As String, X As Integer, Y As Integer
        Dim ContaAC As Integer


        Dim LO_CodiceOspite As New ClsOspite

        LO_CodiceOspite.Leggi(STRINGACONNESSIONEDB, CodOsp)


        Dim KCs As New Cls_DatiOspiteParenteCentroServizio

        KCs.CentroServizio = Cserv
        KCs.CodiceOspite = CodOsp
        KCs.CodiceParente = 0
        KCs.Leggi(STRINGACONNESSIONEDB)

        REM Assegna la tipologia operazione e l'aliquota prendendo quella relativa al cserv
        If KCs.CodiceOspite <> 0 Then
            LO_CodiceOspite.TIPOOPERAZIONE = KCs.TipoOperazione
            LO_CodiceOspite.CODICEIVA = KCs.AliquotaIva
            LO_CodiceOspite.FattAnticipata = KCs.Anticipata
            LO_CodiceOspite.MODALITAPAGAMENTO = KCs.ModalitaPagamento
            LO_CodiceOspite.Compensazione = KCs.Compensazione
            LO_CodiceOspite.SETTIMANA = KCs.Settimana
        End If

        Sql = "Select * From AssenzeCentroDiurno Where CENTROSERVIZIO = '" & Cserv & "' And CODICEOSPITE = " & CodOsp & " And Anno = " & Anno & " And Mese = " & Mese


        Dim cmd As New OleDbCommand()
        cmd.CommandText = Sql
        cmd.Connection = OspitiCon

        Dim ReaderAssenzeCentroDiurno As OleDbDataReader = cmd.ExecuteReader()




        If ReaderAssenzeCentroDiurno.Read Then
            OldCau = ""
            X = 1
            Y = 1

            Dim Diurno As New Cls_Diurno
            Dim MeseAppoggio As Integer = 0
            Dim AnnoAppoggio As Integer = 0

            AnnoAppoggio = Anno
            MeseAppoggio = Mese


            If Mese > 1 Then
                MeseAppoggio = MeseAppoggio - 1
            Else
                AnnoAppoggio = AnnoAppoggio - 1
                MeseAppoggio = 12
            End If


            If Mese > 1 Then
                MeseAppoggio = MeseAppoggio - 1
            Else
                AnnoAppoggio = AnnoAppoggio - 1
                MeseAppoggio = 12
            End If
            Diurno.Leggi(STRINGACONNESSIONEDB, CodOsp, Cserv, AnnoAppoggio, MeseAppoggio)
            MyTabCausale(1) = Diurno.Giorno1
            MyTabCausale(2) = Diurno.Giorno2
            MyTabCausale(3) = Diurno.Giorno3
            MyTabCausale(4) = Diurno.Giorno4
            MyTabCausale(5) = Diurno.Giorno5
            MyTabCausale(6) = Diurno.Giorno6
            MyTabCausale(7) = Diurno.Giorno7
            MyTabCausale(8) = Diurno.Giorno8
            MyTabCausale(9) = Diurno.Giorno9
            MyTabCausale(10) = Diurno.Giorno10
            MyTabCausale(11) = Diurno.Giorno11
            MyTabCausale(12) = Diurno.Giorno12
            MyTabCausale(13) = Diurno.Giorno13
            MyTabCausale(14) = Diurno.Giorno14
            MyTabCausale(15) = Diurno.Giorno15
            MyTabCausale(16) = Diurno.Giorno16
            MyTabCausale(17) = Diurno.Giorno17
            MyTabCausale(18) = Diurno.Giorno18
            MyTabCausale(19) = Diurno.Giorno19
            MyTabCausale(20) = Diurno.Giorno20
            MyTabCausale(21) = Diurno.Giorno21
            MyTabCausale(22) = Diurno.Giorno22
            MyTabCausale(23) = Diurno.Giorno23
            MyTabCausale(24) = Diurno.Giorno24
            MyTabCausale(25) = Diurno.Giorno25
            MyTabCausale(26) = Diurno.Giorno26
            MyTabCausale(27) = Diurno.Giorno27
            MyTabCausale(28) = Diurno.Giorno28
            MyTabCausale(29) = Diurno.Giorno29
            MyTabCausale(30) = Diurno.Giorno30
            MyTabCausale(31) = Diurno.Giorno31
            ContaAC = 0
            For I = 1 To GiorniMese(MeseAppoggio, AnnoAppoggio)
                If MyTabCausale(I) = "C" Or MyTabCausale(I) = "A" Then
                    ContaAC = ContaAC + 1
                Else
                    ContaAC = 0
                End If
                If MyTabCausale(I) <> "C" And I <= GiorniMese(MeseAppoggio, AnnoAppoggio) Then
                    If YOldCau = MyTabCausale(I) Then
                        Y = Y + 1
                    Else
                        Y = 1
                        Y = Y + 1
                        YOldCau = MyTabCausale(I)
                    End If
                End If
                If OldCau = MyTabCausale(I) Then
                    X = X + 1
                Else
                    X = 1
                    X = X + 1
                    OldCau = MyTabCausale(I)
                End If
            Next I

            AnnoAppoggio = Anno
            MeseAppoggio = Mese

            If Mese > 1 Then
                MeseAppoggio = MeseAppoggio - 1
            Else
                AnnoAppoggio = AnnoAppoggio - 1
                MeseAppoggio = 12
            End If
            Diurno.Leggi(STRINGACONNESSIONEDB, CodOsp, Cserv, AnnoAppoggio, MeseAppoggio)
            MyTabCausale(1) = Diurno.Giorno1
            MyTabCausale(2) = Diurno.Giorno2
            MyTabCausale(3) = Diurno.Giorno3
            MyTabCausale(4) = Diurno.Giorno4
            MyTabCausale(5) = Diurno.Giorno5
            MyTabCausale(6) = Diurno.Giorno6
            MyTabCausale(7) = Diurno.Giorno7
            MyTabCausale(8) = Diurno.Giorno8
            MyTabCausale(9) = Diurno.Giorno9
            MyTabCausale(10) = Diurno.Giorno10
            MyTabCausale(11) = Diurno.Giorno11
            MyTabCausale(12) = Diurno.Giorno12
            MyTabCausale(13) = Diurno.Giorno13
            MyTabCausale(14) = Diurno.Giorno14
            MyTabCausale(15) = Diurno.Giorno15
            MyTabCausale(16) = Diurno.Giorno16
            MyTabCausale(17) = Diurno.Giorno17
            MyTabCausale(18) = Diurno.Giorno18
            MyTabCausale(19) = Diurno.Giorno19
            MyTabCausale(20) = Diurno.Giorno20
            MyTabCausale(21) = Diurno.Giorno21
            MyTabCausale(22) = Diurno.Giorno22
            MyTabCausale(23) = Diurno.Giorno23
            MyTabCausale(24) = Diurno.Giorno24
            MyTabCausale(25) = Diurno.Giorno25
            MyTabCausale(26) = Diurno.Giorno26
            MyTabCausale(27) = Diurno.Giorno27
            MyTabCausale(28) = Diurno.Giorno28
            MyTabCausale(29) = Diurno.Giorno29
            MyTabCausale(30) = Diurno.Giorno30
            MyTabCausale(31) = Diurno.Giorno31


            For I = 1 To GiorniMese(MeseAppoggio, AnnoAppoggio)
                If MyTabCausale(I) = "C" Or MyTabCausale(I) = "A" Then
                    ContaAC = ContaAC + 1
                Else
                    ContaAC = 0
                End If

                If MyTabCausale(I) <> "C" And I <= GiorniMese(MeseAppoggio, AnnoAppoggio) Then
                    If YOldCau = MyTabCausale(I) Then
                        Y = Y + 1
                    Else
                        Y = 1
                        Y = Y + 1
                        YOldCau = MyTabCausale(I)
                    End If
                End If
                If OldCau = MyTabCausale(I) Then
                    X = X + 1
                Else
                    X = 1
                    X = X + 1
                    OldCau = MyTabCausale(I)
                End If
            Next I


            MyTabCausale(1) = campodb(ReaderAssenzeCentroDiurno.Item("GIORNO1"))
            MyTabCausale(2) = campodb(ReaderAssenzeCentroDiurno.Item("GIORNO2"))
            MyTabCausale(3) = campodb(ReaderAssenzeCentroDiurno.Item("GIORNO3"))
            MyTabCausale(4) = campodb(ReaderAssenzeCentroDiurno.Item("GIORNO4"))
            MyTabCausale(5) = campodb(ReaderAssenzeCentroDiurno.Item("GIORNO5"))
            MyTabCausale(6) = campodb(ReaderAssenzeCentroDiurno.Item("GIORNO6"))
            MyTabCausale(7) = campodb(ReaderAssenzeCentroDiurno.Item("GIORNO7"))
            MyTabCausale(8) = campodb(ReaderAssenzeCentroDiurno.Item("GIORNO8"))
            MyTabCausale(9) = campodb(ReaderAssenzeCentroDiurno.Item("GIORNO9"))
            MyTabCausale(10) = campodb(ReaderAssenzeCentroDiurno.Item("GIORNO10"))
            MyTabCausale(11) = campodb(ReaderAssenzeCentroDiurno.Item("GIORNO11"))
            MyTabCausale(12) = campodb(ReaderAssenzeCentroDiurno.Item("GIORNO12"))
            MyTabCausale(13) = campodb(ReaderAssenzeCentroDiurno.Item("GIORNO13"))
            MyTabCausale(14) = campodb(ReaderAssenzeCentroDiurno.Item("GIORNO14"))
            MyTabCausale(15) = campodb(ReaderAssenzeCentroDiurno.Item("GIORNO15"))
            MyTabCausale(16) = campodb(ReaderAssenzeCentroDiurno.Item("GIORNO16"))
            MyTabCausale(17) = campodb(ReaderAssenzeCentroDiurno.Item("GIORNO17"))
            MyTabCausale(18) = campodb(ReaderAssenzeCentroDiurno.Item("GIORNO18"))
            MyTabCausale(19) = campodb(ReaderAssenzeCentroDiurno.Item("GIORNO19"))
            MyTabCausale(20) = campodb(ReaderAssenzeCentroDiurno.Item("GIORNO20"))
            MyTabCausale(21) = campodb(ReaderAssenzeCentroDiurno.Item("GIORNO21"))
            MyTabCausale(22) = campodb(ReaderAssenzeCentroDiurno.Item("GIORNO22"))
            MyTabCausale(23) = campodb(ReaderAssenzeCentroDiurno.Item("GIORNO23"))
            MyTabCausale(24) = campodb(ReaderAssenzeCentroDiurno.Item("GIORNO24"))
            MyTabCausale(25) = campodb(ReaderAssenzeCentroDiurno.Item("GIORNO25"))
            MyTabCausale(26) = campodb(ReaderAssenzeCentroDiurno.Item("GIORNO26"))
            MyTabCausale(27) = campodb(ReaderAssenzeCentroDiurno.Item("GIORNO27"))
            MyTabCausale(28) = campodb(ReaderAssenzeCentroDiurno.Item("GIORNO28"))
            MyTabCausale(29) = campodb(ReaderAssenzeCentroDiurno.Item("GIORNO29"))
            MyTabCausale(30) = campodb(ReaderAssenzeCentroDiurno.Item("GIORNO30"))
            MyTabCausale(31) = campodb(ReaderAssenzeCentroDiurno.Item("GIORNO31"))

            For I = 1 To 31
                If MyTabCausale(I) = "C" Or MyTabCausale(I) = "A" Then
                    ContaAC = ContaAC + 1
                Else
                    ContaAC = 0
                End If

                MyTabGiorniRipetizioneCausaleACSum(I) = ContaAC

                If Trim(MyTabCausale(I)) <> "" Then
                    MyTabGiornoAssenza(I) = MyTabGiornoAssenza(I - 1) + 1
                Else
                    MyTabGiornoAssenza(I) = 0
                End If

                If MyTabCausale(I) <> "C" And I <= GiorniMese(Mese, Anno) Then
                    If YOldCau = MyTabCausale(I) Then
                        MyTabGiorniRipetizioneCausaleNC(I) = Y
                        Y = Y + 1
                    Else
                        Y = 1
                        MyTabGiorniRipetizioneCausaleNC(I) = Y
                        Y = Y + 1
                        YOldCau = MyTabCausale(I)
                    End If
                End If
                If OldCau = MyTabCausale(I) Then
                    MyTabGiorniRipetizioneCausale(I) = X
                    X = X + 1
                Else
                    X = 1
                    MyTabGiorniRipetizioneCausale(I) = X
                    X = X + 1
                    OldCau = MyTabCausale(I)
                End If
            Next I
        Else
            SETTIMANA = LO_CodiceOspite.SETTIMANA
            For I = 1 To GiorniMese(Mese, Anno)
                Gg = Weekday(DateSerial(Anno, Mese, I), vbMonday)
                If Trim(Mid(SETTIMANA, Gg, 1)) <> "" Then
                    MyTabCausale(I) = Mid(SETTIMANA, Gg, 1)
                End If
            Next I

            Dim Cp As New Cls_CentroServizio

            Cp.Leggi(STRINGACONNESSIONEDB, Cserv)
            SETTIMANA = Cp.SETTIMANA

            For I = 1 To GiorniMese(Mese, Anno)
                Gg = Weekday(DateSerial(Anno, Mese, I), vbMonday)
                If Trim(Mid(SETTIMANA, Gg, 1)) <> "" Then
                    If Mid(SETTIMANA, Gg, 1) = "A" Then
                        MyTabCausale(I) = "C"
                    End If
                End If
            Next I


            If Mese = 1 Then
                If CampoCentroServizio(Cserv, "Giorno0101") <> "S" Then
                    MyTabCausale(1) = "C"
                End If
                If CampoCentroServizio(Cserv, "Giorno0601") <> "S" Then
                    MyTabCausale(6) = "C"
                End If
            End If
            If Mese = 3 Then
                If CampoCentroServizio(Cserv, "Giorno1903") <> "S" Then
                    MyTabCausale(19) = "C"
                End If
            End If
            If Mese = 4 Then
                If CampoCentroServizio(Cserv, "Giorno2504") <> "S" Then
                    MyTabCausale(25) = "C"
                End If
            End If
            If Mese = 5 Then
                If CampoCentroServizio(Cserv, "Giorno0105") <> "S" Then
                    MyTabCausale(1) = "C"
                End If
            End If

            If Mese = 6 Then
                If CampoCentroServizio(Cserv, "Giorno0206") <> "S" Then
                    MyTabCausale(2) = "C"
                End If
                If CampoCentroServizio(Cserv, "Giorno2906") <> "S" Then
                    MyTabCausale(29) = "C"
                End If
            End If
            If Mese = 8 Then
                If CampoCentroServizio(Cserv, "Giorno1508") <> "S" Then
                    MyTabCausale(15) = "C"
                End If
            End If
            If Mese = 11 Then
                If CampoCentroServizio(Cserv, "Giorno0111") <> "S" Then
                    MyTabCausale(1) = "C"
                End If
                If CampoCentroServizio(Cserv, "Giorno0411") <> "S" Then
                    MyTabCausale(4) = "C"
                End If
            End If
            If Mese = 12 Then
                If CampoCentroServizio(Cserv, "Giorno0812") <> "S" Then
                    MyTabCausale(8) = "C"
                End If
                If CampoCentroServizio(Cserv, "Giorno2512") <> "S" Then
                    MyTabCausale(25) = "C"
                End If
                If CampoCentroServizio(Cserv, "Giorno2612") <> "S" Then
                    MyTabCausale(26) = "C"
                End If
            End If
            If CampoCentroServizio(Cserv, "GIORNO1ATTIVO") <> "S" Then
                If Not IsDBNull(CampoCentroServizio(Cserv, "GIORNO1")) Then
                    If Val(Mid(CampoCentroServizio(Cserv, "GIORNO1"), 4, 2)) = Mese Then
                        MyTabCausale(Val(Mid(CampoCentroServizio(Cserv, "GIORNO1"), 1, 2))) = "C"
                    End If
                End If
            End If

            If CampoCentroServizio(Cserv, "GIORNO2ATTIVO") <> "S" Then
                If Not IsDBNull(CampoCentroServizio(Cserv, "GIORNO2")) Then
                    If Val(Mid(CampoCentroServizio(Cserv, "GIORNO2"), 4, 2)) = Mese Then
                        MyTabCausale(Val(Mid(CampoCentroServizio(Cserv, "GIORNO2"), 1, 2))) = "C"
                    End If
                End If
            End If

            If CampoCentroServizio(Cserv, "GIORNO3ATTIVO") <> "S" Then
                If Not IsDBNull(CampoCentroServizio(Cserv, "GIORNO3")) Then
                    If Val(Mid(CampoCentroServizio(Cserv, "GIORNO3"), 4, 2)) = Mese Then
                        MyTabCausale(Val(Mid(CampoCentroServizio(Cserv, "GIORNO3"), 1, 2))) = "C"
                    End If
                End If
            End If

            If CampoCentroServizio(Cserv, "GIORNO4ATTIVO") <> "S" Then
                If Not IsDBNull(CampoCentroServizio(Cserv, "GIORNO4")) Then
                    If Val(Mid(CampoCentroServizio(Cserv, "GIORNO4"), 4, 2)) = Mese Then
                        MyTabCausale(Val(Mid(CampoCentroServizio(Cserv, "GIORNO4"), 1, 2))) = "C"
                    End If
                End If
            End If

            If CampoCentroServizio(Cserv, "GIORNO5ATTIVO") <> "S" Then
                If Not IsDBNull(CampoCentroServizio(Cserv, "GIORNO5")) Then
                    If Val(Mid(CampoCentroServizio(Cserv, "GIORNO5"), 4, 2)) = Mese Then
                        MyTabCausale(Val(Mid(CampoCentroServizio(Cserv, "GIORNO5"), 1, 2))) = "C"
                    End If
                End If
            End If
        End If
        ReaderAssenzeCentroDiurno.Close()
    End Sub


    Public Sub CreaTabellaPresenze(ByVal Cserv As String, ByVal CodOsp As Long, ByVal Mese As Integer, ByVal Anno As Integer)
        Dim Rs_Movimenti As New ADODB.Recordset
        Dim Rs_MYMOV As New ADODB.Recordset
        Dim MyDataini As Date, MyDatafine As Date
        Dim MySql As String, WValore As String = ""
        Dim Uscito As Boolean
        Dim GGUSCITA As String, GGENTRATA As String
        Dim NGiorniAssenza As Integer, GiornoUscita As Integer, I As Integer


        Dim Myserv As New Cls_CentroServizio


        Myserv.CENTROSERVIZIO = Cserv
        Myserv.Leggi(STRINGACONNESSIONEDB, Myserv.CENTROSERVIZIO)

        If Myserv.TIPOCENTROSERVIZIO = "" And Myserv.Regole.ToUpper.IndexOf("Rem Considera Chiusura Settimanale".ToUpper) >= 0 Then
            Dim SETTIMANA As String
            Dim LO_CodiceOspite As New Cls_DatiOspiteParenteCentroServizio
            Dim Gg As Integer

            LO_CodiceOspite.CodiceOspite = CodOsp
            LO_CodiceOspite.CentroServizio = Cserv
            LO_CodiceOspite.CodiceParente = 0
            LO_CodiceOspite.Leggi(STRINGACONNESSIONEDB)


            SETTIMANA = LO_CodiceOspite.Settimana
            For I = 1 To GiorniMese(Mese, Anno)
                Gg = Weekday(DateSerial(Anno, Mese, I), vbMonday)
                If Trim(Mid(SETTIMANA, Gg, 1)) <> "" Then
                    MyTabCausale(I) = Mid(SETTIMANA, Gg, 1)
                End If
            Next I
        End If

        Accolto = False
        Dimesso = False
        GGUSCITA = CampoParametriGIORNOUSCITA
        GGENTRATA = CampoParametriGIORNOENTRATA
        If GGENTRATA = "P" Then
            GiornoRientro = 1
        Else
            GiornoRientro = 0
        End If


        MyDataini = DateSerial(Anno, Mese, 1)

        MyDatafine = DateSerial(Anno, Mese, GiorniMese(Mese, Anno))



        MySql = "Select Top 1 * From MOVIMENTI Where CENTROSERVIZIO = '" & Cserv & "' and CODICEOSPITE = " & CodOsp & " And "


        REM MySql = MySql & " Data <= {ts '" & Format(MyDataini, "yyyy-MM-dd") & " 00:00:00'}  Order by Data Desc,PROGRESSIVO DESC"

        MySql = MySql & " Data < {ts '" & Format(MyDataini, "yyyy-MM-dd") & " 00:00:00'}  Order by Data Desc,PROGRESSIVO DESC"


        Dim cmd As New OleDbCommand()
        cmd.CommandText = MySql
        cmd.Connection = OspitiCon

        Dim ReaderMovimenti As OleDbDataReader = cmd.ExecuteReader()

        If ReaderMovimenti.Read() Then            
            If campodb(ReaderMovimenti.Item("TipoMov")) = "03" Then
                Uscito = True
                WValore = campodb(ReaderMovimenti.Item("Causale"))
                'DATAINIZIO = MyDataini
                'Rs_MYMOV.Open "Select Top 1 * From MOVIMENTI Where CENTROSERVIZIO = '" & Cserv & "' and CODICEOSPITE = " & CodOsp & " And Data <= {ts '" & Format(MyDataini, "yyyy-MM-dd") & " 00:00:00'} ORDER BY DATA DESC ", OspitiDb, ADODB.CursorTypeEnum.adOpenKeyset, ADODB.LockTypeEnum.adLockOptimistic
                'If Not Rs_MYMOV.EOF Then
                '   DATAINIZIO = Rs_MYMOV.Fields("Data
                'End If
                'Rs_MYMOV.Close
                If GGUSCITA = "P" Then
                    NGiorniAssenza = DateDiff("d", campodbd(ReaderMovimenti.Item("Data")), MyDataini)
                Else
                    NGiorniAssenza = DateDiff("d", campodbd(ReaderMovimenti.Item("Data")), MyDataini) + 1
                End If

            End If
            If campodb(ReaderMovimenti.Item("TipoMov")) = "04" Or campodb(ReaderMovimenti.Item("TipoMov")) = "05" Then
                If campodb(ReaderMovimenti.Item("TipoMov")) = "05" Then
                    Accolto = True
                End If
                Uscito = False
                NGiorniAssenza = 0
            End If
        End If
        ReaderMovimenti.Close()


        MySql = "Select * From MOVIMENTI Where CENTROSERVIZIO = '" & Cserv & "' and CODICEOSPITE = " & CodOsp & " And "
        MySql = MySql & " Data >= {ts '" & Format(MyDataini, "yyyy-MM-dd") & " 00:00:00'} and Data <= {ts '" & Format(MyDatafine, "yyyy-MM-dd") & " 00:00:00'} Order by Data ,PROGRESSIVO"


        Dim cmd1 As New OleDbCommand()
        cmd1.CommandText = MySql
        cmd1.Connection = OspitiCon

        Dim ReaderMovimenti1 As OleDbDataReader = cmd1.ExecuteReader()



        GiornoUscita = 1
        Do While ReaderMovimenti1.Read
            If campodb(ReaderMovimenti1.Item("TipoMov")) = "03" Then
                If GGUSCITA = "P" Then
                    GiornoUscita = Day(DateSerial(Year(campodbd(ReaderMovimenti1.Item("Data"))), Month(campodbd(ReaderMovimenti1.Item("Data"))), Day(campodbd(ReaderMovimenti1.Item("Data"))) + 1))
                    If Month(DateSerial(Year(campodbd(ReaderMovimenti1.Item("Data"))), Month(campodbd(ReaderMovimenti1.Item("Data"))), Day(campodbd(ReaderMovimenti1.Item("Data"))) + 1)) <> Month(campodbd(ReaderMovimenti1.Item("Data"))) Then
                        GiornoUscita = 0
                    Else
                        WValore = campodb(ReaderMovimenti1.Item("Causale"))
                        Uscito = True
                        NGiorniAssenza = 1
                    End If
                Else
                    GiornoUscita = Day(campodbd(ReaderMovimenti1.Item("Data")))
                    WValore = campodb(ReaderMovimenti1.Item("Causale"))
                    Uscito = True
                    NGiorniAssenza = 1
                End If
            End If
            If campodb(ReaderMovimenti1.Item("TipoMov")) = "13" Then
                If Myserv.TIPOCENTROSERVIZIO = "A" Then

                Else
                    If Uscito = True Then
                        For I = GiornoUscita To Day(campodbd(ReaderMovimenti1.Item("Data"))) - GiornoRientro
                            MyTabCausale(I) = WValore
                            MyTabGiornoAssenza(I) = NGiorniAssenza
                            NGiorniAssenza = NGiorniAssenza + 1
                        Next I
                    End If
                    If GGUSCITA = "P" Then
                        GiornoUscita = Day(DateSerial(Year(campodb(ReaderMovimenti1.Item("Data"))), Month(campodb(ReaderMovimenti1.Item("Data"))), Day(campodb(ReaderMovimenti1.Item("Data"))) + 1))
                    Else
                        GiornoUscita = Day(campodb(ReaderMovimenti1.Item("Data")))
                    End If
                    If (Myserv.TIPOCENTROSERVIZIO = "D" Or Myserv.TIPOCENTROSERVIZIO = "A") And MyTabCausale(Day(campodbd(ReaderMovimenti1.Item("Data")))) <> "" Then

                    Else
                        MyTabCausale(Day(campodbd(ReaderMovimenti1.Item("Data")))) = campodb(ReaderMovimenti1.Item("Causale"))
                    End If

                    MyTabGiornoAssenza(Day(campodbd(ReaderMovimenti1.Item("Data")))) = NGiorniAssenza
                    GiornoUscita = Day(campodbd(ReaderMovimenti1.Item("Data"))) + 1
                    WValore = "*"
                    Uscito = True
                    Dimesso = True
                    NGiorniAssenza = 2
                End If
            End If

            If campodb(ReaderMovimenti1.Item("TipoMov")) = "04" Then
                For I = GiornoUscita To Day(campodbd(ReaderMovimenti1.Item("Data"))) - GiornoRientro
                    MyTabCausale(I) = WValore
                    MyTabGiornoAssenza(I) = NGiorniAssenza
                    NGiorniAssenza = NGiorniAssenza + 1
                Next I
                Uscito = False
            End If

            If campodb(ReaderMovimenti1.Item("TipoMov")) = "05" Then
                For I = GiornoUscita To Day(campodbd(ReaderMovimenti1.Item("Data"))) - 1
                    MyTabCausale(I) = "*"
                    MyTabGiornoAssenza(I) = 0
                Next I
                If (Myserv.TIPOCENTROSERVIZIO = "D" Or Myserv.TIPOCENTROSERVIZIO = "A") And campodb(ReaderMovimenti1.Item("Causale")) = "" Then

                Else
                    If Trim(campodb(ReaderMovimenti1.Item("Causale"))) = "" Then
                        MyTabCausale(Day(campodbd(ReaderMovimenti1.Item("Data")))) = CampoParametriCAUSALEACCOGLIMENTO
                    Else
                        MyTabCausale(Day(campodbd(ReaderMovimenti1.Item("Data")))) = Trim(campodb(ReaderMovimenti1.Item("Causale")))
                    End If
                End If
                Uscito = False
                Accolto = True
            End If

        Loop
        If Uscito Then
            For I = GiornoUscita To Day(MyDatafine)
                MyTabCausale(I) = WValore
                MyTabGiornoAssenza(I) = NGiorniAssenza
                NGiorniAssenza = NGiorniAssenza + 1
            Next I
        End If

        For I = 27 To 31
            If Day(DateSerial(Anno, Mese, I)) <> I Then
                MyTabCausale(I) = "*"
            End If
        Next I

        ReaderMovimenti1.Close()
    End Sub

    Public Function TrovaJolly(ByVal Jolly As String) As Integer
        Dim I As Integer

        If IsDBNull(Jolly) Or Trim(Jolly) = "" Then TrovaJolly = 0 : Exit Function
        I = 0
        Do
            I = I + 1
            If I = 11 Then Exit Do
        Loop While CodiceJolly(I) <> Jolly
        TrovaJolly = I
    End Function


    Public Function TrovaComune(ByVal Comune As String) As Integer
        Dim I As Integer

        If IsDBNull(Comune) Or Trim(Comune) = "" Then TrovaComune = 0 : Exit Function
        I = 0
        Do
            I = I + 1
            If I = 11 Then Exit Do
        Loop While CodiceComune(I) <> Comune
        TrovaComune = I
    End Function

    Public Sub InserisciJolly(ByVal Jolly As String)
        Dim I As Integer

        If IsDBNull(Jolly) Or Trim(Jolly) = "" Then Exit Sub
        I = 0
        Do
            I = I + 1
        Loop While CodiceJolly(I) <> Jolly And Trim(CodiceJolly(I)) <> ""
        If CodiceJolly(I) = "" Then
            CodiceJolly(I) = Jolly
        End If
    End Sub

    Public Sub InserisciComune(ByVal Comune As String)
        Dim I As Integer

        If IsDBNull(Comune) Or Trim(Comune) = "" Then Exit Sub
        I = 0
        Do
            I = I + 1
        Loop While CodiceComune(I) <> Comune And Trim(CodiceComune(I)) <> ""
        If CodiceComune(I) = "" Then
            CodiceComune(I) = Comune
        End If
    End Sub

    Public Function TrovaRegione(ByVal Regione As String, ByVal MyTipoRetta As String) As Integer
        Dim I As Integer
        I = 0
        Do
            I = I + 1
            If I > 10 Then TrovaRegione = 11 : Exit Do
        Loop Until CodiceRegione(I) = Regione And (MyTipoRetta = TipoRetta(I) Or MyTipoRetta = "")
        TrovaRegione = I
    End Function
    Public Sub InserisciRegione(ByVal Regione As String, ByVal MyTipoRetta As String)
        Dim I As Integer
        I = 0
        Do
            I = I + 1
        Loop While (CodiceRegione(I) <> Regione Or TipoRetta(I) <> MyTipoRetta) And CodiceRegione(I) <> ""
        If CodiceRegione(I) = "" Then
            CodiceRegione(I) = Regione
            TipoRetta(I) = MyTipoRetta
        End If
    End Sub

    Public Sub EliminaDatiRette(ByVal Mese As Integer, ByVal Anno As Integer, ByVal CentroServizio As String, ByVal CodiceOspite As Long)
        Dim MySql As String

        MySql = "Delete  From RetteOspite Where (Manuale Is Null OR Manuale = '')  And Mese = " & Mese & " And Anno = " & Anno
        If CentroServizio <> "" Then
            MySql = MySql & " And CentroServizio = '" & CentroServizio & "'"
        End If
        If CodiceOspite <> 0 Then
            MySql = MySql & " And CodiceOspite = " & CodiceOspite
        End If

        Dim cmdOspite As New OleDbCommand()
        cmdOspite.CommandText = MySql
        cmdOspite.Connection = OspitiCon
        cmdOspite.ExecuteNonQuery()




        MySql = "Delete  From RetteParente Where (Manuale Is Null OR Manuale = '') And Mese = " & Mese & " And Anno = " & Anno

        If CentroServizio <> "" Then
            MySql = MySql & " And CentroServizio = '" & CentroServizio & "'"
        End If
        If CodiceOspite <> 0 Then
            MySql = MySql & " And CodiceOspite = " & CodiceOspite
        End If
        OspitiDb.Execute(MySql)

        Dim cmdParente As New OleDbCommand()
        cmdParente.CommandText = MySql
        cmdParente.Connection = OspitiCon
        cmdParente.ExecuteNonQuery()

        MySql = "Delete  From RetteComune Where (Manuale Is Null OR Manuale = '')  And Mese = " & Mese & " And Anno = " & Anno

        If CentroServizio <> "" Then
            MySql = MySql & " And CentroServizio = '" & CentroServizio & "'"
        End If
        If CodiceOspite <> 0 Then
            MySql = MySql & " And CodiceOspite = " & CodiceOspite
        End If
        'OspitiDb.Execute(MySql)

        Dim cmdOspiteRC As New OleDbCommand()
        cmdOspiteRC.CommandText = MySql
        cmdOspiteRC.Connection = OspitiCon
        cmdOspiteRC.ExecuteNonQuery()




        MySql = "Delete  From RetteJolly Where (Manuale Is Null OR Manuale = '')  And Mese = " & Mese & " And Anno = " & Anno

        If CentroServizio <> "" Then
            MySql = MySql & " And CentroServizio = '" & CentroServizio & "'"
        End If
        If CodiceOspite <> 0 Then
            MySql = MySql & " And CodiceOspite = " & CodiceOspite
        End If
        'OspitiDb.Execute(MySql)

        Dim cmdOspiteRJ As New OleDbCommand()
        cmdOspiteRJ.CommandText = MySql
        cmdOspiteRJ.Connection = OspitiCon
        cmdOspiteRJ.ExecuteNonQuery()


        MySql = "Delete  From RetteRegione Where (Manuale Is Null OR Manuale = '')  And Mese = " & Mese & " And Anno = " & Anno

        If CentroServizio <> "" Then
            MySql = MySql & " And CentroServizio = '" & CentroServizio & "'"
        End If
        If CodiceOspite <> 0 Then
            MySql = MySql & " And CodiceOspite = " & CodiceOspite
        End If
        Dim cmdOspiteRR As New OleDbCommand()
        cmdOspiteRR.CommandText = MySql
        cmdOspiteRR.Connection = OspitiCon
        cmdOspiteRR.ExecuteNonQuery()


        MySql = "Delete  From RetteEnte Where (Manuale Is Null OR Manuale = '')  And Mese = " & Mese & " And Anno = " & Anno

        If CentroServizio <> "" Then
            MySql = MySql & " And CentroServizio = '" & CentroServizio & "'"
        End If
        If CodiceOspite <> 0 Then
            MySql = MySql & " And CodiceOspite = " & CodiceOspite
        End If
        Dim cmdOspiteRE As New OleDbCommand()
        cmdOspiteRE.CommandText = MySql
        cmdOspiteRE.Connection = OspitiCon
        cmdOspiteRE.ExecuteNonQuery()

    End Sub
    Public Sub AllineaImportiMensili(ByVal Cserv As String, ByVal CodOsp As Long, ByVal Mese As Integer, ByVal Anno As Integer)
        Dim GGMESE As Byte, TotaleRetta As Double, g As Integer, I As Integer, Differenza As String, Percentuale(10), Par As Long
        Dim Impdocumenti As Double
        Dim TotImpMenPar As Long
        Dim ImportoExtraO As Double
        Dim ImportoExtraP As Double
        Dim ImportoExtraC As Double
        Dim UltimoStato As String, xGG As Integer, mypercentuale As Double
        Dim LeggiRegistrazionemese As New ADODB.Recordset
        TotImpMenPar = 0
        ImportoExtraO = 0
        ImportoExtraP = 0
        ImportoExtraC = 0
        Dim NumeroRegistrazione As Long
        Dim Condizione As String = ""
        Dim Parametri As New Cls_Parametri
        Dim MyRs As New ADODB.Recordset

        Differenza = ""
        For I = 1 To 31
            If MyTabModalita(I) <> "" Then
                Differenza = MyTabModalita(I)
            End If
        Next I


        Parametri.LeggiParametri(STRINGACONNESSIONEDB)

        Dim DatiOspite As New Cls_ImportoOspite

        DatiOspite.CodiceOspite = CodOsp
        DatiOspite.CentroServizio = Cserv
        DatiOspite.UltimaData(STRINGACONNESSIONEDB, DatiOspite.CODICEOSPITE, DatiOspite.CENTROSERVIZIO)

        NumeroRegistrazione = 0
        'Impdocumenti = 0
        'Condizione = " MastroPartita = " & MastroOspite(Cserv, CodOsp) & " and ContoPartita = " & ContoOspite(Cserv, CodOsp) & " And SottoContoPartita = " & SottoContoOspite(Cserv, CodOsp) & _
        '             " And CentroServizio = '" & Cserv & "' And PeriodoRiferimentoRettaMese = " & Mese & " And PeriodoRiferimentoRettaAnno  =" & Anno

        'LeggiRegistrazionemese.Open("SELECT MovimentiContabiliTesta.NumeroRegistrazione FROM MovimentiContabiliTesta INNER JOIN MovimentiContabiliRiga ON MovimentiContabiliTesta.NumeroRegistrazione = MovimentiContabiliRiga.Numero Where " & Condizione & " Group by MovimentiContabiliTesta.NumeroRegistrazione", GeneraleDb, ADODB.CursorTypeEnum.adOpenKeyset, ADODB.LockTypeEnum.adLockOptimistic)
        'Do While Not LeggiRegistrazionemese.EOF
        '    NumeroRegistrazione = movefromdb(LeggiRegistrazionemese.Fields(0))
        '    MyRs.Open("Select sum(Importo) From MovimentiContabiliRiga Where Numero =" & NumeroRegistrazione & " And MastroPartita = " & CampoParametri("MastroAnticipo"), GeneraleDb, ADODB.CursorTypeEnum.adOpenKeyset, ADODB.LockTypeEnum.adLockOptimistic)
        '    Impdocumenti = Impdocumenti + MoveFromDb(MyRs.Fields(0))
        '    MyRs.Close()
        '    LeggiRegistrazionemese.MoveNext()
        'Loop
        'LeggiRegistrazionemese.Close()

        If NumeroRegistrazione <> 0 Then
            ImportoMensileOspite = Impdocumenti
            If ImportoPresOspite > 0 Then
                If Differenza = "C" Then
                    If ImportoPresComune(1) > 0 Then
                        ImportoPresComune(1) = ImportoPresComune(1) - (ImportoMensileOspite - ImportoPresOspite - ImportoAssOspite) + MyTabAddebitoOspite(0) + MyTabAddebitoOspite(1) + MyTabAddebitoOspite(2) + MyTabAddebitoOspite(3) + MyTabAddebitoOspite(4) + MyTabAddebitoOspite(5) + MyTabAddebitoOspite(6) + MyTabAddebitoOspite(7) + MyTabAddebitoOspite(8) + MyTabAddebitoOspite(9) _
                                                                                                                                    - MyTabAccreditoOspite(0) - MyTabAccreditoOspite(1) - MyTabAccreditoOspite(2) - MyTabAccreditoOspite(3) - MyTabAccreditoOspite(4) - MyTabAccreditoOspite(5) - MyTabAccreditoOspite(6) - MyTabAccreditoOspite(7) - MyTabAccreditoOspite(8) - MyTabAccreditoOspite(9)
                    Else
                        If ImpExtraComune(1, 0) > 0 Then
                            ImpExtraComune(1, 0) = ImpExtraComune(1, 0) - (ImportoMensileOspite - ImportoPresOspite - ImportoAssOspite)
                        End If
                    End If
                End If
                If Differenza = "E" Then
                    ImportoPresEnte = ImportoPresEnte - (ImportoMensileOspite - ImportoPresOspite - ImportoAssOspite)
                End If
                If Differenza = "P" Then
                    For I = 1 To 10
                        If ImportoPresParente(I) <> 0 Then
                            ImportoPresParente(I) = Modulo.MathRound(ImportoPresParente(I) - _
                              ((ImportoMensileOspite - ImportoPresOspite - ImportoAssOspite) * Percentuale(I)), 2)
                            Exit For
                        End If
                    Next I
                End If
                ImportoPresOspite = ImportoMensileOspite
                ImportoAssOspite = 0
            End If
            If NonImportoPresOspite > 0 Then
                If Differenza = "C" Then
                    If NonImportoPresComune(1) > 0 Then
                        NonImportoPresComune(1) = NonImportoPresComune(1) - (ImportoMensileOspite - NonImportoPresOspite - NonImportoAssOspite) + MyTabAddebitoOspite(0) + MyTabAddebitoOspite(1) + MyTabAddebitoOspite(2) + MyTabAddebitoOspite(3) + MyTabAddebitoOspite(4) + MyTabAddebitoOspite(5) + MyTabAddebitoOspite(6) + MyTabAddebitoOspite(7) + MyTabAddebitoOspite(8) + MyTabAddebitoOspite(9) _
                                                                                                                                                - MyTabAccreditoOspite(0) - MyTabAccreditoOspite(1) - MyTabAccreditoOspite(2) - MyTabAccreditoOspite(3) - MyTabAccreditoOspite(4) - MyTabAccreditoOspite(5) - MyTabAccreditoOspite(6) - MyTabAccreditoOspite(7) - MyTabAccreditoOspite(8) - MyTabAccreditoOspite(9)
                    Else
                        If ImpExtraComune(1, 0) > 0 Then
                            ImpExtraComune(1, 0) = ImpExtraComune(1, 0) - (ImportoMensileOspite - NonImportoPresOspite - NonImportoAssOspite)
                        End If

                    End If
                End If
                If Differenza = "E" Then
                    NonImportoPresEnte = NonImportoPresEnte - (ImportoMensileOspite - NonImportoPresOspite - NonImportoAssOspite)
                End If
                If Differenza = "P" Then
                    For I = 1 To 10
                        If NonImportoPresParente(I) <> 0 Then
                            NonImportoPresParente(I) = Modulo.MathRound(NonImportoPresParente(I) - _
                             ((ImportoMensileOspite - NonImportoPresOspite - NonImportoAssOspite) * Percentuale(I)), 2)
                            Exit For
                        End If
                    Next I
                End If
                NonImportoPresOspite = ImportoMensileOspite
                NonImportoAssOspite = 0
            End If
            ImportoMensileOspite = 0
        End If

        For I = 0 To 10
            TotImpMenPar = TotImpMenPar + ImportoMensileParente(I)
            ImportoExtraO = ImportoExtraO + MensileImportoExtrOspite(I)
            ImportoExtraP = ImportoExtraP + MensileImportoExtrParent(I)
            ImportoExtraC = ImportoExtraC + MensileImportoExtrComune(I)

            ImportoExtraC = ImportoExtraC + ImportoExtrJolly(I)
        Next I


        If CodOsp = 7 Then
            CodOsp = 7
        End If

        If ImportoMensileRetta = 0 And ImportoMensileOspite = 0 And ImportoMensileComune = 0 And TotImpMenPar = 0 And ImportoExtraO = 0 And ImportoExtraP = 0 And ImportoExtraC = 0 And ImportoMensileJolly = 0 Then
            Exit Sub
        End If

        GGMESE = GiorniMese(Mese, Anno)

        UltimoStato = ""
        For I = 1 To 31
            If MyTabAutoSufficente(I) <> "" Then UltimoStato = MyTabAutoSufficente(I) : Exit For
        Next I
        For I = 0 To 10

            'Or (ImportoExtrOspite(I) > NonImpExtraOspite(I)
            'Or (NonImpExtraOspite(I) > ImpExtraOspite(I)

            If (ImpExtraOspite(I) > NonImpExtraOspite(I)) And MensileImportoExtrOspite(I) > 0 Then
                ImpExtraOspite(I) = MensileImportoExtrOspite(I)
                NonImpExtraOspite(I) = 0
            End If
            If (NonImpExtraOspite(I) > ImpExtraOspite(I)) And MensileImportoExtrOspite(I) > 0 Then
                NonImpExtraOspite(I) = MensileImportoExtrOspite(I)
                ImpExtraOspite(I) = 0
            End If

            Percentuale(I) = 0
            For Par = 0 To 10
                mypercentuale = 0
                For xGG = 1 To 31
                    If MyTabPercentuale(xGG, Par) > 0 Then
                        mypercentuale = MyTabPercentuale(xGG, Par)
                        Exit For
                    End If
                Next xGG

                

                If ImpExtraParente(Par, I) > NonImpExtraParente(Par, I) And mypercentuale > 0 And MensileImportoExtrParent(I) > 0 Then
                    ImpExtraParente(Par, I) = Modulo.MathRound(MensileImportoExtrParent(I) * mypercentuale / 100, 2)
                    NonImpExtraParente(Par, I) = 0
                End If
                If ImpExtraParente(Par, I) < NonImpExtraParente(Par, I) And mypercentuale > 0 And MensileImportoExtrParent(I) > 0 Then
                    NonImpExtraParente(Par, I) = Modulo.MathRound(MensileImportoExtrParent(I) * mypercentuale / 100, 2)
                    ImpExtraParente(Par, I) = MensileImportoExtrParent(I)
                End If

                If ImpExtraComune(Par, I) > NonImpExtraComune(Par, I) And MensileImportoExtrComune(I) > 0 Then
                    ImpExtraComune(Par, I) = MensileImportoExtrComune(I)
                    NonImpExtraComune(Par, I) = 0
                End If
                If ImpExtraComune(Par, I) < NonGiorniPresComune(Par) > 0 And MensileImportoExtrComune(I) > 0 Then
                    NonImpExtraComune(Par, I) = MensileImportoExtrComune(I)
                    ImpExtraComune(Par, I) = 0
                End If


                If ImportoExtrJolly(I) > 0 And GiorniPresJolly(Par) > 0 Then
                    ImpExtraJolly(Par, I) = ImportoExtrJolly(I)
                End If
                If ImportoExtrJolly(I) > 0 And NonGiorniPresJolly(Par) > 0 Then
                    NonImpExtraJolly(Par, I) = ImportoExtrJolly(I)
                End If

            Next
        Next


        For g = 1 To 31
            For I = 1 To 10
                If MyTabPercentuale(g, I) <> 0 Then
                    Percentuale(I) = MyTabPercentuale(g, I)
                End If
            Next I
        Next g


        If ImportoMensileOspite > 0 Then
            If (GiorniAss = 0 And NonGiorniAss = 0 And NonGiorniPres = 0 And GiorniPres = GGMESE) Or (DatiOspite.MensileFisso = 1 And NonGiorniAss = 0 And NonGiorniPres = 0) Then
                If Differenza = "C" And Parametri.NonAllineareMensileAdifferenza = 0 Then
                    If ImportoPresComune(1) > 0 Then
                        ImportoPresComune(1) = ImportoPresComune(1) - (ImportoMensileOspite - ImportoPresOspite)
                    Else
                        If ImpExtraComune(1, 0) > 0 Then
                            ImpExtraComune(1, 0) = ImpExtraComune(1, 0) - (ImportoMensileOspite - ImportoPresOspite)
                        End If
                    End If
                    If Prm_AzzeraSeNegativo = 1 Then
                        If ImportoPresComune(1) < 0 Then ImportoPresComune(1) = 0
                    End If
                End If
                If Differenza = "E" Then
                    ImportoPresEnte = ImportoPresEnte - (ImportoMensileOspite - ImportoPresOspite)
                    If Prm_AzzeraSeNegativo = 1 Then
                        If ImportoPresEnte < 0 Then ImportoPresEnte = 0
                    End If
                End If
                If Differenza = "P" And Parametri.NonAllineareMensileAdifferenza = 0 Then
                    For I = 1 To 10
                        If ImportoPresParente(I) <> 0 Then
                            ImportoPresParente(I) = Modulo.MathRound(ImportoPresParente(I) - _
                              ((ImportoMensileOspite - ImportoPresOspite) * Percentuale(I)), 2)
                            Exit For
                        End If
                        If Prm_AzzeraSeNegativo = 1 Then
                            If ImportoPresParente(I) < 0 Then
                                ImportoPresParente(I) = 0
                            End If
                        End If
                    Next I
                End If
                ImportoPresOspite = ImportoMensileOspite            
            End If
            If (GiorniAss = 0 And NonGiorniAss = 0 And GiorniPres = 0 And NonGiorniPres = GGMESE) Or (DatiOspite.MensileFisso = 1 And GiorniAss = 0 And GiorniPres = 0) Then
                If Differenza = "C" And Parametri.NonAllineareMensileAdifferenza = 0 Then
                    If NonImportoPresComune(1) > 0 Then
                        NonImportoPresComune(1) = NonImportoPresComune(1) - (ImportoMensileOspite - NonImportoPresOspite)
                    Else
                        If ImpExtraComune(1, 0) > 0 Then
                            ImpExtraComune(1, 0) = ImpExtraComune(1, 0) - (ImportoMensileOspite - NonImportoPresOspite)
                        End If
                    End If

                    If NonImportoPresComune(0) > 0 Then
                        NonImportoPresComune(0) = NonImportoPresComune(0) - (ImportoMensileOspite - NonImportoPresOspite)
                    Else
                        If ImpExtraComune(0, 0) > 0 Then
                            ImpExtraComune(0, 0) = ImpExtraComune(1, 0) - (ImportoMensileOspite - NonImportoPresOspite)
                        End If
                    End If

                    If Prm_AzzeraSeNegativo = 1 Then
                        If NonImportoPresComune(1) < 0 Then
                            NonImportoPresComune(1) = 0
                        End If
                    End If
                End If
                If Differenza = "E" Then
                    NonImportoPresEnte = NonImportoPresEnte - (ImportoMensileOspite - NonImportoPresOspite)
                    If Prm_AzzeraSeNegativo = 1 Then
                        If NonImportoPresEnte < 0 Then NonImportoPresEnte = 0
                    End If
                End If
                If Differenza = "P" And Parametri.NonAllineareMensileAdifferenza = 0 Then
                    For I = 1 To 10
                        If NonImportoPresParente(I) <> 0 Then
                            NonImportoPresParente(I) = Modulo.MathRound(NonImportoPresParente(I) - _
                             ((ImportoMensileOspite - NonImportoPresOspite) * Percentuale(I)), 2)
                            Exit For
                        End If
                        If Prm_AzzeraSeNegativo = 1 Then
                            If NonImportoPresParente(I) < 0 Then NonImportoPresParente(I) = 0
                        End If
                    Next I
                End If
                NonImportoPresOspite = ImportoMensileOspite
            End If
        End If

        Dim DatiComune As New Cls_ImportoComune

        DatiComune.CODICEOSPITE = CodOsp
        DatiComune.CENTROSERVIZIO = Cserv
        DatiComune.UltimaData(STRINGACONNESSIONEDB, DatiComune.CODICEOSPITE, DatiComune.CENTROSERVIZIO)

        If ImportoMensileComune > 0 Then
            If (ImportoPresComune(1) <> 0 And GiorniPresComune(1) = GGMESE) Or (DatiComune.MensileFisso = 1 And ImportoPresComune(1) <> 0) Then
                If Differenza = "O" And Parametri.NonAllineareMensileAdifferenza = 0 Then
                    ImportoPresOspite = ImportoPresOspite - (ImportoMensileComune - ImportoPresComune(1)) - (ImportoMensileJolly - ImportoPresJolly(1))
                    If ImportoPresOspite < 0 Then
                        ImportoPresOspite = 0
                    End If

                    If Prm_AzzeraSeNegativo = 1 Then
                        If ImportoPresOspite < 0 Then
                            ImportoPresOspite = 0
                        End If
                    End If
                End If
                If Differenza = "E" Then
                    ImportoPresEnte = ImportoPresEnte - (ImportoMensileComune - ImportoPresComune(1)) - (ImportoMensileJolly - ImportoPresJolly(1))

                    If Prm_AzzeraSeNegativo = 1 Then
                        If ImportoPresEnte < 0 Then
                            ImportoPresEnte = 0
                        End If
                    End If
                End If
                If Differenza = "P" And Parametri.NonAllineareMensileAdifferenza = 0 Then
                    For I = 1 To 10
                        If ImportoPresParente(I) <> 0 Then
                            ImportoPresParente(I) = Modulo.MathRound(ImportoPresParente(I) - _
                              (((ImportoMensileComune - ImportoPresComune(1)) - (ImportoMensileJolly - ImportoPresJolly(1))) * Percentuale(I)), 2)
                            Exit For
                        End If
                        If Prm_AzzeraSeNegativo = 1 Then
                            If ImportoPresParente(I) < 0 Then
                                ImportoPresParente(I) = 0
                            End If
                        End If
                    Next I
                End If
                ImportoPresComune(1) = ImportoMensileComune

                ImportoPresJolly(1) = ImportoMensileJolly

                If Prm_AzzeraSeNegativo = 1 Then
                    If ImportoPresComune(1) < 0 Then ImportoPresComune(1) = 0
                    If ImportoPresJolly(1) < 0 Then ImportoPresJolly(1) = 0
                End If
            End If
            If NonImportoPresComune(1) <> 0 And NonGiorniPresComune(1) = GGMESE Or (DatiComune.MensileFisso = 1 And NonImportoPresComune(1) <> 0) Then
                If Differenza = "O" And Parametri.NonAllineareMensileAdifferenza = 0 Then
                    ImportoPresOspite = ImportoPresOspite - (ImportoMensileComune - NonImportoPresComune(1))
                    If ImportoPresOspite < 0 Then
                        ImportoPresOspite = 0
                    End If
                    If Prm_AzzeraSeNegativo = 1 Then
                        If ImportoPresOspite < 0 Then ImportoPresOspite = 0
                    End If
                End If
                If Differenza = "E" Then
                    NonImportoPresEnte = NonImportoPresEnte - (ImportoMensileComune - NonImportoPresComune(1))
                    If Prm_AzzeraSeNegativo = 1 Then
                        If NonImportoPresEnte < 0 Then NonImportoPresEnte = 0
                    End If
                End If
                If Differenza = "P" And Parametri.NonAllineareMensileAdifferenza = 0 Then
                    For I = 1 To 10
                        
                        If ImportoPresParente(I) <> 0 Then
                            ImportoPresParente(I) = Modulo.MathRound(ImportoPresParente(I) - _
                             ((ImportoMensileComune - NonImportoPresComune(1)) * Percentuale(I)), 2)
                            Exit For
                        End If
                        If Prm_AzzeraSeNegativo = 1 Then
                            If ImportoPresParente(I) < 0 Then
                                ImportoPresParente(I) = 0
                            End If
                        End If
                    Next I
                End If
                NonImportoPresComune(1) = ImportoMensileComune
            End If
        End If


        If ImportoMensileJolly > 0 Then
            If ImportoPresJolly(1) <> 0 And GiorniPresJolly(1) = GGMESE Then
                If Differenza = "O" And Parametri.NonAllineareMensileAdifferenza = 0 Then
                    ImportoPresOspite = ImportoPresOspite - (ImportoMensileComune - ImportoPresComune(1)) - (ImportoMensileJolly - ImportoPresJolly(1))
                    If ImportoPresOspite < 0 Then
                        ImportoPresOspite = 0
                    End If

                    If Prm_AzzeraSeNegativo = 1 Then
                        If ImportoPresOspite < 0 Then
                            ImportoPresOspite = 0
                        End If
                    End If
                End If
                If Differenza = "E" Then
                    ImportoPresEnte = ImportoPresEnte - (ImportoMensileComune - ImportoPresComune(1)) - (ImportoMensileJolly - ImportoPresJolly(1))

                    If Prm_AzzeraSeNegativo = 1 Then
                        If ImportoPresEnte < 0 Then
                            ImportoPresEnte = 0
                        End If
                    End If
                End If
                If Differenza = "P" And Parametri.NonAllineareMensileAdifferenza = 0 Then
                    For I = 1 To 10
                        If ImportoPresParente(I) <> 0 Then
                            ImportoPresParente(I) = Modulo.MathRound(ImportoPresParente(I) - _
                              (((ImportoMensileComune - ImportoPresComune(1)) - (ImportoMensileJolly - ImportoPresJolly(1))) * Percentuale(I)), 2)
                            Exit For
                        End If
                        If Prm_AzzeraSeNegativo = 1 Then
                            If ImportoPresParente(I) < 0 Then
                                ImportoPresParente(I) = 0
                            End If
                        End If
                    Next I
                End If
                'ImportoPresComune(1) = ImportoMensileComune

                ImportoPresJolly(1) = ImportoMensileJolly

                If Prm_AzzeraSeNegativo = 1 Then
                    If ImportoPresComune(1) < 0 Then ImportoPresComune(1) = 0
                    If ImportoPresJolly(1) < 0 Then ImportoPresJolly(1) = 0
                End If
            End If
            If NonImportoPresJolly(1) <> 0 And NonGiorniPresJolly(1) = GGMESE Then
                If Differenza = "O" And Parametri.NonAllineareMensileAdifferenza = 0 Then
                    ImportoPresOspite = ImportoPresOspite - (ImportoMensileJolly - NonImportoPresJolly(1))
                    If ImportoPresOspite < 0 Then
                        ImportoPresOspite = 0
                    End If
                    If Prm_AzzeraSeNegativo = 1 Then
                        If ImportoPresOspite < 0 Then ImportoPresOspite = 0
                    End If
                End If
                If Differenza = "E" Then
                    ImportoPresEnte = ImportoPresEnte - (ImportoMensileJolly - NonImportoPresJolly(1))
                    If Prm_AzzeraSeNegativo = 1 Then
                        If ImportoPresEnte < 0 Then ImportoPresEnte = 0
                    End If
                End If
                If Differenza = "P" And Parametri.NonAllineareMensileAdifferenza = 0 Then
                    For I = 1 To 10
                        If ImportoPresParente(I) <> 0 Then
                            ImportoPresParente(I) = Modulo.MathRound(ImportoPresParente(I) - _
                             ((ImportoMensileJolly - NonImportoPresJolly(1)) * Percentuale(I)), 2)
                            Exit For
                        End If
                        If Prm_AzzeraSeNegativo = 1 Then
                            If ImportoPresParente(I) < 0 Then
                                ImportoPresParente(I) = 0
                            End If
                        End If
                    Next I
                End If
                NonImportoPresJolly(1) = ImportoMensileJolly
            End If
        End If


        For I = 1 To 10

            Dim KCps As New Cls_ImportoParente

            KCps.CENTROSERVIZIO = Cserv
            KCps.CODICEOSPITE = CodOsp
            KCps.CODICEPARENTE = I
            KCps.UltimaData(STRINGACONNESSIONEDB, KCps.CODICEOSPITE, KCps.CENTROSERVIZIO, KCps.CODICEPARENTE)


            If ImportoMensileParente(I) > 0 Then
                If (ImportoPresParente(I) <> 0 And GiorniPresParente(I) = GGMESE) Or (ImportoPresParente(I) <> 0 And KCps.MensileFisso = 1) Then
                    If Differenza = "O" And Parametri.NonAllineareMensileAdifferenza = 0 Then
                        ImportoPresOspite = ImportoPresOspite - (ImportoMensileParente(I) - ImportoPresParente(I))
                        If ImportoPresOspite < 0 Then
                            ImportoPresOspite = 0
                        End If
                        If Prm_AzzeraSeNegativo = 1 Then
                            If ImportoPresOspite < 0 Then
                                ImportoPresOspite = 0
                            End If
                        End If
                    End If
                    If Differenza = "C" And Parametri.NonAllineareMensileAdifferenza = 0 Then
                        ImportoPresComune(1) = ImportoPresComune(1) - (ImportoMensileParente(I) - ImportoPresParente(I))
                        If Prm_AzzeraSeNegativo = 1 Then
                            If ImportoPresComune(1) < 0 Then
                                ImportoPresComune(1) = 0
                            End If
                        End If
                    End If
                    If Differenza = "E" Then
                        ImportoPresEnte = ImportoPresEnte - (ImportoMensileParente(I) - ImportoPresParente(I))
                    End If
                    ImportoPresParente(I) = ImportoMensileParente(I)
                    ImportoPresParente2(I) = ImportoMensileParente2(I)
                    If Prm_AzzeraSeNegativo = 1 Then
                        If ImportoPresParente(I) < 0 Then ImportoPresParente(I) = 0
                    End If
                End If
                If (NonImportoPresParente(I) <> 0 And NonGiorniPresParente(I) = GGMESE) Or (NonImportoPresParente(I) <> 0 And KCps.MensileFisso = 1) Then
                    If Differenza = "O" And Parametri.NonAllineareMensileAdifferenza = 0 Then
                        NonImportoPresOspite = NonImportoPresOspite - (ImportoMensileParente(I) - NonImportoPresParente(I))
                        If NonImportoPresOspite < 0 Then
                            NonImportoPresOspite = 0
                        End If
                        If Prm_AzzeraSeNegativo = 1 Then
                            If NonImportoPresOspite < 0 Then
                                NonImportoPresOspite = 0
                            End If
                        End If
                    End If
                    If Differenza = "C" And Parametri.NonAllineareMensileAdifferenza = 0 Then

                        NonImportoPresComune(1) = NonImportoPresComune(1) - (ImportoMensileParente(I) - NonImportoPresParente(I))
                        If NonImportoPresComune(1) < 0 Then
                            NonImportoPresComune(1) = 0
                        End If
                        If Prm_AzzeraSeNegativo = 1 Then
                            If NonImportoPresComune(1) < 0 Then
                                NonImportoPresComune(1) = 0
                            End If
                        End If
                    End If
                    If Differenza = "E" Then
                        NonImportoPresEnte = NonImportoPresEnte - (ImportoMensileParente(I) - NonImportoPresParente(I))
                        If Prm_AzzeraSeNegativo = 1 Then
                            If NonImportoPresEnte < 0 Then
                                NonImportoPresEnte = 0
                            End If
                        End If
                    End If
                    NonImportoPresParente(I) = ImportoMensileParente(I)
                    NonImportoPresParente2(I) = ImportoMensileParente2(I)
                End If
            End If
        Next I

        If ImportoMensileRetta = 0 Then Exit Sub
        TotaleRetta = 0
        For I = 1 To 10
            TotaleRetta = TotaleRetta + ImportoPresParente(I) + _
                                        ImportoAssParente(I) + _
                                        ImportoPresComune(I) + _
                                        ImportoAssComune(I) + _
                                        ImportoPresRegione(I) + _
                                        ImportoAssRegione(I)

            TotaleRetta = TotaleRetta + NonImportoPresParente(I) + _
                                        NonImportoAssParente(I) + _
                                        NonImportoPresComune(I) + _
                                        NonImportoAssComune(I) + _
                                        NonImportoPresRegione(I) + _
                                        NonImportoAssRegione(I)

        Next I
        TotaleRetta = TotaleRetta + NonImportoPresOspite + NonImportoAssOspite + NonImportoPresEnte + _
                                    NonImportoAssEnte + ImportoPresOspite + ImportoAssOspite + _
                                    ImportoPresEnte + ImportoAssEnte

        If Differenza = "O" And GiorniPres = GGMESE Then
            ImportoPresOspite = ImportoPresOspite + (ImportoMensileRetta - TotaleRetta)
        End If
        If Differenza = "O" And NonGiorniPres = GGMESE Then
            NonImportoPresOspite = NonImportoPresOspite + (ImportoMensileRetta - TotaleRetta)
        End If

        If Differenza = "P" Then
            Dim SoloParente As Integer
            Dim NumeroParentiPaganti As Integer = 0
            For SoloParente = 0 To 10
                If ImportoPresParente(SoloParente) > 0 Or NonImportoPresParente(SoloParente) > 0 Then
                    NumeroParentiPaganti = NumeroParentiPaganti + 1
                End If
            Next

            If NumeroParentiPaganti = 1 Then
                For SoloParente = 0 To 10
                    If GiorniPresParente(SoloParente) = GGMESE And ImportoPresParente(SoloParente) > 0 Then
                        ImportoPresParente(SoloParente) = ImportoPresParente(SoloParente) + (ImportoMensileRetta - TotaleRetta)
                    End If
                    If NonGiorniPresParente(SoloParente) = GGMESE And NonImportoPresParente(SoloParente) > 0 Then
                        NonImportoPresParente(SoloParente) = NonImportoPresParente(SoloParente) + (ImportoMensileRetta - TotaleRetta)
                    End If
                Next
            End If
        End If

        If Differenza = "C" And GiorniPresComune(1) = GGMESE Then
            ImportoPresComune(1) = ImportoPresComune(1) + (ImportoMensileRetta - TotaleRetta)
        End If
        If Differenza = "C" And NonGiorniPresComune(1) = GGMESE Then
            NonImportoPresComune(1) = NonImportoPresComune(1) + (ImportoMensileRetta - TotaleRetta)
        End If

        If Differenza = "E" And GiorniPresEnte = GGMESE Then
            ImportoPresEnte = ImportoPresEnte + (ImportoMensileRetta - TotaleRetta)
        End If
        If Differenza = "E" And NonGiorniPresEnte = GGMESE Then
            NonImportoPresEnte = NonImportoPresEnte + (ImportoMensileRetta - TotaleRetta)
        End If


    End Sub



    Public Function GiorniNelMese(ByVal Cserv As String, ByVal CodOsp As Long, ByVal Mese As Integer, ByVal Anno As Integer) As Long
        Dim MyMese(32) As String
        Dim RsMovimenti As New ADODB.Recordset
        Dim Presente As Boolean
        Dim I As Long

        Presente = False

        Dim cmdMovimenti As New OleDbCommand()
        cmdMovimenti.CommandText = "Select * From Movimenti Where CENTROSERVIZIO = '" & Cserv & "' And CODICEOSPITE = " & CodOsp & " And Data < {ts '" & Format(DateSerial(Anno, Mese, 1), "yyyy-MM-dd") & " 00:00:00'} Order By Data Desc"
        cmdMovimenti.Connection = OspitiCon
        Dim ReaderMovimenti As OleDbDataReader = cmdMovimenti.ExecuteReader()

        If ReaderMovimenti.Read Then
            If campodb(ReaderMovimenti.Item("TipoMov")) <> "13" Then
                Presente = True
            End If
        End If
        ReaderMovimenti.Close()

        If Presente = True Then
            For I = 1 To GiorniMese(Mese, Anno)
                MyMese(I) = "P"
            Next I
        Else
            For I = 1 To GiorniMese(Mese, Anno)
                MyMese(I) = "A"
            Next I
        End If

        Dim cmdMovimenti1 As New OleDbCommand()
        cmdMovimenti1.CommandText = "Select * From Movimenti Where CENTROSERVIZIO = '" & Cserv & "' And CODICEOSPITE = " & CodOsp & " And Data >= {ts '" & Format(DateSerial(Anno, Mese, 1), "yyyy-MM-dd") & " 00:00:00'} And Data <= {ts '" & Format(DateSerial(Anno, Mese, GiorniMese(Mese, Anno)), "yyyy-MM-dd") & " 00:00:00'} Order By Data"
        cmdMovimenti1.Connection = OspitiCon
        Dim ReaderMovimenti1 As OleDbDataReader = cmdMovimenti1.ExecuteReader()

        'RsMovimenti.Open(, OspitiDb, ADODB.CursorTypeEnum.adOpenForwardOnly, ADODB.LockTypeEnum.adLockOptimistic)
        Do While ReaderMovimenti1.Read
            If campodb(ReaderMovimenti1.Item("TipoMov")) = "05" Then
                For I = Day(campodbd(ReaderMovimenti1.Item("Data"))) To GiorniMese(Mese, Anno)
                    MyMese(I) = "P"
                Next I
            End If

            If campodb(ReaderMovimenti1.Item("TipoMov")) = "13" Then
                I = Day(campodb(ReaderMovimenti1.Item("Data")))
                Dim CausaleEu As New Cls_CausaliEntrataUscita


                CausaleEu.Codice = campodb(ReaderMovimenti1.Item("Causale"))
                CausaleEu.LeggiCausale(STRINGACONNESSIONEDB)


                If CausaleEu.GiornoUscita = "P" Then
                    MyMese(I) = "P"
                Else
                    If CausaleEu.TIPOMOVIMENTO = "P" Or CausaleEu.GiornoUscita = "A" Or CausaleEu.GiornoUscita = "" Then
                        Dim Regola As String
                        Regola = CausaleEu.Regole
                        If IsNothing(Regola) Then
                            Regola = ""
                        End If

                        If Regola <> "" Then
                            If Regola.IndexOf("SalvaOspite = SalvaOspite") >= 0 Then '????
                                MyMese(I) = "P"
                            Else
                                MyMese(I) = "A"
                            End If
                        Else
                            MyMese(I) = "P"
                        End If

                    Else
                        MyMese(I) = "P"
                    End If
                    If CausaleEu.TIPOMOVIMENTO = "N" Then
                        MyMese(I) = "A"
                    End If
                    End If
                For I = Day(campodb(ReaderMovimenti1.Item("Data"))) + 1 To GiorniMese(Mese, Anno)
                    MyMese(I) = "A"
                Next
            End If

        Loop
        ReaderMovimenti1.Close()
        GiorniNelMese = 0
        For I = 1 To GiorniMese(Mese, Anno)
            If MyMese(I) = "P" Then
                GiorniNelMese = GiorniNelMese + 1
            End If
        Next I
    End Function


    Public Function ContaPresenzeDiurno(ByVal Cserv As String, ByVal CodOsp As Long, ByVal Mese As Integer, ByVal Anno As Integer, ByVal XGiorni As Integer) As Long
        Dim Rs_AssenzeCentroDiurno As New ADODB.Recordset
        Dim Sql As String, SETTIMANA As String
        Dim I As Integer, Gg As Integer
        Dim YOldCau As String, OldCau As String, X As Integer, Y As Integer
        Dim Causale(40) As String




        Dim LO_CodiceOspite As New ClsOspite

        LO_CodiceOspite.Leggi(STRINGACONNESSIONEDB, CodOsp)


        Dim KCs As New Cls_DatiOspiteParenteCentroServizio

        KCs.CentroServizio = Cserv
        KCs.CodiceOspite = CodOsp
        KCs.CodiceParente = 0
        KCs.Leggi(STRINGACONNESSIONEDB)

        REM Assegna la tipologia operazione e l'aliquota prendendo quella relativa al cserv
        If KCs.CodiceOspite <> 0 Then
            LO_CodiceOspite.TIPOOPERAZIONE = KCs.TipoOperazione
            LO_CodiceOspite.CODICEIVA = KCs.AliquotaIva
            LO_CodiceOspite.FattAnticipata = KCs.Anticipata
            LO_CodiceOspite.MODALITAPAGAMENTO = KCs.ModalitaPagamento
            LO_CodiceOspite.Compensazione = KCs.Compensazione
            LO_CodiceOspite.SETTIMANA = KCs.Settimana
        End If

        Dim K As New Cls_Diurno

        K.Anno = 0
        K.LeggiMese(STRINGACONNESSIONEDB, CodOsp, Cserv, Anno, Mese)
        
       
        Causale(1) = K.Giorno1
        Causale(2) = K.Giorno2
        Causale(3) = K.Giorno3
        Causale(4) = K.Giorno4
        Causale(5) = K.Giorno5
        Causale(6) = K.Giorno6
        Causale(7) = K.Giorno7
        Causale(8) = K.Giorno8
        Causale(9) = K.Giorno9
        Causale(10) = K.Giorno10
        Causale(11) = K.Giorno11
        Causale(12) = K.Giorno12
        Causale(13) = K.Giorno13
        Causale(14) = K.Giorno14
        Causale(15) = K.Giorno15
        Causale(16) = K.Giorno16
        Causale(17) = K.Giorno17
        Causale(18) = K.Giorno18
        Causale(19) = K.Giorno19
        Causale(20) = K.Giorno20
        Causale(21) = K.Giorno21
        Causale(22) = K.Giorno22
        Causale(23) = K.Giorno23
        Causale(24) = K.Giorno24
        Causale(25) = K.Giorno25
        Causale(26) = K.Giorno26
        Causale(27) = K.Giorno27
        Causale(28) = K.Giorno28
        Causale(29) = K.Giorno29
        Causale(30) = K.Giorno30
        Causale(31) = K.Giorno31

        OldCau = ""
        X = 1
        Y = 1

        If K.Anno = 0 Then
            SETTIMANA = LO_CodiceOspite.SETTIMANA
            For I = 1 To GiorniMese(Mese, Anno)
                Gg = Weekday(DateSerial(Anno, Mese, I), vbMonday)
                If Trim(Mid(SETTIMANA, Gg, 1)) <> "" Then
                    Causale(I) = Mid(SETTIMANA, Gg, 1)
                End If
            Next I
        End If

        Dim Cp As New Cls_CentroServizio

        Cp.Leggi(STRINGACONNESSIONEDB, Cserv)
        SETTIMANA = Cp.SETTIMANA
        For I = 1 To GiorniMese(Mese, Anno)

            Gg = Weekday(DateSerial(Anno, Mese, I), vbMonday)
            If Trim(Mid(SETTIMANA, Gg, 1)) <> "" Then
                If Mid(SETTIMANA, Gg, 1) = "A" Then
                    Causale(I) = "C"
                End If
            End If
        Next I


        If Mese = 1 Then
            If CampoCentroServizio(Cserv, "Giorno0101") <> "S" Then
                Causale(1) = "C"
            End If
            If CampoCentroServizio(Cserv, "Giorno0601") <> "S" Then
                Causale(6) = "C"
            End If
        End If
        If Mese = 3 Then
            If CampoCentroServizio(Cserv, "Giorno1903") <> "S" Then
                Causale(19) = "C"
            End If
        End If
        If Mese = 4 Then
            If CampoCentroServizio(Cserv, "Giorno2504") <> "S" Then
                Causale(25) = "C"
            End If
        End If
        If Mese = 5 Then
            If CampoCentroServizio(Cserv, "Giorno0105") <> "S" Then
                Causale(1) = "C"
            End If
        End If

        If Mese = 6 Then
            If CampoCentroServizio(Cserv, "Giorno0206") <> "S" Then
                Causale(2) = "C"
            End If
            If CampoCentroServizio(Cserv, "Giorno2906") <> "S" Then
                Causale(29) = "C"
            End If
        End If
        If Mese = 8 Then
            If CampoCentroServizio(Cserv, "Giorno1508") <> "S" Then
                Causale(15) = "C"
            End If
        End If
        If Mese = 11 Then
            If CampoCentroServizio(Cserv, "Giorno0111") <> "S" Then
                Causale(1) = "C"
            End If
            If CampoCentroServizio(Cserv, "Giorno0411") <> "S" Then
                Causale(4) = "C"
            End If
        End If
        If Mese = 12 Then
            If CampoCentroServizio(Cserv, "Giorno0812") <> "S" Then
                Causale(8) = "C"
            End If
            If CampoCentroServizio(Cserv, "Giorno2512") <> "S" Then
                Causale(25) = "C"
            End If
            If CampoCentroServizio(Cserv, "Giorno2612") <> "S" Then
                Causale(26) = "C"
            End If
        End If
        If CampoCentroServizio(Cserv, "GIORNO1ATTIVO") <> "S" Then
            If Not IsDBNull(CampoCentroServizio(Cserv, "GIORNO1")) Then
                If Val(Mid(CampoCentroServizio(Cserv, "GIORNO1"), 4, 2)) = Mese Then
                    Causale(Val(Mid(CampoCentroServizio(Cserv, "GIORNO1"), 1, 2))) = "C"
                End If
            End If
        End If

        If CampoCentroServizio(Cserv, "GIORNO2ATTIVO") <> "S" Then
            If Not IsDBNull(CampoCentroServizio(Cserv, "GIORNO2")) Then
                If Val(Mid(CampoCentroServizio(Cserv, "GIORNO2"), 4, 2)) = Mese Then
                    Causale(Val(Mid(CampoCentroServizio(Cserv, "GIORNO2"), 1, 2))) = "C"
                End If
            End If
        End If

        If CampoCentroServizio(Cserv, "GIORNO3ATTIVO") <> "S" Then
            If Not IsDBNull(CampoCentroServizio(Cserv, "GIORNO3")) Then
                If Val(Mid(CampoCentroServizio(Cserv, "GIORNO3"), 4, 2)) = Mese Then
                    Causale(Val(Mid(CampoCentroServizio(Cserv, "GIORNO3"), 1, 2))) = "C"
                End If
            End If
        End If

        If CampoCentroServizio(Cserv, "GIORNO4ATTIVO") <> "S" Then
            If Not IsDBNull(CampoCentroServizio(Cserv, "GIORNO4")) Then
                If Val(Mid(CampoCentroServizio(Cserv, "GIORNO4"), 4, 2)) = Mese Then
                    Causale(Val(Mid(CampoCentroServizio(Cserv, "GIORNO4"), 1, 2))) = "C"
                End If
            End If
        End If

        If CampoCentroServizio(Cserv, "GIORNO5ATTIVO") <> "S" Then
            If Not IsDBNull(CampoCentroServizio(Cserv, "GIORNO5")) Then
                If Val(Mid(CampoCentroServizio(Cserv, "GIORNO5"), 4, 2)) = Mese Then
                    Causale(Val(Mid(CampoCentroServizio(Cserv, "GIORNO5"), 1, 2))) = "C"
                End If
            End If

        End If                

        ContaPresenzeDiurno = 0
        For I = 1 To XGiorni        

            If Causale(I) = "" Then
                ContaPresenzeDiurno = ContaPresenzeDiurno + 1
            Else
                Dim mc As New Cls_CausaliEntrataUscita

                mc.Codice = Causale(I)
                mc.LeggiCausale(STRINGACONNESSIONEDB)
                If mc.GiornoUscita = "P" Then
                    ContaPresenzeDiurno = ContaPresenzeDiurno + 1
                End If
            End If
        Next
    End Function



    Public Sub SalvaDati(ByVal Cserv As String, ByVal CodOsp As Long, ByVal Mese As Integer, ByVal Anno As Integer, Optional ByVal FattureNC As Boolean = False)
        Dim Par As Integer, Com As Integer, Reg As Integer
        Dim Rs_RettaOspite As New ADODB.Recordset
        Dim Rs_RettaParente As New ADODB.Recordset
        Dim Rs_RettaComune As New ADODB.Recordset
        Dim Rs_RettaRegione As New ADODB.Recordset
        Dim Rs_RettaEnte As New ADODB.Recordset
        Dim ClasseRette As New ClassRetteOspiti
        Dim MyRs As New ADODB.Recordset
        Dim ImportoRpx As Double, Differenze As Double, Calcola As Double, Calcola2P1 As Double
        Dim ImportoRpx2 As Double
        Dim ExtraAnticipati As Double
        Dim xMese As Long, xAnno As Long, I As Integer, GiornoCalcolati As Long
        Dim xMese1 As Long, xAnno1 As Long, xMese2 As Long, xAnno2 As Long
        Dim Ext As Integer
        Dim IntNonAuto As String, IntAuto As String
        Dim M2GiornoCalcolati As Long, M1GiornoCalcolati As Long, Giorni2P1 As Integer
        Dim DataAcco As Date
        Dim XINDICE As Long
        Dim ImportoExtra As Double

        Dim MScriviOspite As Boolean
        Dim MScriviParente As Boolean
        Dim MScriviComune As Boolean
        Dim MScriviRegione As Boolean
        Dim Calcola2 As Double





        ClasseRette.ApriDB(STRINGACONNESSIONEDB)

        MScriviOspite = True
        MScriviParente = True
        MScriviComune = True
        MScriviRegione = True


        Dim LO_CodiceOspite As New ClsOspite

        LO_CodiceOspite.Leggi(STRINGACONNESSIONEDB, CodOsp)


        Dim KCs As New Cls_DatiOspiteParenteCentroServizio

        KCs.CentroServizio = Cserv
        KCs.CodiceOspite = CodOsp
        KCs.CodiceParente = 0
        KCs.Leggi(STRINGACONNESSIONEDB)

        REM Assegna la tipologia operazione e l'aliquota prendendo quella relativa al cserv
        If KCs.CodiceOspite <> 0 Then
            LO_CodiceOspite.TIPOOPERAZIONE = KCs.TipoOperazione
            LO_CodiceOspite.CODICEIVA = KCs.AliquotaIva
            LO_CodiceOspite.FattAnticipata = KCs.Anticipata
            LO_CodiceOspite.MODALITAPAGAMENTO = KCs.ModalitaPagamento
            LO_CodiceOspite.Compensazione = KCs.Compensazione
            LO_CodiceOspite.SETTIMANA = KCs.Settimana

            If KCs.NonCalcoloComune = 1 Then
                MScriviComune = False
            End If
        End If



        Dim BloccaConguagli As Integer = 0

        Dim MioParetri As New Cls_Parametri

        MioParetri.LeggiParametri(STRINGACONNESSIONEDB)

        If BloccaConguaglioDaRegola = 1 Then
            BloccaConguagli = 1
        End If





        If MioParetri.CausaleCambioServizioSenzaCognuaglio <> "" Then
            Dim cmdCambioCservMese As New OleDbCommand()
            cmdCambioCservMese.CommandText = "Select * From MOVIMENTI Where CodiceOspite = " & CodOsp & " And CAUSALE = '" & MioParetri.CausaleCambioServizioSenzaCognuaglio & "' And MONTH(DATA) = " & Mese & " And YEAR(DATA) = " & Anno
            cmdCambioCservMese.Connection = OspitiCon
            Dim RDCambioCservMese As OleDbDataReader = cmdCambioCservMese.ExecuteReader()
            If RDCambioCservMese.Read() Then
                BloccaConguagli = 1
            End If
            RDCambioCservMese.Close()
        End If

        Dim cmdRettaOspite As New OleDbCommand()
        cmdRettaOspite.CommandText = "Select count(*) From RETTEOSPITE Where CodiceOspite = " & CodOsp & " And CENTROSERVIZIO = '" & Cserv & "' And Mese = " & Mese & " And Anno = " & Anno & " And MANUALE = 'M'"
        cmdRettaOspite.Connection = OspitiCon
        Dim RDRettaOspite As OleDbDataReader = cmdRettaOspite.ExecuteReader()
        If RDRettaOspite.Read() Then
            If campodb(RDRettaOspite.Item(0)) > 0 Then
                MScriviOspite = False
            End If
        End If
        RDRettaOspite.Close()



        Dim cmdRettaParenti As New OleDbCommand()
        cmdRettaParenti.CommandText = "Select count(*) From RETTEPARENTE Where CodiceOspite = " & CodOsp & " And CENTROSERVIZIO = '" & Cserv & "' And Mese = " & Mese & " And Anno = " & Anno & " And MANUALE = 'M'"
        cmdRettaParenti.Connection = OspitiCon
        Dim RDRettaParenti As OleDbDataReader = cmdRettaParenti.ExecuteReader()
        If RDRettaParenti.Read() Then
            If campodb(RDRettaParenti.Item(0)) > 0 Then
                MScriviParente = False
            End If
        End If
        RDRettaParenti.Close()


        Dim cmdRettaComune As New OleDbCommand()
        cmdRettaComune.CommandText = "Select count(*) From RETTECOMUNE Where CodiceOspite = " & CodOsp & " And CENTROSERVIZIO = '" & Cserv & "' And Mese = " & Mese & " And Anno = " & Anno & " And MANUALE = 'M'"
        cmdRettaComune.Connection = OspitiCon
        Dim RDRettaComune As OleDbDataReader = cmdRettaComune.ExecuteReader()
        If RDRettaComune.Read() Then
            If campodb(RDRettaComune.Item(0)) > 0 Then
                MScriviComune = False
            End If
        End If
        RDRettaParenti.Close()



        Dim cmdRettaRegione As New OleDbCommand()
        cmdRettaRegione.CommandText = "Select count(*) From RETTEREGIONE Where CodiceOspite = " & CodOsp & " And CENTROSERVIZIO = '" & Cserv & "' And Mese = " & Mese & " And Anno = " & Anno & " And MANUALE = 'M'"
        cmdRettaRegione.Connection = OspitiCon
        Dim RDRettaRegione As OleDbDataReader = cmdRettaRegione.ExecuteReader()
        If RDRettaRegione.Read() Then
            If campodb(RDRettaRegione.Item(0)) > 0 Then
                MScriviRegione = False
            End If
        End If
        RDRettaRegione.Close()


        xMese = Mese + 1
        xAnno = Anno
        If xMese = 13 Then
            xMese = 1
            xAnno = Anno + 1
        End If

        xMese1 = xMese + 1
        xAnno1 = xAnno
        If xMese1 = 13 Then
            xMese1 = 1
            xAnno1 = xAnno + 1
        End If

        xMese2 = xMese1 + 1
        xAnno2 = xAnno1
        If xMese2 = 13 Then
            xMese2 = 1
            xAnno2 = xAnno1 + 1
        End If


        REM ***********************************************************************************************
        REM *    Routine che calcola i giorni di presenza e di assenza di un ospite
        REM *    Nel periodo successivo
        REM ***********************************************************************************************



        If LO_CodiceOspite.MesiAnticipo > 1 Then
            M1GiornoCalcolati = GiorniNelMese(Cserv, CodOsp, xMese1, xAnno1)
        End If
        If LO_CodiceOspite.MesiAnticipo > 2 Then
            M2GiornoCalcolati = GiorniNelMese(Cserv, CodOsp, xMese2, xAnno2)
        End If

        GiornoCalcolati = GiorniNelMese(Cserv, CodOsp, xMese, xAnno)


        If CampoCentroServizio(Cserv, "TIPOCENTROSERVIZIO") = "D" Then
            GiornoCalcolati = ContaPresenzeDiurno(Cserv, CodOsp, xMese, xAnno, GiornoCalcolati)
        End If

        'Rs_RettaOspite.Open "Select * From RetteOspite Where CENTROSERVIZIO = '" & Cserv & "' And CODICEOSPITE = " & CodOsp & " And Anno = 0", OspitiDb, ADODB.CursorTypeEnum.adOpenForwardOnly,  ADODB.LockTypeEnum.adLockOptimistic
        'Rs_RettaParente.Open "Select * From RetteParente Where CENTROSERVIZIO = '" & Cserv & "' And CODICEOSPITE = " & CodOsp & " And Anno = 0", OspitiDb, ADODB.CursorTypeEnum.adOpenForwardOnly,  ADODB.LockTypeEnum.adLockOptimistic
        'Rs_RettaComune.Open "Select * From RetteComune Where CENTROSERVIZIO = '" & Cserv & "' And CODICEOSPITE = " & CodOsp & " And Anno = 0", OspitiDb, ADODB.CursorTypeEnum.adOpenForwardOnly,  ADODB.LockTypeEnum.adLockOptimistic
        'Rs_RettaRegione.Open "Select * From RetteRegione Where CENTROSERVIZIO = '" & Cserv & "' And CODICEOSPITE = " & CodOsp & " And Anno = 0", OspitiDb, ADODB.CursorTypeEnum.adOpenForwardOnly,  ADODB.LockTypeEnum.adLockOptimistic
        'Rs_RettaEnte.Open "Select * From RetteEnte Where CENTROSERVIZIO = '" & Cserv & "' And CODICEOSPITE = " & CodOsp & " And Anno = 0", OspitiDb, ADODB.CursorTypeEnum.adOpenForwardOnly,  ADODB.LockTypeEnum.adLockOptimistic
        'Controllo importi generati mese precedente..


        ImportoRpx = 0
        ImportoRpx2 = 0
        ExtraAnticipati = 0

        Dim MEmissione As New Cls_EmissioneRetta
        Dim MDocument As New Cls_MovimentoContabile
        Dim CentroServizio As New Cls_CentroServizio

        CentroServizio.Leggi(STRINGACONNESSIONEDB, Cserv)

        'MEmissione.EsisteDocumentoRendiNumero(CentroServizio.MASTRO, CentroServizio.CONTO, CodOsp * 100)


        'ImportoRpx = MDocument.ImportoDocumento()

        REM If CampoOspite(CodOsp, "FattAnticipata", False) = "S" Then




        Dim cmdRpxO As New OleDbCommand()
        cmdRpxO.CommandText = "Select sum(Importo) as Importo From RetteOspite Where CodiceOspite = " & CodOsp & " And CentroServizio = '" & Cserv & "' And  Elemento = 'RPX'   And MeseCompetenza = " & Mese & " And AnnoCompetenza = " & Anno
        cmdRpxO.Connection = OspitiCon
        Dim RDRettaRpxO As OleDbDataReader = cmdRpxO.ExecuteReader()
        If RDRettaRpxO.Read() Then
            ImportoRpx = campodbN(RDRettaRpxO.Item("Importo"))
        End If
        RDRettaRpxO.Close()

        If FattureNC = True Then
            Dim TipoOperazione As New Cls_TipoOperazione
            Dim ScorporoIVA As Boolean = False

            TipoOperazione.Codice = LO_CodiceOspite.TIPOOPERAZIONE

            Dim ImpOperatore As New Cls_ImportoOspite

            ImpOperatore.CODICEOSPITE = CodOsp
            ImpOperatore.CENTROSERVIZIO = Cserv
            ImpOperatore.UltimaDataAData(STRINGACONNESSIONEDB, CodOsp, Cserv, DateSerial(Anno, Mese, 1))

            If ImpOperatore.TIPOOPERAZIONE <> "" Then
                TipoOperazione.Codice = ImpOperatore.TIPOOPERAZIONE
            End If

           
            TipoOperazione.Leggi(STRINGACONNESSIONEDB, TipoOperazione.Codice)

            If TipoOperazione.SCORPORAIVA = 1 Then
                ScorporoIVA = True
            End If

            ImportoRpx = ImportoDocumentoOspite(CentroServizio.MASTRO, CentroServizio.CONTO, CodOsp * 100, CentroServizio.CENTROSERVIZIO, Anno, Mese, "", Anno, Mese, 0, TipoOperazione.TipoAddebitoAccredito, ScorporoIVA, ConnessioneTabelle)


        End If


        Dim cmdRpxO2 As New OleDbCommand()
        cmdRpxO2.CommandText = "Select sum(Importo) as Importo From RetteOspite Where CodiceOspite = " & CodOsp & " And CentroServizio = '" & Cserv & "' And  Elemento = 'R2X'   And MeseCompetenza = " & Mese & " And AnnoCompetenza = " & Anno
        cmdRpxO2.Connection = OspitiCon
        Dim RDRettaRpxO2 As OleDbDataReader = cmdRpxO2.ExecuteReader()
        If RDRettaRpxO2.Read() Then
            ImportoRpx2 = campodbN(RDRettaRpxO2.Item("Importo"))
        End If
        RDRettaRpxO2.Close()

        If FattureNC = True Then
            Dim TipoOperazione As New Cls_TipoOperazione
            Dim ScorporoIVA As Boolean = False

            TipoOperazione.Codice = LO_CodiceOspite.TIPOOPERAZIONE

            Dim ImpOperatore As New Cls_ImportoOspite

            ImpOperatore.CODICEOSPITE = CodOsp
            ImpOperatore.CENTROSERVIZIO = Cserv
            ImpOperatore.UltimaDataAData(STRINGACONNESSIONEDB, CodOsp, Cserv, DateSerial(Anno, Mese, 1))


            TipoOperazione.Codice = LO_CodiceOspite.TIPOOPERAZIONE
            If ImpOperatore.TIPOOPERAZIONE <> "" Then
                TipoOperazione.Codice = ImpOperatore.TIPOOPERAZIONE
            End If


            TipoOperazione.Leggi(STRINGACONNESSIONEDB, TipoOperazione.Codice)

            If TipoOperazione.SCORPORAIVA = 1 Then
                ScorporoIVA = True
            End If

            ImportoRpx2 = ImportoDocumentoOspite2(CentroServizio.MASTRO, CentroServizio.CONTO, CodOsp * 100, CentroServizio.CENTROSERVIZIO, Anno, Mese, "", Anno, Mese, 0, TipoOperazione.TipoAddebitoAccredito2, ScorporoIVA, ConnessioneTabelle)

        End If



        Dim cmdRpxEX As New OleDbCommand()
        cmdRpxEX.CommandText = "Select sum(Importo) as Importo From RetteOspite Where CodiceOspite = " & CodOsp & " And CentroServizio = '" & Cserv & "' And  Elemento Like 'E%'   And MeseCompetenza = " & Mese & " And AnnoCompetenza = " & Anno
        cmdRpxEX.Connection = OspitiCon
        Dim RDRettaRpxEX As OleDbDataReader = cmdRpxEX.ExecuteReader()
        If RDRettaRpxEX.Read() Then
            ExtraAnticipati = campodbN(RDRettaRpxEX.Item("Importo"))
        End If
        RDRettaRpxEX.Close()




        ' Tolti il 05/10/2017 per incongruenza calcolo fatturato, ma non ricordo perch� erano stati messi
        'If ImportoRpx = 0 And LO_CodiceOspite.FattAnticipata = "S" Then
        '    ImportoRpx = ImportoDocumentoOspite(CentroServizio.MASTRO, CentroServizio.CONTO, CodOsp * 100, CentroServizio.CENTROSERVIZIO, Anno, Mese, "", Anno, Mese, 0)
        '    If ImportoRpx = 0 Then
        '        ImportoRpx = ImportoDocumentoOspite(CentroServizio.MASTRO, CentroServizio.CONTO, CodOsp * 100, CentroServizio.CENTROSERVIZIO, Anno, Mese - 1, "", Anno, Mese, 0)
        '    End If
        'End If


        'If ImportoRpx2 = 0 And LO_CodiceOspite.FattAnticipata = "S" Then
        '    ImportoRpx2 = ImportoDocumentoOspite2(CentroServizio.MASTRO, CentroServizio.CONTO, CodOsp * 100, CentroServizio.CENTROSERVIZIO, Anno, Mese, "", Anno, Mese, 0)
        '    If ImportoRpx2 = 0 Then
        '        ImportoRpx2 = ImportoDocumentoOspite2(CentroServizio.MASTRO, CentroServizio.CONTO, CodOsp * 100, CentroServizio.CENTROSERVIZIO, Anno, Mese - 1, "", Anno, Mese, 0)
        '    End If
        'End If

        If ImpExtrOspMan1 <> 0 Then
            ClasseRette.Pulisci()
            ClasseRette.CentroServizio = Cserv
            ClasseRette.CodiceOspite = CodOsp
            ClasseRette.Mese = Mese
            ClasseRette.Anno = Anno

            ClasseRette.MESECOMPETENZA = Mese
            ClasseRette.ANNOCOMPETENZA = Anno
            If ImportoPresOspite <> 0 Then
                ClasseRette.STATOCONTABILE = "A"
            Else
                ClasseRette.STATOCONTABILE = "N"
            End If
            ClasseRette.ELEMENTO = "ACC"
            If ImpExtrOspMan1 > 0 Then
                ClasseRette.ELEMENTO = "ADD"
            End If
            ClasseRette.Giorni = NumExtrOspMan1
            ClasseRette.Importo = Math.Abs(Modulo.MathRound(ImpExtrOspMan1, 2))

            Dim Ospite As New ClsOspite

            Ospite.CodiceOspite = CodOsp
            Ospite.Leggi(STRINGACONNESSIONEDB, CodOsp)

            Dim OspiteCsver As New Cls_DatiOspiteParenteCentroServizio

            OspiteCsver.CodiceOspite = CodOsp
            OspiteCsver.CentroServizio = Cserv
            OspiteCsver.CodiceParente = 0
            OspiteCsver.Leggi(STRINGACONNESSIONEDB)
            If OspiteCsver.CodiceOspite > 0 Then
                Ospite.TIPOOPERAZIONE = OspiteCsver.TipoOperazione
            End If

            Dim TipoOperazione As New Cls_TipoOperazione

            TipoOperazione.Codice = Ospite.TIPOOPERAZIONE
            TipoOperazione.Leggi(STRINGACONNESSIONEDB, TipoOperazione.Codice)

            ClasseRette.CODICEEXTRA = TipoOperazione.TipoAddebitoAccredito

            If TipoAddExtrOspMan1 <> "" Then
                ClasseRette.CODICEEXTRA = TipoAddExtrOspMan1
            End If

            Dim kTipoAddebito As New Cls_Addebito
            kTipoAddebito.Codice = ClasseRette.CODICEEXTRA
            kTipoAddebito.Leggi(STRINGACONNESSIONEDB, kTipoAddebito.Codice)

            ClasseRette.Descrizione = kTipoAddebito.Descrizione


            If MScriviOspite = True Then ClasseRette.ScriviRettaOspite()
        End If



        If ImpExtrOspMan2 <> 0 Then
            ClasseRette.Pulisci()
            ClasseRette.CentroServizio = Cserv
            ClasseRette.CodiceOspite = CodOsp
            ClasseRette.Mese = Mese
            ClasseRette.Anno = Anno
            ClasseRette.MESECOMPETENZA = Mese
            ClasseRette.ANNOCOMPETENZA = Anno
            If ImportoPresOspite <> 0 Then
                ClasseRette.STATOCONTABILE = "A"
            Else
                ClasseRette.STATOCONTABILE = "N"
            End If
            ClasseRette.ELEMENTO = "ACC"
            If ImpExtrOspMan2 > 0 Then
                ClasseRette.ELEMENTO = "ADD"
            End If

            ClasseRette.Giorni = NumExtrOspMan2
            ClasseRette.Importo = Math.Abs(Modulo.MathRound(ImpExtrOspMan2, 2))

            Dim Ospite As New ClsOspite

            Ospite.CodiceOspite = CodOsp
            Ospite.Leggi(STRINGACONNESSIONEDB, CodOsp)

            Dim OspiteCsver As New Cls_DatiOspiteParenteCentroServizio

            OspiteCsver.CodiceOspite = CodOsp
            OspiteCsver.CentroServizio = Cserv
            OspiteCsver.CodiceParente = 0
            OspiteCsver.Leggi(STRINGACONNESSIONEDB)
            If OspiteCsver.CodiceOspite > 0 Then
                Ospite.TIPOOPERAZIONE = OspiteCsver.TipoOperazione
            End If

            Dim TipoOperazione As New Cls_TipoOperazione

            TipoOperazione.Codice = Ospite.TIPOOPERAZIONE
            TipoOperazione.Leggi(STRINGACONNESSIONEDB, TipoOperazione.Codice)

            ClasseRette.CODICEEXTRA = TipoOperazione.TipoAddebitoAccredito

            If TipoAddExtrOspMan2 <> "" Then
                ClasseRette.CODICEEXTRA = TipoAddExtrOspMan2
            End If

            Dim kTipoAddebito As New Cls_Addebito
            kTipoAddebito.Codice = ClasseRette.CODICEEXTRA
            kTipoAddebito.Leggi(STRINGACONNESSIONEDB, kTipoAddebito.Codice)

            ClasseRette.Descrizione = kTipoAddebito.Descrizione

            If MScriviOspite = True Then ClasseRette.ScriviRettaOspite()
        End If



        If ImpExtrOspMan3 <> 0 Then
            ClasseRette.Pulisci()
            ClasseRette.CentroServizio = Cserv
            ClasseRette.CodiceOspite = CodOsp
            ClasseRette.Mese = Mese
            ClasseRette.Anno = Anno
            ClasseRette.MESECOMPETENZA = Mese
            ClasseRette.ANNOCOMPETENZA = Anno

            If ImportoPresOspite <> 0 Then
                ClasseRette.STATOCONTABILE = "A"
            Else
                ClasseRette.STATOCONTABILE = "N"
            End If

            ClasseRette.ELEMENTO = "ACC"
            If ImpExtrOspMan3 > 0 Then
                ClasseRette.ELEMENTO = "ADD"
            End If
            ClasseRette.Giorni = NumExtrOspMan3
            ClasseRette.Importo = Math.Abs(Modulo.MathRound(ImpExtrOspMan3, 2))

            Dim Ospite As New ClsOspite

            Ospite.CodiceOspite = CodOsp
            Ospite.Leggi(STRINGACONNESSIONEDB, CodOsp)

            Dim OspiteCsver As New Cls_DatiOspiteParenteCentroServizio

            OspiteCsver.CodiceOspite = CodOsp
            OspiteCsver.CentroServizio = Cserv
            OspiteCsver.CodiceParente = 0
            OspiteCsver.Leggi(STRINGACONNESSIONEDB)
            If OspiteCsver.CodiceOspite > 0 Then
                Ospite.TIPOOPERAZIONE = OspiteCsver.TipoOperazione
            End If

            Dim TipoOperazione As New Cls_TipoOperazione

            TipoOperazione.Codice = Ospite.TIPOOPERAZIONE
            TipoOperazione.Leggi(STRINGACONNESSIONEDB, TipoOperazione.Codice)

            ClasseRette.CODICEEXTRA = TipoOperazione.TipoAddebitoAccredito

            If TipoAddExtrOspMan3 <> "" Then
                ClasseRette.CODICEEXTRA = TipoAddExtrOspMan3
            End If

            Dim kTipoAddebito As New Cls_Addebito
            kTipoAddebito.Codice = ClasseRette.CODICEEXTRA
            kTipoAddebito.Leggi(STRINGACONNESSIONEDB, kTipoAddebito.Codice)

            ClasseRette.Descrizione = kTipoAddebito.Descrizione

            If MScriviOspite = True Then ClasseRette.ScriviRettaOspite()
        End If



        If ImpExtrOspMan4 <> 0 Then
            ClasseRette.Pulisci()
            ClasseRette.CentroServizio = Cserv
            ClasseRette.CodiceOspite = CodOsp
            ClasseRette.Mese = Mese
            ClasseRette.Anno = Anno
            ClasseRette.MESECOMPETENZA = Mese
            ClasseRette.ANNOCOMPETENZA = Anno

            If ImportoPresOspite <> 0 Then
                ClasseRette.STATOCONTABILE = "A"
            Else
                ClasseRette.STATOCONTABILE = "N"
            End If
            ClasseRette.ELEMENTO = "ACC"
            If ImpExtrOspMan4 > 0 Then
                ClasseRette.ELEMENTO = "ADD"
            End If

            ClasseRette.Giorni = NumExtrOspMan4
            ClasseRette.Importo = Math.Abs(Modulo.MathRound(ImpExtrOspMan4, 2))

            Dim Ospite As New ClsOspite

            Ospite.CodiceOspite = CodOsp
            Ospite.Leggi(STRINGACONNESSIONEDB, CodOsp)

            Dim OspiteCsver As New Cls_DatiOspiteParenteCentroServizio

            OspiteCsver.CodiceOspite = CodOsp
            OspiteCsver.CentroServizio = Cserv
            OspiteCsver.CodiceParente = 0
            OspiteCsver.Leggi(STRINGACONNESSIONEDB)
            If OspiteCsver.CodiceOspite > 0 Then
                Ospite.TIPOOPERAZIONE = OspiteCsver.TipoOperazione
            End If

            Dim TipoOperazione As New Cls_TipoOperazione

            TipoOperazione.Codice = Ospite.TIPOOPERAZIONE
            TipoOperazione.Leggi(STRINGACONNESSIONEDB, TipoOperazione.Codice)

            ClasseRette.CODICEEXTRA = TipoOperazione.TipoAddebitoAccredito

            If TipoAddExtrOspMan4 <> "" Then
                ClasseRette.CODICEEXTRA = TipoAddExtrOspMan4
            End If


            Dim kTipoAddebito As New Cls_Addebito
            kTipoAddebito.Codice = ClasseRette.CODICEEXTRA
            kTipoAddebito.Leggi(STRINGACONNESSIONEDB, kTipoAddebito.Codice)

            ClasseRette.Descrizione = kTipoAddebito.Descrizione

            If MScriviOspite = True Then ClasseRette.ScriviRettaOspite()
        End If


        'If ImportoRpx = 0 Then
        'CampoOspite(CodOsp, "FattAnticipata", False)
        If (LO_CodiceOspite.FattAnticipata <> "S" Or (ImportoRpx = 0 And ExtraAnticipati = 0)) And BloccaConguagli = 0 Then
            ' Calcolo "normale"

            If ImportoPresOspite2 <> 0 Then
                ClasseRette.Pulisci()
                ClasseRette.CentroServizio = Cserv
                ClasseRette.CodiceOspite = CodOsp
                ClasseRette.Mese = Mese
                ClasseRette.Anno = Anno
                ClasseRette.STATOCONTABILE = "A"
                ClasseRette.ELEMENTO = "R2P"
                ClasseRette.Giorni = GiorniPres
                ClasseRette.Importo = Modulo.MathRound(ImportoPresOspite2, 2)
                If MScriviOspite = True Then ClasseRette.ScriviRettaOspite()
            End If

            If GiorniPres <> 0 Or ImportoPresOspite <> 0 Then
                ClasseRette.Pulisci()
                ClasseRette.CentroServizio = Cserv
                ClasseRette.CodiceOspite = CodOsp
                ClasseRette.Mese = Mese
                ClasseRette.Anno = Anno
                ClasseRette.STATOCONTABILE = "A"
                ClasseRette.ELEMENTO = "RGP"
                ClasseRette.Giorni = GiorniPres
                ClasseRette.Importo = Modulo.MathRound(ImportoPresOspite, 2)
                If MScriviOspite = True Then ClasseRette.ScriviRettaOspite()


                If LO_CodiceOspite.Compensazione = "" Then
                    StringaDegliErrori = StringaDegliErrori & "Compensazione mancante per Ospite " & CodOsp & " - " & CampoOspite(CodOsp, "NOME", False) & Chr(13)
                End If
                If LO_CodiceOspite.PERIODO = "" Then
                    StringaDegliErrori = StringaDegliErrori & "PERIODO mancante per Ospite " & CodOsp & " - " & CampoOspite(CodOsp, "NOME", False) & Chr(13)
                End If

                If LO_CodiceOspite.TIPOOPERAZIONE = "" Then
                    StringaDegliErrori = StringaDegliErrori & "Errore non presente il Tipo Operazione x Ospite : " & CodOsp & " - " & CampoOspite(CodOsp, "NOME", False) & vbNewLine
                End If
                If LO_CodiceOspite.CODICEIVA = "" Then
                    StringaDegliErrori = StringaDegliErrori & "Errore non presente il Codice IVA x Ospite : " & CodOsp & " - " & CampoOspite(CodOsp, "NOME", False) & vbNewLine
                End If
            End If




            'Tolto GiorniAss <> 0 Or  non credo avesse senso 20/07/2017
            If ImportoAssOspite2 <> 0 Then

                ClasseRette.Pulisci()
                ClasseRette.CentroServizio = Cserv
                ClasseRette.CodiceOspite = CodOsp
                ClasseRette.Mese = Mese
                ClasseRette.Anno = Anno
                ClasseRette.STATOCONTABILE = "A"
                ClasseRette.ELEMENTO = "R2A"
                ClasseRette.Giorni = GiorniAss
                ClasseRette.Importo = Modulo.MathRound(ImportoAssOspite2, 2)
                If MScriviOspite = True Then ClasseRette.ScriviRettaOspite()

                If LO_CodiceOspite.TIPOOPERAZIONE = "" Then
                    StringaDegliErrori = StringaDegliErrori & "Errore non presente il Tipo Operazione x Ospite : " & CodOsp & " - " & CampoOspite(CodOsp, "NOME", False) & vbNewLine
                End If
                If LO_CodiceOspite.CODICEIVA = "" Then
                    StringaDegliErrori = StringaDegliErrori & "Errore non presente il Codice IVA x Ospite : " & CodOsp & " - " & CampoOspite(CodOsp, "NOME", False) & vbNewLine
                End If

            End If


            If GiorniAss <> 0 Or ImportoAssOspite <> 0 Then

                ClasseRette.Pulisci()
                ClasseRette.CentroServizio = Cserv
                ClasseRette.CodiceOspite = CodOsp
                ClasseRette.Mese = Mese
                ClasseRette.Anno = Anno
                ClasseRette.STATOCONTABILE = "A"
                ClasseRette.ELEMENTO = "RGA"
                ClasseRette.Giorni = GiorniAss
                ClasseRette.Importo = Modulo.MathRound(ImportoAssOspite, 2)
                If MScriviOspite = True Then ClasseRette.ScriviRettaOspite()

                'Rs_RettaOspite.AddNew
                'MoveToDb Rs_RettaOspite.Fields("CentroServizio, Cserv
                'MoveToDb Rs_RettaOspite.Fields("CodiceOspite, CodOsp
                'MoveToDb Rs_RettaOspite.Fields("Mese, Mese
                'MoveToDb Rs_RettaOspite.Fields("Anno, Anno
                'MoveToDb Rs_RettaOspite.Fields("STATOCONTABILE, "A"
                'MoveToDb Rs_RettaOspite.Fields("ELEMENTO, "RGA"
                'MoveToDb Rs_RettaOspite.Fields("Giorni, GiorniAss
                'MoveToDb Rs_RettaOspite.Fields("Importo, Modulo.MathRound(ImportoAssOspite, 2)
                'Rs_RettaOspite.Update


                If LO_CodiceOspite.TIPOOPERAZIONE = "" Then
                    StringaDegliErrori = StringaDegliErrori & "Errore non presente il Tipo Operazione x Ospite : " & CodOsp & " - " & CampoOspite(CodOsp, "NOME", False) & vbNewLine
                End If
                If LO_CodiceOspite.CODICEIVA = "" Then
                    StringaDegliErrori = StringaDegliErrori & "Errore non presente il Codice IVA x Ospite : " & CodOsp & " - " & CampoOspite(CodOsp, "NOME", False) & vbNewLine
                End If

            End If


            If NonImportoPresOspite2 <> 0 Then

                ClasseRette.Pulisci()
                ClasseRette.CentroServizio = Cserv
                ClasseRette.CodiceOspite = CodOsp
                ClasseRette.Mese = Mese
                ClasseRette.Anno = Anno
                ClasseRette.STATOCONTABILE = "N"
                ClasseRette.ELEMENTO = "R2P"
                ClasseRette.Giorni = NonGiorniPres
                ClasseRette.Importo = Modulo.MathRound(NonImportoPresOspite2, 2)
                If MScriviOspite = True Then ClasseRette.ScriviRettaOspite()
            End If

            If NonGiorniPres <> 0 Or NonImportoPresOspite <> 0 Then

                ClasseRette.Pulisci()
                ClasseRette.CentroServizio = Cserv
                ClasseRette.CodiceOspite = CodOsp
                ClasseRette.Mese = Mese
                ClasseRette.Anno = Anno
                ClasseRette.STATOCONTABILE = "N"
                ClasseRette.ELEMENTO = "RGP"
                ClasseRette.Giorni = NonGiorniPres
                ClasseRette.Importo = Modulo.MathRound(NonImportoPresOspite, 2)
                If MScriviOspite = True Then ClasseRette.ScriviRettaOspite()

                If LO_CodiceOspite.TIPOOPERAZIONE = "" Then
                    StringaDegliErrori = StringaDegliErrori & "Errore non presente il Tipo Operazione x Ospite : " & CodOsp & " - " & CampoOspite(CodOsp, "NOME", False) & vbNewLine
                End If
                If LO_CodiceOspite.CODICEIVA = "" Then
                    StringaDegliErrori = StringaDegliErrori & "Errore non presente il Codice IVA Ospite : " & CodOsp & " - " & CampoOspite(CodOsp, "NOME", False) & vbNewLine
                End If
            End If
            If NonGiorniAss <> 0 Or NonImportoAssOspite <> 0 Then

                ClasseRette.Pulisci()
                ClasseRette.CentroServizio = Cserv
                ClasseRette.CodiceOspite = CodOsp
                ClasseRette.Mese = Mese
                ClasseRette.Anno = Anno
                ClasseRette.STATOCONTABILE = "N"
                ClasseRette.ELEMENTO = "RGA"
                ClasseRette.Giorni = NonGiorniAss
                ClasseRette.Importo = Modulo.MathRound(NonImportoAssOspite, 2)
                If MScriviOspite = True Then ClasseRette.ScriviRettaOspite()

                If LO_CodiceOspite.TIPOOPERAZIONE = "" Then
                    StringaDegliErrori = StringaDegliErrori & "Errore non presente il Tipo Operazione x Ospite : " & CodOsp & " - " & CampoOspite(CodOsp, "NOME", False) & vbNewLine
                End If
                If LO_CodiceOspite.CODICEIVA = "" Then
                    StringaDegliErrori = StringaDegliErrori & "Errore non presente il Codice IVA x Ospite : " & CodOsp & " - " & CampoOspite(CodOsp, "NOME", False) & vbNewLine
                End If
            End If

            If NonImportoAssOspite2 <> 0 Then

                ClasseRette.Pulisci()
                ClasseRette.CentroServizio = Cserv
                ClasseRette.CodiceOspite = CodOsp
                ClasseRette.Mese = Mese
                ClasseRette.Anno = Anno
                ClasseRette.STATOCONTABILE = "N"
                ClasseRette.ELEMENTO = "R2A"
                ClasseRette.Giorni = NonGiorniAss
                ClasseRette.Importo = Modulo.MathRound(NonImportoAssOspite2, 2)
                If MScriviOspite = True Then ClasseRette.ScriviRettaOspite()

                If LO_CodiceOspite.TIPOOPERAZIONE = "" Then
                    StringaDegliErrori = StringaDegliErrori & "Errore non presente il Tipo Operazione x Ospite : " & CodOsp & " - " & CampoOspite(CodOsp, "NOME", False) & vbNewLine
                End If
                If LO_CodiceOspite.CODICEIVA = "" Then
                    StringaDegliErrori = StringaDegliErrori & "Errore non presente il Codice IVA x Ospite : " & CodOsp & " - " & CampoOspite(CodOsp, "NOME", False) & vbNewLine
                End If
            End If

        Else
            ' Conguaglio Mese precedente

            ImportoExtra = 0 'ImpExtraOspite(0) + ImpExtraOspite(1) + ImpExtraOspite(2) + ImpExtraOspite(3) + ImpExtraOspite(4) + ImpExtraOspite(5) + ImpExtraOspite(6) + ImpExtraOspite(7) + ImpExtraOspite(8) + ImpExtraOspite(9) + _
            'NonImpExtraOspite(0) + NonImpExtraOspite(1) + NonImpExtraOspite(2) + NonImpExtraOspite(3) + NonImpExtraOspite(4) + NonImpExtraOspite(5) + NonImpExtraOspite(6) + NonImpExtraOspite(7) + NonImpExtraOspite(8) + NonImpExtraOspite(9)

            Differenze = Modulo.MathRound(ImportoRpx - Modulo.MathRound((ImportoPresOspite + NonImportoPresOspite + NonImportoAssOspite + ImportoAssOspite + ImportoExtra), 2), 2)

            'MyRs.Open("Select  Sum(Importo) From ADDACR where RETTA = 'S' And TipoMov = 'AD' And CodiceOspite = " & CodOsp & " And CENTROSERVIZIO = '" & Cserv & "' And MeseCompetenza = " & Mese & " And AnnoCompetenza = " & Anno & " And Riferimento = 'O'", OspitiDb, ADODB.CursorTypeEnum.adOpenKeyset, ADODB.LockTypeEnum.adLockOptimistic)
            'Differenze = Differenze + MoveFromDb(MyRs.Fields(0))
            'MyRs.Close()

            Dim cmdAdd As New OleDbCommand()
            cmdAdd.CommandText = "Select  Sum(Importo) From ADDACR where RETTA = 'S' And TipoMov = 'AD' And CodiceOspite = " & CodOsp & " And CENTROSERVIZIO = '" & Cserv & "' And MeseCompetenza = " & Mese & " And AnnoCompetenza = " & Anno & " And Riferimento = 'O'"
            cmdAdd.Connection = OspitiCon
            Dim RDRettaAdd As OleDbDataReader = cmdAdd.ExecuteReader()
            If RDRettaAdd.Read() Then
                Differenze = Differenze + campodbN(RDRettaAdd.Item(0))
            End If
            RDRettaAdd.Close()



            'MyRs.Open("Select  Sum(Importo) From ADDACR where RETTA = 'S' And TipoMov = 'AC' And CodiceOspite = " & CodOsp & " And CENTROSERVIZIO = '" & Cserv & "' And MeseCompetenza = " & Mese & " And AnnoCompetenza  = " & Anno & " And Riferimento = 'O'", OspitiDb, ADODB.CursorTypeEnum.adOpenKeyset, ADODB.LockTypeEnum.adLockOptimistic)
            'Differenze = Differenze - MoveFromDb(MyRs.Fields(0))
            'MyRs.Close()

            Dim cmdAcc As New OleDbCommand()
            cmdAcc.CommandText = "Select  Sum(Importo) From ADDACR where RETTA = 'S' And TipoMov = 'AC' And CodiceOspite = " & CodOsp & " And CENTROSERVIZIO = '" & Cserv & "' And MeseCompetenza = " & Mese & " And AnnoCompetenza  = " & Anno & " And Riferimento = 'O'"
            cmdAcc.Connection = OspitiCon
            Dim RDRettaACC As OleDbDataReader = cmdAcc.ExecuteReader()
            If RDRettaACC.Read() Then
                Differenze = Differenze - campodbN(RDRettaACC.Item(0))
            End If
            RDRettaACC.Close()


            If Math.Abs(Differenze) < ConguaglioMinimo Then
                Differenze = 0
            End If
            If BloccaConguagli = 1 Then
                Differenze = 0
            End If

            REM If CampoOspite(CodOsp, "FattAnticipata", False) = "S" Then
            If Differenze < 0 Then
                MaxAddebitoOspite = MaxAddebitoOspite + 1
                MyTabAddebitoOspite(MaxAddebitoOspite) = Modulo.MathRound(MyTabAddebitoOspite(MaxAddebitoOspite) + (Differenze * -1), 2)
                MyTabAddebitoMeseCompetenza(MaxAddebitoOspite) = Mese
                MyTabAddebitoAnnoCompetenza(MaxAddebitoOspite) = Anno
            Else
                MaxAccreditoOspite = MaxAccreditoOspite + 1
                MyTabAccreditoOspite(MaxAccreditoOspite) = Modulo.MathRound(MyTabAccreditoOspite(MaxAccreditoOspite) + (Differenze), 2)
                MyTabAccreditoMeseCompetenza(MaxAccreditoOspite) = Mese
                MyTabAccreditoAnnoCompetenza(MaxAccreditoOspite) = Anno
            End If

            REM *************************************CONGUAGLIO SECONDO IMPORTO *************************************
            Dim TipoOperazione As New Cls_TipoOperazione

            TipoOperazione.Codice = LO_CodiceOspite.TIPOOPERAZIONE
            TipoOperazione.Leggi(STRINGACONNESSIONEDB, TipoOperazione.Codice)


            Differenze = Modulo.MathRound(ImportoRpx2 - Modulo.MathRound((ImportoPresOspite2 + NonImportoPresOspite2 + NonImportoAssOspite2 + ImportoAssOspite2), 2), 2)



            If Math.Abs(Differenze) < ConguaglioMinimo Then
                Differenze = 0
            End If
            If BloccaConguagli = 1 Then
                Differenze = 0
            End If

            If Differenze < 0 Then
                MaxAddebitoOspite = MaxAddebitoOspite + 1
                MyTabAddebitoOspite(MaxAddebitoOspite) = Modulo.MathRound(MyTabAddebitoOspite(MaxAddebitoOspite) + (Differenze * -1), 2)
                MyTabCodiceIvaAddebitoOspite(MaxAddebitoOspite) = TipoOperazione.TipoAddebitoAccredito2
                MyTabAddebitoMeseCompetenza(MaxAddebitoOspite) = Mese
                MyTabAddebitoAnnoCompetenza(MaxAddebitoOspite) = Anno
            Else
                MaxAccreditoOspite = MaxAccreditoOspite + 1
                MyTabAccreditoOspite(MaxAccreditoOspite) = Modulo.MathRound(MyTabAccreditoOspite(MaxAccreditoOspite) + (Differenze), 2)
                MyTabCodiceIvaAccreditoOspite(MaxAccreditoOspite) = TipoOperazione.TipoAddebitoAccredito2
                MyTabAccreditoMeseCompetenza(MaxAccreditoOspite) = Mese
                MyTabAccreditoAnnoCompetenza(MaxAccreditoOspite) = Anno
            End If


            REM End If
        End If
        'CampoOspite(CodOsp, "FattAnticipata", False)
        If LO_CodiceOspite.FattAnticipata = "S" And OspitePresente(Cserv, CodOsp, Mese, Anno) = True Then
            ' ********************************************
            ' * Creazione importo mese successivo
            xMese = Mese + 1
            xAnno = Anno
            If xMese = 13 Then
                xMese = 1
                xAnno = Anno + 1
            End If
            Calcola = 0
            Calcola2 = 0
            Calcola2P1 = 0
            Giorni2P1 = 0

            Dim GiornoCalcolatiFatturati As Integer = 0


            Call AnticipoExtraFissi(CodOsp, Cserv, Anno, Mese, xAnno, xMese, BloccaConguagli, GiornoCalcolati, MScriviOspite, FattureNC)


            If OspitePresente(Cserv, CodOsp, xMese, xAnno) = True Then
                SalvaImportoRegione = 0
                SalvaImportoComune = 0
                SalvaImportoOspite = 0 ' UTILIZZATO COME CONTATORE PER I PARENTI
                SalvaImportoTotale = 0
                SalvaPercentuale = 0


                'For i = 1 To GiorniMese(Mese, Anno)
                '  If MyTabModalita(i) <> "" Then
                '     SalvaDifferenza = MyTabModalita(i)
                '  End If
                '  Differenze = 0
                '  For Par = 1 To 10
                '    If MyTabImportoParente(i, Par) <> -1 Then
                '      Differenze = Differenze + MyTabImportoParente(i, Par)
                '     SalvaImportoOspite = Differenze
                '    End If
                '  Next Par
                '  If MyTabImportoRetta(i) <> -1 Then
                '     Differenze = MyTabImportoRetta(i) - Differenze
                '     SalvaImportoTotale = MyTabImportoRetta(i)
                '    Else
                '     Differenze = SalvaImportoTotale - Differenze
                '  End If
                '  If MyTabImportoRegione(i) <> -1 Then
                '    Differenze = Differenze - MyTabImportoRegione(i)
                '    SalvaImportoRegione = MyTabImportoRegione(i)
                '   Else
                '    MyTabImportoRegione(i) = SalvaImportoRegione
                '  End If
                '  If MyTabImportoComune(i) <> -1 Then
                '    Differenze = Differenze - MyTabImportoComune(i)
                '    SalvaImportoComune = MyTabImportoComune(i)
                '   Else
                '    Differenze = Differenze - SalvaImportoComune
                '  End If
                '
                '  If SalvaDifferenza = "O" Then
                '    Calcola = Differenze * GiornoCalcolati
                '    Else
                '    If MyTabImportoOspite(i) > 0 Then
                '      Calcola = MyTabImportoOspite(i) * GiornoCalcolati
                '    End If
                '  End If
                'Next i
                Dim QuotaGiorno1 As Double
                Dim QuotaGiorno2 As Double
                Dim VariazioniPresenti As Boolean



                QuotaGiorno1 = QuoteGiornaliere(Cserv, CodOsp, "O1", 0, DateSerial(xAnno, xMese, 1))
                QuotaGiorno2 = QuoteGiornaliere(Cserv, CodOsp, "O2", 0, DateSerial(xAnno, xMese, 1))
                VariazioniPresenti = VariazioniRettaDopoData(Cserv, CodOsp, DateSerial(xAnno, xMese, 1))
                Calcola = 0
                Calcola2 = 0
                Calcola2P1 = 0
                Giorni2P1 = 0

                For I = 1 To GiornoCalcolati
                    If VariazioniPresenti = True Then
                        Dim AppoggioVariabile As Double = QuoteGiornaliere(Cserv, CodOsp, "O1", 0, DateSerial(xAnno, xMese, I))
                        Calcola = Calcola + AppoggioVariabile
                        If AppoggioVariabile > 0 Then
                            GiornoCalcolatiFatturati = GiornoCalcolatiFatturati + 1
                        End If
                        If Math.Round(AppoggioVariabile, 2) <> Math.Round(QuotaGiorno1, 2) And (QuotaGiorno1 > 0 Or Calcola2P1 > 0) And AppoggioVariabile > 0 Then
                            Calcola2P1 = Calcola2P1 + AppoggioVariabile
                            Giorni2P1 = Giorni2P1 + 1
                            QuotaGiorno1 = 0
                        End If
                    Else
                        Calcola = Calcola + QuotaGiorno1
                    End If

                    If VariazioniPresenti = True Then
                        Calcola2 = Calcola2 + QuoteGiornaliere(Cserv, CodOsp, "O2", 0, DateSerial(xAnno, xMese, I))
                    Else
                        Calcola2 = Calcola2 + QuotaGiorno2
                    End If
                Next I



                If GiornoCalcolati = GiorniMese(xMese, xAnno) Then
                    If QuoteMensile(Cserv, CodOsp, "O1", 0, DateSerial(xAnno, xMese, I - 1)) > 0 And Calcola > 0 Then
                        Calcola = QuoteMensile(Cserv, CodOsp, "O1", 0, DateSerial(xAnno, xMese, I - 1))
                        Calcola2P1 = 0
                        Giorni2P1 = 0
                    Else
                        If ImportoMensileOspite = 0 And Calcola > 0 Then
                            If QuoteMensile(Cserv, CodOsp, "O1", 0, DateSerial(xAnno, xMese, I)) > 0 Then
                                Calcola = QuoteMensile(Cserv, CodOsp, "O1", 0, DateSerial(xAnno, xMese, I))
                                Calcola2P1 = 0
                                Giorni2P1 = 0
                            End If
                        End If
                    End If
                    If QuoteMensile(Cserv, CodOsp, "O2", 0, DateSerial(xAnno, xMese, I - 1)) > 0 And Calcola > 0 Then
                        Calcola2 = QuoteMensile(Cserv, CodOsp, "O2", 0, DateSerial(xAnno, xMese, I - 1))
                    Else
                        If ImportoMensileOspite = 0 And Calcola > 0 Then
                            If QuoteMensile(Cserv, CodOsp, "O2", 0, DateSerial(xAnno, xMese, I)) > 0 Then
                                Calcola2 = QuoteMensile(Cserv, CodOsp, "O2", 0, DateSerial(xAnno, xMese, I))
                            End If
                        End If
                    End If
                Else
                    If CampoCentroServizio(Cserv, "TIPOCENTROSERVIZIO") = "D" Then
                        If QuoteMensile(Cserv, CodOsp, "O1", 0, DateSerial(xAnno, xMese, I)) > 0 Then
                            Calcola = QuoteMensile(Cserv, CodOsp, "O1", 0, DateSerial(xAnno, xMese, I))
                            Calcola2P1 = 0
                            Giorni2P1 = 0
                        End If
                    End If
                    If CampoCentroServizio(Cserv, "TIPOCENTROSERVIZIO") = "D" Then
                        If QuoteMensile(Cserv, CodOsp, "O2", 0, DateSerial(xAnno, xMese, I)) > 0 Then
                            Calcola2 = QuoteMensile(Cserv, CodOsp, "O2", 0, DateSerial(xAnno, xMese, I))
                        End If
                    End If
                End If

                If GiornoCalcolati = GiorniMese(xMese, xAnno) Then
                    Dim Modalita As New Cls_Modalita

                    Modalita.CentroServizio = Cserv
                    Modalita.CodiceOspite = CodOsp
                    Modalita.UltimaData(STRINGACONNESSIONEDB, Modalita.CodiceOspite, Modalita.CentroServizio)
                    If Modalita.MODALITA = "O" Then
                        Dim ML As New Cls_ImportoComune

                        ML.CODICEOSPITE = CodOsp
                        ML.CENTROSERVIZIO = Cserv
                        ML.UltimaDataAData(STRINGACONNESSIONEDB, Modalita.CodiceOspite, Modalita.CentroServizio, DateSerial(xAnno, xMese, 1))
                        If ML.Tipo = "M" Then
                            Dim QuotaComune As Double
                            Dim QuotaComuneCal As Double

                            QuotaComuneCal = QuoteGiornaliere(Cserv, CodOsp, "C", 0, DateSerial(xAnno, xMese, 1)) * GiornoCalcolati

                            Calcola = Calcola + QuotaComuneCal - ML.Importo
                            Calcola2P1 = 0
                            Giorni2P1 = 0
                        End If
                    End If
                End If


                If ForzaRPXOspite > 0 And GiornoCalcolati = GiorniMese(xMese, xAnno) Then
                    Calcola = ForzaRPXOspite
                    Calcola2P1 = 0
                    Giorni2P1 = 0
                End If


                'Calcola = QuoteGiornaliere(Cserv, CodOsp, "O", 0, DateSerial(xAnno, xMese, GiorniMese(xMese, xAnno))) * GiornoCalcolati
            End If

            If Calcola > 0 Then
                'Val(CampoOspite(CodOsp, "MesiAnticipo", False)) 
                If LO_CodiceOspite.MesiAnticipo < 2 Then
                    ClasseRette.Pulisci()
                    ClasseRette.CentroServizio = Cserv
                    ClasseRette.CodiceOspite = CodOsp
                    ClasseRette.Mese = Mese
                    ClasseRette.Anno = Anno
                    ClasseRette.STATOCONTABILE = ""
                    ClasseRette.ELEMENTO = "RPX"
                    ClasseRette.Giorni = GiornoCalcolatiFatturati - Giorni2P1
                    If GiornoCalcolatiFatturati <= 0 Then
                        ClasseRette.Giorni = GiornoCalcolati - Giorni2P1
                    End If
                    ClasseRette.Importo = Modulo.MathRound(Calcola - Calcola2P1, 2)
                    ClasseRette.MESECOMPETENZA = xMese
                    ClasseRette.ANNOCOMPETENZA = xAnno
                    If MScriviOspite = True Then ClasseRette.ScriviRettaOspite()
                End If

                If Calcola2P1 > 0 Then
                    ClasseRette.Pulisci()
                    ClasseRette.CentroServizio = Cserv
                    ClasseRette.CodiceOspite = CodOsp
                    ClasseRette.Mese = Mese
                    ClasseRette.Anno = Anno
                    ClasseRette.STATOCONTABILE = ""
                    ClasseRette.ELEMENTO = "RPX"
                    ClasseRette.Giorni = Giorni2P1
                    ClasseRette.Importo = Modulo.MathRound(Calcola2P1, 2)
                    ClasseRette.MESECOMPETENZA = xMese
                    ClasseRette.ANNOCOMPETENZA = xAnno
                    If MScriviOspite = True Then ClasseRette.ScriviRettaOspite()
                End If

                DataAcco = DataAccoglimento(Cserv, CodOsp, Mese, Anno)
                'Val(CampoOspite(CodOsp, "MesiAnticipo", False)) 
                If LO_CodiceOspite.MesiAnticipo = 2 And _
                       Mese = Month(DataAcco) And Anno = Year(DataAcco) Then
                    ClasseRette.Pulisci()
                    ClasseRette.CentroServizio = Cserv
                    ClasseRette.CodiceOspite = CodOsp
                    ClasseRette.Mese = Mese
                    ClasseRette.Anno = Anno
                    ClasseRette.STATOCONTABILE = ""
                    ClasseRette.ELEMENTO = "RPX"
                    ClasseRette.Giorni = GiornoCalcolati
                    ClasseRette.Importo = Modulo.MathRound(Calcola, 2)
                    ClasseRette.MESECOMPETENZA = xMese
                    ClasseRette.ANNOCOMPETENZA = xAnno
                    If MScriviOspite = True Then ClasseRette.ScriviRettaOspite()

                    ClasseRette.Pulisci()
                    ClasseRette.CentroServizio = Cserv
                    ClasseRette.CodiceOspite = CodOsp
                    ClasseRette.Mese = Mese
                    ClasseRette.Anno = Anno
                    ClasseRette.STATOCONTABILE = ""
                    ClasseRette.ELEMENTO = "RPX"
                    ClasseRette.Giorni = M1GiornoCalcolati
                    ClasseRette.Importo = Modulo.MathRound(QuoteGiornaliere(Cserv, CodOsp, "O", 0, DateSerial(xAnno1, xMese1, GiorniMese(xMese1, xAnno1))) * M1GiornoCalcolati, 2)
                    ClasseRette.MESECOMPETENZA = xMese1
                    ClasseRette.ANNOCOMPETENZA = xAnno1
                    If MScriviOspite = True Then ClasseRette.ScriviRettaOspite()

                End If

                'Val(CampoOspite(CodOsp, "MesiAnticipo", False))
                If LO_CodiceOspite.MesiAnticipo = 3 And _
                      Mese = Month(DataAcco) And Anno = Year(DataAcco) Then
                    ClasseRette.Pulisci()
                    ClasseRette.CentroServizio = Cserv
                    ClasseRette.CodiceOspite = CodOsp
                    ClasseRette.Mese = Mese
                    ClasseRette.Anno = Anno
                    ClasseRette.STATOCONTABILE = ""
                    ClasseRette.ELEMENTO = "RPX"
                    ClasseRette.Giorni = GiornoCalcolati
                    ClasseRette.Importo = Modulo.MathRound(Calcola, 2)
                    ClasseRette.MESECOMPETENZA = xMese
                    ClasseRette.ANNOCOMPETENZA = xAnno
                    If MScriviOspite = True Then ClasseRette.ScriviRettaOspite()

                    ClasseRette.Pulisci()
                    ClasseRette.CentroServizio = Cserv
                    ClasseRette.CodiceOspite = CodOsp
                    ClasseRette.Mese = Mese
                    ClasseRette.Anno = Anno
                    ClasseRette.STATOCONTABILE = ""
                    ClasseRette.ELEMENTO = "RPX"
                    ClasseRette.Giorni = M1GiornoCalcolati
                    ClasseRette.Importo = Modulo.MathRound(QuoteGiornaliere(Cserv, CodOsp, "O", 0, DateSerial(xAnno1, xMese1, GiorniMese(xMese1, xAnno1))) * M1GiornoCalcolati, 2)
                    ClasseRette.MESECOMPETENZA = xMese1
                    ClasseRette.ANNOCOMPETENZA = xAnno1
                    If MScriviOspite = True Then ClasseRette.ScriviRettaOspite()

                    ClasseRette.Pulisci()
                    ClasseRette.CentroServizio = Cserv
                    ClasseRette.CodiceOspite = CodOsp
                    ClasseRette.Mese = Mese
                    ClasseRette.Anno = Anno
                    ClasseRette.STATOCONTABILE = ""
                    ClasseRette.ELEMENTO = "RPX"
                    ClasseRette.Giorni = M2GiornoCalcolati
                    ClasseRette.Importo = Modulo.MathRound(QuoteGiornaliere(Cserv, CodOsp, "O", 0, DateSerial(xAnno2, xMese2, GiorniMese(xMese2, xAnno2))) * M2GiornoCalcolati, 2)
                    ClasseRette.MESECOMPETENZA = xMese2
                    ClasseRette.ANNOCOMPETENZA = xAnno2
                    If MScriviOspite = True Then ClasseRette.ScriviRettaOspite()
                End If
            End If


            If Calcola2 > 0 Then
                'Val(CampoOspite(CodOsp, "MesiAnticipo", False)) 
                If LO_CodiceOspite.MesiAnticipo < 2 Then
                    ClasseRette.Pulisci()
                    ClasseRette.CentroServizio = Cserv
                    ClasseRette.CodiceOspite = CodOsp
                    ClasseRette.Mese = Mese
                    ClasseRette.Anno = Anno
                    ClasseRette.STATOCONTABILE = ""
                    ClasseRette.ELEMENTO = "R2X"
                    ClasseRette.Giorni = GiornoCalcolati
                    ClasseRette.Importo = Modulo.MathRound(Calcola2, 2)
                    ClasseRette.MESECOMPETENZA = xMese
                    ClasseRette.ANNOCOMPETENZA = xAnno
                    If MScriviOspite = True Then ClasseRette.ScriviRettaOspite()
                End If

            End If
        End If

        For Ext = 0 To 10


            If ImpExtraOspite(Ext) <> 0 Then
                ClasseRette.Pulisci()
                ClasseRette.CentroServizio = Cserv
                ClasseRette.CodiceOspite = CodOsp
                ClasseRette.Mese = Mese
                ClasseRette.Anno = Anno
                ClasseRette.STATOCONTABILE = "A"
                ClasseRette.ELEMENTO = "E" & Format(Ext, "00")
                ClasseRette.Giorni = MyTabCodiceExtrOspiteGIORNI(Ext)
                ClasseRette.Importo = Modulo.MathRound(ImpExtraOspite(Ext), 4)
                ClasseRette.CODICEEXTRA = MyTabCodiceExtrOspite(Ext)
                If MScriviOspite = True Then ClasseRette.ScriviRettaOspite()
            End If

            If NonImpExtraOspite(Ext) <> 0 Then
                ClasseRette.Pulisci()
                ClasseRette.CentroServizio = Cserv
                ClasseRette.CodiceOspite = CodOsp
                ClasseRette.Mese = Mese
                ClasseRette.Anno = Anno
                ClasseRette.STATOCONTABILE = "N"
                ClasseRette.ELEMENTO = "E" & Format(Ext, "00")
                ClasseRette.Giorni = MyTabCodiceNonExtrOspiteGIORNI(Ext)
                ClasseRette.Importo = Modulo.MathRound(NonImpExtraOspite(Ext), 4)
                ClasseRette.CODICEEXTRA = MyTabCodiceExtrOspite(Ext)
                If MScriviOspite = True Then ClasseRette.ScriviRettaOspite()
            End If
        Next Ext

        For I = 1 To MaxAddebitoOspite
            If MyTabAddebitoOspite(I) <> 0 Then

                ClasseRette.Pulisci()
                ClasseRette.CentroServizio = Cserv
                ClasseRette.CodiceOspite = CodOsp
                ClasseRette.Mese = Mese
                ClasseRette.Anno = Anno

                ClasseRette.STATOCONTABILE = "N"
                ClasseRette.ELEMENTO = "ADD"
                ClasseRette.Giorni = MyTabAddebitoOspiteQty(I)
                ClasseRette.Importo = Modulo.MathRound(MyTabAddebitoOspite(I), 2)

                If MyTabAddebitoMeseCompetenza(I) > 0 And MyTabAddebitoAnnoCompetenza(I) > 0 Then
                    ClasseRette.MESECOMPETENZA = MyTabAddebitoMeseCompetenza(I)
                    ClasseRette.ANNOCOMPETENZA = MyTabAddebitoAnnoCompetenza(I)
                Else
                    ClasseRette.MESECOMPETENZA = 0
                    ClasseRette.ANNOCOMPETENZA = 0
                End If
                ClasseRette.Descrizione = MyTabDescrizioneAddebitoOspite(I)

                If MyTabCodiceIvaAddebitoOspite(I) = "" Then
                    Dim Ospite As New ClsOspite

                    Ospite.CodiceOspite = CodOsp
                    Ospite.Leggi(STRINGACONNESSIONEDB, CodOsp)

                    Dim OspiteCsver As New Cls_DatiOspiteParenteCentroServizio

                    OspiteCsver.CodiceOspite = CodOsp
                    OspiteCsver.CentroServizio = Cserv
                    OspiteCsver.CodiceParente = 0
                    OspiteCsver.Leggi(STRINGACONNESSIONEDB)
                    If OspiteCsver.CodiceOspite > 0 Then
                        Ospite.TIPOOPERAZIONE = OspiteCsver.TipoOperazione
                    End If

                    Dim TipoOperazione As New Cls_TipoOperazione

                    TipoOperazione.Codice = Ospite.TIPOOPERAZIONE
                    TipoOperazione.Leggi(STRINGACONNESSIONEDB, TipoOperazione.Codice)

                    MyTabCodiceIvaAddebitoOspite(I) = TipoOperazione.TipoAddebitoAccredito
                End If
                ClasseRette.CODICEEXTRA = MyTabCodiceIvaAddebitoOspite(I)
                ClasseRette.NumeroRegistrazione = MyTabAddebitoOspiteRegistrazione(I)

                If MScriviOspite = True Then ClasseRette.ScriviRettaOspite()


                If LO_CodiceOspite.TIPOOPERAZIONE = "" Then
                    StringaDegliErrori = StringaDegliErrori & "Errore non presente il Tipo Operazione x Ospite : " & CodOsp & " - " & CampoOspite(CodOsp, "NOME", False) & vbNewLine
                End If
                If LO_CodiceOspite.CODICEIVA = "" Then
                    StringaDegliErrori = StringaDegliErrori & "Errore non presente il Codice IVA x Ospite : " & CodOsp & " - " & CampoOspite(CodOsp, "NOME", False) & vbNewLine
                End If
            End If
        Next I

        For I = 1 To MaxAccreditoOspite

            If MyTabAccreditoOspite(I) <> 0 Then

                ClasseRette.Pulisci()
                ClasseRette.CentroServizio = Cserv
                ClasseRette.CodiceOspite = CodOsp
                ClasseRette.Mese = Mese
                ClasseRette.Anno = Anno
                ClasseRette.STATOCONTABILE = "N"
                ClasseRette.ELEMENTO = "ACC"
                ClasseRette.Giorni = 0
                ClasseRette.Importo = Modulo.MathRound(MyTabAccreditoOspite(I), 2)
                ClasseRette.Descrizione = MyTabDescrizioneAccreditoOspite(I)
                If MyTabAccreditoMeseCompetenza(I) > 0 And MyTabAccreditoAnnoCompetenza(I) > 0 Then
                    ClasseRette.MESECOMPETENZA = MyTabAccreditoMeseCompetenza(I)
                    ClasseRette.ANNOCOMPETENZA = MyTabAccreditoAnnoCompetenza(I)
                Else
                    ClasseRette.MESECOMPETENZA = 0
                    ClasseRette.ANNOCOMPETENZA = 0
                End If

                If MyTabCodiceIvaAccreditoOspite(I) = "" Then
                    Dim Ospite As New ClsOspite

                    Ospite.CodiceOspite = CodOsp
                    Ospite.Leggi(STRINGACONNESSIONEDB, CodOsp)

                    Dim OspiteCsver As New Cls_DatiOspiteParenteCentroServizio

                    OspiteCsver.CodiceOspite = CodOsp
                    OspiteCsver.CentroServizio = Cserv
                    OspiteCsver.CodiceParente = 0
                    OspiteCsver.Leggi(STRINGACONNESSIONEDB)
                    If OspiteCsver.CodiceOspite > 0 Then
                        Ospite.TIPOOPERAZIONE = OspiteCsver.TipoOperazione
                    End If

                    Dim TipoOperazione As New Cls_TipoOperazione

                    TipoOperazione.Codice = Ospite.TIPOOPERAZIONE
                    TipoOperazione.Leggi(STRINGACONNESSIONEDB, TipoOperazione.Codice)

                    MyTabCodiceIvaAccreditoOspite(I) = TipoOperazione.TipoAddebitoAccredito
                End If
                ClasseRette.CODICEEXTRA = MyTabCodiceIvaAccreditoOspite(I)
                ClasseRette.NumeroRegistrazione = MyTabAccreditoOspiteRegistrazione(I)


                If MScriviOspite = True Then ClasseRette.ScriviRettaOspite()

                If LO_CodiceOspite.TIPOOPERAZIONE = "" Then
                    StringaDegliErrori = StringaDegliErrori & "Errore non presente il Tipo Operazione x Ospite : " & CodOsp & " - " & CampoOspite(CodOsp, "NOME", False) & vbNewLine
                End If
                If LO_CodiceOspite.CODICEIVA = "" Then
                    StringaDegliErrori = StringaDegliErrori & "Errore non presente il Codice IVA x Ospite : " & CodOsp & " -  " & CampoOspite(CodOsp, "NOME", False) & vbNewLine
                End If
            End If
        Next I

        For Par = 1 To 10

            Dim LO_Parente As New Cls_Parenti

            LO_Parente.Leggi(STRINGACONNESSIONEDB, CodOsp, Par)


            If Not IsNothing(LO_Parente.Nome) Then

                Dim KCPs As New Cls_DatiOspiteParenteCentroServizio

                KCPs.CentroServizio = Cserv
                KCPs.CodiceOspite = CodOsp
                KCPs.CodiceParente = Par
                KCPs.Leggi(STRINGACONNESSIONEDB)

                REM Assegna la tipologia operazione e l'aliquota prendendo quella relativa al cserv
                If KCPs.CodiceOspite <> 0 Then
                    LO_Parente.TIPOOPERAZIONE = KCPs.TipoOperazione
                    LO_Parente.CODICEIVA = KCPs.AliquotaIva
                    LO_Parente.FattAnticipata = KCPs.Anticipata
                    LO_Parente.MODALITAPAGAMENTO = KCPs.ModalitaPagamento
                    LO_Parente.Compensazione = KCPs.Compensazione
                End If

                If LO_Parente.TIPOOPERAZIONE <> "" Then
                    ImportoRpx = 0
                    ImportoRpx2 = 0
                    ExtraAnticipati = 0

                    If LO_Parente.MesiAnticipo > 1 And M1GiornoCalcolati = 0 Then
                        M1GiornoCalcolati = GiorniNelMese(Cserv, CodOsp, xMese1, xAnno1)
                    End If
                    If LO_Parente.MesiAnticipo > 2 And M2GiornoCalcolati = 0 Then
                        M2GiornoCalcolati = GiorniNelMese(Cserv, CodOsp, xMese2, xAnno2)
                    End If

                    REM If CampoParente(CodOsp, Par, "FattAnticipata", False) = "S" Then




                    Dim cmdRpxP As New OleDbCommand()
                    cmdRpxP.CommandText = "Select sum(Importo) as Importo From RetteParente Where CodiceOspite = " & CodOsp & " And  CodiceParente = " & Par & " And CentroServizio = '" & Cserv & "'  And Elemento = 'RPX' And AnnoCompetenza = " & Anno & " And MeseCompetenza = " & Mese
                    cmdRpxP.Connection = OspitiCon
                    Dim RDRettaRpxP As OleDbDataReader = cmdRpxP.ExecuteReader()
                    If RDRettaRpxP.Read() Then
                        ImportoRpx = campodbN(RDRettaRpxP.Item("Importo"))
                    End If
                    RDRettaRpxP.Close()


                    Dim cmdRpxP2 As New OleDbCommand()
                    cmdRpxP2.CommandText = "Select sum(Importo) as Importo From RetteParente Where CodiceOspite = " & CodOsp & " And  CodiceParente = " & Par & " And CentroServizio = '" & Cserv & "'  And Elemento = 'R2X' And AnnoCompetenza = " & Anno & " And MeseCompetenza = " & Mese
                    cmdRpxP2.Connection = OspitiCon
                    Dim RDRettaRpxP2 As OleDbDataReader = cmdRpxP2.ExecuteReader()
                    If RDRettaRpxP2.Read() Then
                        ImportoRpx2 = campodbN(RDRettaRpxP2.Item("Importo"))
                    End If
                    RDRettaRpxP2.Close()



                    Dim cmdRpxEP As New OleDbCommand()
                    cmdRpxEP.CommandText = "Select sum(Importo) as Importo From RetteParente Where CodiceOspite = " & CodOsp & " And  CodiceParente = " & Par & " And CentroServizio = '" & Cserv & "'  And Elemento Like 'E%' And AnnoCompetenza = " & Anno & " And MeseCompetenza = " & Mese
                    cmdRpxEP.Connection = OspitiCon
                    Dim RDRettaRpxEP As OleDbDataReader = cmdRpxEP.ExecuteReader()
                    If RDRettaRpxEP.Read() Then
                        ExtraAnticipati = campodbN(RDRettaRpxEP.Item("Importo"))
                    End If
                    RDRettaRpxEP.Close()



                    'MyRs.Open("Select * From RetteParente Where CodiceOspite = " & CodOsp & " And CodiceParente = " & Par & " And CentroServizio = '" & Cserv & "' And  Elemento Like 'E%' And MeseCompetenza = " & Mese & " And AnnoCompetenza = " & Anno, OspitiDb, ADODB.CursorTypeEnum.adOpenKeyset, ADODB.LockTypeEnum.adLockOptimistic)
                    'If Not MyRs.EOF Then
                    '    ImportoRpx = ImportoRpx + MoveFromDb(MyRs.Fields("Importo"))
                    'End If
                    'MyRs.Close()

                    REM End If


                    If FattureNC = True Then
                        Dim TipoOperazione As New Cls_TipoOperazione
                        Dim ScorporoIVA As Boolean = False

                        TipoOperazione.Codice = LO_Parente.TIPOOPERAZIONE
                        TipoOperazione.Leggi(STRINGACONNESSIONEDB, TipoOperazione.Codice)

                        If TipoOperazione.SCORPORAIVA = 1 Then
                            ScorporoIVA = True
                        End If

                        ImportoRpx = ImportoDocumentoOspite(CentroServizio.MASTRO, CentroServizio.CONTO, CodOsp * 100 + Par, CentroServizio.CENTROSERVIZIO, Anno, Mese, "", Anno, Mese, 0, TipoOperazione.TipoAddebitoAccredito, ScorporoIVA, ConnessioneTabelle)
                    End If




                    If ImpExtrParMan1(Par) <> 0 Then
                        ClasseRette.Pulisci()
                        ClasseRette.CentroServizio = Cserv
                        ClasseRette.CodiceOspite = CodOsp
                        ClasseRette.CODICEPARENTE = Par
                        ClasseRette.Mese = Mese
                        ClasseRette.Anno = Anno

                        ClasseRette.MESECOMPETENZA = Mese
                        ClasseRette.ANNOCOMPETENZA = Anno


                        If Modulo.MathRound(ImportoPresParente(Com), 2) <> 0 Then
                            ClasseRette.STATOCONTABILE = "A"
                        Else
                            ClasseRette.STATOCONTABILE = "N"
                        End If
                        ClasseRette.ELEMENTO = "ACC"
                        If ImpExtrParMan1(Par) > 0 Then
                            ClasseRette.ELEMENTO = "ADD"
                        End If
                        ClasseRette.Giorni = NumExtrParMan1(Par)
                        ClasseRette.Importo = Math.Abs(Modulo.MathRound(ImpExtrParMan1(Par), 2))
                        ClasseRette.Descrizione = DecodificaTabella("IME", "01")

                        Dim TipoOperazione As New Cls_TipoOperazione

                        TipoOperazione.Codice = LO_Parente.TIPOOPERAZIONE
                        TipoOperazione.Leggi(STRINGACONNESSIONEDB, TipoOperazione.Codice)
                        ClasseRette.CODICEEXTRA = TipoOperazione.TipoAddebitoAccredito

                        If TipoExtrParMan1(Par) <> "" Then
                            ClasseRette.CODICEEXTRA = TipoExtrParMan1(Par)
                        End If

                        Dim kTipoAddebito As New Cls_Addebito
                        kTipoAddebito.Codice = ClasseRette.CODICEEXTRA
                        kTipoAddebito.Leggi(STRINGACONNESSIONEDB, kTipoAddebito.Codice)

                        ClasseRette.Descrizione = kTipoAddebito.Descrizione

                        If MScriviParente = True Then ClasseRette.ScriviRettaParente()

                    End If



                    If ImpExtrParMan2(Par) <> 0 Then
                        ClasseRette.Pulisci()
                        ClasseRette.CentroServizio = Cserv
                        ClasseRette.CodiceOspite = CodOsp
                        ClasseRette.CODICEPARENTE = Par
                        ClasseRette.Mese = Mese
                        ClasseRette.Anno = Anno
                        ClasseRette.MESECOMPETENZA = Mese
                        ClasseRette.ANNOCOMPETENZA = Anno


                        If Modulo.MathRound(ImportoPresParente(Com), 2) <> 0 Then
                            ClasseRette.STATOCONTABILE = "A"
                        Else
                            ClasseRette.STATOCONTABILE = "N"
                        End If
                        ClasseRette.ELEMENTO = "ACC"
                        If ImpExtrParMan2(Par) > 0 Then
                            ClasseRette.ELEMENTO = "ADD"
                        End If
                        ClasseRette.Giorni = NumExtrParMan2(Par)
                        ClasseRette.Importo = Math.Abs(Modulo.MathRound(ImpExtrParMan2(Par), 2))
                        ClasseRette.Descrizione = DecodificaTabella("IME", "02")

                        Dim TipoOperazione As New Cls_TipoOperazione

                        TipoOperazione.Codice = LO_Parente.TIPOOPERAZIONE
                        TipoOperazione.Leggi(STRINGACONNESSIONEDB, TipoOperazione.Codice)
                        ClasseRette.CODICEEXTRA = TipoOperazione.TipoAddebitoAccredito
                        If TipoExtrParMan2(Par) <> "" Then
                            ClasseRette.CODICEEXTRA = TipoExtrParMan2(Par)
                        End If

                        Dim kTipoAddebito As New Cls_Addebito
                        kTipoAddebito.Codice = ClasseRette.CODICEEXTRA
                        kTipoAddebito.Leggi(STRINGACONNESSIONEDB, kTipoAddebito.Codice)

                        ClasseRette.Descrizione = kTipoAddebito.Descrizione

                        If MScriviParente = True Then ClasseRette.ScriviRettaParente()

                    End If


                    If ImpExtrParMan3(Par) <> 0 Then
                        ClasseRette.Pulisci()
                        ClasseRette.CentroServizio = Cserv
                        ClasseRette.CodiceOspite = CodOsp
                        ClasseRette.CODICEPARENTE = Par
                        ClasseRette.Mese = Mese
                        ClasseRette.Anno = Anno

                        ClasseRette.MESECOMPETENZA = Mese
                        ClasseRette.ANNOCOMPETENZA = Anno


                        If Modulo.MathRound(ImportoPresParente(Com), 2) <> 0 Then
                            ClasseRette.STATOCONTABILE = "A"
                        Else
                            ClasseRette.STATOCONTABILE = "N"
                        End If
                        ClasseRette.ELEMENTO = "ACC"
                        If ImpExtrParMan3(Par) > 0 Then
                            ClasseRette.ELEMENTO = "ADD"
                        End If
                        ClasseRette.Giorni = NumExtrParMan3(Par)
                        ClasseRette.Importo = Math.Abs(Modulo.MathRound(ImpExtrParMan3(Par), 2))
                        ClasseRette.Descrizione = DecodificaTabella("IME", "03")

                        Dim TipoOperazione As New Cls_TipoOperazione

                        TipoOperazione.Codice = LO_Parente.TIPOOPERAZIONE
                        TipoOperazione.Leggi(STRINGACONNESSIONEDB, TipoOperazione.Codice)
                        ClasseRette.CODICEEXTRA = TipoOperazione.TipoAddebitoAccredito
                        If TipoExtrParMan3(Par) <> "" Then
                            ClasseRette.CODICEEXTRA = TipoExtrParMan3(Par)
                        End If

                        Dim kTipoAddebito As New Cls_Addebito
                        kTipoAddebito.Codice = ClasseRette.CODICEEXTRA
                        kTipoAddebito.Leggi(STRINGACONNESSIONEDB, kTipoAddebito.Codice)

                        ClasseRette.Descrizione = kTipoAddebito.Descrizione

                        If MScriviParente = True Then ClasseRette.ScriviRettaParente()

                    End If



                    If ImpExtrParMan4(Par) <> 0 Then
                        ClasseRette.Pulisci()
                        ClasseRette.CentroServizio = Cserv
                        ClasseRette.CodiceOspite = CodOsp
                        ClasseRette.CODICEPARENTE = Par
                        ClasseRette.Mese = Mese
                        ClasseRette.Anno = Anno

                        ClasseRette.MESECOMPETENZA = Mese
                        ClasseRette.ANNOCOMPETENZA = Anno


                        If Modulo.MathRound(ImportoPresParente(Com), 2) <> 0 Then
                            ClasseRette.STATOCONTABILE = "A"
                        Else
                            ClasseRette.STATOCONTABILE = "N"
                        End If
                        ClasseRette.ELEMENTO = "ACC"
                        If ImpExtrParMan4(Par) > 0 Then
                            ClasseRette.ELEMENTO = "ADD"
                        End If

                        ClasseRette.Giorni = NumExtrParMan4(Par)
                        ClasseRette.Importo = Math.Abs(Modulo.MathRound(ImpExtrParMan4(Par), 2))
                        ClasseRette.Descrizione = DecodificaTabella("IME", "04")

                        Dim TipoOperazione As New Cls_TipoOperazione

                        TipoOperazione.Codice = LO_Parente.TIPOOPERAZIONE
                        TipoOperazione.Leggi(STRINGACONNESSIONEDB, TipoOperazione.Codice)
                        ClasseRette.CODICEEXTRA = TipoOperazione.TipoAddebitoAccredito
                        If TipoExtrParMan4(Par) <> "" Then
                            ClasseRette.CODICEEXTRA = TipoExtrParMan4(Par)
                        End If

                        Dim kTipoAddebito As New Cls_Addebito
                        kTipoAddebito.Codice = ClasseRette.CODICEEXTRA
                        kTipoAddebito.Leggi(STRINGACONNESSIONEDB, kTipoAddebito.Codice)

                        ClasseRette.Descrizione = kTipoAddebito.Descrizione

                        If MScriviParente = True Then ClasseRette.ScriviRettaParente()

                    End If


                    If (LO_Parente.FattAnticipata <> "S" Or (ImportoRpx = 0 And ExtraAnticipati = 0)) And BloccaConguagli = 0 Then
                        ' Calcolo "normale"

                        If ImportoPresParente2(Par) <> 0 Then

                            ClasseRette.Pulisci()
                            ClasseRette.CentroServizio = Cserv
                            ClasseRette.CodiceOspite = CodOsp
                            ClasseRette.CODICEPARENTE = Par
                            ClasseRette.Mese = Mese
                            ClasseRette.Anno = Anno
                            ClasseRette.STATOCONTABILE = "A"
                            ClasseRette.ELEMENTO = "R2P"
                            ClasseRette.Giorni = GiorniPresParente(Par)
                            ClasseRette.Importo = Modulo.MathRound(ImportoPresParente2(Par), 2)


                            If MScriviParente = True Then ClasseRette.ScriviRettaParente()
                        End If

                        If GiorniPresParente(Par) <> 0 Or ImportoPresParente(Par) <> 0 Or GiorniPresParente(Par) <> 0 Then

                            ClasseRette.Pulisci()
                            ClasseRette.CentroServizio = Cserv
                            ClasseRette.CodiceOspite = CodOsp
                            ClasseRette.CODICEPARENTE = Par
                            ClasseRette.Mese = Mese
                            ClasseRette.Anno = Anno
                            ClasseRette.STATOCONTABILE = "A"
                            ClasseRette.ELEMENTO = "RGP"
                            ClasseRette.Giorni = GiorniPresParente(Par)
                            ClasseRette.Importo = Modulo.MathRound(ImportoPresParente(Par), 2)


                            If MScriviParente = True Then ClasseRette.ScriviRettaParente()

                        End If


                        If ImportoAssParente2(Par) <> 0 Or GiorniAssParente(Par) <> 0 Then
                            ClasseRette.Pulisci()
                            ClasseRette.CentroServizio = Cserv
                            ClasseRette.CodiceOspite = CodOsp
                            ClasseRette.CODICEPARENTE = Par
                            ClasseRette.Mese = Mese
                            ClasseRette.Anno = Anno
                            ClasseRette.STATOCONTABILE = "A"
                            ClasseRette.ELEMENTO = "R2A"
                            ClasseRette.Giorni = GiorniAssParente(Par)
                            ClasseRette.Importo = Modulo.MathRound(ImportoAssParente2(Par), 2)
                            If MScriviParente = True Then ClasseRette.ScriviRettaParente()

                        End If



                        If GiorniAssParente(Par) <> 0 Or ImportoAssParente(Par) <> 0 Or GiorniAssParente(Par) <> 0 Then
                            ClasseRette.Pulisci()
                            ClasseRette.CentroServizio = Cserv
                            ClasseRette.CodiceOspite = CodOsp
                            ClasseRette.CODICEPARENTE = Par
                            ClasseRette.Mese = Mese
                            ClasseRette.Anno = Anno
                            ClasseRette.STATOCONTABILE = "A"
                            ClasseRette.ELEMENTO = "RGA"
                            ClasseRette.Giorni = GiorniAssParente(Par)
                            ClasseRette.Importo = Modulo.MathRound(ImportoAssParente(Par), 2)
                            If MScriviParente = True Then ClasseRette.ScriviRettaParente()

                        End If

                        If NonImportoPresParente2(Par) <> 0 Then
                            ClasseRette.Pulisci()
                            ClasseRette.CentroServizio = Cserv
                            ClasseRette.CodiceOspite = CodOsp
                            ClasseRette.CODICEPARENTE = Par
                            ClasseRette.Mese = Mese
                            ClasseRette.Anno = Anno
                            ClasseRette.STATOCONTABILE = "N"
                            ClasseRette.ELEMENTO = "R2P"
                            ClasseRette.Giorni = NonGiorniPresParente(Par)
                            ClasseRette.Importo = Modulo.MathRound(NonImportoPresParente2(Par), 2)
                            If MScriviParente = True Then ClasseRette.ScriviRettaParente()
                        End If

                        If NonGiorniPresParente(Par) <> 0 Or NonImportoPresParente(Par) <> 0 Or NonGiorniPresParente(Par) <> 0 Then
                            ClasseRette.Pulisci()
                            ClasseRette.CentroServizio = Cserv
                            ClasseRette.CodiceOspite = CodOsp
                            ClasseRette.CODICEPARENTE = Par
                            ClasseRette.Mese = Mese
                            ClasseRette.Anno = Anno
                            ClasseRette.STATOCONTABILE = "N"
                            ClasseRette.ELEMENTO = "RGP"
                            ClasseRette.Giorni = NonGiorniPresParente(Par)
                            ClasseRette.Importo = Modulo.MathRound(NonImportoPresParente(Par), 2)
                            If MScriviParente = True Then ClasseRette.ScriviRettaParente()
                        End If

                        If NonImportoAssParente(Par) <> 0 Or NonGiorniAssParente(Par) <> 0 Or NonGiorniAssParente(Par) <> 0 Then
                            ClasseRette.Pulisci()
                            ClasseRette.CentroServizio = Cserv
                            ClasseRette.CodiceOspite = CodOsp
                            ClasseRette.CODICEPARENTE = Par
                            ClasseRette.Mese = Mese
                            ClasseRette.Anno = Anno
                            ClasseRette.STATOCONTABILE = "N"
                            ClasseRette.ELEMENTO = "RGA"
                            ClasseRette.Giorni = NonGiorniAssParente(Par)
                            ClasseRette.Importo = Modulo.MathRound(NonImportoAssParente(Par), 2)
                            If MScriviParente = True Then ClasseRette.ScriviRettaParente()
                        End If

                        If NonImportoAssParente2(Par) <> 0 Or NonGiorniAssParente(Par) <> 0 Then
                            ClasseRette.Pulisci()
                            ClasseRette.CentroServizio = Cserv
                            ClasseRette.CodiceOspite = CodOsp
                            ClasseRette.CODICEPARENTE = Par
                            ClasseRette.Mese = Mese
                            ClasseRette.Anno = Anno
                            ClasseRette.STATOCONTABILE = "N"
                            ClasseRette.ELEMENTO = "R2A"
                            ClasseRette.Giorni = NonGiorniAssParente(Par)
                            ClasseRette.Importo = Modulo.MathRound(NonImportoAssParente2(Par), 2)
                            If MScriviParente = True Then ClasseRette.ScriviRettaParente()
                        End If


                    Else
                        ' Conguaglio Mese precedente

                        ImportoExtra = 0 'ImpExtraParente(Par, 0) + ImpExtraParente(Par, 1) + ImpExtraParente(Par, 2) + ImpExtraParente(Par, 3) + ImpExtraParente(Par, 4) + ImpExtraParente(Par, 5) + ImpExtraParente(Par, 6) + ImpExtraParente(Par, 7) + ImpExtraParente(Par, 8) + ImpExtraParente(Par, 9) + _
                        'NonImpExtraParente(Par, 0) + NonImpExtraParente(Par, 1) + NonImpExtraParente(Par, 2) + NonImpExtraParente(Par, 3) + NonImpExtraParente(Par, 4) + NonImpExtraParente(Par, 5) + NonImpExtraParente(Par, 6) + NonImpExtraParente(Par, 7) + NonImpExtraParente(Par, 8) + NonImpExtraParente(Par, 9)


                        Differenze = ImportoRpx - (Modulo.MathRound(ImportoPresParente(Par), 2) + Modulo.MathRound(ImportoAssParente(Par), 2) + Modulo.MathRound(NonImportoPresParente(Par), 2) + Modulo.MathRound(NonImportoAssParente(Par) + ImportoExtra, 2))


                        MyRs.Open("Select  Sum(Importo) From ADDACR where  RETTA = 'S' And TipoMov = 'AD' And CodiceOspite = " & CodOsp & " And PARENTE = " & Par & " And CENTROSERVIZIO = '" & Cserv & "' And MeseCompetenza = " & Mese & " And AnnoCompetenza = " & Anno & " And Riferimento = 'P'", OspitiDb, ADODB.CursorTypeEnum.adOpenKeyset, ADODB.LockTypeEnum.adLockOptimistic)
                        Differenze = Differenze + MoveFromDb(MyRs.Fields(0))
                        MyRs.Close()

                        MyRs.Open("Select  Sum(Importo) From ADDACR where RETTA = 'S' And  TipoMov = 'AC' And CodiceOspite = " & CodOsp & " And PARENTE = " & Par & " And CENTROSERVIZIO = '" & Cserv & "' And MeseCompetenza = " & Mese & " And AnnoCompetenza  = " & Anno & " And Riferimento = 'P'", OspitiDb, ADODB.CursorTypeEnum.adOpenKeyset, ADODB.LockTypeEnum.adLockOptimistic)
                        Differenze = Differenze - MoveFromDb(MyRs.Fields(0))
                        MyRs.Close()

                        Differenze = Math.Round(Differenze, 2)

                        If Math.Abs(Differenze) < ConguaglioMinimo Then
                            Differenze = 0
                        End If
                        If BloccaConguagli = 1 Then
                            Differenze = 0
                        End If

                        REM If CampoParente(CodOsp, Par, "FattAnticipata", False) = "S" Then
                        If Differenze < 0 Then
                            MaxAddebitoParente(Par) = MaxAddebitoParente(Par) + 1
                            MyTabAddebitoParente(Par, MaxAddebitoParente(Par)) = MyTabAddebitoParente(Par, MaxAddebitoParente(Par)) + (Differenze * -1)

                            MyTabAddebitoParenteAnnoCompetenza(Par, MaxAddebitoParente(Par)) = Anno
                            MyTabAddebitoParenteMeseCompetenza(Par, MaxAddebitoParente(Par)) = Mese


                        Else
                            MaxAccreditoParente(Par) = MaxAccreditoParente(Par) + 1
                            MyTabAccreditoParente(Par, MaxAccreditoParente(Par)) = MyTabAccreditoParente(Par, MaxAccreditoParente(Par)) + (Differenze)

                            MyTabAccreditoParenteAnnoCompetenza(Par, MaxAccreditoParente(Par)) = Anno
                            MyTabAccreditoParenteMeseCompetenza(Par, MaxAccreditoParente(Par)) = Mese
                        End If

                        REM *************************************CONGUAGLIO SECONDO IMPORTO *************************************
                        Dim TipoOperazione As New Cls_TipoOperazione

                        TipoOperazione.Codice = LO_Parente.TIPOOPERAZIONE
                        TipoOperazione.Leggi(STRINGACONNESSIONEDB, TipoOperazione.Codice)



                        Differenze = ImportoRpx2 - (Modulo.MathRound(ImportoPresParente2(Par), 2) + Modulo.MathRound(ImportoAssParente2(Par), 2) + Modulo.MathRound(NonImportoPresParente2(Par), 2) + Modulo.MathRound(NonImportoAssParente2(Par) + ImportoExtra, 2))



                        If Math.Abs(Differenze) < ConguaglioMinimo Then
                            Differenze = 0
                        End If
                        If BloccaConguagli = 1 Then
                            Differenze = 0
                        End If

                        REM If CampoParente(CodOsp, Par, "FattAnticipata", False) = "S" Then
                        If Differenze < 0 Then
                            MaxAddebitoParente(Par) = MaxAddebitoParente(Par) + 1
                            MyTabAddebitoParente(Par, MaxAddebitoParente(Par)) = MyTabAddebitoParente(Par, MaxAddebitoParente(Par)) + (Differenze * -1)
                            MyTabCodiceIvaAddebitoParente(Par, MaxAddebitoParente(Par)) = TipoOperazione.TipoAddebitoAccredito2
                            MyTabAddebitoParenteAnnoCompetenza(Par, MaxAddebitoParente(Par)) = Anno
                            MyTabAddebitoParenteMeseCompetenza(Par, MaxAddebitoParente(Par)) = Mese

                        Else
                            MaxAccreditoParente(Par) = MaxAccreditoParente(Par) + 1
                            MyTabAccreditoParente(Par, MaxAccreditoParente(Par)) = MyTabAccreditoParente(Par, MaxAccreditoParente(Par)) + (Differenze)
                            MyTabCodiceIvaAccreditoParente(Par, MaxAccreditoParente(Par)) = TipoOperazione.TipoAddebitoAccredito2

                            MyTabAccreditoParenteAnnoCompetenza(Par, MaxAccreditoParente(Par)) = Anno
                            MyTabAccreditoParenteMeseCompetenza(Par, MaxAccreditoParente(Par)) = Mese
                        End If

                    End If

                    'CampoParente(CodOsp, Par, "FattAnticipata", False) 
                    If LO_Parente.FattAnticipata = "S" And OspitePresente(Cserv, CodOsp, Mese, Anno) = True Then
                        ' ********************************************
                        ' * Creazione importo mese successivo

                        Calcola = 0
                        Calcola2 = 0

                        Calcola2P1 = 0
                        Giorni2P1 = 0
                        If OspitePresente(Cserv, CodOsp, xMese, xAnno) = True Then
                            SalvaImportoRegione = 0
                            SalvaImportoComune = 0
                            SalvaImportoOspite = 0
                            SalvaImportoTotale = 0
                            SalvaPercentuale = 0
                            Dim QuotaGiorno1 As Double
                            Dim QuotaGiorno2 As Double
                            Dim VariazioniPresenti As Boolean
                            QuotaGiorno1 = QuoteGiornaliere(Cserv, CodOsp, "P1", Par, DateSerial(xAnno, xMese, I))
                            QuotaGiorno2 = QuoteGiornaliere(Cserv, CodOsp, "P2", Par, DateSerial(xAnno, xMese, I))
                            VariazioniPresenti = VariazioniRettaDopoData(Cserv, CodOsp, DateSerial(xAnno, xMese, 1))
                            Calcola = 0
                            For I = 1 To GiornoCalcolati
                                If VariazioniPresenti = True Then
                                    Dim AppoggioVariabile As Double = QuoteGiornaliere(Cserv, CodOsp, "P1", Par, DateSerial(xAnno, xMese, I))
                                    Calcola = Calcola + AppoggioVariabile
                                    If Math.Round(AppoggioVariabile, 2) <> Math.Round(QuotaGiorno1, 2) And (QuotaGiorno1 > 0 Or Calcola2P1 > 0) And AppoggioVariabile > 0 Then
                                        Calcola2P1 = Calcola2P1 + AppoggioVariabile
                                        Giorni2P1 = Giorni2P1 + 1
                                        QuotaGiorno1 = 0
                                    End If
                                    Calcola2 = Calcola2 + QuoteGiornaliere(Cserv, CodOsp, "P2", Par, DateSerial(xAnno, xMese, I))
                                Else
                                    Calcola = Calcola + QuotaGiorno1
                                    Calcola2 = Calcola2 + QuotaGiorno2
                                End If
                            Next I
                            If QuoteMensile(Cserv, CodOsp, "P", Par, DateSerial(xAnno, xMese, I - 1)) > 0 And Calcola > 0 And GiornoCalcolati = GiorniMese(xMese, xAnno) Then
                                Calcola = QuoteMensile(Cserv, CodOsp, "P", Par, DateSerial(xAnno, xMese, I - 1))  'ImportoMensileParente(Par)
                                Calcola2 = QuoteMensile(Cserv, CodOsp, "P2", Par, DateSerial(xAnno, xMese, I - 1))  'ImportoMensileParente(Par)

                                Calcola2P1 = 0
                                Giorni2P1 = 0
                            Else
                                If ImportoMensileParente(Par) = 0 And Calcola > 0 Then
                                    If QuoteMensile(Cserv, CodOsp, "P", Par, DateSerial(xAnno, xMese, I - 1)) > 0 Then
                                        Calcola = QuoteMensile(Cserv, CodOsp, "P", Par, DateSerial(xAnno, xMese, I - 1))   'ImportoMensileParente(Par)
                                        Calcola2 = QuoteMensile(Cserv, CodOsp, "P2", Par, DateSerial(xAnno, xMese, I - 1))  'ImportoMensileParente(Par)

                                        Calcola2P1 = 0
                                        Giorni2P1 = 0
                                    End If
                                End If
                            End If

                            If GiornoCalcolati = GiorniMese(xMese, xAnno) Then
                                Dim Modalita As New Cls_Modalita

                                Modalita.CentroServizio = Cserv
                                Modalita.CodiceOspite = CodOsp
                                Modalita.UltimaData(STRINGACONNESSIONEDB, Modalita.CodiceOspite, Modalita.CentroServizio)
                                If Modalita.MODALITA = "P" Then
                                    Dim ML As New Cls_ImportoComune

                                    ML.CODICEOSPITE = CodOsp
                                    ML.CENTROSERVIZIO = Cserv
                                    ML.UltimaData(STRINGACONNESSIONEDB, Modalita.CodiceOspite, Modalita.CentroServizio)
                                    If ML.Tipo = "M" Then
                                        Dim QuotaComune As Double
                                        Dim QuotaComuneCal As Double

                                        QuotaComuneCal = QuoteGiornaliere(Cserv, CodOsp, "C", 0, DateSerial(xAnno, xMese, 1)) * GiornoCalcolati

                                        Calcola = Calcola + QuotaComuneCal - ML.Importo

                                        Calcola2P1 = 0
                                        Giorni2P1 = 0
                                    End If
                                End If
                            End If

                            If ForzaRPXParente(Par) > 0 And GiornoCalcolati = GiorniMese(xMese, xAnno) And Calcola > 0 Then
                                Calcola = ForzaRPXParente(Par)

                                Calcola2P1 = 0
                                Giorni2P1 = 0
                            End If
                        End If

                        AnticipoExtraFissiParente(CodOsp, Par, Cserv, Anno, Mese, xAnno, xMese, BloccaConguagli, GiornoCalcolati, MScriviParente, Calcola, FattureNC)


                        If Calcola > 0 Then
                            'Val(CampoParente(CodOsp, Par, "MesiAnticipo", False))
                            If LO_Parente.MesiAnticipo < 2 Then
                                ClasseRette.Pulisci()
                                ClasseRette.CentroServizio = Cserv
                                ClasseRette.CodiceOspite = CodOsp
                                ClasseRette.CODICEPARENTE = Par
                                ClasseRette.Mese = Mese
                                ClasseRette.Anno = Anno
                                ClasseRette.STATOCONTABILE = "A"
                                ClasseRette.ELEMENTO = "RPX"
                                ClasseRette.Giorni = GiornoCalcolati - Giorni2P1
                                ClasseRette.Importo = Modulo.MathRound(Calcola - Calcola2P1, 2)

                                ClasseRette.MESECOMPETENZA = xMese
                                ClasseRette.ANNOCOMPETENZA = xAnno

                                If MScriviParente = True Then ClasseRette.ScriviRettaParente()

                                'For XINDICE = 0 To 10

                                '    If MyTabImpExtrParent(GiorniMese(Mese, Anno), XINDICE) > 0 Or MyTabImpExtrParent(1, XINDICE) > 0 Then

                                '        ClasseRette.Pulisci()
                                '        ClasseRette.CentroServizio = Cserv
                                '        ClasseRette.CodiceOspite = CodOsp
                                '        ClasseRette.CODICEPARENTE = Par
                                '        ClasseRette.Mese = Mese
                                '        ClasseRette.Anno = Anno
                                '        ClasseRette.STATOCONTABILE = ""
                                '        ClasseRette.ELEMENTO = "E" & MyTabCodiceExtrParent(XINDICE)
                                '        ClasseRette.Giorni = 0
                                '        If MyTabImpExtrParent(GiorniMese(Mese, Anno), XINDICE) > 0 Then
                                '            ClasseRette.Importo = MyTabImpExtrParent(GiorniMese(Mese, Anno), XINDICE) * GiorniMese(xMese, xAnno)
                                '        Else
                                '            ClasseRette.Importo = MyTabImpExtrParent(1, XINDICE) * GiorniMese(xMese, xAnno)
                                '        End If
                                '        ClasseRette.MESECOMPETENZA = xMese
                                '        ClasseRette.ANNOCOMPETENZA = xAnno
                                '        If MScriviParente = True Then ClasseRette.ScriviRettaParente()

                                '    End If
                                'Next
                            End If

                            If Calcola2P1 > 0 Then
                                ClasseRette.Pulisci()
                                ClasseRette.CentroServizio = Cserv
                                ClasseRette.CodiceOspite = CodOsp
                                ClasseRette.CODICEPARENTE = Par
                                ClasseRette.Mese = Mese
                                ClasseRette.Anno = Anno
                                ClasseRette.STATOCONTABILE = "A"
                                ClasseRette.ELEMENTO = "RPX"
                                ClasseRette.Giorni = Giorni2P1
                                ClasseRette.Importo = Modulo.MathRound(Calcola2P1, 2)
                                ClasseRette.MESECOMPETENZA = xMese
                                ClasseRette.ANNOCOMPETENZA = xAnno
                                If MScriviParente = True Then ClasseRette.ScriviRettaParente()
                            End If

                            DataAcco = DataAccoglimento(Cserv, CodOsp, Mese, Anno)
                            'CampoParente(CodOsp, Par, "MesiAnticipo", False) 
                            If LO_Parente.MesiAnticipo = 2 And _
                               Mese = Month(DataAcco) And Anno = Year(DataAcco) Then


                                ClasseRette.Pulisci()
                                ClasseRette.CentroServizio = Cserv
                                ClasseRette.CodiceOspite = CodOsp
                                ClasseRette.CODICEPARENTE = Par
                                ClasseRette.Mese = Mese
                                ClasseRette.Anno = Anno
                                ClasseRette.STATOCONTABILE = "A"
                                ClasseRette.ELEMENTO = "RPX"
                                ClasseRette.Giorni = GiornoCalcolati
                                ClasseRette.Importo = Modulo.MathRound(Calcola, 2)

                                ClasseRette.MESECOMPETENZA = xMese
                                ClasseRette.ANNOCOMPETENZA = xAnno
                                If MScriviParente = True Then ClasseRette.ScriviRettaParente()

                                ClasseRette.Pulisci()
                                ClasseRette.CentroServizio = Cserv
                                ClasseRette.CodiceOspite = CodOsp
                                ClasseRette.CODICEPARENTE = Par
                                ClasseRette.Mese = Mese
                                ClasseRette.Anno = Anno
                                ClasseRette.STATOCONTABILE = "A"
                                ClasseRette.ELEMENTO = "RPX"
                                ClasseRette.Giorni = M1GiornoCalcolati
                                ClasseRette.Importo = Modulo.MathRound(QuoteGiornaliere(Cserv, CodOsp, "P", Par, DateSerial(xAnno1, xMese1, 2)) * M1GiornoCalcolati, 2)
                                ClasseRette.MESECOMPETENZA = xMese
                                ClasseRette.ANNOCOMPETENZA = xAnno
                                If MScriviParente = True Then ClasseRette.ScriviRettaParente()

                            End If

                            'CampoParente(CodOsp, Par, "MesiAnticipo", False) = 3
                            If LO_Parente.MesiAnticipo = 3 And _
                               Mese = Month(DataAcco) And Anno = Year(DataAcco) Then
                                ClasseRette.Pulisci()
                                ClasseRette.CentroServizio = Cserv
                                ClasseRette.CodiceOspite = CodOsp
                                ClasseRette.CODICEPARENTE = Par
                                ClasseRette.Mese = Mese
                                ClasseRette.Anno = Anno
                                ClasseRette.STATOCONTABILE = "A"
                                ClasseRette.ELEMENTO = "RPX"
                                ClasseRette.Giorni = GiornoCalcolati
                                ClasseRette.Importo = Modulo.MathRound(Calcola, 2)
                                ClasseRette.MESECOMPETENZA = xMese
                                ClasseRette.ANNOCOMPETENZA = xAnno
                                If MScriviParente = True Then ClasseRette.ScriviRettaParente()

                                ClasseRette.Pulisci()
                                ClasseRette.CentroServizio = Cserv
                                ClasseRette.CodiceOspite = CodOsp
                                ClasseRette.CODICEPARENTE = Par
                                ClasseRette.Mese = Mese
                                ClasseRette.Anno = Anno
                                ClasseRette.STATOCONTABILE = "A"
                                ClasseRette.ELEMENTO = "RPX"
                                ClasseRette.Giorni = M1GiornoCalcolati
                                ClasseRette.Importo = Modulo.MathRound(QuoteGiornaliere(Cserv, CodOsp, "P", Par, DateSerial(xAnno1, xMese1, GiorniMese(xMese1, xAnno1))) * M1GiornoCalcolati, 2)
                                ClasseRette.MESECOMPETENZA = xMese1
                                ClasseRette.ANNOCOMPETENZA = xAnno1
                                If MScriviParente = True Then ClasseRette.ScriviRettaParente()

                                ClasseRette.Pulisci()
                                ClasseRette.CentroServizio = Cserv
                                ClasseRette.CodiceOspite = CodOsp
                                ClasseRette.CODICEPARENTE = Par
                                ClasseRette.Mese = Mese
                                ClasseRette.Anno = Anno
                                ClasseRette.STATOCONTABILE = "A"
                                ClasseRette.ELEMENTO = "RPX"
                                ClasseRette.Giorni = M2GiornoCalcolati
                                ClasseRette.Importo = Modulo.MathRound(QuoteGiornaliere(Cserv, CodOsp, "P", Par, DateSerial(xAnno2, xMese2, GiorniMese(xMese2, xAnno2))) * M2GiornoCalcolati, 2)
                                ClasseRette.MESECOMPETENZA = xMese2
                                ClasseRette.ANNOCOMPETENZA = xAnno2
                                If MScriviParente = True Then ClasseRette.ScriviRettaParente()

                            End If



                            If Calcola2 > 0 Then
                                'Val(CampoParente(CodOsp, Par, "MesiAnticipo", False))
                                If LO_Parente.MesiAnticipo < 2 Then
                                    ClasseRette.Pulisci()
                                    ClasseRette.CentroServizio = Cserv
                                    ClasseRette.CodiceOspite = CodOsp
                                    ClasseRette.CODICEPARENTE = Par
                                    ClasseRette.Mese = Mese
                                    ClasseRette.Anno = Anno
                                    ClasseRette.STATOCONTABILE = "A"
                                    ClasseRette.ELEMENTO = "R2X"
                                    ClasseRette.Giorni = GiornoCalcolati
                                    ClasseRette.Importo = Modulo.MathRound(Calcola2, 2)

                                    ClasseRette.MESECOMPETENZA = xMese
                                    ClasseRette.ANNOCOMPETENZA = xAnno

                                    If MScriviParente = True Then ClasseRette.ScriviRettaParente()

                                End If
                            End If
                        End If
                    End If

                    For Ext = 0 To 20

                        If ImpExtraParente(Par, Ext) <> 0 Then
                            ClasseRette.Pulisci()
                            ClasseRette.CentroServizio = Cserv
                            ClasseRette.CodiceOspite = CodOsp
                            ClasseRette.CODICEPARENTE = Par
                            ClasseRette.Mese = Mese
                            ClasseRette.Anno = Anno
                            ClasseRette.STATOCONTABILE = "A"
                            ClasseRette.ELEMENTO = "E" & Format(Ext, "00")
                            ClasseRette.Giorni = MyTabCodiceExtrParentGIORNI(Par, Ext)
                            ClasseRette.Importo = Modulo.MathRound(ImpExtraParente(Par, Ext), 4)
                            ClasseRette.CODICEEXTRA = MyTabCodiceExtrParent(Ext)

                            If MScriviParente = True Then ClasseRette.ScriviRettaParente()
                        End If
                        If NonImpExtraParente(Par, Ext) <> 0 Then
                            ClasseRette.Pulisci()
                            ClasseRette.CentroServizio = Cserv
                            ClasseRette.CodiceOspite = CodOsp
                            ClasseRette.CODICEPARENTE = Par
                            ClasseRette.Mese = Mese
                            ClasseRette.Anno = Anno
                            ClasseRette.STATOCONTABILE = "N"
                            ClasseRette.ELEMENTO = "E" & Format(Ext, "00")
                            ClasseRette.Giorni = MyTabCodiceExtrParentNonGIORNI(Par, Ext)
                            ClasseRette.Importo = Modulo.MathRound(NonImpExtraParente(Par, Ext), 4)
                            ClasseRette.CODICEEXTRA = MyTabCodiceExtrParent(Ext)

                            If MScriviParente = True Then ClasseRette.ScriviRettaParente()

                        End If
                    Next Ext

                    For I = 1 To MaxAccreditoParente(Par)

                        If MyTabAccreditoParente(Par, I) <> 0 Then
                            ClasseRette.Pulisci()
                            ClasseRette.CentroServizio = Cserv
                            ClasseRette.CodiceOspite = CodOsp
                            ClasseRette.CODICEPARENTE = Par
                            ClasseRette.Mese = Mese
                            ClasseRette.Anno = Anno
                            ClasseRette.STATOCONTABILE = "N"
                            ClasseRette.ELEMENTO = "ACC"
                            ClasseRette.Giorni = 0
                            ClasseRette.Importo = Modulo.MathRound(MyTabAccreditoParente(Par, I), 2)
                            ClasseRette.Descrizione = MyTabDescrizioneAccreditoParente(Par, I)

                            If MyTabAccreditoParenteAnnoCompetenza(Par, I) > 0 And MyTabAccreditoParenteMeseCompetenza(Par, I) > 0 Then
                                ClasseRette.ANNOCOMPETENZA = MyTabAccreditoParenteAnnoCompetenza(Par, I)
                                ClasseRette.MESECOMPETENZA = MyTabAccreditoParenteMeseCompetenza(Par, I) = Mese
                            Else
                                ClasseRette.ANNOCOMPETENZA = 0
                                ClasseRette.MESECOMPETENZA = 0
                            End If

                            If MyTabCodiceIvaAccreditoParente(Par, I) = "" Then
                                Dim Parente As New Cls_Parenti

                                Parente.CodiceOspite = CodOsp
                                Parente.CodiceParente = Par
                                Parente.Leggi(STRINGACONNESSIONEDB, CodOsp, Par)

                                Dim OspiteCsver As New Cls_DatiOspiteParenteCentroServizio

                                OspiteCsver.CodiceOspite = CodOsp
                                OspiteCsver.CentroServizio = Cserv
                                OspiteCsver.CodiceParente = Par
                                OspiteCsver.Leggi(STRINGACONNESSIONEDB)
                                If OspiteCsver.CodiceOspite > 0 Then
                                    Parente.TIPOOPERAZIONE = OspiteCsver.TipoOperazione
                                End If

                                Dim TipoOperazione As New Cls_TipoOperazione

                                TipoOperazione.Codice = Parente.TIPOOPERAZIONE
                                TipoOperazione.Leggi(STRINGACONNESSIONEDB, TipoOperazione.Codice)

                                MyTabCodiceIvaAccreditoParente(Par, I) = TipoOperazione.TipoAddebitoAccredito
                            End If

                            ClasseRette.CODICEEXTRA = MyTabCodiceIvaAccreditoParente(Par, I)
                            ClasseRette.NumeroRegistrazione = MyTabAccreditoParenteRegistrazione(Par, I)
                            If MScriviParente = True Then ClasseRette.ScriviRettaParente()

                        End If
                    Next I
                    For I = 1 To MaxAddebitoParente(Par)
                        If MyTabAddebitoParente(Par, I) <> 0 Then
                            ClasseRette.Pulisci()
                            ClasseRette.CentroServizio = Cserv
                            ClasseRette.CodiceOspite = CodOsp
                            ClasseRette.CODICEPARENTE = Par
                            ClasseRette.Mese = Mese
                            ClasseRette.Anno = Anno
                            ClasseRette.STATOCONTABILE = "N"
                            ClasseRette.ELEMENTO = "ADD"
                            ClasseRette.Giorni = 0
                            ClasseRette.Importo = Modulo.MathRound(MyTabAddebitoParente(Par, I), 2)
                            ClasseRette.NumeroRegistrazione = MyTabAddebitoParenteRegistrazione(Par, I)

                            If MyTabAddebitoParenteAnnoCompetenza(Par, I) > 0 And MyTabAddebitoParenteMeseCompetenza(Par, I) > 0 Then
                                ClasseRette.ANNOCOMPETENZA = MyTabAddebitoParenteAnnoCompetenza(Par, I)
                                ClasseRette.MESECOMPETENZA = MyTabAddebitoParenteMeseCompetenza(Par, I) = Mese
                            Else
                                ClasseRette.MESECOMPETENZA = 0
                                ClasseRette.ANNOCOMPETENZA = 0
                            End If

                            ClasseRette.Descrizione = MyTabDescrizioneAddebitoParente(Par, I)

                            If MyTabCodiceIvaAddebitoParente(Par, I) = "" Then
                                Dim Parente As New Cls_Parenti

                                Parente.CodiceOspite = CodOsp
                                Parente.CodiceParente = Par
                                Parente.Leggi(STRINGACONNESSIONEDB, CodOsp, Par)

                                Dim OspiteCsver As New Cls_DatiOspiteParenteCentroServizio

                                OspiteCsver.CodiceOspite = CodOsp
                                OspiteCsver.CentroServizio = Cserv
                                OspiteCsver.CodiceParente = Par
                                OspiteCsver.Leggi(STRINGACONNESSIONEDB)
                                If OspiteCsver.CodiceOspite > 0 Then
                                    Parente.TIPOOPERAZIONE = OspiteCsver.TipoOperazione
                                End If

                                Dim TipoOperazione As New Cls_TipoOperazione

                                TipoOperazione.Codice = Parente.TIPOOPERAZIONE
                                TipoOperazione.Leggi(STRINGACONNESSIONEDB, TipoOperazione.Codice)

                                MyTabCodiceIvaAddebitoParente(Par, I) = TipoOperazione.TipoAddebitoAccredito
                            End If
                            ClasseRette.CODICEEXTRA = MyTabCodiceIvaAddebitoParente(Par, I)
                            If MScriviParente = True Then ClasseRette.ScriviRettaParente()

                        End If
                    Next I
                End If
            End If
        Next Par

        Dim NienteComune As Boolean

        NienteComune = False
        For Com = 0 To 10

            If Trim(CodiceComune(Com)) <> "" Then
                Dim KCom As New ClsComune

                KCom.Provincia = Mid(CodiceComune(Com), 1, 3)
                KCom.Comune = Mid(CodiceComune(Com) & Space(6), 4, 3)
                KCom.Leggi(STRINGACONNESSIONEDB)

                If KCom.NonInUso = "S" Then
                    MScriviComune = False
                End If


                If KCom.FattAnticipata = "S" Then
                    Dim ImportoComuneRPX As Double
                    Dim Mese1 As Integer = Mese
                    Dim Anno1 As Integer = Anno

                    If Mese > 1 Then
                        Mese1 = Mese1 - 1
                    Else
                        Mese1 = 12
                        Anno1 = Anno1 - 1
                    End If


                    Dim cmdRpxComune As New OleDbCommand()
                    cmdRpxComune.CommandText = "Select sum(importo) From RETTECOMUNE Where CodiceOspite = " & CodOsp & " And CENTROSERVIZIO = '" & Cserv & "' And  Elemento = 'RPX'   And Mese = " & Mese1 & " And Anno= " & Anno1 & " And Provincia = '" & Mid(CodiceComune(Com), 1, 3) & "' And Comune = '" & Mid(CodiceComune(Com) & Space(6), 4, 3) & "'"
                    cmdRpxComune.Connection = OspitiCon
                    Dim RDRpxComune As OleDbDataReader = cmdRpxComune.ExecuteReader()
                    If RDRpxComune.Read() Then
                        If campodbN(RDRpxComune.Item(0)) > 0 Then
                            ImportoComuneRPX = campodbN(RDRpxComune.Item(0))
                        End If
                    End If
                    RDRpxComune.Close()

                    If ImportoComuneRPX > 0 Then
                        ImportoAssComune(Com) = 0
                        ImportoPresComune(Com) = 0
                        NonImportoAssComune(Com) = 0
                        NonImportoPresComune(Com) = 0
                        GiorniPresComune(Com) = 0
                        NonGiorniPresComune(Com) = 0
                        GiorniAssComune(Com) = 0
                        NonGiorniAssComune(Com) = 0
                    End If

                    Dim GiorniComAnticipato As Integer = 0
                    Calcola = 0
                    For I = 1 To GiornoCalcolati
                        Dim Appoggio As Double = QuoteGiornaliere(Cserv, CodOsp, "C", 0, DateSerial(xAnno, xMese, I))
                        If Appoggio > 0 Then
                            GiorniComAnticipato = GiorniComAnticipato + 1
                        End If
                        Calcola = Calcola + Appoggio
                    Next I
                    If Calcola > 0 Then
                        Dim ImpMen As New Cls_ImportoComune

                        ImpMen.CENTROSERVIZIO = Cserv
                        ImpMen.CODICEOSPITE = CodOsp
                        ImpMen.UltimaData(STRINGACONNESSIONEDB, CodOsp, Cserv)
                        If ImpMen.Tipo = "M" Then
                            If GiorniComAnticipato = GiornoCalcolati Then
                                Calcola = ImpMen.Importo
                            End If
                        End If
                    End If


                    Dim Totale As New Cls_rettatotale

                    Totale.CODICEOSPITE = CodOsp
                    Totale.CENTROSERVIZIO = Cserv
                    Totale.RettaTotaleAData(STRINGACONNESSIONEDB, Totale.CODICEOSPITE, Totale.CENTROSERVIZIO, DateSerial(xAnno, xMese, 1))

                    Dim Extra As New Cls_ExtraFisso

                    Extra.CENTROSERVIZIO = Totale.CENTROSERVIZIO
                    Extra.CODICEOSPITE = Totale.CODICEOSPITE
                    Extra.Data = Totale.Data
                    Extra.UltimaDataAData(STRINGACONNESSIONEDB)

                    Dim TipoExtra As New Cls_TipoExtraFisso

                    TipoExtra.CODICEEXTRA = Extra.CODICEEXTRA
                    TipoExtra.Leggi(STRINGACONNESSIONEDB)

                    If TipoExtra.Ripartizione = "C" And TipoExtra.Anticipato = "S" Then
                        If TipoExtra.TipoImporto = "G" Then

                            Dim VerificaExtra As Double
                            Dim cmdRpxO1 As New OleDbCommand()
                            cmdRpxO1.CommandText = "Select * From RETTECOMUNE Where CodiceOspite = " & CodOsp & " And CentroServizio = '" & Cserv & "'  And Elemento Like 'E%' And Mese = " & Mese1 & " And Anno= " & Anno1 & " And Provincia = '" & Mid(CodiceComune(Com), 1, 3) & "' And Comune = '" & Mid(CodiceComune(Com) & Space(6), 4, 3) & "'"
                            cmdRpxO1.Connection = OspitiCon
                            Dim RDRettaRpxO1 As OleDbDataReader = cmdRpxO1.ExecuteReader()
                            If RDRettaRpxO1.Read() Then
                                VerificaExtra = campodb(RDRettaRpxO1.Item("Importo"))
                            End If
                            RDRettaRpxO1.Close()

                            If VerificaExtra <> ImpExtraComune(Com, 0) + NonImpExtraComune(Com, 0) Then
                                If ImpExtraComune(Com, 0) > 0 Then
                                    ImpExtraComune(Com, 0) = ImpExtraComune(Com, 0) - VerificaExtra
                                End If
                                If NonImpExtraComune(Com, 0) > 0 Then
                                    NonImpExtraComune(Com, 0) = NonImpExtraComune(Com, 0) - VerificaExtra
                                End If
                            Else
                                ImpExtraComune(Com, 0) = 0
                                NonImpExtraComune(Com, 0) = 0
                            End If

                            If GiornoCalcolati > 0 Then
                                ClasseRette.Pulisci()
                                ClasseRette.CentroServizio = Cserv
                                ClasseRette.CodiceOspite = CodOsp
                                ClasseRette.CODICEPARENTE = Par
                                ClasseRette.Provincia = Mid(CodiceComune(Com), 1, 3)
                                ClasseRette.Comune = Mid(CodiceComune(Com) & Space(6), 4, 3)
                                ClasseRette.Mese = Mese
                                ClasseRette.Anno = Anno
                                ClasseRette.ANNOCOMPETENZA = xAnno
                                ClasseRette.MESECOMPETENZA = xMese
                                ClasseRette.STATOCONTABILE = "A"
                                ClasseRette.ELEMENTO = "E" & Format(TipoExtra.CODICEEXTRA, "00")
                                ClasseRette.Giorni = GiornoCalcolati
                                ClasseRette.Importo = Modulo.MathRound(TipoExtra.IMPORTO * GiornoCalcolati, 4)
                                ClasseRette.CODICEEXTRA = TipoExtra.CODICEEXTRA
                                If MScriviComune = True Then ClasseRette.ScriviRettaComune()
                            End If
                        End If
                        If TipoExtra.TipoImporto = "M" Then
                            ImpExtraOspite(1) = 0
                            ImpExtraOspite(0) = 0
                            NonImpExtraOspite(1) = 0
                            NonImpExtraOspite(0) = 0

                            If GiornoCalcolati > 0 Then
                                ClasseRette.Pulisci()
                                ClasseRette.CentroServizio = Cserv
                                ClasseRette.CodiceOspite = CodOsp
                                ClasseRette.CODICEPARENTE = Par
                                ClasseRette.Mese = Mese
                                ClasseRette.Anno = Anno
                                ClasseRette.ANNOCOMPETENZA = xAnno
                                ClasseRette.MESECOMPETENZA = xMese
                                ClasseRette.Provincia = Mid(CodiceComune(Com), 1, 3)
                                ClasseRette.Comune = Mid(CodiceComune(Com) & Space(6), 4, 3)
                                ClasseRette.STATOCONTABILE = "A"
                                ClasseRette.ELEMENTO = "E" & Format(TipoExtra.CODICEEXTRA, "00")
                                ClasseRette.Giorni = 1
                                ClasseRette.Importo = Modulo.MathRound(TipoExtra.IMPORTO, 4)
                                ClasseRette.CODICEEXTRA = TipoExtra.CODICEEXTRA
                                If MScriviComune = True Then ClasseRette.ScriviRettaParente()
                            End If
                        End If
                    End If


                    Dim DesComune As New Cls_ImportoComune

                    DesComune.CODICEOSPITE = CodOsp
                    DesComune.CENTROSERVIZIO = Cserv
                    DesComune.UltimaDataAData(STRINGACONNESSIONEDB, DesComune.CODICEOSPITE, DesComune.CENTROSERVIZIO, DateSerial(xAnno, xMese, GiorniMese(xMese, xAnno)))

                    ClasseRette.Pulisci()
                    ClasseRette.CentroServizio = Cserv
                    ClasseRette.CodiceOspite = CodOsp
                    ClasseRette.Provincia = Mid(CodiceComune(Com), 1, 3)
                    ClasseRette.Comune = Mid(CodiceComune(Com) & Space(6), 4, 3)
                    ClasseRette.Mese = Mese
                    ClasseRette.Anno = Anno
                    ClasseRette.MESECOMPETENZA = xMese
                    ClasseRette.ANNOCOMPETENZA = xAnno
                    ClasseRette.STATOCONTABILE = "A"
                    ClasseRette.ELEMENTO = "RPX"
                    ClasseRette.Giorni = GiorniComAnticipato
                    ClasseRette.Importo = Modulo.MathRound(Calcola, 2)
                    ClasseRette.Descrizione = DesComune.DescrizioneRiga
                    If MScriviComune = True Then ClasseRette.ScriviRettaComune()
                End If
            End If




            If Trim(CodiceJolly(Com)) <> "" Then
                Dim KCom As New ClsComune

                KCom.Provincia = Mid(CodiceJolly(Com), 1, 3)
                KCom.Comune = Mid(CodiceJolly(Com) & Space(6), 4, 3)
                KCom.Leggi(STRINGACONNESSIONEDB)

                If KCom.FattAnticipata = "S" Then
                    Dim ImportoJollyRPX As Double
                    Dim Mese1 As Integer = Mese
                    Dim Anno1 As Integer = Anno

                    If Mese > 1 Then
                        Mese1 = Mese1 - 1
                    Else
                        Mese1 = 12
                        Anno1 = Anno1 - 1
                    End If


                    Dim cmdRpxComune As New OleDbCommand()
                    cmdRpxComune.CommandText = "Select sum(importo) From RETTEJOLLY Where CodiceOspite = " & CodOsp & " And CENTROSERVIZIO = '" & Cserv & "' And  Elemento = 'RPX'   And Mese = " & Mese1 & " And Anno= " & Anno1 & " And Provincia = '" & Mid(CodiceJolly(Com), 1, 3) & "' And Comune = '" & Mid(CodiceJolly(Com) & Space(6), 4, 3) & "'"
                    cmdRpxComune.Connection = OspitiCon
                    Dim RDRpxComune As OleDbDataReader = cmdRpxComune.ExecuteReader()
                    If RDRpxComune.Read() Then
                        If campodbN(RDRpxComune.Item(0)) > 0 Then
                            ImportoJollyRPX = campodbN(RDRpxComune.Item(0))
                        End If
                    End If
                    RDRpxComune.Close()

                    If ImportoJollyRPX > 0 Then
                        ImportoAssJolly(Com) = 0
                        ImportoPresJolly(Com) = 0
                        NonImportoAssJolly(Com) = 0
                        NonImportoPresJolly(Com) = 0
                        GiorniPresJolly(Com) = 0
                        NonGiorniPresJolly(Com) = 0
                        GiorniAssJolly(Com) = 0
                        NonGiorniAssJolly(Com) = 0
                    End If

                    Dim GiorniComAnticipato As Integer = 0
                    Calcola = 0
                    For I = 1 To GiornoCalcolati
                        Dim Appoggio As Double = QuoteGiornaliere(Cserv, CodOsp, "J", 0, DateSerial(xAnno, xMese, I))
                        If Appoggio > 0 Then
                            GiorniComAnticipato = GiorniComAnticipato + 1
                        End If
                        Calcola = Calcola + Appoggio
                    Next I
                    If Calcola > 0 Then
                        Dim ImpMen As New Cls_ImportoJolly

                        ImpMen.CENTROSERVIZIO = Cserv
                        ImpMen.CODICEOSPITE = CodOsp
                        ImpMen.UltimaData(STRINGACONNESSIONEDB, CodOsp, Cserv)
                        If ImpMen.Tipo = "M" Then
                            If GiorniComAnticipato = GiornoCalcolati Then
                                Calcola = ImpMen.Importo
                            End If
                        End If
                    End If


                    Dim Totale As New Cls_rettatotale

                    Totale.CODICEOSPITE = CodOsp
                    Totale.CENTROSERVIZIO = Cserv
                    Totale.RettaTotaleAData(STRINGACONNESSIONEDB, Totale.CODICEOSPITE, Totale.CENTROSERVIZIO, DateSerial(xAnno, xMese, 1))

                    Dim Extra As New Cls_ExtraFisso

                    Extra.CENTROSERVIZIO = Totale.CENTROSERVIZIO
                    Extra.CODICEOSPITE = Totale.CODICEOSPITE
                    Extra.Data = Totale.Data
                    Extra.UltimaDataAData(STRINGACONNESSIONEDB)

                    Dim TipoExtra As New Cls_TipoExtraFisso

                    TipoExtra.CODICEEXTRA = Extra.CODICEEXTRA
                    TipoExtra.Leggi(STRINGACONNESSIONEDB)

                    If TipoExtra.Ripartizione = "J" And TipoExtra.Anticipato = "S" Then
                        If TipoExtra.TipoImporto = "G" Then

                            Dim VerificaExtra As Double
                            Dim cmdRpxO1 As New OleDbCommand()
                            cmdRpxO1.CommandText = "Select * From RETTEJOLLY Where CodiceOspite = " & CodOsp & " And CentroServizio = '" & Cserv & "'  And Elemento Like 'E%' And Mese = " & Mese1 & " And Anno= " & Anno1 & " And Provincia = '" & Mid(CodiceComune(Com), 1, 3) & "' And Comune = '" & Mid(CodiceComune(Com) & Space(6), 4, 3) & "'"
                            cmdRpxO1.Connection = OspitiCon
                            Dim RDRettaRpxO1 As OleDbDataReader = cmdRpxO1.ExecuteReader()
                            If RDRettaRpxO1.Read() Then
                                VerificaExtra = campodb(RDRettaRpxO1.Item("Importo"))
                            End If
                            RDRettaRpxO1.Close()

                            If VerificaExtra <> ImpExtraComune(Com, 0) + NonImpExtraComune(Com, 0) Then
                                If ImpExtraJolly(Com, 0) > 0 Then
                                    ImpExtraJolly(Com, 0) = ImpExtraJolly(Com, 0) - VerificaExtra
                                End If
                                If NonImpExtraJolly(Com, 0) > 0 Then
                                    NonImpExtraJolly(Com, 0) = NonImpExtraJolly(Com, 0) - VerificaExtra
                                End If
                            Else
                                ImpExtraJolly(Com, 0) = 0
                                NonImpExtraJolly(Com, 0) = 0
                            End If

                            If GiornoCalcolati > 0 Then
                                ClasseRette.Pulisci()
                                ClasseRette.CentroServizio = Cserv
                                ClasseRette.CodiceOspite = CodOsp
                                ClasseRette.CODICEPARENTE = Par
                                ClasseRette.Provincia = Mid(CodiceJolly(Com), 1, 3)
                                ClasseRette.Comune = Mid(CodiceJolly(Com) & Space(6), 4, 3)
                                ClasseRette.Mese = Mese
                                ClasseRette.Anno = Anno
                                ClasseRette.ANNOCOMPETENZA = xAnno
                                ClasseRette.MESECOMPETENZA = xMese
                                ClasseRette.STATOCONTABILE = "A"
                                ClasseRette.ELEMENTO = "E" & Format(TipoExtra.CODICEEXTRA, "00")
                                ClasseRette.Giorni = GiornoCalcolati
                                ClasseRette.Importo = Modulo.MathRound(TipoExtra.IMPORTO * GiornoCalcolati, 4)
                                ClasseRette.CODICEEXTRA = TipoExtra.CODICEEXTRA
                                If MScriviComune = True Then ClasseRette.ScriviRettaJolly()
                            End If
                        End If
                        If TipoExtra.TipoImporto = "M" Then


                            If GiornoCalcolati > 0 Then
                                ClasseRette.Pulisci()
                                ClasseRette.CentroServizio = Cserv
                                ClasseRette.CodiceOspite = CodOsp
                                ClasseRette.CODICEPARENTE = Par
                                ClasseRette.Mese = Mese
                                ClasseRette.Anno = Anno
                                ClasseRette.ANNOCOMPETENZA = xAnno
                                ClasseRette.MESECOMPETENZA = xMese
                                ClasseRette.Provincia = Mid(CodiceJolly(Com), 1, 3)
                                ClasseRette.Comune = Mid(CodiceJolly(Com) & Space(6), 4, 3)
                                ClasseRette.STATOCONTABILE = "A"
                                ClasseRette.ELEMENTO = "E" & Format(TipoExtra.CODICEEXTRA, "00")
                                ClasseRette.Giorni = 1
                                ClasseRette.Importo = Modulo.MathRound(TipoExtra.IMPORTO, 4)
                                ClasseRette.CODICEEXTRA = TipoExtra.CODICEEXTRA
                                If MScriviComune = True Then ClasseRette.ScriviRettaJolly()
                            End If
                        End If
                    End If


                    Dim DesComune As New Cls_ImportoJolly

                    DesComune.CODICEOSPITE = CodOsp
                    DesComune.CENTROSERVIZIO = Cserv
                    DesComune.UltimaData(STRINGACONNESSIONEDB, DesComune.CODICEOSPITE, DesComune.CENTROSERVIZIO)

                    ClasseRette.Pulisci()
                    ClasseRette.CentroServizio = Cserv
                    ClasseRette.CodiceOspite = CodOsp
                    ClasseRette.Provincia = Mid(CodiceJolly(Com), 1, 3)
                    ClasseRette.Comune = Mid(CodiceJolly(Com) & Space(6), 4, 3)
                    ClasseRette.Mese = Mese
                    ClasseRette.Anno = Anno
                    ClasseRette.MESECOMPETENZA = xMese
                    ClasseRette.ANNOCOMPETENZA = xAnno
                    ClasseRette.STATOCONTABILE = "A"
                    ClasseRette.ELEMENTO = "RPX"
                    ClasseRette.Giorni = GiorniComAnticipato
                    ClasseRette.Importo = Modulo.MathRound(Calcola, 2)
                    ClasseRette.Descrizione = ""
                    If MScriviComune = True Then ClasseRette.ScriviRettaJolly()
                End If
            End If


            If GiorniPresJolly(Com) <> 0 Or Modulo.MathRound(ImportoPresJolly(Com), 2) <> 0 Then
                ClasseRette.Pulisci()
                ClasseRette.CentroServizio = Cserv
                ClasseRette.CodiceOspite = CodOsp
                ClasseRette.Provincia = Mid(CodiceJolly(Com), 1, 3)
                ClasseRette.Comune = Mid(CodiceJolly(Com), 4, 3)
                ClasseRette.Mese = Mese
                ClasseRette.Anno = Anno
                ClasseRette.STATOCONTABILE = "A"
                ClasseRette.ELEMENTO = "RGP"
                ClasseRette.Giorni = GiorniPresJolly(Com)
                ClasseRette.Importo = Modulo.MathRound(ImportoPresJolly(Com), 2)
                ClasseRette.ScriviRettaJolly()


                If Trim(ClasseRette.Provincia) = "" And Trim(ClasseRette.Comune) = "" Then
                    StringaDegliErrori = StringaDegliErrori & "Errore non presente il Jolly su Ospite : " & CodOsp & " - " & CampoOspite(CodOsp, "NOME", False) & vbNewLine
                End If
                NienteComune = True
            End If


            If ImpExtrComMan1(Com) <> 0 Then
                ClasseRette.Pulisci()
                ClasseRette.CentroServizio = Cserv
                ClasseRette.CodiceOspite = CodOsp
                ClasseRette.Provincia = Mid(CodiceComune(Com), 1, 3)
                ClasseRette.Comune = Mid(CodiceComune(Com), 4, 3)
                ClasseRette.Mese = Mese
                ClasseRette.Anno = Anno
                If Modulo.MathRound(ImportoPresComune(Com), 2) <> 0 Then
                    ClasseRette.STATOCONTABILE = "A"
                Else
                    ClasseRette.STATOCONTABILE = "N"
                End If

                ClasseRette.ELEMENTO = "ACC"
                If ImpExtrComMan1(Com) > 0 Then
                    ClasseRette.ELEMENTO = "ADD"
                End If
                ClasseRette.Giorni = NumExtrComMan1(Com)
                ClasseRette.Importo = Math.Abs(Modulo.MathRound(ImpExtrComMan1(Com), 2))
                ClasseRette.Descrizione = DecodificaTabella("IME", "01")

                Dim Comune As New ClsComune
                Comune.Comune = Mid(CodiceComune(Com), 4, 3)
                Comune.Provincia = Mid(CodiceComune(Com), 1, 3)
                Comune.Leggi(STRINGACONNESSIONEDB)

                Dim TipoOperazione As New Cls_TipoOperazione

                TipoOperazione.Codice = Comune.TipoOperazione
                TipoOperazione.Leggi(STRINGACONNESSIONEDB, TipoOperazione.Codice)
                ClasseRette.CODICEEXTRA = TipoOperazione.TipoAddebitoAccredito

                If TipoExtrComMan1(I) <> "" Then
                    ClasseRette.CODICEEXTRA = TipoExtrComMan1(I)

                    Dim kTipoAddebito As New Cls_Addebito
                    kTipoAddebito.Codice = ClasseRette.CODICEEXTRA
                    kTipoAddebito.Leggi(STRINGACONNESSIONEDB, kTipoAddebito.Codice)

                    ClasseRette.Descrizione = kTipoAddebito.Descrizione
                End If


                If MScriviComune = True Then ClasseRette.ScriviRettaComune()

                NienteComune = True
            End If



            If ImpExtrComMan2(Com) <> 0 Then
                ClasseRette.Pulisci()
                ClasseRette.CentroServizio = Cserv
                ClasseRette.CodiceOspite = CodOsp
                ClasseRette.Provincia = Mid(CodiceComune(Com), 1, 3)
                ClasseRette.Comune = Mid(CodiceComune(Com), 4, 3)
                ClasseRette.Mese = Mese
                ClasseRette.Anno = Anno
                If Modulo.MathRound(ImportoPresComune(Com), 2) <> 0 Then
                    ClasseRette.STATOCONTABILE = "A"
                Else
                    ClasseRette.STATOCONTABILE = "N"
                End If
                ClasseRette.ELEMENTO = "ACC"
                If ImpExtrComMan2(Com) > 0 Then
                    ClasseRette.ELEMENTO = "ADD"
                End If

                ClasseRette.Giorni = NumExtrComMan2(Com)
                ClasseRette.Importo = Math.Abs(Modulo.MathRound(ImpExtrComMan2(Com), 2))
                ClasseRette.Descrizione = DecodificaTabella("IME", "02")

                Dim Comune As New ClsComune
                Comune.Comune = Mid(CodiceComune(Com), 4, 3)
                Comune.Provincia = Mid(CodiceComune(Com), 1, 3)
                Comune.Leggi(STRINGACONNESSIONEDB)

                Dim TipoOperazione As New Cls_TipoOperazione

                TipoOperazione.Codice = Comune.TipoOperazione
                TipoOperazione.Leggi(STRINGACONNESSIONEDB, TipoOperazione.Codice)
                ClasseRette.CODICEEXTRA = TipoOperazione.TipoAddebitoAccredito

                If TipoExtrComMan2(I) <> "" Then
                    ClasseRette.CODICEEXTRA = TipoExtrComMan2(I)

                    Dim kTipoAddebito As New Cls_Addebito
                    kTipoAddebito.Codice = ClasseRette.CODICEEXTRA
                    kTipoAddebito.Leggi(STRINGACONNESSIONEDB, kTipoAddebito.Codice)

                    ClasseRette.Descrizione = kTipoAddebito.Descrizione
                End If



                If MScriviComune = True Then ClasseRette.ScriviRettaComune()

                NienteComune = True
            End If



            If ImpExtrComMan3(Com) <> 0 And (Modulo.MathRound(ImportoPresComune(Com), 2) <> 0 Or Modulo.MathRound(NonImportoPresComune(Com), 2) <> 0) Then
                ClasseRette.Pulisci()
                ClasseRette.CentroServizio = Cserv
                ClasseRette.CodiceOspite = CodOsp
                ClasseRette.Provincia = Mid(CodiceComune(Com), 1, 3)
                ClasseRette.Comune = Mid(CodiceComune(Com), 4, 3)
                ClasseRette.Mese = Mese
                ClasseRette.Anno = Anno
                If Modulo.MathRound(ImportoPresComune(Com), 2) <> 0 Then
                    ClasseRette.STATOCONTABILE = "A"
                Else
                    ClasseRette.STATOCONTABILE = "N"
                End If
                ClasseRette.ELEMENTO = "ACC"
                If ImpExtrComMan3(Com) > 0 Then
                    ClasseRette.ELEMENTO = "ADD"
                End If
                ClasseRette.Giorni = NumExtrComMan3(Com)
                ClasseRette.Importo = Math.Abs(Modulo.MathRound(ImpExtrComMan3(Com), 2))
                ClasseRette.Descrizione = DecodificaTabella("IME", "03")

                Dim Comune As New ClsComune
                Comune.Comune = Mid(CodiceComune(Com), 4, 3)
                Comune.Provincia = Mid(CodiceComune(Com), 1, 3)
                Comune.Leggi(STRINGACONNESSIONEDB)

                Dim TipoOperazione As New Cls_TipoOperazione

                TipoOperazione.Codice = Comune.TipoOperazione
                TipoOperazione.Leggi(STRINGACONNESSIONEDB, TipoOperazione.Codice)
                ClasseRette.CODICEEXTRA = TipoOperazione.TipoAddebitoAccredito

                If TipoExtrComMan3(I) <> "" Then
                    ClasseRette.CODICEEXTRA = TipoExtrComMan3(I)

                    Dim kTipoAddebito As New Cls_Addebito
                    kTipoAddebito.Codice = ClasseRette.CODICEEXTRA
                    kTipoAddebito.Leggi(STRINGACONNESSIONEDB, kTipoAddebito.Codice)

                    ClasseRette.Descrizione = kTipoAddebito.Descrizione
                End If



                ClasseRette.ScriviRettaComune()

                NienteComune = True
            End If


            If ImpExtrComMan4(Com) <> 0 And (Modulo.MathRound(ImportoPresComune(Com), 2) <> 0 Or Modulo.MathRound(NonImportoPresComune(Com), 2) <> 0) Then
                ClasseRette.Pulisci()
                ClasseRette.CentroServizio = Cserv
                ClasseRette.CodiceOspite = CodOsp
                ClasseRette.Provincia = Mid(CodiceComune(Com), 1, 3)
                ClasseRette.Comune = Mid(CodiceComune(Com), 4, 3)
                ClasseRette.Mese = Mese
                ClasseRette.Anno = Anno
                If Modulo.MathRound(ImportoPresComune(Com), 2) <> 0 Then
                    ClasseRette.STATOCONTABILE = "A"
                Else
                    ClasseRette.STATOCONTABILE = "N"
                End If
                ClasseRette.ELEMENTO = "ACC"
                If ImpExtrComMan4(Com) > 0 Then
                    ClasseRette.ELEMENTO = "ADD"
                End If
                ClasseRette.Giorni = NumExtrComMan4(Com)
                ClasseRette.Importo = Math.Abs(Modulo.MathRound(ImpExtrComMan4(Com), 2))
                ClasseRette.Descrizione = DecodificaTabella("IME", "04")

                Dim Comune As New ClsComune
                Comune.Comune = Mid(CodiceComune(Com), 4, 3)
                Comune.Provincia = Mid(CodiceComune(Com), 1, 3)
                Comune.Leggi(STRINGACONNESSIONEDB)

                Dim TipoOperazione As New Cls_TipoOperazione

                TipoOperazione.Codice = Comune.TipoOperazione
                TipoOperazione.Leggi(STRINGACONNESSIONEDB, TipoOperazione.Codice)
                ClasseRette.CODICEEXTRA = TipoOperazione.TipoAddebitoAccredito

                If TipoExtrComMan4(I) <> "" Then
                    ClasseRette.CODICEEXTRA = TipoExtrComMan4(I)

                    Dim kTipoAddebito As New Cls_Addebito
                    kTipoAddebito.Codice = ClasseRette.CODICEEXTRA
                    kTipoAddebito.Leggi(STRINGACONNESSIONEDB, kTipoAddebito.Codice)

                    ClasseRette.Descrizione = kTipoAddebito.Descrizione
                End If

                ClasseRette.ScriviRettaComune()

                NienteComune = True
            End If



            If GiorniPresComune(Com) <> 0 Or Modulo.MathRound(ImportoPresComune(Com), 2) <> 0 Then

                Dim DesComune As New Cls_ImportoComune

                DesComune.CODICEOSPITE = CodOsp
                DesComune.CENTROSERVIZIO = Cserv
                DesComune.UltimaDataAData(STRINGACONNESSIONEDB, DesComune.CODICEOSPITE, DesComune.CENTROSERVIZIO, DateSerial(xAnno, xMese, GiorniMese(xMese, xAnno)))


                ClasseRette.Pulisci()
                ClasseRette.CentroServizio = Cserv
                ClasseRette.CodiceOspite = CodOsp
                ClasseRette.Provincia = Mid(CodiceComune(Com), 1, 3)
                ClasseRette.Comune = Mid(CodiceComune(Com), 4, 3)
                ClasseRette.Mese = Mese
                ClasseRette.Anno = Anno
                ClasseRette.STATOCONTABILE = "A"
                ClasseRette.ELEMENTO = "RGP"
                ClasseRette.Giorni = GiorniPresComune(Com)
                ClasseRette.Importo = Modulo.MathRound(ImportoPresComune(Com), 2)
                ClasseRette.Descrizione = DesComune.DescrizioneRiga

                If MScriviComune = True Then ClasseRette.ScriviRettaComune()

                If Trim(ClasseRette.Provincia) = "" And Trim(ClasseRette.Comune) = "" Then
                    StringaDegliErrori = StringaDegliErrori & "Errore non presente il Comune su Ospite : " & CodOsp & " - " & CampoOspite(CodOsp, "NOME", False) & vbNewLine
                End If

                NienteComune = True
            End If


            If GiorniAssJolly(Com) <> 0 Or Modulo.MathRound(ImportoAssJolly(Com), 2) <> 0 Then
                ClasseRette.Pulisci()
                ClasseRette.CentroServizio = Cserv
                ClasseRette.CodiceOspite = CodOsp
                ClasseRette.Provincia = Mid(CodiceJolly(Com), 1, 3)
                ClasseRette.Comune = Mid(CodiceJolly(Com), 4, 3)
                ClasseRette.Mese = Mese
                ClasseRette.Anno = Anno
                ClasseRette.STATOCONTABILE = "A"
                ClasseRette.ELEMENTO = "RGA"
                ClasseRette.Giorni = GiorniAssJolly(Com)
                ClasseRette.Importo = Modulo.MathRound(ImportoAssJolly(Com), 2)
                ClasseRette.ScriviRettaJolly()

                NienteComune = True
            End If


            If GiorniAssComune(Com) <> 0 Or Modulo.MathRound(ImportoAssComune(Com), 2) <> 0 Then
                Dim DesComune As New Cls_ImportoComune

                DesComune.CODICEOSPITE = CodOsp
                DesComune.CENTROSERVIZIO = Cserv
                DesComune.UltimaDataAData(STRINGACONNESSIONEDB, DesComune.CODICEOSPITE, DesComune.CENTROSERVIZIO, DateSerial(xAnno, xMese, GiorniMese(xMese, xAnno)))

                ClasseRette.Pulisci()
                ClasseRette.CentroServizio = Cserv
                ClasseRette.CodiceOspite = CodOsp
                ClasseRette.Provincia = Mid(CodiceComune(Com), 1, 3)
                ClasseRette.Comune = Mid(CodiceComune(Com), 4, 3)
                ClasseRette.Mese = Mese
                ClasseRette.Anno = Anno
                ClasseRette.STATOCONTABILE = "A"
                ClasseRette.ELEMENTO = "RGA"
                ClasseRette.Giorni = GiorniAssComune(Com)
                ClasseRette.Importo = Modulo.MathRound(ImportoAssComune(Com), 2)
                ClasseRette.Descrizione = DesComune.DescrizioneRiga
                If MScriviComune = True Then ClasseRette.ScriviRettaComune()

                If Trim(ClasseRette.Provincia) = "" And Trim(ClasseRette.Comune) = "" Then
                    StringaDegliErrori = StringaDegliErrori & "Errore non presente il Comune su Ospite : " & CodOsp & " - " & CampoOspite(CodOsp, "NOME", False) & vbNewLine
                End If

                NienteComune = True
            End If

            If NonGiorniPresComune(Com) <> 0 Or Modulo.MathRound(NonImportoPresComune(Com), 2) <> 0 Then
                Dim DesComune As New Cls_ImportoComune

                DesComune.CODICEOSPITE = CodOsp
                DesComune.CENTROSERVIZIO = Cserv
                DesComune.UltimaDataAData(STRINGACONNESSIONEDB, DesComune.CODICEOSPITE, DesComune.CENTROSERVIZIO, DateSerial(xAnno, xMese, GiorniMese(xMese, xAnno)))

                ClasseRette.Pulisci()
                ClasseRette.CentroServizio = Cserv
                ClasseRette.CodiceOspite = CodOsp
                ClasseRette.Provincia = Mid(CodiceComune(Com), 1, 3)
                ClasseRette.Comune = Mid(CodiceComune(Com), 4, 3)
                ClasseRette.Mese = Mese
                ClasseRette.Anno = Anno
                ClasseRette.STATOCONTABILE = "N"
                ClasseRette.ELEMENTO = "RGP"
                ClasseRette.Giorni = NonGiorniPresComune(Com)
                ClasseRette.Importo = Modulo.MathRound(NonImportoPresComune(Com), 2)
                ClasseRette.Descrizione = DesComune.DescrizioneRiga
                If MScriviComune = True Then ClasseRette.ScriviRettaComune()

                NienteComune = True
            End If

            If NonGiorniPresJolly(Com) <> 0 Or Modulo.MathRound(NonImportoPresJolly(Com), 2) <> 0 Then
                ClasseRette.Pulisci()
                ClasseRette.CentroServizio = Cserv
                ClasseRette.CodiceOspite = CodOsp
                ClasseRette.Provincia = Mid(CodiceJolly(Com), 1, 3)
                ClasseRette.Comune = Mid(CodiceJolly(Com), 4, 3)
                ClasseRette.Mese = Mese
                ClasseRette.Anno = Anno
                ClasseRette.STATOCONTABILE = "N"
                ClasseRette.ELEMENTO = "RGP"
                ClasseRette.Giorni = NonGiorniPresJolly(Com)
                ClasseRette.Importo = Modulo.MathRound(NonImportoPresJolly(Com), 2)
                ClasseRette.ScriviRettaJolly()

                NienteComune = True
            End If


            If NonGiorniAssComune(Com) <> 0 Or Modulo.MathRound(NonImportoAssComune(Com), 2) <> 0 Then
                Dim DesComune As New Cls_ImportoComune

                DesComune.CODICEOSPITE = CodOsp
                DesComune.CENTROSERVIZIO = Cserv
                DesComune.UltimaDataAData(STRINGACONNESSIONEDB, DesComune.CODICEOSPITE, DesComune.CENTROSERVIZIO, DateSerial(xAnno, xMese, GiorniMese(xMese, xAnno)))

                ClasseRette.Pulisci()
                ClasseRette.CentroServizio = Cserv
                ClasseRette.CodiceOspite = CodOsp
                ClasseRette.Provincia = Mid(CodiceComune(Com), 1, 3)
                ClasseRette.Comune = Mid(CodiceComune(Com), 4, 3)
                ClasseRette.Mese = Mese
                ClasseRette.Anno = Anno
                ClasseRette.STATOCONTABILE = "N"
                ClasseRette.ELEMENTO = "RGA"
                ClasseRette.Giorni = NonGiorniAssComune(Com)
                ClasseRette.Importo = Modulo.MathRound(NonImportoAssComune(Com), 2)
                ClasseRette.Descrizione = DesComune.DescrizioneRiga

                If MScriviComune = True Then ClasseRette.ScriviRettaComune()

                NienteComune = True
            End If



            If NonGiorniAssJolly(Com) <> 0 Or Modulo.MathRound(NonImportoAssJolly(Com), 2) <> 0 Then
                ClasseRette.Pulisci()
                ClasseRette.CentroServizio = Cserv
                ClasseRette.CodiceOspite = CodOsp
                ClasseRette.Provincia = Mid(CodiceJolly(Com), 1, 3)
                ClasseRette.Comune = Mid(CodiceJolly(Com), 4, 3)
                ClasseRette.Mese = Mese
                ClasseRette.Anno = Anno
                ClasseRette.STATOCONTABILE = "N"
                ClasseRette.ELEMENTO = "RGA"
                ClasseRette.Giorni = NonGiorniAssJolly(Com)
                ClasseRette.Importo = Modulo.MathRound(NonImportoAssJolly(Com), 2)
                ClasseRette.ScriviRettaJolly()
                NienteComune = True
            End If

            If ImportoAddebitoPrimoComune <> 0 And Trim(Com) <> "" And Val(Com) <> 0 Then
                ClasseRette.Pulisci()
                ClasseRette.CentroServizio = Cserv
                ClasseRette.CodiceOspite = CodOsp
                ClasseRette.Provincia = Mid(CodiceComune(Com), 1, 3)
                ClasseRette.Comune = Mid(CodiceComune(Com), 4, 3)
                ClasseRette.Mese = Mese
                ClasseRette.Anno = Anno
                If GiorniPresComune(Com) + GiorniAssComune(Com) > NonGiorniPresComune(Com) + NonGiorniAssComune(Com) Then
                    ClasseRette.STATOCONTABILE = "A"
                Else
                    ClasseRette.STATOCONTABILE = "N"
                End If
                ClasseRette.ELEMENTO = "ADD"
                ClasseRette.Giorni = 0
                ClasseRette.Importo = Math.Abs(ImportoAddebitoPrimoComune)
                If ImportoAddebitoPrimoComune < 0 Then
                    ClasseRette.ELEMENTO = "ACC"
                End If
                Dim Comune As New ClsComune
                Comune.Comune = Mid(CodiceComune(Com), 4, 3)
                Comune.Provincia = Mid(CodiceComune(Com), 1, 3)
                Comune.Leggi(STRINGACONNESSIONEDB)

                Dim TipoOperazione As New Cls_TipoOperazione

                TipoOperazione.Codice = Comune.TipoOperazione
                TipoOperazione.Leggi(STRINGACONNESSIONEDB, TipoOperazione.Codice)
                ClasseRette.CODICEEXTRA = TipoOperazione.TipoAddebitoAccredito

                ClasseRette.Descrizione = "Addebito automatico da calcolo"
                If TipoAddebitoPrimoComune <> "" Then
                    ClasseRette.CODICEEXTRA = TipoAddebitoPrimoComune
                    ClasseRette.Descrizione = ""
                End If

                If MScriviComune = True Then ClasseRette.ScriviRettaComune()
                ImportoAddebitoPrimoComune = 0
            End If

            If ImportoAddebitoPrimoJolly > 0 And Trim(Com) <> "" And Val(Com) <> 0 Then
                ClasseRette.Pulisci()
                ClasseRette.CentroServizio = Cserv
                ClasseRette.CodiceOspite = CodOsp
                ClasseRette.Provincia = Mid(CodiceJolly(Com), 1, 3)
                ClasseRette.Comune = Mid(CodiceJolly(Com), 4, 3)
                ClasseRette.Mese = Mese
                ClasseRette.Anno = Anno
                If GiorniPresComune(Com) + GiorniAssJolly(Com) > NonGiorniPresJolly(Com) + NonGiorniAssJolly(Com) Then
                    ClasseRette.STATOCONTABILE = "A"
                Else
                    ClasseRette.STATOCONTABILE = "N"
                End If
                ClasseRette.ELEMENTO = "ADD"
                ClasseRette.Giorni = 0
                ClasseRette.Importo = ImportoAddebitoPrimoJolly



                Dim Comune As New ClsComune
                Comune.Comune = Mid(CodiceJolly(Com), 4, 3)
                Comune.Provincia = Mid(CodiceJolly(Com), 1, 3)
                Comune.Leggi(STRINGACONNESSIONEDB)

                Dim TipoOperazione As New Cls_TipoOperazione

                TipoOperazione.Codice = Comune.TipoOperazione
                TipoOperazione.Leggi(STRINGACONNESSIONEDB, TipoOperazione.Codice)

                If TipoAddebitoPrimoJolly = "" Then
                    ClasseRette.CODICEEXTRA = TipoOperazione.TipoAddebitoAccredito
                Else
                    ClasseRette.CODICEEXTRA = TipoAddebitoPrimoJolly
                End If

                ClasseRette.Descrizione = "Addebito automatico da calcolo"
                ClasseRette.ScriviRettaJolly()
                ImportoAddebitoPrimoJolly = 0
            End If

            For I = 0 To MaxAddebitoComune(Com)

                If MyTabAddebitoComune(Com, I) <> 0 Then
                    ClasseRette.Pulisci()
                    ClasseRette.CentroServizio = Cserv
                    ClasseRette.CodiceOspite = CodOsp
                    ClasseRette.Provincia = Mid(CodiceComune(Com), 1, 3)
                    ClasseRette.Comune = Mid(CodiceComune(Com), 4, 3)
                    ClasseRette.Mese = Mese
                    ClasseRette.Anno = Anno

                    If GiorniPresComune(Com) + GiorniAssComune(Com) > NonGiorniPresComune(Com) + NonGiorniAssComune(Com) Then
                        ClasseRette.STATOCONTABILE = "A"
                    Else
                        ClasseRette.STATOCONTABILE = "N"
                    End If

                    ClasseRette.ELEMENTO = "ADD"
                    ClasseRette.Giorni = MyTabAddebitoQty(Com, I)
                    ClasseRette.Importo = Modulo.MathRound(MyTabAddebitoComune(Com, I), 2)
                    ClasseRette.Descrizione = MyTabDescrizioneAddebitoComune(Com, I)
                    ClasseRette.CODICEEXTRA = MyTabCodiceIvaAddebitoComune(Com, I)

                    If MScriviComune = True Then ClasseRette.ScriviRettaComune()


                    NienteComune = True
                End If
            Next I
            For I = 0 To MaxAccreditoComune(Com)

                If MyTabAccreditoComune(Com, I) <> 0 Then
                    ClasseRette.Pulisci()
                    ClasseRette.CentroServizio = Cserv
                    ClasseRette.CodiceOspite = CodOsp
                    ClasseRette.Provincia = Mid(CodiceComune(Com), 1, 3)
                    ClasseRette.Comune = Mid(CodiceComune(Com), 4, 3)
                    ClasseRette.Mese = Mese
                    ClasseRette.Anno = Anno

                    If GiorniPresComune(Com) + GiorniAssComune(Com) > NonGiorniPresComune(Com) + NonGiorniAssComune(Com) Then
                        ClasseRette.STATOCONTABILE = "A"
                    Else
                        ClasseRette.STATOCONTABILE = "N"
                    End If

                    ClasseRette.ELEMENTO = "ACC"
                    ClasseRette.Giorni = 0
                    ClasseRette.Importo = Modulo.MathRound(MyTabAccreditoComune(Com, I), 2)
                    ClasseRette.Descrizione = MyTabDescrizioneAccreditoComune(Com, I)
                    ClasseRette.CODICEEXTRA = MyTabCodiceIvaAccreditoComune(Com, I)

                    If MScriviComune = True Then ClasseRette.ScriviRettaComune()

                    NienteComune = True
                End If
            Next I



            For I = 0 To MaxAddebitoJolly(Com)

                If MyTabAddebitoJolly(Com, I) <> 0 Then
                    ClasseRette.Pulisci()
                    ClasseRette.CentroServizio = Cserv
                    ClasseRette.CodiceOspite = CodOsp
                    ClasseRette.Provincia = Mid(CodiceJolly(Com), 1, 3)
                    ClasseRette.Comune = Mid(CodiceJolly(Com), 4, 3)
                    ClasseRette.Mese = Mese
                    ClasseRette.Anno = Anno

                    If GiorniPresJolly(Com) + GiorniAssJolly(Com) > NonGiorniPresJolly(Com) + NonGiorniAssJolly(Com) Then
                        ClasseRette.STATOCONTABILE = "A"
                    Else
                        ClasseRette.STATOCONTABILE = "N"
                    End If

                    ClasseRette.ELEMENTO = "ADD"
                    ClasseRette.Giorni = 0
                    ClasseRette.Importo = Modulo.MathRound(MyTabAddebitoJolly(Com, I), 2)
                    ClasseRette.Descrizione = MyTabDescrizioneAddebitoJolly(Com, I)
                    ClasseRette.CODICEEXTRA = MyTabCodiceIvaAddebitoJolly(Com, I)
                    ClasseRette.ScriviRettaJolly()


                    'NienteJolly = True
                End If
            Next I
            For I = 0 To MaxAccreditoJolly(Com)

                If MyTabAccreditoJolly(Com, I) <> 0 Then
                    ClasseRette.Pulisci()
                    ClasseRette.CentroServizio = Cserv
                    ClasseRette.CodiceOspite = CodOsp
                    ClasseRette.Provincia = Mid(CodiceJolly(Com), 1, 3)
                    ClasseRette.Comune = Mid(CodiceJolly(Com), 4, 3)
                    ClasseRette.Mese = Mese
                    ClasseRette.Anno = Anno

                    If GiorniPresJolly(Com) + GiorniAssJolly(Com) > NonGiorniPresJolly(Com) + NonGiorniAssJolly(Com) Then
                        ClasseRette.STATOCONTABILE = "A"
                    Else
                        ClasseRette.STATOCONTABILE = "N"
                    End If

                    ClasseRette.ELEMENTO = "ACC"
                    ClasseRette.Giorni = 0
                    ClasseRette.Importo = Modulo.MathRound(MyTabAccreditoJolly(Com, I), 2)
                    ClasseRette.Descrizione = MyTabDescrizioneAccreditoJolly(Com, I)
                    ClasseRette.CODICEEXTRA = MyTabCodiceIvaAccreditoJolly(Com, I)
                    ClasseRette.ScriviRettaJolly()

                    'NienteJolly = True
                End If
            Next I
        Next Com

        If NienteComune = False And Trim(CodiceComune(1)) <> "" Then

            IntNonAuto = ""
            IntAuto = ""
            For I = 1 To GiorniMese(Mese, Anno)
                If MyTabAutoSufficente(I) = "A" Then
                    IntAuto = "A"
                End If
                If MyTabAutoSufficente(I) = "N" Then
                    IntNonAuto = "N"
                End If
            Next I
            If IntAuto = "A" Then
                ClasseRette.Pulisci()
                ClasseRette.CentroServizio = Cserv
                ClasseRette.CodiceOspite = CodOsp
                ClasseRette.Provincia = Mid(CodiceComune(1), 1, 3)
                ClasseRette.Comune = Mid(CodiceComune(1), 4, 3)
                ClasseRette.Mese = Mese
                ClasseRette.Anno = Anno
                ClasseRette.STATOCONTABILE = "A"
                ClasseRette.ELEMENTO = "RGP"
                ClasseRette.Giorni = 0
                ClasseRette.Importo = 0
                If MScriviComune = True Then ClasseRette.ScriviRettaComune()
            End If
            If IntNonAuto = "N" Then
                ClasseRette.Pulisci()
                ClasseRette.CentroServizio = Cserv
                ClasseRette.CodiceOspite = CodOsp
                ClasseRette.Provincia = Mid(CodiceComune(1), 1, 3)
                ClasseRette.Comune = Mid(CodiceComune(1), 4, 3)
                ClasseRette.Mese = Mese
                ClasseRette.Anno = Anno
                ClasseRette.STATOCONTABILE = "N"
                ClasseRette.ELEMENTO = "RGP"
                ClasseRette.Giorni = 0
                ClasseRette.Importo = 0
                If MScriviComune = True Then ClasseRette.ScriviRettaComune()
            End If

            'Rs_RettaComune.AddNew
            'MoveToDb Rs_RettaComune.Fields("CentroServizio, Cserv
            'MoveToDb Rs_RettaComune.Fields("CodiceOspite, CodOsp
            'MoveToDb Rs_RettaComune.Fields("Provincia, Mid(CodiceComune(1), 1, 3)
            'MoveToDb Rs_RettaComune.Fields("Comune, Mid(CodiceComune(1), 4, 3)
            'MoveToDb Rs_RettaComune.Fields("Mese, Mese
            'MoveToDb Rs_RettaComune.Fields("Anno, Anno
            'MoveToDb Rs_RettaComune.Fields("STATOCONTABILE, "A"
            'MoveToDb Rs_RettaComune.Fields("ELEMENTO, "RGP"
            'MoveToDb Rs_RettaComune.Fields("Giorni, 0
            'MoveToDb Rs_RettaComune.Fields("Importo, 0
            'Rs_RettaComune.Update
        End If



        For Reg = 1 To 10

            If GiorniPresRegione(Reg) <> 0 Or ImportoPresRegione(Reg) <> 0 Then
                ClasseRette.Pulisci()
                ClasseRette.CentroServizio = Cserv
                ClasseRette.CodiceOspite = CodOsp
                ClasseRette.Regione = CodiceRegione(Reg)
                ClasseRette.Mese = Mese
                ClasseRette.Anno = Anno
                ClasseRette.STATOCONTABILE = "A"
                ClasseRette.ELEMENTO = "RGP"
                ClasseRette.Giorni = GiorniPresRegione(Reg)
                ClasseRette.Importo = Modulo.MathRound(ImportoPresRegione(Reg), 2)
                ClasseRette.CODICEEXTRA = TipoImportoPresRegione(Reg)
                If MScriviRegione = True Then ClasseRette.ScriviRettaRegione()

                'Rs_RettaRegione.AddNew
                'MoveToDb Rs_RettaRegione.Fields("CentroServizio, Cserv
                'MoveToDb Rs_RettaRegione.Fields("CodiceOspite, CodOsp
                'MoveToDb Rs_RettaRegione.Fields("Regione, CodiceRegione(Reg)
                'MoveToDb Rs_RettaRegione.Fields("Mese, Mese
                'MoveToDb Rs_RettaRegione.Fields("Anno, Anno
                'MoveToDb Rs_RettaRegione.Fields("STATOCONTABILE, "A"
                'MoveToDb Rs_RettaRegione.Fields("ELEMENTO, "RGP"
                'MoveToDb Rs_RettaRegione.Fields("Giorni, GiorniPresRegione(Reg)
                'MoveToDb Rs_RettaRegione.Fields("Importo, Modulo.MathRound(ImportoPresRegione(Reg), 2)
                'Rs_RettaRegione.Update
            End If

            If ImportoAssRegione(Reg) <> 0 Or GiorniAssRegione(Reg) <> 0 Then
                ClasseRette.Pulisci()
                ClasseRette.CentroServizio = Cserv
                ClasseRette.CodiceOspite = CodOsp
                ClasseRette.Regione = CodiceRegione(Reg)
                ClasseRette.Mese = Mese
                ClasseRette.Anno = Anno
                ClasseRette.STATOCONTABILE = "A"
                ClasseRette.ELEMENTO = "RGA"
                ClasseRette.Giorni = GiorniAssRegione(Reg)
                ClasseRette.Importo = Modulo.MathRound(ImportoAssRegione(Reg), 2)
                ClasseRette.CODICEEXTRA = TipoImportoAssRegione(Reg)
                If MScriviRegione = True Then ClasseRette.ScriviRettaRegione()

                'Rs_RettaRegione.AddNew
                'MoveToDb Rs_RettaRegione.Fields("CentroServizio, Cserv
                'MoveToDb Rs_RettaRegione.Fields("CodiceOspite, CodOsp
                'MoveToDb Rs_RettaRegione.Fields("Regione, CodiceRegione(Reg)
                'MoveToDb Rs_RettaRegione.Fields("Mese, Mese
                'MoveToDb Rs_RettaRegione.Fields("Anno, Anno
                'MoveToDb Rs_RettaRegione.Fields("STATOCONTABILE, "A"
                'MoveToDb Rs_RettaRegione.Fields("ELEMENTO, "RGA"
                'MoveToDb Rs_RettaRegione.Fields("Giorni, GiorniAssRegione(Reg)
                'MoveToDb Rs_RettaRegione.Fields("Importo, Modulo.MathRound(ImportoAssRegione(Reg), 2)
                'Rs_RettaRegione.Update
            End If

            If NonImportoPresRegione(Reg) <> 0 Or NonGiorniPresRegione(Reg) <> 0 Then
                ClasseRette.Pulisci()
                ClasseRette.CentroServizio = Cserv
                ClasseRette.CodiceOspite = CodOsp
                ClasseRette.Regione = CodiceRegione(Reg)
                ClasseRette.Mese = Mese
                ClasseRette.Anno = Anno
                ClasseRette.STATOCONTABILE = "N"
                ClasseRette.ELEMENTO = "RGP"
                ClasseRette.Giorni = NonGiorniPresRegione(Reg)
                ClasseRette.Importo = Modulo.MathRound(NonImportoPresRegione(Reg), 2)
                ClasseRette.CODICEEXTRA = TipoImportoPresRegione(Reg)
                If MScriviRegione = True Then ClasseRette.ScriviRettaRegione()

                'Rs_RettaRegione.AddNew
                'MoveToDb Rs_RettaRegione.Fields("CentroServizio, Cserv
                'MoveToDb Rs_RettaRegione.Fields("CodiceOspite, CodOsp
                'MoveToDb Rs_RettaRegione.Fields("Regione, CodiceRegione(Reg)
                'MoveToDb Rs_RettaRegione.Fields("Mese, Mese
                'MoveToDb Rs_RettaRegione.Fields("Anno, Anno
                'MoveToDb Rs_RettaRegione.Fields("STATOCONTABILE, "N"
                'MoveToDb Rs_RettaRegione.Fields("ELEMENTO, "RGP"
                'MoveToDb Rs_RettaRegione.Fields("Giorni, NonGiorniPresRegione(Reg)
                'MoveToDb Rs_RettaRegione.Fields("Importo, Modulo.MathRound(NonImportoPresRegione(Reg), 2)
                'Rs_RettaRegione.Update
            End If

            If NonImportoAssRegione(Reg) <> 0 Or NonGiorniAssRegione(Reg) <> 0 Then
                ClasseRette.Pulisci()
                ClasseRette.CentroServizio = Cserv
                ClasseRette.CodiceOspite = CodOsp
                ClasseRette.Regione = CodiceRegione(Reg)
                ClasseRette.Mese = Mese
                ClasseRette.Anno = Anno
                ClasseRette.STATOCONTABILE = "N"
                ClasseRette.ELEMENTO = "RGA"
                ClasseRette.Giorni = NonGiorniAssRegione(Reg)
                ClasseRette.Importo = Modulo.MathRound(NonImportoAssRegione(Reg), 2)
                ClasseRette.CODICEEXTRA = TipoImportoAssRegione(Reg)
                If MScriviRegione = True Then ClasseRette.ScriviRettaRegione()

                'Rs_RettaRegione.AddNew
                'MoveToDb Rs_RettaRegione.Fields("CentroServizio, Cserv
                'MoveToDb Rs_RettaRegione.Fields("CodiceOspite, CodOsp
                'MoveToDb Rs_RettaRegione.Fields("Regione, CodiceRegione(Reg)
                'MoveToDb Rs_RettaRegione.Fields("Mese, Mese
                'MoveToDb Rs_RettaRegione.Fields("Anno, Anno
                'MoveToDb Rs_RettaRegione.Fields("STATOCONTABILE, "N"
                'MoveToDb Rs_RettaRegione.Fields("ELEMENTO, "RGA"
                'MoveToDb Rs_RettaRegione.Fields("Giorni, NonGiorniAssRegione(Reg)
                'MoveToDb Rs_RettaRegione.Fields("Importo, Modulo.MathRound(NonImportoAssRegione(Reg), 2)
                'Rs_RettaRegione.Update
            End If

            If ImportoAddebitoPrimoRegione <> 0 And CodiceRegione(Reg) <> "" Then
                ClasseRette.Pulisci()
                ClasseRette.CentroServizio = Cserv
                ClasseRette.CodiceOspite = CodOsp
                ClasseRette.Regione = CodiceRegione(Reg)
                ClasseRette.Mese = Mese
                ClasseRette.Anno = Anno
                ClasseRette.STATOCONTABILE = "N"
                ClasseRette.ELEMENTO = "ADD"
                If ImportoAddebitoPrimoRegione <> 0 Then
                    ClasseRette.ELEMENTO = "ACC"
                End If

                ClasseRette.Giorni = 0
                ClasseRette.Importo = Math.Abs(ImportoAddebitoPrimoRegione)


                Dim Regione As New ClsUSL
                Regione.CodiceRegione = CodiceRegione(Reg)
                Regione.Leggi(STRINGACONNESSIONEDB)

                Dim TipoOperazione As New Cls_TipoOperazione

                TipoOperazione.Codice = Regione.TipoOperazione
                TipoOperazione.Leggi(STRINGACONNESSIONEDB, TipoOperazione.Codice)
                ClasseRette.CODICEEXTRA = TipoOperazione.TipoAddebitoAccredito
                ClasseRette.Descrizione = "Addebito automatico da calcolo"
                If TipoAddebitoPrimoRegione <> "" Then
                    ClasseRette.CODICEEXTRA = TipoAddebitoPrimoRegione
                    ClasseRette.Descrizione = ""
                End If

                If MScriviRegione = True Then ClasseRette.ScriviRettaRegione()
            End If

            For I = 0 To MaxAddebitoRegione(Reg)
                If MyTabAddebitoRegione(Reg, I) <> 0 Then
                    ClasseRette.Pulisci()
                    ClasseRette.CentroServizio = Cserv
                    ClasseRette.CodiceOspite = CodOsp
                    ClasseRette.Regione = CodiceRegione(Reg)
                    ClasseRette.Mese = Mese
                    ClasseRette.Anno = Anno
                    ClasseRette.STATOCONTABILE = "N"
                    ClasseRette.ELEMENTO = "ADD"
                    ClasseRette.Giorni = 0
                    ClasseRette.Importo = Modulo.MathRound(MyTabAddebitoRegione(Reg, I), 2)
                    ClasseRette.Descrizione = MyTabDescrizioneAddebitoRegione(Reg, I)
                    ClasseRette.CODICEEXTRA = MyTabCodiceIvaAddebitoRegione(Reg, I)
                    If MScriviRegione = True Then ClasseRette.ScriviRettaRegione()
                End If
            Next
            For I = 0 To MaxAccreditoRegione(Reg)
                If MyTabAccreditoRegione(Reg, I) <> 0 Then
                    ClasseRette.Pulisci()
                    ClasseRette.CentroServizio = Cserv
                    ClasseRette.CodiceOspite = CodOsp
                    ClasseRette.Regione = CodiceRegione(Reg)
                    ClasseRette.Mese = Mese
                    ClasseRette.Anno = Anno
                    ClasseRette.STATOCONTABILE = "N"
                    ClasseRette.ELEMENTO = "ACC"
                    ClasseRette.Giorni = 0
                    ClasseRette.Importo = Modulo.MathRound(MyTabAccreditoRegione(Reg, I), 2)
                    ClasseRette.Descrizione = MyTabDescrizioneAccreditoRegione(Reg, I)
                    ClasseRette.CODICEEXTRA = MyTabCodiceIvaAccreditoRegione(Reg, I)
                    If MScriviRegione = True Then ClasseRette.ScriviRettaRegione()
                End If

            Next I

        Next Reg

        If GiorniPresEnte <> 0 Or ImportoPresEnte <> 0 Then
            ClasseRette.Pulisci()
            ClasseRette.CentroServizio = Cserv
            ClasseRette.CodiceOspite = CodOsp
            ClasseRette.Mese = Mese
            ClasseRette.Anno = Anno
            ClasseRette.STATOCONTABILE = "A"
            ClasseRette.ELEMENTO = "RGP"
            ClasseRette.Giorni = GiorniPresEnte
            ClasseRette.Importo = Modulo.MathRound(ImportoPresEnte, 2)
            ClasseRette.ScriviRettaEnte()
        End If

        If GiorniAssEnte <> 0 Or ImportoAssEnte <> 0 Then
            ClasseRette.Pulisci()
            ClasseRette.CentroServizio = Cserv
            ClasseRette.CodiceOspite = CodOsp
            ClasseRette.Mese = Mese
            ClasseRette.Anno = Anno
            ClasseRette.STATOCONTABILE = "A"
            ClasseRette.ELEMENTO = "RGA"
            ClasseRette.Giorni = GiorniAssEnte
            ClasseRette.Importo = Modulo.MathRound(ImportoAssEnte, 2)
            ClasseRette.ScriviRettaEnte()
        End If

        If NonGiorniPresEnte <> 0 Or NonImportoPresEnte <> 0 Then
            ClasseRette.Pulisci()
            ClasseRette.CentroServizio = Cserv
            ClasseRette.CodiceOspite = CodOsp
            ClasseRette.Mese = Mese
            ClasseRette.Anno = Anno
            ClasseRette.STATOCONTABILE = "N"
            ClasseRette.ELEMENTO = "RGP"
            ClasseRette.Giorni = NonGiorniPresEnte
            ClasseRette.Importo = Modulo.MathRound(NonImportoPresEnte, 2)
            ClasseRette.ScriviRettaEnte()
        End If
        If NonGiorniAssEnte <> 0 Or NonImportoAssEnte <> 0 Then

            ClasseRette.Pulisci()
            ClasseRette.CentroServizio = Cserv
            ClasseRette.CodiceOspite = CodOsp
            ClasseRette.Mese = Mese
            ClasseRette.Anno = Anno
            ClasseRette.STATOCONTABILE = "N"
            ClasseRette.ELEMENTO = "RGA"
            ClasseRette.Giorni = NonGiorniAssEnte
            ClasseRette.Importo = Modulo.MathRound(NonImportoAssEnte, 2)
            ClasseRette.ScriviRettaEnte()
        End If

        For Ext = 0 To 10


            For Com = 0 To 10
                If ImpExtraComune(Com, Ext) <> 0 Then
                    ClasseRette.Pulisci()
                    ClasseRette.CentroServizio = Cserv
                    ClasseRette.CodiceOspite = CodOsp
                    ClasseRette.Provincia = Mid(CodiceComune(Com), 1, 3)
                    ClasseRette.Comune = Mid(CodiceComune(Com), 4, 3)
                    ClasseRette.Mese = Mese
                    ClasseRette.Anno = Anno
                    ClasseRette.STATOCONTABILE = "A"
                    ClasseRette.ELEMENTO = "E" & Format(Ext, "00")
                    ClasseRette.Giorni = GiorniPresExtraC(Ext)
                    ClasseRette.Importo = Modulo.MathRound(ImpExtraComune(Com, Ext), 4)
                    ClasseRette.CODICEEXTRA = MyTabCodiceExtrComune(Ext)
                    If MScriviComune = True Then ClasseRette.ScriviRettaComune()
                End If
                If NonImpExtraComune(Com, Ext) <> 0 Then
                    ClasseRette.Pulisci()
                    ClasseRette.CentroServizio = Cserv
                    ClasseRette.CodiceOspite = CodOsp
                    ClasseRette.Provincia = Mid(CodiceComune(Com), 1, 3)
                    ClasseRette.Comune = Mid(CodiceComune(Com), 4, 3)
                    ClasseRette.Mese = Mese
                    ClasseRette.Anno = Anno
                    ClasseRette.STATOCONTABILE = "N"
                    ClasseRette.ELEMENTO = "E" & Format(Ext, "00")
                    ClasseRette.Giorni = GiorniPresExtraC(Ext)
                    ClasseRette.Importo = Modulo.MathRound(NonImpExtraComune(Com, Ext), 4)
                    ClasseRette.CODICEEXTRA = MyTabCodiceExtrComune(Ext)
                    If MScriviComune = True Then ClasseRette.ScriviRettaComune()


                End If
            Next Com
        Next Ext
    End Sub

    Public Function ProgressivoADDACC() As Long        

        Dim cmd As New OleDbCommand()
        cmd.CommandText = "Select Max(Progressivo) From ADDACR"
        cmd.Connection = OspitiCon

        Dim RSMax As OleDbDataReader = cmd.ExecuteReader()

        If RSMax.Read() Then
            ProgressivoADDACC = campodbN(RSMax.Item(0)) + 1
        Else
            ProgressivoADDACC = 1
        End If
        RSMax.Close()
    End Function
    Public Sub ConfrontaDati(ByVal Cserv As String, ByVal CodOsp As Long, ByVal Mese As Integer, ByVal Anno As Integer, ByVal CreoAddAccr As Integer, ByVal DataMov As Date, ByVal Tdesc As String)
        Dim Rs_RettaOspite As New ADODB.Recordset
        Dim Rs_RettaParente As New ADODB.Recordset
        Dim Rs_RettaComune As New ADODB.Recordset
        Dim Rs_RettaRegione As New ADODB.Recordset
        Dim Rs_RettaEnte As New ADODB.Recordset
        Dim MySql As String
        Dim Rs_AddebitiAccrediti As New ADODB.Recordset
        Dim tot, TotCal As Double, MeseAnno As String
        Dim I As Integer, Provincia As String, Comune As String
        Dim ImpAdd As Double, ImpAcc As Double
        Dim TotaleExtra As Double, E As Integer, xMese As Integer, xAnno As Long




        xMese = Mese - 1
        xAnno = Anno
        If xMese = 0 Then
            xMese = 12
            xAnno = xAnno - 1
        End If
        tot = 0
        MySql = "Select SUM(IMPORTO) From RetteOspite Where CENTROSERVIZIO = '" & Cserv & "' And CODICEOSPITE = " & CodOsp
        MySql = MySql & " And MeseCompetenza = " & Mese & " And AnnoCompetenza = " & Anno & " And Elemento = 'RPX' AND Elemento <> 'ACC' AND Elemento <> 'ADD'"

        Dim cmdRettaOspite As New OleDbCommand()
        cmdRettaOspite.CommandText = MySql
        cmdRettaOspite.Connection = OspitiCon
        Dim RDRettaOspite As OleDbDataReader = cmdRettaOspite.ExecuteReader()
        If RDRettaOspite.Read() Then
            If campodb(RDRettaOspite.Item(0)) = "" Then
                tot = 0
            Else
                tot = campodb(RDRettaOspite.Item(0))
            End If
        End If
        RDRettaOspite.Close()

        Dim CentroServizio As New Cls_CentroServizio

        CentroServizio.CENTROSERVIZIO = Cserv
        CentroServizio.Leggi(STRINGACONNESSIONEDB, CentroServizio.CENTROSERVIZIO)


        Dim LO_CodiceOspite As New ClsOspite

        LO_CodiceOspite.Leggi(STRINGACONNESSIONEDB, CodOsp)


        Dim KCs As New Cls_DatiOspiteParenteCentroServizio

        KCs.CentroServizio = Cserv
        KCs.CodiceOspite = CodOsp
        KCs.CodiceParente = 0
        KCs.Leggi(STRINGACONNESSIONEDB)

        REM Assegna la tipologia operazione e l'aliquota prendendo quella relativa al cserv
        If KCs.CodiceOspite <> 0 Then
            LO_CodiceOspite.TIPOOPERAZIONE = KCs.TipoOperazione
            LO_CodiceOspite.CODICEIVA = KCs.AliquotaIva
            LO_CodiceOspite.FattAnticipata = KCs.Anticipata
            LO_CodiceOspite.MODALITAPAGAMENTO = KCs.ModalitaPagamento
            LO_CodiceOspite.Compensazione = KCs.Compensazione
            LO_CodiceOspite.SETTIMANA = KCs.Settimana
        End If

        If tot = 0 And LO_CodiceOspite.FattAnticipata = "S" Then
            Dim tipooperazione As New Cls_TipoOperazione

            tipooperazione.Codice = LO_CodiceOspite.TIPOOPERAZIONE
            tipooperazione.Leggi(STRINGACONNESSIONEDB, tipooperazione.Codice)

            tot = ImportoDocumentoOspite(CentroServizio.MASTRO, CentroServizio.CONTO, CodOsp * 100, Cserv, Anno, Mese, "", Anno, Mese, 0, tipooperazione.TipoAddebitoAccredito)

            tot = tot + ImportoDocumentoOspiteExtra(CentroServizio.MASTRO, CentroServizio.CONTO, CodOsp * 100, Cserv, Anno, Mese, "", Anno, Mese, 0, tipooperazione.TipoAddebitoAccredito)
        Else
            MeseAnno = " AnnoCompetenza =" & Anno & " And MeseCompetenza = " & Mese

            MySql = "Select SUM(IMPORTO) From RetteOspite Where CENTROSERVIZIO = '" & Cserv & "' And CODICEOSPITE = " & CodOsp
            MySql = MySql & " And Mese = " & Mese & " And Anno = " & Anno & " And Elemento <> 'RPX' AND Elemento <> 'R2X' AND Elemento <> 'R2P' AND Elemento <> 'ACC' AND Elemento <> 'ADD' "
            Dim cmdRettaOspiteO As New OleDbCommand()
            cmdRettaOspiteO.CommandText = MySql
            cmdRettaOspiteO.Connection = OspitiCon
            Dim RDRettaOspiteO As OleDbDataReader = cmdRettaOspiteO.ExecuteReader()
            If RDRettaOspiteO.Read() Then
                If campodb(RDRettaOspiteO.Item(0)) = "" Then
                    tot = tot + 0
                Else
                    tot = tot + campodb(RDRettaOspiteO.Item(0))
                End If
            End If
            RDRettaOspiteO.Close()


        End If



        MeseAnno = " AnnoCompetenza =" & Anno & " And MeseCompetenza = " & Mese

        TotaleExtra = 0
        For E = 0 To 10 : TotaleExtra = TotaleExtra + ImpExtraOspite(E) + NonImpExtraOspite(E) : Next E


        TotCal = ImportoPresOspite + ImportoAssOspite + NonImportoPresOspite + NonImportoAssOspite + TotaleExtra + ImpExtrOspMan1 + ImpExtrOspMan2 + ImpExtrOspMan3 + ImpExtrOspMan4

        MySql = "Select Sum(IMPORTO) From ADDACR Where CENTROSERVIZIO = '" & Cserv & "' And CODICEOSPITE = " & CodOsp & " And Riferimento = 'O' And TipoMov = 'AD' AND RETTA = 'S' And " & MeseAnno
        Dim cmdAddebitiAccrediti As New OleDbCommand()
        cmdAddebitiAccrediti.CommandText = MySql
        cmdAddebitiAccrediti.Connection = OspitiCon
        Dim RDAddebitiAccrediti As OleDbDataReader = cmdAddebitiAccrediti.ExecuteReader()
        If RDAddebitiAccrediti.Read() Then
            If campodb(RDAddebitiAccrediti.Item(0)) = "" Then
                ImpAdd = Modulo.MathRound(0, 2)
            Else
                ImpAdd = Modulo.MathRound(CDbl(campodb(RDAddebitiAccrediti.Item(0))), 2)
            End If
        End If
        RDAddebitiAccrediti.Close()




        MySql = "Select Sum(IMPORTO) From ADDACR Where CENTROSERVIZIO = '" & Cserv & "' And CODICEOSPITE = " & CodOsp & "  And Riferimento = 'O' And TipoMov = 'AC' AND RETTA = 'S' And " & MeseAnno
        Dim cmdAddebitiAccrediti1 As New OleDbCommand()
        cmdAddebitiAccrediti1.CommandText = MySql
        cmdAddebitiAccrediti1.Connection = OspitiCon
        Dim RDAddebitiAccrediti1 As OleDbDataReader = cmdAddebitiAccrediti1.ExecuteReader()
        If RDAddebitiAccrediti1.Read() Then
            If campodb(RDAddebitiAccrediti1.Item(0)) = "" Then
                ImpAcc = Modulo.MathRound(0, 2)
            Else
                ImpAcc = Modulo.MathRound(CDbl(campodb(RDAddebitiAccrediti1.Item(0))), 2)
            End If
        End If
        RDAddebitiAccrediti1.Close()

        ' CORREZIONE RICERCA CONGUAGLI 
        REM *****************
        'MySql = "Select SUM(IMPORTO) From RetteOspite Where CENTROSERVIZIO = '" & Cserv & "' And CODICEOSPITE = " & CodOsp
        'MySql = MySql & " And Mese = " & Mese & " And Anno = " & Anno & " And Elemento = 'ACC' And (CODICEEXTRA is null or CODICEEXTRA = '')"
        'Dim cmdRettaOspiteACC As New OleDbCommand()
        'cmdRettaOspiteACC.CommandText = MySql
        'cmdRettaOspiteACC.Connection = OspitiCon
        'Dim RDRettaOspiteACC As OleDbDataReader = cmdRettaOspiteACC.ExecuteReader()
        'If RDRettaOspiteACC.Read() Then
        '    If campodb(RDRettaOspiteACC.Item(0)) = "" Then
        '        ImpAcc = 0
        '    Else
        '        ImpAcc = campodb(RDRettaOspiteACC.Item(0))
        '    End If
        'End If
        'RDRettaOspiteACC.Close()


        'MySql = "Select SUM(IMPORTO) From RetteOspite Where CENTROSERVIZIO = '" & Cserv & "' And CODICEOSPITE = " & CodOsp
        'MySql = MySql & " And Mese = " & Mese & " And Anno = " & Anno & " And Elemento = 'ADD'  And (CODICEEXTRA is null or CODICEEXTRA = '')"
        'Dim cmdRettaOspiteADD As New OleDbCommand()
        'cmdRettaOspiteADD.CommandText = MySql
        'cmdRettaOspiteADD.Connection = OspitiCon
        'Dim RDRettaOspiteADD As OleDbDataReader = cmdRettaOspiteADD.ExecuteReader()
        'If RDRettaOspiteADD.Read() Then
        '    If campodb(RDRettaOspiteADD.Item(0)) = "" Then
        '        ImpAdd = 0
        '    Else
        '        ImpAdd = campodb(RDRettaOspiteADD.Item(0))
        '    End If
        'End If
        'RDRettaOspiteADD.Close()
        REM  *** 


        MySql = "Select SUM(IMPORTO) From RetteOspite Where CENTROSERVIZIO = '" & Cserv & "' And CODICEOSPITE = " & CodOsp
        MySql = MySql & " And MeseCompetenza  = " & Mese & " And AnnoCompetenza  = " & Anno & " And Elemento = 'ACC'"
        Dim cmdRettaOspiteACC As New OleDbCommand()
        cmdRettaOspiteACC.CommandText = MySql
        cmdRettaOspiteACC.Connection = OspitiCon
        Dim RDRettaOspiteACC As OleDbDataReader = cmdRettaOspiteACC.ExecuteReader()
        If RDRettaOspiteACC.Read() Then
            If campodb(RDRettaOspiteACC.Item(0)) = "" Then
                ImpAcc = ImpAcc + 0
            Else
                ImpAcc = ImpAcc + campodb(RDRettaOspiteACC.Item(0))
            End If
        End If
        RDRettaOspiteACC.Close()

        MySql = "Select SUM(IMPORTO) From RetteOspite Where CENTROSERVIZIO = '" & Cserv & "' And CODICEOSPITE = " & CodOsp
        MySql = MySql & " And MeseCompetenza  = " & Mese & " And AnnoCompetenza  = " & Anno & " And Elemento = 'ADD'"
        Dim cmdRettaOspiteADD As New OleDbCommand()
        cmdRettaOspiteADD.CommandText = MySql
        cmdRettaOspiteADD.Connection = OspitiCon
        Dim RDRettaOspiteADD As OleDbDataReader = cmdRettaOspiteADD.ExecuteReader()
        If RDRettaOspiteADD.Read() Then
            If campodb(RDRettaOspiteADD.Item(0)) = "" Then
                ImpAdd = ImpAdd + 0
            Else
                ImpAdd = ImpAdd + campodb(RDRettaOspiteADD.Item(0))
            End If
        End If
        RDRettaOspiteADD.Close()


        tot = tot + ImpAdd - ImpAcc
        If Val(CampoParametri("EmissioneOspite")) = 1 Then
            tot = Modulo.MathRound(tot, 2)
            REM MySql = "SELECT * FROM MovimentiContabiliRiga INNER JOIN MovimentiContabiliTesta ON MovimentiContabiliRiga.Numero = MovimentiContabiliTesta.NumeroRegistrazione Where MastroPartita = " & MastroOspite(Cserv, CodOsp) & " And ContoPartita = " & ContoOspite(Cserv, CodOsp) & " And SottoContoPartita = " & SottoContoOspite(Cserv, CodOsp) & " And AnnoCompetenza = " & Anno & " And MeseCompetenza = " & Mese & " And Tipologia Is Null And Tipo = 'CF'"
            REM Rs_RettaOspite.Open MySql, GeneraleDb, ADODB.CursorTypeEnum.adOpenKeyset, ADODB.LockTypeEnum.adLockOptimistic
            REM Do While Not Rs_RettaOspite.EOF
            REM    tot = tot + MoveFromDb(Rs_RettaOspite.Fields("Importo"))
            REM    Rs_RettaOspite.MoveNext
            REM Loop
            REM Rs_RettaOspite.Close
        End If

        If Modulo.MathRound(TotCal, 2) <> Modulo.MathRound(tot, 2) Then
            If 2 = 2 Then
                StringaDegliErrori = StringaDegliErrori & "Periodo " & Mese & "/" & Anno & "  Importo Ospite Diverso per Euro " & vbTab & Modulo.MathRound(TotCal - tot, 2) & vbTab & "Codice Servizio : " & Cserv & " Ospite : " & CodOsp & " - " & CampoOspite(CodOsp, "NOME", False) & vbNewLine
            Else
                StringaDegliErrori = StringaDegliErrori & "Periodo " & Mese & "/" & Anno & "  Importo Ospite Diverso per Lire " & vbTab & Modulo.MathRound(TotCal - tot, 2) & vbTab & "Codice Servizio : " & Cserv & " Ospite : " & CodOsp & " - " & CampoOspite(CodOsp, "NOME", False) & vbNewLine
            End If


            IndiceVariazione = IndiceVariazione + 1

            RicalcoloGriglia_CServ(IndiceVariazione) = Cserv
            RicalcoloGriglia_CodiceOspite(IndiceVariazione) = CodOsp
            RicalcoloGriglia_Anno(IndiceVariazione) = Anno
            RicalcoloGriglia_Mese(IndiceVariazione) = Mese
            RicalcoloGriglia_Ospite(IndiceVariazione) = Modulo.MathRound(TotCal - tot, 2)
            RicalcoloGriglia_Parente_id(IndiceVariazione) = 0
            RicalcoloGriglia_Parente(IndiceVariazione) = 0
            RicalcoloGriglia_Comune(IndiceVariazione) = 0
            RicalcoloGriglia_Jolly(IndiceVariazione) = 0
            RicalcoloGriglia_Regione(IndiceVariazione) = 0
            RicalcoloGriglia_Descrizione(IndiceVariazione) = ""


            If CreoAddAccr = 1 Then

                Dim InsAddAcc As New Cls_AddebitiAccrediti

                InsAddAcc.CENTROSERVIZIO = Cserv
                InsAddAcc.CodiceOspite = CodOsp
                InsAddAcc.Descrizione = Tdesc
                InsAddAcc.ANNOCOMPETENZA = Anno
                InsAddAcc.MESECOMPETENZA = Mese
                InsAddAcc.CodiceIva = CampoParametri("NCDeposito")

                Dim Ospite As New ClsOspite

                Ospite.CodiceOspite = CodOsp
                Ospite.Leggi(STRINGACONNESSIONEDB, CodOsp)

                Dim Dati As New Cls_DatiOspiteParenteCentroServizio

                Dati.CodiceOspite = CodOsp
                Dati.CentroServizio = Cserv
                Dati.CodiceParente = 0
                Dati.Leggi(STRINGACONNESSIONEDB)
                If Dati.CodiceOspite <> 0 Then
                    Ospite.TIPOOPERAZIONE = Dati.TipoOperazione
                End If

                Dim TipoOperazione As New Cls_TipoOperazione

                TipoOperazione.Codice = Ospite.TIPOOPERAZIONE
                TipoOperazione.Leggi(STRINGACONNESSIONEDB, TipoOperazione.Codice)

                If TipoOperazione.TipoAddebitoAccredito <> "" Then
                    InsAddAcc.CodiceIva = TipoOperazione.TipoAddebitoAccredito
                End If

                If TotCal - tot > 0 Then
                    InsAddAcc.TipoMov = "AD"
                Else
                    InsAddAcc.TipoMov = "AC"
                End If
                InsAddAcc.RETTA = "S"
                InsAddAcc.RIFERIMENTO = "O"
                InsAddAcc.IMPORTO = Modulo.MathRound(Math.Abs(TotCal - tot), 2)
                InsAddAcc.PROVINCIA = ""
                InsAddAcc.COMUNE = ""
                InsAddAcc.PARENTE = 0
                InsAddAcc.Data = Format(DataMov, "dd/MM/yyyy")
                InsAddAcc.Progressivo = ProgressivoADDACC()
                InsAddAcc.Utente = "EmissioneAddebitiAccrediti"
                InsAddAcc.InserisciAddebito(STRINGACONNESSIONEDB)

                StringaDegliErrori = StringaDegliErrori & "Periodo " & Mese & "/" & Anno & "  Creato Movimento numero : " & InsAddAcc.Progressivo & vbNewLine
                RicalcoloGriglia_NumeroMovimento(IndiceVariazione) = InsAddAcc.Progressivo


                'Rs_AddebitiAccrediti.Open("Select * From ADDACR Where CodiceOspite  = 0", OspitiDb, ADODB.CursorTypeEnum.adOpenKeyset, ADODB.LockTypeEnum.adLockOptimistic)
                'Rs_AddebitiAccrediti.AddNew()

                'MoveToDb(Rs_AddebitiAccrediti.Fields("CentroServizio"), Cserv)
                'MoveToDb(Rs_AddebitiAccrediti.Fields("CodiceOspite"), CodOsp)
                'MoveToDb(Rs_AddebitiAccrediti.Fields("Descrizione"), Tdesc) '"Movimento Automatico"
                'MoveToDb(Rs_AddebitiAccrediti.Fields("ANNOCOMPETENZA"), Anno)
                'MoveToDb(Rs_AddebitiAccrediti.Fields("MESECOMPETENZA"), Mese)

                'MoveToDb(Rs_AddebitiAccrediti.Fields("CodiceIva"), CampoParametri("NCDeposito")) ' VARIAZIONI RETTA

                'If TotCal - tot > 0 Then
                '    MoveToDb(Rs_AddebitiAccrediti.Fields("TipoMov"), "AD")
                'Else
                '    MoveToDb(Rs_AddebitiAccrediti.Fields("TipoMov"), "AC")
                'End If
                'MoveToDb(Rs_AddebitiAccrediti.Fields("Retta"), "S")
                'MoveToDb(Rs_AddebitiAccrediti.Fields("Riferimento"), "O")
                'MoveToDb(Rs_AddebitiAccrediti.Fields("Importo"), Modulo.MathRound(Math.Abs(TotCal - tot), 2))
                'MoveToDb(Rs_AddebitiAccrediti.Fields("Provincia"), "")
                'MoveToDb(Rs_AddebitiAccrediti.Fields("Comune"), "")
                'MoveToDb(Rs_AddebitiAccrediti.Fields("Regione"), "")
                'MoveToDb(Rs_AddebitiAccrediti.Fields("Parente"), 0)
                'MoveToDb(Rs_AddebitiAccrediti.Fields("Data"), Format(DataMov, "dd/MM/yyyy"))
                'MoveToDb(Rs_AddebitiAccrediti.Fields("Progressivo"), ProgressivoADDACC())
                'MoveToDb(Rs_AddebitiAccrediti.Fields("Utente"), "EmissioneAddebitiAccrediti")
                'MoveToDb(Rs_AddebitiAccrediti.Fields("DataAggiornamento"), Now)
                'MoveToDb(Rs_AddebitiAccrediti.Fields("chiaveselezione"), "")
                'Rs_AddebitiAccrediti.Update()
                'StringaDegliErrori = StringaDegliErrori & "Periodo " & Mese & "/" & Anno & "  Creato Movimento numero : " & MoveFromDb(Rs_AddebitiAccrediti.Fields("Progressivo")) & vbNewLine
                'RicalcoloGriglia_NumeroMovimento(IndiceVariazione) = MoveFromDb(Rs_AddebitiAccrediti.Fields("Progressivo"))

                'Rs_AddebitiAccrediti.Close()
            End If
        End If




        For I = 1 To 10

            xAnno = Anno
            xMese = Mese

            'If CampoParente(CodOsp, I, "FattAnticipata", False) = "S" Then
            '   xAnno = Anno
            '   xMese = Mese - 1
            '   If xMese = 0 Then
            '      xAnno = Anno - 1
            '      xMese = 12
            '    End If
            'MeseAnno = " AnnoCompetenza =" & xAnno & " And MeseCompetenza = " & xMese
            'End If

            TotCal = 0
            MySql = "Select SUM(IMPORTO) From RETTEPARENTE Where CENTROSERVIZIO = '" & Cserv & "' And CODICEOSPITE = " & CodOsp & " And CODICEPARENTE = " & I & " And Elemento = 'RPX'  And Elemento <> 'ACC' And Elemento <> 'ADD' And MeseCompetenza = " & Mese & " And AnnoCompetenza = " & Anno

            Dim cmdParenteRetta As New OleDbCommand()
            cmdParenteRetta.CommandText = MySql
            cmdParenteRetta.Connection = OspitiCon
            Dim RDParenteRetta As OleDbDataReader = cmdParenteRetta.ExecuteReader()
            If RDParenteRetta.Read() Then
                tot = campodbN(RDParenteRetta.Item(0))
            End If
            RDParenteRetta.Close()


            Dim LO_Parente As New Cls_Parenti

            LO_Parente.Leggi(STRINGACONNESSIONEDB, CodOsp, I)


            Dim KCP As New Cls_DatiOspiteParenteCentroServizio

            KCP.CentroServizio = Cserv
            KCP.CodiceOspite = CodOsp
            KCP.CodiceParente = I
            KCP.Leggi(STRINGACONNESSIONEDB)

            REM Assegna la tipologia operazione e l'aliquota prendendo quella relativa al cserv
            If KCP.CodiceOspite <> 0 Then
                LO_Parente.TIPOOPERAZIONE = KCP.TipoOperazione
                LO_Parente.CODICEIVA = KCP.AliquotaIva
                LO_Parente.FattAnticipata = KCP.Anticipata
                LO_Parente.MODALITAPAGAMENTO = KCP.ModalitaPagamento
                LO_Parente.Compensazione = KCP.Compensazione
            End If

            If tot = 0 And LO_Parente.FattAnticipata = "S" Then
                Dim tipooperazione As New Cls_TipoOperazione

                tipooperazione.Codice = LO_Parente.TIPOOPERAZIONE
                tipooperazione.Leggi(STRINGACONNESSIONEDB, tipooperazione.Codice)


                tot = ImportoDocumentoOspite(CentroServizio.MASTRO, CentroServizio.CONTO, CodOsp * 100 + I, Cserv, Anno, Mese, "", Anno, Mese, 0, tipooperazione.TipoAddebitoAccredito)

                tot = tot + ImportoDocumentoOspiteExtra(CentroServizio.MASTRO, CentroServizio.CONTO, CodOsp * 100 + I, Cserv, Anno, Mese, "", Anno, Mese, 0, tipooperazione.TipoAddebitoAccredito)
            Else
                MySql = "Select SUM(IMPORTO) From RETTEPARENTE Where CENTROSERVIZIO = '" & Cserv & "' And CODICEOSPITE = " & CodOsp & " And CODICEPARENTE = " & I & " And Elemento <> 'RPX' And Elemento <> 'R2X'  And Elemento <> 'ACC' And Elemento <> 'ADD'"
                MySql = MySql & " And Mese = " & xMese & " And Anno = " & xAnno & " "
                Dim cmdParenteRetta1 As New OleDbCommand()
                cmdParenteRetta1.CommandText = MySql
                cmdParenteRetta1.Connection = OspitiCon
                Dim RDParenteRetta1 As OleDbDataReader = cmdParenteRetta1.ExecuteReader()
                If RDParenteRetta1.Read() Then
                    tot = tot + campodbN(RDParenteRetta1.Item(0))
                End If
                RDParenteRetta1.Close()
            End If



            'If tot = 0 Then
            '    MySql = "Select SUM(IMPORTO) From RETTEPARENTE Where CENTROSERVIZIO = '" & Cserv & "' And CODICEOSPITE = " & CodOsp & " And CODICEPARENTE = " & I & " And (Elemento = 'RPX' )" ' OR  Elemento = 'RGA' ???
            '    If xMese > 1 Then
            '        MySql = MySql & " And Mese = " & xMese - 1 & " And Anno = " & xAnno & " "
            '    Else
            '        MySql = MySql & " And Mese = 12 And Anno = " & xAnno - 1 & " "
            '    End If
            '    Dim cmdParenteRettaRPX As New OleDbCommand()
            '    cmdParenteRettaRPX.CommandText = MySql
            '    cmdParenteRettaRPX.Connection = OspitiCon
            '    Dim RDParenteRettaRPX As OleDbDataReader = cmdParenteRettaRPX.ExecuteReader()
            '    If RDParenteRettaRPX.Read() Then
            '        tot = tot + campodbN(RDParenteRettaRPX.Item(0))
            '    End If
            '    RDParenteRettaRPX.Close()
            'End If

            TotaleExtra = 0
            For E = 1 To 10 : TotaleExtra = TotaleExtra + ImpExtraParente(I, E) + NonImpExtraParente(I, E) : Next E

            TotCal = CDbl(ImportoPresParente(I)) + CDbl(ImportoAssParente(I)) + CDbl(NonImportoPresParente(I)) + CDbl(NonImportoAssParente(I)) + CDbl(TotaleExtra)

            MySql = "Select Sum(IMPORTO) From ADDACR Where Retta = 'S' And Riferimento = 'P' And CENTROSERVIZIO = '" & Cserv & "' And CODICEOSPITE = " & CodOsp & " And Parente = " & I & " And  TipoMov = 'AD' And " & MeseAnno

            Dim cmdMovimentiADD As New OleDbCommand()
            cmdMovimentiADD.CommandText = MySql
            cmdMovimentiADD.Connection = OspitiCon
            Dim RDMovimentiADD As OleDbDataReader = cmdMovimentiADD.ExecuteReader()
            If RDMovimentiADD.Read() Then
                ImpAdd = Modulo.MathRound(campodbN(RDMovimentiADD.Item(0)), 2)
            End If
            RDMovimentiADD.Close()



            MySql = "Select Sum(IMPORTO) From ADDACR Where Retta = 'S' And Riferimento = 'P' And  CENTROSERVIZIO = '" & Cserv & "' And CODICEOSPITE = " & CodOsp & " And Parente = " & I & " And TipoMov = 'AC' And " & MeseAnno
            Dim cmdMovimentiACC As New OleDbCommand()
            cmdMovimentiACC.CommandText = MySql
            cmdMovimentiACC.Connection = OspitiCon
            Dim RDMovimentiACC As OleDbDataReader = cmdMovimentiACC.ExecuteReader()
            If RDMovimentiACC.Read() Then
                ImpAcc = Modulo.MathRound(campodbN(RDMovimentiACC.Item(0)), 2)
            End If
            RDMovimentiACC.Close()


            ' verifica conguagli
            'MySql = "Select SUM(IMPORTO) From RETTEPARENTE Where CENTROSERVIZIO = '" & Cserv & "' And CODICEOSPITE = " & CodOsp & " And CODICEPARENTE = " & I
            'MySql = MySql & " And Mese = " & Mese & " And Anno = " & Anno & " And Elemento = 'ACC' And (CODICEEXTRA is null or CODICEEXTRA = '')"
            'Dim cmdRettaParenteACC As New OleDbCommand()
            'cmdRettaParenteACC.CommandText = MySql
            'cmdRettaParenteACC.Connection = OspitiCon
            'Dim RDRettaParenteACC As OleDbDataReader = cmdRettaParenteACC.ExecuteReader()
            'If RDRettaParenteACC.Read() Then
            '    If campodb(RDRettaParenteACC.Item(0)) = "" Then
            '        ImpAcc = 0
            '    Else
            '        ImpAcc = campodb(RDRettaParenteACC.Item(0))
            '    End If
            'End If
            'RDRettaParenteACC.Close()


            'MySql = "Select SUM(IMPORTO) From RETTEPARENTE Where CENTROSERVIZIO = '" & Cserv & "' And CODICEOSPITE = " & CodOsp & " And CODICEPARENTE = " & I
            'MySql = MySql & " And Mese = " & Mese & " And Anno = " & Anno & " And Elemento = 'ADD'  And (CODICEEXTRA is null or CODICEEXTRA = '')"
            'Dim cmdRettaParenteADD As New OleDbCommand()
            'cmdRettaParenteADD.CommandText = MySql
            'cmdRettaParenteADD.Connection = OspitiCon
            'Dim RDRettaParenteADD As OleDbDataReader = cmdRettaParenteADD.ExecuteReader()
            'If RDRettaParenteADD.Read() Then
            '    If campodb(RDRettaParenteADD.Item(0)) = "" Then
            '        ImpAdd = 0
            '    Else
            '        ImpAdd = campodb(RDRettaParenteADD.Item(0))
            '    End If
            'End If
            'RDRettaParenteADD.Close()

            MySql = "Select SUM(IMPORTO) From RETTEPARENTE Where CENTROSERVIZIO = '" & Cserv & "' And CODICEOSPITE = " & CodOsp & " And CODICEPARENTE = " & I
            MySql = MySql & " And MeseCompetenza  = " & Mese & " And AnnoCompetenza  = " & Anno & " And Elemento = 'ACC'"
            Dim cmdRettaParenteACC As New OleDbCommand()
            cmdRettaParenteACC.CommandText = MySql
            cmdRettaParenteACC.Connection = OspitiCon
            Dim RDRettaParenteACC As OleDbDataReader = cmdRettaParenteACC.ExecuteReader()
            If RDRettaParenteACC.Read() Then
                If campodb(RDRettaParenteACC.Item(0)) = "" Then
                    ImpAcc = ImpAcc + 0
                Else
                    ImpAcc = ImpAcc + campodb(RDRettaParenteACC.Item(0))
                End If
            End If
            RDRettaParenteACC.Close()


            MySql = "Select SUM(IMPORTO) From RETTEPARENTE Where CENTROSERVIZIO = '" & Cserv & "' And CODICEOSPITE = " & CodOsp & " And CODICEPARENTE = " & I
            MySql = MySql & " And MeseCompetenza  = " & Mese & " And AnnoCompetenza  = " & Anno & " And Elemento = 'DD'"
            Dim cmdRettaParenteADD As New OleDbCommand()
            cmdRettaParenteADD.CommandText = MySql
            cmdRettaParenteADD.Connection = OspitiCon
            Dim RDRettaParenteADD As OleDbDataReader = cmdRettaParenteADD.ExecuteReader()
            If RDRettaParenteADD.Read() Then
                If campodb(RDRettaParenteADD.Item(0)) = "" Then
                    ImpAdd = ImpAdd + 0
                Else
                    ImpAdd = ImpAdd + campodb(RDRettaParenteADD.Item(0))
                End If
            End If
            RDRettaParenteADD.Close()


            tot = tot + ImpAdd - ImpAcc
            If Modulo.MathRound(TotCal, 2) <> Modulo.MathRound(tot, 2) Then
                If 2 = 2 Then
                    StringaDegliErrori = StringaDegliErrori & "Periodo " & Mese & "/" & Anno & "  Importo Parenti Diverso per Euro " & vbTab & Modulo.MathRound(TotCal - tot, 2) & vbTab & "Codice Servizio : " & Cserv & " Ospite : " & CodOsp & " Parente " & I & vbTab & CampoParente(CodOsp, I, "NOME", False) & vbNewLine
                Else
                    StringaDegliErrori = StringaDegliErrori & "Periodo " & Mese & "/" & Anno & "  Importo Parenti Diverso per Lire " & vbTab & Modulo.MathRound(TotCal - tot, 2) & vbTab & "Codice Servizio : " & Cserv & " Ospite : " & CodOsp & " Parente " & I & vbTab & CampoParente(CodOsp, I, "NOME", False) & vbNewLine
                End If

                IndiceVariazione = IndiceVariazione + 1

                RicalcoloGriglia_CServ(IndiceVariazione) = Cserv
                RicalcoloGriglia_CodiceOspite(IndiceVariazione) = CodOsp
                RicalcoloGriglia_Anno(IndiceVariazione) = Anno
                RicalcoloGriglia_Mese(IndiceVariazione) = Mese
                RicalcoloGriglia_Ospite(IndiceVariazione) = 0
                RicalcoloGriglia_Parente_id(IndiceVariazione) = I
                RicalcoloGriglia_Parente(IndiceVariazione) = Modulo.MathRound(TotCal - tot, 2)
                RicalcoloGriglia_Comune(IndiceVariazione) = 0
                RicalcoloGriglia_Jolly(IndiceVariazione) = 0
                RicalcoloGriglia_Regione(IndiceVariazione) = 0
                RicalcoloGriglia_Descrizione(IndiceVariazione) = ""

                If CreoAddAccr = 1 Then
                    Dim CreAdd As New Cls_AddebitiAccrediti


                    CreAdd.CENTROSERVIZIO = Cserv
                    CreAdd.CodiceOspite = CodOsp
                    CreAdd.Descrizione = Tdesc
                    CreAdd.ANNOCOMPETENZA = Anno
                    CreAdd.MESECOMPETENZA = Mese
                    If TotCal - tot > 0 Then
                        CreAdd.TipoMov = "AD"
                    Else
                        CreAdd.TipoMov = "AC"
                    End If
                    CreAdd.CodiceIva = CampoParametri("NCDeposito") ' VARIAZIONI RETTA


                    Dim Parente As New Cls_Parenti

                    Parente.CodiceOspite = CodOsp
                    Parente.CodiceParente = I
                    Parente.Leggi(STRINGACONNESSIONEDB, CodOsp, I)

                    Dim Dati As New Cls_DatiOspiteParenteCentroServizio

                    Dati.CodiceOspite = CodOsp
                    Dati.CentroServizio = Cserv
                    Dati.CodiceParente = I
                    Dati.Leggi(STRINGACONNESSIONEDB)
                    If Dati.CodiceOspite <> 0 Then
                        Parente.TIPOOPERAZIONE = Dati.TipoOperazione
                    End If

                    Dim TipoOperazione As New Cls_TipoOperazione

                    TipoOperazione.Codice = Parente.TIPOOPERAZIONE
                    TipoOperazione.Leggi(STRINGACONNESSIONEDB, TipoOperazione.Codice)

                    If TipoOperazione.TipoAddebitoAccredito <> "" Then
                        CreAdd.CodiceIva = TipoOperazione.TipoAddebitoAccredito
                    End If

                    CreAdd.RETTA = "S"
                    CreAdd.RIFERIMENTO = "P"
                    CreAdd.IMPORTO = Modulo.MathRound(Math.Abs(TotCal - tot), 2)
                    CreAdd.PROVINCIA = ""
                    CreAdd.COMUNE = ""
                    CreAdd.REGIONE = ""
                    CreAdd.PARENTE = I
                    CreAdd.Data = Format(DataMov, "dd/MM/yyyy")
                    CreAdd.Progressivo = ProgressivoADDACC()
                    CreAdd.Utente = "EmissioneAddebitiAccrediti"
                    CreAdd.InserisciAddebito(STRINGACONNESSIONEDB)

                    StringaDegliErrori = StringaDegliErrori & "Periodo " & Mese & "/" & Anno & "  Creato Movimento numero : " & CreAdd.Progressivo & vbNewLine

                    RicalcoloGriglia_NumeroMovimento(IndiceVariazione) = CreAdd.Progressivo


                    'Rs_AddebitiAccrediti.Open("Select * From ADDACR Where CodiceOspite = 0", OspitiDb, ADODB.CursorTypeEnum.adOpenKeyset, ADODB.LockTypeEnum.adLockOptimistic)
                    'Rs_AddebitiAccrediti.AddNew()
                    'MoveToDb(Rs_AddebitiAccrediti.Fields("CentroServizio"), Cserv)
                    'MoveToDb(Rs_AddebitiAccrediti.Fields("CodiceOspite"), CodOsp)
                    'MoveToDb(Rs_AddebitiAccrediti.Fields("Descrizione"), Tdesc) ' "Movimento Automatico"
                    'MoveToDb(Rs_AddebitiAccrediti.Fields("ANNOCOMPETENZA"), Anno)
                    'MoveToDb(Rs_AddebitiAccrediti.Fields("MESECOMPETENZA"), Mese)
                    'If TotCal - tot > 0 Then
                    '    MoveToDb(Rs_AddebitiAccrediti.Fields("TipoMov"), "AD")
                    'Else
                    '    MoveToDb(Rs_AddebitiAccrediti.Fields("TipoMov"), "AC")
                    'End If

                    'MoveToDb(Rs_AddebitiAccrediti.Fields("CodiceIva"), CampoParametri("NCDeposito")) ' VARIAZIONI RETTA

                    'MoveToDb(Rs_AddebitiAccrediti.Fields("Retta"), "S")
                    'MoveToDb(Rs_AddebitiAccrediti.Fields("Riferimento"), "P")
                    'MoveToDb(Rs_AddebitiAccrediti.Fields("Importo"), Modulo.MathRound(Math.Abs(TotCal - tot), 2))
                    'MoveToDb(Rs_AddebitiAccrediti.Fields("Provincia"), "")
                    'MoveToDb(Rs_AddebitiAccrediti.Fields("Comune"), "")
                    'MoveToDb(Rs_AddebitiAccrediti.Fields("Regione"), "")
                    'MoveToDb(Rs_AddebitiAccrediti.Fields("Parente"), I)
                    'MoveToDb(Rs_AddebitiAccrediti.Fields("Data"), Format(DataMov, "dd/MM/yyyy"))
                    'MoveToDb(Rs_AddebitiAccrediti.Fields("Progressivo"), ProgressivoADDACC())
                    'MoveToDb(Rs_AddebitiAccrediti.Fields("Utente"), "EmissioneAddebitiAccrediti")
                    'MoveToDb(Rs_AddebitiAccrediti.Fields("DataAggiornamento"), Now)
                    'Rs_AddebitiAccrediti.Update()
                    'StringaDegliErrori = StringaDegliErrori & "Periodo " & Mese & "/" & Anno & "  Creato Movimento numero : " & MoveFromDb(Rs_AddebitiAccrediti.Fields("Progressivo")) & vbNewLine

                    'RicalcoloGriglia_NumeroMovimento(IndiceVariazione) = MoveFromDb(Rs_AddebitiAccrediti.Fields("Progressivo"))
                    'Rs_AddebitiAccrediti.Close()
                End If
            End If
        Next I



        For I = 1 To 10
            Provincia = Mid(CodiceComune(I), 1, 3)
            Comune = Mid(CodiceComune(I), 4, 3)

            Dim KCom As New ClsComune

            KCom.Provincia = Provincia
            KCom.Comune = Comune
            KCom.Leggi(STRINGACONNESSIONEDB)

            If KCom.NonInUso <> "S" Then

                MySql = "Select SUM(IMPORTO) From RetteComune Where CENTROSERVIZIO = '" & Cserv & "' And CODICEOSPITE = " & CodOsp & " And Provincia = '" & Provincia & "' And Comune = '" & Comune & "'  And ElemenTo <> 'ACC' And ElemenTo <> 'ADD'"
                MySql = MySql & " And Mese = " & Mese & " And Anno = " & Anno & " "

                Dim cmdRetteComune As New OleDbCommand()
                cmdRetteComune.CommandText = MySql
                cmdRetteComune.Connection = OspitiCon
                Dim RDRetteComune As OleDbDataReader = cmdRetteComune.ExecuteReader()
                If RDRetteComune.Read() Then
                    tot = campodbN(RDRetteComune.Item(0))
                End If
                RDRetteComune.Close()

                TotaleExtra = 0
                For E = 0 To 10 : TotaleExtra = TotaleExtra + ImpExtraComune(I, E) + NonImpExtraComune(I, E) : Next E

                'For E = 0 To 20
                '    TotaleExtra = TotaleExtra + MyTabAddebitoComune(i, E) - MyTabAccreditoComune(i, E)
                'Next E
                TotCal = ImportoPresComune(I) + ImportoAssComune(I) + NonImportoPresComune(I) + NonImportoAssComune(I) + TotaleExtra
                MySql = "Select Sum(IMPORTO) From ADDACR Where Retta = 'S' And Riferimento = 'C' And  CENTROSERVIZIO = '" & Cserv & "' And CODICEOSPITE = " & CodOsp & " And Comune = '" & Comune & "' And Provincia = '" & Provincia & "' And TipoMov = 'AD' And " & MeseAnno
                Dim cmdRetteComuneAD As New OleDbCommand()
                cmdRetteComuneAD.CommandText = MySql
                cmdRetteComuneAD.Connection = OspitiCon
                Dim RDRetteComuneAD As OleDbDataReader = cmdRetteComuneAD.ExecuteReader()
                If RDRetteComuneAD.Read() Then
                    ImpAdd = Modulo.MathRound(campodbN(RDRetteComuneAD.Item(0)), 2)
                End If
                RDRetteComuneAD.Close()

                MySql = "Select Sum(IMPORTO) From ADDACR Where Retta = 'S' And Riferimento = 'C' And  CENTROSERVIZIO = '" & Cserv & "' And CODICEOSPITE = " & CodOsp & " And Comune = '" & Comune & "' And Provincia = '" & Provincia & "' And TipoMov = 'AC' And " & MeseAnno
                Dim cmdRetteComuneAC As New OleDbCommand()
                cmdRetteComuneAC.CommandText = MySql
                cmdRetteComuneAC.Connection = OspitiCon
                Dim RDRetteComuneAC As OleDbDataReader = cmdRetteComuneAC.ExecuteReader()
                If RDRetteComuneAC.Read() Then
                    ImpAcc = Modulo.MathRound(campodbN(RDRetteComuneAC.Item(0)), 2)
                End If
                RDRetteComuneAC.Close()


                tot = tot + ImpAdd - ImpAcc



                If Modulo.MathRound(TotCal, 2) <> Modulo.MathRound(tot, 2) And Provincia <> "" Then
                    StringaDegliErrori = StringaDegliErrori & "Periodo " & Mese & "/" & Anno & "  Importo Comune Diverso per " & vbTab & Modulo.MathRound(TotCal - tot, 2) & vbTab & "Codice Servizio : " & Cserv & " Ospite : " & CodOsp & vbTab & " Provincia " & Provincia & " Comune " & Comune & " " & DecodificaComune(Provincia, Comune) & vbNewLine

                    IndiceVariazione = IndiceVariazione + 1

                    RicalcoloGriglia_CServ(IndiceVariazione) = Cserv
                    RicalcoloGriglia_CodiceOspite(IndiceVariazione) = CodOsp
                    RicalcoloGriglia_Anno(IndiceVariazione) = Anno
                    RicalcoloGriglia_Mese(IndiceVariazione) = Mese
                    RicalcoloGriglia_Ospite(IndiceVariazione) = 0
                    RicalcoloGriglia_Parente_id(IndiceVariazione) = 0
                    RicalcoloGriglia_Parente(IndiceVariazione) = 0
                    RicalcoloGriglia_Comune(IndiceVariazione) = Modulo.MathRound(TotCal - tot, 2)
                    RicalcoloGriglia_Jolly(IndiceVariazione) = 0
                    RicalcoloGriglia_Regione(IndiceVariazione) = 0
                    RicalcoloGriglia_Descrizione(IndiceVariazione) = DecodificaComune(Provincia, Comune)

                    If CreoAddAccr = 1 Then
                        Dim CreaAddComune As New Cls_AddebitiAccrediti


                        CreaAddComune.CENTROSERVIZIO = Cserv
                        CreaAddComune.CodiceOspite = CodOsp
                        CreaAddComune.Descrizione = Tdesc
                        CreaAddComune.ANNOCOMPETENZA = Anno
                        CreaAddComune.MESECOMPETENZA = Mese
                        If TotCal - tot > 0 Then
                            CreaAddComune.TipoMov = "AD"
                        Else
                            CreaAddComune.TipoMov = "AC"
                        End If
                        CreaAddComune.RETTA = "S"
                        CreaAddComune.IMPORTO = Modulo.MathRound(Math.Abs(TotCal - tot), 2)
                        CreaAddComune.CodiceIva = CampoParametri("NCDeposito")


                        Dim TComune As New ClsComune

                        TComune.Comune = Comune
                        TComune.Provincia = Provincia
                        TComune.Leggi(STRINGACONNESSIONEDB)


                        Dim TipoOperazione As New Cls_TipoOperazione

                        TipoOperazione.Codice = TComune.TipoOperazione
                        TipoOperazione.Leggi(STRINGACONNESSIONEDB, TipoOperazione.Codice)

                        If TipoOperazione.TipoAddebitoAccredito <> "" Then
                            CreaAddComune.CodiceIva = TipoOperazione.TipoAddebitoAccredito
                        End If

                        CreaAddComune.PROVINCIA = Provincia
                        CreaAddComune.COMUNE = Comune
                        CreaAddComune.REGIONE = ""
                        CreaAddComune.RIFERIMENTO = "C"
                        CreaAddComune.Data = Format(DataMov, "dd/MM/yyyy")
                        CreaAddComune.Progressivo = ProgressivoADDACC()
                        CreaAddComune.Utente = "EmissioneAddebitiAccrediti"
                        CreaAddComune.InserisciAddebito(STRINGACONNESSIONEDB)

                        StringaDegliErrori = StringaDegliErrori & "Periodo " & Mese & "/" & Anno & "  Creato Movimento numero : " & CreaAddComune.Progressivo & vbNewLine
                        RicalcoloGriglia_NumeroMovimento(IndiceVariazione) = CreaAddComune.Progressivo
                    End If
                End If
            End If
        Next I


        For I = 1 To 10
            Provincia = Mid(CodiceJolly(I), 1, 3)
            Comune = Mid(CodiceJolly(I), 4, 3)
            MySql = "Select SUM(IMPORTO) From RetteJolly Where CENTROSERVIZIO = '" & Cserv & "' And CODICEOSPITE = " & CodOsp & " And Provincia = '" & Provincia & "' And Comune = '" & Comune & "'  And ElemenTo <> 'ACC' And ElemenTo <> 'ADD'"
            MySql = MySql & " And Mese = " & Mese & " And Anno = " & Anno & " "

            Dim cmdRetteJolly As New OleDbCommand()
            cmdRetteJolly.CommandText = MySql
            cmdRetteJolly.Connection = OspitiCon
            Dim RDRetteJolly As OleDbDataReader = cmdRetteJolly.ExecuteReader()
            If RDRetteJolly.Read() Then
                tot = Modulo.MathRound(campodbN(RDRetteJolly.Item(0)), 2)
            End If
            RDRetteJolly.Close()




            TotaleExtra = 0
            For E = 0 To 10 : TotaleExtra = TotaleExtra + ImpExtraJolly(I, E) + NonImpExtraJolly(I, E) : Next E

            'For E = 0 To 20
            '    TotaleExtra = TotaleExtra + MyTabAddebitoComune(i, E) - MyTabAccreditoComune(i, E)
            'Next E
            TotCal = ImportoPresJolly(I) + ImportoAssJolly(I) + NonImportoPresJolly(I) + NonImportoAssJolly(I) + TotaleExtra
            MySql = "Select Sum(IMPORTO) From ADDACR Where Retta = 'S' And Riferimento = 'J' And  CENTROSERVIZIO = '" & Cserv & "' And CODICEOSPITE = " & CodOsp & " And Comune = '" & Comune & "' And Provincia = '" & Provincia & "' And TipoMov = 'AD' And " & MeseAnno
            Dim cmdRetteJollyAD As New OleDbCommand()
            cmdRetteJollyAD.CommandText = MySql
            cmdRetteJollyAD.Connection = OspitiCon
            Dim RDRetteJollyAD As OleDbDataReader = cmdRetteJollyAD.ExecuteReader()
            If RDRetteJollyAD.Read() Then
                ImpAdd = Modulo.MathRound(campodbN(RDRetteJollyAD.Item(0)), 2)
            End If
            RDRetteJolly.Close()


            MySql = "Select Sum(IMPORTO) From ADDACR Where Retta = 'S' And Riferimento = 'J' And  CENTROSERVIZIO = '" & Cserv & "' And CODICEOSPITE = " & CodOsp & " And Comune = '" & Comune & "' And Provincia = '" & Provincia & "' And TipoMov = 'AC' And " & MeseAnno
            Dim cmdRetteJollyAC As New OleDbCommand()
            cmdRetteJollyAC.CommandText = MySql
            cmdRetteJollyAC.Connection = OspitiCon
            Dim RDRetteJollyAC As OleDbDataReader = cmdRetteJollyAC.ExecuteReader()
            If RDRetteJollyAC.Read() Then
                ImpAcc = Modulo.MathRound(campodbN(RDRetteJollyAC.Item(0)), 2)
            End If
            RDRetteJollyAC.Close()



            tot = tot + ImpAdd - ImpAcc


            If Modulo.MathRound(TotCal, 2) <> Modulo.MathRound(tot, 2) And Provincia <> "" Then
                StringaDegliErrori = StringaDegliErrori & "Periodo " & Mese & "/" & Anno & "  Importo Jolly Diverso per " & vbTab & Modulo.MathRound(TotCal - tot, 2) & vbTab & "Codice Servizio : " & Cserv & " Ospite : " & CodOsp & vbTab & " Provincia " & Provincia & " Comune " & Comune & " " & DecodificaComune(Provincia, Comune) & vbNewLine
                IndiceVariazione = IndiceVariazione + 1

                RicalcoloGriglia_CServ(IndiceVariazione) = Cserv
                RicalcoloGriglia_CodiceOspite(IndiceVariazione) = CodOsp
                RicalcoloGriglia_Anno(IndiceVariazione) = Anno
                RicalcoloGriglia_Mese(IndiceVariazione) = Mese
                RicalcoloGriglia_Ospite(IndiceVariazione) = 0
                RicalcoloGriglia_Parente_id(IndiceVariazione) = 0
                RicalcoloGriglia_Parente(IndiceVariazione) = 0
                RicalcoloGriglia_Comune(IndiceVariazione) = 0
                RicalcoloGriglia_Jolly(IndiceVariazione) = Modulo.MathRound(TotCal - tot, 2)
                RicalcoloGriglia_Regione(IndiceVariazione) = 0
                RicalcoloGriglia_Descrizione(IndiceVariazione) = DecodificaComune(Provincia, Comune)

                If CreoAddAccr = 1 Then

                    Dim CreaAddAcr As New Cls_AddebitiAccrediti

                    CreaAddAcr.CENTROSERVIZIO = Cserv
                    CreaAddAcr.CodiceOspite = CodOsp
                    CreaAddAcr.Descrizione = Tdesc
                    CreaAddAcr.ANNOCOMPETENZA = Anno
                    CreaAddAcr.MESECOMPETENZA = Mese
                    If TotCal - tot > 0 Then
                        CreaAddAcr.TipoMov = "AD"
                    Else
                        CreaAddAcr.TipoMov = "AC"
                    End If
                    CreaAddAcr.CodiceIva = CampoParametri("NCDeposito")

                    Dim TComune As New ClsComune

                    TComune.Comune = Comune
                    TComune.Provincia = Provincia
                    TComune.Leggi(STRINGACONNESSIONEDB)


                    Dim TipoOperazione As New Cls_TipoOperazione

                    TipoOperazione.Codice = TComune.TipoOperazione
                    TipoOperazione.Leggi(STRINGACONNESSIONEDB, TipoOperazione.Codice)

                    If TipoOperazione.TipoAddebitoAccredito <> "" Then
                        CreaAddAcr.CodiceIva = TipoOperazione.TipoAddebitoAccredito
                    End If

                    CreaAddAcr.RETTA = "S"
                    CreaAddAcr.IMPORTO = Modulo.MathRound(Math.Abs(TotCal - tot), 2)
                    CreaAddAcr.PROVINCIA = Provincia
                    CreaAddAcr.COMUNE = Comune
                    CreaAddAcr.RIFERIMENTO = "J"
                    CreaAddAcr.Data = Format(DataMov, "dd/MM/yyyy")
                    CreaAddAcr.Progressivo = ProgressivoADDACC()
                    CreaAddAcr.Utente = "EmissioneAddebitiAccrediti"
                    CreaAddAcr.InserisciAddebito(STRINGACONNESSIONEDB)

                    StringaDegliErrori = StringaDegliErrori & "Periodo " & Mese & "/" & Anno & "  Creato Movimento numero : " & CreaAddAcr.Progressivo & vbNewLine

                    RicalcoloGriglia_NumeroMovimento(IndiceVariazione) = CreaAddAcr.Progressivo

                    'Rs_AddebitiAccrediti.Open("Select * From ADDACR Where CodiceOspite = 0", OspitiDb, ADODB.CursorTypeEnum.adOpenKeyset, ADODB.LockTypeEnum.adLockOptimistic)
                    'Rs_AddebitiAccrediti.AddNew()
                    'MoveToDb(Rs_AddebitiAccrediti.Fields("CentroServizio"), Cserv)
                    'MoveToDb(Rs_AddebitiAccrediti.Fields("CodiceOspite"), CodOsp)
                    'MoveToDb(Rs_AddebitiAccrediti.Fields("Descrizione"), Tdesc)  '"Movimento Automatico"
                    'MoveToDb(Rs_AddebitiAccrediti.Fields("ANNOCOMPETENZA"), Anno)
                    'MoveToDb(Rs_AddebitiAccrediti.Fields("MESECOMPETENZA"), Mese)
                    'If TotCal - tot > 0 Then
                    '    MoveToDb(Rs_AddebitiAccrediti.Fields("TipoMov"), "AD")
                    'Else
                    '    MoveToDb(Rs_AddebitiAccrediti.Fields("TipoMov"), "AC")
                    'End If
                    'MoveToDb(Rs_AddebitiAccrediti.Fields("CodiceIva"), CampoParametri("NCDeposito"))
                    'MoveToDb(Rs_AddebitiAccrediti.Fields("Retta"), "S")
                    'MoveToDb(Rs_AddebitiAccrediti.Fields("Importo"), Modulo.MathRound(Math.Abs(TotCal - tot), 2))
                    'MoveToDb(Rs_AddebitiAccrediti.Fields("Provincia"), Provincia)
                    'MoveToDb(Rs_AddebitiAccrediti.Fields("Comune"), Comune)
                    'MoveToDb(Rs_AddebitiAccrediti.Fields("Regione"), "")
                    'MoveToDb(Rs_AddebitiAccrediti.Fields("Parente"), 0)
                    'MoveToDb(Rs_AddebitiAccrediti.Fields("Riferimento"), "J")
                    'MoveToDb(Rs_AddebitiAccrediti.Fields("Data"), Format(DataMov, "dd/MM/yyyy"))
                    'MoveToDb(Rs_AddebitiAccrediti.Fields("Progressivo"), ProgressivoADDACC())
                    'MoveToDb(Rs_AddebitiAccrediti.Fields("Utente"), "EmissioneAddebitiAccrediti")
                    'MoveToDb(Rs_AddebitiAccrediti.Fields("DataAggiornamento"), Now)
                    'Rs_AddebitiAccrediti.Update()
                    'StringaDegliErrori = StringaDegliErrori & "Periodo " & Mese & "/" & Anno & "  Creato Movimento numero : " & MoveFromDb(Rs_AddebitiAccrediti.Fields("Progressivo")) & vbNewLine

                    'RicalcoloGriglia_NumeroMovimento(IndiceVariazione) = MoveFromDb(Rs_AddebitiAccrediti.Fields("Progressivo"))
                    'Rs_AddebitiAccrediti.Close()

                End If
            End If
        Next I


        For I = 0 To 10
            TotCal = 0
            MySql = "Select SUM(IMPORTO) From RetteRegione Where CENTROSERVIZIO = '" & Cserv & "' And CODICEOSPITE = " & CodOsp & " And REGIONE = '" & CodiceRegione(I) & "'"
            MySql = MySql & " And Mese = " & Mese & " And Anno = " & Anno & "  And ElemenTo <> 'ACC' And ElemenTo <> 'ADD'"
            Dim cmdRetteRegione As New OleDbCommand()
            cmdRetteRegione.CommandText = MySql
            cmdRetteRegione.Connection = OspitiCon
            Dim RDRetteRegione As OleDbDataReader = cmdRetteRegione.ExecuteReader()
            If RDRetteRegione.Read() Then
                tot = Modulo.MathRound(campodbN(RDRetteRegione.Item(0)), 2)
            End If
            RDRetteRegione.Close()



            TotCal = TotCal + ImportoPresRegione(I) + ImportoAssRegione(I) + NonImportoPresRegione(I) + NonImportoAssRegione(I)
            TotaleExtra = 0
            'For E = 0 To 20
            '    TotaleExtra = TotaleExtra + MyTabAddebitoRegione(i, E) - MyTabAccreditoRegione(i, E)
            'Next E

            MySql = "Select Sum(IMPORTO) From ADDACR Where Retta = 'S' And Riferimento = 'R' And CENTROSERVIZIO = '" & Cserv & "' And CODICEOSPITE = " & CodOsp & " And Regione = '" & CodiceRegione(I) & "' And TipoMov = 'AD' And " & MeseAnno

            Dim cmdRetteRegioneAdd As New OleDbCommand()
            cmdRetteRegioneAdd.CommandText = MySql
            cmdRetteRegioneAdd.Connection = OspitiCon
            Dim RDRetteRegioneAdd As OleDbDataReader = cmdRetteRegioneAdd.ExecuteReader()
            If RDRetteRegioneAdd.Read() Then
                ImpAdd = Modulo.MathRound(campodbN(RDRetteRegioneAdd.Item(0)), 2)
            End If
            RDRetteRegioneAdd.Close()





            MySql = "Select Sum(IMPORTO) From ADDACR Where Retta = 'S' And Riferimento = 'R' And CENTROSERVIZIO = '" & Cserv & "' And CODICEOSPITE = " & CodOsp & " And Regione = '" & CodiceRegione(I) & "' And TipoMov = 'AC' And " & MeseAnno
            Dim cmdRetteRegioneAcc As New OleDbCommand()
            cmdRetteRegioneAcc.CommandText = MySql
            cmdRetteRegioneAcc.Connection = OspitiCon
            Dim RDRetteRegioneAcc As OleDbDataReader = cmdRetteRegioneAcc.ExecuteReader()
            If RDRetteRegioneAcc.Read() Then
                ImpAcc = Modulo.MathRound(campodbN(RDRetteRegioneAcc.Item(0)), 2)
            End If
            RDRetteRegioneAcc.Close()



            tot = tot + ImpAdd - ImpAcc


            If Modulo.MathRound(TotCal, 2) <> Modulo.MathRound(tot, 2) Then
                StringaDegliErrori = StringaDegliErrori & "Periodo " & Mese & "/" & Anno & "  Importo Regione Diverso per " & vbTab & Modulo.MathRound(TotCal - tot, 2) & " " & vbTab & "Codice Servizio : " & Cserv & " Ospite : " & CodOsp & vbTab & " Regione " & CodiceRegione(I) & " " & CampoRegioneT(CodiceRegione(I), "NOME") & vbNewLine

                IndiceVariazione = IndiceVariazione + 1

                RicalcoloGriglia_CServ(IndiceVariazione) = Cserv
                RicalcoloGriglia_CodiceOspite(IndiceVariazione) = CodOsp
                RicalcoloGriglia_Anno(IndiceVariazione) = Anno
                RicalcoloGriglia_Mese(IndiceVariazione) = Mese
                RicalcoloGriglia_Ospite(IndiceVariazione) = 0
                RicalcoloGriglia_Parente_id(IndiceVariazione) = 0
                RicalcoloGriglia_Parente(IndiceVariazione) = 0
                RicalcoloGriglia_Comune(IndiceVariazione) = 0
                RicalcoloGriglia_Jolly(IndiceVariazione) = 0
                RicalcoloGriglia_Regione(IndiceVariazione) = Modulo.MathRound(TotCal - tot, 2)

                Dim k As New ClsUSL
                k.CodiceRegione = CodiceRegione(I)
                k.Leggi(STRINGACONNESSIONEDB)
                RicalcoloGriglia_Descrizione(IndiceVariazione) = k.Nome

                If CreoAddAccr = 1 And CodiceRegione(I) <> "" Then
                    Dim MyAddAcc As New Cls_AddebitiAccrediti


                    MyAddAcc.CENTROSERVIZIO = Cserv
                    MyAddAcc.CodiceOspite = CodOsp
                    MyAddAcc.Descrizione = Tdesc
                    MyAddAcc.ANNOCOMPETENZA = Anno
                    MyAddAcc.MESECOMPETENZA = Mese
                    If TotCal - tot > 0 Then
                        MyAddAcc.TipoMov = "AD"
                    Else
                        MyAddAcc.TipoMov = "AC"
                    End If

                    MyAddAcc.CodiceIva = CampoParametri("NCDeposito")
                    Dim TRegione As New ClsUSL

                    TRegione.CodiceRegione = CodiceRegione(I)
                    TRegione.Leggi(STRINGACONNESSIONEDB)


                    Dim TipoOperazione As New Cls_TipoOperazione

                    TipoOperazione.Codice = TRegione.TipoOperazione
                    TipoOperazione.Leggi(STRINGACONNESSIONEDB, TipoOperazione.Codice)

                    If TipoOperazione.TipoAddebitoAccredito <> "" Then
                        MyAddAcc.CodiceIva = TipoOperazione.TipoAddebitoAccredito
                    End If

                    MyAddAcc.RETTA = "S"
                    MyAddAcc.IMPORTO = Modulo.MathRound(Math.Abs(TotCal - tot), 2)
                    MyAddAcc.PROVINCIA = ""
                    MyAddAcc.COMUNE = ""
                    MyAddAcc.REGIONE = CodiceRegione(I)
                    MyAddAcc.PARENTE = 0
                    MyAddAcc.RIFERIMENTO = "R"
                    MyAddAcc.Data = Format(DataMov, "dd/MM/yyyy")
                    MyAddAcc.Progressivo = ProgressivoADDACC()
                    MyAddAcc.Utente = "EmissioneAddebitiAccrediti"

                    MyAddAcc.InserisciAddebito(STRINGACONNESSIONEDB)

                    StringaDegliErrori = StringaDegliErrori & "Periodo " & Mese & "/" & Anno & "  Creato Movimento numero : " & MyAddAcc.Progressivo & vbNewLine

                    RicalcoloGriglia_NumeroMovimento(IndiceVariazione) = MyAddAcc.Progressivo

                    'Rs_AddebitiAccrediti.Open("Select * From ADDACR Where CodiceOspite = 0", OspitiDb, ADODB.CursorTypeEnum.adOpenKeyset, ADODB.LockTypeEnum.adLockOptimistic)
                    'Rs_AddebitiAccrediti.AddNew()
                    'MoveToDb(Rs_AddebitiAccrediti.Fields("CentroServizio"), Cserv)
                    'MoveToDb(Rs_AddebitiAccrediti.Fields("CodiceOspite"), CodOsp)
                    'MoveToDb(Rs_AddebitiAccrediti.Fields("Descrizione"), Tdesc) '"Movimento Automatico"
                    'MoveToDb(Rs_AddebitiAccrediti.Fields("ANNOCOMPETENZA"), Anno)
                    'MoveToDb(Rs_AddebitiAccrediti.Fields("MESECOMPETENZA"), Mese)
                    'If TotCal - tot > 0 Then
                    '    MoveToDb(Rs_AddebitiAccrediti.Fields("TipoMov"), "AD")
                    'Else
                    '    MoveToDb(Rs_AddebitiAccrediti.Fields("TipoMov"), "AC")
                    'End If
                    'MoveToDb(Rs_AddebitiAccrediti.Fields("Retta"), "S")
                    'MoveToDb(Rs_AddebitiAccrediti.Fields("Importo"), Modulo.MathRound(Math.Abs(TotCal - tot), 2))
                    'MoveToDb(Rs_AddebitiAccrediti.Fields("Provincia"), "")
                    'MoveToDb(Rs_AddebitiAccrediti.Fields("Comune"), "")
                    'MoveToDb(Rs_AddebitiAccrediti.Fields("Regione"), CodiceRegione(I))
                    'MoveToDb(Rs_AddebitiAccrediti.Fields("Parente"), 0)
                    'MoveToDb(Rs_AddebitiAccrediti.Fields("Riferimento"), "R")
                    'MoveToDb(Rs_AddebitiAccrediti.Fields("Data"), Format(DataMov, "dd/MM/yyyy"))
                    'MoveToDb(Rs_AddebitiAccrediti.Fields("Progressivo"), ProgressivoADDACC())
                    'MoveToDb(Rs_AddebitiAccrediti.Fields("Utente"), "EmissioneAddebitiAccrediti")
                    'MoveToDb(Rs_AddebitiAccrediti.Fields("DataAggiornamento"), Now)
                    'Rs_AddebitiAccrediti.Update()


                    'StringaDegliErrori = StringaDegliErrori & "Periodo " & Mese & "/" & Anno & "  Creato Movimento numero : " & MoveFromDb(Rs_AddebitiAccrediti.Fields("Progressivo")) & vbNewLine

                    'RicalcoloGriglia_NumeroMovimento(IndiceVariazione) = MoveFromDb(Rs_AddebitiAccrediti.Fields("Progressivo"))

                    'Rs_AddebitiAccrediti.Close()

                End If
            End If
        Next I
    End Sub


    Public Sub MioCalcolaRettaDaTabella(ByVal Cserv As String, ByVal CodOsp As Long, ByVal Anno As Long, ByVal Mese As Long, ByRef SC1 As Object)
        Dim SalvaParente2(10) As Single, SalvaParente(10) As Single, SalvaPerc(10) As Single        
        Dim TReg As Integer, TCom As Integer, I As Integer, Ext As Integer, XS As Integer
        Dim AutoNonAuto As String, SalvaCodiceComune As String, SalvaCodiceRegione As String, SalvaMod As String = "", SalvaTipoImportoRegione = ""

        Dim SalvaCodiceJolly As String


        Dim SalvaComune As Single, SalvaRegione As Single, SalvaOspite As Single, SalvaOspite2 As Single, SalvaEnte As Single, SalvaRetta As Single

        Dim SalvaJolly As Single
        REM XSalvaEnte As Single
        Dim XSalvaComune As Single, XSalvaRegione As Single, XSalvaOspite As Single, XSalvaOspite2 As Single, XSalvaRetta As Single

        Dim XSalvaJolly As Single


        Dim XSalvaParente(10) As Single, XSalvaPerc(10) As Single
        Dim XSalvaParente2(10) As Single

        Dim XSalvaExtImporto1 As Double, XSalvaExtImporto2 As Double, XSalvaExtImporto3 As Double, XSalvaExtImporto4 As Double
        Dim XSalvaExtImporto1C As Double, XSalvaExtImporto2C As Double, XSalvaExtImporto3C As Double, XSalvaExtImporto4C As Double
        Dim XSalvaExtImporto1J As Double, XSalvaExtImporto2J As Double, XSalvaExtImporto3J As Double, XSalvaExtImporto4J As Double

        Dim PXSalvaExtImporto1(10) As Double, PXSalvaExtImporto2(10) As Double, PXSalvaExtImporto3(10) As Double, PXSalvaExtImporto4(10) As Double

        Dim Par As Integer, ImpPERC As Single
        REM Dim DetrComune As Single, MaxDetrOspite As Single, MaxDetrParente As Single, DetrTotale As Single

        REM Dim DetrJolly As Single


        Dim WParente As Single, WSaveParente As Single, WPerc As Single
        REM Dim ImpDetrOspite As Double, ImpDetrParente As Double, ImpDetrComune As Double, ImpDetrRegione As Double

        REM Dim ImpDetrJolly As Double
        REM MioSalva As Single, DetrazioneTot As Double, Detraz As Double, 
        Dim SommoGiorni As Boolean
        REM TipoUscita As String
        Dim Codice As String, Regola As String, TipoMov As String = ""
        Dim VARIABILE1 As Double, Variabile2 As Double, Variabile3 As Double, Variabile4 As Double

        Dim WSalvaComune As Double ', WSalvaCodiceComune As Double, WSalvaCodiceRegione As Double

        Dim WSalvaJolly As Double

        Dim WSalvaRegione As Double, WSalvaOspite As Double, WSalvaOspite2 As Double, WSalvaEnte As Double, WSalvaRetta As Double, MyCausaleFlag As Integer

        Dim SCSalvaComune As Double, SCSalvaRegione As Double, SCSalvaOspite As Double, SCSalvaOspite2 As Double, SCSalvaEnte As Double

        Dim SCSalvaJolly As Double

        Dim SCSalvaParente(10) As Double, SCSalvaParente2(10) As Double, SCSalvaImpExtrParente(10) As Double, SCSalvaImpExtrOspite(10) As Double, SCSalvaImpExtrComune(10) As Double, SCSalvaPerc(10) As Double

        Dim SCSalvaImpExtrJolly(10) As Double

        Dim CheckT As String = ""

        Dim TJol As Long

        Dim xMese As Integer
        Dim xAnno As Integer

        Dim GiornoCalcolati As Integer

        '
        '   Azzero Variabili Per totale Importo
        '


        Dim OspiteAnagrafico As New ClsOspite

        OspiteAnagrafico.CodiceOspite = CodOsp
        OspiteAnagrafico.Leggi(STRINGACONNESSIONEDB, CodOsp)




        Dim cmdMovimentiAccolto As New OleDbCommand()
        cmdMovimentiAccolto.CommandText = "Select * From Movimenti Where TIPOMOV = '05' and CODICEOSPITE = " & CodOsp & " And CENTROSERVIZIO = '" & Cserv & "' And month(DATA) = " & Mese & " And year(Data) = " & Anno
        cmdMovimentiAccolto.Connection = OspitiCon
        Dim RDMovimentiAccolto As OleDbDataReader = cmdMovimentiAccolto.ExecuteReader()
        If RDMovimentiAccolto.Read() Then
            Accolto = True
        Else
            Accolto = False
        End If
        RDMovimentiAccolto.Close()



        Dim cmdMovimentiDimesso As New OleDbCommand()
        cmdMovimentiDimesso.CommandText = "Select * From Movimenti Where TIPOMOV = '13' and CODICEOSPITE = " & CodOsp & " And CENTROSERVIZIO = '" & Cserv & "' And month(DATA) = " & Mese & " And year(Data) = " & Anno
        cmdMovimentiDimesso.Connection = OspitiCon
        Dim RDMovimentiDimesso As OleDbDataReader = cmdMovimentiDimesso.ExecuteReader()
        If RDMovimentiDimesso.Read() Then
            Dimesso = True
        Else
            Dimesso = False
        End If
        RDMovimentiAccolto.Close()



        MyCausaleFlag = 0
        GiorniPres = 0 : GiorniAss = 0

        GiorniPresEnte = 0 : GiorniAssEnte = 0
        NonGiorniPres = 0 : NonGiorniAss = 0
        NonGiorniPresEnte = 0 : NonGiorniAssEnte = 0
        VARIABILE1 = 0
        Variabile2 = 0
        Variabile3 = 0
        Variabile4 = 0
        For I = 0 To 10
            GiorniPresExtraC(I) = 0
            ImportoPresComune(I) = 0 : ImportoAssComune(I) = 0

            ImportoPresJolly(I) = 0 : ImportoAssJolly(I) = 0

            ImportoPresRegione(I) = 0 : ImportoAssRegione(I) = 0

            NonImportoPresComune(I) = 0 : NonImportoAssComune(I) = 0

            NonImportoPresJolly(I) = 0 : NonImportoAssJolly(I) = 0

            NonImportoPresRegione(I) = 0 : NonImportoAssRegione(I) = 0
            GiorniPresParente(I) = 0 : GiorniAssParente(I) = 0

            GiorniPresComune(I) = 0 : GiorniAssComune(I) = 0

            GiorniPresJolly(I) = 0 : GiorniAssJolly(I) = 0

            GiorniPresRegione(I) = 0 : GiorniAssRegione(I) = 0
            NonGiorniPresParente(I) = 0 : NonGiorniAssParente(I) = 0

            NonGiorniPresComune(I) = 0 : NonGiorniAssComune(I) = 0

            NonGiorniPresJolly(I) = 0 : NonGiorniAssJolly(I) = 0

            NonGiorniPresRegione(I) = 0 : NonGiorniAssRegione(I) = 0
            For Ext = 0 To 10
                NonImpExtraOspite(Ext) = 0 : ImpExtraOspite(Ext) = 0
                NonImpExtraParente(I, Ext) = 0 : ImpExtraParente(I, Ext) = 0
                NonImpExtraComune(I, Ext) = 0 : ImpExtraComune(I, Ext) = 0

                NonImpExtraJolly(I, Ext) = 0 : ImpExtraJolly(I, Ext) = 0
            Next Ext

            PXSalvaExtImporto1(I) = 0 : PXSalvaExtImporto2(I) = 0
            PXSalvaExtImporto3(I) = 0 : PXSalvaExtImporto4(I) = 0

            ImportoPresParente(I) = 0 : ImportoAssParente(I) = 0
            NonImportoPresParente(I) = 0 : NonImportoAssParente(I) = 0
            SalvaImpExtrOspite(I) = 0
            SalvaImpExtrParent(I) = 0

            SalvaImpExtrComune(I) = 0

            SalvaImpExtrJolly(I) = 0

            SalvaParente(I) = 0
            SalvaParente2(I) = 0
        Next I

        ImportoPresEnte = 0 : ImportoAssEnte = 0
        NonImportoPresEnte = 0 : NonImportoAssEnte = 0
        ImportoPresOspite = 0 : ImportoAssOspite = 0
        NonImportoPresOspite = 0 : NonImportoAssOspite = 0

        ImportoPresOspite2 = 0 : ImportoAssOspite2 = 0
        NonImportoPresOspite2 = 0 : NonImportoAssOspite2 = 0

        '
        '   Azzero Variabili Per Importo
        '
        AutoNonAuto = ""
        SalvaComune = 0

        SalvaJolly = 0

        SalvaCodiceComune = ""

        SalvaCodiceJolly = ""

        SalvaCodiceRegione = ""
        SalvaRegione = 0
        SalvaOspite = 0
        SalvaOspite2 = 0
        SalvaEnte = 0
        SalvaRetta = 0
        XSalvaExtImporto1 = 0 : XSalvaExtImporto2 = 0 : XSalvaExtImporto3 = 0 : XSalvaExtImporto4 = 0



        Codice = ""
        Regola = ""
        SommoGiorni = True
        For I = 1 To 31


            If MyTabAutoSufficente(I) <> "" And MyTabAutoSufficente(I) <> " " Then
                AutoNonAuto = MyTabAutoSufficente(I)
            End If

            If I = 18 Then
                I = 18
            End If

            'If MyTabCausale(i) <> "*" Then
            For XS = 0 To 10
                If MyTabImpExtrOspite(I, XS) <> -1 Then
                    SalvaImpExtrOspite(XS) = MyTabImpExtrOspite(I, XS)
                End If
                If MyTabImpExtrParent(I, XS) <> -1 Then
                    SalvaImpExtrParent(XS) = MyTabImpExtrParent(I, XS)
                End If
                If MyTabImpExtrComune(I, XS) <> -1 Then
                    SalvaImpExtrComune(XS) = MyTabImpExtrComune(I, XS)
                End If

                If MyTabImpExtrJolly(I, XS) <> -1 Then
                    SalvaImpExtrJolly(XS) = MyTabImpExtrJolly(I, XS)
                End If
            Next XS

            If MyTabAutoSufficente(I) <> "" Then
                AutoNonAuto = MyTabAutoSufficente(I)
            End If

            If MyTabImportoRegione(I) <> -1 Then
                XSalvaRegione = MyTabImportoRegione(I)
                If MyTabCodiceRegione(I) <> "****" Then
                    SalvaCodiceRegione = MyTabCodiceRegione(I)
                    SalvaTipoImportoRegione = MyTabRegioneTipoImporto(I)
                    InserisciRegione(SalvaCodiceRegione, SalvaTipoImportoRegione)
                End If
            End If

            If MyTabImportoJolly(I) <> -1 Then
                XSalvaJolly = MyTabImportoJolly(I)

                XSalvaExtImporto1J = MyTabGiornalieroExtraImporto1J(I)
                XSalvaExtImporto2J = MyTabGiornalieroExtraImporto2J(I)
                XSalvaExtImporto3J = MyTabGiornalieroExtraImporto3J(I)
                XSalvaExtImporto4J = MyTabGiornalieroExtraImporto4J(I)

                SalvaCodiceJolly = MyTabCodiceProvJolly(I) & MyTabCodiceJolly(I)
                InserisciJolly(SalvaCodiceJolly)
            End If


            If MyTabImportoComune(I) <> -1 Then
                XSalvaComune = MyTabImportoComune(I)

                XSalvaExtImporto1C = MyTabGiornalieroExtraImporto1C(I)
                XSalvaExtImporto2C = MyTabGiornalieroExtraImporto2C(I)
                XSalvaExtImporto3C = MyTabGiornalieroExtraImporto3C(I)
                XSalvaExtImporto4C = MyTabGiornalieroExtraImporto4C(I)

                SalvaCodiceComune = MyTabCodiceProv(I) & MyTabCodiceComune(I)
                InserisciComune(SalvaCodiceComune)
            End If


            If MyTabImportoOspite(I) <> -1 Then
                XSalvaOspite = MyTabImportoOspite(I)
                XSalvaOspite2 = MyTabImportoOspite2(I)

                XSalvaExtImporto1 = MyTabGiornalieroExtraImporto1(I)
                XSalvaExtImporto2 = MyTabGiornalieroExtraImporto2(I)
                XSalvaExtImporto3 = MyTabGiornalieroExtraImporto3(I)
                XSalvaExtImporto4 = MyTabGiornalieroExtraImporto4(I)
            End If


            For Par = 0 To MaxParenti
                If MyTabPercentuale(I, Par) <> -1 Then
                    XSalvaParente(Par) = MyTabImportoParente(I, Par)
                    XSalvaParente2(Par) = MyTabImportoParente2(I, Par)
                    XSalvaPerc(Par) = MyTabPercentuale(I, Par)

                    PXSalvaExtImporto1(Par) = MyTabGiornalieroExtraImporto1P(I, Par)
                    PXSalvaExtImporto2(Par) = MyTabGiornalieroExtraImporto2P(I, Par)
                    PXSalvaExtImporto3(Par) = MyTabGiornalieroExtraImporto3P(I, Par)
                    PXSalvaExtImporto4(Par) = MyTabGiornalieroExtraImporto4P(I, Par)
                End If
            Next Par

            If MyTabImportoRetta(I) <> -1 Then
                XSalvaRetta = MyTabImportoRetta(I)
            End If
            If MyTabModalita(I) <> "" Then
                SalvaMod = MyTabModalita(I)
            End If


            If SalvaMod = "O" Then
                WParente = 0
                For Par = 0 To MaxParenti
                    WParente = WParente + XSalvaParente(Par)
                    WParente = WParente + XSalvaParente2(Par)
                    SalvaParente(Par) = XSalvaParente(Par)
                    SalvaParente2(Par) = XSalvaParente2(Par)
                Next Par
                SalvaOspite = XSalvaRetta - XSalvaComune - XSalvaRegione - WParente - XSalvaJolly
                If Prm_AzzeraSeNegativo = 1 Then
                    If SalvaOspite < 0 Then SalvaOspite = 0
                End If
                SalvaRetta = XSalvaRetta
                SalvaComune = XSalvaComune

                SalvaJolly = XSalvaJolly
                SalvaOspite2 = XSalvaOspite2
                SalvaRegione = XSalvaRegione
            End If
            If SalvaMod = "P" Then
                WParente = XSalvaRetta - XSalvaOspite - XSalvaRegione - XSalvaComune - XSalvaJolly - XSalvaOspite2
                WSaveParente = WParente
                WPerc = 0
                For Par = 0 To MaxParenti
                    WPerc = WPerc + XSalvaPerc(Par)
                    If WPerc = 100 Then
                        SalvaParente(Par) = WParente
                        WParente = 0
                    Else
                        SalvaParente(Par) = Format(Modulo.MathRound(WSaveParente * XSalvaPerc(Par) / 100, 2), "#,##0.00")
                        WParente = WParente - SalvaParente(Par)
                    End If
                    If Prm_AzzeraSeNegativo = 1 Then
                        If SalvaParente(Par) < 0 Then SalvaParente(Par) = 0
                    End If
                    SalvaParente2(Par) = XSalvaParente2(Par)
                Next Par
          
                SalvaRetta = XSalvaRetta

                SalvaComune = XSalvaComune

                SalvaJolly = XSalvaJolly

                SalvaRegione = XSalvaRegione
                SalvaOspite = XSalvaOspite
                SalvaOspite2 = XSalvaOspite2
            End If
            If SalvaMod = "C" Then
                WParente = 0
                For Par = 0 To MaxParenti
                    WParente = WParente + XSalvaParente(Par) + XSalvaParente2(Par)
                    SalvaParente(Par) = XSalvaParente(Par)
                    SalvaParente2(Par) = XSalvaParente2(Par)
                Next Par

                SalvaComune = XSalvaRetta - XSalvaOspite - XSalvaRegione - WParente - XSalvaJolly - XSalvaOspite2

                If Prm_AzzeraSeNegativo = 1 Then
                    If SalvaComune < 0 Then SalvaComune = 0
                End If
                SalvaRetta = XSalvaRetta

                SalvaJolly = XSalvaJolly

                SalvaRegione = XSalvaRegione
                SalvaOspite = XSalvaOspite
                SalvaOspite2 = XSalvaOspite2
            End If
            If SalvaMod = "E" Then
                WParente = 0
                For Par = 0 To MaxParenti
                    WParente = WParente + XSalvaParente(Par) + XSalvaParente2(Par)
                    SalvaParente(Par) = XSalvaParente(Par)
                    SalvaParente2(Par) = XSalvaParente2(Par)
                Next Par
                SalvaEnte = Modulo.MathRound(XSalvaRetta - XSalvaOspite - XSalvaComune - XSalvaJolly - WParente - XSalvaRegione - XSalvaOspite2, 5)
                If Prm_AzzeraSeNegativo = 1 Then
                    If SalvaEnte < 0 Then SalvaEnte = 0
                End If
                SalvaRetta = XSalvaRetta
                SalvaComune = XSalvaComune

                SalvaJolly = XSalvaJolly

                SalvaRegione = XSalvaRegione
                SalvaOspite = XSalvaOspite
                SalvaOspite2 = XSalvaOspite2
            End If
            'End If

            If MyTabCausale(I) = "" Then
                Codice = ""
                If AutoNonAuto = "A" And MyTabCausale(I) <> "*" Then
                    TCom = TrovaComune(SalvaCodiceComune)
                    ImportoPresComune(TCom) = ImportoPresComune(TCom) + SalvaComune


                    TJol = TrovaJolly(SalvaCodiceJolly)
                    ImportoPresJolly(TJol) = ImportoPresJolly(TJol) + SalvaJolly


                    TReg = TrovaRegione(SalvaCodiceRegione, SalvaTipoImportoRegione)
                    ImportoPresRegione(TReg) = ImportoPresRegione(TReg) + SalvaRegione
                    TipoImportoPresRegione(TReg) = SalvaTipoImportoRegione

                    ImportoPresOspite = ImportoPresOspite + SalvaOspite
                    ImportoPresOspite2 = ImportoPresOspite2 + SalvaOspite2

                    ImportoPresEnte = ImportoPresEnte + SalvaEnte

                    For Ext = 0 To 10
                        ImpExtraOspite(Ext) = ImpExtraOspite(Ext) + SalvaImpExtrOspite(Ext)
                        If SalvaImpExtrOspite(Ext) > 0 Then
                            MyTabCodiceExtrOspiteGIORNI(Ext) = MyTabCodiceExtrOspiteGIORNI(Ext) + 1
                        End If

                        ImpExtraComune(TCom, Ext) = ImpExtraComune(TCom, Ext) + SalvaImpExtrComune(Ext)

                        If SalvaImpExtrComune(Ext) > 0 Then
                            MyTabCodiceExtrComuneGIORNI(Ext) = MyTabCodiceExtrComuneGIORNI(Ext) + 1
                        End If

                        ImpExtraJolly(TJol, Ext) = ImpExtraJolly(TJol, Ext) + SalvaImpExtrJolly(Ext)

                        If Modulo.MathRound(SalvaImpExtrComune(Ext), 2) > 0 Then GiorniPresExtraC(Ext) = GiorniPresExtraC(Ext) + 1
                    Next Ext

                    For Par = 0 To MaxParenti
                        For Ext = 0 To 10
                            If SalvaImpExtrParent(Ext) <> 0 Then
                                ImpPERC = Format(Modulo.MathRound(SalvaImpExtrParent(Ext) * XSalvaPerc(Par) / 100, 2), "#,##0.00")
                                ImpExtraParente(Par, Ext) = ImpExtraParente(Par, Ext) + ImpPERC
                                If ImpPERC > 0 Then
                                    MyTabCodiceExtrParentGIORNI(Par, Ext) = MyTabCodiceExtrParentGIORNI(Par, Ext) + 1
                                End If
                            End If
                        Next Ext
                        ImportoPresParente(Par) = ImportoPresParente(Par) + SalvaParente(Par)
                        ImportoPresParente2(Par) = ImportoPresParente2(Par) + SalvaParente2(Par)

                        If SalvaParente(Par) > 0 Then
                            GiorniPresParente(Par) = GiorniPresParente(Par) + 1
                        End If
                    Next Par
                    If SommoGiorni Then
                        If Modulo.MathRound(SalvaOspite, 2) <> 0 Then GiorniPres = GiorniPres + 1
                        If SalvaEnte <> 0 Then GiorniPresEnte = GiorniPresEnte + 1
                        If Modulo.MathRound(SalvaComune, 2) <> 0 Then GiorniPresComune(TCom) = GiorniPresComune(TCom) + 1

                        If Modulo.MathRound(SalvaJolly, 2) <> 0 Then GiorniPresJolly(TJol) = GiorniPresJolly(TJol) + 1

                        If Modulo.MathRound(SalvaRegione, 2) <> 0 Then GiorniPresRegione(TReg) = GiorniPresRegione(TReg) + 1
                    End If
                End If
                If AutoNonAuto = "N" And MyTabCausale(I) <> "*" Then
                    TCom = TrovaComune(SalvaCodiceComune)
                    NonImportoPresComune(TCom) = NonImportoPresComune(TCom) + SalvaComune

                    TJol = TrovaJolly(SalvaCodiceJolly)
                    NonImportoPresJolly(TJol) = NonImportoPresJolly(TJol) + SalvaJolly


                    TReg = TrovaRegione(SalvaCodiceRegione, SalvaTipoImportoRegione)
                    NonImportoPresRegione(TReg) = NonImportoPresRegione(TReg) + SalvaRegione
                    TipoImportoPresRegione(TReg) = SalvaTipoImportoRegione
                    NonImportoPresOspite = NonImportoPresOspite + SalvaOspite
                    NonImportoPresOspite2 = NonImportoPresOspite2 + SalvaOspite2
                    NonImportoPresEnte = NonImportoPresEnte + SalvaEnte
                    For Ext = 0 To 10
                        NonImpExtraOspite(Ext) = NonImpExtraOspite(Ext) + SalvaImpExtrOspite(Ext)
                        If SalvaImpExtrOspite(Ext) > 0 Then
                            MyTabCodiceNonExtrOspiteGIORNI(Ext) = MyTabCodiceNonExtrOspiteGIORNI(Ext) + 1
                        End If

                        NonImpExtraComune(TCom, Ext) = NonImpExtraComune(TCom, Ext) + SalvaImpExtrComune(Ext)

                        If SalvaImpExtrComune(Ext) > 0 Then
                            MyTabCodiceExtrComuneGIORNI(Ext) = MyTabCodiceExtrComuneGIORNI(Ext) + 1
                        End If

                        NonImpExtraJolly(TJol, Ext) = NonImpExtraJolly(TJol, Ext) + SalvaImpExtrJolly(Ext)

                        If Modulo.MathRound(SalvaImpExtrComune(Ext), 2) > 0 Then GiorniPresExtraC(Ext) = GiorniPresExtraC(Ext) + 1
                    Next Ext

                    For Par = 0 To MaxParenti
                        For Ext = 0 To 10
                            If SalvaImpExtrParent(Ext) <> 0 Then
                                ImpPERC = Format(Modulo.MathRound(SalvaImpExtrParent(Ext) * XSalvaPerc(Par) / 100, 4), "#,##0.0000")
                                NonImpExtraParente(Par, Ext) = NonImpExtraParente(Par, Ext) + ImpPERC
                                If ImpPERC > 0 Then
                                    MyTabCodiceExtrParentNonGIORNI(Par, Ext) = MyTabCodiceExtrParentNonGIORNI(Par, Ext) + 1
                                End If
                            End If
                        Next Ext
                        NonImportoPresParente(Par) = NonImportoPresParente(Par) + SalvaParente(Par)
                        NonImportoPresParente2(Par) = NonImportoPresParente2(Par) + SalvaParente2(Par)

                        If SalvaParente(Par) > 0 Then
                            NonGiorniPresParente(Par) = NonGiorniPresParente(Par) + 1
                        End If
                    Next Par

                    If SommoGiorni Then
                        If Modulo.MathRound(SalvaOspite, 2) <> 0 Then NonGiorniPres = NonGiorniPres + 1
                        If SalvaEnte <> 0 Then NonGiorniPresEnte = NonGiorniPresEnte + 1
                        If Modulo.MathRound(SalvaComune, 2) <> 0 Then NonGiorniPresComune(TCom) = NonGiorniPresComune(TCom) + 1

                        If Modulo.MathRound(SalvaJolly, 2) <> 0 Then NonGiorniPresJolly(TJol) = NonGiorniPresJolly(TJol) + 1

                        If Modulo.MathRound(SalvaRegione, 2) <> 0 Then NonGiorniPresRegione(TReg) = NonGiorniPresRegione(TReg) + 1
                    End If
                End If
            End If



            If MyTabCausale(I) <> "" And MyTabCausale(I) <> "*" Then

                WSalvaComune = Val(SalvaComune)

                WSalvaJolly = Val(SalvaJolly)

                WSalvaRegione = Val(SalvaRegione)
                WSalvaOspite = Val(SalvaOspite)
                WSalvaOspite2 = Val(SalvaOspite2)
                WSalvaEnte = Val(SalvaEnte)
                WSalvaRetta = Val(SalvaRetta)

                If MyTabCausale(I) <> Codice Then
                    MyCausaleFlag = 0


                    'Dim cmdCausale As New OleDbCommand()
                    'cmdCausale.CommandText = "Select * From Causali Where " & "CODICE = '" & MyTabCausale(I) & "'"
                    'cmdCausale.Connection = OspitiCon
                    'Dim RDCausale As OleDbDataReader = cmdCausale.ExecuteReader()
                    'If RDCausale.Read() Then
                    '    Codice = campodb(RDCausale.Item("Codice"))
                    '    Regola = campodb(RDCausale.Item("Regole"))
                    '    CheckT = campodb(RDCausale.Item("ChekCausale"))
                    '    If MyTabGiornoAssenza(I) < 2 Then
                    '        TipoMov = campodb(RDCausale.Item("GiornoUscita"))
                    '    Else
                    '        TipoMov = campodb(RDCausale.Item("TIPOMOVIMENTO"))
                    '    End If
                    'Else
                    '    StringaDegliErrori = StringaDegliErrori & " Causale " & MyTabCausale(I) & " uscita non presente x Ospite :" & CampoOspite(CodOsp, "NOME", False) & Chr(13)
                    'End If
                    'RDCausale.Close()

                    Dim NIDCausale As Integer = NumeroDaCodice(MyTabCausale(I))
                    Codice = MyTabCausale(I)

                    Regola = Vt_CausaleRegola(NIDCausale)
                    CheckT = Vt_CausaleCheck(NIDCausale)
                    If MyTabGiornoAssenza(I) < 2 Then
                        TipoMov = Vt_GiornoUscita(NIDCausale)
                    Else
                        TipoMov = Vt_CausaleTimpoMovimento(NIDCausale)
                    End If                
                Else
                    MyCausaleFlag = 1


                    Dim NIDCausale As Integer = NumeroDaCodice(MyTabCausale(I))

                    TipoMov = Vt_CausaleTimpoMovimento(NIDCausale)

                    'Dim cmdCausale As New OleDbCommand()
                    'cmdCausale.CommandText = "Select TIPOMOVIMENTO From Causali Where " & "CODICE = '" & MyTabCausale(I) & "'"
                    'cmdCausale.Connection = OspitiCon
                    'Dim RDCausale As OleDbDataReader = cmdCausale.ExecuteReader()
                    'If RDCausale.Read() Then
                    '    TipoMov = campodb(RDCausale.Item("TIPOMOVIMENTO"))
                    'End If
                    'RDCausale.Close()
                End If

                    If Trim(Regola) = "" Then
                        SCSalvaComune = SalvaComune

                        SCSalvaJolly = SalvaJolly

                        SCSalvaRegione = SalvaRegione
                    SCSalvaOspite = SalvaOspite
                    SCSalvaOspite2 = SalvaOspite2
                        SCSalvaEnte = SalvaEnte

                        For Ext = 0 To 10
                            SCSalvaParente(Ext) = SalvaParente(Ext)
                            SCSalvaImpExtrOspite(Ext) = SalvaImpExtrOspite(Ext)
                            SCSalvaImpExtrComune(Ext) = SalvaImpExtrComune(Ext)
                            SCSalvaImpExtrParente(Ext) = SalvaImpExtrParent(Ext)
                            SCSalvaImpExtrJolly(Ext) = SalvaImpExtrJolly(Ext)

                            SCSalvaPerc(Ext) = SalvaPerc(Ext)
                        Next
                    End If

                    If Trim(Regola) <> "" Then

                        SC1.Reset()
                        'SC1.AddObject("Form", Maschera)



                        If InStr(1, CheckT, "A") > 0 Then
                            SC1.ExecuteStatement("DIM SalvaImpExtrOspite(20)")
                            SC1.ExecuteStatement("DIM CodiceExtrOspite(20)")

                            SC1.ExecuteStatement("DIM SalvaImpExtrComune(20)")
                            SC1.ExecuteStatement("DIM CodiceExtrComune(20)")

                            SC1.ExecuteStatement("DIM SalvaImpExtrJolly(20)")

                            SC1.ExecuteStatement("DIM SalvaImpExtrParent(20)")
                            SC1.ExecuteStatement("DIM CodiceExtrParent(20)")
                        End If

                        If InStr(1, CheckT, "B") > 0 Then
                        SC1.ExecuteStatement("DIM SalvaParente (20)")
                        SC1.ExecuteStatement("DIM SalvaParente2(20)")
                            SC1.ExecuteStatement("DIM SalvaPerc(20)")
                        End If


                        If InStr(1, CheckT, "C") > 0 Then
                            SC1.ExecuteStatement("DIM GiorniPresParente(10)")
                            SC1.ExecuteStatement("DIM GiorniAssParente (10)")

                            SC1.ExecuteStatement("DIM GiorniPresComune (10)")
                            SC1.ExecuteStatement("DIM GiorniAssComune (10)")

                            SC1.ExecuteStatement("DIM GiorniPresJolly (10)")
                            SC1.ExecuteStatement("DIM GiorniAssJolly (10)")

                            SC1.ExecuteStatement("DIM GiorniPresRegione (10)")
                            SC1.ExecuteStatement("DIM GiorniAssRegione (10)")
                        End If


                        If InStr(1, CheckT, "D") > 0 Then
                            SC1.ExecuteStatement("DIM NonGiorniPresParente (10)")
                            SC1.ExecuteStatement("DIM NonGiorniAssParente (10)")

                            SC1.ExecuteStatement("DIM NonGiorniPresComune (10)")
                            SC1.ExecuteStatement("DIM NonGiorniAssComune (10)")

                            SC1.ExecuteStatement("DIM NonGiorniPresJolly (10)")
                            SC1.ExecuteStatement("DIM NonGiorniAssJolly (10)")


                            SC1.ExecuteStatement("DIM NonGiorniPresRegione (10)")
                            SC1.ExecuteStatement("DIM NonGiorniAssRegione (10)")
                        End If


                        If InStr(1, CheckT, "E") > 0 Then
                            SC1.ExecuteStatement("DIM CodiceComune(10)")

                            SC1.ExecuteStatement("DIM CodiceJolly(10)")

                            SC1.ExecuteStatement("DIM CodiceRegione(10)")
                        End If

                        If InStr(1, CheckT, "F") > 0 Then
                            SC1.ExecuteStatement("DIM pXSalvaExtImporto1(10)")
                            SC1.ExecuteStatement("DIM pXSalvaExtImporto2(10)")
                            SC1.ExecuteStatement("DIM pXSalvaExtImporto3(10)")
                            SC1.ExecuteStatement("DIM pXSalvaExtImporto4(10)")

                            SC1.ExecuteStatement("DIM ImpExtrParMan1(10)")
                            SC1.ExecuteStatement("DIM ImpExtrParMan2(10)")
                            SC1.ExecuteStatement("DIM ImpExtrParMan3(10)")
                            SC1.ExecuteStatement("DIM ImpExtrParMan4(10)")

                            SC1.ExecuteStatement("DIM NumExtrParMan1(10)")
                            SC1.ExecuteStatement("DIM NumExtrParMan2(10)")
                            SC1.ExecuteStatement("DIM NumExtrParMan3(10)")
                            SC1.ExecuteStatement("DIM NumExtrParMan4(10)")

                            SC1.ExecuteStatement("DIM TipoExtrParMan1(10)")
                            SC1.ExecuteStatement("DIM TipoExtrParMan2(10)")
                            SC1.ExecuteStatement("DIM TipoExtrParMan3(10)")
                            SC1.ExecuteStatement("DIM TipoExtrParMan4(10)")


                        End If

                        If InStr(1, CheckT, "M") > 0 Then
                            SC1.ExecuteStatement("DIM TipoExtrComMan1(10)")
                            SC1.ExecuteStatement("DIM TipoExtrComMan2(10)")
                            SC1.ExecuteStatement("DIM TipoExtrComMan3(10)")
                            SC1.ExecuteStatement("DIM TipoExtrComMan4(10)")
                        End If


                    Try
                        SC1.AddCode(Regola)
                    Catch ex As Exception

                    End Try


                        If Trim(AutoNonAuto) <> "" Then SC1.ExecuteStatement("Stato =" & AutoNonAuto)

                        SC1.ExecuteStatement("GiornoElab = #" & Replace(Format(DateSerial(Anno, Mese, I), "MM/dd/yyyy"), ".", "/") & "#")


                        If InStr(1, CheckT, "G") > 0 Then
                            TCom = TrovaComune(SalvaCodiceComune)
                            SC1.ExecuteStatement("NumeroComune  =" & TCom)

                            TJol = TrovaJolly(SalvaCodiceJolly)
                            SC1.ExecuteStatement("NumeroJolly  =" & TJol)

                            TReg = TrovaRegione(SalvaCodiceRegione, SalvaTipoImportoRegione)
                            SC1.ExecuteStatement("NumeroRegione = " & TReg)
                        End If


                    If InStr(1, CheckT, "H") > 0 Then
                        SC1.ExecuteStatement("SalvaComune = " & Replace(SalvaComune, ",", "."))

                        SC1.ExecuteStatement("SalvaJolly = " & Replace(SalvaJolly, ",", "."))

                        SC1.ExecuteStatement("SalvaRegione = " & Replace(SalvaRegione, ",", "."))
                        SC1.ExecuteStatement("SalvaOspite = " & Replace(SalvaOspite, ",", "."))
                        SC1.ExecuteStatement("SalvaOspite2 = " & Replace(SalvaOspite2, ",", "."))
                        SC1.ExecuteStatement("SalvaEnte = " & Replace(SalvaEnte, ",", "."))
                    Else
                        SCSalvaComune = SalvaComune

                        SCSalvaJolly = SalvaJolly

                        SCSalvaRegione = SalvaRegione
                        SCSalvaOspite = SalvaOspite
                        SCSalvaOspite2 = SalvaOspite2
                        SCSalvaEnte = SalvaEnte
                    End If

                    If InStr(1, CheckT, "I") > 0 Then
                        SC1.ExecuteStatement("Variabile1 = " & Replace(VARIABILE1, ",", "."))
                        SC1.ExecuteStatement("Variabile2 = " & Replace(Variabile2, ",", "."))
                        SC1.ExecuteStatement("Variabile3 = " & Replace(Variabile3, ",", "."))
                        SC1.ExecuteStatement("Variabile4 = " & Replace(Variabile4, ",", "."))
                    End If

                    SC1.ExecuteStatement("GiorniAssenza = " & MyTabGiornoAssenza(I))
                    SC1.ExecuteStatement("GiorniRipetizioneCausale = " & MyTabGiorniRipetizioneCausale(I))
                    SC1.ExecuteStatement("GiorniRipetizioneCausaleNC = " & MyTabGiorniRipetizioneCausaleNC(I))
                    SC1.ExecuteStatement("MyTabGiorniRipetizioneCausaleACSum = " & MyTabGiorniRipetizioneCausaleACSum(I))


                    If InStr(1, CheckT, "L") > 0 Then
                        SC1.ExecuteStatement("XSalvaExtImporto1 = " & Replace(XSalvaExtImporto1, ",", "."))
                        SC1.ExecuteStatement("XSalvaExtImporto2 = " & Replace(XSalvaExtImporto2, ",", "."))
                        SC1.ExecuteStatement("XSalvaExtImporto3 = " & Replace(XSalvaExtImporto3, ",", "."))
                        SC1.ExecuteStatement("XSalvaExtImporto4 = " & Replace(XSalvaExtImporto4, ",", "."))

                        SC1.ExecuteStatement("ImpExtrOspMan1 = " & Replace(ImpExtrOspMan1, ",", "."))
                        SC1.ExecuteStatement("ImpExtrOspMan2 = " & Replace(ImpExtrOspMan2, ",", "."))
                        SC1.ExecuteStatement("ImpExtrOspMan3 = " & Replace(ImpExtrOspMan3, ",", "."))
                        SC1.ExecuteStatement("ImpExtrOspMan4 = " & Replace(ImpExtrOspMan4, ",", "."))

                        SC1.ExecuteStatement("NumExtrOspMan1 = " & Replace(NumExtrOspMan1, ",", "."))
                        SC1.ExecuteStatement("NumExtrOspMan2 = " & Replace(NumExtrOspMan2, ",", "."))
                        SC1.ExecuteStatement("NumExtrOspMan3 = " & Replace(NumExtrOspMan3, ",", "."))
                        SC1.ExecuteStatement("NumExtrOspMan4 = " & Replace(NumExtrOspMan4, ",", "."))

                        SC1.ExecuteStatement("TipoAddExtrOspMan1 = " & Chr(34) & TipoAddExtrOspMan1 & Chr(34))
                        SC1.ExecuteStatement("TipoAddExtrOspMan2 = " & Chr(34) & TipoAddExtrOspMan2 & Chr(34))
                        SC1.ExecuteStatement("TipoAddExtrOspMan3 = " & Chr(34) & TipoAddExtrOspMan3 & Chr(34))
                        SC1.ExecuteStatement("TipoAddExtrOspMan4 = " & Chr(34) & TipoAddExtrOspMan4 & Chr(34))
                    End If

                    If InStr(1, CheckT, "M") > 0 Then
                        SC1.ExecuteStatement("XSalvaExtImporto1C = " & Replace(XSalvaExtImporto1C, ",", "."))
                        SC1.ExecuteStatement("XSalvaExtImporto2C = " & Replace(XSalvaExtImporto2C, ",", "."))
                        SC1.ExecuteStatement("XSalvaExtImporto3C = " & Replace(XSalvaExtImporto3C, ",", "."))
                        SC1.ExecuteStatement("XSalvaExtImporto4C = " & Replace(XSalvaExtImporto4C, ",", "."))

                        SC1.ExecuteStatement("XSalvaExtImporto1J = " & Replace(XSalvaExtImporto1J, ",", "."))
                        SC1.ExecuteStatement("XSalvaExtImporto2J = " & Replace(XSalvaExtImporto2J, ",", "."))
                        SC1.ExecuteStatement("XSalvaExtImporto3J = " & Replace(XSalvaExtImporto3J, ",", "."))
                        SC1.ExecuteStatement("XSalvaExtImporto4J = " & Replace(XSalvaExtImporto4J, ",", "."))

                        SC1.ExecuteStatement("DIM ImpExtrComMan1(10)")
                        SC1.ExecuteStatement("DIM ImpExtrComMan2(10)")
                        SC1.ExecuteStatement("DIM ImpExtrComMan3(10)")
                        SC1.ExecuteStatement("DIM ImpExtrComMan4(10)")

                        SC1.ExecuteStatement("DIM NumExtrComMan1(10)")
                        SC1.ExecuteStatement("DIM NumExtrComMan2(10)")
                        SC1.ExecuteStatement("DIM NumExtrComMan3(10)")
                        SC1.ExecuteStatement("DIM NumExtrComMan4(10)")


                        For Par = 0 To 10
                            SC1.ExecuteStatement("ImpExtrComMan1(" & Par & ") = " & Replace(ImpExtrComMan1(Par), ",", "."))
                            SC1.ExecuteStatement("ImpExtrComMan2(" & Par & ") = " & Replace(ImpExtrComMan2(Par), ",", "."))
                            SC1.ExecuteStatement("ImpExtrComMan3(" & Par & ") = " & Replace(ImpExtrComMan3(Par), ",", "."))
                            SC1.ExecuteStatement("ImpExtrComMan4(" & Par & ") = " & Replace(ImpExtrComMan4(Par), ",", "."))

                            SC1.ExecuteStatement("NumExtrComMan1(" & Par & ") = " & Replace(NumExtrComMan1(Par), ",", "."))
                            SC1.ExecuteStatement("NumExtrComMan2(" & Par & ") = " & Replace(NumExtrComMan2(Par), ",", "."))
                            SC1.ExecuteStatement("NumExtrComMan3(" & Par & ") = " & Replace(NumExtrComMan3(Par), ",", "."))
                            SC1.ExecuteStatement("NumExtrComMan4(" & Par & ") = " & Replace(NumExtrComMan4(Par), ",", "."))

                            SC1.ExecuteStatement("TipoExtrComMan1(" & Par & ") = " & Chr(34) & TipoExtrComMan1(Par) & Chr(34))
                            SC1.ExecuteStatement("TipoExtrComMan2(" & Par & ") = " & Chr(34) & TipoExtrComMan2(Par) & Chr(34))
                            SC1.ExecuteStatement("TipoExtrComMan3(" & Par & ") = " & Chr(34) & TipoExtrComMan3(Par) & Chr(34))
                            SC1.ExecuteStatement("TipoExtrComMan4(" & Par & ") = " & Chr(34) & TipoExtrComMan4(Par) & Chr(34))
                        Next
                    End If


                        'If InStr(1, CheckT, "N") > 0 Then
                        SC1.ExecuteStatement("Mese = " & Mese)
                        SC1.ExecuteStatement("Anno = " & Anno)
                        SC1.ExecuteStatement("Cserv = " & Chr(34) & Cserv & Chr(34))
                        SC1.ExecuteStatement("CodiceOspite = " & CodOsp)
                        'End If

                        If InStr(1, CheckT, "O") > 0 Then
                            SC1.ExecuteStatement("GiorniPres = " & GiorniPres)
                            SC1.ExecuteStatement("GiorniAss = " & GiorniAss)
                            SC1.ExecuteStatement("GiorniPresEnte = " & GiorniPresEnte)
                            SC1.ExecuteStatement("GiorniAssEnte = " & GiorniAssEnte)
                        End If

                        If InStr(1, CheckT, "P") > 0 Then
                            SC1.ExecuteStatement("NonGiorniPres = " & NonGiorniPres)
                            SC1.ExecuteStatement("NonGiorniAss = " & NonGiorniAss)
                            SC1.ExecuteStatement("NonGiorniPresEnte = " & NonGiorniPresEnte)
                            SC1.ExecuteStatement("NonGiorniAssEnte = " & NonGiorniAssEnte)
                    End If
                    If InStr(1, CheckT, "-") > 0 Then
                        SC1.ExecuteStatement("ComuneResidenza = " & Chr(34) & OspiteAnagrafico.RESIDENZAPROVINCIA1 & OspiteAnagrafico.RESIDENZACOMUNE1 & Chr(34))

                    End If

                    If InStr(1, CheckT, "F") > 0 Then
                        For Par = 1 To 10
                            SC1.ExecuteStatement("pXSalvaExtImporto1(" & Par & ") = " & Replace(PXSalvaExtImporto1(Par), ",", "."))
                            SC1.ExecuteStatement("pXSalvaExtImporto2(" & Par & ") = " & Replace(PXSalvaExtImporto2(Par), ",", "."))
                            SC1.ExecuteStatement("pXSalvaExtImporto3(" & Par & ") = " & Replace(PXSalvaExtImporto3(Par), ",", "."))
                            SC1.ExecuteStatement("pXSalvaExtImporto4(" & Par & ") = " & Replace(PXSalvaExtImporto4(Par), ",", "."))

                            SC1.ExecuteStatement("ImpExtrParMan1(" & Par & ") = " & Replace(ImpExtrParMan1(Par), ",", "."))
                            SC1.ExecuteStatement("ImpExtrParMan2(" & Par & ") = " & Replace(ImpExtrParMan2(Par), ",", "."))
                            SC1.ExecuteStatement("ImpExtrParMan3(" & Par & ") = " & Replace(ImpExtrParMan3(Par), ",", "."))
                            SC1.ExecuteStatement("ImpExtrParMan4(" & Par & ") = " & Replace(ImpExtrParMan4(Par), ",", "."))

                            SC1.ExecuteStatement("NumExtrParMan1(" & Par & ") = " & Replace(NumExtrParMan1(Par), ",", "."))
                            SC1.ExecuteStatement("NumExtrParMan2(" & Par & ") = " & Replace(NumExtrParMan2(Par), ",", "."))
                            SC1.ExecuteStatement("NumExtrParMan3(" & Par & ") = " & Replace(NumExtrParMan3(Par), ",", "."))
                            SC1.ExecuteStatement("NumExtrParMan4(" & Par & ") = " & Replace(NumExtrParMan4(Par), ",", "."))

                            SC1.ExecuteStatement("TipoExtrParMan1(" & Par & ") = " & Chr(34) & TipoExtrParMan1(Par) & Chr(34))
                            SC1.ExecuteStatement("TipoExtrParMan2(" & Par & ") = " & Chr(34) & TipoExtrParMan2(Par) & Chr(34))
                            SC1.ExecuteStatement("TipoExtrParMan3(" & Par & ") = " & Chr(34) & TipoExtrParMan3(Par) & Chr(34))
                            SC1.ExecuteStatement("TipoExtrParMan4(" & Par & ") = " & Chr(34) & TipoExtrParMan4(Par) & Chr(34))
                        Next
                    End If


                        If InStr(1, CheckT, "E") > 0 Or InStr(1, CheckT, "C") > 0 Or InStr(1, CheckT, "D") > 0 Then
                            For Ext = 1 To 10
                                If InStr(1, CheckT, "E") > 0 Then
                                    If CodiceComune(Ext) <> "" Then SC1.ExecuteStatement("CodiceComune(" & Ext & ") = " & Chr(34) & CodiceComune(Ext) & Chr(34))

                                    If CodiceJolly(Ext) <> "" Then SC1.ExecuteStatement("CodiceJolly(" & Ext & ") = " & Chr(34) & CodiceJolly(Ext) & Chr(34))

                                    If CodiceRegione(Ext) <> "" Then SC1.ExecuteStatement("CodiceRegione(" & Ext & ") = " & Chr(34) & CodiceRegione(Ext) & Chr(34))
                                End If
                                If InStr(1, CheckT, "C") > 0 Then
                                    SC1.ExecuteStatement("GiorniPresParente(" & Ext & ") = " & GiorniPresParente(Ext))
                                    SC1.ExecuteStatement("GiorniAssParente (" & Ext & ") = " & GiorniAssParente(Ext))

                                    SC1.ExecuteStatement("GiorniPresComune (" & Ext & ") = " & GiorniPresComune(Ext))
                                    SC1.ExecuteStatement("GiorniAssComune (" & Ext & ") = " & GiorniAssComune(Ext))

                                    SC1.ExecuteStatement("GiorniPresJolly (" & Ext & ") = " & GiorniPresJolly(Ext))
                                    SC1.ExecuteStatement("GiorniAssJolly (" & Ext & ") = " & GiorniAssJolly(Ext))

                                    SC1.ExecuteStatement("GiorniPresRegione (" & Ext & ") = " & GiorniPresRegione(Ext))
                                    SC1.ExecuteStatement("GiorniAssRegione (" & Ext & ") = " & GiorniAssRegione(Ext))
                                End If

                                If InStr(1, CheckT, "D") > 0 Then
                                    SC1.ExecuteStatement("NonGiorniPresParente (" & Ext & ") = " & NonGiorniPresParente(Ext))
                                    SC1.ExecuteStatement("NonGiorniAssParente (" & Ext & ") = " & NonGiorniAssParente(Ext))

                                    SC1.ExecuteStatement("NonGiorniPresComune (" & Ext & ") = " & NonGiorniPresComune(Ext))
                                    SC1.ExecuteStatement("NonGiorniAssComune (" & Ext & ") = " & NonGiorniAssComune(Ext))

                                    SC1.ExecuteStatement("NonGiorniPresJolly (" & Ext & ") = " & NonGiorniPresJolly(Ext))
                                    SC1.ExecuteStatement("NonGiorniAssJolly (" & Ext & ") = " & NonGiorniAssJolly(Ext))

                                    SC1.ExecuteStatement("NonGiorniPresRegione (" & Ext & ") = " & NonGiorniPresRegione(Ext))
                                    SC1.ExecuteStatement("NonGiorniAssRegione (" & Ext & ") = " & NonGiorniAssRegione(Ext))
                                End If
                            Next Ext
                        End If


                        If InStr(1, CheckT, "A") > 0 Or InStr(1, CheckT, "B") > 0 Then
                            For Ext = 0 To 10
                                If InStr(1, CheckT, "A") > 0 Then
                                    SC1.ExecuteStatement("SalvaImpExtrOspite(" & Ext & ") = " & Replace(SalvaImpExtrOspite(Ext), ",", "."))
                                    If Trim(MyTabCodiceExtrOspite(Ext)) <> "" Then SC1.ExecuteStatement("CodiceExtrOspite(" & Ext & ") = " & Chr(34) & MyTabCodiceExtrOspite(Ext) & Chr(34))

                                    SC1.ExecuteStatement("SalvaImpExtrComune(" & Ext & ") = " & Replace(SalvaImpExtrComune(Ext), ",", "."))
                                    If Trim(MyTabCodiceExtrComune(Ext)) <> "" Then SC1.ExecuteStatement("CodiceExtrComune(" & Ext & ") = " & Chr(34) & MyTabCodiceExtrComune(Ext) & Chr(34))

                                    SC1.ExecuteStatement("SalvaImpExtrJolly(" & Ext & ") = " & Replace(SalvaImpExtrJolly(Ext), ",", "."))


                                    If IsDBNull(SalvaImpExtrParent(Ext)) Or Val(SalvaImpExtrParent(Ext)) = 0 Then
                                        SC1.ExecuteStatement("SalvaImpExtrParent (" & Ext & ") = 0")
                                    Else
                                        SC1.ExecuteStatement("SalvaImpExtrParent(" & Ext & ") = " & Replace(SalvaImpExtrParent(Ext), ",", "."))
                                    End If
                                    If Trim(MyTabCodiceExtrParent(Ext)) <> "" Then SC1.ExecuteStatement("CodiceExtrParent(" & Ext & ") = " & Chr(34) & MyTabCodiceExtrParent(Ext) & Chr(34))

                                Else
                                    SCSalvaImpExtrOspite(Ext) = SalvaImpExtrOspite(Ext)
                                    SCSalvaImpExtrParente(Ext) = SalvaImpExtrParent(Ext)
                                    SCSalvaImpExtrComune(Ext) = SalvaImpExtrComune(Ext)

                                    SCSalvaImpExtrJolly(Ext) = SalvaImpExtrJolly(Ext)

                                End If

                                If InStr(1, CheckT, "B") > 0 Then
                                SC1.ExecuteStatement("SalvaParente(" & Ext & ") = " & Replace(SalvaParente(Ext), ",", "."))
                                SC1.ExecuteStatement("SalvaParente2(" & Ext & ") = " & Replace(SalvaParente2(Ext), ",", "."))
                                    SC1.ExecuteStatement("SalvaPerc(" & Ext & ") = " & Replace(XSalvaPerc(Ext), ",", "."))
                                Else
                                SCSalvaParente(Ext) = SalvaParente(Ext)
                                SCSalvaParente2(Ext) = SalvaParente2(Ext)
                                    SCSalvaPerc(Ext) = SalvaPerc(Ext)
                                End If
                            Next
                        Else
                            For Ext = 0 To 10
                            SCSalvaParente(Ext) = SalvaParente(Ext)
                            SCSalvaParente2(Ext) = SalvaParente2(Ext)
                                SCSalvaImpExtrOspite(Ext) = SalvaImpExtrOspite(Ext)
                                SCSalvaImpExtrComune(Ext) = SalvaImpExtrComune(Ext)
                                SCSalvaImpExtrParente(Ext) = SalvaImpExtrParent(Ext)
                                SCSalvaImpExtrJolly(Ext) = SalvaImpExtrJolly(Ext)

                                SCSalvaPerc(Ext) = SalvaPerc(Ext)
                            Next
                        End If


                    If Regola.ToUpper.IndexOf("TIPORETTA") > 0 Then
                        Dim VD As New Cls_rettatotale

                        VD.CENTROSERVIZIO = Cserv
                        VD.CODICEOSPITE = CodOsp
                        VD.RettaTotaleAData(STRINGACONNESSIONEDB, CodOsp, Cserv, DateSerial(Anno, Mese, I))
                        SC1.ExecuteStatement("TipoRetta = " & Chr(34) & VD.TipoRetta & Chr(34))
                    End If


                    If Regola.ToUpper.IndexOf("TIPOOPERAZIONE") > 0 Then
                        Dim VDOsp As New Cls_DatiOspiteParenteCentroServizio

                        VDOsp.CentroServizio = Cserv
                        VDOsp.CodiceOspite = CodOsp
                        VDOsp.Leggi(STRINGACONNESSIONEDB)
                        SC1.ExecuteStatement("TIPOOPERAZIONE = " & Chr(34) & VDOsp.TipoOperazione & Chr(34))
                    End If


                        Dim VerCserv As New Cls_CentroServizio

                        VerCserv.Leggi(STRINGACONNESSIONEDB, Cserv)

                    If VerCserv.TIPOCENTROSERVIZIO = "A" Then

                        Dim Rs_AssenzeCentroDiurno As New ADODB.Recordset
                        Dim Sql As String
                        Dim AppoggioOre(100) As Double
                        Dim AppoggioOpertore(100) As String
                        Dim AppoggioTipo(100) As String
                        Dim AppoggioCreatoDocumentoOspiti(100) As String

                        Dim Indice As Integer = 0
                        Dim xIndice As Integer = 0

                        SC1.ExecuteStatement("Dim DomOre(80)")
                        SC1.ExecuteStatement("Dim DomOpertore(80)")
                        SC1.ExecuteStatement("Dim DomTipo(80)")
                        SC1.ExecuteStatement("Dim DomCreatoDocumentoOspiti(80)")


                        Sql = "Select * From MovimentiDomiciliare Where CENTROSERVIZIO = '" & Cserv & "' And CODICEOSPITE = " & CodOsp & " And Data = ? Order by OraInizio"


                        Dim cmd As New OleDbCommand()
                        cmd.CommandText = Sql
                        cmd.Parameters.AddWithValue("@Data", DateSerial(Anno, Mese, I))
                        cmd.Connection = OspitiCon
                        Dim ReaderAssenzeCentroDiurno As OleDbDataReader = cmd.ExecuteReader()
                        Do While ReaderAssenzeCentroDiurno.Read
                            AppoggioOre(Indice) = DateDiff("n", campodbd(ReaderAssenzeCentroDiurno.Item("OraInizio")), campodbd(ReaderAssenzeCentroDiurno.Item("OraFine")))
                            AppoggioOpertore(Indice) = campodb(ReaderAssenzeCentroDiurno.Item("Operatore"))
                            AppoggioTipo(Indice) = campodb(ReaderAssenzeCentroDiurno.Item("Tipologia"))
                            AppoggioCreatoDocumentoOspiti(Indice) = campodbN(ReaderAssenzeCentroDiurno.Item("CreatoDocumentoOspiti"))
                            Indice = Indice + 1
                        Loop
                        ReaderAssenzeCentroDiurno.Close()

                        For xIndice = 0 To Indice
                            SC1.ExecuteStatement("DomOre(" & xIndice & ") = " & AppoggioOre(xIndice))
                            SC1.ExecuteStatement("DomOpertore(" & xIndice & ") = " & Chr(34) & AppoggioOpertore(xIndice) & Chr(34))
                            SC1.ExecuteStatement("DomTipo(" & xIndice & ") = " & Chr(34) & AppoggioTipo(xIndice) & Chr(34))
                            SC1.ExecuteStatement("DomCreatoDocumentoOspiti(" & xIndice & ") = " & AppoggioCreatoDocumentoOspiti(xIndice))
                        Next
                    End If



                        Try
                            If Not IsDBNull(Regola) And Regola <> "" Then SC1.Run("Calcolo")
                        Catch ex As Exception
                            StringaDegliErrori = StringaDegliErrori & " Errore in Regola Causale " & MyTabCausale(I) & "-" & ex.Message
                        End Try

                        REM If Not IsDBNull(Regola) And Regola <> "" Then SC1.Run("Calcolo")

                        If InStr(1, CheckT, "I") > 0 Then
                            VARIABILE1 = SC1.Eval("Variabile1")
                            Variabile2 = SC1.Eval("Variabile2")
                            Variabile3 = SC1.Eval("Variabile3")
                            Variabile4 = SC1.Eval("Variabile4")
                        End If

                        If InStr(1, CheckT, "L") > 0 Then
                            ImpExtrOspMan1 = SC1.Eval("ImpExtrOspMan1")
                            ImpExtrOspMan2 = SC1.Eval("ImpExtrOspMan2")
                            ImpExtrOspMan3 = SC1.Eval("ImpExtrOspMan3")
                            ImpExtrOspMan4 = SC1.Eval("ImpExtrOspMan4")

                            NumExtrOspMan1 = SC1.Eval("NumExtrOspMan1")
                            NumExtrOspMan2 = SC1.Eval("NumExtrOspMan2")
                            NumExtrOspMan3 = SC1.Eval("NumExtrOspMan3")
                            NumExtrOspMan4 = SC1.Eval("NumExtrOspMan4")

                            TipoAddExtrOspMan1 = SC1.Eval("TipoAddExtrOspMan1")
                            TipoAddExtrOspMan2 = SC1.Eval("TipoAddExtrOspMan2")
                            TipoAddExtrOspMan3 = SC1.Eval("TipoAddExtrOspMan3")
                            TipoAddExtrOspMan4 = SC1.Eval("TipoAddExtrOspMan4")

                        End If

                        If InStr(1, CheckT, "M") > 0 Then
                            For Par = 0 To 10
                                ImpExtrComMan1(Par) = SC1.Eval("ImpExtrComMan1(" & Par & ")")
                                ImpExtrComMan2(Par) = SC1.Eval("ImpExtrComMan2(" & Par & ")")
                                ImpExtrComMan3(Par) = SC1.Eval("ImpExtrComMan3(" & Par & ")")
                                ImpExtrComMan4(Par) = SC1.Eval("ImpExtrComMan4(" & Par & ")")

                                NumExtrComMan1(Par) = SC1.Eval("NumExtrComMan1(" & Par & ")")
                                NumExtrComMan2(Par) = SC1.Eval("NumExtrComMan2(" & Par & ")")
                                NumExtrComMan3(Par) = SC1.Eval("NumExtrComMan3(" & Par & ")")
                                NumExtrComMan4(Par) = SC1.Eval("NumExtrComMan4(" & Par & ")")

                                TipoExtrComMan1(Par) = SC1.Eval("TipoExtrComMan1(" & Par & ")")
                                TipoExtrComMan2(Par) = SC1.Eval("TipoExtrComMan2(" & Par & ")")
                                TipoExtrComMan3(Par) = SC1.Eval("TipoExtrComMan3(" & Par & ")")
                                TipoExtrComMan4(Par) = SC1.Eval("TipoExtrComMan4(" & Par & ")")
                            Next
                        End If

                        If InStr(1, CheckT, "F") > 0 Then
                            For Par = 0 To 10
                                ImpExtrParMan1(Par) = SC1.Eval("ImpExtrParMan1 (" & Par & ")")
                                ImpExtrParMan2(Par) = SC1.Eval("ImpExtrParMan2 (" & Par & ")")
                                ImpExtrParMan3(Par) = SC1.Eval("ImpExtrParMan3 (" & Par & ")")
                                ImpExtrParMan4(Par) = SC1.Eval("ImpExtrParMan4 (" & Par & ")")

                                NumExtrParMan1(Par) = SC1.Eval("NumExtrParMan1 (" & Par & ")")
                                NumExtrParMan2(Par) = SC1.Eval("NumExtrParMan2 (" & Par & ")")
                                NumExtrParMan3(Par) = SC1.Eval("NumExtrParMan3 (" & Par & ")")
                                NumExtrParMan4(Par) = SC1.Eval("NumExtrParMan4 (" & Par & ")")

                                TipoExtrParMan1(Par) = SC1.Eval("TipoExtrParMan1 (" & Par & ")")
                                TipoExtrParMan2(Par) = SC1.Eval("TipoExtrParMan2 (" & Par & ")")
                                TipoExtrParMan3(Par) = SC1.Eval("TipoExtrParMan3 (" & Par & ")")
                                TipoExtrParMan4(Par) = SC1.Eval("TipoExtrParMan4 (" & Par & ")")
                            Next
                        End If

                        If InStr(1, CheckT, "O") > 0 Then
                            GiorniPres = SC1.Eval("GiorniPres")
                            GiorniAss = SC1.Eval("GiorniAss")
                            GiorniPresEnte = SC1.Eval("GiorniPresEnte")
                            GiorniAssEnte = SC1.Eval("GiorniAssEnte")
                        End If

                        If InStr(1, CheckT, "P") > 0 Then
                            NonGiorniPres = SC1.Eval("NonGiorniPres")
                            NonGiorniAss = SC1.Eval("NonGiorniAss")
                            NonGiorniPresEnte = SC1.Eval("NonGiorniPresEnte")
                            NonGiorniAssEnte = SC1.Eval("NonGiorniAssEnte")
                        End If

                        If InStr(1, CheckT, "C") > 0 Or InStr(1, CheckT, "D") > 0 Then
                            For Ext = 0 To 10
                                If InStr(1, CheckT, "C") > 0 Then
                                    GiorniPresParente(Ext) = SC1.Eval("GiorniPresParente(" & Ext & ")")
                                    GiorniAssParente(Ext) = SC1.Eval("GiorniAssParente (" & Ext & ")")

                                    GiorniPresComune(Ext) = SC1.Eval("GiorniPresComune (" & Ext & ")")
                                    GiorniAssComune(Ext) = SC1.Eval("GiorniAssComune (" & Ext & ")")


                                    GiorniPresJolly(Ext) = SC1.Eval("GiorniPresJolly (" & Ext & ")")
                                    GiorniAssJolly(Ext) = SC1.Eval("GiorniAssJolly (" & Ext & ")")


                                    GiorniPresRegione(Ext) = SC1.Eval("GiorniPresRegione (" & Ext & ")")
                                    GiorniAssRegione(Ext) = SC1.Eval("GiorniAssRegione (" & Ext & ")")
                                End If

                                If InStr(1, CheckT, "D") > 0 Then
                                    NonGiorniPresParente(Ext) = SC1.Eval("NonGiorniPresParente (" & Ext & ")")
                                    NonGiorniAssParente(Ext) = SC1.Eval("NonGiorniAssParente (" & Ext & ")")

                                    NonGiorniPresComune(Ext) = SC1.Eval("NonGiorniPresComune (" & Ext & ")")
                                    NonGiorniAssComune(Ext) = SC1.Eval("NonGiorniAssComune (" & Ext & ")")

                                    NonGiorniPresJolly(Ext) = SC1.Eval("NonGiorniPresJolly (" & Ext & ")")
                                    NonGiorniAssJolly(Ext) = SC1.Eval("NonGiorniAssJolly (" & Ext & ")")


                                    NonGiorniPresRegione(Ext) = SC1.Eval("NonGiorniPresRegione (" & Ext & ")")
                                    NonGiorniAssRegione(Ext) = SC1.Eval("NonGiorniAssRegione (" & Ext & ")")
                                End If
                            Next Ext
                        End If

                        If InStr(1, CheckT, "H") > 0 Then
                            SCSalvaComune = SC1.Eval("SalvaComune")

                            SCSalvaJolly = SC1.Eval("SalvaJolly")

                            SCSalvaRegione = SC1.Eval("SalvaRegione")
                            SCSalvaOspite = SC1.Eval("SalvaOspite")
                            SCSalvaOspite2 = SC1.Eval("SalvaOspite2")
                            SCSalvaEnte = SC1.Eval("SalvaEnte")
                        End If

                        If InStr(1, CheckT, "B") > 0 Or InStr(1, CheckT, "A") > 0 Then
                            For Ext = 0 To 10
                                If InStr(1, CheckT, "B") > 0 Then
                                    SCSalvaParente(Ext) = SC1.Eval("SalvaParente(" & Ext & ")")
                                    SCSalvaParente2(Ext) = SC1.Eval("SalvaParente2(" & Ext & ")")
                                    SCSalvaPerc(Ext) = SC1.Eval("SalvaPerc(" & Ext & ")")

                                End If
                                If InStr(1, CheckT, "A") > 0 Then
                                    SCSalvaImpExtrOspite(Ext) = SC1.Eval("SalvaImpExtrOspite(" & Ext & " )")
                                    SCSalvaImpExtrComune(Ext) = SC1.Eval("SalvaImpExtrComune(" & Ext & ")")
                                    SCSalvaImpExtrParente(Ext) = SC1.Eval("SalvaImpExtrParent(" & Ext & " )")

                                    SCSalvaImpExtrJolly(Ext) = SC1.Eval("SalvaImpExtrJolly(" & Ext & ")")
                                End If
                            Next
                        End If
                    End If
                    If AutoNonAuto = "N" Then
                        If TipoMov = "P" Or TipoMov = "" Then
                            TCom = TrovaComune(SalvaCodiceComune)
                            NonImportoPresComune(TCom) = NonImportoPresComune(TCom) + SCSalvaComune

                            TJol = TrovaJolly(SalvaCodiceJolly)
                            NonImportoPresJolly(TJol) = NonImportoPresJolly(TJol) + SCSalvaJolly

                            TReg = TrovaRegione(SalvaCodiceRegione, SalvaTipoImportoRegione)
                            NonImportoPresRegione(TReg) = NonImportoPresRegione(TReg) + SCSalvaRegione
                            TipoImportoPresRegione(TReg) = SalvaTipoImportoRegione
                            NonImportoPresOspite = NonImportoPresOspite + SCSalvaOspite
                            NonImportoPresOspite2 = NonImportoPresOspite2 + SCSalvaOspite2
                            NonImportoPresEnte = NonImportoPresEnte + SCSalvaEnte
                            For Ext = 0 To 10
                                NonImpExtraOspite(Ext) = NonImpExtraOspite(Ext) + SCSalvaImpExtrOspite(Ext)
                                If SCSalvaImpExtrOspite(Ext) > 0 Then
                                MyTabCodiceNonExtrOspiteGIORNI(Ext) = MyTabCodiceNonExtrOspiteGIORNI(Ext) + 1
                                End If

                                NonImpExtraComune(TCom, Ext) = NonImpExtraComune(TCom, Ext) + SCSalvaImpExtrComune(Ext)

                                If SCSalvaImpExtrComune(Ext) > 0 Then
                                    MyTabCodiceExtrComuneGIORNI(Ext) = MyTabCodiceExtrComuneGIORNI(Ext) + 1
                                End If

                                'NonImpExtraParente(1, Ext) = NonImpExtraParente(1, Ext) + SCSalvaImpExtrParente(Ext)
                                NonImpExtraJolly(TJol, Ext) = NonImpExtraJolly(TJol, Ext) + SCSalvaImpExtrJolly(Ext)
                            Next Ext
                            For Par = 0 To MaxParenti
                                For Ext = 0 To 10
                                    If SCSalvaImpExtrParente(Ext) <> 0 Then
                                        ImpPERC = Format(Modulo.MathRound(SalvaImpExtrParent(Ext) * SCSalvaPerc(Par) / 100, 2), "#,##0.00")
                                        NonImpExtraParente(Par, Ext) = NonImpExtraParente(Par, Ext) + ImpPERC
                                        If ImpPERC > 0 Then
                                        MyTabCodiceExtrParentNonGIORNI(Par, Ext) = MyTabCodiceExtrParentNonGIORNI(Par, Ext) + 1
                                        End If
                                    End If
                                Next Ext
                                NonImportoPresParente(Par) = NonImportoPresParente(Par) + SCSalvaParente(Par)
                                NonImportoPresParente2(Par) = NonImportoPresParente2(Par) + SCSalvaParente2(Par)
                                If SCSalvaParente(Par) > 0 Then
                                    If TipoMov = "P" Then
                                        NonGiorniPresParente(Par) = NonGiorniPresParente(Par) + 1
                                    End If
                                End If
                            Next Par
                        End If
                        If TipoMov = "A" Then
                            TCom = TrovaComune(SalvaCodiceComune)
                            NonImportoAssComune(TCom) = NonImportoAssComune(TCom) + SCSalvaComune

                            TJol = TrovaJolly(SalvaCodiceJolly)
                            NonImportoAssJolly(TJol) = NonImportoAssJolly(TJol) + SCSalvaJolly

                            TReg = TrovaRegione(SalvaCodiceRegione, SalvaTipoImportoRegione)
                            NonImportoAssRegione(TReg) = NonImportoAssRegione(TReg) + SCSalvaRegione
                            TipoImportoAssRegione(TReg) = SalvaTipoImportoRegione
                            NonImportoAssOspite = NonImportoAssOspite + SCSalvaOspite
                            NonImportoAssOspite2 = NonImportoAssOspite2 + SCSalvaOspite2
                            NonImportoAssEnte = NonImportoAssEnte + SCSalvaEnte
                            For Ext = 0 To 10
                                NonImpExtraOspite(Ext) = NonImpExtraOspite(Ext) + SCSalvaImpExtrOspite(Ext)

                                If SCSalvaImpExtrOspite(Ext) > 0 Then
                                MyTabCodiceNonExtrOspiteGIORNI(Ext) = MyTabCodiceNonExtrOspiteGIORNI(Ext) + 1
                                End If

                                NonImpExtraComune(TCom, Ext) = NonImpExtraComune(TCom, Ext) + SCSalvaImpExtrComune(Ext)

                                If SCSalvaImpExtrComune(Ext) > 0 Then
                                    MyTabCodiceExtrComuneGIORNI(Ext) = MyTabCodiceExtrComuneGIORNI(Ext) + 1
                                End If

                                'NonImpExtraParente(1, Ext) = NonImpExtraParente(1, Ext) + SCSalvaImpExtrParente(Ext)
                                NonImpExtraJolly(TJol, Ext) = NonImpExtraJolly(TJol, Ext) + SCSalvaImpExtrJolly(Ext)
                            Next Ext
                            For Par = 0 To MaxParenti
                                For Ext = 0 To 10
                                    If SCSalvaImpExtrParente(Ext) <> 0 Then
                                        ImpPERC = Format(Modulo.MathRound(SalvaImpExtrParent(Ext) * SCSalvaPerc(Par) / 100, 2), "#,##0.00")
                                        NonImpExtraParente(Par, Ext) = NonImpExtraParente(Par, Ext) + ImpPERC
                                        If ImpPERC > 0 Then
                                        MyTabCodiceExtrParentNonGIORNI(Par, Ext) = MyTabCodiceExtrParentNonGIORNI(Par, Ext) + 1
                                        End If
                                    End If
                                Next Ext
                                NonImportoAssParente(Par) = NonImportoAssParente(Par) + SCSalvaParente(Par)
                                NonImportoAssParente2(Par) = NonImportoAssParente2(Par) + SCSalvaParente2(Par)
                                If SalvaParente(Par) > 0 Then
                                    If TipoMov <> "N" Then
                                        NonGiorniAssParente(Par) = NonGiorniAssParente(Par) + 1
                                    End If
                                End If
                            Next Par
                        End If
                        If TipoMov = "P" Then
                            If Modulo.MathRound(SalvaOspite, 2) <> 0 Then NonGiorniPres = NonGiorniPres + 1
                            If SalvaEnte <> 0 Then NonGiorniPresEnte = NonGiorniPresEnte + 1

                            TCom = TrovaComune(SalvaCodiceComune)
                            If Modulo.MathRound(SalvaComune, 2) <> 0 Then NonGiorniPresComune(TCom) = NonGiorniPresComune(TCom) + 1

                            TJol = TrovaJolly(SalvaCodiceJolly)
                            If Modulo.MathRound(SalvaJolly, 2) <> 0 Then NonGiorniPresJolly(TJol) = NonGiorniPresJolly(TJol) + 1

                            If Modulo.MathRound(SalvaRegione, 2) <> 0 Then NonGiorniPresRegione(TReg) = NonGiorniPresRegione(TReg) + 1
                        End If
                        If TipoMov = "A" Then
                            If Modulo.MathRound(SalvaOspite, 2) <> 0 Then NonGiorniAss = NonGiorniAss + 1
                            If SalvaEnte <> 0 Then NonGiorniAssEnte = NonGiorniAssEnte + 1
                            TCom = TrovaComune(SalvaCodiceComune)
                            If Modulo.MathRound(SalvaComune, 2) <> 0 Then NonGiorniAssComune(TCom) = NonGiorniAssComune(TCom) + 1
                            TJol = TrovaJolly(SalvaCodiceJolly)
                            If Modulo.MathRound(SalvaJolly, 2) <> 0 Then NonGiorniAssJolly(TJol) = NonGiorniAssJolly(TJol) + 1

                            If Modulo.MathRound(SalvaRegione, 2) <> 0 Then NonGiorniAssRegione(TReg) = NonGiorniAssRegione(TReg) + 1
                        End If
                    Else
                        If TipoMov = "P" Or TipoMov = "" Then
                            TCom = TrovaComune(SalvaCodiceComune)
                            ImportoPresComune(TCom) = ImportoPresComune(TCom) + SCSalvaComune

                            TJol = TrovaJolly(SalvaCodiceJolly)
                            ImportoPresJolly(TJol) = ImportoPresJolly(TJol) + SCSalvaJolly

                            TReg = TrovaRegione(SalvaCodiceRegione, SalvaTipoImportoRegione)
                            ImportoPresRegione(TReg) = ImportoPresRegione(TReg) + SCSalvaRegione
                            TipoImportoPresRegione(TReg) = SalvaTipoImportoRegione
                            ImportoPresOspite = ImportoPresOspite + SCSalvaOspite
                            ImportoPresOspite2 = ImportoPresOspite2 + SCSalvaOspite2
                            ImportoPresEnte = ImportoPresEnte + SCSalvaEnte
                            For Ext = 0 To 10
                                ImpExtraOspite(Ext) = ImpExtraOspite(Ext) + SCSalvaImpExtrOspite(Ext)
                                If SCSalvaImpExtrOspite(Ext) > 0 Then
                                    MyTabCodiceExtrOspiteGIORNI(Ext) = MyTabCodiceExtrOspiteGIORNI(Ext) + 1
                                End If
                                ImpExtraComune(TCom, Ext) = ImpExtraComune(TCom, Ext) + SCSalvaImpExtrComune(Ext)
                                If SCSalvaImpExtrComune(Ext) > 0 Then
                                    MyTabCodiceExtrComuneGIORNI(Ext) = MyTabCodiceExtrComuneGIORNI(Ext) + 1
                                End If
                                'ImpExtraParente(1, Ext) = ImpExtraParente(1, Ext) + SCSalvaImpExtrParente(Ext)
                                ImpExtraJolly(TJol, Ext) = ImpExtraJolly(TJol, Ext) + SCSalvaImpExtrJolly(Ext)
                            Next Ext
                            For Par = 0 To MaxParenti
                                For Ext = 0 To 10
                                    If SCSalvaImpExtrParente(Ext) <> 0 Then
                                        ImpPERC = Format(Modulo.MathRound(SalvaImpExtrParent(Ext) * SCSalvaPerc(Par) / 100, 2), "#,##0.00")
                                        ImpExtraParente(Par, Ext) = ImpExtraParente(Par, Ext) + ImpPERC
                                        If ImpPERC > 0 Then
                                        MyTabCodiceExtrParentGIORNI(Par, Ext) = MyTabCodiceExtrParentGIORNI(Par, Ext) + 1
                                        End If
                                    End If
                                Next Ext
                                ImportoPresParente(Par) = ImportoPresParente(Par) + SCSalvaParente(Par)
                                ImportoPresParente2(Par) = ImportoPresParente2(Par) + SCSalvaParente2(Par)
                                If TipoMov = "P" Then
                                    If SCSalvaParente(Par) > 0 Then
                                        GiorniPresParente(Par) = GiorniPresParente(Par) + 1
                                    End If
                                End If
                            Next Par
                        End If
                        If TipoMov = "A" Then
                            TCom = TrovaComune(SalvaCodiceComune)
                            ImportoAssComune(TCom) = ImportoAssComune(TCom) + SCSalvaComune

                            TJol = TrovaJolly(SalvaCodiceJolly)
                            ImportoAssJolly(TJol) = ImportoAssJolly(TJol) + SCSalvaJolly


                            TReg = TrovaRegione(SalvaCodiceRegione, SalvaTipoImportoRegione)
                            ImportoAssRegione(TReg) = ImportoAssRegione(TReg) + SCSalvaRegione
                            TipoImportoAssRegione(TReg) = SalvaTipoImportoRegione
                            ImportoAssOspite = ImportoAssOspite + SCSalvaOspite
                            ImportoAssOspite2 = ImportoAssOspite2 + SCSalvaOspite2
                            ImportoAssEnte = ImportoAssEnte + SCSalvaEnte
                            For Ext = 0 To 10
                                ImpExtraOspite(Ext) = ImpExtraOspite(Ext) + SCSalvaImpExtrOspite(Ext)
                                If SCSalvaImpExtrOspite(Ext) > 0 Then
                                    MyTabCodiceExtrOspiteGIORNI(Ext) = MyTabCodiceExtrOspiteGIORNI(Ext) + 1
                                End If


                                ImpExtraComune(TCom, Ext) = ImpExtraComune(TCom, Ext) + SCSalvaImpExtrComune(Ext)
                                If SCSalvaImpExtrComune(Ext) > 0 Then
                                    MyTabCodiceExtrComuneGIORNI(Ext) = MyTabCodiceExtrComuneGIORNI(Ext) + 1
                                End If


                                ImpExtraJolly(TJol, Ext) = ImpExtraJolly(TJol, Ext) + SCSalvaImpExtrJolly(Ext)
                            Next Ext
                            For Par = 0 To MaxParenti
                                For Ext = 0 To 10
                                    If SCSalvaImpExtrParente(Ext) <> 0 Then
                                        ImpPERC = Format(Modulo.MathRound(SalvaImpExtrParent(Ext) * SCSalvaPerc(Par) / 100, 2), "#,##0.00")
                                        ImpExtraParente(Par, Ext) = ImpExtraParente(Par, Ext) + ImpPERC
                                        If ImpPERC > 0 Then
                                        MyTabCodiceExtrParentGIORNI(Par, Ext) = MyTabCodiceExtrParentGIORNI(Par, Ext) + 1
                                        End If
                                    End If
                                Next Ext
                                ImportoAssParente(Par) = ImportoAssParente(Par) + SCSalvaParente(Par)
                                ImportoAssParente2(Par) = ImportoAssParente2(Par) + SCSalvaParente2(Par)
                                If SCSalvaParente(Par) > 0 Then
                                    GiorniAssParente(Par) = GiorniAssParente(Par) + 1
                                End If
                            Next Par
                        End If
                        If TipoMov = "P" Then
                            If Modulo.MathRound(SalvaOspite, 2) <> 0 Then GiorniPres = GiorniPres + 1
                            If SalvaEnte <> 0 Then GiorniPresEnte = GiorniPresEnte + 1

                            If Modulo.MathRound(SalvaComune, 2) <> 0 Then GiorniPresComune(TCom) = GiorniPresComune(TCom) + 1

                            If Modulo.MathRound(SalvaJolly, 2) <> 0 Then GiorniPresJolly(TJol) = GiorniPresJolly(TJol) + 1

                            If Modulo.MathRound(SalvaRegione, 2) <> 0 Then GiorniPresRegione(TReg) = GiorniPresRegione(TReg) + 1
                        End If
                        If TipoMov = "A" Then
                            If Modulo.MathRound(SalvaOspite, 2) <> 0 Then GiorniAss = GiorniAss + 1
                            If SalvaEnte <> 0 Then GiorniAssEnte = GiorniAssEnte + 1
                            If Modulo.MathRound(SalvaComune, 2) <> 0 Then GiorniAssComune(TCom) = GiorniAssComune(TCom) + 1

                            If Modulo.MathRound(SalvaJolly, 2) <> 0 Then GiorniAssJolly(TJol) = GiorniAssJolly(TJol) + 1

                            If Modulo.MathRound(SalvaRegione, 2) <> 0 Then GiorniAssRegione(TReg) = GiorniAssRegione(TReg) + 1
                        End If
                    End If

                    SalvaComune = WSalvaComune

                    SalvaJolly = WSalvaJolly

                    SalvaRegione = WSalvaRegione
                    SalvaOspite = WSalvaOspite
                    SalvaOspite2 = WSalvaOspite2
                    SalvaEnte = WSalvaEnte
                    SalvaRetta = WSalvaRetta
                End If
        Next I
        I = I + 1


        ForzaRPXOspite = 0

        Regola = CampoCentroServizio(Cserv, "REGOLE")
        If Not IsDBNull(Regola) And Regola <> "" Then
            SC1.Reset()
            'SC1.AddObject("Form", Maschera)

            '************************************************************************
            '* Copio le variabili per VB Script
            '************************************************************************
            SC1.ExecuteStatement("DIM ImportoPresParente(10),ImportoAssParente(10) ")
            SC1.ExecuteStatement("DIM ImportoPresParente2(10),ImportoAssParente2(10) ")
            SC1.ExecuteStatement("DIM ImportoPresComune(10) , ImportoAssComune(10) ")

            SC1.ExecuteStatement("DIM ImportoPresJolly(10) , ImportoAssJolly(10) ")

            SC1.ExecuteStatement("DIM ImportoPresRegione(10) , ImportoAssRegione(10) ")
            SC1.ExecuteStatement("DIM NonImportoPresParente(10) , NonImportoAssParente(10) ")
            SC1.ExecuteStatement("DIM NonImportoPresParente2(10) , NonImportoAssParente2(10) ")

            SC1.ExecuteStatement("DIM NonImportoPresComune(10) , NonImportoAssComune(10) ")

            SC1.ExecuteStatement("DIM NonImportoPresJolly(10) , NonImportoAssJolly(10) ")

            SC1.ExecuteStatement("DIM NonImportoPresRegione(10) , NonImportoAssRegione(10) ")
            SC1.ExecuteStatement("DIM pXSalvaExtImporto1(10)")
            SC1.ExecuteStatement("DIM pXSalvaExtImporto2(10)")
            SC1.ExecuteStatement("DIM pXSalvaExtImporto3(10)")
            SC1.ExecuteStatement("DIM pXSalvaExtImporto4(10)")
            SC1.ExecuteStatement("DIM ParenteMensile(20)")
            SC1.ExecuteStatement("DIM ForzaRPXParente(10)")

            SC1.ExecuteStatement("DIM ImpExtrParMan1(10)")
            SC1.ExecuteStatement("DIM ImpExtrParMan2(10)")
            SC1.ExecuteStatement("DIM ImpExtrParMan3(10)")
            SC1.ExecuteStatement("DIM ImpExtrParMan4(10)")            
            SC1.ExecuteStatement("DIM QuotaParente(10)")

            SC1.ExecuteStatement("NonImportoPresOspite = " & Replace(Modulo.MathRound(NonImportoPresOspite, 2), ",", "."))
            SC1.ExecuteStatement("NonImportoAssOspite = " & Replace(Modulo.MathRound(NonImportoAssOspite, 2), ",", "."))
            SC1.ExecuteStatement("NonImportoPresEnte = " & Replace(Modulo.MathRound(NonImportoPresEnte, 2), ",", "."))
            SC1.ExecuteStatement("NonImportoAssEnte = " & Replace(Modulo.MathRound(NonImportoAssEnte, 2), ",", "."))
            SC1.ExecuteStatement("ImportoPresOspite = " & Replace(Modulo.MathRound(ImportoPresOspite, 2), ",", "."))
            SC1.ExecuteStatement("ImportoAssOspite = " & Replace(Modulo.MathRound(ImportoAssOspite, 2), ",", "."))
            SC1.ExecuteStatement("ImportoPresEnte =  " & Replace(Modulo.MathRound(ImportoPresEnte, 2), ",", "."))
            SC1.ExecuteStatement("ImportoAssEnte = " & Replace(Modulo.MathRound(ImportoAssEnte, 2), ",", "."))



            SC1.ExecuteStatement("OspiteMensile = " & Replace(MyTabOspitiMensile, ",", "."))

            SC1.ExecuteStatement("TotaleMensileRetta  = " & Replace(ImportoMensileRetta, ",", "."))
            SC1.ExecuteStatement("ImportoMensileComune = " & Replace(ImportoMensileComune, ",", "."))

            SC1.ExecuteStatement("XSalvaExtImporto1 = " & Replace(XSalvaExtImporto1, ",", "."))
            SC1.ExecuteStatement("XSalvaExtImporto2 = " & Replace(XSalvaExtImporto2, ",", "."))
            SC1.ExecuteStatement("XSalvaExtImporto3 = " & Replace(XSalvaExtImporto3, ",", "."))
            SC1.ExecuteStatement("XSalvaExtImporto4 = " & Replace(XSalvaExtImporto4, ",", "."))


            For Par = 1 To 10
                SC1.ExecuteStatement("pXSalvaExtImporto1(" & Par & ") = " & Replace(PXSalvaExtImporto1(Par), ",", "."))
                SC1.ExecuteStatement("pXSalvaExtImporto2(" & Par & ") = " & Replace(PXSalvaExtImporto1(Par), ",", "."))
                SC1.ExecuteStatement("pXSalvaExtImporto3(" & Par & ") = " & Replace(PXSalvaExtImporto1(Par), ",", "."))
                SC1.ExecuteStatement("pXSalvaExtImporto4(" & Par & ") = " & Replace(PXSalvaExtImporto1(Par), ",", "."))

                SC1.ExecuteStatement("ImpExtrParMan1(" & Par & ") = " & Replace(ImpExtrParMan1(Par), ",", "."))
                SC1.ExecuteStatement("ImpExtrParMan2(" & Par & ") = " & Replace(ImpExtrParMan2(Par), ",", "."))
                SC1.ExecuteStatement("ImpExtrParMan3(" & Par & ") = " & Replace(ImpExtrParMan3(Par), ",", "."))
                SC1.ExecuteStatement("ImpExtrParMan4(" & Par & ") = " & Replace(ImpExtrParMan4(Par), ",", "."))

                SC1.ExecuteStatement("ParenteMensile (" & Par & ") = " & Replace(MyTabParenteMensile(Par), ",", "."))


                Dim QuotaParente As Double
                If Regola.ToUpper.IndexOf("QUOTAPARENTE") > 0 Then
                    QuotaParente = QuoteGiornaliere(Cserv, CodOsp, "P", Par, DateSerial(Anno, Mese, GiorniMese(Mese, Anno)))
                End If
                SC1.ExecuteStatement("QuotaParente (" & Par & ") = " & Replace(QuotaParente, ",", "."))
            Next

            If Accolto = True Then
                SC1.ExecuteStatement("Accolto = 1")
            Else
                SC1.ExecuteStatement("Accolto = 0")
            End If
            If Dimesso = True Then
                SC1.ExecuteStatement("Dimesso = 1")
            Else
                SC1.ExecuteStatement("Dimesso = 0")
            End If
            mGiorniPres = 0
            mGiorniAss = 0
            For I = 1 To 31
                If Trim(MyTabCausale(I)) <> "*" Then
                    If Trim(MyTabCausale(I)) = "" Then
                        mGiorniPres = mGiorniPres + 1
                    Else
                        mGiorniAss = mGiorniAss + 1
                    End If
                End If
            Next I
            Dim QuotaOspite As Double

            If Regola.ToUpper.IndexOf("QUOTAOSPITE") > 0 Then
                QuotaOspite = QuoteGiornaliere(Cserv, CodOsp, "O", 0, DateSerial(Anno, Mese, GiorniMese(Mese, Anno)))
            End If

            SC1.ExecuteStatement("QuotaOspite = " & Replace(QuotaOspite, ",", "."))

            SC1.ExecuteStatement("mGiorniPres = " & Replace(mGiorniPres, ",", "."))
            SC1.ExecuteStatement("mGiorniAss = " & Replace(mGiorniAss, ",", "."))
            SC1.ExecuteStatement("GiorniPres = " & Replace(GiorniPres, ",", "."))
            SC1.ExecuteStatement("GiorniAss = " & Replace(GiorniAss, ",", "."))
            SC1.ExecuteStatement("NonGiorniPres = " & Replace(NonGiorniPres, ",", "."))
            SC1.ExecuteStatement("NonGiorniAss = " & Replace(NonGiorniAss, ",", "."))
            SC1.ExecuteStatement("Variabile1 = " & Replace(VARIABILE1, ",", "."))
            SC1.ExecuteStatement("Variabile2 = " & Replace(Variabile2, ",", "."))
            SC1.ExecuteStatement("Variabile3 = " & Replace(Variabile3, ",", "."))
            SC1.ExecuteStatement("Variabile4 = " & Replace(Variabile4, ",", "."))

            SC1.ExecuteStatement("GiorniAssenza = " & MyTabGiornoAssenza(31))
            SC1.ExecuteStatement("GiorniRipetizioneCausale = " & MyTabGiorniRipetizioneCausale(31))
            SC1.ExecuteStatement("GiorniRipetizioneCausaleNC = " & MyTabGiorniRipetizioneCausaleNC(31))
            SC1.ExecuteStatement("MyTabGiorniRipetizioneCausaleACSum = " & MyTabGiorniRipetizioneCausaleACSum(31))
            SC1.ExecuteStatement("Stato = " & Chr(34) & AutoNonAuto & Chr(34))
            SC1.ExecuteStatement("CodiceOspite =" & CodOsp)
            SC1.ExecuteStatement("Mese = " & Val(Mese))
            SC1.ExecuteStatement("Anno = " & Val(Anno))
            SC1.ExecuteStatement("GiorniMese =" & GiorniMese(Mese, Anno))


            SC1.ExecuteStatement("ParentePagante= 0")
            Dim XPar As New Cls_Modalita

            XPar.UltimaData(STRINGACONNESSIONEDB, CodOsp, Cserv)
            If XPar.MODALITA = "P" Then
                SC1.ExecuteStatement("ParentePagante= 1")
            End If



            SC1.ExecuteStatement("DIM CodiceComune(10)")

            SC1.ExecuteStatement("DIM CodiceJolly(10)")

            SC1.ExecuteStatement("DIM CodiceRegione(10)")
            SC1.ExecuteStatement("DIM ImportoExtrComune(10)")

            SC1.ExecuteStatement("DIM ImportoExtrJolly(10)")


            SC1.ExecuteStatement("DIM ImportoExtrParent(10)")
            SC1.ExecuteStatement("DIM ImportoExtrOspite(10)")

            SC1.ExecuteStatement("DIM ImpExtraOspite(10)")
            SC1.ExecuteStatement("DIM NonImpExtraOspite(10)")

            SC1.ExecuteStatement("DIM TipoExtrParMan1(10)")
            SC1.ExecuteStatement("DIM TipoExtrParMan2(10)")
            SC1.ExecuteStatement("DIM TipoExtrParMan3(10)")
            SC1.ExecuteStatement("DIM TipoExtrParMan4(10)")



            For Ext = 0 To 10
                SC1.ExecuteStatement("ImportoExtrComune(" & Ext & ") = " & Replace(ImportoExtrComune(Ext), ",", "."))

                If ImpExtraComune(Ext, 0) > 0 Then
                    '            ImpExtraComune(Ext, 0) = Modulo.MathRound(ImportoExtrComune(Ext - 1), 2)
                    'ImpExtraComune(Ext, 0) = Modulo.MathRound(ImportoExtrComune(Ext), 2)
                End If

                SC1.ExecuteStatement("ImportoExtrJolly(" & Ext & ") = " & Replace(ImportoExtrJolly(Ext), ",", "."))

                SC1.ExecuteStatement("ImportoExtrParent(" & Ext & ") = " & Replace(ImportoExtrParent(Ext), ",", "."))
                SC1.ExecuteStatement("ImportoExtrOspite(" & Ext & ") = " & Replace(ImportoExtrOspite(Ext), ",", "."))

                SC1.ExecuteStatement("ImpExtraOspite(" & Ext & ") = " & Replace(ImpExtraOspite(Ext), ",", "."))

                If CodiceComune(Ext) <> "" Then SC1.ExecuteStatement("CodiceComune(" & Ext & ") = " & Chr(34) & CodiceComune(Ext) & Chr(34))

                If CodiceJolly(Ext) <> "" Then SC1.ExecuteStatement("CodiceJolly(" & Ext & ") = " & Chr(34) & CodiceJolly(Ext) & Chr(34))

                If CodiceRegione(Ext) <> "" Then SC1.ExecuteStatement("CodiceRegione(" & Ext & ") = " & Chr(34) & CodiceRegione(Ext) & Chr(34))



                'SC1.ExecuteStatement "ParenteMensile = " & Replace(MyTabParenteMensile(Ext), ",", ".")
                SC1.ExecuteStatement("ImportoPresParente(" & Ext & ") = " & Replace(Modulo.MathRound(ImportoPresParente(Ext), 2), ",", "."))
                SC1.ExecuteStatement("ImportoAssParente (" & Ext & ") = " & Replace(Modulo.MathRound(ImportoAssParente(Ext), 2), ",", "."))
                SC1.ExecuteStatement("ImportoPresParente2(" & Ext & ") = " & Replace(Modulo.MathRound(ImportoPresParente2(Ext), 2), ",", "."))
                SC1.ExecuteStatement("ImportoAssParente2(" & Ext & ") = " & Replace(Modulo.MathRound(ImportoAssParente2(Ext), 2), ",", "."))

                SC1.ExecuteStatement("ImportoPresComune(" & Ext & ") = " & Replace(Modulo.MathRound(ImportoPresComune(Ext), 2), ",", "."))
                SC1.ExecuteStatement("ImportoAssComune (" & Ext & ") = " & Replace(Modulo.MathRound(ImportoAssComune(Ext), 2), ",", "."))

                SC1.ExecuteStatement("ImportoPresJolly(" & Ext & ") = " & Replace(Modulo.MathRound(ImportoPresJolly(Ext), 2), ",", "."))
                SC1.ExecuteStatement("ImportoAssJolly (" & Ext & ") = " & Replace(Modulo.MathRound(ImportoAssJolly(Ext), 2), ",", "."))


                SC1.ExecuteStatement("ImportoPresRegione(" & Ext & ") = " & Replace(Modulo.MathRound(ImportoPresRegione(Ext), 2), ",", "."))
                SC1.ExecuteStatement("ImportoAssRegione(" & Ext & ") = " & Replace(Modulo.MathRound(ImportoAssRegione(Ext), 2), ",", "."))
                SC1.ExecuteStatement("NonImportoPresParente(" & Ext & ") = " & Replace(Modulo.MathRound(NonImportoPresParente(Ext), 2), ",", "."))
                SC1.ExecuteStatement("NonImportoAssParente(" & Ext & ") = " & Replace(Modulo.MathRound(NonImportoAssParente(Ext), 2), ",", "."))
                SC1.ExecuteStatement("NonImportoPresParente2(" & Ext & ") = " & Replace(Modulo.MathRound(NonImportoPresParente2(Ext), 2), ",", "."))
                SC1.ExecuteStatement("NonImportoAssParente2(" & Ext & ") = " & Replace(Modulo.MathRound(NonImportoAssParente2(Ext), 2), ",", "."))

                SC1.ExecuteStatement("NonImportoPresComune(" & Ext & ") = " & Replace(Modulo.MathRound(NonImportoPresComune(Ext), 2), ",", "."))
                SC1.ExecuteStatement("NonImportoAssComune(" & Ext & ") = " & Replace(Modulo.MathRound(NonImportoAssComune(Ext), 2), ",", "."))

                SC1.ExecuteStatement("NonImportoPresJolly(" & Ext & ") = " & Replace(Modulo.MathRound(NonImportoPresJolly(Ext), 2), ",", "."))
                SC1.ExecuteStatement("NonImportoAssJolly(" & Ext & ") = " & Replace(Modulo.MathRound(NonImportoAssJolly(Ext), 2), ",", "."))

                SC1.ExecuteStatement("NonImportoPresRegione(" & Ext & ") = " & Replace(Modulo.MathRound(NonImportoPresRegione(Ext), 2), ",", "."))
                SC1.ExecuteStatement("NonImportoAssRegione(" & Ext & ") = " & Replace(Modulo.MathRound(NonImportoAssRegione(Ext), 2), ",", "."))

            Next

            For I = 31 To 1 Step -1
                If Trim(MyTabModalita(I)) <> "" Then
                    SC1.ExecuteStatement("Modalita = """ & MyTabModalita(I) & """")
                    Exit For
                End If
            Next

            '******************************************************************************
            '*
            '******************************************************************************
            SC1.ExecuteStatement("XSalvaExtImporto1 = " & Replace(XSalvaExtImporto1, ",", "."))
            SC1.ExecuteStatement("XSalvaExtImporto2 = " & Replace(XSalvaExtImporto2, ",", "."))
            SC1.ExecuteStatement("XSalvaExtImporto3 = " & Replace(XSalvaExtImporto3, ",", "."))
            SC1.ExecuteStatement("XSalvaExtImporto4 = " & Replace(XSalvaExtImporto4, ",", "."))

            SC1.ExecuteStatement("XSalvaExtImporto1C = " & Replace(XSalvaExtImporto1C, ",", "."))
            SC1.ExecuteStatement("XSalvaExtImporto2C = " & Replace(XSalvaExtImporto2C, ",", "."))
            SC1.ExecuteStatement("XSalvaExtImporto3C = " & Replace(XSalvaExtImporto3C, ",", "."))
            SC1.ExecuteStatement("XSalvaExtImporto4C = " & Replace(XSalvaExtImporto4C, ",", "."))

            SC1.ExecuteStatement("XSalvaExtImporto1J = " & Replace(XSalvaExtImporto1J, ",", "."))
            SC1.ExecuteStatement("XSalvaExtImporto2J = " & Replace(XSalvaExtImporto2J, ",", "."))
            SC1.ExecuteStatement("XSalvaExtImporto3J = " & Replace(XSalvaExtImporto3J, ",", "."))
            SC1.ExecuteStatement("XSalvaExtImporto4J = " & Replace(XSalvaExtImporto4J, ",", "."))


            If Mese = 12 Then
                xMese = 1
                xAnno = Anno + 1
            Else
                xMese = Mese + 1
                xAnno = Anno
            End If

            GiornoCalcolati = GiorniNelMese(Cserv, CodOsp, xMese, xAnno)

            For Par = 1 To 10
                SC1.ExecuteStatement("pXSalvaExtImporto1(" & Par & ") = " & Replace(PXSalvaExtImporto1(Par), ",", "."))
                SC1.ExecuteStatement("pXSalvaExtImporto2(" & Par & ") = " & Replace(PXSalvaExtImporto2(Par), ",", "."))
                SC1.ExecuteStatement("pXSalvaExtImporto3(" & Par & ") = " & Replace(PXSalvaExtImporto3(Par), ",", "."))
                SC1.ExecuteStatement("pXSalvaExtImporto4(" & Par & ") = " & Replace(PXSalvaExtImporto4(Par), ",", "."))

                SC1.ExecuteStatement("TipoExtrParMan1(" & Par & ") = " & Chr(34) & TipoExtrParMan1(Par) & Chr(34))
                SC1.ExecuteStatement("TipoExtrParMan2(" & Par & ") = " & Chr(34) & TipoExtrParMan2(Par) & Chr(34))
                SC1.ExecuteStatement("TipoExtrParMan3(" & Par & ") = " & Chr(34) & TipoExtrParMan3(Par) & Chr(34))
                SC1.ExecuteStatement("TipoExtrParMan4(" & Par & ") = " & Chr(34) & TipoExtrParMan4(Par) & Chr(34))



                ForzaRPXParente(Par) = 0

                If Regola.ToUpper.IndexOf("ForzaRPXParente".ToUpper) > 0 Then
                    If OspitePresente(Cserv, CodOsp, xMese, xAnno) = True Then
                        SalvaImportoRegione = 0
                        SalvaImportoComune = 0
                        SalvaImportoOspite = 0
                        SalvaImportoTotale = 0
                        SalvaPercentuale = 0

                        ForzaRPXParente(Par) = 0

                        Dim LO_Parente As New Cls_Parenti

                        LO_Parente.Leggi(STRINGACONNESSIONEDB, CodOsp, Par)


                        Dim KCs As New Cls_DatiOspiteParenteCentroServizio

                        KCs.CentroServizio = Cserv
                        KCs.CodiceOspite = CodOsp
                        KCs.CodiceParente = Par
                        KCs.Leggi(STRINGACONNESSIONEDB)

                        REM Assegna la tipologia operazione e l'aliquota prendendo quella relativa al cserv
                        If KCs.CodiceOspite <> 0 Then
                            LO_Parente.TIPOOPERAZIONE = KCs.TipoOperazione
                            LO_Parente.CODICEIVA = KCs.AliquotaIva
                            LO_Parente.FattAnticipata = KCs.Anticipata
                            LO_Parente.MODALITAPAGAMENTO = KCs.ModalitaPagamento
                            LO_Parente.Compensazione = KCs.Compensazione
                        End If

                        'CampoParente(CodOsp, Par, "FattAnticipata", False) 
                        If LO_Parente.FattAnticipata = "S" Then
                            For I = 1 To GiornoCalcolati
                                ForzaRPXParente(Par) = ForzaRPXParente(Par) + QuoteGiornaliere(Cserv, CodOsp, "P", Par, DateSerial(xAnno, xMese, I))
                            Next I
                        End If

                        'CampoParente(CodOsp, Par, "FattAnticipata", False) 
                        If LO_Parente.FattAnticipata = "S" Then
                            If QuoteMensile(Cserv, CodOsp, "P", Par, DateSerial(xAnno, xMese, I)) > 0 And ForzaRPXParente(Par) > 0 Then
                                ForzaRPXParente(Par) = QuoteMensile(Cserv, CodOsp, "P", Par, DateSerial(xAnno, xMese, I)) 'ImportoMensileParente(Par)
                            Else
                                If ImportoMensileParente(Par) = 0 And ForzaRPXParente(Par) = 0 Then
                                    If QuoteMensile(Cserv, CodOsp, "P", Par, DateSerial(xAnno, xMese, I)) > 0 Then
                                        ForzaRPXParente(Par) = QuoteMensile(Cserv, CodOsp, "P", Par, DateSerial(xAnno, xMese, I))  'ImportoMensileParente(Par)
                                    End If
                                End If
                            End If
                        End If
                    End If
                    SC1.ExecuteStatement("ForzaRPXParente(" & Par & ")= " & Replace(ForzaRPXParente(Par), ",", "."))
                End If
            Next
            '******************************************************************************
            '*
            '******************************************************************************


            ForzaRPXOspite = 0
            If Regola.ToUpper.IndexOf("ForzaRPXOspite".ToUpper) > 0 Then
                If OspitePresente(Cserv, CodOsp, xMese, xAnno) = True Then
                    SalvaImportoRegione = 0
                    SalvaImportoComune = 0
                    SalvaImportoOspite = 0
                    SalvaImportoTotale = 0
                    SalvaPercentuale = 0
                    ForzaRPXOspite = 0


                    Dim LO_CodiceOspite As New ClsOspite

                    LO_CodiceOspite.Leggi(STRINGACONNESSIONEDB, CodOsp)


                    Dim KCs As New Cls_DatiOspiteParenteCentroServizio

                    KCs.CentroServizio = Cserv
                    KCs.CodiceOspite = CodOsp
                    KCs.CodiceParente = 0
                    KCs.Leggi(STRINGACONNESSIONEDB)

                    REM Assegna la tipologia operazione e l'aliquota prendendo quella relativa al cserv
                    If KCs.CodiceOspite <> 0 Then
                        LO_CodiceOspite.TIPOOPERAZIONE = KCs.TipoOperazione
                        LO_CodiceOspite.CODICEIVA = KCs.AliquotaIva
                        LO_CodiceOspite.FattAnticipata = KCs.Anticipata
                        LO_CodiceOspite.MODALITAPAGAMENTO = KCs.ModalitaPagamento
                        LO_CodiceOspite.Compensazione = KCs.Compensazione
                        LO_CodiceOspite.SETTIMANA = KCs.Settimana
                    End If

                    'CampoOspite(CodOsp, "FattAnticipata", False)
                    If LO_CodiceOspite.FattAnticipata = "S" Then
                        For I = 1 To GiornoCalcolati
                            ForzaRPXOspite = ForzaRPXOspite + QuoteGiornaliere(Cserv, CodOsp, "O1", 0, DateSerial(xAnno, xMese, I))
                        Next I
                    End If

                    If LO_CodiceOspite.FattAnticipata = "S" Then
                        If QuoteMensile(Cserv, CodOsp, "O1", 0, DateSerial(xAnno, xMese, I - 1)) > 0 And ForzaRPXOspite > 0 Then
                            ForzaRPXOspite = QuoteMensile(Cserv, CodOsp, "O", 0, DateSerial(xAnno, xMese, I - 1))
                        Else
                            If ImportoMensileOspite = 0 And ForzaRPXOspite > 0 Then
                                If QuoteMensile(Cserv, CodOsp, "O1", 0, DateSerial(xAnno, xMese, I)) > 0 Then
                                    ForzaRPXOspite = QuoteMensile(Cserv, CodOsp, "O", 0, DateSerial(xAnno, xMese, I))
                                End If
                            End If
                        End If
                    End If
                End If
                SC1.ExecuteStatement("ForzaRPXOspite= " & Replace(ForzaRPXOspite, ",", "."))
            End If




            SC1.ExecuteStatement("ImportoAddebitoPrimoComune = 0")

            SC1.ExecuteStatement("ImportoAddebitoPrimoJolly = 0")

            SC1.ExecuteStatement("ImportoAddebitoPrimoRegione = 0")
            SC1.ExecuteStatement("TipoAddebitoPrimoRegione = " & Chr(34) & Chr(34))

            SC1.ExecuteStatement("TipoAddebitoPrimoComune = " & Chr(34) & Chr(34))


            SC1.ExecuteStatement("TipoAddebitoPrimoJolly = " & Chr(34) & Chr(34))

            SC1.ExecuteStatement("Cserv = " & Chr(34) & Cserv & Chr(34))
            SC1.ExecuteStatement("ImpExtrOspMan1 = " & Replace(ImpExtrOspMan1, ",", "."))
            SC1.ExecuteStatement("ImpExtrOspMan2 = " & Replace(ImpExtrOspMan2, ",", "."))
            SC1.ExecuteStatement("ImpExtrOspMan3 = " & Replace(ImpExtrOspMan3, ",", "."))
            SC1.ExecuteStatement("ImpExtrOspMan4 = " & Replace(ImpExtrOspMan4, ",", "."))

            SC1.ExecuteStatement("TipoAddExtrOspMan1 = " & Chr(34) & TipoAddExtrOspMan1 & Chr(34))
            SC1.ExecuteStatement("TipoAddExtrOspMan2 = " & Chr(34) & TipoAddExtrOspMan2 & Chr(34))
            SC1.ExecuteStatement("TipoAddExtrOspMan3 = " & Chr(34) & TipoAddExtrOspMan3 & Chr(34))
            SC1.ExecuteStatement("TipoAddExtrOspMan4 = " & Chr(34) & TipoAddExtrOspMan4 & Chr(34))

            If Regola.ToUpper.IndexOf("TIPORETTA") > 0 Then
                Dim VD As New Cls_rettatotale

                VD.CENTROSERVIZIO = Cserv
                VD.CODICEOSPITE = CodOsp
                VD.UltimaData(STRINGACONNESSIONEDB, CodOsp, Cserv)
                SC1.ExecuteStatement("TipoRetta = " & Chr(34) & VD.TipoRetta & Chr(34))
            End If


            'UscitoPrimaMese
            'USCITOPRIMAMESE
            'UscitoPenultimoGiorno
            'USCITOPENULTIMOGIORNO

            If Regola.ToUpper.IndexOf("USCITOPRIMAMESE") > 0 Or Regola.ToUpper.IndexOf("USCITOPENULTIMOGIORNO") > 0 Then
                Dim Kl As New Cls_Movimenti

                Kl.CodiceOspite = CodOsp
                Kl.CENTROSERVIZIO = Cserv
                Kl.UltimaMovimentoPrimaData(STRINGACONNESSIONEDB, CodOsp, Cserv, DateSerial(Anno, Mese, 1))
                If Kl.TipoMov = "03" Then
                    SC1.ExecuteStatement("UscitoPrimaMese = 1")
                Else
                    SC1.ExecuteStatement("UscitoPrimaMese = 0")
                End If



                Kl.CodiceOspite = CodOsp
                Kl.CENTROSERVIZIO = Cserv
                Kl.UltimaMovimentoPrimaData(STRINGACONNESSIONEDB, CodOsp, Cserv, DateSerial(Anno, Mese, GiorniMese(Mese, Anno)))
                If Kl.TipoMov = "03" And Day(Kl.Data) = GiorniMese(Mese, Anno) - 1 Then
                    SC1.ExecuteStatement("UscitoPenultimoGiorno = 1")
                Else
                    SC1.ExecuteStatement("UscitoPenultimoGiorno = 0")
                End If
            End If

            SC1.AddCode(Regola)


            Try
                SC1.Run("Calcolo")
            Catch ex As Exception
                StringaDegliErrori = StringaDegliErrori & " Errore in Regola (10195) " & ex.Message
            End Try



            'Call Calcolo(VARIABILE1)

            '************************************************************************
            '* Leggo le variabili modificate da VB SCRIPT
            '************************************************************************
            NonImportoPresOspite = SC1.Eval("NonImportoPresOspite")
            NonImportoAssOspite = SC1.Eval("NonImportoAssOspite")
            NonImportoPresEnte = SC1.Eval("NonImportoPresEnte")
            NonImportoAssEnte = SC1.Eval("NonImportoAssEnte")
            ImportoPresOspite = SC1.Eval("ImportoPresOspite")
            ImportoAssOspite = SC1.Eval("ImportoAssOspite")
            ImportoPresEnte = SC1.Eval("ImportoPresEnte")
            ImportoAssEnte = SC1.Eval("ImportoAssEnte")
            GiorniPres = SC1.Eval("GiorniPres")
            GiorniAss = SC1.Eval("GiorniAss")
            NonGiorniPres = SC1.Eval("NonGiorniPres")
            NonGiorniAss = SC1.Eval("NonGiorniAss")
            VARIABILE1 = SC1.Eval("Variabile1")
            Variabile2 = SC1.Eval("Variabile2")
            Variabile3 = SC1.Eval("Variabile3")
            Variabile4 = SC1.Eval("Variabile4")
            ImpExtrOspMan1 = SC1.Eval("ImpExtrOspMan1")
            ImpExtrOspMan2 = SC1.Eval("ImpExtrOspMan2")
            ImpExtrOspMan3 = SC1.Eval("ImpExtrOspMan3")
            ImpExtrOspMan4 = SC1.Eval("ImpExtrOspMan4")
            TipoAddExtrOspMan1 = SC1.Eval("TipoAddExtrOspMan1")
            TipoAddExtrOspMan2 = SC1.Eval("TipoAddExtrOspMan2")
            TipoAddExtrOspMan3 = SC1.Eval("TipoAddExtrOspMan3")
            TipoAddExtrOspMan4 = SC1.Eval("TipoAddExtrOspMan4")

            For Ext = 0 To 10



                ImportoExtrComune(Ext) = SC1.Eval("ImportoExtrComune(" & Ext & ")")

                ImportoExtrJolly(Ext) = SC1.Eval("ImportoExtrJolly(" & Ext & ")")

                ImportoExtrParent(Ext) = SC1.Eval("ImportoExtrParent(" & Ext & ")")
                ImportoExtrOspite(Ext) = SC1.Eval("ImportoExtrOspite(" & Ext & ")")
                ImportoPresParente(Ext) = SC1.Eval("ImportoPresParente(" & Ext & ")")
                ImportoAssParente(Ext) = SC1.Eval("ImportoAssParente (" & Ext & ")")
                ImportoPresParente2(Ext) = SC1.Eval("ImportoPresParente2(" & Ext & ")")
                ImportoAssParente2(Ext) = SC1.Eval("ImportoAssParente2(" & Ext & ")")


                ImpExtraOspite(Ext) = SC1.Eval("ImpExtraOspite(" & Ext & ")")


                ImportoPresComune(Ext) = SC1.Eval("ImportoPresComune(" & Ext & ")")
                ImportoAssComune(Ext) = SC1.Eval("ImportoAssComune (" & Ext & ")")

                ImportoPresJolly(Ext) = SC1.Eval("ImportoPresJolly(" & Ext & ")")
                ImportoAssJolly(Ext) = SC1.Eval("ImportoAssJolly (" & Ext & ")")


                ImportoPresRegione(Ext) = SC1.Eval("ImportoPresRegione(" & Ext & ")")
                ImportoAssRegione(Ext) = SC1.Eval("ImportoAssRegione(" & Ext & ")")
                NonImportoPresParente(Ext) = SC1.Eval("NonImportoPresParente(" & Ext & ")")
                NonImportoAssParente(Ext) = SC1.Eval("NonImportoAssParente(" & Ext & ")")

                NonImportoPresParente2(Ext) = SC1.Eval("NonImportoPresParente2(" & Ext & ")")
                NonImportoAssParente2(Ext) = SC1.Eval("NonImportoAssParente2(" & Ext & ")")

                NonImportoPresComune(Ext) = SC1.Eval("NonImportoPresComune(" & Ext & ")")

                NonImportoPresJolly(Ext) = SC1.Eval("NonImportoPresJolly(" & Ext & ")")

                NonImportoAssComune(Ext) = SC1.Eval("NonImportoAssComune(" & Ext & ")")

                NonImportoAssJolly(Ext) = SC1.Eval("NonImportoAssJolly(" & Ext & ")")

                NonImportoPresRegione(Ext) = SC1.Eval("NonImportoPresRegione(" & Ext & ")")
                NonImportoAssRegione(Ext) = SC1.Eval("NonImportoAssRegione(" & Ext & ")")
                ForzaRPXParente(Ext) = SC1.Eval("ForzaRPXParente(" & Ext & ")")

                ImpExtrParMan1(Ext) = SC1.Eval("ImpExtrParMan1 (" & Ext & ")")
                ImpExtrParMan2(Ext) = SC1.Eval("ImpExtrParMan2 (" & Ext & ")")
                ImpExtrParMan3(Ext) = SC1.Eval("ImpExtrParMan3 (" & Ext & ")")
                ImpExtrParMan4(Ext) = SC1.Eval("ImpExtrParMan4 (" & Ext & ")")

                TipoExtrParMan1(Ext) = SC1.Eval("TipoExtrParMan1(" & Ext & ")")
                TipoExtrParMan2(Ext) = SC1.Eval("TipoExtrParMan2(" & Ext & ")")
                TipoExtrParMan3(Ext) = SC1.Eval("TipoExtrParMan3(" & Ext & ")")
                TipoExtrParMan4(Ext) = SC1.Eval("TipoExtrParMan4(" & Ext & ")")
            Next


            ForzaRPXOspite = SC1.Eval("ForzaRPXOspite")

            ImportoAddebitoPrimoComune = SC1.Eval("ImportoAddebitoPrimoComune")

            ImportoAddebitoPrimoJolly = SC1.Eval("ImportoAddebitoPrimoJolly")

            ImportoAddebitoPrimoRegione = SC1.Eval("ImportoAddebitoPrimoRegione")
            TipoAddebitoPrimoRegione = SC1.Eval("TipoAddebitoPrimoRegione")


            TipoAddebitoPrimoComune = SC1.Eval("TipoAddebitoPrimoComune")


            TipoAddebitoPrimoJolly = SC1.Eval("TipoAddebitoPrimoJolly")

        End If



        REM Dim MyExtra As New Cls_DatiPagamento

        REM MyExtra.CodiceOspite = CodOsp
        REM MyExtra.CodiceParente = 0
        REM MyExtra.LeggiUltimaDataData(STRINGACONNESSIONEDB, DateSerial(Anno, Mese, 1))
        REM If MyExtra.TipoExtra <> "" Then
        REM MyTabImpExtrOspite(1, XSO) = DecodificaExtra("O", MyExtra.TipoExtra, Mese, Anno, XSO, Cserv)
        REM MyTabCodiceExtrOspite(XSO) = MyExtra.TipoExtra
        REM XSO = XSO + 1
        REM End If



    End Sub


    Public Sub VerificaPrimaCalcolo(ByVal Cserv As String, ByVal CodOsp As Long)       
        Dim MySql As String

        MySql = "SELECT [CENTROSERVIZIO],[CODICEOSPITE] FROM [EXTRAOSPITE] where CodiceOspite = " & CodOsp & " And [CENTROSERVIZIO] = '" & Cserv & "' And  RIPARTIZIONE = 'P' aND  (SELECT COUNT(*) FROM IMPORTOPARENTI WHERE IMPORTOPARENTI.CENTROSERVIZIO = EXTRAOSPITE.CENTROSERVIZIO AND IMPORTOPARENTI.CODICEOSPITE = [EXTRAOSPITE].CODICEOSPITE AND  IMPORTOPARENTI.PERCENTUALE  > 0) = 0"
        Dim cmdParenteRetta1 As New OleDbCommand()
        cmdParenteRetta1.CommandText = MySql
        cmdParenteRetta1.Connection = OspitiCon
        Dim RDParenteRetta1 As OleDbDataReader = cmdParenteRetta1.ExecuteReader()
        If RDParenteRetta1.Read() Then
            StringaDegliErrori = StringaDegliErrori & "Indicato extra fisso senza indicare percentuale per ospite " & CodOsp & " centro servizio " & Cserv
        End If
        RDParenteRetta1.Close()

    End Sub

    Private Function OspitePresente(ByVal Cserv As String, ByVal CodOsp As Long, ByVal Mese As Long, ByVal Anno As Long) As Boolean
        Dim RsMovimenti As New ADODB.Recordset
        Dim MySql As String



        MySql = "Select top 1 * From Movimenti Where  CENTROSERVIZIO = '" & Cserv & "' And CODICEOSPITE =" & CodOsp & " And Data <= {ts '" & Format(DateSerial(Anno, Mese, GiorniMese(Mese, Anno)), "yyyy-MM-dd") & " 00:00:00'}  Order By DATA Desc,PROGRESSIVO Desc"
        RsMovimenti.Open(MySql, OspitiDb, ADODB.CursorTypeEnum.adOpenForwardOnly, ADODB.LockTypeEnum.adLockOptimistic)
        If RsMovimenti.EOF Then
            OspitePresente = False
        Else
            OspitePresente = False
            Dim DateAp As Date
            DateAp = RsMovimenti.Fields("Data").Value
            If MoveFromDb(RsMovimenti.Fields("TipoMov")) = "13" And Format(DateAp, "yyyyMMdd") >= Format(DateSerial(Anno, Mese, 1), "yyyyMMdd") Then
                OspitePresente = True
            End If
            If MoveFromDb(RsMovimenti.Fields("TipoMov")) <> "13" Or IsDBNull(RsMovimenti.Fields("TipoMov").Value) Then
                OspitePresente = True
            End If
        End If
        RsMovimenti.Close()
    End Function


    Public Sub ModificaTabellaPresenze(ByVal Cserv As String, ByVal CodOsp As Long, ByVal Mese As Integer, ByVal Anno As Integer)
        Dim MyRs As New ADODB.Recordset
        Dim D As Long

        For D = 1 To GiorniMese(Mese, Anno)
            'If Not NonImpegnativa Then
            MyRs.Open("Select * from IMPEGNATIVE Where CENTROSERVIZIO = '" & Cserv & "' And CodiceOspite = " & CodOsp & " And DATAINIZIO <= {ts '" & Format(DateSerial(Anno, Mese, D), "yyyy-MM-dd") & " 00:00:00'} And (DATAFINE >= {ts '" & Format(DateSerial(Anno, Mese, D), "yyyy-MM-dd") & " 00:00:00'} Or DATAFINE IS NULL)", OspitiDb, ADODB.CursorTypeEnum.adOpenKeyset, ADODB.LockTypeEnum.adLockOptimistic)
            If MyRs.EOF Then
                MyTabCausale(D) = "*"
            End If
            MyRs.Close()
            'End If
        Next D
    End Sub


    Public Sub RiassociaComuni(ByVal Cserv As String, ByVal CodOsp As Long, ByVal Mese As Integer, ByVal Anno As Integer)

        Dim MyRs As New ADODB.Recordset
        Dim I As Long
        Dim LastStato As String
        Dim LastProvincia As String
        Dim LastComune As String
        Dim LastImporto As Double
        Dim XTrovato As Double
        Dim NazionalitaOspite As String
        Dim SessoOspite As String
        Dim DataNascita As Date
        REM Dim ImpParenti As Double
        REM Dim ImpMiA As Double


        LastStato = MyTabAutoSufficente(1)
        LastProvincia = MyTabCodiceProv(1)
        LastComune = MyTabCodiceComune(1)
        NazionalitaOspite = CampoOspite(CodOsp, "Nazionalita")
        SessoOspite = CampoOspite(CodOsp, "Sesso")

        If IsDate(CampoOspite(CodOsp, "DataNascita")) Then
            DataNascita = CampoOspite(CodOsp, "DataNascita")
        Else
            DataNascita = Now

        End If



        For I = 1 To GiorniMese(Mese, Anno)

            If MyTabCodiceProv(I) <> "" And MyTabCodiceComune(I) <> "" Then
                LastProvincia = MyTabCodiceProv(I)
                LastComune = MyTabCodiceComune(I)
                LastImporto = MyTabImportoComune(I)

                MyRs.Open("Select * From LegamiComune Where Provincia = '" & MyTabCodiceProv(I) & "' And Comune = '" & MyTabCodiceComune(I) & "' And Stato = '" & MyTabAutoSufficente(I) & "'", OspitiDb, ADODB.CursorTypeEnum.adOpenKeyset, ADODB.LockTypeEnum.adLockOptimistic)
                Do While Not MyRs.EOF
                    MyTabCodiceProv(I) = MoveFromDb(MyRs.Fields("LegameProvincia"))
                    MyTabCodiceComune(I) = MoveFromDb(MyRs.Fields("LegameComune"))
                    XTrovato = True
                    If Trim(MoveFromDb(MyRs.Fields("Nazionalita"))) <> "" And Trim(MoveFromDb(MyRs.Fields("Nazionalita"))) <> NazionalitaOspite Then
                        XTrovato = False
                    End If
                    If Trim(MoveFromDb(MyRs.Fields("Sesso"))) <> "" And Trim(MoveFromDb(MyRs.Fields("Sesso"))) <> SessoOspite Then
                        XTrovato = False
                    End If


                    If MoveFromDb(MyRs.Fields("EtaDa")) <> 0 And DateDiff("d", DataNascita, DateSerial(Anno, Mese, I)) < DateDiff("d", DataNascita, DateAdd("yyyy", MoveFromDb(MyRs.Fields("EtaDa")), DataNascita)) Then
                        XTrovato = False
                    End If
                    If MoveFromDb(MyRs.Fields("EtaA")) <> 0 And DateDiff("d", DataNascita, DateSerial(Anno, Mese, I)) > DateDiff("d", DataNascita, DateAdd("yyyy", MoveFromDb(MyRs.Fields("EtaA")), DataNascita)) Then
                        XTrovato = False
                    End If

                    If XTrovato = True Then Exit Do
                    MyRs.MoveNext()
                Loop
                MyRs.Close()
            End If

            If MyTabAutoSufficente(I) <> LastStato And MyTabAutoSufficente(I) <> "" Then
                MyRs.Open("Select * From LegamiComune Where Provincia = '" & LastProvincia & "' And Comune = '" & LastComune & "' And Stato = '" & MyTabAutoSufficente(I) & "'", OspitiDb, ADODB.CursorTypeEnum.adOpenKeyset, ADODB.LockTypeEnum.adLockOptimistic)
                Do While Not MyRs.EOF
                    MyTabCodiceProv(I) = MoveFromDb(MyRs.Fields("LegameProvincia"))
                    MyTabCodiceComune(I) = MoveFromDb(MyRs.Fields("LegameComune"))
                    MyTabImportoComune(I) = LastImporto
                    XTrovato = True
                    If Trim(MoveFromDb(MyRs.Fields("Nazionalita"))) <> "" And Trim(MoveFromDb(MyRs.Fields("Nazionalita"))) <> NazionalitaOspite Then
                        XTrovato = False
                    End If
                    If Trim(MoveFromDb(MyRs.Fields("Sesso"))) <> "" And Trim(MoveFromDb(MyRs.Fields("Sesso"))) <> SessoOspite Then
                        XTrovato = False
                    End If

                    If MoveFromDb(MyRs.Fields("EtaDa")) <> 0 And DateDiff("d", DataNascita, DateSerial(Anno, Mese, I)) < DateDiff("d", DataNascita, DateAdd("yyyy", MoveFromDb(MyRs.Fields("EtaDa")), DataNascita)) Then
                        XTrovato = False
                    End If
                    If MoveFromDb(MyRs.Fields("EtaA")) <> 0 And DateDiff("d", DataNascita, DateSerial(Anno, Mese, I)) > DateDiff("d", DataNascita, DateAdd("yyyy", MoveFromDb(MyRs.Fields("EtaA")), DataNascita)) Then
                        XTrovato = False
                    End If

                    If XTrovato = True Then Exit Do
                    MyRs.MoveNext()
                Loop
                MyRs.Close()
                LastStato = MyTabAutoSufficente(I)
            End If


            If Day(DataNascita) + 1 = I And Month(DataNascita) = Mese Then
                MyRs.Open("Select * From LegamiComune Where Provincia = '" & LastProvincia & "' And Comune = '" & LastComune & "' And Stato = '" & LastStato & "'", OspitiDb, ADODB.CursorTypeEnum.adOpenKeyset, ADODB.LockTypeEnum.adLockOptimistic)
                Do While Not MyRs.EOF
                    MyTabCodiceProv(I) = MoveFromDb(MyRs.Fields("LegameProvincia"))
                    MyTabCodiceComune(I) = MoveFromDb(MyRs.Fields("LegameComune"))
                    MyTabImportoComune(I) = LastImporto
                    XTrovato = True
                    If Trim(MoveFromDb(MyRs.Fields("Nazionalita"))) <> "" And Trim(MoveFromDb(MyRs.Fields("Nazionalita"))) <> NazionalitaOspite Then
                        XTrovato = False
                    End If
                    If Trim(MoveFromDb(MyRs.Fields("Sesso"))) <> "" And Trim(MoveFromDb(MyRs.Fields("Sesso"))) <> SessoOspite Then
                        XTrovato = False
                    End If

                    If MoveFromDb(MyRs.Fields("EtaDa")) <> 0 And DateDiff("d", DataNascita, DateSerial(Anno, Mese, I)) < DateDiff("d", DataNascita, DateAdd("yyyy", MoveFromDb(MyRs.Fields("EtaDa")), DataNascita)) Then
                        XTrovato = False
                    End If
                    If MoveFromDb(MyRs.Fields("EtaA")) <> 0 And DateDiff("d", DataNascita, DateSerial(Anno, Mese, I)) > DateDiff("d", DataNascita, DateAdd("yyyy", MoveFromDb(MyRs.Fields("EtaA")), DataNascita)) Then
                        XTrovato = False
                    End If

                    If XTrovato = True Then Exit Do
                    MyRs.MoveNext()
                Loop
                MyRs.Close()
                LastStato = MyTabAutoSufficente(I)
            End If
        Next


    End Sub


    Public Function CampoCausaleEntrataUscita(ByVal Causale As String, ByVal Campo As String) As String
        Dim MySql As String

        If Causale <> "" Then
            MySql = "Select * From CAUSALI Where CODICE = '" & Causale & "' "
            'Rs_Regione.Open(MySql, OspitiDb, ADODB.CursorTypeEnum.adOpenKeyset, ADODB.LockTypeEnum.adLockOptimistic)

            Dim cmdCausali As New OleDbCommand()
            cmdCausali.CommandText = MySql
            cmdCausali.Connection = OspitiCon
            Dim ReaderCausali As OleDbDataReader = cmdCausali.ExecuteReader()

            If ReaderCausali.Read Then
                CampoCausaleEntrataUscita = campodb(ReaderCausali.Item(Campo))
            Else
                CampoCausaleEntrataUscita = "Causale Non Decodificata"
            End If

            ReaderCausali.Close()
        Else
            CampoCausaleEntrataUscita = "Causale Non Decodificata"
        End If
    End Function



    Public Function CalcolaRipetizioniCausali(ByVal CentroServizio As String, ByVal CodiceOspite As Long, ByVal Anno As Integer, ByVal Tipo As String, ByVal GiornoElab As Date) As String
        Dim MyRs As New ADODB.Recordset
        Dim dataini As Date
        Dim GiorniCausale As Integer
        Dim InizioGiorni As Date

        dataini = DateSerial(Anno, 1, 1)
        MyRs.Open("Select * From Movimenti Where CentroServizio = '" & CentroServizio & "' And CodiceOspite= " & CodiceOspite & "And year(Data) = " & Anno & " Order By Data", OspitiDb, ADODB.CursorTypeEnum.adOpenKeyset, ADODB.LockTypeEnum.adLockOptimistic)
        If Not MyRs.EOF Then
            dataini = MoveFromDb(MyRs.Fields("Data"))
        End If
        MyRs.Close()

        GiorniCausale = 0
        InizioGiorni = DateSerial(1900, 1, 1)
        MyRs.Open("Select * From Movimenti Where CentroServizio = '" & CentroServizio & "' And CodiceOspite= " & CodiceOspite & " And Data >= {ts '" & Format(dataini, "yyyy-MM-dd") & " 00:00:00'} Order By Data", OspitiDb, ADODB.CursorTypeEnum.adOpenKeyset, ADODB.LockTypeEnum.adLockOptimistic)
        Do While Not MyRs.EOF
            If MoveFromDb(MyRs.Fields("TipoMov")) = "03" And MoveFromDb(MyRs.Fields("Causale")) = Tipo Then
                InizioGiorni = MoveFromDb(MyRs.Fields("Data"))
            Else
                If InizioGiorni <> DateSerial(1900, 1, 1) Then
                    GiorniCausale = GiorniCausale + DateDiff("d", InizioGiorni, MoveFromDb(MyRs.Fields("Data")))
                    InizioGiorni = DateSerial(1900, 1, 1)
                Else
                    InizioGiorni = DateSerial(1900, 1, 1)
                End If

            End If

            MyRs.MoveNext()
        Loop
        MyRs.Close()

        If CampoCentroServizio(CentroServizio, " ") = "" Then
        End If

        If InizioGiorni <> DateSerial(1900, 1, 1) Then
            GiorniCausale = GiorniCausale + DateDiff("d", InizioGiorni, GiornoElab)
        End If
        CalcolaRipetizioniCausali = GiorniCausale



    End Function










    Public Sub ImpostaPresenzedaImpegnativa(ByVal Cserv As String, ByVal CodOsp As Long, ByVal Mese As Integer, ByVal Anno As Integer)

        Dim MyRs As New ADODB.Recordset
        Dim giornoini As Integer
        Dim giornofin As Integer
        Dim I As Integer
        Dim Passato As Boolean

        Passato = False

        If CodOsp = 1482 Then

        End If
        giornoini = 1
        giornofin = GiorniMese(Mese, Anno)
        MyRs.Open("Select * From IMPEGNATIVE WHERE CODICEOSPITE = " & CodOsp & " And CENTROSERVIZIO = '" & Cserv & "' And Descrizione Like '%FONDO%' Order By DataInizio Desc", OspitiDb, ADODB.CursorTypeEnum.adOpenKeyset, ADODB.LockTypeEnum.adLockOptimistic)
        Do While Not MyRs.EOF


            If Month(MoveFromDb(MyRs.Fields("DATAINIZIO"))) = Mese And Year(MoveFromDb(MyRs.Fields("DATAINIZIO"))) = Anno Then
                giornoini = Day(MoveFromDb(MyRs.Fields("DATAINIZIO")))
                Passato = True
            End If
            If Not IsDBNull(MyRs.Fields("DataFine")) Then
                If Month(MoveFromDb(MyRs.Fields("DataFine"))) = Mese And Year(MoveFromDb(MyRs.Fields("DataFine"))) = Val(Anno) Then
                    giornofin = Day(MoveFromDb(MyRs.Fields("DataFine")))
                    Passato = True
                End If
            End If

            If Format(MoveFromDb(MyRs.Fields("DATAINIZIO")), "yyyyMMdd") <= Format(DateSerial(Anno, Mese, 1), "yyyyMMdd") And Format(MoveFromDb(MyRs.Fields("DataFine")), "yyyyMMdd") >= Format(DateSerial(Anno, Mese, GiorniMese(Mese, Anno)), "yyyyMMdd") Then
                Passato = True
                Exit Do
            End If
            MyRs.MoveNext()
        Loop
        MyRs.Close()
        If giornoini > giornofin Then
            Exit Sub
        End If
        For I = 1 To giornoini - 1
            MyTabCausale(I) = "*"
        Next
        For I = giornofin + 1 To GiorniMese(Mese, Anno)
            MyTabCausale(I) = "*"
        Next
        If Passato = False Then
            For I = 1 To GiorniMese(Mese, Anno)
                MyTabCausale(I) = "*"
            Next
        End If
    End Sub
    Public Function MoveFromDbWC(ByVal MyRs As ADODB.Recordset, ByVal Campo As String) As Object
        Dim MyField As ADODB.Field

#If FLAG_DEBUG = False Then
        On Error GoTo ErroreCampoInesistente
#End If
        MyField = MyRs.Fields(Campo)

        Select Case MyField.Type
            Case ADODB.DataTypeEnum.adDBDate
                If IsDBNull(MyField.Value) Then
                    MoveFromDbWC = "__/__/____"
                Else
                    If Not IsDate(MyField.Value) Or MyField.Value = "0.00.00" Then
                        MoveFromDbWC = "__/__/____"
                    Else
                        MoveFromDbWC = MyField.Value
                    End If
                End If
            Case ADODB.DataTypeEnum.adDBTimeStamp
                If IsDBNull(MyField.Value) Then
                    MoveFromDbWC = "__/__/____"
                Else
                    If Not IsDate(MyField.Value) Or MyField.Value = "0.00.00" Then
                        MoveFromDbWC = "__/__/____"
                    Else
                        MoveFromDbWC = MyField.Value
                    End If
                End If
            Case ADODB.DataTypeEnum.adSmallInt
                If IsDBNull(MyField.Value) Then
                    MoveFromDbWC = 0
                Else
                    MoveFromDbWC = MyField.Value
                End If
            Case ADODB.DataTypeEnum.adInteger
                If IsDBNull(MyField.Value) Then
                    MoveFromDbWC = 0
                Else
                    MoveFromDbWC = MyField.Value
                End If
            Case ADODB.DataTypeEnum.adNumeric
                If IsDBNull(MyField.Value) Then
                    MoveFromDbWC = 0
                Else
                    MoveFromDbWC = MyField.Value
                End If
            Case ADODB.DataTypeEnum.adTinyInt
                If IsDBNull(MyField.Value) Then
                    MoveFromDbWC = 0
                Else
                    MoveFromDbWC = MyField.Value
                End If
            Case ADODB.DataTypeEnum.adSingle
                If IsDBNull(MyField.Value) Then
                    MoveFromDbWC = 0
                Else
                    MoveFromDbWC = MyField.Value
                End If
            Case ADODB.DataTypeEnum.adLongVarBinary
                If IsDBNull(MyField.Value) Then
                    MoveFromDbWC = 0
                Else
                    MoveFromDbWC = MyField.Value
                End If
            Case ADODB.DataTypeEnum.adDouble
                If IsDBNull(MyField.Value) Then
                    MoveFromDbWC = 0
                Else
                    MoveFromDbWC = MyField.Value
                End If
            Case ADODB.DataTypeEnum.adVarChar
                If IsDBNull(MyField.Value) Then
                    MoveFromDbWC = vbNullString
                Else
                    MoveFromDbWC = CStr(MyField.Value)
                End If
            Case ADODB.DataTypeEnum.adUnsignedTinyInt
                If IsDBNull(MyField.Value) Then
                    MoveFromDbWC = 0
                Else
                    MoveFromDbWC = MyField.Value
                End If
            Case Else
                If IsDBNull(MyField.Value) Then
                    MoveFromDbWC = ""
                End If
                If IsDBNull(MyField.Value) Then
                    MoveFromDbWC = ""
                Else
                    MoveFromDbWC = MyField.Value
                End If
        End Select

        On Error GoTo 0
        Exit Function
ErroreCampoInesistente:

        On Error GoTo 0
    End Function
    Private Function MoveFromDb(ByVal MyField As ADODB.Field, Optional ByVal Tipo As Integer = 0) As Object
        If Tipo = 0 Then
            Select Case MyField.Type
                Case ADODB.DataTypeEnum.adDBDate Or ADODB.DataTypeEnum.adDBTimeStamp
                    If IsDBNull(MyField.Value) Then
                        MoveFromDb = "__/__/____"
                    Else
                        If Not IsDate(MyField.Value) Then
                            MoveFromDb = "__/__/____"
                        Else
                            MoveFromDb = MyField.Value
                        End If
                    End If
                Case ADODB.DataTypeEnum.adSmallInt
                    If IsDBNull(MyField.Value) Then
                        MoveFromDb = 0
                    Else
                        MoveFromDb = MyField.Value
                    End If
                Case 20 ' INTERO PER MYSQL
                    If IsDBNull(MyField.Value) Then
                        MoveFromDb = 0
                    Else
                        MoveFromDb = MyField.Value
                    End If
                Case ADODB.DataTypeEnum.adInteger
                    If IsDBNull(MyField.Value) Then
                        MoveFromDb = 0
                    Else
                        MoveFromDb = MyField.Value
                    End If
                Case ADODB.DataTypeEnum.adNumeric
                    If IsDBNull(MyField.Value) Then
                        MoveFromDb = 0
                    Else
                        MoveFromDb = MyField.Value
                    End If
                Case ADODB.DataTypeEnum.adTinyInt
                    If IsDBNull(MyField.Value) Then
                        MoveFromDb = 0
                    Else
                        MoveFromDb = MyField.Value
                    End If
                Case ADODB.DataTypeEnum.adSingle
                    If IsDBNull(MyField.Value) Then
                        MoveFromDb = 0
                    Else
                        MoveFromDb = MyField.Value
                    End If
                Case ADODB.DataTypeEnum.adLongVarBinary
                    If IsDBNull(MyField.Value) Then
                        MoveFromDb = 0
                    Else
                        MoveFromDb = MyField.Value
                    End If
                Case ADODB.DataTypeEnum.adDouble
                    If IsDBNull(MyField.Value) Then
                        MoveFromDb = 0
                    Else
                        MoveFromDb = MyField.Value
                    End If
                Case 139
                    If IsDBNull(MyField.Value) Then
                        MoveFromDb = 0
                    Else
                        MoveFromDb = MyField.Value
                    End If
                Case ADODB.DataTypeEnum.adVarChar
                    If IsDBNull(MyField.Value) Then
                        MoveFromDb = vbNullString
                    Else
                        MoveFromDb = CStr(MyField.Value)
                    End If
                Case ADODB.DataTypeEnum.adUnsignedTinyInt
                    If IsDBNull(MyField.Value) Then
                        MoveFromDb = 0
                    Else
                        MoveFromDb = MyField.Value
                    End If
                Case Else
                    If IsDBNull(MyField.Value) Then
                        MoveFromDb = ""
                    End If
                    If IsDBNull(MyField.Value) Then
                        MoveFromDb = ""
                    Else
                        'MoveFromDb = StringaSqlS(MyField.Value)
                        MoveFromDb = RTrim(MyField.Value)
                    End If
            End Select
        End If
    End Function
    Public Function GiorniMese(ByVal Mese As Byte, ByVal Anno As Integer) As Byte
        If Mese = 1 Or Mese = 3 Or Mese = 5 Or Mese = 7 Or Mese = 8 Or _
           Mese = 10 Or Mese = 12 Then
            GiorniMese = 31
        Else
            If Mese <> 2 Then
                GiorniMese = 30
            Else
                If Day(DateSerial(Anno, Mese, 29)) = 29 Then
                    GiorniMese = 29
                Else
                    GiorniMese = 28
                End If
            End If
        End If
    End Function
    Public Function CampoOspite(ByVal CodOsp As Long, ByVal Campo As String, Optional ByVal ControllaCampo As Boolean = True) As String        


        Dim cmd As New OleDbCommand()
        cmd.CommandText = "Select * From AnagraficaComune Where Tipologia = 'O' And CODICEOSPITE = " & CodOsp
        cmd.Connection = OspitiCon

        Dim myPOSTreader As OleDbDataReader = cmd.ExecuteReader()
        If myPOSTreader.Read Then
            CampoOspite = campodb(myPOSTreader.Item(Campo))
        Else
            CampoOspite = ""
        End If
        myPOSTreader.Close()
    End Function

    Function StatoAutoOspite(ByVal CodiceOspite As Long, ByVal DataMov As Date) As String
        Dim cmd As New OleDbCommand()
        cmd.CommandText = "Select * From  StatoAuto Where CodiceOspite = " & CodiceOspite & " And Data <= {ts '" & Format(DataMov, "yyyy-MM-dd") & " 00:00:00'} Order by Data Desc"
        cmd.Connection = OspitiCon

        Dim myPOSTreader As OleDbDataReader = cmd.ExecuteReader()
        If myPOSTreader.Read Then
            StatoAutoOspite = campodb(myPOSTreader.Item("STATOAUTO"))
        Else
            StatoAutoOspite = ""
        End If
        myPOSTreader.Close()
    End Function


    Public Function CampoCentroServizio(ByVal Cserv As String, ByVal Campo As String, Optional ByVal ControllaCampo As Boolean = True) As String
        Dim cmd As New OleDbCommand()
        cmd.CommandText = "Select " & Campo & " From TABELLACENTROSERVIZIO Where CENTROSERVIZIO = '" & Cserv & "'"
        cmd.Connection = OspitiCon

        Dim myPOSTreader As OleDbDataReader = cmd.ExecuteReader()
        If myPOSTreader.Read Then
            CampoCentroServizio = campodb(myPOSTreader.Item(Campo))
        Else
            CampoCentroServizio = ""
        End If
        myPOSTreader.Close()
    End Function

    Public Function CampoRegioneN(ByVal Regione As String, ByVal Campo As String, Optional ByVal ControllaCampo As Boolean = True) As Double

        Dim cmd As New OleDbCommand()
        cmd.CommandText = "Select " & Campo & " From AnagraficaComune Where Tipologia = 'R' And CodiceRegione = '" & Regione & "'"
        cmd.Connection = OspitiCon

        Dim myPOSTreader As OleDbDataReader = cmd.ExecuteReader()
        If myPOSTreader.Read Then
            CampoRegioneN = campodb(myPOSTreader.Item(Campo))
        Else
            CampoRegioneN = 0
        End If
        myPOSTreader.Close()
    End Function



    Public Function CampoRegioneT(ByVal Regione As String, ByVal Campo As String, Optional ByVal ControllaCampo As Boolean = True) As String

        Dim cmd As New OleDbCommand()
        cmd.CommandText = "Select " & Campo & " From AnagraficaComune Where Tipologia = 'R' And CodiceRegione = '" & Regione & "'"
        cmd.Connection = OspitiCon

        Dim myPOSTreader As OleDbDataReader = cmd.ExecuteReader()
        If myPOSTreader.Read Then
            CampoRegioneT = campodb(myPOSTreader.Item(Campo))
        Else
            CampoRegioneT = ""
        End If
        myPOSTreader.Close()
    End Function

    Function campodb(ByVal oggetto As Object) As String
        If IsDBNull(oggetto) Then
            Return ""
        Else
            Return oggetto
        End If
    End Function

    Function campodbd(ByVal oggetto As Object) As Date
        If IsDBNull(oggetto) Then
            Return Nothing
        Else
            Return oggetto
        End If
    End Function

    Function campodbN(ByVal oggetto As Object) As Double
        If IsDBNull(oggetto) Then
            Return 0
        Else
            Return oggetto
        End If
    End Function

    Public Function DecodificaComune(ByVal Provincia As String, ByVal Comune As String) As String
        Dim RS_Comuni As New ADODB.Recordset        

        If Trim(Provincia) <> "" And Trim(Comune) <> "" Then            
            Dim cmd As New OleDbCommand()
            cmd.CommandText = "Select * From AnagraficaComune Where CodiceProvincia = '" & Provincia & "' AND CodiceComune = '" & Comune & "' And Tipologia = 'C'"
            cmd.Connection = OspitiCon

            Dim myPOSTreader As OleDbDataReader = cmd.ExecuteReader()
            If myPOSTreader.Read Then
                DecodificaComune = campodb(myPOSTreader.Item("Nome"))
            Else
                DecodificaComune = ""
            End If
            myPOSTreader.Close()
        Else
            Dim cmd As New OleDbCommand()
            cmd.CommandText = "Select * From AnagraficaComune Where CodiceProvincia = '" & Provincia & "' AND CodiceComune = '" & Comune & "' And Tipologia = 'C'"
            cmd.Connection = OspitiCon

            Dim myPOSTreader As OleDbDataReader = cmd.ExecuteReader()
            If myPOSTreader.Read Then
                DecodificaComune = campodb(myPOSTreader.Item("Nome"))
            Else
                DecodificaComune = ""
            End If
            myPOSTreader.Close()
        End If


    End Function

    Public Function QuoteGiornaliereAssenzaCausale(ByVal Cserv As String, ByVal CodOsp As Long, ByVal Tipo As String, ByVal CodPar As Long, ByRef sc1 As Object, ByVal Causale As String, ByVal XGiorniAssenza As Integer, Optional ByVal DataImporti As Date = #1/1/1971#) As Double
        Dim Rs_Myrec As New ADODB.Recordset
        Dim Rs_Ospite As New ADODB.Recordset
        Dim Rs_Regioni As New ADODB.Recordset
        Dim CodiciParente(100) As Boolean
        Dim Data As Date
        Dim MySql As String = ""
        Dim Modalita As String = ""
        Dim ImpRetta As Double = 0
        Dim ImpComune As Double = 0
        Dim ImpJolly As Double = 0
        Dim ImpOspite As Double = 0
        Dim ImpOspite2 As Double = 0
        Dim ImportoRegione As Double = 0
        Dim ImpRegione As Double = 0
        Dim MiaRegione As String = ""
        Dim AutoSufficente As String = ""
        Dim ImpParente As Double = 0
        Dim ImpParente2 As Double = 0
        Dim ImpMiA As Double = 0
        Dim ImpParenti As Double = 0
        Dim ImpEnte As Double = 0
        Dim Regola As String = ""

        Data = DataImporti


        If Format(DataImporti, "ddmmyyyy") = 1011971 Then
            Data = Now
        Else
            Data = DataImporti
        End If


        MySql = "Select * From Modalita Where CENTROSERVIZIO = '" & Cserv & "' and CODICEOSPITE = " & CodOsp & " and"
        MySql = MySql & " Data <= {ts '" & Format(Data, "yyyy-MM-dd") & " 00:00:00'}  Order by Data Desc"

        Dim cmd As New OleDbCommand()
        cmd.CommandText = MySql
        cmd.Connection = OspitiCon

        Dim myPOSTreader As OleDbDataReader = cmd.ExecuteReader()
        If myPOSTreader.Read Then
            Modalita = campodb(myPOSTreader.Item("Modalita"))
        End If
        myPOSTreader.Close()

        


        MySql = "Select * From ImportoOspite Where CENTROSERVIZIO = '" & Cserv & "' and CODICEOSPITE = " & CodOsp & " and"
        MySql = MySql & " Data <= {ts '" & Format(Data, "yyyy-MM-dd") & " 00:00:00'}  Order by Data Desc"
        Dim cmd1 As New OleDbCommand()
        cmd1.CommandText = MySql
        cmd1.Connection = OspitiCon

        Dim RdRead1 As OleDbDataReader = cmd1.ExecuteReader()
        If RdRead1.Read Then
            If campodb(RdRead1.Item("TIPORETTA")) = "M" Then
                ImpOspite = Modulo.MathRound(Mensile(campodbN(RdRead1.Item("Importo")), Month(Data), Year(Data), Cserv), 2)
            End If
            If campodb(RdRead1.Item("TIPORETTA")) = "A" Then
                ImpOspite = Modulo.MathRound(Annuale(campodbN(RdRead1.Item("Importo")), Year(Data)), 2)
            End If
            If campodb(RdRead1.Item("TIPORETTA")) = "G" Or campodb(RdRead1.Item("TIPORETTA")) = "" Then
                ImpOspite = Modulo.MathRound(campodbN(RdRead1.Item("Importo")), 2)
            End If

            If campodb(RdRead1.Item("TIPORETTA")) = "M" Then
                ImpOspite2 = Modulo.MathRound(Mensile(campodbN(RdRead1.Item("Importo_2")), Month(Data), Year(Data), Cserv), 2)
            End If
            If campodb(RdRead1.Item("TIPORETTA")) = "A" Then
                ImpOspite2 = Modulo.MathRound(Annuale(campodbN(RdRead1.Item("Importo_2")), Year(Data)), 2)
            End If
            If campodb(RdRead1.Item("TIPORETTA")) = "G" Or campodb(RdRead1.Item("TIPORETTA")) = "" Then
                ImpOspite2 = Modulo.MathRound(campodbN(RdRead1.Item("Importo_2")), 2)
            End If
        End If
        RdRead1.Close()



        For I = 0 To 100 : CodiciParente(I) = False : Next I


        MySql = "Select * From ImportoRetta Where CENTROSERVIZIO = '" & Cserv & "' and CODICEOSPITE = " & CodOsp & " and"
        MySql = MySql & " Data <= {ts '" & Format(Data, "yyyy-MM-dd") & " 00:00:00'}  Order by Data Desc"
        Rs_Myrec.Open(MySql, OspitiDb, ADODB.CursorTypeEnum.adOpenKeyset, ADODB.LockTypeEnum.adLockOptimistic)
        If Not Rs_Myrec.EOF Then
            Rs_Myrec.MoveFirst()
            If MoveFromDbWC(Rs_Myrec, "TipoImporto") = "M" Then
                ImpRetta = Modulo.MathRound(Mensile(MoveFromDbWC(Rs_Myrec, "Importo"), Month(Data), Year(Data), Cserv), 2)
            End If
            If MoveFromDbWC(Rs_Myrec, "TipoImporto") = "A" Then
                ImpRetta = Modulo.MathRound(Annuale(MoveFromDbWC(Rs_Myrec, "Importo"), Year(Data)), 2)
            End If
            If MoveFromDbWC(Rs_Myrec, "TipoImporto") = "G" Or IsDBNull(Rs_Myrec.Fields("TipoImporto")) Then
                ImpRetta = Modulo.MathRound(MoveFromDbWC(Rs_Myrec, "Importo"), 2)
            End If
        End If

        Rs_Myrec.Close()

        MySql = "Select * From ImportoComune Where CENTROSERVIZIO = '" & Cserv & "' and CODICEOSPITE = " & CodOsp & " and"
        MySql = MySql & " Data <= {ts '" & Format(Data, "yyyy-MM-dd") & " 00:00:00'}  Order by Data Desc"
        Rs_Myrec.Open(MySql, OspitiDb, ADODB.CursorTypeEnum.adOpenKeyset, ADODB.LockTypeEnum.adLockOptimistic)
        If Not Rs_Myrec.EOF Then
            Rs_Myrec.MoveFirst()
            If MoveFromDbWC(Rs_Myrec, "TIPORETTA") = "M" Then
                ImpComune = Modulo.MathRound(Mensile(MoveFromDbWC(Rs_Myrec, "Importo"), Month(Data), Year(Data), Cserv), 2)
            End If
            If MoveFromDbWC(Rs_Myrec, "TIPORETTA") = "A" Then
                ImpComune = Modulo.MathRound(Annuale(MoveFromDbWC(Rs_Myrec, "Importo"), Year(Data)), 2)
            End If
            If MoveFromDbWC(Rs_Myrec, "TIPORETTA") = "G" Or MoveFromDbWC(Rs_Myrec, "TIPORETTA") = "" Then
                ImpComune = Modulo.MathRound(MoveFromDbWC(Rs_Myrec, "Importo"), 2)
            End If
        End If
        Rs_Myrec.Close()


        MySql = "Select * From ImportoJolly Where CENTROSERVIZIO = '" & Cserv & "' and CODICEOSPITE = " & CodOsp & " and"
        MySql = MySql & " Data <= {ts '" & Format(Data, "yyyy-MM-dd") & " 00:00:00'}  Order by Data Desc"
        Rs_Myrec.Open(MySql, OspitiDb, ADODB.CursorTypeEnum.adOpenKeyset, ADODB.LockTypeEnum.adLockOptimistic)
        If Not Rs_Myrec.EOF Then
            Rs_Myrec.MoveFirst()
            If MoveFromDbWC(Rs_Myrec, "TIPORETTA") = "M" Then
                ImpJolly = Mensile(MoveFromDbWC(Rs_Myrec, "Importo"), Month(Data), Year(Data), Cserv)
            End If
            If MoveFromDbWC(Rs_Myrec, "TIPORETTA") = "A" Then
                ImpJolly = Annuale(MoveFromDbWC(Rs_Myrec, "Importo"), Year(Data))
            End If
            If MoveFromDbWC(Rs_Myrec, "TIPORETTA") = "G" Or MoveFromDbWC(Rs_Myrec, "TIPORETTA") = "" Then
                ImpJolly = MoveFromDbWC(Rs_Myrec, "Importo")
            End If
        End If
        Rs_Myrec.Close()

        Dim MiaTipoRetta As String = ""

        MySql = "Select * From StatoAuto Where CENTROSERVIZIO = '" & Cserv & "' and CODICEOSPITE = " & CodOsp & " and"
        MySql = MySql & " Data <= {ts '" & Format(Data, "yyyy-MM-dd") & " 00:00:00'}  Order by Data Desc"

        Dim DataValidita As Date
        Dim cmdStatoAuto As New OleDbCommand()
        cmdStatoAuto.CommandText = MySql
        cmdStatoAuto.Connection = OspitiCon
        Dim StatoAutoRD As OleDbDataReader = cmdStatoAuto.ExecuteReader()
        If StatoAutoRD.Read Then
            MiaRegione = campodb(StatoAutoRD.Item("USL"))
            AutoSufficente = campodb(StatoAutoRD.Item("STATOAUTO"))
            MiaTipoRetta = campodb(StatoAutoRD.Item("TipoRetta"))
            DataValidita = campodb(StatoAutoRD.Item("DATA"))
        End If
        StatoAutoRD.Close()
        If Format(DataValidita, "yyyyMMdd") < Format(DataImporti, "yyyyMMdd") Then
            DataValidita = DataImporti
        End If


        If Trim(MiaTipoRetta) = "" Then
            MySql = "Select * From ImportoRegioni Where CODICEREGIONE = '" & MiaRegione & "' and  DATA <= {ts '" & Format(DataValidita, "yyyy-MM-dd") & " 00:00:00'}  Order by Data Desc"
        Else
            MySql = "Select * From ImportoRegioni Where CODICEREGIONE = '" & MiaRegione & "' and  TipoRetta = '" & MiaTipoRetta & "'  and DATA <= {ts '" & Format(DataValidita, "yyyy-MM-dd") & " 00:00:00'}  Order by Data Desc"
        End If


        Dim cmdRegione As New OleDbCommand()
        cmdRegione.CommandText = MySql
        cmdRegione.Connection = OspitiCon
        Dim RegioneRD As OleDbDataReader = cmdRegione.ExecuteReader()
        If RegioneRD.Read Then
            ImportoRegione = campodb(RegioneRD.Item("Importo"))
        End If
        RegioneRD.Close()



        Dim Percentuale(100) As Double
        Dim Importo(100) As Double

        MySql = "Select * From ImportoParenti Where CENTROSERVIZIO = '" & Cserv & "' and CODICEOSPITE = " & CodOsp & " and"
        MySql = MySql & " Data <= {ts '" & Format(Data, "yyyy-MM-dd") & " 00:00:00'}  Order by Data Desc"
        Rs_Myrec.Open(MySql, OspitiDb, ADODB.CursorTypeEnum.adOpenKeyset, ADODB.LockTypeEnum.adLockOptimistic)
        ImpParente = 0
        If Not Rs_Myrec.EOF Then
            Rs_Myrec.MoveFirst()
        End If
        Do While Not Rs_Myrec.EOF
            ImpMiA = 0
            If Not CodiciParente(MoveFromDbWC(Rs_Myrec, "CODICEPARENTE")) Then
                CodiciParente(MoveFromDbWC(Rs_Myrec, "CODICEPARENTE")) = True
                If MoveFromDbWC(Rs_Myrec, "TipoImporto") = "M" Then
                    ImpMiA = Mensile(MoveFromDbWC(Rs_Myrec, "Importo"), Month(Data), Year(Data), Cserv)
                End If
                If MoveFromDbWC(Rs_Myrec, "TipoImporto") = "A" Then
                    ImpMiA = Annuale(MoveFromDbWC(Rs_Myrec, "Importo"), Year(Data))
                End If
                If MoveFromDbWC(Rs_Myrec, "TipoImporto") = "G" Or MoveFromDbWC(Rs_Myrec, "TipoImporto") = "" Then
                    ImpMiA = MoveFromDbWC(Rs_Myrec, "Importo")
                End If

                Percentuale(MoveFromDbWC(Rs_Myrec, "CODICEPARENTE")) = MoveFromDbWC(Rs_Myrec, "PERCENTUALE")
                Importo(MoveFromDbWC(Rs_Myrec, "CODICEPARENTE")) = ImpMiA

                ImpParenti = ImpParenti + ImpMiA
            End If
            Rs_Myrec.MoveNext()
        Loop
        Rs_Myrec.Close()

        If Modalita = "O" Then
            If Tipo = "O1" Then
                QuoteGiornaliereAssenzaCausale = ImpRetta - ImpComune - ImportoRegione - ImpParenti - ImpJolly - ImpParente2 - ImpOspite2
                ImpOspite = ImpRetta - ImpComune - ImportoRegione - ImpParenti - ImpJolly - ImpParente2 - ImpOspite2
            Else
                QuoteGiornaliereAssenzaCausale = ImpRetta - ImpComune - ImportoRegione - ImpParenti - ImpJolly - ImpParente2
                ImpOspite = ImpRetta - ImpComune - ImportoRegione - ImpParenti - ImpJolly - ImpParente2
            End If
        End If
        If Modalita = "E" Then
            QuoteGiornaliereAssenzaCausale = ImpRetta - ImpComune - ImportoRegione - ImpParenti - ImpOspite - ImpJolly - ImpParente2 - ImpOspite2
        End If
        If Modalita = "C" Then
            QuoteGiornaliereAssenzaCausale = ImpRetta - ImportoRegione - ImpParenti - ImpOspite - ImpJolly - ImpParente2 - ImpOspite2
            ImpComune = ImpRetta - ImportoRegione - ImpParenti - ImpOspite - ImpJolly
        End If
        If Modalita = "P" Then
            QuoteGiornaliereAssenzaCausale = ImpRetta - ImpComune - ImportoRegione - ImpOspite - ImpJolly - ImpParente2 - ImpOspite2
            ImpParenti = ImpRetta - ImpComune - ImportoRegione - ImpOspite - ImpJolly
            For i = 1 To 10
                If Percentuale(i) > 0 Then
                    Importo(i) = Math.Round(ImpParenti * Percentuale(i), 2)
                End If
            Next
        End If


        QuoteGiornaliereAssenzaCausale = Modulo.MathRound(QuoteGiornaliereAssenzaCausale, 2)


        sc1.Reset()
        sc1.ExecuteStatement("DIM SalvaParente (20)")
        sc1.ExecuteStatement("DIM SalvaImpExtrOspite (20)")
        sc1.ExecuteStatement("CentroServizio = " & Chr(34) & Cserv & Chr(34))
        sc1.ExecuteStatement("SalvaComune = " & Replace(CDbl(ImpComune), ",", "."))
        sc1.ExecuteStatement("SalvaJolly = " & Replace(CDbl(ImpJolly), ",", "."))

        sc1.ExecuteStatement("SalvaParente(1) = " & Replace(Importo(1), ",", "."))
        sc1.ExecuteStatement("SalvaParente(2) = " & Replace(Importo(2), ",", "."))
        sc1.ExecuteStatement("SalvaParente(3) = " & Replace(Importo(3), ",", "."))
        sc1.ExecuteStatement("SalvaParente(4) = " & Replace(Importo(4), ",", "."))
        sc1.ExecuteStatement("SalvaParente(5) = " & Replace(Importo(5), ",", "."))
        
        sc1.ExecuteStatement("SalvaRegione = " & Replace(CDbl(ImpRegione), ",", "."))
        sc1.ExecuteStatement("SalvaOspite = " & Replace(CDbl(ImpOspite), ",", "."))
        sc1.ExecuteStatement("SalvaOspite2 = " & Replace(ImpOspite2, ",", "."))
        sc1.ExecuteStatement("SalvaEnte = " & Replace(CDbl(ImpEnte), ",", "."))
        sc1.ExecuteStatement("GiorniAssenza = " & XGiorniAssenza)
        Rs_Myrec.Open("Select * From Causali Where " & "CODICE = '" & Causale & "'", OspitiDb, ADODB.CursorTypeEnum.adOpenKeyset, ADODB.LockTypeEnum.adLockPessimistic)
        If Not Rs_Myrec.EOF Then
            Regola = MoveFromDb(Rs_Myrec.Fields("Regole"))
        End If
        Rs_Myrec.Close()


        If Tipo = "O" Then
            QuoteGiornaliereAssenzaCausale = ImpOspite
            If Tipo = "O2" Then
                QuoteGiornaliereAssenzaCausale = ImpOspite2
            End If
        End If

        If Tipo = "P" Then
            If CodPar = 0 Then
                QuoteGiornaliereAssenzaCausale = ImpParenti
            Else
                QuoteGiornaliereAssenzaCausale = Importo(CodPar)
            End If
        End If


        If Tipo = "C" Then
            QuoteGiornaliereAssenzaCausale = ImpComune
        End If

        If Tipo = "R" Then
            QuoteGiornaliereAssenzaCausale = ImpRegione
        End If

        If Regola <> "" Then
            sc1.AddCode(Regola)
            Try
                If Not IsDBNull(Regola) And Regola <> "" Then sc1.Run("Calcolo")
            Catch ex As Exception

            End Try


            If Tipo = "O" Then
                QuoteGiornaliereAssenzaCausale = sc1.Eval("SalvaOspite")
                If Tipo = "O2" Then
                    QuoteGiornaliereAssenzaCausale = sc1.Eval("SalvaOspite2")
                End If
            End If

            If Tipo = "P" Then
                If CodPar = 0 Then
                    QuoteGiornaliereAssenzaCausale = sc1.Eval("SalvaParente(1)") + sc1.Eval("SalvaParente(2)") + sc1.Eval("SalvaParente(3)") + sc1.Eval("SalvaParente(4)") + sc1.Eval("SalvaParente(5)")
                End If
                If CodPar = 1 Then
                    QuoteGiornaliereAssenzaCausale = sc1.Eval("SalvaParente(1)")
                End If
                If CodPar = 2 Then
                    QuoteGiornaliereAssenzaCausale = sc1.Eval("SalvaParente(2)")
                End If
                If CodPar = 3 Then
                    QuoteGiornaliereAssenzaCausale = sc1.Eval("SalvaParente(3)")
                End If
                If CodPar = 4 Then
                    QuoteGiornaliereAssenzaCausale = sc1.Eval("SalvaParente(4)")
                End If
                If CodPar = 5 Then
                    QuoteGiornaliereAssenzaCausale = sc1.Eval("SalvaParente(5)")
                End If
            End If


            If Tipo = "C" Then
                QuoteGiornaliereAssenzaCausale = sc1.Eval("SalvaComune")
            End If

            If Tipo = "R" Then
                QuoteGiornaliereAssenzaCausale = sc1.Eval("SalvaRegione")
            End If
        End If
    End Function

    Public Function VariazioniRettaDopoData(ByVal Cserv As String, ByVal CodOsp As Long, ByVal DataVar As Date) As Boolean

        Dim MySql As String
        Dim Campi As String

        VariazioniRettaDopoData = False


        Campi = " (Select count(Id) From Modalita Where CENTROSERVIZIO = '" & Cserv & "' and CODICEOSPITE = " & CodOsp & " and DATA > {ts '" & Format(DataVar, "yyyy-MM-dd") & " 00:00:00'} ) As CambMod, "
        Campi = Campi & " (Select count(Id) From ImportoOspite Where CENTROSERVIZIO = '" & Cserv & "' and CODICEOSPITE = " & CodOsp & " and DATA > {ts '" & Format(DataVar, "yyyy-MM-dd") & " 00:00:00'} ) As CambOSp, "
        Campi = Campi & " (Select count(Id) From ImportoRetta Where CENTROSERVIZIO = '" & Cserv & "' and CODICEOSPITE = " & CodOsp & " and DATA > {ts '" & Format(DataVar, "yyyy-MM-dd") & " 00:00:00'} ) As CambRetta, "
        Campi = Campi & " (Select count(Id) From ImportoComune Where CENTROSERVIZIO = '" & Cserv & "' and CODICEOSPITE = " & CodOsp & " and DATA > {ts '" & Format(DataVar, "yyyy-MM-dd") & " 00:00:00'} ) As CambComune, "
        Campi = Campi & " (Select count(Id) From ImportoJolly Where CENTROSERVIZIO = '" & Cserv & "' and CODICEOSPITE = " & CodOsp & " and DATA > {ts '" & Format(DataVar, "yyyy-MM-dd") & " 00:00:00'} ) As CambJolly, "
        Campi = Campi & " (Select count(Id) From ImportoParenti Where CENTROSERVIZIO = '" & Cserv & "' and CODICEOSPITE = " & CodOsp & " and DATA > {ts '" & Format(DataVar, "yyyy-MM-dd") & " 00:00:00'}) As CambParenti, "
        Campi = Campi & " (Select count(Id) From StatoAuto Where CENTROSERVIZIO = '" & Cserv & "' and CODICEOSPITE = " & CodOsp & " and DATA > {ts '" & Format(DataVar, "yyyy-MM-dd") & " 00:00:00'}) As CambStato  "
        MySql = "Select " & Campi & ",count(Id) From ImportoRegioni Where DATA > {ts '" & Format(DataVar, "yyyy-MM-dd") & " 00:00:00'}  "
        Dim cmdModalita As New OleDbCommand()
        cmdModalita.CommandText = MySql
        cmdModalita.Connection = OspitiCon
        Dim ModalitaRD As OleDbDataReader = cmdModalita.ExecuteReader()
        If ModalitaRD.Read Then
            If Val(campodb(ModalitaRD.Item("CambMod"))) > 0 Then
                VariazioniRettaDopoData = True
            End If
            If Val(campodb(ModalitaRD.Item("CambOSp"))) > 0 Then
                VariazioniRettaDopoData = True
            End If
            If Val(campodb(ModalitaRD.Item("CambRetta"))) > 0 Then
                VariazioniRettaDopoData = True
            End If
            If Val(campodb(ModalitaRD.Item("CambComune"))) > 0 Then
                VariazioniRettaDopoData = True
            End If
            If Val(campodb(ModalitaRD.Item("CambJolly"))) > 0 Then
                VariazioniRettaDopoData = True
            End If
            If Val(campodb(ModalitaRD.Item("CambParenti"))) > 0 Then
                VariazioniRettaDopoData = True
            End If
            If Val(campodb(ModalitaRD.Item("CambStato"))) > 0 Then
                VariazioniRettaDopoData = True
            End If
        End If
        ModalitaRD.Close()
    End Function

    Public Function QuotaRegione(ByVal Cserv As String, ByVal CodOsp As Long, ByVal Tipo As String, ByVal CodPar As Long, Optional ByVal DataImporti As Date = #1/1/1971#) As Double
        Dim MySQl As String
        Dim ImportoRegione As Double
        Dim MiaRegione As String = ""
        Dim AutoSufficente As String
        Dim MiaTipoRetta As String = ""
        Dim Data As Date

        Data = DataImporti



        MySQl = "Select * From StatoAuto Where CENTROSERVIZIO = '" & Cserv & "' and CODICEOSPITE = " & CodOsp & " and"
        MySQl = MySQl & " Data <= {ts '" & Format(Data, "yyyy-MM-dd") & " 00:00:00'}  Order by Data Desc"

        Dim DataValidita As Date
        Dim cmdStatoAuto As New OleDbCommand()
        cmdStatoAuto.CommandText = MySQl
        cmdStatoAuto.Connection = OspitiCon
        Dim StatoAutoRD As OleDbDataReader = cmdStatoAuto.ExecuteReader()
        If StatoAutoRD.Read Then
            MiaRegione = campodb(StatoAutoRD.Item("USL"))
            AutoSufficente = campodb(StatoAutoRD.Item("STATOAUTO"))
            MiaTipoRetta = campodb(StatoAutoRD.Item("TipoRetta"))
            DataValidita = campodb(StatoAutoRD.Item("DATA"))
        End If
        StatoAutoRD.Close()
        If Format(DataValidita, "yyyyMMdd") < Format(DataImporti, "yyyyMMdd") Then
            DataValidita = DataImporti
        End If


        If Trim(MiaTipoRetta) = "" Then
            MySQl = "Select * From ImportoRegioni Where CODICEREGIONE = '" & MiaRegione & "' and  DATA <= {ts '" & Format(DataValidita, "yyyy-MM-dd") & " 00:00:00'}  Order by Data Desc"
        Else
            MySQl = "Select * From ImportoRegioni Where CODICEREGIONE = '" & MiaRegione & "' and  TipoRetta = '" & MiaTipoRetta & "'  and DATA <= {ts '" & Format(DataValidita, "yyyy-MM-dd") & " 00:00:00'}  Order by Data Desc"
        End If


        Dim cmdRegione As New OleDbCommand()
        cmdRegione.CommandText = MySQl
        cmdRegione.Connection = OspitiCon
        Dim RegioneRD As OleDbDataReader = cmdRegione.ExecuteReader()
        If RegioneRD.Read Then
            ImportoRegione = campodb(RegioneRD.Item("Importo"))
        End If
        RegioneRD.Close()

        Return ImportoRegione
    End Function


    Public Function QuoteGiornaliere(ByVal Cserv As String, ByVal CodOsp As Long, ByVal Tipo As String, ByVal CodPar As Long, Optional ByVal DataImporti As Date = #1/1/1971#) As Double
        Dim CodiciParente(100) As Boolean
        Dim Data As Date
        Dim MySQl As String
        Dim Modalita As String = ""
        Dim ImpRetta As Double
        Dim ImpComune As Double
        Dim ImpJolly As Double
        Dim ImpOspite As Double
        Dim ImpOspite2 As Double
        Dim MiaRegione As String = ""
        Dim MiaTipoRetta As String = ""
        Dim AutoSufficente As String
        Dim ImportoRegione As Double
        Dim ImpParente As Double
        Dim ImpParente2 As Double
        Dim i As Integer
        Dim ImpMiA As Double
        Dim ImpParenti As Double
        Dim ImpEnte As Double


        Data = DataImporti





        MySQl = "Select * From Modalita Where CENTROSERVIZIO = '" & Cserv & "' and CODICEOSPITE = " & CodOsp & " and"
        MySQl = MySQl & " Data <= {ts '" & Format(Data, "yyyy-MM-dd") & " 00:00:00'}  Order by Data Desc"
        Dim cmdModalita As New OleDbCommand()
        cmdModalita.CommandText = MySQl
        cmdModalita.Connection = OspitiCon

        Dim ModalitaRD As OleDbDataReader = cmdModalita.ExecuteReader()
        If ModalitaRD.Read Then
            Modalita = campodb(ModalitaRD.Item("Modalita"))
        End If
        ModalitaRD.Close()

        MySQl = "Select * From ImportoOspite Where CENTROSERVIZIO = '" & Cserv & "' and CODICEOSPITE = " & CodOsp & " and"
        MySQl = MySQl & " Data <= {ts '" & Format(Data, "yyyy-MM-dd") & " 00:00:00'}  Order by Data Desc"

        Dim cmdImpOspite As New OleDbCommand()
        cmdImpOspite.CommandText = MySQl
        cmdImpOspite.Connection = OspitiCon
        Dim ImpOspiteRD As OleDbDataReader = cmdImpOspite.ExecuteReader()
        If ImpOspiteRD.Read Then
            If campodb(ImpOspiteRD.Item("TIPORETTA")) = "M" Then
                ImpOspite = Mensile(campodbN(ImpOspiteRD.Item("Importo")), Month(Data), Year(Data), Cserv)
            End If
            If campodb(ImpOspiteRD.Item("TIPORETTA")) = "A" Then
                ImpOspite = Annuale(campodbN(ImpOspiteRD.Item("Importo")), Year(Data))
            End If
            If campodb(ImpOspiteRD.Item("TIPORETTA")) = "G" Or campodb(ImpOspiteRD.Item("TIPORETTA")) = "" Then
                ImpOspite = campodbN(ImpOspiteRD.Item("Importo"))
            End If
            If campodb(ImpOspiteRD.Item("TIPORETTA")) = "M" Then
                ImpOspite2 = Mensile(campodbN(ImpOspiteRD.Item("Importo_2")), Month(Data), Year(Data), Cserv)
            End If
            If campodb(ImpOspiteRD.Item("TIPORETTA")) = "A" Then
                ImpOspite2 = Annuale(campodbN(ImpOspiteRD.Item("Importo_2")), Year(Data))
            End If
            If campodb(ImpOspiteRD.Item("TIPORETTA")) = "G" Or campodb(ImpOspiteRD.Item("TIPORETTA")) = "" Then
                ImpOspite2 = campodbN(ImpOspiteRD.Item("Importo_2"))
            End If
        End If
        ImpOspiteRD.Close()


        If Modalita <> "O" And (ImpOspite <> 0 Or ImpOspite2 <> 0) And Tipo = "O" Then
            Return ImpOspite + ImpOspite2
        End If
        If Modalita <> "O" And ImpOspite <> 0 And Tipo = "O1" Then
            Return ImpOspite
        End If
        If Modalita <> "O" And ImpOspite <> 0 And Tipo = "O2" Then
            Return ImpOspite2
        End If




        MySQl = "Select * From ImportoRetta Where CENTROSERVIZIO = '" & Cserv & "' and CODICEOSPITE = " & CodOsp & " and"
        MySQl = MySQl & " Data <= {ts '" & Format(Data, "yyyy-MM-dd") & " 00:00:00'}  Order by Data Desc"
        Dim cmdImpRetta As New OleDbCommand()
        cmdImpRetta.CommandText = MySQl
        cmdImpRetta.Connection = OspitiCon

        Dim ImpRettaRD As OleDbDataReader = cmdImpRetta.ExecuteReader()
        If ImpRettaRD.Read Then
            If campodb(ImpRettaRD("TipoImporto")) = "M" Then
                ImpRetta = Mensile(campodbN(ImpRettaRD.Item("Importo")), Month(Data), Year(Data), Cserv)
            End If
            If campodb(ImpRettaRD("TipoImporto")) = "A" Then
                ImpRetta = Annuale(campodbN(ImpRettaRD.Item("Importo")), Year(Data))
            End If
            If campodb(ImpRettaRD("TipoImporto")) = "G" Or campodb(ImpRettaRD.Item("TipoImporto")) = "" Then
                ImpRetta = campodbN(ImpRettaRD.Item("Importo"))
            End If
        End If
        ImpRettaRD.Close()




        MySQl = "Select * From ImportoComune Where CENTROSERVIZIO = '" & Cserv & "' and CODICEOSPITE = " & CodOsp & " and"
        If QuoteDaSoia = 1024 Then
            MySQl = MySQl & " DataSosia <= {ts '" & Format(Data, "yyyy-MM-dd") & " 00:00:00'}  Order by Data Desc"
        Else
            MySQl = MySQl & " Data <= {ts '" & Format(Data, "yyyy-MM-dd") & " 00:00:00'}  Order by Data Desc"
        End If

        'Rs_Myrec.Open(MySQl, OspitiDb, ADODB.CursorTypeEnum.adOpenKeyset, ADODB.LockTypeEnum.adLockOptimistic)

        Dim cmdImpComune As New OleDbCommand()
        cmdImpComune.CommandText = MySQl
        cmdImpComune.Connection = OspitiCon
        Dim ImpComuneRD As OleDbDataReader = cmdImpComune.ExecuteReader()

        If ImpComuneRD.Read Then
            If campodb(ImpComuneRD.Item("TIPORETTA")) = "M" Then
                ImpComune = Mensile(campodbN(ImpComuneRD.Item("Importo")), Month(Data), Year(Data), Cserv)
            End If
            If campodb(ImpComuneRD.Item("TIPORETTA")) = "A" Then
                ImpComune = Annuale(campodbN(ImpComuneRD.Item("Importo")), Year(Data))
            End If
            If campodb(ImpComuneRD.Item("TIPORETTA")) = "G" Or campodb(ImpComuneRD.Item("TIPORETTA")) = "" Then
                ImpComune = campodbN(ImpComuneRD.Item("Importo"))
            End If
        End If
        ImpComuneRD.Close()

        If Modalita <> "C" And ImpComune <> 0 And Tipo = "C" Then
            Return ImpComune
        End If


        MySQl = "Select * From ImportoJolly Where CENTROSERVIZIO = '" & Cserv & "' and CODICEOSPITE = " & CodOsp & " and"
        MySQl = MySQl & " Data <= {ts '" & Format(Data, "yyyy-MM-dd") & " 00:00:00'}  Order by Data Desc"

        Dim cmdImpJolly As New OleDbCommand()
        cmdImpJolly.CommandText = MySQl
        cmdImpJolly.Connection = OspitiCon
        Dim ImpJollyRD As OleDbDataReader = cmdImpJolly.ExecuteReader()
        If ImpJollyRD.Read Then
            If campodb(ImpJollyRD.Item("TIPORETTA")) = "M" Then
                ImpJolly = Mensile(campodbN(ImpJollyRD.Item("Importo")), Month(Data), Year(Data), Cserv)
            End If
            If campodb(ImpJollyRD.Item("TIPORETTA")) = "A" Then
                ImpJolly = Annuale(campodbN(ImpJollyRD.Item("Importo")), Year(Data))
            End If
            If campodb(ImpJollyRD.Item("TIPORETTA")) = "G" Or campodb(ImpJollyRD.Item("TIPORETTA")) = "" Then
                ImpJolly = campodbN(ImpJollyRD.Item("Importo"))
            End If
        End If
        ImpJollyRD.Close()



        MySQl = "Select * From StatoAuto Where CENTROSERVIZIO = '" & Cserv & "' and CODICEOSPITE = " & CodOsp & " and"
        MySQl = MySQl & " Data <= {ts '" & Format(Data, "yyyy-MM-dd") & " 00:00:00'}  Order by Data Desc"

        Dim DataValidita As Date
        Dim cmdStatoAuto As New OleDbCommand()
        cmdStatoAuto.CommandText = MySQl
        cmdStatoAuto.Connection = OspitiCon
        Dim StatoAutoRD As OleDbDataReader = cmdStatoAuto.ExecuteReader()
        If StatoAutoRD.Read Then
            MiaRegione = campodb(StatoAutoRD.Item("USL"))
            AutoSufficente = campodb(StatoAutoRD.Item("STATOAUTO"))
            MiaTipoRetta = campodb(StatoAutoRD.Item("TipoRetta"))
            DataValidita = campodb(StatoAutoRD.Item("DATA"))
        End If
        StatoAutoRD.Close()
        If Format(DataValidita, "yyyyMMdd") < Format(DataImporti, "yyyyMMdd") Then
            DataValidita = DataImporti
        End If


        If Trim(MiaTipoRetta) = "" Then
            MySQl = "Select * From ImportoRegioni Where CODICEREGIONE = '" & MiaRegione & "' and  DATA <= {ts '" & Format(DataValidita, "yyyy-MM-dd") & " 00:00:00'}  Order by Data Desc"
        Else
            MySQl = "Select * From ImportoRegioni Where CODICEREGIONE = '" & MiaRegione & "' and  TipoRetta = '" & MiaTipoRetta & "'  and DATA <= {ts '" & Format(DataValidita, "yyyy-MM-dd") & " 00:00:00'}  Order by Data Desc"
        End If


        Dim cmdRegione As New OleDbCommand()
        cmdRegione.CommandText = MySQl
        cmdRegione.Connection = OspitiCon
        Dim RegioneRD As OleDbDataReader = cmdRegione.ExecuteReader()
        If RegioneRD.Read Then
            If campodb(RegioneRD.Item("TIPOIMPORTO")) = "G" Then
                ImportoRegione = campodb(RegioneRD.Item("Importo"))
            End If
            If campodb(RegioneRD.Item("TIPOIMPORTO")) = "M" Then
                ImportoRegione = Mensile(campodb(RegioneRD.Item("Importo")), Month(DataValidita), Year(DataValidita), Cserv)
            End If
        End If
        RegioneRD.Close()

        For i = 0 To 100 : CodiciParente(i) = False : Next i

        MySQl = "Select * From ImportoParenti Where CENTROSERVIZIO = '" & Cserv & "' and CODICEOSPITE = " & CodOsp & " and"
        MySQl = MySQl & " Data <= {ts '" & Format(Data, "yyyy-MM-dd") & " 00:00:00'}  Order by Data Desc"
        ImpParente = 0
        ImpParente2 = 0
        Dim cmdImportoParenti As New OleDbCommand()
        cmdImportoParenti.CommandText = MySQl
        cmdImportoParenti.Connection = OspitiCon
        Dim ImportoParentiRD As OleDbDataReader = cmdImportoParenti.ExecuteReader()
        Do While ImportoParentiRD.Read
            ImpMiA = 0
            If Not CodiciParente(campodbN(ImportoParentiRD.Item("CODICEPARENTE"))) Then
                CodiciParente(campodbN(ImportoParentiRD.Item("CODICEPARENTE"))) = True
                If campodb(ImportoParentiRD.Item("TipoImporto")) = "M" Then
                    ImpMiA = Mensile(campodbN(ImportoParentiRD.Item("Importo")), Month(Data), Year(Data), Cserv)
                End If
                If campodb(ImportoParentiRD.Item("TipoImporto")) = "A" Then
                    ImpMiA = Annuale(campodbN(ImportoParentiRD.Item("Importo")), Year(Data))
                End If
                If campodb(ImportoParentiRD.Item("TipoImporto")) = "G" Or campodb(ImportoParentiRD.Item("TipoImporto")) = "" Then
                    ImpMiA = campodbN(ImportoParentiRD.Item("Importo"))
                End If

                If campodb(ImportoParentiRD.Item("TipoImporto")) = "M" Then
                    ImpParente2 = ImpParente2 + Mensile(campodbN(ImportoParentiRD.Item("Importo_2")), Month(Data), Year(Data), Cserv)
                End If
                If campodb(ImportoParentiRD.Item("TipoImporto")) = "A" Then
                    ImpParente2 = ImpParente2 + Annuale(campodbN(ImportoParentiRD.Item("Importo_2")), Year(Data))
                End If
                If campodb(ImportoParentiRD.Item("TipoImporto")) = "G" Or campodb(ImportoParentiRD.Item("TipoImporto")) = "" Then
                    ImpParente2 = ImpParente2 + campodbN(ImportoParentiRD.Item("Importo_2"))
                End If

                ImpParenti = ImpParenti + ImpMiA

            End If
        Loop
        ImportoParentiRD.Close()

        If Modalita = "O" Then
            If Tipo = "O1" Then
                ImpOspite = ImpRetta - ImpComune - ImportoRegione - ImpParenti - ImpJolly - ImpParente2 - ImpOspite2
            Else
                ImpOspite = ImpRetta - ImpComune - ImportoRegione - ImpParenti - ImpJolly - ImpParente2
            End If
        End If
        If Modalita = "E" Then
            ImpEnte = ImpRetta - ImpComune - ImportoRegione - ImpParenti - ImpOspite - ImpJolly - ImpParente2 - ImpOspite2
        End If
        If Modalita = "C" Then
            ImpComune = ImpRetta - ImportoRegione - ImpParenti - ImpOspite - ImpJolly - ImpOspite2 - ImpParente2
        End If
        If Modalita = "P" Then
            ImpParenti = ImpRetta - ImpComune - ImportoRegione - ImpOspite - ImpJolly - ImpOspite2
        End If
        If Tipo = "O" Then
            QuoteGiornaliere = ImpOspite
        End If
        If Tipo = "O1" Then
            QuoteGiornaliere = ImpOspite
        End If
        If Tipo = "O2" Then
            QuoteGiornaliere = ImpOspite2
        End If
        If Tipo = "C" Then
            QuoteGiornaliere = ImpComune
        End If
        If Tipo = "J" Then
            QuoteGiornaliere = ImpJolly
        End If
        If Tipo = "R" Then
            QuoteGiornaliere = ImportoRegione
        End If
        If Tipo = "E" Then
            QuoteGiornaliere = ImpEnte
        End If
        If Tipo = "P" Then
            If CodPar = 0 Then
                QuoteGiornaliere = ImpParenti
            Else
                MySQl = "Select * From ImportoParenti Where CENTROSERVIZIO = '" & Cserv & "' and CODICEOSPITE = " & CodOsp & " and"
                MySQl = MySQl & " Data <= {ts '" & Format(Data, "yyyy-MM-dd") & " 00:00:00'}  And CodiceParente = " & CodPar & " Order by Data Desc"

                Dim cmdImpParante As New OleDbCommand()
                cmdImpParante.CommandText = MySQl
                cmdImpParante.Connection = OspitiCon
                Dim ImpParRD As OleDbDataReader = cmdImpParante.ExecuteReader()
                If ImpParRD.Read Then
                    If IsDBNull(ImpParRD.Item("Percentuale")) Then 'Or Rs_Myrec.Fields("Percentuale").Value = 0
                        QuoteGiornaliere = ImpParenti
                    Else
                        If campodbN(ImpParRD.Item("Importo")) > 0 Then
                            If campodb(ImpParRD.Item("TipoImporto")) = "M" Then
                                QuoteGiornaliere = Mensile(campodbN(ImpParRD.Item("Importo")) + campodbN(ImpParRD.Item("Importo_2")), Month(Data), Year(Data), Cserv)
                            End If
                            If campodb(ImpParRD.Item("TipoImporto")) = "A" Then
                                QuoteGiornaliere = Annuale(campodbN(ImpParRD.Item("Importo")) + campodbN(ImpParRD.Item("Importo_2")), Year(Data))
                            End If
                            If campodb(ImpParRD.Item("TipoImporto")) = "G" Or campodb(ImpParRD.Item("TipoImporto")) = "" Then
                                QuoteGiornaliere = campodbN(ImpParRD.Item("Importo")) + campodbN(ImpParRD.Item("Importo_2"))
                            End If
                        Else
                            QuoteGiornaliere = Modulo.MathRound((ImpParenti * campodbN(ImpParRD.Item("Percentuale"))) + campodbN(ImpParRD.Item("Importo_2")), 2)
                        End If
                    End If
                End If
                ImpParRD.Close()
            End If
        End If

        If Tipo = "P1" Then
            If CodPar = 0 Then
                QuoteGiornaliere = ImpParenti
            Else
                MySQl = "Select * From ImportoParenti Where CENTROSERVIZIO = '" & Cserv & "' and CODICEOSPITE = " & CodOsp & " and"
                MySQl = MySQl & " Data <= {ts '" & Format(Data, "yyyy-MM-dd") & " 00:00:00'}  And CodiceParente = " & CodPar & " Order by Data Desc"

                Dim cmdImpParante As New OleDbCommand()
                cmdImpParante.CommandText = MySQl
                cmdImpParante.Connection = OspitiCon
                Dim ImpParRD As OleDbDataReader = cmdImpParante.ExecuteReader()
                If ImpParRD.Read Then
                    If IsDBNull(ImpParRD.Item("Percentuale")) Then 'Or Rs_Myrec.Fields("Percentuale").Value = 0
                        QuoteGiornaliere = ImpParenti
                    Else
                        If campodbN(ImpParRD.Item("Importo")) > 0 Then
                            If campodb(ImpParRD.Item("TipoImporto")) = "M" Then
                                QuoteGiornaliere = Mensile(campodbN(ImpParRD.Item("Importo")), Month(Data), Year(Data), Cserv)
                            End If
                            If campodb(ImpParRD.Item("TipoImporto")) = "A" Then
                                QuoteGiornaliere = Annuale(campodbN(ImpParRD.Item("Importo")), Year(Data))
                            End If
                            If campodb(ImpParRD.Item("TipoImporto")) = "G" Or campodb(ImpParRD.Item("TipoImporto")) = "" Then
                                QuoteGiornaliere = campodbN(ImpParRD.Item("Importo"))
                            End If
                        Else
                            QuoteGiornaliere = Modulo.MathRound(ImpParenti * campodbN(ImpParRD.Item("Percentuale")), 2)
                        End If
                    End If
                End If
                ImpParRD.Close()
            End If
        End If

        If Tipo = "P2" Then
            If CodPar = 0 Then
                QuoteGiornaliere = ImpParenti
            Else
                MySQl = "Select * From ImportoParenti Where CENTROSERVIZIO = '" & Cserv & "' and CODICEOSPITE = " & CodOsp & " and"
                MySQl = MySQl & " Data <= {ts '" & Format(Data, "yyyy-MM-dd") & " 00:00:00'}  And CodiceParente = " & CodPar & " Order by Data Desc"

                Dim cmdImpParante As New OleDbCommand()
                cmdImpParante.CommandText = MySQl
                cmdImpParante.Connection = OspitiCon
                Dim ImpParRD As OleDbDataReader = cmdImpParante.ExecuteReader()
                If ImpParRD.Read Then
                    If IsDBNull(ImpParRD.Item("Percentuale")) Then 'Or Rs_Myrec.Fields("Percentuale").Value = 0
                        QuoteGiornaliere = 0
                    Else
                        If campodbN(ImpParRD.Item("Importo_2")) > 0 Then
                            If campodb(ImpParRD.Item("TipoImporto")) = "M" Then
                                QuoteGiornaliere = Mensile(campodbN(ImpParRD.Item("Importo_2")), Month(Data), Year(Data), Cserv)
                            End If
                            If campodb(ImpParRD.Item("TipoImporto")) = "A" Then
                                QuoteGiornaliere = Annuale(campodbN(ImpParRD.Item("Importo_2")), Year(Data))
                            End If
                            If campodb(ImpParRD.Item("TipoImporto")) = "G" Or campodb(ImpParRD.Item("TipoImporto")) = "" Then
                                QuoteGiornaliere = campodbN(ImpParRD.Item("Importo_2"))
                            End If
                        Else
                            QuoteGiornaliere = 0
                        End If
                    End If
                End If
                ImpParRD.Close()
            End If
        End If
        QuoteGiornaliere = Modulo.MathRound(QuoteGiornaliere, 10)

    End Function
    Public Function QuoteMensile(ByVal Cserv As String, ByVal CodOsp As Long, ByVal Tipo As String, ByVal CodPar As Long, Optional ByVal DataImporti As Date = #1/1/1971#) As Double
        Dim CodiciParente(100) As Boolean
        Dim MySql As String, Modalita As String = ""
        Dim ImpParente As Double
        Dim ImpMiA As Double
        Dim ImpParenti As Double

        QuoteMensile = 0
        If Tipo = "O" Or Tipo = "O1" Then
            MySql = "Select * From ImportoOspite Where CENTROSERVIZIO = '" & Cserv & "' and CODICEOSPITE = " & CodOsp & " and"
            MySql = MySql & " Data <= {ts '" & Format(DataImporti, "yyyy-MM-dd") & " 00:00:00'}  Order by Data Desc"
            Dim LeggiImportoOspite As New OleDbCommand()
            LeggiImportoOspite.CommandText = MySql
            LeggiImportoOspite.Connection = OspitiCon
            Dim RecordImportoOspite As OleDbDataReader = LeggiImportoOspite.ExecuteReader()
            If RecordImportoOspite.Read Then
                If campodb(RecordImportoOspite.Item("TIPORETTA")) = "M" Then
                    QuoteMensile = campodbN(RecordImportoOspite.Item("IMPORTO"))
                    If QuoteMensile = 0 Then

                        MySql = "Select * From Modalita Where CENTROSERVIZIO = '" & Cserv & "' and CODICEOSPITE = " & CodOsp & " and"
                        MySql = MySql & " Data <= {ts '" & Format(DataImporti, "yyyy-MM-dd") & " 00:00:00'}  Order by Data Desc"

                        Dim LeggiModalita As New OleDbCommand()
                        LeggiModalita.CommandText = MySql
                        LeggiModalita.Connection = OspitiCon
                        Dim RecordModalita As OleDbDataReader = LeggiModalita.ExecuteReader()
                        If RecordModalita.Read Then
                            Modalita = campodb(RecordModalita.Item("MODALITA"))
                        End If
                        RecordModalita.Close()

                        If Modalita = "O" And QuoteGiornaliere(Cserv, CodOsp, "P", 0, DataImporti) = 0 And QuoteGiornaliere(Cserv, CodOsp, "C", 0, DataImporti) = 0 And QuoteGiornaliere(Cserv, CodOsp, "R", 0, DataImporti) = 0 Then
                            MySql = "Select * From IMPORTORETTA Where CENTROSERVIZIO = '" & Cserv & "' and CODICEOSPITE = " & CodOsp & " and"
                            MySql = MySql & " Data <= {ts '" & Format(DataImporti, "yyyy-MM-dd") & " 00:00:00'}  Order by Data Desc"

                            Dim LeggiImportoRetta As New OleDbCommand()

                            LeggiImportoRetta.CommandText = MySql
                            LeggiImportoRetta.Connection = OspitiCon
                            Dim RecordImportoRetta As OleDbDataReader = LeggiImportoRetta.ExecuteReader()
                            If RecordImportoRetta.Read Then
                                If campodb(RecordImportoRetta.Item("TIPOIMPORTO")) = "M" Then
                                    QuoteMensile = campodb(RecordImportoRetta.Item("IMPORTO"))
                                End If
                            End If
                            RecordImportoRetta.Close()
                        End If
                    End If
                End If
            End If
            RecordImportoOspite.Close()
        End If
        If Tipo = "O2" Then
            MySql = "Select * From ImportoOspite Where CENTROSERVIZIO = '" & Cserv & "' and CODICEOSPITE = " & CodOsp & " and"
            MySql = MySql & " Data <= {ts '" & Format(DataImporti, "yyyy-MM-dd") & " 00:00:00'}  Order by Data Desc"
            Dim LeggiImportoOspite As New OleDbCommand()
            LeggiImportoOspite.CommandText = MySql
            LeggiImportoOspite.Connection = OspitiCon
            Dim RecordImportoOspite As OleDbDataReader = LeggiImportoOspite.ExecuteReader()
            If RecordImportoOspite.Read Then
                If campodb(RecordImportoOspite.Item("TIPORETTA")) = "M" Then
                    QuoteMensile = campodbN(RecordImportoOspite.Item("IMPORTO_2"))
                    If QuoteMensile = 0 Then

                        MySql = "Select * From Modalita Where CENTROSERVIZIO = '" & Cserv & "' and CODICEOSPITE = " & CodOsp & " and"
                        MySql = MySql & " Data <= {ts '" & Format(DataImporti, "yyyy-MM-dd") & " 00:00:00'}  Order by Data Desc"

                        Dim LeggiModalita As New OleDbCommand()
                        LeggiModalita.CommandText = MySql
                        LeggiModalita.Connection = OspitiCon
                        Dim RecordModalita As OleDbDataReader = LeggiModalita.ExecuteReader()
                        If RecordModalita.Read Then
                            Modalita = campodb(RecordModalita.Item("MODALITA"))
                        End If
                        RecordModalita.Close()

                        If Modalita = "O" And QuoteGiornaliere(Cserv, CodOsp, "P", 0, DataImporti) = 0 And QuoteGiornaliere(Cserv, CodOsp, "C", 0, DataImporti) = 0 And QuoteGiornaliere(Cserv, CodOsp, "R", 0, DataImporti) = 0 Then
                            QuoteMensile = 0
                        End If
                    End If
                End If
            End If
            RecordImportoOspite.Close()
        End If

        If Tipo = "P" Or Tipo = "P1" Then
            MySql = "Select * From ImportoParenti Where CENTROSERVIZIO = '" & Cserv & "' and CODICEOSPITE = " & CodOsp & " and CodiceParente = " & CodPar & " And "
            MySql = MySql & " Data <= {ts '" & Format(DataImporti, "yyyy-MM-dd") & " 00:00:00'}  Order by Data Desc"

            Dim LeggiImportoParenti As New OleDbCommand()
            LeggiImportoParenti.CommandText = MySql
            LeggiImportoParenti.Connection = OspitiCon
            Dim RecordImportoParenti As OleDbDataReader = LeggiImportoParenti.ExecuteReader()

            ImpParente = 0

            Do While RecordImportoParenti.Read
                ImpMiA = 0
                If Not CodiciParente(campodb(RecordImportoParenti.Item("CODICEPARENTE"))) Then
                    CodiciParente(campodb(RecordImportoParenti.Item("CODICEPARENTE"))) = True
                    If campodb(RecordImportoParenti.Item("TipoImporto")) = "M" Then
                        QuoteMensile = campodbN(RecordImportoParenti.Item("Importo"))
                        If QuoteMensile = 0 Then

                            MySql = "Select * From Modalita Where CENTROSERVIZIO = '" & Cserv & "' and CODICEOSPITE = " & CodOsp & " and"
                            MySql = MySql & " Data <= {ts '" & Format(DataImporti, "yyyy-MM-dd") & " 00:00:00'}  Order by Data Desc"


                            Dim LeggiModalita As New OleDbCommand()
                            LeggiModalita.CommandText = MySql
                            LeggiModalita.Connection = OspitiCon
                            Dim RecordModalita As OleDbDataReader = LeggiModalita.ExecuteReader()

                            If RecordModalita.Read Then
                                Modalita = campodb(RecordModalita.Item("MODALITA"))
                            End If
                            RecordModalita.Close()

                            If Modalita = "P" And QuoteGiornaliere(Cserv, CodOsp, "O", 0, DataImporti) = 0 And QuoteGiornaliere(Cserv, CodOsp, "C", 0, DataImporti) = 0 And QuoteGiornaliere(Cserv, CodOsp, "R", 0, DataImporti) = 0 Then
                                MySql = "Select * From IMPORTORETTA Where CENTROSERVIZIO = '" & Cserv & "' and CODICEOSPITE = " & CodOsp & " and"
                                MySql = MySql & " Data <= {ts '" & Format(DataImporti, "yyyy-MM-dd") & " 00:00:00'}  Order by Data Desc"

                                Dim LeggiImportoRetta As New OleDbCommand()

                                LeggiImportoRetta.CommandText = MySql
                                LeggiImportoRetta.Connection = OspitiCon
                                Dim RecordImportoRetta As OleDbDataReader = LeggiImportoRetta.ExecuteReader()
                                If RecordImportoRetta.Read Then
                                    If campodb(RecordImportoRetta.Item("TIPOIMPORTO")) = "M" Then
                                        QuoteMensile = campodb(RecordImportoRetta.Item("IMPORTO"))
                                    End If
                                End If
                                RecordImportoRetta.Close()
                            End If
                        End If
                    End If
                    ImpParenti = ImpParenti + ImpMiA
                End If
            Loop
            RecordImportoParenti.Close()
        End If

        If Tipo = "P2" Then
            MySql = "Select * From ImportoParenti Where CENTROSERVIZIO = '" & Cserv & "' and CODICEOSPITE = " & CodOsp & " and CodiceParente = " & CodPar & " And "
            MySql = MySql & " Data <= {ts '" & Format(DataImporti, "yyyy-MM-dd") & " 00:00:00'}  Order by Data Desc"

            Dim LeggiImportoParenti As New OleDbCommand()
            LeggiImportoParenti.CommandText = MySql
            LeggiImportoParenti.Connection = OspitiCon
            Dim RecordImportoParenti As OleDbDataReader = LeggiImportoParenti.ExecuteReader()

            ImpParente = 0

            Do While RecordImportoParenti.Read
                ImpMiA = 0
                If Not CodiciParente(campodb(RecordImportoParenti.Item("CODICEPARENTE"))) Then
                    CodiciParente(campodb(RecordImportoParenti.Item("CODICEPARENTE"))) = True
                    If campodb(RecordImportoParenti.Item("TipoImporto")) = "M" Then
                        QuoteMensile = campodbN(RecordImportoParenti.Item("Importo_2"))
                        If QuoteMensile = 0 Then

                            MySql = "Select * From Modalita Where CENTROSERVIZIO = '" & Cserv & "' and CODICEOSPITE = " & CodOsp & " and"
                            MySql = MySql & " Data <= {ts '" & Format(DataImporti, "yyyy-MM-dd") & " 00:00:00'}  Order by Data Desc"


                            Dim LeggiModalita As New OleDbCommand()
                            LeggiModalita.CommandText = MySql
                            LeggiModalita.Connection = OspitiCon
                            Dim RecordModalita As OleDbDataReader = LeggiModalita.ExecuteReader()

                            If RecordModalita.Read Then
                                Modalita = campodb(RecordModalita.Item("MODALITA"))
                            End If
                            RecordModalita.Close()

                            If Modalita = "P" And QuoteGiornaliere(Cserv, CodOsp, "O", 0, DataImporti) = 0 And QuoteGiornaliere(Cserv, CodOsp, "C", 0, DataImporti) = 0 And QuoteGiornaliere(Cserv, CodOsp, "R", 0, DataImporti) = 0 Then
                                QuoteMensile = 0
                            End If
                        End If
                    End If
                    ImpParenti = ImpParenti + ImpMiA
                End If
            Loop
            RecordImportoParenti.Close()
        End If


    End Function

    Public Sub MoveToDb(ByRef MyField As ADODB.Field, ByVal MyVal As Object)
        Select Case Val(MyField.Type)
            Case ADODB.DataTypeEnum.adDBDate Or ADODB.DataTypeEnum.adDBTimeStamp
                If IsDate(MyVal) Then
                    MyField.Value = MyVal
                Else
                    MyField.Value = 0
                End If
            Case ADODB.DataTypeEnum.adDouble
                If IsNumeric(MyVal) Then
                    MyField.Value = MyVal
                Else
                    MyField.Value = 0
                End If
            Case ADODB.DataTypeEnum.adSmallInt
                If IsNumeric(MyVal) Then
                    MyField.Value = MyVal
                Else : MyField.Value = 0
                End If
            Case ADODB.DataTypeEnum.adInteger
                If IsNumeric(MyVal) Then
                    MyField.Value = MyVal
                Else
                    MyField.Value = 0
                End If
            Case ADODB.DataTypeEnum.adNumeric
                If IsNumeric(MyVal) Then
                    MyField.Value = MyVal
                Else
                    MyField.Value = 0
                End If
            Case ADODB.DataTypeEnum.adTinyInt
                If IsNumeric(MyVal) Then
                    MyField.Value = MyVal
                Else
                    MyField.Value = 0
                End If
            Case ADODB.DataTypeEnum.adSingle
                If IsNumeric(MyVal) Then
                    MyField.Value = MyVal
                Else
                    MyField.Value = 0
                End If
            Case ADODB.DataTypeEnum.adLongVarBinary
                If IsNumeric(MyVal) Then
                    MyField.Value = MyVal
                Else
                    MyField.Value = 0
                End If
            Case ADODB.DataTypeEnum.adUnsignedTinyInt
                If IsNumeric(MyVal) Then
                    MyField.Value = MyVal
                Else
                    MyField.Value = 0
                End If
            Case ADODB.DataTypeEnum.adVarChar Or ADODB.DataTypeEnum.adVarWChar Or ADODB.DataTypeEnum.adWChar Or ADODB.DataTypeEnum.adBSTR
                If IsDBNull(MyVal) Then
                    MyField.Value = ""
                Else
                    If Len(MyVal) > MyField.DefinedSize Then
                        MyField.Value = Mid(MyVal, 1, MyField.DefinedSize)
                    Else
                        MyField.Value = MyVal
                    End If
                End If
            Case Else
                If Val(MyField.Type) = ADODB.DataTypeEnum.adVarChar Or Val(MyField.Type) = ADODB.DataTypeEnum.adVarWChar Or _
                   Val(MyField.Type) = ADODB.DataTypeEnum.adWChar Or Val(MyField.Type) = ADODB.DataTypeEnum.adBSTR Then
                    If IsDBNull(MyVal) Then
                        MyField.Value = ""
                    Else
                        If Len(MyVal) > MyField.DefinedSize Then
                            MyField.Value = Mid(MyVal, 1, MyField.DefinedSize)
                        Else
                            MyField.Value = MyVal
                        End If
                    End If
                Else
                    MyField.Value = MyVal
                End If
        End Select

    End Sub

    Public Function DecodificaTabella(ByVal TipTab As String, ByVal CodTab As String) As String
        Dim MyRs As New ADODB.Recordset
        DecodificaTabella = ""
        MyRs.Open("Select * From Tabelle Where TIPTAB = '" & TipTab & "' AND CODTAB = '" & CodTab & "'", OspitiDb, ADODB.CursorTypeEnum.adOpenKeyset, ADODB.LockTypeEnum.adLockOptimistic)
        If Not MyRs.EOF Then
            DecodificaTabella = MoveFromDb(MyRs.Fields("Descrizione"))
        End If
        MyRs.Close()

    End Function
    Public Function CampoParametri(ByVal Campo As String) As Object
        Dim MyRs As New ADODB.Recordset
        Dim Condizione As String
        Condizione = "Select * From TabellaParametri"
        MyRs.Open(Condizione, OspitiDb, ADODB.CursorTypeEnum.adOpenKeyset, ADODB.LockTypeEnum.adLockOptimistic)
        If MyRs.EOF Then
            CampoParametri = ""
        Else
            CampoParametri = MoveFromDb(MyRs.Fields(Campo))
        End If
        MyRs.Close()
    End Function
    Public Function CampoParente(ByVal CodOsp As Long, ByVal CODICEPARENTE As Byte, ByVal Campo As String, Optional ByVal ControllaCampo As Boolean = True) As String
        Dim MyRs As New ADODB.Recordset
        Dim Condizione As String
        If ControllaCampo Then
            Condizione = "Select * From AnagraficaComune Where Tipologia = 'P' And CODICEOSPITE = " & CodOsp & " And CODICEPARENTE = " & CODICEPARENTE
            MyRs.Open(Condizione, OspitiDb, ADODB.CursorTypeEnum.adOpenKeyset, ADODB.LockTypeEnum.adLockOptimistic)
        Else
            Condizione = "Select " & Campo & " From AnagraficaComune Where Tipologia = 'P' And CODICEOSPITE = " & CodOsp & " And CODICEPARENTE = " & CODICEPARENTE
            MyRs.Open(Condizione, OspitiDb, ADODB.CursorTypeEnum.adOpenKeyset, ADODB.LockTypeEnum.adLockOptimistic)
        End If
        If MyRs.EOF Then
            CampoParente = ""
        Else
            CampoParente = MoveFromDb(MyRs.Fields(Campo))
        End If
        MyRs.Close()
    End Function

    'Private Function ImportoDocumentoOspite2(ByVal Mastro As Integer, ByVal Conto As Long, ByVal SottoConto As Double, ByVal CentroServizio As String, ByVal Anno As Integer, ByVal Mese As Byte, ByVal Tipologia As String, ByVal AnnoRiferimento As Integer, ByVal MeseRiferimento As Byte, ByVal CodiceOspite As Long, ByVal TipoExtra As String) As Double
    '    Dim MyRs As New ADODB.Recordset
    '    Dim MyRsSrc As New ADODB.Recordset
    '    Dim GeneraleDB As New ADODB.Connection
    '    Dim MySql As String


    '    '" And MovimentiContabiliTesta.CentroServizio = '" & CentroServizio & "'" & _

    '    GeneraleDB.Open(ConnessioneGenerale)
    '    MySql = "SELECT MovimentiContabiliTesta.FatturaDiAnticipo,NumeroRegistrazione " & _
    '        " FROM MovimentiContabiliRiga INNER JOIN MovimentiContabiliTesta " & _
    '        " ON MovimentiContabiliRiga.Numero = MovimentiContabiliTesta.NumeroRegistrazione " & _
    '        " WHERE MovimentiContabiliRiga.MastroPartita = " & Mastro & _
    '        " AND MovimentiContabiliRiga.ContoPartita = " & Conto & _
    '        " AND MovimentiContabiliRiga.SottocontoPartita = " & SottoConto & _
    '        " AND (Select count(*) From MovimentiContabiliRiga as Riga Where Riga.AnnoRiferimento = " & AnnoRiferimento & _
    '        " AND Riga.MeseRiferimento = " & MeseRiferimento & " And Riga.Numero = MovimentiContabiliTesta.NumeroRegistrazione) > 0"
    '    If Trim(Tipologia) <> "" Then
    '        MySql = MySql & " AND MovimentiContabiliTesta.Tipologia = '" & Tipologia & "'"
    '    End If
    '    If CodiceOspite > 0 Then
    '        MySql = MySql & " AND MovimentiContabiliTesta.CodiceOspite = " & CodiceOspite
    '    End If
    '    ImportoDocumentoOspite2 = 0
    '    MyRs.Open(MySql, GeneraleDB, ADODB.CursorTypeEnum.adOpenKeyset, ADODB.LockTypeEnum.adLockOptimistic)
    '    Do While Not MyRs.EOF
    '        If IsDBNull(MyRs.Fields("FatturaDiAnticipo")) Or IsNothing(MyRs.Fields("FatturaDiAnticipo")) Or MoveFromDbWC(MyRs, "FatturaDiAnticipo") = "" Then
    '            MyRsSrc.Open("Select * From MovimentiContabiliRiga Where Numero = " & MyRs.Fields("NumeroRegistrazione").Value & " And RigaDaCausale = 13 And AnnoRiferimento =  " & AnnoRiferimento & " And MeseRiferimento = " & MeseRiferimento, GeneraleDB, ADODB.CursorTypeEnum.adOpenKeyset, ADODB.LockTypeEnum.adLockOptimistic)
    '            Do While Not MyRsSrc.EOF

    '                If (MoveFromDbWC(MyRsSrc, "TipoExtra") = "2A" Or MoveFromDbWC(MyRsSrc, "TipoExtra") = "1A" Or MoveFromDbWC(MyRsSrc, "TipoExtra") = "2P" Or MoveFromDbWC(MyRsSrc, "TipoExtra") = "1P" Or MoveFromDbWC(MyRsSrc, "TipoExtra") = "" Or MoveFromDbWC(MyRsSrc, "TipoExtra") = "A" & TipoExtra) And (MoveFromDbWC(MyRsSrc, "CentroServizio") = CentroServizio Or (MoveFromDbWC(MyRs, "CentroServizio") = CentroServizio And MoveFromDbWC(MyRsSrc, "CentroServizio") = "")) Then
    '                    ImportoDocumentoOspite2 = ImportoDocumentoOspite2 + MyRsSrc.Fields("Importo").Value
    '                End If
    '                MyRsSrc.MoveNext()
    '            Loop
    '            MyRsSrc.Close()
    '        End If
    '        MyRs.MoveNext()
    '    Loop
    '    MyRs.Close()
    'End Function


    Private Function ImportoDocumentoOspite2(ByVal Mastro As Integer, ByVal Conto As Long, ByVal SottoConto As Double, ByVal CentroServizio As String, ByVal Anno As Integer, ByVal Mese As Byte, ByVal Tipologia As String, ByVal AnnoRiferimento As Integer, ByVal MeseRiferimento As Byte, ByVal CodiceOspite As Long, ByVal TipoExtra As String, Optional ByVal ScorporoIVA As Boolean = False, Optional ByVal ConnessioneTabelle As String = "") As Double
        Dim MyRs As New ADODB.Recordset
        Dim MyRsSrc As New ADODB.Recordset
        Dim GeneraleDB As New ADODB.Connection
        Dim MySql As String


        '" And MovimentiContabiliTesta.CentroServizio = '" & CentroServizio & "'" & _

        GeneraleDB.Open(ConnessioneGenerale)
        MySql = "SELECT MovimentiContabiliTesta.FatturaDiAnticipo,NumeroRegistrazione " & _
                " FROM MovimentiContabiliRiga INNER JOIN MovimentiContabiliTesta " & _
                " ON MovimentiContabiliRiga.Numero = MovimentiContabiliTesta.NumeroRegistrazione " & _
                " WHERE MovimentiContabiliRiga.MastroPartita = " & Mastro & _
                " AND MovimentiContabiliRiga.ContoPartita = " & Conto & _
                " AND MovimentiContabiliRiga.SottocontoPartita = " & SottoConto & _
                " AND (Select count(*) From MovimentiContabiliRiga as Riga Where Riga.AnnoRiferimento = " & AnnoRiferimento & _
                " AND Riga.MeseRiferimento = " & MeseRiferimento & " And Riga.Numero = MovimentiContabiliTesta.NumeroRegistrazione) > 0"
        If Trim(Tipologia) <> "" Then
            MySql = MySql & " AND MovimentiContabiliTesta.Tipologia = '" & Tipologia & "'"
        End If
        If CodiceOspite > 0 Then
            MySql = MySql & " AND MovimentiContabiliTesta.CodiceOspite = " & CodiceOspite
        End If
        ImportoDocumentoOspite2 = 0
        MyRs.Open(MySql, GeneraleDB, ADODB.CursorTypeEnum.adOpenKeyset, ADODB.LockTypeEnum.adLockOptimistic)
        Do While Not MyRs.EOF
            If IsDBNull(MyRs.Fields("FatturaDiAnticipo")) Or IsNothing(MyRs.Fields("FatturaDiAnticipo")) Or MoveFromDbWC(MyRs, "FatturaDiAnticipo") = "" Then
                'And RigaDaCausale = 13 
                MyRsSrc.Open("Select * From MovimentiContabiliRiga Where Numero = " & MyRs.Fields("NumeroRegistrazione").Value & " And (RigaDaCausale = 13 OR RigaDaCausale = 5 OR RigaDaCausale = 6 ) And AnnoRiferimento =  " & AnnoRiferimento & " And MeseRiferimento = " & MeseRiferimento, GeneraleDB, ADODB.CursorTypeEnum.adOpenKeyset, ADODB.LockTypeEnum.adLockOptimistic)
                Do While Not MyRsSrc.EOF
                    If (MoveFromDbWC(MyRsSrc, "TipoExtra") = "2A" Or MoveFromDbWC(MyRsSrc, "TipoExtra") = "1A" Or MoveFromDbWC(MyRsSrc, "TipoExtra") = "2P" Or MoveFromDbWC(MyRsSrc, "TipoExtra") = "1P" Or MoveFromDbWC(MyRsSrc, "TipoExtra") = "" Or MoveFromDbWC(MyRsSrc, "TipoExtra") = "A" & TipoExtra) And (MoveFromDbWC(MyRsSrc, "CentroServizio") = CentroServizio Or (MoveFromDbWC(MyRs, "CentroServizio") = CentroServizio And MoveFromDbWC(MyRsSrc, "CentroServizio") = "")) Then
                        Dim AppoggioImporto As Double = MyRsSrc.Fields("Importo").Value
                        If ScorporoIVA = True Then
                            Dim CodIVA As New Cls_IVA

                            CodIVA.Codice = MyRsSrc.Fields("CODICEIVA").Value
                            CodIVA.Leggi(ConnessioneTabelle, CodIVA.Codice)

                            AppoggioImporto = AppoggioImporto + Modulo.MathRound(AppoggioImporto * CodIVA.Aliquota, 2)
                        End If

                        If MoveFromDbWC(MyRsSrc, "DareAvere") = "A" Then
                            ImportoDocumentoOspite2 = ImportoDocumentoOspite2 + AppoggioImporto
                        Else
                            ImportoDocumentoOspite2 = ImportoDocumentoOspite2 - AppoggioImporto
                        End If
                    End If
                    MyRsSrc.MoveNext()
                Loop
                MyRsSrc.Close()
            End If
            MyRs.MoveNext()
        Loop
        MyRs.Close()
    End Function

    Private Function ImportoDocumentoOspite(ByVal Mastro As Integer, ByVal Conto As Long, ByVal SottoConto As Double, ByVal CentroServizio As String, ByVal Anno As Integer, ByVal Mese As Byte, ByVal Tipologia As String, ByVal AnnoRiferimento As Integer, ByVal MeseRiferimento As Byte, ByVal CodiceOspite As Long, ByVal TipoExtra As String, Optional ByVal ScorporoIVA As Boolean = False, Optional ByVal ConnessioneTabelle As String = "") As Double
        Dim MyRs As New ADODB.Recordset
        Dim MyRsSrc As New ADODB.Recordset
        Dim GeneraleDB As New ADODB.Connection
        Dim MySql As String


        '" And MovimentiContabiliTesta.CentroServizio = '" & CentroServizio & "'" & _

        GeneraleDB.Open(ConnessioneGenerale)
        MySql = "SELECT MovimentiContabiliTesta.FatturaDiAnticipo,NumeroRegistrazione " & _
                " FROM MovimentiContabiliRiga INNER JOIN MovimentiContabiliTesta " & _
                " ON MovimentiContabiliRiga.Numero = MovimentiContabiliTesta.NumeroRegistrazione " & _
                " WHERE MovimentiContabiliRiga.MastroPartita = " & Mastro & _
                " AND MovimentiContabiliRiga.ContoPartita = " & Conto & _
                " AND MovimentiContabiliRiga.SottocontoPartita = " & SottoConto & _
                " AND (Select count(*) From MovimentiContabiliRiga as Riga Where Riga.AnnoRiferimento = " & AnnoRiferimento & _
                " AND Riga.MeseRiferimento = " & MeseRiferimento & " And Riga.Numero = MovimentiContabiliTesta.NumeroRegistrazione) > 0"
        If Trim(Tipologia) <> "" Then
            MySql = MySql & " AND MovimentiContabiliTesta.Tipologia = '" & Tipologia & "'"
        End If
        If CodiceOspite > 0 Then
            MySql = MySql & " AND MovimentiContabiliTesta.CodiceOspite = " & CodiceOspite
        End If
        ImportoDocumentoOspite = 0
        MyRs.Open(MySql, GeneraleDB, ADODB.CursorTypeEnum.adOpenKeyset, ADODB.LockTypeEnum.adLockOptimistic)
        Do While Not MyRs.EOF
            If IsDBNull(MyRs.Fields("FatturaDiAnticipo")) Or IsNothing(MyRs.Fields("FatturaDiAnticipo")) Or MoveFromDbWC(MyRs, "FatturaDiAnticipo") = "" Then
                MyRsSrc.Open("Select * From MovimentiContabiliRiga Where Numero = " & MyRs.Fields("NumeroRegistrazione").Value & " And RigaDaCausale <> 13 And AnnoRiferimento =  " & AnnoRiferimento & " And MeseRiferimento = " & MeseRiferimento, GeneraleDB, ADODB.CursorTypeEnum.adOpenKeyset, ADODB.LockTypeEnum.adLockOptimistic)
                Do While Not MyRsSrc.EOF
                    If (MoveFromDbWC(MyRsSrc, "TipoExtra") = "2A" Or MoveFromDbWC(MyRsSrc, "TipoExtra") = "1A" Or MoveFromDbWC(MyRsSrc, "TipoExtra") = "2P" Or MoveFromDbWC(MyRsSrc, "TipoExtra") = "1P" Or MoveFromDbWC(MyRsSrc, "TipoExtra") = "" Or MoveFromDbWC(MyRsSrc, "TipoExtra") = "A" & TipoExtra) And (MoveFromDbWC(MyRsSrc, "CentroServizio") = CentroServizio Or (MoveFromDbWC(MyRs, "CentroServizio") = CentroServizio And MoveFromDbWC(MyRsSrc, "CentroServizio") = "")) Then
                        Dim AppoggioImporto As Double = MyRsSrc.Fields("Importo").Value
                        If ScorporoIVA = True Then
                            Dim CodIVA As New Cls_IVA

                            CodIVA.Codice = MyRsSrc.Fields("CODICEIVA").Value
                            CodIVA.Leggi(ConnessioneTabelle, CodIVA.Codice)

                            AppoggioImporto = AppoggioImporto + Modulo.MathRound(AppoggioImporto * CodIVA.Aliquota, 2)
                        End If

                        If MoveFromDbWC(MyRsSrc, "DareAvere") = "A" Then
                            ImportoDocumentoOspite = ImportoDocumentoOspite + AppoggioImporto
                        Else
                            ImportoDocumentoOspite = ImportoDocumentoOspite - AppoggioImporto
                        End If
                    End If
                    MyRsSrc.MoveNext()
                Loop
                MyRsSrc.Close()
            End If
            MyRs.MoveNext()
        Loop
        MyRs.Close()
    End Function


    Private Function ImportoDocumentoOspiteExtra(ByVal Mastro As Integer, ByVal Conto As Long, ByVal SottoConto As Double, ByVal CentroServizio As String, ByVal Anno As Integer, ByVal Mese As Byte, ByVal Tipologia As String, ByVal AnnoRiferimento As Integer, ByVal MeseRiferimento As Byte, ByVal CodiceOspite As Long, ByVal TipoExtra As String, Optional ByVal ScorporoIVA As Boolean = False, Optional ByVal ConnessioneTabelle As String = "") As Double
        Dim MyRs As New ADODB.Recordset
        Dim MyRsSrc As New ADODB.Recordset
        Dim GeneraleDB As New ADODB.Connection
        Dim MySql As String


        '" And MovimentiContabiliTesta.CentroServizio = '" & CentroServizio & "'" & _

        GeneraleDB.Open(ConnessioneGenerale)
        MySql = "SELECT MovimentiContabiliTesta.FatturaDiAnticipo,NumeroRegistrazione " & _
                " FROM MovimentiContabiliRiga INNER JOIN MovimentiContabiliTesta " & _
                " ON MovimentiContabiliRiga.Numero = MovimentiContabiliTesta.NumeroRegistrazione " & _
                " WHERE MovimentiContabiliRiga.MastroPartita = " & Mastro & _
                " AND MovimentiContabiliRiga.ContoPartita = " & Conto & _
                " AND MovimentiContabiliRiga.SottocontoPartita = " & SottoConto & _
                " AND (Select count(*) From MovimentiContabiliRiga as Riga Where Riga.AnnoRiferimento = " & AnnoRiferimento & _
                " AND Riga.MeseRiferimento = " & MeseRiferimento & " And Riga.Numero = MovimentiContabiliTesta.NumeroRegistrazione) > 0"
        If Trim(Tipologia) <> "" Then
            MySql = MySql & " AND MovimentiContabiliTesta.Tipologia = '" & Tipologia & "'"
        End If
        If CodiceOspite > 0 Then
            MySql = MySql & " AND MovimentiContabiliTesta.CodiceOspite = " & CodiceOspite
        End If
        ImportoDocumentoOspiteExtra = 0
        MyRs.Open(MySql, GeneraleDB, ADODB.CursorTypeEnum.adOpenKeyset, ADODB.LockTypeEnum.adLockOptimistic)
        Do While Not MyRs.EOF
            If IsDBNull(MyRs.Fields("FatturaDiAnticipo")) Or IsNothing(MyRs.Fields("FatturaDiAnticipo")) Or MoveFromDbWC(MyRs, "FatturaDiAnticipo") = "" Then
                MyRsSrc.Open("Select * From MovimentiContabiliRiga Where Numero = " & MyRs.Fields("NumeroRegistrazione").Value & " And RigaDaCausale <> 13 And AnnoRiferimento =  " & AnnoRiferimento & " And MeseRiferimento = " & MeseRiferimento, GeneraleDB, ADODB.CursorTypeEnum.adOpenKeyset, ADODB.LockTypeEnum.adLockOptimistic)
                Do While Not MyRsSrc.EOF
                    If (Mid(MoveFromDbWC(MyRsSrc, "TipoExtra") & Space(10), 1, 1) = "E") And (MoveFromDbWC(MyRsSrc, "CentroServizio") = CentroServizio Or (MoveFromDbWC(MyRs, "CentroServizio") = CentroServizio And MoveFromDbWC(MyRsSrc, "CentroServizio") = "")) Then
                        Dim AppoggioImporto As Double = MyRsSrc.Fields("Importo").Value
                        If ScorporoIVA = True Then
                            Dim CodIVA As New Cls_IVA

                            CodIVA.Codice = MyRsSrc.Fields("CODICEIVA").Value
                            CodIVA.Leggi(ConnessioneTabelle, CodIVA.Codice)

                            AppoggioImporto = AppoggioImporto + Modulo.MathRound(AppoggioImporto * CodIVA.Aliquota, 2)
                        End If

                        If MoveFromDbWC(MyRsSrc, "DareAvere") = "A" Then
                            ImportoDocumentoOspiteExtra = ImportoDocumentoOspiteExtra + AppoggioImporto
                        Else
                            ImportoDocumentoOspiteExtra = ImportoDocumentoOspiteExtra - AppoggioImporto
                        End If
                    End If
                    MyRsSrc.MoveNext()
                Loop
                MyRsSrc.Close()
            End If
            MyRs.MoveNext()
        Loop
        MyRs.Close()
    End Function




    Public Sub CalcolaRettaDaTabella(ByVal Cserv As String, ByVal CodOsp As Long, ByVal Anno As Long, ByVal Mese As Long, ByRef SC1 As Object)
        Dim SalvaParente2(10) As Single, SalvaParente(10) As Single, SalvaPerc(10) As Single
        Dim TReg As Integer, TCom As Integer, I As Integer, Ext As Integer, XS As Integer
        Dim AutoNonAuto As String, SalvaCodiceComune As String, SalvaCodiceRegione As String, SalvaMod As String = "", SalvaTipoImportoRegione = ""

        Dim SalvaCodiceJolly As String


        Dim SalvaComune As Single, SalvaRegione As Single, SalvaOspite As Single, SalvaOspite2 As Single, SalvaEnte As Single, SalvaRetta As Single
        Dim SalvaJolly As Single

        Dim XSalvaComune As Single, XSalvaRegione As Single, XSalvaOspite As Single, XSalvaOspite2 As Single, XSalvaRetta As Single

        Dim XSalvaJolly As Single


        Dim XSalvaParente(10) As Single, XSalvaPerc(10) As Single
        Dim XSalvaParente2(10) As Single

        Dim XSalvaExtImporto1 As Double, XSalvaExtImporto2 As Double, XSalvaExtImporto3 As Double, XSalvaExtImporto4 As Double
        Dim XSalvaExtImporto1C As Double, XSalvaExtImporto2C As Double, XSalvaExtImporto3C As Double, XSalvaExtImporto4C As Double

        Dim XSalvaExtImporto1J As Double, XSalvaExtImporto2J As Double, XSalvaExtImporto3J As Double, XSalvaExtImporto4J As Double

        Dim PXSalvaExtImporto1(10) As Double, PXSalvaExtImporto2(10) As Double, PXSalvaExtImporto3(10) As Double, PXSalvaExtImporto4(10) As Double
        Dim Par As Integer, ImpPERC As Single

        Dim WParente As Single, WSaveParente As Single, WPerc As Single
        

        Dim SommoGiorni As Boolean

        Dim Codice As String, Regola As String, TipoMov As String = ""
        Dim VARIABILE1 As Double, Variabile2 As Double, Variabile3 As Double, Variabile4 As Double
        Dim VARIABILEA As Double, VariabileB As Double, VariabileC As Double, VariabileD As Double
        Dim WSalvaComune As Double

        Dim WSalvaJolly As Double

        Dim WSalvaRegione As Double, WSalvaOspite As Double, WSalvaOspite2 As Double, WSalvaEnte As Double, WSalvaRetta As Double, MyCausaleFlag As Integer


        Dim WTipoRegione As String = ""


        Dim SCSalvaComune As Double, SCSalvaRegione As Double, SCSalvaOspite As Double, SCSalvaOspite2 As Double, SCSalvaEnte As Double

        Dim SCSalvaJolly As Double

        Dim SCSalvaParente(10) As Double, SCSalvaParente2(10) As Double, SCSalvaImpExtrParente(10) As Double, SCSalvaImpExtrOspite(10) As Double, SCSalvaImpExtrComune(10) As Double, SCSalvaPerc(10) As Double

        Dim SCSalvaImpExtrJolly(10) As Double

        Dim CheckT As String = ""

        Dim TJol As Long

        Dim xMese As Integer
        Dim xAnno As Integer

        Dim GiornoCalcolati As Integer



        '
        '   Azzero Variabili Per totale Importo
        '


        Dim OspiteAnagrafico As New ClsOspite

        OspiteAnagrafico.CodiceOspite = CodOsp
        OspiteAnagrafico.Leggi(STRINGACONNESSIONEDB, CodOsp)




        Dim cmdMovimentiAccolto As New OleDbCommand()
        cmdMovimentiAccolto.CommandText = "Select * From Movimenti Where TIPOMOV = '05' and CODICEOSPITE = " & CodOsp & " And CENTROSERVIZIO = '" & Cserv & "' And month(DATA) = " & Mese & " And year(Data) = " & Anno
        cmdMovimentiAccolto.Connection = OspitiCon
        Dim RDMovimentiAccolto As OleDbDataReader = cmdMovimentiAccolto.ExecuteReader()
        If RDMovimentiAccolto.Read() Then
            Accolto = True
        Else
            Accolto = False
        End If
        RDMovimentiAccolto.Close()



        Dim cmdMovimentiDimesso As New OleDbCommand()
        cmdMovimentiDimesso.CommandText = "Select * From Movimenti Where TIPOMOV = '13' and CODICEOSPITE = " & CodOsp & " And CENTROSERVIZIO = '" & Cserv & "' And month(DATA) = " & Mese & " And year(Data) = " & Anno
        cmdMovimentiDimesso.Connection = OspitiCon
        Dim RDMovimentiDimesso As OleDbDataReader = cmdMovimentiDimesso.ExecuteReader()
        If RDMovimentiDimesso.Read() Then
            Dimesso = True
        Else
            Dimesso = False
        End If
        RDMovimentiAccolto.Close()





        MyCausaleFlag = 0
        GiorniPres = 0 : GiorniAss = 0

        GiorniPresEnte = 0 : GiorniAssEnte = 0
        NonGiorniPres = 0 : NonGiorniAss = 0
        NonGiorniPresEnte = 0 : NonGiorniAssEnte = 0
        VARIABILE1 = 0
        Variabile2 = 0
        Variabile3 = 0
        Variabile4 = 0

        VARIABILEA = 0
        VariabileB = 0
        VariabileC = 0
        VariabileD = 0
        For I = 0 To 10
            GiorniPresExtraC(I) = 0
            ImportoPresComune(I) = 0 : ImportoAssComune(I) = 0
            ImportoPresJolly(I) = 0 : ImportoAssJolly(I) = 0

            ImportoPresRegione(I) = 0 : ImportoAssRegione(I) = 0

            NonImportoPresComune(I) = 0 : NonImportoAssComune(I) = 0

            NonImportoPresJolly(I) = 0 : NonImportoAssJolly(I) = 0

            NonImportoPresRegione(I) = 0 : NonImportoAssRegione(I) = 0
            GiorniPresParente(I) = 0 : GiorniAssParente(I) = 0

            GiorniPresComune(I) = 0 : GiorniAssComune(I) = 0

            GiorniPresJolly(I) = 0 : GiorniAssJolly(I) = 0

            GiorniPresRegione(I) = 0 : GiorniAssRegione(I) = 0
            NonGiorniPresParente(I) = 0 : NonGiorniAssParente(I) = 0

            NonGiorniPresComune(I) = 0 : NonGiorniAssComune(I) = 0

            NonGiorniPresJolly(I) = 0 : NonGiorniAssJolly(I) = 0

            NonGiorniPresRegione(I) = 0 : NonGiorniAssRegione(I) = 0
            For Ext = 0 To 10
                NonImpExtraOspite(Ext) = 0 : ImpExtraOspite(Ext) = 0
                NonImpExtraParente(I, Ext) = 0 : ImpExtraParente(I, Ext) = 0
                NonImpExtraComune(I, Ext) = 0 : ImpExtraComune(I, Ext) = 0

                NonImpExtraJolly(I, Ext) = 0 : ImpExtraJolly(I, Ext) = 0
            Next Ext

            PXSalvaExtImporto1(I) = 0 : PXSalvaExtImporto2(I) = 0
            PXSalvaExtImporto3(I) = 0 : PXSalvaExtImporto4(I) = 0

            ImportoPresParente(I) = 0 : ImportoAssParente(I) = 0
            NonImportoPresParente(I) = 0 : NonImportoAssParente(I) = 0
            SalvaImpExtrOspite(I) = 0
            SalvaImpExtrParent(I) = 0

            SalvaImpExtrComune(I) = 0

            SalvaImpExtrJolly(I) = 0

            SalvaParente(I) = 0
            SalvaParente2(I) = 0
        Next I

        ImportoPresEnte = 0 : ImportoAssEnte = 0
        NonImportoPresEnte = 0 : NonImportoAssEnte = 0
        ImportoPresOspite = 0 : ImportoAssOspite = 0
        NonImportoPresOspite = 0 : NonImportoAssOspite = 0

        ImportoPresOspite2 = 0 : ImportoAssOspite2 = 0
        NonImportoPresOspite2 = 0 : NonImportoAssOspite2 = 0

        '
        '   Azzero Variabili Per Importo
        '
        AutoNonAuto = ""
        SalvaComune = 0
        SalvaJolly = 0
        SalvaCodiceComune = ""
        SalvaCodiceJolly = ""
        SalvaCodiceRegione = ""
        SalvaRegione = 0
        SalvaOspite = 0
        SalvaOspite2 = 0
        SalvaEnte = 0
        SalvaRetta = 0
        XSalvaExtImporto1 = 0 : XSalvaExtImporto2 = 0 : XSalvaExtImporto3 = 0 : XSalvaExtImporto4 = 0

        Codice = ""
        Regola = ""
        SommoGiorni = True
        For I = 1 To 31

            If MyTabAutoSufficente(I) <> "" And MyTabAutoSufficente(I) <> " " Then
                AutoNonAuto = MyTabAutoSufficente(I)
            End If

            For XS = 0 To 20
                If MyTabImpExtrOspite(I, XS) <> -1 Then
                    SalvaImpExtrOspite(XS) = MyTabImpExtrOspite(I, XS)
                End If
                If MyTabImpExtrParent(I, XS) <> -1 Then
                    SalvaImpExtrParent(XS) = MyTabImpExtrParent(I, XS)
                End If
                If MyTabImpExtrComune(I, XS) <> -1 Then
                    SalvaImpExtrComune(XS) = MyTabImpExtrComune(I, XS)
                End If

                If MyTabImpExtrJolly(I, XS) <> -1 Then
                    SalvaImpExtrJolly(XS) = MyTabImpExtrJolly(I, XS)
                End If
            Next XS

            If MyTabAutoSufficente(I) <> "" Then
                AutoNonAuto = MyTabAutoSufficente(I)
            End If

            If MyTabImportoRegione(I) <> -1 Then
                XSalvaRegione = MyTabImportoRegione(I)
                If MyTabCodiceRegione(I) <> "****" Then
                    SalvaCodiceRegione = MyTabCodiceRegione(I)
                    SalvaTipoImportoRegione = MyTabRegioneTipoImporto(I)
                    InserisciRegione(SalvaCodiceRegione, SalvaTipoImportoRegione)
                End If
            End If

            If MyTabImportoJolly(I) <> -1 Then
                XSalvaJolly = MyTabImportoJolly(I)

                XSalvaExtImporto1J = MyTabGiornalieroExtraImporto1J(I)
                XSalvaExtImporto2J = MyTabGiornalieroExtraImporto2J(I)
                XSalvaExtImporto3J = MyTabGiornalieroExtraImporto3J(I)
                XSalvaExtImporto4J = MyTabGiornalieroExtraImporto4J(I)

                SalvaCodiceJolly = MyTabCodiceProvJolly(I) & MyTabCodiceJolly(I)
                InserisciJolly(SalvaCodiceJolly)
            End If


            If MyTabImportoComune(I) <> -1 Then
                XSalvaComune = MyTabImportoComune(I)

                XSalvaExtImporto1C = MyTabGiornalieroExtraImporto1C(I)
                XSalvaExtImporto2C = MyTabGiornalieroExtraImporto2C(I)
                XSalvaExtImporto3C = MyTabGiornalieroExtraImporto3C(I)
                XSalvaExtImporto4C = MyTabGiornalieroExtraImporto4C(I)

                SalvaCodiceComune = MyTabCodiceProv(I) & MyTabCodiceComune(I)
                InserisciComune(SalvaCodiceComune)
            End If


            If MyTabImportoOspite(I) <> -1 Then
                XSalvaOspite = MyTabImportoOspite(I)
                XSalvaOspite2 = MyTabImportoOspite2(I)

                XSalvaExtImporto1 = MyTabGiornalieroExtraImporto1(I)
                XSalvaExtImporto2 = MyTabGiornalieroExtraImporto2(I)
                XSalvaExtImporto3 = MyTabGiornalieroExtraImporto3(I)
                XSalvaExtImporto4 = MyTabGiornalieroExtraImporto4(I)
            End If


            For Par = 0 To MaxParenti
                If MyTabPercentuale(I, Par) <> -1 Then
                    XSalvaParente(Par) = MyTabImportoParente(I, Par)
                    XSalvaParente2(Par) = MyTabImportoParente2(I, Par)
                    XSalvaPerc(Par) = MyTabPercentuale(I, Par)

                    PXSalvaExtImporto1(Par) = MyTabGiornalieroExtraImporto1P(I, Par)
                    PXSalvaExtImporto2(Par) = MyTabGiornalieroExtraImporto2P(I, Par)
                    PXSalvaExtImporto3(Par) = MyTabGiornalieroExtraImporto3P(I, Par)
                    PXSalvaExtImporto4(Par) = MyTabGiornalieroExtraImporto4P(I, Par)
                End If
            Next Par
            If MyTabImportoRetta(I) <> -1 Then
                XSalvaRetta = MyTabImportoRetta(I)
            End If
            If MyTabModalita(I) <> "" Then
                SalvaMod = MyTabModalita(I)
            End If


            If SalvaMod = "O" Then
                WParente = 0
                For Par = 0 To MaxParenti
                    WParente = WParente + XSalvaParente(Par)
                    WParente = WParente + XSalvaParente2(Par)
                    SalvaParente(Par) = XSalvaParente(Par)
                    SalvaParente2(Par) = XSalvaParente2(Par)
                Next Par
                SalvaOspite = XSalvaRetta - XSalvaComune - XSalvaRegione - WParente - XSalvaJolly - XSalvaOspite2
                If Prm_AzzeraSeNegativo = 1 Then
                    If SalvaOspite < 0 Then SalvaOspite = 0
                End If
                SalvaRetta = XSalvaRetta
                SalvaComune = XSalvaComune

                SalvaJolly = XSalvaJolly
                SalvaOspite2 = XSalvaOspite2
                SalvaRegione = XSalvaRegione
            End If
            If SalvaMod = "P" Then
                WParente = 0
                For Par = 0 To MaxParenti
                    WParente = WParente + XSalvaParente2(Par)
                Next Par
                WParente = XSalvaRetta - XSalvaOspite - XSalvaRegione - XSalvaComune - XSalvaJolly - XSalvaOspite2 - WParente
                WSaveParente = WParente
                WPerc = 0
                For Par = 0 To MaxParenti
                    WPerc = WPerc + XSalvaPerc(Par)
                    If WPerc = 100 Then
                        SalvaParente(Par) = WParente
                        WParente = 0
                    Else
                        SalvaParente(Par) = Format(Modulo.MathRound(WSaveParente * XSalvaPerc(Par) / 100, 2), "#,##0.00")
                        WParente = WParente - SalvaParente(Par)
                    End If
                    If Prm_AzzeraSeNegativo = 1 Then
                        If SalvaParente(Par) < 0 Then SalvaParente(Par) = 0
                    End If
                    SalvaParente2(Par) = XSalvaParente2(Par)
                Next Par
                If WPerc = 0 Then
                    StringaDegliErrori = StringaDegliErrori & "Percentuale non indicata su parente a differenza " & CodOsp & vbNewLine
                End If
                SalvaRetta = XSalvaRetta
                SalvaComune = XSalvaComune
                SalvaJolly = XSalvaJolly
                SalvaRegione = XSalvaRegione
                SalvaOspite = XSalvaOspite
                SalvaOspite2 = XSalvaOspite2
            End If
            If SalvaMod = "C" Then
                WParente = 0
                For Par = 0 To MaxParenti
                    WParente = WParente + XSalvaParente(Par) + XSalvaParente2(Par)
                    SalvaParente(Par) = XSalvaParente(Par)
                    SalvaParente2(Par) = XSalvaParente2(Par)
                Next Par

                SalvaComune = XSalvaRetta - XSalvaOspite - XSalvaRegione - WParente - XSalvaJolly - XSalvaOspite2

                If Prm_AzzeraSeNegativo = 1 Then
                    If SalvaComune < 0 Then SalvaComune = 0
                End If
                SalvaRetta = XSalvaRetta

                SalvaJolly = XSalvaJolly

                SalvaRegione = XSalvaRegione
                SalvaOspite = XSalvaOspite
                SalvaOspite2 = XSalvaOspite2
            End If
            If SalvaMod = "E" Then
                WParente = 0
                For Par = 0 To MaxParenti
                    WParente = WParente + XSalvaParente(Par) + XSalvaParente2(Par)
                    SalvaParente(Par) = XSalvaParente(Par)
                    SalvaParente2(Par) = XSalvaParente2(Par)
                Next Par
                SalvaEnte = Modulo.MathRound(XSalvaRetta - XSalvaOspite - XSalvaComune - XSalvaJolly - WParente - XSalvaRegione - XSalvaOspite2, 5)
                If Prm_AzzeraSeNegativo = 1 Then
                    If SalvaEnte < 0 Then SalvaEnte = 0
                End If
                SalvaRetta = XSalvaRetta
                SalvaComune = XSalvaComune

                SalvaJolly = XSalvaJolly

                SalvaRegione = XSalvaRegione
                SalvaOspite = XSalvaOspite
                SalvaOspite2 = XSalvaOspite2
            End If

            If MyTabCausale(I) = "" Then
                Codice = ""
                If GiornoCambioRettaOspite > I Or GiornoCambioRettaOspite = 0 Then
                    ImportoPresOspite = ImportoPresOspite + SalvaOspite
                    ImportoPresOspite2 = ImportoPresOspite2 + SalvaOspite2


                    For Ext = 0 To 20
                        ImpExtraOspite(Ext) = ImpExtraOspite(Ext) + SalvaImpExtrOspite(Ext)
                        If SalvaImpExtrOspite(Ext) > 0 Then
                            MyTabCodiceExtrOspiteGIORNI(Ext) = MyTabCodiceExtrOspiteGIORNI(Ext) + 1
                        End If

                    Next
                    If Modulo.MathRound(SalvaOspite, 2) <> 0 Or Modulo.MathRound(SalvaOspite2, 2) <> 0 Then GiorniPres = GiorniPres + 1
                End If


                For Par = 0 To MaxParenti
                    If GiornoCambioRettaParenti(Par) > I Or GiornoCambioRettaParenti(Par) = 0 Then

                        For Ext = 0 To 20
                            If SalvaImpExtrParent(Ext) <> 0 Then
                                ImpPERC = Format(Modulo.MathRound(SalvaImpExtrParent(Ext) * XSalvaPerc(Par) / 100, 2), "#,##0.00")
                                ImpExtraParente(Par, Ext) = ImpExtraParente(Par, Ext) + ImpPERC
                                If ImpPERC > 0 Then
                                    MyTabCodiceExtrParentGIORNI(Par, Ext) = MyTabCodiceExtrParentGIORNI(Par, Ext) + 1
                                End If
                            End If
                        Next Ext
                        ImportoPresParente(Par) = ImportoPresParente(Par) + SalvaParente(Par)
                        ImportoPresParente2(Par) = ImportoPresParente2(Par) + SalvaParente2(Par)

                        If SalvaParente(Par) > 0 Or SalvaParente2(Par) > 0 Then
                            GiorniPresParente(Par) = GiorniPresParente(Par) + 1
                        End If
                    End If
                Next Par


                If AutoNonAuto = "A" And MyTabCausale(I) <> "*" Then
                    TCom = TrovaComune(SalvaCodiceComune)
                    ImportoPresComune(TCom) = ImportoPresComune(TCom) + SalvaComune


                    TJol = TrovaJolly(SalvaCodiceJolly)
                    ImportoPresJolly(TJol) = ImportoPresJolly(TJol) + SalvaJolly


                    TReg = TrovaRegione(SalvaCodiceRegione, SalvaTipoImportoRegione)
                    ImportoPresRegione(TReg) = ImportoPresRegione(TReg) + SalvaRegione
                    TipoImportoPresRegione(TReg) = SalvaTipoImportoRegione


                    ImportoPresEnte = ImportoPresEnte + SalvaEnte

                    For Ext = 0 To 10

                        ImpExtraComune(TCom, Ext) = ImpExtraComune(TCom, Ext) + SalvaImpExtrComune(Ext)

                        If SalvaImpExtrComune(Ext) > 0 Then
                            MyTabCodiceExtrComuneGIORNI(Ext) = MyTabCodiceExtrComuneGIORNI(Ext) + 1
                        End If

                        ImpExtraJolly(TJol, Ext) = ImpExtraJolly(TJol, Ext) + SalvaImpExtrJolly(Ext)

                        If Modulo.MathRound(SalvaImpExtrComune(Ext), 2) > 0 Then GiorniPresExtraC(Ext) = GiorniPresExtraC(Ext) + 1
                    Next Ext

                    If SommoGiorni Then

                        If SalvaEnte <> 0 Then GiorniPresEnte = GiorniPresEnte + 1
                        If Modulo.MathRound(SalvaComune, 2) <> 0 Then GiorniPresComune(TCom) = GiorniPresComune(TCom) + 1

                        If Modulo.MathRound(SalvaJolly, 2) <> 0 Then GiorniPresJolly(TJol) = GiorniPresJolly(TJol) + 1

                        If Modulo.MathRound(SalvaRegione, 2) <> 0 Then GiorniPresRegione(TReg) = GiorniPresRegione(TReg) + 1
                    End If
                End If

                If GiornoCambioRettaOspite <= I And GiornoCambioRettaOspite <> 0 Then
                    NonImportoPresOspite = NonImportoPresOspite + SalvaOspite
                    NonImportoPresOspite2 = NonImportoPresOspite2 + SalvaOspite2
                    For Ext = 0 To 10
                        NonImpExtraOspite(Ext) = NonImpExtraOspite(Ext) + SalvaImpExtrOspite(Ext)
                        If SalvaImpExtrOspite(Ext) > 0 Then
                            MyTabCodiceNonExtrOspiteGIORNI(Ext) = MyTabCodiceNonExtrOspiteGIORNI(Ext) + 1
                        End If
                    Next

                    If Modulo.MathRound(SalvaOspite, 2) <> 0 Or Modulo.MathRound(SalvaOspite2, 2) <> 0 Then NonGiorniPres = NonGiorniPres + 1
                End If


                For Par = 0 To MaxParenti
                    If GiornoCambioRettaParenti(Par) <= I And GiornoCambioRettaParenti(Par) <> 0 Then
                        For Ext = 0 To 10
                            If SalvaImpExtrParent(Ext) <> 0 Then
                                ImpPERC = Format(Modulo.MathRound(SalvaImpExtrParent(Ext) * XSalvaPerc(Par) / 100, 4), "#,##0.0000")
                                NonImpExtraParente(Par, Ext) = NonImpExtraParente(Par, Ext) + ImpPERC
                                If ImpPERC > 0 Then
                                    MyTabCodiceExtrParentNonGIORNI(Par, Ext) = MyTabCodiceExtrParentNonGIORNI(Par, Ext) + 1
                                End If
                            End If
                        Next Ext
                        NonImportoPresParente(Par) = NonImportoPresParente(Par) + SalvaParente(Par)
                        NonImportoPresParente2(Par) = NonImportoPresParente2(Par) + SalvaParente2(Par)

                        If SalvaParente(Par) > 0 Or SalvaParente2(Par) > 0 Then
                            NonGiorniPresParente(Par) = NonGiorniPresParente(Par) + 1
                        End If
                    End If
                Next Par


                If AutoNonAuto = "N" And MyTabCausale(I) <> "*" Then
                    TCom = TrovaComune(SalvaCodiceComune)
                    NonImportoPresComune(TCom) = NonImportoPresComune(TCom) + SalvaComune

                    TJol = TrovaJolly(SalvaCodiceJolly)
                    NonImportoPresJolly(TJol) = NonImportoPresJolly(TJol) + SalvaJolly


                    TReg = TrovaRegione(SalvaCodiceRegione, SalvaTipoImportoRegione)
                    NonImportoPresRegione(TReg) = NonImportoPresRegione(TReg) + SalvaRegione
                    TipoImportoPresRegione(TReg) = SalvaTipoImportoRegione
                    NonImportoPresEnte = NonImportoPresEnte + SalvaEnte
                    For Ext = 0 To 10

                        NonImpExtraComune(TCom, Ext) = NonImpExtraComune(TCom, Ext) + SalvaImpExtrComune(Ext)

                        If SalvaImpExtrComune(Ext) > 0 Then
                            MyTabCodiceExtrComuneGIORNI(Ext) = MyTabCodiceExtrComuneGIORNI(Ext) + 1
                        End If

                        NonImpExtraJolly(TJol, Ext) = NonImpExtraJolly(TJol, Ext) + SalvaImpExtrJolly(Ext)

                        If Modulo.MathRound(SalvaImpExtrComune(Ext), 2) > 0 Then GiorniPresExtraC(Ext) = GiorniPresExtraC(Ext) + 1
                    Next Ext


                    If SommoGiorni Then

                        If SalvaEnte <> 0 Then NonGiorniPresEnte = NonGiorniPresEnte + 1
                        If Modulo.MathRound(SalvaComune, 2) <> 0 Then NonGiorniPresComune(TCom) = NonGiorniPresComune(TCom) + 1

                        If Modulo.MathRound(SalvaJolly, 2) <> 0 Then NonGiorniPresJolly(TJol) = NonGiorniPresJolly(TJol) + 1

                        If Modulo.MathRound(SalvaRegione, 2) <> 0 Then NonGiorniPresRegione(TReg) = NonGiorniPresRegione(TReg) + 1
                    End If
                End If
            End If



            If MyTabCausale(I) <> "" And MyTabCausale(I) <> "*" Then

                WSalvaComune = Val(SalvaComune)

                WSalvaJolly = Val(SalvaJolly)

                WSalvaRegione = Val(SalvaRegione)
                WSalvaOspite = Val(SalvaOspite)
                WSalvaOspite2 = Val(SalvaOspite2)
                WSalvaEnte = Val(SalvaEnte)
                WSalvaRetta = Val(SalvaRetta)

                WTipoRegione = SalvaTipoImportoRegione


                If MyTabCausale(I) <> Codice Then
                    MyCausaleFlag = 0


                    Dim NIDCausale As Integer = NumeroDaCodice(MyTabCausale(I))
                    Codice = MyTabCausale(I)

                    Regola = Vt_CausaleRegola(NIDCausale)
                    CheckT = Vt_CausaleCheck(NIDCausale)
                    If MyTabGiornoAssenza(I) < 2 Then
                        TipoMov = Vt_GiornoUscita(NIDCausale)
                    Else
                        TipoMov = Vt_CausaleTimpoMovimento(NIDCausale)
                    End If
                Else
                    MyCausaleFlag = 1


                    Dim NIDCausale As Integer = NumeroDaCodice(MyTabCausale(I))

                    TipoMov = Vt_CausaleTimpoMovimento(NIDCausale)

                End If

                If Trim(Regola) = "" Then
                    SCSalvaComune = SalvaComune

                    SCSalvaJolly = SalvaJolly

                    SCSalvaRegione = SalvaRegione
                    SCSalvaOspite = SalvaOspite
                    SCSalvaOspite2 = SalvaOspite2
                    SCSalvaEnte = SalvaEnte

                    For Ext = 0 To 10
                        SCSalvaParente(Ext) = SalvaParente(Ext)
                        SCSalvaImpExtrOspite(Ext) = SalvaImpExtrOspite(Ext)
                        SCSalvaImpExtrComune(Ext) = SalvaImpExtrComune(Ext)
                        SCSalvaImpExtrParente(Ext) = SalvaImpExtrParent(Ext)
                        SCSalvaImpExtrJolly(Ext) = SalvaImpExtrJolly(Ext)

                        SCSalvaPerc(Ext) = XSalvaPerc(Ext)
                    Next
                End If

                
                If Trim(Regola) <> "" Then


                    SC1.Reset()



                    If Regola.ToUpper.IndexOf("ModificaTipoRegione".ToUpper) > 0 Then
                        SC1.ExecuteStatement("ModificaTipoRegione = " & Chr(34) & SalvaTipoImportoRegione & Chr(34))
                    End If



                    If InStr(1, CheckT, "A") > 0 Then
                        SC1.ExecuteStatement("DIM SalvaImpExtrOspite(20)")
                        SC1.ExecuteStatement("DIM CodiceExtrOspite(20)")

                        SC1.ExecuteStatement("DIM SalvaImpExtrComune(20)")
                        SC1.ExecuteStatement("DIM CodiceExtrComune(20)")

                        SC1.ExecuteStatement("DIM SalvaImpExtrJolly(20)")

                        SC1.ExecuteStatement("DIM SalvaImpExtrParent(20)")
                        SC1.ExecuteStatement("DIM CodiceExtrParent(20)")
                    End If

                    If InStr(1, CheckT, "B") > 0 Then
                        SC1.ExecuteStatement("DIM SalvaParente (20)")
                        SC1.ExecuteStatement("DIM SalvaParente2(20)")
                        SC1.ExecuteStatement("DIM SalvaPerc(20)")
                    End If


                    If InStr(1, CheckT, "C") > 0 Then
                        SC1.ExecuteStatement("DIM GiorniPresParente(10)")
                        SC1.ExecuteStatement("DIM GiorniAssParente (10)")

                        SC1.ExecuteStatement("DIM GiorniPresComune (10)")
                        SC1.ExecuteStatement("DIM GiorniAssComune (10)")

                        SC1.ExecuteStatement("DIM GiorniPresJolly (10)")
                        SC1.ExecuteStatement("DIM GiorniAssJolly (10)")

                        SC1.ExecuteStatement("DIM GiorniPresRegione (10)")
                        SC1.ExecuteStatement("DIM GiorniAssRegione (10)")
                    End If


                    If InStr(1, CheckT, "D") > 0 Then
                        SC1.ExecuteStatement("DIM NonGiorniPresParente (10)")
                        SC1.ExecuteStatement("DIM NonGiorniAssParente (10)")

                        SC1.ExecuteStatement("DIM NonGiorniPresComune (10)")
                        SC1.ExecuteStatement("DIM NonGiorniAssComune (10)")

                        SC1.ExecuteStatement("DIM NonGiorniPresJolly (10)")
                        SC1.ExecuteStatement("DIM NonGiorniAssJolly (10)")


                        SC1.ExecuteStatement("DIM NonGiorniPresRegione (10)")
                        SC1.ExecuteStatement("DIM NonGiorniAssRegione (10)")
                    End If


                    If InStr(1, CheckT, "E") > 0 Then
                        SC1.ExecuteStatement("DIM CodiceComune(10)")

                        SC1.ExecuteStatement("DIM CodiceJolly(10)")

                        SC1.ExecuteStatement("DIM CodiceRegione(10)")
                    End If

                    If InStr(1, CheckT, "F") > 0 Then
                        SC1.ExecuteStatement("DIM pXSalvaExtImporto1(10)")
                        SC1.ExecuteStatement("DIM pXSalvaExtImporto2(10)")
                        SC1.ExecuteStatement("DIM pXSalvaExtImporto3(10)")
                        SC1.ExecuteStatement("DIM pXSalvaExtImporto4(10)")

                        SC1.ExecuteStatement("DIM ImpExtrParMan1(10)")
                        SC1.ExecuteStatement("DIM ImpExtrParMan2(10)")
                        SC1.ExecuteStatement("DIM ImpExtrParMan3(10)")
                        SC1.ExecuteStatement("DIM ImpExtrParMan4(10)")

                        SC1.ExecuteStatement("DIM NumExtrParMan1(10)")
                        SC1.ExecuteStatement("DIM NumExtrParMan2(10)")
                        SC1.ExecuteStatement("DIM NumExtrParMan3(10)")
                        SC1.ExecuteStatement("DIM NumExtrParMan4(10)")

                        SC1.ExecuteStatement("DIM TipoExtrParMan1(10)")
                        SC1.ExecuteStatement("DIM TipoExtrParMan2(10)")
                        SC1.ExecuteStatement("DIM TipoExtrParMan3(10)")
                        SC1.ExecuteStatement("DIM TipoExtrParMan4(10)")


                    End If

                    If InStr(1, CheckT, "M") > 0 Then
                        SC1.ExecuteStatement("DIM TipoExtrComMan1(10)")
                        SC1.ExecuteStatement("DIM TipoExtrComMan2(10)")
                        SC1.ExecuteStatement("DIM TipoExtrComMan3(10)")
                        SC1.ExecuteStatement("DIM TipoExtrComMan4(10)")
                    End If


                    If Regola <> "" Then
                        If Regola.IndexOf("ISE") > 0 Then
                            Dim DatiIse As New Cls_Ise


                            DatiIse.CODICEOSPITE = CodOsp
                            DatiIse.CENTROSERVIZIO = Cserv
                            DatiIse.IseeAdata(STRINGACONNESSIONEDB, DatiIse.CODICEOSPITE, DatiIse.CENTROSERVIZIO, DateSerial(Anno, Mese, I))

                            If DatiIse.ISE = "" Then
                                DatiIse.ISE = "0"
                            End If
                            If DatiIse.ISEE = "" Then
                                DatiIse.ISEE = "0"
                            End If
                            SC1.ExecuteStatement("ISE = " & Replace(CDbl(DatiIse.ISE), ",", "."))

                            SC1.ExecuteStatement("ISEE = " & Replace(CDbl(DatiIse.ISEE), ",", "."))
                         

                        End If
                    End If

                    If Regola <> "" Then
                        If Regola.IndexOf("ACCOMPAGNAMENTO") > 0 Then
                            Dim DatiAccog As New Cls_Accompagnamento


                            DatiAccog.CODICEOSPITE = CodOsp
                            DatiAccog.CENTROSERVIZIO = Cserv
                            DatiAccog.LeggiAData(STRINGACONNESSIONEDB, DateSerial(Anno, Mese, GiorniMese(Mese, Anno)))

                            SC1.ExecuteStatement("ACCOMPAGNAMENTO = " & DatiAccog.Accompagnamento)
                        End If
                    End If

                    Try
                        SC1.AddCode(Regola)
                    Catch ex As Exception

                    End Try


                    If Trim(AutoNonAuto) <> "" Then SC1.ExecuteStatement("Stato = " & Chr(34) & AutoNonAuto & Chr(34))

                    SC1.ExecuteStatement("GiornoElab = #" & Replace(Format(DateSerial(Anno, Mese, I), "MM/dd/yyyy"), ".", "/") & "#")



                    SC1.ExecuteStatement("GiornoSettimana = " & DateSerial(Anno, Mese, I).DayOfWeek)



                    If InStr(1, CheckT, "G") > 0 Then
                        TCom = TrovaComune(SalvaCodiceComune)
                        SC1.ExecuteStatement("NumeroComune  =" & TCom)

                        TJol = TrovaJolly(SalvaCodiceJolly)
                        SC1.ExecuteStatement("NumeroJolly  =" & TJol)

                        TReg = TrovaRegione(SalvaCodiceRegione, SalvaTipoImportoRegione)
                        SC1.ExecuteStatement("NumeroRegione = " & TReg)
                    End If


                    If InStr(1, CheckT, "H") > 0 Then
                        SC1.ExecuteStatement("SalvaComune = " & Replace(SalvaComune, ",", "."))

                        SC1.ExecuteStatement("SalvaJolly = " & Replace(SalvaJolly, ",", "."))

                        SC1.ExecuteStatement("SalvaRegione = " & Replace(SalvaRegione, ",", "."))
                        SC1.ExecuteStatement("SalvaOspite = " & Replace(SalvaOspite, ",", "."))
                        SC1.ExecuteStatement("SalvaOspite2 = " & Replace(SalvaOspite2, ",", "."))
                        SC1.ExecuteStatement("SalvaEnte = " & Replace(SalvaEnte, ",", "."))
                    Else
                        SCSalvaComune = SalvaComune

                        SCSalvaJolly = SalvaJolly

                        SCSalvaRegione = SalvaRegione
                        SCSalvaOspite = SalvaOspite
                        SCSalvaOspite2 = SalvaOspite2
                        SCSalvaEnte = SalvaEnte
                    End If

                    If InStr(1, CheckT, "I") > 0 Then
                        SC1.ExecuteStatement("Variabile1 = " & Replace(VARIABILE1, ",", "."))
                        SC1.ExecuteStatement("Variabile2 = " & Replace(Variabile2, ",", "."))
                        SC1.ExecuteStatement("Variabile3 = " & Replace(Variabile3, ",", "."))
                        SC1.ExecuteStatement("Variabile4 = " & Replace(Variabile4, ",", "."))

                        SC1.ExecuteStatement("VariabileA = " & Replace(VARIABILEA, ",", "."))
                        SC1.ExecuteStatement("VariabileB = " & Replace(VariabileB, ",", "."))
                        SC1.ExecuteStatement("VariabileC = " & Replace(VariabileC, ",", "."))
                        SC1.ExecuteStatement("VariabileD = " & Replace(VariabileD, ",", "."))
                    End If
                    SC1.ExecuteStatement("GiorniAssenza = " & MyTabGiornoAssenza(I))
                    SC1.ExecuteStatement("GiorniRipetizioneCausale = " & MyTabGiorniRipetizioneCausale(I))
                    SC1.ExecuteStatement("GiorniRipetizioneCausaleNC = " & MyTabGiorniRipetizioneCausaleNC(I))
                    SC1.ExecuteStatement("MyTabGiorniRipetizioneCausaleACSum = " & MyTabGiorniRipetizioneCausaleACSum(I))


                    If InStr(1, CheckT, "L") > 0 Then
                        SC1.ExecuteStatement("XSalvaExtImporto1 = " & Replace(XSalvaExtImporto1, ",", "."))
                        SC1.ExecuteStatement("XSalvaExtImporto2 = " & Replace(XSalvaExtImporto2, ",", "."))
                        SC1.ExecuteStatement("XSalvaExtImporto3 = " & Replace(XSalvaExtImporto3, ",", "."))
                        SC1.ExecuteStatement("XSalvaExtImporto4 = " & Replace(XSalvaExtImporto4, ",", "."))

                        SC1.ExecuteStatement("ImpExtrOspMan1 = " & Replace(ImpExtrOspMan1, ",", "."))
                        SC1.ExecuteStatement("ImpExtrOspMan2 = " & Replace(ImpExtrOspMan2, ",", "."))
                        SC1.ExecuteStatement("ImpExtrOspMan3 = " & Replace(ImpExtrOspMan3, ",", "."))
                        SC1.ExecuteStatement("ImpExtrOspMan4 = " & Replace(ImpExtrOspMan4, ",", "."))

                        SC1.ExecuteStatement("NumExtrOspMan1 = " & Replace(NumExtrOspMan1, ",", "."))
                        SC1.ExecuteStatement("NumExtrOspMan2 = " & Replace(NumExtrOspMan2, ",", "."))
                        SC1.ExecuteStatement("NumExtrOspMan3 = " & Replace(NumExtrOspMan3, ",", "."))
                        SC1.ExecuteStatement("NumExtrOspMan4 = " & Replace(NumExtrOspMan4, ",", "."))

                        SC1.ExecuteStatement("TipoAddExtrOspMan1 = " & Chr(34) & TipoAddExtrOspMan1 & Chr(34))
                        SC1.ExecuteStatement("TipoAddExtrOspMan2 = " & Chr(34) & TipoAddExtrOspMan2 & Chr(34))
                        SC1.ExecuteStatement("TipoAddExtrOspMan3 = " & Chr(34) & TipoAddExtrOspMan3 & Chr(34))
                        SC1.ExecuteStatement("TipoAddExtrOspMan4 = " & Chr(34) & TipoAddExtrOspMan4 & Chr(34))



                    End If

                    If InStr(1, CheckT, "M") > 0 Then
                        SC1.ExecuteStatement("XSalvaExtImporto1C = " & Replace(XSalvaExtImporto1C, ",", "."))
                        SC1.ExecuteStatement("XSalvaExtImporto2C = " & Replace(XSalvaExtImporto2C, ",", "."))
                        SC1.ExecuteStatement("XSalvaExtImporto3C = " & Replace(XSalvaExtImporto3C, ",", "."))
                        SC1.ExecuteStatement("XSalvaExtImporto4C = " & Replace(XSalvaExtImporto4C, ",", "."))

                        SC1.ExecuteStatement("XSalvaExtImporto1j = " & Replace(XSalvaExtImporto1J, ",", "."))
                        SC1.ExecuteStatement("XSalvaExtImporto2j = " & Replace(XSalvaExtImporto2J, ",", "."))
                        SC1.ExecuteStatement("XSalvaExtImporto3j = " & Replace(XSalvaExtImporto3J, ",", "."))
                        SC1.ExecuteStatement("XSalvaExtImporto4j = " & Replace(XSalvaExtImporto4J, ",", "."))

                        SC1.ExecuteStatement("DIM ImpExtrComMan1(10)")
                        SC1.ExecuteStatement("DIM ImpExtrComMan2(10)")
                        SC1.ExecuteStatement("DIM ImpExtrComMan3(10)")
                        SC1.ExecuteStatement("DIM ImpExtrComMan4(10)")

                        SC1.ExecuteStatement("DIM NumExtrComMan1(10)")
                        SC1.ExecuteStatement("DIM NumExtrComMan2(10)")
                        SC1.ExecuteStatement("DIM NumExtrComMan3(10)")
                        SC1.ExecuteStatement("DIM NumExtrComMan4(10)")


                        For Par = 0 To 10
                            SC1.ExecuteStatement("ImpExtrComMan1(" & Par & ") = " & Replace(ImpExtrComMan1(Par), ",", "."))
                            SC1.ExecuteStatement("ImpExtrComMan2(" & Par & ") = " & Replace(ImpExtrComMan2(Par), ",", "."))
                            SC1.ExecuteStatement("ImpExtrComMan3(" & Par & ") = " & Replace(ImpExtrComMan3(Par), ",", "."))
                            SC1.ExecuteStatement("ImpExtrComMan4(" & Par & ") = " & Replace(ImpExtrComMan4(Par), ",", "."))

                            SC1.ExecuteStatement("NumExtrComMan1(" & Par & ") = " & Replace(NumExtrComMan1(Par), ",", "."))
                            SC1.ExecuteStatement("NumExtrComMan2(" & Par & ") = " & Replace(NumExtrComMan2(Par), ",", "."))
                            SC1.ExecuteStatement("NumExtrComMan3(" & Par & ") = " & Replace(NumExtrComMan3(Par), ",", "."))
                            SC1.ExecuteStatement("NumExtrComMan4(" & Par & ") = " & Replace(NumExtrComMan4(Par), ",", "."))

                            SC1.ExecuteStatement("TipoExtrComMan1(" & Par & ") = " & Chr(34) & TipoExtrComMan1(Par) & Chr(34))
                            SC1.ExecuteStatement("TipoExtrComMan2(" & Par & ") = " & Chr(34) & TipoExtrComMan2(Par) & Chr(34))
                            SC1.ExecuteStatement("TipoExtrComMan3(" & Par & ") = " & Chr(34) & TipoExtrComMan3(Par) & Chr(34))
                            SC1.ExecuteStatement("TipoExtrComMan4(" & Par & ") = " & Chr(34) & TipoExtrComMan4(Par) & Chr(34))
                        Next
                    End If


                    'If InStr(1, CheckT, "N") > 0 Then
                    SC1.ExecuteStatement("Mese = " & Mese)
                    SC1.ExecuteStatement("Anno = " & Anno)
                    SC1.ExecuteStatement("Cserv = " & Chr(34) & Cserv & Chr(34))

                    SC1.ExecuteStatement("TipoMov = " & Chr(34) & TipoMov & Chr(34))



                    'TipoMov
                    SC1.ExecuteStatement("CodiceOspite = " & CodOsp)
                    'End If

                    If InStr(1, CheckT, "O") > 0 Then
                        SC1.ExecuteStatement("GiorniPres = " & GiorniPres)
                        SC1.ExecuteStatement("GiorniAss = " & GiorniAss)
                        SC1.ExecuteStatement("GiorniPresEnte = " & GiorniPresEnte)
                        SC1.ExecuteStatement("GiorniAssEnte = " & GiorniAssEnte)
                    End If

                    If InStr(1, CheckT, "P") > 0 Then
                        SC1.ExecuteStatement("NonGiorniPres = " & NonGiorniPres)
                        SC1.ExecuteStatement("NonGiorniAss = " & NonGiorniAss)
                        SC1.ExecuteStatement("NonGiorniPresEnte = " & NonGiorniPresEnte)
                        SC1.ExecuteStatement("NonGiorniAssEnte = " & NonGiorniAssEnte)
                    End If
                    If InStr(1, CheckT, "-") > 0 Then
                        SC1.ExecuteStatement("ComuneResidenza = " & Chr(34) & OspiteAnagrafico.RESIDENZAPROVINCIA1 & OspiteAnagrafico.RESIDENZACOMUNE1 & Chr(34))

                    End If

                    If InStr(1, CheckT, "F") > 0 Then
                        For Par = 1 To 10
                            SC1.ExecuteStatement("pXSalvaExtImporto1(" & Par & ") = " & Replace(PXSalvaExtImporto1(Par), ",", "."))
                            SC1.ExecuteStatement("pXSalvaExtImporto2(" & Par & ") = " & Replace(PXSalvaExtImporto2(Par), ",", "."))
                            SC1.ExecuteStatement("pXSalvaExtImporto3(" & Par & ") = " & Replace(PXSalvaExtImporto3(Par), ",", "."))
                            SC1.ExecuteStatement("pXSalvaExtImporto4(" & Par & ") = " & Replace(PXSalvaExtImporto4(Par), ",", "."))

                            SC1.ExecuteStatement("ImpExtrParMan1(" & Par & ") = " & Replace(ImpExtrParMan1(Par), ",", "."))
                            SC1.ExecuteStatement("ImpExtrParMan2(" & Par & ") = " & Replace(ImpExtrParMan2(Par), ",", "."))
                            SC1.ExecuteStatement("ImpExtrParMan3(" & Par & ") = " & Replace(ImpExtrParMan3(Par), ",", "."))
                            SC1.ExecuteStatement("ImpExtrParMan4(" & Par & ") = " & Replace(ImpExtrParMan4(Par), ",", "."))

                            SC1.ExecuteStatement("NumExtrParMan1(" & Par & ") = " & Replace(NumExtrParMan1(Par), ",", "."))
                            SC1.ExecuteStatement("NumExtrParMan2(" & Par & ") = " & Replace(NumExtrParMan2(Par), ",", "."))
                            SC1.ExecuteStatement("NumExtrParMan3(" & Par & ") = " & Replace(NumExtrParMan3(Par), ",", "."))
                            SC1.ExecuteStatement("NumExtrParMan4(" & Par & ") = " & Replace(NumExtrParMan4(Par), ",", "."))

                            SC1.ExecuteStatement("TipoExtrParMan1(" & Par & ") = " & Chr(34) & TipoExtrParMan1(Par) & Chr(34))
                            SC1.ExecuteStatement("TipoExtrParMan2(" & Par & ") = " & Chr(34) & TipoExtrParMan2(Par) & Chr(34))
                            SC1.ExecuteStatement("TipoExtrParMan3(" & Par & ") = " & Chr(34) & TipoExtrParMan3(Par) & Chr(34))
                            SC1.ExecuteStatement("TipoExtrParMan4(" & Par & ") = " & Chr(34) & TipoExtrParMan4(Par) & Chr(34))
                        Next
                    End If


                    If InStr(1, CheckT, "E") > 0 Or InStr(1, CheckT, "C") > 0 Or InStr(1, CheckT, "D") > 0 Then
                        For Ext = 0 To 10
                            If InStr(1, CheckT, "E") > 0 Then
                                If CodiceComune(Ext) <> "" Then SC1.ExecuteStatement("CodiceComune(" & Ext & ") = " & Chr(34) & CodiceComune(Ext) & Chr(34))

                                If CodiceJolly(Ext) <> "" Then SC1.ExecuteStatement("CodiceJolly(" & Ext & ") = " & Chr(34) & CodiceJolly(Ext) & Chr(34))

                                If CodiceRegione(Ext) <> "" Then SC1.ExecuteStatement("CodiceRegione(" & Ext & ") = " & Chr(34) & CodiceRegione(Ext) & Chr(34))
                            End If
                            If InStr(1, CheckT, "C") > 0 Then
                                SC1.ExecuteStatement("GiorniPresParente(" & Ext & ") = " & GiorniPresParente(Ext))
                                SC1.ExecuteStatement("GiorniAssParente (" & Ext & ") = " & GiorniAssParente(Ext))

                                SC1.ExecuteStatement("GiorniPresComune (" & Ext & ") = " & GiorniPresComune(Ext))
                                SC1.ExecuteStatement("GiorniAssComune (" & Ext & ") = " & GiorniAssComune(Ext))

                                SC1.ExecuteStatement("GiorniPresJolly (" & Ext & ") = " & GiorniPresJolly(Ext))
                                SC1.ExecuteStatement("GiorniAssJolly (" & Ext & ") = " & GiorniAssJolly(Ext))

                                SC1.ExecuteStatement("GiorniPresRegione (" & Ext & ") = " & GiorniPresRegione(Ext))
                                SC1.ExecuteStatement("GiorniAssRegione (" & Ext & ") = " & GiorniAssRegione(Ext))
                            End If

                            If InStr(1, CheckT, "D") > 0 Then
                                SC1.ExecuteStatement("NonGiorniPresParente (" & Ext & ") = " & NonGiorniPresParente(Ext))
                                SC1.ExecuteStatement("NonGiorniAssParente (" & Ext & ") = " & NonGiorniAssParente(Ext))

                                SC1.ExecuteStatement("NonGiorniPresComune (" & Ext & ") = " & NonGiorniPresComune(Ext))
                                SC1.ExecuteStatement("NonGiorniAssComune (" & Ext & ") = " & NonGiorniAssComune(Ext))

                                SC1.ExecuteStatement("NonGiorniPresJolly (" & Ext & ") = " & NonGiorniPresJolly(Ext))
                                SC1.ExecuteStatement("NonGiorniAssJolly (" & Ext & ") = " & NonGiorniAssJolly(Ext))

                                SC1.ExecuteStatement("NonGiorniPresRegione (" & Ext & ") = " & NonGiorniPresRegione(Ext))
                                SC1.ExecuteStatement("NonGiorniAssRegione (" & Ext & ") = " & NonGiorniAssRegione(Ext))
                            End If
                        Next Ext
                    End If


                    If InStr(1, CheckT, "A") > 0 Or InStr(1, CheckT, "B") > 0 Then
                        For Ext = 0 To 10
                            If InStr(1, CheckT, "A") > 0 Then
                                SC1.ExecuteStatement("SalvaImpExtrOspite(" & Ext & ") = " & Replace(SalvaImpExtrOspite(Ext), ",", "."))
                                If Trim(MyTabCodiceExtrOspite(Ext)) <> "" Then SC1.ExecuteStatement("CodiceExtrOspite(" & Ext & ") = " & Chr(34) & MyTabCodiceExtrOspite(Ext) & Chr(34))

                                SC1.ExecuteStatement("SalvaImpExtrComune(" & Ext & ") = " & Replace(SalvaImpExtrComune(Ext), ",", "."))
                                If Trim(MyTabCodiceExtrComune(Ext)) <> "" Then SC1.ExecuteStatement("CodiceExtrComune(" & Ext & ") = " & Chr(34) & MyTabCodiceExtrComune(Ext) & Chr(34))

                                SC1.ExecuteStatement("SalvaImpExtrJolly(" & Ext & ") = " & Replace(SalvaImpExtrJolly(Ext), ",", "."))


                                If IsDBNull(SalvaImpExtrParent(Ext)) Or Val(SalvaImpExtrParent(Ext)) = 0 Then
                                    SC1.ExecuteStatement("SalvaImpExtrParent (" & Ext & ") = 0")
                                Else
                                    SC1.ExecuteStatement("SalvaImpExtrParent(" & Ext & ") = " & Replace(SalvaImpExtrParent(Ext), ",", "."))
                                End If
                                If Trim(MyTabCodiceExtrParent(Ext)) <> "" Then SC1.ExecuteStatement("CodiceExtrParent(" & Ext & ") = " & Chr(34) & MyTabCodiceExtrParent(Ext) & Chr(34))

                            Else
                                SCSalvaImpExtrOspite(Ext) = SalvaImpExtrOspite(Ext)
                                SCSalvaImpExtrParente(Ext) = SalvaImpExtrParent(Ext)
                                SCSalvaImpExtrComune(Ext) = SalvaImpExtrComune(Ext)

                                SCSalvaImpExtrJolly(Ext) = SalvaImpExtrJolly(Ext)

                            End If


                            If InStr(1, CheckT, "B") > 0 Then
                                SC1.ExecuteStatement("SalvaParente(" & Ext & ") = " & Replace(SalvaParente(Ext), ",", "."))
                                SC1.ExecuteStatement("SalvaParente2(" & Ext & ") = " & Replace(SalvaParente2(Ext), ",", "."))
                                SC1.ExecuteStatement("SalvaPerc(" & Ext & ") = " & Replace(XSalvaPerc(Ext), ",", "."))
                            Else
                                SCSalvaParente(Ext) = SalvaParente(Ext)
                                SCSalvaParente2(Ext) = SalvaParente2(Ext)
                                SCSalvaPerc(Ext) = XSalvaPerc(Ext)
                            End If
                        Next
                    Else
                        For Ext = 0 To 10
                            SCSalvaParente(Ext) = SalvaParente(Ext)
                            SCSalvaParente2(Ext) = SalvaParente2(Ext)
                            SCSalvaImpExtrOspite(Ext) = SalvaImpExtrOspite(Ext)
                            SCSalvaImpExtrComune(Ext) = SalvaImpExtrComune(Ext)
                            SCSalvaImpExtrParente(Ext) = SalvaImpExtrParent(Ext)
                            SCSalvaImpExtrJolly(Ext) = SalvaImpExtrJolly(Ext)

                            SCSalvaPerc(Ext) = XSalvaPerc(Ext)
                        Next
                    End If

                    If Regola.ToUpper.IndexOf("LISTINO") > 0 Then
                        Dim VD As New Cls_Listino

                        VD.CENTROSERVIZIO = Cserv
                        VD.CODICEOSPITE = CodOsp
                        VD.LeggiAData(STRINGACONNESSIONEDB, DateSerial(Anno, Mese, I))

                        SC1.ExecuteStatement("LISTINO = " & Chr(34) & VD.CodiceListino & Chr(34))
                    End If

                    If Regola.ToUpper.IndexOf("DATAACCOGLIMENTO") > 0 Then
                        Dim VD As New Cls_Movimenti

                        VD.CENTROSERVIZIO = Cserv
                        VD.CODICEOSPITE = CodOsp
                        VD.UltimaDataAccoglimentoSenzaCserv(STRINGACONNESSIONEDB)
                        SC1.ExecuteStatement("DATAACCOGLIMENTO = #" & Replace(Format(VD.Data, "MM/dd/yyyy"), ".", "/") & "#")
                    End If

                    If Regola.ToUpper.IndexOf("TIPORETTA") > 0 Then
                        Dim VD As New Cls_rettatotale

                        VD.CENTROSERVIZIO = Cserv
                        VD.CODICEOSPITE = CodOsp
                        VD.RettaTotaleAData(STRINGACONNESSIONEDB, CodOsp, Cserv, DateSerial(Anno, Mese, I))
                        SC1.ExecuteStatement("TipoRetta = " & Chr(34) & VD.TipoRetta & Chr(34))
                    End If

                    If Regola.ToUpper.IndexOf("TIPOIMPORTOREGIONE") > 0 Then
                        Dim VD As New Cls_StatoAuto

                        VD.CENTROSERVIZIO = Cserv
                        VD.CODICEOSPITE = CodOsp
                        VD.Data = DateSerial(Anno, Mese, I)
                        VD.StatoPrimaData(STRINGACONNESSIONEDB, CodOsp, Cserv)
                        SC1.ExecuteStatement("TIPOIMPORTOREGIONE = " & Chr(34) & VD.TipoRetta & Chr(34))
                    End If


                    If Regola.ToUpper.IndexOf("AssenzaNellAnno".ToUpper) > 0 Then

                        SC1.ExecuteStatement("AssenzaNellAnno = " & Chr(34) & GiorniAssenzaAnno(Cserv, CodOsp, Anno, Mese, I) & Chr(34))
                    End If


                    If Regola.ToUpper.IndexOf("AssenzaDiurnoNellAnno".ToUpper) > 0 Then

                        SC1.ExecuteStatement("AssenzaDiurnoNellAnno = " & Chr(34) & AssenzaDiurnoNellAnno(Cserv, CodOsp, Anno, Mese, I) & Chr(34))
                    End If

                    If Regola.ToUpper.IndexOf("AssenzaDiurnoNellAnnoEscludiAssenza".ToUpper) > 0 Then

                        SC1.ExecuteStatement("AssenzaDiurnoNellAnnoEscludiAssenza = " & Chr(34) & AssenzaDiurnoNellAnnoEscludiAssenza(Cserv, CodOsp, Anno, Mese, I) & Chr(34))
                    End If


                    If Regola.ToUpper.IndexOf("EtaUtente".ToUpper) > 0 Then
                        SC1.ExecuteStatement("EtaUTente = " & Int(DateDiff(DateInterval.Day, OspiteAnagrafico.DataNascita, DateSerial(Anno, Mese, I)) / 365))
                    End If


                    Dim VerCserv As New Cls_CentroServizio

                    VerCserv.Leggi(STRINGACONNESSIONEDB, Cserv)

                    If VerCserv.TIPOCENTROSERVIZIO = "A" Then

                        Dim Rs_AssenzeCentroDiurno As New ADODB.Recordset
                        Dim Sql As String
                        Dim AppoggioOre(100) As Double
                        Dim AppoggioOpertore(100) As String
                        Dim AppoggioTipo(100) As String

                        Dim Indice As Integer = 0
                        Dim xIndice As Integer = 0

                        SC1.ExecuteStatement("Dim DomOre(80)")
                        SC1.ExecuteStatement("Dim DomOpertore(80)")
                        SC1.ExecuteStatement("Dim DomTipo(80)")


                        Sql = "Select * From MovimentiDomiciliare Where CENTROSERVIZIO = '" & Cserv & "' And CODICEOSPITE = " & CodOsp & " And Data = ? "


                        Dim cmd As New OleDbCommand()
                        cmd.CommandText = Sql
                        cmd.Parameters.AddWithValue("@Data", DateSerial(Anno, Mese, I))
                        cmd.Connection = OspitiCon
                        Dim ReaderAssenzeCentroDiurno As OleDbDataReader = cmd.ExecuteReader()
                        Do While ReaderAssenzeCentroDiurno.Read
                            AppoggioOre(Indice) = DateDiff("n", campodbd(ReaderAssenzeCentroDiurno.Item("OraInizio")), campodbd(ReaderAssenzeCentroDiurno.Item("OraFine")))
                            AppoggioOpertore(Indice) = campodb(ReaderAssenzeCentroDiurno.Item("Operatore"))
                            AppoggioTipo(Indice) = campodb(ReaderAssenzeCentroDiurno.Item("Tipologia"))

                            Indice = Indice + 1
                        Loop
                        ReaderAssenzeCentroDiurno.Close()

                        For xIndice = 0 To Indice
                            SC1.ExecuteStatement("DomOre(" & xIndice & ") = " & AppoggioOre(xIndice))
                            SC1.ExecuteStatement("DomOpertore(" & xIndice & ") = " & Chr(34) & AppoggioOpertore(xIndice) & Chr(34))
                            SC1.ExecuteStatement("DomTipo(" & xIndice & ") = " & Chr(34) & AppoggioTipo(xIndice) & Chr(34))
                        Next

                    End If



                    Try
                        If Not IsDBNull(Regola) And Regola <> "" Then SC1.Run("Calcolo")
                    Catch ex As Exception
                        StringaDegliErrori = StringaDegliErrori & " Errore in Regola Causale " & MyTabCausale(I) & "-" & ex.Message
                    End Try

                    REM If Not IsDBNull(Regola) And Regola <> "" Then SC1.Run("Calcolo")

                    If InStr(1, CheckT, "I") > 0 Then
                        VARIABILE1 = SC1.Eval("Variabile1")
                        Variabile2 = SC1.Eval("Variabile2")
                        Variabile3 = SC1.Eval("Variabile3")
                        Variabile4 = SC1.Eval("Variabile4")

                        VARIABILEA = SC1.Eval("VariabileA")
                        VariabileB = SC1.Eval("VariabileB")
                        VariabileC = SC1.Eval("VariabileC")
                        VariabileD = SC1.Eval("VariabileD")
                    End If

                    If InStr(1, CheckT, "L") > 0 Then
                        ImpExtrOspMan1 = SC1.Eval("ImpExtrOspMan1")
                        ImpExtrOspMan2 = SC1.Eval("ImpExtrOspMan2")
                        ImpExtrOspMan3 = SC1.Eval("ImpExtrOspMan3")
                        ImpExtrOspMan4 = SC1.Eval("ImpExtrOspMan4")

                        NumExtrOspMan1 = SC1.Eval("NumExtrOspMan1")
                        NumExtrOspMan2 = SC1.Eval("NumExtrOspMan2")
                        NumExtrOspMan3 = SC1.Eval("NumExtrOspMan3")
                        NumExtrOspMan4 = SC1.Eval("NumExtrOspMan4")

                        TipoAddExtrOspMan1 = SC1.Eval("TipoAddExtrOspMan1")
                        TipoAddExtrOspMan2 = SC1.Eval("TipoAddExtrOspMan2")
                        TipoAddExtrOspMan3 = SC1.Eval("TipoAddExtrOspMan3")
                        TipoAddExtrOspMan4 = SC1.Eval("TipoAddExtrOspMan4")

                    End If


                    If Regola.ToUpper.IndexOf("ModificaTipoRegione".ToUpper) > 0 Then
                        SalvaTipoImportoRegione = SC1.Eval("ModificaTipoRegione")
                        InserisciRegione(SalvaCodiceRegione, SalvaTipoImportoRegione)
                    End If


                    TipoMov = SC1.Eval("TipoMov")


                    If InStr(1, CheckT, "M") > 0 Then
                        For Par = 0 To 10
                            ImpExtrComMan1(Par) = SC1.Eval("ImpExtrComMan1(" & Par & ")")
                            ImpExtrComMan2(Par) = SC1.Eval("ImpExtrComMan2(" & Par & ")")
                            ImpExtrComMan3(Par) = SC1.Eval("ImpExtrComMan3(" & Par & ")")
                            ImpExtrComMan4(Par) = SC1.Eval("ImpExtrComMan4(" & Par & ")")

                            NumExtrComMan1(Par) = SC1.Eval("NumExtrComMan1(" & Par & ")")
                            NumExtrComMan2(Par) = SC1.Eval("NumExtrComMan2(" & Par & ")")
                            NumExtrComMan3(Par) = SC1.Eval("NumExtrComMan3(" & Par & ")")
                            NumExtrComMan4(Par) = SC1.Eval("NumExtrComMan4(" & Par & ")")

                            TipoExtrComMan1(Par) = SC1.Eval("TipoExtrComMan1(" & Par & ")")
                            TipoExtrComMan2(Par) = SC1.Eval("TipoExtrComMan2(" & Par & ")")
                            TipoExtrComMan3(Par) = SC1.Eval("TipoExtrComMan3(" & Par & ")")
                            TipoExtrComMan4(Par) = SC1.Eval("TipoExtrComMan4(" & Par & ")")
                        Next
                    End If

                    If InStr(1, CheckT, "F") > 0 Then
                        For Par = 0 To 10
                            ImpExtrParMan1(Par) = SC1.Eval("ImpExtrParMan1 (" & Par & ")")
                            ImpExtrParMan2(Par) = SC1.Eval("ImpExtrParMan2 (" & Par & ")")
                            ImpExtrParMan3(Par) = SC1.Eval("ImpExtrParMan3 (" & Par & ")")
                            ImpExtrParMan4(Par) = SC1.Eval("ImpExtrParMan4 (" & Par & ")")

                            NumExtrParMan1(Par) = SC1.Eval("NumExtrParMan1 (" & Par & ")")
                            NumExtrParMan2(Par) = SC1.Eval("NumExtrParMan2 (" & Par & ")")
                            NumExtrParMan3(Par) = SC1.Eval("NumExtrParMan3 (" & Par & ")")
                            NumExtrParMan4(Par) = SC1.Eval("NumExtrParMan4 (" & Par & ")")

                            TipoExtrParMan1(Par) = SC1.Eval("TipoExtrParMan1 (" & Par & ")")
                            TipoExtrParMan2(Par) = SC1.Eval("TipoExtrParMan2 (" & Par & ")")
                            TipoExtrParMan3(Par) = SC1.Eval("TipoExtrParMan3 (" & Par & ")")
                            TipoExtrParMan4(Par) = SC1.Eval("TipoExtrParMan4 (" & Par & ")")
                        Next
                    End If

                    If InStr(1, CheckT, "O") > 0 Then
                        GiorniPres = SC1.Eval("GiorniPres")
                        GiorniAss = SC1.Eval("GiorniAss")
                        GiorniPresEnte = SC1.Eval("GiorniPresEnte")
                        GiorniAssEnte = SC1.Eval("GiorniAssEnte")
                    End If

                    If InStr(1, CheckT, "P") > 0 Then
                        NonGiorniPres = SC1.Eval("NonGiorniPres")
                        NonGiorniAss = SC1.Eval("NonGiorniAss")
                        NonGiorniPresEnte = SC1.Eval("NonGiorniPresEnte")
                        NonGiorniAssEnte = SC1.Eval("NonGiorniAssEnte")
                    End If

                    If InStr(1, CheckT, "C") > 0 Or InStr(1, CheckT, "D") > 0 Then
                        For Ext = 0 To 10
                            If InStr(1, CheckT, "C") > 0 Then
                                GiorniPresParente(Ext) = SC1.Eval("GiorniPresParente(" & Ext & ")")
                                GiorniAssParente(Ext) = SC1.Eval("GiorniAssParente (" & Ext & ")")

                                GiorniPresComune(Ext) = SC1.Eval("GiorniPresComune (" & Ext & ")")
                                GiorniAssComune(Ext) = SC1.Eval("GiorniAssComune (" & Ext & ")")


                                GiorniPresJolly(Ext) = SC1.Eval("GiorniPresJolly (" & Ext & ")")
                                GiorniAssJolly(Ext) = SC1.Eval("GiorniAssJolly (" & Ext & ")")


                                GiorniPresRegione(Ext) = SC1.Eval("GiorniPresRegione (" & Ext & ")")
                                GiorniAssRegione(Ext) = SC1.Eval("GiorniAssRegione (" & Ext & ")")
                            End If

                            If InStr(1, CheckT, "D") > 0 Then
                                NonGiorniPresParente(Ext) = SC1.Eval("NonGiorniPresParente (" & Ext & ")")
                                NonGiorniAssParente(Ext) = SC1.Eval("NonGiorniAssParente (" & Ext & ")")

                                NonGiorniPresComune(Ext) = SC1.Eval("NonGiorniPresComune (" & Ext & ")")
                                NonGiorniAssComune(Ext) = SC1.Eval("NonGiorniAssComune (" & Ext & ")")

                                NonGiorniPresJolly(Ext) = SC1.Eval("NonGiorniPresJolly (" & Ext & ")")
                                NonGiorniAssJolly(Ext) = SC1.Eval("NonGiorniAssJolly (" & Ext & ")")


                                NonGiorniPresRegione(Ext) = SC1.Eval("NonGiorniPresRegione (" & Ext & ")")
                                NonGiorniAssRegione(Ext) = SC1.Eval("NonGiorniAssRegione (" & Ext & ")")
                            End If
                        Next Ext
                    End If

                    If InStr(1, CheckT, "H") > 0 Then
                        SCSalvaComune = SC1.Eval("SalvaComune")

                        SCSalvaJolly = SC1.Eval("SalvaJolly")

                        SCSalvaRegione = SC1.Eval("SalvaRegione")
                        SCSalvaOspite = SC1.Eval("SalvaOspite")
                        SCSalvaOspite2 = SC1.Eval("SalvaOspite2")
                        SCSalvaEnte = SC1.Eval("SalvaEnte")
                    End If

                    If InStr(1, CheckT, "B") > 0 Or InStr(1, CheckT, "A") > 0 Then
                        For Ext = 0 To 10
                            If InStr(1, CheckT, "B") > 0 Then
                                SCSalvaParente(Ext) = SC1.Eval("SalvaParente(" & Ext & ")")
                                SCSalvaParente2(Ext) = SC1.Eval("SalvaParente2(" & Ext & ")")
                                SCSalvaPerc(Ext) = SC1.Eval("SalvaPerc(" & Ext & ")")

                            End If
                            If InStr(1, CheckT, "A") > 0 Then
                                SCSalvaImpExtrOspite(Ext) = SC1.Eval("SalvaImpExtrOspite(" & Ext & " )")
                                SCSalvaImpExtrComune(Ext) = SC1.Eval("SalvaImpExtrComune(" & Ext & ")")
                                SCSalvaImpExtrParente(Ext) = SC1.Eval("SalvaImpExtrParent(" & Ext & " )")

                                SCSalvaImpExtrJolly(Ext) = SC1.Eval("SalvaImpExtrJolly(" & Ext & ")")
                            End If
                        Next
                    End If
                End If

                If GiornoCambioRettaOspite > I Or GiornoCambioRettaOspite = 0 Then
                    'Auto
                    If TipoMov = "P" Or TipoMov = "" Then
                        ImportoPresOspite = ImportoPresOspite + SCSalvaOspite
                        ImportoPresOspite2 = ImportoPresOspite2 + SCSalvaOspite2

                        For Ext = 0 To 10
                            ImpExtraOspite(Ext) = ImpExtraOspite(Ext) + SCSalvaImpExtrOspite(Ext)
                            If SCSalvaImpExtrOspite(Ext) > 0 Then
                                MyTabCodiceExtrOspiteGIORNI(Ext) = MyTabCodiceExtrOspiteGIORNI(Ext) + 1
                            End If
                        Next
                        If (Modulo.MathRound(SalvaOspite, 2) <> 0 Or Modulo.MathRound(SalvaOspite2, 2) <> 0) And TipoMov = "P" Then GiorniPres = GiorniPres + 1
                    End If

                    If TipoMov = "A" Then
                        ImportoAssOspite = ImportoAssOspite + SCSalvaOspite
                        ImportoAssOspite2 = ImportoAssOspite2 + SCSalvaOspite2

                        For Ext = 0 To 10
                            ImpExtraOspite(Ext) = ImpExtraOspite(Ext) + SCSalvaImpExtrOspite(Ext)
                            If SCSalvaImpExtrOspite(Ext) > 0 Then
                                MyTabCodiceExtrOspiteGIORNI(Ext) = MyTabCodiceExtrOspiteGIORNI(Ext) + 1
                            End If
                        Next

                        If Modulo.MathRound(SalvaOspite, 2) <> 0 Or Modulo.MathRound(SalvaOspite2, 2) <> 0 Then GiorniAss = GiorniAss + 1
                    End If
                End If

                If GiornoCambioRettaOspite <= I And GiornoCambioRettaOspite <> 0 Then
                    If TipoMov = "P" Or TipoMov = "" Then
                        NonImportoPresOspite = NonImportoPresOspite + SCSalvaOspite
                        NonImportoPresOspite2 = NonImportoPresOspite2 + SCSalvaOspite2
                        For Ext = 0 To 10
                            NonImpExtraOspite(Ext) = NonImpExtraOspite(Ext) + SCSalvaImpExtrOspite(Ext)
                            If SCSalvaImpExtrOspite(Ext) > 0 Then
                                MyTabCodiceNonExtrOspiteGIORNI(Ext) = MyTabCodiceNonExtrOspiteGIORNI(Ext) + 1
                            End If
                        Next

                        If (Modulo.MathRound(SalvaOspite, 2) <> 0 Or Modulo.MathRound(SalvaOspite2, 2) <> 0) And TipoMov = "P" Then NonGiorniPres = NonGiorniPres + 1
                    End If
                    If TipoMov = "A" Then
                        NonImportoAssOspite = NonImportoAssOspite + SCSalvaOspite
                        NonImportoAssOspite2 = NonImportoAssOspite2 + SCSalvaOspite2
                        For Ext = 0 To 10
                            NonImpExtraOspite(Ext) = NonImpExtraOspite(Ext) + SCSalvaImpExtrOspite(Ext)

                            If SCSalvaImpExtrOspite(Ext) > 0 Then
                                MyTabCodiceNonExtrOspiteGIORNI(Ext) = MyTabCodiceNonExtrOspiteGIORNI(Ext) + 1
                            End If
                        Next

                        If Modulo.MathRound(SalvaOspite, 2) <> 0 Or Modulo.MathRound(SalvaOspite2, 2) <> 0 Then NonGiorniAss = NonGiorniAss + 1
                    End If

                End If



                'auto
                If TipoMov = "P" Or TipoMov = "" Then


                    For Par = 0 To MaxParenti
                        If GiornoCambioRettaParenti(Par) > I Or GiornoCambioRettaParenti(Par) = 0 Then

                            For Ext = 0 To 10
                                If SCSalvaImpExtrParente(Ext) <> 0 Then
                                    ImpPERC = Format(Modulo.MathRound(SalvaImpExtrParent(Ext) * SCSalvaPerc(Par) / 100, 2), "#,##0.00")
                                    ImpExtraParente(Par, Ext) = ImpExtraParente(Par, Ext) + ImpPERC
                                    If ImpPERC > 0 Then
                                        MyTabCodiceExtrParentGIORNI(Par, Ext) = MyTabCodiceExtrParentGIORNI(Par, Ext) + 1
                                    End If
                                End If
                            Next Ext
                            ImportoPresParente(Par) = ImportoPresParente(Par) + SCSalvaParente(Par)
                            ImportoPresParente2(Par) = ImportoPresParente2(Par) + SCSalvaParente2(Par)
                            If TipoMov = "P" Then
                                If SCSalvaParente(Par) > 0 Or SCSalvaParente2(Par) > 0 Then
                                    GiorniPresParente(Par) = GiorniPresParente(Par) + 1
                                End If
                            End If
                        End If
                    Next Par
                End If

                If TipoMov = "A" Then


                    For Par = 0 To MaxParenti
                        If GiornoCambioRettaParenti(Par) > I Or GiornoCambioRettaParenti(Par) = 0 Then

                            For Ext = 0 To 10
                                If SCSalvaImpExtrParente(Ext) <> 0 Then
                                    ImpPERC = Format(Modulo.MathRound(SalvaImpExtrParent(Ext) * SCSalvaPerc(Par) / 100, 2), "#,##0.00")
                                    ImpExtraParente(Par, Ext) = ImpExtraParente(Par, Ext) + ImpPERC
                                    If ImpPERC > 0 Then
                                        MyTabCodiceExtrParentGIORNI(Par, Ext) = MyTabCodiceExtrParentGIORNI(Par, Ext) + 1
                                    End If
                                End If
                            Next Ext
                            ImportoAssParente(Par) = ImportoAssParente(Par) + SCSalvaParente(Par)
                            ImportoAssParente2(Par) = ImportoAssParente2(Par) + SCSalvaParente2(Par)
                            If SCSalvaParente(Par) > 0 Or SCSalvaParente2(Par) > 0 Then
                                GiorniAssParente(Par) = GiorniAssParente(Par) + 1
                            End If
                        End If
                    Next Par
                End If


                If TipoMov = "P" Or TipoMov = "" Then
                    For Par = 0 To MaxParenti
                        If GiornoCambioRettaParenti(Par) <= I And GiornoCambioRettaParenti(Par) <> 0 Then
                            For Ext = 0 To 10
                                If SCSalvaImpExtrParente(Ext) <> 0 Then
                                    ImpPERC = Format(Modulo.MathRound(SalvaImpExtrParent(Ext) * SCSalvaPerc(Par) / 100, 2), "#,##0.00")
                                    NonImpExtraParente(Par, Ext) = NonImpExtraParente(Par, Ext) + ImpPERC
                                    If ImpPERC > 0 Then
                                        MyTabCodiceExtrParentNonGIORNI(Par, Ext) = MyTabCodiceExtrParentNonGIORNI(Par, Ext) + 1
                                    End If
                                End If
                            Next Ext
                            NonImportoPresParente(Par) = NonImportoPresParente(Par) + SCSalvaParente(Par)
                            NonImportoPresParente2(Par) = NonImportoPresParente2(Par) + SCSalvaParente2(Par)
                            If SCSalvaParente(Par) > 0 Or SCSalvaParente2(Par) > 0 Then
                                If TipoMov = "P" Then
                                    NonGiorniPresParente(Par) = NonGiorniPresParente(Par) + 1
                                End If
                            End If
                        End If
                    Next Par
                End If
                If TipoMov = "A" Then
                    For Par = 0 To MaxParenti
                        If GiornoCambioRettaParenti(Par) <= I And GiornoCambioRettaParenti(Par) <> 0 Then
                            For Ext = 0 To 10
                                If SCSalvaImpExtrParente(Ext) <> 0 Then
                                    ImpPERC = Format(Modulo.MathRound(SalvaImpExtrParent(Ext) * SCSalvaPerc(Par) / 100, 2), "#,##0.00")
                                    NonImpExtraParente(Par, Ext) = NonImpExtraParente(Par, Ext) + ImpPERC
                                    If ImpPERC > 0 Then
                                        MyTabCodiceExtrParentNonGIORNI(Par, Ext) = MyTabCodiceExtrParentNonGIORNI(Par, Ext) + 1
                                    End If
                                End If
                            Next Ext
                            NonImportoAssParente(Par) = NonImportoAssParente(Par) + SCSalvaParente(Par)
                            NonImportoAssParente2(Par) = NonImportoAssParente2(Par) + SCSalvaParente2(Par)
                            If SalvaParente(Par) > 0 Or SalvaParente2(Par) > 0 Then
                                If TipoMov <> "N" Then
                                    NonGiorniAssParente(Par) = NonGiorniAssParente(Par) + 1
                                End If
                            End If
                        End If
                    Next Par
                End If


                If AutoNonAuto = "N" Then
                    If TipoMov = "P" Or TipoMov = "" Then
                        TCom = TrovaComune(SalvaCodiceComune)
                        NonImportoPresComune(TCom) = NonImportoPresComune(TCom) + SCSalvaComune

                        TJol = TrovaJolly(SalvaCodiceJolly)
                        NonImportoPresJolly(TJol) = NonImportoPresJolly(TJol) + SCSalvaJolly

                        TReg = TrovaRegione(SalvaCodiceRegione, SalvaTipoImportoRegione)
                        NonImportoPresRegione(TReg) = NonImportoPresRegione(TReg) + SCSalvaRegione
                        TipoImportoPresRegione(TReg) = SalvaTipoImportoRegione

                        NonImportoPresEnte = NonImportoPresEnte + SCSalvaEnte
                        For Ext = 0 To 10

                            NonImpExtraComune(TCom, Ext) = NonImpExtraComune(TCom, Ext) + SCSalvaImpExtrComune(Ext)

                            If SCSalvaImpExtrComune(Ext) > 0 Then
                                MyTabCodiceExtrComuneGIORNI(Ext) = MyTabCodiceExtrComuneGIORNI(Ext) + 1
                            End If

                            NonImpExtraJolly(TJol, Ext) = NonImpExtraJolly(TJol, Ext) + SCSalvaImpExtrJolly(Ext)
                        Next Ext
                    End If
                    If TipoMov = "A" Then
                        TCom = TrovaComune(SalvaCodiceComune)
                        NonImportoAssComune(TCom) = NonImportoAssComune(TCom) + SCSalvaComune

                        TJol = TrovaJolly(SalvaCodiceJolly)
                        NonImportoAssJolly(TJol) = NonImportoAssJolly(TJol) + SCSalvaJolly

                        TReg = TrovaRegione(SalvaCodiceRegione, SalvaTipoImportoRegione)
                        NonImportoAssRegione(TReg) = NonImportoAssRegione(TReg) + SCSalvaRegione
                        TipoImportoAssRegione(TReg) = SalvaTipoImportoRegione

                        NonImportoAssEnte = NonImportoAssEnte + SCSalvaEnte
                        For Ext = 0 To 10

                            NonImpExtraComune(TCom, Ext) = NonImpExtraComune(TCom, Ext) + SCSalvaImpExtrComune(Ext)

                            If SCSalvaImpExtrComune(Ext) > 0 Then
                                MyTabCodiceExtrComuneGIORNI(Ext) = MyTabCodiceExtrComuneGIORNI(Ext) + 1
                            End If

                            'NonImpExtraParente(1, Ext) = NonImpExtraParente(1, Ext) + SCSalvaImpExtrParente(Ext)
                            NonImpExtraJolly(TJol, Ext) = NonImpExtraJolly(TJol, Ext) + SCSalvaImpExtrJolly(Ext)
                        Next Ext

                    End If
                    If TipoMov = "P" Then

                        If SalvaEnte <> 0 Then NonGiorniPresEnte = NonGiorniPresEnte + 1

                        TCom = TrovaComune(SalvaCodiceComune)
                        If Modulo.MathRound(SalvaComune, 2) <> 0 Then NonGiorniPresComune(TCom) = NonGiorniPresComune(TCom) + 1

                        TJol = TrovaJolly(SalvaCodiceJolly)
                        If Modulo.MathRound(SalvaJolly, 2) <> 0 Then NonGiorniPresJolly(TJol) = NonGiorniPresJolly(TJol) + 1

                        If Modulo.MathRound(SalvaRegione, 2) <> 0 Then NonGiorniPresRegione(TReg) = NonGiorniPresRegione(TReg) + 1
                    End If
                    If TipoMov = "A" Then

                        If SalvaEnte <> 0 Then NonGiorniAssEnte = NonGiorniAssEnte + 1
                        TCom = TrovaComune(SalvaCodiceComune)
                        If Modulo.MathRound(SalvaComune, 2) <> 0 Then NonGiorniAssComune(TCom) = NonGiorniAssComune(TCom) + 1
                        TJol = TrovaJolly(SalvaCodiceJolly)
                        If Modulo.MathRound(SalvaJolly, 2) <> 0 Then NonGiorniAssJolly(TJol) = NonGiorniAssJolly(TJol) + 1

                        If Modulo.MathRound(SalvaRegione, 2) <> 0 Then NonGiorniAssRegione(TReg) = NonGiorniAssRegione(TReg) + 1
                    End If
                Else
                    If TipoMov = "P" Or TipoMov = "" Then
                        TCom = TrovaComune(SalvaCodiceComune)
                        ImportoPresComune(TCom) = ImportoPresComune(TCom) + SCSalvaComune

                        TJol = TrovaJolly(SalvaCodiceJolly)
                        ImportoPresJolly(TJol) = ImportoPresJolly(TJol) + SCSalvaJolly

                        TReg = TrovaRegione(SalvaCodiceRegione, SalvaTipoImportoRegione)
                        ImportoPresRegione(TReg) = ImportoPresRegione(TReg) + SCSalvaRegione
                        TipoImportoPresRegione(TReg) = SalvaTipoImportoRegione

                        ImportoPresEnte = ImportoPresEnte + SCSalvaEnte
                        For Ext = 0 To 10

                            ImpExtraComune(TCom, Ext) = ImpExtraComune(TCom, Ext) + SCSalvaImpExtrComune(Ext)
                            If SCSalvaImpExtrComune(Ext) > 0 Then
                                MyTabCodiceExtrComuneGIORNI(Ext) = MyTabCodiceExtrComuneGIORNI(Ext) + 1
                            End If

                            ImpExtraJolly(TJol, Ext) = ImpExtraJolly(TJol, Ext) + SCSalvaImpExtrJolly(Ext)
                        Next Ext

                    End If
                    If TipoMov = "A" Then
                        TCom = TrovaComune(SalvaCodiceComune)
                        ImportoAssComune(TCom) = ImportoAssComune(TCom) + SCSalvaComune

                        TJol = TrovaJolly(SalvaCodiceJolly)
                        ImportoAssJolly(TJol) = ImportoAssJolly(TJol) + SCSalvaJolly


                        TReg = TrovaRegione(SalvaCodiceRegione, SalvaTipoImportoRegione)
                        ImportoAssRegione(TReg) = ImportoAssRegione(TReg) + SCSalvaRegione
                        TipoImportoAssRegione(TReg) = SalvaTipoImportoRegione

                        ImportoAssEnte = ImportoAssEnte + SCSalvaEnte
                        For Ext = 0 To 10

                            ImpExtraComune(TCom, Ext) = ImpExtraComune(TCom, Ext) + SCSalvaImpExtrComune(Ext)
                            If SCSalvaImpExtrComune(Ext) > 0 Then
                                MyTabCodiceExtrComuneGIORNI(Ext) = MyTabCodiceExtrComuneGIORNI(Ext) + 1
                            End If


                            ImpExtraJolly(TJol, Ext) = ImpExtraJolly(TJol, Ext) + SCSalvaImpExtrJolly(Ext)
                        Next Ext

                    End If
                    If TipoMov = "P" Then

                        If SalvaEnte <> 0 Then GiorniPresEnte = GiorniPresEnte + 1

                        If Modulo.MathRound(SalvaComune, 2) <> 0 Then GiorniPresComune(TCom) = GiorniPresComune(TCom) + 1

                        If Modulo.MathRound(SalvaJolly, 2) <> 0 Then GiorniPresJolly(TJol) = GiorniPresJolly(TJol) + 1

                        If Modulo.MathRound(SalvaRegione, 2) <> 0 Then GiorniPresRegione(TReg) = GiorniPresRegione(TReg) + 1
                    End If
                    If TipoMov = "A" Then

                        If SalvaEnte <> 0 Then GiorniAssEnte = GiorniAssEnte + 1
                        If Modulo.MathRound(SalvaComune, 2) <> 0 Then GiorniAssComune(TCom) = GiorniAssComune(TCom) + 1

                        If Modulo.MathRound(SalvaJolly, 2) <> 0 Then GiorniAssJolly(TJol) = GiorniAssJolly(TJol) + 1

                        If Modulo.MathRound(SalvaRegione, 2) <> 0 Then GiorniAssRegione(TReg) = GiorniAssRegione(TReg) + 1
                    End If
                End If

                SalvaComune = WSalvaComune

                SalvaJolly = WSalvaJolly

                SalvaRegione = WSalvaRegione

                SalvaTipoImportoRegione = WTipoRegione
                SalvaOspite = WSalvaOspite
                SalvaOspite2 = WSalvaOspite2
                SalvaEnte = WSalvaEnte
                SalvaRetta = WSalvaRetta
            End If
        Next I
        I = I + 1


        ForzaRPXOspite = 0
        BloccaConguaglioDaRegola = 0

        Regola = CampoCentroServizio(Cserv, "REGOLE")
        If Not IsDBNull(Regola) And Regola <> "" Then
            SC1.Reset()
            'SC1.AddObject("Form", Maschera)

            '************************************************************************
            '* Copio le variabili per VB Script
            '************************************************************************
            SC1.ExecuteStatement("DIM ImportoPresParente(10),ImportoAssParente(10) ")
            SC1.ExecuteStatement("DIM ImportoPresParente2(10),ImportoAssParente2(10) ")
            SC1.ExecuteStatement("DIM ImportoPresComune(10) , ImportoAssComune(10) ")

            SC1.ExecuteStatement("DIM ImportoPresJolly(10) , ImportoAssJolly(10) ")
            SC1.ExecuteStatement("DIM GiorniPresJolly(10) , NonGiorniPresJolly(10) ")
            SC1.ExecuteStatement("DIM GiorniAssJolly(10) , NonGiorniAssJolly(10) ")

            'GiorniPresJolly



            SC1.ExecuteStatement("DIM ImportoPresRegione(10) , ImportoAssRegione(10) ")
            SC1.ExecuteStatement("DIM NonImportoPresParente(10) , NonImportoAssParente(10) ")
            SC1.ExecuteStatement("DIM NonImportoPresParente2(10) , NonImportoAssParente2(10) ")

            SC1.ExecuteStatement("DIM NonImportoPresComune(10) , NonImportoAssComune(10) ")

            SC1.ExecuteStatement("DIM NonImportoPresJolly(10) , NonImportoAssJolly(10) ")

            SC1.ExecuteStatement("DIM NonImportoPresRegione(10) , NonImportoAssRegione(10) ")
            SC1.ExecuteStatement("DIM pXSalvaExtImporto1(10)")
            SC1.ExecuteStatement("DIM pXSalvaExtImporto2(10)")
            SC1.ExecuteStatement("DIM pXSalvaExtImporto3(10)")
            SC1.ExecuteStatement("DIM pXSalvaExtImporto4(10)")
            SC1.ExecuteStatement("DIM ParenteMensile(20)")
            SC1.ExecuteStatement("DIM ForzaRPXParente(10)")

            SC1.ExecuteStatement("DIM ImpExtrParMan1(10)")
            SC1.ExecuteStatement("DIM ImpExtrParMan2(10)")
            SC1.ExecuteStatement("DIM ImpExtrParMan3(10)")
            SC1.ExecuteStatement("DIM ImpExtrParMan4(10)")
            SC1.ExecuteStatement("DIM QuotaParente(10)")

            SC1.ExecuteStatement("NonImportoPresOspite = " & Replace(Modulo.MathRound(NonImportoPresOspite, 2), ",", "."))
            SC1.ExecuteStatement("NonImportoAssOspite = " & Replace(Modulo.MathRound(NonImportoAssOspite, 2), ",", "."))


            SC1.ExecuteStatement("NonImportoPresOspite2 = " & Replace(Modulo.MathRound(NonImportoPresOspite2, 2), ",", "."))
            SC1.ExecuteStatement("NonImportoAssOspite2 = " & Replace(Modulo.MathRound(NonImportoAssOspite2, 2), ",", "."))

            SC1.ExecuteStatement("NonImportoPresEnte = " & Replace(Modulo.MathRound(NonImportoPresEnte, 2), ",", "."))
            SC1.ExecuteStatement("NonImportoAssEnte = " & Replace(Modulo.MathRound(NonImportoAssEnte, 2), ",", "."))

            SC1.ExecuteStatement("ImportoPresOspite = " & Replace(Modulo.MathRound(ImportoPresOspite, 2), ",", "."))
            SC1.ExecuteStatement("ImportoAssOspite = " & Replace(Modulo.MathRound(ImportoAssOspite, 2), ",", "."))

            SC1.ExecuteStatement("ImportoPresOspite2 = " & Replace(Modulo.MathRound(ImportoPresOspite2, 2), ",", "."))
            SC1.ExecuteStatement("ImportoAssOspite2 = " & Replace(Modulo.MathRound(ImportoAssOspite2, 2), ",", "."))

            SC1.ExecuteStatement("ImportoPresEnte =  " & Replace(Modulo.MathRound(ImportoPresEnte, 2), ",", "."))
            SC1.ExecuteStatement("ImportoAssEnte = " & Replace(Modulo.MathRound(ImportoAssEnte, 2), ",", "."))



            SC1.ExecuteStatement("OspiteMensile = " & Replace(MyTabOspitiMensile, ",", "."))

            If Regola.ToUpper.IndexOf("TotaleGiornalieroRetta".ToUpper) >= 0 Then
                Dim RettaTotaleGiorno As Double = 0
                Dim IndiceGiornoTot As Integer

                For IndiceGiornoTot = 1 To 31
                    If MyTabImportoRetta(IndiceGiornoTot) > 0 Then
                        RettaTotaleGiorno = MyTabImportoRetta(IndiceGiornoTot)
                    End If
                Next
                SC1.ExecuteStatement("TotaleGiornalieroRetta  = " & Replace(RettaTotaleGiorno, ",", "."))
            End If

            SC1.ExecuteStatement("TotaleMensileRetta  = " & Replace(ImportoMensileRetta, ",", "."))
            SC1.ExecuteStatement("ImportoMensileComune = " & Replace(ImportoMensileComune, ",", "."))

            SC1.ExecuteStatement("XSalvaExtImporto1 = " & Replace(XSalvaExtImporto1, ",", "."))
            SC1.ExecuteStatement("XSalvaExtImporto2 = " & Replace(XSalvaExtImporto2, ",", "."))
            SC1.ExecuteStatement("XSalvaExtImporto3 = " & Replace(XSalvaExtImporto3, ",", "."))
            SC1.ExecuteStatement("XSalvaExtImporto4 = " & Replace(XSalvaExtImporto4, ",", "."))



            For Par = 1 To 10
                SC1.ExecuteStatement("pXSalvaExtImporto1(" & Par & ") = " & Replace(PXSalvaExtImporto1(Par), ",", "."))
                SC1.ExecuteStatement("pXSalvaExtImporto2(" & Par & ") = " & Replace(PXSalvaExtImporto1(Par), ",", "."))
                SC1.ExecuteStatement("pXSalvaExtImporto3(" & Par & ") = " & Replace(PXSalvaExtImporto1(Par), ",", "."))
                SC1.ExecuteStatement("pXSalvaExtImporto4(" & Par & ") = " & Replace(PXSalvaExtImporto1(Par), ",", "."))

                SC1.ExecuteStatement("ImpExtrParMan1(" & Par & ") = " & Replace(ImpExtrParMan1(Par), ",", "."))
                SC1.ExecuteStatement("ImpExtrParMan2(" & Par & ") = " & Replace(ImpExtrParMan2(Par), ",", "."))
                SC1.ExecuteStatement("ImpExtrParMan3(" & Par & ") = " & Replace(ImpExtrParMan3(Par), ",", "."))
                SC1.ExecuteStatement("ImpExtrParMan4(" & Par & ") = " & Replace(ImpExtrParMan4(Par), ",", "."))

                SC1.ExecuteStatement("ParenteMensile (" & Par & ") = " & Replace(MyTabParenteMensile(Par), ",", "."))


                Dim QuotaParente As Double
                If Regola.ToUpper.IndexOf("QUOTAPARENTE") > 0 Then
                    QuotaParente = QuoteGiornaliere(Cserv, CodOsp, "P", Par, DateSerial(Anno, Mese, GiorniMese(Mese, Anno)))
                End If
                SC1.ExecuteStatement("QuotaParente (" & Par & ") = " & Replace(QuotaParente, ",", "."))




                SC1.ExecuteStatement("GiorniPresJolly(" & Par & ") = " & Replace(GiorniPresJolly(Par), ",", "."))
                SC1.ExecuteStatement("GiorniAssJolly(" & Par & ") = " & Replace(GiorniAssJolly(Par), ",", "."))
                SC1.ExecuteStatement("NonGiorniPresJolly(" & Par & ") = " & Replace(NonGiorniPresJolly(Par), ",", "."))
                SC1.ExecuteStatement("NonGiorniAssJolly(" & Par & ") = " & Replace(NonGiorniAssJolly(Par), ",", "."))

            Next

            If Accolto = True Then
                SC1.ExecuteStatement("Accolto = 1")
            Else
                SC1.ExecuteStatement("Accolto = 0")
            End If
            If Dimesso = True Then
                SC1.ExecuteStatement("Dimesso = 1")
            Else
                SC1.ExecuteStatement("Dimesso = 0")
            End If

            If Regola.ToUpper.IndexOf("NomeOspite".ToUpper) > 0 Then
                Dim Mospite As New ClsOspite

                Mospite.CodiceOspite = CodOsp
                Mospite.Leggi(STRINGACONNESSIONEDB, Mospite.CodiceOspite)


                SC1.ExecuteStatement("NomeOspite = " & Chr(34) & Mospite.Nome & Chr(34))
            End If
            If Regola.ToUpper.IndexOf("OspiteDimesso".ToUpper) > 0 Then
                Dim M As New Cls_Movimenti

                SC1.ExecuteStatement("OspiteDimesso = 0")

                M.CodiceOspite = CodOsp
                M.UltimaData(STRINGACONNESSIONEDB, CodOsp)

                If M.TipoMov = "13" Then
                    SC1.ExecuteStatement("OspiteDimesso = 1")
                End If
            End If

            mGiorniPres = 0
            mGiorniAss = 0
            For I = 1 To 31
                If Trim(MyTabCausale(I)) <> "*" Then
                    If Trim(MyTabCausale(I)) = "" Then
                        mGiorniPres = mGiorniPres + 1
                    Else
                        mGiorniAss = mGiorniAss + 1
                    End If
                End If
            Next I
            Dim QuotaOspite As Double

            If Regola.ToUpper.IndexOf("QUOTAOSPITE") > 0 Then
                QuotaOspite = QuoteGiornaliere(Cserv, CodOsp, "O", 0, DateSerial(Anno, Mese, GiorniMese(Mese, Anno)))
            End If

            SC1.ExecuteStatement("QuotaOspite = " & Replace(QuotaOspite, ",", "."))

            SC1.ExecuteStatement("mGiorniPres = " & Replace(mGiorniPres, ",", "."))
            SC1.ExecuteStatement("mGiorniAss = " & Replace(mGiorniAss, ",", "."))
            SC1.ExecuteStatement("GiorniPres = " & Replace(GiorniPres, ",", "."))
            SC1.ExecuteStatement("GiorniAss = " & Replace(GiorniAss, ",", "."))
            SC1.ExecuteStatement("NonGiorniPres = " & Replace(NonGiorniPres, ",", "."))
            SC1.ExecuteStatement("NonGiorniAss = " & Replace(NonGiorniAss, ",", "."))
            SC1.ExecuteStatement("Variabile1 = " & Replace(VARIABILE1, ",", "."))
            SC1.ExecuteStatement("Variabile2 = " & Replace(Variabile2, ",", "."))
            SC1.ExecuteStatement("Variabile3 = " & Replace(Variabile3, ",", "."))
            SC1.ExecuteStatement("Variabile4 = " & Replace(Variabile4, ",", "."))

            SC1.ExecuteStatement("VariabileA = " & Replace(VARIABILEA, ",", "."))
            SC1.ExecuteStatement("VariabileB = " & Replace(VariabileB, ",", "."))
            SC1.ExecuteStatement("VariabileC = " & Replace(VariabileC, ",", "."))
            SC1.ExecuteStatement("VariabileD = " & Replace(VariabileD, ",", "."))

            SC1.ExecuteStatement("GiorniAssenza = " & MyTabGiornoAssenza(31))
            SC1.ExecuteStatement("GiorniRipetizioneCausale = " & MyTabGiorniRipetizioneCausale(31))
            SC1.ExecuteStatement("GiorniRipetizioneCausaleNC = " & MyTabGiorniRipetizioneCausaleNC(31))
            SC1.ExecuteStatement("MyTabGiorniRipetizioneCausaleACSum = " & MyTabGiorniRipetizioneCausaleACSum(31))
            SC1.ExecuteStatement("Stato = " & Chr(34) & AutoNonAuto & Chr(34))
            SC1.ExecuteStatement("CodiceOspite =" & CodOsp)
            SC1.ExecuteStatement("Mese = " & Val(Mese))
            SC1.ExecuteStatement("Anno = " & Val(Anno))
            SC1.ExecuteStatement("GiorniMese =" & GiorniMese(Mese, Anno))


            SC1.ExecuteStatement("ParentePagante= 0")
            Dim XPar As New Cls_Modalita

            XPar.UltimaData(STRINGACONNESSIONEDB, CodOsp, Cserv)
            If XPar.MODALITA = "P" Then
                SC1.ExecuteStatement("ParentePagante= 1")
            End If

            Dim UltMov As New Cls_Movimenti


            UltMov.CodiceOspite = CodOsp
            UltMov.CENTROSERVIZIO = Cserv
            UltMov.UltimaDataAccoglimento(STRINGACONNESSIONEDB)


            SC1.ExecuteStatement("Accoglimento = #" & Replace(Format(UltMov.Data, "MM/dd/yyyy"), ".", "/") & "#")




            SC1.ExecuteStatement("DIM CodiceComune(10)")

            SC1.ExecuteStatement("DIM CodiceJolly(10)")

            SC1.ExecuteStatement("DIM CodiceRegione(10)")
            SC1.ExecuteStatement("DIM ImportoExtrComune(10)")

            SC1.ExecuteStatement("DIM ImportoExtrJolly(10)")


            SC1.ExecuteStatement("DIM ImportoExtrParent(10)")
            SC1.ExecuteStatement("DIM ImportoExtrOspite(10)")
            SC1.ExecuteStatement("DIM NonImpExtraOspite(10)")

            SC1.ExecuteStatement("DIM ImpExtraOspite(10)")

            SC1.ExecuteStatement("DIM TipoExtrParMan1(10)")
            SC1.ExecuteStatement("DIM TipoExtrParMan2(10)")
            SC1.ExecuteStatement("DIM TipoExtrParMan3(10)")
            SC1.ExecuteStatement("DIM TipoExtrParMan4(10)")



            For Ext = 0 To 10
                SC1.ExecuteStatement("ImportoExtrComune(" & Ext & ") = " & Replace(ImportoExtrComune(Ext), ",", "."))

                If ImpExtraComune(Ext, 0) > 0 Then
                    '            ImpExtraComune(Ext, 0) = Modulo.MathRound(ImportoExtrComune(Ext - 1), 2)
                    'ImpExtraComune(Ext, 0) = Modulo.MathRound(ImportoExtrComune(Ext), 2)
                End If

                SC1.ExecuteStatement("ImportoExtrJolly(" & Ext & ") = " & Replace(ImportoExtrJolly(Ext), ",", "."))

                SC1.ExecuteStatement("ImportoExtrParent(" & Ext & ") = " & Replace(ImportoExtrParent(Ext), ",", "."))
                SC1.ExecuteStatement("ImportoExtrOspite(" & Ext & ") = " & Replace(ImportoExtrOspite(Ext), ",", "."))

                SC1.ExecuteStatement("ImpExtraOspite(" & Ext & ") = " & Replace(ImpExtraOspite(Ext), ",", "."))
                SC1.ExecuteStatement("NonImpExtraOspite(" & Ext & ") = " & Replace(NonImpExtraOspite(Ext), ",", "."))

                If CodiceComune(Ext) <> "" Then SC1.ExecuteStatement("CodiceComune(" & Ext & ") = " & Chr(34) & CodiceComune(Ext) & Chr(34))

                If CodiceJolly(Ext) <> "" Then SC1.ExecuteStatement("CodiceJolly(" & Ext & ") = " & Chr(34) & CodiceJolly(Ext) & Chr(34))

                If CodiceRegione(Ext) <> "" Then SC1.ExecuteStatement("CodiceRegione(" & Ext & ") = " & Chr(34) & CodiceRegione(Ext) & Chr(34))



                'SC1.ExecuteStatement "ParenteMensile = " & Replace(MyTabParenteMensile(Ext), ",", ".")
                SC1.ExecuteStatement("ImportoPresParente(" & Ext & ") = " & Replace(Modulo.MathRound(ImportoPresParente(Ext), 2), ",", "."))
                SC1.ExecuteStatement("ImportoAssParente (" & Ext & ") = " & Replace(Modulo.MathRound(ImportoAssParente(Ext), 2), ",", "."))
                SC1.ExecuteStatement("ImportoPresParente2(" & Ext & ") = " & Replace(Modulo.MathRound(ImportoPresParente2(Ext), 2), ",", "."))
                SC1.ExecuteStatement("ImportoAssParente2(" & Ext & ") = " & Replace(Modulo.MathRound(ImportoAssParente2(Ext), 2), ",", "."))

                SC1.ExecuteStatement("ImportoPresComune(" & Ext & ") = " & Replace(Modulo.MathRound(ImportoPresComune(Ext), 2), ",", "."))
                SC1.ExecuteStatement("ImportoAssComune (" & Ext & ") = " & Replace(Modulo.MathRound(ImportoAssComune(Ext), 2), ",", "."))

                SC1.ExecuteStatement("ImportoPresJolly(" & Ext & ") = " & Replace(Modulo.MathRound(ImportoPresJolly(Ext), 2), ",", "."))
                SC1.ExecuteStatement("ImportoAssJolly (" & Ext & ") = " & Replace(Modulo.MathRound(ImportoAssJolly(Ext), 2), ",", "."))


                SC1.ExecuteStatement("GiorniPresJolly(" & Ext & ") = " & Replace(Modulo.MathRound(GiorniPresJolly(Ext), 2), ",", "."))
                SC1.ExecuteStatement("GiorniAssJolly (" & Ext & ") = " & Replace(Modulo.MathRound(GiorniAssJolly(Ext), 2), ",", "."))

                SC1.ExecuteStatement("NonGiorniPresJolly(" & Ext & ") = " & Replace(Modulo.MathRound(NonGiorniPresJolly(Ext), 2), ",", "."))
                SC1.ExecuteStatement("NonGiorniAssJolly (" & Ext & ") = " & Replace(Modulo.MathRound(NonGiorniAssJolly(Ext), 2), ",", "."))



                SC1.ExecuteStatement("ImportoPresRegione(" & Ext & ") = " & Replace(Modulo.MathRound(ImportoPresRegione(Ext), 2), ",", "."))
                SC1.ExecuteStatement("ImportoAssRegione(" & Ext & ") = " & Replace(Modulo.MathRound(ImportoAssRegione(Ext), 2), ",", "."))
                SC1.ExecuteStatement("NonImportoPresParente(" & Ext & ") = " & Replace(Modulo.MathRound(NonImportoPresParente(Ext), 2), ",", "."))
                SC1.ExecuteStatement("NonImportoAssParente(" & Ext & ") = " & Replace(Modulo.MathRound(NonImportoAssParente(Ext), 2), ",", "."))
                SC1.ExecuteStatement("NonImportoPresParente2(" & Ext & ") = " & Replace(Modulo.MathRound(NonImportoPresParente2(Ext), 2), ",", "."))
                SC1.ExecuteStatement("NonImportoAssParente2(" & Ext & ") = " & Replace(Modulo.MathRound(NonImportoAssParente2(Ext), 2), ",", "."))

                SC1.ExecuteStatement("NonImportoPresComune(" & Ext & ") = " & Replace(Modulo.MathRound(NonImportoPresComune(Ext), 2), ",", "."))
                SC1.ExecuteStatement("NonImportoAssComune(" & Ext & ") = " & Replace(Modulo.MathRound(NonImportoAssComune(Ext), 2), ",", "."))

                SC1.ExecuteStatement("NonImportoPresJolly(" & Ext & ") = " & Replace(Modulo.MathRound(NonImportoPresJolly(Ext), 2), ",", "."))
                SC1.ExecuteStatement("NonImportoAssJolly(" & Ext & ") = " & Replace(Modulo.MathRound(NonImportoAssJolly(Ext), 2), ",", "."))

                SC1.ExecuteStatement("NonImportoPresRegione(" & Ext & ") = " & Replace(Modulo.MathRound(NonImportoPresRegione(Ext), 2), ",", "."))
                SC1.ExecuteStatement("NonImportoAssRegione(" & Ext & ") = " & Replace(Modulo.MathRound(NonImportoAssRegione(Ext), 2), ",", "."))

            Next

            For I = 31 To 1 Step -1
                If Trim(MyTabModalita(I)) <> "" Then
                    SC1.ExecuteStatement("Modalita = """ & MyTabModalita(I) & """")
                    Exit For
                End If
            Next

            '******************************************************************************
            '*
            '******************************************************************************
            SC1.ExecuteStatement("XSalvaExtImporto1 = " & Replace(XSalvaExtImporto1, ",", "."))
            SC1.ExecuteStatement("XSalvaExtImporto2 = " & Replace(XSalvaExtImporto2, ",", "."))
            SC1.ExecuteStatement("XSalvaExtImporto3 = " & Replace(XSalvaExtImporto3, ",", "."))
            SC1.ExecuteStatement("XSalvaExtImporto4 = " & Replace(XSalvaExtImporto4, ",", "."))

            SC1.ExecuteStatement("XSalvaExtImporto1C = " & Replace(XSalvaExtImporto1C, ",", "."))
            SC1.ExecuteStatement("XSalvaExtImporto2C = " & Replace(XSalvaExtImporto2C, ",", "."))
            SC1.ExecuteStatement("XSalvaExtImporto3C = " & Replace(XSalvaExtImporto3C, ",", "."))
            SC1.ExecuteStatement("XSalvaExtImporto4C = " & Replace(XSalvaExtImporto4C, ",", "."))

            SC1.ExecuteStatement("XSalvaExtImporto1j = " & Replace(XSalvaExtImporto1J, ",", "."))
            SC1.ExecuteStatement("XSalvaExtImporto2j = " & Replace(XSalvaExtImporto2J, ",", "."))
            SC1.ExecuteStatement("XSalvaExtImporto3j = " & Replace(XSalvaExtImporto3J, ",", "."))
            SC1.ExecuteStatement("XSalvaExtImporto4j = " & Replace(XSalvaExtImporto4J, ",", "."))

            If Mese = 12 Then
                xMese = 1
                xAnno = Anno + 1
            Else
                xMese = Mese + 1
                xAnno = Anno
            End If

            GiornoCalcolati = GiorniNelMese(Cserv, CodOsp, xMese, xAnno)

            For Par = 1 To 10
                SC1.ExecuteStatement("pXSalvaExtImporto1(" & Par & ") = " & Replace(PXSalvaExtImporto1(Par), ",", "."))
                SC1.ExecuteStatement("pXSalvaExtImporto2(" & Par & ") = " & Replace(PXSalvaExtImporto2(Par), ",", "."))
                SC1.ExecuteStatement("pXSalvaExtImporto3(" & Par & ") = " & Replace(PXSalvaExtImporto3(Par), ",", "."))
                SC1.ExecuteStatement("pXSalvaExtImporto4(" & Par & ") = " & Replace(PXSalvaExtImporto4(Par), ",", "."))

                SC1.ExecuteStatement("TipoExtrParMan1(" & Par & ") = " & Chr(34) & TipoExtrParMan1(Par) & Chr(34))
                SC1.ExecuteStatement("TipoExtrParMan2(" & Par & ") = " & Chr(34) & TipoExtrParMan2(Par) & Chr(34))
                SC1.ExecuteStatement("TipoExtrParMan3(" & Par & ") = " & Chr(34) & TipoExtrParMan3(Par) & Chr(34))
                SC1.ExecuteStatement("TipoExtrParMan4(" & Par & ") = " & Chr(34) & TipoExtrParMan4(Par) & Chr(34))



                ForzaRPXParente(Par) = 0

                If Regola.ToUpper.IndexOf("ForzaRPXParente".ToUpper) > 0 Then
                    If OspitePresente(Cserv, CodOsp, xMese, xAnno) = True Then
                        SalvaImportoRegione = 0
                        SalvaImportoComune = 0
                        SalvaImportoOspite = 0
                        SalvaImportoTotale = 0
                        SalvaPercentuale = 0

                        ForzaRPXParente(Par) = 0

                        Dim LO_Parente As New Cls_Parenti

                        LO_Parente.FattAnticipata = ""

                        LO_Parente.Leggi(STRINGACONNESSIONEDB, CodOsp, Par)


                        Dim KCs As New Cls_DatiOspiteParenteCentroServizio

                        KCs.CentroServizio = Cserv
                        KCs.CodiceOspite = CodOsp
                        KCs.CodiceParente = Par
                        KCs.Leggi(STRINGACONNESSIONEDB)

                        REM Assegna la tipologia operazione e l'aliquota prendendo quella relativa al cserv
                        If KCs.CodiceOspite <> 0 Then
                            LO_Parente.TIPOOPERAZIONE = KCs.TipoOperazione
                            LO_Parente.CODICEIVA = KCs.AliquotaIva
                            LO_Parente.FattAnticipata = KCs.Anticipata
                            LO_Parente.MODALITAPAGAMENTO = KCs.ModalitaPagamento
                            LO_Parente.Compensazione = KCs.Compensazione
                        End If

                        'CampoParente(CodOsp, Par, "FattAnticipata", False) 
                        If LO_Parente.FattAnticipata = "S" Then
                            For I = 1 To GiornoCalcolati
                                ForzaRPXParente(Par) = ForzaRPXParente(Par) + QuoteGiornaliere(Cserv, CodOsp, "P", Par, DateSerial(xAnno, xMese, I))
                            Next I
                        End If

                        'CampoParente(CodOsp, Par, "FattAnticipata", False) 
                        If LO_Parente.FattAnticipata = "S" Then
                            If QuoteMensile(Cserv, CodOsp, "P", Par, DateSerial(xAnno, xMese, I)) > 0 And ForzaRPXParente(Par) > 0 Then
                                ForzaRPXParente(Par) = QuoteMensile(Cserv, CodOsp, "P", Par, DateSerial(xAnno, xMese, I)) 'ImportoMensileParente(Par)
                            Else
                                If ImportoMensileParente(Par) = 0 And ForzaRPXParente(Par) = 0 Then
                                    If QuoteMensile(Cserv, CodOsp, "P", Par, DateSerial(xAnno, xMese, I)) > 0 Then
                                        ForzaRPXParente(Par) = QuoteMensile(Cserv, CodOsp, "P", Par, DateSerial(xAnno, xMese, I))  'ImportoMensileParente(Par)
                                    End If
                                End If
                            End If
                        End If
                    End If
                    SC1.ExecuteStatement("ForzaRPXParente(" & Par & ")= " & Replace(ForzaRPXParente(Par), ",", "."))
                End If
            Next
            '******************************************************************************
            '*
            '******************************************************************************


            ForzaRPXOspite = 0
            If Regola.ToUpper.IndexOf("ForzaRPXOspite".ToUpper) > 0 Then
                If OspitePresente(Cserv, CodOsp, xMese, xAnno) = True Then
                    SalvaImportoRegione = 0
                    SalvaImportoComune = 0
                    SalvaImportoOspite = 0
                    SalvaImportoTotale = 0
                    SalvaPercentuale = 0
                    ForzaRPXOspite = 0


                    Dim LO_CodiceOspite As New ClsOspite

                    LO_CodiceOspite.Leggi(STRINGACONNESSIONEDB, CodOsp)


                    Dim KCs As New Cls_DatiOspiteParenteCentroServizio

                    KCs.CentroServizio = Cserv
                    KCs.CodiceOspite = CodOsp
                    KCs.CodiceParente = 0
                    KCs.Leggi(STRINGACONNESSIONEDB)

                    REM Assegna la tipologia operazione e l'aliquota prendendo quella relativa al cserv
                    If KCs.CodiceOspite <> 0 Then
                        LO_CodiceOspite.TIPOOPERAZIONE = KCs.TipoOperazione
                        LO_CodiceOspite.CODICEIVA = KCs.AliquotaIva
                        LO_CodiceOspite.FattAnticipata = KCs.Anticipata
                        LO_CodiceOspite.MODALITAPAGAMENTO = KCs.ModalitaPagamento
                        LO_CodiceOspite.Compensazione = KCs.Compensazione
                        LO_CodiceOspite.SETTIMANA = KCs.Settimana
                    End If

                    'CampoOspite(CodOsp, "FattAnticipata", False)
                    If LO_CodiceOspite.FattAnticipata = "S" Then
                        For I = 1 To GiornoCalcolati
                            ForzaRPXOspite = ForzaRPXOspite + QuoteGiornaliere(Cserv, CodOsp, "O1", 0, DateSerial(xAnno, xMese, I))
                        Next I
                    End If

                    If LO_CodiceOspite.FattAnticipata = "S" Then
                        If QuoteMensile(Cserv, CodOsp, "O1", 0, DateSerial(xAnno, xMese, I - 1)) > 0 And ForzaRPXOspite > 0 Then
                            ForzaRPXOspite = QuoteMensile(Cserv, CodOsp, "O", 0, DateSerial(xAnno, xMese, I - 1))
                        Else
                            If ImportoMensileOspite = 0 And ForzaRPXOspite > 0 Then
                                If QuoteMensile(Cserv, CodOsp, "O1", 0, DateSerial(xAnno, xMese, I)) > 0 Then
                                    ForzaRPXOspite = QuoteMensile(Cserv, CodOsp, "O", 0, DateSerial(xAnno, xMese, I))
                                End If
                            End If
                        End If
                    End If
                End If
                SC1.ExecuteStatement("ForzaRPXOspite= " & Replace(ForzaRPXOspite, ",", "."))
            End If
            SC1.ExecuteStatement("XSalvaExtImporto1C = " & Replace(XSalvaExtImporto1C, ",", "."))
            SC1.ExecuteStatement("XSalvaExtImporto2C = " & Replace(XSalvaExtImporto2C, ",", "."))
            SC1.ExecuteStatement("XSalvaExtImporto3C = " & Replace(XSalvaExtImporto3C, ",", "."))
            SC1.ExecuteStatement("XSalvaExtImporto4C = " & Replace(XSalvaExtImporto4C, ",", "."))

            SC1.ExecuteStatement("XSalvaExtImporto1j = " & Replace(XSalvaExtImporto1J, ",", "."))
            SC1.ExecuteStatement("XSalvaExtImporto2j = " & Replace(XSalvaExtImporto2J, ",", "."))
            SC1.ExecuteStatement("XSalvaExtImporto3j = " & Replace(XSalvaExtImporto3J, ",", "."))
            SC1.ExecuteStatement("XSalvaExtImporto4j = " & Replace(XSalvaExtImporto4J, ",", "."))

            SC1.ExecuteStatement("BloccaConguaglioDaRegola = 0")

            SC1.ExecuteStatement("ImportoAddebitoPrimoComune = 0")

            SC1.ExecuteStatement("ImportoAddebitoPrimoJolly = 0")

            SC1.ExecuteStatement("ImportoAddebitoPrimoRegione = 0")
            SC1.ExecuteStatement("TipoAddebitoPrimoRegione =" & Chr(34) & Chr(34))

            SC1.ExecuteStatement("TipoAddebitoPrimoComune =" & Chr(34) & Chr(34))

            SC1.ExecuteStatement("TipoAddebitoPrimoJolly =" & Chr(34) & Chr(34))

            SC1.ExecuteStatement("Cserv = " & Chr(34) & Cserv & Chr(34))
            SC1.ExecuteStatement("ImpExtrOspMan1 = " & Replace(ImpExtrOspMan1, ",", "."))
            SC1.ExecuteStatement("ImpExtrOspMan2 = " & Replace(ImpExtrOspMan2, ",", "."))
            SC1.ExecuteStatement("ImpExtrOspMan3 = " & Replace(ImpExtrOspMan3, ",", "."))
            SC1.ExecuteStatement("ImpExtrOspMan4 = " & Replace(ImpExtrOspMan4, ",", "."))

            SC1.ExecuteStatement("TipoAddExtrOspMan1 = " & Chr(34) & TipoAddExtrOspMan1 & Chr(34))
            SC1.ExecuteStatement("TipoAddExtrOspMan2 = " & Chr(34) & TipoAddExtrOspMan2 & Chr(34))
            SC1.ExecuteStatement("TipoAddExtrOspMan3 = " & Chr(34) & TipoAddExtrOspMan3 & Chr(34))
            SC1.ExecuteStatement("TipoAddExtrOspMan4 = " & Chr(34) & TipoAddExtrOspMan4 & Chr(34))

            SC1.ExecuteStatement("StringaDegliErrori = " & Chr(34) & Chr(34))

            If Regola.ToUpper.IndexOf("TIPORETTA") > 0 Then
                Dim VD As New Cls_rettatotale

                VD.CENTROSERVIZIO = Cserv
                VD.CODICEOSPITE = CodOsp
                VD.UltimaData(STRINGACONNESSIONEDB, CodOsp, Cserv)
                SC1.ExecuteStatement("TipoRetta = " & Chr(34) & VD.TipoRetta & Chr(34))
            End If


            'UscitoPrimaMese
            'USCITOPRIMAMESE
            'UscitoPenultimoGiorno
            'USCITOPENULTIMOGIORNO

            If Regola.ToUpper.IndexOf("USCITOPRIMAMESE") > 0 Or Regola.ToUpper.IndexOf("USCITOPENULTIMOGIORNO") > 0 Then
                Dim Kl As New Cls_Movimenti

                Kl.CodiceOspite = CodOsp
                Kl.CENTROSERVIZIO = Cserv
                Kl.UltimaMovimentoPrimaData(STRINGACONNESSIONEDB, CodOsp, Cserv, DateSerial(Anno, Mese, 1))
                If Kl.TipoMov = "03" Then
                    SC1.ExecuteStatement("UscitoPrimaMese = 1")
                Else
                    SC1.ExecuteStatement("UscitoPrimaMese = 0")
                End If



                Kl.CodiceOspite = CodOsp
                Kl.CENTROSERVIZIO = Cserv
                Kl.UltimaMovimentoPrimaData(STRINGACONNESSIONEDB, CodOsp, Cserv, DateSerial(Anno, Mese, GiorniMese(Mese, Anno)))
                If Kl.TipoMov = "03" And Day(Kl.Data) = GiorniMese(Mese, Anno) - 1 Then
                    SC1.ExecuteStatement("UscitoPenultimoGiorno = 1")
                Else
                    SC1.ExecuteStatement("UscitoPenultimoGiorno = 0")
                End If
            End If

            SC1.AddCode(Regola)


            Try
                SC1.Run("Calcolo")
            Catch ex As Exception
                StringaDegliErrori = StringaDegliErrori & " Errore in Regola (10195) " & ex.Message
            End Try



            'Call Calcolo(VARIABILE1)

            '************************************************************************
            '* Leggo le variabili modificate da VB SCRIPT
            '************************************************************************
            NonImportoPresOspite = SC1.Eval("NonImportoPresOspite")
            NonImportoAssOspite = SC1.Eval("NonImportoAssOspite")
            NonImportoPresOspite2 = SC1.Eval("NonImportoPresOspite2")
            NonImportoAssOspite2 = SC1.Eval("NonImportoAssOspite2")
            NonImportoPresEnte = SC1.Eval("NonImportoPresEnte")
            NonImportoAssEnte = SC1.Eval("NonImportoAssEnte")
            ImportoPresOspite = SC1.Eval("ImportoPresOspite")
            ImportoAssOspite = SC1.Eval("ImportoAssOspite")
            ImportoPresOspite2 = SC1.Eval("ImportoPresOspite2")
            ImportoAssOspite2 = SC1.Eval("ImportoAssOspite2")
            ImportoPresEnte = SC1.Eval("ImportoPresEnte")
            ImportoAssEnte = SC1.Eval("ImportoAssEnte")
            GiorniPres = SC1.Eval("GiorniPres")
            GiorniAss = SC1.Eval("GiorniAss")
            NonGiorniPres = SC1.Eval("NonGiorniPres")
            NonGiorniAss = SC1.Eval("NonGiorniAss")
            VARIABILE1 = SC1.Eval("Variabile1")
            Variabile2 = SC1.Eval("Variabile2")
            Variabile3 = SC1.Eval("Variabile3")
            Variabile4 = SC1.Eval("Variabile4")

            VARIABILEA = SC1.Eval("VariabileA")
            VariabileB = SC1.Eval("VariabileB")
            VariabileC = SC1.Eval("VariabileC")
            VariabileD = SC1.Eval("VariabileD")

            BloccaConguaglioDaRegola = Val(SC1.Eval("BloccaConguaglioDaRegola"))

            ImpExtrOspMan1 = SC1.Eval("ImpExtrOspMan1")
            ImpExtrOspMan2 = SC1.Eval("ImpExtrOspMan2")
            ImpExtrOspMan3 = SC1.Eval("ImpExtrOspMan3")
            ImpExtrOspMan4 = SC1.Eval("ImpExtrOspMan4")
            TipoAddExtrOspMan1 = SC1.Eval("TipoAddExtrOspMan1")
            TipoAddExtrOspMan2 = SC1.Eval("TipoAddExtrOspMan2")
            TipoAddExtrOspMan3 = SC1.Eval("TipoAddExtrOspMan3")
            TipoAddExtrOspMan4 = SC1.Eval("TipoAddExtrOspMan4")

            For Ext = 0 To 10



                ImportoExtrComune(Ext) = SC1.Eval("ImportoExtrComune(" & Ext & ")")

                ImportoExtrJolly(Ext) = SC1.Eval("ImportoExtrJolly(" & Ext & ")")

                ImportoExtrParent(Ext) = SC1.Eval("ImportoExtrParent(" & Ext & ")")
                ImportoExtrOspite(Ext) = SC1.Eval("ImportoExtrOspite(" & Ext & ")")
                ImportoPresParente(Ext) = SC1.Eval("ImportoPresParente(" & Ext & ")")
                ImportoAssParente(Ext) = SC1.Eval("ImportoAssParente (" & Ext & ")")
                ImportoPresParente2(Ext) = SC1.Eval("ImportoPresParente2(" & Ext & ")")
                ImportoAssParente2(Ext) = SC1.Eval("ImportoAssParente2(" & Ext & ")")


                ImpExtraOspite(Ext) = SC1.Eval("ImpExtraOspite(" & Ext & ")")
                NonImpExtraOspite(Ext) = SC1.Eval("NonImpExtraOspite(" & Ext & ")")


                ImportoPresComune(Ext) = SC1.Eval("ImportoPresComune(" & Ext & ")")
                ImportoAssComune(Ext) = SC1.Eval("ImportoAssComune (" & Ext & ")")

                ImportoPresJolly(Ext) = SC1.Eval("ImportoPresJolly(" & Ext & ")")
                ImportoAssJolly(Ext) = SC1.Eval("ImportoAssJolly (" & Ext & ")")



                NonGiorniPresJolly(Ext) = SC1.Eval("NonGiorniPresJolly (" & Ext & ")")
                NonGiorniAssJolly(Ext) = SC1.Eval("NonGiorniAssJolly (" & Ext & ")")

                GiorniPresJolly(Ext) = SC1.Eval("GiorniPresJolly (" & Ext & ")")
                GiorniAssJolly(Ext) = SC1.Eval("GiorniAssJolly (" & Ext & ")")


                ImportoPresRegione(Ext) = SC1.Eval("ImportoPresRegione(" & Ext & ")")
                ImportoAssRegione(Ext) = SC1.Eval("ImportoAssRegione(" & Ext & ")")
                NonImportoPresParente(Ext) = SC1.Eval("NonImportoPresParente(" & Ext & ")")
                NonImportoAssParente(Ext) = SC1.Eval("NonImportoAssParente(" & Ext & ")")

                NonImportoPresParente2(Ext) = SC1.Eval("NonImportoPresParente2(" & Ext & ")")
                NonImportoAssParente2(Ext) = SC1.Eval("NonImportoAssParente2(" & Ext & ")")

                NonImportoPresComune(Ext) = SC1.Eval("NonImportoPresComune(" & Ext & ")")

                NonImportoPresJolly(Ext) = SC1.Eval("NonImportoPresJolly(" & Ext & ")")

                NonImportoAssComune(Ext) = SC1.Eval("NonImportoAssComune(" & Ext & ")")

                NonImportoAssJolly(Ext) = SC1.Eval("NonImportoAssJolly(" & Ext & ")")

                NonImportoPresRegione(Ext) = SC1.Eval("NonImportoPresRegione(" & Ext & ")")
                NonImportoAssRegione(Ext) = SC1.Eval("NonImportoAssRegione(" & Ext & ")")
                ForzaRPXParente(Ext) = SC1.Eval("ForzaRPXParente(" & Ext & ")")

                ImpExtrParMan1(Ext) = SC1.Eval("ImpExtrParMan1 (" & Ext & ")")
                ImpExtrParMan2(Ext) = SC1.Eval("ImpExtrParMan2 (" & Ext & ")")
                ImpExtrParMan3(Ext) = SC1.Eval("ImpExtrParMan3 (" & Ext & ")")
                ImpExtrParMan4(Ext) = SC1.Eval("ImpExtrParMan4 (" & Ext & ")")

                TipoExtrParMan1(Ext) = SC1.Eval("TipoExtrParMan1(" & Ext & ")")
                TipoExtrParMan2(Ext) = SC1.Eval("TipoExtrParMan2(" & Ext & ")")
                TipoExtrParMan3(Ext) = SC1.Eval("TipoExtrParMan3(" & Ext & ")")
                TipoExtrParMan4(Ext) = SC1.Eval("TipoExtrParMan4(" & Ext & ")")
            Next

            ForzaRPXOspite = SC1.Eval("ForzaRPXOspite")

            ImportoAddebitoPrimoComune = SC1.Eval("ImportoAddebitoPrimoComune")
            TipoAddebitoPrimoComune = SC1.Eval("TipoAddebitoPrimoComune")


            ImportoAddebitoPrimoJolly = SC1.Eval("ImportoAddebitoPrimoJolly")

            ImportoAddebitoPrimoRegione = SC1.Eval("ImportoAddebitoPrimoRegione")
            TipoAddebitoPrimoRegione = SC1.Eval("TipoAddebitoPrimoRegione")
            TipoAddebitoPrimoJolly = SC1.Eval("TipoAddebitoPrimoJolly")


            StringaDegliErrori = StringaDegliErrori & SC1.Eval("StringaDegliErrori")
        End If




    End Sub


    Private Function AssenzaDiurnoNellAnnoEscludiAssenza(ByVal Cserv As String, ByVal CodOsp As Long, ByVal Anno As Long, ByVal Mese As Long, ByVal Giorno As Integer) As Integer
        Dim IndiceMese As Integer
        Dim I As Integer
        Dim GiorniCau(31) As String


        AssenzaDiurnoNellAnnoEscludiAssenza = 0
        For IndiceMese = 1 To Mese - 1
            Dim Diurno As New Cls_Diurno


            Diurno.Leggi(STRINGACONNESSIONEDB, CodOsp, Cserv, Anno, IndiceMese)
            GiorniCau(1) = Diurno.Giorno1
            GiorniCau(2) = Diurno.Giorno2
            GiorniCau(3) = Diurno.Giorno3
            GiorniCau(4) = Diurno.Giorno4
            GiorniCau(5) = Diurno.Giorno5
            GiorniCau(6) = Diurno.Giorno6
            GiorniCau(7) = Diurno.Giorno7
            GiorniCau(8) = Diurno.Giorno8
            GiorniCau(9) = Diurno.Giorno9
            GiorniCau(10) = Diurno.Giorno10
            GiorniCau(11) = Diurno.Giorno11
            GiorniCau(12) = Diurno.Giorno12
            GiorniCau(13) = Diurno.Giorno13
            GiorniCau(14) = Diurno.Giorno14
            GiorniCau(15) = Diurno.Giorno15
            GiorniCau(16) = Diurno.Giorno16
            GiorniCau(17) = Diurno.Giorno17
            GiorniCau(18) = Diurno.Giorno18
            GiorniCau(19) = Diurno.Giorno19
            GiorniCau(20) = Diurno.Giorno20
            GiorniCau(21) = Diurno.Giorno21
            GiorniCau(22) = Diurno.Giorno22
            GiorniCau(23) = Diurno.Giorno23
            GiorniCau(24) = Diurno.Giorno24
            GiorniCau(25) = Diurno.Giorno25
            GiorniCau(26) = Diurno.Giorno26
            GiorniCau(27) = Diurno.Giorno27
            GiorniCau(28) = Diurno.Giorno28
            GiorniCau(29) = Diurno.Giorno29
            GiorniCau(30) = Diurno.Giorno30
            GiorniCau(31) = Diurno.Giorno31

            For I = 1 To 31
                If GiorniCau(I) <> "" And GiorniCau(I) <> "C" And GiorniCau(I) <> "A" Then
                    AssenzaDiurnoNellAnnoEscludiAssenza = AssenzaDiurnoNellAnnoEscludiAssenza + 1
                End If
            Next

        Next



    End Function

    Private Function AssenzaDiurnoNellAnno(ByVal Cserv As String, ByVal CodOsp As Long, ByVal Anno As Long, ByVal Mese As Long, ByVal Giorno As Integer) As Integer
        Dim IndiceMese As Integer
        Dim I As Integer
        Dim GiorniCau(31) As String


        AssenzaDiurnoNellAnno = 0
        For IndiceMese = 1 To Mese - 1
            Dim Diurno As New Cls_Diurno


            Diurno.Leggi(STRINGACONNESSIONEDB, CodOsp, Cserv, Anno, IndiceMese)
            GiorniCau(1) = Diurno.Giorno1
            GiorniCau(2) = Diurno.Giorno2
            GiorniCau(3) = Diurno.Giorno3
            GiorniCau(4) = Diurno.Giorno4
            GiorniCau(5) = Diurno.Giorno5
            GiorniCau(6) = Diurno.Giorno6
            GiorniCau(7) = Diurno.Giorno7
            GiorniCau(8) = Diurno.Giorno8
            GiorniCau(9) = Diurno.Giorno9
            GiorniCau(10) = Diurno.Giorno10
            GiorniCau(11) = Diurno.Giorno11
            GiorniCau(12) = Diurno.Giorno12
            GiorniCau(13) = Diurno.Giorno13
            GiorniCau(14) = Diurno.Giorno14
            GiorniCau(15) = Diurno.Giorno15
            GiorniCau(16) = Diurno.Giorno16
            GiorniCau(17) = Diurno.Giorno17
            GiorniCau(18) = Diurno.Giorno18
            GiorniCau(19) = Diurno.Giorno19
            GiorniCau(20) = Diurno.Giorno20
            GiorniCau(21) = Diurno.Giorno21
            GiorniCau(22) = Diurno.Giorno22
            GiorniCau(23) = Diurno.Giorno23
            GiorniCau(24) = Diurno.Giorno24
            GiorniCau(25) = Diurno.Giorno25
            GiorniCau(26) = Diurno.Giorno26
            GiorniCau(27) = Diurno.Giorno27
            GiorniCau(28) = Diurno.Giorno28
            GiorniCau(29) = Diurno.Giorno29
            GiorniCau(30) = Diurno.Giorno30
            GiorniCau(31) = Diurno.Giorno31

            For I = 1 To 31
                If GiorniCau(I) <> "" And GiorniCau(I) <> "C" Then
                    AssenzaDiurnoNellAnno = AssenzaDiurnoNellAnno + 1
                End If
            Next

        Next



    End Function
    Private Function GiorniAssenzaAnno(ByVal Cserv As String, ByVal CodOsp As Long, ByVal Anno As Long, ByVal Mese As Long, ByVal Giorno As Integer) As Integer
        Dim accolto As Boolean
        GiorniAssenzaAnno = 0

        Dim cmdMovimentiAccolto As New OleDbCommand()

        cmdMovimentiAccolto.CommandText = "Select * From Movimenti Where CODICEOSPITE = " & CodOsp & " And CENTROSERVIZIO = '" & Cserv & "' And DATA < {ts '" & Format(DateSerial(Anno, 1, 1), "yyyy-MM-dd") & " 00:00:00'} order by Data Desc"
        cmdMovimentiAccolto.Connection = OspitiCon
        Dim RDMovimentiAccolto As OleDbDataReader = cmdMovimentiAccolto.ExecuteReader()
        If RDMovimentiAccolto.Read() Then
            accolto = True
        Else
            accolto = False
        End If
        RDMovimentiAccolto.Close()


        Dim InizioAssenza As Date
        Dim cmdMovimenti As New OleDbCommand()

        cmdMovimentiAccolto.CommandText = "Select * From Movimenti Where CODICEOSPITE = " & CodOsp & " And CENTROSERVIZIO = '" & Cserv & "' And DATA >= {ts '" & Format(DateSerial(Anno, 1, 1), "yyyy-MM-dd") & " 00:00:00'} And DATA <= {ts '" & Format(DateSerial(Anno, Mese, Giorno), "yyyy-MM-dd") & " 00:00:00'} order by Data"
        cmdMovimentiAccolto.Connection = OspitiCon
        Dim RDMovimenti As OleDbDataReader = cmdMovimentiAccolto.ExecuteReader()
        Do While RDMovimenti.Read()

            If campodb(RDMovimenti.Item("TIPOMOV")) = "05" Then
                accolto = True
                InizioAssenza = Nothing
            End If
            If campodb(RDMovimenti.Item("TIPOMOV")) = "13" Then
                If Not IsNothing(InizioAssenza) Then
                    GiorniAssenzaAnno = GiorniAssenzaAnno + DateDiff(DateInterval.Day, InizioAssenza, campodbd(RDMovimenti.Item("DATA")))
                End If
                accolto = False
                InizioAssenza = Nothing
            End If

            If campodb(RDMovimenti.Item("TIPOMOV")) = "03" Then
                accolto = True

                Dim TCausale As New Cls_CausaliEntrataUscita

                TCausale.Codice = campodb(RDMovimenti.Item("CAUSALE"))
                TCausale.LeggiCausale(STRINGACONNESSIONEDB)

                If TCausale.TIPOMOVIMENTO = "A" Then
                    InizioAssenza = campodb(RDMovimenti.Item("DATA"))
                Else
                    InizioAssenza = Nothing
                End If
            End If


            If campodb(RDMovimenti.Item("TIPOMOV")) = "04" Then
                If Not IsNothing(InizioAssenza) Then
                    GiorniAssenzaAnno = GiorniAssenzaAnno + DateDiff(DateInterval.Day, InizioAssenza, campodbd(RDMovimenti.Item("DATA")))
                End If
                InizioAssenza = Nothing
            End If


        Loop
        RDMovimenti.Close()
        If Not IsNothing(InizioAssenza) Then
            GiorniAssenzaAnno = GiorniAssenzaAnno + DateDiff(DateInterval.Day, InizioAssenza, DateSerial(Anno, Mese, Giorno))
        End If

    End Function


    Public Sub AnticipoExtraFissi(ByVal CodOsp As Integer, ByVal Cserv As String, ByVal Anno As Integer, ByVal Mese As Integer, ByVal xAnno As Integer, ByVal xMese As Integer, ByVal BloccaConguagli As Integer, ByVal GiornoCalcolati As Integer, ByVal MScriviOspite As Boolean, ByVal FattureNC As Boolean)

        Dim Totale As New Cls_rettatotale
        Dim ClasseRette As New ClassRetteOspiti


        Dim CercaExtra As Integer
        Dim CercaExtraMeseAnt As Integer
        Dim VettoreCE(100) As String
        Dim VettoreRI(100) As String
        Dim Indice As Integer

        ClasseRette.ApriDB(STRINGACONNESSIONEDB)

        Totale.CODICEOSPITE = CodOsp
        Totale.CENTROSERVIZIO = Cserv
        Totale.RettaTotaleAData(STRINGACONNESSIONEDB, Totale.CODICEOSPITE, Totale.CENTROSERVIZIO, DateSerial(xAnno, xMese, 1))

        Dim Extra As New Cls_ExtraFisso

        Extra.CENTROSERVIZIO = Totale.CENTROSERVIZIO
        Extra.CODICEOSPITE = Totale.CODICEOSPITE
        Extra.Data = Totale.Data
        Extra.UltimaDataAData_Multi(STRINGACONNESSIONEDB, VettoreCE, VettoreRI)

        Dim TipoExtra As New Cls_TipoExtraFisso



        Dim cmdLeggiTipoAdd As New OleDbCommand()
        cmdLeggiTipoAdd.CommandText = "Select * From [TABELLAEXTRAFISSI] where [Ripartizione]='O' " ' And (CENTROSERVIZIO is null or CENTROSERVIZIO = '' OR CENTROSERVIZIO= ?) "
        cmdLeggiTipoAdd.Connection = OspitiCon
        'cmdLeggiTipoAdd.Parameters.AddWithValue("@Cserv", Cserv)


        Dim RdTA As OleDbDataReader = cmdLeggiTipoAdd.ExecuteReader()

        Do While RdTA.Read

            TipoExtra.CODICEEXTRA = campodb(RdTA.Item("CodiceExtra"))

            TipoExtra.Leggi(STRINGACONNESSIONEDB)

            If TipoExtra.CODICEEXTRA = "" Or TipoExtra.Ripartizione <> "O" Then
                Dim TipoExtraAttivo As New Cls_TipoExtraFisso

                TipoExtraAttivo.CODICEEXTRA = MyTabCodiceExtrOspite(0)
                TipoExtraAttivo.Leggi(STRINGACONNESSIONEDB)

                If TipoExtraAttivo.Anticipato = "S" Then
                    ImpExtraOspite(0) = 0
                    NonImpExtraOspite(0) = 0
                End If
            End If

            For CercaExtra = 0 To 10
                If MyTabCodiceExtrOspite(CercaExtra) = TipoExtra.CODICEEXTRA Then
                    Exit For
                End If
            Next


            For CercaExtraMeseAnt = 0 To 10
                If VettoreCE(CercaExtraMeseAnt) = TipoExtra.CODICEEXTRA And VettoreRI(CercaExtraMeseAnt) = "O" Then
                    Exit For
                End If
            Next

            If TipoExtra.Ripartizione = "O" And TipoExtra.Anticipato = "S" Then


                If TipoExtra.TipoImporto = "G" Then

                    Dim VerificaExtra As Double = 0
                    Dim cmdRpxO1 As New OleDbCommand()
                    cmdRpxO1.CommandText = "Select * From RetteOspite Where CodiceOspite = " & CodOsp & " And CentroServizio = '" & Cserv & "' And  Elemento Like 'E%' and [CODICEEXTRA] = '" & TipoExtra.CODICEEXTRA & "' And MeseCompetenza = " & Mese & " And AnnoCompetenza = " & Anno
                    cmdRpxO1.Connection = OspitiCon
                    Dim RDRettaRpxO1 As OleDbDataReader = cmdRpxO1.ExecuteReader()
                    If RDRettaRpxO1.Read() Then
                        VerificaExtra = campodb(RDRettaRpxO1.Item("Importo"))
                    End If
                    RDRettaRpxO1.Close()
                    If VerificaExtra = 0 Or FattureNC = True Then


                        Dim LO_CodiceOspite As New ClsOspite

                        LO_CodiceOspite.Leggi(STRINGACONNESSIONEDB, CodOsp)



                        Dim KCs As New Cls_DatiOspiteParenteCentroServizio

                        KCs.CentroServizio = Cserv
                        KCs.CodiceOspite = CodOsp
                        KCs.CodiceParente = 0
                        KCs.Leggi(STRINGACONNESSIONEDB)

                        REM Assegna la tipologia operazione e l'aliquota prendendo quella relativa al cserv
                        If KCs.CodiceOspite <> 0 Then
                            LO_CodiceOspite.TIPOOPERAZIONE = KCs.TipoOperazione
                            LO_CodiceOspite.CODICEIVA = KCs.AliquotaIva
                            LO_CodiceOspite.FattAnticipata = KCs.Anticipata
                            LO_CodiceOspite.MODALITAPAGAMENTO = KCs.ModalitaPagamento
                            LO_CodiceOspite.Compensazione = KCs.Compensazione

                        End If

                        Dim TipoOperazione As New Cls_TipoOperazione
                        Dim ScorporoIVA As Boolean = False

                        TipoOperazione.Codice = LO_CodiceOspite.TIPOOPERAZIONE
                        TipoOperazione.Leggi(STRINGACONNESSIONEDB, TipoOperazione.Codice)

                        If TipoOperazione.SCORPORAIVA = 1 Then
                            ScorporoIVA = True
                        End If



                        Dim GeneraleConc As New OleDbConnection


                        GeneraleConc.ConnectionString = ConnessioneGenerale

                        GeneraleConc.Open()


                        Dim ImportoExtraSuRegistrazione As Double = 0

                        Dim cmdRpxO2 As New OleDbCommand()
                        cmdRpxO2.CommandText = "Select sum(Importo) As TotImporto From MovimentiContabiliRiga As Principale Where  DareAvere = 'A' AND (select count(*) from  MovimentiContabiliRiga as k where k.numero = Principale.numero and rigadacausale = 1 and sottocontopartita = " & CodOsp * 100 & " ) >= 1 And CentroServizio = '" & Cserv & "' And TipoExtra = 'E" & TipoExtra.CODICEEXTRA & "' And  AnnoRiferimento = " & Anno & " And MeseRiferimento = " & Mese
                        cmdRpxO2.Connection = GeneraleConc
                        Dim RDRettaRpxO2 As OleDbDataReader = cmdRpxO2.ExecuteReader()
                        If RDRettaRpxO2.Read() Then
                            ImportoExtraSuRegistrazione = campodbN(RDRettaRpxO2.Item("TotImporto"))
                        End If
                        RDRettaRpxO2.Close()
                        If ImportoExtraSuRegistrazione = 0 Then
                            Dim cmdRpxO2_1 As New OleDbCommand()
                            cmdRpxO2_1.CommandText = "Select sum(Importo) As TotImporto From MovimentiContabiliRiga As Principale Where  DareAvere = 'A' AND (select count(*) from  MovimentiContabiliRiga as k where k.numero = Principale.numero and rigadacausale = 1 and sottocontopartita = " & CodOsp * 100 & " ) >= 1 And CentroServizio = '" & Cserv & "' And TipoExtra = 'E" & TipoExtra.CODICEEXTRA & "' And  (SELECT COUNT(*) FROM MovimentiContabiliTesta  WHERE Principale.Numero = NumeroRegistrazione AND MovimentiContabiliTesta.MeseCompetenza =  " & Mese & " AND   MovimentiContabiliTesta.AnnoCompetenza = " & Anno & ") >= 1  "
                            cmdRpxO2_1.Connection = GeneraleConc
                            Dim RDRettaRpxO2_1 As OleDbDataReader = cmdRpxO2_1.ExecuteReader()
                            If RDRettaRpxO2_1.Read() Then
                                ImportoExtraSuRegistrazione = campodbN(RDRettaRpxO2_1.Item("TotImporto"))
                            End If
                            RDRettaRpxO2_1.Close()
                        End If

                        VerificaExtra = ImportoExtraSuRegistrazione

                        ImportoExtraSuRegistrazione = 0

                        Dim cmdRpxO3 As New OleDbCommand()
                        cmdRpxO3.CommandText = "Select sum(Importo) As TotImporto From MovimentiContabiliRiga As Principale Where  DareAvere = 'D'  AND (select count(*) from  MovimentiContabiliRiga as k where k.numero = Principale.numero and rigadacausale = 1 and sottocontopartita = " & CodOsp * 100 & " ) >= 1 And CentroServizio = '" & Cserv & "' And  TipoExtra = 'E" & TipoExtra.CODICEEXTRA & "' And  AnnoRiferimento = " & Anno & " And MeseRiferimento = " & Mese
                        cmdRpxO3.Connection = GeneraleConc
                        Dim RDRettaRpxO3 As OleDbDataReader = cmdRpxO3.ExecuteReader()
                        If RDRettaRpxO3.Read() Then
                            ImportoExtraSuRegistrazione = campodbN(RDRettaRpxO3.Item("TotImporto"))
                        End If
                        RDRettaRpxO3.Close()
                        If ImportoExtraSuRegistrazione = 0 Then
                            Dim cmdRpxO3_1 As New OleDbCommand()
                            cmdRpxO3_1.CommandText = "Select sum(Importo) As TotImporto From MovimentiContabiliRiga As Principale Where  DareAvere = 'D'  AND (select count(*) from  MovimentiContabiliRiga as k where k.numero = Principale.numero and rigadacausale = 1 and sottocontopartita = " & CodOsp * 100 & " ) >= 1 And CentroServizio = '" & Cserv & "' And  TipoExtra = 'E" & TipoExtra.CODICEEXTRA & "' And  (SELECT COUNT(*) FROM MovimentiContabiliTesta  WHERE Principale.Numero = NumeroRegistrazione AND MovimentiContabiliTesta.MeseCompetenza =  " & Mese & " AND   MovimentiContabiliTesta.AnnoCompetenza = " & Anno & ") >= 1 "
                            cmdRpxO3_1.Connection = GeneraleConc
                            Dim RDRettaRpxO3_1 As OleDbDataReader = cmdRpxO3_1.ExecuteReader()
                            If RDRettaRpxO3_1.Read() Then
                                ImportoExtraSuRegistrazione = campodbN(RDRettaRpxO3_1.Item("TotImporto"))
                            End If
                            RDRettaRpxO3_1.Close()
                        End If

                        VerificaExtra = VerificaExtra - ImportoExtraSuRegistrazione


                        If ScorporoIVA = True And FattureNC = True Then
                            Dim CodIVA As New Cls_IVA

                            CodIVA.Codice = TipoExtra.CodiceIva
                            CodIVA.Leggi(ConnessioneTabelle, CodIVA.Codice)

                            VerificaExtra = VerificaExtra + Modulo.MathRound(VerificaExtra * CodIVA.Aliquota, 2)
                        End If
                        GeneraleConc.Close()
                    End If

                    If CercaExtra <> 11 Then
                        If Math.Round(VerificaExtra, 2) <> Math.Round(ImpExtraOspite(CercaExtra) + NonImpExtraOspite(CercaExtra), 2) Then
                            If NonImpExtraOspite(CercaExtra) > 0 And ImpExtraOspite(CercaExtra) > 0 Then
                                NonImpExtraOspite(CercaExtra) = NonImpExtraOspite(CercaExtra) + ImpExtraOspite(CercaExtra) - VerificaExtra
                                ImpExtraOspite(CercaExtra) = 0
                            Else
                                If ImpExtraOspite(CercaExtra) > 0 Then
                                    ImpExtraOspite(CercaExtra) = ImpExtraOspite(CercaExtra) - VerificaExtra
                                End If
                                If NonImpExtraOspite(CercaExtra) > 0 Then
                                    NonImpExtraOspite(CercaExtra) = NonImpExtraOspite(CercaExtra) - VerificaExtra
                                End If
                            End If

                            If ImpExtraOspite(CercaExtra) = 0 And NonImpExtraOspite(CercaExtra) = 0 And VerificaExtra > 0 Then
                                ImpExtraOspite(CercaExtra) = ImpExtraOspite(CercaExtra) - VerificaExtra
                            End If

                            If BloccaConguagli = 1 Then
                                ImpExtraOspite(CercaExtra) = 0
                                NonImpExtraOspite(CercaExtra) = 0
                            End If
                        Else
                            ImpExtraOspite(CercaExtra) = 0
                            NonImpExtraOspite(CercaExtra) = 0
                        End If
                        Else

                            If VerificaExtra > 0 Then
                                Dim ExtraLibero As Integer

                                For ExtraLibero = 1 To 10
                                    If MyTabCodiceExtrOspite(ExtraLibero) = "0" Then
                                        Exit For
                                    End If
                                Next
                                MyTabCodiceExtrOspite(ExtraLibero) = TipoExtra.CODICEEXTRA
                                ImpExtraOspite(ExtraLibero) = VerificaExtra * -1
                            End If
                        End If
                        If GiornoCalcolati > 0 And CercaExtraMeseAnt <> 11 Then
                            ClasseRette.Pulisci()
                            ClasseRette.CentroServizio = Cserv
                            ClasseRette.CodiceOspite = CodOsp
                            ClasseRette.Mese = Mese
                            ClasseRette.Anno = Anno
                            ClasseRette.ANNOCOMPETENZA = xAnno
                            ClasseRette.MESECOMPETENZA = xMese
                            ClasseRette.STATOCONTABILE = "A"
                            ClasseRette.ELEMENTO = "E" & Format(TipoExtra.CODICEEXTRA, "00")
                            ClasseRette.Giorni = GiornoCalcolati
                            ClasseRette.Importo = Modulo.MathRound(TipoExtra.IMPORTO * GiornoCalcolati, 4)
                            ClasseRette.CODICEEXTRA = TipoExtra.CODICEEXTRA
                            If MScriviOspite = True Then ClasseRette.ScriviRettaOspite()
                        End If


                    End If
                    If CercaExtraMeseAnt <> 11 Then
                        If TipoExtra.TipoImporto = "M" Then
                        Dim ScorriExtraMese As Integer
                        For ScorriExtraMese = 0 To 10
                            If TipoExtra.CODICEEXTRA = MyTabCodiceExtrOspite(ScorriExtraMese) Or MyTabCodiceExtrOspite(ScorriExtraMese) = "" Then
                                ImpExtraOspite(ScorriExtraMese) = 0
                                NonImpExtraOspite(ScorriExtraMese) = 0
                            End If
                        Next


                        If GiornoCalcolati > 0 Then
                            ClasseRette.Pulisci()
                            ClasseRette.CentroServizio = Cserv
                            ClasseRette.CodiceOspite = CodOsp
                            ClasseRette.Mese = Mese
                            ClasseRette.Anno = Anno
                            ClasseRette.ANNOCOMPETENZA = xAnno
                            ClasseRette.MESECOMPETENZA = xMese
                            ClasseRette.STATOCONTABILE = "A"
                            ClasseRette.ELEMENTO = "E" & Format(TipoExtra.CODICEEXTRA, "00")
                            ClasseRette.Giorni = 1
                            ClasseRette.Importo = Modulo.MathRound(TipoExtra.IMPORTO, 4)
                            ClasseRette.CODICEEXTRA = TipoExtra.CODICEEXTRA
                            If MScriviOspite = True Then ClasseRette.ScriviRettaOspite()
                        End If
                    End If
                End If
            End If
        Loop

        ClasseRette.ChiudiDB()
    End Sub


    Public Sub AnticipoExtraFissiParente(ByVal CodOsp As Integer, ByVal Par As Integer, ByVal Cserv As String, ByVal Anno As Integer, ByVal Mese As Integer, ByVal xAnno As Integer, ByVal xMese As Integer, ByVal BloccaConguagli As Integer, ByVal GiornoCalcolati As Integer, ByVal MScriviOspite As Boolean, ByVal Calcola As Double, ByVal FattureNC As Boolean)
        Dim ClasseRette As New ClassRetteOspiti
        Dim CercaExtra As Integer
        Dim CercaExtraMeseAnt As Integer
        Dim VettoreCE(100) As String
        Dim VettoreRI(100) As String

        ClasseRette.ApriDB(STRINGACONNESSIONEDB)


        Dim PercentualeParente As New Cls_ImportoParente

        PercentualeParente.CODICEOSPITE = CodOsp
        PercentualeParente.CENTROSERVIZIO = Cserv
        PercentualeParente.CODICEPARENTE = Par
        PercentualeParente.UltimaData(STRINGACONNESSIONEDB, CodOsp, Cserv, Par)


        Dim Totale As New Cls_rettatotale

        Totale.CODICEOSPITE = CodOsp
        Totale.CENTROSERVIZIO = Cserv
        Totale.RettaTotaleAData(STRINGACONNESSIONEDB, Totale.CODICEOSPITE, Totale.CENTROSERVIZIO, DateSerial(xAnno, xMese, 1))

        Dim TipoExtra As New Cls_TipoExtraFisso


        Dim Extra As New Cls_ExtraFisso

        Extra.CENTROSERVIZIO = Totale.CENTROSERVIZIO
        Extra.CODICEOSPITE = Totale.CODICEOSPITE
        Extra.Data = Totale.Data
        Extra.UltimaDataAData_Multi(STRINGACONNESSIONEDB, VettoreCE, VettoreRI)


        Dim cmdLeggiTipoAdd As New OleDbCommand()
        cmdLeggiTipoAdd.CommandText = "Select * From [TABELLAEXTRAFISSI] where [Ripartizione]='P' " '  And (CENTROSERVIZIO is null or CENTROSERVIZIO = '' OR CENTROSERVIZIO= ?) "
        cmdLeggiTipoAdd.Connection = OspitiCon
        'cmdLeggiTipoAdd.Parameters.AddWithValue("@Cserv", Cserv)

        Dim RdTA As OleDbDataReader = cmdLeggiTipoAdd.ExecuteReader()

        Do While RdTA.Read


            TipoExtra.CODICEEXTRA = campodb(RdTA.Item("CodiceExtra"))
            TipoExtra.Leggi(STRINGACONNESSIONEDB)


            'If TipoExtra.CODICEEXTRA = "" Or TipoExtra.Ripartizione <> "P" Then
            '    Dim TipoExtraAttivo As New Cls_TipoExtraFisso

            '    TipoExtraAttivo.CODICEEXTRA = MyTabCodiceExtrParent(1)
            '    TipoExtraAttivo.Leggi(STRINGACONNESSIONEDB)

            '    If TipoExtraAttivo.Anticipato = "S" Then
            '        ImpExtraParente(Par, 0) = 0
            '        NonImpExtraParente(Par, 0) = 0
            '        ImpExtraParente(Par, 1) = 0
            '        NonImpExtraParente(Par, 1) = 0
            '    End If
            'End If
            If TipoExtra.CODICEEXTRA = "31" Then
                TipoExtra.CODICEEXTRA = "31"
            End If


            For CercaExtra = 0 To 10
                If MyTabCodiceExtrParent(CercaExtra) = TipoExtra.CODICEEXTRA Then
                    Exit For
                End If
            Next


            For CercaExtraMeseAnt = 0 To 10
                If VettoreCE(CercaExtraMeseAnt) = TipoExtra.CODICEEXTRA And VettoreRI(CercaExtraMeseAnt) = "P" Then
                    Exit For
                End If
            Next


            If TipoExtra.Ripartizione = "P" And TipoExtra.Anticipato = "S" Then


                If TipoExtra.TipoImporto = "G" Then

                    Dim VerificaExtra As Double = 0
                    Dim cmdRpxO1 As New OleDbCommand()
                    cmdRpxO1.CommandText = "Select sum(Importo) As TotImporto From RETTEPARENTE Where CodiceOspite = " & CodOsp & " And CentroServizio = '" & Cserv & "' And  CODICEPARENTE =  " & Par & " And Elemento Like 'E%' And MeseCompetenza = " & Mese & " And AnnoCompetenza = " & Anno & " and [CODICEEXTRA] = '" & TipoExtra.CODICEEXTRA & "'"
                    cmdRpxO1.Connection = OspitiCon
                    Dim RDRettaRpxO1 As OleDbDataReader = cmdRpxO1.ExecuteReader()
                    If RDRettaRpxO1.Read() Then
                        VerificaExtra = campodbN(RDRettaRpxO1.Item("TotImporto"))
                    End If
                    RDRettaRpxO1.Close()

                    If VerificaExtra = 0 Or FattureNC = True Then


                        Dim LO_CodiceParente As New Cls_Parenti

                        LO_CodiceParente.Leggi(STRINGACONNESSIONEDB, CodOsp, Par)



                        Dim KCs As New Cls_DatiOspiteParenteCentroServizio

                        KCs.CentroServizio = Cserv
                        KCs.CodiceOspite = CodOsp
                        KCs.CodiceParente = 0
                        KCs.Leggi(STRINGACONNESSIONEDB)

                        REM Assegna la tipologia operazione e l'aliquota prendendo quella relativa al cserv
                        If KCs.CodiceOspite <> 0 Then
                            LO_CodiceParente.TIPOOPERAZIONE = KCs.TipoOperazione
                            LO_CodiceParente.CODICEIVA = KCs.AliquotaIva
                            LO_CodiceParente.FattAnticipata = KCs.Anticipata
                            LO_CodiceParente.MODALITAPAGAMENTO = KCs.ModalitaPagamento
                            LO_CodiceParente.Compensazione = KCs.Compensazione

                        End If

                        Dim TipoOperazione As New Cls_TipoOperazione
                        Dim ScorporoIVA As Boolean = False

                        TipoOperazione.Codice = LO_CodiceParente.TIPOOPERAZIONE
                        TipoOperazione.Leggi(STRINGACONNESSIONEDB, TipoOperazione.Codice)

                        If TipoOperazione.SCORPORAIVA = 1 Then
                            ScorporoIVA = True
                        End If



                        Dim GeneraleConc As New OleDbConnection


                        GeneraleConc.ConnectionString = ConnessioneGenerale

                        GeneraleConc.Open()

                        Dim ImportoExtraSuRegistrazione As Double = 0


                        Dim cmdRpxO2 As New OleDbCommand()
                        cmdRpxO2.CommandText = "Select sum(Importo) As TotImporto From MovimentiContabiliRiga as Principale Where  DareAvere = 'A' AND (select count(*) from  MovimentiContabiliRiga as k where k.numero = Principale.numero and rigadacausale = 1 and sottocontopartita = " & (CodOsp * 100) + Par & " ) >= 1 And CentroServizio = '" & Cserv & "' And TipoExtra = 'E" & TipoExtra.CODICEEXTRA & "' And  MeseRiferimento = " & Mese & " And AnnoRiferimento = " & Anno
                        cmdRpxO2.Connection = GeneraleConc
                        Dim RDRettaRpxO2 As OleDbDataReader = cmdRpxO2.ExecuteReader()
                        If RDRettaRpxO2.Read() Then
                            ImportoExtraSuRegistrazione = campodbN(RDRettaRpxO2.Item("TotImporto"))
                        End If
                        RDRettaRpxO2.Close()
                        If ImportoExtraSuRegistrazione = 0 Then

                            Dim cmdRpxO2_1 As New OleDbCommand()
                            cmdRpxO2_1.CommandText = "Select sum(Importo) As TotImporto From MovimentiContabiliRiga as Principale Where  DareAvere = 'A' AND (select count(*) from  MovimentiContabiliRiga as k where k.numero = Principale.numero and rigadacausale = 1 and sottocontopartita = " & (CodOsp * 100) + Par & " ) >= 1 And CentroServizio = '" & Cserv & "' And TipoExtra = 'E" & TipoExtra.CODICEEXTRA & "' And  (SELECT COUNT(*) FROM MovimentiContabiliTesta  WHERE Principale.Numero = NumeroRegistrazione AND MovimentiContabiliTesta.MeseCompetenza =  " & Mese & " AND   MovimentiContabiliTesta.AnnoCompetenza = " & Anno & ") >= 1  "
                            cmdRpxO2_1.Connection = GeneraleConc
                            Dim RDRettaRpxO2_1 As OleDbDataReader = cmdRpxO2_1.ExecuteReader()
                            If RDRettaRpxO2_1.Read() Then
                                ImportoExtraSuRegistrazione = campodbN(RDRettaRpxO2_1.Item("TotImporto"))
                            End If
                            RDRettaRpxO2_1.Close()
                        End If


                        VerificaExtra = ImportoExtraSuRegistrazione

                        ImportoExtraSuRegistrazione = 0

                        Dim cmdRpxO3 As New OleDbCommand()
                        cmdRpxO3.CommandText = "Select sum(Importo) As TotImporto  From MovimentiContabiliRiga as Principale Where  DareAvere = 'D'  AND (select count(*) from  MovimentiContabiliRiga as k where k.numero = Principale.numero and rigadacausale = 1 and sottocontopartita = " & (CodOsp * 100) + Par & " ) >= 1 And CentroServizio = '" & Cserv & "' And TipoExtra = 'E" & TipoExtra.CODICEEXTRA & "' And  MeseRiferimento = " & Mese & " And AnnoRiferimento = " & Anno
                        cmdRpxO3.Connection = GeneraleConc
                        Dim RDRettaRpxO3 As OleDbDataReader = cmdRpxO3.ExecuteReader()
                        If RDRettaRpxO3.Read() Then
                            ImportoExtraSuRegistrazione = campodbN(RDRettaRpxO3.Item("TotImporto"))
                        End If
                        RDRettaRpxO3.Close()
                        If ImportoExtraSuRegistrazione = 0 Then
                            Dim cmdRpxO3_1 As New OleDbCommand()
                            cmdRpxO3_1.CommandText = "Select sum(Importo) As TotImporto  From MovimentiContabiliRiga as Principale Where  DareAvere = 'D'  AND (select count(*) from  MovimentiContabiliRiga as k where k.numero = Principale.numero and rigadacausale = 1 and sottocontopartita = " & (CodOsp * 100) + Par & " ) >= 1 And CentroServizio = '" & Cserv & "' And  TipoExtra = 'E" & TipoExtra.CODICEEXTRA & "' And  (SELECT COUNT(*) FROM MovimentiContabiliTesta  WHERE Principale.Numero = NumeroRegistrazione AND MovimentiContabiliTesta.MeseCompetenza =  " & Mese & " AND   MovimentiContabiliTesta.AnnoCompetenza = " & Anno & ") >= 1 "
                            cmdRpxO3_1.Connection = GeneraleConc
                            Dim RDRettaRpxO3_1 As OleDbDataReader = cmdRpxO3_1.ExecuteReader()
                            If RDRettaRpxO3_1.Read() Then
                                ImportoExtraSuRegistrazione = campodbN(RDRettaRpxO3_1.Item("TotImporto"))
                            End If
                            RDRettaRpxO3_1.Close()
                        End If
                        GeneraleConc.Close()
                        VerificaExtra = VerificaExtra - ImportoExtraSuRegistrazione

                        If ScorporoIVA = True And FattureNC = True Then
                            Dim CodIVA As New Cls_IVA

                            CodIVA.Codice = TipoExtra.CodiceIva
                            CodIVA.Leggi(ConnessioneTabelle, CodIVA.Codice)

                            VerificaExtra = VerificaExtra + Modulo.MathRound(VerificaExtra * CodIVA.Aliquota, 2)
                        End If


                    End If

                    If CercaExtra <> 11 Then
                        If Math.Round(VerificaExtra, 2) <> Math.Round(ImpExtraParente(Par, CercaExtra) + NonImpExtraParente(Par, CercaExtra), 2) Then
                            If ImpExtraParente(Par, CercaExtra) > 0 Then
                                ImpExtraParente(Par, CercaExtra) = ImpExtraParente(Par, CercaExtra) - VerificaExtra
                            End If
                            If NonImpExtraParente(Par, CercaExtra) > 0 Then
                                NonImpExtraParente(Par, CercaExtra) = NonImpExtraParente(Par, CercaExtra) - VerificaExtra
                            End If


                            If ImpExtraParente(Par, CercaExtra) = 0 And NonImpExtraParente(Par, CercaExtra) = 0 And VerificaExtra > 0 Then
                                ImpExtraParente(Par, CercaExtra) = ImpExtraParente(Par, CercaExtra) - VerificaExtra
                            End If


                            If BloccaConguagli = 1 Then
                                ImpExtraParente(Par, CercaExtra) = 0
                                NonImpExtraParente(Par, CercaExtra) = 0
                            End If
                        Else
                            ImpExtraParente(Par, CercaExtra) = 0
                            NonImpExtraParente(Par, CercaExtra) = 0
                        End If
                    Else
                        If VerificaExtra > 0 Then
                            Dim ExtraLibero As Integer

                            For ExtraLibero = 1 To 10
                                If MyTabCodiceExtrParent(ExtraLibero) = "0" Then
                                    Exit For
                                End If
                            Next
                            MyTabCodiceExtrParent(ExtraLibero) = TipoExtra.CODICEEXTRA
                            ImpExtraParente(Par, ExtraLibero) = VerificaExtra * -1
                        End If
                    End If

                    If GiornoCalcolati > 0 And PercentualeParente.PERCENTUALE > 0 And CercaExtraMeseAnt <> 11 Then
                        ClasseRette.Pulisci()
                        ClasseRette.CentroServizio = Cserv
                        ClasseRette.CodiceOspite = CodOsp
                        ClasseRette.CODICEPARENTE = Par
                        ClasseRette.Mese = Mese
                        ClasseRette.Anno = Anno
                        ClasseRette.ANNOCOMPETENZA = xAnno
                        ClasseRette.MESECOMPETENZA = xMese
                        ClasseRette.STATOCONTABILE = "A"
                        ClasseRette.ELEMENTO = "E" & Format(TipoExtra.CODICEEXTRA, "00")
                        ClasseRette.Giorni = GiornoCalcolati
                        ClasseRette.Importo = Modulo.MathRound(Math.Round(TipoExtra.IMPORTO * PercentualeParente.PERCENTUALE, 2) * GiornoCalcolati, 4)
                        ClasseRette.CODICEEXTRA = TipoExtra.CODICEEXTRA
                        If MScriviOspite = True Then ClasseRette.ScriviRettaParente()
                    End If
                End If
                If CercaExtraMeseAnt <> 11 Then
                    If TipoExtra.TipoImporto = "M" Then
                        If CercaExtra < 11 Then
                            ImpExtraParente(Par, CercaExtra) = 0
                            NonImpExtraParente(Par, CercaExtra) = 0
                        End If

                        If GiornoCalcolati > 0 And (ImportoPresParente(Par) > 0 Or NonImportoPresParente(Par) > 0) Then
                            ClasseRette.Pulisci()
                            ClasseRette.CentroServizio = Cserv
                            ClasseRette.CodiceOspite = CodOsp
                            ClasseRette.CODICEPARENTE = Par
                            ClasseRette.Mese = Mese
                            ClasseRette.Anno = Anno
                            ClasseRette.ANNOCOMPETENZA = xAnno
                            ClasseRette.MESECOMPETENZA = xMese
                            ClasseRette.STATOCONTABILE = "A"
                            ClasseRette.ELEMENTO = "E" & Format(TipoExtra.CODICEEXTRA, "00")
                            ClasseRette.Giorni = 1
                            ClasseRette.Importo = Modulo.MathRound(TipoExtra.IMPORTO, 4)
                            ClasseRette.CODICEEXTRA = TipoExtra.CODICEEXTRA
                            If MScriviOspite = True Then ClasseRette.ScriviRettaParente()
                        End If
                    End If
                End If
            End If
        Loop

        ClasseRette.ChiudiDB()

    End Sub

    Public Sub AnticipoExtraFissiParenteOld(ByVal CodOsp As Integer, ByVal Par As Integer, ByVal Cserv As String, ByVal Anno As Integer, ByVal Mese As Integer, ByVal xAnno As Integer, ByVal xMese As Integer, ByVal BloccaConguagli As Integer, ByVal GiornoCalcolati As Integer, ByVal MScriviOspite As Boolean, ByVal Calcola As Double)
        Dim ClasseRette As New ClassRetteOspiti

        ClasseRette.ApriDB(STRINGACONNESSIONEDB)


        Dim Totale As New Cls_rettatotale

        Totale.CODICEOSPITE = CodOsp
        Totale.CENTROSERVIZIO = Cserv
        Totale.RettaTotaleAData(STRINGACONNESSIONEDB, Totale.CODICEOSPITE, Totale.CENTROSERVIZIO, DateSerial(xAnno, xMese, 1))

        Dim TipoExtra As New Cls_TipoExtraFisso

        If Year(Totale.Data) > 1 Then
            Dim Extra As New Cls_ExtraFisso

            Extra.CENTROSERVIZIO = Totale.CENTROSERVIZIO
            Extra.CODICEOSPITE = Totale.CODICEOSPITE
            Extra.Data = Totale.Data
            Extra.UltimaDataAData(STRINGACONNESSIONEDB)



            TipoExtra.CODICEEXTRA = Extra.CODICEEXTRA
            TipoExtra.Leggi(STRINGACONNESSIONEDB)


            If TipoExtra.CODICEEXTRA = "" Or TipoExtra.Ripartizione <> "P" Then
                Dim TipoExtraAttivo As New Cls_TipoExtraFisso

                TipoExtraAttivo.CODICEEXTRA = MyTabCodiceExtrParent(1)
                TipoExtraAttivo.Leggi(STRINGACONNESSIONEDB)

                If TipoExtraAttivo.Anticipato = "S" Then
                    ImpExtraParente(Par, 0) = 0
                    NonImpExtraParente(Par, 0) = 0
                    ImpExtraParente(Par, 1) = 0
                    NonImpExtraParente(Par, 1) = 0
                End If
            End If
        Else
            TipoExtra.Ripartizione = ""
            TipoExtra.Anticipato = ""
        End If
        If TipoExtra.Ripartizione = "P" And TipoExtra.Anticipato = "S" Then


            If TipoExtra.TipoImporto = "G" Then

                Dim VerificaExtra As Double
                Dim cmdRpxO1 As New OleDbCommand()
                cmdRpxO1.CommandText = "Select * From RETTEPARENTE Where CodiceOspite = " & CodOsp & " And CentroServizio = '" & Cserv & "' And  CODICEPARENTE =  " & Par & " And Elemento Like 'E%' And MeseCompetenza = " & Mese & " And AnnoCompetenza = " & Anno
                cmdRpxO1.Connection = OspitiCon
                Dim RDRettaRpxO1 As OleDbDataReader = cmdRpxO1.ExecuteReader()
                If RDRettaRpxO1.Read() Then
                    VerificaExtra = campodb(RDRettaRpxO1.Item("Importo"))
                End If
                RDRettaRpxO1.Close()

                If VerificaExtra <> ImpExtraParente(Par, 0) + NonImpExtraParente(Par, 0) + ImpExtraParente(Par, 1) + NonImpExtraParente(Par, 1) + ImpExtraParente(Par, 2) + NonImpExtraParente(Par, 2) + ImpExtraParente(Par, 3) + NonImpExtraParente(Par, 3) Then
                    If ImpExtraParente(Par, 0) > 0 Then
                        ImpExtraParente(Par, 0) = ImpExtraParente(Par, 0) - VerificaExtra
                    End If
                    If NonImpExtraParente(Par, 0) > 0 Then
                        NonImpExtraParente(Par, 0) = NonImpExtraParente(Par, 0) - VerificaExtra
                    End If
                    If ImpExtraParente(Par, 1) > 0 Then
                        ImpExtraParente(Par, 1) = ImpExtraParente(Par, 1) - VerificaExtra
                    End If
                    If NonImpExtraParente(Par, 1) > 0 Then
                        NonImpExtraParente(Par, 1) = NonImpExtraParente(Par, 1) - VerificaExtra
                    End If
                    If BloccaConguagli = 1 Then
                        ImpExtraParente(Par, 0) = 0
                        NonImpExtraParente(Par, 0) = 0
                        ImpExtraParente(Par, 1) = 0
                        NonImpExtraParente(Par, 1) = 0
                        ImpExtraParente(Par, 2) = 0
                        NonImpExtraParente(Par, 2) = 0
                        ImpExtraParente(Par, 3) = 0
                        NonImpExtraParente(Par, 3) = 0
                    End If
                Else
                    ImpExtraParente(Par, 0) = 0
                    NonImpExtraParente(Par, 0) = 0
                    ImpExtraParente(Par, 1) = 0
                    NonImpExtraParente(Par, 1) = 0
                    ImpExtraParente(Par, 2) = 0
                    NonImpExtraParente(Par, 2) = 0
                    ImpExtraParente(Par, 3) = 0
                    NonImpExtraParente(Par, 3) = 0
                End If

                If GiornoCalcolati > 0 And Calcola > 0 Then
                    ClasseRette.Pulisci()
                    ClasseRette.CentroServizio = Cserv
                    ClasseRette.CodiceOspite = CodOsp
                    ClasseRette.CODICEPARENTE = Par
                    ClasseRette.Mese = Mese
                    ClasseRette.Anno = Anno
                    ClasseRette.ANNOCOMPETENZA = xAnno
                    ClasseRette.MESECOMPETENZA = xMese
                    ClasseRette.STATOCONTABILE = "A"
                    ClasseRette.ELEMENTO = "E" & Format(TipoExtra.CODICEEXTRA, "00")
                    ClasseRette.Giorni = GiornoCalcolati
                    ClasseRette.Importo = Modulo.MathRound(TipoExtra.IMPORTO * GiornoCalcolati, 4)
                    ClasseRette.CODICEEXTRA = TipoExtra.CODICEEXTRA
                    If MScriviOspite = True Then ClasseRette.ScriviRettaParente()
                End If
            End If
            If TipoExtra.TipoImporto = "M" Then
                ImpExtraOspite(1) = 0
                ImpExtraOspite(0) = 0
                NonImpExtraOspite(1) = 0
                NonImpExtraOspite(0) = 0

                If GiornoCalcolati > 0 And (ImportoPresParente(Par) > 0 Or NonImportoPresParente(Par) > 0) Then
                    ClasseRette.Pulisci()
                    ClasseRette.CentroServizio = Cserv
                    ClasseRette.CodiceOspite = CodOsp
                    ClasseRette.CODICEPARENTE = Par
                    ClasseRette.Mese = Mese
                    ClasseRette.Anno = Anno
                    ClasseRette.ANNOCOMPETENZA = xAnno
                    ClasseRette.MESECOMPETENZA = xMese
                    ClasseRette.STATOCONTABILE = "A"
                    ClasseRette.ELEMENTO = "E" & Format(TipoExtra.CODICEEXTRA, "00")
                    ClasseRette.Giorni = 1
                    ClasseRette.Importo = Modulo.MathRound(TipoExtra.IMPORTO, 4)
                    ClasseRette.CODICEEXTRA = TipoExtra.CODICEEXTRA
                    If MScriviOspite = True Then ClasseRette.ScriviRettaParente()
                End If
            End If
        End If

        ClasseRette.ChiudiDB()

    End Sub

    Private Sub Old_AnticipoExtraFissi(ByVal CodOsp As Integer, ByVal Cserv As String, ByVal Anno As Integer, ByVal Mese As Integer, ByVal xAnno As Integer, ByVal xMese As Integer, ByVal BloccaConguagli As Integer, ByVal GiornoCalcolati As Integer, ByVal MScriviOspite As Boolean)
        Dim Totale As New Cls_rettatotale
        Dim ClasseRette As New ClassRetteOspiti

        ClasseRette.ApriDB(STRINGACONNESSIONEDB)

        Totale.CODICEOSPITE = CodOsp
        Totale.CENTROSERVIZIO = Cserv
        Totale.RettaTotaleAData(STRINGACONNESSIONEDB, Totale.CODICEOSPITE, Totale.CENTROSERVIZIO, DateSerial(xAnno, xMese, 1))

        Dim TipoExtra As New Cls_TipoExtraFisso

        If Year(Totale.Data) > 1 Then
            Dim Extra As New Cls_ExtraFisso

            Extra.CENTROSERVIZIO = Totale.CENTROSERVIZIO
            Extra.CODICEOSPITE = Totale.CODICEOSPITE
            Extra.Data = Totale.Data
            Extra.UltimaDataAData(STRINGACONNESSIONEDB)



            TipoExtra.CODICEEXTRA = Extra.CODICEEXTRA
            TipoExtra.Leggi(STRINGACONNESSIONEDB)

            If TipoExtra.CODICEEXTRA = "" Or TipoExtra.Ripartizione <> "O" Then
                Dim TipoExtraAttivo As New Cls_TipoExtraFisso

                TipoExtraAttivo.CODICEEXTRA = MyTabCodiceExtrOspite(0)
                TipoExtraAttivo.Leggi(STRINGACONNESSIONEDB)

                If TipoExtraAttivo.Anticipato = "S" Then
                    ImpExtraOspite(0) = 0
                    NonImpExtraOspite(0) = 0
                End If
            End If
        Else
            TipoExtra.Ripartizione = ""
            TipoExtra.Anticipato = ""
        End If

        If TipoExtra.Ripartizione = "O" And TipoExtra.Anticipato = "S" Then


            If TipoExtra.TipoImporto = "G" Then

                Dim VerificaExtra As Double
                Dim cmdRpxO1 As New OleDbCommand()
                cmdRpxO1.CommandText = "Select * From RetteOspite Where CodiceOspite = " & CodOsp & " And CentroServizio = '" & Cserv & "' And  Elemento Like 'E%' And MeseCompetenza = " & Mese & " And AnnoCompetenza = " & Anno
                cmdRpxO1.Connection = OspitiCon
                Dim RDRettaRpxO1 As OleDbDataReader = cmdRpxO1.ExecuteReader()
                If RDRettaRpxO1.Read() Then
                    VerificaExtra = campodb(RDRettaRpxO1.Item("Importo"))
                End If
                RDRettaRpxO1.Close()


                If VerificaExtra <> ImpExtraOspite(0) + NonImpExtraOspite(0) + ImpExtraOspite(1) + NonImpExtraOspite(1) + ImpExtraOspite(2) + NonImpExtraOspite(2) Then
                    If ImpExtraOspite(0) > 0 Then
                        ImpExtraOspite(0) = ImpExtraOspite(0) - VerificaExtra
                    End If
                    If NonImpExtraOspite(0) > 0 Then
                        NonImpExtraOspite(0) = NonImpExtraOspite(0) - VerificaExtra
                    End If

                    If BloccaConguagli = 1 Then
                        ImpExtraOspite(0) = 0
                        NonImpExtraOspite(0) = 0
                        ImpExtraOspite(1) = 0
                        NonImpExtraOspite(1) = 0
                        ImpExtraOspite(2) = 0
                        NonImpExtraOspite(2) = 0
                    End If
                Else
                    ImpExtraOspite(0) = 0
                    NonImpExtraOspite(0) = 0
                    ImpExtraOspite(1) = 0
                    NonImpExtraOspite(1) = 0
                    ImpExtraOspite(2) = 0
                    NonImpExtraOspite(2) = 0
                End If


                If GiornoCalcolati > 0 Then
                    ClasseRette.Pulisci()
                    ClasseRette.CentroServizio = Cserv
                    ClasseRette.CodiceOspite = CodOsp
                    ClasseRette.Mese = Mese
                    ClasseRette.Anno = Anno
                    ClasseRette.ANNOCOMPETENZA = xAnno
                    ClasseRette.MESECOMPETENZA = xMese
                    ClasseRette.STATOCONTABILE = "A"
                    ClasseRette.ELEMENTO = "E" & Format(TipoExtra.CODICEEXTRA, "00")
                    ClasseRette.Giorni = GiornoCalcolati
                    ClasseRette.Importo = Modulo.MathRound(TipoExtra.IMPORTO * GiornoCalcolati, 4)
                    ClasseRette.CODICEEXTRA = TipoExtra.CODICEEXTRA
                    If MScriviOspite = True Then ClasseRette.ScriviRettaOspite()
                End If
            End If
            If TipoExtra.TipoImporto = "M" Then
                ImpExtraOspite(1) = 0
                ImpExtraOspite(0) = 0
                NonImpExtraOspite(1) = 0
                NonImpExtraOspite(0) = 0

                If GiornoCalcolati > 0 Then
                    ClasseRette.Pulisci()
                    ClasseRette.CentroServizio = Cserv
                    ClasseRette.CodiceOspite = CodOsp
                    ClasseRette.Mese = Mese
                    ClasseRette.Anno = Anno
                    ClasseRette.ANNOCOMPETENZA = xAnno
                    ClasseRette.MESECOMPETENZA = xMese
                    ClasseRette.STATOCONTABILE = "A"
                    ClasseRette.ELEMENTO = "E" & Format(TipoExtra.CODICEEXTRA, "00")
                    ClasseRette.Giorni = 1
                    ClasseRette.Importo = Modulo.MathRound(TipoExtra.IMPORTO, 4)
                    ClasseRette.CODICEEXTRA = TipoExtra.CODICEEXTRA
                    If MScriviOspite = True Then ClasseRette.ScriviRettaOspite()
                End If
            End If
        End If

        ClasseRette.ChiudiDB()
    End Sub


    Public Sub CreaAddebitiDomicliari(ByVal Codice As String, ByVal Minuti As Integer, ByRef VettoreTipo() As String, ByRef VettoreCodice() As String, ByRef VettoreDescrizione() As String, ByRef VettoreImporto() As Double, ByRef MaxAddebito As Integer, ByVal CreatoDocumentoOspiti As Integer, ByVal Cserv As String, ByVal CodOsp As Integer, ByVal Data As Date, ByVal NumeroOperatori As Integer, ByVal PrimoIngresso As Boolean, ByVal ID As Integer, ByRef VettoreQuantita() As Double, ByVal FasciaOraria As Integer)
        Dim X As Integer

        Dim TipoDomiciliare As New Cls_TipoDomiciliare

        TipoDomiciliare.Leggi(STRINGACONNESSIONEDB, Codice)



        Dim cmdVerificaTA As New OleDbCommand()
        cmdVerificaTA.CommandText = ("select * from TipoDomiciliare_Addebiti where " & _
                           "CodiceTipoDomiciliare = ? ")
        cmdVerificaTA.Parameters.AddWithValue("@CodiceTipoDomiciliare", Codice)
        cmdVerificaTA.Connection = OspitiCon
        Dim ReadVerificaTA As OleDbDataReader = cmdVerificaTA.ExecuteReader()
        Do While ReadVerificaTA.Read
            Dim InserisciAdAC As Boolean = True
            If campodb(ReadVerificaTA.Item("TipoRetta")) <> "" Then
                Dim Ospite As New Cls_rettatotale

                Ospite.CENTROSERVIZIO = Cserv
                Ospite.CODICEOSPITE = CodOsp
                Ospite.Data = Data
                Ospite.UltimaData(STRINGACONNESSIONEDB, Ospite.CODICEOSPITE, Ospite.CENTROSERVIZIO)
                If campodb(ReadVerificaTA.Item("TipoRetta")) <> Ospite.TipoRetta Then
                    InserisciAdAC = False
                End If
            End If


            If campodb(ReadVerificaTA.Item("TipoOperatore")) <> "" Then
                Dim MovDom As New Cls_MovimentiDomiciliare_Operatori

                MovDom.IdMovimento = ID
                If Not MovDom.LegggiTipoOperatore(STRINGACONNESSIONEDB, campodb(ReadVerificaTA.Item("TipoOperatore"))) Then
                    InserisciAdAC = False
                End If
            End If


            If campodbN(ReadVerificaTA.Item("NumeroOperatori")) > 0 Then
                If campodbN(ReadVerificaTA.Item("NumeroOperatori")) = 1 And NumeroOperatori <> 1 Then
                    InserisciAdAC = False
                End If
                If campodbN(ReadVerificaTA.Item("NumeroOperatori")) = 2 And NumeroOperatori = 1 Then
                    InserisciAdAC = False
                End If
            End If

            If campodbN(ReadVerificaTA.Item("PrimoIngresso")) > 0 Then
                If campodbN(ReadVerificaTA.Item("PrimoIngresso")) = 1 And PrimoIngresso = False Then
                    InserisciAdAC = False
                End If
                If campodbN(ReadVerificaTA.Item("PrimoIngresso")) = 2 And PrimoIngresso = True Then
                    InserisciAdAC = False
                End If
            End If

            If campodbN(ReadVerificaTA.Item("Festivo")) > 0 Then
                If Val(Cserv) > 0 Then
                    If VtFestivita(Val(Cserv), Day(Data)) = 0 Then
                        If GiornoFestivo(Cserv, Data) = True Then
                            VtFestivita(Val(Cserv), Day(Data)) = 1
                        Else
                            VtFestivita(Val(Cserv), Day(Data)) = 2
                        End If
                    End If
                    If campodbN(ReadVerificaTA.Item("Festivo")) = 1 And VtFestivita(Val(Cserv), Day(Data)) = 2 Then
                        InserisciAdAC = False
                    End If
                    If campodbN(ReadVerificaTA.Item("Festivo")) = 2 And VtFestivita(Val(Cserv), Day(Data)) = 1 Then
                        InserisciAdAC = False
                    End If
                Else
                    If campodbN(ReadVerificaTA.Item("Festivo")) = 1 And GiornoFestivo(Cserv, Data) = False Then
                        InserisciAdAC = False
                    End If
                    If campodbN(ReadVerificaTA.Item("Festivo")) = 2 And GiornoFestivo(Cserv, Data) = True Then
                        InserisciAdAC = False
                    End If
                End If
            End If

            'FasciaOraria
            If campodbN(ReadVerificaTA.Item("FasciaOraria")) > 0 Then
                If campodbN(ReadVerificaTA.Item("FasciaOraria")) <> FasciaOraria Then
                    InserisciAdAC = False
                End If
            End If



            'PrimoIngresso


            If InserisciAdAC = True Then
                If CreatoDocumentoOspiti = 0 Then
                    If campodb(ReadVerificaTA.Item("Tipologia")) = "I" Then
                        If campodb(ReadVerificaTA.Item("Ripartizione")) = "O" Then
                            Dim DesT As New Cls_Addebito

                            DesT.Codice = campodb(ReadVerificaTA.Item("TipoAddebito"))
                            DesT.Leggi(STRINGACONNESSIONEDB, DesT.Codice)

                            Dim Trovato As Boolean = False
                            For X = 1 To MaxAddebito
                                If VettoreDescrizione(X) = TipoDomiciliare.Descrizione And VettoreCodice(X) = DesT.Codice And VettoreTipo(X) = "O" Then
                                    VettoreImporto(X) = VettoreImporto(X) + campodbN(ReadVerificaTA.Item("Importo1"))
                                    VettoreDescrizione(X) = TipoDomiciliare.Descrizione ' DesT.Descrizione
                                    VettoreQuantita(X) = VettoreQuantita(X) + 1
                                    VettoreCodice(X) = DesT.Codice
                                    Trovato = True
                                End If
                            Next
                            If Trovato = False Then
                                MaxAddebito = MaxAddebito + 1
                                VettoreImporto(MaxAddebito) = VettoreImporto(MaxAddebito) + campodbN(ReadVerificaTA.Item("Importo1"))
                                VettoreQuantita(MaxAddebito) = VettoreQuantita(MaxAddebito) + 1

                                VettoreDescrizione(MaxAddebito) = TipoDomiciliare.Descrizione ' DesT.Descrizione
                                VettoreCodice(MaxAddebito) = DesT.Codice
                                VettoreTipo(MaxAddebito) = "O"
                            End If
                        End If
                    End If



                    If campodb(ReadVerificaTA.Item("Tipologia")) = "O" Or campodb(ReadVerificaTA.Item("Tipologia")) = "M" Or campodb(ReadVerificaTA.Item("Tipologia")) = "Q" Or campodb(ReadVerificaTA.Item("Tipologia")) = "C" Then
                        If campodb(ReadVerificaTA.Item("Ripartizione")) = "O" Then
                            Dim DesT As New Cls_Addebito
                            Dim UnitaTempo As Integer

                            If campodb(ReadVerificaTA.Item("Tipologia")) = "C" Then
                                UnitaTempo = Math.Round(Minuti / 5)
                            End If
                            If campodb(ReadVerificaTA.Item("Tipologia")) = "O" Then
                                UnitaTempo = Math.Round(Minuti / 60)
                            End If
                            If campodb(ReadVerificaTA.Item("Tipologia")) = "M" Then
                                UnitaTempo = Math.Round(Minuti / 30)
                            End If
                            If campodb(ReadVerificaTA.Item("Tipologia")) = "Q" Then
                                UnitaTempo = Math.Round(Minuti / 15)
                            End If



                            DesT.Codice = campodb(ReadVerificaTA.Item("TipoAddebito"))
                            DesT.Leggi(STRINGACONNESSIONEDB, DesT.Codice)

                            Dim Trovato As Boolean = False
                            For X = 1 To MaxAddebito
                                If VettoreCodice(X) = DesT.Codice And VettoreTipo(X) = "O" Then
                                    If UnitaTempo = 1 Then
                                        VettoreImporto(X) = VettoreImporto(X) + campodbN(ReadVerificaTA.Item("Importo1"))
                                    End If
                                    If UnitaTempo = 2 Then
                                        VettoreImporto(X) = VettoreImporto(X) + campodbN(ReadVerificaTA.Item("Importo1")) + campodbN(ReadVerificaTA.Item("Importo2"))
                                    End If
                                    If UnitaTempo > 2 Then
                                        VettoreImporto(X) = VettoreImporto(X) + (campodbN(ReadVerificaTA.Item("Importo3")) * (UnitaTempo - 2)) + campodbN(ReadVerificaTA.Item("Importo1")) + campodbN(ReadVerificaTA.Item("Importo2"))
                                    End If

                                    VettoreQuantita(X) = VettoreQuantita(X) + Minuti
                                    VettoreDescrizione(X) = TipoDomiciliare.Descrizione ' DesT.Descrizione
                                    VettoreCodice(X) = DesT.Codice
                                    Trovato = True
                                End If
                            Next
                            If Trovato = False Then
                                MaxAddebito = MaxAddebito + 1
                                If UnitaTempo = 1 Then
                                    VettoreImporto(MaxAddebito) = VettoreImporto(MaxAddebito) + campodbN(ReadVerificaTA.Item("Importo1"))
                                End If
                                If UnitaTempo = 2 Then
                                    VettoreImporto(MaxAddebito) = VettoreImporto(MaxAddebito) + campodbN(ReadVerificaTA.Item("Importo1")) + campodbN(ReadVerificaTA.Item("Importo2"))
                                End If
                                If UnitaTempo > 2 Then
                                    VettoreImporto(MaxAddebito) = VettoreImporto(MaxAddebito) + (campodbN(ReadVerificaTA.Item("Importo3")) * (UnitaTempo - 2)) + campodbN(ReadVerificaTA.Item("Importo1")) + campodbN(ReadVerificaTA.Item("Importo2"))
                                End If

                                VettoreQuantita(MaxAddebito) = VettoreQuantita(MaxAddebito) + Minuti
                                VettoreDescrizione(MaxAddebito) = TipoDomiciliare.Descrizione ' DesT.Descrizione
                                VettoreCodice(MaxAddebito) = DesT.Codice
                                VettoreTipo(MaxAddebito) = "O"
                            End If
                        End If
                    End If
                End If


                If campodb(ReadVerificaTA.Item("Tipologia")) = "I" Then
                    If campodb(ReadVerificaTA.Item("Ripartizione")) = "C" Then
                        Dim DesT As New Cls_Addebito

                        DesT.Codice = campodb(ReadVerificaTA.Item("TipoAddebito"))
                        DesT.Leggi(STRINGACONNESSIONEDB, DesT.Codice)

                        Dim Trovato As Boolean = False
                        For X = 1 To MaxAddebito
                            If VettoreCodice(X) = DesT.Codice And VettoreTipo(X) = "C" Then
                                VettoreImporto(X) = VettoreImporto(X) + campodbN(ReadVerificaTA.Item("Importo1"))
                                VettoreDescrizione(X) = TipoDomiciliare.Descrizione ' DesT.Descrizione
                                VettoreCodice(X) = DesT.Codice
                                Trovato = True
                            End If
                        Next
                        If Trovato = False Then
                            MaxAddebito = MaxAddebito + 1
                            VettoreImporto(MaxAddebito) = VettoreImporto(MaxAddebito) + campodbN(ReadVerificaTA.Item("Importo1"))

                            VettoreDescrizione(MaxAddebito) = TipoDomiciliare.Descrizione ' DesT.Descrizione
                            VettoreCodice(MaxAddebito) = DesT.Codice
                            VettoreTipo(MaxAddebito) = "C"
                        End If
                    End If
                End If


                If campodb(ReadVerificaTA.Item("Tipologia")) = "O" Or campodb(ReadVerificaTA.Item("Tipologia")) = "M" Or campodb(ReadVerificaTA.Item("Tipologia")) = "Q" Or campodb(ReadVerificaTA.Item("Tipologia")) = "C" Then
                    If campodb(ReadVerificaTA.Item("Ripartizione")) = "C" Then
                        Dim DesT As New Cls_Addebito
                        Dim UnitaTempo As Integer


                        If campodb(ReadVerificaTA.Item("Tipologia")) = "C" Then
                            UnitaTempo = Math.Round(Minuti / 5)
                        End If
                        If campodb(ReadVerificaTA.Item("Tipologia")) = "O" Then
                            UnitaTempo = Math.Round(Minuti / 60)
                        End If
                        If campodb(ReadVerificaTA.Item("Tipologia")) = "M" Then
                            UnitaTempo = Math.Round(Minuti / 30)
                        End If
                        If campodb(ReadVerificaTA.Item("Tipologia")) = "Q" Then
                            UnitaTempo = Math.Round(Minuti / 15)
                        End If


                        DesT.Codice = campodb(ReadVerificaTA.Item("TipoAddebito"))
                        DesT.Leggi(STRINGACONNESSIONEDB, DesT.Codice)

                        Dim Trovato As Boolean = False
                        For X = 1 To MaxAddebito
                            If VettoreCodice(X) = DesT.Codice And VettoreTipo(X) = "C" Then
                                If UnitaTempo = 1 Then
                                    VettoreImporto(X) = VettoreImporto(X) + campodbN(ReadVerificaTA.Item("Importo1"))
                                End If
                                If UnitaTempo = 2 Then
                                    VettoreImporto(X) = VettoreImporto(X) + campodbN(ReadVerificaTA.Item("Importo1")) + campodbN(ReadVerificaTA.Item("Importo2"))
                                End If
                                If UnitaTempo > 2 Then
                                    VettoreImporto(X) = VettoreImporto(X) + (campodbN(ReadVerificaTA.Item("Importo3")) * (UnitaTempo - 2)) + campodbN(ReadVerificaTA.Item("Importo1")) + campodbN(ReadVerificaTA.Item("Importo2"))
                                End If

                                VettoreDescrizione(X) = TipoDomiciliare.Descrizione ' DesT.Descrizione
                                VettoreCodice(X) = DesT.Codice
                                VettoreQuantita(X) = VettoreQuantita(X) + Minuti
                                Trovato = True
                            End If
                        Next
                        If Trovato = False Then
                            MaxAddebito = MaxAddebito + 1
                            If UnitaTempo = 1 Then
                                VettoreImporto(MaxAddebito) = VettoreImporto(MaxAddebito) + campodbN(ReadVerificaTA.Item("Importo1"))
                            End If
                            If UnitaTempo = 2 Then
                                VettoreImporto(MaxAddebito) = VettoreImporto(MaxAddebito) + campodbN(ReadVerificaTA.Item("Importo1")) + campodbN(ReadVerificaTA.Item("Importo2"))
                            End If

                            If UnitaTempo > 2 Then
                                VettoreImporto(MaxAddebito) = VettoreImporto(MaxAddebito) + (campodbN(ReadVerificaTA.Item("Importo3")) * (UnitaTempo - 2)) + campodbN(ReadVerificaTA.Item("Importo1")) + campodbN(ReadVerificaTA.Item("Importo2"))
                            End If

                            VettoreQuantita(MaxAddebito) = Minuti
                            VettoreDescrizione(MaxAddebito) = TipoDomiciliare.Descrizione ' DesT.Descrizione
                            VettoreCodice(MaxAddebito) = DesT.Codice
                            VettoreTipo(MaxAddebito) = "C"
                        End If
                    End If
                End If



                If campodb(ReadVerificaTA.Item("Tipologia")) = "I" Then
                    If campodb(ReadVerificaTA.Item("Ripartizione")) = "J" Then
                        Dim DesT As New Cls_Addebito

                        DesT.Codice = campodb(ReadVerificaTA.Item("TipoAddebito"))
                        DesT.Leggi(STRINGACONNESSIONEDB, DesT.Codice)

                        Dim Trovato As Boolean = False
                        For X = 1 To MaxAddebito
                            If VettoreCodice(X) = DesT.Codice And VettoreTipo(X) = "J" Then
                                VettoreImporto(X) = VettoreImporto(X) + campodbN(ReadVerificaTA.Item("Importo1"))
                                VettoreDescrizione(X) = TipoDomiciliare.Descrizione ' DesT.Descrizione
                                VettoreCodice(X) = DesT.Codice
                                Trovato = True
                            End If
                        Next
                        If Trovato = False Then
                            MaxAddebito = MaxAddebito + 1
                            VettoreImporto(MaxAddebito) = VettoreImporto(MaxAddebito) + campodbN(ReadVerificaTA.Item("Importo1"))

                            VettoreDescrizione(MaxAddebito) = TipoDomiciliare.Descrizione ' DesT.Descrizione
                            VettoreCodice(MaxAddebito) = DesT.Codice
                            VettoreTipo(MaxAddebito) = "J"
                        End If
                    End If
                End If


                If campodb(ReadVerificaTA.Item("Tipologia")) = "O" Or campodb(ReadVerificaTA.Item("Tipologia")) = "M" Or campodb(ReadVerificaTA.Item("Tipologia")) = "Q" Or campodb(ReadVerificaTA.Item("Tipologia")) = "C" Then
                    If campodb(ReadVerificaTA.Item("Ripartizione")) = "J" Then
                        Dim DesT As New Cls_Addebito
                        Dim UnitaTempo As Integer


                        If campodb(ReadVerificaTA.Item("Tipologia")) = "C" Then
                            UnitaTempo = Math.Round(Minuti / 5)
                        End If
                        If campodb(ReadVerificaTA.Item("Tipologia")) = "O" Then
                            UnitaTempo = Math.Round(Minuti / 60)
                        End If
                        If campodb(ReadVerificaTA.Item("Tipologia")) = "M" Then
                            UnitaTempo = Math.Round(Minuti / 30)
                        End If
                        If campodb(ReadVerificaTA.Item("Tipologia")) = "Q" Then
                            UnitaTempo = Math.Round(Minuti / 15)
                        End If


                        DesT.Codice = campodb(ReadVerificaTA.Item("TipoAddebito"))
                        DesT.Leggi(STRINGACONNESSIONEDB, DesT.Codice)

                        Dim Trovato As Boolean = False
                        For X = 1 To MaxAddebito
                            If VettoreCodice(X) = DesT.Codice And VettoreTipo(X) = "J" Then
                                If UnitaTempo = 1 Then
                                    VettoreImporto(X) = VettoreImporto(X) + campodbN(ReadVerificaTA.Item("Importo1"))
                                End If
                                If UnitaTempo = 2 Then
                                    VettoreImporto(X) = VettoreImporto(X) + campodbN(ReadVerificaTA.Item("Importo1")) + campodbN(ReadVerificaTA.Item("Importo2"))
                                End If
                                If UnitaTempo > 2 Then
                                    VettoreImporto(X) = VettoreImporto(X) + (campodbN(ReadVerificaTA.Item("Importo3")) * (UnitaTempo - 2)) + campodbN(ReadVerificaTA.Item("Importo1")) + campodbN(ReadVerificaTA.Item("Importo2"))
                                End If

                                VettoreDescrizione(X) = TipoDomiciliare.Descrizione ' DesT.Descrizione
                                VettoreCodice(X) = DesT.Codice
                                Trovato = True
                            End If
                        Next
                        If Trovato = False Then
                            MaxAddebito = MaxAddebito + 1
                            If UnitaTempo = 1 Then
                                VettoreImporto(MaxAddebito) = VettoreImporto(MaxAddebito) + campodbN(ReadVerificaTA.Item("Importo1"))
                            End If
                            If UnitaTempo = 2 Then
                                VettoreImporto(MaxAddebito) = VettoreImporto(MaxAddebito) + campodbN(ReadVerificaTA.Item("Importo1")) + campodbN(ReadVerificaTA.Item("Importo2"))
                            End If

                            If UnitaTempo > 2 Then
                                VettoreImporto(MaxAddebito) = VettoreImporto(MaxAddebito) + (campodbN(ReadVerificaTA.Item("Importo3")) * (UnitaTempo - 2)) + campodbN(ReadVerificaTA.Item("Importo1")) + campodbN(ReadVerificaTA.Item("Importo2"))
                            End If


                            VettoreDescrizione(MaxAddebito) = TipoDomiciliare.Descrizione ' DesT.Descrizione
                            VettoreCodice(MaxAddebito) = DesT.Codice
                            VettoreTipo(MaxAddebito) = "J"
                        End If
                    End If
                End If

                If campodb(ReadVerificaTA.Item("Tipologia")) = "I" Then
                    If campodb(ReadVerificaTA.Item("Ripartizione")) = "R" Then
                        Dim DesT As New Cls_Addebito

                        DesT.Codice = campodb(ReadVerificaTA.Item("TipoAddebito"))
                        DesT.Leggi(STRINGACONNESSIONEDB, DesT.Codice)

                        Dim Trovato As Boolean = False

                        For X = 1 To MaxAddebito
                            If VettoreCodice(X) = DesT.Codice And VettoreTipo(X) = "R" Then
                                VettoreImporto(X) = VettoreImporto(X) + campodbN(ReadVerificaTA.Item("Importo1"))
                                VettoreDescrizione(X) = TipoDomiciliare.Descrizione ' DesT.Descrizione
                                VettoreCodice(X) = DesT.Codice
                                Trovato = True
                            End If
                        Next
                        If Trovato = False Then
                            MaxAddebito = MaxAddebito + 1
                            VettoreImporto(MaxAddebito) = VettoreImporto(MaxAddebito) + campodbN(ReadVerificaTA.Item("Importo1"))

                            VettoreDescrizione(MaxAddebito) = TipoDomiciliare.Descrizione ' DesT.Descrizione
                            VettoreCodice(MaxAddebito) = DesT.Codice
                            VettoreTipo(MaxAddebito) = "R"
                        End If
                    End If
                End If


                If campodb(ReadVerificaTA.Item("Tipologia")) = "O" Or campodb(ReadVerificaTA.Item("Tipologia")) = "M" Or campodb(ReadVerificaTA.Item("Tipologia")) = "Q" Or campodb(ReadVerificaTA.Item("Tipologia")) = "C" Then
                    If campodb(ReadVerificaTA.Item("Ripartizione")) = "R" Then
                        Dim DesT As New Cls_Addebito
                        Dim UnitaTempo As Integer

                        If campodb(ReadVerificaTA.Item("Tipologia")) = "C" Then
                            UnitaTempo = Math.Round(Minuti / 5)
                        End If
                        If campodb(ReadVerificaTA.Item("Tipologia")) = "O" Then
                            UnitaTempo = Math.Round(Minuti / 60)
                        End If
                        If campodb(ReadVerificaTA.Item("Tipologia")) = "M" Then
                            UnitaTempo = Math.Round(Minuti / 30)
                        End If
                        If campodb(ReadVerificaTA.Item("Tipologia")) = "Q" Then
                            UnitaTempo = Math.Round(Minuti / 15)
                        End If


                        DesT.Codice = campodb(ReadVerificaTA.Item("TipoAddebito"))
                        DesT.Leggi(STRINGACONNESSIONEDB, DesT.Codice)

                        Dim Trovato As Boolean = False

                        For X = 1 To MaxAddebito
                            If VettoreCodice(X) = DesT.Codice And VettoreTipo(X) = "R" Then
                                If UnitaTempo = 1 Then
                                    VettoreImporto(X) = VettoreImporto(X) + campodbN(ReadVerificaTA.Item("Importo1"))
                                End If
                                If UnitaTempo = 2 Then
                                    VettoreImporto(X) = VettoreImporto(X) + campodbN(ReadVerificaTA.Item("Importo1")) + campodbN(ReadVerificaTA.Item("Importo2"))
                                End If
                                If UnitaTempo > 2 Then
                                    VettoreImporto(X) = VettoreImporto(X) + (campodbN(ReadVerificaTA.Item("Importo3")) * (UnitaTempo - 2)) + campodbN(ReadVerificaTA.Item("Importo1")) + campodbN(ReadVerificaTA.Item("Importo2"))
                                End If


                                VettoreDescrizione(X) = TipoDomiciliare.Descrizione ' DesT.Descrizione
                                VettoreCodice(X) = DesT.Codice
                                Trovato = True
                            End If
                        Next
                        If Trovato = False Then
                            MaxAddebito = MaxAddebito + 1
                            If UnitaTempo = 1 Then
                                VettoreImporto(MaxAddebito) = VettoreImporto(MaxAddebito) + campodbN(ReadVerificaTA.Item("Importo1"))
                            End If
                            If UnitaTempo = 2 Then
                                VettoreImporto(MaxAddebito) = VettoreImporto(MaxAddebito) + campodbN(ReadVerificaTA.Item("Importo1")) + campodbN(ReadVerificaTA.Item("Importo2"))
                            End If

                            If UnitaTempo > 2 Then
                                VettoreImporto(MaxAddebito) = VettoreImporto(MaxAddebito) + (campodbN(ReadVerificaTA.Item("Importo3")) * (UnitaTempo - 2)) + campodbN(ReadVerificaTA.Item("Importo1")) + campodbN(ReadVerificaTA.Item("Importo2"))
                            End If


                            VettoreDescrizione(MaxAddebito) = TipoDomiciliare.Descrizione ' DesT.Descrizione
                            VettoreCodice(MaxAddebito) = DesT.Codice
                            VettoreTipo(MaxAddebito) = "R"
                        End If
                    End If
                End If
            End If
        Loop
        ReadVerificaTA.Close()
    End Sub

End Class






