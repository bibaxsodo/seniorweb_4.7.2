Imports Microsoft.VisualBasic
Imports System.Data.OleDb
Imports System.Web.Hosting

Public Class ClsModalitaPagamento
    Public Codice As String
    Public DESCRIZIONE As String
    Public MASTRO As Long
    Public CONTO As Long
    Public SOTTOCONTO As Long
    Public CODICEBOLLETTA As String
    Public ModalitaPagamento As String
    Public BancaAbi As Integer
    Public BancaCab As Integer
    Public BancaCin As String
    Public BancaCliente As String
    Public CCBancario As String
    Public IntCliente As String
    Public NumeroControlloCliente As Integer
    Public Utente As String
    Public DataAggiornamento As Date
    Public DescrizioneEstesa As String
    Public Tipo As String

    Public ExtraFisso As String

    Public Sub UpDateDropBox(ByVal StringaConnessione As String, ByRef appoggio As DropDownList)
        Dim cn As OleDbConnection



        cn = New Data.OleDb.OleDbConnection(StringaConnessione)

        cn.Open()
        Dim cmd As New OleDbCommand()
        cmd.CommandText = ("select * from MODALITAPAGAMENTO")
        cmd.Connection = cn
        Dim sb As StringBuilder = New StringBuilder

        appoggio.Items.Clear()
        Dim myPOSTreader As OleDbDataReader = cmd.ExecuteReader()
        Do While myPOSTreader.Read
            appoggio.Items.Add(myPOSTreader.Item("Descrizione"))
            appoggio.Items(appoggio.Items.Count - 1).Value = myPOSTreader.Item("Codice")
        Loop
        myPOSTreader.Close()
        cn.Close()
        appoggio.Items.Add("")
        appoggio.Items(appoggio.Items.Count - 1).Value = ""
        appoggio.Items(appoggio.Items.Count - 1).Selected = True

    End Sub

    Function InUso(ByVal StringaConnessione As String) As Boolean
        Dim cn As OleDbConnection
        InUso = False


        cn = New Data.OleDb.OleDbConnection(StringaConnessione)

        cn.Open()
        Dim cmd As New OleDbCommand()
        cmd.CommandText = ("select *  from AnagraficaComune Where ModalitaPagamento = ? ")
        cmd.Parameters.AddWithValue("@Codice", Codice)
        cmd.Connection = cn
        Dim InUsoRd As OleDbDataReader = cmd.ExecuteReader()
        If InUsoRd.Read Then
            InUso = True
        End If
        InUsoRd.Close()
        cn.Close()


    End Function

    Sub Elimina(ByVal StringaConnessione As String)
        Dim cn As OleDbConnection


        cn = New Data.OleDb.OleDbConnection(StringaConnessione)

        cn.Open()
        Dim cmd As New OleDbCommand()
        cmd.CommandText = ("Delete from MODALITAPAGAMENTO where Codice = '" & Codice & "' ")
        cmd.Connection = cn
        cmd.ExecuteNonQuery()
        cn.Close()
    End Sub

    Function MaxModalitaPagamento(ByVal StringaConnessione As String) As String
        Dim cn As OleDbConnection


        MaxModalitaPagamento = ""

        cn = New Data.OleDb.OleDbConnection(StringaConnessione)

        cn.Open()
        Dim cmd As New OleDbCommand()
        cmd.CommandText = ("select max(CASE WHEN (len(Codice) = 1) THEN '0' + CODICE ELSE Codice END)  from MODALITAPAGAMENTO ")
        cmd.Connection = cn
        Dim MaxAddebito As OleDbDataReader = cmd.ExecuteReader()
        If MaxAddebito.Read Then
            Dim MassimoLetto As String

            MassimoLetto = campodb(MaxAddebito.Item(0))


            If MassimoLetto = "Z9" Then
                MassimoLetto = "a0"
                cn.Close()
                Return MassimoLetto
            End If


            If MassimoLetto = "99" Then
                MassimoLetto = "A0"
            Else
                If Mid(MassimoLetto & Space(10), 1, 1) > "0" And Mid(MassimoLetto & Space(10), 1, 1) > "9" Then
                    If Mid(MassimoLetto & Space(10), 2, 1) < "9" Then
                        MassimoLetto = Mid(MassimoLetto & Space(10), 1, 1) & Val(Mid(MassimoLetto & Space(10), 2, 1)) + 1
                    Else
                        Dim CodiceAscii As String
                        CodiceAscii = Asc(Mid(MassimoLetto & Space(10), 1, 1))

                        MassimoLetto = Chr(CodiceAscii + 1) & "0"
                    End If
                Else
                    MassimoLetto = Format(Val(MassimoLetto) + 1, "00")
                End If
            End If

            Return MassimoLetto
        Else
            Return "01"
        End If
        cn.Close()

    End Function


    Sub LeggiDescrizione(ByVal StringaConnessione As String)
        Dim cn As OleDbConnection


        cn = New Data.OleDb.OleDbConnection(StringaConnessione)

        cn.Open()
        Dim cmd As New OleDbCommand()
        cmd.CommandText = ("select * from MODALITAPAGAMENTO where DESCRIZIONE = '" & DESCRIZIONE & "' ")
        cmd.Connection = cn
        Dim myPOSTreader As OleDbDataReader = cmd.ExecuteReader()
        If myPOSTreader.Read Then
            Codice = campodb(myPOSTreader.Item("Codice"))
            Call Leggi(StringaConnessione)
        End If
        cn.Close()
    End Sub

    Sub Leggi(ByVal StringaConnessione As String)
        Dim cn As OleDbConnection


        cn = New Data.OleDb.OleDbConnection(StringaConnessione)

        cn.Open()
        Dim cmd As New OleDbCommand()
        cmd.CommandText = ("select * from MODALITAPAGAMENTO where Codice = '" & Codice & "' ")
        cmd.Connection = cn
        Dim myPOSTreader As OleDbDataReader = cmd.ExecuteReader()
        If myPOSTreader.Read Then
            Codice = campodb(myPOSTreader.Item("Codice"))
            DESCRIZIONE = campodb(myPOSTreader.Item("DESCRIZIONE"))
            MASTRO = campodbN(myPOSTreader.Item("MASTRO"))
            CONTO = campodbN(myPOSTreader.Item("CONTO"))
            SOTTOCONTO = campodbN(myPOSTreader.Item("SOTTOCONTO"))
            CODICEBOLLETTA = campodb(myPOSTreader.Item("CODICEBOLLETTA"))
            ModalitaPagamento = campodb(myPOSTreader.Item("ModalitaPagamento"))
            BancaAbi = campodbN(myPOSTreader.Item("BancaAbi"))
            BancaCab = campodbN(myPOSTreader.Item("BancaCab"))
            BancaCin = campodb(myPOSTreader.Item("BancaCin"))
            BancaCliente = campodb(myPOSTreader.Item("BancaCliente"))
            CCBancario = campodb(myPOSTreader.Item("CCBancario"))
            IntCliente = campodb(myPOSTreader.Item("IntCliente"))
            NumeroControlloCliente = Val(campodb(myPOSTreader.Item("NumeroControlloCliente")))
            Utente = campodb(myPOSTreader.Item("Utente"))
            DataAggiornamento = campodbd(myPOSTreader.Item("DataAggiornamento"))
            DescrizioneEstesa = campodb(myPOSTreader.Item("DescrizioneEstesa"))
            Tipo = campodb(myPOSTreader.Item("Tipo"))

            ExtraFisso = campodb(myPOSTreader.Item("ExtraFisso"))
        End If
        cn.Close()
    End Sub

    Sub LeggiModalitaFattura(ByVal StringaConnessione As String)
        Dim cn As OleDbConnection


        cn = New Data.OleDb.OleDbConnection(StringaConnessione)

        cn.Open()
        Dim cmd As New OleDbCommand()
        cmd.CommandText = ("select * from MODALITAPAGAMENTO where ModalitaPagamento = '" & ModalitaPagamento & "' ")
        cmd.Connection = cn
        Dim myPOSTreader As OleDbDataReader = cmd.ExecuteReader()
        If myPOSTreader.Read Then
            Codice = campodb(myPOSTreader.Item("Codice"))
            DESCRIZIONE = campodb(myPOSTreader.Item("DESCRIZIONE"))
            MASTRO = campodbN(myPOSTreader.Item("MASTRO"))
            CONTO = campodbN(myPOSTreader.Item("CONTO"))
            SOTTOCONTO = campodbN(myPOSTreader.Item("SOTTOCONTO"))
            CODICEBOLLETTA = campodb(myPOSTreader.Item("CODICEBOLLETTA"))
            ModalitaPagamento = campodb(myPOSTreader.Item("ModalitaPagamento"))
            BancaAbi = campodbN(myPOSTreader.Item("BancaAbi"))
            BancaCab = campodbN(myPOSTreader.Item("BancaCab"))
            BancaCin = campodb(myPOSTreader.Item("BancaCin"))
            BancaCliente = campodb(myPOSTreader.Item("BancaCliente"))
            CCBancario = campodb(myPOSTreader.Item("CCBancario"))
            IntCliente = campodb(myPOSTreader.Item("IntCliente"))
            NumeroControlloCliente = Val(campodb(myPOSTreader.Item("NumeroControlloCliente")))
            Utente = campodb(myPOSTreader.Item("Utente"))
            DataAggiornamento = campodbd(myPOSTreader.Item("DataAggiornamento"))
            DescrizioneEstesa = campodb(myPOSTreader.Item("DescrizioneEstesa"))
            Tipo = campodb(myPOSTreader.Item("Tipo"))

            ExtraFisso = campodb(myPOSTreader.Item("ExtraFisso"))
        End If
        cn.Close()
    End Sub

    Function campodbd(ByVal oggetto As Object) As Date
        If IsDBNull(oggetto) Then
            Return Nothing
        Else
            Return oggetto
        End If
    End Function


    Function campodb(ByVal oggetto As Object) As String
        If IsDBNull(oggetto) Then
            Return ""
        Else
            Return oggetto
        End If
    End Function


    Function campodbN(ByVal oggetto As Object) As Double
        If IsDBNull(oggetto) Then
            Return 0
        Else
            Return oggetto
        End If
    End Function


    Public Sub Scrivi(ByVal StringaConnessione As String)
        Dim cn As OleDbConnection
        Dim MySql As String

        MySql = ""


        cn = New Data.OleDb.OleDbConnection(StringaConnessione)

        cn.Open()
        Dim cmd As New OleDbCommand()
        cmd.CommandText = ("select * from MODALITAPAGAMENTO where Codice = '" & Codice & "'")
        cmd.Connection = cn
        Dim myPOSTreader As OleDbDataReader = cmd.ExecuteReader()
        If myPOSTreader.Read Then
            MySql = "UPDATE MODALITAPAGAMENTO SET DESCRIZIONE = ?,MASTRO = ?,CONTO = ?,SOTTOCONTO = ?,CODICEBOLLETTA = ?,ModalitaPagamento = ?,BancaAbi = ?,BancaCab = ?,BancaCin = ?,BancaCliente = ?,CCBancario = ?,IntCliente = ?,NumeroControlloCliente = ?,DescrizioneEstesa  = ?,Tipo = ? ,Utente = ?,DataAggiornamento = ?,ExtraFisso = ? WHERE  Codice = '" & Codice & "'"
            Dim cmdw As New OleDbCommand()
            cmdw.CommandText = (MySql)
            cmdw.Parameters.AddWithValue("DESCRIZIONE", DESCRIZIONE)
            cmdw.Parameters.AddWithValue("MASTRO", MASTRO)
            cmdw.Parameters.AddWithValue("CONTO", CONTO)
            cmdw.Parameters.AddWithValue("SOTTOCONTO", SOTTOCONTO)
            cmdw.Parameters.AddWithValue("CODICEBOLLETTA", CODICEBOLLETTA)
            cmdw.Parameters.AddWithValue("ModalitaPagamento", ModalitaPagamento)
            cmdw.Parameters.AddWithValue("BancaAbi", BancaAbi)
            cmdw.Parameters.AddWithValue("BancaCab", BancaCab)
            cmdw.Parameters.AddWithValue("BancaCin", BancaCin)
            cmdw.Parameters.AddWithValue("BancaCliente", BancaCliente)
            cmdw.Parameters.AddWithValue("CCBancario", CCBancario)
            cmdw.Parameters.AddWithValue("IntCliente", IntCliente)
            cmdw.Parameters.AddWithValue("NumeroControlloCliente", NumeroControlloCliente)
            cmdw.Parameters.AddWithValue("DescrizioneEstesa", DescrizioneEstesa)
            cmdw.Parameters.AddWithValue("Tipo", Tipo)
            cmdw.Parameters.AddWithValue("Utente", Utente)
            cmdw.Parameters.AddWithValue("DataAggiornamento", Now)
            cmdw.Parameters.AddWithValue("ExtraFisso", ExtraFisso)
            'ExtraFisso
            cmdw.Connection = cn
            cmdw.ExecuteNonQuery()
        Else
            MySql = "INSERT INTO MODALITAPAGAMENTO (Codice ,DESCRIZIONE ,MASTRO ,CONTO ,SOTTOCONTO ,CODICEBOLLETTA ,ModalitaPagamento ,BancaAbi ,BancaCab ,BancaCin ,BancaCliente ,CCBancario ,IntCliente ,NumeroControlloCliente,DescrizioneEstesa, Tipo ,Utente ,DataAggiornamento,ExtraFisso) VALUES " & _
                                                  "(?,                 ?,      ?,     ?,          ?,              ?,                 ?,        ?,        ?,        ?,            ?,          ?,          ?,                     ?,                ?,     ?,      ?,                ?,  ?)"
            Dim cmdw As New OleDbCommand()
            cmdw.CommandText = (MySql)
            cmdw.Parameters.AddWithValue("Codice", Codice)
            cmdw.Parameters.AddWithValue("DESCRIZIONE", DESCRIZIONE)
            cmdw.Parameters.AddWithValue("MASTRO", MASTRO)
            cmdw.Parameters.AddWithValue("CONTO", CONTO)
            cmdw.Parameters.AddWithValue("SOTTOCONTO", SOTTOCONTO)
            cmdw.Parameters.AddWithValue("CODICEBOLLETTA", CODICEBOLLETTA)
            cmdw.Parameters.AddWithValue("ModalitaPagamento", ModalitaPagamento)
            cmdw.Parameters.AddWithValue("BancaAbi", BancaAbi)
            cmdw.Parameters.AddWithValue("BancaCab", BancaCab)
            cmdw.Parameters.AddWithValue("BancaCin", BancaCin)
            cmdw.Parameters.AddWithValue("BancaCliente", BancaCliente)
            cmdw.Parameters.AddWithValue("CCBancario", CCBancario)
            cmdw.Parameters.AddWithValue("IntCliente", IntCliente)
            cmdw.Parameters.AddWithValue("NumeroControlloCliente", NumeroControlloCliente)
            cmdw.Parameters.AddWithValue("DescrizioneEstesa", DescrizioneEstesa)
            cmdw.Parameters.AddWithValue("Tipo", Tipo)
            cmdw.Parameters.AddWithValue("Utente", Utente)
            cmdw.Parameters.AddWithValue("DataAggiornamento", Now)
            cmdw.Parameters.AddWithValue("ExtraFisso", ExtraFisso)
            cmdw.Connection = cn
            cmdw.ExecuteNonQuery()
        End If
        myPOSTreader.Close()
        cn.Close()
    End Sub
End Class
