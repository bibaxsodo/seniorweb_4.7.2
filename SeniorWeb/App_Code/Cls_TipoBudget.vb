﻿Imports Microsoft.VisualBasic
Imports System.Data.OleDb
Imports System.Web.Hosting

Public Class Cls_TipoBudget
    Public Anno As Long
    Public Livello1 As Long
    Public Livello2 As Long
    Public Livello3 As Long
    Public Descrizione As String
    Public Tipo As String

    Function campodb(ByVal oggetto As Object) As String
        If IsDBNull(oggetto) Then
            Return ""
        Else
            Return oggetto
        End If
    End Function
    Sub New()
        Livello1 = 0
        Livello2 = 0
        Livello3 = 0
        Descrizione = ""
        Tipo = ""
        Anno = 0
    End Sub


    Sub Elimina(ByVal StringaConnessione As String)
        Dim cn As OleDbConnection

        Descrizione = ""
        If IsNothing(Livello1) Or IsNothing(Livello2) Or IsNothing(Livello3) Or IsNothing(Anno) Then
            Exit Sub
        End If

        cn = New Data.OleDb.OleDbConnection(StringaConnessione)

        cn.Open()
        Dim cmd As New OleDbCommand()
        cmd.CommandText = ("Delete from TipoBudget where " & _
                           "Anno = ? And Livello1 = ? And Livello2 = ? And Livello3 = ?")

        cmd.Parameters.AddWithValue("@Anno", Anno)
        cmd.Parameters.AddWithValue("@Livello1", Livello1)
        cmd.Parameters.AddWithValue("@Livello2", Livello2)
        cmd.Parameters.AddWithValue("@Livello3", Livello3)
        cmd.Connection = cn
        cmd.ExecuteNonQuery()

        cn.Close()
    End Sub




    Sub Scrivi(ByVal StringaConnessione As String)
        Dim cn As OleDbConnection
        Dim MySql As String

        Dim Modifica As Boolean = False

        cn = New Data.OleDb.OleDbConnection(StringaConnessione)

        cn.Open()
        Dim cmd As New OleDbCommand()
        cmd.CommandText = ("select * from TipoBudget where " & _
                           "Anno = ? And Livello1 = ? And Livello2 = ? And Livello3 = ?")

        cmd.Parameters.AddWithValue("@Anno", Anno)
        cmd.Parameters.AddWithValue("@Livello1", Livello1)
        cmd.Parameters.AddWithValue("@Livello2", Livello2)
        cmd.Parameters.AddWithValue("@Livello3", Livello3)
        cmd.Connection = cn
        Dim sb As StringBuilder = New StringBuilder

        Dim myPOSTreader As OleDbDataReader = cmd.ExecuteReader()
        If myPOSTreader.Read Then
            Modifica = True
        End If
        myPOSTreader.Close()

        If Modifica = False Then
            Dim APPOGGIO As String = ""
            Dim I As Integer
            For I = 1 To 10
                APPOGGIO = APPOGGIO & Int(Rnd(1) * 10)
            Next
            Dim cmdIns As New OleDbCommand()
            MySql = "INSERT INTO TipoBudget (Anno,Livello1,Livello2,Livello3,Tipo,Descrizione) VALUES (?,?,?,?,?,?)"
            cmdIns.CommandText = MySql
            cmdIns.Parameters.AddWithValue("@Anno", Anno)
            cmdIns.Parameters.AddWithValue("@Livello1", Livello1)
            cmdIns.Parameters.AddWithValue("@Livello2", Livello2)
            cmdIns.Parameters.AddWithValue("@Livello3", Livello3)
            cmdIns.Parameters.AddWithValue("@Tipo", Tipo)
            cmdIns.Parameters.AddWithValue("@Descrizione", Descrizione)
            cmdIns.Connection = cn
            cmdIns.ExecuteNonQuery()
        End If

        MySql = "UPDATE TipoBudget SET " & _
                "Descrizione = ?, " & _
                "Tipo = ? " & _
                " Where Anno = ? And Livello1 = ? And Livello2 = ? And Livello3 = ?"
        Dim cmd1 As New OleDbCommand()
        cmd1.Parameters.AddWithValue("@Descrizione", Descrizione)
        cmd1.Parameters.AddWithValue("@Tipo", Tipo)
        cmd1.Parameters.AddWithValue("@Anno", Anno)
        cmd1.Parameters.AddWithValue("@Livello1", Livello1)
        cmd1.Parameters.AddWithValue("@Livello2", Livello2)
        cmd1.Parameters.AddWithValue("@Livello3", Livello3)
        cmd1.CommandText = MySql
        cmd1.Connection = cn
        cmd1.ExecuteNonQuery()

        cn.Close()
    End Sub


    Sub Decodfica(ByVal StringaConnessione As String)
        Dim cn As OleDbConnection

        Descrizione = ""
        If IsNothing(Livello1) Or IsNothing(Livello2) Or IsNothing(Livello3) Or IsNothing(Anno) Then
            Exit Sub
        End If

        cn = New Data.OleDb.OleDbConnection(StringaConnessione)

        cn.Open()
        Dim cmd As New OleDbCommand()
        cmd.CommandText = ("select * from TipoBudget where " & _
                           "Anno = ? And Livello1 = ? And Livello2 = ? And Livello3 = ?")

        cmd.Parameters.AddWithValue("@Anno", Anno)
        cmd.Parameters.AddWithValue("@Livello1", Livello1)
        cmd.Parameters.AddWithValue("@Livello2", Livello2)
        cmd.Parameters.AddWithValue("@Livello3", Livello3)
        cmd.Connection = cn
        Dim sb As StringBuilder = New StringBuilder

        Dim myPOSTreader As OleDbDataReader = cmd.ExecuteReader()
        If myPOSTreader.Read Then
            Descrizione = campodb(myPOSTreader.Item("Descrizione"))
            Tipo = campodb(myPOSTreader.Item("Tipo"))
        End If
        myPOSTreader.Close()
        cn.Close()
    End Sub


End Class
