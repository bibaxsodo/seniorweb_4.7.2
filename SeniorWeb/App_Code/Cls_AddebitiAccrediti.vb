Imports Microsoft.VisualBasic
Imports System.Data.OleDb
Imports System.Web.Hosting


Public Class Cls_AddebitiAccrediti
    Public ID As Long
    Public CENTROSERVIZIO As String
    Public CodiceOspite As Long
    Public TipoMov As String
    Public Progressivo As Long
    Public Data As Date    
    Public Descrizione As String
    Public IMPORTO As Double
    Public RIFERIMENTO As String
    Public PARENTE As Long
    Public REGIONE As String
    Public PROVINCIA As String
    Public COMUNE As String
    Public MESECOMPETENZA As Long
    Public ANNOCOMPETENZA As Long
    Public RETTA As String
    Public CodiceIva As String
    Public Elaborato As Long
    Public NumeroRegistrazione As Long
    Public Utente As String
    Public chiaveselezione As String
    Public raggruppa As Integer
    Public IDEPERSONAM As String
    Public Quantita As Integer
    
    Public Function GiorniMese(ByVal Mese As Byte, ByVal Anno As Integer) As Byte
        If Mese = 1 Or Mese = 3 Or Mese = 5 Or Mese = 7 Or Mese = 8 Or _
           Mese = 10 Or Mese = 12 Then
            GiorniMese = 31
        Else
            If Mese <> 2 Then
                GiorniMese = 30
            Else
                If Day(DateSerial(Anno, Mese, 29)) = 29 Then
                    GiorniMese = 29
                Else
                    GiorniMese = 28
                End If
            End If
        End If
    End Function

    Function campodb(ByVal oggetto As Object) As String
        If IsDBNull(oggetto) Then
            Return ""
        Else
            Return oggetto
        End If
    End Function
    Function campodbn(ByVal oggetto As Object) As Double
        If IsDBNull(oggetto) Then
            Return 0
        Else
            Return oggetto
        End If
    End Function


    Function ImportoAddebitoMese(ByVal StringaConnessione As String, ByVal codiceospite As Integer, ByVal centroservizio As String, ByVal TipoAddebito As String, ByVal RIFERIMENTO As String, ByVal DataDal As Date, ByVal DataAl As Date) As Double
        Dim cn As OleDbConnection

        ImportoAddebitoMese = 0

        cn = New Data.OleDb.OleDbConnection(StringaConnessione)

        cn.Open()
        Dim cmd As New OleDbCommand()
        cmd.CommandText = ("select sum(Importo) from ADDACR where centroservizio = '" & centroservizio & "' And CodiceOspite = " & codiceospite & " And CodiceIva = ? And RIFERIMENTO = ? And Data >= ? And Data <= ? And Not ChiaveSelezione IS Null And ChiaveSelezione <> ''")
        cmd.Connection = cn
        cmd.Parameters.AddWithValue("@CodiceIva", TipoAddebito)
        cmd.Parameters.AddWithValue("@RIFERIMENTO", RIFERIMENTO)
        cmd.Parameters.AddWithValue("@Data", DataDal)
        cmd.Parameters.AddWithValue("@Data", DataAl)

        Dim myPOSTreader As OleDbDataReader = cmd.ExecuteReader()
        If myPOSTreader.Read Then

            ImportoAddebitoMese = campodbn(myPOSTreader.Item(0))
        End If

        myPOSTreader.Close()

        cn.Close()
    End Function

    Sub leggi(ByVal StringaConnessione As String, ByVal codiceospite As Integer, ByVal centroservizio As String, ByVal MyID As Integer)
        Dim cn As OleDbConnection


        cn = New Data.OleDb.OleDbConnection(StringaConnessione)

        cn.Open()
        Dim cmd As New OleDbCommand()
        cmd.CommandText = ("select * from ADDACR where centroservizio = '" & centroservizio & "' And CodiceOspite = " & codiceospite & " And ID = " & MyID)
        cmd.Connection = cn
        Dim myPOSTreader As OleDbDataReader = cmd.ExecuteReader()
        If myPOSTreader.Read Then

            id = Val(campodb(myPOSTreader.Item("TIPOMOV")))
            Data = Format(myPOSTreader.Item("DATA"), "dd/MM/yyyy")
            TipoMov = campodb(myPOSTreader.Item("TIPOMOV"))
            Descrizione = campodb(myPOSTreader.Item("DESCRIZIONE"))
            Progressivo = campodbn(myPOSTreader.Item("PROGRESSIVO"))

            IMPORTO = Format(campodbn(myPOSTreader.Item("IMPORTO")), "#,##0.00")
            RIFERIMENTO = campodb(myPOSTreader.Item("RIFERIMENTO"))
            PARENTE = Val(campodb(myPOSTreader.Item("PARENTE")))
            REGIONE = campodb(myPOSTreader.Item("REGIONE"))
            PROVINCIA = campodb(myPOSTreader.Item("PROVINCIA"))
            COMUNE = campodb(myPOSTreader.Item("COMUNE"))
            MESECOMPETENZA = Val(campodb(myPOSTreader.Item("MESECOMPETENZA")))
            ANNOCOMPETENZA = Val(campodb(myPOSTreader.Item("ANNOCOMPETENZA")))
            RETTA = campodb(myPOSTreader.Item("RETTA"))
            CodiceIva = campodb(myPOSTreader.Item("CodiceIva"))
            Elaborato = Val(campodb(myPOSTreader.Item("Elaborato")))
            NumeroRegistrazione = Val(campodb(myPOSTreader.Item("NumeroRegistrazione")))
            chiaveselezione = campodb(myPOSTreader.Item("chiaveselezione"))

            raggruppa = Val(campodb(myPOSTreader.Item("raggruppa")))

            IDEPERSONAM = campodb(myPOSTreader.Item("IDEPERSONAM"))

            Quantita = Val(campodb(myPOSTreader.Item("Quantita")))

        End If
        myPOSTreader.Close()
        cn.Close()
    End Sub

    Sub DeleteChiave(ByVal StringaConnessione As String, ByVal codiceospite As Integer, ByVal centroservizio As String, ByVal Mese As Integer, ByVal Anno As Integer)
        Dim cn As OleDbConnection


        cn = New Data.OleDb.OleDbConnection(StringaConnessione)

        cn.Open()
        Dim cmd As New OleDbCommand()
        cmd.CommandText = ("DELETE FROM ADDACR where centroservizio = '" & centroservizio & "' And CodiceOspite = " & codiceospite & " And (NOT chiaveselezione IS NULL and chiaveselezione <> '') And DATA >= ? And DATA <= ? And Elaborato = 0 ")
        cmd.Connection = cn
        cmd.Parameters.AddWithValue("@DATA", DateSerial(Anno, Mese, 1))
        cmd.Parameters.AddWithValue("@DATA", DateSerial(Anno, Mese, GiorniMese(Mese, Anno)))
        cmd.ExecuteNonQuery()

        cn.Close()
    End Sub

    Sub LeggiChiave(ByVal StringaConnessione As String, ByVal codiceospite As Integer, ByVal centroservizio As String, ByVal MyID As Integer)
        Dim cn As OleDbConnection


        cn = New Data.OleDb.OleDbConnection(StringaConnessione)

        cn.Open()
        Dim cmd As New OleDbCommand()
        cmd.CommandText = ("select * from ADDACR where centroservizio = '" & centroservizio & "' And CodiceOspite = " & codiceospite & " And chiaveselezione = ? And MeseCompetenza = ? And AnnoCompetenza = ?  ")
        cmd.Parameters.AddWithValue("@chiaveselezione", chiaveselezione)
        cmd.Parameters.AddWithValue("@MESECOMPETENZA", MESECOMPETENZA)
        cmd.Parameters.AddWithValue("@ANNOCOMPETENZA", ANNOCOMPETENZA)
        cmd.Connection = cn
        Dim myPOSTreader As OleDbDataReader = cmd.ExecuteReader()
        If myPOSTreader.Read Then

            ID = Val(campodb(myPOSTreader.Item("TIPOMOV")))
            Data = Format(myPOSTreader.Item("DATA"), "dd/MM/yyyy")
            TipoMov = campodb(myPOSTreader.Item("TIPOMOV"))
            Descrizione = campodb(myPOSTreader.Item("DESCRIZIONE"))
            Progressivo = campodbn(myPOSTreader.Item("PROGRESSIVO"))

            IMPORTO = Format(campodbn(myPOSTreader.Item("IMPORTO")), "#,##0.00")
            RIFERIMENTO = campodb(myPOSTreader.Item("RIFERIMENTO"))
            PARENTE = Val(campodb(myPOSTreader.Item("PARENTE")))
            REGIONE = campodb(myPOSTreader.Item("REGIONE"))
            PROVINCIA = campodb(myPOSTreader.Item("PROVINCIA"))
            COMUNE = campodb(myPOSTreader.Item("COMUNE"))
            MESECOMPETENZA = Val(campodb(myPOSTreader.Item("MESECOMPETENZA")))
            ANNOCOMPETENZA = Val(campodb(myPOSTreader.Item("ANNOCOMPETENZA")))
            RETTA = campodb(myPOSTreader.Item("RETTA"))
            CodiceIva = campodb(myPOSTreader.Item("CodiceIva"))
            Elaborato = Val(campodb(myPOSTreader.Item("Elaborato")))
            NumeroRegistrazione = Val(campodb(myPOSTreader.Item("NumeroRegistrazione")))
            chiaveselezione = campodb(myPOSTreader.Item("chiaveselezione"))

            raggruppa = Val(campodb(myPOSTreader.Item("raggruppa")))

            IDEPERSONAM = campodb(myPOSTreader.Item("IDEPERSONAM"))

            Quantita = Val(campodb(myPOSTreader.Item("Quantita")))
        End If
        myPOSTreader.Close()
        cn.Close()
    End Sub



    Public Sub Elimina(ByVal StringaConnessione As String)
        Dim cn As OleDbConnection
        Dim MySql As String

        MySql = ""

        cn = New Data.OleDb.OleDbConnection(StringaConnessione)

        cn.Open()
        Dim cmd As New OleDbCommand()
        cmd.CommandText = ("Delete from ADDACR where CentroServizio = '" & CENTROSERVIZIO & "' And  CodiceOspite = " & CodiceOspite & " And Progressivo = " & Progressivo)
        cmd.Connection = cn
        cmd.ExecuteNonQuery()
        cn.Close()
    End Sub

    Public Sub InserisciAddebito(ByVal StringaConnessione As String)
        Dim cn As OleDbConnection
        Dim MySql As String

        MySql = ""


        cn = New Data.OleDb.OleDbConnection(StringaConnessione)

        cn.Open()


        If Progressivo = 0 Then
            Dim cmd1 As New OleDbCommand()
            cmd1.CommandText = ("select MAX(Progressivo) from ADDACR where CentroServizio = '" & CENTROSERVIZIO & "' And  CodiceOspite = " & CodiceOspite)
            cmd1.Connection = cn
            Dim myPOSTreader1 As OleDbDataReader = cmd1.ExecuteReader()
            If myPOSTreader1.Read Then
                If Not IsDBNull(myPOSTreader1.Item(0)) Then
                    Progressivo = myPOSTreader1.Item(0) + 1
                Else
                    Progressivo = 1
                End If
            Else
                Progressivo = 1
            End If
        End If

        MySql = "INSERT INTO ADDACR (CentroServizio,CodiceOspite,Data,IMPORTO,TipoMov,Progressivo,Descrizione,RIFERIMENTO,PARENTE,REGIONE,PROVINCIA,COMUNE,MESECOMPETENZA,ANNOCOMPETENZA,RETTA,CodiceIva,Elaborato,Utente,DataAggiornamento,NumeroRegistrazione,chiaveselezione,raggruppa,IDEPERSONAM,Quantita) VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)"
        Dim cmdw As New OleDbCommand()
        cmdw.CommandText = (MySql)
        cmdw.Parameters.AddWithValue("@CentroServizio", CENTROSERVIZIO)
        cmdw.Parameters.AddWithValue("@CodiceOspite", CodiceOspite)
        cmdw.Parameters.AddWithValue("@Data", Data)
        cmdw.Parameters.AddWithValue("@IMPORTO", IMPORTO)
        cmdw.Parameters.AddWithValue("@TipoMov", TipoMov)
        cmdw.Parameters.AddWithValue("@Progressivo", Progressivo)
        cmdw.Parameters.AddWithValue("@Descrizione", Descrizione)
        cmdw.Parameters.AddWithValue("@RIFERIMENTO", RIFERIMENTO)
        cmdw.Parameters.AddWithValue("@PARENTE", PARENTE)
        cmdw.Parameters.AddWithValue("@REGIONE", REGIONE)
        cmdw.Parameters.AddWithValue("@PROVINCIA", PROVINCIA)
        cmdw.Parameters.AddWithValue("@COMUNE", COMUNE)
        cmdw.Parameters.AddWithValue("@MESECOMPETENZA", MESECOMPETENZA)
        cmdw.Parameters.AddWithValue("@ANNOCOMPETENZA", ANNOCOMPETENZA)
        cmdw.Parameters.AddWithValue("@RETTA", RETTA)
        cmdw.Parameters.AddWithValue("@CodiceIva", CodiceIva)
        cmdw.Parameters.AddWithValue("@Elaborato", Elaborato)
        cmdw.Parameters.AddWithValue("@Utente", Utente)
        cmdw.Parameters.AddWithValue("@DataAggiornamento", Now)
        cmdw.Parameters.AddWithValue("@NumeroRegistrazione", NumeroRegistrazione)
        cmdw.Parameters.AddWithValue("@chiaveselezione", chiaveselezione)
        cmdw.Parameters.AddWithValue("@raggruppa", raggruppa)

        cmdw.Parameters.AddWithValue("@IDEPERSONAM", IDEPERSONAM)
        cmdw.Parameters.AddWithValue("@Quantita", Quantita)


        'Quantita
        'raggruppa = Val(campodb(myPOSTreader.Item("raggruppa")))
        cmdw.Connection = cn
        cmdw.ExecuteNonQuery()

        cn.Close()
    End Sub
    Public Sub AggiornaDB(ByVal StringaConnessione As String)
        Dim cn As OleDbConnection
        Dim MySql As String

        MySql = ""


        cn = New Data.OleDb.OleDbConnection(StringaConnessione)

        cn.Open()
        Dim cmd As New OleDbCommand()
        cmd.CommandText = ("select * from ADDACR where CentroServizio = '" & CENTROSERVIZIO & "' And  CodiceOspite = " & CodiceOspite & " And Progressivo = " & Progressivo)
        cmd.Connection = cn
        Dim myPOSTreader As OleDbDataReader = cmd.ExecuteReader()
        If myPOSTreader.Read Then
            MySql = "UPDATE ADDACR SET IMPORTO=?,TipoMov=?,Progressivo=?,Descrizione=?," & _
                    "RIFERIMENTO=?,PARENTE=?,REGIONE=?,PROVINCIA=?,COMUNE=?," & _
                    "MESECOMPETENZA=?, ANNOCOMPETENZA=?,RETTA=?,CodiceIva=?, " & _
                    " Elaborato = ?,Data = ?,Utente=?,DataAggiornamento=?,NumeroRegistrazione =?,chiaveselezione=?,raggruppa =?,IDEPERSONAM = ?,Quantita=? " & _
                    " WHERE  CentroServizio = '" & CENTROSERVIZIO & "' And  CodiceOspite = " & CodiceOspite & " And Progressivo = " & Progressivo
            Dim cmdw As New OleDbCommand()
            cmdw.CommandText = (MySql)
            cmdw.Parameters.AddWithValue("@IMPORTO", IMPORTO)
            cmdw.Parameters.AddWithValue("@TipoMov", TipoMov)
            cmdw.Parameters.AddWithValue("@Progressivo", Progressivo)
            cmdw.Parameters.AddWithValue("@Descrizione", Descrizione)
            cmdw.Parameters.AddWithValue("@RIFERIMENTO", RIFERIMENTO)
            cmdw.Parameters.AddWithValue("@PARENTE", PARENTE)
            cmdw.Parameters.AddWithValue("@REGIONE", REGIONE)
            cmdw.Parameters.AddWithValue("@PROVINCIA", PROVINCIA)
            cmdw.Parameters.AddWithValue("@COMUNE", COMUNE)
            cmdw.Parameters.AddWithValue("@MESECOMPETENZA", MESECOMPETENZA)
            cmdw.Parameters.AddWithValue("@ANNOCOMPETENZA", ANNOCOMPETENZA)
            cmdw.Parameters.AddWithValue("@RETTA", RETTA)
            cmdw.Parameters.AddWithValue("@CodiceIva", CodiceIva)
            cmdw.Parameters.AddWithValue("@Elaborato", Elaborato)
            cmdw.Parameters.AddWithValue("@Data", Data)
            cmdw.Parameters.AddWithValue("@Utente", Utente)
            cmdw.Parameters.AddWithValue("@DataAggiornamento", Now)
            cmdw.Parameters.AddWithValue("@NumeroRegistrazione", NumeroRegistrazione)
            cmdw.Parameters.AddWithValue("@chiaveselezione", chiaveselezione)
            cmdw.Parameters.AddWithValue("@raggruppa", raggruppa)
            cmdw.Parameters.AddWithValue("@IDEPERSONAM", IDEPERSONAM)
            cmdw.Parameters.AddWithValue("@Quantita", Quantita)
            cmdw.Connection = cn
            cmdw.ExecuteNonQuery()
        Else
            If Progressivo = 0 Then
                Dim cmd1 As New OleDbCommand()
                cmd1.CommandText = ("select MAX(Progressivo) from ADDACR where CentroServizio = '" & CENTROSERVIZIO & "' And  CodiceOspite = " & CodiceOspite)
                cmd1.Connection = cn
                Dim myPOSTreader1 As OleDbDataReader = cmd1.ExecuteReader()
                If myPOSTreader1.Read Then
                    If Not IsDBNull(myPOSTreader1.Item(0)) Then
                        Progressivo = myPOSTreader1.Item(0) + 1
                    Else
                        Progressivo = 1
                    End If
                Else
                    Progressivo = 1
                End If
            End If
            MySql = "INSERT INTO ADDACR (CentroServizio,CodiceOspite,Data,IMPORTO,TipoMov,Progressivo,Descrizione,RIFERIMENTO,PARENTE,REGIONE,PROVINCIA,COMUNE,MESECOMPETENZA,ANNOCOMPETENZA,RETTA,CodiceIva,Elaborato,Utente,DataAggiornamento,NumeroRegistrazione,chiaveselezione,raggruppa,IDEPERSONAM,Quantita) VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)"
            Dim cmdw As New OleDbCommand()
            cmdw.CommandText = (MySql)
            cmdw.Parameters.AddWithValue("@CentroServizio", CENTROSERVIZIO)
            cmdw.Parameters.AddWithValue("@CodiceOspite", CodiceOspite)
            cmdw.Parameters.AddWithValue("@Data", Data)
            cmdw.Parameters.AddWithValue("@IMPORTO", IMPORTO)
            cmdw.Parameters.AddWithValue("@TipoMov", TipoMov)
            cmdw.Parameters.AddWithValue("@Progressivo", Progressivo)
            cmdw.Parameters.AddWithValue("@Descrizione", Descrizione)
            cmdw.Parameters.AddWithValue("@RIFERIMENTO", RIFERIMENTO)
            cmdw.Parameters.AddWithValue("@PARENTE", PARENTE)
            cmdw.Parameters.AddWithValue("@REGIONE", REGIONE)
            cmdw.Parameters.AddWithValue("@PROVINCIA", PROVINCIA)
            cmdw.Parameters.AddWithValue("@COMUNE", COMUNE)
            cmdw.Parameters.AddWithValue("@MESECOMPETENZA", MESECOMPETENZA)
            cmdw.Parameters.AddWithValue("@ANNOCOMPETENZA", ANNOCOMPETENZA)
            cmdw.Parameters.AddWithValue("@RETTA", RETTA)
            cmdw.Parameters.AddWithValue("@CodiceIva", CodiceIva)
            cmdw.Parameters.AddWithValue("@Elaborato", Elaborato)
            cmdw.Parameters.AddWithValue("@Utente", Utente)
            cmdw.Parameters.AddWithValue("@DataAggiornamento", Now)
            cmdw.Parameters.AddWithValue("@NumeroRegistrazione", NumeroRegistrazione)
            cmdw.Parameters.AddWithValue("@chiaveselezione", chiaveselezione)
            cmdw.Parameters.AddWithValue("@raggruppa", raggruppa)
            cmdw.Parameters.AddWithValue("@IDEPERSONAM", IDEPERSONAM)
            cmdw.Parameters.AddWithValue("@Quantita", Quantita)

            'Quantita
            cmdw.Connection = cn
            cmdw.ExecuteNonQuery()
        End If
        myPOSTreader.Close()
        cn.Close()
    End Sub


    Sub loaddati(ByVal StringaConnessione As String, ByVal codiceospite As Integer, ByVal centroservizio As String, ByVal Tabella As System.Data.DataTable, Optional ByVal DataDal As Date = Nothing, Optional ByVal DataAl As Date = Nothing, Optional ByVal Tipo As String = "")
        Dim cn As OleDbConnection


        cn = New Data.OleDb.OleDbConnection(StringaConnessione)

        cn.Open()
        Dim cmd As New OleDbCommand()

        Dim Condizione As String = ""

        If Tipo = "O" Then
            Condizione = " And Riferimento = 'O' "
        End If
        If Tipo = "P" Then
            Condizione = " And Riferimento = 'P' "
        End If
        If Tipo = "C" Then
            Condizione = " And Riferimento = 'C' "
        End If
        If Tipo = "J" Then
            Condizione = " And Riferimento = 'J' "
        End If
        If Tipo = "R" Then
            Condizione = " And Riferimento = 'R' "
        End If
        

        If IsNothing(DataDal) Or Year(DataDal) < 1920 Then
            cmd.CommandText = ("select * from ADDACR where centroservizio = '" & centroservizio & "' And CodiceOspite = " & codiceospite & Condizione & " Order by Data Desc")
        Else
            cmd.CommandText = ("select * from ADDACR where centroservizio = '" & centroservizio & "' And CodiceOspite = " & codiceospite & Condizione & " And Data >= ? And Data <= ? Order by Data Desc")
            cmd.Parameters.AddWithValue("@Data", DataDal)
            cmd.Parameters.AddWithValue("@Data", DataAl)
        End If
        cmd.Connection = cn
        Dim sb As StringBuilder = New StringBuilder

        Tabella.Clear()
        Tabella.Columns.Clear()
        Tabella.Columns.Add("ID", GetType(Long))
        Tabella.Columns.Add("Data", GetType(String))
        Tabella.Columns.Add("TIPOMOV", GetType(String))
        Tabella.Columns.Add("Descrizione", GetType(String))
        Tabella.Columns.Add("IMPORTO", GetType(String))
        Tabella.Columns.Add("RIFERIMENTO", GetType(String))
        Tabella.Columns.Add("PARENTE", GetType(String))
        Tabella.Columns.Add("REGIONE", GetType(String))
        Tabella.Columns.Add("COMUNE", GetType(String))
        Tabella.Columns.Add("PERIODO", GetType(String))
        Tabella.Columns.Add("ELABORATO", GetType(String))

        Dim myPOSTreader As OleDbDataReader = cmd.ExecuteReader()
        Do While myPOSTreader.Read

            Dim myriga As System.Data.DataRow = Tabella.NewRow()
            myriga(0) = myPOSTreader.Item("ID")
            myriga(1) = Format(myPOSTreader.Item("DATA"), "dd/MM/yyyy")

            If campodb(myPOSTreader.Item("TIPOMOV")) = "AD" Then
                myriga(2) = "Addebito"
            Else
                myriga(2) = "Accredito"
            End If
            myriga(3) = myPOSTreader.Item("DESCRIZIONE")

            myriga(4) = Format(campodbn(myPOSTreader.Item("IMPORTO")), "#,##0.00")

            If campodb(myPOSTreader.Item("RIFERIMENTO")) = "O" Then
                myriga(5) = "Ospite"
            End If
            If campodb(myPOSTreader.Item("RIFERIMENTO")) = "P" Then
                myriga(5) = "Parente"
            End If
            If campodb(myPOSTreader.Item("RIFERIMENTO")) = "C" Then
                myriga(5) = "Comune"
            End If
            If campodb(myPOSTreader.Item("RIFERIMENTO")) = "R" Then
                myriga(5) = "Regione"
            End If
            If campodb(myPOSTreader.Item("RIFERIMENTO")) = "J" Then
                myriga(5) = "Jolly"
            End If



            If Val(myPOSTreader.Item("PARENTE")) > 0 Then
                Dim KPar As New Cls_Parenti

                KPar.Leggi(StringaConnessione, codiceospite, Val(myPOSTreader.Item("PARENTE")))
                myriga(6) = KPar.Nome
            End If

            Dim kR As New ClsUSL

            kR.CodiceRegione = campodb(myPOSTreader.Item("REGIONE"))
            kR.Leggi(StringaConnessione)
            myriga(7) = kR.Nome

            Dim k As New ClsComune

            k.Provincia = campodb(myPOSTreader.Item("PROVINCIA"))
            k.Comune = campodb(myPOSTreader.Item("COMUNE"))
            k.DecodficaComune(StringaConnessione)
            myriga(8) = k.Descrizione

            myriga(9) = campodb(myPOSTreader.Item("MESECOMPETENZA")) & "/" & campodb(myPOSTreader.Item("ANNOCOMPETENZA"))

            myriga(10) = campodb(myPOSTreader.Item("ELABORATO"))

            Tabella.Rows.Add(myriga)
        Loop
        myPOSTreader.Close()
        cn.Close()
    End Sub
End Class
