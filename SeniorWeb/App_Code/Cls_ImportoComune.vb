Imports Microsoft.VisualBasic
Imports System.Data.OleDb
Imports System.Web.Hosting

Public Class Cls_ImportoComune


    Public CENTROSERVIZIO As String
    Public CODICEOSPITE As Long
    Public Data As Date
    Public Importo As Double
    Public Tipo As String
    Public Importo1 As Double
    Public Importo2 As Double
    Public Importo3 As Double
    Public Importo4 As Double
    Public MensileFisso As Integer
    Public PROV As String
    Public COMUNE As String
    Public DescrizioneRiga As String
    Public DataSosia As Date

    Public Sub EliminaAll(ByVal StringaConnessione As String)
        Dim cn As OleDbConnection
        Dim MySql As String

        MySql = ""

        cn = New Data.OleDb.OleDbConnection(StringaConnessione)

        cn.Open()
        Dim cmd As New OleDbCommand()
        cmd.CommandText = ("Delete from IMPORTOCOMUNE where CentroServizio = '" & CENTROSERVIZIO & "' And  CodiceOspite = " & CODICEOSPITE)

        cmd.Connection = cn
        cmd.ExecuteNonQuery()
        cn.Close()
    End Sub

    Public Sub Elimina(ByVal StringaConnessione As String)
        Dim cn As OleDbConnection
        Dim MySql As String

        MySql = ""

        cn = New Data.OleDb.OleDbConnection(StringaConnessione)

        cn.Open()
        Dim cmd As New OleDbCommand()
        cmd.CommandText = ("Delete from IMPORTOCOMUNE where CentroServizio = '" & CENTROSERVIZIO & "' And  CodiceOspite = " & CODICEOSPITE & "  And Data = ?")
        cmd.Parameters.AddWithValue("@DATAINIZIO", Data)
        cmd.Connection = cn
        cmd.ExecuteNonQuery()
        cn.Close()
    End Sub

    Function campodbd(ByVal oggetto As Object) As Date
        If IsDBNull(oggetto) Then
            Return Nothing
        Else
            Return oggetto
        End If
    End Function
    Function campodb(ByVal oggetto As Object) As String
        If IsDBNull(oggetto) Then
            Return ""
        Else
            Return oggetto
        End If
    End Function

    Function campodbN(ByVal oggetto As Object) As Double
        If IsDBNull(oggetto) Then
            Return 0
        Else
            Return oggetto
        End If
    End Function
    Public Sub AggiornaDB(ByVal StringaConnessione As String)
        Dim cn As OleDbConnection
        Dim MySql As String

        MySql = ""

        If IsDate(DataSosia) Then
            If Year(DataSosia) < 1900 Then
                DataSosia = Data
            End If
        Else
            DataSosia = Data
        End If
        cn = New Data.OleDb.OleDbConnection(StringaConnessione)

        cn.Open()
        Dim cmd As New OleDbCommand()
        cmd.CommandText = ("select * from IMPORTOCOMUNE where CentroServizio = '" & CENTROSERVIZIO & "' And  CodiceOspite = " & CODICEOSPITE & "  And Data = ?")
        cmd.Parameters.AddWithValue("@Data", Data)
        cmd.Connection = cn
        Dim myPOSTreader As OleDbDataReader = cmd.ExecuteReader()
        If myPOSTreader.Read Then
            MySql = "UPDATE IMPORTOCOMUNE SET IMPORTO = ?,TipoRetta=?,PROV=?,COMUNE=?,IMPORTO1 = ?,IMPORTO2 = ?,IMPORTO3 = ?,IMPORTO4 = ?,DescrizioneRiga=?,MensileFisso = ?,DataSosia = ? " & _
                    " WHERE  CentroServizio = '" & CENTROSERVIZIO & "' And  CodiceOspite = " & CODICEOSPITE & " And Data = ?"
            Dim cmdw As New OleDbCommand()
            cmdw.CommandText = (MySql)
            cmdw.Parameters.AddWithValue("@IMPORTO", Importo)
            cmdw.Parameters.AddWithValue("@TIPOIMPORTO", Tipo)
            cmdw.Parameters.AddWithValue("@PROV", PROV)
            cmdw.Parameters.AddWithValue("@COMUNE", COMUNE)
            cmdw.Parameters.AddWithValue("@IMPORTO1", Importo1)
            cmdw.Parameters.AddWithValue("@IMPORTO2", Importo2)
            cmdw.Parameters.AddWithValue("@IMPORTO3", Importo3)
            cmdw.Parameters.AddWithValue("@IMPORTO4", Importo4)            
            cmdw.Parameters.AddWithValue("@DescrizioneRiga", DescrizioneRiga)
            cmdw.Parameters.AddWithValue("@MensileFisso", MensileFisso)
            cmdw.Parameters.AddWithValue("@DataSosia", DataSosia)
            cmdw.Parameters.AddWithValue("@Data", Data)
            cmdw.Connection = cn
            cmdw.ExecuteNonQuery()
        Else
            MySql = "INSERT INTO IMPORTOCOMUNE (CentroServizio,CodiceOspite,Data,IMPORTO,TipoRetta,PROV,COMUNE,IMPORTO1,IMPORTO2,IMPORTO3,IMPORTO4,DescrizioneRiga,MensileFisso,DataSosia) VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?,?)"
            Dim cmdw As New OleDbCommand()
            cmdw.CommandText = (MySql)
            cmdw.Parameters.AddWithValue("@CentroServizio", CentroServizio)
            cmdw.Parameters.AddWithValue("@CodiceOspite", CODICEOSPITE)
            cmdw.Parameters.AddWithValue("@Data", Data)
            cmdw.Parameters.AddWithValue("@IMPORTO", Importo)
            cmdw.Parameters.AddWithValue("@TIPOIMPORTO", Tipo)
            cmdw.Parameters.AddWithValue("@PROV", PROV)
            cmdw.Parameters.AddWithValue("@COMUNE", COMUNE)
            cmdw.Parameters.AddWithValue("@IMPORTO1", Importo1)
            cmdw.Parameters.AddWithValue("@IMPORTO2", Importo2)
            cmdw.Parameters.AddWithValue("@IMPORTO3", Importo3)
            cmdw.Parameters.AddWithValue("@IMPORTO4", Importo4)
            cmdw.Parameters.AddWithValue("@DescrizioneRiga", DescrizioneRiga)
            cmdw.Parameters.AddWithValue("@MensileFisso", MensileFisso)
            cmdw.Parameters.AddWithValue("@DataSosia", DataSosia)

            cmdw.Connection = cn
            cmdw.ExecuteNonQuery()
        End If
        myPOSTreader.Close()
        cn.Close()
    End Sub

    Public Sub AggiornaDaTabella(ByVal StringaConnessione As String, ByVal Tabella As System.Data.DataTable)
        Dim cn As OleDbConnection
        Dim MySql As String

        MySql = ""

        cn = New Data.OleDb.OleDbConnection(StringaConnessione)

        cn.Open()


        Dim Transan As OleDbTransaction = cn.BeginTransaction()

        Dim cmd As New OleDbCommand()
        cmd.CommandText = "Delete from IMPORTOCOMUNE where CentroServizio = '" & CENTROSERVIZIO & "' And  CodiceOspite = " & CODICEOSPITE
        cmd.Connection = cn
        cmd.Transaction = Transan
        cmd.ExecuteNonQuery()

        Dim i As Long

        For i = 0 To Tabella.Rows.Count - 1
            If IsDate(Tabella.Rows(i).Item(0)) And Tabella.Rows(i).Item(0) <> "" Then
                MySql = "INSERT INTO IMPORTOCOMUNE (CentroServizio,CodiceOspite,Data,IMPORTO,TipoRetta,PROV,COMUNE,IMPORTO1,IMPORTO2,IMPORTO3,IMPORTO4,DescrizioneRiga,MensileFisso,DataSosia) VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?,?)"
                Dim cmdw As New OleDbCommand()
                cmdw.CommandText = (MySql)
                cmdw.Parameters.AddWithValue("@CentroServizio", CENTROSERVIZIO)
                cmdw.Parameters.AddWithValue("@CodiceOspite", CODICEOSPITE)
                Dim xData As Date

                xData = Tabella.Rows(i).Item(0)
                cmdw.Parameters.AddWithValue("@Data", xData)
                cmdw.Parameters.AddWithValue("@IMPORTO", Convert.ToDouble(Tabella.Rows(i).Item(1)))
                cmdw.Parameters.AddWithValue("@TIPOIMPORTO", Tabella.Rows(i).Item(2))
                cmdw.Parameters.AddWithValue("@PROV", Tabella.Rows(i).Item(3))
                cmdw.Parameters.AddWithValue("@COMUNE", Tabella.Rows(i).Item(4))
                cmdw.Parameters.AddWithValue("@IMPORTO1", campodbN(Tabella.Rows(i).Item(6)))
                cmdw.Parameters.AddWithValue("@IMPORTO2", campodbN(Tabella.Rows(i).Item(7)))
                cmdw.Parameters.AddWithValue("@IMPORTO3", campodbN(Tabella.Rows(i).Item(8)))
                cmdw.Parameters.AddWithValue("@IMPORTO4", campodbN(Tabella.Rows(i).Item(9)))
                cmdw.Parameters.AddWithValue("@DescrizioneRiga", Tabella.Rows(i).Item(10))
                cmdw.Parameters.AddWithValue("@MensileFisso", Tabella.Rows(i).Item(11))

                If IsDate(Tabella.Rows(i).Item(12)) Then
                    If Year(Tabella.Rows(i).Item(12)) > 1900 Then
                        xData = Tabella.Rows(i).Item(12)
                    End If
                End If
                cmdw.Parameters.AddWithValue("@DataSosia", xData)

                cmdw.Transaction = Transan
                cmdw.Connection = cn
                cmdw.ExecuteNonQuery()
            End If
        Next
        Transan.Commit()
        cn.Close()

    End Sub

    Sub loaddati(ByVal StringaConnessione As String, ByVal codiceospite As Integer, ByVal centroservizio As String, ByVal Tabella As System.Data.DataTable)
        Dim cn As OleDbConnection


        cn = New Data.OleDb.OleDbConnection(StringaConnessione)

        cn.Open()
        Dim cmd As New OleDbCommand()
        cmd.CommandText = ("select * from IMPORTOCOMUNE where centroservizio = '" & centroservizio & "' And CodiceOspite = " & codiceospite & " Order By Data")
        cmd.Connection = cn
        Dim sb As StringBuilder = New StringBuilder

        Tabella.Clear()
        Tabella.Columns.Clear()
        Tabella.Columns.Add("Data", GetType(String))
        Tabella.Columns.Add("Importo", GetType(String))
        Tabella.Columns.Add("Tipo", GetType(String))
        Tabella.Columns.Add("PROV", GetType(String))
        Tabella.Columns.Add("COMUNE", GetType(String))
        Tabella.Columns.Add("DECODIFICACOMUNE", GetType(String))
        Tabella.Columns.Add("Importo1", GetType(String))
        Tabella.Columns.Add("Importo2", GetType(String))
        Tabella.Columns.Add("Importo3", GetType(String))
        Tabella.Columns.Add("Importo4", GetType(String))
        Tabella.Columns.Add("DescrizioneRiga", GetType(String))
        Tabella.Columns.Add("MensileFisso", GetType(String))
        Tabella.Columns.Add("DataSosia", GetType(String))


        Dim myPOSTreader As OleDbDataReader = cmd.ExecuteReader()
        Do While myPOSTreader.Read

            Dim myriga As System.Data.DataRow = Tabella.NewRow()
            myriga(0) = Format(myPOSTreader.Item("DATA"), "dd/MM/yyyy")
            myriga(1) = Format(campodbN(myPOSTreader.Item("Importo")), "#,##0.00")

            If campodbN(myPOSTreader.Item("MensileFisso")) = 0 Then
                myriga(2) = campodb(myPOSTreader.Item("TIPORETTA"))
            Else
                myriga(2) = "N"
            End If

            myriga(3) = myPOSTreader.Item("PROV")
            myriga(4) = myPOSTreader.Item("COMUNE")

            Dim x As New ClsComune
            x.Provincia = campodb(myPOSTreader.Item("PROV"))
            x.Comune = campodb(myPOSTreader.Item("COMUNE"))
            x.DecodficaComune(StringaConnessione)
            myriga(5) = x.Descrizione

            myriga(6) = Format(campodbN(myPOSTreader.Item("IMPORTO1")), "#,##0.00")
            myriga(7) = Format(campodbN(myPOSTreader.Item("IMPORTO2")), "#,##0.00")
            myriga(8) = Format(campodbN(myPOSTreader.Item("IMPORTO3")), "#,##0.00")
            myriga(9) = Format(campodbN(myPOSTreader.Item("IMPORTO4")), "#,##0.00")

            myriga(10) = campodb(myPOSTreader.Item("DescrizioneRiga"))
            myriga(11) = campodbN(myPOSTreader.Item("MensileFisso"))
            myriga(12) = campodbd(myPOSTreader.Item("DataSosia"))
            'MensileFisso

            Tabella.Rows.Add(myriga)
        Loop
        myPOSTreader.Close()
        If Tabella.Rows.Count = 0 Then
            Dim myriga As System.Data.DataRow = Tabella.NewRow()

            Dim Mov As New Cls_Movimenti

            Mov.CENTROSERVIZIO = centroservizio
            Mov.CodiceOspite = codiceospite
            Mov.UltimaDataAccoglimento(StringaConnessione)
            myriga(0) = Mov.Data

            myriga(1) = 0
            myriga(2) = "G"
            myriga(3) = ""
            myriga(4) = ""
            myriga(5) = ""
            myriga(6) = 0
            myriga(7) = 0
            myriga(8) = 0
            myriga(9) = 0
            myriga(10) = ""
            myriga(11) = 0
            myriga(12) = Mov.Data
            Tabella.Rows.Add(myriga)
        End If
        cn.Close()
    End Sub

    Public Sub UltimaData(ByVal StringaConnessione As String, ByVal CodiceOspite As Long, ByVal CentroServizio As String)
        Dim cn As OleDbConnection



        cn = New Data.OleDb.OleDbConnection(StringaConnessione)

        cn.Open()
        Dim cmd As New OleDbCommand()
        cmd.CommandText = ("select * from IMPORTOCOMUNE where CentroServizio = '" & CentroServizio & "' And  CodiceOspite = " & CodiceOspite & " Order by Data Desc")
        cmd.Connection = cn
        Dim sb As StringBuilder = New StringBuilder

        Dim myPOSTreader As OleDbDataReader = cmd.ExecuteReader()
        If myPOSTreader.Read Then

            CodiceOspite = campodb(myPOSTreader.Item("CodiceOspite"))
            Importo = campodb(myPOSTreader.Item("Importo"))
            Tipo = campodb(myPOSTreader.Item("TIPORETTA"))
            Data = campodb(myPOSTreader.Item("Data"))

            PROV = campodb(myPOSTreader.Item("PROV"))
            COMUNE = campodb(myPOSTreader.Item("COMUNE"))
            If campodb(myPOSTreader.Item("Importo1")) = "" Then
                Importo1 = 0
            Else
                Importo1 = campodb(myPOSTreader.Item("Importo1"))
            End If

            If campodb(myPOSTreader.Item("Importo2")) = "" Then
                Importo2 = 0
            Else
                Importo2 = campodb(myPOSTreader.Item("Importo2"))
            End If
            If campodb(myPOSTreader.Item("Importo3")) = "" Then
                Importo3 = 0
            Else
                Importo3 = campodb(myPOSTreader.Item("Importo3"))
            End If

            If campodb(myPOSTreader.Item("Importo4")) = "" Then
                Importo4 = 0
            Else
                Importo4 = campodb(myPOSTreader.Item("Importo4"))
            End If

            DescrizioneRiga = campodb(myPOSTreader.Item("DescrizioneRiga"))

            MensileFisso = campodbN(myPOSTreader.Item("MensileFisso"))

            DataSosia = campodbd(myPOSTreader.Item("DataSosia"))
        End If
        myPOSTreader.Close()
        cn.Close()
    End Sub


    Public Sub UltimaDataAData(ByVal StringaConnessione As String, ByVal CodiceOspite As Long, ByVal CentroServizio As String, ByVal Data As Date)
        Dim cn As OleDbConnection



        cn = New Data.OleDb.OleDbConnection(StringaConnessione)

        cn.Open()
        Dim cmd As New OleDbCommand()
        cmd.CommandText = ("select * from IMPORTOCOMUNE where CentroServizio = '" & CentroServizio & "' And  CodiceOspite = " & CodiceOspite & " And Data <= ? Order by Data Desc")
        cmd.Connection = cn
        cmd.Parameters.AddWithValue("@Data", Data)


        Dim myPOSTreader As OleDbDataReader = cmd.ExecuteReader()
        If myPOSTreader.Read Then

            CodiceOspite = campodb(myPOSTreader.Item("CodiceOspite"))
            Importo = campodb(myPOSTreader.Item("Importo"))
            Tipo = campodb(myPOSTreader.Item("TIPORETTA"))
            Data = campodb(myPOSTreader.Item("Data"))

            PROV = campodb(myPOSTreader.Item("PROV"))
            COMUNE = campodb(myPOSTreader.Item("COMUNE"))
            If campodb(myPOSTreader.Item("Importo1")) = "" Then
                Importo1 = 0
            Else
                Importo1 = campodb(myPOSTreader.Item("Importo1"))
            End If

            If campodb(myPOSTreader.Item("Importo2")) = "" Then
                Importo2 = 0
            Else
                Importo2 = campodb(myPOSTreader.Item("Importo2"))
            End If
            If campodb(myPOSTreader.Item("Importo3")) = "" Then
                Importo3 = 0
            Else
                Importo3 = campodb(myPOSTreader.Item("Importo3"))
            End If

            If campodb(myPOSTreader.Item("Importo4")) = "" Then
                Importo4 = 0
            Else
                Importo4 = campodb(myPOSTreader.Item("Importo4"))
            End If

            DescrizioneRiga = campodb(myPOSTreader.Item("DescrizioneRiga"))

            MensileFisso = campodbN(myPOSTreader.Item("MensileFisso"))

            DataSosia = campodbd(myPOSTreader.Item("DataSosia"))
        End If
        myPOSTreader.Close()
        cn.Close()
    End Sub

End Class
