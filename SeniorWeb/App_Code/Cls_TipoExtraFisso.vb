Imports Microsoft.VisualBasic
Imports System.Data.OleDb
Imports System.Web.Hosting

Public Class Cls_TipoExtraFisso
    
	
    Public CENTROSERVIZIO As String
    Public Struttura As String
    Public CODICEEXTRA As String
    Public Descrizione As String
    Public IMPORTO As Double
    Public Mastro As Long
    Public Conto As Long
    Public Sottoconto As Long
    Public TipoImporto As String
    Public CodiceIva As String
    Public Ripartizione As String
    Public Anticipato As String
    Public DescrizioneInterna As String
    Public DataScadenza As Date
    Public FuoriRetta As Integer



    Public Sub UpDateDropBox(ByVal StringaConnessione As String, ByRef appoggio As DropDownList)
        Dim cn As OleDbConnection



        cn = New Data.OleDb.OleDbConnection(StringaConnessione)

        cn.Open()
        Dim cmd As New OleDbCommand()
        cmd.CommandText = ("select * from TABELLAEXTRAFISSI where (SCADENZA IS NULL OR SCADENZA > GETDATE())")
        cmd.Connection = cn
        Dim sb As StringBuilder = New StringBuilder

        appoggio.Items.Clear()
        Dim myPOSTreader As OleDbDataReader = cmd.ExecuteReader()
        Do While myPOSTreader.Read
            appoggio.Items.Add(campodb(myPOSTreader.Item("Descrizione")) & " " & campodb(myPOSTreader.Item("DESCRIZIONEINTERNA")) & " (" & campodb(myPOSTreader.Item("RIPARTIZIONE")) & ")")
            appoggio.Items(appoggio.Items.Count - 1).Value = myPOSTreader.Item("CODICEEXTRA")
        Loop
        myPOSTreader.Close()
        cn.Close()
        appoggio.Items.Add("")
        appoggio.Items(appoggio.Items.Count - 1).Value = ""
        appoggio.Items(appoggio.Items.Count - 1).Selected = True

    End Sub

    Function MaxTipoExtra(ByVal StringaConnessione As String) As String
        Dim cn As OleDbConnection


        MaxTipoExtra = ""

        cn = New Data.OleDb.OleDbConnection(StringaConnessione)

        cn.Open()
        Dim cmd As New OleDbCommand()
        cmd.CommandText = ("select max(CASE WHEN (len(CODICEEXTRA) = 1) THEN '0' + CODICEEXTRA ELSE CODICEEXTRA END) from TABELLAEXTRAFISSI ")
        cmd.Connection = cn
        Dim MaxAddebito As OleDbDataReader = cmd.ExecuteReader()
        If MaxAddebito.Read Then
            Dim MassimoLetto As String

            MassimoLetto = campodb(MaxAddebito.Item(0))

            If MassimoLetto = "Z9" Then
                MassimoLetto = "a0"
                cn.Close()
                Return MassimoLetto
            End If

            If MassimoLetto = "99" Then
                MassimoLetto = "A0"
            Else
                If Mid(MassimoLetto & Space(10), 1, 1) > "0" And Mid(MassimoLetto & Space(10), 1, 1) > "9" Then
                    If Mid(MassimoLetto & Space(10), 2, 1) < "9" Then
                        MassimoLetto = Mid(MassimoLetto & Space(10), 1, 1) & Val(Mid(MassimoLetto & Space(10), 2, 1)) + 1
                    Else
                        Dim CodiceAscii As String
                        CodiceAscii = Asc(Mid(MassimoLetto & Space(10), 1, 1))

                        MassimoLetto = Chr(CodiceAscii + 1) & "0"
                    End If
                Else
                    MassimoLetto = Format(Val(MassimoLetto) + 1, "00")
                End If
            End If

            Return MassimoLetto
        Else
            Return "01"
        End If
        cn.Close()

    End Function

    Sub Elimina(ByVal StringaConnessione As String)
        Dim cn As OleDbConnection


        cn = New Data.OleDb.OleDbConnection(StringaConnessione)

        cn.Open()
        Dim cmd As New OleDbCommand()
        cmd.CommandText = ("Delete from TABELLAEXTRAFISSI where CODICEEXTRA = '" & CODICEEXTRA & "' ")
        cmd.Connection = cn
        cmd.ExecuteNonQuery()
        cn.Close()
    End Sub
    
    Sub Leggi(ByVal StringaConnessione As String)
        Dim cn As OleDbConnection


        cn = New Data.OleDb.OleDbConnection(StringaConnessione)

        cn.Open()
        Dim cmd As New OleDbCommand()
        cmd.CommandText = ("select * from TABELLAEXTRAFISSI where CODICEEXTRA = '" & CODICEEXTRA & "' ")
        cmd.Connection = cn
        Dim myPOSTreader As OleDbDataReader = cmd.ExecuteReader()
        If myPOSTreader.Read Then
            CODICEEXTRA = campodb(myPOSTreader.Item("CODICEEXTRA"))
            Descrizione = campodb(myPOSTreader.Item("DESCRIZIONE"))
            DescrizioneInterna = campodb(myPOSTreader.Item("DESCRIZIONEINTERNA"))
            IMPORTO = campodbN(myPOSTreader.Item("IMPORTO"))
            Mastro = campodbN(myPOSTreader.Item("Mastro"))
            CONTO = campodbN(myPOSTreader.Item("CONTO"))
            SOTTOCONTO = campodbN(myPOSTreader.Item("SOTTOCONTO"))

            TipoImporto = campodb(myPOSTreader.Item("TipoImporto"))
            CodiceIva = campodb(myPOSTreader.Item("CodiceIva"))
            Ripartizione = campodb(myPOSTreader.Item("Ripartizione"))

            Anticipato = campodb(myPOSTreader.Item("Anticipato"))

            CENTROSERVIZIO = campodb(myPOSTreader.Item("CENTROSERVIZIO"))
            Struttura = campodb(myPOSTreader.Item("Struttura"))

            DataScadenza = campodbd(myPOSTreader.Item("Scadenza"))
            FuoriRetta = Val(campodb(myPOSTreader.Item("FuoriRetta")))

        End If
        myPOSTreader.Close()
        cn.Close()
    End Sub
    Sub LeggiDaDescrizione(ByVal StringaConnessione As String)
        Dim cn As OleDbConnection


        cn = New Data.OleDb.OleDbConnection(StringaConnessione)

        cn.Open()
        Dim cmd As New OleDbCommand()
        cmd.CommandText = ("select * from TABELLAEXTRAFISSI where Descrizione Like '" & Descrizione & "' ")
        cmd.Connection = cn
        Dim myPOSTreader As OleDbDataReader = cmd.ExecuteReader()
        If myPOSTreader.Read Then
            CENTROSERVIZIO = campodb(myPOSTreader.Item("CENTROSERVIZIO"))
            CODICEEXTRA = campodb(myPOSTreader.Item("CODICEEXTRA"))
            Descrizione = campodb(myPOSTreader.Item("DESCRIZIONE"))
            DescrizioneInterna = campodb(myPOSTreader.Item("DESCRIZIONEINTERNA"))
            IMPORTO = campodbN(myPOSTreader.Item("IMPORTO"))
            Mastro = campodbN(myPOSTreader.Item("Mastro"))
            Conto = campodbN(myPOSTreader.Item("CONTO"))
            Sottoconto = campodbN(myPOSTreader.Item("SOTTOCONTO"))

            TipoImporto = campodb(myPOSTreader.Item("TipoImporto"))
            CodiceIva = campodbN(myPOSTreader.Item("CodiceIva"))
            Ripartizione = campodb(myPOSTreader.Item("Ripartizione"))
            Anticipato = campodb(myPOSTreader.Item("Anticipato"))

            CENTROSERVIZIO = campodb(myPOSTreader.Item("CENTROSERVIZIO"))
            Struttura = campodb(myPOSTreader.Item("Struttura"))

            DataScadenza = campodbd(myPOSTreader.Item("Scadenza"))
            FuoriRetta = Val(campodb(myPOSTreader.Item("FuoriRetta")))

        End If
        myPOSTreader.Close()
        cn.Close()
    End Sub


    Sub LeggiDaDescrizioneEDescrizioneInternaRipartizione(ByVal StringaConnessione As String)
        Dim cn As OleDbConnection


        cn = New Data.OleDb.OleDbConnection(StringaConnessione)

        cn.Open()
        Dim cmd As New OleDbCommand()
        cmd.CommandText = ("select * from TABELLAEXTRAFISSI where Descrizione + ' ' + ISNULL(DESCRIZIONEINTERNA,'') Like '" & Descrizione & "%' And Ripartizione = ? ")
        cmd.Parameters.AddWithValue("@Ripartizione", Ripartizione)
        cmd.Connection = cn
        Dim myPOSTreader As OleDbDataReader = cmd.ExecuteReader()
        If myPOSTreader.Read Then
            CENTROSERVIZIO = campodb(myPOSTreader.Item("CENTROSERVIZIO"))
            CODICEEXTRA = campodb(myPOSTreader.Item("CODICEEXTRA"))
            Descrizione = campodb(myPOSTreader.Item("DESCRIZIONE"))
            DescrizioneInterna = campodb(myPOSTreader.Item("DESCRIZIONEINTERNA"))
            IMPORTO = campodbN(myPOSTreader.Item("IMPORTO"))
            Mastro = campodbN(myPOSTreader.Item("Mastro"))
            Conto = campodbN(myPOSTreader.Item("CONTO"))
            Sottoconto = campodbN(myPOSTreader.Item("SOTTOCONTO"))

            TipoImporto = campodb(myPOSTreader.Item("TipoImporto"))
            CodiceIva = campodb(myPOSTreader.Item("CodiceIva"))
            Ripartizione = campodb(myPOSTreader.Item("Ripartizione"))
            Anticipato = campodb(myPOSTreader.Item("Anticipato"))

            CENTROSERVIZIO = campodb(myPOSTreader.Item("CENTROSERVIZIO"))

            Struttura = campodb(myPOSTreader.Item("Struttura"))
            DataScadenza = campodbd(myPOSTreader.Item("Scadenza"))
            FuoriRetta = Val(campodb(myPOSTreader.Item("FuoriRetta")))

        End If
        cn.Close()
    End Sub

    Sub LeggiDaDescrizioneRipartizione(ByVal StringaConnessione As String)
        Dim cn As OleDbConnection


        cn = New Data.OleDb.OleDbConnection(StringaConnessione)

        cn.Open()
        Dim cmd As New OleDbCommand()
        cmd.CommandText = ("select * from TABELLAEXTRAFISSI where Descrizione Like '" & Descrizione & "%' And Ripartizione = ? ")
        cmd.Parameters.AddWithValue("@Ripartizione", Ripartizione)
        cmd.Connection = cn
        Dim myPOSTreader As OleDbDataReader = cmd.ExecuteReader()
        If myPOSTreader.Read Then
            CENTROSERVIZIO = campodb(myPOSTreader.Item("CENTROSERVIZIO"))
            CODICEEXTRA = campodb(myPOSTreader.Item("CODICEEXTRA"))
            Descrizione = campodb(myPOSTreader.Item("DESCRIZIONE"))
            DescrizioneInterna = campodb(myPOSTreader.Item("DESCRIZIONEINTERNA"))
            IMPORTO = campodbN(myPOSTreader.Item("IMPORTO"))
            Mastro = campodbN(myPOSTreader.Item("Mastro"))
            Conto = campodbN(myPOSTreader.Item("CONTO"))
            Sottoconto = campodbN(myPOSTreader.Item("SOTTOCONTO"))

            TipoImporto = campodb(myPOSTreader.Item("TipoImporto"))
            CodiceIva = campodb(myPOSTreader.Item("CodiceIva"))
            Ripartizione = campodb(myPOSTreader.Item("Ripartizione"))
            Anticipato = campodb(myPOSTreader.Item("Anticipato"))

            CENTROSERVIZIO = campodb(myPOSTreader.Item("CENTROSERVIZIO"))

            Struttura = campodb(myPOSTreader.Item("Struttura"))
            DataScadenza = campodbd(myPOSTreader.Item("Scadenza"))
            FuoriRetta = Val(campodb(myPOSTreader.Item("FuoriRetta")))

        End If
        cn.Close()
    End Sub
    Function campodbd(ByVal oggetto As Object) As Date
        If IsDBNull(oggetto) Then
            Return Nothing
        Else
            Return oggetto
        End If
    End Function


    Function campodb(ByVal oggetto As Object) As String
        If IsDBNull(oggetto) Then
            Return ""
        Else
            Return oggetto
        End If
    End Function


    Function campodbN(ByVal oggetto As Object) As Double
        If IsDBNull(oggetto) Then
            Return 0
        Else
            Return oggetto
        End If
    End Function


    Public Sub Scrivi(ByVal StringaConnessione As String)
        Dim cn As OleDbConnection
        Dim MySql As String

        MySql = ""


        cn = New Data.OleDb.OleDbConnection(StringaConnessione)

        cn.Open()
        Dim cmd As New OleDbCommand()
        cmd.CommandText = ("select * from TABELLAEXTRAFISSI where CODICEEXTRA = '" & CODICEEXTRA & "'")
        cmd.Connection = cn
        Dim myPOSTreader As OleDbDataReader = cmd.ExecuteReader()
        If myPOSTreader.Read Then
            'DescrizioneInterna =  campodb(myPOSTreader.Item("DESCRIZIONEINTERNA"))
            MySql = "UPDATE TABELLAEXTRAFISSI SET Descrizione = ?,DescrizioneInterna =?,Ripartizione = ?,IMPORTO = ?,Mastro = ?,Conto = ?,Sottoconto = ?,TipoImporto = ?,CodiceIva = ?,Anticipato= ?,CENTROSERVIZIO=?,Scadenza = ?,Struttura = ?,FuoriRetta = ? WHERE  CODICEEXTRA  = ?"

            Dim cmdw As New OleDbCommand()
            cmdw.CommandText = (MySql)
            cmdw.Parameters.AddWithValue("@Descrizione", Descrizione)
            cmdw.Parameters.AddWithValue("@DescrizioneInterna", DescrizioneInterna)
            cmdw.Parameters.AddWithValue("@Ripartizione", Ripartizione)
            cmdw.Parameters.AddWithValue("@IMPORTO", IMPORTO)
            cmdw.Parameters.AddWithValue("@Mastro", Mastro)
            cmdw.Parameters.AddWithValue("@Conto", Conto)
            cmdw.Parameters.AddWithValue("@Sottoconto", Sottoconto)
            cmdw.Parameters.AddWithValue("@TipoImporto", TipoImporto)
            cmdw.Parameters.AddWithValue("@CodiceIva", CodiceIva)
            cmdw.Parameters.AddWithValue("@Anticipato", Anticipato)
            cmdw.Parameters.AddWithValue("@CENTROSERVIZIO", CENTROSERVIZIO)
            If Year(DataScadenza) > 1 Then
                cmdw.Parameters.AddWithValue("@DataScadenza", DataScadenza)
            Else
                cmdw.Parameters.AddWithValue("@DataScadenza", System.DBNull.Value)
            End If


            cmdw.Parameters.AddWithValue("@Struttura", Struttura)
            cmdw.Parameters.AddWithValue("@FuoriRetta", FuoriRetta)

            'Struttura
            cmdw.Parameters.AddWithValue("@CODICEEXTRA", CODICEEXTRA)


            cmdw.Connection = cn
            cmdw.ExecuteNonQuery()
        Else
            MySql = "INSERT INTO TABELLAEXTRAFISSI (Descrizione,DescrizioneInterna ,IMPORTO,Mastro,Conto,Sottoconto,TipoImporto,CodiceIva,Ripartizione,Anticipato,CENTROSERVIZIO,Scadenza,Struttura,FuoriRetta,CODICEEXTRA) VALUES " & _
                                                  "(?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)"
            Dim cmdw As New OleDbCommand()
            cmdw.CommandText = (MySql)
            cmdw.Parameters.AddWithValue("@Descrizione", Descrizione)
            cmdw.Parameters.AddWithValue("@DescrizioneInterna", DescrizioneInterna)
            cmdw.Parameters.AddWithValue("@IMPORTO", IMPORTO)
            cmdw.Parameters.AddWithValue("@Mastro", Mastro)
            cmdw.Parameters.AddWithValue("@Conto", Conto)
            cmdw.Parameters.AddWithValue("@Sottoconto", Sottoconto)
            cmdw.Parameters.AddWithValue("@TipoImporto", TipoImporto)
            cmdw.Parameters.AddWithValue("@CodiceIva", CodiceIva)
            cmdw.Parameters.AddWithValue("@Ripartizione", Ripartizione)
            cmdw.Parameters.AddWithValue("@Anticipato", Anticipato)
            cmdw.Parameters.AddWithValue("@CENTROSERVIZIO", CENTROSERVIZIO)

            If Year(DataScadenza) > 1 Then
                cmdw.Parameters.AddWithValue("@DataScadenza", DataScadenza)
            Else
                cmdw.Parameters.AddWithValue("@DataScadenza", System.DBNull.Value)
            End If


            cmdw.Parameters.AddWithValue("@Struttura", Struttura)

            cmdw.Parameters.AddWithValue("@FuoriRetta", FuoriRetta)
            cmdw.Parameters.AddWithValue("@CODICEEXTRA", CODICEEXTRA)


            cmdw.Connection = cn
            cmdw.ExecuteNonQuery()
        End If
        myPOSTreader.Close()
        cn.Close()
    End Sub
End Class

