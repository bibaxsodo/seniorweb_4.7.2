﻿Imports Microsoft.VisualBasic
Imports System.Data.OleDb
Imports System.Web.Hosting


Public Class Cls_Cespiti
    Public ID As Integer
    Public Descrizione As String
    Public Ubicazione As String
    Public Categoria As Integer
    Public NumeroAcq As Integer
    Public AnnoAcq As Integer
    Public NumeroVnd As Integer
    Public AnnoVnd As Integer
    Public TipoPercentuale As String
    Public Dismesso As Integer
    Public ImportoAcq As Double
    Public ImportoVnd As Double
    Public DescrizioneAcq As String
    Public DescrizioneVnd As String
    Public DataUltimaStampaCB As Date
    Public Codice As Integer
    Public ImportoFondo As Double
    Public DataDismissione As Date
    Public DescrizioneDismissione As String
    Public Utente As String
    Public DataAggiornamento As Date

    Function campodbd(ByVal oggetto As Object) As Date
        If IsDBNull(oggetto) Then
            Return Nothing
        Else
            Return oggetto
        End If
    End Function


    Function campodb(ByVal oggetto As Object) As String
        If IsDBNull(oggetto) Then
            Return ""
        Else
            Return oggetto
        End If
    End Function

    Function campodbn(ByVal oggetto As Object) As Double
        If IsDBNull(oggetto) Then
            Return 0
        Else
            Return oggetto
        End If
    End Function

    Sub Leggi(ByVal StringaConnessione As String, ByVal Codice As String)
        Dim cn As OleDbConnection

        cn = New Data.OleDb.OleDbConnection(StringaConnessione)

        cn.Open()
        Dim cmd As New OleDbCommand()
        cmd.CommandText = ("select * from Cespiti Where  CODICE = ?")
        cmd.Connection = cn
        cmd.Parameters.AddWithValue("@CODICE", Codice)
        Dim myPOSTreader As OleDbDataReader = cmd.ExecuteReader()
        If myPOSTreader.Read Then
            Descrizione = campodb(myPOSTreader.Item("Descrizione"))
            Ubicazione = campodb(myPOSTreader.Item("Ubicazione"))
            Categoria = campodbn(myPOSTreader.Item("Categoria"))
            NumeroAcq = campodbn(myPOSTreader.Item("NumeroAcq"))
            AnnoAcq = campodbn(myPOSTreader.Item("AnnoAcq"))
            NumeroVnd = campodbn(myPOSTreader.Item("NumeroVnd"))
            AnnoVnd = campodbn(myPOSTreader.Item("AnnoVnd"))
            TipoPercentuale = campodb(myPOSTreader.Item("TipoPercentuale"))
            Dismesso = campodbn(myPOSTreader.Item("Dismesso"))
            ImportoAcq = campodbn(myPOSTreader.Item("ImportoAcq"))
            ImportoVnd = campodbn(myPOSTreader.Item("ImportoVnd"))
            DescrizioneAcq = campodb(myPOSTreader.Item("DescrizioneAcq"))
            DescrizioneVnd = campodb(myPOSTreader.Item("DescrizioneVnd"))
            DataUltimaStampaCB = campodbd(myPOSTreader.Item("DataUltimaStampaCB"))
            Codice = campodbn(myPOSTreader.Item("Codice"))
            ImportoFondo = campodbn(myPOSTreader.Item("ImportoFondo"))
            DataDismissione = campodbd(myPOSTreader.Item("DataDismissione"))
            DescrizioneDismissione = campodb(myPOSTreader.Item("DescrizioneDismissione"))
            DataAggiornamento = campodbd(myPOSTreader.Item("DataAggiornamento"))

            Utente = campodb(myPOSTreader.Item("Utente"))
        End If
        myPOSTreader.Close()
        cn.Close()
    End Sub


    Sub Scrivi(ByVal StringaConnessione As String)
        Dim cn As OleDbConnection
        Dim MySql As String

        cn = New Data.OleDb.OleDbConnection(StringaConnessione)

        cn.Open()
        Dim cmd As New OleDbCommand()
        cmd.CommandText = ("select * from Cespiti Where  CODICE = ?")
        cmd.Connection = cn
        cmd.Parameters.AddWithValue("@CODICE", Codice)
        Dim myPOSTreader As OleDbDataReader = cmd.ExecuteReader()
        If Not myPOSTreader.Read Then
            Dim cmdRead As New OleDbCommand()
            cmdRead.CommandText = ("select max(CODICE) as maxid from Cespiti")
            cmdRead.Connection = cn
            cmdRead.Parameters.AddWithValue("@Descrizione", Descrizione)
            Dim ReadOB As OleDbDataReader = cmdRead.ExecuteReader()
            If ReadOB.Read Then
                Codice = campodbn(ReadOB.Item("maxid")) + 1
            End If
            ReadOB.Close()

            Dim cmdIns As New OleDbCommand()
            cmdIns.CommandText = "Insert Into Cespiti (CODICE,Descrizione) Values (?,?)"
            cmdIns.Connection = cn
            cmdIns.Parameters.AddWithValue("@CODICE", Codice)
            cmdIns.Parameters.AddWithValue("@Descrizione", Descrizione)
            cmdIns.ExecuteNonQuery()


  
        End If
        myPOSTreader.Close()


        MySql = "UPDATE Cespiti  SET "
        MySql = MySql & " Descrizione = ?,"
        MySql = MySql & " Ubicazione = ?,"
        MySql = MySql & " Categoria = ?,"
        MySql = MySql & " NumeroAcq = ?,"
        MySql = MySql & " AnnoAcq = ?,"
        MySql = MySql & " NumeroVnd = ?,"
        MySql = MySql & " AnnoVnd = ?,"
        MySql = MySql & " TipoPercentuale = ?,"
        MySql = MySql & " Dismesso = ?,"
        MySql = MySql & " ImportoAcq = ?,"
        MySql = MySql & " ImportoVnd = ?,"
        MySql = MySql & " DescrizioneAcq = ?,"
        MySql = MySql & " DescrizioneVnd = ?,"
        MySql = MySql & " DataUltimaStampaCB = ?,"
        MySql = MySql & " Codice = ?,"
        MySql = MySql & " ImportoFondo = ?,"
        MySql = MySql & " DataDismissione = ?,"
        MySql = MySql & " DescrizioneDismissione = ?,"
        MySql = MySql & " DataAggiornamento = ?,Utente = ? Where Codice = ? "

        Dim cmdMod As New OleDbCommand()
        cmdMod.CommandText = MySql
        cmdMod.Connection = cn
        cmdMod.Parameters.AddWithValue("@Descrizione", Descrizione)

        cmdMod.Parameters.AddWithValue("@Ubicazione", Ubicazione)

        cmdMod.Parameters.AddWithValue("@Categoria", Categoria)
        cmdMod.Parameters.AddWithValue("@NumeroAcq", NumeroAcq)
        cmdMod.Parameters.AddWithValue("@AnnoAcq", AnnoAcq)
        cmdMod.Parameters.AddWithValue("@NumeroVnd", NumeroVnd)
        cmdMod.Parameters.AddWithValue("@AnnoVnd", AnnoVnd)
        cmdMod.Parameters.AddWithValue("@TipoPercentuale", TipoPercentuale)



        cmdMod.Parameters.AddWithValue("@Dismesso", Dismesso)
        cmdMod.Parameters.AddWithValue("@ImportoAcq", ImportoAcq)
        cmdMod.Parameters.AddWithValue("@ImportoVnd", ImportoVnd)
        cmdMod.Parameters.AddWithValue("@DescrizioneAcq", DescrizioneAcq)
        cmdMod.Parameters.AddWithValue("@DescrizioneVnd", DescrizioneVnd)
        cmdMod.Parameters.AddWithValue("@DataUltimaStampaCB", IIf(Year(DataUltimaStampaCB) > 1, DataUltimaStampaCB, System.DBNull.Value))
        cmdMod.Parameters.AddWithValue("@Codice", Codice)
        cmdMod.Parameters.AddWithValue("@ImportoFondo", ImportoFondo)
        cmdMod.Parameters.AddWithValue("@DataDismissione", IIf(Year(DataDismissione) > 1, DataDismissione, System.DBNull.Value))
        cmdMod.Parameters.AddWithValue("@DescrizioneDismissione", DescrizioneDismissione)
        cmdMod.Parameters.AddWithValue("@DataAggiornamento", Now)
        cmdMod.Parameters.AddWithValue("@Utente", Utente)



        cmdMod.Parameters.AddWithValue("@Codice", Codice)
        cmdMod.ExecuteNonQuery()
    End Sub

End Class
