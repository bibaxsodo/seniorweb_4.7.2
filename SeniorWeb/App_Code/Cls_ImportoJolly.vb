Imports Microsoft.VisualBasic
Imports System.Data.OleDb
Imports System.Web.Hosting

Public Class Cls_ImportoJolly
    Public CENTROSERVIZIO As String
    Public CODICEOSPITE As Long
    Public Data As Date
    Public Importo As Double
    Public Tipo As String
    Public Importo1 As Double
    Public Importo2 As Double
    Public Importo3 As Double
    Public Importo4 As Double
    Public PROV As String
    Public COMUNE As String


    Public Sub EliminaAll(ByVal StringaConnessione As String)
        Dim cn As OleDbConnection
        Dim MySql As String

        MySql = ""

        cn = New Data.OleDb.OleDbConnection(StringaConnessione)

        cn.Open()
        Dim cmd As New OleDbCommand()
        cmd.CommandText = ("Delete from IMPORTOJOLLY where CentroServizio = '" & CENTROSERVIZIO & "' And  CodiceOspite = " & CODICEOSPITE)
        cmd.Connection = cn
        cmd.ExecuteNonQuery()
        cn.Close()
    End Sub

    Public Sub Elimina(ByVal StringaConnessione As String)
        Dim cn As OleDbConnection
        Dim MySql As String

        MySql = ""

        cn = New Data.OleDb.OleDbConnection(StringaConnessione)

        cn.Open()
        Dim cmd As New OleDbCommand()
        cmd.CommandText = ("Delete from IMPORTOJOLLY where CentroServizio = '" & CENTROSERVIZIO & "' And  CodiceOspite = " & CODICEOSPITE & "  And Data = ?")
        cmd.Parameters.AddWithValue("@Data", Data)
        cmd.Connection = cn
        cmd.ExecuteNonQuery()
        cn.Close()
    End Sub

    Function campodb(ByVal oggetto As Object) As String
        If IsDBNull(oggetto) Then
            Return ""
        Else
            Return oggetto
        End If
    End Function
    Function campodbN(ByVal oggetto As Object) As Double
        If IsDBNull(oggetto) Then
            Return 0
        Else
            Return oggetto
        End If
    End Function
    Public Sub AggiornaDB(ByVal StringaConnessione As String)
        Dim cn As OleDbConnection
        Dim MySql As String

        MySql = ""


        cn = New Data.OleDb.OleDbConnection(StringaConnessione)

        cn.Open()
        Dim cmd As New OleDbCommand()
        cmd.CommandText = ("select * from IMPORTOJOLLY where CentroServizio = '" & CENTROSERVIZIO & "' And  CodiceOspite = " & CODICEOSPITE & "  And Data = ?")
        cmd.Parameters.AddWithValue("@Data", Data)
        cmd.Connection = cn
        Dim myPOSTreader As OleDbDataReader = cmd.ExecuteReader()
        If myPOSTreader.Read Then
            MySql = "UPDATE IMPORTOJOLLY SET IMPORTO = ?,TipoRetta=?,PROV=?,COMUNE=?,IMPORTO1=?,IMPORTO2=?,IMPORTO3=?,IMPORTO4=? " & _
                    " WHERE  CentroServizio = '" & CENTROSERVIZIO & "' And  CodiceOspite = " & CODICEOSPITE & " And Data = ?"
            Dim cmdw As New OleDbCommand()
            cmdw.CommandText = (MySql)
            cmdw.Parameters.AddWithValue("@IMPORTO", Importo)
            cmdw.Parameters.AddWithValue("@TIPOIMPORTO", Tipo)
            cmdw.Parameters.AddWithValue("@PROV", PROV)
            cmdw.Parameters.AddWithValue("@COMUNE", COMUNE)
            cmdw.Parameters.AddWithValue("@IMPORTO1", Importo1)
            cmdw.Parameters.AddWithValue("@IMPORTO2", Importo2)
            cmdw.Parameters.AddWithValue("@IMPORTO3", Importo3)
            cmdw.Parameters.AddWithValue("@IMPORTO4", Importo4)
            cmdw.Parameters.AddWithValue("@Data", Data)
            cmdw.Connection = cn
            cmdw.ExecuteNonQuery()
        Else
            MySql = "INSERT INTO IMPORTOJOLLY (CentroServizio,CodiceOspite,Data,IMPORTO,TipoRetta,PROV,COMUNE,IMPORTO1,IMPORTO2,IMPORTO3,IMPORTO4) VALUES (?,?,?,?,?,?,?,?,?,?,?)"
            Dim cmdw As New OleDbCommand()
            cmdw.CommandText = (MySql)
            cmdw.Parameters.AddWithValue("@CentroServizio", CentroServizio)
            cmdw.Parameters.AddWithValue("@CodiceOspite", CODICEOSPITE)
            cmdw.Parameters.AddWithValue("@Data", Data)
            cmdw.Parameters.AddWithValue("@IMPORTO", Importo)
            cmdw.Parameters.AddWithValue("@TIPOIMPORTO", Tipo)
            cmdw.Parameters.AddWithValue("@PROV", PROV)
            cmdw.Parameters.AddWithValue("@COMUNE", COMUNE)
            cmdw.Parameters.AddWithValue("@IMPORTO1", Importo1)
            cmdw.Parameters.AddWithValue("@IMPORTO2", Importo2)
            cmdw.Parameters.AddWithValue("@IMPORTO3", Importo3)
            cmdw.Parameters.AddWithValue("@IMPORTO4", Importo4)
            cmdw.Connection = cn
            cmdw.ExecuteNonQuery()
        End If
        myPOSTreader.Close()
        cn.Close()
    End Sub

    Public Sub AggiornaDaTabella(ByVal StringaConnessione As String, ByVal Tabella As System.Data.DataTable)
        Dim cn As OleDbConnection
        Dim MySql As String

        MySql = ""

        cn = New Data.OleDb.OleDbConnection(StringaConnessione)

        cn.Open()


        Dim Transan As OleDbTransaction = cn.BeginTransaction()

        Dim cmd As New OleDbCommand()
        cmd.CommandText = "Delete from IMPORTOJOLLY where CentroServizio = '" & CENTROSERVIZIO & "' And  CodiceOspite = " & CODICEOSPITE
        cmd.Connection = cn
        cmd.Transaction = Transan
        cmd.ExecuteNonQuery()

        Dim i As Long

        For i = 0 To Tabella.Rows.Count - 1
            If IsDate(Tabella.Rows(i).Item(0)) And Tabella.Rows(i).Item(0) <> "" Then
                MySql = "INSERT INTO IMPORTOJOLLY (CentroServizio,CodiceOspite,Data,IMPORTO,TipoRetta,PROV,COMUNE,IMPORTO1,IMPORTO2,IMPORTO3,IMPORTO4) VALUES (?,?,?,?,?,?,?,?,?,?,?)"
                Dim cmdw As New OleDbCommand()
                cmdw.CommandText = (MySql)
                cmdw.Parameters.AddWithValue("@CentroServizio", CENTROSERVIZIO)
                cmdw.Parameters.AddWithValue("@CodiceOspite", CODICEOSPITE)
                Dim xData As Date

                xData = Tabella.Rows(i).Item(0)
                cmdw.Parameters.AddWithValue("@Data", xData)
                cmdw.Parameters.AddWithValue("@IMPORTO", Convert.ToDouble(Tabella.Rows(i).Item(1)))
                cmdw.Parameters.AddWithValue("@TIPOIMPORTO", Tabella.Rows(i).Item(2))
                cmdw.Parameters.AddWithValue("@PROV", Tabella.Rows(i).Item(3))
                cmdw.Parameters.AddWithValue("@COMUNE", Tabella.Rows(i).Item(4))
                cmdw.Parameters.AddWithValue("@IMPORTO1", Convert.ToDouble(Tabella.Rows(i).Item(6)))
                cmdw.Parameters.AddWithValue("@IMPORTO2", Convert.ToDouble(Tabella.Rows(i).Item(7)))
                cmdw.Parameters.AddWithValue("@IMPORTO3", Convert.ToDouble(Tabella.Rows(i).Item(8)))
                cmdw.Parameters.AddWithValue("@IMPORTO4", Convert.ToDouble(Tabella.Rows(i).Item(9)))                
                cmdw.Transaction = Transan
                cmdw.Connection = cn
                cmdw.ExecuteNonQuery()
            End If
        Next
        Transan.Commit()
        cn.Close()

    End Sub

    Public Sub UltimaAData(ByVal StringaConnessione As String, ByVal CodiceOspite As Long, ByVal CentroServizio As String, ByVal data As Date)
        Dim cn As OleDbConnection



        cn = New Data.OleDb.OleDbConnection(StringaConnessione)

        cn.Open()
        Dim cmd As New OleDbCommand()
        cmd.CommandText = ("select * from IMPORTOJOLLY where CentroServizio = '" & CentroServizio & "' And  CodiceOspite = " & CodiceOspite & " And Data <= ? Order by Data Desc")
        cmd.Parameters.AddWithValue("@Data", data)
        cmd.Connection = cn
        Dim sb As StringBuilder = New StringBuilder

        Dim myPOSTreader As OleDbDataReader = cmd.ExecuteReader()
        If myPOSTreader.Read Then

            CodiceOspite = campodb(myPOSTreader.Item("CodiceOspite"))
            Importo = campodb(myPOSTreader.Item("Importo"))
            Tipo = campodb(myPOSTreader.Item("TIPORETTA"))
            Data = campodb(myPOSTreader.Item("Data"))

            PROV = campodb(myPOSTreader.Item("PROV"))
            COMUNE = campodb(myPOSTreader.Item("COMUNE"))
            Importo1 = campodb(myPOSTreader.Item("Importo1"))
            Importo2 = campodb(myPOSTreader.Item("Importo2"))
            Importo3 = campodb(myPOSTreader.Item("Importo3"))
            Importo4 = campodb(myPOSTreader.Item("Importo4"))
        End If
        myPOSTreader.Close()
        cn.Close()

    End Sub

    Public Sub UltimaData(ByVal StringaConnessione As String, ByVal CodiceOspite As Long, ByVal CentroServizio As String)
        Dim cn As OleDbConnection



        cn = New Data.OleDb.OleDbConnection(StringaConnessione)

        cn.Open()
        Dim cmd As New OleDbCommand()
        cmd.CommandText = ("select * from IMPORTOJOLLY where CentroServizio = '" & CentroServizio & "' And  CodiceOspite = " & CodiceOspite & " Order by Data Desc")
        cmd.Connection = cn
        Dim sb As StringBuilder = New StringBuilder

        Dim myPOSTreader As OleDbDataReader = cmd.ExecuteReader()
        If myPOSTreader.Read Then

            CodiceOspite = campodb(myPOSTreader.Item("CodiceOspite"))
            Importo = campodb(myPOSTreader.Item("Importo"))
            Tipo = campodb(myPOSTreader.Item("TIPORETTA"))
            Data = campodb(myPOSTreader.Item("Data"))

            PROV = campodb(myPOSTreader.Item("PROV"))
            COMUNE = campodb(myPOSTreader.Item("COMUNE"))
            Importo1 = campodb(myPOSTreader.Item("Importo1"))
            Importo2 = campodb(myPOSTreader.Item("Importo2"))
            Importo3 = campodb(myPOSTreader.Item("Importo3"))
            Importo4 = campodb(myPOSTreader.Item("Importo4"))
        End If
        myPOSTreader.Close()
        cn.Close()

    End Sub

    Sub loaddati(ByVal StringaConnessione As String, ByVal codiceospite As Integer, ByVal centroservizio As String, ByVal Tabella As System.Data.DataTable)
        Dim cn As OleDbConnection


        cn = New Data.OleDb.OleDbConnection(StringaConnessione)

        cn.Open()
        Dim cmd As New OleDbCommand()
        cmd.CommandText = ("select * from IMPORTOJOLLY where centroservizio = '" & centroservizio & "' And CodiceOspite = " & codiceospite & " Order By Data")
        cmd.Connection = cn
        Dim sb As StringBuilder = New StringBuilder

        Tabella.Clear()
        Tabella.Columns.Clear()
        Tabella.Columns.Add("Data", GetType(String))
        Tabella.Columns.Add("Importo", GetType(String))
        Tabella.Columns.Add("Tipo", GetType(String))
        Tabella.Columns.Add("PROV", GetType(String))
        Tabella.Columns.Add("COMUNE", GetType(String))
        Tabella.Columns.Add("Decodifica", GetType(String))
        Tabella.Columns.Add("Importo1", GetType(String))
        Tabella.Columns.Add("Importo2", GetType(String))
        Tabella.Columns.Add("Importo3", GetType(String))
        Tabella.Columns.Add("Importo4", GetType(String))

        Dim myPOSTreader As OleDbDataReader = cmd.ExecuteReader()
        Do While myPOSTreader.Read

            Dim myriga As System.Data.DataRow = Tabella.NewRow()
            myriga(0) = Format(myPOSTreader.Item("DATA"), "dd/MM/yyyy")
            myriga(1) = Format(campodbN(myPOSTreader.Item("Importo")), "#,##0.00")
            myriga(2) = campodb(myPOSTreader.Item("TIPORETTA"))
            myriga(3) = campodb(myPOSTreader.Item("PROV"))
            myriga(4) = campodb(myPOSTreader.Item("COMUNE"))

            Dim k As New ClsComune

            k.Provincia = campodb(myPOSTreader.Item("PROV"))
            k.Comune = campodb(myPOSTreader.Item("COMUNE"))

            k.DecodficaComune(StringaConnessione)

            myriga(5) = k.Descrizione
            myriga(6) = Format(campodbN(myPOSTreader.Item("IMPORTO1")), "#,##0.00")
            myriga(7) = Format(campodbN(myPOSTreader.Item("IMPORTO2")), "#,##0.00")
            myriga(8) = Format(campodbN(myPOSTreader.Item("IMPORTO3")), "#,##0.00")
            myriga(9) = Format(campodbN(myPOSTreader.Item("IMPORTO4")), "#,##0.00")
            Tabella.Rows.Add(myriga)
        Loop
        myPOSTreader.Close()

        If Tabella.Rows.Count = 0 Then
            Dim myriga As System.Data.DataRow = Tabella.NewRow()

            Dim Mov As New Cls_Movimenti

            Mov.CENTROSERVIZIO = centroservizio
            Mov.CodiceOspite = codiceospite
            Mov.UltimaDataAccoglimento(StringaConnessione)
            myriga(0) = Mov.Data

            myriga(1) = 0
            myriga(2) = "G"
            myriga(3) = ""
            myriga(4) = ""
            myriga(5) = ""
            myriga(6) = 0
            myriga(7) = 0
            myriga(8) = 0
            myriga(9) = 0
            Tabella.Rows.Add(myriga)
        End If
        cn.Close()
    End Sub
End Class
