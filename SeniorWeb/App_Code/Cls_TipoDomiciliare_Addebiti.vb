﻿Imports Microsoft.VisualBasic
Imports System.Data.OleDb
Imports System.Web.Hosting

Public Class Cls_TipoDomiciliare_Addebiti
    Public ID As Long
    Public CodiceTipoDomiciliare As String
    Public TipoAddebito As String
    Public TipoRetta As String
    Public TipoOperatore As String
    Public Ripartizione As String
    Public Tipologia As String
    Public Importo1 As Double
    Public Importo2 As Double
    Public Importo3 As Double
    Public NumeroOperatori As Integer



    Sub Scrivi(ByVal StringaConnessione As String)
        Dim cn As OleDbConnection



        cn = New Data.OleDb.OleDbConnection(StringaConnessione)

        cn.Open()
        Dim cmd As New OleDbCommand()
        cmd.CommandText = ("select * from TipoDomiciliare_Addebiti where " & _
                           "ID= ? ")
        cmd.Parameters.AddWithValue("@ID", ID)
        cmd.Connection = cn
        Dim myPOSTreader As OleDbDataReader = cmd.ExecuteReader()
        If Not myPOSTreader.Read Then
            Dim XSql As String
            XSql = "INSERT INTO TipoDomiciliare (CodiceTipoDomiciliare) values (?)"
            Dim cmdIns As New OleDbCommand()
            cmdIns.CommandText = XSql
            cmdIns.Parameters.AddWithValue("@CodiceTipoDomiciliare", CodiceTipoDomiciliare)            
            cmdIns.Connection = cn
            cmdIns.ExecuteNonQuery()
        End If
        myPOSTreader.Close()

        Dim MySql As String

        MySql = "UPDATE TipoDomiciliare_Addebiti SET " & _
                " CodiceTipoDomiciliare = ?, " & _
                " TipoAddebito = ?, " & _
                " TipoRetta = ?, " & _
                " TipoOperatore = ?, " & _
                " Ripartizione = ?, " & _
                " Tipologia = ?, " & _
                " Importo1 = ?, " & _
                " Importo2 = ?, " & _
                " Importo3 = ?, " & _
                " NumeroOperatori = ? " & _
                " Where ID = ? "
        Dim cmd1 As New OleDbCommand()
        cmd1.Connection = cn
        cmd1.CommandText = MySql
        cmd1.Parameters.AddWithValue("@CodiceTipoDomiciliare", CodiceTipoDomiciliare)
        cmd1.Parameters.AddWithValue("@TipoAddebito", TipoAddebito)
        cmd1.Parameters.AddWithValue("@TipoRetta", TipoRetta)
        cmd1.Parameters.AddWithValue("@TipoOperatore", TipoOperatore)
        cmd1.Parameters.AddWithValue("@Ripartizione", Ripartizione)
        cmd1.Parameters.AddWithValue("@Tipologia", Tipologia)
        cmd1.Parameters.AddWithValue("@Importo1", Importo1)
        cmd1.Parameters.AddWithValue("@Importo2", Importo2)
        cmd1.Parameters.AddWithValue("@Importo3", Importo3)
        cmd1.Parameters.AddWithValue("@NumeroOperatori", NumeroOperatori)

        cmd1.ExecuteNonQuery()

        cn.Close()
    End Sub


    Sub EliminaTutteDelCodice(ByVal StringaConnessione As String)
        Dim cn As OleDbConnection



        cn = New Data.OleDb.OleDbConnection(StringaConnessione)

        cn.Open()
        Dim cmd As New OleDbCommand()
        cmd.CommandText = ("Delete from TipoDomiciliare_Addebiti where " & _
                           "CodiceTipoDomiciliare = ? ")
        cmd.Parameters.AddWithValue("@CodiceTipoDomiciliare", CodiceTipoDomiciliare)
        cmd.Connection = cn
        cmd.ExecuteNonQuery()
        cn.Close()
    End Sub


    Sub Leggi(ByVal StringaConnessione As String, ByVal MyCodice As String)
        Dim cn As OleDbConnection



        cn = New Data.OleDb.OleDbConnection(StringaConnessione)

        cn.Open()
        Dim cmd As New OleDbCommand()
        cmd.CommandText = ("select * from TipoDomiciliare_Addebiti where " & _
                           "CodiceTipoDomiciliare = ? ")
        cmd.Parameters.AddWithValue("@CodiceTipoDomiciliare", CodiceTipoDomiciliare)
        cmd.Connection = cn
        Dim myPOSTreader As OleDbDataReader = cmd.ExecuteReader()
        If myPOSTreader.Read Then
            CodiceTipoDomiciliare = campodb(myPOSTreader.Item("CodiceTipoDomiciliare"))
            TipoAddebito = campodb(myPOSTreader.Item("TipoAddebito"))
            TipoRetta = campodb(myPOSTreader.Item("TipoAddebito"))
            TipoOperatore = campodb(myPOSTreader.Item("TipoOperatore"))

            Ripartizione = campodb(myPOSTreader.Item("Ripartizione"))
            Tipologia = campodb(myPOSTreader.Item("Tipologia"))
            Importo1 = campodb(myPOSTreader.Item("Importo1"))
            Importo2 = campodb(myPOSTreader.Item("Importo2"))
            Importo3 = campodb(myPOSTreader.Item("Importo3"))
            NumeroOperatori = campodb(myPOSTreader.Item("NumeroOperatori"))
        End If
        myPOSTreader.Close()
        cn.Close()
    End Sub
    Function campodb(ByVal oggetto As Object) As String
        If IsDBNull(oggetto) Then
            Return ""
        Else
            Return oggetto
        End If
    End Function

End Class
