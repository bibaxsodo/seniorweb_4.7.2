Imports Microsoft.VisualBasic
Imports System.Data.OleDb
Imports System.Web.Hosting

Public Class Cls_Diurno
    Public CENTROSERVIZIO As String
    Public CodiceOspite As Long
    Public Anno As Long
    Public Mese As Long
    Public Giorno1 As String
    Public Giorno2 As String
    Public Giorno3 As String
    Public Giorno4 As String
    Public Giorno5 As String
    Public Giorno6 As String
    Public Giorno7 As String
    Public Giorno8 As String
    Public Giorno9 As String
    Public Giorno10 As String
    Public Giorno11 As String
    Public Giorno12 As String
    Public Giorno13 As String
    Public Giorno14 As String
    Public Giorno15 As String
    Public Giorno16 As String
    Public Giorno17 As String
    Public Giorno18 As String
    Public Giorno19 As String
    Public Giorno20 As String
    Public Giorno21 As String
    Public Giorno22 As String
    Public Giorno23 As String
    Public Giorno24 As String
    Public Giorno25 As String
    Public Giorno26 As String
    Public Giorno27 As String
    Public Giorno28 As String
    Public Giorno29 As String
    Public Giorno30 As String
    Public Giorno31 As String
    Public Utente As String


    Public Sub AggiornaDB(ByVal StringaConnessione As String)
        Dim cn As OleDbConnection
        Dim MySql As String        

        MySql = ""


        cn = New Data.OleDb.OleDbConnection(StringaConnessione)

        cn.Open()
        Dim cmd As New OleDbCommand()
        cmd.CommandText = ("select * from AssenzeCentroDiurno where CentroServizio = '" & CENTROSERVIZIO & "' And  CodiceOspite = " & CodiceOspite & "  And Anno = " & Anno & " And Mese = " & Mese)
        cmd.Connection = cn
        Dim myPOSTreader As OleDbDataReader = cmd.ExecuteReader()
        If myPOSTreader.Read Then
            MySql = "UPDATE AssenzeCentroDiurno SET GIORNO1 = ?," & _
                    " GIORNO2 = ?," & _
                    " GIORNO3 = ?," & _
                    " GIORNO4 = ?," & _
                    " GIORNO5 = ?," & _
                    " GIORNO6 = ?," & _
                    " GIORNO7 = ?," & _
                    " GIORNO8 = ?," & _
                    " GIORNO9 = ?," & _
                    " GIORNO10 = ?," & _
                    " GIORNO11 = ?," & _
                    " GIORNO12 = ?," & _
                    " GIORNO13 = ?," & _
                    " GIORNO14 = ?," & _
                    " GIORNO15 = ?," & _
                    " GIORNO16 = ?," & _
                    " GIORNO17 = ?," & _
                    " GIORNO18 = ?," & _
                    " GIORNO19 = ?," & _
                    " GIORNO20 = ?," & _
                    " GIORNO21 = ?," & _
                    " GIORNO22 = ?," & _
                    " GIORNO23 = ?," & _
                    " GIORNO24 = ?," & _
                    " GIORNO25 = ?," & _
                    " GIORNO26 = ?," & _
                    " GIORNO27 = ?," & _
                    " GIORNO28 = ?," & _
                    " GIORNO29 = ?," & _
                    " GIORNO30 = ?," & _
                    " GIORNO31 = ?," & _
                    " UTENTE = ?," & _
                    " DATAAGGIORNAMENTO = ?" & _
                    " WHERE  CentroServizio = '" & CENTROSERVIZIO & "' And  CodiceOspite = " & CodiceOspite & "  And Anno = " & Anno & " And Mese = " & Mese
            Dim cmdw As New OleDbCommand()
            cmdw.CommandText = (MySql)

            cmdw.Parameters.AddWithValue("@GIORNO1", Giorno1)
            cmdw.Parameters.AddWithValue("@GIORNO2", Giorno2)
            cmdw.Parameters.AddWithValue("@GIORNO3", Giorno3)
            cmdw.Parameters.AddWithValue("@GIORNO4", Giorno4)
            cmdw.Parameters.AddWithValue("@GIORNO5", Giorno5)
            cmdw.Parameters.AddWithValue("@GIORNO6", Giorno6)
            cmdw.Parameters.AddWithValue("@GIORNO7", Giorno7)
            cmdw.Parameters.AddWithValue("@GIORNO8", Giorno8)
            cmdw.Parameters.AddWithValue("@GIORNO9", Giorno9)
            cmdw.Parameters.AddWithValue("@GIORNO10", Giorno10)
            cmdw.Parameters.AddWithValue("@GIORNO11", Giorno11)
            cmdw.Parameters.AddWithValue("@GIORNO12", Giorno12)
            cmdw.Parameters.AddWithValue("@GIORNO13", Giorno13)
            cmdw.Parameters.AddWithValue("@GIORNO14", Giorno14)
            cmdw.Parameters.AddWithValue("@GIORNO15", Giorno15)
            cmdw.Parameters.AddWithValue("@GIORNO16", Giorno16)
            cmdw.Parameters.AddWithValue("@GIORNO17", Giorno17)
            cmdw.Parameters.AddWithValue("@GIORNO18", Giorno18)
            cmdw.Parameters.AddWithValue("@GIORNO19", Giorno19)
            cmdw.Parameters.AddWithValue("@GIORNO20", Giorno20)
            cmdw.Parameters.AddWithValue("@GIORNO21", Giorno21)
            cmdw.Parameters.AddWithValue("@GIORNO22", Giorno22)
            cmdw.Parameters.AddWithValue("@GIORNO23", Giorno23)
            cmdw.Parameters.AddWithValue("@GIORNO24", Giorno24)
            cmdw.Parameters.AddWithValue("@GIORNO25", Giorno25)
            cmdw.Parameters.AddWithValue("@GIORNO26", Giorno26)
            cmdw.Parameters.AddWithValue("@GIORNO27", Giorno27)
            cmdw.Parameters.AddWithValue("@GIORNO28", Giorno28)
            cmdw.Parameters.AddWithValue("@GIORNO29", Giorno29)
            cmdw.Parameters.AddWithValue("@GIORNO30", Giorno30)
            cmdw.Parameters.AddWithValue("@GIORNO31", Giorno31)
            cmdw.Parameters.AddWithValue("@UTENTE", Utente)
            cmdw.Parameters.AddWithValue("@DATAAGGIORNAMENTO", Now)
            'cmdw.Parameters.AddWithValue("@CentroServizio", CENTROSERVIZIO)
            'cmdw.Parameters.AddWithValue("@CodiceOspite", CodiceOspite)
            'cmdw.Parameters.AddWithValue("@ANNO", Anno)
            'cmdw.Parameters.AddWithValue("@MESE", Mese)
            cmdw.Connection = cn
            cmdw.ExecuteNonQuery()
        Else
            MySql = "INSERT INTO AssenzeCentroDiurno (CentroServizio,CodiceOspite,Anno,Mese,GIORNO1,GIORNO2,GIORNO3,GIORNO4,GIORNO5,GIORNO6,GIORNO7,GIORNO8,GIORNO9,GIORNO10,GIORNO11,GIORNO12,GIORNO13,GIORNO14,GIORNO15,GIORNO16,GIORNO17,GIORNO18,GIORNO19,GIORNO20,GIORNO21,GIORNO22,GIORNO23,GIORNO24,GIORNO25,GIORNO26,GIORNO27,GIORNO28,GIORNO29,GIORNO30,GIORNO31,UTENTE,DATAAGGIORNAMENTO) VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)"
            Dim cmdw As New OleDbCommand()
            cmdw.CommandText = (MySql)
            cmdw.Parameters.AddWithValue("@CentroServizio", CENTROSERVIZIO)
            cmdw.Parameters.AddWithValue("@CodiceOspite", CodiceOspite)

            cmdw.Parameters.AddWithValue("@ANNO", Anno)
            cmdw.Parameters.AddWithValue("@MESE", Mese)

            cmdw.Parameters.AddWithValue("@GIORNO1", Giorno1)
            cmdw.Parameters.AddWithValue("@GIORNO2", Giorno2)
            cmdw.Parameters.AddWithValue("@GIORNO3", Giorno3)
            cmdw.Parameters.AddWithValue("@GIORNO4", Giorno4)
            cmdw.Parameters.AddWithValue("@GIORNO5", Giorno5)
            cmdw.Parameters.AddWithValue("@GIORNO6", Giorno6)
            cmdw.Parameters.AddWithValue("@GIORNO7", Giorno7)
            cmdw.Parameters.AddWithValue("@GIORNO8", Giorno8)
            cmdw.Parameters.AddWithValue("@GIORNO9", Giorno9)
            cmdw.Parameters.AddWithValue("@GIORNO10", Giorno10)
            cmdw.Parameters.AddWithValue("@GIORNO11", Giorno11)
            cmdw.Parameters.AddWithValue("@GIORNO12", Giorno12)
            cmdw.Parameters.AddWithValue("@GIORNO13", Giorno13)
            cmdw.Parameters.AddWithValue("@GIORNO14", Giorno14)
            cmdw.Parameters.AddWithValue("@GIORNO15", Giorno15)
            cmdw.Parameters.AddWithValue("@GIORNO16", Giorno16)
            cmdw.Parameters.AddWithValue("@GIORNO17", Giorno17)
            cmdw.Parameters.AddWithValue("@GIORNO18", Giorno18)
            cmdw.Parameters.AddWithValue("@GIORNO19", Giorno19)
            cmdw.Parameters.AddWithValue("@GIORNO20", Giorno20)
            cmdw.Parameters.AddWithValue("@GIORNO21", Giorno21)
            cmdw.Parameters.AddWithValue("@GIORNO22", Giorno22)
            cmdw.Parameters.AddWithValue("@GIORNO23", Giorno23)
            cmdw.Parameters.AddWithValue("@GIORNO24", Giorno24)
            cmdw.Parameters.AddWithValue("@GIORNO25", Giorno25)
            cmdw.Parameters.AddWithValue("@GIORNO26", Giorno26)
            cmdw.Parameters.AddWithValue("@GIORNO27", Giorno27)
            cmdw.Parameters.AddWithValue("@GIORNO28", Giorno28)
            cmdw.Parameters.AddWithValue("@GIORNO29", Giorno29)
            cmdw.Parameters.AddWithValue("@GIORNO30", Giorno30)
            cmdw.Parameters.AddWithValue("@GIORNO31", Giorno31)

            cmdw.Parameters.AddWithValue("@UTENTE", Utente)
            cmdw.Parameters.AddWithValue("@DATAAGGIORNAMENTO", Now)

            cmdw.Connection = cn
            cmdw.ExecuteNonQuery()
        End If
        myPOSTreader.Close()
        cn.Close()
    End Sub

    Public Sub Leggi(ByVal StringaConnessione As String, ByVal xcodiceospite As Integer, ByVal xcentroservizio As String, ByVal xAnno As Long, ByVal xMese As Long)
        Dim cn As OleDbConnection
        Dim I As Long


        cn = New Data.OleDb.OleDbConnection(StringaConnessione)

        cn.Open()
        Dim cmd As New OleDbCommand()
        cmd.CommandText = ("select * from AssenzeCentroDiurno where centroservizio = '" & xcentroservizio & "' And CodiceOspite = " & xcodiceospite & " And Mese = " & xMese & " And Anno = " & xAnno)
        cmd.Connection = cn

        Dim myPOSTreader As OleDbDataReader = cmd.ExecuteReader()
        If myPOSTreader.Read Then
            Anno = campodb(myPOSTreader.Item("Anno"))
            Mese = campodb(myPOSTreader.Item("Mese"))
            centroservizio = campodb(myPOSTreader.Item("centroservizio"))
            codiceospite = campodb(myPOSTreader.Item("codiceospite"))

            Giorno1 = campodb(myPOSTreader.Item("Giorno1"))
            Giorno2 = campodb(myPOSTreader.Item("Giorno2"))
            Giorno3 = campodb(myPOSTreader.Item("Giorno3"))
            Giorno4 = campodb(myPOSTreader.Item("Giorno4"))
            Giorno5 = campodb(myPOSTreader.Item("Giorno5"))
            Giorno6 = campodb(myPOSTreader.Item("Giorno6"))
            Giorno7 = campodb(myPOSTreader.Item("Giorno7"))
            Giorno8 = campodb(myPOSTreader.Item("Giorno8"))
            Giorno9 = campodb(myPOSTreader.Item("Giorno9"))
            Giorno10 = campodb(myPOSTreader.Item("Giorno10"))
            Giorno11 = campodb(myPOSTreader.Item("Giorno11"))
            Giorno12 = campodb(myPOSTreader.Item("Giorno12"))
            Giorno13 = campodb(myPOSTreader.Item("Giorno13"))
            Giorno14 = campodb(myPOSTreader.Item("Giorno14"))
            Giorno15 = campodb(myPOSTreader.Item("Giorno15"))
            Giorno16 = campodb(myPOSTreader.Item("Giorno16"))
            Giorno17 = campodb(myPOSTreader.Item("Giorno17"))
            Giorno18 = campodb(myPOSTreader.Item("Giorno18"))
            Giorno19 = campodb(myPOSTreader.Item("Giorno19"))
            Giorno20 = campodb(myPOSTreader.Item("Giorno20"))
            Giorno21 = campodb(myPOSTreader.Item("Giorno21"))
            Giorno22 = campodb(myPOSTreader.Item("Giorno22"))
            Giorno23 = campodb(myPOSTreader.Item("Giorno23"))
            Giorno24 = campodb(myPOSTreader.Item("Giorno24"))
            Giorno25 = campodb(myPOSTreader.Item("Giorno25"))
            Giorno26 = campodb(myPOSTreader.Item("Giorno26"))
            Giorno27 = campodb(myPOSTreader.Item("Giorno27"))
            Giorno28 = campodb(myPOSTreader.Item("Giorno28"))
            Giorno29 = campodb(myPOSTreader.Item("Giorno29"))
            Giorno30 = campodb(myPOSTreader.Item("Giorno30"))
            Giorno31 = campodb(myPOSTreader.Item("Giorno31"))
        End If
        myPOSTreader.Close()
        cn.Close()
    End Sub
    Function LeggiMese(ByVal StringaConnessione As String, ByVal codiceospite As Integer, ByVal centroservizio As String, ByVal Anno As Long, ByVal Mese As Long) As String
        Dim cn As OleDbConnection        
        Dim I As Long

        LeggiMese = ""
        cn = New Data.OleDb.OleDbConnection(StringaConnessione)

        cn.Open()
        Dim cmd As New OleDbCommand()
        cmd.CommandText = ("select * from AssenzeCentroDiurno where centroservizio = '" & centroservizio & "' And CodiceOspite = " & codiceospite & " And Mese = " & Mese & " And Anno = " & Anno)
        cmd.Connection = cn

        Dim myPOSTreader As OleDbDataReader = cmd.ExecuteReader()
        If myPOSTreader.Read Then
            For I = 1 To 31
                If IsDBNull(myPOSTreader.Item("GIORNO" & I)) Then
                    LeggiMese = LeggiMese & " "
                Else
                    If myPOSTreader.Item("GIORNO" & I) = "" Then
                        LeggiMese = LeggiMese & "  "
                    Else
                        If Len(myPOSTreader.Item("GIORNO" & I)) = 1 Then
                            LeggiMese = LeggiMese & myPOSTreader.Item("GIORNO" & I) & " "
                        Else
                            LeggiMese = LeggiMese & myPOSTreader.Item("GIORNO" & I)
                        End If
                    End If
                End If
            Next
        End If

        myPOSTreader.Close()


        cn.Close()
    End Function

    Function RipetizioniCausaleMese(ByVal StringaConnessione As String, ByVal codiceospite As Integer, ByVal centroservizio As String, ByVal Anno As Long, ByVal Mese As Long, ByVal Causale As String) As Integer
        Dim cn As OleDbConnection
        Dim I As Long


        RipetizioniCausaleMese = 0
        cn = New Data.OleDb.OleDbConnection(StringaConnessione)

        cn.Open()
        Dim cmd As New OleDbCommand()
        cmd.CommandText = ("select * from AssenzeCentroDiurno where centroservizio = '" & centroservizio & "' And CodiceOspite = " & codiceospite & " And Mese = " & Mese & " And Anno = " & Anno)
        cmd.Connection = cn

        Dim myPOSTreader As OleDbDataReader = cmd.ExecuteReader()
        If myPOSTreader.Read Then
            For I = 1 To GiorniMese(Mese, Anno)
                If campodb(myPOSTreader.Item("GIORNO" & I)) = Causale Then
                    RipetizioniCausaleMese = RipetizioniCausaleMese + 1
                End If
            Next
        End If

        myPOSTreader.Close()


        cn.Close()
    End Function
    Public Function GiorniMese(ByVal Mese As Byte, ByVal Anno As Integer) As Byte
        If Mese = 1 Or Mese = 3 Or Mese = 5 Or Mese = 7 Or Mese = 8 Or _
           Mese = 10 Or Mese = 12 Then
            GiorniMese = 31
        Else
            If Mese <> 2 Then
                GiorniMese = 30
            Else
                If Day(DateSerial(Anno, Mese, 29)) = 29 Then
                    GiorniMese = 29
                Else
                    GiorniMese = 28
                End If
            End If
        End If
    End Function


    Sub loaddati(ByVal StringaConnessione As String, ByVal codiceospite As Integer, ByVal centroservizio As String, ByVal Anno As Long, ByVal Tabella As System.Data.DataTable)
        Dim cn As OleDbConnection
        Dim StringaGiorni As String
        Dim I As Integer


        cn = New Data.OleDb.OleDbConnection(StringaConnessione)

        cn.Open()
        Dim cmd As New OleDbCommand()
        cmd.CommandText = ("select * from AssenzeCentroDiurno where centroservizio = '" & centroservizio & "' And CodiceOspite = " & codiceospite & " And Anno = " & Anno & " Order by Anno,Mese")
        cmd.Connection = cn
        Dim sb As StringBuilder = New StringBuilder

        Tabella.Clear()
        Tabella.Columns.Clear()
        Tabella.Columns.Add("Anno", GetType(String))
        Tabella.Columns.Add("Mese", GetType(String))

        For I = 1 To 31
            Tabella.Columns.Add(I, GetType(String))
        Next


        Dim myPOSTreader As OleDbDataReader = cmd.ExecuteReader()
        Do While myPOSTreader.Read

            Dim myriga As System.Data.DataRow = Tabella.NewRow()
            myriga(0) = myPOSTreader.Item("Anno")
            myriga(1) = myPOSTreader.Item("Mese")
            StringaGiorni = ""
            For I = 1 To 31
                myriga(I + 1) = myPOSTreader.Item("GIORNO" & I)
            Next I

            Tabella.Rows.Add(myriga)
        Loop
        myPOSTreader.Close()
        cn.Close()
    End Sub

    Function campodb(ByVal oggetto As Object) As String


        If IsDBNull(oggetto) Then
            Return ""
        Else
            Return oggetto
        End If
    End Function

End Class
