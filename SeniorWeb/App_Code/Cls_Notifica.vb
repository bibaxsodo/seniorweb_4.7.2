﻿Imports Microsoft.VisualBasic
Imports System.Data.OleDb
Imports System.Web.Hosting

Public Class Cls_Notifica
    Public API_KEY As String
    Public subscriptionId As String
    Public ErroriCalcolo As String
    Public ErroriEmissione As String
    Public ErroriStampa As String
    Public ErroriWarning As String


    Sub Delete(ByVal StringaConnessione As String)
        Dim cn As OleDbConnection



        cn = New Data.OleDb.OleDbConnection(StringaConnessione)

        cn.Open()

        Dim XSql As String
        XSql = "DELETE FROM Notifiche"
        Dim cmdIns As New OleDbCommand()
        cmdIns.CommandText = XSql
        cmdIns.Connection = cn
        cmdIns.ExecuteNonQuery()

        cn.Close()
    End Sub


    Sub Scrivi(ByVal StringaConnessione As String)
        Dim cn As OleDbConnection



        cn = New Data.OleDb.OleDbConnection(StringaConnessione)

        cn.Open()

        Dim XSql As String
        XSql = "INSERT INTO Notifiche (API_KEY,subscriptionId,ErroriCalcolo,ErroriEmissione,ErroriStampa,ErroriWarning,DataOra) values (?,?,?,?,?,?,?)"
        Dim cmdIns As New OleDbCommand()
        cmdIns.CommandText = XSql
        cmdIns.Parameters.AddWithValue("@API_KEY", API_KEY)
        cmdIns.Parameters.AddWithValue("@subscriptionId", subscriptionId)
        cmdIns.Parameters.AddWithValue("@ErroriCalcolo", ErroriCalcolo)
        cmdIns.Parameters.AddWithValue("@ErroriEmissione", ErroriEmissione)
        cmdIns.Parameters.AddWithValue("@ErroriStampa", ErroriStampa)
        cmdIns.Parameters.AddWithValue("@ErroriWarning", ErroriWarning)
        cmdIns.Parameters.AddWithValue("@DataOra", Now)
        cmdIns.Connection = cn
        cmdIns.ExecuteNonQuery()


        cn.Close()
    End Sub


End Class
