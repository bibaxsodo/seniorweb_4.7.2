﻿Imports Microsoft.VisualBasic
Imports System.Data.OleDb
Imports System.Web.Hosting

Public Class Cls_PrenotazioniTesta
    Public Anno As Long
    Public Descrizione As String
    Public Numero As Long
    Public Data As Date
    Public NumeroAtto As String
    Public DataAtto As Date
    Public TipoAtto As String
    Public Righe(4000) As Cls_PrenotazioniRighe

    Public Sub New()
        Anno = 0
        Descrizione = ""
        Numero = 0
        Data = Nothing
        NumeroAtto = ""
        DataAtto = Nothing
        TipoAtto = ""
    End Sub

    Function campodb(ByVal oggetto As Object) As String


        If IsDBNull(oggetto) Then
            Return ""
        Else
            Return oggetto
        End If
    End Function

    Sub Scrivi(ByVal StringaConnessione As String)
        Dim cn As OleDbConnection
        Dim MySql As String

        Dim Modifica As Boolean = False

        cn = New Data.OleDb.OleDbConnection(StringaConnessione)

        cn.Open()
        Dim cmd As New OleDbCommand()
        cmd.CommandText = ("select * from PrenotazioniTesta where " & _
                           "Anno = ? And Numero = ? ")
        cmd.Parameters.AddWithValue("@Anno", Anno)
        cmd.Parameters.AddWithValue("@Numero", Numero)
        cmd.Connection = cn
        Dim sb As StringBuilder = New StringBuilder

        Dim myPOSTreader As OleDbDataReader = cmd.ExecuteReader()
        If myPOSTreader.Read Then            
            Modifica = True
        End If
        myPOSTreader.Close()

        If Modifica = False Then
            Dim APPOGGIO As String = ""
            Dim I As Integer
            For I = 1 To 10
                APPOGGIO = APPOGGIO & Int(Rnd(1) * 10)
            Next
            Dim cmdIns As New OleDbCommand()
            MySql = "DECLARE @NUMERO integer" & _
                    " SET @NUMERO = (SELECT     max(Numero) + 1  FROM  PrenotazioniTesta Where Anno =" & Anno & ")" & _
                    " IF @NUMERO IS NULL SET @NUMERO = 1" & _
                    " INSERT INTO PrenotazioniTesta (Numero,Anno) VALUES (@NUMERO," & Anno & ")"
            cmdIns.CommandText = MySql
            cmdIns.Connection = cn
            cmdIns.ExecuteNonQuery()

            Dim cmdT As New OleDbCommand()
            cmdT.CommandText = ("select MAX(Numero) from PrenotazioniTesta WHERE Anno = '" & Anno & "'")
            cmdT.Connection = cn
            Dim myR As OleDbDataReader = cmdT.ExecuteReader()
            If myR.Read Then
                Numero = myR.Item(0)
            End If
            myR.Close()

        End If

        MySql = "UPDATE PrenotazioniTesta SET " & _
         " Descrizione  = ?, " & _
         " Data  = ?, " & _
         " NumeroAtto  = ?, " & _
         " DataAtto  = ?, " & _
         " TipoAtto  = ?  " & _
         " Where Anno = ? And Numero = ? "

        Dim cmd1 As New OleDbCommand()

        cmd1.CommandText = MySql
        cmd1.Parameters.AddWithValue("@Descrizione", Descrizione)
        cmd1.Parameters.AddWithValue("@Data", IIf(Year(Data) > 1, Data, System.DBNull.Value))
        cmd1.Parameters.AddWithValue("@NumeroAtto", NumeroAtto)
        cmd1.Parameters.AddWithValue("@DataAtto", IIf(Year(DataAtto) > 1, Data, System.DBNull.Value))
        cmd1.Parameters.AddWithValue("@TipoAtto", TipoAtto)
        cmd1.Parameters.AddWithValue("@Anno", Anno)
        cmd1.Parameters.AddWithValue("@Numero", Numero)
        cmd1.Connection = cn
        cmd1.ExecuteNonQuery()


        Dim cmdDelete As New OleDbCommand()
        cmdDelete.CommandText = ("Delete from PrenotazioniRiga where " & _
                           "Numero = ? ")
        cmdDelete.Parameters.AddWithValue("@Numero", Numero)
        cmdDelete.Connection = cn

        cmdDelete.ExecuteNonQuery()

        Dim x As Integer

        For x = 0 To Righe.Length - 1

            If Not IsNothing(Righe(x)) Then
                Righe(x).Anno = Anno
                Righe(x).Numero = Numero
                Righe(x).Scrivi(StringaConnessione)
            Else
                Exit For
            End If

        Next


        cn.Close()
    End Sub


    Sub Elimina(ByVal StringaConnessione As String)
        Dim cn As OleDbConnection


        cn = New Data.OleDb.OleDbConnection(StringaConnessione)

        cn.Open()
        Dim cmd As New OleDbCommand()
        cmd.CommandText = ("Delete  from PrenotazioniTesta where " & _
                           "Anno = ? And Numero = ?")
        cmd.Parameters.AddWithValue("@Anno", Anno)
        cmd.Parameters.AddWithValue("@Numero", Numero)
        cmd.Connection = cn
        cmd.ExecuteNonQuery()

        Dim cmdR As New OleDbCommand()
        cmdR.CommandText = ("Delete  from PrenotazioniRiga where " & _
                           "Anno = ? And Numero = ?")
        cmdR.Parameters.AddWithValue("@Anno", Anno)
        cmdR.Parameters.AddWithValue("@Numero", Numero)
        cmdR.Connection = cn
        cmdR.ExecuteNonQuery()

        cn.Close()
    End Sub

    Sub Leggi(ByVal StringaConnessione As String)
        Dim cn As OleDbConnection


        cn = New Data.OleDb.OleDbConnection(StringaConnessione)

        cn.Open()
        Dim cmd As New OleDbCommand()
        cmd.CommandText = ("select * from PrenotazioniTesta where " & _
                           "Anno = ? And Numero = ?")
        cmd.Parameters.AddWithValue("@Anno", Anno)
        cmd.Parameters.AddWithValue("@Numero", Numero)
        cmd.Connection = cn
        Dim VerReader As OleDbDataReader = cmd.ExecuteReader()
        If Not VerReader.Read Then
            Anno = 0
            Numero = 0
            VerReader.Close()
            cn.Close()
            Exit Sub
        End If
        VerReader.Close()

        Dim myPOSTreader As OleDbDataReader = cmd.ExecuteReader()
        If myPOSTreader.Read Then

            Anno = myPOSTreader.Item("Anno")
            Numero = myPOSTreader.Item("Numero")

            If IsDate(myPOSTreader.Item("Data")) Then
                Data = myPOSTreader.Item("Data")
            Else
                Data = Nothing
            End If
            NumeroAtto = campodb(myPOSTreader.Item("NumeroAtto"))
            If IsDate(myPOSTreader.Item("DataAtto")) Then
                DataAtto = myPOSTreader.Item("DataAtto")
            Else
                DataAtto = Nothing
            End If
            Descrizione = campodb(myPOSTreader.Item("Descrizione"))
            TipoAtto = campodb(myPOSTreader.Item("TipoAtto"))
        End If
        myPOSTreader.Close()

        Dim cmd1 As New OleDbCommand()
        cmd1.CommandText = ("select * from PrenotazioniRiga where " & _
                           "Anno = ? And Numero = ? ")
        cmd1.Parameters.AddWithValue("@Anno", Anno)
        cmd1.Parameters.AddWithValue("@Numero", Numero)
        cmd1.Connection = cn

        Dim Indice As Long = 0
        Dim myrd As OleDbDataReader = cmd1.ExecuteReader()
        Do While myrd.Read
            Righe(Indice) = New Cls_PrenotazioniRighe
            Righe(Indice).Numero = Val(campodb(myrd.Item("Numero")))
            Righe(Indice).Livello1 = Val(campodb(myrd.Item("Livello1")))
            Righe(Indice).Livello2 = Val(campodb(myrd.Item("Livello2")))
            Righe(Indice).Livello3 = Val(campodb(myrd.Item("Livello3")))
            Righe(Indice).Colonna = Val(campodb(myrd.Item("Colonna")))
            Righe(Indice).Descrizione = campodb(myrd.Item("DescrIzione"))
            Righe(Indice).Importo = CDbl(campodb(myrd.Item("Importo")))
            Righe(Indice).Riga = Val(campodb(myrd.Item("Riga")))
            Indice = Indice + 1
        Loop
        myrd.Close()


        cn.Close()
    End Sub


    Sub loaddati(ByVal StringaConnessione As String, ByVal AnnoMovimento As Long, ByVal Tabella As System.Data.DataTable)
        Dim cn As OleDbConnection


        cn = New Data.OleDb.OleDbConnection(StringaConnessione)

        cn.Open()
        Dim cmd As New OleDbCommand()
        cmd.CommandText = ("select * from PrenotazioniTesta where Anno = " & AnnoMovimento & " Order by Numero Desc")
        cmd.Connection = cn
        Dim sb As StringBuilder = New StringBuilder

        Tabella.Clear()
        Tabella.Columns.Clear()
        Tabella.Columns.Add("Anno", GetType(String))
        Tabella.Columns.Add("Descrizione", GetType(String))
        Tabella.Columns.Add("Numero", GetType(String))
        Tabella.Columns.Add("Data", GetType(String))
        Tabella.Columns.Add("NumeroAtto", GetType(String))
        Tabella.Columns.Add("DataAtto", GetType(String))        


        Dim myPOSTreader As OleDbDataReader = cmd.ExecuteReader()
        Do While myPOSTreader.Read

            Dim myriga As System.Data.DataRow = Tabella.NewRow()
            myriga(0) = campodb(myPOSTreader.Item("Anno"))
            myriga(1) = campodb(myPOSTreader.Item("Descrizione"))
            myriga(2) = campodb(myPOSTreader.Item("Numero"))
            If campodb(myPOSTreader.Item("Data")) <> "" Then
                myriga(3) = Format(myPOSTreader.Item("Data"), "dd/MM/yyyy")
            Else
                myriga(3) = ""
            End If
            myriga(4) = campodb(myPOSTreader.Item("NumeroAtto"))

            If campodb(myPOSTreader.Item("DataAtto")) <> "" Then
                myriga(5) = Format(myPOSTreader.Item("DataAtto"), "dd/MM/yyyy")
            Else
                myriga(5) = ""
            End If
            Tabella.Rows.Add(myriga)
        Loop
        myPOSTreader.Close()
        cn.Close()
    End Sub

End Class
