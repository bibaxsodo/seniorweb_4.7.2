﻿Imports Microsoft.VisualBasic
Imports System.Data.OleDb
Imports System.Web.Hosting

Public Class Cls_ParametriGenerale
    Public CausaleEntrata As String
    Public CausaleUscita As String
    Public CausaleGiroconto As String
    Public CausaliReversali As String
    Public CausaliMandati As String


    Function campodb(ByVal oggetto As Object) As String
        If IsDBNull(oggetto) Then
            Return ""
        Else
            Return oggetto
        End If
    End Function


    
    Function campodbD(ByVal oggetto As Object) As Date
        If IsDBNull(oggetto) Then
            Return Nothing
        Else
            Return oggetto
        End If
    End Function

    Public Sub LeggiParametri(ByVal StringaConnessione As String)
        Dim cn As OleDbConnection


        cn = New Data.OleDb.OleDbConnection(StringaConnessione)

        cn.Open()
        Dim cmd As New OleDbCommand()
        cmd.CommandText = ("select * from Parametri ")
        cmd.Connection = cn
        Dim sb As StringBuilder = New StringBuilder


        Dim myPOSTreader As OleDbDataReader = cmd.ExecuteReader()
        If myPOSTreader.Read Then
            CausaleEntrata = campodb(myPOSTreader.Item("CausaleEntrata"))
            CausaleUscita = campodb(myPOSTreader.Item("CausaleUscita"))
            CausaleGiroconto = campodb(myPOSTreader.Item("CausaleGiroconto"))

            CausaliReversali = campodb(myPOSTreader.Item("CausaliReversali"))
            CausaliMandati = campodb(myPOSTreader.Item("CausaliMandati"))
        End If
        myPOSTreader.Close()
        cn.Close()
    End Sub
    Public Sub Elaborazione(ByVal StringaConnessione As String)
        Dim cn As OleDbConnection
        Dim MySql As String

        cn = New Data.OleDb.OleDbConnection(StringaConnessione)

        cn.Open()
        MySql = "UPDATE Parametri SET DataOraElaborazione=?  "
        Dim cmdw As New OleDbCommand()
        cmdw.CommandText = (MySql)
        cmdw.Connection = cn
        cmdw.Parameters.AddWithValue("@DataOraElaborazione", Now)
        cmdw.ExecuteNonQuery()
        cn.Close()

    End Sub

    Public Sub FineElaborazione(ByVal StringaConnessione As String)
        Dim cn As OleDbConnection
        Dim MySql As String

        cn = New Data.OleDb.OleDbConnection(StringaConnessione)

        cn.Open()
        MySql = "UPDATE Parametri SET DataOraElaborazione= ?  "
        Dim cmdw As New OleDbCommand()
        cmdw.CommandText = (MySql)
        cmdw.Connection = cn
        cmdw.Parameters.AddWithValue("@DataOraElaborazione", System.DBNull.Value)
        cmdw.ExecuteNonQuery()
        cn.Close()

    End Sub

    Public Function InElaborazione(ByVal StringaConnessione As String) As Boolean
        Dim cn As OleDbConnection

        cn = New Data.OleDb.OleDbConnection(StringaConnessione)

        cn.Open()
        Dim cmd As New OleDbCommand()
        cmd.CommandText = ("select * from Parametri ")
        cmd.Connection = cn

        InElaborazione = False

        Dim myPOSTreader As OleDbDataReader = cmd.ExecuteReader()
        If myPOSTreader.Read Then
            If DateDiff(DateInterval.Second, campodbD(myPOSTreader.Item("DataOraElaborazione")), Now) < 30 Then
                InElaborazione = True
            Else
                InElaborazione = False
            End If
        End If
        cn.Close()
    End Function


    Public Sub ModificaParametri(ByVal StringaConnessione As String)
        Dim cn As OleDbConnection


        cn = New Data.OleDb.OleDbConnection(StringaConnessione)

        cn.Open()
        Dim cmd As New OleDbCommand()
        cmd.CommandText = ("UPDATE Parametri SET CausaleEntrata =  ?,CausaleUscita = ?,CausaleGiroconto = ?,CausaliReversali = ?,CausaliMandati  = ?")
        cmd.Connection = cn
        cmd.Parameters.AddWithValue("@CausaleEntrata", CausaleEntrata)
        cmd.Parameters.AddWithValue("@CausaleUscita", CausaleUscita)
        cmd.Parameters.AddWithValue("@CausaleGiroconto", CausaleGiroconto)
        cmd.Parameters.AddWithValue("@CausaliReversali", CausaliReversali)
        cmd.Parameters.AddWithValue("@CausaliMandati", CausaliMandati)
        cmd.ExecuteNonQuery()
        cn.Close()
    End Sub


End Class
