﻿Imports Microsoft.VisualBasic
Imports System.Data.OleDb
Imports System.Web.Hosting

Public Class Cls_TabellaTipoImportoRegione
    Public Codice As String
    Public Descrizione As String
    Public Mastro As String
    Public Conto As String
    Public Sottoconto As String

    Sub Scrivi(ByVal StringaConnessione As String)
        Dim cn As OleDbConnection



        cn = New Data.OleDb.OleDbConnection(StringaConnessione)

        cn.Open()
        Dim cmd As New OleDbCommand()
        cmd.CommandText = ("select * from TabellaTipoImportoRegione where " & _
                           "CODICE = ? ")
        cmd.Parameters.AddWithValue("@CODICE", Codice)
        cmd.Connection = cn
        Dim myPOSTreader As OleDbDataReader = cmd.ExecuteReader()
        If Not myPOSTreader.Read Then
            Dim XSql As String
            XSql = "INSERT INTO TabellaTipoImportoRegione (CODICE,DESCRIZIONE,Mastro,Conto,Sottoconto) values (?,?,?,?,?)"
            Dim cmdIns As New OleDbCommand()
            cmdIns.CommandText = XSql
            cmdIns.Parameters.AddWithValue("@Codice", Codice)
            cmdIns.Parameters.AddWithValue("@Descrizione", Descrizione)
            cmdIns.Parameters.AddWithValue("@Mastro", Mastro)
            cmdIns.Parameters.AddWithValue("@Conto", Conto)
            cmdIns.Parameters.AddWithValue("@Sottoconto", Sottoconto)
            cmdIns.Connection = cn
            cmdIns.ExecuteNonQuery()
        End If
        myPOSTreader.Close()

        Dim MySql As String

        MySql = "UPDATE TabellaTipoImportoRegione SET " & _
                " Descrizione = ?, " & _
                " Mastro = ?, " & _
                " Conto = ?, " & _
                " Sottoconto = ? " & _
                " Where Codice = ? "
        Dim cmd1 As New OleDbCommand()
        cmd1.Connection = cn
        cmd1.CommandText = MySql
        cmd1.Parameters.AddWithValue("@Descrizione", Descrizione)
        cmd1.Parameters.AddWithValue("@Mastro", Mastro)
        cmd1.Parameters.AddWithValue("@Conto", Conto)
        cmd1.Parameters.AddWithValue("@Sottoconto", Sottoconto)
        cmd1.Parameters.AddWithValue("@Codice", Codice)
        cmd1.ExecuteNonQuery()

        cn.Close()
    End Sub

    Sub Elimina(ByVal StringaConnessione As String)
        Dim cn As OleDbConnection



        cn = New Data.OleDb.OleDbConnection(StringaConnessione)

        cn.Open()
        Dim cmd As New OleDbCommand()
        cmd.CommandText = ("Delete from TabellaTipoImportoRegione where " & _
                           "CODICE = ? ")
        cmd.Parameters.AddWithValue("@CODICE", Codice)
        cmd.Connection = cn
        cmd.ExecuteNonQuery()
        cn.Close()
    End Sub


    Function MaxTipoImportoRegione(ByVal StringaConnessione As String) As String
        Dim cn As OleDbConnection


        MaxTipoImportoRegione = ""

        cn = New Data.OleDb.OleDbConnection(StringaConnessione)

        cn.Open()
        Dim cmd As New OleDbCommand()
        cmd.CommandText = ("select max(CASE WHEN (len(Codice) = 1) THEN '0' + CODICE ELSE Codice END) from TabellaTipoImportoRegione ")
        cmd.Connection = cn
        Dim MaxAddebito As OleDbDataReader = cmd.ExecuteReader()
        If MaxAddebito.Read Then
            Dim MassimoLetto As String

            MassimoLetto = campodb(MaxAddebito.Item(0))

            If MassimoLetto = "Z9" Then
                MassimoLetto = "a0"
                cn.Close()
                Return MassimoLetto
            End If

            If MassimoLetto = "99" Then
                MassimoLetto = "A0"
            Else
                If Mid(MassimoLetto & Space(10), 1, 1) > "0" And Mid(MassimoLetto & Space(10), 1, 1) > "9" Then
                    If Mid(MassimoLetto & Space(10), 2, 1) < "9" Then
                        MassimoLetto = Mid(MassimoLetto & Space(10), 1, 1) & Val(Mid(MassimoLetto & Space(10), 2, 1)) + 1
                    Else
                        Dim CodiceAscii As String
                        CodiceAscii = Asc(Mid(MassimoLetto & Space(10), 1, 1))

                        MassimoLetto = Chr(CodiceAscii + 1) & "0"
                    End If
                Else
                    MassimoLetto = Format(Val(MassimoLetto) + 1, "00")
                End If
            End If

            Return MassimoLetto
        Else
            Return "01"
        End If
        cn.Close()

    End Function

    Sub LeggiDescrizione(ByVal StringaConnessione As String, ByVal MyDescrizione As String)
        Dim cn As OleDbConnection



        cn = New Data.OleDb.OleDbConnection(StringaConnessione)

        cn.Open()
        Dim cmd As New OleDbCommand()
        cmd.CommandText = ("select * from TabellaTipoImportoRegione where " & _
                           "Descrizione Like ? ")
        cmd.Parameters.AddWithValue("@MyDescrizione", MyDescrizione)
        cmd.Connection = cn
        Dim sb As StringBuilder = New StringBuilder

        Dim myPOSTreader As OleDbDataReader = cmd.ExecuteReader()
        If myPOSTreader.Read Then
            Codice = campodb(myPOSTreader.Item("Codice"))
            Descrizione = campodb(myPOSTreader.Item("Descrizione"))
            Mastro = Val(campodb(myPOSTreader.Item("Mastro")))
            Conto = Val(campodb(myPOSTreader.Item("Conto")))
            Sottoconto = Val(campodb(myPOSTreader.Item("Sottoconto")))
        End If
        myPOSTreader.Close()
        cn.Close()
    End Sub

    Sub Leggi(ByVal StringaConnessione As String, ByVal MyCodice As String)
        Dim cn As OleDbConnection



        cn = New Data.OleDb.OleDbConnection(StringaConnessione)

        cn.Open()
        Dim cmd As New OleDbCommand()
        cmd.CommandText = ("select * from TabellaTipoImportoRegione where " & _
                           "CODICE = ? ")
        cmd.Parameters.AddWithValue("@CODICE", MyCodice)
        cmd.Connection = cn
        Dim sb As StringBuilder = New StringBuilder

        Dim myPOSTreader As OleDbDataReader = cmd.ExecuteReader()
        If myPOSTreader.Read Then
            Codice = campodb(myPOSTreader.Item("Codice"))
            Descrizione = campodb(myPOSTreader.Item("Descrizione"))
            Mastro = Val(campodb(myPOSTreader.Item("Mastro")))
            Conto = Val(campodb(myPOSTreader.Item("Conto")))
            Sottoconto = Val(campodb(myPOSTreader.Item("Sottoconto")))
        End If
        myPOSTreader.Close()
        cn.Close()
    End Sub
    Function campodb(ByVal oggetto As Object) As String
        If IsDBNull(oggetto) Then
            Return ""
        Else
            Return oggetto
        End If
    End Function


    Public Sub UpDateDropBox(ByVal StringaConnessione As String, ByRef appoggio As DropDownList)
        Dim cn As OleDbConnection



        cn = New Data.OleDb.OleDbConnection(StringaConnessione)

        cn.Open()
        Dim cmd As New OleDbCommand()

        cmd.CommandText = ("select * from TabellaTipoImportoRegione Order by Descrizione")
        cmd.Connection = cn
        Dim sb As StringBuilder = New StringBuilder

        appoggio.Items.Clear()
        appoggio.Items.Add("")
        appoggio.Items(appoggio.Items.Count - 1).Value = ""
        appoggio.Items(appoggio.Items.Count - 1).Selected = True
        Dim myPOSTreader As OleDbDataReader = cmd.ExecuteReader()
        Do While myPOSTreader.Read
            appoggio.Items.Add(myPOSTreader.Item("Descrizione"))
            appoggio.Items(appoggio.Items.Count - 1).Value = myPOSTreader.Item("CODICE")
        Loop
        myPOSTreader.Close()
        cn.Close()
        
    End Sub


    Public Sub UpDateDropBox(ByVal StringaConnessione As String, ByRef appoggio As DropDownList, ByVal CodiceRegione As String)
        Dim cn As OleDbConnection



        cn = New Data.OleDb.OleDbConnection(StringaConnessione)

        cn.Open()
        Dim cmd As New OleDbCommand()

        cmd.CommandText = ("select * from TabellaTipoImportoRegione Order by Descrizione")
        cmd.Connection = cn
        Dim sb As StringBuilder = New StringBuilder

        appoggio.Items.Clear()
        appoggio.Items.Add("")
        appoggio.Items(appoggio.Items.Count - 1).Value = ""
        appoggio.Items(appoggio.Items.Count - 1).Selected = True

        Dim myPOSTreader As OleDbDataReader = cmd.ExecuteReader()
        Do While myPOSTreader.Read
            appoggio.Items.Add(myPOSTreader.Item("Descrizione"))
            appoggio.Items(appoggio.Items.Count - 1).Value = myPOSTreader.Item("CODICE")
        Loop
        myPOSTreader.Close()
        cn.Close()
    End Sub



End Class
