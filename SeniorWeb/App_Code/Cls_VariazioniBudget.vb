﻿Imports Microsoft.VisualBasic
Imports System.Data.OleDb
Imports System.Web.Hosting


Public Class Cls_VariazioniBudget
    Public Anno As Long
    Public Livello1 As Long
    Public Livello2 As Long
    Public Livello3 As Long
    Public Colonna As Long    
    Public Data As Date
    Public Descrizione As String
    Public Importo As Double


    Function campodbN(ByVal oggetto As Object) As Double

        If IsDBNull(oggetto) Then
            Return 0
        Else
            Return oggetto
        End If
    End Function

    Function campodb(ByVal oggetto As Object) As String


        If IsDBNull(oggetto) Then
            Return ""
        Else
            Return oggetto
        End If
    End Function

    Sub Scrivi(ByVal StringaConnessione As String)
        Dim cn As OleDbConnection
        Dim MySql As String

        cn = New Data.OleDb.OleDbConnection(StringaConnessione)

        cn.Open()
        Dim cmd As New OleDbCommand()
        cmd.CommandText = ("select * from VariazioniBudget where " & _
                           "Anno = ? And Livello1 = ? And Livello2 = ? And Livello3 = ? And Colonna = ? And Data = ?")
        cmd.Parameters.AddWithValue("@Anno", Anno)
        cmd.Parameters.AddWithValue("@Livello1", Livello1)
        cmd.Parameters.AddWithValue("@Livello2", Livello2)
        cmd.Parameters.AddWithValue("@Livello3", Livello3)
        cmd.Parameters.AddWithValue("@Colonna", Colonna)
        cmd.Parameters.AddWithValue("@Data", Data)

        cmd.Connection = cn

        Dim myPOSTreader As OleDbDataReader = cmd.ExecuteReader()
        If myPOSTreader.Read Then
            MySql = "Update VariazioniBudget Set Descrizione = ?,Importo = ?  Where Anno = ? And Livello1 = ? And Livello2 = ? And Livello3 = ? And Colonna = ? And Data = ?"
        Else
            MySql = "INSERT INTO VariazioniBudget (Descrizione,Importo,Anno,Livello1,Livello2,Livello3,Colonna,Data) VALUES (?,?,?,?,?,?,?,?) "
        End If
        Dim cmdScrivi As New OleDbCommand()
        cmdScrivi.CommandText = MySql
        cmdScrivi.Connection = cn

        cmdScrivi.Parameters.AddWithValue("@Descrizione", Descrizione)
        cmdScrivi.Parameters.AddWithValue("@Importo", Importo)
        cmdScrivi.Parameters.AddWithValue("@Anno", Anno)
        cmdScrivi.Parameters.AddWithValue("@Livello1", Livello1)
        cmdScrivi.Parameters.AddWithValue("@Livello2", Livello2)
        cmdScrivi.Parameters.AddWithValue("@Livello3", Livello3)
        cmdScrivi.Parameters.AddWithValue("@Colonna", Colonna)
        cmdScrivi.Parameters.AddWithValue("@Data", Data)
        cmdScrivi.ExecuteNonQuery()

        myPOSTreader.Close()

        cn.Close()
    End Sub


    Sub Elimina(ByVal StringaConnessione As String)
        Dim cn As OleDbConnection


        cn = New Data.OleDb.OleDbConnection(StringaConnessione)

        cn.Open()
        Dim cmd As New OleDbCommand()
        cmd.CommandText = ("Delete from VariazioniBudget where " & _
                           "Anno = ? And Livello1 = ? And Livello2 = ? And Livello3 = ? And Colonna = ? And Data = ?")
        cmd.Parameters.AddWithValue("@Anno", Anno)
        cmd.Parameters.AddWithValue("@Livello1", Livello1)
        cmd.Parameters.AddWithValue("@Livello2", Livello2)
        cmd.Parameters.AddWithValue("@Livello3", Livello3)
        cmd.Parameters.AddWithValue("@Colonna", Colonna)
        cmd.Parameters.AddWithValue("@Data", Data)

        cmd.Connection = cn

        cmd.ExecuteNonQuery()


        cn.Close()
    End Sub


    Sub Leggi(ByVal StringaConnessione As String)
        Dim cn As OleDbConnection


        cn = New Data.OleDb.OleDbConnection(StringaConnessione)

        cn.Open()
        Dim cmd As New OleDbCommand()
        cmd.CommandText = ("select * from VariazioniBudget where " & _
                           "Anno = ? And Livello1 = ? And Livello2 = ? And Livello3 = ? And Colonna = ? And Data = ?")
        cmd.Parameters.AddWithValue("@Anno", Anno)
        cmd.Parameters.AddWithValue("@Livello1", Livello1)
        cmd.Parameters.AddWithValue("@Livello2", Livello2)
        cmd.Parameters.AddWithValue("@Livello3", Livello3)
        cmd.Parameters.AddWithValue("@Colonna", Colonna)
        cmd.Parameters.AddWithValue("@Data", Data)

        cmd.Connection = cn

        Dim myPOSTreader As OleDbDataReader = cmd.ExecuteReader()
        If myPOSTreader.Read Then

            Anno = myPOSTreader.Item("Anno")
            Livello1 = campodb(myPOSTreader.Item("Livello1"))
            Livello2 = campodb(myPOSTreader.Item("Livello2"))
            Livello3 = campodb(myPOSTreader.Item("Livello3"))
            Colonna = campodb(myPOSTreader.Item("Colonna"))

            If IsDate(myPOSTreader.Item("Data")) Then
                Data = myPOSTreader.Item("Data")
            Else
                Data = Nothing
            End If

            Descrizione = Trim(campodb(myPOSTreader.Item("Descrizione")))
            Importo = campodbN(myPOSTreader.Item("Importo"))
        End If
        myPOSTreader.Close()

        cn.Close()
    End Sub


    Sub loaddati(ByVal StringaConnessione As String, ByVal AnnoMovimento As Long, ByVal Tabella As System.Data.DataTable)
        Dim cn As OleDbConnection


        cn = New Data.OleDb.OleDbConnection(StringaConnessione)

        cn.Open()
        Dim cmd As New OleDbCommand()
        cmd.CommandText = ("select * from VariazioniBudget where Anno = " & AnnoMovimento & " Order by Livello1,Livello2,Livello3,Colonna")
        cmd.Connection = cn
        Dim sb As StringBuilder = New StringBuilder

        Tabella.Clear()
        Tabella.Columns.Clear()
        Tabella.Columns.Add("Anno", GetType(Long))
        Tabella.Columns.Add("Livello1", GetType(Long))
        Tabella.Columns.Add("Livello2", GetType(Long))
        Tabella.Columns.Add("Livello3", GetType(Long))
        Tabella.Columns.Add("Colonna", GetType(Long))
        Tabella.Columns.Add("Data", GetType(String))
        Tabella.Columns.Add("Descrizione", GetType(String))
        Tabella.Columns.Add("Importo", GetType(String))


        Dim myPOSTreader As OleDbDataReader = cmd.ExecuteReader()
        Do While myPOSTreader.Read

            Dim myriga As System.Data.DataRow = Tabella.NewRow()
            myriga(0) = campodb(myPOSTreader.Item("Anno"))
            myriga(1) = campodb(myPOSTreader.Item("Livello1"))
            myriga(2) = campodb(myPOSTreader.Item("Livello2"))
            myriga(3) = campodb(myPOSTreader.Item("Livello3"))
            myriga(4) = campodb(myPOSTreader.Item("Colonna"))            
            If campodb(myPOSTreader.Item("Data")) <> "" Then
                myriga(5) = Format(myPOSTreader.Item("Data"), "dd/MM/yyyy")
            Else
                myriga(5) = ""
            End If
            myriga(6) = Trim(campodb(myPOSTreader.Item("Descrizione")))
            myriga(7) = campodb(myPOSTreader.Item("Importo"))

            
            Tabella.Rows.Add(myriga)
        Loop
        myPOSTreader.Close()
        cn.Close()
    End Sub

End Class

