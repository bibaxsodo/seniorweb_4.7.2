﻿Imports Microsoft.VisualBasic
Imports System.Data.OleDb

Public Class Cls_FunzioniVB6
    Public StringaOspiti As String
    Public StringaTabelle As String
    Public StringaGenerale As String
    Dim OspitiDB As New ADODB.Connection
    Dim GeneraleDB As New ADODB.Connection
    Dim TabelleDb As New ADODB.Connection

    Public Function ImportoExtraOSpite(ByVal CodiceOspite As Long, ByVal DataIn As Date) As Double
        Dim MyRs As New ADODB.Recordset
        MyRs.Open("Select * From IMPORTOOSPITE Where CodiceOspite = " & CodiceOspite & " And DATA <= {ts '" & Format(DataIn, "yyyy-MM-dd") & " 00:00:00'} Order By Data Desc", OspitiDB, ADODB.CursorTypeEnum.adOpenKeyset, ADODB.LockTypeEnum.adLockOptimistic)
        If Not MyRs.EOF Then
            ImportoExtraOSpite = ImportoExtraOSpite + MoveFromDb(MyRs.Fields("Importo1")) + MoveFromDb(MyRs.Fields("Importo2")) + MoveFromDb(MyRs.Fields("Importo3")) + MoveFromDb(MyRs.Fields("Importo4"))
        End If
        MyRs.Close()
    End Function


    Public Function ImportoExtraParente(ByVal CodiceOspite As Long, ByVal CODICEPARENTE As Long, ByVal DataIn As Date) As Double
        Dim MyRs As New ADODB.Recordset
        MyRs.Open("Select * From IMPORTOPARENTI Where CodiceOspite = " & CodiceOspite & " And DATA <= {ts '" & Format(DataIn, "yyyy-MM-dd") & " 00:00:00'} And CodiceParente = " & CODICEPARENTE & " Order By Data Desc", OspitiDB, ADODB.CursorTypeEnum.adOpenKeyset, ADODB.LockTypeEnum.adLockOptimistic)
        If Not MyRs.EOF Then
            ImportoExtraParente = ImportoExtraParente + MoveFromDb(MyRs.Fields("Importo1")) + MoveFromDb(MyRs.Fields("Importo2")) + MoveFromDb(MyRs.Fields("Importo3")) + MoveFromDb(MyRs.Fields("Importo4"))
        End If
        MyRs.Close()
    End Function

    Public Function ImportoExtraComune(ByVal CodiceOspite As Long, ByVal DataIn As Date) As Double
        Dim MyRs As New ADODB.Recordset
        MyRs.Open("Select * From IMPORTOCOMUNE Where CodiceOspite = " & CodiceOspite & " And DATA <= {ts '" & Format(DataIn, "yyyy-MM-dd") & " 00:00:00'} Order By Data Desc", OspitiDB, ADODB.CursorTypeEnum.adOpenKeyset, ADODB.LockTypeEnum.adLockOptimistic)
        If Not MyRs.EOF Then
            ImportoExtraComune = ImportoExtraComune + MoveFromDb(MyRs.Fields("Importo1")) + MoveFromDb(MyRs.Fields("Importo2")) + MoveFromDb(MyRs.Fields("Importo3")) + MoveFromDb(MyRs.Fields("Importo4"))
        End If
        MyRs.Close()
    End Function
    Public Function DecodificaTabella(ByVal TipTab As String, ByVal CodTab As String) As String
        Dim MyRs As New ADODB.Recordset

        MyRs.Open("Select * From Tabelle Where TIPTAB = '" & TipTab & "' AND CODTAB = '" & CodTab & "'", OspitiDB, ADODB.CursorTypeEnum.adOpenKeyset, ADODB.LockTypeEnum.adLockOptimistic)
        If Not MyRs.EOF Then
            DecodificaTabella = MoveFromDbWC(MyRs, "Descrizione")
        End If
        MyRs.Close()

    End Function

    Public Function DecodificaComune(ByVal Provincia As String, ByVal Comune As String) As String
        Dim RS_Comuni As New ADODB.Recordset
        Dim MySql As String

        If Trim(Provincia) <> "" And Trim(Comune) <> "" Then
            MySql = "Select * From AnagraficaComune Where CodiceProvincia = '" & Provincia & "' AND CodiceComune = '" & Comune & "' And Tipologia = 'C'"
            RS_Comuni.Open(MySql, OspitiDB, ADODB.CursorTypeEnum.adOpenKeyset, ADODB.LockTypeEnum.adLockOptimistic)
            If RS_Comuni.EOF Then
                '       DecodificaComune = "Comune Non Decodificato"
                DecodificaComune = ""
            Else
                DecodificaComune = MoveFromDbWC(RS_Comuni, "Nome")
            End If
            RS_Comuni.Close()
        Else
            MySql = "Select * From AnagraficaComune Where CodiceProvincia = '" & Provincia & "' AND CodiceComune Is Null And Tipologia = 'C'"
            RS_Comuni.Open(MySql, OspitiDB, ADODB.CursorTypeEnum.adOpenKeyset, ADODB.LockTypeEnum.adLockOptimistic)
            If RS_Comuni.EOF Then
                '       DecodificaComune = "Comune Non Decodificato"
                DecodificaComune = ""
            Else
                DecodificaComune = MoveFromDbWC(RS_Comuni, "Nome")
            End If
            RS_Comuni.Close()
        End If
    End Function
    Public Function CampoMovimentiContabiliTesta(ByVal NumeroReg As Long, ByVal Campo As String) As String
        Dim MyRs As New ADODB.Recordset

        MyRs.Open("Select * From MovimentiContabiliTesta Where NumeroRegistrazione = " & NumeroReg, GeneraleDB, ADODB.CursorTypeEnum.adOpenKeyset, ADODB.LockTypeEnum.adLockOptimistic)

        If MyRs.EOF Then
            CampoMovimentiContabiliTesta = ""
        Else
            CampoMovimentiContabiliTesta = MoveFromDb(MyRs.Fields(Campo))
        End If
        MyRs.Close()
    End Function

    Public Function CampoTempMovimentiContabiliTesta(ByVal NumeroReg As Long, ByVal Campo As String) As String
        Dim MyRs As New ADODB.Recordset

        MyRs.Open("Select * From Temp_MovimentiContabiliTesta Where NumeroRegistrazione = " & NumeroReg, GeneraleDB, ADODB.CursorTypeEnum.adOpenKeyset, ADODB.LockTypeEnum.adLockOptimistic)

        If MyRs.EOF Then
            CampoTempMovimentiContabiliTesta = ""
        Else
            CampoTempMovimentiContabiliTesta = MoveFromDb(MyRs.Fields(Campo))
        End If
        MyRs.Close()
    End Function

    Public Function DecodificaCommentoConto(ByVal Mastro As String, ByVal Conto As String, ByVal SottoConto As String) As String

        Dim Rs_PianoConti As New ADODB.Recordset
        Dim Contr As String
        Dim MySql As String

        Contr = "Mastro = " & Mastro & " AND Conto = " & Conto & " AND Sottoconto = " & Val(SottoConto)
        MySql = "SELECT * from PianoConti where " & Contr
        If (Val(Mastro) = 0 And Val(Conto) = 0 And Val(SottoConto) = 0) Or (Mastro = "" Or Conto = "" Or SottoConto = "") Then
            DecodificaCommentoConto = ""
        Else
            Rs_PianoConti.Open(MySql, GeneraleDB, ADODB.CursorTypeEnum.adOpenKeyset, ADODB.LockTypeEnum.adLockOptimistic)
            If Rs_PianoConti.EOF Then
                DecodificaCommentoConto = "Conto Non Decodificato"
            Else
                DecodificaCommentoConto = MoveFromDbWC(Rs_PianoConti, "Commento")
            End If
            Rs_PianoConti.Close()
        End If

    End Function
    Public Function DecodificaConto(ByVal Mastro As String, ByVal Conto As String, ByVal SottoConto As String) As String

        Dim Rs_PianoConti As New ADODB.Recordset
        Dim Contr As String
        Dim MySql As String

        Contr = "Mastro = " & Mastro & " AND Conto = " & Conto & " AND Sottoconto = " & Val(SottoConto)
        MySql = "SELECT * from PianoConti where " & Contr
        If (Val(Mastro) = 0 And Val(Conto) = 0 And Val(SottoConto) = 0) Or (Mastro = "" Or Conto = "" Or SottoConto = "") Then
            DecodificaConto = ""
        Else
            Rs_PianoConti.Open(MySql, GeneraleDB, ADODB.CursorTypeEnum.adOpenKeyset, ADODB.LockTypeEnum.adLockOptimistic)
            If Rs_PianoConti.EOF Then
                DecodificaConto = "Conto Non Decodificato"
            Else
                DecodificaConto = MoveFromDbWC(Rs_PianoConti, "Descrizione")
            End If
            Rs_PianoConti.Close()
        End If

    End Function
    Public Function ReadNote(ByVal Condizione As String, ByVal Up As Boolean, ByVal ConessioneOspiti As String) As String
        'Dim MyRs As New ADODB.Recordset
        ReadNote = ""
        'MyRs.Open("Select * From NoteFatture Where " & Condizione, OspitiDB, ADODB.CursorTypeEnum.adOpenKeyset, ADODB.LockTypeEnum.adLockOptimistic)
        'If Not MyRs.EOF Then
        '    If Up Then
        '        ReadNote = MoveFromDbWC(MyRs, "NoteUp")
        '    Else
        '        ReadNote = MoveFromDbWC(MyRs, "NoteDown")
        '    End If
        'End If
        'MyRs.Close()

        Dim cn As OleDbConnection

        cn = New Data.OleDb.OleDbConnection(ConessioneOspiti)

        cn.Open()
        Dim cmd As New OleDbCommand()
        cmd.CommandText = "select * from NoteFatture Where " & Condizione
        cmd.Connection = cn
        Dim sb As StringBuilder = New StringBuilder

        Dim myPOSTreader As OleDbDataReader = cmd.ExecuteReader()
        If myPOSTreader.Read Then
            If Up Then
                ReadNote = campodb(myPOSTreader.Item("NoteUp"))
            Else
                ReadNote = campodb(myPOSTreader.Item("NoteDown"))
            End If
        End If
        myPOSTreader.Close()
        cn.Close()
    End Function



    Public Function LeggiNote(ByVal CodiceServizio As String, ByVal Prov As String, ByVal Comune As String, ByVal Regione As String, ByVal Up As Boolean, ByVal NumeroRegistrazione As Long, ByVal myCodiceOspite As Long, ByVal ConessioneOspiti As String) As String
        Dim Condizione As String
        Dim Stringa As String

        LeggiNote = ""


        If Prov <> "" And Comune <> "" Then
            If CodiceServizio <> "" Then
                Condizione = " CodiceServizio = '" & CodiceServizio & "' AND CodiceProvincia = '" & Prov & "' And CodiceComune = '" & Comune & "' And OspitiParentiComuniRegioni = 'C'"
                LeggiNote = ReadNote(Condizione, Up, ConessioneOspiti)
                Condizione = " CodiceServizio = '" & CodiceServizio & "' AND CodiceProvincia = '" & Prov & "' And (CodiceComune = '' Or CodiceComune Is Null) And OspitiParentiComuniRegioni = 'C'"
                Stringa = ReadNote(Condizione, Up, ConessioneOspiti)
                If Stringa <> "" Then LeggiNote = Stringa & vbNewLine & LeggiNote
                If LeggiNote = "" Then
                    Condizione = " (CodiceServizio = '' Or  CodiceServizio Is Null) AND CodiceProvincia = '" & Prov & "' And CodiceComune = '" & Comune & "'  And OspitiParentiComuniRegioni = 'C'"
                    LeggiNote = ReadNote(Condizione, Up, ConessioneOspiti)
                    Condizione = " (CodiceServizio = '' Or  CodiceServizio Is Null) AND CodiceProvincia = '" & Prov & "'And (CodiceComune = '' Or CodiceComune Is Null)  And OspitiParentiComuniRegioni = 'C'"
                    Stringa = ReadNote(Condizione, Up, ConessioneOspiti)
                    If Stringa <> "" Then LeggiNote = Stringa & vbNewLine & LeggiNote
                End If
                Condizione = " OspitiParentiComuniRegioni = 'C' AND CodiceServizio = '" & CodiceServizio & "' AND (CodiceProvincia = '' Or CodiceProvincia Is Null) And (CodiceComune = '' Or CodiceComune Is Null) AND (CodiceRegione = '' Or CodiceRegione Is Null)"
                Stringa = ReadNote(Condizione, Up, ConessioneOspiti)
                If Stringa <> "" Then LeggiNote = Stringa & vbNewLine & LeggiNote
                Stringa = ""
                If Stringa = "" Then
                    Condizione = " OspitiParentiComuniRegioni = 'C' AND (CodiceServizio = '' Or  CodiceServizio Is Null) AND (CodiceProvincia = '' Or CodiceProvincia Is Null) And (CodiceComune = '' Or CodiceComune Is Null) AND (CodiceRegione = '' Or CodiceRegione Is Null) And OspitiParentiComuniRegioni = 'C'"
                    Stringa = ReadNote(Condizione, Up, ConessioneOspiti)
                End If
                If Stringa <> "" Then LeggiNote = Stringa & vbNewLine & LeggiNote
                Condizione = " OspitiParentiComuniRegioni = 'E' AND CodiceServizio = '" & CodiceServizio & "' AND (CodiceProvincia = '' Or CodiceProvincia Is Null) And (CodiceComune = '' Or CodiceComune Is Null) AND (CodiceRegione = '' Or CodiceRegione Is Null) "
                Stringa = ReadNote(Condizione, Up, ConessioneOspiti)
                If Stringa = "" Then
                    Condizione = " OspitiParentiComuniRegioni = 'E' AND (CodiceServizio = '' Or  CodiceServizio Is Null) AND (CodiceProvincia = '' Or CodiceProvincia Is Null) And (CodiceComune = '' Or CodiceComune Is Null) AND (CodiceRegione = '' Or CodiceRegione Is Null) "
                    Stringa = ReadNote(Condizione, Up, ConessioneOspiti)
                End If
                If Stringa <> "" Then LeggiNote = Stringa & vbNewLine & LeggiNote
            Else
                Condizione = " (CodiceServizio = '' Or  CodiceServizio Is Null) AND CodiceProvincia = '" & Prov & "' And CodiceComune = '" & Comune & "'  And OspitiParentiComuniRegioni = 'C'"
                LeggiNote = ReadNote(Condizione, Up, ConessioneOspiti)
                Condizione = " (CodiceServizio = '' Or  CodiceServizio Is Null) AND CodiceProvincia = '" & Prov & "' And (CodiceComune = '' Or CodiceComune Is Null)  And OspitiParentiComuniRegioni = 'C'"
                Stringa = ReadNote(Condizione, Up, ConessioneOspiti)
                If Stringa <> "" Then LeggiNote = Stringa & vbNewLine & LeggiNote
                Condizione = " OspitiParentiComuniRegioni = 'C' AND (CodiceServizio = '' Or  CodiceServizio Is Null) AND (CodiceProvincia = '' Or CodiceProvincia Is Null) And (CodiceComune = '' Or CodiceComune Is Null) AND (CodiceRegione = '' Or CodiceRegione Is Null) And OspitiParentiComuniRegioni = 'C'"
                Stringa = ReadNote(Condizione, Up, ConessioneOspiti)
                If Stringa <> "" Then LeggiNote = Stringa & vbNewLine & LeggiNote
                Condizione = " OspitiParentiComuniRegioni = 'E' AND (CodiceServizio = '' Or  CodiceServizio Is Null) AND (CodiceProvincia = '' Or CodiceProvincia Is Null) And (CodiceComune = '' Or CodiceComune Is Null) AND (CodiceRegione = '' Or CodiceRegione Is Null) And OspitiParentiComuniRegioni = 'C'"
                Stringa = ReadNote(Condizione, Up, ConessioneOspiti)
                If Stringa <> "" Then LeggiNote = Stringa & vbNewLine & LeggiNote
            End If
            Condizione = " NumeroRegistrazione = " & NumeroRegistrazione
            Stringa = ReadNote(Condizione, Up, ConessioneOspiti)
            If Stringa <> "" Then LeggiNote = Stringa & vbNewLine & LeggiNote
            Exit Function
        End If

        If Regione <> "" Then
            If CodiceServizio <> "" Then
                Condizione = " (CodiceServizio Is Null or CodiceServizio = '') AND (CodiceRegione Is Null  Or CodiceRegione  = '') And (CodiceProvincia = '' Or CodiceProvincia Is Null) And (CodiceComune = '' Or CodiceComune Is Null) And OspitiParentiComuniRegioni = 'C'"
                LeggiNote = ReadNote(Condizione, Up, ConessioneOspiti)

                Condizione = " CodiceServizio = '" & CodiceServizio & "' AND CodiceRegione = '" & Regione & "'  And OspitiParentiComuniRegioni = 'C'"
                LeggiNote = LeggiNote & ReadNote(Condizione, Up, ConessioneOspiti)
                Condizione = " (CodiceServizio = '' Or  CodiceServizio Is Null) AND CodiceRegione = '" & Regione & "'  And OspitiParentiComuniRegioni = 'C'"
                LeggiNote = LeggiNote & ReadNote(Condizione, Up, ConessioneOspiti)

                Condizione = " OspitiParentiComuniRegioni = 'C' AND CodiceServizio = '" & CodiceServizio & "' AND (CodiceProvincia = '' Or CodiceProvincia Is Null) And (CodiceComune = '' Or CodiceComune Is Null) AND (CodiceRegione = '' Or CodiceRegione Is Null)  And OspitiParentiComuniRegioni = 'C'"
                Stringa = ReadNote(Condizione, Up, ConessioneOspiti)
                If Stringa <> "" Then LeggiNote = Stringa & vbNewLine & LeggiNote
                Condizione = " OspitiParentiComuniRegioni = 'E' AND CodiceServizio = '" & CodiceServizio & "' " ' AND (CodiceProvincia = '' Or CodiceProvincia Is Null) And (CodiceComune = '' Or CodiceComune Is Null) AND (CodiceRegione = '' Or CodiceRegione Is Null) "
                Stringa = ReadNote(Condizione, Up, ConessioneOspiti)
                If Stringa = "" Then
                    Condizione = " OspitiParentiComuniRegioni = 'E' AND (CodiceServizio = '' Or  CodiceServizio Is Null) " ' AND (CodiceProvincia = '' Or CodiceProvincia Is Null) And (CodiceComune = '' Or CodiceComune Is Null) AND (CodiceRegione = '' Or CodiceRegione Is Null) "
                    Stringa = ReadNote(Condizione, Up, ConessioneOspiti)
                End If
                If Stringa <> "" Then LeggiNote = Stringa & vbNewLine & LeggiNote
            Else
                Condizione = " (CodiceServizio = '' Or  CodiceServizio Is Null) AND CodiceRegione = '" & Regione & "' And OspitiParentiComuniRegioni = 'C'"
                LeggiNote = ReadNote(Condizione, Up, ConessioneOspiti)
                Condizione = " OspitiParentiComuniRegioni = 'C' AND (CodiceServizio = '' Or  CodiceServizio Is Null) AND (CodiceProvincia = '' Or CodiceProvincia Is Null) And (CodiceComune = '' Or CodiceComune Is Null) AND (CodiceRegione = '' Or CodiceRegione Is Null) And OspitiParentiComuniRegioni = 'C'"
                Stringa = ReadNote(Condizione, Up, ConessioneOspiti)
                If Stringa <> "" Then LeggiNote = Stringa & vbNewLine & LeggiNote
                Condizione = " OspitiParentiComuniRegioni = 'E' AND (CodiceServizio = '' Or  CodiceServizio Is Null) " ' AND (CodiceProvincia = '' Or CodiceProvincia Is Null) And (CodiceComune = '' Or CodiceComune Is Null) AND (CodiceRegione = '' Or CodiceRegione Is Null) And OspitiParentiComuniRegioni = 'C'"
                Stringa = ReadNote(Condizione, Up, ConessioneOspiti)
                If Stringa <> "" Then LeggiNote = Stringa & vbNewLine & LeggiNote
            End If

            Condizione = " NumeroRegistrazione = " & NumeroRegistrazione
            Stringa = ReadNote(Condizione, Up, ConessioneOspiti)
            If Stringa <> "" Then LeggiNote = Stringa & vbNewLine & LeggiNote
            Exit Function
        End If

        If Prov = "" And Comune = "" And Regione = "" Then
            If CodiceServizio <> "" Then
                Condizione = " (CODICIOSPITI  is Null Or CODICIOSPITI = '') And OspitiParentiComuniRegioni = 'O' AND CodiceServizio = '" & CodiceServizio & "' "
                'Condizione = " CodiceServizio = '" & CodiceServizio & "' AND (CodiceProvincia = '' Or CodiceProvincia Is Null) And (CodiceComune = '' Or CodiceComune = Null) AND (CodiceRegione = '' Or CodiceRegione = Null)"
                Stringa = ReadNote(Condizione, Up, ConessioneOspiti)
                If Stringa = "" Then
                    Condizione = " (CODICIOSPITI  is Null Or CODICIOSPITI = '') And OspitiParentiComuniRegioni = 'O' AND (CodiceServizio = '' Or  CodiceServizio Is Null)  "
                    Stringa = ReadNote(Condizione, Up, ConessioneOspiti)
                End If
                If Stringa <> "" Then LeggiNote = Stringa & vbNewLine & LeggiNote
                Condizione = " (CODICIOSPITI  is Null Or CODICIOSPITI = '') And OspitiParentiComuniRegioni = 'E' AND CodiceServizio = '" & CodiceServizio & "' "
                Stringa = ReadNote(Condizione, Up, ConessioneOspiti)
                If Stringa = "" Then
                    Condizione = " (CODICIOSPITI  is Null Or CODICIOSPITI = '') And OspitiParentiComuniRegioni = 'E' AND (CodiceServizio = '' Or  CodiceServizio Is Null) "
                    Stringa = ReadNote(Condizione, Up, ConessioneOspiti)
                End If
                If Stringa <> "" Then LeggiNote = Stringa & vbNewLine & LeggiNote
            Else
                Condizione = " (CODICIOSPITI  is Null Or CODICIOSPITI = '') And OspitiParentiComuniRegioni = 'O' AND (CodiceServizio = '' Or  CodiceServizio Is Null) "
                Stringa = ReadNote(Condizione, Up, ConessioneOspiti)
                If Stringa <> "" Then LeggiNote = Stringa & vbNewLine & LeggiNote
                Condizione = " (CODICIOSPITI  is Null Or CODICIOSPITI = '') And OspitiParentiComuniRegioni = 'E' AND (CodiceServizio = '' Or  CodiceServizio Is Null) "
                Stringa = ReadNote(Condizione, Up, ConessioneOspiti)
                If Stringa <> "" Then LeggiNote = Stringa & vbNewLine & LeggiNote
            End If

            Condizione = " (CODICIOSPITI Like '%," & Trim(Str(myCodiceOspite)) & ",%') OR CODICIOSPITI Like '" & Trim(Str(myCodiceOspite)) & ",%' "
            Stringa = ReadNote(Condizione, Up, ConessioneOspiti)
            If Stringa <> "" Then LeggiNote = Stringa & vbNewLine & LeggiNote

            Condizione = " NumeroRegistrazione = " & NumeroRegistrazione
            Stringa = ReadNote(Condizione, Up, ConessioneOspiti)
            If Stringa <> "" Then LeggiNote = Stringa & vbNewLine & LeggiNote
            Exit Function
        End If

    End Function


    Public Function StatoAutoOspite(ByVal CodiceOspite As Long, ByVal DataMov As Date) As String
        Dim MyRs As New ADODB.Recordset
        StatoAutoOspite = ""
        MyRs.Open("Select * From  StatoAuto Where CodiceOspite = " & CodiceOspite & " And Data <= {ts '" & Format(DataMov, "yyyy-MM-dd") & " 00:00:00'} Order by Data Desc", OspitiDB, ADODB.CursorTypeEnum.adOpenKeyset, ADODB.LockTypeEnum.adLockOptimistic)
        If Not MyRs.EOF Then
            StatoAutoOspite = MoveFromDbWC(MyRs, "STATOAUTO")
        End If
        MyRs.Close()
    End Function
    Public Sub ApriDB()
        OspitiDB.Open(StringaOspiti)
        TabelleDb.Open(StringaTabelle)
        GeneraleDB.Open(StringaGenerale)
    End Sub
    Public Sub ChiudiDB()
        OspitiDB.Close()
        TabelleDb.Close()
        GeneraleDB.Close()
    End Sub
    Public Function CampoIVA(ByVal Codice As String, ByVal Campo As String) As String
        'Dim MyRs As New ADODB.Recordset
        'MyRs.Open("Select * From IVA Where Codice = '" & Codice & "'", TabelleDb, ADODB.CursorTypeEnum.adOpenKeyset, ADODB.LockTypeEnum.adLockOptimistic)
        'If MyRs.EOF Then
        '    CampoIVA = ""
        'Else
        '    CampoIVA = MoveFromDb(MyRs.Fields(Campo))
        'End If
        'MyRs.Close()

        Dim cn As OleDbConnection

        cn = New Data.OleDb.OleDbConnection(StringaTabelle)

        cn.Open()
        Dim cmd As New OleDbCommand()
        cmd.CommandText = "Select * From IVA Where Codice = '" & Codice & "'"
        cmd.Connection = cn
        CampoIVA = ""

        Dim myPOSTreader As OleDbDataReader = cmd.ExecuteReader()
        If myPOSTreader.Read Then
            CampoIVA = campodb(myPOSTreader.Item(Campo))
        End If
        myPOSTreader.Close()
        cn.Close()
    End Function

    Public Function CampoCausaleContabile(ByVal Codice As String, ByVal Campo As String) As String
        'Dim MyRs As New ADODB.Recordset
        'MyRs.Open("Select * From CausaliContabiliTesta Where Codice = '" & Codice & "'", TabelleDb, ADODB.CursorTypeEnum.adOpenKeyset, ADODB.LockTypeEnum.adLockOptimistic)
        'If MyRs.EOF Then
        '    CampoCausaleContabile = ""
        'Else
        '    CampoCausaleContabile = MoveFromDb(MyRs.Fields(Campo))
        'End If
        'MyRs.Close()

        Dim cn As OleDbConnection

        cn = New Data.OleDb.OleDbConnection(StringaTabelle)

        cn.Open()
        Dim cmd As New OleDbCommand()
        cmd.CommandText = "Select * From CausaliContabiliTesta Where Codice = '" & Codice & "'"
        cmd.Connection = cn
        CampoCausaleContabile = ""

        Dim myPOSTreader As OleDbDataReader = cmd.ExecuteReader()
        If myPOSTreader.Read Then
            CampoCausaleContabile = campodb(myPOSTreader.Item(Campo))
        End If
        myPOSTreader.Close()
        cn.Close()

    End Function

    Function campodb(ByVal oggetto As Object) As String
        If IsDBNull(oggetto) Then
            Return ""
        Else
            Return oggetto
        End If
    End Function

    Public Function CampoAnagraficaComune(ByVal Mastro As Long, ByVal Conto As Long, ByVal SottoConto As Long, ByVal CliFor As String, ByVal Campo As String) As String
        Dim MyRs As New ADODB.Recordset
        Dim Condizione As String
        Dim PianoConti As New Cls_Pianodeiconti

        If CliFor = "C" Then
            PianoConti.Mastro = Mastro
            PianoConti.Conto = Conto
            PianoConti.Sottoconto = SottoConto
            PianoConti.Decodfica(StringaGenerale)
            If PianoConti.TipoAnagrafica = "O" Then
                Condizione = "Select " & Campo & " From AnagraficaComune Where CodiceOspite = " & Int(SottoConto / 100) & " And TIPOLOGIA ='O'"
                MyRs.Open(Condizione, OspitiDB, ADODB.CursorTypeEnum.adOpenKeyset, ADODB.LockTypeEnum.adLockOptimistic)
                If MyRs.EOF Then
                    CampoAnagraficaComune = ""
                Else
                    CampoAnagraficaComune = MoveFromDb(MyRs.Fields(Campo))
                End If
                MyRs.Close()
                Exit Function
            End If
            If PianoConti.TipoAnagrafica = "P" Then
                Condizione = "Select " & Campo & " From AnagraficaComune Where CodiceOspite = " & Int(SottoConto / 100) & " And  CodiceParente = " & SottoConto - (Int(SottoConto / 100) * 100)  '& " And TIPOLOGIA ='P'"
                MyRs.Open(Condizione, OspitiDB, ADODB.CursorTypeEnum.adOpenKeyset, ADODB.LockTypeEnum.adLockOptimistic)
                If MyRs.EOF Then
                    CampoAnagraficaComune = ""
                Else
                    CampoAnagraficaComune = MoveFromDb(MyRs.Fields(Campo))
                End If
                MyRs.Close()
                Exit Function
            End If
            Condizione = "Select " & Campo & " From AnagraficaComune Where MastroCliente = " & Mastro & " And ContoCliente = " & Conto & " And SottoContoCliente = " & SottoConto
            MyRs.Open(Condizione, OspitiDB, ADODB.CursorTypeEnum.adOpenKeyset, ADODB.LockTypeEnum.adLockOptimistic)
            If MyRs.EOF Then
                CampoAnagraficaComune = ""
            Else
                CampoAnagraficaComune = MoveFromDb(MyRs.Fields(Campo))
            End If
            MyRs.Close()

        Else
            Condizione = "Select " & Campo & " From AnagraficaComune Where MastroFornitore = " & Mastro & " And ContoFornitore = " & Conto & " And SottoContoFornitore = " & SottoConto
            MyRs.Open(Condizione, OspitiDB, ADODB.CursorTypeEnum.adOpenKeyset, ADODB.LockTypeEnum.adLockOptimistic)
            If MyRs.EOF Then
                CampoAnagraficaComune = ""
            Else
                CampoAnagraficaComune = MoveFromDb(MyRs.Fields(Campo))
            End If
            MyRs.Close()
        End If
    End Function
    Public Function CampoOspiteParenteMCS(ByVal NomeCampo As String, ByVal Mastro As Long, ByVal Conto As Long, ByVal SottoConto As Long) As Object
        Dim MyRs As New ADODB.Recordset
        Dim CodOsp As Long
        Dim CodPar As Long

        MyRs.Open("Select * From AnagraficaComune Where (TIPOLOGIA = 'O' OR TIPOLOGIA = 'P') And MastroCliente = " & Mastro & " And ContoCliente = " & Conto & " And SottoContoCliente = " & SottoConto, OspitiDB, ADODB.CursorTypeEnum.adOpenKeyset, ADODB.LockTypeEnum.adLockOptimistic)
        If Not MyRs.EOF Then
            CampoOspiteParenteMCS = MoveFromDb(MyRs.Fields(NomeCampo))
            MyRs.Close()
            Exit Function
        End If
        MyRs.Close()

        If Int(SottoConto / 100) = Modulo.MathRound(SottoConto / 100, 10) Then
            CodOsp = Int(SottoConto / 100)
            CampoOspiteParenteMCS = CampoOspite(CodOsp, NomeCampo)

        Else

            CodOsp = Int(SottoConto / 100)
            CodPar = SottoConto - (Int(SottoConto / 100) * 100)
            CampoOspiteParenteMCS = CampoParente(CodOsp, CodPar, NomeCampo)
        End If

    End Function
    Public Function CampoModalitaOspiti(ByVal Codice As String, ByVal Campo As String) As String
        Dim MyRs As New ADODB.Recordset
        Dim Condizione As String
        Condizione = "Select " & Campo & " From MODALITAPAGAMENTO Where Codice = '" & Codice & "'"
        MyRs.Open(Condizione, OspitiDB, ADODB.CursorTypeEnum.adOpenKeyset, ADODB.LockTypeEnum.adLockOptimistic)
        If MyRs.EOF Then
            CampoModalitaOspiti = ""
        Else
            CampoModalitaOspiti = MoveFromDb(MyRs.Fields(Campo))
        End If
        MyRs.Close()
    End Function
    Public Function CampoOspite(ByVal CodOsp As Long, ByVal Campo As String) As String
        Dim MyRs As New ADODB.Recordset
        Dim Condizione As String
        Condizione = "Select * From AnagraficaComune Where Tipologia = 'O' And CODICEOSPITE = " & CodOsp
        MyRs.Open(Condizione, OspitiDB, ADODB.CursorTypeEnum.adOpenKeyset, ADODB.LockTypeEnum.adLockOptimistic)
        If MyRs.EOF Then
            CampoOspite = ""
        Else
            CampoOspite = MoveFromDb(MyRs.Fields(Campo))
        End If
        MyRs.Close()
    End Function
    Public Function CampoParente(ByVal CodOsp As Long, ByVal CODICEPARENTE As Byte, ByVal Campo As String) As String
        Dim MyRs As New ADODB.Recordset
        Dim Condizione As String

        Condizione = "Select " & Campo & " From AnagraficaComune Where Tipologia = 'P' And CODICEOSPITE = " & CodOsp & " And CODICEPARENTE = " & CODICEPARENTE
        MyRs.Open(Condizione, OspitiDB, ADODB.CursorTypeEnum.adOpenKeyset, ADODB.LockTypeEnum.adLockOptimistic)

        If MyRs.EOF Then
            CampoParente = ""
        Else
            CampoParente = MoveFromDb(MyRs.Fields(Campo))
        End If
        MyRs.Close()
    End Function
    Public Function CampoCentroServizio(ByVal Cserv As String, ByVal Campo As String, Optional ByVal ControllaCampo As Boolean = True) As String
        Dim MyRs As New ADODB.Recordset
        Dim Condizione As String
        Condizione = "Select * From TABELLACENTROSERVIZIO Where CENTROSERVIZIO = '" & Cserv & "'"
        MyRs.Open(Condizione, OspitiDB, ADODB.CursorTypeEnum.adOpenKeyset, ADODB.LockTypeEnum.adLockOptimistic)

        If MyRs.EOF Then
            CampoCentroServizio = ""
        Else
            CampoCentroServizio = MoveFromDb(MyRs.Fields(Campo))
        End If
        MyRs.Close()
    End Function


    Public Function CampoParametri(ByVal Campo As String) As Object
        Dim MyRs As New ADODB.Recordset
        Dim Condizione As String
        Condizione = "Select * From TabellaParametri"
        MyRs.Open(Condizione, OspitiDB, ADODB.CursorTypeEnum.adOpenKeyset, ADODB.LockTypeEnum.adLockOptimistic)
        If MyRs.EOF Then
            CampoParametri = ""
        Else
            CampoParametri = MoveFromDb(MyRs.Fields(Campo))
        End If
        MyRs.Close()
    End Function
    Public Function CampoRegione(ByVal Regione As String, ByVal Campo As String, Optional ByVal ControllaCampo As Double = True) As String
        Dim MyRs As New ADODB.Recordset
        Dim Condizione As String
        Condizione = "Select * From AnagraficaComune Where Tipologia = 'R' And CodiceRegione = '" & Regione & "'"
        MyRs.Open(Condizione, OspitiDB, ADODB.CursorTypeEnum.adOpenKeyset, ADODB.LockTypeEnum.adLockOptimistic)
        If MyRs.EOF Then
            CampoRegione = 0
        Else
            CampoRegione = MoveFromDb(MyRs.Fields(Campo))
        End If
        MyRs.Close()
    End Function
    Public Function CampoOperazione(ByVal Codice As String, ByVal Campo As String) As String
        Dim MyRs As New ADODB.Recordset
        Dim Condizione As String
        Condizione = "Select * From TipoOperazione Where Codice = '" & Codice & "'"
        MyRs.Open(Condizione, OspitiDB, ADODB.CursorTypeEnum.adOpenKeyset, ADODB.LockTypeEnum.adLockOptimistic)
        If MyRs.EOF Then
            CampoOperazione = ""
        Else
            CampoOperazione = MoveFromDb(MyRs.Fields(Campo))
        End If
        MyRs.Close()
    End Function
    Public Sub MoveToDb(ByRef MyField As ADODB.Field, ByVal MyVal As Object)
        Select Case Val(MyField.Type)
            Case ADODB.DataTypeEnum.adDBDate Or ADODB.DataTypeEnum.adDBTimeStamp
                If IsDate(MyVal) Then
                    MyField.Value = MyVal
                Else
                    MyField.Value = 0
                End If
            Case ADODB.DataTypeEnum.adDouble
                If IsNumeric(MyVal) Then
                    MyField.Value = MyVal
                Else
                    MyField.Value = 0
                End If
            Case ADODB.DataTypeEnum.adSmallInt
                If IsNumeric(MyVal) Then
                    MyField.Value = MyVal
                Else : MyField.Value = 0
                End If
            Case ADODB.DataTypeEnum.adInteger
                If IsNumeric(MyVal) Then
                    MyField.Value = MyVal
                Else
                    MyField.Value = 0
                End If
            Case ADODB.DataTypeEnum.adNumeric
                If IsNumeric(MyVal) Then
                    MyField.Value = MyVal
                Else
                    MyField.Value = 0
                End If
            Case ADODB.DataTypeEnum.adTinyInt
                If IsNumeric(MyVal) Then
                    MyField.Value = MyVal
                Else
                    MyField.Value = 0
                End If
            Case ADODB.DataTypeEnum.adSingle
                If IsNumeric(MyVal) Then
                    MyField.Value = MyVal
                Else
                    MyField.Value = 0
                End If
            Case ADODB.DataTypeEnum.adLongVarBinary
                If IsNumeric(MyVal) Then
                    MyField.Value = MyVal
                Else
                    MyField.Value = 0
                End If
            Case ADODB.DataTypeEnum.adUnsignedTinyInt
                If IsNumeric(MyVal) Then
                    MyField.Value = MyVal
                Else
                    MyField.Value = 0
                End If
            Case ADODB.DataTypeEnum.adVarChar Or ADODB.DataTypeEnum.adVarWChar Or ADODB.DataTypeEnum.adWChar Or ADODB.DataTypeEnum.adBSTR
                If IsDBNull(MyVal) Then
                    MyField.Value = ""
                Else
                    If Len(MyVal) > MyField.DefinedSize Then
                        MyField.Value = Mid(MyVal, 1, MyField.DefinedSize)
                    Else
                        MyField.Value = MyVal
                    End If
                End If
            Case Else
                If Val(MyField.Type) = ADODB.DataTypeEnum.adVarChar Or Val(MyField.Type) = ADODB.DataTypeEnum.adVarWChar Or _
                   Val(MyField.Type) = ADODB.DataTypeEnum.adWChar Or Val(MyField.Type) = ADODB.DataTypeEnum.adBSTR Then
                    If IsDBNull(MyVal) Then
                        MyField.Value = ""
                    Else
                        If Len(MyVal) > MyField.DefinedSize Then
                            MyField.Value = Mid(MyVal, 1, MyField.DefinedSize)
                        Else
                            MyField.Value = MyVal
                        End If
                    End If
                Else
                    MyField.Value = MyVal
                End If
        End Select

    End Sub

    Public Function MoveFromDbWC(ByVal MyRs As ADODB.Recordset, ByVal Campo As String) As Object
        Dim MyField As ADODB.Field

#If FLAG_DEBUG = False Then
        On Error GoTo ErroreCampoInesistente
#End If
        MyField = MyRs.Fields(Campo)

        Select Case MyField.Type
            Case ADODB.DataTypeEnum.adDBDate
                If IsDBNull(MyField.Value) Then
                    MoveFromDbWC = "__/__/____"
                Else
                    If Not IsDate(MyField.Value) Then
                        'Or MyField.Value = "0.00.00" 
                        MoveFromDbWC = "__/__/____"
                    Else
                        MoveFromDbWC = MyField.Value
                    End If
                End If
            Case ADODB.DataTypeEnum.adDBTimeStamp
                If IsDBNull(MyField.Value) Then
                    MoveFromDbWC = "__/__/____"
                Else
                    If Not IsDate(MyField.Value) Then
                        'Or MyField.Value = "0.00.00"
                        MoveFromDbWC = "__/__/____"
                    Else
                        MoveFromDbWC = MyField.Value
                    End If
                End If
            Case ADODB.DataTypeEnum.adSmallInt
                If IsDBNull(MyField.Value) Then
                    MoveFromDbWC = 0
                Else
                    MoveFromDbWC = MyField.Value
                End If
            Case ADODB.DataTypeEnum.adInteger
                If IsDBNull(MyField.Value) Then
                    MoveFromDbWC = 0
                Else
                    MoveFromDbWC = MyField.Value
                End If
            Case ADODB.DataTypeEnum.adNumeric
                If IsDBNull(MyField.Value) Then
                    MoveFromDbWC = 0
                Else
                    MoveFromDbWC = MyField.Value
                End If
            Case ADODB.DataTypeEnum.adTinyInt
                If IsDBNull(MyField.Value) Then
                    MoveFromDbWC = 0
                Else
                    MoveFromDbWC = MyField.Value
                End If
            Case ADODB.DataTypeEnum.adSingle
                If IsDBNull(MyField.Value) Then
                    MoveFromDbWC = 0
                Else
                    MoveFromDbWC = MyField.Value
                End If
            Case ADODB.DataTypeEnum.adLongVarBinary
                If IsDBNull(MyField.Value) Then
                    MoveFromDbWC = 0
                Else
                    MoveFromDbWC = MyField.Value
                End If
            Case ADODB.DataTypeEnum.adDouble
                If IsDBNull(MyField.Value) Then
                    MoveFromDbWC = 0
                Else
                    MoveFromDbWC = MyField.Value
                End If
            Case ADODB.DataTypeEnum.adVarChar
                If IsDBNull(MyField.Value) Then
                    MoveFromDbWC = vbNullString
                Else
                    MoveFromDbWC = CStr(MyField.Value)
                End If
            Case ADODB.DataTypeEnum.adUnsignedTinyInt
                If IsDBNull(MyField.Value) Then
                    MoveFromDbWC = 0
                Else
                    MoveFromDbWC = MyField.Value
                End If
            Case Else
                If IsDBNull(MyField.Value) Then
                    MoveFromDbWC = ""
                End If
                If IsDBNull(MyField.Value) Then
                    MoveFromDbWC = ""
                Else
                    MoveFromDbWC = MyField.Value
                End If
        End Select

        On Error GoTo 0
        Exit Function
ErroreCampoInesistente:

        On Error GoTo 0
    End Function
    Private Function MoveFromDb(ByVal MyField As ADODB.Field, Optional ByVal Tipo As Integer = 0) As Object
        If Tipo = 0 Then
            Select Case MyField.Type
                Case ADODB.DataTypeEnum.adDBDate Or ADODB.DataTypeEnum.adDBTimeStamp
                    If IsDBNull(MyField.Value) Then
                        MoveFromDb = "__/__/____"
                    Else
                        If Not IsDate(MyField.Value) Then
                            MoveFromDb = "__/__/____"
                        Else
                            MoveFromDb = MyField.Value
                        End If
                    End If
                Case ADODB.DataTypeEnum.adSmallInt
                    If IsDBNull(MyField.Value) Then
                        MoveFromDb = 0
                    Else
                        MoveFromDb = MyField.Value
                    End If
                Case 20 ' INTERO PER MYSQL
                    If IsDBNull(MyField.Value) Then
                        MoveFromDb = 0
                    Else
                        MoveFromDb = MyField.Value
                    End If
                Case ADODB.DataTypeEnum.adInteger
                    If IsDBNull(MyField.Value) Then
                        MoveFromDb = 0
                    Else
                        MoveFromDb = MyField.Value
                    End If
                Case ADODB.DataTypeEnum.adNumeric
                    If IsDBNull(MyField.Value) Then
                        MoveFromDb = 0
                    Else
                        MoveFromDb = MyField.Value
                    End If
                Case ADODB.DataTypeEnum.adTinyInt
                    If IsDBNull(MyField.Value) Then
                        MoveFromDb = 0
                    Else
                        MoveFromDb = MyField.Value
                    End If
                Case ADODB.DataTypeEnum.adSingle
                    If IsDBNull(MyField.Value) Then
                        MoveFromDb = 0
                    Else
                        MoveFromDb = MyField.Value
                    End If
                Case ADODB.DataTypeEnum.adLongVarBinary
                    If IsDBNull(MyField.Value) Then
                        MoveFromDb = 0
                    Else
                        MoveFromDb = MyField.Value
                    End If
                Case ADODB.DataTypeEnum.adDouble
                    If IsDBNull(MyField.Value) Then
                        MoveFromDb = 0
                    Else
                        MoveFromDb = MyField.Value
                    End If
                Case 139
                    If IsDBNull(MyField.Value) Then
                        MoveFromDb = 0
                    Else
                        MoveFromDb = MyField.Value
                    End If
                Case ADODB.DataTypeEnum.adVarChar
                    If IsDBNull(MyField.Value) Then
                        MoveFromDb = vbNullString
                    Else
                        MoveFromDb = CStr(MyField.Value)
                    End If
                Case ADODB.DataTypeEnum.adUnsignedTinyInt
                    If IsDBNull(MyField.Value) Then
                        MoveFromDb = 0
                    Else
                        MoveFromDb = MyField.Value
                    End If
                Case Else
                    If IsDBNull(MyField.Value) Then
                        MoveFromDb = ""
                    End If
                    If IsDBNull(MyField.Value) Then
                        MoveFromDb = ""
                    Else
                        'MoveFromDb = StringaSqlS(MyField.Value)
                        MoveFromDb = RTrim(MyField.Value)
                    End If
            End Select
        End If
    End Function

    Public Function GiorniMese(ByVal Mese As Byte, ByVal Anno As Integer) As Byte
        If Mese = 1 Or Mese = 3 Or Mese = 5 Or Mese = 7 Or Mese = 8 Or _
           Mese = 10 Or Mese = 12 Then
            GiorniMese = 31
        Else
            If Mese <> 2 Then
                GiorniMese = 30
            Else
                If Day(DateSerial(Anno, Mese, 29)) = 29 Then
                    GiorniMese = 29
                Else
                    GiorniMese = 28
                End If
            End If
        End If
    End Function


    Public Function DescrizioneRigaOspite(ByVal CodiceOspite As Long, ByVal Anno As Integer, ByVal Mese As Integer) As String
        Dim MyRs As New ADODB.Recordset

        DescrizioneRigaOspite = ""
        MyRs.Open("Select * From IMPORTOCOMUNE where  CODICEOSPITE = " & CodiceOspite & " And Data <= {ts '" & Format(DateSerial(Anno, Mese, GiorniMese(Mese, Anno)), "yyyy-MM-dd") & " 00:00:00'} Order by Data Desc", OspitiDB, ADODB.CursorTypeEnum.adOpenKeyset, ADODB.LockTypeEnum.adLockOptimistic)
        If Not MyRs.EOF Then
            DescrizioneRigaOspite = MoveFromDb(MyRs.Fields("DescrizioneRiga"))
        End If
        MyRs.Close()

    End Function

    Public Function DecodificaModalita(ByVal Codice As String) As String
        Dim RS_ModalitaPagamentoOspite As New ADODB.Recordset

        RS_ModalitaPagamentoOspite.Open("Select * From ModalitaPagamento Where codice  = '" & Codice & "'", OspitiDB, ADODB.CursorTypeEnum.adOpenKeyset, ADODB.LockTypeEnum.adLockOptimistic)
        If Not RS_ModalitaPagamentoOspite.EOF Then
            DecodificaModalita = MoveFromDb(RS_ModalitaPagamentoOspite.Fields("Descrizione"))
        End If
        RS_ModalitaPagamentoOspite.Close()
    End Function

    Public Function DecodificaCausaleEntrataUscita(ByVal Causale As String) As String
        Dim Rs_Regione As New ADODB.Recordset
        Dim MySql As String

        If Causale <> "" Then
            MySql = "Select * From CAUSALI Where CODICE = '" & Causale & "' "
            Rs_Regione.Open(MySql, OspitiDB, ADODB.CursorTypeEnum.adOpenKeyset, ADODB.LockTypeEnum.adLockOptimistic)
            If Rs_Regione.EOF Then
                DecodificaCausaleEntrataUscita = "Causale Non Decodificata"
            Else
                DecodificaCausaleEntrataUscita = MoveFromDbWC(Rs_Regione, "Descrizione")
            End If
            Rs_Regione.Close()
        Else
            DecodificaCausaleEntrataUscita = "Causale Non Decodificata"
        End If
    End Function
    Public Function CampoTipoPagamento(ByVal Codice As String, ByVal Campo As String) As String
        Dim MyRs As New ADODB.Recordset
        MyRs.Open("Select * From TipoPagamento Where Codice = '" & Codice & "'", TabelleDb, ADODB.CursorTypeEnum.adOpenKeyset, ADODB.LockTypeEnum.adLockOptimistic)

        If MyRs.EOF Then
            CampoTipoPagamento = ""
        Else
            CampoTipoPagamento = MoveFromDb(MyRs.Fields(Campo))
        End If
        MyRs.Close()
    End Function


    Public Function ImportoTuttiExtrDoc(ByVal Numero As Long) As Double
        Dim MyRs As New ADODB.Recordset


        MyRs.Open("Select Sum(Importo) From MovimentiContabiliRiga Where Numero = " & Numero & " And RigaDaCausale = 4", GeneraleDB, ADODB.CursorTypeEnum.adOpenKeyset, ADODB.LockTypeEnum.adLockOptimistic)
        ImportoTuttiExtrDoc = MoveFromDb(MyRs.Fields(0))
        MyRs.Close()
    End Function





    Protected Overrides Sub Finalize()
        MyBase.Finalize()
    End Sub
End Class
