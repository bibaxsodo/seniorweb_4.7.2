﻿Imports Microsoft.VisualBasic

<Serializable()> Public Class Cls_SqlString
    Public Campo(100) As String
    Public Valore(100) As String

    Public Sub Add(ByVal Vcampo As String, ByVal Vvalore As String)
        Dim I As Integer
        For I = 0 To 100
            If Campo(I) = "" Then
                Exit For
            End If
        Next
        Campo(I) = Vcampo
        Valore(I) = Vvalore
    End Sub


    Public Function GetValue(ByVal Vcampo As String) As String
        Dim I As Integer
        For I = 0 To 100
            If Campo(I) = Vcampo Then
                Return Valore(I)

            End If
        Next
        Return ""
    End Function
End Class
