Imports Microsoft.VisualBasic
Imports System.Data.OleDb
Imports System.Web.Hosting

Public Class Cls_CausaliEntrataUscita
    Public Codice As String
    Public Raggruppamento As String
    Public Descrizione As String
    Public Regole As String
    Public TIPOMOVIMENTO As String
    Public GiornoUscita As String
    Public Diurno As Long
    Public Utente As String
    Public CheckCausale As String

    Function campodb(ByVal oggetto As Object) As String
        If IsDBNull(oggetto) Then
            Return ""
        Else
            Return oggetto
        End If
    End Function

    Public Function CheckCausaleFunction(ByVal Testo As String) As String
        CheckCausaleFunction = ""
        Testo = UCase(Testo)
        If InStr(1, Testo, UCase("SalvaImpExtrOspite")) > 0 Or _
           InStr(1, Testo, UCase("SalvaImpExtrParent")) > 0 Or _
           InStr(1, Testo, UCase("SalvaImpExtrComune")) > 0 Then
            CheckCausaleFunction = CheckCausaleFunction & "A"
        End If

        If InStr(1, Testo, UCase("SalvaParente")) > 0 Or _
           InStr(1, Testo, UCase("SalvaPerc")) > 0 Then
            CheckCausaleFunction = CheckCausaleFunction & "B"
        End If

        If InStr(1, Testo, UCase("GiorniPresParente")) > 0 Or _
            InStr(1, Testo, UCase("GiorniAssParente")) > 0 Or _
            InStr(1, Testo, UCase("GiorniPresComune")) > 0 Or _
            InStr(1, Testo, UCase("GiorniPresRegione")) > 0 Or _
            InStr(1, Testo, UCase("GiorniAssRegione")) > 0 Then
            CheckCausaleFunction = CheckCausaleFunction & "C"
        End If

        If InStr(1, Testo, UCase("ComuneResidenza")) > 0 Then
            CheckCausaleFunction = CheckCausaleFunction & "-"
        End If


        If InStr(1, Testo, UCase("NonGiorniPresParente")) > 0 Or _
            InStr(1, Testo, UCase("NonGiorniAssParente")) > 0 Or _
            InStr(1, Testo, UCase("NonGiorniPresComune")) > 0 Or _
            InStr(1, Testo, UCase("NonGiorniAssComune")) > 0 Or _
            InStr(1, Testo, UCase("NonGiorniPresRegione")) > 0 Or _
            InStr(1, Testo, UCase("NonGiorniAssRegione")) > 0 Then
            CheckCausaleFunction = CheckCausaleFunction & "D"
        End If


        If InStr(1, Testo, UCase("CodiceComune")) > 0 Or _
           InStr(1, Testo, UCase("CodiceRegione")) > 0 Then
            CheckCausaleFunction = CheckCausaleFunction & "E"
        End If


        If InStr(1, Testo, UCase("pXSalvaExtImporto1")) > 0 Or _
           InStr(1, Testo, UCase("pXSalvaExtImporto2")) > 0 Or _
           InStr(1, Testo, UCase("pXSalvaExtImporto3")) > 0 Or _
           InStr(1, Testo, UCase("pXSalvaExtImporto4")) > 0 Or _
           InStr(1, Testo, UCase("ImpExtrParMan1")) > 0 Or _
           InStr(1, Testo, UCase("ImpExtrParMan2")) > 0 Or _
           InStr(1, Testo, UCase("ImpExtrParMan3")) > 0 Or _
           InStr(1, Testo, UCase("ImpExtrParMan4")) > 0 Then            
            CheckCausaleFunction = CheckCausaleFunction & "F"
        End If






        If InStr(1, Testo, UCase("NumeroComune")) > 0 Or _
           InStr(1, Testo, UCase("NumeroRegione")) > 0 Then
            CheckCausaleFunction = CheckCausaleFunction & "G"
        End If


        If InStr(1, Testo, UCase("SalvaComune")) > 0 Or _
           InStr(1, Testo, UCase("SalvaRegione")) > 0 Or _
           InStr(1, Testo, UCase("SalvaOspite")) > 0 Or _
           InStr(1, Testo, UCase("SalvaEnte")) > 0 Then
            CheckCausaleFunction = CheckCausaleFunction & "H"
        End If



        If InStr(1, Testo, UCase("Variabile1")) > 0 Or _
           InStr(1, Testo, UCase("Variabile2")) > 0 Or _
           InStr(1, Testo, UCase("Variabile3")) > 0 Or _
           InStr(1, Testo, UCase("Variabile4")) > 0 Then
            CheckCausaleFunction = CheckCausaleFunction & "I"
        End If

        If InStr(1, Testo, UCase("XSalvaExtImporto1")) > 0 Or _
           InStr(1, Testo, UCase("XSalvaExtImporto2")) > 0 Or _
           InStr(1, Testo, UCase("XSalvaExtImporto3")) > 0 Or _
           InStr(1, Testo, UCase("XSalvaExtImporto4")) > 0 Or _
           InStr(1, Testo, UCase("ImpExtrOspMan")) > 0 Then

            CheckCausaleFunction = CheckCausaleFunction & "L"
        End If

        If InStr(1, Testo, UCase("XSalvaExtImporto1C")) > 0 Or _
           InStr(1, Testo, UCase("XSalvaExtImporto2C")) > 0 Or _
           InStr(1, Testo, UCase("XSalvaExtImporto3C")) > 0 Or _
           InStr(1, Testo, UCase("XSalvaExtImporto4C")) > 0 Or _
           InStr(1, Testo, UCase("ImpExtrComMan")) > 0 Then
            CheckCausaleFunction = CheckCausaleFunction & "M"
        End If

        If InStr(1, Testo, UCase("Mese")) > 0 Or _
           InStr(1, Testo, UCase("Anno")) > 0 Then
            CheckCausaleFunction = CheckCausaleFunction & "N"
        End If

        If InStr(1, Testo, UCase("GiorniPres")) > 0 Or _
           InStr(1, Testo, UCase("GiorniAss")) > 0 Or _
           InStr(1, Testo, UCase("GiorniPresEnte")) > 0 Or _
           InStr(1, Testo, UCase("GiorniAssEnte")) > 0 Then
            CheckCausaleFunction = CheckCausaleFunction & "O"
        End If

        If InStr(1, Testo, UCase("NonGiorniPres")) > 0 Or _
           InStr(1, Testo, UCase("NonGiorniAss")) > 0 Or _
           InStr(1, Testo, UCase("NonGiorniPresEnte")) > 0 Or _
           InStr(1, Testo, UCase("NonGiorniAssEnte")) > 0 Then
            CheckCausaleFunction = CheckCausaleFunction & "P"
        End If

    End Function


    Sub Scrivi(ByVal StringaConnessione As String)
        Dim cn As OleDbConnection
        Dim MySql As String = ""

        cn = New Data.OleDb.OleDbConnection(StringaConnessione)

        cn.Open()
        Dim cmd As New OleDbCommand()
        cmd.CommandText = ("select * from CAUSALI Where codice = ?")

        cmd.Parameters.AddWithValue("@codice", Codice)
        cmd.Connection = cn
        Dim sb As StringBuilder = New StringBuilder

        
        Dim myPOSTreader As OleDbDataReader = cmd.ExecuteReader()
        If myPOSTreader.Read Then
            MySql = "UPDATE CAUSALI SET  " & _
                   " RaggruppamentoCausale = ?, " & _
                    " Descrizione = ?, " & _
                    " Regole = ?, " & _
                    " TIPOMOVIMENTO = ?, " & _
                    " GiornoUscita = ?, " & _
                    " Diurno = ?, " & _
                    " ChekCausale = ?," & _
                    " Utente = ?, " & _
                    " DataAggiornamento = ? " & _
                    " WHERE CODICE = ? "
        Else
            MySql = "INSERT INTO CAUSALI  (RaggruppamentoCausale,Descrizione,Regole,TIPOMOVIMENTO,GiornoUscita,Diurno,ChekCausale,Utente,DataAggiornamento,CODICE) VALUES " & _
                                              "(?,?,?,?,?,?,?,?,?,?) "
        End If
        Dim cmdw As New OleDbCommand()

        cmdw.Connection = cn
        cmdw.CommandText = (MySql)
        cmdw.Parameters.AddWithValue("@Raggruppamento", Raggruppamento)
        cmdw.Parameters.AddWithValue("Descrizione", Descrizione)
        cmdw.Parameters.AddWithValue("Regole", Regole)
        cmdw.Parameters.AddWithValue("TIPOMOVIMENTO", TIPOMOVIMENTO)
        cmdw.Parameters.AddWithValue("GiornoUscita", GiornoUscita)
        cmdw.Parameters.AddWithValue("Diurno", Diurno)
        cmdw.Parameters.AddWithValue("CheckCausale", CheckCausale)
        cmdw.Parameters.AddWithValue("Utente", Utente)
        cmdw.Parameters.AddWithValue("DataAggiornamento", Now)
        cmdw.Parameters.AddWithValue("CODICE", Codice)
        cmdw.ExecuteNonQuery()
        myPOSTreader.Close()
        cn.Close()
    End Sub

    Function VerificaElimina(ByVal StringaConnessione As String) As Boolean
        Dim cn As OleDbConnection



        cn = New Data.OleDb.OleDbConnection(StringaConnessione)
        VerificaElimina = True
        cn.Open()
        Dim cmd As New OleDbCommand()
        cmd.CommandText = ("Select * from MOVIMENTI where " & _
                           "Causale = ? " )
        cmd.Parameters.AddWithValue("@Causale", Codice)
        cmd.Connection = cn
        Dim myPOSTreader As OleDbDataReader = cmd.ExecuteReader()
        If myPOSTreader.Read Then
            VerificaElimina = False
        End If
        cn.Close()
    End Function

    Sub Elimina(ByVal StringaConnessione As String)
        Dim cn As OleDbConnection


        cn = New Data.OleDb.OleDbConnection(StringaConnessione)

        cn.Open()
        Dim cmd As New OleDbCommand()
        cmd.CommandText = ("DELETE from CAUSALI Where codice = ?")

        cmd.Parameters.AddWithValue("@codice", Codice)
        cmd.Connection = cn
        cmd.ExecuteNonQuery()
    End Sub

    Sub LeggiCausale(ByVal StringaConnessione As String)
        Dim cn As OleDbConnection


        cn = New Data.OleDb.OleDbConnection(StringaConnessione)

        cn.Open()
        Dim cmd As New OleDbCommand()
        cmd.CommandText = ("select * from CAUSALI Where codice = ?")

        cmd.Parameters.AddWithValue("@codice", Codice)
        cmd.Connection = cn

        Raggruppamento = ""
        Descrizione = ""

        Dim myPOSTreader As OleDbDataReader = cmd.ExecuteReader()
        If myPOSTreader.Read Then
            Codice = myPOSTreader.Item("Codice")

            Raggruppamento = campodb(myPOSTreader.Item("Raggruppamentocausale"))
            Descrizione = campodb(myPOSTreader.Item("Descrizione"))
            Regole = campodb(myPOSTreader.Item("Regole"))
            TIPOMOVIMENTO = campodb(myPOSTreader.Item("TIPOMOVIMENTO"))
            GiornoUscita = campodb(myPOSTreader.Item("GiornoUscita"))
            Diurno = Val(campodb(myPOSTreader.Item("Diurno")))
            CheckCausale = campodb(myPOSTreader.Item("ChekCausale"))

        End If
        myPOSTreader.Close()
        cn.Close()
    End Sub

    Sub LeggiCausaleColore(ByVal StringaConnessione As String)
        Dim cn As OleDbConnection


        cn = New Data.OleDb.OleDbConnection(StringaConnessione)

        cn.Open()
        Dim cmd As New OleDbCommand()
        cmd.CommandText = ("select * from CAUSALI Where Diurno = ?")

        cmd.Parameters.AddWithValue("@Diurno", Diurno)
        cmd.Connection = cn

        Raggruppamento = ""
        Descrizione = ""

        Dim myPOSTreader As OleDbDataReader = cmd.ExecuteReader()
        If myPOSTreader.Read Then
            Codice = myPOSTreader.Item("Codice")

            Raggruppamento = campodb(myPOSTreader.Item("Raggruppamentocausale"))
            Descrizione = campodb(myPOSTreader.Item("Descrizione"))
            Regole = campodb(myPOSTreader.Item("Regole"))
            TIPOMOVIMENTO = campodb(myPOSTreader.Item("TIPOMOVIMENTO"))
            GiornoUscita = campodb(myPOSTreader.Item("GiornoUscita"))
            Diurno = Val(campodb(myPOSTreader.Item("Diurno")))
            CheckCausale = campodb(myPOSTreader.Item("ChekCausale"))
        End If
        myPOSTreader.Close()
        cn.Close()
    End Sub
    Sub LeggiCausaleDescrizione(ByVal StringaConnessione As String)
        Dim cn As OleDbConnection


        cn = New Data.OleDb.OleDbConnection(StringaConnessione)

        cn.Open()
        Dim cmd As New OleDbCommand()
        cmd.CommandText = ("select * from CAUSALI Where Descrizione = ?")

        cmd.Parameters.AddWithValue("@Descrizione", Descrizione)
        cmd.Connection = cn

        Raggruppamento = ""
        Descrizione = ""

        Dim myPOSTreader As OleDbDataReader = cmd.ExecuteReader()
        If myPOSTreader.Read Then
            Codice = myPOSTreader.Item("Codice")

            Raggruppamento = campodb(myPOSTreader.Item("Raggruppamentocausale"))
            Descrizione = campodb(myPOSTreader.Item("Descrizione"))
            Regole = campodb(myPOSTreader.Item("Regole"))
            TIPOMOVIMENTO = campodb(myPOSTreader.Item("TIPOMOVIMENTO"))
            GiornoUscita = campodb(myPOSTreader.Item("GiornoUscita"))
            CheckCausale = campodb(myPOSTreader.Item("ChekCausale"))
            Diurno = Val(campodb(myPOSTreader.Item("Diurno")))

        End If
        myPOSTreader.Close()
        cn.Close()
    End Sub



    Public Sub UpDateDropBox(ByVal StringaConnessione As String, ByVal Codice As String, ByRef appoggio As DropDownList)
        Dim cn As OleDbConnection



        cn = New Data.OleDb.OleDbConnection(StringaConnessione)

        cn.Open()
        Dim cmd As New OleDbCommand()

        If Codice = "99" Then
            cmd.CommandText = ("select * from CAUSALI Order by Descrizione")
        Else
            If Codice = "" Then
                cmd.CommandText = ("select * from CAUSALI Where Raggruppamentocausale = '' or Raggruppamentocausale Is Null Order by Descrizione")
            Else
                cmd.CommandText = ("select * from CAUSALI Where Raggruppamentocausale  = '" & Codice & "' Order by Descrizione")
            End If
        End If
        cmd.Connection = cn
        Dim sb As StringBuilder = New StringBuilder

        appoggio.Items.Clear()
        Dim myPOSTreader As OleDbDataReader = cmd.ExecuteReader()
        Do While myPOSTreader.Read
            appoggio.Items.Add(myPOSTreader.Item("Descrizione"))
            appoggio.Items(appoggio.Items.Count - 1).Value = myPOSTreader.Item("CODICE")
        Loop
        myPOSTreader.Close()
        cn.Close()
        appoggio.Items.Add("")
        appoggio.Items(appoggio.Items.Count - 1).Value = ""
        appoggio.Items(appoggio.Items.Count - 1).Selected = True
    End Sub


    Public Sub UpDateDropBoxCodiceDescrizione(ByVal StringaConnessione As String, ByRef appoggio As DropDownList)
        Dim cn As OleDbConnection



        cn = New Data.OleDb.OleDbConnection(StringaConnessione)

        cn.Open()
        Dim cmd As New OleDbCommand()


        cmd.CommandText = ("select * from CAUSALI Order by Codice,Descrizione")
        cmd.Connection = cn
        Dim sb As StringBuilder = New StringBuilder

        appoggio.Items.Clear()
        Dim myPOSTreader As OleDbDataReader = cmd.ExecuteReader()
        Do While myPOSTreader.Read
            appoggio.Items.Add(myPOSTreader.Item("CODICE") & " " & myPOSTreader.Item("Descrizione"))
            appoggio.Items(appoggio.Items.Count - 1).Value = myPOSTreader.Item("CODICE")
        Loop
        myPOSTreader.Close()
        cn.Close()
        appoggio.Items.Add("")
        appoggio.Items(appoggio.Items.Count - 1).Value = ""
        appoggio.Items(appoggio.Items.Count - 1).Selected = True
    End Sub
End Class
