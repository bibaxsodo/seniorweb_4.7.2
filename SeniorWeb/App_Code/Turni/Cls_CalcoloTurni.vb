﻿Imports Microsoft.VisualBasic
Imports System.Data.OleDb

Public Class Cls_CalcoloTurni

    Public ConnectionString As String
    Public Utente As String

    Public ParametroA As String
    Public ParametroB As String
    Public ParametroC As String
    Public ParametroD As String
    Public ParametroE As String
    Public ParametroF As String
    Public ParametroG As String
    Public ParametroH As String
    Public ParametroI As String
    Public ParametroJ As String
    Public ParametroK As String

    'Public ControlloTipoAutomatico As Boolean

    Public CodiceDipendente As Long
    Public Familiare As Byte
    Public MenuGo As Boolean
    Public SchedeGo As Boolean
    Public OrdiniServizioGo As Boolean
    Public VariazioniOrdiniServizioGo As Boolean
    Public VisualizzaModificaRigheOrdiniServizioGo As Boolean
    Public VisualizzaModificaRigheOrdiniServizioComandatiGo As Boolean
    Public OrdiniServizioComandatiGo As Boolean
    Public QuadroAssenzeGo As Boolean
    Public QuadroStraordinariGo As Boolean
    Public QuadroOrdiniServizioGo As Boolean
    Public QuadroOrdiniServizioComandatiGo As Boolean
    Public QuadroOrdiniServizioTuttiGruppiGo As Boolean
    Public RicercaAnagraficaGo As Boolean
    Public SelezioneApparatiGo As Boolean
    Public SelezioneDipendentiGo As Boolean
    Public SelezioneDipendentiComandatiGo As Boolean
    Public SelezioneMezziGo As Boolean
    Public StatisticaPresenzaOrariaGo As Boolean

    Public Fase As String
    Public Controllo As Boolean

    Public CodicePerCopiaIncolla As Long
    Public CodiciPerCopiaMultipla As String
    Public CopiaDaVisualizzaOrdiniServizioComandati As String
    Public ApparatoPerCopiaIncolla As String
    Public MezzoPerCopiaIncolla As String

    Public Variabile(9) As Object

    Public W_TempoStandard As Double
    Public W_TempoMese As Double
    Public W_OrarioDomenica As Date
    Public W_OrarioLunedi As Date
    Public W_OrarioMartedi As Date
    Public W_OrarioMercoledi As Date
    Public W_OrarioGiovedi As Date
    Public W_OrarioVenerdi As Date
    Public W_OrarioSabato As Date
    Public W_OrarioDelGiorno As Date
    Public W_OrarioContrattuale As Single
    Public W_PercentualePartimeGiorni As Single
    Public W_PercentualePartimeGiorniDelMese As Single
    Public W_PercentualePartimeOre As Single
    Public W_PercentualePartimeOreDelMese As Single
    Public W_TipoPartime As String

    Public FileNameDataBaseName As String

    ' Tabelle per Formule

    Public CodiceFormula(100) As String
    Public DataFormula(100) As Date
    Public ScriptFormula(100) As String
    Public ValoriFormula(100) As String

    Public CodiceFormulaCG(100) As String
    Public DataFormulaCG(100) As Date
    Public ScriptFormulaCG(100) As String
    Public ValoriFormulaCG(100) As String

    Public CodiceFormulaCT(100) As String
    Public DataFormulaCT(100) As Date
    Public ScriptFormulaCT(100) As String
    Public ValoriFormulaCT(100) As String

    Public CodiceFormulaCTT(100) As Integer
    Public DataFormulaCTT(100) As Date
    Public ScriptFormulaCTT(100) As String
    Public ValoriFormulaCTT(100) As String

    Public CodiceFormulaDV(100) As String
    Public DataFormulaDV(100) As Date
    Public ScriptFormulaDV(100) As String
    Public ValoriFormulaDV(100) As String

    Public CodiceFormulaSSI(100) As String
    Public DataFormulaSSI(100) As Date
    Public ScriptFormulaSSI(100) As String
    Public ValoriFormulaSSI(100) As String

    ' Campi di CreaOrariTimbrature

    Private AlleGiornoPrecedente As String
    Private DeltaAlto As String
    Private DeltaBasso As String
    Private DeltaGiustificativo As String
    Private DeltaNelGruppo As String
    Private DeltaFamiliare As Byte
    Private DeltaTipo As String
    Private DeltaTipoServizio As String
    Private DeltaGiornoSuccessivo As String

    ' Campi di ControlloIntervalloDiTempo

    Private GiornoPrecedenteFestivitaSettimanale As Byte
    Private FestivitaSettimanale As Byte
    Private GiornoSuccessivoFestivitaSettimanale As Byte
    Private X_Precedente As Byte
    Private Intervallo_Precedente As Long
    Private Tipo_Precedente As String

    Private Work_1_NelGiorno As String
    Private Work_2_NelGiorno As String
    Private Pausa_NelGiorno As String
    Private Lavorate_NelGiorno As String
    Private Lavorate_Diurne As String
    Private Lavorate_Notturne As String
    Private Lavorate_FestiveDiurne As String
    Private Lavorate_FestiveNotturne As String
    Private Lavorate_Retribuite As String
    Private OperazionePausa As String
    Private OperazioneTempo As String
    Private OperazioneOreLavorate As String
    Private OperazioneOreFeriali As String
    Private OperazioneOreFestive As String
    Private OperazioneOreRetribuite As String
    Private Ore_Pausa As String
    Private Ore_Intervallo As String
    Private Ore_Diurne As String
    Private Ore_Notturne As String
    Private Ore_FestiveDiurne As String
    Private Ore_FestiveNotturne As String
    Private Orario_Dalle As String
    Private Orario_Alle As String
    Private Causale As String
    Private Colore As Long
    Private NelGruppo As String
    Private Giustificativo As String
    Private FlagGiornoSuccessivo As String
    Private Diurno_Dalle As String
    Private Diurno_Alle As String

    Private Risultato As String
    Private DataAssunzione As Date
    Private DataLicenziamento As Date
    Private Validita As Date

    Private Sw_Ok As Boolean

    Private Numero As Single
    Private a As String
    Private Testo As String

    Private W_Dalle As String
    Private W_DalleAlto As String
    Private W_DalleBasso As String
    Private W_DalleObbligo As String
    Private W_Dalle_Causale As String
    Private W_Dalle_Colore As Long
    Private W_Alle As String
    Private W_AlleAlto As String
    Private W_AlleBasso As String
    Private W_AlleObbligo As String
    Private W_Alle_Causale As String
    Private W_Alle_Colore As Long
    Private W_Pausa As String
    Private W_Giustificativo As String
    Private W_NelGruppo As String
    Private W_Familiare As Byte
    Private W_Tipo As String
    Private W_TipoServizio As String
    Private W_GiornoSuccessivo As String
    Private W_OrariAlle As String


    Private W_Tempo As Single
    Private Ore As Integer
    Private Minuti As Integer

    Private vbYellow As Long = RGB(255, 255, 0)
    Private vbWhite As Long = RGB(255, 255, 255)

    Private Function StringaDb(ByVal Oggetto As Object) As String
        If IsDBNull(Oggetto) Then
            Return ""
        Else
            Return Oggetto
        End If
    End Function

    Private Function DataDb(ByVal Oggetto As Object) As Date
        If Not IsDate(Oggetto) Then
            Return Nothing
        Else
            Return Oggetto
        End If
    End Function

    Private Function NumeroDb(ByVal Oggetto As Object) As Object
        If IsDBNull(Oggetto) Then
            Return 0
        Else
            Return Oggetto
        End If
    End Function

    Public Function GiornoSettimana(ByVal i As Byte) As String
        Select Case i
            Case 1
                GiornoSettimana = "Domenica"
            Case 2
                GiornoSettimana = "Lunedi"
            Case 3
                GiornoSettimana = "Martedi"
            Case 4
                GiornoSettimana = "Mercoledi"
            Case 5
                GiornoSettimana = "Giovedi"
            Case 6
                GiornoSettimana = "Venerdi"
            Case 7
                GiornoSettimana = "Sabato"
            Case Else
                GiornoSettimana = "Errore"
        End Select
    End Function

    Public Function CodificaMese(ByVal Descrizione As String) As Byte
        CodificaMese = 0
        Select Case Descrizione
            Case "Gennaio"
                CodificaMese = 1
            Case "Febbraio"
                CodificaMese = 2
            Case "Marzo"
                CodificaMese = 3
            Case "Aprile"
                CodificaMese = 4
            Case "Maggio"
                CodificaMese = 5
            Case "Giugno"
                CodificaMese = 6
            Case "Luglio"
                CodificaMese = 7
            Case "Agosto"
                CodificaMese = 8
            Case "Settembre"
                CodificaMese = 9
            Case "Ottobre"
                CodificaMese = 10
            Case "Novembre"
                CodificaMese = 11
            Case "Dicembre"
                CodificaMese = 12
        End Select
    End Function

    Public Function DecodificaMese(ByVal Mese As Byte) As String
        DecodificaMese = "Errore nel Mese"
        Select Case Mese
            Case 1
                DecodificaMese = "Gennaio"
            Case 2
                DecodificaMese = "Febbraio"
            Case 3
                DecodificaMese = "Marzo"
            Case 4
                DecodificaMese = "Aprile"
            Case 5
                DecodificaMese = "Maggio"
            Case 6
                DecodificaMese = "Giugno"
            Case 7
                DecodificaMese = "Luglio"
            Case 8
                DecodificaMese = "Agosto"
            Case 9
                DecodificaMese = "Settembre"
            Case 10
                DecodificaMese = "Ottobre"
            Case 11
                DecodificaMese = "Novembre"
            Case 12
                DecodificaMese = "Dicembre"
        End Select
    End Function

    Private Sub Azzera_FestivitaSettimanale()
        GiornoPrecedenteFestivitaSettimanale = 0
        FestivitaSettimanale = 0
        GiornoSuccessivoFestivitaSettimanale = 0
    End Sub

    '------------------------------------------------------------
    ' Trova l'ultimo giorno del mese
    '------------------------------------------------------------
    Function GiorniMese(ByVal Mese As Byte, ByVal Anno As Integer) As Byte
        If Mese = 1 Or Mese = 3 Or Mese = 5 Or Mese = 7 Or Mese = 8 Or _
          Mese = 10 Or Mese = 12 Then
            GiorniMese = 31
        Else
            If Mese <> 2 Then
                GiorniMese = 30
            Else
                If Day(DateSerial(Anno, Mese, 29)) = 29 Then
                    GiorniMese = 29
                Else
                    GiorniMese = 28
                End If
            End If
        End If
    End Function

    Sub Inizializza_FestivitaSettimanale(ByVal Dipendente As Object, ByVal Data As Object)
        Dim cpt As New Cls_ParametriTurni
        cpt.Leggi(ConnectionString)

        Dim cdv As New Cls_DatiVariabili
        Dim ccc As New Cls_CondizioniContrattuali

        GiornoPrecedenteFestivitaSettimanale = 0
        FestivitaSettimanale = 0
        GiornoSuccessivoFestivitaSettimanale = 0

        If Dipendente = 0 Then Exit Sub

        cdv.Trova(ConnectionString, Dipendente, cpt.CodiceAnagraficoContratto, Data)

        ccc.Trova(ConnectionString, cdv.ContenutoTesto, Data)

        FestivitaSettimanale = ccc.FestivitaSettimanale

        If FestivitaSettimanale = 1 Then
            GiornoPrecedenteFestivitaSettimanale = 7
        Else
            GiornoPrecedenteFestivitaSettimanale = FestivitaSettimanale - 1
        End If
        If FestivitaSettimanale = 7 Then
            GiornoSuccessivoFestivitaSettimanale = 1
        Else
            GiornoSuccessivoFestivitaSettimanale = FestivitaSettimanale + 1
        End If
    End Sub

    Function Giorno_Partime(ByVal Data As Object, ByVal Contratto As Object) As Object
        Dim ccc As New Cls_CondizioniContrattuali

        Giorno_Partime = "N"
        If Not IsDate(Data) Then Exit Function

        ccc.Trova(ConnectionString, Contratto, Data)
        If ccc.Id <> 0 Then
            Select Case Weekday(Data)
                Case 1
                    Giorno_Partime = ccc.PartimeDomenica
                Case 2
                    Giorno_Partime = ccc.PartimeLunedi
                Case 3
                    Giorno_Partime = ccc.PartimeMartedi
                Case 4
                    Giorno_Partime = ccc.PartimeMercoledi
                Case 5
                    Giorno_Partime = ccc.PartimeGiovedi
                Case 6
                    Giorno_Partime = ccc.PartimeVenerdi
                Case 7
                    Giorno_Partime = ccc.PartimeSabato
            End Select
        End If
    End Function

    Public Function TestFormula(ByRef CodiceScript As String) As Long
        Dim Sc As New MSScriptControl.ScriptControl
        Sc.Language = "VBScript"
        Sc.UseSafeSubset = False
        On Error Resume Next
        Sc.Reset()
        Sc.AddCode(CodiceScript)
        If Err.Number <> 0 Then
            TestFormula = Err.Number
        End If
        On Error GoTo 0
    End Function

    Public Function Esegui_FormulaControlloGiustificativi(ByVal Chiave As Object, ByVal Data As Object, ByVal RegolaCalcolo As Object, ByRef Giustificativo As String, ByRef NelGruppo As String, ByRef Familiare As Byte) As Object
        Dim cxst As New Cls_ClsXScriptTurni
        cxst.Utente = Utente
        cxst.ConnectionString = ConnectionString

        Dim Sc As New MSScriptControl.ScriptControl
        Sc.Language = "VBScript"
        Sc.UseSafeSubset = False
        Dim cpt As New Cls_ParametriTurni
        cpt.Leggi(ConnectionString)

        Dim Sw_Vuoto As Boolean

        Dim CCM(12) As String
        Dim Dipendente As Long
        Dim Struttura As String
        Dim MyScript As String
        Dim MyValori As String
        Dim ii As Integer
        Dim aa As Integer
        Dim m As Byte

        Dim InizioMese As Date
        Dim MesePrec As Date
        Dim MeseSucc As Date
        Dim DataTest As Date

        Dim cn As OleDbConnection
        cn = New Data.OleDb.OleDbConnection(ConnectionString)
        cn.Open()

        If IsNumeric(Chiave) Then
            Dipendente = Val(Chiave)
            Struttura = ""
        Else
            Dipendente = 0
            Struttura = Chiave
        End If

        MyScript = ""
        MyValori = ""

        Dim cmd As New OleDbCommand()
        cmd.CommandText = "SELECT * FROM FormuleControlloGiustificativi WHERE Codice = ? ORDER BY Validita DESC, Id"
        cmd.Parameters.AddWithValue("@Codice", RegolaCalcolo)

        cmd.Connection = cn
        Dim myPOSTreader As OleDbDataReader = cmd.ExecuteReader()
        Do While myPOSTreader.Read
            CodiceFormula(ii) = StringaDb(myPOSTreader.Item("Codice"))
            DataFormula(ii) = DataDb(myPOSTreader.Item("Validita"))
            ScriptFormula(ii) = StringaDb(myPOSTreader.Item("CodiceScript"))
            ValoriFormula(ii) = StringaDb(myPOSTreader.Item("ValoriPerExecute"))
            ii = ii + 1
        Loop
        myPOSTreader.Close()
        cmd.Parameters.Clear()

        For Kii = 0 To ii
            If CodiceFormula(Kii) = "" Then Exit For
            If CodiceFormula(Kii) = RegolaCalcolo Then
                If Format(DataFormula(Kii), "yyyyMMdd") <= Format(Data, "yyyyMMdd") Then
                    MyScript = ScriptFormula(Kii)
                    MyValori = ValoriFormula(Kii)
                    Exit For
                End If
            End If
        Next Kii

        If MyScript = "" Then
            Esegui_FormulaControlloGiustificativi = "(E) Codice script vuoto nella formula: " & RegolaCalcolo & vbNewLine
            Exit Function
        End If

        Sc.Reset()
        Sc.AddObject("Form", cxst)
        Sc.AddCode(MyScript)

        If InStr(MyValori, "16,") > 0 Then Sc.ExecuteStatement("Dipendente = " & Dipendente)

        If InStr(MyValori, "43,") > 0 Then Sc.ExecuteStatement("Giustificativo = """ & Giustificativo & """")
        If InStr(MyValori, "44,") > 0 Then Sc.ExecuteStatement("NelGruppo = """ & NelGruppo & """")
        If InStr(MyValori, "45,") > 0 Then Sc.ExecuteStatement("Familiare = """ & Familiare & """")

        If InStr(MyValori, "17,") > 0 Then Sc.ExecuteStatement("Struttura = """ & Struttura & """")
        If InStr(MyValori, "10,") > 0 Then
            For v = 1 To 9
                If IsNumeric(Variabile(v)) And Not (Variabile(v)) = "" Then
                    Sc.ExecuteStatement("Variabile" & v & " = " & cxst.Sostituisci_Da_A(Variabile(v), ",", "."))
                Else
                    If IsDate(Variabile(v)) Then
                        Sc.ExecuteStatement("Variabile" & v & " = #" & Format(Variabile(v), "MM/dd/yyyy") & "#")
                    Else
                        Sc.ExecuteStatement("Variabile" & v & " = """ & Variabile(v) & """")
                    End If
                End If
            Next v
        End If
        If InStr(MyValori, "19,") > 0 Then Sc.ExecuteStatement("Errori = """"")
        If InStr(MyValori, "01,") > 0 Then Sc.ExecuteStatement("Oggi = #" & Format(Data, "MM/dd/yyyy") & "#")
        If InStr(MyValori, "20,") > 0 Then Sc.ExecuteStatement("Giorno = """ & GiornoSettimana(Weekday(Data)) & """")
        If InStr(MyValori, "21,") > 0 Then Sc.ExecuteStatement("GiornoPrecedente = """ & GiornoSettimana(Weekday(DateAdd("d", -1, Data))) & """")
        If InStr(MyValori, "22,") > 0 Then Sc.ExecuteStatement("GiornoSuccessivo = """ & GiornoSettimana(Weekday(DateAdd("d", 1, Data))) & """")
        If InStr(MyValori, "33,") > 0 Then Sc.ExecuteStatement("FestivitaSettimanale = """ & GiornoSettimana(FestivitaSettimanale) & """")
        If InStr(MyValori, "36,") > 0 Then Sc.ExecuteStatement("GiornoPrecedenteFestivitaSettimanale = """ & GiornoSettimana(GiornoPrecedenteFestivitaSettimanale) & """")
        If InStr(MyValori, "37,") > 0 Then Sc.ExecuteStatement("GiornoSuccessivoFestivitaSettimanale = """ & GiornoSettimana(GiornoSuccessivoFestivitaSettimanale) & """")
        If InStr(MyValori, "23,") > 0 Or InStr(MyValori, "24,") > 0 Or InStr(MyValori, "25,") > 0 Then
            Dim CodiceSuperGruppo As String = cxst.Mod_Leggi_DatiVariabili(CodiceDipendente, cpt.CodiceAnagraficoSuperGruppo, "<=", Data)
            If InStr(MyValori, "23,") > 0 Then
                If cxst.GiornoFestivo(DateAdd("d", -1, Data), CodiceSuperGruppo) = True Then
                    Sc.ExecuteStatement("GiornoPrecedenteFestivo = ""S""")
                Else
                    Sc.ExecuteStatement("GiornoPrecedenteFestivo = ""N""")
                End If
            End If
            If InStr(MyValori, "24,") > 0 Then
                If cxst.GiornoFestivo(Data, CodiceSuperGruppo) = True Then
                    Sc.ExecuteStatement("GiornoFestivo = ""S""")
                Else
                    Sc.ExecuteStatement("GiornoFestivo = ""N""")
                End If
            End If
            If InStr(MyValori, "25,") > 0 Then
                If cxst.GiornoFestivo(DateAdd("d", 1, Data), CodiceSuperGruppo) = True Then
                    Sc.ExecuteStatement("GiornoSuccessivoFestivo = ""S""")
                Else
                    Sc.ExecuteStatement("GiornoSuccessivoFestivo = ""N""")
                End If
            End If
        End If
        If InStr(MyValori, "31,") > 0 Or InStr(MyValori, "34,") > 0 Or InStr(MyValori, "35,") > 0 Or InStr(MyValori, "39,") > 0 Or InStr(MyValori, "41,") > 0 Or InStr(MyValori, "42,") > 0 Then
            Sc.ExecuteStatement("Dim PercentualePartimeGiorniDelMese(12)")
            Sc.ExecuteStatement("Dim PercentualePartimeOreDelMese(12)")
            If Dipendente = 0 Then
                Sc.ExecuteStatement("GiornoPartime = ""N""")
                Sc.ExecuteStatement("OrarioContrattuale = 0")
                Sc.ExecuteStatement("PercentualePartimeGiorni = 0")
                Sc.ExecuteStatement("PercentualePartimeOre = 0")
                For m = 1 To 12
                    Sc.ExecuteStatement("PercentualePartimeGiorniDelMese(" & m & ") = 0")
                    Sc.ExecuteStatement("PercentualePartimeOreDelMese(" & m & ") = 0")
                Next m
            Else

                cmd.Cancel()
                cmd.CommandText = "SELECT * FROM DatiVariabili" & _
                                  " WHERE CodiceDipendente = ?" & _
                                  " AND CodiceVariabile = ?" & _
                                  " AND Validita <= ?" & _
                                  " ORDER BY Validita"
                cmd.Parameters.AddWithValue("@CodiceDipendente", Dipendente)
                cmd.Parameters.AddWithValue("@CodiceVariabile", cpt.CodiceAnagraficoContratto)
                cmd.Parameters.AddWithValue("@Validita", Data)

                Sw_Vuoto = True
                cmd.Connection = cn
                myPOSTreader = cmd.ExecuteReader()
                Do While myPOSTreader.Read
                    Sw_Vuoto = False
                    If Year(DataDb(myPOSTreader.Item("Validita"))) < Year(Data) Then
                        For m = 1 To 12
                            CCM(m) = StringaDb(myPOSTreader.Item("ContenutoTesto"))
                        Next m
                    Else
                        m = Month(DataDb(myPOSTreader.Item("Validita")))
                        For m = m To 12
                            CCM(m) = StringaDb(myPOSTreader.Item("ContenutoTesto"))
                        Next m
                    End If
                Loop
                myPOSTreader.Close()
                cmd.Parameters.Clear()
                If Sw_Vuoto = True Then
                    Esegui_FormulaControlloGiustificativi = "(E) Contratto non indicato per il Dipendente " & Dipendente & vbNewLine
                    Exit Function
                End If
                m = Month(Data)

                Sc.ExecuteStatement("GiornoPartime = """ & Giorno_Partime(Data, CCM(m)) & """")
                Sc.ExecuteStatement("OrarioContrattuale = " & cxst.Sostituisci_Da_A(cxst.Calcolo_TempoContrattuale(Data, CCM(m)), ",", "."))
                Sc.ExecuteStatement("PercentualePartimeGiorni = " & cxst.Sostituisci_Da_A(cxst.CampoCondizioniContrattuali(CCM(m), Data, "PercentualePartimeGiorni"), ",", "."))
                Sc.ExecuteStatement("PercentualePartimeOre = " & cxst.Sostituisci_Da_A(cxst.CampoCondizioniContrattuali(CCM(m), Data, "PercentualePartimeOre"), ",", "."))
                aa = Year(Data)
                For m = 1 To 12
                    a = cxst.CampoCondizioniPartimeMese(CCM(m), DateSerial(aa, m, GiorniMese(m, aa)), "PercentualePartimeGiorni")
                    If a <> "" Then
                        Sc.ExecuteStatement("PercentualePartimeGiorniDelMese(" & m & ") = " & cxst.Sostituisci_Da_A(a, ",", "."))
                    End If
                    a = cxst.CampoCondizioniPartimeMese(CCM(m), DateSerial(aa, m, GiorniMese(m, aa)), "PercentualePartimeOre")
                    If a <> "" Then
                        Sc.ExecuteStatement("PercentualePartimeOreDelMese(" & m & ") = " & cxst.Sostituisci_Da_A(a, ",", "."))
                    End If
                Next m
            End If
        End If
        If InStr(MyValori, "26,") > 0 Then Sc.ExecuteStatement("UltimoGiorno = " & GiorniMese(Month(Data), Year(Data)))
        If InStr(MyValori, "27,") > 0 Then Sc.ExecuteStatement("Mese = " & Month(Data))
        If InStr(MyValori, "28,") > 0 Then Sc.ExecuteStatement("ValiditaMese = #" & Format(DateSerial(Year(Data), Month(Data), GiorniMese(Month(Data), Year(Data))), "MM/dd/yyyy") & "#")
        If InStr(MyValori, "38,") > 0 Then
            Validita = DateSerial(Year(Data), Month(Data), GiorniMese(Month(Data), Year(Data)))
            If Dipendente > 0 Then
                DataLicenziamento = cxst.LeggiDataLicenziamento(Dipendente, Validita)
                InizioMese = DateSerial(Year(Data), Month(Data), 1)
                If Format(DataLicenziamento, "yyyyMMdd") >= Format(InizioMese, "yyyyMMdd") And Format(DataLicenziamento, "yyyyMMdd") <= Format(Validita, "yyyyMMdd") Then
                    If Format(DataLicenziamento, "yyyyMMdd") >= Format(Data, "yyyyMMdd") Then Validita = DataLicenziamento
                End If
            End If
            Sc.ExecuteStatement("Validita = #" & Format(Validita, "MM/dd/yyyy") & "#")
        End If
        If InStr(MyValori, "32,") > 0 Then
            MesePrec = DateAdd("m", -1, Data)
            Sc.ExecuteStatement("ValiditaMesePrecedente = #" & Format(DateSerial(Year(MesePrec), Month(MesePrec), GiorniMese(Month(MesePrec), Year(MesePrec))), "MM/dd/yyyy") & "#")
        End If
        If InStr(MyValori, "40,") > 0 Then
            MeseSucc = DateAdd("m", 1, Data)
            Sc.ExecuteStatement("ValiditaMeseSuccessivo = #" & Format(DateSerial(Year(MeseSucc), Month(MeseSucc), GiorniMese(Month(MeseSucc), Year(MeseSucc))), "MM/dd/yyyy") & "#")
        End If
        If InStr(MyValori, "29,") > 0 Then Sc.ExecuteStatement("Anno = " & Year(Data))
        If InStr(MyValori, "30,") > 0 Then Sc.ExecuteStatement("ValiditaAnno = #" & Format(DateSerial(Year(Data), 12, 31), "MM/dd/yyyy") & "#")
        If InStr(MyValori, "06,") > 0 Then Sc.ExecuteStatement("Validita1900 = #" & Format(DateSerial(1900, 1, 1), "MM/dd/yyyy") & "#")
        If Dipendente > 0 Then
            If InStr(MyValori, "04,") > 0 Then
                DataTest = cxst.LeggiDataAssunzione(Dipendente, Data)
                If IsDate(DataTest) = False Then DataTest = "30/12/1899"
                Sc.ExecuteStatement("DataAssunzione = #" & Format(DataTest, "MM/dd/yyyy") & "#")
            End If
            If InStr(MyValori, "05,") > 0 Then
                DataTest = cxst.LeggiDataLicenziamento(Dipendente, Data)
                Sc.ExecuteStatement("DataLicenziamento = #" & Format(DataTest, "MM/dd/yyyy") & "#")
            End If
            If InStr(MyValori, "02,") > 0 Then
                DataTest = cxst.LeggiDataNascita(Dipendente)
                If IsDate(DataTest) = False Then DataTest = "30/12/1899"
                Sc.ExecuteStatement("DataNascita = #" & Format(DataTest, "MM/dd/yyyy") & "#")
            End If
            If InStr(MyValori, "03,") > 0 Then Sc.ExecuteStatement("Sesso = """ & cxst.LeggiSesso(Dipendente) & """")
        End If

        Esegui_FormulaControlloGiustificativi = Sc.Run("Calcolo")

        If InStr(MyValori, "43,") > 0 Then Giustificativo = Sc.Eval("Giustificativo")
        If InStr(MyValori, "44,") > 0 Then NelGruppo = Sc.Eval("NelGruppo")
        If InStr(MyValori, "45,") > 0 Then Familiare = Sc.Eval("Familiare")

        If InStr(MyValori, "10,") > 0 Then
            For v = 1 To 9
                Variabile(v) = Sc.Eval("Variabile" & v)
            Next v
        End If

        If InStr(MyValori, "19,") > 0 Then
            Testo = Sc.Eval("Errori")
            If Testo <> "" Then
                Esegui_FormulaControlloGiustificativi = Testo & vbNewLine
            End If
        End If
        On Error Resume Next
        If Err.Number <> 0 Then
            Esegui_FormulaControlloGiustificativi = Esegui_FormulaControlloGiustificativi & "(E) Nella formula: " & RegolaCalcolo & " errore script: " & Err.Number & vbNewLine
        End If
        On Error GoTo 0
    End Function

    Public Function Esegui_FormulaDatiVariabili(ByVal Chiave As Object, ByVal Data As Date, ByVal RegolaCalcolo As String) As Object
        Dim cxst As New Cls_ClsXScriptTurni
        cxst.Utente = Utente
        cxst.ConnectionString = ConnectionString

        Dim Sc As New MSScriptControl.ScriptControl
        Sc.Language = "VBScript"
        Sc.UseSafeSubset = False

        Dim cpt As New Cls_ParametriTurni
        cpt.Leggi(ConnectionString)

        Dim Sw_Vuoto As Boolean

        Dim CCM(12) As String
        Dim Dipendente As Long
        Dim Struttura As String
        Dim MyScript As String
        Dim MyValori As String
        Dim ii As Integer
        Dim aa As Integer
        Dim m As Byte

        Dim InizioMese As Date
        Dim MesePrec As Date
        Dim MeseSucc As Date
        Dim DataTest As Date

        Dim cn As OleDbConnection
        cn = New Data.OleDb.OleDbConnection(ConnectionString)
        cn.Open()

        If IsNumeric(Chiave) Then
            Dipendente = Val(Chiave)
            Struttura = ""
            Inizializza_FestivitaSettimanale(Dipendente, Data)
        Else
            Dipendente = 0
            Struttura = Chiave
            Inizializza_FestivitaSettimanale(0, Data)
        End If

        MyScript = ""
        MyValori = ""

        Dim cmd As New OleDbCommand()
        cmd.CommandText = "SELECT * FROM FormuleDatiVariabili WHERE Codice = ? ORDER BY Validita DESC, Id"
        cmd.Parameters.AddWithValue("@Codice", RegolaCalcolo)

        cmd.Connection = cn
        Dim myPOSTreader As OleDbDataReader = cmd.ExecuteReader()
        Do While myPOSTreader.Read
            CodiceFormula(ii) = StringaDb(myPOSTreader.Item("Codice"))
            DataFormula(ii) = DataDb(myPOSTreader.Item("Validita"))
            ScriptFormula(ii) = StringaDb(myPOSTreader.Item("CodiceScript"))
            ValoriFormula(ii) = StringaDb(myPOSTreader.Item("ValoriPerExecute"))
            ii = ii + 1
        Loop
        myPOSTreader.Close()
        cmd.Parameters.Clear()

        For Kii = 0 To ii
            If CodiceFormula(Kii) = "" Then Exit For
            If CodiceFormula(Kii) = RegolaCalcolo Then
                If Format(DataFormula(Kii), "yyyyMMdd") <= Format(Data, "yyyyMMdd") Then
                    MyScript = ScriptFormula(Kii)
                    MyValori = ValoriFormula(Kii)
                    Exit For
                End If
            End If
        Next Kii

        If MyScript = "" Then
            Esegui_FormulaDatiVariabili = "(E) Codice script vuoto nella formula: " & RegolaCalcolo & vbNewLine
            Exit Function
        End If

        Sc.Reset()
        Sc.AddObject("Form", cxst)
        Sc.AddCode(MyScript)

        If InStr(MyValori, "16,") > 0 Then Sc.ExecuteStatement("Dipendente = " & Dipendente)

        If InStr(MyValori, "17,") > 0 Then Sc.ExecuteStatement("Struttura = """ & Struttura & """")
        If InStr(MyValori, "10,") > 0 Then
            For v = 1 To 9
                If IsNumeric(Variabile(v)) And Not (Variabile(v)) = "" Then
                    Sc.ExecuteStatement("Variabile" & v & " = " & cxst.Sostituisci_Da_A(Variabile(v), ",", "."))
                Else
                    If IsDate(Variabile(v)) Then
                        Sc.ExecuteStatement("Variabile" & v & " = #" & Format(Variabile(v), "MM/dd/yyyy") & "#")
                    Else
                        Sc.ExecuteStatement("Variabile" & v & " = """ & Variabile(v) & """")
                    End If
                End If
            Next v
        End If
        If InStr(MyValori, "19,") > 0 Then Sc.ExecuteStatement("Errori = """"")
        If InStr(MyValori, "01,") > 0 Then Sc.ExecuteStatement("Oggi = #" & Format(Data, "MM/dd/yyyy") & "#")
        If InStr(MyValori, "20,") > 0 Then Sc.ExecuteStatement("Giorno = """ & GiornoSettimana(Weekday(Data)) & """")
        If InStr(MyValori, "21,") > 0 Then Sc.ExecuteStatement("GiornoPrecedente = """ & GiornoSettimana(Weekday(DateAdd("d", -1, Data))) & """")
        If InStr(MyValori, "22,") > 0 Then Sc.ExecuteStatement("GiornoSuccessivo = """ & GiornoSettimana(Weekday(DateAdd("d", 1, Data))) & """")
        If InStr(MyValori, "33,") > 0 Then Sc.ExecuteStatement("FestivitaSettimanale = """ & GiornoSettimana(FestivitaSettimanale) & """")
        If InStr(MyValori, "36,") > 0 Then Sc.ExecuteStatement("GiornoPrecedenteFestivitaSettimanale = """ & GiornoSettimana(GiornoPrecedenteFestivitaSettimanale) & """")
        If InStr(MyValori, "37,") > 0 Then Sc.ExecuteStatement("GiornoSuccessivoFestivitaSettimanale = """ & GiornoSettimana(GiornoSuccessivoFestivitaSettimanale) & """")
        If InStr(MyValori, "23,") > 0 Or InStr(MyValori, "24,") > 0 Or InStr(MyValori, "25,") > 0 Then
            Dim CodiceSuperGruppo As String = cxst.Mod_Leggi_DatiVariabili(CodiceDipendente, cpt.CodiceAnagraficoSuperGruppo, "<=", Data)
            If InStr(MyValori, "23,") > 0 Then
                If cxst.GiornoFestivo(DateAdd("d", -1, Data), CodiceSuperGruppo) = True Then
                    Sc.ExecuteStatement("GiornoPrecedenteFestivo = ""S""")
                Else
                    Sc.ExecuteStatement("GiornoPrecedenteFestivo = ""N""")
                End If
            End If
            If InStr(MyValori, "24,") > 0 Then
                If cxst.GiornoFestivo(Data, CodiceSuperGruppo) = True Then
                    Sc.ExecuteStatement("GiornoFestivo = ""S""")
                Else
                    Sc.ExecuteStatement("GiornoFestivo = ""N""")
                End If
            End If
            If InStr(MyValori, "25,") > 0 Then
                If cxst.GiornoFestivo(DateAdd("d", 1, Data), CodiceSuperGruppo) = True Then
                    Sc.ExecuteStatement("GiornoSuccessivoFestivo = ""S""")
                Else
                    Sc.ExecuteStatement("GiornoSuccessivoFestivo = ""N""")
                End If
            End If
        End If
        If InStr(MyValori, "31,") > 0 Or InStr(MyValori, "34,") > 0 Or InStr(MyValori, "35,") > 0 Or InStr(MyValori, "39,") > 0 Or InStr(MyValori, "41,") > 0 Or InStr(MyValori, "42,") > 0 Then
            Sc.ExecuteStatement("Dim PercentualePartimeGiorniDelMese(12)")
            Sc.ExecuteStatement("Dim PercentualePartimeOreDelMese(12)")
            If Dipendente = 0 Then
                Sc.ExecuteStatement("GiornoPartime = ""N""")
                Sc.ExecuteStatement("OrarioContrattuale = 0")
                Sc.ExecuteStatement("PercentualePartimeGiorni = 0")
                Sc.ExecuteStatement("PercentualePartimeOre = 0")
                For m = 1 To 12
                    Sc.ExecuteStatement("PercentualePartimeGiorniDelMese(" & m & ") = 0")
                    Sc.ExecuteStatement("PercentualePartimeOreDelMese(" & m & ") = 0")
                Next m
            Else

                cmd.CommandText = "SELECT * FROM DatiVariabili" & _
                                  " WHERE CodiceDipendente = ?" & _
                                  " AND CodiceVariabile = ?" & _
                                  " AND Validita <= ?" & _
                                  " ORDER BY Validita"
                cmd.Parameters.AddWithValue("@CodiceDipendente", Dipendente)
                cmd.Parameters.AddWithValue("@CodiceVariabile", cpt.CodiceAnagraficoContratto)
                cmd.Parameters.AddWithValue("@Validita", Data)

                Sw_Vuoto = True
                cmd.Connection = cn
                myPOSTreader = cmd.ExecuteReader()
                Do While myPOSTreader.Read
                    Sw_Vuoto = False
                    If Year(DataDb(myPOSTreader.Item("Validita"))) < Year(Data) Then
                        For m = 1 To 12
                            CCM(m) = StringaDb(myPOSTreader.Item("ContenutoTesto"))
                        Next m
                    Else
                        m = Month(DataDb(myPOSTreader.Item("Validita")))
                        For m = m To 12
                            CCM(m) = StringaDb(myPOSTreader.Item("ContenutoTesto"))
                        Next m
                    End If
                Loop
                myPOSTreader.Close()
                cmd.Parameters.Clear()
                If Sw_Vuoto = True Then
                    Esegui_FormulaDatiVariabili = "(E) Contratto non indicato per il Dipendente " & Dipendente & vbNewLine
                    Exit Function
                End If
                m = Month(Data)

                Sc.ExecuteStatement("GiornoPartime = """ & Giorno_Partime(Data, CCM(m)) & """")
                Sc.ExecuteStatement("OrarioContrattuale = " & cxst.Sostituisci_Da_A(cxst.Calcolo_TempoContrattuale(Data, CCM(m)), ",", "."))
                Sc.ExecuteStatement("PercentualePartimeGiorni = " & cxst.Sostituisci_Da_A(cxst.CampoCondizioniContrattuali(CCM(m), Data, "PercentualePartimeGiorni"), ",", "."))
                Sc.ExecuteStatement("PercentualePartimeOre = " & cxst.Sostituisci_Da_A(cxst.CampoCondizioniContrattuali(CCM(m), Data, "PercentualePartimeOre"), ",", "."))
                aa = Year(Data)
                For m = 1 To 12
                    a = cxst.CampoCondizioniPartimeMese(CCM(m), DateSerial(aa, m, GiorniMese(m, aa)), "PercentualePartimeGiorni")
                    If a <> "" Then
                        Sc.ExecuteStatement("PercentualePartimeGiorniDelMese(" & m & ") = " & cxst.Sostituisci_Da_A(a, ",", "."))
                    End If
                    a = cxst.CampoCondizioniPartimeMese(CCM(m), DateSerial(aa, m, GiorniMese(m, aa)), "PercentualePartimeOre")
                    If a <> "" Then
                        Sc.ExecuteStatement("PercentualePartimeOreDelMese(" & m & ") = " & cxst.Sostituisci_Da_A(a, ",", "."))
                    End If
                Next m
            End If
        End If
        If InStr(MyValori, "26,") > 0 Then Sc.ExecuteStatement("UltimoGiorno = " & GiorniMese(Month(Data), Year(Data)))
        If InStr(MyValori, "27,") > 0 Then Sc.ExecuteStatement("Mese = " & Month(Data))
        If InStr(MyValori, "28,") > 0 Then Sc.ExecuteStatement("ValiditaMese = #" & Format(DateSerial(Year(Data), Month(Data), GiorniMese(Month(Data), Year(Data))), "MM/dd/yyyy") & "#")
        If InStr(MyValori, "38,") > 0 Then
            Validita = DateSerial(Year(Data), Month(Data), GiorniMese(Month(Data), Year(Data)))
            If Dipendente > 0 Then
                DataLicenziamento = cxst.LeggiDataLicenziamento(Dipendente, Validita)
                InizioMese = DateSerial(Year(Data), Month(Data), 1)
                If Format(DataLicenziamento, "yyyyMMdd") >= Format(InizioMese, "yyyyMMdd") And Format(DataLicenziamento, "yyyyMMdd") <= Format(Validita, "yyyyMMdd") Then
                    If Format(DataLicenziamento, "yyyyMMdd") >= Format(Data, "yyyyMMdd") Then Validita = DataLicenziamento
                End If
            End If
            Sc.ExecuteStatement("Validita = #" & Format(Validita, "MM/dd/yyyy") & "#")
        End If
        If InStr(MyValori, "32,") > 0 Then
            MesePrec = DateAdd("m", -1, Data)
            Sc.ExecuteStatement("ValiditaMesePrecedente = #" & Format(DateSerial(Year(MesePrec), Month(MesePrec), GiorniMese(Month(MesePrec), Year(MesePrec))), "MM/dd/yyyy") & "#")
        End If
        If InStr(MyValori, "40,") > 0 Then
            MeseSucc = DateAdd("m", 1, Data)
            Sc.ExecuteStatement("ValiditaMeseSuccessivo = #" & Format(DateSerial(Year(MeseSucc), Month(MeseSucc), GiorniMese(Month(MeseSucc), Year(MeseSucc))), "MM/dd/yyyy") & "#")
        End If
        If InStr(MyValori, "29,") > 0 Then Sc.ExecuteStatement("Anno = " & Year(Data))
        If InStr(MyValori, "30,") > 0 Then Sc.ExecuteStatement("ValiditaAnno = #" & Format(DateSerial(Year(Data), 12, 31), "MM/dd/yyyy") & "#")
        If InStr(MyValori, "06,") > 0 Then Sc.ExecuteStatement("Validita1900 = #" & Format(DateSerial(1900, 1, 1), "MM/dd/yyyy") & "#")
        If Dipendente > 0 Then
            If InStr(MyValori, "04,") > 0 Then
                DataTest = cxst.LeggiDataAssunzione(Dipendente, Data)
                If IsDate(DataTest) = False Then DataTest = "30/12/1899"
                Sc.ExecuteStatement("DataAssunzione = #" & Format(DataTest, "MM/dd/yyyy") & "#")
            End If
            If InStr(MyValori, "05,") > 0 Then
                DataTest = cxst.LeggiDataLicenziamento(Dipendente, Data)
                Sc.ExecuteStatement("DataLicenziamento = #" & Format(DataTest, "MM/dd/yyyy") & "#")
            End If
            If InStr(MyValori, "02,") > 0 Then
                DataTest = cxst.LeggiDataNascita(Dipendente)
                If IsDate(DataTest) = False Then DataTest = "30/12/1899"
                Sc.ExecuteStatement("DataNascita = #" & Format(DataTest, "MM/dd/yyyy") & "#")
            End If
            If InStr(MyValori, "03,") > 0 Then Sc.ExecuteStatement("Sesso = """ & cxst.LeggiSesso(Dipendente) & """")
        End If

        Esegui_FormulaDatiVariabili = Sc.Run("Calcolo")

        If InStr(MyValori, "10,") > 0 Then
            For v = 1 To 9
                Variabile(v) = Sc.Eval("Variabile" & v)
            Next v
        End If

        If InStr(MyValori, "19,") > 0 Then
            Testo = Sc.Eval("Errori")
            If Testo <> "" Then
                Esegui_FormulaDatiVariabili = Testo & vbNewLine
            End If
        End If
        On Error Resume Next
        If Err.Number <> 0 Then
            Esegui_FormulaDatiVariabili = Esegui_FormulaDatiVariabili & "(E) Nella formula: " & RegolaCalcolo & " errore script: " & Err.Number & vbNewLine
        End If
        On Error GoTo 0
    End Function

    Public Function ElaboraTurniTimbrature(ByVal Dipendente As Long, ByVal Anno As Integer, ByVal Mese As Byte, ByVal Giorno As Byte, ByVal Tipo As String, ByRef Vettore_Giustificativi As Cls_Struttura_Giustificativi, ByRef Vettore_Orari As Cls_Struttura_Orari, ByRef Vettore_Timbrature As Cls_Struttura_Timbrature, ByRef Vettore_Orari_Timbrature As Cls_Struttura_Orari_Timbrature, ByRef Vettore_Calcolo_Orari As Cls_Struttura_Calcolo_Orari) As String

        Dim cxst As New Cls_ClsXScriptTurni
        cxst.ConnectionString = ConnectionString
        cxst.Utente = Utente
        cxst.Vettore_Calcolo_Orari = Vettore_Calcolo_Orari

        Dim Sc As New MSScriptControl.ScriptControl
        Sc.Language = "VBScript"
        Sc.UseSafeSubset = False
        Dim W_Diff As Integer
        Dim MinutiMese As Integer


        Dim Controllo As Boolean = False
        ElaboraTurniTimbrature = ""

        '  If Anno > 2009 Or (Anno = 2009 And Mese > 3) Then Exit Function

        Call Azzera_FestivitaSettimanale()

        Dim cd As New Cls_Dipendenti

        Dim Sw_Stampato As Boolean = False
        CodiceDipendente = Dipendente
        Dim CognomeDipendente As String = cd.CampoDipendenti(ConnectionString, Dipendente, "Cognome")
        Dim NomeDipendente As String = cd.CampoDipendenti(ConnectionString, Dipendente, "Nome")

        Dim g As Byte

        Vettore_Calcolo_Orari.Pulisci()

        W_TempoMese = 0

        For v = 1 To 9
            Variabile(v) = Nothing
        Next v

        Dim DataMin As Date = DateSerial(Anno, Mese, 0)
        If Giorno = 0 Then Giorno = GiorniMese(Mese, Anno)
        Dim DataMax As Date = DateSerial(Anno, Mese, Giorno)
        '
        ' Toglie i dati variabili elaborati nel periodo
        '

        Dim cdv As New Cls_DatiVariabili

        cdv.EliminaDalAl(ConnectionString, Dipendente, DateSerial(Anno, Mese, 1), DateSerial(Anno, Mese, GiorniMese(Mese, Anno)), "A")

        Dim cpt As New Cls_ParametriTurni
        cpt.Leggi(ConnectionString)

        Dim SuperGruppoVariato As Boolean = False
        Dim DataSuperGruppoVariato As Date = DateSerial(1899, 12, 30)
        Dim CodiceSuperGruppo As String = ""
        Dim W_SuperGruppo_Validita As Date = DateSerial(1899, 12, 30)
        cdv.Trova(ConnectionString, Dipendente, cpt.CodiceAnagraficoSuperGruppo, DataMax)
        If cdv.Id <> 0 Then
            CodiceSuperGruppo = cdv.ContenutoTesto
            W_SuperGruppo_Validita = cdv.Validita
        End If

        cdv.Trova(ConnectionString, Dipendente, cpt.CodiceAnagraficoContratto, DataMax)
        If cdv.Id = 0 Then
            ElaboraTurniTimbrature = ElaboraTurniTimbrature & "Elaborazione di " & Dipendente & " " & CognomeDipendente & " " & NomeDipendente & " " & Mese & "/" & Anno & vbNewLine
            ElaboraTurniTimbrature = ElaboraTurniTimbrature & "(E) Contratto non indicato" & vbNewLine
            Exit Function
        End If

        Dim ContrattoVariato As Boolean = False
        Dim DataContrattoVariato As Date = DateSerial(1899, 12, 30)
        Dim CodiceContratto As String = cdv.ContenutoTesto
        Dim W_Contratto_Validita As Date = cdv.Validita


        Dim Sw_Assunto As Boolean = False
        Dim Data_Test As Date = DateAdd("d", -1, DataMin)
        Do Until Sw_Assunto = True
            Data_Test = DateAdd("d", 1, Data_Test)
            If cd.DipendenteAssunto(ConnectionString, Dipendente, Data_Test, Data_Test) = True Then
                Sw_Assunto = True
                If Format(Data_Test, "yyyymmdd") < Format(W_SuperGruppo_Validita, "yyyymmdd") Then
                    SuperGruppoVariato = True
                    DataSuperGruppoVariato = W_SuperGruppo_Validita
                End If
                If Format(Data_Test, "yyyymmdd") < Format(W_Contratto_Validita, "yyyymmdd") Then
                    ContrattoVariato = True
                    DataContrattoVariato = W_Contratto_Validita
                End If
            End If
        Loop

        Dim CodiceSuperGruppoVariato As String = ""
        If SuperGruppoVariato = True Then
            cdv.Trova(ConnectionString, Dipendente, cpt.CodiceAnagraficoSuperGruppo, Data_Test)
            If cdv.Id <> 0 Then
                CodiceSuperGruppoVariato = cdv.ContenutoTesto
            End If
        End If

        Dim CodiceContrattoVariato As String = ""
        If ContrattoVariato = True Then
            cdv.Trova(ConnectionString, Dipendente, cpt.CodiceAnagraficoContratto, Data_Test)
            If cdv.Id = 0 Then
                ElaboraTurniTimbrature = ElaboraTurniTimbrature & "Elaborazione di " & Dipendente & " " & CognomeDipendente & " " & NomeDipendente & " " & Mese & "/" & Anno & vbNewLine
                ElaboraTurniTimbrature = ElaboraTurniTimbrature & "(E) Contratto non indicato" & vbNewLine
                Exit Function
            End If

            CodiceContrattoVariato = cdv.ContenutoTesto
        End If

        Dim ccc As New Cls_CondizioniContrattuali

        ccc.Trova(ConnectionString, CodiceContratto, DataMax)

        If ccc.Id = 0 Then
            ElaboraTurniTimbrature = ElaboraTurniTimbrature & "Elaborazione di " & Dipendente & " " & CognomeDipendente & " " & NomeDipendente & " " & Mese & "/" & Anno & vbNewLine
            ElaboraTurniTimbrature = ElaboraTurniTimbrature & "(E) Contratto: " & CodiceContratto & "  inesistente" & vbNewLine
            Exit Function
        End If
        Diurno_Dalle = Format(ccc.OrarioDiurnoDalle, "HH.mm")
        Diurno_Alle = Format(ccc.OrarioDiurnoAlle, "HH.mm")

        Dim ControlloOrario As String
        Dim TimbratureProfili As String
        Dim Calcolo_Presenze As String

        If Tipo = "Timbrature" Or Tipo = "ControlloTimbrature" Then
            ControlloOrario = ccc.ControlloOrario
            TimbratureProfili = ccc.TimbratureProfili
        Else
            ControlloOrario = "N"
            TimbratureProfili = "S"
        End If
        If Tipo = "ControlloTimbrature" Then
            Calcolo_Presenze = "N"
        Else
            Calcolo_Presenze = ccc.CalcoloPresenzeMaggiorazioni
        End If
        FestivitaSettimanale = ccc.FestivitaSettimanale
        If FestivitaSettimanale = 1 Then
            GiornoPrecedenteFestivitaSettimanale = 7
        Else
            GiornoPrecedenteFestivitaSettimanale = FestivitaSettimanale - 1
        End If
        If FestivitaSettimanale = 7 Then
            GiornoSuccessivoFestivitaSettimanale = 1
        Else
            GiornoSuccessivoFestivitaSettimanale = FestivitaSettimanale + 1
        End If

        If ccc.OrarioDomenica <> New Date(1899, 12, 30, 0, 0, 0) Then W_OrarioDomenica = ccc.OrarioDomenica Else W_OrarioDomenica = DateSerial(1899, 12, 30)
        If ccc.OrarioLunedi <> New Date(1899, 12, 30, 0, 0, 0) Then W_OrarioLunedi = ccc.OrarioLunedi Else W_OrarioLunedi = DateSerial(1899, 12, 30)
        If ccc.OrarioMartedi <> New Date(1899, 12, 30, 0, 0, 0) Then W_OrarioMartedi = ccc.OrarioMartedi Else W_OrarioMartedi = DateSerial(1899, 12, 30)
        If ccc.OrarioMercoledi <> New Date(1899, 12, 30, 0, 0, 0) Then W_OrarioMercoledi = ccc.OrarioMercoledi Else W_OrarioMercoledi = DateSerial(1899, 12, 30)
        If ccc.OrarioGiovedi <> New Date(1899, 12, 30, 0, 0, 0) Then W_OrarioGiovedi = ccc.OrarioGiovedi Else W_OrarioGiovedi = DateSerial(1899, 12, 30)
        If ccc.OrarioVenerdi <> New Date(1899, 12, 30, 0, 0, 0) Then W_OrarioVenerdi = ccc.OrarioVenerdi Else W_OrarioVenerdi = DateSerial(1899, 12, 30)
        If ccc.OrarioSabato <> New Date(1899, 12, 30, 0, 0, 0) Then W_OrarioSabato = ccc.OrarioSabato Else W_OrarioSabato = DateSerial(1899, 12, 30)
        W_PercentualePartimeGiorni = ccc.PercentualePartimeGiorni
        W_PercentualePartimeGiorniDelMese = ccc.PercentualePartimeGiorniMese
        W_PercentualePartimeOre = ccc.PercentualePartimeOre
        W_PercentualePartimeOreDelMese = ccc.PercentualePartimeOreMese
        W_TipoPartime = ccc.TipoPartime

        Dim ccpm As New Cls_CondizioniPartimeMese

        ccpm.Leggi(ConnectionString, CodiceContratto, ccc.Validita, Mese)
        If ccpm.Id > 0 Then
            If ccpm.OrarioLunedi <> New Date(1899, 12, 30, 0, 0, 0) Then W_OrarioLunedi = ccc.OrarioLunedi Else W_OrarioLunedi = DateSerial(1899, 12, 30)
            If ccc.OrarioMartedi <> New Date(1899, 12, 30, 0, 0, 0) Then W_OrarioMartedi = ccc.OrarioMartedi Else W_OrarioMartedi = DateSerial(1899, 12, 30)
            If ccc.OrarioMercoledi <> New Date(1899, 12, 30, 0, 0, 0) Then W_OrarioMercoledi = ccc.OrarioMercoledi Else W_OrarioMercoledi = DateSerial(1899, 12, 30)
            If ccc.OrarioGiovedi <> New Date(1899, 12, 30, 0, 0, 0) Then W_OrarioGiovedi = ccc.OrarioGiovedi Else W_OrarioGiovedi = DateSerial(1899, 12, 30)
            If ccc.OrarioVenerdi <> New Date(1899, 12, 30, 0, 0, 0) Then W_OrarioVenerdi = ccc.OrarioVenerdi Else W_OrarioVenerdi = DateSerial(1899, 12, 30)
            If ccc.OrarioSabato <> New Date(1899, 12, 30, 0, 0, 0) Then W_OrarioSabato = ccc.OrarioSabato Else W_OrarioSabato = DateSerial(1899, 12, 30)
            If ccc.OrarioDomenica <> New Date(1899, 12, 30, 0, 0, 0) Then W_OrarioDomenica = ccc.OrarioDomenica Else W_OrarioDomenica = DateSerial(1899, 12, 30)
            W_PercentualePartimeGiorniDelMese = ccc.PercentualePartimeGiorni
            W_PercentualePartimeOreDelMese = ccc.PercentualePartimeOre
        End If

        Dim ceo As New Cls_ElaboraOrari

        W_OrarioContrattuale = cxst.Calcolo_TempoContrattuale(DataMax, CodiceContratto)

        If Tipo <> "Proposta" Then
            Risultato = ceo.SviluppoTurniOrdiniServizio(ConnectionString, Dipendente, DataMin, "N", Vettore_Orari, Vettore_Timbrature, "N", "S", "N", "S", "S", DataMin)
        End If
        If Trim(Risultato) <> "" Then
            If Sw_Stampato = False Then
                Sw_Stampato = True
                ElaboraTurniTimbrature = ElaboraTurniTimbrature & "Elaborazione di " & Dipendente & " " & CognomeDipendente & " " & NomeDipendente & " " & Mese & "/" & Anno & vbNewLine
            End If
            ElaboraTurniTimbrature = ElaboraTurniTimbrature & Risultato
            If InStr(Risultato, "(E)") <> 0 Then Exit Function
        End If

        If ControlloOrario <> "N" Then
            Risultato = CreaOrariTimbrature(TimbratureProfili, Dipendente, Anno, Mese, Giorno, CodiceSuperGruppo, Vettore_Orari, Vettore_Timbrature, Vettore_Orari_Timbrature, ControlloOrario, "N")
            If Trim(Risultato) <> "" Then
                If Sw_Stampato = False Then
                    Sw_Stampato = True
                    ElaboraTurniTimbrature = ElaboraTurniTimbrature & "Elaborazione di " & Dipendente & " " & CognomeDipendente & " " & NomeDipendente & " " & Mese & "/" & Anno & vbNewLine
                End If
                ElaboraTurniTimbrature = ElaboraTurniTimbrature & Risultato
                If InStr(Risultato, "(E)") <> 0 Then
                    Exit Function
                End If
            End If
        Else
            Risultato = CreaDifferenzeTimbrature(TimbratureProfili, Dipendente, DataMin, Vettore_Orari, Vettore_Timbrature, Vettore_Orari_Timbrature, Giorno, "N")
            If Trim(Risultato) <> "" Then
                If Sw_Stampato = False Then
                    Sw_Stampato = True
                    ElaboraTurniTimbrature = ElaboraTurniTimbrature & "Elaborazione di " & Dipendente & " " & CognomeDipendente & " " & NomeDipendente & " " & Mese & "/" & Anno & vbNewLine
                End If
                ElaboraTurniTimbrature = ElaboraTurniTimbrature & Risultato
                If InStr(Risultato, "(E)") <> 0 Then
                    Exit Function
                End If
            End If
        End If

        OrdinaVettoreOrariTimbrature(Vettore_Orari_Timbrature, Giorno)

        Controllo_CausaliSuVettoreOrariTimbrature(Vettore_Orari_Timbrature, Giorno)

        Risultato = ElaboraCausaliTimbrature(Dipendente, Anno, Mese, Giorno, CodiceSuperGruppo, CodiceContratto, Vettore_Orari_Timbrature, "N")
        If Trim(Risultato) <> "" Then
            If Sw_Stampato = False Then
                Sw_Stampato = True
                ElaboraTurniTimbrature = ElaboraTurniTimbrature & "Elaborazione di " & Dipendente & " " & CognomeDipendente & " " & NomeDipendente & " " & Mese & "/" & Anno & vbNewLine
            End If
            ElaboraTurniTimbrature = ElaboraTurniTimbrature & Risultato
            If InStr(Risultato, "(E)") <> 0 Then
                Exit Function
            End If
        End If

        If Calcolo_Presenze = "N" Then
            Vettore_Calcolo_Orari = cxst.Vettore_Calcolo_Orari
            Exit Function
        End If

        Dim Sw_TempoStandard As Boolean = False
        W_TempoStandard = 0

        Dim cn As OleDbConnection
        cn = New Data.OleDb.OleDbConnection(ConnectionString)
        cn.Open()

        Dim cmd As New OleDbCommand()
        cmd.CommandText = " SELECT * FROM Matricola" & _
                          " WHERE CodiceDipendente = ?" & _
                          " AND DataAssunzione <= ?" & _
                          " ORDER BY DataAssunzione"
        cmd.Parameters.AddWithValue("@CodiceDipendente", Dipendente)
        cmd.Parameters.AddWithValue("@DataAssunzione", DataMax)

        cmd.Connection = cn
        Dim myPOSTreader As OleDbDataReader = cmd.ExecuteReader()
        Do While myPOSTreader.Read
            If Format(DataDb(myPOSTreader.Item("DataAssunzione")), "yyyyMMdd") <= Format(DateSerial(Anno, Mese, 1), "yyyyMMdd") Then
                If Not IsDate(DataDb(myPOSTreader.Item("DataLicenziamento"))) Then Exit Do
                If Format(DataDb(myPOSTreader.Item("DataLicenziamento")), "yyyyMMdd") >= Format(DateSerial(Anno, Mese, 1), "yyyyMMdd") Then
                    If Format(DataDb(myPOSTreader.Item("DataLicenziamento")), "yyyyMMdd") >= Format(DataMax, "yyyyMMdd") Then Exit Do
                    Sw_TempoStandard = True
                    W_TempoStandard = cxst.Mod_OperaConMinuti(W_TempoStandard, "+", cxst.Calcolo_TempoStandard(DateSerial(Anno, Mese, 1), DataDb(myPOSTreader.Item("DataLicenziamento")), CodiceSuperGruppo, CodiceContratto))
                End If
            Else
                Sw_TempoStandard = True
                If Not IsDate(DataDb(myPOSTreader.Item("DataLicenziamento"))) Then
                    W_TempoStandard = cxst.Mod_OperaConMinuti(W_TempoStandard, "+", cxst.Calcolo_TempoStandard(DataDb(myPOSTreader.Item("DataAssunzione")), DataMax, CodiceSuperGruppo, CodiceContratto))
                Else
                    If Format(DataDb(myPOSTreader.Item("DataLicenziamento")), "yyyyMMdd") >= Format(DataMax, "yyyyMMdd") Then
                        W_TempoStandard = cxst.Mod_OperaConMinuti(W_TempoStandard, "+", cxst.Calcolo_TempoStandard(DataDb(myPOSTreader.Item("DataAssunzione")), DataMax, CodiceSuperGruppo, CodiceContratto))
                    Else
                        W_TempoStandard = cxst.Mod_OperaConMinuti(W_TempoStandard, "+", cxst.Calcolo_TempoStandard(DataDb(myPOSTreader.Item("DataAssunzione")), DataDb(myPOSTreader.Item("DataLicenziamento")), CodiceSuperGruppo, CodiceContratto))
                    End If
                End If
            End If
        Loop
        myPOSTreader.Close()
        cmd.Parameters.Clear()

        Dim com As New Cls_OrarioMensile
        Dim comp As New Cls_OrarioMensilePatrono

        If Sw_TempoStandard = False Then
            If SuperGruppoVariato = True And Day(DataSuperGruppoVariato) > 1 Then
                W_TempoStandard = cxst.Calcolo_TempoStandard(DateSerial(Anno, Mese, 1), DateAdd("d", -1, DataSuperGruppoVariato), CodiceSuperGruppoVariato, CodiceContratto)
                W_TempoStandard = cxst.Mod_OperaConMinuti(W_TempoStandard, "+", cxst.Calcolo_TempoStandard(DataSuperGruppoVariato, DataMax, CodiceSuperGruppo, CodiceContratto))
            Else
                If ContrattoVariato = True And Day(DataContrattoVariato) > 1 Then
                    W_TempoStandard = cxst.Calcolo_TempoStandard(DateSerial(Anno, Mese, 1), DateAdd("d", -1, DataContrattoVariato), CodiceSuperGruppo, CodiceContrattoVariato)
                    W_TempoStandard = cxst.Mod_OperaConMinuti(W_TempoStandard, "+", cxst.Calcolo_TempoStandard(DataContrattoVariato, DataMax, CodiceSuperGruppo, CodiceContratto))
                Else
                    If Giorno <> GiorniMese(Mese, Anno) Then
                        W_TempoStandard = cxst.Calcolo_TempoStandard(DateSerial(Anno, Mese, 1), DataMax, CodiceSuperGruppo, CodiceContratto)
                    Else
                        W_TempoStandard = 0
                        If CodiceSuperGruppo <> "" Then
                            comp.Leggi(ConnectionString, Anno, CodiceContratto, CodiceSuperGruppo)
                            If comp.Id <> 0 Then
                                If Mese = comp.Mese Then
                                    W_TempoStandard = comp.TempoLavoro
                                End If
                                If Mese = comp.MeseStandard Then
                                    W_TempoStandard = comp.TempoLavoroStandard
                                End If
                            End If
                        End If
                    End If
                    If W_TempoStandard = 0 Then
                        W_TempoStandard = com.TempoLavoroMese(ConnectionString, Anno, CodiceContratto, Mese)
                        If W_TempoStandard = 0 Then
                            If Sw_Stampato = False Then
                                Sw_Stampato = True
                                ElaboraTurniTimbrature = ElaboraTurniTimbrature & "Elaborazione di " & Dipendente & " " & CognomeDipendente & " " & NomeDipendente & " " & Mese & "/" & Anno & vbNewLine
                            End If
                            ElaboraTurniTimbrature = ElaboraTurniTimbrature & "(E) Orario Mensile per il Contratto: " & CodiceContratto & "  inesistente" & vbNewLine
                            Exit Function
                        End If
                    End If
                End If
            End If
        End If

        If cpt.FormulaCalcoloInizioMese <> "" Then
            Risultato = Calcolo_Mensile(cpt.FormulaCalcoloInizioMese, DataMax, CodiceSuperGruppo, CodiceContratto, Vettore_Orari_Timbrature)
            If Trim(Risultato) <> "" Then
                If Sw_Stampato = False Then
                    Sw_Stampato = True
                    ElaboraTurniTimbrature = ElaboraTurniTimbrature & "Elaborazione di " & Dipendente & " " & CognomeDipendente & " " & NomeDipendente & " " & Mese & "/" & Anno & vbNewLine
                End If
                If InStr(Risultato, "(E)") <> 0 Then
                    ElaboraTurniTimbrature = ElaboraTurniTimbrature & "(E) Calcolo Inizio Mense" & vbNewLine
                    ElaboraTurniTimbrature = ElaboraTurniTimbrature & Risultato
                    Exit Function
                Else
                    ElaboraTurniTimbrature = ElaboraTurniTimbrature & Risultato
                End If
            End If
        End If

        Dim Mydate As Date = DateSerial(Anno, Mese, 1)
        Dim d As Byte

        Do While Mese = Month(Mydate)
            If Giorno < Day(Mydate) Then Exit Do
            g = Day(Mydate)

            d = Weekday(Mydate)
            Select Case d
                Case 1
                    W_OrarioDelGiorno = W_OrarioDomenica
                Case 2
                    W_OrarioDelGiorno = W_OrarioLunedi
                Case 3
                    W_OrarioDelGiorno = W_OrarioMartedi
                Case 4
                    W_OrarioDelGiorno = W_OrarioMercoledi
                Case 5
                    W_OrarioDelGiorno = W_OrarioGiovedi
                Case 6
                    W_OrarioDelGiorno = W_OrarioVenerdi
                Case 7
                    W_OrarioDelGiorno = W_OrarioSabato
            End Select

            If cpt.FormulaCalcoloInizioGiorno <> "" Then
                Risultato = Calcolo_Riepiloghi(cpt.FormulaCalcoloInizioGiorno, Mydate, CodiceSuperGruppo, CodiceContratto, cxst.Vettore_Calcolo_Orari)
                If Trim(Risultato) <> "" Then
                    If Sw_Stampato = False Then
                        Sw_Stampato = True
                        ElaboraTurniTimbrature = ElaboraTurniTimbrature & "Elaborazione di " & Dipendente & " " & CognomeDipendente & " " & NomeDipendente & " " & Mese & "/" & Anno & vbNewLine
                    End If
                    If InStr(Risultato, "(E)") <> 0 Then
                        ElaboraTurniTimbrature = ElaboraTurniTimbrature & "(E) Calcolo Inizio Giorno, il giorno : " & Day(Mydate) & vbNewLine
                        ElaboraTurniTimbrature = ElaboraTurniTimbrature & Risultato
                        Exit Function
                    Else
                        ElaboraTurniTimbrature = ElaboraTurniTimbrature & Risultato
                    End If
                End If
            End If

            Work_1_NelGiorno = "00.00"
            Work_2_NelGiorno = "00.00"
            Pausa_NelGiorno = "00.00"
            Lavorate_NelGiorno = "00.00"
            Lavorate_Diurne = "00.00"
            Lavorate_Notturne = "00.00"
            Lavorate_FestiveDiurne = "00.00"
            Lavorate_FestiveNotturne = "00.00"
            Lavorate_Retribuite = "00.00"
            Dim i As Byte = 0
            Do Until i > 14 Or (Vettore_Orari_Timbrature.Dalle(i, g) = "" And Vettore_Orari_Timbrature.Alle(i, g) = "" And Vettore_Orari_Timbrature.Pausa(i, g) = "")
                If Vettore_Orari_Timbrature.Giustificativo(i, g) = "" Then
                    Vettore_Orari_Timbrature.Giustificativo(i, g) = cpt.GiustificativoOrarioLavoro
                End If
                '
                '   Variazione effettuata per non sommare in modo incontrollabile attraverso le formule il tempo di pausa (Tolta per l'inserimento di cpt.TrattaOrarioPausaAutomatico)
                '
                '      If Vettore_Orari_Timbrature.Tipo(i, g) = "" And Vettore_Orari_Timbrature.Pausa(i, g) <> "00.00" Then
                '        If Vettore_Orari_Timbrature.Pausa(i, g) = "__.__" Then
                '           Vettore_Orari_Timbrature.Pausa(i, g) = 0
                '        End If
                '        W_Pausa = DateDiff("n", "00.00", Vettore_Orari_Timbrature.Pausa(i, g))
                '        If Vettore_Orari_Timbrature.Alle(i, g) = "24.00" Then
                '          W_Diff = DateDiff("n", Vettore_Orari_Timbrature.Dalle(i, g), "23.59")
                '          W_Diff = W_Diff + 1
                '          Else
                '          W_Diff = DateDiff("n", Vettore_Orari_Timbrature.Dalle(i, g), Vettore_Orari_Timbrature.Alle(i, g))
                '        End If
                '        If W_Diff > W_Pausa Then
                '          Vettore_Orari_Timbrature.Dalle(i, g) = Format(DateAdd("n", W_Pausa, Vettore_Orari_Timbrature.Dalle(i, g)), "HH.mm")
                '          Else
                '          Vettore_Orari_Timbrature.Dalle(i, g) = Vettore_Orari_Timbrature.Alle(i, g)
                '        End If
                '      End If
                '
                ' Inizio Prova di Mauro
                '
                Risultato = Calcolo_Maggiorazioni(Dipendente, i, Mydate, CodiceSuperGruppo, CodiceContratto, ControlloOrario, Vettore_Orari_Timbrature, cxst.Vettore_Calcolo_Orari)
                If Trim(Risultato) <> "" Then
                    If Sw_Stampato = False Then
                        Sw_Stampato = True
                        ElaboraTurniTimbrature = ElaboraTurniTimbrature & "Elaborazione di " & Dipendente & " " & CognomeDipendente & " " & NomeDipendente & " " & Mese & "/" & Anno & vbNewLine
                    End If
                    ElaboraTurniTimbrature = ElaboraTurniTimbrature & Risultato
                    If InStr(Risultato, "(E)") <> 0 Then
                        Exit Function
                    End If
                End If

                If Ore_Intervallo = "24.00" Then
                    W_Diff = 1440
                Else
                    W_Diff = DateDiff("n", TimeSerial(0, 0, 0), TimeSerial(Val(Mid(Ore_Intervallo, 1, 2)), Val(Mid(Ore_Intervallo, 4, 2)), 0))
                End If

                Dim ii As Byte = 0
                Do Until ii > 50 Or cxst.Vettore_Calcolo_Orari.Elemento(ii, 0) = Vettore_Orari_Timbrature.Giustificativo(i, g) & "/" & Vettore_Orari_Timbrature.Familiare(i, g) Or cxst.Vettore_Calcolo_Orari.Elemento(ii, 0) = ""
                    ii = ii + 1
                Loop
                cxst.Vettore_Calcolo_Orari.Elemento(ii, 0) = Vettore_Orari_Timbrature.Giustificativo(i, g) & "/" & Vettore_Orari_Timbrature.Familiare(i, g)
                If cxst.Vettore_Calcolo_Orari.Elemento(ii, Day(Mydate)) = "00.00" Then
                    cxst.Vettore_Calcolo_Orari.Elemento(ii, Day(Mydate)) = 0.0#
                End If
                If cxst.Vettore_Calcolo_Orari.Elemento(ii, Day(Mydate)) = "" Then
                    cxst.Vettore_Calcolo_Orari.Elemento(ii, Day(Mydate)) = cxst.Sostituisci(Format(cxst.Mod_OperaConMinuti("00.00", "+", W_Diff, "n"), "00.00"), ",", ".")
                Else
                    cxst.Vettore_Calcolo_Orari.Elemento(ii, Day(Mydate)) = cxst.Sostituisci(Format(cxst.Mod_OperaConMinuti(cxst.Vettore_Calcolo_Orari.Elemento(ii, Day(Mydate)), "+", W_Diff, "n"), "00.00"), ",", ".")
                End If
                '
                ' Fine Prova di Mauro
                '
                i = i + 1
            Loop
            For i = 0 To 2
                If Vettore_Giustificativi.Giustificativo(i, Day(Mydate)) <> "" Then
                    For ii = 0 To 40
                        If cxst.Vettore_Calcolo_Orari.Elemento(ii, 0) = "" Or cxst.Vettore_Calcolo_Orari.Elemento(ii, 0) = Vettore_Giustificativi.Giustificativo(i, Day(Mydate)) & "/0" Then
                            cxst.Vettore_Calcolo_Orari.Elemento(ii, 0) = Vettore_Giustificativi.Giustificativo(i, Day(Mydate)) & "/0"
                            cxst.Vettore_Calcolo_Orari.Elemento(ii, Day(Mydate)) = Vettore_Giustificativi.Orario(i, Day(Mydate))
                            '
                            '   Tratta il giustificativo del giorno nella fase "Orari" solo come intervallo di tempo e non qualificato come diurno / notturno o festivo
                            '
                            Ore_Intervallo = Vettore_Giustificativi.Orario(i, Day(Mydate))
                            Risultato = Calcolo_Orari(cxst.Vettore_Calcolo_Orari.Elemento(ii, 0), "", "", "", Mydate, ii, CodiceSuperGruppo, CodiceContratto, cxst.Vettore_Calcolo_Orari)
                            If Trim(Risultato) <> "" Then
                                If Sw_Stampato = False Then
                                    Sw_Stampato = True
                                    ElaboraTurniTimbrature = ElaboraTurniTimbrature & "Elaborazione di " & Dipendente & " " & CognomeDipendente & " " & NomeDipendente & " " & Mese & "/" & Anno & vbNewLine
                                End If
                                If InStr(Risultato, "(E)") <> 0 Then
                                    ElaboraTurniTimbrature = ElaboraTurniTimbrature & "(E) Calcolo Orari il giorno : " & Day(Mydate) & " cxst.Vettore_Calcolo_Orari.Elemento: " & ii & vbNewLine
                                    ElaboraTurniTimbrature = ElaboraTurniTimbrature & Risultato
                                    Exit Function
                                Else
                                    ElaboraTurniTimbrature = ElaboraTurniTimbrature & Risultato
                                End If
                            End If
                            '
                            Exit For
                        End If
                    Next ii
                End If
            Next i
            Fase = "Giustificativi"
            ' + Mauro
            Ore_Pausa = "00.00"
            Ore_Intervallo = "00.00"
            Ore_Diurne = "00.00"
            Ore_Notturne = "00.00"
            Ore_FestiveDiurne = "00.00"
            Ore_FestiveNotturne = "00.00"
            ' -
            cxst.Vettore_Calcolo_Orari.Elemento(41, Day(Mydate)) = cxst.Sostituisci(Format(Work_1_NelGiorno, "00.00"), ",", ".")
            cxst.Vettore_Calcolo_Orari.Elemento(42, Day(Mydate)) = cxst.Sostituisci(Format(Work_2_NelGiorno, "00.00"), ",", ".")
            cxst.Vettore_Calcolo_Orari.Elemento(43, Day(Mydate)) = PrevisteNelGiorno(Mydate, Vettore_Orari)
            cxst.Vettore_Calcolo_Orari.Elemento(44, Day(Mydate)) = cxst.Sostituisci(Pausa_NelGiorno, ",", ".")
            cxst.Vettore_Calcolo_Orari.Elemento(45, Day(Mydate)) = cxst.Sostituisci(Lavorate_NelGiorno, ",", ".")
            cxst.Vettore_Calcolo_Orari.Elemento(46, Day(Mydate)) = cxst.Sostituisci(Lavorate_Diurne, ",", ".")
            cxst.Vettore_Calcolo_Orari.Elemento(47, Day(Mydate)) = cxst.Sostituisci(Lavorate_Notturne, ",", ".")
            cxst.Vettore_Calcolo_Orari.Elemento(48, Day(Mydate)) = cxst.Sostituisci(Lavorate_FestiveDiurne, ",", ".")
            cxst.Vettore_Calcolo_Orari.Elemento(49, Day(Mydate)) = cxst.Sostituisci(Lavorate_FestiveNotturne, ",", ".")
            cxst.Vettore_Calcolo_Orari.Elemento(50, Day(Mydate)) = cxst.Sostituisci(Lavorate_Retribuite, ",", ".")
            For i = 0 To 40
                If cxst.Vettore_Calcolo_Orari.Elemento(i, Day(Mydate)) <> "" Then
                    Risultato = Calcolo_Orari(cxst.Vettore_Calcolo_Orari.Elemento(i, 0), "", "", "", Mydate, i, CodiceSuperGruppo, CodiceContratto, cxst.Vettore_Calcolo_Orari)
                    If Trim(Risultato) <> "" Then
                        If Sw_Stampato = False Then
                            Sw_Stampato = True
                            ElaboraTurniTimbrature = ElaboraTurniTimbrature & "Elaborazione di " & Dipendente & " " & CognomeDipendente & " " & NomeDipendente & " " & Mese & "/" & Anno & vbNewLine
                        End If
                        If InStr(Risultato, "(E)") <> 0 Then
                            ElaboraTurniTimbrature = ElaboraTurniTimbrature & "(E) Calcolo Orari il giorno : " & Day(Mydate) & " cxst.Vettore_Calcolo_Orari.Elemento: " & i & vbNewLine
                            ElaboraTurniTimbrature = ElaboraTurniTimbrature & Risultato
                            Exit Function
                        Else
                            ElaboraTurniTimbrature = ElaboraTurniTimbrature & Risultato
                        End If
                    End If
                End If
            Next i
            If cxst.Vettore_Calcolo_Orari.Elemento(46, Day(Mydate)) <> "00.00" And cpt.FormulaCalcoloDiurno <> "" Then
                Risultato = Calcolo_Riepiloghi(cpt.FormulaCalcoloDiurno, Mydate, CodiceSuperGruppo, CodiceContratto, cxst.Vettore_Calcolo_Orari)
                If Trim(Risultato) <> "" Then
                    If Sw_Stampato = False Then
                        Sw_Stampato = True
                        ElaboraTurniTimbrature = ElaboraTurniTimbrature & "Elaborazione di " & Dipendente & " " & CognomeDipendente & " " & NomeDipendente & " " & Mese & "/" & Anno & vbNewLine
                    End If
                    If InStr(Risultato, "(E)") <> 0 Then
                        ElaboraTurniTimbrature = ElaboraTurniTimbrature & "(E) Calcolo Diurno il giorno : " & Day(Mydate) & vbNewLine
                        ElaboraTurniTimbrature = ElaboraTurniTimbrature & Risultato
                        Exit Function
                    Else
                        ElaboraTurniTimbrature = ElaboraTurniTimbrature & Risultato
                    End If
                End If
            End If
            If cxst.Vettore_Calcolo_Orari.Elemento(47, Day(Mydate)) <> "00.00" And cpt.FormulaCalcoloNotturno <> "" Then
                Risultato = Calcolo_Riepiloghi(cpt.FormulaCalcoloNotturno, Mydate, CodiceSuperGruppo, CodiceContratto, cxst.Vettore_Calcolo_Orari)
                If Trim(Risultato) <> "" Then
                    If Sw_Stampato = False Then
                        Sw_Stampato = True
                        ElaboraTurniTimbrature = ElaboraTurniTimbrature & "Elaborazione di " & Dipendente & " " & CognomeDipendente & " " & NomeDipendente & " " & Mese & "/" & Anno & vbNewLine
                    End If
                    If InStr(Risultato, "(E)") <> 0 Then
                        ElaboraTurniTimbrature = ElaboraTurniTimbrature & "(E) Calcolo Notturno il giorno : " & Day(Mydate) & vbNewLine
                        ElaboraTurniTimbrature = ElaboraTurniTimbrature & Risultato
                        Exit Function
                    Else
                        ElaboraTurniTimbrature = ElaboraTurniTimbrature & Risultato
                    End If
                End If
            End If
            If cxst.Vettore_Calcolo_Orari.Elemento(48, Day(Mydate)) <> "00.00" And cpt.FormulaCalcoloFestivoDiurno <> "" Then
                Risultato = Calcolo_Riepiloghi(cpt.FormulaCalcoloFestivoDiurno, Mydate, CodiceSuperGruppo, CodiceContratto, cxst.Vettore_Calcolo_Orari)
                If Trim(Risultato) <> "" Then
                    If Sw_Stampato = False Then
                        Sw_Stampato = True
                        ElaboraTurniTimbrature = ElaboraTurniTimbrature & "Elaborazione di " & Dipendente & " " & CognomeDipendente & " " & NomeDipendente & " " & Mese & "/" & Anno & vbNewLine
                    End If
                    If InStr(Risultato, "(E)") <> 0 Then
                        ElaboraTurniTimbrature = ElaboraTurniTimbrature & "(E) Calcolo Festivo Diurno il giorno : " & Day(Mydate) & vbNewLine
                        ElaboraTurniTimbrature = ElaboraTurniTimbrature & Risultato
                        Exit Function
                    Else
                        ElaboraTurniTimbrature = ElaboraTurniTimbrature & Risultato
                    End If
                End If
            End If
            If cxst.Vettore_Calcolo_Orari.Elemento(49, Day(Mydate)) <> "00.00" And cpt.FormulaCalcoloFestivoNotturno <> "" Then
                Risultato = Calcolo_Riepiloghi(cpt.FormulaCalcoloFestivoNotturno, Mydate, CodiceSuperGruppo, CodiceContratto, cxst.Vettore_Calcolo_Orari)
                If Trim(Risultato) <> "" Then
                    If Sw_Stampato = False Then
                        Sw_Stampato = True
                        ElaboraTurniTimbrature = ElaboraTurniTimbrature & "Elaborazione di " & Dipendente & " " & CognomeDipendente & " " & NomeDipendente & " " & Mese & "/" & Anno & vbNewLine
                    End If
                    If InStr(Risultato, "(E)") <> 0 Then
                        ElaboraTurniTimbrature = ElaboraTurniTimbrature & "(E) Calcolo Festivo Notturno il giorno : " & Day(Mydate) & vbNewLine
                        ElaboraTurniTimbrature = ElaboraTurniTimbrature & Risultato
                        Exit Function
                    Else
                        ElaboraTurniTimbrature = ElaboraTurniTimbrature & Risultato
                    End If
                End If
            End If
            If cpt.FormulaCalcoloPausa <> "" Then
                Risultato = Calcolo_Riepiloghi(cpt.FormulaCalcoloPausa, Mydate, CodiceSuperGruppo, CodiceContratto, cxst.Vettore_Calcolo_Orari)
                If Trim(Risultato) <> "" Then
                    If Sw_Stampato = False Then
                        Sw_Stampato = True
                        ElaboraTurniTimbrature = ElaboraTurniTimbrature & "Elaborazione di " & Dipendente & " " & CognomeDipendente & " " & NomeDipendente & " " & Mese & "/" & Anno & vbNewLine
                    End If
                    If InStr(Risultato, "(E)") <> 0 Then
                        ElaboraTurniTimbrature = ElaboraTurniTimbrature & "(E) Calcolo Pausa il giorno : " & Day(Mydate) & vbNewLine
                        ElaboraTurniTimbrature = ElaboraTurniTimbrature & Risultato
                        Exit Function
                    Else
                        ElaboraTurniTimbrature = ElaboraTurniTimbrature & Risultato
                    End If
                End If
            End If
            If cpt.FormulaCalcoloFineGiorno <> "" Then
                Risultato = Calcolo_Riepiloghi(cpt.FormulaCalcoloFineGiorno, Mydate, CodiceSuperGruppo, CodiceContratto, cxst.Vettore_Calcolo_Orari)
                If Trim(Risultato) <> "" Then
                    If Sw_Stampato = False Then
                        Sw_Stampato = True
                        ElaboraTurniTimbrature = ElaboraTurniTimbrature & "Elaborazione di " & Dipendente & " " & CognomeDipendente & " " & NomeDipendente & " " & Mese & "/" & Anno & vbNewLine
                    End If
                    If InStr(Risultato, "(E)") <> 0 Then
                        ElaboraTurniTimbrature = ElaboraTurniTimbrature & "(E) Calcolo Fine Giorno, il giorno : " & Day(Mydate) & vbNewLine
                        ElaboraTurniTimbrature = ElaboraTurniTimbrature & Risultato
                        Exit Function
                    Else
                        ElaboraTurniTimbrature = ElaboraTurniTimbrature & Risultato
                    End If
                End If
            End If
            If cpt.FormulaCalcoloSettimana <> "" Then
                If cpt.GiornoFineSettimana = Weekday(Mydate) Then
                    Risultato = Calcolo_Riepiloghi(cpt.FormulaCalcoloSettimana, Mydate, CodiceSuperGruppo, CodiceContratto, cxst.Vettore_Calcolo_Orari)
                    If Trim(Risultato) <> "" Then
                        If Sw_Stampato = False Then
                            Sw_Stampato = True
                            ElaboraTurniTimbrature = ElaboraTurniTimbrature & "Elaborazione di " & Dipendente & " " & CognomeDipendente & " " & NomeDipendente & " " & Mese & "/" & Anno & vbNewLine
                        End If
                        If InStr(Risultato, "(E)") <> 0 Then
                            ElaboraTurniTimbrature = ElaboraTurniTimbrature & "(E) Calcolo della Settimana il giorno : " & Day(Mydate) & vbNewLine
                            ElaboraTurniTimbrature = ElaboraTurniTimbrature & Risultato
                            Exit Function
                        Else
                            ElaboraTurniTimbrature = ElaboraTurniTimbrature & Risultato
                        End If
                    End If
                End If
            End If

            If cxst.Vettore_Calcolo_Orari.Elemento(45, Day(Mydate)) <> "00.00" Then
                If cxst.Vettore_Calcolo_Orari.Elemento(45, Day(Mydate)) = "24.00" Then
                    Minuti = 1440
                Else
                    Ore = Val(Mid(cxst.Vettore_Calcolo_Orari.Elemento(45, Day(Mydate)), 1, 2))
                    Minuti = Val(Mid(cxst.Vettore_Calcolo_Orari.Elemento(45, Day(Mydate)), 4, 2))
                    Minuti = Math.Round(Ore * 60, 0) + Minuti
                    '   Orario = Format(cxst.Vettore_Calcolo_Orari.Elemento(45, Day(Mydate)), "HH.mm")
                    '   Minuti = Math.Round(Hour(Orario) * 60, 0) + Minute(Orario)
                End If
                If W_TempoMese < 0 Then
                    W_TempoMese = Math.Round(W_TempoMese * -1, 2)
                    MinutiMese = Math.Round(Int(W_TempoMese) * 60, 0) + Math.Round((W_TempoMese - Int(W_TempoMese)) * 100, 0)
                    MinutiMese = Math.Round(MinutiMese * -1, 0)
                Else
                    MinutiMese = Math.Round(Int(W_TempoMese) * 60, 0) + Math.Round((W_TempoMese - Int(W_TempoMese)) * 100, 0)
                End If
                Minuti = Minuti + MinutiMese
                Ore = Int(Math.Round(Minuti / 60, 2))
                Minuti = Minuti - Math.Round(Ore * 60, 0)
                W_TempoMese = Math.Round((Ore + Minuti / 100), 2)
            End If
            Mydate = DateAdd("d", 1, Mydate)
        Loop

        If cpt.FormulaCalcoloFineMese <> "" Then
            Risultato = Calcolo_Mensile(cpt.FormulaCalcoloFineMese, DataMax, CodiceSuperGruppo, CodiceContratto, Vettore_Orari_Timbrature)
            If Trim(Risultato) <> "" Then
                If Sw_Stampato = False Then
                    Sw_Stampato = True
                    ElaboraTurniTimbrature = ElaboraTurniTimbrature & "Elaborazione di " & Dipendente & " " & CognomeDipendente & " " & NomeDipendente & " " & Mese & "/" & Anno & vbNewLine
                End If
                If InStr(Risultato, "(E)") <> 0 Then
                    ElaboraTurniTimbrature = ElaboraTurniTimbrature & "(E) Calcolo Fine Mense" & vbNewLine
                    ElaboraTurniTimbrature = ElaboraTurniTimbrature & Risultato
                    Exit Function
                Else
                    ElaboraTurniTimbrature = ElaboraTurniTimbrature & Risultato
                End If
            End If
        End If

        Call EliminaGiustificativiValoreZero(cxst.Vettore_Calcolo_Orari)

        Risultato = SalvaCalcoloOrari(Dipendente, Anno, Mese, cxst.Vettore_Calcolo_Orari)
        If InStr(Risultato, "(E)") <> 0 Then
            ElaboraTurniTimbrature = ElaboraTurniTimbrature & "(E) Salva Calcolo Orari" & vbNewLine
            ElaboraTurniTimbrature = ElaboraTurniTimbrature & Risultato
            Exit Function
        Else
            ElaboraTurniTimbrature = ElaboraTurniTimbrature & Risultato
        End If

        cxst.Vettore_Calcolo_Orari.Elemento(41, 0) = "Ore Work_1"
        cxst.Vettore_Calcolo_Orari.Elemento(42, 0) = "Ore Work_2"
        cxst.Vettore_Calcolo_Orari.Elemento(43, 0) = "Ore Previste"
        cxst.Vettore_Calcolo_Orari.Elemento(44, 0) = "Ore Pausa"
        cxst.Vettore_Calcolo_Orari.Elemento(45, 0) = "Ore Lavorate"
        cxst.Vettore_Calcolo_Orari.Elemento(46, 0) = "Ore Diurne"
        cxst.Vettore_Calcolo_Orari.Elemento(47, 0) = "Ore Notturne"
        cxst.Vettore_Calcolo_Orari.Elemento(48, 0) = "Ore Festive Diurne"
        cxst.Vettore_Calcolo_Orari.Elemento(49, 0) = "Ore Festive Notturne"
        cxst.Vettore_Calcolo_Orari.Elemento(50, 0) = "Ore Retribuite"

        Vettore_Calcolo_Orari = cxst.Vettore_Calcolo_Orari
    End Function

    Private Sub EliminaGiustificativiValoreZero(ByRef Vettore_Calcolo_Orari As Cls_Struttura_Calcolo_Orari)
        Dim cg As New Cls_Giustificativi

        For c = 0 To 40
            If Vettore_Calcolo_Orari.Elemento(c, 0) = "" Then Exit For
            Dim CodZero As String = Left(Vettore_Calcolo_Orari.Elemento(c, 0), InStr(Vettore_Calcolo_Orari.Elemento(c, 0), "/") - 1)
            If cg.CampoGiustificativi(ConnectionString, CodZero, "EliminaValoreZero") = "S" Then
                Sw_Ok = True
                For r = 1 To 31
                    If Vettore_Calcolo_Orari.Elemento(c, r) <> "" And Vettore_Calcolo_Orari.Elemento(c, r) <> "00.00" Then
                        Sw_Ok = False
                        Exit For
                    End If
                Next r
                If Sw_Ok = True Then
                    For r = 0 To 31
                        Vettore_Calcolo_Orari.Elemento(c, r) = ""
                    Next r
                End If
            End If
        Next c
        For c = 0 To 40
            If Vettore_Calcolo_Orari.Elemento(c, 0) = "" Then
                For c1 = c + 1 To 40
                    If Vettore_Calcolo_Orari.Elemento(c1, 0) <> "" Then
                        For r = 0 To 31
                            Vettore_Calcolo_Orari.Elemento(c, r) = Vettore_Calcolo_Orari.Elemento(c1, r)
                            Vettore_Calcolo_Orari.Elemento(c1, r) = ""
                        Next r
                        Exit For
                    End If
                Next c1
            End If
        Next c
    End Sub

    Private Function CreaOrariTimbrature(ByVal TimbratureProfili As String, ByVal Dipendente As Long, ByVal Anno As Integer, ByVal Mese As Byte, ByVal Giorno As Byte, ByVal CodiceSuperGruppo As String, ByRef Vettore_Orari As Cls_Struttura_Orari, ByRef Vettore_Timbrature As Cls_Struttura_Timbrature, ByRef Vettore_Orari_Timbrature As Cls_Struttura_Orari_Timbrature, ByVal ControlloOrario As String, ByVal SwGiorno As String) As String
        Dim XR As Byte
        Dim Delta As Single
        Dim W_EU As String
        Dim gs As Byte

        CreaOrariTimbrature = ""

        If Giorno = 0 Then Giorno = 31
        If SwGiorno = "S" Then
            gs = Giorno
        Else
            gs = 1
        End If

        W_EU = "U"
        XR = 0

        For i = 9 To 0 Step -1
            If Vettore_Timbrature.Alle(i, gs - 1) <> "" Or Vettore_Timbrature.Alle_Causale(i, gs - 1) <> "" Then
                W_EU = "U"
                Exit For
            End If
            If Vettore_Timbrature.Dalle(i, gs - 1) <> "" Or Vettore_Timbrature.Dalle_Causale(i, gs - 1) <> "" Then
                W_EU = "E"
                Exit For
            End If
        Next i

        Delta = 0
        DeltaAlto = "00.00"
        DeltaBasso = "00.00"
        DeltaGiustificativo = ""
        DeltaNelGruppo = ""
        DeltaFamiliare = 0
        DeltaTipo = ""
        DeltaTipoServizio = ""
        DeltaGiornoSuccessivo = ""
        AlleGiornoPrecedente = "00.00"

        Risultato = CreaOrariTimbratureDelGiorno(TimbratureProfili, Dipendente, Anno, Mese, 0, XR, Delta, W_EU, CodiceSuperGruppo, Vettore_Orari, Vettore_Timbrature, Vettore_Orari_Timbrature, ControlloOrario)
        CreaOrariTimbrature = CreaOrariTimbrature & Risultato

        For g = gs To Giorno

            Risultato = CreaOrariTimbratureDelGiorno(TimbratureProfili, Dipendente, Anno, Mese, g, XR, Delta, W_EU, CodiceSuperGruppo, Vettore_Orari, Vettore_Timbrature, Vettore_Orari_Timbrature, ControlloOrario)
            CreaOrariTimbrature = CreaOrariTimbrature & Risultato

        Next g
    End Function

    Private Function CreaOrariTimbratureDelGiorno(ByVal TimbratureProfili As String, ByVal Dipendente As Long, ByVal Anno As Integer, ByVal Mese As Byte, ByVal g As Byte, ByRef XR As Byte, ByRef Delta As Object, ByRef W_EU As String, ByVal CodiceSuperGruppo As String, ByRef Vettore_Orari As Cls_Struttura_Orari, ByRef Vettore_Timbrature As Cls_Struttura_Timbrature, ByRef Vettore_Orari_Timbrature As Cls_Struttura_Orari_Timbrature, ByVal ControlloOrario As String) As String
        Dim Timbratura As New Cls_Struttura_Timbratura
        Dim Orario As New Cls_Struttura_Orario
        Dim Assenza As New Cls_Struttura_Timbratura
        'Dim LavoroOrdinario(9) As String
        'Dim Vettore_Timbrature_Giorno As Struct_Timbrature_Giorno
        'Dim Vettore_Orari_Giorno As Struct_Orari_Giorno

        Dim cg As New Cls_Giustificativi
        Dim cxst As New Cls_ClsXScriptTurni
        cxst.ConnectionString = ConnectionString
        cxst.Utente = Utente

        Dim cpt As New Cls_ParametriTurni
        cpt.Leggi(ConnectionString)

        Dim TempoIntervallo As Integer
        Dim x As Integer

        For x = XR To 14
            Vettore_Orari_Timbrature.Dalle(x, g) = ""
            Vettore_Orari_Timbrature.Alle(x, g) = ""
            Vettore_Orari_Timbrature.Pausa(x, g) = ""
            Vettore_Orari_Timbrature.Causale(x, g) = ""
            Vettore_Orari_Timbrature.Colore(x, g) = vbWhite
            Vettore_Orari_Timbrature.Giustificativo(x, g) = ""
            Vettore_Orari_Timbrature.NelGruppo(x, g) = ""
            Vettore_Orari_Timbrature.Familiare(x, g) = 0
            Vettore_Orari_Timbrature.Tipo(x, g) = ""
            Vettore_Orari_Timbrature.Qualita(x, g) = ""
            Vettore_Orari_Timbrature.TipoServizio(x, g) = ""
            Vettore_Orari_Timbrature.GiornoSuccessivo(x, g) = ""
        Next x
        If TimbratureProfili = "S" _
          Or cg.CampoGiustificativi(ConnectionString, Vettore_Orari.Giustificativo(0, g), "TimbratureComeOrari") = "S" _
          Or (Vettore_Orari.TipoFlessibilita(g) = "P" And (Vettore_Timbrature.Dalle(0, g) <> "" Or Vettore_Timbrature.Alle(0, g) <> "")) Then
            For x = 0 To 9
                If Vettore_Orari.Dalle(x, g) <> "00.00" Or Vettore_Orari.Alle(x, g) <> "00.00" Or Vettore_Orari.Pausa(x, g) <> "00.00" Or Vettore_Orari.Giustificativo(x, g) <> "" Then
                    Vettore_Orari_Timbrature.Dalle(x, g) = Vettore_Orari.Dalle(x, g)
                    Vettore_Orari_Timbrature.Alle(x, g) = Vettore_Orari.Alle(x, g)
                    Vettore_Orari_Timbrature.Pausa(x, g) = Vettore_Orari.Pausa(x, g)
                    Vettore_Orari_Timbrature.Giustificativo(x, g) = Vettore_Orari.Giustificativo(x, g)
                    Vettore_Orari_Timbrature.NelGruppo(x, g) = Vettore_Orari.NelGruppo(x, g)
                    Vettore_Orari_Timbrature.Familiare(x, g) = Vettore_Orari.Familiare(x, g)
                    If Vettore_Orari.Tipo(x, g) = "A" Then Vettore_Orari_Timbrature.Tipo(x, g) = "Assenza"
                    Vettore_Orari_Timbrature.TipoServizio(x, g) = Vettore_Orari.TipoServizio(x, g)
                    Vettore_Orari_Timbrature.GiornoSuccessivo(x, g) = Vettore_Orari.GiornoSuccessivo(x, g)
                End If
            Next x
        Else
            x = XR
            XR = 0
            If Vettore_Orari.TipoFlessibilita(g) = "F" Or Vettore_Orari.TipoFlessibilita(g) = "FA" Then
                Dim t As Integer = 0
                Dim h As Integer = 0
                Dim W_TimbratureDalle As String = Vettore_Timbrature.Dalle(t, g)
                '        If W_EU = "E" Then
                '          If W_TimbratureDalle = "" Then W_TimbratureDalle = "00.00"
                '          Else
                '          DeltaAlto = 0
                '          DeltaBasso = 0
                '        End If
                If W_TimbratureDalle = "" Then W_TimbratureDalle = "00.00"
                If W_EU = "U" Then
                    DeltaAlto = 0
                    DeltaBasso = 0
                End If
                Do Until t > 9 And h > 9
                    '          If t = 0 And h = 0 And Delta <> 0 And (Vettore_Orari.Dalle(h, g) <> "00.00" Or Vettore_Orari.Tipo(h, g) <> "P") Then
                    '            For i = 8 To 0 Step -1
                    '              Vettore_Orari.Dalle(i + 1, g) = Vettore_Orari.Dalle(i, g)
                    '              Vettore_Orari.Alle(i + 1, g) = Vettore_Orari.Alle(i, g)
                    '              Vettore_Orari.DalleAlto(i + 1, g) = Vettore_Orari.DalleAlto(i, g)
                    '              Vettore_Orari.DalleBasso(i + 1, g) = Vettore_Orari.DalleBasso(i, g)
                    '              Vettore_Orari.AlleAlto(i + 1, g) = Vettore_Orari.AlleAlto(i, g)
                    '              Vettore_Orari.AlleBasso(i + 1, g) = Vettore_Orari.AlleBasso(i, g)
                    '              Vettore_Orari.DalleObbligo(i + 1, g) = Vettore_Orari.DalleObbligo(i, g)
                    '              Vettore_Orari.AlleObbligo(i + 1, g) = Vettore_Orari.AlleObbligo(i, g)
                    '              Vettore_Orari.Pausa(i + 1, g) = Vettore_Orari.Pausa(i, g)
                    '              Vettore_Orari.Giustificativo(i + 1, g) = Vettore_Orari.Giustificativo(i, g)
                    '              Vettore_Orari.Familiare(i + 1, g) = Vettore_Orari.Familiare(i, g)
                    '              Vettore_Orari.Tipo(i + 1, g) = Vettore_Orari.Tipo(i, g)
                    '              Vettore_Orari.TipoServizio(i + 1, g) = Vettore_Orari.TipoServizio(i, g)
                    '              Vettore_Orari.GiornoSuccessivo(i + 1, g) = Vettore_Orari.GiornoSuccessivo(i, g)
                    '            Next i
                    '            Vettore_Orari.Dalle(0, g) = "00.00"
                    '            Vettore_Orari.Alle(0, g) = "00.00"
                    '            Vettore_Orari.DalleAlto(0, g) = "00.00"
                    '            Vettore_Orari.DalleBasso(0, g) = "00.00"
                    '            Vettore_Orari.AlleAlto(0, g) = "00.00"
                    '            Vettore_Orari.AlleBasso(0, g) = "00.00"
                    '            Vettore_Orari.DalleObbligo(0, g) = "00.00"
                    '            Vettore_Orari.AlleObbligo(0, g) = "00.00"
                    '            Vettore_Orari.Pausa(0, g) = "00.00"
                    '            Vettore_Orari.Giustificativo(0, g) = DeltaGiustificativo
                    '            Vettore_Orari.Familiare(0, g) = DeltaFamiliare
                    '            Vettore_Orari.Tipo(0, g) = cg.CampoGiustificativi(connectionstring,Vettore_Orari.Giustificativo(0, g), "Tipo")
                    '            Vettore_Orari.TipoServizio(0, g) = ""
                    '            Vettore_Orari.GiornoSuccessivo(0, g) = ""
                    '             AggiornaFlessibilitaOraria(Dipendente, Anno, Mese, 0, g, Delta, DeltaGiustificativo, DeltaFamiliare, DeltaTipo, DeltaTipoServizio, DeltaGiornoSuccessivo, Vettore_Orari)
                    '          End If
                    If Vettore_Timbrature.Dalle(t, g) = "" And Vettore_Timbrature.Alle(t, g) = "" Then
                        If t > 0 Then Exit Do
                    End If
                    If Vettore_Orari.Dalle(h, g) = "00.00" And Vettore_Orari.Alle(h, g) = "00.00" And Vettore_Orari.Giustificativo(h, g) = "" Then Exit Do
                    If Vettore_Orari.Tipo(h, g) <> "I" Then
                        If t = 0 And h = 0 And Vettore_Timbrature.Dalle(0, g) = "" And Vettore_Timbrature.Alle(0, g) <> "" And W_TimbratureDalle = "00.00" And AlleGiornoPrecedente = "24.00" _
                           And ((Vettore_Orari.Dalle(0, g) = "00.00" And DeltaGiustificativo <> Vettore_Orari.Giustificativo(0, g)) _
                           Or Vettore_Orari.Dalle(0, g) <> "00.00" Or Vettore_Orari.Tipo(h, g) <> "P") Then
                            For i = 8 To 0 Step -1
                                Vettore_Orari.Dalle(i + 1, g) = Vettore_Orari.Dalle(i, g)
                                Vettore_Orari.Alle(i + 1, g) = Vettore_Orari.Alle(i, g)
                                Vettore_Orari.DalleAlto(i + 1, g) = Vettore_Orari.DalleAlto(i, g)
                                Vettore_Orari.DalleBasso(i + 1, g) = Vettore_Orari.DalleBasso(i, g)
                                Vettore_Orari.AlleAlto(i + 1, g) = Vettore_Orari.AlleAlto(i, g)
                                Vettore_Orari.AlleBasso(i + 1, g) = Vettore_Orari.AlleBasso(i, g)
                                Vettore_Orari.DalleObbligo(i + 1, g) = Vettore_Orari.DalleObbligo(i, g)
                                Vettore_Orari.AlleObbligo(i + 1, g) = Vettore_Orari.AlleObbligo(i, g)
                                '                Vettore_Orari.DCT(i + 1, g) = Vettore_Orari.DCT(i, g)
                                Vettore_Orari.Pausa(i + 1, g) = Vettore_Orari.Pausa(i, g)
                                Vettore_Orari.Giustificativo(i + 1, g) = Vettore_Orari.Giustificativo(i, g)
                                Vettore_Orari.NelGruppo(i + 1, g) = Vettore_Orari.NelGruppo(i, g)
                                Vettore_Orari.Familiare(i + 1, g) = Vettore_Orari.Familiare(i, g)
                                Vettore_Orari.Tipo(i + 1, g) = Vettore_Orari.Tipo(i, g)
                                Vettore_Orari.TipoServizio(i + 1, g) = Vettore_Orari.TipoServizio(i, g)
                                Vettore_Orari.GiornoSuccessivo(i + 1, g) = Vettore_Orari.GiornoSuccessivo(i, g)
                            Next i
                            Vettore_Orari.Dalle(0, g) = "00.00"
                            Vettore_Orari.Alle(0, g) = "00.00"
                            Vettore_Orari.DalleAlto(0, g) = "00.00"
                            Vettore_Orari.DalleBasso(0, g) = "00.00"
                            Vettore_Orari.AlleAlto(0, g) = "00.00"
                            Vettore_Orari.AlleBasso(0, g) = "00.00"
                            Vettore_Orari.DalleObbligo(0, g) = "00.00"
                            Vettore_Orari.AlleObbligo(0, g) = "00.00"
                            '              Vettore_Orari.DCT(0, g) = ""
                            Vettore_Orari.Pausa(0, g) = "00.00"
                            Vettore_Orari.Giustificativo(0, g) = cpt.GiustificativoOrarioLavoro
                            Vettore_Orari.NelGruppo(0, g) = ""
                            Vettore_Orari.Familiare(0, g) = 0
                            Vettore_Orari.Tipo(0, g) = cg.CampoGiustificativi(ConnectionString, Vettore_Orari.Giustificativo(0, g), "Tipo")
                            Vettore_Orari.TipoServizio(0, g) = ""
                            Vettore_Orari.GiornoSuccessivo(0, g) = ""
                            If DeltaBasso <> "" And Val(cxst.Sostituisci(DeltaBasso, ",", ".")) <> 0 And h = 0 Then
                                W_Tempo = Math.Round(Int(DeltaBasso) * 60, 0) + Math.Round((DeltaBasso - Int(DeltaBasso)) * 100, 0)
                                Vettore_Orari.Alle(0, g) = Format(DateAdd("n", W_Tempo, TimeSerial(0, 0, 0)), "HH.mm")
                                Vettore_Orari.AlleAlto(0, g) = Vettore_Orari.Alle(h, g)
                                Vettore_Orari.AlleBasso(0, g) = Vettore_Orari.Alle(h, g)
                                Vettore_Orari.Giustificativo(0, g) = DeltaGiustificativo
                                Vettore_Orari.NelGruppo(0, g) = DeltaNelGruppo
                                Vettore_Orari.Familiare(0, g) = DeltaFamiliare
                                Vettore_Orari.Tipo(0, g) = DeltaTipo
                                Vettore_Orari.TipoServizio(0, g) = DeltaTipoServizio
                                Vettore_Orari.GiornoSuccessivo(0, g) = "S"
                                If Vettore_Orari.Dalle(h + 1, g) = "00.00" Then
                                    If cg.CampoGiustificativi(ConnectionString, Vettore_Orari.Giustificativo(h + 1, g), "Tipo") <> "P" And Vettore_Orari.Alle(h + 1, g) = "24.00" Then
                                        Delta = 0
                                    Else
                                        Vettore_Orari.Dalle(h + 1, g) = Vettore_Orari.Alle(h, g)
                                        Vettore_Orari.DalleAlto(h + 1, g) = Vettore_Orari.Alle(h, g)
                                        Vettore_Orari.DalleBasso(h + 1, g) = Vettore_Orari.Alle(h, g)
                                        W_Alle = Vettore_Orari.Alle(h + 1, g)
                                        Vettore_Orari.Alle(h + 1, g) = Format(DateAdd("n", W_Tempo, TimeSerial(Val(Mid(Vettore_Orari.Alle(h + 1, g), 1, 2)), Val(Mid(Vettore_Orari.Alle(h + 1, g), 4, 2)), 0)), "HH.mm")
                                        If Vettore_Orari.Alle(h + 1, g) < W_Alle Then Vettore_Orari.Alle(h + 1, g) = "24.00"
                                        Vettore_Orari.AlleAlto(h + 1, g) = Vettore_Orari.Alle(h + 1, g)
                                        Vettore_Orari.AlleBasso(h + 1, g) = Vettore_Orari.Alle(h + 1, g)
                                    End If
                                Else
                                    Delta = 0
                                End If
                            End If
                            If Vettore_Orari.Alle(h + 1, g) < Vettore_Timbrature.Alle(t, g) Then
                                Vettore_Orari.DalleBasso(h + 1, g) = Vettore_Orari.Dalle(h + 1, g)
                                Vettore_Orari.AlleBasso(h + 1, g) = Vettore_Orari.Alle(h + 1, g)
                                W_TimbratureDalle = Vettore_Orari.Alle(h + 1, g)
                                W_OrariAlle = Vettore_Orari.Alle(h, g)
                                h = h + 1
                            Else
                                If Vettore_Orari.Alle(h + 1, g) > Vettore_Timbrature.Alle(t, g) Then
                                    t = t + 1
                                    W_TimbratureDalle = Vettore_Timbrature.Dalle(t, g)
                                    h = h + 1
                                Else
                                    W_OrariAlle = Vettore_Orari.Alle(h, g)
                                    h = h + 1
                                    t = t + 1
                                    W_TimbratureDalle = Vettore_Timbrature.Dalle(t, g)
                                End If
                            End If
                        Else
                            If Vettore_Timbrature.Alle(t, g) = "" Then
                                If W_TimbratureDalle >= Vettore_Orari.DalleAlto(h, g) Then
                                    If W_TimbratureDalle <= Vettore_Orari.DalleBasso(h, g) Then
                                        Vettore_Orari.DalleAlto(h, g) = W_TimbratureDalle
                                        DeltaAlto = 0
                                        DeltaBasso = cxst.Mod_OperaConMinuti(W_TimbratureDalle, "-", Vettore_Orari.Dalle(h, g))
                                        If Delta = 0 Then Delta = DeltaBasso
                                        DeltaGiustificativo = Vettore_Orari.Giustificativo(h, g)
                                        DeltaNelGruppo = Vettore_Orari.NelGruppo(h, g)
                                        DeltaFamiliare = Vettore_Orari.Familiare(h, g)
                                        DeltaTipo = Vettore_Orari.Tipo(h, g)
                                        DeltaTipoServizio = Vettore_Orari.TipoServizio(h, g)
                                        DeltaGiornoSuccessivo = Vettore_Orari.GiornoSuccessivo(h, g)

                                        If Vettore_Orari.Dalle(h, g) <> Vettore_Orari.DalleBasso(h, g) Then
                                            AggiornaFlessibilitaOraria(Dipendente, Anno, Mese, h, g, Delta, DeltaGiustificativo, DeltaNelGruppo, DeltaFamiliare, DeltaTipo, DeltaTipoServizio, DeltaGiornoSuccessivo, Vettore_Orari)
                                        End If

                                        W_TimbratureDalle = Vettore_Orari.Alle(h, g)
                                        W_OrariAlle = Vettore_Orari.Alle(h, g)
                                        h = h + 1
                                        t = t + 1
                                    Else
                                        If W_TimbratureDalle < Vettore_Orari.Alle(h, g) Then
                                            If Vettore_Orari.Dalle(h, g) <> "00.00" Or Vettore_Orari.Alle(h, g) <> "24.00" Then
                                                DeltaAlto = 0
                                                DeltaBasso = cxst.Mod_OperaConMinuti(Vettore_Orari.DalleBasso(h, g), "-", Vettore_Orari.Dalle(h, g))
                                                If Delta = 0 Then Delta = DeltaBasso
                                                DeltaGiustificativo = Vettore_Orari.Giustificativo(h, g)
                                                DeltaNelGruppo = Vettore_Orari.NelGruppo(h, g)
                                                DeltaFamiliare = Vettore_Orari.Familiare(h, g)
                                                DeltaTipo = Vettore_Orari.Tipo(h, g)
                                                DeltaTipoServizio = Vettore_Orari.TipoServizio(h, g)
                                                DeltaGiornoSuccessivo = Vettore_Orari.GiornoSuccessivo(h, g)
                                                If Vettore_Orari.Dalle(h, g) <> Vettore_Orari.DalleBasso(h, g) Then
                                                    AggiornaFlessibilitaOraria(Dipendente, Anno, Mese, h, g, Delta, DeltaGiustificativo, DeltaNelGruppo, DeltaFamiliare, DeltaTipo, DeltaTipoServizio, DeltaGiornoSuccessivo, Vettore_Orari)
                                                End If
                                            End If
                                        End If
                                        W_OrariAlle = Vettore_Orari.Alle(h, g)
                                        h = h + 1
                                    End If
                                Else
                                    If W_TimbratureDalle <= Vettore_Orari.Dalle(h, g) Then
                                        DeltaAlto = 0
                                        DeltaBasso = 0
                                        DeltaGiustificativo = Vettore_Orari.Giustificativo(h, g)
                                        DeltaNelGruppo = Vettore_Orari.NelGruppo(h, g)
                                        DeltaFamiliare = Vettore_Orari.Familiare(h, g)
                                        DeltaTipo = Vettore_Orari.Tipo(h, g)
                                        DeltaTipoServizio = Vettore_Orari.TipoServizio(h, g)
                                        DeltaGiornoSuccessivo = Vettore_Orari.GiornoSuccessivo(h, g)
                                        If Vettore_Orari.Dalle(h, g) <> Vettore_Orari.DalleBasso(h, g) Then
                                            Delta = 0
                                            AggiornaFlessibilitaOraria(Dipendente, Anno, Mese, h, g, Delta, DeltaGiustificativo, DeltaNelGruppo, DeltaFamiliare, DeltaTipo, DeltaTipoServizio, DeltaGiornoSuccessivo, Vettore_Orari)
                                        End If
                                    End If
                                    t = t + 1
                                    W_TimbratureDalle = Vettore_Timbrature.Dalle(t, g)
                                End If
                            Else
                                If Vettore_Timbrature.Alle(t, g) <= Vettore_Orari.DalleAlto(h, g) Then
                                    t = t + 1
                                    W_TimbratureDalle = Vettore_Timbrature.Dalle(t, g)
                                Else
                                    If W_TimbratureDalle >= Vettore_Orari.AlleBasso(h, g) Then
                                        W_OrariAlle = Vettore_Orari.Alle(h, g)
                                        h = h + 1
                                    Else
                                        If W_TimbratureDalle >= Vettore_Orari.DalleAlto(h, g) Then
                                            If W_TimbratureDalle <= Vettore_Orari.DalleBasso(h, g) Then
                                                If Vettore_Orari.DalleAlto(h, g) <> Vettore_Orari.DalleBasso(h, g) Then
                                                    If Delta = 0 Then
                                                        Delta = cxst.Mod_OperaConMinuti(W_TimbratureDalle, "-", Vettore_Orari.Dalle(h, g))
                                                    End If
                                                    AggiornaFlessibilitaOraria(Dipendente, Anno, Mese, h, g, Delta, DeltaGiustificativo, DeltaNelGruppo, DeltaFamiliare, DeltaTipo, DeltaTipoServizio, DeltaGiornoSuccessivo, Vettore_Orari)
                                                End If
                                                W_TimbratureDalle = Vettore_Orari.Alle(h, g)
                                                W_OrariAlle = Vettore_Orari.Alle(h, g)
                                                h = h + 1
                                            Else
                                                If Vettore_Orari.Dalle(h, g) <> Vettore_Orari.DalleBasso(h, g) Then
                                                    If Delta = 0 Then
                                                        Delta = cxst.Mod_OperaConMinuti(Vettore_Orari.DalleBasso(h, g), "-", Vettore_Orari.Dalle(h, g))
                                                    End If
                                                    AggiornaFlessibilitaOraria(Dipendente, Anno, Mese, h, g, Delta, DeltaGiustificativo, DeltaNelGruppo, DeltaFamiliare, DeltaTipo, DeltaTipoServizio, DeltaGiornoSuccessivo, Vettore_Orari)
                                                End If
                                                W_OrariAlle = Vettore_Orari.Alle(h, g)
                                                h = h + 1
                                            End If
                                        Else
                                            If W_TimbratureDalle <= Vettore_Orari.Dalle(h, g) Then
                                                If Vettore_Orari.Dalle(h, g) <> Vettore_Orari.DalleBasso(h, g) Then
                                                    Vettore_Orari.DalleBasso(h, g) = Vettore_Orari.Dalle(h, g)
                                                    ' AggiornaFlessibilitaOraria(Dipendente, Anno, Mese, h, g, Delta, DeltaGiustificativo, DeltaNelGruppo, DeltaFamiliare, DeltaTipo, DeltaTipoServizio, DeltaGiornoSuccessivo, Vettore_Orari)
                                                End If
                                            End If
                                            W_TimbratureDalle = Vettore_Orari.Alle(h, g)
                                        End If
                                    End If
                                End If
                            End If
                        End If
                    Else
                        '            W_OrariAlle = Vettore_Orari.Alle(h, g)
                        h = h + 1
                    End If
                Loop
            End If
            '
            '   Tratta gli straordinari per consentire di fare straordinario nel tempo di recupero orario
            '
            Dim Sw_OP As Boolean = False

            For h = 0 To 9
                Sw_Ok = False
                If Vettore_Orari.Dalle(h, g) = "00.00" And Vettore_Orari.Alle(h, g) = "00.00" And Vettore_Orari.Giustificativo(h, g) = "" Then Exit For
                If Vettore_Orari.Tipo(h, g) = "P" Then
                    Sw_OP = True
                    If cg.CampoGiustificativi(ConnectionString, Vettore_Orari.Giustificativo(h, g), "LavoroOrdinario") = "N" Then
                        If h > 0 Then
                            If Vettore_Orari.Alle(h - 1, g) > Vettore_Orari.Dalle(h, g) Then Sw_Ok = True
                        End If
                        If h < 9 Then
                            If Vettore_Orari.Alle(h, g) > Vettore_Orari.Dalle(h + 1, g) And Vettore_Orari.Dalle(h + 1, g) <> "00.00" Then Sw_Ok = True
                        End If
                        If Sw_Ok = True Then
                            Orario.GgSett = Vettore_Orari.GgSett(g)
                            Orario.Richiesti = Vettore_Orari.Richiesti(g)
                            Orario.Variati = Vettore_Orari.Variati(g)
                            Orario.Modificati = Vettore_Orari.Modificati(g)
                            Orario.TipoFlessibilita = Vettore_Orari.TipoFlessibilita(g)
                            Orario.FasceOrarie = Vettore_Orari.FasceOrarie(g)
                            Orario.Dalle = Vettore_Orari.Dalle(h, g)
                            Orario.Alle = Vettore_Orari.Alle(h, g)
                            Orario.DalleAlto = Vettore_Orari.DalleAlto(h, g)
                            Orario.AlleAlto = Vettore_Orari.AlleAlto(h, g)
                            Orario.DalleBasso = Vettore_Orari.DalleBasso(h, g)
                            Orario.AlleBasso = Vettore_Orari.AlleBasso(h, g)
                            Orario.DalleObbligo = Vettore_Orari.DalleObbligo(h, g)
                            Orario.AlleObbligo = Vettore_Orari.AlleObbligo(h, g)
                            Orario.Pausa = Vettore_Orari.Pausa(h, g)
                            Orario.Giustificativo = Vettore_Orari.Giustificativo(h, g)
                            Orario.NelGruppo = Vettore_Orari.NelGruppo(h, g)
                            Orario.Familiare = Vettore_Orari.Familiare(h, g)
                            Orario.Tipo = Vettore_Orari.Tipo(h, g)
                            Orario.TipoServizio = Vettore_Orari.TipoServizio(h, g)
                            Orario.GiornoSuccessivo = Vettore_Orari.GiornoSuccessivo(h, g)
                            Orario.PU = ""
                            If h = 0 Then
                                Orario.PU = "P"
                            Else
                                If Vettore_Orari.Dalle(h + 1, g) = "00.00" Then Orario.PU = "U"
                            End If
                            If Vettore_Orari.TipoFlessibilita(g) = "X" Or Vettore_Orari.TipoFlessibilita(g) = "XA" Then
                                CreaOrariTimbratureDelGiorno = CreaOrariTimbratureDelGiorno & Risultato
                            Else
                                If ControlloOrario = "P" Or ControlloOrario = "S" Then
                                    Risultato = AbbinaOrariTimbrature(Dipendente, g, x, XR, Vettore_Timbrature, Vettore_Orari_Timbrature, Orario, ControlloOrario)
                                    CreaOrariTimbratureDelGiorno = CreaOrariTimbratureDelGiorno & Risultato
                                Else
                                    CreaOrariTimbratureDelGiorno = CreaOrariTimbratureDelGiorno & Risultato
                                End If
                            End If
                            Vettore_Orari.Dalle(h, g) = "00.00"
                            Vettore_Orari.DalleAlto(h, g) = "00.00"
                            Vettore_Orari.DalleBasso(h, g) = "00.00"
                            Vettore_Orari.DalleObbligo(h, g) = "00.00"
                            '              Vettore_Orari.DCT(h, g) = ""
                            Vettore_Orari.Alle(h, g) = "00.00"
                            Vettore_Orari.AlleAlto(h, g) = "00.00"
                            Vettore_Orari.AlleBasso(h, g) = "00.00"
                            Vettore_Orari.AlleObbligo(h, g) = "00.00"
                            Vettore_Orari.Pausa(h, g) = "00.00"
                            Vettore_Orari.Giustificativo(h, g) = ""
                            Vettore_Orari.NelGruppo(h, g) = ""
                            Vettore_Orari.Familiare(h, g) = 0
                            Vettore_Orari.Tipo(h, g) = ""
                            Vettore_Orari.TipoServizio(h, g) = ""
                            Vettore_Orari.GiornoSuccessivo(h, g) = ""
                        End If
                    End If
                End If
            Next h

            Sw_Ok = False
            Do Until Sw_Ok = True
                Sw_Ok = True
                For i = 1 To 9
                    If (Vettore_Orari.Dalle(i - 1, g) = "00.00" And Vettore_Orari.Alle(i - 1, g) = "00.00" And Vettore_Orari.Giustificativo(i - 1, g) = "") And (Vettore_Orari.Dalle(i, g) <> "00.00" Or Vettore_Orari.Alle(i, g) <> "00.00" Or Vettore_Orari.Giustificativo(i, g) <> "") Then
                        Sw_Ok = False
                        W_Dalle = Vettore_Orari.Dalle(i, g)
                        W_DalleAlto = Vettore_Orari.DalleAlto(i, g)
                        W_DalleBasso = Vettore_Orari.DalleBasso(i, g)
                        W_DalleObbligo = Vettore_Orari.DalleObbligo(i, g)
                        '            W_DCT = Vettore_Orari.DCT(i, g)
                        W_Alle = Vettore_Orari.Alle(i, g)
                        W_AlleAlto = Vettore_Orari.AlleAlto(i, g)
                        W_AlleBasso = Vettore_Orari.AlleBasso(i, g)
                        W_AlleObbligo = Vettore_Orari.AlleObbligo(i, g)
                        W_Pausa = Vettore_Orari.Pausa(i, g)
                        W_Giustificativo = Vettore_Orari.Giustificativo(i, g)
                        W_NelGruppo = Vettore_Orari.NelGruppo(i, g)
                        W_Familiare = Vettore_Orari.Familiare(i, g)
                        W_Tipo = Vettore_Orari.Tipo(i, g)
                        W_TipoServizio = Vettore_Orari.TipoServizio(i, g)
                        W_GiornoSuccessivo = Vettore_Orari.GiornoSuccessivo(i, g)

                        Vettore_Orari.Dalle(i, g) = Vettore_Orari.Dalle(i - 1, g)
                        Vettore_Orari.DalleAlto(i, g) = Vettore_Orari.DalleAlto(i - 1, g)
                        Vettore_Orari.DalleBasso(i, g) = Vettore_Orari.DalleBasso(i - 1, g)
                        Vettore_Orari.DalleObbligo(i, g) = Vettore_Orari.DalleObbligo(i - 1, g)
                        '            Vettore_Orari.DCT(i, g) = Vettore_Orari.DCT(i - 1, g)
                        Vettore_Orari.Alle(i, g) = Vettore_Orari.Alle(i - 1, g)
                        Vettore_Orari.AlleAlto(i, g) = Vettore_Orari.AlleAlto(i - 1, g)
                        Vettore_Orari.AlleBasso(i, g) = Vettore_Orari.AlleBasso(i - 1, g)
                        Vettore_Orari.AlleObbligo(i, g) = Vettore_Orari.AlleObbligo(i - 1, g)
                        Vettore_Orari.Pausa(i, g) = Vettore_Orari.Pausa(i - 1, g)
                        Vettore_Orari.Giustificativo(i, g) = Vettore_Orari.Giustificativo(i - 1, g)
                        Vettore_Orari.NelGruppo(i, g) = Vettore_Orari.NelGruppo(i - 1, g)
                        Vettore_Orari.Familiare(i, g) = Vettore_Orari.Familiare(i - 1, g)
                        Vettore_Orari.Tipo(i, g) = Vettore_Orari.Tipo(i - 1, g)
                        Vettore_Orari.TipoServizio(i, g) = Vettore_Orari.TipoServizio(i - 1, g)
                        Vettore_Orari.GiornoSuccessivo(i, g) = Vettore_Orari.GiornoSuccessivo(i - 1, g)

                        Vettore_Orari.Dalle(i - 1, g) = W_Dalle
                        Vettore_Orari.DalleAlto(i - 1, g) = W_DalleAlto
                        Vettore_Orari.DalleBasso(i - 1, g) = W_DalleBasso
                        Vettore_Orari.DalleObbligo(i - 1, g) = W_DalleObbligo
                        '            Vettore_Orari.DCT(i - 1, g) = W_DCT
                        Vettore_Orari.Alle(i - 1, g) = W_Alle
                        Vettore_Orari.AlleAlto(i - 1, g) = W_AlleAlto
                        Vettore_Orari.AlleBasso(i - 1, g) = W_AlleBasso
                        Vettore_Orari.AlleObbligo(i - 1, g) = W_AlleObbligo
                        Vettore_Orari.Pausa(i - 1, g) = W_Pausa
                        Vettore_Orari.Giustificativo(i - 1, g) = W_Giustificativo
                        Vettore_Orari.NelGruppo(i - 1, g) = W_NelGruppo
                        Vettore_Orari.Familiare(i - 1, g) = W_Familiare
                        Vettore_Orari.Tipo(i - 1, g) = W_Tipo
                        Vettore_Orari.TipoServizio(i - 1, g) = W_TipoServizio
                        Vettore_Orari.GiornoSuccessivo(i - 1, g) = W_GiornoSuccessivo
                    End If
                Next i
            Loop

            Sw_Ok = False
            Do Until Sw_Ok = True
                Sw_Ok = True
                For i = 1 To 9
                    If (Vettore_Timbrature.Dalle(i - 1, g) = "" And Vettore_Timbrature.Dalle_Causale(i - 1, g) = "" And Vettore_Timbrature.Alle(i - 1, g) = "" And Vettore_Timbrature.Alle_Causale(i - 1, g) = "") And (Vettore_Timbrature.Dalle(i, g) <> "" Or Vettore_Timbrature.Dalle_Causale(i, g) <> "" Or Vettore_Timbrature.Alle(i, g) <> "" Or Vettore_Timbrature.Alle_Causale(i, g) <> "") Then
                        Sw_Ok = False
                        W_Dalle = Vettore_Timbrature.Dalle(i, g)
                        W_Dalle_Causale = Vettore_Timbrature.Dalle_Causale(i, g)
                        W_Dalle_Colore = Vettore_Timbrature.Dalle_Colore(i, g)
                        W_Alle = Vettore_Timbrature.Alle(i, g)
                        W_Alle_Causale = Vettore_Timbrature.Alle_Causale(i, g)
                        W_Alle_Colore = Vettore_Timbrature.Alle_Colore(i, g)

                        Vettore_Timbrature.Dalle(i, g) = Vettore_Timbrature.Dalle(i - 1, g)
                        Vettore_Timbrature.Dalle_Causale(i, g) = Vettore_Timbrature.Dalle_Causale(i - 1, g)
                        Vettore_Timbrature.Dalle_Colore(i, g) = Vettore_Timbrature.Dalle_Colore(i - 1, g)
                        Vettore_Timbrature.Alle(i, g) = Vettore_Timbrature.Alle(i - 1, g)
                        Vettore_Timbrature.Alle_Causale(i, g) = Vettore_Timbrature.Alle_Causale(i - 1, g)
                        Vettore_Timbrature.Alle_Colore(i, g) = Vettore_Timbrature.Alle_Colore(i - 1, g)

                        Vettore_Timbrature.Dalle(i - 1, g) = W_Dalle
                        Vettore_Timbrature.Dalle_Causale(i - 1, g) = W_Dalle_Causale
                        Vettore_Timbrature.Dalle_Colore(i - 1, g) = W_Dalle_Colore
                        Vettore_Timbrature.Alle(i - 1, g) = W_Alle
                        Vettore_Timbrature.Alle_Causale(i - 1, g) = W_Alle_Causale
                        Vettore_Timbrature.Alle_Colore(i - 1, g) = W_Alle_Colore
                    End If
                Next i
            Loop
            '
            '   Fine Trattamento Straordinari
            '
            If Vettore_Orari.TipoFlessibilita(g) = "X" Or Vettore_Orari.TipoFlessibilita(g) = "XA" Then
                TempoIntervallo = 0
                For i = 0 To 9
                    If Vettore_Orari.Tipo(i, g) = "P" Or (Vettore_Orari.Tipo(i, g) = "A" And cg.CampoGiustificativi(ConnectionString, Vettore_Orari.Giustificativo(i, g), "RegoleGenerali") = "S") Then
                        TempoIntervallo = TempoIntervallo + DateDiff("n", TimeSerial(Val(Mid(Vettore_Orari.Dalle(i, g), 1, 2)), Val(Mid(Vettore_Orari.Dalle(i, g), 4, 2)), 0), TimeSerial(Val(Mid(Vettore_Orari.Alle(i, g), 1, 2)), Val(Mid(Vettore_Orari.Alle(i, g), 4, 2)), 0))
                    End If
                Next i
            End If
            For t = 0 To 9
                If Vettore_Timbrature.Dalle(t, g) <> Vettore_Timbrature.Alle(t, g) Or (Vettore_Timbrature.Dalle(t, g) = Vettore_Timbrature.Alle(t, g) And (Vettore_Timbrature.Dalle(t, g) = "" And t = 0 And W_EU = "E" And Sw_OP = True) Or Vettore_Timbrature.Dalle_Causale(t, g) <> "" Or Vettore_Timbrature.Alle_Causale(t, g) <> "") Then
                    If Vettore_Timbrature.Alle(t, g) = "" Then Vettore_Timbrature.Alle(t, g) = "24.00"
                    Timbratura.Dalle = Vettore_Timbrature.Dalle(t, g)
                    Timbratura.Dalle_Causale = Vettore_Timbrature.Dalle_Causale(t, g)
                    Timbratura.Dalle_Colore = Vettore_Timbrature.Dalle_Colore(t, g)
                    Timbratura.Alle = Vettore_Timbrature.Alle(t, g)
                    Timbratura.Alle_Causale = Vettore_Timbrature.Alle_Causale(t, g)
                    Timbratura.Alle_Colore = Vettore_Timbrature.Alle_Colore(t, g)
                    Timbratura.Proseguimento = ""
                    If Vettore_Timbrature.Dalle(t, g) = "" Then Timbratura.Proseguimento = "S"
                    If Vettore_Orari.TipoFlessibilita(g) = "X" Or Vettore_Orari.TipoFlessibilita(g) = "XA" Then
                        Risultato = AbbinaTimbratureOrariFlex(Dipendente, g, x, XR, TempoIntervallo, Vettore_Orari, Vettore_Orari_Timbrature, Timbratura)
                        CreaOrariTimbratureDelGiorno = CreaOrariTimbratureDelGiorno & Risultato
                    Else
                        If ControlloOrario = "E" Then
                            Risultato = AbbinaTimbratureEntrataOrari(Dipendente, g, x, XR, Vettore_Orari, Vettore_Orari_Timbrature, Timbratura)
                            CreaOrariTimbratureDelGiorno = CreaOrariTimbratureDelGiorno & Risultato
                        End If
                        If ControlloOrario = "P" Or ControlloOrario = "S" Then
                            Risultato = AbbinaTimbratureOrari(Dipendente, g, x, XR, Vettore_Orari, Vettore_Orari_Timbrature, Timbratura, ControlloOrario)
                            CreaOrariTimbratureDelGiorno = CreaOrariTimbratureDelGiorno & Risultato
                        End If
                    End If
                End If
                If Vettore_Timbrature.Dalle(t, g) <> "" Then W_EU = "E"
                If Vettore_Timbrature.Alle(t, g) <> "" And Vettore_Timbrature.Alle(t, g) <> "24.00" Then W_EU = "U"
            Next t
            For t = 0 To 9
                If Vettore_Timbrature.Dalle(t, g) = "" And Vettore_Timbrature.Alle(t, g) = "" Then Exit For
                Sw_Ok = True
                If t = 0 Then
                    Timbratura.Dalle = Vettore_Timbrature.Dalle(t, g)
                    Timbratura.Dalle_Causale = Vettore_Timbrature.Dalle_Causale(t, g)
                    Timbratura.Dalle_Colore = Vettore_Timbrature.Dalle_Colore(t, g)
                    Timbratura.Alle = Vettore_Timbrature.Alle(t, g)
                    Timbratura.Alle_Causale = Vettore_Timbrature.Alle_Causale(t, g)
                    Timbratura.Alle_Colore = Vettore_Timbrature.Alle_Colore(t, g)
                    If Vettore_Timbrature.Dalle(t, g) = "" Then Sw_Ok = False
                    Assenza.Dalle = "00.00"
                    Assenza.Dalle_Causale = ""
                    Assenza.Dalle_Colore = vbWhite
                Else
                    Timbratura.Dalle = Vettore_Timbrature.Dalle(t - 1, g)
                    Timbratura.Dalle_Causale = Vettore_Timbrature.Dalle_Causale(t - 1, g)
                    Timbratura.Dalle_Colore = Vettore_Timbrature.Dalle_Colore(t - 1, g)
                    Timbratura.Alle = Vettore_Timbrature.Alle(t - 1, g)
                    Timbratura.Alle_Causale = Vettore_Timbrature.Alle_Causale(t - 1, g)
                    Timbratura.Alle_Colore = Vettore_Timbrature.Alle_Colore(t - 1, g)
                    Timbratura.Proseguimento = ""
                    If Vettore_Timbrature.Dalle(t - 1, g) = "" Then Timbratura.Proseguimento = "S"
                    Assenza.Dalle = Vettore_Timbrature.Alle(t - 1, g)
                    Assenza.Dalle_Causale = Vettore_Timbrature.Alle_Causale(t - 1, g)
                    Assenza.Dalle_Colore = Vettore_Timbrature.Alle_Colore(t - 1, g)
                End If
                If Sw_Ok = True Then
                    Assenza.Alle = Vettore_Timbrature.Dalle(t, g)
                    Assenza.Alle_Causale = Vettore_Timbrature.Dalle_Causale(t, g)
                    Assenza.Alle_Colore = Vettore_Timbrature.Dalle_Colore(t, g)
                    If Vettore_Orari.TipoFlessibilita(g) = "X" Or Vettore_Orari.TipoFlessibilita(g) = "XA" Then
                        Risultato = AbbinaAssenzeOrariFlex(Dipendente, Anno, Mese, g, x, XR, CodiceSuperGruppo, Vettore_Orari, Vettore_Orari_Timbrature, Assenza, Timbratura, ControlloOrario)
                        CreaOrariTimbratureDelGiorno = CreaOrariTimbratureDelGiorno & Risultato
                    Else
                        If ControlloOrario = "E" Then
                            Risultato = AbbinaAssenzeEntrataOrari(Dipendente, Anno, Mese, g, x, XR, Vettore_Orari, Vettore_Orari_Timbrature, Assenza, Timbratura)
                            CreaOrariTimbratureDelGiorno = CreaOrariTimbratureDelGiorno & Risultato
                        End If
                        If ControlloOrario = "P" Or ControlloOrario = "S" Then
                            Risultato = AbbinaAssenzeOrari(Dipendente, Anno, Mese, g, x, XR, CodiceSuperGruppo, Vettore_Orari, Vettore_Orari_Timbrature, Assenza, Timbratura, ControlloOrario)
                            CreaOrariTimbratureDelGiorno = CreaOrariTimbratureDelGiorno & Risultato
                        End If
                    End If
                End If
            Next t
            Assenza.Dalle = "00.00"
            For t = 9 To 0 Step -1
                If Vettore_Timbrature.Alle(t, g) <> "" Then
                    Assenza.Dalle = Vettore_Timbrature.Alle(t, g)
                    Exit For
                End If
            Next t
            Assenza.Dalle_Causale = ""
            Assenza.Dalle_Colore = vbWhite
            Assenza.Alle = "24.00"
            Assenza.Alle_Causale = ""
            Assenza.Alle_Colore = vbWhite
            If Vettore_Orari.TipoFlessibilita(g) = "X" Or Vettore_Orari.TipoFlessibilita(g) = "XA" Then
                Risultato = AbbinaAssenzeOrariFlex(Dipendente, Anno, Mese, g, x, XR, CodiceSuperGruppo, Vettore_Orari, Vettore_Orari_Timbrature, Assenza, Timbratura, ControlloOrario)
                CreaOrariTimbratureDelGiorno = CreaOrariTimbratureDelGiorno & Risultato
            Else
                If ControlloOrario = "E" Then
                    Risultato = AbbinaAssenzeEntrataOrari(Dipendente, Anno, Mese, g, x, XR, Vettore_Orari, Vettore_Orari_Timbrature, Assenza, Timbratura)
                    CreaOrariTimbratureDelGiorno = CreaOrariTimbratureDelGiorno & Risultato
                End If
                If ControlloOrario = "P" Or ControlloOrario = "S" Then
                    Risultato = AbbinaAssenzeOrari(Dipendente, Anno, Mese, g, x, XR, CodiceSuperGruppo, Vettore_Orari, Vettore_Orari_Timbrature, Assenza, Timbratura, ControlloOrario)
                    CreaOrariTimbratureDelGiorno = CreaOrariTimbratureDelGiorno & Risultato
                End If
            End If
        End If
        '
        ' Carica gli orari con Tipo giustificativo = "A" e RegoleGenerali = "N"
        '
        Risultato = CaricaOrariTipoAI(Dipendente, g, x, XR, "A", "N", Vettore_Orari, Vettore_Orari_Timbrature)
        '
        ' Carica gli orari con Tipo giustificativo = "I"
        '
        Risultato = CaricaOrariTipoAI(Dipendente, g, x, XR, "I", "", Vettore_Orari, Vettore_Orari_Timbrature)
        '
        'mau+
        ' "Sostituzione"
        ' "Assenza"
        ' "UscitaAnticipata"
        ' "UscitaPosticipata"
        ' "EntrataAnticipata"
        ' "EntrataPosticipata"
        ' ""
        For x = 0 To 14
            If Vettore_Orari_Timbrature.Giustificativo(x, g) <> "" Then
                If Vettore_Orari_Timbrature.Tipo(x, g) <> "Informazione" And cg.CampoGiustificativi(ConnectionString, Vettore_Orari_Timbrature.Giustificativo(x, g), "RegoleGenerali") = "S" Then
                    If Vettore_Orari_Timbrature.Tipo(x, g) = "Assenza" Or Vettore_Orari_Timbrature.Tipo(x, g) = "UscitaAnticipata" Or Vettore_Orari_Timbrature.Tipo(x, g) = "EntrataPosticipata" Then
                        If cg.CampoGiustificativi(ConnectionString, Vettore_Orari_Timbrature.Giustificativo(x, g), "Tipo") = "P" Then
                            If Vettore_Orari_Timbrature.Tipo(x, g) = "EntrataPosticipata" And cpt.GiustificativoEntrataPosticipata <> "" Then
                                Vettore_Orari_Timbrature.Giustificativo(x, g) = cpt.GiustificativoEntrataPosticipata
                            Else
                                If Vettore_Orari_Timbrature.Tipo(x, g) = "UscitaAnticipata" And cpt.GiustificativoUscitaAnticipata <> "" Then
                                    Vettore_Orari_Timbrature.Giustificativo(x, g) = cpt.GiustificativoUscitaAnticipata
                                Else
                                    Vettore_Orari_Timbrature.Giustificativo(x, g) = cg.CampoGiustificativi(ConnectionString, Vettore_Orari_Timbrature.Giustificativo(x, g), "GiustificativoPerAssenza")
                                End If
                            End If
                        End If
                    Else
                        If cg.CampoGiustificativi(ConnectionString, Vettore_Orari_Timbrature.Giustificativo(x, g), "Tipo") = "A" Then
                            If Vettore_Orari_Timbrature.Tipo(x, g) = "EntrataAnticipata" And cpt.GiustificativoEntrataAnticipata <> "" Then
                                Vettore_Orari_Timbrature.Giustificativo(x, g) = cpt.GiustificativoEntrataAnticipata
                            Else
                                If Vettore_Orari_Timbrature.Tipo(x, g) = "UscitaPosticipata" And cpt.GiustificativoUscitaPosticipata <> "" Then
                                    Vettore_Orari_Timbrature.Giustificativo(x, g) = cpt.GiustificativoUscitaPosticipata
                                Else
                                    If Vettore_Orari_Timbrature.Tipo(x, g) = "Sostituzione" And cpt.GiustificativoSostituzione <> "" Then
                                        Vettore_Orari_Timbrature.Giustificativo(x, g) = cpt.GiustificativoSostituzione
                                    Else
                                        Vettore_Orari_Timbrature.Giustificativo(x, g) = cg.CampoGiustificativi(ConnectionString, Vettore_Orari_Timbrature.Giustificativo(x, g), "GiustificativoPerPresenza")
                                    End If
                                End If
                            End If
                        End If
                    End If
                End If
            End If
        Next x
        'mau-
        AlleGiornoPrecedente = "00.00"
        For i = 9 To 0 Step -1
            If Vettore_Orari.Alle(i, g) <> "00.00" Then
                AlleGiornoPrecedente = Vettore_Orari.Alle(i, g)
                Exit For
            End If
        Next i
    End Function

    Private Function CreaDifferenzeTimbrature(ByVal TimbratureProfili As String, ByVal Dipendente As Long, ByVal Data As Date, ByRef Vettore_Orari As Cls_Struttura_Orari, ByRef Vettore_Timbrature As Cls_Struttura_Timbrature, ByRef Vettore_Orari_Timbrature As Cls_Struttura_Orari_Timbrature, ByVal Giorno As Byte, ByVal SwGiorno As String) As String
        Dim cg As New Cls_Giustificativi
        Dim cct As New Cls_CausaliTimbrature
        Dim cpt As New Cls_ParametriTurni
        cpt.Leggi(ConnectionString)

        Dim gs As Byte

        AlleGiornoPrecedente = "00.00"

        If Giorno = 0 Then Giorno = 31
        If SwGiorno = "S" Then
            gs = Giorno
        Else
            gs = 0
        End If

        Dim x As Byte = 0
        Dim XR As Byte = 0

        For g = gs To Giorno
            For x = XR To 14
                Vettore_Orari_Timbrature.Dalle(x, g) = ""
                Vettore_Orari_Timbrature.Alle(x, g) = ""
                Vettore_Orari_Timbrature.Pausa(x, g) = ""
                Vettore_Orari_Timbrature.Causale(x, g) = ""
                Vettore_Orari_Timbrature.Colore(x, g) = vbWhite
                Vettore_Orari_Timbrature.Giustificativo(x, g) = ""
                Vettore_Orari_Timbrature.NelGruppo(x, g) = ""
                Vettore_Orari_Timbrature.Familiare(x, g) = 0
                Vettore_Orari_Timbrature.Tipo(x, g) = ""
                Vettore_Orari_Timbrature.Qualita(x, g) = ""
                Vettore_Orari_Timbrature.TipoServizio(x, g) = ""
                Vettore_Orari_Timbrature.GiornoSuccessivo(x, g) = ""
            Next x
            If TimbratureProfili = "S" _
              Or (Vettore_Orari.TipoFlessibilita(g) = "P" And (Vettore_Timbrature.Dalle(0, g) <> "" Or Vettore_Timbrature.Alle(0, g) <> "")) Then
                For x = 0 To 9
                    If Vettore_Orari.Dalle(x, g) = "00.00" And Vettore_Orari.Alle(x, g) = "00.00" And Vettore_Orari.Pausa(x, g) = "00.00" And Vettore_Orari.Giustificativo(x, g) = "" Then Exit For
                    Vettore_Orari_Timbrature.Dalle(x, g) = Vettore_Orari.Dalle(x, g)
                    Vettore_Orari_Timbrature.Alle(x, g) = Vettore_Orari.Alle(x, g)
                    Vettore_Orari_Timbrature.Pausa(x, g) = Vettore_Orari.Pausa(x, g)
                    Vettore_Orari_Timbrature.Giustificativo(x, g) = Vettore_Orari.Giustificativo(x, g)
                    Vettore_Orari_Timbrature.NelGruppo(x, g) = Vettore_Orari.NelGruppo(x, g)
                    Vettore_Orari_Timbrature.Familiare(x, g) = Vettore_Orari.Familiare(x, g)
                    Vettore_Orari_Timbrature.TipoServizio(x, g) = Vettore_Orari.TipoServizio(x, g)
                    Vettore_Orari_Timbrature.GiornoSuccessivo(x, g) = Vettore_Orari.GiornoSuccessivo(x, g)
                Next x
            Else
                x = XR
                XR = 0
                For t = 0 To 9
                    If Vettore_Timbrature.Dalle(t, g) = "" And Vettore_Timbrature.Dalle_Causale(t, g) = "" And Vettore_Timbrature.Alle(t, g) = "" And Vettore_Timbrature.Alle_Causale(t, g) = "" Then Exit For
                    Vettore_Orari_Timbrature.GiornoSuccessivo(x, g) = Vettore_Orari.GiornoSuccessivo(x, g)
                    If cg.CampoGiustificativi(ConnectionString, Vettore_Orari.Giustificativo(x, g), "TimbratureComeOrari") = "S" Then
                        Vettore_Orari_Timbrature.Dalle(x, g) = Vettore_Orari.Dalle(x, g)
                        Vettore_Orari_Timbrature.Alle(x, g) = Vettore_Orari.Alle(x, g)
                        Vettore_Orari_Timbrature.Pausa(x, g) = Vettore_Orari.Pausa(x, g)
                        Vettore_Orari_Timbrature.Giustificativo(x, g) = Vettore_Orari.Giustificativo(x, g)
                        Vettore_Orari_Timbrature.NelGruppo(x, g) = Vettore_Orari.NelGruppo(x, g)
                        Vettore_Orari_Timbrature.Familiare(x, g) = Vettore_Orari.Familiare(x, g)
                        Vettore_Orari_Timbrature.TipoServizio(x, g) = Vettore_Orari.TipoServizio(x, g)
                        x = x + 1
                    Else
                        Dim TipoCausaleDalle As String = ""
                        If Vettore_Timbrature.Dalle_Causale(t, g) <> "" Then
                            TipoCausaleDalle = cct.CampoCausaliTimbrature(ConnectionString, Vettore_Timbrature.Dalle_Causale(t, g), "Tipo")
                        End If
                        If Vettore_Timbrature.Dalle_Causale(t, g) <> "" And TipoCausaleDalle <> "I" Then
                            If TipoCausaleDalle = "P" Then
                                If Vettore_Timbrature.Dalle(t, g) = "" Then
                                    Vettore_Orari_Timbrature.Dalle(x, g) = "00.00"
                                Else
                                    Vettore_Orari_Timbrature.Dalle(x, g) = Vettore_Timbrature.Dalle(t, g)
                                End If
                                If Vettore_Timbrature.Alle(t, g) = "" Then
                                    Vettore_Orari_Timbrature.Alle(x, g) = "24.00"
                                Else
                                    Vettore_Orari_Timbrature.Alle(x, g) = Vettore_Timbrature.Alle(t, g)
                                End If
                                Vettore_Orari_Timbrature.Pausa(x, g) = "00.00"
                                Vettore_Orari_Timbrature.Causale(x, g) = Vettore_Timbrature.Dalle_Causale(t, g)
                                Vettore_Orari_Timbrature.Colore(x, g) = Vettore_Timbrature.Dalle_Colore(t, g)
                                If Vettore_Orari_Timbrature.Colore(x, g) = vbWhite Then
                                    Vettore_Orari_Timbrature.Colore(x, g) = Vettore_Timbrature.Alle_Colore(t, g)
                                End If
                                Vettore_Orari_Timbrature.Giustificativo(x, g) = cpt.GiustificativoOrarioLavoro
                                If Vettore_Orari_Timbrature.Dalle(x, g) = "00.00" Then
                                    If AlleGiornoPrecedente = "24.00" Then Vettore_Orari_Timbrature.GiornoSuccessivo(x, g) = "S"
                                End If
                                x = x + 1
                            End If
                            If TipoCausaleDalle = "A" Then
                                If t = 0 Then
                                    If Vettore_Timbrature.Dalle(t, g) <> "" Then
                                        Vettore_Orari_Timbrature.Dalle(x, g) = "00.00"
                                        Vettore_Orari_Timbrature.Alle(x, g) = Vettore_Timbrature.Dalle(t, g)
                                        Vettore_Orari_Timbrature.Pausa(x, g) = "00.00"
                                        Vettore_Orari_Timbrature.Causale(x, g) = Vettore_Timbrature.Dalle_Causale(t, g)
                                        Vettore_Orari_Timbrature.Colore(x, g) = Vettore_Timbrature.Dalle_Colore(t, g)
                                        Vettore_Orari_Timbrature.Giustificativo(x, g) = cpt.GiustificativoAssenza
                                        x = x + 1
                                    End If
                                Else
                                    Vettore_Orari_Timbrature.Dalle(x, g) = Vettore_Timbrature.Alle(t - 1, g)
                                    Vettore_Orari_Timbrature.Alle(x, g) = Vettore_Timbrature.Dalle(t, g)
                                    Vettore_Orari_Timbrature.Pausa(x, g) = "00.00"
                                    Vettore_Orari_Timbrature.Causale(x, g) = Vettore_Timbrature.Dalle_Causale(t, g)
                                    Vettore_Orari_Timbrature.Colore(x, g) = Vettore_Timbrature.Dalle_Colore(t, g)
                                    Vettore_Orari_Timbrature.Giustificativo(x, g) = cpt.GiustificativoAssenza
                                    x = x + 1
                                End If
                            End If
                            If Vettore_Timbrature.Alle_Causale(t, g) <> "" Then
                                If cct.CampoCausaliTimbrature(ConnectionString, Vettore_Timbrature.Alle_Causale(t, g), "Tipo") = "A" Then
                                    If Vettore_Timbrature.Alle(t, g) = "" Then
                                        Vettore_Orari_Timbrature.Dalle(x, g) = "24.00"
                                    Else
                                        Vettore_Orari_Timbrature.Dalle(x, g) = Vettore_Timbrature.Alle(t, g)
                                    End If
                                    If Vettore_Timbrature.Dalle(t + 1, g) = "" Then
                                        Vettore_Orari_Timbrature.Alle(x, g) = "24.00"
                                    Else
                                        Vettore_Orari_Timbrature.Alle(x, g) = Vettore_Timbrature.Dalle(t + 1, g)
                                    End If
                                    Vettore_Orari_Timbrature.Pausa(x, g) = "00.00"
                                    Vettore_Orari_Timbrature.Causale(x, g) = Vettore_Timbrature.Alle_Causale(t, g)
                                    Vettore_Orari_Timbrature.Colore(x, g) = Vettore_Timbrature.Alle_Colore(t, g)
                                    If Vettore_Orari_Timbrature.Colore(x, g) = vbWhite Then
                                        Vettore_Orari_Timbrature.Colore(x, g) = Vettore_Timbrature.Alle_Colore(t, g)
                                    End If
                                    Vettore_Orari_Timbrature.Giustificativo(x, g) = cpt.GiustificativoAssenza
                                    x = x + 1
                                End If
                            End If
                        Else
                            If Vettore_Timbrature.Dalle(t, g) = "" Then
                                Vettore_Orari_Timbrature.Dalle(x, g) = "00.00"
                            Else
                                Vettore_Orari_Timbrature.Dalle(x, g) = Vettore_Timbrature.Dalle(t, g)
                            End If
                            If Vettore_Timbrature.Alle(t, g) = "" Then
                                Vettore_Orari_Timbrature.Alle(x, g) = "24.00"
                            Else
                                Vettore_Orari_Timbrature.Alle(x, g) = Vettore_Timbrature.Alle(t, g)
                            End If
                            Vettore_Orari_Timbrature.Pausa(x, g) = "00.00"
                            Vettore_Orari_Timbrature.Causale(x, g) = ""
                            Vettore_Orari_Timbrature.Colore(x, g) = Vettore_Timbrature.Dalle_Colore(t, g)
                            If Vettore_Orari_Timbrature.Colore(x, g) = vbWhite Then
                                Vettore_Orari_Timbrature.Colore(x, g) = Vettore_Timbrature.Alle_Colore(t, g)
                            End If
                            Vettore_Orari_Timbrature.Giustificativo(x, g) = cpt.GiustificativoOrarioLavoro
                            If Vettore_Orari_Timbrature.Dalle(x, g) = "00.00" Then
                                If AlleGiornoPrecedente = "24.00" Then Vettore_Orari_Timbrature.GiornoSuccessivo(x, g) = "S"
                            End If
                            x = x + 1
                            If Vettore_Timbrature.Alle_Causale(t, g) <> "" Then
                                If cct.CampoCausaliTimbrature(ConnectionString, Vettore_Timbrature.Alle_Causale(t, g), "Tipo") = "P" Then
                                    If Vettore_Timbrature.Dalle(t, g) = "" Then
                                        Vettore_Orari_Timbrature.Dalle(x, g) = "00.00"
                                    Else
                                        Vettore_Orari_Timbrature.Dalle(x, g) = Vettore_Timbrature.Dalle(t, g)
                                    End If
                                    If Vettore_Timbrature.Alle(t, g) = "" Then
                                        Vettore_Orari_Timbrature.Alle(x, g) = "24.00"
                                    Else
                                        Vettore_Orari_Timbrature.Alle(x, g) = Vettore_Timbrature.Alle(t, g)
                                    End If
                                    Vettore_Orari_Timbrature.Pausa(x, g) = "00.00"
                                    Vettore_Orari_Timbrature.Causale(x, g) = Vettore_Timbrature.Alle_Causale(t, g)
                                    Vettore_Orari_Timbrature.Colore(x, g) = Vettore_Timbrature.Alle_Colore(t, g)
                                    Vettore_Orari_Timbrature.Giustificativo(x, g) = cpt.GiustificativoOrarioLavoro
                                    If Vettore_Orari_Timbrature.Dalle(x, g) = "00.00" Then
                                        If AlleGiornoPrecedente = "24.00" Then Vettore_Orari_Timbrature.GiornoSuccessivo(x, g) = "S"
                                    End If
                                    x = x + 1
                                End If
                                If cct.CampoCausaliTimbrature(ConnectionString, Vettore_Timbrature.Alle_Causale(t, g), "Tipo") = "A" Then
                                    If Vettore_Timbrature.Alle(t, g) = "" Then
                                        Vettore_Orari_Timbrature.Dalle(x, g) = "24.00"
                                    Else
                                        Vettore_Orari_Timbrature.Dalle(x, g) = Vettore_Timbrature.Alle(t, g)
                                    End If
                                    If Vettore_Timbrature.Dalle(t + 1, g) = "" Then
                                        Vettore_Orari_Timbrature.Alle(x, g) = "24.00"
                                    Else
                                        Vettore_Orari_Timbrature.Alle(x, g) = Vettore_Timbrature.Dalle(t + 1, g)
                                    End If
                                    Vettore_Orari_Timbrature.Pausa(x, g) = "00.00"
                                    Vettore_Orari_Timbrature.Causale(x, g) = Vettore_Timbrature.Alle_Causale(t, g)
                                    Vettore_Orari_Timbrature.Colore(x, g) = Vettore_Timbrature.Alle_Colore(t, g)
                                    If Vettore_Orari_Timbrature.Colore(x, g) = vbWhite Then
                                        Vettore_Orari_Timbrature.Colore(x, g) = Vettore_Timbrature.Alle_Colore(t, g)
                                    End If
                                    Vettore_Orari_Timbrature.Giustificativo(x, g) = cpt.GiustificativoAssenza
                                    x = x + 1
                                End If
                            End If
                        End If
                    End If
                Next t
                '
                ' Carica gli orari con Tipo giustificativo = "A" oppure = "I"
                '
                Risultato = CaricaOrariTipoAI(Dipendente, g, x, XR, "A", "", Vettore_Orari, Vettore_Orari_Timbrature)
                Risultato = CaricaOrariTipoAI(Dipendente, g, x, XR, "I", "", Vettore_Orari, Vettore_Orari_Timbrature)
            End If

            AlleGiornoPrecedente = "00.00"
            For i = 9 To 0 Step -1
                If Vettore_Orari.Alle(i, g) <> "00.00" Then
                    AlleGiornoPrecedente = Vettore_Orari.Alle(i, g)
                    Exit For
                End If
            Next i
        Next g
    End Function

    Private Sub OrdinaVettoreOrariTimbrature(ByRef Vettore_Orari_Timbrature As Cls_Struttura_Orari_Timbrature, ByVal Giorno As Byte)

        Dim W_Causale As String
        Dim W_Colore As Long
        Dim W_Qualita As String

        For g = 0 To Giorno
            Sw_Ok = False
            Do Until Sw_Ok = True
                Sw_Ok = True
                For i = 1 To 14
                    If Vettore_Orari_Timbrature.Dalle(i, g) <> "" Or Vettore_Orari_Timbrature.Alle(i, g) <> "" Or Vettore_Orari_Timbrature.Pausa(i, g) <> "" Then
                        If Vettore_Orari_Timbrature.Dalle(i, g) < Vettore_Orari_Timbrature.Dalle(i - 1, g) Then
                            Sw_Ok = False
                            W_Dalle = Vettore_Orari_Timbrature.Dalle(i, g)
                            W_Alle = Vettore_Orari_Timbrature.Alle(i, g)
                            W_Pausa = Vettore_Orari_Timbrature.Pausa(i, g)
                            W_Causale = Vettore_Orari_Timbrature.Causale(i, g)
                            W_Colore = Vettore_Orari_Timbrature.Colore(i, g)
                            W_Giustificativo = Vettore_Orari_Timbrature.Giustificativo(i, g)
                            W_NelGruppo = Vettore_Orari_Timbrature.NelGruppo(i, g)
                            W_Familiare = Vettore_Orari_Timbrature.Familiare(i, g)
                            W_Tipo = Vettore_Orari_Timbrature.Tipo(i, g)
                            W_Qualita = Vettore_Orari_Timbrature.Qualita(i, g)
                            W_TipoServizio = Vettore_Orari_Timbrature.TipoServizio(i, g)
                            W_GiornoSuccessivo = Vettore_Orari_Timbrature.GiornoSuccessivo(i, g)
                            Vettore_Orari_Timbrature.Dalle(i, g) = Vettore_Orari_Timbrature.Dalle(i - 1, g)
                            Vettore_Orari_Timbrature.Alle(i, g) = Vettore_Orari_Timbrature.Alle(i - 1, g)
                            Vettore_Orari_Timbrature.Pausa(i, g) = Vettore_Orari_Timbrature.Pausa(i - 1, g)
                            Vettore_Orari_Timbrature.Causale(i, g) = Vettore_Orari_Timbrature.Causale(i - 1, g)
                            Vettore_Orari_Timbrature.Colore(i, g) = Vettore_Orari_Timbrature.Colore(i - 1, g)
                            Vettore_Orari_Timbrature.Giustificativo(i, g) = Vettore_Orari_Timbrature.Giustificativo(i - 1, g)
                            Vettore_Orari_Timbrature.NelGruppo(i, g) = Vettore_Orari_Timbrature.NelGruppo(i - 1, g)
                            Vettore_Orari_Timbrature.Familiare(i, g) = Vettore_Orari_Timbrature.Familiare(i - 1, g)
                            Vettore_Orari_Timbrature.Tipo(i, g) = Vettore_Orari_Timbrature.Tipo(i - 1, g)
                            Vettore_Orari_Timbrature.Qualita(i, g) = Vettore_Orari_Timbrature.Qualita(i - 1, g)
                            Vettore_Orari_Timbrature.TipoServizio(i, g) = Vettore_Orari_Timbrature.TipoServizio(i - 1, g)
                            Vettore_Orari_Timbrature.GiornoSuccessivo(i, g) = Vettore_Orari_Timbrature.GiornoSuccessivo(i - 1, g)
                            Vettore_Orari_Timbrature.Dalle(i - 1, g) = W_Dalle
                            Vettore_Orari_Timbrature.Alle(i - 1, g) = W_Alle
                            Vettore_Orari_Timbrature.Pausa(i - 1, g) = W_Pausa
                            Vettore_Orari_Timbrature.Causale(i - 1, g) = W_Causale
                            Vettore_Orari_Timbrature.Colore(i - 1, g) = W_Colore
                            Vettore_Orari_Timbrature.Giustificativo(i - 1, g) = W_Giustificativo
                            Vettore_Orari_Timbrature.NelGruppo(i - 1, g) = W_NelGruppo
                            Vettore_Orari_Timbrature.Familiare(i - 1, g) = W_Familiare
                            Vettore_Orari_Timbrature.Tipo(i - 1, g) = W_Tipo
                            Vettore_Orari_Timbrature.Qualita(i - 1, g) = W_Qualita
                            Vettore_Orari_Timbrature.TipoServizio(i - 1, g) = W_TipoServizio
                            Vettore_Orari_Timbrature.GiornoSuccessivo(i - 1, g) = W_GiornoSuccessivo
                            i = i - 1
                        End If
                    Else
                        Exit For
                    End If
                Next i
            Loop
        Next g
    End Sub

    Private Sub Controllo_CausaliSuVettoreOrariTimbrature(ByRef Vettore_Orari_Timbrature As Cls_Struttura_Orari_Timbrature, ByVal Giorno As Byte)
        Dim cct As New Cls_CausaliTimbrature

        Dim Sw_Zero As Boolean
        Dim g As Integer
        Dim i As Integer

        Sw_Zero = False
        For g = Giorno To 0 Step -1
            For i = 14 To 1 Step -1
                If Vettore_Orari_Timbrature.Dalle(i, g) <> "" Or Vettore_Orari_Timbrature.Alle(i, g) <> "" Or Vettore_Orari_Timbrature.Pausa(i, g) <> "" Then
                    If Sw_Zero = True Then
                        Sw_Zero = False
                        If Vettore_Orari_Timbrature.Causale(i, g) <> "" Then
                            If Vettore_Orari_Timbrature.Alle(i, g) = "24.00" Then
                                If Vettore_Orari_Timbrature.Causale(i, g) = Vettore_Orari_Timbrature.Causale(0, g + 1) Then
                                    If cct.CampoCausaliTimbrature(ConnectionString, Vettore_Orari_Timbrature.Causale(i, g), "Propagazione") = "N" Then
                                        Vettore_Orari_Timbrature.Causale(0, g + 1) = ""
                                    End If
                                End If
                            Else
                                If Vettore_Orari_Timbrature.Dalle(i, g) = Vettore_Orari_Timbrature.Alle(i - 1, g) Then
                                    If Vettore_Orari_Timbrature.Causale(i, g) = Vettore_Orari_Timbrature.Causale(i - 1, g) Then
                                        If cct.CampoCausaliTimbrature(ConnectionString, Vettore_Orari_Timbrature.Causale(i, g), "Propagazione") = "N" Then
                                            Vettore_Orari_Timbrature.Causale(i, g) = ""
                                        End If
                                    End If
                                End If
                            End If
                        End If
                    Else
                        If Vettore_Orari_Timbrature.Causale(i, g) <> "" Then
                            If Vettore_Orari_Timbrature.Dalle(i, g) = Vettore_Orari_Timbrature.Alle(i - 1, g) Then
                                If Vettore_Orari_Timbrature.Causale(i, g) = Vettore_Orari_Timbrature.Causale(i - 1, g) Then
                                    If cct.CampoCausaliTimbrature(ConnectionString, Vettore_Orari_Timbrature.Causale(i, g), "Propagazione") = "N" Then
                                        Vettore_Orari_Timbrature.Causale(i, g) = ""
                                    End If
                                End If
                            End If
                        End If
                    End If
                End If
            Next i
            If Vettore_Orari_Timbrature.Dalle(i, g) = "00.00" Then Sw_Zero = True
        Next g
    End Sub

    Private Function ElaboraCausaliTimbrature(ByVal Dipendente As Long, ByVal Anno As Integer, ByVal Mese As Byte, ByVal Giorno As Byte, ByVal CodiceSuperGruppo As String, ByVal CodiceContratto As String, ByRef Vettore_Orari_Timbrature As Cls_Struttura_Orari_Timbrature, ByVal SwGiorno As String) As String
        Dim Sc As New MSScriptControl.ScriptControl
        Sc.Language = "VBScript"
        Sc.UseSafeSubset = False
        Dim MyDate As Date
        Dim g As Byte
        Dim a As Boolean

        If SwGiorno = "S" Then
            MyDate = DateSerial(Anno, Mese, Giorno)
        Else
            MyDate = DateSerial(Anno, Mese, 0)
        End If

        g = 0
        Do While Anno > Year(MyDate) Or (Anno = Year(MyDate) And Mese >= Month(MyDate))
            If Mese = Month(MyDate) Then
                If Giorno < Day(MyDate) Then Exit Do
                g = Day(MyDate)
            End If
            ' Elabora Vettore_Orari_Timbrature con Causale <> 0
            For i = 0 To 14
                If Vettore_Orari_Timbrature.Dalle(i, g) = "" And Vettore_Orari_Timbrature.Alle(i, g) = "" Then Exit For
                If Vettore_Orari_Timbrature.Causale(i, g) <> "" Then
                    a = Esegui_CalcoloCausaliTimbrature(Dipendente, Vettore_Orari_Timbrature.Causale(i, g), Anno, Mese, g, i, CodiceSuperGruppo, CodiceContratto, Vettore_Orari_Timbrature)
                End If
            Next i

            MyDate = DateAdd("d", 1, MyDate)
        Loop
    End Function

    Private Function Esegui_CalcoloCausaliTimbrature(ByVal Dipendente As Long, ByVal RegolaCalcolo As String, ByVal Anno As Integer, ByVal Mese As Byte, ByVal Giorno As Byte, ByVal i As Long, ByVal CodiceSuperGruppo As String, ByVal CodiceContratto As String, ByRef Vettore_Orari_Timbrature As Cls_Struttura_Orari_Timbrature) As Boolean
        Dim cxst As New Cls_ClsXScriptTurni
        cxst.ConnectionString = ConnectionString
        cxst.Utente = Utente

        Dim Sc As New MSScriptControl.ScriptControl
        Sc.Language = "VBScript"
        Sc.UseSafeSubset = False
        Dim MyDate As Date
        Dim g As Byte
        Dim MyScript As String
        Dim MyValori As String
        Dim ii As Integer
        Dim a As String

        Esegui_CalcoloCausaliTimbrature = False
        MyDate = DateSerial(Anno, Mese, Giorno)
        g = Giorno

        MyScript = ""
        MyValori = ""
        For ii = 0 To 100
            If Val(CodiceFormulaCT(ii)) = 0 Then
                Exit For
            End If
            If CodiceFormulaCT(ii) = RegolaCalcolo Then
                If Format(DataFormulaCT(ii), "yyyyMMdd") <= Format(MyDate, "yyyyMMdd") Then
                    MyScript = ScriptFormulaCT(ii)
                    MyValori = ValoriFormulaCT(ii)
                    Exit For
                End If
            End If
        Next ii

        If MyScript = "" Then
            Dim cn As OleDbConnection
            cn = New Data.OleDb.OleDbConnection(ConnectionString)
            cn.Open()

            Dim cmd As New OleDbCommand()
            cmd.CommandText = "SELECT * FROM FormuleCausaliTimbrature WHERE Codice = ? ORDER BY Validita DESC"
            cmd.Parameters.AddWithValue("@Codice", RegolaCalcolo)

            cmd.Connection = cn
            Dim myPOSTreader As OleDbDataReader = cmd.ExecuteReader()
            Do While myPOSTreader.Read
                CodiceFormulaCT(ii) = StringaDb(myPOSTreader.Item("Codice"))
                DataFormulaCT(ii) = DataDb(myPOSTreader.Item("Validita"))
                ScriptFormulaCT(ii) = StringaDb(myPOSTreader.Item("CodiceScript"))
                ValoriFormulaCT(ii) = StringaDb(myPOSTreader.Item("ValoriPerExecute"))
                ii = ii + 1
            Loop
            myPOSTreader.Close()
            cmd.Parameters.Clear()
            cn.Close()

            For Kii = 0 To ii
                If CodiceFormulaCT(Kii) = RegolaCalcolo Then
                    If Format(DataFormulaCT(Kii), "yyyyMMdd") <= Format(MyDate, "yyyyMMdd") Then
                        MyScript = ScriptFormulaCT(Kii)
                        MyValori = ValoriFormulaCT(Kii)
                        Exit For
                    End If
                End If
            Next Kii
        End If

        If MyScript = "" Or IsDBNull(MyScript) Then Exit Function

        Sc.Reset()
        Sc.AddObject("Form", cxst)
        Sc.AddCode(MyScript)

        If InStr(MyValori, "13,") > 0 Then Sc.ExecuteStatement("Dipendente = " & Dipendente)

        If InStr(MyValori, "04,") > 0 Then
            If IsNumeric(Variabile(1)) Then
                Sc.ExecuteStatement("Variabile1 = " & cxst.Sostituisci(Variabile(1), ",", "."))
            Else
                If IsDate(Variabile(1)) Then
                    Sc.ExecuteStatement("Variabile1 = #" & Format(Variabile(1), "MM/dd/yyyy") & "#")
                Else
                    Sc.ExecuteStatement("Variabile1 = """ & Variabile(1) & """")
                End If
            End If
        End If
        If InStr(MyValori, "05,") > 0 Then
            If IsNumeric(Variabile(2)) Then
                Sc.ExecuteStatement("Variabile2 = " & cxst.Sostituisci(Variabile(2), ",", "."))
            Else
                If IsDate(Variabile(2)) Then
                    Sc.ExecuteStatement("Variabile2 = #" & Format(Variabile(2), "MM/dd/yyyy") & "#")
                Else
                    Sc.ExecuteStatement("Variabile2 = """ & Variabile(2) & """")
                End If
            End If
        End If
        If InStr(MyValori, "06,") > 0 Then
            If IsNumeric(Variabile(3)) Then
                Sc.ExecuteStatement("Variabile3 = " & cxst.Sostituisci(Variabile(3), ",", "."))
            Else
                If IsDate(Variabile(3)) Then
                    Sc.ExecuteStatement("Variabile3 = #" & Format(Variabile(3), "MM/dd/yyyy") & "#")
                Else
                    Sc.ExecuteStatement("Variabile3 = """ & Variabile(3) & """")
                End If
            End If
        End If
        If InStr(MyValori, "07,") > 0 Then
            If IsNumeric(Variabile(4)) Then
                Sc.ExecuteStatement("Variabile4 = " & cxst.Sostituisci(Variabile(4), ",", "."))
            Else
                If IsDate(Variabile(4)) Then
                    Sc.ExecuteStatement("Variabile4 = #" & Format(Variabile(4), "MM/dd/yyyy") & "#")
                Else
                    Sc.ExecuteStatement("Variabile4 = """ & Variabile(4) & """")
                End If
            End If
        End If
        If InStr(MyValori, "08,") > 0 Then
            If IsNumeric(Variabile(5)) Then
                Sc.ExecuteStatement("Variabile5 = " & cxst.Sostituisci(Variabile(5), ",", "."))
            Else
                If IsDate(Variabile(5)) Then
                    Sc.ExecuteStatement("Variabile5 = #" & Format(Variabile(5), "MM/dd/yyyy") & "#")
                Else
                    Sc.ExecuteStatement("Variabile5 = """ & Variabile(5) & """")
                End If
            End If
        End If
        If InStr(MyValori, "09,") > 0 Then
            If IsNumeric(Variabile(6)) Then
                Sc.ExecuteStatement("Variabile6 = " & cxst.Sostituisci(Variabile(6), ",", "."))
            Else
                If IsDate(Variabile(6)) Then
                    Sc.ExecuteStatement("Variabile6 = #" & Format(Variabile(6), "MM/dd/yyyy") & "#")
                Else
                    Sc.ExecuteStatement("Variabile6 = """ & Variabile(6) & """")
                End If
            End If
        End If
        If InStr(MyValori, "10,") > 0 Then
            If IsNumeric(Variabile(7)) Then
                Sc.ExecuteStatement("Variabile7 = " & cxst.Sostituisci(Variabile(7), ",", "."))
            Else
                If IsDate(Variabile(7)) Then
                    Sc.ExecuteStatement("Variabile7 = #" & Format(Variabile(7), "MM/dd/yyyy") & "#")
                Else
                    Sc.ExecuteStatement("Variabile7 = """ & Variabile(7) & """")
                End If
            End If
        End If
        If InStr(MyValori, "11,") > 0 Then
            If IsNumeric(Variabile(8)) Then
                Sc.ExecuteStatement("Variabile8 = " & cxst.Sostituisci(Variabile(8), ",", "."))
            Else
                If IsDate(Variabile(8)) Then
                    Sc.ExecuteStatement("Variabile8 = #" & Format(Variabile(8), "MM/dd/yyyy") & "#")
                Else
                    Sc.ExecuteStatement("Variabile8 = """ & Variabile(8) & """")
                End If
            End If
        End If
        If InStr(MyValori, "12,") > 0 Then
            If IsNumeric(Variabile(9)) Then
                Sc.ExecuteStatement("Variabile9 = " & cxst.Sostituisci(Variabile(9), ",", "."))
            Else
                If IsDate(Variabile(9)) Then
                    Sc.ExecuteStatement("Variabile9 = #" & Format(Variabile(9), "MM/dd/yyyy") & "#")
                Else
                    Sc.ExecuteStatement("Variabile9 = """ & Variabile(9) & """")
                End If
            End If
        End If

        If g = 0 Then
            If i = 0 Then
                If InStr(MyValori, "18,") > 0 Then Sc.ExecuteStatement("Pre_Dalle = """"")
                If InStr(MyValori, "15,") > 0 Then Sc.ExecuteStatement("Pre_Alle = """"")
                If InStr(MyValori, "21,") > 0 Then Sc.ExecuteStatement("Pre_Causale = 0")
                If InStr(MyValori, "38,") > 0 Then Sc.ExecuteStatement("Pre_Colore = " & vbWhite)
                If InStr(MyValori, "24,") > 0 Then Sc.ExecuteStatement("Pre_Giustificativo = """"")
                If InStr(MyValori, "40,") > 0 Then Sc.ExecuteStatement("Pre_NelGruppo = """"")
                If InStr(MyValori, "43,") > 0 Then Sc.ExecuteStatement("Pre_Familiare = 0")
            Else
                'If InStr(MyValori, "18,") > 0 Then Sc.ExecuteStatement("Pre_Dalle = " & Format(Vettore_Orari_Timbrature.Dalle(i - 1, g), "HH.mm"))
                'If InStr(MyValori, "15,") > 0 Then Sc.ExecuteStatement("Pre_Alle = " & Format(Vettore_Orari_Timbrature.Alle(i - 1, g), "HH.mm"))
                If InStr(MyValori, "18,") > 0 Then Sc.ExecuteStatement("Pre_Dalle = " & Vettore_Orari_Timbrature.Dalle(i - 1, g))
                If InStr(MyValori, "15,") > 0 Then Sc.ExecuteStatement("Pre_Alle = " & Vettore_Orari_Timbrature.Alle(i - 1, g))
                If InStr(MyValori, "21,") > 0 Then Sc.ExecuteStatement("Pre_Causale = """ & Vettore_Orari_Timbrature.Causale(i - 1, g) & """")
                If InStr(MyValori, "38,") > 0 Then Sc.ExecuteStatement("Pre_Colore = " & Vettore_Orari_Timbrature.Colore(i - 1, g))
                If InStr(MyValori, "24,") > 0 Then Sc.ExecuteStatement("Pre_Giustificativo = """ & Vettore_Orari_Timbrature.Giustificativo(i - 1, g) & """")
                If InStr(MyValori, "40,") > 0 Then Sc.ExecuteStatement("Pre_NelGruppo = """ & Vettore_Orari_Timbrature.NelGruppo(i - 1, g) & """")
                If InStr(MyValori, "43,") > 0 Then Sc.ExecuteStatement("Pre_Familiare = """ & Vettore_Orari_Timbrature.Familiare(i - 1, g) & """")
            End If
        Else
            If i = 0 Then
                For ii = 14 To 1
                    If Vettore_Orari_Timbrature.Dalle(ii, g - 1) <> "" Or Vettore_Orari_Timbrature.Alle(ii, g - 1) <> "" Then Exit For
                Next ii
                'If InStr(MyValori, "18,") > 0 Then Sc.ExecuteStatement("Pre_Dalle = " & Format(Vettore_Orari_Timbrature.Dalle(ii, g - 1), "HH.mm"))
                'If InStr(MyValori, "15,") > 0 Then Sc.ExecuteStatement("Pre_Alle = " & Format(Vettore_Orari_Timbrature.Alle(ii, g - 1), "HH.mm"))
                If InStr(MyValori, "18,") > 0 Then Sc.ExecuteStatement("Pre_Dalle = " & Vettore_Orari_Timbrature.Dalle(ii, g - 1))
                If InStr(MyValori, "15,") > 0 Then Sc.ExecuteStatement("Pre_Alle = " & Vettore_Orari_Timbrature.Alle(ii, g - 1))
                If InStr(MyValori, "21,") > 0 Then Sc.ExecuteStatement("Pre_Causale = """ & Vettore_Orari_Timbrature.Causale(ii, g - 1) & """")
                If InStr(MyValori, "38,") > 0 Then Sc.ExecuteStatement("Pre_Colore = " & Vettore_Orari_Timbrature.Colore(ii, g - 1))
                If InStr(MyValori, "24,") > 0 Then Sc.ExecuteStatement("Pre_Giustificativo = """ & Vettore_Orari_Timbrature.Giustificativo(ii, g - 1) & """")
                If InStr(MyValori, "40,") > 0 Then Sc.ExecuteStatement("Pre_NelGruppo = """ & Vettore_Orari_Timbrature.NelGruppo(ii, g - 1) & """")
                If InStr(MyValori, "43,") > 0 Then Sc.ExecuteStatement("Pre_Familiare = """ & Vettore_Orari_Timbrature.Familiare(ii, g - 1) & """")
            Else
                'If InStr(MyValori, "18,") > 0 Then Sc.ExecuteStatement("Pre_Dalle = " & Format(Vettore_Orari_Timbrature.Dalle(i - 1, g), "HH.mm"))
                'If InStr(MyValori, "15,") > 0 Then Sc.ExecuteStatement("Pre_Alle = " & Format(Vettore_Orari_Timbrature.Alle(i - 1, g), "HH.mm"))
                If InStr(MyValori, "18,") > 0 Then Sc.ExecuteStatement("Pre_Dalle = " & Vettore_Orari_Timbrature.Dalle(i - 1, g))
                If InStr(MyValori, "15,") > 0 Then Sc.ExecuteStatement("Pre_Alle = " & Vettore_Orari_Timbrature.Alle(i - 1, g))
                If InStr(MyValori, "21,") > 0 Then Sc.ExecuteStatement("Pre_Causale = """ & Vettore_Orari_Timbrature.Causale(i - 1, g) & """")
                If InStr(MyValori, "38,") > 0 Then Sc.ExecuteStatement("Pre_Colore = " & Vettore_Orari_Timbrature.Colore(i - 1, g))
                If InStr(MyValori, "24,") > 0 Then Sc.ExecuteStatement("Pre_Giustificativo = """ & Vettore_Orari_Timbrature.Giustificativo(i - 1, g) & """")
                If InStr(MyValori, "41,") > 0 Then Sc.ExecuteStatement("Pre_NelGruppo = """ & Vettore_Orari_Timbrature.NelGruppo(i - 1, g) & """")
                If InStr(MyValori, "44,") > 0 Then Sc.ExecuteStatement("Pre_Familiare = """ & Vettore_Orari_Timbrature.Familiare(i - 1, g) & """")
            End If
        End If
        'If InStr(MyValori, "17,") > 0 Then Sc.ExecuteStatement("Dalle = " & Format(Vettore_Orari_Timbrature.Dalle(i, g), "HH.mm"))
        'If InStr(MyValori, "14,") > 0 Then Sc.ExecuteStatement("Alle = " & Format(Vettore_Orari_Timbrature.Alle(i, g), "HH.mm"))
        If InStr(MyValori, "17,") > 0 Then Sc.ExecuteStatement("Dalle = " & Vettore_Orari_Timbrature.Dalle(i, g))
        If InStr(MyValori, "14,") > 0 Then Sc.ExecuteStatement("Alle = " & Vettore_Orari_Timbrature.Alle(i, g))
        If InStr(MyValori, "20,") > 0 Then Sc.ExecuteStatement("Causale = """ & Vettore_Orari_Timbrature.Causale(i, g) & """")
        If InStr(MyValori, "37,") > 0 Then Sc.ExecuteStatement("Colore = " & Vettore_Orari_Timbrature.Colore(i, g))
        If InStr(MyValori, "23,") > 0 Then Sc.ExecuteStatement("Giustificativo = """ & Vettore_Orari_Timbrature.Giustificativo(i, g) & """")
        If InStr(MyValori, "40,") > 0 Then Sc.ExecuteStatement("NelGruppo = """ & Vettore_Orari_Timbrature.NelGruppo(i, g) & """")
        If InStr(MyValori, "43,") > 0 Then Sc.ExecuteStatement("Familiare = """ & Vettore_Orari_Timbrature.Familiare(i, g) & """")
        If i < 14 Then
            If Vettore_Orari_Timbrature.Dalle(i + 1, g) <> "" Or Vettore_Orari_Timbrature.Alle(i + 1, g) <> "" Then
                'If InStr(MyValori, "19,") > 0 Then Sc.ExecuteStatement("Suc_Dalle = " & Format(Vettore_Orari_Timbrature.Dalle(i + 1, g), "HH.mm"))
                'If InStr(MyValori, "16,") > 0 Then Sc.ExecuteStatement("Suc_Alle = " & Format(Vettore_Orari_Timbrature.Alle(i + 1, g), "HH.mm"))
                If InStr(MyValori, "19,") > 0 Then Sc.ExecuteStatement("Suc_Dalle = " & Vettore_Orari_Timbrature.Dalle(i + 1, g))
                If InStr(MyValori, "16,") > 0 Then Sc.ExecuteStatement("Suc_Alle = " & Vettore_Orari_Timbrature.Alle(i + 1, g))
                If InStr(MyValori, "22,") > 0 Then Sc.ExecuteStatement("Suc_Causale = """ & Vettore_Orari_Timbrature.Causale(i + 1, g) & """")
                If InStr(MyValori, "39,") > 0 Then Sc.ExecuteStatement("Suc_Colore = " & Vettore_Orari_Timbrature.Colore(i + 1, g))
                If InStr(MyValori, "25,") > 0 Then Sc.ExecuteStatement("Suc_Giustificativo = """ & Vettore_Orari_Timbrature.Giustificativo(i + 1, g) & """")
                If InStr(MyValori, "42,") > 0 Then Sc.ExecuteStatement("Suc_NelGruppo = """ & Vettore_Orari_Timbrature.NelGruppo(i + 1, g) & """")
                If InStr(MyValori, "45,") > 0 Then Sc.ExecuteStatement("Suc_Familiare = """ & Vettore_Orari_Timbrature.Familiare(i + 1, g) & """")
            Else
                If g = 31 Then
                    If InStr(MyValori, "19,") > 0 Then Sc.ExecuteStatement("Suc_Dalle = """"")
                    If InStr(MyValori, "16,") > 0 Then Sc.ExecuteStatement("Suc_Alle = """"")
                    If InStr(MyValori, "22,") > 0 Then Sc.ExecuteStatement("Suc_Causale = 0")
                    If InStr(MyValori, "39,") > 0 Then Sc.ExecuteStatement("Suc_Colore = " & vbWhite)
                    If InStr(MyValori, "25,") > 0 Then Sc.ExecuteStatement("Suc_Giustificativo = """"")
                    If InStr(MyValori, "42,") > 0 Then Sc.ExecuteStatement("Suc_NelGruppo = """"")
                    If InStr(MyValori, "45,") > 0 Then Sc.ExecuteStatement("Suc_Familiare = 0")
                Else
                    'If InStr(MyValori, "19,") > 0 Then Sc.ExecuteStatement("Suc_Dalle = " & Format(Vettore_Orari_Timbrature.Dalle(0, g + 1), "HH.mm"))
                    'If InStr(MyValori, "16,") > 0 Then Sc.ExecuteStatement("Suc_Alle = " & Format(Vettore_Orari_Timbrature.Alle(0, g + 1), "HH.mm"))
                    If InStr(MyValori, "19,") > 0 Then Sc.ExecuteStatement("Suc_Dalle = " & Vettore_Orari_Timbrature.Dalle(0, g + 1))
                    If InStr(MyValori, "16,") > 0 Then Sc.ExecuteStatement("Suc_Alle = " & Vettore_Orari_Timbrature.Alle(0, g + 1))
                    If InStr(MyValori, "22,") > 0 Then Sc.ExecuteStatement("Suc_Causale = """ & Vettore_Orari_Timbrature.Causale(0, g + 1) & """")
                    If InStr(MyValori, "39,") > 0 Then Sc.ExecuteStatement("Suc_Colore = " & Vettore_Orari_Timbrature.Colore(0, g + 1))
                    If InStr(MyValori, "25,") > 0 Then Sc.ExecuteStatement("Suc_Giustificativo = """ & Vettore_Orari_Timbrature.Giustificativo(0, g + 1) & """")
                    If InStr(MyValori, "42,") > 0 Then Sc.ExecuteStatement("Suc_NelGruppo = """ & Vettore_Orari_Timbrature.NelGruppo(0, g + 1) & """")
                    If InStr(MyValori, "45,") > 0 Then Sc.ExecuteStatement("Suc_Familiare = """ & Vettore_Orari_Timbrature.Familiare(0, g + 1) & """")
                End If
            End If
        Else
            If g = 31 Then
                If InStr(MyValori, "19,") > 0 Then Sc.ExecuteStatement("Suc_Dalle = """"")
                If InStr(MyValori, "16,") > 0 Then Sc.ExecuteStatement("Suc_Alle = """"")
                If InStr(MyValori, "22,") > 0 Then Sc.ExecuteStatement("Suc_Causale = 0")
                If InStr(MyValori, "39,") > 0 Then Sc.ExecuteStatement("Suc_Colore = " & vbWhite)
                If InStr(MyValori, "25,") > 0 Then Sc.ExecuteStatement("Suc_Giustificativo = """"")
                If InStr(MyValori, "42,") > 0 Then Sc.ExecuteStatement("Suc_NelGruppo = """"")
                If InStr(MyValori, "45,") > 0 Then Sc.ExecuteStatement("Suc_Familiare = 0")
            Else
                'If InStr(MyValori, "19,") > 0 Then Sc.ExecuteStatement("Suc_Dalle = " & Format(Vettore_Orari_Timbrature.Dalle(0, g + 1), "HH.mm"))
                'If InStr(MyValori, "16,") > 0 Then Sc.ExecuteStatement("Suc_Alle = " & Format(Vettore_Orari_Timbrature.Alle(0, g + 1), "HH.mm"))
                If InStr(MyValori, "19,") > 0 Then Sc.ExecuteStatement("Suc_Dalle = " & Vettore_Orari_Timbrature.Dalle(0, g + 1))
                If InStr(MyValori, "16,") > 0 Then Sc.ExecuteStatement("Suc_Alle = " & Vettore_Orari_Timbrature.Alle(0, g + 1))
                If InStr(MyValori, "22,") > 0 Then Sc.ExecuteStatement("Suc_Causale = """ & Vettore_Orari_Timbrature.Causale(0, g + 1) & """")
                If InStr(MyValori, "39,") > 0 Then Sc.ExecuteStatement("Suc_Colore = " & Vettore_Orari_Timbrature.Colore(0, g + 1))
                If InStr(MyValori, "25,") > 0 Then Sc.ExecuteStatement("Suc_Giustificativo = """ & Vettore_Orari_Timbrature.Giustificativo(0, g + 1) & """")
                If InStr(MyValori, "42,") > 0 Then Sc.ExecuteStatement("Suc_NelGruppo = """ & Vettore_Orari_Timbrature.NelGruppo(0, g + 1) & """")
                If InStr(MyValori, "45,") > 0 Then Sc.ExecuteStatement("Suc_Familiare = """ & Vettore_Orari_Timbrature.Familiare(0, g + 1) & """")
            End If
        End If
        If InStr(MyValori, "01,") > 0 Then Sc.ExecuteStatement("Oggi = #" & Format(MyDate, "MM/dd/yyyy") & "#")
        If InStr(MyValori, "26,") > 0 Then Sc.ExecuteStatement("Giorno = """ & GiornoSettimana(Weekday(MyDate)) & """")
        If InStr(MyValori, "27,") > 0 Then Sc.ExecuteStatement("GiornoPrecedente = """ & GiornoSettimana(Weekday(DateAdd("d", -1, MyDate))) & """")
        If InStr(MyValori, "28,") > 0 Then Sc.ExecuteStatement("GiornoSuccessivo = """ & GiornoSettimana(Weekday(DateAdd("d", 1, MyDate))) & """")
        If InStr(MyValori, "34,") > 0 Then Sc.ExecuteStatement("FestivitaSettimanale = """ & GiornoSettimana(FestivitaSettimanale) & """")
        If InStr(MyValori, "35,") > 0 Then Sc.ExecuteStatement("GiornoPrecedenteFestivitaSettimanale = """ & GiornoSettimana(GiornoPrecedenteFestivitaSettimanale) & """")
        If InStr(MyValori, "36,") > 0 Then Sc.ExecuteStatement("GiornoSuccessivoFestivitaSettimanale = """ & GiornoSettimana(GiornoSuccessivoFestivitaSettimanale) & """")
        If InStr(MyValori, "29,") > 0 Then
            If cxst.GiornoFestivo(DateAdd("d", -1, MyDate), CodiceSuperGruppo) = True Then
                Sc.ExecuteStatement("GiornoPrecedenteFestivo = ""S""")
            Else
                Sc.ExecuteStatement("GiornoPrecedenteFestivo = ""N""")
            End If
        End If
        If InStr(MyValori, "30,") > 0 Then
            If cxst.GiornoFestivo(MyDate, CodiceSuperGruppo) = True Then
                Sc.ExecuteStatement("GiornoFestivo = ""S""")
            Else
                Sc.ExecuteStatement("GiornoFestivo = ""N""")
            End If
        End If
        If InStr(MyValori, "31,") > 0 Then
            If cxst.GiornoFestivo(DateAdd("d", 1, MyDate), CodiceSuperGruppo) = True Then
                Sc.ExecuteStatement("GiornoSuccessivoFestivo = ""S""")
            Else
                Sc.ExecuteStatement("GiornoSuccessivoFestivo = ""N""")
            End If
        End If
        If InStr(MyValori, "33,") > 0 Then
            Sc.ExecuteStatement("GiornoPartime = """ & Giorno_Partime(MyDate, CodiceContratto) & """")
        End If
        If InStr(MyValori, "32,") > 0 Then Sc.ExecuteStatement("UltimoGiorno = " & GiorniMese(Month(MyDate), Year(MyDate)))
        If InStr(MyValori, "02,") > 0 Then Sc.ExecuteStatement("Mese = " & Month(MyDate))
        If InStr(MyValori, "03,") > 0 Then Sc.ExecuteStatement("Anno = " & Year(MyDate))

        a = Sc.Run("Calcolo")

        If InStr(MyValori, "04,") > 0 Then Variabile(1) = Sc.Eval("Variabile1")
        If InStr(MyValori, "05,") > 0 Then Variabile(2) = Sc.Eval("Variabile2")
        If InStr(MyValori, "06,") > 0 Then Variabile(3) = Sc.Eval("Variabile3")
        If InStr(MyValori, "07,") > 0 Then Variabile(4) = Sc.Eval("Variabile4")
        If InStr(MyValori, "08,") > 0 Then Variabile(5) = Sc.Eval("Variabile5")
        If InStr(MyValori, "09,") > 0 Then Variabile(6) = Sc.Eval("Variabile6")
        If InStr(MyValori, "10,") > 0 Then Variabile(7) = Sc.Eval("Variabile7")
        If InStr(MyValori, "11,") > 0 Then Variabile(8) = Sc.Eval("Variabile8")
        If InStr(MyValori, "12,") > 0 Then Variabile(9) = Sc.Eval("Variabile9")


        If g = 0 Then
            If i > 0 Then
                If InStr(MyValori, "18,") > 0 Then
                    Numero = Sc.Eval("Pre_Dalle")
                    Vettore_Orari_Timbrature.Dalle(i - 1, g) = Format(TimeSerial(Int(Numero), (Numero - Int(Numero)) * 100, 0), "HH.mm")
                End If
                If InStr(MyValori, "15,") > 0 Then
                    Numero = Sc.Eval("Pre_Alle")
                    Vettore_Orari_Timbrature.Alle(i - 1, g) = Format(TimeSerial(Int(Numero), (Numero - Int(Numero)) * 100, 0), "HH.mm")
                End If
                If InStr(MyValori, "21,") > 0 Then Vettore_Orari_Timbrature.Causale(i - 1, g) = Sc.Eval("Pre_Causale")
                If InStr(MyValori, "38,") > 0 Then Vettore_Orari_Timbrature.Colore(i - 1, g) = Sc.Eval("Pre_Colore")
                If InStr(MyValori, "24,") > 0 Then Vettore_Orari_Timbrature.Giustificativo(i - 1, g) = Sc.Eval("Pre_Giustificativo")
                If InStr(MyValori, "40,") > 0 Then Vettore_Orari_Timbrature.NelGruppo(i - 1, g) = Sc.Eval("Pre_NelGruppo")
                If InStr(MyValori, "43,") > 0 Then Vettore_Orari_Timbrature.Familiare(i - 1, g) = Sc.Eval("Pre_Familiare")
            End If
        Else
            If i = 0 Then
                If InStr(MyValori, "18,") > 0 Then
                    Numero = Sc.Eval("Pre_Dalle")
                    Vettore_Orari_Timbrature.Dalle(ii, g - 1) = Format(TimeSerial(Int(Numero), (Numero - Int(Numero)) * 100, 0), "HH.mm")
                End If
                If InStr(MyValori, "15,") > 0 Then
                    Numero = Sc.Eval("Pre_Alle")
                    Vettore_Orari_Timbrature.Alle(ii, g - 1) = Format(TimeSerial(Int(Numero), (Numero - Int(Numero)) * 100, 0), "HH.mm")
                End If
                If InStr(MyValori, "21,") > 0 Then Vettore_Orari_Timbrature.Causale(ii, g - 1) = Sc.Eval("Pre_Causale")
                If InStr(MyValori, "38,") > 0 Then Vettore_Orari_Timbrature.Colore(ii, g - 1) = Sc.Eval("Pre_Colore")
                If InStr(MyValori, "24,") > 0 Then Vettore_Orari_Timbrature.Giustificativo(ii, g - 1) = Sc.Eval("Pre_Giustificativo")
                If InStr(MyValori, "40,") > 0 Then Vettore_Orari_Timbrature.NelGruppo(ii, g - 1) = Sc.Eval("Pre_NelGruppo")
                If InStr(MyValori, "43,") > 0 Then Vettore_Orari_Timbrature.Familiare(ii, g - 1) = Sc.Eval("Pre_Familiare")
            Else
                If InStr(MyValori, "18,") > 0 Then
                    Numero = Sc.Eval("Pre_Dalle")
                    Vettore_Orari_Timbrature.Dalle(i - 1, g) = Format(TimeSerial(Int(Numero), (Numero - Int(Numero)) * 100, 0), "HH.mm")
                End If
                If InStr(MyValori, "15,") > 0 Then
                    Numero = Sc.Eval("Pre_Alle")
                    Vettore_Orari_Timbrature.Alle(i - 1, g) = Format(TimeSerial(Int(Numero), (Numero - Int(Numero)) * 100, 0), "HH.mm")
                End If
                If InStr(MyValori, "21,") > 0 Then Vettore_Orari_Timbrature.Causale(i - 1, g) = Sc.Eval("Pre_Causale")
                If InStr(MyValori, "38,") > 0 Then Vettore_Orari_Timbrature.Colore(i - 1, g) = Sc.Eval("Pre_Colore")
                If InStr(MyValori, "24,") > 0 Then Vettore_Orari_Timbrature.Giustificativo(i - 1, g) = Sc.Eval("Pre_Giustificativo")
                If InStr(MyValori, "40,") > 0 Then Vettore_Orari_Timbrature.NelGruppo(i - 1, g) = Sc.Eval("Pre_NelGruppo")
                If InStr(MyValori, "43,") > 0 Then Vettore_Orari_Timbrature.Familiare(i - 1, g) = Sc.Eval("Pre_Familiare")
            End If
        End If
        If InStr(MyValori, "17,") > 0 Then
            Numero = Sc.Eval("Dalle")
            Vettore_Orari_Timbrature.Dalle(i, g) = Format(TimeSerial(Int(Numero), (Numero - Int(Numero)) * 100, 0), "HH.mm")
        End If
        If InStr(MyValori, "14,") > 0 Then
            Numero = Sc.Eval("Alle")
            Vettore_Orari_Timbrature.Alle(i, g) = Format(TimeSerial(Int(Numero), (Numero - Int(Numero)) * 100, 0), "HH.mm")
        End If
        If InStr(MyValori, "20,") > 0 Then Vettore_Orari_Timbrature.Causale(i, g) = Sc.Eval("Causale")
        If InStr(MyValori, "37,") > 0 Then Vettore_Orari_Timbrature.Colore(i, g) = Sc.Eval("Colore")
        If InStr(MyValori, "23,") > 0 Then Vettore_Orari_Timbrature.Giustificativo(i, g) = Sc.Eval("Giustificativo")
        If InStr(MyValori, "41,") > 0 Then Vettore_Orari_Timbrature.NelGruppo(i, g) = Sc.Eval("NelGruppo")
        If InStr(MyValori, "44,") > 0 Then Vettore_Orari_Timbrature.Familiare(i, g) = Sc.Eval("Familiare")

        If i < 14 Then
            If Vettore_Orari_Timbrature.Dalle(i + 1, g) <> "" Or Vettore_Orari_Timbrature.Alle(i + 1, g) <> "" Then
                If InStr(MyValori, "19,") > 0 Then
                    Numero = Sc.Eval("Suc_Dalle")
                    If Numero <> "" Then Vettore_Orari_Timbrature.Dalle(i + 1, g) = Format(TimeSerial(Int(Numero), (Numero - Int(Numero)) * 100, 0), "HH.mm")
                End If
                If InStr(MyValori, "16,") > 0 Then
                    Numero = Sc.Eval("Suc_Alle")
                    If Numero <> "" Then Vettore_Orari_Timbrature.Alle(i + 1, g) = Format(TimeSerial(Int(Numero), (Numero - Int(Numero)) * 100, 0), "HH.mm")
                End If
                If InStr(MyValori, "22,") > 0 Then Vettore_Orari_Timbrature.Causale(i + 1, g) = Sc.Eval("Suc_Causale")
                If InStr(MyValori, "39,") > 0 Then Vettore_Orari_Timbrature.Colore(i + 1, g) = Sc.Eval("Suc_Colore")
                If InStr(MyValori, "25,") > 0 Then Vettore_Orari_Timbrature.Giustificativo(i + 1, g) = Sc.Eval("Suc_Giustificativo")
                If InStr(MyValori, "42,") > 0 Then Vettore_Orari_Timbrature.NelGruppo(i + 1, g) = Sc.Eval("Suc_NelGruppo")
                If InStr(MyValori, "45,") > 0 Then Vettore_Orari_Timbrature.Familiare(i + 1, g) = Sc.Eval("Suc_Familiare")
            Else
                If g < 31 Then
                    If InStr(MyValori, "19,") > 0 Then
                        Numero = Sc.Eval("Suc_Dalle")
                        If Numero <> "" Then Vettore_Orari_Timbrature.Dalle(0, g + 1) = Format(TimeSerial(Int(Numero), (Numero - Int(Numero)) * 100, 0), "HH.mm")
                    End If
                    If InStr(MyValori, "16,") > 0 Then
                        Numero = Sc.Eval("Suc_Alle")
                        If Numero <> "" Then Vettore_Orari_Timbrature.Alle(0, g + 1) = Format(TimeSerial(Int(Numero), (Numero - Int(Numero)) * 100, 0), "HH.mm")
                    End If
                    If InStr(MyValori, "22,") > 0 Then Vettore_Orari_Timbrature.Causale(0, g + 1) = Sc.Eval("Suc_Causale")
                    If InStr(MyValori, "39,") > 0 Then Vettore_Orari_Timbrature.Colore(0, g + 1) = Sc.Eval("Suc_Colore")
                    If InStr(MyValori, "25,") > 0 Then Vettore_Orari_Timbrature.Giustificativo(0, g + 1) = Sc.Eval("Suc_Giustificativo")
                    If InStr(MyValori, "42,") > 0 Then Vettore_Orari_Timbrature.NelGruppo(0, g + 1) = Sc.Eval("Suc_NelGruppo")
                    If InStr(MyValori, "45,") > 0 Then Vettore_Orari_Timbrature.Familiare(0, g + 1) = Sc.Eval("Suc_Familiare")
                End If
            End If
        Else
            If g < 31 Then
                If InStr(MyValori, "19,") > 0 Then
                    Numero = Sc.Eval("Suc_Dalle")
                    If Numero <> "" Then Vettore_Orari_Timbrature.Dalle(0, g + 1) = Format(TimeSerial(Int(Numero), (Numero - Int(Numero)) * 100, 0), "HH.mm")
                End If
                If InStr(MyValori, "16,") > 0 Then
                    Numero = Sc.Eval("Suc_Alle")
                    If Numero <> "" Then Vettore_Orari_Timbrature.Alle(0, g + 1) = Format(TimeSerial(Int(Numero), (Numero - Int(Numero)) * 100, 0), "HH.mm")
                End If
                If InStr(MyValori, "22,") > 0 Then Vettore_Orari_Timbrature.Causale(0, g + 1) = Sc.Eval("Suc_Causale")
                If InStr(MyValori, "39,") > 0 Then Vettore_Orari_Timbrature.Colore(0, g + 1) = Sc.Eval("Suc_Colore")
                If InStr(MyValori, "25,") > 0 Then Vettore_Orari_Timbrature.Giustificativo(0, g + 1) = Sc.Eval("Suc_Giustificativo")
                If InStr(MyValori, "42,") > 0 Then Vettore_Orari_Timbrature.NelGruppo(0, g + 1) = Sc.Eval("Suc_NelGruppo")
                If InStr(MyValori, "45,") > 0 Then Vettore_Orari_Timbrature.Familiare(0, g + 1) = Sc.Eval("Suc_Familiare")
            End If
        End If

        On Error Resume Next
        If Val(Err()) <> 0 Then
            Exit Function
        End If
        On Error GoTo 0

        Esegui_CalcoloCausaliTimbrature = True
    End Function

    Private Function Calcolo_Mensile(ByVal RegolaCalcolo As String, ByVal Data As Date, ByVal CodiceSuperGruppo As String, ByVal CodiceContratto As String, ByRef Vettore_Orari_Timbrature As Cls_Struttura_Orari_Timbrature) As String
        Dim cxst As New Cls_ClsXScriptTurni
        cxst.ConnectionString = ConnectionString
        cxst.Utente = Utente

        Dim cd As New Cls_Dipendenti

        Dim Sc As New MSScriptControl.ScriptControl

        Dim Dipendente As Long
        Dim Struttura As String
        Dim MyScript As String
        Dim MyValori As String
        Dim ii As Integer
        Dim aa As Integer
        Dim m As Byte

        Dim InizioMese As Date
        Dim MesePrec As Date
        Dim MeseSucc As Date
        Dim DataTest As Date

        Sc.Language = "VBScript"
        Sc.UseSafeSubset = False

        Calcolo_Mensile = ""
        MyScript = ""
        MyValori = ""
        For ii = 0 To 100
            If CodiceFormula(ii) = RegolaCalcolo Then
                If Format(DataFormula(ii), "yyyyMMdd") <= Format(Data, "yyyyMMdd") Then
                    MyScript = ScriptFormula(ii)
                    MyValori = ValoriFormula(ii)
                    Exit For
                End If
            End If
        Next ii

        If MyScript = "" Then
            Dim cn As OleDbConnection
            cn = New Data.OleDb.OleDbConnection(ConnectionString)

            cn.Open()
            Dim cmd As New OleDbCommand()
            cmd.CommandText = "SELECT TOP 1 * FROM FormuleCalcolo" & _
                              " WHERE Codice = ?" & _
                              " AND Validita <= ?" & _
                              " ORDER BY Validita DESC, Id"
            cmd.Parameters.AddWithValue("@Codice", RegolaCalcolo)
            cmd.Parameters.AddWithValue("@Validita", Data)
            cmd.Connection = cn

            Dim myPOSTreader As OleDbDataReader = cmd.ExecuteReader()
            If myPOSTreader.Read Then
                MyScript = StringaDb(myPOSTreader.Item("CodiceScript"))
                MyValori = StringaDb(myPOSTreader.Item("ValoriPerExecute"))
            End If
            myPOSTreader.Close()
            cmd.Parameters.Clear()
            cn.Close()
        End If

        If MyScript = "" Or IsDBNull(MyScript) Then
            Calcolo_Mensile = "(E) Codice script vuoto nella formula: " & RegolaCalcolo & vbNewLine
            Exit Function
        End If

        Sc.Reset()
        Sc.AddObject("Form", cxst)
        Sc.AddCode(MyScript)

        If Controllo = True Then
            Sc.ExecuteStatement("Controllo = ""S""")
        Else
            Sc.ExecuteStatement("Controllo = ""N""")
        End If

        If InStr(MyValori, "19,") > 0 Then Sc.ExecuteStatement("Dipendente = " & CodiceDipendente)

        If InStr(MyValori, "10,") > 0 Then
            If IsNumeric(Variabile(1)) Then
                Sc.ExecuteStatement("Variabile1 = " & cxst.Sostituisci(Variabile(1), ",", "."))
            Else
                If IsDate(Variabile(1)) Then
                    Sc.ExecuteStatement("Variabile1 = #" & Format(Variabile(1), "MM/dd/yyyy") & "#")
                Else
                    Sc.ExecuteStatement("Variabile1 = """ & Variabile(1) & """")
                End If
            End If
        End If
        If InStr(MyValori, "11,") > 0 Then
            If IsNumeric(Variabile(2)) Then
                Sc.ExecuteStatement("Variabile2 = " & cxst.Sostituisci(Variabile(2), ",", "."))
            Else
                If IsDate(Variabile(2)) Then
                    Sc.ExecuteStatement("Variabile2 = #" & Format(Variabile(2), "MM/dd/yyyy") & "#")
                Else
                    Sc.ExecuteStatement("Variabile2 = """ & Variabile(2) & """")
                End If
            End If
        End If
        If InStr(MyValori, "12,") > 0 Then
            If IsNumeric(Variabile(3)) Then
                Sc.ExecuteStatement("Variabile3 = " & cxst.Sostituisci(Variabile(3), ",", "."))
            Else
                If IsDate(Variabile(3)) Then
                    Sc.ExecuteStatement("Variabile3 = #" & Format(Variabile(3), "MM/dd/yyyy") & "#")
                Else
                    Sc.ExecuteStatement("Variabile3 = """ & Variabile(3) & """")
                End If
            End If
        End If
        If InStr(MyValori, "13,") > 0 Then
            If IsNumeric(Variabile(4)) Then
                Sc.ExecuteStatement("Variabile4 = " & cxst.Sostituisci(Variabile(4), ",", "."))
            Else
                If IsDate(Variabile(4)) Then
                    Sc.ExecuteStatement("Variabile4 = #" & Format(Variabile(4), "MM/dd/yyyy") & "#")
                Else
                    Sc.ExecuteStatement("Variabile4 = """ & Variabile(4) & """")
                End If
            End If
        End If
        If InStr(MyValori, "14,") > 0 Then
            If IsNumeric(Variabile(5)) Then
                Sc.ExecuteStatement("Variabile5 = " & cxst.Sostituisci(Variabile(5), ",", "."))
            Else
                If IsDate(Variabile(5)) Then
                    Sc.ExecuteStatement("Variabile5 = #" & Format(Variabile(5), "MM/dd/yyyy") & "#")
                Else
                    Sc.ExecuteStatement("Variabile5 = """ & Variabile(5) & """")
                End If
            End If
        End If
        If InStr(MyValori, "15,") > 0 Then
            If IsNumeric(Variabile(6)) Then
                Sc.ExecuteStatement("Variabile6 = " & cxst.Sostituisci(Variabile(6), ",", "."))
            Else
                If IsDate(Variabile(6)) Then
                    Sc.ExecuteStatement("Variabile6 = #" & Format(Variabile(6), "MM/dd/yyyy") & "#")
                Else
                    Sc.ExecuteStatement("Variabile6 = """ & Variabile(6) & """")
                End If
            End If
        End If
        If InStr(MyValori, "16,") > 0 Then
            If IsNumeric(Variabile(7)) Then
                Sc.ExecuteStatement("Variabile7 = " & cxst.Sostituisci(Variabile(7), ",", "."))
            Else
                If IsDate(Variabile(7)) Then
                    Sc.ExecuteStatement("Variabile7 = #" & Format(Variabile(7), "MM/dd/yyyy") & "#")
                Else
                    Sc.ExecuteStatement("Variabile7 = """ & Variabile(7) & """")
                End If
            End If
        End If
        If InStr(MyValori, "17,") > 0 Then
            If IsNumeric(Variabile(8)) Then
                Sc.ExecuteStatement("Variabile8 = " & cxst.Sostituisci(Variabile(8), ",", "."))
            Else
                If IsDate(Variabile(8)) Then
                    Sc.ExecuteStatement("Variabile8 = #" & Format(Variabile(8), "MM/dd/yyyy") & "#")
                Else
                    Sc.ExecuteStatement("Variabile8 = """ & Variabile(8) & """")
                End If
            End If
        End If
        If InStr(MyValori, "18,") > 0 Then
            If IsNumeric(Variabile(9)) Then
                Sc.ExecuteStatement("Variabile9 = " & cxst.Sostituisci(Variabile(9), ",", "."))
            Else
                If IsDate(Variabile(9)) Then
                    Sc.ExecuteStatement("Variabile9 = #" & Format(Variabile(9), "MM/dd/yyyy") & "#")
                Else
                    Sc.ExecuteStatement("Variabile9 = """ & Variabile(9) & """")
                End If
            End If
        End If
        If InStr(MyValori, "30,") > 0 Then Sc.ExecuteStatement("Errori = """"")
        If InStr(MyValori, "08,") > 0 Then Sc.ExecuteStatement("Giustificativo = """"")
        If InStr(MyValori, "80,") > 0 Then Sc.ExecuteStatement("NelGruppo = """"")
        If InStr(MyValori, "44,") > 0 Then Sc.ExecuteStatement("Tempo = 00.00")
        If InStr(MyValori, "45,") > 0 Then Sc.ExecuteStatement("TempoStandard = " & cxst.Sostituisci(W_TempoStandard, ",", "."))
        If InStr(MyValori, "48,") > 0 Then Sc.ExecuteStatement("Pausa = 00.00")
        If InStr(MyValori, "38,") > 0 Then Sc.ExecuteStatement("TempoGiorno = 00.00")
        If InStr(MyValori, "73,") > 0 Then Sc.ExecuteStatement("TempoRetribuito = 00.00")
        If InStr(MyValori, "41,") > 0 Then Sc.ExecuteStatement("TempoMese = " & cxst.Sostituisci(W_TempoMese, ",", "."))
        If InStr(MyValori, "50,") > 0 Then Sc.ExecuteStatement("Diurno = 00.00")
        If InStr(MyValori, "51,") > 0 Then Sc.ExecuteStatement("FestivoDiurno = 00.00")
        If InStr(MyValori, "52,") > 0 Then Sc.ExecuteStatement("Notturno = 00.00")
        If InStr(MyValori, "53,") > 0 Then Sc.ExecuteStatement("FestivoNotturno = 00.00")
        If InStr(MyValori, "02,") > 0 Then Sc.ExecuteStatement("Oggi = #" & Format(Data, "MM/dd/yyyy") & "#")
        If InStr(MyValori, "31,") > 0 Then Sc.ExecuteStatement("Giorno = """ & GiornoSettimana(Weekday(Data)) & """")
        If InStr(MyValori, "32,") > 0 Then Sc.ExecuteStatement("GiornoPrecedente = """ & GiornoSettimana(Weekday(DateAdd("d", -1, Data))) & """")
        If InStr(MyValori, "33,") > 0 Then Sc.ExecuteStatement("GiornoSuccessivo = """ & GiornoSettimana(Weekday(DateAdd("d", 1, Data))) & """")
        If InStr(MyValori, "63,") > 0 Then Sc.ExecuteStatement("FestivitaSettimanale = """ & GiornoSettimana(FestivitaSettimanale) & """")
        If InStr(MyValori, "65,") > 0 Then Sc.ExecuteStatement("GiornoPrecedenteFestivitaSettimanale = """ & GiornoSettimana(GiornoPrecedenteFestivitaSettimanale) & """")
        If InStr(MyValori, "66,") > 0 Then Sc.ExecuteStatement("GiornoSuccessivoFestivitaSettimanale = """ & GiornoSettimana(GiornoSuccessivoFestivitaSettimanale) & """")
        If InStr(MyValori, "64,") > 0 Then Sc.ExecuteStatement("PercentualePartimeGiorni = " & cxst.Sostituisci(W_PercentualePartimeGiorni, ",", "."))
        If InStr(MyValori, "68,") > 0 Then Sc.ExecuteStatement("PercentualePartimeGiorniDelMese = " & cxst.Sostituisci(W_PercentualePartimeGiorniDelMese, ",", "."))
        If InStr(MyValori, "76,") > 0 Then Sc.ExecuteStatement("PercentualePartimeOre = " & cxst.Sostituisci(W_PercentualePartimeOre, ",", "."))
        If InStr(MyValori, "77,") > 0 Then Sc.ExecuteStatement("PercentualePartimeOreDelMese = " & cxst.Sostituisci(W_PercentualePartimeOreDelMese, ",", "."))
        If InStr(MyValori, "79,") > 0 Then Sc.ExecuteStatement("TipoPartime = """ & W_TipoPartime & """")
        If InStr(MyValori, "34,") > 0 Then
            If cxst.GiornoFestivo(DateAdd("d", -1, Data), CodiceSuperGruppo) = True Then
                Sc.ExecuteStatement("GiornoPrecedenteFestivo = ""S""")
            Else
                Sc.ExecuteStatement("GiornoPrecedenteFestivo = ""N""")
            End If
        End If
        If InStr(MyValori, "35,") > 0 Then
            If cxst.GiornoFestivo(Data, CodiceSuperGruppo) = True Then
                Sc.ExecuteStatement("GiornoFestivo = ""S""")
            Else
                Sc.ExecuteStatement("GiornoFestivo = ""N""")
            End If
        End If
        If InStr(MyValori, "36,") > 0 Then
            If cxst.GiornoFestivo(DateAdd("d", 1, Data), CodiceSuperGruppo) = True Then
                Sc.ExecuteStatement("GiornoSuccessivoFestivo = ""S""")
            Else
                Sc.ExecuteStatement("GiornoSuccessivoFestivo = ""N""")
            End If
        End If
        If InStr(MyValori, "54,") > 0 Then
            Sc.ExecuteStatement("GiornoPartime = """ & Giorno_Partime(Data, CodiceContratto) & """")
        End If
        If InStr(MyValori, "37,") > 0 Then Sc.ExecuteStatement("UltimoGiorno = " & GiorniMese(Month(Data), Year(Data)))
        If InStr(MyValori, "39,") > 0 Then Sc.ExecuteStatement("Mese = " & Month(Data))
        If InStr(MyValori, "40,") > 0 Then Sc.ExecuteStatement("ValiditaMese = #" & Format(DateSerial(Year(Data), Month(Data), GiorniMese(Month(Data), Year(Data))), "MM/dd/yyyy") & "#")
        If InStr(MyValori, "67,") > 0 Then
            Validita = DateSerial(Year(Data), Month(Data), GiorniMese(Month(Data), Year(Data)))
            If CodiceDipendente > 0 Then
                DataLicenziamento = cxst.LeggiDataLicenziamento(CodiceDipendente, Validita)
                InizioMese = DateSerial(Year(Data), Month(Data), 1)
                If Format(DataLicenziamento, "yyyyMMdd") >= Format(InizioMese, "yyyyMMdd") And Format(DataLicenziamento, "yyyyMMdd") <= Format(Validita, "yyyyMMdd") Then
                    If Format(DataLicenziamento, "yyyyMMdd") <= Format(Data, "yyyyMMdd") Then Validita = DataLicenziamento
                End If
            End If
            Sc.ExecuteStatement("Validita = #" & Format(Validita, "MM/dd/yyyy") & "#")
        End If
        If InStr(MyValori, "61,") > 0 Then
            MesePrec = DateAdd("m", -1, Data)
            Sc.ExecuteStatement("ValiditaMesePrecedente = #" & Format(DateSerial(Year(MesePrec), Month(MesePrec), GiorniMese(Month(MesePrec), Year(MesePrec))), "MM/dd/yyyy") & "#")
        End If
        If InStr(MyValori, "42,") > 0 Then Sc.ExecuteStatement("Anno = " & Year(Data))
        If InStr(MyValori, "43,") > 0 Then Sc.ExecuteStatement("ValiditaAnno = #" & Format(DateSerial(Year(Data), 12, 31), "MM/dd/yyyy") & "#")
        If InStr(MyValori, "07,") > 0 Then Sc.ExecuteStatement("Validita1900 = #" & Format(DateSerial(1900, 1, 1), "MM/dd/yyyy") & "#")
        If InStr(MyValori, "05,") > 0 Then
            DataAssunzione = cxst.LeggiDataAssunzione(CodiceDipendente, Data)
            DataLicenziamento = cxst.LeggiDataLicenziamento(CodiceDipendente, Data)
            If Format(DataAssunzione, "yyyyMMdd") <= Format(DataLicenziamento, "yyyyMMdd") Then
                DataAssunzione = cxst.LeggiDataAssunzione(CodiceDipendente, DateSerial(Year(Data), Month(Data), GiorniMese(Month(Data), Year(Data))))
            End If
            Sc.ExecuteStatement("DataAssunzione = #" & Format(DataAssunzione, "MM/dd/yyyy") & "#")
        End If
        If InStr(MyValori, "06,") > 0 Then Sc.ExecuteStatement("DataLicenziamento = #" & Format(cxst.LeggiDataLicenziamento(CodiceDipendente, Data), "MM/dd/yyyy") & "#")

        If InStr(MyValori, "03,") > 0 Then
            DataTest = cd.CampoDipendenti(ConnectionString, CodiceDipendente, "DataNascita")
            If IsDate(DataTest) = False Then DataTest = "30/12/1899"
            Sc.ExecuteStatement("DataNascita = #" & Format(DataTest, "MM/dd/yyyy") & "#")
        End If
        If InStr(MyValori, "04,") > 0 Then Sc.ExecuteStatement("Sesso = """ & cd.CampoDipendenti(ConnectionString, CodiceDipendente, "Sesso") & """")

        a = Sc.Run("Calcolo")

        If InStr(MyValori, "10,") > 0 Then Variabile(1) = Sc.Eval("Variabile1")
        If InStr(MyValori, "11,") > 0 Then Variabile(2) = Sc.Eval("Variabile2")
        If InStr(MyValori, "12,") > 0 Then Variabile(3) = Sc.Eval("Variabile3")
        If InStr(MyValori, "13,") > 0 Then Variabile(4) = Sc.Eval("Variabile4")
        If InStr(MyValori, "14,") > 0 Then Variabile(5) = Sc.Eval("Variabile5")
        If InStr(MyValori, "15,") > 0 Then Variabile(6) = Sc.Eval("Variabile6")
        If InStr(MyValori, "16,") > 0 Then Variabile(7) = Sc.Eval("Variabile7")
        If InStr(MyValori, "17,") > 0 Then Variabile(8) = Sc.Eval("Variabile8")
        If InStr(MyValori, "18,") > 0 Then Variabile(9) = Sc.Eval("Variabile9")

        If InStr(MyValori, "30,") > 0 Then
            Testo = Sc.Eval("Errori")
            If Testo <> "" Then
                Calcolo_Mensile = Testo & vbNewLine
            End If
        End If
        If InStr(MyValori, "41,") > 0 Then W_TempoMese = Math.Round(Sc.Eval("TempoMese"), 2)
        If InStr(MyValori, "45,") > 0 Then W_TempoStandard = Math.Round(Sc.Eval("TempoStandard"), 2)
        On Error Resume Next
        If Val(Err()) <> 0 Then
            Calcolo_Mensile = Calcolo_Mensile & "(E) Nella formula: " & RegolaCalcolo & " errore script: " & Val(Err()) & vbNewLine
        End If
        On Error GoTo 0
    End Function

    Private Function Calcolo_Riepiloghi(ByVal RegolaCalcolo As String, ByVal Data As Date, ByVal CodiceSuperGruppo As String, ByVal CodiceContratto As String, ByRef Vettore_Calcolo_Orari As Cls_Struttura_Calcolo_Orari) As String
        Dim cxst As New Cls_ClsXScriptTurni
        cxst.ConnectionString = ConnectionString
        cxst.Utente = Utente
        cxst.Vettore_Calcolo_Orari = Vettore_Calcolo_Orari

        Dim cd As New Cls_Dipendenti

        Dim Dipendente As Long
        Dim Struttura As String
        Dim MyScript As String
        Dim MyValori As String
        Dim ii As Integer
        Dim aa As Integer
        Dim m As Byte

        Dim InizioMese As Date
        Dim MesePrec As Date
        Dim MeseSucc As Date
        Dim DataTest As Date

        Dim Sc As New MSScriptControl.ScriptControl
        Sc.Language = "VBScript"
        Sc.UseSafeSubset = False

        MyScript = ""
        MyValori = ""
        For ii = 0 To 100
            If CodiceFormula(ii) = "" Then Exit For
            If CodiceFormula(ii) = RegolaCalcolo Then
                If Format(DataFormula(ii), "yyyyMMdd") <= Format(Data, "yyyyMMdd") Then
                    MyScript = ScriptFormula(ii)
                    MyValori = ValoriFormula(ii)
                    Exit For
                End If
            End If
        Next ii

        If MyScript = "" Then
            Dim cn As OleDbConnection
            cn = New Data.OleDb.OleDbConnection(ConnectionString)

            cn.Open()
            Dim cmd As New OleDbCommand()
            cmd.CommandText = "SELECT * FROM FormuleCalcolo" & _
                              " WHERE Codice = ?" & _
                              " ORDER BY Validita DESC, Id"
            cmd.Parameters.AddWithValue("@Codice", RegolaCalcolo)
            cmd.Connection = cn

            Dim myPOSTreader As OleDbDataReader = cmd.ExecuteReader()
            Do While myPOSTreader.Read
                CodiceFormula(ii) = StringaDb(myPOSTreader.Item("Codice"))
                DataFormula(ii) = DataDb(myPOSTreader.Item("Validita"))
                ScriptFormula(ii) = StringaDb(myPOSTreader.Item("CodiceScript"))
                ValoriFormula(ii) = StringaDb(myPOSTreader.Item("ValoriPerExecute"))
                ii = ii + 1
            Loop
            myPOSTreader.Close()
            cmd.Parameters.Clear()
            cn.Close()

            For Kii = 0 To ii
                If CodiceFormula(Kii) = "" Then Exit For
                If CodiceFormula(Kii) = RegolaCalcolo Then
                    If Format(DataFormula(Kii), "yyyyMMdd") <= Format(Data, "yyyyMMdd") Then
                        MyScript = ScriptFormula(Kii)
                        MyValori = ValoriFormula(Kii)
                        Exit For
                    End If
                End If
            Next
        End If

        If MyScript = "" Or IsDBNull(MyScript) Then
            Calcolo_Riepiloghi = "(E) Codice script vuoto nella formula: " & RegolaCalcolo & vbNewLine
            Exit Function
        End If

        Sc.Reset()
        Sc.AddObject("Form", cxst)
        Sc.AddCode(MyScript)

        If Controllo = True Then
            Sc.ExecuteStatement("Controllo = ""S""")
        Else
            Sc.ExecuteStatement("Controllo = ""N""")
        End If

        If InStr(MyValori, "19,") > 0 Then Sc.ExecuteStatement("Dipendente = " & CodiceDipendente)

        If InStr(MyValori, "10,") > 0 Then
            If IsNumeric(Variabile(1)) Then
                Sc.ExecuteStatement("Variabile1 = " & cxst.Sostituisci(Variabile(1), ",", "."))
            Else
                If IsDate(Variabile(1)) Then
                    Sc.ExecuteStatement("Variabile1 = #" & Format(Variabile(1), "MM/dd/yyyy") & "#")
                Else
                    Sc.ExecuteStatement("Variabile1 = """ & Variabile(1) & """")
                End If
            End If
        End If
        If InStr(MyValori, "11,") > 0 Then
            If IsNumeric(Variabile(2)) Then
                Sc.ExecuteStatement("Variabile2 = " & cxst.Sostituisci(Variabile(2), ",", "."))
            Else
                If IsDate(Variabile(2)) Then
                    Sc.ExecuteStatement("Variabile2 = #" & Format(Variabile(2), "MM/dd/yyyy") & "#")
                Else
                    Sc.ExecuteStatement("Variabile2 = """ & Variabile(2) & """")
                End If
            End If
        End If
        If InStr(MyValori, "12,") > 0 Then
            If IsNumeric(Variabile(3)) Then
                Sc.ExecuteStatement("Variabile3 = " & cxst.Sostituisci(Variabile(3), ",", "."))
            Else
                If IsDate(Variabile(3)) Then
                    Sc.ExecuteStatement("Variabile3 = #" & Format(Variabile(3), "MM/dd/yyyy") & "#")
                Else
                    Sc.ExecuteStatement("Variabile3 = """ & Variabile(3) & """")
                End If
            End If
        End If
        If InStr(MyValori, "13,") > 0 Then
            If IsNumeric(Variabile(4)) Then
                Sc.ExecuteStatement("Variabile4 = " & cxst.Sostituisci(Variabile(4), ",", "."))
            Else
                If IsDate(Variabile(4)) Then
                    Sc.ExecuteStatement("Variabile4 = #" & Format(Variabile(4), "MM/dd/yyyy") & "#")
                Else
                    Sc.ExecuteStatement("Variabile4 = """ & Variabile(4) & """")
                End If
            End If
        End If
        If InStr(MyValori, "14,") > 0 Then
            If IsNumeric(Variabile(5)) Then
                Sc.ExecuteStatement("Variabile5 = " & cxst.Sostituisci(Variabile(5), ",", "."))
            Else
                If IsDate(Variabile(5)) Then
                    Sc.ExecuteStatement("Variabile5 = #" & Format(Variabile(5), "MM/dd/yyyy") & "#")
                Else
                    Sc.ExecuteStatement("Variabile5 = """ & Variabile(5) & """")
                End If
            End If
        End If
        If InStr(MyValori, "15,") > 0 Then
            If IsNumeric(Variabile(6)) Then
                Sc.ExecuteStatement("Variabile6 = " & cxst.Sostituisci(Variabile(6), ",", "."))
            Else
                If IsDate(Variabile(6)) Then
                    Sc.ExecuteStatement("Variabile6 = #" & Format(Variabile(6), "MM/dd/yyyy") & "#")
                Else
                    Sc.ExecuteStatement("Variabile6 = """ & Variabile(6) & """")
                End If
            End If
        End If
        If InStr(MyValori, "16,") > 0 Then
            If IsNumeric(Variabile(7)) Then
                Sc.ExecuteStatement("Variabile7 = " & cxst.Sostituisci(Variabile(7), ",", "."))
            Else
                If IsDate(Variabile(7)) Then
                    Sc.ExecuteStatement("Variabile7 = #" & Format(Variabile(7), "MM/dd/yyyy") & "#")
                Else
                    Sc.ExecuteStatement("Variabile7 = """ & Variabile(7) & """")
                End If
            End If
        End If
        If InStr(MyValori, "17,") > 0 Then
            If IsNumeric(Variabile(8)) Then
                Sc.ExecuteStatement("Variabile8 = " & cxst.Sostituisci(Variabile(8), ",", "."))
            Else
                If IsDate(Variabile(8)) Then
                    Sc.ExecuteStatement("Variabile8 = #" & Format(Variabile(8), "MM/dd/yyyy") & "#")
                Else
                    Sc.ExecuteStatement("Variabile8 = """ & Variabile(8) & """")
                End If
            End If
        End If
        If InStr(MyValori, "18,") > 0 Then
            If IsNumeric(Variabile(9)) Then
                Sc.ExecuteStatement("Variabile9 = " & cxst.Sostituisci(Variabile(9), ",", "."))
            Else
                If IsDate(Variabile(9)) Then
                    Sc.ExecuteStatement("Variabile9 = #" & Format(Variabile(9), "MM/dd/yyyy") & "#")
                Else
                    Sc.ExecuteStatement("Variabile9 = """ & Variabile(9) & """")
                End If
            End If
        End If
        If InStr(MyValori, "30,") > 0 Then Sc.ExecuteStatement("Errori = """"")
        If InStr(MyValori, "08,") > 0 Then Sc.ExecuteStatement("Giustificativo = """"")
        If InStr(MyValori, "80,") > 0 Then Sc.ExecuteStatement("NelGruppo = """"")
        If InStr(MyValori, "44,") > 0 Then Sc.ExecuteStatement("Tempo = 00.00")
        If InStr(MyValori, "82,") > 0 Then Sc.ExecuteStatement("Work_1 = " & cxst.Sostituisci(cxst.Vettore_Calcolo_Orari.Elemento(41, Day(Data)), ",", "."))
        If InStr(MyValori, "83,") > 0 Then Sc.ExecuteStatement("Work_2 = " & cxst.Sostituisci(cxst.Vettore_Calcolo_Orari.Elemento(42, Day(Data)), ",", "."))
        If InStr(MyValori, "46,") > 0 Then Sc.ExecuteStatement("TempoPrevisto = " & cxst.Sostituisci(cxst.Vettore_Calcolo_Orari.Elemento(43, Day(Data)), ",", "."))
        If InStr(MyValori, "48,") > 0 Then Sc.ExecuteStatement("Pausa = " & cxst.Sostituisci(cxst.Vettore_Calcolo_Orari.Elemento(44, Day(Data)), ",", "."))
        If InStr(MyValori, "38,") > 0 Then Sc.ExecuteStatement("TempoGiorno = " & cxst.Sostituisci(cxst.Vettore_Calcolo_Orari.Elemento(45, Day(Data)), ",", "."))
        If InStr(MyValori, "73,") > 0 Then Sc.ExecuteStatement("TempoRetribuito = " & cxst.Sostituisci(cxst.Vettore_Calcolo_Orari.Elemento(50, Day(Data)), ",", "."))
        If InStr(MyValori, "41,") > 0 Then Sc.ExecuteStatement("TempoMese = " & cxst.Sostituisci(W_TempoMese, ",", "."))
        If InStr(MyValori, "45,") > 0 Then Sc.ExecuteStatement("TempoStandard = " & cxst.Sostituisci(W_TempoStandard, ",", "."))
        If InStr(MyValori, "22,") > 0 Then Sc.ExecuteStatement("OrarioDomenica = " & Format(W_OrarioDomenica, "HH.mm"))
        If InStr(MyValori, "23,") > 0 Then Sc.ExecuteStatement("OrarioLunedi = " & Format(W_OrarioLunedi, "HH.mm"))
        If InStr(MyValori, "24,") > 0 Then Sc.ExecuteStatement("OrarioMartedi = " & Format(W_OrarioMartedi, "HH.mm"))
        If InStr(MyValori, "25,") > 0 Then Sc.ExecuteStatement("OrarioMercoledi = " & Format(W_OrarioMercoledi, "HH.mm"))
        If InStr(MyValori, "26,") > 0 Then Sc.ExecuteStatement("OrarioGiovedi = " & Format(W_OrarioGiovedi, "HH.mm"))
        If InStr(MyValori, "27,") > 0 Then Sc.ExecuteStatement("OrarioVenerdi = " & Format(W_OrarioVenerdi, "HH.mm"))
        If InStr(MyValori, "28,") > 0 Then Sc.ExecuteStatement("OrarioSabato = " & Format(W_OrarioSabato, "HH.mm"))
        If InStr(MyValori, "78,") > 0 Then Sc.ExecuteStatement("OrarioDelGiorno = " & Format(W_OrarioDelGiorno, "HH.mm"))
        If InStr(MyValori, "29,") > 0 Then Sc.ExecuteStatement("OrarioContrattuale = " & cxst.Sostituisci(W_OrarioContrattuale, ",", "."))
        If InStr(MyValori, "64,") > 0 Then Sc.ExecuteStatement("PercentualePartimeGiorni = " & cxst.Sostituisci(W_PercentualePartimeGiorni, ",", "."))
        If InStr(MyValori, "68,") > 0 Then Sc.ExecuteStatement("PercentualePartimeGiorniDelMese = " & cxst.Sostituisci(W_PercentualePartimeGiorniDelMese, ",", "."))
        If InStr(MyValori, "76,") > 0 Then Sc.ExecuteStatement("PercentualePartimeOre = " & cxst.Sostituisci(W_PercentualePartimeOre, ",", "."))
        If InStr(MyValori, "77,") > 0 Then Sc.ExecuteStatement("PercentualePartimeOreDelMese = " & cxst.Sostituisci(W_PercentualePartimeOreDelMese, ",", "."))
        If InStr(MyValori, "79,") > 0 Then Sc.ExecuteStatement("TipoPartime = """ & W_TipoPartime & """")
        If InStr(MyValori, "50,") > 0 Then Sc.ExecuteStatement("Diurno = " & cxst.Sostituisci(cxst.Vettore_Calcolo_Orari.Elemento(46, Day(Data)), ",", "."))
        If InStr(MyValori, "51,") > 0 Then Sc.ExecuteStatement("FestivoDiurno = " & cxst.Sostituisci(cxst.Vettore_Calcolo_Orari.Elemento(48, Day(Data)), ",", "."))
        If InStr(MyValori, "52,") > 0 Then Sc.ExecuteStatement("Notturno = " & cxst.Sostituisci(cxst.Vettore_Calcolo_Orari.Elemento(47, Day(Data)), ",", "."))
        If InStr(MyValori, "53,") > 0 Then Sc.ExecuteStatement("FestivoNotturno = " & cxst.Sostituisci(cxst.Vettore_Calcolo_Orari.Elemento(49, Day(Data)), ",", "."))
        If InStr(MyValori, "02,") > 0 Then Sc.ExecuteStatement("Oggi = #" & Format(Data, "MM/dd/yyyy") & "#")
        If InStr(MyValori, "31,") > 0 Then Sc.ExecuteStatement("Giorno = """ & GiornoSettimana(Weekday(Data)) & """")
        If InStr(MyValori, "32,") > 0 Then Sc.ExecuteStatement("GiornoPrecedente = """ & GiornoSettimana(Weekday(DateAdd("d", -1, Data))) & """")
        If InStr(MyValori, "33,") > 0 Then Sc.ExecuteStatement("GiornoSuccessivo = """ & GiornoSettimana(Weekday(DateAdd("d", 1, Data))) & """")
        If InStr(MyValori, "63,") > 0 Then Sc.ExecuteStatement("FestivitaSettimanale = """ & GiornoSettimana(FestivitaSettimanale) & """")
        If InStr(MyValori, "65,") > 0 Then Sc.ExecuteStatement("GiornoPrecedenteFestivitaSettimanale = """ & GiornoSettimana(GiornoPrecedenteFestivitaSettimanale) & """")
        If InStr(MyValori, "66,") > 0 Then Sc.ExecuteStatement("GiornoSuccessivoFestivitaSettimanale = """ & GiornoSettimana(GiornoSuccessivoFestivitaSettimanale) & """")
        If InStr(MyValori, "70,") > 0 Then Sc.ExecuteStatement("Familiare = 0")
        If InStr(MyValori, "34,") > 0 Then
            If cxst.GiornoFestivo(DateAdd("d", -1, Data), CodiceSuperGruppo) = True Then
                Sc.ExecuteStatement("GiornoPrecedenteFestivo = ""S""")
            Else
                Sc.ExecuteStatement("GiornoPrecedenteFestivo = ""N""")
            End If
        End If
        If InStr(MyValori, "35,") > 0 Then
            If cxst.GiornoFestivo(Data, CodiceSuperGruppo) = True Then
                Sc.ExecuteStatement("GiornoFestivo = ""S""")
            Else
                Sc.ExecuteStatement("GiornoFestivo = ""N""")
            End If
        End If
        If InStr(MyValori, "36,") > 0 Then
            If cxst.GiornoFestivo(DateAdd("d", 1, Data), CodiceSuperGruppo) = True Then
                Sc.ExecuteStatement("GiornoSuccessivoFestivo = ""S""")
            Else
                Sc.ExecuteStatement("GiornoSuccessivoFestivo = ""N""")
            End If
        End If
        If InStr(MyValori, "54,") > 0 Then
            Sc.ExecuteStatement("GiornoPartime = """ & Giorno_Partime(Data, CodiceContratto) & """")
        End If
        If InStr(MyValori, "37,") > 0 Then Sc.ExecuteStatement("UltimoGiorno = " & GiorniMese(Month(Data), Year(Data)))
        If InStr(MyValori, "39,") > 0 Then Sc.ExecuteStatement("Mese = " & Month(Data))
        If InStr(MyValori, "40,") > 0 Then Sc.ExecuteStatement("ValiditaMese = #" & Format(DateSerial(Year(Data), Month(Data), GiorniMese(Month(Data), Year(Data))), "MM/dd/yyyy") & "#")
        If InStr(MyValori, "67,") > 0 Then
            Validita = DateSerial(Year(Data), Month(Data), GiorniMese(Month(Data), Year(Data)))
            If CodiceDipendente > 0 Then
                DataLicenziamento = cxst.LeggiDataLicenziamento(CodiceDipendente, Validita)
                InizioMese = DateSerial(Year(Data), Month(Data), 1)
                If Format(DataLicenziamento, "yyyyMMdd") >= Format(InizioMese, "yyyyMMdd") And Format(DataLicenziamento, "yyyyMMdd") <= Format(Validita, "yyyyMMdd") Then
                    If Format(DataLicenziamento, "yyyyMMdd") >= Format(Data, "yyyyMMdd") Then Validita = DataLicenziamento
                End If
            End If
            Sc.ExecuteStatement("Validita = #" & Format(Validita, "MM/dd/yyyy") & "#")
        End If
        If InStr(MyValori, "61,") > 0 Then
            MesePrec = DateAdd("m", -1, Data)
            Sc.ExecuteStatement("ValiditaMesePrecedente = #" & Format(DateSerial(Year(MesePrec), Month(MesePrec), GiorniMese(Month(MesePrec), Year(MesePrec))), "MM/dd/yyyy") & "#")
        End If
        If InStr(MyValori, "42,") > 0 Then Sc.ExecuteStatement("Anno = " & Year(Data))
        If InStr(MyValori, "43,") > 0 Then Sc.ExecuteStatement("ValiditaAnno = #" & Format(DateSerial(Year(Data), 12, 31), "MM/dd/yyyy") & "#")
        If InStr(MyValori, "07,") > 0 Then Sc.ExecuteStatement("Validita1900 = #" & Format(DateSerial(1900, 1, 1), "MM/dd/yyyy") & "#")
        If InStr(MyValori, "05,") > 0 Then
            DataAssunzione = cxst.LeggiDataAssunzione(CodiceDipendente, Data)
            DataLicenziamento = cxst.LeggiDataLicenziamento(CodiceDipendente, Data)
            If Format(DataAssunzione, "yyyyMMdd") <= Format(DataLicenziamento, "yyyyMMdd") Then
                DataAssunzione = cxst.LeggiDataAssunzione(CodiceDipendente, DateSerial(Year(Data), Month(Data), GiorniMese(Month(Data), Year(Data))))
            End If
            Sc.ExecuteStatement("DataAssunzione = #" & Format(DataAssunzione, "MM/dd/yyyy") & "#")
        End If
        If InStr(MyValori, "06,") > 0 Then Sc.ExecuteStatement("DataLicenziamento = #" & Format(cxst.LeggiDataLicenziamento(CodiceDipendente, Data), "MM/dd/yyyy") & "#")

        If InStr(MyValori, "03,") > 0 Then
            DataTest = cd.CampoDipendenti(ConnectionString, CodiceDipendente, "DataNascita")
            If IsDate(DataTest) = False Then DataTest = "30/12/1899"
            Sc.ExecuteStatement("DataNascita = #" & Format(DataTest, "MM/dd/yyyy") & "#")
        End If
        If InStr(MyValori, "04,") > 0 Then Sc.ExecuteStatement("Sesso = """ & cd.CampoDipendenti(ConnectionString, CodiceDipendente, "Sesso") & """")

        a = Sc.Run("Calcolo")

        If InStr(MyValori, "10,") > 0 Then Variabile(1) = Sc.Eval("Variabile1")
        If InStr(MyValori, "11,") > 0 Then Variabile(2) = Sc.Eval("Variabile2")
        If InStr(MyValori, "12,") > 0 Then Variabile(3) = Sc.Eval("Variabile3")
        If InStr(MyValori, "13,") > 0 Then Variabile(4) = Sc.Eval("Variabile4")
        If InStr(MyValori, "14,") > 0 Then Variabile(5) = Sc.Eval("Variabile5")
        If InStr(MyValori, "15,") > 0 Then Variabile(6) = Sc.Eval("Variabile6")
        If InStr(MyValori, "16,") > 0 Then Variabile(7) = Sc.Eval("Variabile7")
        If InStr(MyValori, "17,") > 0 Then Variabile(8) = Sc.Eval("Variabile8")
        If InStr(MyValori, "18,") > 0 Then Variabile(9) = Sc.Eval("Variabile9")

        If InStr(MyValori, "30,") > 0 Then
            Testo = Sc.Eval("Errori")
            If Testo <> "" Then
                Calcolo_Riepiloghi = Testo & vbNewLine
            End If
        End If
        If InStr(MyValori, "82,") > 0 Then
            Numero = Sc.Eval("Work_1")
            Vettore_Calcolo_Orari.Elemento(41, Day(Data)) = cxst.Sostituisci(Format(Numero, "00.00"), ",", ".")
        End If
        If InStr(MyValori, "83,") > 0 Then
            Numero = Sc.Eval("Work_2")
            Vettore_Calcolo_Orari.Elemento(42, Day(Data)) = cxst.Sostituisci(Format(Numero, "00.00"), ",", ".")
        End If
        If InStr(MyValori, "46,") > 0 Then
            Numero = Sc.Eval("TempoPrevisto")
            cxst.Vettore_Calcolo_Orari.Elemento(43, Day(Data)) = cxst.Sostituisci(Format(Numero, "00.00"), ",", ".")
        End If
        If InStr(MyValori, "48,") > 0 Then
            Numero = Sc.Eval("Pausa")
            cxst.Vettore_Calcolo_Orari.Elemento(44, Day(Data)) = cxst.Sostituisci(Format(Numero, "00.00"), ",", ".")
        End If
        If InStr(MyValori, "38,") > 0 Then
            Numero = Sc.Eval("TempoGiorno")
            cxst.Vettore_Calcolo_Orari.Elemento(45, Day(Data)) = cxst.Sostituisci(Format(Numero, "00.00"), ",", ".")
        End If
        If InStr(MyValori, "50,") > 0 Then
            Numero = Sc.Eval("Diurno")
            cxst.Vettore_Calcolo_Orari.Elemento(46, Day(Data)) = cxst.Sostituisci(Format(Numero, "00.00"), ",", ".")
        End If
        If InStr(MyValori, "51,") > 0 Then
            Numero = Sc.Eval("FestivoDiurno")
            cxst.Vettore_Calcolo_Orari.Elemento(48, Day(Data)) = cxst.Sostituisci(Format(Numero, "00.00"), ",", ".")
        End If
        If InStr(MyValori, "52,") > 0 Then
            Numero = Sc.Eval("Notturno")
            cxst.Vettore_Calcolo_Orari.Elemento(47, Day(Data)) = cxst.Sostituisci(Format(Numero, "00.00"), ",", ".")
        End If
        If InStr(MyValori, "53,") > 0 Then
            Numero = Sc.Eval("FestivoNotturno")
            cxst.Vettore_Calcolo_Orari.Elemento(49, Day(Data)) = cxst.Sostituisci(Format(Numero, "00.00"), ",", ".")
        End If
        If InStr(MyValori, "73,") > 0 Then
            Numero = Sc.Eval("TempoRetribuito")
            cxst.Vettore_Calcolo_Orari.Elemento(50, Day(Data)) = cxst.Sostituisci(Format(Numero, "00.00"), ",", ".")
        End If
        If InStr(MyValori, "41,") > 0 Then W_TempoMese = Math.Round(Sc.Eval("TempoMese"), 2)
        If InStr(MyValori, "45,") > 0 Then W_TempoStandard = Math.Round(Sc.Eval("TempoStandard"), 2)

        Vettore_Calcolo_Orari = cxst.Vettore_Calcolo_Orari

        On Error Resume Next
        If Val(Err()) <> 0 Then
            Calcolo_Riepiloghi = Calcolo_Riepiloghi & "(E) Nella formula: " & RegolaCalcolo & " errore script: " & Val(Err()) & vbNewLine
        End If
        On Error GoTo 0
    End Function

    Private Function Calcolo_Maggiorazioni(ByVal Dipendente As Long, ByVal i As Long, ByVal Data As Date, ByVal CodiceSuperGruppo As String, ByVal CodiceContratto As String, ByVal ControlloOrario As String, ByRef Vettore_Orari_Timbrature As Cls_Struttura_Orari_Timbrature, ByRef Vettore_Calcolo_Orari As Cls_Struttura_Calcolo_Orari) As String
        Dim cxst As New Cls_ClsXScriptTurni
        cxst.ConnectionString = ConnectionString
        cxst.Utente = Utente

        Dim cpt As New Cls_ParametriTurni
        cpt.Leggi(ConnectionString)

        Dim cg As New Cls_Giustificativi
        Dim W_CG As String

        Dim g As Byte = Day(Data)
        Dim W_Diurno As Integer = 0
        Dim W_Notturno As Integer = 0
        Dim W_Pausa As Integer = 0
        Dim W_Diff As Integer = 0

        Calcolo_Maggiorazioni = ""
        If Vettore_Orari_Timbrature.Dalle(i, g) <> Vettore_Orari_Timbrature.Alle(i, g) Then
            If Vettore_Orari_Timbrature.Dalle(i, g) > Diurno_Dalle Then
                If Vettore_Orari_Timbrature.Dalle(i, g) > Diurno_Alle Then
                    If Vettore_Orari_Timbrature.Alle(i, g) = "24.00" Then
                        W_Notturno = DateDiff("n", TimeSerial(Val(Mid(Vettore_Orari_Timbrature.Dalle(i, g), 1, 2)), Val(Mid(Vettore_Orari_Timbrature.Dalle(i, g), 4, 2)), 0), TimeSerial(23, 59, 0)) + 1
                    Else
                        W_Notturno = DateDiff("n", TimeSerial(Val(Mid(Vettore_Orari_Timbrature.Dalle(i, g), 1, 2)), Val(Mid(Vettore_Orari_Timbrature.Dalle(i, g), 4, 2)), 0), TimeSerial(Val(Mid(Vettore_Orari_Timbrature.Alle(i, g), 1, 2)), Val(Mid(Vettore_Orari_Timbrature.Alle(i, g), 4, 2)), 0))
                    End If
                Else
                    If Vettore_Orari_Timbrature.Alle(i, g) = "24.00" Then
                        If "23.59" > Diurno_Alle Then
                            W_Diurno = DateDiff("n", TimeSerial(Val(Mid(Vettore_Orari_Timbrature.Dalle(i, g), 1, 2)), Val(Mid(Vettore_Orari_Timbrature.Dalle(i, g), 4, 2)), 0), TimeSerial(Val(Mid(Diurno_Alle, 1, 2)), Val(Mid(Diurno_Alle, 4, 2)), 0))
                            W_Notturno = DateDiff("n", TimeSerial(Val(Mid(Diurno_Alle, 1, 2)), Val(Mid(Diurno_Alle, 4, 2)), 0), TimeSerial(23, 59, 0)) + 1
                        Else
                            W_Diurno = DateDiff("n", TimeSerial(Val(Mid(Vettore_Orari_Timbrature.Dalle(i, g), 1, 2)), Val(Mid(Vettore_Orari_Timbrature.Dalle(i, g), 4, 2)), 0), TimeSerial(23, 59, 0)) + 1
                        End If
                    Else
                        If Vettore_Orari_Timbrature.Alle(i, g) > Diurno_Alle Then
                            W_Diurno = DateDiff("n", TimeSerial(Val(Mid(Vettore_Orari_Timbrature.Dalle(i, g), 1, 2)), Val(Mid(Vettore_Orari_Timbrature.Dalle(i, g), 4, 2)), 0), TimeSerial(Val(Mid(Diurno_Alle, 1, 2)), Val(Mid(Diurno_Alle, 4, 2)), 0))
                            W_Notturno = DateDiff("n", TimeSerial(Val(Mid(Diurno_Alle, 1, 2)), Val(Mid(Diurno_Alle, 4, 2)), 0), TimeSerial(Val(Mid(Vettore_Orari_Timbrature.Alle(i, g), 1, 2)), Val(Mid(Vettore_Orari_Timbrature.Alle(i, g), 4, 2)), 0))
                        Else
                            W_Diurno = DateDiff("n", TimeSerial(Val(Mid(Vettore_Orari_Timbrature.Dalle(i, g), 1, 2)), Val(Mid(Vettore_Orari_Timbrature.Dalle(i, g), 4, 2)), 0), TimeSerial(Val(Mid(Vettore_Orari_Timbrature.Alle(i, g), 1, 2)), Val(Mid(Vettore_Orari_Timbrature.Alle(i, g), 4, 2)), 0))
                        End If
                    End If
                End If
            Else
                If Vettore_Orari_Timbrature.Alle(i, g) = "24.00" Then
                    ' If "23.59" <= Diurno_Dalle Then
                    If "23.59" = Diurno_Dalle Then
                        W_Notturno = DateDiff("n", TimeSerial(Val(Mid(Vettore_Orari_Timbrature.Dalle(i, g), 1, 2)), Val(Mid(Vettore_Orari_Timbrature.Dalle(i, g), 4, 2)), 0), TimeSerial(23, 59, 0)) + 1
                    Else
                        W_Notturno = DateDiff("n", TimeSerial(Val(Mid(Vettore_Orari_Timbrature.Dalle(i, g), 1, 2)), Val(Mid(Vettore_Orari_Timbrature.Dalle(i, g), 4, 2)), 0), TimeSerial(Val(Mid(Diurno_Dalle, 1, 2)), Val(Mid(Diurno_Dalle, 4, 2)), 0))
                        ' If "23.59" > Diurno_Alle Then
                        If "23.59" = Diurno_Alle Then
                            W_Diurno = DateDiff("n", TimeSerial(Val(Mid(Diurno_Dalle, 1, 2)), Val(Mid(Diurno_Dalle, 4, 2)), 0), TimeSerial(Val(Mid(Diurno_Alle, 1, 2)), Val(Mid(Diurno_Alle, 4, 2)), 0))
                            W_Diff = DateDiff("n", TimeSerial(Val(Mid(Diurno_Alle, 1, 2)), Val(Mid(Diurno_Alle, 4, 2)), 0), TimeSerial(23, 59, 0)) + 1
                            W_Notturno = W_Notturno + W_Diff
                        Else
                            W_Diurno = DateDiff("n", TimeSerial(Val(Mid(Diurno_Dalle, 1, 2)), Val(Mid(Diurno_Dalle, 4, 2)), 0), TimeSerial(23, 59, 0))
                        End If
                    End If
                Else
                    If Vettore_Orari_Timbrature.Alle(i, g) <= Diurno_Dalle Then
                        W_Notturno = DateDiff("n", TimeSerial(Val(Mid(Vettore_Orari_Timbrature.Dalle(i, g), 1, 2)), Val(Mid(Vettore_Orari_Timbrature.Dalle(i, g), 4, 2)), 0), TimeSerial(Val(Mid(Vettore_Orari_Timbrature.Alle(i, g), 1, 2)), Val(Mid(Vettore_Orari_Timbrature.Alle(i, g), 4, 2)), 0))
                    Else
                        W_Notturno = DateDiff("n", TimeSerial(Val(Mid(Vettore_Orari_Timbrature.Dalle(i, g), 1, 2)), Val(Mid(Vettore_Orari_Timbrature.Dalle(i, g), 4, 2)), 0), TimeSerial(Val(Mid(Diurno_Dalle, 1, 2)), Val(Mid(Diurno_Dalle, 4, 2)), 0))
                        If Vettore_Orari_Timbrature.Alle(i, g) > Diurno_Alle Then
                            W_Diurno = DateDiff("n", TimeSerial(Val(Mid(Diurno_Dalle, 1, 2)), Val(Mid(Diurno_Dalle, 4, 2)), 0), TimeSerial(Val(Mid(Diurno_Alle, 1, 2)), Val(Mid(Diurno_Alle, 4, 2)), 0))
                            W_Diff = DateDiff("n", TimeSerial(Val(Mid(Diurno_Alle, 1, 2)), Val(Mid(Diurno_Alle, 4, 2)), 0), TimeSerial(Val(Mid(Vettore_Orari_Timbrature.Alle(i, g), 1, 2)), Val(Mid(Vettore_Orari_Timbrature.Alle(i, g), 4, 2)), 0))
                            W_Notturno = W_Notturno + W_Diff
                        Else
                            W_Diurno = DateDiff("n", TimeSerial(Val(Mid(Diurno_Dalle, 1, 2)), Val(Mid(Diurno_Dalle, 4, 2)), 0), TimeSerial(Val(Mid(Vettore_Orari_Timbrature.Alle(i, g), 1, 2)), Val(Mid(Vettore_Orari_Timbrature.Alle(i, g), 4, 2)), 0))
                        End If
                    End If
                End If
            End If
        End If
        NelGruppo = Vettore_Orari_Timbrature.NelGruppo(i, g)
        FlagGiornoSuccessivo = Vettore_Orari_Timbrature.GiornoSuccessivo(i, g)
        Orario_Dalle = Vettore_Orari_Timbrature.Dalle(i, g)
        Orario_Alle = Vettore_Orari_Timbrature.Alle(i, g)
        Ore_Pausa = Vettore_Orari_Timbrature.Pausa(i, g)
        Causale = Vettore_Orari_Timbrature.Causale(i, g)
        Colore = Vettore_Orari_Timbrature.Colore(i, g)
        Ore_Intervallo = "00.00"
        Ore_Diurne = "00.00"
        Ore_Notturne = "00.00"
        Ore_FestiveDiurne = "00.00"
        Ore_FestiveNotturne = "00.00"
        If cpt.TrattaOrarioPausaAutomatico = "S" Then
            If Ore_Pausa > 0 Then
                W_Pausa = DateDiff("n", TimeSerial(0, 0, 0), TimeSerial(Val(Mid(Ore_Pausa, 1, 2)), Val(Mid(Ore_Pausa, 4, 2)), 0))
                If W_Pausa > W_Diurno Then
                    W_Pausa = W_Pausa - W_Diurno
                    W_Diurno = 0
                Else
                    W_Diurno = W_Diurno - W_Pausa
                    W_Pausa = 0
                End If
                If W_Pausa > 0 Then
                    If W_Pausa > W_Notturno Then
                        W_Notturno = 0
                    Else
                        W_Notturno = W_Notturno - W_Pausa
                    End If
                End If
            End If
        End If
        If (Weekday(Data) = FestivitaSettimanale Or cxst.GiornoFestivo(Data, CodiceSuperGruppo) = True) Then
            Ore_FestiveNotturne = cxst.Sostituisci(Format(cxst.Mod_OperaConMinuti(Ore_FestiveNotturne, "+", W_Notturno, "n"), "00.00"), ",", ".")
            Ore_FestiveDiurne = cxst.Sostituisci(Format(cxst.Mod_OperaConMinuti(Ore_FestiveDiurne, "+", W_Diurno, "n"), "00.00"), ",", ".")
        Else
            Ore_Notturne = cxst.Sostituisci(Format(cxst.Mod_OperaConMinuti(Ore_Notturne, "+", W_Notturno, "n"), "00.00"), ",", ".")
            Ore_Diurne = cxst.Sostituisci(Format(cxst.Mod_OperaConMinuti(Ore_Diurne, "+", W_Diurno, "n"), "00.00"), ",", ".")
        End If
        Ore_Intervallo = cxst.Sostituisci(Format(cxst.Mod_OperaConMinuti(Ore_Intervallo, "+", W_Notturno + W_Diurno, "n"), "00.00"), ",", ".")
        Fase = "Orari"

        Risultato = Calcolo_Orari(Vettore_Orari_Timbrature.Giustificativo(i, g) & "/" & Vettore_Orari_Timbrature.Familiare(i, g), Vettore_Orari_Timbrature.Tipo(i, g), Vettore_Orari_Timbrature.Qualita(i, g), Vettore_Orari_Timbrature.TipoServizio(i, g), Data, i, CodiceSuperGruppo, CodiceContratto, Vettore_Calcolo_Orari)
        If InStr(Risultato, "(E)") <> 0 Then
            Calcolo_Maggiorazioni = Calcolo_Maggiorazioni & Dipendente & " (E) Calcolo Orari il giorno : " & Day(Data) & " Vettore_Orari_Timbrature.Giustificativo: " & i & vbNewLine
            Calcolo_Maggiorazioni = Calcolo_Maggiorazioni & Risultato
        Else
            Calcolo_Maggiorazioni = Calcolo_Maggiorazioni & Risultato
        End If

        'mau+
        ' "Sostituzione"
        ' "Assenza"
        ' "UscitaAnticipata"
        ' "UscitaPosticipata"
        ' "EntrataAnticipata"
        ' "EntrataPosticipata"
        ' ""
        If Vettore_Orari_Timbrature.Giustificativo(i, g) <> "" Then
            If ControlloOrario = "S" Then
                If Vettore_Orari_Timbrature.Tipo(i, g) <> "Informazione" And cg.CampoGiustificativi(ConnectionString, Vettore_Orari_Timbrature.Giustificativo(i, g), "RegoleGenerali") = "S" Then
                    If Vettore_Orari_Timbrature.Tipo(i, g) = "Assenza" Or Vettore_Orari_Timbrature.Tipo(i, g) = "UscitaAnticipata" Or Vettore_Orari_Timbrature.Tipo(i, g) = "EntrataPosticipata" Then
                        W_CG = cg.CampoGiustificativi(ConnectionString, Vettore_Orari_Timbrature.Giustificativo(i, g), "GiustificativoPerAssenza")
                        If W_CG <> "" Then Vettore_Orari_Timbrature.Giustificativo(i, g) = W_CG
                    Else
                        W_CG = cg.CampoGiustificativi(ConnectionString, Vettore_Orari_Timbrature.Giustificativo(i, g), "GiustificativoPerPresenza")
                        If W_CG <> "" Then Vettore_Orari_Timbrature.Giustificativo(i, g) = W_CG
                    End If
                End If
                If cg.CampoGiustificativi(ConnectionString, Vettore_Orari_Timbrature.Giustificativo(i, g), "SuTurniTimbrature") = "S" Then
                    Calcolo_Maggiorazioni = Calcolo_Maggiorazioni & Dipendente & " (I) Il giorno : " & g & "   " & cg.CampoGiustificativi(ConnectionString, Vettore_Orari_Timbrature.Giustificativo(i, g), "Descrizione") & " : " & Ore_Intervallo & vbNewLine
                End If
            End If
        End If
        'mau-

    End Function

    Private Function Calcolo_Orari(ByVal Codice As String, ByVal Tipo As String, ByVal Qualita As String, ByVal TipoServizio As String, ByVal Data As Date, ByVal i As Long, ByVal CodiceSuperGruppo As String, ByVal CodiceContratto As String, ByRef Vettore_Calcolo_Orari As Cls_Struttura_Calcolo_Orari) As String
        Dim cg As New Cls_Giustificativi

        Calcolo_Orari = ""
        cg.Leggi(ConnectionString, Left(Codice, InStr(Codice, "/") - 1))
        If cg.Id = 0 Then
            Exit Function
        End If
        If cg.RegolaCalcolo1 <> "" Then
            Risultato = Esegui_Calcolo_Orari(cg.RegolaCalcolo1, Codice, Tipo, Qualita, TipoServizio, Data, i, CodiceSuperGruppo, CodiceContratto, Vettore_Calcolo_Orari)
            Calcolo_Orari = Calcolo_Orari & Risultato
            If InStr(Risultato, "(E)") <> 0 Then
                Exit Function
            End If
            Esegui_OperazioneTempo_Pausa()
        End If
        If cg.RegolaCalcolo2 <> "" Then
            Risultato = Esegui_Calcolo_Orari(cg.RegolaCalcolo2, Codice, Tipo, Qualita, TipoServizio, Data, i, CodiceSuperGruppo, CodiceContratto, Vettore_Calcolo_Orari)
            Calcolo_Orari = Calcolo_Orari & Risultato
            If InStr(Risultato, "(E)") <> 0 Then
                Exit Function
            End If
            Esegui_OperazioneTempo_Pausa()
        End If
        If cg.RegolaCalcolo3 <> "" Then
            Risultato = Esegui_Calcolo_Orari(cg.RegolaCalcolo3, Codice, Tipo, Qualita, TipoServizio, Data, i, CodiceSuperGruppo, CodiceContratto, Vettore_Calcolo_Orari)
            Calcolo_Orari = Calcolo_Orari & Risultato
            If InStr(Risultato, "(E)") <> 0 Then
                Exit Function
            End If
            Esegui_OperazioneTempo_Pausa()
        End If
    End Function

    Private Function Esegui_Calcolo_Orari(ByVal RegolaCalcolo As String, ByVal Codice As String, ByVal Tipo As String, ByVal Qualita As String, ByVal TipoServizio As String, ByVal Data As Date, ByVal i As Long, ByVal CodiceSuperGruppo As String, ByVal CodiceContratto As String, ByRef Vettore_Calcolo_Orari As Cls_Struttura_Calcolo_Orari) As String
        Dim cxst As New Cls_ClsXScriptTurni
        cxst.ConnectionString = ConnectionString
        cxst.Utente = Utente
        cxst.Vettore_Calcolo_Orari = Vettore_Calcolo_Orari

        Dim cd As New Cls_Dipendenti
        Dim cf As New Cls_Familiari

        Dim Sc As New MSScriptControl.ScriptControl
        Sc.Language = "VBScript"
        Sc.UseSafeSubset = False

        Dim DataTest As Date
        Dim MyScript As String
        Dim MyValori As String

        Dim ii As Integer

        OperazionePausa = ""
        OperazioneTempo = ""
        OperazioneOreLavorate = ""
        OperazioneOreFeriali = ""
        OperazioneOreFestive = ""
        OperazioneOreRetribuite = ""

        MyScript = ""
        MyValori = ""
        For ii = 0 To 100
            If CodiceFormula(ii) = "" Then Exit For
            If CodiceFormula(ii) = RegolaCalcolo Then
                If Format(DataFormula(ii), "yyyyMMdd") <= Format(Data, "yyyyMMdd") Then
                    MyScript = ScriptFormula(ii)
                    MyValori = ValoriFormula(ii)
                    Exit For
                End If
            End If
        Next ii

        If MyScript = "" Then
            Dim cn As OleDbConnection
            cn = New Data.OleDb.OleDbConnection(ConnectionString)

            cn.Open()
            Dim cmd As New OleDbCommand()
            cmd.CommandText = "SELECT * FROM FormuleCalcolo" & _
                              " WHERE Codice = ?" & _
                              " ORDER BY Validita DESC, Id"
            cmd.Parameters.AddWithValue("@Codice", RegolaCalcolo)
            cmd.Connection = cn

            Dim myPOSTreader As OleDbDataReader = cmd.ExecuteReader()
            Do While myPOSTreader.Read
                CodiceFormula(ii) = StringaDb(myPOSTreader.Item("Codice"))
                DataFormula(ii) = DataDb(myPOSTreader.Item("Validita"))
                ScriptFormula(ii) = StringaDb(myPOSTreader.Item("CodiceScript"))
                ValoriFormula(ii) = StringaDb(myPOSTreader.Item("ValoriPerExecute"))
                ii = ii + 1
            Loop
            myPOSTreader.Close()
            cmd.Parameters.Clear()
            cn.Close()

            For Kii = 0 To ii
                If CodiceFormula(Kii) = "" Then Exit For
                If CodiceFormula(Kii) = RegolaCalcolo Then
                    If Format(DataFormula(Kii), "yyyyMMdd") <= Format(Data, "yyyyMMdd") Then
                        MyScript = ScriptFormula(Kii)
                        MyValori = ValoriFormula(Kii)
                        Exit For
                    End If
                End If
            Next

        End If

        If MyScript = "" Or IsDBNull(MyScript) Then
            Esegui_Calcolo_Orari = "(E) Codice script vuoto nella formula: " & RegolaCalcolo & vbNewLine
            Exit Function
        End If

        Sc.Reset()
        Sc.AddObject("Form", cxst)
        Sc.AddCode(MyScript)

        If Controllo = True Then
            Sc.ExecuteStatement("Controllo = ""S""")
        Else
            Sc.ExecuteStatement("Controllo = ""N""")
        End If

        If InStr(MyValori, "19,") > 0 Then Sc.ExecuteStatement("Dipendente = " & CodiceDipendente)

        If InStr(MyValori, "10,") > 0 Then
            If IsNumeric(Variabile(1)) Then
                Sc.ExecuteStatement("Variabile1 = " & cxst.Sostituisci(Variabile(1), ",", "."))
            Else
                If IsDate(Variabile(1)) Then
                    Sc.ExecuteStatement("Variabile1 = #" & Format(Variabile(1), "MM/dd/yyyy") & "#")
                Else
                    Sc.ExecuteStatement("Variabile1 = """ & Variabile(1) & """")
                End If
            End If
        End If
        If InStr(MyValori, "11,") > 0 Then
            If IsNumeric(Variabile(2)) Then
                Sc.ExecuteStatement("Variabile2 = " & cxst.Sostituisci(Variabile(2), ",", "."))
            Else
                If IsDate(Variabile(2)) Then
                    Sc.ExecuteStatement("Variabile2 = #" & Format(Variabile(2), "MM/dd/yyyy") & "#")
                Else
                    Sc.ExecuteStatement("Variabile2 = """ & Variabile(2) & """")
                End If
            End If
        End If
        If InStr(MyValori, "12,") > 0 Then
            If IsNumeric(Variabile(3)) Then
                Sc.ExecuteStatement("Variabile3 = " & cxst.Sostituisci(Variabile(3), ",", "."))
            Else
                If IsDate(Variabile(3)) Then
                    Sc.ExecuteStatement("Variabile3 = #" & Format(Variabile(3), "MM/dd/yyyy") & "#")
                Else
                    Sc.ExecuteStatement("Variabile3 = """ & Variabile(3) & """")
                End If
            End If
        End If
        If InStr(MyValori, "13,") > 0 Then
            If IsNumeric(Variabile(4)) Then
                Sc.ExecuteStatement("Variabile4 = " & cxst.Sostituisci(Variabile(4), ",", "."))
            Else
                If IsDate(Variabile(4)) Then
                    Sc.ExecuteStatement("Variabile4 = #" & Format(Variabile(4), "MM/dd/yyyy") & "#")
                Else
                    Sc.ExecuteStatement("Variabile4 = """ & Variabile(4) & """")
                End If
            End If
        End If
        If InStr(MyValori, "14,") > 0 Then
            If IsNumeric(Variabile(5)) Then
                Sc.ExecuteStatement("Variabile5 = " & cxst.Sostituisci(Variabile(5), ",", "."))
            Else
                If IsDate(Variabile(5)) Then
                    Sc.ExecuteStatement("Variabile5 = #" & Format(Variabile(5), "MM/dd/yyyy") & "#")
                Else
                    Sc.ExecuteStatement("Variabile5 = """ & Variabile(5) & """")
                End If
            End If
        End If
        If InStr(MyValori, "15,") > 0 Then
            If IsNumeric(Variabile(6)) Then
                Sc.ExecuteStatement("Variabile6 = " & cxst.Sostituisci(Variabile(6), ",", "."))
            Else
                If IsDate(Variabile(6)) Then
                    Sc.ExecuteStatement("Variabile6 = #" & Format(Variabile(6), "MM/dd/yyyy") & "#")
                Else
                    Sc.ExecuteStatement("Variabile6 = """ & Variabile(6) & """")
                End If
            End If
        End If
        If InStr(MyValori, "16,") > 0 Then
            If IsNumeric(Variabile(7)) Then
                Sc.ExecuteStatement("Variabile7 = " & cxst.Sostituisci(Variabile(7), ",", "."))
            Else
                If IsDate(Variabile(7)) Then
                    Sc.ExecuteStatement("Variabile7 = #" & Format(Variabile(7), "MM/dd/yyyy") & "#")
                Else
                    Sc.ExecuteStatement("Variabile7 = """ & Variabile(7) & """")
                End If
            End If
        End If
        If InStr(MyValori, "17,") > 0 Then
            If IsNumeric(Variabile(8)) Then
                Sc.ExecuteStatement("Variabile8 = " & cxst.Sostituisci(Variabile(8), ",", "."))
            Else
                If IsDate(Variabile(8)) Then
                    Sc.ExecuteStatement("Variabile8 = #" & Format(Variabile(8), "MM/dd/yyyy") & "#")
                Else
                    Sc.ExecuteStatement("Variabile8 = """ & Variabile(8) & """")
                End If
            End If
        End If
        If InStr(MyValori, "18,") > 0 Then
            If IsNumeric(Variabile(9)) Then
                Sc.ExecuteStatement("Variabile9 = " & cxst.Sostituisci(Variabile(9), ",", "."))
            Else
                If IsDate(Variabile(9)) Then
                    Sc.ExecuteStatement("Variabile9 = #" & Format(Variabile(9), "MM/dd/yyyy") & "#")
                Else
                    Sc.ExecuteStatement("Variabile9 = """ & Variabile(9) & """")
                End If
            End If
        End If
        If Fase = "Orari" Then
            If InStr(MyValori, "08,") > 0 Then Sc.ExecuteStatement("Giustificativo = """ & Left(Codice, InStr(Codice, "/") - 1) & """")
            If InStr(MyValori, "70,") > 0 Then
                Familiare = Mid(Codice, InStr(Codice, "/") + 1)
                Sc.ExecuteStatement("Familiare = " & Familiare)
            End If
            If InStr(MyValori, "58,") > 0 Then Sc.ExecuteStatement("Tipo = """ & Tipo & """")
            If InStr(MyValori, "59,") > 0 Then Sc.ExecuteStatement("Qualita = """ & Qualita & """")
            If InStr(MyValori, "09,") > 0 Then Sc.ExecuteStatement("TipoServizio = """ & TipoServizio & """")
            '            If InStr(MyValori, "48,") > 0 Then Sc.ExecuteStatement("Pausa = " & Format(Ore_Pausa, "HH.mm"))
            If InStr(MyValori, "48,") > 0 Then Sc.ExecuteStatement("Pausa = " & Ore_Pausa)
            '            If InStr(MyValori, "44,") > 0 Then Sc.ExecuteStatement("Tempo = " & Format(Ore_Intervallo, "HH.mm"))
            If InStr(MyValori, "44,") > 0 Then Sc.ExecuteStatement("Tempo = " & Ore_Intervallo)
            If InStr(MyValori, "38,") > 0 Then Sc.ExecuteStatement("TempoGiorno = 00.00")
            If InStr(MyValori, "73,") > 0 Then Sc.ExecuteStatement("TempoRetribuito = 00.00")
            '            If InStr(MyValori, "50,") > 0 Then Sc.ExecuteStatement("Diurno = " & Format(Ore_Diurne, "HH.mm"))
            If InStr(MyValori, "50,") > 0 Then Sc.ExecuteStatement("Diurno = " & Ore_Diurne)
            '            If InStr(MyValori, "51,") > 0 Then Sc.ExecuteStatement("FestivoDiurno = " & Format(Ore_FestiveDiurne, "HH.mm"))
            If InStr(MyValori, "51,") > 0 Then Sc.ExecuteStatement("FestivoDiurno = " & Ore_FestiveDiurne)
            '            If InStr(MyValori, "52,") > 0 Then Sc.ExecuteStatement("Notturno = " & Format(Ore_Notturne, "HH.mm"))
            If InStr(MyValori, "52,") > 0 Then Sc.ExecuteStatement("Notturno = " & Ore_Notturne)
            '            If InStr(MyValori, "53,") > 0 Then Sc.ExecuteStatement("FestivoNotturno = " & Format(Ore_FestiveNotturne, "HH.mm"))
            If InStr(MyValori, "53,") > 0 Then Sc.ExecuteStatement("FestivoNotturno = " & Ore_FestiveNotturne)
            '            If InStr(MyValori, "20,") > 0 Then Sc.ExecuteStatement("OrarioDalle = " & Format(Orario_Dalle, "HH.mm"))
            If InStr(MyValori, "20,") > 0 Then Sc.ExecuteStatement("OrarioDalle = " & Orario_Dalle)
            '            If InStr(MyValori, "21,") > 0 Then Sc.ExecuteStatement("OrarioAlle = " & Format(Orario_Alle, "HH.mm"))
            If InStr(MyValori, "21,") > 0 Then Sc.ExecuteStatement("OrarioAlle = " & Orario_Alle)
            If InStr(MyValori, "62,") > 0 Then Sc.ExecuteStatement("Causale = """ & Causale & """")
            If InStr(MyValori, "69,") > 0 Then Sc.ExecuteStatement("Colore = " & Colore)
            If InStr(MyValori, "60,") > 0 Then Sc.ExecuteStatement("FlagGiornoSuccessivo = """ & FlagGiornoSuccessivo & """")
            If InStr(MyValori, "80,") > 0 Then Sc.ExecuteStatement("NelGruppo = """ & NelGruppo & """")
        Else
            If InStr(MyValori, "08,") > 0 Then Sc.ExecuteStatement("Giustificativo = """ & Left(cxst.Vettore_Calcolo_Orari.Elemento(i, 0), InStr(cxst.Vettore_Calcolo_Orari.Elemento(i, 0), "/") - 1) & """")
            If InStr(MyValori, "70,") > 0 Then
                Familiare = Mid(cxst.Vettore_Calcolo_Orari.Elemento(i, 0), InStr(cxst.Vettore_Calcolo_Orari.Elemento(i, 0), "/") + 1)
                Sc.ExecuteStatement("Familiare = " & Familiare)
            End If
            If InStr(MyValori, "44,") > 0 Then Sc.ExecuteStatement("Tempo = " & cxst.Sostituisci(cxst.Vettore_Calcolo_Orari.Elemento(i, Day(Data)), ",", "."))
            If InStr(MyValori, "82,") > 0 Then Sc.ExecuteStatement("Work_1 = " & cxst.Sostituisci(cxst.Vettore_Calcolo_Orari.Elemento(41, Day(Data)), ",", "."))
            If InStr(MyValori, "83,") > 0 Then Sc.ExecuteStatement("Work_2 = " & cxst.Sostituisci(cxst.Vettore_Calcolo_Orari.Elemento(42, Day(Data)), ",", "."))
            If InStr(MyValori, "46,") > 0 Then Sc.ExecuteStatement("TempoPrevisto = " & cxst.Sostituisci(cxst.Vettore_Calcolo_Orari.Elemento(43, Day(Data)), ",", "."))
            If InStr(MyValori, "48,") > 0 Then Sc.ExecuteStatement("Pausa = " & cxst.Sostituisci(cxst.Vettore_Calcolo_Orari.Elemento(44, Day(Data)), ",", "."))
            If InStr(MyValori, "38,") > 0 Then Sc.ExecuteStatement("TempoGiorno = " & cxst.Sostituisci(cxst.Vettore_Calcolo_Orari.Elemento(45, Day(Data)), ",", "."))
            If InStr(MyValori, "73,") > 0 Then Sc.ExecuteStatement("TempoRetribuito = " & cxst.Sostituisci(cxst.Vettore_Calcolo_Orari.Elemento(50, Day(Data)), ",", "."))
            If InStr(MyValori, "50,") > 0 Then Sc.ExecuteStatement("Diurno = " & cxst.Sostituisci(cxst.Vettore_Calcolo_Orari.Elemento(46, Day(Data)), ",", "."))
            If InStr(MyValori, "51,") > 0 Then Sc.ExecuteStatement("FestivoDiurno = " & cxst.Sostituisci(cxst.Vettore_Calcolo_Orari.Elemento(48, Day(Data)), ",", "."))
            If InStr(MyValori, "52,") > 0 Then Sc.ExecuteStatement("Notturno = " & cxst.Sostituisci(cxst.Vettore_Calcolo_Orari.Elemento(47, Day(Data)), ",", "."))
            If InStr(MyValori, "53,") > 0 Then Sc.ExecuteStatement("FestivoNotturno = " & cxst.Sostituisci(cxst.Vettore_Calcolo_Orari.Elemento(49, Day(Data)), ",", "."))
            If InStr(MyValori, "20,") > 0 Then Sc.ExecuteStatement("OrarioDalle = 00.00")
            If InStr(MyValori, "21,") > 0 Then Sc.ExecuteStatement("OrarioAlle = 00.00")
            If InStr(MyValori, "80,") > 0 Then Sc.ExecuteStatement("NelGruppo = """"")
        End If
        If InStr(MyValori, "30,") > 0 Then Sc.ExecuteStatement("Errori = """"")
        If InStr(MyValori, "01,") > 0 Then Sc.ExecuteStatement("Fase = """ & Fase & """")
        If InStr(MyValori, "41,") > 0 Then Sc.ExecuteStatement("TempoMese = " & cxst.Sostituisci(W_TempoMese, ",", "."))
        If InStr(MyValori, "45,") > 0 Then Sc.ExecuteStatement("TempoStandard = " & cxst.Sostituisci(W_TempoStandard, ",", "."))
        If InStr(MyValori, "22,") > 0 Then Sc.ExecuteStatement("OrarioDomenica = " & Format(W_OrarioDomenica, "HH.mm"))
        If InStr(MyValori, "23,") > 0 Then Sc.ExecuteStatement("OrarioLunedi = " & Format(W_OrarioLunedi, "HH.mm"))
        If InStr(MyValori, "24,") > 0 Then Sc.ExecuteStatement("OrarioMartedi = " & Format(W_OrarioMartedi, "HH.mm"))
        If InStr(MyValori, "25,") > 0 Then Sc.ExecuteStatement("OrarioMercoledi = " & Format(W_OrarioMercoledi, "HH.mm"))
        If InStr(MyValori, "26,") > 0 Then Sc.ExecuteStatement("OrarioGiovedi = " & Format(W_OrarioGiovedi, "HH.mm"))
        If InStr(MyValori, "27,") > 0 Then Sc.ExecuteStatement("OrarioVenerdi = " & Format(W_OrarioVenerdi, "HH.mm"))
        If InStr(MyValori, "28,") > 0 Then Sc.ExecuteStatement("OrarioSabato = " & Format(W_OrarioSabato, "HH.mm"))
        If InStr(MyValori, "78,") > 0 Then Sc.ExecuteStatement("OrarioDelGiorno = " & Format(W_OrarioDelGiorno, "HH.mm"))
        If InStr(MyValori, "29,") > 0 Then Sc.ExecuteStatement("OrarioContrattuale = " & cxst.Sostituisci(W_OrarioContrattuale, ",", "."))
        If InStr(MyValori, "64,") > 0 Then Sc.ExecuteStatement("PercentualePartimeGiorni = " & cxst.Sostituisci(W_PercentualePartimeGiorni, ",", "."))
        If InStr(MyValori, "68,") > 0 Then Sc.ExecuteStatement("PercentualePartimeGiorniDelMese = " & cxst.Sostituisci(W_PercentualePartimeGiorniDelMese, ",", "."))
        If InStr(MyValori, "76,") > 0 Then Sc.ExecuteStatement("PercentualePartimeOre = " & cxst.Sostituisci(W_PercentualePartimeOre, ",", "."))
        If InStr(MyValori, "77,") > 0 Then Sc.ExecuteStatement("PercentualePartimeOreDelMese = " & cxst.Sostituisci(W_PercentualePartimeOreDelMese, ",", "."))
        If InStr(MyValori, "79,") > 0 Then Sc.ExecuteStatement("TipoPartime = """ & W_TipoPartime & """")
        If InStr(MyValori, "02,") > 0 Then Sc.ExecuteStatement("Oggi = #" & Format(Data, "MM/dd/yyyy") & "#")
        If InStr(MyValori, "31,") > 0 Then Sc.ExecuteStatement("Giorno = """ & GiornoSettimana(Weekday(Data)) & """")
        If InStr(MyValori, "32,") > 0 Then Sc.ExecuteStatement("GiornoPrecedente = """ & GiornoSettimana(Weekday(DateAdd("d", -1, Data))) & """")
        If InStr(MyValori, "33,") > 0 Then Sc.ExecuteStatement("GiornoSuccessivo = """ & GiornoSettimana(Weekday(DateAdd("d", 1, Data))) & """")
        If InStr(MyValori, "63,") > 0 Then Sc.ExecuteStatement("FestivitaSettimanale = """ & GiornoSettimana(FestivitaSettimanale) & """")
        If InStr(MyValori, "65,") > 0 Then Sc.ExecuteStatement("GiornoPrecedenteFestivitaSettimanale = """ & GiornoSettimana(GiornoPrecedenteFestivitaSettimanale) & """")
        If InStr(MyValori, "66,") > 0 Then Sc.ExecuteStatement("GiornoSuccessivoFestivitaSettimanale = """ & GiornoSettimana(GiornoSuccessivoFestivitaSettimanale) & """")
        If InStr(MyValori, "34,") > 0 Then
            If cxst.GiornoFestivo(DateAdd("d", -1, Data), CodiceSuperGruppo) = True Then
                Sc.ExecuteStatement("GiornoPrecedenteFestivo = ""S""")
            Else
                Sc.ExecuteStatement("GiornoPrecedenteFestivo = ""N""")
            End If
        End If
        If InStr(MyValori, "35,") > 0 Then
            If cxst.GiornoFestivo(Data, CodiceSuperGruppo) = True Then
                Sc.ExecuteStatement("GiornoFestivo = ""S""")
            Else
                Sc.ExecuteStatement("GiornoFestivo = ""N""")
            End If
        End If
        If InStr(MyValori, "36,") > 0 Then
            If cxst.GiornoFestivo(DateAdd("d", 1, Data), CodiceSuperGruppo) = True Then
                Sc.ExecuteStatement("GiornoSuccessivoFestivo = ""S""")
            Else
                Sc.ExecuteStatement("GiornoSuccessivoFestivo = ""N""")
            End If
        End If
        If InStr(MyValori, "54,") > 0 Then
            Sc.ExecuteStatement("GiornoPartime = """ & Giorno_Partime(Data, CodiceContratto) & """")
        End If

        If InStr(MyValori, "37,") > 0 Then Sc.ExecuteStatement("UltimoGiorno = " & GiorniMese(Month(Data), Year(Data)))
        If InStr(MyValori, "39,") > 0 Then Sc.ExecuteStatement("Mese = " & Month(Data))
        If InStr(MyValori, "40,") > 0 Then Sc.ExecuteStatement("ValiditaMese = #" & Format(DateSerial(Year(Data), Month(Data), GiorniMese(Month(Data), Year(Data))), "MM/dd/yyyy") & "#")
        If InStr(MyValori, "67,") > 0 Then
            Validita = DateSerial(Year(Data), Month(Data), GiorniMese(Month(Data), Year(Data)))
            If CodiceDipendente > 0 Then
                DataLicenziamento = cxst.LeggiDataLicenziamento(CodiceDipendente, Validita)
                Dim InizioMese As Date = DateSerial(Year(Data), Month(Data), 1)
                If Format(DataLicenziamento, "yyyyMMdd") >= Format(InizioMese, "yyyyMMdd") And Format(DataLicenziamento, "yyyyMMdd") <= Format(Validita, "yyyyMMdd") Then
                    If Format(DataLicenziamento, "yyyyMMdd") >= Format(Data, "yyyyMMdd") Then Validita = DataLicenziamento
                End If
            End If
            Sc.ExecuteStatement("Validita = #" & Format(Validita, "MM/dd/yyyy") & "#")
        End If
        If InStr(MyValori, "61,") > 0 Then
            Dim MesePrec As Date = DateAdd("m", -1, Data)
            Sc.ExecuteStatement("ValiditaMesePrecedente = #" & Format(DateSerial(Year(MesePrec), Month(MesePrec), GiorniMese(Month(MesePrec), Year(MesePrec))), "MM/dd/yyyy") & "#")
        End If
        If InStr(MyValori, "42,") > 0 Then Sc.ExecuteStatement("Anno = " & Year(Data))
        If InStr(MyValori, "43,") > 0 Then Sc.ExecuteStatement("ValiditaAnno = #" & Format(DateSerial(Year(Data), 12, 31), "MM/dd/yyyy") & "#")
        If InStr(MyValori, "07,") > 0 Then Sc.ExecuteStatement("Validita1900 = #" & Format(DateSerial(1900, 1, 1), "MM/dd/yyyy") & "#")
        If InStr(MyValori, "05,") > 0 Then
            DataAssunzione = cxst.LeggiDataAssunzione(CodiceDipendente, Data)
            DataLicenziamento = cxst.LeggiDataLicenziamento(CodiceDipendente, Data)
            If Format(DataAssunzione, "yyyyMMdd") <= Format(DataLicenziamento, "yyyyMMdd") Then
                DataAssunzione = cxst.LeggiDataAssunzione(CodiceDipendente, DateSerial(Year(Data), Month(Data), GiorniMese(Month(Data), Year(Data))))
            End If
            Sc.ExecuteStatement("DataAssunzione = #" & Format(DataAssunzione, "MM/dd/yyyy") & "#")
        End If
        If InStr(MyValori, "06,") > 0 Then Sc.ExecuteStatement("DataLicenziamento = #" & Format(cxst.LeggiDataLicenziamento(CodiceDipendente, Data), "MM/dd/yyyy") & "#")

        If InStr(MyValori, "03,") > 0 Then
            DataTest = cd.CampoDipendenti(ConnectionString, CodiceDipendente, "DataNascita")
            If IsDate(DataTest) = False Then DataTest = "30/12/1899"
            Sc.ExecuteStatement("DataNascita = #" & Format(DataTest, "MM/dd/yyyy") & "#")
        End If
        If InStr(MyValori, "04,") > 0 Then Sc.ExecuteStatement("Sesso = """ & cd.CampoDipendenti(ConnectionString, CodiceDipendente, "Sesso") & """")
        If InStr(MyValori, "71,") > 0 Then
            DataTest = cf.CampoFamiliari(ConnectionString, CodiceDipendente, Familiare, "DataNascita")
            If IsDate(DataTest) = False Then DataTest = "30/12/1899"
            Sc.ExecuteStatement("DataNascitaFamiliare = #" & Format(DataTest, "MM/dd/yyyy") & "#")
        End If
        If InStr(MyValori, "72,") > 0 Then
            Sc.ExecuteStatement("FamiliareDipendente = " & Val(cf.CampoFamiliari(ConnectionString, CodiceDipendente, Familiare, "CodiceFamiliareDipendente")))
        End If
        If InStr(MyValori, "75,") > 0 Then
            DataTest = cf.CampoFamiliari(ConnectionString, CodiceDipendente, Familiare, "DataAdozione")
            If IsDate(DataTest) = False Then DataTest = "30/12/1899"
            Sc.ExecuteStatement("DataAdozioneFamiliare = #" & Format(DataTest, "MM/dd/yyyy") & "#")
        End If
        ' + Mauro
        If InStr(MyValori, "49,") > 0 Then Sc.ExecuteStatement("OperazionePausa = """"")
        If InStr(MyValori, "47,") > 0 Then Sc.ExecuteStatement("OperazioneTempo = """"")
        If InStr(MyValori, "55,") > 0 Then Sc.ExecuteStatement("OperazioneOreLavorate = """"")
        If InStr(MyValori, "56,") > 0 Then Sc.ExecuteStatement("OperazioneOreFeriali = """"")
        If InStr(MyValori, "57,") > 0 Then Sc.ExecuteStatement("OperazioneOreFestive = """"")
        If InStr(MyValori, "74,") > 0 Then Sc.ExecuteStatement("OperazioneOreRetribuite = """"")
        ' -

        Dim a As String = Sc.Run("Calcolo")

        If InStr(MyValori, "10,") > 0 Then Variabile(1) = Sc.Eval("Variabile1")
        If InStr(MyValori, "11,") > 0 Then Variabile(2) = Sc.Eval("Variabile2")
        If InStr(MyValori, "12,") > 0 Then Variabile(3) = Sc.Eval("Variabile3")
        If InStr(MyValori, "13,") > 0 Then Variabile(4) = Sc.Eval("Variabile4")
        If InStr(MyValori, "14,") > 0 Then Variabile(5) = Sc.Eval("Variabile5")
        If InStr(MyValori, "15,") > 0 Then Variabile(6) = Sc.Eval("Variabile6")
        If InStr(MyValori, "16,") > 0 Then Variabile(7) = Sc.Eval("Variabile7")
        If InStr(MyValori, "17,") > 0 Then Variabile(8) = Sc.Eval("Variabile8")
        If InStr(MyValori, "18,") > 0 Then Variabile(9) = Sc.Eval("Variabile9")

        If Fase = "Orari" Then
            If InStr(MyValori, "48,") > 0 Then
                Numero = Sc.Eval("Pausa")
                Ore_Pausa = Format(TimeSerial(Int(Numero), (Numero - Int(Numero)) * 100, 0), "HH.mm")
            End If
            If InStr(MyValori, "44,") > 0 Then
                Numero = Sc.Eval("Tempo")
                Ore_Intervallo = Format(TimeSerial(Int(Numero), (Numero - Int(Numero)) * 100, 0), "HH.mm")
            End If
            If InStr(MyValori, "50,") > 0 Then
                Numero = Sc.Eval("Diurno")
                Ore_Diurne = Format(TimeSerial(Int(Numero), (Numero - Int(Numero)) * 100, 0), "HH.mm")
            End If
            If InStr(MyValori, "51,") > 0 Then
                Numero = Sc.Eval("FestivoDiurno")
                Ore_FestiveDiurne = Format(TimeSerial(Int(Numero), (Numero - Int(Numero)) * 100, 0), "HH.mm")
            End If
            If InStr(MyValori, "52,") > 0 Then
                Numero = Sc.Eval("Notturno")
                Ore_Notturne = Format(TimeSerial(Int(Numero), (Numero - Int(Numero)) * 100, 0), "HH.mm")
            End If
            If InStr(MyValori, "53,") > 0 Then
                Numero = Sc.Eval("FestivoNotturno")
                Ore_FestiveNotturne = Format(TimeSerial(Int(Numero), (Numero - Int(Numero)) * 100, 0), "HH.mm")
            End If
        Else
            If InStr(MyValori, "44,") > 0 Then
                Numero = Sc.Eval("Tempo")
                cxst.Vettore_Calcolo_Orari.Elemento(i, Day(Data)) = cxst.Sostituisci(Format(Numero, "00.00"), ",", ".")
            End If
            If InStr(MyValori, "82,") > 0 Then
                Numero = Sc.Eval("Work_1")
                Vettore_Calcolo_Orari.Elemento(41, Day(Data)) = cxst.Sostituisci(Format(Numero, "00.00"), ",", ".")
            End If
            If InStr(MyValori, "83,") > 0 Then
                Numero = Sc.Eval("Work_2")
                Vettore_Calcolo_Orari.Elemento(42, Day(Data)) = cxst.Sostituisci(Format(Numero, "00.00"), ",", ".")
            End If
            If InStr(MyValori, "46,") > 0 Then
                Numero = Sc.Eval("TempoPrevisto")
                cxst.Vettore_Calcolo_Orari.Elemento(43, Day(Data)) = cxst.Sostituisci(Format(Numero, "00.00"), ",", ".")
            End If
            If InStr(MyValori, "48,") > 0 Then
                Numero = Sc.Eval("Pausa")
                cxst.Vettore_Calcolo_Orari.Elemento(44, Day(Data)) = cxst.Sostituisci(Format(Numero, "00.00"), ",", ".")
            End If
            If InStr(MyValori, "38,") > 0 Then
                Numero = Sc.Eval("TempoGiorno")
                cxst.Vettore_Calcolo_Orari.Elemento(45, Day(Data)) = cxst.Sostituisci(Format(Numero, "00.00"), ",", ".")
            End If
            If InStr(MyValori, "50,") > 0 Then
                Numero = Sc.Eval("Diurno")
                cxst.Vettore_Calcolo_Orari.Elemento(46, Day(Data)) = cxst.Sostituisci(Format(Numero, "00.00"), ",", ".")
            End If
            If InStr(MyValori, "51,") > 0 Then
                Numero = Sc.Eval("FestivoDiurno")
                cxst.Vettore_Calcolo_Orari.Elemento(48, Day(Data)) = cxst.Sostituisci(Format(Numero, "00.00"), ",", ".")
            End If
            If InStr(MyValori, "52,") > 0 Then
                Numero = Sc.Eval("Notturno")
                cxst.Vettore_Calcolo_Orari.Elemento(47, Day(Data)) = cxst.Sostituisci(Format(Numero, "00.00"), ",", ".")
            End If
            If InStr(MyValori, "53,") > 0 Then
                Numero = Sc.Eval("FestivoNotturno")
                cxst.Vettore_Calcolo_Orari.Elemento(49, Day(Data)) = cxst.Sostituisci(Format(Numero, "00.00"), ",", ".")
            End If
            If InStr(MyValori, "73,") > 0 Then
                Numero = Sc.Eval("TempoRetribuito")
                cxst.Vettore_Calcolo_Orari.Elemento(50, Day(Data)) = cxst.Sostituisci(Format(Numero, "00.00"), ",", ".")
            End If
        End If
        ' + Mauro
        If InStr(MyValori, "49,") > 0 Then OperazionePausa = Sc.Eval("OperazionePausa")
        If InStr(MyValori, "47,") > 0 Then OperazioneTempo = Sc.Eval("OperazioneTempo")
        If InStr(MyValori, "55,") > 0 Then OperazioneOreLavorate = Sc.Eval("OperazioneOreLavorate")
        If InStr(MyValori, "56,") > 0 Then OperazioneOreFeriali = Sc.Eval("OperazioneOreFeriali")
        If InStr(MyValori, "57,") > 0 Then OperazioneOreFestive = Sc.Eval("OperazioneOreFestive")
        If InStr(MyValori, "74,") > 0 Then OperazioneOreRetribuite = Sc.Eval("OperazioneOreRetribuite")
        ' -
        If InStr(MyValori, "30,") > 0 Then
            Testo = Sc.Eval("Errori")
            If Testo <> "" Then
                Esegui_Calcolo_Orari = Testo & vbNewLine
            End If
        End If
        If InStr(MyValori, "41,") > 0 Then W_TempoMese = Math.Round(Sc.Eval("TempoMese"), 2)
        If InStr(MyValori, "45,") > 0 Then W_TempoStandard = Sc.Eval("TempoStandard")

        Vettore_Calcolo_Orari = cxst.Vettore_Calcolo_Orari

        On Error Resume Next
        If Val(Err()) <> 0 Then
            Esegui_Calcolo_Orari = Esegui_Calcolo_Orari & "(E) Nella formula: " & RegolaCalcolo & " errore script: " & Val(Err()) & vbNewLine
        End If
        On Error GoTo 0
    End Function

    Private Function PrevisteNelGiorno(ByVal MyDate As Date, ByRef Vettore_Orari As Cls_Struttura_Orari) As String
        Dim cpt As New Cls_ParametriTurni
        cpt.Leggi(ConnectionString)

        Dim cxst As New Cls_ClsXScriptTurni
        cxst.ConnectionString = ConnectionString
        cxst.Utente = Utente

        Dim g As Byte = Day(MyDate)
        Dim T_Tempo As Integer = 0
        Dim cg As New Cls_Giustificativi
        Dim Intervallo As Integer

        PrevisteNelGiorno = "00.00"

        For i = 0 To 9
            If Vettore_Orari.Dalle(i, g) = "00.00" And Vettore_Orari.Alle(i, g) = "00.00" And Vettore_Orari.Giustificativo(i, g) = "" Then Exit For
            If Vettore_Orari.Giustificativo(i, g) = "" Then Vettore_Orari.Giustificativo(i, g) = cpt.GiustificativoOrarioLavoro
            If cg.CampoGiustificativi(ConnectionString, Vettore_Orari.Giustificativo(i, g), "SuTempoPrevisto") = "S" Then
                If cg.CampoGiustificativi(ConnectionString, Vettore_Orari.Giustificativo(i, g), "Tipo") = "A" Then
                    If Vettore_Orari.Dalle(i, g) = "00.00" And Vettore_Orari.Alle(i, g) = "24.00" Then Vettore_Orari.Alle(i, g) = "00.00"
                End If
                If Vettore_Orari.Alle(i, g) = "24.00" Then
                    Intervallo = DateDiff("n", TimeSerial(Val(Mid(Vettore_Orari.Dalle(i, g), 1, 2)), Val(Mid(Vettore_Orari.Dalle(i, g), 4, 2)), 0), TimeSerial(23, 59, 0)) + 1
                Else
                    Intervallo = DateDiff("n", TimeSerial(Val(Mid(Vettore_Orari.Dalle(i, g), 1, 2)), Val(Mid(Vettore_Orari.Dalle(i, g), 4, 2)), 0), TimeSerial(Val(Mid(Vettore_Orari.Alle(i, g), 1, 2)), Val(Mid(Vettore_Orari.Alle(i, g), 4, 2)), 0))
                End If
                T_Tempo = T_Tempo + Intervallo
                If Vettore_Orari.Pausa(i, g) <> "00.00" Then
                    Intervallo = DateDiff("n", TimeSerial(0, 0, 0), TimeSerial(Val(Mid(Vettore_Orari.Pausa(i, g), 1, 2)), Val(Mid(Vettore_Orari.Pausa(i, g), 4, 2)), 0))
                    T_Tempo = T_Tempo - Intervallo
                End If
            End If
        Next i
        Ore = Int(T_Tempo / 60)
        Minuti = T_Tempo - (Ore * 60)
        PrevisteNelGiorno = Format(Ore, "00") & "." & Format(Minuti, "00")
    End Function

    Private Function SalvaCalcoloOrari(ByVal Dipendente As Long, ByVal Anno As Integer, ByVal Mese As Byte, ByRef Vettore_Calcolo_Orari As Cls_Struttura_Calcolo_Orari) As String
        Dim cg As New Cls_Giustificativi
        Dim cf As New Cls_Familiari

        Dim OkS(50) As String

        '
        ' Controllo se il Vettore_Calcolo_Orari è stato variato
        '
        Dim MyTempoStandard As Single = 0
        Dim MyTempoMese As Single = 0
        Dim FlagCollegato As String = ""
        Dim Indice As Byte
        Dim MaxIndice As Byte
        Dim CodRic As String
        Dim CodZero As String = ""
        Dim CodFam As String
        Dim i As Byte
        Dim ii As Integer

        SalvaCalcoloOrari = ""

        Sw_Ok = True
        For i = 0 To 50
            If Vettore_Calcolo_Orari.Elemento(i, 0) <> "" Then
                For r = 1 To 31
                    If InStr(Vettore_Calcolo_Orari.Elemento(i, r), "-") > 0 Then
                        SalvaCalcoloOrari = SalvaCalcoloOrari & "(E) Nella colonna: " & Vettore_Calcolo_Orari.Elemento(i, 0) & " il giorno: " & r & " ha valore negativo " & vbNewLine
                        Sw_Ok = False
                    End If
                Next r
            End If
        Next i

        If Sw_Ok = False Then Exit Function

        Dim cn As OleDbConnection
        cn = New Data.OleDb.OleDbConnection(ConnectionString)

        cn.Open()
        Dim cmd As New OleDbCommand()
        cmd.CommandText = "SELECT * FROM SalvaCalcoloOrari" & _
                          " WHERE Anno = ?" & _
                          " AND Mese = ?" & _
                          " AND CodiceDipendente = ?" & _
                          " ORDER BY Indice"
        cmd.Parameters.AddWithValue("@Anno", Anno)
        cmd.Parameters.AddWithValue("@Mese", Mese)
        cmd.Parameters.AddWithValue("@CodiceDipendente", Dipendente)

        cmd.Connection = cn
        Dim myPOSTreader As OleDbDataReader = cmd.ExecuteReader()
        Do While myPOSTreader.Read
            Indice = NumeroDb(myPOSTreader.Item("Indice"))
            CodRic = StringaDb(myPOSTreader.Item("Elemento_0")) & "/" & NumeroDb(myPOSTreader.Item("CodiceFamiliare"))
            MyTempoStandard = NumeroDb(myPOSTreader.Item("TempoStandard"))
            MyTempoMese = NumeroDb(myPOSTreader.Item("TempoMese"))
            FlagCollegato = StringaDb(myPOSTreader.Item("FlagCollegato"))
            If Indice < 41 Then
                MaxIndice = Indice
                For i = 0 To 40
                    If CodRic = Vettore_Calcolo_Orari.Elemento(i, 0) Then
                        For r = 1 To 31
                            If StringaDb(myPOSTreader.Item("Elemento_" & r)) <> Vettore_Calcolo_Orari.Elemento(i, r) Then Exit Do
                        Next r
                        OkS(i) = "S"
                    End If
                Next i
            Else
                i = Indice
                For r = 0 To 31
                    If StringaDb(myPOSTreader.Item("Elemento_" & r)) <> Vettore_Calcolo_Orari.Elemento(i, r) Then Exit Do
                Next r
                OkS(i) = "S"
            End If
        Loop
        myPOSTreader.Close()
        cmd.Parameters.Clear()

        If Vettore_Calcolo_Orari.Elemento(MaxIndice, 0) = "" Or Vettore_Calcolo_Orari.Elemento(MaxIndice + 1, 0) <> "" Then
            Sw_Ok = False
        Else
            Sw_Ok = True
            For i = 0 To 40
                If Vettore_Calcolo_Orari.Elemento(i, 0) <> "" Then
                    If OkS(i) <> "S" Then
                        Sw_Ok = False
                        Exit For
                    End If
                End If
            Next i

            For i = 41 To 50
                If OkS(i) <> "S" Then
                    Sw_Ok = False
                    Exit For
                End If
            Next i
        End If

        If Sw_Ok = False Then FlagCollegato = ""

        If Sw_Ok = True And MyTempoStandard = W_TempoStandard And MyTempoMese = W_TempoMese Then
            For i = 0 To 40
                If Vettore_Calcolo_Orari.Elemento(i, 0) <> "" Then
                    CodZero = Left(Vettore_Calcolo_Orari.Elemento(i, 0), InStr(Vettore_Calcolo_Orari.Elemento(i, 0), "/") - 1)
                    CodFam = Mid(Vettore_Calcolo_Orari.Elemento(i, 0), InStr(Vettore_Calcolo_Orari.Elemento(i, 0), "/") + 1)
                    Vettore_Calcolo_Orari.Elemento(i, 0) = cg.DecodificaGiustificativo(ConnectionString, CodZero)
                    If CodFam <> 0 Then
                        Vettore_Calcolo_Orari.Elemento(i, 0) = Vettore_Calcolo_Orari.Elemento(i, 0) & " / " & cf.CampoFamiliari(ConnectionString, Dipendente, CodFam, "Cognome") & " " & cf.CampoFamiliari(ConnectionString, Dipendente, CodFam, "Nome")
                    End If
                End If
            Next i

        Else

            ii = -1

            cmd.CommandText = "DELETE FROM SalvaCalcoloOrari" & _
                              " WHERE Anno = ?" & _
                              " AND Mese = ?" & _
                              " AND CodiceDipendente = ?"
            cmd.Parameters.AddWithValue("@Anno", Anno)
            cmd.Parameters.AddWithValue("@Mese", Mese)
            cmd.Parameters.AddWithValue("@CodiceDipendente", Dipendente)
            cmd.Connection = cn
            cmd.ExecuteNonQuery()
            cmd.Parameters.Clear()

            For i = 0 To 40
                If Vettore_Calcolo_Orari.Elemento(i, 0) <> "" Then
                    Sw_Ok = False
                    CodZero = Left(Vettore_Calcolo_Orari.Elemento(i, 0), InStr(Vettore_Calcolo_Orari.Elemento(i, 0), "/") - 1)
                    CodFam = Mid(Vettore_Calcolo_Orari.Elemento(i, 0), InStr(Vettore_Calcolo_Orari.Elemento(i, 0), "/") + 1)
                    For r = 1 To 31
                        If Vettore_Calcolo_Orari.Elemento(i, r) <> "" Then
                            Sw_Ok = True
                            Exit For
                        End If
                    Next r
                    If Sw_Ok = True Then
                        ii = ii + 1

                        cmd.CommandText = "INSERT INTO SalvaCalcoloOrari (Utente, DataAggiornamento, Anno, Mese, CodiceDipendente, CodiceFamiliare, Indice, Elemento_0, Elemento_1, Elemento_2, Elemento_3, Elemento_4, Elemento_5, Elemento_6, Elemento_7, Elemento_8, Elemento_9, Elemento_10, Elemento_11, Elemento_12, Elemento_13, Elemento_14, Elemento_15, Elemento_16, Elemento_17, Elemento_18, Elemento_19, Elemento_20, Elemento_21, Elemento_22, Elemento_23, Elemento_24, Elemento_25, Elemento_26, Elemento_27, Elemento_28, Elemento_29, Elemento_30, Elemento_31, TempoStandard, TempoMese, FlagCollegato) VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)"

                        cmd.Parameters.AddWithValue("@Utente", Utente)
                        cmd.Parameters.AddWithValue("@DataAggiornamento", Now)
                        cmd.Parameters.AddWithValue("@Anno", Anno)
                        cmd.Parameters.AddWithValue("@Mese", Mese)
                        cmd.Parameters.AddWithValue("@CodiceDipendente", Dipendente)
                        cmd.Parameters.AddWithValue("@CodiceFamiliare", CodFam)
                        cmd.Parameters.AddWithValue("@Indice", ii)
                        cmd.Parameters.AddWithValue("@Elemento_0", CodZero)
                        For r = 1 To 31
                            cmd.Parameters.AddWithValue("@Elemento_" & r, Vettore_Calcolo_Orari.Elemento(i, r))
                        Next r
                        cmd.Parameters.AddWithValue("@TempoStandard", W_TempoStandard)
                        cmd.Parameters.AddWithValue("@TempoMese", W_TempoMese)
                        cmd.Parameters.AddWithValue("@FlagCollegato", FlagCollegato)

                        cmd.Connection = cn
                        cmd.ExecuteNonQuery()
                        cmd.Parameters.Clear()
                    End If
                    Vettore_Calcolo_Orari.Elemento(i, 0) = cg.DecodificaGiustificativo(ConnectionString, CodZero)
                    If CodFam <> 0 Then
                        Vettore_Calcolo_Orari.Elemento(i, 0) = Vettore_Calcolo_Orari.Elemento(i, 0) & " / " & cf.CampoFamiliari(ConnectionString, Dipendente, CodFam, "Cognome") & " " & cf.CampoFamiliari(ConnectionString, Dipendente, CodFam, "Nome")
                    End If
                End If
            Next i
            For i = 41 To 50
                cmd.CommandText = "INSERT INTO SalvaCalcoloOrari (Utente, DataAggiornamento, Anno, Mese, CodiceDipendente, CodiceFamiliare, Indice, Elemento_0, Elemento_1, Elemento_2, Elemento_3, Elemento_4, Elemento_5, Elemento_6, Elemento_7, Elemento_8, Elemento_9, Elemento_10, Elemento_11, Elemento_12, Elemento_13, Elemento_14, Elemento_15, Elemento_16, Elemento_17, Elemento_18, Elemento_19, Elemento_20, Elemento_21, Elemento_22, Elemento_23, Elemento_24, Elemento_25, Elemento_26, Elemento_27, Elemento_28, Elemento_29, Elemento_30, Elemento_31, TempoStandard, TempoMese, FlagCollegato) VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)"

                cmd.Parameters.AddWithValue("@Utente", Utente)
                cmd.Parameters.AddWithValue("@DataAggiornamento", Now)
                cmd.Parameters.AddWithValue("@Anno", Anno)
                cmd.Parameters.AddWithValue("@Mese", Mese)
                cmd.Parameters.AddWithValue("@CodiceDipendente", Dipendente)
                cmd.Parameters.AddWithValue("@CodiceFamiliare", 0)
                cmd.Parameters.AddWithValue("@Indice", i)
                cmd.Parameters.AddWithValue("@Elemento_0", CodZero)
                For r = 1 To 31
                    cmd.Parameters.AddWithValue("@Elemento_" & r, Vettore_Calcolo_Orari.Elemento(i, r))
                Next r
                cmd.Parameters.AddWithValue("@TempoStandard", W_TempoStandard)
                cmd.Parameters.AddWithValue("@TempoMese", W_TempoMese)
                cmd.Parameters.AddWithValue("@FlagCollegato", FlagCollegato)

                cmd.Connection = cn
                cmd.ExecuteNonQuery()
                cmd.Parameters.Clear()
            Next i
        End If
    End Function

    Private Sub AggiornaFlessibilitaOraria(ByVal Dipendente As Long, ByVal Anno As Integer, ByVal Mese As Byte, ByVal h As Long, ByVal g As Long, ByRef Delta As Object, ByRef DeltaGiustificativo As Object, ByRef DeltaNelGruppo As Object, ByRef DeltaFamiliare As Object, ByRef DeltaTipo As Object, ByRef DeltaTipoServizio As Object, ByRef DeltaGiornoSuccessivo As Object, ByRef Vettore_Orari As Cls_Struttura_Orari)
        Dim cg As New Cls_Giustificativi

        Dim Sw_Intero As Boolean
        '
        ' L'aggiornamento deve essere eseguito anche con Delta a zero
        '
        If Vettore_Orari.Tipo(h, g) <> "P" Then
            Delta = 0
            Exit Sub
        End If
        If Delta < 0 Then
            Delta = Delta * -1
            W_Tempo = Math.Round(Int(Delta) * 60, 0) + Math.Round((Delta - Int(Delta)) * 100, 0)
            W_Tempo = W_Tempo * -1
        Else
            W_Tempo = Math.Round(Int(Delta) * 60, 0) + Math.Round((Delta - Int(Delta)) * 100, 0)
        End If
        W_Alle = Vettore_Orari.Dalle(h, g)
        For h = h To 9
            If Vettore_Orari.Tipo(h, g) <> "I" Then
                If W_Alle <> Vettore_Orari.Dalle(h, g) Then Exit For
                If Vettore_Orari.Dalle(h, g) <> "00.00" And Vettore_Orari.Alle(h, g) = "00.00" Then Vettore_Orari.Alle(h, g) = "24.00"
                Sw_Intero = False
                If Vettore_Orari.Dalle(h, g) = "00.00" And Vettore_Orari.Alle(h, g) = "24.00" Then Sw_Intero = True
                W_Alle = Vettore_Orari.Alle(h, g)
                '      If InStr(1, Vettore_Orari.DCT(h, g), "D") = 0 Then
                If Vettore_Orari.Dalle(h, g) = "00.00" Then
                    If DeltaGiustificativo <> "" Then
                        If cg.CampoGiustificativi(ConnectionString, DeltaGiustificativo, "LavoroOrdinario") = "S" Then
                            If DalleZeroZero(Dipendente, Anno, Mese, h, g, Vettore_Orari) = True Then
                                Vettore_Orari.Dalle(h, g) = Format(DateAdd("n", W_Tempo, TimeSerial(Val(Mid(Vettore_Orari.Dalle(h, g), 1, 2)), Val(Mid(Vettore_Orari.Dalle(h, g), 4, 2)), 0)), "HH.mm")
                                Vettore_Orari.DalleAlto(h, g) = Format(DateAdd("n", W_Tempo, TimeSerial(Val(Mid(Vettore_Orari.DalleAlto(h, g), 1, 2)), Val(Mid(Vettore_Orari.DalleAlto(h, g), 4, 2)), 0)), "HH.mm")
                                Vettore_Orari.DalleBasso(h, g) = Format(DateAdd("n", W_Tempo, TimeSerial(Val(Mid(Vettore_Orari.DalleBasso(h, g), 1, 2)), Val(Mid(Vettore_Orari.DalleBasso(h, g), 4, 2)), 0)), "HH.mm")
                            End If
                        End If
                    Else
                        Vettore_Orari.Dalle(h, g) = Format(DateAdd("n", W_Tempo, TimeSerial(Val(Mid(Vettore_Orari.Dalle(h, g), 1, 2)), Val(Mid(Vettore_Orari.Dalle(h, g), 4, 2)), 0)), "HH.mm")
                        Vettore_Orari.DalleAlto(h, g) = Format(DateAdd("n", W_Tempo, TimeSerial(Val(Mid(Vettore_Orari.DalleAlto(h, g), 1, 2)), Val(Mid(Vettore_Orari.DalleAlto(h, g), 4, 2)), 0)), "HH.mm")
                        Vettore_Orari.DalleBasso(h, g) = Format(DateAdd("n", W_Tempo, TimeSerial(Val(Mid(Vettore_Orari.DalleBasso(h, g), 1, 2)), Val(Mid(Vettore_Orari.DalleBasso(h, g), 4, 2)), 0)), "HH.mm")
                    End If
                Else
                    Vettore_Orari.Dalle(h, g) = Format(DateAdd("n", W_Tempo, TimeSerial(Val(Mid(Vettore_Orari.Dalle(h, g), 1, 2)), Val(Mid(Vettore_Orari.Dalle(h, g), 4, 2)), 0)), "HH.mm")
                    Vettore_Orari.DalleAlto(h, g) = Format(DateAdd("n", W_Tempo, TimeSerial(Val(Mid(Vettore_Orari.DalleAlto(h, g), 1, 2)), Val(Mid(Vettore_Orari.DalleAlto(h, g), 4, 2)), 0)), "HH.mm")
                    Vettore_Orari.DalleBasso(h, g) = Format(DateAdd("n", W_Tempo, TimeSerial(Val(Mid(Vettore_Orari.DalleBasso(h, g), 1, 2)), Val(Mid(Vettore_Orari.DalleBasso(h, g), 4, 2)), 0)), "HH.mm")
                End If
                '      End If
                '      If InStr(1, Vettore_Orari.DCT(h, g), "A") = 0 Then
                If Sw_Intero = False Then
                    Vettore_Orari.Alle(h, g) = Format(DateAdd("n", W_Tempo, TimeSerial(Val(Mid(Vettore_Orari.Alle(h, g), 1, 2)), Val(Mid(Vettore_Orari.Alle(h, g), 4, 2)), 0)), "HH.mm")
                    Vettore_Orari.AlleAlto(h, g) = Format(DateAdd("n", W_Tempo, TimeSerial(Val(Mid(Vettore_Orari.AlleAlto(h, g), 1, 2)), Val(Mid(Vettore_Orari.AlleAlto(h, g), 4, 2)), 0)), "HH.mm")
                    Vettore_Orari.AlleBasso(h, g) = Format(DateAdd("n", W_Tempo, TimeSerial(Val(Mid(Vettore_Orari.AlleBasso(h, g), 1, 2)), Val(Mid(Vettore_Orari.AlleBasso(h, g), 4, 2)), 0)), "HH.mm")
                    If Vettore_Orari.Alle(h, g) < Vettore_Orari.Dalle(h, g) Then
                        Vettore_Orari.Alle(h, g) = "24.00"
                        Vettore_Orari.AlleAlto(h, g) = "24.00"
                        Vettore_Orari.AlleBasso(h, g) = "24.00"
                    End If
                Else
                    Delta = 0
                End If
                '      End If
                DeltaGiustificativo = Vettore_Orari.Giustificativo(h, g)
                DeltaNelGruppo = Vettore_Orari.NelGruppo(h, g)
                DeltaFamiliare = Vettore_Orari.Familiare(h, g)
                DeltaTipo = Vettore_Orari.Tipo(h, g)
                DeltaTipoServizio = Vettore_Orari.TipoServizio(h, g)
                DeltaGiornoSuccessivo = Vettore_Orari.GiornoSuccessivo(h, g)
            End If
        Next h
        If W_Alle <> "24.00" Then
            Delta = 0
            DeltaGiustificativo = ""
            DeltaNelGruppo = ""
            DeltaFamiliare = 0
            DeltaTipo = ""
            DeltaTipoServizio = ""
            DeltaGiornoSuccessivo = ""
        End If
    End Sub

    Private Function AbbinaOrariTimbrature(ByVal Dipendente As Long, ByRef g As Object, ByRef x As Object, ByRef XR As Object, ByRef Vettore_Timbrature As Cls_Struttura_Timbrature, ByRef Vettore_Orari_Timbrature As Cls_Struttura_Orari_Timbrature, ByRef Orario As Cls_Struttura_Orario, ByVal ControlloOrario As String) As String
        Dim Orario_Timbratura As New Cls_Struttura_Orario_Timbratura
        Dim cpt As New Cls_ParametriTurni
        cpt.Leggi(ConnectionString)

        Dim W_Alle As String = "00.00"

        Orario_Timbratura.Pulisci()

        Dim W_h As Byte = 10

        Dim h As Byte = 0
        Dim Sw_Fine As Boolean = False

        Do Until h > 9
            If Sw_Fine = True Then Exit Do
            If Vettore_Timbrature.Dalle(h, g) = "" And Vettore_Timbrature.Alle(h, g) = "" And Vettore_Timbrature.Dalle_Causale(h, g) = "" And Vettore_Timbrature.Alle_Causale(h, g) = "" Then Exit Do
            ' Timbrature.Dalle < Orari.Dalle
            If Vettore_Timbrature.Dalle(h, g) < Orario.Dalle Then
                If Vettore_Timbrature.Alle(h, g) = "" Or Vettore_Timbrature.Alle(h, g) > Orario.Dalle Then
                    If Orario.PU = "P" Then
                        Orario_Timbratura.Dalle = Vettore_Timbrature.Dalle(h, g)
                        Orario_Timbratura.Causale = Vettore_Timbrature.Dalle_Causale(h, g)
                        Orario_Timbratura.Colore = Vettore_Timbrature.Dalle_Colore(h, g)
                        If ControlloOrario = "S" Then
                            Orario_Timbratura.Tipo = "EntrataAnticipata"
                            Orario_Timbratura.Giustificativo = cpt.GiustificativoEntrataAnticipata
                        Else
                            Orario_Timbratura.Tipo = ""
                            Orario_Timbratura.Giustificativo = Orario.Giustificativo
                        End If
                        Orario_Timbratura.NelGruppo = Orario.NelGruppo
                        Orario_Timbratura.Familiare = Orario.Familiare
                        Orario_Timbratura.TipoServizio = Orario.TipoServizio
                        Orario_Timbratura.GiornoSuccessivo = Orario.GiornoSuccessivo
                        Orario_Timbratura.Alle = Orario.Dalle
                        If Orario_Timbratura.Causale = "" Then
                            Orario_Timbratura.Causale = Vettore_Timbrature.Alle_Causale(h, g)
                        End If
                        If Orario_Timbratura.Colore = vbWhite Then Orario_Timbratura.Colore = Vettore_Timbrature.Alle_Colore(h, g)
                        Risultato = AggiungiTurniTimbrature(Dipendente, g, x, XR, Orario_Timbratura, Vettore_Orari_Timbrature)
                        AbbinaOrariTimbrature = AbbinaOrariTimbrature & Risultato
                        Vettore_Timbrature.Dalle(h, g) = Orario.Dalle
                    End If
                    Orario_Timbratura.Dalle = Orario.Dalle
                    Orario_Timbratura.Causale = Vettore_Timbrature.Dalle_Causale(h, g)
                    Orario_Timbratura.Colore = Vettore_Timbrature.Dalle_Colore(h, g)
                    Orario_Timbratura.Giustificativo = Orario.Giustificativo
                    Orario_Timbratura.NelGruppo = Orario.NelGruppo
                    Orario_Timbratura.Familiare = Orario.Familiare
                    Orario_Timbratura.TipoServizio = Orario.TipoServizio
                    Orario_Timbratura.GiornoSuccessivo = Orario.GiornoSuccessivo
                    If Vettore_Timbrature.Alle(h, g) <> "" And Vettore_Timbrature.Alle(h, g) < Orario.Alle Then
                        Orario_Timbratura.Alle = Vettore_Timbrature.Alle(h, g)
                        Orario_Timbratura.Pausa = Orario.Pausa
                        If Orario_Timbratura.Causale = "" Then
                            Orario_Timbratura.Causale = Vettore_Timbrature.Alle_Causale(h, g)
                        End If
                        If Orario_Timbratura.Colore = vbWhite Then Orario_Timbratura.Colore = Vettore_Timbrature.Alle_Colore(h, g)
                        Orario.Dalle = Vettore_Timbrature.Alle(h, g)
                        Vettore_Timbrature.Alle(h, g) = Orario_Timbratura.Dalle
                        Risultato = AggiungiTurniTimbrature(Dipendente, g, x, XR, Orario_Timbratura, Vettore_Orari_Timbrature)
                        AbbinaOrariTimbrature = AbbinaOrariTimbrature & Risultato
                        If Vettore_Timbrature.Dalle(h, g) = Vettore_Timbrature.Alle(h, g) Then
                            Vettore_Timbrature.Dalle(h, g) = ""
                            Vettore_Timbrature.Dalle_Causale(h, g) = ""
                            Vettore_Timbrature.Dalle_Colore(h, g) = vbWhite
                            Vettore_Timbrature.Alle(h, g) = ""
                            Vettore_Timbrature.Alle_Causale(h, g) = ""
                            Vettore_Timbrature.Alle_Colore(h, g) = vbWhite
                        End If
                        h = h + 1
                    Else
                        Orario_Timbratura.Alle = Orario.Alle
                        Orario_Timbratura.Pausa = Orario.Pausa
                        If Orario_Timbratura.Causale = "" Then
                            Orario_Timbratura.Causale = Vettore_Timbrature.Alle_Causale(h, g)
                        End If
                        If Orario_Timbratura.Colore = vbWhite Then Orario_Timbratura.Colore = Vettore_Timbrature.Alle_Colore(h, g)
                        Risultato = AggiungiTurniTimbrature(Dipendente, g, x, XR, Orario_Timbratura, Vettore_Orari_Timbrature)
                        AbbinaOrariTimbrature = AbbinaOrariTimbrature & Risultato
                        If Orario.PU = "U" Then
                            If Orario.Alle <> "24.00" Then
                                Orario_Timbratura.Dalle = Orario.Alle
                                Orario_Timbratura.Causale = Vettore_Timbrature.Dalle_Causale(h, g)
                                Orario_Timbratura.Colore = Vettore_Timbrature.Dalle_Colore(h, g)
                                If ControlloOrario = "S" Then
                                    Orario_Timbratura.Tipo = "UscitaPosticipata"
                                    Orario_Timbratura.Giustificativo = cpt.GiustificativoUscitaPosticipata
                                Else
                                    Orario_Timbratura.Tipo = ""
                                    Orario_Timbratura.Giustificativo = Orario.Giustificativo
                                End If
                                Orario_Timbratura.NelGruppo = Orario.NelGruppo
                                Orario_Timbratura.Familiare = Orario.Familiare
                                Orario_Timbratura.TipoServizio = Orario.TipoServizio
                                Orario_Timbratura.GiornoSuccessivo = Orario.GiornoSuccessivo
                                Orario_Timbratura.Alle = Vettore_Timbrature.Alle(h, g)
                                If Orario_Timbratura.Causale = "" Then
                                    Orario_Timbratura.Causale = Vettore_Timbrature.Alle_Causale(h, g)
                                End If
                                If Orario_Timbratura.Colore = vbWhite Then Orario_Timbratura.Colore = Vettore_Timbrature.Alle_Colore(h, g)
                                Risultato = AggiungiTurniTimbrature(Dipendente, g, x, XR, Orario_Timbratura, Vettore_Orari_Timbrature)
                                AbbinaOrariTimbrature = AbbinaOrariTimbrature & Risultato
                            End If
                            If Orario.Dalle <= Vettore_Timbrature.Dalle(h, g) Then
                                Vettore_Timbrature.Dalle(h, g) = ""
                                Vettore_Timbrature.Dalle_Causale(h, g) = ""
                                Vettore_Timbrature.Dalle_Colore(h, g) = vbWhite
                                Vettore_Timbrature.Alle(h, g) = ""
                                Vettore_Timbrature.Alle_Causale(h, g) = ""
                                Vettore_Timbrature.Alle_Colore(h, g) = vbWhite
                            Else
                                Vettore_Timbrature.Alle(h, g) = Orario.Dalle
                            End If
                        Else
                            If Orario.PU = "P" Then
                                Vettore_Timbrature.Dalle(h, g) = Orario.Alle
                            Else
                                For i = 9 To h + 1 Step -1
                                    Vettore_Timbrature.Dalle(i, g) = Vettore_Timbrature.Dalle(i - 1, g)
                                    Vettore_Timbrature.Dalle_Causale(i, g) = Vettore_Timbrature.Dalle_Causale(i - 1, g)
                                    Vettore_Timbrature.Dalle_Colore(i, g) = Vettore_Timbrature.Dalle_Colore(i - 1, g)
                                    Vettore_Timbrature.Alle(i, g) = Vettore_Timbrature.Alle(i - 1, g)
                                    Vettore_Timbrature.Alle_Causale(i, g) = Vettore_Timbrature.Alle_Causale(i - 1, g)
                                    Vettore_Timbrature.Alle_Colore(i, g) = Vettore_Timbrature.Alle_Colore(i - 1, g)
                                Next i
                                Vettore_Timbrature.Alle(h, g) = Orario.Dalle
                                Vettore_Timbrature.Dalle(h + 1, g) = Orario.Alle
                            End If
                            Risultato = AggiungiTurniTimbrature(Dipendente, g, x, XR, Orario_Timbratura, Vettore_Orari_Timbrature)
                            AbbinaOrariTimbrature = AbbinaOrariTimbrature & Risultato
                        End If
                        Sw_Fine = True
                    End If
                Else
                    h = h + 1
                End If
            End If
            ' Timbrature.Dalle = Orari.Dalle
            If Sw_Fine = False Then
                If Vettore_Timbrature.Dalle(h, g) = Orario.Dalle Then
                    Orario_Timbratura.Dalle = Orario.Dalle
                    Orario_Timbratura.Causale = Vettore_Timbrature.Dalle_Causale(h, g)
                    Orario_Timbratura.Colore = Vettore_Timbrature.Dalle_Colore(h, g)
                    Orario_Timbratura.Giustificativo = Orario.Giustificativo
                    Orario_Timbratura.NelGruppo = Orario.NelGruppo
                    Orario_Timbratura.Familiare = Orario.Familiare
                    Orario_Timbratura.TipoServizio = Orario.TipoServizio
                    Orario_Timbratura.GiornoSuccessivo = Orario.GiornoSuccessivo
                    If (Vettore_Timbrature.Alle(h, g) = "" And Orario.Alle = "24.00") Or (Vettore_Timbrature.Alle(h, g) <> "" And Vettore_Timbrature.Alle(h, g) <= Orario.Alle) Then
                        Orario_Timbratura.Alle = Vettore_Timbrature.Alle(h, g)
                        Orario_Timbratura.Pausa = Orario.Pausa
                        If Orario_Timbratura.Causale = "" Then
                            Orario_Timbratura.Causale = Vettore_Timbrature.Alle_Causale(h, g)
                        End If
                        If Orario_Timbratura.Colore = vbWhite Then Orario_Timbratura.Colore = Vettore_Timbrature.Alle_Colore(h, g)
                        Orario.Dalle = Vettore_Timbrature.Alle(h, g)
                        Risultato = AggiungiTurniTimbrature(Dipendente, g, x, XR, Orario_Timbratura, Vettore_Orari_Timbrature)
                        AbbinaOrariTimbrature = AbbinaOrariTimbrature & Risultato
                        Vettore_Timbrature.Dalle(h, g) = ""
                        Vettore_Timbrature.Dalle_Causale(h, g) = ""
                        Vettore_Timbrature.Dalle_Colore(h, g) = vbWhite
                        Vettore_Timbrature.Alle(h, g) = ""
                        Vettore_Timbrature.Alle_Causale(h, g) = ""
                        Vettore_Timbrature.Alle_Colore(h, g) = vbWhite
                        h = h + 1
                    Else
                        Orario_Timbratura.Alle = Orario.Alle
                        Orario_Timbratura.Pausa = Orario.Pausa
                        If Orario_Timbratura.Causale = "" Then
                            Orario_Timbratura.Causale = Vettore_Timbrature.Alle_Causale(h, g)
                        End If
                        If Orario_Timbratura.Colore = vbWhite Then Orario_Timbratura.Colore = Vettore_Timbrature.Alle_Colore(h, g)
                        Vettore_Timbrature.Dalle(h, g) = Orario.Alle
                        Risultato = AggiungiTurniTimbrature(Dipendente, g, x, XR, Orario_Timbratura, Vettore_Orari_Timbrature)
                        AbbinaOrariTimbrature = AbbinaOrariTimbrature & Risultato
                        Sw_Fine = True
                    End If
                End If
            End If
            ' Timbrature.Dalle > Orari.Dalle
            If Sw_Fine = False Then
                If Vettore_Timbrature.Dalle(h, g) > Orario.Dalle Then
                    If Vettore_Timbrature.Dalle(h, g) >= Orario.Alle Then
                        Sw_Fine = True
                    Else
                        Orario_Timbratura.Dalle = Vettore_Timbrature.Dalle(h, g)
                        Orario_Timbratura.Causale = Vettore_Timbrature.Dalle_Causale(h, g)
                        Orario_Timbratura.Colore = Vettore_Timbrature.Dalle_Colore(h, g)
                        Orario_Timbratura.Giustificativo = Orario.Giustificativo
                        Orario_Timbratura.NelGruppo = Orario.NelGruppo
                        Orario_Timbratura.Familiare = Orario.Familiare
                        Orario_Timbratura.TipoServizio = Orario.TipoServizio
                        Orario_Timbratura.GiornoSuccessivo = Orario.GiornoSuccessivo
                        If Vettore_Timbrature.Alle(h, g) <> "" And Vettore_Timbrature.Alle(h, g) < Orario.Alle Then
                            Orario_Timbratura.Alle = Vettore_Timbrature.Alle(h, g)
                            Orario_Timbratura.Pausa = Orario.Pausa
                            If Orario_Timbratura.Causale = "" Then
                                Orario_Timbratura.Causale = Vettore_Timbrature.Alle_Causale(h, g)
                            End If
                            If Orario_Timbratura.Colore = vbWhite Then Orario_Timbratura.Colore = Vettore_Timbrature.Alle_Colore(h, g)
                            Orario.Dalle = Vettore_Timbrature.Alle(h, g)
                            Risultato = AggiungiTurniTimbrature(Dipendente, g, x, XR, Orario_Timbratura, Vettore_Orari_Timbrature)
                            AbbinaOrariTimbrature = AbbinaOrariTimbrature & Risultato
                            Vettore_Timbrature.Dalle(h, g) = ""
                            Vettore_Timbrature.Dalle_Causale(h, g) = ""
                            Vettore_Timbrature.Dalle_Colore(h, g) = vbWhite
                            Vettore_Timbrature.Alle(h, g) = ""
                            Vettore_Timbrature.Alle_Causale(h, g) = ""
                            Vettore_Timbrature.Alle_Colore(h, g) = vbWhite
                            h = h + 1
                        Else
                            Orario_Timbratura.Alle = Orario.Alle
                            Orario_Timbratura.Pausa = Orario.Pausa
                            If Orario_Timbratura.Causale = "" Then
                                Orario_Timbratura.Causale = Vettore_Timbrature.Alle_Causale(h, g)
                            End If
                            If Orario_Timbratura.Colore = vbWhite Then Orario_Timbratura.Colore = Vettore_Timbrature.Alle_Colore(h, g)
                            Vettore_Timbrature.Dalle(h, g) = Orario.Alle
                            Risultato = AggiungiTurniTimbrature(Dipendente, g, x, XR, Orario_Timbratura, Vettore_Orari_Timbrature)
                            AbbinaOrariTimbrature = AbbinaOrariTimbrature & Risultato
                            Sw_Fine = True
                        End If
                    End If
                End If
            End If
        Loop
    End Function

    Private Function AggiungiTurniTimbrature(ByVal Dipendente As Long, ByRef g As Object, ByRef x As Object, ByRef XR As Object, ByRef Orario_Timbratura As Cls_Struttura_Orario_Timbratura, ByRef Vettore_Orari_Timbrature As Cls_Struttura_Orari_Timbrature) As String
        AggiungiTurniTimbrature = ""
        If g > 31 Or x > 14 Or XR > 14 Then Exit Function
        '  If Orario_Timbratura.Dalle = Orario_Timbratura.Alle And Orario_Timbratura.Causale = "" Then Exit Function
        If Orario_Timbratura.Dalle = Orario_Timbratura.Alle And Orario_Timbratura.Tipo <> "Informazione" Then Exit Function

        Dim cg As New Cls_Giustificativi
        Dim cct As New Cls_CausaliTimbrature

        Select Case Orario_Timbratura.Tipo
            Case "Assenza", "EntrataPosticipata", "UscitaAnticipata"
                If Orario_Timbratura.Causale <> "" Then
                    If cct.CampoCausaliTimbrature(ConnectionString, Orario_Timbratura.Causale, "Tipo") <> "A" Then
                        Orario_Timbratura.Causale = ""
                    End If
                End If
            Case "EntrataAnticipata", "UscitaPosticipata", "Sostituzione"
                If Orario_Timbratura.Causale <> "" Then
                    If cct.CampoCausaliTimbrature(ConnectionString, Orario_Timbratura.Causale, "Tipo") = "A" Then
                        Orario_Timbratura.Causale = ""
                    End If
                End If
            Case ""
                If Orario_Timbratura.Causale <> "" Then
                    If cg.CampoGiustificativi(ConnectionString, Orario_Timbratura.Giustificativo, "Tipo") = "A" Then
                        If cct.CampoCausaliTimbrature(ConnectionString, Orario_Timbratura.Causale, "Tipo") <> "A" Then
                            Orario_Timbratura.Causale = ""
                        End If
                    Else
                        If cct.CampoCausaliTimbrature(ConnectionString, Orario_Timbratura.Causale, "Tipo") = "A" Then
                            Orario_Timbratura.Causale = ""
                        End If
                    End If
                End If
        End Select

        If Orario_Timbratura.GiornoSuccessivo = "R" Then
            If g < 31 Then
                Vettore_Orari_Timbrature.Dalle(XR, g + 1) = Orario_Timbratura.Dalle
                Vettore_Orari_Timbrature.Alle(XR, g + 1) = Orario_Timbratura.Alle
                Vettore_Orari_Timbrature.Pausa(XR, g + 1) = Orario_Timbratura.Pausa
                Vettore_Orari_Timbrature.Causale(XR, g + 1) = Orario_Timbratura.Causale
                Vettore_Orari_Timbrature.Colore(XR, g + 1) = Orario_Timbratura.Colore
                Vettore_Orari_Timbrature.Giustificativo(XR, g + 1) = Orario_Timbratura.Giustificativo
                Vettore_Orari_Timbrature.NelGruppo(XR, g + 1) = Orario_Timbratura.NelGruppo
                Vettore_Orari_Timbrature.Familiare(XR, g + 1) = Orario_Timbratura.Familiare
                Vettore_Orari_Timbrature.Tipo(XR, g + 1) = Orario_Timbratura.Tipo
                Vettore_Orari_Timbrature.Qualita(XR, g + 1) = Orario_Timbratura.Qualita
                Vettore_Orari_Timbrature.TipoServizio(XR, g + 1) = Orario_Timbratura.TipoServizio
                Vettore_Orari_Timbrature.GiornoSuccessivo(XR, g + 1) = Orario_Timbratura.GiornoSuccessivo
                Risultato = ControlloIntervalloDiTempo(Dipendente, Vettore_Orari_Timbrature, XR, g + 1)
            End If
        Else
            Vettore_Orari_Timbrature.Dalle(x, g) = Orario_Timbratura.Dalle
            Vettore_Orari_Timbrature.Alle(x, g) = Orario_Timbratura.Alle
            Vettore_Orari_Timbrature.Pausa(x, g) = Orario_Timbratura.Pausa
            Vettore_Orari_Timbrature.Causale(x, g) = Orario_Timbratura.Causale
            Vettore_Orari_Timbrature.Colore(x, g) = Orario_Timbratura.Colore
            Vettore_Orari_Timbrature.Giustificativo(x, g) = Orario_Timbratura.Giustificativo
            Vettore_Orari_Timbrature.NelGruppo(x, g) = Orario_Timbratura.NelGruppo
            Vettore_Orari_Timbrature.Familiare(x, g) = Orario_Timbratura.Familiare
            Vettore_Orari_Timbrature.Tipo(x, g) = Orario_Timbratura.Tipo
            Vettore_Orari_Timbrature.Qualita(x, g) = Orario_Timbratura.Qualita
            Vettore_Orari_Timbrature.TipoServizio(x, g) = Orario_Timbratura.TipoServizio
            Vettore_Orari_Timbrature.GiornoSuccessivo(x, g) = Orario_Timbratura.GiornoSuccessivo
            Risultato = ControlloIntervalloDiTempo(Dipendente, Vettore_Orari_Timbrature, x, g)
        End If
        AggiungiTurniTimbrature = ""
        If InStr(Risultato, "(E)") = 0 Then
            If Orario_Timbratura.GiornoSuccessivo = "R" Then
                XR = XR + 1
            Else
                x = x + 1
            End If
        End If
        Orario_Timbratura.Pulisci()
    End Function

    Private Function AbbinaTimbratureOrariFlex(ByVal Dipendente As Long, ByRef g As Object, ByRef x As Object, ByRef XR As Object, ByRef TempoIntervallo As Integer, ByRef Vettore_Orari As Cls_Struttura_Orari, ByRef Vettore_Orari_Timbrature As Cls_Struttura_Orari_Timbrature, ByRef Timbratura As Cls_Struttura_Timbratura) As String
        Dim Orario_Timbratura As New Cls_Struttura_Orario_Timbratura
        Dim cg As New Cls_Giustificativi
        Dim cpt As New Cls_ParametriTurni
        cpt.Leggi(ConnectionString)
        Dim cxst As New Cls_ClsXScriptTurni
        cxst.ConnectionString = ConnectionString

        Orario_Timbratura.Pulisci()

        Dim h As Byte = 0
        Dim Sw_Fine As Boolean = False
        Dim Delta As Integer

        AbbinaTimbratureOrariFlex = ""

        Do Until h > 9
            If Sw_Fine = True Then Exit Do
            If Vettore_Orari.Dalle(h, g) = "00.00" And Vettore_Orari.Alle(h, g) = "00.00" And Vettore_Orari.Giustificativo(h, g) = "" Then Exit Do
            If Vettore_Orari.Tipo(h, g) = "I" Then
                h = h + 1
            Else
                ' Orari.DalleBasso < Timbrature.Dalle
                If Vettore_Orari.DalleBasso(h, g) < Timbratura.Dalle Then
                    If Vettore_Orari.AlleBasso(h, g) > Timbratura.Dalle Then
                        Orario_Timbratura.Dalle = Timbratura.Dalle
                        Orario_Timbratura.Causale = Timbratura.Dalle_Causale
                        Orario_Timbratura.Colore = Timbratura.Dalle_Colore
                        If Vettore_Orari.Tipo(h, g) = "P" Then
                            Orario_Timbratura.Giustificativo = Vettore_Orari.Giustificativo(h, g)
                        Else
                            Orario_Timbratura.Giustificativo = cg.CampoGiustificativi(ConnectionString, Vettore_Orari.Giustificativo(h, g), "GiustificativoPerPresenza")
                        End If
                        Orario_Timbratura.NelGruppo = Vettore_Orari.NelGruppo(h, g)
                        Orario_Timbratura.Familiare = Vettore_Orari.Familiare(h, g)
                        Orario_Timbratura.TipoServizio = Vettore_Orari.TipoServizio(h, g)
                        Orario_Timbratura.GiornoSuccessivo = Vettore_Orari.GiornoSuccessivo(h, g)
                        If Vettore_Orari.AlleBasso(h, g) < Timbratura.Alle Then
                            Delta = DateDiff("n", DateSerial(Mid(Orario_Timbratura.Dalle, 1, 2), Mid(Orario_Timbratura.Dalle, 4, 2), 0), DateSerial(Mid(Timbratura.Alle, 1, 2), Mid(Timbratura.Alle, 4, 2), 0))
                            If TempoIntervallo < Delta Then
                                Delta = TempoIntervallo
                                TempoIntervallo = 0
                                Ore = Int(Delta / 60)
                                Minuti = Delta - (Ore * 60)
                                Delta = Format(Ore, "00") & "." & Format(Minuti, "00")
                                Orario_Timbratura.Alle = cxst.Sostituisci(Format(cxst.Mod_OperaConMinuti(Orario_Timbratura.Dalle, "+", Delta), "00.00"), ",", ".")
                                Timbratura.Dalle = Orario_Timbratura.Alle
                                Vettore_Orari.AlleBasso(h, g) = Orario_Timbratura.Alle
                            Else
                                TempoIntervallo = TempoIntervallo - Delta
                            End If
                            Orario_Timbratura.Alle = Vettore_Orari.AlleBasso(h, g)
                            Orario_Timbratura.Pausa = Vettore_Orari.Pausa(h, g)
                            If Orario_Timbratura.Causale = "" Then Orario_Timbratura.Causale = Timbratura.Alle_Causale
                            If Orario_Timbratura.Colore = vbWhite Then Orario_Timbratura.Colore = Timbratura.Alle_Colore
                            If Vettore_Orari.Tipo(h, g) = "P" Or (Vettore_Orari.Tipo(h, g) = "A" And cg.CampoGiustificativi(ConnectionString, Vettore_Orari.Giustificativo(h, g), "RegoleGenerali") = "S") Then
                                Risultato = AggiungiTurniTimbrature(Dipendente, g, x, XR, Orario_Timbratura, Vettore_Orari_Timbrature)
                                AbbinaTimbratureOrariFlex = AbbinaTimbratureOrariFlex & Risultato
                            Else
                                Orario_Timbratura.Pulisci()
                            End If
                            Timbratura.Dalle = Vettore_Orari.AlleBasso(h, g)
                        Else
                            Delta = DateDiff("n", DateSerial(Mid(Orario_Timbratura.Dalle, 1, 2), Mid(Orario_Timbratura.Dalle, 4, 2), 0), DateSerial(Mid(Timbratura.Alle, 1, 2), Mid(Timbratura.Alle, 4, 2), 0))
                            If TempoIntervallo < Delta Then
                                Delta = Delta - TempoIntervallo
                                Ore = Int(Delta / 60)
                                Minuti = Delta - (Ore * 60)
                                Delta = Format(Ore, "00") & "." & Format(Minuti, "00")
                                Vettore_Orari.AlleBasso(h, g) = cxst.Sostituisci(Format(cxst.Mod_OperaConMinuti(Vettore_Orari.AlleBasso(h, g), "-", Delta), "00.00"), ",", ".")
                                Delta = TempoIntervallo
                                TempoIntervallo = 0
                                Ore = Int(Delta / 60)
                                Minuti = Delta - (Ore * 60)
                                Delta = Format(Ore, "00") & "." & Format(Minuti, "00")
                                Orario_Timbratura.Alle = cxst.Sostituisci(Format(cxst.Mod_OperaConMinuti(Orario_Timbratura.Dalle, "+", Delta), "00.00"), ",", ".")
                                Timbratura.Dalle = Orario_Timbratura.Alle
                            Else
                                TempoIntervallo = TempoIntervallo - Delta
                                Orario_Timbratura.Alle = Timbratura.Alle
                                Sw_Fine = True
                            End If
                            Orario_Timbratura.Pausa = Vettore_Orari.Pausa(h, g)
                            If Orario_Timbratura.Causale = "" Then Orario_Timbratura.Causale = Timbratura.Alle_Causale
                            If Orario_Timbratura.Colore = vbWhite Then Orario_Timbratura.Colore = Timbratura.Alle_Colore
                            If Vettore_Orari.Tipo(h, g) = "P" Or (Vettore_Orari.Tipo(h, g) = "A" And cg.CampoGiustificativi(ConnectionString, Vettore_Orari.Giustificativo(h, g), "RegoleGenerali") = "S") Then
                                Risultato = AggiungiTurniTimbrature(Dipendente, g, x, XR, Orario_Timbratura, Vettore_Orari_Timbrature)
                                AbbinaTimbratureOrariFlex = AbbinaTimbratureOrariFlex & Risultato
                            Else
                                Orario_Timbratura.Pulisci()
                            End If
                        End If
                    Else
                        h = h + 1
                    End If
                Else
                    ' Orari.DalleAlto <= Timbrature.Dalle and Orari.DalleBasso >= Timbrature.Dalle
                    If Vettore_Orari.DalleAlto(h, g) <= Timbratura.Dalle And Vettore_Orari.DalleBasso(h, g) >= Timbratura.Dalle Then
                        Orario_Timbratura.Dalle = Timbratura.Dalle
                        Orario_Timbratura.Causale = Timbratura.Dalle_Causale
                        Orario_Timbratura.Colore = Timbratura.Dalle_Colore
                        If Vettore_Orari.Tipo(h, g) = "P" Then
                            Orario_Timbratura.Giustificativo = Vettore_Orari.Giustificativo(h, g)
                        Else
                            Orario_Timbratura.Giustificativo = cg.CampoGiustificativi(ConnectionString, Vettore_Orari.Giustificativo(h, g), "GiustificativoPerPresenza")
                        End If
                        Orario_Timbratura.NelGruppo = Vettore_Orari.NelGruppo(h, g)
                        Orario_Timbratura.Familiare = Vettore_Orari.Familiare(h, g)
                        Orario_Timbratura.TipoServizio = Vettore_Orari.TipoServizio(h, g)
                        Orario_Timbratura.GiornoSuccessivo = Vettore_Orari.GiornoSuccessivo(h, g)
                        If Vettore_Orari.AlleBasso(h, g) < Timbratura.Alle Then
                            Orario_Timbratura.Alle = Vettore_Orari.AlleBasso(h, g)
                            Orario_Timbratura.Pausa = Vettore_Orari.Pausa(h, g)
                            If Orario_Timbratura.Causale = "" Then Orario_Timbratura.Causale = Timbratura.Alle_Causale
                            If Orario_Timbratura.Colore = vbWhite Then Orario_Timbratura.Colore = Timbratura.Alle_Colore
                            If Vettore_Orari.Tipo(h, g) = "P" Or (Vettore_Orari.Tipo(h, g) = "A" And cg.CampoGiustificativi(ConnectionString, Vettore_Orari.Giustificativo(h, g), "RegoleGenerali") = "S") Then
                                Risultato = AggiungiTurniTimbrature(Dipendente, g, x, XR, Orario_Timbratura, Vettore_Orari_Timbrature)
                                AbbinaTimbratureOrariFlex = AbbinaTimbratureOrariFlex & Risultato
                            Else
                                Orario_Timbratura.Pulisci()
                            End If
                            Timbratura.Dalle = Vettore_Orari.AlleBasso(h, g)
                            h = h + 1
                            If Timbratura.Dalle <> "00.00" Or Timbratura.Alle <> "24.00" Then
                                If Vettore_Orari.Dalle(h, g) = "00.00" And Vettore_Orari.Alle(h, g) = "24.00" Then
                                    Orario_Timbratura.Dalle = Timbratura.Dalle
                                    Orario_Timbratura.Alle = Timbratura.Alle
                                    If Timbratura.Dalle_Causale <> "" Then
                                        Orario_Timbratura.Causale = Timbratura.Dalle_Causale
                                    Else
                                        Orario_Timbratura.Causale = Timbratura.Alle_Causale
                                    End If
                                    If Timbratura.Dalle_Colore <> vbWhite Then
                                        Orario_Timbratura.Colore = Timbratura.Dalle_Colore
                                    Else
                                        Orario_Timbratura.Colore = Timbratura.Alle_Colore
                                    End If
                                    Orario_Timbratura.Giustificativo = cpt.GiustificativoUscitaPosticipata
                                    Orario_Timbratura.NelGruppo = Vettore_Orari.NelGruppo(h, g)
                                    Orario_Timbratura.Familiare = Vettore_Orari.Familiare(h, g)
                                    Orario_Timbratura.Tipo = "UscitaPosticipata"
                                    Orario_Timbratura.TipoServizio = Vettore_Orari.TipoServizio(h, g)
                                    Orario_Timbratura.GiornoSuccessivo = Vettore_Orari.GiornoSuccessivo(h, g)
                                    If Vettore_Orari.Tipo(h, g) = "P" Or (Vettore_Orari.Tipo(h, g) = "A" And cg.CampoGiustificativi(ConnectionString, Vettore_Orari.Giustificativo(h, g), "RegoleGenerali") = "S") Then
                                        Risultato = AggiungiTurniTimbrature(Dipendente, g, x, XR, Orario_Timbratura, Vettore_Orari_Timbrature)
                                        AbbinaTimbratureOrariFlex = AbbinaTimbratureOrariFlex & Risultato
                                    Else
                                        Orario_Timbratura.Pulisci()
                                    End If
                                    Sw_Fine = True
                                End If
                            Else
                                Sw_Fine = True
                            End If
                        Else
                            Delta = DateDiff("n", DateSerial(Mid(Orario_Timbratura.Dalle, 1, 2), Mid(Orario_Timbratura.Dalle, 4, 2), 0), DateSerial(Mid(Timbratura.Alle, 1, 2), Mid(Timbratura.Alle, 4, 2), 0))
                            If TempoIntervallo < Delta Then
                                Delta = Delta - TempoIntervallo
                                Ore = Int(Delta / 60)
                                Minuti = Delta - (Ore * 60)
                                Delta = Format(Ore, "00") & "." & Format(Minuti, "00")
                                Vettore_Orari.AlleBasso(h, g) = cxst.Sostituisci(Format(cxst.Mod_OperaConMinuti(Vettore_Orari.AlleBasso(h, g), "-", Delta), "00.00"), ",", ".")
                                Delta = TempoIntervallo
                                TempoIntervallo = 0
                                Ore = Int(Delta / 60)
                                Minuti = Delta - (Ore * 60)
                                Delta = Format(Ore, "00") & "." & Format(Minuti, "00")
                                Orario_Timbratura.Alle = cxst.Sostituisci(Format(cxst.Mod_OperaConMinuti(Orario_Timbratura.Dalle, "+", Delta), "00.00"), ",", ".")
                                Timbratura.Dalle = Orario_Timbratura.Alle
                            Else
                                TempoIntervallo = TempoIntervallo - Delta
                                Orario_Timbratura.Alle = Timbratura.Alle
                                Sw_Fine = True
                            End If
                            Orario_Timbratura.Pausa = Vettore_Orari.Pausa(h, g)
                            If Orario_Timbratura.Causale = "" Then Orario_Timbratura.Causale = Timbratura.Alle_Causale
                            If Orario_Timbratura.Colore = vbWhite Then Orario_Timbratura.Colore = Timbratura.Alle_Colore
                            If Vettore_Orari.Tipo(h, g) = "P" Or (Vettore_Orari.Tipo(h, g) = "A" And cg.CampoGiustificativi(ConnectionString, Vettore_Orari.Giustificativo(h, g), "RegoleGenerali") = "S") Then
                                Risultato = AggiungiTurniTimbrature(Dipendente, g, x, XR, Orario_Timbratura, Vettore_Orari_Timbrature)
                                AbbinaTimbratureOrariFlex = AbbinaTimbratureOrariFlex & Risultato
                            Else
                                Orario_Timbratura.Pulisci()
                            End If
                        End If
                    Else
                        ' Orari.DalleAlto > Timbrature.Dalle
                        If Vettore_Orari.DalleAlto(h, g) >= Timbratura.Alle Then
                            Orario_Timbratura.Dalle = Timbratura.Dalle
                            Orario_Timbratura.Alle = Timbratura.Alle
                            If Timbratura.Dalle_Causale <> "" Then
                                Orario_Timbratura.Causale = Timbratura.Dalle_Causale
                            Else
                                Orario_Timbratura.Causale = Timbratura.Alle_Causale
                            End If
                            If Timbratura.Dalle_Colore <> vbWhite Then
                                Orario_Timbratura.Colore = Timbratura.Dalle_Colore
                            Else
                                Orario_Timbratura.Colore = Timbratura.Alle_Colore
                            End If
                            Orario_Timbratura.Giustificativo = cpt.GiustificativoSostituzione
                            Orario_Timbratura.NelGruppo = Vettore_Orari.NelGruppo(h, g)
                            Orario_Timbratura.Familiare = Vettore_Orari.Familiare(h, g)
                            Orario_Timbratura.Tipo = "Sostituzione"
                            Orario_Timbratura.TipoServizio = Vettore_Orari.TipoServizio(h, g)
                            Orario_Timbratura.GiornoSuccessivo = Vettore_Orari.GiornoSuccessivo(h, g)
                            If Vettore_Orari.Tipo(h, g) = "P" Or (Vettore_Orari.Tipo(h, g) = "A" And cg.CampoGiustificativi(ConnectionString, Vettore_Orari.Giustificativo(h, g), "RegoleGenerali") = "S") Then
                                Risultato = AggiungiTurniTimbrature(Dipendente, g, x, XR, Orario_Timbratura, Vettore_Orari_Timbrature)
                                AbbinaTimbratureOrariFlex = AbbinaTimbratureOrariFlex & Risultato
                            Else
                                Orario_Timbratura.Pulisci()
                            End If
                            Sw_Fine = True
                        Else
                            Orario_Timbratura.Dalle = Timbratura.Dalle
                            Orario_Timbratura.Causale = Timbratura.Dalle_Causale
                            Orario_Timbratura.Colore = Timbratura.Dalle_Colore
                            Orario_Timbratura.Alle = Vettore_Orari.DalleAlto(h, g)
                            Orario_Timbratura.Tipo = "EntrataAnticipata"
                            Orario_Timbratura.Giustificativo = cpt.GiustificativoEntrataAnticipata
                            Orario_Timbratura.NelGruppo = Vettore_Orari.NelGruppo(h, g)
                            Orario_Timbratura.Familiare = Vettore_Orari.Familiare(h, g)
                            Orario_Timbratura.TipoServizio = Vettore_Orari.TipoServizio(h, g)
                            Orario_Timbratura.GiornoSuccessivo = Vettore_Orari.GiornoSuccessivo(h, g)
                            If Vettore_Orari.Tipo(h, g) = "P" Or (Vettore_Orari.Tipo(h, g) = "A" And cg.CampoGiustificativi(ConnectionString, Vettore_Orari.Giustificativo(h, g), "RegoleGenerali") = "S") Then
                                Risultato = AggiungiTurniTimbrature(Dipendente, g, x, XR, Orario_Timbratura, Vettore_Orari_Timbrature)
                                AbbinaTimbratureOrariFlex = AbbinaTimbratureOrariFlex & Risultato
                            Else
                                Orario_Timbratura.Pulisci()
                            End If
                            Timbratura.Dalle = Vettore_Orari.DalleAlto(h, g)
                        End If
                    End If
                End If
            End If
        Loop
        If Sw_Fine = False Then
            Orario_Timbratura.Dalle = Timbratura.Dalle
            Orario_Timbratura.Alle = Timbratura.Alle
            If Timbratura.Dalle_Causale <> "" Then
                Orario_Timbratura.Causale = Timbratura.Dalle_Causale
            Else
                Orario_Timbratura.Causale = Timbratura.Alle_Causale
            End If
            If Timbratura.Dalle_Colore <> vbWhite Then
                Orario_Timbratura.Colore = Timbratura.Dalle_Colore
            Else
                Orario_Timbratura.Colore = Timbratura.Alle_Colore
            End If
            Orario_Timbratura.Giustificativo = cpt.GiustificativoUscitaPosticipata
            Orario_Timbratura.NelGruppo = ""
            Orario_Timbratura.Familiare = 0
            Orario_Timbratura.Tipo = "UscitaPosticipata"
            Orario_Timbratura.TipoServizio = ""
            Orario_Timbratura.GiornoSuccessivo = ""
            Risultato = AggiungiTurniTimbrature(Dipendente, g, x, XR, Orario_Timbratura, Vettore_Orari_Timbrature)
        End If
    End Function

    Private Function AbbinaTimbratureEntrataOrari(ByVal Dipendente As Long, ByRef g As Object, ByRef x As Object, ByRef XR As Object, ByRef Vettore_Orari As Cls_Struttura_Orari, ByRef Vettore_Orari_Timbrature As Cls_Struttura_Orari_Timbrature, ByRef Timbratura As Cls_Struttura_Timbratura) As String
        Dim Orario_Timbratura As New Cls_Struttura_Orario_Timbratura
        Dim cg As New Cls_Giustificativi
        Dim cpt As New Cls_ParametriTurni
        cpt.Leggi(ConnectionString)

        Orario_Timbratura.Pulisci()

        Dim h As Byte = 0
        Dim Sw_Fine As Boolean = False

        Do Until h > 9
            If Sw_Fine = True Then Exit Do
            If Vettore_Orari.Dalle(h, g) = "00.00" And Vettore_Orari.Alle(h, g) = "00.00" And Vettore_Orari.Giustificativo(h, g) = "" Then Exit Do
            If Vettore_Orari.Tipo(h, g) = "I" Then
                h = h + 1
            Else
                ' Orari.Dalle < Timbrature.Dalle
                If Vettore_Orari.Dalle(h, g) < Timbratura.Dalle Then
                    If Vettore_Orari.Alle(h, g) > Timbratura.Dalle Then
                        Orario_Timbratura.Dalle = Timbratura.Dalle
                        Orario_Timbratura.Alle = Vettore_Orari.Alle(h, g)
                        Orario_Timbratura.Pausa = Vettore_Orari.Pausa(h, g)
                        If Timbratura.Dalle_Causale <> "" Then
                            Orario_Timbratura.Causale = Timbratura.Dalle_Causale
                        Else
                            Orario_Timbratura.Causale = Timbratura.Alle_Causale
                        End If
                        If Timbratura.Dalle_Colore <> vbWhite Then
                            Orario_Timbratura.Colore = Timbratura.Dalle_Colore
                        Else
                            Orario_Timbratura.Colore = Timbratura.Alle_Colore
                        End If
                        If Vettore_Orari.Tipo(h, g) = "P" Then
                            Orario_Timbratura.Giustificativo = Vettore_Orari.Giustificativo(h, g)
                        Else
                            Orario_Timbratura.Giustificativo = cg.CampoGiustificativi(ConnectionString, Vettore_Orari.Giustificativo(h, g), "GiustificativoPerPresenza")
                        End If
                        Orario_Timbratura.NelGruppo = Vettore_Orari.NelGruppo(h, g)
                        Orario_Timbratura.Familiare = Vettore_Orari.Familiare(h, g)
                        Orario_Timbratura.TipoServizio = Vettore_Orari.TipoServizio(h, g)
                        Orario_Timbratura.GiornoSuccessivo = Vettore_Orari.GiornoSuccessivo(h, g)
                        If Vettore_Orari.Tipo(h, g) = "P" Or (Vettore_Orari.Tipo(h, g) = "A" And cg.CampoGiustificativi(ConnectionString, Vettore_Orari.Giustificativo(h, g), "RegoleGenerali") = "S") Then
                            Risultato = AggiungiTurniTimbrature(Dipendente, g, x, XR, Orario_Timbratura, Vettore_Orari_Timbrature)
                            AbbinaTimbratureEntrataOrari = AbbinaTimbratureEntrataOrari & Risultato
                        Else
                            Orario_Timbratura.Pulisci()
                        End If
                        Sw_Fine = True
                    Else
                        h = h + 1
                    End If
                Else
                    ' Orari.Dalle = Timbrature.Dalle
                    If Vettore_Orari.Dalle(h, g) = Timbratura.Dalle Then
                        Orario_Timbratura.Dalle = Timbratura.Dalle
                        Orario_Timbratura.Alle = Vettore_Orari.Alle(h, g)
                        Orario_Timbratura.Pausa = Vettore_Orari.Pausa(h, g)
                        If Timbratura.Dalle_Causale <> "" Then
                            Orario_Timbratura.Causale = Timbratura.Dalle_Causale
                        Else
                            Orario_Timbratura.Causale = Timbratura.Alle_Causale
                        End If
                        If Timbratura.Dalle_Colore <> vbWhite Then
                            Orario_Timbratura.Colore = Timbratura.Dalle_Colore
                        Else
                            Orario_Timbratura.Colore = Timbratura.Alle_Colore
                        End If
                        If Vettore_Orari.Tipo(h, g) = "P" Then
                            Orario_Timbratura.Giustificativo = Vettore_Orari.Giustificativo(h, g)
                        Else
                            Orario_Timbratura.Giustificativo = cg.CampoGiustificativi(ConnectionString, Vettore_Orari.Giustificativo(h, g), "GiustificativoPerPresenza")
                        End If
                        Orario_Timbratura.NelGruppo = Vettore_Orari.NelGruppo(h, g)
                        Orario_Timbratura.Familiare = Vettore_Orari.Familiare(h, g)
                        Orario_Timbratura.TipoServizio = Vettore_Orari.TipoServizio(h, g)
                        Orario_Timbratura.GiornoSuccessivo = Vettore_Orari.GiornoSuccessivo(h, g)
                        If Vettore_Orari.Tipo(h, g) = "P" Or (Vettore_Orari.Tipo(h, g) = "A" And cg.CampoGiustificativi(ConnectionString, Vettore_Orari.Giustificativo(h, g), "RegoleGenerali") = "S") Then
                            Risultato = AggiungiTurniTimbrature(Dipendente, g, x, XR, Orario_Timbratura, Vettore_Orari_Timbrature)
                            AbbinaTimbratureEntrataOrari = AbbinaTimbratureEntrataOrari & Risultato
                        Else
                            Orario_Timbratura.Pulisci()
                        End If
                        Sw_Fine = True
                    Else
                        ' Orari.Dalle > Timbrature.Dalle
                        Orario_Timbratura.Dalle = Timbratura.Dalle
                        Orario_Timbratura.Alle = Vettore_Orari.Dalle(h, g)
                        Orario_Timbratura.Causale = Timbratura.Dalle_Causale
                        Orario_Timbratura.Colore = Timbratura.Dalle_Colore
                        Orario_Timbratura.Giustificativo = cpt.GiustificativoEntrataAnticipata
                        Orario_Timbratura.NelGruppo = Vettore_Orari.NelGruppo(h, g)
                        Orario_Timbratura.Familiare = Vettore_Orari.Familiare(h, g)
                        Orario_Timbratura.Tipo = "EntrataAnticipata"
                        Orario_Timbratura.TipoServizio = Vettore_Orari.TipoServizio(h, g)
                        Orario_Timbratura.GiornoSuccessivo = Vettore_Orari.GiornoSuccessivo(h, g)
                        If Vettore_Orari.Tipo(h, g) = "P" Or (Vettore_Orari.Tipo(h, g) = "A" And cg.CampoGiustificativi(ConnectionString, Vettore_Orari.Giustificativo(h, g), "RegoleGenerali") = "S") Then
                            Risultato = AggiungiTurniTimbrature(Dipendente, g, x, XR, Orario_Timbratura, Vettore_Orari_Timbrature)
                            AbbinaTimbratureEntrataOrari = AbbinaTimbratureEntrataOrari & Risultato
                        Else
                            Orario_Timbratura.Pulisci()
                        End If
                        Timbratura.Dalle = Vettore_Orari.Dalle(h, g)
                    End If
                End If
            End If
        Loop
        If Sw_Fine = False Then
            Orario_Timbratura.Dalle = Timbratura.Dalle
            Orario_Timbratura.Alle = Timbratura.Alle
            If Timbratura.Dalle_Causale <> "" Then
                Orario_Timbratura.Causale = Timbratura.Dalle_Causale
            Else
                Orario_Timbratura.Causale = Timbratura.Alle_Causale
            End If
            If Timbratura.Dalle_Colore <> vbWhite Then
                Orario_Timbratura.Colore = Timbratura.Dalle_Colore
            Else
                Orario_Timbratura.Colore = Timbratura.Alle_Colore
            End If
            Orario_Timbratura.Giustificativo = cpt.GiustificativoUscitaPosticipata
            Orario_Timbratura.NelGruppo = Vettore_Orari.NelGruppo(h, g)
            Orario_Timbratura.Familiare = Vettore_Orari.Familiare(h, g)
            Orario_Timbratura.Tipo = "UscitaPosticipata"
            Orario_Timbratura.TipoServizio = Vettore_Orari.TipoServizio(h, g)
            Orario_Timbratura.GiornoSuccessivo = Vettore_Orari.GiornoSuccessivo(h, g)
            If Vettore_Orari.Tipo(h, g) = "P" Or (Vettore_Orari.Tipo(h, g) = "A" And cg.CampoGiustificativi(ConnectionString, Vettore_Orari.Giustificativo(h, g), "RegoleGenerali") = "S") Then
                Risultato = AggiungiTurniTimbrature(Dipendente, g, x, XR, Orario_Timbratura, Vettore_Orari_Timbrature)
                AbbinaTimbratureEntrataOrari = AbbinaTimbratureEntrataOrari & Risultato
            Else
                Orario_Timbratura.Pulisci()
            End If
        End If
    End Function

    Private Function AbbinaTimbratureOrari(ByVal Dipendente As Long, ByRef g As Object, ByRef x As Object, ByRef XR As Object, ByRef Vettore_Orari As Cls_Struttura_Orari, ByRef Vettore_Orari_Timbrature As Cls_Struttura_Orari_Timbrature, ByRef Timbratura As Cls_Struttura_Timbratura, ByVal ControlloOrario As String) As String
        Dim Orario_Timbratura As New Cls_Struttura_Orario_Timbratura
        Dim cg As New Cls_Giustificativi
        Dim cpt As New Cls_ParametriTurni
        cpt.Leggi(ConnectionString)

        Dim W_Alle As String = "00.00"

        If Timbratura.Dalle = "" Then Timbratura.Dalle = "00.00"

        Orario_Timbratura.Pulisci()

        Dim h As Integer = 0
        Dim Sw_Fine As Boolean = False

        Do Until h > 9
            If Sw_Fine = True Then Exit Do
            If Vettore_Orari.Tipo(h, g) = "I" Then
                h = h + 1
            Else
                If Vettore_Orari.Dalle(h, g) = "00.00" And Vettore_Orari.Alle(h, g) = "24.00" And Vettore_Orari.Tipo(h, g) = "A" Then
                    h = h + 1
                Else
                    If Vettore_Orari.Dalle(h, g) = "00.00" And Vettore_Orari.Alle(h, g) = "00.00" And Vettore_Orari.Giustificativo(h, g) = "" Then Exit Do
                    ' Orari.Dalle < Timbrature.Dalle
                    If Vettore_Orari.Dalle(h, g) < Timbratura.Dalle Then
                        If Vettore_Orari.Alle(h, g) > Timbratura.Dalle Then
                            Orario_Timbratura.Dalle = Timbratura.Dalle
                            Orario_Timbratura.Causale = Timbratura.Dalle_Causale
                            Orario_Timbratura.Colore = Timbratura.Dalle_Colore
                            If Vettore_Orari.Tipo(h, g) = "P" Then
                                Orario_Timbratura.Giustificativo = Vettore_Orari.Giustificativo(h, g)
                            Else
                                Orario_Timbratura.Giustificativo = cg.CampoGiustificativi(ConnectionString, Vettore_Orari.Giustificativo(h, g), "GiustificativoPerPresenza")
                            End If
                            Orario_Timbratura.NelGruppo = Vettore_Orari.NelGruppo(h, g)
                            Orario_Timbratura.Familiare = Vettore_Orari.Familiare(h, g)
                            Orario_Timbratura.TipoServizio = Vettore_Orari.TipoServizio(h, g)
                            Orario_Timbratura.GiornoSuccessivo = Vettore_Orari.GiornoSuccessivo(h, g)
                            If Vettore_Orari.Alle(h, g) < Timbratura.Alle Then
                                Orario_Timbratura.Alle = Vettore_Orari.Alle(h, g)
                                W_Alle = Vettore_Orari.Alle(h, g)
                                Orario_Timbratura.Pausa = Vettore_Orari.Pausa(h, g)
                                If Orario_Timbratura.Causale = "" Then Orario_Timbratura.Causale = Timbratura.Alle_Causale
                                If Orario_Timbratura.Colore = vbWhite Then Orario_Timbratura.Colore = Timbratura.Alle_Colore
                                If Vettore_Orari.Tipo(h, g) = "P" Or (Vettore_Orari.Tipo(h, g) = "A" And cg.CampoGiustificativi(ConnectionString, Vettore_Orari.Giustificativo(h, g), "RegoleGenerali") = "S") Then
                                    Risultato = AggiungiTurniTimbrature(Dipendente, g, x, XR, Orario_Timbratura, Vettore_Orari_Timbrature)
                                    AbbinaTimbratureOrari = AbbinaTimbratureOrari & Risultato
                                Else
                                    Orario_Timbratura.Pulisci()
                                End If
                                Timbratura.Dalle = Vettore_Orari.Alle(h, g)
                                h = h + 1
                                If Timbratura.Dalle <> "00.00" Or Timbratura.Alle <> "24.00" Then
                                    If (Vettore_Orari.Dalle(h, g) = "00.00" And Vettore_Orari.Alle(h, g) = "24.00") Or Vettore_Orari.Dalle(h, g) >= Timbratura.Alle Then
                                        Exit Do
                                    End If
                                Else
                                    Sw_Fine = True
                                End If
                            Else
                                Orario_Timbratura.Alle = Timbratura.Alle
                                Orario_Timbratura.Pausa = Vettore_Orari.Pausa(h, g)
                                If Orario_Timbratura.Causale = "" Then Orario_Timbratura.Causale = Timbratura.Alle_Causale
                                If Orario_Timbratura.Colore = vbWhite Then Orario_Timbratura.Colore = Timbratura.Alle_Colore
                                If Vettore_Orari.Tipo(h, g) = "P" Or (Vettore_Orari.Tipo(h, g) = "A" And cg.CampoGiustificativi(ConnectionString, Vettore_Orari.Giustificativo(h, g), "RegoleGenerali") = "S") Then
                                    Risultato = AggiungiTurniTimbrature(Dipendente, g, x, XR, Orario_Timbratura, Vettore_Orari_Timbrature)
                                    AbbinaTimbratureOrari = AbbinaTimbratureOrari & Risultato
                                Else
                                    Orario_Timbratura.Pulisci()
                                End If
                                Sw_Fine = True
                            End If
                        Else
                            h = h + 1
                            If Vettore_Orari.Dalle(h, g) = "00.00" And Vettore_Orari.Alle(h, g) = "00.00" And Vettore_Orari.Giustificativo(h, g) = "" Then Exit Do
                        End If
                    End If
                    ' Orari.Dalle = Timbrature.Dalle
                    If Sw_Fine = False Then
                        If Vettore_Orari.Dalle(h, g) = Timbratura.Dalle Then
                            Orario_Timbratura.Dalle = Timbratura.Dalle
                            Orario_Timbratura.Causale = Timbratura.Dalle_Causale
                            Orario_Timbratura.Colore = Timbratura.Dalle_Colore
                            If Vettore_Orari.Tipo(h, g) = "P" Then
                                Orario_Timbratura.Giustificativo = Vettore_Orari.Giustificativo(h, g)
                            Else
                                Orario_Timbratura.Giustificativo = cg.CampoGiustificativi(ConnectionString, Vettore_Orari.Giustificativo(h, g), "GiustificativoPerPresenza")
                            End If
                            Orario_Timbratura.NelGruppo = Vettore_Orari.NelGruppo(h, g)
                            Orario_Timbratura.Familiare = Vettore_Orari.Familiare(h, g)
                            Orario_Timbratura.TipoServizio = Vettore_Orari.TipoServizio(h, g)
                            Orario_Timbratura.GiornoSuccessivo = Vettore_Orari.GiornoSuccessivo(h, g)
                            If Vettore_Orari.Alle(h, g) < Timbratura.Alle Then
                                Orario_Timbratura.Alle = Vettore_Orari.Alle(h, g)
                                W_Alle = Vettore_Orari.Alle(h, g)
                                Orario_Timbratura.Pausa = Vettore_Orari.Pausa(h, g)
                                If Orario_Timbratura.Causale = "" Then Orario_Timbratura.Causale = Timbratura.Alle_Causale
                                If Orario_Timbratura.Colore = vbWhite Then Orario_Timbratura.Colore = Timbratura.Alle_Colore
                                If Vettore_Orari.Tipo(h, g) = "P" Or (Vettore_Orari.Tipo(h, g) = "A" And cg.CampoGiustificativi(ConnectionString, Vettore_Orari.Giustificativo(h, g), "RegoleGenerali") = "S") Then
                                    Risultato = AggiungiTurniTimbrature(Dipendente, g, x, XR, Orario_Timbratura, Vettore_Orari_Timbrature)
                                    AbbinaTimbratureOrari = AbbinaTimbratureOrari & Risultato
                                Else
                                    Orario_Timbratura.Pulisci()
                                End If
                                Timbratura.Dalle = Vettore_Orari.Alle(h, g)
                                h = h + 1
                                If Timbratura.Dalle <> "00.00" Or Timbratura.Alle <> "24.00" Then
                                    If (Vettore_Orari.Dalle(h, g) = "00.00" And Vettore_Orari.Alle(h, g) = "24.00") Or Vettore_Orari.Dalle(h, g) >= Timbratura.Alle Then
                                        Exit Do
                                    End If
                                Else
                                    Sw_Fine = True
                                End If
                            Else
                                Orario_Timbratura.Alle = Timbratura.Alle
                                Orario_Timbratura.Pausa = Vettore_Orari.Pausa(h, g)
                                If Orario_Timbratura.Causale = "" Then Orario_Timbratura.Causale = Timbratura.Alle_Causale
                                If Orario_Timbratura.Colore = vbWhite Then Orario_Timbratura.Colore = Timbratura.Alle_Colore
                                If Vettore_Orari.Tipo(h, g) = "P" Or (Vettore_Orari.Tipo(h, g) = "A" And cg.CampoGiustificativi(ConnectionString, Vettore_Orari.Giustificativo(h, g), "RegoleGenerali") = "S") Then
                                    Risultato = AggiungiTurniTimbrature(Dipendente, g, x, XR, Orario_Timbratura, Vettore_Orari_Timbrature)
                                    AbbinaTimbratureOrari = AbbinaTimbratureOrari & Risultato
                                Else
                                    Orario_Timbratura.Pulisci()
                                End If
                                Sw_Fine = True
                            End If
                        End If
                    End If
                    ' Orari.Dalle > Timbrature.Dalle
                    If Sw_Fine = False Then
                        If Vettore_Orari.Dalle(h, g) > Timbratura.Dalle Then
                            If Vettore_Orari.Dalle(h, g) >= Timbratura.Alle Then
                                Exit Do
                            Else
                                Orario_Timbratura.Dalle = Timbratura.Dalle
                                Orario_Timbratura.Causale = Timbratura.Dalle_Causale
                                Orario_Timbratura.Colore = Timbratura.Dalle_Colore
                                Orario_Timbratura.Alle = Vettore_Orari.Dalle(h, g)
                                If ControlloOrario = "S" Then
                                    Orario_Timbratura.Tipo = "EntrataAnticipata"
                                    Orario_Timbratura.Giustificativo = cpt.GiustificativoEntrataAnticipata
                                Else
                                    Orario_Timbratura.Tipo = ""
                                    Orario_Timbratura.Giustificativo = Vettore_Orari.Giustificativo(h, g)
                                End If
                                Orario_Timbratura.NelGruppo = Vettore_Orari.NelGruppo(h, g)
                                Orario_Timbratura.Familiare = Vettore_Orari.Familiare(h, g)
                                Orario_Timbratura.TipoServizio = Vettore_Orari.TipoServizio(h, g)
                                Orario_Timbratura.GiornoSuccessivo = Vettore_Orari.GiornoSuccessivo(h, g)
                                If h > 0 Then
                                    If Vettore_Orari.Tipo(h - 1, g) = "A" And Vettore_Orari.Alle(h - 1, g) = Vettore_Orari.Dalle(h, g) And cg.CampoGiustificativi(ConnectionString, Vettore_Orari.Giustificativo(h - 1, g), "RegoleGenerali") = "S" Then
                                        Orario_Timbratura.Giustificativo = cpt.GiustificativoSostituzione
                                        Orario_Timbratura.Tipo = "Sostituzione"
                                    End If
                                End If
                                If Vettore_Orari.Tipo(h, g) = "P" Or (Vettore_Orari.Tipo(h, g) = "A" And cg.CampoGiustificativi(ConnectionString, Vettore_Orari.Giustificativo(h, g), "RegoleGenerali") = "S") Then
                                    Risultato = AggiungiTurniTimbrature(Dipendente, g, x, XR, Orario_Timbratura, Vettore_Orari_Timbrature)
                                    AbbinaTimbratureOrari = AbbinaTimbratureOrari & Risultato
                                Else
                                    Orario_Timbratura.Pulisci()
                                End If
                                Timbratura.Dalle = Vettore_Orari.Dalle(h, g)
                            End If
                        End If
                    End If
                End If
            End If
        Loop

        If Sw_Fine = False Then
            h = 9
            Do Until h < 0
                If Vettore_Orari.Dalle(h, g) <> "00.00" Or Vettore_Orari.Alle(h, g) <> "00.00" Or Vettore_Orari.Giustificativo(h, g) <> "" Then
                    If (Vettore_Orari.Dalle(h, g) = "00.00" And Vettore_Orari.Alle(h, g) = "24.00" And Vettore_Orari.Tipo(h, g) = "A") Or Vettore_Orari.Tipo(h, g) = "I" Then
                        h = h - 1
                    Else
                        If Vettore_Orari.Alle(h, g) > Timbratura.Alle Then
                            If Vettore_Orari.Dalle(h, g) < Timbratura.Alle Then
                                Orario_Timbratura.Alle = Timbratura.Alle
                                If Timbratura.Dalle_Causale <> "" Then
                                    Orario_Timbratura.Causale = Timbratura.Dalle_Causale
                                Else
                                    Orario_Timbratura.Causale = Timbratura.Alle_Causale
                                End If
                                If Timbratura.Dalle_Colore <> vbWhite Then
                                    Orario_Timbratura.Colore = Timbratura.Dalle_Colore
                                Else
                                    Orario_Timbratura.Colore = Timbratura.Alle_Colore
                                End If
                                Orario_Timbratura.Giustificativo = cpt.GiustificativoSostituzione
                                Orario_Timbratura.NelGruppo = Vettore_Orari.NelGruppo(h, g)
                                Orario_Timbratura.Familiare = Vettore_Orari.Familiare(h, g)
                                Orario_Timbratura.Tipo = "Sostituzione"
                                Orario_Timbratura.TipoServizio = Vettore_Orari.TipoServizio(h, g)
                                Orario_Timbratura.GiornoSuccessivo = Vettore_Orari.GiornoSuccessivo(h, g)
                                If Vettore_Orari.Dalle(h, g) <= Timbratura.Dalle Then
                                    Orario_Timbratura.Dalle = Timbratura.Dalle
                                    If Vettore_Orari.Tipo(h, g) = "P" Or (Vettore_Orari.Tipo(h, g) = "A" And cg.CampoGiustificativi(ConnectionString, Vettore_Orari.Giustificativo(h, g), "RegoleGenerali") = "S") Then
                                        Risultato = AggiungiTurniTimbrature(Dipendente, g, x, XR, Orario_Timbratura, Vettore_Orari_Timbrature)
                                        AbbinaTimbratureOrari = AbbinaTimbratureOrari & Risultato
                                    Else
                                        Orario_Timbratura.Pulisci()
                                    End If
                                    Exit Do
                                Else
                                    Orario_Timbratura.Dalle = Vettore_Orari.Dalle(h, g)
                                    Timbratura.Alle = Vettore_Orari.Dalle(h, g)
                                    If Vettore_Orari.Tipo(h, g) = "P" Or (Vettore_Orari.Tipo(h, g) = "A" And cg.CampoGiustificativi(ConnectionString, Vettore_Orari.Giustificativo(h, g), "RegoleGenerali") = "S") Then
                                        Risultato = AggiungiTurniTimbrature(Dipendente, g, x, XR, Orario_Timbratura, Vettore_Orari_Timbrature)
                                        AbbinaTimbratureOrari = AbbinaTimbratureOrari & Risultato
                                    Else
                                        Orario_Timbratura.Pulisci()
                                    End If
                                    h = h - 1
                                End If
                            Else
                                If h > 0 Then
                                    h = h - 1
                                Else
                                    Orario_Timbratura.Dalle = Timbratura.Dalle
                                    Orario_Timbratura.Alle = Timbratura.Alle
                                    If Timbratura.Dalle_Causale <> "" Then
                                        Orario_Timbratura.Causale = Timbratura.Dalle_Causale
                                    Else
                                        Orario_Timbratura.Causale = Timbratura.Alle_Causale
                                    End If
                                    If Timbratura.Dalle_Colore <> vbWhite Then
                                        Orario_Timbratura.Colore = Timbratura.Dalle_Colore
                                    Else
                                        Orario_Timbratura.Colore = Timbratura.Alle_Colore
                                    End If
                                    If ControlloOrario = "S" Then
                                        Orario_Timbratura.Tipo = "EntrataAnticipata"
                                        Orario_Timbratura.Giustificativo = cpt.GiustificativoEntrataAnticipata
                                    Else
                                        Orario_Timbratura.Tipo = ""
                                        Orario_Timbratura.Giustificativo = Vettore_Orari.Giustificativo(h, g)
                                    End If
                                    Orario_Timbratura.NelGruppo = Vettore_Orari.NelGruppo(h, g)
                                    Orario_Timbratura.Familiare = Vettore_Orari.Familiare(h, g)
                                    Orario_Timbratura.TipoServizio = Vettore_Orari.TipoServizio(h, g)
                                    Orario_Timbratura.GiornoSuccessivo = Vettore_Orari.GiornoSuccessivo(h, g)
                                    If Vettore_Orari.Tipo(h, g) = "P" Or (Vettore_Orari.Tipo(h, g) = "A" And cg.CampoGiustificativi(ConnectionString, Vettore_Orari.Giustificativo(h, g), "RegoleGenerali") = "S") Then
                                        Risultato = AggiungiTurniTimbrature(Dipendente, g, x, XR, Orario_Timbratura, Vettore_Orari_Timbrature)
                                        AbbinaTimbratureOrari = AbbinaTimbratureOrari & Risultato
                                    Else
                                        Orario_Timbratura.Pulisci()
                                    End If
                                    Exit Do
                                End If
                            End If
                        End If

                        If h >= 0 Then
                            If Vettore_Orari.Alle(h, g) = Timbratura.Alle Then
                                Orario_Timbratura.Alle = Timbratura.Alle
                                If Timbratura.Dalle_Causale <> "" Then
                                    Orario_Timbratura.Causale = Timbratura.Dalle_Causale
                                Else
                                    Orario_Timbratura.Causale = Timbratura.Alle_Causale
                                End If
                                If Timbratura.Dalle_Colore <> vbWhite Then
                                    Orario_Timbratura.Colore = Timbratura.Dalle_Colore
                                Else
                                    Orario_Timbratura.Colore = Timbratura.Alle_Colore
                                End If
                                Orario_Timbratura.Giustificativo = cpt.GiustificativoSostituzione
                                Orario_Timbratura.NelGruppo = Vettore_Orari.NelGruppo(h, g)
                                Orario_Timbratura.Familiare = Vettore_Orari.Familiare(h, g)
                                Orario_Timbratura.Tipo = "Sostituzione"
                                Orario_Timbratura.TipoServizio = Vettore_Orari.TipoServizio(h, g)
                                Orario_Timbratura.GiornoSuccessivo = Vettore_Orari.GiornoSuccessivo(h, g)
                                If Vettore_Orari.Dalle(h, g) <= Timbratura.Dalle Then
                                    Orario_Timbratura.Dalle = Timbratura.Dalle
                                    If Vettore_Orari.Tipo(h, g) = "P" Or (Vettore_Orari.Tipo(h, g) = "A" And cg.CampoGiustificativi(ConnectionString, Vettore_Orari.Giustificativo(h, g), "RegoleGenerali") = "S") Then
                                        Risultato = AggiungiTurniTimbrature(Dipendente, g, x, XR, Orario_Timbratura, Vettore_Orari_Timbrature)
                                        AbbinaTimbratureOrari = AbbinaTimbratureOrari & Risultato
                                    Else
                                        Orario_Timbratura.Pulisci()
                                    End If
                                    Exit Do
                                Else
                                    Orario_Timbratura.Dalle = Vettore_Orari.Dalle(h, g)
                                    Timbratura.Alle = Vettore_Orari.Dalle(h, g)
                                    If Vettore_Orari.Tipo(h, g) = "P" Or (Vettore_Orari.Tipo(h, g) = "A" And cg.CampoGiustificativi(ConnectionString, Vettore_Orari.Giustificativo(h, g), "RegoleGenerali") = "S") Then
                                        Risultato = AggiungiTurniTimbrature(Dipendente, g, x, XR, Orario_Timbratura, Vettore_Orari_Timbrature)
                                        AbbinaTimbratureOrari = AbbinaTimbratureOrari & Risultato
                                    Else
                                        Orario_Timbratura.Pulisci()
                                    End If
                                    h = h - 1
                                End If
                            End If
                        End If

                        If h >= 0 Then
                            If Vettore_Orari.Alle(h, g) < Timbratura.Alle Then
                                If Vettore_Orari.Alle(h, g) < Timbratura.Dalle Then
                                    Orario_Timbratura.Dalle = Timbratura.Dalle
                                    Orario_Timbratura.Alle = Timbratura.Alle
                                    If Timbratura.Dalle_Causale <> "" Then
                                        Orario_Timbratura.Causale = Timbratura.Dalle_Causale
                                    Else
                                        Orario_Timbratura.Causale = Timbratura.Alle_Causale
                                    End If
                                    If Timbratura.Dalle_Colore <> vbWhite Then
                                        Orario_Timbratura.Colore = Timbratura.Dalle_Colore
                                    Else
                                        Orario_Timbratura.Colore = Timbratura.Alle_Colore
                                    End If
                                    If Vettore_Orari.Dalle(0, g + 1) = "00.00" And Vettore_Orari.Tipo(0, g + 1) = "P" Then
                                        If ControlloOrario = "S" Then
                                            Orario_Timbratura.Tipo = "EntrataAnticipata"
                                            Orario_Timbratura.Giustificativo = cpt.GiustificativoEntrataAnticipata
                                        Else
                                            Orario_Timbratura.Tipo = ""
                                            Orario_Timbratura.Giustificativo = Vettore_Orari.Giustificativo(h, g)
                                        End If
                                    Else
                                        Orario_Timbratura.Giustificativo = cpt.GiustificativoSostituzione
                                        Orario_Timbratura.Tipo = "Sostituzione"
                                    End If
                                    Orario_Timbratura.NelGruppo = Vettore_Orari.NelGruppo(h, g)
                                    Orario_Timbratura.Familiare = Vettore_Orari.Familiare(h, g)
                                    Orario_Timbratura.TipoServizio = Vettore_Orari.TipoServizio(h, g)
                                    Orario_Timbratura.GiornoSuccessivo = Vettore_Orari.GiornoSuccessivo(h, g)
                                    If Vettore_Orari.Tipo(h, g) = "P" Or (Vettore_Orari.Tipo(h, g) = "A" And cg.CampoGiustificativi(ConnectionString, Vettore_Orari.Giustificativo(h, g), "RegoleGenerali") = "S") Then
                                        Risultato = AggiungiTurniTimbrature(Dipendente, g, x, XR, Orario_Timbratura, Vettore_Orari_Timbrature)
                                        AbbinaTimbratureOrari = AbbinaTimbratureOrari & Risultato
                                    Else
                                        Orario_Timbratura.Pulisci()
                                    End If
                                    Exit Do
                                End If
                                If Vettore_Orari.Alle(h, g) = Timbratura.Dalle Then
                                    Orario_Timbratura.Dalle = Timbratura.Dalle
                                    Orario_Timbratura.Alle = Timbratura.Alle
                                    If Timbratura.Dalle_Causale <> "" Then
                                        Orario_Timbratura.Causale = Timbratura.Dalle_Causale
                                    Else
                                        Orario_Timbratura.Causale = Timbratura.Alle_Causale
                                    End If
                                    If Timbratura.Dalle_Colore <> vbWhite Then
                                        Orario_Timbratura.Colore = Timbratura.Dalle_Colore
                                    Else
                                        Orario_Timbratura.Colore = Timbratura.Alle_Colore
                                    End If
                                    If ControlloOrario = "S" Then
                                        Orario_Timbratura.Tipo = "UscitaPosticipata"
                                        Orario_Timbratura.Giustificativo = cpt.GiustificativoUscitaPosticipata
                                    Else
                                        Orario_Timbratura.Tipo = ""
                                        Orario_Timbratura.Giustificativo = Vettore_Orari.Giustificativo(h, g)
                                    End If
                                    Orario_Timbratura.NelGruppo = Vettore_Orari.NelGruppo(h, g)
                                    Orario_Timbratura.Familiare = Vettore_Orari.Familiare(h, g)
                                    Orario_Timbratura.TipoServizio = Vettore_Orari.TipoServizio(h, g)
                                    Orario_Timbratura.GiornoSuccessivo = Vettore_Orari.GiornoSuccessivo(h, g)
                                    If Vettore_Orari.Tipo(h, g) = "P" Or (Vettore_Orari.Tipo(h, g) = "A" And cg.CampoGiustificativi(ConnectionString, Vettore_Orari.Giustificativo(h, g), "RegoleGenerali") = "S") Then
                                        Risultato = AggiungiTurniTimbrature(Dipendente, g, x, XR, Orario_Timbratura, Vettore_Orari_Timbrature)
                                        AbbinaTimbratureOrari = AbbinaTimbratureOrari & Risultato
                                    Else
                                        Orario_Timbratura.Pulisci()
                                    End If
                                    Exit Do
                                End If
                                If Vettore_Orari.Alle(h, g) > Timbratura.Dalle Then
                                    Orario_Timbratura.Dalle = Vettore_Orari.Alle(h, g)
                                    Orario_Timbratura.Alle = Timbratura.Alle
                                    If Timbratura.Dalle_Causale <> "" Then
                                        Orario_Timbratura.Causale = Timbratura.Dalle_Causale
                                    Else
                                        Orario_Timbratura.Causale = Timbratura.Alle_Causale
                                    End If
                                    If Timbratura.Dalle_Colore <> vbWhite Then
                                        Orario_Timbratura.Colore = Timbratura.Dalle_Colore
                                    Else
                                        Orario_Timbratura.Colore = Timbratura.Alle_Colore
                                    End If
                                    If ControlloOrario = "S" Then
                                        Orario_Timbratura.Tipo = "UscitaPosticipata"
                                        Orario_Timbratura.Giustificativo = cpt.GiustificativoUscitaPosticipata
                                    Else
                                        Orario_Timbratura.Tipo = ""
                                        Orario_Timbratura.Giustificativo = Vettore_Orari.Giustificativo(h, g)
                                    End If
                                    Orario_Timbratura.NelGruppo = Vettore_Orari.NelGruppo(h, g)
                                    Orario_Timbratura.Familiare = Vettore_Orari.Familiare(h, g)
                                    Orario_Timbratura.TipoServizio = Vettore_Orari.TipoServizio(h, g)
                                    Orario_Timbratura.GiornoSuccessivo = Vettore_Orari.GiornoSuccessivo(h, g)
                                    If Vettore_Orari.Tipo(h, g) = "P" Or (Vettore_Orari.Tipo(h, g) = "A" And cg.CampoGiustificativi(ConnectionString, Vettore_Orari.Giustificativo(h, g), "RegoleGenerali") = "S") Then
                                        Risultato = AggiungiTurniTimbrature(Dipendente, g, x, XR, Orario_Timbratura, Vettore_Orari_Timbrature)
                                        AbbinaTimbratureOrari = AbbinaTimbratureOrari & Risultato
                                    Else
                                        Orario_Timbratura.Pulisci()
                                    End If
                                    Timbratura.Alle = Vettore_Orari.Alle(h, g)
                                End If
                            End If
                        End If
                    End If
                Else
                    h = h - 1
                End If
            Loop

            If h < 0 Then
                If Timbratura.Dalle = "00.00" Then
                    If Timbratura.Proseguimento = "S" Then
                        Orario_Timbratura.Dalle = Timbratura.Dalle
                        Orario_Timbratura.Alle = Timbratura.Alle
                        If Timbratura.Dalle_Causale <> "" Then
                            Orario_Timbratura.Causale = Timbratura.Dalle_Causale
                        Else
                            Orario_Timbratura.Causale = Timbratura.Alle_Causale
                        End If
                        If Timbratura.Dalle_Colore <> vbWhite Then
                            Orario_Timbratura.Colore = Timbratura.Dalle_Colore
                        Else
                            Orario_Timbratura.Colore = Timbratura.Alle_Colore
                        End If
                        If ControlloOrario = "S" Then
                            Orario_Timbratura.Tipo = "UscitaPosticipata"
                            Orario_Timbratura.Giustificativo = cpt.GiustificativoUscitaPosticipata
                        Else
                            Orario_Timbratura.Tipo = ""
                            Orario_Timbratura.Giustificativo = cpt.GiustificativoOrarioLavoro
                        End If
                        Orario_Timbratura.NelGruppo = ""
                        Orario_Timbratura.Familiare = 0
                        Orario_Timbratura.TipoServizio = ""
                        Orario_Timbratura.GiornoSuccessivo = ""
                        Orario_Timbratura.GiornoSuccessivo = Timbratura.Proseguimento
                        Risultato = AggiungiTurniTimbrature(Dipendente, g, x, XR, Orario_Timbratura, Vettore_Orari_Timbrature)
                        AbbinaTimbratureOrari = AbbinaTimbratureOrari & Risultato
                    Else
                        Orario_Timbratura.Dalle = Timbratura.Dalle
                        Orario_Timbratura.Alle = Timbratura.Alle
                        If Timbratura.Dalle_Causale <> "" Then
                            Orario_Timbratura.Causale = Timbratura.Dalle_Causale
                        Else
                            Orario_Timbratura.Causale = Timbratura.Alle_Causale
                        End If
                        If Timbratura.Dalle_Colore <> vbWhite Then
                            Orario_Timbratura.Colore = Timbratura.Dalle_Colore
                        Else
                            Orario_Timbratura.Colore = Timbratura.Alle_Colore
                        End If
                        If ControlloOrario = "S" Then
                            Orario_Timbratura.Tipo = "EntrataAnticipata"
                            Orario_Timbratura.Giustificativo = cpt.GiustificativoEntrataAnticipata
                        Else
                            Orario_Timbratura.Tipo = ""
                            Orario_Timbratura.Giustificativo = cpt.GiustificativoOrarioLavoro
                        End If
                        Orario_Timbratura.NelGruppo = ""
                        Orario_Timbratura.Familiare = 0
                        Orario_Timbratura.TipoServizio = ""
                        Orario_Timbratura.GiornoSuccessivo = ""
                        Risultato = AggiungiTurniTimbrature(Dipendente, g, x, XR, Orario_Timbratura, Vettore_Orari_Timbrature)
                        AbbinaTimbratureOrari = AbbinaTimbratureOrari & Risultato
                    End If
                Else
                    If Timbratura.Alle = "24.00" Then
                        Orario_Timbratura.Dalle = Timbratura.Dalle
                        Orario_Timbratura.Alle = Timbratura.Alle
                        If Timbratura.Dalle_Causale <> "" Then
                            Orario_Timbratura.Causale = Timbratura.Dalle_Causale
                        Else
                            Orario_Timbratura.Causale = Timbratura.Alle_Causale
                        End If
                        If Timbratura.Dalle_Colore <> vbWhite Then
                            Orario_Timbratura.Colore = Timbratura.Dalle_Colore
                        Else
                            Orario_Timbratura.Colore = Timbratura.Alle_Colore
                        End If
                        If ControlloOrario = "S" Then
                            Orario_Timbratura.Tipo = "EntrataAnticipata"
                            Orario_Timbratura.Giustificativo = cpt.GiustificativoEntrataAnticipata
                        Else
                            Orario_Timbratura.Tipo = ""
                            Orario_Timbratura.Giustificativo = cpt.GiustificativoOrarioLavoro
                        End If
                        Orario_Timbratura.NelGruppo = ""
                        Orario_Timbratura.Familiare = 0
                        Orario_Timbratura.TipoServizio = ""
                        Orario_Timbratura.GiornoSuccessivo = ""
                        Risultato = AggiungiTurniTimbrature(Dipendente, g, x, XR, Orario_Timbratura, Vettore_Orari_Timbrature)
                        AbbinaTimbratureOrari = AbbinaTimbratureOrari & Risultato
                    Else
                        Orario_Timbratura.Dalle = Timbratura.Dalle
                        Orario_Timbratura.Alle = Timbratura.Alle
                        If Timbratura.Dalle_Causale <> "" Then
                            Orario_Timbratura.Causale = Timbratura.Dalle_Causale
                        Else
                            Orario_Timbratura.Causale = Timbratura.Alle_Causale
                        End If
                        If Timbratura.Dalle_Colore <> vbWhite Then
                            Orario_Timbratura.Colore = Timbratura.Dalle_Colore
                        Else
                            Orario_Timbratura.Colore = Timbratura.Alle_Colore
                        End If
                        Orario_Timbratura.Giustificativo = cpt.GiustificativoSostituzione
                        Orario_Timbratura.NelGruppo = ""
                        Orario_Timbratura.Familiare = 0
                        Orario_Timbratura.Tipo = "Sostituzione"
                        Orario_Timbratura.TipoServizio = ""
                        Orario_Timbratura.GiornoSuccessivo = ""
                        Risultato = AggiungiTurniTimbrature(Dipendente, g, x, XR, Orario_Timbratura, Vettore_Orari_Timbrature)
                        AbbinaTimbratureOrari = AbbinaTimbratureOrari & Risultato
                    End If
                End If
            End If
        End If
    End Function

    Private Function AbbinaAssenzeEntrataOrari(ByVal Dipendente As Long, ByVal Anno As Integer, ByVal Mese As Byte, ByRef g As Object, ByRef x As Object, ByRef XR As Object, ByRef Vettore_Orari As Cls_Struttura_Orari, ByRef Vettore_Orari_Timbrature As Cls_Struttura_Orari_Timbrature, ByRef Assenza As Cls_Struttura_Timbratura, ByRef Timbratura As Cls_Struttura_Timbratura) As String
        Dim Orario_Timbratura As New Cls_Struttura_Orario_Timbratura
        Dim cg As New Cls_Giustificativi
        Dim cpt As New Cls_ParametriTurni
        cpt.Leggi(ConnectionString)

        Dim W_Alle As String = "00.00"

        If Timbratura.Dalle = "" Then Timbratura.Dalle = "00.00"

        Orario_Timbratura.Pulisci()

        Dim h As Integer = 0
        Dim Sw_Fine As Boolean = False

        h = 0
        Do Until h > 9
            If Sw_Fine = True Then Exit Do
            If Vettore_Orari.Dalle(h, g) = "00.00" And Vettore_Orari.Alle(h, g) = "00.00" And Vettore_Orari.Giustificativo(h, g) = "" Then Exit Do
            If Vettore_Orari.Tipo(h, g) <> "I" Then
                ' Orari.Dalle < Assenze.Dalle
                If Vettore_Orari.Dalle(h, g) < Assenza.Dalle Then
                    If Vettore_Orari.Alle(h, g) > Assenza.Dalle Then
                        Orario_Timbratura.Dalle = Assenza.Dalle
                        Orario_Timbratura.Causale = Assenza.Dalle_Causale
                        Orario_Timbratura.Colore = Assenza.Dalle_Colore
                        Orario_Timbratura.Alle = Assenza.Alle
                        If Orario_Timbratura.Causale = "" Then Orario_Timbratura.Causale = Assenza.Alle_Causale
                        If Orario_Timbratura.Colore = vbWhite Then Orario_Timbratura.Colore = Assenza.Alle_Colore
                        Orario_Timbratura.Tipo = "Assenza"
                        Orario_Timbratura.Qualita = ""
                        If Vettore_Orari.Giustificativo(h, g) <> "" Then
                            If Vettore_Orari.Tipo(h, g) = "A" Then
                                Orario_Timbratura.Giustificativo = Vettore_Orari.Giustificativo(h, g)
                            Else
                                If cg.CampoGiustificativi(ConnectionString, Vettore_Orari.Giustificativo(h, g), "LavoroOrdinario") = "N" Then Orario_Timbratura.Qualita = Orario_Timbratura.Qualita & "Straordinario"
                                Orario_Timbratura.Giustificativo = cg.CampoGiustificativi(ConnectionString, Vettore_Orari.Giustificativo(h, g), "GiustificativoPerAssenza")
                            End If
                        Else
                            Orario_Timbratura.Giustificativo = cpt.GiustificativoAssenza
                        End If
                        Orario_Timbratura.NelGruppo = Vettore_Orari.NelGruppo(h, g)
                        Orario_Timbratura.Familiare = Vettore_Orari.Familiare(h, g)
                        Orario_Timbratura.TipoServizio = Vettore_Orari.TipoServizio(h, g)
                        Orario_Timbratura.GiornoSuccessivo = Vettore_Orari.GiornoSuccessivo(h, g)
                        If Vettore_Orari.Tipo(h, g) = "P" Or (Vettore_Orari.Tipo(h, g) = "A" And cg.CampoGiustificativi(ConnectionString, Vettore_Orari.Giustificativo(h, g), "RegoleGenerali") = "S") Then
                            Risultato = AggiungiTurniTimbrature(Dipendente, g, x, XR, Orario_Timbratura, Vettore_Orari_Timbrature)
                            AbbinaAssenzeEntrataOrari = AbbinaAssenzeEntrataOrari & Risultato
                        Else
                            Orario_Timbratura.Pulisci()
                        End If
                        Sw_Fine = True
                    End If
                Else
                    ' Orari.Dalle >= Assenze.Dalle
                    If Vettore_Orari.Dalle(h, g) < Assenza.Alle Then
                        Orario_Timbratura.Dalle = Vettore_Orari.Dalle(h, g)
                        Orario_Timbratura.Causale = ""
                        Orario_Timbratura.Colore = vbWhite
                        If Vettore_Orari.Alle(h, g) <= Assenza.Alle Then
                            Orario_Timbratura.Alle = Vettore_Orari.Alle(h, g)
                            Orario_Timbratura.Tipo = "Assenza"
                            Orario_Timbratura.Qualita = ""
                            If Vettore_Orari.Giustificativo(h, g) <> "" Then
                                If Vettore_Orari.Tipo(h, g) = "A" Then
                                    Orario_Timbratura.Giustificativo = Vettore_Orari.Giustificativo(h, g)
                                Else
                                    If cg.CampoGiustificativi(ConnectionString, Vettore_Orari.Giustificativo(h, g), "LavoroOrdinario") = "N" Then Orario_Timbratura.Qualita = Orario_Timbratura.Qualita & "Straordinario"
                                    Orario_Timbratura.Giustificativo = cg.CampoGiustificativi(ConnectionString, Vettore_Orari.Giustificativo(h, g), "GiustificativoPerAssenza")
                                End If
                            Else
                                Orario_Timbratura.Giustificativo = cpt.GiustificativoAssenza
                            End If
                            Orario_Timbratura.NelGruppo = Vettore_Orari.NelGruppo(h, g)
                            Orario_Timbratura.Familiare = Vettore_Orari.Familiare(h, g)
                            Orario_Timbratura.TipoServizio = Vettore_Orari.TipoServizio(h, g)
                            Orario_Timbratura.GiornoSuccessivo = Vettore_Orari.GiornoSuccessivo(h, g)
                            If Vettore_Orari.Tipo(h, g) = "P" Or (Vettore_Orari.Tipo(h, g) = "A" And cg.CampoGiustificativi(ConnectionString, Vettore_Orari.Giustificativo(h, g), "RegoleGenerali") = "S") Then
                                Risultato = AggiungiTurniTimbrature(Dipendente, g, x, XR, Orario_Timbratura, Vettore_Orari_Timbrature)
                                AbbinaAssenzeEntrataOrari = AbbinaAssenzeEntrataOrari & Risultato
                            Else
                                Orario_Timbratura.Pulisci()
                            End If
                        Else
                            Orario_Timbratura.Alle = Assenza.Alle
                            If Orario_Timbratura.Causale = "" Then Orario_Timbratura.Causale = Assenza.Alle_Causale
                            If Orario_Timbratura.Colore = vbWhite Then Orario_Timbratura.Colore = Assenza.Alle_Colore
                            Orario_Timbratura.Tipo = "EntrataPosticipata"
                            Orario_Timbratura.Qualita = ""
                            If Vettore_Orari.Giustificativo(h, g) <> "" Then
                                If Vettore_Orari.Tipo(h, g) = "A" Then
                                    Orario_Timbratura.Giustificativo = Vettore_Orari.Giustificativo(h, g)
                                Else
                                    If cg.CampoGiustificativi(ConnectionString, Vettore_Orari.Giustificativo(h, g), "LavoroOrdinario") = "N" Then Orario_Timbratura.Qualita = Orario_Timbratura.Qualita & "Straordinario"
                                    Orario_Timbratura.Giustificativo = cpt.GiustificativoEntrataPosticipata
                                End If
                            Else
                                Orario_Timbratura.Giustificativo = cpt.GiustificativoEntrataPosticipata
                            End If
                            Orario_Timbratura.NelGruppo = Vettore_Orari.NelGruppo(h, g)
                            Orario_Timbratura.Familiare = Vettore_Orari.Familiare(h, g)
                            Orario_Timbratura.TipoServizio = Vettore_Orari.TipoServizio(h, g)
                            Orario_Timbratura.GiornoSuccessivo = Vettore_Orari.GiornoSuccessivo(h, g)
                            If Vettore_Orari.Tipo(h, g) = "P" Or (Vettore_Orari.Tipo(h, g) = "A" And cg.CampoGiustificativi(ConnectionString, Vettore_Orari.Giustificativo(h, g), "RegoleGenerali") = "S") Then
                                Risultato = AggiungiTurniTimbrature(Dipendente, g, x, XR, Orario_Timbratura, Vettore_Orari_Timbrature)
                                AbbinaAssenzeEntrataOrari = AbbinaAssenzeEntrataOrari & Risultato
                            Else
                                Orario_Timbratura.Pulisci()
                            End If
                            If Vettore_Orari.DalleObbligo(h, g) <> "00.00" And Vettore_Orari.DalleObbligo(h, g) < Timbratura.Dalle Then
                                If cg.CampoGiustificativi(ConnectionString, Vettore_Orari.Giustificativo(h, g), "ControlloFasciaObbligatoria") = "S" Then
                                    AbbinaAssenzeEntrataOrari = AbbinaAssenzeEntrataOrari & "(W) Il giorno : " & g & " Entrata " & Timbratura.Dalle & " fuori della fascia obbligatoria" & vbNewLine
                                End If
                            End If
                            Sw_Fine = True
                        End If
                    End If
                End If
            End If
            h = h + 1
        Loop
    End Function

    Private Function AbbinaAssenzeOrariFlex(ByVal Dipendente As Long, ByVal Anno As Integer, ByVal Mese As Byte, ByRef g As Object, ByRef x As Object, ByRef XR As Object, ByVal CodiceSuperGruppo As String, ByRef Vettore_Orari As Cls_Struttura_Orari, ByRef Vettore_Orari_Timbrature As Cls_Struttura_Orari_Timbrature, ByRef Assenza As Cls_Struttura_Timbratura, ByRef Timbratura As Cls_Struttura_Timbratura, ByVal ControlloOrario As String) As String
        Dim cxst As New Cls_ClsXScriptTurni
        cxst.ConnectionString = ConnectionString
        cxst.Utente = Utente

        Dim Orario_Timbratura As New Cls_Struttura_Orario_Timbratura
        Dim cg As New Cls_Giustificativi
        Dim cpt As New Cls_ParametriTurni
        cpt.Leggi(ConnectionString)

        Orario_Timbratura.Pulisci()

        Dim Data As Date = DateSerial(Anno, Mese, g)

        If Assenza.Dalle_Causale <> "" Or Assenza.Alle_Causale <> "" Then
            Orario_Timbratura.Dalle = Assenza.Dalle
            Orario_Timbratura.Alle = Assenza.Alle
            Orario_Timbratura.Pausa = "00.00"
            If Assenza.Dalle_Causale <> "" Then
                Orario_Timbratura.Causale = Assenza.Dalle_Causale
            Else
                Orario_Timbratura.Causale = Assenza.Alle_Causale
            End If
            Orario_Timbratura.Colore = Assenza.Dalle_Colore
            If Orario_Timbratura.Colore = vbWhite Then Orario_Timbratura.Colore = Assenza.Alle_Colore
            Orario_Timbratura.Giustificativo = cpt.GiustificativoAssenza
            Orario_Timbratura.NelGruppo = ""
            Orario_Timbratura.Familiare = 0
            Orario_Timbratura.Tipo = "Assenza"
            Orario_Timbratura.Qualita = ""
            If Weekday(Data) = FestivitaSettimanale Or cxst.GiornoFestivo(Data, CodiceSuperGruppo) = True Then Orario_Timbratura.Qualita = Orario_Timbratura.Qualita & "Festivo"
            Orario_Timbratura.TipoServizio = ""
            Orario_Timbratura.GiornoSuccessivo = ""
            '    If ControlloOrario = "S" Then
            Risultato = AggiungiTurniTimbrature(Dipendente, g, x, XR, Orario_Timbratura, Vettore_Orari_Timbrature)
            AbbinaAssenzeOrariFlex = AbbinaAssenzeOrariFlex & Risultato
            '    End If
            Exit Function
        End If


        Dim h As Byte = 0
        Dim Sw_Fine As Boolean = False

        Do Until h > 9
            If Sw_Fine = True Then Exit Do
            If Vettore_Orari.Dalle(h, g) = "00.00" And Vettore_Orari.Alle(h, g) = "00.00" And Vettore_Orari.Giustificativo(h, g) = "" Then Exit Do
            '    If Vettore_Orari.Tipo(h, g) <> "I" And cg.CampoGiustificativi(ConnectionString, Vettore_Orari.Giustificativo(h, g), "LavoroOrdinario") <> "N" Then
            '    If Vettore_Orari.Tipo(h, g) = "P" Or (Vettore_Orari.Tipo(h, g) = "A" And cg.CampoGiustificativi(ConnectionString,Vettore_Orari.Giustificativo(h, g), "LavoroOrdinario") = "N") Then
            If Vettore_Orari.Tipo(h, g) <> "I" Then
                If Vettore_Orari.Dalle(h, g) = "00.00" And Vettore_Orari.Alle(h, g) = "24.00" And Vettore_Orari.Tipo(h, g) = "A" Then
                    Vettore_Orari.Alle(h, g) = "00.00"
                    Orario_Timbratura.Dalle = "00.00"
                    Orario_Timbratura.Alle = "24.00"
                    Orario_Timbratura.Tipo = "Assenza"
                    Orario_Timbratura.Qualita = ""
                    Orario_Timbratura.Giustificativo = Vettore_Orari.Giustificativo(h, g)
                    Orario_Timbratura.NelGruppo = Vettore_Orari.NelGruppo(h, g)
                    Orario_Timbratura.Familiare = Vettore_Orari.Familiare(h, g)
                    Orario_Timbratura.TipoServizio = Vettore_Orari.TipoServizio(h, g)
                    Orario_Timbratura.GiornoSuccessivo = Vettore_Orari.GiornoSuccessivo(h, g)
                    If cg.CampoGiustificativi(ConnectionString, Vettore_Orari.Giustificativo(h, g), "RegoleGenerali") = "S" Then
                        Risultato = AggiungiTurniTimbrature(Dipendente, g, x, XR, Orario_Timbratura, Vettore_Orari_Timbrature)
                        AbbinaAssenzeOrariFlex = AbbinaAssenzeOrariFlex & Risultato
                    Else
                        Orario_Timbratura.Pulisci()
                    End If
                Else
                    ' Orari.Dalle <= Assenze.Dalle
                    If Vettore_Orari.DalleBasso(h, g) <= Assenza.Dalle Then
                        If Vettore_Orari.AlleAlto(h, g) > Assenza.Dalle Then
                            Orario_Timbratura.Dalle = Assenza.Dalle
                            Orario_Timbratura.Causale = Assenza.Dalle_Causale
                            Orario_Timbratura.Colore = Assenza.Dalle_Colore
                            If Vettore_Orari.AlleAlto(h, g) <= Assenza.Alle Then
                                '              If Assenza.Alle > Vettore_Orari.AlleBasso(h, g) Then
                                '                Orario_Timbratura.Alle = Vettore_Orari.AlleBasso(h, g)
                                '                Else
                                '                Orario_Timbratura.Alle = Assenza.Alle
                                '                If Orario_Timbratura.Causale = "" Then Orario_Timbratura.Causale = Assenza.Alle_Causale
                                '              End If
                                Orario_Timbratura.Alle = Vettore_Orari.AlleAlto(h, g)
                                If Orario_Timbratura.Causale = "" Then Orario_Timbratura.Causale = Assenza.Alle_Causale
                                Assenza.Dalle = Vettore_Orari.AlleAlto(h, g)

                                Orario_Timbratura.Tipo = "UscitaAnticipata"
                                Orario_Timbratura.Qualita = ""
                                Data = DateSerial(Anno, Mese, g)
                                If Timbratura.Dalle = "" And h = 0 Then Data = DateAdd("d", -1, Data)
                                If Weekday(Data) = FestivitaSettimanale Or cxst.GiornoFestivo(Data, CodiceSuperGruppo) = True Then Orario_Timbratura.Qualita = Orario_Timbratura.Qualita & "Festivo"
                                If Vettore_Orari.Giustificativo(h, g) <> "" Then
                                    If Vettore_Orari.Tipo(h, g) = "A" Then
                                        Orario_Timbratura.Giustificativo = Vettore_Orari.Giustificativo(h, g)
                                    Else
                                        If cg.CampoGiustificativi(ConnectionString, Vettore_Orari.Giustificativo(h, g), "LavoroOrdinario") = "N" Then Orario_Timbratura.Qualita = Orario_Timbratura.Qualita & "Straordinario"
                                        Orario_Timbratura.Giustificativo = cpt.GiustificativoUscitaAnticipata
                                    End If
                                Else
                                    Orario_Timbratura.Giustificativo = cpt.GiustificativoUscitaAnticipata
                                End If
                                Orario_Timbratura.NelGruppo = Vettore_Orari.NelGruppo(h, g)
                                Orario_Timbratura.Familiare = Vettore_Orari.Familiare(h, g)
                                Orario_Timbratura.TipoServizio = Vettore_Orari.TipoServizio(h, g)
                                Orario_Timbratura.GiornoSuccessivo = Vettore_Orari.GiornoSuccessivo(h, g)
                                If Vettore_Orari.Tipo(h, g) = "P" Or (Vettore_Orari.Tipo(h, g) = "A" And cg.CampoGiustificativi(ConnectionString, Vettore_Orari.Giustificativo(h, g), "RegoleGenerali") = "S") Then
                                    '                If ControlloOrario = "S" Then
                                    Risultato = AggiungiTurniTimbrature(Dipendente, g, x, XR, Orario_Timbratura, Vettore_Orari_Timbrature)
                                    AbbinaAssenzeOrariFlex = AbbinaAssenzeOrariFlex & Risultato
                                    '                End If
                                Else
                                    Orario_Timbratura.Pulisci()
                                End If
                                If Vettore_Orari.AlleObbligo(h, g) <> "00.00" And Vettore_Orari.AlleObbligo(h, g) > Assenza.Alle Then
                                    If cg.CampoGiustificativi(ConnectionString, Vettore_Orari.Giustificativo(h, g), "ControlloFasciaObbligatoria") = "S" Then
                                        AbbinaAssenzeOrariFlex = AbbinaAssenzeOrariFlex & "(W) Il giorno : " & g & " Uscita " & Assenza.Alle & " fuori della fascia obbligatoria" & vbNewLine
                                    End If
                                End If
                            Else
                                '              Sw_Ok = True
                                Orario_Timbratura.Alle = Assenza.Alle
                                If Orario_Timbratura.Causale = "" Then Orario_Timbratura.Causale = Assenza.Alle_Causale
                                If Orario_Timbratura.Colore = vbWhite Then Orario_Timbratura.Colore = Assenza.Alle_Colore
                                Orario_Timbratura.Tipo = "Assenza"
                                Orario_Timbratura.Qualita = ""
                                If Vettore_Orari.Giustificativo(h, g) <> "" Then
                                    If Vettore_Orari.Tipo(h, g) = "A" Then
                                        Orario_Timbratura.Giustificativo = Vettore_Orari.Giustificativo(h, g)
                                    Else
                                        If cg.CampoGiustificativi(ConnectionString, Vettore_Orari.Giustificativo(h, g), "LavoroOrdinario") = "N" Then Orario_Timbratura.Qualita = Orario_Timbratura.Qualita & "Straordinario"
                                        Orario_Timbratura.Giustificativo = cg.CampoGiustificativi(ConnectionString, Vettore_Orari.Giustificativo(h, g), "GiustificativoPerAssenza")
                                        '                  If ControlloOrario = "P" Then Sw_Ok = False
                                    End If
                                Else
                                    Orario_Timbratura.Giustificativo = cpt.GiustificativoAssenza
                                End If
                                Orario_Timbratura.NelGruppo = Vettore_Orari.NelGruppo(h, g)
                                Orario_Timbratura.Familiare = Vettore_Orari.Familiare(h, g)
                                Orario_Timbratura.TipoServizio = Vettore_Orari.TipoServizio(h, g)
                                Orario_Timbratura.GiornoSuccessivo = Vettore_Orari.GiornoSuccessivo(h, g)
                                If Vettore_Orari.Tipo(h, g) = "P" Or (Vettore_Orari.Tipo(h, g) = "A" And cg.CampoGiustificativi(ConnectionString, Vettore_Orari.Giustificativo(h, g), "RegoleGenerali") = "S") Then
                                    '                If ControlloOrario = "S" Or Sw_Ok = True Then
                                    Risultato = AggiungiTurniTimbrature(Dipendente, g, x, XR, Orario_Timbratura, Vettore_Orari_Timbrature)
                                    AbbinaAssenzeOrariFlex = AbbinaAssenzeOrariFlex & Risultato
                                    '                End If
                                Else
                                    Orario_Timbratura.Pulisci()
                                End If
                                Sw_Fine = True
                            End If
                        End If
                    Else
                        ' Orari.Dalle > Assenza.Dalle
                        If Vettore_Orari.DalleBasso(h, g) < Assenza.Alle Then
                            If Assenza.Dalle < Vettore_Orari.DalleAlto(h, g) Then
                                Orario_Timbratura.Dalle = Vettore_Orari.DalleBasso(h, g)
                                Orario_Timbratura.Causale = ""
                            Else
                                Orario_Timbratura.Dalle = Assenza.Dalle
                                Orario_Timbratura.Causale = Assenza.Dalle_Causale
                            End If
                            Orario_Timbratura.Colore = vbWhite
                            If Vettore_Orari.AlleAlto(h, g) <= Assenza.Alle Then
                                '              Sw_Ok = True
                                If Assenza.Alle > Vettore_Orari.AlleBasso(h, g) Then
                                    Orario_Timbratura.Alle = Vettore_Orari.AlleBasso(h, g)
                                Else
                                    Orario_Timbratura.Alle = Assenza.Alle
                                    If Orario_Timbratura.Causale = "" Then Orario_Timbratura.Causale = Assenza.Alle_Causale
                                End If
                                Orario_Timbratura.Tipo = "Assenza"
                                Orario_Timbratura.Qualita = ""
                                If Vettore_Orari.Giustificativo(h, g) <> "" Then
                                    If Vettore_Orari.Tipo(h, g) = "A" Then
                                        Orario_Timbratura.Giustificativo = Vettore_Orari.Giustificativo(h, g)
                                    Else
                                        If cg.CampoGiustificativi(ConnectionString, Vettore_Orari.Giustificativo(h, g), "LavoroOrdinario") = "N" Then Orario_Timbratura.Qualita = Orario_Timbratura.Qualita & "Straordinario"
                                        Orario_Timbratura.Giustificativo = cg.CampoGiustificativi(ConnectionString, Vettore_Orari.Giustificativo(h, g), "GiustificativoPerAssenza")
                                        '                  If ControlloOrario = "P" Then Sw_Ok = False
                                    End If
                                Else
                                    Orario_Timbratura.Giustificativo = cpt.GiustificativoAssenza
                                End If
                                Orario_Timbratura.NelGruppo = Vettore_Orari.NelGruppo(h, g)
                                Orario_Timbratura.Familiare = Vettore_Orari.Familiare(h, g)
                                Orario_Timbratura.TipoServizio = Vettore_Orari.TipoServizio(h, g)
                                Orario_Timbratura.GiornoSuccessivo = Vettore_Orari.GiornoSuccessivo(h, g)
                                If Vettore_Orari.Tipo(h, g) = "P" Or (Vettore_Orari.Tipo(h, g) = "A" And cg.CampoGiustificativi(ConnectionString, Vettore_Orari.Giustificativo(h, g), "RegoleGenerali") = "S") Then
                                    '                If ControlloOrario = "S" Or Sw_Ok = True Then
                                    Risultato = AggiungiTurniTimbrature(Dipendente, g, x, XR, Orario_Timbratura, Vettore_Orari_Timbrature)
                                    AbbinaAssenzeOrariFlex = AbbinaAssenzeOrariFlex & Risultato
                                    '                End If
                                Else
                                    Orario_Timbratura.Pulisci()
                                End If
                            Else
                                Orario_Timbratura.Alle = Assenza.Alle
                                If Orario_Timbratura.Causale = "" Then Orario_Timbratura.Causale = Assenza.Alle_Causale
                                If Orario_Timbratura.Colore = vbWhite Then Orario_Timbratura.Colore = Assenza.Alle_Colore
                                Orario_Timbratura.Tipo = "EntrataPosticipata"
                                Orario_Timbratura.Qualita = ""
                                Data = DateSerial(Anno, Mese, g)
                                If Weekday(Data) = FestivitaSettimanale Or cxst.GiornoFestivo(Data, CodiceSuperGruppo) = True Then Orario_Timbratura.Qualita = Orario_Timbratura.Qualita & "Festivo"
                                If Vettore_Orari.Giustificativo(h, g) <> "" Then
                                    If Vettore_Orari.Tipo(h, g) = "A" Then
                                        Orario_Timbratura.Giustificativo = Vettore_Orari.Giustificativo(h, g)
                                    Else
                                        If cg.CampoGiustificativi(ConnectionString, Vettore_Orari.Giustificativo(h, g), "LavoroOrdinario") = "N" Then Orario_Timbratura.Qualita = Orario_Timbratura.Qualita & "Straordinario"
                                        Orario_Timbratura.Giustificativo = cpt.GiustificativoEntrataPosticipata
                                    End If
                                Else
                                    Orario_Timbratura.Giustificativo = cpt.GiustificativoEntrataPosticipata
                                End If
                                Orario_Timbratura.NelGruppo = Vettore_Orari.NelGruppo(h, g)
                                Orario_Timbratura.Familiare = Vettore_Orari.Familiare(h, g)
                                Orario_Timbratura.TipoServizio = Vettore_Orari.TipoServizio(h, g)
                                Orario_Timbratura.GiornoSuccessivo = Vettore_Orari.GiornoSuccessivo(h, g)
                                If Vettore_Orari.Tipo(h, g) = "P" Or (Vettore_Orari.Tipo(h, g) = "A" And cg.CampoGiustificativi(ConnectionString, Vettore_Orari.Giustificativo(h, g), "RegoleGenerali") = "S") Then
                                    '                If ControlloOrario = "S" Then
                                    Risultato = AggiungiTurniTimbrature(Dipendente, g, x, XR, Orario_Timbratura, Vettore_Orari_Timbrature)
                                    AbbinaAssenzeOrariFlex = AbbinaAssenzeOrariFlex & Risultato
                                    '                End If
                                Else
                                    Orario_Timbratura.Pulisci()
                                End If
                                If Vettore_Orari.DalleObbligo(h, g) <> "00.00" And Vettore_Orari.DalleObbligo(h, g) < Timbratura.Dalle Then
                                    If cg.CampoGiustificativi(ConnectionString, Vettore_Orari.Giustificativo(h, g), "ControlloFasciaObbligatoria") = "S" Then
                                        AbbinaAssenzeOrariFlex = AbbinaAssenzeOrariFlex & "(W) Il giorno : " & g & " Entrata " & Timbratura.Dalle & " fuori della fascia obbligatoria" & vbNewLine
                                    End If
                                End If
                                Sw_Fine = True
                            End If
                        End If
                    End If
                End If
            End If
            h = h + 1
        Loop
    End Function

    Private Function AbbinaAssenzeOrari(ByVal Dipendente As Long, ByVal Anno As Integer, ByVal Mese As Byte, ByRef g As Object, ByRef x As Object, ByRef XR As Object, ByVal CodiceSuperGruppo As String, ByRef Vettore_Orari As Cls_Struttura_Orari, ByRef Vettore_Orari_Timbrature As Cls_Struttura_Orari_Timbrature, ByRef Assenza As Cls_Struttura_Timbratura, ByRef Timbratura As Cls_Struttura_Timbratura, ByVal ControlloOrario As String) As String
        Dim cxst As New Cls_ClsXScriptTurni
        cxst.ConnectionString = ConnectionString
        cxst.Utente = Utente

        Dim Orario_Timbratura As New Cls_Struttura_Orario_Timbratura
        Dim cg As New Cls_Giustificativi
        Dim cpt As New Cls_ParametriTurni
        cpt.Leggi(ConnectionString)

        Orario_Timbratura.Pulisci()

        '  For h = 0 To 14
        '    If Vettore_Orari_Timbrature.Dalle(h, g) = "" And Vettore_Orari_Timbrature.Alle(h, g) = "" And Vettore_Orari_Timbrature.Giustificativo(h, g) = "" Then Exit For
        '    If Vettore_Orari_Timbrature.Dalle(h, g) = Assenza.Alle Then
        '      If cg.CampoGiustificativi(ConnectionString, Vettore_Orari_Timbrature.Giustificativo(h, g), "LavoroOrdinario") = "N" Then
        '        Assenza.Alle = Vettore_Orari_Timbrature.Alle(h, g)
        '        Exit For
        '      End If
        '    End If
        '  Next h

        Dim Data As Date = DateSerial(Anno, Mese, g)

        Dim h As Byte = 0
        Dim Sw_Fine As Boolean = False

        Do Until h > 9
            If Sw_Fine = True Then Exit Do
            If Vettore_Orari.Dalle(h, g) = "00.00" And Vettore_Orari.Alle(h, g) = "00.00" And Vettore_Orari.Giustificativo(h, g) = "" Then Exit Do
            '    If Vettore_Orari.Tipo(h, g) <> "I" And cg.CampoGiustificativi(ConnectionString, Vettore_Orari.Giustificativo(h, g), "LavoroOrdinario") <> "N" Then
            '    If Vettore_Orari.Tipo(h, g) = "P" Or (Vettore_Orari.Tipo(h, g) = "A" And cg.CampoGiustificativi(ConnectionString, Vettore_Orari.Giustificativo(h, g), "LavoroOrdinario") = "N") Then
            If Vettore_Orari.Tipo(h, g) <> "I" Then
                If Vettore_Orari.Dalle(h, g) = "00.00" And Vettore_Orari.Alle(h, g) = "24.00" And Vettore_Orari.Tipo(h, g) = "A" Then
                    Vettore_Orari.Alle(h, g) = "00.00"
                    Orario_Timbratura.Dalle = "00.00"
                    If cg.CampoGiustificativi(ConnectionString, Vettore_Orari.Giustificativo(h, g), "AssenzaAlleZero") = "S" Then
                        Orario_Timbratura.Alle = "00.00"
                    Else
                        Orario_Timbratura.Alle = "24.00"
                    End If
                    Orario_Timbratura.Tipo = "Assenza"
                    Orario_Timbratura.Qualita = ""
                    Orario_Timbratura.Giustificativo = Vettore_Orari.Giustificativo(h, g)
                    Orario_Timbratura.NelGruppo = Vettore_Orari.NelGruppo(h, g)
                    Orario_Timbratura.Familiare = Vettore_Orari.Familiare(h, g)
                    Orario_Timbratura.TipoServizio = Vettore_Orari.TipoServizio(h, g)
                    Orario_Timbratura.GiornoSuccessivo = Vettore_Orari.GiornoSuccessivo(h, g)
                    If cg.CampoGiustificativi(ConnectionString, Vettore_Orari.Giustificativo(h, g), "RegoleGenerali") = "S" Then
                        Risultato = AggiungiTurniTimbrature(Dipendente, g, x, XR, Orario_Timbratura, Vettore_Orari_Timbrature)
                        AbbinaAssenzeOrari = AbbinaAssenzeOrari & Risultato
                    Else
                        Orario_Timbratura.Pulisci()
                    End If
                Else
                    ' Orari.Dalle <= Assenze.Dalle
                    If Vettore_Orari.Dalle(h, g) <= Assenza.Dalle Then
                        If Vettore_Orari.Alle(h, g) > Assenza.Dalle Then
                            Orario_Timbratura.Dalle = Assenza.Dalle
                            Orario_Timbratura.Causale = Assenza.Dalle_Causale
                            Orario_Timbratura.Colore = Assenza.Dalle_Colore
                            If Vettore_Orari.Alle(h, g) <= Assenza.Alle Then
                                Orario_Timbratura.Alle = Vettore_Orari.Alle(h, g)
                                Orario_Timbratura.Tipo = "UscitaAnticipata"
                                Orario_Timbratura.Qualita = ""
                                If Timbratura.Dalle = "" And h = 0 Then Data = DateAdd("d", -1, Data)
                                If Weekday(Data) = FestivitaSettimanale Or cxst.GiornoFestivo(Data, CodiceSuperGruppo) = True Then Orario_Timbratura.Qualita = Orario_Timbratura.Qualita & "Festivo"
                                If Vettore_Orari.Giustificativo(h, g) <> "" Then
                                    If Vettore_Orari.Tipo(h, g) = "A" Then
                                        Orario_Timbratura.Giustificativo = Vettore_Orari.Giustificativo(h, g)
                                    Else
                                        If cg.CampoGiustificativi(ConnectionString, Vettore_Orari.Giustificativo(h, g), "LavoroOrdinario") = "N" Then Orario_Timbratura.Qualita = Orario_Timbratura.Qualita & "Straordinario"
                                        Orario_Timbratura.Giustificativo = cpt.GiustificativoUscitaAnticipata
                                    End If
                                Else
                                    Orario_Timbratura.Giustificativo = cpt.GiustificativoUscitaAnticipata
                                End If
                                Orario_Timbratura.NelGruppo = Vettore_Orari.NelGruppo(h, g)
                                Orario_Timbratura.Familiare = Vettore_Orari.Familiare(h, g)
                                Orario_Timbratura.TipoServizio = Vettore_Orari.TipoServizio(h, g)
                                Orario_Timbratura.GiornoSuccessivo = Vettore_Orari.GiornoSuccessivo(h, g)
                                If Vettore_Orari.Tipo(h, g) = "P" Or (Vettore_Orari.Tipo(h, g) = "A" And cg.CampoGiustificativi(ConnectionString, Vettore_Orari.Giustificativo(h, g), "RegoleGenerali") = "S") Then
                                    '                If ControlloOrario = "S" Then
                                    Risultato = AggiungiTurniTimbrature(Dipendente, g, x, XR, Orario_Timbratura, Vettore_Orari_Timbrature)
                                    AbbinaAssenzeOrari = AbbinaAssenzeOrari & Risultato
                                    '                End If
                                Else
                                    Orario_Timbratura.Pulisci()
                                End If
                                If Vettore_Orari.AlleObbligo(h, g) <> "00.00" And Vettore_Orari.AlleObbligo(h, g) > Timbratura.Alle Then
                                    If cg.CampoGiustificativi(ConnectionString, Vettore_Orari.Giustificativo(h, g), "ControlloFasciaObbligatoria") = "S" Then
                                        AbbinaAssenzeOrari = AbbinaAssenzeOrari & "(W) Il giorno : " & g & " Uscita " & Timbratura.Alle & " fuori della fascia obbligatoria" & vbNewLine
                                    End If
                                End If
                            Else
                                ' Sw_Ok = True
                                Orario_Timbratura.Alle = Assenza.Alle
                                If Orario_Timbratura.Causale = "" Then Orario_Timbratura.Causale = Assenza.Alle_Causale
                                If Orario_Timbratura.Colore = vbWhite Then Orario_Timbratura.Colore = Assenza.Alle_Colore
                                Orario_Timbratura.Tipo = "Assenza"
                                Orario_Timbratura.Qualita = ""
                                If Vettore_Orari.Giustificativo(h, g) <> "" Then
                                    If Vettore_Orari.Tipo(h, g) = "A" Then
                                        Orario_Timbratura.Giustificativo = Vettore_Orari.Giustificativo(h, g)
                                    Else
                                        If cg.CampoGiustificativi(ConnectionString, Vettore_Orari.Giustificativo(h, g), "LavoroOrdinario") = "N" Then Orario_Timbratura.Qualita = Orario_Timbratura.Qualita & "Straordinario"
                                        Orario_Timbratura.Giustificativo = cg.CampoGiustificativi(ConnectionString, Vettore_Orari.Giustificativo(h, g), "GiustificativoPerAssenza")
                                        ' If ControlloOrario = "P" Then Sw_Ok = False
                                    End If
                                Else
                                    Orario_Timbratura.Giustificativo = cpt.GiustificativoAssenza
                                End If
                                Orario_Timbratura.NelGruppo = Vettore_Orari.NelGruppo(h, g)
                                Orario_Timbratura.Familiare = Vettore_Orari.Familiare(h, g)
                                Orario_Timbratura.TipoServizio = Vettore_Orari.TipoServizio(h, g)
                                Orario_Timbratura.GiornoSuccessivo = Vettore_Orari.GiornoSuccessivo(h, g)
                                If Vettore_Orari.Tipo(h, g) = "P" Or (Vettore_Orari.Tipo(h, g) = "A" And cg.CampoGiustificativi(ConnectionString, Vettore_Orari.Giustificativo(h, g), "RegoleGenerali") = "S") Then
                                    '                If ControlloOrario = "S" Or Sw_Ok = True Then
                                    Risultato = AggiungiTurniTimbrature(Dipendente, g, x, XR, Orario_Timbratura, Vettore_Orari_Timbrature)
                                    AbbinaAssenzeOrari = AbbinaAssenzeOrari & Risultato
                                    '                End If
                                Else
                                    Orario_Timbratura.Pulisci()
                                End If
                                Sw_Fine = True
                            End If
                        End If
                    Else
                        ' Orari.Dalle > Assenze.Dalle
                        If Vettore_Orari.Dalle(h, g) < Assenza.Alle Then
                            Orario_Timbratura.Dalle = Vettore_Orari.Dalle(h, g)
                            Orario_Timbratura.Causale = ""
                            Orario_Timbratura.Colore = vbWhite
                            If Vettore_Orari.Alle(h, g) <= Assenza.Alle Then
                                ' Sw_Ok = True
                                Orario_Timbratura.Alle = Vettore_Orari.Alle(h, g)
                                Orario_Timbratura.Pausa = Vettore_Orari.Pausa(h, g)
                                Orario_Timbratura.Tipo = "Assenza"
                                Orario_Timbratura.Qualita = ""
                                If Vettore_Orari.Giustificativo(h, g) <> "" Then
                                    If Vettore_Orari.Tipo(h, g) = "A" Then
                                        Orario_Timbratura.Giustificativo = Vettore_Orari.Giustificativo(h, g)
                                    Else
                                        If cg.CampoGiustificativi(ConnectionString, Vettore_Orari.Giustificativo(h, g), "LavoroOrdinario") = "N" Then Orario_Timbratura.Qualita = Orario_Timbratura.Qualita & "Straordinario"
                                        Orario_Timbratura.Giustificativo = cg.CampoGiustificativi(ConnectionString, Vettore_Orari.Giustificativo(h, g), "GiustificativoPerAssenza")
                                        ' If ControlloOrario = "P" Then Sw_Ok = False
                                    End If
                                Else
                                    Orario_Timbratura.Giustificativo = cpt.GiustificativoAssenza
                                End If
                                Orario_Timbratura.NelGruppo = Vettore_Orari.NelGruppo(h, g)
                                Orario_Timbratura.Familiare = Vettore_Orari.Familiare(h, g)
                                Orario_Timbratura.TipoServizio = Vettore_Orari.TipoServizio(h, g)
                                Orario_Timbratura.GiornoSuccessivo = Vettore_Orari.GiornoSuccessivo(h, g)
                                If Vettore_Orari.Tipo(h, g) = "P" Or (Vettore_Orari.Tipo(h, g) = "A" And cg.CampoGiustificativi(ConnectionString, Vettore_Orari.Giustificativo(h, g), "RegoleGenerali") = "S") Then
                                    '                If ControlloOrario = "S" Or Sw_Ok = True Then
                                    Risultato = AggiungiTurniTimbrature(Dipendente, g, x, XR, Orario_Timbratura, Vettore_Orari_Timbrature)
                                    AbbinaAssenzeOrari = AbbinaAssenzeOrari & Risultato
                                    '                End If
                                Else
                                    Orario_Timbratura.Pulisci()
                                End If
                            Else
                                Orario_Timbratura.Alle = Assenza.Alle
                                If Orario_Timbratura.Causale = "" Then Orario_Timbratura.Causale = Assenza.Alle_Causale
                                If Orario_Timbratura.Colore = vbWhite Then Orario_Timbratura.Colore = Assenza.Alle_Colore
                                Orario_Timbratura.Tipo = "EntrataPosticipata"
                                Orario_Timbratura.Qualita = ""
                                If Weekday(Data) = FestivitaSettimanale Or cxst.GiornoFestivo(Data, CodiceSuperGruppo) = True Then Orario_Timbratura.Qualita = Orario_Timbratura.Qualita & "Festivo"
                                If Vettore_Orari.Giustificativo(h, g) <> "" Then
                                    If Vettore_Orari.Tipo(h, g) = "A" Then
                                        '                  Sw_Ok = True
                                        Orario_Timbratura.Giustificativo = Vettore_Orari.Giustificativo(h, g)
                                        Orario_Timbratura.Tipo = "Assenza"
                                    Else
                                        If cg.CampoGiustificativi(ConnectionString, Vettore_Orari.Giustificativo(h, g), "LavoroOrdinario") = "N" Then Orario_Timbratura.Qualita = Orario_Timbratura.Qualita & "Straordinario"
                                        Orario_Timbratura.Giustificativo = cpt.GiustificativoEntrataPosticipata
                                    End If
                                Else
                                    Orario_Timbratura.Giustificativo = cpt.GiustificativoEntrataPosticipata
                                End If
                                Orario_Timbratura.NelGruppo = Vettore_Orari.NelGruppo(h, g)
                                Orario_Timbratura.Familiare = Vettore_Orari.Familiare(h, g)
                                Orario_Timbratura.TipoServizio = Vettore_Orari.TipoServizio(h, g)
                                Orario_Timbratura.GiornoSuccessivo = Vettore_Orari.GiornoSuccessivo(h, g)
                                If Vettore_Orari.Tipo(h, g) = "P" Or (Vettore_Orari.Tipo(h, g) = "A" And cg.CampoGiustificativi(ConnectionString, Vettore_Orari.Giustificativo(h, g), "RegoleGenerali") = "S") Then
                                    '                If ControlloOrario = "S" Or Sw_Ok = True Then
                                    Risultato = AggiungiTurniTimbrature(Dipendente, g, x, XR, Orario_Timbratura, Vettore_Orari_Timbrature)
                                    AbbinaAssenzeOrari = AbbinaAssenzeOrari & Risultato
                                    '                End If
                                Else
                                    Orario_Timbratura.Pulisci()
                                End If
                                If Vettore_Orari.DalleObbligo(h, g) <> "00.00" And Vettore_Orari.DalleObbligo(h, g) < Timbratura.Dalle Then
                                    If cg.CampoGiustificativi(ConnectionString, Vettore_Orari.Giustificativo(h, g), "ControlloFasciaObbligatoria") = "S" Then
                                        AbbinaAssenzeOrari = AbbinaAssenzeOrari & "(W) Il giorno : " & g & " Entrata " & Timbratura.Dalle & " fuori della fascia obbligatoria" & vbNewLine
                                    End If
                                End If
                                Sw_Fine = True
                            End If
                        End If
                    End If
                End If
            End If
            h = h + 1
        Loop
    End Function

    Private Function CaricaOrariTipoAI(ByVal Dipendente As Long, ByVal g As Object, ByRef x As Object, ByRef XR As Object, ByVal Tipo As Object, ByVal RegoleGenerali As Object, ByRef Vettore_Orari As Cls_Struttura_Orari, ByRef Vettore_Orari_Timbrature As Cls_Struttura_Orari_Timbrature) As String
        Dim Orario_Timbratura As New Cls_Struttura_Orario_Timbratura
        Dim cg As New Cls_Giustificativi

        Dim h As Byte = 0
        Do Until h > 9
            If Vettore_Orari.Dalle(h, g) = "00.00" And Vettore_Orari.Alle(h, g) = "00.00" And Vettore_Orari.Giustificativo(h, g) = "" Then Exit Do
            ' If Vettore_Orari.Dalle(h, g) = "00.00" And Vettore_Orari.Alle(h, g) = "00.00" Then Vettore_Orari.Alle(h, g) = "24.00"
            If Vettore_Orari.Tipo(h, g) = Tipo Then
                Orario_Timbratura.Dalle = Vettore_Orari.Dalle(h, g)
                Orario_Timbratura.Alle = Vettore_Orari.Alle(h, g)
                Orario_Timbratura.Pausa = Vettore_Orari.Pausa(h, g)
                Orario_Timbratura.Causale = ""
                Orario_Timbratura.Colore = vbWhite
                Orario_Timbratura.Giustificativo = Vettore_Orari.Giustificativo(h, g)
                Orario_Timbratura.NelGruppo = Vettore_Orari.NelGruppo(h, g)
                Orario_Timbratura.Familiare = Vettore_Orari.Familiare(h, g)
                Orario_Timbratura.Qualita = ""
                Orario_Timbratura.TipoServizio = Vettore_Orari.TipoServizio(h, g)
                Orario_Timbratura.GiornoSuccessivo = Vettore_Orari.GiornoSuccessivo(h, g)
                If Tipo = "I" Then
                    Orario_Timbratura.Tipo = "Informazione"
                    Risultato = AggiungiTurniTimbrature(Dipendente, g, x, XR, Orario_Timbratura, Vettore_Orari_Timbrature)
                Else
                    Orario_Timbratura.Tipo = "Assenza"
                    If RegoleGenerali <> "" Then
                        If cg.CampoGiustificativi(ConnectionString, Vettore_Orari.Giustificativo(h, g), "RegoleGenerali") = RegoleGenerali Then
                            Risultato = AggiungiTurniTimbrature(Dipendente, g, x, XR, Orario_Timbratura, Vettore_Orari_Timbrature)
                        End If
                    Else
                        Risultato = AggiungiTurniTimbrature(Dipendente, g, x, XR, Orario_Timbratura, Vettore_Orari_Timbrature)
                    End If
                End If
            End If
            h = h + 1
        Loop
    End Function

    Public Sub Esegui_OperazioneTempo_Pausa()
        Dim cxst As New Cls_ClsXScriptTurni
        cxst.Utente = Utente
        cxst.ConnectionString = ConnectionString

        If OperazionePausa = "Somma" Then
            Pausa_NelGiorno = Format(cxst.Mod_OperaConMinuti(Pausa_NelGiorno, "+", Ore_Pausa), "00.00")
        End If
        If OperazioneTempo = "Somma" Then
            Lavorate_NelGiorno = Format(cxst.Mod_OperaConMinuti(Lavorate_NelGiorno, "+", Ore_Intervallo), "00.00")
            Lavorate_Diurne = Format(cxst.Mod_OperaConMinuti(Lavorate_Diurne, "+", Ore_Diurne), "00.00")
            Lavorate_Notturne = Format(cxst.Mod_OperaConMinuti(Lavorate_Notturne, "+", Ore_Notturne), "00.00")
            Lavorate_FestiveDiurne = Format(cxst.Mod_OperaConMinuti(Lavorate_FestiveDiurne, "+", Ore_FestiveDiurne), "00.00")
            Lavorate_FestiveNotturne = Format(cxst.Mod_OperaConMinuti(Lavorate_FestiveNotturne, "+", Ore_FestiveNotturne), "00.00")
            Lavorate_Retribuite = Format(cxst.Mod_OperaConMinuti(Lavorate_Retribuite, "+", Ore_Intervallo), "00.00")
        End If
        If OperazionePausa = "Sottrai" Then
            Pausa_NelGiorno = Format(cxst.Mod_OperaConMinuti(Pausa_NelGiorno, "-", Ore_Pausa), "00.00")
        End If
        If OperazioneTempo = "Sottrai" Then
            Lavorate_NelGiorno = Format(cxst.Mod_OperaConMinuti(Lavorate_NelGiorno, "-", Ore_Intervallo), "00.00")
            Lavorate_Diurne = Format(cxst.Mod_OperaConMinuti(Lavorate_Diurne, "-", Ore_Diurne), "00.00")
            Lavorate_Notturne = Format(cxst.Mod_OperaConMinuti(Lavorate_Notturne, "-", Ore_Notturne), "00.00")
            Lavorate_FestiveDiurne = Format(cxst.Mod_OperaConMinuti(Lavorate_FestiveDiurne, "-", Ore_FestiveDiurne), "00.00")
            Lavorate_FestiveNotturne = Format(cxst.Mod_OperaConMinuti(Lavorate_FestiveNotturne, "-", Ore_FestiveNotturne), "00.00")
            Lavorate_Retribuite = Format(cxst.Mod_OperaConMinuti(Lavorate_Retribuite, "-", Ore_Intervallo), "00.00")
        End If
        If OperazioneTempo = "Nullo" Then
            If OperazioneOreLavorate = "Somma" Then
                Lavorate_NelGiorno = Format(cxst.Mod_OperaConMinuti(Lavorate_NelGiorno, "+", Ore_Intervallo), "00.00")
            End If
            If OperazioneOreFeriali = "Somma" Then
                Lavorate_Diurne = Format(cxst.Mod_OperaConMinuti(Lavorate_Diurne, "+", Ore_Diurne), "00.00")
                Lavorate_Notturne = Format(cxst.Mod_OperaConMinuti(Lavorate_Notturne, "+", Ore_Notturne), "00.00")
            End If
            If OperazioneOreFestive = "Somma" Then
                Lavorate_FestiveDiurne = Format(cxst.Mod_OperaConMinuti(Lavorate_FestiveDiurne, "+", Ore_FestiveDiurne), "00.00")
                Lavorate_FestiveNotturne = Format(cxst.Mod_OperaConMinuti(Lavorate_FestiveNotturne, "+", Ore_FestiveNotturne), "00.00")
            End If
            If OperazioneOreLavorate = "Sottrai" Then
                Lavorate_NelGiorno = Format(cxst.Mod_OperaConMinuti(Lavorate_NelGiorno, "-", Ore_Intervallo), "00.00")
            End If
            If OperazioneOreFeriali = "Sottrai" Then
                Lavorate_Diurne = Format(cxst.Mod_OperaConMinuti(Lavorate_Diurne, "-", Ore_Diurne), "00.00")
                Lavorate_Notturne = Format(cxst.Mod_OperaConMinuti(Lavorate_Notturne, "-", Ore_Notturne), "00.00")
            End If
            If OperazioneOreFestive = "Sottrai" Then
                Lavorate_FestiveDiurne = Format(cxst.Mod_OperaConMinuti(Lavorate_FestiveDiurne, "-", Ore_FestiveDiurne), "00.00")
                Lavorate_FestiveNotturne = Format(cxst.Mod_OperaConMinuti(Lavorate_FestiveNotturne, "-", Ore_FestiveNotturne), "00.00")
            End If
        End If
        If OperazioneOreRetribuite = "Somma" Then
            Lavorate_Retribuite = Format(cxst.Mod_OperaConMinuti(Lavorate_Retribuite, "+", Ore_Intervallo), "00.00")
        End If
        If OperazioneOreRetribuite = "Sottrai" Then
            Lavorate_Retribuite = Format(cxst.Mod_OperaConMinuti(Lavorate_Retribuite, "-", Ore_Intervallo), "00.00")
        End If
    End Sub

    Private Function DalleZeroZero(ByVal Dipendente As Long, ByVal Anno As Integer, ByVal Mese As Byte, ByVal h As Long, ByVal g As Long, ByRef Vettore_Orari As Cls_Struttura_Orari) As Boolean
        Dim ceo As New Cls_ElaboraOrari
        Dim Vettore_Orari_Giorno As New Cls_Struttura_Orari_Giorno
        Dim Vettore_Timbrature_Giorno As New Cls_Struttura_Timbrature_Giorno

        DalleZeroZero = True

        If h <> 0 Then Exit Function
        '
        '   Azzera il vettore delle timbrature del giorno
        '
        For i = 0 To 9
            Vettore_Timbrature_Giorno.Dalle(i) = ""
            Vettore_Timbrature_Giorno.Dalle_Causale(i) = ""
            Vettore_Timbrature_Giorno.Dalle_Colore(i) = vbWhite
            Vettore_Timbrature_Giorno.Alle(i) = ""
            Vettore_Timbrature_Giorno.Alle_Causale(i) = ""
            Vettore_Timbrature_Giorno.Alle_Colore(i) = vbWhite
        Next i
        '
        If g = 0 Then
            If ceo.SviluppoTurniOrdiniServizioSingoloGiorno(ConnectionString, Dipendente, DateSerial(Anno, Mese, 0), "N", Vettore_Orari_Giorno, Vettore_Timbrature_Giorno, "S", "N") = "" Then
                For h = 0 To 14
                    If Vettore_Orari_Giorno.Dalle(h) = Vettore_Orari_Giorno.Alle(h) Then Exit For
                    If Vettore_Orari_Giorno.Alle(h) = "00.00" Then
                        DalleZeroZero = False
                        Exit For
                    End If
                Next h
            End If
            Exit Function
        End If

        g = g - 1
        For h = 0 To 9
            If Vettore_Orari.Dalle(h, g) = Vettore_Orari.Alle(h, g) Then Exit For
            If Vettore_Orari.Dalle(h, g) <> "00.00" And Vettore_Orari.Alle(h, g) = "24.00" And Vettore_Orari.Tipo(h, g) <> "I" Then
                DalleZeroZero = False
                Exit For
            End If
        Next h
    End Function

    Private Function ControlloIntervalloDiTempo(ByVal Dipendente As Long, ByRef Vettore_Orari_Timbrature As Cls_Struttura_Orari_Timbrature, ByVal x As Byte, ByVal g As Byte) As String

        ControlloIntervalloDiTempo = ""

        Dim cg As New Cls_Giustificativi
        Dim cpt As New Cls_ParametriTurni
        cpt.Leggi(ConnectionString)

        ' Campi di ControlloIntervalloDiTempo

        X_Precedente = 0
        Intervallo_Precedente = 0
        Tipo_Precedente = ""

        If Vettore_Orari_Timbrature.Dalle(x, g) = "00.00" And Vettore_Orari_Timbrature.Alle(x, g) = "00.00" And Vettore_Orari_Timbrature.Tipo(x, g) = "Assenza" Then Exit Function

        If Vettore_Orari_Timbrature.Giustificativo(x, g) <> "" And (Vettore_Orari_Timbrature.Tipo(x, g) = "" Or Vettore_Orari_Timbrature.Tipo(x, g) = "Informazione") Then
            If Vettore_Orari_Timbrature.Tipo(x, g) = "Informazione" Or cg.CampoGiustificativi(ConnectionString, Vettore_Orari_Timbrature.Giustificativo(x, g), "RegoleGenerali") = "N" Then
                ControlloIntervalloDiTempoPrecedente(g, Vettore_Orari_Timbrature)
                Exit Function
            End If
        End If

        Dim Intervallo As Long = 0
        If Vettore_Orari_Timbrature.Dalle(x, g) = "" Then Vettore_Orari_Timbrature.Dalle(x, g) = "00.00"
        If Vettore_Orari_Timbrature.Alle(x, g) = "" Then Vettore_Orari_Timbrature.Alle(x, g) = "24.00"

        If x = 0 Then
            If Vettore_Orari_Timbrature.Dalle(x, g) = "00.00" Then
                If Vettore_Orari_Timbrature.Tipo(x, g) = "Assenza" Or Vettore_Orari_Timbrature.Tipo(x, g) = "EntrataPosticipata" Or Vettore_Orari_Timbrature.Tipo(x, g) = "UscitaAnticipata" Then
                    If Tipo_Precedente = "Assenza" Or Tipo_Precedente = "EntrataPosticipata" Or Tipo_Precedente = "UscitaAnticipata" Then
                        Intervallo = Intervallo_Precedente
                    Else
                        ControlloIntervalloDiTempoPrecedente(g, Vettore_Orari_Timbrature)
                    End If
                Else
                    If g = 0 Then
                        ControlloIntervalloDiTempo = "(W) Il giorno : 1" & " Controllare l'ultimo giorno del mese precedente" & vbNewLine
                    End If
                    If Tipo_Precedente <> "Assenza" And Tipo_Precedente <> "EntrataPosticipata" And Tipo_Precedente <> "UscitaAnticipata" Then
                        Intervallo = Intervallo_Precedente
                    Else
                        ControlloIntervalloDiTempoPrecedente(g, Vettore_Orari_Timbrature)
                    End If
                End If
            Else
                ControlloIntervalloDiTempoPrecedente(g, Vettore_Orari_Timbrature)
            End If
        End If

        If Vettore_Orari_Timbrature.Giustificativo(x, g) <> cpt.GiustificativoRiposo Then
            If Vettore_Orari_Timbrature.Alle(x, g) = "24.00" Then
                Intervallo = Intervallo + DateDiff("n", TimeSerial(Val(Mid(Vettore_Orari_Timbrature.Dalle(x, g), 1, 2)), Val(Mid(Vettore_Orari_Timbrature.Dalle(x, g), 4, 2)), 0), TimeSerial(23, 59, 0)) + 1
            Else
                Intervallo = Intervallo + DateDiff("n", TimeSerial(Val(Mid(Vettore_Orari_Timbrature.Dalle(x, g), 1, 2)), Val(Mid(Vettore_Orari_Timbrature.Dalle(x, g), 4, 2)), 0), TimeSerial(Val(Mid(Vettore_Orari_Timbrature.Alle(x, g), 1, 2)), Val(Mid(Vettore_Orari_Timbrature.Alle(x, g), 4, 2)), 0))
            End If
        End If

        If Vettore_Orari_Timbrature.Alle(x, g) = "24.00" Then
            Tipo_Precedente = Vettore_Orari_Timbrature.Tipo(x, g)
            Intervallo_Precedente = Intervallo
            X_Precedente = x
            Exit Function
        End If

        Dim W_TempoOrarioLavoro As Single = Math.Round(Hour(cpt.TempoOrarioLavoro) + (Minute(cpt.TempoOrarioLavoro) / 100), 2)
        If Vettore_Orari_Timbrature.Tipo(x, g) = "" And W_TempoOrarioLavoro <= Intervallo Then Exit Function
        Dim W_TempoAssenza As Single = Math.Round(Hour(cpt.TempoAssenza) + (Minute(cpt.TempoAssenza) / 100), 2)
        If Vettore_Orari_Timbrature.Tipo(x, g) = "Assenza" And W_TempoAssenza <= Intervallo Then Exit Function
        Dim W_TempoSostituzione As Single = Math.Round(Hour(cpt.TempoSostituzione) + (Minute(cpt.TempoSostituzione) / 100), 2)
        If Vettore_Orari_Timbrature.Tipo(x, g) = "Sostituzione" And W_TempoSostituzione <= Intervallo Then Exit Function
        If Vettore_Orari_Timbrature.Tipo(x, g) = "EntrataPosticipata" Then
            Dim W_TempoEntrataPosticipata As Single = Math.Round(Hour(cpt.TempoEntrataPosticipata) + (Minute(cpt.TempoEntrataPosticipata) / 100), 2)
            If W_TempoEntrataPosticipata > Intervallo Then
                Vettore_Orari_Timbrature.Giustificativo(x, g) = cpt.GiustificativoOrarioLavoro
                Vettore_Orari_Timbrature.Familiare(x, g) = 0
                Vettore_Orari_Timbrature.Tipo(x, g) = ""
                Vettore_Orari_Timbrature.Qualita(x, g) = ""
            End If
            Exit Function
        End If
        If Vettore_Orari_Timbrature.Tipo(x, g) = "UscitaAnticipata" Then
            Dim W_TempoUscitaAnticipata As Single = Math.Round(Hour(cpt.TempoUscitaAnticipata) + (Minute(cpt.TempoUscitaAnticipata) / 100), 2)
            If W_TempoUscitaAnticipata > Intervallo Then
                Vettore_Orari_Timbrature.Giustificativo(x, g) = cpt.GiustificativoOrarioLavoro
                Vettore_Orari_Timbrature.Familiare(x, g) = 0
                Vettore_Orari_Timbrature.Tipo(x, g) = ""
                Vettore_Orari_Timbrature.Qualita(x, g) = ""
            End If
            Exit Function
        End If
        Dim W_TempoEntrataAnticipata As Single = Math.Round(Hour(cpt.TempoEntrataAnticipata) + (Minute(cpt.TempoEntrataAnticipata) / 100), 2)
        If Vettore_Orari_Timbrature.Tipo(x, g) = "EntrataAnticipata" And W_TempoEntrataAnticipata <= Intervallo Then Exit Function
        Dim W_TempoUscitaPosticipata As Single = Math.Round(Hour(cpt.TempoUscitaPosticipata) + (Minute(cpt.TempoUscitaPosticipata) / 100), 2)
        If Vettore_Orari_Timbrature.Tipo(x, g) = "UscitaPosticipata" And W_TempoUscitaPosticipata <= Intervallo Then Exit Function

        Vettore_Orari_Timbrature.Dalle(x, g) = ""
        Vettore_Orari_Timbrature.Alle(x, g) = ""
        Vettore_Orari_Timbrature.Pausa(x, g) = ""
        Vettore_Orari_Timbrature.Causale(x, g) = ""
        Vettore_Orari_Timbrature.Colore(x, g) = vbWhite
        Vettore_Orari_Timbrature.Giustificativo(x, g) = ""
        Vettore_Orari_Timbrature.Familiare(x, g) = 0
        Vettore_Orari_Timbrature.Tipo(x, g) = ""
        Vettore_Orari_Timbrature.Qualita(x, g) = ""
        Vettore_Orari_Timbrature.TipoServizio(x, g) = ""
        Vettore_Orari_Timbrature.GiornoSuccessivo(x, g) = ""

        ControlloIntervalloDiTempo = "(E) Controllo intervallo di tempo errato il giorno : " & g & vbNewLine
    End Function

    Public Sub ControlloIntervalloDiTempoPrecedente(ByVal g As Byte, ByRef Vettore_Orari_Timbrature As Cls_Struttura_Orari_Timbrature)
        Dim cpt As New Cls_ParametriTurni
        cpt.Leggi(ConnectionString)

        If Intervallo_Precedente = 0 Then Exit Sub
        Dim W_Tmp As String
        Select Case Tipo_Precedente
            Case ""
                W_Tmp = Format(cpt.TempoOrarioLavoro, "HH.mm")
                If W_Tmp <= Intervallo_Precedente Then
                    Tipo_Precedente = ""
                    Intervallo_Precedente = 0
                    X_Precedente = 0
                    Exit Sub
                End If
            Case "Assenza"
                W_Tmp = Format(cpt.TempoAssenza, "HH.mm")
                If W_Tmp <= Intervallo_Precedente Then
                    Tipo_Precedente = ""
                    Intervallo_Precedente = 0
                    X_Precedente = 0
                    Exit Sub
                End If
            Case "Sostituzione"
                W_Tmp = Format(cpt.TempoSostituzione, "HH.mm")
                If W_Tmp <= Intervallo_Precedente Then
                    Tipo_Precedente = ""
                    Intervallo_Precedente = 0
                    X_Precedente = 0
                    Exit Sub
                End If
            Case "EntrataPosticipata"
                W_Tmp = Format(cpt.TempoEntrataPosticipata, "HH.mm")
                If W_Tmp <= Intervallo_Precedente Then
                    Tipo_Precedente = ""
                    Intervallo_Precedente = 0
                    X_Precedente = 0
                    Exit Sub
                End If
            Case "UscitaAnticipata"
                W_Tmp = Format(cpt.TempoUscitaAnticipata, "HH.mm")
                If W_Tmp <= Intervallo_Precedente Then
                    Tipo_Precedente = ""
                    Intervallo_Precedente = 0
                    X_Precedente = 0
                    Exit Sub
                End If
            Case "EntrataAnticipata"
                W_Tmp = Format(cpt.TempoEntrataAnticipata, "HH.mm")
                If W_Tmp <= Intervallo_Precedente Then
                    Tipo_Precedente = ""
                    Intervallo_Precedente = 0
                    X_Precedente = 0
                    Exit Sub
                End If
            Case "UscitaPosticipata"
                W_Tmp = Format(cpt.TempoUscitaPosticipata, "HH.mm")
                If W_Tmp <= Intervallo_Precedente Then
                    Tipo_Precedente = ""
                    Intervallo_Precedente = 0
                    X_Precedente = 0
                    Exit Sub
                End If
        End Select

        If g > 0 Then
            Vettore_Orari_Timbrature.Dalle(X_Precedente, g - 1) = ""
            Vettore_Orari_Timbrature.Alle(X_Precedente, g - 1) = ""
            Vettore_Orari_Timbrature.Pausa(X_Precedente, g - 1) = ""
            Vettore_Orari_Timbrature.Causale(X_Precedente, g - 1) = ""
            Vettore_Orari_Timbrature.Colore(X_Precedente, g - 1) = vbWhite
            Vettore_Orari_Timbrature.Giustificativo(X_Precedente, g - 1) = ""
            Vettore_Orari_Timbrature.NelGruppo(X_Precedente, g - 1) = ""
            Vettore_Orari_Timbrature.Familiare(X_Precedente, g - 1) = 0
            Vettore_Orari_Timbrature.Tipo(X_Precedente, g - 1) = ""
            Vettore_Orari_Timbrature.Qualita(X_Precedente, g - 1) = ""
            Vettore_Orari_Timbrature.TipoServizio(X_Precedente, g - 1) = ""
            Vettore_Orari_Timbrature.GiornoSuccessivo(X_Precedente, g - 1) = ""
        End If

        Tipo_Precedente = ""
        Intervallo_Precedente = 0
        X_Precedente = 0
    End Sub


    Public Sub Carica_VettoreTimbrature(ByVal CodiceDipendente As Double, ByVal Anno As Integer, ByVal Mese As Byte, ByVal Tipo As String, ByRef Vettore_Timbrature As Cls_Struttura_Timbrature)

        Dim DataMin As Date
        Dim DataMax As Date
        Dim W_g As Integer
        Dim g As Integer
        Dim c As Integer
        Dim UltimaEntrata As Integer

        Dim cxst As New Cls_ClsXScriptTurni

        '
        '   Azzera il vettore delle timbrature
        '
        For g = 0 To 31
            For i = 0 To 9
                Vettore_Timbrature.Dalle(i, g) = ""
                Vettore_Timbrature.Dalle_Causale(i, g) = ""
                Vettore_Timbrature.Dalle_Colore(i, g) = vbWhite
                Vettore_Timbrature.Alle(i, g) = ""
                Vettore_Timbrature.Alle_Causale(i, g) = ""
                Vettore_Timbrature.Alle_Colore(i, g) = vbWhite
            Next i
        Next g
        '
        '   Carica il vettore delle timbrature
        '
        DataMin = DateSerial(Anno, Mese, 0)
        DataMax = DateSerial(Anno, Mese, cxst.GiorniMese(Mese, Anno))
        W_g = -1

        Dim cmd As New OleDbCommand()
        Dim cn As OleDbConnection
        cn = New Data.OleDb.OleDbConnection(ConnectionString)
        cn.Open()

        If Tipo = "Originali" Then
            cmd.CommandText = "SELECT * FROM TimbratureOriginali" & _
                  " WHERE CodiceDipendente = ?" & _
                  " AND Data >= ?" & _
                  " AND Data <= ?" & _
                  " ORDER BY Data, Orario, EntrataUscita"
        Else
            cmd.CommandText = "SELECT TimbratureManipolate.*, (SELECT TOP 1 CodiceDipendente FROM TimbratureOriginali" & _
                  " WHERE TimbratureOriginali.CodiceDipendente = TimbratureManipolate.CodiceDipendente" & _
                  " AND TimbratureOriginali.Data = TimbratureManipolate.Data" & _
                  " AND TimbratureOriginali.Orario = TimbratureManipolate.Orario" & _
                  " AND TimbratureOriginali.EntrataUscita = TimbratureManipolate.EntrataUscita" & _
                  " AND TimbratureOriginali.Causale = TimbratureManipolate.Causale) AS Originale FROM TimbratureManipolate" & _
                  " WHERE CodiceDipendente = ?" & _
                  " AND Data >= ?" & _
                  " AND Data <= ?" & _
                  " ORDER BY Data, Orario, EntrataUscita"
        End If

        cmd.Parameters.AddWithValue("@CodiceDipendente", CodiceDipendente)
        cmd.Parameters.AddWithValue("@Data", DataMin)
        cmd.Parameters.AddWithValue("@Data", DataMax)
        cmd.Connection = cn

        Dim myPOSTreader As OleDbDataReader = cmd.ExecuteReader()

        Do While myPOSTreader.Read
            g = DateDiff("d", DataMin, DataDb(myPOSTreader.Item("Data")))
            c = 0
            If W_g <> g Then
                W_g = g
                UltimaEntrata = 0
            End If
            If StringaDb(myPOSTreader.Item("EntrataUscita")) = "E" Then
                UltimaEntrata = Format(DataDb(myPOSTreader.Item("Orario")), "HH.mm")
                Do Until Vettore_Timbrature.Dalle(c, g) = "" And Vettore_Timbrature.Alle(c, g) = ""
                    c = c + 1
                Loop
                Vettore_Timbrature.Dalle(c, g) = Format(DataDb(myPOSTreader.Item("Orario")), "HH.mm")
                If NumeroDb(myPOSTreader.Item("Causale")) <> 0 Then
                    Vettore_Timbrature.Dalle_Causale(c, g) = NumeroDb(myPOSTreader.Item("Causale"))
                End If
                If Tipo = "Manipolate" Then
                    If NumeroDb(myPOSTreader.Item("CodiceDipendente")) <> NumeroDb(myPOSTreader.Item("Originale")) Then
                        Vettore_Timbrature.Dalle_Colore(c, g) = vbYellow
                    End If
                End If
            Else
                Do Until Vettore_Timbrature.Alle(c, g) = "" And Vettore_Timbrature.Dalle(c + 1, g) = ""
                    c = c + 1
                Loop
                If UltimaEntrata = Format(DataDb(myPOSTreader.Item("Orario")), "HH.mm") Then
                    If c > 0 Then
                        If Vettore_Timbrature.Alle(c - 1, g) = "" Then
                            c = c - 1
                        End If
                    End If
                End If
                Vettore_Timbrature.Alle(c, g) = Format(DataDb(myPOSTreader.Item("Orario")), "HH.mm")
                If NumeroDb(myPOSTreader.Item("Causale")) <> 0 Then
                    Vettore_Timbrature.Alle_Causale(c, g) = NumeroDb(myPOSTreader.Item("Causale"))
                End If
                If Tipo = "Manipolate" Then
                    If NumeroDb(myPOSTreader.Item("CodiceDipendente")) <> NumeroDb(myPOSTreader.Item("Originale")) Then
                        Vettore_Timbrature.Alle_Colore(c, g) = vbYellow
                    End If
                End If
            End If
        Loop
        myPOSTreader.Close()
        cn.Close()
    End Sub

    Public Sub Carica_VettoreCalcoloOrari(ByVal CodiceDipendente As Double, ByVal Anno As Integer, ByVal Mese As Byte, ByRef Vettore_Calcolo_Orari As Cls_Struttura_Calcolo_Orari)
        Dim W_TempoStandard As Single
        Dim W_TempoMese As Single
        Dim i As Integer

        '
        '   Azzera il vettore calcolo orari
        '
        Vettore_Calcolo_Orari.Pulisci()
        '
        '   Carica il vettore calcolo orari
        '
        Dim cn As OleDbConnection
        cn = New Data.OleDb.OleDbConnection(ConnectionString)
        cn.Open()

        Dim cmd As New OleDbCommand()
        cmd.CommandText = "SELECT * FROM SalvaCalcoloOrari" & _
                          " WHERE Anno = ?" & _
                          " AND Mese = ?" & _
                          " AND CodiceDipendente = ?"
        cmd.Parameters.AddWithValue("@Anno", Anno)
        cmd.Parameters.AddWithValue("@Mese", Mese)
        cmd.Parameters.AddWithValue("@CodiceDipendente", CodiceDipendente)

        cmd.Connection = cn
        Dim myPOSTreader As OleDbDataReader = cmd.ExecuteReader()
        Do While myPOSTreader.Read
            W_TempoStandard = NumeroDb(myPOSTreader.Item("TempoStandard"))
            W_TempoMese = NumeroDb(myPOSTreader.Item("TempoMese"))
            i = NumeroDb(myPOSTreader.Item("Indice"))
            Vettore_Calcolo_Orari.Elemento(i, 0) = StringaDb(myPOSTreader.Item("Elemento_0")) & "/" & NumeroDb(myPOSTreader.Item("CodiceFamiliare"))
            For g = 1 To 31
                Vettore_Calcolo_Orari.Elemento(i, g) = StringaDb(myPOSTreader.Item("Elemento_" & g))
            Next g
        Loop
        myPOSTreader.Close()
        cn.Close()
        '
        '   Imposta le colonne > 40
        '
        Vettore_Calcolo_Orari.Elemento(41, 0) = "Ore Work_1"
        Vettore_Calcolo_Orari.Elemento(42, 0) = "Ore Work_2"
        Vettore_Calcolo_Orari.Elemento(43, 0) = "Ore Previste"
        Vettore_Calcolo_Orari.Elemento(44, 0) = "Ore Pausa"
        Vettore_Calcolo_Orari.Elemento(45, 0) = "Ore Lavorate"
        Vettore_Calcolo_Orari.Elemento(46, 0) = "Ore Diurne"
        Vettore_Calcolo_Orari.Elemento(47, 0) = "Ore Notturne"
        Vettore_Calcolo_Orari.Elemento(48, 0) = "Ore Festive Diurne"
        Vettore_Calcolo_Orari.Elemento(49, 0) = "Ore Festive Notturne"
        Vettore_Calcolo_Orari.Elemento(50, 0) = "Ore Retribuite"
    End Sub

    Public Sub Carica_VettoreGiustificativi(ByVal CodiceDipendente As Double, ByVal Anno As Integer, ByVal Mese As Byte, ByRef Vettore_Giustificativi As Cls_Struttura_Giustificativi)

        Dim DataMin As Date
        Dim DataMax As Date
        Dim OldData As Date
        Dim i As Integer
        Dim r As Integer
        '
        '   Azzera il vettore giustificativi
        '
        For g = 0 To 31
            For i = 0 To 2
                Vettore_Giustificativi.Orario(i, g) = "00.00"
                Vettore_Giustificativi.Giustificativo(i, g) = ""
            Next i
        Next g
        '
        '   Carica il vettore giustificativi
        '

        Dim cmd As New OleDbCommand()
        Dim cn As OleDbConnection
        cn = New Data.OleDb.OleDbConnection(ConnectionString)
        cn.Open()

        DataMin = DateSerial(Anno, Mese, 0)
        DataMax = DateSerial(Anno, Mese, GiorniMese(Mese, Anno))
        OldData = DateSerial(1899, 12, 30)
        cmd.CommandText = " SELECT * FROM GiustificativiDelGiorno " & _
                          " WHERE CodiceDipendente = ?" & _
                          " AND Data >= ?" & _
                          " AND Data <= ?" & _
                          " ORDER BY Data"
        cmd.Parameters.AddWithValue("@CodiceDipendente", CodiceDipendente)
        cmd.Parameters.AddWithValue("@Data", DataMin)
        cmd.Parameters.AddWithValue("@Data", DataMax)
        cmd.Connection = cn

        Dim myPOSTreader As OleDbDataReader = cmd.ExecuteReader()

        Do While myPOSTreader.Read
            If OldData <> DataDb(myPOSTreader.Item("Data")) Then
                OldData = DataDb(myPOSTreader.Item("Data"))
                i = 0
            End If
            r = DateDiff("d", DataMin, DataDb(myPOSTreader.Item("Data")))
            Vettore_Giustificativi.Orario(i, r) = Format(DataDb(myPOSTreader.Item("Orario")), "HH.mm")
            Vettore_Giustificativi.Giustificativo(i, r) = StringaDb(myPOSTreader.Item("Giustificativo"))
            i = i + 1
        Loop
        myPOSTreader.Close()
        cn.Close()
    End Sub

End Class
