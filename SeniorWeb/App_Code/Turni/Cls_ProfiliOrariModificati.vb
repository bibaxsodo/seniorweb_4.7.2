Imports Microsoft.VisualBasic
Imports System.Data.OleDb
Imports System.Web.Hosting

Public Class Cls_ProfiliOrariModificati
    Public Id As Long
    Public Utente As String
    Public DataAggiornamento As Date
    Public Data As Date
    Public CodiceDipendente As Integer
    Public CodiceFamiliare As Byte
    Public Dalle As Date
    Public Alle As Date
    Public Pausa As Date
    Public Giustificativo As String
    Public NelGruppo As String

    Public Sub Pulisci()
        Id = 0
        Utente = ""
        DataAggiornamento = Nothing
        Data = Nothing
        CodiceDipendente = 0
        CodiceFamiliare = 0
        Dalle = Nothing
        Alle = Nothing
        Pausa = Nothing
        Giustificativo = ""
        NelGruppo = ""
    End Sub

    Function campodb(ByVal oggetto As Object) As String
        If IsDBNull(oggetto) Then
            Return ""
        Else
            Return oggetto
        End If
    End Function

    Public Sub Scrivi(ByVal StringaConnessione As String)
        Dim cn As OleDbConnection
        Dim MySql As String


        cn = New Data.OleDb.OleDbConnection(StringaConnessione)

        cn.Open()

        MySql = "INSERT INTO ProfiliOrariModificati (Dalle,Alle,Pausa,Giustificativo,Data,CodiceDipendente,NelGruppo) VALUES (?,?,?,?,?,?,?)  "

        Dim cmdw As New OleDbCommand()
        cmdw.Connection = cn
        cmdw.CommandText = MySql
        cmdw.Parameters.AddWithValue("@Dalle", Dalle)
        cmdw.Parameters.AddWithValue("@Alle", Alle)
        cmdw.Parameters.AddWithValue("@Pausa", IIf(Hour(Pausa) = 0, System.DBNull.Value, Pausa))
        cmdw.Parameters.AddWithValue("@Giustificativo", Giustificativo)
        cmdw.Parameters.AddWithValue("@Data", Data)
        cmdw.Parameters.AddWithValue("@CodiceDipendente", CodiceDipendente)
        cmdw.Parameters.AddWithValue("@NelGruppo", NelGruppo)
        cmdw.ExecuteNonQuery()
        cn.Close()
    End Sub


    Public Sub Elimina(ByVal StringaConnessione As String)
        Dim cn As OleDbConnection

        cn = New Data.OleDb.OleDbConnection(StringaConnessione)
        cn.Open()

        Dim cmd As New OleDbCommand()
        cmd.CommandText = ("Delete  from ProfiliOrariModificati where " & _
                           " Data = ? And CodiceDipendente = ?  ")
        cmd.Parameters.AddWithValue("@Data", Data)
        cmd.Parameters.AddWithValue("@CodiceDipendente", CodiceDipendente)
        cmd.Connection = cn
        cmd.ExecuteNonQuery()
        cn.Close()
    End Sub


    Public Sub Leggi(ByVal StringaConnessione As String)
        Dim cn As OleDbConnection

        cn = New Data.OleDb.OleDbConnection(StringaConnessione)

        cn.Open()
        Dim cmd As New OleDbCommand()
        cmd.CommandText = ("select * from ProfiliOrariModificati where " & _
                           " Data = ? And CodiceDipendente = ?  ")
        cmd.Parameters.AddWithValue("@Data", Data)
        cmd.Parameters.AddWithValue("@CodiceDipendente", CodiceDipendente)
        cmd.Connection = cn
        Dim sb As StringBuilder = New StringBuilder

        Dim myPOSTreader As OleDbDataReader = cmd.ExecuteReader()
        If myPOSTreader.Read Then
            CodiceFamiliare = Val(campodb(myPOSTreader.Item("CodiceFamiliare")))
            Dalle = campodbd(myPOSTreader.Item("Dalle"))
            Alle = campodbd(myPOSTreader.Item("Alle"))
            Pausa = campodbd(myPOSTreader.Item("Pausa"))
            Giustificativo = campodb(myPOSTreader.Item("Giustificativo"))
            NelGruppo = campodb(myPOSTreader.Item("NelGruppo"))
        Else
            Call Pulisci()
        End If
        cn.Close()
    End Sub



    Function campodbd(ByVal oggetto As Object) As Date
        If IsDBNull(oggetto) Then
            Return Nothing
        Else
            Return oggetto
        End If
    End Function
End Class

