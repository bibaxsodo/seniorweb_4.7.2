Imports Microsoft.VisualBasic
Imports System.Data.OleDb
Imports System.Web.Hosting

Public Class Cls_MovimentiDiStruttura
    Public Id As Long
    Public Utente As String
    Public DataAggiornamento As Date
    Public Data As Date
    Public CodiceDipendente As Integer
    Public CodiceStruttura As String
    Public CodiceDipendenteSostituito As Integer

    Public Sub Pulisci()
        Id = 0
        Utente = ""
        DataAggiornamento = Nothing
        Data = Nothing
        CodiceDipendente = 0
        CodiceStruttura = ""
        CodiceDipendenteSostituito = 0
    End Sub
End Class
