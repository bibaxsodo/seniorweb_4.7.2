Imports Microsoft.VisualBasic
Imports System.Data.OleDb
Imports System.Web.Hosting

Public Class Cls_GiustificativiPerGruppo
    Public Id As Long
    Public Utente As String
    Public DataAggiornamento As Date
    Public Gruppo As String
    Public Codice As String
    Public Descrizione As String
    Public CodiceDiRiferimento As String
    Public ColoreSfondo As Long
    Public ColoreScritta As Long
    Public Riga As Integer
    Public Peso As Integer

    Public Sub Pulisci()
        Id = 0
        Utente = ""
        DataAggiornamento = Nothing
        Gruppo = ""
        Codice = ""
        Descrizione = ""
        CodiceDiRiferimento = ""
        ColoreSfondo = 0
        ColoreScritta = 0
        Riga = 0
        Peso = 0
    End Sub

    Public Sub Elimina(ByVal StringaConnessione As String, ByVal xGruppo As String, ByVal xCodice As String)
        Dim cn As OleDbConnection

        cn = New Data.OleDb.OleDbConnection(StringaConnessione)

        cn.Open()
        Dim cmd As New OleDbCommand()
        cmd.CommandText = "DELETE FROM GiustificativiPerGruppo" & _
                          " WHERE Gruppo = ?" & _
                          " AND Codice = ?"
        cmd.Parameters.AddWithValue("@Gruppo", xGruppo)
        cmd.Parameters.AddWithValue("@Codice", xCodice)
        cmd.Connection = cn
        cmd.ExecuteNonQuery()
        cn.Close()
    End Sub

    Sub Aggiorna(ByVal StringaConnessione As String)
        Dim cn As OleDbConnection

        cn = New Data.OleDb.OleDbConnection(StringaConnessione)

        cn.Open()
        Dim cmd As New OleDbCommand()
        cmd.CommandText = "SELECT * FROM GiustificativiPerGruppo" & _
                          " WHERE Gruppo = ?" & _
                          " AND Codice = ?"
        cmd.Parameters.AddWithValue("@Gruppo", Gruppo)
        cmd.Parameters.AddWithValue("@Codice", Codice)
        cmd.Connection = cn
        Dim myPOSTreader As OleDbDataReader = cmd.ExecuteReader()
        If myPOSTreader.Read Then
            Dim cmdw As New OleDbCommand()
            cmdw.CommandText = "UPDATE GiustificativiPerGruppo SET " & _
                               " Utente = ?," & _
                               " DataAggiornamento  = ?," & _
                               " Descrizione = ?," & _
                               " CodiceDiRiferimento = ?," & _
                               " ColoreSfondo = ?," & _
                               " ColoreScritta = ?," & _
                               " Riga = ?," & _
                               " Peso = ?" & _
                               " WHERE Gruppo = ?" & _
                               " AND Codice = ?"

            cmdw.Parameters.AddWithValue("@Utente", Utente)
            cmdw.Parameters.AddWithValue("@DataAggiornamento ", Now)
            cmdw.Parameters.AddWithValue("@Descrizione", Descrizione)
            cmdw.Parameters.AddWithValue("@CodiceDiRiferimento", CodiceDiRiferimento)
            cmdw.Parameters.AddWithValue("@ColoreSfondo", ColoreSfondo)
            cmdw.Parameters.AddWithValue("@ColoreScritta", ColoreScritta)
            cmdw.Parameters.AddWithValue("@Riga", Riga)
            cmdw.Parameters.AddWithValue("@Peso", Peso)
            cmdw.Parameters.AddWithValue("@Gruppo", Gruppo)
            cmdw.Parameters.AddWithValue("@Codice", Codice)
            cmdw.Connection = cn
            cmdw.ExecuteNonQuery()
        Else
            Dim cmdw As New OleDbCommand()
            cmdw.CommandText = "INSERT INTO GiustificativiPerGruppo (Utente, DataAggiornamento, Gruppo, Codice, Descrizione, ColoreSfondo, ColoreScritta, Riga, Peso) VALUES (?,?,?,?,?,?,?,?,?)"

            cmdw.Parameters.AddWithValue("@Utente", Utente)
            cmdw.Parameters.AddWithValue("@DataAggiornamento", Now)
            cmdw.Parameters.AddWithValue("@Gruppo", Gruppo)
            cmdw.Parameters.AddWithValue("@Codice", Codice)
            cmdw.Parameters.AddWithValue("@Descrizione", Descrizione)
            cmdw.Parameters.AddWithValue("@ColoreSfondo", ColoreSfondo)
            cmdw.Parameters.AddWithValue("@ColoreScritta", ColoreScritta)
            cmdw.Parameters.AddWithValue("@Riga", Riga)
            cmdw.Parameters.AddWithValue("@Peso", Peso)
            cmdw.Connection = cn
            cmdw.ExecuteNonQuery()
        End If
        myPOSTreader.Close()
        cn.Close()

        Call Pulisci()
    End Sub

    Sub LoadDati(ByVal StringaConnessione As String, ByVal xGruppo As String, ByVal Tabella As System.Data.DataTable)
        Dim cn As OleDbConnection

        cn = New Data.OleDb.OleDbConnection(StringaConnessione)

        cn.Open()
        Dim cmd As New OleDbCommand()

        cmd.CommandText = "Select * From GiustificativiPerGruppo" & _
                          " WHERE Gruppo = ?" & _
                          " Order By Descrizione"

        cmd.Parameters.AddWithValue("@Gruppo", xGruppo)

        cmd.Connection = cn

        Tabella.Clear()
        Tabella.Columns.Add("Codice", GetType(String))
        Tabella.Columns.Add("Descrizione", GetType(String))
        Tabella.Columns.Add("Riferimento", GetType(String))

        Dim myPOSTreader As OleDbDataReader = cmd.ExecuteReader()
        Do While myPOSTreader.Read

            Dim myriga As System.Data.DataRow = Tabella.NewRow()
            myriga(0) = myPOSTreader.Item("Codice")
            myriga(1) = myPOSTreader.Item("Descrizione")
            myriga(2) = myPOSTreader.Item("CodiceDiRiferimento")

            Tabella.Rows.Add(myriga)
        Loop
        myPOSTreader.Close()
        cn.Close()

    End Sub

    Function StringaDb(ByVal Oggetto As Object) As String
        If IsDBNull(Oggetto) Then
            Return ""
        Else
            Return Oggetto
        End If
    End Function

    Function NumeroDb(ByVal Oggetto As Object) As Object
        If IsDBNull(Oggetto) Then
            Return 0
        Else
            Return Oggetto
        End If
    End Function

    Sub Leggi(ByVal StringaConnessione As String, ByVal xGruppo As String, ByVal xCodice As String)
        Dim cn As OleDbConnection

        cn = New Data.OleDb.OleDbConnection(StringaConnessione)

        cn.Open()
        Dim cmd As New OleDbCommand()
        cmd.CommandText = "Select * From GiustificativiPerGruppo" & _
                          " WHERE Gruppo = ?" & _
                          " AND Codice = ?"
        cmd.Parameters.AddWithValue("@Gruppo", xGruppo)
        cmd.Parameters.AddWithValue("@Codice", xCodice)
        cmd.Connection = cn

        Dim myPOSTreader As OleDbDataReader = cmd.ExecuteReader()
        If myPOSTreader.Read Then
            Id = NumeroDb(myPOSTreader.Item("Id"))
            Gruppo = StringaDb(myPOSTreader.Item("Gruppo"))
            Codice = StringaDb(myPOSTreader.Item("Codice"))
            Descrizione = StringaDb(myPOSTreader.Item("Descrizione"))
            CodiceDiRiferimento = StringaDb(myPOSTreader.Item("CodiceDiRiferimento"))
            ColoreSfondo = NumeroDb(myPOSTreader.Item("ColoreSfondo"))
            ColoreScritta = NumeroDb(myPOSTreader.Item("ColoreScritta"))
            Riga = NumeroDb(myPOSTreader.Item("Riga"))
            Peso = NumeroDb(myPOSTreader.Item("Peso"))
        Else
            Call Pulisci()
        End If
        cn.Close()
    End Sub

    Function TestUsato(ByVal StringaConnessione As String, ByVal xGruppo As String, ByVal xCodice As String) As Boolean
        Dim cn As OleDbConnection
        cn = New Data.OleDb.OleDbConnection(StringaConnessione)
        cn.Open()

        Dim cmd As New OleDbCommand()
        cmd.CommandText = "SELECT Count(*) FROM OrdiniServizioRiga" & _
                          " WHERE Gruppo = ?" & _
                          " AND ServizioPerGruppo = ?"
        cmd.Parameters.AddWithValue("@Gruppo", xGruppo)
        cmd.Parameters.AddWithValue("@ServizioPerGruppo", xCodice)

        cmd.Connection = cn
        Dim myPOSTreader As OleDbDataReader = cmd.ExecuteReader()
        If myPOSTreader.Read Then
            If myPOSTreader.Item(0) = 0 Then
                Return False
            Else
                Return True
            End If
        Else
            Return False
        End If
        myPOSTreader.Close()
        cn.Close()
    End Function

    Function Esiste(ByVal ConnectionString As String, ByVal xGruppo As String, ByVal xCodice As String) As Boolean
        Dim cn As OleDbConnection

        cn = New Data.OleDb.OleDbConnection(ConnectionString)

        cn.Open()
        Dim cmd As New OleDbCommand()
        cmd.CommandText = "Select * FROM GiustificativiPerGruppo" & _
                          " WHERE Gruppo = ?" & _
                          " AND Codice = ?"
        cmd.Parameters.AddWithValue("@Gruppo", xGruppo)
        cmd.Parameters.AddWithValue("@Codice", xCodice)
        cmd.Connection = cn

        Dim myPOSTreader As OleDbDataReader = cmd.ExecuteReader()

        If myPOSTreader.Read Then
            Return True
        Else
            Return False
        End If
        myPOSTreader.Close()
        cn.Close()
    End Function

    Function DescrizioneDuplicata(ByVal ConnectionString As String, ByVal xGruppo As String, ByVal xCodice As String, ByVal xDescrizione As String) As Boolean
        DescrizioneDuplicata = False
        Dim cn As OleDbConnection

        cn = New Data.OleDb.OleDbConnection(ConnectionString)

        cn.Open()
        Dim cmd As New OleDbCommand()
        cmd.CommandText = "Select * FROM GiustificativiPerGruppo" & _
                          " WHERE Gruppo = ?" & _
                          " AND Descrizione = ?"
        cmd.Parameters.AddWithValue("@Gruppo", xGruppo)
        cmd.Parameters.AddWithValue("@Descrizione", xDescrizione)
        cmd.Connection = cn

        Dim myPOSTreader As OleDbDataReader = cmd.ExecuteReader()

        Do While myPOSTreader.Read
            If myPOSTreader.Item("Codice") <> xCodice Then
                Return True
                Exit Do
            End If
        Loop
        myPOSTreader.Close()
        cn.Close()
    End Function

    Sub UpDateDropBox(ByVal StringaConnessione As String, ByVal xGruppo As String, ByRef Appoggio As DropDownList)
        Dim cn As OleDbConnection

        cn = New Data.OleDb.OleDbConnection(StringaConnessione)

        cn.Open()
        Dim cmd As New OleDbCommand()
        cmd.CommandText = "SELECT Codice, Descrizione FROM GiustificativiPerGruppo" & _
                          " WHERE Gruppo = ?" & _
                          " ORDER BY Descrizione"
        cmd.Parameters.AddWithValue("@Gruppo", xGruppo)
        cmd.Connection = cn

        Appoggio.Items.Clear()
        Dim myPOSTreader As OleDbDataReader = cmd.ExecuteReader()
        Do While myPOSTreader.Read
            Appoggio.Items.Add(myPOSTreader.Item("Descrizione"))
            Appoggio.Items(Appoggio.Items.Count - 1).Value = myPOSTreader.Item("Codice")
        Loop
        myPOSTreader.Close()
        cn.Close()
        Appoggio.Items.Add("")
        Appoggio.Items(Appoggio.Items.Count - 1).Value = ""
        Appoggio.Items(Appoggio.Items.Count - 1).Selected = True

    End Sub
End Class
