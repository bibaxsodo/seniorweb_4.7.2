﻿Imports Microsoft.VisualBasic
Imports System.Data.OleDb
Imports System.Web.Hosting

Public Class Cls_Struttura_Giustificativi_Giorno
    Public Quantita(2) As Double
    Public Orario(2) As String
    Public Giustificativo(2) As String

    Public Sub Pulisci()

        For i = 0 To 2
            Quantita(i) = 0
            Orario(i) = ""
            Giustificativo(i) = ""
        Next i
    End Sub
End Class
