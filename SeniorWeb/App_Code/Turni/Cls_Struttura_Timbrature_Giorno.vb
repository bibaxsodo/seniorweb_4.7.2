﻿Imports Microsoft.VisualBasic
Imports System.Data.OleDb
Imports System.Web.Hosting

Public Class Cls_Struttura_Timbrature_Giorno
    Public Dalle(9) As String
    Public Dalle_Causale(9) As String
    Public Dalle_Colore(9) As Long
    Public Alle(9) As String
    Public Alle_Causale(9) As String
    Public Alle_Colore(9) As Long

    Public Sub Pulisci()
        For i = 0 To 9
            Dalle(i) = ""
            Dalle_Causale(i) = ""
            Dalle_Colore(i) = RGB(255, 255, 255)
            Alle(i) = ""
            Alle_Causale(i) = ""
            Alle_Colore(i) = RGB(255, 255, 255)
        Next i
    End Sub
End Class
