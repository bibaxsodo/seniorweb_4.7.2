Imports Microsoft.VisualBasic
Imports System.Data.OleDb
Imports System.Web.Hosting

Public Class Cls_ParagrafoDiStruttura
    Public Id As Long
    Public Utente As String
    Public DataAggiornamento As Date
    Public CodiceLivello As Long
    Public CodicePadre As String
    Public Codice As String
    Public Descrizione As String
    Public PresenzaMinima1 As Integer
    Public PresenzaMassima1 As Integer
    Public Dalgg1 As Byte
    Public Dalmm1 As Byte
    Public PresenzaMinima2 As Integer
    Public PresenzaMassima2 As Integer
    Public Dalgg2 As Byte
    Public Dalmm2 As Byte
    Public PresenzaMinima3 As Integer
    Public PresenzaMassima3 As Integer
    Public Dalgg3 As Byte
    Public Dalmm3 As Byte
    Public PresenzaMinima4 As Integer
    Public PresenzaMassima4 As Integer
    Public Dalgg4 As Byte
    Public Dalmm4 As Byte
    Public PresenzaMinima5 As Integer
    Public PresenzaMassima5 As Integer
    Public Dalgg5 As Byte
    Public Dalmm5 As Byte
    Public PresenzaMinima6 As Integer
    Public PresenzaMassima6 As Integer
    Public Dalgg6 As Byte
    Public Dalmm6 As Byte
    Public PresenzaMinima7 As Integer
    Public PresenzaMassima7 As Integer
    Public Dalgg7 As Byte
    Public Dalmm7 As Byte
    Public PresenzaMinima8 As Integer
    Public PresenzaMassima8 As Integer
    Public Dalgg8 As Byte
    Public Dalmm8 As Byte
    Public PresenzaMinima9 As Integer
    Public PresenzaMassima9 As Integer
    Public Dalgg9 As Byte
    Public Dalmm9 As Byte
    Public PresenzaMinima10 As Integer
    Public PresenzaMassima10 As Integer
    Public Dalgg10 As Byte
    Public Dalmm10 As Byte
    Public PresenzaMinima11 As Integer
    Public PresenzaMassima11 As Integer
    Public Dalgg11 As Byte
    Public Dalmm11 As Byte
    Public PresenzaMinima12 As Integer
    Public PresenzaMassima12 As Integer
    Public Dalgg12 As Byte
    Public Dalmm12 As Byte
    Public AttivoSabato As String
    Public AttivoFestivo As String
    Public CodiceScript As String

    Public Sub Pulisci()
        Id = 0
        Utente = ""
        DataAggiornamento = Nothing
        CodiceLivello = 0
        CodicePadre = ""
        Codice = ""
        Descrizione = ""
        PresenzaMinima1 = 0
        PresenzaMassima1 = 0
        Dalgg1 = 0
        Dalmm1 = 0
        PresenzaMinima2 = 0
        PresenzaMassima2 = 0
        Dalgg2 = 0
        Dalmm2 = 0
        PresenzaMinima3 = 0
        PresenzaMassima3 = 0
        Dalgg3 = 0
        Dalmm3 = 0
        PresenzaMinima4 = 0
        PresenzaMassima4 = 0
        Dalgg4 = 0
        Dalmm4 = 0
        PresenzaMinima5 = 0
        PresenzaMassima5 = 0
        Dalgg5 = 0
        Dalmm5 = 0
        PresenzaMinima6 = 0
        PresenzaMassima6 = 0
        Dalgg6 = 0
        Dalmm6 = 0
        PresenzaMinima7 = 0
        PresenzaMassima7 = 0
        Dalgg7 = 0
        Dalmm7 = 0
        PresenzaMinima8 = 0
        PresenzaMassima8 = 0
        Dalgg8 = 0
        Dalmm8 = 0
        PresenzaMinima9 = 0
        PresenzaMassima9 = 0
        Dalgg9 = 0
        Dalmm9 = 0
        PresenzaMinima10 = 0
        PresenzaMassima10 = 0
        Dalgg10 = 0
        Dalmm10 = 0
        PresenzaMinima11 = 0
        PresenzaMassima11 = 0
        Dalgg11 = 0
        Dalmm11 = 0
        PresenzaMinima12 = 0
        PresenzaMassima12 = 0
        Dalgg12 = 0
        Dalmm12 = 0
        AttivoSabato = ""
        AttivoFestivo = ""
        CodiceScript = ""
    End Sub

    Public Sub Elimina(ByVal StringaConnessione As String, ByVal xCodice As String)
        Dim cn As OleDbConnection
        Dim MySql As String

        MySql = ""

        cn = New Data.OleDb.OleDbConnection(StringaConnessione)

        cn.Open()
        Dim cmd As New OleDbCommand()
        cmd.CommandText = ("DELETE FROM ParagrafoDiStruttura WHERE " & _
                           "Codice = ?")
        cmd.Parameters.AddWithValue("@Codice", xCodice)
        cmd.Connection = cn
        cmd.ExecuteNonQuery()
        cn.Close()
    End Sub

    Sub Aggiorna(ByVal StringaConnessione As String)
        Dim cn As OleDbConnection
        Dim MySql As String

        MySql = ""

        cn = New Data.OleDb.OleDbConnection(StringaConnessione)

        cn.Open()
        Dim cmd As New OleDbCommand()
        cmd.CommandText = "SELECT * FROM ParagrafoDiStruttura WHERE Codice = ?"
        cmd.Parameters.AddWithValue("@Codice", Codice)
        cmd.Connection = cn
        Dim myPOSTreader As OleDbDataReader = cmd.ExecuteReader()
        If myPOSTreader.Read Then
            MySql = "UPDATE ParagrafoDiStruttura SET " & _
                    " Utente = ?, " & _
                    " DataAggiornamento = ?," & _
                    " CodiceLivello = ?," & _
                    " CodicePadre = ?," & _
                    " Descrizione = ?," & _
                    " PresenzaMinima1 = ?," & _
                    " PresenzaMassima1 = ?," & _
                    " Dalgg1= ?," & _
                    " Dalmm1= ?," & _
                    " PresenzaMinima2 = ?," & _
                    " PresenzaMassima2 = ?," & _
                    " Dalgg2= ?," & _
                    " Dalmm2= ?," & _
                    " PresenzaMinima3 = ?," & _
                    " PresenzaMassima3 = ?," & _
                    " Dalgg3= ?," & _
                    " Dalmm3= ?," & _
                    " PresenzaMinima4 = ?," & _
                    " PresenzaMassima4 = ?," & _
                    " Dalgg4= ?," & _
                    " Dalmm4= ?," & _
                    " PresenzaMinima5 = ?," & _
                    " PresenzaMassima5 = ?," & _
                    " Dalgg5= ?," & _
                    " Dalmm5= ?," & _
                    " PresenzaMinima6 = ?," & _
                    " PresenzaMassima6 = ?," & _
                    " Dalgg6= ?," & _
                    " Dalmm6= ?," & _
                    " PresenzaMinima7 = ?," & _
                    " PresenzaMassima7 = ?," & _
                    " Dalgg7= ?," & _
                    " Dalmm7= ?," & _
                    " PresenzaMinima8 = ?," & _
                    " PresenzaMassima8 = ?," & _
                    " Dalgg8= ?," & _
                    " Dalmm8= ?," & _
                    " PresenzaMinima9 = ?," & _
                    " PresenzaMassima9 = ?," & _
                    " Dalgg9= ?," & _
                    " Dalmm9= ?," & _
                    " PresenzaMinima10 = ?," & _
                    " PresenzaMassima10 = ?," & _
                    " Dalgg10= ?," & _
                    " Dalmm10= ?," & _
                    " PresenzaMinima11 = ?," & _
                    " PresenzaMassima11 = ?," & _
                    " Dalgg11= ?," & _
                    " Dalmm11= ?," & _
                    " PresenzaMinima12 = ?," & _
                    " PresenzaMassima12 = ?," & _
                    " Dalgg12= ?," & _
                    " Dalmm12= ?," & _
                    " AttivoSabato = ?," & _
                    " AttivoFestivo = ?," & _
                    " CodiceScript = ?" & _
                    " WHERE Codice = ?"

            Dim cmdw As New OleDbCommand()
            cmdw.CommandText = MySql
            cmdw.Parameters.AddWithValue("@Utente", Utente)
            cmdw.Parameters.AddWithValue("@DataAggiornamento", Now)
            cmdw.Parameters.AddWithValue("@CodiceLivello", CodiceLivello)
            cmdw.Parameters.AddWithValue("@CodicePadre", CodicePadre)
            cmdw.Parameters.AddWithValue("@Descrizione", Descrizione)
            cmdw.Parameters.AddWithValue("@PresenzaMinima1", PresenzaMinima1)
            cmdw.Parameters.AddWithValue("@PresenzaMassima1", PresenzaMassima1)
            cmdw.Parameters.AddWithValue("@Dalgg1", Dalgg1)
            cmdw.Parameters.AddWithValue("@Dalmm1", Dalmm1)
            cmdw.Parameters.AddWithValue("@PresenzaMinima2", PresenzaMinima2)
            cmdw.Parameters.AddWithValue("@PresenzaMassima2", PresenzaMassima2)
            cmdw.Parameters.AddWithValue("@Dalgg2", Dalgg2)
            cmdw.Parameters.AddWithValue("@Dalmm2", Dalmm2)
            cmdw.Parameters.AddWithValue("@PresenzaMinima3", PresenzaMinima3)
            cmdw.Parameters.AddWithValue("@PresenzaMassima3", PresenzaMassima3)
            cmdw.Parameters.AddWithValue("@Dalgg3", Dalgg3)
            cmdw.Parameters.AddWithValue("@Dalmm3", Dalmm3)
            cmdw.Parameters.AddWithValue("@PresenzaMinima4", PresenzaMinima4)
            cmdw.Parameters.AddWithValue("@PresenzaMassima4", PresenzaMassima4)
            cmdw.Parameters.AddWithValue("@Dalgg4", Dalgg4)
            cmdw.Parameters.AddWithValue("@Dalmm4", Dalmm4)
            cmdw.Parameters.AddWithValue("@PresenzaMinima5", PresenzaMinima5)
            cmdw.Parameters.AddWithValue("@PresenzaMassima5", PresenzaMassima5)
            cmdw.Parameters.AddWithValue("@Dalgg5", Dalgg5)
            cmdw.Parameters.AddWithValue("@Dalmm5", Dalmm5)
            cmdw.Parameters.AddWithValue("@PresenzaMinima6", PresenzaMinima6)
            cmdw.Parameters.AddWithValue("@PresenzaMassima6", PresenzaMassima6)
            cmdw.Parameters.AddWithValue("@Dalgg6", Dalgg6)
            cmdw.Parameters.AddWithValue("@Dalmm6", Dalmm6)
            cmdw.Parameters.AddWithValue("@PresenzaMinima7", PresenzaMinima7)
            cmdw.Parameters.AddWithValue("@PresenzaMassima7", PresenzaMassima7)
            cmdw.Parameters.AddWithValue("@Dalgg7", Dalgg7)
            cmdw.Parameters.AddWithValue("@Dalmm7", Dalmm7)
            cmdw.Parameters.AddWithValue("@PresenzaMinima8", PresenzaMinima8)
            cmdw.Parameters.AddWithValue("@PresenzaMassima8", PresenzaMassima8)
            cmdw.Parameters.AddWithValue("@Dalgg8", Dalgg8)
            cmdw.Parameters.AddWithValue("@Dalmm8", Dalmm8)
            cmdw.Parameters.AddWithValue("@PresenzaMinima9", PresenzaMinima9)
            cmdw.Parameters.AddWithValue("@PresenzaMassima9", PresenzaMassima9)
            cmdw.Parameters.AddWithValue("@Dalgg9", Dalgg9)
            cmdw.Parameters.AddWithValue("@Dalmm9", Dalmm9)
            cmdw.Parameters.AddWithValue("@PresenzaMinima10", PresenzaMinima10)
            cmdw.Parameters.AddWithValue("@PresenzaMassima10", PresenzaMassima10)
            cmdw.Parameters.AddWithValue("@Dalgg10", Dalgg10)
            cmdw.Parameters.AddWithValue("@Dalmm10", Dalmm10)
            cmdw.Parameters.AddWithValue("@PresenzaMinima11", PresenzaMinima11)
            cmdw.Parameters.AddWithValue("@PresenzaMassima11", PresenzaMassima11)
            cmdw.Parameters.AddWithValue("@Dalgg11", Dalgg11)
            cmdw.Parameters.AddWithValue("@Dalmm11", Dalmm11)
            cmdw.Parameters.AddWithValue("@PresenzaMinima12", PresenzaMinima12)
            cmdw.Parameters.AddWithValue("@PresenzaMassima12", PresenzaMassima12)
            cmdw.Parameters.AddWithValue("@Dalgg12", Dalgg12)
            cmdw.Parameters.AddWithValue("@Dalmm12", Dalmm12)
            cmdw.Parameters.AddWithValue("@AttivoSabato", AttivoSabato)
            cmdw.Parameters.AddWithValue("@AttivoFestivo", AttivoFestivo)
            cmdw.Parameters.AddWithValue("@CodiceScript", CodiceScript)
            cmdw.Parameters.AddWithValue("@Codice", Codice)
            cmdw.Connection = cn
            cmdw.ExecuteNonQuery()
        Else
            MySql = "INSERT INTO ParagrafoDiStruttura (Utente, DataAggiornamento, CodiceLivello, CodicePadre, Codice, Descrizione, PresenzaMinima1, PresenzaMassima1, Dalgg1, Dalmm1, PresenzaMinima2, PresenzaMassima2, Dalgg2, Dalmm2, PresenzaMinima3, PresenzaMassima3, Dalgg3, Dalmm3, PresenzaMinima4, PresenzaMassima4, Dalgg4, Dalmm4, PresenzaMinima5, PresenzaMassima5, Dalgg5, Dalmm5, PresenzaMinima6, PresenzaMassima6, Dalgg6, Dalmm6, PresenzaMinima7, PresenzaMassima7, Dalgg7, Dalmm7, PresenzaMinima8, PresenzaMassima8, Dalgg8, Dalmm8, PresenzaMinima9, PresenzaMassima9, Dalgg9, Dalmm9, PresenzaMinima10, PresenzaMassima10, Dalgg10, Dalmm10, PresenzaMinima11, PresenzaMassima11, Dalgg11, Dalmm11, PresenzaMinima12, PresenzaMassima12, Dalgg12, Dalmm12, AttivoSabato, AttivoFestivo, CodiceScript) VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)"

            Dim cmdw As New OleDbCommand()
            cmdw.CommandText = MySql
            cmdw.Parameters.AddWithValue("@Utente", Utente)
            cmdw.Parameters.AddWithValue("@DataAggiornamento", Now)
            cmdw.Parameters.AddWithValue("@CodiceLivello", CodiceLivello)
            cmdw.Parameters.AddWithValue("@CodicePadre", CodicePadre)
            cmdw.Parameters.AddWithValue("@Codice", Codice)
            cmdw.Parameters.AddWithValue("@Descrizione", Descrizione)
            cmdw.Parameters.AddWithValue("@PresenzaMinima1", PresenzaMinima1)
            cmdw.Parameters.AddWithValue("@PresenzaMassima1", PresenzaMassima1)
            cmdw.Parameters.AddWithValue("@Dalgg1", Dalgg1)
            cmdw.Parameters.AddWithValue("@Dalmm1", Dalmm1)
            cmdw.Parameters.AddWithValue("@PresenzaMinima2", PresenzaMinima2)
            cmdw.Parameters.AddWithValue("@PresenzaMassima2", PresenzaMassima2)
            cmdw.Parameters.AddWithValue("@Dalgg2", Dalgg2)
            cmdw.Parameters.AddWithValue("@Dalmm2", Dalmm2)
            cmdw.Parameters.AddWithValue("@PresenzaMinima3", PresenzaMinima3)
            cmdw.Parameters.AddWithValue("@PresenzaMassima3", PresenzaMassima3)
            cmdw.Parameters.AddWithValue("@Dalgg3", Dalgg3)
            cmdw.Parameters.AddWithValue("@Dalmm3", Dalmm3)
            cmdw.Parameters.AddWithValue("@PresenzaMinima4", PresenzaMinima4)
            cmdw.Parameters.AddWithValue("@PresenzaMassima4", PresenzaMassima4)
            cmdw.Parameters.AddWithValue("@Dalgg4", Dalgg4)
            cmdw.Parameters.AddWithValue("@Dalmm4", Dalmm4)
            cmdw.Parameters.AddWithValue("@PresenzaMinima5", PresenzaMinima5)
            cmdw.Parameters.AddWithValue("@PresenzaMassima5", PresenzaMassima5)
            cmdw.Parameters.AddWithValue("@Dalgg5", Dalgg5)
            cmdw.Parameters.AddWithValue("@Dalmm5", Dalmm5)
            cmdw.Parameters.AddWithValue("@PresenzaMinima6", PresenzaMinima6)
            cmdw.Parameters.AddWithValue("@PresenzaMassima6", PresenzaMassima6)
            cmdw.Parameters.AddWithValue("@Dalgg6", Dalgg6)
            cmdw.Parameters.AddWithValue("@Dalmm6", Dalmm6)
            cmdw.Parameters.AddWithValue("@PresenzaMinima7", PresenzaMinima7)
            cmdw.Parameters.AddWithValue("@PresenzaMassima7", PresenzaMassima7)
            cmdw.Parameters.AddWithValue("@Dalgg7", Dalgg7)
            cmdw.Parameters.AddWithValue("@Dalmm7", Dalmm7)
            cmdw.Parameters.AddWithValue("@PresenzaMinima8", PresenzaMinima8)
            cmdw.Parameters.AddWithValue("@PresenzaMassima8", PresenzaMassima8)
            cmdw.Parameters.AddWithValue("@Dalgg8", Dalgg8)
            cmdw.Parameters.AddWithValue("@Dalmm8", Dalmm8)
            cmdw.Parameters.AddWithValue("@PresenzaMinima9", PresenzaMinima9)
            cmdw.Parameters.AddWithValue("@PresenzaMassima9", PresenzaMassima9)
            cmdw.Parameters.AddWithValue("@Dalgg9", Dalgg9)
            cmdw.Parameters.AddWithValue("@Dalmm9", Dalmm9)
            cmdw.Parameters.AddWithValue("@PresenzaMinima10", PresenzaMinima10)
            cmdw.Parameters.AddWithValue("@PresenzaMassima10", PresenzaMassima10)
            cmdw.Parameters.AddWithValue("@Dalgg10", Dalgg10)
            cmdw.Parameters.AddWithValue("@Dalmm10", Dalmm10)
            cmdw.Parameters.AddWithValue("@PresenzaMinima11", PresenzaMinima11)
            cmdw.Parameters.AddWithValue("@PresenzaMassima11", PresenzaMassima11)
            cmdw.Parameters.AddWithValue("@Dalgg11", Dalgg11)
            cmdw.Parameters.AddWithValue("@Dalmm11", Dalmm11)
            cmdw.Parameters.AddWithValue("@PresenzaMinima12", PresenzaMinima12)
            cmdw.Parameters.AddWithValue("@PresenzaMassima12", PresenzaMassima12)
            cmdw.Parameters.AddWithValue("@Dalgg12", Dalgg12)
            cmdw.Parameters.AddWithValue("@Dalmm12", Dalmm12)
            cmdw.Parameters.AddWithValue("@AttivoSabato", AttivoSabato)
            cmdw.Parameters.AddWithValue("@AttivoFestivo", AttivoFestivo)
            cmdw.Parameters.AddWithValue("@CodiceScript", CodiceScript)
            cmdw.Connection = cn
            cmdw.ExecuteNonQuery()
        End If
        myPOSTreader.Close()
        cn.Close()

        Call Pulisci()
    End Sub

    Sub LoadDati(ByVal StringaConnessione As String, ByVal Tabella As System.Data.DataTable)
        Dim cn As OleDbConnection

        cn = New Data.OleDb.OleDbConnection(StringaConnessione)

        cn.Open()
        Dim cmd As New OleDbCommand()

        cmd.CommandText = ("Select * From ParagrafoDiStruttura Order By Descrizione")

        cmd.Connection = cn

        Tabella.Clear()
        Tabella.Columns.Clear()
        Tabella.Columns.Add("Codice", GetType(String))
        Tabella.Columns.Add("Descrizione", GetType(String))
        Tabella.Columns.Add("Codice Livello", GetType(String))
        Tabella.Columns.Add("Codice Padre", GetType(String))

        Dim myPOSTreader As OleDbDataReader = cmd.ExecuteReader()
        Do While myPOSTreader.Read

            Dim myriga As System.Data.DataRow = Tabella.NewRow()
            myriga(0) = myPOSTreader.Item("Codice")
            myriga(1) = myPOSTreader.Item("Descrizione")
            myriga(2) = myPOSTreader.Item("CodiceLivello")
            myriga(3) = myPOSTreader.Item("CodicePadre")

            Tabella.Rows.Add(myriga)
        Loop
        myPOSTreader.Close()
        cn.Close()

    End Sub

    Function StringaDb(ByVal Oggetto As Object) As String
        If IsDBNull(Oggetto) Then
            Return ""
        Else
            Return Oggetto
        End If
    End Function

    Function NumeroDb(ByVal Oggetto As Object) As Object
        If IsDBNull(Oggetto) Then
            Return 0
        Else
            Return Oggetto
        End If
    End Function

    Sub Leggi(ByVal StringaConnessione As String, ByVal xCodice As String)
        Dim cn As OleDbConnection

        cn = New Data.OleDb.OleDbConnection(StringaConnessione)

        cn.Open()
        Dim cmd As New OleDbCommand()
        cmd.CommandText = ("Select * From ParagrafoDiStruttura WHERE " & _
                           "Codice = ?")
        cmd.Parameters.AddWithValue("@Codice", xCodice)
        cmd.Connection = cn

        Dim myPOSTreader As OleDbDataReader = cmd.ExecuteReader()
        If myPOSTreader.Read Then
            Id = NumeroDb(myPOSTreader.Item("Id"))
            CodiceLivello = NumeroDb(myPOSTreader.Item("Codice"))
            CodicePadre = StringaDb(myPOSTreader.Item("CodicePadre"))
            Codice = StringaDb(myPOSTreader.Item("Codice"))
            Descrizione = StringaDb(myPOSTreader.Item("Descrizione"))
            PresenzaMinima1 = NumeroDb(myPOSTreader.Item("PresenzaMinima1"))
            PresenzaMassima1 = NumeroDb(myPOSTreader.Item("PresenzaMassima1"))
            Dalgg1 = NumeroDb(myPOSTreader.Item("Dalgg1"))
            Dalmm1 = NumeroDb(myPOSTreader.Item("Dalmm1"))
            PresenzaMinima2 = NumeroDb(myPOSTreader.Item("PresenzaMinima2"))
            PresenzaMassima2 = NumeroDb(myPOSTreader.Item("PresenzaMassima2"))
            Dalgg2 = NumeroDb(myPOSTreader.Item("Dalgg2"))
            Dalmm2 = NumeroDb(myPOSTreader.Item("Dalmm2"))
            PresenzaMinima3 = NumeroDb(myPOSTreader.Item("PresenzaMinima3"))
            PresenzaMassima3 = NumeroDb(myPOSTreader.Item("PresenzaMassima3"))
            Dalgg3 = NumeroDb(myPOSTreader.Item("Dalgg3"))
            Dalmm3 = NumeroDb(myPOSTreader.Item("Dalmm3"))
            PresenzaMinima4 = NumeroDb(myPOSTreader.Item("PresenzaMinima4"))
            PresenzaMassima4 = NumeroDb(myPOSTreader.Item("PresenzaMassima4"))
            Dalgg4 = NumeroDb(myPOSTreader.Item("Dalgg4"))
            Dalmm4 = NumeroDb(myPOSTreader.Item("Dalmm4"))
            PresenzaMinima5 = NumeroDb(myPOSTreader.Item("PresenzaMinima5"))
            PresenzaMassima5 = NumeroDb(myPOSTreader.Item("PresenzaMassima5"))
            Dalgg5 = NumeroDb(myPOSTreader.Item("Dalgg5"))
            Dalmm5 = NumeroDb(myPOSTreader.Item("Dalmm5"))
            PresenzaMinima6 = NumeroDb(myPOSTreader.Item("PresenzaMinima6"))
            PresenzaMassima6 = NumeroDb(myPOSTreader.Item("PresenzaMassima6"))
            Dalgg6 = NumeroDb(myPOSTreader.Item("Dalgg6"))
            Dalmm6 = NumeroDb(myPOSTreader.Item("Dalmm6"))
            PresenzaMinima7 = NumeroDb(myPOSTreader.Item("PresenzaMinima7"))
            PresenzaMassima7 = NumeroDb(myPOSTreader.Item("PresenzaMassima7"))
            Dalgg7 = NumeroDb(myPOSTreader.Item("Dalgg7"))
            Dalmm7 = NumeroDb(myPOSTreader.Item("Dalmm7"))
            PresenzaMinima8 = NumeroDb(myPOSTreader.Item("PresenzaMinima8"))
            PresenzaMassima8 = NumeroDb(myPOSTreader.Item("PresenzaMassima8"))
            Dalgg8 = NumeroDb(myPOSTreader.Item("Dalgg8"))
            Dalmm8 = NumeroDb(myPOSTreader.Item("Dalmm8"))
            PresenzaMinima9 = NumeroDb(myPOSTreader.Item("PresenzaMinima9"))
            PresenzaMassima9 = NumeroDb(myPOSTreader.Item("PresenzaMassima9"))
            Dalgg9 = NumeroDb(myPOSTreader.Item("Dalgg9"))
            Dalmm9 = NumeroDb(myPOSTreader.Item("Dalmm9"))
            PresenzaMinima10 = NumeroDb(myPOSTreader.Item("PresenzaMinima10"))
            PresenzaMassima10 = NumeroDb(myPOSTreader.Item("PresenzaMassima10"))
            Dalgg10 = NumeroDb(myPOSTreader.Item("Dalgg10"))
            Dalmm10 = NumeroDb(myPOSTreader.Item("Dalmm10"))
            PresenzaMinima11 = NumeroDb(myPOSTreader.Item("PresenzaMinima11"))
            PresenzaMassima11 = NumeroDb(myPOSTreader.Item("PresenzaMassima11"))
            Dalgg11 = NumeroDb(myPOSTreader.Item("Dalgg11"))
            Dalmm11 = NumeroDb(myPOSTreader.Item("Dalmm11"))
            PresenzaMinima12 = NumeroDb(myPOSTreader.Item("PresenzaMinima12"))
            PresenzaMassima12 = NumeroDb(myPOSTreader.Item("PresenzaMassima12"))
            Dalgg12 = NumeroDb(myPOSTreader.Item("Dalgg12"))
            Dalmm12 = NumeroDb(myPOSTreader.Item("Dalmm12"))
            AttivoSabato = StringaDb(myPOSTreader.Item("AttivoSabato"))
            AttivoFestivo = StringaDb(myPOSTreader.Item("AttivoFestivo"))
            CodiceScript = StringaDb(myPOSTreader.Item("CodiceScript"))
        Else
            Call Pulisci()
        End If
        cn.Close()
    End Sub

    Sub UpDateDropBox(ByVal StringaConnessione As String, ByRef Appoggio As DropDownList)
        Dim cn As OleDbConnection

        cn = New Data.OleDb.OleDbConnection(StringaConnessione)

        cn.Open()
        Dim cmd As New OleDbCommand()
        cmd.CommandText = ("SELECT Codice, Descrizione FROM ParagrafoDiStruttura ORDER BY Descrizione")
        cmd.Connection = cn

        Appoggio.Items.Clear()
        Dim myPOSTreader As OleDbDataReader = cmd.ExecuteReader()
        Do While myPOSTreader.Read
            Appoggio.Items.Add(myPOSTreader.Item("Descrizione"))
            Appoggio.Items(Appoggio.Items.Count - 1).Value = myPOSTreader.Item("Codice")
        Loop
        myPOSTreader.Close()
        cn.Close()
        Appoggio.Items.Add("")
        Appoggio.Items(Appoggio.Items.Count - 1).Value = ""
        Appoggio.Items(Appoggio.Items.Count - 1).Selected = True

    End Sub

End Class
