Imports Microsoft.VisualBasic
Imports System.Data.OleDb
Imports System.Web.Hosting

Public Class Cls_GruppiDipendentiGestiti
    Public Id As Long
    Public Utente As String
    Public DataAggiornamento As Date
    Public CodiceDipendente As Long
    Public CodiceGruppo As String
    Public Gestione As String
    Public Autorizza1 As String
    Public Autorizza2 As String
    Public Autorizza3 As String

    Public Sub Pulisci()
        Id = 0
        Utente = ""
        DataAggiornamento = Nothing
        CodiceDipendente = 0
        CodiceGruppo = ""
        Gestione = ""
        Autorizza1 = ""
        Autorizza2 = ""
        Autorizza3 = ""
    End Sub

    Public Sub EliminaAll(ByVal StringaConnessione As String, ByVal xCodiceDipendente As Long)
        Dim cn As OleDbConnection

        cn = New Data.OleDb.OleDbConnection(StringaConnessione)

        cn.Open()
        Dim cmd As New OleDbCommand()
        cmd.CommandText = "DELETE FROM GruppiDipendentiGestiti" & _
                           " WHERE CodiceDipendente = ?"

        cmd.Parameters.AddWithValue("@CodiceDipendente", xCodiceDipendente)
        cmd.Connection = cn
        cmd.ExecuteNonQuery()
        cn.Close()
    End Sub

    Sub Aggiorna(ByVal StringaConnessione As String)
        Dim cn As OleDbConnection

        cn = New Data.OleDb.OleDbConnection(StringaConnessione)

        cn.Open()
        Dim cmd As New OleDbCommand()
        cmd.CommandText = "SELECT * FROM GruppiDipendentiGestiti" & _
                          " WHERE CodiceDipendente = ?" & _
                          " AND Codicegruppo = ?"
        cmd.Parameters.AddWithValue("@CodiceDipendente", CodiceDipendente)
        cmd.Parameters.AddWithValue("@Codicegruppo", CodiceGruppo)
        cmd.Connection = cn
        Dim myPOSTreader As OleDbDataReader = cmd.ExecuteReader()
        If myPOSTreader.Read Then
            Dim cmdw As New OleDbCommand()
            cmdw.CommandText = "UPDATE GruppiDipendentiGestiti SET " & _
                    " Utente = ?," & _
                    " DataAggiornamento  = ?," & _
                    " Gestione = ?," & _
                    " Autorizza1 = ?," & _
                    " Autorizza2 = ?," & _
                    " Autorizza3 = ?" & _
                    " WHERE CodiceDipendente = ?" & _
                    " AND Codicegruppo = ?"
            cmdw.Parameters.AddWithValue("@Utente", Utente)
            cmdw.Parameters.AddWithValue("@DataAggiornamento", Now)
            cmdw.Parameters.AddWithValue("@Gestione", Gestione)
            cmdw.Parameters.AddWithValue("@Autorizza1", Autorizza1)
            cmdw.Parameters.AddWithValue("@Autorizza2", Autorizza2)
            cmdw.Parameters.AddWithValue("@Autorizza3", Autorizza3)
            cmdw.Parameters.AddWithValue("@CodiceDipendente", CodiceDipendente)
            cmdw.Parameters.AddWithValue("@Codicegruppo", CodiceGruppo)
            cmdw.Connection = cn
            cmdw.ExecuteNonQuery()
        Else
            Dim cmdw As New OleDbCommand()
            cmdw.CommandText = "INSERT INTO GruppiDipendentiGestiti (Utente, DataAggiornamento, CodiceDipendente, CodiceGruppo, Gestione, Autorizza1, Autorizza2, Autorizza3) VALUES (?,?,?,?,?,?,?,?)"

            cmdw.Parameters.AddWithValue("@Utente", Utente)
            cmdw.Parameters.AddWithValue("@DataAggiornamento", Now)
            cmdw.Parameters.AddWithValue("@CodiceDipendente", CodiceDipendente)
            cmdw.Parameters.AddWithValue("@Codicegruppo", CodiceGruppo)
            cmdw.Parameters.AddWithValue("@Gestione", Gestione)
            cmdw.Parameters.AddWithValue("@Autorizza1", Autorizza1)
            cmdw.Parameters.AddWithValue("@Autorizza2", Autorizza2)
            cmdw.Parameters.AddWithValue("@Autorizza3", Autorizza3)
            cmdw.Connection = cn
            cmdw.ExecuteNonQuery()
        End If
        myPOSTreader.Close()
        cn.Close()

        Call Pulisci()
    End Sub

    Sub LoadDati(ByVal StringaConnessione As String, ByVal Tabella As System.Data.DataTable)
        Dim cn As OleDbConnection

        cn = New Data.OleDb.OleDbConnection(StringaConnessione)


        cn.Open()
        Dim cmd As New OleDbCommand()

        cmd.CommandText = "SELECT Dipendenti.CodiceDipendente, Dipendenti.Cognome, Dipendenti.Nome FROM Dipendenti Order By Cognome, Nome"

        cmd.Connection = cn

        Tabella.Clear()
        Tabella.Columns.Clear()
        Tabella.Columns.Add("Codice Dipendente", GetType(String))
        Tabella.Columns.Add("Cognome", GetType(String))
        Tabella.Columns.Add("Nome", GetType(String))

        Dim myPOSTreader As OleDbDataReader = cmd.ExecuteReader()
        Do While myPOSTreader.Read

            Dim myriga As System.Data.DataRow = Tabella.NewRow()
            myriga(0) = myPOSTreader.Item("CodiceDipendente")
            myriga(1) = myPOSTreader.Item("Cognome")
            myriga(2) = myPOSTreader.Item("Nome")

            Tabella.Rows.Add(myriga)
        Loop
        myPOSTreader.Close()
        cn.Close()

    End Sub

    Function StringaDb(ByVal Oggetto As Object) As String
        If IsDBNull(Oggetto) Then
            Return ""
        Else
            Return Oggetto
        End If
    End Function

    Function NumeroDb(ByVal Oggetto As Object) As Object
        If IsDBNull(Oggetto) Then
            Return 0
        Else
            Return Oggetto
        End If
    End Function

    Sub Leggi(ByVal StringaConnessione As String, ByVal xCodiceDipendente As Long, ByVal xCodiceGruppo As String)
        Dim cn As OleDbConnection

        cn = New Data.OleDb.OleDbConnection(StringaConnessione)

        cn.Open()
        Dim cmd As New OleDbCommand()
        cmd.CommandText = "SELECT * FROM GruppiDipendentiGestiti" & _
                           " WHERE CodiceDipendente = ?" & _
                           " AND Codicegruppo = ?"
        cmd.Parameters.AddWithValue("@CodiceDipendente", xCodiceDipendente)
        cmd.Parameters.AddWithValue("@Codicegruppo", xCodiceGruppo)
        cmd.Connection = cn

        Dim myPOSTreader As OleDbDataReader = cmd.ExecuteReader()
        If myPOSTreader.Read Then
            Id = NumeroDb(myPOSTreader.Item("Id"))
            CodiceDipendente = NumeroDb(myPOSTreader.Item("CodiceDipendente"))
            CodiceGruppo = StringaDb(myPOSTreader.Item("Codicegruppo"))
            Gestione = StringaDb(myPOSTreader.Item("Gestione"))
            Autorizza1 = StringaDb(myPOSTreader.Item("Autorizza1"))
            Autorizza2 = StringaDb(myPOSTreader.Item("Autorizza2"))
            Autorizza3 = StringaDb(myPOSTreader.Item("Autorizza3"))
        Else
            Call Pulisci()
        End If
        cn.Close()
    End Sub

    Function Esiste(ByVal StringaConnessione As String, ByVal xCodiceDipendente As Long, ByVal xCodiceGruppo As String) As Boolean
        Dim cn As OleDbConnection

        cn = New Data.OleDb.OleDbConnection(StringaConnessione)

        cn.Open()
        Dim cmd As New OleDbCommand()
        cmd.CommandText = "SELECT * FROM GruppiDipendentiGestiti" & _
                           " WHERE CodiceDipendente = ?" & _
                           " AND Codicegruppo = ?"
        cmd.Parameters.AddWithValue("@CodiceDipendente", xCodiceDipendente)
        cmd.Parameters.AddWithValue("@Codicegruppo", xCodiceGruppo)
        cmd.Connection = cn

        Dim myPOSTreader As OleDbDataReader = cmd.ExecuteReader()
        If myPOSTreader.Read Then
            Return True
        Else
            Return False
        End If
        myPOSTreader.Close()
        cn.Close()
    End Function

    Public Sub CaricaGrigliaGruppo(ByVal StringaConnessione As String, ByVal xCodiceDipendente As Long, ByVal xValidita As Date, ByRef Tabella As System.Data.DataTable)
        Dim cn As OleDbConnection
        Dim Fine As Boolean
        Dim r As Integer
        Dim i As Integer
        Dim ii As Integer

        Tabella.Clear()
        Tabella.Columns.Add("Gestito", GetType(String))
        Tabella.Columns.Add("Autorizza1", GetType(String))
        Tabella.Columns.Add("Autorizza2", GetType(String))
        Tabella.Columns.Add("Autorizza3", GetType(String))
        Tabella.Columns.Add("LegatoA", GetType(String))
        Tabella.Columns.Add("NelServizio", GetType(String))
        Tabella.Columns.Add("OrdiniServizio", GetType(String))
        Tabella.Columns.Add("Codice", GetType(String))
        Tabella.Columns.Add("Chiave", GetType(String))
        Tabella.Columns.Add("Livello", GetType(String))
        Tabella.Columns.Add("Livello1", GetType(String))
        Tabella.Columns.Add("Livello2", GetType(String))
        Tabella.Columns.Add("Livello3", GetType(String))

        cn = New Data.OleDb.OleDbConnection(StringaConnessione)

        cn.Open()
        Dim cmd As New OleDbCommand()

        cmd.CommandText = "SELECT (SELECT TOP 1 LegatoA FROM GruppiDipendentiLegami WHERE GruppiDipendentiLegami.Codice = GruppiDipendenti.Codice And Validita <= ? ORDER BY Validita DESC, Id DESC) AS LegatoA, (SELECT TOP 1 NelServizio FROM GruppiDipendentiLegami WHERE GruppiDipendentiLegami.Codice = GruppiDipendenti.Codice And Validita <= ? ORDER BY Validita DESC, Id DESC) AS NelServizio, (SELECT TOP 1 OrdiniServizio FROM GruppiDipendentiLegami WHERE GruppiDipendentiLegami.Codice = GruppiDipendenti.Codice And Validita <= ? ORDER BY Validita DESC, Id DESC) AS OrdiniServizio, Codice, Descrizione + ' (' + Attivo + ')' As Descrizione1 FROM GruppiDipendenti " & _
          " WHERE Attivo = 'S'" & _
          " OR (Attivo = 'N' AND DataNonAttivo > ?)" & _
          " ORDER BY Codice"

        cmd.Parameters.AddWithValue("@Validita", xValidita)
        cmd.Parameters.AddWithValue("@Validita", xValidita)
        cmd.Parameters.AddWithValue("@Validita", xValidita)
        cmd.Parameters.AddWithValue("@Validita", xValidita)
        cmd.Connection = cn

        Dim myPOSTreader As OleDbDataReader = cmd.ExecuteReader()
        Do While myPOSTreader.Read

            Dim myriga As System.Data.DataRow = Tabella.NewRow()
            myriga(0) = 0
            myriga(1) = 0
            myriga(2) = 0
            myriga(3) = 0
            myriga(4) = myPOSTreader.Item("LegatoA")
            myriga(5) = myPOSTreader.Item("NelServizio")
            myriga(6) = myPOSTreader.Item("OrdiniServizio")
            myriga(7) = myPOSTreader.Item("Codice")
            myriga(9) = 0
            myriga(10) = myPOSTreader.Item("Descrizione1")

            Tabella.Rows.Add(myriga)
        Loop
        myPOSTreader.Close()

        Fine = False
        Do Until Fine = True
            Fine = True
            For r = 0 To Tabella.Rows.Count - 1
                If IsDBNull(Tabella.Rows(r).Item(4)) Or Tabella.Rows(r).Item(4) = "" Then
                    Tabella.Rows(r).Item(8) = Tabella.Rows(r).Item(7)
                    Tabella.Rows(r).Item(9) = 1
                Else
                    For i = 0 To Tabella.Rows.Count - 1
                        If Tabella.Rows(r).Item(4) = Tabella.Rows(i).Item(7) Then
                            If Not IsDBNull(Tabella.Rows(i).Item(8)) Then
                                If IsDBNull(Tabella.Rows(r).Item(8)) Then
                                    Tabella.Rows(r).Item(8) = Tabella.Rows(i).Item(8) + Tabella.Rows(r).Item(7)
                                    Tabella.Rows(r).Item(9) = Tabella.Rows(i).Item(9) + 1
                                    ii = Tabella.Rows(r).Item(9) + 10
                                    If Tabella.Columns.Count < ii Then
                                        Tabella.Columns.Add("Livello - " & Tabella.Rows(r).Item(9), GetType(String))
                                    End If
                                    Tabella.Rows(r).Item(ii - 1) = Tabella.Rows(r).Item(10)
                                    Tabella.Rows(r).Item(10) = ""
                                End If
                            Else
                                Fine = False
                            End If
                            Exit For
                        End If
                    Next i
                End If
            Next r
        Loop

        Dim dv As System.Data.DataView = Tabella.DefaultView
        dv.Sort = "Chiave"
        Tabella = dv.ToTable

        cmd.CommandText = "SELECT * FROM GruppiDipendentiGestiti" & _
                           " WHERE CodiceDipendente = ?"
        cmd.Parameters.Clear()
        cmd.Parameters.AddWithValue("@CodiceDipendente", xCodiceDipendente)
        cmd.Connection = cn

        myPOSTreader = cmd.ExecuteReader()
        Do While myPOSTreader.Read
            CodiceGruppo = StringaDb(myPOSTreader.Item("CodiceGruppo"))
            For i = 0 To Tabella.Rows.Count - 1
                If Tabella.Rows(i).Item(7) = CodiceGruppo Then
                    If StringaDb(myPOSTreader.Item("Gestione")) = "S" Then
                        Tabella.Rows(i).Item(0) = 1
                    End If
                    If StringaDb(myPOSTreader.Item("Autorizza1")) = "S" Then
                        Tabella.Rows(i).Item(1) = 1
                    End If
                    If StringaDb(myPOSTreader.Item("Autorizza2")) = "S" Then
                        Tabella.Rows(i).Item(2) = 1
                    End If
                    If StringaDb(myPOSTreader.Item("Autorizza3")) = "S" Then
                        Tabella.Rows(i).Item(3) = 1
                    End If
                    Exit For
                End If
            Next
        Loop
        myPOSTreader.Close()
        cn.Close()
    End Sub

End Class
