Imports Microsoft.VisualBasic
Imports System.Data.OleDb
Imports System.Web.Hosting

Public Class Cls_TabellaPassWord
    Public Id As Long
    Public Utente As String
    Public DataAggiornamento As Date
    Public NomeUtente As String
    Public Password As String
    Public Abilitazioni As String
    Public CodiceDipendente As Integer
    Public UltimoAccesso As Date
    Public Applicazione As String
    Public Profilo As String

    Public Sub Pulisci()
        Id = 0
        Utente = ""
        DataAggiornamento = Nothing
        NomeUtente = ""
        Password = ""
        Abilitazioni = ""
        CodiceDipendente = 0
        UltimoAccesso = Nothing
        Applicazione = ""
        Profilo = ""
    End Sub

    Public Sub Elimina(ByVal StringaConnessione As String, ByVal xNomeUtente As String)
        Dim cn As OleDbConnection

        cn = New Data.OleDb.OleDbConnection(StringaConnessione)

        cn.Open()
        Dim cmd As New OleDbCommand()
        cmd.CommandText = ("DELETE FROM TabellaPassWord where NomeUtente = ?")
        cmd.Parameters.AddWithValue("@NomeUtente", xNomeUtente)
        cmd.Connection = cn
        cmd.ExecuteNonQuery()
        cn.Close()
    End Sub

    Sub Aggiorna(ByVal StringaConnessione As String)
        Dim cn As OleDbConnection
        Dim MySql As String

        MySql = ""

        cn = New Data.OleDb.OleDbConnection(StringaConnessione)

        cn.Open()
        Dim cmd As New OleDbCommand()
        cmd.CommandText = "SELECT * FROM TabellaPassWord WHERE NomeUtente = ?"
        cmd.Parameters.AddWithValue("@NomeUtente", NomeUtente)
        cmd.Connection = cn
        Dim myPOSTreader As OleDbDataReader = cmd.ExecuteReader()
        Dim cmdw As New OleDbCommand()
        If myPOSTreader.Read Then
            cmdw.CommandText = "UPDATE TabellaPassWord SET " & _
                               " Utente = ?," & _
                               " DataAggiornamento = ?," & _
                               " Password = ?," & _
                               " Abilitazioni = ?," & _
                               " CodiceDipendente = ?," & _
                               " UltimoAccesso = ?," & _
                               " Applicazione = ?," & _
                               " Profilo = ?" & _
                               " WHERE NomeUtente = ?"

            cmdw.Parameters.AddWithValue("@Utente", Utente)
            cmdw.Parameters.AddWithValue("@DataAggiornamento ", Now)
            cmdw.Parameters.AddWithValue("@Password", Password)
            cmdw.Parameters.AddWithValue("@Abilitazioni", Abilitazioni)
            cmdw.Parameters.AddWithValue("@CodiceDipendente", CodiceDipendente)
            cmdw.Parameters.AddWithValue("@UltimoAccesso", UltimoAccesso)
            cmdw.Parameters.AddWithValue("@Applicazione", Applicazione)
            cmdw.Parameters.AddWithValue("@Profilo", Profilo)
            cmdw.Parameters.AddWithValue("@NomeUtente", NomeUtente)
            cmdw.Connection = cn
            cmdw.ExecuteNonQuery()
        Else
            cmdw.CommandText = "INSERT INTO TabellaPassWord (Utente, DataAggiornamento, NomeUtente, Password, Abilitazioni, CodiceDipendente, UltimoAccesso, Applicazione, Profilo) VALUES (?,?,?,?,?,?,?,?,?)"

            cmdw.Parameters.AddWithValue("@Utente", Utente)
            cmdw.Parameters.AddWithValue("@DataAggiornamento", Now)
            cmdw.Parameters.AddWithValue("@NomeUtente", NomeUtente)
            cmdw.Parameters.AddWithValue("@Password", Password)
            cmdw.Parameters.AddWithValue("@Abilitazioni", Abilitazioni)
            cmdw.Parameters.AddWithValue("@CodiceDipendente", CodiceDipendente)
            cmdw.Parameters.AddWithValue("@UltimoAccesso", UltimoAccesso)
            cmdw.Parameters.AddWithValue("@Applicazione", Applicazione)
            cmdw.Parameters.AddWithValue("@Profilo", Profilo)
            cmdw.Connection = cn
            cmdw.ExecuteNonQuery()
        End If
        myPOSTreader.Close()
        cn.Close()

        Call Pulisci()
    End Sub

    Sub LoadDati(ByVal StringaConnessione As String, ByVal Tabella As System.Data.DataTable)
        Dim cn As OleDbConnection

        cn = New Data.OleDb.OleDbConnection(StringaConnessione)

        cn.Open()
        Dim cmd As New OleDbCommand()

        cmd.CommandText = "Select NomeUtente, Descrizione From TabellaPassWord" & _
                          " Order By Descrizione"

        cmd.Connection = cn

        Tabella.Clear()
        Tabella.Columns.Add("NomeUtente", GetType(String))
        Tabella.Columns.Add("Descrizione", GetType(String))

        Dim myPOSTreader As OleDbDataReader = cmd.ExecuteReader()
        Do While myPOSTreader.Read

            Dim myriga As System.Data.DataRow = Tabella.NewRow()
            myriga(0) = myPOSTreader.Item("NomeUtente")
            myriga(1) = myPOSTreader.Item("Descrizione")

            Tabella.Rows.Add(myriga)
        Loop
        myPOSTreader.Close()
        cn.Close()

    End Sub

    Sub SearchDati(ByVal StringaConnessione As String, ByVal xTesto As String, ByVal Tabella As System.Data.DataTable)
        Dim cn As OleDbConnection
        cn = New Data.OleDb.OleDbConnection(StringaConnessione)

        cn.Open()
        Dim cmd As New OleDbCommand()

        cmd.CommandText = "SELECT NomeUtente, Descrizione FROM TabellaPassWord" & _
                          " WHERE CodiceScript like '%" & xTesto & "%'" & _
                          " ORDER BY NomeUtente"

        cmd.Connection = cn

        Tabella.Clear()
        Tabella.Columns.Add("NomeUtente", GetType(String))
        Tabella.Columns.Add("Descrizione", GetType(String))

        Dim myPOSTreader As OleDbDataReader = cmd.ExecuteReader()
        Do While myPOSTreader.Read

            Dim myriga As System.Data.DataRow = Tabella.NewRow()
            myriga(0) = myPOSTreader.Item("NomeUtente")
            myriga(1) = myPOSTreader.Item("Descrizione")

            Tabella.Rows.Add(myriga)
        Loop
        myPOSTreader.Close()
        cn.Close()

    End Sub

    Function StringaDb(ByVal Oggetto As Object) As String
        If IsDBNull(Oggetto) Then
            Return ""
        Else
            Return Oggetto
        End If
    End Function

    Function DataDb(ByVal Oggetto As Object) As Date
        If Not IsDate(Oggetto) Then
            Return Nothing
        Else
            Return Oggetto
        End If
    End Function

    Function NumeroDb(ByVal Oggetto As Object) As Object
        If IsDBNull(Oggetto) Then
            Return 0
        Else
            Return Oggetto
        End If
    End Function

    Sub Leggi(ByVal StringaConnessione As String, ByVal xNomeUtente As String)
        Dim cn As OleDbConnection

        cn = New Data.OleDb.OleDbConnection(StringaConnessione)

        cn.Open()
        Dim cmd As New OleDbCommand()
        cmd.CommandText = "Select * From TabellaPassWord" & _
                            " Where NomeUtente = ?"
        cmd.Parameters.AddWithValue("@NomeUtente", xNomeUtente)
        cmd.Connection = cn

        Dim myPOSTreader As OleDbDataReader = cmd.ExecuteReader()
        If myPOSTreader.Read Then
            Id = NumeroDb(myPOSTreader.Item("Id"))
            NomeUtente = StringaDb(myPOSTreader.Item("NomeUtente"))
            Password = StringaDb(myPOSTreader.Item("Password"))
            Abilitazioni = StringaDb(myPOSTreader.Item("Abilitazioni"))
            CodiceDipendente = NumeroDb(myPOSTreader.Item("CodiceDipendente"))
            UltimoAccesso = DataDb(myPOSTreader.Item("UltimoAccesso"))
            Applicazione = StringaDb(myPOSTreader.Item("Applicazione"))
            Profilo = StringaDb(myPOSTreader.Item("Profilo"))
        Else
            Call Pulisci()
        End If
        cn.Close()
    End Sub

    Function TestUsato(ByVal StringaConnessione As String, ByVal xNomeUtente As String) As Boolean
        Dim cn As OleDbConnection
        cn = New Data.OleDb.OleDbConnection(StringaConnessione)
        cn.Open()

        Dim cmd As New OleDbCommand()
        cmd.CommandText = "Select * FROM TabellaPassWord" & _
                          " WHERE NomeUtente = ?"
        cmd.Parameters.AddWithValue("@NomeUtente", xNomeUtente)

        cmd.Connection = cn
        Dim myPOSTreader As OleDbDataReader = cmd.ExecuteReader()
        If myPOSTreader.Read Then
            If myPOSTreader.Item(0) = 0 Then
                Return False
            Else
                Return True
                myPOSTreader.Close()
                cn.Close()
                Exit Function
            End If
        Else
            Return False
        End If
        myPOSTreader.Close()

        cmd.CommandText = "SELECT Count(*) FROM Giustificativi" & _
                          " WHERE RegolaCalcolo1 = ?" & _
                          " OR RegolaCalcolo2 = ?" & _
                          " OR RegolaCalcolo3 = ?"

        cmd.Parameters.Clear()
        cmd.Parameters.AddWithValue("@RegolaCalcolo1", xNomeUtente)
        cmd.Parameters.AddWithValue("@RegolaCalcolo2", xNomeUtente)
        cmd.Parameters.AddWithValue("@RegolaCalcolo3", xNomeUtente)

        myPOSTreader = cmd.ExecuteReader()
        If myPOSTreader.Read Then
            If myPOSTreader.Item(0) = 0 Then
                Return False
            Else
                Return True
                myPOSTreader.Close()
                cn.Close()
                Exit Function
            End If
        Else
            Return False
        End If
        myPOSTreader.Close()

        Dim p As New Cls_ParametriTurni

        p.Leggi(StringaConnessione)

        Select Case xNomeUtente
            Case p.FormulaCalcoloDiurno
                Return True
            Case p.FormulaCalcoloFestivoDiurno
                Return True
            Case p.FormulaCalcoloFestivoNotturno
                Return True
            Case p.FormulaCalcoloFineGiorno
                Return True
            Case p.FormulaCalcoloFineMese
                Return True
            Case p.FormulaCalcoloInizioGiorno
                Return True
            Case p.FormulaCalcoloInizioMese
                Return True
            Case p.FormulaCalcoloMese
                Return True
            Case p.FormulaCalcoloNotturno
                Return True
            Case p.FormulaCalcoloPausa
                Return True
            Case p.FormulaCalcoloSettimana
                Return True
        End Select

        cn.Close()
    End Function

    Function Esiste(ByVal ConnectionString As String, ByVal xNomeUtente As String) As Boolean
        Dim cn As OleDbConnection

        cn = New Data.OleDb.OleDbConnection(ConnectionString)

        cn.Open()
        Dim cmd As New OleDbCommand()
        cmd.CommandText = "Select * FROM TabellaPassWord" & _
                          " WHERE NomeUtente = ?"
        cmd.Parameters.AddWithValue("@NomeUtente", xNomeUtente)
        cmd.Connection = cn

        Dim myPOSTreader As OleDbDataReader = cmd.ExecuteReader()

        If myPOSTreader.Read Then
            Return True
        Else
            Return False
        End If
        myPOSTreader.Close()
        cn.Close()
    End Function

    Function DescrizioneDuplicata(ByVal ConnectionString As String, ByVal xNomeUtente As String, ByVal xDescrizione As String) As Boolean
        DescrizioneDuplicata = False
        Dim cn As OleDbConnection

        cn = New Data.OleDb.OleDbConnection(ConnectionString)

        cn.Open()
        Dim cmd As New OleDbCommand()
        cmd.CommandText = "Select * FROM TabellaPassWord" & _
                          " WHERE Descrizione = ?"
        cmd.Parameters.AddWithValue("@Descrizione", xDescrizione)
        cmd.Connection = cn

        Dim myPOSTreader As OleDbDataReader = cmd.ExecuteReader()

        Do While myPOSTreader.Read
            If myPOSTreader.Item("NomeUtente") <> xNomeUtente Then
                Return True
                Exit Do
            End If
        Loop
        myPOSTreader.Close()
        cn.Close()
    End Function

    Sub UpDateDropBox(ByVal StringaConnessione As String, ByRef Appoggio As DropDownList)
        Dim cn As OleDbConnection

        cn = New Data.OleDb.OleDbConnection(StringaConnessione)

        cn.Open()
        Dim cmd As New OleDbCommand()
        cmd.CommandText = ("SELECT NomeUtente, Descrizione FROM TabellaPassWord ORDER BY Descrizione")
        cmd.Connection = cn

        Appoggio.Items.Clear()
        Dim myPOSTreader As OleDbDataReader = cmd.ExecuteReader()
        Do While myPOSTreader.Read
            Appoggio.Items.Add(myPOSTreader.Item("Descrizione"))
            Appoggio.Items(Appoggio.Items.Count - 1).Value = myPOSTreader.Item("NomeUtente")
        Loop
        myPOSTreader.Close()
        cn.Close()
        Appoggio.Items.Add("")
        Appoggio.Items(Appoggio.Items.Count - 1).Value = ""
        Appoggio.Items(Appoggio.Items.Count - 1).Selected = True

    End Sub
End Class
