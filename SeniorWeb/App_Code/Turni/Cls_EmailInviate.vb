Imports Microsoft.VisualBasic
Imports System.Data.OleDb
Imports System.Web.Hosting

Public Class Cls_EmailInviate
    Public Id As Long
    Public Data As Date
    Public MailDestinatario As String
    Public Testo As String

    Public Sub Pulisci()
        Id = 0
        Data = Nothing
        MailDestinatario = ""
        Testo = ""
    End Sub

    Public Sub Elimina(ByVal StringaConnessione As String, ByVal xData As Date)
        Dim cn As OleDbConnection
        Dim MySql As String

        MySql = ""

        cn = New Data.OleDb.OleDbConnection(StringaConnessione)

        cn.Open()
        Dim cmd As New OleDbCommand()
        cmd.CommandText = ("DELETE FROM EmailInviate WHERE " & _
                           "Data = ?")
        cmd.Parameters.AddWithValue("@Data", xData)
        cmd.Connection = cn
        cmd.ExecuteNonQuery()
        cn.Close()
    End Sub

    Sub Aggiorna(ByVal StringaConnessione As String)
        Dim cn As OleDbConnection
        Dim MySql As String

        MySql = ""

        cn = New Data.OleDb.OleDbConnection(StringaConnessione)

        cn.Open()
        Dim cmd As New OleDbCommand()
        cmd.CommandText = "SELECT * FROM EmailInviate WHERE Data = ?"
        cmd.Parameters.AddWithValue("@Data", Data)
        cmd.Connection = cn
        Dim myPOSTreader As OleDbDataReader = cmd.ExecuteReader()
        If myPOSTreader.Read Then
            MySql = "UPDATE EmailInviate SET " & _
                    " MailDestinatario  = ? " & _
                    " Testo = ?, " & _
                    " WHERE Data = ?"

            Dim cmdw As New OleDbCommand()
            cmdw.CommandText = MySql
            cmdw.Parameters.AddWithValue("@Data", Data)
            cmdw.Parameters.AddWithValue("@MailDestinatario", MailDestinatario)
            cmdw.Parameters.AddWithValue("@Testo", Testo)
            cmdw.Connection = cn
            cmdw.ExecuteNonQuery()
        Else
            MySql = "INSERT INTO EmailInviate (Data, MailDestinatario, Testo) VALUES (?,?,?)"

            Dim cmdw As New OleDbCommand()
            cmdw.CommandText = MySql
            cmdw.Parameters.AddWithValue("@Data", IIf(Year(Data) > 1, Data, System.DBNull.Value))
            cmdw.Parameters.AddWithValue("@MailDestinatario", MailDestinatario)
            cmdw.Parameters.AddWithValue("@Testo", Testo)
            cmdw.Connection = cn
            cmdw.ExecuteNonQuery()
        End If
        myPOSTreader.Close()
        cn.Close()

        Call Pulisci()
    End Sub

    Sub LoadDati(ByVal StringaConnessione As String, ByVal Tabella As System.Data.DataTable)
        Dim cn As OleDbConnection

        cn = New Data.OleDb.OleDbConnection(StringaConnessione)

        cn.Open()
        Dim cmd As New OleDbCommand()

        cmd.CommandText = ("Select * From EmailInviate Order By MailDestinatario, Data")

        cmd.Connection = cn

        Tabella.Clear()
        Tabella.Columns.Clear()
        Tabella.Columns.Add("MailDestinatario", GetType(String))
        Tabella.Columns.Add("Data", GetType(String))
        Tabella.Columns.Add("Testo", GetType(String))

        Dim myPOSTreader As OleDbDataReader = cmd.ExecuteReader()
        Do While myPOSTreader.Read

            Dim myriga As System.Data.DataRow = Tabella.NewRow()
            myriga(0) = myPOSTreader.Item("MailDestinatario")
            myriga(1) = Format(myPOSTreader.Item("Data"), "dd/MM/yyyy")
            myriga(2) = myPOSTreader.Item("Testo")

            Tabella.Rows.Add(myriga)
        Loop
        myPOSTreader.Close()
        cn.Close()

    End Sub

    Function StringaDb(ByVal Oggetto As Object) As String
        If IsDBNull(Oggetto) Then
            Return ""
        Else
            Return Oggetto
        End If
    End Function

    Function DataDb(ByVal Oggetto As Object) As Date
        If Not IsDate(Oggetto) Then
            Return Nothing
        Else
            Return Oggetto
        End If
    End Function

    Function NumeroDb(ByVal Oggetto As Object) As Object
        If IsDBNull(Oggetto) Then
            Return 0
        Else
            Return Oggetto
        End If
    End Function

    Sub Leggi(ByVal StringaConnessione As String, ByVal xData As String)
        Dim cn As OleDbConnection

        cn = New Data.OleDb.OleDbConnection(StringaConnessione)

        cn.Open()
        Dim cmd As New OleDbCommand()
        cmd.CommandText = ("Select * From EmailInviate WHERE " & _
                           "Data = ?")
        cmd.Parameters.AddWithValue("@Data", xData)
        cmd.Connection = cn

        Dim myPOSTreader As OleDbDataReader = cmd.ExecuteReader()
        If myPOSTreader.Read Then
            Id = NumeroDb(myPOSTreader.Item("Id"))
            Data = DataDb(myPOSTreader.Item("Data"))
            MailDestinatario = StringaDb(myPOSTreader.Item("MailDestinatario"))
            Testo = NumeroDb(myPOSTreader.Item("Testo"))
        Else
            Call Pulisci()
        End If
        cn.Close()
    End Sub

End Class
