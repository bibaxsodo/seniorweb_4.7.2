Imports Microsoft.VisualBasic
Imports System.Data.OleDb
Imports System.Web.Hosting

Public Class Cls_DefinizioneTimbrature
    Public Id As Long
    Public Utente As String
    Public DataAggiornamento As Date
    Public DataBasePath As String
    Public AnnoManuale As String
    Public AnnoDa As Integer
    Public AnnoLungo As Integer
    Public AnnoSomma As Integer
    Public MeseDa As Integer
    Public GiornoDa As Integer
    Public CartellinoDa As Integer
    Public CartellinoLungo As Integer
    Public OreDa As Integer
    Public MinutiDa As Integer
    Public VersoDa As Integer
    Public VersoLungo As Integer
    Public EntrataValore As String
    Public UscitaValore As String
    Public CausaleDa As Integer
    Public CausaleLungo As Integer
    Public Cartellino As String
    Public VisualizzaErrori As String
    Public RilevatoreDa As Integer
    Public RilevatoreLungo As Integer
    Public Rinomina As String
    Public ControlloSequenza As String
    Public BloccoErrori As String
    Public PathDataVariabile As String
    Public IndirizziDiPostaElettronica As String
    Public ServerDiPostaElettronica As String
    Public FormatoData As String
    Public FormatoHHMMSS As String
    Public FormatoEntrataUscita As String
    Public FormatoCartellino As String
    Public FormatoCausale As String
    Public FormatoRilevatore As String
    Public StrutturaFileTimbrature As String
    Public TipoStruttura As String

    Public Sub Pulisci()
        Id = 0
        Utente = ""
        DataAggiornamento = Nothing
        DataBasePath = ""
        AnnoManuale = ""
        AnnoDa = 0
        AnnoLungo = 0
        AnnoSomma = 0
        MeseDa = 0
        GiornoDa = 0
        CartellinoDa = 0
        CartellinoLungo = 0
        OreDa = 0
        MinutiDa = 0
        VersoDa = 0
        VersoLungo = 0
        EntrataValore = ""
        UscitaValore = ""
        CausaleDa = 0
        CausaleLungo = 0
        Cartellino = ""
        VisualizzaErrori = ""
        RilevatoreDa = 0
        RilevatoreLungo = 0
        Rinomina = ""
        ControlloSequenza = ""
        BloccoErrori = ""
        PathDataVariabile = ""
        IndirizziDiPostaElettronica = ""
        ServerDiPostaElettronica = ""
        FormatoData = ""
        FormatoHHMMSS = ""
        FormatoEntrataUscita = ""
        FormatoCartellino = ""
        FormatoCausale = ""
        FormatoRilevatore = ""
        StrutturaFileTimbrature = ""
        TipoStruttura = ""
    End Sub

    Public Sub Elimina(ByVal StringaConnessione As String)
        Dim cn As OleDbConnection
        Dim MySql As String

        MySql = ""

        cn = New Data.OleDb.OleDbConnection(StringaConnessione)

        cn.Open()
        Dim cmd As New OleDbCommand()
        cmd.CommandText = ("DELETE FROM DefinizioneTimbrature")
        cmd.Connection = cn
        cmd.ExecuteNonQuery()
        cn.Close()
    End Sub

    Sub Aggiorna(ByVal StringaConnessione As String)
        Dim cn As OleDbConnection
        Dim MySql As String

        MySql = ""

        cn = New Data.OleDb.OleDbConnection(StringaConnessione)

        cn.Open()
        Dim cmd As New OleDbCommand()
        cmd.CommandText = "SELECT * FROM DefinizioneTimbrature"
        cmd.Connection = cn
        Dim myPOSTreader As OleDbDataReader = cmd.ExecuteReader()
        If myPOSTreader.Read Then
            MySql = "UPDATE DefinizioneTimbrature SET " & _
                    " Utente = ?, " & _
                    " DataAggiornamento = ?," & _
                    " DataBasePath = ?," & _
                    " AnnoManuale = ?," & _
                    " AnnoDa = ?," & _
                    " AnnoLungo = ?," & _
                    " AnnoSomma = ?," & _
                    " MeseDa = ?," & _
                    " GiornoDa = ?," & _
                    " CartellinoDa = ?," & _
                    " CartellinoLungo = ?," & _
                    " OreDa = ?," & _
                    " MinutiDa = ?," & _
                    " VersoDa = ?," & _
                    " VersoLungo = ?," & _
                    " EntrataValore = ?," & _
                    " UscitaValore = ?," & _
                    " CausaleDa = ?," & _
                    " CausaleLungo = ?," & _
                    " Cartellino = ?," & _
                    " VisualizzaErrori = ?," & _
                    " RilevatoreDa = ?," & _
                    " RilevatoreLungo = ?," & _
                    " Rinomina = ?," & _
                    " ControlloSequenza = ?," & _
                    " BloccoErrori = ?," & _
                    " PathDataVariabile = ?" & _
                    " IndirizziDiPostaElettronica = ?" & _
                    " ServerDiPostaElettronica = ?" & _
                    " FormatoData = ?" & _
                    " FormatoHHMMSS = ?" & _
                    " FormatoEntrataUscita = ?" & _
                    " FormatoCartellino = ?" & _
                    " FormatoCausale = ?" & _
                    " FormatoRilevatore = ?" & _
                    " StrutturaFileTimbrature = ?" & _
                    " TipoStruttura = ?"

            Dim cmdw As New OleDbCommand()
            cmdw.CommandText = MySql
            cmdw.Parameters.AddWithValue("@Utente", Utente)
            cmdw.Parameters.AddWithValue("@DataAggiornamento", Now)
            cmdw.Parameters.AddWithValue("@DataBasePath", DataBasePath)
            cmdw.Parameters.AddWithValue("@AnnoManuale", AnnoManuale)
            cmdw.Parameters.AddWithValue("@AnnoDa", AnnoDa)
            cmdw.Parameters.AddWithValue("@AnnoLungo", AnnoLungo)
            cmdw.Parameters.AddWithValue("@AnnoSomma", AnnoSomma)
            cmdw.Parameters.AddWithValue("@MeseDa", MeseDa)
            cmdw.Parameters.AddWithValue("@GiornoDa", GiornoDa)
            cmdw.Parameters.AddWithValue("@CartellinoDa", CartellinoDa)
            cmdw.Parameters.AddWithValue("@CartellinoLungo", CartellinoLungo)
            cmdw.Parameters.AddWithValue("@OreDa", OreDa)
            cmdw.Parameters.AddWithValue("@MinutiDa", MinutiDa)
            cmdw.Parameters.AddWithValue("@VersoDa", VersoDa)
            cmdw.Parameters.AddWithValue("@VersoLungo", VersoLungo)
            cmdw.Parameters.AddWithValue("@EntrataValore", EntrataValore)
            cmdw.Parameters.AddWithValue("@UscitaValore", UscitaValore)
            cmdw.Parameters.AddWithValue("@CausaleDa", CausaleDa)
            cmdw.Parameters.AddWithValue("@CausaleLungo", CausaleLungo)
            cmdw.Parameters.AddWithValue("@Cartellino", Cartellino)
            cmdw.Parameters.AddWithValue("@VisualizzaErrori", VisualizzaErrori)
            cmdw.Parameters.AddWithValue("@RilevatoreDa", RilevatoreDa)
            cmdw.Parameters.AddWithValue("@RilevatoreLungo", RilevatoreLungo)
            cmdw.Parameters.AddWithValue("@Rinomina", Rinomina)
            cmdw.Parameters.AddWithValue("@ControlloSequenza", ControlloSequenza)
            cmdw.Parameters.AddWithValue("@BloccoErrori", BloccoErrori)
            cmdw.Parameters.AddWithValue("@PathDataVariabile", PathDataVariabile)
            cmdw.Parameters.AddWithValue("@IndirizziDiPostaElettronica", IndirizziDiPostaElettronica)
            cmdw.Parameters.AddWithValue("@ServerDiPostaElettronica", ServerDiPostaElettronica)
            cmdw.Parameters.AddWithValue("@FormatoData", FormatoData)
            cmdw.Parameters.AddWithValue("@FormatoHHMMSS", FormatoHHMMSS)
            cmdw.Parameters.AddWithValue("@FormatoEntrataUscita", FormatoEntrataUscita)
            cmdw.Parameters.AddWithValue("@FormatoCartellino", FormatoCartellino)
            cmdw.Parameters.AddWithValue("@FormatoCausale", FormatoCausale)
            cmdw.Parameters.AddWithValue("@FormatoRilevatore", FormatoRilevatore)
            cmdw.Parameters.AddWithValue("@StrutturaFileTimbrature", StrutturaFileTimbrature)
            cmdw.Parameters.AddWithValue("@TipoStruttura", TipoStruttura)
            cmdw.Connection = cn
            cmdw.ExecuteNonQuery()
        Else
            MySql = "INSERT INTO DefinizioneTimbrature (" & _
                    " Utente," & _
                    " DataAggiornamento," & _
                    " DataBasePath," & _
                    " AnnoManuale," & _
                    " AnnoDa," & _
                    " AnnoLungo," & _
                    " AnnoSomma," & _
                    " MeseDa," & _
                    " GiornoDa," & _
                    " CartellinoDa," & _
                    " CartellinoLungo," & _
                    " OreDa," & _
                    " MinutiDa," & _
                    " VersoDa," & _
                    " VersoLungo," & _
                    " EntrataValore," & _
                    " UscitaValore," & _
                    " CausaleDa," & _
                    " CausaleLungo," & _
                    " Cartellino," & _
                    " VisualizzaErrori," & _
                    " RilevatoreDa," & _
                    " RilevatoreLungo," & _
                    " Rinomina," & _
                    " ControlloSequenza," & _
                    " BloccoErrori," & _
                    " PathDataVariabile," & _
                    " IndirizziDiPostaElettronica," & _
                    " ServerDiPostaElettronica," & _
                    " FormatoData," & _
                    " FormatoHHMMSS," & _
                    " FormatoEntrataUscita," & _
                    " FormatoCartellino," & _
                    " FormatoCausale," & _
                    " FormatoRilevatore," & _
                    " StrutturaFileTimbrature," & _
                    " TipoStruttura," & _
                    " VALUES ?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)"

            Dim cmdw As New OleDbCommand()
            cmdw.CommandText = MySql
            cmdw.Parameters.AddWithValue("@Utente", Utente)
            cmdw.Parameters.AddWithValue("@DataAggiornamento", Now)
            cmdw.Parameters.AddWithValue("@DataBasePath", DataBasePath)
            cmdw.Parameters.AddWithValue("@AnnoManuale", AnnoManuale)
            cmdw.Parameters.AddWithValue("@AnnoDa", AnnoDa)
            cmdw.Parameters.AddWithValue("@AnnoLungo", AnnoLungo)
            cmdw.Parameters.AddWithValue("@AnnoSomma", AnnoSomma)
            cmdw.Parameters.AddWithValue("@MeseDa", MeseDa)
            cmdw.Parameters.AddWithValue("@GiornoDa", GiornoDa)
            cmdw.Parameters.AddWithValue("@CartellinoDa", CartellinoDa)
            cmdw.Parameters.AddWithValue("@CartellinoLungo", CartellinoLungo)
            cmdw.Parameters.AddWithValue("@OreDa", OreDa)
            cmdw.Parameters.AddWithValue("@MinutiDa", MinutiDa)
            cmdw.Parameters.AddWithValue("@VersoDa", VersoDa)
            cmdw.Parameters.AddWithValue("@VersoLungo", VersoLungo)
            cmdw.Parameters.AddWithValue("@EntrataValore", EntrataValore)
            cmdw.Parameters.AddWithValue("@UscitaValore", UscitaValore)
            cmdw.Parameters.AddWithValue("@CausaleDa", CausaleDa)
            cmdw.Parameters.AddWithValue("@CausaleLungo", CausaleLungo)
            cmdw.Parameters.AddWithValue("@Cartellino", Cartellino)
            cmdw.Parameters.AddWithValue("@VisualizzaErrori", VisualizzaErrori)
            cmdw.Parameters.AddWithValue("@RilevatoreDa", RilevatoreDa)
            cmdw.Parameters.AddWithValue("@RilevatoreLungo", RilevatoreLungo)
            cmdw.Parameters.AddWithValue("@Rinomina", Rinomina)
            cmdw.Parameters.AddWithValue("@ControlloSequenza", ControlloSequenza)
            cmdw.Parameters.AddWithValue("@BloccoErrori", BloccoErrori)
            cmdw.Parameters.AddWithValue("@PathDataVariabile", PathDataVariabile)
            cmdw.Parameters.AddWithValue("@IndirizziDiPostaElettronica", IndirizziDiPostaElettronica)
            cmdw.Parameters.AddWithValue("@ServerDiPostaElettronica", ServerDiPostaElettronica)
            cmdw.Parameters.AddWithValue("@FormatoData", FormatoData)
            cmdw.Parameters.AddWithValue("@FormatoHHMMSS", FormatoHHMMSS)
            cmdw.Parameters.AddWithValue("@FormatoEntrataUscita", FormatoEntrataUscita)
            cmdw.Parameters.AddWithValue("@FormatoCartellino", FormatoCartellino)
            cmdw.Parameters.AddWithValue("@FormatoCausale", FormatoCausale)
            cmdw.Parameters.AddWithValue("@FormatoRilevatore", FormatoRilevatore)
            cmdw.Parameters.AddWithValue("@StrutturaFileTimbrature", StrutturaFileTimbrature)
            cmdw.Parameters.AddWithValue("@TipoStruttura", TipoStruttura)
            cmdw.Connection = cn
            cmdw.ExecuteNonQuery()
        End If
        myPOSTreader.Close()
        cn.Close()

        Call Pulisci()
    End Sub

    Function StringaDb(ByVal Oggetto As Object) As String
        If IsDBNull(Oggetto) Then
            Return ""
        Else
            Return Oggetto
        End If
    End Function

    Function DataDb(ByVal Oggetto As Object) As Date
        If Not IsDate(Oggetto) Then
            Return Nothing
        Else
            Return Oggetto
        End If
    End Function

    Function NumeroDb(ByVal Oggetto As Object) As Object
        If IsDBNull(Oggetto) Then
            Return 0
        Else
            Return Oggetto
        End If
    End Function

    Sub Leggi(ByVal StringaConnessione As String)
        Dim cn As OleDbConnection

        cn = New Data.OleDb.OleDbConnection(StringaConnessione)

        cn.Open()
        Dim cmd As New OleDbCommand()
        cmd.CommandText = ("Select * From DefinizioneTimbrature")
        cmd.Connection = cn

        Dim myPOSTreader As OleDbDataReader = cmd.ExecuteReader()
        If myPOSTreader.Read Then
            Id = NumeroDb(myPOSTreader.Item("Id"))
            DataBasePath = StringaDb(myPOSTreader.Item("DataBasePath"))
            AnnoManuale = StringaDb(myPOSTreader.Item("AnnoManuale"))
            AnnoDa = NumeroDb(myPOSTreader.Item("AnnoDa"))
            AnnoLungo = NumeroDb(myPOSTreader.Item("AnnoLungo"))
            AnnoSomma = NumeroDb(myPOSTreader.Item("AnnoSomma"))
            MeseDa = NumeroDb(myPOSTreader.Item("MeseDa"))
            GiornoDa = NumeroDb(myPOSTreader.Item("GiornoDa"))
            CartellinoDa = NumeroDb(myPOSTreader.Item("CartellinoDa"))
            CartellinoLungo = NumeroDb(myPOSTreader.Item("CartellinoLungo"))
            OreDa = NumeroDb(myPOSTreader.Item("OreDa"))
            MinutiDa = NumeroDb(myPOSTreader.Item("MinutiDa"))
            VersoDa = NumeroDb(myPOSTreader.Item("VersoDa"))
            VersoLungo = NumeroDb(myPOSTreader.Item("VersoLungo"))
            EntrataValore = StringaDb(myPOSTreader.Item("EntrataValore"))
            UscitaValore = StringaDb(myPOSTreader.Item("UscitaValore"))
            CausaleDa = NumeroDb(myPOSTreader.Item("CausaleDa"))
            CausaleLungo = NumeroDb(myPOSTreader.Item("CausaleLungo"))
            Cartellino = StringaDb(myPOSTreader.Item("Cartellino"))
            VisualizzaErrori = StringaDb(myPOSTreader.Item("VisualizzaErrori"))
            RilevatoreDa = NumeroDb(myPOSTreader.Item("RilevatoreDa"))
            RilevatoreLungo = NumeroDb(myPOSTreader.Item("RilevatoreLungo"))
            Rinomina = StringaDb(myPOSTreader.Item("Rinomina"))
            ControlloSequenza = StringaDb(myPOSTreader.Item("ControlloSequenza"))
            BloccoErrori = StringaDb(myPOSTreader.Item("BloccoErrori"))
            PathDataVariabile = StringaDb(myPOSTreader.Item("PathDataVariabile"))
            IndirizziDiPostaElettronica = StringaDb(myPOSTreader.Item("IndirizziDiPostaElettronica"))
            ServerDiPostaElettronica = StringaDb(myPOSTreader.Item("ServerDiPostaElettronica"))
            FormatoData = StringaDb(myPOSTreader.Item("FormatoData"))
            FormatoHHMMSS = StringaDb(myPOSTreader.Item("FormatoHHMMSS"))
            FormatoEntrataUscita = StringaDb(myPOSTreader.Item("FormatoEntrataUscita"))
            FormatoCartellino = StringaDb(myPOSTreader.Item("FormatoCartellino"))
            FormatoCausale = StringaDb(myPOSTreader.Item("FormatoCausale"))
            FormatoRilevatore = StringaDb(myPOSTreader.Item("FormatoRilevatore"))
            StrutturaFileTimbrature = StringaDb(myPOSTreader.Item("StrutturaFileTimbrature"))
            TipoStruttura = StringaDb(myPOSTreader.Item("TipoStruttura"))
        Else
            Call Pulisci()
        End If
        cn.Close()
    End Sub

End Class
