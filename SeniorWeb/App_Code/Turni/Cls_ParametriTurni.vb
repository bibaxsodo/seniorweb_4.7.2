Imports Microsoft.VisualBasic
Imports System.Data.OleDb
Imports System.Web.Hosting

Public Class Cls_ParametriTurni
    Public Id As Long
    Public Utente As String
    Public DataAggiornamento As Date
    Public CodiceAnagraficoGruppo As String
    Public CodiceAnagraficoSuperGruppo As String
    Public CodiceAnagraficoDitta As String
    Public CodiceAnagraficoOreCreditoDebito As String
    Public CodiceAnagraficoQualifica As String
    Public CodiceAnagraficoTurno As String
    Public CodiceAnagraficoCartellino As String
    Public CodiceAnagraficoContratto As String
    Public CodiceAnagraficoFlessibilita As String
    Public CodiceAnagraficoSelezione As String
    Public CodiceAnagraficoMatricola As String
    Public CodiceAnagraficoFasceOrarie As String
    Public CodiceAnagraficoPosizioneINAIL As String
    Public CodiceAnagraficoNumeroRientri As String
    Public CodiceAnagraficoPiantaOrganica As String
    Public CodiceAnagraficoEquipaggio As String
    Public CodiceAnagraficoTurnoFestivo As String
    Public CodiceAnagraficoTestaDatiFissi As String
    Public CodiceAnagraficoVisualizzazione1 As String
    Public CodiceAnagraficoVisualizzazione2 As String
    Public CodiceAnagraficoVisualizzazione3 As String
    Public CodiceAnagraficoTesseraRiconoscimento As String
    Public CodiceAnagraficoProfiloFerie As String
    Public GiustificativoOrarioLavoro As String
    Public GiustificativoNotte As String
    Public GiustificativoSmontoNotte As String
    Public GiustificativoRiposo As String
    Public GiustificativoAssenza As String
    Public GiustificativoSostituzione As String
    Public GiustificativoEstensioneOrario As String
    Public GiustificativoEntrataPosticipata As String
    Public GiustificativoUscitaAnticipata As String
    Public GiustificativoEntrataAnticipata As String
    Public GiustificativoUscitaPosticipata As String
    Public TempoOrarioLavoro As Date
    Public TempoAssenza As Date
    Public TempoSostituzione As Date
    Public TempoEntrataPosticipata As Date
    Public TempoUscitaAnticipata As Date
    Public TempoEntrataAnticipata As Date
    Public TempoUscitaPosticipata As Date
    Public FormulaCalcoloInizioGiorno As String
    Public FormulaCalcoloFineGiorno As String
    Public FormulaCalcoloSettimana As String
    Public FormulaCalcoloInizioMese As String
    Public FormulaCalcoloFineMese As String
    Public FormulaCalcoloDiurno As String
    Public FormulaCalcoloNotturno As String
    Public FormulaCalcoloFestivoDiurno As String
    Public FormulaCalcoloFestivoNotturno As String
    Public GiornoFineSettimana As Byte
    Public ChiusuraAnagraficaAnno As Integer
    Public ChiusuraAnagraficaMese As Byte
    Public ChiusuraAnagraficaAnnoDaCalcolo As Integer
    Public ChiusuraAnagraficaMeseDaCalcolo As Byte
    Public FormulaCalcoloPausa As String
    Public CodiceDipendenteXCollegamentoRetribuzioni As String
    Public CodiceDipendenteXCollegamentoePersonam As String
    Public StampaTempoWork_1 As String
    Public StampaTempoWork_2 As String
    Public StampaTempoPrevisto As String
    Public StampaPausa As String
    Public StampaTempoStandard As String
    Public StampaTempoGiorno As String
    Public StampaDiurno As String
    Public StampaNotturno As String
    Public StampaFestivoDiurno As String
    Public StampaFestivoNotturno As String
    Public CodiceGruppoDatiVariabiliXSelezioni As String
    Public DataInizioOrdiniServizio As Date
    Public CodiceAnagraficoIndennitaTurno As String
    Public CodiceAnagraficoLivelloRetributivo As String
    Public CodiceAnagraficoMatricolaArma As String
    Public CodiceAnagraficoOrdinanzaArma As String
    Public CodiceGruppoDatiVariabiliMPCS As String
    Public ConfermaObbligatoria As String
    Public GiorniConfermaOrdiniServizio As Integer
    Public CodiceAnagraficoTurnoAB As String
    Public CodiceGruppoDatiVariabiliXServiziComandati As String
    Public GiustificativoPartime As String
    Public GiustificativoAssenzaPerStraordinario As String
    Public GiustificativoStraordinarioPagamento As String
    Public GiustificativoStraordinarioRecupero As String
    Public CreaUtenteSuPassword As String
    Public CodiceAnagraficoIndirizzoPostaElettronica As String
    Public FasciaUnoDalle As Byte
    Public FasciaDueDalle As Byte
    Public FasciaTreDalle As Byte
    Public FasciaQuattroDalle As Byte
    Public FormulaCalcoloMese As String
    Public ProfiliOrariStrutturati As String
    Public CodiceAnagraficoGiorniPresenzaEffettiva As String
    Public GestioneOrariManuali As String
    Public IndirizziDiPostaPerCompleanni As String
    Public IndirizziDiPostaPerTimbrature As String
    Public ServerDiPostaElettronica As String
    Public AbilitazioniDellaProcedura As String
    Public CodiceContrattoDefault As String
    Public AssegnaRiposoFestivo As String
    Public TrattaOrarioPausaAutomatico As String
    Public CodiceOrarioFerie As String
    Public CodiceAnagraficoStrutturaNotti As String
    Public CodiceAnagraficoContatoreFerie As String
    Public CodiceAnagraficoContatorePermessi As String
    Public CodiceAnagraficoPrioritaNellaProposta As String
    Public CodiceAnagraficoNotteSenzaRiposo As String
    Public CodiceAnagraficoUnoLibero As String
    Public CodiceAnagraficoRiposoAlternato As String
    Public CodiceAnagraficoRiposoDoppio As String
    Public CodiceAnagraficoRiposoMassimo As String
    Public TempoTraOrari As Date
    Public QuantiOccupati As Byte
    Public CodiceAnagraficoGiorniEffettivaPresenza As String
    Public CodiceAnagraficoTraDueRiposi As String
    Public TempoPresenzeConsecutive As Single

    Public Cst_Testo As Integer = 0
    Public Cst_NumericoIntero As Integer = 100
    Public Cst_NumericoVirgola As Integer = 200
    Public Cst_Percentuale As Integer = 300
    Public Cst_Data As Integer = 400
    Public Cst_DataOra As Integer = 500
    Public Cst_Orario As Integer = 600
    Public Cst_Numero As Integer = 700
    Public Cst_VeroFalso As Integer = 1000
    Public Cst_SiNo As Integer = 1010
    Public Cst_UtenteFunction As Integer = 30000
    Public Cst_UtenteValue As Integer = 29000
    Public Cst_Noninuso As Integer = 31000
    Public Cst_Combo As Integer = 10
    Public Cst_Combo3 As Integer = 30

    Public Sub Pulisci()
        Id = 0
        Utente = ""
        DataAggiornamento = Nothing
        CodiceAnagraficoGruppo = ""
        CodiceAnagraficoSuperGruppo = ""
        CodiceAnagraficoDitta = ""
        CodiceAnagraficoOreCreditoDebito = ""
        CodiceAnagraficoQualifica = ""
        CodiceAnagraficoTurno = ""
        CodiceAnagraficoCartellino = ""
        CodiceAnagraficoContratto = ""
        CodiceAnagraficoFlessibilita = ""
        CodiceAnagraficoSelezione = ""
        CodiceAnagraficoMatricola = ""
        CodiceAnagraficoFasceOrarie = ""
        CodiceAnagraficoPosizioneINAIL = ""
        CodiceAnagraficoNumeroRientri = ""
        CodiceAnagraficoPiantaOrganica = ""
        CodiceAnagraficoEquipaggio = ""
        CodiceAnagraficoTurnoFestivo = ""
        CodiceAnagraficoTestaDatiFissi = ""
        CodiceAnagraficoVisualizzazione1 = ""
        CodiceAnagraficoVisualizzazione2 = ""
        CodiceAnagraficoVisualizzazione3 = ""
        CodiceAnagraficoTesseraRiconoscimento = ""
        CodiceAnagraficoProfiloFerie = ""
        GiustificativoOrarioLavoro = ""
        GiustificativoNotte = ""
        GiustificativoSmontoNotte = ""
        GiustificativoRiposo = ""
        GiustificativoAssenza = ""
        GiustificativoSostituzione = ""
        GiustificativoEstensioneOrario = ""
        GiustificativoEntrataPosticipata = ""
        GiustificativoUscitaAnticipata = ""
        GiustificativoEntrataAnticipata = ""
        GiustificativoUscitaPosticipata = ""
        TempoOrarioLavoro = Nothing
        TempoAssenza = Nothing
        TempoSostituzione = Nothing
        TempoEntrataPosticipata = Nothing
        TempoUscitaAnticipata = Nothing
        TempoEntrataAnticipata = Nothing
        TempoUscitaPosticipata = Nothing
        FormulaCalcoloInizioGiorno = ""
        FormulaCalcoloFineGiorno = ""
        FormulaCalcoloSettimana = ""
        FormulaCalcoloInizioMese = ""
        FormulaCalcoloFineMese = ""
        FormulaCalcoloDiurno = ""
        FormulaCalcoloNotturno = ""
        FormulaCalcoloFestivoDiurno = ""
        FormulaCalcoloFestivoNotturno = ""
        GiornoFineSettimana = 0
        ChiusuraAnagraficaAnno = 0
        ChiusuraAnagraficaMese = 0
        ChiusuraAnagraficaAnnoDaCalcolo = 0
        ChiusuraAnagraficaMeseDaCalcolo = 0
        FormulaCalcoloPausa = ""
        CodiceDipendenteXCollegamentoRetribuzioni = ""
        CodiceDipendenteXCollegamentoePersonam = ""
        StampaTempoWork_1 = ""
        StampaTempoWork_2 = ""
        StampaTempoPrevisto = ""
        StampaPausa = ""
        StampaTempoStandard = ""
        StampaTempoGiorno = ""
        StampaDiurno = ""
        StampaNotturno = ""
        StampaFestivoDiurno = ""
        StampaFestivoNotturno = ""
        CodiceGruppoDatiVariabiliXSelezioni = ""
        DataInizioOrdiniServizio = Nothing
        CodiceAnagraficoIndennitaTurno = ""
        CodiceAnagraficoLivelloRetributivo = ""
        CodiceAnagraficoMatricolaArma = ""
        CodiceAnagraficoOrdinanzaArma = ""
        CodiceGruppoDatiVariabiliMPCS = ""
        ConfermaObbligatoria = ""
        GiorniConfermaOrdiniServizio = 0
        CodiceAnagraficoTurnoAB = ""
        CodiceGruppoDatiVariabiliXServiziComandati = ""
        GiustificativoPartime = ""
        GiustificativoAssenzaPerStraordinario = ""
        GiustificativoStraordinarioPagamento = ""
        GiustificativoStraordinarioRecupero = ""
        CreaUtenteSuPassword = ""
        CodiceAnagraficoIndirizzoPostaElettronica = ""
        FasciaUnoDalle = 0
        FasciaDueDalle = 0
        FasciaTreDalle = 0
        FasciaQuattroDalle = 0
        FormulaCalcoloMese = ""
        ProfiliOrariStrutturati = ""
        CodiceAnagraficoGiorniPresenzaEffettiva = ""
        GestioneOrariManuali = ""
        IndirizziDiPostaPerCompleanni = ""
        IndirizziDiPostaPerTimbrature = ""
        ServerDiPostaElettronica = ""
        AbilitazioniDellaProcedura = ""
        CodiceContrattoDefault = ""
        AssegnaRiposoFestivo = ""
        TrattaOrarioPausaAutomatico = ""
        CodiceOrarioFerie = ""
        CodiceAnagraficoStrutturaNotti = ""
        CodiceAnagraficoContatoreFerie = ""
        CodiceAnagraficoContatorePermessi = ""
        CodiceAnagraficoPrioritaNellaProposta = ""
        CodiceAnagraficoNotteSenzaRiposo = ""
        CodiceAnagraficoUnoLibero = ""
        CodiceAnagraficoRiposoAlternato = ""
        CodiceAnagraficoRiposoDoppio = ""
        CodiceAnagraficoRiposoMassimo = ""
        TempoTraOrari = Nothing
        QuantiOccupati = 0
        CodiceAnagraficoGiorniEffettivaPresenza = ""
        CodiceAnagraficoTraDueRiposi = ""
        TempoPresenzeConsecutive = 0

    End Sub

    Public Sub Elimina(ByVal StringaConnessione As String)
        Dim cn As OleDbConnection
        Dim MySql As String

        MySql = ""

        cn = New Data.OleDb.OleDbConnection(StringaConnessione)

        cn.Open()
        Dim cmd As New OleDbCommand()
        cmd.CommandText = ("DELETE FROM Parametri")
        cmd.Connection = cn
        cmd.ExecuteNonQuery()
        cn.Close()
    End Sub

    Sub Aggiorna(ByVal StringaConnessione As String)
        Dim cn As OleDbConnection
        Dim MySql As String

        MySql = ""

        cn = New Data.OleDb.OleDbConnection(StringaConnessione)

        cn.Open()
        Dim cmd As New OleDbCommand()
        cmd.CommandText = "SELECT * FROM Parametri"
        cmd.Connection = cn
        Dim myPOSTreader As OleDbDataReader = cmd.ExecuteReader()
        If myPOSTreader.Read Then
            MySql = "UPDATE Parametri SET " & _
                    " Utente = ?, " & _
                    " DataAggiornamento = ?," & _
                    " CodiceAnagraficoGruppo = ?," & _
                    " CodiceAnagraficoSuperGruppo = ?," & _
                    " CodiceAnagraficoDitta = ?," & _
                    " CodiceAnagraficoOreCreditoDebito = ?," & _
                    " CodiceAnagraficoQualifica = ?," & _
                    " CodiceAnagraficoTurno = ?," & _
                    " CodiceAnagraficoCartellino = ?," & _
                    " CodiceAnagraficoContratto = ?," & _
                    " CodiceAnagraficoFlessibilita = ?," & _
                    " CodiceAnagraficoSelezione = ?," & _
                    " CodiceAnagraficoMatricola = ?," & _
                    " CodiceAnagraficoFasceOrarie = ?," & _
                    " CodiceAnagraficoPosizioneINAIL = ?," & _
                    " CodiceAnagraficoNumeroRientri = ?," & _
                    " CodiceAnagraficoPiantaOrganica = ?," & _
                    " CodiceAnagraficoEquipaggio = ?," & _
                    " CodiceAnagraficoTurnoFestivo = ?," & _
                    " CodiceAnagraficoTestaDatiFissi = ?," & _
                    " CodiceAnagraficoVisualizzazione1 = ?," & _
                    " CodiceAnagraficoVisualizzazione2 = ?," & _
                    " CodiceAnagraficoVisualizzazione3 = ?," & _
                    " CodiceAnagraficoTesseraRiconoscimento = ?," & _
                    " CodiceAnagraficoProfiloFerie = ?," & _
                    " GiustificativoOrarioLavoro = ?," & _
                    " GiustificativoNotte = ?," & _
                    " GiustificativoSmontoNotte = ?," & _
                    " GiustificativoRiposo = ?," & _
                    " GiustificativoAssenza = ?," & _
                    " GiustificativoSostituzione = ?," & _
                    " GiustificativoEstensioneOrario = ?," & _
                    " GiustificativoEntrataPosticipata = ?," & _
                    " GiustificativoUscitaAnticipata = ?," & _
                    " GiustificativoEntrataAnticipata = ?," & _
                    " GiustificativoUscitaPosticipata = ?," & _
                    " TempoOrarioLavoro = ?," & _
                    " TempoAssenza = ?," & _
                    " TempoSostituzione = ?," & _
                    " TempoEntrataPosticipata = ?," & _
                    " TempoUscitaAnticipata = ?," & _
                    " TempoEntrataAnticipata = ?," & _
                    " TempoUscitaPosticipata = ?," & _
                    " FormulaCalcoloInizioGiorno = ?," & _
                    " FormulaCalcoloFineGiorno = ?," & _
                    " FormulaCalcoloSettimana = ?," & _
                    " FormulaCalcoloInizioMese = ?," & _
                    " FormulaCalcoloFineMese = ?," & _
                    " FormulaCalcoloDiurno = ?," & _
                    " FormulaCalcoloNotturno = ?," & _
                    " FormulaCalcoloFestivoDiurno = ?," & _
                    " FormulaCalcoloFestivoNotturno = ?," & _
                    " GiornoFineSettimana = ?," & _
                    " ChiusuraAnagraficaAnno = ?," & _
                    " ChiusuraAnagraficaMese = ?," & _
                    " ChiusuraAnagraficaAnnoDaCalcolo = ?," & _
                    " ChiusuraAnagraficaMeseDaCalcolo = ?," & _
                    " FormulaCalcoloPausa = ?," & _
                    " CodiceDipendenteXCollegamentoRetribuzioni = ?," & _
                    " CodiceDipendenteXCollegamentoePersonam = ?," & _
                    " StampaTempoWork_1 = ?," & _
                    " StampaTempoWork_2 = ?," & _
                    " StampaTempoPrevisto = ?," & _
                    " StampaPausa = ?," & _
                    " StampaTempoStandard = ?," & _
                    " StampaTempoGiorno = ?," & _
                    " StampaDiurno = ?," & _
                    " StampaNotturno = ?," & _
                    " StampaFestivoDiurno = ?," & _
                    " StampaFestivoNotturno = ?," & _
                    " CodiceGruppoDatiVariabiliXSelezioni = ?," & _
                    " DataInizioOrdiniServizio = ?," & _
                    " CodiceAnagraficoIndennitaTurno = ?," & _
                    " CodiceAnagraficoLivelloRetributivo = ?," & _
                    " CodiceAnagraficoMatricolaArma = ?," & _
                    " CodiceAnagraficoOrdinanzaArma = ?," & _
                    " CodiceGruppoDatiVariabiliMPCS = ?," & _
                    " ConfermaObbligatoria = ?," & _
                    " GiorniConfermaOrdiniServizio = ?," & _
                    " CodiceAnagraficoTurnoAB = ?," & _
                    " CodiceGruppoDatiVariabiliXServiziComandati = ?," & _
                    " GiustificativoPartime = ?," & _
                    " GiustificativoAssenzaPerStraordinario = ?," & _
                    " GiustificativoStraordinarioPagamento = ?," & _
                    " GiustificativoStraordinarioRecupero = ?," & _
                    " CreaUtenteSuPassword = ?," & _
                    " CodiceAnagraficoIndirizzoPostaElettronica = ?," & _
                    " FasciaUnoDalle = ?," & _
                    " FasciaDueDalle = ?," & _
                    " FasciaTreDalle = ?," & _
                    " FasciaQuattroDalle = ?," & _
                    " FormulaCalcoloMese = ?," & _
                    " ProfiliOrariStrutturati = ?," & _
                    " CodiceAnagraficoGiorniPresenzaEffettiva = ?," & _
                    " GestioneOrariManuali = ?," & _
                    " IndirizziDiPostaPerCompleanni = ?," & _
                    " IndirizziDiPostaPerTimbrature = ?," & _
                    " ServerDiPostaElettronica = ?," & _
                    " AbilitazioniDellaProcedura = ?," & _
                    " CodiceContrattoDefault = ?," & _
                    " AssegnaRiposoFestivo = ?," & _
                    " TrattaOrarioPausaAutomatico = ?," & _
                    " CodiceOrarioFerie = ?," & _
                    " CodiceAnagraficoStrutturaNotti = ?," & _
                    " CodiceAnagraficoContatoreFerie = ?," & _
                    " CodiceAnagraficoContatorePermessi = ?," & _
                    " CodiceAnagraficoPrioritaNellaProposta = ?," & _
                    " CodiceAnagraficoNotteSenzaRiposo = ?," & _
                    " CodiceAnagraficoUnoLibero = ?," & _
                    " CodiceAnagraficoRiposoAlternato = ?," & _
                    " CodiceAnagraficoRiposoDoppio = ?," & _
                    " CodiceAnagraficoRiposoMassimo = ?," & _
                    " TempoTraOrari = ?," & _
                    " QuantiOccupati = ?," & _
                    " CodiceAnagraficoGiorniEffettivaPresenza = ?," & _
                    " CodiceAnagraficoTraDueRiposi = ?," & _
                    " TempoPresenzeConsecutive = ?"


            Dim cmdw As New OleDbCommand()
            cmdw.CommandText = MySql
            cmdw.Parameters.AddWithValue("@Utente", Utente)
            cmdw.Parameters.AddWithValue("@DataAggiornamento", Now)
            cmdw.Parameters.AddWithValue("@CodiceAnagraficoGruppo", CodiceAnagraficoGruppo)
            cmdw.Parameters.AddWithValue("@CodiceAnagraficoSuperGruppo", CodiceAnagraficoSuperGruppo)
            cmdw.Parameters.AddWithValue("@CodiceAnagraficoDitta", CodiceAnagraficoDitta)
            cmdw.Parameters.AddWithValue("@CodiceAnagraficoOreCreditoDebito", CodiceAnagraficoOreCreditoDebito)
            cmdw.Parameters.AddWithValue("@CodiceAnagraficoQualifica", CodiceAnagraficoQualifica)
            cmdw.Parameters.AddWithValue("@CodiceAnagraficoTurno", CodiceAnagraficoTurno)
            cmdw.Parameters.AddWithValue("@CodiceAnagraficoCartellino", CodiceAnagraficoCartellino)
            cmdw.Parameters.AddWithValue("@CodiceAnagraficoContratto", CodiceAnagraficoContratto)
            cmdw.Parameters.AddWithValue("@CodiceAnagraficoFlessibilita", CodiceAnagraficoFlessibilita)
            cmdw.Parameters.AddWithValue("@CodiceAnagraficoSelezione", CodiceAnagraficoSelezione)
            cmdw.Parameters.AddWithValue("@CodiceAnagraficoMatricola", CodiceAnagraficoMatricola)
            cmdw.Parameters.AddWithValue("@CodiceAnagraficoFasceOrarie", CodiceAnagraficoFasceOrarie)
            cmdw.Parameters.AddWithValue("@CodiceAnagraficoPosizioneINAIL", CodiceAnagraficoPosizioneINAIL)
            cmdw.Parameters.AddWithValue("@CodiceAnagraficoNumeroRientri", CodiceAnagraficoNumeroRientri)
            cmdw.Parameters.AddWithValue("@CodiceAnagraficoPiantaOrganica", CodiceAnagraficoPiantaOrganica)
            cmdw.Parameters.AddWithValue("@CodiceAnagraficoEquipaggio", CodiceAnagraficoEquipaggio)
            cmdw.Parameters.AddWithValue("@CodiceAnagraficoTurnoFestivo", CodiceAnagraficoTurnoFestivo)
            cmdw.Parameters.AddWithValue("@CodiceAnagraficoTestaDatiFissi", CodiceAnagraficoTestaDatiFissi)
            cmdw.Parameters.AddWithValue("@CodiceAnagraficoVisualizzazione1", CodiceAnagraficoVisualizzazione1)
            cmdw.Parameters.AddWithValue("@CodiceAnagraficoVisualizzazione2", CodiceAnagraficoVisualizzazione2)
            cmdw.Parameters.AddWithValue("@CodiceAnagraficoVisualizzazione3", CodiceAnagraficoVisualizzazione3)
            cmdw.Parameters.AddWithValue("@CodiceAnagraficoTesseraRiconoscimento", CodiceAnagraficoTesseraRiconoscimento)
            cmdw.Parameters.AddWithValue("@CodiceAnagraficoProfiloFerie", CodiceAnagraficoProfiloFerie)
            cmdw.Parameters.AddWithValue("@GiustificativoOrarioLavoro", GiustificativoOrarioLavoro)
            cmdw.Parameters.AddWithValue("@GiustificativoNotte", GiustificativoNotte)
            cmdw.Parameters.AddWithValue("@GiustificativoSmontoNotte", GiustificativoSmontoNotte)
            cmdw.Parameters.AddWithValue("@GiustificativoRiposo", GiustificativoRiposo)
            cmdw.Parameters.AddWithValue("@GiustificativoAssenza", GiustificativoAssenza)
            cmdw.Parameters.AddWithValue("@GiustificativoSostituzione", GiustificativoSostituzione)
            cmdw.Parameters.AddWithValue("@GiustificativoEstensioneOrario", GiustificativoEstensioneOrario)
            cmdw.Parameters.AddWithValue("@GiustificativoEntrataPosticipata", GiustificativoEntrataPosticipata)
            cmdw.Parameters.AddWithValue("@GiustificativoUscitaAnticipata", GiustificativoUscitaAnticipata)
            cmdw.Parameters.AddWithValue("@GiustificativoEntrataAnticipata", GiustificativoEntrataAnticipata)
            cmdw.Parameters.AddWithValue("@GiustificativoUscitaPosticipata", GiustificativoUscitaPosticipata)
            cmdw.Parameters.AddWithValue("@TempoOrarioLavoro", TempoOrarioLavoro)
            cmdw.Parameters.AddWithValue("@TempoAssenza", TempoAssenza)
            cmdw.Parameters.AddWithValue("@TempoSostituzione", TempoSostituzione)
            cmdw.Parameters.AddWithValue("@TempoEntrataPosticipata", TempoEntrataPosticipata)
            cmdw.Parameters.AddWithValue("@TempoUscitaAnticipata", TempoUscitaAnticipata)
            cmdw.Parameters.AddWithValue("@TempoEntrataAnticipata", TempoEntrataAnticipata)
            cmdw.Parameters.AddWithValue("@TempoUscitaPosticipata", TempoUscitaPosticipata)
            cmdw.Parameters.AddWithValue("@FormulaCalcoloInizioGiorno", FormulaCalcoloInizioGiorno)
            cmdw.Parameters.AddWithValue("@FormulaCalcoloFineGiorno", FormulaCalcoloFineGiorno)
            cmdw.Parameters.AddWithValue("@FormulaCalcoloSettimana", FormulaCalcoloSettimana)
            cmdw.Parameters.AddWithValue("@FormulaCalcoloInizioMese", FormulaCalcoloInizioMese)
            cmdw.Parameters.AddWithValue("@FormulaCalcoloFineMese", FormulaCalcoloFineMese)
            cmdw.Parameters.AddWithValue("@FormulaCalcoloDiurno", FormulaCalcoloDiurno)
            cmdw.Parameters.AddWithValue("@FormulaCalcoloNotturno", FormulaCalcoloNotturno)
            cmdw.Parameters.AddWithValue("@FormulaCalcoloFestivoDiurno", FormulaCalcoloFestivoDiurno)
            cmdw.Parameters.AddWithValue("@FormulaCalcoloFestivoNotturno", FormulaCalcoloFestivoNotturno)
            cmdw.Parameters.AddWithValue("@GiornoFineSettimana", GiornoFineSettimana)
            cmdw.Parameters.AddWithValue("@ChiusuraAnagraficaAnno", ChiusuraAnagraficaAnno)
            cmdw.Parameters.AddWithValue("@ChiusuraAnagraficaMese", ChiusuraAnagraficaMese)
            cmdw.Parameters.AddWithValue("@ChiusuraAnagraficaAnnoDaCalcolo", ChiusuraAnagraficaAnnoDaCalcolo)
            cmdw.Parameters.AddWithValue("@ChiusuraAnagraficaMeseDaCalcolo", ChiusuraAnagraficaMeseDaCalcolo)
            cmdw.Parameters.AddWithValue("@FormulaCalcoloPausa", FormulaCalcoloPausa)
            cmdw.Parameters.AddWithValue("@CodiceDipendenteXCollegamentoRetribuzioni", CodiceDipendenteXCollegamentoRetribuzioni)
            cmdw.Parameters.AddWithValue("@CodiceDipendenteXCollegamentoePersonam", CodiceDipendenteXCollegamentoePersonam)
            cmdw.Parameters.AddWithValue("@StampaTempoWork_1", StampaTempoWork_1)
            cmdw.Parameters.AddWithValue("@StampaTempoWork_2", StampaTempoWork_2)
            cmdw.Parameters.AddWithValue("@StampaTempoPrevisto", StampaTempoPrevisto)
            cmdw.Parameters.AddWithValue("@StampaPausa", StampaPausa)
            cmdw.Parameters.AddWithValue("@StampaTempoStandard", StampaTempoStandard)
            cmdw.Parameters.AddWithValue("@StampaTempoGiorno", StampaTempoGiorno)
            cmdw.Parameters.AddWithValue("@StampaDiurno", StampaDiurno)
            cmdw.Parameters.AddWithValue("@StampaNotturno", StampaNotturno)
            cmdw.Parameters.AddWithValue("@StampaFestivoDiurno", StampaFestivoDiurno)
            cmdw.Parameters.AddWithValue("@StampaFestivoNotturno", StampaFestivoNotturno)
            cmdw.Parameters.AddWithValue("@CodiceGruppoDatiVariabiliXSelezioni", CodiceGruppoDatiVariabiliXSelezioni)
            cmdw.Parameters.AddWithValue("@DataInizioOrdiniServizio", DataInizioOrdiniServizio)
            cmdw.Parameters.AddWithValue("@CodiceAnagraficoIndennitaTurno", CodiceAnagraficoIndennitaTurno)
            cmdw.Parameters.AddWithValue("@CodiceAnagraficoLivelloRetributivo", CodiceAnagraficoLivelloRetributivo)
            cmdw.Parameters.AddWithValue("@CodiceAnagraficoMatricolaArma", CodiceAnagraficoMatricolaArma)
            cmdw.Parameters.AddWithValue("@CodiceAnagraficoOrdinanzaArma", CodiceAnagraficoOrdinanzaArma)
            cmdw.Parameters.AddWithValue("@CodiceGruppoDatiVariabiliMPCS", CodiceGruppoDatiVariabiliMPCS)
            cmdw.Parameters.AddWithValue("@ConfermaObbligatoria", ConfermaObbligatoria)
            cmdw.Parameters.AddWithValue("@GiorniConfermaOrdiniServizio", GiorniConfermaOrdiniServizio)
            cmdw.Parameters.AddWithValue("@CodiceAnagraficoTurnoAB", CodiceAnagraficoTurnoAB)
            cmdw.Parameters.AddWithValue("@CodiceGruppoDatiVariabiliXServiziComandati", CodiceGruppoDatiVariabiliXServiziComandati)
            cmdw.Parameters.AddWithValue("@GiustificativoPartime", GiustificativoPartime)
            cmdw.Parameters.AddWithValue("@GiustificativoAssenzaPerStraordinario", GiustificativoAssenzaPerStraordinario)
            cmdw.Parameters.AddWithValue("@GiustificativoStraordinarioPagamento", GiustificativoStraordinarioPagamento)
            cmdw.Parameters.AddWithValue("@GiustificativoStraordinarioRecupero", GiustificativoStraordinarioRecupero)
            cmdw.Parameters.AddWithValue("@CreaUtenteSuPassword", CreaUtenteSuPassword)
            cmdw.Parameters.AddWithValue("@CodiceAnagraficoIndirizzoPostaElettronica", CodiceAnagraficoIndirizzoPostaElettronica)
            cmdw.Parameters.AddWithValue("@FasciaUnoDalle", FasciaUnoDalle)
            cmdw.Parameters.AddWithValue("@FasciaDueDalle", FasciaDueDalle)
            cmdw.Parameters.AddWithValue("@FasciaTreDalle", FasciaTreDalle)
            cmdw.Parameters.AddWithValue("@FasciaQuattroDalle", FasciaQuattroDalle)
            cmdw.Parameters.AddWithValue("@FormulaCalcoloMese", FormulaCalcoloMese)
            cmdw.Parameters.AddWithValue("@ProfiliOrariStrutturati", ProfiliOrariStrutturati)
            cmdw.Parameters.AddWithValue("@CodiceAnagraficoGiorniPresenzaEffettiva", CodiceAnagraficoGiorniPresenzaEffettiva)
            cmdw.Parameters.AddWithValue("@GestioneOrariManuali", GestioneOrariManuali)
            cmdw.Parameters.AddWithValue("@IndirizziDiPostaPerCompleanni", IndirizziDiPostaPerCompleanni)
            cmdw.Parameters.AddWithValue("@IndirizziDiPostaPerTimbrature", IndirizziDiPostaPerTimbrature)
            cmdw.Parameters.AddWithValue("@ServerDiPostaElettronica", ServerDiPostaElettronica)
            cmdw.Parameters.AddWithValue("@AbilitazioniDellaProcedura", AbilitazioniDellaProcedura)
            cmdw.Parameters.AddWithValue("@CodiceContrattoDefault", CodiceContrattoDefault)
            cmdw.Parameters.AddWithValue("@AssegnaRiposoFestivo", AssegnaRiposoFestivo)
            cmdw.Parameters.AddWithValue("@TrattaOrarioPausaAutomatico", TrattaOrarioPausaAutomatico)
            cmdw.Parameters.AddWithValue("@CodiceOrarioFerie", CodiceOrarioFerie)
            cmdw.Parameters.AddWithValue("@CodiceAnagraficoStrutturaNotti", CodiceAnagraficoStrutturaNotti)
            cmdw.Parameters.AddWithValue("@CodiceAnagraficoContatoreFerie", CodiceAnagraficoContatoreFerie)
            cmdw.Parameters.AddWithValue("@CodiceAnagraficoContatorePermessi", CodiceAnagraficoContatorePermessi)
            cmdw.Parameters.AddWithValue("@CodiceAnagraficoPrioritaNellaProposta", CodiceAnagraficoPrioritaNellaProposta)
            cmdw.Parameters.AddWithValue("@CodiceAnagraficoNotteSenzaRiposo", CodiceAnagraficoNotteSenzaRiposo)
            cmdw.Parameters.AddWithValue("@CodiceAnagraficoUnoLibero", CodiceAnagraficoUnoLibero)
            cmdw.Parameters.AddWithValue("@CodiceAnagraficoRiposoAlternato", CodiceAnagraficoRiposoAlternato)
            cmdw.Parameters.AddWithValue("@CodiceAnagraficoRiposoDoppio", CodiceAnagraficoRiposoDoppio)
            cmdw.Parameters.AddWithValue("@CodiceAnagraficoRiposoMassimo", CodiceAnagraficoRiposoMassimo)
            cmdw.Parameters.AddWithValue("@TempoTraOrari", TempoTraOrari)
            cmdw.Parameters.AddWithValue("@QuantiOccupati", QuantiOccupati)
            cmdw.Parameters.AddWithValue("@CodiceAnagraficoGiorniEffettivaPresenza", CodiceAnagraficoGiorniEffettivaPresenza)
            cmdw.Parameters.AddWithValue("@CodiceAnagraficoTraDueRiposi", CodiceAnagraficoTraDueRiposi)
            cmdw.Parameters.AddWithValue("@TempoPresenzeConsecutive", TempoPresenzeConsecutive)
            cmdw.Connection = cn
            cmdw.ExecuteNonQuery()
        Else
            MySql = "INSERT INTO Parametri (" & _
                    " Utente," & _
                    " DataAggiornamento," & _
                    " CodiceAnagraficoGruppo," & _
                    " CodiceAnagraficoSuperGruppo," & _
                    " CodiceAnagraficoDitta," & _
                    " CodiceAnagraficoOreCreditoDebito," & _
                    " CodiceAnagraficoQualifica," & _
                    " CodiceAnagraficoTurno," & _
                    " CodiceAnagraficoCartellino," & _
                    " CodiceAnagraficoContratto," & _
                    " CodiceAnagraficoFlessibilita," & _
                    " CodiceAnagraficoSelezione," & _
                    " CodiceAnagraficoMatricola," & _
                    " CodiceAnagraficoFasceOrarie," & _
                    " CodiceAnagraficoPosizioneINAIL," & _
                    " CodiceAnagraficoNumeroRientri," & _
                    " CodiceAnagraficoPiantaOrganica," & _
                    " CodiceAnagraficoEquipaggio," & _
                    " CodiceAnagraficoTurnoFestivo," & _
                    " CodiceAnagraficoTestaDatiFissi," & _
                    " CodiceAnagraficoVisualizzazione1," & _
                    " CodiceAnagraficoVisualizzazione2," & _
                    " CodiceAnagraficoVisualizzazione3," & _
                    " CodiceAnagraficoTesseraRiconoscimento," & _
                    " CodiceAnagraficoProfiloFerie," & _
                    " GiustificativoOrarioLavoro," & _
                    " GiustificativoNotte," & _
                    " GiustificativoSmontoNotte," & _
                    " GiustificativoRiposo," & _
                    " GiustificativoAssenza," & _
                    " GiustificativoSostituzione," & _
                    " GiustificativoEstensioneOrario," & _
                    " GiustificativoEntrataPosticipata," & _
                    " GiustificativoUscitaAnticipata," & _
                    " GiustificativoEntrataAnticipata," & _
                    " GiustificativoUscitaPosticipata," & _
                    " TempoOrarioLavoro," & _
                    " TempoAssenza," & _
                    " TempoSostituzione," & _
                    " TempoEntrataPosticipata," & _
                    " TempoUscitaAnticipata," & _
                    " TempoEntrataAnticipata," & _
                    " TempoUscitaPosticipata," & _
                    " FormulaCalcoloInizioGiorno," & _
                    " FormulaCalcoloFineGiorno," & _
                    " FormulaCalcoloSettimana," & _
                    " FormulaCalcoloInizioMese," & _
                    " FormulaCalcoloFineMese," & _
                    " FormulaCalcoloDiurno," & _
                    " FormulaCalcoloNotturno," & _
                    " FormulaCalcoloFestivoDiurno," & _
                    " FormulaCalcoloFestivoNotturno," & _
                    " GiornoFineSettimana," & _
                    " ChiusuraAnagraficaAnno," & _
                    " ChiusuraAnagraficaMese," & _
                    " ChiusuraAnagraficaAnnoDaCalcolo," & _
                    " ChiusuraAnagraficaMeseDaCalcolo," & _
                    " FormulaCalcoloPausa," & _
                    " CodiceDipendenteXCollegamentoRetribuzioni," & _
                    " CodiceDipendenteXCollegamentoePersonam," & _
                    " StampaTempoWork_1," & _
                    " StampaTempoWork_2," & _
                    " StampaTempoPrevisto," & _
                    " StampaPausa," & _
                    " StampaTempoStandard," & _
                    " StampaTempoGiorno," & _
                    " StampaDiurno," & _
                    " StampaNotturno," & _
                    " StampaFestivoDiurno," & _
                    " StampaFestivoNotturno," & _
                    " CodiceGruppoDatiVariabiliXSelezioni," & _
                    " DataInizioOrdiniServizio," & _
                    " CodiceAnagraficoIndennitaTurno," & _
                    " CodiceAnagraficoLivelloRetributivo," & _
                    " CodiceAnagraficoMatricolaArma," & _
                    " CodiceAnagraficoOrdinanzaArma," & _
                    " CodiceGruppoDatiVariabiliMPCS," & _
                    " ConfermaObbligatoria," & _
                    " GiorniConfermaOrdiniServizio," & _
                    " CodiceAnagraficoTurnoAB," & _
                    " CodiceGruppoDatiVariabiliXServiziComandati," & _
                    " GiustificativoPartime," & _
                    " GiustificativoAssenzaPerStraordinario," & _
                    " GiustificativoStraordinarioPagamento," & _
                    " GiustificativoStraordinarioRecupero," & _
                    " CreaUtenteSuPassword," & _
                    " CodiceAnagraficoIndirizzoPostaElettronica," & _
                    " FasciaUnoDalle," & _
                    " FasciaDueDalle," & _
                    " FasciaTreDalle," & _
                    " FasciaQuattroDalle," & _
                    " FormulaCalcoloMese," & _
                    " ProfiliOrariStrutturati," & _
                    " CodiceAnagraficoGiorniPresenzaEffettiva," & _
                    " GestioneOrariManuali," & _
                    " IndirizziDiPostaPerCompleanni," & _
                    " IndirizziDiPostaPerTimbrature," & _
                    " ServerDiPostaElettronica," & _
                    " AbilitazioniDellaProcedura," & _
                    " CodiceContrattoDefault," & _
                    " AssegnaRiposoFestivo," & _
                    " TrattaOrarioPausaAutomatico," & _
                    " CodiceOrarioFerie," & _
                    " CodiceAnagraficoStrutturaNotti," & _
                    " CodiceAnagraficoContatoreFerie," & _
                    " CodiceAnagraficoContatorePermessi," & _
                    " CodiceAnagraficoPrioritaNellaProposta," & _
                    " CodiceAnagraficoNotteSenzaRiposo," & _
                    " CodiceAnagraficoUnoLibero," & _
                    " CodiceAnagraficoRiposoAlternato," & _
                    " CodiceAnagraficoRiposoDoppio," & _
                    " CodiceAnagraficoRiposoMassimo," & _
                    " TempoTraOrari," & _
                    " QuantiOccupati," & _
                    " CodiceAnagraficoGiorniEffettivaPresenza," & _
                    " CodiceAnagraficoTraDueRiposi," & _
                    " TempoPresenzeConsecutive)" & _
                    " VALUES ?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)"

            Dim cmdw As New OleDbCommand()
            cmdw.CommandText = MySql
            cmdw.Parameters.AddWithValue("@Utente", Utente)
            cmdw.Parameters.AddWithValue("@DataAggiornamento", Now)
            cmdw.Parameters.AddWithValue("@CodiceAnagraficoGruppo", CodiceAnagraficoGruppo)
            cmdw.Parameters.AddWithValue("@CodiceAnagraficoSuperGruppo", CodiceAnagraficoSuperGruppo)
            cmdw.Parameters.AddWithValue("@CodiceAnagraficoDitta", CodiceAnagraficoDitta)
            cmdw.Parameters.AddWithValue("@CodiceAnagraficoOreCreditoDebito", CodiceAnagraficoOreCreditoDebito)
            cmdw.Parameters.AddWithValue("@CodiceAnagraficoQualifica", CodiceAnagraficoQualifica)
            cmdw.Parameters.AddWithValue("@CodiceAnagraficoTurno", CodiceAnagraficoTurno)
            cmdw.Parameters.AddWithValue("@CodiceAnagraficoCartellino", CodiceAnagraficoCartellino)
            cmdw.Parameters.AddWithValue("@CodiceAnagraficoContratto", CodiceAnagraficoContratto)
            cmdw.Parameters.AddWithValue("@CodiceAnagraficoFlessibilita", CodiceAnagraficoFlessibilita)
            cmdw.Parameters.AddWithValue("@CodiceAnagraficoSelezione", CodiceAnagraficoSelezione)
            cmdw.Parameters.AddWithValue("@CodiceAnagraficoMatricola", CodiceAnagraficoMatricola)
            cmdw.Parameters.AddWithValue("@CodiceAnagraficoFasceOrarie", CodiceAnagraficoFasceOrarie)
            cmdw.Parameters.AddWithValue("@CodiceAnagraficoPosizioneINAIL", CodiceAnagraficoPosizioneINAIL)
            cmdw.Parameters.AddWithValue("@CodiceAnagraficoNumeroRientri", CodiceAnagraficoNumeroRientri)
            cmdw.Parameters.AddWithValue("@CodiceAnagraficoPiantaOrganica", CodiceAnagraficoPiantaOrganica)
            cmdw.Parameters.AddWithValue("@CodiceAnagraficoEquipaggio", CodiceAnagraficoEquipaggio)
            cmdw.Parameters.AddWithValue("@CodiceAnagraficoTurnoFestivo", CodiceAnagraficoTurnoFestivo)
            cmdw.Parameters.AddWithValue("@CodiceAnagraficoTestaDatiFissi", CodiceAnagraficoTestaDatiFissi)
            cmdw.Parameters.AddWithValue("@CodiceAnagraficoVisualizzazione1", CodiceAnagraficoVisualizzazione1)
            cmdw.Parameters.AddWithValue("@CodiceAnagraficoVisualizzazione2", CodiceAnagraficoVisualizzazione2)
            cmdw.Parameters.AddWithValue("@CodiceAnagraficoVisualizzazione3", CodiceAnagraficoVisualizzazione3)
            cmdw.Parameters.AddWithValue("@CodiceAnagraficoTesseraRiconoscimento", CodiceAnagraficoTesseraRiconoscimento)
            cmdw.Parameters.AddWithValue("@CodiceAnagraficoProfiloFerie", CodiceAnagraficoProfiloFerie)
            cmdw.Parameters.AddWithValue("@GiustificativoOrarioLavoro", GiustificativoOrarioLavoro)
            cmdw.Parameters.AddWithValue("@GiustificativoNotte", GiustificativoNotte)
            cmdw.Parameters.AddWithValue("@GiustificativoSmontoNotte", GiustificativoSmontoNotte)
            cmdw.Parameters.AddWithValue("@GiustificativoRiposo", GiustificativoRiposo)
            cmdw.Parameters.AddWithValue("@GiustificativoAssenza", GiustificativoAssenza)
            cmdw.Parameters.AddWithValue("@GiustificativoSostituzione", GiustificativoSostituzione)
            cmdw.Parameters.AddWithValue("@GiustificativoEstensioneOrario", GiustificativoEstensioneOrario)
            cmdw.Parameters.AddWithValue("@GiustificativoEntrataPosticipata", GiustificativoEntrataPosticipata)
            cmdw.Parameters.AddWithValue("@GiustificativoUscitaAnticipata", GiustificativoUscitaAnticipata)
            cmdw.Parameters.AddWithValue("@GiustificativoEntrataAnticipata", GiustificativoEntrataAnticipata)
            cmdw.Parameters.AddWithValue("@GiustificativoUscitaPosticipata", GiustificativoUscitaPosticipata)
            cmdw.Parameters.AddWithValue("@TempoOrarioLavoro", TempoOrarioLavoro)
            cmdw.Parameters.AddWithValue("@TempoAssenza", TempoAssenza)
            cmdw.Parameters.AddWithValue("@TempoSostituzione", TempoSostituzione)
            cmdw.Parameters.AddWithValue("@TempoEntrataPosticipata", TempoEntrataPosticipata)
            cmdw.Parameters.AddWithValue("@TempoUscitaAnticipata", TempoUscitaAnticipata)
            cmdw.Parameters.AddWithValue("@TempoEntrataAnticipata", TempoEntrataAnticipata)
            cmdw.Parameters.AddWithValue("@TempoUscitaPosticipata", TempoUscitaPosticipata)
            cmdw.Parameters.AddWithValue("@FormulaCalcoloInizioGiorno", FormulaCalcoloInizioGiorno)
            cmdw.Parameters.AddWithValue("@FormulaCalcoloFineGiorno", FormulaCalcoloFineGiorno)
            cmdw.Parameters.AddWithValue("@FormulaCalcoloSettimana", FormulaCalcoloSettimana)
            cmdw.Parameters.AddWithValue("@FormulaCalcoloInizioMese", FormulaCalcoloInizioMese)
            cmdw.Parameters.AddWithValue("@FormulaCalcoloFineMese", FormulaCalcoloFineMese)
            cmdw.Parameters.AddWithValue("@FormulaCalcoloDiurno", FormulaCalcoloDiurno)
            cmdw.Parameters.AddWithValue("@FormulaCalcoloNotturno", FormulaCalcoloNotturno)
            cmdw.Parameters.AddWithValue("@FormulaCalcoloFestivoDiurno", FormulaCalcoloFestivoDiurno)
            cmdw.Parameters.AddWithValue("@FormulaCalcoloFestivoNotturno", FormulaCalcoloFestivoNotturno)
            cmdw.Parameters.AddWithValue("@GiornoFineSettimana", GiornoFineSettimana)
            cmdw.Parameters.AddWithValue("@ChiusuraAnagraficaAnno", ChiusuraAnagraficaAnno)
            cmdw.Parameters.AddWithValue("@ChiusuraAnagraficaMese", ChiusuraAnagraficaMese)
            cmdw.Parameters.AddWithValue("@ChiusuraAnagraficaAnnoDaCalcolo", ChiusuraAnagraficaAnnoDaCalcolo)
            cmdw.Parameters.AddWithValue("@ChiusuraAnagraficaMeseDaCalcolo", ChiusuraAnagraficaMeseDaCalcolo)
            cmdw.Parameters.AddWithValue("@FormulaCalcoloPausa", FormulaCalcoloPausa)
            cmdw.Parameters.AddWithValue("@CodiceDipendenteXCollegamentoRetribuzioni", CodiceDipendenteXCollegamentoRetribuzioni)
            cmdw.Parameters.AddWithValue("@CodiceDipendenteXCollegamentoePersonam", CodiceDipendenteXCollegamentoePersonam)
            cmdw.Parameters.AddWithValue("@StampaTempoWork_1", StampaTempoWork_1)
            cmdw.Parameters.AddWithValue("@StampaTempoWork_2", StampaTempoWork_2)
            cmdw.Parameters.AddWithValue("@StampaTempoPrevisto", StampaTempoPrevisto)
            cmdw.Parameters.AddWithValue("@StampaPausa", StampaPausa)
            cmdw.Parameters.AddWithValue("@StampaTempoStandard", StampaTempoStandard)
            cmdw.Parameters.AddWithValue("@StampaTempoGiorno", StampaTempoGiorno)
            cmdw.Parameters.AddWithValue("@StampaDiurno", StampaDiurno)
            cmdw.Parameters.AddWithValue("@StampaNotturno", StampaNotturno)
            cmdw.Parameters.AddWithValue("@StampaFestivoDiurno", StampaFestivoDiurno)
            cmdw.Parameters.AddWithValue("@StampaFestivoNotturno", StampaFestivoNotturno)
            cmdw.Parameters.AddWithValue("@CodiceGruppoDatiVariabiliXSelezioni", CodiceGruppoDatiVariabiliXSelezioni)
            cmdw.Parameters.AddWithValue("@DataInizioOrdiniServizio", DataInizioOrdiniServizio)
            cmdw.Parameters.AddWithValue("@CodiceAnagraficoIndennitaTurno", CodiceAnagraficoIndennitaTurno)
            cmdw.Parameters.AddWithValue("@CodiceAnagraficoLivelloRetributivo", CodiceAnagraficoLivelloRetributivo)
            cmdw.Parameters.AddWithValue("@CodiceAnagraficoMatricolaArma", CodiceAnagraficoMatricolaArma)
            cmdw.Parameters.AddWithValue("@CodiceAnagraficoOrdinanzaArma", CodiceAnagraficoOrdinanzaArma)
            cmdw.Parameters.AddWithValue("@CodiceGruppoDatiVariabiliMPCS", CodiceGruppoDatiVariabiliMPCS)
            cmdw.Parameters.AddWithValue("@ConfermaObbligatoria", ConfermaObbligatoria)
            cmdw.Parameters.AddWithValue("@GiorniConfermaOrdiniServizio", GiorniConfermaOrdiniServizio)
            cmdw.Parameters.AddWithValue("@CodiceAnagraficoTurnoAB", CodiceAnagraficoTurnoAB)
            cmdw.Parameters.AddWithValue("@CodiceGruppoDatiVariabiliXServiziComandati", CodiceGruppoDatiVariabiliXServiziComandati)
            cmdw.Parameters.AddWithValue("@GiustificativoPartime", GiustificativoPartime)
            cmdw.Parameters.AddWithValue("@GiustificativoAssenzaPerStraordinario", GiustificativoAssenzaPerStraordinario)
            cmdw.Parameters.AddWithValue("@GiustificativoStraordinarioPagamento", GiustificativoStraordinarioPagamento)
            cmdw.Parameters.AddWithValue("@GiustificativoStraordinarioRecupero", GiustificativoStraordinarioRecupero)
            cmdw.Parameters.AddWithValue("@CreaUtenteSuPassword", CreaUtenteSuPassword)
            cmdw.Parameters.AddWithValue("@CodiceAnagraficoIndirizzoPostaElettronica", CodiceAnagraficoIndirizzoPostaElettronica)
            cmdw.Parameters.AddWithValue("@FasciaUnoDalle", FasciaUnoDalle)
            cmdw.Parameters.AddWithValue("@FasciaDueDalle", FasciaDueDalle)
            cmdw.Parameters.AddWithValue("@FasciaTreDalle", FasciaTreDalle)
            cmdw.Parameters.AddWithValue("@FasciaQuattroDalle", FasciaQuattroDalle)
            cmdw.Parameters.AddWithValue("@FormulaCalcoloMese", FormulaCalcoloMese)
            cmdw.Parameters.AddWithValue("@ProfiliOrariStrutturati", ProfiliOrariStrutturati)
            cmdw.Parameters.AddWithValue("@CodiceAnagraficoGiorniPresenzaEffettiva", CodiceAnagraficoGiorniPresenzaEffettiva)
            cmdw.Parameters.AddWithValue("@GestioneOrariManuali", GestioneOrariManuali)
            cmdw.Parameters.AddWithValue("@IndirizziDiPostaPerCompleanni", IndirizziDiPostaPerCompleanni)
            cmdw.Parameters.AddWithValue("@IndirizziDiPostaPerTimbrature", IndirizziDiPostaPerTimbrature)
            cmdw.Parameters.AddWithValue("@ServerDiPostaElettronica", ServerDiPostaElettronica)
            cmdw.Parameters.AddWithValue("@AbilitazioniDellaProcedura", AbilitazioniDellaProcedura)
            cmdw.Parameters.AddWithValue("@CodiceContrattoDefault", CodiceContrattoDefault)
            cmdw.Parameters.AddWithValue("@AssegnaRiposoFestivo", AssegnaRiposoFestivo)
            cmdw.Parameters.AddWithValue("@TrattaOrarioPausaAutomatico", TrattaOrarioPausaAutomatico)
            cmdw.Parameters.AddWithValue("@CodiceOrarioFerie", CodiceOrarioFerie)
            cmdw.Parameters.AddWithValue("@CodiceAnagraficoStrutturaNotti", CodiceAnagraficoStrutturaNotti)
            cmdw.Parameters.AddWithValue("@CodiceAnagraficoContatoreFerie", CodiceAnagraficoContatoreFerie)
            cmdw.Parameters.AddWithValue("@CodiceAnagraficoContatorePermessi", CodiceAnagraficoContatorePermessi)
            cmdw.Parameters.AddWithValue("@CodiceAnagraficoPrioritaNellaProposta", CodiceAnagraficoPrioritaNellaProposta)
            cmdw.Parameters.AddWithValue("@CodiceAnagraficoNotteSenzaRiposo", CodiceAnagraficoNotteSenzaRiposo)
            cmdw.Parameters.AddWithValue("@CodiceAnagraficoUnoLibero", CodiceAnagraficoUnoLibero)
            cmdw.Parameters.AddWithValue("@CodiceAnagraficoRiposoAlternato", CodiceAnagraficoRiposoAlternato)
            cmdw.Parameters.AddWithValue("@CodiceAnagraficoRiposoDoppio", CodiceAnagraficoRiposoDoppio)
            cmdw.Parameters.AddWithValue("@CodiceAnagraficoRiposoMassimo", CodiceAnagraficoRiposoMassimo)
            cmdw.Parameters.AddWithValue("@TempoTraOrari", TempoTraOrari)
            cmdw.Parameters.AddWithValue("@QuantiOccupati", QuantiOccupati)
            cmdw.Parameters.AddWithValue("@CodiceAnagraficoGiorniEffettivaPresenza", CodiceAnagraficoGiorniEffettivaPresenza)
            cmdw.Parameters.AddWithValue("@CodiceAnagraficoTraDueRiposi", CodiceAnagraficoTraDueRiposi)
            cmdw.Parameters.AddWithValue("@TempoPresenzeConsecutive", TempoPresenzeConsecutive)
            cmdw.Connection = cn
            cmdw.ExecuteNonQuery()
        End If
        myPOSTreader.Close()
        cn.Close()

        Call Pulisci()
    End Sub

    Function StringaDb(ByVal Oggetto As Object) As String
        If IsDBNull(Oggetto) Then
            Return ""
        Else
            Return Oggetto
        End If
    End Function

    Function DataDb(ByVal Oggetto As Object) As Date
        If Not IsDate(Oggetto) Then
            Return Nothing
        Else
            Return Oggetto
        End If
    End Function

    Function NumeroDb(ByVal Oggetto As Object) As Object
        If IsDBNull(Oggetto) Then
            Return 0
        Else
            Return Oggetto
        End If
    End Function

    Sub Leggi(ByVal StringaConnessione As String)
        Dim cn As OleDbConnection

        cn = New Data.OleDb.OleDbConnection(StringaConnessione)

        cn.Open()
        Dim cmd As New OleDbCommand()
        cmd.CommandText = ("Select * From Parametri")
        cmd.Connection = cn

        Dim myPOSTreader As OleDbDataReader = cmd.ExecuteReader()
        If myPOSTreader.Read Then
            Id = StringaDb(myPOSTreader.Item("Id"))
            CodiceAnagraficoGruppo = StringaDb(myPOSTreader.Item("CodiceAnagraficoGruppo"))
            CodiceAnagraficoSuperGruppo = StringaDb(myPOSTreader.Item("CodiceAnagraficoSuperGruppo"))
            CodiceAnagraficoDitta = StringaDb(myPOSTreader.Item("CodiceAnagraficoDitta"))
            CodiceAnagraficoOreCreditoDebito = StringaDb(myPOSTreader.Item("CodiceAnagraficoOreCreditoDebito"))
            CodiceAnagraficoQualifica = StringaDb(myPOSTreader.Item("CodiceAnagraficoQualifica"))
            CodiceAnagraficoTurno = StringaDb(myPOSTreader.Item("CodiceAnagraficoTurno"))
            CodiceAnagraficoCartellino = StringaDb(myPOSTreader.Item("CodiceAnagraficoCartellino"))
            CodiceAnagraficoContratto = StringaDb(myPOSTreader.Item("CodiceAnagraficoContratto"))
            CodiceAnagraficoFlessibilita = StringaDb(myPOSTreader.Item("CodiceAnagraficoFlessibilita"))
            CodiceAnagraficoSelezione = StringaDb(myPOSTreader.Item("CodiceAnagraficoSelezione"))
            CodiceAnagraficoMatricola = StringaDb(myPOSTreader.Item("CodiceAnagraficoMatricola"))
            CodiceAnagraficoFasceOrarie = StringaDb(myPOSTreader.Item("CodiceAnagraficoFasceOrarie"))
            CodiceAnagraficoPosizioneINAIL = StringaDb(myPOSTreader.Item("CodiceAnagraficoPosizioneINAIL"))
            CodiceAnagraficoNumeroRientri = StringaDb(myPOSTreader.Item("CodiceAnagraficoNumeroRientri"))
            CodiceAnagraficoPiantaOrganica = StringaDb(myPOSTreader.Item("CodiceAnagraficoPiantaOrganica"))
            CodiceAnagraficoEquipaggio = StringaDb(myPOSTreader.Item("CodiceAnagraficoEquipaggio"))
            CodiceAnagraficoTurnoFestivo = StringaDb(myPOSTreader.Item("CodiceAnagraficoTurnoFestivo"))
            CodiceAnagraficoTestaDatiFissi = StringaDb(myPOSTreader.Item("CodiceAnagraficoTestaDatiFissi"))
            CodiceAnagraficoVisualizzazione1 = StringaDb(myPOSTreader.Item("CodiceAnagraficoVisualizzazione1"))
            CodiceAnagraficoVisualizzazione2 = StringaDb(myPOSTreader.Item("CodiceAnagraficoVisualizzazione2"))
            CodiceAnagraficoVisualizzazione3 = StringaDb(myPOSTreader.Item("CodiceAnagraficoVisualizzazione3"))
            CodiceAnagraficoTesseraRiconoscimento = StringaDb(myPOSTreader.Item("CodiceAnagraficoTesseraRiconoscimento"))
            CodiceAnagraficoProfiloFerie = StringaDb(myPOSTreader.Item("CodiceAnagraficoProfiloFerie"))
            GiustificativoOrarioLavoro = StringaDb(myPOSTreader.Item("GiustificativoOrarioLavoro"))
            GiustificativoNotte = StringaDb(myPOSTreader.Item("GiustificativoNotte"))
            GiustificativoSmontoNotte = StringaDb(myPOSTreader.Item("GiustificativoSmontoNotte"))
            GiustificativoRiposo = StringaDb(myPOSTreader.Item("GiustificativoRiposo"))
            GiustificativoAssenza = StringaDb(myPOSTreader.Item("GiustificativoAssenza"))
            GiustificativoSostituzione = StringaDb(myPOSTreader.Item("GiustificativoSostituzione"))
            GiustificativoEstensioneOrario = StringaDb(myPOSTreader.Item("GiustificativoEstensioneOrario"))
            GiustificativoEntrataPosticipata = StringaDb(myPOSTreader.Item("GiustificativoEntrataPosticipata"))
            GiustificativoUscitaAnticipata = StringaDb(myPOSTreader.Item("GiustificativoUscitaAnticipata"))
            GiustificativoEntrataAnticipata = StringaDb(myPOSTreader.Item("GiustificativoEntrataAnticipata"))
            GiustificativoUscitaPosticipata = StringaDb(myPOSTreader.Item("GiustificativoUscitaPosticipata"))
            TempoOrarioLavoro = NumeroDb(myPOSTreader.Item("TempoOrarioLavoro"))
            TempoAssenza = NumeroDb(myPOSTreader.Item("TempoAssenza"))
            TempoSostituzione = NumeroDb(myPOSTreader.Item("TempoSostituzione"))
            TempoEntrataPosticipata = NumeroDb(myPOSTreader.Item("TempoEntrataPosticipata"))
            TempoUscitaAnticipata = NumeroDb(myPOSTreader.Item("TempoUscitaAnticipata"))
            TempoEntrataAnticipata = NumeroDb(myPOSTreader.Item("TempoEntrataAnticipata"))
            TempoUscitaPosticipata = NumeroDb(myPOSTreader.Item("TempoUscitaPosticipata"))
            FormulaCalcoloInizioGiorno = StringaDb(myPOSTreader.Item("FormulaCalcoloInizioGiorno"))
            FormulaCalcoloFineGiorno = StringaDb(myPOSTreader.Item("FormulaCalcoloFineGiorno"))
            FormulaCalcoloSettimana = StringaDb(myPOSTreader.Item("FormulaCalcoloSettimana"))
            FormulaCalcoloInizioMese = StringaDb(myPOSTreader.Item("FormulaCalcoloInizioMese"))
            FormulaCalcoloFineMese = StringaDb(myPOSTreader.Item("FormulaCalcoloFineMese"))
            FormulaCalcoloDiurno = StringaDb(myPOSTreader.Item("FormulaCalcoloDiurno"))
            FormulaCalcoloNotturno = StringaDb(myPOSTreader.Item("FormulaCalcoloNotturno"))
            FormulaCalcoloFestivoDiurno = StringaDb(myPOSTreader.Item("FormulaCalcoloFestivoDiurno"))
            FormulaCalcoloFestivoNotturno = StringaDb(myPOSTreader.Item("FormulaCalcoloFestivoNotturno"))
            GiornoFineSettimana = NumeroDb(myPOSTreader.Item("GiornoFineSettimana"))
            ChiusuraAnagraficaAnno = NumeroDb(myPOSTreader.Item("ChiusuraAnagraficaAnno"))
            ChiusuraAnagraficaMese = NumeroDb(myPOSTreader.Item("ChiusuraAnagraficaMese"))
            ChiusuraAnagraficaAnnoDaCalcolo = NumeroDb(myPOSTreader.Item("ChiusuraAnagraficaAnnoDaCalcolo"))
            ChiusuraAnagraficaMeseDaCalcolo = NumeroDb(myPOSTreader.Item("ChiusuraAnagraficaMeseDaCalcolo"))
            FormulaCalcoloPausa = StringaDb(myPOSTreader.Item("FormulaCalcoloPausa"))
            CodiceDipendenteXCollegamentoRetribuzioni = StringaDb(myPOSTreader.Item("CodiceDipendenteXCollegamentoRetribuzioni"))
            CodiceDipendenteXCollegamentoePersonam = StringaDb(myPOSTreader.Item("CodiceDipendenteXCollegamentoePersonam"))
            StampaTempoWork_1 = StringaDb(myPOSTreader.Item("StampaTempoWork_1"))
            StampaTempoWork_2 = StringaDb(myPOSTreader.Item("StampaTempoWork_2"))
            StampaTempoPrevisto = StringaDb(myPOSTreader.Item("StampaTempoPrevisto"))
            StampaPausa = StringaDb(myPOSTreader.Item("StampaPausa"))
            StampaTempoStandard = StringaDb(myPOSTreader.Item("StampaTempoStandard"))
            StampaTempoGiorno = StringaDb(myPOSTreader.Item("StampaTempoGiorno"))
            StampaDiurno = StringaDb(myPOSTreader.Item("StampaDiurno"))
            StampaNotturno = StringaDb(myPOSTreader.Item("StampaNotturno"))
            StampaFestivoDiurno = StringaDb(myPOSTreader.Item("StampaFestivoDiurno"))
            StampaFestivoNotturno = StringaDb(myPOSTreader.Item("StampaFestivoNotturno"))
            CodiceGruppoDatiVariabiliXSelezioni = StringaDb(myPOSTreader.Item("CodiceGruppoDatiVariabiliXSelezioni"))
            DataInizioOrdiniServizio = DataDb(myPOSTreader.Item("DataInizioOrdiniServizio"))
            CodiceAnagraficoIndennitaTurno = StringaDb(myPOSTreader.Item("CodiceAnagraficoIndennitaTurno"))
            CodiceAnagraficoLivelloRetributivo = StringaDb(myPOSTreader.Item("CodiceAnagraficoLivelloRetributivo"))
            CodiceAnagraficoMatricolaArma = StringaDb(myPOSTreader.Item("CodiceAnagraficoMatricolaArma"))
            CodiceAnagraficoOrdinanzaArma = StringaDb(myPOSTreader.Item("CodiceAnagraficoOrdinanzaArma"))
            CodiceGruppoDatiVariabiliMPCS = StringaDb(myPOSTreader.Item("CodiceGruppoDatiVariabiliMPCS"))
            ConfermaObbligatoria = StringaDb(myPOSTreader.Item("ConfermaObbligatoria"))
            GiorniConfermaOrdiniServizio = NumeroDb(myPOSTreader.Item("GiorniConfermaOrdiniServizio"))
            CodiceAnagraficoTurnoAB = StringaDb(myPOSTreader.Item("CodiceAnagraficoTurnoAB"))
            CodiceGruppoDatiVariabiliXServiziComandati = StringaDb(myPOSTreader.Item("CodiceGruppoDatiVariabiliXServiziComandati"))
            GiustificativoPartime = StringaDb(myPOSTreader.Item("GiustificativoPartime"))
            GiustificativoAssenzaPerStraordinario = StringaDb(myPOSTreader.Item("GiustificativoAssenzaPerStraordinario"))
            GiustificativoStraordinarioPagamento = StringaDb(myPOSTreader.Item("GiustificativoStraordinarioPagamento"))
            GiustificativoStraordinarioRecupero = StringaDb(myPOSTreader.Item("GiustificativoStraordinarioRecupero"))
            CreaUtenteSuPassword = StringaDb(myPOSTreader.Item("CreaUtenteSuPassword"))
            CodiceAnagraficoIndirizzoPostaElettronica = StringaDb(myPOSTreader.Item("CodiceAnagraficoIndirizzoPostaElettronica"))
            FasciaUnoDalle = NumeroDb(myPOSTreader.Item("FasciaUnoDalle"))
            FasciaDueDalle = NumeroDb(myPOSTreader.Item("FasciaDueDalle"))
            FasciaTreDalle = NumeroDb(myPOSTreader.Item("FasciaTreDalle"))
            FasciaQuattroDalle = NumeroDb(myPOSTreader.Item("FasciaQuattroDalle"))
            FormulaCalcoloMese = StringaDb(myPOSTreader.Item("FormulaCalcoloMese"))
            ProfiliOrariStrutturati = StringaDb(myPOSTreader.Item("ProfiliOrariStrutturati"))
            CodiceAnagraficoGiorniPresenzaEffettiva = StringaDb(myPOSTreader.Item("CodiceAnagraficoGiorniPresenzaEffettiva"))
            GestioneOrariManuali = StringaDb(myPOSTreader.Item("GestioneOrariManuali"))
            IndirizziDiPostaPerCompleanni = StringaDb(myPOSTreader.Item("IndirizziDiPostaPerCompleanni"))
            IndirizziDiPostaPerTimbrature = StringaDb(myPOSTreader.Item("IndirizziDiPostaPerTimbrature"))
            ServerDiPostaElettronica = StringaDb(myPOSTreader.Item("ServerDiPostaElettronica"))
            AbilitazioniDellaProcedura = StringaDb(myPOSTreader.Item("AbilitazioniDellaProcedura"))
            CodiceContrattoDefault = StringaDb(myPOSTreader.Item("CodiceContrattoDefault"))
            AssegnaRiposoFestivo = StringaDb(myPOSTreader.Item("AssegnaRiposoFestivo"))
            TrattaOrarioPausaAutomatico = StringaDb(myPOSTreader.Item("TrattaOrarioPausaAutomatico"))
            CodiceOrarioFerie = StringaDb(myPOSTreader.Item("CodiceOrarioFerie"))
            CodiceAnagraficoStrutturaNotti = StringaDb(myPOSTreader.Item("CodiceAnagraficoStrutturaNotti"))
            CodiceAnagraficoContatoreFerie = StringaDb(myPOSTreader.Item("CodiceAnagraficoContatoreFerie"))
            CodiceAnagraficoContatorePermessi = StringaDb(myPOSTreader.Item("CodiceAnagraficoContatorePermessi"))
            CodiceAnagraficoPrioritaNellaProposta = StringaDb(myPOSTreader.Item("CodiceAnagraficoPrioritaNellaProposta"))
            CodiceAnagraficoNotteSenzaRiposo = StringaDb(myPOSTreader.Item("CodiceAnagraficoNotteSenzaRiposo"))
            CodiceAnagraficoUnoLibero = StringaDb(myPOSTreader.Item("CodiceAnagraficoUnoLibero"))
            CodiceAnagraficoRiposoAlternato = StringaDb(myPOSTreader.Item("CodiceAnagraficoRiposoAlternato"))
            CodiceAnagraficoRiposoDoppio = StringaDb(myPOSTreader.Item("CodiceAnagraficoRiposoDoppio"))
            CodiceAnagraficoRiposoMassimo = StringaDb(myPOSTreader.Item("CodiceAnagraficoRiposoMassimo"))
            TempoTraOrari = DataDb(myPOSTreader.Item("TempoTraOrari"))
            QuantiOccupati = NumeroDb(myPOSTreader.Item("QuantiOccupati"))
            CodiceAnagraficoGiorniEffettivaPresenza = StringaDb(myPOSTreader.Item("CodiceAnagraficoGiorniEffettivaPresenza"))
            CodiceAnagraficoTraDueRiposi = StringaDb(myPOSTreader.Item("CodiceAnagraficoTraDueRiposi"))
            TempoPresenzeConsecutive = NumeroDb(myPOSTreader.Item("TempoPresenzeConsecutive"))
        Else
            Call Pulisci()
        End If
        cn.Close()
    End Sub

End Class
