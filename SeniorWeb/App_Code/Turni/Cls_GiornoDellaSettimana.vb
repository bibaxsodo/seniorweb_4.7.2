﻿Imports Microsoft.VisualBasic

Public Class Cls_GiornoDellaSettimana

    Sub UpDateDropBox(ByRef Appoggio As DropDownList)

        Appoggio.Items.Clear()
        Appoggio.Items.Add("Domenica")
        Appoggio.Items(Appoggio.Items.Count - 1).Value = 1
        Appoggio.Items.Add("Lunedì")
        Appoggio.Items(Appoggio.Items.Count - 1).Value = 2
        Appoggio.Items.Add("Martedì")
        Appoggio.Items(Appoggio.Items.Count - 1).Value = 3
        Appoggio.Items.Add("Mercoledì")
        Appoggio.Items(Appoggio.Items.Count - 1).Value = 4
        Appoggio.Items.Add("Giovedì")
        Appoggio.Items(Appoggio.Items.Count - 1).Value = 5
        Appoggio.Items.Add("Venerdì")
        Appoggio.Items(Appoggio.Items.Count - 1).Value = 6
        Appoggio.Items.Add("Sabato")
        Appoggio.Items(Appoggio.Items.Count - 1).Value = 7
        Appoggio.Items.Add("")
        Appoggio.Items(Appoggio.Items.Count - 1).Value = ""
        Appoggio.Items(Appoggio.Items.Count - 1).Selected = True
    End Sub

    Function GiornoSettimana(ByVal i As Byte) As String
        GiornoSettimana = ""
        Select Case i
            Case 1
                GiornoSettimana = "Domenica"
            Case 2
                GiornoSettimana = "Lunedi"
            Case 3
                GiornoSettimana = "Martedi"
            Case 4
                GiornoSettimana = "Mercoledi"
            Case 5
                GiornoSettimana = "Giovedi"
            Case 6
                GiornoSettimana = "Venerdi"
            Case 7
                GiornoSettimana = "Sabato"
        End Select

        Return GiornoSettimana
    End Function

End Class
