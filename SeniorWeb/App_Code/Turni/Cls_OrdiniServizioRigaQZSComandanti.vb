Imports Microsoft.VisualBasic
Imports System.Data.OleDb
Imports System.Web.Hosting

Public Class Cls_OrdiniServizioRigaQZSComandanti
    Public Id As Long
    Public Utente As String
    Public DataAggiornamento As Date
    Public Data As Date
    Public Gruppo As String
    Public Tipo As String
    Public Riga As Short
    Public Ordine As Short
    Public Orario As Date
    Public CodiceQZS As String

    Public Sub Pulisci()
        Id = 0
        Utente = ""
        DataAggiornamento = Nothing
        Data = Nothing
        Gruppo = ""
        Tipo = ""
        Riga = 0
        Ordine = 0
        Orario = Nothing
        CodiceQZS = ""
    End Sub
End Class
