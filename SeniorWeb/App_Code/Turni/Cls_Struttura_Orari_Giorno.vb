﻿Imports Microsoft.VisualBasic
Imports System.Data.OleDb
Imports System.Web.Hosting

Public Class Cls_Struttura_Orari_Giorno
    Public GgSett As Byte
    Public Richiesti As String
    Public Variati As String
    Public Modificati As String
    Public TipoFlessibilita As String      ' Impostato da Orario Flessibile (P = Timbratura Presenza ; F = Orario Flessibile ; X = Orario Flessibile con flessibilità dell'intervallo)
    Public FasceOrarie As String
    Public PrimoOrario As String
    Public SecondoOrario As String
    Public TerzoOrario As String
    Public QuartoOrario As String
    Public QuintoOrario As String
    Public Dalle(14) As String
    Public Alle(14) As String
    Public DalleAlto(14) As String
    Public AlleAlto(14) As String
    Public DalleBasso(14) As String
    Public AlleBasso(14) As String
    Public DalleObbligo(14) As String
    Public AlleObbligo(14) As String
    Public Pausa(14) As String
    Public Giustificativo(14) As String
    Public NelGruppo(14) As String
    Public Familiare(14) As Byte
    Public CambioRichiesto(14) As String
    Public Tipo(14) As String
    Public TipoServizio(14) As String
    Public GiornoSuccessivo(14) As String

    Public Sub Pulisci()
        For i = 0 To 14
            Dalle(i) = "00.00"
            Alle(i) = "00.00"
            DalleAlto(i) = "00.00"
            AlleAlto(i) = "00.00"
            DalleBasso(i) = "00.00"
            AlleBasso(i) = "00.00"
            DalleObbligo(i) = "00.00"
            AlleObbligo(i) = "00.00"
            Pausa(i) = "00.00"
            Giustificativo(i) = ""
            NelGruppo(i) = ""
            Familiare(i) = 0
            CambioRichiesto(i) = ""
            Tipo(i) = ""
            TipoServizio(i) = ""
            GiornoSuccessivo(i) = ""
        Next i
        PrimoOrario = ""
        SecondoOrario = ""
        TerzoOrario = ""
        QuartoOrario = ""
        QuintoOrario = ""
        Richiesti = ""
        Variati = ""
        Modificati = ""
        TipoFlessibilita = ""
        FasceOrarie = ""
        GgSett = 0
    End Sub

End Class
