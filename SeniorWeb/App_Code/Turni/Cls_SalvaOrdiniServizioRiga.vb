﻿Imports Microsoft.VisualBasic
Imports System.Data.OleDb
Imports System.Web.Hosting

Public Class Cls_SalvaOrdiniServizioRiga
    Public Id As Long
    Public Utente As String
    Public DataAggiornamento As Date
    Public CodiceGruppo As String
    Public Data As Date
    Public Riga As Long
    Public Selezione As Long
    Public Gruppo As String
    Public Qualifica As String
    Public CognomeNome As String
    Public Commento As String
    Public DalleAlle As String
    Public Mezzo As String
    Public Apparato As String
    Public CodiceServizio As String
    Public Servizio As String
    Public Tipo As String
    Public NumeroPagina As Long
    Public IntestazionePagina As String
    Public NumeroInterruzione As Long
    Public IntestazioneInterruzione As String

    Public Sub Pulisci()
        Id = 0
        Utente = ""
        DataAggiornamento = Nothing
        CodiceGruppo = ""
        Data = Nothing
        Riga = 0
        Selezione = 0
        Gruppo = ""
        Qualifica = ""
        CognomeNome = ""
        Commento = ""
        DalleAlle = ""
        Mezzo = ""
        Apparato = ""
        CodiceServizio = ""
        Servizio = ""
        Tipo = ""
        NumeroPagina = 0
        IntestazionePagina = ""
        NumeroInterruzione = 0
        IntestazioneInterruzione = ""
    End Sub

End Class
