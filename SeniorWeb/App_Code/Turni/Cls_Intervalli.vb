Imports Microsoft.VisualBasic
Imports System.Data.OleDb
Imports System.Web.Hosting

Public Class Cls_Intervalli
    Public Id As Long
    Public Utente As String
    Public DataAggiornamento As Date
    Public Codice As String
    Public Descrizione As String
    Public DalGiorno As Byte
    Public DalMese As Byte
    Public AlGiorno As Byte
    Public AlMese As Byte
    Public DallaSettimana As Byte
    Public AllaSettimana As Byte

    Public Sub Pulisci()
        Id = 0
        Utente = ""
        DataAggiornamento = Nothing
        Codice = ""
        Descrizione = ""
        DalGiorno = 0
        DalMese = 0
        AlGiorno = 0
        AlMese = 0
        DallaSettimana = 0
        AllaSettimana = 0
    End Sub

    Sub Leggi(ByVal StringaConnessione As String, ByVal xCodice As String)
        Dim cn As OleDbConnection

        cn = New Data.OleDb.OleDbConnection(StringaConnessione)

        cn.Open()
        Dim cmd As New OleDbCommand()
        cmd.CommandText = "Select * From Intervalli" & _
                          " WHERE Codice = ?"
        cmd.Parameters.AddWithValue("@Codice", xCodice)
        cmd.Connection = cn

        Dim myPOSTreader As OleDbDataReader = cmd.ExecuteReader()
        If myPOSTreader.Read Then
            Id = NumeroDb(myPOSTreader.Item("Id"))
            Codice = StringaDb(myPOSTreader.Item("Codice"))
            Descrizione = StringaDb(myPOSTreader.Item("Descrizione"))
            DalGiorno = NumeroDb(myPOSTreader.Item("DalGiorno"))
            DalMese = NumeroDb(myPOSTreader.Item("DalMese"))
            AlGiorno = NumeroDb(myPOSTreader.Item("AlGiorno"))
            AlMese = NumeroDb(myPOSTreader.Item("AlMese"))
            DallaSettimana = NumeroDb(myPOSTreader.Item("DallaSettimana"))
            AllaSettimana = NumeroDb(myPOSTreader.Item("AllaSettimana"))
        Else
            Call Pulisci()
        End If
        cn.Close()
    End Sub

    Public Sub Elimina(ByVal StringaConnessione As String, ByVal xCodice As String)
        Dim cn As OleDbConnection

        cn = New Data.OleDb.OleDbConnection(StringaConnessione)

        cn.Open()
        Dim cmd As New OleDbCommand()
        cmd.CommandText = ("DELETE FROM Intervalli where Codice = ?")
        cmd.Parameters.AddWithValue("@Codice", xCodice)
        cmd.Connection = cn
        cmd.ExecuteNonQuery()
        cn.Close()
    End Sub

    Sub Aggiorna(ByVal StringaConnessione As String)
        Dim cn As OleDbConnection

        cn = New Data.OleDb.OleDbConnection(StringaConnessione)

        cn.Open()
        Dim cmd As New OleDbCommand()
        cmd.CommandText = "SELECT * FROM Intervalli WHERE Codice = ?"
        cmd.Parameters.AddWithValue("@Codice", Codice)
        cmd.Connection = cn
        Dim myPOSTreader As OleDbDataReader = cmd.ExecuteReader()
        Dim cmdw As New OleDbCommand()
        If myPOSTreader.Read Then
            cmdw.CommandText = "UPDATE Orari SET " & _
                               " Utente = ?," & _
                               " DataAggiornamento  = ?," & _
                               " Descrizione = ?," & _
                               " DalGiorno = ?," & _
                               " DalMese = ?," & _
                               " AlGiorno = ?," & _
                               " AlMese = ?" & _
                               " DallaSettimana = ?" & _
                               " AllaSettimana = ?" & _
                               " WHERE Codice = ?"

            cmdw.Parameters.AddWithValue("@Utente", Utente)
            cmdw.Parameters.AddWithValue("@DataAggiornamento ", Now)
            cmdw.Parameters.AddWithValue("@Descrizione", Descrizione)
            cmdw.Parameters.AddWithValue("@DalGiorno", DalGiorno)
            cmdw.Parameters.AddWithValue("@DalMese", DalMese)
            cmdw.Parameters.AddWithValue("@AlGiorno", AlGiorno)
            cmdw.Parameters.AddWithValue("@AlMese", AlMese)
            cmdw.Parameters.AddWithValue("@DallaSettimana", DallaSettimana)
            cmdw.Parameters.AddWithValue("@AllaSettimana", AllaSettimana)
            cmdw.Parameters.AddWithValue("@Codice", Codice)
            cmdw.Connection = cn
            cmdw.ExecuteNonQuery()
        Else
            cmdw.CommandText = "INSERT INTO Orari (Utente, DataAggiornamento, Codice, Descrizione, DalGiorno, Dalmese, AlGiorno, AlMese, DallaSettimana, AllaSettimana) VALUES (?,?,?,?,?,?,?,?,?,?)"

            cmdw.Parameters.AddWithValue("@Utente", Utente)
            cmdw.Parameters.AddWithValue("@DataAggiornamento", Now)
            cmdw.Parameters.AddWithValue("@Codice", Codice)
            cmdw.Parameters.AddWithValue("@Descrizione", Descrizione)
            cmdw.Parameters.AddWithValue("@DalGiorno", DalGiorno)
            cmdw.Parameters.AddWithValue("@DalMese", DalMese)
            cmdw.Parameters.AddWithValue("@AlGiorno", AlGiorno)
            cmdw.Parameters.AddWithValue("@AlMese", AlMese)
            cmdw.Parameters.AddWithValue("@DallaSettimana", DallaSettimana)
            cmdw.Parameters.AddWithValue("@AllaSettimana", AllaSettimana)
            cmdw.Connection = cn
            cmdw.ExecuteNonQuery()
        End If
        myPOSTreader.Close()
        cn.Close()

        Call Pulisci()
    End Sub

    Sub LoadDati(ByVal StringaConnessione As String, ByVal Tabella As System.Data.DataTable)
        Dim cn As OleDbConnection

        cn = New Data.OleDb.OleDbConnection(StringaConnessione)

        cn.Open()
        Dim cmd As New OleDbCommand()

        cmd.CommandText = ("Select * From Intervalli Order By Descrizione")

        cmd.Connection = cn

        Tabella.Clear()
        Tabella.Columns.Add("Codice", GetType(String))
        Tabella.Columns.Add("Descrizione", GetType(String))

        Dim myPOSTreader As OleDbDataReader = cmd.ExecuteReader()
        Do While myPOSTreader.Read

            Dim myriga As System.Data.DataRow = Tabella.NewRow()
            myriga(0) = myPOSTreader.Item("Codice")
            myriga(1) = myPOSTreader.Item("Descrizione")

            Tabella.Rows.Add(myriga)
        Loop
        myPOSTreader.Close()
        cn.Close()

    End Sub

    Function Esiste(ByVal ConnectionString As String, ByVal xCodice As String) As Boolean
        Dim cn As OleDbConnection

        cn = New Data.OleDb.OleDbConnection(ConnectionString)

        cn.Open()
        Dim cmd As New OleDbCommand()
        cmd.CommandText = "Select * FROM Intervalli" & _
                          " WHERE Codice = ?"
        cmd.Parameters.AddWithValue("@Codice", xCodice)
        cmd.Connection = cn

        Dim myPOSTreader As OleDbDataReader = cmd.ExecuteReader()

        If myPOSTreader.Read Then
            Return True
        Else
            Return False
        End If
        myPOSTreader.Close()
        cn.Close()
    End Function

    Function DescrizioneDuplicata(ByVal ConnectionString As String, ByVal xCodice As String, ByVal xDescrizione As String) As Boolean
        DescrizioneDuplicata = False
        Dim cn As OleDbConnection

        cn = New Data.OleDb.OleDbConnection(ConnectionString)

        cn.Open()
        Dim cmd As New OleDbCommand()
        cmd.CommandText = "Select * FROM Intervalli" & _
                          " WHERE Descrizione = ?"
        cmd.Parameters.AddWithValue("@Descrizione", xDescrizione)
        cmd.Connection = cn

        Dim myPOSTreader As OleDbDataReader = cmd.ExecuteReader()

        Do While myPOSTreader.Read
            If myPOSTreader.Item("Codice") <> xCodice Then
                Return True
                Exit Do
            End If
        Loop
        myPOSTreader.Close()
        cn.Close()
    End Function

    Function StringaDb(ByVal Oggetto As Object) As String
        If IsDBNull(Oggetto) Then
            Return ""
        Else
            Return Oggetto
        End If
    End Function

    Function NumeroDb(ByVal Oggetto As Object) As Object
        If IsDBNull(Oggetto) Then
            Return 0
        Else
            Return Oggetto
        End If
    End Function
End Class
