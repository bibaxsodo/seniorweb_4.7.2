Imports Microsoft.VisualBasic
Imports System.Data.OleDb
Imports System.Web.Hosting

Public Class Cls_TabelleDescrittiveRiga
    Public Id As Long
    Public Utente As String
    Public DataAggiornamento As Date
    Public CodiceTabella As String
    Public Codice As String
    Public Descrizione As String

    Public Sub Pulisci()
        Id = 0
        Utente = ""
        DataAggiornamento = Nothing
        CodiceTabella = ""
        Codice = ""
        Descrizione = ""
    End Sub

    Public Sub Elimina(ByVal ConnectionString As String, ByVal xCodiceTabella As String, ByVal xCodice As String)
        Dim cn As OleDbConnection
        Dim MySql As String

        MySql = ""

        cn = New Data.OleDb.OleDbConnection(ConnectionString)

        cn.Open()
        Dim cmd As New OleDbCommand()
        cmd.CommandText = "DELETE FROM TabelleDescrittiveRiga" & _
                           " WHERE CodiceTabella = ?" & _
                           " AND Codice = ?"
        cmd.Parameters.AddWithValue("@CodiceTabella", xCodiceTabella)
        cmd.Parameters.AddWithValue("@Codice", xCodice)
        cmd.Connection = cn
        cmd.ExecuteNonQuery()
        cn.Close()
    End Sub

    Public Sub EliminaAll(ByVal StringaConnessione As String, ByVal xCodiceTabella As String)
        Dim cn As OleDbConnection

        cn = New Data.OleDb.OleDbConnection(StringaConnessione)

        cn.Open()
        Dim cmd As New OleDbCommand()
        cmd.CommandText = "DELETE FROM TabelleDescrittiveRiga WHERE CodiceTabella = ?"
        cmd.Parameters.AddWithValue("@CodiceTabella", xCodiceTabella)
        cmd.Connection = cn
        cmd.ExecuteNonQuery()
        cn.Close()
    End Sub

    Sub Aggiorna(ByVal ConnectionString As String)
        Dim cn As OleDbConnection
        Dim MySql As String

        MySql = ""

        cn = New Data.OleDb.OleDbConnection(ConnectionString)

        cn.Open()
        Dim cmd As New OleDbCommand()
        cmd.CommandText = "SELECT * FROM TabelleDescrittiveRiga" & _
                           " WHERE CodiceTabella = ?" & _
                           " AND Codice = ?"
        cmd.Parameters.AddWithValue("@CodiceTabella", CodiceTabella)
        cmd.Parameters.AddWithValue("@Codice", Codice)
        cmd.Connection = cn
        Dim myPOSTreader As OleDbDataReader = cmd.ExecuteReader()
        If myPOSTreader.Read Then
            MySql = "UPDATE TabelleDescrittiveRiga SET " & _
                    " Utente = ?," & _
                    " DataAggiornamento  = ?," & _
                    " Descrizione = ?" & _
                    " WHERE CodiceTabella  = ?" & _
                    " AND Codice = ?"

            Dim cmdw As New OleDbCommand()
            cmdw.CommandText = MySql
            cmdw.Parameters.AddWithValue("@Utente", Utente)
            cmdw.Parameters.AddWithValue("@DataAggiornamento", Now)
            cmdw.Parameters.AddWithValue("@Descrizione", Descrizione)
            cmdw.Parameters.AddWithValue("@CodiceTabella", CodiceTabella)
            cmdw.Parameters.AddWithValue("@Codice", Codice)
            cmdw.Connection = cn
            cmdw.ExecuteNonQuery()
        Else
            MySql = "INSERT INTO TabelleDescrittiveRiga (Utente, DataAggiornamento, CodiceTabella, Codice, Descrizione) VALUES (?,?,?,?,?)"

            Dim cmdw As New OleDbCommand()
            cmdw.CommandText = MySql
            cmdw.Parameters.AddWithValue("@Utente", Utente)
            cmdw.Parameters.AddWithValue("@DataAggiornamento", Now)
            cmdw.Parameters.AddWithValue("@CodiceTabella", CodiceTabella)
            cmdw.Parameters.AddWithValue("@Codice", Codice)
            cmdw.Parameters.AddWithValue("@Descrizione", Descrizione)
            cmdw.Connection = cn
            cmdw.ExecuteNonQuery()
        End If
        myPOSTreader.Close()
        cn.Close()

        Call Pulisci()
    End Sub

    Sub LoadDati(ByVal ConnectionString As String, ByVal xCodiceTabella As String, ByVal Tabella As System.Data.DataTable)
        Dim cn As OleDbConnection

        cn = New Data.OleDb.OleDbConnection(ConnectionString)

        cn.Open()
        Dim cmd As New OleDbCommand()

        cmd.CommandText = "SELECT * FROM TabelleDescrittiveRiga" & _
                           " WHERE CodiceTabella = ?" & _
                           " ORDER By Codice"
        cmd.Parameters.AddWithValue("@CodiceTabella", xCodiceTabella)

        cmd.Connection = cn

        Tabella.Clear()
        Tabella.Columns.Clear()
        Tabella.Columns.Add("Codice", GetType(String))
        Tabella.Columns.Add("Descrizione", GetType(String))

        Dim myPOSTreader As OleDbDataReader = cmd.ExecuteReader()
        Do While myPOSTreader.Read

            Dim myriga As System.Data.DataRow = Tabella.NewRow()
            myriga(0) = myPOSTreader.Item("Codice")
            myriga(1) = myPOSTreader.Item("Descrizione")

            Tabella.Rows.Add(myriga)
        Loop
        myPOSTreader.Close()
        cn.Close()

    End Sub

    Function StringaDb(ByVal Oggetto As Object) As String
        If IsDBNull(Oggetto) Then
            Return ""
        Else
            Return Oggetto
        End If
    End Function

    Function NumeroDb(ByVal Oggetto As Object) As Object
        If IsDBNull(Oggetto) Then
            Return 0
        Else
            Return Oggetto
        End If
    End Function

    Sub Leggi(ByVal ConnectionString As String, ByVal xCodiceTabella As String, ByVal xCodice As String)
        Dim cn As OleDbConnection

        cn = New Data.OleDb.OleDbConnection(ConnectionString)

        cn.Open()
        Dim cmd As New OleDbCommand()
        cmd.CommandText = "Select * From TabelleDescrittiveRiga" & _
                           " WHERE CodiceTabella = ?" & _
                           " AND Codice = ?"
        cmd.Parameters.AddWithValue("@CodiceTabella", xCodiceTabella)
        cmd.Parameters.AddWithValue("@Codice", xCodice)
        cmd.Connection = cn

        Dim myPOSTreader As OleDbDataReader = cmd.ExecuteReader()
        If myPOSTreader.Read Then
            Id = NumeroDb(myPOSTreader.Item("Id"))
            CodiceTabella = StringaDb(myPOSTreader.Item("CodiceTabella"))
            Codice = StringaDb(myPOSTreader.Item("Codice"))
            Descrizione = StringaDb(myPOSTreader.Item("Descrizione"))
        Else
            Call Pulisci()
        End If
        cn.Close()
    End Sub

    Function Decodifica(ByVal ConnectionString As String, ByVal xCodiceTabella As String, ByVal xCodice As String) As String
        Dim cn As OleDbConnection

        cn = New Data.OleDb.OleDbConnection(ConnectionString)

        cn.Open()
        Dim cmd As New OleDbCommand()
        cmd.CommandText = "Select * From TabelleDescrittiveRiga" & _
                           " WHERE CodiceTabella = ?" & _
                           " AND Codice = ?"
        cmd.Parameters.AddWithValue("@CodiceTabella", xCodiceTabella)
        cmd.Parameters.AddWithValue("@Codice", xCodice)
        cmd.Connection = cn

        Dim myPOSTreader As OleDbDataReader = cmd.ExecuteReader()
        If myPOSTreader.Read Then
            Decodifica = StringaDb(myPOSTreader.Item("Descrizione"))
        Else
            Decodifica = "Tabella Descrittiva Non Decodificata"
        End If
        cn.Close()
    End Function

    Function TestUsato(ByVal ConnectionString As String, ByVal xCodiceTabella As String, ByVal xCodice As String) As Boolean
        Dim cn As OleDbConnection
        cn = New Data.OleDb.OleDbConnection(ConnectionString)
        cn.Open()

        Dim cmd As New OleDbCommand()
        cmd.CommandText = "SELECT Count(*) FROM (DatiVariabili INNER JOIN TipiDatiAnagrafici ON DatiVariabili.CodiceVariabile = TipiDatiAnagrafici.Codice) INNER JOIN TabelleDescrittiveRiga ON TipiDatiAnagrafici.TabellaDescrittiva = TabelleDescrittiveRiga.CodiceTabella" & _
                          " WHERE TabelleDescrittiveRiga.CodiceTabella = ?" & _
                          " And ContenutoTesto = ?"

        cmd.Parameters.AddWithValue("@CodiceTabella", xCodiceTabella)
        cmd.Parameters.AddWithValue("@ContenutoTesto", xCodice)

        cmd.Connection = cn
        Dim myPOSTreader As OleDbDataReader = cmd.ExecuteReader()
        If myPOSTreader.Read Then
            If myPOSTreader.Item(0) = 0 Then
                Return False
            Else
                Return True
            End If
        Else
            Return False
        End If
        myPOSTreader.Close()
        cn.Close()
    End Function

    Sub UpDateDropBox(ByVal ConnectionString As String, ByVal xCodiceTabella As String, ByRef Appoggio As DropDownList)
        Dim cn As OleDbConnection

        cn = New Data.OleDb.OleDbConnection(ConnectionString)

        cn.Open()
        Dim cmd As New OleDbCommand()
        cmd.CommandText = "SELECT Codice, Descrizione FROM TabelleDescrittiveRiga" & _
                          " WHERE CodiceTabella = ?" & _
                          " ORDER BY Descrizione"
        cmd.Parameters.AddWithValue("@CodiceTabella", xCodiceTabella)
        cmd.Connection = cn

        Appoggio.Items.Clear()
        Dim myPOSTreader As OleDbDataReader = cmd.ExecuteReader()
        Do While myPOSTreader.Read
            Appoggio.Items.Add(myPOSTreader.Item("Descrizione"))
            Appoggio.Items(Appoggio.Items.Count - 1).Value = myPOSTreader.Item("Codice")
        Loop
        myPOSTreader.Close()
        cn.Close()
        Appoggio.Items.Add("")
        Appoggio.Items(Appoggio.Items.Count - 1).Value = ""
        Appoggio.Items(Appoggio.Items.Count - 1).Selected = True

    End Sub

    Function DecodificaTabellaDescrittiva(ByVal ConnectionString As String, ByVal xCodiceTabella As String, ByVal xCodice As String) As String
        Dim cn As OleDbConnection

        cn = New Data.OleDb.OleDbConnection(ConnectionString)

        DecodificaTabellaDescrittiva = "Tabella Descrittiva Non Decodificata"

        cn.Open()
        Dim cmd As New OleDbCommand()
        cmd.CommandText = "Select * From TabelleDescrittiveRiga" & _
                           " WHERE CodiceTabella = ?" & _
                           " AND Codice = ?"
        cmd.Parameters.AddWithValue("@CodiceTabella", xCodiceTabella)
        cmd.Parameters.AddWithValue("@Codice", xCodice)
        cmd.Connection = cn

        Dim myPOSTreader As OleDbDataReader = cmd.ExecuteReader()
        If myPOSTreader.Read Then
            DecodificaTabellaDescrittiva = StringaDb(myPOSTreader.Item("Descrizione"))
        End If
        cn.Close()
    End Function

End Class
