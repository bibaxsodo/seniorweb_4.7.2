Imports Microsoft.VisualBasic
Imports System.Data.OleDb
Imports System.Web.Hosting

Public Class Cls_RegoleSostituzione
    Public Id As Long
    Public Utente As String
    Public DataAggiornamento As Date
    Public Codice As String
    Public Validita As Date
    Public Regola1 As String
    Public Regola2 As String
    Public Regola3 As String
    Public Regola4 As String
    Public Regola5 As String
    Public Regola6 As String
    Public Regola7 As String
    Public Regola8 As String
    Public Regola9 As String
    Public Regola10 As String
    Public Qualifica1 As String
    Public Qualifica2 As String
    Public Qualifica3 As String
    Public Qualifica4 As String
    Public Qualifica5 As String
    Public Qualifica6 As String
    Public Qualifica7 As String
    Public Qualifica8 As String
    Public Qualifica9 As String
    Public Qualifica10 As String
    Public OperatorePrecedente As Date
    Public OperatoreSuccessivo As Date
    Public GiustificativoA As String
    Public GiustificativoB As String
    Public GiustificativoC As String

    Public Sub Pulisci()
        Id = 0
        Utente = ""
        DataAggiornamento = Nothing
        Codice = ""
        Validita = Nothing
        Regola1 = ""
        Regola2 = ""
        Regola3 = ""
        Regola4 = ""
        Regola5 = ""
        Regola6 = ""
        Regola7 = ""
        Regola8 = ""
        Regola9 = ""
        Regola10 = ""
        Qualifica1 = ""
        Qualifica2 = ""
        Qualifica3 = ""
        Qualifica4 = ""
        Qualifica5 = ""
        Qualifica6 = ""
        Qualifica7 = ""
        Qualifica8 = ""
        Qualifica9 = ""
        Qualifica10 = ""
        OperatorePrecedente = Nothing
        OperatoreSuccessivo = Nothing
        GiustificativoA = ""
        GiustificativoB = ""
        GiustificativoC = ""
    End Sub

    Public Sub Elimina(ByVal StringaConnessione As String, ByVal xCodice As String, ByVal xValidita As Date)
        Dim cn As OleDbConnection

        cn = New Data.OleDb.OleDbConnection(StringaConnessione)

        cn.Open()
        Dim cmd As New OleDbCommand()
        cmd.CommandText = "DELETE FROM RegoleSostituzione" & _
                          " WHERE Codice = ?" & _
                          " AND Validita = ?"
        cmd.Parameters.AddWithValue("@Codice", xCodice)
        cmd.Parameters.AddWithValue("@Validita", xValidita)
        cmd.Connection = cn
        cmd.ExecuteNonQuery()
        cn.Close()
    End Sub

    Sub Aggiorna(ByVal StringaConnessione As String)
        Dim cn As OleDbConnection
        Dim MySql As String

        cn = New Data.OleDb.OleDbConnection(StringaConnessione)

        cn.Open()
        Dim cmd As New OleDbCommand()
        cmd.CommandText = "Select * from RegoleSostituzione" & _
                          " WHERE Codice = ?" & _
                          " AND Validita = ?"
        cmd.Parameters.AddWithValue("@Codice", Codice)
        cmd.Parameters.AddWithValue("@Validita", Validita)
        cmd.Connection = cn
        Dim myPOSTreader As OleDbDataReader = cmd.ExecuteReader()
        If myPOSTreader.Read Then
            Dim cmdw As New OleDbCommand()
            cmdw.CommandText = "UPDATE RegoleSostituzione SET " & _
                               " Utente = ?," & _
                               " DataAggiornamento = ?," & _
                               " Regola1 = ?," & _
                               " Regola2 = ?," & _
                               " Regola3 = ?," & _
                               " Regola4 = ?," & _
                               " Regola5 = ?," & _
                               " Regola6 = ?," & _
                               " Regola7 = ?," & _
                               " Regola8 = ?," & _
                               " Regola9 = ?," & _
                               " Regola10 = ?," & _
                               " Qualifica1 = ?," & _
                               " Qualifica2 = ?," & _
                               " Qualifica3 = ?," & _
                               " Qualifica4 = ?," & _
                               " Qualifica5 = ?," & _
                               " Qualifica6 = ?," & _
                               " Qualifica7 = ?," & _
                               " Qualifica8 = ?," & _
                               " Qualifica9 = ?," & _
                               " Qualifica10 = ?," & _
                               " OperatorePrecedente = ?," & _
                               " OperatoreSuccessivo = ?," & _
                               " GiustificativoA = ?," & _
                               " GiustificativoB = ?," & _
                               " GiustificativoC = ?" & _
                               " WHERE Codice = ?" & _
                               " AND Validita = ?"

            cmdw.Parameters.AddWithValue("@Utente", Utente)
            cmdw.Parameters.AddWithValue("@DataAggiornamento", Now)
            cmdw.Parameters.AddWithValue("@Regola1", Regola1)
            cmdw.Parameters.AddWithValue("@Regola2", Regola2)
            cmdw.Parameters.AddWithValue("@Regola3", Regola3)
            cmdw.Parameters.AddWithValue("@Regola4", Regola4)
            cmdw.Parameters.AddWithValue("@Regola5", Regola5)
            cmdw.Parameters.AddWithValue("@Regola6", Regola6)
            cmdw.Parameters.AddWithValue("@Regola7", Regola7)
            cmdw.Parameters.AddWithValue("@Regola8", Regola8)
            cmdw.Parameters.AddWithValue("@Regola9", Regola9)
            cmdw.Parameters.AddWithValue("@Regola10", Regola10)
            cmdw.Parameters.AddWithValue("@Qualifica1", Qualifica1)
            cmdw.Parameters.AddWithValue("@Qualifica2", Qualifica2)
            cmdw.Parameters.AddWithValue("@Qualifica3", Qualifica3)
            cmdw.Parameters.AddWithValue("@Qualifica4", Qualifica4)
            cmdw.Parameters.AddWithValue("@Qualifica5", Qualifica5)
            cmdw.Parameters.AddWithValue("@Qualifica6", Qualifica6)
            cmdw.Parameters.AddWithValue("@Qualifica7", Qualifica7)
            cmdw.Parameters.AddWithValue("@Qualifica8", Qualifica8)
            cmdw.Parameters.AddWithValue("@Qualifica9", Qualifica9)
            cmdw.Parameters.AddWithValue("@Qualifica10", Qualifica10)
            cmdw.Parameters.AddWithValue("@OperatorePrecedente", OperatorePrecedente)
            cmdw.Parameters.AddWithValue("@OperatoreSuccessivo", OperatoreSuccessivo)
            cmdw.Parameters.AddWithValue("@GiustificativoA", GiustificativoA)
            cmdw.Parameters.AddWithValue("@GiustificativoB", GiustificativoB)
            cmdw.Parameters.AddWithValue("@GiustificativoC", GiustificativoC)
            cmdw.Parameters.AddWithValue("@Codice", Codice)
            cmdw.Parameters.AddWithValue("@Validita", Validita)
            cmdw.ExecuteNonQuery()
        Else
            Dim cmdw As New OleDbCommand()
            cmdw.CommandText = "INSERT INTO RegoleSostituzione (Utente, DataAggiornamento, Codice, Validita, Regola1, Regola2 Regola3, Regola4, Regola5, Regola6, Regola7, Regola8, Regola9, Regola10, Qualifica1, Qualifica2, Qualifica3, Qualifica4, Qualifica5, Qualifica6, Qualifica7, Qualifica8, Qualifica9, Qualifica10, OperatorePrecedente, OperatoreSuccessivo, GiustificativoA, GiustificativoB, GiustificativoC) VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)"

            cmdw.Parameters.AddWithValue("@Utente", Utente)
            cmdw.Parameters.AddWithValue("@DataAggiornamento", Now)
            cmdw.Parameters.AddWithValue("@Codice", Codice)
            cmdw.Parameters.AddWithValue("@Validita", Validita)
            cmdw.Parameters.AddWithValue("@Regola1", Regola1)
            cmdw.Parameters.AddWithValue("@Regola2", Regola2)
            cmdw.Parameters.AddWithValue("@Regola3", Regola3)
            cmdw.Parameters.AddWithValue("@Regola4", Regola4)
            cmdw.Parameters.AddWithValue("@Regola5", Regola5)
            cmdw.Parameters.AddWithValue("@Regola6", Regola6)
            cmdw.Parameters.AddWithValue("@Regola7", Regola7)
            cmdw.Parameters.AddWithValue("@Regola8", Regola8)
            cmdw.Parameters.AddWithValue("@Regola9", Regola9)
            cmdw.Parameters.AddWithValue("@Regola10", Regola10)
            cmdw.Parameters.AddWithValue("@Qualifica1", Qualifica1)
            cmdw.Parameters.AddWithValue("@Qualifica2", Qualifica2)
            cmdw.Parameters.AddWithValue("@Qualifica3", Qualifica3)
            cmdw.Parameters.AddWithValue("@Qualifica4", Qualifica4)
            cmdw.Parameters.AddWithValue("@Qualifica5", Qualifica5)
            cmdw.Parameters.AddWithValue("@Qualifica6", Qualifica6)
            cmdw.Parameters.AddWithValue("@Qualifica7", Qualifica7)
            cmdw.Parameters.AddWithValue("@Qualifica8", Qualifica8)
            cmdw.Parameters.AddWithValue("@Qualifica9", Qualifica9)
            cmdw.Parameters.AddWithValue("@Qualifica10", Qualifica10)
            cmdw.Parameters.AddWithValue("@OperatorePrecedente", OperatorePrecedente)
            cmdw.Parameters.AddWithValue("@OperatoreSuccessivo", OperatoreSuccessivo)
            cmdw.Parameters.AddWithValue("@GiustificativoA", GiustificativoA)
            cmdw.Parameters.AddWithValue("@GiustificativoB", GiustificativoB)
            cmdw.Parameters.AddWithValue("@GiustificativoC", GiustificativoC)
            cmdw.Connection = cn
            cmdw.ExecuteNonQuery()
        End If
        myPOSTreader.Close()
        cn.Close()

        Call Pulisci()
    End Sub

    Sub LoadDati(ByVal StringaConnessione As String, ByVal Tabella As System.Data.DataTable)
        Dim cn As OleDbConnection
        Dim d As New Cls_Qualifica

        Tabella.Clear()
        Tabella.Columns.Add("Codice", GetType(String))
        Tabella.Columns.Add("Validita", GetType(String))
        Tabella.Columns.Add("Qualifica1", GetType(String))
        Tabella.Columns.Add("Qualifica2", GetType(String))
        Tabella.Columns.Add("Qualifica3", GetType(String))
        Tabella.Columns.Add("Qualifica4", GetType(String))
        Tabella.Columns.Add("DescrizioneQualifica1", GetType(String))
        Tabella.Columns.Add("DescrizioneQualifica2", GetType(String))
        Tabella.Columns.Add("DescrizioneQualifica3", GetType(String))
        Tabella.Columns.Add("DescrizioneQualifica4", GetType(String))

        cn = New Data.OleDb.OleDbConnection(StringaConnessione)

        cn.Open()
        Dim cmd As New OleDbCommand()

        cmd.CommandText = ("Select * From RegoleSostituzione Order By Codice, Validita")

        cmd.Connection = cn

        Dim myPOSTreader As OleDbDataReader = cmd.ExecuteReader()
        Do While myPOSTreader.Read

            Dim myriga As System.Data.DataRow = Tabella.NewRow()
            myriga(0) = StringaDb(myPOSTreader.Item("Codice"))
            myriga(1) = Format(DataDb(myPOSTreader.Item("Validita")), "dd/MM/yyyy")
            myriga(2) = StringaDb(myPOSTreader.Item("Qualifica1"))
            myriga(3) = StringaDb(myPOSTreader.Item("Qualifica2"))
            myriga(4) = StringaDb(myPOSTreader.Item("Qualifica3"))
            myriga(5) = StringaDb(myPOSTreader.Item("Qualifica4"))
            myriga(6) = d.Decodifica(StringaConnessione, StringaDb(myPOSTreader.Item("Qualifica1")))
            myriga(7) = d.Decodifica(StringaConnessione, StringaDb(myPOSTreader.Item("Qualifica2")))
            myriga(8) = d.Decodifica(StringaConnessione, StringaDb(myPOSTreader.Item("Qualifica3")))
            myriga(9) = d.Decodifica(StringaConnessione, StringaDb(myPOSTreader.Item("Qualifica4")))

            Tabella.Rows.Add(myriga)
        Loop
        myPOSTreader.Close()
        cn.Close()
    End Sub

    Function StringaDb(ByVal Oggetto As Object) As String
        If IsDBNull(Oggetto) Then
            Return ""
        Else
            Return Oggetto
        End If
    End Function

    Function DataDb(ByVal Oggetto As Object) As Date
        If Not IsDate(Oggetto) Then
            Return Nothing
        Else
            Return Oggetto
        End If
    End Function

    Function NumeroDb(ByVal Oggetto As Object) As Object
        If IsDBNull(Oggetto) Then
            Return 0
        Else
            Return Oggetto
        End If
    End Function

    Sub Leggi(ByVal StringaConnessione As String, ByVal xCodice As String, ByVal xValidita As Date)
        Dim cn As OleDbConnection

        cn = New Data.OleDb.OleDbConnection(StringaConnessione)

        cn.Open()
        Dim cmd As New OleDbCommand()
        cmd.CommandText = "Select * From RegoleSostituzione" & _
                          " where Codice = ?" & _
                          " AND Validita = ?"
        cmd.Parameters.AddWithValue("@Codice", xCodice)
        cmd.Parameters.AddWithValue("@Validita", xValidita)
        cmd.Connection = cn

        Dim myPOSTreader As OleDbDataReader = cmd.ExecuteReader()
        If myPOSTreader.Read Then
            Id = NumeroDb(myPOSTreader.Item("Id"))
            Codice = StringaDb(myPOSTreader.Item("Codice"))
            Validita = DataDb(myPOSTreader.Item("Validita"))
            Regola1 = StringaDb(myPOSTreader.Item("Regola1"))
            Regola2 = StringaDb(myPOSTreader.Item("Regola2"))
            Regola3 = StringaDb(myPOSTreader.Item("Regola3"))
            Regola4 = StringaDb(myPOSTreader.Item("Regola4"))
            Regola5 = StringaDb(myPOSTreader.Item("Regola5"))
            Regola6 = StringaDb(myPOSTreader.Item("Regola6"))
            Regola7 = StringaDb(myPOSTreader.Item("Regola7"))
            Regola8 = StringaDb(myPOSTreader.Item("Regola8"))
            Regola9 = StringaDb(myPOSTreader.Item("Regola9"))
            Regola10 = StringaDb(myPOSTreader.Item("Regola10"))
            Qualifica1 = StringaDb(myPOSTreader.Item("Qualifica1"))
            Qualifica2 = StringaDb(myPOSTreader.Item("Qualifica2"))
            Qualifica3 = StringaDb(myPOSTreader.Item("Qualifica3"))
            Qualifica4 = StringaDb(myPOSTreader.Item("Qualifica4"))
            Qualifica5 = StringaDb(myPOSTreader.Item("Qualifica5"))
            Qualifica6 = StringaDb(myPOSTreader.Item("Qualifica6"))
            Qualifica7 = StringaDb(myPOSTreader.Item("Qualifica7"))
            Qualifica8 = StringaDb(myPOSTreader.Item("Qualifica8"))
            Qualifica9 = StringaDb(myPOSTreader.Item("Qualifica9"))
            Qualifica10 = StringaDb(myPOSTreader.Item("Qualifica10"))
            OperatorePrecedente = DataDb(myPOSTreader.Item("OperatorePrecedente"))
            OperatoreSuccessivo = DataDb(myPOSTreader.Item("OperatoreSuccessivo"))
            GiustificativoA = StringaDb(myPOSTreader.Item("GiustificativoA"))
            GiustificativoB = StringaDb(myPOSTreader.Item("GiustificativoB"))
            GiustificativoC = StringaDb(myPOSTreader.Item("GiustificativoC"))
        Else
            Call Pulisci()
        End If
        myPOSTreader.Close()
        cn.Close()
    End Sub

    Function Esiste(ByVal StringaConnessione As String, ByVal xCodice As String, ByVal xValidita As Date) As Boolean
        Dim cn As OleDbConnection

        cn = New Data.OleDb.OleDbConnection(StringaConnessione)

        cn.Open()
        Dim cmd As New OleDbCommand()
        cmd.CommandText = "Select * From RegoleSostituzione" & _
                          " where Codice = ?" & _
                          " AND Validita = ?"
        cmd.Parameters.AddWithValue("@Codice", xCodice)
        cmd.Parameters.AddWithValue("@Validita", xValidita)
        cmd.Connection = cn

        Dim myPOSTreader As OleDbDataReader = cmd.ExecuteReader()

        If myPOSTreader.Read Then
            Return True
        Else
            Return False
        End If
        myPOSTreader.Close()
        cn.Close()
    End Function

End Class
