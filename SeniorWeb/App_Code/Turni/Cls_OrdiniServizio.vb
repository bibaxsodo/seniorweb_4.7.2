Imports Microsoft.VisualBasic
Imports System.Data.OleDb
Imports System.Web.Hosting

Public Class Cls_OrdiniServizio
    Public Id As Long
    Public Utente As String
    Public DataAggiornamento As Date
    Public CodiceDipendente As Integer
    Public Data As Date
    Public Orario As Date
    Public Giustificativo As String
    Public Commento As String
    Public Tempo As Date

    Public Sub Pulisci()
        Id = 0
        Utente = ""
        DataAggiornamento = Nothing
        CodiceDipendente = 0
        Data = Nothing
        Orario = Nothing
        Giustificativo = ""
        Commento = ""
        Tempo = Nothing
    End Sub

End Class
