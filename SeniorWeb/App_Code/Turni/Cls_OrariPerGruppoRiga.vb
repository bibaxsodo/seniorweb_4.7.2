Imports Microsoft.VisualBasic
Imports System.Data.OleDb
Imports System.Web.Hosting

Public Class Cls_OrariPerGruppoRiga
    Public Id As Long
    Public Utente As String
    Public DataAggiornamento As Date
    Public Codice As String
    Public Validita As Date
    Public Riga As Integer
    Public Orario As String
    Public OrarioSabato As String
    Public OrarioFestivo As String
    Public Giustificativo As String
    Public GiustificativoSabato As String
    Public GiustificativoFestivo As String
    Public Dipendenti As Short
    Public DipendentiSabato As Short
    Public DipendentiFestivo As Short

    Public Sub Pulisci()
        Id = 0
        Utente = ""
        DataAggiornamento = Nothing
        Validita = Nothing
        Codice = ""
        Orario = ""
        OrarioSabato = ""
        OrarioFestivo = ""
        Giustificativo = ""
        GiustificativoSabato = ""
        GiustificativoFestivo = ""
        Dipendenti = 0
        DipendentiSabato = 0
        DipendentiFestivo = 0
    End Sub

    Public Sub Elimina(ByVal ConnectionString As String, ByVal xCodice As String, ByVal xValidita As Date, ByVal xRiga As Integer)
        Dim cn As OleDbConnection
        Dim MySql As String

        MySql = ""

        cn = New Data.OleDb.OleDbConnection(ConnectionString)

        cn.Open()
        Dim cmd As New OleDbCommand()
        cmd.CommandText = "DELETE FROM OrariPerGruppoRiga WHERE " & _
                          " Codice = ?" & _
                          " AND Validita = ?" & _
                          " AND Riga = ?"
        cmd.Parameters.AddWithValue("@Codice", xCodice)
        cmd.Parameters.AddWithValue("@Validita", xValidita)
        cmd.Parameters.AddWithValue("@Riga", xRiga)
        cmd.Connection = cn
        cmd.ExecuteNonQuery()
        cn.Close()
    End Sub

    Sub Aggiorna(ByVal ConnectionString As String)
        Dim cn As OleDbConnection
        Dim MySql As String

        MySql = ""

        cn = New Data.OleDb.OleDbConnection(ConnectionString)

        cn.Open()
        Dim cmd As New OleDbCommand()
        cmd.CommandText = "SELECT * FROM OrariPerGruppoRiga" & _
                          " WHERE Codice = ?" & _
                          " AND Validita = ?" & _
                          " AND Riga = ?"
        cmd.Parameters.AddWithValue("@Codice", Codice)
        cmd.Parameters.AddWithValue("@Validita", Validita)
        cmd.Parameters.AddWithValue("@Riga", Riga)
        cmd.Connection = cn
        Dim myPOSTreader As OleDbDataReader = cmd.ExecuteReader()
        If myPOSTreader.Read Then
            MySql = "UPDATE OrariPerGruppoRiga SET " & _
                    " Utente = ?, " & _
                    " DataAggiornamento = ?," & _
                    " Orario = ?," & _
                    " OrarioSabato = ?," & _
                    " OrarioFestivo = ?," & _
                    " Giustificativo = ?," & _
                    " GiustificativoSabato = ?," & _
                    " GiustificativoFestivo = ?," & _
                    " Dipendenti = ?," & _
                    " DipendentiSabato = ?," & _
                    " DipendentiFestivo = ?," & _
                    " WHERE Codice = ?" & _
                    " AND Validita = ?" & _
                    " AND Riga = ?"

            Dim cmdw As New OleDbCommand()
            cmdw.CommandText = MySql
            cmdw.Parameters.AddWithValue("@Utente", Utente)
            cmdw.Parameters.AddWithValue("@DataAggiornamento", Now)
            cmdw.Parameters.AddWithValue("@Orario", Orario)
            cmdw.Parameters.AddWithValue("@OrarioSabato", OrarioSabato)
            cmdw.Parameters.AddWithValue("@OrarioFestivo", OrarioFestivo)
            cmdw.Parameters.AddWithValue("@Giustificativo", Giustificativo)
            cmdw.Parameters.AddWithValue("@GiustificativoSabato", GiustificativoSabato)
            cmdw.Parameters.AddWithValue("@GiustificativoFestivo", GiustificativoFestivo)
            cmdw.Parameters.AddWithValue("@Dipendenti", Dipendenti)
            cmdw.Parameters.AddWithValue("@DipendentiSabato", DipendentiSabato)
            cmdw.Parameters.AddWithValue("@DipendentiFestivo", DipendentiFestivo)
            cmdw.Parameters.AddWithValue("@Codice", Codice)
            cmdw.Parameters.AddWithValue("@Validita", Validita)
            cmdw.Parameters.AddWithValue("@Riga", Riga)
            cmdw.Connection = cn
            cmdw.ExecuteNonQuery()
        Else
            MySql = "INSERT INTO OrariPerGruppoRiga (Utente, DataAggiornamento, Codice, Validita, Riga, Orario, OrarioSabato, OrarioFestivo, Giustificativo, GiustificativoSabato, GiustificativoFestivo, Dipendenti, DipendentiSabato, DipendentiFestivo) VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?,?)"

            Dim cmdw As New OleDbCommand()
            cmdw.CommandText = MySql
            cmdw.Parameters.AddWithValue("@Utente", Utente)
            cmdw.Parameters.AddWithValue("@DataAggiornamento", Now)
            cmdw.Parameters.AddWithValue("@Codice", Codice)
            cmdw.Parameters.AddWithValue("@Validita", Validita)
            cmdw.Parameters.AddWithValue("@Riga", Riga)
            cmdw.Parameters.AddWithValue("@Orario", Orario)
            cmdw.Parameters.AddWithValue("@OrarioSabato", OrarioSabato)
            cmdw.Parameters.AddWithValue("@OrarioFestivo", OrarioFestivo)
            cmdw.Parameters.AddWithValue("@Giustificativo", Giustificativo)
            cmdw.Parameters.AddWithValue("@GiustificativoSabato", GiustificativoSabato)
            cmdw.Parameters.AddWithValue("@GiustificativoFestivo", GiustificativoFestivo)
            cmdw.Parameters.AddWithValue("@Dipendenti", Dipendenti)
            cmdw.Parameters.AddWithValue("@DipendentiSabato", DipendentiSabato)
            cmdw.Parameters.AddWithValue("@DipendentiFestivo", DipendentiFestivo)
            cmdw.Connection = cn
            cmdw.ExecuteNonQuery()
        End If
        myPOSTreader.Close()
        cn.Close()

        Call Pulisci()
    End Sub

    Sub LoadDati(ByVal ConnectionString As String, ByVal Tabella As System.Data.DataTable)
        Dim cn As OleDbConnection

        cn = New Data.OleDb.OleDbConnection(ConnectionString)

        cn.Open()
        Dim cmd As New OleDbCommand()

        cmd.CommandText = ("Select * From OrariPerGruppoRiga Order By Codice, Validita, Riga")

        cmd.Connection = cn

        Tabella.Clear()
        Tabella.Columns.Clear()
        Tabella.Columns.Add("Codice", GetType(String))
        Tabella.Columns.Add("Validita", GetType(String))
        Tabella.Columns.Add("Riga", GetType(String))
        Tabella.Columns.Add("Orario", GetType(String))
        Tabella.Columns.Add("OrarioSabato", GetType(String))
        Tabella.Columns.Add("OrarioFestivo", GetType(String))
        Tabella.Columns.Add("Giustificativo", GetType(String))
        Tabella.Columns.Add("Giustificativo Sabato", GetType(String))
        Tabella.Columns.Add("Giustificativo Festivo", GetType(String))
        Tabella.Columns.Add("Dipendenti", GetType(String))
        Tabella.Columns.Add("Dipendenti Sabato", GetType(String))
        Tabella.Columns.Add("Dipendenti Festivo", GetType(String))

        Dim myPOSTreader As OleDbDataReader = cmd.ExecuteReader()
        Do While myPOSTreader.Read

            Dim myriga As System.Data.DataRow = Tabella.NewRow()
            myriga(0) = myPOSTreader.Item("Codice")
            myriga(1) = myPOSTreader.Item("Validita")
            myriga(2) = myPOSTreader.Item("Riga")
            myriga(3) = myPOSTreader.Item("Orario")
            myriga(4) = myPOSTreader.Item("OrarioSabato")
            myriga(5) = myPOSTreader.Item("OrarioFestivo")
            myriga(6) = myPOSTreader.Item("Giustificativo")
            myriga(7) = myPOSTreader.Item("GiustificativoSabato")
            myriga(8) = myPOSTreader.Item("GiustificativoFestivo")
            myriga(9) = myPOSTreader.Item("Dipendenti")
            myriga(10) = myPOSTreader.Item("DipendentiSabato")
            myriga(11) = myPOSTreader.Item("DipendentiFestivo")

            Tabella.Rows.Add(myriga)
        Loop
        myPOSTreader.Close()
        cn.Close()

    End Sub

    Function StringaDb(ByVal Oggetto As Object) As String
        If IsDBNull(Oggetto) Then
            Return ""
        Else
            Return Oggetto
        End If
    End Function

    Function DataDb(ByVal Oggetto As Object) As Date
        If Not IsDate(Oggetto) Then
            Return Nothing
        Else
            Return Oggetto
        End If
    End Function

    Function NumeroDb(ByVal Oggetto As Object) As Object
        If IsDBNull(Oggetto) Then
            Return 0
        Else
            Return Oggetto
        End If
    End Function

    Sub Leggi(ByVal ConnectionString As String, ByVal xCodice As String, ByVal xValidita As Date, ByVal xRiga As Integer)
        Dim cn As OleDbConnection

        cn = New Data.OleDb.OleDbConnection(ConnectionString)

        cn.Open()
        Dim cmd As New OleDbCommand()
        cmd.CommandText = "Select * From OrariPerGruppoRiga WHERE " & _
                          " Codice = ?" & _
                          " AND Validita = ?" & _
                          " AND Riga = ?"
        cmd.Parameters.AddWithValue("@Codice", xCodice)
        cmd.Parameters.AddWithValue("@Validita", xValidita)
        cmd.Parameters.AddWithValue("@Riga", xRiga)
        cmd.Connection = cn

        Dim myPOSTreader As OleDbDataReader = cmd.ExecuteReader()
        If myPOSTreader.Read Then
            Id = NumeroDb(myPOSTreader.Item("Id"))
            Codice = StringaDb(myPOSTreader.Item("Codice"))
            Validita = DataDb(myPOSTreader.Item("Validita"))
            Riga = NumeroDb(myPOSTreader.Item("Riga"))
            Orario = StringaDb(myPOSTreader.Item("Orario"))
            OrarioSabato = StringaDb(myPOSTreader.Item("OrarioSabato"))
            OrarioFestivo = StringaDb(myPOSTreader.Item("OrarioFestivo"))
            Giustificativo = StringaDb(myPOSTreader.Item("Giustificativo"))
            GiustificativoSabato = StringaDb(myPOSTreader.Item("GiustificativoSabato"))
            GiustificativoFestivo = StringaDb(myPOSTreader.Item("GiustificativoFestivo"))
            Dipendenti = NumeroDb(myPOSTreader.Item("Dipendenti"))
            DipendentiSabato = NumeroDb(myPOSTreader.Item("DipendentiSabato"))
            DipendentiFestivo = NumeroDb(myPOSTreader.Item("DipendentiFestivo"))
        Else
            Call Pulisci()
        End If
        cn.Close()
    End Sub

    Function Esiste(ByVal ConnectionString As String, ByVal xCodice As String, ByVal xValidita As Date, ByVal xRiga As Integer) As Boolean
        Dim cn As OleDbConnection

        cn = New Data.OleDb.OleDbConnection(ConnectionString)

        cn.Open()
        Dim cmd As New OleDbCommand()
        cmd.CommandText = "Select * FROM OrariPerGruppoRiga" & _
                          " WHERE Codice = ?" & _
                          " AND Validita = ?" & _
                          " AND Riga = ?"
        cmd.Parameters.AddWithValue("@Codice", xCodice)
        cmd.Parameters.AddWithValue("@Validita", xValidita)
        cmd.Parameters.AddWithValue("@Riga", xRiga)
        cmd.Connection = cn

        Dim myPOSTreader As OleDbDataReader = cmd.ExecuteReader()

        If myPOSTreader.Read Then
            Return True
        Else
            Return False
        End If
        myPOSTreader.Close()
        cn.Close()
    End Function

End Class
