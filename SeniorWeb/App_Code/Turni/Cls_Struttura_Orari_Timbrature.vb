﻿Imports Microsoft.VisualBasic
Imports System.Data.OleDb
Imports System.Web.Hosting

Public Class Cls_Struttura_Orari_Timbrature
    Public Dalle(14, 31) As String
    Public Alle(14, 31) As String
    Public Pausa(14, 31) As String
    Public Causale(14, 31) As String
    Public Colore(14, 31) As Long
    Public Giustificativo(14, 31) As String
    Public NelGruppo(14, 31) As String
    Public Familiare(14, 31) As Byte
    Public Tipo(14, 31) As String
    Public Qualita(14, 31) As String
    Public TipoServizio(14, 31) As String
    Public GiornoSuccessivo(14, 31) As String

    Public Sub Pulisci()
        For i = 0 To 14
            For ii = 0 To 31
                Dalle(i, ii) = ""
                Alle(i, ii) = ""
                Pausa(i, ii) = ""
                Causale(i, ii) = ""
                Colore(i, ii) = 0
                Giustificativo(i, ii) = ""
                NelGruppo(i, ii) = ""
                Familiare(i, ii) = 0
                Tipo(i, ii) = ""
                Qualita(i, ii) = ""
                TipoServizio(i, ii) = ""
                GiornoSuccessivo(i, ii) = ""
            Next ii
        Next i
    End Sub

End Class
