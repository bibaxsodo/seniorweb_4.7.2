﻿Imports Microsoft.VisualBasic
Imports System.Data.OleDb
Imports System.Web.Hosting

Public Class Cls_SalvaOrdiniServizioTesta
    Public Id As Long
    Public Utente As String
    Public DataAggiornamento As Date
    Public DescrizioneSocieta As String
    Public CodiceGruppo As String
    Public Data As Date
    Public GgSett As String
    Public Giorno As Integer
    Public Mese As String
    Public Anno As Short
    Public Commento As String
    Public Tipo As String

    Public Sub Pulisci()
        Id = 0
        Utente = ""
        DataAggiornamento = Nothing
        DescrizioneSocieta = ""
        CodiceGruppo = ""
        Data = Nothing
        GgSett = ""
        Giorno = 0
        Mese = ""
        Anno = 0
        Commento = ""
        Tipo = ""
    End Sub

End Class
