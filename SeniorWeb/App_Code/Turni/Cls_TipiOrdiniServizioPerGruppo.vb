Imports Microsoft.VisualBasic
Imports System.Data.OleDb
Imports System.Web.Hosting

Public Class Cls_TipiOrdiniServizioPerGruppo
    Public Id As Long
    Public Utente As String
    Public DataAggiornamento As Date
    Public Codice As String
    Public Gruppo As String
    Public Dipendente As String
    Public Mezzo As String
    Public Apparato As String
    Public Canale As String
    Public DipendenteStraordinario As String
    Public MezzoStraordinario As String
    Public ApparatoStraordinario As String
    Public CanaleStraordinario As String

    Public Sub Pulisci()
        Id = 0
        Utente = ""
        DataAggiornamento = ""
        Codice = ""
        Gruppo = ""
        Dipendente = ""
        Mezzo = ""
        Apparato = ""
        Canale = ""
        DipendenteStraordinario = ""
        MezzoStraordinario = ""
        ApparatoStraordinario = ""
        CanaleStraordinario = ""
    End Sub
End Class
