Imports Microsoft.VisualBasic
Imports System.Data.OleDb
Imports System.Web.Hosting

Public Class Cls_OrdiniServizioTestMensile
    Public Id As Long
    Public Utente As String
    Public DataAggiornamento As Date
    Public Gruppo As String
    Public Anno As Short
    Public Mese As Byte

    Public Sub Pulisci()
        Id = 0
        Utente = ""
        DataAggiornamento = Nothing
        Gruppo = ""
        Anno = 0
        Mese = 0
    End Sub
End Class
