Imports Microsoft.VisualBasic
Imports System.Data.OleDb
Imports System.Web.Hosting

Public Class Cls_OrarioMensile
    Public Id As Long
    Public Utente As String
    Public DataAggiornamento As Date
    Public Anno As Short
    Public Codice As String
    Public TempoLavoro1 As Single
    Public TempoLavoro2 As Single
    Public TempoLavoro3 As Single
    Public TempoLavoro4 As Single
    Public TempoLavoro5 As Single
    Public TempoLavoro6 As Single
    Public TempoLavoro7 As Single
    Public TempoLavoro8 As Single
    Public TempoLavoro9 As Single
    Public TempoLavoro10 As Single
    Public TempoLavoro11 As Single
    Public TempoLavoro12 As Single
    Public GiorniLavorativi1 As Byte
    Public GiorniLavorativi2 As Byte
    Public GiorniLavorativi3 As Byte
    Public GiorniLavorativi4 As Byte
    Public GiorniLavorativi5 As Byte
    Public GiorniLavorativi6 As Byte
    Public GiorniLavorativi7 As Byte
    Public GiorniLavorativi8 As Byte
    Public GiorniLavorativi9 As Byte
    Public GiorniLavorativi10 As Byte
    Public GiorniLavorativi11 As Byte
    Public GiorniLavorativi12 As Byte

    Public Sub Pulisci()
        Id = 0
        Utente = ""
        DataAggiornamento = Nothing
        Anno = 0
        Codice = ""
        TempoLavoro1 = 0
        TempoLavoro2 = 0
        TempoLavoro3 = 0
        TempoLavoro4 = 0
        TempoLavoro5 = 0
        TempoLavoro6 = 0
        TempoLavoro7 = 0
        TempoLavoro8 = 0
        TempoLavoro9 = 0
        TempoLavoro10 = 0
        TempoLavoro11 = 0
        TempoLavoro12 = 0
        GiorniLavorativi1 = 0
        GiorniLavorativi2 = 0
        GiorniLavorativi3 = 0
        GiorniLavorativi4 = 0
        GiorniLavorativi5 = 0
        GiorniLavorativi6 = 0
        GiorniLavorativi7 = 0
        GiorniLavorativi8 = 0
        GiorniLavorativi9 = 0
        GiorniLavorativi10 = 0
        GiorniLavorativi11 = 0
        GiorniLavorativi12 = 0
    End Sub

    Public Sub Elimina(ByVal StringaConnessione As String, ByVal xAnno As String, ByVal xCodice As String)
        Dim cn As OleDbConnection
        Dim MySql As String

        MySql = ""

        cn = New Data.OleDb.OleDbConnection(StringaConnessione)

        cn.Open()
        Dim cmd As New OleDbCommand()
        cmd.CommandText = "DELETE FROM OrarioMensile" & _
                          " WHERE Anno = ?" & _
                          " AND Codice = ?"
        cmd.Parameters.AddWithValue("@Anno", xAnno)
        cmd.Parameters.AddWithValue("@Codice", xCodice)
        cmd.Connection = cn
        cmd.ExecuteNonQuery()
        cn.Close()
    End Sub

    Sub Aggiorna(ByVal StringaConnessione As String)
        Dim cn As OleDbConnection
        Dim MySql As String

        MySql = ""

        cn = New Data.OleDb.OleDbConnection(StringaConnessione)

        cn.Open()
        Dim cmd As New OleDbCommand()
        cmd.CommandText = "SELECT * FROM OrarioMensile" & _
                          " WHERE Anno = ?" & _
                          " AND Codice = ?"
        cmd.Parameters.AddWithValue("@Anno", Anno)
        cmd.Parameters.AddWithValue("@Codice", Codice)
        cmd.Connection = cn
        Dim myPOSTreader As OleDbDataReader = cmd.ExecuteReader()
        If myPOSTreader.Read Then
            MySql = "UPDATE OrarioMensile SET " & _
                    " Utente = ?, " & _
                    " DataAggiornamento  = ?," & _
                    " TempoLavoro1 = ?," & _
                    " TempoLavoro2 = ?," & _
                    " TempoLavoro3 = ?," & _
                    " TempoLavoro4 = ?," & _
                    " TempoLavoro5 = ?," & _
                    " TempoLavoro6 = ?," & _
                    " TempoLavoro7 = ?," & _
                    " TempoLavoro8 = ?," & _
                    " TempoLavoro9 = ?," & _
                    " TempoLavoro10 = ?," & _
                    " TempoLavoro11 = ?," & _
                    " TempoLavoro12 = ?" & _
                    " GiorniLavorativi1 = ?," & _
                    " GiorniLavorativi2 = ?," & _
                    " GiorniLavorativi3 = ?," & _
                    " GiorniLavorativi4 = ?," & _
                    " GiorniLavorativi5 = ?," & _
                    " GiorniLavorativi6 = ?," & _
                    " GiorniLavorativi7 = ?," & _
                    " GiorniLavorativi8 = ?," & _
                    " GiorniLavorativi9 = ?," & _
                    " GiorniLavorativi10 = ?," & _
                    " GiorniLavorativi11 = ?," & _
                    " GiorniLavorativi12 = ?," & _
                    " WHERE Anno = ?" & _
                    " AND Codice = ?"

            Dim cmdw As New OleDbCommand()
            cmdw.CommandText = MySql
            cmdw.Parameters.AddWithValue("@Utente", Utente)
            cmdw.Parameters.AddWithValue("@DataAggiornamento", Now)
            cmdw.Parameters.AddWithValue("@TempoLavoro1", TempoLavoro1)
            cmdw.Parameters.AddWithValue("@TempoLavoro2", TempoLavoro2)
            cmdw.Parameters.AddWithValue("@TempoLavoro3", TempoLavoro3)
            cmdw.Parameters.AddWithValue("@TempoLavoro4", TempoLavoro4)
            cmdw.Parameters.AddWithValue("@TempoLavoro5", TempoLavoro5)
            cmdw.Parameters.AddWithValue("@TempoLavoro6", TempoLavoro6)
            cmdw.Parameters.AddWithValue("@TempoLavoro7", TempoLavoro7)
            cmdw.Parameters.AddWithValue("@TempoLavoro8", TempoLavoro8)
            cmdw.Parameters.AddWithValue("@TempoLavoro9", TempoLavoro9)
            cmdw.Parameters.AddWithValue("@TempoLavoro10", TempoLavoro10)
            cmdw.Parameters.AddWithValue("@TempoLavoro11", TempoLavoro11)
            cmdw.Parameters.AddWithValue("@TempoLavoro12", TempoLavoro12)
            cmdw.Parameters.AddWithValue("@GiorniLavorativi1", GiorniLavorativi1)
            cmdw.Parameters.AddWithValue("@GiorniLavorativi2", GiorniLavorativi2)
            cmdw.Parameters.AddWithValue("@GiorniLavorativi3", GiorniLavorativi3)
            cmdw.Parameters.AddWithValue("@GiorniLavorativi4", GiorniLavorativi4)
            cmdw.Parameters.AddWithValue("@GiorniLavorativi5", GiorniLavorativi5)
            cmdw.Parameters.AddWithValue("@GiorniLavorativi6", GiorniLavorativi6)
            cmdw.Parameters.AddWithValue("@GiorniLavorativi7", GiorniLavorativi7)
            cmdw.Parameters.AddWithValue("@GiorniLavorativi8", GiorniLavorativi8)
            cmdw.Parameters.AddWithValue("@GiorniLavorativi9", GiorniLavorativi9)
            cmdw.Parameters.AddWithValue("@GiorniLavorativi10", GiorniLavorativi10)
            cmdw.Parameters.AddWithValue("@GiorniLavorativi11", GiorniLavorativi11)
            cmdw.Parameters.AddWithValue("@GiorniLavorativi12", GiorniLavorativi12)
            cmdw.Parameters.AddWithValue("@Anno", Anno)
            cmdw.Parameters.AddWithValue("@Codice", Codice)
            cmdw.Connection = cn
            cmdw.ExecuteNonQuery()
        Else
            MySql = "INSERT INTO OrarioMensile (Utente, DataAggiornamento, Anno, Codice, TempoLavoro1, TempoLavoro2, TempoLavoro3, TempoLavoro4, TempoLavoro5, TempoLavoro6, TempoLavoro7, TempoLavoro8, TempoLavoro9, TempoLavoro10, TempoLavoro11, TempoLavoro12, GiorniLavorativi1, GiorniLavorativi2, GiorniLavorativi3, GiorniLavorativi4, GiorniLavorativi5, GiorniLavorativi6, GiorniLavorativi7, GiorniLavorativi8, GiorniLavorativi9, GiorniLavorativi10, GiorniLavorativi11, GiorniLavorativi12) VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)"

            Dim cmdw As New OleDbCommand()
            cmdw.CommandText = MySql
            cmdw.Parameters.AddWithValue("@Utente", Utente)
            cmdw.Parameters.AddWithValue("@DataAggiornamento", Now)
            cmdw.Parameters.AddWithValue("@Anno", Anno)
            cmdw.Parameters.AddWithValue("@Codice", Codice)
            cmdw.Parameters.AddWithValue("@TempoLavoro1", TempoLavoro1)
            cmdw.Parameters.AddWithValue("@TempoLavoro2", TempoLavoro2)
            cmdw.Parameters.AddWithValue("@TempoLavoro3", TempoLavoro3)
            cmdw.Parameters.AddWithValue("@TempoLavoro4", TempoLavoro4)
            cmdw.Parameters.AddWithValue("@TempoLavoro5", TempoLavoro5)
            cmdw.Parameters.AddWithValue("@TempoLavoro6", TempoLavoro6)
            cmdw.Parameters.AddWithValue("@TempoLavoro7", TempoLavoro7)
            cmdw.Parameters.AddWithValue("@TempoLavoro8", TempoLavoro8)
            cmdw.Parameters.AddWithValue("@TempoLavoro9", TempoLavoro9)
            cmdw.Parameters.AddWithValue("@TempoLavoro10", TempoLavoro10)
            cmdw.Parameters.AddWithValue("@TempoLavoro11", TempoLavoro11)
            cmdw.Parameters.AddWithValue("@TempoLavoro12", TempoLavoro12)
            cmdw.Parameters.AddWithValue("@GiorniLavorativi1", GiorniLavorativi1)
            cmdw.Parameters.AddWithValue("@GiorniLavorativi2", GiorniLavorativi2)
            cmdw.Parameters.AddWithValue("@GiorniLavorativi3", GiorniLavorativi3)
            cmdw.Parameters.AddWithValue("@GiorniLavorativi4", GiorniLavorativi4)
            cmdw.Parameters.AddWithValue("@GiorniLavorativi5", GiorniLavorativi5)
            cmdw.Parameters.AddWithValue("@GiorniLavorativi6", GiorniLavorativi6)
            cmdw.Parameters.AddWithValue("@GiorniLavorativi7", GiorniLavorativi7)
            cmdw.Parameters.AddWithValue("@GiorniLavorativi8", GiorniLavorativi8)
            cmdw.Parameters.AddWithValue("@GiorniLavorativi9", GiorniLavorativi9)
            cmdw.Parameters.AddWithValue("@GiorniLavorativi10", GiorniLavorativi10)
            cmdw.Parameters.AddWithValue("@GiorniLavorativi11", GiorniLavorativi11)
            cmdw.Parameters.AddWithValue("@GiorniLavorativi12", GiorniLavorativi12)
            cmdw.Connection = cn
            cmdw.ExecuteNonQuery()
        End If
        myPOSTreader.Close()
        cn.Close()

        Call Pulisci()
    End Sub

    Sub LoadDati(ByVal StringaConnessione As String, ByVal Tabella As System.Data.DataTable)
        Dim cn As OleDbConnection

        cn = New Data.OleDb.OleDbConnection(StringaConnessione)

        cn.Open()
        Dim cmd As New OleDbCommand()

        cmd.CommandText = ("Select * From OrarioMensile Order By Anno, Codice")

        cmd.Connection = cn

        Tabella.Clear()
        Tabella.Columns.Clear()
        Tabella.Columns.Add("Anno", GetType(String))
        Tabella.Columns.Add("Codice", GetType(String))
        Tabella.Columns.Add("Gennaio", GetType(String))
        Tabella.Columns.Add("Febbraio", GetType(String))
        Tabella.Columns.Add("Marzo", GetType(String))
        Tabella.Columns.Add("Aprile", GetType(String))
        Tabella.Columns.Add("Maggio", GetType(String))
        Tabella.Columns.Add("Giugno", GetType(String))
        Tabella.Columns.Add("Luglio", GetType(String))
        Tabella.Columns.Add("Agosto", GetType(String))
        Tabella.Columns.Add("Settembre", GetType(String))
        Tabella.Columns.Add("Ottobre", GetType(String))
        Tabella.Columns.Add("Novembre", GetType(String))
        Tabella.Columns.Add("Dicembre", GetType(String))

        Dim myPOSTreader As OleDbDataReader = cmd.ExecuteReader()
        Do While myPOSTreader.Read

            Dim myriga As System.Data.DataRow = Tabella.NewRow()
            myriga(0) = myPOSTreader.Item("Anno")
            myriga(1) = myPOSTreader.Item("Codice")
            myriga(2) = myPOSTreader.Item("TempoLavoro1")
            myriga(3) = myPOSTreader.Item("TempoLavoro2")
            myriga(4) = myPOSTreader.Item("TempoLavoro3")
            myriga(5) = myPOSTreader.Item("TempoLavoro4")
            myriga(6) = myPOSTreader.Item("TempoLavoro5")
            myriga(7) = myPOSTreader.Item("TempoLavoro6")
            myriga(8) = myPOSTreader.Item("TempoLavoro7")
            myriga(9) = myPOSTreader.Item("TempoLavoro8")
            myriga(10) = myPOSTreader.Item("TempoLavoro9")
            myriga(11) = myPOSTreader.Item("TempoLavoro10")
            myriga(12) = myPOSTreader.Item("TempoLavoro11")
            myriga(13) = myPOSTreader.Item("TempoLavoro12")

            Tabella.Rows.Add(myriga)
        Loop
        myPOSTreader.Close()
        cn.Close()

    End Sub

    Function StringaDb(ByVal Oggetto As Object) As String
        If IsDBNull(Oggetto) Then
            Return ""
        Else
            Return Oggetto
        End If
    End Function

    Function NumeroDb(ByVal Oggetto As Object) As Object
        If IsDBNull(Oggetto) Then
            Return 0
        Else
            Return Oggetto
        End If
    End Function

    Sub Leggi(ByVal StringaConnessione As String, ByVal xAnno As String, ByVal xCodice As String)
        Dim cn As OleDbConnection

        cn = New Data.OleDb.OleDbConnection(StringaConnessione)

        cn.Open()
        Dim cmd As New OleDbCommand()
        cmd.CommandText = "SELECT * FROM OrarioMensile" & _
                          " WHERE Anno = ?" & _
                          " AND Codice = ?"
        cmd.Parameters.AddWithValue("@Anno", xAnno)
        cmd.Parameters.AddWithValue("@Codice", xCodice)
        cmd.Connection = cn

        Dim myPOSTreader As OleDbDataReader = cmd.ExecuteReader()
        If myPOSTreader.Read Then
            Id = NumeroDb(myPOSTreader.Item("Id"))
            Anno = NumeroDb(myPOSTreader.Item("Anno"))
            Codice = StringaDb(myPOSTreader.Item("Codice"))
            TempoLavoro1 = NumeroDb(myPOSTreader.Item("TempoLavoro1"))
            TempoLavoro2 = NumeroDb(myPOSTreader.Item("TempoLavoro2"))
            TempoLavoro3 = NumeroDb(myPOSTreader.Item("TempoLavoro3"))
            TempoLavoro4 = NumeroDb(myPOSTreader.Item("TempoLavoro4"))
            TempoLavoro5 = NumeroDb(myPOSTreader.Item("TempoLavoro5"))
            TempoLavoro6 = NumeroDb(myPOSTreader.Item("TempoLavoro6"))
            TempoLavoro7 = NumeroDb(myPOSTreader.Item("TempoLavoro7"))
            TempoLavoro8 = NumeroDb(myPOSTreader.Item("TempoLavoro8"))
            TempoLavoro9 = NumeroDb(myPOSTreader.Item("TempoLavoro9"))
            TempoLavoro10 = NumeroDb(myPOSTreader.Item("TempoLavoro10"))
            TempoLavoro11 = NumeroDb(myPOSTreader.Item("TempoLavoro11"))
            TempoLavoro12 = NumeroDb(myPOSTreader.Item("TempoLavoro12"))
            GiorniLavorativi1 = NumeroDb(myPOSTreader.Item("GiorniLavorativi1"))
            GiorniLavorativi2 = NumeroDb(myPOSTreader.Item("GiorniLavorativi2"))
            GiorniLavorativi3 = NumeroDb(myPOSTreader.Item("GiorniLavorativi3"))
            GiorniLavorativi4 = NumeroDb(myPOSTreader.Item("GiorniLavorativi4"))
            GiorniLavorativi5 = NumeroDb(myPOSTreader.Item("GiorniLavorativi5"))
            GiorniLavorativi6 = NumeroDb(myPOSTreader.Item("GiorniLavorativi6"))
            GiorniLavorativi7 = NumeroDb(myPOSTreader.Item("GiorniLavorativi7"))
            GiorniLavorativi8 = NumeroDb(myPOSTreader.Item("GiorniLavorativi8"))
            GiorniLavorativi9 = NumeroDb(myPOSTreader.Item("GiorniLavorativi9"))
            GiorniLavorativi10 = NumeroDb(myPOSTreader.Item("GiorniLavorativi10"))
            GiorniLavorativi11 = NumeroDb(myPOSTreader.Item("GiorniLavorativi11"))
            GiorniLavorativi12 = NumeroDb(myPOSTreader.Item("GiorniLavorativi12"))
        Else
            Call Pulisci()
        End If
        cn.Close()
    End Sub

    Function Esiste(ByVal ConnectionString As String, ByVal xAnno As String, ByVal xCodice As String) As Boolean
        Dim cn As OleDbConnection

        cn = New Data.OleDb.OleDbConnection(ConnectionString)

        cn.Open()
        Dim cmd As New OleDbCommand()
        cmd.CommandText = "SELECT * FROM OrarioMensile" & _
                          " WHERE Anno = ?" & _
                          " AND Codice = ?"
        cmd.Parameters.AddWithValue("@Anno", xAnno)
        cmd.Parameters.AddWithValue("@Codice", xCodice)
        cmd.Connection = cn

        Dim myPOSTreader As OleDbDataReader = cmd.ExecuteReader()

        If myPOSTreader.Read Then
            Return True
        Else
            Return False
        End If
        myPOSTreader.Close()
        cn.Close()
    End Function

    Function TempoLavoroMese(ByVal StringaConnessione As String, ByVal xAnno As String, ByVal xCodice As String, ByVal xMese As Byte) As Single
        Dim cn As OleDbConnection

        TempoLavoroMese = 0

        cn = New Data.OleDb.OleDbConnection(StringaConnessione)

        cn.Open()
        Dim cmd As New OleDbCommand()
        cmd.CommandText = "SELECT * FROM OrarioMensile" & _
                          " WHERE Anno = ?" & _
                          " AND Codice = ?"
        cmd.Parameters.AddWithValue("@Anno", xAnno)
        cmd.Parameters.AddWithValue("@Codice", xCodice)
        cmd.Connection = cn

        Dim myPOSTreader As OleDbDataReader = cmd.ExecuteReader()
        If myPOSTreader.Read Then
            TempoLavoroMese = NumeroDb(myPOSTreader.Item("TempoLavoro" & xMese))
        End If
        cn.Close()
    End Function

    Function GiorniLavorativiMese(ByVal StringaConnessione As String, ByVal xAnno As String, ByVal xCodice As String, ByVal xMese As Byte) As Single
        Dim cn As OleDbConnection

        GiorniLavorativiMese = 0

        cn = New Data.OleDb.OleDbConnection(StringaConnessione)

        cn.Open()
        Dim cmd As New OleDbCommand()
        cmd.CommandText = "SELECT * FROM OrarioMensile" & _
                          " WHERE Anno = ?" & _
                          " AND Codice = ?"
        cmd.Parameters.AddWithValue("@Anno", xAnno)
        cmd.Parameters.AddWithValue("@Codice", xCodice)
        cmd.Connection = cn

        Dim myPOSTreader As OleDbDataReader = cmd.ExecuteReader()
        If myPOSTreader.Read Then
            GiorniLavorativiMese = NumeroDb(myPOSTreader.Item("GiorniLavorativi" & xMese))
        End If
        cn.Close()
    End Function

End Class
