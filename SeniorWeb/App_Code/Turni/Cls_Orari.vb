Imports Microsoft.VisualBasic
Imports System.Data.OleDb
Imports System.Web.Hosting

Public Class Cls_Orari
    Public Id As Long
    Public Utente As String
    Public DataAggiornamento As Date
    Public Codice As String
    Public Descrizione As String
    Public Dalle As Date
    Public Alle As Date
    Public Giustificativo As String
    Public Jolly As Date
    Public Pausa As Date
    Public DalleAlto As Date
    Public DalleBasso As Date
    Public AlleAlto As Date
    Public AlleBasso As Date
    Public DalleObbligo As Date
    Public AlleObbligo As Date
    Public OrarioGiornoFestivo As String
    Public OrarioGiornoFestivoInfrasettimanale As String
    Public NonVisualizzareDal As Date
    Public TipoControllo As String
    Public Eventuale As String
    Public Aliax As String
    Public SoloPreferenze As String

    Public Sub Pulisci()
        Id = 0
        Utente = ""
        DataAggiornamento = Nothing
        Codice = ""
        Descrizione = ""
        Dalle = Nothing
        Alle = Nothing
        Giustificativo = ""
        Jolly = Nothing
        Pausa = Nothing
        DalleAlto = Nothing
        DalleBasso = Nothing
        AlleAlto = Nothing
        AlleBasso = Nothing
        DalleObbligo = Nothing
        AlleObbligo = Nothing
        OrarioGiornoFestivo = ""
        OrarioGiornoFestivoInfrasettimanale = ""
        NonVisualizzareDal = Nothing
        TipoControllo = ""
        Eventuale = ""
        Aliax = ""
        SoloPreferenze = ""
    End Sub

    Public Sub Elimina(ByVal StringaConnessione As String, ByVal xCodice As String)
        Dim cn As OleDbConnection

        cn = New Data.OleDb.OleDbConnection(StringaConnessione)

        cn.Open()
        Dim cmd As New OleDbCommand()
        cmd.CommandText = ("DELETE FROM Orari where Codice = ?")
        cmd.Parameters.AddWithValue("@Codice", xCodice)
        cmd.Connection = cn
        cmd.ExecuteNonQuery()
        cn.Close()
    End Sub

    Sub Aggiorna(ByVal StringaConnessione As String)
        Dim cn As OleDbConnection

        cn = New Data.OleDb.OleDbConnection(StringaConnessione)

        cn.Open()
        Dim cmd As New OleDbCommand()
        cmd.CommandText = "SELECT * FROM Orari WHERE Codice = ?"
        cmd.Parameters.AddWithValue("@Codice", Codice)
        cmd.Connection = cn
        Dim myPOSTreader As OleDbDataReader = cmd.ExecuteReader()
        Dim cmdw As New OleDbCommand()
        If myPOSTreader.Read Then
            cmdw.CommandText = "UPDATE Orari SET " & _
                               " Utente = ?," & _
                               " DataAggiornamento  = ?," & _
                               " Descrizione = ?," & _
                               " Dalle = ?," & _
                               " Alle = ?," & _
                               " Giustificativo = ?," & _
                               " Jolly = ?," & _
                               " Pausa = ?," & _
                               " DalleAlto = ?," & _
                               " DalleBasso = ?," & _
                               " AlleAlto = ?," & _
                               " AlleBasso = ?," & _
                               " DalleObbligo = ?," & _
                               " AlleObbligo = ?," & _
                               " OrarioGiornoFestivo = ?," & _
                               " OrarioGiornoFestivoInfrasettimanale = ?," & _
                               " NonVisualizzareDal = ?," & _
                               " TipoControllo = ?," & _
                               " Eventuale = ?," & _
                               " Alias = ?," & _
                               " SoloPreferenze = ?" & _
                               " WHERE Codice = ?"

            cmdw.Parameters.AddWithValue("@Utente", Utente)
            cmdw.Parameters.AddWithValue("@DataAggiornamento ", Now)
            cmdw.Parameters.AddWithValue("@Descrizione", Descrizione)
            cmdw.Parameters.AddWithValue("@Dalle", Dalle)
            cmdw.Parameters.AddWithValue("@Alle", Alle)
            cmdw.Parameters.AddWithValue("@Giustificativo", Giustificativo)
            cmdw.Parameters.AddWithValue("@Jolly", Jolly)
            cmdw.Parameters.AddWithValue("@Pausa", Pausa)
            cmdw.Parameters.AddWithValue("@DalleAlto", DalleAlto)
            cmdw.Parameters.AddWithValue("@DalleBasso", DalleBasso)
            cmdw.Parameters.AddWithValue("@AlleAlto", AlleAlto)
            cmdw.Parameters.AddWithValue("@AlleBasso", AlleBasso)
            cmdw.Parameters.AddWithValue("@DalleObbligo", DalleObbligo)
            cmdw.Parameters.AddWithValue("@AlleObbligo", AlleObbligo)
            cmdw.Parameters.AddWithValue("@OrarioGiornoFestivo", OrarioGiornoFestivo)
            cmdw.Parameters.AddWithValue("@OrarioGiornoFestivoInfrasettimanale", OrarioGiornoFestivoInfrasettimanale)
            cmdw.Parameters.AddWithValue("@NonVisualizzareDal", IIf(Year(NonVisualizzareDal) > 1, NonVisualizzareDal, System.DBNull.Value))
            cmdw.Parameters.AddWithValue("@TipoControllo", TipoControllo)
            cmdw.Parameters.AddWithValue("@Eventuale", Eventuale)
            cmdw.Parameters.AddWithValue("@Alias", Aliax)
            cmdw.Parameters.AddWithValue("@SoloPreferenze", SoloPreferenze)
            cmdw.Parameters.AddWithValue("@Codice", Codice)
            cmdw.Connection = cn
            cmdw.ExecuteNonQuery()
        Else
            cmdw.CommandText = "INSERT INTO Orari (Utente, DataAggiornamento, Codice, Descrizione, Dalle, Alle, Giustificativo, Jolly, Pausa, DalleAlto, DalleBasso, AlleAlto, AlleBasso, DalleObbligo, AlleObbligo, OrarioGiornoFestivo,OrarioGiornoFestivoInfrasettimanale, NonVisualizzareDal, TipoControllo, Eventuale, Aliax, SoloPreferenze) VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)"

            cmdw.Parameters.AddWithValue("@Utente", Utente)
            cmdw.Parameters.AddWithValue("@DataAggiornamento", Now)
            cmdw.Parameters.AddWithValue("@Codice", Codice)
            cmdw.Parameters.AddWithValue("@Descrizione", Descrizione)
            cmdw.Parameters.AddWithValue("@Dalle", Dalle)
            cmdw.Parameters.AddWithValue("@Alle", Alle)
            cmdw.Parameters.AddWithValue("@Giustificativo", Giustificativo)
            cmdw.Parameters.AddWithValue("@Jolly", Jolly)
            cmdw.Parameters.AddWithValue("@Pausa", Pausa)
            cmdw.Parameters.AddWithValue("@DalleAlto", DalleAlto)
            cmdw.Parameters.AddWithValue("@DalleBasso", DalleBasso)
            cmdw.Parameters.AddWithValue("@AlleAlto", AlleAlto)
            cmdw.Parameters.AddWithValue("@AlleBasso", AlleBasso)
            cmdw.Parameters.AddWithValue("@DalleObbligo", DalleObbligo)
            cmdw.Parameters.AddWithValue("@AlleObbligo", AlleObbligo)
            cmdw.Parameters.AddWithValue("@OrarioGiornoFestivo", OrarioGiornoFestivo)
            cmdw.Parameters.AddWithValue("@OrarioGiornoFestivoInfrasettimanale", OrarioGiornoFestivoInfrasettimanale)
            cmdw.Parameters.AddWithValue("@NonVisualizzareDal", IIf(Year(NonVisualizzareDal) > 1, NonVisualizzareDal, System.DBNull.Value))
            cmdw.Parameters.AddWithValue("@TipoControllo", TipoControllo)
            cmdw.Parameters.AddWithValue("@Eventuale", Eventuale)
            cmdw.Parameters.AddWithValue("@Alias", Aliax)
            cmdw.Parameters.AddWithValue("@SoloPreferenze", SoloPreferenze)
            cmdw.Connection = cn
            cmdw.ExecuteNonQuery()
        End If
        myPOSTreader.Close()
        cn.Close()

        Call Pulisci()
    End Sub

    Sub LoadDati(ByVal StringaConnessione As String, ByVal Tabella As System.Data.DataTable)
        Dim cn As OleDbConnection

        cn = New Data.OleDb.OleDbConnection(StringaConnessione)

        cn.Open()
        Dim cmd As New OleDbCommand()

        cmd.CommandText = ("Select * From Orari Order By Descrizione")

        cmd.Connection = cn

        Tabella.Clear()
        Tabella.Columns.Add("Codice", GetType(String))
        Tabella.Columns.Add("Descrizione", GetType(String))

        Dim myPOSTreader As OleDbDataReader = cmd.ExecuteReader()
        Do While myPOSTreader.Read

            Dim myriga As System.Data.DataRow = Tabella.NewRow()
            myriga(0) = myPOSTreader.Item("Codice")
            myriga(1) = myPOSTreader.Item("Descrizione")

            Tabella.Rows.Add(myriga)
        Loop
        myPOSTreader.Close()
        cn.Close()

    End Sub

    Function StringaDb(ByVal Oggetto As Object) As String
        If IsDBNull(Oggetto) Then
            Return ""
        Else
            Return Oggetto
        End If
    End Function

    Function DataDb(ByVal Oggetto As Object) As Date
        If Not IsDate(Oggetto) Then
            Return Nothing
        Else
            Return Oggetto
        End If
    End Function

    Function NumeroDb(ByVal Oggetto As Object) As Object
        If IsDBNull(Oggetto) Then
            Return 0
        Else
            Return Oggetto
        End If
    End Function

    Sub Leggi(ByVal StringaConnessione As String, ByVal xCodice As String)
        Dim cn As OleDbConnection

        cn = New Data.OleDb.OleDbConnection(StringaConnessione)

        cn.Open()
        Dim cmd As New OleDbCommand()
        cmd.CommandText = "Select * From Orari" & _
                            " Where Codice = ?"
        cmd.Parameters.AddWithValue("@Codice", xCodice)
        cmd.Connection = cn

        Dim myPOSTreader As OleDbDataReader = cmd.ExecuteReader()
        If myPOSTreader.Read Then
            Id = NumeroDb(myPOSTreader.Item("Id"))
            Codice = StringaDb(myPOSTreader.Item("Codice"))
            Descrizione = StringaDb(myPOSTreader.Item("Descrizione"))
            Dalle = DataDb(myPOSTreader.Item("Dalle"))
            Alle = DataDb(myPOSTreader.Item("Alle"))
            Giustificativo = StringaDb(myPOSTreader.Item("Giustificativo"))
            Jolly = DataDb(myPOSTreader.Item("Jolly"))
            Pausa = DataDb(myPOSTreader.Item("Pausa"))
            DalleAlto = DataDb(myPOSTreader.Item("DalleAlto"))
            DalleBasso = DataDb(myPOSTreader.Item("DalleBasso"))
            AlleAlto = DataDb(myPOSTreader.Item("AlleAlto"))
            AlleBasso = DataDb(myPOSTreader.Item("AlleBasso"))
            DalleObbligo = DataDb(myPOSTreader.Item("DalleObbligo"))
            AlleObbligo = DataDb(myPOSTreader.Item("AlleObbligo"))
            OrarioGiornoFestivo = StringaDb(myPOSTreader.Item("OrarioGiornoFestivo"))
            OrarioGiornoFestivoInfrasettimanale = StringaDb(myPOSTreader.Item("OrarioGiornoFestivoInfrasettimanale"))
            NonVisualizzareDal = DataDb(myPOSTreader.Item("NonVisualizzareDal"))
            TipoControllo = StringaDb(myPOSTreader.Item("TipoControllo"))
            Eventuale = StringaDb(myPOSTreader.Item("Eventuale"))
            Aliax = StringaDb(myPOSTreader.Item("Alias"))
            SoloPreferenze = StringaDb(myPOSTreader.Item("SoloPreferenze"))

        Else
            Call Pulisci()
        End If
        cn.Close()
    End Sub

    Function CampoOrari(ByVal ConnectionString As String, ByVal xCodice As String, ByVal xCampo As String) As Object
        Dim cn As OleDbConnection

        CampoOrari = ""

        cn = New Data.OleDb.OleDbConnection(ConnectionString)

        cn.Open()
        Dim cmd As New OleDbCommand()
        cmd.CommandText = "Select " & xCampo & " FROM Orari WHERE Codice = ?"

        cmd.Parameters.AddWithValue("@Codice", xCodice)
        cmd.Connection = cn

        Dim myPOSTreader As OleDbDataReader = cmd.ExecuteReader()
        If myPOSTreader.Read Then
            Return myPOSTreader.Item(xCampo)
        End If
        myPOSTreader.Close()
        cn.Close()
    End Function

    Function Conta(ByVal StringaConnessione As String) As Long
        Dim cn As OleDbConnection

        cn = New Data.OleDb.OleDbConnection(StringaConnessione)

        cn.Open()
        Dim cmd As New OleDbCommand()
        cmd.CommandText = "SELECT COUNT(*) FROM Orari"

        cmd.Connection = cn

        Dim myPOSTreader As OleDbDataReader = cmd.ExecuteReader()
        If myPOSTreader.Read Then
            Conta = NumeroDb(myPOSTreader.Item(0))
        Else
            Conta = 0
        End If
        cn.Close()
    End Function

    Function OrarioConGiustificativo(ByVal ConnectionString As String, ByVal xGiustificativo As String) As Object
        Dim cn As OleDbConnection

        OrarioConGiustificativo = ""

        cn = New Data.OleDb.OleDbConnection(ConnectionString)

        cn.Open()
        Dim cmd As New OleDbCommand()
        cmd.CommandText = "Select Codice FROM Orari WHERE Giustificativo = ?"

        cmd.Parameters.AddWithValue("@Giustificativo", xGiustificativo)
        cmd.Connection = cn

        Dim myPOSTreader As OleDbDataReader = cmd.ExecuteReader()
        If myPOSTreader.Read Then
            Return myPOSTreader.Item("Codice")
        End If
        myPOSTreader.Close()
        cn.Close()
    End Function

    Sub UpDateDropBox(ByVal StringaConnessione As String, ByRef Appoggio As DropDownList)
        Dim cn As OleDbConnection

        cn = New Data.OleDb.OleDbConnection(StringaConnessione)

        cn.Open()
        Dim cmd As New OleDbCommand()
        cmd.CommandText = ("SELECT Codice, Descrizione FROM Orari ORDER BY Descrizione")
        cmd.Connection = cn

        Appoggio.Items.Clear()
        Dim myPOSTreader As OleDbDataReader = cmd.ExecuteReader()
        Do While myPOSTreader.Read
            Appoggio.Items.Add(myPOSTreader.Item("Descrizione"))
            Appoggio.Items(Appoggio.Items.Count - 1).Value = myPOSTreader.Item("Codice")
        Loop
        myPOSTreader.Close()
        cn.Close()
        Appoggio.Items.Add("")
        Appoggio.Items(Appoggio.Items.Count - 1).Value = ""
        Appoggio.Items(Appoggio.Items.Count - 1).Selected = True
    End Sub

    Function TestUsato(ByVal ConnectionString As String, ByVal xCodice As String) As Boolean
        Dim cn As OleDbConnection

        TestUsato = False

        cn = New Data.OleDb.OleDbConnection(ConnectionString)

        cn.Open()
        Dim cmd As New OleDbCommand()
        cmd.CommandText = "SELECT Count(*) FROM ProfiliOrariRiga" & _
                          " WHERE PrimoOrario = ?" & _
                          " OR SecondoOrario = ?" & _
                          " OR TerzoOrario = ?" & _
                          " OR QuartoOrario = ?" & _
                          " OR QuintoOrario = ?"
        cmd.Parameters.AddWithValue("@PrimoOrario", xCodice)
        cmd.Parameters.AddWithValue("@SecondoOrario", xCodice)
        cmd.Parameters.AddWithValue("@TerzoOrario", xCodice)
        cmd.Parameters.AddWithValue("@QuartoOrario", xCodice)
        cmd.Parameters.AddWithValue("@QuintoOrario", xCodice)

        cmd.Connection = cn

        Dim myPOSTreader As OleDbDataReader = cmd.ExecuteReader()
        If myPOSTreader.Read Then
            If myPOSTreader.Item(0) = 0 Then
                Return False
            Else
                Return True
                myPOSTreader.Close()
                cn.Close()
                Exit Function
            End If
        Else
            Return False
        End If
        myPOSTreader.Close()

        cmd.CommandText = "SELECT Count(*) FROM StrutturaProfiliOrariRiga" & _
                          " WHERE PrimoOrario = ?" & _
                          " OR SecondoOrario = ?" & _
                          " OR TerzoOrario = ?" & _
                          " OR QuartoOrario = ?" & _
                          " OR QuintoOrario = ?"
        cmd.Parameters.Clear()
        cmd.Parameters.AddWithValue("@PrimoOrario", xCodice)
        cmd.Parameters.AddWithValue("@SecondoOrario", xCodice)
        cmd.Parameters.AddWithValue("@TerzoOrario", xCodice)
        cmd.Parameters.AddWithValue("@QuartoOrario", xCodice)
        cmd.Parameters.AddWithValue("@QuintoOrario", xCodice)

        myPOSTreader = cmd.ExecuteReader()
        If myPOSTreader.Read Then
            If myPOSTreader.Item(0) = 0 Then
                Return False
            Else
                Return True
                myPOSTreader.Close()
                cn.Close()
                Exit Function
            End If
        Else
            Return False
        End If
        myPOSTreader.Close()

        cmd.CommandText = "SELECT Count(*) FROM ProfiliOrariVariati" & _
                          " WHERE PrimoOrario = ?" & _
                          " OR SecondoOrario = ?" & _
                          " OR TerzoOrario = ?" & _
                          " OR QuartoOrario = ?" & _
                          " OR QuintoOrario = ?"
        cmd.Parameters.Clear()
        cmd.Parameters.AddWithValue("@PrimoOrario", xCodice)
        cmd.Parameters.AddWithValue("@SecondoOrario", xCodice)
        cmd.Parameters.AddWithValue("@TerzoOrario", xCodice)
        cmd.Parameters.AddWithValue("@QuartoOrario", xCodice)
        cmd.Parameters.AddWithValue("@QuintoOrario", xCodice)

        myPOSTreader = cmd.ExecuteReader()
        If myPOSTreader.Read Then
            If myPOSTreader.Item(0) = 0 Then
                Return False
            Else
                Return True
                myPOSTreader.Close()
                cn.Close()
                Exit Function
            End If
        Else
            Return False
        End If
        myPOSTreader.Close()

        cmd.CommandText = "SELECT Count(*) FROM OrariPerGruppo" & _
                          " WHERE Orario_1 = ?" & _
                          " OR OrarioFestivo_1 = ?" & _
                          " OR Orario_2 = ?" & _
                          " OR OrarioFestivo_2 = ?" & _
                          " OR Orario_3 = ?" & _
                          " OR OrarioFestivo_3 = ?" & _
                          " OR Orario_4 = ?" & _
                          " OR OrarioFestivo_4 = ?" & _
                          " OR Orario_5 = ?" & _
                          " OR OrarioFestivo_5 = ?" & _
                          " OR Orario_6 = ?" & _
                          " OR OrarioFestivo_6 = ?" & _
                          " OR Orario_7 = ?" & _
                          " OR OrarioFestivo_7 = ?" & _
                          " OR Orario_8 = ?" & _
                          " OR OrarioFestivo_8 = ?" & _
                          " OR Orario_9 = ?" & _
                          " OR OrarioFestivo_9 = ?" & _
                          " OR Orario_10 = ?" & _
                          " OR OrarioFestivo_10 = ?"
        cmd.Parameters.Clear()
        cmd.Parameters.AddWithValue("@Orario_1", xCodice)
        cmd.Parameters.AddWithValue("@OrarioFestivo_1", xCodice)
        cmd.Parameters.AddWithValue("@Orario_2", xCodice)
        cmd.Parameters.AddWithValue("@OrarioFestivo_2", xCodice)
        cmd.Parameters.AddWithValue("@Orario_3", xCodice)
        cmd.Parameters.AddWithValue("@OrarioFestivo_3", xCodice)
        cmd.Parameters.AddWithValue("@Orario_4", xCodice)
        cmd.Parameters.AddWithValue("@OrarioFestivo_4", xCodice)
        cmd.Parameters.AddWithValue("@Orario_5", xCodice)
        cmd.Parameters.AddWithValue("@OrarioFestivo_5", xCodice)
        cmd.Parameters.AddWithValue("@Orario_6", xCodice)
        cmd.Parameters.AddWithValue("@OrarioFestivo_6", xCodice)
        cmd.Parameters.AddWithValue("@Orario_7", xCodice)
        cmd.Parameters.AddWithValue("@OrarioFestivo_7", xCodice)
        cmd.Parameters.AddWithValue("@Orario_8", xCodice)
        cmd.Parameters.AddWithValue("@OrarioFestivo_8", xCodice)
        cmd.Parameters.AddWithValue("@Orario_9", xCodice)
        cmd.Parameters.AddWithValue("@OrarioFestivo_9", xCodice)
        cmd.Parameters.AddWithValue("@Orario_10", xCodice)
        cmd.Parameters.AddWithValue("@OrarioFestivo_10", xCodice)

        myPOSTreader = cmd.ExecuteReader()
        If myPOSTreader.Read Then
            If myPOSTreader.Item(0) = 0 Then
                Return False
            Else
                Return True
                myPOSTreader.Close()
                cn.Close()
                Exit Function
            End If
        Else
            Return False
        End If
        myPOSTreader.Close()

        cn.Close()
    End Function

    Function Esiste(ByVal ConnectionString As String, ByVal xCodice As String) As Boolean
        Dim cn As OleDbConnection

        cn = New Data.OleDb.OleDbConnection(ConnectionString)

        cn.Open()
        Dim cmd As New OleDbCommand()
        cmd.CommandText = "Select * FROM Orari" & _
                          " WHERE Codice = ?"
        cmd.Parameters.AddWithValue("@Codice", xCodice)
        cmd.Connection = cn

        Dim myPOSTreader As OleDbDataReader = cmd.ExecuteReader()

        If myPOSTreader.Read Then
            Return True
        Else
            Return False
        End If
        myPOSTreader.Close()
        cn.Close()
    End Function

    Function DescrizioneDuplicata(ByVal ConnectionString As String, ByVal xCodice As String, ByVal xDescrizione As String) As Boolean
        DescrizioneDuplicata = False
        Dim cn As OleDbConnection

        cn = New Data.OleDb.OleDbConnection(ConnectionString)

        cn.Open()
        Dim cmd As New OleDbCommand()
        cmd.CommandText = "Select * FROM Orari" & _
                          " WHERE Descrizione = ?"
        cmd.Parameters.AddWithValue("@Descrizione", xDescrizione)
        cmd.Connection = cn

        Dim myPOSTreader As OleDbDataReader = cmd.ExecuteReader()

        Do While myPOSTreader.Read
            If myPOSTreader.Item("Codice") <> xCodice Then
                Return True
                Exit Do
            End If
        Loop
        myPOSTreader.Close()
        cn.Close()
    End Function
End Class
