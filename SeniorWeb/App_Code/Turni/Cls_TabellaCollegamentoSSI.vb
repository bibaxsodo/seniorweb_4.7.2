Imports Microsoft.VisualBasic
Imports System.Data.OleDb
Imports System.Web.Hosting

Public Class Cls_TabellaCollegamentoSSI
    Public Id As Long
    Public Utente As String
    Public DataAggiornamento As Date
    Public Tipo As String
    Public Codice As String
    Public Riga As Byte
    Public Descrizione As String
    Public Cod_1 As String
    Public Cod_2 As String
    Public Cod_3 As String
    Public Cod_4 As String
    Public Cod_5 As String
    Public Cod_6 As String
    Public Cod_7 As String
    Public Cod_8 As String
    Public Cod_9 As String
    Public Cod_10 As String

    Public Sub Pulisci()
        Id = 0
        Utente = ""
        DataAggiornamento = Nothing
        Tipo = ""
        Codice = ""
        Riga = 0
        Descrizione = ""
        Cod_1 = ""
        Cod_2 = ""
        Cod_3 = ""
        Cod_4 = ""
        Cod_5 = ""
        Cod_6 = ""
        Cod_7 = ""
        Cod_8 = ""
        Cod_9 = ""
        Cod_10 = ""
    End Sub
End Class
