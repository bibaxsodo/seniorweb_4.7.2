Imports Microsoft.VisualBasic
Imports System.Data.OleDb
Imports System.Web.Hosting

Public Class Cls_TimbratureManipolateTrigger
    Public IdTab As Long
    Public Id As Long
    Public Utente As String
    Public DataAggiornamento As Date
    Public CodiceDipendente As Integer
    Public Data As Date
    Public Orario As Date
    Public EntrataUscita As String
    Public Causale As Short
    Public TipoMovimento As String
    Public FlagCollegato As String

    Public Sub Pulisci()
        IdTab = 0
        Id = 0
        Utente = ""
        DataAggiornamento = Nothing
        CodiceDipendente = 0
        Data = Nothing
        Orario = Nothing
        EntrataUscita = ""
        Causale = 0
        TipoMovimento = ""
        FlagCollegato = ""
    End Sub
End Class
