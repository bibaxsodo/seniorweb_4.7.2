Imports Microsoft.VisualBasic
Imports System.Data.OleDb
Imports System.Web.Hosting

Public Class Cls_Comunicazioni
    Public Id As Long
    Public Utente As String
    Public DataAggiornamento As Date
    Public Numero As Integer
    Public Data As Date
    Public Comunicazione As String

    Public Sub Pulisci()
        Id = 0
        Utente = ""
        DataAggiornamento = Nothing
        Numero = 0
        Data = Nothing
        Comunicazione = ""
    End Sub

    Public Sub Elimina(ByVal StringaConnessione As String, ByVal xNumero As Integer)
        Dim cn As OleDbConnection
        Dim MySql As String

        MySql = ""

        cn = New Data.OleDb.OleDbConnection(StringaConnessione)

        cn.Open()
        Dim cmd As New OleDbCommand()
        cmd.CommandText = ("DELETE FROM Comunicazioni WHERE " & _
                           "Numero = ?")
        cmd.Parameters.AddWithValue("@Numero", xNumero)
        cmd.Connection = cn
        cmd.ExecuteNonQuery()
        cn.Close()
    End Sub

    Sub Aggiorna(ByVal StringaConnessione As String)
        Dim cn As OleDbConnection
        Dim MySql As String

        MySql = ""

        cn = New Data.OleDb.OleDbConnection(StringaConnessione)

        cn.Open()
        Dim cmd As New OleDbCommand()
        cmd.CommandText = "SELECT * FROM Comunicazioni WHERE Numero = ?"
        cmd.Parameters.AddWithValue("@Numero", Numero)
        cmd.Connection = cn
        Dim myPOSTreader As OleDbDataReader = cmd.ExecuteReader()
        If myPOSTreader.Read Then
            MySql = "UPDATE Comunicazioni SET " & _
                    " Utente = ?, " & _
                    " DataAggiornamento  = ?," & _
                    " Data = ?, " & _
                    " Comunicazione  = ? " & _
                    " WHERE Numero = ?"

            Dim cmdw As New OleDbCommand()
            cmdw.CommandText = MySql
            cmdw.Parameters.AddWithValue("@Utente", Utente)
            cmdw.Parameters.AddWithValue("@DataAggiornamento", Now)
            cmdw.Parameters.AddWithValue("@Data", Data)
            cmdw.Parameters.AddWithValue("@Comunicazione", Comunicazione)
            cmdw.Parameters.AddWithValue("@Numero", Numero)
            cmdw.Connection = cn
            cmdw.ExecuteNonQuery()
        Else
            MySql = "INSERT INTO Comunicazioni (Utente, DataAggiornamento, Numero, Data, Comunicazione) VALUES (?,?,?,?,?)"

            Dim cmdw As New OleDbCommand()
            cmdw.CommandText = MySql
            cmdw.Parameters.AddWithValue("@Utente", Utente)
            cmdw.Parameters.AddWithValue("@DataAggiornamento", Now)
            cmdw.Parameters.AddWithValue("@Numero", Numero)
            cmdw.Parameters.AddWithValue("@Data", IIf(Year(Data) > 1, Data, System.DBNull.Value))
            cmdw.Parameters.AddWithValue("@Comunicazione", Comunicazione)
            cmdw.Connection = cn
            cmdw.ExecuteNonQuery()
        End If
        myPOSTreader.Close()
        cn.Close()

        Call Pulisci()
    End Sub

    Sub LoadDati(ByVal StringaConnessione As String, ByVal Tabella As System.Data.DataTable)
        Dim cn As OleDbConnection

        cn = New Data.OleDb.OleDbConnection(StringaConnessione)

        cn.Open()
        Dim cmd As New OleDbCommand()

        cmd.CommandText = ("Select * From Comunicazioni Order By Numero")

        cmd.Connection = cn

        Tabella.Clear()
        Tabella.Columns.Clear()
        Tabella.Columns.Add("Numero", GetType(String))
        Tabella.Columns.Add("Data", GetType(String))
        Tabella.Columns.Add("Comunicazione", GetType(String))

        Dim myPOSTreader As OleDbDataReader = cmd.ExecuteReader()
        Do While myPOSTreader.Read

            Dim myriga As System.Data.DataRow = Tabella.NewRow()
            myriga(0) = myPOSTreader.Item("Numero")
            myriga(1) = Format(myPOSTreader.Item("Data"), "dd/MM/yyyy")
            myriga(2) = myPOSTreader.Item("Comunicazione")

            Tabella.Rows.Add(myriga)
        Loop
        myPOSTreader.Close()
        cn.Close()

    End Sub

    Function StringaDb(ByVal Oggetto As Object) As String
        If IsDBNull(Oggetto) Then
            Return ""
        Else
            Return Oggetto
        End If
    End Function

    Function DataDb(ByVal Oggetto As Object) As Date
        If Not IsDate(Oggetto) Then
            Return Nothing
        Else
            Return Oggetto
        End If
    End Function

    Function NumeroDb(ByVal Oggetto As Object) As Object
        If IsDBNull(Oggetto) Then
            Return 0
        Else
            Return Oggetto
        End If
    End Function

    Sub Leggi(ByVal StringaConnessione As String, ByVal xNumero As String)
        Dim cn As OleDbConnection

        cn = New Data.OleDb.OleDbConnection(StringaConnessione)

        cn.Open()
        Dim cmd As New OleDbCommand()
        cmd.CommandText = ("Select * From Comunicazioni WHERE " & _
                           "Numero = ?")
        cmd.Parameters.AddWithValue("@Numero", xNumero)
        cmd.Connection = cn

        Dim myPOSTreader As OleDbDataReader = cmd.ExecuteReader()
        If myPOSTreader.Read Then
            Id = NumeroDb(myPOSTreader.Item("Id"))
            Numero = NumeroDb(myPOSTreader.Item("Numero"))
            Data = DataDb(myPOSTreader.Item("Data"))
            Comunicazione = StringaDb(myPOSTreader.Item("Comunicazione"))
        Else
            Call Pulisci()
        End If
        cn.Close()
    End Sub

End Class
