Imports Microsoft.VisualBasic
Imports System.Data.OleDb
Imports System.Web.Hosting

Public Class Cls_Proposta
    Public Id As Long
    Public Utente As String
    Public DataAggiornamento As Date
    Public Codice As String
    Public Descrizione As String
    Public Gruppo_1 As String
    Public Gruppo_2 As String
    Public Gruppo_3 As String
    Public Gruppo_4 As String
    Public Gruppo_5 As String
    Public Gruppo_6 As String
    Public Gruppo_7 As String
    Public Gruppo_8 As String
    Public Gruppo_9 As String
    Public Gruppo_10 As String
    Public NumeroNotti As Byte
    Public StrutturaNotti As String
    Public NumeroNotti_1 As Byte
    Public StrutturaNotti_1 As String
    Public NumeroNotti_2 As Byte
    Public StrutturaNotti_2 As String

    Public Sub Pulisci()
        Id = 0
        Utente = ""
        DataAggiornamento = Nothing
        Codice = ""
        Descrizione = ""
        Gruppo_1 = ""
        Gruppo_2 = ""
        Gruppo_3 = ""
        Gruppo_4 = ""
        Gruppo_5 = ""
        Gruppo_6 = ""
        Gruppo_7 = ""
        Gruppo_8 = ""
        Gruppo_9 = ""
        Gruppo_10 = ""
        NumeroNotti = 0
        StrutturaNotti = ""
        NumeroNotti_1 = 0
        StrutturaNotti_1 = ""
        NumeroNotti_2 = 0
        StrutturaNotti_2 = ""
    End Sub
End Class
