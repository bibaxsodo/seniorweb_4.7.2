Imports Microsoft.VisualBasic
Imports System.Data.OleDb
Imports System.Web.Hosting

Public Class Cls_Familiari
    Public Id As Long
    Public Utente As String
    Public DataAggiornamento As Date
    Public CodiceDipendente As Integer
    Public CodiceFamiliare As Byte
    Public Cognome As String
    Public Nome As String
    Public DataNascita As Date
    Public ComuneNascita As String
    Public ProvinciaNascita As String
    Public Nazionalita As String
    Public Parentela As String
    Public DataAdozione As Date
    Public CodiceFamiliareDipendente As Integer
    Public DataDecesso As Date
    Public Note As String

    Public Sub Pulisci()
        Id = 0
        Utente = ""
        DataAggiornamento = Nothing
        CodiceDipendente = 0
        CodiceFamiliare = 0
        Cognome = ""
        Nome = ""
        DataNascita = Nothing
        ComuneNascita = ""
        ProvinciaNascita = ""
        Nazionalita = ""
        Parentela = ""
        DataAdozione = Nothing
        CodiceFamiliareDipendente = 0
        DataDecesso = Nothing
        Note = ""
    End Sub

    Public Sub Elimina(ByVal StringaConnessione As String, ByVal xCodiceDipendente As Integer, ByVal xCodiceFamiliare As Byte)
        Dim cn As OleDbConnection
        Dim MySql As String

        MySql = ""

        cn = New Data.OleDb.OleDbConnection(StringaConnessione)

        cn.Open()
        Dim cmd As New OleDbCommand()
        cmd.CommandText = "DELETE FROM Familiari WHERE" & _
                           " CodiceDipendente = ?" & _
                           " AND CodiceFamiliare = ?"
        cmd.Parameters.AddWithValue("@CodiceDipendente", xCodiceDipendente)
        cmd.Parameters.AddWithValue("@CodiceFamiliare", xCodiceFamiliare)
        cmd.Connection = cn
        cmd.ExecuteNonQuery()
        cn.Close()
    End Sub

    Public Sub EliminaAll(ByVal StringaConnessione As String, ByVal xCodiceDipendente As Integer)
        Dim cn As OleDbConnection
        Dim MySql As String

        MySql = ""

        cn = New Data.OleDb.OleDbConnection(StringaConnessione)

        cn.Open()
        Dim cmd As New OleDbCommand()
        cmd.CommandText = "DELETE FROM Familiari WHERE" & _
                           " CodiceDipendente = ?"
        cmd.Parameters.AddWithValue("@CodiceDipendente", xCodiceDipendente)
        cmd.Connection = cn
        cmd.ExecuteNonQuery()
        cn.Close()
    End Sub

    Sub Aggiorna(ByVal StringaConnessione As String)
        Dim cn As OleDbConnection
        Dim MySql As String

        MySql = ""

        cn = New Data.OleDb.OleDbConnection(StringaConnessione)

        cn.Open()
        Dim cmd As New OleDbCommand()
        cmd.CommandText = "SELECT * FROM Familiari WHERE" & _
                           " CodiceDipendente = ?" & _
                           " AND CodiceFamiliare = ?"
        cmd.Parameters.AddWithValue("@CodiceDipendente", CodiceDipendente)
        cmd.Parameters.AddWithValue("@CodiceFamiliare", CodiceFamiliare)
        cmd.Connection = cn
        Dim myPOSTreader As OleDbDataReader = cmd.ExecuteReader()
        If myPOSTreader.Read Then
            MySql = "UPDATE Familiari SET" & _
                    " Utente = ?," & _
                    " DataAggiornamento = ?," & _
                    " Cognome = ?," & _
                    " Nome = ?," & _
                    " DataNascita = ?," & _
                    " ComuneNascita = ?," & _
                    " ProvinciaNascita = ?," & _
                    " Nazionalita = ?," & _
                    " Parentela = ?," & _
                    " DataAdozione = ?," & _
                    " CodiceFamiliareDipendente = ?," & _
                    " DataDecesso = ?," & _
                    " Note = ?" & _
                    " WHERE CodiceDipendente = ?" & _
                    " AND CodiceFamiliare = ?"

            Dim cmdw As New OleDbCommand()
            cmdw.CommandText = MySql
            cmdw.Parameters.AddWithValue("@Utente", Utente)
            cmdw.Parameters.AddWithValue("@DataAggiornamento", Now)
            cmdw.Parameters.AddWithValue("@Cognome", Cognome)
            cmdw.Parameters.AddWithValue("@Nome", Nome)
            cmdw.Parameters.AddWithValue("@DataNascita", IIf(Year(DataNascita) > 1, DataNascita, System.DBNull.Value))
            cmdw.Parameters.AddWithValue("@ComuneNascita", ComuneNascita)
            cmdw.Parameters.AddWithValue("@ProvinciaNascita", ProvinciaNascita)
            cmdw.Parameters.AddWithValue("@Nazionalita", Nazionalita)
            cmdw.Parameters.AddWithValue("@Parentela", Parentela)
            cmdw.Parameters.AddWithValue("@DataAdozione", IIf(Year(DataAdozione) > 1, DataAdozione, System.DBNull.Value))
            cmdw.Parameters.AddWithValue("@CodiceFamiliareDipendente", CodiceFamiliareDipendente)
            cmdw.Parameters.AddWithValue("@DataDecesso", IIf(Year(DataDecesso) > 1, DataDecesso, System.DBNull.Value))
            cmdw.Parameters.AddWithValue("@Note", Note)
            cmdw.Parameters.AddWithValue("@CodiceDipendente", CodiceDipendente)
            cmdw.Parameters.AddWithValue("@CodiceFamiliare", CodiceFamiliare)
            cmdw.Connection = cn
            cmdw.ExecuteNonQuery()
        Else
            MySql = "INSERT INTO Familiari (Utente, DataAggiornamento, CodiceDipendente, CodiceFamiliare, Cognome, Nome, DataNascita, ComuneNascita, ProvinciaNascita, Nazionalita, Parentela, DataAdozione, CodiceFamiliareDipendente, DataDecesso, Note) VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)"

            Dim cmdw As New OleDbCommand()
            cmdw.CommandText = MySql
            cmdw.Parameters.AddWithValue("@Utente", Utente)
            cmdw.Parameters.AddWithValue("@DataAggiornamento", Now)
            cmdw.Parameters.AddWithValue("@CodiceDipendente", CodiceDipendente)
            cmdw.Parameters.AddWithValue("@CodiceFamiliare", CodiceFamiliare)
            cmdw.Parameters.AddWithValue("@Cognome", Cognome)
            cmdw.Parameters.AddWithValue("@Nome", Nome)
            cmdw.Parameters.AddWithValue("@DataNascita", IIf(Year(DataNascita) > 1, DataNascita, System.DBNull.Value))
            cmdw.Parameters.AddWithValue("@ComuneNascita", ComuneNascita)
            cmdw.Parameters.AddWithValue("@ProvinciaNascita", ProvinciaNascita)
            cmdw.Parameters.AddWithValue("@Nazionalita", Nazionalita)
            cmdw.Parameters.AddWithValue("@Parentela", Parentela)
            cmdw.Parameters.AddWithValue("@DataAdozione", IIf(Year(DataAdozione) > 1, DataAdozione, System.DBNull.Value))
            cmdw.Parameters.AddWithValue("@CodiceFamiliareDipendente", CodiceFamiliareDipendente)
            cmdw.Parameters.AddWithValue("@DataDecesso", IIf(Year(DataDecesso) > 1, DataDecesso, System.DBNull.Value))
            cmdw.Parameters.AddWithValue("@Note", Note)
            cmdw.Connection = cn
            cmdw.ExecuteNonQuery()
        End If
        myPOSTreader.Close()
        cn.Close()

        Call Pulisci()
    End Sub

    Sub LoadDati(ByVal StringaConnessione As String, ByVal xCodiceDipendente As Integer, ByVal Tabella As System.Data.DataTable)
        Dim cn As OleDbConnection

        cn = New Data.OleDb.OleDbConnection(StringaConnessione)

        cn.Open()
        Dim cmd As New OleDbCommand()

        cmd.CommandText = "Select * From Familiari WHERE CodiceDipendente = ? Order By CodiceFamiliare ASC"

        cmd.Parameters.AddWithValue("@CodiceDipendente", xCodiceDipendente)

        cmd.Connection = cn

        Tabella.Clear()
        Tabella.Columns.Clear()
        Tabella.Columns.Add("Codice", GetType(String))
        Tabella.Columns.Add("Cognome", GetType(String))
        Tabella.Columns.Add("Nome", GetType(String))
        Tabella.Columns.Add("Data Nascita", GetType(String))
        Tabella.Columns.Add("Parentela", GetType(String))
        Tabella.Columns.Add("Note", GetType(String))

        Dim myPOSTreader As OleDbDataReader = cmd.ExecuteReader()
        Do While myPOSTreader.Read
            Dim MyRiga As System.Data.DataRow = Tabella.NewRow()
            MyRiga(0) = myPOSTreader.Item("CodiceFamiliare")
            MyRiga(1) = myPOSTreader.Item("Cognome")
            MyRiga(2) = myPOSTreader.Item("Nome")
            MyRiga(3) = Format(myPOSTreader.Item("DataNascita"), "dd/MM/yyyy")
            MyRiga(4) = ""
            If myPOSTreader.Item("Parentela") = "C" Then
                MyRiga(4) = "Coniuge"
            End If
            If myPOSTreader.Item("Parentela") = "F" Then
                MyRiga(4) = "Figlio"
            End If
            MyRiga(5) = myPOSTreader.Item("Note")
            Tabella.Rows.Add(MyRiga)
        Loop

        myPOSTreader.Close()
        cn.Close()
    End Sub

    Function StringaDb(ByVal Oggetto As Object) As String
        If IsDBNull(Oggetto) Then
            Return ""
        Else
            Return Oggetto
        End If
    End Function

    Function DataDb(ByVal Oggetto As Object) As Date
        If Not IsDate(Oggetto) Then
            Return Nothing
        Else
            Return Oggetto
        End If
    End Function

    Function NumeroDb(ByVal Oggetto As Object) As Object
        If IsDBNull(Oggetto) Then
            Return 0
        Else
            Return Oggetto
        End If
    End Function

    Sub Leggi(ByVal StringaConnessione As String, ByVal xCodiceDipendente As Integer, ByVal xCodiceFamiliare As Byte)
        Dim cn As OleDbConnection

        cn = New Data.OleDb.OleDbConnection(StringaConnessione)

        cn.Open()
        Dim cmd As New OleDbCommand()
        cmd.CommandText = "SELECT * FROM Familiari WHERE" & _
                           " CodiceDipendente = ?" & _
                           " AND CodiceFamiliare = ?"
        cmd.Parameters.AddWithValue("@CodiceDipendente", xCodiceDipendente)
        cmd.Parameters.AddWithValue("@CodiceFamiliare", xCodiceFamiliare)

        cmd.Connection = cn

        Dim myPOSTreader As OleDbDataReader = cmd.ExecuteReader()
        If myPOSTreader.Read Then
            Id = NumeroDb(myPOSTreader.Item("Id"))
            CodiceDipendente = NumeroDb(myPOSTreader.Item("CodiceDipendente"))
            CodiceFamiliare = NumeroDb(myPOSTreader.Item("CodiceFamiliare"))
            Cognome = StringaDb(myPOSTreader.Item("Cognome"))
            Nome = StringaDb(myPOSTreader.Item("Nome"))
            DataNascita = DataDb(myPOSTreader.Item("DataNascita"))
            ComuneNascita = StringaDb(myPOSTreader.Item("ComuneNascita"))
            ProvinciaNascita = StringaDb(myPOSTreader.Item("ProvinciaNascita"))
            Nazionalita = StringaDb(myPOSTreader.Item("Nazionalita"))
            Parentela = StringaDb(myPOSTreader.Item("Parentela"))
            DataAdozione = DataDb(myPOSTreader.Item("DataAdozione"))
            CodiceFamiliareDipendente = NumeroDb(myPOSTreader.Item("CodiceFamiliareDipendente"))
            DataDecesso = DataDb(myPOSTreader.Item("DataDecesso"))
            Note = StringaDb(myPOSTreader.Item("Note"))
        Else
            Call Pulisci()
        End If
        cn.Close()
    End Sub

    Function TestUsato(ByVal StringaConnessione As String, ByVal xCodiceDipendente As Integer, ByVal xCodiceFamiliare As Byte) As Boolean
        Dim cn As OleDbConnection
        cn = New Data.OleDb.OleDbConnection(StringaConnessione)
        cn.Open()

        Dim cmd As New OleDbCommand()
        cmd.CommandText = "Select Count(*) FROM SalvaCalcoloOrari" & _
                          " WHERE CodiceDipendente = ?" & _
                          " AND CodiceFamiliare = ?"

        cmd.Parameters.AddWithValue("@CodiceDipendente", xCodiceDipendente)
        cmd.Parameters.AddWithValue("@CodiceFamiliare", xCodiceFamiliare)

        cmd.Connection = cn
        Dim myPOSTreader As OleDbDataReader = cmd.ExecuteReader()
        If myPOSTreader.Read Then
            If myPOSTreader.Item(0) = 0 Then
                Return False
            Else
                Return True
            End If
        Else
            Return False
        End If
        myPOSTreader.Close()
        cn.Close()
    End Function

    Function Esiste(ByVal StringaConnessione As String, ByVal xCodiceDipendente As Integer, ByVal xCodiceFamiliare As Byte) As Boolean
        Dim cn As OleDbConnection

        cn = New Data.OleDb.OleDbConnection(StringaConnessione)

        cn.Open()
        Dim cmd As New OleDbCommand()
        cmd.CommandText = "Select * FROM Familiari" & _
                          " WHERE CodiceDipendente = ?" & _
                          " AND CodiceFamiliare = ?"
        cmd.Parameters.AddWithValue("@CodiceDipendente", xCodiceDipendente)
        cmd.Parameters.AddWithValue("@CodiceFamiliare", xCodiceFamiliare)
        cmd.Connection = cn

        Dim myPOSTreader As OleDbDataReader = cmd.ExecuteReader()

        If myPOSTreader.Read Then
            Return True
        Else
            Return False
        End If
        myPOSTreader.Close()
        cn.Close()
    End Function

    Public Function CampoFamiliari(ByVal StringaConnessione As String, ByVal xCodiceDipendente As Integer, ByVal xCodiceFamiliare As Byte, ByVal xCampo As String) As Object
        Dim cn As OleDbConnection

        cn = New Data.OleDb.OleDbConnection(StringaConnessione)

        CampoFamiliari = ""

        cn.Open()

        Dim cmd As New OleDbCommand()
        cmd.CommandText = "Select " & xCampo & " FROM Familiari" & _
                          " WHERE CodiceDipendente = ?" & _
                          " AND CodiceFamiliare = ?"
        cmd.Parameters.AddWithValue("@CodiceDipendente", xCodiceDipendente)
        cmd.Parameters.AddWithValue("@CodiceFamiliare", xCodiceFamiliare)
        cmd.Connection = cn

        Dim myPOSTreader As OleDbDataReader = cmd.ExecuteReader()
        If myPOSTreader.Read Then
            CampoFamiliari = myPOSTreader.Item(xCampo)
        End If
        myPOSTreader.Close()
        cn.Close()
    End Function

End Class
