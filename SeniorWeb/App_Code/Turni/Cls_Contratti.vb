Imports Microsoft.VisualBasic
Imports System.Data.OleDb
Imports System.Web.Hosting

Public Class Cls_Contratti
    Public Id As Long
    Public Utente As String
    Public DataAggiornamento As Date
    Public Codice As String
    Public Descrizione As String

    Public Sub Pulisci()
        Id = 0
        Utente = ""
        DataAggiornamento = Nothing
        Codice = ""
        Descrizione = ""
    End Sub

    Public Sub Elimina(ByVal StringaConnessione As String, ByVal xCodice As String)
        Dim cn As OleDbConnection
        Dim MySql As String

        MySql = ""

        cn = New Data.OleDb.OleDbConnection(StringaConnessione)

        cn.Open()
        Dim cmd As New OleDbCommand()
        cmd.CommandText = ("DELETE FROM Contratti WHERE " & _
                           "Codice = ?")
        cmd.Parameters.AddWithValue("@Codice", xCodice)
        cmd.Connection = cn
        cmd.ExecuteNonQuery()
        cn.Close()
    End Sub

    Sub Aggiorna(ByVal StringaConnessione As String)
        Dim cn As OleDbConnection
        Dim cmdw As New OleDbCommand()

        cn = New Data.OleDb.OleDbConnection(StringaConnessione)

        cn.Open()
        Dim cmd As New OleDbCommand()
        cmd.CommandText = "SELECT * FROM Contratti WHERE Codice = ?"
        cmd.Parameters.AddWithValue("@Codice", Codice)
        cmd.Connection = cn
        Dim myPOSTreader As OleDbDataReader = cmd.ExecuteReader()
        If myPOSTreader.Read Then
            cmdw.CommandText = "UPDATE Contratti SET " & _
                               " Utente = ?, " & _
                               " DataAggiornamento  = ?," & _
                               " Descrizione = ?, " & _
                               " Disponibile  = ? " & _
                               " WHERE Codice = ?"

            cmdw.Parameters.AddWithValue("@Utente", Utente)
            cmdw.Parameters.AddWithValue("@DataAggiornamento", Now)
            cmdw.Parameters.AddWithValue("@Descrizione", Descrizione)
            cmdw.Parameters.AddWithValue("@Codice", Codice)
            cmdw.Connection = cn
            cmdw.ExecuteNonQuery()
        Else
            cmdw.CommandText = "INSERT INTO Contratti (Utente, DataAggiornamento, Codice, Descrizione) VALUES (?,?,?,?)"

            cmdw.Parameters.AddWithValue("@Utente", Utente)
            cmdw.Parameters.AddWithValue("@DataAggiornamento", Now)
            cmdw.Parameters.AddWithValue("@Codice", Codice)
            cmdw.Parameters.AddWithValue("@Descrizione", Descrizione)
            cmdw.Connection = cn
            cmdw.ExecuteNonQuery()
        End If
        myPOSTreader.Close()
        cn.Close()

        Call Pulisci()
    End Sub

    Sub LoadDati(ByVal StringaConnessione As String, ByVal xDescrizione As String, ByVal Tabella As System.Data.DataTable)
        Dim cn As OleDbConnection

        cn = New Data.OleDb.OleDbConnection(StringaConnessione)


        cn.Open()
        Dim cmd As New OleDbCommand()

        If xDescrizione <> "" Then
            cmd.CommandText = "Select * From Contratti WHERE Descrizione LIKE '" & Duplica_Accenti(xDescrizione) & "' Order By Descrizione"
        Else
            cmd.CommandText = "Select * From Contratti Order By Descrizione"
        End If

        cmd.Connection = cn

        Tabella.Clear()
        Tabella.Columns.Clear()
        Tabella.Columns.Add("Codice", GetType(String))
        Tabella.Columns.Add("Descrizione", GetType(String))

        Dim myPOSTreader As OleDbDataReader = cmd.ExecuteReader()
        Do While myPOSTreader.Read

            Dim myriga As System.Data.DataRow = Tabella.NewRow()
            myriga(0) = myPOSTreader.Item("Codice")
            myriga(1) = myPOSTreader.Item("Descrizione")

            Tabella.Rows.Add(myriga)
        Loop
        myPOSTreader.Close()
        cn.Close()

    End Sub

    Function StringaDb(ByVal Oggetto As Object) As String
        If IsDBNull(Oggetto) Then
            Return ""
        Else
            Return Oggetto
        End If
    End Function

    Function NumeroDb(ByVal Oggetto As Object) As Object
        If IsDBNull(Oggetto) Then
            Return 0
        Else
            Return Oggetto
        End If
    End Function

    Sub Leggi(ByVal StringaConnessione As String, ByVal xCodice As String)
        Dim cn As OleDbConnection

        cn = New Data.OleDb.OleDbConnection(StringaConnessione)

        cn.Open()
        Dim cmd As New OleDbCommand()
        cmd.CommandText = ("Select * From Contratti WHERE " & _
                           "Codice = ?")
        cmd.Parameters.AddWithValue("@Codice", xCodice)
        cmd.Connection = cn

        Dim myPOSTreader As OleDbDataReader = cmd.ExecuteReader()
        If myPOSTreader.Read Then
            Id = NumeroDb(myPOSTreader.Item("Id"))
            Codice = StringaDb(myPOSTreader.Item("Codice"))
            Descrizione = StringaDb(myPOSTreader.Item("Descrizione"))
        Else
            Call Pulisci()
        End If
        cn.Close()
    End Sub

    Function TestUsato(ByVal StringaConnessione As String, ByVal xCodice As String) As Boolean
        Dim cn As OleDbConnection
        cn = New Data.OleDb.OleDbConnection(StringaConnessione)
        cn.Open()

        Dim p As New Cls_ParametriTurni

        p.Leggi(StringaConnessione)

        Dim cmd As New OleDbCommand()
        cmd.CommandText = "SELECT Count(*) FROM DatiVariabili" & _
                          " WHERE CodiceVariabile = ?" & _
                          " AND ContenutoTesto = ?"

        cmd.Parameters.AddWithValue("@CodiceVariabile", p.CodiceAnagraficoContratto)
        cmd.Parameters.AddWithValue("@ContenutoTesto", xCodice)

        cmd.Connection = cn
        Dim myPOSTreader As OleDbDataReader = cmd.ExecuteReader()
        If myPOSTreader.Read Then
            If myPOSTreader.Item(0) = 0 Then
                Return False
            Else
                Return True
            End If
        Else
            Return False
        End If
        myPOSTreader.Close()
        cn.Close()
    End Function

    Sub UpDateDropBox(ByVal StringaConnessione As String, ByRef Appoggio As DropDownList)
        Dim cn As OleDbConnection

        cn = New Data.OleDb.OleDbConnection(StringaConnessione)

        cn.Open()
        Dim cmd As New OleDbCommand()
        cmd.CommandText = ("SELECT Codice, Descrizione FROM Contratti ORDER BY Descrizione")
        cmd.Connection = cn

        Appoggio.Items.Clear()
        Dim myPOSTreader As OleDbDataReader = cmd.ExecuteReader()
        Do While myPOSTreader.Read
            Appoggio.Items.Add(myPOSTreader.Item("Descrizione"))
            Appoggio.Items(Appoggio.Items.Count - 1).Value = myPOSTreader.Item("Codice")
        Loop
        myPOSTreader.Close()
        cn.Close()
        Appoggio.Items.Add("")
        Appoggio.Items(Appoggio.Items.Count - 1).Value = ""
        Appoggio.Items(Appoggio.Items.Count - 1).Selected = True

    End Sub


    Function Esiste_Codice(ByVal StringaConnessione As String, ByVal xCodice As String) As Boolean
        Dim cn As OleDbConnection

        cn = New Data.OleDb.OleDbConnection(StringaConnessione)

        cn.Open()
        Dim cmd As New OleDbCommand()
        cmd.CommandText = "Select * FROM Contratti" & _
                          " WHERE Codice = ?"
        cmd.Parameters.AddWithValue("@Codice", xCodice)
        cmd.Connection = cn
        Dim myPOSTreader As OleDbDataReader = cmd.ExecuteReader()
        If myPOSTreader.Read Then
            Return True
        Else
            Return False
        End If
        myPOSTreader.Close()
        cn.Close()
    End Function

    Function Esiste_Descrizione(ByVal StringaConnessione As String, ByVal xCodice As String, ByVal xDescrizione As String) As Boolean
        Dim cn As OleDbConnection

        cn = New Data.OleDb.OleDbConnection(StringaConnessione)

        cn.Open()
        Dim cmd As New OleDbCommand()
        cmd.CommandText = "Select * FROM Contratti" & _
                          " WHERE Descrizione = ?"
        cmd.Parameters.AddWithValue("@Descrizione", xDescrizione)
        cmd.Connection = cn
        Dim myPOSTreader As OleDbDataReader = cmd.ExecuteReader()
        If myPOSTreader.Read Then
            If StringaDb(myPOSTreader.Item("Codice")) = xCodice Then
                Return False
            Else
                Return True
            End If
        Else
            Return False
        End If
        myPOSTreader.Close()
        cn.Close()
    End Function

    Private Function Duplica_Accenti(ByVal xRiga As String) As String
        Dim Appoggio As String = ""
        For i = 1 To Len(xRiga)
            If Mid(xRiga, i, 1) <> "'" Then
                Appoggio = Appoggio + Mid(xRiga, i, 1)
            Else
                Appoggio = Appoggio + Mid(xRiga, i, 1) + Mid(xRiga, i, 1)
            End If
        Next i
        Duplica_Accenti = Appoggio
    End Function

End Class
