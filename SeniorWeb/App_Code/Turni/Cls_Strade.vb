Imports Microsoft.VisualBasic
Imports System.Data.OleDb
Imports System.Web.Hosting

Public Class Cls_Strade
    Public Id As Long
    Public Utente As String
    Public DataAggiornamento As Date
    Public Codice As String
    Public Subx As String
    Public DescrizioneEstesa As String
    Public DescrizioneBreve As String
    Public Ubicazione As String
    Public CAP As Integer
    Public ASL As String
    Public Quartiere As String
    Public QuartiereNome As String
    Public Zona As String
    Public Gruppo As String

    Public Sub Pulisci()
        Id = 0
        Utente = ""
        DataAggiornamento = Nothing
        Codice = ""
        Subx = ""
        DescrizioneEstesa = ""
        DescrizioneBreve = ""
        Ubicazione = ""
        CAP = 0
        ASL = ""
        Quartiere = ""
        QuartiereNome = ""
        Zona = ""
        Gruppo = ""
    End Sub
End Class
