Imports Microsoft.VisualBasic
Imports System.Data.OleDb
Imports System.Web.Hosting

Public Class Cls_ProfiliOrariVariati
    Public Id As Long
    Public Utente As String
    Public DataAggiornamento As Date
    Public Data As Date
    Public CodiceDipendente As Integer
    Public PrimoOrario As String
    Public SecondoOrario As String
    Public TerzoOrario As String
    Public QuartoOrario As String
    Public QuintoOrario As String
    Public PrimoNelGruppo As String
    Public SecondoNelGruppo As String
    Public TerzoNelGruppo As String
    Public QuartoNelGruppo As String
    Public QuintoNelGruppo As String
    Public PrimoGiustificativo As String
    Public SecondoGiustificativo As String
    Public TerzoGiustificativo As String
    Public QuartoGiustificativo As String
    Public QuintoGiustificativo As String
    Public Proposta As String

    Public Sub Pulisci()
        Id = 0
        Utente = ""
        DataAggiornamento = Nothing
        Data = Nothing
        CodiceDipendente = 0
        PrimoOrario = ""
        SecondoOrario = ""
        TerzoOrario = ""
        QuartoOrario = ""
        QuintoOrario = ""
        PrimoNelGruppo = ""
        SecondoNelGruppo = ""
        TerzoNelGruppo = ""
        QuartoNelGruppo = ""
        QuintoNelGruppo = ""
        PrimoGiustificativo = ""
        SecondoGiustificativo = ""
        TerzoGiustificativo = ""
        QuartoGiustificativo = ""
        QuintoGiustificativo = ""
        Proposta = ""
    End Sub

    Function StringaDb(ByVal Oggetto As Object) As String
        If IsDBNull(Oggetto) Then
            Return ""
        Else
            Return Oggetto
        End If
    End Function

    Function DataDb(ByVal Oggetto As Object) As Date
        If Not IsDate(Oggetto) Then
            Return Nothing
        Else
            Return Oggetto
        End If
    End Function

    Function NumeroDb(ByVal Oggetto As Object) As Object
        If IsDBNull(Oggetto) Then
            Return 0
        Else
            Return Oggetto
        End If
    End Function

    Sub Leggi(ByVal ConnectionString As String, ByVal xCodiceDipendente As Integer, ByVal xdata As Date)
        Dim cn As OleDbConnection

        cn = New Data.OleDb.OleDbConnection(ConnectionString)

        cn.Open()
        Dim cmd As New OleDbCommand()
        cmd.CommandText = ("Select * From ProfiliOrariVariati WHERE " & _
                           " CodiceDipendente = ?" & _
                           " AND Data = ?")
        cmd.Parameters.AddWithValue("@CodiceDipendente", xCodiceDipendente)
        cmd.Parameters.AddWithValue("@Data", xData)
        cmd.Connection = cn

        Dim myPOSTreader As OleDbDataReader = cmd.ExecuteReader()
        If myPOSTreader.Read Then
            Id = NumeroDb(myPOSTreader.Item("Id"))
            Data = DataDb(myPOSTreader.Item("Data"))
            CodiceDipendente = NumeroDb(myPOSTreader.Item("CodiceDipendente"))
            PrimoOrario = StringaDb(myPOSTreader.Item("PrimoOrario"))
            SecondoOrario = StringaDb(myPOSTreader.Item("SecondoOrario"))
            TerzoOrario = StringaDb(myPOSTreader.Item("TerzoOrario"))
            QuartoOrario = StringaDb(myPOSTreader.Item("QuartoOrario"))
            QuintoOrario = StringaDb(myPOSTreader.Item("QuintoOrario"))
            PrimoNelGruppo = StringaDb(myPOSTreader.Item("PrimoNelGruppo"))
            SecondoNelGruppo = StringaDb(myPOSTreader.Item("SecondoNelGruppo"))
            TerzoNelGruppo = StringaDb(myPOSTreader.Item("TerzoNelGruppo"))
            QuartoNelGruppo = StringaDb(myPOSTreader.Item("QuartoNelGruppo"))
            QuintoNelGruppo = StringaDb(myPOSTreader.Item("QuintoNelGruppo"))
            PrimoGiustificativo = StringaDb(myPOSTreader.Item("PrimoGiustificativo"))
            SecondoGiustificativo = StringaDb(myPOSTreader.Item("SecondoGiustificativo"))
            TerzoGiustificativo = StringaDb(myPOSTreader.Item("TerzoGiustificativo"))
            QuartoGiustificativo = StringaDb(myPOSTreader.Item("QuartoGiustificativo"))
            QuintoGiustificativo = StringaDb(myPOSTreader.Item("QuintoGiustificativo"))
            Proposta = StringaDb(myPOSTreader.Item("Proposta"))
        Else
            Call Pulisci()
        End If
        cn.Close()
    End Sub


    Sub Scrivi(ByVal ConnectionString As String, ByVal xCodiceDipendente As Integer, ByVal xdata As Date)
        Dim cn As OleDbConnection
        Dim Mysql As String

        cn = New Data.OleDb.OleDbConnection(ConnectionString)

        cn.Open()
        Dim cmd As New OleDbCommand()
        cmd.CommandText = ("Select * From ProfiliOrariVariati WHERE " & _
                           " CodiceDipendente = ?" & _
                           " AND Data = ?")
        cmd.Parameters.AddWithValue("@CodiceDipendente", xCodiceDipendente)
        cmd.Parameters.AddWithValue("@Data", xdata)
        cmd.Connection = cn
        Dim myPOSTreader As OleDbDataReader = cmd.ExecuteReader()
        If myPOSTreader.Read Then
            Mysql = "UPDATE ProfiliOrariVariati set Utente = ?,DataAggiornamento = ?,PrimoOrario = ?,SecondoOrario = ?,TerzoOrario = ?,QuartoOrario = ?,QuintoOrario = ?,PrimoNelGruppo = ?,SecondoNelGruppo = ?,TerzoNelGruppo = ?,QuartoNelGruppo = ?,QuintoNelGruppo = ?,PrimoGiustificativo = ?,SecondoGiustificativo = ?,TerzoGiustificativo = ?,QuartoGiustificativo = ?,QuintoGiustificativo = ?,Proposta = ?  WHERE Data = ? AND CodiceDipendente = ?"
        Else
            Mysql = "INSERT INTO ProfiliOrariVariati (Utente,DataAggiornamento,PrimoOrario,SecondoOrario,TerzoOrario,QuartoOrario,QuintoOrario,PrimoNelGruppo,SecondoNelGruppo,TerzoNelGruppo,QuartoNelGruppo,QuintoNelGruppo,PrimoGiustificativo,SecondoGiustificativo,TerzoGiustificativo,QuartoGiustificativo,QuintoGiustificativo,Proposta,Data,CodiceDipendente) VALUES  (?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?) "
        End If


        Dim Cmd1 As New OleDbCommand()
        Cmd1.CommandText = Mysql
        Cmd1.Parameters.AddWithValue("@Utente", Utente)
        Cmd1.Parameters.AddWithValue("@DataAggiornamento", Now)
        Cmd1.Parameters.AddWithValue("@PrimoOrario", PrimoOrario)
        Cmd1.Parameters.AddWithValue("@SecondoOrario", SecondoOrario)
        Cmd1.Parameters.AddWithValue("@TerzoOrario", TerzoOrario)
        Cmd1.Parameters.AddWithValue("@QuartoOrario", QuartoOrario)
        Cmd1.Parameters.AddWithValue("@QuintoOrario", QuintoOrario)
        Cmd1.Parameters.AddWithValue("@PrimoNelGruppo", PrimoNelGruppo)
        Cmd1.Parameters.AddWithValue("@SecondoNelGruppo", SecondoNelGruppo)
        Cmd1.Parameters.AddWithValue("@TerzoNelGruppo", TerzoNelGruppo)
        Cmd1.Parameters.AddWithValue("@QuartoNelGruppo", QuartoNelGruppo)
        Cmd1.Parameters.AddWithValue("@QuintoNelGruppo", QuintoNelGruppo)
        Cmd1.Parameters.AddWithValue("@PrimoGiustificativo", PrimoGiustificativo)
        Cmd1.Parameters.AddWithValue("@SecondoGiustificativo", SecondoGiustificativo)
        Cmd1.Parameters.AddWithValue("@TerzoGiustificativo", TerzoGiustificativo)
        Cmd1.Parameters.AddWithValue("@QuartoGiustificativo", QuartoGiustificativo)
        Cmd1.Parameters.AddWithValue("@QuintoGiustificativo", QuintoGiustificativo)
        Cmd1.Parameters.AddWithValue("@Proposta", Proposta)
        Cmd1.Parameters.AddWithValue("@Data", Data)
        Cmd1.Parameters.AddWithValue("@CodiceDipendente", CodiceDipendente)
        Cmd1.Connection = cn
        Cmd1.ExecuteNonQuery()

        cn.Close()

        Call Pulisci()
    End Sub
End Class

