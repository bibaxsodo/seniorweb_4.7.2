﻿Imports Microsoft.VisualBasic
Imports System.Data.OleDb
Imports System.Web.Hosting

Public Class Cls_TabellaLivelli
    Public Id As Long
    Public Utente As String
    Public DataAggiornamento As Date
    Public Livello1 As Integer
    Public Livello2 As Integer
    Public Livello3 As Integer
    Public Livello4 As Integer
    Public Livello5 As Integer
    Public Livello6 As Integer
    Public Livello7 As Integer
    Public Livello8 As Integer
    Public Livello9 As Integer
    Public Livello10 As Integer
    Public Testo1 As String
    Public Testo2 As String
    Public Testo3 As String
    Public Testo4 As String
    Public Testo5 As String
    Public Testo6 As String
    Public Testo7 As String
    Public Testo8 As String
    Public Testo9 As String
    Public Testo10 As String

    Public Sub Pulisci()
        Id = 0
        Utente = ""
        DataAggiornamento = Nothing
        Livello1 = 0
        Livello2 = 0
        Livello3 = 0
        Livello4 = 0
        Livello5 = 0
        Livello6 = 0
        Livello7 = 0
        Livello8 = 0
        Livello9 = 0
        Livello10 = 0
        Testo1 = ""
        Testo2 = ""
        Testo3 = ""
        Testo4 = ""
        Testo5 = ""
        Testo6 = ""
        Testo7 = ""
        Testo8 = ""
        Testo9 = ""
        Testo10 = ""
    End Sub
End Class
