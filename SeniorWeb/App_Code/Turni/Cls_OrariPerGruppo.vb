Imports Microsoft.VisualBasic
Imports System.Data.OleDb
Imports System.Web.Hosting

Public Class Cls_OrariPerGruppo
    Public Id As Long
    Public Utente As String
    Public DataAggiornamento As Date
    Public Validita As Date
    Public Codice As String
    Public Orario_1 As String
    Public OrarioSabato_1 As String
    Public OrarioFestivo_1 As String
    Public Dipendenti_1 As Short
    Public DipendentiSabato_1 As Short
    Public DipendentiFestivo_1 As Short
    Public Orario_2 As String
    Public OrarioSabato_2 As String
    Public OrarioFestivo_2 As String
    Public Dipendenti_2 As Short
    Public DipendentiSabato_2 As Short
    Public DipendentiFestivo_2 As Short
    Public Orario_3 As String
    Public OrarioSabato_3 As String
    Public OrarioFestivo_3 As String
    Public Dipendenti_3 As Short
    Public DipendentiSabato_3 As Short
    Public DipendentiFestivo_3 As Short
    Public Orario_4 As String
    Public OrarioSabato_4 As String
    Public OrarioFestivo_4 As String
    Public Dipendenti_4 As Short
    Public DipendentiSabato_4 As Short
    Public DipendentiFestivo_4 As Short
    Public Orario_5 As String
    Public OrarioSabato_5 As String
    Public OrarioFestivo_5 As String
    Public Dipendenti_5 As Short
    Public DipendentiSabato_5 As Short
    Public DipendentiFestivo_5 As Short
    Public Orario_6 As String
    Public OrarioSabato_6 As String
    Public OrarioFestivo_6 As String
    Public Dipendenti_6 As Short
    Public DipendentiSabato_6 As Short
    Public DipendentiFestivo_6 As Short
    Public Orario_7 As String
    Public OrarioSabato_7 As String
    Public OrarioFestivo_7 As String
    Public Dipendenti_7 As Short
    Public DipendentiSabato_7 As Short
    Public DipendentiFestivo_7 As Short
    Public Orario_8 As String
    Public OrarioSabato_8 As String
    Public OrarioFestivo_8 As String
    Public Dipendenti_8 As Short
    Public DipendentiSabato_8 As Short
    Public DipendentiFestivo_8 As Short
    Public Orario_9 As String
    Public OrarioSabato_9 As String
    Public OrarioFestivo_9 As String
    Public Dipendenti_9 As Short
    Public DipendentiSabato_9 As Short
    Public DipendentiFestivo_9 As Short
    Public Orario_10 As String
    Public OrarioSabato_10 As String
    Public OrarioFestivo_10 As String
    Public Dipendenti_10 As Short
    Public DipendentiSabato_10 As Short
    Public DipendentiFestivo_10 As Short
    Public Orario_11 As String
    Public OrarioSabato_11 As String
    Public OrarioFestivo_11 As String
    Public Dipendenti_11 As Short
    Public DipendentiSabato_11 As Short
    Public DipendentiFestivo_11 As Short
    Public Orario_12 As String
    Public OrarioSabato_12 As String
    Public OrarioFestivo_12 As String
    Public Dipendenti_12 As Short
    Public DipendentiSabato_12 As Short
    Public DipendentiFestivo_12 As Short
    Public Orario_13 As String
    Public OrarioSabato_13 As String
    Public OrarioFestivo_13 As String
    Public Dipendenti_13 As Short
    Public DipendentiSabato_13 As Short
    Public DipendentiFestivo_13 As Short
    Public Orario_14 As String
    Public OrarioSabato_14 As String
    Public OrarioFestivo_14 As String
    Public Dipendenti_14 As Short
    Public DipendentiSabato_14 As Short
    Public DipendentiFestivo_14 As Short
    Public Orario_15 As String
    Public OrarioSabato_15 As String
    Public OrarioFestivo_15 As String
    Public Dipendenti_15 As Short
    Public DipendentiSabato_15 As Short
    Public DipendentiFestivo_15 As Short
    Public Orario_16 As String
    Public OrarioSabato_16 As String
    Public OrarioFestivo_16 As String
    Public Dipendenti_16 As Short
    Public DipendentiSabato_16 As Short
    Public DipendentiFestivo_16 As Short
    Public Orario_17 As String
    Public OrarioSabato_17 As String
    Public OrarioFestivo_17 As String
    Public Dipendenti_17 As Short
    Public DipendentiSabato_17 As Short
    Public DipendentiFestivo_17 As Short
    Public Orario_18 As String
    Public OrarioSabato_18 As String
    Public OrarioFestivo_18 As String
    Public Dipendenti_18 As Short
    Public DipendentiSabato_18 As Short
    Public DipendentiFestivo_18 As Short
    Public Orario_19 As String
    Public OrarioSabato_19 As String
    Public OrarioFestivo_19 As String
    Public Dipendenti_19 As Short
    Public DipendentiSabato_19 As Short
    Public DipendentiFestivo_19 As Short
    Public Orario_20 As String
    Public OrarioSabato_20 As String
    Public OrarioFestivo_20 As String
    Public Dipendenti_20 As Short
    Public DipendentiSabato_20 As Short
    Public DipendentiFestivo_20 As Short

    Public Sub Pulisci()
        Id = 0
        Utente = ""
        DataAggiornamento = Nothing
        Validita = Nothing
        Codice = ""
        Orario_1 = ""
        OrarioSabato_1 = ""
        OrarioFestivo_1 = ""
        Dipendenti_1 = 0
        DipendentiSabato_1 = 0
        DipendentiFestivo_1 = 0
        Orario_2 = ""
        OrarioSabato_2 = ""
        OrarioFestivo_2 = ""
        Dipendenti_2 = 0
        DipendentiSabato_2 = 0
        DipendentiFestivo_2 = 0
        Orario_3 = ""
        OrarioSabato_3 = ""
        OrarioFestivo_3 = ""
        Dipendenti_3 = 0
        DipendentiSabato_3 = 0
        DipendentiFestivo_3 = 0
        Orario_4 = ""
        OrarioSabato_4 = ""
        OrarioFestivo_4 = ""
        Dipendenti_4 = 0
        DipendentiSabato_4 = 0
        DipendentiFestivo_4 = 0
        Orario_5 = ""
        OrarioSabato_5 = ""
        OrarioFestivo_5 = ""
        Dipendenti_5 = 0
        DipendentiSabato_5 = 0
        DipendentiFestivo_5 = 0
        Orario_6 = ""
        OrarioSabato_6 = ""
        OrarioFestivo_6 = ""
        Dipendenti_6 = 0
        DipendentiSabato_6 = 0
        DipendentiFestivo_6 = 0
        Orario_7 = ""
        OrarioSabato_7 = ""
        OrarioFestivo_7 = ""
        Dipendenti_7 = 0
        DipendentiSabato_7 = 0
        DipendentiFestivo_7 = 0
        Orario_8 = ""
        OrarioSabato_8 = ""
        OrarioFestivo_8 = ""
        Dipendenti_8 = 0
        DipendentiSabato_8 = 0
        DipendentiFestivo_8 = 0
        Orario_9 = ""
        OrarioSabato_9 = ""
        OrarioFestivo_9 = ""
        Dipendenti_9 = 0
        DipendentiSabato_9 = 0
        DipendentiFestivo_9 = 0
        Orario_10 = ""
        OrarioSabato_10 = ""
        OrarioFestivo_10 = ""
        Dipendenti_10 = 0
        DipendentiSabato_10 = 0
        DipendentiFestivo_10 = 0
        Orario_11 = ""
        OrarioSabato_11 = ""
        OrarioFestivo_11 = ""
        Dipendenti_11 = 0
        DipendentiSabato_11 = 0
        DipendentiFestivo_11 = 0
        Orario_12 = ""
        OrarioSabato_12 = ""
        OrarioFestivo_12 = ""
        Dipendenti_12 = 0
        DipendentiSabato_12 = 0
        DipendentiFestivo_12 = 0
        Orario_13 = ""
        OrarioSabato_13 = ""
        OrarioFestivo_13 = ""
        Dipendenti_13 = 0
        DipendentiSabato_13 = 0
        DipendentiFestivo_13 = 0
        Orario_14 = ""
        OrarioSabato_14 = ""
        OrarioFestivo_14 = ""
        Dipendenti_14 = 0
        DipendentiSabato_14 = 0
        DipendentiFestivo_14 = 0
        Orario_15 = ""
        OrarioSabato_15 = ""
        OrarioFestivo_15 = ""
        Dipendenti_15 = 0
        DipendentiSabato_15 = 0
        DipendentiFestivo_15 = 0
        Orario_16 = ""
        OrarioSabato_16 = ""
        OrarioFestivo_16 = ""
        Dipendenti_16 = 0
        DipendentiSabato_16 = 0
        DipendentiFestivo_16 = 0
        Orario_17 = ""
        OrarioSabato_17 = ""
        OrarioFestivo_17 = ""
        Dipendenti_17 = 0
        DipendentiSabato_17 = 0
        DipendentiFestivo_17 = 0
        Orario_18 = ""
        OrarioSabato_18 = ""
        OrarioFestivo_18 = ""
        Dipendenti_18 = 0
        DipendentiSabato_18 = 0
        DipendentiFestivo_18 = 0
        Orario_19 = ""
        OrarioSabato_19 = ""
        OrarioFestivo_19 = ""
        Dipendenti_19 = 0
        DipendentiSabato_19 = 0
        DipendentiFestivo_19 = 0
        Orario_20 = ""
        OrarioSabato_20 = ""
        OrarioFestivo_20 = ""
        Dipendenti_20 = 0
        DipendentiSabato_20 = 0
        DipendentiFestivo_20 = 0
    End Sub

    Public Sub Elimina(ByVal ConnectionString As String, ByVal xCodice As String, ByVal xValidita As Date)
        Dim cn As OleDbConnection
        Dim MySql As String

        MySql = ""

        cn = New Data.OleDb.OleDbConnection(ConnectionString)

        cn.Open()
        Dim cmd As New OleDbCommand()
        cmd.CommandText = "DELETE FROM OrariPerGruppo WHERE " & _
                          " Codice = ?" & _
                          " AND Validita = ?"
        cmd.Parameters.AddWithValue("@Codice", xCodice)
        cmd.Parameters.AddWithValue("@Validita", xValidita)
        cmd.Connection = cn
        cmd.ExecuteNonQuery()
        cn.Close()
    End Sub

    Sub Aggiorna(ByVal ConnectionString As String)
        Dim cn As OleDbConnection
        Dim MySql As String

        MySql = ""

        cn = New Data.OleDb.OleDbConnection(ConnectionString)

        cn.Open()
        Dim cmd As New OleDbCommand()
        cmd.CommandText = "SELECT * FROM OrariPerGruppo" & _
                          " WHERE Codice = ?" & _
                          " AND Validita = ?"
        cmd.Parameters.AddWithValue("@Codice", Codice)
        cmd.Parameters.AddWithValue("@Validita", Validita)
        cmd.Connection = cn
        Dim myPOSTreader As OleDbDataReader = cmd.ExecuteReader()
        If myPOSTreader.Read Then
            MySql = "UPDATE OrariPerGruppo SET " & _
                    " Utente = ?, " & _
                    " DataAggiornamento = ?," & _
                    " Orario_1 = ?," & _
                    " OrarioSabato_1 = ?," & _
                    " OrarioFestivo_1 = ?," & _
                    " Dipendenti_1 = ?," & _
                    " DipendentiSabato_1 = ?," & _
                    " DipendentiFestivo_1 = ?," & _
                    " Orario_2 = ?," & _
                    " OrarioSabato_2 = ?," & _
                    " OrarioFestivo_2 = ?," & _
                    " Dipendenti_2 = ?," & _
                    " DipendentiSabato_2 = ?," & _
                    " DipendentiFestivo_2 = ?," & _
                    " Orario_3 = ?," & _
                    " OrarioSabato_3 = ?," & _
                    " OrarioFestivo_3 = ?," & _
                    " Dipendenti_3 = ?," & _
                    " DipendentiSabato_3 = ?," & _
                    " DipendentiFestivo_3 = ?," & _
                    " Orario_4 = ?," & _
                    " OrarioSabato_4 = ?," & _
                    " OrarioFestivo_4 = ?," & _
                    " Dipendenti_4 = ?," & _
                    " DipendentiSabato_4 = ?," & _
                    " DipendentiFestivo_4 = ?," & _
                    " Orario_5 = ?," & _
                    " OrarioSabato_5 = ?," & _
                    " OrarioFestivo_5 = ?," & _
                    " Dipendenti_5 = ?," & _
                    " DipendentiSabato_5 = ?," & _
                    " DipendentiFestivo_5 = ?," & _
                    " Orario_6 = ?," & _
                    " OrarioSabato_6 = ?," & _
                    " OrarioFestivo_6 = ?," & _
                    " Dipendenti_6 = ?," & _
                    " DipendentiSabato_6 = ?," & _
                    " DipendentiFestivo_6 = ?," & _
                    " Orario_7 = ?," & _
                    " OrarioSabato_7 = ?," & _
                    " OrarioFestivo_7 = ?," & _
                    " Dipendenti_7 = ?," & _
                    " DipendentiSabato_7 = ?," & _
                    " DipendentiFestivo_7 = ?," & _
                    " Orario_8 = ?," & _
                    " OrarioSabato_8 = ?," & _
                    " OrarioFestivo_8 = ?," & _
                    " Dipendenti_8 = ?," & _
                    " DipendentiSabato_8 = ?," & _
                    " DipendentiFestivo_8 = ?," & _
                    " Orario_9 = ?," & _
                    " OrarioSabato_9 = ?," & _
                    " OrarioFestivo_9 = ?," & _
                    " Dipendenti_9 = ?," & _
                    " DipendentiSabato_9 = ?," & _
                    " DipendentiFestivo_9 = ?," & _
                    " Orario_10 = ?," & _
                    " OrarioSabato_10 = ?," & _
                    " OrarioFestivo_10 = ?," & _
                    " Dipendenti_10 = ?," & _
                    " DipendentiSabato_10 = ?," & _
                    " DipendentiFestivo_10 = ?," & _
                    " Orario_11 = ?," & _
                    " OrarioSabato_11 = ?," & _
                    " OrarioFestivo_11 = ?," & _
                    " Dipendenti_11 = ?," & _
                    " DipendentiSabato_11 = ?," & _
                    " DipendentiFestivo_11 = ?," & _
                    " Orario_12 = ?," & _
                    " OrarioSabato_12 = ?," & _
                    " OrarioFestivo_12 = ?," & _
                    " Dipendenti_12 = ?," & _
                    " DipendentiSabato_12 = ?," & _
                    " DipendentiFestivo_12 = ?," & _
                    " Orario_13 = ?," & _
                    " OrarioSabato_13 = ?," & _
                    " OrarioFestivo_13 = ?," & _
                    " Dipendenti_13 = ?," & _
                    " DipendentiSabato_13 = ?," & _
                    " DipendentiFestivo_13 = ?," & _
                    " Orario_14 = ?," & _
                    " OrarioSabato_14 = ?," & _
                    " OrarioFestivo_14 = ?," & _
                    " Dipendenti_14 = ?," & _
                    " DipendentiSabato_14 = ?," & _
                    " DipendentiFestivo_14 = ?," & _
                    " Orario_15 = ?," & _
                    " OrarioSabato_15 = ?," & _
                    " OrarioFestivo_15 = ?," & _
                    " Dipendenti_15 = ?," & _
                    " DipendentiSabato_15 = ?," & _
                    " DipendentiFestivo_15 = ?," & _
                    " Orario_16 = ?," & _
                    " OrarioSabato_16 = ?," & _
                    " OrarioFestivo_16 = ?," & _
                    " Dipendenti_16 = ?," & _
                    " DipendentiSabato_16 = ?," & _
                    " DipendentiFestivo_16 = ?," & _
                    " Orario_17 = ?," & _
                    " OrarioSabato_17 = ?," & _
                    " OrarioFestivo_17 = ?," & _
                    " Dipendenti_17 = ?," & _
                    " DipendentiSabato_17 = ?," & _
                    " DipendentiFestivo_17 = ?," & _
                    " Orario_18 = ?," & _
                    " OrarioSabato_18 = ?," & _
                    " OrarioFestivo_18 = ?," & _
                    " Dipendenti_18 = ?," & _
                    " DipendentiSabato_18 = ?," & _
                    " DipendentiFestivo_18 = ?," & _
                    " Orario_19 = ?," & _
                    " OrarioSabato_19 = ?," & _
                    " OrarioFestivo_19 = ?," & _
                    " Dipendenti_19 = ?," & _
                    " DipendentiSabato_19 = ?," & _
                    " DipendentiFestivo_19 = ?," & _
                    " Orario_20 = ?," & _
                    " OrarioSabato_20 = ?," & _
                    " OrarioFestivo_20 = ?," & _
                    " Dipendenti_20 = ?," & _
                    " DipendentiSabato_20 = ?," & _
                    " DipendentiFestivo_20 = ?" & _
                    " WHERE Codice = ?" & _
                    " AND Validita = ?"

            Dim cmdw As New OleDbCommand()
            cmdw.CommandText = MySql
            cmdw.Parameters.AddWithValue("@Utente", Utente)
            cmdw.Parameters.AddWithValue("@DataAggiornamento", Now)
            cmdw.Parameters.AddWithValue("@Orario_1", Orario_1)
            cmdw.Parameters.AddWithValue("@OrarioSabato_1", OrarioSabato_1)
            cmdw.Parameters.AddWithValue("@OrarioFestivo_1", OrarioFestivo_1)
            cmdw.Parameters.AddWithValue("@Dipendenti_1", Dipendenti_1)
            cmdw.Parameters.AddWithValue("@DipendentiSabato_1", DipendentiSabato_1)
            cmdw.Parameters.AddWithValue("@DipendentiFestivo_1", DipendentiFestivo_1)
            cmdw.Parameters.AddWithValue("@Orario_2", Orario_2)
            cmdw.Parameters.AddWithValue("@OrarioSabato_2", OrarioSabato_2)
            cmdw.Parameters.AddWithValue("@OrarioFestivo_2", OrarioFestivo_2)
            cmdw.Parameters.AddWithValue("@Dipendenti_2", Dipendenti_2)
            cmdw.Parameters.AddWithValue("@DipendentiSabato_2", DipendentiSabato_2)
            cmdw.Parameters.AddWithValue("@DipendentiFestivo_2", DipendentiFestivo_2)
            cmdw.Parameters.AddWithValue("@Orario_3", Orario_3)
            cmdw.Parameters.AddWithValue("@OrarioSabato_3", OrarioSabato_3)
            cmdw.Parameters.AddWithValue("@OrarioFestivo_3", OrarioFestivo_3)
            cmdw.Parameters.AddWithValue("@Dipendenti_3", Dipendenti_3)
            cmdw.Parameters.AddWithValue("@DipendentiSabato_3", DipendentiSabato_3)
            cmdw.Parameters.AddWithValue("@DipendentiFestivo_3", DipendentiFestivo_3)
            cmdw.Parameters.AddWithValue("@Orario_4", Orario_4)
            cmdw.Parameters.AddWithValue("@OrarioSabato_4", OrarioSabato_4)
            cmdw.Parameters.AddWithValue("@OrarioFestivo_4", OrarioFestivo_4)
            cmdw.Parameters.AddWithValue("@Dipendenti_4", Dipendenti_4)
            cmdw.Parameters.AddWithValue("@DipendentiSabato_4", DipendentiSabato_4)
            cmdw.Parameters.AddWithValue("@DipendentiFestivo_4", DipendentiFestivo_4)
            cmdw.Parameters.AddWithValue("@Orario_5", Orario_5)
            cmdw.Parameters.AddWithValue("@OrarioSabato_5", OrarioSabato_5)
            cmdw.Parameters.AddWithValue("@OrarioFestivo_5", OrarioFestivo_5)
            cmdw.Parameters.AddWithValue("@Dipendenti_5", Dipendenti_5)
            cmdw.Parameters.AddWithValue("@DipendentiSabato_5", DipendentiSabato_5)
            cmdw.Parameters.AddWithValue("@DipendentiFestivo_5", DipendentiFestivo_5)
            cmdw.Parameters.AddWithValue("@Orario_6", Orario_6)
            cmdw.Parameters.AddWithValue("@OrarioSabato_6", OrarioSabato_6)
            cmdw.Parameters.AddWithValue("@OrarioFestivo_6", OrarioFestivo_6)
            cmdw.Parameters.AddWithValue("@Dipendenti_6", Dipendenti_6)
            cmdw.Parameters.AddWithValue("@DipendentiSabato_6", DipendentiSabato_6)
            cmdw.Parameters.AddWithValue("@DipendentiFestivo_6", DipendentiFestivo_6)
            cmdw.Parameters.AddWithValue("@Orario_7", Orario_7)
            cmdw.Parameters.AddWithValue("@OrarioSabato_7", OrarioSabato_7)
            cmdw.Parameters.AddWithValue("@OrarioFestivo_7", OrarioFestivo_7)
            cmdw.Parameters.AddWithValue("@Dipendenti_7", Dipendenti_7)
            cmdw.Parameters.AddWithValue("@DipendentiSabato_7", DipendentiSabato_7)
            cmdw.Parameters.AddWithValue("@DipendentiFestivo_7", DipendentiFestivo_7)
            cmdw.Parameters.AddWithValue("@Orario_8", Orario_8)
            cmdw.Parameters.AddWithValue("@OrarioSabato_8", OrarioSabato_8)
            cmdw.Parameters.AddWithValue("@OrarioFestivo_8", OrarioFestivo_8)
            cmdw.Parameters.AddWithValue("@Dipendenti_8", Dipendenti_8)
            cmdw.Parameters.AddWithValue("@DipendentiSabato_8", DipendentiSabato_8)
            cmdw.Parameters.AddWithValue("@DipendentiFestivo_8", DipendentiFestivo_8)
            cmdw.Parameters.AddWithValue("@Orario_9", Orario_9)
            cmdw.Parameters.AddWithValue("@OrarioSabato_9", OrarioSabato_9)
            cmdw.Parameters.AddWithValue("@OrarioFestivo_9", OrarioFestivo_9)
            cmdw.Parameters.AddWithValue("@Dipendenti_9", Dipendenti_9)
            cmdw.Parameters.AddWithValue("@DipendentiSabato_9", DipendentiSabato_9)
            cmdw.Parameters.AddWithValue("@DipendentiFestivo_9", DipendentiFestivo_9)
            cmdw.Parameters.AddWithValue("@Orario_10", Orario_10)
            cmdw.Parameters.AddWithValue("@OrarioSabato_10", OrarioSabato_10)
            cmdw.Parameters.AddWithValue("@OrarioFestivo_10", OrarioFestivo_10)
            cmdw.Parameters.AddWithValue("@Dipendenti_10", Dipendenti_10)
            cmdw.Parameters.AddWithValue("@DipendentiSabato_10", DipendentiSabato_10)
            cmdw.Parameters.AddWithValue("@DipendentiFestivo_10", DipendentiFestivo_10)
            cmdw.Parameters.AddWithValue("@Orario_11", Orario_11)
            cmdw.Parameters.AddWithValue("@OrarioSabato_11", OrarioSabato_11)
            cmdw.Parameters.AddWithValue("@OrarioFestivo_11", OrarioFestivo_11)
            cmdw.Parameters.AddWithValue("@Dipendenti_11", Dipendenti_11)
            cmdw.Parameters.AddWithValue("@DipendentiSabato_11", DipendentiSabato_11)
            cmdw.Parameters.AddWithValue("@DipendentiFestivo_11", DipendentiFestivo_11)
            cmdw.Parameters.AddWithValue("@Orario_12", Orario_12)
            cmdw.Parameters.AddWithValue("@OrarioSabato_12", OrarioSabato_12)
            cmdw.Parameters.AddWithValue("@OrarioFestivo_12", OrarioFestivo_12)
            cmdw.Parameters.AddWithValue("@Dipendenti_12", Dipendenti_12)
            cmdw.Parameters.AddWithValue("@DipendentiSabato_12", DipendentiSabato_12)
            cmdw.Parameters.AddWithValue("@DipendentiFestivo_12", DipendentiFestivo_12)
            cmdw.Parameters.AddWithValue("@Orario_13", Orario_13)
            cmdw.Parameters.AddWithValue("@OrarioSabato_13", OrarioSabato_13)
            cmdw.Parameters.AddWithValue("@OrarioFestivo_13", OrarioFestivo_13)
            cmdw.Parameters.AddWithValue("@Dipendenti_13", Dipendenti_13)
            cmdw.Parameters.AddWithValue("@DipendentiSabato_13", DipendentiSabato_13)
            cmdw.Parameters.AddWithValue("@DipendentiFestivo_13", DipendentiFestivo_13)
            cmdw.Parameters.AddWithValue("@Orario_14", Orario_14)
            cmdw.Parameters.AddWithValue("@OrarioSabato_14", OrarioSabato_14)
            cmdw.Parameters.AddWithValue("@OrarioFestivo_14", OrarioFestivo_14)
            cmdw.Parameters.AddWithValue("@Dipendenti_14", Dipendenti_14)
            cmdw.Parameters.AddWithValue("@DipendentiSabato_14", DipendentiSabato_14)
            cmdw.Parameters.AddWithValue("@DipendentiFestivo_14", DipendentiFestivo_14)
            cmdw.Parameters.AddWithValue("@Orario_15", Orario_15)
            cmdw.Parameters.AddWithValue("@OrarioSabato_15", OrarioSabato_15)
            cmdw.Parameters.AddWithValue("@OrarioFestivo_15", OrarioFestivo_15)
            cmdw.Parameters.AddWithValue("@Dipendenti_15", Dipendenti_15)
            cmdw.Parameters.AddWithValue("@DipendentiSabato_15", DipendentiSabato_15)
            cmdw.Parameters.AddWithValue("@DipendentiFestivo_15", DipendentiFestivo_15)
            cmdw.Parameters.AddWithValue("@Orario_16", Orario_16)
            cmdw.Parameters.AddWithValue("@OrarioSabato_16", OrarioSabato_16)
            cmdw.Parameters.AddWithValue("@OrarioFestivo_16", OrarioFestivo_16)
            cmdw.Parameters.AddWithValue("@Dipendenti_16", Dipendenti_16)
            cmdw.Parameters.AddWithValue("@DipendentiSabato_16", DipendentiSabato_16)
            cmdw.Parameters.AddWithValue("@DipendentiFestivo_16", DipendentiFestivo_16)
            cmdw.Parameters.AddWithValue("@Orario_17", Orario_17)
            cmdw.Parameters.AddWithValue("@OrarioSabato_17", OrarioSabato_17)
            cmdw.Parameters.AddWithValue("@OrarioFestivo_17", OrarioFestivo_17)
            cmdw.Parameters.AddWithValue("@Dipendenti_17", Dipendenti_17)
            cmdw.Parameters.AddWithValue("@DipendentiSabato_17", DipendentiSabato_17)
            cmdw.Parameters.AddWithValue("@DipendentiFestivo_17", DipendentiFestivo_17)
            cmdw.Parameters.AddWithValue("@Orario_18", Orario_18)
            cmdw.Parameters.AddWithValue("@OrarioSabato_18", OrarioSabato_18)
            cmdw.Parameters.AddWithValue("@OrarioFestivo_18", OrarioFestivo_18)
            cmdw.Parameters.AddWithValue("@Dipendenti_18", Dipendenti_18)
            cmdw.Parameters.AddWithValue("@DipendentiSabato_18", DipendentiSabato_18)
            cmdw.Parameters.AddWithValue("@DipendentiFestivo_18", DipendentiFestivo_18)
            cmdw.Parameters.AddWithValue("@Orario_19", Orario_19)
            cmdw.Parameters.AddWithValue("@OrarioSabato_19", OrarioSabato_19)
            cmdw.Parameters.AddWithValue("@OrarioFestivo_19", OrarioFestivo_19)
            cmdw.Parameters.AddWithValue("@Dipendenti_19", Dipendenti_19)
            cmdw.Parameters.AddWithValue("@DipendentiSabato_19", DipendentiSabato_19)
            cmdw.Parameters.AddWithValue("@DipendentiFestivo_19", DipendentiFestivo_1)
            cmdw.Parameters.AddWithValue("@Orario_20", Orario_20)
            cmdw.Parameters.AddWithValue("@OrarioSabato_20", OrarioSabato_20)
            cmdw.Parameters.AddWithValue("@OrarioFestivo_20", OrarioFestivo_20)
            cmdw.Parameters.AddWithValue("@Dipendenti_20", Dipendenti_20)
            cmdw.Parameters.AddWithValue("@DipendentiSabato_20", DipendentiSabato_20)
            cmdw.Parameters.AddWithValue("@DipendentiFestivo_20", DipendentiFestivo_20)
            cmdw.Parameters.AddWithValue("@Codice", Codice)
            cmdw.Parameters.AddWithValue("@Validita", Validita)
            cmdw.Connection = cn
            cmdw.ExecuteNonQuery()
        Else
            MySql = "INSERT INTO OrariPerGruppo (Utente, DataAggiornamento, Codice, Validita, Orario_1, OrarioFestivo_1, Dipendenti_1, DipendentiFestivo_1, Orario_2, OrarioFestivo_2, Dipendenti_2, DipendentiFestivo_2, Orario_3, OrarioFestivo_3, Dipendenti_3, DipendentiFestivo_3, Orario_4, OrarioFestivo_4, Dipendenti_4, DipendentiFestivo_4, Orario_5, OrarioFestivo_5, Dipendenti_5, DipendentiFestivo_5, Orario_6, OrarioFestivo_6, Dipendenti_6, DipendentiFestivo_6, Orario_7, OrarioFestivo_7, Dipendenti_7, DipendentiFestivo_7, Orario_8, OrarioFestivo_8, Dipendenti_8, DipendentiFestivo_8, Orario_9, OrarioFestivo_9, Dipendenti_9, DipendentiFestivo_9, Orario_10, OrarioFestivo_10, Dipendenti_10, DipendentiFestivo_10) VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)"

            Dim cmdw As New OleDbCommand()
            cmdw.CommandText = MySql
            cmdw.Parameters.AddWithValue("@Utente", Utente)
            cmdw.Parameters.AddWithValue("@DataAggiornamento", Now)
            cmdw.Parameters.AddWithValue("@Codice", Codice)
            cmdw.Parameters.AddWithValue("@Validita", Validita)
            cmdw.Parameters.AddWithValue("@Orario_1", Orario_1)
            cmdw.Parameters.AddWithValue("@OrarioSabato_1", OrarioSabato_1)
            cmdw.Parameters.AddWithValue("@OrarioFestivo_1", OrarioFestivo_1)
            cmdw.Parameters.AddWithValue("@Dipendenti_1", Dipendenti_1)
            cmdw.Parameters.AddWithValue("@DipendentiSabato_1", DipendentiSabato_1)
            cmdw.Parameters.AddWithValue("@DipendentiFestivo_1", DipendentiFestivo_1)
            cmdw.Parameters.AddWithValue("@Orario_2", Orario_2)
            cmdw.Parameters.AddWithValue("@OrarioSabato_2", OrarioSabato_2)
            cmdw.Parameters.AddWithValue("@OrarioFestivo_2", OrarioFestivo_2)
            cmdw.Parameters.AddWithValue("@Dipendenti_2", Dipendenti_2)
            cmdw.Parameters.AddWithValue("@DipendentiSabato_2", DipendentiSabato_2)
            cmdw.Parameters.AddWithValue("@DipendentiFestivo_2", DipendentiFestivo_2)
            cmdw.Parameters.AddWithValue("@Orario_3", Orario_3)
            cmdw.Parameters.AddWithValue("@OrarioSabato_3", OrarioSabato_3)
            cmdw.Parameters.AddWithValue("@OrarioFestivo_3", OrarioFestivo_3)
            cmdw.Parameters.AddWithValue("@Dipendenti_3", Dipendenti_3)
            cmdw.Parameters.AddWithValue("@DipendentiSabato_3", DipendentiSabato_3)
            cmdw.Parameters.AddWithValue("@DipendentiFestivo_3", DipendentiFestivo_3)
            cmdw.Parameters.AddWithValue("@Orario_4", Orario_4)
            cmdw.Parameters.AddWithValue("@OrarioSabato_4", OrarioSabato_4)
            cmdw.Parameters.AddWithValue("@OrarioFestivo_4", OrarioFestivo_4)
            cmdw.Parameters.AddWithValue("@Dipendenti_4", Dipendenti_4)
            cmdw.Parameters.AddWithValue("@DipendentiSabato_4", DipendentiSabato_4)
            cmdw.Parameters.AddWithValue("@DipendentiFestivo_4", DipendentiFestivo_4)
            cmdw.Parameters.AddWithValue("@Orario_5", Orario_5)
            cmdw.Parameters.AddWithValue("@OrarioSabato_5", OrarioSabato_5)
            cmdw.Parameters.AddWithValue("@OrarioFestivo_5", OrarioFestivo_5)
            cmdw.Parameters.AddWithValue("@Dipendenti_5", Dipendenti_5)
            cmdw.Parameters.AddWithValue("@DipendentiSabato_5", DipendentiSabato_5)
            cmdw.Parameters.AddWithValue("@DipendentiFestivo_5", DipendentiFestivo_5)
            cmdw.Parameters.AddWithValue("@Orario_6", Orario_6)
            cmdw.Parameters.AddWithValue("@OrarioSabato_6", OrarioSabato_6)
            cmdw.Parameters.AddWithValue("@OrarioFestivo_6", OrarioFestivo_6)
            cmdw.Parameters.AddWithValue("@Dipendenti_6", Dipendenti_6)
            cmdw.Parameters.AddWithValue("@DipendentiSabato_6", DipendentiSabato_6)
            cmdw.Parameters.AddWithValue("@DipendentiFestivo_6", DipendentiFestivo_6)
            cmdw.Parameters.AddWithValue("@Orario_7", Orario_7)
            cmdw.Parameters.AddWithValue("@OrarioSabato_7", OrarioSabato_7)
            cmdw.Parameters.AddWithValue("@OrarioFestivo_7", OrarioFestivo_7)
            cmdw.Parameters.AddWithValue("@Dipendenti_7", Dipendenti_7)
            cmdw.Parameters.AddWithValue("@DipendentiSabato_7", DipendentiSabato_7)
            cmdw.Parameters.AddWithValue("@DipendentiFestivo_7", DipendentiFestivo_7)
            cmdw.Parameters.AddWithValue("@Orario_8", Orario_8)
            cmdw.Parameters.AddWithValue("@OrarioSabato_8", OrarioSabato_8)
            cmdw.Parameters.AddWithValue("@OrarioFestivo_8", OrarioFestivo_8)
            cmdw.Parameters.AddWithValue("@Dipendenti_8", Dipendenti_8)
            cmdw.Parameters.AddWithValue("@DipendentiSabato_8", DipendentiSabato_8)
            cmdw.Parameters.AddWithValue("@DipendentiFestivo_8", DipendentiFestivo_8)
            cmdw.Parameters.AddWithValue("@Orario_9", Orario_9)
            cmdw.Parameters.AddWithValue("@OrarioSabato_9", OrarioSabato_9)
            cmdw.Parameters.AddWithValue("@OrarioFestivo_9", OrarioFestivo_9)
            cmdw.Parameters.AddWithValue("@Dipendenti_9", Dipendenti_9)
            cmdw.Parameters.AddWithValue("@DipendentiSabato_9", DipendentiSabato_9)
            cmdw.Parameters.AddWithValue("@DipendentiFestivo_9", DipendentiFestivo_9)
            cmdw.Parameters.AddWithValue("@Orario_10", Orario_10)
            cmdw.Parameters.AddWithValue("@OrarioSabato_10", OrarioSabato_10)
            cmdw.Parameters.AddWithValue("@OrarioFestivo_10", OrarioFestivo_10)
            cmdw.Parameters.AddWithValue("@Dipendenti_10", Dipendenti_10)
            cmdw.Parameters.AddWithValue("@DipendentiSabato_10", DipendentiSabato_10)
            cmdw.Parameters.AddWithValue("@DipendentiFestivo_10", DipendentiFestivo_10)
            cmdw.Parameters.AddWithValue("@Orario_11", Orario_11)
            cmdw.Parameters.AddWithValue("@OrarioSabato_11", OrarioSabato_11)
            cmdw.Parameters.AddWithValue("@OrarioFestivo_11", OrarioFestivo_11)
            cmdw.Parameters.AddWithValue("@Dipendenti_11", Dipendenti_11)
            cmdw.Parameters.AddWithValue("@DipendentiSabato_11", DipendentiSabato_11)
            cmdw.Parameters.AddWithValue("@DipendentiFestivo_11", DipendentiFestivo_11)
            cmdw.Parameters.AddWithValue("@Orario_12", Orario_12)
            cmdw.Parameters.AddWithValue("@OrarioSabato_12", OrarioSabato_12)
            cmdw.Parameters.AddWithValue("@OrarioFestivo_12", OrarioFestivo_12)
            cmdw.Parameters.AddWithValue("@Dipendenti_12", Dipendenti_12)
            cmdw.Parameters.AddWithValue("@DipendentiSabato_12", DipendentiSabato_12)
            cmdw.Parameters.AddWithValue("@DipendentiFestivo_12", DipendentiFestivo_12)
            cmdw.Parameters.AddWithValue("@Orario_13", Orario_13)
            cmdw.Parameters.AddWithValue("@OrarioSabato_13", OrarioSabato_13)
            cmdw.Parameters.AddWithValue("@OrarioFestivo_13", OrarioFestivo_13)
            cmdw.Parameters.AddWithValue("@Dipendenti_13", Dipendenti_13)
            cmdw.Parameters.AddWithValue("@DipendentiSabato_13", DipendentiSabato_13)
            cmdw.Parameters.AddWithValue("@DipendentiFestivo_13", DipendentiFestivo_13)
            cmdw.Parameters.AddWithValue("@Orario_14", Orario_14)
            cmdw.Parameters.AddWithValue("@OrarioSabato_14", OrarioSabato_14)
            cmdw.Parameters.AddWithValue("@OrarioFestivo_14", OrarioFestivo_14)
            cmdw.Parameters.AddWithValue("@Dipendenti_14", Dipendenti_14)
            cmdw.Parameters.AddWithValue("@DipendentiSabato_14", DipendentiSabato_14)
            cmdw.Parameters.AddWithValue("@DipendentiFestivo_14", DipendentiFestivo_14)
            cmdw.Parameters.AddWithValue("@Orario_15", Orario_15)
            cmdw.Parameters.AddWithValue("@OrarioSabato_15", OrarioSabato_15)
            cmdw.Parameters.AddWithValue("@OrarioFestivo_15", OrarioFestivo_15)
            cmdw.Parameters.AddWithValue("@Dipendenti_15", Dipendenti_15)
            cmdw.Parameters.AddWithValue("@DipendentiSabato_15", DipendentiSabato_15)
            cmdw.Parameters.AddWithValue("@DipendentiFestivo_15", DipendentiFestivo_15)
            cmdw.Parameters.AddWithValue("@Orario_16", Orario_16)
            cmdw.Parameters.AddWithValue("@OrarioSabato_16", OrarioSabato_16)
            cmdw.Parameters.AddWithValue("@OrarioFestivo_16", OrarioFestivo_16)
            cmdw.Parameters.AddWithValue("@Dipendenti_16", Dipendenti_16)
            cmdw.Parameters.AddWithValue("@DipendentiSabato_16", DipendentiSabato_16)
            cmdw.Parameters.AddWithValue("@DipendentiFestivo_16", DipendentiFestivo_16)
            cmdw.Parameters.AddWithValue("@Orario_17", Orario_17)
            cmdw.Parameters.AddWithValue("@OrarioSabato_17", OrarioSabato_17)
            cmdw.Parameters.AddWithValue("@OrarioFestivo_17", OrarioFestivo_17)
            cmdw.Parameters.AddWithValue("@Dipendenti_17", Dipendenti_17)
            cmdw.Parameters.AddWithValue("@DipendentiSabato_17", DipendentiSabato_17)
            cmdw.Parameters.AddWithValue("@DipendentiFestivo_17", DipendentiFestivo_17)
            cmdw.Parameters.AddWithValue("@Orario_18", Orario_18)
            cmdw.Parameters.AddWithValue("@OrarioSabato_18", OrarioSabato_18)
            cmdw.Parameters.AddWithValue("@OrarioFestivo_18", OrarioFestivo_18)
            cmdw.Parameters.AddWithValue("@Dipendenti_18", Dipendenti_18)
            cmdw.Parameters.AddWithValue("@DipendentiSabato_18", DipendentiSabato_18)
            cmdw.Parameters.AddWithValue("@DipendentiFestivo_18", DipendentiFestivo_18)
            cmdw.Parameters.AddWithValue("@Orario_19", Orario_19)
            cmdw.Parameters.AddWithValue("@OrarioSabato_19", OrarioSabato_19)
            cmdw.Parameters.AddWithValue("@OrarioFestivo_19", OrarioFestivo_19)
            cmdw.Parameters.AddWithValue("@Dipendenti_19", Dipendenti_19)
            cmdw.Parameters.AddWithValue("@DipendentiSabato_19", DipendentiSabato_19)
            cmdw.Parameters.AddWithValue("@DipendentiFestivo_19", DipendentiFestivo_1)
            cmdw.Parameters.AddWithValue("@Orario_20", Orario_20)
            cmdw.Parameters.AddWithValue("@OrarioSabato_20", OrarioSabato_20)
            cmdw.Parameters.AddWithValue("@OrarioFestivo_20", OrarioFestivo_20)
            cmdw.Parameters.AddWithValue("@Dipendenti_20", Dipendenti_20)
            cmdw.Parameters.AddWithValue("@DipendentiSabato_20", DipendentiSabato_20)
            cmdw.Parameters.AddWithValue("@DipendentiFestivo_20", DipendentiFestivo_20)
            cmdw.Connection = cn
            cmdw.ExecuteNonQuery()
        End If
        myPOSTreader.Close()
        cn.Close()

        Call Pulisci()
    End Sub

    Sub LoadDati(ByVal ConnectionString As String, ByVal Tabella As System.Data.DataTable)
        Dim cn As OleDbConnection

        cn = New Data.OleDb.OleDbConnection(ConnectionString)

        cn.Open()
        Dim cmd As New OleDbCommand()

        cmd.CommandText = ("Select * From OrariPerGruppo Order By Codice, Validita")

        cmd.Connection = cn

        Tabella.Clear()
        Tabella.Columns.Clear()
        Tabella.Columns.Add("Codice", GetType(String))
        Tabella.Columns.Add("Validita", GetType(String))
        Tabella.Columns.Add("Orario_1", GetType(String))
        Tabella.Columns.Add("OrarioSabato_1", GetType(String))
        Tabella.Columns.Add("OrarioFestivo_1", GetType(String))
        Tabella.Columns.Add("Dipendenti_1", GetType(String))
        Tabella.Columns.Add("DipendentiSabato_1", GetType(String))
        Tabella.Columns.Add("DipendentiFestivo_1", GetType(String))
        Tabella.Columns.Add("Orario_2", GetType(String))
        Tabella.Columns.Add("OrarioSabato_2", GetType(String))
        Tabella.Columns.Add("OrarioFestivo_2", GetType(String))
        Tabella.Columns.Add("Dipendenti_2", GetType(String))
        Tabella.Columns.Add("DipendentiSabato_2", GetType(String))
        Tabella.Columns.Add("DipendentiFestivo_2", GetType(String))
        Tabella.Columns.Add("Orario_3", GetType(String))
        Tabella.Columns.Add("OrarioSabato_3", GetType(String))
        Tabella.Columns.Add("OrarioFestivo_3", GetType(String))
        Tabella.Columns.Add("Dipendenti_3", GetType(String))
        Tabella.Columns.Add("DipendentiSabato_3", GetType(String))
        Tabella.Columns.Add("DipendentiFestivo_3", GetType(String))
        Tabella.Columns.Add("Orario_4", GetType(String))
        Tabella.Columns.Add("OrarioSabato_4", GetType(String))
        Tabella.Columns.Add("OrarioFestivo_4", GetType(String))
        Tabella.Columns.Add("Dipendenti_4", GetType(String))
        Tabella.Columns.Add("DipendentiSabato_4", GetType(String))
        Tabella.Columns.Add("DipendentiFestivo_4", GetType(String))
        Tabella.Columns.Add("Orario_5", GetType(String))
        Tabella.Columns.Add("OrarioSabato_5", GetType(String))
        Tabella.Columns.Add("OrarioFestivo_5", GetType(String))
        Tabella.Columns.Add("Dipendenti_5", GetType(String))
        Tabella.Columns.Add("DipendentiSabato_5", GetType(String))
        Tabella.Columns.Add("DipendentiFestivo_5", GetType(String))
        Tabella.Columns.Add("Orario_6", GetType(String))
        Tabella.Columns.Add("OrarioSabato_6", GetType(String))
        Tabella.Columns.Add("OrarioFestivo_6", GetType(String))
        Tabella.Columns.Add("Dipendenti_6", GetType(String))
        Tabella.Columns.Add("DipendentiSabato_6", GetType(String))
        Tabella.Columns.Add("DipendentiFestivo_6", GetType(String))
        Tabella.Columns.Add("Orario_7", GetType(String))
        Tabella.Columns.Add("OrarioSabato_7", GetType(String))
        Tabella.Columns.Add("OrarioFestivo_7", GetType(String))
        Tabella.Columns.Add("Dipendenti_7", GetType(String))
        Tabella.Columns.Add("DipendentiSabato_7", GetType(String))
        Tabella.Columns.Add("DipendentiFestivo_7", GetType(String))
        Tabella.Columns.Add("Orario_8", GetType(String))
        Tabella.Columns.Add("OrarioSabato_8", GetType(String))
        Tabella.Columns.Add("OrarioFestivo_8", GetType(String))
        Tabella.Columns.Add("Dipendenti_8", GetType(String))
        Tabella.Columns.Add("DipendentiSabato_8", GetType(String))
        Tabella.Columns.Add("DipendentiFestivo_8", GetType(String))
        Tabella.Columns.Add("Orario_9", GetType(String))
        Tabella.Columns.Add("OrarioSabato_9", GetType(String))
        Tabella.Columns.Add("OrarioFestivo_9", GetType(String))
        Tabella.Columns.Add("Dipendenti_9", GetType(String))
        Tabella.Columns.Add("DipendentiSabato_9", GetType(String))
        Tabella.Columns.Add("DipendentiFestivo_9", GetType(String))
        Tabella.Columns.Add("Orario_10", GetType(String))
        Tabella.Columns.Add("OrarioSabato_10", GetType(String))
        Tabella.Columns.Add("OrarioFestivo_10", GetType(String))
        Tabella.Columns.Add("Dipendenti_10", GetType(String))
        Tabella.Columns.Add("DipendentiSabato_10", GetType(String))
        Tabella.Columns.Add("DipendentiFestivo_10", GetType(String))
        Tabella.Columns.Add("Orario_11", GetType(String))
        Tabella.Columns.Add("OrarioSabato_11", GetType(String))
        Tabella.Columns.Add("OrarioFestivo_11", GetType(String))
        Tabella.Columns.Add("Dipendenti_11", GetType(String))
        Tabella.Columns.Add("DipendentiSabato_11", GetType(String))
        Tabella.Columns.Add("DipendentiFestivo_11", GetType(String))
        Tabella.Columns.Add("Orario_12", GetType(String))
        Tabella.Columns.Add("OrarioSabato_12", GetType(String))
        Tabella.Columns.Add("OrarioFestivo_12", GetType(String))
        Tabella.Columns.Add("Dipendenti_12", GetType(String))
        Tabella.Columns.Add("DipendentiSabato_12", GetType(String))
        Tabella.Columns.Add("DipendentiFestivo_12", GetType(String))
        Tabella.Columns.Add("Orario_13", GetType(String))
        Tabella.Columns.Add("OrarioSabato_13", GetType(String))
        Tabella.Columns.Add("OrarioFestivo_13", GetType(String))
        Tabella.Columns.Add("Dipendenti_13", GetType(String))
        Tabella.Columns.Add("DipendentiSabato_13", GetType(String))
        Tabella.Columns.Add("DipendentiFestivo_13", GetType(String))
        Tabella.Columns.Add("Orario_14", GetType(String))
        Tabella.Columns.Add("OrarioSabato_14", GetType(String))
        Tabella.Columns.Add("OrarioFestivo_14", GetType(String))
        Tabella.Columns.Add("Dipendenti_14", GetType(String))
        Tabella.Columns.Add("DipendentiSabato_14", GetType(String))
        Tabella.Columns.Add("DipendentiFestivo_14", GetType(String))
        Tabella.Columns.Add("Orario_15", GetType(String))
        Tabella.Columns.Add("OrarioSabato_15", GetType(String))
        Tabella.Columns.Add("OrarioFestivo_15", GetType(String))
        Tabella.Columns.Add("Dipendenti_15", GetType(String))
        Tabella.Columns.Add("DipendentiSabato_15", GetType(String))
        Tabella.Columns.Add("DipendentiFestivo_15", GetType(String))
        Tabella.Columns.Add("Orario_16", GetType(String))
        Tabella.Columns.Add("OrarioSabato_16", GetType(String))
        Tabella.Columns.Add("OrarioFestivo_16", GetType(String))
        Tabella.Columns.Add("Dipendenti_16", GetType(String))
        Tabella.Columns.Add("DipendentiSabato_16", GetType(String))
        Tabella.Columns.Add("DipendentiFestivo_16", GetType(String))
        Tabella.Columns.Add("Orario_17", GetType(String))
        Tabella.Columns.Add("OrarioSabato_17", GetType(String))
        Tabella.Columns.Add("OrarioFestivo_17", GetType(String))
        Tabella.Columns.Add("Dipendenti_17", GetType(String))
        Tabella.Columns.Add("DipendentiSabato_17", GetType(String))
        Tabella.Columns.Add("DipendentiFestivo_17", GetType(String))
        Tabella.Columns.Add("Orario_18", GetType(String))
        Tabella.Columns.Add("OrarioSabato_18", GetType(String))
        Tabella.Columns.Add("OrarioFestivo_18", GetType(String))
        Tabella.Columns.Add("Dipendenti_18", GetType(String))
        Tabella.Columns.Add("DipendentiSabato_18", GetType(String))
        Tabella.Columns.Add("DipendentiFestivo_18", GetType(String))
        Tabella.Columns.Add("Orario_19", GetType(String))
        Tabella.Columns.Add("OrarioSabato_19", GetType(String))
        Tabella.Columns.Add("OrarioFestivo_19", GetType(String))
        Tabella.Columns.Add("Dipendenti_19", GetType(String))
        Tabella.Columns.Add("DipendentiSabato_19", GetType(String))
        Tabella.Columns.Add("DipendentiFestivo_19", GetType(String))
        Tabella.Columns.Add("Orario_20", GetType(String))
        Tabella.Columns.Add("OrarioSabato_20", GetType(String))
        Tabella.Columns.Add("OrarioFestivo_20", GetType(String))
        Tabella.Columns.Add("Dipendenti_20", GetType(String))
        Tabella.Columns.Add("DipendentiSabato_20", GetType(String))
        Tabella.Columns.Add("DipendentiFestivo_20", GetType(String))

        Dim myPOSTreader As OleDbDataReader = cmd.ExecuteReader()
        Do While myPOSTreader.Read

            Dim myriga As System.Data.DataRow = Tabella.NewRow()
            myriga(0) = myPOSTreader.Item("Codice")
            myriga(1) = myPOSTreader.Item("Validita")
            myriga(2) = myPOSTreader.Item("Orario_1")
            myriga(3) = myPOSTreader.Item("OrarioSabato_1")
            myriga(4) = myPOSTreader.Item("OrarioFestivo_1")
            myriga(5) = myPOSTreader.Item("Dipendenti_1")
            myriga(6) = myPOSTreader.Item("DipendentiSabato_1")
            myriga(7) = myPOSTreader.Item("DipendentiFestivo_1")
            myriga(8) = myPOSTreader.Item("Orario_2")
            myriga(9) = myPOSTreader.Item("OrarioSabato_2")
            myriga(10) = myPOSTreader.Item("OrarioFestivo_2")
            myriga(11) = myPOSTreader.Item("Dipendenti_2")
            myriga(12) = myPOSTreader.Item("DipendentiSabato_2")
            myriga(13) = myPOSTreader.Item("DipendentiFestivo_2")
            myriga(14) = myPOSTreader.Item("Orario_3")
            myriga(15) = myPOSTreader.Item("OrarioSabato_3")
            myriga(16) = myPOSTreader.Item("OrarioFestivo_3")
            myriga(17) = myPOSTreader.Item("Dipendenti_3")
            myriga(18) = myPOSTreader.Item("DipendentiSabato_3")
            myriga(19) = myPOSTreader.Item("DipendentiFestivo_3")
            myriga(20) = myPOSTreader.Item("Orario_4")
            myriga(21) = myPOSTreader.Item("OrarioSabato_4")
            myriga(22) = myPOSTreader.Item("OrarioFestivo_4")
            myriga(23) = myPOSTreader.Item("Dipendenti_4")
            myriga(24) = myPOSTreader.Item("DipendentiSabato_4")
            myriga(25) = myPOSTreader.Item("DipendentiFestivo_4")
            myriga(26) = myPOSTreader.Item("Orario_5")
            myriga(27) = myPOSTreader.Item("OrarioSabato_5")
            myriga(28) = myPOSTreader.Item("OrarioFestivo_5")
            myriga(29) = myPOSTreader.Item("Dipendenti_5")
            myriga(30) = myPOSTreader.Item("DipendentiSabato_5")
            myriga(31) = myPOSTreader.Item("DipendentiFestivo_5")
            myriga(32) = myPOSTreader.Item("Orario_6")
            myriga(33) = myPOSTreader.Item("OrarioSabato_6")
            myriga(34) = myPOSTreader.Item("OrarioFestivo_6")
            myriga(35) = myPOSTreader.Item("Dipendenti_6")
            myriga(36) = myPOSTreader.Item("DipendentiSabato_6")
            myriga(37) = myPOSTreader.Item("DipendentiFestivo_6")
            myriga(38) = myPOSTreader.Item("Orario_7")
            myriga(39) = myPOSTreader.Item("OrarioSabato_7")
            myriga(40) = myPOSTreader.Item("OrarioFestivo_7")
            myriga(41) = myPOSTreader.Item("Dipendenti_7")
            myriga(42) = myPOSTreader.Item("DipendentiSabato_7")
            myriga(43) = myPOSTreader.Item("DipendentiFestivo_7")
            myriga(44) = myPOSTreader.Item("Orario_8")
            myriga(45) = myPOSTreader.Item("OrarioSabato_8")
            myriga(46) = myPOSTreader.Item("OrarioFestivo_8")
            myriga(47) = myPOSTreader.Item("Dipendenti_8")
            myriga(48) = myPOSTreader.Item("DipendentiSabato_8")
            myriga(49) = myPOSTreader.Item("DipendentiFestivo_8")
            myriga(50) = myPOSTreader.Item("Orario_9")
            myriga(51) = myPOSTreader.Item("OrarioSabato_9")
            myriga(52) = myPOSTreader.Item("OrarioFestivo_9")
            myriga(53) = myPOSTreader.Item("Dipendenti_9")
            myriga(54) = myPOSTreader.Item("DipendentiSabato_9")
            myriga(55) = myPOSTreader.Item("DipendentiFestivo_9")
            myriga(56) = myPOSTreader.Item("Orario_10")
            myriga(57) = myPOSTreader.Item("OrarioSabato_10")
            myriga(58) = myPOSTreader.Item("OrarioFestivo_10")
            myriga(59) = myPOSTreader.Item("Dipendenti_10")
            myriga(60) = myPOSTreader.Item("DipendentiSabato_10")
            myriga(61) = myPOSTreader.Item("DipendentiFestivo_10")
            myriga(62) = myPOSTreader.Item("Orario_11")
            myriga(63) = myPOSTreader.Item("OrarioSabato_11")
            myriga(64) = myPOSTreader.Item("OrarioFestivo_11")
            myriga(65) = myPOSTreader.Item("Dipendenti_11")
            myriga(66) = myPOSTreader.Item("DipendentiSabato_11")
            myriga(67) = myPOSTreader.Item("DipendentiFestivo_11")
            myriga(68) = myPOSTreader.Item("Orario_12")
            myriga(69) = myPOSTreader.Item("OrarioSabato_12")
            myriga(70) = myPOSTreader.Item("OrarioFestivo_12")
            myriga(71) = myPOSTreader.Item("Dipendenti_12")
            myriga(72) = myPOSTreader.Item("DipendentiSabato_12")
            myriga(73) = myPOSTreader.Item("DipendentiFestivo_12")
            myriga(74) = myPOSTreader.Item("Orario_13")
            myriga(75) = myPOSTreader.Item("OrarioSabato_13")
            myriga(76) = myPOSTreader.Item("OrarioFestivo_13")
            myriga(77) = myPOSTreader.Item("Dipendenti_13")
            myriga(78) = myPOSTreader.Item("DipendentiSabato_13")
            myriga(79) = myPOSTreader.Item("DipendentiFestivo_13")
            myriga(80) = myPOSTreader.Item("Orario_14")
            myriga(81) = myPOSTreader.Item("OrarioSabato_14")
            myriga(82) = myPOSTreader.Item("OrarioFestivo_14")
            myriga(83) = myPOSTreader.Item("Dipendenti_14")
            myriga(84) = myPOSTreader.Item("DipendentiSabato_14")
            myriga(85) = myPOSTreader.Item("DipendentiFestivo_14")
            myriga(86) = myPOSTreader.Item("Orario_15")
            myriga(87) = myPOSTreader.Item("OrarioSabato_15")
            myriga(88) = myPOSTreader.Item("OrarioFestivo_15")
            myriga(89) = myPOSTreader.Item("Dipendenti_15")
            myriga(90) = myPOSTreader.Item("DipendentiSabato_15")
            myriga(91) = myPOSTreader.Item("DipendentiFestivo_15")
            myriga(92) = myPOSTreader.Item("Orario_16")
            myriga(93) = myPOSTreader.Item("OrarioSabato_16")
            myriga(94) = myPOSTreader.Item("OrarioFestivo_16")
            myriga(95) = myPOSTreader.Item("Dipendenti_16")
            myriga(96) = myPOSTreader.Item("DipendentiSabato_16")
            myriga(97) = myPOSTreader.Item("DipendentiFestivo_16")
            myriga(98) = myPOSTreader.Item("Orario_17")
            myriga(99) = myPOSTreader.Item("OrarioSabato_17")
            myriga(100) = myPOSTreader.Item("OrarioFestivo_17")
            myriga(101) = myPOSTreader.Item("Dipendenti_17")
            myriga(102) = myPOSTreader.Item("DipendentiSabato_17")
            myriga(103) = myPOSTreader.Item("DipendentiFestivo_17")
            myriga(104) = myPOSTreader.Item("Orario_18")
            myriga(105) = myPOSTreader.Item("OrarioSabato_18")
            myriga(106) = myPOSTreader.Item("OrarioFestivo_18")
            myriga(107) = myPOSTreader.Item("Dipendenti_18")
            myriga(108) = myPOSTreader.Item("DipendentiSabato_18")
            myriga(109) = myPOSTreader.Item("DipendentiFestivo_18")
            myriga(110) = myPOSTreader.Item("Orario_19")
            myriga(111) = myPOSTreader.Item("OrarioSabato_19")
            myriga(112) = myPOSTreader.Item("OrarioFestivo_19")
            myriga(113) = myPOSTreader.Item("Dipendenti_19")
            myriga(114) = myPOSTreader.Item("DipendentiSabato_19")
            myriga(115) = myPOSTreader.Item("DipendentiFestivo_19")
            myriga(116) = myPOSTreader.Item("Orario_20")
            myriga(117) = myPOSTreader.Item("OrarioSabato_20")
            myriga(118) = myPOSTreader.Item("OrarioFestivo_20")
            myriga(119) = myPOSTreader.Item("Dipendenti_20")
            myriga(120) = myPOSTreader.Item("DipendentiSabato_20")
            myriga(121) = myPOSTreader.Item("DipendentiFestivo_20")

            Tabella.Rows.Add(myriga)
        Loop
        myPOSTreader.Close()
        cn.Close()

    End Sub

    Function StringaDb(ByVal Oggetto As Object) As String
        If IsDBNull(Oggetto) Then
            Return ""
        Else
            Return Oggetto
        End If
    End Function

    Function DataDb(ByVal Oggetto As Object) As Date
        If Not IsDate(Oggetto) Then
            Return Nothing
        Else
            Return Oggetto
        End If
    End Function

    Function NumeroDb(ByVal Oggetto As Object) As Object
        If IsDBNull(Oggetto) Then
            Return 0
        Else
            Return Oggetto
        End If
    End Function

    Sub Leggi(ByVal ConnectionString As String, ByVal xCodice As String, ByVal xValidita As Date)
        Dim cn As OleDbConnection

        cn = New Data.OleDb.OleDbConnection(ConnectionString)

        cn.Open()
        Dim cmd As New OleDbCommand()
        cmd.CommandText = ("Select * From OrariPerGruppo WHERE " & _
                           " Codice = ?" & _
                           " AND Validita = ?")
        cmd.Parameters.AddWithValue("@Codice", xCodice)
        cmd.Parameters.AddWithValue("@Validita", xValidita)
        cmd.Connection = cn

        Dim myPOSTreader As OleDbDataReader = cmd.ExecuteReader()
        If myPOSTreader.Read Then
            Id = NumeroDb(myPOSTreader.Item("Id"))
            Codice = StringaDb(myPOSTreader.Item("Codice"))
            Validita = DataDb(myPOSTreader.Item("Validita"))
            Orario_1 = StringaDb(myPOSTreader.Item("Orario_1"))
            OrarioSabato_1 = StringaDb(myPOSTreader.Item("OrarioSabato_1"))
            OrarioFestivo_1 = StringaDb(myPOSTreader.Item("OrarioFestivo_1"))
            Dipendenti_1 = NumeroDb(myPOSTreader.Item("Dipendenti_1"))
            DipendentiSabato_1 = NumeroDb(myPOSTreader.Item("DipendentiSabato_1"))
            DipendentiFestivo_1 = NumeroDb(myPOSTreader.Item("DipendentiFestivo_1"))
            Orario_2 = StringaDb(myPOSTreader.Item("Orario_2"))
            OrarioSabato_2 = StringaDb(myPOSTreader.Item("OrarioSabato_2"))
            OrarioFestivo_2 = StringaDb(myPOSTreader.Item("OrarioFestivo_2"))
            Dipendenti_2 = NumeroDb(myPOSTreader.Item("Dipendenti_2"))
            DipendentiSabato_2 = NumeroDb(myPOSTreader.Item("DipendentiSabato_2"))
            DipendentiFestivo_2 = NumeroDb(myPOSTreader.Item("DipendentiFestivo_2"))
            Orario_3 = StringaDb(myPOSTreader.Item("Orario_3"))
            OrarioSabato_3 = StringaDb(myPOSTreader.Item("OrarioSabato_3"))
            OrarioFestivo_3 = StringaDb(myPOSTreader.Item("OrarioFestivo_3"))
            Dipendenti_3 = NumeroDb(myPOSTreader.Item("Dipendenti_3"))
            DipendentiSabato_3 = NumeroDb(myPOSTreader.Item("DipendentiSabato_3"))
            DipendentiFestivo_3 = NumeroDb(myPOSTreader.Item("DipendentiFestivo_3"))
            Orario_4 = StringaDb(myPOSTreader.Item("Orario_4"))
            OrarioSabato_4 = StringaDb(myPOSTreader.Item("OrarioSabato_4"))
            OrarioFestivo_4 = StringaDb(myPOSTreader.Item("OrarioFestivo_4"))
            Dipendenti_4 = NumeroDb(myPOSTreader.Item("Dipendenti_4"))
            DipendentiSabato_4 = NumeroDb(myPOSTreader.Item("DipendentiSabato_4"))
            DipendentiFestivo_4 = NumeroDb(myPOSTreader.Item("DipendentiFestivo_4"))
            Orario_5 = StringaDb(myPOSTreader.Item("Orario_5"))
            OrarioSabato_5 = StringaDb(myPOSTreader.Item("OrarioSabato_5"))
            OrarioFestivo_5 = StringaDb(myPOSTreader.Item("OrarioFestivo_5"))
            Dipendenti_5 = NumeroDb(myPOSTreader.Item("Dipendenti_5"))
            DipendentiSabato_5 = NumeroDb(myPOSTreader.Item("DipendentiSabato_5"))
            DipendentiFestivo_5 = NumeroDb(myPOSTreader.Item("DipendentiFestivo_5"))
            Orario_6 = StringaDb(myPOSTreader.Item("Orario_6"))
            OrarioSabato_6 = StringaDb(myPOSTreader.Item("OrarioSabato_6"))
            OrarioFestivo_6 = StringaDb(myPOSTreader.Item("OrarioFestivo_6"))
            Dipendenti_6 = NumeroDb(myPOSTreader.Item("Dipendenti_6"))
            DipendentiSabato_6 = NumeroDb(myPOSTreader.Item("DipendentiSabato_6"))
            DipendentiFestivo_6 = NumeroDb(myPOSTreader.Item("DipendentiFestivo_6"))
            Orario_7 = StringaDb(myPOSTreader.Item("Orario_7"))
            OrarioSabato_7 = StringaDb(myPOSTreader.Item("OrarioSabato_7"))
            OrarioFestivo_7 = StringaDb(myPOSTreader.Item("OrarioFestivo_7"))
            Dipendenti_7 = NumeroDb(myPOSTreader.Item("Dipendenti_7"))
            DipendentiSabato_7 = NumeroDb(myPOSTreader.Item("DipendentiSabato_7"))
            DipendentiFestivo_7 = NumeroDb(myPOSTreader.Item("DipendentiFestivo_7"))
            Orario_8 = StringaDb(myPOSTreader.Item("Orario_8"))
            OrarioSabato_8 = StringaDb(myPOSTreader.Item("OrarioSabato_8"))
            OrarioFestivo_8 = StringaDb(myPOSTreader.Item("OrarioFestivo_8"))
            Dipendenti_8 = NumeroDb(myPOSTreader.Item("Dipendenti_8"))
            DipendentiSabato_8 = NumeroDb(myPOSTreader.Item("DipendentiSabato_8"))
            DipendentiFestivo_8 = NumeroDb(myPOSTreader.Item("DipendentiFestivo_8"))
            Orario_9 = StringaDb(myPOSTreader.Item("Orario_9"))
            OrarioSabato_9 = StringaDb(myPOSTreader.Item("OrarioSabato_9"))
            OrarioFestivo_9 = StringaDb(myPOSTreader.Item("OrarioFestivo_9"))
            Dipendenti_9 = NumeroDb(myPOSTreader.Item("Dipendenti_9"))
            DipendentiSabato_9 = NumeroDb(myPOSTreader.Item("DipendentiSabato_9"))
            DipendentiFestivo_9 = NumeroDb(myPOSTreader.Item("DipendentiFestivo_9"))
            Orario_10 = StringaDb(myPOSTreader.Item("Orario_10"))
            OrarioSabato_10 = StringaDb(myPOSTreader.Item("OrarioSabato_10"))
            OrarioFestivo_10 = StringaDb(myPOSTreader.Item("OrarioFestivo_10"))
            Dipendenti_10 = NumeroDb(myPOSTreader.Item("Dipendenti_10"))
            DipendentiSabato_10 = NumeroDb(myPOSTreader.Item("DipendentiSabato_10"))
            DipendentiFestivo_10 = NumeroDb(myPOSTreader.Item("DipendentiFestivo_10"))
            Orario_11 = StringaDb(myPOSTreader.Item("Orario_11"))
            OrarioSabato_11 = StringaDb(myPOSTreader.Item("OrarioSabato_11"))
            OrarioFestivo_11 = StringaDb(myPOSTreader.Item("OrarioFestivo_11"))
            Dipendenti_11 = NumeroDb(myPOSTreader.Item("Dipendenti_11"))
            DipendentiSabato_11 = NumeroDb(myPOSTreader.Item("DipendentiSabato_11"))
            DipendentiFestivo_11 = NumeroDb(myPOSTreader.Item("DipendentiFestivo_11"))
            Orario_12 = StringaDb(myPOSTreader.Item("Orario_12"))
            OrarioSabato_12 = StringaDb(myPOSTreader.Item("OrarioSabato_12"))
            OrarioFestivo_12 = StringaDb(myPOSTreader.Item("OrarioFestivo_12"))
            Dipendenti_12 = NumeroDb(myPOSTreader.Item("Dipendenti_12"))
            DipendentiSabato_12 = NumeroDb(myPOSTreader.Item("DipendentiSabato_12"))
            DipendentiFestivo_12 = NumeroDb(myPOSTreader.Item("DipendentiFestivo_12"))
            Orario_13 = StringaDb(myPOSTreader.Item("Orario_13"))
            OrarioSabato_13 = StringaDb(myPOSTreader.Item("OrarioSabato_13"))
            OrarioFestivo_13 = StringaDb(myPOSTreader.Item("OrarioFestivo_13"))
            Dipendenti_13 = NumeroDb(myPOSTreader.Item("Dipendenti_13"))
            DipendentiSabato_13 = NumeroDb(myPOSTreader.Item("DipendentiSabato_13"))
            DipendentiFestivo_13 = NumeroDb(myPOSTreader.Item("DipendentiFestivo_13"))
            Orario_14 = StringaDb(myPOSTreader.Item("Orario_14"))
            OrarioSabato_14 = StringaDb(myPOSTreader.Item("OrarioSabato_14"))
            OrarioFestivo_14 = StringaDb(myPOSTreader.Item("OrarioFestivo_14"))
            Dipendenti_14 = NumeroDb(myPOSTreader.Item("Dipendenti_14"))
            DipendentiSabato_14 = NumeroDb(myPOSTreader.Item("DipendentiSabato_14"))
            DipendentiFestivo_14 = NumeroDb(myPOSTreader.Item("DipendentiFestivo_14"))
            Orario_15 = StringaDb(myPOSTreader.Item("Orario_15"))
            OrarioSabato_15 = StringaDb(myPOSTreader.Item("OrarioSabato_15"))
            OrarioFestivo_15 = StringaDb(myPOSTreader.Item("OrarioFestivo_15"))
            Dipendenti_15 = NumeroDb(myPOSTreader.Item("Dipendenti_15"))
            DipendentiSabato_15 = NumeroDb(myPOSTreader.Item("DipendentiSabato_15"))
            DipendentiFestivo_15 = NumeroDb(myPOSTreader.Item("DipendentiFestivo_15"))
            Orario_16 = StringaDb(myPOSTreader.Item("Orario_16"))
            OrarioSabato_16 = StringaDb(myPOSTreader.Item("OrarioSabato_16"))
            OrarioFestivo_16 = StringaDb(myPOSTreader.Item("OrarioFestivo_16"))
            Dipendenti_16 = NumeroDb(myPOSTreader.Item("Dipendenti_16"))
            DipendentiSabato_16 = NumeroDb(myPOSTreader.Item("DipendentiSabato_16"))
            DipendentiFestivo_16 = NumeroDb(myPOSTreader.Item("DipendentiFestivo_16"))
            Orario_17 = StringaDb(myPOSTreader.Item("Orario_17"))
            OrarioSabato_17 = StringaDb(myPOSTreader.Item("OrarioSabato_17"))
            OrarioFestivo_17 = StringaDb(myPOSTreader.Item("OrarioFestivo_17"))
            Dipendenti_17 = NumeroDb(myPOSTreader.Item("Dipendenti_17"))
            DipendentiSabato_17 = NumeroDb(myPOSTreader.Item("DipendentiSabato_17"))
            DipendentiFestivo_17 = NumeroDb(myPOSTreader.Item("DipendentiFestivo_17"))
            Orario_18 = StringaDb(myPOSTreader.Item("Orario_18"))
            OrarioSabato_18 = StringaDb(myPOSTreader.Item("OrarioSabato_18"))
            OrarioFestivo_18 = StringaDb(myPOSTreader.Item("OrarioFestivo_18"))
            Dipendenti_18 = NumeroDb(myPOSTreader.Item("Dipendenti_18"))
            DipendentiSabato_18 = NumeroDb(myPOSTreader.Item("DipendentiSabato_18"))
            DipendentiFestivo_18 = NumeroDb(myPOSTreader.Item("DipendentiFestivo_18"))
            Orario_19 = StringaDb(myPOSTreader.Item("Orario_19"))
            OrarioSabato_19 = StringaDb(myPOSTreader.Item("OrarioSabato_19"))
            OrarioFestivo_19 = StringaDb(myPOSTreader.Item("OrarioFestivo_19"))
            Dipendenti_19 = NumeroDb(myPOSTreader.Item("Dipendenti_19"))
            DipendentiSabato_19 = NumeroDb(myPOSTreader.Item("DipendentiSabato_19"))
            DipendentiFestivo_19 = NumeroDb(myPOSTreader.Item("DipendentiFestivo_19"))
            Orario_20 = StringaDb(myPOSTreader.Item("Orario_20"))
            OrarioSabato_20 = StringaDb(myPOSTreader.Item("OrarioSabato_20"))
            OrarioFestivo_20 = StringaDb(myPOSTreader.Item("OrarioFestivo_20"))
            Dipendenti_20 = NumeroDb(myPOSTreader.Item("Dipendenti_20"))
            DipendentiSabato_20 = NumeroDb(myPOSTreader.Item("DipendentiSabato_20"))
            DipendentiFestivo_20 = NumeroDb(myPOSTreader.Item("DipendentiFestivo_20"))
        Else
            Call Pulisci()
        End If
        cn.Close()
    End Sub

    Sub Trova(ByVal ConnectionString As String, ByVal xCodice As String, ByVal xValidita As Date)
        Dim cn As OleDbConnection

        cn = New Data.OleDb.OleDbConnection(ConnectionString)

        cn.Open()
        Dim cmd As New OleDbCommand()
        cmd.CommandText = "Select TOP 1 * FROM OrariPerGruppo WHERE " & _
                          " Codice = ?" & _
                          " AND Validita <= ?" & _
                          " ORDER BY Validita DESC, Id DESC"

        cmd.Parameters.AddWithValue("@Codice", xCodice)
        cmd.Parameters.AddWithValue("@Validita", xValidita)
        cmd.Connection = cn

        Dim myPOSTreader As OleDbDataReader = cmd.ExecuteReader()
        If myPOSTreader.Read Then
            Id = NumeroDb(myPOSTreader.Item("Id"))
            Codice = StringaDb(myPOSTreader.Item("Codice"))
            Validita = DataDb(myPOSTreader.Item("Validita"))
            Orario_1 = StringaDb(myPOSTreader.Item("Orario_1"))
            OrarioSabato_1 = StringaDb(myPOSTreader.Item("OrarioSabato_1"))
            OrarioFestivo_1 = StringaDb(myPOSTreader.Item("OrarioFestivo_1"))
            Dipendenti_1 = NumeroDb(myPOSTreader.Item("Dipendenti_1"))
            DipendentiSabato_1 = NumeroDb(myPOSTreader.Item("DipendentiSabato_1"))
            DipendentiFestivo_1 = NumeroDb(myPOSTreader.Item("DipendentiFestivo_1"))
            Orario_2 = StringaDb(myPOSTreader.Item("Orario_2"))
            OrarioSabato_2 = StringaDb(myPOSTreader.Item("OrarioSabato_2"))
            OrarioFestivo_2 = StringaDb(myPOSTreader.Item("OrarioFestivo_2"))
            Dipendenti_2 = NumeroDb(myPOSTreader.Item("Dipendenti_2"))
            DipendentiSabato_2 = NumeroDb(myPOSTreader.Item("DipendentiSabato_2"))
            DipendentiFestivo_2 = NumeroDb(myPOSTreader.Item("DipendentiFestivo_2"))
            Orario_3 = StringaDb(myPOSTreader.Item("Orario_3"))
            OrarioSabato_3 = StringaDb(myPOSTreader.Item("OrarioSabato_3"))
            OrarioFestivo_3 = StringaDb(myPOSTreader.Item("OrarioFestivo_3"))
            Dipendenti_3 = NumeroDb(myPOSTreader.Item("Dipendenti_3"))
            DipendentiSabato_3 = NumeroDb(myPOSTreader.Item("DipendentiSabato_3"))
            DipendentiFestivo_3 = NumeroDb(myPOSTreader.Item("DipendentiFestivo_3"))
            Orario_4 = StringaDb(myPOSTreader.Item("Orario_4"))
            OrarioSabato_4 = StringaDb(myPOSTreader.Item("OrarioSabato_4"))
            OrarioFestivo_4 = StringaDb(myPOSTreader.Item("OrarioFestivo_4"))
            Dipendenti_4 = NumeroDb(myPOSTreader.Item("Dipendenti_4"))
            DipendentiSabato_4 = NumeroDb(myPOSTreader.Item("DipendentiSabato_4"))
            DipendentiFestivo_4 = NumeroDb(myPOSTreader.Item("DipendentiFestivo_4"))
            Orario_5 = StringaDb(myPOSTreader.Item("Orario_5"))
            OrarioSabato_5 = StringaDb(myPOSTreader.Item("OrarioSabato_5"))
            OrarioFestivo_5 = StringaDb(myPOSTreader.Item("OrarioFestivo_5"))
            Dipendenti_5 = NumeroDb(myPOSTreader.Item("Dipendenti_5"))
            DipendentiSabato_5 = NumeroDb(myPOSTreader.Item("DipendentiSabato_5"))
            DipendentiFestivo_5 = NumeroDb(myPOSTreader.Item("DipendentiFestivo_5"))
            Orario_6 = StringaDb(myPOSTreader.Item("Orario_6"))
            OrarioSabato_6 = StringaDb(myPOSTreader.Item("OrarioSabato_6"))
            OrarioFestivo_6 = StringaDb(myPOSTreader.Item("OrarioFestivo_6"))
            Dipendenti_6 = NumeroDb(myPOSTreader.Item("Dipendenti_6"))
            DipendentiSabato_6 = NumeroDb(myPOSTreader.Item("DipendentiSabato_6"))
            DipendentiFestivo_6 = NumeroDb(myPOSTreader.Item("DipendentiFestivo_6"))
            Orario_7 = StringaDb(myPOSTreader.Item("Orario_7"))
            OrarioSabato_7 = StringaDb(myPOSTreader.Item("OrarioSabato_7"))
            OrarioFestivo_7 = StringaDb(myPOSTreader.Item("OrarioFestivo_7"))
            Dipendenti_7 = NumeroDb(myPOSTreader.Item("Dipendenti_7"))
            DipendentiSabato_7 = NumeroDb(myPOSTreader.Item("DipendentiSabato_7"))
            DipendentiFestivo_7 = NumeroDb(myPOSTreader.Item("DipendentiFestivo_7"))
            Orario_8 = StringaDb(myPOSTreader.Item("Orario_8"))
            OrarioSabato_8 = StringaDb(myPOSTreader.Item("OrarioSabato_8"))
            OrarioFestivo_8 = StringaDb(myPOSTreader.Item("OrarioFestivo_8"))
            Dipendenti_8 = NumeroDb(myPOSTreader.Item("Dipendenti_8"))
            DipendentiSabato_8 = NumeroDb(myPOSTreader.Item("DipendentiSabato_8"))
            DipendentiFestivo_8 = NumeroDb(myPOSTreader.Item("DipendentiFestivo_8"))
            Orario_9 = StringaDb(myPOSTreader.Item("Orario_9"))
            OrarioSabato_9 = StringaDb(myPOSTreader.Item("OrarioSabato_9"))
            OrarioFestivo_9 = StringaDb(myPOSTreader.Item("OrarioFestivo_9"))
            Dipendenti_9 = NumeroDb(myPOSTreader.Item("Dipendenti_9"))
            DipendentiSabato_9 = NumeroDb(myPOSTreader.Item("DipendentiSabato_9"))
            DipendentiFestivo_9 = NumeroDb(myPOSTreader.Item("DipendentiFestivo_9"))
            Orario_10 = StringaDb(myPOSTreader.Item("Orario_10"))
            OrarioSabato_10 = StringaDb(myPOSTreader.Item("OrarioSabato_10"))
            OrarioFestivo_10 = StringaDb(myPOSTreader.Item("OrarioFestivo_10"))
            Dipendenti_10 = NumeroDb(myPOSTreader.Item("Dipendenti_10"))
            DipendentiSabato_10 = NumeroDb(myPOSTreader.Item("DipendentiSabato_10"))
            DipendentiFestivo_10 = NumeroDb(myPOSTreader.Item("DipendentiFestivo_10"))
            Orario_11 = StringaDb(myPOSTreader.Item("Orario_11"))
            OrarioSabato_11 = StringaDb(myPOSTreader.Item("OrarioSabato_11"))
            OrarioFestivo_11 = StringaDb(myPOSTreader.Item("OrarioFestivo_11"))
            Dipendenti_11 = NumeroDb(myPOSTreader.Item("Dipendenti_11"))
            DipendentiSabato_11 = NumeroDb(myPOSTreader.Item("DipendentiSabato_11"))
            DipendentiFestivo_11 = NumeroDb(myPOSTreader.Item("DipendentiFestivo_11"))
            Orario_12 = StringaDb(myPOSTreader.Item("Orario_12"))
            OrarioSabato_12 = StringaDb(myPOSTreader.Item("OrarioSabato_12"))
            OrarioFestivo_12 = StringaDb(myPOSTreader.Item("OrarioFestivo_12"))
            Dipendenti_12 = NumeroDb(myPOSTreader.Item("Dipendenti_12"))
            DipendentiSabato_12 = NumeroDb(myPOSTreader.Item("DipendentiSabato_12"))
            DipendentiFestivo_12 = NumeroDb(myPOSTreader.Item("DipendentiFestivo_12"))
            Orario_13 = StringaDb(myPOSTreader.Item("Orario_13"))
            OrarioSabato_13 = StringaDb(myPOSTreader.Item("OrarioSabato_13"))
            OrarioFestivo_13 = StringaDb(myPOSTreader.Item("OrarioFestivo_13"))
            Dipendenti_13 = NumeroDb(myPOSTreader.Item("Dipendenti_13"))
            DipendentiSabato_13 = NumeroDb(myPOSTreader.Item("DipendentiSabato_13"))
            DipendentiFestivo_13 = NumeroDb(myPOSTreader.Item("DipendentiFestivo_13"))
            Orario_14 = StringaDb(myPOSTreader.Item("Orario_14"))
            OrarioSabato_14 = StringaDb(myPOSTreader.Item("OrarioSabato_14"))
            OrarioFestivo_14 = StringaDb(myPOSTreader.Item("OrarioFestivo_14"))
            Dipendenti_14 = NumeroDb(myPOSTreader.Item("Dipendenti_14"))
            DipendentiSabato_14 = NumeroDb(myPOSTreader.Item("DipendentiSabato_14"))
            DipendentiFestivo_14 = NumeroDb(myPOSTreader.Item("DipendentiFestivo_14"))
            Orario_15 = StringaDb(myPOSTreader.Item("Orario_15"))
            OrarioSabato_15 = StringaDb(myPOSTreader.Item("OrarioSabato_15"))
            OrarioFestivo_15 = StringaDb(myPOSTreader.Item("OrarioFestivo_15"))
            Dipendenti_15 = NumeroDb(myPOSTreader.Item("Dipendenti_15"))
            DipendentiSabato_15 = NumeroDb(myPOSTreader.Item("DipendentiSabato_15"))
            DipendentiFestivo_15 = NumeroDb(myPOSTreader.Item("DipendentiFestivo_15"))
            Orario_16 = StringaDb(myPOSTreader.Item("Orario_16"))
            OrarioSabato_16 = StringaDb(myPOSTreader.Item("OrarioSabato_16"))
            OrarioFestivo_16 = StringaDb(myPOSTreader.Item("OrarioFestivo_16"))
            Dipendenti_16 = NumeroDb(myPOSTreader.Item("Dipendenti_16"))
            DipendentiSabato_16 = NumeroDb(myPOSTreader.Item("DipendentiSabato_16"))
            DipendentiFestivo_16 = NumeroDb(myPOSTreader.Item("DipendentiFestivo_16"))
            Orario_17 = StringaDb(myPOSTreader.Item("Orario_17"))
            OrarioSabato_17 = StringaDb(myPOSTreader.Item("OrarioSabato_17"))
            OrarioFestivo_17 = StringaDb(myPOSTreader.Item("OrarioFestivo_17"))
            Dipendenti_17 = NumeroDb(myPOSTreader.Item("Dipendenti_17"))
            DipendentiSabato_17 = NumeroDb(myPOSTreader.Item("DipendentiSabato_17"))
            DipendentiFestivo_17 = NumeroDb(myPOSTreader.Item("DipendentiFestivo_17"))
            Orario_18 = StringaDb(myPOSTreader.Item("Orario_18"))
            OrarioSabato_18 = StringaDb(myPOSTreader.Item("OrarioSabato_18"))
            OrarioFestivo_18 = StringaDb(myPOSTreader.Item("OrarioFestivo_18"))
            Dipendenti_18 = NumeroDb(myPOSTreader.Item("Dipendenti_18"))
            DipendentiSabato_18 = NumeroDb(myPOSTreader.Item("DipendentiSabato_18"))
            DipendentiFestivo_18 = NumeroDb(myPOSTreader.Item("DipendentiFestivo_18"))
            Orario_19 = StringaDb(myPOSTreader.Item("Orario_19"))
            OrarioSabato_19 = StringaDb(myPOSTreader.Item("OrarioSabato_19"))
            OrarioFestivo_19 = StringaDb(myPOSTreader.Item("OrarioFestivo_19"))
            Dipendenti_19 = NumeroDb(myPOSTreader.Item("Dipendenti_19"))
            DipendentiSabato_19 = NumeroDb(myPOSTreader.Item("DipendentiSabato_19"))
            DipendentiFestivo_19 = NumeroDb(myPOSTreader.Item("DipendentiFestivo_19"))
            Orario_20 = StringaDb(myPOSTreader.Item("Orario_20"))
            OrarioSabato_20 = StringaDb(myPOSTreader.Item("OrarioSabato_20"))
            OrarioFestivo_20 = StringaDb(myPOSTreader.Item("OrarioFestivo_20"))
            Dipendenti_20 = NumeroDb(myPOSTreader.Item("Dipendenti_20"))
            DipendentiSabato_20 = NumeroDb(myPOSTreader.Item("DipendentiSabato_20"))
            DipendentiFestivo_20 = NumeroDb(myPOSTreader.Item("DipendentiFestivo_20"))
        Else
            Call Pulisci()
        End If
        cn.Close()
    End Sub

    Function Esiste(ByVal ConnectionString As String, ByVal xCodice As String, ByVal xValidita As Date) As Boolean
        Dim cn As OleDbConnection

        cn = New Data.OleDb.OleDbConnection(ConnectionString)

        cn.Open()
        Dim cmd As New OleDbCommand()
        cmd.CommandText = "Select * FROM OrariPerGruppo" & _
                          " WHERE Codice = ?" & _
                          " AND Validita = ?"
        cmd.Parameters.AddWithValue("@Codice", xCodice)
        cmd.Parameters.AddWithValue("@Validita", xValidita)
        cmd.Connection = cn

        Dim myPOSTreader As OleDbDataReader = cmd.ExecuteReader()

        If myPOSTreader.Read Then
            Return True
        Else
            Return False
        End If
        myPOSTreader.Close()
        cn.Close()
    End Function

End Class
