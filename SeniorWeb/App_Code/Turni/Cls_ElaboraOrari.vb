﻿Imports Microsoft.VisualBasic
Imports System.Data.OleDb

Public Class Cls_ElaboraOrari

    '------------------------------------------------------------
    'procedura di sviluppo turni per 42 giorni
    '------------------------------------------------------------

    Public Function SviluppoTurniOrdiniServizio(ByVal ConnectionString As String, ByVal Dipendente As Long, ByVal Data As Date, ByVal ZeroAlle As String, ByRef Vettore_Orari As Cls_Struttura_Orari, ByRef Vettore_Timbrature As Cls_Struttura_Timbrature, ByVal Sw_Giorno As String, ByVal Sw_Profilo As String, ByVal Sw_ProfiloFerie As String, ByVal Sw_Variazioni As String, ByVal Sw_Proposta As String, ByVal DataSviluppo As Date) As String
        Dim cpt As New Cls_ParametriTurni
        cpt.Leggi(ConnectionString)

        Dim cxst As New Cls_ClsXScriptTurni
        cxst.ConnectionString = ConnectionString

        Dim DataMin As Date
        Dim DataMax As Date
        Dim Assunzione(41) As String
        Dim CodiceFlessibilita(41) As String
        Dim DataCodiceFlessibilita(41) As Date
        Dim CodiceProfiloFerie(41) As String
        Dim CodiceProfiloFerieTesta(41) As String
        Dim DataCodiceProfiloFerieTesta(41) As Date
        Dim OrarioGiornoFestivoFerie(41) As String
        Dim ControlloNotte(41) As String
        Dim ControlloSmontoNotte(41) As String
        Dim ControlloRiposo(41) As String
        Dim CodiceTurno(41) As String
        Dim CodiceTurnoTesta(41) As String
        Dim DataCodiceTurnoTesta(41) As Date
        Dim CodiceStrutturaTesta(41) As String
        Dim PrimoElementoTesta(41) As Integer
        Dim OrarioGiornoFestivo(41) As String
        Dim OrarioGiornoFestivoInfrasettimanale(41) As String
        Dim W_InAlto(41) As Integer
        Dim W_InBasso(41) As Integer
        Dim ValiditaPreferenze(41) As Date
        Dim NottiSiNo(41) As String
        Dim W_OrarioDalle(10) As String
        Dim W_OrarioDalleObbligo(10) As String
        Dim W_OrarioAlle(10) As String
        Dim W_OrarioAlleObbligo(10) As String
        Dim W_OrarioGiustificativo(10) As String
        Dim W_OrarioNelGruppo(10) As String
        Dim W_OrarioDifferenzaDalle(10) As Long
        Dim W_OrarioDifferenzaAlle(10) As Long
        Dim W_OrarioNotte As String = ""
        Dim W_OrarioSmontoNotte As String = ""
        Dim Contatore As Long
        Dim Chk_Giorno(6) As String
        Dim g_max As Byte
        Dim g_min As Byte
        Dim sg As Byte
        Dim d As Byte
        Dim g As Integer
        Dim Giorni As Integer
        Dim Oggi As Date
        Dim gg As Integer
        Dim Sw_Ok As Boolean

        Dim cn As OleDbConnection

        cn = New Data.OleDb.OleDbConnection(ConnectionString)

        cn.Open()
        Dim cmd As New OleDbCommand()
        cmd.CommandText = "SELECT Codice From Orari" & _
                            " WHERE Giustificativo = ?"
        cmd.Parameters.AddWithValue("@Giustificativo", cpt.GiustificativoNotte)

        cmd.Connection = cn
        Dim myPOSTreader As OleDbDataReader = cmd.ExecuteReader()
        If myPOSTreader.Read Then
            W_OrarioNotte = StringaDb(myPOSTreader.Item("Codice"))
        End If
        myPOSTreader.Close()
        cmd.Parameters.Clear()

        cmd.CommandText = "SELECT Codice From Orari" & _
                            " WHERE Giustificativo = ?"
        cmd.Parameters.AddWithValue("@Giustificativo", cpt.GiustificativoSmontoNotte)

        cmd.Connection = cn
        myPOSTreader = cmd.ExecuteReader()
        If myPOSTreader.Read Then
            W_OrarioSmontoNotte = StringaDb(myPOSTreader.Item("Codice"))
        End If
        myPOSTreader.Close()
        cmd.Parameters.Clear()

        If Sw_Giorno = "S" Then
            g_max = DateDiff("d", DataSviluppo, Data)
            If g_max > 0 Then
                g_min = g_max - 1
            Else
                g_min = g_max
            End If
        Else
            g_min = 0
            g_max = 41
        End If

        DataMin = DateAdd("d", g_min, DataSviluppo)
        DataMax = DateAdd("d", g_max, DataSviluppo)

        If Sw_Giorno = "S" Then
            sg = 1
        Else
            sg = 0
        End If

        d = Weekday(DataMin)
        For g = g_min To g_max + sg

            Call Azzera_Vettore_Orari(g, d, Vettore_Orari)

            d = d + 1
            If d > 7 Then d = 1
            W_InAlto(g) = 0
            W_InBasso(g) = 0
        Next g

        cmd.CommandText = " SELECT * " & _
                          " FROM OrdiniServizio " & _
                          " WHERE CodiceDipendente = ?" & _
                          " AND Data >= ?" & _
                          " AND Data <= ?" & _
                          " ORDER BY Data"
        cmd.Parameters.AddWithValue("@CodiceDipendente", Dipendente)
        cmd.Parameters.AddWithValue("@Data", DataMin)
        cmd.Parameters.AddWithValue("@Data", DataMax)

        cmd.Connection = cn
        myPOSTreader = cmd.ExecuteReader()
        Do While myPOSTreader.Read
            Giorni = DateDiff("d", DataSviluppo, DataDb(myPOSTreader.Item("Data")))
            Vettore_Orari.OrdiniServizio(Giorni) = "S"
        Loop
        myPOSTreader.Close()
        cmd.Parameters.Clear()

        Dim CodiceSuperGruppo As String = ""
        Dim cdv As New Cls_DatiVariabili
        cdv.Trova(ConnectionString, Dipendente, cpt.CodiceAnagraficoSuperGruppo, DataMax)
        If cdv.Id <> 0 Then
            CodiceSupergruppo = cdv.ContenutoTesto
        End If

        cdv.Trova(ConnectionString, Dipendente, cpt.CodiceAnagraficoContratto, DataMax)
        If cdv.Id = 0 Then
            SviluppoTurniOrdiniServizio = "(E) Contratto non indicato"
            Exit Function
        End If
        Dim CodiceContratto As String = cdv.ContenutoTesto

        Dim ccc As New Cls_CondizioniContrattuali
        ccc.Trova(ConnectionString, CodiceContratto, DataMax)
        If ccc.Id = 0 Then
            SviluppoTurniOrdiniServizio = "(E) Condizioni Contrattuali per il Contratto: " & CodiceContratto & "  inesistenti"
            Exit Function
        End If
        Dim FestivitaSettimanale As Byte = ccc.FestivitaSettimanale

        Assunzione(g_min) = "N"

        cmd.CommandText = " SELECT * FROM Matricola" & _
                          " WHERE CodiceDipendente = ?" & _
                          " AND DataAssunzione <= ?" & _
                          " ORDER BY DataAssunzione"
        cmd.Parameters.AddWithValue("@CodiceDipendente", Dipendente)
        cmd.Parameters.AddWithValue("@DataAssunzione", DataMax)

        cmd.Connection = cn
        myPOSTreader = cmd.ExecuteReader()
        Do While myPOSTreader.Read
            If Format(DataDb(myPOSTreader.Item("DataAssunzione")), "yyyyMMdd") <= Format(DataMin, "yyyyMMdd") Then
                Assunzione(g_min) = "S"
            Else
                g = DateDiff("d", DataSviluppo, DataDb(myPOSTreader.Item("DataAssunzione")))
                Assunzione(g) = "S"
            End If
            If DataDb(myPOSTreader.Item("DataLicenziamento")) <> Nothing Then
                If Format(DataDb(myPOSTreader.Item("DataLicenziamento")), "yyyyMMdd") < Format(DataMax, "yyyyMMdd") Then
                    If Format(DataDb(myPOSTreader.Item("DataLicenziamento")), "yyyyMMdd") < Format(DataMin, "yyyyMMdd") Then
                        Assunzione(g_min) = "N"
                    Else
                        g = DateDiff("d", DataSviluppo, DataDb(myPOSTreader.Item("DataLicenziamento"))) + 1
                        Assunzione(g) = "N"
                    End If
                End If
            End If
        Loop
        myPOSTreader.Close()
        cmd.Parameters.Clear()

        For g = g_min + 1 To g_max
            If Assunzione(g) = "" Then Assunzione(g) = Assunzione(g - 1)
        Next g

        cmd.CommandText = " SELECT DatiVariabili.* " & _
                          " FROM DatiVariabili " & _
                          " WHERE CodiceDipendente = ?" & _
                          " AND CodiceVariabile = ?" & _
                          " AND Validita <= ?" & _
                          " ORDER BY Validita"
        cmd.Parameters.AddWithValue("@CodiceDipendente", Dipendente)
        cmd.Parameters.AddWithValue("@CodiceVariabile", cpt.CodiceAnagraficoFasceOrarie)
        cmd.Parameters.AddWithValue("@Validita", DataMax)

        cmd.Connection = cn
        myPOSTreader = cmd.ExecuteReader()
        Do While myPOSTreader.Read
            If Format(DataDb(myPOSTreader.Item("Validita")), "yyyyMMdd") > Format(DataMin, "yyyyMMdd") Then
                Giorni = DateDiff("d", DataSviluppo, DataDb(myPOSTreader.Item("Validita")))
                Vettore_Orari.FasceOrarie(Giorni) = StringaDb(myPOSTreader.Item("ContenutoTesto"))
            Else
                Vettore_Orari.FasceOrarie(g_min) = StringaDb(myPOSTreader.Item("ContenutoTesto"))
            End If
        Loop
        myPOSTreader.Close()
        cmd.Parameters.Clear()

        Dim i As Integer
        Dim j As Byte
        For i = g_max To g_min Step -1
            If Vettore_Orari.FasceOrarie(i) <> "" Then
                For j = i + 1 To g_max
                    If Vettore_Orari.FasceOrarie(j) = "" Then
                        Vettore_Orari.FasceOrarie(j) = Vettore_Orari.FasceOrarie(i)
                    Else
                        Exit For
                    End If
                Next j
            End If
        Next i

        cmd.CommandText = " SELECT DatiVariabili.* " & _
                          " FROM DatiVariabili " & _
                          " WHERE CodiceDipendente = ?" & _
                          " AND CodiceVariabile = ?" & _
                          " AND Validita <= ?" & _
                          " ORDER BY Validita"
        cmd.Parameters.AddWithValue("@CodiceDipendente", Dipendente)
        cmd.Parameters.AddWithValue("@CodiceVariabile", cpt.CodiceAnagraficoFlessibilita)
        cmd.Parameters.AddWithValue("@Validita", DataMax)

        cmd.Connection = cn
        myPOSTreader = cmd.ExecuteReader()
        Do While myPOSTreader.Read
            If Format(DataDb(myPOSTreader.Item("Validita")), "yyyyMMdd") > Format(DataMin, "yyyyMMdd") Then
                Giorni = DateDiff("d", DataSviluppo, DataDb(myPOSTreader.Item("Validita")))
                CodiceFlessibilita(Giorni) = StringaDb(myPOSTreader.Item("ContenutoTesto"))
            Else
                CodiceFlessibilita(g_min) = StringaDb(myPOSTreader.Item("ContenutoTesto"))
            End If
        Loop
        myPOSTreader.Close()
        cmd.Parameters.Clear()

        For i = g_max To g_min Step -1
            If CodiceFlessibilita(i) <> "" Then
                DataCodiceFlessibilita(i) = ValiditaOrarioFlessibile(ConnectionString, CodiceFlessibilita(i), DateAdd("d", i, DataMin))
                For j = i + 1 To g_max
                    If CodiceFlessibilita(j) = "" Then
                        CodiceFlessibilita(j) = CodiceFlessibilita(i)
                        DataCodiceFlessibilita(j) = ValiditaOrarioFlessibile(ConnectionString, CodiceFlessibilita(i), DateAdd("d", j, DataMin))
                    Else
                        Exit For
                    End If
                Next j
            End If
        Next i

        Call Carica_Vettore_Orari_ProfiliOrariVariati(ConnectionString, Dipendente, DataMin, DataMax, DataSviluppo, Sw_Variazioni, Sw_Proposta, Vettore_Orari)

        If Sw_ProfiloFerie = "S" Then
            cmd.CommandText = " SELECT DatiVariabili.* " & _
                              " FROM DatiVariabili " & _
                              " WHERE CodiceDipendente = ?" & _
                              " AND CodiceVariabile = ?" & _
                              " AND Validita <= ?" & _
                              " ORDER BY Validita"
            cmd.Parameters.AddWithValue("@CodiceDipendente", Dipendente)
            cmd.Parameters.AddWithValue("@CodiceVariabile", cpt.CodiceAnagraficoProfiloFerie)
            cmd.Parameters.AddWithValue("@Validita", DataMax)

            cmd.Connection = cn
            myPOSTreader = cmd.ExecuteReader()
            Do While myPOSTreader.Read
                If Format(DataDb(myPOSTreader.Item("Validita")), "yyyyMMdd") > Format(DataMin, "yyyyMMdd") Then
                    Giorni = DateDiff("d", DataSviluppo, DataDb(myPOSTreader.Item("Validita")))
                    CodiceProfiloFerie(Giorni) = StringaDb(myPOSTreader.Item("ContenutoTesto"))
                Else
                    CodiceProfiloFerie(g_min) = StringaDb(myPOSTreader.Item("ContenutoTesto"))
                End If
            Loop
            myPOSTreader.Close()
            cmd.Parameters.Clear()

            For i = g_max To g_min Step -1
                If CodiceProfiloFerie(i) <> "" Then
                    Oggi = DateAdd("d", i, DataMin)
                    cmd.CommandText = "SELECT * FROM ProfiliFerieTesta" & _
                                      " WHERE Codice = '" & CodiceProfiloFerie(i) & "'" & _
                                      " AND Validita <= {ts '" & Format(DataMax, "yyyy-mm-dd") & " 00:00:00'} " & _
                                      " ORDER BY Validita DESC"
                    cmd.Parameters.AddWithValue("@Codice", CodiceProfiloFerie(i))
                    cmd.Parameters.AddWithValue("@Validita", DataMax)

                    cmd.Connection = cn
                    myPOSTreader = cmd.ExecuteReader()
                    Do While myPOSTreader.Read
                        If Format(DataDb(myPOSTreader.Item("Validita")), "yyyyMMdd") > Format(Oggi, "yyyyMMdd") Then
                            gg = DateDiff("d", DataSviluppo, DataDb(myPOSTreader.Item("Validita")))
                            For j = gg To g_max
                                If CodiceProfiloFerieTesta(j) = "" Then
                                    CodiceProfiloFerieTesta(j) = StringaDb(myPOSTreader.Item("Codice"))
                                    DataCodiceProfiloFerieTesta(j) = DataDb(myPOSTreader.Item("Validita"))
                                    OrarioGiornoFestivoFerie(j) = StringaDb(myPOSTreader.Item("OrarioGiornoFestivo"))
                                    ControlloNotte(j) = StringaDb(myPOSTreader.Item("ControlloNotte"))
                                    ControlloSmontoNotte(j) = StringaDb(myPOSTreader.Item("ControlloSmontoNotte"))
                                    ControlloRiposo(j) = StringaDb(myPOSTreader.Item("ControlloRiposo"))
                                Else
                                    Exit For
                                End If
                            Next j
                        Else
                            For j = i To g_max
                                If CodiceProfiloFerieTesta(j) = "" Then
                                    CodiceProfiloFerieTesta(j) = StringaDb(myPOSTreader.Item("Codice"))
                                    DataCodiceProfiloFerieTesta(j) = DataDb(myPOSTreader.Item("Validita"))
                                    OrarioGiornoFestivoFerie(j) = StringaDb(myPOSTreader.Item("OrarioGiornoFestivo"))
                                    ControlloNotte(j) = StringaDb(myPOSTreader.Item("ControlloNotte"))
                                    ControlloSmontoNotte(j) = StringaDb(myPOSTreader.Item("ControlloSmontoNotte"))
                                    ControlloRiposo(j) = StringaDb(myPOSTreader.Item("ControlloRiposo"))
                                Else
                                    Exit For
                                End If
                            Next j
                            Exit Do
                        End If
                    Loop
                    myPOSTreader.Close()
                    cmd.Parameters.Clear()
                End If
            Next i
        End If

        cmd.CommandText = " SELECT DatiVariabili.* " & _
                  " FROM DatiVariabili " & _
                  " WHERE CodiceDipendente = ?" & _
                  " AND CodiceVariabile = ?" & _
                  " AND Validita <= ?" & _
                  " ORDER BY Validita"
        cmd.Parameters.AddWithValue("@CodiceDipendente", Dipendente)
        cmd.Parameters.AddWithValue("@CodiceVariabile", cpt.CodiceAnagraficoTurno)
        cmd.Parameters.AddWithValue("@Validita", DataMax)

        cmd.Connection = cn
        myPOSTreader = cmd.ExecuteReader()
        Do While myPOSTreader.Read
            If Format(DataDb(myPOSTreader.Item("Validita")), "yyyyMMdd") > Format(DataMin, "yyyyMMdd") Then
                Giorni = DateDiff("d", DataSviluppo, DataDb(myPOSTreader.Item("Validita")))
                CodiceTurno(Giorni) = StringaDb(myPOSTreader.Item("ContenutoTesto"))
            Else
                CodiceTurno(g_min) = StringaDb(myPOSTreader.Item("ContenutoTesto"))
            End If
        Loop
        myPOSTreader.Close()
        cmd.Parameters.Clear()

        cmd.CommandText = "SELECT * FROM PreferenzeTesta" & _
                          " WHERE CodiceDipendente = ?" & _
                          " AND Validita <= ?" & _
                          " ORDER BY Validita"
        cmd.Parameters.AddWithValue("@CodiceDipendente", Dipendente)
        cmd.Parameters.AddWithValue("@Validita", DataMax)

        cmd.Connection = cn
        myPOSTreader = cmd.ExecuteReader()
        Do While myPOSTreader.Read
            If Format(DataDb(myPOSTreader.Item("Validita")), "yyyymmdd") > Format(DataMin, "yyyymmdd") Then
                Giorni = DateDiff("d", DataSviluppo, DataDb(myPOSTreader.Item("Validita")))
                ValiditaPreferenze(Giorni) = DataDb(myPOSTreader.Item("Validita"))
            Else
                ValiditaPreferenze(g_min) = DataDb(myPOSTreader.Item("Validita"))
            End If
        Loop
        myPOSTreader.Close()
        cmd.Parameters.Clear()

        For i = 0 To 40
            If ValiditaPreferenze(i + 1) = Nothing Then
                ValiditaPreferenze(i + 1) = ValiditaPreferenze(i)
            End If
        Next i

        cmd.CommandText = "SELECT * FROM DatiVariabili" & _
                          " WHERE CodiceDipendente = ?" & _
                          " AND CodiceVariabile = '" & cpt.CodiceAnagraficoStrutturaNotti & "'" & _
                          " AND Validita <= ?" & _
                          " ORDER BY Validita"
        cmd.Parameters.AddWithValue("@CodiceDipendente", Dipendente)
        cmd.Parameters.AddWithValue("@Validita", DataMax)

        cmd.Connection = cn
        myPOSTreader = cmd.ExecuteReader()
        Do While myPOSTreader.Read
            If Format(DataDb(myPOSTreader.Item("Validita")), "yyyymmdd") > Format(DataMin, "yyyymmdd") Then
                Giorni = DateDiff("d", DataSviluppo, DataDb(myPOSTreader.Item("Validita")))
                NottiSiNo(Giorni) = StringaDb(myPOSTreader.Item("ContenutoTesto"))
            Else
                NottiSiNo(g_min) = StringaDb(myPOSTreader.Item("ContenutoTesto"))
            End If
        Loop
        myPOSTreader.Close()
        cmd.Parameters.Clear()

        For i = 0 To 40
            If NottiSiNo(i + 1) = "" Then
                NottiSiNo(i + 1) = NottiSiNo(i)
            End If
        Next i

        Dim WCodice = ""
        Dim WSiNo = ""
        For i = 0 To 41
            If WCodice = NottiSiNo(i) Then
                NottiSiNo(i) = WSiNo

            Else

                WCodice = NottiSiNo(i)
                WSiNo = ""
                cmd.CommandText = "SELECT TOP 1 * FROM StrutturaNotti" & _
                                  " WHERE Codice = ?" & _
                                  " AND Validita <= ?" & _
                                  " ORDER BY Validita DESC, Id DESC"
                cmd.Parameters.AddWithValue("@Codice", WCodice)
                cmd.Parameters.AddWithValue("@Validita", DataMin)

                cmd.Connection = cn
                myPOSTreader = cmd.ExecuteReader()
                If myPOSTreader.Read Then
                    WSiNo = "No"
                    For g = 0 To 6
                        For x = 1 To 5
                            If StringaDb(myPOSTreader.Item("Giorno" & g + 1 & "Orario" & x)) <> "" Then
                                WSiNo = "Si"
                                Exit For
                            End If
                        Next x
                        If WSiNo = "Si" Then Exit For
                    Next g
                End If
                myPOSTreader.Close()
                cmd.Parameters.Clear()

                NottiSiNo(i) = WSiNo
            End If
        Next i

        If Sw_Profilo = "S" Then
            For i = g_max To g_min Step -1
                If CodiceTurno(i) <> "" Then
                    Oggi = DateAdd("d", i, DataMin)
                    If CodiceTurno(i) = "VUOTO" Then
                        For j = i To g_max
                            If CodiceTurnoTesta(j) = "" Then
                                CodiceStrutturaTesta(j) = ""
                                PrimoElementoTesta(j) = 0
                                CodiceTurnoTesta(j) = "VUOTO"
                                DataCodiceTurnoTesta(j) = DateSerial(1899, 12, 30)
                                OrarioGiornoFestivo(j) = ""
                                OrarioGiornoFestivoInfrasettimanale(j) = ""
                            Else
                                Exit For
                            End If
                        Next j
                    Else
                        cmd.CommandText = "SELECT * FROM ProfiliOrariTesta" & _
                                          " WHERE Codice = ?" & _
                                          " AND Validita <= ?" & _
                                          " ORDER BY Validita DESC"
                        cmd.Parameters.AddWithValue("@Codice", CodiceTurno(i))
                        cmd.Parameters.AddWithValue("@Validita", DataMax)

                        cmd.Connection = cn
                        myPOSTreader = cmd.ExecuteReader()
                        Do While myPOSTreader.Read
                            If Format(DataDb(myPOSTreader.Item("Validita")), "yyyyMMdd") > Format(Oggi, "yyyyMMdd") Then
                                gg = DateDiff("d", DataSviluppo, DataDb(myPOSTreader.Item("Validita")))
                                For j = gg To g_max
                                    If CodiceTurnoTesta(j) = "" Then
                                        CodiceStrutturaTesta(j) = StringaDb(myPOSTreader.Item("CodiceStruttura"))
                                        PrimoElementoTesta(j) = NumeroDb(myPOSTreader.Item("PrimoElemento"))
                                        CodiceTurnoTesta(j) = StringaDb(myPOSTreader.Item("Codice"))
                                        DataCodiceTurnoTesta(j) = DataDb(myPOSTreader.Item("Validita"))
                                        OrarioGiornoFestivo(j) = StringaDb(myPOSTreader.Item("OrarioGiornoFestivo"))
                                    Else
                                        Exit For
                                    End If
                                Next j
                            Else
                                For j = i To g_max
                                    If CodiceTurnoTesta(j) = "" Then
                                        CodiceStrutturaTesta(j) = StringaDb(myPOSTreader.Item("CodiceStruttura"))
                                        PrimoElementoTesta(j) = NumeroDb(myPOSTreader.Item("PrimoElemento"))
                                        CodiceTurnoTesta(j) = StringaDb(myPOSTreader.Item("Codice"))
                                        DataCodiceTurnoTesta(j) = DataDb(myPOSTreader.Item("Validita"))
                                        OrarioGiornoFestivo(j) = StringaDb(myPOSTreader.Item("OrarioGiornoFestivo"))
                                    Else
                                        Exit For
                                    End If
                                Next j
                                Exit Do
                            End If
                        Loop
                        myPOSTreader.Close()
                        cmd.Parameters.Clear()
                    End If
                End If
            Next i
        End If

        Dim co As New Cls_Orari
        If co.Conta(ConnectionString) = 0 Then
            SviluppoTurniOrdiniServizio = Dipendente & " (E) Nessun orario in tabella" & vbNewLine
            Exit Function
        End If

        Dim ci As New Cls_Intervalli
        Dim cof As New Cls_OrarioFlessibile
        Dim cg As New Cls_Giustificativi
        Dim cspor As New Cls_StrutturaProfiliOrariRiga
        Dim cpor As New Cls_ProfiliOrariRiga
        Dim cpfr As New Cls_ProfiliFerieRiga

        Dim W_CodiceFlessibilita As String = ""
        Dim W_DataCodiceFlessibilita As Date = DateSerial(1899, 12, 30)
        Dim W_CodiceTurnoTesta As String = ""
        Dim W_DataCodiceTurnoTesta As Date = DateSerial(1899, 12, 30)
        Dim W_CodiceProfiloFerieTesta As String = ""
        Dim W_DataCodiceProfiloFerieTesta As Date = DateSerial(1899, 12, 30)
        Dim W_CodiceFasceOrarie As String = ""
        Dim W_TipoFlessibilita As String = ""
        Dim MyData As Date
        Dim Ordinare As Boolean
        Dim W_Dalle As String
        Dim W_DalleAlto As String
        Dim W_DalleBasso As String
        Dim W_DalleObbligo As String
        Dim W_Alle As String
        Dim W_AlleAlto As String
        Dim W_AlleBasso As String
        Dim W_AlleObbligo As String
        Dim W_Pausa As String
        Dim W_Giustificativo As String
        Dim W_Familiare As Byte
        Dim W_Tipo As String
        Dim W_TipoServizio As String
        Dim W_GiornoSuccessivo As String
        Dim W_NelGruppo As String
        Dim W_DifferenzaDalle As Long
        Dim W_DifferenzaAlle As Long
        Dim Riga As Integer
        Dim Resto As Integer
        Dim CodTur As String
        Dim Intervallo As Integer
        Dim AnnoAttuale As Integer
        Dim Anni As Integer
        Dim IntervalloDal As Date
        Dim IntervalloAl As Date

        Dim myPOSTreader_po As OleDbDataReader
        Dim cmd_po As New OleDbCommand()

        Dim myPOSTreader_pf As OleDbDataReader
        Dim cmd_pf As New OleDbCommand()

        For i = g_min To g_max
            MyData = DateAdd("d", i, DataMin)
            If Assunzione(i) = "S" Then
                If CodiceFlessibilita(i) <> "" Then
                    If CodiceFlessibilita(i) <> W_CodiceFlessibilita Or DataCodiceFlessibilita(i) <> W_DataCodiceFlessibilita Then
                        W_CodiceFlessibilita = CodiceFlessibilita(i)
                        W_DataCodiceFlessibilita = DataCodiceFlessibilita(i)

                        cof.Leggi(ConnectionString, CodiceFlessibilita(i), DataCodiceFlessibilita(i))

                        If cof.Id = 0 Then
                            SviluppoTurniOrdiniServizio = Dipendente & " (E) Orario Flessibile non trovato" & vbNewLine
                            Exit Function
                        End If
                        If cof.GestioneTimbrature = "S" Then
                            W_TipoFlessibilita = "P"
                        Else
                            If cof.FlessibilitaIntervallo = "S" Then
                                W_TipoFlessibilita = "X"
                            Else
                                W_TipoFlessibilita = "F"
                            End If
                            W_InAlto(i) = DateDiff("n", TimeSerial(0, 0, 0), TimeSerial(Hour(cof.InAlto), Minute(cof.InAlto), Second(cof.InAlto))) * -1
                            W_InBasso(i) = DateDiff("n", TimeSerial(0, 0, 0), TimeSerial(Hour(cof.InBasso), Minute(cof.InBasso), Second(cof.InBasso)))
                        End If
                    Else
                        W_InAlto(i) = W_InAlto(i - 1)
                        W_InBasso(i) = W_InBasso(i - 1)
                    End If
                Else
                    W_TipoFlessibilita = ""
                End If
                Vettore_Orari.TipoFlessibilita(i) = W_TipoFlessibilita
                If Vettore_Orari.FasceOrarie(i) <> "" Then
                    If Vettore_Orari.FasceOrarie(i) <> W_CodiceFasceOrarie Then
                        W_CodiceFasceOrarie = Vettore_Orari.FasceOrarie(i)

                        cmd.CommandText = "SELECT * FROM FasceOrarie" & _
                                          " WHERE Codice = ?"
                        cmd.Parameters.AddWithValue("@Codice", Vettore_Orari.FasceOrarie(i))

                        cmd.Connection = cn
                        myPOSTreader = cmd.ExecuteReader()
                        If Not myPOSTreader.Read Then
                            SviluppoTurniOrdiniServizio = Dipendente & " (E) Codice Fasce Orarie non trovato" & vbNewLine
                            Exit Function
                        End If
                        For f = 1 To 10
                            If StringaDb(myPOSTreader.Item("Orario" & f)) <> "" Then
                                co.Leggi(ConnectionString, StringaDb(myPOSTreader.Item("Orario" & f)))
                                If co.Id > 0 Then
                                    W_OrarioDalle(f) = Format(co.Dalle, "HH.mm")
                                    W_OrarioDalleObbligo(f) = Format(co.DalleObbligo, "HH.mm")
                                    W_OrarioAlle(f) = Format(co.Alle, "HH.mm")
                                    W_OrarioAlleObbligo(f) = Format(co.AlleObbligo, "HH.mm")
                                    W_OrarioGiustificativo(f) = co.Giustificativo
                                    W_OrarioNelGruppo(f) = ""
                                End If
                            End If
                        Next f
                        myPOSTreader.Close()
                        cmd.Parameters.Clear()
                    End If
                    If i > 31 Then
                        Vettore_Orari.Dalle(0, i) = "00.00"
                        Vettore_Orari.DalleAlto(0, i) = "00.00"
                        Vettore_Orari.DalleBasso(0, i) = "00.00"
                        Vettore_Orari.DalleObbligo(0, i) = "00.00"
                        Vettore_Orari.Alle(0, i) = "00.00"
                        Vettore_Orari.AlleAlto(0, i) = "00.00"
                        Vettore_Orari.AlleBasso(0, i) = "00.00"
                        Vettore_Orari.AlleObbligo(0, i) = "00.00"
                        Vettore_Orari.Giustificativo(0, i) = cpt.GiustificativoRiposo
                        Vettore_Orari.NelGruppo(0, i) = ""
                        Vettore_Orari.Familiare(0, i) = 0
                        Vettore_Orari.Tipo(0, i) = cg.CampoGiustificativi(ConnectionString, Vettore_Orari.Giustificativo(0, i), "Tipo")
                    Else
                        If Vettore_Timbrature.Dalle(0, i) = "" Or Vettore_Timbrature.Alle(0, i) = "" Then
                            Vettore_Orari.Dalle(0, i) = "00.00"
                            Vettore_Orari.DalleAlto(0, i) = "00.00"
                            Vettore_Orari.DalleBasso(0, i) = "00.00"
                            Vettore_Orari.DalleObbligo(0, i) = "00.00"
                            Vettore_Orari.Alle(0, i) = "00.00"
                            Vettore_Orari.AlleAlto(0, i) = "00.00"
                            Vettore_Orari.AlleBasso(0, i) = "00.00"
                            Vettore_Orari.AlleObbligo(0, i) = "00.00"
                            Vettore_Orari.Giustificativo(0, i) = cpt.GiustificativoRiposo
                            Vettore_Orari.NelGruppo(0, i) = ""
                            Vettore_Orari.Familiare(0, i) = 0
                            Vettore_Orari.Tipo(0, i) = cg.CampoGiustificativi(ConnectionString, Vettore_Orari.Giustificativo(0, i), "Tipo")
                        Else
                            For ii = 1 To 10
                                If W_OrarioDalle(ii) = "" Then
                                    W_OrarioDifferenzaDalle(ii) = 9999999
                                    W_OrarioDifferenzaAlle(ii) = 9999999
                                Else
                                    W_OrarioDifferenzaDalle(ii) = Math.Abs(DateDiff("n", TimeSerial(Val(Mid(Vettore_Timbrature.Dalle(0, i), 1, 2)), Val(Mid(Vettore_Timbrature.Dalle(0, i), 3, 2)), 0), TimeSerial(Val(Mid(W_OrarioDalle(ii), 1, 2)), Val(Mid(W_OrarioDalle(ii), 3, 2)), 0)))
                                    W_OrarioDifferenzaAlle(ii) = Math.Abs(DateDiff("n", TimeSerial(Val(Mid(Vettore_Timbrature.Alle(0, i), 1, 2)), Val(Mid(Vettore_Timbrature.Alle(0, i), 3, 2)), 0), TimeSerial(Val(Mid(W_OrarioAlle(ii), 1, 2)), Val(Mid(W_OrarioAlle(ii), 3, 2)), 0)))
                                End If
                            Next ii
                            Ordinare = True
                            Do Until Ordinare = False
                                Ordinare = False
                                For ii = 2 To 10
                                    If W_OrarioDifferenzaDalle(ii) < W_OrarioDifferenzaDalle(ii - 1) Or (W_OrarioDifferenzaDalle(ii) = W_OrarioDifferenzaDalle(ii - 1) And W_OrarioDifferenzaAlle(ii) < W_OrarioDifferenzaAlle(ii - 1)) Then
                                        Ordinare = True
                                        W_Dalle = W_OrarioDalle(ii)
                                        W_DalleObbligo = W_OrarioDalleObbligo(ii)
                                        W_Alle = W_OrarioAlle(ii)
                                        W_AlleObbligo = W_OrarioAlleObbligo(ii)
                                        W_Giustificativo = W_OrarioGiustificativo(ii)
                                        W_NelGruppo = W_OrarioNelGruppo(ii)
                                        W_DifferenzaDalle = W_OrarioDifferenzaDalle(ii)
                                        W_DifferenzaAlle = W_OrarioDifferenzaAlle(ii)
                                        W_OrarioDalle(ii) = W_OrarioDalle(ii - 1)
                                        W_OrarioDalleObbligo(ii) = W_OrarioDalleObbligo(ii - 1)
                                        W_OrarioAlle(ii) = W_OrarioAlle(ii - 1)
                                        W_OrarioAlleObbligo(ii) = W_OrarioAlleObbligo(ii - 1)
                                        W_OrarioGiustificativo(ii) = W_OrarioGiustificativo(ii - 1)
                                        W_OrarioNelGruppo(ii) = W_OrarioNelGruppo(ii - 1)
                                        W_OrarioDifferenzaDalle(ii) = W_OrarioDifferenzaDalle(ii - 1)
                                        W_OrarioDifferenzaAlle(ii) = W_OrarioDifferenzaAlle(ii - 1)
                                        W_OrarioDalle(ii - 1) = W_Dalle
                                        W_OrarioDalleObbligo(ii - 1) = W_DalleObbligo
                                        W_OrarioAlle(ii - 1) = W_Alle
                                        W_OrarioAlleObbligo(ii - 1) = W_AlleObbligo
                                        W_OrarioGiustificativo(ii - 1) = W_Giustificativo
                                        W_OrarioNelGruppo(ii - 1) = W_NelGruppo
                                        W_OrarioDifferenzaDalle(ii - 1) = W_DifferenzaDalle
                                        W_OrarioDifferenzaAlle(ii - 1) = W_DifferenzaAlle
                                    End If
                                Next ii
                            Loop
                            Vettore_Orari.Dalle(0, i) = W_OrarioDalle(1)
                            Vettore_Orari.DalleAlto(0, i) = W_OrarioDalle(1)
                            Vettore_Orari.DalleBasso(0, i) = W_OrarioDalle(1)
                            Vettore_Orari.DalleObbligo(0, i) = W_OrarioDalleObbligo(1)
                            Vettore_Orari.Alle(0, i) = W_OrarioAlle(1)
                            Vettore_Orari.AlleAlto(0, i) = W_OrarioAlle(1)
                            Vettore_Orari.AlleBasso(0, i) = W_OrarioAlle(1)
                            Vettore_Orari.AlleObbligo(0, i) = W_OrarioAlleObbligo(1)
                            Vettore_Orari.Giustificativo(0, i) = W_OrarioGiustificativo(1)
                            Vettore_Orari.NelGruppo(0, i) = W_OrarioNelGruppo(1)
                            If Vettore_Orari.Giustificativo(0, i) = "" Then Vettore_Orari.Giustificativo(0, i) = cpt.GiustificativoOrarioLavoro
                            Vettore_Orari.Familiare(0, i) = 0
                            Vettore_Orari.Tipo(0, i) = cg.CampoGiustificativi(ConnectionString, Vettore_Orari.Giustificativo(0, i), "Tipo")
                            If W_InAlto(i) <> 0 Then
                                Vettore_Orari.DalleAlto(0, i) = Format(DateAdd("n", W_InAlto(i), Vettore_Orari.DalleAlto(0, i)), "hh.nn")
                                Vettore_Orari.AlleAlto(0, i) = Format(DateAdd("n", W_InAlto(i), Vettore_Orari.AlleAlto(0, i)), "hh.nn")
                            End If
                            If W_InBasso(i) <> 0 Then
                                Vettore_Orari.DalleBasso(0, i) = Format(DateAdd("n", W_InBasso(i), Vettore_Orari.DalleBasso(0, i)), "hh.nn")
                                Vettore_Orari.AlleBasso(0, i) = Format(DateAdd("n", W_InBasso(i), Vettore_Orari.AlleBasso(0, i)), "hh.nn")
                            End If
                        End If
                    End If
                End If
            Else

                If Vettore_Orari.Variati(i) = "" Then
                    If CodiceTurnoTesta(i) <> "" Then
                        If CodiceTurnoTesta(i) <> W_CodiceTurnoTesta Or Format(DataCodiceTurnoTesta(i), "yyyyMMdd") <> Format(W_DataCodiceTurnoTesta, "yyyyMMdd") Then
                            If W_CodiceTurnoTesta <> "" Then
                                myPOSTreader_po.Close()
                                cmd_po.Parameters.Clear()
                            End If
                            W_CodiceTurnoTesta = CodiceTurnoTesta(i)
                            W_DataCodiceTurnoTesta = DataCodiceTurnoTesta(i) ' Format(DataCodiceTurnoTesta(i), "yyyyMMdd")

                            Riga = 1

                            If CodiceStrutturaTesta(i) <> "" Then
                                CodTur = CodiceStrutturaTesta(i)
                                Contatore = cspor.Conta(ConnectionString, CodiceStrutturaTesta(i))
                            Else
                                CodTur = CodiceTurnoTesta(i)
                                Contatore = cpor.Conta(ConnectionString, CodiceTurnoTesta(i), DataCodiceTurnoTesta(i))
                            End If

                            If Contatore = 0 Then
                                SviluppoTurniOrdiniServizio = SviluppoTurniOrdiniServizio & Dipendente & " (E) Profilo Orario '" & CodTur & "' senza righe" & vbNewLine
                                Exit Function
                            End If

                            If CodiceStrutturaTesta(i) <> "" Then
                                cmd_po.CommandText = "SELECT *, 'A' + RIGHT('00000'+ CONVERT(VARCHAR,Riga),5) As Selezione FROM StrutturaProfiliOrariRiga" & _
                                                     " WHERE Codice = ?" & _
                                                     " AND Riga >= ?" & _
                                                     " UNION " & _
                                                     "SELECT *, 'B' + RIGHT('00000'+ CONVERT(VARCHAR,Riga),5) As Selezione FROM StrutturaProfiliOrariRiga" & _
                                                     " WHERE Codice = ?" & _
                                                     " AND Riga < ?" & _
                                                     " ORDER BY Selezione "
                                cmd_po.Parameters.AddWithValue("@Codice", CodiceStrutturaTesta(i))
                                cmd_po.Parameters.AddWithValue("@Riga", PrimoElementoTesta(i))
                                cmd_po.Parameters.AddWithValue("@Codice", CodiceStrutturaTesta(i))
                                cmd_po.Parameters.AddWithValue("@Riga", PrimoElementoTesta(i))
                            Else
                                cmd_po.CommandText = "SELECT * FROM ProfiliOrariRiga" & _
                                                     " WHERE Codice = ?" & _
                                                     " AND Validita = ?" & _
                                                     " ORDER BY Riga "
                                cmd_po.Parameters.AddWithValue("@Codice", CodiceTurnoTesta(i))
                                cmd_po.Parameters.AddWithValue("@Validita", DataCodiceTurnoTesta(i))
                            End If

                            Oggi = DateAdd("d", i, DataSviluppo)
                            Giorni = DateDiff("d", DataCodiceTurnoTesta(i), Oggi)
                            Giorni = Giorni + 1
                            Resto = Giorni - (Int(Giorni / Contatore) * Contatore)
                            If Resto = 0 Then
                                Resto = Contatore
                            End If

                            cmd_po.Connection = cn
                            myPOSTreader_po = cmd_po.ExecuteReader()
                            Do Until Not myPOSTreader_po.Read Or Riga = Resto
                                Riga = Riga + 1
                            Loop

                        Else

                            Riga = Riga + 1
                            If Not myPOSTreader_po.Read Then
                                Riga = 1
                                myPOSTreader_po = cmd_po.ExecuteReader()
                                myPOSTreader_po.Read()
                            End If
                        End If

                        If OrarioGiornoFestivo(i) <> "" And (Weekday(MyData) = FestivitaSettimanale Or cxst.GiornoFestivo(MyData, CodiceSupergruppo) = True) Then
                            co.Leggi(ConnectionString, OrarioGiornoFestivo(i))

                            Call CaricaVettoreOrari(ConnectionString, 0, i, W_InAlto(i), W_InBasso(i), Vettore_Orari, co)

                        Else

                            If StringaDb(myPOSTreader_po.Item("PrimoOrario")) <> "" Then
                                co.Leggi(ConnectionString, StringaDb(myPOSTreader_po.Item("PrimoOrario")))

                                If co.OrarioGiornoFestivo <> "" And (Weekday(MyData) = FestivitaSettimanale Or cxst.GiornoFestivo(MyData, CodiceSupergruppo) = True) Then
                                    co.Leggi(ConnectionString, co.OrarioGiornoFestivo)
                                End If

                                Sw_Ok = True
                                Vettore_Orari.PrimoOrario(i) = StringaDb(myPOSTreader_po.Item("PrimoOrario"))

                                If Vettore_Orari.PrimoOrario(i) = W_OrarioNotte Or Vettore_Orari.PrimoOrario(i) = W_OrarioSmontoNotte Then
                                    If NottiSiNo(i) = "No" Then
                                        Vettore_Orari.PrimoOrario(i) = ""
                                        Sw_Ok = False
                                    End If
                                Else
                                    If ValiditaPreferenze(i) <> Nothing Then
                                        W_Tipo = "*" & co.CampoOrari(ConnectionString, Vettore_Orari.PrimoOrario(i), "TipoControllo")
                                        cmd.CommandText = "SELECT COUNT (*) FROM PreferenzeRiga" & _
                                                          " WHERE CodiceDipendente = " & Dipendente & _
                                                          " AND Validita = {ts '" & Format(ValiditaPreferenze(i), "yyyy-mm-dd") & " 00:00:00'} " & _
                                                          " AND (PrimoOrario = '" & W_Tipo & "' OR PrimoOrario = '" & Vettore_Orari.PrimoOrario(i) & _
                                                          "' OR SecondoOrario = '" & W_Tipo & "' OR SecondoOrario = '" & Vettore_Orari.PrimoOrario(i) & _
                                                          "' OR TerzoOrario = '" & W_Tipo & "' OR TerzoOrario = '" & Vettore_Orari.PrimoOrario(i) & _
                                                          "' OR QuartoOrario = '" & W_Tipo & "' OR QuartoOrario = '" & Vettore_Orari.PrimoOrario(i) & _
                                                          "' OR QuintoOrario = '" & W_Tipo & "' OR QuintoOrario = '" & Vettore_Orari.PrimoOrario(i) & "')"
                                        cmd.Parameters.AddWithValue("@Codice", CodiceTurno(i))
                                        cmd.Parameters.AddWithValue("@Validita", DataMax)

                                        cmd.Connection = cn
                                        myPOSTreader = cmd.ExecuteReader()
                                        If myPOSTreader.Read Then
                                            Contatore = myPOSTreader.Item(0)
                                            If Contatore = 0 Then Sw_Ok = False
                                        End If
                                        myPOSTreader.Close()
                                        cmd.Parameters.Clear()
                                    End If

                                    If Sw_Ok = True Then

                                        Call CaricaVettoreOrari(ConnectionString, 0, i, W_InAlto(i), W_InBasso(i), Vettore_Orari, co)
                                    End If

                                    If StringaDb(myPOSTreader_po.Item("SecondoOrario")) <> "" Then
                                        co.Leggi(ConnectionString, StringaDb(myPOSTreader_po.Item("SecondoOrario")))

                                        If co.OrarioGiornoFestivo <> "" And (Weekday(MyData) = FestivitaSettimanale Or cxst.GiornoFestivo(MyData, CodiceSupergruppo) = True) Then
                                            co.Leggi(ConnectionString, co.OrarioGiornoFestivo)
                                        End If

                                        Call CaricaVettoreOrari(ConnectionString, 1, i, W_InAlto(i), W_InBasso(i), Vettore_Orari, co)
                                        Vettore_Orari.SecondoOrario(i) = StringaDb(myPOSTreader_po.Item("SecondoOrario"))
                                    End If

                                    If StringaDb(myPOSTreader_po.Item("TerzoOrario")) <> "" Then
                                        co.Leggi(ConnectionString, StringaDb(myPOSTreader_po.Item("TerzoOrario")))

                                        If co.OrarioGiornoFestivo <> "" And (Weekday(MyData) = FestivitaSettimanale Or cxst.GiornoFestivo(MyData, CodiceSupergruppo) = True) Then
                                            co.Leggi(ConnectionString, co.OrarioGiornoFestivo)
                                        End If

                                        Call CaricaVettoreOrari(ConnectionString, 2, i, W_InAlto(i), W_InBasso(i), Vettore_Orari, co)
                                        Vettore_Orari.TerzoOrario(i) = StringaDb(myPOSTreader_po.Item("TerzoOrario"))
                                    End If

                                    If StringaDb(myPOSTreader_po.Item("QuartoOrario")) <> "" Then
                                        co.Leggi(ConnectionString, StringaDb(myPOSTreader_po.Item("QuartoOrario")))

                                        If co.OrarioGiornoFestivo <> "" And (Weekday(MyData) = FestivitaSettimanale Or cxst.GiornoFestivo(MyData, CodiceSupergruppo) = True) Then
                                            co.Leggi(ConnectionString, co.OrarioGiornoFestivo)
                                        End If

                                        Call CaricaVettoreOrari(ConnectionString, 3, i, W_InAlto(i), W_InBasso(i), Vettore_Orari, co)
                                        Vettore_Orari.QuartoOrario(i) = StringaDb(myPOSTreader_po.Item("QuartoOrario"))
                                    End If

                                    If StringaDb(myPOSTreader_po.Item("QuintoOrario")) <> "" Then
                                        co.Leggi(ConnectionString, StringaDb(myPOSTreader_po.Item("QuintoOrario")))

                                        If co.OrarioGiornoFestivo <> "" And (Weekday(MyData) = FestivitaSettimanale Or cxst.GiornoFestivo(MyData, CodiceSupergruppo) = True) Then
                                            co.Leggi(ConnectionString, co.OrarioGiornoFestivo)
                                        End If

                                        Call CaricaVettoreOrari(ConnectionString, 4, i, W_InAlto(i), W_InBasso(i), Vettore_Orari, co)
                                        Vettore_Orari.QuintoOrario(i) = StringaDb(myPOSTreader_po.Item("QuintoOrario"))
                                    End If
                                End If
                            End If

                            ' ************* da qui

                            If CodiceProfiloFerieTesta(i) <> "" Then
                                If CodiceProfiloFerieTesta(i) <> W_CodiceProfiloFerieTesta Or Format(DataCodiceProfiloFerieTesta(i), "yyyyMMdd") <> W_DataCodiceProfiloFerieTesta Then
                                    If W_CodiceProfiloFerieTesta <> "" Then
                                        myPOSTreader_pf.Close()
                                        cmd_pf.Parameters.Clear()
                                    End If

                                    W_CodiceProfiloFerieTesta = CodiceProfiloFerieTesta(i)
                                    W_DataCodiceProfiloFerieTesta = Format(DataCodiceProfiloFerieTesta(i), "yyyyMMdd")

                                    Intervallo = 1

                                    Contatore = cpfr.Conta(ConnectionString, CodiceProfiloFerieTesta(i), DataCodiceProfiloFerieTesta(i))
                                    If Contatore = 0 Then
                                        SviluppoTurniOrdiniServizio = SviluppoTurniOrdiniServizio & Dipendente & " (E) Profilo Ferie '" & CodiceProfiloFerieTesta(i) & "' senza righe" & vbNewLine
                                        Exit Function
                                    End If

                                    cmd_pf.CommandText = "SELECT * FROM ProfiliFerieRiga" & _
                                                         " WHERE Codice = ?" & _
                                                         " AND Validita = ?" & _
                                                         " ORDER BY Riga "
                                    cmd_pf.Parameters.AddWithValue("@Codice", CodiceProfiloFerieTesta(i))
                                    cmd_pf.Parameters.AddWithValue("@Validita", DataCodiceProfiloFerieTesta(i))

                                    AnnoAttuale = Year(DataSviluppo)
                                    Anni = AnnoAttuale - Year(DataCodiceProfiloFerieTesta(i)) + 1
                                    Resto = Anni - (Int(Anni / Contatore) * Contatore)
                                    If Resto = 0 Then
                                        Resto = Contatore
                                    End If
                                    cmd_pf.Connection = cn
                                    myPOSTreader_pf = cmd_pf.ExecuteReader()
                                    Do Until Not myPOSTreader_pf.Read Or Intervallo = Resto
                                        Intervallo = Intervallo + 1
                                    Loop
                                End If

                                If OrarioGiornoFestivo(i) <> "" And (Weekday(MyData) = FestivitaSettimanale Or cxst.GiornoFestivo(MyData, CodiceSupergruppo) = True) Then
                                    co.Leggi(ConnectionString, OrarioGiornoFestivo(i))

                                    Call CaricaVettoreOrari(ConnectionString, 0, i, W_InAlto(i), W_InBasso(i), Vettore_Orari, co)

                                Else

                                    If StringaDb(myPOSTreader_pf.Item("PrimoIntervallo")) <> "" Then
                                        ci.Leggi(ConnectionString, StringaDb(myPOSTreader_pf.Item("PrimoIntervallo")))

                                        IntervalloDal = DateSerial(Year(MyData), ci.DalMese, ci.DalGiorno)
                                        IntervalloAl = DateSerial(Year(MyData), ci.AlMese, ci.AlGiorno)

                                        If Format(IntervalloDal, "yyyyMMdd") <= Format(MyData, "yyyyMMdd") And Format(IntervalloAl, "yyyyMMdd") >= Format(MyData, "yyyyMMdd") Then

                                            co.Leggi(ConnectionString, cpt.CodiceOrarioFerie)

                                            If co.OrarioGiornoFestivo <> "" And (Weekday(MyData) = FestivitaSettimanale Or cxst.GiornoFestivo(MyData, CodiceSupergruppo) = True) Then
                                                co.Leggi(ConnectionString, co.OrarioGiornoFestivo)
                                            End If

                                            Call CaricaVettoreOrariFerie(ConnectionString, i, Vettore_Orari, co)

                                        End If
                                    End If

                                    If StringaDb(myPOSTreader_pf.Item("SecondoIntervallo")) <> "" Then
                                        ci.Leggi(ConnectionString, StringaDb(myPOSTreader_pf.Item("SecondoIntervallo")))

                                        IntervalloDal = DateSerial(Year(MyData), ci.DalMese, ci.DalGiorno)
                                        IntervalloAl = DateSerial(Year(MyData), ci.AlMese, ci.AlGiorno)

                                        If Format(IntervalloDal, "yyyyMMdd") <= Format(MyData, "yyyyMMdd") And Format(IntervalloAl, "yyyyMMdd") >= Format(MyData, "yyyyMMdd") Then

                                            co.Leggi(ConnectionString, cpt.CodiceOrarioFerie)

                                            If co.OrarioGiornoFestivo <> "" And (Weekday(MyData) = FestivitaSettimanale Or cxst.GiornoFestivo(MyData, CodiceSupergruppo) = True) Then
                                                co.Leggi(ConnectionString, co.OrarioGiornoFestivo)
                                            End If

                                            Call CaricaVettoreOrariFerie(ConnectionString, i, Vettore_Orari, co)

                                        End If
                                    End If

                                    If StringaDb(myPOSTreader_pf.Item("TerzoIntervallo")) <> "" Then
                                        ci.Leggi(ConnectionString, StringaDb(myPOSTreader_pf.Item("TerzoIntervallo")))

                                        IntervalloDal = DateSerial(Year(MyData), ci.DalMese, ci.DalGiorno)
                                        IntervalloAl = DateSerial(Year(MyData), ci.AlMese, ci.AlGiorno)

                                        If Format(IntervalloDal, "yyyyMMdd") <= Format(MyData, "yyyyMMdd") And Format(IntervalloAl, "yyyyMMdd") >= Format(MyData, "yyyyMMdd") Then

                                            co.Leggi(ConnectionString, cpt.CodiceOrarioFerie)

                                            If co.OrarioGiornoFestivo <> "" And (Weekday(MyData) = FestivitaSettimanale Or cxst.GiornoFestivo(MyData, CodiceSuperGruppo) = True) Then
                                                co.Leggi(ConnectionString, co.OrarioGiornoFestivo)
                                            End If

                                            Call CaricaVettoreOrariFerie(ConnectionString, i, Vettore_Orari, co)

                                        End If
                                    End If

                                    If StringaDb(myPOSTreader_pf.Item("QuartoIntervallo")) <> "" Then
                                        ci.Leggi(ConnectionString, StringaDb(myPOSTreader_pf.Item("QuartoIntervallo")))

                                        IntervalloDal = DateSerial(Year(MyData), ci.DalMese, ci.DalGiorno)
                                        IntervalloAl = DateSerial(Year(MyData), ci.AlMese, ci.AlGiorno)

                                        If Format(IntervalloDal, "yyyyMMdd") <= Format(MyData, "yyyyMMdd") And Format(IntervalloAl, "yyyyMMdd") >= Format(MyData, "yyyyMMdd") Then

                                            co.Leggi(ConnectionString, cpt.CodiceOrarioFerie)

                                            If co.OrarioGiornoFestivo <> "" And (Weekday(MyData) = FestivitaSettimanale Or cxst.GiornoFestivo(MyData, CodiceSuperGruppo) = True) Then
                                                co.Leggi(ConnectionString, co.OrarioGiornoFestivo)
                                            End If

                                            Call CaricaVettoreOrariFerie(ConnectionString, i, Vettore_Orari, co)

                                        End If
                                    End If

                                    If StringaDb(myPOSTreader_pf.Item("QuintoIntervallo")) <> "" Then
                                        ci.Leggi(ConnectionString, StringaDb(myPOSTreader_pf.Item("QuintoIntervallo")))

                                        IntervalloDal = DateSerial(Year(MyData), ci.DalMese, ci.DalGiorno)
                                        IntervalloAl = DateSerial(Year(MyData), ci.AlMese, ci.AlGiorno)

                                        If Format(IntervalloDal, "yyyyMMdd") <= Format(MyData, "yyyyMMdd") And Format(IntervalloAl, "yyyyMMdd") >= Format(MyData, "yyyyMMdd") Then

                                            co.Leggi(ConnectionString, cpt.CodiceOrarioFerie)

                                            If co.OrarioGiornoFestivo <> "" And (Weekday(MyData) = FestivitaSettimanale Or cxst.GiornoFestivo(MyData, CodiceSuperGruppo) = True) Then
                                                co.Leggi(ConnectionString, co.OrarioGiornoFestivo)
                                            End If

                                            Call CaricaVettoreOrariFerie(ConnectionString, i, Vettore_Orari, co)

                                        End If
                                    End If
                                End If
                            End If
                        End If
                        ' ************* a qui

                    Else

                        If W_CodiceTurnoTesta <> "" Then
                            W_CodiceTurnoTesta = ""
                        End If

                        If Vettore_Orari.PrimoOrario(i) <> "" Then
                            co.Leggi(ConnectionString, Vettore_Orari.PrimoOrario(i))

                            If co.OrarioGiornoFestivo <> "" And (Weekday(MyData) = FestivitaSettimanale Or cxst.GiornoFestivo(MyData, CodiceSuperGruppo) = True) Then
                                co.Leggi(ConnectionString, co.OrarioGiornoFestivo)
                            End If

                            Call CaricaVettoreOrari(ConnectionString, 0, i, W_InAlto(i), W_InBasso(i), Vettore_Orari, co)

                        End If
                        If Vettore_Orari.SecondoOrario(i) <> "" Then
                            co.Leggi(ConnectionString, Vettore_Orari.SecondoOrario(i))

                            If co.OrarioGiornoFestivo <> "" And (Weekday(MyData) = FestivitaSettimanale Or cxst.GiornoFestivo(MyData, CodiceSuperGruppo) = True) Then
                                co.Leggi(ConnectionString, co.OrarioGiornoFestivo)
                            End If

                            Call CaricaVettoreOrari(ConnectionString, 1, i, W_InAlto(i), W_InBasso(i), Vettore_Orari, co)

                        End If
                        If Vettore_Orari.TerzoOrario(i) <> "" Then
                            co.Leggi(ConnectionString, Vettore_Orari.TerzoOrario(i))

                            If co.OrarioGiornoFestivo <> "" And (Weekday(MyData) = FestivitaSettimanale Or cxst.GiornoFestivo(MyData, CodiceSuperGruppo) = True) Then
                                co.Leggi(ConnectionString, co.OrarioGiornoFestivo)
                            End If

                            Call CaricaVettoreOrari(ConnectionString, 2, i, W_InAlto(i), W_InBasso(i), Vettore_Orari, co)

                        End If
                        If Vettore_Orari.QuartoOrario(i) <> "" Then
                            co.Leggi(ConnectionString, Vettore_Orari.QuartoOrario(i))

                            If co.OrarioGiornoFestivo <> "" And (Weekday(MyData) = FestivitaSettimanale Or cxst.GiornoFestivo(MyData, CodiceSuperGruppo) = True) Then
                                co.Leggi(ConnectionString, co.OrarioGiornoFestivo)
                            End If

                            Call CaricaVettoreOrari(ConnectionString, 3, i, W_InAlto(i), W_InBasso(i), Vettore_Orari, co)

                        End If
                        If Vettore_Orari.QuintoOrario(i) <> "" Then
                            co.Leggi(ConnectionString, Vettore_Orari.QuintoOrario(i))

                            If co.OrarioGiornoFestivo <> "" And (Weekday(MyData) = FestivitaSettimanale Or cxst.GiornoFestivo(MyData, CodiceSuperGruppo) = True) Then
                                co.Leggi(ConnectionString, co.OrarioGiornoFestivo)
                            End If

                            Call CaricaVettoreOrari(ConnectionString, 4, i, W_InAlto(i), W_InBasso(i), Vettore_Orari, co)

                        End If
                    End If
                End If
            End If
        Next i

        If W_CodiceTurnoTesta <> "" Then
            myPOSTreader_po.Close()
            cmd_po.Parameters.Clear()
        End If

        Call Carica_Vettore_Orari_RichiesteAssenza(ConnectionString, CodiceSuperGruppo, Dipendente, DataMin, DataMax, DataSviluppo, Vettore_Orari)

        Call Carica_Vettore_Orari_RichiesteStraordinario(ConnectionString, Dipendente, DataMin, DataMax, DataSviluppo, Vettore_Orari)

        Dim W_Data As Date = DateSerial(1899, 12, 30)

        cmd.CommandText = " SELECT * " & _
                              " FROM ProfiliOrariModificati " & _
                              " WHERE CodiceDipendente = ?" & _
                              " AND Data >= ?" & _
                              " AND Data <= ?" & _
                              " ORDER BY Data, Dalle"
        cmd.Parameters.AddWithValue("@CodiceDipendente", Dipendente)
        cmd.Parameters.AddWithValue("@Data", DataMin)
        cmd.Parameters.AddWithValue("@Data", DataMax)

        cmd.Connection = cn
        myPOSTreader = cmd.ExecuteReader()
        Do While myPOSTreader.Read
            If DataDb(myPOSTreader.Item("Data")) <> W_Data Then
                W_Data = DataDb(myPOSTreader.Item("Data"))
                g = DateDiff("d", DataSviluppo, W_Data)
                For i = 0 To 9
                    Vettore_Orari.Dalle(i, g) = "00.00"
                    Vettore_Orari.Alle(i, g) = "00.00"
                    Vettore_Orari.DalleAlto(i, g) = "00.00"
                    Vettore_Orari.AlleAlto(i, g) = "00.00"
                    Vettore_Orari.DalleBasso(i, g) = "00.00"
                    Vettore_Orari.AlleBasso(i, g) = "00.00"
                    Vettore_Orari.DalleObbligo(i, g) = "00.00"
                    Vettore_Orari.AlleObbligo(i, g) = "00.00"
                    '        Vettore_Orari.DCT(i, g) = ""
                    Vettore_Orari.Pausa(i, g) = "00.00"
                    Vettore_Orari.Giustificativo(i, g) = ""
                    Vettore_Orari.NelGruppo(i, g) = ""
                    Vettore_Orari.Familiare(i, g) = 0
                    Vettore_Orari.Tipo(i, g) = ""
                    Vettore_Orari.TipoServizio(i, g) = ""
                    Vettore_Orari.GiornoSuccessivo(i, g) = ""
                    If i < 5 Then
                        Vettore_Orari.NelGruppo(i, g) = ""
                    End If
                Next i
                i = 0
                Vettore_Orari.Modificati(g) = "S"
            Else
                If i < 9 Then i = i + 1
            End If
            Vettore_Orari.Dalle(i, g) = Format(DataDb(myPOSTreader.Item("Dalle")), "HH.mm")
            Vettore_Orari.DalleAlto(i, g) = Format(DataDb(myPOSTreader.Item("Dalle")), "HH.mm")
            Vettore_Orari.DalleBasso(i, g) = Format(DataDb(myPOSTreader.Item("Dalle")), "HH.mm")
            '    Vettore_Orari.DalleObbligo(i, g) = Format(MoveFROMDbWC(MyRs, "DalleObbligo", Cst_Orario), "HH.mm")
            '    Vettore_Orari.DCT(i, g) = ""
            Vettore_Orari.Alle(i, g) = Format(DataDb(myPOSTreader.Item("Alle")), "HH.mm")
            Vettore_Orari.AlleAlto(i, g) = Format(DataDb(myPOSTreader.Item("Alle")), "HH.mm")
            Vettore_Orari.AlleBasso(i, g) = Format(DataDb(myPOSTreader.Item("Alle")), "HH.mm")
            '    Vettore_Orari.AlleObbligo(i, g) = Format(MoveFROMDbWC(MyRs, "AlleObbligo", Cst_Orario), "HH.mm")
            Vettore_Orari.Pausa(i, g) = Format(DataDb(myPOSTreader.Item("Pausa")), "HH.mm")
            Vettore_Orari.Giustificativo(i, g) = StringaDb(myPOSTreader.Item("Giustificativo"))
            If Vettore_Orari.Giustificativo(i, g) = "" Then Vettore_Orari.Giustificativo(i, g) = cpt.GiustificativoOrarioLavoro
            Vettore_Orari.NelGruppo(i, g) = StringaDb(myPOSTreader.Item("NelGruppo"))
            Vettore_Orari.Familiare(i, g) = NumeroDb(myPOSTreader.Item("CodiceFamiliare"))
            Vettore_Orari.Tipo(i, g) = cg.CampoGiustificativi(ConnectionString, Vettore_Orari.Giustificativo(i, g), "Tipo")
            Vettore_Orari.TipoServizio(i, g) = ""
            Vettore_Orari.GiornoSuccessivo(i, g) = ""
            If i < 5 Then
                Vettore_Orari.NelGruppo(i, g) = StringaDb(myPOSTreader.Item("NelGruppo"))
            End If
        Loop
        myPOSTreader.Close()
        cmd.Parameters.Clear()

        Dim cmd_1 As New OleDbCommand()
        Dim myPOSTreader_1 As OleDbDataReader

        cmd.CommandText = "SELECT LegameDipendenteOrdiniServizio.Data, LegameDipendenteOrdiniServizio.Gruppo, LegameDipendenteOrdiniServizio.GruppoComandante, LegameDipendenteOrdiniServizio.TipoComandante, LegameDipendenteOrdiniServizio.Riga, LegameDipendenteOrdiniServizio.CodiceFamiliare,  OrdiniServizioRiga.Servizio" & _
                          " FROM LegameDipendenteOrdiniServizio INNER JOIN OrdiniServizioRiga ON (LegameDipendenteOrdiniServizio.Riga = OrdiniServizioRiga.Riga)" & _
                          " AND (LegameDipendenteOrdiniServizio.Gruppo = OrdiniServizioRiga.Gruppo)" & _
                          " AND (LegameDipendenteOrdiniServizio.Data = OrdiniServizioRiga.Data)" & _
                          " WHERE (LegameDipendenteOrdiniServizio.GruppoComandante Is Null OR LegameDipendenteOrdiniServizio.GruppoComandante = '')" & _
                          " AND (LegameDipendenteOrdiniServizio.TipoComandante Is Null OR LegameDipendenteOrdiniServizio.TipoComandante = '')" & _
                          " AND LegameDipendenteOrdiniServizio.Data >= ?" & _
                          " AND LegameDipendenteOrdiniServizio.Data <= ?" & _
                          " AND CodiceDipendente = ?"
        cmd.CommandText = cmd.CommandText & " UNION" & _
                          " SELECT LegameDipendenteOrdiniServizio.Data, LegameDipendenteOrdiniServizio.Gruppo, LegameDipendenteOrdiniServizio.GruppoComandante, LegameDipendenteOrdiniServizio.TipoComandante, LegameDipendenteOrdiniServizio.Riga, LegameDipendenteOrdiniServizio.CodiceFamiliare,  OrdiniServizioRigaComandanti.Servizio" & _
                          " FROM LegameDipendenteOrdiniServizio INNER JOIN OrdiniServizioRigaComandanti ON (LegameDipendenteOrdiniServizio.Riga = OrdiniServizioRigaComandanti.Riga)" & _
                          " AND (LegameDipendenteOrdiniServizio.GruppoComandante = OrdiniServizioRigaComandanti.Gruppo)" & _
                          " AND (LegameDipendenteOrdiniServizio.TipoComandante = OrdiniServizioRigaComandanti.Tipo)" & _
                          " AND (LegameDipendenteOrdiniServizio.Data = OrdiniServizioRigaComandanti.Data)" & _
                          " WHERE Not LegameDipendenteOrdiniServizio.GruppoComandante Is Null" & _
                          " AND LegameDipendenteOrdiniServizio.GruppoComandante <> ''" & _
                          " AND Not LegameDipendenteOrdiniServizio.TipoComandante Is Null" & _
                          " AND LegameDipendenteOrdiniServizio.TipoComandante <> ''" & _
                          " AND LegameDipendenteOrdiniServizio.Data >= ?" & _
                          " AND LegameDipendenteOrdiniServizio.Data <= ?" & _
                          " AND CodiceDipendente = ?" & _
                          " ORDER BY LegameDipendenteOrdiniServizio.Data"
        cmd.Parameters.AddWithValue("@Data", DataMin)
        cmd.Parameters.AddWithValue("@Data", DataMax)
        cmd.Parameters.AddWithValue("@CodiceDipendente", Dipendente)
        cmd.Parameters.AddWithValue("@Data", DataMin)
        cmd.Parameters.AddWithValue("@Data", DataMax)
        cmd.Parameters.AddWithValue("@CodiceDipendente", Dipendente)

        cmd.Connection = cn
        myPOSTreader = cmd.ExecuteReader()
        Do While myPOSTreader.Read
            g = DateDiff("d", DataSviluppo, DataDb(myPOSTreader.Item("Data")))
            If Vettore_Orari.Azzerati(g) = "" Then
                For i = 0 To 9
                    Vettore_Orari.Dalle(i, g) = "00.00"
                    Vettore_Orari.Alle(i, g) = "00.00"
                    Vettore_Orari.DalleAlto(i, g) = "00.00"
                    Vettore_Orari.AlleAlto(i, g) = "00.00"
                    Vettore_Orari.DalleBasso(i, g) = "00.00"
                    Vettore_Orari.AlleBasso(i, g) = "00.00"
                    Vettore_Orari.DalleObbligo(i, g) = "00.00"
                    Vettore_Orari.AlleObbligo(i, g) = "00.00"
                    '        Vettore_Orari.DCT(i, g) = ""
                    Vettore_Orari.Pausa(i, g) = "00.00"
                    Vettore_Orari.Giustificativo(i, g) = ""
                    Vettore_Orari.NelGruppo(i, g) = ""
                    Vettore_Orari.Familiare(i, g) = 0
                    Vettore_Orari.Tipo(i, g) = ""
                    Vettore_Orari.TipoServizio(i, g) = ""
                    Vettore_Orari.GiornoSuccessivo(i, g) = ""
                    If i < 5 Then
                        Vettore_Orari.NelGruppo(i, g) = ""
                    End If
                Next i
                Vettore_Orari.Richiesti(g) = ""
                Vettore_Orari.Variati(g) = ""
                Vettore_Orari.Modificati(g) = ""
            End If
            If StringaDb(myPOSTreader.Item("GruppoComandante")) = "" Then
                cmd_1.CommandText = "SELECT * FROM OrdiniServizioRigaOrario" & _
                                        " WHERE Data = ?" & _
                                        " AND Gruppo = ?" & _
                                        " AND Riga = ?"
                cmd_1.Parameters.AddWithValue("@Data", DataDb(myPOSTreader.Item("Data")))
                cmd_1.Parameters.AddWithValue("@Gruppo", StringaDb(myPOSTreader.Item("Gruppo")))
                cmd_1.Parameters.AddWithValue("@Riga", NumeroDb(myPOSTreader.Item("Riga")))

                cmd_1.Connection = cn
                myPOSTreader_1 = cmd_1.ExecuteReader()
                If Not myPOSTreader_1.Read Then
                    myPOSTreader_1.Close()
                    cmd_1.Parameters.Clear()
                    cmd_1.CommandText = "SELECT * FROM OrdiniServizioRigaProfilo" & _
                                        " WHERE Data = ?" & _
                                        " AND Gruppo = ?" & _
                                        " AND Riga = ?"
                    cmd_1.Parameters.AddWithValue("@Data", DataDb(myPOSTreader.Item("Data")))
                    cmd_1.Parameters.AddWithValue("@Gruppo", StringaDb(myPOSTreader.Item("Gruppo")))
                    cmd_1.Parameters.AddWithValue("@Riga", NumeroDb(myPOSTreader.Item("Riga")))

                    cmd_1.Connection = cn
                    myPOSTreader_1 = cmd_1.ExecuteReader()
                    If Not myPOSTreader_1.Read Then
                        If Vettore_Orari.Variati(g) <> "P" And Vettore_Orari.Variati(g) <> "S" And Vettore_Orari.Modificati(g) <> "S" Then
                            Vettore_Orari.Azzerati(g) = "S"
                            For i = 0 To 9
                                If Vettore_Orari.Giustificativo(i, g) = "" Then Exit For
                            Next i
                            Vettore_Orari.Giustificativo(i, g) = StringaDb(myPOSTreader.Item("Servizio"))
                            Vettore_Orari.NelGruppo(i, g) = StringaDb(myPOSTreader.Item("NelGruppo"))
                            Vettore_Orari.Familiare(i, g) = NumeroDb(myPOSTreader.Item("CodiceFamiliare"))
                            Vettore_Orari.Tipo(i, g) = cg.CampoGiustificativi(ConnectionString, Vettore_Orari.Giustificativo(i, g), "Tipo")
                            Vettore_Orari.TipoServizio(i, g) = StringaDb(myPOSTreader.Item("TipoComandante"))
                        End If
                    Else
                        Do
                            Call CaricaVettoreOrariOrdiniServizio(g, StringaDb(myPOSTreader_1.Item("GiornoSuccessivo")), W_InAlto, W_InBasso, Format(DataDb(myPOSTreader_1.Item("Dalle")), "HH.mm"), Format(DataDb(myPOSTreader_1.Item("Alle")), "HH.mm"), Format(DataDb(myPOSTreader_1.Item("Pausa")), "HH.mm"), StringaDb(myPOSTreader.Item("TipoComandante")), StringaDb(myPOSTreader.Item("Servizio")), cg.CampoGiustificativi(ConnectionString, StringaDb(myPOSTreader.Item("Servizio")), "TIPO"), NumeroDb(myPOSTreader.Item("CodiceFamiliare")), Vettore_Orari, co)
                        Loop While myPOSTreader_1.Read
                    End If

                Else

                    If StringaDb(myPOSTreader_1.Item("PrimoOrario")) <> "" Then
                        Vettore_Orari.PrimoOrario(g) = StringaDb(myPOSTreader_1.Item("PrimoOrario"))
                        co.Leggi(ConnectionString, StringaDb(myPOSTreader_1.Item("PrimoOrario")))
                        Call CaricaVettoreOrariOrdiniServizio(g, StringaDb(myPOSTreader_1.Item("PrimoGiornoSuccessivo")), W_InAlto, W_InBasso, "", "", "", StringaDb(myPOSTreader.Item("TipoComandante")), StringaDb(myPOSTreader.Item("Servizio")), cg.CampoGiustificativi(ConnectionString, StringaDb(myPOSTreader.Item("Servizio")), "TIPO"), NumeroDb(myPOSTreader.Item("CodiceFamiliare")), Vettore_Orari, co)
                    End If
                    If StringaDb(myPOSTreader_1.Item("SecondoOrario")) <> "" Then
                        Vettore_Orari.SecondoOrario(g) = StringaDb(myPOSTreader_1.Item("SecondoOrario"))
                        co.Leggi(ConnectionString, StringaDb(myPOSTreader_1.Item("SecondoOrario")))
                        Call CaricaVettoreOrariOrdiniServizio(g, StringaDb(myPOSTreader_1.Item("SecondoGiornoSuccessivo")), W_InAlto, W_InBasso, "", "", "", StringaDb(myPOSTreader.Item("TipoComandante")), StringaDb(myPOSTreader.Item("Servizio")), cg.CampoGiustificativi(ConnectionString, StringaDb(myPOSTreader.Item("Servizio")), "TIPO"), NumeroDb(myPOSTreader.Item("CodiceFamiliare")), Vettore_Orari, co)
                    End If
                    If StringaDb(myPOSTreader_1.Item("TerzoOrario")) <> "" Then
                        Vettore_Orari.TerzoOrario(g) = StringaDb(myPOSTreader_1.Item("TerzoOrario"))
                        co.Leggi(ConnectionString, StringaDb(myPOSTreader_1.Item("TerzoOrario")))
                        Call CaricaVettoreOrariOrdiniServizio(g, StringaDb(myPOSTreader_1.Item("TerzoGiornoSuccessivo")), W_InAlto, W_InBasso, "", "", "", StringaDb(myPOSTreader.Item("TipoComandante")), StringaDb(myPOSTreader.Item("Servizio")), cg.CampoGiustificativi(ConnectionString, StringaDb(myPOSTreader.Item("Servizio")), "TIPO"), NumeroDb(myPOSTreader.Item("CodiceFamiliare")), Vettore_Orari, co)
                    End If
                    If StringaDb(myPOSTreader_1.Item("QuartoOrario")) <> "" Then
                        Vettore_Orari.QuartoOrario(g) = StringaDb(myPOSTreader_1.Item("QuartoOrario"))
                        co.Leggi(ConnectionString, StringaDb(myPOSTreader_1.Item("QuartoOrario")))
                        Call CaricaVettoreOrariOrdiniServizio(g, StringaDb(myPOSTreader_1.Item("QuartoGiornoSuccessivo")), W_InAlto, W_InBasso, "", "", "", StringaDb(myPOSTreader.Item("TipoComandante")), StringaDb(myPOSTreader.Item("Servizio")), cg.CampoGiustificativi(ConnectionString, StringaDb(myPOSTreader.Item("Servizio")), "TIPO"), NumeroDb(myPOSTreader.Item("CodiceFamiliare")), Vettore_Orari, co)
                    End If
                    If StringaDb(myPOSTreader_1.Item("QuintoOrario")) <> "" Then
                        Vettore_Orari.QuintoOrario(g) = StringaDb(myPOSTreader_1.Item("QuintoOrario"))
                        co.Leggi(ConnectionString, StringaDb(myPOSTreader_1.Item("QuintoOrario")))
                        Call CaricaVettoreOrariOrdiniServizio(g, StringaDb(myPOSTreader_1.Item("QuintoGiornoSuccessivo")), W_InAlto, W_InBasso, "", "", "", StringaDb(myPOSTreader.Item("TipoComandante")), StringaDb(myPOSTreader.Item("Servizio")), cg.CampoGiustificativi(ConnectionString, StringaDb(myPOSTreader.Item("Servizio")), "TIPO"), NumeroDb(myPOSTreader.Item("CodiceFamiliare")), Vettore_Orari, co)
                    End If
                End If
                myPOSTreader_1.Close()
                cmd_1.Parameters.Clear()

            Else

                cmd_1.CommandText = "SELECT * FROM OrdiniServizioRigaOrarioComandanti" & _
                                        " WHERE Data = ?" & _
                                        " AND Gruppo = ?" & _
                                        " AND Tipo = ?" & _
                                        " AND Riga = ?"
                cmd_1.Parameters.AddWithValue("@Data", DataDb(myPOSTreader.Item("Data")))
                cmd_1.Parameters.AddWithValue("@Gruppo", StringaDb(myPOSTreader.Item("GruppoComandante")))
                cmd_1.Parameters.AddWithValue("@Tipo", StringaDb(myPOSTreader.Item("TipoComandante")))
                cmd_1.Parameters.AddWithValue("@Riga", NumeroDb(myPOSTreader.Item("Riga")))

                cmd_1.Connection = cn
                myPOSTreader_1 = cmd_1.ExecuteReader()
                If Not myPOSTreader_1.Read Then
                    myPOSTreader_1.Close()
                    cmd_1.Parameters.Clear()
                    cmd_1.CommandText = "SELECT * FROM OrdiniServizioRigaProfiloComandanti" & _
                                        " WHERE Data = ?" & _
                                        " AND Gruppo = ?" & _
                                        " AND Tipo = ?" & _
                                        " AND Riga = ?"
                    cmd_1.Parameters.AddWithValue("@Data", DataDb(myPOSTreader.Item("Data")))
                    cmd_1.Parameters.AddWithValue("@Gruppo", StringaDb(myPOSTreader.Item("GruppoComandante")))
                    cmd_1.Parameters.AddWithValue("@Tipo", StringaDb(myPOSTreader.Item("TipoComandante")))
                    cmd_1.Parameters.AddWithValue("@Riga", NumeroDb(myPOSTreader.Item("Riga")))

                    cmd_1.Connection = cn
                    myPOSTreader_1 = cmd_1.ExecuteReader()
                    If Not myPOSTreader_1.Read Then
                        If Vettore_Orari.Variati(g) <> "P" And Vettore_Orari.Variati(g) <> "S" And Vettore_Orari.Modificati(g) <> "S" Then
                            Vettore_Orari.Azzerati(g) = "S"
                            For i = 0 To 9
                                If Vettore_Orari.Giustificativo(i, g) = "" Then Exit For
                            Next i
                            Vettore_Orari.Giustificativo(i, g) = StringaDb(myPOSTreader.Item("Servizio"))
                            Vettore_Orari.NelGruppo(i, g) = StringaDb(myPOSTreader.Item("NelGruppo"))
                            Vettore_Orari.Familiare(i, g) = NumeroDb(myPOSTreader.Item("CodiceFamiliare"))
                            Vettore_Orari.Tipo(i, g) = cg.CampoGiustificativi(ConnectionString, Vettore_Orari.Giustificativo(i, g), "Tipo")
                            Vettore_Orari.TipoServizio(i, g) = StringaDb(myPOSTreader.Item("TipoComandante"))
                        End If
                    Else
                        Do
                            Call CaricaVettoreOrariOrdiniServizio(g, StringaDb(myPOSTreader_1.Item("GiornoSuccessivo")), W_InAlto, W_InBasso, Format(DataDb(myPOSTreader_1.Item("Dalle")), "HH.mm"), Format(DataDb(myPOSTreader_1.Item("Alle")), "HH.mm"), Format(DataDb(myPOSTreader_1.Item("Pausa")), "HH.mm"), StringaDb(myPOSTreader.Item("TipoComandante")), StringaDb(myPOSTreader.Item("Servizio")), cg.CampoGiustificativi(ConnectionString, StringaDb(myPOSTreader.Item("Servizio")), "TIPO"), NumeroDb(myPOSTreader.Item("CodiceFamiliare")), Vettore_Orari, co)
                        Loop While myPOSTreader_1.Read
                    End If
                    myPOSTreader_1.Close()
                    cmd_1.Parameters.Clear()

                Else

                    If StringaDb(myPOSTreader_1.Item("PrimoOrario")) <> "" Then
                        Vettore_Orari.PrimoOrario(g) = StringaDb(myPOSTreader_1.Item("PrimoOrario"))
                        co.Leggi(ConnectionString, StringaDb(myPOSTreader_1.Item("PrimoOrario")))
                        Call CaricaVettoreOrariOrdiniServizio(g, StringaDb(myPOSTreader_1.Item("PrimoGiornoSuccessivo")), W_InAlto, W_InBasso, "", "", "", StringaDb(myPOSTreader.Item("TipoComandante")), StringaDb(myPOSTreader.Item("Servizio")), cg.CampoGiustificativi(ConnectionString, StringaDb(myPOSTreader.Item("Servizio")), "TIPO"), NumeroDb(myPOSTreader.Item("CodiceFamiliare")), Vettore_Orari, co)
                    End If
                    If StringaDb(myPOSTreader_1.Item("SecondoOrario")) <> "" Then
                        Vettore_Orari.SecondoOrario(g) = StringaDb(myPOSTreader_1.Item("SecondoOrario"))
                        co.Leggi(ConnectionString, StringaDb(myPOSTreader_1.Item("SecondoOrario")))
                        Call CaricaVettoreOrariOrdiniServizio(g, StringaDb(myPOSTreader_1.Item("SecondoGiornoSuccessivo")), W_InAlto, W_InBasso, "", "", "", StringaDb(myPOSTreader.Item("TipoComandante")), StringaDb(myPOSTreader.Item("Servizio")), cg.CampoGiustificativi(ConnectionString, StringaDb(myPOSTreader.Item("Servizio")), "TIPO"), NumeroDb(myPOSTreader.Item("CodiceFamiliare")), Vettore_Orari, co)
                    End If
                    If StringaDb(myPOSTreader_1.Item("TerzoOrario")) <> "" Then
                        Vettore_Orari.TerzoOrario(g) = StringaDb(myPOSTreader_1.Item("TerzoOrario"))
                        co.Leggi(ConnectionString, StringaDb(myPOSTreader_1.Item("PrimoOrario")))
                        Call CaricaVettoreOrariOrdiniServizio(g, StringaDb(myPOSTreader_1.Item("TerzoGiornoSuccessivo")), W_InAlto, W_InBasso, "", "", "", StringaDb(myPOSTreader.Item("TipoComandante")), StringaDb(myPOSTreader.Item("Servizio")), cg.CampoGiustificativi(ConnectionString, StringaDb(myPOSTreader.Item("Servizio")), "TIPO"), NumeroDb(myPOSTreader.Item("CodiceFamiliare")), Vettore_Orari, co)
                    End If
                    If StringaDb(myPOSTreader_1.Item("QuartoOrario")) <> "" Then
                        Vettore_Orari.QuartoOrario(g) = StringaDb(myPOSTreader_1.Item("QuartoOrario"))
                        co.Leggi(ConnectionString, StringaDb(myPOSTreader_1.Item("QuartoOrario")))
                        Call CaricaVettoreOrariOrdiniServizio(g, StringaDb(myPOSTreader_1.Item("QuartoGiornoSuccessivo")), W_InAlto, W_InBasso, "", "", "", StringaDb(myPOSTreader.Item("TipoComandante")), StringaDb(myPOSTreader.Item("Servizio")), cg.CampoGiustificativi(ConnectionString, StringaDb(myPOSTreader.Item("Servizio")), "TIPO"), NumeroDb(myPOSTreader.Item("CodiceFamiliare")), Vettore_Orari, co)
                    End If
                    If StringaDb(myPOSTreader_1.Item("QuintoOrario")) <> "" Then
                        Vettore_Orari.QuintoOrario(g) = StringaDb(myPOSTreader_1.Item("QuintoOrario"))
                        co.Leggi(ConnectionString, StringaDb(myPOSTreader_1.Item("QuintoOrario")))
                        Call CaricaVettoreOrariOrdiniServizio(g, StringaDb(myPOSTreader_1.Item("QuintoGiornoSuccessivo")), W_InAlto, W_InBasso, "", "", "", StringaDb(myPOSTreader.Item("TipoComandante")), StringaDb(myPOSTreader.Item("Servizio")), cg.CampoGiustificativi(ConnectionString, StringaDb(myPOSTreader.Item("Servizio")), "TIPO"), NumeroDb(myPOSTreader.Item("CodiceFamiliare")), Vettore_Orari, co)
                    End If
                End If
                myPOSTreader_1.Close()
                cmd_1.Parameters.Clear()
            End If
        Loop
        myPOSTreader.Close()
        cmd.Parameters.Clear()

        '
        ' Ordina il Vettore_Orari
        '

        For g = g_min To g_max
            For i = 0 To 9
                '      If Vettore_Orari.Giustificativo(i, g) <> "" And Vettore_Orari.Dalle(i, g) = "00.00" And Vettore_Orari.Alle(i, g) = "00.00" Then
                '        Vettore_Orari.Dalle(i, g) = "24.00"
                '        If ZeroAlle = "N" Then
                '          Vettore_Orari.Alle(i, g) = "24.00"
                '          Vettore_Orari.AlleAlto(i, g) = "24.00"
                '          Vettore_Orari.AlleBasso(i, g) = "24.00"
                '        End If
                '      End If
                If Vettore_Orari.Giustificativo(i, g) <> "" Then
                    If Vettore_Orari.Dalle(i, g) = "00.00" Then
                        If Vettore_Orari.Alle(i, g) = "00.00" Then
                            Vettore_Orari.Dalle(i, g) = "24.00"
                            If ZeroAlle = "N" Then
                                Vettore_Orari.Alle(i, g) = "24.00"
                                Vettore_Orari.AlleAlto(i, g) = "24.00"
                                Vettore_Orari.AlleBasso(i, g) = "24.00"
                            End If
                        End If
                    Else
                        If Vettore_Orari.Alle(i, g) = "00.00" Then
                            If ZeroAlle = "N" Then
                                Vettore_Orari.Alle(i, g) = "24.00"
                                Vettore_Orari.AlleAlto(i, g) = "24.00"
                                Vettore_Orari.AlleBasso(i, g) = "24.00"
                            End If
                        End If
                    End If
                End If
            Next i
            Sw_Ok = False
            Do Until Sw_Ok = True
                Sw_Ok = True
                For i = 1 To 9
                    If Vettore_Orari.Giustificativo(i, g) = "" Then Exit For
                    If Vettore_Orari.Dalle(i - 1, g) > Vettore_Orari.Dalle(i, g) Or (Vettore_Orari.Dalle(i - 1, g) = Vettore_Orari.Dalle(i, g) And Vettore_Orari.Tipo(i - 1, g) > Vettore_Orari.Tipo(i, g)) Then
                        Sw_Ok = False
                        W_Dalle = Vettore_Orari.Dalle(i, g)
                        W_DalleAlto = Vettore_Orari.DalleAlto(i, g)
                        W_DalleBasso = Vettore_Orari.DalleBasso(i, g)
                        W_DalleObbligo = Vettore_Orari.DalleObbligo(i, g)
                        '          W_DCT = Vettore_Orari.DCT(i, g)
                        W_Alle = Vettore_Orari.Alle(i, g)
                        W_AlleAlto = Vettore_Orari.AlleAlto(i, g)
                        W_AlleBasso = Vettore_Orari.AlleBasso(i, g)
                        W_AlleObbligo = Vettore_Orari.AlleObbligo(i, g)
                        W_Pausa = Vettore_Orari.Pausa(i, g)
                        W_Giustificativo = Vettore_Orari.Giustificativo(i, g)
                        W_NelGruppo = Vettore_Orari.NelGruppo(i, g)
                        W_Familiare = Vettore_Orari.Familiare(i, g)
                        W_Tipo = Vettore_Orari.Tipo(i, g)
                        W_TipoServizio = Vettore_Orari.TipoServizio(i, g)
                        W_GiornoSuccessivo = Vettore_Orari.GiornoSuccessivo(i, g)
                        If i < 5 Then
                            W_NelGruppo = Vettore_Orari.NelGruppo(i, g)
                        End If

                        Vettore_Orari.Dalle(i, g) = Vettore_Orari.Dalle(i - 1, g)
                        Vettore_Orari.DalleAlto(i, g) = Vettore_Orari.DalleAlto(i - 1, g)
                        Vettore_Orari.DalleBasso(i, g) = Vettore_Orari.DalleBasso(i - 1, g)
                        Vettore_Orari.DalleObbligo(i, g) = Vettore_Orari.DalleObbligo(i - 1, g)
                        '          Vettore_Orari.DCT(i, g) = Vettore_Orari.DCT(i - 1, g)
                        Vettore_Orari.Alle(i, g) = Vettore_Orari.Alle(i - 1, g)
                        Vettore_Orari.AlleAlto(i, g) = Vettore_Orari.AlleAlto(i - 1, g)
                        Vettore_Orari.AlleBasso(i, g) = Vettore_Orari.AlleBasso(i - 1, g)
                        Vettore_Orari.AlleObbligo(i, g) = Vettore_Orari.AlleObbligo(i - 1, g)
                        Vettore_Orari.Pausa(i, g) = Vettore_Orari.Pausa(i - 1, g)
                        Vettore_Orari.Giustificativo(i, g) = Vettore_Orari.Giustificativo(i - 1, g)
                        Vettore_Orari.NelGruppo(i, g) = Vettore_Orari.NelGruppo(i - 1, g)
                        Vettore_Orari.Familiare(i, g) = Vettore_Orari.Familiare(i - 1, g)
                        Vettore_Orari.Tipo(i, g) = Vettore_Orari.Tipo(i - 1, g)
                        Vettore_Orari.TipoServizio(i, g) = Vettore_Orari.TipoServizio(i - 1, g)
                        Vettore_Orari.GiornoSuccessivo(i, g) = Vettore_Orari.GiornoSuccessivo(i - 1, g)
                        If i < 5 Then
                            Vettore_Orari.NelGruppo(i, g) = Vettore_Orari.NelGruppo(i - 1, g)
                        End If

                        Vettore_Orari.Dalle(i - 1, g) = W_Dalle
                        Vettore_Orari.DalleAlto(i - 1, g) = W_DalleAlto
                        Vettore_Orari.DalleBasso(i - 1, g) = W_DalleBasso
                        Vettore_Orari.DalleObbligo(i - 1, g) = W_DalleObbligo
                        '          Vettore_Orari.DCT(i - 1, g) = W_DCT
                        Vettore_Orari.Alle(i - 1, g) = W_Alle
                        Vettore_Orari.AlleAlto(i - 1, g) = W_AlleAlto
                        Vettore_Orari.AlleBasso(i - 1, g) = W_AlleBasso
                        Vettore_Orari.AlleObbligo(i - 1, g) = W_AlleObbligo
                        Vettore_Orari.Pausa(i - 1, g) = W_Pausa
                        Vettore_Orari.Giustificativo(i - 1, g) = W_Giustificativo
                        Vettore_Orari.NelGruppo(i - 1, g) = W_NelGruppo
                        Vettore_Orari.Familiare(i - 1, g) = W_Familiare
                        Vettore_Orari.Tipo(i - 1, g) = W_Tipo
                        Vettore_Orari.TipoServizio(i - 1, g) = W_TipoServizio
                        Vettore_Orari.GiornoSuccessivo(i - 1, g) = W_GiornoSuccessivo
                        If i < 5 Then
                            Vettore_Orari.NelGruppo(i - 1, g) = W_NelGruppo
                        End If
                    End If
                Next i
            Loop
            For i = 0 To 9
                If Vettore_Orari.Dalle(i, g) = "24.00" Then Vettore_Orari.Dalle(i, g) = "00.00"
            Next i
            For i = 0 To 9
                If Vettore_Orari.Tipo(i, g) = "A" Then
                    For ii = i To 9
                        If Vettore_Orari.Tipo(ii, g) = "P" And Vettore_Orari.Dalle(i, g) = Vettore_Orari.Dalle(ii, g) Then Vettore_Orari.Tipo(i, g) = "I"
                    Next ii
                End If
            Next i
        Next g
    End Function

    Public Sub Azzera_Vettore_Orari(ByVal g As Byte, ByVal d As Byte, ByRef Vettore_Orari As Cls_Struttura_Orari)

        For i = 0 To 9
            Vettore_Orari.Dalle(i, g) = "00.00"
            Vettore_Orari.Alle(i, g) = "00.00"
            Vettore_Orari.DalleAlto(i, g) = "00.00"
            Vettore_Orari.AlleAlto(i, g) = "00.00"
            Vettore_Orari.DalleBasso(i, g) = "00.00"
            Vettore_Orari.AlleBasso(i, g) = "00.00"
            Vettore_Orari.DalleObbligo(i, g) = "00.00"
            Vettore_Orari.AlleObbligo(i, g) = "00.00"
            '      Vettore_Orari.DCT(i, g) = ""
            Vettore_Orari.Pausa(i, g) = "00.00"
            Vettore_Orari.Giustificativo(i, g) = ""
            Vettore_Orari.Familiare(i, g) = 0
            Vettore_Orari.Tipo(i, g) = ""
            Vettore_Orari.TipoServizio(i, g) = ""
            Vettore_Orari.GiornoSuccessivo(i, g) = ""
            If i < 5 Then
                Vettore_Orari.NelGruppo(i, g) = ""
            End If
        Next i
        Vettore_Orari.PrimoOrario(g) = ""
        Vettore_Orari.SecondoOrario(g) = ""
        Vettore_Orari.TerzoOrario(g) = ""
        Vettore_Orari.QuartoOrario(g) = ""
        Vettore_Orari.QuintoOrario(g) = ""
        Vettore_Orari.Richiesti(g) = ""
        Vettore_Orari.Variati(g) = ""
        Vettore_Orari.Modificati(g) = ""
        Vettore_Orari.Azzerati(g) = ""
        Vettore_Orari.OrdiniServizio(g) = ""
        Vettore_Orari.TipoFlessibilita(g) = ""
        Vettore_Orari.FasceOrarie(g) = ""
        Vettore_Orari.GgSett(g) = d
    End Sub

    Private Function ValiditaOrarioFlessibile(ByVal ConnectionString As String, ByVal Codice As String, ByVal Data As String) As Date
        Dim cof As New Cls_OrarioFlessibile

        ValiditaOrarioFlessibile = DateSerial(1899, 12, 30)
        If Codice = "" Then Exit Function

        cof.Trova(ConnectionString, Codice, Data)
        If cof.Id > 0 Then
            ValiditaOrarioFlessibile = cof.Validita
        End If
    End Function

    Private Sub CaricaVettoreOrari(ByVal ConnectionString As String, ByVal i As Byte, ByVal g As Byte, ByVal W_InAlto As Integer, ByVal W_InBasso As Integer, ByRef Vettore_Orari As Cls_Struttura_Orari, ByRef Orari As Cls_Orari)
        Dim cg As New Cls_Giustificativi
        Dim cpt As New Cls_ParametriTurni
        cpt.Leggi(ConnectionString)

        Vettore_Orari.Dalle(i, g) = Format(Orari.Dalle, "HH.mm")
        Vettore_Orari.DalleAlto(i, g) = Format(Orari.Dalle, "HH.mm")
        Vettore_Orari.DalleBasso(i, g) = Format(Orari.Dalle, "HH.mm")
        Vettore_Orari.Alle(i, g) = Format(Orari.Alle, "HH.mm")
        Vettore_Orari.AlleAlto(i, g) = Format(Orari.Alle, "HH.mm")
        Vettore_Orari.AlleBasso(i, g) = Format(Orari.Alle, "HH.mm")
        If Vettore_Orari.TipoFlessibilita(g) = "F" Or Vettore_Orari.TipoFlessibilita(g) = "X" Then
            Vettore_Orari.DalleObbligo(i, g) = Format(Orari.DalleObbligo, "HH.mm")
            Vettore_Orari.AlleObbligo(i, g) = Format(Orari.AlleObbligo, "HH.mm")
            If Val(Orari.DalleAlto) <> 0 Then Vettore_Orari.DalleAlto(i, g) = Format(Orari.DalleAlto, "HH.mm")
            If Val(Orari.DalleBasso) <> 0 Then Vettore_Orari.DalleBasso(i, g) = Format(Orari.DalleBasso, "HH.mm")
            If Val(Orari.AlleAlto) <> 0 Then Vettore_Orari.AlleAlto(i, g) = Format(Orari.AlleAlto, "HH.mm")
            If Val(Orari.AlleBasso) <> 0 Then Vettore_Orari.AlleBasso(i, g) = Format(Orari.AlleBasso, "HH.mm")
            If W_InAlto <> 0 Then
                Vettore_Orari.DalleAlto(i, g) = Format(DateAdd("n", W_InAlto, Orari.Dalle), "HH.mm")
                Vettore_Orari.AlleAlto(i, g) = Format(DateAdd("n", W_InAlto, Orari.Alle), "HH.mm")
            End If
            If W_InBasso <> 0 Then
                Vettore_Orari.DalleBasso(i, g) = Format(DateAdd("n", W_InBasso, Orari.Dalle), "HH.mm")
                Vettore_Orari.AlleBasso(i, g) = Format(DateAdd("n", W_InBasso, Orari.Alle), "HH.mm")
            End If
        End If
        Vettore_Orari.Pausa(i, g) = Format(Orari.Pausa, "HH.mm")
        Vettore_Orari.Giustificativo(i, g) = Orari.Giustificativo
        Vettore_Orari.Familiare(i, g) = 0
        If Vettore_Orari.Giustificativo(i, g) = "" Then
            Vettore_Orari.Giustificativo(i, g) = cpt.GiustificativoOrarioLavoro
        Else
            If Format(Orari.Jolly, "HH.mm") <> 0 Then
                Vettore_Orari.Alle(i, g) = Format(Orari.Jolly, "HH.mm")
            End If
        End If
        Vettore_Orari.Tipo(i, g) = cg.CampoGiustificativi(ConnectionString, Vettore_Orari.Giustificativo(i, g), "Tipo")
    End Sub

    Private Sub CaricaVettoreOrariOrdiniServizio(ByVal g As Integer, ByVal GiornoSuccessivo As String, ByVal W_InAlto As Object, ByVal W_InBasso As Object, ByVal Dalle As String, ByVal Alle As String, ByVal Pausa As String, ByVal TipoComandante As String, ByVal Servizio As String, ByVal Tipo As String, ByVal Familiare As Byte, ByRef Vettore_Orari As Cls_Struttura_Orari, ByRef Orari As Cls_Orari)

        Dim i As Byte

        If g < 0 And (GiornoSuccessivo = "" Or GiornoSuccessivo = "R") Then Exit Sub
        If GiornoSuccessivo = "S" Then g = g + 1
        If g > 41 Then Exit Sub
        If Vettore_Orari.Variati(g) = "P" Or Vettore_Orari.Variati(g) = "S" Or Vettore_Orari.Modificati(g) = "S" Then Exit Sub
        If Vettore_Orari.Azzerati(g) = "" Then
            Vettore_Orari.Azzerati(g) = "S"
            For i = 0 To 9
                Vettore_Orari.Dalle(i, g) = "00.00"
                Vettore_Orari.Alle(i, g) = "00.00"
                Vettore_Orari.DalleAlto(i, g) = "00.00"
                Vettore_Orari.AlleAlto(i, g) = "00.00"
                Vettore_Orari.DalleBasso(i, g) = "00.00"
                Vettore_Orari.AlleBasso(i, g) = "00.00"
                Vettore_Orari.DalleObbligo(i, g) = "00.00"
                Vettore_Orari.AlleObbligo(i, g) = "00.00"
                '      Vettore_Orari.DCT(i, g) = ""
                Vettore_Orari.Pausa(i, g) = "00.00"
                Vettore_Orari.Giustificativo(i, g) = ""
                Vettore_Orari.Familiare(i, g) = 0
                Vettore_Orari.Tipo(i, g) = ""
                Vettore_Orari.TipoServizio(i, g) = ""
                Vettore_Orari.GiornoSuccessivo(i, g) = ""
            Next i
            Vettore_Orari.Richiesti(g) = ""
            Vettore_Orari.Variati(g) = ""
            Vettore_Orari.Modificati(g) = ""
        End If

        For i = 0 To 9
            If Vettore_Orari.Giustificativo(i, g) = "" Then Exit For
        Next i

        If Dalle <> "" Or Alle <> "" Or Pausa <> "" Then
            Vettore_Orari.Dalle(i, g) = Dalle
            Vettore_Orari.DalleAlto(i, g) = Dalle
            Vettore_Orari.DalleBasso(i, g) = Dalle
            Vettore_Orari.Alle(i, g) = Alle
            Vettore_Orari.AlleAlto(i, g) = Alle
            Vettore_Orari.AlleBasso(i, g) = Alle
            Vettore_Orari.Pausa(i, g) = Pausa
            If Vettore_Orari.TipoFlessibilita(g) = "F" Or Vettore_Orari.TipoFlessibilita(g) = "X" Then
                If W_InAlto(g) <> 0 Then
                    Vettore_Orari.DalleAlto(i, g) = Format(DateAdd("n", W_InAlto(g), TimeSerial(Val(Mid(Dalle, 1, 2)), Val(Mid(Dalle, 4, 2)), 0)), "HH.mm")
                    Vettore_Orari.AlleAlto(i, g) = Format(DateAdd("n", W_InAlto(g), TimeSerial(Val(Mid(Alle, 1, 2)), Val(Mid(Alle, 4, 2)), 0)), "HH.mm")
                End If
                If W_InBasso(g) <> 0 Then
                    Vettore_Orari.DalleBasso(i, g) = Format(DateAdd("n", W_InBasso(g), TimeSerial(Val(Mid(Dalle, 1, 2)), Val(Mid(Dalle, 4, 2)), 0)), "HH.mm")
                    Vettore_Orari.AlleBasso(i, g) = Format(DateAdd("n", W_InBasso(g), TimeSerial(Val(Mid(Alle, 1, 2)), Val(Mid(Alle, 4, 2)), 0)), "HH.mm")
                End If
            End If
        Else
            Vettore_Orari.Dalle(i, g) = Format(Orari.Dalle, "HH.mm")
            Vettore_Orari.DalleAlto(i, g) = Format(Orari.Dalle, "HH.mm")
            Vettore_Orari.DalleBasso(i, g) = Format(Orari.Dalle, "HH.mm")
            Vettore_Orari.Alle(i, g) = Format(Orari.Alle, "HH.mm")
            Vettore_Orari.AlleAlto(i, g) = Format(Orari.Alle, "HH.mm")
            Vettore_Orari.AlleBasso(i, g) = Format(Orari.Alle, "HH.mm")
            Vettore_Orari.Pausa(i, g) = Format(Orari.Pausa, "HH.mm")
            If Not Vettore_Orari.Giustificativo(i, g) = "" And Orari.Jolly <> New Date(1899, 12, 30, 0, 0, 0) Then
                Vettore_Orari.Alle(i, g) = Format(Orari.Jolly, "HH.mm")
            End If
            If Vettore_Orari.TipoFlessibilita(g) = "F" Or Vettore_Orari.TipoFlessibilita(g) = "X" Then
                Vettore_Orari.DalleObbligo(i, g) = Format(Orari.DalleObbligo, "HH.mm")
                Vettore_Orari.AlleObbligo(i, g) = Format(Orari.AlleObbligo, "HH.mm")
                If W_InAlto(g) <> 0 Then
                    Vettore_Orari.DalleAlto(i, g) = Format(DateAdd("n", W_InAlto(g), Orari.Dalle), "HH.mm")
                    Vettore_Orari.AlleAlto(i, g) = Format(DateAdd("n", W_InAlto(g), Orari.Alle), "HH.mm")
                End If
                If W_InBasso(g) <> 0 Then
                    Vettore_Orari.DalleBasso(i, g) = Format(DateAdd("n", W_InBasso(g), Orari.Dalle), "HH.mm")
                    Vettore_Orari.AlleBasso(i, g) = Format(DateAdd("n", W_InBasso(g), Orari.Alle), "HH.mm")
                End If
                If Val(Orari.DalleAlto) <> 0 Then Vettore_Orari.DalleAlto(i, g) = Format(Orari.DalleAlto, "HH.mm")
                If Val(Orari.DalleBasso) <> 0 Then Vettore_Orari.DalleBasso(i, g) = Format(Orari.DalleBasso, "HH.mm")
                If Val(Orari.AlleAlto) <> 0 Then Vettore_Orari.AlleAlto(i, g) = Format(Orari.AlleAlto, "HH.mm")
                If Val(Orari.AlleBasso) <> 0 Then Vettore_Orari.AlleBasso(i, g) = Format(Orari.AlleBasso, "HH.mm")
            End If
        End If
        Vettore_Orari.Giustificativo(i, g) = Servizio
        Vettore_Orari.Familiare(i, g) = Familiare
        Vettore_Orari.Tipo(i, g) = Tipo
        Vettore_Orari.TipoServizio(i, g) = TipoComandante
        Vettore_Orari.GiornoSuccessivo(i, g) = GiornoSuccessivo
    End Sub

    Private Sub CaricaVettoreOrariFerie(ByVal ConnectionString As String, ByVal g As Byte, ByRef Vettore_Orari As Cls_Struttura_Orari, ByRef Orari As Cls_Orari)
        Dim i As Byte
        For i = 0 To 4
            Vettore_Orari.Dalle(i, g) = "00.00"
            Vettore_Orari.DalleAlto(i, g) = "00.00"
            Vettore_Orari.DalleBasso(i, g) = "00.00"
            Vettore_Orari.Alle(i, g) = "00.00"
            Vettore_Orari.AlleAlto(i, g) = "00.00"
            Vettore_Orari.AlleBasso(i, g) = "00.00"
            Vettore_Orari.TipoFlessibilita(g) = ""
            Vettore_Orari.Pausa(i, g) = "00.00"
            Vettore_Orari.Familiare(i, g) = 0
            Vettore_Orari.Giustificativo(i, g) = ""
            Vettore_Orari.Tipo(i, g) = ""
        Next i
        Call CaricaVettoreOrari(ConnectionString, 0, g, 0, 0, Vettore_Orari, Orari)
    End Sub

    '------------------------------------------------------------
    'procedura di sviluppo profili orari per un giorno
    '------------------------------------------------------------

    Public Function SviluppoProfiloOrarioSingoloGiorno(ByVal ConnectionString As String, ByVal CodiceSuperGruppo As String, ByVal CodiceContratto As String, ByVal ProfiloOrario As String, ByVal Data As Date, ByRef Vettore_Orari_Giorno As Cls_Struttura_Orari_Giorno) As String
        Dim ccc As New Cls_CondizioniContrattuali
        Dim cpot As New Cls_ProfiliOrariTesta
        Dim cpor As New Cls_ProfiliOrariRiga
        Dim cspor As New Cls_StrutturaProfiliOrariRiga
        Dim co As New Cls_Orari
        Dim cxst As New Cls_ClsXScriptTurni
        cxst.ConnectionString = ConnectionString

        Dim Riga As Integer
        Dim W_InAlto As Integer
        Dim W_InBasso As Integer
        Dim Giorni As Long
        Dim Resto As Long
        Dim Contatore As Long
        Dim CodTur As String
        Dim Orario As String

        SviluppoProfiloOrarioSingoloGiorno = ""

        For i = 0 To 14
            Vettore_Orari_Giorno.Dalle(i) = "00.00"
            Vettore_Orari_Giorno.Alle(i) = "00.00"
            Vettore_Orari_Giorno.DalleAlto(i) = "00.00"
            Vettore_Orari_Giorno.AlleAlto(i) = "00.00"
            Vettore_Orari_Giorno.DalleBasso(i) = "00.00"
            Vettore_Orari_Giorno.AlleBasso(i) = "00.00"
            Vettore_Orari_Giorno.DalleObbligo(i) = "00.00"
            Vettore_Orari_Giorno.AlleObbligo(i) = "00.00"
            '    Vettore_Orari_Giorno.DCT(i) = ""
            Vettore_Orari_Giorno.Pausa(i) = "00.00"
            Vettore_Orari_Giorno.Giustificativo(i) = ""
            Vettore_Orari_Giorno.Familiare(i) = 0
            Vettore_Orari_Giorno.Tipo(i) = ""
            Vettore_Orari_Giorno.TipoServizio(i) = ""
            Vettore_Orari_Giorno.GiornoSuccessivo(i) = ""
        Next i
        Vettore_Orari_Giorno.PrimoOrario = ""
        Vettore_Orari_Giorno.SecondoOrario = ""
        Vettore_Orari_Giorno.TerzoOrario = ""
        Vettore_Orari_Giorno.QuartoOrario = ""
        Vettore_Orari_Giorno.QuintoOrario = ""
        Vettore_Orari_Giorno.Richiesti = ""
        Vettore_Orari_Giorno.Variati = ""
        Vettore_Orari_Giorno.Modificati = ""
        Vettore_Orari_Giorno.TipoFlessibilita = ""
        Vettore_Orari_Giorno.FasceOrarie = ""
        Vettore_Orari_Giorno.GgSett = Weekday(Data)

        W_InAlto = 0
        W_InBasso = 0

        ccc.Trova(ConnectionString, CodiceContratto, Data)
        If ccc.Id = 0 Then
            SviluppoProfiloOrarioSingoloGiorno = "(E) Condizioni Contrattuali per il Contratto: " & CodiceContratto & "  inesistenti"
            Exit Function
        End If

        If co.Conta(ConnectionString) = 0 Then
            SviluppoProfiloOrarioSingoloGiorno = "(E) Nessun orario in tabella" & vbNewLine
            Exit Function
        End If

        cpot.Trova(ConnectionString, ProfiloOrario, Data)
        If cpot.Id = 0 Then
            SviluppoProfiloOrarioSingoloGiorno = "(E) Profilo Orario '" & ProfiloOrario & "' non sviluppato" & vbNewLine
            Exit Function
        End If

        Riga = 1
        If cpot.CodiceStruttura <> "" Then
            CodTur = cpot.CodiceStruttura
            Contatore = cspor.Conta(ConnectionString, cpot.CodiceStruttura)
        Else
            CodTur = ProfiloOrario
            Contatore = cpor.Conta(ConnectionString, ProfiloOrario, cpot.Validita)
        End If
        If Contatore = 0 Then
            SviluppoProfiloOrarioSingoloGiorno = " (E) Profilo Orario '" & CodTur & "' senza righe" & vbNewLine
            Exit Function
        End If

        Dim cn As OleDbConnection
        cn = New Data.OleDb.OleDbConnection(ConnectionString)

        cn.Open()
        Dim cmd As New OleDbCommand()

        If cpot.CodiceStruttura <> "" Then
            cmd.CommandText = "SELECT *, 'A' + RIGHT('00000'+ CONVERT(VARCHAR,Riga),5) As Selezione FROM StrutturaProfiliOrariRiga" & _
                              " WHERE Codice = ?" & _
                              " AND Riga >= ?" & _
                              " UNION " & _
                              "SELECT *, 'B' + RIGHT('00000'+ CONVERT(VARCHAR,Riga),5) As Selezione FROM StrutturaProfiliOrariRiga" & _
                              " WHERE Codice = ?" & _
                              " AND Riga < ?" & _
                              " ORDER BY Selezione "
            cmd.Parameters.AddWithValue("@Codice", cpot.CodiceStruttura)
            cmd.Parameters.AddWithValue("@Riga", cpot.PrimoElemento)
            cmd.Parameters.AddWithValue("@Codice", cpot.CodiceStruttura)
            cmd.Parameters.AddWithValue("@Riga", cpot.PrimoElemento)
        Else
            cmd.CommandText = "SELECT * FROM ProfiliOrariRiga" & _
                              " WHERE Codice = ?" & _
                              " AND Validita = ?" & _
                              " ORDER BY Riga "
            cmd.Parameters.AddWithValue("@Codice", ProfiloOrario)
            cmd.Parameters.AddWithValue("@Validita", cpot.Validita)
        End If

        Giorni = DateDiff("d", cpot.Validita, Data)
        Giorni = Giorni + 1
        Resto = Giorni - (Int(Giorni / Contatore) * Contatore)
        If Resto = 0 Then
            Resto = Contatore
        End If

        cmd.Connection = cn
        Dim myPOSTreader As OleDbDataReader = cmd.ExecuteReader()
        Do Until Not myPOSTreader.Read Or Riga = Resto
            Riga = Riga + 1
        Loop

        If Vettore_Orari_Giorno.Variati = "" And cpot.OrarioGiornoFestivo <> "" And (Weekday(Data) = ccc.FestivitaSettimanale Or cxst.GiornoFestivo(Data, CodiceSupergruppo) = True) Then
            Vettore_Orari_Giorno.PrimoOrario = cpot.OrarioGiornoFestivo

            Call CaricaVettoreOrariUltimoGiorno(ConnectionString, 0, W_InAlto, W_InBasso, Vettore_Orari_Giorno, Vettore_Orari_Giorno.PrimoOrario)

        Else
            If StringaDb(myPOSTreader.Item("PrimoOrario")) <> "" Then
                Vettore_Orari_Giorno.PrimoOrario = StringaDb(myPOSTreader.Item("PrimoOrario"))
                Orario = Vettore_Orari_Giorno.PrimoOrario
                co.Leggi(ConnectionString, Orario)

                If co.OrarioGiornoFestivo <> "" And (Weekday(Data) = ccc.FestivitaSettimanale Or cxst.GiornoFestivo(Data, CodiceSuperGruppo) = True) Then
                    Orario = co.OrarioGiornoFestivo
                End If

                Call CaricaVettoreOrariUltimoGiorno(ConnectionString, 0, W_InAlto, W_InBasso, Vettore_Orari_Giorno, Orario)
            End If
            If StringaDb(myPOSTreader.Item("SecondoOrario")) <> "" Then
                Vettore_Orari_Giorno.SecondoOrario = StringaDb(myPOSTreader.Item("SecondoOrario"))
                Orario = Vettore_Orari_Giorno.SecondoOrario
                co.Leggi(ConnectionString, Orario)

                If co.OrarioGiornoFestivo <> "" And (Weekday(Data) = ccc.FestivitaSettimanale Or cxst.GiornoFestivo(Data, CodiceSuperGruppo) = True) Then
                    Orario = co.OrarioGiornoFestivo
                End If

                Call CaricaVettoreOrariUltimoGiorno(ConnectionString, 1, W_InAlto, W_InBasso, Vettore_Orari_Giorno, Orario)

            End If
            If StringaDb(myPOSTreader.Item("TerzoOrario")) <> "" Then
                Vettore_Orari_Giorno.TerzoOrario = StringaDb(myPOSTreader.Item("TerzoOrario"))
                Orario = Vettore_Orari_Giorno.TerzoOrario
                co.Leggi(ConnectionString, Orario)

                If co.OrarioGiornoFestivo <> "" And (Weekday(Data) = ccc.FestivitaSettimanale Or cxst.GiornoFestivo(Data, CodiceSuperGruppo) = True) Then
                    Orario = co.OrarioGiornoFestivo
                End If

                Call CaricaVettoreOrariUltimoGiorno(ConnectionString, 2, W_InAlto, W_InBasso, Vettore_Orari_Giorno, Orario)

            End If
            If StringaDb(myPOSTreader.Item("QuartoOrario")) <> "" Then
                Vettore_Orari_Giorno.QuartoOrario = StringaDb(myPOSTreader.Item("QuartoOrario"))
                Orario = Vettore_Orari_Giorno.QuartoOrario
                co.Leggi(ConnectionString, Orario)

                If co.OrarioGiornoFestivo <> "" And (Weekday(Data) = ccc.FestivitaSettimanale Or cxst.GiornoFestivo(Data, CodiceSuperGruppo) = True) Then
                    Orario = co.OrarioGiornoFestivo
                End If

                Call CaricaVettoreOrariUltimoGiorno(ConnectionString, 3, W_InAlto, W_InBasso, Vettore_Orari_Giorno, Orario)

            End If
            If StringaDb(myPOSTreader.Item("QuintoOrario")) <> "" Then
                Vettore_Orari_Giorno.QuintoOrario = StringaDb(myPOSTreader.Item("QuintoOrario"))
                Orario = Vettore_Orari_Giorno.QuintoOrario
                co.Leggi(ConnectionString, Orario)

                If co.OrarioGiornoFestivo <> "" And (Weekday(Data) = ccc.FestivitaSettimanale Or cxst.GiornoFestivo(Data, CodiceSuperGruppo) = True) Then
                    Orario = co.OrarioGiornoFestivo
                End If

                Call CaricaVettoreOrariUltimoGiorno(ConnectionString, 4, W_InAlto, W_InBasso, Vettore_Orari_Giorno, Orario)

            End If
        End If
    End Function

    Private Sub Carica_Vettore_Orari_Giorno_ProfiliOrariVariati(ByVal ConnectionString As String, ByVal Dipendente As Object, ByVal Data As Date, ByVal Sw_Variazioni As String, ByVal Sw_Proposta As String, ByRef Vettore_Orari_Giorno As Cls_Struttura_Orari_Giorno)

        If Sw_Variazioni = "N" And Sw_Proposta = "N" Then Exit Sub

        Dim cn As OleDbConnection
        cn = New Data.OleDb.OleDbConnection(ConnectionString)

        cn.Open()
        Dim cmd As New OleDbCommand()

        If Sw_Variazioni = "S" Then
            If Sw_Proposta = "S" Then
                cmd.CommandText = "SELECT * FROM ProfiliOrariVariati " & _
                                  " WHERE CodiceDipendente = ?" & _
                                  " AND Data = ?" & _
                                  " ORDER BY Data"
            Else
                cmd.CommandText = "SELECT * FROM ProfiliOrariVariati " & _
                                  " WHERE CodiceDipendente = ?" & _
                                  " AND Proposta <> 'S'" & _
                                  " AND Data = ?" & _
                                  " ORDER BY Data"
            End If
        Else
            cmd.CommandText = "SELECT * FROM ProfiliOrariVariati " & _
                              " WHERE CodiceDipendente = ?" & _
                              " AND Proposta = 'S'" & _
                              " AND Data = ?" & _
                              " ORDER BY Data"
        End If
        cmd.Parameters.AddWithValue("@CodiceDipendente", Dipendente)
        cmd.Parameters.AddWithValue("@Data", Data)

        cmd.Connection = cn
        Dim myPOSTreader As OleDbDataReader = cmd.ExecuteReader()
        If myPOSTreader.Read Then

            If StringaDb(myPOSTreader.Item("Proposta")) = "S" Then
                Vettore_Orari_Giorno.Variati = "P"
            Else
                Vettore_Orari_Giorno.Variati = "S"
            End If
            Vettore_Orari_Giorno.PrimoOrario = StringaDb(myPOSTreader.Item("PrimoOrario"))
            Vettore_Orari_Giorno.SecondoOrario = StringaDb(myPOSTreader.Item("SecondoOrario"))
            Vettore_Orari_Giorno.TerzoOrario = StringaDb(myPOSTreader.Item("TerzoOrario"))
            Vettore_Orari_Giorno.QuartoOrario = StringaDb(myPOSTreader.Item("QuartoOrario"))
            Vettore_Orari_Giorno.QuintoOrario = StringaDb(myPOSTreader.Item("QuintoOrario"))
            Vettore_Orari_Giorno.NelGruppo(0) = StringaDb(myPOSTreader.Item("PrimoNelGruppo"))
            Vettore_Orari_Giorno.NelGruppo(1) = StringaDb(myPOSTreader.Item("SecondoNelGruppo"))
            Vettore_Orari_Giorno.NelGruppo(2) = StringaDb(myPOSTreader.Item("TerzoNelGruppo"))
            Vettore_Orari_Giorno.NelGruppo(3) = StringaDb(myPOSTreader.Item("QuartoNelGruppo"))
            Vettore_Orari_Giorno.NelGruppo(4) = StringaDb(myPOSTreader.Item("QuintoNelGruppo"))
            Vettore_Orari_Giorno.CambioRichiesto(0) = StringaDb(myPOSTreader.Item("PrimoCambioRichiesto"))
            Vettore_Orari_Giorno.CambioRichiesto(1) = StringaDb(myPOSTreader.Item("SecondoCambioRichiesto"))
            Vettore_Orari_Giorno.CambioRichiesto(2) = StringaDb(myPOSTreader.Item("TerzoCambioRichiesto"))
            Vettore_Orari_Giorno.CambioRichiesto(3) = StringaDb(myPOSTreader.Item("QuartoCambioRichiesto"))
            Vettore_Orari_Giorno.CambioRichiesto(4) = StringaDb(myPOSTreader.Item("QuintoCambioRichiesto"))
            For i = 0 To 4
                Vettore_Orari_Giorno.Familiare(i) = 0
            Next i
            Vettore_Orari_Giorno.FasceOrarie = ""
        End If
        cn.Close()
    End Sub

    Private Sub Carica_Vettore_Orari_Giorno_RichiesteAssenza(ByVal ConnectionString As String, ByVal Dipendente As Object, ByVal Data As Date, ByVal CodiceSuperGruppo As String, ByRef Vettore_Orari_Giorno As Cls_Struttura_Orari_Giorno)
        Dim cg As New Cls_Giustificativi
        Dim cpt As New Cls_ParametriTurni
        cpt.Leggi(ConnectionString)
        Dim cxst As New Cls_ClsXScriptTurni
        cxst.ConnectionString = ConnectionString

        Dim Chk_Giorno(6) As String
        Dim W_DataDal As Date
        Dim W_DataAl As Date
        Dim W_Dalle As String
        Dim W_Alle As String
        Dim W_Giustificativo As String
        Dim W_NelGruppo As String
        Dim W_Familiare As Byte
        Dim W_CambioRichiesto As String

        Dim Stringa As String

        Dim cn As OleDbConnection
        cn = New Data.OleDb.OleDbConnection(ConnectionString)

        cn.Open()
        Dim cmd As New OleDbCommand()

        cmd.CommandText = "SELECT * FROM RichiesteAssenza" & _
                " WHERE CodiceDipendente = ?" & _
                " AND Acquisito = 'S'" & _
                " AND (Annullato Is Null OR Annullato = '')" & _
                " AND (Revocato Is Null OR Revocato = '')" & _
                " AND ((DataAl Is Null" & _
                " AND DataDal = ?)" & _
                " OR (NOT DataAl Is Null" & _
                " AND DataDal <= ?" & _
                " AND DataAl >= ?))" & _
                " ORDER BY DataDal, Dalle"
        cmd.Parameters.AddWithValue("@CodiceDipendente", Dipendente)
        cmd.Parameters.AddWithValue("@DataDal", Data)
        cmd.Parameters.AddWithValue("@DataDal", Data)
        cmd.Parameters.AddWithValue("@DataAl", Data)

        cmd.Connection = cn
        Dim myPOSTreader As OleDbDataReader = cmd.ExecuteReader()
        Do While myPOSTreader.Read
            Vettore_Orari_Giorno.Richiesti = "S"
            W_DataDal = DataDb(myPOSTreader.Item("DataDal"))
            W_DataAl = DataDb(myPOSTreader.Item("DataAl"))
            W_Dalle = Format(DataDb(myPOSTreader.Item("Dalle")), "hh.nn")
            W_Alle = Format(DataDb(myPOSTreader.Item("Alle")), "hh.nn")
            W_Giustificativo = StringaDb(myPOSTreader.Item("Giustificativo"))
            cg.Leggi(ConnectionString, W_Giustificativo)
            W_NelGruppo = ""
            W_Familiare = 0
            W_CambioRichiesto = ""

            Stringa = cg.Assenza_ChkGiorno
            For i = 1 To 6
                If Mid(Stringa, i, 1) = "S" Then
                    Chk_Giorno(i) = "S"
                Else
                    Chk_Giorno(i) = "N"
                End If
            Next i
            If Mid(Stringa, 7, 1) = "S" Then
                Chk_Giorno(0) = "S"
            Else
                Chk_Giorno(0) = "N"
            End If

            If Not IsDate(W_DataAl) Then W_DataAl = W_DataDal

            If W_DataDal = W_DataAl And (W_Dalle <> "00.00" Or W_Alle <> "00.00") Then

                For i = 0 To 14
                    If Vettore_Orari_Giorno.Giustificativo(i) <> "" Then
                        If Vettore_Orari_Giorno.Dalle(i) = "00.00" And Vettore_Orari_Giorno.Alle(i) = "00.00" Then
                            Vettore_Orari_Giorno.Dalle(i) = W_Dalle
                            Vettore_Orari_Giorno.DalleAlto(i) = W_Dalle
                            Vettore_Orari_Giorno.DalleBasso(i) = W_Dalle
                            Vettore_Orari_Giorno.Alle(i) = W_Alle
                            Vettore_Orari_Giorno.AlleAlto(i) = W_Alle
                            Vettore_Orari_Giorno.AlleBasso(i) = W_Alle
                            Vettore_Orari_Giorno.Pausa(i) = "00.00"
                            Vettore_Orari_Giorno.Giustificativo(i) = W_Giustificativo
                            Vettore_Orari_Giorno.NelGruppo(i) = W_NelGruppo
                            Vettore_Orari_Giorno.Familiare(i) = W_Familiare
                            Vettore_Orari_Giorno.CambioRichiesto(i) = W_CambioRichiesto
                            Vettore_Orari_Giorno.Tipo(i) = cg.Tipo
                            For ii = i + 1 To 14
                                Vettore_Orari_Giorno.Dalle(ii) = "00.00"
                                Vettore_Orari_Giorno.Alle(ii) = "00.00"
                                Vettore_Orari_Giorno.DalleAlto(ii) = "00.00"
                                Vettore_Orari_Giorno.AlleAlto(ii) = "00.00"
                                Vettore_Orari_Giorno.DalleBasso(ii) = "00.00"
                                Vettore_Orari_Giorno.AlleBasso(ii) = "00.00"
                                Vettore_Orari_Giorno.DalleObbligo(ii) = "00.00"
                                Vettore_Orari_Giorno.AlleObbligo(ii) = "00.00"
                                Vettore_Orari_Giorno.Pausa(ii) = "00.00"
                                Vettore_Orari_Giorno.Giustificativo(ii) = ""
                                Vettore_Orari_Giorno.NelGruppo(ii) = ""
                                Vettore_Orari_Giorno.Familiare(ii) = 0
                                Vettore_Orari_Giorno.CambioRichiesto(ii) = ""
                                Vettore_Orari_Giorno.Tipo(ii) = ""
                                Vettore_Orari_Giorno.TipoServizio(ii) = ""
                                Vettore_Orari_Giorno.GiornoSuccessivo(ii) = ""
                            Next ii
                            i = 14

                        Else

                            If Vettore_Orari_Giorno.Dalle(i) < W_Dalle Then
                                If Vettore_Orari_Giorno.Alle(i) > W_Dalle Then
                                    For ii = 14 To i + 1 Step -1
                                        Vettore_Orari_Giorno.Dalle(ii) = Vettore_Orari_Giorno.Dalle(ii - 1)
                                        Vettore_Orari_Giorno.Alle(ii) = Vettore_Orari_Giorno.Alle(ii - 1)
                                        Vettore_Orari_Giorno.DalleAlto(ii) = Vettore_Orari_Giorno.DalleAlto(ii - 1)
                                        Vettore_Orari_Giorno.AlleAlto(ii) = Vettore_Orari_Giorno.AlleAlto(ii - 1)
                                        Vettore_Orari_Giorno.DalleBasso(ii) = Vettore_Orari_Giorno.DalleBasso(ii - 1)
                                        Vettore_Orari_Giorno.AlleBasso(ii) = Vettore_Orari_Giorno.AlleBasso(ii - 1)
                                        Vettore_Orari_Giorno.DalleObbligo(ii) = Vettore_Orari_Giorno.DalleObbligo(ii - 1)
                                        Vettore_Orari_Giorno.AlleObbligo(ii) = Vettore_Orari_Giorno.AlleObbligo(ii - 1)
                                        Vettore_Orari_Giorno.Pausa(ii) = Vettore_Orari_Giorno.Pausa(ii - 1)
                                        Vettore_Orari_Giorno.Giustificativo(ii) = Vettore_Orari_Giorno.Giustificativo(ii - 1)
                                        Vettore_Orari_Giorno.NelGruppo(ii) = Vettore_Orari_Giorno.NelGruppo(ii - 1)
                                        Vettore_Orari_Giorno.Familiare(ii) = Vettore_Orari_Giorno.Familiare(ii - 1)
                                        Vettore_Orari_Giorno.CambioRichiesto(ii) = Vettore_Orari_Giorno.CambioRichiesto(ii - 1)
                                        Vettore_Orari_Giorno.Tipo(ii) = Vettore_Orari_Giorno.Tipo(ii - 1)
                                        Vettore_Orari_Giorno.TipoServizio(ii) = Vettore_Orari_Giorno.TipoServizio(ii - 1)
                                        Vettore_Orari_Giorno.GiornoSuccessivo(ii) = Vettore_Orari_Giorno.GiornoSuccessivo(ii - 1)
                                    Next ii
                                    If Vettore_Orari_Giorno.Alle(i) <= W_Alle Then
                                        Vettore_Orari_Giorno.Alle(i) = W_Dalle
                                        Vettore_Orari_Giorno.AlleAlto(i) = W_Dalle
                                        Vettore_Orari_Giorno.AlleBasso(i) = W_Dalle
                                        Vettore_Orari_Giorno.Dalle(i + 1) = W_Dalle
                                        Vettore_Orari_Giorno.DalleAlto(i + 1) = W_Dalle
                                        Vettore_Orari_Giorno.DalleBasso(i + 1) = W_Dalle
                                        Vettore_Orari_Giorno.Pausa(i + 1) = "00.00"
                                        Vettore_Orari_Giorno.Giustificativo(i + 1) = W_Giustificativo
                                        Vettore_Orari_Giorno.NelGruppo(i + 1) = W_NelGruppo
                                        Vettore_Orari_Giorno.Familiare(i + 1) = W_Familiare
                                        Vettore_Orari_Giorno.CambioRichiesto(i + 1) = W_CambioRichiesto
                                        Vettore_Orari_Giorno.Tipo(i + 1) = cg.Tipo
                                        W_Dalle = Vettore_Orari_Giorno.Alle(i + 1)
                                        Vettore_Orari_Giorno.AlleAlto(i + 1) = W_Dalle
                                        Vettore_Orari_Giorno.AlleBasso(i + 1) = W_Dalle

                                    Else

                                        For ii = 14 To i + 1 Step -1
                                            Vettore_Orari_Giorno.Dalle(ii) = Vettore_Orari_Giorno.Dalle(ii - 1)
                                            Vettore_Orari_Giorno.Alle(ii) = Vettore_Orari_Giorno.Alle(ii - 1)
                                            Vettore_Orari_Giorno.DalleAlto(ii) = Vettore_Orari_Giorno.DalleAlto(ii - 1)
                                            Vettore_Orari_Giorno.AlleAlto(ii) = Vettore_Orari_Giorno.AlleAlto(ii - 1)
                                            Vettore_Orari_Giorno.DalleBasso(ii) = Vettore_Orari_Giorno.DalleBasso(ii - 1)
                                            Vettore_Orari_Giorno.AlleBasso(ii) = Vettore_Orari_Giorno.AlleBasso(ii - 1)
                                            Vettore_Orari_Giorno.DalleObbligo(ii) = Vettore_Orari_Giorno.DalleObbligo(ii - 1)
                                            Vettore_Orari_Giorno.AlleObbligo(ii) = Vettore_Orari_Giorno.AlleObbligo(ii - 1)
                                            Vettore_Orari_Giorno.Pausa(ii) = Vettore_Orari_Giorno.Pausa(ii - 1)
                                            Vettore_Orari_Giorno.Giustificativo(ii) = Vettore_Orari_Giorno.Giustificativo(ii - 1)
                                            Vettore_Orari_Giorno.NelGruppo(ii) = Vettore_Orari_Giorno.NelGruppo(ii - 1)
                                            Vettore_Orari_Giorno.Familiare(ii) = Vettore_Orari_Giorno.Familiare(ii - 1)
                                            Vettore_Orari_Giorno.CambioRichiesto(ii) = Vettore_Orari_Giorno.CambioRichiesto(ii - 1)
                                            Vettore_Orari_Giorno.Tipo(ii) = Vettore_Orari_Giorno.Tipo(ii - 1)
                                            Vettore_Orari_Giorno.TipoServizio(ii) = Vettore_Orari_Giorno.TipoServizio(ii - 1)
                                            Vettore_Orari_Giorno.GiornoSuccessivo(ii) = Vettore_Orari_Giorno.GiornoSuccessivo(ii - 1)
                                        Next ii
                                        Vettore_Orari_Giorno.Alle(i) = W_Dalle
                                        Vettore_Orari_Giorno.AlleAlto(i) = W_Dalle
                                        Vettore_Orari_Giorno.AlleBasso(i) = W_Dalle
                                        Vettore_Orari_Giorno.Dalle(i + 1) = W_Dalle
                                        Vettore_Orari_Giorno.DalleAlto(i + 1) = W_Dalle
                                        Vettore_Orari_Giorno.DalleBasso(i + 1) = W_Dalle
                                        Vettore_Orari_Giorno.Pausa(i + 1) = "00.00"
                                        Vettore_Orari_Giorno.Giustificativo(i + 1) = W_Giustificativo
                                        Vettore_Orari_Giorno.NelGruppo(i + 1) = W_NelGruppo
                                        Vettore_Orari_Giorno.Familiare(i + 1) = W_Familiare
                                        Vettore_Orari_Giorno.CambioRichiesto(i + 1) = W_CambioRichiesto
                                        Vettore_Orari_Giorno.Tipo(i + 1) = cg.Tipo
                                        Vettore_Orari_Giorno.Alle(i + 1) = W_Alle
                                        Vettore_Orari_Giorno.AlleAlto(i + 1) = W_Alle
                                        Vettore_Orari_Giorno.AlleBasso(i + 1) = W_Alle
                                        Vettore_Orari_Giorno.Dalle(i + 2) = W_Alle
                                        Vettore_Orari_Giorno.DalleAlto(i + 2) = W_Alle
                                        Vettore_Orari_Giorno.DalleBasso(i + 2) = W_Alle
                                        Vettore_Orari_Giorno.Pausa(i + 2) = "00.00"
                                        i = 14
                                    End If
                                End If

                            Else

                                If Vettore_Orari_Giorno.Dalle(i) = W_Dalle Then
                                    If Vettore_Orari_Giorno.Alle(i) < W_Alle Then
                                        Vettore_Orari_Giorno.Pausa(i) = "00.00"
                                        Vettore_Orari_Giorno.Giustificativo(i) = W_Giustificativo
                                        Vettore_Orari_Giorno.NelGruppo(i) = W_NelGruppo
                                        Vettore_Orari_Giorno.Familiare(i) = W_Familiare
                                        Vettore_Orari_Giorno.CambioRichiesto(i) = W_CambioRichiesto
                                        Vettore_Orari_Giorno.Tipo(i) = cg.Tipo
                                        W_Dalle = Vettore_Orari_Giorno.Alle(i)
                                        Vettore_Orari_Giorno.AlleAlto(i) = W_Dalle
                                        Vettore_Orari_Giorno.AlleBasso(i) = W_Dalle

                                    Else

                                        If Vettore_Orari_Giorno.Alle(i) = W_Alle Then
                                            Vettore_Orari_Giorno.Pausa(i) = "00.00"
                                            Vettore_Orari_Giorno.Giustificativo(i) = W_Giustificativo
                                            Vettore_Orari_Giorno.NelGruppo(i) = W_NelGruppo
                                            Vettore_Orari_Giorno.Familiare(i) = W_Familiare
                                            Vettore_Orari_Giorno.CambioRichiesto(i) = W_CambioRichiesto
                                            Vettore_Orari_Giorno.Tipo(i) = cg.Tipo
                                            i = 14

                                        Else

                                            For ii = 14 To i + 1 Step -1
                                                Vettore_Orari_Giorno.Dalle(ii) = Vettore_Orari_Giorno.Dalle(ii - 1)
                                                Vettore_Orari_Giorno.Alle(ii) = Vettore_Orari_Giorno.Alle(ii - 1)
                                                Vettore_Orari_Giorno.DalleAlto(ii) = Vettore_Orari_Giorno.DalleAlto(ii - 1)
                                                Vettore_Orari_Giorno.AlleAlto(ii) = Vettore_Orari_Giorno.AlleAlto(ii - 1)
                                                Vettore_Orari_Giorno.DalleBasso(ii) = Vettore_Orari_Giorno.DalleBasso(ii - 1)
                                                Vettore_Orari_Giorno.AlleBasso(ii) = Vettore_Orari_Giorno.AlleBasso(ii - 1)
                                                Vettore_Orari_Giorno.DalleObbligo(ii) = Vettore_Orari_Giorno.DalleObbligo(ii - 1)
                                                Vettore_Orari_Giorno.AlleObbligo(ii) = Vettore_Orari_Giorno.AlleObbligo(ii - 1)
                                                Vettore_Orari_Giorno.Pausa(ii) = Vettore_Orari_Giorno.Pausa(ii - 1)
                                                Vettore_Orari_Giorno.Giustificativo(ii) = Vettore_Orari_Giorno.Giustificativo(ii - 1)
                                                Vettore_Orari_Giorno.NelGruppo(ii) = Vettore_Orari_Giorno.NelGruppo(ii - 1)
                                                Vettore_Orari_Giorno.Familiare(ii) = Vettore_Orari_Giorno.Familiare(ii - 1)
                                                Vettore_Orari_Giorno.CambioRichiesto(ii) = Vettore_Orari_Giorno.CambioRichiesto(ii - 1)
                                                Vettore_Orari_Giorno.Tipo(ii) = Vettore_Orari_Giorno.Tipo(ii - 1)
                                                Vettore_Orari_Giorno.TipoServizio(ii) = Vettore_Orari_Giorno.TipoServizio(ii - 1)
                                                Vettore_Orari_Giorno.GiornoSuccessivo(ii) = Vettore_Orari_Giorno.GiornoSuccessivo(ii - 1)
                                            Next ii
                                            Vettore_Orari_Giorno.Alle(i) = W_Alle
                                            Vettore_Orari_Giorno.AlleAlto(i) = W_Alle
                                            Vettore_Orari_Giorno.AlleBasso(i) = W_Alle
                                            Vettore_Orari_Giorno.Pausa(i) = "00.00"
                                            Vettore_Orari_Giorno.Giustificativo(i) = W_Giustificativo
                                            Vettore_Orari_Giorno.NelGruppo(i) = W_NelGruppo
                                            Vettore_Orari_Giorno.Familiare(i) = W_Familiare
                                            Vettore_Orari_Giorno.CambioRichiesto(i) = W_CambioRichiesto
                                            Vettore_Orari_Giorno.Tipo(i) = cg.Tipo
                                            Vettore_Orari_Giorno.Dalle(i + 1) = W_Alle
                                            Vettore_Orari_Giorno.DalleAlto(i + 1) = W_Alle
                                            Vettore_Orari_Giorno.DalleBasso(i + 1) = W_Alle
                                            i = 14
                                        End If
                                    End If

                                Else

                                    For ii = 14 To i + 1 Step -1
                                        Vettore_Orari_Giorno.Dalle(ii) = Vettore_Orari_Giorno.Dalle(ii - 1)
                                        Vettore_Orari_Giorno.Alle(ii) = Vettore_Orari_Giorno.Alle(ii - 1)
                                        Vettore_Orari_Giorno.DalleAlto(ii) = Vettore_Orari_Giorno.DalleAlto(ii - 1)
                                        Vettore_Orari_Giorno.AlleAlto(ii) = Vettore_Orari_Giorno.AlleAlto(ii - 1)
                                        Vettore_Orari_Giorno.DalleBasso(ii) = Vettore_Orari_Giorno.DalleBasso(ii - 1)
                                        Vettore_Orari_Giorno.AlleBasso(ii) = Vettore_Orari_Giorno.AlleBasso(ii - 1)
                                        Vettore_Orari_Giorno.DalleObbligo(ii) = Vettore_Orari_Giorno.DalleObbligo(ii - 1)
                                        Vettore_Orari_Giorno.AlleObbligo(ii) = Vettore_Orari_Giorno.AlleObbligo(ii - 1)
                                        Vettore_Orari_Giorno.Pausa(ii) = Vettore_Orari_Giorno.Pausa(ii - 1)
                                        Vettore_Orari_Giorno.Giustificativo(ii) = Vettore_Orari_Giorno.Giustificativo(ii - 1)
                                        Vettore_Orari_Giorno.NelGruppo(ii) = Vettore_Orari_Giorno.NelGruppo(ii - 1)
                                        Vettore_Orari_Giorno.Familiare(ii) = Vettore_Orari_Giorno.Familiare(ii - 1)
                                        Vettore_Orari_Giorno.CambioRichiesto(ii) = Vettore_Orari_Giorno.CambioRichiesto(ii - 1)
                                        Vettore_Orari_Giorno.Tipo(ii) = Vettore_Orari_Giorno.Tipo(ii - 1)
                                        Vettore_Orari_Giorno.TipoServizio(ii) = Vettore_Orari_Giorno.TipoServizio(ii - 1)
                                        Vettore_Orari_Giorno.GiornoSuccessivo(ii) = Vettore_Orari_Giorno.GiornoSuccessivo(ii - 1)
                                    Next ii
                                    If Vettore_Orari_Giorno.Dalle(i) >= W_Alle Then
                                        Vettore_Orari_Giorno.Dalle(i) = W_Dalle
                                        Vettore_Orari_Giorno.DalleAlto(i) = W_Dalle
                                        Vettore_Orari_Giorno.DalleBasso(i) = W_Dalle
                                        Vettore_Orari_Giorno.Alle(i) = W_Alle
                                        Vettore_Orari_Giorno.AlleAlto(i) = W_Alle
                                        Vettore_Orari_Giorno.AlleBasso(i) = W_Alle
                                        Vettore_Orari_Giorno.Pausa(i) = "00.00"
                                        Vettore_Orari_Giorno.Giustificativo(i) = W_Giustificativo
                                        Vettore_Orari_Giorno.NelGruppo(i) = W_NelGruppo
                                        Vettore_Orari_Giorno.Familiare(i) = W_Familiare
                                        Vettore_Orari_Giorno.CambioRichiesto(i) = W_CambioRichiesto
                                        Vettore_Orari_Giorno.Tipo(i) = cg.Tipo
                                        i = 14

                                    Else

                                        Vettore_Orari_Giorno.Alle(i) = Vettore_Orari_Giorno.Dalle(i)
                                        Vettore_Orari_Giorno.AlleAlto(i) = Vettore_Orari_Giorno.Dalle(i)
                                        Vettore_Orari_Giorno.AlleBasso(i) = Vettore_Orari_Giorno.Dalle(i)
                                        Vettore_Orari_Giorno.Dalle(i) = W_Dalle
                                        Vettore_Orari_Giorno.DalleAlto(i) = W_Dalle
                                        Vettore_Orari_Giorno.DalleBasso(i) = W_Dalle
                                        Vettore_Orari_Giorno.Pausa(i) = "00.00"
                                        Vettore_Orari_Giorno.Giustificativo(i) = W_Giustificativo
                                        Vettore_Orari_Giorno.NelGruppo(i) = W_NelGruppo
                                        Vettore_Orari_Giorno.Familiare(i) = W_Familiare
                                        Vettore_Orari_Giorno.CambioRichiesto(i) = W_CambioRichiesto
                                        Vettore_Orari_Giorno.Tipo(i) = cg.Tipo
                                    End If
                                End If
                            End If
                        End If

                    Else

                        Vettore_Orari_Giorno.Dalle(i) = W_Dalle
                        Vettore_Orari_Giorno.DalleAlto(i) = W_Dalle
                        Vettore_Orari_Giorno.DalleBasso(i) = W_Dalle
                        Vettore_Orari_Giorno.Alle(i) = W_Alle
                        Vettore_Orari_Giorno.AlleAlto(i) = W_Alle
                        Vettore_Orari_Giorno.AlleBasso(i) = W_Alle
                        Vettore_Orari_Giorno.Pausa(i) = "00.00"
                        Vettore_Orari_Giorno.Giustificativo(i) = W_Giustificativo
                        Vettore_Orari_Giorno.NelGruppo(i) = W_NelGruppo
                        Vettore_Orari_Giorno.Familiare(i) = W_Familiare
                        Vettore_Orari_Giorno.CambioRichiesto(i) = W_CambioRichiesto
                        Vettore_Orari_Giorno.Tipo(i) = cg.Tipo
                        i = 14
                    End If
                Next i

            Else

                If Chk_Giorno(Weekday(Data) - 1) = "S" Then

                    If (cg.Assenza_RiposoSuFestivi = "S" And Weekday(Data) = 1) Or (cg.Assenza_RiposoSuFestiviInfrasettimanali = "S" And cxst.GiornoFestivo(Data, CodiceSuperGruppo) = True) Then

                        Vettore_Orari_Giorno.Richiesti = "S"

                        If cg.Assenza_RimuoviOrari = "S" Then
                            Vettore_Orari_Giorno.Dalle(0) = "00.00"
                            Vettore_Orari_Giorno.DalleAlto(0) = "00.00"
                            Vettore_Orari_Giorno.DalleBasso(0) = "00.00"
                            Vettore_Orari_Giorno.Alle(0) = "00.00"
                            Vettore_Orari_Giorno.AlleAlto(0) = "00.00"
                            Vettore_Orari_Giorno.AlleBasso(0) = "00.00"
                            Vettore_Orari_Giorno.Pausa(0) = "00.00"
                            Vettore_Orari_Giorno.Giustificativo(0) = cpt.GiustificativoRiposo
                            Vettore_Orari_Giorno.NelGruppo(0) = ""
                            Vettore_Orari_Giorno.Familiare(0) = 0
                            Vettore_Orari_Giorno.CambioRichiesto(0) = ""
                            Vettore_Orari_Giorno.Tipo(0) = cg.CampoGiustificativi(ConnectionString, cpt.GiustificativoRiposo, "TIPO")
                            For i = 1 To 14
                                Vettore_Orari_Giorno.Dalle(i) = "00.00"
                                Vettore_Orari_Giorno.Alle(i) = "00.00"
                                Vettore_Orari_Giorno.DalleAlto(i) = "00.00"
                                Vettore_Orari_Giorno.AlleAlto(i) = "00.00"
                                Vettore_Orari_Giorno.DalleBasso(i) = "00.00"
                                Vettore_Orari_Giorno.AlleBasso(i) = "00.00"
                                Vettore_Orari_Giorno.DalleObbligo(i) = "00.00"
                                Vettore_Orari_Giorno.AlleObbligo(i) = "00.00"
                                Vettore_Orari_Giorno.Pausa(i) = "00.00"
                                Vettore_Orari_Giorno.Giustificativo(i) = ""
                                Vettore_Orari_Giorno.NelGruppo(i) = ""
                                Vettore_Orari_Giorno.Familiare(i) = 0
                                Vettore_Orari_Giorno.CambioRichiesto(i) = ""
                                Vettore_Orari_Giorno.Tipo(i) = ""
                                Vettore_Orari_Giorno.TipoServizio(i) = ""
                                Vettore_Orari_Giorno.GiornoSuccessivo(i) = ""
                            Next i
                        Else
                            For i = 0 To 14
                                If Vettore_Orari_Giorno.Dalle(i) = "00.00" And _
                                   Vettore_Orari_Giorno.Alle(i) = "00.00" And _
                                   Vettore_Orari_Giorno.Pausa(i) = "00.00" And _
                                   Vettore_Orari_Giorno.Giustificativo(i) = "" Then Exit For
                                Vettore_Orari_Giorno.Giustificativo(i) = cpt.GiustificativoRiposo
                                Vettore_Orari_Giorno.NelGruppo(i) = ""
                                Vettore_Orari_Giorno.Familiare(i) = 0
                                Vettore_Orari_Giorno.CambioRichiesto(i) = ""
                                Vettore_Orari_Giorno.Tipo(i) = cg.CampoGiustificativi(ConnectionString, cpt.GiustificativoRiposo, "TIPO")
                            Next i
                        End If

                    Else

                        If Not (cg.Assenza_SuRiposo = "N" And Vettore_Orari_Giorno.Giustificativo(0) = cpt.GiustificativoRiposo) Then

                            Vettore_Orari_Giorno.Richiesti = "S"

                            If cg.Assenza_RimuoviOrari = "N" Then
                                For i = 0 To 14
                                    If Vettore_Orari_Giorno.Dalle(i) = "00.00" And _
                                       Vettore_Orari_Giorno.Alle(i) = "00.00" And _
                                       Vettore_Orari_Giorno.Pausa(i) = "00.00" And _
                                       Vettore_Orari_Giorno.Giustificativo(i) = "" Then Exit For
                                    Vettore_Orari_Giorno.Giustificativo(i) = W_Giustificativo
                                    Vettore_Orari_Giorno.NelGruppo(i) = W_NelGruppo
                                    Vettore_Orari_Giorno.Familiare(i) = W_Familiare
                                    Vettore_Orari_Giorno.CambioRichiesto(i) = W_CambioRichiesto
                                    Vettore_Orari_Giorno.Tipo(i) = cg.Tipo
                                Next i

                            Else

                                Vettore_Orari_Giorno.Dalle(0) = "00.00"
                                Vettore_Orari_Giorno.DalleAlto(0) = "00.00"
                                Vettore_Orari_Giorno.DalleBasso(0) = "00.00"
                                Vettore_Orari_Giorno.Alle(0) = "00.00"
                                Vettore_Orari_Giorno.AlleAlto(0) = "00.00"
                                Vettore_Orari_Giorno.AlleBasso(0) = "00.00"
                                Vettore_Orari_Giorno.Pausa(0) = "00.00"
                                Vettore_Orari_Giorno.Giustificativo(0) = W_Giustificativo
                                Vettore_Orari_Giorno.NelGruppo(0) = W_NelGruppo
                                Vettore_Orari_Giorno.Familiare(0) = W_Familiare
                                Vettore_Orari_Giorno.CambioRichiesto(0) = W_CambioRichiesto
                                Vettore_Orari_Giorno.Tipo(0) = cg.Tipo
                                For i = 1 To 14
                                    Vettore_Orari_Giorno.Dalle(i) = "00.00"
                                    Vettore_Orari_Giorno.Alle(i) = "00.00"
                                    Vettore_Orari_Giorno.DalleAlto(i) = "00.00"
                                    Vettore_Orari_Giorno.AlleAlto(i) = "00.00"
                                    Vettore_Orari_Giorno.DalleBasso(i) = "00.00"
                                    Vettore_Orari_Giorno.AlleBasso(i) = "00.00"
                                    Vettore_Orari_Giorno.DalleObbligo(i) = "00.00"
                                    Vettore_Orari_Giorno.AlleObbligo(i) = "00.00"
                                    Vettore_Orari_Giorno.Pausa(i) = "00.00"
                                    Vettore_Orari_Giorno.Giustificativo(i) = ""
                                    Vettore_Orari_Giorno.NelGruppo(i) = ""
                                    Vettore_Orari_Giorno.Familiare(i) = 0
                                    Vettore_Orari_Giorno.CambioRichiesto(i) = ""
                                    Vettore_Orari_Giorno.Tipo(i) = ""
                                    Vettore_Orari_Giorno.TipoServizio(i) = ""
                                    Vettore_Orari_Giorno.GiornoSuccessivo(i) = ""
                                Next i
                            End If
                        End If
                    End If
                End If
            End If
        Loop
        cn.Close()
    End Sub

    Private Sub Carica_Vettore_Orari_Giorno_RichiesteStraordinario(ByVal ConnectionString As String, ByVal Dipendente As Object, ByVal Data As Date, ByRef Vettore_Orari_Giorno As Cls_Struttura_Orari_Giorno)
        Dim cg As New Cls_Giustificativi
        Dim cpt As New Cls_ParametriTurni
        cpt.Leggi(ConnectionString)
        Dim cxst As New Cls_ClsXScriptTurni
        cxst.ConnectionString = ConnectionString

        Dim Chk_Giorno(6) As String
        Dim W_DataDal As Date
        Dim W_DataAl As Date
        Dim W_Dalle As String
        Dim W_Alle As String
        Dim W_Giustificativo As String
        Dim W_NelGruppo As String
        Dim W_Familiare As Byte
        Dim W_CambioRichiesto As String

        Dim Stringa As String

        Dim cn As OleDbConnection
        cn = New Data.OleDb.OleDbConnection(ConnectionString)

        cn.Open()
        Dim cmd As New OleDbCommand()

        cmd.CommandText = "SELECT * FROM RichiesteStraordinario" & _
                " WHERE CodiceDipendente = ?" & _
                " AND Acquisito = 'S'" & _
                " AND (Annullato Is Null OR Annullato = '')" & _
                " AND (Revocato Is Null OR Revocato = '')" & _
                " AND DataRichiesta = ?" & _
                " ORDER BY DataRichiesta, Dalle"
        cmd.Parameters.AddWithValue("@CodiceDipendente", Dipendente)
        cmd.Parameters.AddWithValue("@DataRichiesta", Data)

        cmd.Connection = cn
        Dim myPOSTreader As OleDbDataReader = cmd.ExecuteReader()
        Do While myPOSTreader.Read
            Vettore_Orari_Giorno.Richiesti = "S"
            W_Dalle = Format(DataDb(myPOSTreader.Item("Dalle")), "hh.nn")
            W_Alle = Format(DataDb(myPOSTreader.Item("Alle")), "hh.nn")
            W_Giustificativo = StringaDb(myPOSTreader.Item("Giustificativo"))
            cg.Leggi(ConnectionString, W_Giustificativo)
            W_NelGruppo = ""
            W_Familiare = 0
            W_CambioRichiesto = ""

            For i = 0 To 14
                If Vettore_Orari_Giorno.Giustificativo(i) <> "" Then
                    If Vettore_Orari_Giorno.Dalle(i) = "00.00" And Vettore_Orari_Giorno.Alle(i) = "00.00" Then
                        Vettore_Orari_Giorno.Dalle(i) = W_Dalle
                        Vettore_Orari_Giorno.DalleAlto(i) = W_Dalle
                        Vettore_Orari_Giorno.DalleBasso(i) = W_Dalle
                        Vettore_Orari_Giorno.Alle(i) = W_Alle
                        Vettore_Orari_Giorno.AlleAlto(i) = W_Alle
                        Vettore_Orari_Giorno.AlleBasso(i) = W_Alle
                        Vettore_Orari_Giorno.Pausa(i) = "00.00"
                        Vettore_Orari_Giorno.Giustificativo(i) = W_Giustificativo
                        Vettore_Orari_Giorno.NelGruppo(i) = W_NelGruppo
                        Vettore_Orari_Giorno.Familiare(i) = W_Familiare
                        Vettore_Orari_Giorno.CambioRichiesto(i) = W_CambioRichiesto
                        Vettore_Orari_Giorno.Tipo(i) = cg.Tipo
                        For ii = i + 1 To 14
                            Vettore_Orari_Giorno.Dalle(ii) = "00.00"
                            Vettore_Orari_Giorno.Alle(ii) = "00.00"
                            Vettore_Orari_Giorno.DalleAlto(ii) = "00.00"
                            Vettore_Orari_Giorno.AlleAlto(ii) = "00.00"
                            Vettore_Orari_Giorno.DalleBasso(ii) = "00.00"
                            Vettore_Orari_Giorno.AlleBasso(ii) = "00.00"
                            Vettore_Orari_Giorno.DalleObbligo(ii) = "00.00"
                            Vettore_Orari_Giorno.AlleObbligo(ii) = "00.00"
                            Vettore_Orari_Giorno.Pausa(ii) = "00.00"
                            Vettore_Orari_Giorno.Giustificativo(ii) = ""
                            Vettore_Orari_Giorno.NelGruppo(ii) = ""
                            Vettore_Orari_Giorno.Familiare(ii) = 0
                            Vettore_Orari_Giorno.CambioRichiesto(ii) = ""
                            Vettore_Orari_Giorno.Tipo(ii) = ""
                            Vettore_Orari_Giorno.TipoServizio(ii) = ""
                            Vettore_Orari_Giorno.GiornoSuccessivo(ii) = ""
                        Next ii
                        i = 14

                    Else

                        If Vettore_Orari_Giorno.Dalle(i) < W_Dalle Then
                            If Vettore_Orari_Giorno.Alle(i) > W_Dalle Then
                                For ii = 14 To i + 1 Step -1
                                    Vettore_Orari_Giorno.Dalle(ii) = Vettore_Orari_Giorno.Dalle(ii - 1)
                                    Vettore_Orari_Giorno.Alle(ii) = Vettore_Orari_Giorno.Alle(ii - 1)
                                    Vettore_Orari_Giorno.DalleAlto(ii) = Vettore_Orari_Giorno.DalleAlto(ii - 1)
                                    Vettore_Orari_Giorno.AlleAlto(ii) = Vettore_Orari_Giorno.AlleAlto(ii - 1)
                                    Vettore_Orari_Giorno.DalleBasso(ii) = Vettore_Orari_Giorno.DalleBasso(ii - 1)
                                    Vettore_Orari_Giorno.AlleBasso(ii) = Vettore_Orari_Giorno.AlleBasso(ii - 1)
                                    Vettore_Orari_Giorno.DalleObbligo(ii) = Vettore_Orari_Giorno.DalleObbligo(ii - 1)
                                    Vettore_Orari_Giorno.AlleObbligo(ii) = Vettore_Orari_Giorno.AlleObbligo(ii - 1)
                                    Vettore_Orari_Giorno.Pausa(ii) = Vettore_Orari_Giorno.Pausa(ii - 1)
                                    Vettore_Orari_Giorno.Giustificativo(ii) = Vettore_Orari_Giorno.Giustificativo(ii - 1)
                                    Vettore_Orari_Giorno.NelGruppo(ii) = Vettore_Orari_Giorno.NelGruppo(ii - 1)
                                    Vettore_Orari_Giorno.Familiare(ii) = Vettore_Orari_Giorno.Familiare(ii - 1)
                                    Vettore_Orari_Giorno.CambioRichiesto(ii) = Vettore_Orari_Giorno.CambioRichiesto(ii - 1)
                                    Vettore_Orari_Giorno.Tipo(ii) = Vettore_Orari_Giorno.Tipo(ii - 1)
                                    Vettore_Orari_Giorno.TipoServizio(ii) = Vettore_Orari_Giorno.TipoServizio(ii - 1)
                                    Vettore_Orari_Giorno.GiornoSuccessivo(ii) = Vettore_Orari_Giorno.GiornoSuccessivo(ii - 1)
                                Next ii
                                If Vettore_Orari_Giorno.Alle(i) <= W_Alle Then
                                    Vettore_Orari_Giorno.Alle(i) = W_Dalle
                                    Vettore_Orari_Giorno.AlleAlto(i) = W_Dalle
                                    Vettore_Orari_Giorno.AlleBasso(i) = W_Dalle
                                    Vettore_Orari_Giorno.Dalle(i + 1) = W_Dalle
                                    Vettore_Orari_Giorno.DalleAlto(i + 1) = W_Dalle
                                    Vettore_Orari_Giorno.DalleBasso(i + 1) = W_Dalle
                                    Vettore_Orari_Giorno.Pausa(i + 1) = "00.00"
                                    Vettore_Orari_Giorno.Giustificativo(i + 1) = W_Giustificativo
                                    Vettore_Orari_Giorno.NelGruppo(i + 1) = W_NelGruppo
                                    Vettore_Orari_Giorno.Familiare(i + 1) = W_Familiare
                                    Vettore_Orari_Giorno.CambioRichiesto(i + 1) = W_CambioRichiesto
                                    Vettore_Orari_Giorno.Tipo(i + 1) = cg.Tipo
                                    W_Dalle = Vettore_Orari_Giorno.Alle(i + 1)
                                    Vettore_Orari_Giorno.AlleAlto(i + 1) = W_Dalle
                                    Vettore_Orari_Giorno.AlleBasso(i + 1) = W_Dalle

                                Else

                                    For ii = 14 To i + 1 Step -1
                                        Vettore_Orari_Giorno.Dalle(ii) = Vettore_Orari_Giorno.Dalle(ii - 1)
                                        Vettore_Orari_Giorno.Alle(ii) = Vettore_Orari_Giorno.Alle(ii - 1)
                                        Vettore_Orari_Giorno.DalleAlto(ii) = Vettore_Orari_Giorno.DalleAlto(ii - 1)
                                        Vettore_Orari_Giorno.AlleAlto(ii) = Vettore_Orari_Giorno.AlleAlto(ii - 1)
                                        Vettore_Orari_Giorno.DalleBasso(ii) = Vettore_Orari_Giorno.DalleBasso(ii - 1)
                                        Vettore_Orari_Giorno.AlleBasso(ii) = Vettore_Orari_Giorno.AlleBasso(ii - 1)
                                        Vettore_Orari_Giorno.DalleObbligo(ii) = Vettore_Orari_Giorno.DalleObbligo(ii - 1)
                                        Vettore_Orari_Giorno.AlleObbligo(ii) = Vettore_Orari_Giorno.AlleObbligo(ii - 1)
                                        Vettore_Orari_Giorno.Pausa(ii) = Vettore_Orari_Giorno.Pausa(ii - 1)
                                        Vettore_Orari_Giorno.Giustificativo(ii) = Vettore_Orari_Giorno.Giustificativo(ii - 1)
                                        Vettore_Orari_Giorno.NelGruppo(ii) = Vettore_Orari_Giorno.NelGruppo(ii - 1)
                                        Vettore_Orari_Giorno.Familiare(ii) = Vettore_Orari_Giorno.Familiare(ii - 1)
                                        Vettore_Orari_Giorno.CambioRichiesto(ii) = Vettore_Orari_Giorno.CambioRichiesto(ii - 1)
                                        Vettore_Orari_Giorno.Tipo(ii) = Vettore_Orari_Giorno.Tipo(ii - 1)
                                        Vettore_Orari_Giorno.TipoServizio(ii) = Vettore_Orari_Giorno.TipoServizio(ii - 1)
                                        Vettore_Orari_Giorno.GiornoSuccessivo(ii) = Vettore_Orari_Giorno.GiornoSuccessivo(ii - 1)
                                    Next ii
                                    Vettore_Orari_Giorno.Alle(i) = W_Dalle
                                    Vettore_Orari_Giorno.AlleAlto(i) = W_Dalle
                                    Vettore_Orari_Giorno.AlleBasso(i) = W_Dalle
                                    Vettore_Orari_Giorno.Dalle(i + 1) = W_Dalle
                                    Vettore_Orari_Giorno.DalleAlto(i + 1) = W_Dalle
                                    Vettore_Orari_Giorno.DalleBasso(i + 1) = W_Dalle
                                    Vettore_Orari_Giorno.Pausa(i + 1) = "00.00"
                                    Vettore_Orari_Giorno.Giustificativo(i + 1) = W_Giustificativo
                                    Vettore_Orari_Giorno.NelGruppo(i + 1) = W_NelGruppo
                                    Vettore_Orari_Giorno.Familiare(i + 1) = W_Familiare
                                    Vettore_Orari_Giorno.CambioRichiesto(i + 1) = W_CambioRichiesto
                                    Vettore_Orari_Giorno.Tipo(i + 1) = cg.Tipo
                                    Vettore_Orari_Giorno.Alle(i + 1) = W_Alle
                                    Vettore_Orari_Giorno.AlleAlto(i + 1) = W_Alle
                                    Vettore_Orari_Giorno.AlleBasso(i + 1) = W_Alle
                                    Vettore_Orari_Giorno.Dalle(i + 2) = W_Alle
                                    Vettore_Orari_Giorno.DalleAlto(i + 2) = W_Alle
                                    Vettore_Orari_Giorno.DalleBasso(i + 2) = W_Alle
                                    Vettore_Orari_Giorno.Pausa(i + 2) = "00.00"
                                    i = 14
                                End If
                            End If

                        Else

                            If Vettore_Orari_Giorno.Dalle(i) = W_Dalle Then
                                If Vettore_Orari_Giorno.Alle(i) < W_Alle Then
                                    Vettore_Orari_Giorno.Pausa(i) = "00.00"
                                    Vettore_Orari_Giorno.Giustificativo(i) = W_Giustificativo
                                    Vettore_Orari_Giorno.NelGruppo(i) = W_NelGruppo
                                    Vettore_Orari_Giorno.Familiare(i) = W_Familiare
                                    Vettore_Orari_Giorno.CambioRichiesto(i) = W_CambioRichiesto
                                    Vettore_Orari_Giorno.Tipo(i) = cg.Tipo
                                    W_Dalle = Vettore_Orari_Giorno.Alle(i)
                                    Vettore_Orari_Giorno.AlleAlto(i) = W_Dalle
                                    Vettore_Orari_Giorno.AlleBasso(i) = W_Dalle

                                Else

                                    If Vettore_Orari_Giorno.Alle(i) = W_Alle Then
                                        Vettore_Orari_Giorno.Pausa(i) = "00.00"
                                        Vettore_Orari_Giorno.Giustificativo(i) = W_Giustificativo
                                        Vettore_Orari_Giorno.NelGruppo(i) = W_NelGruppo
                                        Vettore_Orari_Giorno.Familiare(i) = W_Familiare
                                        Vettore_Orari_Giorno.CambioRichiesto(i) = W_CambioRichiesto
                                        Vettore_Orari_Giorno.Tipo(i) = cg.Tipo
                                        i = 14

                                    Else

                                        For ii = 14 To i + 1 Step -1
                                            Vettore_Orari_Giorno.Dalle(ii) = Vettore_Orari_Giorno.Dalle(ii - 1)
                                            Vettore_Orari_Giorno.Alle(ii) = Vettore_Orari_Giorno.Alle(ii - 1)
                                            Vettore_Orari_Giorno.DalleAlto(ii) = Vettore_Orari_Giorno.DalleAlto(ii - 1)
                                            Vettore_Orari_Giorno.AlleAlto(ii) = Vettore_Orari_Giorno.AlleAlto(ii - 1)
                                            Vettore_Orari_Giorno.DalleBasso(ii) = Vettore_Orari_Giorno.DalleBasso(ii - 1)
                                            Vettore_Orari_Giorno.AlleBasso(ii) = Vettore_Orari_Giorno.AlleBasso(ii - 1)
                                            Vettore_Orari_Giorno.DalleObbligo(ii) = Vettore_Orari_Giorno.DalleObbligo(ii - 1)
                                            Vettore_Orari_Giorno.AlleObbligo(ii) = Vettore_Orari_Giorno.AlleObbligo(ii - 1)
                                            Vettore_Orari_Giorno.Pausa(ii) = Vettore_Orari_Giorno.Pausa(ii - 1)
                                            Vettore_Orari_Giorno.Giustificativo(ii) = Vettore_Orari_Giorno.Giustificativo(ii - 1)
                                            Vettore_Orari_Giorno.NelGruppo(ii) = Vettore_Orari_Giorno.NelGruppo(ii - 1)
                                            Vettore_Orari_Giorno.Familiare(ii) = Vettore_Orari_Giorno.Familiare(ii - 1)
                                            Vettore_Orari_Giorno.CambioRichiesto(ii) = Vettore_Orari_Giorno.CambioRichiesto(ii - 1)
                                            Vettore_Orari_Giorno.Tipo(ii) = Vettore_Orari_Giorno.Tipo(ii - 1)
                                            Vettore_Orari_Giorno.TipoServizio(ii) = Vettore_Orari_Giorno.TipoServizio(ii - 1)
                                            Vettore_Orari_Giorno.GiornoSuccessivo(ii) = Vettore_Orari_Giorno.GiornoSuccessivo(ii - 1)
                                        Next ii
                                        Vettore_Orari_Giorno.Alle(i) = W_Alle
                                        Vettore_Orari_Giorno.AlleAlto(i) = W_Alle
                                        Vettore_Orari_Giorno.AlleBasso(i) = W_Alle
                                        Vettore_Orari_Giorno.Pausa(i + 1) = "00.00"
                                        Vettore_Orari_Giorno.Giustificativo(i + 1) = W_Giustificativo
                                        Vettore_Orari_Giorno.NelGruppo(i + 1) = W_NelGruppo
                                        Vettore_Orari_Giorno.Familiare(i + 1) = W_Familiare
                                        Vettore_Orari_Giorno.CambioRichiesto(i + 1) = W_CambioRichiesto
                                        Vettore_Orari_Giorno.Tipo(i + 1) = cg.Tipo
                                        Vettore_Orari_Giorno.Dalle(i + 1) = W_Alle
                                        Vettore_Orari_Giorno.DalleAlto(i + 1) = W_Alle
                                        Vettore_Orari_Giorno.DalleBasso(i + 1) = W_Alle
                                        i = 14
                                    End If
                                End If

                            Else

                                For ii = 14 To i + 1 Step -1
                                    Vettore_Orari_Giorno.Dalle(ii) = Vettore_Orari_Giorno.Dalle(ii - 1)
                                    Vettore_Orari_Giorno.Alle(ii) = Vettore_Orari_Giorno.Alle(ii - 1)
                                    Vettore_Orari_Giorno.DalleAlto(ii) = Vettore_Orari_Giorno.DalleAlto(ii - 1)
                                    Vettore_Orari_Giorno.AlleAlto(ii) = Vettore_Orari_Giorno.AlleAlto(ii - 1)
                                    Vettore_Orari_Giorno.DalleBasso(ii) = Vettore_Orari_Giorno.DalleBasso(ii - 1)
                                    Vettore_Orari_Giorno.AlleBasso(ii) = Vettore_Orari_Giorno.AlleBasso(ii - 1)
                                    Vettore_Orari_Giorno.DalleObbligo(ii) = Vettore_Orari_Giorno.DalleObbligo(ii - 1)
                                    Vettore_Orari_Giorno.AlleObbligo(ii) = Vettore_Orari_Giorno.AlleObbligo(ii - 1)
                                    Vettore_Orari_Giorno.Pausa(ii) = Vettore_Orari_Giorno.Pausa(ii - 1)
                                    Vettore_Orari_Giorno.Giustificativo(ii) = Vettore_Orari_Giorno.Giustificativo(ii - 1)
                                    Vettore_Orari_Giorno.NelGruppo(ii) = Vettore_Orari_Giorno.NelGruppo(ii - 1)
                                    Vettore_Orari_Giorno.Familiare(ii) = Vettore_Orari_Giorno.Familiare(ii - 1)
                                    Vettore_Orari_Giorno.CambioRichiesto(ii) = Vettore_Orari_Giorno.CambioRichiesto(ii - 1)
                                    Vettore_Orari_Giorno.Tipo(ii) = Vettore_Orari_Giorno.Tipo(ii - 1)
                                    Vettore_Orari_Giorno.TipoServizio(ii) = Vettore_Orari_Giorno.TipoServizio(ii - 1)
                                    Vettore_Orari_Giorno.GiornoSuccessivo(ii) = Vettore_Orari_Giorno.GiornoSuccessivo(ii - 1)
                                Next ii
                                If Vettore_Orari_Giorno.Dalle(i) >= W_Alle Then
                                    Vettore_Orari_Giorno.Dalle(i) = W_Dalle
                                    Vettore_Orari_Giorno.DalleAlto(i) = W_Dalle
                                    Vettore_Orari_Giorno.DalleBasso(i) = W_Dalle
                                    Vettore_Orari_Giorno.Alle(i) = W_Alle
                                    Vettore_Orari_Giorno.AlleAlto(i) = W_Alle
                                    Vettore_Orari_Giorno.AlleBasso(i) = W_Alle
                                    Vettore_Orari_Giorno.Pausa(i) = "00.00"
                                    Vettore_Orari_Giorno.Giustificativo(i) = W_Giustificativo
                                    Vettore_Orari_Giorno.NelGruppo(i) = W_NelGruppo
                                    Vettore_Orari_Giorno.Familiare(i) = W_Familiare
                                    Vettore_Orari_Giorno.CambioRichiesto(i) = W_CambioRichiesto
                                    Vettore_Orari_Giorno.Tipo(i) = cg.Tipo
                                    i = 14

                                Else

                                    Vettore_Orari_Giorno.Alle(i) = Vettore_Orari_Giorno.Dalle(i)
                                    Vettore_Orari_Giorno.AlleAlto(i) = Vettore_Orari_Giorno.Dalle(i)
                                    Vettore_Orari_Giorno.AlleBasso(i) = Vettore_Orari_Giorno.Dalle(i)
                                    Vettore_Orari_Giorno.Dalle(i) = W_Dalle
                                    Vettore_Orari_Giorno.DalleAlto(i) = W_Dalle
                                    Vettore_Orari_Giorno.DalleBasso(i) = W_Dalle
                                    Vettore_Orari_Giorno.Pausa(i) = "00.00"
                                    Vettore_Orari_Giorno.Giustificativo(i) = W_Giustificativo
                                    Vettore_Orari_Giorno.NelGruppo(i) = W_NelGruppo
                                    Vettore_Orari_Giorno.Familiare(i) = W_Familiare
                                    Vettore_Orari_Giorno.CambioRichiesto(i) = W_CambioRichiesto
                                    Vettore_Orari_Giorno.Tipo(i) = cg.Tipo
                                End If
                            End If
                        End If
                    End If

                Else

                    If i > 0 Then
                        Vettore_Orari_Giorno.AlleBasso(i - 1) = W_Dalle
                    End If
                    Vettore_Orari_Giorno.Dalle(i) = W_Dalle
                    Vettore_Orari_Giorno.DalleAlto(i) = W_Dalle
                    Vettore_Orari_Giorno.DalleBasso(i) = W_Dalle
                    Vettore_Orari_Giorno.Alle(i) = W_Alle
                    Vettore_Orari_Giorno.AlleAlto(i) = W_Alle
                    Vettore_Orari_Giorno.AlleBasso(i) = W_Alle
                    Vettore_Orari_Giorno.Pausa(i) = "00.00"
                    Vettore_Orari_Giorno.Giustificativo(i) = W_Giustificativo
                    Vettore_Orari_Giorno.NelGruppo(i) = W_NelGruppo
                    Vettore_Orari_Giorno.Familiare(i) = W_Familiare
                    Vettore_Orari_Giorno.CambioRichiesto(i) = W_CambioRichiesto
                    Vettore_Orari_Giorno.Tipo(i) = cg.Tipo
                    i = 14
                End If
            Next i
        Loop
        cn.Close()
    End Sub

    '------------------------------------------------------------
    'procedura di sviluppo profili orari per 42 giorni
    '------------------------------------------------------------

    Public Function SviluppoProfiliOrari(ByVal ConnectionString As String, ByVal CodiceSuperGruppo As String, ByVal CodiceContratto As String, ByVal ProfiloOrario As String, ByVal DataSviluppo As Date, ByRef Vettore_Orari As Cls_Struttura_Orari) As String

        Dim co As New Cls_Orari
        Dim cspor As New Cls_StrutturaProfiliOrariRiga
        Dim cpor As New Cls_ProfiliOrariRiga
        Dim cxst As New Cls_ClsXScriptTurni
        cxst.ConnectionString = ConnectionString

        Dim Assunzione(41) As String
        Dim CodiceTurno(41) As String
        Dim DataCodiceTurno(41) As Date
        Dim CodiceTurnoTesta(41) As String
        Dim CodiceStrutturaTesta(41) As String
        Dim PrimoElementoTesta(41) As Integer
        Dim DataCodiceTurnoTesta(41) As Date
        Dim OrarioGiornoFestivo(41) As String
        Dim W_InAlto(41) As Integer
        Dim W_InBasso(41) As Integer
        Dim Contatore As Long

        Dim g_min As Byte = 0
        Dim g_max As Byte = 41
        Dim gg As Byte

        Dim DataMin As Date = DateAdd("d", g_min, DataSviluppo)
        Dim DataMax As Date = DateAdd("d", g_max, DataSviluppo)

        Dim d As Byte = Weekday(DataMin)
        For g = g_min To g_max
            '    PrimoOrario(g) = ""
            '    SecondoOrario(g) = ""
            '    TerzoOrario(g) = ""
            '    QuartoOrario(g) = ""
            '    QuintoOrario(g) = ""

            Call Azzera_Vettore_Orari(g, d, Vettore_Orari)

            d = d + 1
            If d > 7 Then d = 1
            W_InAlto(g) = 0
            W_InBasso(g) = 0
        Next g

        Dim ccc As New Cls_CondizioniContrattuali
        ccc.Trova(ConnectionString, CodiceContratto, DataMax)
        If ccc.Id = 0 Then
            SviluppoProfiliOrari = "(E) Condizioni Contrattuali per il Contratto: " & CodiceContratto & "  inesistenti"
            Exit Function
        End If
        Dim FestivitaSettimanale As Byte = ccc.FestivitaSettimanale

        CodiceTurno(g_min) = ProfiloOrario

        Dim J As Integer
        Dim i As Integer = g_min

        Dim Oggi As Date = DateAdd("d", i, DataMin)

        Dim cn As OleDbConnection
        cn = New Data.OleDb.OleDbConnection(ConnectionString)

        cn.Open()
        Dim cmd As New OleDbCommand()
        cmd.CommandText = "SELECT * FROM ProfiliOrariTesta" & _
                          " WHERE Codice = ?" & _
                          " AND Validita <= ?" & _
                          " ORDER BY Validita DESC"
        cmd.Parameters.AddWithValue("@Codice", CodiceTurno(i))
        cmd.Parameters.AddWithValue("@Validita", DataMax)
        cmd.Connection = cn

        Dim myPOSTreader As OleDbDataReader = cmd.ExecuteReader()
        Do While myPOSTreader.Read
            If Format(DataDb(myPOSTreader.Item("Validita")), "yyyyMMdd") > Format(Oggi, "yyyyMMdd") Then
                gg = DateDiff("d", DataSviluppo, DataDb(myPOSTreader.Item("Validita")))
                For J = gg To g_max
                    If CodiceTurnoTesta(J) = "" Then
                        CodiceStrutturaTesta(J) = StringaDb(myPOSTreader.Item("CodiceStruttura"))
                        PrimoElementoTesta(J) = NumeroDb(myPOSTreader.Item("PrimoElemento"))
                        CodiceTurnoTesta(J) = StringaDb(myPOSTreader.Item("Codice"))
                        DataCodiceTurnoTesta(J) = DataDb(myPOSTreader.Item("Validita"))
                        OrarioGiornoFestivo(J) = StringaDb(myPOSTreader.Item("OrarioGiornoFestivo"))
                    Else
                        Exit For
                    End If
                Next J
            Else
                For J = i To g_max
                    If CodiceTurnoTesta(J) = "" Then
                        CodiceStrutturaTesta(J) = StringaDb(myPOSTreader.Item("CodiceStruttura"))
                        PrimoElementoTesta(J) = NumeroDb(myPOSTreader.Item("PrimoElemento"))
                        CodiceTurnoTesta(J) = StringaDb(myPOSTreader.Item("Codice"))
                        DataCodiceTurnoTesta(J) = DataDb(myPOSTreader.Item("Validita"))
                        OrarioGiornoFestivo(J) = StringaDb(myPOSTreader.Item("OrarioGiornoFestivo"))
                    Else
                        Exit For
                    End If
                Next J
                Exit Do
            End If
        Loop
        myPOSTreader.Close()
        cmd.Parameters.Clear()

        For i = g_min To g_max
            If CodiceTurnoTesta(i) = "VUOTO" Then
                CodiceStrutturaTesta(i) = ""
                PrimoElementoTesta(i) = 0
                CodiceTurnoTesta(i) = ""
                DataCodiceTurnoTesta(i) = DateSerial(1899, 12, 30)
                OrarioGiornoFestivo(i) = ""
            End If
        Next i

        If co.Conta(ConnectionString) = 0 Then
            SviluppoProfiliOrari = ProfiloOrario & " (E) Nessun orario in tabella" & vbNewLine
            Exit Function
        End If

        Dim W_CodiceTurnoTesta As String = ""
        Dim W_DataCodiceTurnoTesta As Date = DateSerial(1899, 12, 30)
        Dim CodTur As String
        Dim Giorni As Integer
        Dim Resto As Integer
        Dim Riga As Integer
        Dim Orario As String

        For i = g_min To g_max
            If CodiceTurnoTesta(i) <> "" Then
                If CodiceTurnoTesta(i) <> W_CodiceTurnoTesta Or Format(DataCodiceTurnoTesta(i), "yyyyMMdd") <> W_DataCodiceTurnoTesta Then
                    If W_CodiceTurnoTesta <> "" Then
                        myPOSTreader.Close()
                        cmd.Parameters.Clear()
                    End If
                    W_CodiceTurnoTesta = CodiceTurnoTesta(i)
                    W_DataCodiceTurnoTesta = Format(DataCodiceTurnoTesta(i), "yyyyMMdd")

                    Riga = 1
                    If CodiceStrutturaTesta(i) <> "" Then
                        CodTur = CodiceStrutturaTesta(i)
                        Contatore = cspor.Conta(ConnectionString, CodiceStrutturaTesta(i))
                    Else
                        CodTur = CodiceTurnoTesta(i)
                        Contatore = cpor.Conta(ConnectionString, CodiceTurnoTesta(i), DataCodiceTurnoTesta(i))
                    End If
                    If Contatore = 0 Then
                        SviluppoProfiliOrari = SviluppoProfiliOrari & ProfiloOrario & " (E) Profilo Orario '" & CodTur & "' senza righe" & vbNewLine
                        Exit Function
                    End If

                    If CodiceStrutturaTesta(i) <> "" Then
                        cmd.CommandText = "SELECT *, 'A' + RIGHT('00000'+ CONVERT(VARCHAR,Riga),5) As Selezione FROM StrutturaProfiliOrariRiga" & _
                                          " WHERE Codice = ?" & _
                                          " AND Riga >= ?" & _
                                          " UNION " & _
                                          "SELECT *, 'B' + RIGHT('00000'+ CONVERT(VARCHAR,Riga),5) As Selezione FROM StrutturaProfiliOrariRiga" & _
                                          " WHERE Codice = ?" & _
                                          " AND Riga < ?" & _
                                          " ORDER BY Selezione "
                        cmd.Parameters.AddWithValue("@Codice", CodiceStrutturaTesta(i))
                        cmd.Parameters.AddWithValue("@Riga", PrimoElementoTesta(i))
                        cmd.Parameters.AddWithValue("@Codice", CodiceStrutturaTesta(i))
                        cmd.Parameters.AddWithValue("@Riga", PrimoElementoTesta(i))
                    Else
                        cmd.CommandText = "SELECT * FROM ProfiliOrariRiga" & _
                                          " WHERE Codice = ?" & _
                                          " AND Validita = ?" & _
                                          " ORDER BY Riga "
                        cmd.Parameters.AddWithValue("@Codice", CodiceTurnoTesta(i))
                        cmd.Parameters.AddWithValue("@Validita", DataCodiceTurnoTesta(i))
                    End If

                    Oggi = DateAdd("d", i, DataMin)
                    Giorni = DateDiff("d", DataCodiceTurnoTesta(i), Oggi)
                    Giorni = Giorni + 1
                    Resto = Giorni - (Int(Giorni / Contatore) * Contatore)
                    If Resto = 0 Then
                        Resto = Contatore
                    End If

                    cmd.Connection = cn
                    myPOSTreader = cmd.ExecuteReader()
                    Do Until Not myPOSTreader.Read Or Riga = Resto
                        Riga = Riga + 1
                    Loop
                Else
                    Riga = Riga + 1
                    If Not myPOSTreader.Read Then
                        Riga = 1
                        myPOSTreader = cmd.ExecuteReader()
                        myPOSTreader.Read()
                    End If
                End If
            End If
            If Vettore_Orari.Variati(i) = "" Then
                If CodiceTurnoTesta(i) <> "" Then
                    If OrarioGiornoFestivo(i) <> "" And (Weekday(DateAdd("d", i, DataMin)) = FestivitaSettimanale Or cxst.GiornoFestivo(DateAdd("d", i, DataMin), CodiceSuperGruppo) = True) Then
                        co.Leggi(ConnectionString, OrarioGiornoFestivo(i))
                        Call CaricaVettoreOrari(ConnectionString, 0, i, W_InAlto(i), W_InBasso(i), Vettore_Orari, co)

                    Else

                        If StringaDb(myPOSTreader.Item("PrimoOrario")) <> "" Then
                            co.Leggi(ConnectionString, StringaDb(myPOSTreader.Item("PrimoOrario")))

                            If co.OrarioGiornoFestivo <> "" And (Weekday(DateAdd("d", i, DataMin)) = FestivitaSettimanale Or cxst.GiornoFestivo(DateAdd("d", i, DataMin), CodiceSuperGruppo) = True) Then
                                Orario = co.OrarioGiornoFestivo
                                co.Leggi(ConnectionString, Orario)
                            End If

                            Call CaricaVettoreOrari(ConnectionString, 0, i, W_InAlto(i), W_InBasso(i), Vettore_Orari, co)

                        End If

                        If StringaDb(myPOSTreader.Item("SecondoOrario")) <> "" Then
                            co.Leggi(ConnectionString, StringaDb(myPOSTreader.Item("SecondoOrario")))

                            If co.OrarioGiornoFestivo <> "" And (Weekday(DateAdd("d", i, DataMin)) = FestivitaSettimanale Or cxst.GiornoFestivo(DateAdd("d", i, DataMin), CodiceSuperGruppo) = True) Then
                                Orario = co.OrarioGiornoFestivo
                                co.Leggi(ConnectionString, Orario)
                            End If

                            Call CaricaVettoreOrari(ConnectionString, 1, i, W_InAlto(i), W_InBasso(i), Vettore_Orari, co)

                        End If

                        If StringaDb(myPOSTreader.Item("TerzoOrario")) <> "" Then
                            co.Leggi(ConnectionString, StringaDb(myPOSTreader.Item("TerzoOrario")))

                            If co.OrarioGiornoFestivo <> "" And (Weekday(DateAdd("d", i, DataMin)) = FestivitaSettimanale Or cxst.GiornoFestivo(DateAdd("d", i, DataMin), CodiceSuperGruppo) = True) Then
                                Orario = co.OrarioGiornoFestivo
                                co.Leggi(ConnectionString, Orario)
                            End If

                            Call CaricaVettoreOrari(ConnectionString, 2, i, W_InAlto(i), W_InBasso(i), Vettore_Orari, co)

                        End If

                        If StringaDb(myPOSTreader.Item("QuartoOrario")) <> "" Then
                            co.Leggi(ConnectionString, StringaDb(myPOSTreader.Item("QuartoOrario")))

                            If co.OrarioGiornoFestivo <> "" And (Weekday(DateAdd("d", i, DataMin)) = FestivitaSettimanale Or cxst.GiornoFestivo(DateAdd("d", i, DataMin), CodiceSuperGruppo) = True) Then
                                Orario = co.OrarioGiornoFestivo
                                co.Leggi(ConnectionString, Orario)
                            End If

                            Call CaricaVettoreOrari(ConnectionString, 3, i, W_InAlto(i), W_InBasso(i), Vettore_Orari, co)

                        End If

                        If StringaDb(myPOSTreader.Item("QuintoOrario")) <> "" Then
                            co.Leggi(ConnectionString, StringaDb(myPOSTreader.Item("QuintoOrario")))

                            If co.OrarioGiornoFestivo <> "" And (Weekday(DateAdd("d", i, DataMin)) = FestivitaSettimanale Or cxst.GiornoFestivo(DateAdd("d", i, DataMin), CodiceSuperGruppo) = True) Then
                                Orario = co.OrarioGiornoFestivo
                                co.Leggi(ConnectionString, Orario)
                            End If

                            Call CaricaVettoreOrari(ConnectionString, 4, i, W_InAlto(i), W_InBasso(i), Vettore_Orari, co)

                        End If
                    End If
                End If

            Else

                If Vettore_Orari.PrimoOrario(i) <> "" Then
                    co.Leggi(ConnectionString, Vettore_Orari.PrimoOrario(i))

                    If co.OrarioGiornoFestivo <> "" And (Weekday(DateAdd("d", i, DataMin)) = FestivitaSettimanale Or cxst.GiornoFestivo(DateAdd("d", i, DataMin), CodiceSuperGruppo) = True) Then
                        Orario = co.OrarioGiornoFestivo
                        co.Leggi(ConnectionString, Orario)
                    End If

                    Call CaricaVettoreOrari(ConnectionString, 0, i, W_InAlto(i), W_InBasso(i), Vettore_Orari, co)
                End If
                If Vettore_Orari.SecondoOrario(i) <> "" Then
                    co.Leggi(ConnectionString, Vettore_Orari.SecondoOrario(i))

                    If co.OrarioGiornoFestivo <> "" And (Weekday(DateAdd("d", i, DataMin)) = FestivitaSettimanale Or cxst.GiornoFestivo(DateAdd("d", i, DataMin), CodiceSuperGruppo) = True) Then
                        Orario = co.OrarioGiornoFestivo
                        co.Leggi(ConnectionString, Orario)
                    End If

                    Call CaricaVettoreOrari(ConnectionString, 1, i, W_InAlto(i), W_InBasso(i), Vettore_Orari, co)
                End If
                If Vettore_Orari.TerzoOrario(i) <> "" Then
                    co.Leggi(ConnectionString, Vettore_Orari.TerzoOrario(i))

                    If co.OrarioGiornoFestivo <> "" And (Weekday(DateAdd("d", i, DataMin)) = FestivitaSettimanale Or cxst.GiornoFestivo(DateAdd("d", i, DataMin), CodiceSuperGruppo) = True) Then
                        Orario = co.OrarioGiornoFestivo
                        co.Leggi(ConnectionString, Orario)
                    End If

                    Call CaricaVettoreOrari(ConnectionString, 2, i, W_InAlto(i), W_InBasso(i), Vettore_Orari, co)
                End If
                If Vettore_Orari.QuartoOrario(i) <> "" Then
                    co.Leggi(ConnectionString, Vettore_Orari.QuartoOrario(i))

                    If co.OrarioGiornoFestivo <> "" And (Weekday(DateAdd("d", i, DataMin)) = FestivitaSettimanale Or cxst.GiornoFestivo(DateAdd("d", i, DataMin), CodiceSuperGruppo) = True) Then
                        Orario = co.OrarioGiornoFestivo
                        co.Leggi(ConnectionString, Orario)
                    End If

                    Call CaricaVettoreOrari(ConnectionString, 3, i, W_InAlto(i), W_InBasso(i), Vettore_Orari, co)
                End If
                If Vettore_Orari.QuintoOrario(i) <> "" Then
                    co.Leggi(ConnectionString, Vettore_Orari.QuintoOrario(i))

                    If co.OrarioGiornoFestivo <> "" And (Weekday(DateAdd("d", i, DataMin)) = FestivitaSettimanale Or cxst.GiornoFestivo(DateAdd("d", i, DataMin), CodiceSuperGruppo) = True) Then
                        Orario = co.OrarioGiornoFestivo
                        co.Leggi(ConnectionString, Orario)
                    End If

                    Call CaricaVettoreOrari(ConnectionString, 4, i, W_InAlto(i), W_InBasso(i), Vettore_Orari, co)
                End If
            End If
        Next i

        If W_CodiceTurnoTesta <> "" Then
            myPOSTreader.Close()
            cmd.Parameters.Clear()
        End If
    End Function

    '------------------------------------------------------------
    'procedura di sviluppo turni per un giorno con ordini servizio sul Vettore_Orari_Giorno
    '------------------------------------------------------------

    Public Function SviluppoTurniOrdiniServizioSingoloGiorno(ByVal ConnectionString As String, ByVal Dipendente As Long, ByVal Data As Date, ByVal ZeroAlle As String, ByRef Vettore_Orari_Giorno As Cls_Struttura_Orari_Giorno, ByRef Vettore_Timbrature_Giorno As Cls_Struttura_Timbrature_Giorno, ByVal Sw_Profilo As String, ByVal Sw_ProfiloFerie As String) As String
        Dim CodiceProfiloFerie As String
        Dim CodiceTurno As String
        Dim W_InAlto As Integer
        Dim W_InBasso As Integer
        Dim W_OrarioDalle(10) As String
        Dim W_OrarioDalleObbligo(10) As String
        Dim W_OrarioAlle(10) As String
        Dim W_OrarioAlleObbligo(10) As String
        Dim W_OrarioGiustificativo(10) As String
        Dim W_OrarioNelGruppo(10) As String
        Dim W_OrarioFamiliare(10) As Byte
        Dim W_OrarioCambioRichiesto(10) As String
        Dim W_OrarioDifferenzaDalle(10) As Long
        Dim W_OrarioDifferenzaAlle(10) As Long
        Dim W_OrarioNotte As String
        Dim W_OrarioSmontoNotte As String
        Dim Contatore As Long
        Dim Chk_Giorno(6) As String
        Dim i As Integer
        Dim Sw_Ok As Boolean

        SviluppoTurniOrdiniServizioSingoloGiorno = ""

        For i = 0 To 14
            Vettore_Orari_Giorno.Dalle(i) = "00.00"
            Vettore_Orari_Giorno.Alle(i) = "00.00"
            Vettore_Orari_Giorno.DalleAlto(i) = "00.00"
            Vettore_Orari_Giorno.AlleAlto(i) = "00.00"
            Vettore_Orari_Giorno.DalleBasso(i) = "00.00"
            Vettore_Orari_Giorno.AlleBasso(i) = "00.00"
            Vettore_Orari_Giorno.DalleObbligo(i) = "00.00"
            Vettore_Orari_Giorno.AlleObbligo(i) = "00.00"
            Vettore_Orari_Giorno.Pausa(i) = "00.00"
            Vettore_Orari_Giorno.Giustificativo(i) = ""
            Vettore_Orari_Giorno.NelGruppo(i) = ""
            Vettore_Orari_Giorno.Familiare(i) = 0
            Vettore_Orari_Giorno.CambioRichiesto(i) = ""
            Vettore_Orari_Giorno.Tipo(i) = ""
            Vettore_Orari_Giorno.TipoServizio(i) = ""
            Vettore_Orari_Giorno.GiornoSuccessivo(i) = ""
        Next i
        Vettore_Orari_Giorno.PrimoOrario = ""
        Vettore_Orari_Giorno.SecondoOrario = ""
        Vettore_Orari_Giorno.TerzoOrario = ""
        Vettore_Orari_Giorno.QuartoOrario = ""
        Vettore_Orari_Giorno.QuintoOrario = ""
        Vettore_Orari_Giorno.Richiesti = ""
        Vettore_Orari_Giorno.Variati = ""
        Vettore_Orari_Giorno.Modificati = ""
        Vettore_Orari_Giorno.TipoFlessibilita = ""
        Vettore_Orari_Giorno.FasceOrarie = ""
        Vettore_Orari_Giorno.GgSett = Weekday(Data)

        Dim cpt As New Cls_ParametriTurni
        Dim cxst As New Cls_ClsXScriptTurni
        cxst.ConnectionString = ConnectionString

        cpt.Leggi(ConnectionString)

        Dim cm As New Cls_Matricola

        cm.Trova(ConnectionString, Dipendente, Data, "<=")
        If cm.Id = 0 Then
            SviluppoTurniOrdiniServizioSingoloGiorno = "(E) Data di assunzione non indicata" & vbNewLine
            Exit Function
        End If
        If cm.DataLicenziamento <> #12:00:00 AM# Then
            If Format(cm.DataLicenziamento, "yyyyMMdd") < Format(Data, "yyyyMMdd") Then
                SviluppoTurniOrdiniServizioSingoloGiorno = "(E) Dipendente Dimesso" & vbNewLine
                Exit Function
            End If
        End If

        Dim CodiceSuperGruppo As String = ""
        Dim cdv As New Cls_DatiVariabili
        cdv.Trova(ConnectionString, Dipendente, cpt.CodiceAnagraficoSuperGruppo, Data)
        If cdv.Id <> 0 Then
            CodiceSuperGruppo = cdv.ContenutoTesto
        End If

        cdv.Trova(ConnectionString, Dipendente, cpt.CodiceAnagraficoContratto, Data)
        If cdv.Id = 0 Then
            SviluppoTurniOrdiniServizioSingoloGiorno = "(E) Contratto non indicato"
            Exit Function
        End If

        Dim CodiceContratto As String = cdv.ContenutoTesto

        Dim ccc As New Cls_CondizioniContrattuali

        ccc.Trova(ConnectionString, CodiceContratto, Data)
        If ccc.Id = 0 Then
            SviluppoTurniOrdiniServizioSingoloGiorno = "(E) Condizioni Contrattuali per il Contratto: " & CodiceContratto & "  inesistenti"
            Exit Function
        End If

        Dim FestivitaSettimanale As String = ccc.FestivitaSettimanale

        W_InAlto = 0
        W_InBasso = 0

        Dim co As New Cls_Orari

        W_OrarioNotte = co.OrarioConGiustificativo(ConnectionString, cpt.GiustificativoNotte)

        W_OrarioSmontoNotte = co.OrarioConGiustificativo(ConnectionString, cpt.GiustificativoSmontoNotte)

        cdv.Id = 0
        cdv.Trova(ConnectionString, Dipendente, cpt.CodiceAnagraficoFasceOrarie, Data)
        If cdv.Id > 0 Then
            Vettore_Orari_Giorno.FasceOrarie = cdv.ContenutoTesto
        End If

        Dim cof As New Cls_OrarioFlessibile
        cdv.Id = 0
        cdv.Trova(ConnectionString, Dipendente, cpt.CodiceAnagraficoFlessibilita, Data)
        If cdv.Id > 0 Then
            If cdv.ContenutoTesto <> "" Then
                cof.Trova(ConnectionString, cdv.ContenutoTesto, Data)
                If cof.Id = 0 Then
                    SviluppoTurniOrdiniServizioSingoloGiorno = "(E) Orario Flessibile non trovato" & vbNewLine
                    Exit Function
                End If
                If cof.GestioneTimbrature = "S" Then
                    Vettore_Orari_Giorno.TipoFlessibilita = "P"
                Else
                    If cof.FlessibilitaIntervallo = "S" Then
                        Vettore_Orari_Giorno.TipoFlessibilita = "X"
                    Else
                        Vettore_Orari_Giorno.TipoFlessibilita = "F"
                    End If
                    W_InAlto = DateDiff("n", #12/30/1899#, cof.InAlto) * -1
                    W_InBasso = DateDiff("n", #12/30/1899#, cof.InBasso)
                End If
            End If
        End If

        Dim StrutturaNotti As String = ""
        cdv.Trova(ConnectionString, Dipendente, cpt.CodiceAnagraficoStrutturaNotti, Data)
        If cdv.Id > 0 Then
            StrutturaNotti = cdv.ContenutoTesto
        End If

        Dim NottiSiNo As String = "Si"
        Dim WSiNo As String
        Dim csn As New Cls_StrutturaNotti

        If StrutturaNotti <> "" Then
            csn.Trova(ConnectionString, StrutturaNotti, Data)
            WSiNo = "No"
            If csn.Id > 0 Then
                For x = 0 To 6
                    For y = 0 To 4
                        If csn.GiornoOrario(x, y) <> "" Then
                            WSiNo = "Si"
                            Exit For
                        End If
                    Next y
                    If WSiNo = "Si" Then Exit For
                Next x
            End If
            NottiSiNo = WSiNo
        End If

        If co.Conta(ConnectionString) = 0 Then
            SviluppoTurniOrdiniServizioSingoloGiorno = SviluppoTurniOrdiniServizioSingoloGiorno & "(E) Nessun orario in tabella" & vbNewLine
            Exit Function
        End If

        Dim cg As New Cls_Giustificativi
        Dim cfo As New Cls_FasceOrarie

        Dim cn As OleDbConnection
        Dim cmd As New OleDbCommand()
        Dim cmd_1 As New OleDbCommand()
        Dim myPOSTreader As OleDbDataReader
        Dim myPOSTreader_1 As OleDbDataReader

        cn = New Data.OleDb.OleDbConnection(ConnectionString)

        cn.Open()

        If Vettore_Orari_Giorno.FasceOrarie <> "" Then
            cfo.Leggi(ConnectionString, Vettore_Orari_Giorno.FasceOrarie)
            If cfo.Id = 0 Then
                SviluppoTurniOrdiniServizioSingoloGiorno = Dipendente & " (E) Codice Fasce Orarie non trovato" & vbNewLine
                Exit Function
            End If
            For f = 1 To 10
                co.Leggi(ConnectionString, cfo.Orario(f))
                If co.Id > 0 Then
                    W_OrarioDalle(f) = Format(co.Dalle, "HH.mm")
                    W_OrarioDalleObbligo(f) = Format(co.DalleObbligo, "HH.mm")
                    W_OrarioAlle(f) = Format(co.Alle, "HH.mm")
                    W_OrarioAlleObbligo(f) = Format(co.AlleObbligo, "HH.mm")
                    W_OrarioGiustificativo(f) = co.Giustificativo
                    W_OrarioNelGruppo(f) = ""
                    W_OrarioFamiliare(f) = 0
                    W_OrarioCambioRichiesto(f) = ""
                End If
            Next f
            If Vettore_Timbrature_Giorno.Dalle(0) = "" Or Vettore_Timbrature_Giorno.Alle(0) = "" Then
                Vettore_Orari_Giorno.Dalle(0) = "00.00"
                Vettore_Orari_Giorno.DalleAlto(0) = "00.00"
                Vettore_Orari_Giorno.DalleBasso(0) = "00.00"
                Vettore_Orari_Giorno.DalleObbligo(0) = "00.00"
                Vettore_Orari_Giorno.Alle(0) = "00.00"
                Vettore_Orari_Giorno.AlleAlto(0) = "00.00"
                Vettore_Orari_Giorno.AlleBasso(0) = "00.00"
                Vettore_Orari_Giorno.AlleObbligo(0) = "00.00"
                Vettore_Orari_Giorno.Giustificativo(0) = cpt.GiustificativoRiposo
                Vettore_Orari_Giorno.NelGruppo(0) = ""
                Vettore_Orari_Giorno.Familiare(0) = 0
                Vettore_Orari_Giorno.CambioRichiesto(0) = ""
                Vettore_Orari_Giorno.Tipo(0) = cg.CampoGiustificativi(ConnectionString, Vettore_Orari_Giorno.Giustificativo(0), "Tipo")
            Else
                For ii = 1 To 10
                    If W_OrarioDalle(ii) = "" Then
                        W_OrarioDifferenzaDalle(ii) = 9999999
                        W_OrarioDifferenzaAlle(ii) = 9999999
                    Else
                        W_OrarioDifferenzaDalle(ii) = Math.Abs(DateDiff("n", Vettore_Timbrature_Giorno.Dalle(0), W_OrarioDalle(ii)))
                        W_OrarioDifferenzaAlle(ii) = Math.Abs(DateDiff("n", Vettore_Timbrature_Giorno.Alle(0), W_OrarioAlle(ii)))
                    End If
                Next ii
                Dim Ordinare As Boolean = True
                Do Until Ordinare = False
                    Ordinare = False
                    For ii = 2 To 10
                        If W_OrarioDifferenzaDalle(ii) < W_OrarioDifferenzaDalle(ii - 1) Or (W_OrarioDifferenzaDalle(ii) = W_OrarioDifferenzaDalle(ii - 1) And W_OrarioDifferenzaAlle(ii) < W_OrarioDifferenzaAlle(ii - 1)) Then
                            Ordinare = True
                            Dim W_Dalle As String = W_OrarioDalle(ii)
                            Dim W_DalleObbligo As String = W_OrarioDalleObbligo(ii)
                            Dim W_Alle As String = W_OrarioAlle(ii)
                            Dim W_AlleObbligo As String = W_OrarioAlleObbligo(ii)
                            Dim W_Giustificativo As String = W_OrarioGiustificativo(ii)
                            Dim W_NelGruppo As String = W_OrarioNelGruppo(ii)
                            Dim W_Familiare As Byte = W_OrarioFamiliare(ii)
                            Dim W_CambioRichiesto As String = W_OrarioCambioRichiesto(ii)
                            Dim W_DifferenzaDalle As Long = W_OrarioDifferenzaDalle(ii)
                            Dim W_DifferenzaAlle As Long = W_OrarioDifferenzaAlle(ii)
                            W_OrarioDalle(ii) = W_OrarioDalle(ii - 1)
                            W_OrarioDalleObbligo(ii) = W_OrarioDalleObbligo(ii - 1)
                            W_OrarioAlle(ii) = W_OrarioAlle(ii - 1)
                            W_OrarioAlleObbligo(ii) = W_OrarioAlleObbligo(ii - 1)
                            W_OrarioGiustificativo(ii) = W_OrarioGiustificativo(ii - 1)
                            W_OrarioNelGruppo(ii) = W_OrarioNelGruppo(ii - 1)
                            W_OrarioFamiliare(ii) = W_OrarioFamiliare(ii - 1)
                            W_OrarioCambioRichiesto(ii) = W_OrarioCambioRichiesto(ii - 1)
                            W_OrarioDifferenzaDalle(ii) = W_OrarioDifferenzaDalle(ii - 1)
                            W_OrarioDifferenzaAlle(ii) = W_OrarioDifferenzaAlle(ii - 1)
                            W_OrarioDalle(ii - 1) = W_Dalle
                            W_OrarioDalleObbligo(ii - 1) = W_DalleObbligo
                            W_OrarioAlle(ii - 1) = W_Alle
                            W_OrarioAlleObbligo(ii - 1) = W_AlleObbligo
                            W_OrarioGiustificativo(ii - 1) = W_Giustificativo
                            W_OrarioNelGruppo(ii - 1) = W_NelGruppo
                            W_OrarioFamiliare(ii - 1) = W_Familiare
                            W_OrarioCambioRichiesto(ii - 1) = W_CambioRichiesto
                            W_OrarioDifferenzaDalle(ii - 1) = W_DifferenzaDalle
                            W_OrarioDifferenzaAlle(ii - 1) = W_DifferenzaAlle
                        End If
                    Next ii
                Loop
                Vettore_Orari_Giorno.Dalle(0) = W_OrarioDalle(1)
                Vettore_Orari_Giorno.DalleAlto(0) = W_OrarioDalle(1)
                Vettore_Orari_Giorno.DalleBasso(0) = W_OrarioDalle(1)
                Vettore_Orari_Giorno.DalleObbligo(0) = W_OrarioDalleObbligo(1)
                Vettore_Orari_Giorno.Alle(0) = W_OrarioAlle(1)
                Vettore_Orari_Giorno.AlleAlto(0) = W_OrarioAlle(1)
                Vettore_Orari_Giorno.AlleBasso(0) = W_OrarioAlle(1)
                Vettore_Orari_Giorno.AlleObbligo(0) = W_OrarioAlleObbligo(1)
                Vettore_Orari_Giorno.Giustificativo(0) = W_OrarioGiustificativo(1)
                If Vettore_Orari_Giorno.Giustificativo(0) = "" Then Vettore_Orari_Giorno.Giustificativo(0) = cpt.GiustificativoOrarioLavoro
                Vettore_Orari_Giorno.NelGruppo(0) = W_OrarioNelGruppo(1)
                Vettore_Orari_Giorno.Familiare(0) = W_OrarioFamiliare(1)
                Vettore_Orari_Giorno.CambioRichiesto(0) = W_OrarioCambioRichiesto(1)
                Vettore_Orari_Giorno.Tipo(0) = cg.CampoGiustificativi(ConnectionString, Vettore_Orari_Giorno.Giustificativo(0), "Tipo")
            End If

        Else

            CodiceProfiloFerie = ""
            cdv.Id = 0
            cdv.Trova(ConnectionString, Dipendente, cpt.CodiceAnagraficoProfiloFerie, Data)
            If cdv.Id > 0 Then
                CodiceProfiloFerie = cdv.ContenutoTesto
            End If

            CodiceTurno = ""
            cdv.Id = 0
            cdv.Trova(ConnectionString, Dipendente, cpt.CodiceAnagraficoTurno, Data)
            If cdv.Id > 0 Then
                CodiceTurno = cdv.ContenutoTesto
            End If

            Dim ci As New Cls_Intervalli

            If Sw_Profilo = "S" Then
                If CodiceTurno <> "" And CodiceTurno <> "VUOTO" Then

                    Dim cpot As New Cls_ProfiliOrariTesta
                    Dim cpor As New Cls_ProfiliOrariRiga
                    Dim cspor As New Cls_StrutturaProfiliOrariRiga
                    cpot.Id = 0
                    cpot.Trova(ConnectionString, CodiceTurno, Data)
                    If cpot.Id = 0 Then
                        SviluppoTurniOrdiniServizioSingoloGiorno = SviluppoTurniOrdiniServizioSingoloGiorno & "(E) Profilo Orario '" & CodiceTurno & "' non sviluppato" & vbNewLine
                        Exit Function
                    End If

                    Dim Riga As Integer = 1
                    If cpot.CodiceStruttura <> "" Then
                        Contatore = cspor.Conta(ConnectionString, cpot.CodiceStruttura)
                        If cspor.Conta(ConnectionString, cpot.CodiceStruttura) = 0 Then
                            SviluppoTurniOrdiniServizioSingoloGiorno = SviluppoTurniOrdiniServizioSingoloGiorno & "(E) Profilo Orario '" & cpot.CodiceStruttura & "' senza righe" & vbNewLine
                            Exit Function
                        End If

                    Else
                        Contatore = cpor.Conta(ConnectionString, CodiceTurno, cpot.Validita)
                        If cpor.Conta(ConnectionString, CodiceTurno, cpot.Validita) = 0 Then
                            SviluppoTurniOrdiniServizioSingoloGiorno = SviluppoTurniOrdiniServizioSingoloGiorno & "(E) Profilo Orario '" & CodiceTurno & "' senza righe" & vbNewLine
                            Exit Function
                        End If
                    End If

                    If cpot.CodiceStruttura <> "" Then
                        cmd.CommandText = "SELECT *, 'A' + RIGHT('00000'+ CONVERT(VARCHAR,Riga),5) As Selezione FROM StrutturaProfiliOrariRiga" & _
                                    " WHERE Codice = ?" & _
                                    " AND Riga >= ?" & _
                                    " UNION " & _
                                    "SELECT *, 'B' + RIGHT('00000'+ CONVERT(VARCHAR,Riga),5) As Selezione FROM StrutturaProfiliOrariRiga" & _
                                    " WHERE Codice = ?" & _
                                    " AND Riga < ?" & _
                                    " ORDER BY Selezione "
                        cmd.Parameters.Clear()
                        cmd.Parameters.AddWithValue("@Codice", cpot.CodiceStruttura)
                        cmd.Parameters.AddWithValue("@Riga", cpot.PrimoElemento)
                        cmd.Parameters.AddWithValue("@Codice", cpot.CodiceStruttura)
                        cmd.Parameters.AddWithValue("@Riga", cpot.PrimoElemento)
                    Else
                        cmd.CommandText = "SELECT * FROM ProfiliOrariRiga" & _
                                " WHERE Codice = ?" & _
                                " AND Validita = ?" & _
                                " ORDER BY Riga "
                        cmd.Parameters.Clear()
                        cmd.Parameters.AddWithValue("@Codice", CodiceTurno)
                        cmd.Parameters.AddWithValue("@Validita", cpot.Validita)
                    End If

                    cmd.Connection = cn
                    myPOSTreader = cmd.ExecuteReader()

                    Dim Giorni As Integer = DateDiff("d", cpot.Validita, Data)
                    Giorni = Giorni + 1
                    Dim Resto As Integer = 0
                    If Contatore > 0 Then
                        Resto = Giorni - (Int(Giorni / Contatore) * Contatore)
                    End If
                    If Resto = 0 Then
                        Resto = Contatore
                    End If

                    Do While myPOSTreader.Read
                        If Riga = Resto Then Exit Do
                        Riga = Riga + 1
                    Loop

                    If Not StringaDb(myPOSTreader.Item("PrimoOrario")) = "" Then
                        Sw_Ok = True
                        Vettore_Orari_Giorno.PrimoOrario = StringaDb(myPOSTreader.Item("PrimoOrario"))

                        If Vettore_Orari_Giorno.PrimoOrario = W_OrarioNotte Or Vettore_Orari_Giorno.PrimoOrario = W_OrarioSmontoNotte Then
                            If NottiSiNo = "No" Then
                                Vettore_Orari_Giorno.PrimoOrario = ""
                                Sw_Ok = False
                            End If
                        End If

                        If Sw_Ok = True Then
                            Call CaricaVettoreOrariUltimoGiorno(ConnectionString, 0, W_InAlto, W_InBasso, Vettore_Orari_Giorno, Vettore_Orari_Giorno.PrimoOrario)
                        End If
                    End If

                    If Not StringaDb(myPOSTreader.Item("SecondoOrario")) = "" Then
                        Sw_Ok = True
                        Vettore_Orari_Giorno.SecondoOrario = StringaDb(myPOSTreader.Item("SecondoOrario"))

                        If Vettore_Orari_Giorno.SecondoOrario = W_OrarioNotte Or Vettore_Orari_Giorno.SecondoOrario = W_OrarioSmontoNotte Then
                            If NottiSiNo = "No" Then
                                Vettore_Orari_Giorno.SecondoOrario = ""
                                Sw_Ok = False
                            End If
                        End If

                        If Sw_Ok = True Then
                            Call CaricaVettoreOrariUltimoGiorno(ConnectionString, 1, W_InAlto, W_InBasso, Vettore_Orari_Giorno, Vettore_Orari_Giorno.SecondoOrario)
                        End If
                    End If

                    If Not StringaDb(myPOSTreader.Item("TerzoOrario")) = "" Then
                        Sw_Ok = True
                        Vettore_Orari_Giorno.TerzoOrario = StringaDb(myPOSTreader.Item("TerzoOrario"))

                        If Vettore_Orari_Giorno.TerzoOrario = W_OrarioNotte Or Vettore_Orari_Giorno.TerzoOrario = W_OrarioSmontoNotte Then
                            If NottiSiNo = "No" Then
                                Vettore_Orari_Giorno.TerzoOrario = ""
                                Sw_Ok = False
                            End If
                        End If

                        If Sw_Ok = True Then
                            Call CaricaVettoreOrariUltimoGiorno(ConnectionString, 2, W_InAlto, W_InBasso, Vettore_Orari_Giorno, Vettore_Orari_Giorno.TerzoOrario)
                        End If
                    End If

                    If Not StringaDb(myPOSTreader.Item("QuartoOrario")) = "" Then
                        Sw_Ok = True
                        Vettore_Orari_Giorno.QuartoOrario = StringaDb(myPOSTreader.Item("QuartoOrario"))

                        If Vettore_Orari_Giorno.QuartoOrario = W_OrarioNotte Or Vettore_Orari_Giorno.QuartoOrario = W_OrarioSmontoNotte Then
                            If NottiSiNo = "No" Then
                                Vettore_Orari_Giorno.QuartoOrario = ""
                                Sw_Ok = False
                            End If
                        End If
                        If Sw_Ok = True Then
                            Call CaricaVettoreOrariUltimoGiorno(ConnectionString, 3, W_InAlto, W_InBasso, Vettore_Orari_Giorno, Vettore_Orari_Giorno.QuartoOrario)
                        End If
                    End If

                    If Not StringaDb(myPOSTreader.Item("QuintoOrario")) = "" Then
                        Sw_Ok = True
                        Vettore_Orari_Giorno.QuintoOrario = StringaDb(myPOSTreader.Item("QuintoOrario"))

                        If Vettore_Orari_Giorno.QuintoOrario = W_OrarioNotte Or Vettore_Orari_Giorno.QuintoOrario = W_OrarioSmontoNotte Then
                            If NottiSiNo = "No" Then
                                Vettore_Orari_Giorno.QuintoOrario = ""
                                Sw_Ok = False
                            End If
                        End If
                        If Sw_Ok = True Then
                            Call CaricaVettoreOrariUltimoGiorno(ConnectionString, 4, W_InAlto, W_InBasso, Vettore_Orari_Giorno, Vettore_Orari_Giorno.QuintoOrario)
                        End If
                    End If

                    Dim OGF As String = ""
                    Dim DOW As Byte = 0
                    If cpot.OrarioGiornoFestivo <> "" And Weekday(Data) = 1 Then
                        OGF = cpot.OrarioGiornoFestivo
                        DOW = 7
                    End If
                    If cpot.OrarioGiornoFestivoInfrasettimanale <> "" And cxst.GiornoFestivo(Data, CodiceSuperGruppo) = True Then
                        OGF = cpot.OrarioGiornoFestivoInfrasettimanale
                        DOW = Weekday(Data) - 1
                    End If
                    If OGF <> "" Then
                        co.Leggi(ConnectionString, OGF)
                        Dim WOF_Dalle As String = Format(co.Dalle, "hh.nn")
                        Dim WOF_Alle As String = Format(co.Alle, "hh.nn")
                        Dim WOF_Giustificativo As String = co.Giustificativo
                        Dim WOF_Tipo As String = cg.CampoGiustificativi(ConnectionString, WOF_Giustificativo, "Tipo")
                        If WOF_Dalle = "00.00" And WOF_Alle = "00.00" And WOF_Giustificativo <> "" Then
                            cg.Leggi(ConnectionString, WOF_Giustificativo)
                            If Mid(cg.Assenza_ChkGiorno, DOW, 1) = "S" Then
                                If cg.Assenza_SuRiposo = "S" Or (cg.Assenza_SuRiposo = "N" And Vettore_Orari_Giorno.Giustificativo(0) <> cpt.GiustificativoRiposo) Then
                                    If cg.Assenza_RimuoviOrari = "S" Then
                                        Vettore_Orari_Giorno.TipoFlessibilita = ""
                                        For h = 0 To 14
                                            Vettore_Orari_Giorno.Dalle(h) = "00.00"
                                            Vettore_Orari_Giorno.DalleAlto(h) = "00.00"
                                            Vettore_Orari_Giorno.DalleBasso(h) = "00.00"
                                            Vettore_Orari_Giorno.Alle(h) = "00.00"
                                            Vettore_Orari_Giorno.AlleBasso(h) = "00.00"
                                            Vettore_Orari_Giorno.AlleAlto(h) = "00.00"
                                            Vettore_Orari_Giorno.DalleObbligo(h) = "00.00"
                                            Vettore_Orari_Giorno.AlleObbligo(h) = "00.00"
                                            Vettore_Orari_Giorno.Pausa(h) = "00.00"
                                            Vettore_Orari_Giorno.Giustificativo(h) = ""
                                            Vettore_Orari_Giorno.Tipo(h) = ""
                                        Next h
                                        Vettore_Orari_Giorno.Giustificativo(0) = OGF
                                        Vettore_Orari_Giorno.Tipo(0) = WOF_Tipo
                                    Else
                                        For h = 0 To 14
                                            If Vettore_Orari_Giorno.Dalle(h) = "00.00" And Vettore_Orari_Giorno.Alle(h) = "00.00" And Vettore_Orari_Giorno.Giustificativo(h) = "" Then Exit For
                                            Vettore_Orari_Giorno.Giustificativo(h) = OGF
                                            Vettore_Orari_Giorno.Tipo(h) = WOF_Tipo
                                        Next h
                                    End If
                                End If
                            End If
                        Else
                            Vettore_Orari_Giorno.Dalle(0) = WOF_Dalle
                            Vettore_Orari_Giorno.DalleAlto(0) = WOF_Dalle
                            Vettore_Orari_Giorno.DalleBasso(0) = WOF_Dalle
                            Vettore_Orari_Giorno.Alle(0) = WOF_Alle
                            Vettore_Orari_Giorno.AlleAlto(0) = WOF_Alle
                            Vettore_Orari_Giorno.AlleBasso(0) = WOF_Alle
                            If Vettore_Orari_Giorno.TipoFlessibilita = "F" Or Vettore_Orari_Giorno.TipoFlessibilita = "X" Then
                                Vettore_Orari_Giorno.DalleObbligo(0) = Format(co.DalleObbligo, "hh.nn")
                                Vettore_Orari_Giorno.AlleObbligo(0) = Format(co.AlleObbligo, "hh.nn")
                                Vettore_Orari_Giorno.DalleAlto(0) = Format(co.DalleAlto, "hh.nn")
                                Vettore_Orari_Giorno.DalleBasso(0) = Format(co.DalleBasso, "hh.nn")
                                Vettore_Orari_Giorno.AlleAlto(0) = Format(co.AlleAlto, "hh.nn")
                                Vettore_Orari_Giorno.AlleBasso(0) = Format(co.AlleBasso, "hh.nn")
                                If W_InAlto <> 0 Then
                                    Vettore_Orari_Giorno.DalleAlto(0) = Format(DateAdd("n", W_InAlto, co.Dalle), "hh.nn")
                                    Vettore_Orari_Giorno.AlleAlto(0) = Format(DateAdd("n", W_InAlto, co.Alle), "hh.nn")
                                End If
                                If W_InBasso <> 0 Then
                                    Vettore_Orari_Giorno.DalleBasso(0) = Format(DateAdd("n", W_InBasso, co.Dalle), "hh.nn")
                                    Vettore_Orari_Giorno.AlleBasso(0) = Format(DateAdd("n", W_InBasso, co.Alle), "hh.nn")
                                End If
                            End If
                            Vettore_Orari_Giorno.Pausa(0) = Format(co.Pausa, "hh.nn")
                            Vettore_Orari_Giorno.Giustificativo(0) = WOF_Giustificativo
                            Vettore_Orari_Giorno.Tipo(0) = WOF_Tipo
                            If WOF_Giustificativo <> "" And Format(co.Jolly, "hh.nn") <> 0 Then
                                Vettore_Orari_Giorno.Alle(0) = Format(co.Jolly, "hh.nn")
                            End If
                            For h = 1 To 9
                                Vettore_Orari_Giorno.Dalle(h) = "00.00"
                                Vettore_Orari_Giorno.DalleAlto(h) = "00.00"
                                Vettore_Orari_Giorno.DalleBasso(h) = "00.00"
                                Vettore_Orari_Giorno.Alle(h) = "00.00"
                                Vettore_Orari_Giorno.AlleBasso(h) = "00.00"
                                Vettore_Orari_Giorno.AlleAlto(h) = "00.00"
                                Vettore_Orari_Giorno.DalleObbligo(h) = "00.00"
                                Vettore_Orari_Giorno.AlleObbligo(h) = "00.00"
                                Vettore_Orari_Giorno.Pausa(h) = "00.00"
                                Vettore_Orari_Giorno.Giustificativo(h) = ""
                                Vettore_Orari_Giorno.Tipo(h) = ""
                            Next h
                        End If
                    End If
                    myPOSTreader.Close()
                End If
            End If

            ' ************* da qui

            If Sw_ProfiloFerie = "S" Then
                If CodiceProfiloFerie <> "" Then

                    Dim cpft As New Cls_ProfiliFerieTesta

                    cpft.Trova(ConnectionString, CodiceProfiloFerie, Data)
                    If cpft.Id = 0 Then
                        SviluppoTurniOrdiniServizioSingoloGiorno = SviluppoTurniOrdiniServizioSingoloGiorno & "(E) Profilo Ferie '" & CodiceProfiloFerie & "' non sviluppato" & vbNewLine
                        Exit Function
                    End If

                    Dim Intervallo As Integer = 1
                    Dim cpfr As New Cls_ProfiliFerieRiga

                    If cpfr.Conta(ConnectionString, CodiceProfiloFerie, cpft.Validita) = 0 Then
                        SviluppoTurniOrdiniServizioSingoloGiorno = SviluppoTurniOrdiniServizioSingoloGiorno & Dipendente & " (E) Profilo Ferie '" & CodiceProfiloFerie & "' senza righe" & vbNewLine
                        Exit Function
                    End If

                    Dim AnnoAttuale As Integer = Year(Data)
                    Dim Anni As Integer = AnnoAttuale - Year(cpft.Validita) + 1

                    cmd.Cancel()
                    cmd.CommandText = "SELECT * FROM ProfiliFerieRiga" & _
                                      " WHERE Codice = ?" & _
                                      " AND Validita = ?" & _
                                      " AND Riga = ?"
                    cmd.Parameters.Clear()
                    cmd.Parameters.AddWithValue("@Codice", CodiceProfiloFerie)
                    cmd.Parameters.AddWithValue("@Validita", cpft.Validita)
                    cmd.Parameters.AddWithValue("@Riga", Anni)

                    cmd.Connection = cn
                    myPOSTreader = cmd.ExecuteReader()
                    If myPOSTreader.Read Then

                        If cpft.OrarioGiornoFestivo <> "" And (Weekday(Data) = FestivitaSettimanale Or cxst.GiornoFestivo(Data, CodiceSuperGruppo) = True) Then

                            Call CaricaVettoreOrariUltimoGiorno(ConnectionString, 0, W_InAlto, W_InBasso, Vettore_Orari_Giorno, cpft.OrarioGiornoFestivo)

                        Else

                            If StringaDb(myPOSTreader.Item("PrimoIntervallo")) <> "" Then
                                Call InserisciFerie_STOSSG(ConnectionString, cpft.ControlloNotte, cpft.ControlloSmontoNotte, cpft.ControlloRiposo, StringaDb(myPOSTreader.Item("PrimoIntervallo")), Data, Vettore_Orari_Giorno)
                            End If

                            If StringaDb(myPOSTreader.Item("SecondoIntervallo")) <> "" Then
                                Call InserisciFerie_STOSSG(ConnectionString, cpft.ControlloNotte, cpft.ControlloSmontoNotte, cpft.ControlloRiposo, StringaDb(myPOSTreader.Item("SecondoIntervallo")), Data, Vettore_Orari_Giorno)
                            End If

                            If StringaDb(myPOSTreader.Item("TerzoIntervallo")) <> "" Then
                                Call InserisciFerie_STOSSG(ConnectionString, cpft.ControlloNotte, cpft.ControlloSmontoNotte, cpft.ControlloRiposo, StringaDb(myPOSTreader.Item("TerzoIntervallo")), Data, Vettore_Orari_Giorno)
                            End If

                            If StringaDb(myPOSTreader.Item("QuartoIntervallo")) <> "" Then
                                Call InserisciFerie_STOSSG(ConnectionString, cpft.ControlloNotte, cpft.ControlloSmontoNotte, cpft.ControlloRiposo, StringaDb(myPOSTreader.Item("QuartoIntervallo")), Data, Vettore_Orari_Giorno)
                            End If

                            If StringaDb(myPOSTreader.Item("QuintoIntervallo")) <> "" Then
                                Call InserisciFerie_STOSSG(ConnectionString, cpft.ControlloNotte, cpft.ControlloSmontoNotte, cpft.ControlloRiposo, StringaDb(myPOSTreader.Item("QuintoIntervallo")), Data, Vettore_Orari_Giorno)
                            End If
                        End If
                        myPOSTreader.Close()
                    End If
                End If
            End If
        End If

        ' ************* a qui

        Call Carica_Vettore_Orari_Giorno_RichiesteAssenza(ConnectionString, Dipendente, Data, CodiceSuperGruppo, Vettore_Orari_Giorno)

        Call Carica_Vettore_Orari_Giorno_RichiesteStraordinario(ConnectionString, Dipendente, Data, Vettore_Orari_Giorno)

        Call Carica_Vettore_Orari_Giorno_ProfiliOrariVariati(ConnectionString, Dipendente, Data, "S", "S", Vettore_Orari_Giorno)

        cmd.Cancel()
        cmd.CommandText = " SELECT * " & _
                          " FROM ProfiliOrariModificati " & _
                          " WHERE CodiceDipendente = ?" & _
                          " AND Data = ?" & _
                          " ORDER BY Data, Dalle"
        cmd.Parameters.Clear()
        cmd.Parameters.AddWithValue("@CodiceDipendente", Dipendente)
        cmd.Parameters.AddWithValue("@Data", Data)

        cmd.Connection = cn
        myPOSTreader = cmd.ExecuteReader()

        i = 0
        Do While myPOSTreader.Read
            Vettore_Orari_Giorno.Modificati = "S"
            Vettore_Orari_Giorno.Dalle(i) = Format(DataDb(myPOSTreader.Item("Dalle")), "HH.mm")
            Vettore_Orari_Giorno.Alle(i) = Format(DataDb(myPOSTreader.Item("Alle")), "HH.mm")
            Vettore_Orari_Giorno.DalleAlto(i) = "00.00"
            Vettore_Orari_Giorno.AlleAlto(i) = "00.00"
            Vettore_Orari_Giorno.DalleBasso(i) = "00.00"
            Vettore_Orari_Giorno.AlleBasso(i) = "00.00"
            Vettore_Orari_Giorno.DalleObbligo(i) = "00.00"
            Vettore_Orari_Giorno.AlleObbligo(i) = "00.00"
            Vettore_Orari_Giorno.Pausa(i) = Format(DataDb(myPOSTreader.Item("Pausa")), "HH.mm")
            Vettore_Orari_Giorno.Giustificativo(i) = StringaDb(myPOSTreader.Item("Giustificativo"))
            Vettore_Orari_Giorno.NelGruppo(i) = StringaDb(myPOSTreader.Item("NelGruppo"))
            Vettore_Orari_Giorno.Familiare(i) = NumeroDb(myPOSTreader.Item("CodiceFamiliare"))
            Vettore_Orari_Giorno.CambioRichiesto(i) = StringaDb(myPOSTreader.Item("CambioRichiesto"))
            Vettore_Orari_Giorno.Tipo(i) = cg.CampoGiustificativi(ConnectionString, Vettore_Orari_Giorno.Giustificativo(i), "Tipo")
            If i < 14 Then
                i = i + 1
            End If
        Loop
        myPOSTreader.Close()

        cmd.Cancel()

        Dim DataPrec As Date = DateAdd("d", -1, Data)
        cmd.CommandText = "SELECT LegameDipendenteOrdiniServizio.Data, LegameDipendenteOrdiniServizio.Gruppo, LegameDipendenteOrdiniServizio.GruppoComandante, LegameDipendenteOrdiniServizio.TipoComandante, LegameDipendenteOrdiniServizio.Riga, LegameDipendenteOrdiniServizio.CodiceFamiliare, OrdiniServizioRiga.Servizio" & _
                          " FROM LegameDipendenteOrdiniServizio INNER JOIN OrdiniServizioRiga ON (LegameDipendenteOrdiniServizio.Riga = OrdiniServizioRiga.Riga)" & _
                          " AND (LegameDipendenteOrdiniServizio.Gruppo = OrdiniServizioRiga.Gruppo)" & _
                          " AND (LegameDipendenteOrdiniServizio.Data = OrdiniServizioRiga.Data)" & _
                          " WHERE (LegameDipendenteOrdiniServizio.GruppoComandante Is Null OR LegameDipendenteOrdiniServizio.GruppoComandante = '')" & _
                          " AND (LegameDipendenteOrdiniServizio.TipoComandante Is Null OR LegameDipendenteOrdiniServizio.TipoComandante = '')" & _
                          " AND (LegameDipendenteOrdiniServizio.Data = ?" & _
                          " OR LegameDipendenteOrdiniServizio.Data = ?)" & _
                          " AND CodiceDipendente = ?"
        cmd.CommandText = cmd.CommandText & " UNION" & _
                          " SELECT LegameDipendenteOrdiniServizio.Data, LegameDipendenteOrdiniServizio.Gruppo, LegameDipendenteOrdiniServizio.GruppoComandante, LegameDipendenteOrdiniServizio.TipoComandante, LegameDipendenteOrdiniServizio.Riga, LegameDipendenteOrdiniServizio.CodiceFamiliare, OrdiniServizioRigaComandanti.Servizio" & _
                          " FROM LegameDipendenteOrdiniServizio INNER JOIN OrdiniServizioRigaComandanti ON (LegameDipendenteOrdiniServizio.Riga = OrdiniServizioRigaComandanti.Riga)" & _
                          " AND (LegameDipendenteOrdiniServizio.GruppoComandante = OrdiniServizioRigaComandanti.Gruppo)" & _
                          " AND (LegameDipendenteOrdiniServizio.TipoComandante = OrdiniServizioRigaComandanti.Tipo)" & _
                          " AND (LegameDipendenteOrdiniServizio.Data = OrdiniServizioRigaComandanti.Data)" & _
                          " WHERE Not LegameDipendenteOrdiniServizio.GruppoComandante Is Null" & _
                          " AND LegameDipendenteOrdiniServizio.GruppoComandante <> ''" & _
                          " AND Not LegameDipendenteOrdiniServizio.TipoComandante Is Null" & _
                          " AND LegameDipendenteOrdiniServizio.TipoComandante <> ''" & _
                          " AND (LegameDipendenteOrdiniServizio.Data = ?" & _
                          " OR LegameDipendenteOrdiniServizio.Data = ?)" & _
                          " AND CodiceDipendente = ?" & _
                          " ORDER BY LegameDipendenteOrdiniServizio.Data"
        cmd.Parameters.Clear()
        cmd.Parameters.AddWithValue("@Data", DataPrec)
        cmd.Parameters.AddWithValue("@Data", Data)
        cmd.Parameters.AddWithValue("@CodiceDipendente", Dipendente)
        cmd.Parameters.AddWithValue("@Data", DataPrec)
        cmd.Parameters.AddWithValue("@Data", Data)
        cmd.Parameters.AddWithValue("@CodiceDipendente", Dipendente)

        cmd.Connection = cn
        myPOSTreader = cmd.ExecuteReader()

        Dim Entra As Boolean = False

        If myPOSTreader.Read Then
            For i = 0 To 14
                Vettore_Orari_Giorno.Dalle(i) = "00.00"
                Vettore_Orari_Giorno.Alle(i) = "00.00"
                Vettore_Orari_Giorno.DalleAlto(i) = "00.00"
                Vettore_Orari_Giorno.AlleAlto(i) = "00.00"
                Vettore_Orari_Giorno.DalleBasso(i) = "00.00"
                Vettore_Orari_Giorno.AlleBasso(i) = "00.00"
                Vettore_Orari_Giorno.DalleObbligo(i) = "00.00"
                Vettore_Orari_Giorno.AlleObbligo(i) = "00.00"
                '      Vettore_Orari_Giorno.DCT(i) = ""
                Vettore_Orari_Giorno.Pausa(i) = "00.00"
                Vettore_Orari_Giorno.Giustificativo(i) = ""
                Vettore_Orari_Giorno.Familiare(i) = 0
                Vettore_Orari_Giorno.Tipo(i) = ""
                Vettore_Orari_Giorno.TipoServizio(i) = ""
                Vettore_Orari_Giorno.GiornoSuccessivo(i) = ""
            Next i
            Vettore_Orari_Giorno.Richiesti = ""
            Vettore_Orari_Giorno.Variati = ""
            Vettore_Orari_Giorno.Modificati = ""
            Entra = True
        End If
        i = 0
        If Entra = True Then
            Do
                If StringaDb(myPOSTreader.Item("GruppoComandante")) = "" Then
                    cmd_1.Cancel()

                    cmd_1.CommandText = "SELECT * FROM OrdiniServizioRigaOrario" & _
                                        " WHERE Data = ?" & _
                                        " AND Gruppo = ?" & _
                                        " AND Riga = ?"
                    cmd_1.Parameters.Clear()
                    cmd_1.Parameters.AddWithValue("@Data", DataDb(myPOSTreader.Item("Data")))
                    cmd_1.Parameters.AddWithValue("@Gruppo", StringaDb(myPOSTreader.Item("Gruppo")))
                    cmd_1.Parameters.AddWithValue("@Riga", NumeroDb(myPOSTreader.Item("Riga")))

                    cmd_1.Connection = cn
                    myPOSTreader_1 = cmd_1.ExecuteReader()
                    If Not myPOSTreader_1.Read Then
                        myPOSTreader_1.Close()

                        cmd_1.Cancel()

                        cmd_1.CommandText = "SELECT * FROM OrdiniServizioRigaProfilo" & _
                                            " WHERE Data = ?" & _
                                            " AND Gruppo = ?" & _
                                            " AND Riga = ?" & _
                                            " ORDER BY Data, Dalle"
                        cmd_1.Parameters.Clear()
                        cmd_1.Parameters.AddWithValue("@Data", DataDb(myPOSTreader.Item("Data")))
                        cmd_1.Parameters.AddWithValue("@Gruppo", StringaDb(myPOSTreader.Item("Gruppo")))
                        cmd_1.Parameters.AddWithValue("@Riga", NumeroDb(myPOSTreader.Item("Riga")))

                        cmd_1.Connection = cn
                        myPOSTreader_1 = cmd_1.ExecuteReader()

                        If Not myPOSTreader_1.Read Then
                            If Format(DataDb(myPOSTreader.Item("Data")), "yyyyMMdd") = Format(Data, "yyyyMMdd") Then
                                If Vettore_Orari_Giorno.Variati <> "P" And Vettore_Orari_Giorno.Variati <> "S" And Vettore_Orari_Giorno.Modificati <> "S" Then
                                    Vettore_Orari_Giorno.Giustificativo(i) = StringaDb(myPOSTreader.Item("Servizio"))
                                    Vettore_Orari_Giorno.Familiare(i) = NumeroDb(myPOSTreader.Item("CodiceFamiliare"))
                                    Vettore_Orari_Giorno.Tipo(i) = cg.CampoGiustificativi(ConnectionString, Vettore_Orari_Giorno.Giustificativo(i), "Tipo")
                                    Vettore_Orari_Giorno.TipoServizio(i) = StringaDb(myPOSTreader.Item("TipoComandante"))
                                    If i < 14 Then i = i + 1
                                End If
                            End If
                        Else
                            Do
                                If StringaDb(myPOSTreader_1.Item("GiornoSuccessivo")) = "S" Then
                                    If Format(DataDb(myPOSTreader.Item("Data")), "yyyyMMdd") = Format(DataPrec, "yyyyMMdd") Then
                                        If Vettore_Orari_Giorno.Variati <> "P" And Vettore_Orari_Giorno.Variati <> "S" And Vettore_Orari_Giorno.Modificati <> "S" Then
                                            Vettore_Orari_Giorno.Dalle(i) = Format(DataDb(myPOSTreader_1.Item("Dalle")), "HH.mm")
                                            Vettore_Orari_Giorno.DalleAlto(i) = Format(DataDb(myPOSTreader_1.Item("Dalle")), "HH.mm")
                                            Vettore_Orari_Giorno.DalleBasso(i) = Format(DataDb(myPOSTreader_1.Item("Dalle")), "HH.mm")
                                            '                Vettore_Orari_Giorno.DalleObbligo(i) = Format(DataDb(myPOSTreader_1.Item("DalleObbligo")), "HH.mm")
                                            Vettore_Orari_Giorno.Alle(i) = Format(DataDb(myPOSTreader_1.Item("Alle")), "HH.mm")
                                            Vettore_Orari_Giorno.AlleAlto(i) = Format(DataDb(myPOSTreader_1.Item("Alle")), "HH.mm")
                                            Vettore_Orari_Giorno.AlleBasso(i) = Format(DataDb(myPOSTreader_1.Item("Alle")), "HH.mm")
                                            '                Vettore_Orari_Giorno.AlleObbligo(i) = Format(DataDb(myPOSTreader_1.Item("AlleObbligo")), "HH.mm")
                                            Vettore_Orari_Giorno.Pausa(i) = Format(DataDb(myPOSTreader_1.Item("Pausa")), "HH.mm")
                                            If Vettore_Orari_Giorno.TipoFlessibilita = "F" Or Vettore_Orari_Giorno.TipoFlessibilita = "X" Then
                                                If W_InAlto <> 0 Then
                                                    Vettore_Orari_Giorno.DalleAlto(i) = Format(DateAdd("n", W_InAlto, DataDb(myPOSTreader_1.Item("Dalle"))), "HH.mm")
                                                    Vettore_Orari_Giorno.AlleAlto(i) = Format(DateAdd("n", W_InAlto, DataDb(myPOSTreader_1.Item("Alle"))), "HH.mm")
                                                End If
                                                If W_InBasso <> 0 Then
                                                    Vettore_Orari_Giorno.DalleBasso(i) = Format(DateAdd("n", W_InBasso, DataDb(myPOSTreader_1.Item("Dalle"))), "HH.mm")
                                                    Vettore_Orari_Giorno.AlleBasso(i) = Format(DateAdd("n", W_InBasso, DataDb(myPOSTreader_1.Item("Alle"))), "HH.mm")
                                                End If
                                            End If
                                            Vettore_Orari_Giorno.Giustificativo(i) = StringaDb(myPOSTreader.Item("Servizio"))
                                            Vettore_Orari_Giorno.Familiare(i) = NumeroDb(myPOSTreader.Item("CodiceFamiliare"))
                                            Vettore_Orari_Giorno.Tipo(i) = cg.CampoGiustificativi(ConnectionString, Vettore_Orari_Giorno.Giustificativo(i), "Tipo")
                                            Vettore_Orari_Giorno.TipoServizio(i) = StringaDb(myPOSTreader.Item("TipoComandante"))
                                            Vettore_Orari_Giorno.GiornoSuccessivo(i) = StringaDb(myPOSTreader_1.Item("GiornoSuccessivo"))
                                            If i < 14 Then i = i + 1
                                        End If
                                    End If
                                Else
                                    If Format(DataDb(myPOSTreader.Item("Data")), "yyyyMMdd") = Format(Data, "yyyyMMdd") Then
                                        If Vettore_Orari_Giorno.Variati <> "P" And Vettore_Orari_Giorno.Variati <> "S" And Vettore_Orari_Giorno.Modificati <> "S" Then
                                            Vettore_Orari_Giorno.Dalle(i) = Format(DataDb(myPOSTreader_1.Item("Dalle")), "HH.mm")
                                            Vettore_Orari_Giorno.DalleAlto(i) = Format(DataDb(myPOSTreader_1.Item("Dalle")), "HH.mm")
                                            Vettore_Orari_Giorno.DalleBasso(i) = Format(DataDb(myPOSTreader_1.Item("Dalle")), "HH.mm")
                                            '                Vettore_Orari_Giorno.DalleObbligo(i) = Format(DataDb(myPOSTreader_1.Item("DalleObbligo")), "HH.mm")
                                            Vettore_Orari_Giorno.Alle(i) = Format(DataDb(myPOSTreader_1.Item("Alle")), "HH.mm")
                                            Vettore_Orari_Giorno.AlleAlto(i) = Format(DataDb(myPOSTreader_1.Item("Alle")), "HH.mm")
                                            Vettore_Orari_Giorno.AlleBasso(i) = Format(DataDb(myPOSTreader_1.Item("Alle")), "HH.mm")
                                            '                Vettore_Orari_Giorno.AlleObbligo(i) = Format(DataDb(myPOSTreader_1.Item("AlleObbligo")), "HH.mm")
                                            Vettore_Orari_Giorno.Pausa(i) = Format(DataDb(myPOSTreader_1.Item("Pausa")), "HH.mm")
                                            If Vettore_Orari_Giorno.TipoFlessibilita = "F" Or Vettore_Orari_Giorno.TipoFlessibilita = "X" Then
                                                If W_InAlto <> 0 Then
                                                    Vettore_Orari_Giorno.DalleAlto(i) = Format(DateAdd("n", W_InAlto, DataDb(myPOSTreader_1.Item("Dalle"))), "HH.mm")
                                                    Vettore_Orari_Giorno.AlleAlto(i) = Format(DateAdd("n", W_InAlto, DataDb(myPOSTreader_1.Item("Alle"))), "HH.mm")
                                                End If
                                                If W_InBasso <> 0 Then
                                                    Vettore_Orari_Giorno.DalleBasso(i) = Format(DateAdd("n", W_InBasso, DataDb(myPOSTreader_1.Item("Dalle"))), "HH.mm")
                                                    Vettore_Orari_Giorno.AlleBasso(i) = Format(DateAdd("n", W_InBasso, DataDb(myPOSTreader_1.Item("Alle"))), "HH.mm")
                                                End If
                                            End If
                                            Vettore_Orari_Giorno.Giustificativo(i) = StringaDb(myPOSTreader.Item("Servizio"))
                                            Vettore_Orari_Giorno.Familiare(i) = NumeroDb(myPOSTreader.Item("CodiceFamiliare"))
                                            Vettore_Orari_Giorno.Tipo(i) = cg.CampoGiustificativi(ConnectionString, Vettore_Orari_Giorno.Giustificativo(i), "Tipo")
                                            Vettore_Orari_Giorno.TipoServizio(i) = StringaDb(myPOSTreader.Item("TipoComandante"))
                                            Vettore_Orari_Giorno.GiornoSuccessivo(i) = StringaDb(myPOSTreader_1.Item("GiornoSuccessivo"))
                                            If i < 14 Then i = i + 1
                                        End If
                                    End If
                                End If
                            Loop While myPOSTreader_1.Read
                        End If

                    Else

                        If StringaDb(myPOSTreader_1.Item("PrimoOrario")) <> "" Then
                            Vettore_Orari_Giorno.PrimoOrario = StringaDb(myPOSTreader_1.Item("PrimoOrario"))
                            If StringaDb(myPOSTreader_1.Item("PrimoGiornoSuccessivo")) = "S" Then
                                If Format(DataDb(myPOSTreader.Item("Data")), "yyyyMMdd") = Format(DataPrec, "yyyyMMdd") Then
                                    If Vettore_Orari_Giorno.Variati <> "P" And Vettore_Orari_Giorno.Variati <> "S" And Vettore_Orari_Giorno.Modificati <> "S" Then

                                        Call CaricaVettoreOrariOrdiniServizioUltimoGiorno(ConnectionString, i, StringaDb(myPOSTreader_1.Item("PrimoGiornoSuccessivo")), W_InAlto, W_InBasso, Vettore_Orari_Giorno, myPOSTreader, Vettore_Orari_Giorno.PrimoOrario)

                                        If i < 14 Then i = i + 1
                                    End If
                                End If
                            Else
                                If Format(DataDb(myPOSTreader.Item("Data")), "yyyyMMdd") = Format(Data, "yyyyMMdd") Then
                                    If Vettore_Orari_Giorno.Variati <> "P" And Vettore_Orari_Giorno.Variati <> "S" And Vettore_Orari_Giorno.Modificati <> "S" Then

                                        Call CaricaVettoreOrariOrdiniServizioUltimoGiorno(ConnectionString, i, StringaDb(myPOSTreader_1.Item("PrimoGiornoSuccessivo")), W_InAlto, W_InBasso, Vettore_Orari_Giorno, myPOSTreader, Vettore_Orari_Giorno.PrimoOrario)

                                        If i < 14 Then i = i + 1
                                    End If
                                End If
                            End If
                        End If
                        If StringaDb(myPOSTreader_1.Item("SecondoOrario")) <> "" Then
                            Vettore_Orari_Giorno.SecondoOrario = StringaDb(myPOSTreader_1.Item("SecondoOrario"))
                            If StringaDb(myPOSTreader_1.Item("SecondoGiornoSuccessivo")) = "S" Then
                                If Format(DataDb(myPOSTreader.Item("Data")), "yyyyMMdd") = Format(DataPrec, "yyyyMMdd") Then
                                    If Vettore_Orari_Giorno.Variati <> "P" And Vettore_Orari_Giorno.Variati <> "S" And Vettore_Orari_Giorno.Modificati <> "S" Then

                                        Call CaricaVettoreOrariOrdiniServizioUltimoGiorno(ConnectionString, i, StringaDb(myPOSTreader_1.Item("SecondoGiornoSuccessivo")), W_InAlto, W_InBasso, Vettore_Orari_Giorno, myPOSTreader, Vettore_Orari_Giorno.SecondoOrario)

                                        If i < 14 Then i = i + 1
                                    End If
                                End If
                            Else
                                If Format(DataDb(myPOSTreader.Item("Data")), "yyyyMMdd") = Format(Data, "yyyyMMdd") Then
                                    If Vettore_Orari_Giorno.Variati <> "P" And Vettore_Orari_Giorno.Variati <> "S" And Vettore_Orari_Giorno.Modificati <> "S" Then

                                        Call CaricaVettoreOrariOrdiniServizioUltimoGiorno(ConnectionString, i, StringaDb(myPOSTreader_1.Item("SecondoGiornoSuccessivo")), W_InAlto, W_InBasso, Vettore_Orari_Giorno, myPOSTreader, Vettore_Orari_Giorno.SecondoOrario)

                                        If i < 14 Then i = i + 1
                                    End If
                                End If
                            End If
                        End If
                        If StringaDb(myPOSTreader_1.Item("TerzoOrario")) <> "" Then
                            Vettore_Orari_Giorno.TerzoOrario = StringaDb(myPOSTreader_1.Item("TerzoOrario"))
                            If StringaDb(myPOSTreader_1.Item("TerzoGiornoSuccessivo")) = "S" Then
                                If Format(DataDb(myPOSTreader.Item("Data")), "yyyyMMdd") = Format(DataPrec, "yyyyMMdd") Then
                                    If Vettore_Orari_Giorno.Variati <> "P" And Vettore_Orari_Giorno.Variati <> "S" And Vettore_Orari_Giorno.Modificati <> "S" Then

                                        Call CaricaVettoreOrariOrdiniServizioUltimoGiorno(ConnectionString, i, StringaDb(myPOSTreader_1.Item("TerzoGiornoSuccessivo")), W_InAlto, W_InBasso, Vettore_Orari_Giorno, myPOSTreader, Vettore_Orari_Giorno.TerzoOrario)

                                        If i < 14 Then i = i + 1
                                    End If
                                End If
                            Else
                                If Format(DataDb(myPOSTreader.Item("Data")), "yyyyMMdd") = Format(Data, "yyyyMMdd") Then
                                    If Vettore_Orari_Giorno.Variati <> "P" And Vettore_Orari_Giorno.Variati <> "S" And Vettore_Orari_Giorno.Modificati <> "S" Then

                                        Call CaricaVettoreOrariOrdiniServizioUltimoGiorno(ConnectionString, i, StringaDb(myPOSTreader_1.Item("TerzoGiornoSuccessivo")), W_InAlto, W_InBasso, Vettore_Orari_Giorno, myPOSTreader, Vettore_Orari_Giorno.TerzoOrario)

                                        If i < 14 Then i = i + 1
                                    End If
                                End If
                            End If
                        End If
                        If StringaDb(myPOSTreader_1.Item("QuartoOrario")) <> "" Then
                            Vettore_Orari_Giorno.QuartoOrario = StringaDb(myPOSTreader_1.Item("QuartoOrario"))
                            If StringaDb(myPOSTreader_1.Item("QuartoGiornoSuccessivo")) = "S" Then
                                If Format(DataDb(myPOSTreader.Item("Data")), "yyyyMMdd") = Format(DataPrec, "yyyyMMdd") Then
                                    If Vettore_Orari_Giorno.Variati <> "P" And Vettore_Orari_Giorno.Variati <> "S" And Vettore_Orari_Giorno.Modificati <> "S" Then

                                        Call CaricaVettoreOrariOrdiniServizioUltimoGiorno(ConnectionString, i, StringaDb(myPOSTreader_1.Item("QuartoGiornoSuccessivo")), W_InAlto, W_InBasso, Vettore_Orari_Giorno, myPOSTreader, Vettore_Orari_Giorno.QuartoOrario)

                                        If i < 14 Then i = i + 1
                                    End If
                                End If
                            Else
                                If Format(DataDb(myPOSTreader.Item("Data")), "yyyyMMdd") = Format(Data, "yyyyMMdd") Then
                                    If Vettore_Orari_Giorno.Variati <> "P" And Vettore_Orari_Giorno.Variati <> "S" And Vettore_Orari_Giorno.Modificati <> "S" Then

                                        Call CaricaVettoreOrariOrdiniServizioUltimoGiorno(ConnectionString, i, StringaDb(myPOSTreader_1.Item("QuartoGiornoSuccessivo")), W_InAlto, W_InBasso, Vettore_Orari_Giorno, myPOSTreader, Vettore_Orari_Giorno.QuartoOrario)

                                        If i < 14 Then i = i + 1
                                    End If
                                End If
                            End If
                        End If
                        If StringaDb(myPOSTreader_1.Item("QuintoOrario")) <> "" Then
                            Vettore_Orari_Giorno.QuintoOrario = StringaDb(myPOSTreader_1.Item("QuintoOrario"))
                            If StringaDb(myPOSTreader_1.Item("QuintoGiornoSuccessivo")) = "S" Then
                                If Format(DataDb(myPOSTreader.Item("Data")), "yyyyMMdd") = Format(DataPrec, "yyyyMMdd") Then
                                    If Vettore_Orari_Giorno.Variati <> "P" And Vettore_Orari_Giorno.Variati <> "S" And Vettore_Orari_Giorno.Modificati <> "S" Then

                                        Call CaricaVettoreOrariOrdiniServizioUltimoGiorno(ConnectionString, i, StringaDb(myPOSTreader_1.Item("QuintoGiornoSuccessivo")), W_InAlto, W_InBasso, Vettore_Orari_Giorno, myPOSTreader, Vettore_Orari_Giorno.QuintoOrario)

                                        If i < 14 Then i = i + 1
                                    End If
                                End If
                            Else
                                If Format(DataDb(myPOSTreader.Item("Data")), "yyyyMMdd") = Format(Data, "yyyyMMdd") Then
                                    If Vettore_Orari_Giorno.Variati <> "P" And Vettore_Orari_Giorno.Variati <> "S" And Vettore_Orari_Giorno.Modificati <> "S" Then

                                        Call CaricaVettoreOrariOrdiniServizioUltimoGiorno(ConnectionString, i, StringaDb(myPOSTreader_1.Item("QuintoGiornoSuccessivo")), W_InAlto, W_InBasso, Vettore_Orari_Giorno, myPOSTreader, Vettore_Orari_Giorno.QuintoOrario)

                                        If i < 14 Then i = i + 1
                                    End If
                                End If
                            End If
                        End If
                    End If
                    myPOSTreader_1.Close()

                Else

                    cmd_1.Cancel()

                    cmd_1.CommandText = "SELECT * FROM OrdiniServizioRigaOrarioComandanti" & _
                                        " WHERE Data = ?" & _
                                        " AND Gruppo = ?" & _
                                        " AND Tipo = ?" & _
                                        " AND Riga = ?"
                    cmd_1.Parameters.Clear()
                    cmd_1.Parameters.AddWithValue("@Data", DataDb(myPOSTreader.Item("Data")))
                    cmd_1.Parameters.AddWithValue("@Gruppo", StringaDb(myPOSTreader.Item("GruppoComandante")))
                    cmd_1.Parameters.AddWithValue("@Tipo", StringaDb(myPOSTreader.Item("TipoComandante")))
                    cmd_1.Parameters.AddWithValue("@Riga", NumeroDb(myPOSTreader.Item("Riga")))

                    cmd_1.Connection = cn
                    myPOSTreader_1 = cmd_1.ExecuteReader()
                    If Not myPOSTreader_1.Read Then
                        myPOSTreader_1.Close()

                        cmd_1.Cancel()

                        cmd_1.CommandText = "SELECT * FROM OrdiniServizioRigaProfiloComandanti" & _
                                            " WHERE Data = ?" & _
                                            " AND Gruppo = ?" & _
                                            " AND Tipo = ?" & _
                                            " AND Riga = ?"
                        cmd_1.Parameters.Clear()
                        cmd_1.Parameters.AddWithValue("@Data", DataDb(myPOSTreader.Item("Data")))
                        cmd_1.Parameters.AddWithValue("@Gruppo", StringaDb(myPOSTreader.Item("GruppoComandante")))
                        cmd_1.Parameters.AddWithValue("@Tipo", StringaDb(myPOSTreader.Item("TipoComandante")))
                        cmd_1.Parameters.AddWithValue("@Riga", NumeroDb(myPOSTreader.Item("Riga")))

                        cmd_1.Connection = cn
                        myPOSTreader_1 = cmd_1.ExecuteReader()

                        If Not myPOSTreader_1.Read Then
                            If Format(DataDb(myPOSTreader.Item("Data")), "yyyyMMdd") = Format(Data, "yyyyMMdd") Then
                                If Vettore_Orari_Giorno.Variati <> "P" And Vettore_Orari_Giorno.Variati <> "S" And Vettore_Orari_Giorno.Modificati <> "S" Then
                                    Vettore_Orari_Giorno.Giustificativo(i) = StringaDb(myPOSTreader.Item("Servizio"))
                                    Vettore_Orari_Giorno.Familiare(i) = NumeroDb(myPOSTreader.Item("CodiceFamiliare"))
                                    Vettore_Orari_Giorno.Tipo(i) = cg.CampoGiustificativi(ConnectionString, Vettore_Orari_Giorno.Giustificativo(i), "Tipo")
                                    Vettore_Orari_Giorno.TipoServizio(i) = StringaDb(myPOSTreader.Item("TipoComandante"))
                                    If i < 14 Then i = i + 1
                                End If
                            End If
                        Else
                            Do
                                If StringaDb(myPOSTreader_1.Item("GiornoSuccessivo")) = "S" Then
                                    If Format(DataDb(myPOSTreader.Item("Data")), "yyyyMMdd") = Format(DataPrec, "yyyyMMdd") Then
                                        If Vettore_Orari_Giorno.Variati <> "P" And Vettore_Orari_Giorno.Variati <> "S" And Vettore_Orari_Giorno.Modificati <> "S" Then
                                            Vettore_Orari_Giorno.Dalle(i) = Format(DataDb(myPOSTreader_1.Item("Dalle")), "HH.mm")
                                            Vettore_Orari_Giorno.DalleAlto(i) = Format(DataDb(myPOSTreader_1.Item("Dalle")), "HH.mm")
                                            Vettore_Orari_Giorno.DalleBasso(i) = Format(DataDb(myPOSTreader_1.Item("Dalle")), "HH.mm")
                                            '                Vettore_Orari_Giorno.DalleObbligo(i) = Format(DataDb(myPOSTreader_1.Item("DalleObbligo")), "HH.mm")
                                            Vettore_Orari_Giorno.Alle(i) = Format(DataDb(myPOSTreader_1.Item("Alle")), "HH.mm")
                                            Vettore_Orari_Giorno.AlleAlto(i) = Format(DataDb(myPOSTreader_1.Item("Alle")), "HH.mm")
                                            Vettore_Orari_Giorno.AlleBasso(i) = Format(DataDb(myPOSTreader_1.Item("Alle")), "HH.mm")
                                            '                Vettore_Orari_Giorno.AlleObbligo(i) = Format(DataDb(myPOSTreader_1.Item("AlleObbligo")), "HH.mm")
                                            Vettore_Orari_Giorno.Pausa(i) = Format(DataDb(myPOSTreader_1.Item("Pausa")), "HH.mm")
                                            If Vettore_Orari_Giorno.TipoFlessibilita = "F" Or Vettore_Orari_Giorno.TipoFlessibilita = "X" Then
                                                If W_InAlto <> 0 Then
                                                    Vettore_Orari_Giorno.DalleAlto(i) = Format(DateAdd("n", W_InAlto, DataDb(myPOSTreader_1.Item("Dalle"))), "HH.mm")
                                                    Vettore_Orari_Giorno.AlleAlto(i) = Format(DateAdd("n", W_InAlto, DataDb(myPOSTreader_1.Item("Alle"))), "HH.mm")
                                                End If
                                                If W_InBasso <> 0 Then
                                                    Vettore_Orari_Giorno.DalleBasso(i) = Format(DateAdd("n", W_InBasso, DataDb(myPOSTreader_1.Item("Dalle"))), "HH.mm")
                                                    Vettore_Orari_Giorno.AlleBasso(i) = Format(DateAdd("n", W_InBasso, DataDb(myPOSTreader_1.Item("Alle"))), "HH.mm")
                                                End If
                                            End If
                                            Vettore_Orari_Giorno.Giustificativo(i) = StringaDb(myPOSTreader.Item("Servizio"))
                                            Vettore_Orari_Giorno.Familiare(i) = NumeroDb(myPOSTreader.Item("CodiceFamiliare"))
                                            Vettore_Orari_Giorno.Tipo(i) = cg.CampoGiustificativi(ConnectionString, Vettore_Orari_Giorno.Giustificativo(i), "Tipo")
                                            Vettore_Orari_Giorno.TipoServizio(i) = StringaDb(myPOSTreader.Item("TipoComandante"))
                                            Vettore_Orari_Giorno.GiornoSuccessivo(i) = StringaDb(myPOSTreader_1.Item("GiornoSuccessivo"))
                                            If i < 14 Then i = i + 1
                                        End If
                                    End If
                                Else
                                    If Format(DataDb(myPOSTreader.Item("Data")), "yyyyMMdd") = Format(Data, "yyyyMMdd") Then
                                        If Vettore_Orari_Giorno.Variati <> "P" And Vettore_Orari_Giorno.Variati <> "S" And Vettore_Orari_Giorno.Modificati <> "S" Then
                                            Vettore_Orari_Giorno.Dalle(i) = Format(DataDb(myPOSTreader_1.Item("Dalle")), "HH.mm")
                                            Vettore_Orari_Giorno.DalleAlto(i) = Format(DataDb(myPOSTreader_1.Item("Dalle")), "HH.mm")
                                            Vettore_Orari_Giorno.DalleBasso(i) = Format(DataDb(myPOSTreader_1.Item("Dalle")), "HH.mm")
                                            '                Vettore_Orari_Giorno.DalleObbligo(i) = Format(DataDb(myPOSTreader_1.Item("DalleObbligo")), "HH.mm")
                                            Vettore_Orari_Giorno.Alle(i) = Format(DataDb(myPOSTreader_1.Item("Alle")), "HH.mm")
                                            Vettore_Orari_Giorno.AlleAlto(i) = Format(DataDb(myPOSTreader_1.Item("Alle")), "HH.mm")
                                            Vettore_Orari_Giorno.AlleBasso(i) = Format(DataDb(myPOSTreader_1.Item("Alle")), "HH.mm")
                                            '                Vettore_Orari_Giorno.AlleObbligo(i) = Format(DataDb(myPOSTreader_1.Item("AlleObbligo")), "HH.mm")
                                            Vettore_Orari_Giorno.Pausa(i) = Format(DataDb(myPOSTreader_1.Item("Pausa")), "HH.mm")
                                            If Vettore_Orari_Giorno.TipoFlessibilita = "F" Or Vettore_Orari_Giorno.TipoFlessibilita = "X" Then
                                                If W_InAlto <> 0 Then
                                                    Vettore_Orari_Giorno.DalleAlto(i) = Format(DateAdd("n", W_InAlto, DataDb(myPOSTreader_1.Item("Dalle"))), "HH.mm")
                                                    Vettore_Orari_Giorno.AlleAlto(i) = Format(DateAdd("n", W_InAlto, DataDb(myPOSTreader_1.Item("Alle"))), "HH.mm")
                                                End If
                                                If W_InBasso <> 0 Then
                                                    Vettore_Orari_Giorno.DalleBasso(i) = Format(DateAdd("n", W_InBasso, DataDb(myPOSTreader_1.Item("Dalle"))), "HH.mm")
                                                    Vettore_Orari_Giorno.AlleBasso(i) = Format(DateAdd("n", W_InBasso, DataDb(myPOSTreader_1.Item("Alle"))), "HH.mm")
                                                End If
                                            End If
                                            Vettore_Orari_Giorno.Giustificativo(i) = StringaDb(myPOSTreader.Item("Servizio"))
                                            Vettore_Orari_Giorno.Familiare(i) = NumeroDb(myPOSTreader.Item("CodiceFamiliare"))
                                            Vettore_Orari_Giorno.Tipo(i) = cg.CampoGiustificativi(ConnectionString, Vettore_Orari_Giorno.Giustificativo(i), "Tipo")
                                            Vettore_Orari_Giorno.TipoServizio(i) = StringaDb(myPOSTreader.Item("TipoComandante"))
                                            Vettore_Orari_Giorno.GiornoSuccessivo(i) = StringaDb(myPOSTreader_1.Item("GiornoSuccessivo"))
                                            If i < 14 Then i = i + 1
                                        End If
                                    End If
                                End If
                            Loop While myPOSTreader.Read
                        End If

                    Else

                        If StringaDb(myPOSTreader_1.Item("PrimoOrario")) <> "" Then
                            Vettore_Orari_Giorno.PrimoOrario = StringaDb(myPOSTreader_1.Item("PrimoOrario"))
                            If StringaDb(myPOSTreader_1.Item("PrimoGiornoSuccessivo")) = "S" Then
                                If Format(DataDb(myPOSTreader.Item("Data")), "yyyyMMdd") = Format(DataPrec, "yyyyMMdd") Then
                                    If Vettore_Orari_Giorno.Variati <> "P" And Vettore_Orari_Giorno.Variati <> "S" And Vettore_Orari_Giorno.Modificati <> "S" Then

                                        Call CaricaVettoreOrariOrdiniServizioUltimoGiorno(ConnectionString, i, StringaDb(myPOSTreader_1.Item("PrimoGiornoSuccessivo")), W_InAlto, W_InBasso, Vettore_Orari_Giorno, myPOSTreader, Vettore_Orari_Giorno.PrimoOrario)

                                        If i < 14 Then i = i + 1
                                    End If
                                End If
                            Else
                                If Format(DataDb(myPOSTreader.Item("Data")), "yyyyMMdd") = Format(Data, "yyyyMMdd") Then
                                    If Vettore_Orari_Giorno.Variati <> "P" And Vettore_Orari_Giorno.Variati <> "S" And Vettore_Orari_Giorno.Modificati <> "S" Then

                                        Call CaricaVettoreOrariOrdiniServizioUltimoGiorno(ConnectionString, i, StringaDb(myPOSTreader_1.Item("PrimoGiornoSuccessivo")), W_InAlto, W_InBasso, Vettore_Orari_Giorno, myPOSTreader, Vettore_Orari_Giorno.PrimoOrario)

                                        If i < 14 Then i = i + 1
                                    End If
                                End If
                            End If
                        End If
                        If StringaDb(myPOSTreader_1.Item("SecondoOrario")) <> "" Then
                            Vettore_Orari_Giorno.SecondoOrario = StringaDb(myPOSTreader_1.Item("SecondoOrario"))
                            If StringaDb(myPOSTreader_1.Item("SecondoGiornoSuccessivo")) = "S" Then
                                If Format(DataDb(myPOSTreader.Item("Data")), "yyyyMMdd") = Format(DataPrec, "yyyyMMdd") Then
                                    If Vettore_Orari_Giorno.Variati <> "P" And Vettore_Orari_Giorno.Variati <> "S" And Vettore_Orari_Giorno.Modificati <> "S" Then

                                        Call CaricaVettoreOrariOrdiniServizioUltimoGiorno(ConnectionString, i, StringaDb(myPOSTreader_1.Item("SecondoGiornoSuccessivo")), W_InAlto, W_InBasso, Vettore_Orari_Giorno, myPOSTreader, Vettore_Orari_Giorno.SecondoOrario)

                                        If i < 14 Then i = i + 1
                                    End If
                                End If
                            Else
                                If Format(DataDb(myPOSTreader.Item("Data")), "yyyyMMdd") = Format(Data, "yyyyMMdd") Then
                                    If Vettore_Orari_Giorno.Variati <> "P" And Vettore_Orari_Giorno.Variati <> "S" And Vettore_Orari_Giorno.Modificati <> "S" Then

                                        Call CaricaVettoreOrariOrdiniServizioUltimoGiorno(ConnectionString, i, StringaDb(myPOSTreader_1.Item("SecondoGiornoSuccessivo")), W_InAlto, W_InBasso, Vettore_Orari_Giorno, myPOSTreader, Vettore_Orari_Giorno.SecondoOrario)

                                        If i < 14 Then i = i + 1
                                    End If
                                End If
                            End If
                        End If
                        If StringaDb(myPOSTreader_1.Item("TerzoOrario")) <> "" Then
                            Vettore_Orari_Giorno.TerzoOrario = StringaDb(myPOSTreader_1.Item("TerzoOrario"))
                            If StringaDb(myPOSTreader_1.Item("TerzoGiornoSuccessivo")) = "S" Then
                                If Format(DataDb(myPOSTreader.Item("Data")), "yyyyMMdd") = Format(DataPrec, "yyyyMMdd") Then
                                    If Vettore_Orari_Giorno.Variati <> "P" And Vettore_Orari_Giorno.Variati <> "S" And Vettore_Orari_Giorno.Modificati <> "S" Then

                                        Call CaricaVettoreOrariOrdiniServizioUltimoGiorno(ConnectionString, i, StringaDb(myPOSTreader_1.Item("TerzoGiornoSuccessivo")), W_InAlto, W_InBasso, Vettore_Orari_Giorno, myPOSTreader, Vettore_Orari_Giorno.TerzoOrario)

                                        If i < 14 Then i = i + 1
                                    End If
                                End If
                            Else
                                If Format(DataDb(myPOSTreader.Item("Data")), "yyyyMMdd") = Format(Data, "yyyyMMdd") Then
                                    If Vettore_Orari_Giorno.Variati <> "P" And Vettore_Orari_Giorno.Variati <> "S" And Vettore_Orari_Giorno.Modificati <> "S" Then

                                        Call CaricaVettoreOrariOrdiniServizioUltimoGiorno(ConnectionString, i, StringaDb(myPOSTreader_1.Item("TerzoGiornoSuccessivo")), W_InAlto, W_InBasso, Vettore_Orari_Giorno, myPOSTreader, Vettore_Orari_Giorno.TerzoOrario)

                                        If i < 14 Then i = i + 1
                                    End If
                                End If
                            End If
                        End If
                        If StringaDb(myPOSTreader_1.Item("QuartoOrario")) <> "" Then
                            Vettore_Orari_Giorno.QuartoOrario = StringaDb(myPOSTreader_1.Item("QuartoOrario"))
                            If StringaDb(myPOSTreader_1.Item("QuartoGiornoSuccessivo")) = "S" Then
                                If Format(DataDb(myPOSTreader.Item("Data")), "yyyyMMdd") = Format(DataPrec, "yyyyMMdd") Then
                                    If Vettore_Orari_Giorno.Variati <> "P" And Vettore_Orari_Giorno.Variati <> "S" And Vettore_Orari_Giorno.Modificati <> "S" Then

                                        Call CaricaVettoreOrariOrdiniServizioUltimoGiorno(ConnectionString, i, StringaDb(myPOSTreader_1.Item("QuartoGiornoSuccessivo")), W_InAlto, W_InBasso, Vettore_Orari_Giorno, myPOSTreader, Vettore_Orari_Giorno.QuartoOrario)

                                        If i < 14 Then i = i + 1
                                    End If
                                End If
                            Else
                                If Format(DataDb(myPOSTreader.Item("Data")), "yyyyMMdd") = Format(Data, "yyyyMMdd") Then
                                    If Vettore_Orari_Giorno.Variati <> "P" And Vettore_Orari_Giorno.Variati <> "S" And Vettore_Orari_Giorno.Modificati <> "S" Then

                                        Call CaricaVettoreOrariOrdiniServizioUltimoGiorno(ConnectionString, i, StringaDb(myPOSTreader_1.Item("QuartoGiornoSuccessivo")), W_InAlto, W_InBasso, Vettore_Orari_Giorno, myPOSTreader, Vettore_Orari_Giorno.QuartoOrario)

                                        If i < 14 Then i = i + 1
                                    End If
                                End If
                            End If
                        End If
                        If StringaDb(myPOSTreader_1.Item("QuintoOrario")) <> "" Then
                            Vettore_Orari_Giorno.QuintoOrario = StringaDb(myPOSTreader_1.Item("QuintoOrario"))
                            If StringaDb(myPOSTreader_1.Item("QuintoGiornoSuccessivo")) = "S" Then
                                If Format(DataDb(myPOSTreader.Item("Data")), "yyyyMMdd") = Format(DataPrec, "yyyyMMdd") Then
                                    If Vettore_Orari_Giorno.Variati <> "P" And Vettore_Orari_Giorno.Variati <> "S" And Vettore_Orari_Giorno.Modificati <> "S" Then

                                        Call CaricaVettoreOrariOrdiniServizioUltimoGiorno(ConnectionString, i, StringaDb(myPOSTreader_1.Item("QuintoGiornoSuccessivo")), W_InAlto, W_InBasso, Vettore_Orari_Giorno, myPOSTreader, Vettore_Orari_Giorno.QuintoOrario)

                                        If i < 14 Then i = i + 1
                                    End If
                                End If
                            Else
                                If Format(DataDb(myPOSTreader.Item("Data")), "yyyyMMdd") = Format(Data, "yyyyMMdd") Then
                                    If Vettore_Orari_Giorno.Variati <> "P" And Vettore_Orari_Giorno.Variati <> "S" And Vettore_Orari_Giorno.Modificati <> "S" Then

                                        Call CaricaVettoreOrariOrdiniServizioUltimoGiorno(ConnectionString, i, StringaDb(myPOSTreader_1.Item("QuintoGiornoSuccessivo")), W_InAlto, W_InBasso, Vettore_Orari_Giorno, myPOSTreader, Vettore_Orari_Giorno.QuintoOrario)

                                        If i < 14 Then i = i + 1
                                    End If
                                End If
                            End If
                        End If
                    End If
                    myPOSTreader_1.Close()
                End If
            Loop While myPOSTreader.Read
        End If

        myPOSTreader.Close()
        '
        ' Ordina il Vettore_Orari_Giorno
        '
        For i = 0 To 14
            If Vettore_Orari_Giorno.Giustificativo(i) <> "" And Vettore_Orari_Giorno.Dalle(i) = "00.00" And Vettore_Orari_Giorno.Alle(i) = "00.00" Then
                Vettore_Orari_Giorno.Dalle(i) = "24.00"
                If ZeroAlle = "N" Then
                    Vettore_Orari_Giorno.Alle(i) = "24.00"
                    Vettore_Orari_Giorno.AlleAlto(i) = "24.00"
                    Vettore_Orari_Giorno.AlleBasso(i) = "24.00"
                End If
            End If
        Next i

        Sw_Ok = False
        Do Until Sw_Ok = True
            Sw_Ok = True
            For i = 1 To 14
                If Vettore_Orari_Giorno.Giustificativo(i) = "" And Vettore_Orari_Giorno.Dalle(i) = "00.00" And Vettore_Orari_Giorno.Alle(i) = "00.00" Then Exit For
                If Vettore_Orari_Giorno.Dalle(i - 1) > Vettore_Orari_Giorno.Dalle(i) Then
                    Sw_Ok = False
                    Dim W_Dalle As String = Vettore_Orari_Giorno.Dalle(i)
                    Dim W_DalleAlto As String = Vettore_Orari_Giorno.DalleAlto(i)
                    Dim W_DalleBasso As String = Vettore_Orari_Giorno.DalleBasso(i)
                    Dim W_DalleObbligo As String = Vettore_Orari_Giorno.DalleObbligo(i)
                    '        W_DCT = Vettore_Orari_Giorno.DCT(i)
                    Dim W_Alle As String = Vettore_Orari_Giorno.Alle(i)
                    Dim W_AlleAlto As String = Vettore_Orari_Giorno.AlleAlto(i)
                    Dim W_AlleBasso As String = Vettore_Orari_Giorno.AlleBasso(i)
                    Dim W_AlleObbligo As String = Vettore_Orari_Giorno.AlleObbligo(i)
                    Dim W_Pausa As String = Vettore_Orari_Giorno.Pausa(i)
                    Dim W_Giustificativo As String = Vettore_Orari_Giorno.Giustificativo(i)
                    Dim W_Familiare As Byte = Vettore_Orari_Giorno.Familiare(i)
                    Dim W_Tipo As String = Vettore_Orari_Giorno.Tipo(i)
                    Dim W_TipoServizio As String = Vettore_Orari_Giorno.TipoServizio(i)
                    Dim W_GiornoSuccessivo As String = Vettore_Orari_Giorno.GiornoSuccessivo(i)

                    Vettore_Orari_Giorno.Dalle(i) = Vettore_Orari_Giorno.Dalle(i - 1)
                    Vettore_Orari_Giorno.DalleAlto(i) = Vettore_Orari_Giorno.DalleAlto(i - 1)
                    Vettore_Orari_Giorno.DalleBasso(i) = Vettore_Orari_Giorno.DalleBasso(i - 1)
                    Vettore_Orari_Giorno.DalleObbligo(i) = Vettore_Orari_Giorno.DalleObbligo(i - 1)
                    '        Vettore_Orari_Giorno.DCT(i) = Vettore_Orari_Giorno.DCT(i - 1)
                    Vettore_Orari_Giorno.Alle(i) = Vettore_Orari_Giorno.Alle(i - 1)
                    Vettore_Orari_Giorno.AlleAlto(i) = Vettore_Orari_Giorno.AlleAlto(i - 1)
                    Vettore_Orari_Giorno.AlleBasso(i) = Vettore_Orari_Giorno.AlleBasso(i - 1)
                    Vettore_Orari_Giorno.AlleObbligo(i) = Vettore_Orari_Giorno.AlleObbligo(i - 1)
                    Vettore_Orari_Giorno.Pausa(i) = Vettore_Orari_Giorno.Pausa(i - 1)
                    Vettore_Orari_Giorno.Giustificativo(i) = Vettore_Orari_Giorno.Giustificativo(i - 1)
                    Vettore_Orari_Giorno.Familiare(i) = Vettore_Orari_Giorno.Familiare(i - 1)
                    Vettore_Orari_Giorno.Tipo(i) = Vettore_Orari_Giorno.Tipo(i - 1)
                    Vettore_Orari_Giorno.TipoServizio(i) = Vettore_Orari_Giorno.TipoServizio(i - 1)
                    Vettore_Orari_Giorno.GiornoSuccessivo(i) = Vettore_Orari_Giorno.GiornoSuccessivo(i - 1)

                    Vettore_Orari_Giorno.Dalle(i - 1) = W_Dalle
                    Vettore_Orari_Giorno.DalleAlto(i - 1) = W_DalleAlto
                    Vettore_Orari_Giorno.DalleBasso(i - 1) = W_DalleBasso
                    Vettore_Orari_Giorno.DalleObbligo(i - 1) = W_DalleObbligo
                    '        Vettore_Orari_Giorno.DCT(i - 1) = W_DCT
                    Vettore_Orari_Giorno.Alle(i - 1) = W_Alle
                    Vettore_Orari_Giorno.AlleAlto(i - 1) = W_AlleAlto
                    Vettore_Orari_Giorno.AlleBasso(i - 1) = W_AlleBasso
                    Vettore_Orari_Giorno.AlleObbligo(i - 1) = W_AlleObbligo
                    Vettore_Orari_Giorno.Pausa(i - 1) = W_Pausa
                    Vettore_Orari_Giorno.Giustificativo(i - 1) = W_Giustificativo
                    Vettore_Orari_Giorno.Familiare(i - 1) = W_Familiare
                    Vettore_Orari_Giorno.Tipo(i - 1) = W_Tipo
                    Vettore_Orari_Giorno.TipoServizio(i - 1) = W_TipoServizio
                    Vettore_Orari_Giorno.GiornoSuccessivo(i - 1) = W_GiornoSuccessivo
                End If
            Next i
        Loop
        For i = 0 To 14
            If Vettore_Orari_Giorno.Dalle(i) = "24.00" Then Vettore_Orari_Giorno.Dalle(i) = "00.00"
        Next i
    End Function

    Private Sub InserisciFerie_STOSSG(ByVal ConnectionString As String, ByVal ControlloNotte As String, ByVal ControlloSmontoNotte As String, ByVal ControlloRiposo As String, ByVal CodiceIntervallo As String, ByVal Data As Date, ByRef Vettore_Orari_Giorno As Cls_Struttura_Orari_Giorno)
        Dim ci As New Cls_Intervalli
        Dim cpt As New Cls_ParametriTurni
        cpt.Leggi(ConnectionString)

        Dim IntervalloDal As Date
        Dim IntervalloAl As Date
        Dim Sw_Ok As Boolean

        ci.Leggi(ConnectionString, UCase(CodiceIntervallo))

        IntervalloDal = DateSerial(Year(Data), ci.DalMese, ci.DalGiorno)
        IntervalloAl = DateSerial(Year(Data), ci.AlMese, ci.AlGiorno)

        If Format(IntervalloDal, "yyyymmdd") <= Format(Data, "yyyymmdd") And Format(IntervalloAl, "yyyymmdd") >= Format(Data, "yyyymmdd") Then
            Sw_Ok = True
            If Format(IntervalloDal, "yyyymmdd") = Format(Data, "yyyymmdd") Then
                If ControlloNotte = "S" Then
                    For i = 0 To 4
                        If Vettore_Orari_Giorno.Giustificativo(i) = cpt.GiustificativoNotte Then Sw_Ok = False
                    Next i
                End If
                If ControlloSmontoNotte = "S" Then
                    For i = 0 To 4
                        If Vettore_Orari_Giorno.Giustificativo(i) = cpt.GiustificativoSmontoNotte Then Sw_Ok = False
                    Next i
                End If
                If ControlloRiposo = "S" Then
                    For i = 0 To 4
                        If Vettore_Orari_Giorno.Giustificativo(i) = cpt.GiustificativoRiposo Then Sw_Ok = False
                    Next i
                End If
            End If
            If Sw_Ok = True Then
                Call CaricaVettoreOrariUltimoGiornoFerie(ConnectionString, Vettore_Orari_Giorno, cpt.CodiceOrarioFerie)
            End If
        End If
    End Sub

    Private Sub CaricaVettoreOrariUltimoGiorno(ByVal ConnectionString As String, ByVal i As Byte, ByVal W_InAlto As Integer, ByVal W_InBasso As Integer, ByRef Vettore_Orari_Giorno As Cls_Struttura_Orari_Giorno, ByVal Orario As String)
        Dim cpt As New Cls_ParametriTurni
        Dim cg As New Cls_Giustificativi
        Dim co As New Cls_Orari

        cpt.Leggi(ConnectionString)
        co.Leggi(ConnectionString, Orario)
        If co.Id > 0 Then
            Vettore_Orari_Giorno.Dalle(i) = Format(co.Dalle, "HH.mm")
            Vettore_Orari_Giorno.DalleAlto(i) = Format(co.Dalle, "HH.mm")
            Vettore_Orari_Giorno.DalleBasso(i) = Format(co.Dalle, "HH.mm")
            Vettore_Orari_Giorno.Alle(i) = Format(co.Alle, "HH.mm")
            Vettore_Orari_Giorno.AlleAlto(i) = Format(co.Alle, "HH.mm")
            Vettore_Orari_Giorno.AlleBasso(i) = Format(co.Alle, "HH.mm")
            If Vettore_Orari_Giorno.TipoFlessibilita = "F" Or Vettore_Orari_Giorno.TipoFlessibilita = "X" Then
                Vettore_Orari_Giorno.DalleObbligo(i) = Format(co.DalleObbligo, "HH.mm")
                Vettore_Orari_Giorno.AlleObbligo(i) = Format(co.AlleObbligo, "HH.mm")
                If co.DalleAlto <> #12/30/1899# Then Vettore_Orari_Giorno.DalleAlto(i) = Format(co.DalleAlto, "HH.mm")
                If co.DalleBasso <> #12/30/1899# Then Vettore_Orari_Giorno.DalleBasso(i) = Format(co.DalleBasso, "HH.mm")
                If co.AlleAlto <> #12/30/1899# Then Vettore_Orari_Giorno.AlleAlto(i) = Format(co.AlleAlto, "HH.mm")
                If co.AlleBasso <> #12/30/1899# Then Vettore_Orari_Giorno.AlleBasso(i) = Format(co.AlleBasso, "HH.mm")
                If W_InAlto <> 0 Then
                    Vettore_Orari_Giorno.DalleAlto(i) = Format(DateAdd("n", W_InAlto, co.Dalle), "HH.mm")
                    Vettore_Orari_Giorno.AlleAlto(i) = Format(DateAdd("n", W_InAlto, co.Alle), "HH.mm")
                End If
                If W_InBasso <> 0 Then
                    Vettore_Orari_Giorno.DalleBasso(i) = Format(DateAdd("n", W_InBasso, co.Dalle), "HH.mm")
                    Vettore_Orari_Giorno.AlleBasso(i) = Format(DateAdd("n", W_InBasso, co.Alle), "HH.mm")
                End If
            End If
            Vettore_Orari_Giorno.Pausa(i) = Format(co.Pausa, "HH.mm")
            Vettore_Orari_Giorno.Giustificativo(i) = co.Giustificativo
            Vettore_Orari_Giorno.Familiare(i) = 0
            If Vettore_Orari_Giorno.Giustificativo(i) = "" Then
                Vettore_Orari_Giorno.Giustificativo(i) = cpt.GiustificativoOrarioLavoro
            Else
                If Format(co.Jolly, "HH.mm") <> 0 Then
                    Vettore_Orari_Giorno.Alle(i) = Format(co.Jolly, "HH.mm")
                End If
            End If
            Vettore_Orari_Giorno.Tipo(i) = cg.CampoGiustificativi(ConnectionString, Vettore_Orari_Giorno.Giustificativo(i), "Tipo")
        End If
    End Sub

    Private Sub CaricaVettoreOrariUltimoGiornoFerie(ByVal ConnectionString As String, ByRef Vettore_Orari_Giorno As Cls_Struttura_Orari_Giorno, ByVal Orario As String)

        For i = 0 To 4
            Vettore_Orari_Giorno.Dalle(i) = "00.00"
            Vettore_Orari_Giorno.DalleAlto(i) = "00.00"
            Vettore_Orari_Giorno.DalleBasso(i) = "00.00"
            Vettore_Orari_Giorno.Alle(i) = "00.00"
            Vettore_Orari_Giorno.AlleAlto(i) = "00.00"
            Vettore_Orari_Giorno.AlleBasso(i) = "00.00"
            Vettore_Orari_Giorno.TipoFlessibilita = ""
            Vettore_Orari_Giorno.Pausa(i) = "00.00"
            Vettore_Orari_Giorno.Familiare(i) = 0
            Vettore_Orari_Giorno.Giustificativo(i) = ""
            Vettore_Orari_Giorno.Tipo(i) = ""
        Next i
        Call CaricaVettoreOrariUltimoGiorno(ConnectionString, 0, 0, 0, Vettore_Orari_Giorno, Orario)
    End Sub

    Private Sub CaricaVettoreOrariOrdiniServizioUltimoGiorno(ByVal ConnectionString As String, ByVal i As Byte, ByVal GiornoSuccessivo As String, ByVal W_InAlto As Integer, ByVal W_InBasso As Integer, ByRef Vettore_Orari_Giorno As Cls_Struttura_Orari_Giorno, ByRef myPOSTreader As OleDbDataReader, ByVal Orario As String)
        Dim cg As New Cls_Giustificativi
        Dim co As New Cls_Orari

        co.Leggi(ConnectionString, Orario)
        If co.Id > 0 Then
            Vettore_Orari_Giorno.Dalle(i) = Format(co.Dalle, "HH.mm")
            Vettore_Orari_Giorno.DalleAlto(i) = Format(co.Dalle, "HH.mm")
            Vettore_Orari_Giorno.DalleBasso(i) = Format(co.Dalle, "HH.mm")
            Vettore_Orari_Giorno.Alle(i) = Format(co.Alle, "HH.mm")
            Vettore_Orari_Giorno.AlleAlto(i) = Format(co.Alle, "HH.mm")
            Vettore_Orari_Giorno.AlleBasso(i) = Format(co.Alle, "HH.mm")
            If Vettore_Orari_Giorno.TipoFlessibilita = "F" Or Vettore_Orari_Giorno.TipoFlessibilita = "X" Then
                Vettore_Orari_Giorno.DalleObbligo(i) = Format(co.DalleObbligo, "HH.mm")
                Vettore_Orari_Giorno.AlleObbligo(i) = Format(co.AlleObbligo, "HH.mm")
                If co.DalleAlto <> #12/30/1899# Then Vettore_Orari_Giorno.DalleAlto(i) = Format(co.DalleAlto, "HH.mm")
                If co.DalleBasso <> #12/30/1899# Then Vettore_Orari_Giorno.DalleBasso(i) = Format(co.DalleBasso, "HH.mm")
                If co.AlleAlto <> #12/30/1899# Then Vettore_Orari_Giorno.AlleAlto(i) = Format(co.AlleAlto, "HH.mm")
                If co.AlleBasso <> #12/30/1899# Then Vettore_Orari_Giorno.AlleBasso(i) = Format(co.AlleBasso, "HH.mm")
                If W_InAlto <> 0 Then
                    Vettore_Orari_Giorno.DalleAlto(i) = Format(DateAdd("n", W_InAlto, co.Dalle), "HH.mm")
                    Vettore_Orari_Giorno.AlleAlto(i) = Format(DateAdd("n", W_InAlto, co.Alle), "HH.mm")
                End If
                If W_InBasso <> 0 Then
                    Vettore_Orari_Giorno.DalleBasso(i) = Format(DateAdd("n", W_InBasso, co.Dalle), "HH.mm")
                    Vettore_Orari_Giorno.AlleBasso(i) = Format(DateAdd("n", W_InBasso, co.Alle), "HH.mm")
                End If
            End If
            Vettore_Orari_Giorno.Pausa(i) = Format(co.Pausa, "HH.mm")
            Vettore_Orari_Giorno.Giustificativo(i) = StringaDb(myPOSTreader.Item("Servizio"))
            Vettore_Orari_Giorno.Familiare(i) = NumeroDb(myPOSTreader.Item("CodiceFamiliare"))
            If Not co.Giustificativo = "" And Not Format(co.Jolly, "HH.mm") = 0 Then
                Vettore_Orari_Giorno.Alle(i) = Format(co.Jolly, "HH.mm")
            End If
            Vettore_Orari_Giorno.Tipo(i) = cg.CampoGiustificativi(ConnectionString, Vettore_Orari_Giorno.Giustificativo(i), "Tipo")
            Vettore_Orari_Giorno.TipoServizio(i) = StringaDb(myPOSTreader.Item("TipoComandante"))
            Vettore_Orari_Giorno.GiornoSuccessivo(i) = GiornoSuccessivo
        End If
    End Sub

    Private Sub Carica_Vettore_Orari_ProfiliOrariVariati(ByVal ConnectionString As String, ByVal Dipendente As Object, ByVal DataMin As Date, ByVal DataMax As Date, ByVal DataSviluppo As Date, ByVal Sw_Variazioni As String, ByVal Sw_Proposta As String, ByRef Vettore_Orari As Cls_Struttura_Orari)
        Dim Giorni As Integer

        Dim cn As OleDbConnection
        cn = New Data.OleDb.OleDbConnection(ConnectionString)
        cn.Open()

        Dim cmd As New OleDbCommand()

        If Sw_Variazioni = "N" And Sw_Proposta = "N" Then Exit Sub

        If Sw_Variazioni = "S" Then
            cmd.CommandText = " SELECT * FROM ProfiliOrariVariati " & _
                              " WHERE CodiceDipendente = ?" & _
                              " AND Data >= ?" & _
                              " AND Data <= ?" & _
                              " ORDER BY Data"
        Else
            cmd.CommandText = " SELECT * FROM ProfiliOrariVariati " & _
                              " WHERE CodiceDipendente = ?" & _
                              " AND Proposta = 'S'" & _
                              " AND Data >= ?" & _
                              " AND Data <= ?" & _
                              " ORDER BY Data"
        End If
        cmd.Parameters.AddWithValue("@CodiceDipendente", Dipendente)
        cmd.Parameters.AddWithValue("@Data", DataMin)
        cmd.Parameters.AddWithValue("@Data", DataMax)

        cmd.Connection = cn
        Dim myPOSTreader As OleDbDataReader = cmd.ExecuteReader()
        Do While myPOSTreader.Read
            Giorni = DateDiff("d", DataSviluppo, DataDb(myPOSTreader.Item("Data")))

            If StringaDb(myPOSTreader.Item("Proposta")) = "S" Then
                Vettore_Orari.Variati(Giorni) = "P"
            Else
                Vettore_Orari.Variati(Giorni) = "S"
            End If
            Vettore_Orari.PrimoOrario(Giorni) = StringaDb(myPOSTreader.Item("PrimoOrario"))
            Vettore_Orari.SecondoOrario(Giorni) = StringaDb(myPOSTreader.Item("SecondoOrario"))
            Vettore_Orari.TerzoOrario(Giorni) = StringaDb(myPOSTreader.Item("TerzoOrario"))
            Vettore_Orari.QuartoOrario(Giorni) = StringaDb(myPOSTreader.Item("QuartoOrario"))
            Vettore_Orari.QuintoOrario(Giorni) = StringaDb(myPOSTreader.Item("QuintoOrario"))
            Vettore_Orari.NelGruppo(0, Giorni) = StringaDb(myPOSTreader.Item("PrimoNelGruppo"))
            Vettore_Orari.NelGruppo(1, Giorni) = StringaDb(myPOSTreader.Item("SecondoNelGruppo"))
            Vettore_Orari.NelGruppo(2, Giorni) = StringaDb(myPOSTreader.Item("TerzoNelGruppo"))
            Vettore_Orari.NelGruppo(3, Giorni) = StringaDb(myPOSTreader.Item("QuartoNelGruppo"))
            Vettore_Orari.NelGruppo(4, Giorni) = StringaDb(myPOSTreader.Item("QuintoNelGruppo"))
            Vettore_Orari.FasceOrarie(Giorni) = ""
        Loop
        myPOSTreader.Close()
        cn.Close()
    End Sub

    Private Sub Carica_Vettore_Orari_RichiesteAssenza(ByVal ConnectionString As String, ByVal CodiceSuperGruppo As String, ByVal Dipendente As Object, ByVal DataMin As Date, ByVal DataMax As Date, ByVal DataSviluppo As Date, ByRef Vettore_Orari As Cls_Struttura_Orari)
        Dim Chk_Giorno(6) As String
        Dim W_DataDal As Date
        Dim W_DataAl As Date
        Dim W_Dalle As String
        Dim W_Alle As String
        Dim W_Giustificativo As String
        Dim W_NelGruppo As String
        Dim W_Tipo As String
        Dim Chk_SuRiposo As String
        Dim Chk_RiposoSuFestivi As String
        Dim Chk_RiposoSuFestiviInfrasettimanali As String
        Dim Chk_RimuoviOrari As String
        Dim Stringa As String
        Dim Data As Date
        Dim g As Integer

        Dim cpt As New Cls_ParametriTurni
        cpt.Leggi(ConnectionString)

        Dim cxst As New Cls_ClsXScriptTurni
        cxst.ConnectionString = ConnectionString

        Dim cg As New Cls_Giustificativi

        Dim cn As OleDbConnection
        cn = New Data.OleDb.OleDbConnection(ConnectionString)
        cn.Open()

        Dim cmd As New OleDbCommand()

        cmd.CommandText = "SELECT * FROM RichiesteAssenza" & _
                          " WHERE CodiceDipendente = ?" & _
                          " AND Acquisito = 'S'" & _
                          " AND (Annullato Is Null OR Annullato = '')" & _
                          " AND (Revocato Is Null OR Revocato = '')" & _
                          " AND ((DataAl Is Null" & _
                          " AND DataDal >= ?" & _
                          " AND DataDal <= ?) " & _
                          " OR (NOT DataAl Is Null" & _
                          " AND DataDal <= ?" & _
                          " AND DataAl >= ?)) " & _
                          " ORDER BY DataDal, Dalle"
        cmd.Parameters.AddWithValue("@CodiceDipendente", Dipendente)
        cmd.Parameters.AddWithValue("@DataDal", DataMin)
        cmd.Parameters.AddWithValue("@DataDal", DataMax)
        cmd.Parameters.AddWithValue("@DataDal", DataMax)
        cmd.Parameters.AddWithValue("@DataAl", DataMin)

        cmd.Connection = cn
        Dim myPOSTreader As OleDbDataReader = cmd.ExecuteReader()
        Do While myPOSTreader.Read
            W_DataDal = DataDb(myPOSTreader.Item("DataDal"))
            W_DataAl = DataDb(myPOSTreader.Item("DataAl"))
            W_Dalle = Format(DataDb(myPOSTreader.Item("Dalle")), "hh.nn")
            W_Alle = Format(DataDb(myPOSTreader.Item("Alle")), "hh.nn")
            W_Giustificativo = StringaDb(myPOSTreader.Item("Giustificativo"))
            W_NelGruppo = ""
            W_Tipo = cg.CampoGiustificativi(ConnectionString, W_Giustificativo, "Tipo")

            Chk_SuRiposo = cg.CampoGiustificativi(ConnectionString, W_Giustificativo, "Assenza_SuRiposo")
            Chk_RiposoSuFestivi = cg.CampoGiustificativi(ConnectionString, W_Giustificativo, "Assenza_RiposoSuFestivi")
            Chk_RiposoSuFestiviInfrasettimanali = cg.CampoGiustificativi(ConnectionString, W_Giustificativo, "Assenza_RiposoSuFestiviInfrasettimanali")
            Chk_RimuoviOrari = cg.CampoGiustificativi(ConnectionString, W_Giustificativo, "Assenza_RimuoviOrari")
            Stringa = cg.CampoGiustificativi(ConnectionString, W_Giustificativo, "Assenza_ChkGiorno")
            For i = 1 To 6
                If Mid(Stringa, i, 1) = "S" Then
                    Chk_Giorno(i) = "S"
                Else
                    Chk_Giorno(i) = "N"
                End If
            Next i
            If Mid(Stringa, 7, 1) = "S" Then
                Chk_Giorno(0) = "S"
            Else
                Chk_Giorno(0) = "N"
            End If

            If Not IsDate(W_DataAl) Then W_DataAl = W_DataDal

            Data = W_DataDal
            Do Until Format(Data, "yyyymmdd") > Format(W_DataAl, "yyyymmdd")

                g = DateDiff("d", DataSviluppo, Data)

                If W_DataDal = W_DataAl And (W_Dalle <> "00.00" Or W_Alle <> "00.00") Then

                    Vettore_Orari.Richiesti(g) = "S"

                    For i = 0 To 9
                        If Vettore_Orari.Giustificativo(i, g) <> "" Then
                            If Vettore_Orari.Dalle(i, g) = "00.00" And Vettore_Orari.Alle(i, g) = "00.00" Then
                                Vettore_Orari.Dalle(i, g) = W_Dalle
                                Vettore_Orari.DalleAlto(i, g) = W_Dalle
                                Vettore_Orari.DalleBasso(i, g) = W_Dalle
                                Vettore_Orari.Alle(i, g) = W_Alle
                                Vettore_Orari.AlleAlto(i, g) = W_Alle
                                Vettore_Orari.AlleBasso(i, g) = W_Alle
                                Vettore_Orari.Pausa(i, g) = "00.00"
                                Vettore_Orari.Giustificativo(i, g) = W_Giustificativo
                                Vettore_Orari.NelGruppo(i, g) = W_NelGruppo
                                Vettore_Orari.Tipo(i, g) = W_Tipo
                                For ii = i + 1 To 9
                                    Vettore_Orari.Dalle(ii, g) = "00.00"
                                    Vettore_Orari.Alle(ii, g) = "00.00"
                                    Vettore_Orari.DalleAlto(ii, g) = "00.00"
                                    Vettore_Orari.AlleAlto(ii, g) = "00.00"
                                    Vettore_Orari.DalleBasso(ii, g) = "00.00"
                                    Vettore_Orari.AlleBasso(ii, g) = "00.00"
                                    Vettore_Orari.DalleObbligo(ii, g) = "00.00"
                                    Vettore_Orari.AlleObbligo(ii, g) = "00.00"
                                    Vettore_Orari.Pausa(ii, g) = "00.00"
                                    Vettore_Orari.Giustificativo(ii, g) = ""
                                    Vettore_Orari.NelGruppo(ii, g) = ""
                                    Vettore_Orari.Familiare(ii, g) = 0
                                    Vettore_Orari.Tipo(ii, g) = ""
                                    Vettore_Orari.TipoServizio(ii, g) = ""
                                    Vettore_Orari.GiornoSuccessivo(ii, g) = ""
                                    If ii < 5 Then
                                        Vettore_Orari.NelGruppo(ii, g) = ""
                                    End If
                                Next ii
                                i = 9

                            Else

                                If Vettore_Orari.Dalle(i, g) < W_Dalle Then
                                    If Vettore_Orari.Alle(i, g) > W_Dalle Then
                                        For ii = 9 To i + 1 Step -1
                                            Vettore_Orari.Dalle(ii, g) = Vettore_Orari.Dalle(ii - 1, g)
                                            Vettore_Orari.Alle(ii, g) = Vettore_Orari.Alle(ii - 1, g)
                                            Vettore_Orari.DalleAlto(ii, g) = Vettore_Orari.DalleAlto(ii - 1, g)
                                            Vettore_Orari.AlleAlto(ii, g) = Vettore_Orari.AlleAlto(ii - 1, g)
                                            Vettore_Orari.DalleBasso(ii, g) = Vettore_Orari.DalleBasso(ii - 1, g)
                                            Vettore_Orari.AlleBasso(ii, g) = Vettore_Orari.AlleBasso(ii - 1, g)
                                            Vettore_Orari.DalleObbligo(ii, g) = Vettore_Orari.DalleObbligo(ii - 1, g)
                                            Vettore_Orari.AlleObbligo(ii, g) = Vettore_Orari.AlleObbligo(ii - 1, g)
                                            Vettore_Orari.Pausa(ii, g) = Vettore_Orari.Pausa(ii - 1, g)
                                            Vettore_Orari.Giustificativo(ii, g) = Vettore_Orari.Giustificativo(ii - 1, g)
                                            Vettore_Orari.NelGruppo(ii, g) = Vettore_Orari.NelGruppo(ii - 1, g)
                                            Vettore_Orari.Familiare(ii, g) = Vettore_Orari.Familiare(ii - 1, g)
                                            Vettore_Orari.Tipo(ii, g) = Vettore_Orari.Tipo(ii - 1, g)
                                            Vettore_Orari.TipoServizio(ii, g) = Vettore_Orari.TipoServizio(ii - 1, g)
                                            Vettore_Orari.GiornoSuccessivo(ii, g) = Vettore_Orari.GiornoSuccessivo(ii - 1, g)
                                            If ii < 5 Then
                                                Vettore_Orari.NelGruppo(ii, g) = Vettore_Orari.NelGruppo(ii - 1, g)
                                            End If
                                        Next ii
                                        If Vettore_Orari.Alle(i, g) <= W_Alle Then
                                            Vettore_Orari.Alle(i, g) = W_Dalle
                                            Vettore_Orari.AlleAlto(i, g) = W_Dalle
                                            Vettore_Orari.AlleBasso(i, g) = W_Dalle
                                            Vettore_Orari.Dalle(i + 1, g) = W_Dalle
                                            Vettore_Orari.DalleAlto(i + 1, g) = W_Dalle
                                            Vettore_Orari.DalleBasso(i + 1, g) = W_Dalle
                                            Vettore_Orari.Pausa(i + 1, g) = "00.00"
                                            Vettore_Orari.Giustificativo(i + 1, g) = W_Giustificativo
                                            Vettore_Orari.NelGruppo(i + 1, g) = W_NelGruppo
                                            Vettore_Orari.Tipo(i + 1, g) = W_Tipo
                                            W_Dalle = Vettore_Orari.Alle(i + 1, g)
                                            Vettore_Orari.AlleAlto(i + 1, g) = W_Dalle
                                            Vettore_Orari.AlleBasso(i + 1, g) = W_Dalle

                                        Else

                                            For ii = 9 To i + 1 Step -1
                                                Vettore_Orari.Dalle(ii, g) = Vettore_Orari.Dalle(ii - 1, g)
                                                Vettore_Orari.Alle(ii, g) = Vettore_Orari.Alle(ii - 1, g)
                                                Vettore_Orari.DalleAlto(ii, g) = Vettore_Orari.DalleAlto(ii - 1, g)
                                                Vettore_Orari.AlleAlto(ii, g) = Vettore_Orari.AlleAlto(ii - 1, g)
                                                Vettore_Orari.DalleBasso(ii, g) = Vettore_Orari.DalleBasso(ii - 1, g)
                                                Vettore_Orari.AlleBasso(ii, g) = Vettore_Orari.AlleBasso(ii - 1, g)
                                                Vettore_Orari.DalleObbligo(ii, g) = Vettore_Orari.DalleObbligo(ii - 1, g)
                                                Vettore_Orari.AlleObbligo(ii, g) = Vettore_Orari.AlleObbligo(ii - 1, g)
                                                Vettore_Orari.Pausa(ii, g) = Vettore_Orari.Pausa(ii - 1, g)
                                                Vettore_Orari.Giustificativo(ii, g) = Vettore_Orari.Giustificativo(ii - 1, g)
                                                Vettore_Orari.NelGruppo(ii, g) = Vettore_Orari.NelGruppo(ii - 1, g)
                                                Vettore_Orari.Familiare(ii, g) = Vettore_Orari.Familiare(ii - 1, g)
                                                Vettore_Orari.Tipo(ii, g) = Vettore_Orari.Tipo(ii - 1, g)
                                                Vettore_Orari.TipoServizio(ii, g) = Vettore_Orari.TipoServizio(ii - 1, g)
                                                Vettore_Orari.GiornoSuccessivo(ii, g) = Vettore_Orari.GiornoSuccessivo(ii - 1, g)
                                                If ii < 5 Then
                                                    Vettore_Orari.NelGruppo(ii, g) = Vettore_Orari.NelGruppo(ii - 1, g)
                                                End If
                                            Next ii
                                            Vettore_Orari.Alle(i, g) = W_Dalle
                                            Vettore_Orari.AlleAlto(i, g) = W_Dalle
                                            Vettore_Orari.AlleBasso(i, g) = W_Dalle
                                            Vettore_Orari.Dalle(i + 1, g) = W_Dalle
                                            Vettore_Orari.DalleAlto(i + 1, g) = W_Dalle
                                            Vettore_Orari.DalleBasso(i + 1, g) = W_Dalle
                                            Vettore_Orari.Pausa(i + 1, g) = "00.00"
                                            Vettore_Orari.Giustificativo(i + 1, g) = W_Giustificativo
                                            Vettore_Orari.NelGruppo(i + 1, g) = W_NelGruppo
                                            Vettore_Orari.Tipo(i + 1, g) = W_Tipo
                                            Vettore_Orari.Alle(i + 1, g) = W_Alle
                                            Vettore_Orari.AlleAlto(i + 1, g) = W_Alle
                                            Vettore_Orari.AlleBasso(i + 1, g) = W_Alle
                                            Vettore_Orari.Dalle(i + 2, g) = W_Alle
                                            Vettore_Orari.DalleAlto(i + 2, g) = W_Alle
                                            Vettore_Orari.DalleBasso(i + 2, g) = W_Alle
                                            Vettore_Orari.Pausa(i + 2, g) = "00.00"
                                            i = 9
                                        End If
                                    End If

                                Else

                                    If Vettore_Orari.Dalle(i, g) = W_Dalle Then
                                        If Vettore_Orari.Alle(i, g) < W_Alle Then
                                            Vettore_Orari.Pausa(i, g) = "00.00"
                                            Vettore_Orari.Giustificativo(i, g) = W_Giustificativo
                                            Vettore_Orari.NelGruppo(i, g) = W_NelGruppo
                                            Vettore_Orari.Tipo(i, g) = W_Tipo
                                            W_Dalle = Vettore_Orari.Alle(i, g)
                                            Vettore_Orari.AlleAlto(i, g) = W_Dalle
                                            Vettore_Orari.AlleBasso(i, g) = W_Dalle

                                        Else

                                            If Vettore_Orari.Alle(i, g) = W_Alle Then
                                                Vettore_Orari.Pausa(i, g) = "00.00"
                                                Vettore_Orari.Giustificativo(i, g) = W_Giustificativo
                                                Vettore_Orari.NelGruppo(i, g) = W_NelGruppo
                                                Vettore_Orari.Tipo(i, g) = W_Tipo
                                                i = 9

                                            Else

                                                For ii = 9 To i + 1 Step -1
                                                    Vettore_Orari.Dalle(ii, g) = Vettore_Orari.Dalle(ii - 1, g)
                                                    Vettore_Orari.Alle(ii, g) = Vettore_Orari.Alle(ii - 1, g)
                                                    Vettore_Orari.DalleAlto(ii, g) = Vettore_Orari.DalleAlto(ii - 1, g)
                                                    Vettore_Orari.AlleAlto(ii, g) = Vettore_Orari.AlleAlto(ii - 1, g)
                                                    Vettore_Orari.DalleBasso(ii, g) = Vettore_Orari.DalleBasso(ii - 1, g)
                                                    Vettore_Orari.AlleBasso(ii, g) = Vettore_Orari.AlleBasso(ii - 1, g)
                                                    Vettore_Orari.DalleObbligo(ii, g) = Vettore_Orari.DalleObbligo(ii - 1, g)
                                                    Vettore_Orari.AlleObbligo(ii, g) = Vettore_Orari.AlleObbligo(ii - 1, g)
                                                    Vettore_Orari.Pausa(ii, g) = Vettore_Orari.Pausa(ii - 1, g)
                                                    Vettore_Orari.Giustificativo(ii, g) = Vettore_Orari.Giustificativo(ii - 1, g)
                                                    Vettore_Orari.NelGruppo(ii, g) = Vettore_Orari.NelGruppo(ii - 1, g)
                                                    Vettore_Orari.Familiare(ii, g) = Vettore_Orari.Familiare(ii - 1, g)
                                                    Vettore_Orari.Tipo(ii, g) = Vettore_Orari.Tipo(ii - 1, g)
                                                    Vettore_Orari.TipoServizio(ii, g) = Vettore_Orari.TipoServizio(ii - 1, g)
                                                    Vettore_Orari.GiornoSuccessivo(ii, g) = Vettore_Orari.GiornoSuccessivo(ii - 1, g)
                                                    If ii < 5 Then
                                                        Vettore_Orari.NelGruppo(ii, g) = Vettore_Orari.NelGruppo(ii - 1, g)
                                                    End If
                                                Next ii
                                                Vettore_Orari.Alle(i, g) = W_Alle
                                                Vettore_Orari.AlleAlto(i, g) = W_Alle
                                                Vettore_Orari.AlleBasso(i, g) = W_Alle
                                                Vettore_Orari.Pausa(i, g) = "00.00"
                                                Vettore_Orari.Giustificativo(i, g) = W_Giustificativo
                                                Vettore_Orari.NelGruppo(i, g) = W_NelGruppo
                                                Vettore_Orari.Tipo(i, g) = W_Tipo
                                                Vettore_Orari.Dalle(i + 1, g) = W_Alle
                                                Vettore_Orari.DalleAlto(i + 1, g) = W_Alle
                                                Vettore_Orari.DalleBasso(i + 1, g) = W_Alle
                                                i = 9
                                            End If
                                        End If

                                    Else

                                        For ii = 9 To i + 1 Step -1
                                            Vettore_Orari.Dalle(ii, g) = Vettore_Orari.Dalle(ii - 1, g)
                                            Vettore_Orari.Alle(ii, g) = Vettore_Orari.Alle(ii - 1, g)
                                            Vettore_Orari.DalleAlto(ii, g) = Vettore_Orari.DalleAlto(ii - 1, g)
                                            Vettore_Orari.AlleAlto(ii, g) = Vettore_Orari.AlleAlto(ii - 1, g)
                                            Vettore_Orari.DalleBasso(ii, g) = Vettore_Orari.DalleBasso(ii - 1, g)
                                            Vettore_Orari.AlleBasso(ii, g) = Vettore_Orari.AlleBasso(ii - 1, g)
                                            Vettore_Orari.DalleObbligo(ii, g) = Vettore_Orari.DalleObbligo(ii - 1, g)
                                            Vettore_Orari.AlleObbligo(ii, g) = Vettore_Orari.AlleObbligo(ii - 1, g)
                                            Vettore_Orari.Pausa(ii, g) = Vettore_Orari.Pausa(ii - 1, g)
                                            Vettore_Orari.Giustificativo(ii, g) = Vettore_Orari.Giustificativo(ii - 1, g)
                                            Vettore_Orari.NelGruppo(ii, g) = Vettore_Orari.NelGruppo(ii - 1, g)
                                            Vettore_Orari.Familiare(ii, g) = Vettore_Orari.Familiare(ii - 1, g)
                                            Vettore_Orari.Tipo(ii, g) = Vettore_Orari.Tipo(ii - 1, g)
                                            Vettore_Orari.TipoServizio(ii, g) = Vettore_Orari.TipoServizio(ii - 1, g)
                                            Vettore_Orari.GiornoSuccessivo(ii, g) = Vettore_Orari.GiornoSuccessivo(ii - 1, g)
                                            If ii < 5 Then
                                                Vettore_Orari.NelGruppo(ii, g) = Vettore_Orari.NelGruppo(ii - 1, g)
                                            End If
                                        Next ii
                                        If Vettore_Orari.Dalle(i, g) >= W_Alle Then
                                            Vettore_Orari.Dalle(i, g) = W_Dalle
                                            Vettore_Orari.DalleAlto(i, g) = W_Dalle
                                            Vettore_Orari.DalleBasso(i, g) = W_Dalle
                                            Vettore_Orari.Alle(i, g) = W_Alle
                                            Vettore_Orari.AlleAlto(i, g) = W_Alle
                                            Vettore_Orari.AlleBasso(i, g) = W_Alle
                                            Vettore_Orari.Pausa(i, g) = "00.00"
                                            Vettore_Orari.Giustificativo(i, g) = W_Giustificativo
                                            Vettore_Orari.NelGruppo(i, g) = W_NelGruppo
                                            Vettore_Orari.Tipo(i, g) = W_Tipo
                                            i = 9

                                        Else

                                            Vettore_Orari.Alle(i, g) = Vettore_Orari.Dalle(i, g)
                                            Vettore_Orari.AlleAlto(i, g) = Vettore_Orari.Dalle(i, g)
                                            Vettore_Orari.AlleBasso(i, g) = Vettore_Orari.Dalle(i, g)
                                            Vettore_Orari.Dalle(i, g) = W_Dalle
                                            Vettore_Orari.DalleAlto(i, g) = W_Dalle
                                            Vettore_Orari.DalleBasso(i, g) = W_Dalle
                                            Vettore_Orari.Pausa(i, g) = "00.00"
                                            Vettore_Orari.Giustificativo(i, g) = W_Giustificativo
                                            Vettore_Orari.NelGruppo(i, g) = W_NelGruppo
                                            Vettore_Orari.Tipo(i, g) = W_Tipo
                                        End If
                                    End If
                                End If
                            End If

                        Else

                            Vettore_Orari.Dalle(i, g) = W_Dalle
                            Vettore_Orari.DalleAlto(i, g) = W_Dalle
                            Vettore_Orari.DalleBasso(i, g) = W_Dalle
                            Vettore_Orari.Alle(i, g) = W_Alle
                            Vettore_Orari.AlleAlto(i, g) = W_Alle
                            Vettore_Orari.AlleBasso(i, g) = W_Alle
                            Vettore_Orari.Pausa(i, g) = "00.00"
                            Vettore_Orari.Giustificativo(i, g) = W_Giustificativo
                            Vettore_Orari.NelGruppo(i, g) = W_NelGruppo
                            Vettore_Orari.Tipo(i, g) = W_Tipo
                            i = 9
                        End If
                    Next i

                Else

                    If Chk_Giorno(Weekday(Data) - 1) = "S" Then

                        If (Chk_RiposoSuFestivi = "S" And Weekday(Data) = 1) Or (Chk_RiposoSuFestiviInfrasettimanali = "S" And cxst.GiornoFestivo(Data, CodiceSuperGruppo) = True) Then

                            Vettore_Orari.Richiesti(g) = "S"

                            If Chk_RimuoviOrari = "N" Then
                                For i = 0 To 9
                                    If Vettore_Orari.Dalle(i, g) = "00.00" And _
                                       Vettore_Orari.Alle(i, g) = "00.00" And _
                                       Vettore_Orari.Pausa(i, g) = "00.00" And _
                                       Vettore_Orari.Giustificativo(i, g) = "" Then Exit For
                                    Vettore_Orari.Giustificativo(i, g) = cpt.GiustificativoRiposo
                                    Vettore_Orari.NelGruppo(i, g) = ""
                                    Vettore_Orari.Tipo(i, g) = cg.CampoGiustificativi(ConnectionString, cpt.GiustificativoRiposo, "Tipo")
                                Next i

                            Else

                                Vettore_Orari.Dalle(0, g) = "00.00"
                                Vettore_Orari.DalleAlto(0, g) = "00.00"
                                Vettore_Orari.DalleBasso(0, g) = "00.00"
                                Vettore_Orari.Alle(0, g) = "00.00"
                                Vettore_Orari.AlleAlto(0, g) = "00.00"
                                Vettore_Orari.AlleBasso(0, g) = "00.00"
                                Vettore_Orari.Pausa(0, g) = "00.00"
                                Vettore_Orari.Giustificativo(0, g) = cpt.GiustificativoRiposo
                                Vettore_Orari.NelGruppo(0, g) = ""
                                Vettore_Orari.Tipo(0, g) = cg.CampoGiustificativi(ConnectionString, cpt.GiustificativoRiposo, "Tipo")
                                For i = 1 To 9
                                    Vettore_Orari.Dalle(i, g) = "00.00"
                                    Vettore_Orari.Alle(i, g) = "00.00"
                                    Vettore_Orari.DalleAlto(i, g) = "00.00"
                                    Vettore_Orari.AlleAlto(i, g) = "00.00"
                                    Vettore_Orari.DalleBasso(i, g) = "00.00"
                                    Vettore_Orari.AlleBasso(i, g) = "00.00"
                                    Vettore_Orari.DalleObbligo(i, g) = "00.00"
                                    Vettore_Orari.AlleObbligo(i, g) = "00.00"
                                    Vettore_Orari.Pausa(i, g) = "00.00"
                                    Vettore_Orari.Giustificativo(i, g) = ""
                                    Vettore_Orari.NelGruppo(i, g) = ""
                                    Vettore_Orari.Familiare(i, g) = 0
                                    Vettore_Orari.Tipo(i, g) = ""
                                    Vettore_Orari.TipoServizio(i, g) = ""
                                    Vettore_Orari.GiornoSuccessivo(i, g) = ""
                                    If i < 5 Then
                                        Vettore_Orari.NelGruppo(i, g) = ""
                                    End If
                                Next i
                            End If

                        Else

                            If Not (Chk_SuRiposo = "N" And Vettore_Orari.Giustificativo(0, g) = cpt.GiustificativoRiposo) Then

                                Vettore_Orari.Richiesti(g) = "S"

                                If Chk_RimuoviOrari = "N" Then
                                    For i = 0 To 9
                                        If Vettore_Orari.Dalle(i, g) = "00.00" And _
                                           Vettore_Orari.Alle(i, g) = "00.00" And _
                                           Vettore_Orari.Pausa(i, g) = "00.00" And _
                                           Vettore_Orari.Giustificativo(i, g) = "" Then Exit For
                                        Vettore_Orari.Giustificativo(i, g) = W_Giustificativo
                                        Vettore_Orari.NelGruppo(i, g) = W_NelGruppo
                                        Vettore_Orari.Tipo(i, g) = W_Tipo
                                    Next i

                                Else

                                    Vettore_Orari.Dalle(0, g) = "00.00"
                                    Vettore_Orari.DalleAlto(0, g) = "00.00"
                                    Vettore_Orari.DalleBasso(0, g) = "00.00"
                                    Vettore_Orari.Alle(0, g) = "00.00"
                                    Vettore_Orari.AlleAlto(0, g) = "00.00"
                                    Vettore_Orari.AlleBasso(0, g) = "00.00"
                                    Vettore_Orari.Pausa(0, g) = "00.00"
                                    Vettore_Orari.Giustificativo(0, g) = W_Giustificativo
                                    Vettore_Orari.NelGruppo(0, g) = W_NelGruppo
                                    Vettore_Orari.Tipo(0, g) = W_Tipo
                                    For i = 1 To 9
                                        Vettore_Orari.Dalle(i, g) = "00.00"
                                        Vettore_Orari.Alle(i, g) = "00.00"
                                        Vettore_Orari.DalleAlto(i, g) = "00.00"
                                        Vettore_Orari.AlleAlto(i, g) = "00.00"
                                        Vettore_Orari.DalleBasso(i, g) = "00.00"
                                        Vettore_Orari.AlleBasso(i, g) = "00.00"
                                        Vettore_Orari.DalleObbligo(i, g) = "00.00"
                                        Vettore_Orari.AlleObbligo(i, g) = "00.00"
                                        Vettore_Orari.Pausa(i, g) = "00.00"
                                        Vettore_Orari.Giustificativo(i, g) = ""
                                        Vettore_Orari.NelGruppo(i, g) = ""
                                        Vettore_Orari.Familiare(i, g) = 0
                                        Vettore_Orari.Tipo(i, g) = ""
                                        Vettore_Orari.TipoServizio(i, g) = ""
                                        Vettore_Orari.GiornoSuccessivo(i, g) = ""
                                        If i < 5 Then
                                            Vettore_Orari.NelGruppo(i, g) = ""
                                        End If
                                    Next i
                                End If
                            End If
                        End If
                    End If
                End If

                Data = DateAdd("d", 1, Data)
            Loop

        Loop
        myPOSTreader.Close()
        cn.Close()
    End Sub

    Private Sub Carica_Vettore_Orari_RichiesteStraordinario(ByVal ConnectionString As String, ByVal Dipendente As Object, ByVal DataMin As Date, ByVal DataMax As Date, ByVal DataSviluppo As Date, ByRef Vettore_Orari As Cls_Struttura_Orari)
        Dim W_Dalle As String
        Dim W_Alle As String
        Dim W_Giustificativo As String
        Dim W_NelGruppo As String
        Dim W_Tipo As String
        Dim g As Integer

        Dim cg As New Cls_Giustificativi

        Dim cn As OleDbConnection
        cn = New Data.OleDb.OleDbConnection(ConnectionString)
        cn.Open()

        Dim cmd As New OleDbCommand()

        cmd.CommandText = "SELECT * FROM RichiesteAssenza" & _
                          " WHERE CodiceDipendente = ?" & _
                          " AND Acquisito = 'S'" & _
                          " AND (Annullato Is Null OR Annullato = '')" & _
                          " AND (Revocato Is Null OR Revocato = '')" & _
                          " AND ((DataAl Is Null" & _
                          " AND DataDal >= ?" & _
                          " AND DataDal <= ?) " & _
                          " OR (NOT DataAl Is Null" & _
                          " AND DataDal <= ?" & _
                          " AND DataAl >= ?)) " & _
                          " ORDER BY DataDal, Dalle"

        cmd.CommandText = "SELECT * FROM RichiesteStraordinario" & _
            " WHERE CodiceDipendente = ?" & _
            " AND Acquisito = 'S'" & _
            " AND (Annullato Is Null OR Annullato = '')" & _
            " AND (Revocato Is Null OR Revocato = '')" & _
            " AND DataRichiesta >= ?" & _
            " AND DataRichiesta <= ?" & _
            " ORDER BY DataRichiesta, Dalle"
        cmd.Parameters.AddWithValue("@CodiceDipendente", Dipendente)
        cmd.Parameters.AddWithValue("@DataRichiesta", DataMin)
        cmd.Parameters.AddWithValue("@DataRichiesta", DataMax)

        cmd.Connection = cn
        Dim myPOSTreader As OleDbDataReader = cmd.ExecuteReader()
        Do While myPOSTreader.Read

            W_Dalle = Format(DataDb(myPOSTreader.Item("Dalle")), "hh.nn")
            W_Alle = Format(DataDb(myPOSTreader.Item("Alle")), "hh.nn")
            W_Giustificativo = StringaDb(myPOSTreader.Item("Giustificativo"))
            W_NelGruppo = ""
            W_Tipo = cg.CampoGiustificativi(ConnectionString, W_Giustificativo, "Tipo")

            g = DateDiff("d", DataSviluppo, DataDb(myPOSTreader.Item("DataRichiesta")))

            Vettore_Orari.Richiesti(g) = "S"

            For i = 0 To 9
                If Vettore_Orari.Giustificativo(i, g) <> "" Then
                    If Vettore_Orari.Dalle(i, g) = "00.00" And Vettore_Orari.Alle(i, g) = "00.00" Then
                        Vettore_Orari.Dalle(i, g) = W_Dalle
                        Vettore_Orari.DalleAlto(i, g) = W_Dalle
                        Vettore_Orari.DalleBasso(i, g) = W_Dalle
                        Vettore_Orari.Alle(i, g) = W_Alle
                        Vettore_Orari.AlleAlto(i, g) = W_Alle
                        Vettore_Orari.AlleBasso(i, g) = W_Alle
                        Vettore_Orari.Pausa(i, g) = "00.00"
                        Vettore_Orari.Giustificativo(i, g) = W_Giustificativo
                        Vettore_Orari.NelGruppo(i, g) = W_NelGruppo
                        Vettore_Orari.Tipo(i, g) = W_Tipo
                        For ii = i + 1 To 9
                            Vettore_Orari.Dalle(ii, g) = "00.00"
                            Vettore_Orari.Alle(ii, g) = "00.00"
                            Vettore_Orari.DalleAlto(ii, g) = "00.00"
                            Vettore_Orari.AlleAlto(ii, g) = "00.00"
                            Vettore_Orari.DalleBasso(ii, g) = "00.00"
                            Vettore_Orari.AlleBasso(ii, g) = "00.00"
                            Vettore_Orari.DalleObbligo(ii, g) = "00.00"
                            Vettore_Orari.AlleObbligo(ii, g) = "00.00"
                            Vettore_Orari.Pausa(ii, g) = "00.00"
                            Vettore_Orari.Giustificativo(ii, g) = ""
                            Vettore_Orari.NelGruppo(ii, g) = ""
                            Vettore_Orari.Familiare(ii, g) = 0
                            Vettore_Orari.Tipo(ii, g) = ""
                            Vettore_Orari.TipoServizio(ii, g) = ""
                            Vettore_Orari.GiornoSuccessivo(ii, g) = ""
                            If ii < 5 Then
                                Vettore_Orari.NelGruppo(ii, g) = ""
                            End If
                        Next ii
                        i = 9

                    Else

                        If Vettore_Orari.Dalle(i, g) < W_Dalle Then
                            If Vettore_Orari.Alle(i, g) > W_Dalle Then
                                For ii = 9 To i + 1 Step -1
                                    Vettore_Orari.Dalle(ii, g) = Vettore_Orari.Dalle(ii - 1, g)
                                    Vettore_Orari.Alle(ii, g) = Vettore_Orari.Alle(ii - 1, g)
                                    Vettore_Orari.DalleAlto(ii, g) = Vettore_Orari.DalleAlto(ii - 1, g)
                                    Vettore_Orari.AlleAlto(ii, g) = Vettore_Orari.AlleAlto(ii - 1, g)
                                    Vettore_Orari.DalleBasso(ii, g) = Vettore_Orari.DalleBasso(ii - 1, g)
                                    Vettore_Orari.AlleBasso(ii, g) = Vettore_Orari.AlleBasso(ii - 1, g)
                                    Vettore_Orari.DalleObbligo(ii, g) = Vettore_Orari.DalleObbligo(ii - 1, g)
                                    Vettore_Orari.AlleObbligo(ii, g) = Vettore_Orari.AlleObbligo(ii - 1, g)
                                    Vettore_Orari.Pausa(ii, g) = Vettore_Orari.Pausa(ii - 1, g)
                                    Vettore_Orari.Giustificativo(ii, g) = Vettore_Orari.Giustificativo(ii - 1, g)
                                    Vettore_Orari.NelGruppo(ii, g) = Vettore_Orari.NelGruppo(ii - 1, g)
                                    Vettore_Orari.Familiare(ii, g) = Vettore_Orari.Familiare(ii - 1, g)
                                    Vettore_Orari.Tipo(ii, g) = Vettore_Orari.Tipo(ii - 1, g)
                                    Vettore_Orari.TipoServizio(ii, g) = Vettore_Orari.TipoServizio(ii - 1, g)
                                    Vettore_Orari.GiornoSuccessivo(ii, g) = Vettore_Orari.GiornoSuccessivo(ii - 1, g)
                                    If ii < 5 Then
                                        Vettore_Orari.NelGruppo(ii, g) = Vettore_Orari.NelGruppo(ii - 1, g)
                                    End If
                                Next ii
                                If Vettore_Orari.Alle(i, g) <= W_Alle Then
                                    Vettore_Orari.Alle(i, g) = W_Dalle
                                    Vettore_Orari.AlleAlto(i, g) = W_Dalle
                                    Vettore_Orari.AlleBasso(i, g) = W_Dalle
                                    Vettore_Orari.Dalle(i + 1, g) = W_Dalle
                                    Vettore_Orari.DalleAlto(i + 1, g) = W_Dalle
                                    Vettore_Orari.DalleBasso(i + 1, g) = W_Dalle
                                    Vettore_Orari.Pausa(i + 1, g) = "00.00"
                                    Vettore_Orari.Giustificativo(i + 1, g) = W_Giustificativo
                                    Vettore_Orari.NelGruppo(i + 1, g) = W_NelGruppo
                                    Vettore_Orari.Tipo(i + 1, g) = W_Tipo
                                    W_Dalle = Vettore_Orari.Alle(i + 1, g)
                                    Vettore_Orari.AlleAlto(i + 1, g) = W_Dalle
                                    Vettore_Orari.AlleBasso(i + 1, g) = W_Dalle

                                Else

                                    For ii = 9 To i + 1 Step -1
                                        Vettore_Orari.Dalle(ii, g) = Vettore_Orari.Dalle(ii - 1, g)
                                        Vettore_Orari.Alle(ii, g) = Vettore_Orari.Alle(ii - 1, g)
                                        Vettore_Orari.DalleAlto(ii, g) = Vettore_Orari.DalleAlto(ii - 1, g)
                                        Vettore_Orari.AlleAlto(ii, g) = Vettore_Orari.AlleAlto(ii - 1, g)
                                        Vettore_Orari.DalleBasso(ii, g) = Vettore_Orari.DalleBasso(ii - 1, g)
                                        Vettore_Orari.AlleBasso(ii, g) = Vettore_Orari.AlleBasso(ii - 1, g)
                                        Vettore_Orari.DalleObbligo(ii, g) = Vettore_Orari.DalleObbligo(ii - 1, g)
                                        Vettore_Orari.AlleObbligo(ii, g) = Vettore_Orari.AlleObbligo(ii - 1, g)
                                        Vettore_Orari.Pausa(ii, g) = Vettore_Orari.Pausa(ii - 1, g)
                                        Vettore_Orari.Giustificativo(ii, g) = Vettore_Orari.Giustificativo(ii - 1, g)
                                        Vettore_Orari.NelGruppo(ii, g) = Vettore_Orari.NelGruppo(ii - 1, g)
                                        Vettore_Orari.Familiare(ii, g) = Vettore_Orari.Familiare(ii - 1, g)
                                        Vettore_Orari.Tipo(ii, g) = Vettore_Orari.Tipo(ii - 1, g)
                                        Vettore_Orari.TipoServizio(ii, g) = Vettore_Orari.TipoServizio(ii - 1, g)
                                        Vettore_Orari.GiornoSuccessivo(ii, g) = Vettore_Orari.GiornoSuccessivo(ii - 1, g)
                                        If ii < 5 Then
                                            Vettore_Orari.NelGruppo(ii, g) = Vettore_Orari.NelGruppo(ii - 1, g)
                                        End If
                                    Next ii
                                    Vettore_Orari.Alle(i, g) = W_Dalle
                                    Vettore_Orari.AlleAlto(i, g) = W_Dalle
                                    Vettore_Orari.AlleBasso(i, g) = W_Dalle
                                    Vettore_Orari.Dalle(i + 1, g) = W_Dalle
                                    Vettore_Orari.DalleAlto(i + 1, g) = W_Dalle
                                    Vettore_Orari.DalleBasso(i + 1, g) = W_Dalle
                                    Vettore_Orari.Pausa(i + 1, g) = "00.00"
                                    Vettore_Orari.Giustificativo(i + 1, g) = W_Giustificativo
                                    Vettore_Orari.NelGruppo(i + 1, g) = W_NelGruppo
                                    Vettore_Orari.Tipo(i + 1, g) = W_Tipo
                                    Vettore_Orari.Alle(i + 1, g) = W_Alle
                                    Vettore_Orari.AlleAlto(i + 1, g) = W_Alle
                                    Vettore_Orari.AlleBasso(i + 1, g) = W_Alle
                                    Vettore_Orari.Dalle(i + 2, g) = W_Alle
                                    Vettore_Orari.DalleAlto(i + 2, g) = W_Alle
                                    Vettore_Orari.DalleBasso(i + 2, g) = W_Alle
                                    Vettore_Orari.Pausa(i + 2, g) = "00.00"
                                    i = 9
                                End If
                            End If

                        Else

                            If Vettore_Orari.Dalle(i, g) = W_Dalle Then
                                If Vettore_Orari.Alle(i, g) < W_Alle Then
                                    Vettore_Orari.Pausa(i, g) = "00.00"
                                    Vettore_Orari.Giustificativo(i, g) = W_Giustificativo
                                    Vettore_Orari.NelGruppo(i, g) = W_NelGruppo
                                    Vettore_Orari.Tipo(i, g) = W_Tipo
                                    W_Dalle = Vettore_Orari.Alle(i, g)
                                    Vettore_Orari.AlleAlto(i, g) = W_Dalle
                                    Vettore_Orari.AlleBasso(i, g) = W_Dalle

                                Else

                                    If Vettore_Orari.Alle(i, g) = W_Alle Then
                                        Vettore_Orari.Pausa(i, g) = "00.00"
                                        Vettore_Orari.Giustificativo(i, g) = W_Giustificativo
                                        Vettore_Orari.NelGruppo(i, g) = W_NelGruppo
                                        Vettore_Orari.Tipo(i, g) = W_Tipo
                                        i = 9

                                    Else

                                        For ii = 9 To i + 1 Step -1
                                            Vettore_Orari.Dalle(ii, g) = Vettore_Orari.Dalle(ii - 1, g)
                                            Vettore_Orari.Alle(ii, g) = Vettore_Orari.Alle(ii - 1, g)
                                            Vettore_Orari.DalleAlto(ii, g) = Vettore_Orari.DalleAlto(ii - 1, g)
                                            Vettore_Orari.AlleAlto(ii, g) = Vettore_Orari.AlleAlto(ii - 1, g)
                                            Vettore_Orari.DalleBasso(ii, g) = Vettore_Orari.DalleBasso(ii - 1, g)
                                            Vettore_Orari.AlleBasso(ii, g) = Vettore_Orari.AlleBasso(ii - 1, g)
                                            Vettore_Orari.DalleObbligo(ii, g) = Vettore_Orari.DalleObbligo(ii - 1, g)
                                            Vettore_Orari.AlleObbligo(ii, g) = Vettore_Orari.AlleObbligo(ii - 1, g)
                                            Vettore_Orari.Pausa(ii, g) = Vettore_Orari.Pausa(ii - 1, g)
                                            Vettore_Orari.Giustificativo(ii, g) = Vettore_Orari.Giustificativo(ii - 1, g)
                                            Vettore_Orari.NelGruppo(ii, g) = Vettore_Orari.NelGruppo(ii - 1, g)
                                            Vettore_Orari.Familiare(ii, g) = Vettore_Orari.Familiare(ii - 1, g)
                                            Vettore_Orari.Tipo(ii, g) = Vettore_Orari.Tipo(ii - 1, g)
                                            Vettore_Orari.TipoServizio(ii, g) = Vettore_Orari.TipoServizio(ii - 1, g)
                                            Vettore_Orari.GiornoSuccessivo(ii, g) = Vettore_Orari.GiornoSuccessivo(ii - 1, g)
                                            If ii < 5 Then
                                                Vettore_Orari.NelGruppo(ii, g) = Vettore_Orari.NelGruppo(ii - 1, g)
                                            End If
                                        Next ii
                                        Vettore_Orari.Alle(i, g) = W_Alle
                                        Vettore_Orari.AlleAlto(i, g) = W_Alle
                                        Vettore_Orari.AlleBasso(i, g) = W_Alle
                                        Vettore_Orari.Pausa(i + 1, g) = "00.00"
                                        Vettore_Orari.Giustificativo(i + 1, g) = W_Giustificativo
                                        Vettore_Orari.NelGruppo(i + 1, g) = W_NelGruppo
                                        Vettore_Orari.Tipo(i + 1, g) = W_Tipo
                                        Vettore_Orari.Dalle(i + 1, g) = W_Alle
                                        Vettore_Orari.DalleAlto(i + 1, g) = W_Alle
                                        Vettore_Orari.DalleBasso(i + 1, g) = W_Alle
                                        i = 9
                                    End If
                                End If

                            Else

                                For ii = 9 To i + 1 Step -1
                                    Vettore_Orari.Dalle(ii, g) = Vettore_Orari.Dalle(ii - 1, g)
                                    Vettore_Orari.Alle(ii, g) = Vettore_Orari.Alle(ii - 1, g)
                                    Vettore_Orari.DalleAlto(ii, g) = Vettore_Orari.DalleAlto(ii - 1, g)
                                    Vettore_Orari.AlleAlto(ii, g) = Vettore_Orari.AlleAlto(ii - 1, g)
                                    Vettore_Orari.DalleBasso(ii, g) = Vettore_Orari.DalleBasso(ii - 1, g)
                                    Vettore_Orari.AlleBasso(ii, g) = Vettore_Orari.AlleBasso(ii - 1, g)
                                    Vettore_Orari.DalleObbligo(ii, g) = Vettore_Orari.DalleObbligo(ii - 1, g)
                                    Vettore_Orari.AlleObbligo(ii, g) = Vettore_Orari.AlleObbligo(ii - 1, g)
                                    Vettore_Orari.Pausa(ii, g) = Vettore_Orari.Pausa(ii - 1, g)
                                    Vettore_Orari.Giustificativo(ii, g) = Vettore_Orari.Giustificativo(ii - 1, g)
                                    Vettore_Orari.NelGruppo(ii, g) = Vettore_Orari.NelGruppo(ii - 1, g)
                                    Vettore_Orari.Familiare(ii, g) = Vettore_Orari.Familiare(ii - 1, g)
                                    Vettore_Orari.Tipo(ii, g) = Vettore_Orari.Tipo(ii - 1, g)
                                    Vettore_Orari.TipoServizio(ii, g) = Vettore_Orari.TipoServizio(ii - 1, g)
                                    Vettore_Orari.GiornoSuccessivo(ii, g) = Vettore_Orari.GiornoSuccessivo(ii - 1, g)
                                    If ii < 5 Then
                                        Vettore_Orari.NelGruppo(ii, g) = Vettore_Orari.NelGruppo(ii - 1, g)
                                    End If
                                Next ii
                                If Vettore_Orari.Dalle(i, g) >= W_Alle Then
                                    Vettore_Orari.Dalle(i, g) = W_Dalle
                                    Vettore_Orari.DalleAlto(i, g) = W_Dalle
                                    Vettore_Orari.DalleBasso(i, g) = W_Dalle
                                    Vettore_Orari.Alle(i, g) = W_Alle
                                    Vettore_Orari.AlleAlto(i, g) = W_Alle
                                    Vettore_Orari.AlleBasso(i, g) = W_Alle
                                    Vettore_Orari.Pausa(i, g) = "00.00"
                                    Vettore_Orari.Giustificativo(i, g) = W_Giustificativo
                                    Vettore_Orari.NelGruppo(i, g) = W_NelGruppo
                                    Vettore_Orari.Tipo(i, g) = W_Tipo
                                    i = 9

                                Else

                                    Vettore_Orari.Alle(i, g) = Vettore_Orari.Dalle(i, g)
                                    Vettore_Orari.AlleAlto(i, g) = Vettore_Orari.Dalle(i, g)
                                    Vettore_Orari.AlleBasso(i, g) = Vettore_Orari.Dalle(i, g)
                                    Vettore_Orari.Dalle(i, g) = W_Dalle
                                    Vettore_Orari.DalleAlto(i, g) = W_Dalle
                                    Vettore_Orari.DalleBasso(i, g) = W_Dalle
                                    Vettore_Orari.Pausa(i, g) = "00.00"
                                    Vettore_Orari.Giustificativo(i, g) = W_Giustificativo
                                    Vettore_Orari.NelGruppo(i, g) = W_NelGruppo
                                    Vettore_Orari.Tipo(i, g) = W_Tipo
                                End If
                            End If
                        End If
                    End If

                Else

                    If i > 0 Then
                        Vettore_Orari.AlleBasso(i - 1, g) = W_Dalle
                    End If
                    Vettore_Orari.Dalle(i, g) = W_Dalle
                    Vettore_Orari.DalleAlto(i, g) = W_Dalle
                    Vettore_Orari.DalleBasso(i, g) = W_Dalle
                    Vettore_Orari.Alle(i, g) = W_Alle
                    Vettore_Orari.AlleAlto(i, g) = W_Alle
                    Vettore_Orari.AlleBasso(i, g) = W_Alle
                    Vettore_Orari.Pausa(i, g) = "00.00"
                    Vettore_Orari.Giustificativo(i, g) = W_Giustificativo
                    Vettore_Orari.NelGruppo(i, g) = W_NelGruppo
                    Vettore_Orari.Tipo(i, g) = W_Tipo
                    i = 9
                End If
            Next i
        Loop
        myPOSTreader.Close()
        cn.Close()
    End Sub

    Function StringaDb(ByVal Oggetto As Object) As String
        If IsDBNull(Oggetto) Then
            Return ""
        Else
            Return Oggetto
        End If
    End Function

    Function DataDb(ByVal Oggetto As Object) As Date
        If Not IsDate(Oggetto) Then
            Return Nothing
        Else
            Return Oggetto
        End If
    End Function

    Function NumeroDb(ByVal Oggetto As Object) As Object
        If IsDBNull(Oggetto) Then
            Return 0
        Else
            Return Oggetto
        End If
    End Function

End Class
