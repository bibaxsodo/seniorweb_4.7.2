Imports Microsoft.VisualBasic
Imports System.Data.OleDb
Imports System.Web.Hosting

Public Class Cls_TipiDatiAnagrafici
    Public Id As Long
    Public Utente As String
    Public DataAggiornamento As Date
    Public Codice As String
    Public Descrizione As String
    Public Tipo As Integer
    Public Gruppo As String
    Public TipoNumericoVirgola As String
    Public SempreVisibile As String
    Public VisibileDipendenti As String
    Public PerSelezioniServizi As String
    Public Scadenza As String
    Public TabellaDescrittiva As String
    Public Formula As String
    Public Commento As String
    Public Automatico As String
    Public UnitaDiMisura As String
    Public TipoContatore As String
    Public Conversione As String

    Public Sub Pulisci()
        Id = 0
        Utente = ""
        DataAggiornamento = Nothing
        Codice = ""
        Descrizione = ""
        Tipo = 0
        Gruppo = ""
        TipoNumericoVirgola = ""
        SempreVisibile = ""
        VisibileDipendenti = ""
        PerSelezioniServizi = ""
        Scadenza = ""
        TabellaDescrittiva = ""
        Formula = ""
        Commento = ""
        Automatico = ""
        UnitaDiMisura = ""
        TipoContatore = ""
        Conversione = ""
    End Sub

    Public Sub Elimina(ByVal StringaConnessione As String, ByVal xCodice As String)
        Dim cn As OleDbConnection
        Dim MySql As String

        MySql = ""

        cn = New Data.OleDb.OleDbConnection(StringaConnessione)

        cn.Open()
        Dim cmd As New OleDbCommand()
        cmd.CommandText = "DELETE FROM TipiDatiAnagrafici WHERE Codice = ?"
        cmd.Parameters.AddWithValue("@Codice", xCodice)
        cmd.Connection = cn
        cmd.ExecuteNonQuery()
        cn.Close()
    End Sub

    Sub Aggiorna(ByVal StringaConnessione As String)
        Dim cn As OleDbConnection
        Dim MySql As String

        MySql = ""

        cn = New Data.OleDb.OleDbConnection(StringaConnessione)

        cn.Open()
        Dim cmd As New OleDbCommand()
        cmd.CommandText = "SELECT * FROM TipiDatiAnagrafici WHERE Codice = ?"
        cmd.Parameters.AddWithValue("@Codice", Codice)
        cmd.Connection = cn
        Dim myPOSTreader As OleDbDataReader = cmd.ExecuteReader()
        If myPOSTreader.Read Then
            MySql = "UPDATE TipiDatiAnagrafici SET " & _
                    " Utente = ?, " & _
                    " DataAggiornamento  = ?," & _
                    " Descrizione = ?," & _
                    " Tipo = ?," & _
                    " Gruppo = ?," & _
                    " TipoNumericoVirgola = ?," & _
                    " SempreVisibile = ?," & _
                    " VisibileDipendenti = ?," & _
                    " PerSelezioniServizi = ?," & _
                    " Scadenza = ?," & _
                    " TabellaDescrittiva = ?," & _
                    " Formula = ?," & _
                    " Commento = ?," & _
                    " Automatico = ?," & _
                    " UnitaDiMisura = ?," & _
                    " TipoContatore = ?," & _
                    " Conversione = ?" & _
                    " WHERE Codice = ?"

            Dim cmdw As New OleDbCommand()
            cmdw.CommandText = MySql
            cmdw.Parameters.AddWithValue("@Utente", Utente)
            cmdw.Parameters.AddWithValue("@DataAggiornamento", Now)
            cmdw.Parameters.AddWithValue("@Descrizione", Descrizione)
            cmdw.Parameters.AddWithValue("@Tipo", Tipo)
            cmdw.Parameters.AddWithValue("@Gruppo", Gruppo)
            cmdw.Parameters.AddWithValue("@TipoNumericoVirgola", TipoNumericoVirgola)
            cmdw.Parameters.AddWithValue("@SempreVisibile", SempreVisibile)
            cmdw.Parameters.AddWithValue("@VisibileDipendenti", VisibileDipendenti)
            cmdw.Parameters.AddWithValue("@PerSelezioniServizi", PerSelezioniServizi)
            cmdw.Parameters.AddWithValue("@Scadenza", Scadenza)
            cmdw.Parameters.AddWithValue("@TabellaDescrittiva", TabellaDescrittiva)
            cmdw.Parameters.AddWithValue("@Formula", Formula)
            cmdw.Parameters.AddWithValue("@Commento", Commento)
            cmdw.Parameters.AddWithValue("@Automatico", Automatico)
            cmdw.Parameters.AddWithValue("@UnitaDiMisura", UnitaDiMisura)
            cmdw.Parameters.AddWithValue("@TipoContatore", TipoContatore)
            cmdw.Parameters.AddWithValue("@Conversione", Conversione)
            cmdw.Parameters.AddWithValue("@Codice", Codice)
            cmdw.Connection = cn
            cmdw.ExecuteNonQuery()
        Else
            MySql = "INSERT INTO TipiDatiAnagrafici (Utente, DataAggiornamento, Codice, Descrizione, Tipo, Gruppo, TipoNumericoVirgola, SempreVisibile, VisibileDipendenti, PerSelezioniServizi, Scadenza, TabellaDescrittiva, Formula, Commento, Automatico, UnitaDiMisura, TipoContatore, Conversione) VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)"

            Dim cmdw As New OleDbCommand()
            cmdw.CommandText = MySql
            cmdw.Parameters.AddWithValue("@Utente", Utente)
            cmdw.Parameters.AddWithValue("@DataAggiornamento", Now)
            cmdw.Parameters.AddWithValue("@Codice", Codice)
            cmdw.Parameters.AddWithValue("@Descrizione", Descrizione)
            cmdw.Parameters.AddWithValue("@Tipo", Tipo)
            cmdw.Parameters.AddWithValue("@Gruppo", Gruppo)
            cmdw.Parameters.AddWithValue("@TipoNumericoVirgola", TipoNumericoVirgola)
            cmdw.Parameters.AddWithValue("@SempreVisibile", SempreVisibile)
            cmdw.Parameters.AddWithValue("@VisibileDipendenti", VisibileDipendenti)
            cmdw.Parameters.AddWithValue("@PerSelezioniServizi", PerSelezioniServizi)
            cmdw.Parameters.AddWithValue("@Scadenza", Scadenza)
            cmdw.Parameters.AddWithValue("@TabellaDescrittiva", TabellaDescrittiva)
            cmdw.Parameters.AddWithValue("@Formula", Formula)
            cmdw.Parameters.AddWithValue("@Commento", Commento)
            cmdw.Parameters.AddWithValue("@Automatico", Automatico)
            cmdw.Parameters.AddWithValue("@UnitaDiMisura", UnitaDiMisura)
            cmdw.Parameters.AddWithValue("@TipoContatore", TipoContatore)
            cmdw.Parameters.AddWithValue("@Conversione", Conversione)
            cmdw.Connection = cn
            cmdw.ExecuteNonQuery()
        End If
        myPOSTreader.Close()
        cn.Close()

        Call Pulisci()
    End Sub

    Sub LoadDati(ByVal StringaConnessione As String, ByVal Tabella As System.Data.DataTable)
        Dim cn As OleDbConnection

        cn = New Data.OleDb.OleDbConnection(StringaConnessione)

        cn.Open()
        Dim cmd As New OleDbCommand()

        cmd.CommandText = ("Select * From TipiDatiAnagrafici Order By Descrizione")

        cmd.Connection = cn

        Tabella.Clear()
        Tabella.Columns.Clear()
        Tabella.Columns.Add("Codice", GetType(String))
        Tabella.Columns.Add("Descrizione", GetType(String))
        Tabella.Columns.Add("Tipo", GetType(String))
        Tabella.Columns.Add("Gruppo", GetType(String))
        Tabella.Columns.Add("TipoNumericoVirgola", GetType(String))
        Tabella.Columns.Add("SempreVisibile", GetType(String))
        Tabella.Columns.Add("VisibileDipendenti", GetType(String))
        Tabella.Columns.Add("PerSelezioniServizi", GetType(String))
        Tabella.Columns.Add("Scadenza", GetType(String))
        Tabella.Columns.Add("TabellaDescrittiva", GetType(String))
        Tabella.Columns.Add("Formula", GetType(String))
        Tabella.Columns.Add("Commento", GetType(String))
        Tabella.Columns.Add("Automatico", GetType(String))
        Tabella.Columns.Add("UnitaDiMisura", GetType(String))
        Tabella.Columns.Add("TipoContatore", GetType(String))
        Tabella.Columns.Add("Conversione", GetType(String))

        Dim myPOSTreader As OleDbDataReader = cmd.ExecuteReader()
        Do While myPOSTreader.Read

            Dim myriga As System.Data.DataRow = Tabella.NewRow()
            myriga(0) = myPOSTreader.Item("Codice")
            myriga(1) = myPOSTreader.Item("Descrizione")
            myriga(2) = myPOSTreader.Item("Tipo")
            myriga(3) = myPOSTreader.Item("Gruppo")
            myriga(4) = myPOSTreader.Item("TipoNumericoVirgola")
            myriga(5) = myPOSTreader.Item("SempreVisibile")
            myriga(6) = myPOSTreader.Item("VisibileDipendenti")
            myriga(7) = myPOSTreader.Item("PerSelezioniServizi")
            myriga(8) = myPOSTreader.Item("Scadenza")
            myriga(9) = myPOSTreader.Item("TabellaDescrittiva")
            myriga(10) = myPOSTreader.Item("Formula")
            myriga(11) = myPOSTreader.Item("Commento")
            myriga(12) = myPOSTreader.Item("Automatico")
            myriga(13) = myPOSTreader.Item("UnitaDiMisura")
            myriga(14) = myPOSTreader.Item("TipoContatore")
            myriga(15) = myPOSTreader.Item("Conversione")

            Tabella.Rows.Add(myriga)
        Loop
        myPOSTreader.Close()
        cn.Close()

    End Sub

    Function StringaDb(ByVal Oggetto As Object) As String
        If IsDBNull(Oggetto) Then
            Return ""
        Else
            Return Oggetto
        End If
    End Function

    Function NumeroDb(ByVal Oggetto As Object) As Object
        If IsDBNull(Oggetto) Then
            Return 0
        Else
            Return Oggetto
        End If
    End Function

    Sub Leggi(ByVal StringaConnessione As String, ByVal xCodice As String)
        Dim cn As OleDbConnection

        cn = New Data.OleDb.OleDbConnection(StringaConnessione)

        cn.Open()
        Dim cmd As New OleDbCommand()
        cmd.CommandText = "Select * From TipiDatiAnagrafici WHERE Codice = ?"
        cmd.Parameters.AddWithValue("@Codicevariabile", xCodice)
        cmd.Connection = cn

        Dim myPOSTreader As OleDbDataReader = cmd.ExecuteReader()
        If myPOSTreader.Read Then
            Id = NumeroDb(myPOSTreader.Item("Id"))
            Codice = StringaDb(myPOSTreader.Item("Codice"))
            Descrizione = StringaDb(myPOSTreader.Item("Descrizione"))
            Tipo = NumeroDb(myPOSTreader.Item("Tipo"))
            Gruppo = StringaDb(myPOSTreader.Item("Gruppo"))
            TipoNumericoVirgola = StringaDb(myPOSTreader.Item("TipoNumericoVirgola"))
            SempreVisibile = StringaDb(myPOSTreader.Item("SempreVisibile"))
            VisibileDipendenti = StringaDb(myPOSTreader.Item("VisibileDipendenti"))
            PerSelezioniServizi = StringaDb(myPOSTreader.Item("PerSelezioniServizi"))
            Scadenza = StringaDb(myPOSTreader.Item("Scadenza"))
            TabellaDescrittiva = StringaDb(myPOSTreader.Item("TabellaDescrittiva"))
            Formula = StringaDb(myPOSTreader.Item("Formula"))
            Commento = StringaDb(myPOSTreader.Item("Commento"))
            Automatico = StringaDb(myPOSTreader.Item("Automatico"))
            UnitaDiMisura = StringaDb(myPOSTreader.Item("UnitaDiMisura"))
            TipoContatore = StringaDb(myPOSTreader.Item("TipoContatore"))
            Conversione = StringaDb(myPOSTreader.Item("Conversione"))
        Else
            Call Pulisci()
        End If
        cn.Close()
    End Sub

    Function TestUsato(ByVal StringaConnessione As String, ByVal xCodice As String) As Boolean
        Dim cn As OleDbConnection
        cn = New Data.OleDb.OleDbConnection(StringaConnessione)
        cn.Open()

        Dim cmd As New OleDbCommand()
        cmd.CommandText = "SELECT Count(*) FROM DatiVariabili" & _
                          " WHERE CodiceVariabile = ?"

        cmd.Parameters.AddWithValue("@CodiceVariabile", xCodice)

        cmd.Connection = cn
        Dim myPOSTreader As OleDbDataReader = cmd.ExecuteReader()
        If myPOSTreader.Read Then
            If myPOSTreader.Item(0) = 0 Then
                Return False
            Else
                Return True
                myPOSTreader.Close()
                cn.Close()
                Exit Function
            End If
        Else
            Return False
        End If
        myPOSTreader.Close()

        cmd.CommandText = "SELECT Count(*) FROM Giustificativi" & _
                          " WHERE TipoDatoAnagrafico = ?"

        cmd.Parameters.Clear()
        cmd.Parameters.AddWithValue("@TipoDatoAnagrafico", xCodice)

        myPOSTreader = cmd.ExecuteReader()
        If myPOSTreader.Read Then
            If myPOSTreader.Item(0) = 0 Then
                Return False
            Else
                Return True
                myPOSTreader.Close()
                cn.Close()
                Exit Function
            End If
        Else
            Return False
        End If
        myPOSTreader.Close()

        cmd.CommandText = "SELECT Count(*) FROM SchedaRiga" & _
                          " WHERE CodiceTipoDatoAnagrafico = ?"

        cmd.Parameters.Clear()
        cmd.Parameters.AddWithValue("@CodiceTipoDatoAnagrafico", xCodice)

        myPOSTreader = cmd.ExecuteReader()
        If myPOSTreader.Read Then
            If myPOSTreader.Item(0) = 0 Then
                Return False
            Else
                Return True
            End If
        Else
            Return False
        End If
        myPOSTreader.Close()
        cn.Close()
    End Function

    Function Esiste(ByVal StringaConnessione As String, ByVal xCodice As String) As Boolean
        Dim cn As OleDbConnection

        cn = New Data.OleDb.OleDbConnection(StringaConnessione)

        cn.Open()
        Dim cmd As New OleDbCommand()
        cmd.CommandText = "Select * From TipiDatiAnagrafici WHERE Codice = ?"
        cmd.Parameters.AddWithValue("@Codice", xCodice)
        cmd.Connection = cn

        Dim myPOSTreader As OleDbDataReader = cmd.ExecuteReader()

        If myPOSTreader.Read Then
            Return True
        Else
            Return False
        End If
        myPOSTreader.Close()
        cn.Close()
    End Function

    Sub UpDateDropBox(ByVal StringaConnessione As String, ByVal xGruppo As String, ByRef Appoggio As DropDownList)

        Dim cn As New Data.OleDb.OleDbConnection(StringaConnessione)

        cn.Open()
        Dim cmd As New OleDbCommand()
        If xGruppo = "" Then
            cmd.CommandText = "SELECT Codice, Descrizione FROM TipiDatiAnagrafici" & _
                               " ORDER BY Descrizione"
        Else
            cmd.CommandText = "SELECT Codice, Descrizione FROM TipiDatiAnagrafici" & _
                               " WHERE Gruppo = ?" & _
                               " ORDER BY Descrizione"
            cmd.Parameters.AddWithValue("@Gruppo", xGruppo)
        End If
        cmd.Connection = cn

        Appoggio.Items.Clear()
        Dim myPOSTreader As OleDbDataReader = cmd.ExecuteReader()
        Do While myPOSTreader.Read
            Appoggio.Items.Add(myPOSTreader.Item("Descrizione"))
            Appoggio.Items(Appoggio.Items.Count - 1).Value = myPOSTreader.Item("Codice")
        Loop
        myPOSTreader.Close()
        cn.Close()
        Appoggio.Items.Add("")
        Appoggio.Items(Appoggio.Items.Count - 1).Value = ""
        Appoggio.Items(Appoggio.Items.Count - 1).Selected = True

    End Sub

    Public Function CampoTipiDatiAnagrafici(ByVal StringaConnessione As String, ByVal xCodice As String, ByVal xCampo As String) As Object

        Dim cn As New Data.OleDb.OleDbConnection(StringaConnessione)

        CampoTipiDatiAnagrafici = ""

        cn.Open()
        Dim cmd As New OleDbCommand()
        cmd.CommandText = "SELECT " & xCampo & " FROM TipiDatiAnagrafici WHERE Codice = ?"

        cmd.Parameters.AddWithValue("@Codice", xCodice)

        cmd.Connection = cn

        Dim myPOSTreader As OleDbDataReader = cmd.ExecuteReader()

        If myPOSTreader.Read Then
            CampoTipiDatiAnagrafici = myPOSTreader.Item(xCampo)
        End If
        myPOSTreader.Close()
        cn.Close()
    End Function

End Class
