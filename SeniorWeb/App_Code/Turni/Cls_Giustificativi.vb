Imports Microsoft.VisualBasic
Imports System.Data.OleDb
Imports System.Web.Hosting

Public Class Cls_Giustificativi
    Public Id As Long
    Public Utente As String
    Public DataAggiornamento As Date
    Public Codice As String
    Public Descrizione As String
    Public Visibile As String
    Public Tipo As String
    Public Malattia As String
    Public OrariProfiliOrari As String
    Public GiustificativiProfiliOrari As String
    Public RegolaCalcolo1 As String
    Public GiustificativoPerAssenza As String
    Public GiustificativoPerPresenza As String
    Public SuTurniTimbrature As String
    Public SuOrdiniServizio As String
    Public RegoleGenerali As String
    Public RegolaCalcolo2 As String
    Public RegolaCalcolo3 As String
    Public SuTempoPrevisto As String
    Public SuFestivoInfrasettimanale As String
    Public VisibileDipendenti As String
    Public TipoDatoAnagrafico As String
    Public ColoreSfondo As Long
    Public ColoreScritta As Long
    Public Peso As Integer
    Public Conversione As String
    Public OreSuConversione As String
    Public LavoroOrdinario As String
    Public SuQuadroAssenze As String
    Public ControlloSuQuadroAssenze As String
    Public SuSchedaAssenze As String
    Public Privacy As String
    Public RegolaControllo As String
    Public Assenza_SuRiposo As String
    Public Assenza_RiposoSuFestivi As String
    Public Assenza_RiposoSuFestiviInfrasettimanali As String
    Public Assenza_RimuoviOrari As String
    Public Assenza_ChkGiorno As String
    Public Parentela As String
    Public ControlloFasciaObbligatoria As String
    Public TimbratureComeOrari As String
    Public ControlloOrdinarioStraordinario As String
    Public ConsideraRiposo As String
    Public Richiesta As String
    Public CollegamentoEPersonam As String
    Public SuSituazioneAssenti As String
    Public EliminaValoreZero As String
    Public AssenzaAlleZero As String

    Public Sub Pulisci()
        Id = 0
        Utente = ""
        DataAggiornamento = Nothing
        Codice = ""
        Descrizione = ""
        Visibile = ""
        Tipo = ""
        Malattia = ""
        OrariProfiliOrari = ""
        GiustificativiProfiliOrari = ""
        RegolaCalcolo1 = ""
        GiustificativoPerAssenza = ""
        GiustificativoPerPresenza = ""
        SuTurniTimbrature = ""
        SuOrdiniServizio = ""
        RegoleGenerali = ""
        RegolaCalcolo2 = ""
        RegolaCalcolo3 = ""
        SuTempoPrevisto = ""
        SuFestivoInfrasettimanale = ""
        VisibileDipendenti = ""
        TipoDatoAnagrafico = ""
        ColoreSfondo = 0
        ColoreScritta = 0
        Peso = 0
        Conversione = ""
        OreSuConversione = ""
        LavoroOrdinario = ""
        SuQuadroAssenze = ""
        ControlloSuQuadroAssenze = ""
        SuSchedaAssenze = ""
        Privacy = ""
        RegolaControllo = ""
        Assenza_SuRiposo = ""
        Assenza_RiposoSuFestivi = ""
        Assenza_RiposoSuFestiviInfrasettimanali = ""
        Assenza_RimuoviOrari = ""
        Assenza_ChkGiorno = ""
        Parentela = ""
        ControlloFasciaObbligatoria = ""
        TimbratureComeOrari = ""
        ControlloOrdinarioStraordinario = ""
        ConsideraRiposo = ""
        Richiesta = ""
        CollegamentoEPersonam = ""
        SuSituazioneAssenti = ""
        EliminaValoreZero = ""
        AssenzaAlleZero = ""
    End Sub

    Public Sub Elimina(ByVal StringaConnessione As String, ByVal xCodice As String)
        Dim cn As OleDbConnection

        cn = New Data.OleDb.OleDbConnection(StringaConnessione)

        cn.Open()
        Dim cmd As New OleDbCommand()
        cmd.Connection = cn
        cmd.CommandText = "DELETE FROM Giustificativi where Codice = ?"
        cmd.Parameters.AddWithValue("@Codice", xCodice)
        cmd.ExecuteNonQuery()
        cn.Close()
    End Sub

    Sub Aggiorna(ByVal StringaConnessione As String)
        Dim cn As OleDbConnection

        cn = New Data.OleDb.OleDbConnection(StringaConnessione)

        cn.Open()
        Dim cmd As New OleDbCommand()
        cmd.Connection = cn
        cmd.CommandText = "SELECT * FROM Giustificativi WHERE Codice = ?"
        cmd.Parameters.AddWithValue("@Codice", Codice)
        Dim myPOSTreader As OleDbDataReader = cmd.ExecuteReader()
        If myPOSTreader.Read Then
            Dim cmdw As New OleDbCommand()
            cmdw.Connection = cn
            cmdw.CommandText = "UPDATE Giustificativi SET " & _
                               " Utente = ?," & _
                               " DataAggiornamento  = ?," & _
                               " Descrizione = ?," & _
                               " Visibile = ?," & _
                               " Tipo = ?," & _
                               " Malattia = ?," & _
                               " OrariProfiliOrari = ?," & _
                               " GiustificativiProfiliOrari = ?," & _
                               " RegolaCalcolo1 = ?," & _
                               " GiustificativoPerAssenza = ?," & _
                               " GiustificativoPerPresenza = ?," & _
                               " SuTurniTimbrature = ?," & _
                               " SuOrdiniServizio = ?," & _
                               " RegoleGenerali = ?," & _
                               " RegolaCalcolo2 = ?," & _
                               " RegolaCalcolo3 = ?," & _
                               " SuTempoPrevisto = ?," & _
                               " SuFestivoInfrasettimanale = ?," & _
                               " VisibileDipendenti = ?," & _
                               " TipoDatoAnagrafico = ?," & _
                               " ColoreSfondo = ?," & _
                               " ColoreScritta = ?," & _
                               " Peso = ?," & _
                               " Conversione = ?," & _
                               " OreSuConversione = ?," & _
                               " LavoroOrdinario = ?," & _
                               " SuQuadroAssenze = ?," & _
                               " ControlloSuQuadroAssenze = ?," & _
                               " SuSchedaAssenze = ?," & _
                               " Privacy = ?," & _
                               " RegolaControllo = ?," & _
                               " Assenza_SuRiposo = ?," & _
                               " Assenza_RiposoSuFestivi = ?," & _
                               " Assenza_RiposoSuFestiviInfrasettimanali = ?," & _
                               " Assenza_RimuoviOrari = ?," & _
                               " Assenza_ChkGiorno = ?," & _
                               " Parentela = ?," & _
                               " ControlloFasciaObbligatoria = ?," & _
                               " TimbratureComeOrari = ?," & _
                               " ControlloOrdinarioStraordinario = ?," & _
                               " ConsideraRiposo = ?," & _
                               " Richiesta = ?," & _
                               " CollegamentoEPersonam = ?," & _
                               " SuSituazioneAssenti = ?," & _
                               " EliminaValoreZero = ?" & _
                               " AssenzaAlleZero = ?" & _
                               " WHERE Codice = ?"

            cmdw.Parameters.AddWithValue("@Utente", Utente)
            cmdw.Parameters.AddWithValue("@DataAggiornamento ", Now)
            cmdw.Parameters.AddWithValue("@Descrizione", Descrizione)
            cmdw.Parameters.AddWithValue("@Visibile", Visibile)
            cmdw.Parameters.AddWithValue("@Tipo", Tipo)
            cmdw.Parameters.AddWithValue("@Malattia", Malattia)
            cmdw.Parameters.AddWithValue("@OrariProfiliOrari", OrariProfiliOrari)
            cmdw.Parameters.AddWithValue("@GiustificativiProfiliOrari", GiustificativiProfiliOrari)
            cmdw.Parameters.AddWithValue("@RegolaCalcolo1", RegolaCalcolo1)
            cmdw.Parameters.AddWithValue("@GiustificativoPerAssenza", GiustificativoPerAssenza)
            cmdw.Parameters.AddWithValue("@GiustificativoPerPresenza", GiustificativoPerPresenza)
            cmdw.Parameters.AddWithValue("@SuTurniTimbrature", SuTurniTimbrature)
            cmdw.Parameters.AddWithValue("@SuOrdiniServizio", SuOrdiniServizio)
            cmdw.Parameters.AddWithValue("@RegoleGenerali", RegoleGenerali)
            cmdw.Parameters.AddWithValue("@RegolaCalcolo2", RegolaCalcolo2)
            cmdw.Parameters.AddWithValue("@RegolaCalcolo3", RegolaCalcolo3)
            cmdw.Parameters.AddWithValue("@SuTempoPrevisto", SuTempoPrevisto)
            cmdw.Parameters.AddWithValue("@SuFestivoInfrasettimanale", SuFestivoInfrasettimanale)
            cmdw.Parameters.AddWithValue("@VisibileDipendenti", VisibileDipendenti)
            cmdw.Parameters.AddWithValue("@TipoDatoAnagrafico", TipoDatoAnagrafico)
            cmdw.Parameters.AddWithValue("@ColoreSfondo", ColoreSfondo)
            cmdw.Parameters.AddWithValue("@ColoreScritta", ColoreScritta)
            cmdw.Parameters.AddWithValue("@Peso", Peso)
            cmdw.Parameters.AddWithValue("@Conversione", Conversione)
            cmdw.Parameters.AddWithValue("@OreSuConversione", OreSuConversione)
            cmdw.Parameters.AddWithValue("@LavoroOrdinario", LavoroOrdinario)
            cmdw.Parameters.AddWithValue("@SuQuadroAssenze", SuQuadroAssenze)
            cmdw.Parameters.AddWithValue("@ControlloSuQuadroAssenze", ControlloSuQuadroAssenze)
            cmdw.Parameters.AddWithValue("@SuSchedaAssenze", SuSchedaAssenze)
            cmdw.Parameters.AddWithValue("@Privacy", Privacy)
            cmdw.Parameters.AddWithValue("@RegolaControllo", RegolaControllo)
            cmdw.Parameters.AddWithValue("@Assenza_SuRiposo", Assenza_SuRiposo)
            cmdw.Parameters.AddWithValue("@Assenza_RiposoSuFestivi", Assenza_RiposoSuFestivi)
            cmdw.Parameters.AddWithValue("@Assenza_RiposoSuFestiviInfrasettimanali", Assenza_RiposoSuFestiviInfrasettimanali)
            cmdw.Parameters.AddWithValue("@Assenza_RimuoviOrari", Assenza_RimuoviOrari)
            cmdw.Parameters.AddWithValue("@Assenza_ChkGiorno", Assenza_ChkGiorno)
            cmdw.Parameters.AddWithValue("@Parentela", Parentela)
            cmdw.Parameters.AddWithValue("@ControlloFasciaObbligatoria", ControlloFasciaObbligatoria)
            cmdw.Parameters.AddWithValue("@TimbratureComeOrari", TimbratureComeOrari)
            cmdw.Parameters.AddWithValue("@ControlloOrdinarioStraordinario", ControlloOrdinarioStraordinario)
            cmdw.Parameters.AddWithValue("@ConsideraRiposo", ConsideraRiposo)
            cmdw.Parameters.AddWithValue("@Richiesta", Richiesta)
            cmdw.Parameters.AddWithValue("@CollegamentoEPersonam", CollegamentoEPersonam)
            cmdw.Parameters.AddWithValue("@SuSituazioneAssenti", SuSituazioneAssenti)
            cmdw.Parameters.AddWithValue("@EliminaValoreZero", EliminaValoreZero)
            cmdw.Parameters.AddWithValue("@AssenzaAlleZero", AssenzaAlleZero)
            cmdw.Parameters.AddWithValue("@Codice", Codice)
            cmdw.ExecuteNonQuery()
        Else
            Dim cmdw As New OleDbCommand()
            cmdw.Connection = cn
            cmdw.CommandText = "INSERT INTO Giustificativi (Utente, DataAggiornamento, Codice, Descrizione, Visibile, Tipo, Malattia, OrariProfiliOrari, GiustificativiProfiliOrari, RegolaCalcolo1, GiustificativoPerAssenza, GiustificativoPerPresenza, SuTurniTimbrature, SuOrdiniServizio, RegoleGenerali, RegolaCalcolo2, RegolaCalcolo3, SuTempoPrevisto, SuFestivoInfrasettimanale, VisibileDipendenti, TipoDatoAnagrafico, ColoreSfondo, ColoreScritta, Peso, Conversione, OreSuConversione, LavoroOrdinario, SuQuadroAssenze, ControlloSuQuadroAssenze, SuSchedaAssenze, Privacy, RegolaControllo, Assenza_SuRiposo, Assenza_RiposoSuFestivi, Assenza_RiposoSuFestiviInfrasettimanali, Assenza_RimuoviOrari, Assenza_ChkGiorno, Parentela, ControlloFasciaObbligatoria, TimbratureComeOrari, ControlloOrdinarioStraordinario, ConsideraRiposo, Richiesta, CollegamentoEPersonam, SuSituazioneAssenti, EliminaValoreZero,AssenzaAlleZero) VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)"

            cmdw.Parameters.AddWithValue("@Utente", Utente)
            cmdw.Parameters.AddWithValue("@DataAggiornamento", Now)
            cmdw.Parameters.AddWithValue("@Codice", Codice)
            cmdw.Parameters.AddWithValue("@Descrizione", Descrizione)
            cmdw.Parameters.AddWithValue("@Visibile", Visibile)
            cmdw.Parameters.AddWithValue("@Tipo", Tipo)
            cmdw.Parameters.AddWithValue("@Malattia", Malattia)
            cmdw.Parameters.AddWithValue("@OrariProfiliOrari", OrariProfiliOrari)
            cmdw.Parameters.AddWithValue("@GiustificativiProfiliOrari", GiustificativiProfiliOrari)
            cmdw.Parameters.AddWithValue("@RegolaCalcolo1", RegolaCalcolo1)
            cmdw.Parameters.AddWithValue("@GiustificativoPerAssenza", GiustificativoPerAssenza)
            cmdw.Parameters.AddWithValue("@GiustificativoPerPresenza", GiustificativoPerPresenza)
            cmdw.Parameters.AddWithValue("@SuTurniTimbrature", SuTurniTimbrature)
            cmdw.Parameters.AddWithValue("@SuOrdiniServizio", SuOrdiniServizio)
            cmdw.Parameters.AddWithValue("@RegoleGenerali", RegoleGenerali)
            cmdw.Parameters.AddWithValue("@RegolaCalcolo2", RegolaCalcolo2)
            cmdw.Parameters.AddWithValue("@RegolaCalcolo3", RegolaCalcolo3)
            cmdw.Parameters.AddWithValue("@SuTempoPrevisto", SuTempoPrevisto)
            cmdw.Parameters.AddWithValue("@SuFestivoInfrasettimanale", SuFestivoInfrasettimanale)
            cmdw.Parameters.AddWithValue("@VisibileDipendenti", VisibileDipendenti)
            cmdw.Parameters.AddWithValue("@TipoDatoAnagrafico", TipoDatoAnagrafico)
            cmdw.Parameters.AddWithValue("@ColoreSfondo", ColoreSfondo)
            cmdw.Parameters.AddWithValue("@ColoreScritta", ColoreScritta)
            cmdw.Parameters.AddWithValue("@Peso", Peso)
            cmdw.Parameters.AddWithValue("@Conversione", Conversione)
            cmdw.Parameters.AddWithValue("@OreSuConversione", OreSuConversione)
            cmdw.Parameters.AddWithValue("@LavoroOrdinario", LavoroOrdinario)
            cmdw.Parameters.AddWithValue("@SuQuadroAssenze", SuQuadroAssenze)
            cmdw.Parameters.AddWithValue("@ControlloSuQuadroAssenze", ControlloSuQuadroAssenze)
            cmdw.Parameters.AddWithValue("@SuSchedaAssenze", SuSchedaAssenze)
            cmdw.Parameters.AddWithValue("@Privacy", Privacy)
            cmdw.Parameters.AddWithValue("@RegolaControllo", RegolaControllo)
            cmdw.Parameters.AddWithValue("@Assenza_SuRiposo", Assenza_SuRiposo)
            cmdw.Parameters.AddWithValue("@Assenza_RiposoSuFestivi", Assenza_RiposoSuFestivi)
            cmdw.Parameters.AddWithValue("@Assenza_RiposoSuFestiviInfrasettimanali", Assenza_RiposoSuFestiviInfrasettimanali)
            cmdw.Parameters.AddWithValue("@Assenza_RimuoviOrari", Assenza_RimuoviOrari)
            cmdw.Parameters.AddWithValue("@Assenza_ChkGiorno", Assenza_ChkGiorno)
            cmdw.Parameters.AddWithValue("@Parentela", Parentela)
            cmdw.Parameters.AddWithValue("@ControlloFasciaObbligatoria", ControlloFasciaObbligatoria)
            cmdw.Parameters.AddWithValue("@TimbratureComeOrari", TimbratureComeOrari)
            cmdw.Parameters.AddWithValue("@ControlloOrdinarioStraordinario", ControlloOrdinarioStraordinario)
            cmdw.Parameters.AddWithValue("@ConsideraRiposo", ConsideraRiposo)
            cmdw.Parameters.AddWithValue("@Richiesta", Richiesta)
            cmdw.Parameters.AddWithValue("@CollegamentoEPersonam", CollegamentoEPersonam)
            cmdw.Parameters.AddWithValue("@SuSituazioneAssenti", SuSituazioneAssenti)
            cmdw.Parameters.AddWithValue("@EliminaValoreZero", EliminaValoreZero)
            cmdw.Parameters.AddWithValue("@AssenzaAlleZero", AssenzaAlleZero)
            cmdw.Connection = cn
            cmdw.ExecuteNonQuery()
        End If
        myPOSTreader.Close()
        cn.Close()

        Call Pulisci()
    End Sub

    Sub LoadDati(ByVal StringaConnessione As String, ByVal Tabella As System.Data.DataTable)
        Dim cn As OleDbConnection

        cn = New Data.OleDb.OleDbConnection(StringaConnessione)

        cn.Open()

        Dim cmd As New OleDbCommand()
        cmd.Connection = cn

        cmd.CommandText = ("Select * From Giustificativi Order By Descrizione")

        Tabella.Clear()
        Tabella.Columns.Add("Codice", GetType(String))
        Tabella.Columns.Add("Descrizione", GetType(String))
        Tabella.Columns.Add("Tipo", GetType(String))

        Dim myPOSTreader As OleDbDataReader = cmd.ExecuteReader()
        Do While myPOSTreader.Read

            Dim myriga As System.Data.DataRow = Tabella.NewRow()
            myriga(0) = myPOSTreader.Item("Codice")
            myriga(1) = myPOSTreader.Item("Descrizione")
            myriga(2) = myPOSTreader.Item("Tipo")

            Tabella.Rows.Add(myriga)
        Loop
        myPOSTreader.Close()
        cn.Close()

    End Sub

    Function StringaDb(ByVal Oggetto As Object) As String
        If IsDBNull(Oggetto) Then
            Return ""
        Else
            Return Oggetto
        End If
    End Function

    Function NumeroDb(ByVal Oggetto As Object) As Object
        If IsDBNull(Oggetto) Then
            Return 0
        Else
            Return Oggetto
        End If
    End Function

    Sub Leggi(ByVal StringaConnessione As String, ByVal xCodice As String)
        Dim cn As OleDbConnection

        cn = New Data.OleDb.OleDbConnection(StringaConnessione)

        cn.Open()
        Dim cmd As New OleDbCommand()
        cmd.CommandText = "Select * From Giustificativi" & _
                            " Where Codice = ?"
        cmd.Parameters.AddWithValue("@Codice", xCodice)
        cmd.Connection = cn

        Dim myPOSTreader As OleDbDataReader = cmd.ExecuteReader()
        If myPOSTreader.Read Then
            Id = NumeroDb(myPOSTreader.Item("Id"))
            Codice = StringaDb(myPOSTreader.Item("Codice"))
            Descrizione = StringaDb(myPOSTreader.Item("Descrizione"))
            Visibile = StringaDb(myPOSTreader.Item("Visibile"))
            Tipo = StringaDb(myPOSTreader.Item("Tipo"))
            Malattia = StringaDb(myPOSTreader.Item("Malattia"))
            OrariProfiliOrari = StringaDb(myPOSTreader.Item("OrariProfiliOrari"))
            GiustificativiProfiliOrari = StringaDb(myPOSTreader.Item("GiustificativiProfiliOrari"))
            RegolaCalcolo1 = StringaDb(myPOSTreader.Item("RegolaCalcolo1"))
            GiustificativoPerAssenza = StringaDb(myPOSTreader.Item("GiustificativoPerAssenza"))
            GiustificativoPerPresenza = StringaDb(myPOSTreader.Item("GiustificativoPerPresenza"))
            SuTurniTimbrature = StringaDb(myPOSTreader.Item("SuTurniTimbrature"))
            SuOrdiniServizio = StringaDb(myPOSTreader.Item("SuOrdiniServizio"))
            RegoleGenerali = StringaDb(myPOSTreader.Item("RegoleGenerali"))
            RegolaCalcolo2 = StringaDb(myPOSTreader.Item("RegolaCalcolo2"))
            RegolaCalcolo3 = StringaDb(myPOSTreader.Item("RegolaCalcolo3"))
            SuTempoPrevisto = StringaDb(myPOSTreader.Item("SuTempoPrevisto"))
            SuFestivoInfrasettimanale = StringaDb(myPOSTreader.Item("SuFestivoInfrasettimanale"))
            VisibileDipendenti = StringaDb(myPOSTreader.Item("VisibileDipendenti"))
            TipoDatoAnagrafico = StringaDb(myPOSTreader.Item("TipoDatoAnagrafico"))
            ColoreSfondo = NumeroDb(myPOSTreader.Item("ColoreSfondo"))
            ColoreScritta = NumeroDb(myPOSTreader.Item("ColoreScritta"))
            Peso = NumeroDb(myPOSTreader.Item("Peso"))
            Conversione = StringaDb(myPOSTreader.Item("Conversione"))
            OreSuConversione = StringaDb(myPOSTreader.Item("OreSuConversione"))
            LavoroOrdinario = StringaDb(myPOSTreader.Item("LavoroOrdinario"))
            SuQuadroAssenze = StringaDb(myPOSTreader.Item("SuQuadroAssenze"))
            ControlloSuQuadroAssenze = StringaDb(myPOSTreader.Item("ControlloSuQuadroAssenze"))
            SuSchedaAssenze = StringaDb(myPOSTreader.Item("SuSchedaAssenze"))
            Privacy = StringaDb(myPOSTreader.Item("Privacy"))
            RegolaControllo = StringaDb(myPOSTreader.Item("RegolaControllo"))
            Assenza_SuRiposo = StringaDb(myPOSTreader.Item("Assenza_SuRiposo"))
            Assenza_RiposoSuFestivi = StringaDb(myPOSTreader.Item("Assenza_RiposoSuFestivi"))
            Assenza_RiposoSuFestiviInfrasettimanali = StringaDb(myPOSTreader.Item("Assenza_RiposoSuFestiviInfrasettimanali"))
            Assenza_RimuoviOrari = StringaDb(myPOSTreader.Item("Assenza_RimuoviOrari"))
            Assenza_ChkGiorno = StringaDb(myPOSTreader.Item("Assenza_ChkGiorno"))
            Parentela = StringaDb(myPOSTreader.Item("Parentela"))
            ControlloFasciaObbligatoria = StringaDb(myPOSTreader.Item("ControlloFasciaObbligatoria"))
            TimbratureComeOrari = StringaDb(myPOSTreader.Item("TimbratureComeOrari"))
            ControlloOrdinarioStraordinario = StringaDb(myPOSTreader.Item("ControlloOrdinarioStraordinario"))
            ConsideraRiposo = StringaDb(myPOSTreader.Item("ConsideraRiposo"))
            Richiesta = StringaDb(myPOSTreader.Item("Richiesta"))
            CollegamentoEPersonam = StringaDb(myPOSTreader.Item("CollegamentoEPersonam"))
            SuSituazioneAssenti = StringaDb(myPOSTreader.Item("SuSituazioneAssenti"))
            EliminaValoreZero = StringaDb(myPOSTreader.Item("EliminaValoreZero"))
            AssenzaAlleZero = StringaDb(myPOSTreader.Item("AssenzaAlleZero"))
        Else
            Call Pulisci()
        End If
        cn.Close()
    End Sub

    Function TestUsato(ByVal StringaConnessione As String, ByVal xCodice As String) As Boolean
        Dim cn As OleDbConnection
        cn = New Data.OleDb.OleDbConnection(StringaConnessione)
        cn.Open()

        Dim cmd As New OleDbCommand()
        cmd.CommandText = "SELECT Count(*) FROM Orari" & _
                          " WHERE Giustificativo = ?"
        cmd.Parameters.AddWithValue("@Giustificativo", xCodice)

        cmd.Connection = cn
        Dim myPOSTreader As OleDbDataReader = cmd.ExecuteReader()
        If myPOSTreader.Read Then
            If myPOSTreader.Item(0) = 0 Then
                Return False
            Else
                Return True
                myPOSTreader.Close()
                cn.Close()
                Exit Function
            End If
        Else
            Return False
        End If
        myPOSTreader.Close()

        cmd.CommandText = "SELECT Count(*) FROM ProfiliOrariModificati" & _
                          " WHERE Giustificativo = ?"
        cmd.Parameters.Clear()
        cmd.Parameters.AddWithValue("@Giustificativo", xCodice)

        myPOSTreader = cmd.ExecuteReader()
        If myPOSTreader.Read Then
            If myPOSTreader.Item(0) = 0 Then
                Return False
            Else
                Return True
                myPOSTreader.Close()
                cn.Close()
                Exit Function
            End If
        Else
            Return False
        End If
        myPOSTreader.Close()

        cmd.CommandText = "SELECT Count(*) FROM GiustificativiDelGiorno" & _
                          " WHERE Giustificativo = ?"
        cmd.Parameters.Clear()
        cmd.Parameters.AddWithValue("@Giustificativo", xCodice)

        myPOSTreader = cmd.ExecuteReader()
        If myPOSTreader.Read Then
            If myPOSTreader.Item(0) = 0 Then
                Return False
            Else
                Return True
                myPOSTreader.Close()
                cn.Close()
                Exit Function
            End If
        Else
            Return False
        End If
        myPOSTreader.Close()

        cmd.CommandText = "SELECT Count(*) FROM OrdiniServizio" & _
                          " WHERE Giustificativo = ?"
        cmd.Parameters.Clear()
        cmd.Parameters.AddWithValue("@Giustificativo", xCodice)

        myPOSTreader = cmd.ExecuteReader()
        If myPOSTreader.Read Then
            If myPOSTreader.Item(0) = 0 Then
                Return False
            Else
                Return True
                myPOSTreader.Close()
                cn.Close()
                Exit Function
            End If
        Else
            Return False
        End If
        myPOSTreader.Close()

        cmd.CommandText = "SELECT Count(*) FROM OrdiniServizioRiga" & _
                          " WHERE Servizio = ?"
        cmd.Parameters.Clear()
        cmd.Parameters.AddWithValue("@Servizio", xCodice)

        myPOSTreader = cmd.ExecuteReader()
        If myPOSTreader.Read Then
            If myPOSTreader.Item(0) = 0 Then
                Return False
            Else
                Return True
                myPOSTreader.Close()
                cn.Close()
                Exit Function
            End If
        Else
            Return False
        End If
        myPOSTreader.Close()

        cmd.CommandText = "SELECT Count(*) FROM OrdiniServizioRigaComandanti" & _
                          " WHERE Servizio = ?"
        cmd.Parameters.Clear()
        cmd.Parameters.AddWithValue("@Servizio", xCodice)

        myPOSTreader = cmd.ExecuteReader()
        If myPOSTreader.Read Then
            If myPOSTreader.Item(0) = 0 Then
                Return False
            Else
                Return True
                myPOSTreader.Close()
                cn.Close()
                Exit Function
            End If
        Else
            Return False
        End If
        myPOSTreader.Close()

        cmd.CommandText = "SELECT Count(*) FROM RichiesteAssenza" & _
                          " WHERE Giustificativo = ?"
        cmd.Parameters.Clear()
        cmd.Parameters.AddWithValue("@Giustificativo", xCodice)

        myPOSTreader = cmd.ExecuteReader()
        If myPOSTreader.Read Then
            If myPOSTreader.Item(0) = 0 Then
                Return False
            Else
                Return True
                myPOSTreader.Close()
                cn.Close()
                Exit Function
            End If
        Else
            Return False
        End If
        myPOSTreader.Close()

        Dim p As New Cls_ParametriTurni
        p.Leggi(StringaConnessione)

        Select Case xCodice
            Case p.GiustificativoOrarioLavoro
                Return True
            Case p.GiustificativoRiposo
                Return True
            Case p.GiustificativoAssenza
                Return True
            Case p.GiustificativoSostituzione
                Return True
            Case p.GiustificativoEstensioneOrario
                Return True
            Case p.GiustificativoEntrataPosticipata
                Return True
            Case p.GiustificativoUscitaAnticipata
                Return True
            Case p.GiustificativoEntrataAnticipata
                Return True
            Case p.GiustificativoUscitaPosticipata
                Return True
        End Select
    End Function

    Function Esiste(ByVal ConnectionString As String, ByVal xCodice As String) As Boolean
        Dim cn As OleDbConnection

        cn = New Data.OleDb.OleDbConnection(ConnectionString)

        cn.Open()
        Dim cmd As New OleDbCommand()
        cmd.CommandText = "Select * FROM Giustificativi" & _
                          " WHERE Codice = ?"
        cmd.Parameters.AddWithValue("@Codice", xCodice)
        cmd.Connection = cn

        Dim myPOSTreader As OleDbDataReader = cmd.ExecuteReader()

        If myPOSTreader.Read Then
            Return True
        Else
            Return False
        End If
        myPOSTreader.Close()
        cn.Close()
    End Function

    Function DecodificaGiustificativo(ByVal ConnectionString As String, ByVal xCodice As String) As String
        Dim cn As OleDbConnection

        cn = New Data.OleDb.OleDbConnection(ConnectionString)


        DecodificaGiustificativo = ""
        cn.Open()
        Dim cmd As New OleDbCommand()
        cmd.CommandText = "Select * FROM Giustificativi" & _
                          " WHERE Codice = ?"
        cmd.Parameters.AddWithValue("@Codice", xCodice)
        cmd.Connection = cn

        Dim myPOSTreader As OleDbDataReader = cmd.ExecuteReader()

        If myPOSTreader.Read Then            
            DecodificaGiustificativo = StringaDb(myPOSTreader.Item("Descrizione"))
        End If
        myPOSTreader.Close()
        cn.Close()
    End Function

    Function DescrizioneDuplicata(ByVal ConnectionString As String, ByVal xCodice As String, ByVal xDescrizione As String) As Boolean
        DescrizioneDuplicata = False
        Dim cn As OleDbConnection

        cn = New Data.OleDb.OleDbConnection(ConnectionString)

        cn.Open()
        Dim cmd As New OleDbCommand()
        cmd.CommandText = "Select * FROM Giustificativi" & _
                          " WHERE Descrizione = ?"
        cmd.Parameters.AddWithValue("@Descrizione", xDescrizione)
        cmd.Connection = cn

        Dim myPOSTreader As OleDbDataReader = cmd.ExecuteReader()

        Do While myPOSTreader.Read
            If myPOSTreader.Item("Codice") <> xCodice Then
                Return True
                Exit Do
            End If
        Loop
        myPOSTreader.Close()
        cn.Close()
    End Function

    Sub UpDateDropBox(ByVal ConnectionString As String, ByVal xTipo As String, ByVal xRichiesta As String, ByRef Appoggio As DropDownList)
        Dim cn As OleDbConnection

        cn = New Data.OleDb.OleDbConnection(ConnectionString)

        cn.Open()
        Dim cmd As New OleDbCommand()
        cmd.Connection = cn

        Select Case xTipo
            Case ""
                If xRichiesta = "" Then
                    cmd.CommandText = "SELECT Codice, Descrizione FROM Giustificativi ORDER BY Descrizione"
                Else
                    cmd.CommandText = "SELECT Codice, Descrizione FROM Giustificativi" & _
                                      " WHERE Richiesta = '1' OR Richiesta = '2' OR Richiesta = '3'" & _
                                      " ORDER BY Descrizione"
                End If
            Case Else
                If xRichiesta = "" Then
                    cmd.CommandText = "SELECT Codice, Descrizione FROM Giustificativi" & _
                                      " WHERE Tipo = ?" & _
                                      " ORDER BY Descrizione"
                Else
                    cmd.CommandText = "SELECT Codice, Descrizione FROM Giustificativi" & _
                                      " WHERE Tipo = ?" & _
                                      " AND (Richiesta = '1' OR Richiesta = '2' OR Richiesta = '3')" & _
                                      " ORDER BY Descrizione"
                End If
                cmd.Parameters.AddWithValue("@Tipo", xTipo)
        End Select

        Appoggio.Items.Clear()
        Dim myPOSTreader As OleDbDataReader = cmd.ExecuteReader()
        Do While myPOSTreader.Read
            Appoggio.Items.Add(myPOSTreader.Item("Descrizione"))
            Appoggio.Items(Appoggio.Items.Count - 1).Value = myPOSTreader.Item("Codice")
        Loop
        myPOSTreader.Close()
        cn.Close()
        Appoggio.Items.Add("")
        Appoggio.Items(Appoggio.Items.Count - 1).Value = ""
        Appoggio.Items(Appoggio.Items.Count - 1).Selected = True

    End Sub

    Function CampoGiustificativi(ByVal ConnectionString As String, ByVal xCodice As String, ByVal xCampo As String) As Object
        Dim cn As OleDbConnection

        CampoGiustificativi = ""

        cn = New Data.OleDb.OleDbConnection(ConnectionString)

        cn.Open()
        Dim cmd As New OleDbCommand()
        cmd.CommandText = "Select " & xCampo & " FROM Giustificativi WHERE Codice = ?"

        cmd.Parameters.AddWithValue("@Codice", xCodice)
        cmd.Connection = cn

        Dim myPOSTreader As OleDbDataReader = cmd.ExecuteReader()
        If myPOSTreader.Read Then
            Return myPOSTreader.Item(xCampo)
        End If
        myPOSTreader.Close()
        cn.Close()
    End Function

    Function Decodifica(ByVal ConnectionString As String, ByVal xCodice As String) As String
        Dim cn As OleDbConnection

        cn = New Data.OleDb.OleDbConnection(ConnectionString)

        cn.Open()
        Dim cmd As New OleDbCommand()
        cmd.CommandText = "Select * FROM Giustificativi" & _
                          " WHERE Codice = ?"
        cmd.Parameters.AddWithValue("@Codice", xCodice)
        cmd.Connection = cn

        Dim myPOSTreader As OleDbDataReader = cmd.ExecuteReader()

        If myPOSTreader.Read Then
            If myPOSTreader.Item(0) = 0 Then
                Return ""
            Else
                Return myPOSTreader.Item("Descrizione")
            End If
        Else
            Return ""
        End If
        myPOSTreader.Close()
        cn.Close()
    End Function

End Class
