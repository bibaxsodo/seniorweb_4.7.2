Imports Microsoft.VisualBasic
Imports System.Data.OleDb
Imports System.Web.Hosting

Public Class Cls_NotificheDipendenti
    Public Id As Long
    Public Utente As String
    Public DataAggiornamento As Date
    Public CodiceDipendente As Integer
    Public Data As Date
    Public Orario As Date
    Public Gruppo As String
    Public Tipo As String

    Public Sub Pulisci()
        Id = 0
        Utente = ""
        DataAggiornamento = Nothing
        CodiceDipendente = 0
        Data = Nothing
        Orario = Nothing
        Gruppo = ""
        Tipo = ""
    End Sub
End Class
