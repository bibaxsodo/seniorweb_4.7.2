Imports Microsoft.VisualBasic
Imports System.Data.OleDb
Imports System.Web.Hosting

Public Class Cls_Qualifica
    Public Id As Long
    Public Utente As String
    Public DataAggiornamento As Date
    Public Codice As String
    Public Descrizione As String
    Public TipoDipendente As String
    Public StampaSituazioneStraordinari As String
    Public ControlloVariazioniTurno As String

    Public Sub Pulisci()
        Id = 0
        Utente = ""
        DataAggiornamento = Nothing
        Codice = ""
        Descrizione = ""
        TipoDipendente = ""
        StampaSituazioneStraordinari = ""
        ControlloVariazioniTurno = ""
    End Sub

    Public Sub Elimina(ByVal StringaConnessione As String, ByVal xCodice As String)
        Dim cn As OleDbConnection
        Dim MySql As String

        MySql = ""

        cn = New Data.OleDb.OleDbConnection(StringaConnessione)

        cn.Open()
        Dim cmd As New OleDbCommand()
        cmd.CommandText = ("DELETE FROM Qualifica where Codice = ?")
        cmd.Parameters.AddWithValue("@Codice", xCodice)
        cmd.Connection = cn
        cmd.ExecuteNonQuery()
        cn.Close()
    End Sub

    Sub Aggiorna(ByVal StringaConnessione As String)
        Dim cn As OleDbConnection
        Dim MySql As String

        MySql = ""

        cn = New Data.OleDb.OleDbConnection(StringaConnessione)

        cn.Open()
        Dim cmd As New OleDbCommand()
        cmd.CommandText = "SELECT * FROM Qualifica WHERE Codice = ?"
        cmd.Parameters.AddWithValue("@Codice", Codice)
        cmd.Connection = cn
        Dim myPOSTreader As OleDbDataReader = cmd.ExecuteReader()
        If myPOSTreader.Read Then
            MySql = "UPDATE Qualifica SET " & _
                    " Utente = ?," & _
                    " DataAggiornamento  = ?," & _
                    " Descrizione = ?," & _
                    " TipoDipendente  = ?," & _
                    " StampaSituazioneStraordinari  = ?," & _
                    " ControlloVariazioniTurno  = ?," & _
                    " WHERE Codice = ?"

            cmd.CommandText = MySql
            cmd.Parameters.AddWithValue("@Utente", Utente)
            cmd.Parameters.AddWithValue("@DataAggiornamento ", Now)
            cmd.Parameters.AddWithValue("@Descrizione", Descrizione)
            cmd.Parameters.AddWithValue("@TipoDipendente", TipoDipendente)
            cmd.Parameters.AddWithValue("@StampaSituazioneStraordinari", StampaSituazioneStraordinari)
            cmd.Parameters.AddWithValue("@ControlloVariazioniTurno", ControlloVariazioniTurno)
            cmd.Parameters.AddWithValue("@Codice", Codice)
            cmd.ExecuteNonQuery()
        Else
            MySql = "INSERT INTO Qualifica (Utente, DataAggiornamento, Codice, Descrizione, TipoDipendente, StampaSituazioneStraordinari, ControlloVariazioniTurno) VALUES (?,?,?,?,?,?,?)"

            Dim cmdw As New OleDbCommand()
            cmdw.CommandText = MySql
            cmdw.Parameters.AddWithValue("@Utente", Utente)
            cmdw.Parameters.AddWithValue("@DataAggiornamento", Now)
            cmdw.Parameters.AddWithValue("@Codice", Codice)
            cmdw.Parameters.AddWithValue("@Descrizione", Descrizione)
            cmdw.Parameters.AddWithValue("@TipoDipendente", TipoDipendente)
            cmdw.Parameters.AddWithValue("@StampaSituazioneStraordinari", StampaSituazioneStraordinari)
            cmdw.Parameters.AddWithValue("@ControlloVariazioniTurno", ControlloVariazioniTurno)
            cmdw.Connection = cn
            cmdw.ExecuteNonQuery()
        End If
        myPOSTreader.Close()
        cn.Close()

        Call Pulisci()
    End Sub

    Sub LoadDati(ByVal StringaConnessione As String, ByVal Tabella As System.Data.DataTable)
        Dim cn As OleDbConnection

        cn = New Data.OleDb.OleDbConnection(StringaConnessione)

        cn.Open()
        Dim cmd As New OleDbCommand()

        cmd.CommandText = ("Select * From Qualifica Order By Descrizione")

        cmd.Connection = cn


        Tabella.Clear()
        Tabella.Columns.Add("Codice", GetType(String))
        Tabella.Columns.Add("Descrizione", GetType(String))
        Tabella.Columns.Add("TipoDipendente", GetType(String))
        Tabella.Columns.Add("StampaSituazioneStraordinari", GetType(String))
        Tabella.Columns.Add("ControlloVariazioniTurno", GetType(String))

        Dim myPOSTreader As OleDbDataReader = cmd.ExecuteReader()
        Do While myPOSTreader.Read

            Dim myriga As System.Data.DataRow = Tabella.NewRow()
            myriga(0) = myPOSTreader.Item("Codice")
            myriga(1) = myPOSTreader.Item("Descrizione")
            myriga(2) = myPOSTreader.Item("TipoDipendente")
            myriga(3) = myPOSTreader.Item("StampaSituazioneStraordinari")
            myriga(4) = myPOSTreader.Item("ControlloVariazioniTurno")

            Tabella.Rows.Add(myriga)
        Loop
        myPOSTreader.Close()
        cn.Close()

    End Sub

    Public Function Decodifica(ByVal StringaConnessione As String, ByVal xCodice As String) As String
        Dim cn As OleDbConnection

        cn = New Data.OleDb.OleDbConnection(StringaConnessione)

        cn.Open()
        Dim cmd As New OleDbCommand()
        cmd.CommandText = "SELECT Descrizione FROM Qualifica" & _
                          " WHERE Codice = ?"
        cmd.Parameters.AddWithValue("@Codice", xCodice)
        cmd.Connection = cn
        Dim myPOSTreader As OleDbDataReader = cmd.ExecuteReader()
        If myPOSTreader.Read Then
            Decodifica = StringaDb(myPOSTreader.Item(0)) & " (" & StringaDb(myPOSTreader.Item(1)) & ")"
        Else
            Decodifica = ""
        End If
        myPOSTreader.Close()
        cn.Close()
    End Function

    Function StringaDb(ByVal Oggetto As Object) As String
        If IsDBNull(Oggetto) Then
            Return ""
        Else
            Return Oggetto
        End If
    End Function

    Function NumeroDb(ByVal Oggetto As Object) As Object
        If IsDBNull(Oggetto) Then
            Return 0
        Else
            Return Oggetto
        End If
    End Function

    Sub Leggi(ByVal StringaConnessione As String, ByVal xCodice As String)
        Dim cn As OleDbConnection

        cn = New Data.OleDb.OleDbConnection(StringaConnessione)

        cn.Open()
        Dim cmd As New OleDbCommand()
        cmd.CommandText = ("Select * From Qualifica Where " & _
                           "Codice = ?")
        cmd.Parameters.AddWithValue("@Codice", xCodice)
        cmd.Connection = cn

        Dim myPOSTreader As OleDbDataReader = cmd.ExecuteReader()
        If myPOSTreader.Read Then
            Id = NumeroDb(myPOSTreader.Item("Id"))
            Codice = StringaDb(myPOSTreader.Item("Codice"))
            Descrizione = StringaDb(myPOSTreader.Item("Descrizione"))
            TipoDipendente = StringaDb(myPOSTreader.Item("TipoDipendente"))
            StampaSituazioneStraordinari = StringaDb(myPOSTreader.Item("StampaSituazioneStraordinari"))
            ControlloVariazioniTurno = StringaDb(myPOSTreader.Item("ControlloVariazioniTurno"))
        Else
            Call Pulisci()
        End If
        cn.Close()
    End Sub

    Function TestUsato(ByVal StringaConnessione As String, ByVal xCodice As String) As Boolean
        Dim cn As OleDbConnection
        cn = New Data.OleDb.OleDbConnection(StringaConnessione)
        cn.Open()

        Dim p As New Cls_ParametriTurni

        p.Leggi(StringaConnessione)

        Dim cmd As New OleDbCommand()
        cmd.CommandText = "SELECT Count(*) FROM DatiVariabili" & _
                          " WHERE CodiceVariabile = ?" & _
                          " AND ContenutoTesto = ?"

        cmd.Parameters.AddWithValue("@CodiceVariabile", p.CodiceAnagraficoQualifica)
        cmd.Parameters.AddWithValue("@ContenutoTesto", xCodice)

        cmd.Connection = cn
        Dim myPOSTreader As OleDbDataReader = cmd.ExecuteReader()
        If myPOSTreader.Read Then
            If myPOSTreader.Item(0) = 0 Then
                Return False
            Else
                Return True
                myPOSTreader.Close()
                cn.Close()
                Exit Function
            End If
        Else
            Return False
        End If
        myPOSTreader.Close()

        cmd.CommandText = "SELECT Count(*) FROM RegoleSostituzione" & _
                          " WHERE Codice = ?"
        cmd.Parameters.Clear()
        cmd.Parameters.AddWithValue("@Codice", xCodice)

        myPOSTreader = cmd.ExecuteReader()
        If myPOSTreader.Read Then
            If myPOSTreader.Item(0) = 0 Then
                Return False
            Else
                Return True
                myPOSTreader.Close()
                cn.Close()
                Exit Function
            End If
        Else
            Return False
        End If
        myPOSTreader.Close()

        cn.Close()
    End Function

    Sub UpDateDropBox(ByVal StringaConnessione As String, ByRef Appoggio As DropDownList)
        Dim cn As OleDbConnection

        cn = New Data.OleDb.OleDbConnection(StringaConnessione)

        cn.Open()
        Dim cmd As New OleDbCommand()
        cmd.CommandText = ("SELECT Codice, Descrizione FROM Qualifica ORDER BY Descrizione")
        cmd.Connection = cn

        Appoggio.Items.Clear()
        Dim myPOSTreader As OleDbDataReader = cmd.ExecuteReader()
        Do While myPOSTreader.Read
            Appoggio.Items.Add(myPOSTreader.Item("Descrizione"))
            Appoggio.Items(Appoggio.Items.Count - 1).Value = myPOSTreader.Item("Codice")
        Loop
        myPOSTreader.Close()
        cn.Close()
        Appoggio.Items.Add("")
        Appoggio.Items(Appoggio.Items.Count - 1).Value = ""
        Appoggio.Items(Appoggio.Items.Count - 1).Selected = True

    End Sub
End Class
