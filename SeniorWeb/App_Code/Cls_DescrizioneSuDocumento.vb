﻿Imports System.Data.OleDb


Public Class Cls_DescrizioneSuDocumento
    Public Mese As Integer
    Public Anno As Integer

    Public Sub ANFFASCarrara(ByVal Documento As Cls_MovimentoContabile, ByVal ConnessioneOspite As String, ByVal ConnessioneTabelle As String, ByVal ConnessioneGenerale As String)
        If Mid(Documento.Tipologia & Space(10), 1, 1) = "R" Then

            Dim Regione As String
            Dim TabUSl As New ClsUSL

            Regione = Mid(Documento.Tipologia & Space(10), 2, 4).Trim

            TabUSl.CodiceRegione = Regione
            TabUSl.Leggi(ConnessioneOspite)
            If TabUSl.RaggruppaInElaborazione = 0 Then
                For i = 0 To Documento.Righe.Length - 1
                    If Not IsNothing(Documento.Righe(i)) Then
                        If Documento.Righe(i).RigaDaCausale >= 3 Then
                            Dim Pdc As New Cls_Pianodeiconti

                            Pdc.Mastro = Documento.Righe(i).MastroPartita
                            Pdc.Conto = Documento.Righe(i).ContoPartita
                            Pdc.Sottoconto = Documento.Righe(i).SottocontoPartita
                            Pdc.Decodfica(ConnessioneGenerale)


                            Dim PdcCP As New Cls_Pianodeiconti

                            PdcCP.Mastro = Documento.Righe(i).MastroContropartita
                            PdcCP.Conto = Documento.Righe(i).ContoContropartita
                            PdcCP.Sottoconto = Documento.Righe(i).SottocontoContropartita
                            PdcCP.Decodfica(ConnessioneGenerale)

                            Documento.Righe(i).Descrizione = Pdc.Descrizione & " " & Documento.Righe(i).Descrizione & " " & PdcCP.Descrizione
                        End If
                    End If
                Next

                Documento.Scrivi(ConnessioneGenerale, Documento.NumeroRegistrazione, True)
            Else
                For i = 0 To Documento.Righe.Length - 1
                    If Not IsNothing(Documento.Righe(i)) Then
                        If Documento.Righe(i).RigaDaCausale >= 3 Then

                            Dim PdcCP As New Cls_Pianodeiconti

                            PdcCP.Mastro = Documento.Righe(i).MastroPartita
                            PdcCP.Conto = Documento.Righe(i).ContoPartita
                            PdcCP.Sottoconto = Documento.Righe(i).SottocontoPartita
                            PdcCP.Decodfica(ConnessioneGenerale)

                            Documento.Righe(i).Descrizione = PdcCP.Descrizione & " " & Documento.Righe(i).Descrizione
                        End If
                    End If
                Next

                Documento.Scrivi(ConnessioneGenerale, Documento.NumeroRegistrazione, True)
            End If
        End If
        If Mid(Documento.Tipologia & Space(10), 1, 1) = "C" Then
            Dim Provincia As String
            Dim Comune As String
            Dim TabComune As New ClsComune

            Provincia = Mid(Documento.Tipologia & Space(10), 2, 3).Trim
            Comune = Mid(Documento.Tipologia & Space(10), 5, 3).Trim

            TabComune.Provincia = Provincia
            TabComune.Comune = Comune
            TabComune.Leggi(ConnessioneOspite)


            If TabComune.RaggruppaInElaborazione = 0 Then
                For i = 0 To Documento.Righe.Length - 1
                    If Not IsNothing(Documento.Righe(i)) Then
                        If Documento.Righe(i).RigaDaCausale = 3 Then

                            Dim PdcCP As New Cls_Pianodeiconti

                            PdcCP.Mastro = Documento.Righe(i).MastroContropartita
                            PdcCP.Conto = Documento.Righe(i).ContoContropartita
                            PdcCP.Sottoconto = Documento.Righe(i).SottocontoContropartita
                            PdcCP.Decodfica(ConnessioneGenerale)

                            Documento.Righe(i).Descrizione = Documento.Righe(i).Descrizione & " " & PdcCP.Descrizione
                        End If
                    End If
                    If Not IsNothing(Documento.Righe(i)) Then
                        If Documento.Righe(i).RigaDaCausale > 3 Then

                            Dim PdcCP As New Cls_Pianodeiconti

                            PdcCP.Mastro = Documento.Righe(i).MastroPartita
                            PdcCP.Conto = Documento.Righe(i).ContoPartita
                            PdcCP.Sottoconto = Documento.Righe(i).SottocontoPartita
                            PdcCP.Decodfica(ConnessioneGenerale)

                            Documento.Righe(i).Descrizione = PdcCP.Descrizione
                            If PdcCP.Descrizione.IndexOf("Quota com") >= 0 Then
                                PdcCP.Mastro = Documento.Righe(i).MastroContropartita
                                PdcCP.Conto = Documento.Righe(i).ContoContropartita
                                PdcCP.Sottoconto = Documento.Righe(i).SottocontoContropartita
                                PdcCP.Decodfica(ConnessioneGenerale)

                                Documento.Righe(i).Descrizione = Documento.Righe(i).Descrizione & " " & PdcCP.Descrizione
                            End If


                        End If
                    End If
                Next

                Documento.Scrivi(ConnessioneGenerale, Documento.NumeroRegistrazione, True)

            Else
                For i = 0 To Documento.Righe.Length - 1
                    If Not IsNothing(Documento.Righe(i)) Then
                        If Documento.Righe(i).RigaDaCausale = 3 Then

                            Dim PdcCP As New Cls_Pianodeiconti

                            PdcCP.Mastro = Documento.Righe(i).MastroPartita
                            PdcCP.Conto = Documento.Righe(i).ContoPartita
                            PdcCP.Sottoconto = Documento.Righe(i).SottocontoPartita
                            PdcCP.Decodfica(ConnessioneGenerale)

                            Documento.Righe(i).Descrizione = Documento.Righe(i).Descrizione
                        End If
                    End If
                    If Not IsNothing(Documento.Righe(i)) Then
                        If Documento.Righe(i).RigaDaCausale > 3 Then

                            Dim PdcCP As New Cls_Pianodeiconti

                            PdcCP.Mastro = Documento.Righe(i).MastroPartita
                            PdcCP.Conto = Documento.Righe(i).ContoPartita
                            PdcCP.Sottoconto = Documento.Righe(i).SottocontoPartita
                            PdcCP.Decodfica(ConnessioneGenerale)

                            Documento.Righe(i).Descrizione = PdcCP.Descrizione
                            If PdcCP.Descrizione.IndexOf("Quota com") >= 0 Then
                                PdcCP.Mastro = Documento.Righe(i).MastroContropartita
                                PdcCP.Conto = Documento.Righe(i).ContoContropartita
                                PdcCP.Sottoconto = Documento.Righe(i).SottocontoContropartita
                                PdcCP.Decodfica(ConnessioneGenerale)

                                Documento.Righe(i).Descrizione = Documento.Righe(i).Descrizione & " " & PdcCP.Descrizione
                            End If
                        End If
                    End If
                Next

                Documento.Scrivi(ConnessioneGenerale, Documento.NumeroRegistrazione, True)


            End If
        End If

        If Documento.Tipologia.Trim = "" Then
            Dim DecodificaMese As New Cls_StampaFatture

            For i = 0 To Documento.Righe.Length - 1
                If Not IsNothing(Documento.Righe(i)) Then
                    If Documento.Righe(i).RigaDaCausale = 3 Then

                        Dim PdcCP As New Cls_Pianodeiconti

                        PdcCP.Mastro = Documento.Righe(i).MastroPartita
                        PdcCP.Conto = Documento.Righe(i).ContoPartita
                        PdcCP.Sottoconto = Documento.Righe(i).SottocontoPartita
                        PdcCP.Decodfica(ConnessioneGenerale)

                        Documento.Righe(i).Descrizione = "Quota compartecipazione su prestazioni internato mese " & DecodificaMese.DecodificaMese(Documento.MeseCompetenza) & "/" & Documento.AnnoCompetenza
                    End If
                End If
            Next

            Documento.Scrivi(ConnessioneGenerale, Documento.NumeroRegistrazione, True)        
        End If

        'Quota compartecipazione su prestazioni a internato mese
        ' su ospiti e parenti : Quota compartecipazione su prestazioni a internato mese : competenza


    End Sub

    Public Function DecodificaMese(ByVal Mese As Long) As String
        Select Case Mese
            Case 1
                DecodificaMese = "Gennaio"
            Case 2
                DecodificaMese = "Febbraio"
            Case 3
                DecodificaMese = "Marzo"
            Case 4
                DecodificaMese = "Aprile"
            Case 5
                DecodificaMese = "Maggio"
            Case 6
                DecodificaMese = "Giugno"
            Case 7
                DecodificaMese = "Luglio"
            Case 8
                DecodificaMese = "Agosto"
            Case 9
                DecodificaMese = "Settembre"
            Case 10
                DecodificaMese = "Ottobre"
            Case 11
                DecodificaMese = "Novembre"
            Case 12
                DecodificaMese = "Dicembre"
        End Select
    End Function

    Public Sub IlGabbiano(ByVal Documento As Cls_MovimentoContabile, ByVal ConnessioneOspite As String, ByVal ConnessioneTabelle As String, ByVal ConnessioneGenerale As String)
        Dim i as Integer
        
        For i = 0 To Documento.Righe.Length - 1
            If Not IsNothing(Documento.Righe(i)) Then
                If Documento.Righe(i).RigaDaCausale <> 13 and Documento.Righe(i).RigaDaCausale <> 3 Then
                    If Documento.Righe(i).RigaDaCausale = 4 Or Documento.Righe(i).RigaDaCausale = 6 Then
                        If Documento.Righe(i).TipoExtra = "A01" Then
                            Documento.Righe(i).Descrizione =  "Conguaglio mese " & DecodificaMese(Documento.Righe(i).MeseRiferimento) & "/" & Documento.Righe(i).AnnoRiferimento
                        End If
                    End If
                Else
                    If Documento.Tipologia.Trim = "" Then
                            Dim PdcCP As New Cls_Pianodeiconti

                            PdcCP.Mastro = Documento.Righe(i).MastroPartita
                            PdcCP.Conto = Documento.Righe(i).ContoPartita
                            PdcCP.Sottoconto = Documento.Righe(i).SottocontoPartita
                            PdcCP.Decodfica(ConnessioneGenerale)
                            
                            If Documento.CentroServizio ="CD" then
                            If Documento.Righe(i).Descrizione.IndexOf("Assenze") >= 0 Then
                                Documento.Righe(i).Descrizione = PdcCP.Commento & " non fruite"                                
                            Else
                                Documento.Righe(i).Descrizione = PdcCP.Commento & " programmate"
                            End If
                            Else
                                Documento.Righe(i).Descrizione = PdcCP.Commento
                            End if
                    End If

                    If len(Documento.Tipologia.Trim)> 1 Then
                        If mid(Documento.Tipologia.Trim,1,1) = "R" Then
                            Dim Partita As New Cls_Pianodeiconti

                            Partita.Mastro = Documento.Righe(i).MastroPartita
                            Partita.Conto = Documento.Righe(i).ContoPartita
                            Partita.Sottoconto = Documento.Righe(i).SottocontoPartita
                            Partita.Decodfica(ConnessioneGenerale)

                            Dim ControPartita As New Cls_Pianodeiconti

                            ControPartita.Mastro = Documento.Righe(i).MastroContropartita
                            ControPartita.Conto = Documento.Righe(i).ContoContropartita
                            ControPartita.Sottoconto = Documento.Righe(i).SottocontoContropartita
                            ControPartita.Decodfica(ConnessioneGenerale)

                            Dim Personalizzazione As Boolean =False
                            
                            

                           
                            If Documento.CentroServizio ="CD" then
                                Personalizzazione=True
                                If Documento.Righe(i).Descrizione.IndexOf("Presenze") >=0 Then
                                    Documento.Righe(i).Descrizione = "QUOTA SANITARIA DELLA RETTA GIORNALIERA PER LE GIORNATE PROGRAMMATE"
                                Else
                                    Documento.Righe(i).Descrizione = "QUOTA SANITARIA DELLA RETTA GIORNALIERA PER LE GIORNATE PROGRAMMATE E NON FRUITE"
                                End If
                            End If
                            If Documento.CentroServizio ="CDRM" then
                                Dim StatoAuto as new Cls_StatoAuto
                                    
                                StatoAuto.TipoRetta=""
                                StatoAuto.CODICEOSPITE = Int(Documento.Righe(i).SottocontoContropartita / 100)
                                StatoAuto.CENTROSERVIZIO =  Documento.CentroServizio
                                StatoAuto.UltimaData(ConnessioneOspite,StatoAuto.CODICEOSPITE,StatoAuto.CENTROSERVIZIO)
                           
                                 IF StatoAuto.TipoRetta <>"" THEN
                                    Dim Dectipo as new Cls_TabellaTipoImportoRegione

                                    Dectipo.Codice= StatoAuto.TipoRetta
                                    Dectipo.Leggi(ConnessioneOspite,Dectipo.Codice)

                                    Documento.Righe(i).Descrizione ="RETTA FASCIA " & Dectipo.Descrizione 
                                else
                                    Documento.Righe(i).Descrizione ="RETTA FASCIA " 
                                End If                                                        
                                Personalizzazione=True
                            End If

                            If Documento.CentroServizio ="RIB" then
                                Dim StatoAuto as new Cls_StatoAuto
                                    
                                StatoAuto.TipoRetta=""
                                StatoAuto.CODICEOSPITE = Int(Documento.Righe(i).SottocontoContropartita / 100)
                                StatoAuto.CENTROSERVIZIO =  Documento.CentroServizio
                                StatoAuto.UltimaData(ConnessioneOspite,StatoAuto.CODICEOSPITE,StatoAuto.CENTROSERVIZIO)
                           
                                 IF StatoAuto.TipoRetta <>"" THEN
                                    Dim Dectipo as new Cls_TabellaTipoImportoRegione

                                    Dectipo.Codice= StatoAuto.TipoRetta
                                    Dectipo.Leggi(ConnessioneOspite,Dectipo.Codice)

                                    Documento.Righe(i).Descrizione ="RETTA " & Dectipo.Descrizione & "  PER GIORNATE DI PRESENZA ALLA CASA DI RIPOSO PIO ISTITUTO BRIZIO DI SALE SALVO CONGUAGLIO"
                                else
                                    Documento.Righe(i).Descrizione ="RETTA " & "  PER GIORNATE DI PRESENZA ALLA CASA DI RIPOSO PIO ISTITUTO BRIZIO DI SALE SALVO CONGUAGLIO"
                                End If                                                        
                                Personalizzazione=True
                            End If

                            If Documento.CentroServizio = "FRU" Then
                                Documento.Righe(i).Descrizione = "QUOTA RETTA SANITARIA RETTA ALLA RESIDENZA PROTETTA FRUGONE DI BUSALLA"
                                If Documento.Tipologia = "R2" Then
                                    Documento.Righe(i).Descrizione = "QUOTA RETTA SANITARIA RETTA REGIME NAP ALLA RESIDENZA PROTETTA FRUGONE DI BUSALLA"
                                End If
                                If Documento.Tipologia = "R1" Then
                                    Documento.Righe(i).Descrizione = "QUOTA RETTA SANITARIA RETTA REGIME NAT ALLA RESIDENZA PROTETTA FRUGONE DI BUSALLA"
                                End If
                            End If

                            If Not Personalizzazione Then
                                Documento.Righe(i).Descrizione = Partita.Descrizione & " " & ControPartita.Descrizione & " " & Documento.Righe(i).Descrizione
                            End If
                        End If
                        If Mid(Documento.Tipologia.Trim, 1, 1) = "C" Then
                            Dim StatoAuto As New Cls_StatoAuto

                            StatoAuto.TipoRetta = ""
                            StatoAuto.CODICEOSPITE = Int(Documento.Righe(i).SottocontoContropartita / 100)
                            StatoAuto.CENTROSERVIZIO = Documento.CentroServizio
                            StatoAuto.UltimaData(ConnessioneOspite, StatoAuto.CODICEOSPITE, StatoAuto.CENTROSERVIZIO)

                            Dim ControPartita As New Cls_Pianodeiconti

                            ControPartita.Mastro = Documento.Righe(i).MastroContropartita
                            ControPartita.Conto = Documento.Righe(i).ContoContropartita
                            ControPartita.Sottoconto = Documento.Righe(i).SottocontoContropartita
                            ControPartita.Decodfica(ConnessioneGenerale)

                            Dim Personalizzazione As Boolean = False

                            If Documento.CentroServizio = "CDRM" Then
                                If StatoAuto.TipoRetta <> "" Then
                                    Dim Dectipo As New Cls_TabellaTipoImportoRegione

                                    Dectipo.Codice = StatoAuto.TipoRetta
                                    Dectipo.Leggi(ConnessioneOspite, Dectipo.Codice)

                                    Documento.Righe(i).Descrizione = "INTEGRAZIONE RETTA " & Dectipo.Descrizione & " " & ControPartita.Descrizione & " PRESSO RESIDENZA DE MARTINI -LU- SALVO CONGUAGLIO"
                                Else
                                    Documento.Righe(i).Descrizione = "INTEGRAZIONE RETTA " & ControPartita.Descrizione & " PRESSO RESIDENZA DE MARTINI -LU- SALVO CONGUAGLIO"
                                End If
                                Personalizzazione = True
                            End If

                            If Not Personalizzazione Then
                                If StatoAuto.TipoRetta <> "" Then
                                    Dim Dectipo As New Cls_TabellaTipoImportoRegione

                                    Dectipo.Codice = StatoAuto.TipoRetta
                                    Dectipo.Leggi(ConnessioneOspite, Dectipo.Codice)

                                    Documento.Righe(i).Descrizione = "INTEGRAZIONE RETTA " & Dectipo.Descrizione & " " & ControPartita.Descrizione
                                Else
                                    Documento.Righe(i).Descrizione = "INTEGRAZIONE RETTA " & ControPartita.Descrizione
                                End If
                            End If

                        End If
                    End If
                    End If
       
            End If
        Next

        Documento.Scrivi(ConnessioneGenerale, Documento.NumeroRegistrazione, True)

    end Sub
    Public Sub Libera(ByVal Documento As Cls_MovimentoContabile, ByVal ConnessioneOspite As String, ByVal ConnessioneTabelle As String, ByVal ConnessioneGenerale As String)
        Dim CodiceOspite As Integer = 0

        If Documento.Tipologia.Trim = "" Then
            For i = 0 To Documento.Righe.Length - 1
                If Not IsNothing(Documento.Righe(i)) Then


                    If Documento.Righe(i).RigaDaCausale = 1 Then

                        CodiceOspite = Int(Documento.Righe(i).SottocontoPartita / 100)
                    End If

                    If Documento.Righe(i).RigaDaCausale = 3 Then
                        Dim Ospite As New ClsOspite

                        Ospite.CodiceOspite = CodiceOspite
                        Ospite.Leggi(ConnessioneOspite, Ospite.CodiceOspite)

                        Documento.Righe(i).Descrizione = Ospite.Nome
                    End If
                End If
            Next

        Else
            For i = 0 To Documento.Righe.Length - 1
                If Not IsNothing(Documento.Righe(i)) Then
                    If Documento.Righe(i).RigaDaCausale = 3 Then
                        Dim PDC As New Cls_Pianodeiconti

                        PDC.Mastro = Documento.Righe(i).MastroPartita
                        PDC.Conto = Documento.Righe(i).ContoPartita
                        PDC.Sottoconto = Documento.Righe(i).SottocontoPartita
                        PDC.Decodfica(ConnessioneGenerale)

                        Documento.Righe(i).Descrizione = PDC.Descrizione & " " & Int(Documento.Righe(i).SottocontoContropartita / 100)
                    End If

                End If
            Next

        End If

        Documento.Scrivi(ConnessioneGenerale, Documento.NumeroRegistrazione, True)
    End Sub


    ''
    Public Sub DonMoschetta(ByVal Documento As Cls_MovimentoContabile, ByVal ConnessioneOspite As String, ByVal ConnessioneTabelle As String, ByVal ConnessioneGenerale As String)
        Dim CodiceOspite As Integer = 0

        If Documento.Tipologia.Trim = "" Then
            For i = 0 To Documento.Righe.Length - 1
                If Not IsNothing(Documento.Righe(i)) Then


                    If Documento.Righe(i).RigaDaCausale = 1 Then

                        CodiceOspite = Int(Documento.Righe(i).SottocontoPartita / 100)
                    End If

                    If Documento.Righe(i).RigaDaCausale = 3 Then
                        Dim Personalizzaione As Boolean = False


                        If Documento.Righe(i).MastroPartita = 58 And Documento.Righe(i).ContoPartita = 20 And Documento.Righe(i).SottocontoPartita = 25 Then
                            Documento.Righe(i).Descrizione = "Retta Camera Doppia Privati " & Documento.Righe(i).Descrizione
                            Personalizzaione = True
                        End If
                        If Documento.Righe(i).MastroPartita = 58 And Documento.Righe(i).ContoPartita = 20 And Documento.Righe(i).SottocontoPartita = 20 Then
                            Documento.Righe(i).Descrizione = "Retta Ospiti Centro Diurno" & Documento.Righe(i).Descrizione
                            Personalizzaione = True
                        End If

                        If Not Personalizzaione Then
                            Documento.Righe(i).Descrizione = "Retta Camera Doppia N/A Corrente  " & Documento.Righe(i).Descrizione
                        End If

                    End If
                    If Documento.Righe(i).RigaDaCausale = 4 Then

                        Documento.Righe(i).Descrizione = Documento.Righe(i).Descrizione
                    End If
                    If Documento.Righe(i).RigaDaCausale = 6 And Documento.Righe(i).TipoExtra = "A01" And Documento.CentroServizio = "RSA" Then
                        Dim MovimUscita As New Cls_Movimenti

                        MovimUscita.CENTROSERVIZIO = Documento.CentroServizio
                        MovimUscita.CodiceOspite = CodiceOspite
                        MovimUscita.UltimaDataUscita(ConnessioneOspite, MovimUscita.CodiceOspite, MovimUscita.CENTROSERVIZIO, DateSerial(Documento.Righe(i).AnnoRiferimento, Documento.Righe(i).MeseRiferimento, GiorniMese(Documento.Righe(i).MeseRiferimento, Documento.Righe(i).AnnoRiferimento)))

                        Dim MovimIngressoOUD As New Cls_Movimenti

                        MovimIngressoOUD.Data = Nothing
                        MovimIngressoOUD.CENTROSERVIZIO = Documento.CentroServizio
                        MovimIngressoOUD.CodiceOspite = CodiceOspite
                        MovimIngressoOUD.UltimaMovimentoPrimaData(ConnessioneOspite, MovimIngressoOUD.CodiceOspite, MovimIngressoOUD.CENTROSERVIZIO, DateSerial(Documento.Righe(i).AnnoRiferimento, Documento.Righe(i).MeseRiferimento, GiorniMese(Documento.Righe(i).MeseRiferimento, Documento.Righe(i).AnnoRiferimento)))

                        If MovimUscita.TipoMov = "03" Then
                            If IsNothing(MovimIngressoOUD.Data) Or MovimIngressoOUD.TipoMov = "03" Then
                                Documento.Righe(i).Quantita = DateDiff(DateInterval.Day, MovimUscita.Data, DateSerial(Documento.Righe(i).AnnoRiferimento, Documento.Righe(i).MeseRiferimento, GiorniMese(Documento.Righe(i).MeseRiferimento, Documento.Righe(i).AnnoRiferimento))) - 1
                                If Math.Round(Documento.Righe(i).Importo / 25.5, 0) = Math.Round(Documento.Righe(i).Importo / 25.5, 4) Then
                                    Documento.Righe(i).Quantita = Math.Round(Documento.Righe(i).Importo / 25.5, 0)
                                End If
                                Documento.Righe(i).Descrizione = "Ricovero ospedaliero dal  " & Format(MovimUscita.Data, "dd/MM/yyyy")
                            Else
                                If Month(MovimIngressoOUD.Data) = Documento.Righe(i).MeseRiferimento And Year(MovimIngressoOUD.Data) = Documento.Righe(i).AnnoRiferimento Then
                                    Documento.Righe(i).Quantita = DateDiff(DateInterval.Day, MovimUscita.Data, MovimIngressoOUD.Data) - 1
                                    If Math.Round(Documento.Righe(i).Importo / 25.5, 0) = Math.Round(Documento.Righe(i).Importo / 25.5, 4) Then
                                        Documento.Righe(i).Quantita = Math.Round(Documento.Righe(i).Importo / 25.5, 0)
                                    End If
                                    Documento.Righe(i).Descrizione = "Ricovero ospedaliero dal  " & Format(MovimUscita.Data, "dd/MM/yyyy") & " al " & Format(MovimIngressoOUD.Data, "dd/MM/yyyy")
                                End If
                                Dim MesePrecente As Integer
                                Dim AnnoMesePrecedente As Integer
                                If Documento.Righe(i).MeseRiferimento = 1 Then
                                    MesePrecente = 12
                                    AnnoMesePrecedente = Documento.Righe(i).AnnoRiferimento - 1
                                Else
                                    MesePrecente = Documento.Righe(i).MeseRiferimento - 1
                                    AnnoMesePrecedente = Documento.Righe(i).AnnoRiferimento - 1
                                End If

                                If (Month(MovimIngressoOUD.Data) <> Documento.Righe(i).MeseRiferimento Or Year(MovimIngressoOUD.Data) <> Documento.Righe(i).AnnoRiferimento) Then
                                    If IsDate(MovimUscita.Data) Then
                                        If Year(MovimUscita.Data) > 1900 Then
                                            Documento.Righe(i).Quantita = DateDiff(DateInterval.Day, MovimUscita.Data, DateSerial(Documento.Righe(i).AnnoRiferimento, Documento.Righe(i).MeseRiferimento, GiorniMese(Documento.Righe(i).MeseRiferimento, Documento.Righe(i).AnnoRiferimento))) - 1
                                        End If
                                    End If
                                    If Math.Round(Documento.Righe(i).Importo / 25.5, 0) = Math.Round(Documento.Righe(i).Importo / 25.5, 4) Then
                                        Documento.Righe(i).Quantita = Math.Round(Documento.Righe(i).Importo / 25.5, 0)
                                    End If
                                    Documento.Righe(i).Descrizione = "Ricovero ospedaliero dal  " & Format(MovimUscita.Data, "dd/MM/yyyy")
                                End If
                            End If
                        Else
                            Documento.Righe(i).Descrizione = "Conguaglio mese precedente"
                        End If

                    End If
                End If
            Next
        Else
            For i = 0 To Documento.Righe.Length - 1
                If Not IsNothing(Documento.Righe(i)) Then
                    If Documento.Righe(i).RigaDaCausale = 3 Then
                        Dim PdcCP As New Cls_Pianodeiconti

                        PdcCP.Mastro = Documento.Righe(i).MastroPartita
                        PdcCP.Conto = Documento.Righe(i).ContoPartita
                        PdcCP.Sottoconto = Documento.Righe(i).SottocontoPartita
                        PdcCP.Decodfica(ConnessioneGenerale)

                        Dim PdcCPControPartita As New Cls_Pianodeiconti

                        PdcCPControPartita.Mastro = Documento.Righe(i).MastroContropartita
                        PdcCPControPartita.Conto = Documento.Righe(i).ContoContropartita
                        PdcCPControPartita.Sottoconto = Documento.Righe(i).SottocontoContropartita
                        PdcCPControPartita.Decodfica(ConnessioneGenerale)


                        'Documento.Righe(i).Descrizione = PdcCPControPartita.Descrizione & " " & Documento.Righe(i).Descrizione
                        Documento.Righe(i).Descrizione = PdcCPControPartita.Descrizione & " Quota Regionale N/A"
                    End If
                    If Documento.Righe(i).RigaDaCausale = 4 Or Documento.Righe(i).RigaDaCausale = 5 Then
                        Dim PdcCP As New Cls_Pianodeiconti

                        PdcCP.Mastro = Documento.Righe(i).MastroPartita
                        PdcCP.Conto = Documento.Righe(i).ContoPartita
                        PdcCP.Sottoconto = Documento.Righe(i).SottocontoPartita
                        PdcCP.Decodfica(ConnessioneGenerale)

                        Dim PdcCPControPartita As New Cls_Pianodeiconti

                        PdcCPControPartita.Mastro = Documento.Righe(i).MastroContropartita
                        PdcCPControPartita.Conto = Documento.Righe(i).ContoContropartita
                        PdcCPControPartita.Sottoconto = Documento.Righe(i).SottocontoContropartita
                        PdcCPControPartita.Decodfica(ConnessioneGenerale)


                        'Documento.Righe(i).Descrizione = PdcCPControPartita.Descrizione & " " & Documento.Righe(i).Descrizione
                        Documento.Righe(i).Descrizione = PdcCPControPartita.Descrizione
                    End If

                    If Documento.Righe(i).RigaDaCausale = 9 Then
                        Dim PdcCP As New Cls_Pianodeiconti

                        PdcCP.Mastro = Documento.Righe(i).MastroPartita
                        PdcCP.Conto = Documento.Righe(i).ContoPartita
                        PdcCP.Sottoconto = Documento.Righe(i).SottocontoPartita
                        PdcCP.Decodfica(ConnessioneGenerale)

                        Documento.Righe(i).Descrizione = PdcCP.Descrizione
                    End If
                End If
            Next
        End If
        Documento.Scrivi(ConnessioneGenerale, Documento.NumeroRegistrazione, True)

    End Sub

    Public Sub IlFaro(ByVal Documento As Cls_MovimentoContabile, ByVal ConnessioneOspite As String, ByVal ConnessioneTabelle As String, ByVal ConnessioneGenerale As String)
        Dim Quantita As Integer = 0
        Dim GiorniMeseAnticipato As Integer = 0
        Dim CodiceOspite As Integer = 0

        If Documento.Tipologia.Trim = "" Then
            Dim DecodificaMese As New Cls_StampaFatture

            For i = 0 To Documento.Righe.Length - 1
                If Not IsNothing(Documento.Righe(i)) Then
                    If Documento.Righe(i).RigaDaCausale = 1 Then

                        CodiceOspite = Int(Documento.Righe(i).SottocontoPartita / 100)
                    End If
                    If Documento.Righe(i).RigaDaCausale = 3 Then

                        Dim PdcCP As New Cls_Pianodeiconti

                        PdcCP.Mastro = Documento.Righe(i).MastroPartita
                        PdcCP.Conto = Documento.Righe(i).ContoPartita
                        PdcCP.Sottoconto = Documento.Righe(i).SottocontoPartita
                        PdcCP.Decodfica(ConnessioneGenerale)

                        Documento.Righe(i).Descrizione = PdcCP.Descrizione & " " & Documento.Righe(i).Descrizione
                        Quantita = Quantita + Documento.Righe(i).Quantita
                        GiorniMeseAnticipato = GiorniMese(Documento.Righe(i).MeseRiferimento, Documento.Righe(i).AnnoRiferimento)
                    End If
                End If
            Next

            Dim RettaTotale As New Cls_rettatotale

            RettaTotale.CENTROSERVIZIO = Documento.CentroServizio
            RettaTotale.CODICEOSPITE = CodiceOspite
            RettaTotale.UltimaData(ConnessioneOspite, RettaTotale.CODICEOSPITE, RettaTotale.CENTROSERVIZIO)

            Dim StatoAuto As New Cls_StatoAuto

            StatoAuto.CODICEOSPITE = CodiceOspite
            StatoAuto.CENTROSERVIZIO = Documento.CentroServizio
            StatoAuto.UltimaData(ConnessioneOspite, StatoAuto.CODICEOSPITE, StatoAuto.CENTROSERVIZIO)

            Dim Proporzione As Double = 1

            If Quantita < GiorniMeseAnticipato Then
                Proporzione = GiorniMeseAnticipato / Quantita
            End If

            If Documento.CentroServizio = "RF" Then
                REM Documento.Descrizione = "SPESE DETRAIBILI IN DICHIARAZIONE DEI REDDITI: ASSISTENZA DI BASE ALLA PERSONA SVOLTA DA PERSONALE IN POSSESSO DELLA QUALIFICA PROFESSIONALE: €.  " & Math.Round(Proporzione * 627.49, 2)
            End If

            If Documento.CentroServizio = "CSG" Or Documento.CentroServizio = "OPT" Or Documento.CentroServizio = "RC" Or Documento.CentroServizio = "VP" Or Documento.CentroServizio = "MC" Or Documento.CentroServizio = "RG" Then
                Dim ImportoDetraibile As Double = 0
                Dim ImprotoDeducibile As Double = 0

                If StatoAuto.USL = "" Then
                    If RettaTotale.TipoRetta = "01" Then
                        ImportoDetraibile = 724.89
                        ImprotoDeducibile = 109.99
                    End If
                    If RettaTotale.TipoRetta = "02" Then
                        ImportoDetraibile = 825.57
                        ImprotoDeducibile = 164.69
                    End If
                    If RettaTotale.TipoRetta = "03" Then
                        ImportoDetraibile = 875.91
                        ImprotoDeducibile = 219.39
                    End If
                    If RettaTotale.TipoRetta = "04" Then
                        ImportoDetraibile = 1057.13
                        ImprotoDeducibile = 354.46
                    End If
                    If RettaTotale.TipoRetta = "05" Then
                        ImportoDetraibile = 1208.15
                        ImprotoDeducibile = 409.16
                    End If
                    REM Documento.Descrizione = "SPESE DETRAIBILI IN DICHIARAZIONE DEI REDDITI:ASSISTENZA DI BASE ALLA PERSONA SVOLTA DA PERSONALE IN POSSESSO DELLA QUALIFICA PROFESSIONALE: € " & Math.Round(ImportoDetraibile / Proporzione, 2) & vbNewLine & "SPESE DEDUCIBILI IN DICHIARAZIONE DEI REDDITI: SPESE PER ASSISTENZA SPECIFICA SVOLTA DA PERSONALE QUALIFICATO: €  " & Math.Round(ImprotoDeducibile / Proporzione, 2)
                Else
                    If RettaTotale.TipoRetta = "01" Then
                        ImportoDetraibile = 362.45
                    End If
                    If RettaTotale.TipoRetta = "02" Then
                        ImportoDetraibile = 412.78
                    End If
                    If RettaTotale.TipoRetta = "03" Then
                        ImportoDetraibile = 437.95
                    End If
                    If RettaTotale.TipoRetta = "04" Then
                        ImportoDetraibile = 528.57
                    End If
                    If RettaTotale.TipoRetta = "05" Then
                        ImportoDetraibile = 604.08
                    End If
                    REM Documento.Descrizione = "SPESE DETRAIBILI IN DICHIARAZIONE DEI REDDITI:ASSISTENZA DI BASE ALLA PERSONA SVOLTA DA PERSONALE IN POSSESSO DELLA QUALIFICA PROFESSIONALE: € " & Math.Round(ImportoDetraibile / Proporzione, 2)
                End If
            End If

            Documento.Scrivi(ConnessioneGenerale, Documento.NumeroRegistrazione, True)
        Else
            For i = 0 To Documento.Righe.Length - 1
                If Not IsNothing(Documento.Righe(i)) Then
                    If Documento.Righe(i).RigaDaCausale >= 3 Then
                        Dim Pdc As New Cls_Pianodeiconti

                        Pdc.Mastro = Documento.Righe(i).MastroPartita
                        Pdc.Conto = Documento.Righe(i).ContoPartita
                        Pdc.Sottoconto = Documento.Righe(i).SottocontoPartita
                        Pdc.Decodfica(ConnessioneGenerale)


                        Dim PdcCP As New Cls_Pianodeiconti

                        PdcCP.Mastro = Documento.Righe(i).MastroContropartita
                        PdcCP.Conto = Documento.Righe(i).ContoContropartita
                        PdcCP.Sottoconto = Documento.Righe(i).SottocontoContropartita
                        PdcCP.Decodfica(ConnessioneGenerale)

                        Documento.Righe(i).Descrizione = Pdc.Descrizione & " " & Documento.Righe(i).Descrizione & " " & PdcCP.Descrizione
                    End If
                    If Documento.Righe(i).RigaDaCausale = 12 Then
                        Dim Pdc As New Cls_Pianodeiconti

                        Pdc.Mastro = Documento.Righe(i).MastroPartita
                        Pdc.Conto = Documento.Righe(i).ContoPartita
                        Pdc.Sottoconto = Documento.Righe(i).SottocontoPartita
                        Pdc.Decodfica(ConnessioneGenerale)

                        Documento.Righe(i).Descrizione = Pdc.Descrizione
                    End If
                End If
            Next

            Documento.Scrivi(ConnessioneGenerale, Documento.NumeroRegistrazione, True)
        End If

    End Sub
    Public Sub CoopSanLorenzo(ByVal Documento As Cls_MovimentoContabile, ByVal ConnessioneOspite As String, ByVal ConnessioneTabelle As String, ByVal ConnessioneGenerale As String)
        Dim Modificato As Boolean = False

        For i = 0 To Documento.Righe.Length - 1
            If Not IsNothing(Documento.Righe(i)) Then
                If Documento.Righe(i).RigaDaCausale = 3 And Documento.Righe(i).SottocontoContropartita = 888001 Then
                    Dim PDC As New Cls_Pianodeiconti

                    PDC.Mastro = Documento.Righe(i).MastroPartita
                    PDC.Conto = Documento.Righe(i).ContoPartita
                    PDC.Sottoconto = Documento.Righe(i).SottocontoPartita
                    PDC.Decodfica(ConnessioneGenerale)

                    If Documento.CentroServizio = "CD" Then
                        Documento.Righe(i).Descrizione = "QUOTA CENTRO DIURNO UN.COMUNI " & Documento.Righe(i).Descrizione
                    Else
                        Documento.Righe(i).Descrizione = Documento.Righe(i).Descrizione
                    End If
                    Modificato = True
                End If
                If Documento.Righe(i).RigaDaCausale = 3 And Documento.Righe(i).SottocontoContropartita <> 888001 Then
                    Dim PDC As New Cls_Pianodeiconti
                    Dim RaggruppaInElaborazione As Integer = 0


                    PDC.Mastro = Documento.Righe(i).MastroContropartita
                    PDC.Conto = Documento.Righe(i).ContoContropartita
                    PDC.Sottoconto = Documento.Righe(i).SottocontoContropartita
                    PDC.Decodfica(ConnessioneGenerale)

                    If Mid(Documento.Tipologia & Space(10), 1, 1) = "R" Then
                        Dim Regione As String
                        Dim TabUSl As New ClsUSL

                        Regione = Mid(Documento.Tipologia & Space(10), 2, 4).Trim

                        TabUSl.CodiceRegione = Regione
                        TabUSl.Leggi(ConnessioneOspite)

                        RaggruppaInElaborazione = TabUSl.RaggruppaInElaborazione
                    End If
                    If Mid(Documento.Tipologia & Space(10), 1, 1) = "C" Then
                        Dim Provincia As String
                        Dim Comune As String
                        Dim TabComune As New ClsComune

                        Provincia = Mid(Documento.Tipologia & Space(10), 2, 3).Trim
                        Comune = Mid(Documento.Tipologia & Space(10), 5, 3).Trim

                        TabComune.Provincia = Provincia
                        TabComune.Comune = Comune
                        TabComune.Leggi(ConnessioneOspite)

                        RaggruppaInElaborazione = TabComune.RaggruppaInElaborazione
                    End If


                    If RaggruppaInElaborazione = 1 Then
                        If Documento.CentroServizio = "CD" Then
                            If Documento.Righe(i).SottocontoPartita = 5 Then
                                Documento.Righe(i).Descrizione = "RETTA CENTRO DIURNO ALZH. " & Documento.Righe(i).Descrizione
                            Else
                                Documento.Righe(i).Descrizione = "RETTA CENTRO DIURNO BASE " & Documento.Righe(i).Descrizione
                            End If

                        Else
                            Documento.Righe(i).Descrizione = "RETTA RSA  " & Documento.Righe(i).Descrizione
                        End If
                    Else
                        If Documento.CentroServizio = "CD" Then
                            If Documento.Righe(i).SottocontoPartita = 5 Then
                                Documento.Righe(i).Descrizione = "RETTA CENTRO DIURNO ALZH. " & Documento.Righe(i).Descrizione & " " & PDC.Descrizione
                            Else
                                Documento.Righe(i).Descrizione = "RETTA CENTRO DIURNO BASE " & Documento.Righe(i).Descrizione & " " & PDC.Descrizione
                            End If
                        Else
                            Documento.Righe(i).Descrizione = "RETTA RSA  " & Documento.Righe(i).Descrizione & " " & PDC.Descrizione
                        End If
                    End If

                    Modificato = True
                End If
            End If
        Next


        If Modificato Then
            Documento.Scrivi(ConnessioneGenerale, Documento.NumeroRegistrazione, True)
        End If

    End Sub


    Public Sub MezzaSelva(ByVal Documento As Cls_MovimentoContabile, ByVal ConnessioneOspite As String, ByVal ConnessioneTabelle As String, ByVal ConnessioneGenerale As String)
        Dim CodiceOspite As Integer = 0

        CodiceOspite = Int(Documento.Righe(0).SottocontoPartita / 100)



        For i = 0 To Documento.Righe.Length - 1
            If Not IsNothing(Documento.Righe(i)) Then
                If Documento.Righe(i).RigaDaCausale = 3 And Not (Documento.Righe(i).MastroPartita = 51 And Documento.Righe(i).ContoPartita = 7 And Documento.Righe(i).SottocontoPartita = 3) Then

                    Dim Accoglimento As New Cls_Movimenti

                    Accoglimento.CodiceOspite = CodiceOspite
                    Accoglimento.CENTROSERVIZIO = Documento.CentroServizio
                    Accoglimento.UltimaDataAccoglimento(ConnessioneOspite)



                    Dim UscitaDefinitiva As New Cls_Movimenti

                    UscitaDefinitiva.CodiceOspite = CodiceOspite
                    UscitaDefinitiva.CENTROSERVIZIO = Documento.CentroServizio
                    UscitaDefinitiva.UltimaDataUscitaDefinitiva(ConnessioneOspite)

                    If Format(Accoglimento.Data, "yyyyMMdd") <= Format(DateSerial(Documento.Righe(i).AnnoRiferimento, Documento.Righe(i).MeseRiferimento, 1), "yyyyMMdd") Then
                        Accoglimento.Data = DateSerial(Documento.Righe(i).AnnoRiferimento, Documento.Righe(i).MeseRiferimento, 1)
                    End If

                    If Format(UscitaDefinitiva.Data, "yyyyMMdd") > Format(DateSerial(Documento.Righe(i).AnnoRiferimento, Documento.Righe(i).MeseRiferimento, GiorniMese(Documento.Righe(i).MeseRiferimento, Documento.Righe(i).AnnoRiferimento)), "yyyyMMdd") Then
                        UscitaDefinitiva.Data = DateSerial(Documento.Righe(i).AnnoRiferimento, Documento.Righe(i).MeseRiferimento, GiorniMese(Documento.Righe(i).MeseRiferimento, Documento.Righe(i).AnnoRiferimento))
                    End If


                    Documento.Righe(i).Descrizione = "SALDO DEGENZA RIABILITATIVA DAL " & Format(Accoglimento.Data, "dd/MM/yyyy") & " AL " & Format(UscitaDefinitiva.Data, "dd/MM/yyyy")
                End If
            End If
        Next

        Documento.Scrivi(ConnessioneGenerale, Documento.NumeroRegistrazione, True)
    End Sub


    Public Sub Jole(ByVal Documento As Cls_MovimentoContabile, ByVal ConnessioneOspite As String, ByVal ConnessioneTabelle As String, ByVal ConnessioneGenerale As String)

        Dim GiorniPresenza As Integer = 0
        Dim ImportoPresenza As Double
        Dim CodiceOspite As Integer = 0


        Dim cn As OleDbConnection



        cn = New Data.OleDb.OleDbConnection(ConnessioneGenerale)

        cn.Open()

        For i = 0 To Documento.Righe.Length - 1
            If Not IsNothing(Documento.Righe(i)) Then
                If Documento.Righe(i).RigaDaCausale = 3 Then
                    GiorniPresenza = GiorniPresenza + Documento.Righe(i).Quantita
                    ImportoPresenza = ImportoPresenza + Documento.Righe(i).Importo
                End If
            End If
        Next

        CodiceOspite = Int(Documento.Righe(0).SottocontoPartita / 100)


        Dim M As New Cls_CentroServizio

        M.CENTROSERVIZIO = Documento.CentroServizio
        M.Leggi(ConnessioneOspite, M.CENTROSERVIZIO)


        Dim UltModalita As New Cls_Modalita

        UltModalita.CentroServizio = Documento.CentroServizio
        UltModalita.CodiceOspite = CodiceOspite
        UltModalita.UltimaData(ConnessioneOspite, UltModalita.CodiceOspite, UltModalita.CentroServizio)

        Dim UltStato As New Cls_StatoAuto

        UltStato.CENTROSERVIZIO = Documento.CentroServizio
        UltStato.CODICEOSPITE = CodiceOspite
        If Anno = 0 Or Mese = 0 Then
            UltStato.Data = Now
        Else
            UltStato.Data = DateSerial(Documento.AnnoCompetenza, Documento.MeseCompetenza, GiorniMese(Documento.MeseCompetenza, Documento.AnnoCompetenza))
        End If
        UltStato.StatoPrimaData(ConnessioneOspite, UltModalita.CodiceOspite, UltModalita.CentroServizio)


        Dim MRj As New Cls_rettatotale

        MRj.CODICEOSPITE = CodiceOspite

        If Anno = 0 Or Mese = 0 Then
            MRj.Data = Now
        Else
            MRj.Data = DateSerial(Documento.AnnoCompetenza, Documento.MeseCompetenza, GiorniMese(Documento.MeseCompetenza, Documento.AnnoCompetenza))
        End If
        MRj.RettaTotaleAData(ConnessioneOspite, CodiceOspite, Documento.CentroServizio, MRj.Data)

        Dim Xs As New Cls_TipoRetta

        Xs.Tipo = MRj.TipoRetta
        Xs.Leggi(ConnessioneOspite, Xs.Tipo)

        If Xs.Descrizione <> "HCP" Then
            If M.CENTROSERVIZIO = "2" Then
                If Trim(UltStato.USL) <> "" Then
                    If UltModalita.MODALITA = "O" Or UltModalita.MODALITA = "P" Then
                        Documento.Descrizione = Documento.Descrizione & vbNewLine & "Quota da detrarre per assistenza specifica : " & Math.Round(GiorniPresenza * 5.84, 2)
                    Else
                        Documento.Descrizione = Documento.Descrizione & vbNewLine & "Quota da detrarre per assistenza specifica : " & Math.Round(ImportoPresenza * 18.84 / 100, 2)
                    End If
                Else
                    If UltStato.STATOAUTO = "N" Then
                        Documento.Descrizione = Documento.Descrizione & vbNewLine & "Quota da detrarre per assistenza sanitaria e specifica : " & Math.Round(GiorniPresenza * (5.84 + 30.93), 2)
                    End If
                End If
            Else
                If Trim(UltStato.USL) <> "" Then
                    If UltModalita.MODALITA = "O" Or UltModalita.MODALITA = "P" Then
                        Documento.Descrizione = Documento.Descrizione & vbNewLine & "Quota da detrarre per assistenza specifica : " & Math.Round(GiorniPresenza * 9.73, 2)
                    Else
                        Documento.Descrizione = Documento.Descrizione & vbNewLine & "Quota da detrarre per assistenza specifica : " & Math.Round(ImportoPresenza * 18.35 / 100, 2)
                    End If
                Else
                    If UltStato.STATOAUTO = "N" Then
                        Documento.Descrizione = Documento.Descrizione & vbNewLine & "Quota da detrarre per assistenza sanitaria e specifica : " & Math.Round(GiorniPresenza * (9.73 + 52.32), 2)
                    End If
                End If
            End If
        End If


        Dim cmd As New OleDbCommand()


        cmd.CommandText = ("UPDATE   Temp_MovimentiContabiliTesta SET DESCRIZIONE = ? where " & _
                           "NumeroRegistrazione = ? ")

        cmd.Parameters.AddWithValue("@Descrizione", Documento.Descrizione)
        cmd.Parameters.AddWithValue("@NumeroRegistrazione", Documento.NumeroRegistrazione)
        cmd.Connection = cn
        cmd.ExecuteNonQuery()

        cn.Close()
    End Sub

    Public Sub Amelia(ByVal Documento As Cls_MovimentoContabile, ByVal ConnessioneOspite As String, ByVal ConnessioneTabelle As String, ByVal ConnessioneGenerale As String)

        Dim GiorniPresenza As Integer = 0
        Dim ImportoPresenza As Double
        Dim CodiceOspite As Integer = 0
        Dim QuotaPrivata As Boolean = False

        Dim cn As OleDbConnection



        cn = New Data.OleDb.OleDbConnection(ConnessioneGenerale)

        cn.Open()

        For i = 0 To Documento.Righe.Length - 1
            If Not IsNothing(Documento.Righe(i)) Then
                If Documento.Righe(i).RigaDaCausale = 3 Then
                    GiorniPresenza = GiorniPresenza + Documento.Righe(i).Quantita
                    ImportoPresenza = ImportoPresenza + Documento.Righe(i).Importo
                End If
                If Documento.Righe(i).Descrizione.ToUpper = "Quota Privata Quota Privata".ToUpper Then
                    Documento.Righe(i).Descrizione = "Quota Privata"
                    GiorniPresenza = GiorniPresenza + Documento.Righe(i).Quantita
                    ImportoPresenza = ImportoPresenza + Documento.Righe(i).Importo
                    QuotaPrivata = True
                End If
            End If
        Next

        If QuotaPrivata = True Then
            Documento.Scrivi(ConnessioneGenerale, Documento.NumeroRegistrazione, True)
        End If

        CodiceOspite = Int(Documento.Righe(0).SottocontoPartita / 100)



        Dim UltModalita As New Cls_Modalita

        UltModalita.CentroServizio = Documento.CentroServizio
        UltModalita.CodiceOspite = CodiceOspite
        UltModalita.UltimaData(ConnessioneOspite, UltModalita.CodiceOspite, UltModalita.CentroServizio)

        Dim UltStato As New Cls_StatoAuto

        UltStato.CENTROSERVIZIO = Documento.CentroServizio
        UltStato.CODICEOSPITE = CodiceOspite
        UltStato.UltimaData(ConnessioneOspite, UltModalita.CodiceOspite, UltModalita.CentroServizio)


        If Documento.CentroServizio = "2" Then
            If Trim(UltStato.USL) <> "" Then
                If UltModalita.MODALITA = "O" Or UltModalita.MODALITA = "P" Then
                    Documento.Descrizione = Documento.Descrizione & vbNewLine & "Quota da detrarre per assistenza specifica : " & Math.Round(GiorniPresenza * 5.83, 2)
                Else
                    Documento.Descrizione = Documento.Descrizione & vbNewLine & "Quota da detrarre per assistenza specifica : " & Math.Round(ImportoPresenza * 16.56 / 100, 2)
                End If
            Else
                If UltStato.STATOAUTO = "N" Then
                    Documento.Descrizione = Documento.Descrizione & vbNewLine & "Quota da detrarre per assistenza sanitaria e specifica : " & Math.Round(GiorniPresenza * (5.83 + 31.4), 2)
                Else
                    Documento.Descrizione = Documento.Descrizione & vbNewLine & "Quota da detrarre per assistenza specifica : " & Math.Round(GiorniPresenza * 5.83, 2)
                End If
            End If
        Else
            If Trim(UltStato.USL) <> "" Then
                If UltModalita.MODALITA = "O" Or UltModalita.MODALITA = "P" Then
                    Documento.Descrizione = Documento.Descrizione & vbNewLine & "Quota da detrarre per assistenza specifica : " & Math.Round(GiorniPresenza * 9.73, 2)
                Else
                    Documento.Descrizione = Documento.Descrizione & vbNewLine & "Quota da detrarre per assistenza specifica : " & Math.Round(ImportoPresenza * 18.47 / 100, 2)
                End If
            Else
                If UltStato.STATOAUTO = "N" Then
                    Documento.Descrizione = Documento.Descrizione & vbNewLine & "Quota da detrarre per assistenza sanitaria e specifica : " & Math.Round(GiorniPresenza * (9.73 + 52.32), 2)
                Else
                    Documento.Descrizione = Documento.Descrizione & vbNewLine & "Quota da detrarre per assistenza specifica : " & Math.Round(GiorniPresenza * 9.73, 2)
                End If
            End If
        End If

        Dim cmd As New OleDbCommand()


        cmd.CommandText = ("UPDATE   Temp_MovimentiContabiliTesta SET DESCRIZIONE = ? where " & _
                           "NumeroRegistrazione = ? ")

        cmd.Parameters.AddWithValue("@Descrizione", Documento.Descrizione)
        cmd.Parameters.AddWithValue("@NumeroRegistrazione", Documento.NumeroRegistrazione)
        cmd.Connection = cn
        cmd.ExecuteNonQuery()

        cn.Close()
    End Sub


    Public Sub SanMartino(ByVal Documento As Cls_MovimentoContabile, ByVal ConnessioneOspite As String, ByVal ConnessioneTabelle As String, ByVal ConnessioneGenerale As String)

        Dim GiorniPresenza As Integer = 0
        Dim ImportoPresenza As Double
        Dim CodiceOspite As Integer = 0


        Dim cn As OleDbConnection



        cn = New Data.OleDb.OleDbConnection(ConnessioneGenerale)

        cn.Open()

        For i = 0 To Documento.Righe.Length - 1
            If Not IsNothing(Documento.Righe(i)) Then
                If Documento.Righe(i).RigaDaCausale = 3 Then
                    GiorniPresenza = GiorniPresenza + Documento.Righe(i).Quantita
                    ImportoPresenza = ImportoPresenza + Documento.Righe(i).Importo
                End If
            End If
        Next

        CodiceOspite = Int(Documento.Righe(0).SottocontoPartita / 100)



        Dim UltModalita As New Cls_Modalita

        UltModalita.CentroServizio = Documento.CentroServizio
        UltModalita.CodiceOspite = CodiceOspite
        UltModalita.UltimaData(ConnessioneOspite, UltModalita.CodiceOspite, UltModalita.CentroServizio)

        Dim UltStato As New Cls_StatoAuto

        UltStato.CENTROSERVIZIO = Documento.CentroServizio
        UltStato.CODICEOSPITE = CodiceOspite
        UltStato.UltimaData(ConnessioneOspite, UltModalita.CodiceOspite, UltModalita.CentroServizio)


        If Documento.CentroServizio = "2" Then
            If Trim(UltStato.USL) <> "" Then
                If UltModalita.MODALITA = "O" Or UltModalita.MODALITA = "P" Then
                    Documento.Descrizione = Documento.Descrizione & vbNewLine & "Quota da detrarre per assistenza specifica : " & Math.Round(GiorniPresenza * 5.83, 2)
                Else
                    Documento.Descrizione = Documento.Descrizione & vbNewLine & "Quota da detrarre per assistenza specifica : " & Math.Round(ImportoPresenza * 16.56 / 100, 2)
                End If
            Else
                If UltStato.STATOAUTO = "N" Then
                    Documento.Descrizione = Documento.Descrizione & vbNewLine & "Quota da detrarre per assistenza sanitaria e specifica : " & Math.Round(GiorniPresenza * (5.83 + 31.4), 2)
                Else
                    Documento.Descrizione = Documento.Descrizione & vbNewLine & "Quota da detrarre per assistenza specifica : " & Math.Round(GiorniPresenza * 5.83, 2)
                End If
            End If
        Else
            If Trim(UltStato.USL) <> "" Then
                If UltModalita.MODALITA = "O" Or UltModalita.MODALITA = "P" Then
                    Documento.Descrizione = Documento.Descrizione & vbNewLine & "Quota da detrarre per assistenza specifica : " & Math.Round(GiorniPresenza * 9.73, 2)
                Else
                    Documento.Descrizione = Documento.Descrizione & vbNewLine & "Quota da detrarre per assistenza specifica : " & Math.Round(ImportoPresenza * 18.47 / 100, 2)
                End If
            Else
                If UltStato.STATOAUTO = "N" Then
                    Documento.Descrizione = Documento.Descrizione & vbNewLine & "Quota da detrarre per assistenza sanitaria e specifica : " & Math.Round(GiorniPresenza * (9.73 + 52.32), 2)
                Else
                    Documento.Descrizione = Documento.Descrizione & vbNewLine & "Quota da detrarre per assistenza specifica : " & Math.Round(GiorniPresenza * 9.73, 2)
                End If
            End If
        End If

        Dim cmd As New OleDbCommand()


        cmd.CommandText = ("UPDATE   Temp_MovimentiContabiliTesta SET DESCRIZIONE = ? where " & _
                           "NumeroRegistrazione = ? ")

        cmd.Parameters.AddWithValue("@Descrizione", Documento.Descrizione)
        cmd.Parameters.AddWithValue("@NumeroRegistrazione", Documento.NumeroRegistrazione)
        cmd.Connection = cn
        cmd.ExecuteNonQuery()

        cn.Close()
    End Sub

    Private Sub AnniSereni(ByVal Documento As Cls_MovimentoContabile, ByVal ConnessioneOspite As String, ByVal ConnessioneTabelle As String, ByVal ConnessioneGenerale As String)


        Dim GiorniPresenza As Integer = 0
        Dim ImportoPresenza As Double
        Dim CodiceOspite As Integer = 0
        Dim Riga As Integer = 0



        For Riga = 0 To 300

            If Not IsNothing(Documento.Righe(Riga)) Then
                If Documento.Righe(Riga).RigaDaCausale <> 9 Then
                    If Documento.Tipologia = "" Or Documento.Tipologia = "O" Or Documento.Tipologia = "P" Then
                        Dim Conto As New Cls_Pianodeiconti

                        Conto.Mastro = Documento.Righe(Riga).MastroPartita
                        Conto.Conto = Documento.Righe(Riga).ContoPartita
                        Conto.Sottoconto = Documento.Righe(Riga).SottocontoPartita
                        Conto.Decodfica(ConnessioneGenerale)



                        If Documento.Righe(Riga).RigaDaCausale = 4 Or Documento.Righe(Riga).RigaDaCausale = 5 Or Documento.Righe(Riga).RigaDaCausale = 6 Then
                            Documento.Righe(Riga).Descrizione = Documento.Righe(Riga).Descrizione
                        Else
                            If Conto.Descrizione = Documento.Righe(Riga).Descrizione Then
                                Documento.Righe(Riga).Descrizione = Conto.Descrizione
                            Else
                                Documento.Righe(Riga).Descrizione = Conto.Descrizione & "  " & UCase(Documento.Righe(Riga).Descrizione)
                            End If
                        End If
                    Else
                        Dim Conto As New Cls_Pianodeiconti

                        Conto.Mastro = Documento.Righe(Riga).MastroPartita
                        Conto.Conto = Documento.Righe(Riga).ContoPartita
                        Conto.Sottoconto = Documento.Righe(Riga).SottocontoPartita
                        Conto.Decodfica(ConnessioneGenerale)


                        Dim ContoControPartita As New Cls_Pianodeiconti

                        ContoControPartita.Mastro = Documento.Righe(Riga).MastroContropartita
                        ContoControPartita.Conto = Documento.Righe(Riga).ContoContropartita
                        ContoControPartita.Sottoconto = Documento.Righe(Riga).SottocontoContropartita
                        ContoControPartita.Decodfica(ConnessioneGenerale)
                        If Mid(Documento.Tipologia & Space(10), 1, 1) = "C" Then
                            Dim DesRigaComune As New Cls_ImportoComune

                            DesRigaComune.CODICEOSPITE = Int(Documento.Righe(Riga).SottocontoContropartita / 100)
                            DesRigaComune.CENTROSERVIZIO = Documento.CentroServizio
                            DesRigaComune.UltimaData(ConnessioneOspite, DesRigaComune.CODICEOSPITE, DesRigaComune.CENTROSERVIZIO)



                            Documento.Righe(Riga).Descrizione = Conto.Descrizione & "  " & ContoControPartita.Descrizione & " " & Documento.Righe(Riga).Descrizione & " " & DesRigaComune.DescrizioneRiga
                        Else
                            Documento.Righe(Riga).Descrizione = Conto.Descrizione & " " & Documento.Righe(Riga).Descrizione & "  " & ContoControPartita.Descrizione
                        End If
                    End If
                End If
            End If
        Next

        Documento.Scrivi(ConnessioneGenerale, Documento.NumeroRegistrazione, True)
    End Sub

    Private Sub ResidenzaParadiso(ByVal Documento As Cls_MovimentoContabile, ByVal ConnessioneOspite As String, ByVal ConnessioneTabelle As String, ByVal ConnessioneGenerale As String)
        Dim I As Integer
        Dim CodiceOspite As Integer
        Dim CodiceParente As Integer
        Dim TipoOperazione As String = ""



        CodiceOspite = Int(Documento.Righe(0).SottocontoPartita / 100)

        If Int(Documento.Righe(0).SottocontoPartita / 100) <> Documento.Righe(0).SottocontoPartita / 100 Then
            CodiceParente = Documento.Righe(0).SottocontoPartita - (CodiceOspite * 100)
        End If

        If CodiceParente = 0 Then
            Dim M As New ClsOspite

            M.CodiceOspite = CodiceOspite
            M.Leggi(ConnessioneOspite, M.CodiceOspite)

            Dim DatCs As New Cls_DatiOspiteParenteCentroServizio


            DatCs.CodiceOspite = CodiceOspite
            DatCs.CentroServizio = Documento.CentroServizio
            DatCs.Leggi(ConnessioneOspite)
            If DatCs.TipoOperazione <> "" Then
                M.TIPOOPERAZIONE = DatCs.TipoOperazione
            End If

            TipoOperazione = M.TIPOOPERAZIONE
        Else
            Dim M As New Cls_Parenti

            M.CodiceOspite = CodiceOspite
            M.CodiceParente = CodiceParente
            M.Leggi(ConnessioneOspite, M.CodiceOspite, M.CodiceParente)

            Dim DatCs As New Cls_DatiOspiteParenteCentroServizio


            DatCs.CodiceOspite = CodiceOspite
            DatCs.CodiceParente = CodiceParente
            DatCs.CentroServizio = Documento.CentroServizio
            DatCs.Leggi(ConnessioneOspite)
            If DatCs.TipoOperazione <> "" Then
                M.TIPOOPERAZIONE = DatCs.TipoOperazione
            End If

            TipoOperazione = M.TIPOOPERAZIONE
        End If

        Dim DecTipo As New Cls_TipoOperazione

        DecTipo.Codice = TipoOperazione
        DecTipo.Leggi(ConnessioneOspite, DecTipo.Codice)

        For Riga = 0 To 300
            If Not IsNothing(Documento.Righe(Riga)) Then
                If Documento.Righe(Riga).RigaDaCausale = 3 Then
                    Documento.Righe(Riga).Descrizione = DecTipo.Descrizione
                End If
            End If
        Next

        Documento.Scrivi(ConnessioneGenerale, Documento.NumeroRegistrazione, True)
    End Sub

    Private Sub OspizioMarino(ByVal Documento As Cls_MovimentoContabile, ByVal ConnessioneOspite As String, ByVal ConnessioneTabelle As String, ByVal ConnessioneGenerale As String)
        Dim CodiceOspite As Integer = 0
        If Documento.Tipologia = "" Then
            If Documento.CentroServizio = "RIC" Then
                Dim Imponibile As Double = 0
                Dim Aliquota As String = ""
                Dim Listino As New Cls_Listino

                For Riga = 0 To 300
                    If Not IsNothing(Documento.Righe(Riga)) Then
                        If Documento.Righe(Riga).RigaDaCausale = 3 And Documento.Righe(Riga).Descrizione.ToUpper.IndexOf("SCONTO") < 0 Then
                            Documento.Righe(Riga).Descrizione = "Ricovero - Tariffa per assistenza medica infermieristica"
                            Imponibile = Imponibile + Documento.Righe(Riga).Importo
                            Aliquota = Documento.Righe(Riga).CodiceIVA
                            If Listino.CodiceListino = "11" Or Listino.CodiceListino = "03" Then
                                Documento.Righe(Riga).Descrizione = "RICOVERO PRIVATO – ACCOMPAGNATORI"
                            End If
                            If Listino.CodiceListino = "11" Or Listino.CodiceListino = "03" Then
                                Documento.Righe(Riga).Descrizione = "RICOVERO PRIVATO – ACCOMPAGNATORI"
                            End If
                            If Listino.CodiceListino = "16" Then
                                Documento.Righe(Riga).Descrizione = "DEGENZA PER ASSISTENZA - ADDEBITO PASTI"
                            End If
                        End If
                        If Documento.Righe(Riga).RigaDaCausale = 13 Then
                            Documento.Righe(Riga).Descrizione = "Ricovero - Tariffa per spese generali e vitto"
                        End If

                        If Documento.Righe(Riga).RigaDaCausale = 5 And Documento.Righe(Riga).TipoExtra = "A16" Then
                            Imponibile = Imponibile + Documento.Righe(Riga).Importo
                            Aliquota = Documento.Righe(Riga).CodiceIVA
                        End If

                        If Documento.Righe(Riga).RigaDaCausale = 5 And Documento.Righe(Riga).TipoExtra = "A01" Then
                            Documento.Righe(Riga).Descrizione = "Ricovero - Tariffa per assistenza medica infermieristica"
                            Imponibile = Imponibile + Documento.Righe(Riga).Importo
                            Aliquota = Documento.Righe(Riga).CodiceIVA
                        End If
                        If Documento.Righe(Riga).RigaDaCausale = 5 And Documento.Righe(Riga).TipoExtra = "A05" Then
                            Documento.Righe(Riga).Descrizione = "Ricovero - Tariffa per spese generali e vitto"
                        End If

                        If Documento.Righe(Riga).RigaDaCausale = 1 Then
                            CodiceOspite = Int(Documento.Righe(Riga).SottocontoPartita / 100)

                            Listino.CODICEOSPITE = CodiceOspite
                            Listino.CENTROSERVIZIO = Documento.CentroServizio
                            Listino.LeggiAData(ConnessioneOspite, Documento.DataRegistrazione)

                        End If
                    End If
                Next

                Dim DecIVa As New Cls_IVA
                DecIVa.Codice = Aliquota
                DecIVa.Leggi(ConnessioneTabelle, Aliquota)

                Dim Sconto As Double = 0

                If CodiceOspite > 0 Then
                    Dim Ospite As New ClsOspite

                    Ospite.CodiceOspite = CodiceOspite
                    Ospite.Leggi(ConnessioneOspite, CodiceOspite)

                    If Ospite.ImportoSconto > 0 Then
                        Dim DatiSconto As New Cls_Sconto

                        DatiSconto.Codice = Ospite.TipoSconto
                        DatiSconto.Leggi(ConnessioneOspite, DatiSconto.Codice)

                        If DatiSconto.Tipologia = "" Then
                            Sconto = Modulo.MathRound((Imponibile + (Imponibile * DecIVa.Aliquota)) * Ospite.ImportoSconto / 100, 2)

                        End If

                    End If
                End If



                Dim Detraibilita As String = ""

                Detraibilita = "Quota detraibile " & Format(Imponibile + (Imponibile * DecIVa.Aliquota) - Sconto, "#,##0.00")

                If CodiceOspite > 0 Then

                    Listino.CODICEOSPITE = CodiceOspite
                    Listino.CENTROSERVIZIO = Documento.CentroServizio
                    Listino.LeggiAData(ConnessioneOspite, Documento.DataRegistrazione)
                    If Listino.CodiceListino = "05" Then
                        Detraibilita = "RSA BASE 50% - " & Detraibilita
                    End If
                    If Listino.CodiceListino = "03" Or Listino.CodiceListino = "11" Then
                        Detraibilita = ""
                    End If
                End If


                Documento.Descrizione = Detraibilita & " " & Documento.Descrizione

            End If
            If Documento.CentroServizio = "RIC" Then


            End If
        Else
            Dim UltimoMasto As Integer = 0
            Dim UltimoConto As Integer = 0
            Dim UltimoSottoConto As Integer = 0

            For Riga = 0 To 300
                If Not IsNothing(Documento.Righe(Riga)) Then
                    If Documento.Righe(Riga).RigaDaCausale = 3 Then

                        If Documento.Tipologia = "R300" Then 'ASFO
                            If Documento.CentroServizio = "AMB" Then
                                Documento.Righe(Riga).MastroPartita = 58
                                Documento.Righe(Riga).ContoPartita = 5
                                Documento.Righe(Riga).SottocontoPartita = 515 'RICAVI AMBULATORIALI ASFO
                            End If
                            If Documento.CentroServizio = "RIC" Then
                                Documento.Righe(Riga).MastroPartita = 58
                                Documento.Righe(Riga).ContoPartita = 5
                                Documento.Righe(Riga).SottocontoPartita = 510 'RICAVI RICOVERI ASFO - PN N/E/S/O/URB
                            End If
                        End If

                        If Documento.Tipologia = "R1900" Then 'ASUFC
                            If Documento.CentroServizio = "RIC" And Documento.Righe(Riga).SottocontoPartita = 502 Then
                                Documento.Righe(Riga).MastroPartita = 58
                                Documento.Righe(Riga).ContoPartita = 5
                                Documento.Righe(Riga).SottocontoPartita = 504 'RICAVI RSA ASUFC
                            End If
                        End If

                        If Documento.Tipologia = "R200" Then 'ASUFC
                            If Documento.CentroServizio = "AMB" Then
                                Documento.Righe(Riga).MastroPartita = 58
                                Documento.Righe(Riga).ContoPartita = 5
                                Documento.Righe(Riga).SottocontoPartita = 514 'RICAVI AMBULATORIALI ASUFC
                            End If
                            If Documento.CentroServizio = "RIC" And Documento.Righe(Riga).SottocontoPartita = 507 Then
                                Documento.Righe(Riga).MastroPartita = 58
                                Documento.Righe(Riga).ContoPartita = 5
                                Documento.Righe(Riga).SottocontoPartita = 508  'RICOVERI ASUFC
                            End If

                        End If

                        If Documento.Righe(Riga).Descrizione.IndexOf("Scorporo") >= 0 Then
                            If UltimoMasto > 0 And UltimoConto > 0 And UltimoSottoConto > 0 Then
                                Documento.Righe(Riga).MastroPartita = UltimoMasto
                                Documento.Righe(Riga).ContoPartita = UltimoConto
                                Documento.Righe(Riga).SottocontoPartita = UltimoSottoConto
                            End If
                        End If


                        Dim ContoControPartita As New Cls_Pianodeiconti

                        ContoControPartita.Mastro = Documento.Righe(Riga).MastroContropartita
                        ContoControPartita.Conto = Documento.Righe(Riga).ContoContropartita
                        ContoControPartita.Sottoconto = Documento.Righe(Riga).SottocontoContropartita
                        ContoControPartita.Decodfica(ConnessioneGenerale)

                        Dim Conto As New Cls_Pianodeiconti

                        Conto.Mastro = Documento.Righe(Riga).MastroPartita
                        Conto.Conto = Documento.Righe(Riga).ContoPartita
                        Conto.Sottoconto = Documento.Righe(Riga).SottocontoPartita
                        Conto.Decodfica(ConnessioneGenerale)


                        Documento.Righe(Riga).Descrizione = Conto.Descrizione & " " & ContoControPartita.Descrizione & " " & Documento.Righe(Riga).Descrizione


                        UltimoMasto = Documento.Righe(Riga).MastroPartita
                        UltimoConto = Documento.Righe(Riga).ContoPartita
                        UltimoSottoConto = Documento.Righe(Riga).SottocontoPartita
                    End If
                End If
            Next
        End If
        Documento.Scrivi(ConnessioneGenerale, Documento.NumeroRegistrazione, True)
    End Sub



    Private Sub IstitutoCiechi(ByVal Documento As Cls_MovimentoContabile, ByVal ConnessioneOspite As String, ByVal ConnessioneTabelle As String, ByVal ConnessioneGenerale As String)

        For Riga = 0 To 300
            If Not IsNothing(Documento.Righe(Riga)) Then
                If Documento.Righe(Riga).RigaDaCausale = 3 Then
                    If Documento.CentroServizio = "CDD" Then
                        Dim TipoRiga As Integer = 0
                        If Documento.Righe(Riga).Descrizione.IndexOf("Assenze") > 0 Then
                            TipoRiga = 1
                        End If

                        If TipoRiga = 0 Then
                            Documento.Righe(Riga).Descrizione = "Contributo attività fisioterapica e psicomotricità"
                        End If

                        If TipoRiga = 1 Then
                            Documento.Righe(Riga).Descrizione = "Contributo costi generali"
                        End If
                    Else
                        If Documento.Righe(Riga).MastroPartita = 58 And Documento.Righe(Riga).ContoPartita = 20 And Documento.Righe(Riga).SottocontoPartita = 3 Then
                            Documento.Righe(Riga).Descrizione = "Quota Servizi Aggiuntivi Extra Retta"
                        Else
                            Documento.Righe(Riga).Descrizione = "Retta casa famiglia"
                        End If
                    End If
                End If
            End If

        Next

        Documento.Scrivi(ConnessioneGenerale, Documento.NumeroRegistrazione, True)
    End Sub

    Private Sub Betulle(ByVal Documento As Cls_MovimentoContabile, ByVal ConnessioneOspite As String, ByVal ConnessioneTabelle As String, ByVal ConnessioneGenerale As String)
        If Documento.Tipologia = "R1" Then
            For Riga = 0 To 300
                If Not IsNothing(Documento.Righe(Riga)) Then
                    If Documento.Righe(Riga).RigaDaCausale = 3 Then
                        If Documento.Righe(Riga).Descrizione = "Presenze" Then
                            Documento.Righe(Riga).Descrizione = "Quota sanitaria di primo livello assistenziale - AREA SANITARIA - giornate di presenza"
                        End If
                        If Documento.Righe(Riga).Descrizione = "Assenze" Then
                            Documento.Righe(Riga).Descrizione = "Quota sanitaria di primo livello assistenziale - AREA SANITARIA - giornate di assenza"
                        End If
                    End If
                End If
            Next

            Documento.Scrivi(ConnessioneGenerale, Documento.NumeroRegistrazione, True)
        End If
        If Documento.Tipologia = "R2" Then
            For Riga = 0 To 300
                If Not IsNothing(Documento.Righe(Riga)) Then
                    If Documento.Righe(Riga).RigaDaCausale = 3 Then
                        If Documento.Righe(Riga).Descrizione = "Presenze" Then
                            Documento.Righe(Riga).Descrizione = "Quota sanitaria di primo livello assistenziale - AREA SANITARIA - giornate di presenza"
                        End If
                        If Documento.Righe(Riga).Descrizione = "Assenze" Then
                            Documento.Righe(Riga).Descrizione = "Quota sanitaria di primo livello assistenziale - AREA SANITARIA - giornate di assenza"
                        End If
                    End If
                End If
            Next

            Documento.Scrivi(ConnessioneGenerale, Documento.NumeroRegistrazione, True)
        End If

        If Documento.Tipologia = "J888001" Then
            Dim RigaAd As Integer = 0

            For Riga = 0 To 300
                If Not IsNothing(Documento.Righe(Riga)) Then
                    If Documento.Righe(Riga).RigaDaCausale = 3 Then
                        If Documento.Righe(Riga).Descrizione = "Presenze" Then
                            Documento.Righe(Riga).Descrizione = "Importo per Logo AREA SANITARIA"
                            For RigaAd = 0 To 300
                                If Not IsNothing(Documento.Righe(RigaAd)) Then
                                    If Documento.Righe(RigaAd).RigaDaCausale = 5 And Documento.Righe(RigaAd).SottocontoContropartita = Documento.Righe(Riga).SottocontoContropartita And Documento.Righe(RigaAd).ContoContropartita = Documento.Righe(Riga).ContoContropartita And Documento.Righe(RigaAd).MastroContropartita = Documento.Righe(Riga).MastroContropartita Then
                                        Documento.Righe(RigaAd).Descrizione = "Importo per FKT AREA SANITARIA"
                                        Documento.Righe(RigaAd).Quantita = Documento.Righe(Riga).Quantita
                                    End If
                                End If
                            Next
                        End If
                    End If
                End If
            Next

            Documento.Scrivi(ConnessioneGenerale, Documento.NumeroRegistrazione, True)
        End If

    End Sub


    Public Sub ModificaDescrizioneDocumento(ByVal Documento As Cls_MovimentoContabile, ByVal ConnessioneOspite As String, ByVal ConnessioneTabelle As String, ByVal ConnessioneGenerale As String)
        Dim CentroServizio As New Cls_CentroServizio

        CentroServizio.CENTROSERVIZIO = Documento.CentroServizio
        CentroServizio.Leggi(ConnessioneOspite, CentroServizio.CENTROSERVIZIO)

        If CentroServizio.DescrizioneRigaInFattura = "San Martino" Then
            If Documento.Tipologia = "" Then
                SanMartino(Documento, ConnessioneOspite, ConnessioneTabelle, ConnessioneGenerale)
            End If
        End If

        If CentroServizio.DescrizioneRigaInFattura = "Villa Amelia" Then
            If Documento.Tipologia = "" Then
                Amelia(Documento, ConnessioneOspite, ConnessioneTabelle, ConnessioneGenerale)
            End If
        End If

        If CentroServizio.DescrizioneRigaInFattura = "Villa Jole" Then
            If Documento.Tipologia = "" Then
                Jole(Documento, ConnessioneOspite, ConnessioneTabelle, ConnessioneGenerale)
            End If
        End If

        If CentroServizio.DescrizioneRigaInFattura = "ANNISERENI" Then
            AnniSereni(Documento, ConnessioneOspite, ConnessioneTabelle, ConnessioneGenerale)
        End If



        If CentroServizio.DescrizioneRigaInFattura = "Residenza Paradiso" Then
            If Documento.Tipologia = "" Then
                ResidenzaParadiso(Documento, ConnessioneOspite, ConnessioneTabelle, ConnessioneGenerale)
            End If
        End If


        If CentroServizio.DescrizioneRigaInFattura = "Conto Retta - descrizione - Betulle" Then
            If Mid(Documento.Tipologia & Space(10), 1, 1) = "J" Or Mid(Documento.Tipologia & Space(10), 1, 1) = "R" Then
                Betulle(Documento, ConnessioneOspite, ConnessioneTabelle, ConnessioneGenerale)
            End If
        End If


        If CentroServizio.DescrizioneRigaInFattura = "MezzaSelva Res" Then
            If Documento.Tipologia = "" Then
                MezzaSelva(Documento, ConnessioneOspite, ConnessioneTabelle, ConnessioneGenerale)
            End If
        End If

        If CentroServizio.DescrizioneRigaInFattura = "ANFFAS Carrara" Then

            ANFFASCarrara(Documento, ConnessioneOspite, ConnessioneTabelle, ConnessioneGenerale)

        End If



        If CentroServizio.DescrizioneRigaInFattura = "CoopSanLorenzo" Then

            CoopSanLorenzo(Documento, ConnessioneOspite, ConnessioneTabelle, ConnessioneGenerale)

        End If



        If CentroServizio.DescrizioneRigaInFattura = "IlFaro" Then

            IlFaro(Documento, ConnessioneOspite, ConnessioneTabelle, ConnessioneGenerale)

        End If


        If CentroServizio.DescrizioneRigaInFattura = "DonMoschetta" Then

            DonMoschetta(Documento, ConnessioneOspite, ConnessioneTabelle, ConnessioneGenerale)

        End If


        If CentroServizio.DescrizioneRigaInFattura = "IstitutoCiechi" Then

            IstitutoCiechi(Documento, ConnessioneOspite, ConnessioneTabelle, ConnessioneGenerale)

        End If


        If CentroServizio.DescrizioneRigaInFattura = "OspizioMarino" Then

            OspizioMarino(Documento, ConnessioneOspite, ConnessioneTabelle, ConnessioneGenerale)

        End If
        If CentroServizio.DescrizioneRigaInFattura = "Gabbiano" Then

            IlGabbiano(Documento, ConnessioneOspite, ConnessioneTabelle, ConnessioneGenerale)

        End If

        If CentroServizio.DescrizioneRigaInFattura = "Libera" Then

            Libera(Documento, ConnessioneOspite, ConnessioneTabelle, ConnessioneGenerale)

        End If


    End Sub


    Public Function GiorniMese(ByVal Mese As Byte, ByVal Anno As Integer) As Byte
        If Mese = 1 Or Mese = 3 Or Mese = 5 Or Mese = 7 Or Mese = 8 Or _
           Mese = 10 Or Mese = 12 Then
            GiorniMese = 31
        Else
            If Mese <> 2 Then
                GiorniMese = 30
            Else
                If Day(DateSerial(Anno, Mese, 29)) = 29 Then
                    GiorniMese = 29
                Else
                    GiorniMese = 28
                End If
            End If
        End If
    End Function
End Class
