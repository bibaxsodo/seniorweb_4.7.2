Imports Microsoft.VisualBasic
Imports System.Data.OleDb
Imports System.Web.Hosting

Public Class Cls_CausaleContabile
    Public Codice As String
    Public Tipo As String
    Public Descrizione As String
    Public RegistroIVA As Long
    Public TipoDocumento As String
    Public DataObbligatoria As String
    Public NumeroObbligatorio As String
    Public CausaleIncasso As String
    Public AllegatoFineAnno As String
    Public VenditaAcquisti As String
    Public CodiceIva As String
    Public Detraibilita As String
    Public CentroServizio As String
    Public Prorata As String
    Public Ritenuta As String
    Public Report As String
    Public CodiceCIG As Long
    Public DocumentoReverse As String
    Public GirocontoReverse As String

    Public CodiceSede As String 'Esportazione Alyante    

    Public AnaliticaObbligatoria As Integer

    Public GestioneCespiti As Integer
    Public AbilitaGirocontoReverse As Integer
    

    Public Righe(100) As Cls_CausaliContabiliRiga
    Public Utente As String


    Function campodb(ByVal oggetto As Object) As String
        If IsDBNull(oggetto) Then
            Return ""
        Else
            Return oggetto
        End If
    End Function

    Public Sub New()
        Codice = ""
        Tipo = ""
        Descrizione = ""
        RegistroIVA = 0
        TipoDocumento = ""
        DataObbligatoria = Nothing
        NumeroObbligatorio = ""
        CausaleIncasso = ""
        AllegatoFineAnno = ""
        VenditaAcquisti = ""
        CodiceIva = ""
        Detraibilita = ""
        CentroServizio = ""
        Prorata = ""
        Ritenuta = ""
        Report = ""
        CodiceCIG = 0
        DocumentoReverse = ""
        GirocontoReverse = ""
        CodiceSede = ""

        GestioneCespiti = 0
        AbilitaGirocontoReverse = 0


        AnaliticaObbligatoria = 0

    End Sub

    Function EstraiCondizioniSQL(ByVal StringaConnessione As String) As String
        Dim cn As OleDbConnection


        cn = New Data.OleDb.OleDbConnection(StringaConnessione)

        cn.Open()
        Dim cmd As New OleDbCommand()

        EstraiCondizioniSQL = ""
        cmd.CommandText = ("select * from CausaliContabiliTesta Where (TIPO <> 'P' and TIPO <> 'C') AND VenditaAcquisti <> ''")
        cmd.Connection = cn
        Dim myPOSTreader As OleDbDataReader = cmd.ExecuteReader()
        Do While myPOSTreader.Read
            If EstraiCondizioniSQL <> "" Then
                EstraiCondizioniSQL = EstraiCondizioniSQL & " or "
            End If
            EstraiCondizioniSQL = EstraiCondizioniSQL & " CausaleContabile = '" & myPOSTreader.Item("CODICE") & "'"
        Loop
        myPOSTreader.Close()
        cn.Close()
        Return EstraiCondizioniSQL
    End Function

    Sub UpDateDropBoxDoc(ByVal StringaConnessione As String, ByRef appoggio As DropDownList)
        Dim cn As OleDbConnection



        cn = New Data.OleDb.OleDbConnection(StringaConnessione)

        cn.Open()
        Dim cmd As New OleDbCommand()
        cmd.CommandText = ("select * from CausaliContabiliTesta where Tipo = 'I' OR Tipo = 'R' order by Descrizione")
        cmd.Connection = cn
        Dim sb As StringBuilder = New StringBuilder

        appoggio.Items.Clear()
        appoggio.Items.Add("")
        appoggio.Items(appoggio.Items.Count - 1).Value = ""
        appoggio.Items(appoggio.Items.Count - 1).Selected = True
        Dim myPOSTreader As OleDbDataReader = cmd.ExecuteReader()
        Do While myPOSTreader.Read
            appoggio.Items.Add(myPOSTreader.Item("Descrizione"))
            appoggio.Items(appoggio.Items.Count - 1).Value = myPOSTreader.Item("Codice")
        Loop
        myPOSTreader.Close()
        cn.Close()


    End Sub

    Sub UpDateDropBoxPag(ByVal StringaConnessione As String, ByRef appoggio As DropDownList)
        Dim cn As OleDbConnection



        cn = New Data.OleDb.OleDbConnection(StringaConnessione)

        cn.Open()
        Dim cmd As New OleDbCommand()
        cmd.CommandText = ("select * from CausaliContabiliTesta where Tipo = 'P' order by Descrizione")
        cmd.Connection = cn
        Dim sb As StringBuilder = New StringBuilder

        appoggio.Items.Clear()
        appoggio.Items.Add("")
        appoggio.Items(appoggio.Items.Count - 1).Value = ""
        appoggio.Items(appoggio.Items.Count - 1).Selected = True
        Dim myPOSTreader As OleDbDataReader = cmd.ExecuteReader()
        Do While myPOSTreader.Read
            appoggio.Items.Add(myPOSTreader.Item("Descrizione"))
            appoggio.Items(appoggio.Items.Count - 1).Value = myPOSTreader.Item("Codice")
        Loop
        myPOSTreader.Close()
        cn.Close()


    End Sub

    Sub UpDateDropBox(ByVal StringaConnessione As String, ByRef appoggio As DropDownList)
        Dim cn As OleDbConnection



        cn = New Data.OleDb.OleDbConnection(StringaConnessione)

        cn.Open()
        Dim cmd As New OleDbCommand()
        cmd.CommandText = ("select * from CausaliContabiliTesta order by Descrizione")
        cmd.Connection = cn
        Dim sb As StringBuilder = New StringBuilder

        appoggio.Items.Clear()
        appoggio.Items.Add("")
        appoggio.Items(appoggio.Items.Count - 1).Value = ""
        appoggio.Items(appoggio.Items.Count - 1).Selected = True
        Dim myPOSTreader As OleDbDataReader = cmd.ExecuteReader()
        Do While myPOSTreader.Read
            appoggio.Items.Add(myPOSTreader.Item("Descrizione"))
            appoggio.Items(appoggio.Items.Count - 1).Value = myPOSTreader.Item("Codice")
        Loop
        myPOSTreader.Close()
        cn.Close()


    End Sub


    Sub UpDateCheckBox(ByVal StringaConnessione As String, ByRef appoggio As CheckBoxList)
        Dim cn As OleDbConnection



        cn = New Data.OleDb.OleDbConnection(StringaConnessione)

        cn.Open()
        Dim cmd As New OleDbCommand()
        cmd.CommandText = ("select * from CausaliContabiliTesta order by Descrizione")
        cmd.Connection = cn
        Dim sb As StringBuilder = New StringBuilder

        appoggio.Items.Clear()
        Dim myPOSTreader As OleDbDataReader = cmd.ExecuteReader()
        Do While myPOSTreader.Read
            appoggio.Items.Add(myPOSTreader.Item("Descrizione"))
            appoggio.Items(appoggio.Items.Count - 1).Value = myPOSTreader.Item("Codice")
        Loop
        myPOSTreader.Close()
        cn.Close()

    End Sub

    Sub Elimina(ByVal StringaConnessione As String, ByVal Codice As String)
        Dim cn As OleDbConnection



        cn = New Data.OleDb.OleDbConnection(StringaConnessione)

        cn.Open()
        Dim cmd As New OleDbCommand()
        cmd.CommandText = ("Delete from CausaliContabiliTesta where " & _
                           "Codice = ? ")
        cmd.Parameters.AddWithValue("@Codice", Codice)
        cmd.Connection = cn
        cmd.ExecuteNonQuery()

        Dim cmd1 As New OleDbCommand()
        cmd1.CommandText = ("Delete from CausaliContabiliRiga where " & _
                           "Codice = ? ")
        cmd1.Parameters.AddWithValue("@Codice", Codice)
        cmd1.Connection = cn
        cmd1.ExecuteNonQuery()

        cn.Close()
    End Sub

    Sub LeggiDescrizione(ByVal StringaConnessione As String, ByVal Descrizione As String)
        Dim cn As OleDbConnection


        If IsDBNull(Codice) Then
            Exit Sub
        End If

        cn = New Data.OleDb.OleDbConnection(StringaConnessione)

        cn.Open()
        Dim cmd As New OleDbCommand()
        cmd.CommandText = ("select * from CausaliContabiliTesta where " & _
                           "Descrizione Like ? ")
        cmd.Parameters.AddWithValue("@Descrizione", Descrizione.Trim)
        cmd.Connection = cn

        Dim myrd As OleDbDataReader = cmd.ExecuteReader()
        If myrd.Read Then
            Codice = campodb(myrd.Item("Codice"))
            Leggi(StringaConnessione, Codice)
        End If
        myrd.Close()

        cn.Close()
    End Sub

    Sub Leggi(ByVal StringaConnessione As String, ByVal Codice As String)
        Dim cn As OleDbConnection


        If IsDBNull(Codice) Then
            Exit Sub
        End If

        cn = New Data.OleDb.OleDbConnection(StringaConnessione)

        cn.Open()
        Dim cmd As New OleDbCommand()
        cmd.CommandText = ("select * from CausaliContabiliTesta where " & _
                           "Codice = ? ")
        cmd.Parameters.AddWithValue("@Codice", Codice)
        cmd.Connection = cn

        Dim myrd As OleDbDataReader = cmd.ExecuteReader()
        If myrd.Read Then
            Tipo = campodb(myrd.Item("Tipo"))
            Descrizione = campodb(myrd.Item("Descrizione"))
            RegistroIVA = Val(campodb(myrd.Item("RegistroIVA")))
            TipoDocumento = campodb(myrd.Item("TipoDocumento"))
            DataObbligatoria = campodb(myrd.Item("DataObbligatoria"))
            NumeroObbligatorio = campodb(myrd.Item("NumeroObbligatorio"))
            CausaleIncasso = campodb(myrd.Item("CausaleIncasso"))
            AllegatoFineAnno = campodb(myrd.Item("AllegatoFineAnno"))
            VenditaAcquisti = campodb(myrd.Item("VenditaAcquisti"))
            CodiceIva = campodb(myrd.Item("CodiceIva"))
            Detraibilita = campodb(myrd.Item("Detraibilita"))
            Ritenuta = campodb(myrd.Item("Ritenuta"))
            CentroServizio = campodb(myrd.Item("CentroServizio"))
            Prorata = campodb(myrd.Item("Prorata"))
            Report = campodb(myrd.Item("Report"))

            CodiceCIG = Val(campodb(myrd.Item("CodiceCIG")))
            DocumentoReverse = campodb(myrd.Item("DocumentoReverse"))
            GirocontoReverse = campodb(myrd.Item("GirocontoReverse"))
            CodiceSede = campodb(myrd.Item("CodiceSede"))


            AnaliticaObbligatoria = Val(campodb(myrd.Item("AnaliticaObbligatoria")))

            GestioneCespiti = Val(campodb(myrd.Item("GestioneCespiti")))
            AbilitaGirocontoReverse = Val(campodb(myrd.Item("AbilitaGirocontoReverse")))

        End If
        myrd.Close()

        Dim cmd1 As New OleDbCommand()
        cmd1.CommandText = ("select * from CausaliContabiliRiga where " & _
                           "Codice = ? Order by Riga")
        cmd1.Parameters.AddWithValue("@Codice", Codice)
        cmd1.Connection = cn

        Dim Indice As Long = 0
        Dim myrd1 As OleDbDataReader = cmd1.ExecuteReader()
        Do While myrd1.Read
            Righe(Indice) = New Cls_CausaliContabiliRiga

            Righe(Indice).Codice = campodb(myrd1.Item("Codice"))
            Righe(Indice).Riga = Val(campodb(myrd1.Item("Riga")))
            Righe(Indice).Mastro = Val(campodb(myrd1.Item("Mastro")))
            Righe(Indice).Conto = Val(campodb(campodb(myrd1.Item("Conto"))))
            Righe(Indice).Sottoconto = Val(campodb(myrd1.Item("Sottoconto")))
            Righe(Indice).Segno = campodb(myrd1.Item("Segno"))
            Righe(Indice).DareAvere = campodb(myrd1.Item("DareAvere"))


            Indice = Indice + 1
        Loop
        myrd1.Close()


        cn.Close()
    End Sub


    Function MaxCodice(ByVal StringaConnessione As String) As String
        Dim cn As OleDbConnection


        MaxCodice = ""

        cn = New Data.OleDb.OleDbConnection(StringaConnessione)

        cn.Open()
        Dim cmd As New OleDbCommand()
        cmd.CommandText = ("select max(Codice) from CausaliContabiliTesta ")
        cmd.Connection = cn
        Dim MaxAddebito As OleDbDataReader = cmd.ExecuteReader()
        If MaxAddebito.Read Then
            Dim MassimoLetto As String

            MassimoLetto = campodb(MaxAddebito.Item(0))
            If MassimoLetto = "999" Then
                MassimoLetto = "A00"
            Else
                If Mid(MassimoLetto & Space(10), 1, 1) > "0" And Mid(MassimoLetto & Space(10), 1, 1) > "9" Then
                    If Mid(MassimoLetto & Space(10), 2, 2) < "99" Then
                        MassimoLetto = Mid(MassimoLetto & Space(10), 1, 1) & Format(Val(Mid(MassimoLetto & Space(10), 2, 2)) + 1, "00")
                    Else
                        Dim CodiceAscii As String
                        CodiceAscii = Asc(Mid(MassimoLetto & Space(10), 1, 1))

                        MassimoLetto = Chr(CodiceAscii + 1) & "00"
                    End If
                Else
                    MassimoLetto = Format(Val(MassimoLetto) + 1, "000")
                End If
            End If

            Return MassimoLetto
        Else
            Return "001"
        End If
        cn.Close()

    End Function



    Sub UpDateDropBoxWC(ByVal StringaConnessione As String, ByRef appoggio As DropDownList)
        Dim cn As OleDbConnection



        cn = New Data.OleDb.OleDbConnection(StringaConnessione)

        cn.Open()
        Dim cmd As New OleDbCommand()
        cmd.CommandText = ("select * from CausaliContabiliTesta order by Descrizione")
        cmd.Connection = cn
        Dim sb As StringBuilder = New StringBuilder

        appoggio.Items.Clear()
        cn.Close()
        appoggio.Items.Add("")
        appoggio.Items(appoggio.Items.Count - 1).Value = ""
        appoggio.Items(appoggio.Items.Count - 1).Selected = True
        Dim myPOSTreader As OleDbDataReader = cmd.ExecuteReader()
        Do While myPOSTreader.Read
            appoggio.Items.Add(myPOSTreader.Item("Codice") & " " & myPOSTreader.Item("Descrizione"))
            appoggio.Items(appoggio.Items.Count - 1).Value = myPOSTreader.Item("Codice")
        Loop
        myPOSTreader.Close()


    End Sub




    Sub Scrivi(ByVal StringaConnessione As String, ByVal xCodice As String)
        Dim cn As OleDbConnection
        Dim Modifica As Boolean = False
        Dim MySql As String

        Codice = xCodice

        cn = New Data.OleDb.OleDbConnection(StringaConnessione)

        cn.Open()
        Dim cmd As New OleDbCommand()
        cmd.CommandText = ("select * from CausaliContabiliTesta where " & _
                           "Codice = ? ")
        cmd.Parameters.AddWithValue("@Codice", xCodice)
        cmd.Connection = cn

        Dim myrd As OleDbDataReader = cmd.ExecuteReader()
        If myrd.Read Then
            Modifica = True
        End If
        myrd.Close()

        If Modifica = False Then
            Dim cmdIns As New OleDbCommand()
            MySql = " INSERT INTO CausaliContabiliTesta (Codice) VALUES ('" & Codice & "')"
            cmdIns.CommandText = MySql
            cmdIns.Connection = cn
            cmdIns.ExecuteNonQuery()
        End If

        MySql = "UPDATE CausaliContabiliTesta SET " & _
         " Tipo = ?, " & _
         "  Descrizione = ?, " & _
         "  RegistroIVA = ?, " & _
         "  TipoDocumento = ?, " & _
         "  DataObbligatoria = ?, " & _
         "  NumeroObbligatorio = ?, " & _
         "  CausaleIncasso = ?, " & _
         "  AllegatoFineAnno = ?, " & _
         "  VenditaAcquisti = ?, " & _
         "  CodiceIva = ?, " & _
         "  Detraibilita = ?, " & _
         "  CentroServizio = ?, " & _
         "  Prorata = ?, " & _
         "  Report = ?,  " & _
         "  Ritenuta = ?, " & _
         "  CodiceCIG = ?, " & _
         "  DocumentoReverse = ?, " & _
         "  GirocontoReverse = ?, " & _
         "  CodiceSede = ?, " & _
         "  Utente = ?, " & _
         "  DataAggiornamento = ?, " & _
         "  AnaliticaObbligatoria = ?, " & _
         " GestioneCespiti = ?, " & _
         " AbilitaGirocontoReverse = ? " & _
         " Where Codice = ? "




        Dim cmd1 As New OleDbCommand()

        cmd1.CommandText = MySql
        cmd1.Parameters.AddWithValue("@Tipo", Tipo)
        cmd1.Parameters.AddWithValue("@Descrizione", Descrizione)
        cmd1.Parameters.AddWithValue("@RegistroIVA", RegistroIVA)
        cmd1.Parameters.AddWithValue("@TipoDocumento", TipoDocumento)
        cmd1.Parameters.AddWithValue("@DataObbligatoria", DataObbligatoria)
        cmd1.Parameters.AddWithValue("@NumeroObbligatorio", NumeroObbligatorio)
        cmd1.Parameters.AddWithValue("@CausaleIncasso", CausaleIncasso)
        cmd1.Parameters.AddWithValue("@AllegatoFineAnno", AllegatoFineAnno)
        cmd1.Parameters.AddWithValue("@VenditaAcquisti", VenditaAcquisti)
        cmd1.Parameters.AddWithValue("@CodiceIva", CodiceIva)
        cmd1.Parameters.AddWithValue("@Detraibilita", Detraibilita)
        cmd1.Parameters.AddWithValue("@CentroServizio", CentroServizio)
        cmd1.Parameters.AddWithValue("@Prorata", Prorata)
        cmd1.Parameters.AddWithValue("@Report", Report)
        cmd1.Parameters.AddWithValue("@Ritenuta", Ritenuta)
        cmd1.Parameters.AddWithValue("@CodiceCIG", CodiceCIG)
        cmd1.Parameters.AddWithValue("@DocumentoReverse", DocumentoReverse)
        cmd1.Parameters.AddWithValue("@GirocontoReverse", GirocontoReverse)
        cmd1.Parameters.AddWithValue("@CodiceSede", CodiceSede)
        cmd1.Parameters.AddWithValue("@Utente", Utente)
        cmd1.Parameters.AddWithValue("@DataAggiornamento", Now)
        cmd1.Parameters.AddWithValue("@AnaliticaObbligatoria", AnaliticaObbligatoria)
        cmd1.Parameters.AddWithValue("@GestioneCespiti", GestioneCespiti)
        cmd1.Parameters.AddWithValue("@AbilitaGirocontoReverse", AbilitaGirocontoReverse)
        cmd1.Parameters.AddWithValue("@Codice", Codice)
        cmd1.Connection = cn
        cmd1.ExecuteNonQuery()


        Dim cmdDelete As New OleDbCommand()
        cmdDelete.CommandText = ("Delete from CausaliContabiliRiga where " & _
                           "Codice = ? ")
        cmdDelete.Parameters.AddWithValue("@Codice", Codice)
        cmdDelete.Connection = cn

        cmdDelete.ExecuteNonQuery()

        Dim x As Integer

        For x = 0 To Righe.Length - 1

            If Not IsNothing(Righe(x)) Then
                Righe(x).Codice = Codice
                Righe(x).Insert(StringaConnessione)
            Else
                Exit For
            End If

        Next


        cn.Close()
    End Sub
End Class

