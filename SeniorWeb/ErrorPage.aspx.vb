﻿
Partial Class ErrorPage
    Inherits System.Web.UI.Page

    Sub Page_Load(ByVal Sender As Object, ByVal e As EventArgs)
        Throw (New System.ArgumentNullException())
    End Sub


    Sub Page_Error(ByVal Sender As Object, ByVal e As EventArgs)
        Dim objErr As Exception = Server.GetLastError().GetBaseException()
        Dim err As String = "<b>Error Caught in Page_Error event</b><hr><br>" & _
            "<br><b>Error in: </b>" & Request.Url.ToString() & _
            "<br><b>Error Message: </b>" & objErr.Message.ToString() & _
            "<br><b>Stack Trace:</b><br>" & _
            objErr.StackTrace.ToString()


        Lbl_Errore.Text = err.ToString
        Server.ClearError()
    End Sub


End Class
