﻿
// Decodifica pianoconti
function decodifica_pc_callback(r, oggetto)
{
    $("#" + oggetto).html(r);
}

function decodificapianoconti(oggetto,mastro,conto,sottoconto,utente)
{
     
    var url="/WebHandler/decadificapianodeiconti.ashx?";    
    url+="mastro="+escape(mastro);
    url+="&conto="+escape(conto);
    url+="&sottoconto="+escape(sottoconto);
    url+="&UTENTE="+escape(utente);
    url+="&casuale="+Math.floor(Math.random()*1000);    
    
    if (window.XMLHttpRequest)
    {// code for IE7+, Firefox, Chrome, Opera, Safari
    xmlhttp=new XMLHttpRequest();
    }
    else
    {// code for IE6, IE5
    xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
    }
    xmlhttp.onreadystatechange=function()
    {
    if(xmlhttp.readyState==4)
      { decodifica_pc_callback(xmlhttp.responseText,oggetto); }
    }

    xmlhttp.open("GET",url,true);
    xmlhttp.send(null);
}