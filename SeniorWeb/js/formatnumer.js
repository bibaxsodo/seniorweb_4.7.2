function Formatta_Punt (nStr) {
	 nStr = nStr.replace(".",",");
  nStr += '';
  x = nStr.split('.');
  x1 = x[0];
  x2 = x.length > 1 ? '.' + x[1] : '';
  var rgx = /(\d+)(\d{3})/;
  while (rgx.test(x1))
    x1 = x1.replace(rgx, '$1' + '.' + '$2');
  return x1 + x2;
}    


function formatNumber (num, decplaces) {
    var mystr = "";
        
    mystr =num;
        
    if (((mystr.lastIndexOf(".") == mystr.length -3) || (mystr.lastIndexOf(".") == mystr.length -2)) && (mystr.lastIndexOf(".") != -1) ) {              
       num = mystr.substring(0,mystr.lastIndexOf(".")) + "," + mystr.substring(mystr.lastIndexOf(".") + 1, mystr.length );        
    }
        
    
    // convert in case it arrives as a string value
    num = num.replace(".","")
    num = num.replace(".","")
    num = num.replace(".","")
    num = num.replace(",",".")
    
    num = parseFloat(num);
    
    // make sure it passes conversion
    if (!isNaN(num)) {
        // multiply value by 10 to the decplaces power;
        // round the result to the nearest integer;
        // convert the result to a string
        var str = "" + Math.round (eval(num) * Math.pow(10,decplaces));
        // exponent means value is too big or small for this routine
        if (str.indexOf("e") != -1) {
            return "Out of Range";
        }
        // if needed for small values, pad zeros
        // to the left of the number
        while (str.length <= decplaces) {
            str = "0" + str;
        }
        // calculate decimal point position
        var decpoint = str.length - decplaces;
        // assemble final result from: (a) the string up to the position of
        // the decimal point; (b) the decimal point; and (c) the balance
        // of the string. Return finished product.
        return Formatta_Punt(str.substring(0,decpoint)) + "," + str.substring(decpoint,str.length);
    } else {
        return "0";
    }
}  

 
 	function ForceNumericInput(This, AllowDot, AllowMinus)
	{
	    
		if(arguments.length == 1)
		{
        	var s = This.value;
        	// if "-" exists then it better be the 1st character
        	var i = s.lastIndexOf("-");
        	if(i == -1)
            	return;
        	if(i != 0)
           		This.value = s.substring(0,i)+s.substring(i+1);
           	return;
        }
    
        var code = event.keyCode;

        
        switch(code)
        {
            case 8:     // backspace
            case 37:    // left arrow
            case 39:    // right arrow
            case 46:    // delete
            case 44:    // ,
            case 45:    // -
                event.returnValue=true;
                return;            
        }
        if(code == 189)     // minus sign
        {
        	if(AllowMinus == false)
        	{
                event.returnValue=false;
                return;
            }


            // wait until the element has been updated to see if the minus is in the right spot
            var s = "ForceNumericInput(document.getElementById('"+This.id+"'))";
            setTimeout(s, 250);
            return;
        }
                
          
        if(AllowDot && code == 190) 
        {
            if(This.value.indexOf(".") >= 0 || This.value.indexOf(",") >= 0)
            {
            	// don't allow more than one dot
                event.returnValue=false;
                return;
            }
            event.returnValue=true;
            return;
        }
        // allow character of between 0 and 9
        if(code >= 48 && code <= 57)
        {
            event.returnValue=true;
            return;
        }
        event.returnValue=false;
	}


       function soloNumeri(evt) {
       var charCode = (evt.which) ? evt.which : event.keyCode
       if (charCode > 31 && (charCode < 48 || charCode > 57)) {
          return false;
       }
       return true;
     }