﻿Imports System.Web.Optimization

Public Class Global_asax
    Inherits HttpApplication

    Public Function MathMyRound(ByVal Numero As Double, ByVal numerodecimali As Integer) As Double

        Return Math.Round(Numero, numerodecimali, MidpointRounding.AwayFromZero)
    End Function

    Sub Application_Start(ByVal sender As Object, ByVal e As EventArgs)
        RouteConfig.RegisterRoutes(RouteTable.Routes)
        'BundleConfig.RegisterBundles(BundleTable.Bundles)
        BundleTable.EnableOptimizations = True

        
        Application("SENIOR") = "Provider=SQLOLEDB;Data Source=localhost\SQLE2014;Initial Catalog=SENIOR;User Id=sa;Password=Advenias1820!;"    
        

        If Server.MachineName.ToUpper = "DESKTOP-SGFMN3T" Then
            'PORTATILE ADVENIAS
            Application("SENIOR") = "Provider=SQLOLEDB;Data Source=DESKTOP-SGFMN3T\SQLEXPRESS;User ID=sa;Initial Catalog=SENIOR;Password=MASTER;"
        End If

        If Server.MachineName.ToUpper = "DESKTOP-NUT8GS5" Then
            'PORTATILE ADVENIAS
            Application("SENIOR") = "Provider=SQLOLEDB;Data Source=DESKTOP-NUT8GS5\SQLSEVER2008R2;Initial Catalog=SENIOR;User Id=sa;Password=MASTER;"
        End If


        If Server.MachineName.ToUpper = "DESKTOP-7OMVORS" Then
            'PC VALERIA
            Application("SENIOR") = "Provider=SQLOLEDB;Data Source=DESKTOP-7OMVORS\SQLEXPRESS;Initial Catalog=SENIOR;User Id=sa;Password=MASTER;"
        End If
        If Server.MachineName.ToUpper = "DESKTOP-1NALNBL" Then
            ' Serever Lucca
            Application("SENIOR") = "Provider=SQLOLEDB;Data Source=DESKTOP-1NALNBL\SQLEXPRESS;Initial Catalog=SENIOR;User Id=sa;Password=MASTER;"
        End If

        If Server.MachineName.ToUpper = "DESKTOP-60QH1NJ" Then
            ' Serever Lucca
            Application("SENIOR") = "Provider=SQLOLEDB;Data Source=DESKTOP-60QH1NJ\SQL2014;Initial Catalog=SENIOR;User Id=sa;Password=MASTER;"
        End If
        If Server.MachineName.ToUpper = "GABENOTEBOOK" Then
            ' Serever Lucca
            Application("SENIOR") = "Provider=SQLOLEDB;Data Source=GABENOTEBOOK\SQLGABE;Initial Catalog=SENIOR;User Id=sa;Password=MASTER;"
        End If

        If Server.MachineName = "GABE-PC" Then
            Application("SENIOR") = "Provider=SQLOLEDB;Data Source=GABE-PC\GABESQL;Initial Catalog=SENIOR;User Id=sa;Password=MASTER;"
        End If
        If Server.MachineName = "MAURO-PC" Then
            '    Application("SENIOR") = "Provider=SQLOLEDB;Data Source=Localhost\SQLSERVER08;Initial Catalog=SENIOR;User Id=sa;Password=MASTER;"
            Application("SENIOR") = "Provider=SQLOLEDB;Data Source=MAURO-PC\SQLEXPRESS08;Initial Catalog=SENIOR;User Id=sa;Password=MASTER;"
        End If
        If Server.MachineName = "PC-CESARE" Then
            Application("SENIOR") = "Provider=SQLOLEDB;Data Source=PC-CESARE\SQLEXPRESS08;Initial Catalog=SENIOR;User Id=sa;Password=MASTER;"
        End If

        If Server.MachineName = "PCADFOUR" Then
            Application("SENIOR") = "Provider=SQLOLEDB;Data Source=PCADFOUR\SQLE2014;Initial Catalog=SENIOR;User Id=sa;Password=advsis;"
        End If

        '"DESKTOP-KTRTTS2"

        If Server.MachineName = "DESKTOP-KTRTTS2" Then
            Application("SENIOR") = "Provider=SQLOLEDB;Data Source=DESKTOP-KTRTTS2\SQL2014;Initial Catalog=SENIOR;User Id=sa;Password=MASTER;"
        End If

    End Sub

    Sub Application_End(ByVal sender As Object, ByVal e As EventArgs)
        ' Codice eseguito alla chiusura dell&apos;applicazione
    End Sub

    Sub Application_Error(ByVal sender As Object, ByVal e As EventArgs)
        ' Codice eseguito in caso di errore non gestito
    End Sub

    Sub Session_Start(ByVal sender As Object, ByVal e As EventArgs)
        ' Codice eseguito all&apos;avvio di una nuova sessione
    End Sub

    Sub Session_End(ByVal sender As Object, ByVal e As EventArgs)
        ' Codice eseguito al termine di una sessione. 
        ' Nota: l&apos;evento Session_End viene generato solo quando la modalità sessionstate
        ' è impostata su InProc nel file Web.config. Se tale modalità è impostata su StateServer 
        ' o su SQLServer, l&apos;evento non viene generato.
    End Sub
End Class