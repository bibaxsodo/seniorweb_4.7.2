﻿
Partial Class ModificaPassword
    Inherits System.Web.UI.Page

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load


        If Session("UTENTE") = "" And (Request.Item("USER") = "" Or IsNothing(Request.Item("USER"))) Then
            Response.Redirect("Login.aspx")
            Exit Sub
        End If

        If Not (Request.Item("USER") = "" Or IsNothing(Request.Item("USER"))) Then

            Dim M As New Cls_Login


            M.Utente = Request.Item("USER")
            M.Ospiti = ""
            M.LeggiSP(Application("SENIOR"))
            If Request.Item("TOKEN") = M.PasswordCriptata Then
                ViewState("UTENTE") = Request.Item("USER")
                Exit Sub
            End If

            Response.Redirect("Login.aspx")
        End If


        ViewState("UTENTE") = Session("UTENTE")

    End Sub

    Protected Sub Button1_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles Button1.Click
        Txt_Password.Text = Txt_Password.Text.Trim
        If Txt_Password.Text <> Txt_PasswordConferma.Text Then
            ClientScript.RegisterClientScriptBlock(Me.GetType(), "Errore", "VisualizzaErrore('<center><b>Le password non coincidono</b></center>');", True)
            Exit Sub
        End If
        If Len(Txt_Password.Text) < 6 Then
            ClientScript.RegisterClientScriptBlock(Me.GetType(), "Errore", "VisualizzaErrore('<center><b>Password troppo corta</b></center>');", True)
            Exit Sub
        End If

        If Txt_Password.Text.Trim = "" Then
            ClientScript.RegisterClientScriptBlock(Me.GetType(), "Errore", "VisualizzaErrore('<center><b>Password troppo corta</b></center>');", True)
            Exit Sub
        End If

        If Txt_Password.Text = "******" Or Txt_Password.Text = "*******" Or Txt_Password.Text = "***** Then" Then
            ClientScript.RegisterClientScriptBlock(Me.GetType(), "Errore", "VisualizzaErrore('<center><b>Digitare Password</b></center>');", True)
            Exit Sub
        End If

        Dim TrovatoNumero As Boolean = False
        For i = 0 To Len(Txt_Password.Text) - 1
            If Txt_Password.Text.Substring(i, 1) = "0" Or Txt_Password.Text.Substring(i, 1) = "1" Or Txt_Password.Text.Substring(i, 1) = "2" Or Txt_Password.Text.Substring(i, 1) = "3" & _
               Txt_Password.Text.Substring(i, 1) = "4" Or Txt_Password.Text.Substring(i, 1) = "5" Or Txt_Password.Text.Substring(i, 1) = "6" Or Txt_Password.Text.Substring(i, 1) = "7" & _
               Txt_Password.Text.Substring(i, 1) = "8" Or Txt_Password.Text.Substring(i, 1) = "9" Then
                TrovatoNumero = True
            End If
        Next

        If TrovatoNumero = False Then
            ClientScript.RegisterClientScriptBlock(Me.GetType(), "Errore", "VisualizzaErrore('<center><b>La Password deve contenere un numero</b></center>');", True)
            Exit Sub
        End If

        Dim Kx As New Cls_Login
        Kx.Utente = ViewState("UTENTE")
        Kx.LeggiSP(Application("SENIOR"))
        If Kx.PasswordCriptata <> "" Then
            If BCrypt.Net.BCrypt.Verify(Txt_Password.Text, Kx.PasswordCriptata) = True Then
                ClientScript.RegisterClientScriptBlock(Me.GetType(), "Errore", "VisualizzaErrore('<center><b>Password già utilizzata</b></center>');", True)
                Exit Sub
            End If
        End If

        'If Kx.EmailUtente.Trim = "" Then
        '    ClientScript.RegisterClientScriptBlock(Me.GetType(), "Errore", "VisualizzaErrore('<center><b>Password troppo corta</b></center>');", True)
        '    Exit Sub
        'End If



        Dim KxVer As New Cls_Login

        KxVer.Cliente = 0

        Dim AppoggioUtente As String = ViewState("UTENTE").ToString

        For i = 1 To 20
            AppoggioUtente = AppoggioUtente.Replace("<" & i & ">", "")
        Next i

        KxVer.Utente = AppoggioUtente & "<1>"
        KxVer.LeggiSP(Application("SENIOR"))




        Dim ChiaveCriptata As String = BCrypt.Net.BCrypt.HashPassword(Txt_Password.Text)

        If KxVer.Cliente <> 0 Then

            Dim Appoggio As String = ViewState("UTENTE").ToString

            For i = 1 To 20
                Appoggio = Appoggio.Replace("<" & i & ">", "")
            Next i

            Kx.Utente = Appoggio
            Kx.LeggiSP(Application("SENIOR"))
            Kx.Chiave = Txt_Password.Text
            Kx.PasswordCriptata = ChiaveCriptata
            Kx.ScadenzaPassword = DateAdd(DateInterval.Month, 3, Now)
            Kx.Scrivi(Application("SENIOR"))

            For i = 1 To 20
                Kx.Utente = Appoggio & "<" & i & ">"
                Kx.Cliente = 0
                Kx.LeggiSP(Application("SENIOR"))
                If Kx.Cliente > 0 Then
                    Kx.Chiave = Txt_Password.Text
                    Kx.PasswordCriptata = ChiaveCriptata
                    Kx.ScadenzaPassword = DateAdd(DateInterval.Month, 3, Now)
                    Kx.Scrivi(Application("SENIOR"))
                End If
            Next

        Else
            Kx.Utente = ViewState("UTENTE")
            Kx.LeggiSP(Application("SENIOR"))
            Kx.Chiave = Txt_Password.Text
            Kx.PasswordCriptata = ChiaveCriptata
            Kx.ScadenzaPassword = DateAdd(DateInterval.Month, 3, Now)
            Kx.Scrivi(Application("SENIOR"))
        End If


        Response.Redirect("Login.aspx")
    End Sub
End Class
