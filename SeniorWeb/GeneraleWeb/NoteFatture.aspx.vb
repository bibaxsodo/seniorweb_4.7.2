﻿
Partial Class GeneraleWeb_NoteFatture
    Inherits System.Web.UI.Page

    Protected Sub GeneraleWeb_NoteFatture_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Trim(Session("UTENTE")) = "" Then
            If Request.ServerVariables("URL").ToUpper.IndexOf("SENIORWEBBLANCO") > 0 Then
                Response.Redirect("/ODV/Login.aspx")
                Exit Sub
            Else
                Response.Redirect("/SeniorWeb/Login.aspx")
                Exit Sub
            End If
        End If

        Dim K1 As New Cls_SqlString

        Dim Barra As New Cls_BarraSenior

        If Not IsNothing(Session("RicercaGeneraleSQLString")) Then
            K1 = Session("RicercaGeneraleSQLString")
        End If

        Lbl_BarraSenior.Text = Barra.CodiceBarra(Application("SENIOR"), Session("UTENTE"), Page, K1)

        If Page.IsPostBack = True Then Exit Sub


        Dim x As New Cls_NoteFattura



        If Request.Item("Codice") <> "" Then
            x.Codice = Request.Item("Codice")
            x.Leggi(Session("DC_GENERALE"))

            Txt_Codice.Enabled = False
            Txt_Codice.Text = x.Codice
            Txt_Descrizione.Text = x.Descrizione
            Txt_Note.Text = x.Note

        Else
            Txt_Codice.Text = x.MaxNoteFatture(Session("DC_GENERALE"))

        End If

    End Sub

    Protected Sub Btn_Modifica_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles Btn_Modifica.Click
        If Val(Txt_Codice.Text) = 0 Then
            ClientScript.RegisterClientScriptBlock(Me.GetType(), "Errore", "VisualizzaErrore('Specifica il codice');", True)
            Exit Sub
        End If

        If Txt_Codice.Enabled = True Then
            Dim xVerifico As New Cls_NoteFattura

            xVerifico.Codice = Txt_Codice.Text
            xVerifico.Leggi(Session("DC_GENERALE"))
            If xVerifico.Descrizione <> "" Then
                ClientScript.RegisterClientScriptBlock(Me.GetType(), "Errore", "VisualizzaErrore('Codice già presente');", True)
                Exit Sub
            End If
        End If

        Dim x As New Cls_NoteFattura


        If Not IsNothing(Request.Item("Codice")) Then
            x.Codice = Request.Item("Codice")
            x.Leggi(Session("DC_GENERALE"))
        End If

        x.Codice = Txt_Codice.Text
        x.Descrizione = Txt_Descrizione.Text
        x.Note = Txt_Note.Text


        x.Scrivi(Session("DC_GENERALE"))

        Response.Redirect("Elenco_NoteFatture.aspx")
    End Sub

    Protected Sub ImageButton1_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles ImageButton1.Click
        If Val(Txt_Codice.Text) = 0 Then
            ClientScript.RegisterClientScriptBlock(Me.GetType(), "Errore", "VisualizzaErrore('Specifica il codice');", True)
            Exit Sub
        End If

        Dim x As New Cls_NoteFattura


        x.Codice = Request.Item("Codice")
        x.Elimina(Session("DC_GENERALE"))
        Response.Redirect("Elenco_NoteFatture.aspx")
    End Sub

    Protected Sub Btn_Esci_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles Btn_Esci.Click
        Response.Redirect("Elenco_NoteFatture.aspx")
    End Sub

    Protected Sub Txt_Codice_TextChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles Txt_Codice.TextChanged
        If Txt_Codice.Enabled = True Then
            Img_VerificaCodice.ImageUrl = "~/images/corretto.gif"

            Dim x As New Cls_NoteFattura

            x.Codice = Txt_Codice.Text
            x.Leggi(Session("DC_GENERALE"))

            If x.Descrizione <> "" Then
                Img_VerificaCodice.ImageUrl = "~/images/errore.gif"
            End If
        End If
    End Sub

    Protected Sub Txt_Descrizione_TextChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles Txt_Descrizione.TextChanged
        If Txt_Codice.Enabled = True Then
            Img_VerificaDescrizione.ImageUrl = "~/images/corretto.gif"

            Dim x As New Cls_NoteFattura

            x.Codice = ""
            x.Descrizione = Txt_Descrizione.Text.Trim
            x.LeggiDescrizione(Session("DC_GENERALE"))

            If x.Codice <> "" Then
                Img_VerificaDescrizione.ImageUrl = "~/images/errore.gif"
            End If
        End If
    End Sub
End Class
