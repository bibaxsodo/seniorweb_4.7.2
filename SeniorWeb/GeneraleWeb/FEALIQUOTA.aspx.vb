﻿
Partial Class GeneraleWeb_FEALIQUOTA
    Inherits System.Web.UI.Page

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Trim(Session("UTENTE")) = "" Then
            Response.Redirect("../Login_generale.aspx")
            Exit Sub
        End If

        If Page.IsPostBack = True Then Exit Sub



        Dim K1 As New Cls_SqlString

        Dim Barra As New Cls_BarraSenior

        If Not IsNothing(Session("RicercaAnagraficaSQLString")) Then
            Try
                K1 = Session("RicercaAnagraficaSQLString")
            Catch ex As Exception
                K1 = Nothing
            End Try
        End If

        Lbl_BarraSenior.Text = Barra.CodiceBarra(Application("SENIOR"), Session("UTENTE"), Page, K1)


        Dim M As New Cls_IVA

        M.UpDateDropBox(Session("DC_TABELLE"), Dd_CodiceIVA)
        If Request.Item("Codice") = "" Then
            Exit Sub
        End If

        Dim Ks As New Cls_FeAliquotaIVA

        Ks.LeggiID(Session("DC_TABELLE"), Request.Item("Codice"))

 
        Dd_CodiceIVA.SelectedValue = Ks.CODICE
		Txt_Natura.Text = KS.NATURA
		Txt_Aliquota.Text = KS.ALIQUOTA
    End Sub

    Protected Sub Btn_Modifica_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles Btn_Modifica.Click
        Dim Ks As New Cls_FeAliquotaIVA
        

        Ks.LeggiID(Session("DC_TABELLE"), Val(Request.Item("Codice")))

        Ks.CODICE = Dd_CodiceIVA.SelectedValue
		KS.NATURA =Txt_Natura.Text
		KS.ALIQUOTA= Txt_Aliquota.Text 

        Ks.Scrivi(Session("DC_TABELLE"), Request.Item("Codice"))

        Response.Redirect("Elenco_FEALIQUOTA.aspx")
    End Sub

    Protected Sub Btn_Esci_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles Btn_Esci.Click
        Response.Redirect("Elenco_FEALIQUOTA.aspx")
    End Sub

    Protected Sub ImageButton1_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles ImageButton1.Click

        Dim Ks As New Cls_FeAliquotaIVA


        Ks.DeleteID(Session("DC_TABELLE"), Val(Request.Item("Codice")))

        Response.Redirect("Elenco_FEALIQUOTA.ASPX")


    End Sub
End Class
