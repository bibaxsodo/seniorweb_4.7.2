﻿
Partial Class GeneraleWeb_RicercaExcel
    Inherits System.Web.UI.Page
    Dim Tabella As New System.Data.DataTable("tabella")

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load


        Tabella = Session("AppoggioExcel")
        GridView1.AutoGenerateColumns = True
        GridView1.DataSource = Tabella
        GridView1.DataBind()


        Response.Clear()
        Response.AddHeader("content-disposition", "attachment;filename=Ricerca.xls")
        Response.Charset = String.Empty
        Response.Cache.SetCacheability(HttpCacheability.NoCache)
        Response.ContentType = "application/vnd.xls"
        Dim stringWrite As New System.IO.StringWriter
        Dim htmlWrite As New HtmlTextWriter(stringWrite)

        form1.Controls.Clear()
        form1.Controls.Add(GridView1)

        form1.RenderControl(htmlWrite)

        Response.Write(stringWrite.ToString())
        Response.End()
    End Sub
End Class
