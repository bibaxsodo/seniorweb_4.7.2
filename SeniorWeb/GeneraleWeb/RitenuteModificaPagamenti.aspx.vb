﻿
Partial Class GeneraleWeb_RitenuteModificaPagamenti
    Inherits System.Web.UI.Page

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load

        If Page.IsPostBack = True Then Exit Sub


        Dim Ritenuta As New Cls_RitenuteAcconto


        Ritenuta.ID = Request.Item("ID")
        Ritenuta.Leggi(Session("DC_GENERALE"))


        Txt_Importo.Text = Format(Ritenuta.ImportoPagamento, "#,##0.00")


        
        Dim registrazionecontabile As New Cls_MovimentoContabile


        registrazionecontabile.NumeroRegistrazione = Ritenuta.NumeroRegistrazionePagamento
        registrazionecontabile.Leggi(Session("DC_GENERAlE"), registrazionecontabile.NumeroRegistrazione)

        Lbl_DatiDoc.Text = "Numero " & registrazionecontabile.NumeroRegistrazione & " " & Format(registrazionecontabile.DataRegistrazione, "dd/MM/yyyy")


        Dim registrazionecontabileDoc As New Cls_MovimentoContabile


        registrazionecontabileDoc.NumeroRegistrazione = Ritenuta.NumeroRegistrazioneDocumento
        registrazionecontabileDoc.Leggi(Session("DC_GENERAlE"), registrazionecontabileDoc.NumeroRegistrazione)


        LblDati.Text = LblDati.Text & "<br/><br/>Numero Registrazioen :" & registrazionecontabile.NumeroRegistrazione
        LblDati.Text = LblDati.Text & "<br/><br/>Data Pagamento :" & Format(registrazionecontabile.DataRegistrazione, "dd/MM/yyyy")
        LblDati.Text = LblDati.Text & "<br/><br/>Importo :" & Format(registrazionecontabile.ImportoRegistrazioneDocumento(Session("DC_TABELLE")), "#,##0.00")
        LblDati.Text = LblDati.Text & "<br/>"
        LblDati.Text = LblDati.Text & "<br/><br/>Numero Documento :" & registrazionecontabileDoc.NumeroDocumento
        LblDati.Text = LblDati.Text & "<br/>Data Documento :" & Format(registrazionecontabileDoc.DataRegistrazione, "dd/MM/yyyy")
        LblDati.Text = LblDati.Text & "<br/>"
        Dim Riga As Integer
        For Riga = 0 To 300

            If Not IsNothing(registrazionecontabileDoc.Righe(Riga)) Then
                If registrazionecontabileDoc.Righe(Riga).Tipo = "RI" And registrazionecontabileDoc.Righe(Riga).DareAvere = "D" Then
                    LblDati.Text = LblDati.Text & "<br/>Imponibile Ritenuta :" & Format(registrazionecontabileDoc.Righe(Riga).Imponibile, "#,##0.00")

                    Dim Rit As New ClsRitenuta

                    Rit.Codice = registrazionecontabileDoc.Righe(Riga).CodiceRitenuta
                    Rit.Leggi(Session("DC_TABELLE"))

                    LblDati.Text = LblDati.Text & "<br/>Tipo Ritenuta :" & Rit.Descrizione
                    LblDati.Text = LblDati.Text & "<br/>Importo Ritenuta :" & Format(registrazionecontabileDoc.Righe(Riga).Importo, "#,##0.00")

                End If
            End If

        Next


    End Sub


    Protected Sub Btn_Elimina_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles Btn_Elimina.Click

        Dim Ritenuta As New Cls_RitenuteAcconto


        Ritenuta.ID = Request.Item("ID")
        Ritenuta.Leggi(Session("DC_GENERALE"))
        Ritenuta.NumeroRegistrazionePagamento = 0
        Ritenuta.ImportoPagamento = 0

        Ritenuta.Scrivi(Session("DC_GENERALE"))


        Response.Redirect("RitenutePagamento.aspx?NumeroRegistrazione=" & Request.Item("NUMERO"))
    End Sub

    Protected Sub Btn_Modifica_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles Btn_Modifica.Click
        Dim Ritenuta As New Cls_RitenuteAcconto


        Ritenuta.ID = Request.Item("ID")
        Ritenuta.Leggi(Session("DC_GENERALE"))

        If Ritenuta.ImportoRitenuta < CDbl(Txt_Importo.Text) Then
            Exit Sub
        End If
        Ritenuta.ImportoPagamento = Txt_Importo.Text


        Ritenuta.Scrivi(Session("DC_GENERALE"))

        Response.Redirect("RitenutePagamento.aspx?NumeroRegistrazione=" & Request.Item("NUMERO"))
    End Sub

End Class
