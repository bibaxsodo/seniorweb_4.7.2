﻿
Partial Class GeneraleWeb_RegistroIVA
    Inherits System.Web.UI.Page


    Protected Sub Btn_Modifica_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles Btn_Modifica.Click
        Dim k As New Cls_RegistriIVAanno
        Lbl_Errori.Text = ""

        If Val(Txt_Anno.Text) = 0 Then
            ScriptManager.RegisterClientScriptBlock(Me, Me.GetType(), "Errore", "VisualizzaErrore('Digitare anno');", True)
            Dim MyJs1 As String
            MyJs1 = "$(" & Chr(34) & "#" & Txt_DataStampa.ClientID & Chr(34) & ").mask(""99/99/9999""); "
            MyJs1 = MyJs1 & " $(" & Chr(34) & "#" & Txt_DataStampa.ClientID & Chr(34) & ").datepicker({ changeMonth: true,changeYear: true,  yearRange: ""1890:" & Year(Now) + 1 & """},$.datepicker.regional[""it""]);"
            ScriptManager.RegisterStartupScript(Me, Me.GetType(), "FormatDataDal", MyJs1, True)
            REM Lbl_Errori.Text = "Digitare anno"
            Exit Sub
        End If

        If DD_RegistroIVA.SelectedValue = "" Then
            ScriptManager.RegisterClientScriptBlock(Me, Me.GetType(), "Errore", "VisualizzaErrore('Selezionare tipo registro iva');", True)
            Dim MyJs1 As String
            MyJs1 = "$(" & Chr(34) & "#" & Txt_DataStampa.ClientID & Chr(34) & ").mask(""99/99/9999""); "
            MyJs1 = MyJs1 & " $(" & Chr(34) & "#" & Txt_DataStampa.ClientID & Chr(34) & ").datepicker({ changeMonth: true,changeYear: true,  yearRange: ""1890:" & Year(Now) + 1 & """},$.datepicker.regional[""it""]);"
            ScriptManager.RegisterStartupScript(Me, Me.GetType(), "FormatDataDal", MyJs1, True)
            Exit Sub
        End If

        If Not IsDate(Txt_DataStampa.Text) Then
            ScriptManager.RegisterClientScriptBlock(Me, Me.GetType(), "Errore", "VisualizzaErrore('Data formalmente errata');", True)

            Dim MyJs1 As String
            MyJs1 = "$(" & Chr(34) & "#" & Txt_DataStampa.ClientID & Chr(34) & ").mask(""99/99/9999""); "
            MyJs1 = MyJs1 & " $(" & Chr(34) & "#" & Txt_DataStampa.ClientID & Chr(34) & ").datepicker({ changeMonth: true,changeYear: true,  yearRange: ""1890:" & Year(Now) + 1 & """},$.datepicker.regional[""it""]);"
            ScriptManager.RegisterStartupScript(Me, Me.GetType(), "FormatDataDal", MyJs1, True)
            Exit Sub
        End If

        k.Leggi(Session("DC_TABELLE"), DD_RegistroIVA.SelectedValue, Txt_Anno.Text)


        k.DataStampa = Txt_DataStampa.Text
        k.UltimaPaginaStampata = Val(Txt_UltimaPagina.Text)

        k.Scrivi(Session("DC_TABELLE"), DD_RegistroIVA.SelectedValue, Txt_Anno.Text)

        Response.Redirect("ElencoRegistriIVA.aspx")
    End Sub

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Trim(Session("UTENTE")) = "" Then
            If Request.ServerVariables("URL").ToUpper.IndexOf("SENIORWEBBLANCO") > 0 Then
                Response.Redirect("/ODV/Login.aspx")
                Exit Sub
            Else
                Response.Redirect("/SeniorWeb/Login.aspx")
                Exit Sub
            End If
        End If

        Dim MyJs As String
        MyJs = "$(" & Chr(34) & "#" & Txt_DataStampa.ClientID & Chr(34) & ").mask(""99/99/9999""); "
        MyJs = MyJs & " $(" & Chr(34) & "#" & Txt_DataStampa.ClientID & Chr(34) & ").datepicker({ changeMonth: true,changeYear: true,  yearRange: ""1890:" & Year(Now) + 1 & """},$.datepicker.regional[""it""]);"
        ScriptManager.RegisterStartupScript(Me, Me.GetType(), "FormatDataDal", MyJs, True)


        If Page.IsPostBack = True Then Exit Sub

        Dim Ks As New Cls_RegistroIVA

        Ks.UpDateDropBox(Session("DC_TABELLE"), DD_RegistroIVA)



        Dim K1 As New Cls_SqlString

        Dim Barra As New Cls_BarraSenior

        If Not IsNothing(Session("RicercaAnagraficaSQLString")) Then
            Try
                K1 = Session("RicercaAnagraficaSQLString")
            Catch ex As Exception

            End Try
        End If

        Lbl_BarraSenior.Text = Barra.CodiceBarra(Application("SENIOR"), Session("UTENTE"), Page, K1)




        Dim k As New Cls_RegistriIVAanno

        If Val(Request.Item("Codcie")) <> 0 Then
            k.Leggi(Session("DC_TABELLE"), Request.Item("Codcie"), Request.Item("Anno"))

            Txt_Anno.Text = Request.Item("Anno")
            DD_RegistroIVA.SelectedValue = Request.Item("Codcie")
            Txt_Anno.Enabled = False
            DD_RegistroIVA.Enabled = False

            Txt_DataStampa.Text = Format(k.DataStampa, "dd/MM/yyyy")
            Txt_UltimaPagina.Text = k.UltimaPaginaStampata



        End If
    End Sub



    Protected Sub Btn_Esci_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles Btn_Esci.Click
        Response.Redirect("ElencoRegistriIVA.aspx")
    End Sub
End Class
