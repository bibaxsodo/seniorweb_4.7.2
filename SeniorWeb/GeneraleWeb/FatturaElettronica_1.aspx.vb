﻿Imports Microsoft.VisualBasic
Imports System.Data.OleDb
Imports System.Web.Hosting

Partial Class GeneraleWeb_FatturaElettronica_1
    Inherits System.Web.UI.Page

    Protected Sub RB_Contratto_CheckedChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles RB_Contratto.CheckedChanged
        If RB_Contratto.Checked = True Then

            lbl_label.Text = "Indica numero contratto"

        End If
    End Sub

    Protected Sub RB_Convezione_CheckedChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles RB_Convezione.CheckedChanged
        If RB_Convezione.Checked = True Then

            lbl_label.Text = "Indica numero commessa convenzione"

        End If
    End Sub

    Protected Sub RB_Ordine_CheckedChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles RB_Ordine.CheckedChanged
        If RB_Ordine.Checked = True Then

            lbl_label.Text = "Indica numero linea ordine acquisto"

        End If
    End Sub

    Protected Sub RB_NonIndicato_CheckedChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles RB_NonIndicato.CheckedChanged
        If RB_NonIndicato.Checked = True Then

            lbl_label.Text = ""

        End If
    End Sub

    Protected Sub Btn_Visualizza_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles Btn_Visualizza.Click
        Dim TestoXML As String
        Dim CodiceDestinatario As String = ""
        Dim CFCOMMITTENTE As String = ""
        Dim DENOMINAZIONECOMMITTENTE As String = ""
        Dim DENOMINAZIONEINDIRIZZO As String = ""
        Dim DENOMINAZIONECAP As String = ""
        Dim DENOMINAZIONECOMUNE As String = ""
        Dim DENOMINAZIONEPROV As String = ""
        Dim CodiceCup As String = ""
        Dim CodiceCig As String = ""
        Dim Iban As String = ""
        Dim IstitutoFinanziario As String = ""
        Dim xModalitaPagamento As String = ""
        Dim ModalitaPagamento As String = ""
        Dim IdDocumento As String = ""
        Dim RiferimentoAmministrazione As String = ""
        Dim NumeroFatturaDDT As Integer = 0
        Dim MaxID As Long
        Dim tipodoc As String
        Dim DataDoc As String
        Dim NumeroDocumento As String
        Dim NumeroRegistrazione As Long
        Dim NumItem As String = ""
        Dim CodiceCommessaConvezione As String = ""
        Dim Allegato As String = ""
        Dim iLen As Integer


        Session("NOMEFILE") = ""
        If FileUpload1.HasFile Then
            iLen = FileUpload1.PostedFile.ContentLength
            Dim btArr(iLen) As Byte

            FileUpload1.PostedFile.InputStream.Read(btArr, 0, iLen)
            Allegato = Convert.ToBase64String(btArr)

            Session("NOMEFILE") = FileUpload1.FileName
        End If

        If Chk_BolloVirtuale.Checked = True Then
            Session("BOLLO") = 1
        End If

        Session("Allegato") = Allegato
        tipodoc = Session("tipodoc")
        DataDoc = Session("DataDoc")
        NumeroDocumento = Session("NumeroDocumento")
        NumeroRegistrazione = Session("NumeroRegistrazione")
        CodiceDestinatario = Session("CodiceDestinatario")
        CFCOMMITTENTE = Session("CFCOMMITTENTE")
        DENOMINAZIONECOMMITTENTE = Session("DENOMINAZIONECOMMITTENTE")
        DENOMINAZIONEINDIRIZZO = Session("DENOMINAZIONEINDIRIZZO")
        DENOMINAZIONECAP = Session("DENOMINAZIONECAP")
        DENOMINAZIONECOMUNE = Session("DENOMINAZIONECOMUNE")
        DENOMINAZIONEPROV = Session("DENOMINAZIONEPROV")
        CodiceCup = Session("CodiceCup")
        CodiceCig = Session("CodiceCig")
        Iban = Session("Iban")
        IstitutoFinanziario = Session("IstitutoFinanziario")
        xModalitaPagamento = Session("xModalitaPagamento")
        ModalitaPagamento = Session("ModalitaPagamento")
        IdDocumento = Session("IdDocumento")
        RiferimentoAmministrazione = Session("RiferimentoAmministrazione")
        NumeroFatturaDDT = Session("NumeroFatturaDDT")


        Response.Cookies("UserSettings")("CAUSALE") = Txt_Causale.Text
        Response.Cookies("UserSettings")("Chk_UsaAnchePIVA") = Chk_UsaAnchePIVA.Checked
        Response.Cookies("UserSettings")("Chk_NoteFattura") = Chk_NoteFattura.Checked

        Response.Cookies("UserSettings")("Chk_BolloVirtuale") = Chk_BolloVirtuale.Checked
        Response.Cookies("UserSettings")("Chk_Competenza") = Chk_Competenza.Checked
        Response.Cookies("UserSettings")("Chk_GiorniInDescrizione") = Chk_GiorniInDescrizione.Checked
        Response.Cookies("UserSettings")("Chk_IndicaMastroPartita") = Chk_IndicaMastroPartita.Checked
        Response.Cookies("UserSettings")("Chk_RaggruppaRicavi") = Chk_RaggruppaRicavi.Checked
        Response.Cookies("UserSettings")("Chk_SoloIniziali") = Chk_SoloIniziali.Checked
        Response.Cookies("UserSettings")("Chk_TipoNumero") = Chk_TipoNumero.Checked
        Response.Cookies("UserSettings")("ChkCentroServizio") = ChkCentroServizio.Checked

        Response.Cookies("UserSettings")("Chk_UsaPIVACF") = Chk_UsaPIVACF.Checked


        Response.Cookies("UserSettings")("Chk_TipoRetta") = Chk_TipoRetta.Checked

        If RB_Contratto.Checked = True Then
            Response.Cookies("UserSettings")("tipo") = "contr"
        End If
        If RB_Ordine.Checked = True Then
            Response.Cookies("UserSettings")("tipo") = "ord"
        End If
        If RB_Convezione.Checked = True Then
            Response.Cookies("UserSettings")("tipo") = "conv"
        End If
        If RB_NonIndicato.Checked = True Then
            Response.Cookies("UserSettings")("tipo") = "nonind"
        End If

        Response.Cookies("UserSettings").Expires = DateTime.Now.AddYears(1)



        If Chk_TipoNumero.Checked = True Then
            Dim DocAp As New Cls_MovimentoContabile

            DocAp.NumeroRegistrazione = NumeroRegistrazione
            DocAp.Leggi(Session("DC_GENERALE"), DocAp.NumeroRegistrazione)
            Dim TipoAp As New Cls_RegistroIVA

            TipoAp.Leggi(Session("DC_TABELLE"), DocAp.RegistroIVA)

            If TipoAp.IndicatoreRegistro <> "" Then
                NumeroDocumento = Trim(TipoAp.IndicatoreRegistro) & Format(Val(DocAp.NumeroProtocollo), "000")
            End If
        Else
            Dim DocAp As New Cls_MovimentoContabile

            DocAp.NumeroRegistrazione = NumeroRegistrazione
            DocAp.Leggi(Session("DC_GENERALE"), DocAp.NumeroRegistrazione)
            Dim TipoAp As New Cls_RegistroIVA

            TipoAp.Leggi(Session("DC_TABELLE"), DocAp.RegistroIVA)

            If TipoAp.IndicatoreRegistro <> "" Then
                NumeroDocumento = DocAp.NumeroProtocollo & "/" & Trim(TipoAp.IndicatoreRegistro)
            End If            
        End If



        If RB_Contratto.Checked = True Then
            NumItem = Txt_Descrizione.Text
        End If
        If RB_Ordine.Checked = True Then
            NumItem = Txt_Descrizione.Text
        End If
        If RB_Convezione.Checked = True Then
            CodiceCommessaConvezione = Txt_Descrizione.Text
        End If

        Dim MovimentiCT As New Cls_MovimentoContabile


        MovimentiCT.NumeroRegistrazione = NumeroRegistrazione
        MovimentiCT.Leggi(Session("DC_GENERALE"), MovimentiCT.NumeroRegistrazione)

        Dim Soc As New Cls_DecodificaSocieta

        Soc.Leggi(Session("DC_TABELLE"))


        If Trim(Soc.IBAN) <> "" Then
            Iban = Trim(Soc.IBAN)
        End If
        'IstitutoFinanziario = CampoSocieta("Banca")

        TestoXML = ""
        REM TestoXML = TestoXML & "<?xml version=""1.0"" encoding=""ISO-8859-1""?>" & vbNewLine
        REM TestoXML = TestoXML & "<p:FatturaElettronica versione=""1.1"" xmlns:ds=""http://www.w3.org/2000/09/xmldsig#"" xmlns:p=""http://www.fatturapa.gov.it/sdi/fatturapa/v1.1"" xmlns:xsi=""http://www.w3.org/2001/XMLSchema-instance"">" & vbNewLine

        'TestoXML = TestoXML & "<?xml-stylesheet type=""text/xsl"" href=""fatturapa_v1.2.xsl""?>"
        'TestoXML = TestoXML & "<p:FatturaElettronica versione=""FPA12"" xmlns:ds=""http://www.w3.org/2000/09/xmldsig#"" xmlns:p=""http://ivaservizi.agenziaentrate.gov.it/docs/xsd/fatture/v1.2"" xmlns:xsi=""http://www.w3.org/2001/XMLSchema-instance"">"

        'TestoXML = TestoXML & "<FatturaElettronicaHeader>" & vbNewLine

        TestoXML = TestoXML & "<?xml version=""1.0"" encoding=""UTF-8""?>" & vbNewLine
        TestoXML = TestoXML & "<p:FatturaElettronica versione=""FPA12"" xmlns:ds=""http://www.w3.org/2000/09/xmldsig"" xmlns:p=""http://ivaservizi.agenziaentrate.gov.it/docs/xsd/fatture/v1.2"" xmlns:xsi=""http://www.w3.org/2001/XMLSchema"">" & vbNewLine
        TestoXML = TestoXML & "<FatturaElettronicaHeader>" & vbNewLine

        TestoXML = TestoXML & "<DatiTrasmissione>" & vbNewLine
        TestoXML = TestoXML & "<IdTrasmittente>" & vbNewLine
        TestoXML = TestoXML & "<IdPaese>IT</IdPaese>" & vbNewLine
        TestoXML = TestoXML & "<IdCodice>" & Soc.CodiceFiscale & "</IdCodice>" & vbNewLine
        TestoXML = TestoXML & "</IdTrasmittente>" & vbNewLine
        TestoXML = TestoXML & "<ProgressivoInvio>" & Txt_Progressivo.Text & "</ProgressivoInvio>" & vbNewLine
        REM TestoXML = TestoXML & "<FormatoTrasmissione>SDI11</FormatoTrasmissione>" & vbNewLine
        TestoXML = TestoXML & "<FormatoTrasmissione>FPA12</FormatoTrasmissione>" & vbNewLine
        TestoXML = TestoXML & "<CodiceDestinatario>" & CodiceDestinatario & "</CodiceDestinatario>" & vbNewLine
        TestoXML = TestoXML & "<ContattiTrasmittente>" & vbNewLine
        TestoXML = TestoXML & "<Telefono>" & Soc.Telefono & "</Telefono>" & vbNewLine
        'TestoXML = TestoXML & "<Email>prova@mail.it</Email>" & vbNewLine
        TestoXML = TestoXML & "</ContattiTrasmittente>" & vbNewLine
        TestoXML = TestoXML & "</DatiTrasmissione>" & vbNewLine
        TestoXML = TestoXML & "<CedentePrestatore>" & vbNewLine
        TestoXML = TestoXML & "<DatiAnagrafici>" & vbNewLine
        TestoXML = TestoXML & "<IdFiscaleIVA>" & vbNewLine
        TestoXML = TestoXML & "<IdPaese>IT</IdPaese>" & vbNewLine
        TestoXML = TestoXML & "<IdCodice>" & Format(Soc.PartitaIVA, "00000000000") & "</IdCodice>" & vbNewLine                                                                     
        TestoXML = TestoXML & "</IdFiscaleIVA>" & vbNewLine
        If Chk_UsaPIVACF.Checked = True Then
            TestoXML = TestoXML & "<CodiceFiscale>" & Soc.CodiceFiscale & "</CodiceFiscale>"
        End If
        TestoXML = TestoXML & "<Anagrafica>" & vbNewLine
        TestoXML = TestoXML & "<Denominazione>" & Soc.RagioneSociale & "</Denominazione>" & vbNewLine
        TestoXML = TestoXML & "</Anagrafica>" & vbNewLine
        TestoXML = TestoXML & "<RegimeFiscale>RF01</RegimeFiscale>" & vbNewLine
        TestoXML = TestoXML & "</DatiAnagrafici>" & vbNewLine
        TestoXML = TestoXML & "<Sede>" & vbNewLine
        TestoXML = TestoXML & "<Indirizzo>" & Soc.Indirizzo & "</Indirizzo>" & vbNewLine
        TestoXML = TestoXML & "<CAP>" & Format(Val(Soc.CAP), "00000") & "</CAP>" & vbNewLine
        TestoXML = TestoXML & "<Comune>" & Soc.Localita & "</Comune>" & vbNewLine
        TestoXML = TestoXML & "<Provincia>" & Soc.Provincia & "</Provincia>" & vbNewLine
        TestoXML = TestoXML & "<Nazione>IT</Nazione>" & vbNewLine
        TestoXML = TestoXML & "</Sede>" & vbNewLine

        If RiferimentoAmministrazione <> "" Then
            TestoXML = TestoXML & "<RiferimentoAmministrazione>" & RiferimentoAmministrazione & "</RiferimentoAmministrazione>" & vbNewLine
        End If

        TestoXML = TestoXML & "</CedentePrestatore>" & vbNewLine
        TestoXML = TestoXML & "<CessionarioCommittente>" & vbNewLine
        TestoXML = TestoXML & "<DatiAnagrafici>" & vbNewLine
        If Chk_UsaPIVACF.Checked = True Then
            TestoXML = TestoXML & "<IdFiscaleIVA>" & vbNewLine
            TestoXML = TestoXML & "<IdPaese>IT</IdPaese>" & vbNewLine
            TestoXML = TestoXML & "<IdCodice>" & Session("PIVACOMMITTENTE") & "</IdCodice>" & vbNewLine
            TestoXML = TestoXML & "</IdFiscaleIVA>" & vbNewLine
        End If
        If CFCOMMITTENTE <> "" Then
            TestoXML = TestoXML & "<CodiceFiscale>" & CFCOMMITTENTE & "</CodiceFiscale>" & vbNewLine
        End If
        TestoXML = TestoXML & "<Anagrafica>" & vbNewLine
        TestoXML = TestoXML & "<Denominazione>" & DENOMINAZIONECOMMITTENTE & "</Denominazione>" & vbNewLine
        TestoXML = TestoXML & "</Anagrafica>" & vbNewLine
        TestoXML = TestoXML & "</DatiAnagrafici>" & vbNewLine
        TestoXML = TestoXML & "<Sede>" & vbNewLine
        TestoXML = TestoXML & "<Indirizzo>" & DENOMINAZIONEINDIRIZZO & "</Indirizzo>" & vbNewLine
        TestoXML = TestoXML & "<CAP>" & DENOMINAZIONECAP & "</CAP>" & vbNewLine
        TestoXML = TestoXML & "<Comune>" & DENOMINAZIONECOMUNE & "</Comune>" & vbNewLine
        TestoXML = TestoXML & "<Provincia>" & DENOMINAZIONEPROV & "</Provincia>" & vbNewLine
        TestoXML = TestoXML & "<Nazione>IT</Nazione>" & vbNewLine
        TestoXML = TestoXML & "</Sede>" & vbNewLine
        TestoXML = TestoXML & "</CessionarioCommittente>" & vbNewLine
        TestoXML = TestoXML & "<SoggettoEmittente>CC</SoggettoEmittente>" & vbNewLine
        TestoXML = TestoXML & "</FatturaElettronicaHeader>" & vbNewLine
        TestoXML = TestoXML & "<FatturaElettronicaBody>" & vbNewLine
        TestoXML = TestoXML & "<DatiGenerali>" & vbNewLine
        TestoXML = TestoXML & "<DatiGeneraliDocumento>" & vbNewLine

        If tipodoc = "NC" Then
            TestoXML = TestoXML & "<TipoDocumento>TD04</TipoDocumento>" & vbNewLine
        Else
            TestoXML = TestoXML & "<TipoDocumento>TD01</TipoDocumento>" & vbNewLine
        End If
        TestoXML = TestoXML & "<Divisa>EUR</Divisa>" & vbNewLine
        TestoXML = TestoXML & "<Data>" & DataDoc & "</Data>" & vbNewLine
        TestoXML = TestoXML & "<Numero>" & NumeroDocumento & "</Numero>" & vbNewLine

        If Session("BOLLO") > 0 Then
            TestoXML = TestoXML & "<DatiBollo>" & vbNewLine
            TestoXML = TestoXML & "<BolloVirtuale>SI</BolloVirtuale>" & vbNewLine
            TestoXML = TestoXML & "<ImportoBollo>2.00</ImportoBollo>" & vbNewLine
            TestoXML = TestoXML & "</DatiBollo>" & vbNewLine
        End If
        TestoXML = TestoXML & "<ImportoTotaleDocumento>" & Replace(Format(Math.Round(Math.Abs(MovimentiCT.ImportoDocumento(Session("DC_TABELLE"))), 2), "0.00"), ",", ".") & "</ImportoTotaleDocumento>" & vbNewLine

        If Trim(Txt_Causale.Text) <> "" Then
            TestoXML = TestoXML & "<Causale>" & Trim(Txt_Causale.Text) & "</Causale>"
        End If

        TestoXML = TestoXML & "</DatiGeneraliDocumento>" & vbNewLine


        If RB_NonIndicato.Checked = True Then
            If IdDocumento <> "" Then                
                ScriptManager.RegisterStartupScript(Me, Me.GetType(), "visualizzaA", "VisualizzaErrore('Hai selezionato [Non Indicare] l ID Documento non sarà inserito nell xml');", True)
                Exit Sub
            End If
            If CodiceCup <> "" Then
                ScriptManager.RegisterStartupScript(Me, Me.GetType(), "visualizzaA", "VisualizzaErrore('Hai selezionato [Non Indicare] il CodiceCup non sarà inserito nell xml');", True)
                Exit Sub
            End If
            If CodiceCig <> "" Then                
                ScriptManager.RegisterStartupScript(Me, Me.GetType(), "visualizzaA", "VisualizzaErrore('Hai selezionato [Non Indicare] il CodiceCig non sarà inserito nell xml');", True)
                Exit Sub
            End If
        End If

        If RB_NonIndicato.Checked = False Then
            If RB_Ordine.Checked = True Then
                TestoXML = TestoXML & "<DatiOrdineAcquisto>" & vbNewLine
            End If
            If RB_Contratto.Checked = True Then
                TestoXML = TestoXML & "<DatiContratto>" & vbNewLine
            End If
            If RB_Convezione.Checked = True Then
                TestoXML = TestoXML & "<DatiConvenzione>" & vbNewLine
            End If

            'TestoXML = TestoXML & "<RiferimentoNumeroLinea>1</RiferimentoNumeroLinea>" & vbNewLine
            If IdDocumento = "" Then
                TestoXML = TestoXML & "<IdDocumento>" & NumeroDocumento & "</IdDocumento>" & vbNewLine
            Else
                TestoXML = TestoXML & "<IdDocumento>" & IdDocumento & "</IdDocumento>" & vbNewLine
            End If

            TestoXML = TestoXML & "<Data>" & Session("IDDataDoc") & "</Data>" & vbNewLine
            'TestoXML = TestoXML & "<NumItem>5</NumItem>" & vbNewLine
            If CodiceCup <> "" Then
                TestoXML = TestoXML & "<CodiceCUP>" & CodiceCup & "</CodiceCUP>" & vbNewLine
            End If
            If CodiceCig <> "" Then
                TestoXML = TestoXML & "<CodiceCIG>" & CodiceCig & "</CodiceCIG>" & vbNewLine
            End If

            If NumItem <> "" Then
                TestoXML = TestoXML & "<NumItem>" & NumItem & "</NumItem>" & vbNewLine
            End If
            If CodiceCommessaConvezione <> "" Then
                TestoXML = TestoXML & "<CodiceCommessaConvenzione>" & CodiceCommessaConvezione & "</CodiceCommessaConvenzione>" & vbNewLine
            End If

            If RB_Convezione.Checked = True Then
                TestoXML = TestoXML & "</DatiConvenzione>" & vbNewLine
            End If
            If RB_Contratto.Checked = True Then
                TestoXML = TestoXML & "</DatiContratto>" & vbNewLine
            End If
            If RB_Ordine.Checked = True Then
                TestoXML = TestoXML & "</DatiOrdineAcquisto>" & vbNewLine
            End If
        End If

        If tipodoc = "NC" Then
            If Txt_RiferimentoNumeroLinea.Text <> "" Or Txt_IDDocumento.Text <> "" Or Txt_NumeroItem.Text <> "" Or _
                Txt_CodiceComeessa.Text <> "" Or TxT_Cup.Text <> "" Or Txt_CodiceCIn.Text <> "" Then
                TestoXML = TestoXML & "<DatiFattureCollegate>" & vbNewLine
                If Txt_RiferimentoNumeroLinea.Text <> "" Then
                    TestoXML = TestoXML & "<RiferimentoNumeroLinea>" & vbNewLine
                    TestoXML = TestoXML & Txt_RiferimentoNumeroLinea.Text & vbNewLine
                    TestoXML = TestoXML & "</RiferimentoNumeroLinea>" & vbNewLine
                End If
                If Txt_IDDocumento.Text <> "" Then
                    TestoXML = TestoXML & "<IdDocumento>" & vbNewLine
                    TestoXML = TestoXML & Txt_IDDocumento.Text & vbNewLine
                    TestoXML = TestoXML & "</IdDocumento>" & vbNewLine
                End If
                If Txt_NumeroItem.Text <> "" Then
                    TestoXML = TestoXML & "<NumItem>" & vbNewLine
                    TestoXML = TestoXML & Txt_NumeroItem.Text & vbNewLine
                    TestoXML = TestoXML & "</NumItem>" & vbNewLine
                End If
                If Txt_CodiceComeessa.Text <> "" Then
                    TestoXML = TestoXML & "<CodiceCommessaConvenzione>" & vbNewLine
                    TestoXML = TestoXML & Txt_CodiceComeessa.Text & vbNewLine
                    TestoXML = TestoXML & "</CodiceCommessaConvenzione>" & vbNewLine
                End If
                If TxT_Cup.Text <> "" Then
                    TestoXML = TestoXML & "<CodiceCUP>" & vbNewLine
                    TestoXML = TestoXML & TxT_Cup.Text & vbNewLine
                    TestoXML = TestoXML & "</CodiceCUP>" & vbNewLine
                End If
                If Txt_CodiceCIn.Text <> "" Then
                    TestoXML = TestoXML & "<CodiceCIG>" & vbNewLine
                    TestoXML = TestoXML & Txt_CodiceCIn.Text & vbNewLine
                    TestoXML = TestoXML & "</CodiceCIG>" & vbNewLine
                End If
                TestoXML = TestoXML & "</DatiFattureCollegate>" & vbNewLine

            End If
        End If

        If NumeroFatturaDDT = 1 Then
            TestoXML = TestoXML & "<DatiDDT>" & vbNewLine
            TestoXML = TestoXML & "<NumeroDDT>" & NumeroDocumento & "</NumeroDDT>"
            TestoXML = TestoXML & "<DataDDT>" & DataDoc & "</DataDDT>"
            TestoXML = TestoXML & "</DatiDDT>" & vbNewLine
        End If

        TestoXML = TestoXML & "<DatiTrasporto>" & vbNewLine
        TestoXML = TestoXML & "</DatiTrasporto>" & vbNewLine
        TestoXML = TestoXML & "</DatiGenerali>" & vbNewLine
        TestoXML = TestoXML & "<DatiBeniServizi>" & vbNewLine

        Dim Indice As Integer
        Dim cn As OleDbConnection
        Dim MySql As String
        Dim Caus As New Cls_CausaleContabile
        Dim Totale As Double

        Dim Modifica As Boolean = False

        cn = New Data.OleDb.OleDbConnection(Session("DC_GENERALE"))

        cn.Open()

        Dim Registrazione As New Cls_MovimentoContabile


        Registrazione.NumeroRegistrazione = Session("NumeroRegistrazione")
        Registrazione.Leggi(Session("DC_GENERALE"), Registrazione.NumeroRegistrazione)


        Caus.Leggi(Session("DC_TABELLE"), Session("CausaleContablie"))


        Indice = 0
        If Caus.Tipo = "R" Then
            MySql = "Select * From MovimentiContabiliRiga Where Numero = " & Session("NumeroRegistrazione") & " And (RigaDaCausale =3 or RigaDaCausale =4 or RigaDaCausale =5 or RigaDaCausale =6 or RigaDaCausale =9 or RigaDaCausale =12) ORDER BY (SELECT TOP 1 dESCRIZIONE FROM  PIANOCONTI WHERE MASTRO = MASTROCONTROPARTITA AND CONTO = CONTOCONTROPARTITA AND SOTTOCONTO = SOTTOCONTOCONTROPARTITA)"
        Else
            MySql = "Select * From MovimentiContabiliRiga Where Numero = " & Session("NumeroRegistrazione") & " And (RigaDaCausale =2 or RigaDaCausale =9 ) ORDER BY (SELECT TOP 1 dESCRIZIONE FROM  PIANOCONTI WHERE MASTRO = MASTROCONTROPARTITA AND CONTO = CONTOCONTROPARTITA AND SOTTOCONTO = SOTTOCONTOCONTROPARTITA)"
        End If
        Dim cmd As New OleDbCommand

        cmd.CommandText = MySql
        cmd.Connection = cn
        Dim myPOSTreader As OleDbDataReader = cmd.ExecuteReader()

        Dim ULTIMAALIQUOTA As Double = 0
        Dim ULTIMANATURA As String = ""


        Do While myPOSTreader.Read()
            Indice = Indice + 1

            Dim MyIvx As New Cls_IVA

            MyIvx.Leggi(Session("DC_TABELLE"), campodb(myPOSTreader.Item("CodiceIva")))

            ULTIMAALIQUOTA = MyIvx.Aliquota
            ULTIMANATURA = MyIvx.Natura


            If Chk_RaggruppaRicavi.Checked = False Then

                TestoXML = TestoXML & "<DettaglioLinee>" & vbNewLine
                TestoXML = TestoXML & "<NumeroLinea>" & Indice & "</NumeroLinea>" & vbNewLine

                Dim DecPianoConti As New Cls_Pianodeiconti

                DecPianoConti.Mastro = campodbN(myPOSTreader.Item("MastroContropartita"))
                DecPianoConti.Conto = campodbN(myPOSTreader.Item("ContoContropartita"))
                DecPianoConti.Sottoconto = campodbN(myPOSTreader.Item("SottocontoContropartita"))
                DecPianoConti.Decodfica(Session("DC_GENERALE"))

                Dim MyIva As New Cls_IVA

                MyIva.Leggi(Session("DC_TABELLE"), campodb(myPOSTreader.Item("CodiceIva")))

                ULTIMAALIQUOTA = MyIva.Aliquota
                ULTIMANATURA = MyIva.Natura


                Dim AppoggioDescrizione As String = ""
                If Session("RIGANOTE") <> "" Then
                    AppoggioDescrizione = " " & Session("RIGANOTE")
                End If
                If Chk_IndicaMastroPartita.Checked = True And MyIva.Natura <> "N1" Then
                    Dim DecPianoContiPartita As New Cls_Pianodeiconti

                    DecPianoContiPartita.Mastro = campodbN(myPOSTreader.Item("MastroContropartita"))
                    DecPianoContiPartita.Conto = campodbN(myPOSTreader.Item("ContoContropartita"))
                    DecPianoContiPartita.Sottoconto = campodbN(myPOSTreader.Item("SottocontoContropartita"))
                    DecPianoContiPartita.Decodfica(Session("DC_GENERALE"))
                    AppoggioDescrizione = AppoggioDescrizione & " " & DecPianoContiPartita.Descrizione
                End If
                If Chk_Competenza.Checked = True And MyIva.Natura <> "N1" Then
                    AppoggioDescrizione = AppoggioDescrizione & " competenza : " & Session("PERIODO")
                End If
                If ChkCentroServizio.Checked = True And MyIva.Natura <> "N1" Then
                    Dim AppoServ As New Cls_CentroServizio

                    AppoServ.Leggi(Session("DC_OSPITE"), MovimentiCT.CentroServizio)
                    AppoggioDescrizione = AppoggioDescrizione & " " & AppoServ.DESCRIZIONE
                End If


                If Chk_GiorniInDescrizione.Visible = True Then
                    If campodbN(myPOSTreader.Item("Quantita")) > 0 Then
                        AppoggioDescrizione = AppoggioDescrizione & " Giorni " & campodbN(myPOSTreader.Item("Quantita"))
                    End If
                End If

                If Chk_TipoRetta.Checked = True Then
                    If campodbN(myPOSTreader.Item("Quantita")) > 0 Then
                        Dim K As New Cls_rettatotale

                        K.CODICEOSPITE = Int(campodbN(myPOSTreader.Item("SottocontoContropartita")) / 100)
                        K.UltimaData(Session("DC_OSPITE"), K.CODICEOSPITE, Registrazione.CentroServizio)

                        If K.TipoRetta <> "" Then
                            Dim MyTipoRetta As New Cls_TipoRetta

                            MyTipoRetta.Codice = K.TipoRetta
                            MyTipoRetta.Leggi(Session("DC_OSPITE"), MyTipoRetta.Codice)

                            AppoggioDescrizione = AppoggioDescrizione & " " & MyTipoRetta.Descrizione
                        End If
                    End If
                End If

                Dim Appoggio As String = ""


                If Chk_SoloIniziali.Checked = False Then
                    If MyIva.Natura = "N1" Then
                        TestoXML = TestoXML & "<Descrizione>" & campodb(myPOSTreader.Item("Descrizione")) & "</Descrizione>" & vbNewLine
                    Else
                        TestoXML = TestoXML & "<Descrizione>" & campodb(myPOSTreader.Item("Descrizione")) & Appoggio & AppoggioDescrizione & " " & DecPianoConti.Descrizione & "</Descrizione>" & vbNewLine
                    End If
                Else
                    Dim Cognome As String
                    Dim Nome As String

                    Dim Ospite As New ClsOspite

                    Ospite.CodiceOspite = Int(campodbN(myPOSTreader.Item("SottocontoContropartita")) / 100)
                    Ospite.Leggi(Session("DC_OSPITE"), Ospite.CodiceOspite)


                    Cognome = Trim(Ospite.CognomeOspite)
                    Nome = Trim(Ospite.Nome)



                    If MyIva.Natura = "N1" Then
                        TestoXML = TestoXML & "<Descrizione>" & campodb(myPOSTreader.Item("Descrizione")) & "</Descrizione>" & vbNewLine
                    Else
                        TestoXML = TestoXML & "<Descrizione>" & campodb(myPOSTreader.Item("Descrizione")) & Appoggio & AppoggioDescrizione & " " & Mid(Cognome & Space(4), 1, 1) & ". " & Mid(Nome & Space(4), 1, 1) & ". " & "</Descrizione>" & vbNewLine
                    End If

                End If


                Dim negativo As String = ""
                If tipodoc = "NC" Then
                    If campodb(myPOSTreader.Item("DareAvere")) = "A" Then
                        negativo = "-"
                    End If
                Else
                    If campodb(myPOSTreader.Item("DareAvere")) = "D" Then
                        negativo = "-"
                    End If
                End If

                'If campodbN(myPOSTreader.Item("Quantita")) > 0 Then
                '    TestoXML = TestoXML & "<Quantita>" & Replace(Format(campodbN(myPOSTreader.Item("Quantita")), "0.00"), ",", ".") & "</Quantita>" & vbNewLine
                '    TestoXML = TestoXML & "<PrezzoUnitario>" & negativo & Replace(Format(Math.Round(campodb(myPOSTreader.Item("Importo")) / campodb(myPOSTreader.Item("Quantita")), 2), "0.00"), ",", ".") & "</PrezzoUnitario>" & vbNewLine
                'Else
                TestoXML = TestoXML & "<Quantita>1.00</Quantita>" & vbNewLine

                TestoXML = TestoXML & "<PrezzoUnitario>" & negativo & Replace(Format(Math.Round(campodbN(myPOSTreader.Item("Importo")), 2), "0.00"), ",", ".") & "</PrezzoUnitario>" & vbNewLine
                'End If

                TestoXML = TestoXML & "<PrezzoTotale>" & negativo & Replace(Format(Math.Round(campodbN(myPOSTreader.Item("Importo")), 2), "0.00"), ",", ".") & "</PrezzoTotale>" & vbNewLine
                'If DecodificaIvaCampo(MyRs!CodiceIva, "NATURA") = "" Then


                TestoXML = TestoXML & "<AliquotaIVA>" & Replace(Format(MyIva.Aliquota * 100, "0.00"), ",", ".") & "</AliquotaIVA>" & vbNewLine
                'End If
                If MyIva.Natura <> "" Then
                    TestoXML = TestoXML & "<Natura>" & MyIva.Natura & "</Natura>" & vbNewLine
                End If

                ULTIMAALIQUOTA = MyIva.Aliquota
                ULTIMANATURA = MyIva.Natura

                TestoXML = TestoXML & "</DettaglioLinee>" & vbNewLine
            End If
        Loop
        myPOSTreader.Close()


        If Chk_RaggruppaRicavi.Checked = True Then
            Totale = 0
            Indice = 0

            If Chk_NoteFattura.Checked = True Then
                Dim Appoggio As String = ""

                Dim AppoggioCentroServizio As String = ""

                If Chk_Competenza.Checked = True Then
                    Indice = Indice + 1

                    TestoXML = TestoXML & "<DettaglioLinee>" & vbNewLine
                    TestoXML = TestoXML & "<NumeroLinea>" & Indice & "</NumeroLinea>" & vbNewLine
                    TestoXML = TestoXML & "<Descrizione>Competenza : " & Session("PERIODO") & "</Descrizione>"
                    TestoXML = TestoXML & "<Quantita>0.00</Quantita>"
                    TestoXML = TestoXML & "<PrezzoUnitario>0.00</PrezzoUnitario>"
                    TestoXML = TestoXML & "<PrezzoTotale>0.00</PrezzoTotale>"
                    TestoXML = TestoXML & "<AliquotaIVA>" & Replace(Format(ULTIMAALIQUOTA * 100, "0.00"), ",", ".") & "</AliquotaIVA>" & vbNewLine

                    If ULTIMANATURA <> "" Then
                        TestoXML = TestoXML & "<Natura>" & ULTIMANATURA & "</Natura>" & vbNewLine
                    End If

                    TestoXML = TestoXML & "</DettaglioLinee>"
                End If

                If ChkCentroServizio.Checked = True Then
                    Dim AppoServ As New Cls_CentroServizio

                    AppoServ.Leggi(Session("DC_OSPITE"), MovimentiCT.CentroServizio)

                    TestoXML = TestoXML & "<DettaglioLinee>" & vbNewLine
                    TestoXML = TestoXML & "<NumeroLinea>" & Indice & "</NumeroLinea>" & vbNewLine
                    TestoXML = TestoXML & "<Descrizione>Servizio : " & AppoServ.DESCRIZIONE & "</Descrizione>"
                    TestoXML = TestoXML & "<Quantita>0.00</Quantita>"
                    TestoXML = TestoXML & "<PrezzoUnitario>0.00</PrezzoUnitario>"
                    TestoXML = TestoXML & "<PrezzoTotale>0.00</PrezzoTotale>"
                    TestoXML = TestoXML & "<AliquotaIVA>" & Replace(Format(ULTIMAALIQUOTA * 100, "0.00"), ",", ".") & "</AliquotaIVA>" & vbNewLine

                    If ULTIMANATURA <> "" Then
                        TestoXML = TestoXML & "<Natura>" & ULTIMANATURA & "</Natura>" & vbNewLine
                    End If


                    TestoXML = TestoXML & "</DettaglioLinee>"
                End If


                If Not IsNothing(Session("NoteFattura")) Then
                    Appoggio = Session("NoteFattura").ToString.Replace(vbNewLine, "")
                End If
                Dim LineaDes As String = 0
                Dim I1 As Integer
                Dim N1 As Integer = 0
                LineaDes = ""
                For I1 = 1 To Appoggio.Length - 1
                    If Mid(Appoggio, I1, 1) = " " And N1 > 60 Then
                        Indice = Indice + 1

                        TestoXML = TestoXML & "<DettaglioLinee>" & vbNewLine
                        TestoXML = TestoXML & "<NumeroLinea>" & Indice & "</NumeroLinea>" & vbNewLine
                        TestoXML = TestoXML & "<Descrizione>" & LineaDes & "</Descrizione>"
                        TestoXML = TestoXML & "<Quantita>0.00</Quantita>"
                        TestoXML = TestoXML & "<PrezzoUnitario>0.00</PrezzoUnitario>"
                        TestoXML = TestoXML & "<PrezzoTotale>0.00</PrezzoTotale>"
                        TestoXML = TestoXML & "<AliquotaIVA>" & Replace(Format(ULTIMAALIQUOTA * 100, "0.00"), ",", ".") & "</AliquotaIVA>" & vbNewLine
                        If ULTIMANATURA <> "" Then
                            TestoXML = TestoXML & "<Natura>" & ULTIMANATURA & "</Natura>" & vbNewLine
                        End If
                        TestoXML = TestoXML & "</DettaglioLinee>"

                        LineaDes = ""
                        N1 = 0
                    Else
                        N1 = N1 + 1
                        LineaDes = LineaDes & Mid(Appoggio, I1, 1)
                    End If
                Next
                If LineaDes <> "" Then
                    Indice = Indice + 1

                    TestoXML = TestoXML & "<DettaglioLinee>" & vbNewLine
                    TestoXML = TestoXML & "<NumeroLinea>" & Indice & "</NumeroLinea>" & vbNewLine
                    TestoXML = TestoXML & "<Descrizione>" & LineaDes & "</Descrizione>"
                    TestoXML = TestoXML & "<Quantita>0.00</Quantita>"
                    TestoXML = TestoXML & "<PrezzoUnitario>0.00</PrezzoUnitario>"
                    TestoXML = TestoXML & "<PrezzoTotale>0.00</PrezzoTotale>"
                    TestoXML = TestoXML & "<AliquotaIVA>" & Replace(Format(ULTIMAALIQUOTA * 100, "0.00"), ",", ".") & "</AliquotaIVA>" & vbNewLine
                    If ULTIMANATURA <> "" Then
                        TestoXML = TestoXML & "<Natura>" & ULTIMANATURA & "</Natura>" & vbNewLine
                    End If
                    TestoXML = TestoXML & "</DettaglioLinee>"
                End If
            End If


            If Caus.Tipo = "R" Then
                MySql = "Select * From MovimentiContabiliRiga Where Numero = " & Session("NumeroRegistrazione") & " And RigaDaCausale =2 "
            Else
                MySql = "Select * From MovimentiContabiliRiga Where Numero = " & Session("NumeroRegistrazione") & " And RigaDaCausale = 3 "
            End If

            Dim cmd_Rag As New OleDbCommand

            cmd_Rag.CommandText = MySql
            cmd_Rag.Connection = cn
            Dim ReadRS_RAG As OleDbDataReader = cmd_Rag.ExecuteReader()

            Do While ReadRS_RAG.Read()

                Indice = Indice + 1

                Dim MyIva As New Cls_IVA

                MyIva.Leggi(Session("DC_TABELLE"), campodb(ReadRS_RAG.Item("CodiceIVA")))

                ULTIMAALIQUOTA = MyIva.Aliquota
                ULTIMANATURA = MyIva.Natura

                TestoXML = TestoXML & "<DettaglioLinee>" & vbNewLine
                TestoXML = TestoXML & "<NumeroLinea>" & Indice & "</NumeroLinea>" & vbNewLine

                Dim AppoggioCentroServizio As String = ""
                If ChkCentroServizio.Checked = True And MyIva.Natura <> "N1" Then
                    Dim AppoServ As New Cls_CentroServizio

                    AppoServ.Leggi(Session("DC_OSPITE"), MovimentiCT.CentroServizio)
                    AppoggioCentroServizio = " " & AppoServ.DESCRIZIONE
                End If

                If Chk_Competenza.Checked = True Then

                    TestoXML = TestoXML & "<Descrizione>Rette  competenza : " & Session("PERIODO") & AppoggioCentroServizio & "</Descrizione>" & vbNewLine
                Else
                    TestoXML = TestoXML & "<Descrizione>Rette" & AppoggioCentroServizio & "</Descrizione>" & vbNewLine
                End If


                TestoXML = TestoXML & "<Quantita>1.00</Quantita>" & vbNewLine

                Dim negativo As String = ""
                If tipodoc = "NC" Then
                    If campodb(ReadRS_RAG.Item("DareAvere")) = "A" Then
                        negativo = "-"
                    End If
                Else
                    If campodb(ReadRS_RAG.Item("DareAvere")) = "D" Then
                        negativo = "-"
                    End If
                End If

                TestoXML = TestoXML & "<PrezzoUnitario>" & negativo & Replace(Format(Math.Round(campodbN(ReadRS_RAG.Item("Imponibile")), 2), "0.00"), ",", ".") & "</PrezzoUnitario>" & vbNewLine
                TestoXML = TestoXML & "<PrezzoTotale>" & negativo & Replace(Format(Math.Round(campodbN(ReadRS_RAG.Item("Imponibile")), 2), "0.00"), ",", ".") & "</PrezzoTotale>" & vbNewLine



                TestoXML = TestoXML & "<AliquotaIVA>" & Replace(Format(MyIva.Aliquota * 100, "0.00"), ",", ".") & "</AliquotaIVA>" & vbNewLine
                If MyIva.Natura <> "" Then
                    TestoXML = TestoXML & "<Natura>" & MyIva.Natura & "</Natura>" & vbNewLine
                End If
                TestoXML = TestoXML & "</DettaglioLinee>" & vbNewLine
            Loop
            ReadRS_RAG.Close()
        Else
            If Chk_NoteFattura.Checked = True Then
                Dim Appoggio As String = ""


                If Not IsNothing(Session("NoteFattura")) Then
                    Appoggio = Session("NoteFattura").ToString.Replace(vbNewLine, "")
                End If
                Dim LineaDes As String = 0
                Dim I1 As Integer
                Dim N1 As Integer = 0
                LineaDes = ""
                For I1 = 1 To Appoggio.Length - 1
                    If Mid(Appoggio, I1, 1) = " " And N1 > 60 Then
                        Indice = Indice + 1

                        TestoXML = TestoXML & "<DettaglioLinee>" & vbNewLine
                        TestoXML = TestoXML & "<NumeroLinea>" & Indice & "</NumeroLinea>" & vbNewLine
                        TestoXML = TestoXML & "<Descrizione>" & LineaDes & "</Descrizione>"
                        TestoXML = TestoXML & "<Quantita>0.00</Quantita>"
                        TestoXML = TestoXML & "<PrezzoUnitario>0.00</PrezzoUnitario>"
                        TestoXML = TestoXML & "<PrezzoTotale>0.00</PrezzoTotale>"
                        TestoXML = TestoXML & "<AliquotaIVA>" & Replace(Format(ULTIMAALIQUOTA * 100, "0.00"), ",", ".") & "</AliquotaIVA>" & vbNewLine
                        'If ULTIMANATURA <> "" Then
                        TestoXML = TestoXML & "<Natura>N4</Natura>" & vbNewLine
                        'End If
                        TestoXML = TestoXML & "</DettaglioLinee>"

                        LineaDes = ""
                        N1 = 0
                    Else
                        N1 = N1 + 1
                        LineaDes = LineaDes & Mid(Appoggio, I1, 1)
                    End If
                Next
                If LineaDes <> "" Then
                    Indice = Indice + 1

                    TestoXML = TestoXML & "<DettaglioLinee>" & vbNewLine
                    TestoXML = TestoXML & "<NumeroLinea>" & Indice & "</NumeroLinea>" & vbNewLine
                    TestoXML = TestoXML & "<Descrizione>" & LineaDes & "</Descrizione>"
                    TestoXML = TestoXML & "<Quantita>0.00</Quantita>"
                    TestoXML = TestoXML & "<PrezzoUnitario>0.00</PrezzoUnitario>"
                    TestoXML = TestoXML & "<PrezzoTotale>0.00</PrezzoTotale>"
                    TestoXML = TestoXML & "<AliquotaIVA>" & Replace(Format(ULTIMAALIQUOTA * 100, "0.00"), ",", ".") & "</AliquotaIVA>" & vbNewLine
                    If ULTIMANATURA <> "" Then
                        TestoXML = TestoXML & "<Natura>N4</Natura>" & vbNewLine
                    End If
                    TestoXML = TestoXML & "</DettaglioLinee>"
                End If
            End If
        End If


        Totale = 0

        If Caus.Tipo = "R" Then
            MySql = "Select * From MovimentiContabiliRiga Where Numero = " & Session("NumeroRegistrazione") & " And RigaDaCausale =2 "
        Else
            MySql = "Select * From MovimentiContabiliRiga Where Numero = " & Session("NumeroRegistrazione") & " And RigaDaCausale = 3 "
        End If

        Dim In4 As Integer = 0
        Dim TipoN4 As String = ""

        Dim cmd1 As New OleDbCommand

        cmd1.CommandText = MySql
        cmd1.Connection = cn
        Dim ReadRS As OleDbDataReader = cmd1.ExecuteReader()

        Do While ReadRS.Read()
            TestoXML = TestoXML & "<DatiRiepilogo>" & vbNewLine

            Dim MyIva As New Cls_IVA

            MyIva.Leggi(Session("DC_TABELLE"), campodb(ReadRS.Item("CodiceIVA")))

            TestoXML = TestoXML & "<AliquotaIVA>" & Replace(Format(MyIva.Aliquota * 100, "0.00"), ",", ".") & "</AliquotaIVA>" & vbNewLine
            If MyIva.Natura.Trim <> "" Then
                TestoXML = TestoXML & "<Natura>" & MyIva.Natura & "</Natura>" & vbNewLine
            End If
            If MyIva.Natura = "N4" Then
                In4 = 1
            End If
            Dim negativo As String = ""
            If tipodoc = "NC" Then
                If campodb(ReadRS.Item("DareAvere")) = "A" Then
                    negativo = "-"
                End If
            Else
                If campodb(ReadRS.Item("DareAvere")) = "D" Then
                    negativo = "-"
                End If
            End If

            TestoXML = TestoXML & "<ImponibileImporto>" & negativo & Replace(Format(Math.Round(campodbN(ReadRS.Item("Imponibile")), 2), "0.00"), ",", ".") & "</ImponibileImporto>" & vbNewLine
            TestoXML = TestoXML & "<Imposta>" & negativo & Replace(Format(Math.Round(campodbN(ReadRS.Item("Importo")), 2), "0.00"), ",", ".") & "</Imposta>" & vbNewLine

            If campodbN(ReadRS.Item("Importo")) > 0 Then
                TestoXML = TestoXML & "<EsigibilitaIVA>S</EsigibilitaIVA>"
            End If
            If MyIva.Natura = "N1" Then
                TestoXML = TestoXML & "<RiferimentoNormativo>Imposta di bollo assolta in modo virtuale ai sensi del D.M. 17/6/2014</RiferimentoNormativo>" & vbNewLine
            Else
                TestoXML = TestoXML & "<RiferimentoNormativo>" & MyIva.Descrizione & "</RiferimentoNormativo>" & vbNewLine
            End If
            If MyIva.Natura = "N4" Then
                TipoN4 = "S"
            End If
            TestoXML = TestoXML & "</DatiRiepilogo>" & vbNewLine
            'Round(MoveFromDb(MyRs!Importo), 2) +

            If tipodoc = "NC" Then
                If campodb(ReadRS.Item("DareAvere")) = "A" Then
                    Totale = Totale - Math.Round(campodbN(ReadRS.Item("Imponibile")), 2)
                Else
                    Totale = Totale + Math.Round(campodbN(ReadRS.Item("Imponibile")), 2)
                End If
            Else
                If campodb(ReadRS.Item("DareAvere")) = "D" Then
                    Totale = Totale - Math.Round(campodbN(ReadRS.Item("Imponibile")), 2)
                Else
                    Totale = Totale + Math.Round(campodbN(ReadRS.Item("Imponibile")), 2)
                End If
            End If
        Loop
        ReadRS.Close()



        'If Chk_NoteFattura.Checked = True And TipoN4 = "" Then
        '    TestoXML = TestoXML & "<DatiRiepilogo>" & vbNewLine
        '    TestoXML = TestoXML & "<AliquotaIVA>0</AliquotaIVA>" & vbNewLine
        '    TestoXML = TestoXML & "<Natura>N4</Natura>" & vbNewLine
        '    TestoXML = TestoXML & "<ImponibileImporto>" & 0 & "</ImponibileImporto>"
        '    TestoXML = TestoXML & "<Imposta>" & 0 & "</Imposta>"
        '    TestoXML = TestoXML & "<EsigibilitaIVA>S</EsigibilitaIVA>"
        '    TestoXML = TestoXML & "<RiferimentoNormativo></RiferimentoNormativo>" & vbNewLine
        '    TestoXML = TestoXML & "</DatiRiepilogo>" & vbNewLine
        'End If

        TestoXML = TestoXML & "</DatiBeniServizi>" & vbNewLine
        TestoXML = TestoXML & "<DatiPagamento>" & vbNewLine
        TestoXML = TestoXML & "<CondizioniPagamento>TP02</CondizioniPagamento>" & vbNewLine
        TestoXML = TestoXML & "<DettaglioPagamento>" & vbNewLine

        If ModalitaPagamento = "" Then
            ModalitaPagamento = "MP05"
        End If
        If Server.MachineName.ToUpper = "COP64" Then
            ModalitaPagamento = "MP15"
        End If

        TestoXML = TestoXML & "<ModalitaPagamento>" & ModalitaPagamento & "</ModalitaPagamento>" & vbNewLine


        Dim cmd2 As New OleDbCommand

        cmd2.CommandText = "Select * From Scadenzario where NumeroRegistrazioneContabile = " & NumeroRegistrazione
        cmd2.Connection = cn
        Dim ReadRS2 As OleDbDataReader = cmd2.ExecuteReader()
        If ReadRS2.Read Then
            TestoXML = TestoXML & "<DataScadenzaPagamento>" & Format(ReadRS2.Item("DataScadenza"), "yyyy-MM-dd") & "</DataScadenzaPagamento>" & vbNewLine
        End If
        ReadRS2.Close()

        If tipodoc = "NC" Then
            TestoXML = TestoXML & "<ImportoPagamento>" & Replace(Format(Math.Abs(Math.Round(Totale, 2)), "0.00"), ",", ".") & "</ImportoPagamento>" & vbNewLine
        Else
            TestoXML = TestoXML & "<ImportoPagamento>" & Replace(Format(Math.Round(Totale, 2), "0.00"), ",", ".") & "</ImportoPagamento>" & vbNewLine
        End If
        If ModalitaPagamento = "MP05" Then
            TestoXML = TestoXML & "<IBAN>" & Iban & "</IBAN>" & vbNewLine
        End If


        If Server.MachineName.ToUpper = "COP64" Then
            TestoXML = TestoXML & "<IstitutoFinanziario>Banca D'Italia</IstitutoFinanziario>"
            TestoXML = TestoXML & "<CodicePagamento>0062618</CodicePagamento>" & vbNewLine
        End If

        TestoXML = TestoXML & "</DettaglioPagamento>" & vbNewLine
        TestoXML = TestoXML & "</DatiPagamento>" & vbNewLine
        If Trim(FileUpload1.FileName) <> "" Then
            TestoXML = TestoXML & "<Allegati></Allegati>" & vbNewLine
        End If
        TestoXML = TestoXML & "</FatturaElettronicaBody>" & vbNewLine
        TestoXML = TestoXML & "</p:FatturaElettronica>" & vbNewLine


        Dim cmdw As New OleDbCommand

        cmdw.CommandText = "UPDATE FatturaElettronica Set XMLOriginale  =?,Progressivo= ?    Where NumeroRegistrazione = ?"
        cmdw.Connection = cn
        cmdw.Parameters.AddWithValue("@XMLOriginale", TestoXML)
        cmdw.Parameters.AddWithValue("@Progressivo", Txt_Progressivo.Text)
        cmdw.Parameters.AddWithValue("@NumeroRegistrazione", Session("NumeroRegistrazione"))
        cmdw.ExecuteNonQuery()

        Session("TestoXML") = TestoXML


        Session("Progressivo") = Txt_Progressivo.Text

        Response.Redirect("FatturaElettronica_2.aspx")
    End Sub


    Function campodbN(ByVal oggetto As Object) As Double


        If IsDBNull(oggetto) Then
            Return 0
        Else
            Return oggetto
        End If
    End Function


    Function campodb(ByVal oggetto As Object) As String


        If IsDBNull(oggetto) Then
            Return ""
        Else
            Return oggetto
        End If
    End Function

    Protected Sub GeneraleWeb_FatturaElettronica_1_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load

        Dim K1 As New Cls_SqlString

        Dim Barra As New Cls_BarraSenior

        If Not IsNothing(Session("RicercaGeneraleSQLString")) Then
            K1 = Session("RicercaGeneraleSQLString")
        End If

        Lbl_BarraSenior.Text = Barra.CodiceBarra(Application("SENIOR"), Session("UTENTE"), Page, K1)

        If Page.IsPostBack = True Then Exit Sub

        If (Request.Cookies("UserSettings") IsNot Nothing) Then            
            If (Request.Cookies("UserSettings")("Chk_UsaAnchePIVA") IsNot Nothing) Then
                If Request.Cookies("UserSettings")("Chk_UsaAnchePIVA").ToUpper = "true".ToUpper Then
                    Chk_UsaAnchePIVA.Checked = True
                Else
                    'Chk_UsaAnchePIVA.Checked = False
                End If
            End If
            If (Request.Cookies("UserSettings")("Chk_NoteFattura") IsNot Nothing) Then
                If Request.Cookies("UserSettings")("Chk_NoteFattura").ToUpper = "true".ToUpper Then
                    Chk_NoteFattura.Checked = True
                Else
                    Chk_NoteFattura.Checked = False
                End If
            End If
            If (Request.Cookies("UserSettings")("Chk_BolloVirtuale") IsNot Nothing) Then
                If Request.Cookies("UserSettings")("Chk_BolloVirtuale").ToUpper = "true".ToUpper Then
                    Chk_BolloVirtuale.Checked = True
                Else
                    Chk_BolloVirtuale.Checked = False
                End If
            End If
            If (Request.Cookies("UserSettings")("Chk_Competenza") IsNot Nothing) Then
                If Request.Cookies("UserSettings")("Chk_Competenza").ToUpper = "true".ToUpper Then
                    Chk_Competenza.Checked = True
                Else
                    Chk_Competenza.Checked = False
                End If
            End If

            If (Request.Cookies("UserSettings")("Chk_GiorniInDescrizione") IsNot Nothing) Then
                If Request.Cookies("UserSettings")("Chk_GiorniInDescrizione").ToUpper = "true".ToUpper Then
                    Chk_GiorniInDescrizione.Checked = True
                Else
                    Chk_GiorniInDescrizione.Checked = False
                End If
            End If

            If (Request.Cookies("UserSettings")("Chk_IndicaMastroPartita") IsNot Nothing) Then
                If Request.Cookies("UserSettings")("Chk_IndicaMastroPartita").ToUpper = "true".ToUpper Then
                    Chk_IndicaMastroPartita.Checked = True
                Else
                    Chk_IndicaMastroPartita.Checked = False
                End If
            End If

            If (Request.Cookies("UserSettings")("Chk_RaggruppaRicavi") IsNot Nothing) Then
                If Request.Cookies("UserSettings")("Chk_RaggruppaRicavi").ToUpper = "true".ToUpper Then
                    Chk_RaggruppaRicavi.Checked = True
                Else
                    Chk_RaggruppaRicavi.Checked = False
                End If
            End If


            If (Request.Cookies("UserSettings")("Chk_SoloIniziali") IsNot Nothing) Then
                If Request.Cookies("UserSettings")("Chk_SoloIniziali").ToUpper = "true".ToUpper Then
                    Chk_SoloIniziali.Checked = True
                Else
                    Chk_SoloIniziali.Checked = False
                End If
            End If


            If (Request.Cookies("UserSettings")("Chk_TipoNumero") IsNot Nothing) Then
                If Request.Cookies("UserSettings")("Chk_TipoNumero").ToUpper = "true".ToUpper Then
                    Chk_TipoNumero.Checked = True
                Else
                    Chk_TipoNumero.Checked = False
                End If
            End If


            If (Request.Cookies("UserSettings")("ChkCentroServizio") IsNot Nothing) Then
                If Request.Cookies("UserSettings")("ChkCentroServizio").ToUpper = "true".ToUpper Then
                    ChkCentroServizio.Checked = True
                Else
                    ChkCentroServizio.Checked = False
                End If
            End If

            If (Request.Cookies("UserSettings")("Chk_TipoRetta") IsNot Nothing) Then
                If Request.Cookies("UserSettings")("Chk_TipoRetta").ToUpper = "true".ToUpper Then
                    Chk_TipoRetta.Checked = True
                Else
                    Chk_TipoRetta.Checked = False
                End If
            End If


            If (Request.Cookies("UserSettings")("Chk_UsaPIVACF") IsNot Nothing) Then
                If Request.Cookies("UserSettings")("Chk_UsaPIVACF").ToUpper = "true".ToUpper Then
                    Chk_UsaPIVACF.Checked = True
                Else
                    Chk_UsaPIVACF.Checked = False
                End If
            End If            

        End If

        RB_NonIndicato.Checked = True
        If Session("TIPINVIO") = "O" Then
            RB_Ordine.Checked = True
            RB_Contratto.Checked = False
            RB_Ordine.Checked = False
            RB_Convezione.Checked = False
            RB_NonIndicato.Checked = False
        End If

        If Session("TIPINVIO") = "C" Then
            RB_Ordine.Checked = False
            RB_Contratto.Checked = True
            RB_Ordine.Checked = False
            RB_Convezione.Checked = False
            RB_NonIndicato.Checked = False
        End If

        If Session("TIPINVIO") = "Z" Then
            RB_Ordine.Checked = False
            RB_Contratto.Checked = False
            RB_Ordine.Checked = False
            RB_Convezione.Checked = True
            RB_NonIndicato.Checked = False
        End If

        If RB_NonIndicato.Checked = True Then

            lbl_label.Text = ""

        End If

        If RB_Ordine.Checked = True Then

            lbl_label.Text = "Indica numero linea ordine acquisto"

        End If

        If RB_Convezione.Checked = True Then

            lbl_label.Text = "Indica numero commessa convenzione"

        End If

        If RB_Contratto.Checked = True Then

            lbl_label.Text = "Indica numero contratto"

        End If

        Try
            If (Request.Cookies("UserSettings")("CAUSALE") IsNot Nothing) Then
                Txt_Causale.Text = Request.Cookies("UserSettings")("CAUSALE")
            End If

        Catch ex As Exception

        End Try

        Txt_Progressivo.Text = Session("Progressivo")
    End Sub
End Class
