﻿Imports Microsoft.VisualBasic
Imports System.Data.OleDb
Imports System.Web.Hosting
Imports System.Web.Script.Serialization

Partial Class incassipagamenti
    Inherits System.Web.UI.Page
    Dim Tabella As New System.Data.DataTable("tabella")
 

    Private Sub PienaGriglia()
        Dim Xs As New Cls_CausaleContabile
        Dim DareAvereIncPag As String
        Dim CausaleAcqVend As String
        Dim DareAvere As String
        Dim MySql As String
        Dim Mastro As Long
        Dim Conto As Long
        Dim Sottoconto As Long
        Dim Importo As Double
        Dim DocumentoPagato As Double
        Dim DataDocumento As String = ""
        Dim NumeroDocumento As String = ""
        Dim CausaleContabile As String
        Dim NumeroRegistrazione As Long


        Xs.Leggi(Session("DC_TABELLE"), Dd_CausaleContabile.SelectedValue)
        DareAvereIncPag = ""
        If Not IsNothing(Xs.Righe(0)) Then
            DareAvereIncPag = Xs.Righe(0).DareAvere
        End If
        CausaleAcqVend = Xs.VenditaAcquisti
        If DareAvereIncPag = "D" Then
            DareAvere = "A"
        Else
            DareAvere = "D"
        End If

        Dim Vettore(100) As String
        Vettore = SplitWords(Txt_ClienteFornitore.Text)

        If Vettore.Length < 2 Then
            Exit Sub
        End If

        Try
            Mastro = Val(Vettore(0))
            Conto = Val(Vettore(1))
            Sottoconto = Val(Vettore(2))
        Catch ex As Exception

        End Try

        If Mastro = 0 Or Conto = 0 Or Sottoconto = 0 Then
            Exit Sub
        End If


        MySql = "SELECT (SELECT SUM(IMPORTO) FROM TABELLALEGAMI WHERE CODICEDOCUMENTO = MovimentiContabiliTesta.NumeroRegistrazione ) AS DocumentoPagato, MovimentiContabiliTesta.NumeroRegistrazione, MovimentiContabiliTesta.CausaleContabile, MovimentiContabiliTesta.DataDocumento, MovimentiContabiliTesta.NumeroDocumento, MovimentiContabiliTesta.Descrizione, MovimentiContabiliTesta.DataRegistrazione, MovimentiContabiliRiga.Importo, MovimentiContabiliRiga.DareAvere, MovimentiContabiliRiga.Tipo " & _
                         " FROM MovimentiContabiliRiga  INNER JOIN MovimentiContabiliTesta  ON MovimentiContabiliTesta.NumeroRegistrazione = MovimentiContabiliRiga.Numero  " & _
                         " WHERE MovimentiContabiliRiga.MastroPartita = " & Mastro & " AND MovimentiContabiliRiga.ContoPartita = " & Conto & " AND MovimentiContabiliRiga.SottocontoPartita = " & Sottoconto & " AND MovimentiContabiliTesta.NumeroRegistrazione <> " & Val(Txt_Numero.Text) & " AND (" & Xs.EstraiCondizioniSQL(Session("DC_TABELLE")) & ")" & _
                         " ORDER BY NumeroRegistrazione "

        Dim cn As OleDbConnection


        cn = New Data.OleDb.OleDbConnection(Session("DC_GENERALE"))

        cn.Open()
        Dim cmd As New OleDbCommand()
        Tabella.Clear()
        Tabella.Columns.Clear()
        Tabella.Columns.Add("Copia", GetType(String))
        Tabella.Columns.Add("Registrazione", GetType(String))
        Tabella.Columns.Add("DataDocumento", GetType(String))
        
        Tabella.Columns.Add("NumeroDocumento", GetType(String))
        Tabella.Columns.Add("ImportoDocumento", GetType(String))
        Tabella.Columns.Add("ImportoPagato", GetType(String))
        Tabella.Columns.Add("Scadenzario", GetType(Boolean))
        Tabella.Columns.Add("Chiusa", GetType(Long))
        Tabella.Columns.Add("NumeroScadenza", GetType(Long))
        Tabella.Columns.Add("DataScadenza", GetType(String))
        cmd.CommandText = MySql

        cmd.CommandTimeout = 120
        cmd.Connection = cn
        Dim myPOSTreader As OleDbDataReader = cmd.ExecuteReader()
        Do While myPOSTreader.Read

            If NumeroRegistrazione = campodbN(myPOSTreader.Item("NumeroRegistrazione")) Then
                If DareAvereIncPag = campodb(myPOSTreader.Item("DareAvere")) Then
                    Importo = Importo - campodbN(myPOSTreader.Item("Importo"))
                Else
                    Importo = Importo + campodbN(myPOSTreader.Item("Importo"))
                End If

            Else
                If campodb(Request.Item("TIPO")) <> "SCADENZARIO" Then

                    If Math.Abs(Math.Round(DocumentoPagato, 2)) < Math.Abs(Math.Round(Importo, 2)) Then
                        Dim myriga As System.Data.DataRow = Tabella.NewRow()
                        myriga(1) = NumeroRegistrazione
                        myriga(2) = DataDocumento
                        myriga(3) = NumeroDocumento
                        If Importo < 0 Then
                            myriga(4) = Format(Math.Round((Math.Abs(Importo) - Math.Abs(DocumentoPagato)) * -1, 2), "#,##0.00")
                        Else
                            myriga(4) = Format(Math.Round(Importo - DocumentoPagato, 2), "#,##0.00")
                        End If


                        myriga(6) = False

                        myriga(7) = False
                        myriga(8) = 0
                        Tabella.Rows.Add(myriga)
                    End If
                Else
                    If Math.Abs(Math.Round(DocumentoPagato, 2)) < Math.Abs(Math.Round(Importo, 2)) Then
                        Dim kScadenze As New Cls_Scadenziario
                        Dim TabScadenze As New System.Data.DataTable
                        Dim i As Long
                        Dim Inserito As Boolean = False


                        kScadenze.NumeroRegistrazioneContabile = NumeroRegistrazione
                        kScadenze.loaddati(Session("DC_GENERALE"), TabScadenze)

                        For i = 0 To TabScadenze.Rows.Count - 1
                            If TabScadenze.Rows(i).Item(3).ToString <> "Chiusa" And TabScadenze.Rows(i).Item(1).ToString <> "" Then
                                Dim myriga As System.Data.DataRow = Tabella.NewRow()
                                myriga(1) = NumeroRegistrazione
                                myriga(2) = DataDocumento
                                myriga(9) = TabScadenze.Rows(i).Item(0)
                                myriga(3) = NumeroDocumento
                                If TabScadenze.Rows(i).Item(1).ToString = "" Then
                                    myriga(4) = 0
                                Else
                                    'If Importo < 0 Then
                                    ' myriga(4) = Format(Math.Round(CDbl(TabScadenze.Rows(i).Item(1).ToString) * -1, 2), "#,##0.00")
                                    'Else
                                    
                                    myriga(4) = Format(Math.Round(CDbl(TabScadenze.Rows(i).Item(1).ToString), 2), "#,##0.00")
                                    'End If

                                    Dim Documento As New Cls_MovimentoContabile
                                    Dim LegamiDocumento As New Cls_Legami

                                    Documento.NumeroRegistrazione = NumeroRegistrazione
                                    Documento.Leggi(Session("DC_GENERALE"), Documento.NumeroRegistrazione)


                                    If Math.Abs(CDbl(myriga(4))) > Math.Abs(Documento.ImportoDocumento(Session("DC_TABELLE"))) - Math.Abs(LegamiDocumento.TotaleLegame(Session("DC_GENERALE"), Documento.NumeroRegistrazione)) Then
                                        If myriga(4) > 0 Then
                                            myriga(4) = Format(Documento.ImportoDocumento(Session("DC_TABELLE")) - LegamiDocumento.TotaleLegame(Session("DC_GENERALE"), Documento.NumeroRegistrazione), "#,##0.00")
                                        Else
                                            'TOLTO * -1 L'8/10/2019 perche non prendeva l'importo sulle nc

                                            myriga(4) = Format((Documento.ImportoDocumento(Session("DC_TABELLE")) - LegamiDocumento.TotaleLegame(Session("DC_GENERALE"), Documento.NumeroRegistrazione)), "#,##0.00")
                                        End If
                                    End If
                                End If

                            myriga(6) = True


                            myriga(7) = False
                            myriga(8) = TabScadenze.Rows(i).Item(4)
                            Tabella.Rows.Add(myriga)
                            Inserito = True
                            End If
                        Next
                        If Not Inserito Then
                            Dim myriga As System.Data.DataRow = Tabella.NewRow()
                            myriga(1) = NumeroRegistrazione
                            myriga(2) = DataDocumento
                            myriga(9) = DataDocumento
                            myriga(3) = NumeroDocumento

                            If Importo < 0 Then
                                myriga(4) = Format(Math.Round((Math.Abs(Importo) - Math.Abs(DocumentoPagato)) * -1, 2), "#,##0.00")
                            Else
                                myriga(4) = Format(Math.Round(Importo - DocumentoPagato, 2), "#,##0.00")
                            End If
                            myriga(6) = True


                            myriga(7) = False
                            myriga(8) = -1
                            Tabella.Rows.Add(myriga)
                        End If
                    End If
                End If

                NumeroRegistrazione = myPOSTreader.Item("NumeroRegistrazione")
                DocumentoPagato = Math.Abs(campodbN(myPOSTreader.Item("DocumentoPagato")))
                DataDocumento = CAMPODBD(myPOSTreader.Item("DataDocumento"))
                NumeroDocumento = campodb(myPOSTreader.Item("NumeroDocumento"))
                CausaleContabile = myPOSTreader.Item("CausaleContabile")
                If NumeroRegistrazione = 15483 Then
                    NumeroRegistrazione = 15483

                End If
                Dim XCau As New Cls_CausaleContabile

                XCau.Leggi(Session("DC_TABELLE"), myPOSTreader.Item("CausaleContabile"))

                If XCau.VenditaAcquisti <> CausaleAcqVend Then
                    If DareAvereIncPag <> myPOSTreader.Item("DareAvere") Then
                        Importo = Math.Round(myPOSTreader.Item("Importo") * -1, 2)
                    Else
                        Importo = Math.Round(myPOSTreader.Item("Importo"), 2)
                    End If
                Else
                    If DareAvereIncPag = myPOSTreader.Item("DareAvere") Then
                        Importo = Math.Round(myPOSTreader.Item("Importo") * -1, 2)
                    Else
                        Importo = Math.Round(myPOSTreader.Item("Importo"), 2)
                    End If
                End If
            End If
        Loop
        myPOSTreader.Close()
        cn.Close()

        If Math.Abs(Math.Round(DocumentoPagato, 2)) < Math.Abs(Math.Round(Importo, 2)) Then
            If Request.Item("TIPO") <> "SCADENZARIO" Then
                Dim myriga As System.Data.DataRow = Tabella.NewRow()
                myriga(1) = NumeroRegistrazione
                myriga(2) = DataDocumento
                myriga(3) = NumeroDocumento
                If Importo < 0 Then
                    myriga(4) = Format(Math.Round((Math.Abs(Importo) - Math.Abs(DocumentoPagato)) * -1, 2), "#,##0.00")
                Else
                    myriga(4) = Format(Math.Round(Importo - DocumentoPagato, 2), "#,##0.00")
                End If

                myriga(6) = False

                myriga(7) = False
                myriga(8) = 0
                Tabella.Rows.Add(myriga)
            Else
                Dim kScadenze As New Cls_Scadenziario
                Dim TabScadenze As New System.Data.DataTable
                Dim i As Long
                Dim Inserito As Boolean = False

                kScadenze.NumeroRegistrazioneContabile = NumeroRegistrazione
                kScadenze.loaddati(Session("DC_GENERALE"), TabScadenze)

                For i = 0 To TabScadenze.Rows.Count - 1
                    If TabScadenze.Rows(i).Item(3).ToString <> "Chiusa" And TabScadenze.Rows(i).Item(1).ToString <> "" Then
                        Dim myriga As System.Data.DataRow = Tabella.NewRow()
                        myriga(1) = NumeroRegistrazione
                        myriga(2) = DataDocumento
                        myriga(3) = NumeroDocumento
                        myriga(9) = TabScadenze.Rows(i).Item(0)
                        If TabScadenze.Rows(i).Item(1).ToString = "" Then
                            myriga(4) = 0
                        Else
                            'If Importo < 0 Then
                            '    myriga(4) = Format(Math.Round(CDbl(TabScadenze.Rows(i).Item(1).ToString) * -1, 2), "#,##0.00")
                            'Else
                            myriga(4) = Format(Math.Round(CDbl(TabScadenze.Rows(i).Item(1).ToString), 2), "#,##0.00")
                            'End If

                            Dim Documento As New Cls_MovimentoContabile
                            Dim LegamiDocumento As New Cls_Legami

                            Documento.NumeroRegistrazione = NumeroRegistrazione
                            Documento.Leggi(Session("DC_GENERALE"), Documento.NumeroRegistrazione)


                            If Math.Abs(CDbl(myriga(4))) > Math.Abs(Documento.ImportoDocumento(Session("DC_TABELLE"))) - Math.Abs(LegamiDocumento.TotaleLegame(Session("DC_GENERALE"), Documento.NumeroRegistrazione)) Then
                                If myriga(4) > 0 Then
                                    myriga(4) = Format(Documento.ImportoDocumento(Session("DC_TABELLE")) - LegamiDocumento.TotaleLegame(Session("DC_GENERALE"), Documento.NumeroRegistrazione), "#,##0.00")
                                Else
                                    myriga(4) = Format((Documento.ImportoDocumento(Session("DC_TABELLE")) - LegamiDocumento.TotaleLegame(Session("DC_GENERALE"), Documento.NumeroRegistrazione)) * -1, "#,##0.00")
                                End If
                            End If
                        End If

                        myriga(6) = True


                        myriga(7) = False
                        myriga(8) = TabScadenze.Rows(i).Item(4)
                        Tabella.Rows.Add(myriga)
                        Inserito = True
                        End If
                Next
                If Not Inserito Then
                    Dim myriga As System.Data.DataRow = Tabella.NewRow()
                    myriga(1) = NumeroRegistrazione
                    myriga(2) = DataDocumento
                    myriga(9) = DataDocumento
                    myriga(3) = NumeroDocumento

                    If Importo < 0 Then
                        myriga(4) = Format(Math.Round((Math.Abs(Importo) - Math.Abs(DocumentoPagato)) * -1, 2), "#,##0.00")
                    Else
                        myriga(4) = Format(Math.Round(Importo - DocumentoPagato, 2), "#,##0.00")
                    End If
                    myriga(6) = True


                    myriga(7) = False
                    myriga(8) = -1
                    Tabella.Rows.Add(myriga)
                End If
            End If

        End If


        If Val(Txt_Numero.Text) > 0 Then
            Dim X As New Cls_MovimentoContabile

            X.Leggi(Session("DC_GENERALE"), Val(Txt_Numero.Text))

            Dim XL As New Cls_Legami
            XL.Leggi(Session("DC_GENERALE"), 0, Val(Txt_Numero.Text))

            Dim MyCau As New Cls_CausaleContabile

            MyCau.Codice = X.CausaleContabile
            MyCau.Leggi(Session("DC_TABELLE"), X.CausaleContabile)


            For I = 0 To 100
                If XL.NumeroPagamento(I) = Val(Txt_Numero.Text) Then
                    Dim Docum As New Cls_MovimentoContabile
                    Docum.Leggi(Session("DC_GENERALE"), XL.NumeroDocumento(I))
                    Dim myriga As System.Data.DataRow = Tabella.NewRow()

                    myriga(1) = Docum.NumeroRegistrazione
                    myriga(2) = Format(Docum.DataDocumento, "dd/MM/yyyy")
                    myriga(3) = Docum.NumeroDocumento

                    If X.Righe(0).DareAvere = "D" And MyCau.VenditaAcquisti = "V" Then
                        myriga(4) = Format(Math.Abs(Docum.ImportoDocumento(Session("DC_TABELLE")) - Docum.ImportoRitenuta(Session("DC_TABELLE"))), "#,##0.00")
                        myriga(5) = Format(XL.Importo(I), "#,##0.00")
                    Else
                        myriga(4) = Format(Docum.ImportoDocumento(Session("DC_TABELLE")) - Docum.ImportoRitenuta(Session("DC_TABELLE")), "#,##0.00")
                        If Docum.ImportoDocumento(Session("DC_TABELLE")) - Docum.ImportoRitenuta(Session("DC_TABELLE")) < 0 Then
                            myriga(5) = Format(XL.Importo(I) * -1, "-#,##0.00")
                        Else
                            myriga(5) = Format(XL.Importo(I), "#,##0.00")
                        End If
                    End If


                    If Request.Item("TIPO") = "SCADENZARIO" Then
                        myriga(6) = False
                    Else
                        myriga(6) = False
                    End If

                    myriga(7) = False
                    myriga(8) = 0
                    Tabella.Rows.Add(myriga)
                End If
            Next
        End If

        Grid.AutoGenerateColumns = False
        Grid.DataSource = Tabella
        Grid.DataBind()

        If Request.Item("TIPO") <> "SCADENZARIO" Then
            Grid.Columns(3).Visible = False
        End If

        For I = 0 To Grid.Rows.Count - 1
            'copiaimportoScad
            If Request.Item("TIPO") <> "SCADENZARIO" Then
                Grid.Rows(I).Cells(0).Text = "<a href=""#"" onclick=""copiaimporto('" & I & "');""><img src=""images\copiaimporto.png"" height=""16"" width=""16"" alt=""digita importo""></a>" & Grid.Rows(I).Cells(0).Text
            Else
                Grid.Rows(I).Cells(0).Text = "<a href=""#"" onclick=""copiaimportoScad('" & I & "');""><img src=""images\copiaimporto.png"" height=""16"" width=""16"" alt=""digita importo""></a>" & Grid.Rows(I).Cells(0).Text
            End If
        Next

        ViewState("Save_Tab_IP") = Tabella
    End Sub
    Protected Sub Button1_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles Button1.Click
        If Dd_CausaleContabile.SelectedValue = "" Then
            ClientScript.RegisterClientScriptBlock(Me.GetType(), "Errore", "VisualizzaErrore('Specificare causale contabile');", True)
            REM Lbl_errori.Text = "Specificare causale contabile"
            Exit Sub
        End If
        If Txt_ClienteFornitore.Text = "" Then
            ClientScript.RegisterClientScriptBlock(Me.GetType(), "Errore", "VisualizzaErrore('Specificare il conto cliente/fornitore');", True)
            REM Lbl_errori.Text = "Specificare il conto cliente/fornitore"
            Exit Sub
        End If
        Lbl_errori.Text = ""
        Call PienaGriglia()
        ImpostaJS()
    End Sub
    Function campodb(ByVal oggetto As Object) As String
        If IsDBNull(oggetto) Then
            Return ""
        Else
            Return oggetto
        End If
    End Function
    Function campodbN(ByVal oggetto As Object) As Double
        If IsDBNull(oggetto) Then
            Return 0
        Else
            Return oggetto
        End If
    End Function
    Function campodbD(ByVal oggetto As Object) As Date
        If IsDBNull(oggetto) Then
            Return Nothing
        Else
            Return oggetto
        End If
    End Function
    Private Function SplitWords(ByVal s As String) As String()
        '
        ' Call Regex.Split function from the imported namespace.
        ' Return the result array.
        '
        Return Regex.Split(s, "\W+")
    End Function

    Private Sub ImpostaJS()
        Dim MyJs As String

        MyJs = "$(" & Chr(34) & "#" & Txt_ClienteFornitore.ClientID & Chr(34) & ").autocomplete('ClientiFornitori.ashx?TIPO=R&Utente=" & Session("UTENTE") & "', {delay:5,minChars:3});"


        'Dim int_MilliSecondsTimeReminder As Integer
        'Dim int_MilliSecondsTimeOut As Integer
        'Dim msgSession As String
        'msgSession = "Warning: Se nei prossimi 3 minuti, non fai nessuna operazione, il sistema tornerà alla pagina di login. Salva i tuoi dati."

        'int_MilliSecondsTimeReminder = ((Page.Session.Timeout - 3) * 50 * 1000)
        'int_MilliSecondsTimeOut = ((Page.Session.Timeout) * 50 * 1000)

        'MyJs = MyJs & " clearTimeout(myTimeReminder); " & vbNewLine & _
        '        " clearTimeout(myTimeOut); " & vbNewLine & _
        '        "var sessionTimeReminder = " & int_MilliSecondsTimeReminder.ToString() & ";" & vbNewLine & _
        '        "var sessionTimeout = " & int_MilliSecondsTimeOut.ToString() & ";" & vbNewLine & _
        '        "function doReminder(){ alert('" + msgSession & "'); }" & vbNewLine & _
        '        "function doRedirect(){ window.location.href='/Seniorweb/Login.aspx'; }" & vbNewLine & _
        '        " myTimeReminder=setTimeout('document.getElementById(""IdTimeOut"").style.visibility= ""visible"";',sessionTimeReminder); myTimeOut=setTimeout('window.location.href=""/Seniorweb/Login.aspx"";',sessionTimeout); " & vbNewLine


        REM MyJs = "$(" & Chr(34) & "#" & Txt_ClienteFornitore.ClientID & Chr(34) & ").change(function() {  __doPostBack(""Button1"", ""0""); });"

        MyJs = MyJs & "$(" & Chr(34) & "#" & Txt_SottocontoCassa.ClientID & Chr(34) & ").autocomplete('/WebHandler/GestioneConto.ashx?TIPO=R&Utente=" & Session("UTENTE") & "', {delay:5,minChars:3});"
        MyJs = MyJs & "$(" & Chr(34) & "#" & Txt_SottocontoAbbuono.ClientID & Chr(34) & ").autocomplete('/WebHandler/GestioneConto.ashx?TIPO=R&Utente=" & Session("UTENTE") & "', {delay:5,minChars:3});"
        MyJs = MyJs & "$(" & Chr(34) & "#" & Txt_Sottocontospese.ClientID & Chr(34) & ").autocomplete('/WebHandler/GestioneConto.ashx?TIPO=R&Utente=" & Session("UTENTE") & "', {delay:5,minChars:3});"
        MyJs = MyJs & "$(" & Chr(34) & "#" & Txt_SottocontoLibero.ClientID & Chr(34) & ").autocomplete('/WebHandler/GestioneConto.ashx?TIPO=R&Utente=" & Session("UTENTE") & "', {delay:5,minChars:3});"

        MyJs = MyJs & "$('#" & Txt_Importo.ClientID & "').keypress(function() { ForceNumericInput($('#" & Txt_Importo.ClientID & "').val(), true, true); } );  $('#" & Txt_Importo.ClientID & "').blur(function() { var ap = formatNumber ($('#" & Txt_Importo.ClientID & "').val(),2); $('#" & Txt_Importo.ClientID & "').val(ap); }); "

        MyJs = MyJs & "$('#" & Txt_ImportoAbbuono.ClientID & "').keypress(function() { ForceNumericInput($('#" & Txt_ImportoAbbuono.ClientID & "').val(), true, true); } );  $('#" & Txt_ImportoAbbuono.ClientID & "').blur(function() { var ap = formatNumber ($('#" & Txt_ImportoAbbuono.ClientID & "').val(),2); $('#" & Txt_ImportoAbbuono.ClientID & "').val(ap); }); "

        MyJs = MyJs & "$('#" & Txt_Importospese.ClientID & "').keypress(function() { ForceNumericInput($('#" & Txt_Importospese.ClientID & "').val(), true, true); } );  $('#" & Txt_Importospese.ClientID & "').blur(function() { var ap = formatNumber ($('#" & Txt_Importospese.ClientID & "').val(),2); $('#" & Txt_Importospese.ClientID & "').val(ap); }); "
        MyJs = MyJs & "$('#" & Txt_ImportoLibero.ClientID & "').keypress(function() { ForceNumericInput($('#" & Txt_ImportoLibero.ClientID & "').val(), true, true); } );  $('#" & Txt_ImportoLibero.ClientID & "').blur(function() { var ap = formatNumber ($('#" & Txt_ImportoLibero.ClientID & "').val(),2); $('#" & Txt_ImportoLibero.ClientID & "').val(ap); }); "


        MyJs = MyJs & "$(" & Chr(34) & "#" & Txt_DataRegistrazione.ClientID & Chr(34) & ").mask(""99/99/9999""); "
        MyJs = MyJs & "$(" & Chr(34) & "#" & Txt_DataBolletta.ClientID & Chr(34) & ").mask(""99/99/9999""); "

        MyJs = MyJs & " $(" & Chr(34) & "#" & Txt_DataRegistrazione.ClientID & Chr(34) & ").datepicker({ changeMonth: true,changeYear: true,  yearRange: ""1890:" & Year(Now) + 1 & """},$.datepicker.regional[""it""]);"
        MyJs = MyJs & " $(" & Chr(34) & "#" & Txt_DataBolletta.ClientID & Chr(34) & ").datepicker({ changeMonth: true,changeYear: true,  yearRange: ""1890:" & Year(Now) + 1 & """},$.datepicker.regional[""it""]);"

        If Request.Item("NONMENU") = "OK" Then
            MyJs = MyJs & "$('.destraclasse').css('display','none');"
            MyJs = MyJs & "$('.Barra').css('display','none');"
            MyJs = MyJs & "$('.DivTasti').css( ""top"", ""70px"" );"
        End If


        MyJs = MyJs & " $(" & Chr(34) & "#" & Txt_DataRegistrazione.ClientID & Chr(34) & ").blur(function() {" & vbNewLine
        MyJs = MyJs & " var apponumero = $(" & Chr(34) & "#" & Txt_Numero.ClientID & Chr(34) & ").val();" & vbNewLine
        MyJs = MyJs & " var prova  = parseInt(apponumero);" & vbNewLine
        MyJs = MyJs & " if (prova  != 0) {" & vbNewLine
        MyJs = MyJs & " return true; }" & vbNewLine
        MyJs = MyJs & " var today = new Date(); " & vbNewLine
        MyJs = MyJs & " var appoggio = '' + $(" & Chr(34) & "#" & Txt_DataRegistrazione.ClientID & Chr(34) & ").val();"
        MyJs = MyJs & " var dateArray = appoggio.split('/');" & vbNewLine
        MyJs = MyJs & " if (today.getFullYear() != dateArray[2]) { " & vbNewLine
        MyJs = MyJs & " $(" & Chr(34) & "#" & Txt_DataRegistrazione.ClientID & Chr(34) & ").css(""border"",""3px solid #FFAA00"");"
        MyJs = MyJs & " } else { " & vbNewLine
        MyJs = MyJs & " $(" & Chr(34) & "#" & Txt_DataRegistrazione.ClientID & Chr(34) & ").css(""border"",""1px solid #8894A0"");"
        MyJs = MyJs & " } "
        MyJs = MyJs & " });"


        MyJs = MyJs & "$("".chosen-select"").chosen({"
        MyJs = MyJs & "no_results_text: ""Causale non trovata"","
        MyJs = MyJs & "display_disabled_options: true,"
        MyJs = MyJs & "allow_single_deselect: true,"
        MyJs = MyJs & "width: ""350px"","
        MyJs = MyJs & "placeholder_text_single: ""Seleziona Causale"""
        MyJs = MyJs & "});"




        ScriptManager.RegisterStartupScript(Me, Me.GetType(), "ContoCostiRicavi", MyJs, True)

    End Sub

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Trim(Session("UTENTE")) = "" Then
            If Request.ServerVariables("URL").ToUpper.IndexOf("SENIORWEBBLANCO") > 0 Then
                Response.Redirect("/ODV/Login.aspx")
                Exit Sub
            Else
                Response.Redirect("/SeniorWeb/Login.aspx")
                Exit Sub
            End If
        End If
        If Request.Item("CHIAMATAESTERNA") > 0 Then
            Btn_Esci.Visible = False
            BtnRicerca.Visible = False
            Call MettiInvisibileJS()
        End If

        LblBox.Text = ""

        Timer1.Interval = 900 * 1000

        Call ImpostaJS()

        If Session("ABILITAZIONI").ToString.IndexOf("<REGISTRAZIONE>") < 0 Then
            Response.Redirect("../MainMenu.aspx")
            Exit Sub
        End If

        If Page.IsPostBack = True Then Exit Sub

        Dim Cserv As New Cls_CentroServizio

        Cserv.UpDateDropBox(Session("DC_OSPITE"), DD_CSERV)

        Dim Into As New Cls_DatiGenerali

        Into.LeggiDati(Session("DC_TABELLE"))

        If Into.AttivaCServPrimanoIncassi = 1 Then

            DD_CSERV.Visible = True
            Lbl_CentroSerivizio.Visible = True
        End If


        Dim K1 As New Cls_SqlString

        Dim Barra As New Cls_BarraSenior


        If Val(Request.Item("NumeroRegistrazione")) > 0 Then
            Session("NumeroRegistrazione") = Val(Request.Item("NumeroRegistrazione"))
        End If



        If Not IsNothing(Session("RicercaGeneraleSQLString")) Then
            K1 = Session("RicercaGeneraleSQLString")
        End If

        If Request.Item("NONMENU") <> "OK" Then
            Lbl_BarraSenior.Text = Barra.CodiceBarra(Application("SENIOR"), Session("UTENTE"), Page, K1)
        Else
            Dim Log As New Cls_LogPrivacy

            Log.LogPrivacy(Application("SENIOR"), Session("CLIENTE"), Session("UTENTE"), Session("UTENTEIMPER"), Page.GetType.Name.ToUpper, 0, 0, "", "", Session("NumeroRegistrazione"), "", "V", "", "")
        End If



        Dim k As New Cls_CausaleContabile
        Dim ConnectionString As String = Session("DC_TABELLE")

        k.UpDateDropBoxPag(ConnectionString, Dd_CausaleContabile)

        Tabella.Clear()
        Tabella.Columns.Clear()
        Tabella.Columns.Add("Copia", GetType(String))
        Tabella.Columns.Add("Registrazione", GetType(String))
        Tabella.Columns.Add("DataDocumento", GetType(String))
        Tabella.Columns.Add("NumeroDocumento", GetType(String))
        Tabella.Columns.Add("ImportoDocumento", GetType(String))
        Tabella.Columns.Add("ImportoPagato", GetType(String))
        Tabella.Columns.Add("Scadenzario", GetType(Boolean))
        Tabella.Columns.Add("Chiusa", GetType(Long))
        Tabella.Columns.Add("NumeroScadenza", GetType(Long))
        Tabella.Columns.Add("DataScadenza", GetType(String))


        Dim myriga As System.Data.DataRow = Tabella.NewRow()
        If Request.Item("TIPO") = "SCADENZARIO" Then
            myriga(6) = True
            myriga(7) = False
        Else
            myriga(6) = False
            myriga(7) = False
        End If
        myriga(8) = 0
        Tabella.Rows.Add(myriga)
        Grid.AutoGenerateColumns = False
        Grid.DataSource = Tabella
        Grid.DataBind()

        If Request.Item("TIPO") <> "SCADENZARIO" Then
            Grid.Columns(3).Visible = False
        End If

        Txt_DataRegistrazione.Text = Format(Now, "dd/MM/yyyy")

        Dim kP As New Cls_MovimentoContabile


        Txt_ImportoAbbuono.Text = "0,00"
        Txt_ImportoCassa.Text = "0,00"
        Txt_ImportoLibero.Text = "0,00"
        Txt_Importospese.Text = "0,00"

        Txt_Numero.Text = "0"
        If Val(Session("NumeroRegistrazione")) > 0 Then
            Dim x As New Cls_MovimentoContabile
            x.Leggi(Session("DC_GENERALE"), Val(Session("NumeroRegistrazione")))
            Dim Ks As New Cls_CausaleContabile
            Ks.Leggi(Session("DC_TABELLE"), x.CausaleContabile)
            If Ks.Tipo <> "P" Then
                Exit Sub
            End If
            If x.NumeroRegistrazione > 0 Then
                Txt_Numero.Text = Val(Session("NumeroRegistrazione"))
                Txt_Numero.Enabled = False
                Call LeggiRegistrazione()
            End If
        Else
            If Request.Item("Data") <> "" Then
                Dim DataCopia As String
                DataCopia = Request.Item("Data") & "00000000"

                Try
                    Txt_DataRegistrazione.Text = Format(DateSerial(Val(Mid(DataCopia, 1, 4)), Val(Mid(DataCopia, 5, 2)), Val(Mid(DataCopia, 7, 2))), "dd/MM/yyyy")

                    Dd_CausaleContabile.SelectedValue = Request.Item("CausaleContabile")
                Catch ex As Exception

                End Try


            End If
        End If


        
    End Sub

    Protected Sub Btn_Modifica_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles Btn_Modifica.Click
        Lbl_errori.Text = ""
        If Not IsDate(Txt_DataRegistrazione.Text) Then
            ClientScript.RegisterClientScriptBlock(Me.GetType(), "Errore", "VisualizzaErrore('Data Registrazione formalmente errata');", True)
            REM Lbl_errori.Text = "Data Registrazione formalmente errata"
            Exit Sub
        End If
        If Dd_CausaleContabile.SelectedValue = "" Then
            ClientScript.RegisterClientScriptBlock(Me.GetType(), "Errore", "VisualizzaErrore('Digitare causale contabile');", True)
            REM Lbl_errori.Text = "Digitare causale contabile"
            Exit Sub
        End If

        If Txt_DataBolletta.Text.Trim <> "" Then
            If Not IsDate(Txt_DataBolletta.Text) Then
                ClientScript.RegisterClientScriptBlock(Me.GetType(), "Errore", "VisualizzaErrore('Data Bolletta formalmente errata');", True)
                REM Lbl_errori.Text = "Data Registrazione formalmente errata"
                Exit Sub
            End If
            If Year(Txt_DataBolletta.Text) < 1950 Then
                ClientScript.RegisterClientScriptBlock(Me.GetType(), "Errore", "VisualizzaErrore('Data Bolletta formalmente errata');", True)
                REM Lbl_errori.Text = "Data Registrazione formalmente errata"
                Exit Sub
            End If

        End If

        If Txt_Importo.Text = "" Then
            ClientScript.RegisterClientScriptBlock(Me.GetType(), "Errore", "VisualizzaErrore('Digitare importo pagato/incassato');", True)
            REM Lbl_errori.Text = "Digitare importo pagato/incassato"
            Exit Sub
        End If
        If Val(Txt_Numero.Text) > 0 Then
            Dim VerificaBollato As New Cls_MovimentoContabile
            VerificaBollato.NumeroRegistrazione = Val(Txt_Numero.Text)
            If VerificaBollato.Bollato(Session("DC_GENERALE")) = True Then
                ClientScript.RegisterClientScriptBlock(Me.GetType(), "Errore", "VisualizzaErrore('Già creato il giornale bollato');", True)
            End If
        End If

        Dim XDatiGenerali As New Cls_DatiGenerali
        Dim AppoggioData As Date

        AppoggioData = Txt_DataRegistrazione.Text
        XDatiGenerali.LeggiDati(Session("DC_TABELLE"))

        If Format(XDatiGenerali.DataChiusura, "yyyyMMdd") >= Format(AppoggioData, "yyyyMMdd") Then
            ClientScript.RegisterClientScriptBlock(Me.GetType(), "Errore", "VisualizzaErrore('Non posso modificare o inserire la registrazione,<BR/> già effettuato la chiusura');", True)
            Exit Sub
        End If
        Tabella = ViewState("Save_Tab_IP")
        Dim xTot As Double
        Dim i As Long

        Dim PagamentoConScadenza As Boolean = False
        Dim PagamentoSenzaScadenza As Boolean = False

        Try

            xTot = 0
            For i = 0 To Tabella.Rows.Count - 1
                Dim TxtImponibile As TextBox = DirectCast(Grid.Rows(i).FindControl("Txt_ImportoPagato"), TextBox)
                Dim Chk_Chiusa As CheckBox = DirectCast(Grid.Rows(i).FindControl("Chk_Chiusa"), CheckBox)

                If TxtImponibile.Text <> "" Then
                    If TxtImponibile.Text <> 0 Then

                        If Grid.Rows(i).BackColor = System.Drawing.Color.Gray Then
                            PagamentoSenzaScadenza = True
                        Else
                            PagamentoConScadenza = True
                        End If

                        If CDbl(TxtImponibile.Text) > 0 And CDbl(Tabella.Rows(i).Item(4)) < 0 Then
                            ClientScript.RegisterClientScriptBlock(Me.GetType(), "Errore", "VisualizzaErrore('Errore importo documento negativo in <br/>riga " & i + 1 & " importo pagato positivo');", True)
                            Exit Sub
                        End If
                        If Math.Abs(CDbl(TxtImponibile.Text)) > Math.Abs(CDbl(Tabella.Rows(i).Item(4))) Then
                            ClientScript.RegisterClientScriptBlock(Me.GetType(), "Errore", "VisualizzaErrore('Errore importo pagato/incassato <br/>riga " & i + 1 & " maggiore importo documento');", True)
                            Exit Sub
                        End If
                        If XDatiGenerali.BloccaLegameScadenzario = 1 Then
                            If Request.Item("TIPO") <> "SCADENZARIO" Then
                                Dim Scadenzario As New Cls_Scadenziario

                                Scadenzario.NumeroRegistrazioneContabile = Val(CDbl(Tabella.Rows(i).Item(1)))
                                Scadenzario.Leggi(Session("DC_GENERALE"))

                                If Scadenzario.Importo > 0 Then
                                    ClientScript.RegisterClientScriptBlock(Me.GetType(), "Errore", "VisualizzaErrore('Errore presente scadenza su questo documento " & Scadenzario.NumeroRegistrazioneContabile & " ');", True)
                                    Exit Sub
                                End If
                            End If
                        End If
                        If XDatiGenerali.ScadenziarioCheckChiuso = 1 Then
                            If Request.Item("TIPO") = "SCADENZARIO" And Chk_Chiusa.Visible = True Then
                                If Chk_Chiusa.Checked = False Then
                                    ClientScript.RegisterClientScriptBlock(Me.GetType(), "Errore", "VisualizzaErrore('Devi chiudere scadenza su questo documento " & Val(CDbl(Tabella.Rows(i).Item(1))) & " ');", True)
                                    Exit Sub
                                End If
                            End If
                        End If


                        xTot = xTot + CDbl(TxtImponibile.Text)
                    End If
                End If
            Next

        Catch ex As Exception
            ClientScript.RegisterClientScriptBlock(Me.GetType(), "Errore", "VisualizzaErrore('" & ex.Message.Replace("'", "") & "');", True)
            REM Lbl_errori.Text = "Importo distribuito maggiore del pagato/incassato"
            Exit Sub
        End Try
        If CDbl(Txt_Importo.Text) < Math.Round(xTot, 2) Then
            ClientScript.RegisterClientScriptBlock(Me.GetType(), "Errore", "VisualizzaErrore('Importo distribuito maggiore del pagato/incassato');", True)
            REM Lbl_errori.Text = "Importo distribuito maggiore del pagato/incassato"
            Exit Sub
        End If

        If PagamentoConScadenza And PagamentoSenzaScadenza Then
            ClientScript.RegisterClientScriptBlock(Me.GetType(), "Errore", "VisualizzaErrore('Attenzione non puoi fare una registrazione combinata scadenziario/legame');", True)
            Exit Sub
        End If

        Dim MovRitenuta As New Cls_RitenuteAcconto

        MovRitenuta.NumeroRegistrazionePagamento = Val(Txt_Numero.Text)

        If MovRitenuta.RitenutePagateSuPagamento(Session("DC_GENERALE"), MovRitenuta.NumeroRegistrazionePagamento) Then
            ClientScript.RegisterClientScriptBlock(Me.GetType(), "Errore", "VisualizzaErrore('Non posso modificare registrazione,<BR/> ritenute pagate');", True)
            Exit Sub
        End If



        If VerificaSeInserito() Then
            Esci()
            Exit Sub
        End If



        Dim X As New Cls_MovimentoContabile
        Dim MyCau As New Cls_CausaleContabile

        X.Leggi(Session("DC_GENERALE"), Val(Txt_Numero.Text))

        If XDatiGenerali.GirocontoRitenuta = 1 Then
            For i = 0 To 300
                If Not IsNothing(X.Righe(i)) Then
                    If X.Righe(i).RigaDaCausale = 5 Then
                        X.Righe(i) = Nothing
                    End If
                End If
            Next
        End If

        X.NumeroRegistrazione = Val(Txt_Numero.Text)
        X.DataRegistrazione = Txt_DataRegistrazione.Text
        If IsDate(Txt_DataBolletta.Text) Then
            X.DataBolletta = Txt_DataBolletta.Text
        Else
            X.DataBolletta = Nothing
        End If
        X.NumeroBolletta = Txt_NumeroBolletta.Text
        X.Descrizione = Txt_Descrizione.Text

        X.CentroServizio = DD_CSERV.SelectedValue


        X.CausaleContabile = Dd_CausaleContabile.SelectedValue
        MyCau.Leggi(Session("DC_TABELLE"), Dd_CausaleContabile.SelectedValue)



        Dim Mastro As Long
        Dim Conto As Long
        Dim Sottoconto As Long
        Dim Vettore(100) As String
        Dim Riga As Long

        Riga = 0

        Vettore = SplitWords(Txt_ClienteFornitore.Text)

        If Vettore.Length > 2 Then
            Mastro = Val(Vettore(0))
            Conto = Val(Vettore(1))
            Sottoconto = Val(Vettore(2))
        End If

        If Sottoconto = 0 Then
            ClientScript.RegisterClientScriptBlock(Me.GetType(), "Errore", "VisualizzaErrore('Non posso proseguire non è indicato il sottoconto fornitore');", True)
            Exit Sub
        End If


        If Val(Txt_Numero.Text) > 0 Then
            Dim Log As New Cls_LogPrivacy

            Log.LogPrivacy(Application("SENIOR"), Session("CLIENTE"), Session("UTENTE"), Session("UTENTEIMPER"), Page.GetType.Name.ToUpper, 0, 0, "", "", Session("NumeroRegistrazione"), "", "M", "INCASSO", "")

        Else
            Dim Log As New Cls_LogPrivacy

            Log.LogPrivacy(Application("SENIOR"), Session("CLIENTE"), Session("UTENTE"), Session("UTENTEIMPER"), Page.GetType.Name.ToUpper, 0, 0, "", "", Session("NumeroRegistrazione"), "", "I", "INCASSO", "")
        End If

        If Val(Txt_Numero.Text) = 0 Then
            If X.Descrizione = "" Then
                Tabella = ViewState("Save_Tab_IP")

                If MyCau.VenditaAcquisti = "V" Then
                    X.Descrizione = "Incasso Nota "
                Else
                    X.Descrizione = "Pagam. Fatt. "
                End If

                For i = 0 To Tabella.Rows.Count - 1
                    Dim TxtImponibile As TextBox = DirectCast(Grid.Rows(i).FindControl("Txt_ImportoPagato"), TextBox)
                    Dim Chk_Chiusa As CheckBox = DirectCast(Grid.Rows(i).FindControl("Chk_Chiusa"), CheckBox)

                    If TxtImponibile.Text <> "" Then
                        If CDbl(TxtImponibile.Text) <> 0 Then
                            Dim Appoggio As New Cls_MovimentoContabile

                            Appoggio.NumeroRegistrazione = Val(Tabella.Rows(i).Item(1))
                            Appoggio.Leggi(Session("DC_GENERALE"), Appoggio.NumeroRegistrazione)
                            If X.Descrizione.IndexOf("/") > 0 Then
                                X.Descrizione = X.Descrizione & ","
                            End If
                            If MyCau.VenditaAcquisti = "V" Then
                                X.Descrizione = X.Descrizione & Appoggio.NumeroDocumento & "/" & Year(Appoggio.DataRegistrazione)
                            Else
                                X.Descrizione = X.Descrizione & Appoggio.NumeroDocumento & "/" & Year(Appoggio.DataDocumento)
                            End If
                        End If
                    End If
                Next
                Dim PianoConti As New Cls_Pianodeiconti

                PianoConti.Mastro = Mastro
                PianoConti.Conto = Conto
                PianoConti.Sottoconto = Sottoconto
                PianoConti.Decodfica(Session("DC_GENERALE"))
                X.Descrizione = X.Descrizione & " " & PianoConti.Descrizione
            End If
        End If

        If IsNothing(X.Righe(Riga)) Then
            X.Righe(Riga) = New Cls_MovimentiContabiliRiga
        End If


        X.Righe(0).MastroPartita = Mastro
        X.Righe(0).ContoPartita = Conto
        X.Righe(0).SottocontoPartita = Sottoconto
        X.Righe(0).Importo = Txt_Importo.Text
        X.Righe(0).Segno = "+"
        X.Righe(0).Tipo = "CF"
        X.Righe(0).Descrizione = Txt_Descrizione.Text
        X.Righe(0).RigaDaCausale = 1
        X.Righe(0).DareAvere = MyCau.Righe(0).DareAvere

        Riga = Riga + 1

        If Txt_ImportoCassa.Text <> "" Then
            If IsNothing(X.Righe(Riga)) Then
                X.Righe(Riga) = New Cls_MovimentiContabiliRiga
            End If
            Vettore = SplitWords(Txt_SottocontoCassa.Text)

            If Vettore.Length > 2 Then
                Mastro = Val(Vettore(0))
                Conto = Val(Vettore(1))
                Sottoconto = Val(Vettore(2))
            End If


            If Sottoconto = 0 Then
                ClientScript.RegisterClientScriptBlock(Me.GetType(), "Errore", "VisualizzaErrore('Non posso proseguire non è indicato il sottoconto cassa/banca');", True)
                Exit Sub
            End If
            X.Righe(Riga).MastroPartita = Mastro
            X.Righe(Riga).ContoPartita = Conto
            X.Righe(Riga).SottocontoPartita = Sottoconto
            X.Righe(Riga).Importo = Txt_ImportoCassa.Text
            X.Righe(Riga).Descrizione = Txt_Descrizione.Text
            X.Righe(Riga).Segno = "+"
            X.Righe(Riga).Tipo = ""
            X.Righe(Riga).RigaDaCausale = 2
            X.Righe(Riga).DareAvere = MyCau.Righe(1).DareAvere
            Riga = Riga + 1
        Else
            If Not IsNothing(X.Righe(Riga)) Then
                If X.Righe(Riga).Importo > 0 Then
                    X.Righe(Riga).Importo = 0
                End If
            End If
        End If

        If Txt_ImportoAbbuono.Text <> "" Then
            If CDbl(Txt_ImportoAbbuono.Text) <> 0 Then
                If IsNothing(X.Righe(Riga)) Then
                    X.Righe(Riga) = New Cls_MovimentiContabiliRiga
                End If

                Vettore = SplitWords(Txt_SottocontoAbbuono.Text)

                If Vettore.Length > 2 Then
                    Mastro = Val(Vettore(0))
                    Conto = Val(Vettore(1))
                    Sottoconto = Val(Vettore(2))
                End If

                If Mastro > 0 And Sottoconto = 0 Then
                    ClientScript.RegisterClientScriptBlock(Me.GetType(), "Errore", "VisualizzaErrore('Non posso proseguire non è indicato il sottoconto abbuono');", True)
                    Exit Sub
                End If


                X.Righe(Riga).MastroPartita = Mastro
                X.Righe(Riga).ContoPartita = Conto
                X.Righe(Riga).SottocontoPartita = Sottoconto
                X.Righe(Riga).Importo = Txt_ImportoAbbuono.Text
                X.Righe(Riga).Descrizione = Txt_Descrizione.Text
                X.Righe(Riga).Segno = "+"
                X.Righe(Riga).Tipo = "CF"
                X.Righe(Riga).RigaDaCausale = 3
                X.Righe(Riga).DareAvere = MyCau.Righe(2).DareAvere
                Riga = Riga + 1
            Else
                If Not IsNothing(X.Righe(Riga)) Then
                    If X.Righe(Riga).Importo > 0 Then
                        X.Righe(Riga).Importo = 0
                    End If
                End If
            End If
        Else
            If Not IsNothing(X.Righe(Riga)) Then
                If X.Righe(Riga).Importo > 0 Then
                    X.Righe(Riga).Importo = 0
                End If
            End If
        End If

        If Txt_Importospese.Text <> "" Then
            If CDbl(Txt_Importospese.Text) <> 0 Then
                If IsNothing(X.Righe(Riga)) Then
                    X.Righe(Riga) = New Cls_MovimentiContabiliRiga
                End If
                Vettore = SplitWords(Txt_Sottocontospese.Text)

                If Vettore.Length > 2 Then
                    Mastro = Val(Vettore(0))
                    Conto = Val(Vettore(1))
                    Sottoconto = Val(Vettore(2))
                End If

                If Mastro > 0 And Sottoconto = 0 Then
                    ClientScript.RegisterClientScriptBlock(Me.GetType(), "Errore", "VisualizzaErrore('Non posso proseguire non è indicato il sottoconto spese');", True)
                    Exit Sub
                End If


                X.Righe(Riga).MastroPartita = Mastro
                X.Righe(Riga).ContoPartita = Conto
                X.Righe(Riga).SottocontoPartita = Sottoconto
                X.Righe(Riga).Importo = Txt_Importospese.Text
                X.Righe(Riga).Descrizione = Txt_Descrizione.Text
                X.Righe(Riga).Segno = "+"
                X.Righe(Riga).Tipo = "CF"
                X.Righe(Riga).RigaDaCausale = 4
                X.Righe(Riga).DareAvere = MyCau.Righe(3).DareAvere
                Riga = Riga + 1
            Else
                If Not IsNothing(X.Righe(Riga)) Then
                    If X.Righe(Riga).Importo > 0 Then
                        X.Righe(Riga).Importo = 0
                    End If
                End If
            End If
        Else
            If Not IsNothing(X.Righe(Riga)) Then
                If X.Righe(Riga).Importo > 0 Then
                    X.Righe(Riga).Importo = 0
                End If
            End If
        End If

        If Txt_ImportoLibero.Text <> "" Then
            If CDbl(Txt_ImportoLibero.Text) <> 0 Then
                If IsNothing(X.Righe(Riga)) Then
                    X.Righe(Riga) = New Cls_MovimentiContabiliRiga
                End If
                Vettore = SplitWords(Txt_SottocontoLibero.Text)

                If Vettore.Length > 2 Then
                    Mastro = Val(Vettore(0))
                    Conto = Val(Vettore(1))
                    Sottoconto = Val(Vettore(2))
                End If

                If Mastro > 0 And Sottoconto = 0 Then
                    ClientScript.RegisterClientScriptBlock(Me.GetType(), "Errore", "VisualizzaErrore('Non posso proseguire non è indicato il sottoconto libero');", True)
                    Exit Sub
                End If

                X.Righe(Riga).MastroPartita = Mastro
                X.Righe(Riga).ContoPartita = Conto
                X.Righe(Riga).SottocontoPartita = Sottoconto
                X.Righe(Riga).Importo = Txt_ImportoLibero.Text
                X.Righe(Riga).Descrizione = Txt_Descrizione.Text
                X.Righe(Riga).Segno = "+"
                X.Righe(Riga).Tipo = "CF"
                If Rb_Dare.Checked = True Then
                    X.Righe(Riga).DareAvere = "D"
                Else
                    X.Righe(Riga).DareAvere = "A"
                End If
                X.Righe(Riga).RigaDaCausale = 6
                Riga = Riga + 1
            Else
                If Not IsNothing(X.Righe(Riga)) Then
                    If X.Righe(Riga).Importo > 0 Then
                        X.Righe(Riga).Importo = 0
                    End If
                End If
            End If
        Else
            If Not IsNothing(X.Righe(Riga)) Then
                If X.Righe(Riga).Importo > 0 Then
                    X.Righe(Riga).Importo = 0
                End If
            End If
        End If

        X.Utente = Session("UTENTE")


        Dim Xleg As Integer
        Dim Legami1 As New Cls_Legami
        Tabella = ViewState("Save_Tab_IP")
        Xleg = 0
        For i = 0 To Tabella.Rows.Count - 1
            Dim TxtImponibile As TextBox = DirectCast(Grid.Rows(i).FindControl("Txt_ImportoPagato"), TextBox)
            Dim Chk_Chiusa As CheckBox = DirectCast(Grid.Rows(i).FindControl("Chk_Chiusa"), CheckBox)

            If TxtImponibile.Text <> "" Then
                If CDbl(TxtImponibile.Text) <> 0 Then
                    Legami1.Importo(Xleg) = TxtImponibile.Text
                    Legami1.NumeroDocumento(Xleg) = Val(Tabella.Rows(i).Item(1))
                    Legami1.NumeroPagamento(Xleg) = X.NumeroRegistrazione
                    Xleg = Xleg + 1
                End If
            End If
        Next

        If X.VerificaRegistroIVA(Session("DC_GENERALE"), Session("DC_TABELLE"), Legami1) = False Then
            ClientScript.RegisterClientScriptBlock(Me.GetType(), "Errore", "VisualizzaErrore('Legami con documenti con il registro iva già chiuso');", True)
            Exit Sub
        End If

        If X.VerificaValidita(Session("DC_GENERALE"), Session("DC_TABELLE"), "I") <> "OK" Then
            ClientScript.RegisterClientScriptBlock(Me.GetType(), "Errore", "VisualizzaErrore('" & X.VerificaValidita(Session("DC_GENERALE"), Session("DC_TABELLE")) & "');", True)
            Exit Sub
        End If

        If X.Scrivi(Session("DC_GENERALE"), Val(Txt_Numero.Text)) = False Then
            ClientScript.RegisterClientScriptBlock(Me.GetType(), "Errore", "VisualizzaErrore('Errore bloccante in modifica/inserimento');", True)
            Exit Sub
        End If

        Txt_Numero.Text = X.NumeroRegistrazione

        Dim TrovatoRitenuta As Boolean = False
        Dim Legami As New Cls_Legami
        Tabella = ViewState("Save_Tab_IP")
        Xleg = 0
        For i = 0 To Tabella.Rows.Count - 1
            Dim TxtImponibile As TextBox = DirectCast(Grid.Rows(i).FindControl("Txt_ImportoPagato"), TextBox)
            Dim Chk_Chiusa As CheckBox = DirectCast(Grid.Rows(i).FindControl("Chk_Chiusa"), CheckBox)

            If TxtImponibile.Text <> "" Then
                If CDbl(TxtImponibile.Text) <> 0 Then
                    Legami.Importo(Xleg) = TxtImponibile.Text
                    Legami.NumeroDocumento(Xleg) = Val(Tabella.Rows(i).Item(1))
                    Legami.NumeroPagamento(Xleg) = X.NumeroRegistrazione

                    If TrovatoRitenuta = False Then
                        Dim Registrazione As New Cls_MovimentoContabile
                        Dim CercaRitenuta As Integer

                        Registrazione.Leggi(Session("DC_GENERALE"), Val(Tabella.Rows(i).Item(1)))
                        For CercaRitenuta = 0 To 300
                            If Not IsNothing(Registrazione.Righe(CercaRitenuta)) Then
                                If Registrazione.Righe(CercaRitenuta).Tipo = "RI" Then
                                    TrovatoRitenuta = True
                                End If
                            End If
                        Next
                    End If

                    Xleg = Xleg + 1

                    If Chk_Chiusa.Checked = True Then
                        Dim Ks As New Cls_Scadenziario
                        Ks.NumeroRegistrazioneContabile = Val(Tabella.Rows(i).Item(1))
                        Ks.Numero = Val(Tabella.Rows(i).Item(8))
                        Ks.Leggi(Session("DC_GENERALE"))

                        Ks.Chiusa = 1
                        Ks.RegistrazioneIncasso = Val(Txt_Numero.Text)
                        Ks.ScriviScadenza(Session("DC_GENERALE"))

                    End If

                End If
            End If
        Next

        Legami.Scrivi(Session("DC_GENERALE"), 0, Val(Txt_Numero.Text))



        If Val(Txt_Numero.Text) > 0 Then
            For i = 0 To Tabella.Rows.Count - 1
                Dim TxtImponibile As TextBox = DirectCast(Grid.Rows(i).FindControl("Txt_ImportoPagato"), TextBox)

                If TxtImponibile.Text <> "" Then
                    If CDbl(TxtImponibile.Text) <> 0 Then
                        Dim MovimentiRitenuta As New Cls_RitenuteAcconto

                        MovimentiRitenuta.ID = 0
                        MovimentiRitenuta.NumeroRegistrazioneDocumento = Val(Tabella.Rows(i).Item(1))
                        MovimentiRitenuta.NumeroRegistrazionePagamento = Val(Txt_Numero.Text)
                        MovimentiRitenuta.PagaRitenutaDelDocumento(Session("DC_GENERALE"))

                    End If
                End If
            Next
        End If


        Txt_Numero.Enabled = False
        Dim MyJs As String


        X.Leggi(Session("DC_GENERALE"), X.NumeroRegistrazione)

        Session("NumeroRegistrazione") = X.NumeroRegistrazione

        'MyJs = "alert('Salvato Registrazione " & X.NumeroRegistrazione & " del " & Txt_DataRegistrazione.Text & "');  "
        'MyJs = "$(document).ready(function() { alert('Salvato Registrazione " & Val(Txt_Numero.Text) & "'); } );"
        'ClientScript.RegisterClientScriptBlock(Me.GetType(), "ModReg", MyJs, True)

        Dim BottoneChiudi As String = "<br/><a href=""#"" onclick=""ChiudiConfermaModifica();""  style=""position: inherit;bottom: 20px;left: 30%;width: 40%;""><input type=""button"" class=""SeniorButton"" value=""CHIUDI"" style=""width: 100%;""></a>"
        MyJs = "<div id=""ConfermaModifica"" class=""confermamodifca""  style=""position:absolute; top:20%; left:40%; width:20%;  height:20%;""><br />Salvato Registrazione " & X.NumeroRegistrazione & "<br/>del " & Txt_DataRegistrazione.Text & BottoneChiudi & "</div>"
        LblBox.Text = MyJs


        Dim DG As New Cls_DatiGenerali

        DG.LeggiDati(Session("DC_TABELLE"))

        If TrovatoRitenuta = False And Val(Txt_Numero.Text) > 0 Then
            Dim MovRit As New Cls_RitenuteAcconto

            If MovRit.RitenutePagamento(Session("DC_GENERALE"), Txt_Numero.Text) = True Then
                TrovatoRitenuta = True
            End If
        End If

        If DG.MovimentiRitenute = 1 And TrovatoRitenuta Then
            MyJs = "$(document).ready( function() { setTimeout(function(){ DialogBox('RitenutePagamento.aspx?NumeroRegistrazione=" & Txt_Numero.Text & "'); }, 1000);}); "
            ScriptManager.RegisterStartupScript(Me, Me.GetType(), "Rit1DialogBox", MyJs, True)
        End If

        Call PulisciRegistrazione()

        Call ImpostaJS()
    End Sub


    Private Sub AggiornaCausale()
        Try
            Dim X As New Cls_CausaleContabile

            X.Leggi(Session("DC_TABELLE"), Dd_CausaleContabile.SelectedValue)

            Dim xS As New Cls_Pianodeiconti

            xS.Mastro = X.Righe(1).Mastro
            xS.Conto = X.Righe(1).Conto
            xS.Sottoconto = X.Righe(1).Sottoconto

            xS.Decodfica(Session("DC_GENERALE"))

            Txt_SottocontoCassa.Text = xS.Mastro & " " & xS.Conto & " " & xS.Sottoconto & " " & xS.Descrizione

            xS.Mastro = X.Righe(2).Mastro
            xS.Conto = X.Righe(2).Conto
            xS.Sottoconto = X.Righe(2).Sottoconto

            xS.Decodfica(Session("DC_GENERALE"))

            Txt_SottocontoAbbuono.Text = xS.Mastro & " " & xS.Conto & " " & xS.Sottoconto & " " & xS.Descrizione

            xS.Mastro = X.Righe(3).Mastro
            xS.Conto = X.Righe(3).Conto
            xS.Sottoconto = X.Righe(3).Sottoconto

            xS.Decodfica(Session("DC_GENERALE"))

            Txt_Sottocontospese.Text = xS.Mastro & " " & xS.Conto & " " & xS.Sottoconto & " " & xS.Descrizione

            xS.Mastro = X.Righe(5).Mastro
            xS.Conto = X.Righe(5).Conto
            xS.Sottoconto = X.Righe(5).Sottoconto

            xS.Decodfica(Session("DC_GENERALE"))

            Txt_SottocontoLibero.Text = xS.Mastro & " " & xS.Conto & " " & xS.Sottoconto & " " & xS.Descrizione

        Catch ex As Exception

        End Try
    End Sub
    Protected Sub Dd_CausaleContabile_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles Dd_CausaleContabile.SelectedIndexChanged
        Call AggiornaCausale()


    End Sub

    Private Sub AggiornaTotaleCasa()
        If Dd_CausaleContabile.SelectedValue = "" Then
            Exit Sub
        End If
        If Txt_ImportoCassa.Text.Trim = "" Then
            Txt_ImportoCassa.Text = "0"
        End If
        If Txt_Importo.Text.Trim = "" Then
            Txt_Importo.Text = "0"
        End If
        If Txt_ImportoAbbuono.Text.Trim = "" Then
            Txt_ImportoAbbuono.Text = "0"
        End If
        If Txt_Importospese.Text.Trim = "" Then
            Txt_Importospese.Text = "0"
        End If
        If Txt_ImportoLibero.Text.Trim = "" Then
            Txt_ImportoLibero.Text = "0"
        End If

        Txt_ImportoCassa.Text = Txt_Importo.Text
        Dim X As New Cls_CausaleContabile
        Dim XDareAvere As String



        X.Leggi(Session("DC_TABELLE"), Dd_CausaleContabile.SelectedValue)

        If CDbl(Txt_ImportoAbbuono.Text) <> 0 Then
            If X.Righe(1).DareAvere <> X.Righe(2).DareAvere Then
                Txt_ImportoCassa.Text = CDbl(Txt_ImportoCassa.Text) + CDbl(Txt_ImportoAbbuono.Text)
            Else
                Txt_ImportoCassa.Text = CDbl(Txt_ImportoCassa.Text) - CDbl(Txt_ImportoAbbuono.Text)
            End If
        End If


        If CDbl(Txt_Importospese.Text) <> 0 Then
            If X.Righe(1).DareAvere <> X.Righe(3).DareAvere Then
                Txt_ImportoCassa.Text = CDbl(Txt_ImportoCassa.Text) + CDbl(Txt_Importospese.Text)
            Else
                Txt_ImportoCassa.Text = CDbl(Txt_ImportoCassa.Text) - CDbl(Txt_Importospese.Text)
            End If
        End If


        If CDbl(Txt_ImportoLibero.Text) <> 0 Then
            XDareAvere = ""
            If Rb_Dare.Checked = True Then
                XDareAvere = "D"
            End If
            If Rb_Avere.Checked = True Then
                XDareAvere = "A"
            End If
            If X.Righe(1).DareAvere <> XDareAvere Then
                Txt_ImportoCassa.Text = CDbl(Txt_ImportoCassa.Text) + CDbl(Txt_ImportoLibero.Text)
            Else
                Txt_ImportoCassa.Text = CDbl(Txt_ImportoCassa.Text) - CDbl(Txt_ImportoLibero.Text)
            End If
        End If
    End Sub


    Protected Sub Txt_Importo_TextChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles Txt_Importo.TextChanged
        Call AggiornaTotaleCasa()
        Call ImpostaJS()
    End Sub

    Protected Sub Txt_ImportoAbbuono_TextChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles Txt_ImportoAbbuono.TextChanged
        Call AggiornaTotaleCasa()
        Call ImpostaJS()
    End Sub

    Protected Sub Txt_Importospese_TextChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles Txt_Importospese.TextChanged
        Call AggiornaTotaleCasa()
        Call ImpostaJS()
    End Sub

    Protected Sub Txt_ImportoLibero_TextChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles Txt_ImportoLibero.TextChanged
        Call AggiornaTotaleCasa()
        Call ImpostaJS()
    End Sub

    Private Sub LeggiRegistrazione()
        Dim X As New Cls_MovimentoContabile

        X.Leggi(Session("DC_GENERALE"), Val(Txt_Numero.Text))

        Txt_DataRegistrazione.Text = X.DataRegistrazione
        Txt_DataBolletta.Text = X.DataBolletta
        Txt_NumeroBolletta.Text = X.NumeroBolletta
        Txt_Descrizione.Text = X.Descrizione
        Dd_CausaleContabile.SelectedValue = X.CausaleContabile        
        DD_CSERV.SelectedValue = X.CentroServizio

        Call AggiornaCausale()

        Dim XL As New Cls_Legami
        XL.Leggi(Session("DC_GENERALE"), 0, Val(Txt_Numero.Text))

        Dim MyCau As New Cls_CausaleContabile

        MyCau.Codice = X.CausaleContabile
        MyCau.Leggi(Session("DC_TABELLE"), X.CausaleContabile)


        Tabella.Clear()
        Tabella.Columns.Clear()
        Tabella.Columns.Add("Copia", GetType(String)) '0
        Tabella.Columns.Add("Registrazione", GetType(String)) '1
        Tabella.Columns.Add("DataDocumento", GetType(String)) '2
        Tabella.Columns.Add("NumeroDocumento", GetType(String)) '3
        Tabella.Columns.Add("ImportoDocumento", GetType(String)) '4
        Tabella.Columns.Add("ImportoPagato", GetType(String)) '5
        Tabella.Columns.Add("Scadenzario", GetType(Boolean)) '6
        Tabella.Columns.Add("Chiusa", GetType(Long)) '7
        Tabella.Columns.Add("NumeroScadenza", GetType(Long)) '8
        Tabella.Columns.Add("DataScadenza", GetType(String)) '9
        Dim I As Integer
        For I = 0 To 100
            If XL.NumeroPagamento(I) = Val(Txt_Numero.Text) Then
                Dim Docum As New Cls_MovimentoContabile
                Docum.Leggi(Session("DC_GENERALE"), XL.NumeroDocumento(I))
                Dim myriga As System.Data.DataRow = Tabella.NewRow()

                myriga(1) = Docum.NumeroRegistrazione
                myriga(2) = Format(Docum.DataDocumento, "dd/MM/yyyy")
                myriga(3) = Docum.NumeroDocumento

                If X.Righe(0).DareAvere = "D" And MyCau.VenditaAcquisti = "V" Then
                    myriga(4) = Format(Math.Abs(Docum.ImportoDocumento(Session("DC_TABELLE")) - Docum.ImportoRitenuta(Session("DC_TABELLE"))), "#,##0.00")                    
                    myriga(5) = Format(XL.Importo(I), "#,##0.00")
                Else
                    myriga(4) = Format(Docum.ImportoDocumento(Session("DC_TABELLE")) - Docum.ImportoRitenuta(Session("DC_TABELLE")), "#,##0.00")
                    If Docum.ImportoDocumento(Session("DC_TABELLE")) - Docum.ImportoRitenuta(Session("DC_TABELLE")) < 0 Then
                        myriga(5) = Format(XL.Importo(I) * -1, "-#,##0.00")
                    Else
                        myriga(5) = Format(XL.Importo(I), "#,##0.00")
                    End If
                End If


                If Request.Item("TIPO") = "SCADENZARIO" Then
                    myriga(6) = False
                Else
                    myriga(6) = False
                End If

                myriga(7) = False
                myriga(8) = -1

                Dim Scadenza As New Cls_Scadenziario


                Scadenza.NumeroRegistrazioneContabile = Docum.NumeroRegistrazione
                Scadenza.RegistrazioneIncasso = Val(Txt_Numero.Text)
                Scadenza.LeggiDocumentoIncasso(Session("DC_GENERALE"))

                If Year(Scadenza.DataScadenza) > 1910 Then
                    myriga(9) = Format(Scadenza.DataScadenza, "dd/MM/yyyy")
                End If
                Tabella.Rows.Add(myriga)
            End If
        Next
        Dim xS1 As New Cls_Pianodeiconti

        xS1.Mastro = X.Righe(0).MastroPartita
        xS1.Conto = X.Righe(0).ContoPartita
        xS1.Sottoconto = X.Righe(0).SottocontoPartita

        xS1.Decodfica(Session("DC_GENERALE"))

        Txt_ClienteFornitore.Text = xS1.Mastro & " " & xS1.Conto & " " & xS1.Sottoconto & " " & xS1.Descrizione
        Txt_Importo.Text = Format(X.Righe(0).Importo, "#,##0.00")

        For I = 0 To X.Righe.Length - 1
            If Not IsNothing(X.Righe(I)) Then
                If X.Righe(I).RigaDaCausale = 2 Then
                    Dim xS As New Cls_Pianodeiconti

                    xS.Mastro = X.Righe(I).MastroPartita
                    xS.Conto = X.Righe(I).ContoPartita
                    xS.Sottoconto = X.Righe(I).SottocontoPartita

                    xS.Decodfica(Session("DC_GENERALE"))

                    Txt_SottocontoCassa.Text = xS.Mastro & " " & xS.Conto & " " & xS.Sottoconto & " " & xS.Descrizione
                End If

                If X.Righe(I).RigaDaCausale = 3 Then
                    Dim xS As New Cls_Pianodeiconti

                    xS.Mastro = X.Righe(I).MastroPartita
                    xS.Conto = X.Righe(I).ContoPartita
                    xS.Sottoconto = X.Righe(I).SottocontoPartita

                    xS.Decodfica(Session("DC_GENERALE"))

                    Txt_SottocontoAbbuono.Text = xS.Mastro & " " & xS.Conto & " " & xS.Sottoconto & " " & xS.Descrizione
                    Txt_ImportoAbbuono.Text = Format(X.Righe(I).Importo, "#,##0.00")
                End If

                If X.Righe(I).RigaDaCausale = 4 Then
                    Dim xS As New Cls_Pianodeiconti

                    xS.Mastro = X.Righe(I).MastroPartita
                    xS.Conto = X.Righe(I).ContoPartita
                    xS.Sottoconto = X.Righe(I).SottocontoPartita

                    xS.Decodfica(Session("DC_GENERALE"))

                    Txt_Sottocontospese.Text = xS.Mastro & " " & xS.Conto & " " & xS.Sottoconto & " " & xS.Descrizione
                    Txt_Importospese.Text = Format(X.Righe(I).Importo, "#,##0.00")
                End If

                If X.Righe(I).RigaDaCausale = 6 Then
                    Dim xS As New Cls_Pianodeiconti

                    xS.Mastro = X.Righe(I).MastroPartita
                    xS.Conto = X.Righe(I).ContoPartita
                    xS.Sottoconto = X.Righe(I).SottocontoPartita

                    xS.Decodfica(Session("DC_GENERALE"))

                    Txt_SottocontoLibero.Text = xS.Mastro & " " & xS.Conto & " " & xS.Sottoconto & " " & xS.Descrizione
                    Txt_ImportoLibero.Text = Format(X.Righe(I).Importo, "#,##0.00")
                    If X.Righe(I).DareAvere = "D" Then
                        Rb_Dare.Checked = True
                        Rb_Avere.Checked = False
                    Else
                        Rb_Dare.Checked = False
                        Rb_Avere.Checked = True
                    End If
                End If
            End If
        Next
        Grid.AutoGenerateColumns = False
        Grid.DataSource = Tabella
        Grid.DataBind()


        If Request.Item("TIPO") <> "SCADENZARIO" Then
            Grid.Columns(3).Visible = False
        End If
        ViewState("Save_Tab_IP") = Tabella
        Call AggiornaTotaleCasa()

    End Sub
    Protected Sub Txt_Numero_TextChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles Txt_Numero.TextChanged
        If Val(Txt_Numero.Text) > 0 Then
            Dim x As New Cls_MovimentoContabile
            x.Leggi(Session("DC_GENERALE"), Val(Txt_Numero.Text))
            Dim Ks As New Cls_CausaleContabile
            Ks.Leggi(Session("DC_TABELLE"), x.CausaleContabile)
            If Ks.Tipo <> "P" Then
                Exit Sub
            End If
            If x.NumeroRegistrazione > 0 Then
                Txt_Numero.Enabled = False
                Call LeggiRegistrazione()
            End If            
        End If
        Call ImpostaJS()
    End Sub

    Private Sub PulisciRegistrazione()
        Lbl_errori.Text = ""

        Txt_Numero.Enabled = True
        Txt_Numero.Text = 0
        'Txt_DataRegistrazione.Text = Format(Now, "dd/MM/yyyy")
        Txt_DataBolletta.Text = ""
        Txt_NumeroBolletta.Text = 0
        Txt_Descrizione.Text = ""
        Dd_CausaleContabile.SelectedValue = ""
        DD_CSERV.SelectedValue = ""

        Txt_ClienteFornitore.Text = ""
        Txt_Importo.Text = 0

        Txt_SottocontoCassa.Text = ""
        Txt_ImportoCassa.Text = 0
        Txt_SottocontoAbbuono.Text = ""
        Txt_ImportoAbbuono.Text = 0
        Txt_Sottocontospese.Text = ""
        Txt_Importospese.Text = 0
        Txt_SottocontoLibero.Text = ""
        Txt_ImportoLibero.Text = ""


        Tabella.Clear()
        Tabella.Columns.Clear()
        Tabella.Columns.Add("Copia", GetType(String))
        Tabella.Columns.Add("Registrazione", GetType(String))
        Tabella.Columns.Add("DataDocumento", GetType(String))
        Tabella.Columns.Add("NumeroDocumento", GetType(String))
        Tabella.Columns.Add("ImportoDocumento", GetType(String))
        Tabella.Columns.Add("ImportoPagato", GetType(String))
        Tabella.Columns.Add("Scadenzario", GetType(Boolean))
        Tabella.Columns.Add("Chiusa", GetType(Long))
        Tabella.Columns.Add("NumeroScadenza", GetType(Long))
        Tabella.Columns.Add("DataScadenza", GetType(String))
        Dim myriga As System.Data.DataRow = Tabella.NewRow()
        If Request.Item("TIPO") = "SCADENZARIO" Then
            myriga(6) = True
            myriga(7) = False
        Else
            myriga(6) = False
            myriga(7) = False
        End If
        myriga(1) = 0
        myriga(2) = ""
        myriga(3) = 0
        myriga(4) = 0
        myriga(5) = 0
        myriga(8) = 0
        Tabella.Rows.Add(myriga)


        Dim k As New Cls_MovimentoContabile

        Grid.AutoGenerateColumns = False
        Grid.DataSource = Tabella
        Grid.DataBind()

        If Request.Item("TIPO") <> "SCADENZARIO" Then
            Grid.Columns(3).Visible = False
        End If

        ViewState("Save_Tab_IP") = Tabella
        Call AggiornaTotaleCasa()
        ImpostaJS()
    End Sub

    Protected Sub Btn_Pulisci_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles Btn_Pulisci.Click
        PulisciRegistrazione()
        Call ImpostaJS()
    End Sub

    Protected Sub ImageButton1_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles ImageButton1.Click

        If Val(Txt_Numero.Text) > 0 Then
            Dim VerificaBollato As New Cls_MovimentoContabile
            VerificaBollato.NumeroRegistrazione = Val(Txt_Numero.Text)
            If VerificaBollato.Bollato(Session("DC_GENERALE")) = True Then
                ClientScript.RegisterClientScriptBlock(Me.GetType(), "Errore", "VisualizzaErrore('Già creato il giornale bollato');", True)
                Exit Sub
            End If
        End If
        If Val(Txt_Numero.Text) = 0 Then
            ClientScript.RegisterClientScriptBlock(Me.GetType(), "Errore", "VisualizzaErrore('Specificare registrazione');", True)
            Exit Sub
        End If
        If Txt_DataRegistrazione.Text = "" Then
            ClientScript.RegisterClientScriptBlock(Me.GetType(), "Errore", "VisualizzaErrore('Specificare data registrazione');", True)
            Exit Sub
        End If

        If Not IsDate(Txt_DataRegistrazione.Text) Then
            ClientScript.RegisterClientScriptBlock(Me.GetType(), "Errore", "VisualizzaErrore('Specificare data registrazione');", True)
            Exit Sub
        End If


        Dim XDatiGenerali As New Cls_DatiGenerali
        Dim AppoggioData As Date

        AppoggioData = Txt_DataRegistrazione.Text
        XDatiGenerali.LeggiDati(Session("DC_TABELLE"))

        If Format(XDatiGenerali.DataChiusura, "yyyyMMdd") >= Format(AppoggioData, "yyyyMMdd") Then
            ClientScript.RegisterClientScriptBlock(Me.GetType(), "Errore", "VisualizzaErrore('Non posso eliminare registrazione,<BR/> già effettuato la chiusura');", True)
            Exit Sub
        End If


        Dim MovRitenuta As New Cls_RitenuteAcconto

        MovRitenuta.NumeroRegistrazionePagamento = Val(Txt_Numero.Text)

        If MovRitenuta.RitenutePagateSuPagamento(Session("DC_GENERALE"), MovRitenuta.NumeroRegistrazionePagamento) Then
            ClientScript.RegisterClientScriptBlock(Me.GetType(), "Errore", "VisualizzaErrore('Non posso eliminare registrazione,<BR/> ritenute pagate');", True)
            Exit Sub
        End If


        MovRitenuta.EliminaPagamento(Session("DC_GENERALE"), MovRitenuta.NumeroRegistrazionePagamento)


        Lbl_errori.Text = ""

        Dim XReg As New Cls_MovimentoContabile



        Dim Log As New Cls_LogPrivacy

        XReg.NumeroRegistrazione = Txt_Numero.Text
        XReg.Leggi(Session("DC_GENERALE"), XReg.NumeroRegistrazione)

        Dim serializer As JavaScriptSerializer
        serializer = New JavaScriptSerializer()

        Log.LogPrivacy(Application("SENIOR"), Session("CLIENTE"), Session("UTENTE"), Session("UTENTEIMPER"), Page.GetType.Name.ToUpper, 0, 0, "", "", Val(Txt_Numero.Text), "", "D", "INCASSOPAGAMENTI", serializer.Serialize(XReg))



        XReg.NumeroRegistrazione = Txt_Numero.Text
        XReg.EliminaRegistrazione(Session("DC_GENERALE"))

        Dim XL As New Cls_Legami

        XL.Leggi(Session("DC_GENERALE"), 0, Txt_Numero.Text)

        Dim i As Integer
        For i = 0 To 300
            If XL.NumeroDocumento(i) > 0 And XL.NumeroPagamento(i) > 0 Then
                Dim Scadenze As New Cls_Scadenziario

                Scadenze.NumeroRegistrazioneContabile = XL.NumeroDocumento(i)
                Scadenze.RegistrazioneIncasso = XL.NumeroPagamento(i)
                Scadenze.ApriScadenzaDocumentoIncasso(Session("DC_GENERALE"))
            End If

            XL.Importo(i) = 0
            XL.NumeroDocumento(i) = 0
            XL.NumeroPagamento(i) = 0
        Next
        XL.Scrivi(Session("DC_GENERALE"), 0, Txt_Numero.Text)

        Txt_DataRegistrazione.Text = ""
        Txt_DataBolletta.Text = ""
        Txt_NumeroBolletta.Text = 0
        Txt_Descrizione.Text = ""
        Dd_CausaleContabile.SelectedValue = ""

        Txt_ClienteFornitore.Text = ""
        Txt_Importo.Text = 0

        Txt_SottocontoCassa.Text = ""
        Txt_ImportoCassa.Text = 0
        Txt_SottocontoAbbuono.Text = ""
        Txt_ImportoAbbuono.Text = 0
        Txt_Sottocontospese.Text = ""
        Txt_Importospese.Text = 0
        Txt_SottocontoLibero.Text = ""
        Txt_ImportoLibero.Text = ""
        Txt_Numero.Text = 0
        Txt_Numero.Enabled = True

        Tabella.Clear()
        Tabella.Columns.Clear()
        Tabella.Columns.Add("Copia", GetType(String))
        Tabella.Columns.Add("Registrazione", GetType(String))
        Tabella.Columns.Add("DataDocumento", GetType(String))
        Tabella.Columns.Add("NumeroDocumento", GetType(String))
        Tabella.Columns.Add("ImportoDocumento", GetType(String))
        Tabella.Columns.Add("ImportoPagato", GetType(String))
        Tabella.Columns.Add("Scadenzario", GetType(Boolean))
        Tabella.Columns.Add("Chiusa", GetType(Long))
        Tabella.Columns.Add("NumeroScadenza", GetType(Long))
        Tabella.Columns.Add("DataScadenza", GetType(String))
        Dim myriga As System.Data.DataRow = Tabella.NewRow()
        If Request.Item("TIPO") = "SCADENZARIO" Then
            myriga(6) = True
            myriga(7) = False
        Else
            myriga(6) = False
            myriga(7) = False
        End If
        myriga(1) = 0
        myriga(2) = ""
        myriga(3) = 0
        myriga(4) = 0
        myriga(5) = 0
        myriga(8) = 0
        Tabella.Rows.Add(myriga)

        Grid.AutoGenerateColumns = False
        Grid.DataSource = Tabella
        Grid.DataBind()

        If Request.Item("TIPO") <> "SCADENZARIO" Then
            Grid.Columns(3).Visible = False
        End If

        ViewState("Save_Tab_IP") = Tabella
        Call AggiornaTotaleCasa()
        Call ImpostaJS()
    End Sub

    Protected Sub Grid_RowDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs) Handles Grid.RowDataBound
        If e.Row.RowType = DataControlRowType.DataRow Then
            Dim Txt_ImportoPagato As TextBox = DirectCast(e.Row.FindControl("Txt_ImportoPagato"), TextBox)

            Dim Chk_Chiusa As CheckBox = DirectCast(e.Row.FindControl("Chk_Chiusa"), CheckBox)

            If Not IsNothing(Txt_ImportoPagato) Then
                Dim MyJs As String

                MyJs = "$(document).ready(function() { $('#" & Txt_ImportoPagato.ClientID & "').keypress(function() { ForceNumericInput($('#" & Txt_ImportoPagato.ClientID & "').val(), true, true); } );     $('#" & Txt_ImportoPagato.ClientID & "').blur(function() { var ap = formatNumber ($('#" & Txt_ImportoPagato.ClientID & "').val(),2); $('#" & Txt_ImportoPagato.ClientID & "').val(ap); }); });"
                ClientScript.RegisterClientScriptBlock(Me.GetType(), "visualizzaRitIm", MyJs, True)

                REM MyJs = "$(document).ready(function() { $('#" & Txt_ImportoPagato.ClientID & "').blur(function() { var ap = formatNumber ($('#" & Txt_ImportoPagato.ClientID & "').val(),2); $('#" & Txt_ImportoPagato.ClientID & "').val(ap); }); });"
                REM ClientScript.RegisterClientScriptBlock(Me.GetType(), "ImpPagFor", MyJs, True)

                REM ScriptManager.RegisterStartupScript(Me, Me.GetType(), "visualizzaRitIm", MyJs, True)
            End If
            'Txt_ImportoPagato.Enabled = True
            'Chk_Chiusa.Visible = True
            If Tabella.Rows(e.Row.DataItemIndex).Item(8) = -1 And Request.Item("TIPO") = "SCADENZARIO" Then

                'Txt_ImportoPagato.Visible = False
                Chk_Chiusa.Visible = False

                'e.Row.Cells(0).Text = ""
                e.Row.BackColor = System.Drawing.Color.Gray
            End If
            Dim NumeroRegistrazione As Long

            NumeroRegistrazione = Val(e.Row.Cells(1).Text)

            e.Row.Cells(1).Text = "<label id=""lbl" & e.Row.Cells(1).Text & """ ondblclick=""DialogBoxBigDocumenti('documenti.aspx?NumeroRegistrazione=" & NumeroRegistrazione & "');""  onmouseout=""deletebox('lbl" & e.Row.Cells(1).Text & "');"" onmouseover=""apribox(" & NumeroRegistrazione & ",'lbl" & e.Row.Cells(1).Text & "');"">" & e.Row.Cells(1).Text & "</label>"


        End If

    End Sub


    Function VerificaSeInserito() As Boolean
        Dim cn As OleDbConnection

        cn = New Data.OleDb.OleDbConnection(Session("DC_GENERALE"))

        cn.Open()
        Dim cmdTesta As New OleDbCommand()
        cmdTesta.CommandText = "SELECT * FROM MovimentiContabiliTesta  Where Utente = ? And DATAAGGIORNAMENTO >= ?"
        cmdTesta.Connection = cn
        cmdTesta.Parameters.AddWithValue("@Utente", Session("UTENTE"))
        cmdTesta.Parameters.AddWithValue("@Utente", Now.AddSeconds(-2))
        Dim ReadTesta As OleDbDataReader = cmdTesta.ExecuteReader()
        If ReadTesta.Read Then
            ReadTesta.Close()

            cn.Close()
            Return True
        End If
        ReadTesta.Close()

        cn.Close()
        Return False
    End Function

    Protected Sub BtnRicerca_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles BtnRicerca.Click
        Dim MyJs As String

        Dim Mastro As Long
        Dim Conto As Long
        Dim Sottoconto As Long
        Dim Vettore(100) As String




        Vettore = SplitWords(Txt_ClienteFornitore.Text)

        If Vettore.Length > 2 Then
            Mastro = Val(Vettore(0))
            Conto = Val(Vettore(1))
            Sottoconto = Val(Vettore(2))
        End If

        If Request.Item("TIPO") = "SCADENZARIO" Then
            If Mastro > 0 Then
                MyJs = "$(document).ready(function() {  setTimeout(function(){ DialogBoxBig('Ricerca.aspx?TIPO=INCS&MASTRO=" & Mastro & "&CONTO=" & Conto & "&SOTTOCONTO=" & Sottoconto & "');}, 500);});"
            Else
                MyJs = "$(document).ready(function() {  setTimeout(function(){DialogBoxBig('Ricerca.aspx?TIPO=INCS');}, 500);});"
            End If
        Else
            If Mastro > 0 Then
                MyJs = "$(document).ready(function() {  setTimeout(function(){ DialogBoxBig('Ricerca.aspx?TIPO=INC&MASTRO=" & Mastro & "&CONTO=" & Conto & "&SOTTOCONTO=" & Sottoconto & "');}, 500);});"
            Else
                MyJs = "$(document).ready(function() {  setTimeout(function(){ DialogBoxBig('Ricerca.aspx?TIPO=INC');}, 500);});"
            End If
        End If

        ScriptManager.RegisterClientScriptBlock(Me, Me.GetType(), "RicercaDOC", MyJs, True)

    End Sub

    Protected Sub Btn_Esci_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles Btn_Esci.Click
        Esci()
    End Sub

    Private Sub Esci()
        If Request.Item("Chiamante") = "PrimaNota" Then

            Dim Data As Date = Now


            Try
                Data = Txt_DataRegistrazione.Text
            Catch ex As Exception

            End Try

            Response.Redirect("primanota.aspx?Chiamante=PrimaNote&Data=" & Format(Data, "yyyyMMdd"))
        End If

        If Request.Item("TIPO") = "SCADENZARIO" Then
            'Response.Redirect("UltimiMovimenti.aspx?TIPO=INCS")
            If Request.Item("PAGINA") = "" Then
                Response.Redirect("UltimiMovimenti.aspx?TIPO=INCS")
            Else
                Response.Redirect("UltimiMovimenti.aspx?CHIAMATA=RITORNO&PAGINA=" & Request.Item("PAGINA") & "&TIPO=INCS")
            End If
        Else
            'Response.Redirect("UltimiMovimenti.aspx?TIPO=INC")
            If Request.Item("PAGINA") = "" Then
                Response.Redirect("UltimiMovimenti.aspx?TIPO=INC")
            Else
                Response.Redirect("UltimiMovimenti.aspx?CHIAMATA=RITORNO&PAGINA=" & Request.Item("PAGINA") & "&TIPO=INC")
            End If
        End If
    End Sub

    Protected Sub Btn_DistribuisciImporto_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles Btn_DistribuisciImporto.Click
        Dim i As Integer
        Dim Importo As Double
        Dim xImporto As Double

        Try
            Tabella = ViewState("Save_Tab_IP")

            Importo = Txt_Importo.Text
            i = 0
            Do While Importo > 0

                If Tabella.Rows.Count = i Then
                    Exit Sub
                End If

                If Tabella.Rows(i).Item(4).ToString = "" Then
                    xImporto = 0
                Else
                    xImporto = CDbl(Tabella.Rows(i).Item(4).ToString)
                End If


                If xImporto > 0 Then
                    If xImporto > Importo Then
                        Dim TxtImponibile As TextBox = DirectCast(Grid.Rows(i).FindControl("Txt_ImportoPagato"), TextBox)
                        TxtImponibile.Text = Format(Importo, "#,##0.00")
                        Importo = 0
                    Else
                        Dim TxtImponibile As TextBox = DirectCast(Grid.Rows(i).FindControl("Txt_ImportoPagato"), TextBox)
                        TxtImponibile.Text = Format(xImporto, "#,##0.00")
                        Importo = Importo - xImporto
                    End If
                End If

                i = i + 1
            Loop

        Catch ex As Exception

        End Try

        Call ImpostaJS()
    End Sub

    Protected Sub Timer1_Tick(ByVal sender As Object, ByVal e As System.EventArgs) Handles Timer1.Tick
        If Trim(Session("UTENTE")) = "" Then
            If Request.ServerVariables("URL").ToUpper.IndexOf("SENIORWEBBLANCO") > 0 Then
                Response.Redirect("/ODV/Login.aspx")
                Exit Sub
            Else
                Response.Redirect("/SeniorWeb/Login.aspx")
                Exit Sub
            End If
        End If
    End Sub


    Private Sub MettiInvisibileJS()
        Dim MyJs As String


        MyJs = "var els = document.getElementsByTagName(""*"");"

        MyJs = MyJs & "for (var i=0;i<els.length;i++)"
        MyJs = MyJs & "if ( els[i].id ) { "
        MyJs = MyJs & " var appoggio =els[i].id; "

        MyJs = MyJs & " if ((appoggio.match('BOTTONEHOME')!= null) || (appoggio.match('BOTTONERICERCA')!= null) || (appoggio.match('BOTTONEADDSOCIO')!= null)) {  "
        MyJs = MyJs & " $(els[i]).hide();"
        MyJs = MyJs & "    }"


        MyJs = MyJs & "} "


        'TabContainer1_Tab_Anagrafica_Txt_ClienteFornitore



        'MyJs = "$(document).ready(function() { $('.myClass').keypress(function() { handleEnter($('.myClass').val(), true, true); } );     $('#" & Txt_ImportoPagato.ClientID & "').blur(function() { var ap = formatNumber ($('#" & Txt_ImportoPagato.ClientID & "').val(),2); $('#" & Txt_ImportoPagato.ClientID & "').val(ap); }); });"
        ScriptManager.RegisterStartupScript(Me, Me.GetType(), "MettiInsibileJS", MyJs, True)
        'ClientScript.RegisterClientScriptBlock(Me.GetType(), "MettiInsibileJSCS", MyJs, True)
    End Sub

    Protected Sub BtnGiroconto_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles BtnGiroconto.Click
        Dim DtGen As New Cls_DatiGenerali


        DtGen.LeggiDati(Session("DC_TABELLE"))

        If DtGen.GirocontoRitenuta = 1 Then
            Dim MovimentiContabili As New Cls_MovimentoContabile
            Dim Riga As Integer
            Dim MySql As String

            MovimentiContabili.NumeroRegistrazione = Session("NumeroRegistrazione")
            MovimentiContabili.Leggi(Session("DC_GENERALE"), MovimentiContabili.NumeroRegistrazione)


            For Riga = 0 To 300
                If Not IsNothing(MovimentiContabili.Righe(Riga)) Then
                    If MovimentiContabili.Righe(Riga).RigaDaCausale = 5 Then
                        MovimentiContabili.Righe(Riga) = Nothing
                    End If
                End If
            Next

            Dim Indice As Integer = 0

            Riga = 0

            For Indice = 0 To 300
                If Not IsNothing(MovimentiContabili.Righe(Indice)) Then
                    Riga = Riga + 1
                End If
            Next
            Riga = Riga - 1

  
            Dim cn As OleDbConnection
            Dim StringaConnessione As String = Session("DC_GENERALE")
            Dim StringaConnessioneTabelle As String = Session("DC_TABELLE")
            Dim CausaleContabile As New Cls_CausaleContabile
            Dim ImportoTotale As Double = 0

            CausaleContabile.Leggi(StringaConnessioneTabelle, MovimentiContabili.CausaleContabile)

            cn = New Data.OleDb.OleDbConnection(Session("DC_GENERALE"))

            cn.Open()

            Dim cmd As New OleDbCommand()


            MySql = "Select TipoRitenuta, sum(ImportoRitenuta) From MovimentiRitenute where NumeroRegistrazionePagamento = ? Group by TipoRitenuta"
            cmd.CommandText = MySql
            cmd.Connection = cn
            cmd.Parameters.AddWithValue("@NumeroRegistrazionePagamento", Session("NumeroRegistrazione"))
            Dim myPOSTreader As OleDbDataReader = cmd.ExecuteReader()
            Do While myPOSTreader.Read
                Riga = Riga + 1

                If IsNothing(MovimentiContabili.Righe(Riga)) Then
                    MovimentiContabili.Righe(Riga) = New Cls_MovimentiContabiliRiga
                End If

                MovimentiContabili.Righe(Riga).MastroPartita = CausaleContabile.Righe(4).Mastro
                MovimentiContabili.Righe(Riga).ContoPartita = CausaleContabile.Righe(4).Conto
                MovimentiContabili.Righe(Riga).SottocontoPartita = CausaleContabile.Righe(4).Sottoconto
                MovimentiContabili.Righe(Riga).Importo = campodbN(myPOSTreader.Item(1))
                MovimentiContabili.Righe(Riga).Segno = "+"
                MovimentiContabili.Righe(Riga).Tipo = ""
                MovimentiContabili.Righe(Riga).Descrizione = ""
                MovimentiContabili.Righe(Riga).RigaDaCausale = 5
                MovimentiContabili.Righe(Riga).DareAvere = CausaleContabile.Righe(4).DareAvere

                Riga = Riga + 1

                If IsNothing(MovimentiContabili.Righe(Riga)) Then
                    MovimentiContabili.Righe(Riga) = New Cls_MovimentiContabiliRiga
                End If
                Dim TipoRitenuta As New ClsRitenuta

                TipoRitenuta.Codice = campodb(myPOSTreader.Item("TipoRitenuta"))
                TipoRitenuta.Leggi(StringaConnessioneTabelle)

                MovimentiContabili.Righe(Riga).MastroPartita = TipoRitenuta.Mastro
                MovimentiContabili.Righe(Riga).ContoPartita = TipoRitenuta.Conto
                MovimentiContabili.Righe(Riga).SottocontoPartita = TipoRitenuta.Sottoconto
                MovimentiContabili.Righe(Riga).Importo = campodbN(myPOSTreader.Item(1))
                MovimentiContabili.Righe(Riga).Segno = "+"
                MovimentiContabili.Righe(Riga).Tipo = ""
                MovimentiContabili.Righe(Riga).Descrizione = ""
                MovimentiContabili.Righe(Riga).RigaDaCausale = 5
                If CausaleContabile.Righe(4).DareAvere = "D" Then
                    MovimentiContabili.Righe(Riga).DareAvere = "A"
                Else
                    MovimentiContabili.Righe(Riga).DareAvere = "D"
                End If
                ImportoTotale = ImportoTotale + campodbN(myPOSTreader.Item(1))
            Loop
            myPOSTreader.Close()
            MovimentiContabili.Scrivi(StringaConnessione, MovimentiContabili.NumeroRegistrazione)



            ClientScript.RegisterClientScriptBlock(Me.GetType(), "Errore", "alert('Creato/Modifica giroconto ritenuta " & Format(ImportoTotale, "#,##0.00") & "');", True)
        End If
    End Sub

    Protected Sub Txt_ClienteFornitore_TextChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles Txt_ClienteFornitore.TextChanged
        If Dd_CausaleContabile.SelectedValue = "" Then
            ClientScript.RegisterClientScriptBlock(Me.GetType(), "Errore", "VisualizzaErrore('Specificare causale contabile');", True)
            REM Lbl_errori.Text = "Specificare causale contabile"
            Exit Sub
        End If
        If Txt_ClienteFornitore.Text = "" Then
            ClientScript.RegisterClientScriptBlock(Me.GetType(), "Errore", "VisualizzaErrore('Specificare il conto cliente/fornitore');", True)
            REM Lbl_errori.Text = "Specificare il conto cliente/fornitore"
            Exit Sub
        End If
        Lbl_errori.Text = ""
        Call PienaGriglia()
        ImpostaJS()
    End Sub
End Class
