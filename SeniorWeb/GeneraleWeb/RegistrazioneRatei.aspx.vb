﻿Imports Microsoft.VisualBasic
Imports System.Data.OleDb
Imports System.Web.Hosting
Imports System.Data.SqlTypes
Partial Class GeneraleWeb_RegistrazioneRatei
    Inherits System.Web.UI.Page
    Dim Tabella As New System.Data.DataTable("tabella")

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Session("UTENTE") = "" Then
            Response.Redirect("..\Login.aspx")
            Exit Sub
        End If

        Dim MyJs As String
        MyJs = "$(" & Chr(34) & "#" & Txt_DataRegistrazione.ClientID & Chr(34) & ").mask(""99/99/9999""); "

        MyJs = MyJs & " $(" & Chr(34) & "#" & Txt_DataRegistrazione.ClientID & Chr(34) & ").datepicker({ changeMonth: true,changeYear: true,  yearRange: ""1990:" & Year(Now) + 5 & """},$.datepicker.regional[""it""]);"

        ScriptManager.RegisterStartupScript(Me, Me.GetType(), "FormatDataApertura", MyJs, True)

        If Page.IsPostBack = True Then
            Exit Sub
        End If


        Dim K1 As New Cls_SqlString

        Dim Barra As New Cls_BarraSenior

        If Not IsNothing(Session("RicercaGeneraleSQLString")) Then
            K1 = Session("RicercaGeneraleSQLString")
        End If

        Lbl_BarraSenior.Text = Barra.CodiceBarra(Application("SENIOR"), Session("UTENTE"), Page, K1)


        Dim MCausaliContabili As New Cls_CausaleContabile

        MCausaliContabili.UpDateDropBox(Session("DC_TABELLE"), Dd_CausaleContabile)


        Txt_Anno.Text = Year(Now)
    End Sub


    Function campodbd(ByVal oggetto As Object) As Date
        If IsDBNull(oggetto) Then
            Return Nothing
        Else
            Return oggetto
        End If
    End Function


    Function campodb(ByVal oggetto As Object) As String
        If IsDBNull(oggetto) Then
            Return ""
        Else
            Return oggetto
        End If
    End Function

    Function campodbn(ByVal oggetto As Object) As Double
        If IsDBNull(oggetto) Then
            Return 0
        Else
            Return oggetto
        End If
    End Function


    Private Function CreaRegistrazione(ByVal tipo As String, ByVal Mastro As Long, ByVal Conto As Long, ByVal SottoConto As Long, ByVal MastroA As Long, ByVal ContoA As Long, ByVal SottoContoA As Long, ByVal Totale As Double) As Long
        Dim Registrazione As New Cls_MovimentoContabile

        Registrazione.DataRegistrazione = Txt_DataRegistrazione.Text
        Registrazione.CausaleContabile = tipo
        Registrazione.Utente = Session("UTENTE")

        Dim CausaleContabile As New Cls_CausaleContabile

        CausaleContabile.Codice = tipo
        CausaleContabile.Leggi(Session("DC_TABELLE"), CausaleContabile.Codice)


        Dim CSe As New Cls_CategoriaCespite


        If IsNothing(Registrazione.Righe(0)) Then
            Registrazione.Righe(0) = New Cls_MovimentiContabiliRiga
        End If
        Registrazione.Righe(0).MastroPartita = Mastro
        Registrazione.Righe(0).ContoPartita = Conto
        Registrazione.Righe(0).SottocontoPartita = SottoConto
        Registrazione.Righe(0).DareAvere = "D"
        Registrazione.Righe(0).Segno = "+"
        Registrazione.Righe(0).Importo = Totale
        Registrazione.Righe(0).RigaRegistrazione = 1
        Registrazione.Righe(0).RigaDaCausale = 1

        If IsNothing(Registrazione.Righe(1)) Then
            Registrazione.Righe(1) = New Cls_MovimentiContabiliRiga
        End If
        Registrazione.Righe(1).MastroPartita = MastroA
        Registrazione.Righe(1).ContoPartita = ContoA
        Registrazione.Righe(1).SottocontoPartita = SottoContoA
        Registrazione.Righe(1).DareAvere = "A"
        Registrazione.Righe(1).Segno = "+"
        Registrazione.Righe(1).Importo = Totale
        Registrazione.Righe(1).RigaRegistrazione = 2
        Registrazione.Righe(1).RigaDaCausale = 2


        Registrazione.Scrivi(Session("DC_GENERALE"), Registrazione.NumeroRegistrazione)

        CreaRegistrazione = Registrazione.NumeroRegistrazione
    End Function

    Private Sub CreateRegistrazione()
        Dim cn As OleDbConnection
        Dim MySql As String

        cn = New Data.OleDb.OleDbConnection(Session("DC_GENERALE"))

        cn.Open()



        Tabella.Clear()
        Tabella.Columns.Add("MastroDare", GetType(String))
        Tabella.Columns.Add("ContoDare", GetType(String))
        Tabella.Columns.Add("SottocontoDare", GetType(String))
        Tabella.Columns.Add("DecodificaDare", GetType(String))

        Tabella.Columns.Add("MastroAvere", GetType(String))
        Tabella.Columns.Add("ContoAvere", GetType(String))
        Tabella.Columns.Add("SottocontoAvere", GetType(String))
        Tabella.Columns.Add("DecodificaAvere", GetType(String))

        Tabella.Columns.Add("Importo", GetType(String))
        Tabella.Columns.Add("Registrazione", GetType(String))



        Dim Cespiti As New Cls_ParametriCespiti


        Cespiti.LeggiParametri(Session("DC_GENERALE"))

        Dim MastroAvere As Integer = 0
        Dim ContoAvere As Integer = 0
        Dim SottoContoAvere As Integer = 0

        Dim MastroDare As Integer = 0
        Dim ContoDare As Integer = 0
        Dim SottoContoDare As Integer = 0
        Dim Totale As Double = 0

        Dim cmdCsp As New OleDbCommand()
        'MastroDare  = 7 and ContoDare =20 and SottoContoDare = 1 and 
        cmdCsp.CommandText = "SELECT  ServiziodAl,Servizioal  ,[MastroDare]  ,[ContoDare]   ,[SottoContoDare]   ,[MastroAvere]    ,[ContoAvere]   ,[SottoContoAvere]   ,ImportoTotale FROM RateiRisconti where YEAR((Select DataRegistrazione From MovimentiContabiliTesta where RateiRisconti.[NumeroRegistrazione] =MovimentiContabiliTesta.NumeroRegistrazione)) = " & Txt_Anno.Text & " order by [MastroAvere],[ContoAvere],[SottoContoAvere]"
        cmdCsp.Connection = cn
        Dim RsCespiti As OleDbDataReader = cmdCsp.ExecuteReader()
        Do While RsCespiti.Read
            If (MastroAvere <> campodbn(RsCespiti.Item("MastroAvere")) Or _
               ContoAvere <> campodbn(RsCespiti.Item("ContoAvere")) Or _
               SottoContoAvere <> campodbn(RsCespiti.Item("SottoContoAvere"))) And Totale > 0 Then
                Dim NumeroSS As Long = 0
                If Chk_Prova.Checked = False Then
                    If RD_Chiusure.Checked Then
                        NumeroSS = CreaRegistrazione(Dd_CausaleContabile.SelectedValue, MastroDare, ContoDare, SottoContoDare, MastroAvere, ContoAvere, SottoContoAvere, Totale)
                    Else
                        NumeroSS = CreaRegistrazione(Dd_CausaleContabile.SelectedValue, MastroAvere, ContoAvere, SottoContoAvere, MastroDare, ContoDare, SottoContoDare, Totale)
                    End If
                End If
                If RD_Chiusure.Checked Then
                    Dim myriga As System.Data.DataRow = Tabella.NewRow()
                    myriga(0) = MastroDare
                    myriga(1) = ContoDare
                    myriga(2) = SottoContoDare
                    Dim MS As New Cls_Pianodeiconti

                    MS.Descrizione = ""
                    MS.Mastro = MastroDare
                    MS.Conto = ContoDare
                    MS.Sottoconto = SottoContoDare
                    MS.Decodfica(Session("DC_GENERALE"))

                    myriga(3) = MS.Descrizione


                    myriga(4) = MastroAvere
                    myriga(5) = ContoAvere
                    myriga(6) = SottoContoAvere

                    MS.Descrizione = ""
                    MS.Mastro = MastroAvere
                    MS.Conto = ContoAvere
                    MS.Sottoconto = SottoContoAvere
                    MS.Decodfica(Session("DC_GENERALE"))

                    myriga(7) = MS.Descrizione

                    myriga(8) = Format(Totale, "#,##0.00")

                    myriga(9) = NumeroSS

                    Tabella.Rows.Add(myriga)
                Else
                    Dim myriga As System.Data.DataRow = Tabella.NewRow()
                    myriga(0) = MastroAvere
                    myriga(1) = ContoAvere
                    myriga(2) = SottoContoAvere
                    Dim MS As New Cls_Pianodeiconti

                    MS.Descrizione = ""
                    MS.Mastro = MastroAvere
                    MS.Conto = ContoAvere
                    MS.Sottoconto = SottoContoAvere
                    MS.Decodfica(Session("DC_GENERALE"))

                    myriga(3) = MS.Descrizione


                    myriga(4) = MastroDare
                    myriga(5) = ContoDare
                    myriga(6) = SottoContoDare

                    MS.Descrizione = ""
                    MS.Mastro = MastroDare
                    MS.Conto = ContoDare
                    MS.Sottoconto = SottoContoDare
                    MS.Decodfica(Session("DC_GENERALE"))

                    myriga(7) = MS.Descrizione

                    myriga(8) = Format(Totale, "#,##0.00")

                    myriga(9) = NumeroSS
                    Tabella.Rows.Add(myriga)

                End If
                Totale = 0
            End If



            Dim MioImpGG As Integer
            Dim GGFineAnno As Integer
            Dim GGDaInizioAnno As Integer

            MioImpGG = DateDiff("d", campodbd(RsCespiti.Item("ServizioDal")), campodbd(RsCespiti.Item("Servizioal"))) + 1
            GGFineAnno = DateDiff("d", campodbd(RsCespiti.Item("ServizioDal")), DateSerial(Txt_Anno.Text, 12, 31)) + 1
            GGDaInizioAnno = DateDiff("d", DateSerial(Txt_Anno.Text + 1, 1, 1), campodbd(RsCespiti.Item("Servizioal"))) + 1
            If campodbn(RsCespiti.Item("ImportoTotale")) <> 0 Then

                'WrRs.Item("ImportoAnnoPrecedente") = Math.Round(campodbn(RsCespiti.Item("ImportoTotale")) / MioImpGG * GGFineAnno, 2)
                Totale = Totale + Math.Round(campodbn(RsCespiti.Item("ImportoTotale")) / MioImpGG * GGDaInizioAnno, 2)
            End If


            MastroAvere = campodbn(RsCespiti.Item("MastroAvere"))
            ContoAvere = campodbn(RsCespiti.Item("ContoAvere"))
            SottoContoAvere = campodbn(RsCespiti.Item("SottoContoAvere"))


            MastroDare = campodbn(RsCespiti.Item("MastroDare"))
            ContoDare = campodbn(RsCespiti.Item("ContoDare"))
            SottoContoDare = campodbn(RsCespiti.Item("SottoContoDare"))

        Loop
        RsCespiti.Close()
        If Totale > 0 Then
            Dim NumeroSS As Integer = 0

            If Chk_Prova.Checked = False Then
                If RD_Chiusure.Checked Then
                    NumeroSS = CreaRegistrazione(Dd_CausaleContabile.SelectedValue, MastroDare, ContoDare, SottoContoDare, MastroAvere, ContoAvere, SottoContoAvere, Totale)
                Else
                    NumeroSS = CreaRegistrazione(Dd_CausaleContabile.SelectedValue, MastroAvere, ContoAvere, SottoContoAvere, MastroDare, ContoDare, SottoContoDare, Totale)
                End If
            End If
            If RD_Chiusure.Checked Then
                Dim myriga As System.Data.DataRow = Tabella.NewRow()
                myriga(0) = MastroDare
                myriga(1) = ContoDare
                myriga(2) = SottoContoDare
                Dim MS As New Cls_Pianodeiconti

                MS.Descrizione = ""
                MS.Mastro = MastroDare
                MS.Conto = ContoDare
                MS.Sottoconto = SottoContoDare
                MS.Decodfica(Session("DC_GENERALE"))

                myriga(3) = MS.Descrizione


                myriga(4) = MastroAvere
                myriga(5) = ContoAvere
                myriga(6) = SottoContoAvere

                MS.Descrizione = ""
                MS.Mastro = MastroAvere
                MS.Conto = ContoAvere
                MS.Sottoconto = SottoContoAvere
                MS.Decodfica(Session("DC_GENERALE"))

                myriga(7) = MS.Descrizione

                myriga(8) = Format(Totale, "#,##0.00")
                myriga(9) = NumeroSS
                Tabella.Rows.Add(myriga)
            Else
                Dim myriga As System.Data.DataRow = Tabella.NewRow()
                myriga(0) = MastroAvere
                myriga(1) = ContoAvere
                myriga(2) = SottoContoAvere
                Dim MS As New Cls_Pianodeiconti

                MS.Descrizione = ""
                MS.Mastro = MastroAvere
                MS.Conto = ContoAvere
                MS.Sottoconto = SottoContoAvere
                MS.Decodfica(Session("DC_GENERALE"))

                myriga(3) = MS.Descrizione


                myriga(4) = MastroDare
                myriga(5) = ContoDare
                myriga(6) = SottoContoDare

                MS.Descrizione = ""
                MS.Mastro = MastroDare
                MS.Conto = ContoDare
                MS.Sottoconto = SottoContoDare
                MS.Decodfica(Session("DC_GENERALE"))

                myriga(7) = MS.Descrizione

                myriga(8) = Format(Totale, "#,##0.00")
                myriga(9) = NumeroSS

                Tabella.Rows.Add(myriga)

            End If
        Totale = 0
        End If

        cn.Close()



        GridView1.AutoGenerateColumns = True
        GridView1.DataSource = Tabella
        GridView1.DataBind()
    End Sub

    Protected Sub ImageButton1_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles ImageButton1.Click
        If Not IsDate(Txt_DataRegistrazione.Text) Then
            ClientScript.RegisterClientScriptBlock(Me.GetType(), "Errore", "VisualizzaErrore('Data Registrazione formalmente errata');", True)
            Exit Sub
        End If

        If Dd_CausaleContabile.SelectedValue = "" Then
            ClientScript.RegisterClientScriptBlock(Me.GetType(), "Errore", "VisualizzaErrore('Causale contabile formalmente non indicato');", True)
            Exit Sub
        End If
        CreateRegistrazione()
    End Sub

    Protected Sub Btn_Esci_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles Btn_Esci.Click
        Response.Redirect("Menu_Servizi.aspx")
    End Sub

    Protected Sub Btn_Esci_Command(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.CommandEventArgs) Handles Btn_Esci.Command

    End Sub
End Class
