﻿Imports Microsoft.VisualBasic
Imports System.Data.OleDb
Imports System.Web.Hosting
Imports CrystalDecisions.CrystalReports.Engine
Imports CrystalDecisions.Shared
Imports System.IO

Partial Class GeneraleWeb_stamparegistriiva
    Inherits System.Web.UI.Page

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Trim(Session("UTENTE")) = "" Then
            Response.Redirect("../Login.aspx")
            Exit Sub
        End If



        If Page.IsPostBack = True Then Exit Sub

        Dim K1 As New Cls_SqlString

        Dim Barra As New Cls_BarraSenior

        If Not IsNothing(Session("RicercaAnagraficaSQLString")) Then
            Try
                K1 = Session("RicercaAnagraficaSQLString")
            Catch ex As Exception

            End Try
        End If

        Lbl_BarraSenior.Text = Barra.CodiceBarra(Application("SENIOR"), Session("UTENTE"), Page, K1)


        Dim MyRegIva As New Cls_RegistroIVA


        MyRegIva.UpDateDropBox(Session("DC_TABELLE"), DD_RegistroIVA)

        Txt_Anno.Text = Year(Now)

        ImageButton3.Visible = False

    End Sub

    Private Sub CreaRegistroIVA()
        Dim MyRs As New ADODB.Recordset
        Dim RigaDaCausaleRs As New ADODB.Recordset
        Dim WrRs As New ADODB.Recordset
        Dim RsReg As New ADODB.Recordset
        Dim Inserito As Boolean
        Dim RsDecodifica As New ADODB.Recordset
        Dim StampeDb As New ADODB.Connection
        Dim TabelleDb As New ADODB.Connection
        Dim GeneraleDb As New ADODB.Connection
        Dim WSocieta As String
        Dim Registro As Long
        Dim MySql As String
        Dim Condizione As String
        Dim AcquistiVendite As String
        Dim MeseDa As Long
        Dim MeseA As Long
        Dim MeseArrivo As Long
        Dim MeseEffettivo As Long
        Dim Protocollo As Long
        Dim Entra As Boolean
        Dim numero As Long
        Dim Importo As Double
        Dim Imponibile As Double
        Dim Contr As String
        Dim tipo As String
        Dim Detraibile As Double
        Dim AppoggioProrata As Double


        ViewState("PRINTERKEY") = Format(Now, "yyyyMMddHHmmss") & Session.SessionID

        Session("Errori") = ""

        StampeDb.Open(Session("StampeFinanziaria"))
        TabelleDb.Open(Session("DC_TABELLE"))
        GeneraleDb.Open(Session("DC_GENERALE"))

        StampeDb.Execute("Delete From RegistroIVA where PRINTERKEY = '" & ViewState("PRINTERKEY") & "'")

        Dim XSoc As New Cls_DecodificaSocieta

        WSocieta = XSoc.DecodificaSocieta(Session("DC_TABELLE"))


        Registro = DD_RegistroIVA.SelectedValue

        MySql = "SELECT TipoIva From TipoRegistro Where Tipo = " & Registro
        RsReg.Open(MySql, TabelleDb, ADODB.CursorTypeEnum.adOpenDynamic, ADODB.LockTypeEnum.adLockOptimistic)
        If Not RsReg.EOF Then
            AcquistiVendite = MoveFromDbWC(RsReg, "TipoIVA")
        End If
        RsReg.Close()

        MeseDa = DD_MeseU.SelectedValue
        MeseA = DD_MeseA.SelectedValue

        If RB_Trimestrale.Checked = True Then
            MeseArrivo = MeseA + 3
            MeseEffettivo = MeseA + 2
        Else
            MeseArrivo = MeseA + 1
            MeseEffettivo = MeseA
        End If

        Inserito = False

        WrRs.Open("RegistroIVA", StampeDb, ADODB.CursorTypeEnum.adOpenDynamic, ADODB.LockTypeEnum.adLockOptimistic)

        Do While MeseA < MeseArrivo

            Condizione = " And Year(DataRegistrazione) = " & Txt_AnnoA.Text & " And Month(DataRegistrazione) = " & MeseA

            Protocollo = 0
            If Chk_ContabilitaSemplificata.Checked = True Then

                MySql = "SELECT * FROM MovimentiContabiliTesta " & _
                        " INNER JOIN MovimentiContabiliRiga " & _
                        " ON MovimentiContabiliTesta.NumeroRegistrazione = MovimentiContabiliRiga.Numero " & _
                        " WHERE  " & _
                        " RegistroIVA = " & Registro & _
                        " AND AnnoProtocollo = " & Txt_Anno.Text & Condizione & _
                        " ORDER BY NumeroProtocollo, MovimentiContabiliRiga.Numero, DataRegistrazione"
            Else
                MySql = "SELECT * FROM MovimentiContabiliTesta " & _
                        " INNER JOIN MovimentiContabiliRiga " & _
                        " ON MovimentiContabiliTesta.NumeroRegistrazione = MovimentiContabiliRiga.Numero " & _
                        " WHERE Tipo = 'IV' " & _
                        " AND RegistroIVA = " & Registro & _
                        " AND AnnoProtocollo = " & Txt_Anno.Text & Condizione & _
                        " ORDER BY NumeroProtocollo, MovimentiContabiliRiga.Numero, DataRegistrazione"
            End If

            MyRs.Open(MySql, GeneraleDb, ADODB.CursorTypeEnum.adOpenDynamic, ADODB.LockTypeEnum.adLockOptimistic)
            If Not MyRs.EOF Then
                Protocollo = MoveFromDbWC(MyRs, "NumeroProtocollo")
            End If
            Do While Not MyRs.EOF
                Entra = False
                If Chk_ContabilitaSemplificata.Checked = True Then
                    If MoveFromDbWC(MyRs, "TIPO") = "IV" Then
                        Entra = True
                    Else
                        numero = 0
                        RsDecodifica.Open("Select count(*) from MovimentiContabiliRiga Where Numero = " & MoveFromDb(MyRs.Fields("numero")) & " And TIPO = 'IV'", GeneraleDb, ADODB.CursorTypeEnum.adOpenDynamic, ADODB.LockTypeEnum.adLockOptimistic)
                        If Not RsDecodifica.EOF Then
                            numero = MoveFromDb(RsDecodifica.Fields(0))
                        End If
                        RsDecodifica.Close()
                        If numero = 0 Then
                            Dim XCamPC As New Cls_Pianodeiconti

                            XCamPC.Mastro = MoveFromDbWC(MyRs, "MASTRO")
                            XCamPC.Conto = MoveFromDbWC(MyRs, "CONTO")
                            XCamPC.Sottoconto = MoveFromDbWC(MyRs, "SOTTOCONTO")
                            XCamPC.Decodfica(Session("DC_TABELLE"))


                            If XCamPC.Tipo = "C" Or XCamPC.Tipo = "R" Then
                                Entra = True
                            End If
                        End If
                    End If
                Else
                    Entra = True
                End If

                If Entra Then


                    If Protocollo + 1 < MoveFromDbWC(MyRs, "NumeroProtocollo") Then
                        Session("Errori") = Session("Errori") & "Attenzione protocollo " & Protocollo + 1 & " non presente "
                    End If


                    Protocollo = MoveFromDbWC(MyRs, "NumeroProtocollo")



                    Importo = MoveFromDbWC(MyRs, "Importo")
                    Imponibile = MoveFromDbWC(MyRs, "Imponibile")
                    Dim DecoCaus As New Cls_CausaleContabile

                    DecoCaus.Leggi(Session("DC_TABELLE"), MoveFromDbWC(MyRs, "CausaleContabile"))

                    If DecoCaus.TipoDocumento = "FA" Or DecoCaus.TipoDocumento = "RE" Then
                        If AcquistiVendite = "A" Then
                            If MoveFromDbWC(MyRs, "DareAvere") = "A" And MoveFromDbWC(MyRs, "Segno") = "+" Then
                                Importo = Importo * -1
                                Imponibile = Imponibile * -1
                            End If
                        Else
                            If MoveFromDbWC(MyRs, "DareAvere") = "D" And MoveFromDbWC(MyRs, "Segno") = "+" Then
                                Importo = Importo * -1
                                Imponibile = Imponibile * -1
                            End If
                        End If
                    End If

                    If DecoCaus.TipoDocumento = "NC" Then
                        If AcquistiVendite = "A" Then
                            '          Importo = Importo * -1
                            '          Imponibile = Imponibile * -1
                            If MoveFromDbWC(MyRs, "DareAvere") = "A" And MoveFromDbWC(MyRs, "Segno") = "+" Then
                                Importo = Importo * -1
                                Imponibile = Imponibile * -1
                            End If
                        Else
                            If MoveFromDbWC(MyRs, "DareAvere") = "D" And MoveFromDbWC(MyRs, "Segno") = "+" Then
                                Importo = Importo * -1
                                Imponibile = Imponibile * -1
                            End If
                        End If
                    End If
                    If DecoCaus.TipoDocumento = "CR" Then
                        If AcquistiVendite = "A" Then
                            If MoveFromDbWC(MyRs, "DareAvere") = "A" And MoveFromDbWC(MyRs, "Segno") = "+" Then
                                Importo = Importo * -1
                                Imponibile = Imponibile * -1
                            End If
                        Else
                            If MoveFromDbWC(MyRs, "DareAvere") = "D" And MoveFromDbWC(MyRs, "Segno") = "+" Then
                                Importo = Importo * -1
                                Imponibile = Imponibile * -1
                            End If
                        End If

                    End If


                    Inserito = True
                    WrRs.AddNew()
                    MoveToDb(WrRs.Fields("DESCRIZIONEREGISTRO"), DD_RegistroIVA.SelectedItem.Text)
                    MoveToDb(WrRs.Fields("Mese"), MeseA)
                    MoveToDb(WrRs.Fields("NumeroProtocollo"), MoveFromDbWC(MyRs, "NumeroProtocollo"))

                    Dim XCl As New Cls_RegistroIVA
                    XCl.Leggi(Session("DC_TABELLE"), Registro)

                    MoveToDb(WrRs.Fields("NumeroRegistro"), Registro)

                    MoveToDb(WrRs.Fields("IdentificativoRegistro"), XCl.IndicatoreRegistro)
                    MoveToDb(WrRs.Fields("Bis"), XCl.IndicatoreRegistro & " " & MoveFromDbWC(MyRs, "Bis"))
                    MoveToDb(WrRs.Fields("DataRegistrazione"), MoveFromDbWC(MyRs, "DataRegistrazione"))
                    MoveToDb(WrRs.Fields("MastroPartita"), MoveFromDbWC(MyRs, "MastroPartita"))
                    MoveToDb(WrRs.Fields("ContoPartita"), MoveFromDbWC(MyRs, "ContoPartita"))
                    MoveToDb(WrRs.Fields("SottocontoPartita"), MoveFromDbWC(MyRs, "SottocontoPartita"))
                    Contr = "Mastro = " & MoveFromDbWC(MyRs, "MastroPartita") & " AND Conto = " & MoveFromDbWC(MyRs, "ContoPartita") & " AND Sottoconto = " & MoveFromDbWC(MyRs, "SottocontoPartita")
                    MySql = "SELECT * from PianoConti where " & Contr
                    RsDecodifica.Open(MySql, GeneraleDb, ADODB.CursorTypeEnum.adOpenDynamic, ADODB.LockTypeEnum.adLockOptimistic)
                    If Not RsDecodifica.EOF Then
                        MoveToDb(WrRs.Fields("DescrizionePartita"), MoveFromDbWC(RsDecodifica, "Descrizione"))


                    End If
                    RsDecodifica.Close()

                    If MoveFromDbWC(MyRs, "SottocontoContropartita") <> 0 Then
                        MoveToDb(WrRs.Fields("MastroContropartita"), MoveFromDbWC(MyRs, "MastroContropartita"))
                        MoveToDb(WrRs.Fields("ContoContropartita"), MoveFromDbWC(MyRs, "ContoContropartita"))
                        MoveToDb(WrRs.Fields("SottocontoContropartita"), MoveFromDbWC(MyRs, "SottocontoContropartita"))
                        Contr = "Mastro = " & MoveFromDbWC(MyRs, "MastroContropartita") & " AND Conto = " & MoveFromDbWC(MyRs, "ContoContropartita") & " AND Sottoconto = " & MoveFromDbWC(MyRs, "SottocontoContropartita")
                        MySql = "SELECT * from PianoConti where " & Contr
                        RsDecodifica.Open(MySql, GeneraleDb, ADODB.CursorTypeEnum.adOpenDynamic, ADODB.LockTypeEnum.adLockOptimistic)
                        If Not RsDecodifica.EOF Then
                            MoveToDb(WrRs.Fields("DescrizioneContropartita"), MoveFromDbWC(RsDecodifica, "Descrizione"))
                            If Chk_AggiungiPIVACF.Checked = True Then
                                If MoveFromDbWC(RsDecodifica, "tipoanagrafica") = "O" Or MoveFromDbWC(RsDecodifica, "tipoanagrafica") = "P" Or MoveFromDbWC(RsDecodifica, "tipoanagrafica") = "R" Or MoveFromDbWC(RsDecodifica, "tipoanagrafica") = "C" Or MoveFromDbWC(RsDecodifica, "tipoanagrafica") = "D" Then
                                    If MoveFromDbWC(RsDecodifica, "tipoanagrafica") = "O" Then
                                        Dim M As New ClsOspite

                                        M.CodiceOspite = MoveFromDbWC(MyRs, "MastroContropartita") / 100
                                        M.Leggi(Session("DC_OSPITE"), M.CodiceOspite)
                                        MoveToDb(WrRs.Fields("CFPIVA"), M.CODICEFISCALE)
                                    End If
                                    If MoveFromDbWC(RsDecodifica, "tipoanagrafica") = "P" Then
                                        Dim M As New Cls_Parenti

                                        M.CodiceOspite = Int(MoveFromDbWC(MyRs, "MastroContropartita") / 100)

                                        M.CodiceParente = MoveFromDbWC(MyRs, "MastroContropartita") - (Int(MoveFromDbWC(MyRs, "MastroContropartita") / 100) * 100)

                                        M.Leggi(Session("DC_OSPITE"), M.CodiceOspite, M.CodiceParente)
                                        MoveToDb(WrRs.Fields("CFPIVA"), M.CODICEFISCALE)
                                    End If
                                    If MoveFromDbWC(RsDecodifica, "tipoanagrafica") = "R" Or MoveFromDbWC(RsDecodifica, "tipoanagrafica") = "C" Or MoveFromDbWC(RsDecodifica, "tipoanagrafica") = "D" Then
                                        Dim M As New Cls_ClienteFornitore
                                        M.MastroCliente = MoveFromDbWC(MyRs, "MastroContropartita")
                                        M.ContoCliente = MoveFromDbWC(MyRs, "ContoContropartita")
                                        M.SottoContoCliente = MoveFromDbWC(MyRs, "SottoContoContropartita")
                                        M.CODICEDEBITORECREDITORE = 0
                                        M.Leggi(Session("DC_OSPITE"))
                                        If M.CODICEDEBITORECREDITORE = 0 Then
                                            M.MastroCliente = 0
                                            M.ContoCliente = 0
                                            M.SottoContoCliente = 0
                                            M.MastroFornitore = MoveFromDbWC(MyRs, "MastroContropartita")
                                            M.ContoFornitore = MoveFromDbWC(MyRs, "ContoContropartita")
                                            M.SottoContoFornitore = MoveFromDbWC(MyRs, "SottoContoContropartita")
                                            M.CODICEDEBITORECREDITORE = 0
                                            M.Leggi(Session("DC_OSPITE"))
                                        End If
                                        If M.CodiceFiscale <> "" Then
                                            MoveToDb(WrRs.Fields("CFPIVA"), M.CodiceFiscale)
                                        End If
                                        If Val(M.PARTITAIVA) > 0 Then
                                            MoveToDb(WrRs.Fields("CFPIVA"), Format(Val(M.PARTITAIVA), "00000000000"))
                                        End If
                                    End If
                                End If
                            End If
                        End If
                        RsDecodifica.Close()
                    Else
                        RigaDaCausaleRs.Open("Select * From MovimentiContabiliRiga Where Numero = " & Val(MoveFromDbWC(MyRs, "Numero")) & " And RigaDaCausale = 1", GeneraleDb, ADODB.CursorTypeEnum.adOpenDynamic, ADODB.LockTypeEnum.adLockOptimistic)
                        If Not RigaDaCausaleRs.EOF Then
                            MoveToDb(WrRs.Fields("MastroContropartita"), MoveFromDbWC(RigaDaCausaleRs, "MastroPartita"))
                            MoveToDb(WrRs.Fields("ContoContropartita"), MoveFromDbWC(RigaDaCausaleRs, "ContoPartita"))
                            MoveToDb(WrRs.Fields("SottocontoContropartita"), MoveFromDbWC(RigaDaCausaleRs, "SottocontoPartita"))
                            Contr = "Mastro = " & MoveFromDbWC(RigaDaCausaleRs, "MastroPartita") & " AND Conto = " & MoveFromDbWC(RigaDaCausaleRs, "ContoPartita") & " AND Sottoconto = " & MoveFromDbWC(RigaDaCausaleRs, "SottocontoPartita")
                            MySql = "SELECT * from PianoConti where " & Contr
                            RsDecodifica.Open(MySql, GeneraleDb, ADODB.CursorTypeEnum.adOpenDynamic, ADODB.LockTypeEnum.adLockOptimistic)
                            If Not RsDecodifica.EOF Then
                                MoveToDb(WrRs.Fields("DescrizioneContropartita"), MoveFromDbWC(RsDecodifica, "Descrizione"))

                                If Chk_AggiungiPIVACF.Checked = True Then
                                    If MoveFromDbWC(RsDecodifica, "tipoanagrafica") = "O" Or MoveFromDbWC(RsDecodifica, "tipoanagrafica") = "P" Or MoveFromDbWC(RsDecodifica, "tipoanagrafica") = "R" Or MoveFromDbWC(RsDecodifica, "tipoanagrafica") = "C" Or MoveFromDbWC(RsDecodifica, "tipoanagrafica") = "D" Then
                                        If MoveFromDbWC(RsDecodifica, "tipoanagrafica") = "O" Then
                                            Dim M As New ClsOspite

                                            M.CodiceOspite = MoveFromDbWC(RigaDaCausaleRs, "SottocontoPartita") / 100
                                            M.Leggi(Session("DC_OSPITE"), M.CodiceOspite)
                                            MoveToDb(WrRs.Fields("CFPIVA"), M.CODICEFISCALE)
                                        End If
                                        If MoveFromDbWC(RsDecodifica, "tipoanagrafica") = "P" Then
                                            Dim M As New Cls_Parenti

                                            M.CodiceOspite = Int(MoveFromDbWC(RigaDaCausaleRs, "SottocontoPartita") / 100)

                                            M.CodiceParente = MoveFromDbWC(RigaDaCausaleRs, "SottocontoPartita") - (Int(MoveFromDbWC(RigaDaCausaleRs, "SottocontoPartita") / 100) * 100)

                                            M.Leggi(Session("DC_OSPITE"), M.CodiceOspite, M.CodiceParente)
                                            MoveToDb(WrRs.Fields("CFPIVA"), M.CODICEFISCALE)
                                        End If
                                        If MoveFromDbWC(RsDecodifica, "tipoanagrafica") = "R" Or MoveFromDbWC(RsDecodifica, "tipoanagrafica") = "C" Or MoveFromDbWC(RsDecodifica, "tipoanagrafica") = "D" Then
                                            Dim M As New Cls_ClienteFornitore
                                            M.MastroCliente = MoveFromDbWC(RigaDaCausaleRs, "MastroPartita")
                                            M.ContoCliente = MoveFromDbWC(RigaDaCausaleRs, "ContoPartita")
                                            M.SottoContoCliente = MoveFromDbWC(RigaDaCausaleRs, "SottocontoPartita")
                                            M.CODICEDEBITORECREDITORE = 0
                                            M.Leggi(Session("DC_OSPITE"))
                                            If M.CODICEDEBITORECREDITORE = 0 Then
                                                M.MastroCliente = 0
                                                M.ContoCliente = 0
                                                M.SottoContoCliente = 0
                                                M.MastroFornitore = MoveFromDbWC(RigaDaCausaleRs, "MastroPartita")
                                                M.ContoFornitore = MoveFromDbWC(RigaDaCausaleRs, "ContoPartita")
                                                M.SottoContoFornitore = MoveFromDbWC(RigaDaCausaleRs, "SottocontoPartita")
                                                M.CODICEDEBITORECREDITORE = 0
                                                M.Leggi(Session("DC_OSPITE"))
                                            End If
                                            If M.CodiceFiscale <> "" Then
                                                MoveToDb(WrRs.Fields("CFPIVA"), M.CodiceFiscale)
                                            End If
                                            If M.PARTITAIVA > 0 Then
                                                MoveToDb(WrRs.Fields("CFPIVA"), Format(Val(M.PARTITAIVA), "00000000000"))
                                            End If
                                        End If
                                    End If
                                End If
                            End If
                            RsDecodifica.Close()
                        End If
                        RigaDaCausaleRs.Close()
                    End If
                    MoveToDb(WrRs.Fields("NumeroDocumento"), MoveFromDbWC(MyRs, "NumeroDocumento"))
                    MoveToDb(WrRs.Fields("DataDocumento"), MoveFromDbWC(MyRs, "DataDocumento"))
                    MoveToDb(WrRs.Fields("Imponibile"), Imponibile)
                    MoveToDb(WrRs.Fields("CodiceIva"), MoveFromDbWC(MyRs, "CodiceIva"))
                    Dim XDIva As New Cls_IVA

                    XDIva.Leggi(Session("DC_TABELLE"), MoveFromDbWC(MyRs, "CodiceIva"))

                    MoveToDb(WrRs.Fields("DescrizioneIVA"), XDIva.Descrizione)



                    tipo = XDIva.Tipo
                    MoveToDb(WrRs.Fields("CodiceIvaDetraibile"), MoveFromDbWC(MyRs, "Detraibile") & MoveFromDbWC(MyRs, "CodiceIva"))


                    REM MoveToDb(WrRs.Fields("Imponibile"), 0)
                    MoveToDb(WrRs.Fields("ImportoEsente"), 0)
                    MoveToDb(WrRs.Fields("ImportoNonSoggetto"), 0)
                    MoveToDb(WrRs.Fields("ImportoNonImponibile"), 0)
                    If tipo = "ES" Then
                        MoveToDb(WrRs.Fields("Imponibile"), 0)
                        MoveToDb(WrRs.Fields("ImportoEsente"), Imponibile)
                    End If
                    If tipo = "NS" Then
                        MoveToDb(WrRs.Fields("Imponibile"), 0)
                        MoveToDb(WrRs.Fields("ImportoNonSoggetto"), Imponibile)
                    End If
                    If tipo = "NI" Then
                        MoveToDb(WrRs.Fields("Imponibile"), 0)
                        MoveToDb(WrRs.Fields("ImportoNonImponibile"), Imponibile)
                    End If
                    MoveToDb(WrRs.Fields("Aliquota"), XDIva.Aliquota)
                    MoveToDb(WrRs.Fields("Importo"), Importo)
                    MoveToDb(WrRs.Fields("IVASospesa"), MoveFromDbWC(MyRs, "IVASospesa"))
                    If AcquistiVendite = "A" Then
                        If tipo = "" Then
                            MoveToDb(WrRs.Fields("Detraibile"), MoveFromDbWC(MyRs, "Detraibile"))

                            Dim AliDetr As New ClsDetraibilita

                            AliDetr.Codice = MoveFromDbWC(MyRs, "Detraibile")
                            AliDetr.Leggi(Session("DC_TABELLE"))

                            Detraibile = AliDetr.Aliquota

                            AppoggioProrata = 0
                            If Val(AliDetr.Prorata) = 1 Then
                                Dim RS_Prorata As New ADODB.Recordset

                                MySql = "SELECT * FROM Prorata Where Anno = " & Val(Txt_Anno.Text) - 1 & " And RegistroIVA = " & Registro
                                RS_Prorata.Open(MySql, TabelleDb, ADODB.CursorTypeEnum.adOpenDynamic, ADODB.LockTypeEnum.adLockOptimistic)
                                If Not RS_Prorata.EOF Then
                                    AppoggioProrata = Modulo.MathRound(Importo * MoveFromDb(RS_Prorata.Fields("DefinitivoPercentuale")), 2)
                                End If
                                RS_Prorata.Close()
                            End If
                            MoveToDb(WrRs.Fields("CodiceIvaDetraibile"), MoveFromDbWC(MyRs, "CodiceIva") & MoveFromDbWC(MyRs, "Detraibile"))
                            MoveToDb(WrRs.Fields("DescrizioneDetraibile"), AliDetr.Descrizione)
                            MoveToDb(WrRs.Fields("ImportoIndeducibile"), Importo - (Importo * Detraibile) - AppoggioProrata)
                        End If
                    Else
                        MoveToDb(WrRs.Fields("ImportoIndeducibile"), 0)
                    End If
                    '      If CampoCausaleContabile(MoveFromDbWC(MyRs,"CausaleContabile"), "TipoDocumento") = "NC" Then
                    '        MoveToDb WrRs.Fields("ImportoDocumento, ImportoDocumento(MoveFromDbWC(MyRs,"NumeroRegistrazione")) * -1
                    '        Else

                    Dim Xs As New Cls_MovimentoContabile
                    Xs.Leggi(Session("DC_GENERALE"), MoveFromDbWC(MyRs, "NumeroRegistrazione"))

                    MoveToDb(WrRs.Fields("ImportoDocumento"), Xs.ImportoDocumento(Session("DC_TABELLE")))
                    '      End If
                    MoveToDb(WrRs.Fields("NomeRegistro"), DD_RegistroIVA.SelectedItem.Text)
                    MoveToDb(WrRs.Fields("IntestaSocieta"), WSocieta)
                    MoveToDb(WrRs.Fields("UltimaPagina"), Txt_UltPaginaStampata.Text - 1)
                    MoveToDb(WrRs.Fields("Esercizio"), Txt_Anno.Text)

                    MoveToDb(WrRs.Fields("PRINTERKEY"), ViewState("PRINTERKEY"))

                    WrRs.Update()

                End If
                MyRs.MoveNext()
            Loop
            MyRs.Close()
            MeseA = MeseA + 1
        Loop
        If Inserito = False Then
            WrRs.AddNew()
            MoveToDb(WrRs.Fields("DESCRIZIONEREGISTRO"), DD_RegistroIVA.SelectedItem.Text)
            MoveToDb(WrRs.Fields("Mese"), MeseA - 1)
            MoveToDb(WrRs.Fields("IntestaSocieta"), WSocieta)
            MoveToDb(WrRs.Fields("UltimaPagina"), Txt_UltPaginaStampata.Text - 1)
            MoveToDb(WrRs.Fields("Esercizio"), Txt_Anno.Text)
            MoveToDb(WrRs.Fields("PRINTERKEY"), ViewState("PRINTERKEY"))
            WrRs.Update()
        End If
        StampeDb.Close()

        Lbl_Errori.Visible = False
        If Session("Errori") <> "" Then
            Lbl_Errori.Text = Session("Errori")
            Lbl_Errori.Visible = True
            ScriptManager.RegisterStartupScript(Me, Me.GetType(), "visualizzaA", "VisualizzaErrore('" & Lbl_Errori.Text & "');", True)
        End If
    End Sub
    Public Function MoveFromDbWC(ByVal MyRs As ADODB.Recordset, ByVal Campo As String) As Object
        Dim MyField As ADODB.Field

#If FLAG_DEBUG = False Then
        On Error GoTo ErroreCampoInesistente
#End If
        MyField = MyRs.Fields(Campo)

        Select Case MyField.Type
            Case ADODB.DataTypeEnum.adDBDate
                If IsDBNull(MyField.Value) Then
                    MoveFromDbWC = "__/__/____"
                Else
                    If Not IsDate(MyField.Value) Or MyField.Value = "0.00.00" Then
                        MoveFromDbWC = "__/__/____"
                    Else
                        MoveFromDbWC = MyField.Value
                    End If
                End If
            Case ADODB.DataTypeEnum.adDBTimeStamp
                If IsDBNull(MyField.Value) Then
                    MoveFromDbWC = "__/__/____"
                Else
                    If Not IsDate(MyField.Value) Then
                        MoveFromDbWC = "__/__/____"
                    Else
                        MoveFromDbWC = MyField.Value
                    End If
                End If
            Case ADODB.DataTypeEnum.adSmallInt
                If IsDBNull(MyField.Value) Then
                    MoveFromDbWC = 0
                Else
                    MoveFromDbWC = MyField.Value
                End If
            Case ADODB.DataTypeEnum.adInteger
                If IsDBNull(MyField.Value) Then
                    MoveFromDbWC = 0
                Else
                    MoveFromDbWC = MyField.Value
                End If
            Case ADODB.DataTypeEnum.adNumeric
                If IsDBNull(MyField.Value) Then
                    MoveFromDbWC = 0
                Else
                    MoveFromDbWC = MyField.Value
                End If
            Case ADODB.DataTypeEnum.adTinyInt
                If IsDBNull(MyField.Value) Then
                    MoveFromDbWC = 0
                Else
                    MoveFromDbWC = MyField.Value
                End If
            Case ADODB.DataTypeEnum.adSingle
                If IsDBNull(MyField.Value) Then
                    MoveFromDbWC = 0
                Else
                    MoveFromDbWC = MyField.Value
                End If
            Case ADODB.DataTypeEnum.adLongVarBinary
                If IsDBNull(MyField.Value) Then
                    MoveFromDbWC = 0
                Else
                    MoveFromDbWC = MyField.Value
                End If
            Case ADODB.DataTypeEnum.adDouble
                If IsDBNull(MyField.Value) Then
                    MoveFromDbWC = 0
                Else
                    MoveFromDbWC = MyField.Value
                End If
            Case ADODB.DataTypeEnum.adVarChar
                If IsDBNull(MyField.Value) Then
                    MoveFromDbWC = vbNullString
                Else
                    MoveFromDbWC = CStr(MyField.Value)
                End If
            Case ADODB.DataTypeEnum.adUnsignedTinyInt
                If IsDBNull(MyField.Value) Then
                    MoveFromDbWC = 0
                Else
                    MoveFromDbWC = MyField.Value
                End If
            Case Else
                If IsDBNull(MyField.Value) Then
                    MoveFromDbWC = ""
                End If
                If IsDBNull(MyField.Value) Then
                    MoveFromDbWC = ""
                Else
                    MoveFromDbWC = MyField.Value
                End If
        End Select

        On Error GoTo 0
        Exit Function
ErroreCampoInesistente:

        On Error GoTo 0
    End Function
    Private Function MoveFromDb(ByVal MyField As ADODB.Field, Optional ByVal Tipo As Integer = 0) As Object
        If Tipo = 0 Then
            Select Case MyField.Type
                Case ADODB.DataTypeEnum.adDBDate Or ADODB.DataTypeEnum.adDBTimeStamp
                    If IsDBNull(MyField.Value) Then
                        MoveFromDb = "__/__/____"
                    Else
                        If Not IsDate(MyField.Value) Then
                            MoveFromDb = "__/__/____"
                        Else
                            MoveFromDb = MyField.Value
                        End If
                    End If
                Case ADODB.DataTypeEnum.adSmallInt
                    If IsDBNull(MyField.Value) Then
                        MoveFromDb = 0
                    Else
                        MoveFromDb = MyField.Value
                    End If
                Case 20 ' INTERO PER MYSQL
                    If IsDBNull(MyField.Value) Then
                        MoveFromDb = 0
                    Else
                        MoveFromDb = MyField.Value
                    End If
                Case ADODB.DataTypeEnum.adInteger
                    If IsDBNull(MyField.Value) Then
                        MoveFromDb = 0
                    Else
                        MoveFromDb = MyField.Value
                    End If
                Case ADODB.DataTypeEnum.adNumeric
                    If IsDBNull(MyField.Value) Then
                        MoveFromDb = 0
                    Else
                        MoveFromDb = MyField.Value
                    End If
                Case ADODB.DataTypeEnum.adTinyInt
                    If IsDBNull(MyField.Value) Then
                        MoveFromDb = 0
                    Else
                        MoveFromDb = MyField.Value
                    End If
                Case ADODB.DataTypeEnum.adSingle
                    If IsDBNull(MyField.Value) Then
                        MoveFromDb = 0
                    Else
                        MoveFromDb = MyField.Value
                    End If
                Case ADODB.DataTypeEnum.adLongVarBinary
                    If IsDBNull(MyField.Value) Then
                        MoveFromDb = 0
                    Else
                        MoveFromDb = MyField.Value
                    End If
                Case ADODB.DataTypeEnum.adDouble
                    If IsDBNull(MyField.Value) Then
                        MoveFromDb = 0
                    Else
                        MoveFromDb = MyField.Value
                    End If
                Case 139
                    If IsDBNull(MyField.Value) Then
                        MoveFromDb = 0
                    Else
                        MoveFromDb = MyField.Value
                    End If
                Case ADODB.DataTypeEnum.adVarChar
                    If IsDBNull(MyField.Value) Then
                        MoveFromDb = vbNullString
                    Else
                        MoveFromDb = CStr(MyField.Value)
                    End If
                Case ADODB.DataTypeEnum.adUnsignedTinyInt
                    If IsDBNull(MyField.Value) Then
                        MoveFromDb = 0
                    Else
                        MoveFromDb = MyField.Value
                    End If
                Case Else
                    If IsDBNull(MyField.Value) Then
                        MoveFromDb = ""
                    End If
                    If IsDBNull(MyField.Value) Then
                        MoveFromDb = ""
                    Else
                        'MoveFromDb = StringaSqlS(MyField.Value)
                        MoveFromDb = RTrim(MyField.Value)
                    End If
            End Select
        End If
    End Function
    Public Sub MoveToDb(ByRef MyField As ADODB.Field, ByVal MyVal As Object)
        Select Case Val(MyField.Type)
            Case ADODB.DataTypeEnum.adDBDate Or ADODB.DataTypeEnum.adDBTimeStamp
                If IsDate(MyVal) Then
                    MyField.Value = MyVal
                Else
                    MyField.Value = 0
                End If
            Case ADODB.DataTypeEnum.adDouble
                If IsNumeric(MyVal) Then
                    MyField.Value = MyVal
                Else
                    MyField.Value = 0
                End If
            Case ADODB.DataTypeEnum.adSmallInt
                If IsNumeric(MyVal) Then
                    MyField.Value = MyVal
                Else : MyField.Value = 0
                End If
            Case ADODB.DataTypeEnum.adInteger
                If IsNumeric(MyVal) Then
                    MyField.Value = MyVal
                Else
                    MyField.Value = 0
                End If
            Case ADODB.DataTypeEnum.adNumeric
                If IsNumeric(MyVal) Then
                    MyField.Value = MyVal
                Else
                    MyField.Value = 0
                End If
            Case ADODB.DataTypeEnum.adTinyInt
                If IsNumeric(MyVal) Then
                    MyField.Value = MyVal
                Else
                    MyField.Value = 0
                End If
            Case ADODB.DataTypeEnum.adSingle
                If IsNumeric(MyVal) Then
                    MyField.Value = MyVal
                Else
                    MyField.Value = 0
                End If
            Case ADODB.DataTypeEnum.adLongVarBinary
                If IsNumeric(MyVal) Then
                    MyField.Value = MyVal
                Else
                    MyField.Value = 0
                End If
            Case ADODB.DataTypeEnum.adUnsignedTinyInt
                If IsNumeric(MyVal) Then
                    MyField.Value = MyVal
                Else
                    MyField.Value = 0
                End If
            Case ADODB.DataTypeEnum.adVarChar Or ADODB.DataTypeEnum.adVarWChar Or ADODB.DataTypeEnum.adWChar Or ADODB.DataTypeEnum.adBSTR
                If IsDBNull(MyVal) Then
                    MyField.Value = ""
                Else
                    If Len(MyVal) > MyField.DefinedSize Then
                        MyField.Value = Mid(MyVal, 1, MyField.DefinedSize)
                    Else
                        MyField.Value = MyVal
                    End If
                End If
            Case Else
                If Val(MyField.Type) = ADODB.DataTypeEnum.adVarChar Or Val(MyField.Type) = ADODB.DataTypeEnum.adVarWChar Or _
                   Val(MyField.Type) = ADODB.DataTypeEnum.adWChar Or Val(MyField.Type) = ADODB.DataTypeEnum.adBSTR Then
                    If IsDBNull(MyVal) Then
                        MyField.Value = ""
                    Else
                        If Len(MyVal) > MyField.DefinedSize Then
                            MyField.Value = Mid(MyVal, 1, MyField.DefinedSize)
                        Else
                            MyField.Value = MyVal
                        End If
                    End If
                Else
                    MyField.Value = MyVal
                End If
        End Select

    End Sub


    Private Sub ImpostaRegistri()
        If Val(Txt_Anno.Text) = 0 Then Exit Sub
        If Val(DD_RegistroIVA.SelectedValue) = 0 Then Exit Sub

        Dim k As New Cls_RegistriIVAanno

        k.Leggi(Session("DC_TABELLE"), DD_RegistroIVA.SelectedValue, Val(Txt_Anno.Text))

        If Year(k.DataStampa) = Val(Txt_Anno.Text) Then
            DD_MeseU.SelectedValue = Month(k.DataStampa)
        Else
            DD_MeseU.SelectedValue = 1
        End If
        Txt_AnnoU.Text = k.Anno
        Txt_PaginaU.Text = k.UltimaPaginaStampata + 1
        Txt_UltPaginaStampata.text = Txt_PaginaU.Text

        Txt_AnnoA.Text = k.Anno

        If k.Tipo > 0 Then

            If Year(k.DataStampa) < k.Anno Then
                DD_MeseA.SelectedValue = 1
            Else
                If Month(k.DataStampa) < 12 Then DD_MeseA.SelectedValue = Month(k.DataStampa) + 1
            End If

        End If

    End Sub
    Protected Sub DD_RegistroIVA_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles DD_RegistroIVA.SelectedIndexChanged
        Call ImpostaRegistri()
    End Sub

    Protected Sub Txt_Anno_TextChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles Txt_Anno.TextChanged
        Call ImpostaRegistri()
    End Sub



    Public Function GiorniMese(ByVal Mese As Byte, ByVal Anno As Integer) As Byte
        If Mese = 1 Or Mese = 3 Or Mese = 5 Or Mese = 7 Or Mese = 8 Or _
           Mese = 10 Or Mese = 12 Then
            GiorniMese = 31
        Else
            If Mese <> 2 Then
                GiorniMese = 30
            Else
                If Day(DateSerial(Anno, Mese, 29)) = 29 Then
                    GiorniMese = 29
                Else
                    GiorniMese = 28
                End If
            End If
        End If
    End Function

    Protected Sub ImageButton1_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles ImageButton1.Click
        If Txt_AnnoA.Text = "0" Or Txt_AnnoA.Text = "" Then
            REM Lbl_Errori.Text = "Devi specificare un anno valido in [Anno A]"
            REM ClientScript.RegisterClientScriptBlock(Me.GetType(), "Errore", "VisualizzaErrore('Devi specificare un anno valido in [Anno A]');", True)
            ScriptManager.RegisterStartupScript(Me, Me.GetType(), "visualizzaA", "VisualizzaErrore('Devi specificare un anno valido in [Anno A]');", True)
            Exit Sub
        End If
        If DD_MeseA.SelectedValue = "0" Then
            ScriptManager.RegisterStartupScript(Me, Me.GetType(), "visualizzaA", "VisualizzaErrore('Devi specificare un mese valido in [Mese A]');", True)
            REM ClientScript.RegisterClientScriptBlock(Me.GetType(), "Errore", "VisualizzaErrore('Devi specificare un mese valido in [Mese A]');", True)
            REM Lbl_Errori.Text = "Devi specificare un mese valido in [Mese A]"
            Exit Sub
        End If


        Dim XS As New Cls_Login

        XS.Utente = Session("UTENTE")
        XS.LeggiSP(Application("SENIOR"))


        If XS.ReportPersonalizzato("REGISTROIVA") = "" Then
            ScriptManager.RegisterStartupScript(Me, Me.GetType(), "visualizzaA", "VisualizzaErrore('Devi indicare il report');", True)
            Exit Sub
        End If

        Txt_Anno.Enabled = False
        DD_MeseA.Enabled = False
        DD_RegistroIVA.Enabled = False


        Call CreaRegistroIVA()

        Session("SelectionFormula") = "{RegistroIVA.PRINTERKEY} = " & Chr(34) & ViewState("PRINTERKEY") & Chr(34)
        REM ScriptManager.RegisterStartupScript(Me, Me.GetType(), "Stampa", "openPopUp('StampaReport.aspx?REPORT=REGISTROIVA&PRINTERKEY=ON','Stampe','width=800,height=600');", True)

        ScriptManager.RegisterClientScriptBlock(Me, Me.GetType(), "Stampa12", "openPopUp('StampaReport.aspx?REPORT=REGISTROIVA&PRINTERKEY=ON','Stampe','width=800,height=600');", True)
        'Response.Redirect("StampaReport.aspx?REPORT=REGISTROIVA")


        Dim rpt As New ReportDocument

        rpt.Load(HostingEnvironment.ApplicationPhysicalPath() & "Report\" & XS.ReportPersonalizzato("REGISTROIVA"))

        rpt.SetDataSource(Session("stampa"))
        Dim VetServer As String
        VetServer = Session("STAMPEFINANZIARIA")

        Dim Xl As Integer = VetServer.IndexOf("Data Source=")
        Xl = Xl + Len("Data Source=") + 1

        Dim Xc As Integer = VetServer.IndexOf("Initial Catalog=")
        Xc = Xc + Len("Initial Catalog=") + 1

        Dim ServerIstanza As String = Mid(VetServer, Xl, VetServer.IndexOf(";", Xl) - Xl + 1)
        Dim NomeDb As String = Mid(VetServer, Xc, VetServer.IndexOf(";", Xc) - Xc + 1)

        '
        If Server.MachineName.ToUpper = "ADVSEN002".ToUpper Then
            rpt.DataSourceConnections(0).SetConnection(ServerIstanza, NomeDb, False)
            rpt.DataSourceConnections(0).SetLogon("sa", "Advenias1820!")
        Else
            rpt.DataSourceConnections(0).SetConnection(ServerIstanza, NomeDb, False)
            rpt.DataSourceConnections(0).SetLogon("sa", "MASTER")
        End If


        If rpt.DataSourceConnections.Count > 1 Then
            If Server.MachineName.ToUpper = "ADVSEN002".ToUpper Then
                rpt.DataSourceConnections(1).SetConnection(ServerIstanza, NomeDb, False)
                rpt.DataSourceConnections(1).SetLogon("sa", "Advenias1820!")
            Else
                rpt.DataSourceConnections(1).SetConnection(ServerIstanza, NomeDb, False)
                rpt.DataSourceConnections(1).SetLogon("sa", "MASTER")
            End If

        End If

        rpt.RecordSelectionFormula = Session("SelectionFormula")
        rpt.Refresh()

        ' Try
        rpt.ExportToDisk(ExportFormatType.PortableDocFormat, HostingEnvironment.ApplicationPhysicalPath() & "Public\" & Session("NomeEPersonam") & "_Protocollo.pdf")

        Txt_UltPaginaStampata.Text = Val(Txt_UltPaginaStampata.Text) + rpt.FormatEngine.GetLastPageNumber(New CrystalDecisions.Shared.ReportPageRequestContext())
        'Finally
        '    Kill(HostingEnvironment.ApplicationPhysicalPath() & "Public\" & Session("NomeEPersonam") & "_Protocollo.pdf")
        'End Try

        ImageButton3.Visible = True
    End Sub

    Protected Sub ImageButton2_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles ImageButton2.Click
        Lbl_Errori.Text = ""
        If Txt_AnnoA.Text = "0" Or Txt_AnnoA.Text = "" Then
            ScriptManager.RegisterStartupScript(Me, Me.GetType(), "visualizzaA", "VisualizzaErrore('Devi specificare un anno valido in [Anno A]');", True)
            REM ClientScript.RegisterClientScriptBlock(Me.GetType(), "Errore", "VisualizzaErrore('Devi specificare un anno valido in [Anno A]');", True)
            REM Lbl_errori.Text = "Devi specificare un anno valido in [Anno A]"
            Exit Sub
        End If
        If DD_MeseA.SelectedValue = "0" Then
            ScriptManager.RegisterStartupScript(Me, Me.GetType(), "visualizzaA", "VisualizzaErrore('Devi specificare un anno valido in [Mese A]');", True)
            REM ClientScript.RegisterClientScriptBlock(Me.GetType(), "Errore", "VisualizzaErrore('Devi specificare un mese valido in [Mese A]');", True)
            REM Lbl_Errori.Text = "Devi specificare un mese valido in [Mese A]"
            Exit Sub
        End If

        If Val(Txt_Anno.Text) = 0 Then
            ScriptManager.RegisterStartupScript(Me, Me.GetType(), "visualizzaA", "VisualizzaErrore('Devi specificare un anno');", True)
            REM ClientScript.RegisterClientScriptBlock(Me.GetType(), "Errore", "VisualizzaErrore('Devi specificare un anno');", True)
            REM Lbl_Errori.Text = "Devi specificare un anno"
            Exit Sub
        End If

        If Val(Txt_AnnoA.Text) = 0 Then
            ScriptManager.RegisterStartupScript(Me, Me.GetType(), "visualizzaA", "VisualizzaErrore('Devi specificare [Anno A]');", True)
            REM ClientScript.RegisterClientScriptBlock(Me.GetType(), "Errore", "VisualizzaErrore('Devi specificare [Anno A]');", True)
            REM Lbl_Errori.Text = "Devi specificare [Anno A]"
            Exit Sub
        End If


        Dim XS As New Cls_Login

        XS.Utente = Session("UTENTE")
        XS.LeggiSP(Application("SENIOR"))



        If XS.ReportPersonalizzato("REGISTROIVA") = "" Then
            ScriptManager.RegisterStartupScript(Me, Me.GetType(), "visualizzaA", "VisualizzaErrore('Devi indicare il report');", True)
            Exit Sub
        End If



        Txt_Anno.Enabled = False
        DD_MeseA.Enabled = False
        DD_RegistroIVA.Enabled = False


        Call CreaRegistroIVA()




        Session("NumeroPagineStampate") = 0


        Session("SelectionFormula") = "{RegistroIVA.PRINTERKEY} = " & Chr(34) & ViewState("PRINTERKEY") & Chr(34)        
        ScriptManager.RegisterClientScriptBlock(Me, Me.GetType(), "Stampa", "openPopUp('StampaReport.aspx?REPORT=REGISTROIVATOT&PRINTERKEY=ON','Stampe','width=800,height=600');", True)

        Dim rpt As New ReportDocument

        rpt.Load(HostingEnvironment.ApplicationPhysicalPath() & "Report\" & XS.ReportPersonalizzato("REGISTROIVATOT"))

        rpt.SetDataSource(Session("stampa"))
        Dim VetServer As String
        VetServer = Session("STAMPEFINANZIARIA")

        Dim Xl As Integer = VetServer.IndexOf("Data Source=")
        Xl = Xl + Len("Data Source=") + 1

        Dim Xc As Integer = VetServer.IndexOf("Initial Catalog=")
        Xc = Xc + Len("Initial Catalog=") + 1

        Dim ServerIstanza As String = Mid(VetServer, Xl, VetServer.IndexOf(";", Xl) - Xl + 1)
        Dim NomeDb As String = Mid(VetServer, Xc, VetServer.IndexOf(";", Xc) - Xc + 1)

        '
        If Server.MachineName.ToUpper = "ADVSEN002".ToUpper Then
            rpt.DataSourceConnections(0).SetConnection(ServerIstanza, NomeDb, False)
            rpt.DataSourceConnections(0).SetLogon("sa", "Advenias1820!")
        Else
            rpt.DataSourceConnections(0).SetConnection(ServerIstanza, NomeDb, False)
            rpt.DataSourceConnections(0).SetLogon("sa", "MASTER")
        End If


        If rpt.DataSourceConnections.Count > 1 Then
            If Server.MachineName.ToUpper = "ADVSEN002".ToUpper Then
                rpt.DataSourceConnections(1).SetConnection(ServerIstanza, NomeDb, False)
                rpt.DataSourceConnections(1).SetLogon("sa", "Advenias1820!")
            Else
                rpt.DataSourceConnections(1).SetConnection(ServerIstanza, NomeDb, False)
                rpt.DataSourceConnections(1).SetLogon("sa", "MASTER")
            End If

        End If

        rpt.RecordSelectionFormula = Session("SelectionFormula")
        rpt.Refresh()


        Try
            rpt.ExportToDisk(ExportFormatType.PortableDocFormat, HostingEnvironment.ApplicationPhysicalPath() & "Public\" & Session("NomeEPersonam") & "_Protocollo.pdf")

            Txt_UltPaginaStampata.Text = Val(Txt_UltPaginaStampata.Text) + rpt.FormatEngine.GetLastPageNumber(New CrystalDecisions.Shared.ReportPageRequestContext())

        Finally
            Kill(HostingEnvironment.ApplicationPhysicalPath() & "Public\" & Session("NomeEPersonam") & "_Protocollo.pdf")
        End Try
        
        ImageButton3.Visible = True
    End Sub

    Protected Sub ImageButton3_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles ImageButton3.Click
        Dim MyReg As New Cls_RegistriIVAanno
        MyReg.Leggi(Session("DC_TABELLE"), DD_RegistroIVA.SelectedValue, Txt_Anno.Text)
        MyReg.UltimaPaginaStampata = Val(Txt_UltPaginaStampata.Text) - 1

        If RB_Trimestrale.Checked = True Then
            MyReg.DataStampa = DateSerial(Txt_Anno.Text, DD_MeseA.SelectedValue + 2, GiorniMese(DD_MeseA.SelectedValue + 2, Txt_Anno.Text))
        Else
            MyReg.DataStampa = DateSerial(Txt_Anno.Text, DD_MeseA.SelectedValue, GiorniMese(DD_MeseA.SelectedValue, Txt_Anno.Text))
        End If
        MyReg.Scrivi(Session("DC_TABELLE"), DD_RegistroIVA.SelectedValue, Txt_Anno.Text)

        ScriptManager.RegisterClientScriptBlock(Me, Me.GetType(), "Stampa", "alert('Registro Modificato');", True)
        Call ImpostaRegistri()

        ImageButton3.Visible = False
    End Sub

    Protected Sub Btn_Esci_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles Btn_Esci.Click
        Response.Redirect("Menu_Stampe.aspx")
    End Sub
End Class
