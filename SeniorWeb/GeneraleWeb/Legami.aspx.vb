﻿Imports System.Web.Hosting
Imports System.Data.OleDb
Partial Class GeneraleWeb_Legami
    Inherits System.Web.UI.Page
    Dim Tabella As New System.Data.DataTable("tabella")
    Dim TabellaRisultati As New System.Data.DataTable("tabella")

    Private Sub aggiornalegami()
        Dim Registrazione As New Cls_MovimentoContabile
        Dim Legami As New Cls_Legami

        Registrazione.Leggi(Session("DC_GENERALE"), Request.Item("NUMERO"))



        Registrazione.Leggi(Session("DC_GENERALE"), Request.Item("NUMERO"))
        Dim Causale As New Cls_CausaleContabile

        Causale.Leggi(Session("DC_TABELLE"), Registrazione.CausaleContabile)

        If Causale.TipoDocumento = "RE" Or Causale.TipoDocumento = "FA" Or Causale.TipoDocumento = "NC" Then
            Legami.Leggi(Session("DC_GENERALE"), Val(Request.Item("NUMERO")), 0)
        Else
            Legami.Leggi(Session("DC_GENERALE"), 0, Val(Request.Item("NUMERO")))
        End If

        Lbl_DatiRegistrazione.Text = "<div  id=""Registrazione"" class=""Registrazione"">Numero Registrazione : " & Val(Request.Item("NUMERO")) & " Importo da legare : " & Format(Math.Round(Registrazione.ImportoRegistrazioneDocumento(Session("DC_TABELLE")) - Legami.TotaleLegame(Session("DC_GENERALE"), Request.Item("NUMERO")), 2), "#,##0.00") & " Tipo " & Causale.TipoDocumento & "-" & Legami.NumeroDocumento(1) & "</div>"

        Tabella.Clear()
        Tabella.Columns.Clear()
        Tabella.Columns.Add("NumeroDocumento", GetType(Long))
        Tabella.Columns.Add("NumIncPag", GetType(Long))
        Tabella.Columns.Add("Importo", GetType(String))

        Dim I As Integer
        Dim ENTRATO As Boolean = False
        For I = 0 To 300
            If Legami.Importo(I) <> 0 Then
                Dim myriga As System.Data.DataRow = Tabella.NewRow()
                myriga(0) = Legami.NumeroDocumento(I)
                myriga(1) = Legami.NumeroPagamento(I)
                myriga(2) = Format(Legami.Importo(I), "#,##0.00")
                Tabella.Rows.Add(myriga)
                ENTRATO = True
            End If
        Next
        If ENTRATO = False Then
            Dim myriga As System.Data.DataRow = Tabella.NewRow()            
            Tabella.Rows.Add(myriga)
        End If

        Session("GestioneLegami") = Tabella

        Grid.AutoGenerateColumns = False
        Grid.DataSource = Tabella
        Grid.DataBind()
    End Sub

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Trim(Session("UTENTE")) = "" Then
            Response.Redirect("../Login_generale.aspx")
            Exit Sub
        End If

        If Page.IsPostBack = True Then
            Exit Sub
        End If

        Call aggiornalegami()
        TabellaRisultati.Clear()
        TabellaRisultati.Columns.Clear()
        TabellaRisultati.Columns.Add("NumeroRegistrazione", GetType(Long))
        TabellaRisultati.Columns.Add("DataRegistrazione", GetType(String))
        TabellaRisultati.Columns.Add("ImportoDocumento", GetType(Double))
        TabellaRisultati.Columns.Add("ImportoLegare", GetType(Double))
        TabellaRisultati.Columns.Add("ImportoLegame", GetType(String))
        TabellaRisultati.Columns.Add("CausaleContabile", GetType(String))
        Dim myriga As System.Data.DataRow = TabellaRisultati.NewRow()
        TabellaRisultati.Rows.Add(myriga)
        GridEstrazione.AutoGenerateColumns = False
        GridEstrazione.DataSource = TabellaRisultati
        GridEstrazione.DataBind()

        Call EseguiJS()
    End Sub

    Function campodb(ByVal oggetto As Object) As String


        If IsDBNull(oggetto) Then
            Return ""
        Else
            Return oggetto
        End If
    End Function


    Private Sub CaricaPagina()
        TabellaRisultati.Clear()
        TabellaRisultati.Columns.Clear()
        TabellaRisultati.Columns.Add("NumeroRegistrazione", GetType(Long))
        TabellaRisultati.Columns.Add("DataRegistrazione", GetType(String))
        TabellaRisultati.Columns.Add("ImportoDocumento", GetType(String))
        TabellaRisultati.Columns.Add("ImportoLegare", GetType(String))
        TabellaRisultati.Columns.Add("ImportoLegame", GetType(String))
        TabellaRisultati.Columns.Add("CausaleContabile", GetType(String))

        Dim Condizione As String
        Dim cn As OleDbConnection


        cn = New Data.OleDb.OleDbConnection(Session("DC_GENERALE"))

        cn.Open()
        Dim cmd As New OleDbCommand()

        cmd.Connection = cn
        Condizione = ""
        If IsDate(Txt_DataDal.Text) And IsDate(Txt_DataAl.Text) Then
            If Condizione <> "" Then
                Condizione = Condizione & " And "
            End If
            Condizione = " Dataregistrazione >= ? And Dataregistrazione <= ?"
            Dim Txt_DataDalText As Date = Txt_DataDal.Text
            Dim Txt_DataAlText As Date = Txt_DataAl.Text

            cmd.Parameters.AddWithValue("@DATADAL", Txt_DataDalText)
            cmd.Parameters.AddWithValue("@DATAAL", Txt_DataAlText)
        End If
        If Val(Txt_NumeroRegistrazioneDal.Text) > 0 And Val(Txt_NumeroRegistrazioneAl.Text) > 0 Then
            If Condizione <> "" Then
                Condizione = Condizione & " And "
            End If

            Condizione = " NumeroRegistrazione >= ? And NumeroRegistrazione <= ?"

            cmd.Parameters.AddWithValue("@NUMERODAL", Txt_NumeroRegistrazioneDal.Text)
            cmd.Parameters.AddWithValue("@NUMEROAL", Txt_NumeroRegistrazioneAl.Text)
        End If
        If Txt_Sottoconto.Text <> "" Then
            If Condizione <> "" Then
                Condizione = Condizione & " And "
            End If
            Dim xVettore(100) As String

            xVettore = SplitWords(Txt_Sottoconto.Text)

            If xVettore.Length > 1 Then
                Condizione = Condizione & " (Select count(*) From MovimentiContabiliRiga Where Numero = NumeroRegistrazione And MastroPartita = " & Val(xVettore(0)) & _
                            " And ContoPartita = " & Val(xVettore(1)) & " And SottoContoPartita = " & Val(xVettore(2)) & " ) > 0"
            End If
        End If

        If Condizione = "" Then
            Dim myriga As System.Data.DataRow = TabellaRisultati.NewRow()
            TabellaRisultati.Rows.Add(myriga)

            GridEstrazione.AutoGenerateColumns = False
            GridEstrazione.DataSource = TabellaRisultati
            GridEstrazione.DataBind()
            Exit Sub
        End If


        cmd.CommandText = ("select * from MOVIMENTICONTABILITESTA where " & Condizione)

        Dim numerorecord As Integer = 0
        Dim myPOSTreader As OleDbDataReader = cmd.ExecuteReader()
        Do While myPOSTreader.Read
            Dim myriga As System.Data.DataRow = TabellaRisultati.NewRow()
            myriga(0) = myPOSTreader.Item("NumeroRegistrazione")
            myriga(1) = Format(myPOSTreader.Item("DataRegistrazione"), "dd/MM/yyyy")
            Dim LegDoc As New Cls_MovimentoContabile
            LegDoc.Leggi(Session("DC_GENERALE"), myPOSTreader.Item("NumeroRegistrazione"))
            Dim TotaleDocumento As Double
            TotaleDocumento = LegDoc.ImportoRegistrazioneDocumento(Session("DC_TABELLE"))

            Dim K As New Cls_CausaleContabile

            K.Leggi(Session("DC_TABELLE"), campodb(myPOSTreader.Item("CAUSALECONTABILE")))
            If K.TipoDocumento = "NC" Then
                TotaleDocumento = TotaleDocumento * -1
            End If
            myriga(2) = Format(TotaleDocumento, "#,##0.00")
            Dim xLegame As New Cls_Legami
            myriga(3) = Format(TotaleDocumento - xLegame.TotaleLegame(Session("DC_GENERALE"), myPOSTreader.Item("NumeroRegistrazione")), "#,##0.00")
            myriga(4) = "0,00"
            Dim MyCausale As New Cls_CausaleContabile
            MyCausale.Leggi(Session("DC_TABELLE"), LegDoc.CausaleContabile)
            myriga(5) = MyCausale.Descrizione
            TabellaRisultati.Rows.Add(myriga)
            numerorecord = numerorecord + 1
            If numerorecord > 100 Then
                Exit Do
            End If
        Loop
        myPOSTreader.Close()

        Session("GestioneLegamiEstrazioni") = TabellaRisultati
        GridEstrazione.AutoGenerateColumns = False
        GridEstrazione.DataSource = TabellaRisultati
        GridEstrazione.DataBind()
        Call EseguiJS()
    End Sub
    Protected Sub Btn_Ricerca_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles Btn_Ricerca.Click
        If Txt_DataAl.Text <> "" Then
            If Not IsDate(Txt_DataAl.Text) Then
                ClientScript.RegisterClientScriptBlock(Me.GetType(), "Errore", "VisualizzaErrore('Data al errata');", True)
                Exit Sub
            End If
        End If
        If Txt_DataDal.Text <> "" Then
            If Not IsDate(Txt_DataDal.Text) Then
                ClientScript.RegisterClientScriptBlock(Me.GetType(), "Errore", "VisualizzaErrore('Data dal errata');", True)
                Exit Sub
            End If
        End If

        Call CaricaPagina()
    End Sub

    Private Function SplitWords(ByVal s As String) As String()
        '
        ' Call Regex.Split function from the imported namespace.
        ' Return the result array.
        '
        Return Regex.Split(s, "\W+")
    End Function

    Protected Sub Grid_RowCommand(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewCommandEventArgs) Handles Grid.RowCommand
        If e.CommandName = "Seleziona" Then
            Dim d As Long

            Tabella = Session("GestioneLegami")
            d = Val(e.CommandArgument)
            Session("NumeroRegistrazione") = Tabella.Rows(d).Item(1).ToString

            Dim MyJS As String
            MyJS = "<script type=""text/javascript"">"
            MyJS = MyJS & " if (window.self != window.top) {"
            MyJS = MyJS & "window.top.location.href = 'incassipagamenti.aspx';"
            MyJS = MyJS & "}"
            MyJS = MyJS & "</script>"
            ClientScript.RegisterStartupScript(Me.GetType(), "RichiamaIncassi", MyJS)
        End If
    End Sub


    Protected Sub Grid_RowDeleting(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewDeleteEventArgs) Handles Grid.RowDeleting                
        
        Tabella.Clear()
        Tabella.Columns.Clear()
        Tabella.Columns.Add("NumeroDocumento", GetType(Long))
        Tabella.Columns.Add("NumIncPag", GetType(Long))
        Tabella.Columns.Add("Importo", GetType(Double))

        Tabella = Session("GestioneLegami")

        Try
            If campodb(Tabella.Rows(e.RowIndex).Item(0)) = "" Then
                Exit Sub
            End If

        Catch ex As Exception
            Exit Sub
        End Try


        Dim NumeroDocumeto As Integer = Tabella.Rows(e.RowIndex).Item(0)
        Dim NumeroPagameto As Integer = Tabella.Rows(e.RowIndex).Item(1)
        Dim I As Integer

        Dim Registrazione As New Cls_MovimentoContabile

        Registrazione.Leggi(Session("DC_GENERALE"), Request.Item("NUMERO"))
        Dim Causale As New Cls_CausaleContabile

        Causale.Leggi(Session("DC_TABELLE"), Registrazione.CausaleContabile)

        Dim Legami As New Cls_Legami
        If Causale.TipoDocumento = "RE" Or Causale.TipoDocumento = "FA" Or Causale.TipoDocumento = "NC" Then
            Legami.Leggi(Session("DC_GENERALE"), Request.Item("NUMERO"), 0)
        Else
            Legami.Leggi(Session("DC_GENERALE"), 0, Request.Item("NUMERO"))
        End If
        For I = 0 To 300
            If Legami.NumeroDocumento(I) = NumeroDocumeto And Legami.NumeroPagamento(I) = NumeroPagameto Then
                Legami.NumeroDocumento(I) = 0
                Legami.NumeroPagamento(I) = 0
                Legami.Importo(I) = 0
            End If
        Next
        If Causale.TipoDocumento = "RE" Or Causale.TipoDocumento = "FA" Or Causale.TipoDocumento = "NC" Then
            Legami.Scrivi(Session("DC_GENERALE"), Request.Item("NUMERO"), 0)
        Else
            Legami.Scrivi(Session("DC_GENERALE"), 0, Request.Item("NUMERO"))
        End If


        Tabella.Rows.RemoveAt(e.RowIndex)

        If Tabella.Rows.Count = 0 Then
            Dim myriga As System.Data.DataRow = Tabella.NewRow()
            Tabella.Rows.Add(myriga)
        End If

        Session("GestioneLegami") = Tabella

        Grid.AutoGenerateColumns = False
        Grid.DataSource = Tabella
        Grid.DataBind()
        Call EseguiJS()
    End Sub
    Public Sub BindRisultati()
        TabellaRisultati = Session("GestioneLegamiEstrazioni")
        GridEstrazione.AutoGenerateColumns = False
        GridEstrazione.DataSource = TabellaRisultati
        GridEstrazione.DataBind()
    End Sub

    Protected Sub GridEstrazione_RowCancelingEdit(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewCancelEditEventArgs) Handles GridEstrazione.RowCancelingEdit
        GridEstrazione.EditIndex = -1
        Call BindRisultati()
    End Sub

    Protected Sub GridEstrazione_RowDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs) Handles GridEstrazione.RowDataBound
        If e.Row.RowType = DataControlRowType.DataRow Then
            TabellaRisultati = Session("GestioneLegamiEstrazioni")

            Dim LblImportoLegame As Label = DirectCast(e.Row.FindControl("LblImportoLegame"), Label)
            If Not IsNothing(LblImportoLegame) Then

            Else
                Dim TxtImportoLegame As TextBox = DirectCast(e.Row.FindControl("TxtImportoLegame"), TextBox)
                TxtImportoLegame.Text = TabellaRisultati.Rows(e.Row.RowIndex).Item(4).ToString
                Dim MyJs As String

                MyJs = "$(document).ready(function() { $('#" & TxtImportoLegame.ClientID & "').blur(function() { var ap = formatNumber ($('#" & TxtImportoLegame.ClientID & "').val(),2); $('#" & TxtImportoLegame.ClientID & "').val(ap); }); });"
                ClientScript.RegisterClientScriptBlock(Me.GetType(), "JS_IMPORTOLEGAME", MyJs, True)
            End If
        End If
    End Sub

    Protected Sub GridEstrazione_RowEditing(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewEditEventArgs) Handles GridEstrazione.RowEditing
        GridEstrazione.EditIndex = e.NewEditIndex
        Call BindRisultati()
    End Sub


    Protected Sub GridEstrazione_RowUpdating(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewUpdateEventArgs) Handles GridEstrazione.RowUpdating
        TabellaRisultati = Session("GestioneLegamiEstrazioni")
        Dim TxtImportoLegame As TextBox = DirectCast(GridEstrazione.Rows(e.RowIndex).FindControl("TxtImportoLegame"), TextBox)

        If Not IsNumeric(TxtImportoLegame.Text) Then
            TxtImportoLegame.Text = 0
        End If

        Dim NumeroRegistrazioneLegam As Integer = TabellaRisultati.Rows(e.RowIndex).Item(0)

        Dim ImportoDaLegare As Double = TabellaRisultati.Rows(e.RowIndex).Item(3)

        If ImportoDaLegare < TxtImportoLegame.Text Then
            Exit Sub
        End If


        Dim I As Integer

        Dim Registrazione As New Cls_MovimentoContabile

        Registrazione.Leggi(Session("DC_GENERALE"), Request.Item("NUMERO"))
        Dim Causale As New Cls_CausaleContabile

        Causale.Leggi(Session("DC_TABELLE"), Registrazione.CausaleContabile)

        Dim Legami As New Cls_Legami
        If Causale.Tipo = "RE" Or Causale.Tipo = "FA" Or Causale.Tipo = "NC" Then
            Legami.Leggi(Session("DC_GENERALE"), Request.Item("NUMERO"), 0)
        Else
            Legami.Leggi(Session("DC_GENERALE"), 0, Request.Item("NUMERO"))            
        End If

        If TxtImportoLegame.Text > 0 Then
            If Math.Round(Registrazione.ImportoRegistrazioneDocumento(Session("DC_TABELLE")) - Legami.TotaleLegame(Session("DC_GENERALE"), Request.Item("NUMERO")), 2) < TxtImportoLegame.Text Then
                Exit Sub
            End If
        End If


        For I = 0 To 300
            If Legami.NumeroDocumento(I) = 0 And Legami.NumeroPagamento(I) = 0 Then
                If Causale.Tipo = "RE" Or Causale.Tipo = "FA" Or Causale.Tipo = "NC" Then
                    Legami.NumeroDocumento(I) = NumeroRegistrazioneLegam 
                    Legami.NumeroPagamento(I) = Registrazione.NumeroRegistrazione
                Else
                    Legami.NumeroDocumento(I) = Registrazione.NumeroRegistrazione                    
                    Legami.NumeroPagamento(I) = NumeroRegistrazioneLegam 
                End If
                Legami.Importo(I) = CDbl(TxtImportoLegame.Text)
                Exit For
            End If
        Next

        If Causale.Tipo = "RE" Or Causale.Tipo = "FA" Or Causale.Tipo = "NC" Then
            Legami.Scrivi(Session("DC_GENERALE"), Request.Item("NUMERO"), 0)
        Else
            Legami.Scrivi(Session("DC_GENERALE"), 0, Request.Item("NUMERO"))            
        End If


        GridEstrazione.EditIndex = -1
        Call aggiornalegami()

        Call CaricaPagina()
    End Sub

    Private Sub EseguiJS()
        Dim MyJs As String
        MyJs = "$(document).ready(function() {"

        MyJs = MyJs & "var els = document.getElementsByTagName(""*"");"

        MyJs = MyJs & "for (var i=0;i<els.length;i++)"
        MyJs = MyJs & "if ( els[i].id ) { "
        MyJs = MyJs & " var appoggio =els[i].id; "
        MyJs = MyJs & " if (appoggio.match('Txt_Sottoconto')!= null) {  "
        MyJs = MyJs & " $(els[i]).autocomplete('/WebHandler/GestioneConto.ashx?Utente=" & Session("UTENTE") & "', {delay:5,minChars:3});"
        MyJs = MyJs & "    }"
        MyJs = MyJs & " if (appoggio.match('Txt_Delegato')!= null) {  "
        MyJs = MyJs & " $(els[i]).autocomplete('autocompleteclientifornitori.ashx?Utente=" & Session("UTENTE") & "', {delay:5,minChars:3});"
        MyJs = MyJs & "    }"



        MyJs = MyJs & " if ((appoggio.match('Txt_Importo')!= null) || appoggio.match('TxtAvere')!= null) {  "
        MyJs = MyJs & " $(els[i]).keypress(function() { ForceNumericInput($(this).val(), true, true); } ); "
        MyJs = MyJs & " $(els[i]).blur(function() { var ap = formatNumber($(this).val(),2); $(this).val(ap); });"
        MyJs = MyJs & "    }"

        MyJs = MyJs & " if (appoggio.match('Txt_Data')!= null) {  "
        MyJs = MyJs & " $(els[i]).mask(""99/99/9999"");"
        MyJs = MyJs & "    }"



        MyJs = MyJs & "} "



        MyJs = MyJs & "if (window.innerHeight>0) { $(""#BarraLaterale"").css(""height"",(window.innerHeight - 94) + ""px""); } else"
        MyJs = MyJs & "{ $(""#BarraLaterale"").css(""height"",(document.documentElement.offsetHeight - 94) + ""px"");  }"
        MyJs = MyJs & "});"

        'MyJs = "$(document).ready(function() { $('.myClass').keypress(function() { handleEnter($('.myClass').val(), true, true); } );     $('#" & Txt_ImportoPagato.ClientID & "').blur(function() { var ap = formatNumber ($('#" & Txt_ImportoPagato.ClientID & "').val(),2); $('#" & Txt_ImportoPagato.ClientID & "').val(ap); }); });"
        ScriptManager.RegisterStartupScript(Me, Me.GetType(), "visualizzaRitIm", MyJs, True)
    End Sub
End Class

