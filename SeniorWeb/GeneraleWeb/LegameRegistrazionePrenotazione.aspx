﻿<%@ Page Language="VB" AutoEventWireup="false" EnableEventValidation="false" Inherits="GeneraleWeb_LegameRegistrazionePrenotazione" CodeFile="LegameRegistrazionePrenotazione.aspx.vb" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
    <title>Legame Prenotazione</title>
    <asp:PlaceHolder runat="server">
        <%: System.Web.Optimization.Styles.Render("~/Content/AjaxControlToolkit/Styles/Bundle") %>
    </asp:PlaceHolder>
    <link href="ospiti.css?ver=11" rel="stylesheet" type="text/css" />
    <link rel="shortcut icon" href="images/SENIOR.ico" />
    <link href="js/jquery.autocomplete.css" rel="stylesheet" type="text/css" />

    <script src="js/jquery-1.5.1.min.js" type="text/javascript"></script>
    <script src="js/jquery.autocomplete.js" type="text/javascript"></script>
    <script src="js/soapclient.js" type="text/javascript"></script>
    <script src="/js/convertinumero.js" type="text/javascript"></script>

    <script src="js/jquery.maskedinput-1.3.min.js" type="text/javascript"></script>
    <script src="/js/formatnumer.js" type="text/javascript"></script>

    <script src="/js/pianoconti.js" type="text/javascript"></script>
    <script src="js/JSErrore.js" type="text/javascript"></script>

    <link rel="stylesheet" href="dialogbox/redips-dialog.css" type="text/css" media="screen" />
    <script type="text/javascript" src="dialogbox/redips-dialog-min.js"></script>
    <script type="text/javascript" src="dialogbox/script.js"></script>
    <script type="text/javascript">  
        $(window).load(function () {
            closedh();
        });

        function DialogBox(Path) {

            REDIPS.dialog.show(500, 300, '<iframe id="output" src="' + Path + '" height="300px" width="500px"></iframe>');
            return false;

        }
        function apridh() {
            $("#MyImportoDIV").css("visibility", 'visible');
        }
        function closedh() {
            $("#MyImportoDIV").css("visibility", 'hidden');
        }
    </script>

</head>
<body>
    <form id="form1" runat="server">
        <asp:ScriptManager ID="ScriptManager1" runat="server" EnableScriptGlobalization="true">
            <Scripts>
                <asp:ScriptReference Path="~/Scripts/AjaxControlToolkit/Bundle" />
            </Scripts>
        </asp:ScriptManager>
        <div style="text-align: left;">
            <br />
            Righe Prenotazioni
        <asp:GridView ID="GrdRighe" runat="server" CellPadding="3" Height="16px"
            Width="590px" ShowFooter="True"
            BackColor="White" BorderColor="#CCCCCC" BorderStyle="None" BorderWidth="1px">
            <RowStyle ForeColor="#000066" />
            <Columns>
                <asp:TemplateField HeaderText="">
                    <ItemTemplate>
                        <asp:ImageButton ID="Seleziona" CommandName="Seleziona" runat="Server"
                            ImageUrl="~/images/select.png" class="EffettoBottoniTondi"
                            CommandArgument="<%#   Container.DataItemIndex  %>" />
                    </ItemTemplate>
                </asp:TemplateField>

                <asp:TemplateField HeaderText="ID">
                    <EditItemTemplate>
                        <asp:Label ID="LblID" runat="server" Text='<%# Eval("ID") %>' Width="136px"></asp:Label>
                    </EditItemTemplate>
                    <ItemTemplate>
                        <asp:Label ID="LblID" runat="server" Text='<%# Eval("ID") %>' Width="136px"></asp:Label>
                    </ItemTemplate>
                </asp:TemplateField>

                <asp:TemplateField HeaderText="Conto">
                    <EditItemTemplate>
                        <asp:Label ID="LblConto" runat="server" Text='<%# Eval("Conto") %>' Width="136px"></asp:Label>
                    </EditItemTemplate>
                    <ItemTemplate>
                        <asp:Label ID="LblConto" runat="server" Text='<%# Eval("Conto") %>' Width="136px"></asp:Label>
                    </ItemTemplate>
                </asp:TemplateField>

                <asp:TemplateField HeaderText="Dare">
                    <EditItemTemplate>
                        <asp:Label ID="LblDare" runat="server" Text='<%# Eval("Dare") %>' Width="136px"></asp:Label>
                    </EditItemTemplate>
                    <ItemTemplate>
                        <asp:Label ID="LblDare" runat="server" Text='<%# Eval("Dare") %>' Width="136px"></asp:Label>
                    </ItemTemplate>
                </asp:TemplateField>


                <asp:TemplateField HeaderText="Avere">
                    <EditItemTemplate>
                        <asp:Label ID="LblAvere" runat="server" Text='<%# Eval("Avere") %>' Width="136px"></asp:Label>
                    </EditItemTemplate>
                    <ItemTemplate>
                        <asp:Label ID="LblAvere" runat="server" Text='<%# Eval("Avere") %>' Width="136px"></asp:Label>
                    </ItemTemplate>
                </asp:TemplateField>
            </Columns>
            <FooterStyle BackColor="White" ForeColor="#000066" />
            <PagerStyle BackColor="White" ForeColor="#000066" HorizontalAlign="Left" />
            <SelectedRowStyle BackColor="#669999" Font-Bold="True" ForeColor="White" />
            <HeaderStyle BackColor="#006699" Font-Bold="True" ForeColor="White" />
        </asp:GridView>
            <br />
            <br />
            Righe Legate a prenotazione<br />
            <br />
            <asp:GridView ID="Grid" runat="server" CellPadding="3" Height="16px"
                Width="590px" ShowFooter="True" BackColor="White" BorderColor="#CCCCCC"
                BorderStyle="None" BorderWidth="1px">
                <RowStyle ForeColor="#000066" />
                <Columns>
                    <asp:CommandField ButtonType="Image" ShowDeleteButton="True" DeleteImageUrl="~/images/cancella.png" />
                    <asp:TemplateField HeaderText="Anno">
                        <EditItemTemplate>
                            <asp:Label ID="LblAnno" runat="server" Text='<%# Eval("Anno") %>' Width="136px"></asp:Label>
                        </EditItemTemplate>
                        <ItemTemplate>
                            <asp:Label ID="LblAnno" runat="server" Text='<%# Eval("Anno") %>' Width="136px"></asp:Label>
                        </ItemTemplate>
                    </asp:TemplateField>

                    <asp:TemplateField HeaderText="Numero">
                        <EditItemTemplate>
                            <asp:Label ID="LblNumero" runat="server" Text='<%# Eval("Numero") %>' Width="136px"></asp:Label>
                        </EditItemTemplate>
                        <ItemTemplate>
                            <asp:Label ID="LblNumero" runat="server" Text='<%# Eval("Numero") %>' Width="136px"></asp:Label>
                        </ItemTemplate>
                    </asp:TemplateField>

                    <asp:TemplateField HeaderText="Riga">
                        <EditItemTemplate>
                            <asp:Label ID="LblRiga" runat="server" Text='<%# Eval("Riga") %>' Width="136px"></asp:Label>
                        </EditItemTemplate>
                        <ItemTemplate>
                            <asp:Label ID="LblRiga" runat="server" Text='<%# Eval("Riga") %>' Width="136px"></asp:Label>
                        </ItemTemplate>
                    </asp:TemplateField>


                    <asp:TemplateField HeaderText="Importo">
                        <EditItemTemplate>
                            <asp:Label ID="LblImporto" runat="server" Text='<%# Eval("Importo") %>' Width="136px"></asp:Label>
                        </EditItemTemplate>
                        <ItemTemplate>
                            <asp:Label ID="LblImporto" runat="server" Text='<%# Eval("Importo") %>' Width="136px"></asp:Label>
                        </ItemTemplate>
                    </asp:TemplateField>

                    <asp:TemplateField HeaderText="Descrizione">
                        <EditItemTemplate>
                            <asp:Label ID="LblDescrizione" runat="server" Text='<%# Eval("Descrizione") %>' Width="136px"></asp:Label>
                        </EditItemTemplate>
                        <ItemTemplate>
                            <asp:Label ID="LblDescrizione" runat="server" Text='<%# Eval("Descrizione") %>' Width="136px"></asp:Label>
                        </ItemTemplate>
                    </asp:TemplateField>

                    <asp:TemplateField HeaderText="SottocontoRiga">
                        <EditItemTemplate>
                            <asp:Label ID="LblSottocontoRiga" runat="server" Text='<%# Eval("SottocontoRiga") %>' Width="136px"></asp:Label>
                        </EditItemTemplate>
                        <ItemTemplate>
                            <asp:Label ID="LblSottocontoRiga" runat="server" Text='<%# Eval("SottocontoRiga") %>' Width="136px"></asp:Label>
                        </ItemTemplate>
                    </asp:TemplateField>
                </Columns>
                <FooterStyle BackColor="White" ForeColor="#000066" />
                <PagerStyle BackColor="White" ForeColor="#000066" HorizontalAlign="Left" />
                <SelectedRowStyle BackColor="#669999" Font-Bold="True" ForeColor="White" />
                <HeaderStyle BackColor="#006699" Font-Bold="True" ForeColor="White" />
            </asp:GridView>

            <br />
            <br />


            Anno
        <asp:TextBox ID="Txt_Anno" autocomplete="off" onkeypress="return soloNumeri(event);" runat="server" Width="52px"></asp:TextBox>
            &nbsp;Numero
        <asp:TextBox ID="Txt_Numero" autocomplete="off" onkeypress="return soloNumeri(event);" runat="server" Width="65px"></asp:TextBox>
            &nbsp;Livello
        <asp:TextBox ID="Txt_Livello" runat="server" Width="266px"></asp:TextBox>

            <asp:Button ID="BTN_Ricerca" runat="server" Text="Ricerca" Width="63px" />

            <asp:GridView ID="GridPrenotazioni" runat="server" BackColor="White"
                BorderColor="#CCCCCC" BorderStyle="None" BorderWidth="1px" CellPadding="3"
                Width="992px">
                <RowStyle ForeColor="#000066" />
                <FooterStyle BackColor="White" ForeColor="#000066" />
                <PagerStyle BackColor="White" ForeColor="#000066" HorizontalAlign="Left" />
                <SelectedRowStyle BackColor="#669999" Font-Bold="True" ForeColor="White" />
                <HeaderStyle BackColor="#006699" Font-Bold="True" ForeColor="White" />
                <Columns>
                    <asp:TemplateField HeaderText="">
                        <ItemTemplate>
                            <asp:ImageButton ID="Seleziona" CommandName="Seleziona" runat="Server"
                                ImageUrl="~/images/select.png" class="EffettoBottoniTondi"
                                CommandArgument="<%#   Container.DataItemIndex  %>" />
                        </ItemTemplate>
                    </asp:TemplateField>
                </Columns>
            </asp:GridView>
        </div>

        <div id="MyImportoDIV" style="position: absolute; top: 40%; left: 35%; background-color: Yellow; width: 300px; height: 30px;">
            Importo :
            <asp:TextBox ID="Txt_Importo" onkeypress="ForceNumericInput(this, true, true)" runat="server"></asp:TextBox>
            <asp:Button ID="Btn_Importo" runat="server" Text="Importo" />
            <a href="#">
                <img src="images/close.png" alt="chiudi" onclick="closedh();" /></a>

        </div>
    </form>
</body>
</html>
