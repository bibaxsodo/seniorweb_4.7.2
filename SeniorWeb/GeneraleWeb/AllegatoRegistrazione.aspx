﻿<%@ Page Language="VB" AutoEventWireup="false" Inherits="GeneraleWeb_AllegatoRegistrazione" CodeFile="AllegatoRegistrazione.aspx.vb" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
    <asp:PlaceHolder runat="server">
        <%: System.Web.Optimization.Styles.Render("~/Content/AjaxControlToolkit/Styles/Bundle") %>
    </asp:PlaceHolder>

    <style>
        .Registrazione {
            width: 98%;
            height: 24px;
            -ms-filter: "progid:DXImageTransform.Microsoft.Alpha(Opacity=96)";
            filter: alpha(opacity=96);
            -moz-opacity: 0.96;
            -khtml-opacity: 0.96;
            opacity: 0.96;
            border: 1px #888888 solid;
            background-color: #86d5f9;
            padding: 2px;
            -moz-box-shadow: 4px 3px 2px 1px #110e0e;
            -webkit-box-shadow: 4px 3px 2px 1px #110e0e;
            box-shadow: 4px 3px 2px 1px #110e0e;
        }
    </style>
</head>
<body>
    <form id="form1" runat="server">
        <asp:ScriptManager ID="ScriptManager1" runat="server" EnableScriptGlobalization="true">
            <Scripts>
                <asp:ScriptReference Path="~/Scripts/AjaxControlToolkit/Bundle" />
            </Scripts>
        </asp:ScriptManager>
        <div>
            <asp:Label ID="lbl_Registrazione" runat="server" CssClass="Registrazione" Width="100%"></asp:Label>
            <br />
            <br />
            <asp:LinkButton ID="lbl_Allegato" target="_blank" runat="server" CssClass="Registrazione" Text="Downlaod" Width="90%"></asp:LinkButton>
            <asp:LinkButton ID="Lnk_Delete" target="_blank" runat="server" CssClass="Registrazione" Text="Elimina" Width="8%"></asp:LinkButton>
            <asp:LinkButton ID="lbl_Allegato1" target="_blank" runat="server" CssClass="Registrazione" Text="Downlaod" Width="90%"></asp:LinkButton>
            <asp:LinkButton ID="Lnk_Delete1" target="_blank" runat="server" CssClass="Registrazione" Text="Elimina" Width="8%"></asp:LinkButton>
            <asp:LinkButton ID="lbl_Allegato2" target="_blank" runat="server" CssClass="Registrazione" Text="Downlaod" Width="90%"></asp:LinkButton>
            <asp:LinkButton ID="Lnk_Delete2" target="_blank" runat="server" CssClass="Registrazione" Text="Elimina" Width="8%"></asp:LinkButton>
            <asp:LinkButton ID="lbl_Allegato3" target="_blank" runat="server" CssClass="Registrazione" Text="Downlaod" Width="90%"></asp:LinkButton>
            <asp:LinkButton ID="Lnk_Delete3" target="_blank" runat="server" CssClass="Registrazione" Text="Elimina" Width="8%"></asp:LinkButton>
            <asp:LinkButton ID="lbl_Allegato4" target="_blank" runat="server" CssClass="Registrazione" Text="Downlaod" Width="90%"></asp:LinkButton>
            <asp:LinkButton ID="Lnk_Delete4" target="_blank" runat="server" CssClass="Registrazione" Text="Elimina" Width="8%"></asp:LinkButton>
            <asp:LinkButton ID="lbl_Allegato5" target="_blank" runat="server" CssClass="Registrazione" Text="Downlaod" Width="90%"></asp:LinkButton>
            <asp:LinkButton ID="Lnk_Delete5" target="_blank" runat="server" CssClass="Registrazione" Text="Elimina" Width="8%"></asp:LinkButton>
            <asp:LinkButton ID="lbl_Allegato6" target="_blank" runat="server" CssClass="Registrazione" Text="Downlaod" Width="90%"></asp:LinkButton>
            <asp:LinkButton ID="Lnk_Delete6" target="_blank" runat="server" CssClass="Registrazione" Text="Elimina" Width="8%"></asp:LinkButton>
            <asp:LinkButton ID="lbl_Allegato7" target="_blank" runat="server" CssClass="Registrazione" Text="Downlaod" Width="90%"></asp:LinkButton>
            <asp:LinkButton ID="Lnk_Delete7" target="_blank" runat="server" CssClass="Registrazione" Text="Elimina" Width="8%"></asp:LinkButton>
            <br />
            <br />

            Carica un file :<br />
            <div id="upload-file-container">
                <div style="text-align: center; width: 300px; padding-top: 15px;" id="nomefile"></div>
                <asp:FileUpload ID="FileUpload1" runat="server" Width="599px" />
            </div>
            <br />
            <br />
            <asp:Button ID="UpLoadFile" runat="server" Height="28px" Width="88px" ToolTip="Up Load File" BackColor="#007DC4" ForeColor="White" Text="Carica File" />

        </div>
    </form>
</body>
</html>
