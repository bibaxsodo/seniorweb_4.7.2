﻿Imports System.Web.Hosting
Imports System.Data.OleDb

Partial Class GeneraleWeb_VisualizzaScadenzario
    Inherits System.Web.UI.Page
    Dim Tabella As New System.Data.DataTable("tabella")

    Function campodbN(ByVal oggetto As Object) As Double
        If IsDBNull(oggetto) Then
            Return 0
        Else
            Return oggetto
        End If
    End Function
    Function campodb(ByVal oggetto As Object) As String
        If IsDBNull(oggetto) Then
            Return ""
        Else
            Return oggetto
        End If
    End Function
    Private Function SplitWords(ByVal s As String) As String()
        '
        ' Call Regex.Split function from the imported namespace.
        ' Return the result array.
        '
        Return Regex.Split(s, "\W+")
    End Function
    Private Sub Esegui(ByVal ToExcel As Boolean)
        Dim Condizione As String
        Dim MySql As String
        Dim DATE1 As Date = Txt_DataDal.Text
        Dim DATE2 As Date = Txt_DataAl.Text


        Condizione = ""
        If IsDate(Txt_DataDal.Text) Then
            Condizione = " And Scadenzario.DataScadenza >= {ts '" & Format(DATE1, "yyyy-MM-dd") & " 00:00:00'} And Scadenzario.DataScadenza <= {ts '" & Format(DATE2, "yyyy-MM-dd") & " 00:00:00'}"
        End If

        If Trim(Txt_Sottoconto.Text) <> "" Then
            Dim Vettore(100) As String
            Dim APPOGGIO As String

            APPOGGIO = Txt_Sottoconto.Text
            Vettore = SplitWords(APPOGGIO)

            If Vettore.Length > 0 Then
                If Val(Vettore(0)) > 0 Then
                    If Condizione <> "" Then
                        Condizione = Condizione & " AND "
                    End If
                    Condizione = Condizione & " MovimentiContabiliRiga.MastroPartita = " & Val(Vettore(0))
                End If
            End If
            If Vettore.Length > 1 Then
                If Val(Vettore(1)) > 0 Then
                    If Condizione <> "" Then
                        Condizione = Condizione & " AND "
                    End If
                    Condizione = Condizione & " MovimentiContabiliRiga.ContoPartita = " & Val(Vettore(1))
                End If
            End If

            If Vettore.Length > 2 Then
                If Val(Vettore(2)) > 0 Then
                    If Condizione <> "" Then
                        Condizione = Condizione & " AND "
                    End If
                    Condizione = Condizione & " MovimentiContabiliRiga.SottoContoPartita = " & Val(Vettore(2))
                End If
            End If
        End If



        If Chk_SoloAperte.Checked = True Then
            Condizione = Condizione & " And Chiusa =0 "
        End If

        Tabella.Clear()
        Tabella.Columns.Clear()
        Tabella.Columns.Add("NumeroRegistrazione", GetType(String))
        Tabella.Columns.Add("Numero Documento", GetType(String))
        Tabella.Columns.Add("Numero Protocollo", GetType(String))
        Tabella.Columns.Add("Numero", GetType(String))
        Tabella.Columns.Add("Data Documento", GetType(String))
        Tabella.Columns.Add("Importo", GetType(String))
        Tabella.Columns.Add("Data Scadenziario", GetType(String))
        Tabella.Columns.Add("Ragione Sociale", GetType(String))
        Tabella.Columns.Add("Descrizione", GetType(String))
        Tabella.Columns.Add("Modalita Pagamento", GetType(String))
        Tabella.Columns.Add("Aperta/Chiusta", GetType(String))
        Tabella.Columns.Add("Importo Pagato", GetType(String))
        Tabella.Columns.Add("Legato Split/Reverse", GetType(String))
        Tabella.Columns.Add("Saldo", GetType(String))


        MySql = "SELECT MovimentiContabiliRiga.Numero,Scadenzario.Numero as ScNumero, Scadenzario.Importo, Scadenzario.DataScadenza, MovimentiContabiliRiga.MastroPartita, MovimentiContabiliRiga.ContoPartita, MovimentiContabiliRiga.SottocontoPartita,MovimentiContabiliTesta.NumeroProtocollo, MovimentiContabiliTesta.NumeroDocumento, MovimentiContabiliTesta.DataDocumento, (Select Sum(Importo) from TabellaLegami where CodiceDocumento = NumeroRegistrazione )  as ImportoPagato, Scadenzario.Chiusa,Scadenzario.Descrizione " & _
                " FROM (Scadenzario INNER JOIN MovimentiContabiliRiga ON Scadenzario.NumeroRegistrazioneContabile = MovimentiContabiliRiga.Numero) " & _
                " INNER JOIN MovimentiContabiliTesta ON MovimentiContabiliRiga.Numero = MovimentiContabiliTesta.NumeroRegistrazione " & _
                " WHERE ((MovimentiContabiliRiga.Tipo ='CF' And NumeroProtocollo > 0)  Or (NumeroProtocollo = 0))" & Condizione



       
        Dim cn As OleDbConnection

        cn = New Data.OleDb.OleDbConnection(Session("DC_GENERALE"))

        cn.Open()
        Dim cmd As New OleDbCommand()

        cmd.CommandText = (MySql)

        cmd.Connection = cn
        Dim myPOSTreader As OleDbDataReader = cmd.ExecuteReader()
        Do While myPOSTreader.Read
            Dim myriga As System.Data.DataRow = Tabella.NewRow()
            myriga(0) = campodb(myPOSTreader.Item("Numero"))
            myriga(1) = campodb(myPOSTreader.Item("NumeroDocumento"))
            myriga(2) = campodb(myPOSTreader.Item("NumeroProtocollo"))
            myriga(3) = campodb(myPOSTreader.Item("ScNumero"))
            myriga(4) = campodb(myPOSTreader.Item("DataDocumento"))
            myriga(5) = Format(campodbN(myPOSTreader.Item("Importo")), "#,##00.00")
            myriga(6) = campodb(myPOSTreader.Item("DataScadenza"))
            Dim XK As New Cls_Pianodeiconti
            XK.Mastro = campodb(myPOSTreader.Item("MastroPartita"))
            XK.Conto = campodb(myPOSTreader.Item("ContoPartita"))
            XK.Sottoconto = campodb(myPOSTreader.Item("SottoContoPartita"))
            XK.Decodfica(Session("DC_GENERALE"))
            myriga(7) = XK.Descrizione
            myriga(8) = campodb(myPOSTreader.Item("Descrizione"))

            Dim Registrazione As New Cls_MovimentoContabile

            Registrazione.NumeroRegistrazione = campodbN(myPOSTreader.Item("Numero"))
            Registrazione.Leggi(Session("DC_GENERALE"), Registrazione.NumeroRegistrazione)


            If Registrazione.ModalitaPagamento <> "" Then
                Dim ModPag As New Cls_TipoPagamento

                ModPag.Codice = Registrazione.ModalitaPagamento
                ModPag.Leggi(Session("DC_TABELLE"))

                myriga(9) = ModPag.Descrizione
            End If



            If Val(campodb(myPOSTreader.Item("Chiusa"))) = "1" Then
                myriga(10) = "Chiuso"
            Else
                myriga(10) = "Aperta"
            End If

            Dim MovCon As New Cls_MovimentoContabile
            Dim ImportIVA As Double = 0
            Dim ImportoReverseSplit As Double = 0

            MovCon.NumeroRegistrazione = Val(campodb(myPOSTreader.Item("Numero")))
            MovCon.Leggi(Session("DC_GENERALE"), MovCon.NumeroRegistrazione)

            Dim Indice As Integer = 0

            For i = 0 To 100
                If Not IsNothing(MovCon.Righe(i)) Then
                    If MovCon.Righe(i).Tipo = "IV" Then
                        ImportIVA = ImportIVA + MovCon.Righe(i).Importo
                    End If
                End If
            Next

            Dim Legami As New Cls_Legami

            Legami.Leggi(Session("DC_GENERALE"), MovCon.NumeroRegistrazione, 0)

            For i = 0 To 100
                If Not IsNothing(Legami.Importo(i)) Then
                    If Legami.Importo(i) > 0 Then
                        Dim LeggiReg As New Cls_MovimentoContabile

                        LeggiReg.NumeroRegistrazione = Legami.NumeroPagamento(i)
                        LeggiReg.Leggi(Session("DC_GENERALE"), LeggiReg.NumeroRegistrazione)

                        If LeggiReg.CausaleContabile <> "" Then
                            Dim CausaleContabile As New Cls_CausaleContabile

                            CausaleContabile.Codice = MovCon.CausaleContabile
                            CausaleContabile.Leggi(Session("DC_TABELLE"), CausaleContabile.Codice)

                            If CausaleContabile.GirocontoReverse = LeggiReg.CausaleContabile Then
                                If ImportIVA = Legami.Importo(i) Then
                                    myriga(12) = Format(ImportIVA, "#,##00.00")
                                    ImportoReverseSplit = ImportIVA
                                End If
                            End If
                        End If

                    End If
                End If
            Next

            If ImportoReverseSplit > 0 Then
                If Math.Round(campodbN(myPOSTreader.Item("Importo")), 2) = Math.Round(MovCon.ImportoDocumento(Session("DC_TABELLE")), 2) Then
                    ImportoReverseSplit = 0
                End If
            End If

            myriga(11) = Format(campodbN(myPOSTreader.Item("ImportoPagato")) - ImportoReverseSplit, "#,##00.00")



            myriga(13) = Format(campodbN(myPOSTreader.Item("Importo")) - (campodbN(myPOSTreader.Item("ImportoPagato")) - ImportoReverseSplit), "#,##00.00")

            Tabella.Rows.Add(myriga)
        Loop
        myPOSTreader.Close()
        cn.Close()

        Session("Scadenziario") = Tabella

        If ToExcel = False Then
            Grid.AutoGenerateColumns = True
            Grid.DataSource = Tabella
            Grid.Font.Size = 10
            Grid.DataBind()
        Else
            GridView1.AutoGenerateColumns = True
            GridView1.DataSource = Tabella
            GridView1.Font.Size = 10
            GridView1.DataBind()
        End If
    End Sub
    Protected Sub ImageButton1_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles ImageButton1.Click

        If Not IsDate(Txt_DataDal.Text) Then
            ScriptManager.RegisterStartupScript(Me, Me.GetType(), "visualizzaA", "VisualizzaErrore('Data dal formalemente errata');", True)
            REM Lbl_Errori.Text = "Data dal formalemente errata"
            Exit Sub
        End If
        If Not IsDate(Txt_DataAl.Text) Then
            ScriptManager.RegisterStartupScript(Me, Me.GetType(), "visualizzaA", "VisualizzaErrore('Data al formalemente errata');", True)
            REM Lbl_Errori.Text = "Data dal formalemente errata"
            Exit Sub
        End If

        Esegui(False)
    End Sub

    Protected Sub Grid_RowCommand(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewCommandEventArgs) Handles Grid.RowCommand
        If e.CommandName = "Seleziona" Then
            Dim ConnectionString As String = Session("DC_OSPITE")
            Dim d As Integer

            If ViewState("XLINEA") <> -1 Then
                Grid.Rows(ViewState("XLINEA")).BackColor = ViewState("XBACKUP")
            End If


            Tabella = Session("Scadenziario")
            d = Val(e.CommandArgument)
            Session("NumeroRegistrazione") = Tabella.Rows(d).Item(0).ToString



            ViewState("XLINEA") = d
            ViewState("XBACKUP") = Grid.Rows(d).BackColor
            Grid.Rows(d).BackColor = Drawing.Color.Violet
        End If

        If (e.CommandName = "Richiama") Then
            Dim Registrazione As Long
            Dim d As Integer

            Tabella = Session("Scadenziario")
            d = Val(e.CommandArgument)
            Registrazione = Tabella.Rows(d).Item(0).ToString



            Dim Reg As New Cls_MovimentoContabile

            Reg.Leggi(Session("DC_GENERALE"), Registrazione)
            Dim CauCon As New Cls_CausaleContabile

            CauCon.Leggi(Session("DC_TABELLE"), Reg.CausaleContabile)
            If CauCon.Tipo = "I" Or CauCon.Tipo = "R" Then
                Session("NumeroRegistrazione") = Registrazione
                ClientScript.RegisterClientScriptBlock(Me.GetType(), "MyBox", "$(window).load(function() {  DialogBox('../GeneraleWeb/Documenti.aspx');  });", True)
                REM Response.Redirect("../GeneraleWeb/Documenti.aspx")
                Exit Sub
            End If
            If CauCon.Tipo = "P" Then
                Session("NumeroRegistrazione") = Registrazione
                ClientScript.RegisterClientScriptBlock(Me.GetType(), "MyBox", "$(window).load(function() {  DialogBox('../GeneraleWeb/incassipagamenti.aspx');  });", True)
                REM Response.Redirect("../GeneraleWeb/incassipagamenti.aspx")
                Exit Sub
            End If
            Session("NumeroRegistrazione") = Registrazione
            ClientScript.RegisterClientScriptBlock(Me.GetType(), "MyBox", "$(window).load(function() {  DialogBox('../GeneraleWeb/primanota.aspx');  });", True)
            REM Response.Redirect("../GeneraleWeb/primanota.aspx")
            Exit Sub
        End If
    End Sub


    Protected Sub ImageButton2_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles ImageButton2.Click
        If Not IsDate(Txt_DataDal.Text) Then
            ClientScript.RegisterClientScriptBlock(Me.GetType(), "Errore", "VisualizzaErrore('Data dal formalemente errata');", True)
            REM Lbl_Errori.Text = "Data dal formalemente errata"
            Exit Sub
        End If
        If Not IsDate(Txt_DataAl.Text) Then
            ClientScript.RegisterClientScriptBlock(Me.GetType(), "Errore", "VisualizzaErrore('Data al formalemente errata');", True)
            REM Lbl_Errori.Text = "Data dal formalemente errata"
            Exit Sub
        End If

        Esegui(True)

        If Tabella.Rows.Count > 1 Then
            Session("GrigliaSoloStampa") = Session("Scadenziario")

            ScriptManager.RegisterClientScriptBlock(Me, Me.GetType(), "Stampa", "window.open('ExportExcel.aspx','Excel','width=800,height=600');", True)
        End If
    End Sub

   
    Protected Sub Btn_Esci_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles Btn_Esci.Click
        Response.Redirect("Menu_Visualizzazioni.aspx")
    End Sub

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Trim(Session("UTENTE")) = "" Then
            If Request.ServerVariables("URL").ToUpper.IndexOf("SENIORWEBBLANCO") > 0 Then
                Response.Redirect("/ODV/Login.aspx")
                Exit Sub
            Else
                Response.Redirect("/SeniorWeb/Login.aspx")
                Exit Sub
            End If
        End If


        Call EseguiJS()

        Dim K1 As New Cls_SqlString

        Dim Barra As New Cls_BarraSenior


        Lbl_BarraSenior.Text = Barra.CodiceBarra(Application("SENIOR"), Session("UTENTE"), Page, K1)
    End Sub

    Private Sub EseguiJS()
        Dim MyJs As String
        MyJs = "$(document).ready(function() {"

        MyJs = MyJs & "var els = document.getElementsByTagName(""*"");"

        MyJs = MyJs & "for (var i=0;i<els.length;i++)"
        MyJs = MyJs & "if ( els[i].id ) { "
        MyJs = MyJs & " var appoggio =els[i].id; "
        MyJs = MyJs & " if (appoggio.match('Txt_Sottoconto')!= null) {  "
        MyJs = MyJs & " $(els[i]).autocomplete('/WebHandler/GestioneConto.ashx?Utente=" & Session("UTENTE") & "', {delay:5,minChars:3});"
        MyJs = MyJs & "    }"

        MyJs = MyJs & " if (appoggio.match('Txt_Data')!= null) {  "
        MyJs = MyJs & " $(els[i]).mask(""99/99/9999"");  "
        MyJs = MyJs & " $(els[i]).datepicker({ changeMonth: true,changeYear: true,  yearRange: ""1890:" & Year(Now) + 1 & """},$.datepicker.regional[""it""]);"
        MyJs = MyJs & "    }"

        MyJs = MyJs & " if (appoggio.match('Txt_Importo')!= null)  {  "
        MyJs = MyJs & " $(els[i]).keypress(function() { ForceNumericInput($(this).val(), true, true); } ); "
        MyJs = MyJs & " $(els[i]).blur(function() { var ap = formatNumber($(this).val(),2); $(this).val(ap); });"
        MyJs = MyJs & "    }"

        MyJs = MyJs & "} "
        MyJs = MyJs & "});"

        'MyJs = "$(document).ready(function() { $('.myClass').keypress(function() { handleEnter($('.myClass').val(), true, true); } );     $('#" & Txt_ImportoPagato.ClientID & "').blur(function() { var ap = formatNumber ($('#" & Txt_ImportoPagato.ClientID & "').val(),2); $('#" & Txt_ImportoPagato.ClientID & "').val(ap); }); });"
        ScriptManager.RegisterStartupScript(Me, Me.GetType(), "visualizzaRitIm", MyJs, True)
    End Sub
End Class
