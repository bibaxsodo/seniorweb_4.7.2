﻿Imports System.Data.OleDb
Imports System.Web
Imports System.Web.Services

Public Class autocompleteclientifornitori
    Implements System.Web.IHttpHandler, IRequiresSessionState

    Public Sub ProcessRequest(ByVal context As HttpContext) Implements IHttpHandler.ProcessRequest
        context.Response.ContentType = "text/plain"
        Dim RICERCA As String = context.Request.QueryString("q")
        Dim UTENTE As String = context.Request.QueryString("UTENTE")
        context.Response.Cache.SetCacheability(HttpCacheability.NoCache)

        Dim sb As StringBuilder = New StringBuilder

        Dim cn As OleDbConnection
        'Dim DbC As New Cls_Login

        'DbC.Utente = UTENTE
        'DbC.LeggiSP(context.Application("SENIOR"))

        cn = New Data.OleDb.OleDbConnection(context.Session("DC_OSPITE"))

        cn.Open()
        Dim cmd As New OleDbCommand()


        If Val(RICERCA) > 0 Then

            cmd.CommandText = ("select * from AnagraficaComune where Tipologia = 'D' And " &
                               "CodiceDebitoreCreditore = ? Order by Nome")
            cmd.Parameters.AddWithValue("@CodiceDebitoreCreditore", Val(RICERCA))
        Else

            cmd.CommandText = ("select * from AnagraficaComune where Tipologia = 'D' And " &
                               "Nome Like ? Order by Nome")
            cmd.Parameters.AddWithValue("@Nome", "%" & RICERCA & "%")
        End If

        cmd.Connection = cn

        Dim Counter As Integer = 0
        Dim myPOSTreader As OleDbDataReader = cmd.ExecuteReader()
        Do While myPOSTreader.Read

            sb.Append(myPOSTreader.Item("CodiceDebitoreCreditore") & " " & myPOSTreader.Item("Nome")).Append(Environment.NewLine)
            Counter = Counter + 1
            If Counter > 20 Then
                Exit Do
            End If
        Loop
        context.Response.Write(sb.ToString)
    End Sub

    Public ReadOnly Property IsReusable() As Boolean Implements IHttpHandler.IsReusable
        Get
            Return False
        End Get
    End Property

End Class