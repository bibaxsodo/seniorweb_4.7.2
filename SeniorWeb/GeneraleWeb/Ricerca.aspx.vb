﻿Imports System.Web.Hosting
Imports System.Data.OleDb

Partial Class Ricerca
    Inherits System.Web.UI.Page
    Dim Tabella As New System.Data.DataTable("tabella")

    Protected Sub Esegui_Ricerca(ByVal ToExcel As Boolean)
        Dim ConnectionString As String = Session("DC_TABELLE")
        Dim ConnectionStringGenerale As String = Session("DC_GENERALE")
        Dim MySql As String
        Dim Condizione As String
        Dim Tipo As Integer = 0
        Dim MySqlrs As String
        Dim MySqlrs2 As String
        Dim Saldo As Double



        Dim cn As OleDbConnection

        cn = New Data.OleDb.OleDbConnection(ConnectionStringGenerale)

        cn.Open()

        If Trim(Txt_Sottoconto.Text) <> "" Then
            Tipo = 2
            MySql = "Select '0' as MyOrdine,NumeroRegistrazione,DataRegistrazione,NumeroDocumento,DataDocumento,NumeroProtocollo,AnnoProtocollo,CausaleContabile,MastroPartita,ContoPartita,SottocontoPartita,MovimentiContabiliRiga.Descrizione,MovimentiContabiliTesta.Descrizione as DescrizioneT,DareAvere,Importo,RigaDaCausale,MastroContropartita,ContoContropartita,SottocontoContropartita,Imponibile,CodiceRitenuta,MovimentiContabiliRiga.id FROM MovimentiContabiliRiga , MovimentiContabiliTesta Where MovimentiContabiliRiga.Numero = MovimentiContabiliTesta.NumeroRegistrazione And (Invisibile = 0 OR Invisibile Is Null) And "
            MySqlrs = "Select '1' as MyOrdine,NumeroRegistrazione,DataRegistrazione,NumeroDocumento,DataDocumento,NumeroProtocollo,AnnoProtocollo,CausaleContabile,MovimentiContabiliRiga.Descrizione,MovimentiContabiliTesta.Descrizione as DescrizioneT,DareAvere,Importo,RigaDaCausale,MastroContropartita,ContoContropartita,SottocontoContropartita,Imponibile,CodiceRitenuta,MovimentiContabiliRiga.id FROM MovimentiContabiliRiga , MovimentiContabiliTesta Where MovimentiContabiliRiga.Numero = MovimentiContabiliTesta.NumeroRegistrazione And (Invisibile = 0 OR Invisibile Is Null) And  "
            MySqlrs2 = "Select '2' as MyOrdine,NumeroRegistrazione,DataRegistrazione,NumeroDocumento,DataDocumento,NumeroProtocollo,AnnoProtocollo,CausaleContabile,MovimentiContabiliRiga.Descrizione,MovimentiContabiliTesta.Descrizione as DescrizioneT,DareAvere,Importo,RigaDaCausale,MastroContropartita,ContoContropartita,SottocontoContropartita,Imponibile,CodiceRitenuta,MovimentiContabiliRiga.id FROM MovimentiContabiliRiga , MovimentiContabiliTesta Where MovimentiContabiliRiga.Numero = MovimentiContabiliTesta.NumeroRegistrazione And (Invisibile = 0 OR Invisibile Is Null) And  "
        Else
            Tipo = 1
            MySql = "Select '0' as MyOrdine,NumeroRegistrazione,DataRegistrazione,NumeroDocumento,DataDocumento,NumeroProtocollo,AnnoProtocollo,CausaleContabile,MastroPartita,ContoPartita,SottocontoPartita,MovimentiContabiliRiga.Descrizione,MovimentiContabiliTesta.Descrizione as DescrizioneT,DareAvere,Importo,RigaDaCausale,MastroContropartita,ContoContropartita,SottocontoContropartita,Imponibile,CodiceRitenuta,MovimentiContabiliRiga.id FROM MovimentiContabiliRiga ,MovimentiContabiliTesta Where MovimentiContabiliRiga.Numero = MovimentiContabiliTesta.NumeroRegistrazione And (Invisibile = 0 OR Invisibile Is Null) And "
            MySqlrs = "Select '1' as MyOrdine,NumeroRegistrazione,DataRegistrazione,NumeroDocumento,DataDocumento,NumeroProtocollo,AnnoProtocollo,CausaleContabile,MastroPartita,ContoPartita,SottocontoPartita,MovimentiContabiliRiga.Descrizione,MovimentiContabiliTesta.Descrizione as DescrizioneT,DareAvere,Importo,RigaDaCausale,MastroContropartita,ContoContropartita,SottocontoContropartita,Imponibile,CodiceRitenuta,MovimentiContabiliRiga.id FROM MovimentiContabiliRiga ,MovimentiContabiliTesta Where MovimentiContabiliRiga.Numero = MovimentiContabiliTesta.NumeroRegistrazione And (Invisibile = 0 OR Invisibile Is Null) And "
            MySqlrs2 = "Select '2' as MyOrdine,NumeroRegistrazione,DataRegistrazione,NumeroDocumento,DataDocumento,NumeroProtocollo,AnnoProtocollo,CausaleContabile,MastroPartita,ContoPartita,SottocontoPartita,MovimentiContabiliRiga.Descrizione,MovimentiContabiliTesta.Descrizione as DescrizioneT,DareAvere,Importo,RigaDaCausale,MastroContropartita,ContoContropartita,SottocontoContropartita,Imponibile,CodiceRitenuta,MovimentiContabiliRiga.id FROM MovimentiContabiliRiga ,MovimentiContabiliTesta Where MovimentiContabiliRiga.Numero = MovimentiContabiliTesta.NumeroRegistrazione And (Invisibile = 0 OR Invisibile Is Null) And "
        End If

        Condizione = ""

        If Trim(Txt_Sottoconto.Text) <> "" Then
            Dim Vettore(100) As String
            Dim APPOGGIO As String

            APPOGGIO = Txt_Sottoconto.Text
            Vettore = SplitWords(APPOGGIO)



            If Chk_ControPartita.Checked = True Then
                Try

                    If Val(Vettore(0)) > 0 Then
                        If Condizione <> "" Then
                            Condizione = Condizione & " AND "
                        End If
                        Condizione = Condizione & " MastroControPartita = " & Val(Vettore(0))
                    End If

                    If Val(Vettore(1)) > 0 Then
                        If Condizione <> "" Then
                            Condizione = Condizione & " AND "
                        End If
                        Condizione = Condizione & " ContoControPartita = " & Val(Vettore(1))
                    End If

                    If Val(Vettore(2)) > 0 Then
                        If Condizione <> "" Then
                            Condizione = Condizione & " AND "
                        End If
                        Condizione = Condizione & " SottoContoControPartita = " & Val(Vettore(2))
                    End If
                Catch ex As Exception

                End Try
            Else
                Try
                    If Val(Vettore(0)) > 0 Then
                        If Condizione <> "" Then
                            Condizione = Condizione & " AND "
                        End If
                        Condizione = Condizione & " MastroPartita = " & Val(Vettore(0))
                    End If

                    If Val(Vettore(1)) > 0 Then
                        If Condizione <> "" Then
                            Condizione = Condizione & " AND "
                        End If
                        Condizione = Condizione & " ContoPartita = " & Val(Vettore(1))
                    End If

                    If Val(Vettore(2)) > 0 Then
                        If Condizione <> "" Then
                            Condizione = Condizione & " AND "
                        End If
                        Condizione = Condizione & " SottoContoPartita = " & Val(Vettore(2))
                    End If

                Catch ex As Exception

                End Try
            End If





            Saldo = 0

            Try
                If IsDate(Txt_DataDal.Text) Then
                    Dim DataDal As Date

                    DataDal = DateSerial(Mid(Txt_DataDal.Text, 7, 4), Mid(Txt_DataDal.Text, 4, 2), Mid(Txt_DataDal.Text, 1, 2))
                    Dim cmdSaldoD As New OleDbCommand()
                    cmdSaldoD.CommandText = "SELECT SUM (Importo) As Totale FROM MovimentiContabiliTesta " & _
                        " INNER JOIN MovimentiContabiliRiga " & _
                        " ON MovimentiContabiliTesta.NumeroRegistrazione = MovimentiContabiliRiga.Numero " & _
                        " WHERE MastroPartita = " & Val(Vettore(0)) & _
                        " AND ContoPartita = " & Val(Vettore(1)) & _
                        " AND SottocontoPartita = " & Val(Vettore(2)) & _
                        " AND DareAvere = 'D' " & _
                        " AND DataRegistrazione < {ts'" & Format(DataDal, "yyyy-MM-dd") & " 00:00:00'}"
                    cmdSaldoD.Connection = cn

                    Dim ReadSaldoD As OleDbDataReader = cmdSaldoD.ExecuteReader()
                    If ReadSaldoD.Read Then
                        Saldo = Saldo + campodbn(ReadSaldoD.Item("Totale"))
                    End If
                    ReadSaldoD.Close()

                    Dim cmdSaldo As New OleDbCommand()
                    cmdSaldo.CommandText = "SELECT SUM (Importo) As Totale FROM MovimentiContabiliTesta " & _
                        " INNER JOIN MovimentiContabiliRiga " & _
                        " ON MovimentiContabiliTesta.NumeroRegistrazione = MovimentiContabiliRiga.Numero " & _
                        " WHERE MastroPartita = " & Val(Vettore(0)) & _
                        " AND ContoPartita = " & Val(Vettore(1)) & _
                        " AND SottocontoPartita = " & Val(Vettore(2)) & _
                        " AND DareAvere = 'A' " & _
                        " AND DataRegistrazione < {ts'" & Format(DataDal, "yyyy-MM-dd") & " 00:00:00'}"
                    cmdSaldo.Connection = cn

                    Dim ReadSaldo As OleDbDataReader = cmdSaldo.ExecuteReader()
                    If ReadSaldo.Read Then
                        Saldo = Saldo - campodbn(ReadSaldo.Item("Totale"))
                    End If
                    ReadSaldo.Close()


                End If

            Catch ex As Exception

            End Try

        End If

        If Txt_Descrizione.Text <> "" Then
            If Condizione <> "" Then
                Condizione = Condizione & " And "
            End If
            Condizione = Condizione & " MovimentiContabiliTesta.Descrizione Like '" & Txt_Descrizione.Text & "'"
        End If

        If IsDate(Txt_DataBolletta.Text) Then
            If Condizione <> "" Then
                Condizione = Condizione & " And "
            End If
            Condizione = Condizione & " MovimentiContabiliTesta.Descrizione Like '" & Txt_Descrizione.Text & "'"
        End If

        If IsDate(Txt_DataDal.Text) Then
            If Condizione <> "" Then
                Condizione = Condizione & " And "
            End If
            Condizione = Condizione & " DataRegistrazione >= ?"
        End If

        If IsDate(Txt_DataAl.Text) Then
            If Condizione <> "" Then
                Condizione = Condizione & " And "
            End If
            Condizione = Condizione & " DataRegistrazione <= ? "
        End If
        If Chk_BolloVirtuale.Checked = True Then
            If Condizione <> "" Then
                Condizione = Condizione & " And "
            End If
            Condizione = Condizione & " BolloVirtuale = 1 "
        End If

        If DD_CausaleContabile.SelectedValue <> "" Then
            If Condizione <> "" Then
                Condizione = Condizione & " And "
            End If
            Condizione = Condizione & "( CausaleContabile = '" & DD_CausaleContabile.SelectedValue & "'"
            If DD_CausaleContabile1.SelectedValue <> "" Then
                Condizione = Condizione & " OR CausaleContabile = '" & DD_CausaleContabile1.SelectedValue & "'"
            End If
            If DD_CausaleContabile2.SelectedValue <> "" Then
                Condizione = Condizione & " OR CausaleContabile = '" & DD_CausaleContabile2.SelectedValue & "'"
            End If
            If DD_CausaleContabile3.SelectedValue <> "" Then
                Condizione = Condizione & " OR CausaleContabile = '" & DD_CausaleContabile3.SelectedValue & "'"
            End If
            Condizione = Condizione & ") "
        End If

        If Val(DD_RegistroIVA.SelectedValue) <> 0 Then
            If Condizione <> "" Then
                Condizione = Condizione & " And "
            End If
            Condizione = Condizione & " RegistroIVA = " & DD_RegistroIVA.SelectedValue
        End If

        If Txt_DataBolletta.Text <> "" Then
            If IsDate(Txt_DataBolletta.Text) Then
                If Condizione <> "" Then
                    Condizione = Condizione & " And "
                End If
                Condizione = Condizione & " DataBolletta = ? "
            End If
        End If

        If Txt_NumeroBolletta.Text <> "" Then
            If Condizione <> "" Then
                Condizione = Condizione & " And "
            End If
            Condizione = Condizione & " NumeroBolletta = '" & Txt_NumeroBolletta.Text & "'"
        End If

        If Val(Txt_AnnoProtocollo.Text) <> 0 Then
            If Condizione <> "" Then
                Condizione = Condizione & " And "
            End If
            Condizione = Condizione & " AnnoProtocollo = " & Txt_AnnoProtocollo.Text
        End If

        If Val(Txt_NumeroProtocollo.Text) <> 0 Then
            If Condizione <> "" Then
                Condizione = Condizione & " And "
            End If
            Condizione = Condizione & " NumeroProtocollo = " & Txt_NumeroProtocollo.Text
        End If

        If Txt_Importo.Text <> "" Then
            If CDbl(Txt_Importo.Text) > 0 Then
                If Condizione <> "" Then
                    Condizione = Condizione & " And "
                End If
                Condizione = Condizione & " Importo = ? "
            End If
        End If

        If Val(Txt_NumeroDal.Text) > 0 Then
            If Condizione <> "" Then
                Condizione = Condizione & " And "
            End If
            Condizione = Condizione & " NumeroRegistrazione >= ?"
        End If

        If Val(Txt_NumeroAl.Text) > 0 Then
            If Condizione <> "" Then
                Condizione = Condizione & " And "
            End If
            Condizione = Condizione & " NumeroRegistrazione <= ? "
        End If

        If Txt_NumeroDocumento.Text <> "" Then
            If Condizione <> "" Then
                Condizione = Condizione & " And "
            End If
            Condizione = Condizione & " NumeroDocumento = ? "
        End If


        If DD_IVA.SelectedValue <> "" Then
            If Condizione <> "" Then
                Condizione = Condizione & " And "
            End If
            Condizione = Condizione & " (CodiceIVA = ?  And Tipo = 'IV')"
        End If



        If DD_Detraibilita.SelectedValue <> "" And DD_Detraibilita.SelectedValue <> "Null" Then
            If Condizione <> "" Then
                Condizione = Condizione & " And "
            End If
            Condizione = Condizione & " (Detraibile = ?  And Tipo = 'IV')"
        End If

        If DD_Detraibilita.SelectedValue = "Null" Then
            If Condizione <> "" Then
                Condizione = Condizione & " And "
            End If
            Condizione = Condizione & " ((Detraibile IS Null or Detraibile = '' ) And Tipo = 'IV')"
        End If



        If DD_CSERV.SelectedValue <> "" Then
            If Condizione <> "" Then
                Condizione = Condizione & " And "
            End If
            Condizione = Condizione & " MovimentiContabiliTesta.CentroServizio = ? "
        End If

        Lbl_Errori.Text = ""
        If Condizione = "" Then
            ClientScript.RegisterClientScriptBlock(Me.GetType(), "Errore", "VisualizzaErrore('Errore nessuna condizione indicata');", True)
            REM Lbl_Errori.Text = "Errore nessuna condizione indicata"
            Exit Sub
        End If

        If Val(Txt_NumeroAl.Text) > 0 Then
            If Tipo = 2 Then
                MySql = MySql & Condizione & " Order By NumeroRegistrazione,DataRegistrazione,CausaleContabile, RigaDaCausale "
            Else
                MySql = MySql & Condizione & " Order By NumeroRegistrazione,DataRegistrazione,CausaleContabile, RigaDaCausale,MastroPartita,ContoPartita,SottocontoPartita "
            End If
        Else
            If Tipo = 2 Then
                MySql = MySql & Condizione & " Order By DataRegistrazione,NumeroRegistrazione,CausaleContabile, RigaDaCausale "
            Else
                MySql = MySql & Condizione & " Order By DataRegistrazione,NumeroRegistrazione,CausaleContabile, RigaDaCausale,MastroPartita,ContoPartita,SottocontoPartita "
            End If

        End If


        Dim cmd As New OleDbCommand()

        cmd.CommandText = (MySql)
        If IsDate(Txt_DataDal.Text) Then
            Dim DataDal As Date = Txt_DataDal.Text
            cmd.Parameters.AddWithValue("@DataDal", DataDal)
        End If

        If IsDate(Txt_DataAl.Text) Then
            Dim DataAl As Date = Txt_DataAl.Text
            cmd.Parameters.AddWithValue("@DataAl", DataAl)
        End If

        If IsDate(Txt_DataBolletta.Text) Then
            Dim DataBolletta As Date = Txt_DataBolletta.Text
            cmd.Parameters.AddWithValue("@DataBolletta", DataBolletta)
        End If

        If Txt_Importo.Text <> "" Then
            If CDbl(Txt_Importo.Text) > 0 Then
                cmd.Parameters.AddWithValue("@Importo", CDbl(Txt_Importo.Text))
            End If
        End If
        If Val(Txt_NumeroDal.Text) > 0 Then
            cmd.Parameters.AddWithValue("@NumeroRegistrazione", Val(Txt_NumeroDal.Text))
        End If
        If Val(Txt_NumeroAl.Text) > 0 Then
            cmd.Parameters.AddWithValue("@NumeroRegistrazione", Val(Txt_NumeroAl.Text))
        End If
        If Txt_NumeroDocumento.Text <> "" Then
            cmd.Parameters.AddWithValue("@NumeroDocumento", Txt_NumeroDocumento.Text)
        End If

        If DD_IVA.SelectedValue <> "" Then
            cmd.Parameters.AddWithValue("@IVA", DD_IVA.SelectedValue)
        End If



        If DD_Detraibilita.SelectedValue <> "" And DD_Detraibilita.SelectedValue <> "Null" Then
            cmd.Parameters.AddWithValue("@Detraibile", DD_Detraibilita.SelectedValue)
        End If


        If DD_CSERV.SelectedValue <> "" Then
            cmd.Parameters.AddWithValue("@CentroServizio", DD_CSERV.SelectedValue)
        End If

        cmd.Connection = cn


        Dim sb As StringBuilder = New StringBuilder


        Tabella.Clear()
        Tabella.Columns.Clear()
        Tabella.Columns.Add("Numero", GetType(String))
        Tabella.Columns.Add("Dt.Reg.", GetType(String))
        Tabella.Columns.Add("N.Doc.", GetType(String))
        Tabella.Columns.Add("Dt.Doc.", GetType(String))

        Tabella.Columns.Add("N.Protoc.", GetType(Long))
        Tabella.Columns.Add("Anno Protoc.", GetType(Long))
        Tabella.Columns.Add("Causale", GetType(String))
        Tabella.Columns.Add("Descrizione", GetType(String))
        Tabella.Columns.Add("Dare", GetType(String))
        Tabella.Columns.Add("Avere", GetType(String))
        Tabella.Columns.Add("Saldo", GetType(String))
        Tabella.Columns.Add("Riga", GetType(Integer))

        'If Trim(Txt_Sottoconto.Text) = "" Then
        Tabella.Columns.Add("Partita", GetType(String))
        'End If

        Tabella.Columns.Add("Contro Partita", GetType(String))
        Tabella.Columns.Add("Imponibile", GetType(Double))
        Tabella.Columns.Add("Ritenuta", GetType(String))

        If Chk_DatiOspite.Checked = True Then
            Tabella.Columns.Add("StatoAuto", GetType(String))
            Tabella.Columns.Add("TipoRetta", GetType(String))
            Tabella.Columns.Add("Nome Ospite", GetType(String))
        End If

        If Chk_InserisicSaldo.Checked = True Then
            If Math.Abs(Saldo) > 0 Then
                Dim myriga As System.Data.DataRow = Tabella.NewRow()
                myriga(10) = Format(Saldo, "#,##0.00")
                Tabella.Rows.Add(myriga)
            End If
        Else
            Saldo = 0
        End If

        Dim oldCodiceServizio As String
        Dim OldCodiceOspite As Long
        oldCodiceServizio = ""
        OldCodiceOspite = 0

        Dim TotDare As Double = 0
        Dim TotAvere As Double = 0

        Dim myPOSTreader As OleDbDataReader = cmd.ExecuteReader()
        Do While myPOSTreader.Read


            Dim myriga As System.Data.DataRow = Tabella.NewRow()
            myriga(0) = myPOSTreader.Item("NumeroRegistrazione")
            If IsDBNull(myPOSTreader.Item("DataRegistrazione")) Then
                myriga(1) = ""
            Else
                myriga(1) = Format(myPOSTreader.Item("DataRegistrazione"), "dd/MM/yyyy")
            End If
            myriga(2) = myPOSTreader.Item("NumeroDocumento")
            If IsDBNull(myPOSTreader.Item("DataDocumento")) Then
                myriga(3) = ""
            Else
                myriga(3) = Format(myPOSTreader.Item("DataDocumento"), "dd/MM/yyyy")
            End If
            myriga(4) = myPOSTreader.Item("NumeroProtocollo")
            myriga(5) = myPOSTreader.Item("AnnoProtocollo")

            Dim dC As New Cls_CausaleContabile

            dC.Leggi(Session("DC_TABELLE"), campodb(myPOSTreader.Item("CausaleContabile")))
            myriga(6) = dC.Descrizione

            myriga(7) = campodb(myPOSTreader.Item("Descrizione"))
            'DescrizioneT
            If myriga(7) <> campodb(myPOSTreader.Item("DescrizioneT")) Then
                myriga(7) = myriga(7) & " " & campodb(myPOSTreader.Item("DescrizioneT"))
            End If


            If campodb(myPOSTreader.Item("DareAvere")) = "D" Then
                myriga(8) = Format(campodbn(myPOSTreader.Item("Importo")), "#,##0.00")
                TotDare = TotDare + campodbn(myPOSTreader.Item("Importo"))
                myriga(9) = 0
                Saldo = Saldo + campodbn(myPOSTreader.Item("Importo"))
            Else
                myriga(8) = 0
                myriga(9) = Format(campodbn(myPOSTreader.Item("Importo")), "#,##0.00")
                Saldo = Saldo - campodbn(myPOSTreader.Item("Importo"))
                TotAvere = TotAvere + campodbn(myPOSTreader.Item("Importo"))
            End If



            myriga(10) = Format(Saldo, "#,##0.00")
            myriga(11) = myPOSTreader.Item("RigaDaCausale")

            'If Trim(Txt_Sottoconto.Text) = "" Then
            Dim xs As New Cls_Pianodeiconti
            xs.Mastro = Val(campodb(myPOSTreader.Item("Mastropartita")))
            xs.Conto = Val(campodb(myPOSTreader.Item("Contopartita")))
            xs.Sottoconto = Val(campodb(myPOSTreader.Item("Sottocontopartita")))
            xs.Decodfica(Session("DC_GENERALE"))
            myriga(12) = campodb(xs.Descrizione)
            'End If

            Dim xs1 As New Cls_Pianodeiconti
            xs1.Mastro = Val(campodb(myPOSTreader.Item("MastroContropartita")))
            xs1.Conto = Val(campodb(myPOSTreader.Item("ContoContropartita")))
            xs1.Sottoconto = Val(campodb(myPOSTreader.Item("SottocontoContropartita")))
            xs1.Decodfica(Session("DC_GENERALE"))
            'If Trim(Txt_Sottoconto.Text) = "" Then
            myriga(13) = campodb(xs1.Descrizione)
            myriga(14) = myPOSTreader.Item("Imponibile")
            myriga(15) = campodb(myPOSTreader.Item("CodiceRitenuta"))
            'Else
            'myriga(11) = campodb(xs1.Descrizione)
            'myriga(12) = myPOSTreader.Item("Imponibile")
            'myriga(13) = campodb(myPOSTreader.Item("CodiceRitenuta"))
            'End If
            If Chk_DatiOspite.Checked = True Then
                Dim Registrazione As New Cls_MovimentoContabile

                Registrazione.Leggi(Session("DC_GENERALE"), campodbn(myPOSTreader.Item("NumeroRegistrazione")))
                If Registrazione.Righe(0).RigaDaCausale = 1 Then
                    If Registrazione.CentroServizio <> "" Then
                        Dim M As New Cls_StatoAuto

                        M.CODICEOSPITE = Int(Registrazione.Righe(0).SottocontoPartita / 100)
                        M.CENTROSERVIZIO = Registrazione.CentroServizio
                        M.Data = campodb(myPOSTreader.Item("DataRegistrazione"))
                        M.StatoPrimaData(Session("DC_OSPITE"), M.CODICEOSPITE, M.CENTROSERVIZIO)

                        If campodb(M.TipoRetta) <> "" Then
                            Dim MTipoRetta As New Cls_TabellaTipoImportoRegione

                            MTipoRetta.Codice = M.TipoRetta
                            MTipoRetta.Leggi(Session("DC_OSPITE"), MTipoRetta.Codice)
                            myriga(16) = MTipoRetta.Descrizione
                        End If

                        Dim Tot As New Cls_rettatotale

                        Tot.CODICEOSPITE = M.CODICEOSPITE
                        Tot.CENTROSERVIZIO = M.CENTROSERVIZIO
                        Tot.Data = campodb(myPOSTreader.Item("DataRegistrazione"))
                        Tot.RettaTotaleAData(Session("DC_OSPITE"), Tot.CODICEOSPITE, Tot.CENTROSERVIZIO, Tot.Data)
                        If campodb(Tot.TipoRetta) <> "" Then
                            Dim MTipoRetta As New Cls_TipoRetta

                            MTipoRetta.Codice = Tot.TipoRetta
                            MTipoRetta.Leggi(Session("DC_OSPITE"), MTipoRetta.Codice)
                            myriga(17) = MTipoRetta.Descrizione
                        End If


                        Dim NomeOspite As New ClsOspite

                        NomeOspite.CodiceOspite = M.CODICEOSPITE
                        NomeOspite.Leggi(Session("DC_OSPITE"), NomeOspite.CodiceOspite)

                        myriga(18) = NomeOspite.Nome
                    End If
                End If
            End If
            Tabella.Rows.Add(myriga)

        Loop
        myPOSTreader.Close()
        cn.Close()

        Dim myrigaT As System.Data.DataRow = Tabella.NewRow()
        myrigaT(8) = Format(TotDare, "#,##0.00")
        myrigaT(9) = Format(TotAvere, "#,##0.00")
        Tabella.Rows.Add(myrigaT)

        Dim X As Integer
        Dim Y As Integer

        For X = 0 To Chk_Campi.Items.Count - 1
            For Y = Tabella.Columns.Count - 1 To 1 Step -1
                If Chk_Campi.Items(X).Text = Tabella.Columns(Y).ColumnName Then
                    If Chk_Campi.Items(X).Selected = False Then
                        Tabella.Columns.Remove(Tabella.Columns(Y).ColumnName)
                    End If
                End If
            Next
        Next X



        If ToExcel = True Then
            Session("AppoggioExcel") = Tabella
        Else
            Grid.AutoGenerateColumns = True
            Grid.DataSource = Tabella
            Grid.DataBind()
        End If




        Session("RicercaGenerale") = Tabella
    End Sub
    Function campodb(ByVal oggetto As Object) As String
        If IsDBNull(oggetto) Then
            Return ""
        Else
            Return oggetto
        End If
    End Function

    Function campodbn(ByVal oggetto As Object) As Double
        If IsDBNull(oggetto) Then
            Return 0
        Else
            Return oggetto
        End If
    End Function
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Trim(Session("UTENTE")) = "" Then
            If Request.ServerVariables("URL").ToUpper.IndexOf("SENIORWEBBLANCO") > 0 Then
                Response.Redirect("/ODV/Login.aspx")
                Exit Sub
            Else
                Response.Redirect("/SeniorWeb/Login.aspx")
                Exit Sub
            End If
        End If


        If Page.IsPostBack = True Then Exit Sub

        Dim K1 As New Cls_SqlString

        Dim Barra As New Cls_BarraSenior

        If Not IsNothing(Session("RicercaAnagraficaSQLString")) Then
            Try
                K1 = Session("RicercaAnagraficaSQLString")
            Catch ex As Exception

            End Try
        End If

        Lbl_BarraSenior.Text = Barra.CodiceBarra(Application("SENIOR"), Session("UTENTE"), Page, K1)


        Dim M As New Cls_CentroServizio


        M.UpDateDropBox(Session("DC_OSPITE"), DD_CSERV)



        Dim k As New Cls_CausaleContabile

        Dim ConnectionString As String = Session("DC_TABELLE")

        k.UpDateDropBox(ConnectionString, DD_CausaleContabile)

        k.UpDateDropBox(ConnectionString, DD_CausaleContabile1)
        k.UpDateDropBox(ConnectionString, DD_CausaleContabile2)
        k.UpDateDropBox(ConnectionString, DD_CausaleContabile3)

        Dim Iva As New Cls_IVA

        Iva.UpDateDropBox(ConnectionString, DD_IVA)


        Dim Detraibilita As New ClsDetraibilita

        Detraibilita.UpDateDropBox(ConnectionString, DD_Detraibilita)


        DD_Detraibilita.Items.Add("Non Indicato")
        DD_Detraibilita.Items(DD_Detraibilita.Items.Count - 1).Value = "Null"


        DD_Detraibilita.Items.Add("Tutti")
        DD_Detraibilita.Items(DD_Detraibilita.Items.Count - 1).Value = ""

        Dim x As New Cls_RegistroIVA

        x.UpDateDropBox(ConnectionString, DD_RegistroIVA)


        'Dim I As Integer
        'For I = 0 To Chk_Campi.Items.Count - 1
        'Chk_Campi.Items(I).Selected = True
        'Next

        Txt_DataDal.Text = Format(DateSerial(Year(Now), 1, 1), "dd/MM/yyyy")
        Txt_DataAl.Text = Format(DateSerial(Year(Now), 12, 31), "dd/MM/yyyy")

        If Val(Request.Item("MASTRO")) > 0 Then
            Dim MCsec As New Cls_Pianodeiconti

            MCsec.Mastro = Val(Request.Item("MASTRO"))
            MCsec.Conto = Val(Request.Item("CONTO"))
            MCsec.Sottoconto = Val(Request.Item("SOTTOCONTO"))
            MCsec.Decodfica(Session("DC_GENERALE"))


            Txt_Sottoconto.Text = MCsec.Mastro & " " & MCsec.Conto & " " & MCsec.Sottoconto & " " & MCsec.Descrizione
        End If

        ViewState("XLINEA") = -1
        ViewState("XBACKUP") = 0
        EseguiJS()
    End Sub

    Protected Sub Grid_RowCommand(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewCommandEventArgs) Handles Grid.RowCommand

        If e.CommandName = "Seleziona" Then
            Dim ConnectionString As String = Session("DC_OSPITE")
            Dim d As Integer

            If ViewState("XLINEA") <> -1 Then
                Grid.Rows(ViewState("XLINEA")).BackColor = ViewState("XBACKUP")
            End If


            Tabella = Session("RicercaGenerale")
            d = Val(e.CommandArgument)
            Session("NumeroRegistrazione") = Tabella.Rows(d).Item(0).ToString



            ViewState("XLINEA") = d
            ViewState("XBACKUP") = Grid.Rows(d).BackColor
            Grid.Rows(d).BackColor = Drawing.Color.Violet

            If Request.Item("TIPO") = "PRNT" Then
                Response.Redirect("primanota.aspx")
            End If
            If Request.Item("TIPO") = "DOC" Then
                Response.Redirect("documenti.aspx")
            End If
            If Request.Item("TIPO") = "INC" Then
                Response.Redirect("incassipagamenti.aspx")
            End If
            If Request.Item("TIPO") = "INCS" Then
                Response.Redirect("incassipagamenti.aspx?TIPO=SCADENZARIO")
            End If

            EseguiJS()
        End If

        If (e.CommandName = "Richiama") Then
            Dim Registrazione As Long
            Registrazione = Val(e.CommandArgument)

            Dim Reg As New Cls_MovimentoContabile

            Reg.Leggi(Session("DC_GENERALE"), Registrazione)
            Dim CauCon As New Cls_CausaleContabile

            CauCon.Leggi(Session("DC_TABELLE"), Reg.CausaleContabile)
            If CauCon.Tipo = "I" Or CauCon.Tipo = "R" Then
                Session("NumeroRegistrazione") = Registrazione
                ClientScript.RegisterClientScriptBlock(Me.GetType(), "MyBox", "$(window).load(function() {  DialogBox('../GeneraleWeb/Documenti.aspx?CHIAMATAESTERNA=1');  });", True)
                REM Response.Redirect("../GeneraleWeb/Documenti.aspx")
                EseguiJS()
                Exit Sub
            End If
            If CauCon.Tipo = "P" Then
                Session("NumeroRegistrazione") = Registrazione
                Dim DatiGenerali As New Cls_DatiGenerali

                DatiGenerali.LeggiDati(Session("DC_TABELLE"))


                If DatiGenerali.ScadenziarioCheckChiuso = 1 Then
                    ClientScript.RegisterClientScriptBlock(Me.GetType(), "MyBox", "$(window).load(function() {  DialogBox('../GeneraleWeb/incassipagamenti.aspx?TIPO=SCADENZARIO&CHIAMATAESTERNA=1');  });", True)
                Else
                    ClientScript.RegisterClientScriptBlock(Me.GetType(), "MyBox", "$(window).load(function() {  DialogBox('../GeneraleWeb/incassipagamenti.aspx?CHIAMATAESTERNA=1');  });", True)
                End If
                REM Response.Redirect("../GeneraleWeb/incassipagamenti.aspx")

                EseguiJS()
                Exit Sub
            End If
            Session("NumeroRegistrazione") = Registrazione
            ClientScript.RegisterClientScriptBlock(Me.GetType(), "MyBox", "$(window).load(function() {  DialogBox('../GeneraleWeb/primanota.aspx?CHIAMATAESTERNA=1');  });", True)
            REM Response.Redirect("../GeneraleWeb/primanota.aspx")
            EseguiJS()
        End If

    End Sub


    Private Function IncassoConScadenze(ByVal NumeroRegistrazione As Integer) As Boolean
        IncassoConScadenze = False
        Dim cn As OleDbConnection

        cn = New Data.OleDb.OleDbConnection(Session("DC_GENERALE"))

        cn.Open()
        Dim cmd As New OleDbCommand()



        cmd.CommandText = ("Select * FRom  Scadenzario   where " & _
                             " RegistrazioneIncasso = ?")
        cmd.Parameters.AddWithValue("@RegistrazioneIncasso", NumeroRegistrazione)

        cmd.Connection = cn
        Dim VerReader As OleDbDataReader = cmd.ExecuteReader()
        If VerReader.Read Then
            IncassoConScadenze = True
        End If
        VerReader.Close()
        cn.Close()
    End Function
    Private Function SplitWords(ByVal s As String) As String()
        '
        ' Call Regex.Split function from the imported namespace.
        ' Return the result array.
        '
        Return Regex.Split(s, "\W+")
    End Function
    Protected Sub form1_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles form1.Load

    End Sub

   
    Protected Sub ImageButton1_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles ImageButton1.Click
        If Txt_DataAl.Text <> "" Then
            If Not IsDate(Txt_DataAl.Text) Then
                REM ClientScript.RegisterClientScriptBlock(Me.GetType(), "Errore", "VisualizzaErrore('Errore data al formalmente errata');", True)
                ScriptManager.RegisterStartupScript(Me, Me.GetType(), "visualizzaA", "VisualizzaErrore('Errore data al formalmente errata');", True)
                Exit Sub
            End If
        End If
        If Txt_DataDal.Text <> "" Then
            If Not IsDate(Txt_DataDal.Text) Then
                REM ClientScript.RegisterClientScriptBlock(Me.GetType(), "Errore", "VisualizzaErrore('Errore data dal formalmente errata');", True)
                ScriptManager.RegisterStartupScript(Me, Me.GetType(), "visualizzaA", "VisualizzaErrore('Errore data dal formalmente errata');", True)
                Exit Sub
            End If
        End If
        If Txt_DataBolletta.Text <> "" Then
            If Not IsDate(Txt_DataBolletta.Text) Then
                REM ClientScript.RegisterClientScriptBlock(Me.GetType(), "Errore", "VisualizzaErrore('Errore data bolletta formalmente errata');", True)
                ScriptManager.RegisterStartupScript(Me, Me.GetType(), "visualizzaA", "VisualizzaErrore('Errore data bolletta formalmente errata');", True)
                Exit Sub
            End If
        End If

        If Chk_BolloVirtuale.Checked = True Then
            Dim DataDal As Date

            DataDal = Txt_DataDal.Text
            If Format(DataDal, "yyyyMMdd") < "20190501" Then
                ScriptManager.RegisterStartupScript(Me, Me.GetType(), "visualizzaA", "VisualizzaErrore('Estrazioni bollo virtuale devono essere posteriori al 01/05/2019');", True)
                Exit Sub
            End If
        End If

        Call Esegui_Ricerca(False)
        EseguiJS()
    End Sub

    Protected Sub ImageButton2_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles ImageButton2.Click
        If Txt_DataAl.Text <> "" Then
            If Not IsDate(Txt_DataAl.Text) Then
                ClientScript.RegisterClientScriptBlock(Me.GetType(), "Errore", "VisualizzaErrore('Errore data al formalmente errata');", True)
                Exit Sub
            End If
        End If
        If Txt_DataDal.Text <> "" Then
            If Not IsDate(Txt_DataDal.Text) Then
                ClientScript.RegisterClientScriptBlock(Me.GetType(), "Errore", "VisualizzaErrore('Errore data dal formalmente errata');", True)
                Exit Sub
            End If
        End If
        If Txt_DataBolletta.Text <> "" Then
            If Not IsDate(Txt_DataBolletta.Text) Then
                ClientScript.RegisterClientScriptBlock(Me.GetType(), "Errore", "VisualizzaErrore('Errore data bolletta formalmente errata');", True)
                Exit Sub
            End If
        End If
        If Chk_BolloVirtuale.Checked = True Then
            Dim DataDal As Date

            DataDal = Txt_DataDal.Text
            If Format(DataDal, "yyyyMMdd") < "20190501" Then
                ScriptManager.RegisterStartupScript(Me, Me.GetType(), "visualizzaA", "VisualizzaErrore('Estrazioni bollo virtuale devono essere posteriori al 01/05/2019');", True)
                Exit Sub
            End If
        End If

        Call Esegui_Ricerca(True)

        Response.Redirect("RicercaExcel.aspx")
    End Sub

    Private Sub EseguiJS()
        Dim MyJs As String
        MyJs = "$(document).ready(function() {"

        MyJs = MyJs & "var els = document.getElementsByTagName(""*"");"

        MyJs = MyJs & "for (var i=0;i<els.length;i++)"
        MyJs = MyJs & "if ( els[i].id ) { "
        MyJs = MyJs & " var appoggio =els[i].id; "
        MyJs = MyJs & " if (appoggio.match('Txt_Sottoconto')!= null) {  "
        MyJs = MyJs & " $(els[i]).autocomplete('/WebHandler/GestioneConto.ashx?Utente=" & Session("UTENTE") & "', {delay:5,minChars:3});"
        MyJs = MyJs & "    }"

        MyJs = MyJs & " if (appoggio.match('Txt_Data')!= null) {  "
        MyJs = MyJs & " $(els[i]).mask(""99/99/9999"");  "
        MyJs = MyJs & " $(els[i]).datepicker({ changeMonth: true,changeYear: true,  yearRange: ""1890:" & Year(Now) + 1 & """},$.datepicker.regional[""it""]);"
        MyJs = MyJs & "    }"

        MyJs = MyJs & " if (appoggio.match('Txt_Importo')!= null)  {  "
        MyJs = MyJs & " $(els[i]).keypress(function() { ForceNumericInput($(this).val(), true, true); } ); "
        MyJs = MyJs & " $(els[i]).blur(function() { var ap = formatNumber($(this).val(),2); $(this).val(ap); });"
        MyJs = MyJs & "    }"

        MyJs = MyJs & "$("".chosen-select"").chosen({"
        MyJs = MyJs & "no_results_text: ""Causale non trovata"","
        MyJs = MyJs & "display_disabled_options: true,"
        'MyJs = MyJs & "allow_single_deselect: true,"
        MyJs = MyJs & "width: ""350px"","
        MyJs = MyJs & "placeholder_text_single: ""Seleziona Causale"""
        MyJs = MyJs & "});"


        MyJs = MyJs & "} "
        MyJs = MyJs & "});"

        'MyJs = "$(document).ready(function() { $('.myClass').keypress(function() { handleEnter($('.myClass').val(), true, true); } );     $('#" & Txt_ImportoPagato.ClientID & "').blur(function() { var ap = formatNumber ($('#" & Txt_ImportoPagato.ClientID & "').val(),2); $('#" & Txt_ImportoPagato.ClientID & "').val(ap); }); });"
        ScriptManager.RegisterStartupScript(Me, Me.GetType(), "visualizzaRitIm", MyJs, True)
    End Sub


    Protected Sub Grid_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles Grid.SelectedIndexChanged

    End Sub

    Protected Sub Btn_Esci_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles Btn_Esci.Click
        If Request.Item("TIPO") = "MENU" Then
            Response.Redirect("Menu_Generale.aspx")
            Exit Sub
        End If

        Response.Redirect("UltimiMovimenti.aspx?TIPO=" & Request.Item("TIPO"))
    End Sub

    Protected Sub Btn_Piu1_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles Btn_Piu1.Click
        DD_CausaleContabile1.Visible = True
        Btn_Piu2.Visible = True
        Btn_Piu1.Visible = False
    End Sub

    Protected Sub Btn_Piu2_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles Btn_Piu2.Click
        DD_CausaleContabile2.Visible = True
        Btn_Piu3.Visible = True
        Btn_Piu2.Visible = False
    End Sub

    Protected Sub Btn_Piu3_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles Btn_Piu3.Click
        DD_CausaleContabile3.Visible = True
        Btn_Piu2.Visible = True
        Btn_Piu3.Visible = False
    End Sub

    Protected Overrides Sub Finalize()
        MyBase.Finalize()
    End Sub
End Class
