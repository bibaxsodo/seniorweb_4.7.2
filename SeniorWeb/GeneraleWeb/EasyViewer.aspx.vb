﻿Imports Microsoft.VisualBasic
Imports System.Data.OleDb
Imports System.Web.Hosting

Partial Class GeneraleWeb_EasyViewer
    Inherits System.Web.UI.Page

    Private Sub PianoConti()
        Label1.Text = "<table style=""width: 100%""><tr><td><img  width=""11"" height=""11""  src=""images/pallinorosso.jpg"">Piano Dei Conti<a  style=""border-width:0;"" href=""easyviewer.aspx?Tipo=Clifor""><img  style=""border-width:0;"" src=""images/pallinoblu.gif"" title=""Clienti/Fornitori""></a><a  style=""border-width:0;"" href=""easyviewer.aspx?Tipo=Causali""><img style=""border-width:0;""  src=""images\pallinoverde.png"" title=""Causali Contabili""></a>" & "</td><td align=""right""><a href=""#""><img src=""images/font_piu.jpg"" width=""11"" height=""11"" onclick=""piugrande();"" ></a></td></tr></table>"



        Dim cn As OleDbConnection


        
        cn = New Data.OleDb.OleDbConnection(Session("DC_GENERALE"))

        cn.Open()
        Dim cmd As New OleDbCommand()
        cmd.CommandText = ("select * from PianoConti WHERE TipoAnagrafica = '' OR TipoAnagrafica IS NULL Order by  " & _
                           "Mastro ,Conto , SottoConto ")

   
        cmd.Connection = cn

        Dim Griglia As String

        Griglia = "<div id=""caratteri"" style=""font-size: xx-small;""><table style=""width: 100%"">"

        Dim myPOSTreader As OleDbDataReader = cmd.ExecuteReader()
        Do While myPOSTreader.Read
            Griglia = Griglia & "<tr>"
            Griglia = Griglia & "<td><span>" & myPOSTreader.Item("Mastro") & "</span></td>"
            Griglia = Griglia & "<td><span>" & myPOSTreader.Item("Conto") & "</span></td>"
            Griglia = Griglia & "<td><span>" & campodbN(myPOSTreader.Item("SottoConto")) & "</span></td>"
            Griglia = Griglia & "<td><span>" & campodb(myPOSTreader.Item("Descrizione")) & "</span></td>"
            Griglia = Griglia & "</tr>"
        Loop
        myPOSTreader.Close()
        cn.Close()
        Griglia = Griglia & "</table>"
        Label1.Text = Label1.Text & Griglia
    End Sub

    Function campodb(ByVal oggetto As Object) As String
        If IsDBNull(oggetto) Then
            Return ""
        Else
            Return oggetto
        End If
    End Function

    Function campodbN(ByVal oggetto As Object) As Long
        If IsDBNull(oggetto) Then
            Return 0
        Else
            Return oggetto
        End If
    End Function

    Private Sub CausaliContabili()
        Label1.Text = "<table style=""width: 100%""><tr><td><a  style=""border-width:0;"" href=""easyviewer.aspx?Tipo=PianoConti""><img width=""11"" height=""11"" style=""border-width:0;""  src=""images/pallinorosso.jpg""></a><a  style=""border-width:0;"" href=""easyviewer.aspx?Tipo=Clifor""><img  style=""border-width:0;"" src=""images/pallinoblu.gif"" title=""Clienti/Fornitori""></a><img style=""border-width:0;""  src=""images\pallinoverde.png"" title=""Causali Contabili"">Causali Contabili" & "</td><td align=""right""><a href=""#""><img src=""images/font_piu.jpg"" width=""11"" height=""11"" onclick=""piugrande();"" ></a></td></tr></table>"


        Dim cn As OleDbConnection




        cn = New Data.OleDb.OleDbConnection(Session("DC_TABELLE"))

        cn.Open()
        Dim cmd As New OleDbCommand()
        cmd.CommandText = ("select * from CausaliContabiliTesta  Order by  " & _
                           "Codice ")


        cmd.Connection = cn
        Dim Griglia As String

        Griglia = "<div id=""caratteri"" style=""font-size: xx-small;""><table style=""width: 100%"">"
        Dim myPOSTreader As OleDbDataReader = cmd.ExecuteReader()
        Do While myPOSTreader.Read
            Griglia = Griglia & "<tr>"
            Griglia = Griglia & "<td><span>" & campodb(myPOSTreader.Item("Codice")) & "</span></td>"
            Griglia = Griglia & "<td><span>" & campodb(myPOSTreader.Item("Descrizione")) & "</span></td>"
            Griglia = Griglia & "</tr>"
        Loop
        myPOSTreader.Close()
        cn.Close()
        Griglia = Griglia & "</table></div>"

        Label1.Text = Label1.Text & Griglia
    End Sub

    Private Sub ClientiFornitori()
        Label1.Text = "<table style=""width: 100%""><tr><td><a  style=""border-width:0;"" href=""easyviewer.aspx?Tipo=PianoConti""><img width=""11"" height=""11"" style=""border-width:0;""  src=""images/pallinorosso.jpg""></a><img  style=""border-width:0;"" src=""images/pallinoblu.gif"" title=""Clienti/Fornitori"">Clienti/Fornitori<a  style=""border-width:0;"" href=""easyviewer.aspx?Tipo=Causali""><img style=""border-width:0;""  src=""images\pallinoverde.png"" title=""Causali Contabili""></a>" & "</td><td align=""right""><a href=""#""><img src=""images/font_piu.jpg"" width=""11"" height=""11"" onclick=""piugrande();"" ></a></td></tr></table>"


        Dim cn As OleDbConnection



        cn = New Data.OleDb.OleDbConnection(Session("DC_OSPITE"))

        cn.Open()
        Dim cmd As New OleDbCommand()
        cmd.CommandText = ("select * from AnagraficaComune  Order by  " & _
                           "Nome ")


        cmd.Connection = cn
        Dim Griglia As String

        Griglia = "<div id=""caratteri"" style=""font-size: xx-small;""><table style=""width: 100%"">"
        Dim myPOSTreader As OleDbDataReader = cmd.ExecuteReader()
        Do While myPOSTreader.Read
            If campodbN(myPOSTreader.Item("MastroCliente")) > 0 Then
                Griglia = Griglia & "<tr>"
                Griglia = Griglia & "<td><span>" & campodbN(myPOSTreader.Item("MastroCliente")) & "</span></td>"
                Griglia = Griglia & "<td><span>" & campodbN(myPOSTreader.Item("ContoCliente")) & "</span></td>"
                Griglia = Griglia & "<td><span>" & campodbN(myPOSTreader.Item("SottoContoCliente")) & "</span></td>"
                Griglia = Griglia & "<td><span>" & campodb(myPOSTreader.Item("Nome")) & "</span></td>"
                Griglia = Griglia & "</tr>"
            End If
            If campodbN(myPOSTreader.Item("MastroFornitore")) > 0 Then
                Griglia = Griglia & "<tr>"
                Griglia = Griglia & "<td><span>" & campodbN(myPOSTreader.Item("MastroFornitore")) & "</span></td>"
                Griglia = Griglia & "<td><span>" & campodbN(myPOSTreader.Item("ContoFornitore")) & "</span></td>"
                Griglia = Griglia & "<td><span>" & campodbN(myPOSTreader.Item("SottoContoFornitore")) & "</span></td>"
                Griglia = Griglia & "<td><span>" & campodb(myPOSTreader.Item("Nome")) & "</span></td>"
                Griglia = Griglia & "</tr>"
            End If
        Loop
        myPOSTreader.Close()
        cn.Close()
        Griglia = Griglia & "</table>"
        Label1.Text = Label1.Text & Griglia
    End Sub

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Trim(Session("UTENTE")) = "" Then
            Response.Redirect("../Login_generale.aspx")
            Exit Sub
        End If

        If Request.Item("Tipo") = "Clifor" Then
            Call ClientiFornitori()
            Exit Sub
        End If
        If Request.Item("Tipo") = "Causali" Then
            Call CausaliContabili()
            Exit Sub
        End If
        PianoConti()
    End Sub
End Class
