﻿Imports System.Data.OleDb
Imports System.Web.Hosting

Partial Class GeneraleWeb_InserimentoBudget
    Inherits System.Web.UI.Page
    Function campodb(ByVal oggetto As Object) As String
        If IsDBNull(oggetto) Then
            Return ""
        Else
            Return oggetto
        End If
    End Function
    Function campodbN(ByVal oggetto As Object) As Double
        If IsDBNull(oggetto) Then
            Return 0
        Else
            Return oggetto
        End If
    End Function
    Private Sub PienaGriglia()
        Dim ConnectionString As String = Session("DC_GENERALE")
        Dim Tabella As New System.Data.DataTable("tabella")
        Dim MyDataSet As New System.Data.DataSet()
        Dim Spaziatura As String
        Dim Indice As Long = 0
        Dim Livello1(1000) As Long
        Dim Livello2(1000) As Long
        Dim Livello3(1000) As Long
        Dim Colonna(100) As Long
        Dim xC As Long = 1
        Dim Cella(1000, 20) As String

        Dim cn As OleDbConnection

        cn = New Data.OleDb.OleDbConnection(ConnectionString)


        cn.Open()



        Tabella.Clear()
        Tabella.Columns.Clear()
        Tabella.Columns.Add("Conto", GetType(String))
        Tabella.Columns.Add("Decodifica", GetType(String))
        Tabella.Columns.Add("Non Asseganto", GetType(String))


        Dim CommandColonne As New OleDbCommand("Select * From ColonneBudget Where Anno = ? Order By Livello1", cn)

        CommandColonne.Parameters.Add("@Anno", OleDbType.Integer, 8)
        CommandColonne.Parameters("@Anno").Value = Txt_Anno.Text
        Dim myColonne As OleDbDataReader = CommandColonne.ExecuteReader()

        Do While myColonne.Read()
            Tabella.Columns.Add(myColonne.Item("Descrizione"), GetType(String))
            Colonna(xC) = myColonne.Item("Livello1")
            xC = xC + 1
        Loop
        myColonne.Close()


        Dim CommandRighe As New OleDbCommand("Select * From TipoBudget Where Anno = ? Order by Livello1,Livello2,Livello3 ", cn)

        CommandRighe.Parameters.Add("@Anno", OleDbType.Integer, 8)
        CommandRighe.Parameters("@Anno").Value = Txt_Anno.Text
        Dim myRighe As OleDbDataReader = CommandRighe.ExecuteReader()        
        Do While myRighe.Read()

            Spaziatura = ""
            If campodbN(myRighe.Item("Livello1")) > 0 And campodbN(myRighe.Item("Livello2")) = 0 And campodbN(myRighe.Item("Livello3")) = 0 Then
                Spaziatura = ""
            End If
            If campodbN(myRighe.Item("Livello1")) > 0 And campodbN(myRighe.Item("Livello2")) > 0 And campodbN(myRighe.Item("Livello3")) = 0 Then
                Spaziatura = ""
            End If
            If campodbN(myRighe.Item("Livello1")) > 0 And campodbN(myRighe.Item("Livello2")) > 0 And campodbN(myRighe.Item("Livello3")) > 0 Then
                Spaziatura = ""
            End If


            Livello1(Indice) = campodbN(myRighe.Item("Livello1"))
            Livello2(Indice) = campodbN(myRighe.Item("Livello2"))
            Livello3(Indice) = campodbN(myRighe.Item("Livello3"))


            Dim myriga As System.Data.DataRow = Tabella.NewRow()
            myriga(0) = campodb(myRighe.Item("Livello1")) & "-" & campodb(myRighe.Item("Livello2")) & "-" & campodb(myRighe.Item("Livello3"))
            myriga(1) = Spaziatura & myRighe.Item("Descrizione")

            For Y = 2 To Tabella.Columns.Count - 1
                myriga(Y) = "0,00"
            Next

            Dim CommandBudget As New OleDbCommand("Select * From Budget Where Anno = ? And Livello1 = ? And Livello2 = ? And Livello3 = ? And Colonna = ? ", cn)

            CommandBudget.Parameters.AddWithValue("@Anno", Txt_Anno.Text)
            CommandBudget.Parameters.AddWithValue("@Livello1", myRighe.Item("Livello1"))
            CommandBudget.Parameters.AddWithValue("@Livello2", myRighe.Item("Livello2"))
            CommandBudget.Parameters.AddWithValue("@Livello3", myRighe.Item("Livello3"))
            CommandBudget.Parameters.AddWithValue("@Colonna", 0)


            Dim myBudget As OleDbDataReader = CommandBudget.ExecuteReader()
            If myBudget.Read() Then  
                If campodbN(myBudget.Item("Importo")) <> 0 Then
                    myriga(2) = Format(campodbN(myBudget.Item("Importo")), "#,##0.00")
                Else

                    If campodbN(myRighe.Item("Livello3")) = 0 Then
                        Dim CommandBudgetSL As New OleDbCommand("Select *  From Budget Where Anno = ? And Livello1 = ? And Livello2 = ?  And Colonna = ? ", cn)
                        Dim SommaSl As Double = 0

                        CommandBudgetSL.Parameters.AddWithValue("@Anno", Txt_Anno.Text)
                        CommandBudgetSL.Parameters.AddWithValue("@Livello1", myRighe.Item("Livello1"))
                        CommandBudgetSL.Parameters.AddWithValue("@Livello2", myRighe.Item("Livello2"))
                        CommandBudgetSL.Parameters.AddWithValue("@Colonna", 0)

                        Dim myBudgetSL As OleDbDataReader = CommandBudgetSL.ExecuteReader()
                        Do While myBudgetSL.Read()
                            SommaSl = SommaSl + campodbN(myBudgetSL.Item("Importo"))
                        Loop
                        myBudgetSL.Close()
                        myriga(2) = Format(SommaSl, "#,##0.00")

                        Cella(Indice, 2) = "G"
                    End If
                End If
            Else
                If campodbN(myRighe.Item("Livello3")) = 0 Then
                    Dim CommandBudgetSL As New OleDbCommand("Select * From Budget Where Anno = ? And Livello1 = ? And Livello2 = ? And Colonna = ? ", cn)
                    Dim SommaSl As Double = 0

                    CommandBudgetSL.Parameters.AddWithValue("@Anno", Txt_Anno.Text)
                    CommandBudgetSL.Parameters.AddWithValue("@Livello1", myRighe.Item("Livello1"))
                    CommandBudgetSL.Parameters.AddWithValue("@Livello2", myRighe.Item("Livello2"))
                    CommandBudgetSL.Parameters.AddWithValue("@Colonna", 0)

                    Dim myBudgetSL As OleDbDataReader = CommandBudgetSL.ExecuteReader()
                    Do While myBudgetSL.Read()
                        SommaSl = SommaSl + campodbN(myBudgetSL.Item("Importo"))
                    Loop
                    myBudgetSL.Close()
                    myriga(2) = Format(SommaSl, "#,##0.00")
                    Cella(Indice, 2) = "G"
                End If
            End If
            myBudget.Close()

            Dim IndiceColGr As Integer = 2
            Dim CommandColonneInt As New OleDbCommand("Select * From ColonneBudget Where Anno = ? Order By Livello1", cn)

            CommandColonneInt.Parameters.Add("@Anno", OleDbType.Integer, 8)
            CommandColonneInt.Parameters("@Anno").Value = Txt_Anno.Text
            Dim myColonneInt As OleDbDataReader = CommandColonneInt.ExecuteReader()

            Do While myColonneInt.Read()

                IndiceColGr = IndiceColGr + 1
                Dim CommandBudgetC As New OleDbCommand("Select * From Budget Where Anno = ? And Livello1 = ? And Livello2 = ? And Livello3 = ? And Colonna = ? ", cn)

                CommandBudgetC.Parameters.AddWithValue("@Anno", Txt_Anno.Text)
                CommandBudgetC.Parameters.AddWithValue("@Livello1", myRighe.Item("Livello1"))
                CommandBudgetC.Parameters.AddWithValue("@Livello2", myRighe.Item("Livello2"))
                CommandBudgetC.Parameters.AddWithValue("@Livello3", myRighe.Item("Livello3"))
                CommandBudgetC.Parameters.AddWithValue("@Colonna", myColonneInt.Item("Livello1"))


                Dim myBudgetC As OleDbDataReader = CommandBudgetC.ExecuteReader()
                If myBudgetC.Read() Then


                    If campodbN(myBudgetC.Item("Importo")) <> 0 Then
                        myriga(IndiceColGr) = Format(campodbN(myBudgetC.Item("Importo")), "#,##0.00")
                    Else

                        If campodbN(myRighe.Item("Livello3")) = 0 Then
                            Dim CommandBudgetSL As New OleDbCommand("Select *  From Budget Where Anno = ? And Livello1 = ? And Livello2 = ? And Colonna = ? ", cn)
                            Dim SommaSl As Double = 0

                            CommandBudgetSL.Parameters.AddWithValue("@Anno", Txt_Anno.Text)
                            CommandBudgetSL.Parameters.AddWithValue("@Livello1", myRighe.Item("Livello1"))
                            CommandBudgetSL.Parameters.AddWithValue("@Livello2", myRighe.Item("Livello2"))
                            CommandBudgetSL.Parameters.AddWithValue("@Colonna", myColonneInt.Item("Livello1"))

                            Dim myBudgetSL As OleDbDataReader = CommandBudgetSL.ExecuteReader()
                            Do While myBudgetSL.Read()
                                SommaSl = SommaSl + campodbN(myBudgetSL.Item("Importo"))
                            Loop
                            myBudgetSL.Close()
                            myriga(IndiceColGr) = Format(SommaSl, "#,##0.00")
                            Cella(Indice, IndiceColGr) = "G"
                        End If
                    End If
                Else
                    If campodbN(myRighe.Item("Livello3")) = 0 Then
                        Dim CommandBudgetSL As New OleDbCommand("Select *  From Budget Where Anno = ? And Livello1 = ? And Livello2 = ? And Colonna = ? ", cn)
                        Dim SommaSl As Double = 0

                        CommandBudgetSL.Parameters.AddWithValue("@Anno", Txt_Anno.Text)
                        CommandBudgetSL.Parameters.AddWithValue("@Livello1", myRighe.Item("Livello1"))
                        CommandBudgetSL.Parameters.AddWithValue("@Livello2", myRighe.Item("Livello2"))
                        CommandBudgetSL.Parameters.AddWithValue("@Colonna", myColonneInt.Item("Livello1"))

                        Dim myBudgetSL As OleDbDataReader = CommandBudgetSL.ExecuteReader()
                        Do While myBudgetSL.Read()
                            SommaSl = SommaSl + campodbN(myBudgetSL.Item("Importo"))
                        Loop
                        myBudgetSL.Close()
                        myriga(IndiceColGr) = Format(SommaSl, "#,##0.00")
                        Cella(Indice, IndiceColGr) = "G"
                    End If
                End If
                myBudgetC.Close()                
            Loop
            myColonneInt.Close()

            Tabella.Rows.Add(myriga)
            Indice = Indice + 1
        Loop
        myRighe.Close()

        Grid.AutoGenerateColumns = True
        Grid.DataSource = Tabella
        Grid.Font.Size = 10
        Grid.DataBind()

        Dim X As Integer = 0


        For y = 0 To Grid.Rows.Count - 1
            'If Livello3(y) = 0 Then
            '    Grid.Rows.Item(y).Cells(0).ForeColor = Drawing.Color.White
            '    Grid.Rows.Item(y).Cells(0).BackColor = Drawing.Color.Red

            '    Grid.Rows.Item(y).Cells(1).ForeColor = Drawing.Color.White
            '    Grid.Rows.Item(y).Cells(1).BackColor = Drawing.Color.Red
            '    For X = 2 To Tabella.Columns.Count - 1
            '        Grid.Rows.Item(y).Cells(X).ForeColor = Drawing.Color.White
            '        Grid.Rows.Item(y).Cells(X).BackColor = Drawing.Color.Red
            '    Next
            'End If            
            For X = 2 To Tabella.Columns.Count - 1
                If Cella(y, X) = "G" Then
                    If Grid.Rows.Item(y).Cells(X).Text <> "0,00" Then
                        Grid.Rows.Item(y).Cells(X).ForeColor = Drawing.Color.Black
                        Grid.Rows.Item(y).Cells(X).BackColor = Drawing.Color.Aqua
                    End If
                End If

                Dim ApCella As String = "'R" & y & "C" & X & "'"
                Dim ApImporto As String = ""
                If Trim(Grid.Rows.Item(y).Cells(X).Text) = "" Or Trim(Grid.Rows.Item(y).Cells(X).Text) = "&nbsp;" Then
                    ApImporto = "'0'," & Livello1(y) & "," & Livello2(y) & "," & Livello3(y) & "," & Txt_Anno.Text & "," & Colonna(X - 2)
                Else
                    ApImporto = "'" & Grid.Rows.Item(y).Cells(X).Text & "'," & Livello1(y) & "," & Livello2(y) & "," & Livello3(y) & "," & Txt_Anno.Text & "," & Colonna(X - 2)
                End If

                If Grid.Rows.Item(y).Cells(X).Text = "" Then
                    Grid.Rows.Item(y).Cells(X).Text = "<div id=" & ApCella & "><a href=""#"" onclick=""ModificaCella(" & ApCella & "," & ApImporto & "); return false;"">0,00</a></div>"
                    'Grid.Rows.Item(y).Cells(X).Text = "<div id=" & ApCella & "><a href=""#"" onclick=""alert(" & ApCella & "," & ApImporto & ");"">0,00</a></div>"
                Else
                    Grid.Rows.Item(y).Cells(X).Text = "<div id=" & ApCella & "><a href=""#"" onclick=""ModificaCella(" & ApCella & "," & ApImporto & "); return false;"">" & Grid.Rows.Item(y).Cells(X).Text & "</a></div>"
                    'Grid.Rows.Item(y).Cells(X).Text = "<div id=" & ApCella & "><a href=""#"" onclick=""alert(" & ApCella & "," & ApImporto & ");"">" & Grid.Rows.Item(y).Cells(X).Text & "</a></div>"
                End If
                Grid.Rows.Item(y).Cells(X).Style.Value = "text-align: right;"
            Next
        Next
    End Sub
    Protected Sub ImageButton1_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles ImageButton1.Click
        Call PienaGriglia()
    End Sub

    Protected Sub Btn_Esci_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles Btn_Esci.Click
        Response.Redirect("Menu_Budget.aspx")
    End Sub

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Trim(Session("UTENTE")) = "" Then
            Response.Redirect("/ODV/Login.aspx")
            Exit Sub
        End If

        If Page.IsPostBack = True Then
            Exit Sub
        End If

        Dim K1 As New Cls_SqlString

        Txt_Anno.Text = Year(Now)

        Dim Barra As New Cls_BarraSenior

        If Not IsNothing(Session("RicercaGeneraleSQLString")) Then
            K1 = Session("RicercaGeneraleSQLString")
        End If

        Lbl_BarraSenior.Text = Barra.CodiceBarra(Application("SENIOR"), Session("UTENTE"), Page, K1)

    End Sub
End Class

