﻿Imports Microsoft.VisualBasic
Imports System.Data.OleDb
Imports System.Web.Hosting
Imports System.Data.SqlTypes

Partial Class GeneraleWeb_ElencoClientiFornitori
    Inherits System.Web.UI.Page
    Dim Tabella As New System.Data.DataTable("tabella")


    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Session("UTENTE") = "" Then
            Response.Redirect("..\Login.aspx")
            Exit Sub
        End If

        If Page.IsPostBack = True Then
            Exit Sub
        End If

        Dim K1 As New Cls_SqlString

        Dim Barra As New Cls_BarraSenior

        If Not IsNothing(Session("RicercaGeneraleSQLString")) Then
            K1 = Session("RicercaGeneraleSQLString")
        End If

        Lbl_BarraSenior.Text = Barra.CodiceBarra(Application("SENIOR"), Session("UTENTE"), Page, K1)


        If Request.Item("CHIAMATA") <> "RITORNO" Then
            Call CaricaGriglia()
            Session("ElencoCliForSQLString") = Nothing
        Else

            If Not IsNothing(Session("ElencoCliForSQLString")) Then
                Dim k As New Cls_SqlString
                Dim Appoggio As System.Web.UI.ImageClickEventArgs

                k = Session("ElencoCliForSQLString")


                Txt_Conto.Text = k.GetValue("Txt_Conto")


                Chk_SoloClienti.Checked = k.GetValue("Chk_SoloClienti")
                Chk_SoloFornitori.Checked = k.GetValue("Chk_SoloFornitori")


                Call CaricaGriglia()


            Else
                Call CaricaGriglia()
            End If

            If Val(Request.Item("PAGINA")) > 0 Then
                If Val(Request.Item("PAGINA")) < Grid.PageCount Then
                    Grid.PageIndex = Val(Request.Item("PAGINA"))
                End If
            End If

            Tabella = ViewState("Appoggio")

            Grid.AutoGenerateColumns = True
            Grid.DataSource = Tabella
            Grid.DataBind()
        End If
        'Btn_Nuovo.Visible = True
    End Sub

    Function campodb(ByVal oggetto As Object) As String
        If IsDBNull(oggetto) Then
            Return ""
        Else
            Return oggetto
        End If
    End Function

    Function campodbN(ByVal oggetto As Object) As Double
        If IsDBNull(oggetto) Then
            Return 0
        Else
            Return oggetto
        End If
    End Function

    Protected Sub Grid_PageIndexChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewPageEventArgs) Handles Grid.PageIndexChanging
        Tabella = ViewState("Appoggio")
        Grid.PageIndex = e.NewPageIndex
        Grid.DataSource = Tabella
        Grid.DataBind()
    End Sub

    Protected Sub Grid_RowCommand(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewCommandEventArgs) Handles Grid.RowCommand
        If e.CommandName = "Seleziona" Then


            Dim d As Integer


            Tabella = ViewState("Appoggio")

            d = Val(e.CommandArgument)


            Dim Codice As String

            Codice = Tabella.Rows(d).Item(0).ToString


            Response.Redirect("AnagraficaClientiFornitori.aspx?Codice=" & Codice & "&PAGINA=" & Grid.PageIndex)
        End If
    End Sub

    Protected Sub ImgRicerca_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles ImgRicerca.Click
        Response.Redirect("AnagraficaClientiFornitori.aspx")
    End Sub

    Protected Sub Btn_Ricerca_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles Btn_Ricerca.Click

        Call CaricaGriglia()

    End Sub

    Private Sub CaricaGriglia()

        Dim cn As New OleDbConnection


        Dim ConnectionString As String = Session("DC_OSPITE")

        cn = New Data.OleDb.OleDbConnection(ConnectionString)

        cn.Open()


        Dim cmd As New OleDbCommand

        Dim k1 As New Cls_SqlString



        k1.Add("Txt_Conto", Txt_Conto.Text)
        k1.Add("Chk_SoloClienti", Chk_SoloClienti.Checked)
        k1.Add("Chk_SoloFornitori", Chk_SoloFornitori.Checked)

        Session("ElencoCliForSQLString") = k1


        If Txt_Conto.Text.Trim = "" Then
            cmd.CommandText = "Select * from AnagraficaComune where  TIPOLOGIA = 'D' Order By CODICEDEBITORECREDITORE Desc"
            cmd.Connection = cn
            cmd.Parameters.AddWithValue("@Descrizione", Txt_Conto.Text)
        Else
            cmd.CommandText = "Select  * from AnagraficaComune where  (Nome Like ? Or CodiceFiscale Like ? Or (PartitaIva = ? And PartitaIva > 0)) And TIPOLOGIA = 'D' Order By Nome"
            cmd.Connection = cn
            If Txt_Conto.Text.Trim = "" Then
                Txt_Conto.Text = "%"
            End If
            cmd.Parameters.AddWithValue("@Descrizione", Txt_Conto.Text)
            cmd.Parameters.AddWithValue("@Descrizione", Txt_Conto.Text)
            cmd.Parameters.AddWithValue("@Descrizione", Val(Txt_Conto.Text))
        End If

        Dim MaxID As Long = 0
        Tabella.Clear()
        Tabella.Columns.Add("Codice", GetType(String))
        Tabella.Columns.Add("Descrizione", GetType(String))
        Tabella.Columns.Add("Comune", GetType(String))
        Tabella.Columns.Add("Cliente", GetType(String))
        Tabella.Columns.Add("Fornitore", GetType(String))

        Dim myPOSTreader As OleDbDataReader = cmd.ExecuteReader()
        Do While myPOSTreader.Read

            If (Chk_SoloClienti.Checked = True And campodbN(myPOSTreader.Item("MASTROCLIENTE")) > 0) Or (Chk_SoloFornitori.Checked = True And campodbN(myPOSTreader.Item("MASTROFORNITORE")) > 0) Or (campodbN(myPOSTreader.Item("MASTROFORNITORE")) = 0 And campodbN(myPOSTreader.Item("MASTROCLIENTE")) = 0) Then
                MaxID = MaxID + 1

                Dim myriga As System.Data.DataRow = Tabella.NewRow()

                myriga(0) = myPOSTreader.Item("CODICEDEBITORECREDITORE")
                myriga(1) = myPOSTreader.Item("Nome")

                Dim k As New ClsComune

                k.Provincia = campodb(myPOSTreader.Item("Nome"))
                k.Comune = campodb(myPOSTreader.Item("Nome"))
                k.Leggi(Session("DC_OSPITE"))
                myriga(2) = k.Descrizione


                If campodbN(myPOSTreader.Item("MASTROCLIENTE")) > 0 Then
                    myriga(3) = "X " & campodbN(myPOSTreader.Item("MASTROCLIENTE")) & " " & campodbN(myPOSTreader.Item("CONTOCLIENTE")) & " " & campodbN(myPOSTreader.Item("SOTTOCONTOCLIENTE"))
                End If


                If campodbN(myPOSTreader.Item("MASTROFORNITORE")) > 0 Then
                    myriga(4) = "X " & campodbN(myPOSTreader.Item("MASTROFORNITORE")) & " " & campodbN(myPOSTreader.Item("CONTOFORNITORE")) & " " & campodbN(myPOSTreader.Item("SOTTOCONTOFORNITORE"))
                End If


                Tabella.Rows.Add(myriga)                
                If MaxID > 20 Then
                    Exit Do
                End If
            End If
        Loop

        myPOSTreader.Close()
        cn.Close()

        Session("UltimoNumero") = MaxID + 1

        ViewState("Appoggio") = Tabella

        Grid.AutoGenerateColumns = True

        Grid.DataSource = Tabella

        Grid.DataBind()

    End Sub


    Private Function SplitWords(ByVal s As String) As String()
        '
        ' Call Regex.Split function from the imported namespace.
        ' Return the result array.
        '
        Return Regex.Split(s, "\W+")
    End Function

    Protected Sub Btn_Esci_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles Btn_Esci.Click
        Response.Redirect("Menu_Generale.aspx")
    End Sub

    

    Protected Sub Img_Espandi_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles Img_Espandi.Click
        Dim cn As New OleDbConnection

        Tabella = ViewState("Appoggio")

        Dim ConnectionString As String = Session("DC_OSPITE")

        cn = New Data.OleDb.OleDbConnection(ConnectionString)

        cn.Open()


        Dim cmd As New OleDbCommand

        Dim k1 As New Cls_SqlString



        k1.Add("Txt_Conto", Txt_Conto.Text)
        k1.Add("Chk_SoloClienti", Chk_SoloClienti.Checked)
        k1.Add("Chk_SoloFornitori", Chk_SoloFornitori.Checked)

        Session("ElencoCliForSQLString") = k1


        If Txt_Conto.Text.Trim = "" Then
            cmd.CommandText = "Select  * from AnagraficaComune where  TIPOLOGIA = 'D' Order By CODICEDEBITORECREDITORE Desc"
            cmd.Connection = cn
            cmd.Parameters.AddWithValue("@Descrizione", Txt_Conto.Text)
        Else
            cmd.CommandText = "Select  * from AnagraficaComune where  Nome Like ? And TIPOLOGIA = 'D' Order By Nome"
            cmd.Connection = cn
            If Txt_Conto.Text.Trim = "" Then
                Txt_Conto.Text = "%"
            End If
            cmd.Parameters.AddWithValue("@Descrizione", Txt_Conto.Text)
        End If

        Dim MaxID As Long = 0


        Dim myPOSTreader As OleDbDataReader = cmd.ExecuteReader()
        Do While myPOSTreader.Read

            If (Chk_SoloClienti.Checked = True And campodbN(myPOSTreader.Item("MASTROCLIENTE")) > 0) Or (Chk_SoloFornitori.Checked = True And campodbN(myPOSTreader.Item("MASTROFORNITORE")) > 0) Or (campodbN(myPOSTreader.Item("MASTROFORNITORE")) = 0 And campodbN(myPOSTreader.Item("MASTROCLIENTE")) = 0) Then
                MaxID = MaxID + 1

                If MaxID > 5 + Session("UltimoNumero") Then
                    Exit Do
                End If
                If MaxID >= Session("UltimoNumero") Then
                    Dim ControllaUltimoCodice As Boolean = True
                    If myPOSTreader.Item("CODICEDEBITORECREDITORE") = Tabella.Rows(Tabella.Rows.Count - 1).Item(0) Then
                        ControllaUltimoCodice = False
                    End If
                    If ControllaUltimoCodice Then

                        Dim myriga As System.Data.DataRow = Tabella.NewRow()

                        myriga(0) = myPOSTreader.Item("CODICEDEBITORECREDITORE")
                        myriga(1) = myPOSTreader.Item("Nome")

                        Dim k As New ClsComune

                        k.Provincia = campodb(myPOSTreader.Item("Nome"))
                        k.Comune = campodb(myPOSTreader.Item("Nome"))
                        k.Leggi(Session("DC_OSPITE"))
                        myriga(2) = k.Descrizione


                        If campodbN(myPOSTreader.Item("MASTROCLIENTE")) > 0 Then
                            myriga(3) = "X " & campodbN(myPOSTreader.Item("MASTROCLIENTE")) & " " & campodbN(myPOSTreader.Item("CONTOCLIENTE")) & " " & campodbN(myPOSTreader.Item("SOTTOCONTOCLIENTE"))
                        End If


                        If campodbN(myPOSTreader.Item("MASTROFORNITORE")) > 0 Then
                            myriga(4) = "X " & campodbN(myPOSTreader.Item("MASTROFORNITORE")) & " " & campodbN(myPOSTreader.Item("CONTOFORNITORE")) & " " & campodbN(myPOSTreader.Item("SOTTOCONTOFORNITORE"))
                        End If


                        Tabella.Rows.Add(myriga)
                    End If
                End If
            End If
        Loop

        myPOSTreader.Close()
        cn.Close()


        Session("UltimoNumero") = MaxID

        ViewState("Appoggio") = Tabella

        Grid.AutoGenerateColumns = True

        Grid.DataSource = Tabella

        Grid.DataBind()

    End Sub
End Class
