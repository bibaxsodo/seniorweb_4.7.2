﻿<%@ Page Language="VB" AutoEventWireup="false" Inherits="GeneraleWeb_DatiMandatiReversali" CodeFile="DatiMandatiReversali.aspx.vb" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>Dati Mandati Reversali</title>
    <asp:PlaceHolder runat="server">
        <%: System.Web.Optimization.Styles.Render("~/Content/AjaxControlToolkit/Styles/Bundle") %>
    </asp:PlaceHolder>
    <link href="ospiti.css?ver=11" rel="stylesheet" type="text/css" />
    <link rel="shortcut icon" href="images/SENIOR.ico" />
    <link href="js/jquery.autocomplete.css" rel="stylesheet" type="text/css" />

    <script src="js/jquery-1.5.1.min.js" type="text/javascript"></script>
    <script src="js/jquery.autocomplete.js" type="text/javascript"></script>
    <script src="js/soapclient.js" type="text/javascript"></script>
    <script src="/js/convertinumero.js" type="text/javascript"></script>

    <script src="js/jquery.maskedinput-1.3.min.js" type="text/javascript"></script>
    <script src="/js/formatnumer.js" type="text/javascript"></script>

    <script src="/js/pianoconti.js" type="text/javascript"></script>

    <script src="js/JSErrore.js" type="text/javascript"></script>

    <link rel="stylesheet" href="dialogbox/redips-dialog.css" type="text/css" media="screen" />
    <script type="text/javascript" src="dialogbox/redips-dialog-min.js"></script>
    <script type="text/javascript" src="dialogbox/script.js"></script>
    <script src="js/NoEnter.js" type="text/javascript"></script>
    <link rel="stylesheet" href="jqueryui/jquery-ui.css" type="text/css" />
    <script src="jqueryui/jquery-ui.js" type="text/javascript"></script>
    <script src="jqueryui/datapicker-it.js" type="text/javascript"></script>

</head>
<body>
    <form id="form1" runat="server">
        <asp:ScriptManager ID="ScriptManager1" runat="server" EnableScriptGlobalization="true">
            <Scripts>
                <asp:ScriptReference Path="~/Scripts/AjaxControlToolkit/Bundle" />
            </Scripts>
        </asp:ScriptManager>
        <div>
            <label class="LabelCampo">Reversale Data :</label>
            <asp:TextBox ID="Txt_DataReversale" autocomplete="off" runat="server" Width="90px"></asp:TextBox>&nbsp;         
         Numero :
         <asp:TextBox ID="Txt_NumeroReversale" Style="text-align: right;" autocomplete="off" runat="server" Width="104px"></asp:TextBox><br />
            <br />
            <label class="LabelCampo">Mandato Data :</label>
            <asp:TextBox ID="Txt_DataMandato" autocomplete="off" runat="server" Width="90px"></asp:TextBox>&nbsp;         
         Numero :
         <asp:TextBox ID="Txt_NumeroMandato" Style="text-align: right;" autocomplete="off" runat="server" Width="104px"></asp:TextBox><br />
            <br />
            <label class="LabelCampo">Storno :</label>
            <asp:CheckBox ID="Chk_Storno" runat="server" Text="" /><br />
            <br />
            <label class="LabelCampo">Distinta Data :</label>
            <asp:TextBox ID="Txt_DataDistinta" autocomplete="off" runat="server" Width="90px"></asp:TextBox>&nbsp;
         Numero :
         <asp:TextBox ID="Txt_NumeroDistinta" Style="text-align: right;" autocomplete="off" runat="server" Width="104px"></asp:TextBox><br />
            <br />

            <label class="LabelCampo">Bollo : </label>
            <asp:RadioButton ID="RB_CaricoEnte_Bollo" runat="server" Text="Carico Ento" GroupName="Bollo" />
            <asp:RadioButton ID="Rb_CaricoCliente_Bollo" runat="server" Text="Carico Cliente" GroupName="Bollo" />
            <asp:RadioButton ID="Rb_Esente_Bollo" runat="server" Text="Esente" Checked="true" GroupName="Bollo" /><br />
            <br />
            <label class="LabelCampo">Spese: </label>
            <asp:RadioButton ID="RB_CaricoEnte_Spese" runat="server" Text="Carico Ento" GroupName="Spese" />
            <asp:RadioButton ID="Rb_CaricoCliente_Spese" runat="server" Text="Carico Cliente" GroupName="Spese" />
            <asp:RadioButton ID="Rb_Esente_Spese" runat="server" Text="Esente" Checked="true" GroupName="Spese" /><br />
            <br />
            <label class="LabelCampo">Data :</label>
            <asp:TextBox ID="Txt_DataEsecuzionePagamento" autocomplete="off" runat="server" Width="90px"></asp:TextBox>
            (Esecuzione Pagamento)&nbsp;
         <br />
            <label class="LabelCampo">Impegno Anno :</label>
            <asp:TextBox ID="Txt_AnnoImpegno" Style="text-align: right;" autocomplete="off" runat="server" Width="104px"></asp:TextBox>
            Numero :
         <asp:TextBox ID="Txt_NumeroImpegno" Style="text-align: right;" autocomplete="off" runat="server" Width="104px"></asp:TextBox><br />
            <br />
            <label class="LabelCampo">Cup :</label>
            <asp:TextBox ID="Txt_Cup" Style="text-align: right;" autocomplete="off" runat="server" Width="104px"></asp:TextBox>
            Siope :
         <asp:TextBox ID="Txt_Siope" Style="text-align: right;" autocomplete="off" runat="server" Width="104px"></asp:TextBox>
            <br />
            <br />
            <label class="LabelCampo">Codice Cig :</label>
            <asp:TextBox ID="Txt_CodiceCig" Style="text-align: right;" autocomplete="off" runat="server" Width="104px"></asp:TextBox>
            Cofog :
         <asp:DropDownList ID="DD_Cofog" runat="Server"></asp:DropDownList><br />
            <br />
            <br />
            <label class="LabelCampo">Tipo Pagamento :</label>
            <asp:DropDownList ID="DD_TipoPagametno" runat="Server"></asp:DropDownList><br />
            <br />
            <label class="LabelCampo">Numero Regolazione :</label>
            <asp:TextBox ID="Txt_NumeroRegolazione" Style="text-align: right;" autocomplete="off" runat="server" Width="104px"></asp:TextBox><br />
            <br />
            <label class="LabelCampo">Numero Elenco :</label>
            <asp:TextBox ID="Txt_NumeroElenco" Style="text-align: right;" autocomplete="off" runat="server" Width="104px"></asp:TextBox><br />
            <br />
            <label class="LabelCampo">Delegato :</label>
            <asp:TextBox ID="Txt_Delegato" CssClass="MyAutoComplete" runat="server" Width="420px"></asp:TextBox>
            <br />
            <asp:ImageButton ID="Btn_Modifica" Height="38px" runat="server" ImageUrl="~/images/salva.jpg" class="EffettoBottoniTondi" ToolTip="Modifica" />&nbsp; 
        </div>
    </form>
</body>
</html>
