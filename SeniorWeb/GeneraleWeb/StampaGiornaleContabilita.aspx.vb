﻿Imports Microsoft.VisualBasic
Imports System.Data.OleDb
Imports System.Web.Hosting
Imports CrystalDecisions.CrystalReports.Engine
Imports CrystalDecisions.Shared


Partial Class GeneraleWeb_StampaGiornaleContabilita
    Inherits System.Web.UI.Page

    Public Function MoveFromDbWC(ByVal MyRs As ADODB.Recordset, ByVal Campo As String) As Object
        Dim MyField As ADODB.Field

#If FLAG_DEBUG = False Then
        On Error GoTo ErroreCampoInesistente
#End If
        MyField = MyRs.Fields(Campo)

        Select Case MyField.Type
            Case ADODB.DataTypeEnum.adDBDate
                If IsDBNull(MyField.Value) Then
                    MoveFromDbWC = ""
                Else
                    If Not IsDate(MyField.Value) Then
                        MoveFromDbWC = ""
                    Else
                        MoveFromDbWC = Format(MyField.Value, "dd/MM/yyy")
                    End If
                End If
            Case ADODB.DataTypeEnum.adDBTimeStamp
                If IsDBNull(MyField.Value) Then
                    MoveFromDbWC = ""
                Else
                    If Not IsDate(MyField.Value) Then
                        MoveFromDbWC = ""
                    Else
                        MoveFromDbWC = Format(MyField.Value, "dd/MM/yyy")
                    End If
                End If
            Case ADODB.DataTypeEnum.adSmallInt
                If IsDBNull(MyField.Value) Then
                    MoveFromDbWC = 0
                Else
                    MoveFromDbWC = MyField.Value
                End If
            Case ADODB.DataTypeEnum.adInteger
                If IsDBNull(MyField.Value) Then
                    MoveFromDbWC = 0
                Else
                    MoveFromDbWC = MyField.Value
                End If
            Case ADODB.DataTypeEnum.adNumeric
                If IsDBNull(MyField.Value) Then
                    MoveFromDbWC = 0
                Else
                    MoveFromDbWC = MyField.Value
                End If
            Case ADODB.DataTypeEnum.adTinyInt
                If IsDBNull(MyField.Value) Then
                    MoveFromDbWC = 0
                Else
                    MoveFromDbWC = MyField.Value
                End If
            Case ADODB.DataTypeEnum.adSingle
                If IsDBNull(MyField.Value) Then
                    MoveFromDbWC = 0
                Else
                    MoveFromDbWC = MyField.Value
                End If
            Case ADODB.DataTypeEnum.adLongVarBinary
                If IsDBNull(MyField.Value) Then
                    MoveFromDbWC = 0
                Else
                    MoveFromDbWC = MyField.Value
                End If
            Case ADODB.DataTypeEnum.adDouble
                If IsDBNull(MyField.Value) Then
                    MoveFromDbWC = 0
                Else
                    MoveFromDbWC = MyField.Value
                End If
            Case ADODB.DataTypeEnum.adVarChar
                If IsDBNull(MyField.Value) Then
                    MoveFromDbWC = vbNullString
                Else
                    MoveFromDbWC = CStr(MyField.Value)
                End If
            Case ADODB.DataTypeEnum.adUnsignedTinyInt
                If IsDBNull(MyField.Value) Then
                    MoveFromDbWC = 0
                Else
                    MoveFromDbWC = MyField.Value
                End If
            Case Else
                If IsDBNull(MyField.Value) Then
                    MoveFromDbWC = ""
                End If
                If IsDBNull(MyField.Value) Then
                    MoveFromDbWC = ""
                Else
                    MoveFromDbWC = MyField.Value
                End If
        End Select

        On Error GoTo 0
        Exit Function
ErroreCampoInesistente:

        On Error GoTo 0
    End Function
    Private Function MoveFromDb(ByVal MyField As ADODB.Field, Optional ByVal Tipo As Integer = 0) As Object
        If Tipo = 0 Then
            Select Case MyField.Type
                Case ADODB.DataTypeEnum.adDBDate Or ADODB.DataTypeEnum.adDBTimeStamp
                    If IsDBNull(MyField.Value) Then
                        MoveFromDb = ""
                    Else
                        If Not IsDate(MyField.Value) Then
                            MoveFromDb = ""
                        Else
                            MoveFromDb = MyField.Value
                        End If
                    End If
                Case ADODB.DataTypeEnum.adSmallInt
                    If IsDBNull(MyField.Value) Then
                        MoveFromDb = 0
                    Else
                        MoveFromDb = MyField.Value
                    End If
                Case 20 ' INTERO PER MYSQL
                    If IsDBNull(MyField.Value) Then
                        MoveFromDb = 0
                    Else
                        MoveFromDb = MyField.Value
                    End If
                Case ADODB.DataTypeEnum.adInteger
                    If IsDBNull(MyField.Value) Then
                        MoveFromDb = 0
                    Else
                        MoveFromDb = MyField.Value
                    End If
                Case ADODB.DataTypeEnum.adNumeric
                    If IsDBNull(MyField.Value) Then
                        MoveFromDb = 0
                    Else
                        MoveFromDb = MyField.Value
                    End If
                Case ADODB.DataTypeEnum.adTinyInt
                    If IsDBNull(MyField.Value) Then
                        MoveFromDb = 0
                    Else
                        MoveFromDb = MyField.Value
                    End If
                Case ADODB.DataTypeEnum.adSingle
                    If IsDBNull(MyField.Value) Then
                        MoveFromDb = 0
                    Else
                        MoveFromDb = MyField.Value
                    End If
                Case ADODB.DataTypeEnum.adLongVarBinary
                    If IsDBNull(MyField.Value) Then
                        MoveFromDb = 0
                    Else
                        MoveFromDb = MyField.Value
                    End If
                Case ADODB.DataTypeEnum.adDouble
                    If IsDBNull(MyField.Value) Then
                        MoveFromDb = 0
                    Else
                        MoveFromDb = MyField.Value
                    End If
                Case 139
                    If IsDBNull(MyField.Value) Then
                        MoveFromDb = 0
                    Else
                        MoveFromDb = MyField.Value
                    End If
                Case ADODB.DataTypeEnum.adVarChar
                    If IsDBNull(MyField.Value) Then
                        MoveFromDb = vbNullString
                    Else
                        MoveFromDb = CStr(MyField.Value)
                    End If
                Case ADODB.DataTypeEnum.adUnsignedTinyInt
                    If IsDBNull(MyField.Value) Then
                        MoveFromDb = 0
                    Else
                        MoveFromDb = MyField.Value
                    End If
                Case Else
                    If IsDBNull(MyField.Value) Then
                        MoveFromDb = ""
                    End If
                    If IsDBNull(MyField.Value) Then
                        MoveFromDb = ""
                    Else
                        'MoveFromDb = StringaSqlS(MyField.Value)
                        MoveFromDb = RTrim(MyField.Value)
                    End If
            End Select
        End If
    End Function

    Public Sub MoveToDb(ByRef MyField As ADODB.Field, ByVal MyVal As Object)
        Select Case Val(MyField.Type)
            Case ADODB.DataTypeEnum.adDBDate Or ADODB.DataTypeEnum.adDBTimeStamp
                If IsDate(MyVal) Then
                    MyField.Value = MyVal
                Else
                    MyField.Value = Nothing
                End If
            Case ADODB.DataTypeEnum.adDouble
                If IsNumeric(MyVal) Then
                    MyField.Value = MyVal
                Else
                    MyField.Value = 0
                End If
            Case ADODB.DataTypeEnum.adSmallInt
                If IsNumeric(MyVal) Then
                    MyField.Value = MyVal
                Else : MyField.Value = 0
                End If
            Case ADODB.DataTypeEnum.adInteger
                If IsNumeric(MyVal) Then
                    MyField.Value = MyVal
                Else
                    MyField.Value = 0
                End If
            Case ADODB.DataTypeEnum.adNumeric
                If IsNumeric(MyVal) Then
                    MyField.Value = MyVal
                Else
                    MyField.Value = 0
                End If
            Case ADODB.DataTypeEnum.adTinyInt
                If IsNumeric(MyVal) Then
                    MyField.Value = MyVal
                Else
                    MyField.Value = 0
                End If
            Case ADODB.DataTypeEnum.adSingle
                If IsNumeric(MyVal) Then
                    MyField.Value = MyVal
                Else
                    MyField.Value = 0
                End If
            Case ADODB.DataTypeEnum.adLongVarBinary
                If IsNumeric(MyVal) Then
                    MyField.Value = MyVal
                Else
                    MyField.Value = 0
                End If
            Case ADODB.DataTypeEnum.adUnsignedTinyInt
                If IsNumeric(MyVal) Then
                    MyField.Value = MyVal
                Else
                    MyField.Value = 0
                End If
            Case ADODB.DataTypeEnum.adVarChar Or ADODB.DataTypeEnum.adVarWChar Or ADODB.DataTypeEnum.adWChar Or ADODB.DataTypeEnum.adBSTR
                If IsDBNull(MyVal) Then
                    MyField.Value = ""
                Else
                    If Len(MyVal) > MyField.DefinedSize Then
                        MyField.Value = Mid(MyVal, 1, MyField.DefinedSize)
                    Else
                        MyField.Value = MyVal
                    End If
                End If
            Case Else
                If Val(MyField.Type) = ADODB.DataTypeEnum.adVarChar Or Val(MyField.Type) = ADODB.DataTypeEnum.adVarWChar Or _
                   Val(MyField.Type) = ADODB.DataTypeEnum.adWChar Or Val(MyField.Type) = ADODB.DataTypeEnum.adBSTR Then
                    If IsDBNull(MyVal) Then
                        MyField.Value = ""
                    Else
                        If Len(MyVal) > MyField.DefinedSize Then
                            MyField.Value = Mid(MyVal, 1, MyField.DefinedSize)
                        Else
                            MyField.Value = MyVal
                        End If
                    End If
                Else
                    MyField.Value = MyVal
                End If
        End Select
    End Sub


    Private Sub CreaRecordStampa(ByVal DataInizio As Date, ByVal DataFine As Date)
        REM Dim StampeDb As New ADODB.Connection

        Dim MyRsT As New ADODB.Recordset
        Dim MyRsR As New ADODB.Recordset
        Dim GeneraleDb As New ADODB.Connection
        Dim TotaleAvere, TotaleDare As Double
        Dim OldNumeroRegistrazione As Long
        Dim UltimaRiga As Long
        Dim WSocieta As String
        Dim MySql As String
        Dim XC As New Cls_DecodificaSocieta

        Dim DallaData As Date
        Dim AllaData As Date

        Dim Stampa As New StampeGenerale


        DallaData = Txt_DallaData.Text
        AllaData = Txt_AllaData.Text



        UltimaRiga = Val(Txt_DallaRiga.Text) - 1


        ViewState("PRINTERKEY") = Format(Now, "yyyyMMddHHmmss") & Session.SessionID

        WSocieta = XC.DecodificaSocieta(Session("DC_TABELLE"))

        GeneraleDb.Open(Session("DC_GENERALE"))


        REM StampeDb.Open(Session("StampeFinanziaria"))
        REM StampeDb.Execute("Delete From GiornaleContabilita where PRINTERKEY = '" & ViewState("PRINTERKEY") & "'")

        MySql = "SELECT SUM(Importo) AS Totale FROM MovimentiContabiliTesta " & _
                " INNER JOIN MovimentiContabiliRiga ON MovimentiContabiliTesta.NumeroRegistrazione = MovimentiContabiliRiga.Numero " & _
                " WHERE DareAvere = 'D' " & _
                " AND RigaBollato > 0 " & _
                " AND RigaBollato < " & Txt_DallaRiga.Text & _
                " AND DataRegistrazione >= {ts '" & Format(DataInizio, "yyyy-MM-dd") & " 00:00:00'} " & _
                " AND DataRegistrazione <= {ts '" & Format(DataFine, "yyyy-MM-dd") & " 00:00:00'}"
        MyRsT.Open(MySql, GeneraleDb, ADODB.CursorTypeEnum.adOpenKeyset, ADODB.LockTypeEnum.adLockOptimistic)
        TotaleDare = MoveFromDbWC(MyRsT, "Totale")
        MyRsT.Close()

        MySql = "SELECT SUM(Importo) AS Totale FROM MovimentiContabiliTesta " & _
                " INNER JOIN MovimentiContabiliRiga ON MovimentiContabiliTesta.NumeroRegistrazione = MovimentiContabiliRiga.Numero " & _
                " WHERE DareAvere = 'A' " & _
                " AND RigaBollato > 0 " & _
                " AND RigaBollato < " & Txt_DallaRiga.Text & _
                " AND DataRegistrazione >= {ts '" & Format(DataInizio, "yyyy-MM-dd") & " 00:00:00'} " & _
                " AND DataRegistrazione <= {ts '" & Format(DataFine, "yyyy-MM-dd") & " 00:00:00'}"
        MyRsT.Open(MySql, GeneraleDb, ADODB.CursorTypeEnum.adOpenKeyset, ADODB.LockTypeEnum.adLockOptimistic)
        TotaleAvere = MoveFromDbWC(MyRsT, "Totale")
        MyRsT.Close()

        OldNumeroRegistrazione = 0
        REM MyRs.Open("GiornaleContabilita", StampeDb, ADODB.CursorTypeEnum.adOpenKeyset, ADODB.LockTypeEnum.adLockOptimistic)
        If Txt_AllaRiga.Text = 0 Then
            MySql = "SELECT NumeroRegistrazione, DataRegistrazione, CausaleContabile, NumeroDocumento, DataDocumento, Competenza FROM MovimentiContabiliTesta " & _
                    " INNER JOIN MovimentiContabiliRiga ON MovimentiContabiliTesta.NumeroRegistrazione = MovimentiContabiliRiga.Numero " & _
                    " WHERE (RigaBollato = 0 OR RigaBollato IS NULL)  And " & _
                    " DataRegistrazione >= {ts '" & Format(DallaData, "yyyy-MM-dd") & " 00:00:00'} " & _
                    " AND DataRegistrazione <= {ts '" & Format(AllaData, "yyyy-MM-dd") & " 00:00:00'}" & _
                    " Group by  NumeroRegistrazione, DataRegistrazione, CausaleContabile, NumeroDocumento, DataDocumento, Competenza" & _
                    " ORDER BY DataRegistrazione, NumeroRegistrazione"
            MyRsT.Open(MySql, GeneraleDb, ADODB.CursorTypeEnum.adOpenKeyset, ADODB.LockTypeEnum.adLockOptimistic)
            Do While Not MyRsT.EOF
                MySql = "SELECT * FROM MovimentiContabiliRiga " & _
                        " WHERE Numero = " & MoveFromDbWC(MyRsT, "NumeroRegistrazione") & _
                        " ORDER By RigaDaCausale"
                MyRsR.Open(MySql, GeneraleDb, ADODB.CursorTypeEnum.adOpenKeyset, ADODB.LockTypeEnum.adLockOptimistic)
                Do While Not MyRsR.EOF

                    Dim MyRs As System.Data.DataRow = Stampa.Tables("GiornaleContabilita").NewRow


                    If Chk_StampaEffettiva.Checked = True Then
                        If Chk_NumeroRigaRegistrazione.Checked = True Then
                            If MoveFromDbWC(MyRsT, "NumeroRegistrazione") <> OldNumeroRegistrazione Then
                                UltimaRiga = UltimaRiga + 1
                                OldNumeroRegistrazione = MoveFromDbWC(MyRsT, "NumeroRegistrazione")
                            End If
                        Else
                            UltimaRiga = UltimaRiga + 1
                        End If
                        MyRs.Item("Riga") = UltimaRiga
                    Else
                        MyRs.Item("Riga") = 0
                    End If
                    MyRs.Item("NumeroRegistrazione") = MoveFromDbWC(MyRsT, "NumeroRegistrazione")
                    MyRs.Item("DataRegistrazione") = Format(MyRsT.Fields("DataRegistrazione").Value, "dd/MM/yyyy")
                    MyRs.Item("CausaleCodice") = MoveFromDbWC(MyRsT, "CausaleContabile")

                    Dim CampoCausa As New Cls_CausaleContabile

                    CampoCausa.Leggi(Session("DC_TABELLE"), MoveFromDbWC(MyRsT, "CausaleContabile"))

                    Dim NumReg As New Cls_MovimentoContabile

                    NumReg.Leggi(Session("DC_GENERALE"), MoveFromDbWC(MyRsT, "NumeroRegistrazione"))

                    If CampoCausa.TipoDocumento = "FA" Or CampoCausa.TipoDocumento = "NC" Then
                        MyRs.Item("CausaleDescrizione") = CampoCausa.Descrizione & " - " & NumReg.Descrizione & " Fattura n. " & NumReg.NumeroDocumento & " Data Documento : " & NumReg.DataDocumento
                    Else
                        MyRs.Item("CausaleDescrizione") = CampoCausa.Descrizione & " - " & NumReg.Descrizione
                    End If

                    MyRs.Item("DocumentoNumero") = MoveFromDbWC(MyRsT, "NumeroDocumento")
                    MyRs.Item("DocumentoData") = MoveFromDbWC(MyRsT, "DataDocumento")
                    MyRs.Item("Mastro") = MoveFromDbWC(MyRsR, "MastroPartita")
                    MyRs.Item("Conto") = MoveFromDbWC(MyRsR, "ContoPartita")
                    MyRs.Item("Sottoconto") = MoveFromDbWC(MyRsR, "SottocontoPartita")

                    MyRs.Item("Segno") = MoveFromDbWC(MyRsR, "DareAvere")

                    Dim Nconto As New Cls_Pianodeiconti

                    Nconto.Mastro = MoveFromDbWC(MyRsR, "MastroPartita")
                    Nconto.Conto = MoveFromDbWC(MyRsR, "ContoPartita")
                    Nconto.Sottoconto = MoveFromDbWC(MyRsR, "SottocontoPartita")
                    Nconto.Decodfica(Session("DC_GENERALE"))

                    MyRs.Item("ContoDescrizione") = Nconto.Descrizione
                    MyRs.Item("Competenza") = MoveFromDbWC(MyRsT, "Competenza")
                    If MoveFromDbWC(MyRsR, "DareAvere") = "D" Then
                        MyRs.Item("ImportoDare") = campodbn(MoveFromDbWC(MyRsR, "Importo"))
                    Else
                        MyRs.Item("ImportoAvere") = campodbn(MoveFromDbWC(MyRsR, "Importo"))
                    End If
                    MyRs.Item("Descrizione") = MoveFromDbWC(MyRsR, "Descrizione")
                    MyRs.Item("UltimaPagina") = Txt_DallaPagina.Text - 1
                    MyRs.Item("TotaleDare") = TotaleDare
                    MyRs.Item("TotaleAvere") = TotaleAvere
                    MyRs.Item("IntestaSocieta") = WSocieta
                    MyRs.Item("Esercizio") = Txt_Anno.Text                    
                    Stampa.Tables("GiornaleContabilita").Rows.Add(MyRs)

                    If Chk_StampaEffettiva.Checked = True Then
                        MoveToDb(MyRsR.Fields("RigaBollato"), UltimaRiga)
                        MyRsR.Update()
                    End If
                    MyRsR.MoveNext()
                Loop
                MyRsR.Close()
                MyRsT.MoveNext()
            Loop
            MyRsT.Close()
        Else
            MySql = "SELECT MovimentiContabiliTesta.*, MovimentiContabiliRiga.* FROM MovimentiContabiliTesta " & _
                    " INNER JOIN MovimentiContabiliRiga ON MovimentiContabiliTesta.NumeroRegistrazione = MovimentiContabiliRiga.Numero " & _
                    " WHERE RigaBollato >= " & Txt_DallaRiga.Text & _
                    " AND RigaBollato <= " & Txt_AllaRiga.Text & _
                    " AND DataRegistrazione >= {ts '" & Format(DataInizio, "yyyy-MM-dd") & " 00:00:00'} " & _
                    " AND DataRegistrazione <= {ts '" & Format(DataFine, "yyyy-MM-dd") & " 00:00:00'}" & _
                    " ORDER BY MovimentiContabiliRiga.RigaBollato"
            MyRsT.Open(MySql, GeneraleDb, ADODB.CursorTypeEnum.adOpenKeyset, ADODB.LockTypeEnum.adLockOptimistic)
            Do While Not MyRsT.EOF
                Dim MyRs As System.Data.DataRow = Stampa.Tables("GiornaleContabilita").NewRow

                MyRs.Item("Riga") = MoveFromDbWC(MyRsT, "RigaBollato")
                MyRs.Item("NumeroRegistrazione") = MoveFromDbWC(MyRsT, "NumeroRegistrazione")
                MyRs.Item("DataRegistrazione") = Format(MyRsT.Fields("DataRegistrazione").Value, "dd/MM/yyyy")
                MyRs.Item("CausaleCodice") = MoveFromDbWC(MyRsT, "CausaleContabile")


                Dim CampoCausa As New Cls_CausaleContabile

                CampoCausa.Leggi(Session("DC_TABELLE"), MoveFromDbWC(MyRsT, "CausaleContabile"))


                Dim NumReg As New Cls_MovimentoContabile

                NumReg.Leggi(Session("DC_GENERALE"), MoveFromDbWC(MyRsT, "NumeroRegistrazione"))

                MyRs.Item("CausaleDescrizione") = CampoCausa.Descrizione & " - " & NumReg.Descrizione
                MyRs.Item("DocumentoNumero") = MoveFromDbWC(MyRsT, "NumeroDocumento")
                MyRs.Item("DocumentoData") = MoveFromDbWC(MyRsT, "DataDocumento")
                MyRs.Item("Mastro") = MoveFromDbWC(MyRsT, "MastroPartita")
                MyRs.Item("Conto") = MoveFromDbWC(MyRsT, "ContoPartita")
                MyRs.Item("Sottoconto") = MoveFromDbWC(MyRsT, "SottocontoPartita")


                Dim Nconto2 As New Cls_Pianodeiconti

                Nconto2.Mastro = MoveFromDbWC(MyRsT, "MastroPartita")
                Nconto2.Conto = MoveFromDbWC(MyRsT, "ContoPartita")
                Nconto2.Sottoconto = MoveFromDbWC(MyRsT, "SottocontoPartita")
                Nconto2.Decodfica(Session("DC_GENERALE"))

                MyRs.Item("ContoDescrizione") = Nconto2.Descrizione
                MyRs.Item("Competenza") = MoveFromDbWC(MyRsT, "Competenza")
                Dim Importo As Double

                Importo = MoveFromDbWC(MyRsT, "Importo")

                If MoveFromDbWC(MyRsT, "DareAvere") = "D" Then
                    MyRs.Item("ImportoDare") = Importo
                Else
                    MyRs.Item("ImportoAvere") = Importo
                End If
                MyRs.Item("Segno") = MoveFromDbWC(MyRsT, "DareAvere")

                MyRs.Item("Descrizione") = MoveFromDbWC(MyRsT, "Descrizione")
                MyRs.Item("UltimaPagina") = Txt_DallaPagina.Text - 1
                MyRs.Item("TotaleDare") = TotaleDare
                MyRs.Item("TotaleAvere") = TotaleAvere
                MyRs.Item("IntestaSocieta") = WSocieta
                MyRs.Item("Esercizio") = Txt_Anno.Text


                Stampa.Tables("GiornaleContabilita").Rows.Add(MyRs)

                MyRsT.MoveNext()
            Loop
            MyRsT.Close()
        End If        
        GeneraleDb.Close()

        Session("stampa") = Stampa
    End Sub

    Private Sub RimuoviNumeroRiga(ByVal DataInizio As Date, ByVal DataFine As Date)
        Dim MyRs As New ADODB.Recordset
        Dim MyRsUp As New ADODB.Recordset
        Dim MySql As String
        Dim GeneraleDb As New ADODB.Connection

        GeneraleDb.Open(Session("DC_GENERALE"))

        If Txt_AllaRiga.Text = 0 Then
            MySql = "SELECT MovimentiContabiliRiga.* FROM MovimentiContabiliTesta " & _
                    " INNER JOIN MovimentiContabiliRiga ON MovimentiContabiliTesta.NumeroRegistrazione = MovimentiContabiliRiga.Numero " & _
                    " WHERE RigaBollato >= " & Txt_DallaRiga.Text & _
                    " AND DataRegistrazione >= {ts '" & Format(DataInizio, "yyyy-MM-dd") & " 00:00:00'} " & _
                    " AND DataRegistrazione <= {ts '" & Format(DataFine, "yyyy-MM-dd") & " 00:00:00'}" & _
                    " ORDER BY MovimentiContabiliRiga.RigaBollato"
        Else
            MySql = "SELECT MovimentiContabiliRiga.* FROM MovimentiContabiliTesta " & _
                    " INNER JOIN MovimentiContabiliRiga ON MovimentiContabiliTesta.NumeroRegistrazione = MovimentiContabiliRiga.Numero " & _
                    " WHERE RigaBollato >= " & Txt_DallaRiga.Text & _
                    " AND RigaBollato <= " & Txt_AllaRiga.Text & _
                    " AND DataRegistrazione >= {ts '" & Format(DataInizio, "yyyy-MM-dd") & " 00:00:00'} " & _
                    " AND DataRegistrazione <= {ts '" & Format(DataFine, "yyyy-MM-dd") & " 00:00:00'}" & _
                    " ORDER BY MovimentiContabiliRiga.RigaBollato"
        End If
        MyRs.Open(MySql, GeneraleDb, ADODB.CursorTypeEnum.adOpenKeyset, ADODB.LockTypeEnum.adLockOptimistic)
        Do While Not MyRs.EOF
            MySql = "SELECT * FROM MovimentiContabiliRiga " & _
                    " WHERE Numero = " & MoveFromDbWC(MyRs, "Numero") & _
                    " AND RigaBollato = " & MoveFromDbWC(MyRs, "RigaBollato")
            MyRsUp.Open(MySql, GeneraleDb, ADODB.CursorTypeEnum.adOpenKeyset, ADODB.LockTypeEnum.adLockOptimistic)
            If Not MyRsUp.EOF Then
                MoveToDb(MyRsUp.Fields("RigaBollato"), 0)
                MyRsUp.Update()
            End If
            MyRsUp.Close()
            MyRs.MoveNext()
        Loop
        MyRs.Close()
        GeneraleDb.Close()
    End Sub

    Protected Sub Img_Stampa_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles Img_Stampa.Click
        Dim DataInizio As Date
        Dim DataFine As Date
        Dim UltimaPagina As Long
        Dim UltimaRiga As Long
        Dim UltimaData As Date
        Dim MyRs As New ADODB.Recordset
        Dim MySql As String

        Dim DallaData As Date
        Dim AllaData As Date


        If Not IsDate(Txt_DallaData.Text) Then            
            ScriptManager.RegisterStartupScript(Me, Me.GetType(), "visualizzaA", "VisualizzaErrore('Data dalla errata');", True)
            Exit Sub
        End If
        If Not IsDate(Txt_AllaData.Text) Then
            ScriptManager.RegisterStartupScript(Me, Me.GetType(), "visualizzaA", "VisualizzaErrore('Data alla errata');", True)
            Exit Sub
        End If

        DallaData = Txt_DallaData.Text
        AllaData = Txt_AllaData.Text


        Dim TipoGiornale As New Cls_RegistroIVA
        TipoGiornale.LeggiGiornale(Session("DC_TABELLE"))
        Dim Registro As New Cls_RegistriIVAanno
        Registro.Leggi(Session("DC_TABELLE"), TipoGiornale.Tipo, Txt_Anno.Text)

        UltimaPagina = Registro.UltimaPaginaStampata


        Dim DtGen As New Cls_DatiGenerali

        DtGen.LeggiDati(Session("DC_TABELLE"))


        DataInizio = DateSerial(Txt_Anno.Text, DtGen.EsercizioMese, DtGen.EsercizioGiorno)

        DataFine = DateAdd("yyyy", 1, DataInizio)
        DataFine = DateAdd("d", -1, DataFine)

        MySql = "Select Max(RigaBollato) as Massimo FROM MovimentiContabiliTesta INNER JOIN MovimentiContabiliRiga ON MovimentiContabiliTesta.NumeroRegistrazione = MovimentiContabiliRiga.Numero Where DataRegistrazione >= {ts '" & Format(DataInizio, "yyyy-MM-dd") & " 00:00:00'} And DataRegistrazione <= {ts '" & Format(DataFine, "yyyy-MM-dd") & " 00:00:00'}"

        Dim cnT As OleDbConnection

        cnT = New Data.OleDb.OleDbConnection(Session("DC_GENERALE"))

        cnT.Open()

        Dim cmdX As New OleDbCommand()
        cmdX.CommandText = (MySql)
        cmdX.Connection = cnT
        Dim ReadDX As OleDbDataReader = cmdX.ExecuteReader()
        If ReadDX.Read Then
            UltimaRiga = Val(campodb(ReadDX.Item("Massimo")))
        End If
        ReadDX.Close()

        MySql = "Select Max(DataRegistrazione) as Massimo FROM MovimentiContabiliTesta INNER JOIN MovimentiContabiliRiga ON MovimentiContabiliTesta.NumeroRegistrazione = MovimentiContabiliRiga.Numero Where RigaBollato > 0 And DataRegistrazione >= ? And DataRegistrazione <= ?"

        Dim cmdX1 As New OleDbCommand()
        cmdX1.CommandText = (MySql)
        cmdX1.Connection = cnT
        cmdX1.Parameters.AddWithValue("@DataInizio",DataInizio)
        cmdX1.Parameters.AddWithValue("@DAtaFine", DataFine)
        Dim ReadDX1 As OleDbDataReader = cmdX1.ExecuteReader()
        If ReadDX1.Read Then
            If IsDBNull(ReadDX1.Item("Massimo")) Then
                UltimaData = DataInizio
            Else
  
                If campodb(ReadDX1.Item("Massimo")) <> "" Then
                    Dim AppoggioData As Date = campodb(ReadDX1.Item("Massimo"))

                    UltimaData = DateAdd(DateInterval.Day, 1, AppoggioData)
                Else
                    UltimaData = DateSerial(Year(Now), 1, 1)
                End If
            End If
        End If
        ReadDX1.Close()

        
        If Val(Txt_DallaRiga.Text) > UltimaRiga + 1 Then
            REM ClientScript.RegisterClientScriptBlock(Me.GetType(), "Errore", "VisualizzaErrore('Riga di [Stampare dalla] errata');", True)
            ScriptManager.RegisterStartupScript(Me, Me.GetType(), "visualizzaA", "VisualizzaErrore('Riga di [Stampare dalla] " & UltimaData & "  errata');", True)
            Exit Sub
        End If

        If Val(Txt_DallaPagina.Text) = 0 Then
            ScriptManager.RegisterStartupScript(Me, Me.GetType(), "visualizzaA", "VisualizzaErrore('Pagina di [Stampare dalla] errata');", True)
            Exit Sub
        End If
        If Val(Txt_DallaPagina.Text) > UltimaPagina + 1 Then
            REM ClientScript.RegisterClientScriptBlock(Me.GetType(), "Errore", "VisualizzaErrore('Pagina di [Stampare dalla] errata');", True)
            ScriptManager.RegisterStartupScript(Me, Me.GetType(), "visualizzaA", "VisualizzaErrore('Pagina di [Stampare dalla] errata');", True)
            Exit Sub
        End If
        If Format(DallaData, "yyyyMMdd") > Format(UltimaData, "yyyyMMdd") Then
            REM ClientScript.RegisterClientScriptBlock(Me.GetType(), "Errore", "VisualizzaErrore('Data di [Stampare dalla] errata');", True)
            ScriptManager.RegisterStartupScript(Me, Me.GetType(), "visualizzaA", "VisualizzaErrore('Data di [Stampare dalla] errata');", True)
            'MySql
            Chk_Rimuovi.Text = MySql
            Exit Sub
        End If
     
        If Val(Txt_AllaRiga.Text) > UltimaRiga Then
            REM ClientScript.RegisterClientScriptBlock(Me.GetType(), "Errore", "VisualizzaErrore('Riga di [Stampare alla] errata');", True)
            ScriptManager.RegisterStartupScript(Me, Me.GetType(), "visualizzaA", "VisualizzaErrore('Riga di [Stampare alla] errata');", True)
            Exit Sub
        End If
        If Format(AllaData, "yyyyMMdd") > Format(DataFine, "yyyyMMdd") Then
            REM ClientScript.RegisterClientScriptBlock(Me.GetType(), "Errore", "VisualizzaErrore('Data di [Stampare alla] errata');", True)
            ScriptManager.RegisterStartupScript(Me, Me.GetType(), "visualizzaA", "VisualizzaErrore('Data di [Stampare alla] errata');", True)
            Exit Sub
        End If
        If Val(Txt_AllaRiga.Text) <> 0 Then
            If Math.Round(CDbl(Txt_AllaRiga.Text), 2) < Math.Round(CDbl(Txt_DallaRiga.Text), 2) Then
                REM ClientScript.RegisterClientScriptBlock(Me.GetType(), "Errore", "VisualizzaErrore('Valori dei campi Riga... incongruenti');", True)
                ScriptManager.RegisterStartupScript(Me, Me.GetType(), "visualizzaA", "VisualizzaErrore('Valori dei campi Riga... incongruenti');", True)
                Exit Sub
            End If
        End If
        If Format(AllaData, "yyyyMMdd") < Format(DallaData, "yyyyMMdd") Then
            REM ClientScript.RegisterClientScriptBlock(Me.GetType(), "Errore", "VisualizzaErrore('Valori dei campi Data... incongruenti');", True)
            ScriptManager.RegisterStartupScript(Me, Me.GetType(), "visualizzaA", "VisualizzaErrore('Valori dei campi Data... incongruenti');", True)
            Exit Sub
        End If


        If Chk_Rimuovi.Checked = True Then
            Call RimuoviNumeroRiga(DataInizio, DataFine)

        Else
            Call CreaRecordStampa(DataInizio, DataFine)

            If Chk_StampaEffettiva.Checked = True Then
                Dim TipoGiornaleY As New Cls_RegistroIVA
                TipoGiornaleY.LeggiGiornale(Session("DC_TABELLE"))
                Dim RegistroY As New Cls_RegistriIVAanno
                RegistroY.Leggi(Session("DC_TABELLE"), TipoGiornale.Tipo, Txt_Anno.Text)

                RegistroY.DataStampa = Txt_AllaData.Text

                RegistroY.UltimaPaginaStampata = RegistroY.UltimaPaginaStampata + RecuperaNumeroPagina()

                RegistroY.Scrivi(Session("DC_TABELLE"), RegistroY.Tipo, RegistroY.Anno)
            End If


          
            'Response.Redirect("StampaReport.aspx?REPORT=GIORNALECONTABILITA")
            'Session("SelectionFormula") = "{GiornaleContabilita.PRINTERKEY} = " & Chr(34) & ViewState("PRINTERKEY") & Chr(34)

            ScriptManager.RegisterClientScriptBlock(Me, Me.GetType(), "Stampa", "openPopUp('Stampa_ReportXSD.aspx?REPORT=GIORNALECONTABILITA&PRINTERKEY=OFF','Stampe','width=800,height=600');", True)
        End If

    End Sub


    Private Function RecuperaNumeroPagina() As Integer
        Dim XS As New Cls_Login

        XS.Utente = Session("UTENTE")
        XS.LeggiSP(Application("SENIOR"))


        Dim rpt As New ReportDocument

        rpt.Load(HostingEnvironment.ApplicationPhysicalPath() & "Report\" & XS.ReportPersonalizzato("GIORNALECONTABILITA"))

        rpt.SetDataSource(Session("stampa"))
        
        rpt.Refresh()

        Try
            rpt.ExportToDisk(ExportFormatType.PortableDocFormat, HostingEnvironment.ApplicationPhysicalPath() & "Public\" & Session("NomeEPersonam") & "_Protocollo.pdf")



            RecuperaNumeroPagina = rpt.FormatEngine.GetLastPageNumber(New CrystalDecisions.Shared.ReportPageRequestContext())
        Finally
            Kill(HostingEnvironment.ApplicationPhysicalPath() & "Public\" & Session("NomeEPersonam") & "_Protocollo.pdf")
        End Try
    End Function
    Function campodb(ByVal oggetto As Object) As String


        If IsDBNull(oggetto) Then
            Return ""
        Else
            Return oggetto
        End If
    End Function

    Function campodbn(ByVal oggetto As Object) As Double


        If IsDBNull(oggetto) Then
            Return 0
        Else
            Return oggetto
        End If
    End Function

    Function campodbD(ByVal oggetto As Object) As DATE

        If IsDBNull(oggetto) Then
            Return NOTHING
        Else
            Return oggetto
        End If
    End Function


    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Trim(Session("UTENTE")) = "" Then
            Response.Redirect("../Login.aspx")
            Exit Sub
        End If

        
        Dim MyJs As String
        MyJs = "$(" & Chr(34) & "#" & Txt_DallaData.ClientID & Chr(34) & ").mask(""99/99/9999""); "
        MyJs = MyJs & " $(" & Chr(34) & "#" & Txt_DallaData.ClientID & Chr(34) & ").datepicker({ changeMonth: true,changeYear: true,  yearRange: ""1890:" & Year(Now) + 1 & """},$.datepicker.regional[""it""]);"
        ScriptManager.RegisterStartupScript(Me, Me.GetType(), "FormatDataDal", MyJs, True)
        MyJs = "$(" & Chr(34) & "#" & Txt_AllaData.ClientID & Chr(34) & ").mask(""99/99/9999""); "
        MyJs = MyJs & " $(" & Chr(34) & "#" & Txt_AllaData.ClientID & Chr(34) & ").datepicker({ changeMonth: true,changeYear: true,  yearRange: ""1890:" & Year(Now) + 1 & """},$.datepicker.regional[""it""]);"
        ScriptManager.RegisterStartupScript(Me, Me.GetType(), "FormatDataAl", MyJs, True)

        If Page.IsPostBack = True Then
            Exit Sub
        End If



        Dim K1 As New Cls_SqlString

        Dim Barra As New Cls_BarraSenior

        If Not IsNothing(Session("RicercaAnagraficaSQLString")) Then
            Try
                K1 = Session("RicercaAnagraficaSQLString")
            Catch ex As Exception

            End Try
        End If

        Lbl_BarraSenior.Text = Barra.CodiceBarra(Application("SENIOR"), Session("UTENTE"), Page, K1)

        Dim DataInizio As Date
        Dim DataFine As Date
        Dim UltimaPagina As Long
        Dim UltimaRiga As Long
        Dim UltimaData As Date
        Dim MyRs As New ADODB.Recordset
        Dim MySql As String

        Txt_Anno.Text = Year(Now)

        Dim TipoGiornale As New Cls_RegistroIVA
        TipoGiornale.LeggiGiornale(Session("DC_TABELLE"))
        Dim Registro As New Cls_RegistriIVAanno
        Registro.Leggi(Session("DC_TABELLE"), TipoGiornale.Tipo, Txt_Anno.Text)

        UltimaPagina = Registro.UltimaPaginaStampata


        Dim DtGen As New Cls_DatiGenerali

        DtGen.LeggiDati(Session("DC_TABELLE"))


        DataInizio = DateSerial(Txt_Anno.Text, DtGen.EsercizioMese, DtGen.EsercizioGiorno)

        DataFine = DateAdd("yyyy", 1, DataInizio)
        DataFine = DateAdd("d", -1, DataFine)

        MySql = "Select Max(RigaBollato) as Massimo FROM MovimentiContabiliTesta INNER JOIN MovimentiContabiliRiga ON MovimentiContabiliTesta.NumeroRegistrazione = MovimentiContabiliRiga.Numero Where DataRegistrazione >= {ts '" & Format(DataInizio, "yyyy-MM-dd") & " 00:00:00'} And DataRegistrazione <= {ts '" & Format(DataFine, "yyyy-MM-dd") & " 00:00:00'}"

        Dim cnT As OleDbConnection

        cnT = New Data.OleDb.OleDbConnection(Session("DC_GENERALE"))

        cnT.Open()

        Dim cmdX As New OleDbCommand()
        cmdX.CommandText = (MySql)
        cmdX.Connection = cnT
        Dim ReadDX As OleDbDataReader = cmdX.ExecuteReader()
        If ReadDX.Read Then
            UltimaRiga = val(campodb(ReadDX.Item("Massimo")))
        End If
        ReadDX.Close()

        MySql = "Select Max(DataRegistrazione) as Massimo FROM MovimentiContabiliTesta INNER JOIN MovimentiContabiliRiga ON MovimentiContabiliTesta.NumeroRegistrazione = MovimentiContabiliRiga.Numero Where RigaBollato > 0 And DataRegistrazione >= {ts '" & Format(DataInizio, "yyyy-MM-dd") & " 00:00:00'} And DataRegistrazione <= {ts '" & Format(DataFine, "yyyy-MM-dd") & " 00:00:00'}"


        Dim cmdX1 As New OleDbCommand()
        cmdX1.CommandText = (MySql)
        cmdX1.Connection = cnT
        Dim ReadDX1 As OleDbDataReader = cmdX.ExecuteReader()
        If ReadDX1.Read Then
            If IsDBNull(ReadDX1.Item("Massimo")) Then
                UltimaData = DataInizio
            Else
                If campodb(ReadDX1.Item("Massimo")) = 0 Then
                    UltimaData = DataInizio
                Else
                    IF campodb(ReadDX1.Item("Massimo")) ="" THEN
                       UltimaData = campodb(ReadDX1.Item("Massimo"))
		     ELSE
		       UltimaData = DATESERIAL(YEAR(NOW),1,1)
                    END IF 
                End If
            End If
        End If
        ReadDX1.Close()

        cnT.Close()

        Txt_DallaRiga.Text = UltimaRiga + 1
        Txt_DallaPagina.Text = UltimaPagina + 1
        Txt_DallaData.Text = Format(UltimaData, "dd/MM/yyyy")

        Txt_AllaRiga.Text = 0
        If DataFine > Now Then
            Txt_AllaData.Text = Format(Now, "dd/MM/yyyy")
        Else
            Txt_AllaData.Text = Format(DataFine, "dd/MM/yyyy")
        End If

    End Sub

    Protected Sub Btn_Esci_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles Btn_Esci.Click
        Response.Redirect("Menu_Stampe.aspx")

    End Sub

    Private Sub UpDateCampi()
        Dim DataInizio As Date
        Dim DataFine As Date
        Dim UltimaPagina As Long
        Dim UltimaRiga As Long
        Dim UltimaData As Date
        Dim MyRs As New ADODB.Recordset
        Dim MySql As String


        Dim TipoGiornale As New Cls_RegistroIVA
        TipoGiornale.LeggiGiornale(Session("DC_TABELLE"))
        Dim Registro As New Cls_RegistriIVAanno
        Registro.Leggi(Session("DC_TABELLE"), TipoGiornale.Tipo, Txt_Anno.Text)

        UltimaPagina = Registro.UltimaPaginaStampata


        Dim DtGen As New Cls_DatiGenerali

        DtGen.LeggiDati(Session("DC_TABELLE"))


        DataInizio = DateSerial(Txt_Anno.Text, DtGen.EsercizioMese, DtGen.EsercizioGiorno)

        DataFine = DateAdd("yyyy", 1, DataInizio)
        DataFine = DateAdd("d", -1, DataFine)

        MySql = "Select Max(RigaBollato) as Massimo FROM MovimentiContabiliTesta INNER JOIN MovimentiContabiliRiga ON MovimentiContabiliTesta.NumeroRegistrazione = MovimentiContabiliRiga.Numero Where DataRegistrazione >= {ts '" & Format(DataInizio, "yyyy-MM-dd") & " 00:00:00'} And DataRegistrazione <= {ts '" & Format(DataFine, "yyyy-MM-dd") & " 00:00:00'}"

        Dim cnT As OleDbConnection

        cnT = New Data.OleDb.OleDbConnection(Session("DC_GENERALE"))

        cnT.Open()

        Dim cmdX As New OleDbCommand()
        cmdX.CommandText = (MySql)
        cmdX.Connection = cnT
        Dim ReadDX As OleDbDataReader = cmdX.ExecuteReader()
        If ReadDX.Read Then
            UltimaRiga = Val(campodb(ReadDX.Item("Massimo")))
        End If
        ReadDX.Close()

        MySql = "Select Max(DataRegistrazione) as Massimo FROM MovimentiContabiliTesta INNER JOIN MovimentiContabiliRiga ON MovimentiContabiliTesta.NumeroRegistrazione = MovimentiContabiliRiga.Numero Where RigaBollato > 0 And DataRegistrazione >= {ts '" & Format(DataInizio, "yyyy-MM-dd") & " 00:00:00'} And DataRegistrazione <= {ts '" & Format(DataFine, "yyyy-MM-dd") & " 00:00:00'}"


        Dim cmdX1 As New OleDbCommand()
        cmdX1.CommandText = (MySql)
        cmdX1.Connection = cnT
        Dim ReadDX1 As OleDbDataReader = cmdX.ExecuteReader()
        If ReadDX1.Read Then
            If IsDBNull(ReadDX1.Item("Massimo")) Then
                UltimaData = DataInizio
            Else
                If campodb(ReadDX1.Item("Massimo")) = 0 Then
                    UltimaData = DataInizio
                Else
                    If campodb(ReadDX1.Item("Massimo")) = "" Then
                        UltimaData = campodb(ReadDX1.Item("Massimo"))
                    Else
                        UltimaData = DateSerial(Year(Now), 1, 1)
                    End If
                End If
            End If
        End If
        ReadDX1.Close()

        cnT.Close()

        Txt_DallaRiga.Text = UltimaRiga + 1
        Txt_DallaPagina.Text = UltimaPagina + 1
        Txt_DallaData.Text = Format(Registro.DataStampa, "dd/MM/yyyy")

        Txt_AllaRiga.Text = 0
        If DataFine > Now Then
            Txt_AllaData.Text = Format(Now, "dd/MM/yyyy")
        Else
            Txt_AllaData.Text = Format(DataFine, "dd/MM/yyyy")
        End If

    End Sub

    Protected Sub Txt_Anno_TextChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles Txt_Anno.TextChanged
        Call UpDateCampi()
    End Sub
End Class
