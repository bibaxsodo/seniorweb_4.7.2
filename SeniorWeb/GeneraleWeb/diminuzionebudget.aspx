﻿<%@ Page Language="VB" AutoEventWireup="false" Inherits="GeneraleWeb_diminuzionebudget" CodeFile="diminuzionebudget.aspx.vb" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
    <meta http-equiv="x-ua-compatible" content="IE=9" />
    <title>Aliquota IVA</title>
    <asp:PlaceHolder runat="server">
        <%: System.Web.Optimization.Styles.Render("~/Content/AjaxControlToolkit/Styles/Bundle") %>
    </asp:PlaceHolder>
    <link href="ospiti.css?ver=11" rel="stylesheet" type="text/css" />
    <link rel="shortcut icon" href="images/SENIOR.ico" />
    <link href="js/jquery.autocomplete.css" rel="stylesheet" type="text/css" />

    <script src="js/jquery-1.5.1.min.js" type="text/javascript"></script>
    <script src="js/jquery.autocomplete.js" type="text/javascript"></script>
    <script src="js/soapclient.js" type="text/javascript"></script>
    <script src="/js/convertinumero.js" type="text/javascript"></script>

    <script src="js/jquery.maskedinput-1.3.min.js" type="text/javascript"></script>
    <script src="/js/formatnumer.js" type="text/javascript"></script>

    <script src="/js/pianoconti.js" type="text/javascript"></script>
    <script src="js/JSErrore.js" type="text/javascript"></script>
    <link rel="stylesheet" href="jqueryui/jquery-ui.css" type="text/css" />
    <script src="jqueryui/jquery-ui.js" type="text/javascript"></script>
    <script src="jqueryui/datapicker-it.js" type="text/javascript"></script>
</head>
<body>
    <form id="form1" runat="server">
        <asp:ScriptManager ID="ScriptManager1" runat="server" EnableScriptGlobalization="true">
            <Scripts>
                <asp:ScriptReference Path="~/Scripts/AjaxControlToolkit/Bundle" />
            </Scripts>
        </asp:ScriptManager>
        <asp:Label ID="Lbl_BarraSenior" runat="server" Text=""></asp:Label>
        <div style="text-align: right;">
            <asp:ImageButton ID="Btn_Modifica" runat="server" ImageUrl="~/images/salva.jpg" class="EffettoBottoniTondi" />
        </div>
        <div style="text-align: left;">
            <asp:GridView ID="Grid" runat="server" CellPadding="3" Height="60px" Width="590px"
                ShowFooter="True" BackColor="White" BorderColor="#CCCCCC" BorderStyle="None"
                BorderWidth="1px">
                <RowStyle ForeColor="#000066" />
                <Columns>
                    <asp:CommandField ButtonType="Image" ShowDeleteButton="True" DeleteImageUrl="~/images/cancella.png" />
                    <asp:CommandField ButtonType="Image" EditImageUrl="~/images/modifica.png" ShowEditButton="True" CancelImageUrl="~/images/annulla.png" UpdateImageUrl="~/images/aggiorna.png" />


                    <asp:TemplateField HeaderText="Anno">
                        <EditItemTemplate>
                            <asp:TextBox ID="TxtAnno" MaxLength="4" autocomplete="off" onkeypress="return soloNumeri(event);" runat="server" Width="60px"></asp:TextBox>
                        </EditItemTemplate>
                        <ItemTemplate>
                            <asp:Label ID="LblAnno" runat="server" Text='<%# Eval("Anno") %>' Width="60px"></asp:Label>
                        </ItemTemplate>
                        <FooterTemplate>
                            <div style="text-align: left">
                                <asp:ImageButton ID="ImageButton1" ImageUrl="~/images/inserisci.png" CommandName="Inserisci" runat="server" ToolTip="Inserisci (F9)" />
                            </div>
                        </FooterTemplate>
                    </asp:TemplateField>

                    <asp:TemplateField HeaderText="Data">
                        <EditItemTemplate>
                            <asp:TextBox ID="TxtData" autocomplete="off" runat="server" Width="90px"></asp:TextBox>
                        </EditItemTemplate>
                        <ItemTemplate>
                            <asp:Label ID="LblData" runat="server" Text='<%# Eval("Data") %>' Width="80px"></asp:Label>
                        </ItemTemplate>
                    </asp:TemplateField>

                    <asp:TemplateField HeaderText="Importo">
                        <EditItemTemplate>
                            <asp:TextBox ID="TxtImporto" autocomplete="off" onkeypress="ForceNumericInput(this, true, true)" Width="136px" runat="server"></asp:TextBox>
                        </EditItemTemplate>
                        <ItemTemplate>
                            <asp:Label ID="LblImporto" runat="server" Text='<%# Eval("Importo") %>' Width="136px"></asp:Label>
                        </ItemTemplate>
                    </asp:TemplateField>

                    <asp:TemplateField HeaderText="Descrizione">
                        <EditItemTemplate>
                            <asp:TextBox ID="TxtDescrizione" autocomplete="off" runat="server" Width="300px"></asp:TextBox>
                        </EditItemTemplate>
                        <ItemTemplate>
                            <asp:Label ID="LblDescrizione" runat="server" Text='<%# Eval("Descrizione") %>' Width="300px"></asp:Label>
                        </ItemTemplate>
                    </asp:TemplateField>
                </Columns>
                <FooterStyle BackColor="White" ForeColor="#000066" />
                <PagerStyle BackColor="White" ForeColor="#000066" HorizontalAlign="Left" />
                <SelectedRowStyle BackColor="#669999" Font-Bold="True" ForeColor="White" />
                <HeaderStyle BackColor="#006699" Font-Bold="True" ForeColor="White" />
            </asp:GridView>
        </div>
    </form>
</body>
</html>
