﻿Imports System
Imports System.Web
Imports System.Data.OleDb
Imports System.Configuration
Imports System.Text
Imports System.Web.Hosting

Partial Class causalicontabili
    Inherits System.Web.UI.Page
    Dim Tabella As New System.Data.DataTable("tabella")

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Trim(Session("UTENTE")) = "" Then
            
            Response.Redirect("/SeniorWeb/Login.aspx")
            Exit Sub
        End If


        If Page.IsPostBack = True Then Exit Sub


        Dim K1 As New Cls_SqlString

        Dim Barra As New Cls_BarraSenior

        If Not IsNothing(Session("RicercaGeneraleSQLString")) Then
            K1 = Session("RicercaGeneraleSQLString")
        End If

        Lbl_BarraSenior.Text = Barra.CodiceBarra(Application("SENIOR"), Session("UTENTE"), Page, K1)




        'Lbl_Prorata.Text = "Prorata"
        'If Session("TIPOAPP") = "RSA" Then            
        '    RB_ProrataSI.Visible = True
        '    RB_ProrataNO.Visible = True
        'Else
        '    Lbl_Prorata.Text = ""
        '    RB_ProrataSI.Visible = False
        '    RB_ProrataNO.Visible = False
        'End If

        Dim x As New Cls_CausaleContabile

        x.UpDateDropBox(Session("DC_TABELLE"), DD_CausaleIncasso)

        x.UpDateDropBox(Session("DC_TABELLE"), DD_CausaleDocumento)
        x.UpDateDropBox(Session("DC_TABELLE"), DD_CausaleGiroconto)


        Dim M As New Cls_RegistroIVA

        M.UpDateDropBox(Session("DC_TABELLE"), DD_RegistroIVA)


        Dim xd As New ClsDetraibilita
        xd.UpDateDropBox(Session("DC_TABELLE"), DD_Detraibilita)

        Dim MyRt As New ClsRitenuta

        MyRt.UpDateDropBox(Session("DC_TABELLE"), DD_Ritenuta)

        Dim CodiciIva As New Cls_IVA

        CodiciIva.UpDateDropBox(Session("DC_TABELLE"), DD_IVA)




        DD_Tipo.Items.Clear()
        DD_Tipo.Items.Add("IVA")
        DD_Tipo.Items(DD_Tipo.Items.Count - 1).Value = "I"

        DD_Tipo.Items.Add("Conto Generico")
        DD_Tipo.Items(DD_Tipo.Items.Count - 1).Value = "C"

        DD_Tipo.Items.Add("Incasso/Pagamento")
        DD_Tipo.Items(DD_Tipo.Items.Count - 1).Value = "P"

        DD_Tipo.Items.Add("Giroconto Anticipi")
        DD_Tipo.Items(DD_Tipo.Items.Count - 1).Value = "G"


        If Session("TIPOAPP") = "RSA" Then
            DD_Tipo.Items.Add("Retta")
            DD_Tipo.Items(DD_Tipo.Items.Count - 1).Value = "R"
        End If
        DD_Tipo.Items.Add("")
        DD_Tipo.Items(DD_Tipo.Items.Count - 1).Value = ""
        DD_Tipo.Items(DD_Tipo.Items.Count - 1).Selected = True


        DD_TipoDocumento.Items.Add("")
        DD_TipoDocumento.Items(DD_TipoDocumento.Items.Count - 1).Value = ""

        If Request.Item("CODICE") <> "" Then
            Call CaricaCausale()
        Else            
            Txt_Codice.text = x.MaxCodice(Session("DC_TABELLE"))
        End If

        Call EseguiJS()
    End Sub

    Private Sub ImpostaGriglia()
        Tabella.Clear()
        Tabella.Columns.Clear()
        Tabella.Columns.Add("Descrizione", GetType(String))
        Tabella.Columns.Add("Sottoconto", GetType(String))        
        Tabella.Columns.Add("DareAvere", GetType(String))
        Tabella.Columns.Add("Riga", GetType(Long))
        If DD_Tipo.SelectedValue = "I" Then

            DD_TipoDocumento.Items.Clear()
            DD_TipoDocumento.Items.Add("Fattura")
            DD_TipoDocumento.Items(DD_TipoDocumento.Items.Count - 1).Value = "FA"
            DD_TipoDocumento.Items.Add("Nota Credito")
            DD_TipoDocumento.Items(DD_TipoDocumento.Items.Count - 1).Value = "NC"
            DD_TipoDocumento.Items.Add("Nota Debito")
            DD_TipoDocumento.Items(DD_TipoDocumento.Items.Count - 1).Value = "ND"
            DD_TipoDocumento.Items.Add("Corrispettivo")
            DD_TipoDocumento.Items(DD_TipoDocumento.Items.Count - 1).Value = "CR"

            DD_TipoDocumento.Items.Add("")
            DD_TipoDocumento.Items(DD_TipoDocumento.Items.Count - 1).Value = ""

            DD_TipoDocumento.Enabled = True

            Dim myriga1 As System.Data.DataRow = Tabella.NewRow()
            myriga1(0) = "Cliente/Fornitore/Cassa"
            myriga1(3) = 1
            Tabella.Rows.Add(myriga1)

            Dim myriga2 As System.Data.DataRow = Tabella.NewRow()
            myriga2(0) = "Costo/Ricavo"
            myriga2(3) = 2
            Tabella.Rows.Add(myriga2)

            Dim myriga3 As System.Data.DataRow = Tabella.NewRow()
            myriga3(0) = "Iva Vendite/Acquisti"
            myriga3(3) = 3
            Tabella.Rows.Add(myriga3)

            Dim myriga4 As System.Data.DataRow = Tabella.NewRow()
            myriga4(0) = "Iva Non Detraibile"
            myriga4(3) = 4
            Tabella.Rows.Add(myriga4)

            Dim myriga5 As System.Data.DataRow = Tabella.NewRow()
            myriga5(0) = "Scadenza Collaboratori"
            myriga5(3) = 5
            Tabella.Rows.Add(myriga5)

            Dim myriga6 As System.Data.DataRow = Tabella.NewRow()
            myriga6(0) = ""
            myriga6(3) = 6
            Tabella.Rows.Add(myriga6)

            Dim myriga7 As System.Data.DataRow = Tabella.NewRow()
            myriga7(0) = "Abbuoni/Sconti"
            myriga7(3) = 7
            Tabella.Rows.Add(myriga7)

            Dim myriga8 As System.Data.DataRow = Tabella.NewRow()
            myriga8(0) = "Arrotondamenti"
            myriga8(3) = 8
            Tabella.Rows.Add(myriga8)

            Dim myriga9 As System.Data.DataRow = Tabella.NewRow()
            myriga9(0) = "Ritenute"
            myriga9(3) = 9
            Tabella.Rows.Add(myriga9)

            Dim myriga10 As System.Data.DataRow = Tabella.NewRow()
            myriga10(0) = "Contributi Ditta"
            myriga10(3) = 10
            Tabella.Rows.Add(myriga10)

            Dim myriga11 As System.Data.DataRow = Tabella.NewRow()
            myriga11(0) = "Costo Indeducibile"
            myriga11(3) = 11
            Tabella.Rows.Add(myriga11)

            ViewState("Tabella") = Tabella
        End If
        If DD_Tipo.SelectedValue = "R" Then

            DD_TipoDocumento.Items.Clear()
            DD_TipoDocumento.Items.Add("Retta")
            DD_TipoDocumento.Items(DD_TipoDocumento.Items.Count - 1).Value = "RE"
            DD_TipoDocumento.Items.Add("Nota Credito")
            DD_TipoDocumento.Items(DD_TipoDocumento.Items.Count - 1).Value = "NC"
            DD_TipoDocumento.Items.Add("Nota Debito")
            DD_TipoDocumento.Items(DD_TipoDocumento.Items.Count - 1).Value = "NB"
            DD_TipoDocumento.Items.Add("Corrispettivo")
            DD_TipoDocumento.Items(DD_TipoDocumento.Items.Count - 1).Value = "CR"

            DD_TipoDocumento.Items.Add("")
            DD_TipoDocumento.Items(DD_TipoDocumento.Items.Count - 1).Value = ""

            DD_TipoDocumento.Enabled = True

            Dim myriga1 As System.Data.DataRow = Tabella.NewRow()
            myriga1(0) = "Debitore/Cassa"
            myriga1(3) = 1
            Tabella.Rows.Add(myriga1)

            Dim myriga2 As System.Data.DataRow = Tabella.NewRow()
            myriga2(0) = "IVA"
            myriga2(3) = 2
            Tabella.Rows.Add(myriga2)

            Dim myriga3 As System.Data.DataRow = Tabella.NewRow()
            myriga3(0) = "Retta"
            myriga3(3) = 3
            Tabella.Rows.Add(myriga3)

            Dim myriga4 As System.Data.DataRow = Tabella.NewRow()
            myriga4(0) = "Extra fissi"
            myriga4(3) = 4
            Tabella.Rows.Add(myriga4)

            Dim myriga5 As System.Data.DataRow = Tabella.NewRow()
            myriga5(0) = "Extra variabili"
            myriga5(3) = 5
            Tabella.Rows.Add(myriga5)

            Dim myriga6 As System.Data.DataRow = Tabella.NewRow()
            myriga6(0) = "Accrediti"
            myriga6(3) = 6
            Tabella.Rows.Add(myriga6)

            Dim myriga7 As System.Data.DataRow = Tabella.NewRow()
            myriga7(0) = "Arrotondamenti"
            myriga7(3) = 7
            Tabella.Rows.Add(myriga7)

            Dim myriga8 As System.Data.DataRow = Tabella.NewRow()
            myriga8(0) = "Anticipi"
            myriga8(3) = 8
            Tabella.Rows.Add(myriga8)

            Dim myriga9 As System.Data.DataRow = Tabella.NewRow()
            myriga9(0) = "Bollo"
            myriga9(3) = 9
            Tabella.Rows.Add(myriga9)

            Dim myriga10 As System.Data.DataRow = Tabella.NewRow()
            myriga10(0) = "Giroconto Anticipi"
            myriga10(3) = 10
            Tabella.Rows.Add(myriga10)

            Dim myriga11 As System.Data.DataRow = Tabella.NewRow()
            myriga11(0) = "Giroconto Anticipi"
            myriga11(3) = 11
            Tabella.Rows.Add(myriga11)


            Dim myriga12 As System.Data.DataRow = Tabella.NewRow()
            myriga12(0) = "Sconto"
            myriga12(3) = 12
            Tabella.Rows.Add(myriga12)


            Dim myriga13 As System.Data.DataRow = Tabella.NewRow()
            myriga13(0) = "Retta 2"
            myriga13(3) = 13
            Tabella.Rows.Add(myriga13)

            Dim myriga14 As System.Data.DataRow = Tabella.NewRow()
            myriga14(0) = "Retta Percentuale"
            myriga14(3) = 14
            Tabella.Rows.Add(myriga14)


            ViewState("Tabella") = Tabella
        End If

        If DD_Tipo.SelectedValue = "G" Then
            DD_TipoDocumento.Items.Clear()
            DD_TipoDocumento.Items.Add("Retta")
            DD_TipoDocumento.Items(DD_TipoDocumento.Items.Count - 1).Value = "RE"
            DD_TipoDocumento.Items.Add("Nota Credito")
            DD_TipoDocumento.Items(DD_TipoDocumento.Items.Count - 1).Value = "NC"
            DD_TipoDocumento.Items.Add("Nota Debito")
            DD_TipoDocumento.Items(DD_TipoDocumento.Items.Count - 1).Value = "ND"

            DD_TipoDocumento.Items.Add("")
            DD_TipoDocumento.Items(DD_TipoDocumento.Items.Count - 1).Value = ""

            DD_TipoDocumento.Enabled = True

            Dim myriga1 As System.Data.DataRow = Tabella.NewRow()
            myriga1(0) = "Retta"
            myriga1(3) = 1
            Tabella.Rows.Add(myriga1)

            Dim myriga2 As System.Data.DataRow = Tabella.NewRow()
            myriga2(0) = "Extra fissi"
            myriga2(3) = 2
            Tabella.Rows.Add(myriga2)

            Dim myriga3 As System.Data.DataRow = Tabella.NewRow()
            myriga3(0) = "Extra variabili"
            myriga3(3) = 3
            Tabella.Rows.Add(myriga3)

            Dim myriga4 As System.Data.DataRow = Tabella.NewRow()
            myriga4(0) = "Accrediti"
            myriga4(3) = 4
            Tabella.Rows.Add(myriga4)

            Dim myriga5 As System.Data.DataRow = Tabella.NewRow()
            myriga5(0) = "Anticipi"
            myriga5(3) = 5
            Tabella.Rows.Add(myriga5)

            Dim myriga6 As System.Data.DataRow = Tabella.NewRow()
            myriga6(0) = "Accrediti"
            myriga6(3) = 6
            Tabella.Rows.Add(myriga6)

            Dim myriga7 As System.Data.DataRow = Tabella.NewRow()
            myriga7(0) = "Arrotondamenti"
            myriga7(3) = 7
            Tabella.Rows.Add(myriga7)

            ViewState("Tabella") = Tabella
        End If

        If DD_Tipo.SelectedValue = "P" Then
            DD_TipoDocumento.Items.Clear()
            DD_TipoDocumento.Items.Add("Bonifico")
            DD_TipoDocumento.Items(DD_TipoDocumento.Items.Count - 1).Value = 0
            DD_TipoDocumento.Items.Add("Storno")
            DD_TipoDocumento.Items(DD_TipoDocumento.Items.Count - 1).Value = 1

            DD_TipoDocumento.Items.Add("")
            DD_TipoDocumento.Items(DD_TipoDocumento.Items.Count - 1).Value = ""
            DD_TipoDocumento.Enabled = True

            Dim myriga1 As System.Data.DataRow = Tabella.NewRow()
            myriga1(0) = "Cliente/Fornitore"
            myriga1(3) = 1
            Tabella.Rows.Add(myriga1)

            Dim myriga2 As System.Data.DataRow = Tabella.NewRow()
            myriga2(0) = "Cassa/Banca"
            myriga2(3) = 2
            Tabella.Rows.Add(myriga2)

            Dim myriga3 As System.Data.DataRow = Tabella.NewRow()
            myriga3(0) = "Abbuono/Sconto"
            myriga3(3) = 3
            Tabella.Rows.Add(myriga3)

            Dim myriga4 As System.Data.DataRow = Tabella.NewRow()
            myriga4(0) = "Spese/Rimborso"
            myriga4(3) = 4
            Tabella.Rows.Add(myriga4)

            Dim myriga5 As System.Data.DataRow = Tabella.NewRow()
            myriga5(0) = "Ritenute fiscali"
            myriga5(3) = 5
            Tabella.Rows.Add(myriga5)

            Dim myriga6 As System.Data.DataRow = Tabella.NewRow()
            myriga6(0) = "Libero"
            myriga6(3) = 6
            Tabella.Rows.Add(myriga6)


            ViewState("Tabella") = Tabella
        End If
        If DD_Tipo.SelectedValue = "C" Then
            DD_TipoDocumento.Items.Clear()
            DD_TipoDocumento.Items.Add("")
            DD_TipoDocumento.Items(DD_TipoDocumento.Items.Count - 1).Value = ""
            DD_TipoDocumento.Enabled = False

            Dim myriga7 As System.Data.DataRow = Tabella.NewRow()
            myriga7(0) = ""
            myriga7(3) = 0
            Tabella.Rows.Add(myriga7)

            ViewState("Tabella") = Tabella
        End If
        Call BindGrid()
        Call EseguiJS()
    End Sub
    Private Sub BindGrid()
        Tabella = ViewState("Tabella")
        Grid.AutoGenerateColumns = False
        Grid.DataSource = Tabella
        Grid.DataBind()
    End Sub
    Protected Sub DD_Tipo_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles DD_Tipo.SelectedIndexChanged
        Call ImpostaGriglia()
    End Sub

    Private Sub MaxCodice()
        Dim x As New Cls_CausaleContabile

    End Sub

    Protected Sub CaricaCausale()
        Dim x As New Cls_CausaleContabile

        x.Leggi(Session("DC_TABELLE"), Request.Item("CODICE"))

        Txt_Codice.Text = Request.Item("CODICE")
        DD_Tipo.SelectedValue = x.Tipo
        Call ImpostaGriglia()

        Txt_Descrizione.Text = x.Descrizione
        Try
            DD_RegistroIVA.SelectedValue = x.RegistroIVA
        Catch ex As Exception
            DD_RegistroIVA.SelectedValue = 0
        End Try
        Try
            DD_TipoDocumento.SelectedValue = x.TipoDocumento
        Catch ex As Exception
            DD_TipoDocumento.SelectedValue = ""
        End Try

        Try
            DD_CausaleIncasso.SelectedValue = x.CausaleIncasso
        Catch ex As Exception
            DD_CausaleIncasso.SelectedValue = ""
        End Try

        Try
            DD_Ritenuta.SelectedValue = x.Ritenuta
        Catch ex As Exception
            DD_Ritenuta.SelectedValue = ""
        End Try

        Try
            DD_CausaleDocumento.SelectedValue = x.DocumentoReverse
        Catch ex As Exception
            DD_CausaleDocumento.SelectedValue = ""
        End Try

        Try
            DD_CausaleGiroconto.SelectedValue = x.GirocontoReverse
        Catch ex As Exception
            DD_CausaleGiroconto.SelectedValue = ""
        End Try


        Txt_CodiceSede.Text = x.CodiceSede        

        DD_IVA.SelectedValue = x.CodiceIva

        DD_Detraibilita.SelectedValue = x.Detraibilita

        RB_ProrataSI.Checked = False
        RB_ProrataNO.Checked = False
        If x.Prorata = "S" Then
            RB_ProrataSI.Checked = True
            RB_ProrataNO.Checked = False
        End If
        If x.Prorata = "N" Then
            RB_ProrataSI.Checked = False
            RB_ProrataNO.Checked = True
        End If

        RB_DataObbSI.Checked = False
        RB_DataObbNO.Checked = False
        If x.DataObbligatoria = "S" Then
            RB_DataObbSI.Checked = True
            RB_DataObbNO.Checked = False
        End If
        If x.DataObbligatoria = "N" Then
            RB_DataObbSI.Checked = False
            RB_DataObbNO.Checked = True
        End If

        RB_NumObbSI.Checked = False
        RB_NumObbNO.Checked = False
        If x.NumeroObbligatorio = "S" Then
            RB_NumObbSI.Checked = True
            RB_NumObbNO.Checked = False
        End If
        If x.NumeroObbligatorio = "N" Then
            RB_NumObbSI.Checked = False
            RB_NumObbNO.Checked = True
        End If

        RB_AllegatoSI.Checked = False
        RB_AllegatoNO.Checked = False
        If x.NumeroObbligatorio = "S" Then
            RB_AllegatoSI.Checked = True
            RB_AllegatoNO.Checked = False
        End If
        If x.NumeroObbligatorio = "N" Then
            RB_AllegatoSI.Checked = False
            RB_AllegatoNO.Checked = True
        End If

        RB_Vendita.Checked = False
        RB_Acquisti.Checked = False
        If x.VenditaAcquisti = "V" Then
            RB_Acquisti.Checked = False
            RB_Vendita.Checked = True
        End If
        If x.VenditaAcquisti = "A" Then
            RB_Acquisti.Checked = True
            RB_Vendita.Checked = False
        End If

        RB_AnaliticaNO.Checked = False
        RB_AnaliticaSI.Checked = False
        If x.AnaliticaObbligatoria = 1 Then
            RB_AnaliticaNO.Checked = False
            RB_AnaliticaSI.Checked = True
        Else
            RB_AnaliticaNO.Checked = True
            RB_AnaliticaSI.Checked = False
        End If

        RB_ReverseAutomaticoNO.Checked = False
        RB_ReverseAutomaticoSI.Checked = False
        If x.AbilitaGirocontoReverse = 1 Then
            RB_ReverseAutomaticoSI.Checked = True
            RB_ReverseAutomaticoNO.Checked = False
        Else
            RB_ReverseAutomaticoSI.Checked = False
            RB_ReverseAutomaticoNO.Checked = True
        End If

        RB_CespitiNO.Checked = False
        RB_CespitiSI.Checked = False
        If x.GestioneCespiti = 1 Then
            RB_CespitiNO.Checked = False
            RB_CespitiSI.Checked = True
        Else
            RB_CespitiNO.Checked = True
            RB_CespitiSI.Checked = False
        End If

        Tabella = ViewState("Tabella")
        Dim i As Integer
        For i = 0 To x.Righe.Length - 1
            If Not IsNothing(x.Righe(i)) Then
                Dim DecCon As New Cls_Pianodeiconti
                DecCon.Mastro = x.Righe(i).Mastro
                DecCon.Conto = x.Righe(i).Conto
                DecCon.Sottoconto = x.Righe(i).Sottoconto
                DecCon.Decodfica(Session("DC_GENERALE"))
                If Tabella.Rows.Count > i Then
                    Tabella.Rows(i).Item(1) = x.Righe(i).Mastro & " " & x.Righe(i).Conto & " " & x.Righe(i).Sottoconto & " " & DecCon.Descrizione
                    Tabella.Rows(i).Item(2) = x.Righe(i).DareAvere
                    Tabella.Rows(i).Item(3) = x.Righe(i).Riga
                Else
                    Dim myriga6 As System.Data.DataRow = Tabella.NewRow()
                    myriga6(1) = x.Righe(i).Mastro & " " & x.Righe(i).Conto & " " & x.Righe(i).Sottoconto & " " & DecCon.Descrizione
                    myriga6(2) = x.Righe(i).DareAvere
                    myriga6(3) = x.Righe(i).Riga
                    Tabella.Rows.Add(myriga6)
                End If
            End If
        Next

        ViewState("Tabella") = Tabella
        Call BindGrid()
    End Sub

    Private Sub InserisciRiga()
        Call UpDateTable()
        Tabella = ViewState("Tabella")
        Dim myriga As System.Data.DataRow = Tabella.NewRow()
        myriga(3) = Val(Tabella.Rows(Tabella.Rows.Count - 1).Item(3).ToString) + 1
        Tabella.Rows.Add(myriga)

        ViewState("Tabella") = Tabella
        Call BindGrid()

        Call EseguiJS()
    End Sub
    Protected Sub Grid_RowCommand(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewCommandEventArgs) Handles Grid.RowCommand
        If (e.CommandName = "Inserisci") Then
            Call InserisciRiga()       
        End If
    End Sub
    Protected Sub Grid_RowDeleted(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewDeletedEventArgs) Handles Grid.RowDeleted

    End Sub
    Protected Sub Grid_RowCancelingEdit(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewCancelEditEventArgs) Handles Grid.RowCancelingEdit
        Grid.EditIndex = -1
        Call BindGrid()
    End Sub
    Protected Sub Grid_RowDeleting(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewDeleteEventArgs) Handles Grid.RowDeleting


        Call UpDateTable()
        Tabella = ViewState("Tabella")


        Tabella.Rows.RemoveAt(e.RowIndex)

        If Tabella.Rows.Count = 0 Then
            Dim myriga As System.Data.DataRow = Tabella.NewRow()
            myriga(1) = 0
            myriga(2) = 0
            myriga(3) = 0
            Tabella.Rows.Add(myriga)
        End If

        ViewState("Tabella") = Tabella

        Call BindGrid()
        Call EseguiJS()
    End Sub
    Protected Sub Grid_RowEditing(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewEditEventArgs) Handles Grid.RowEditing
        Grid.EditIndex = e.NewEditIndex
        Call BindGrid()
    End Sub


    Protected Sub Grid_RowDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs) Handles Grid.RowDataBound
        If e.Row.RowType = DataControlRowType.DataRow Then


            Dim LblSottoconto As Label = DirectCast(e.Row.FindControl("LblSottoconto"), Label)
            If Not IsNothing(LblSottoconto) Then

            Else
                Dim TxtSottoconto As TextBox = DirectCast(e.Row.FindControl("TxtSottoconto"), TextBox)
                TxtSottoconto.Text = Tabella.Rows(e.Row.RowIndex).Item(1).ToString

                Dim MyJs As String

                MyJs = "$(document).ready(function() { $(" & Chr(34) & "#" & TxtSottoconto.ClientID & Chr(34) & ").autocomplete('/WebHandler/GestioneConto.ashx?Utente=" & Session("UTENTE") & "', {delay:5,minChars:3}); });"
                REM MyJs = "$(document).ready(function() { alert('ww'); } );"
                ClientScript.RegisterClientScriptBlock(Me.GetType(), "visualizza", MyJs, True)

            End If

            Dim LblDare As Label = DirectCast(e.Row.FindControl("LblDareAvere"), Label)
            If Not IsNothing(LblDare) Then

            Else
                Dim RB_Avere As RadioButton = DirectCast(e.Row.FindControl("RB_Avere"), RadioButton)
                Dim RB_Dare As RadioButton = DirectCast(e.Row.FindControl("RB_Dare"), RadioButton)
                If Tabella.Rows(e.Row.RowIndex).Item(2).ToString = "D" Then
                    RB_Dare.Checked = True
                    RB_Avere.Checked = False
                Else
                    RB_Dare.Checked = False
                    RB_Avere.Checked = True
                End If
            End If

            Dim LblRiga As Label = DirectCast(e.Row.FindControl("LblRiga"), Label)
            If Not IsNothing(LblRiga) Then

            Else
                Dim TxtRiga As TextBox = DirectCast(e.Row.FindControl("TxtRiga"), TextBox)
                TxtRiga.Text = Tabella.Rows(e.Row.RowIndex).Item(3).ToString
            End If
        End If
    End Sub

    Protected Sub Grid_RowUpdating(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewUpdateEventArgs) Handles Grid.RowUpdating
        Dim TxtSottoconto As TextBox = DirectCast(Grid.Rows(e.RowIndex).FindControl("TxtSottoconto"), TextBox)
        Dim TxtDare As RadioButton = DirectCast(Grid.Rows(e.RowIndex).FindControl("RB_Dare"), RadioButton)
        Dim TxtAvere As RadioButton = DirectCast(Grid.Rows(e.RowIndex).FindControl("RB_Avere"), RadioButton)
        'Dim TxtRiga As TextBox = DirectCast(Grid.Rows(e.RowIndex).FindControl("TxtRiga"), TextBox)

       
  

        Tabella = ViewState("Tabella")
        Dim row = Grid.Rows(e.RowIndex)

        Tabella.Rows(row.DataItemIndex).Item(1) = TxtSottoconto.Text
        If TxtDare.Checked = True Then
            Tabella.Rows(row.DataItemIndex).Item(2) = "D"
        Else
            Tabella.Rows(row.DataItemIndex).Item(2) = "A"
        End If
        'Tabella.Rows(row.DataItemIndex).Item(3) = Val(TxtRiga.Text)

        ViewState("Tabella") = Tabella

        Grid.EditIndex = -1
        Call BindGrid()
    End Sub

    Protected Sub Btn_Modifica_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles Btn_Modifica.Click
        Dim x As New Cls_CausaleContabile

        Call UpDateTable()


        'x.Leggi(Session("DC_TABELLE"), Txt_Codice.Text)

        If Txt_Descrizione.Text.Trim = "" Then
            ClientScript.RegisterClientScriptBlock(Me.GetType(), "Errore", "VisualizzaErrore('Deve specificare la descrizione');", True)
            Call EseguiJS()
            Exit Sub
        End If



        Dim K As Long

        If Request.Item("CODICE") = "" Then
            For K = 0 To DD_CausaleIncasso.Items.Count - 1
                If Txt_Codice.Text = DD_CausaleIncasso.Items(K).Value Then
                    ClientScript.RegisterClientScriptBlock(Me.GetType(), "Errore", "VisualizzaErrore('Codice già inserito');", True)
                    Call EseguiJS()
                    REM Lbl_Errori.Text = "Codice già inserita"
                    Exit Sub
                End If
            Next
        End If

        If Txt_Codice.Text.Trim = "" Then
            ClientScript.RegisterClientScriptBlock(Me.GetType(), "Errore", "VisualizzaErrore('Deve specificare il codice');", True)
            Call EseguiJS()
            Exit Sub
        End If

        If DD_Tipo.Text.Trim = "" Then
            ClientScript.RegisterClientScriptBlock(Me.GetType(), "Errore", "VisualizzaErrore('Deve specificare il tipo causale');", True)
            Call EseguiJS()
            Exit Sub
        End If


        If RB_ReverseAutomaticoSI.Checked = True And (DD_CausaleGiroconto.SelectedValue = "" Or DD_CausaleDocumento.SelectedValue = "") Then
            ClientScript.RegisterClientScriptBlock(Me.GetType(), "Errore", "VisualizzaErrore('Deve specificare il documento reverse e il giroconto reverse');", True)
            Call EseguiJS()
            Exit Sub
        End If


        For K = 0 To DD_CausaleIncasso.Items.Count - 1
            If Trim(DD_CausaleIncasso.Items(K).Text) <> "" Then
                If DD_CausaleIncasso.Items(K).Text.Trim = Txt_Descrizione.Text And Txt_Codice.Text.Trim <> DD_CausaleIncasso.Items(K).Value.Trim Then
                    ClientScript.RegisterClientScriptBlock(Me.GetType(), "Errore", "VisualizzaErrore('Descrizione già inserita');", True)
                    Call EseguiJS()
                    Exit Sub
                End If
            End If
        Next

        Tabella = ViewState("Tabella")
        If Not IsNothing(Tabella) Then
            If Tabella.Rows.Count < 1 Then
                ClientScript.RegisterClientScriptBlock(Me.GetType(), "Errore", "VisualizzaErrore('Specificare almeno una riga');", True)
                Call EseguiJS()
                Exit Sub
            End If
        Else
            ClientScript.RegisterClientScriptBlock(Me.GetType(), "Errore", "VisualizzaErrore('Specificare almeno una riga');", True)
            Call EseguiJS()
            Exit Sub
        End If


        REM DD_CausaleContabile.SelectedValue = Txt_Codice.Text
        x.Tipo = DD_Tipo.SelectedValue
        x.Codice = Txt_Codice.Text

        x.Descrizione = Txt_Descrizione.Text
        x.RegistroIVA = DD_RegistroIVA.SelectedValue
        x.TipoDocumento = DD_TipoDocumento.SelectedValue
        x.CausaleIncasso = DD_CausaleIncasso.SelectedValue

        x.CodiceIva = DD_IVA.SelectedValue

        x.Detraibilita = DD_Detraibilita.SelectedValue

        x.Ritenuta = DD_Ritenuta.SelectedValue


        x.DocumentoReverse = DD_CausaleDocumento.SelectedValue
        x.GirocontoReverse = DD_CausaleGiroconto.SelectedValue

        x.CodiceSede = Txt_CodiceSede.Text

        If RB_ProrataSI.Checked = True Then
            x.Prorata = "S"
        Else
            x.Prorata = "N"
        End If


        If RB_DataObbSI.Checked = True Then
            x.DataObbligatoria = "S"
        Else
            x.DataObbligatoria = "N"
        End If



        If RB_NumObbSI.Checked = True Then
            x.NumeroObbligatorio = "S"
        Else
            x.NumeroObbligatorio = "N"
        End If


        If RB_AllegatoNO.Checked = True Then
            x.AllegatoFineAnno = "N"
        Else
            x.AllegatoFineAnno = "S"
        End If

        If RB_Vendita.Checked = True Then
            x.VenditaAcquisti = "V"
        Else
            x.VenditaAcquisti = "A"
        End If

        If RB_AnaliticaSI.Checked = True Then
            x.AnaliticaObbligatoria = 1
        Else
            x.AnaliticaObbligatoria = 0
        End If

        If RB_CespitiSI.Checked = True Then
            x.GestioneCespiti = 1
        Else
            x.GestioneCespiti = 0
        End If

        If RB_ReverseAutomaticoSI.Checked = True Then
            x.AbilitaGirocontoReverse = 1
        Else
            x.AbilitaGirocontoReverse = 0
        End If


        Tabella = ViewState("Tabella")
        Dim i As Integer

        For i = 0 To Tabella.Rows.Count - 1
            If IsNothing(x.Righe(i)) Then
                x.Righe(i) = New Cls_CausaliContabiliRiga
            End If
            If Tabella.Rows.Count > i Then
                Dim Mastro As Long = 0
                Dim Conto As Long = 0
                Dim Sottoconto As Long = 0
                Dim Vettore(100) As String

                If Not IsDBNull(Tabella.Rows(i).Item(1)) Then
                    Vettore = SplitWords(Tabella.Rows(i).Item(1))

                    If Vettore.Length >= 3 Then
                        Mastro = Val(Vettore(0))
                        Conto = Val(Vettore(1))
                        Sottoconto = Val(Vettore(2))
                    End If
                End If
                x.Righe(i).Mastro = Mastro
                x.Righe(i).Conto = Conto
                x.Righe(i).Sottoconto = Sottoconto
                If Not IsDBNull(Tabella.Rows(i).Item(2)) Then
                    x.Righe(i).DareAvere = Tabella.Rows(i).Item(2)
                End If
                x.Righe(i).Riga = Val(Tabella.Rows(i).Item(3))
                x.Righe(i).Utente = Session("UTENTE")
            End If
        Next

        x.Utente = Session("UTENTE")

        x.Scrivi(Session("DC_TABELLE"), Txt_Codice.Text)


        Lbl_Errori.Text = ""
     

        If Request.Item("PAGINA") = "" Then
            Response.Redirect("ElencoCausaliContabili.aspx")
        Else
            Response.Redirect("ElencoCausaliContabili.aspx?CHIAMATA=RITORNO&PAGINA=" & Request.Item("PAGINA"))
        End If



    End Sub


    Private Sub Pulisci()
        Lbl_Errori.Text = ""
        Txt_Codice.Text = ""
        DD_Tipo.SelectedValue = ""        
        Txt_Descrizione.Text = ""
        DD_RegistroIVA.SelectedValue = 0
        DD_TipoDocumento.SelectedValue = ""
        DD_CausaleIncasso.SelectedValue = ""

        RB_ProrataSI.Checked = True
        RB_ProrataNO.Checked = False
 

        RB_DataObbSI.Checked = True
        RB_DataObbNO.Checked = False
        

        RB_NumObbSI.Checked = True
        RB_NumObbNO.Checked = False
        

        RB_AllegatoSI.Checked = True
        RB_AllegatoNO.Checked = False
        

        RB_Vendita.Checked = False
        RB_Acquisti.Checked = False

        DD_CausaleDocumento.SelectedValue = ""
        DD_CausaleGiroconto.SelectedValue = ""

        
        Tabella.Clear()
        Tabella.Columns.Clear()
        Tabella.Columns.Add("Descrizione", GetType(String))
        Tabella.Columns.Add("Sottoconto", GetType(String))
        Tabella.Columns.Add("DareAvere", GetType(String))
        Tabella.Columns.Add("Riga", GetType(Long))
        ViewState("Tabella") = Tabella
        Call BindGrid()


        Dim x As New Cls_CausaleContabile


        x.UpDateDropBox(Session("DC_TABELLE"), DD_CausaleIncasso)

        Dim M As New Cls_RegistroIVA

        M.UpDateDropBox(Session("DC_TABELLE"), DD_RegistroIVA)


        Dim xd As New ClsDetraibilita
        xd.UpDateDropBox(Session("DC_TABELLE"), DD_Detraibilita)

        Dim CodiciIva As New Cls_IVA

        CodiciIva.UpDateDropBox(Session("DC_TABELLE"), DD_IVA)

    End Sub
    Private Function SplitWords(ByVal s As String) As String()
        '
        ' Call Regex.Split function from the imported namespace.
        ' Return the result array.
        '
        Return Regex.Split(s, "\W+")
    End Function

    Protected Sub ImageButton1_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles ImageButton1.Click
        Lbl_Errori.Text = ""

        If Txt_Codice.Text = "" Then
            ClientScript.RegisterClientScriptBlock(Me.GetType(), "Errore", "VisualizzaErrore('Deve selezionare una causale');", True)
            REM Lbl_Errori.Text = "Deve selezionare una causale"
            Call EseguiJS()
            Exit Sub
        End If

        Dim x As New Cls_CausaleContabile

        x.Leggi(Session("DC_TABELLE"), Txt_Codice.Text)

        If x.Descrizione.Trim = "" Then
            ClientScript.RegisterClientScriptBlock(Me.GetType(), "Errore", "VisualizzaErrore('La causale selezionata non è registrata');", True)
            REM Lbl_Errori.Text = "La causale selezionata non è registrata"
            Call EseguiJS()
            Exit Sub
        End If


        Dim cn As OleDbConnection

        cn = New Data.OleDb.OleDbConnection(Session("DC_GENERALE"))

        cn.Open()
        Dim cmd As New OleDbCommand()
        cmd.CommandText = ("select * from MovimentiContabiliTesta where " & _
                           "CausaleContabile = ? ")
        cmd.Parameters.AddWithValue("@Mastro", Txt_Codice.Text)
        cmd.Connection = cn

        Dim myPOSTreader As OleDbDataReader = cmd.ExecuteReader()
        If myPOSTreader.Read Then
            ClientScript.RegisterClientScriptBlock(Me.GetType(), "Errore", "VisualizzaErrore('Non posso eliminare vi sono registrazioni con questa causale');", True)
            REM Lbl_Errori.Text = "Non posso eliminare vi sono registrazioni con questo conto"
            cn.Close()
            Exit Sub
        End If
        cn.Close()



        x.Elimina(Session("DC_TABELLE"), Txt_Codice.Text)

        If Request.Item("PAGINA") = "" Then
            Response.Redirect("ElencoCausaliContabili.aspx")
        Else
            Response.Redirect("ElencoCausaliContabili.aspx?CHIAMATA=RITORNO&PAGINA=" & Request.Item("PAGINA"))
        End If


    End Sub



    Protected Sub Btn_Esci_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles Btn_Esci.Click

        If Request.Item("PAGINA") = "" Then
            Response.Redirect("ElencoCausaliContabili.aspx")
        Else
            Response.Redirect("ElencoCausaliContabili.aspx?CHIAMATA=RITORNO&PAGINA=" & Request.Item("PAGINA"))
        End If

    End Sub

    Protected Sub DD_CausaleIncasso_TextChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles DD_CausaleIncasso.TextChanged

    End Sub

    Protected Sub Btn_Duplica_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles Btn_Duplica.Click
        Dim x As New Cls_CausaleContabile

        Txt_Codice.Text = x.MaxCodice(Session("DC_TABELLE"))
        Txt_Codice.Enabled = True

        Call Txt_Descrizione_TextChanged(sender, e)

        Call EseguiJS()
    End Sub

    Private Sub EseguiJS()
        Dim MyJs As String
        MyJs = "$(document).ready(function() {"

        MyJs = MyJs & "var els = document.getElementsByTagName(""*"");"

        MyJs = MyJs & "for (var i=0;i<els.length;i++)"
        MyJs = MyJs & "if ( els[i].id ) { "
        MyJs = MyJs & " var appoggio =els[i].id; "
        MyJs = MyJs & " if (appoggio.match('TxtSottoconto')!= null) {  "
        MyJs = MyJs & " $(els[i]).autocomplete('/WebHandler/GestioneConto.ashx?Utente=" & Session("UTENTE") & "', {delay:5,minChars:3});"
        MyJs = MyJs & "    }"

        MyJs = MyJs & " if ((appoggio.match('TxtDare')!= null) || appoggio.match('TxtAvere')!= null) {  "
        MyJs = MyJs & " $(els[i]).keypress(function() { ForceNumericInput($(this).val(), true, true); } ); "
        MyJs = MyJs & " $(els[i]).blur(function() { var ap = formatNumber($(this).val(),2); $(this).val(ap); });"
        MyJs = MyJs & "    }"



        MyJs = MyJs & "} "
        MyJs = MyJs & "});"

        'MyJs = "$(document).ready(function() { $('.myClass').keypress(function() { handleEnter($('.myClass').val(), true, true); } );     $('#" & Txt_ImportoPagato.ClientID & "').blur(function() { var ap = formatNumber ($('#" & Txt_ImportoPagato.ClientID & "').val(),2); $('#" & Txt_ImportoPagato.ClientID & "').val(ap); }); });"
        ScriptManager.RegisterStartupScript(Me, Me.GetType(), "visualizzaRitIm", MyJs, True)
    End Sub

    Private Sub UpDateTable()
        Dim i As Integer


        Tabella = ViewState("Tabella")


        For i = 0 To Grid.Rows.Count - 1

            Dim TxtSottoconto As TextBox = DirectCast(Grid.Rows(i).FindControl("TxtSottoconto"), TextBox)
            Dim TxtDare As RadioButton = DirectCast(Grid.Rows(i).FindControl("RB_Dare"), RadioButton)
            Dim TxtAvere As RadioButton = DirectCast(Grid.Rows(i).FindControl("RB_Avere"), RadioButton)


            Tabella.Rows(i).Item(1) = TxtSottoconto.Text
            If TxtDare.Checked = True Then
                Tabella.Rows(i).Item(2) = "D"
            Else
                Tabella.Rows(i).Item(2) = "A"
            End If            

        Next

        ViewState("Tabella") = Tabella

        Call BindGrid()
    End Sub

    Protected Sub BTN_InserisciRiga_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles BTN_InserisciRiga.Click
        Call InserisciRiga()
    End Sub

    
    Protected Sub DD_IVA_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles DD_IVA.SelectedIndexChanged

    End Sub

    Protected Sub Txt_Codice_TextChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles Txt_Codice.TextChanged
        If Txt_Codice.Enabled = True Then
            Img_VerificaCodice.ImageUrl = "~/images/corretto.gif"

            Dim x As New Cls_CausaleContabile

            x.Codice = Txt_Codice.Text
            x.Leggi(Session("DC_TABELLE"), x.Codice)

            If x.Descrizione <> "" Then
                Img_VerificaCodice.ImageUrl = "~/images/errore.gif"
            End If
        End If
        Call EseguiJS()
    End Sub

    Protected Sub Txt_Descrizione_TextChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles Txt_Descrizione.TextChanged
        If Txt_Codice.Enabled = True Then
            Img_VerificaDescrizione.ImageUrl = "~/images/corretto.gif"

            Dim x As New Cls_CausaleContabile

            x.Codice = ""
            x.Descrizione = Txt_Descrizione.Text.Trim
            x.LeggiDescrizione(Session("DC_TABELLE"), x.Descrizione.Trim)

            If x.Codice <> "" Then
                Img_VerificaDescrizione.ImageUrl = "~/images/errore.gif"
            End If
        End If
        Call EseguiJS()
    End Sub
End Class

