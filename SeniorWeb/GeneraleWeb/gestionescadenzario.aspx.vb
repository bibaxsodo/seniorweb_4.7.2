﻿Imports Microsoft.VisualBasic
Imports System.Data.OleDb
Imports System.Web.Hosting

Partial Class GeneraleWeb_gestionescadenzario
    Inherits System.Web.UI.Page
    Dim Tabella As New System.Data.DataTable("tabella")

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Trim(Session("UTENTE")) = "" Then
            If Request.ServerVariables("URL").ToUpper.IndexOf("SENIORWEBBLANCO") > 0 Then
                Response.Redirect("/ODV/Login.aspx")
                Exit Sub
            Else
                Response.Redirect("/SeniorWeb/Login.aspx")
                Exit Sub
            End If
        End If

        Call EseguiJS()

        If Page.IsPostBack = True Then Exit Sub

        Lbl_DatiRegistrazione.Text = "Scadenze registrazioni " & Val(Request.Item("NUMERO"))
        Dim xs As New Cls_Scadenziario

        xs.NumeroRegistrazioneContabile = Val(Request.Item("NUMERO"))
        xs.loaddati(Session("DC_GENERALE"), Tabella)


        xs.Numero = 0
        xs.Leggi(Session("DC_GENERALE"))


        If Request.Item("INSERIMENTO") = "TRUE" And xs.Numero = 0 Then
            Dim Registrazione As New Cls_MovimentoContabile


            Tabella.Clear()
            Tabella.Columns.Clear()

            Tabella.Columns.Add("DataScadenza", GetType(String))
            Tabella.Columns.Add("Importo", GetType(String))
            Tabella.Columns.Add("Descrizione", GetType(String))
            Tabella.Columns.Add("Chiusa", GetType(String))
            Tabella.Columns.Add("Numero", GetType(Long))
            Tabella.Columns.Add("RegistrazioneIncasso", GetType(Long))


            Registrazione.NumeroRegistrazione = Val(Request.Item("NUMERO"))
            Registrazione.Leggi(Session("DC_GENERALE"), Registrazione.NumeroRegistrazione)

            Dim myriga As System.Data.DataRow = Tabella.NewRow()

            myriga(0) = Format(Registrazione.DataRegistrazione, "dd/MM/yyyy")
            myriga(1) = Registrazione.ImportoDocumento(Session("DC_TABELLE"))
            myriga(2) = ""

            myriga(3) = "Aperta"
            myriga(4) = Registrazione.NumeroRegistrazione

            myriga(5) = 0

            Tabella.Rows.Add(myriga)
        End If

        Session("RigheScadenzario") = Tabella

        Call faibind()
    End Sub


    Private Sub faibind()

        Tabella = Session("RigheScadenzario")
        Grid.AutoGenerateColumns = False
        Grid.DataSource = Tabella
        Grid.DataBind()

    End Sub

    Protected Sub Grid_RowCommand(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewCommandEventArgs) Handles Grid.RowCommand
        If (e.CommandName = "Inserisci") Then
            Call UpDateTable()
            Tabella = Session("RigheScadenzario")


            Dim d As Integer



            d = Val(e.CommandArgument)


            Dim myriga As System.Data.DataRow = Tabella.NewRow()
            Tabella.Rows.InsertAt(myriga, d + 1)

            Session("RigheScadenzario") = Tabella
            Call faibind()
        End If
    End Sub

    Protected Sub Grid_RowDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs) Handles Grid.RowDataBound
        If e.Row.RowType = DataControlRowType.DataRow Then


        
            Dim ChkChiusa As CheckBox = DirectCast(e.Row.FindControl("ChkChiusa"), CheckBox)
            If Tabella.Rows(e.Row.RowIndex).Item(3).ToString = "Chiusa" Then
                ChkChiusa.Checked = True
            Else
                ChkChiusa.Checked = False
            End If


            Dim TxtDataScadenza As TextBox = DirectCast(e.Row.FindControl("TxtDataScadenza"), TextBox)
            TxtDataScadenza.Text = Tabella.Rows(e.Row.RowIndex).Item(0).ToString




            Dim TxtImporto As TextBox = DirectCast(e.Row.FindControl("TxtImporto"), TextBox)
            If campodb(Tabella.Rows(e.Row.RowIndex).Item(1).ToString) = "" Then
                TxtImporto.Text = Format(0, "#,##0.00")
            Else
                TxtImporto.Text = Format(campodbN(Tabella.Rows(e.Row.RowIndex).Item(1).ToString), "#,##0.00")
            End If



            Dim TxtDescrizione As TextBox = DirectCast(e.Row.FindControl("TxtDescrizione"), TextBox)
            TxtDescrizione.Text = Tabella.Rows(e.Row.RowIndex).Item(2).ToString

            Dim TxtRegistrazioneIncasso As TextBox = DirectCast(e.Row.FindControl("TxtRegistrazioneIncasso"), TextBox)
            TxtRegistrazioneIncasso.Text = Tabella.Rows(e.Row.RowIndex).Item(5).ToString
        End If
    End Sub


    Private Sub EseguiJS()
        Dim MyJs As String
        MyJs = "$(document).ready(function() {"

        MyJs = MyJs & "var els = document.getElementsByTagName(""*"");"

        MyJs = MyJs & "for (var i=0;i<els.length;i++)"
        MyJs = MyJs & "if ( els[i].id ) { "
        MyJs = MyJs & " var appoggio =els[i].id; "
        MyJs = MyJs & " if (appoggio.match('TxtData')!= null) {  "
        MyJs = MyJs & " $(els[i]).mask(""99/99/9999"");"
        MyJs = MyJs & " $(els[i]).datepicker({ changeMonth: true,changeYear: true,  yearRange: ""1990:" & Year(Now) + 5 & """},$.datepicker.regional[""it""]);"
        MyJs = MyJs & "    }"
        MyJs = MyJs & " if ((appoggio.match('TxtImporto')!= null) ||  (appoggio.match('Txt_Importo')!= null) ) {  "
        MyJs = MyJs & " $(els[i]).keypress(function() { ForceNumericInput($(this).val(), true, true); } ); "
        MyJs = MyJs & " $(els[i]).blur(function() { var ap = formatNumber($(this).val(),2); $(this).val(ap); });"
        MyJs = MyJs & "    }"


        MyJs = MyJs & "} "
        MyJs = MyJs & "});"

        'MyJs = "$(document).ready(function() { $('.myClass').keypress(function() { handleEnter($('.myClass').val(), true, true); } );     $('#" & Txt_ImportoPagato.ClientID & "').blur(function() { var ap = formatNumber ($('#" & Txt_ImportoPagato.ClientID & "').val(),2); $('#" & Txt_ImportoPagato.ClientID & "').val(ap); }); });"
        ScriptManager.RegisterStartupScript(Me, Me.GetType(), "visualizzaRitIm", MyJs, True)
    End Sub

    Function campodbN(ByVal oggetto As Object) As Double


        If IsDBNull(oggetto) Then
            Return 0
        Else
            Return oggetto
        End If
    End Function
    Function campodb(ByVal oggetto As Object) As String


        If IsDBNull(oggetto) Then
            Return ""
        Else
            Return oggetto
        End If
    End Function
 
    Protected Sub Grid_RowCancelingEdit(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewCancelEditEventArgs) Handles Grid.RowCancelingEdit
        Grid.EditIndex = -1
        Call faibind()
    End Sub
  
    Protected Sub Grid_RowUpdating(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewUpdateEventArgs) Handles Grid.RowUpdating
        Dim ChkChiusa As CheckBox = DirectCast(Grid.Rows(e.RowIndex).FindControl("ChkChiusa"), CheckBox)
        Dim TxtDataScadenza As TextBox = DirectCast(Grid.Rows(e.RowIndex).FindControl("TxtDataScadenza"), TextBox)
        Dim TxtImporto As TextBox = DirectCast(Grid.Rows(e.RowIndex).FindControl("TxtImporto"), TextBox)
        Dim TxtDescrizione As TextBox = DirectCast(Grid.Rows(e.RowIndex).FindControl("TxtDescrizione"), TextBox)
        Dim TxtRegistrazioneIncasso As TextBox = DirectCast(Grid.Rows(e.RowIndex).FindControl("TxtRegistrazioneIncasso"), TextBox)
        'TxtRegistrazioneIncasso


        If Not IsDate(TxtDataScadenza.Text) Then
            Exit Sub
        End If

        If Not IsNumeric(TxtImporto.Text) Then
            Exit Sub
        End If

        Tabella = Session("RigheScadenzario")
        Dim row = Grid.Rows(e.RowIndex)

        If ChkChiusa.Checked Then
            Tabella.Rows(row.DataItemIndex).Item(3) = "Chiusa"
        Else
            Tabella.Rows(row.DataItemIndex).Item(3) = "Aperta"
        End If

        Tabella.Rows(row.DataItemIndex).Item(0) = TxtDataScadenza.Text
        Tabella.Rows(row.DataItemIndex).Item(1) = TxtImporto.Text
        Tabella.Rows(row.DataItemIndex).Item(2) = TxtDescrizione.Text
        Tabella.Rows(row.DataItemIndex).Item(5) = TxtRegistrazioneIncasso.Text
        Session("RigheScadenzario") = Tabella

        Grid.EditIndex = -1
        Call faibind()
    End Sub


    Protected Sub Grid_RowDeleting(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewDeleteEventArgs) Handles Grid.RowDeleting        

        Call UpDateTable()

        Tabella = Session("RigheScadenzario")


        Tabella.Rows.RemoveAt(e.RowIndex)

        If Tabella.Rows.Count = 0 Then
            Dim myriga As System.Data.DataRow = Tabella.NewRow()

            Tabella.Rows.Add(myriga)
        End If

        Session("RigheScadenzario") = Tabella

        Call faibind()
    End Sub




    Protected Sub Grid_RowEditing(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewEditEventArgs) Handles Grid.RowEditing
        Grid.EditIndex = e.NewEditIndex
        Call faibind()
    End Sub

    Protected Sub Btn_Modifica_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles Btn_Modifica.Click
        Call UpDateTable()
        Dim ImportoTotale As Double = 0
        Dim ImportoDocumento As Double = 0
        Dim Registrazione As New Cls_MovimentoContabile


        Registrazione.Leggi(Session("DC_GENERALE"), Val(Request.Item("NUMERO")))

        ImportoDocumento = Registrazione.ImportoDocumento(Session("DC_TABELLE"))
        If ImportoDocumento = 0 Then

            ImportoDocumento = Registrazione.ImportoRegistrazioneDocumento(Session("DC_TABELLE"))
        End If


        For i = 0 To Grid.Rows.Count - 1

            Dim TxtImporto As TextBox = DirectCast(Grid.Rows(i).FindControl("TxtImporto"), TextBox)

            If TxtImporto.Text = "" Then
                TxtImporto.Text = "0"
            End If
            ImportoTotale = ImportoTotale + CDbl(TxtImporto.Text)
        Next

        If Math.Abs(Math.Round(ImportoTotale, 2)) > Math.Abs(Math.Round(ImportoDocumento, 2)) Then

            Dim MyJs1 As String

            MyJs1 = "$(document).ready(function() { alert('Importo scadenze superiore al documento non posso procedere " & ImportoDocumento & " - " & ImportoTotale & "'); } );"
            ClientScript.RegisterClientScriptBlock(Me.GetType(), "MscM", MyJs1, True)
            Exit Sub
        End If

        Tabella = Session("RigheScadenzario")
        Dim xs As New Cls_Scadenziario

        xs.NumeroRegistrazioneContabile = Val(Request.Item("NUMERO"))
        xs.Scrivi(Session("DC_GENERALE"), Tabella)

        Dim MyJs As String

        MyJs = "$(document).ready(function() { alert('Modificato Scadenzario'); } );"
        ClientScript.RegisterClientScriptBlock(Me.GetType(), "ModScM", MyJs, True)
    End Sub

    Private Sub UpDateTable()



        Tabella.Clear()
        Tabella.Columns.Clear()

        Tabella.Columns.Add("DataScadenza", GetType(String))
        Tabella.Columns.Add("Importo", GetType(String))
        Tabella.Columns.Add("Descrizione", GetType(String))
        Tabella.Columns.Add("Chiusa", GetType(String))
        Tabella.Columns.Add("Numero", GetType(Long))
        Tabella.Columns.Add("RegistrazioneIncasso", GetType(Long))
        For i = 0 To Grid.Rows.Count - 1

            Dim ChkChiusa As CheckBox = DirectCast(Grid.Rows(i).FindControl("ChkChiusa"), CheckBox)
            Dim TxtDataScadenza As TextBox = DirectCast(Grid.Rows(i).FindControl("TxtDataScadenza"), TextBox)
            Dim TxtImporto As TextBox = DirectCast(Grid.Rows(i).FindControl("TxtImporto"), TextBox)
            Dim TxtDescrizione As TextBox = DirectCast(Grid.Rows(i).FindControl("TxtDescrizione"), TextBox)

            Dim TxtRegistrazioneIncasso As TextBox = DirectCast(Grid.Rows(i).FindControl("TxtRegistrazioneIncasso"), TextBox)
            Dim myrigaR As System.Data.DataRow = Tabella.NewRow()

            myrigaR(0) = TxtDataScadenza.Text
            myrigaR(1) = TxtImporto.Text
            myrigaR(2) = TxtDescrizione.Text
            If ChkChiusa.Checked Then
                myrigaR(3) = "Chiusa"
            Else
                myrigaR(3) = "Aperta"
            End If
            myrigaR(4) = 0

            myrigaR(5) = Val(TxtRegistrazioneIncasso.Text)
            Tabella.Rows.Add(myrigaR)

        Next

        Session("RigheScadenzario") = Tabella
    End Sub
    Protected Sub Grid_RowUpdated(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewUpdatedEventArgs) Handles Grid.RowUpdated

    End Sub

    Protected Sub Grid_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles Grid.SelectedIndexChanged

    End Sub
End Class
