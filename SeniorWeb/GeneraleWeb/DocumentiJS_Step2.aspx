﻿<%@ Page Language="VB" AutoEventWireup="false" Inherits="GeneraleWeb_DocumentiJS_Step2" CodeFile="DocumentiJS_Step2.aspx.vb" %>


<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="xasp" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
    <meta http-equiv="x-ua-compatible" content="IE=9" />
    <title>Documenti</title>
    <asp:PlaceHolder runat="server">
        <%: System.Web.Optimization.Styles.Render("~/Content/AjaxControlToolkit/Styles/Bundle") %>
    </asp:PlaceHolder>
    <style type="text/css">
        #header {
            background: url(title.png) center center no-repeat;
            height: 150px;
            width: 100%;
            margin: 45px 0 0 0;
            padding: 0;
        }

        #mainContent {
            background: #FFF;
            padding: 50px;
            width: 1255px;
            margin: 0 auto;
            overflow: hidden;
            position: relative;
            -moz-box-shadow: 0px 0px 8px #CCC; /* FF3.5+ */
            -webkit-box-shadow: 0px 0px 8px #CCC; /* Saf3.0+, Chrome */
            box-shadow: 0px 0px 8px #CCC; /* Opera 10.5, IE 9.0 */
            filter: /* IE6,IE7 e IE8 */
            progid:DXImageTransform.Microsoft.Shadow(color=#dddddd,direction=0,strength=5) /* top */
            progid:DXImageTransform.Microsoft.Shadow(color=#dddddd,direction=90,strength=5) /* left */
            progid:DXImageTransform.Microsoft.Shadow(color=#dddddd,direction=180,strength=5) /* bottom */
            progid:DXImageTransform.Microsoft.Shadow(color=#dddddd,direction=270,strength=5); /* right */
        }

        div.comandi {
            position: fixed;
            margin-right: 15px;
            margin-top: 500px;
            margin-left: 20px
        }

        .confermamodifca {
            -webkit-box-shadow: 10px 11px 11px 0px rgba(0,0,0,0.65);
            -moz-box-shadow: 10px 11px 11px 0px rgba(0,0,0,0.65);
            box-shadow: 10px 11px 11px 0px rgba(0,0,0,0.65);
            background-color: #82807d;
            text-align: center;
            color: White;
            z-index: 100;
        }



        .container {
            width: 100%;
            border: 1px solid #d3d3d3;
        }

            .container div {
                width: 100%;
            }


            .container .header {
                background-color: #d3d3d3;
                padding: 10px;
                cursor: pointer;
                font-weight: bold;
            }

            .container .content {
                display: none;
                padding: 5px;
            }



        .containerCampiTesta {
            width: 100%;
        }

            .containerCampiTesta .headerCampiTesta {
                background-color: #eee;
                padding: 10px;
                font-weight: bold;
            }



        .hiddencol {
            display: none;
        }

        .showcol {
            display: block;
        }
    </style>

    <link href="ospiti.css?ver=11" rel="stylesheet" type="text/css" />
    <link rel="shortcut icon" href="images/SENIOR.ico" />
    <link href="js/jquery.autocomplete.css" rel="stylesheet" type="text/css" />

    <script src="js/jquery-1.5.1.min.js" type="text/javascript"></script>
    <script src="js/jquery.autocomplete.js?ver=4" type="text/javascript"></script>
    <script src="js/soapclient.js" type="text/javascript"></script>
    <script src="/js/convertinumero.js" type="text/javascript"></script>

    <script src="js/jquery.maskedinput-1.3.min.js" type="text/javascript"></script>
    <script src="/js/formatnumer.js" type="text/javascript"></script>

    <script src="/js/pianoconti.js" type="text/javascript"></script>

    <script type="text/javascript" src="js/jquery.corner.js"></script>
    <link rel="stylesheet" href="dialogbox/redips-dialog.css" type="text/css" media="screen" />
    <script type="text/javascript" src="dialogbox/redips-dialog-min.js"></script>
    <script type="text/javascript" src="dialogbox/script.js"></script>
    <script src="js/JSErrore.js" type="text/javascript"></script>
    <script src="js/chosen/chosen.jquery.js" type="text/javascript"></script>
    <script src="js/chosen/docsupport/prism.js" type="text/javascript" charset="utf-8"></script>


    <link rel="stylesheet" href="js/chosen/docsupport/style.css">
    <link rel="stylesheet" href="js/chosen/docsupport/prism.css">
    <link rel="stylesheet" href="js/chosen/chosen.css">

    <link rel="stylesheet" href="jqueryui/jquery-ui.css" type="text/css" />
    <script src="jqueryui/jquery-ui.js" type="text/javascript"></script>
    <script src="jqueryui/datapicker-it.js" type="text/javascript"></script>

    <style>
        .SeniorButton:hover {
            background-color: Silver;
            color: gray;
        }
    </style>
    <script type="text/javascript">  
        $(document).ready(function () {
            $('html').keyup(function (event) {
                if (event.keyCode == 113) {
                    __doPostBack("Btn_Modifica", "0");
                }

                if (event.keyCode == 119) {
                    __doPostBack("Btn_Pulisci", "0");
                }
                if (event.keyCode == 120) {
                    __doPostBack("BTN_InserisciRiga", "0");
                }


            });
        });

        $(document).ready(function () {
            if (window.innerHeight > 0) { $("#BarraLaterale").css("height", (window.innerHeight - 115) + "px"); } else { $("#BarraLaterale").css("height", (document.documentElement.offsetHeight - 115) + "px"); }

            $("#idtestatab").css("width", (window.innerWidth - 161) + "px");

            if (window.innerWidth < 1600) {
                $("#cellcaucont").css("width", "200%");
                $("#cellcaucont").css("float", "left");
                $("#cellcaucont").css("clear", "left");

                $("#cellmodpag").css("width", "100%");
                $("#cellmodpag").css("float", "left");
                $("#cellmodpag").css("clear", "left");

                //$("#TabContainer1_Tab_Testata_Dd_CausaleContabile").css("width", "160px");
            } else {
                $("#cellcaucont").css("width", "50%");
                //$("#TabContainer1_Tab_Testata_Dd_CausaleContabile").css("width", "350px");
            }
            //$("#TabContainer1$Tab_Testata$Txt_ClienteFornitore").blur(function() { alert('a'); setTimeout(function() { __doPostBack("BtnClienteFornitoreDec_Click", "0"); }, 200); });
        });


        function inserisciriga(Numero) {
            if (Numero == 0) {
                __doPostBack("BtnInsertRiga3", "0");
            }
            if (Numero == 1) {
                __doPostBack("BtnInsertRiga4", "0");
            }
            if (Numero == 2) {
                __doPostBack("BtnInsertRiga5", "0");
            }
            if (Numero == 3) {
                __doPostBack("BtnInsertRiga6", "0");
            }
            if (Numero == 4) {
                __doPostBack("BtnInsertRiga9", "0");
            }

            if (Numero == 7) {
                __doPostBack("BtnInsertRiga7", "0");
            }

            if (Numero == 8) {
                __doPostBack("BtnInsertRiga8", "0");
            }


            if (Numero == 100) {

                __doPostBack("BtnRicalcolaIVA", "0");
            }
        }

        function soloNumeri(evt) {
            var charCode = (evt.which) ? evt.which : event.keyCode
            if (charCode > 31 && (charCode < 48 || charCode > 57)) {
                return false;
            }
            return true;
        }

        function DialogBoxBig(Path) {


            var winW = 630, winH = 460;
            if (document.body && document.body.offsetWidth) {
                winW = document.body.offsetWidth;
                winH = document.body.offsetHeight;
            }
            if (document.compatMode == 'CSS1Compat' &&
                document.documentElement &&
                document.documentElement.offsetWidth) {
                winW = document.documentElement.offsetWidth;
                winH = document.documentElement.offsetHeight;
            }
            if (window.innerWidth && window.innerHeight) {
                winW = window.innerWidth;
                winH = window.innerHeight;
            }

            var APPO = winH - 100;


            REDIPS.dialog.show(winW - 200, winH - 100, '<iframe id="output" src="' + Path + '" height="' + APPO + '" width="100%" ></iframe>');

            return false;

        }

        function DialogBoxW(Path) {
            var tot = 0;

            tot = document.body.offsetWidth - 100;
            REDIPS.dialog.show(tot, 500, '<iframe id="output" src="' + Path + '" height="490px" width="' + tot + 'px"></iframe>');
            return false;

        }

        function DialogBox(Path) {

            var tot = 0;

            tot = document.body.offsetWidth - 300;

            REDIPS.dialog.show(tot, 520, '<iframe id="output" src="' + Path + '" height="510px" width="' + tot + 'px"></iframe>');
            return false;

        }
        function DialogBoxSlim(Path) {

            REDIPS.dialog.show(900, 400, '<iframe id="output" src="' + Path + '" height="390px" width="890"></iframe>');
            return false;

        }
        $(document).ready(function () {
            var winW = 630, winH = 460;
            if (document.body && document.body.offsetWidth) {
                winW = document.body.offsetWidth;
                winH = document.body.offsetHeight;
            }
            if (document.compatMode == 'CSS1Compat' &&
                document.documentElement &&
                document.documentElement.offsetWidth) {
                winW = document.documentElement.offsetWidth;
                winH = document.documentElement.offsetHeight;
            }
            if (window.innerWidth && window.innerHeight) {
                winW = window.innerWidth;
                winH = window.innerHeight;
            }
            //document.getElementById("xcommand").setAttribute('style','margin-top: ' + (winH  - 140) + 'px');


        });

        $(document).ready(function () {
            $('ul li:last').css('margin-right', '0');
            $("ul").corner('bottom', '18px');
            $('ul li a').click(function () {
                $('.current').removeClass('current');
                $(this).addClass('current');
            });


            $('a[href*=#]').click(function () {
                if (location.pathname.replace(/^\//, '') == this.pathname.replace(/^\//, '')
                    && location.hostname == this.hostname) {
                    var $target = $(this.hash);
                    $target = $target.length && $target || $('[name=' + this.hash.slice(1) + ']');
                    if ($target.length) {
                        var targetOffset = $target.offset().top;
                        $('html,body').animate({ scrollTop: targetOffset }, 1000);
                        return false;
                    }
                }
            });


        });

        function ChiudiConfermaModifica() {
            window.location.href = "documentijs.aspx";


            $("#ConfermaModifica").remove();
            $("#LblBox").empty();


        }



        function VerifcaIVA(Imponile) {
            var nome = Imponile.attr('id');

            //'TabContainer1_Tab_IVA_GridIVA_ctl02_TxtImponibileIVA

            var imponibile = $('#' + nome).val().replace(".", "").replace(",", ".");

            var importonome = nome.replace("TxtImponibileIVA", "TxtImpostaIVA");


            var ddiva = nome.replace("TxtImponibileIVA", "DDIva");


            var importo = $('#' + importonome).val().replace(".", "").replace(",", ".");

            var codiva = $('#' + ddiva).val();

            $.ajax({
                url: "DecodificaIVA.ashx?CODICE=" + codiva,
                success: function (data, stato) {
                    var AliquotaIVA = parseFloat(data.replace(".", "").replace(",", "."));

                    var calcola = Math.round((parseFloat(imponibile) * AliquotaIVA) * 100) / 100;

                    if (calcola != importo) {
                        //alert(calcola + '  ' + importo + ' ' + AliquotaIVA);
                        $('#' + importonome).css({ 'background-color': '#FF0000' });
                    } else {
                        $('#' + importonome).css({ 'background-color': '#FFFFFF' });
                    }
                },
                error: function (richiesta, stato, errori) {
                    alert("E' evvenuto un errore. Il stato della chiamata: " + stato);
                }
            });

        }


    </script>
    <style>
        .chosen-container-single .chosen-single {
            height: 30px;
            background: white;
            color: Black;
            font-family: "Cuprum","Roboto","Calibri",Lucida Grande,"Helvetica Neue",Helvetica,Arial,sans-serif;
            font-size: 16px;
        }

        .chosen-container {
            font-family: "Cuprum","Roboto","Calibri",Lucida Grande,"Helvetica Neue",Helvetica,Arial,sans-serif;
            font-size: 16px;
        }

        .switch {
            position: relative;
            display: inline-block;
            width: 40px;
            height: 24px;
        }

            /* Hide default HTML checkbox */
            .switch input {
                opacity: 0;
                width: 0;
                height: 0;
            }

        /* The slider */
        .slider {
            position: absolute;
            cursor: pointer;
            top: 0;
            left: 0;
            right: 0;
            bottom: 0;
            background-color: #ccc;
            -webkit-transition: .4s;
            transition: .4s;
        }

            .slider:before {
                position: absolute;
                content: "";
                height: 16px;
                width: 16px;
                left: 4px;
                bottom: 4px;
                background-color: white;
                -webkit-transition: .4s;
                transition: .4s;
            }

        input:checked + .slider {
            background-color: #2196F3;
        }

        input:focus + .slider {
            box-shadow: 0 0 1px #2196F3;
        }

        input:checked + .slider:before {
            -webkit-transform: translateX(16px);
            -ms-transform: translateX(16px);
            transform: translateX(16px);
        }

        /* Rounded sliders */
        .slider.round {
            border-radius: 34px;
        }

            .slider.round:before {
                border-radius: 50%;
            }
    </style>
</head>
<body>
    <form id="form1" runat="server">
        <asp:ScriptManager ID="ScriptManager1" runat="server" EnableScriptGlobalization="true">
            <Scripts>
                <asp:ScriptReference Path="~/Scripts/AjaxControlToolkit/Bundle" />
            </Scripts>
        </asp:ScriptManager>
        <asp:Label ID="LblBox" runat="server" Text=""></asp:Label>
        <asp:Label ID="Lbl_BarraSenior" runat="server" Text=""></asp:Label>
        <asp:Button ID="BTN_InserisciRiga" runat="server" Visible="False" />
        <asp:Button ID="BtnInsertRiga3" runat="server" Text="" Visible="false" />
        <asp:Button ID="BtnInsertRiga4" runat="server" Text="" Visible="false" />
        <asp:Button ID="BtnInsertRiga5" runat="server" Text="" Visible="false" />
        <asp:Button ID="BtnInsertRiga6" runat="server" Text="" Visible="false" />
        <asp:Button ID="BtnInsertRiga7" runat="server" Text="" Visible="false" />
        <asp:Button ID="BtnInsertRiga8" runat="server" Text="" Visible="false" />
        <asp:Button ID="BtnInsertRiga9" runat="server" Text="" Visible="false" />

        <asp:Button ID="BntFlagRosso" runat="server" Text="" Visible="false" />

        <asp:Button ID="BtnRicalcolaIVA" runat="server" Text="" Visible="false" />

        <asp:Button ID="BtnClienteFornitoreDec" runat="server" Text="" Visible="false" />

        <table style="width: 100%;" cellpadding="0" cellspacing="0">
            <tr>
                <td style="background-color: #F0F0F0; width: 160px;" class="destraclasse"></td>
                <td>
                    <div class="Titolo">Contabilità - Principale - Documenti</div>
                    <div class="SottoTitolo">
                        <br />
                        <br />
                    </div>
                </td>
                <td style="text-align: right; padding-top: 50px;">
                    <asp:ImageButton ID="BtnRicalcola" runat="server" ImageUrl="images\update.png" Width="38px" Height="38px" Style="margin-top: 12px;" />
                    <asp:Label ID="Lbl_Importo" runat="server"></asp:Label>
                </td>
            </tr>

            <tr>
                <td style="background-color: #F0F0F0; width: 185px;" class="destraclasse"></td>
                <td></td>
                <td style="text-align: right; vertical-align: top;">

                    <div class="DivTastiOspite">
                        <asp:ImageButton ID="Btn_Stampa" Height="38px" Visible="false" runat="server" ImageUrl="~/images/printer-blue.png" class="EffettoBottoniTondi" ToolTip="Stampa" />
                        <asp:ImageButton ID="Btn_Duplica" Height="38px" runat="server" ImageUrl="~/images/duplica.png" class="EffettoBottoniTondi" ToolTip="Duplica" />
                        <asp:ImageButton ID="Btn_Modifica" runat="server" ImageUrl="~/images/salva.jpg" class="EffettoBottoniTondi" ToolTip="Modifica / Inserisci (F2)" />
                        <asp:ImageButton ID="Btn_Cancella" OnClientClick="return window.confirm('Eliminare?');" runat="server" ImageUrl="~/images/elimina.jpg" class="EffettoBottoniTondi" ToolTip="Elimina" />
                    </div>

                </td>
            </tr>
        </table>

        <table width="100%" cellpadding="0" cellspacing="0">
            <tr>
                <td style="width: 160px; background-color: #F0F0F0; vertical-align: top; text-align: center;" id="BarraLaterale" class="destraclasse">

                    <a href="Menu_Generale.aspx" style="border-width: 0px;">
                        <img src="images/Home.jpg" class="Effetto" id="BOTTONEHOME" alt="Menù" /></a>
                    <br />
                    <asp:ImageButton ID="Btn_Esci" runat="server" BackColor="Transparent" ImageUrl="../images/Menu_Indietro.png" class="Effetto" ToolTip="Chiudi" />


                    <asp:ImageButton ID="ImgRic" src="images/ricerca.jpg" Width="112px" class="Effetto" ToolTip="Ricerca Registrazioni" runat="server" />
                    <br />
                    <label class="MenuDestra"><a href="#" onclick="window.open('VsPianoConti.aspx','Stampe','width=450,height=600,scrollbars=yes');">Vis. Conti</a></label>
                    <br />
                    <label class="MenuDestra"><a href="#" onclick="DialogBoxBig('Pianoconti.aspx');">Piano Conti</a></label>
                    <label class="MenuDestra"><a href="#" onclick="DialogBoxBig('AnagraficaClientiFornitori.aspx?CHIAMANTE=DOCUMENTI');">Cli./For.</a></label><br />

                    <br />
                    <asp:Label ID="Lbl_BtnLegami" runat="server" Text=""></asp:Label>
                    <asp:Label ID="Lbl_BtnScadenzario" runat="server" Text=""></asp:Label>
                    <br />


                </td>
                <ul class="custom-menu" id="rightmenu">
                </ul>
                <ul class="custom-menu" id="rightmenuIVA">
                </ul>

                <td style="vertical-align: top;" colspan="3">
                    <asp:UpdatePanel ID="Updatecontainer" runat="server">
                        <ContentTemplate>
                            <br />


                            <xasp:TabContainer ID="TabContainer1" runat="server" ActiveTabIndex="0" CssClass="TabSenior" AutoPostBack="true" Width="100%" BorderStyle="None" Style="margin-left: 4px">
                                <xasp:TabPanel runat="server" HeaderText="Testata" ID="Tab_Testata" Width="100%">
                                    <HeaderTemplate>
                                        Documento
                                    </HeaderTemplate>
                                    <ContentTemplate>
                                        <asp:Label ID="LblUltimaModifica" runat="server" Style="position: absolute; right: 10px;"></asp:Label>
                                        <table style="width: 100%;" id="Table1">


                                            <tr>
                                                <td colspan="4">
                                                    <br />
                                                    <br />

                                                </td>
                                            </tr>



                                            <tr style="height: 35px;">
                                                <td colspan="2">
                                                    <div class="containerCampiTesta">
                                                        <span class="headerCampiTesta">Causale Contabile :</span>
                                                        <asp:DropDownList ID="Dd_CausaleContabile" runat="server" Width="90%" Enabled="false" class="form-control chosen-select"></asp:DropDownList>

                                                    </div>

                                                </td>


                                                <td colspan="2">
                                                    <div class="containerCampiTesta">
                                                        <span class="headerCampiTesta">Modalità Pagametno :</span>
                                                        <asp:DropDownList ID="DD_ModalitaPagamento" runat="server" Enabled="false" Width="90%" class="form-control chosen-select"></asp:DropDownList>
                                                    </div>
                                                </td>

                                            </tr>

                                            <tr>
                                                <td colspan="4">
                                                    <br />
                                                </td>
                                            </tr>


                                            <tr>
                                                <td>
                                                    <div class="containerCampiTesta" style="width: 250px;">
                                                        <span class="headerCampiTesta">Numero :</span>
                                                        <asp:Label ID="lbl_Numero" runat="server" Text="22"></asp:Label>
                                                    </div>
                                                </td>
                                                <td>
                                                    <div class="containerCampiTesta" style="width: 250px;">
                                                        <span class="headerCampiTesta">Data Registr. :</span>
                                                        <asp:Label ID="Txt_DataRegistrazione" runat="server" Text="01/01/2019"></asp:Label>
                                                    </div>
                                                </td>

                                                <td>
                                                    <div class="containerCampiTesta" style="width: 250px;">
                                                        <span class="headerCampiTesta">Registro :</span>
                                                        <asp:Label ID="Lbl_RegistroIVA" runat="server" Text=""></asp:Label>
                                                    </div>
                                                </td>

                                                <td>
                                                    <div class="containerCampiTesta" style="width: 250px;">
                                                        <span class="headerCampiTesta">N.Protocollo :</span>
                                                        <asp:Label ID="Lbl_AnnoProtocollo" runat="server" Text=""></asp:Label>
                                                        <asp:Label ID="Lbl_Protocollo" runat="server" Text=""></asp:Label>
                                                        <asp:Label ID="Lbl_BisProtocollo" runat="server" Text=""></asp:Label>
                                                    </div>
                                                </td>
                                            </tr>


                                            <tr>
                                                <td colspan="4">
                                                    <br />
                                                </td>
                                            </tr>

                                            <tr>
                                                <td>
                                                    <div class="containerCampiTesta" style="width: 250px;">
                                                        <span class="headerCampiTesta">Data Documento :</span>
                                                        <asp:Label ID="Lbl_DataDocumento" runat="server" Text=""></asp:Label>
                                                    </div>
                                                </td>
                                                <td>
                                                    <div class="containerCampiTesta" style="width: 250px;">
                                                        <span class="headerCampiTesta">Numero Documento :</span>
                                                        <asp:Label ID="Lbl_NumeroDocumento" runat="server" Text=""></asp:Label>
                                                    </div>
                                                </td>

                                                <td>
                                                    <div class="containerCampiTesta" style="width: 250px;">
                                                        <span class="headerCampiTesta">Iva Sospesa :</span>
                                                        <asp:Label ID="Lbl_IvaSospesa" runat="server" Text=""></asp:Label>
                                                    </div>
                                                </td>

                                                <td>
                                                    <div class="containerCampiTesta" style="width: 250px;">
                                                        <span class="headerCampiTesta">Bollo Virtuale :</span>
                                                        <asp:Label ID="Lbl_BolloVirtuale" runat="server" Text=""></asp:Label>
                                                    </div>
                                                </td>


                                            </tr>

                                            <tr>
                                                <td colspan="4">
                                                    <br />
                                                </td>
                                            </tr>


                                            <tr>

                                                <td colspan="3">
                                                    <div class="containerCampiTesta" style="width: 100%;">
                                                        <span class="headerCampiTesta">Cliente/Fornitore :</span>
                                                        <asp:Label ID="Txt_ClienteFornitore" runat="server" Text=""></asp:Label>
                                                    </div>
                                                </td>
                                                <td colspan="1"></td>
                                            </tr>

                                            <tr>
                                                <td colspan="3">
                                                    <br />
                                                </td>
                                            </tr>



                                        </table>

                                        <div class="container" id="IdCostiRicavi">
                                            <div class="header">
                                                <span>Costi/Ricavi</span>

                                            </div>
                                            <div class="content" id="IdCostiRicavicontent">

                                                <asp:UpdatePanel ID="UpdatePanel2" runat="server">
                                                    <ContentTemplate>

                                                        <asp:Label ID="Lbl_ErroriCR" ForeColor="Red" runat="server"></asp:Label><br />


                                                        <asp:GridView ID="GridCostiRicavi" runat="server" CellPadding="4" Height="60px"
                                                            ShowFooter="True" BackColor="White" BorderColor="#6FA7D1"
                                                            BorderStyle="Dotted" BorderWidth="1px">
                                                            <RowStyle ForeColor="#333333" BackColor="White" />
                                                            <Columns>

                                                                <asp:TemplateField HeaderText="">
                                                                    <ItemTemplate>
                                                                        <asp:ImageButton ID="IB_Delete" CommandName="Delete" runat="Server"
                                                                            ImageUrl="~/images/cancella.png" />
                                                                    </ItemTemplate>
                                                                    <FooterTemplate>
                                                                        <div style="text-align: left" id="menutastodestro">
                                                                            <asp:ImageButton ID="ImageButton2" ImageUrl="~/images/inserisci.png" ToolTip="Inserisci (F9)"
                                                                                CommandName="Inserisci" runat="server" />
                                                                        </div>
                                                                    </FooterTemplate>
                                                                </asp:TemplateField>


                                                                <asp:TemplateField HeaderText="SOTTOCONTO" ItemStyle-Width="250px">
                                                                    <ItemTemplate>
                                                                        <asp:TextBox ID="TxtSottocontoCR" Text='<%# Eval("Sottoconto") %>' CssClass="MyAutoComplete" runat="server" Width="250px"></asp:TextBox>
                                                                    </ItemTemplate>
                                                                    <HeaderStyle Font-Bold="False" Font-Italic="False" />
                                                                </asp:TemplateField>
                                                                <asp:TemplateField HeaderText="DARE AVERE">
                                                                    <ItemTemplate>
                                                                        <asp:RadioButton ID="RBDareCR" Text="Dare" GroupName="DareAvereCR" runat="server" />
                                                                        <asp:RadioButton ID="RBAvereCR" Text="Avere" GroupName="DareAvereCR" runat="server" />
                                                                    </ItemTemplate>
                                                                    <HeaderStyle Font-Bold="False" Font-Italic="False" />
                                                                </asp:TemplateField>
                                                                <asp:TemplateField HeaderText="PRORATA" ItemStyle-Width="80px">
                                                                    <ItemTemplate>
                                                                        <asp:RadioButton ID="RBPorataSiCR" Text="SI" GroupName="PorataCR" runat="server" />
                                                                        <asp:RadioButton ID="RBPorataNoCR" Text="NO" GroupName="PorataCR" runat="server" />
                                                                    </ItemTemplate>
                                                                    <HeaderStyle Font-Bold="False" Font-Italic="False" />
                                                                </asp:TemplateField>
                                                                <asp:TemplateField HeaderText="QUANTITA" ItemStyle-Width="80px">
                                                                    <ItemTemplate>
                                                                        <asp:TextBox ID="TxtQuantitaCR" Style="text-align: right;" autocomplete="off" runat="server" Text='<%# Eval("Quantita") %>' Width="80px"></asp:TextBox>
                                                                    </ItemTemplate>
                                                                    <HeaderStyle Font-Bold="False" Font-Italic="False" />
                                                                </asp:TemplateField>
                                                                <asp:TemplateField HeaderText="IMPORTO" ItemStyle-Width="80px">
                                                                    <ItemTemplate>
                                                                        <asp:TextBox ID="TxtImportoCR" Style="text-align: right;" autocomplete="off" runat="server" Width="80px" Text='<%# Eval("Importo") %>'></asp:TextBox>
                                                                    </ItemTemplate>
                                                                    <HeaderStyle Font-Bold="False" Font-Italic="False" />
                                                                </asp:TemplateField>
                                                                <asp:TemplateField HeaderText="IVA">

                                                                    <ItemTemplate>
                                                                        <center>
                                      <asp:DropDownList ID="DDIvaCr" runat="server"></asp:DropDownList>        
                                    </center>
                                                                    </ItemTemplate>
                                                                    <HeaderStyle Font-Bold="False" Font-Italic="False" />
                                                                </asp:TemplateField>
                                                                <asp:TemplateField HeaderText="SOTTOCONTO PARTITA" ItemStyle-Width="200px">
                                                                    <ItemTemplate>
                                                                        <asp:TextBox ID="TxtSottocontoControPartitaCR" CssClass="MyAutoComplete" runat="server" Text='<%# Eval("SottocontoControPartita") %>' Width="200px"></asp:TextBox>
                                                                    </ItemTemplate>
                                                                    <HeaderStyle Font-Bold="False" Font-Italic="False" />
                                                                </asp:TemplateField>
                                                                <asp:TemplateField HeaderText="DESCRIZIONE" ItemStyle-Width="200px">
                                                                    <ItemTemplate>
                                                                        <asp:TextBox ID="TxtDescrizioneCr" TextMode="MultiLine" Text='<%# Eval("Descrizione") %>' autocomplete="off" runat="server" MaxLength="150" Width="200px"></asp:TextBox>
                                                                    </ItemTemplate>
                                                                    <HeaderStyle Font-Bold="False" Font-Italic="False" />
                                                                </asp:TemplateField>
                                                                <asp:TemplateField HeaderText="ANNO" ItemStyle-Width="70px">
                                                                    <ItemTemplate>
                                                                        <center>
                                        <asp:TextBox ID="TxtAnnoCr" Text='<%# Eval("Anno") %>'  autocomplete="off" onkeypress="return soloNumeri(event);" MaxLength="4" runat="server" Width="70px"></asp:TextBox>                                        
                                    </center>
                                                                    </ItemTemplate>
                                                                    <HeaderStyle Font-Bold="False" Font-Italic="False" />
                                                                </asp:TemplateField>
                                                                <asp:TemplateField HeaderText="MESE">
                                                                    <ItemTemplate>
                                                                        <center>
                                    <asp:DropDownList ID="DDMeseCr" runat="server">
                                        <asp:ListItem Value="0">Seleziona Mese</asp:ListItem>
                                        <asp:ListItem Text="Gennaio" Value="1" ></asp:ListItem>
                                        <asp:ListItem Text="Febbraio" Value="2" ></asp:ListItem>
                                        <asp:ListItem Text="Marzo" Value="3" ></asp:ListItem>
                                        <asp:ListItem Text="Aprile" Value="4" ></asp:ListItem>
                                        <asp:ListItem Text="Maggio" Value="5" ></asp:ListItem>
                                        <asp:ListItem Text="Giugno" Value="6" ></asp:ListItem>
                                        <asp:ListItem Text="Luglio" Value="7" ></asp:ListItem>
                                        <asp:ListItem Text="Agosto" Value="8" ></asp:ListItem>
                                        <asp:ListItem Text="Settembre" Value="9" ></asp:ListItem>
                                        <asp:ListItem Text="Ottobre" Value="10" ></asp:ListItem>
                                        <asp:ListItem Text="Novembre" Value="11" ></asp:ListItem>
                                        <asp:ListItem Text="Dicembre" Value="12" ></asp:ListItem>
                                    </asp:DropDownList>                                        
                                    </center>
                                                                        <asp:TextBox ID="Txt_RigaRegistrazione" runat="server" Visible="false" Text="0"></asp:TextBox>
                                                                    </ItemTemplate>
                                                                    <HeaderStyle Font-Bold="False" Font-Italic="False" />
                                                                </asp:TemplateField>
                                                                <asp:TemplateField HeaderText="RIGA CAUSALE" ItemStyle-Width="50">
                                                                    <ItemTemplate>
                                                                        <center>
                                        <asp:TextBox ID="Txt_RigaDaCausale" runat="server" Width="50" Visible="true" Text="0"></asp:TextBox>
                                    </center>
                                                                    </ItemTemplate>
                                                                    <HeaderStyle Font-Bold="False" Font-Italic="False" />
                                                                </asp:TemplateField>
                                                                <asp:TemplateField HeaderText="CENTRO SERVIZIO">
                                                                    <ItemTemplate>
                                                                        <center>
                                    <asp:DropDownList ID="DDCentroServizio" runat="server">
                                    </asp:DropDownList>                                        
                                    </center>
                                                                    </ItemTemplate>
                                                                    <HeaderStyle Font-Bold="False" Font-Italic="False" />
                                                                </asp:TemplateField>
                                                                <asp:TemplateField HeaderText="TIPO EXTRA" ItemStyle-Width="50">
                                                                    <ItemTemplate>
                                                                        <center>
                                        <asp:TextBox ID="Txt_TIPOEXTRA" runat="server" Width="50" Visible="true" Text="0"></asp:TextBox>
                                    </center>
                                                                    </ItemTemplate>
                                                                    <HeaderStyle Font-Bold="False" Font-Italic="False" />
                                                                </asp:TemplateField>
                                                            </Columns>
                                                            <FooterStyle BackColor="White" ForeColor="#023102" />
                                                            <PagerStyle BackColor="#336666" ForeColor="White" HorizontalAlign="Center" />
                                                            <SelectedRowStyle BackColor="#339966" Font-Bold="True" ForeColor="White" />
                                                            <HeaderStyle BackColor="#A6C9E2" Font-Bold="False" ForeColor="White" BorderColor="#6FA7D1" BorderWidth="1" />
                                                        </asp:GridView>
                                                    </ContentTemplate>
                                                </asp:UpdatePanel>
                                                <br />
                                            </div>
                                        </div>
                                        <br />
                                        <div class="container" id="idRitenute">
                                            <div class="header">
                                                <span>Ritenute</span>

                                            </div>
                                            <div class="content" id="idRitenutecontent">
                                                <asp:UpdatePanel ID="UpdatePanel3" runat="server">
                                                    <ContentTemplate>
                                                        <asp:Label ID="Lbl_RitenutaEr" ForeColor="Red" runat="server"></asp:Label>
                                                        <br />
                                                        <asp:GridView ID="GridRitenuta" runat="server" CellPadding="4" Height="60px"
                                                            ShowFooter="True" BackColor="White" BorderColor="#6FA7D1"
                                                            BorderStyle="Dotted" BorderWidth="1px">
                                                            <RowStyle ForeColor="#333333" BackColor="White" />
                                                            <Columns>


                                                                <asp:TemplateField HeaderText="">
                                                                    <ItemTemplate>
                                                                        <asp:ImageButton ID="IB_Delete" CommandName="Delete" runat="Server"
                                                                            ImageUrl="~/images/cancella.png" />
                                                                    </ItemTemplate>
                                                                    <FooterTemplate>
                                                                        <div style="text-align: left">
                                                                            <asp:ImageButton ID="ImageButton3" ImageUrl="~/images/inserisci.png" ToolTip="Inserisci (F9)"
                                                                                CommandName="Inserisci" runat="server" />
                                                                        </div>
                                                                    </FooterTemplate>
                                                                </asp:TemplateField>


                                                                <asp:TemplateField HeaderText="IMPONIBILE" ItemStyle-Width="100px">
                                                                    <ItemTemplate>
                                                                        <asp:TextBox ID="TxtImponibileRT" Style="text-align: right;" Text='<%# Eval("Imponibile") %>' autocomplete="off" runat="server" Width="100px"></asp:TextBox>
                                                                    </ItemTemplate>
                                                                    <HeaderStyle Font-Bold="False" Font-Italic="False" />
                                                                </asp:TemplateField>
                                                                <asp:TemplateField HeaderText="TIPO RITENUTA" ItemStyle-Width="150px">
                                                                    <ItemTemplate>
                                                                        <asp:DropDownList ID="DDTRitenutaRt" AutoPostBack="true" OnSelectedIndexChanged="Dd_Ritenuta_SelectedIndexChanged" runat="server" Width="150px">
                                                                        </asp:DropDownList>
                                                                    </ItemTemplate>
                                                                    <HeaderStyle Font-Bold="False" Font-Italic="False" />
                                                                </asp:TemplateField>
                                                                <asp:TemplateField HeaderText="RITENUTA" ItemStyle-Width="100px">
                                                                    <ItemTemplate>
                                                                        <asp:TextBox ID="TxtRitenutaRT" Style="text-align: right;" Text='<%# Eval("Ritenuta") %>' autocomplete="off" runat="server" Width="100px"></asp:TextBox>
                                                                    </ItemTemplate>
                                                                    <HeaderStyle Font-Bold="False" Font-Italic="False" />
                                                                </asp:TemplateField>
                                                                <asp:TemplateField HeaderText="SOTTOCONTO DARE" ItemStyle-Width="250px">
                                                                    <ItemTemplate>
                                                                        <asp:TextBox ID="TxtSottocontoDareRT" CssClass="MyAutoComplete" runat="server" Text='<%# Eval("SottocontoDare") %>' Width="250px"></asp:TextBox>
                                                                    </ItemTemplate>
                                                                    <HeaderStyle Font-Bold="False" Font-Italic="False" />
                                                                </asp:TemplateField>
                                                                <asp:TemplateField HeaderText="SOTTOCONTO AVERE" ItemStyle-Width="250px">
                                                                    <ItemTemplate>
                                                                        <asp:TextBox ID="TxtSottocontoAvereRT" Text='<%# Eval("SottocontoAvere") %>' CssClass="MyAutoComplete" runat="server" Width="250px"></asp:TextBox>
                                                                    </ItemTemplate>
                                                                    <HeaderStyle Font-Bold="False" Font-Italic="False" />
                                                                </asp:TemplateField>
                                                                <asp:TemplateField HeaderText="DESCRIZIONE" ItemStyle-Width="200px">
                                                                    <ItemTemplate>
                                                                        <asp:TextBox ID="TxtDescrizioneRT" Text='<%# Eval("Descrizione") %>' autocomplete="off" runat="server" MaxLength="50" Width="200px"></asp:TextBox>
                                                                    </ItemTemplate>
                                                                    <HeaderStyle Font-Bold="False" Font-Italic="False" />
                                                                </asp:TemplateField>
                                                            </Columns>
                                                            <FooterStyle BackColor="White" ForeColor="#023102" />
                                                            <PagerStyle BackColor="#336666" ForeColor="White" HorizontalAlign="Center" />
                                                            <SelectedRowStyle BackColor="#339966" Font-Bold="True" ForeColor="White" />
                                                            <HeaderStyle BackColor="#A6C9E2" Font-Bold="False" ForeColor="White" BorderColor="#6FA7D1" BorderWidth="1" />
                                                        </asp:GridView>
                                                    </ContentTemplate>
                                                </asp:UpdatePanel>
                                            </div>
                                        </div>
                                        <br />

                                        <div class="container" id="Ivacontainer">
                                            <div class="header">
                                                <span>IVA</span>

                                            </div>
                                            <div class="content" id="Ivacontent">
                                                <asp:UpdatePanel ID="UpdatePanel1" runat="server">
                                                    <ContentTemplate>

                                                        <asp:Label ID="Lbl_ErroriIVA" ForeColor="Red" runat="server"></asp:Label><br />
                                                        <asp:GridView ID="GridIVA" runat="server" CellPadding="4" Height="60px"
                                                            ShowFooter="True" BackColor="White" BorderColor="#6FA7D1"
                                                            BorderStyle="Dotted" BorderWidth="1px">
                                                            <RowStyle ForeColor="#333333" BackColor="White" />
                                                            <Columns>


                                                                <asp:TemplateField HeaderText="">
                                                                    <HeaderTemplate>
                                                                        <div style="text-align: left; width: 45px; height: 25px;" id="menutastodestroIVA">
                                                                        </div>
                                                                    </HeaderTemplate>
                                                                    <ItemTemplate>
                                                                        <asp:ImageButton ID="IB_Delete" CommandName="Delete" runat="Server"
                                                                            ImageUrl="~/images/cancella.png" />
                                                                    </ItemTemplate>
                                                                    <FooterTemplate>
                                                                        <div style="text-align: left">
                                                                            <asp:ImageButton ID="ImageButton1" ImageUrl="~/images/inserisci.png" ToolTip="Inserisci (F9)"
                                                                                CommandName="Inserisci" runat="server" />
                                                                        </div>
                                                                    </FooterTemplate>
                                                                </asp:TemplateField>

                                                                <asp:TemplateField HeaderText="IMPONIBILE" ItemStyle-Width="100px">
                                                                    <ItemTemplate>
                                                                        <asp:TextBox ID="TxtImponibileIVA" Style="text-align: right;" Text='<%# Eval("Imponibile") %>' autocomplete="off" runat="server" Width="100px"></asp:TextBox>
                                                                    </ItemTemplate>
                                                                    <HeaderStyle Font-Bold="False" Font-Italic="False" />
                                                                </asp:TemplateField>

                                                                <asp:TemplateField HeaderText="IVA" ItemStyle-Width="150px">
                                                                    <ItemTemplate>
                                                                        <center>
                                        <asp:DropDownList ID="DDIva" AutoPostBack="True"   OnSelectedIndexChanged="Dd_IVA_SelectedIndexChanged"  runat="server"></asp:DropDownList>
                                    </center>
                                                                    </ItemTemplate>
                                                                    <HeaderStyle Font-Bold="False" Font-Italic="False" />
                                                                </asp:TemplateField>
                                                                <asp:TemplateField HeaderText="IMPOSTA" ItemStyle-Width="100px">
                                                                    <ItemTemplate>
                                                                        <asp:TextBox ID="TxtImpostaIVA" Style="text-align: right;" autocomplete="off" runat="server" Text='<%# Eval("Imposta") %>' Width="100px" AutoPostBack="True" OnTextChanged="TxtImpostaChanged"> </asp:TextBox>
                                                                    </ItemTemplate>
                                                                    <HeaderStyle Font-Bold="False" Font-Italic="False" />
                                                                </asp:TemplateField>
                                                                <asp:TemplateField HeaderText="DETRAIBILE" ItemStyle-Width="150px">

                                                                    <ItemTemplate>
                                                                        <center>
                                        <asp:DropDownList ID="DDDetraibileIVA" runat="server" Width="150px"></asp:DropDownList>
                                    </center>
                                                                    </ItemTemplate>
                                                                    <HeaderStyle Font-Bold="False" Font-Italic="False" />
                                                                </asp:TemplateField>
                                                                <asp:TemplateField HeaderText="SOTTOCONTO" ItemStyle-Width="300px">
                                                                    <EditItemTemplate>
                                                                    </EditItemTemplate>
                                                                    <ItemTemplate>
                                                                        <asp:TextBox ID="TxtSottocontoIVA" CssClass="MyAutoComplete" Text='<%# Eval("Sottoconto") %>' runat="server" Width="300px"></asp:TextBox>
                                                                    </ItemTemplate>
                                                                    <HeaderStyle Font-Bold="False" Font-Italic="False" />
                                                                </asp:TemplateField>
                                                                <asp:TemplateField HeaderText="DARE AVERE" ItemStyle-Width="120px">
                                                                    <ItemTemplate>
                                                                        <asp:RadioButton ID="RBDareIVA" Text="Dare" GroupName="DareAvere" runat="server" />
                                                                        <asp:RadioButton ID="RBAvereIVA" Text="Avere" GroupName="DareAvere" runat="server" />
                                                                    </ItemTemplate>
                                                                    <HeaderStyle Font-Bold="False" Font-Italic="False" />
                                                                </asp:TemplateField>
                                                                <asp:TemplateField HeaderText="DESCRIZIONE" ItemStyle-Width="350px">
                                                                    <ItemTemplate>
                                                                        <asp:TextBox ID="TxtDescrizioneIVA" autocomplete="off" Text='<%# Eval("Descrizione") %>' MaxLength="50" runat="server" Width="350px"></asp:TextBox>
                                                                    </ItemTemplate>
                                                                    <HeaderStyle Font-Bold="False" Font-Italic="False" />
                                                                </asp:TemplateField>
                                                            </Columns>
                                                            <FooterStyle BackColor="White" ForeColor="#023102" />
                                                            <PagerStyle BackColor="#336666" ForeColor="White" HorizontalAlign="Center" />
                                                            <SelectedRowStyle BackColor="#339966" Font-Bold="True" ForeColor="White" />
                                                            <HeaderStyle BackColor="#A6C9E2" Font-Bold="False" ForeColor="White" BorderColor="#6FA7D1" BorderWidth="1" />
                                                        </asp:GridView>
                                                    </ContentTemplate>
                                                </asp:UpdatePanel>
                                            </div>
                                        </div>

                                        <br />
                                        <div class="container" id="idNOTE">
                                            <div class="header">
                                                <span>NOTE</span>
                                            </div>
                                            <div class="content" id="idNOTEcontent">
                                                Note<br />
                                                <asp:TextBox ID="Txt_DescrizioneTesta" runat="server" Height="288px" Width="728px" MaxLength="250" TextMode="MultiLine"></asp:TextBox>
                                            </div>
                                        </div>



                                        <br />

                                        <div class="container" id="idAltriDati">
                                            <div class="header">
                                                <span>Altri Dati</span>
                                            </div>
                                            <div class="content" id="idAltriDaticontent">
                                                <br />
                                                <br />
                                                Riferimento Data:<br />
                                                <label style="display: block; float: left; width: 200px;">Anno:</label>
                                                <asp:TextBox ID="Txt_Anno" autocomplete="off" onkeypress="return soloNumeri(event);" runat="server" AutoPostBack="True" Width="150px" MaxLength="4"></asp:TextBox>
                                                <asp:CheckBox ID="Chk_FatturazioneAnticipata" runat="server" Text="Fatturazione Anticipata" />
                                                <br />
                                                <br />
                                                <label style="display: block; float: left; width: 200px;">Mese</label>
                                                <asp:DropDownList ID="Dd_Mese" runat="server" Width="150px">
                                                    <asp:ListItem Value="0">Seleziona Mese</asp:ListItem>
                                                    <asp:ListItem Value="1">Gennaio</asp:ListItem>
                                                    <asp:ListItem Value="2">Febbraio</asp:ListItem>
                                                    <asp:ListItem Value="3">Marzo</asp:ListItem>
                                                    <asp:ListItem Value="4">Aprile</asp:ListItem>
                                                    <asp:ListItem Value="5">Maggio</asp:ListItem>
                                                    <asp:ListItem Value="6">Giugno</asp:ListItem>
                                                    <asp:ListItem Value="7">Luglio</asp:ListItem>
                                                    <asp:ListItem Value="8">Agosto</asp:ListItem>
                                                    <asp:ListItem Value="9">Settembre</asp:ListItem>
                                                    <asp:ListItem Value="10">Ottobre</asp:ListItem>
                                                    <asp:ListItem Value="11">Novembre</asp:ListItem>
                                                    <asp:ListItem Value="12">Dicembre</asp:ListItem>
                                                </asp:DropDownList>
                                                <br />
                                                <br />
                                                <label style="display: block; float: left; width: 200px;">Contratto:</label>
                                                <asp:TextBox ID="Txt_Contratto" runat="server" Width="150px" MaxLength="10"></asp:TextBox><br />
                                                <br />
                                                <label style="display: block; float: left; width: 200px;">Leasing</label>
                                                <asp:DropDownList ID="DD_Leasing" Width="150px" runat="server"></asp:DropDownList><br />
                                                <br />
                                                <label style="display: block; float: left; width: 200px;">Centro Servizio:</label>
                                                <asp:DropDownList ID="DD_CentroServizio" runat="server" Width="429px"></asp:DropDownList>
                                                <br />
                                                <br />
                                                <label style="display: block; float: left; width: 200px;">Riferimento Retta:</label>
                                                <asp:DropDownList ID="DD_Tipologia" runat="server">
                                                    <asp:ListItem></asp:ListItem>
                                                    <asp:ListItem Value="O">Ospiti</asp:ListItem>
                                                    <asp:ListItem Value="P">Parenti</asp:ListItem>
                                                    <asp:ListItem Value="C">Comuni</asp:ListItem>
                                                    <asp:ListItem Value="J">Jolly</asp:ListItem>
                                                    <asp:ListItem Value="R">Regioni</asp:ListItem>
                                                </asp:DropDownList>
                                                <br />
                                                <br />
                                                <label style="display: block; float: left; width: 200px;">Comune:</label>
                                                <asp:DropDownList ID="DD_Comune" runat="server" class="chosen-select"></asp:DropDownList>
                                                <br />
                                                <br />
                                                <label style="display: block; float: left; width: 200px;">Regione:</label>
                                                <asp:DropDownList ID="DD_Regione" runat="server" class="chosen-select"></asp:DropDownList>
                                                <br />
                                                <br />
                                                <hr />
                                                <label style="display: block; float: left; width: 250px;">Riferimento per creazione retta</label><br />
                                                <br />


                                                <label style="display: block; float: left; width: 200px;">Anno:</label>
                                                <asp:TextBox ID="Txt_AnnoRif" autocomplete="off" onkeypress="return soloNumeri(event);" runat="server" AutoPostBack="True" Width="150px" MaxLength="4"></asp:TextBox>
                                                <br />
                                                <br />




                                                <label style="display: block; float: left; width: 200px;">Mese:</label>
                                                <asp:DropDownList ID="DD_MeseRif" runat="server" Width="150px">
                                                    <asp:ListItem Value="0">Seleziona Mese</asp:ListItem>
                                                    <asp:ListItem Value="1">Gennaio</asp:ListItem>
                                                    <asp:ListItem Value="2">Febbraio</asp:ListItem>
                                                    <asp:ListItem Value="3">Marzo</asp:ListItem>
                                                    <asp:ListItem Value="4">Aprile</asp:ListItem>
                                                    <asp:ListItem Value="5">Maggio</asp:ListItem>
                                                    <asp:ListItem Value="6">Giugno</asp:ListItem>
                                                    <asp:ListItem Value="7">Luglio</asp:ListItem>
                                                    <asp:ListItem Value="8">Agosto</asp:ListItem>
                                                    <asp:ListItem Value="9">Settembre</asp:ListItem>
                                                    <asp:ListItem Value="10">Ottobre</asp:ListItem>
                                                    <asp:ListItem Value="11">Novembre</asp:ListItem>
                                                    <asp:ListItem Value="12">Dicembre</asp:ListItem>
                                                </asp:DropDownList>




                                                <br />
                                                <hr />
                                                <label style="display: block; float: left; width: 250px;">Riferimento per 730</label><br />
                                                <br />




                                                <label style="display: block; float: left; width: 200px;">Riferimento Numero:</label>
                                                <asp:TextBox ID="Txt_RifNumero" autocomplete="off" runat="server" Width="150px"></asp:TextBox>




                                                <br />



                                                <br />




                                                <label style="display: block; float: left; width: 200px;">Riferimento Data:</label>
                                                <asp:TextBox ID="Txt_DataRif" autocomplete="off" runat="server" Width="150px"></asp:TextBox>



                                                <br />
                                                <hr />
                                                <label style="display: block; float: left; width: 250px;">Riferimento per SEPA</label><br />
                                                <br />

                                                <label style="display: block; float: left; width: 200px;">Riferimento Numero:</label>
                                                <asp:TextBox ID="Txt_IDSEPA" autocomplete="off" runat="server" Width="150px"></asp:TextBox>
                                                <br />
                                                <br />

                                                <label style="display: block; float: left; width: 200px;">Data Sepa</label>
                                                <asp:TextBox ID="Txt_DataSepa" autocomplete="off" runat="server" Width="150px"></asp:TextBox>
                                                <br />
                                                <br />
                                                <hr />
                                                <br />
                                                <label style="display: block; float: left; width: 200px;">Export</label>
                                                <asp:TextBox ID="Txt_ExportData" autocomplete="off" runat="server" Width="150px"></asp:TextBox><br />

                                            </div>
                                        </div>

                                        <br />
                                    </ContentTemplate>
                                </xasp:TabPanel>
                            </xasp:TabContainer>

                        </ContentTemplate>
                    </asp:UpdatePanel>

                </td>
            </tr>
        </table>

        <asp:Timer ID="Timer1" runat="server">
        </asp:Timer>
    </form>
</body>

</html>

