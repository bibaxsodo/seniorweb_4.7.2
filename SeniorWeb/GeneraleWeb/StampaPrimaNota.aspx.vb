﻿Imports Microsoft.VisualBasic
Imports System.Data.OleDb

Partial Class GeneraleWeb_StampaPrimaNota
    Inherits System.Web.UI.Page

    Function CondizioneCserv() As String
        Dim Condizione As String = ""
        If DD_CServ.SelectedValue = "" Then
            If DD_Struttura.SelectedValue <> "" Then
                Dim Indice As Integer
                Call AggiornaCServ()
                Condizione = Condizione & " ("
                For Indice = 0 To DD_CServ.Items.Count - 1
                    If DD_CServ.Items(Indice).Value <> "" Then
                        If Indice >= 1 Then
                            If Condizione <> "" Then
                                Condizione = Condizione & " Or "
                            End If
                        End If

                        Condizione = Condizione & "  MovimentiContabiliTesta.CentroServizio = '" & DD_CServ.Items(Indice).Value & "'"
                    End If
                Next
                Condizione = Condizione & ") "
            End If
        Else
            Condizione = Condizione & "  MovimentiContabiliTesta.CentroServizio = '" & DD_CServ.SelectedValue & "'"
        End If

        Return Condizione
    End Function

    Private Sub CreaArchivio()
        Dim Con As New OleDbConnection        
        Dim MySql As String
        Dim Condizione As String
        Dim Stampa As New StampeGenerale

        ViewState("PRINTERKEY") = Format(Now, "yyyyMMddHHmmss") & Session.SessionID



        Con.ConnectionString = Session("DC_GENERALE")
        Con.Open()



        Condizione = ""
        If IsDate(Txt_DallaData.Text) Then
            If Condizione <> "" Then
                Condizione = Condizione & " And "
            End If
            Condizione = Condizione & " DataRegistrazione >= ?"
        End If
        If IsDate(Txt_DallaData.Text) Then
            If Condizione <> "" Then
                Condizione = Condizione & " And "
            End If
            Condizione = Condizione & " DataRegistrazione <= ?"
        End If


        If Condizione = "" Then
            Exit Sub
        End If

        If CondizioneCserv() <> "" Then
            Condizione = Condizione & " And " & CondizioneCserv()
        End If

        MySql = "Select  *,MovimentiContabiliTesta.Descrizione as DescrizioneTesta FROM MovimentiContabiliRiga INNER JOIN MovimentiContabiliTesta ON MovimentiContabiliRiga.Numero = MovimentiContabiliTesta.NumeroRegistrazione Where (Invisibile Is Null or Invisibile = 0) And " & Condizione
        Dim cmd As New OleDbCommand()
        cmd.CommandText = MySql
        cmd.Connection = Con

        If IsDate(Txt_DallaData.Text) Then
            cmd.Parameters.AddWithValue("@DALLADATA", Txt_DallaData.Text)
        End If
        If IsDate(Txt_AllaData.Text) Then
            cmd.Parameters.AddWithValue("@ALLADATA", Txt_AllaData.Text)
        End If


        Dim Reader As OleDbDataReader = cmd.ExecuteReader()
        Do While Reader.Read


            'Dim cmdScrivi As New OleDbCommand()
            'cmdScrivi.CommandText = "INSERT INTO Mastrino (ChiaveSelezione,IntestaSocieta,PRINTERKEY,DecodificaCausaleContabile,NumeroRegistrazione,MastroPartita,ContoPartita,SottocontoPartita,DescrizioneSottocontoPartita,DataRegistrazione,Descrizione,Dare,Avere,ProgressivoAnno,ProgressivoNumero) VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)"
            'cmdScrivi.Connection = ConStampa

            Dim WrRs As System.Data.DataRow = Stampa.Tables("Mastrino").NewRow

            WrRs.Item("ChiaveSelezione") = "Periodo dal " & Txt_DallaData.Text & " al " & Txt_AllaData.Text

            Dim MyClsLogin As New Cls_Login

            MyClsLogin.Utente = Session("UTENTE")

            MyClsLogin.LeggiSP(Application("SENIOR"))

            WrRs.Item("IntestaSocieta") = MyClsLogin.RagioneSociale
            'WrRs.Item("PRINTERKEY") = ViewState("PRINTERKEY")

            Dim kCau As New Cls_CausaleContabile

            kCau.Leggi(Session("DC_TABELLE"), campodb(Reader.Item("CAUSALECONTABILE")))

            WrRs.Item("DecodificaCausaleContabile") = kCau.Descrizione
            WrRs.Item("NumeroRegistrazione") = campodbN(Reader.Item("NumeroRegistrazione"))
            WrRs.Item("MastroPartita") = campodbN(Reader.Item("MastroPartita"))
            WrRs.Item("ContoPartita") = campodbN(Reader.Item("ContoPartita"))
            WrRs.Item("SottocontoPartita") = campodbN(Reader.Item("SottocontoPartita"))
            Dim K As New Cls_Pianodeiconti
            K.Mastro = campodbN(Reader.Item("MastroPartita"))
            K.Conto = campodbN(Reader.Item("ContoPartita"))
            K.Sottoconto = campodbN(Reader.Item("SottocontoPartita"))
            K.Decodfica(Session("DC_GENERALE"))

            WrRs.Item("DescrizioneSottocontoPartita") = K.Descrizione
            WrRs.Item("DataRegistrazione") = campodb(Reader.Item("DataRegistrazione"))
            If Len(campodb(Reader.Item("DescrizioneTesta"))) > 240 Then
                WrRs.Item("Descrizione") = Mid(campodb(Reader.Item("DescrizioneTesta")), 1, 240)
            Else
                WrRs.Item("Descrizione") = campodb(Reader.Item("DescrizioneTesta"))
            End If
            If campodb(Reader.Item("DareAvere")) = "A" Then
                WrRs.Item("Dare") = 0
                WrRs.Item("Avere") = campodbN(Reader.Item("Importo"))
            Else
                WrRs.Item("Dare") = campodbN(Reader.Item("Importo"))
                WrRs.Item("Avere") = 0
            End If
            WrRs.Item("ProgressivoAnno") = campodbN(Reader.Item("ProgressivoAnno"))
            WrRs.Item("ProgressivoNumero") = campodbN(Reader.Item("ProgressivoNumero"))

            'cmdScrivi.ExecuteNonQuery()
            Stampa.Tables("Mastrino").Rows.Add(WrRs)
        Loop
        Con.Close()        
        Session("stampa") = Stampa
    End Sub

    Protected Sub Img_Stampa_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles Img_Stampa.Click
        If Not IsDate(Txt_DallaData.Text) Then
            ScriptManager.RegisterClientScriptBlock(Me, Me.GetType(), "Errore", "VisualizzaErrore('Data Dalla errata');", True)

            EseguiJS()
            Exit Sub
        End If

        If Not IsDate(Txt_AllaData.Text) Then
            ScriptManager.RegisterClientScriptBlock(Me, Me.GetType(), "Errore", "VisualizzaErrore('Data alla errata');", True)

            EseguiJS()
            Exit Sub
        End If


        Dim DataDal As Date
        Dim DataAl As Date
        DataDal = Txt_DallaData.Text
        DataAl = Txt_AllaData.Text

        If Format(DataDal, "yyyyMMdd") > Format(DataAl, "yyyyMMdd") Then
            ScriptManager.RegisterClientScriptBlock(Me, Me.GetType(), "Errore", "VisualizzaErrore('Data Dal Maggiore data Al');", True)

            EseguiJS()
            Exit Sub
        End If



        CreaArchivio()

        If RB_TotaleGiorno.Checked = False And RB_TotaleRegistrazione.Checked = False Then
            ScriptManager.RegisterClientScriptBlock(Me, Me.GetType(), "Stampa1", "openPopUp('Stampa_ReportXSD.aspx?REPORT=PRIMANOTA&PRINTERKEY=ON','Stampe','width=800,height=600');", True)
            EseguiJS()
            Exit Sub
        End If

        If RB_TotaleGiorno.Checked = True Then
            ScriptManager.RegisterClientScriptBlock(Me, Me.GetType(), "Stampa2", "openPopUp('Stampa_ReportXSD.aspx?REPORT=PRIMANOTAGIORNO&PRINTERKEY=ON','Stampe','width=800,height=600');", True)
            EseguiJS()
            Exit Sub
        End If

        If RB_TotaleRegistrazione.Checked = True Then            
            ScriptManager.RegisterClientScriptBlock(Me, Me.GetType(), "Stampa3", "openPopUp('Stampa_ReportXSD.aspx?REPORT=PRIMANOTAREGISTRAZIONE&PRINTERKEY=ON','Stampe','width=800,height=600');", True)
            EseguiJS()
            Exit Sub
        End If


    End Sub

    Protected Sub Btn_Esci_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles Btn_Esci.Click
        Response.Redirect("Menu_Stampe.aspx")

    End Sub

    Function campodb(ByVal oggetto As Object) As String
        If IsDBNull(oggetto) Then
            Return ""
        Else
            Return oggetto
        End If
    End Function

    Function campodbN(ByVal oggetto As Object) As Double
        If IsDBNull(oggetto) Then
            Return 0
        Else
            Return oggetto
        End If
    End Function


    Private Sub EseguiJS()
        Dim MyJs As String
        MyJs = "$(document).ready(function() {"

        MyJs = MyJs & "var els = document.getElementsByTagName(""*"");"

        MyJs = MyJs & "for (var i=0;i<els.length;i++)"
        MyJs = MyJs & "if ( els[i].id ) { "
        MyJs = MyJs & " var appoggio =els[i].id; "
        'MyJs = MyJs & " if (appoggio.match('TxtSottoconto')!= null) {  "
        'MyJs = MyJs & " $(els[i]).autocomplete('/WebHandler/GestioneConto.ashx?Utente=" & Session("UTENTE") & "', {delay:5,minChars:3});"
        'MyJs = MyJs & "    }"

        MyJs = MyJs & " if ((appoggio.match('Txt_DallaData')!= null) || (appoggio.match('Txt_AllaData')!= null)) {  "
        MyJs = MyJs & " $(els[i]).mask(""99/99/9999"");  "
        MyJs = MyJs & " $(els[i]).datepicker({ changeMonth: true,changeYear: true,  yearRange: ""1890:" & Year(Now) + 1 & """},$.datepicker.regional[""it""]);"
        MyJs = MyJs & "    }"

        'MyJs = MyJs & " if (appoggio.match('Txt_Importo')!= null)  {  "
        'MyJs = MyJs & " $(els[i]).keypress(function() { ForceNumericInput($(this).val(), true, true); } ); "
        'MyJs = MyJs & " $(els[i]).blur(function() { var ap = formatNumber($(this).val(),2); $(this).val(ap); });"
        'MyJs = MyJs & "    }"

        MyJs = MyJs & "} "
        MyJs = MyJs & "});"

        'MyJs = "$(document).ready(function() { $('.myClass').keypress(function() { handleEnter($('.myClass').val(), true, true); } );     $('#" & Txt_ImportoPagato.ClientID & "').blur(function() { var ap = formatNumber ($('#" & Txt_ImportoPagato.ClientID & "').val(),2); $('#" & Txt_ImportoPagato.ClientID & "').val(ap); }); });"
        ScriptManager.RegisterStartupScript(Me, Me.GetType(), "visualizzaRitIm", MyJs, True)
    End Sub


    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load

        EseguiJS()

        If Page.IsPostBack = True Then Exit Sub


        Dim K1 As New Cls_SqlString

        Dim Barra As New Cls_BarraSenior

        If Not IsNothing(Session("RicercaAnagraficaSQLString")) Then
            Try
                K1 = Session("RicercaAnagraficaSQLString")
            Catch ex As Exception

            End Try
        End If

        Lbl_BarraSenior.Text = Barra.CodiceBarra(Application("SENIOR"), Session("UTENTE"), Page, K1)


        Dim kVilla As New Cls_TabelleDescrittiveOspitiAccessori


        kVilla.UpDateDropBox(Session("DC_OSPITIACCESSORI"), "VIL", DD_Struttura)
        If DD_Struttura.Items.Count = 1 Then
            Call AggiornaCServ()
        End If

        Dim Into As New Cls_DatiGenerali

        Into.LeggiDati(Session("DC_TABELLE"))

        If Into.AttivaCServPrimanoIncassi = 1 Then

            DD_CServ.Visible = True
            lblCentroservizio.Visible = True

            DD_Struttura.Visible = True
            lblStruttura.Visible = True
        End If
    End Sub

    Protected Sub ImgMenu_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles ImgMenu.Click
        Response.Redirect("Menu_Generale.aspx")

    End Sub
    Private Sub AggiornaCServ()
        Dim kCsrv As New Cls_CentroServizio

        If DD_Struttura.SelectedValue = "" Then
            kCsrv.UpDateDropBox(Session("DC_OSPITE"), DD_CServ)
        Else
            kCsrv.UpDateDropBoxStruttura(Session("DC_OSPITE"), DD_CServ, DD_Struttura.SelectedValue)
        End If
    End Sub



    Protected Sub DD_Struttura_TextChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles DD_Struttura.TextChanged
        AggiornaCServ()
    End Sub
End Class
