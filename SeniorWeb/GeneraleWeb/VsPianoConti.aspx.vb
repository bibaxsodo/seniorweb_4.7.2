﻿Imports Microsoft.VisualBasic
Imports System.Data.OleDb
Imports System.Web.Hosting

Partial Class GeneraleWeb_VsPianoConti
    Inherits System.Web.UI.Page

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load


        If Trim(Session("UTENTE")) = "" Then
            Response.Redirect("../Login.aspx")
            Exit Sub
        End If

        Dim cn As OleDbConnection



        cn = New Data.OleDb.OleDbConnection(Session("DC_GENERALE"))

        cn.Open()
        Dim cmd As New OleDbCommand()

        cmd.CommandText = ("select * from PianoConti where  Conto = 0 And Sottoconto = 0" & _
                               " Order by Mastro ")

        cmd.Connection = cn

        Lbl_VsPianoConti.Text = ""

        Dim myPOSTreader As OleDbDataReader = cmd.ExecuteReader()
        Do While myPOSTreader.Read
            If Request.Item("MASTRO") = campodb(myPOSTreader.Item("Mastro")) Then
                Lbl_VsPianoConti.Text = Lbl_VsPianoConti.Text & "<a href=""VsPianoConti.aspx""><img src=""images/arrow.gif"" /></a>" & campodb(myPOSTreader.Item("Mastro")) & " " & campodb(myPOSTreader.Item("Descrizione")) & "<br>"
            Else
                Lbl_VsPianoConti.Text = Lbl_VsPianoConti.Text & "<a href=""VsPianoConti.aspx?Mastro=" & campodb(myPOSTreader.Item("Mastro")) & """><img src=""images/arrow.gif"" /></a>" & campodb(myPOSTreader.Item("Mastro")) & " " & campodb(myPOSTreader.Item("Descrizione")) & "<br>"
            End If
            If Request.Item("MASTRO") = campodb(myPOSTreader.Item("Mastro")) Then

                Dim cmd1 As New OleDbCommand()

                cmd1.CommandText = ("select * from PianoConti where Mastro = ? And Sottoconto = 0 And Conto >0 " & _
                                       " Order by Mastro ")

                cmd1.Connection = cn
                cmd1.Parameters.AddWithValue("@Mastro", Request.Item("MASTRO"))

                Dim myPOSTreader1 As OleDbDataReader = cmd1.ExecuteReader()
                Do While myPOSTreader1.Read
                    If Request.Item("MASTRO") = campodb(myPOSTreader1.Item("Mastro")) And Request.Item("CONTO") = campodb(myPOSTreader1.Item("Conto")) Then
                        Lbl_VsPianoConti.Text = Lbl_VsPianoConti.Text & "&nbsp;&nbsp;<a href=""VsPianoConti.aspx?Mastro=" & campodb(myPOSTreader1.Item("Mastro")) & """><img src=""images/arrow.gif"" /></a><font color=""gray"">" & campodb(myPOSTreader1.Item("Mastro")) & " " & campodb(myPOSTreader1.Item("Conto")) & " " & campodb(myPOSTreader1.Item("Descrizione")) & "</font><br>"
                    Else
                        Lbl_VsPianoConti.Text = Lbl_VsPianoConti.Text & "&nbsp;&nbsp;<a href=""VsPianoConti.aspx?Mastro=" & campodb(myPOSTreader1.Item("Mastro")) & "&Conto=" & campodb(myPOSTreader1.Item("Conto")) & """><img src=""images/arrow.gif"" /></a><font color=""gray"">" & campodb(myPOSTreader1.Item("Mastro")) & " " & campodb(myPOSTreader1.Item("Conto")) & " " & campodb(myPOSTreader1.Item("Descrizione")) & "</font><br>"
                    End If

                    If Request.Item("MASTRO") = campodb(myPOSTreader1.Item("Mastro")) And Request.Item("CONTO") = campodb(myPOSTreader1.Item("Conto")) Then
                        Dim cmd2 As New OleDbCommand()

                        cmd2.CommandText = ("select * from PianoConti where Mastro = ? And Conto = ?" & _
                                               " And Sottoconto > 0 Order by Mastro ")

                        cmd2.Connection = cn
                        cmd2.Parameters.AddWithValue("@Mastro", Request.Item("MASTRO"))
                        cmd2.Parameters.AddWithValue("@Conto", Request.Item("CONTO"))

                        Dim myPOSTreader2 As OleDbDataReader = cmd2.ExecuteReader()
                        Do While myPOSTreader2.Read
                            Lbl_VsPianoConti.Text = Lbl_VsPianoConti.Text & "&nbsp;&nbsp;&nbsp;&nbsp;<img src=""images/arrow.gif"" /><font color=""blue"">" & campodb(myPOSTreader2.Item("Mastro")) & " " & campodb(myPOSTreader2.Item("Conto")) & " " & campodb(myPOSTreader2.Item("SottoConto")) & " " & campodb(myPOSTreader2.Item("Descrizione")) & "</font><br>"
                        Loop
                        myPOSTreader2.Close()
                    End If
                Loop
                myPOSTreader1.Close()
            End If
        Loop
        myPOSTreader.Close()
        cn.Close()

    End Sub


    Function campodb(ByVal oggetto As Object) As String
        If IsDBNull(oggetto) Then
            Return ""
        Else
            Return oggetto
        End If
    End Function
End Class

