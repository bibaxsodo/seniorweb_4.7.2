﻿<%@ Page Language="VB" AutoEventWireup="false" Inherits="GeneraleWeb_FatturaElettronicaPrivati" CodeFile="FatturaElettronicaPrivati.aspx.vb" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="sau" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
    <meta http-equiv="x-ua-compatible" content="IE=9" />
    <title>Fatturazione Elettronica</title>
    <asp:PlaceHolder runat="server">
        <%: System.Web.Optimization.Styles.Render("~/Content/AjaxControlToolkit/Styles/Bundle") %>
    </asp:PlaceHolder>
    <link href="ospiti.css?ver=11" rel="stylesheet" type="text/css" />
    <link rel="shortcut icon" href="images/SENIOR.ico" />
    <link href="js/jquery.autocomplete.css" rel="stylesheet" type="text/css" />

    <script src="js/jquery-1.5.1.min.js" type="text/javascript"></script>
    <script src="js/jquery.autocomplete.js?ver=4" type="text/javascript"></script>
    <script src="js/soapclient.js" type="text/javascript"></script>
    <script src="/js/convertinumero.js" type="text/javascript"></script>

    <script src="js/jquery.maskedinput-1.3.min.js" type="text/javascript"></script>
    <script src="/js/formatnumer.js" type="text/javascript"></script>

    <script src="/js/pianoconti.js" type="text/javascript"></script>
    <script src="js/JSErrore.js" type="text/javascript"></script>
    <script src="js/NoEnter.js" type="text/javascript"></script>
    <script src="js/chosen/chosen.jquery.js" type="text/javascript"></script>
    <script src="js/chosen/docsupport/prism.js" type="text/javascript" charset="utf-8"></script>


    <link rel="stylesheet" href="js/chosen/docsupport/style.css">
    <link rel="stylesheet" href="js/chosen/docsupport/prism.css">
    <link rel="stylesheet" href="js/chosen/chosen.css">

    <link rel="stylesheet" href="dialogbox/redips-dialog.css" type="text/css" media="screen" />
    <script type="text/javascript" src="dialogbox/redips-dialog-min.js"></script>
    <script type="text/javascript" src="dialogbox/script.js"></script>

    <link rel="stylesheet" href="jqueryui/jquery-ui.css" type="text/css" />
    <script src="jqueryui/jquery-ui.js" type="text/javascript"></script>
    <script src="jqueryui/datapicker-it.js" type="text/javascript"></script>
    <style>
        .wait {
            border: solid 1px #2d2d2d;
            text-align: left;
            vertical-align: top;
            background: white;
            width: 200px;
            height: 240px;
            position: absolute;
            top: 290px;
            right: 10px;
            -moz-border-radius: 5px;
            -webkit-border-radius: 5px;
            border-radius: 5px;
            box-shadow: 0px 0px 10px 4px rgba(0, 125, 196, 0.75);
            -moz-box-shadow: 0px 0px 10px 4px rgba(0, 125, 196, 0.75);
            -webkit-box-shadow: 0px 0px 10px 4px rgba(0, 125, 196, 0.75);
            z-index: 133;
        }

        #blur {
            width: 100%;
            background-color: black;
            moz-opacity: 0.5;
            khtml-opacity: .5;
            opacity: .5;
            filter: alpha(opacity=50);
            z-index: 120;
            height: 100%;
            position: absolute;
            top: 0;
            left: 0;
        }

        #progress {
            z-index: 200;
            background-color: White;
            position: absolute;
            top: 0pt;
            left: 0pt;
            border: solid 1px black;
            padding: 5px 5px 5px 5px;
            text-align: center;
        }
    </style>


    <script type="text/javascript">
        function DialogBox(Path) {

            var winW = 630, winH = 460;
            if (document.body && document.body.offsetWidth) {
                winW = document.body.offsetWidth;
                winH = document.body.offsetHeight;
            }
            if (document.compatMode == 'CSS1Compat' &&
                document.documentElement &&
                document.documentElement.offsetWidth) {
                winW = document.documentElement.offsetWidth;
                winH = document.documentElement.offsetHeight;
            }
            if (window.innerWidth && window.innerHeight) {
                winW = window.innerWidth;
                winH = window.innerHeight;
            }

            var APPO = winH - 100;

            REDIPS.dialog.show(winW - 200, winH - 100, '<iframe id="output" src="' + Path + '" height="' + APPO + '" width="100%" ></iframe>');
            return false;

        }

        function DialogBoxSmall(Path) {

            var winW = 630, winH = 460;


            REDIPS.dialog.show(winW - 200, winH - 100, '<iframe id="output" src="' + Path + '" height="' + winH + '" width="' + winW + 'px" ></iframe>');
            return false;

        }

        $(document).ready(function () {
            if (window.innerHeight > 0) { $("#BarraLaterale").css("height", (window.innerHeight - 105) + "px"); } else { $("#BarraLaterale").css("height", (document.documentElement.offsetHeight - 105) + "px"); }
        });

        function openPopUp(urlToOpen, nome, parametri) {
            var popup_window = window.open(urlToOpen, '_blank');
            try {
                popup_window.focus();
            } catch (e) {
                alert("E' stata bloccata l'apertura del popup da parte del browser");
            }
        }

    </script>
</head>
<body>
    <form id="form1" runat="server">
        <asp:ScriptManager ID="ScriptManager1" runat="server" EnableScriptGlobalization="true">
            <Scripts>
                <asp:ScriptReference Path="~/Scripts/AjaxControlToolkit/Bundle" />
            </Scripts>
        </asp:ScriptManager>
        <asp:Label ID="Lbl_BarraSenior" runat="server" Text=""></asp:Label>
        <div style="text-align: left;">
            <table style="width: 100%;" cellpadding="0" cellspacing="0">
                <tr>
                    <td style="width: 160px; background-color: #F0F0F0;"></td>
                    <td>
                        <div class="Titolo">Ospiti - Export - Generazione Fattura Elettronica</div>
                        <div class="SottoTitolo">
                            <asp:Label ID="Lbl_NomeOspite" runat="server"></asp:Label><br />
                            <br />
                        </div>
                    </td>
                    <td style="text-align: right;">
                        <span class="BenvenutoText">Benvenuto
                            <asp:Label ID="Lbl_Utente" runat="server"></asp:Label>&nbsp;&nbsp;&nbsp;&nbsp;</span>
                        <div class="DivTasti">

                            <asp:ImageButton ID="Btn_Modifica" runat="server" Height="38px" ImageUrl="~/images/download.png" class="EffettoBottoniTondi" Width="38px" ToolTip="Modifica" />
                            <asp:ImageButton ID="Btn_Movimenti" runat="server" Height="38px" ImageUrl="~/images/esegui.png" class="EffettoBottoniTondi" Width="38px" ToolTip="Estrai" />
                            <asp:ImageButton ID="Btn_Send" runat="server" Height="38px" ImageUrl="~/images/sendmail.png" class="EffettoBottoniTondi" Width="38px" ToolTip="Invia" />
                        </div>
                    </td>
                </tr>

                <tr>
                    <td style="width: 160px; background-color: #F0F0F0; vertical-align: top; text-align: center;" id="BarraLaterale">
                        <asp:ImageButton ID="Btn_Esci" runat="server" BackColor="Transparent" ImageUrl="../images/Menu_Indietro.png" class="Effetto" ToolTip="Chiudi" />

                        <br />

                        <div id="MENUDIV"></div>
                        <br />
                        <br />
                        <br />
                        <br />
                        <br />
                        <br />
                        <br />
                        <br />
                        <br />
                        <br />
                        <br />
                        <br />
                    </td>
                    <td colspan="2" style="background-color: #FFFFFF; vertical-align: top;">
                        <sau:TabContainer ID="TabContainer1" runat="server" ActiveTabIndex="0" CssClass="TabSenior"
                            Width="100%" BorderStyle="None" Style="margin-right: 39px">
                            <sau:TabPanel runat="server" HeaderText="Prima Nota" ID="Tab_Anagrafica">
                                <HeaderTemplate>
                                    Fatturazione Elettronica
         
         
                                </HeaderTemplate>


                                <ContentTemplate>

                                    <table style="width: 100%;">
                                        <tr>
                                            <td>
                                                <label class="LabelCampo">Registro IVA:</label>
                                                <asp:DropDownList ID="DD_RegIVa" runat="server"></asp:DropDownList>
                                                <br />
                                                <br />

                                                <label class="LabelCampo">Data Dal:</label>
                                                <asp:TextBox ID="Txt_DataDal" autocomplete="off" runat="server" Width="90px"></asp:TextBox>
                                                <br />
                                                <br />

                                                <label class="LabelCampo">Data Al:</label>
                                                <asp:TextBox ID="Txt_DataAl" autocomplete="off" runat="server" Width="90px"></asp:TextBox>
                                                <br />
                                                <br />


                                                <label class="LabelCampo">Causale Contabile :</label>
                                                <asp:DropDownList ID="Dd_CausaleContabile" runat="server" class="chosen-select" Width="450px"></asp:DropDownList><br />
                                                <br />

                                                <label class="LabelCampo">Escludi generati : </label>
                                                <asp:CheckBox ID="Chk_generato" runat="server" />
                                                <br />
                                                <br />
                                            </td>
                                            <td style="vertical-align: top; text-align: right;">
                                                <div style="float: left; margin-top: 10px; border-width: thin; border-style: solid; border-color: red;">
                                                    La dicitura "Bollo Virtuale" sarà indicata in XML se presente il flag &#10004; nella colonna "Bollo Virtuale"
                                                </div>
                                            </td>
                                        </tr>
                                    </table>

                                    <asp:Button ID="Btn_Seleziona" runat="server" Text="Seleziona Tutto" />
                                    <br />
                                    <br />

                                    <asp:GridView ID="GridView1" runat="server" CellPadding="3"
                                        Width="100%" BackColor="White" BorderColor="#999999" BorderStyle="None"
                                        BorderWidth="1px" GridLines="Vertical">
                                        <RowStyle ForeColor="#565151" BackColor="#EEEEEE" />
                                        <AlternatingRowStyle BackColor="Gainsboro" />

                                        <Columns>



                                            <asp:TemplateField ItemStyle-Width="40px">
                                                <ItemTemplate>
                                                    <asp:ImageButton ID="VisualizzaXML" CommandName="Richiama" runat="Server" ImageUrl="~/images/esegui.png" class="EffettoBottoniTondi" BackColor="Transparent" CommandArgument='<%#  Eval("NumeroRegistrazione") %>' />
                                                </ItemTemplate>
                                            </asp:TemplateField>


                                            <asp:TemplateField HeaderText="Esporta">
                                                <ItemTemplate>
                                                    <asp:CheckBox ID="ChkImporta" runat="server" Text="" />
                                                </ItemTemplate>
                                                <HeaderStyle Font-Bold="False" Font-Italic="False" />
                                                <ItemStyle Width="30px" />
                                            </asp:TemplateField>

                                            <asp:TemplateField ItemStyle-Width="40px">
                                                <ItemTemplate>
                                                    <asp:ImageButton ID="UpLoadDoc" CommandName="UpLoadDoc" runat="Server" ImageUrl="~/images/upload.png" class="EffettoBottoniTondi" BackColor="Transparent" CommandArgument='<%#  Eval("NumeroRegistrazione") %>' />
                                                </ItemTemplate>
                                            </asp:TemplateField>



                                            <asp:BoundField HeaderText="Numero Registrazione" DataField="NumeroRegistrazione" />
                                            <asp:BoundField HeaderText="Progressivo" DataField="Progressivo" />
                                            <asp:BoundField HeaderText="Data Registrazione" DataField="DataRegistrazione" />
                                            <asp:BoundField HeaderText="Tipo Documento" DataField="Tipo Doc" />
                                            <asp:BoundField HeaderText="Numero Protocollo" DataField="NumeroProtocollo" />
                                            <asp:BoundField HeaderText="Ragione Sociale" DataField="Ragione Sociale" />
                                            <asp:BoundField HeaderText="ImportoDocumento" DataField="ImportoDocumento" />


                                            <asp:TemplateField HeaderText="Bollo Virtuale">
                                                <ItemTemplate>
                                                    <asp:CheckBox ID="ChkBollo" runat="server" Text="" />
                                                </ItemTemplate>
                                                <HeaderStyle Font-Bold="True" Font-Italic="False" />
                                                <ItemStyle Width="30px" />
                                            </asp:TemplateField>


                                            <asp:BoundField HeaderText="Segnalazione" DataField="Segnalazione" />


                                        </Columns>

                                        <FooterStyle BackColor="#CCCCCC" ForeColor="Black" />
                                        <PagerStyle BackColor="#999999" ForeColor="Black" HorizontalAlign="Center" />
                                        <SelectedRowStyle BackColor="#008A8C" Font-Bold="True" ForeColor="White" />
                                        <HeaderStyle BackColor="#565151" Font-Bold="False" Font-Size="Small"
                                            ForeColor="White" />
                                    </asp:GridView>


                                    <asp:GridView ID="GridStampa" runat="server" Visible="false"></asp:GridView>



                                </ContentTemplate>



                            </sau:TabPanel>
                        </sau:TabContainer>
                    </td>
                </tr>
            </table>


        </div>
    </form>
</body>
</html>
