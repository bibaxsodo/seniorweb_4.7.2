﻿Imports Microsoft.VisualBasic
Imports System.Data.OleDb
Imports System.Web.Hosting
Partial Class GeneraleWeb_PagamentoF24
    Inherits System.Web.UI.Page
    Dim Tabella As New System.Data.DataTable("tabella")


    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Trim(Session("UTENTE")) = "" Then
            Response.Redirect("/SeniorWeb/Login.aspx")
            Exit Sub
        End If
        EseguiJS()
        If Page.IsPostBack = True Then Exit Sub


        Txt_Anno.Text = Year(Now)

        Dd_Mese.SelectedValue = Month(Now)
        Dim K1 As New Cls_SqlString

        Dim Barra As New Cls_BarraSenior

        If Not IsNothing(Session("RicercaGeneraleSQLString")) Then
            K1 = Session("RicercaGeneraleSQLString")
        End If

        Btn_Excel.Visible = False


        Dim k As New Cls_CausaleContabile
        Dim ConnectionString As String = Session("DC_TABELLE")

        k.UpDateDropBox(ConnectionString, Dd_CausaleContabile)

        Txt_DataRegistrazione.Text = Format(Now, "dd/MM/yyyy")

        Lbl_BarraSenior.Text = Barra.CodiceBarra(Application("SENIOR"), Session("UTENTE"), Page, K1)
    End Sub

    Protected Sub Btn_Movimenti_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles Btn_Movimenti.Click
        Dim MySql As String
        Dim cn As OleDbConnection
        Dim StringaConnessione As String = Session("DC_GENERALE")
        Dim StringaConnessioneTabelle As String = Session("DC_TABELLE")

        cn = New Data.OleDb.OleDbConnection(Session("DC_GENERALE"))

        cn.Open()

        Dim cmd As New OleDbCommand()

        Tabella.Clear()
        Tabella.Columns.Clear()
        Tabella.Columns.Add("Id", GetType(String))
        Tabella.Columns.Add("Tipo Doc.", GetType(String))
        Tabella.Columns.Add("N. Regis.", GetType(String))
        Tabella.Columns.Add("Num.Doc.", GetType(String))
        Tabella.Columns.Add("Data Doc.", GetType(String))
        Tabella.Columns.Add("Cod. Tributo", GetType(String))
        Tabella.Columns.Add("Tributo", GetType(String))
        Tabella.Columns.Add("Imponibile Rit.", GetType(String))
        Tabella.Columns.Add("Tipo Rit.", GetType(String))
        Tabella.Columns.Add("Imp. Ritenuta", GetType(String))
        Tabella.Columns.Add("Pagamento", GetType(String))
        Tabella.Columns.Add("Data Pagamento", GetType(String))
        Tabella.Columns.Add("Imp. Pagamento", GetType(String))
   

        MySql = "Select * From MovimentiRitenute where  (SELECT YEAR(DataRegistrazione) FROM [MovimentiContabiliTesta]  WHERE NumeroRegistrazione = NumeroRegistrazionePagamento) = " & Txt_Anno.Text
        MySql = MySql & " And  (SELECT month(DataRegistrazione) FROM [MovimentiContabiliTesta]  WHERE NumeroRegistrazione = NumeroRegistrazionePagamento) = " & Dd_Mese.SelectedValue
        MySql = MySql & " And ImportoPagamentoRitenuta = 0 "
        MySql = MySql & " Order by CodiceTributo"
        cmd.CommandText = MySql
        cmd.Connection = cn
        Dim myPOSTreader As OleDbDataReader = cmd.ExecuteReader()
        Do While myPOSTreader.Read

            Dim myriga As System.Data.DataRow = Tabella.NewRow()


            Dim RegistrazioneContabile As New Cls_MovimentoContabile

            RegistrazioneContabile.NumeroRegistrazione = campodbN(myPOSTreader.Item("NumeroRegistrazioneDocumento"))
            RegistrazioneContabile.Leggi(StringaConnessione, RegistrazioneContabile.NumeroRegistrazione)

            Dim CausaleContabile As New Cls_CausaleContabile

            CausaleContabile.Codice = RegistrazioneContabile.CausaleContabile
            CausaleContabile.Leggi(StringaConnessioneTabelle, CausaleContabile.Codice)

            myriga(0) = campodbN(myPOSTreader.Item("ID"))

            myriga(1) = CausaleContabile.TipoDocumento
            myriga(2) = RegistrazioneContabile.NumeroRegistrazione

            myriga(3) = RegistrazioneContabile.NumeroDocumento

            myriga(4) = Format(RegistrazioneContabile.DataDocumento, "dd/MM/yyyy")

            myriga(5) = campodb(myPOSTreader.Item("CodiceTributo"))
            myriga(6) = campodb(myPOSTreader.Item("CodiceTributo")) 'inserire decodifica tributi

            myriga(7) = Format(campodbN(myPOSTreader.Item("ImponibileRitenuta")), "#,##0.00")

            Dim TipoRitenuta As New ClsRitenuta

            TipoRitenuta.Codice = campodb(myPOSTreader.Item("TipoRitenuta"))
            TipoRitenuta.Leggi(StringaConnessioneTabelle)

            myriga(8) = TipoRitenuta.Descrizione

            myriga(9) = Format(campodbN(myPOSTreader.Item("ImportoRitenuta")), "#,##0.00")


            Dim RegistrazioneContabilePagamento As New Cls_MovimentoContabile

            RegistrazioneContabilePagamento.NumeroRegistrazione = campodbN(myPOSTreader.Item("NumeroRegistrazionePagamento"))
            RegistrazioneContabilePagamento.Leggi(StringaConnessione, RegistrazioneContabile.NumeroRegistrazione)

            myriga(10) = campodbN(myPOSTreader.Item("NumeroRegistrazionePagamento"))


            If campodbN(myPOSTreader.Item("NumeroRegistrazionePagamento")) = 0 Then
                myriga(11) = ""
            Else

                myriga(11) = Format(RegistrazioneContabilePagamento.DataRegistrazione, "dd/MM/yyyy")
            End If
            myriga(12) = Format(campodbN(myPOSTreader.Item("ImportoPagamento")), "#,##0.00")
            Tabella.Rows.Add(myriga)
        Loop
        myPOSTreader.Close()
        cn.Close()

        ViewState("Appoggio") = Tabella

        GridView1.AutoGenerateColumns = True

        GridView1.DataSource = Tabella

        GridView1.DataBind()

        GridView1.Visible = True

        Btn_Excel.Visible = True
    End Sub




    Function campodbN(ByVal oggetto As Object) As Double


        If IsDBNull(oggetto) Then
            Return 0
        Else
            Return oggetto
        End If
    End Function
    Function campodb(ByVal oggetto As Object) As String


        If IsDBNull(oggetto) Then
            Return ""
        Else
            Return oggetto
        End If
    End Function

    Function campodbd(ByVal oggetto As Object) As Date
        If IsDBNull(oggetto) Then
            Return Nothing
        Else
            Return oggetto
        End If
    End Function

    Protected Sub Btn_Modifica_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles Btn_Modifica.Click
        Dim Riga As Integer
        Dim TotaleRitenuta As Double


        Tabella = ViewState("Appoggio")
        For Riga = 0 To GridView1.Rows.Count - 1

            Dim CheckBox As CheckBox = DirectCast(GridView1.Rows(Riga).FindControl("ChkImporta"), CheckBox)

            If CheckBox.Checked = True Then
                Dim Ritentute As New Cls_RitenuteAcconto


                Ritentute.ID = Val(Tabella.Rows(Riga).Item(0))
                Ritentute.Leggi(Session("DC_GENERALE"))

                TotaleRitenuta = TotaleRitenuta + Ritentute.ImportoPagamento
            End If

        Next


        Dim MovGT As New Cls_MovimentoContabile

        MovGT.DataRegistrazione = Txt_DataRegistrazione.Text
        MovGT.CausaleContabile = Dd_CausaleContabile.SelectedValue

        Dim CausaleContabile As New Cls_CausaleContabile

        CausaleContabile.Leggi(Session("DC_TABELLE"), Dd_CausaleContabile.SelectedValue)

        If IsNothing(MovGT.Righe(0)) Then
            MovGT.Righe(0) = New Cls_MovimentiContabiliRiga
        End If


        MovGT.Righe(0).MastroPartita = CausaleContabile.Righe(0).Mastro
        MovGT.Righe(0).ContoPartita = CausaleContabile.Righe(0).Conto
        MovGT.Righe(0).SottocontoPartita = CausaleContabile.Righe(0).Sottoconto
        MovGT.Righe(0).Importo = TotaleRitenuta
        MovGT.Righe(0).Segno = "+"
        MovGT.Righe(0).Tipo = ""
        MovGT.Righe(0).Descrizione = ""
        MovGT.Righe(0).RigaDaCausale = 1
        MovGT.Righe(0).DareAvere = CausaleContabile.Righe(0).DareAvere

        If IsNothing(MovGT.Righe(1)) Then
            MovGT.Righe(1) = New Cls_MovimentiContabiliRiga
        End If


        MovGT.Righe(1).MastroPartita = CausaleContabile.Righe(1).Mastro
        MovGT.Righe(1).ContoPartita = CausaleContabile.Righe(1).Conto
        MovGT.Righe(1).SottocontoPartita = CausaleContabile.Righe(1).Sottoconto
        MovGT.Righe(1).Importo = TotaleRitenuta
        MovGT.Righe(1).Segno = "+"
        MovGT.Righe(1).Tipo = ""
        MovGT.Righe(1).Descrizione = ""
        MovGT.Righe(1).RigaDaCausale = 1
        MovGT.Righe(1).DareAvere = CausaleContabile.Righe(1).DareAvere


        MovGT.Scrivi(Session("DC_GENERALE"), 0)

        For Riga = 0 To GridView1.Rows.Count - 1

            Dim CheckBox As CheckBox = DirectCast(GridView1.Rows(Riga).FindControl("ChkImporta"), CheckBox)

            If CheckBox.Checked = True Then
                Dim Ritentute As New Cls_RitenuteAcconto


                Ritentute.ID = Val(Tabella.Rows(Riga).Item(0))
                Ritentute.Leggi(Session("DC_GENERALE"))

                Ritentute.NumeroRegistrazionePagamentoRitenuta = MovGT.NumeroRegistrazione
                Ritentute.ImportoPagamentoRitenuta = Ritentute.ImportoPagamento
                Ritentute.Scrivi(Session("DC_GENERALE"))


            End If

        Next

        Btn_Movimenti_Click(sender, e)
    End Sub

    Protected Sub Btn_Esci_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles Btn_Esci.Click
        Response.Redirect("Menu_Servizi.aspx")
    End Sub

    Protected Sub Btn_Excel_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles Btn_Excel.Click
        Tabella = ViewState("Appoggio")

        GridStampa.AutoGenerateColumns = True

        GridStampa.DataSource = Tabella

        GridStampa.DataBind()

        GridStampa.Visible = True

        Response.Clear()
        Response.AddHeader("content-disposition", "attachment;filename=PagamentoF24.xls")
        Response.Charset = String.Empty
        Response.Cache.SetCacheability(HttpCacheability.NoCache)
        Response.ContentType = "application/vnd.xls"
        Dim stringWrite As New System.IO.StringWriter
        Dim htmlWrite As New HtmlTextWriter(stringWrite)

        form1.Controls.Clear()
        form1.Controls.Add(GridStampa)

        form1.RenderControl(htmlWrite)

        Response.Write(stringWrite.ToString())
        Response.End()
    End Sub

    Protected Sub Btn_Seleziona_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles Btn_Seleziona.Click


        For Riga = 0 To GridView1.Rows.Count - 1

            Dim CheckBox As CheckBox = DirectCast(GridView1.Rows(Riga).FindControl("ChkImporta"), CheckBox)
            
            If CheckBox.Enabled = True Then
                CheckBox.Checked = True
            End If
        Next
    End Sub


    Private Sub EseguiJS()
        Dim MyJs As String
        MyJs = "$(document).ready(function() {"

        MyJs = MyJs & "var els = document.getElementsByTagName(""*"");"

        MyJs = MyJs & "for (var i=0;i<els.length;i++)"
        MyJs = MyJs & "if ( els[i].id ) { "
        MyJs = MyJs & " var appoggio =els[i].id; "
        MyJs = MyJs & " if (appoggio.match('Txt_RateoRisconto')!= null || appoggio.match('Txt_Costo')!= null) {  "
        MyJs = MyJs & " $(els[i]).autocomplete('/WebHandler/GestioneConto.ashx?Utente=" & Session("UTENTE") & "', {delay:5,minChars:3});"
        MyJs = MyJs & "    }"

        MyJs = MyJs & " if (appoggio.match('Txt_Importo')!= null)  {  "
        MyJs = MyJs & " $(els[i]).keypress(function() { ForceNumericInput($(this).val(), true, true); } ); "
        MyJs = MyJs & " $(els[i]).blur(function() { var ap = formatNumber($(this).val(),2); $(this).val(ap); });"
        MyJs = MyJs & "    }"

        MyJs = MyJs & " if (appoggio.match('Txt_Data')!= null || appoggio.match('Txt_DataAl')!= null)  {  "
        MyJs = MyJs & " $(els[i]).mask(""99/99/9999""); "
        MyJs = MyJs & " $(els[i]).datepicker({ changeMonth: true,changeYear: true,  yearRange: ""1890:" & Year(Now) + 1 & """},$.datepicker.regional[""it""]);"
        MyJs = MyJs & "    }"


        MyJs = MyJs & "} "
        MyJs = MyJs & "});"

        'MyJs = "$(document).ready(function() { $('.myClass').keypress(function() { handleEnter($('.myClass').val(), true, true); } );     $('#" & Txt_ImportoPagato.ClientID & "').blur(function() { var ap = formatNumber ($('#" & Txt_ImportoPagato.ClientID & "').val(),2); $('#" & Txt_ImportoPagato.ClientID & "').val(ap); }); });"
        ScriptManager.RegisterStartupScript(Me, Me.GetType(), "visualizzaRitIm", MyJs, True)
    End Sub

End Class
