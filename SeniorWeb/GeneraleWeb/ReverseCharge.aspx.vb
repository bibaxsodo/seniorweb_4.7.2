﻿Imports Microsoft.VisualBasic
Imports System.Data.OleDb
Imports System.Web.Hosting
Imports System.Web.Script.Serialization

Partial Class GeneraleWeb_ReverseCharge
    Inherits System.Web.UI.Page

    Protected Sub Btn_Modifica_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles Btn_Modifica.Click

        'MyRs.Open("Select * From TabellaLegami Where CodiceDocumento = " & NumeroRegistrazione, GeneraleDb, adOpenKeyset, adLockOptimistic)
        'If Not MyRs.EOF Then
        '    MyRs.Close()
        '    MsgBox("Non posso procedere è già presente un legame", vbCritical)
        '    Exit Sub
        'End If
        'MyRs.Close()

        If Dd_CausaleContabileFV.SelectedValue = "" Then
            ClientScript.RegisterClientScriptBlock(Me.GetType(), "ControlloImporto", "alert('Devi indicare la causale della fattura di vendita');", True)
            Exit Sub
        End If


        If Dd_CausaleContabile.SelectedValue = "" Then
            ClientScript.RegisterClientScriptBlock(Me.GetType(), "ControlloImporto", "alert('Devi indicare la causale del giroconto');", True)
            Exit Sub
        End If


        Dim VerLegami As New Cls_Legami

        If VerLegami.TotaleLegame(Session("DC_GENERALE"), Val(Request.Item("NUMERO"))) <> 0 Then
            ClientScript.RegisterClientScriptBlock(Me.GetType(), "ControlloImporto", "alert('Non posso procedere è già presente un legame');", True)
            Exit Sub
        End If


        Response.Cookies("UserSettings")("Dd_CausaleContabileFV") = Dd_CausaleContabileFV.SelectedValue
        Response.Cookies("UserSettings")("Dd_CausaleContabile") = Dd_CausaleContabile.SelectedValue



        Dim Reverse As New Cls_ReverseCharge
        Dim Registrazione As New Cls_MovimentoContabile

        Registrazione.Leggi(Session("DC_GENERALE"), Registrazione.NumeroRegistrazione)

        Dim Segnalazione As String = ""

        Dim Log As New Cls_LogPrivacy

        Dim serializer As JavaScriptSerializer
        serializer = New JavaScriptSerializer()



        Log.LogPrivacy(Application("SENIOR"), Session("CLIENTE"), Session("UTENTE"), Session("UTENTEIMPER"), Page.GetType.Name.ToUpper, 0, 0, "", "", Session("NumeroRegistrazione"), "", "R", "REVERSE", serializer.Serialize(Registrazione))


        Segnalazione = Reverse.Reverse(Val(Request.Item("NUMERO")), Dd_CausaleContabileFV.SelectedValue, Dd_CausaleContabile.SelectedValue, Session("DC_GENERALE"), Session("DC_TABELLE"))


        ClientScript.RegisterClientScriptBlock(Me.GetType(), "ControlloImporto", "alert('" & Segnalazione & "');", True)

    End Sub



    Protected Sub GeneraleWeb_ReverseCharge_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load

        If Page.IsPostBack = True Then
            Exit Sub
        End If

        Dim x1 As New Cls_CausaleContabile

        x1.UpDateDropBoxDoc(Session("DC_TABELLE"), Dd_CausaleContabileFV)


        Dim x2 As New Cls_CausaleContabile

        x2.UpDateDropBox(Session("DC_TABELLE"), Dd_CausaleContabile)


        If (Request.Cookies("UserSettings") IsNot Nothing) Then
            If (Request.Cookies("UserSettings")("Dd_CausaleContabileFV") IsNot Nothing) Then
                Dd_CausaleContabileFV.SelectedValue = Request.Cookies("UserSettings")("Dd_CausaleContabileFV")
            End If
        End If

        If (Request.Cookies("UserSettings") IsNot Nothing) Then
            If (Request.Cookies("UserSettings")("Dd_CausaleContabile") IsNot Nothing) Then
                Dd_CausaleContabile.SelectedValue = Request.Cookies("UserSettings")("Dd_CausaleContabile")
            End If
        End If

        Dim Reg As New Cls_MovimentoContabile

        Reg.Leggi(Session("DC_GENERALE"), Val(Request.Item("NUMERO")))

        Dim CReg As New Cls_CausaleContabile

        CReg.Leggi(Session("DC_TABELLE"), Reg.CausaleContabile)

        If CReg.DocumentoReverse <> "" And CReg.GirocontoReverse <> "" Then
            Dd_CausaleContabileFV.SelectedValue = CReg.DocumentoReverse
            Dd_CausaleContabile.SelectedValue = CReg.GirocontoReverse
        End If

    End Sub
End Class

