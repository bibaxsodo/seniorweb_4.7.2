﻿Imports Microsoft.VisualBasic
Imports System.Data.OleDb
Imports System.Web.Hosting
Imports System.Data.SqlTypes

Partial Class GeneraleWeb_ElencoPianoConti
    Inherits System.Web.UI.Page

    Dim Tabella As New System.Data.DataTable("tabella")


    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Session("UTENTE") = "" Then
            Response.Redirect("..\Login.aspx")
            Exit Sub
        End If

        If Page.IsPostBack = True Then
            Exit Sub
        End If

        Dim K1 As New Cls_SqlString

        Dim Barra As New Cls_BarraSenior

        If Not IsNothing(Session("RicercaGeneraleSQLString")) Then
            K1 = Session("RicercaGeneraleSQLString")
        End If

        Lbl_BarraSenior.Text = Barra.CodiceBarra(Application("SENIOR"), Session("UTENTE"), Page, K1)

        If Request.Item("TIPO") <> "RITORNO" Then
            Call CaricaUltimi10()
            Session("ElencoPianoContiSQLString") = Nothing
        Else

            If Not IsNothing(Session("ElencoPianoContiSQLString")) Then
                Dim k As New Cls_SqlString
                Dim Appoggio As System.Web.UI.ImageClickEventArgs

                k = Session("ElencoPianoContiSQLString")

                Txt_Conto.Text = k.GetValue("Txt_Conto")
     

                Call Btn_Ricerca_Click(sender, Appoggio)



            Else
                Call CaricaUltimi10()
            End If

            If Val(Request.Item("PAGINA")) > 0 Then
                If Val(Request.Item("PAGINA")) < Grid.PageCount Then
                    Grid.PageIndex = Val(Request.Item("PAGINA"))
                End If
            End If

            Tabella = Session("ElencoPianoConti")

            Grid.AutoGenerateColumns = True
            Grid.DataSource = Tabella
            Grid.DataBind()
        End If



        'Btn_Nuovo.Visible = True
    End Sub

    Private Sub CaricaUltimi10()
        Dim cn As New OleDbConnection


        Dim ConnectionString As String = Session("DC_GENERALE")

        cn = New Data.OleDb.OleDbConnection(ConnectionString)

        cn.Open()

        Dim cmd As New OleDbCommand

        cmd.CommandText = "Select top 20  * From PianoConti Order By Mastro,Conto,Sottoconto"
        cmd.Connection = cn



        Tabella.Clear()
        Tabella.Columns.Add("Mastro", GetType(String))
        Tabella.Columns.Add("Conto", GetType(String))
        Tabella.Columns.Add("Sottoconto", GetType(String))
        Tabella.Columns.Add("Descrizione", GetType(String))

        Dim NumeroRiga As Integer = 0

        Dim myPOSTreader As OleDbDataReader = cmd.ExecuteReader()
        Do While myPOSTreader.Read

            NumeroRiga = NumeroRiga + 1
            Dim myriga As System.Data.DataRow = Tabella.NewRow()

            myriga(0) = myPOSTreader.Item("Mastro")
            myriga(1) = myPOSTreader.Item("Conto")
            myriga(2) = myPOSTreader.Item("Sottoconto")
            myriga(3) = myPOSTreader.Item("Descrizione")


            Tabella.Rows.Add(myriga)
        Loop

        myPOSTreader.Close()
        cn.Close()

        Session("UltimoNumero") = NumeroRiga

        Session("ElencoPianoConti") = Tabella

        Grid.AutoGenerateColumns = True

        Grid.DataSource = Tabella

        Grid.DataBind()
    End Sub
    Protected Sub Grid_PageIndexChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewPageEventArgs) Handles Grid.PageIndexChanging
        Tabella = Session("ElencoPianoConti")
        Grid.PageIndex = e.NewPageIndex
        Grid.DataSource = Tabella
        Grid.DataBind()
    End Sub

    Protected Sub Grid_RowCommand(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewCommandEventArgs) Handles Grid.RowCommand
        If e.CommandName = "Seleziona" Then


            Dim d As Integer


            Tabella = Session("ElencoPianoConti")

            d = Val(e.CommandArgument)


            Dim Mastro As String
            Dim Conto As String
            Dim Sottoconto As String


            Mastro = Val(Tabella.Rows(e.CommandArgument).Item(0).ToString)
            Conto = Val(Tabella.Rows(e.CommandArgument).Item(1).ToString)
            Sottoconto = Val(Tabella.Rows(e.CommandArgument).Item(2).ToString)


            Response.Redirect("Pianoconti.aspx?Mastro=" & Mastro & "&Conto=" & Conto & "&Sottoconto=" & Sottoconto & "&PAGINA=" & Grid.PageIndex)
        End If
    End Sub

    Protected Sub ImgRicerca_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles ImgRicerca.Click
        Response.Redirect("Pianoconti.aspx")
    End Sub

    Protected Sub Btn_Ricerca_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles Btn_Ricerca.Click



        Dim cn As New OleDbConnection


        Dim ConnectionString As String = Session("DC_GENERALE")

        cn = New Data.OleDb.OleDbConnection(ConnectionString)

        cn.Open()


        Dim cmd As New OleDbCommand        
        Dim Mastro As Long
        Dim Conto As Long
        Dim Sottoconto As Long
        Dim Vettore(100) As String



        Dim k1 As New Cls_SqlString

        k1.Add("Txt_Conto", Txt_Conto.Text)


        Session("ElencoPianoContiSQLString") = k1

        Vettore = SplitWords(Txt_Conto.Text)

        If Val(Vettore(0)) > 0 And Vettore.Length >= 3 Then

            If Vettore.Length >= 3 Then
                Mastro = Val(Vettore(0))
                Conto = Val(Vettore(1))
                Sottoconto = Val(Vettore(2))

                If Conto = 0 Then
                    cmd.CommandText = "Select top 20  * From PianoConti  Where Mastro = ? Order By Mastro,Conto,Sottoconto"
                    cmd.Connection = cn
                    cmd.Parameters.AddWithValue("@Mastro", Mastro)
                End If

                If Sottoconto = 0 And Conto > 0 Then
                    cmd.CommandText = "Select top 20  * From PianoConti  Where Mastro = ? And Conto = ? Order By Mastro,Conto,Sottoconto"
                    cmd.Connection = cn
                    cmd.Parameters.AddWithValue("@Mastro", Mastro)
                    cmd.Parameters.AddWithValue("@Conto", Conto)
                End If

                If Sottoconto > 0 Then
                    cmd.CommandText = "Select top 20  * From PianoConti  Where Mastro = ? And Conto = ? And Sottoconto = ? Order By Mastro,Conto,Sottoconto"
                    cmd.Connection = cn
                    cmd.Parameters.AddWithValue("@Mastro", Mastro)
                    cmd.Parameters.AddWithValue("@Conto", Conto)
                    cmd.Parameters.AddWithValue("@Sottoconto", Sottoconto)
                End If
            End If
        Else
            cmd.CommandText = "Select top 20  * From PianoConti where Descrizione Like ? Order By Mastro,Conto,Sottoconto"
            cmd.Connection = cn
            cmd.Parameters.AddWithValue("@Descrizione", Txt_Conto.Text)
        End If



        Tabella.Clear()
        Tabella.Columns.Add("Mastro", GetType(String))
        Tabella.Columns.Add("Conto", GetType(String))
        Tabella.Columns.Add("Sottoconto", GetType(String))
        Tabella.Columns.Add("Descrizione", GetType(String))

        Dim NumeroRiga As Integer = 0
        Try



            Dim myPOSTreader As OleDbDataReader = cmd.ExecuteReader()
            Do While myPOSTreader.Read

                NumeroRiga = NumeroRiga + 1
                Dim myriga As System.Data.DataRow = Tabella.NewRow()

                myriga(0) = myPOSTreader.Item("Mastro")
                myriga(1) = myPOSTreader.Item("Conto")
                myriga(2) = myPOSTreader.Item("Sottoconto")
                myriga(3) = myPOSTreader.Item("Descrizione")


                Tabella.Rows.Add(myriga)
            Loop

            myPOSTreader.Close()
        Catch ex As Exception

        End Try


        cn.Close()

        Session("UltimoNumero") = NumeroRiga

        Session("ElencoPianoConti") = Tabella

        Grid.AutoGenerateColumns = True

        Grid.DataSource = Tabella

        Grid.DataBind()

    End Sub


    Private Function SplitWords(ByVal s As String) As String()
        '
        ' Call Regex.Split function from the imported namespace.
        ' Return the result array.
        '
        Return Regex.Split(s, "\W+")
    End Function

    Protected Sub Btn_Esci_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles Btn_Esci.Click
        Response.Redirect("Menu_Generale.aspx")
    End Sub

    

    Protected Sub Img_Espandi_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles Img_Espandi.Click

        Tabella = Session("ElencoPianoConti")
        Dim cn As New OleDbConnection


        Dim ConnectionString As String = Session("DC_GENERALE")

        cn = New Data.OleDb.OleDbConnection(ConnectionString)

        cn.Open()


        Dim cmd As New OleDbCommand
        Dim Mastro As Long
        Dim Conto As Long
        Dim Sottoconto As Long
        Dim Vettore(100) As String



        Dim k1 As New Cls_SqlString

        k1.Add("Txt_Conto", Txt_Conto.Text)


        Session("ElencoPianoContiSQLString") = k1

        Vettore = SplitWords(Txt_Conto.Text)

        If Val(Vettore(0)) > 0 And Vettore.Length >= 3 Then

            If Vettore.Length >= 3 Then
                Mastro = Val(Vettore(0))
                Conto = Val(Vettore(1))
                Sottoconto = Val(Vettore(2))

                If Conto = 0 Then
                    cmd.CommandText = "Select * From PianoConti  Where Mastro = ? Order By Mastro,Conto,Sottoconto"
                    cmd.Connection = cn
                    cmd.Parameters.AddWithValue("@Mastro", Mastro)
                End If

                If Sottoconto = 0 And Conto > 0 Then
                    cmd.CommandText = "Select * From PianoConti  Where Mastro = ? And Conto = ? Order By Mastro,Conto,Sottoconto"
                    cmd.Connection = cn
                    cmd.Parameters.AddWithValue("@Mastro", Mastro)
                    cmd.Parameters.AddWithValue("@Conto", Conto)
                End If

                If Sottoconto > 0 Then
                    cmd.CommandText = "Select * From PianoConti  Where Mastro = ? And Conto = ? And Sottoconto = ? Order By Mastro,Conto,Sottoconto"
                    cmd.Connection = cn
                    cmd.Parameters.AddWithValue("@Mastro", Mastro)
                    cmd.Parameters.AddWithValue("@Conto", Conto)
                    cmd.Parameters.AddWithValue("@Sottoconto", Sottoconto)
                End If
            End If
        Else
            If Txt_Conto.Text.Trim <> "" Then
                cmd.CommandText = "Select * From PianoConti where Descrizione Like ? Order By Mastro,Conto,Sottoconto"
                cmd.Connection = cn
                cmd.Parameters.AddWithValue("@Descrizione", Txt_Conto.Text)
            Else
                cmd.CommandText = "Select * From PianoConti Order By Mastro,Conto,Sottoconto"
                cmd.Connection = cn
                cmd.Parameters.AddWithValue("@Descrizione", Txt_Conto.Text)
            End If
        End If



        Dim IndiceRiga As Integer = 0

        Dim myPOSTreader As OleDbDataReader = cmd.ExecuteReader()
        Do While myPOSTreader.Read
            IndiceRiga = IndiceRiga + 1

            If IndiceRiga > 5 + Session("UltimoNumero") Then
                Exit Do
            End If
            If IndiceRiga >= Session("UltimoNumero") Then
                Dim myriga As System.Data.DataRow = Tabella.NewRow()

                myriga(0) = Val(campodb(myPOSTreader.Item("Mastro")))
                myriga(1) = Val(campodb(myPOSTreader.Item("Conto")))
                myriga(2) = Val(campodb(myPOSTreader.Item("Sottoconto")))
                myriga(3) = campodb(myPOSTreader.Item("Descrizione"))


                Tabella.Rows.Add(myriga)
            End If
        Loop

        myPOSTreader.Close()
        cn.Close()


        Session("UltimoNumero") = IndiceRiga
        Session("ElencoPianoConti") = Tabella

        Grid.AutoGenerateColumns = True

        Grid.DataSource = Tabella

        Grid.DataBind()
    End Sub
    Function campodb(ByVal oggetto As Object) As String
        If IsDBNull(oggetto) Then
            Return ""
        Else
            Return oggetto
        End If
    End Function

End Class
