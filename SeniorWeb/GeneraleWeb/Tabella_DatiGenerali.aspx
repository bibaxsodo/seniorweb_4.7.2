﻿<%@ Page Language="VB" AutoEventWireup="false" Inherits="GeneraleWeb_Tabella_DatiGenerali" CodeFile="Tabella_DatiGenerali.aspx.vb" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="xasp" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
    <meta http-equiv="x-ua-compatible" content="IE=9" />
    <title>Dati Generali</title>
    <asp:PlaceHolder runat="server">
        <%: System.Web.Optimization.Styles.Render("~/Content/AjaxControlToolkit/Styles/Bundle") %>
    </asp:PlaceHolder>
    <link href="ospiti.css?ver=11" rel="stylesheet" type="text/css" />
    <link rel="shortcut icon" href="images/SENIOR.ico" />
    <link href="js/jquery.autocomplete.css" rel="stylesheet" type="text/css" />

    <script src="js/jquery-1.5.1.min.js" type="text/javascript"></script>
    <script src="js/jquery.autocomplete.js" type="text/javascript"></script>
    <script src="js/soapclient.js" type="text/javascript"></script>
    <script src="/js/convertinumero.js" type="text/javascript"></script>

    <script src="js/jquery.maskedinput-1.3.min.js" type="text/javascript"></script>
    <script src="/js/formatnumer.js" type="text/javascript"></script>

    <script src="/js/pianoconti.js" type="text/javascript"></script>
    <script src="js/JSErrore.js" type="text/javascript"></script>
    <link rel="stylesheet" href="jqueryui/jquery-ui.css" type="text/css" />
    <script src="jqueryui/jquery-ui.js" type="text/javascript"></script>
    <script src="jqueryui/datapicker-it.js" type="text/javascript"></script>

    <script type="text/javascript">                

        $(document).ready(function () {
            $('html').keyup(function (event) {
                if (event.keyCode == 113) {
                    __doPostBack("Btn_Modifica", "0");
                }
            });
        });
        $(document).ready(function () {
            if (window.innerHeight > 0) { $("#BarraLaterale").css("height", (window.innerHeight - 94) + "px"); } else { $("#BarraLaterale").css("height", (document.documentElement.offsetHeight - 94) + "px"); }
        });

    </script>
</head>
<body>
    <form id="form1" runat="server" autocomplete="off">
        <asp:ScriptManager ID="ScriptManager1" runat="server" EnableScriptGlobalization="true">
            <Scripts>
                <asp:ScriptReference Path="~/Scripts/AjaxControlToolkit/Bundle" />
            </Scripts>
        </asp:ScriptManager>
        <asp:Label ID="Lbl_BarraSenior" runat="server" Text=""></asp:Label>
        <table style="width: 100%;" cellpadding="0" cellspacing="0">
            <tr>
                <td style="width: 160px; background-color: #F0F0F0;"></td>
                <td>
                    <div class="Titolo">Contabilità - Tabelle - Dati Generali</div>
                    <div class="SottoTitolo">
                        <br />
                        <br />
                    </div>
                </td>
                <td style="text-align: right; vertical-align: top;">
                    <div class="DivTasti">
                        <asp:ImageButton ID="Btn_Modifica" runat="server" Height="38px" ImageUrl="~/images/salva.jpg" class="EffettoBottoniTondi" Width="38px" ToolTip="Inserisci/Modifica (F2)" />
                    </div>
                </td>
            </tr>

            <tr>
                <td style="width: 160px; background-color: #F0F0F0; vertical-align: top; text-align: center;" id="BarraLaterale">
                    <a href="Menu_Generale.aspx" style="border-width: 0px;">
                        <img src="images/Home.jpg" alt="Menù" class="Effetto" /></a><br />
                    <asp:ImageButton ID="Btn_Esci" runat="server" BackColor="Transparent" ImageUrl="../images/Menu_Indietro.png" class="Effetto" ToolTip="Chiudi" />
                </td>
                <td colspan="2" style="background-color: #FFFFFF; vertical-align: top;">
                    <xasp:TabContainer ID="TabContainer1" runat="server" ActiveTabIndex="0" CssClass="TabSenior"
                        Width="100%" BorderStyle="None" Style="margin-right: 39px">
                        <xasp:TabPanel runat="server" HeaderText="Prima Nota" ID="Tab_Anagrafica">
                            <HeaderTemplate>
                                Dati Generali
                            </HeaderTemplate>
                            <ContentTemplate>
                                <br />

                                <label class="LabelCampo">Mastro Clienti : </label>
                                <asp:TextBox ID="Txt_MastroCliente" runat="server" Width="80px" onkeypress="return soloNumeri(event);"></asp:TextBox><br />
                                <br />
                                <label class="LabelCampo">Conto Clienti : </label>
                                <asp:TextBox ID="Txt_ContoClienti" runat="server" Width="80px" onkeypress="return soloNumeri(event);"></asp:TextBox><br />
                                <br />
                                <label class="LabelCampo">Mastro Fornitore : </label>
                                <asp:TextBox ID="Txt_MatroFornitore" runat="server" Width="80px" onkeypress="return soloNumeri(event);"></asp:TextBox><br />
                                <br />
                                <label class="LabelCampo">Conto Fornitore : </label>
                                <asp:TextBox ID="Txt_ContoFornitore" runat="server" Width="80px" onkeypress="return soloNumeri(event);"></asp:TextBox><br />
                                <br />

                                <label class="LabelCampo">Conto Bollo : </label>
                                <asp:TextBox ID="Txt_SottocontoBollo" runat="server" Width="280px"></asp:TextBox><br />
                                <br />

                                <label class="LabelCampo">Bilancio Apertura : </label>
                                <asp:TextBox ID="Txt_SottocontoBilancioApertura" runat="server" Width="280px"></asp:TextBox><br />
                                <br />
                                <label class="LabelCampo">Bilancio Chiusura : </label>
                                <asp:TextBox ID="Txt_SottocontoBilancioChiusura" runat="server" Width="280px"></asp:TextBox><br />
                                <br />
                                <label class="LabelCampo">Profitti e Perdite : </label>
                                <asp:TextBox ID="Txt_SottocontoProfittiPerdite" runat="server" Width="280px"></asp:TextBox><br />
                                <br />
                                <label class="LabelCampo">Utile Patrimoniale : </label>
                                <asp:TextBox ID="Txt_SottocontoUtilePatrimoniale" runat="server" Width="280px"></asp:TextBox><br />
                                <br />
                                <label class="LabelCampo">Perdita Patrimoniale : </label>
                                <asp:TextBox ID="Txt_SottocontoPerdiPatrimoniale" runat="server" Width="280px"></asp:TextBox><br />
                                <br />

                                <label class="LabelCampo">Utile Economico : </label>
                                <asp:TextBox ID="Txt_SottocontoUtileEconomico" runat="server" Width="280px"></asp:TextBox><br />
                                <br />
                                <label class="LabelCampo">Perdita Economico : </label>
                                <asp:TextBox ID="Txt_SottocontoPerditaEconomico" runat="server" Width="280px"></asp:TextBox><br />
                                <br />
                                <label class="LabelCampo">Data Chiusura : </label>
                                <asp:TextBox ID="Txt_DataChiusura" runat="server" Width="90px"></asp:TextBox><br />
                                <br />

                                <label class="LabelCampo">Causale Chiusura : </label>
                                <asp:DropDownList ID="DD_CausaleChiusura" runat="server"></asp:DropDownList>
                                <br />
                                <label class="LabelCampo">Causale Apertura : </label>
                                <asp:DropDownList ID="DD_CausaleApertura" runat="server"></asp:DropDownList>
                                <br />
                                <label class="LabelCampo">Causale Rettifica : </label>
                                <asp:DropDownList ID="DD_CasualeRettifica" runat="server"></asp:DropDownList>
                                <br />
                                <br />
                                <label class="LabelCampo">Stampa Da Gestione Documenti: </label>
                                <asp:CheckBox ID="Chk_StampaDaGestioneDocumenti" runat="server" Text="" />
                                <br />
                                <br />
                                <label class="LabelCampo">Movimenti Ritenuta: </label>
                                <asp:CheckBox ID="Chk_MovimentiRitenuta" runat="server" Text="" />
                                <br />
                                <br />
                                <label class="LabelCampo">Giroconto Ritenuta: </label>
                                <asp:CheckBox ID="Chk_GirocontoRitentute" runat="server" Text="" />
                                <br />
                                <br />
                                <label class="LabelCampo">Blocca Legame : </label>
                                <asp:CheckBox ID="Chk_BloccaLegameScadenzario" runat="server" Text="Blocca Legame su  documenti con Scadenzario" />
                                <br />
                                <br />
                                <label class="LabelCampo">Scadenzario Check Chiuso: </label>
                                <asp:CheckBox ID="Chk_ScadenziarioCheckChiuso" runat="server" Text="" />
                                <br />
                                <br />
                                <label class="LabelCampo">Controllo gestione : </label>
                                <asp:RadioButton ID="Rd_Budget" runat="server" GroupName="BudgetAnalitica" Text="Budget" />
                                <asp:RadioButton ID="Rd_Analitica" runat="server" GroupName="BudgetAnalitica" Text="Analitica" />
                                <br />
                                <br />
                                <label class="LabelCampo">Causale Import Fatture : </label>
                                <asp:DropDownList ID="DD_CasualeDefualt" runat="server"></asp:DropDownList>
                                <br />
                                <br />
                                <label class="LabelCampo">Causale NC Import Fatture : </label>
                                <asp:DropDownList ID="DD_CasualeDefualtNC" runat="server"></asp:DropDownList>
                                <br />
                                <br />


                                <label class="LabelCampo">Causale Import Fatture (Att.2) : </label>
                                <asp:DropDownList ID="DD_CasualeDefualtAttivita2" runat="server"></asp:DropDownList>
                                <br />
                                <br />
                                <label class="LabelCampo">Causale NC Import Fatture (Att.2) : </label>
                                <asp:DropDownList ID="DD_CasualeDefualtNCAttiva2" runat="server"></asp:DropDownList>
                                <br />
                                <br />


                                <label class="LabelCampo">Attiva Centro Servizio : </label>
                                <asp:CheckBox ID="Chk_AttivaCServPrimanoIncassi" runat="server" Text="" />
                                <br />
                                <br />
                                <label class="LabelCampo">Modalità Pagamento Obbligatoria : </label>
                                <asp:CheckBox ID="Chk_ModalitaPagamentoOblDoc" runat="server" Text="" />
                                <br />
                                <br />
                                <label class="LabelCampo">Tipo Visualizzazione : </label>
                                <asp:CheckBox ID="Chk_TipoVisualizzazione" runat="server" Text="Compatta" />
                                <br />
                                <br />
                                <label class="LabelCampo">Analitica Comp. : </label>
                                <asp:CheckBox ID="Chk_CompetenzaObbligatiroAnalitica" runat="server" Text="Competenza Obbligatoria" />
                                <br />
                                <br />
                                <label class="LabelCampo">Gestione Documenti : </label>
                                <asp:CheckBox ID="Chk_GestioneDocumenti" runat="server" Text="Nuova Gestione Documenti" />
                                <br />
                                <br />
                            </ContentTemplate>

                        </xasp:TabPanel>

                        <xasp:TabPanel runat="server" HeaderText="Prima Nota" ID="TabPanel2">
                            <HeaderTemplate>
                                Cespiti
                            </HeaderTemplate>
                            <ContentTemplate>
                                <label class="LabelCampo">Causale Ammort. : </label>
                                <asp:DropDownList ID="DD_CespitiAmmort" runat="server"></asp:DropDownList>
                                <br />
                                <br />
                                <label class="LabelCampo">Cespiti Dimissione : </label>
                                <asp:DropDownList ID="DD_CespitiDimissione" runat="server"></asp:DropDownList>
                                <br />
                                <br />

                            </ContentTemplate>

                        </xasp:TabPanel>

                        <xasp:TabPanel runat="server" HeaderText="Prima Nota" ID="TabPanel1">
                            <HeaderTemplate>
                                Connectors
                            </HeaderTemplate>
                            <ContentTemplate>
                                <br />
                                <hr />
                                <center>AGYO</center>
                                <br />
                                <br />
                                <label class="LabelCampo">EndPoint Effettivo : </label>
                                <asp:CheckBox ID="Chk_EffettivoAgyo" runat="server" Text="" />
                                <br />
                                <br />
                                <label class="LabelCampo">Id Agyo : </label>
                                <asp:TextBox ID="Txt_IdAgyo" runat="server" Width="280px"></asp:TextBox><br />
                                <br />
                                <br />
                                <label class="LabelCampo">Password Agyo : </label>
                                <asp:TextBox ID="Txt_PasswordAgyo" runat="server" Width="280px"></asp:TextBox><br />
                                <br />
                                <br />
                                <label class="LabelCampo">Token Agyo : </label>
                                <asp:TextBox ID="Txt_TokenAgyo" runat="server" Width="280px"></asp:TextBox>
                                <asp:Button ID="BTN_AGYO" runat="server" Text="Leggi Token" />
                                <br />
                                <br />
                            </ContentTemplate>

                        </xasp:TabPanel>

                    </xasp:TabContainer>
                </td>
                <td>
                    <br />
                    <br />
                    <br />
                </td>
            </tr>
        </table>
    </form>
</body>
</html>
