﻿Imports Microsoft.VisualBasic
Imports System.Data.OleDb

Partial Class GeneraleWeb_LibroGiornaleODV
    Inherits System.Web.UI.Page


    Private Sub CreaArchivio()
        Dim Con As New OleDbConnection
        Dim ConStampa As New OleDbConnection
        Dim MySql As String
        Dim Condizione As String

        Dim WSocieta As String

        Dim XC As New Cls_DecodificaSocieta

        WSocieta = XC.DecodificaSocieta(Session("DC_TABELLE"))


        ViewState("PRINTERKEY") = Format(Now, "yyyyMMddHHmmss") & Session.SessionID


        ConStampa.ConnectionString = Session("StampeFinanziaria")
        ConStampa.Open()


        Con.ConnectionString = Session("DC_GENERALE")
        Con.Open()


        Dim cmdDel As New OleDbCommand()
        cmdDel.CommandText = "Delete From Mastrino where PRINTERKEY = '" & ViewState("PRINTERKEY") & "'"
        cmdDel.Connection = ConStampa
        cmdDel.ExecuteNonQuery()



        Condizione = ""
        If IsDate(Txt_DallaData.Text) Then
            If Condizione <> "" Then
                Condizione = Condizione & " And "
            End If
            Condizione = Condizione & " DataRegistrazione >= ?"
        End If
        If IsDate(Txt_DallaData.Text) Then
            If Condizione <> "" Then
                Condizione = Condizione & " And "
            End If
            Condizione = Condizione & " DataRegistrazione <= ?"
        End If

        If Condizione = "" Then
            Exit Sub
        End If

        MySql = "Select  *,MovimentiContabiliTesta.Descrizione as DescrizioneTesta FROM MovimentiContabiliRiga INNER JOIN MovimentiContabiliTesta ON MovimentiContabiliRiga.Numero = MovimentiContabiliTesta.NumeroRegistrazione Where (Invisibile = 0 OR Invisibile Is Null) And  " & Condizione
        Dim cmd As New OleDbCommand()
        cmd.CommandText = MySql
        cmd.Connection = Con

        If IsDate(Txt_DallaData.Text) Then
            cmd.Parameters.AddWithValue("@DALLADATA", Txt_DallaData.Text)
        End If
        If IsDate(Txt_AllaData.Text) Then
            cmd.Parameters.AddWithValue("@ALLADATA", Txt_AllaData.Text)
        End If


        Dim Reader As OleDbDataReader = cmd.ExecuteReader()
        Do While Reader.Read


            Dim cmdScrivi As New OleDbCommand()
            cmdScrivi.CommandText = "INSERT INTO Mastrino (ChiaveSelezione,IntestaSocieta,PRINTERKEY,DecodificaCausaleContabile,NumeroRegistrazione,MastroPartita,ContoPartita,SottocontoPartita,DescrizioneSottocontoPartita,DataRegistrazione,Descrizione,Dare,Avere,ProgressivoAnno,ProgressivoNumero) VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)"
            cmdScrivi.Connection = ConStampa

            cmdScrivi.Parameters.AddWithValue("@ChiaveSelezione", "Periodo dal " & Txt_DallaData.Text & " al " & Txt_AllaData.Text)

            'Dim MyClsLogin As New Cls_Login

            'MyClsLogin.Utente = Session("UTENTE")

            'MyClsLogin.LeggiSP(Application("SENIOR"))

            cmdScrivi.Parameters.AddWithValue("@IntestaSocieta", WSocieta)
            cmdScrivi.Parameters.AddWithValue("@PRINTERKEY", ViewState("PRINTERKEY"))

            Dim kCau As New Cls_CausaleContabile

            kCau.Leggi(Session("DC_TABELLE"), campodb(Reader.Item("CAUSALECONTABILE")))

            cmdScrivi.Parameters.AddWithValue("@CausaleContabile", kCau.Descrizione)
            cmdScrivi.Parameters.AddWithValue("@NumeroRegistrazione", campodbN(Reader.Item("NumeroRegistrazione")))
            cmdScrivi.Parameters.AddWithValue("@MastroPartita", campodbN(Reader.Item("MastroPartita")))
            cmdScrivi.Parameters.AddWithValue("@ContoPartita", campodbN(Reader.Item("ContoPartita")))
            cmdScrivi.Parameters.AddWithValue("@SottocontoPartita", campodbN(Reader.Item("SottocontoPartita")))
            Dim K As New Cls_Pianodeiconti
            K.Mastro = campodbN(Reader.Item("MastroPartita"))
            K.Conto = campodbN(Reader.Item("ContoPartita"))
            K.Sottoconto = campodbN(Reader.Item("SottocontoPartita"))
            K.Decodfica(Session("DC_GENERALE"))

            cmdScrivi.Parameters.AddWithValue("@DescrizioneSottocontoPartita", K.Descrizione)
            cmdScrivi.Parameters.AddWithValue("@DataRegistrazione", campodb(Reader.Item("DataRegistrazione")))
            cmdScrivi.Parameters.AddWithValue("@Descrizione", campodb(Reader.Item("DescrizioneTesta")))
            If campodb(Reader.Item("DareAvere")) = "A" Then
                cmdScrivi.Parameters.AddWithValue("@Dare", 0)
                cmdScrivi.Parameters.AddWithValue("@Avere", campodbN(Reader.Item("Importo")))
            Else
                cmdScrivi.Parameters.AddWithValue("@Dare", campodbN(Reader.Item("Importo")))
                cmdScrivi.Parameters.AddWithValue("@Avere", 0)
            End If
            cmdScrivi.Parameters.AddWithValue("@ProgressivoAnno", campodbN(Reader.Item("ProgressivoAnno")))
            cmdScrivi.Parameters.AddWithValue("@ProgessivoNumero", campodbN(Reader.Item("ProgressivoNumero")))

            cmdScrivi.ExecuteNonQuery()
        Loop
        Con.Close()
        ConStampa.Close()
    End Sub

    Protected Sub Img_Stampa_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles Img_Stampa.Click

        If Not IsDate(Txt_DallaData.Text) Then
            ScriptManager.RegisterClientScriptBlock(Me, Me.GetType(), "Errore", "VisualizzaErrore('Data Dalla errata');", True)

            EseguiJS()
            Exit Sub
        End If

        If Not IsDate(Txt_AllaData.Text) Then
            ScriptManager.RegisterClientScriptBlock(Me, Me.GetType(), "Errore", "VisualizzaErrore('Data alla errata');", True)

            EseguiJS()
            Exit Sub
        End If

        Dim DataDal As Date
        Dim DataAl As Date
        DataDal = Txt_DallaData.Text
        DataAl = Txt_AllaData.Text

        If Format(DataDal, "yyyyMMdd") >= Format(DataAl, "yyyyMMdd") Then
            ScriptManager.RegisterClientScriptBlock(Me, Me.GetType(), "Errore", "VisualizzaErrore('Data Dal Maggiore data Al');", True)

            EseguiJS()
            Exit Sub
        End If

        CreaArchivio()


        Session("SelectionFormula") = "{Mastrino.PRINTERKEY} = " & Chr(34) & ViewState("PRINTERKEY") & Chr(34)
        ScriptManager.RegisterClientScriptBlock(Me, Me.GetType(), "Stampa1", "window.open('StampaReport.aspx?REPORT=GIORNALEODV&PRINTERKEY=ON','Stampe','width=800,height=600');", True)
        EseguiJS()
        Exit Sub



    End Sub

    Protected Sub Btn_Esci_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles Btn_Esci.Click
        Response.Redirect("Menu_Stampe.aspx")
    End Sub

    Function campodb(ByVal oggetto As Object) As String
        If IsDBNull(oggetto) Then
            Return ""
        Else
            Return oggetto
        End If
    End Function

    Function campodbN(ByVal oggetto As Object) As Double
        If IsDBNull(oggetto) Then
            Return 0
        Else
            Return oggetto
        End If
    End Function


    Private Sub EseguiJS()
        Dim MyJs As String
        MyJs = "$(document).ready(function() {"

        MyJs = MyJs & "var els = document.getElementsByTagName(""*"");"

        MyJs = MyJs & "for (var i=0;i<els.length;i++)"
        MyJs = MyJs & "if ( els[i].id ) { "
        MyJs = MyJs & " var appoggio =els[i].id; "
        'MyJs = MyJs & " if (appoggio.match('TxtSottoconto')!= null) {  "
        'MyJs = MyJs & " $(els[i]).autocomplete('/WebHandler/GestioneConto.ashx?Utente=" & Session("UTENTE") & "', {delay:5,minChars:3});"
        'MyJs = MyJs & "    }"

        MyJs = MyJs & " if ((appoggio.match('Txt_DallaData')!= null) || (appoggio.match('Txt_AllaData')!= null)) {  "
        MyJs = MyJs & " $(els[i]).mask(""99/99/9999"");  "
        MyJs = MyJs & "    }"

        'MyJs = MyJs & " if (appoggio.match('Txt_Importo')!= null)  {  "
        'MyJs = MyJs & " $(els[i]).keypress(function() { ForceNumericInput($(this).val(), true, true); } ); "
        'MyJs = MyJs & " $(els[i]).blur(function() { var ap = formatNumber($(this).val(),2); $(this).val(ap); });"
        'MyJs = MyJs & "    }"

        MyJs = MyJs & "} "
        MyJs = MyJs & "});"

        'MyJs = "$(document).ready(function() { $('.myClass').keypress(function() { handleEnter($('.myClass').val(), true, true); } );     $('#" & Txt_ImportoPagato.ClientID & "').blur(function() { var ap = formatNumber ($('#" & Txt_ImportoPagato.ClientID & "').val(),2); $('#" & Txt_ImportoPagato.ClientID & "').val(ap); }); });"
        ScriptManager.RegisterStartupScript(Me, Me.GetType(), "visualizzaRitIm", MyJs, True)
    End Sub


    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load

        EseguiJS()
    End Sub

    Protected Sub ImgMenu_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles ImgMenu.Click
        Response.Redirect("Menu_Stampe.aspx")
    End Sub

    Protected Sub ImageButton1_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles ImageButton1.Click

    End Sub
End Class
