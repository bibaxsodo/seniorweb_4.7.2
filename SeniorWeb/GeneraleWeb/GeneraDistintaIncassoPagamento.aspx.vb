﻿Imports Microsoft.VisualBasic
Imports System.Data.OleDb
Imports System.Web.Hosting

Partial Class GeneraleWeb_GeneraDistintaIncassoPagamento
    Inherits System.Web.UI.Page
    Dim Tabella As New System.Data.DataTable("tabella")

    
    Function AdattaLunghezzaNumero(ByVal Testo As String, ByVal Lunghezza As Integer) As String
        If Len(Testo) > Lunghezza Then
            AdattaLunghezzaNumero = Mid(Testo, 1, Lunghezza)
            Exit Function
        End If
        AdattaLunghezzaNumero = Replace(Space(Lunghezza - Len(Testo)) & Testo, " ", "0")
    End Function

    Function AdattaLunghezza(ByVal Testo As String, ByVal Lunghezza As Integer) As String
        If Len(Testo) > Lunghezza Then
            Testo = Mid(Testo, 1, Lunghezza)
        End If

        AdattaLunghezza = Testo & Space(Lunghezza - Len(Testo) + 1)

        If Len(AdattaLunghezza) > Lunghezza Then
            AdattaLunghezza = Mid(AdattaLunghezza, 1, Lunghezza)
        End If
    End Function

    Protected Sub Img_Etrai_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles Img_Etrai.Click
        Dim cn As OleDbConnection

        Dim DareAvereIncPag As String = 0

        Dim NumeroRegistrazione As Integer
        Dim AnnoProtocollo As Integer
        Dim NumeroProtocollo As Integer
        Dim RagioneSociale As String
        Dim Iban As String
        Dim Importo As Double = 0
        Dim DocumentoPagato As Double = 0


        Dim CausaleContabile As String
        Dim CausaleAcqVend As String


        Dim Mastro As Integer = 0
        Dim Conto As Integer = 0
        Dim Xs As New Cls_CausaleContabile


        Dim DatiGenerali As New Cls_DatiGenerali

        DatiGenerali.LeggiDati(Session("DC_TABELLE"))
        If DD_Clienti.Checked = True Then
            Mastro = DatiGenerali.ClientiMastro
            Conto = DatiGenerali.ClientiConto

            CausaleAcqVend = "V"
        Else
            Mastro = DatiGenerali.FornitoriMastro
            Conto = DatiGenerali.FornitoriConto
            CausaleAcqVend = "A"
        End If


        cn = New Data.OleDb.OleDbConnection(Session("DC_GENERALE"))


        cn.Open()

        Tabella.Clear()
        Tabella.Columns.Clear()
        Tabella.Columns.Add("NumeroRegistrazione", GetType(Integer))
        Tabella.Columns.Add("NumeroProtocollo", GetType(Integer))
        Tabella.Columns.Add("AnnoProtocollo", GetType(Integer))
        Tabella.Columns.Add("RagioneSociale", GetType(String))
        Tabella.Columns.Add("Iban", GetType(String))
        Tabella.Columns.Add("Importo", GetType(Double))
        Tabella.Columns.Add("ID", GetType(Integer))

        Dim MySql As String = ""


        MySql = "SELECT (SELECT SUM(IMPORTO) FROM TABELLALEGAMI WHERE CODICEDOCUMENTO = MovimentiContabiliTesta.NumeroRegistrazione ) AS DocumentoPagato, MovimentiContabiliTesta.NumeroRegistrazione, MovimentiContabiliTesta.CausaleContabile, MovimentiContabiliTesta.DataDocumento, MovimentiContabiliTesta.NumeroDocumento, MovimentiContabiliTesta.Descrizione, MovimentiContabiliTesta.DataRegistrazione, MovimentiContabiliRiga.Importo, MovimentiContabiliRiga.DareAvere, MovimentiContabiliRiga.Tipo " & _
                         " ,MovimentiContabiliTesta.NumeroProtocollo, MovimentiContabiliTesta.AnnoProtocollo  " & _
                         " FROM MovimentiContabiliRiga  INNER JOIN MovimentiContabiliTesta  ON MovimentiContabiliTesta.NumeroRegistrazione = MovimentiContabiliRiga.Numero  " & _
                         " WHERE MovimentiContabiliRiga.MastroPartita = " & Mastro & " AND MovimentiContabiliRiga.ContoPartita = " & Conto & " AND (" & Xs.EstraiCondizioniSQL(Session("DC_TABELLE")) & ")" & _
                         " And RegistroIVA = " & val(DD_RegIVa.SelectedValue) &  _
                         " And (select count(*) From DistintaIncassoPagamentoRiga Where NumeroRegistrazioneDocumento = NumeroRegistrazione) = 0 " &  _
                         " ORDER BY NumeroRegistrazione "

        Dim cmd As New OleDbCommand()
        cmd.CommandText = MySql

        cmd.CommandTimeout = 120
        cmd.Connection = cn

        Dim myPOSTreader As OleDbDataReader = cmd.ExecuteReader()
        Do While myPOSTreader.Read


            If NumeroRegistrazione = campodbN(myPOSTreader.Item("NumeroRegistrazione")) Then
                If DareAvereIncPag = campodb(myPOSTreader.Item("DareAvere")) Then
                    Importo = Importo - campodbN(myPOSTreader.Item("Importo"))
                Else
                    Importo = Importo + campodbN(myPOSTreader.Item("Importo"))
                End If
            Else
                If Math.Abs(Math.Round(DocumentoPagato, 2)) < Math.Abs(Math.Round(Importo, 2)) Then
                    Dim kScadenze As New Cls_Scadenziario
                    Dim TabScadenze As New System.Data.DataTable
                    Dim i As Long
                    Dim Inserito As Boolean = False


                    kScadenze.NumeroRegistrazioneContabile = NumeroRegistrazione
                    kScadenze.loaddati(Session("DC_GENERALE"), TabScadenze)

                    For i = 0 To TabScadenze.Rows.Count - 1
                        If TabScadenze.Rows(i).Item(3).ToString <> "Chiusa" And TabScadenze.Rows(i).Item(1).ToString <> "" Then
                            Dim myriga As System.Data.DataRow = Tabella.NewRow()
                            myriga(0) = NumeroRegistrazione
                            myriga(1) = AnnoProtocollo
                            myriga(2) = NumeroProtocollo
                            myriga(3) = RagioneSociale

                            myriga(4) =  Iban
                            If TabScadenze.Rows(i).Item(1).ToString = "" Then
                                myriga(5) = 0
                            Else
                                myriga(5) = Format(Math.Round(CDbl(TabScadenze.Rows(i).Item(1).ToString), 2), "#,##0.00")

                                Dim Documento As New Cls_MovimentoContabile
                                Dim LegamiDocumento As New Cls_Legami

                                Documento.NumeroRegistrazione = NumeroRegistrazione
                                Documento.Leggi(Session("DC_GENERALE"), Documento.NumeroRegistrazione)


                                If Math.Abs(CDbl(myriga(5))) > Math.Abs(Documento.ImportoDocumento(Session("DC_TABELLE"))) - Math.Abs(LegamiDocumento.TotaleLegame(Session("DC_GENERALE"), Documento.NumeroRegistrazione)) Then
                                    If myriga(5) > 0 Then
                                        myriga(5) = Format(Documento.ImportoDocumento(Session("DC_TABELLE")) - LegamiDocumento.TotaleLegame(Session("DC_GENERALE"), Documento.NumeroRegistrazione), "#,##0.00")
                                    Else

                                        myriga(5) = Format((Documento.ImportoDocumento(Session("DC_TABELLE")) - LegamiDocumento.TotaleLegame(Session("DC_GENERALE"), Documento.NumeroRegistrazione)), "#,##0.00")
                                    End If
                                End If
                            End If

                            myriga(6) = val(TabScadenze.Rows(i).Item(6).ToString)
                            Tabella.Rows.Add(myriga)
                            Inserito = True
                        End If
                    Next
                    If Not Inserito Then
                        Dim myriga As System.Data.DataRow = Tabella.NewRow()
                        myriga(0) = NumeroRegistrazione
                        myriga(1) = AnnoProtocollo
                        myriga(2) = NumeroProtocollo
                        myriga(3) = RagioneSociale
                        myriga(4) =  Iban
                        If Importo < 0 Then
                            myriga(5) = Format(Math.Round((Math.Abs(Importo) - Math.Abs(DocumentoPagato)) * -1, 2), "#,##0.00")
                        Else
                            myriga(5) = Format(Math.Round(Importo - DocumentoPagato, 2), "#,##0.00")
                        End If         
                        myriga(6) =0
                        Tabella.Rows.Add(myriga)
                    End If
                End If
            End If
            NumeroRegistrazione = myPOSTreader.Item("NumeroRegistrazione")
            NumeroProtocollo = campodbN(myPOSTreader.Item("NumeroProtocollo"))
            AnnoProtocollo = campodbN(myPOSTreader.Item("AnnoProtocollo"))
            DocumentoPagato = Math.Abs(campodbN(myPOSTreader.Item("DocumentoPagato")))

            CausaleContabile = campodb(myPOSTreader.Item("CausaleContabile"))



            Dim Registrazione As New Cls_MovimentoContabile

            Registrazione.NumeroRegistrazione = NumeroRegistrazione
            Registrazione.Leggi(Session("DC_GENERALE"), Registrazione.NumeroRegistrazione)

            If Not IsNothing(Registrazione.Righe(0)) Then
                Dim PDC As New Cls_Pianodeiconti


                PDC.Mastro = Registrazione.Righe(0).MastroPartita
                PDC.Conto = Registrazione.Righe(0).ContoPartita
                PDC.Sottoconto = Registrazione.Righe(0).SottocontoPartita
                PDC.Decodfica(Session("DC_GENERALE"))

                RagioneSociale = PDC.Descrizione


                if PDC.TipoAnagrafica ="D" and DD_Fornitori.Checked = true  then
                    Dim Clifor as new Cls_ClienteFornitore

                    Clifor.MastroFornitore =  PDC.Mastro 
                    Clifor.ContoFornitore =  PDC.Conto
                    Clifor.SottoContoFornitore =  PDC.Sottoconto
                    Clifor.Leggi(Session("DC_OSPITE"))
                    
                    iban = AdattaLunghezza(Clifor.IntFornitore, 2) & AdattaLunghezzaNumero(Clifor.NumeroControlloFornitore, 2) & AdattaLunghezza(Clifor.CinFornitore, 1) & AdattaLunghezzaNumero(Clifor.ABIFornitore, 5) & AdattaLunghezzaNumero(Clifor.CABFornitore, 5) & AdattaLunghezzaNumero(Clifor.CCBancarioFornitore, 12)
                    
                End If
            Else
                RagioneSociale = ""
            End If

            
            Dim XCau As New Cls_CausaleContabile

            XCau.Leggi(Session("DC_TABELLE"), myPOSTreader.Item("CausaleContabile"))

            If XCau.VenditaAcquisti <> CausaleAcqVend Then
                If DareAvereIncPag <> myPOSTreader.Item("DareAvere") Then
                    Importo = Math.Round(myPOSTreader.Item("Importo") * -1, 2)
                Else
                    Importo = Math.Round(myPOSTreader.Item("Importo"), 2)
                End If
            Else
                If DareAvereIncPag = myPOSTreader.Item("DareAvere") Then
                    Importo = Math.Round(myPOSTreader.Item("Importo") * -1, 2)
                Else
                    Importo = Math.Round(myPOSTreader.Item("Importo"), 2)
                End If
            End If

        Loop
        cn.Close()

        Grid.AutoGenerateColumns = False
        Grid.DataSource = Tabella
        Grid.DataBind()

        
        Session("TABELLA") = Tabella

        LblBox.Text = ""
        Img_Modifica.Visible=true
    End Sub

    Private Sub GeneraleWeb_GeneraDistintaIncassoPagamento_Load(sender As Object, e As EventArgs) Handles Me.Load
        
        LblBox.Text = ""
        Call EseguiJS()

        If Page.IsPostBack = True Then
            Exit Sub
        End If


        Dim K1 As New Cls_SqlString

        Dim Barra As New Cls_BarraSenior

        If Not IsNothing(Session("RicercaGeneraleSQLString")) Then
            K1 = Session("RicercaGeneraleSQLString")
        End If

        

        Dim MyReg As New Cls_RegistroIVA

        MyReg.UpDateDropBox(Session("DC_TABELLE"), DD_RegIVa)

        Img_Modifica.Visible=False
        Lbl_BarraSenior.Text = Barra.CodiceBarra(Application("SENIOR"), Session("UTENTE"), Page, K1)
    End Sub
    Private Sub EseguiJS()
        Dim MyJs As String
        MyJs = "$(document).ready(function() {"

        MyJs = MyJs & "var els = document.getElementsByTagName(""*"");"

        MyJs = MyJs & "for (var i=0;i<els.length;i++)"
        MyJs = MyJs & "if ( els[i].id ) { "
        MyJs = MyJs & " var appoggio =els[i].id; "


        MyJs = MyJs & " if ((appoggio.match('Txt_Import')!= null) || appoggio.match('Base')!= null) {  "
        MyJs = MyJs & " $(els[i]).keypress(function() { ForceNumericInput($(this).val(), true, true); } ); "
        MyJs = MyJs & " $(els[i]).blur(function() { var ap = formatNumber($(this).val(),2); $(this).val(ap); });"
        MyJs = MyJs & "    }"

        MyJs = MyJs & " if (appoggio.match('Txt_Data')!= null) {  "
        MyJs = MyJs & " $(els[i]).mask(""99/99/9999""); "

        MyJs = MyJs & " $(els[i]).datepicker({ changeMonth: true,changeYear: true,  yearRange: ""1990:" & Year(Now) + 5 & """},$.datepicker.regional[""it""]);"

        MyJs = MyJs & "    }"

        MyJs = MyJs & "} "
        MyJs = MyJs & "});"

        ScriptManager.RegisterStartupScript(Me, Me.GetType(), "JS_GESTPRE", MyJs, True)
    End Sub
    Function campodb(ByVal oggetto As Object) As String
        If IsDBNull(oggetto) Then
            Return ""
        Else
            Return oggetto
        End If
    End Function
    Function campodbN(ByVal oggetto As Object) As Double
        If IsDBNull(oggetto) Then
            Return 0
        Else
            Return oggetto
        End If
    End Function
    Function campodbD(ByVal oggetto As Object) As Date
        If IsDBNull(oggetto) Then
            Return Nothing
        Else
            Return oggetto
        End If
    End Function

    Private Sub Img_Modifica_Click(sender As Object, e As ImageClickEventArgs) Handles Img_Modifica.Click
        Dim I as  Integer
        Dim x as  Integer =0

        Dim Distinta as new Cls_DistintaIncassoPagamento


        Distinta.DataDistinta = Txt_Data.Text
        Distinta.Descrizione = Txt_Descrizione.Text
        Distinta.Generato = 0
        Distinta.TipoAV = "A"
        If DD_Clienti.Checked = True then
            Distinta.TipoAV = "V"
        End If



        Tabella =Session("TABELLA")

        For I =0 to  Tabella.Rows.Count -1
            Dim CheckBox As CheckBox = DirectCast(Grid.Rows(i).FindControl("ChkImporta"), CheckBox)

            if CheckBox.Checked = True then
                if val(Tabella.Rows(i).Item(6)) >0 Then                
                    Distinta.Riga(x)   = new Cls_DistintaIncassoPagamentoRiga

                    Distinta.Riga(x).IdScadenza =val(Tabella.Rows(i).Item(6))
                    Distinta.Riga(x).Importo =  cdbl(Tabella.Rows(i).Item(5))
                    Distinta.Riga(x).NumeroRegistrazioneDocumento = val(Tabella.Rows(i).Item(0))                
                    x = x + 1
                End If
            End If
        Next

        Distinta.Max = x

        Distinta.Scrivi(Session("DC_GENERALE"), 0)

       Dim MyJs  as  String
       Dim BottoneChiudi As String = "<br/><a href=""#"" onclick=""ChiudiConfermaModifica();""  style=""position: inherit;bottom: 20px;left: 30%;width: 40%;""><input type=""button"" class=""SeniorButton"" value=""CHIUDI"" style=""width: 100%;""></a>"
       MyJs = "<div id=""ConfermaModifica"" class=""confermamodifca""  style=""position:absolute; top:20%; left:42%; width:24%;  height:23%;""><br />Salvato Distinta " & Distinta.Id & BottoneChiudi & "</div>"
       LblBox.Text = MyJs

        Img_Modifica.Visible=False
    End Sub
End Class
