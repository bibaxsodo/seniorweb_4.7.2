﻿Imports Microsoft.VisualBasic
Imports System.Data.OleDb
Imports System.Web.Hosting
Partial Class GeneraleWeb_RicercaPrenotazione
    Inherits System.Web.UI.Page
    Dim Tabella As New System.Data.DataTable("tabella")

    Private Function SplitWords(ByVal s As String) As String()
        '
        ' Call Regex.Split function from the imported namespace.
        ' Return the result array.
        '
        Return Regex.Split(s, "\W+")
    End Function
    Private Sub CaricaGriglia(ByVal Errore As Boolean)
        Dim MySql As String
        Dim Condizione As String


        Condizione = ""


        If Txt_Descrizione.Text <> "" Then
            If Condizione <> "" Then
                Condizione = Condizione & " And "
            End If
            Condizione = Condizione & " PrenotazioniTesta.Descrizione Like '" & Txt_Descrizione.Text & "'"
        End If


        If Val(Txt_Anno.Text) <> 0 Then
            If Condizione <> "" Then
                Condizione = Condizione & " And "
            End If
            Condizione = Condizione & " PrenotazioniRiga.Anno = " & Txt_Anno.Text
        End If

        Dim Livello1 As Long
        Dim Livello2 As Long
        Dim Livello3 As Long

        Dim Vettore(100) As String
        Vettore = SplitWords(Txt_Budget.Text)

        If Vettore.Length >= 3 Then
            Livello1 = Val(Vettore(0))
            Livello2 = Val(Vettore(1))
            Livello3 = Val(Vettore(2))
        End If

        If Livello1 <> 0 Then
            If Condizione <> "" Then
                Condizione = Condizione & " And "
            End If
            Condizione = Condizione & " PrenotazioniRiga.Livello1 = " & Livello1
        End If

        If Livello2 <> 0 Then
            If Condizione <> "" Then
                Condizione = Condizione & " And "
            End If
            Condizione = Condizione & " PrenotazioniRiga.Livello2 = " & Livello2
        End If

        If Livello3 <> 0 Then
            If Condizione <> "" Then
                Condizione = Condizione & " And "
            End If
            Condizione = Condizione & " PrenotazioniRiga.Livello3 = " & Livello3
        End If

        If Val(DD_Colonna.SelectedValue) <> 0 Then
            If Condizione <> "" Then
                Condizione = Condizione & " And "
            End If
            Condizione = Condizione & " PrenotazioniRiga.Colonna = " & Val(DD_Colonna.SelectedValue)
        End If

        If Txt_Descrizione.Text <> "" Then
            If Condizione <> "" Then
                Condizione = Condizione & " And "
            End If
            Condizione = Condizione & " PrenotazioniRiga.Descrizione Like '" & Txt_Descrizione.Text & "'"
        End If


        If IsDate(Txt_DataDal.Text) And IsDate(Txt_DataAl.Text) Then
            If Condizione <> "" Then
                Condizione = Condizione & " And "
            End If
            Condizione = Condizione & " PrenotazioniTesta.Data >=  ? And PrenotazioniTesta.Data <= ?"
        End If



        If Condizione = "" Then
            ClientScript.RegisterClientScriptBlock(Me.GetType(), "Errore", "VisualizzaErrore('Errore nessuna condizione indicata');", True)
            Exit Sub
        End If


        If Chk_SoloAperte.Checked = True Then
            If Condizione <> "" Then
                Condizione = Condizione & " And "
            End If
            Condizione = Condizione & " PrenotazioniRiga.Importo > "
            Condizione = Condizione & " ISNULL((Select sum(PrenotazioniUsate.Importo) From PrenotazioniUsate Where PrenotazioniUsate.AnnoPrenotazione = PrenotazioniRiga.Anno  And PrenotazioniUsate.NumeroPrenotazione = PrenotazioniRiga.Numero  And PrenotazioniUsate.RigaPrenotazione = PrenotazioniRiga.Riga),0)  + "
            Condizione = Condizione & " ISNULL((Select sum(VariazioniPrenotazioni.Importo) From VariazioniPrenotazioni Where VariazioniPrenotazioni.AnnoPrenotazione = PrenotazioniRiga.Anno  And VariazioniPrenotazioni.NumeroPrenotazione = PrenotazioniRiga.Numero  And VariazioniPrenotazioni.RigaPrenotazione = PrenotazioniRiga.Riga),0) "
        End If



        MySql = "SELECT PrenotazioniTesta.Anno, PrenotazioniTesta.Numero,PrenotazioniRiga.Riga, PrenotazioniTesta.Descrizione, PrenotazioniTesta.TipoAtto "
        MySql = MySql & " ,PrenotazioniTesta.NumeroAtto, PrenotazioniTesta.DataAtto, PrenotazioniRiga.Descrizione"
        MySql = MySql & " ,PrenotazioniRiga.Livello1,PrenotazioniRiga.Livello2,PrenotazioniRiga.Livello3"
        MySql = MySql & " ,(Select top 1 TipoBudget.Descrizione From TipoBudget Where TipoBudget.Livello1 = PrenotazioniRiga.Livello1 And TipoBudget.Livello2 = PrenotazioniRiga.Livello2 And TipoBudget.Livello3 = PrenotazioniRiga.Livello3 order by id desc) As DecodificaBudget"
        MySql = MySql & " ,PrenotazioniRiga.Colonna "
        MySql = MySql & " ,(Select top 1 ColonneBudget.Descrizione From ColonneBudget Where ColonneBudget.Anno = PrenotazioniRiga.Anno And ColonneBudget.Livello1 = PrenotazioniRiga.Colonna) As DecodificaColonna"
        'And ColonneBudget.Livello1 = ColonneBudget.Colonna
        MySql = MySql & " ,PrenotazioniRiga.Importo"
        MySql = MySql & " ,(Select sum(PrenotazioniUsate.Importo) From PrenotazioniUsate Where PrenotazioniUsate.AnnoPrenotazione = PrenotazioniRiga.Anno  And PrenotazioniUsate.NumeroPrenotazione = PrenotazioniRiga.Numero  And PrenotazioniUsate.RigaPrenotazione = PrenotazioniRiga.Riga) As Liquidato "
        MySql = MySql & " ,(Select sum(VariazioniPrenotazioni.Importo) From VariazioniPrenotazioni Where VariazioniPrenotazioni.AnnoPrenotazione = PrenotazioniRiga.Anno  And VariazioniPrenotazioni.NumeroPrenotazione = PrenotazioniRiga.Numero  And VariazioniPrenotazioni.RigaPrenotazione = PrenotazioniRiga.Riga) As Diminuzioni "
        MySql = MySql & " FROM PrenotazioniTesta INNER JOIN PrenotazioniRiga ON (PrenotazioniRiga.Numero = PrenotazioniTesta.Numero) AND (PrenotazioniTesta.Anno = PrenotazioniRiga.Anno) "
        MySql = MySql & " Where " & Condizione


        Tabella.Clear()
        Tabella.Columns.Clear()
        Tabella.Columns.Add("Anno", GetType(Long))
        Tabella.Columns.Add("Numero", GetType(Long))
        Tabella.Columns.Add("Riga", GetType(Long))
        Tabella.Columns.Add("DecodificaBudget", GetType(String))
        Tabella.Columns.Add("DecodificaColonna", GetType(String))
        Tabella.Columns.Add("Importo", GetType(Double))
        Tabella.Columns.Add("Liquidato", GetType(Double))
        Tabella.Columns.Add("Diminuzioni", GetType(Double))



        Dim cn As OleDbConnection

        cn = New Data.OleDb.OleDbConnection(Session("DC_GENERALE"))

        cn.Open()

        Dim cmd As New OleDbCommand()

        cmd.CommandText = (MySql)
        cmd.Connection = cn

        If IsDate(Txt_DataDal.Text) And IsDate(Txt_DataAl.Text) Then
            cmd.Parameters.AddWithValue("@DATA", Txt_DataDal.Text)
            cmd.Parameters.AddWithValue("@DATA", Txt_DataAl.Text)
        End If

        Dim ReadCmd As OleDbDataReader = cmd.ExecuteReader()
        Do While ReadCmd.Read
            Dim myriga As System.Data.DataRow = Tabella.NewRow()
            myriga(0) = campodbN(ReadCmd.Item("Anno"))
            myriga(1) = campodbN(ReadCmd.Item("Numero"))
            myriga(2) = campodbN(ReadCmd.Item("Riga"))
            myriga(3) = campodb(ReadCmd.Item("DecodificaBudget"))
            myriga(4) = campodb(ReadCmd.Item("DecodificaColonna"))
            myriga(5) = campodbN(ReadCmd.Item("Importo"))
            myriga(6) = campodbN(ReadCmd.Item("Liquidato"))
            myriga(7) = campodbN(ReadCmd.Item("Diminuzioni"))

            Tabella.Rows.Add(myriga)

        Loop
        cn.Close()

        ViewState("RicercaPrenotazione") = Tabella

        If Errore = True Then
            GridView1.AutoGenerateColumns = True
            GridView1.DataSource = Tabella
            GridView1.DataBind()
        Else
            Grid.AutoGenerateColumns = True
            Grid.DataSource = Tabella
            Grid.DataBind()
        End If
    End Sub
    Protected Sub ImageButton1_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles ImageButton1.Click
        Call CaricaGriglia(False)
        Dim MyJs As String

        MyJs = "$(document).ready(function() { $(" & Chr(34) & "#" & Txt_Budget.ClientID & Chr(34) & ").autocomplete('autocompletecontobudget.ashx?Anno=" & Val(Txt_Anno.Text) & "&Utente=" & Session("UTENTE") & "', {delay:5,minChars:3}); });"
        ScriptManager.RegisterStartupScript(Me, Me.GetType(), "ESTBUDGET", MyJs, True)
    End Sub
    Function campodbN(ByVal oggetto As Object) As Double
        If IsDBNull(oggetto) Then
            Return 0
        Else
            Return oggetto
        End If
    End Function
    
    Function campodb(ByVal oggetto As Object) As String
        If IsDBNull(oggetto) Then
            Return ""
        Else
            Return oggetto
        End If
    End Function

    Protected Sub Txt_Anno_TextChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles Txt_Anno.TextChanged
        Dim MyJs As String

        MyJs = "$(document).ready(function() { $(" & Chr(34) & "#" & Txt_Budget.ClientID & Chr(34) & ").autocomplete('autocompletecontobudget.ashx?Anno=" & Val(Txt_Anno.Text) & "&Utente=" & Session("UTENTE") & "', {delay:5,minChars:3}); });"
        ScriptManager.RegisterStartupScript(Me, Me.GetType(), "ESTBUDGET", MyJs, True)

        Dim XS As New Cls_colonnebudget

        XS.Anno = Val(Txt_Anno.Text)

        XS.UpDateDropBox(Session("DC_GENERALE"), DD_Colonna)
    End Sub

    Protected Sub ImageButton2_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles ImageButton2.Click
        Call CaricaGriglia(True)

        If Tabella.Rows.Count > 1 Then
            Response.Clear()
            Response.AddHeader("content-disposition", "attachment;filename=Ricerca.xls")
            Response.Charset = String.Empty
            Response.Cache.SetCacheability(HttpCacheability.NoCache)
            Response.ContentType = "application/vnd.xls"
            Dim stringWrite As New System.IO.StringWriter
            Dim htmlWrite As New HtmlTextWriter(stringWrite)

            form1.Controls.Clear()
            form1.Controls.Add(GridView1)

            form1.RenderControl(htmlWrite)

            Response.Write(stringWrite.ToString())
            Response.End()
        End If

        Dim MyJs As String

        MyJs = "$(document).ready(function() { $(" & Chr(34) & "#" & Txt_Budget.ClientID & Chr(34) & ").autocomplete('autocompletecontobudget.ashx?Anno=" & Val(Txt_Anno.Text) & "&Utente=" & Session("UTENTE") & "', {delay:5,minChars:3}); });"
        ScriptManager.RegisterStartupScript(Me, Me.GetType(), "ESTBUDGET", MyJs, True)
    End Sub

    Protected Sub Grid_RowCommand(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewCommandEventArgs) Handles Grid.RowCommand
        If e.CommandName = "Seleziona" Then
            Dim ConnectionString As String = Session("DC_OSPITE")
            Dim d As Integer

            If ViewState("XLINEA") <> -1 Then
                Grid.Rows(ViewState("XLINEA")).BackColor = ViewState("XBACKUP")
            End If


            Tabella = ViewState("RicercaPrenotazione")
            d = Val(e.CommandArgument)
            Session("AnnoPrenotazione") = Val(Tabella.Rows(d).Item(0).ToString)
            Session("NumeroPrenotazione") = Val(Tabella.Rows(d).Item(1).ToString)


            ViewState("XLINEA") = d
            ViewState("XBACKUP") = Grid.Rows(d).BackColor
            Grid.Rows(d).BackColor = Drawing.Color.Violet
        End If

        If (e.CommandName = "Richiama") Then            
            Dim d As Long

            Tabella = ViewState("RicercaPrenotazione")

            d = Val(e.CommandArgument)
            Session("AnnoPrenotazione") = Val(Tabella.Rows(d).Item(0).ToString)
            Session("NumeroPrenotazione") = Val(Tabella.Rows(d).Item(1).ToString)
            ClientScript.RegisterClientScriptBlock(Me.GetType(), "MyBox", "$(window).load(function() {  DialogBox('../GeneraleWeb/GestionePrenotazione.aspx?NUMERO=" & Session("NumeroPrenotazione") & "&Anno=" & Session("AnnoPrenotazione") & "');  });", True)
            REM Response.Redirect("../GeneraleWeb/primanota.aspx")
            Exit Sub
        End If

    End Sub

    Protected Sub Grid_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles Grid.SelectedIndexChanged

    End Sub

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Trim(Session("UTENTE")) = "" Then
            If Request.ServerVariables("URL").ToUpper.IndexOf("SENIORWEBBLANCO") > 0 Then
                Response.Redirect("/ODV/Login.aspx")
                Exit Sub
            Else
                Response.Redirect("/SeniorWeb/Login.aspx")
                Exit Sub
            End If
        End If
        Call EseguiJS()


        If Page.IsPostBack = True Then
            Exit Sub
        End If


        Dim K1 As New Cls_SqlString

        Dim Barra As New Cls_BarraSenior

        If Not IsNothing(Session("RicercaAnagraficaSQLString")) Then
            Try
                K1 = Session("RicercaAnagraficaSQLString")
            Catch ex As Exception

            End Try
        End If

        Lbl_BarraSenior.Text = Barra.CodiceBarra(Application("SENIOR"), Session("UTENTE"), Page, K1)




        ViewState("XLINEA") = -1
        ViewState("XBACKUP") = 0
    End Sub

    Protected Sub Btn_Esci_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles Btn_Esci.Click
        Response.Redirect("Menu_Budget.aspx")
    End Sub

    Protected Sub GridView1_RowCommand(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewCommandEventArgs) Handles GridView1.RowCommand

    End Sub

    Private Sub EseguiJS()
        Dim MyJs As String
        MyJs = "$(document).ready(function() { "

        MyJs = MyJs & "var els = document.getElementsByTagName(""*"");"

        MyJs = MyJs & "for (var i=0;i<els.length;i++)"
        MyJs = MyJs & "if ( els[i].id ) { "
        MyJs = MyJs & " var appoggio =els[i].id; "
        MyJs = MyJs & " if (appoggio.match('Txt_Sottoconto')!= null) {  "
        MyJs = MyJs & " $(els[i]).autocomplete('/WebHandler/GestioneConto.ashx?Utente=" & Session("UTENTE") & "', {delay:5,minChars:3});"
        MyJs = MyJs & "    }"

        MyJs = MyJs & " if (appoggio.match('Txt_Data')!= null) {  "
        MyJs = MyJs & " $(els[i]).mask(""99/99/9999"");  "
        MyJs = MyJs & " $(els[i]).datepicker({ changeMonth: true,changeYear: true,  yearRange: ""1890:" & Year(Now) + 1 & """},$.datepicker.regional[""it""]);"
        MyJs = MyJs & "    }"

        MyJs = MyJs & " if (appoggio.match('Txt_Importo')!= null)  {  "
        MyJs = MyJs & " $(els[i]).keypress(function() { ForceNumericInput($(this).val(), true, true); } ); "
        MyJs = MyJs & " $(els[i]).blur(function() { var ap = formatNumber($(this).val(),2); $(this).val(ap); });"
        MyJs = MyJs & "    }"

        MyJs = MyJs & "$("".chosen-select"").chosen({"
        MyJs = MyJs & "no_results_text: ""Causale non trovata"","
        MyJs = MyJs & "display_disabled_options: true,"
        'MyJs = MyJs & "allow_single_deselect: true,"
        MyJs = MyJs & "width: ""350px"","
        MyJs = MyJs & "placeholder_text_single: ""Seleziona Causale"""
        MyJs = MyJs & "});"


        MyJs = MyJs & "} "
        MyJs = MyJs & " $(" & Chr(34) & "#" & Txt_Budget.ClientID & Chr(34) & ").autocomplete('autocompletecontobudget.ashx?Anno=" & Val(Txt_Anno.Text) & "&Utente=" & Session("UTENTE") & "', {delay:5,minChars:3}); "
        MyJs = MyJs & "});"

        'MyJs = "$(document).ready(function() { $('.myClass').keypress(function() { handleEnter($('.myClass').val(), true, true); } );     $('#" & Txt_ImportoPagato.ClientID & "').blur(function() { var ap = formatNumber ($('#" & Txt_ImportoPagato.ClientID & "').val(),2); $('#" & Txt_ImportoPagato.ClientID & "').val(ap); }); });"
        ScriptManager.RegisterStartupScript(Me, Me.GetType(), "visualizzaRitIm", MyJs, True)
    End Sub


End Class

