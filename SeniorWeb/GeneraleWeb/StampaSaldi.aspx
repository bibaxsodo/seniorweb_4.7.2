﻿<%@ Page Language="VB" AutoEventWireup="false" Inherits="GeneraleWeb_StampaSaldi" CodeFile="StampaSaldi.aspx.vb" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="xasp" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
    <meta http-equiv="x-ua-compatible" content="IE=9" />
    <title>Stampa Saldi</title>
    <asp:PlaceHolder runat="server">
        <%: System.Web.Optimization.Styles.Render("~/Content/AjaxControlToolkit/Styles/Bundle") %>
    </asp:PlaceHolder>
    <link href="ospiti.css?ver=11" rel="stylesheet" type="text/css" />
    <link rel="shortcut icon" href="images/SENIOR.ico" />
    <link href="js/jquery.autocomplete.css" rel="stylesheet" type="text/css" />

    <script src="js/jquery-1.5.1.min.js" type="text/javascript"></script>
    <script src="js/jquery.autocomplete.js" type="text/javascript"></script>
    <script src="js/soapclient.js" type="text/javascript"></script>
    <script src="/js/convertinumero.js" type="text/javascript"></script>

    <script src="js/jquery.maskedinput-1.3.min.js" type="text/javascript"></script>
    <script src="/js/formatnumer.js" type="text/javascript"></script>

    <script src="/js/pianoconti.js" type="text/javascript"></script>
    <script src="js/JSErrore.js" type="text/javascript"></script>
    <link rel="stylesheet" href="jqueryui/jquery-ui.css" type="text/css" />
    <script src="jqueryui/jquery-ui.js" type="text/javascript"></script>
    <script src="jqueryui/datapicker-it.js" type="text/javascript"></script>
    <script type="text/javascript">
        $(document).ready(function () {
            if (window.innerHeight > 0) { $("#BarraLaterale").css("height", (window.innerHeight - 105) + "px"); } else { $("#BarraLaterale").css("height", (document.documentElement.offsetHeight - 105) + "px"); }
        });
        function openPopUp(urlToOpen, nome, parametri) {
            var popup_window = window.open(urlToOpen, '_blank');
            try {
                popup_window.focus();
            } catch (e) {
                alert("E' stata bloccata l'apertura del popup da parte del browser");
            }
        }
    </script>
    <style>
        .wait {
            border: solid 1px #2d2d2d;
            text-align: center;
            vertical-align: top;
            background: white;
            width: 20%;
            height: 180px;
            top: 30%;
            left: 40%;
            position: absolute;
            -moz-border-radius: 5px;
            -webkit-border-radius: 5px;
            border-radius: 5px;
            box-shadow: 0px 0px 10px 4px rgba(0, 125, 196, 0.75);
            -moz-box-shadow: 0px 0px 10px 4px rgba(0, 125, 196, 0.75);
            -webkit-box-shadow: 0px 0px 10px 4px rgba(0, 125, 196, 0.75);
        }


        #blur {
            width: 100%;
            background-color: black;
            moz-opacity: 0.5;
            khtml-opacity: .5;
            opacity: .5;
            filter: alpha(opacity=50);
            z-index: 120;
            height: 100%;
            position: absolute;
            top: 0;
            left: 0;
        }

        #progress {
            z-index: 200;
            background-color: White;
            position: absolute;
            top: 0pt;
            left: 0pt;
            border: solid 1px black;
            padding: 5px 5px 5px 5px;
            text-align: center;
        }
    </style>

    <script type="text/javascript">

</script>
</head>
<body>
    <form id="form1" runat="server" autocomplete="off">
        <asp:ScriptManager ID="ScriptManager1" runat="server" AsyncPostBackTimeout="4000" EnableScriptGlobalization="true">
            <Scripts>
                <asp:ScriptReference Path="~/Scripts/AjaxControlToolkit/Bundle" />
            </Scripts>
        </asp:ScriptManager>
        <asp:Label ID="Lbl_BarraSenior" runat="server" Text=""></asp:Label>
        <div align="left">
            <asp:UpdatePanel ID="UpdatePanel1" runat="server">
                <ContentTemplate>

                    <table style="width: 100%;" cellpadding="0" cellspacing="0">
                        <tr>
                            <td style="width: 160px; background-color: #F0F0F0;"></td>
                            <td>
                                <div class="Titolo">Contabilità - Stampa - Saldi</div>
                                <div class="SottoTitolo">
                                    <br />
                                    <br />
                                </div>
                            </td>
                            <td style="text-align: right; vertical-align: top;">
                                <div class="DivTasti">
                                    <asp:ImageButton ID="ImageButton1" runat="server" Height="38px" ImageUrl="~/generaleweb/images/printer-blue.png" class="EffettoBottoniTondi" Width="38px" />
                                </div>
                            </td>
                        </tr>

                        <tr>
                            <td style="width: 160px; background-color: #F0F0F0; vertical-align: top; text-align: center;" id="BarraLaterale">
                                <asp:ImageButton ID="ImgMenu" ImageUrl="images/Home.jpg" Width="112px" alt="Menù" class="Effetto" runat="server" /><br />
                                <asp:ImageButton ID="Btn_Esci" runat="server" BackColor="Transparent" ImageUrl="../images/Menu_Indietro.png" class="Effetto" ToolTip="Chiudi" />
                                <br />
                                <br />
                                <br />
                                <br />
                                <br />
                                <br />
                                <br />
                                <br />
                                <br />
                                <br />
                                <br />
                                <br />
                                <br />
                                <br />
                                <br />
                                <br />
                                <br />
                                <br />
                                <br />
                                <br />
                                <br />
                                <br />
                                <br />
                                <br />
                                <br />
                                <br />
                                <br />
                                <br />
                                <br />
                            </td>
                            <td colspan="2" style="background-color: #FFFFFF;" valign="top">
                                <xasp:TabContainer ID="TabContainer1" runat="server" ActiveTabIndex="0" CssClass="TabSenior" Width="100%" BorderStyle="None" Style="margin-right: 39px">
                                    <xasp:TabPanel runat="server" HeaderText="Stampa Saldi" ID="Tab_Anagrafica">
                                        <HeaderTemplate>
                                            Stampa Saldi
                         
                                        </HeaderTemplate>

                                        <ContentTemplate>




                                            <label class="LabelCampo">Al Conto:</label>
                                            <asp:TextBox ID="Txt_SottocontoDal" class="Sottoconto" CssClass="MyAutoComplete" runat="server"
                                                Width="488px"></asp:TextBox>
                                            <br />
                                            <br />

                                            <label class="LabelCampo">Dal Conto:</label>
                                            <asp:TextBox ID="Txt_SottocontoAl" CssClass="MyAutoComplete" runat="server" Width="489px"></asp:TextBox>
                                            <br />
                                            <br />

                                            <label class="LabelCampo">Data Al:</label>
                                            <asp:TextBox ID="Txt_DataDal" runat="server" Width="90px"></asp:TextBox>

                                            <br />
                                            <br />
                                            <label class="LabelCampo">Data Dal:</label>
                                            <asp:TextBox ID="Txt_DataAl" runat="server" Width="90px"></asp:TextBox>

                                            <br />
                                            <br />

                                            <label class="LabelCampo">Tipo Estrazione:</label>
                                            <br />
                                            </br>
             <asp:CheckBox ID="Chk_SenzaMovimentiChiusura" runat="server" />
                                            Senza Movimenti Chiusura
         <br />
                                            <asp:CheckBox ID="Chk_ImportiDiversiDaZero" runat="server" />
                                            Anche Saldo uguale a Zero<br />
                                            <asp:CheckBox ID="Chk_parenti" runat="server" />
                                            Parenti<br />
                                            <asp:CheckBox ID="Chk_Regioni" runat="server" />
                                            Regioni<br />
                                            <asp:CheckBox ID="Chk_Ospiti" runat="server" />
                                            Ospiti<br />
                                            <asp:CheckBox ID="Chk_Comuni" runat="server" />
                                            Comuni<br />
                                            <asp:CheckBox ID="Chk_RagrCliFor" runat="server" />
                                            Clienti/Fornitori<br />

                                            <br />
                                            <br />



                                            <asp:UpdateProgress ID="UpdateProgress2" runat="server"
                                                AssociatedUpdatePanelID="UpdatePanel1">
                                                <ProgressTemplate>
                                                    <div id="blur">&nbsp;</div>
                                                    <div id="Div1">&nbsp;</div>
                                                    <div id="pippo" class="wait">
                                                        <br />
                                                        <img height="30px" src="images/loading.gif"><br />
                                                    </div>
                                                </ProgressTemplate>
                                            </asp:UpdateProgress>



                                        </ContentTemplate>
                                    </xasp:TabPanel>
                                </xasp:TabContainer>
                            </td>

                        </tr>

                        <tr>
                            <td style="width: 160px; background-color: #F0F0F0; vertical-align: top; text-align: center;" id="BarraLaterale">&nbsp;</td>
                            <td colspan="2" style="background-color: #FFFFFF;" valign="top">&nbsp;</td>
                        </tr>
                    </table>
                </ContentTemplate>
            </asp:UpdatePanel>


        </div>
    </form>
</body>
</html>
