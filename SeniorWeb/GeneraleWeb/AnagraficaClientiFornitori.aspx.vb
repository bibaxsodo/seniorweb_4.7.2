﻿
Partial Class AnagraficaClientiFornitori
    Inherits System.Web.UI.Page
    Dim Tabella As New System.Data.DataTable("tabella")


    Private Sub Pulisci()


        Txt_RagioneSociale.Text = ""

        Txt_Indirizzo.Text = ""
        Txt_Cap.Text = ""
        REM Txt_Prov.Text = ""
        Txt_Comune.Text = ""
        Txt_Telefono.Text = ""
        Txt_Fax.Text = ""
        Txt_CodiceFiscale.Text = ""
        Txt_PartitaIVA.Text = ""


        
        Txt_Mail.Text = ""
        Txt_Cellulare.Text = ""


        Txt_Pec.Text = ""

        Chk_Cliente.Checked = False
        Chk_Fornitore.Checked = False

        RB_NoEsterometro.Checked = True
        RB_SiEsterometro.Checked = False
        RB_NODATIFATTURE.Checked = False

        RB_PersonaFisica.Checked = True        
        RB_PersonaGiuridica.Checked = False
        RB_Altro.Checked = False


        RB_SiP.Checked = False
        Rb_NoP.Checked = True

        RB_SiSB.Checked = False
        RB_NoSB.Checked = True 

        RB_SiEN.Checked = False
        RB_NoEN.Checked = True

        Rb_SISospCli.Checked = False
        Rb_NOSospCli.Checked = True
        Rb_NSSospCli.Checked = False


        DD_DetraiblitaFor.SelectedValue = ""


        Txt_ContoCliente.Text = ""

        
        Txt_ContoRicavo.Text = ""


        RB_SI.Checked = False
        RB_No.Checked = True
        Txt_ContoAllegato.Text = ""

        Txt_DescrizioneCli.Text = ""

        Txt_PostaleCli.Text = ""
        DD_IncassoCli.SelectedValue = ""
        DD_CausaleContabileCli.SelectedValue = ""
        Txt_IntCli.Text = ""
        Txt_NumeroControlloCli.Text = ""
        Txt_CodiceCinCli.Text = ""
        Txt_CodiceABICli.Text = ""
        TxT_CodiceCABCli.Text = ""
        Txt_CCBancarioCli.Text = ""

        Txt_Dichiarazione.Text = ""

        Txt_ContoFornitore.Text = ""

        Txt_ContoCosto.Text = ""

        Txt_ContoAllegatoFornitore.Text = ""

        RB_AllegoFornitoreSI.Checked = False
        RB_AllegoFornitoreNo.Checked = True


        Txt_DescrizioneFornitore.Text = ""

        Txt_PostaleFor.Text = ""
        DD_IncassoFor.SelectedValue = ""
        DD_CausaleContabileFor.SelectedValue = ""
        Txt_IntFor.Text = 0
        Txt_NumeroControlloFor.Text = ""
        Txt_CodiceCinFor.Text = ""
        Txt_CodiceABIFor.Text = ""
        TxT_CodiceCABFor.Text = ""
        Txt_CCBancarioFor.Text = ""

        Txt_CodiceUnivoco.Text = ""
    End Sub
    Protected Sub CaricaCliFor(ByVal CodiceInterno As Long)
        Dim K As New Cls_ClienteFornitore
        Dim Codice As Long


        If CodiceInterno = 0 Then
            Codice = Request.Item("Codice")
        Else
            Codice = CodiceInterno
        End If


        If Val(Codice) = 0 Then
            Exit Sub
        End If



        Dim Log As New Cls_LogPrivacy
        Log.LogPrivacy(Application("SENIOR"), Session("CLIENTE"), Session("UTENTE"), Session("UTENTEIMPER"), Page.GetType.Name.ToUpper, 0, 0, "", "", 0, Txt_RagioneSociale.Text, "V", "CLIFOR", "")


        Txt_Codice.Text = Codice

        K.Nome = ""
        K.CODICEDEBITORECREDITORE = Codice
        K.Leggi(Session("DC_OSPITE"))

        If K.Nome = "" Then
            Exit Sub
        End If
        Txt_RagioneSociale.Text = K.Nome

        Txt_Indirizzo.Text = K.RESIDENZAINDIRIZZO1
        Txt_Cap.Text = K.RESIDENZACAP1
        REM Txt_Prov.Text = K.RESIDENZAPROVINCIA1
        Dim mKomune As New ClsComune
        mKomune.Comune = K.RESIDENZACOMUNE1
        mKomune.Provincia = K.RESIDENZAPROVINCIA1
        mKomune.Leggi(Session("DC_OSPITE"))
        Txt_Comune.Text = K.RESIDENZAPROVINCIA1 & " " & K.RESIDENZACOMUNE1 & " " & mKomune.Descrizione
        Txt_Telefono.Text = K.Telefono1
        Txt_Fax.Text = K.Telefono2
        Txt_CodiceFiscale.Text = K.CodiceFiscale
        Txt_PartitaIVA.Text = K.PARTITAIVA        

        Txt_Mail.Text = K.Mail
        Txt_Cellulare.Text = K.Cellulare

        If K.MastroCliente > 0 Then
            Txt_ContoCliente.Enabled = False
        End If
        If K.MastroFornitore > 0 Then
            Txt_ContoFornitore.Enabled = False
        End If


        Chk_Cliente.Checked = False
        Chk_Fornitore.Checked = False
        If Val(K.MastroCliente) > 0 Then
            Chk_Cliente.Checked = True
        End If
        If Val(K.MastroFornitore) > 0 Then
            Chk_Fornitore.Checked = True
        End If

        RB_No.Checked = False
        RB_Si.Checked = False


        RB_PersonaFisica.Checked = False
        RB_PersonaGiuridica.Checked = False
        RB_Altro.Checked = False
        RB_PubblicaAmministrazione.Checked = False
        Rb_NoP.Checked = False
        RB_SiP.Checked = False

        RB_SI.Checked = False
        RB_No.Checked = False

        RB_SiSB.Checked = False
        RB_NoSB.Checked = False

        RB_SiEN.Checked = False
        RB_NoEN.Checked = False


        If K.Contratto = 0 Then
            RB_NoEsterometro.Checked = True
        End If
        If K.Contratto = 1 Then
            RB_SiEsterometro.Checked = True
        End If
        If K.Contratto = 99 Then
            RB_NODATIFATTURE.Checked = True
        End If

        If K.FisicaGiuridica = "F" Then
            RB_PersonaFisica.Checked = True
        End If
        If K.FisicaGiuridica = "G" Then
            RB_PersonaGiuridica.Checked = True
        End If
        If K.FisicaGiuridica = "A" Then
            RB_Altro.Checked = True
        End If
        If K.FisicaGiuridica = "P" Then
            RB_PubblicaAmministrazione.Checked = True
        End If

        If K.Professionista = "S" Then
            RB_SiP.Checked = True
        End If
        If K.Professionista = "N" Then
            Rb_NoP.Checked = True
        End If

        If K.SoggettoABollo = "S" Then
            RB_SiSB.Checked = True
        End If
        If K.SoggettoABollo = "N" Then
            RB_NoSB.Checked = True
        End If

        If K.Economo = "S" Then
            RB_SiEN.Checked = True
        End If
        If K.Economo = "N" Then
            RB_NoEN.Checked = True
        End If


        Dim DecPC As New Cls_Pianodeiconti

        DecPC.Mastro = K.MastroCliente
        DecPC.Conto = K.ContoCliente
        DecPC.Sottoconto = K.SottoContoCliente
        DecPC.Decodfica(Session("DC_GENERALE"))
        Txt_ContoCliente.Text = K.MastroCliente & " " & K.ContoCliente & " " & K.SottoContoCliente & " " & DecPC.Descrizione

        DecPC.Mastro = K.MastroRicavo
        DecPC.Conto = K.ContoRicavo
        DecPC.Sottoconto = K.SottocontoRicavo
        DecPC.Decodfica(Session("DC_GENERALE"))
        Txt_ContoRicavo.Text = K.MastroRicavo & " " & K.ContoRicavo & " " & K.SottocontoRicavo & " " & DecPC.Descrizione


        DecPC.Mastro = K.MastroClienteAllegato
        DecPC.Conto = K.ContoClienteAllegato
        DecPC.Sottoconto = K.SottocontoClienteAllegato
        DecPC.Decodfica(Session("DC_GENERALE"))
        Txt_ContoAllegato.Text = K.MastroClienteAllegato & " " & K.ContoClienteAllegato & " " & K.SottocontoClienteAllegato & " " & DecPC.Descrizione

        Txt_DescrizioneCli.Text = K.DescrizioneCliente

        Txt_PostaleCli.Text = K.CCPostaleCliente
        DD_IncassoCli.SelectedValue = K.ModalitaPagamento
        DD_CausaleContabileCli.SelectedValue = K.CausaleContabileCliente
        Txt_IntCli.Text = K.IntCliente
        Txt_NumeroControlloCli.Text = K.NumeroControlloCliente
        Txt_CodiceCinCli.Text = K.CinCliente
        Txt_CodiceABICli.Text = K.ABICliente
        TxT_CodiceCABCli.Text = K.CABCliente
        Txt_CCBancarioCli.Text = K.CCBancarioCliente

        Rb_SISospCli.Checked = False
        Rb_NOSospCli.Checked = False
        Rb_NSSospCli.Checked = False
        If K.IVASospesaCli = "S" Then
            Rb_SISospCli.Checked = True
        End If
        If K.IVASospesaCli = "" Then
            Rb_NOSospCli.Checked = True
        End If
        If K.IVASospesaCli = "O" Then
            Rb_NSSospCli.Checked = True
        End If

        RB_No.Checked = False
        RB_SI.Checked = False
        If K.AllegatoCliente = "N" Then
            RB_No.Checked = True
        End If
        If K.AllegatoCliente = "S" Then
            RB_SI.Checked = True
        End If

        Txt_Dichiarazione.Text = K.Dichiarazione

        DecPC.Mastro = K.MastroFornitore
        DecPC.Conto = K.ContoFornitore
        DecPC.Sottoconto = K.SottoContoFornitore
        DecPC.Decodfica(Session("DC_GENERALE"))
        Txt_ContoFornitore.Text = K.MastroFornitore & " " & K.ContoFornitore & " " & K.SottoContoFornitore & " " & DecPC.Descrizione

        DecPC.Mastro = K.MastroCosto
        DecPC.Conto = K.ContoCosto
        DecPC.Sottoconto = K.SottocontoCosto
        DecPC.Decodfica(Session("DC_GENERALE"))
        Txt_ContoCosto.Text = K.MastroCosto & " " & K.ContoCosto & " " & K.SottocontoCosto & " " & DecPC.Descrizione


        DecPC.Mastro = K.MastroFornitoreAllegato
        DecPC.Conto = K.ContoFornitoreAllegato
        DecPC.Sottoconto = K.SottocontoFornitoreAllegato
        DecPC.Decodfica(Session("DC_GENERALE"))
        Txt_ContoAllegatoFornitore.Text = K.MastroFornitoreAllegato & " " & K.ContoFornitoreAllegato & " " & K.SottocontoFornitoreAllegato & " " & DecPC.Descrizione

        RB_AllegoFornitoreNo.Checked = False
        RB_AllegoFornitoreSI.Checked = False
        If K.AllegatoFornitore = "N" Then
            RB_AllegoFornitoreNo.Checked = True
        End If
        If K.AllegatoFornitore = "S" Then
            RB_AllegoFornitoreSI.Checked = True
        End If

        Txt_DescrizioneFornitore.Text = K.DescrizioneFornitore

        Rb_SISospFor.Checked = False
        Rb_NoSospFor.Checked = False
        Rb_NSSospFor.Checked = False
        If K.IVASospesa = "S" Then
            Rb_SISospFor.Checked = True
        End If
        If K.IVASospesa = "" Then
            Rb_NoSospFor.Checked = True
        End If
        If K.IVASospesa = "O" Then
            Rb_NSSospFor.Checked = True
        End If

        Txt_PostaleFor.Text = K.CCPostaleFornitore
        Try
            DD_IncassoFor.SelectedValue = K.ModalitaPagamentoFornitore
            DD_CausaleContabileFor.SelectedValue = K.CausaleContabileFornitore
        Catch ex As Exception

        End Try

        Txt_IntFor.Text = K.IntFornitore
        Txt_NumeroControlloFor.Text = K.NumeroControlloFornitore
        Txt_CodiceCinFor.Text = K.CinFornitore
        Txt_CodiceABIFor.Text = K.ABIFornitore
        TxT_CodiceCABFor.Text = K.CABFornitore
        Txt_CCBancarioFor.Text = K.CCBancarioFornitore


        DD_DetraiblitaFor.SelectedValue = K.DetrabilitaFornitore

        Txt_Pec.Text = K.PEC
        Txt_CodiceUnivoco.Text = K.CodiceDestinatario

        If K.EGo = "" Or K.EGo = "N" Then
            DD_EGO.SelectedValue = "N"
        Else
            DD_EGO.SelectedValue = K.EGo
        End If


        Txt_NumItem.Text = K.NumItem

        Txt_CodiceCommessaConvezione.Text = K.CodiceCommessaConvezione


        Txt_NoteFe.Text = K.Note

        Txt_Causale.Text = K.Causale

        Txt_IdDocumento.Text = K.IdDocumento
        Txt_RiferimentoAmministrazione.Text = K.RiferimentoAmministrazione
        If K.NumeroFatturaDDT = 1 Then
            Chk_NFDtt.Checked = True
        Else
            Chk_NFDtt.Checked = False
        End If
        Txt_IdDocumentoData.Text = K.IdDocumentoData


        Dim CigFor As   New Cls_CigClientiFornitori

        CigFor.CodiceDebitoreCreditore = Txt_Codice.Text
        CigFor.loaddati(Session("DC_GENERALE"),Tabella)

        Grd_Cig.DataSource = Tabella
        Grd_Cig.AutoGenerateColumns = False
        Grd_Cig.DataBind

        Session("TABELLA") =Tabella
    End Sub


    Private Function SplitWords(ByVal s As String) As String()
        '
        ' Call Regex.Split function from the imported namespace.
        ' Return the result array.
        '
        Return Regex.Split(s, "\W+")
    End Function

    Protected Sub Btn_Modifica_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles Btn_Modifica.Click


        If Txt_RagioneSociale.Text.Trim = "" Then
            ClientScript.RegisterClientScriptBlock(Me.GetType(), "Errore", "VisualizzaErrore('Deve specificare la ragione sociale');", True)
            REM Lbl_Errori.Text = "Deve specificare la ragione sociale"            
            Exit Sub
        End If

        Dim AnaG As New Cls_ClienteFornitore

        AnaG.CODICEDEBITORECREDITORE = 0

        'If Txt_CodiceFiscale.Text <> "" Then

        '    AnaG.CodiceFiscale = Txt_CodiceFiscale.Text
        '    AnaG.CercaCF(Session("DC_OSPITE"))

        '    If AnaG.CODICEDEBITORECREDITORE > 0 And AnaG.CODICEDEBITORECREDITORE <> Val(Txt_Codice.Text) Then
        '        ClientScript.RegisterClientScriptBlock(Me.GetType(), "Errore", "VisualizzaErrore('Codice Fiscale già usato per " & AnaG.Nome & "');", True)
        '        Exit Sub
        '    End If
        'End If

        'If Val(Txt_PartitaIVA.Text) > 0 Then
        '    AnaG.PARTITAIVA = Val(Txt_PartitaIVA.Text)            
        '    AnaG.CercaCF(Session("DC_OSPITE"))

        '    If AnaG.CODICEDEBITORECREDITORE > 0 And AnaG.CODICEDEBITORECREDITORE <> Val(Txt_Codice.Text) Then
        '        ClientScript.RegisterClientScriptBlock(Me.GetType(), "Errore", "VisualizzaErrore('P.IVA già usato per " & AnaG.Nome & "');", True)
        '        Exit Sub
        '    End If
        'End If



        If Val(Txt_ContoCliente.Text) = 0 And Val(Txt_ContoFornitore.Text) = 0 Then
            ClientScript.RegisterClientScriptBlock(Me.GetType(), "Errore", "VisualizzaErrore('Deve specificare indicare almeno il conto cliente o fornitore');", True)
            REM Lbl_Errori.Text = "Deve specificare la ragione sociale"            
            Exit Sub
        End If


        For i = 0 To Grd_Cig.Rows.Count - 1

            Dim TxtDataInizio As TextBox = DirectCast(Grd_Cig.Rows(i).FindControl("TxtDataInizio"), TextBox)
            Dim TxtDataFine As TextBox = DirectCast(Grd_Cig.Rows(i).FindControl("TxtDataFine"), TextBox)
            Dim TxtCig As TextBox = DirectCast(Grd_Cig.Rows(i).FindControl("TxtCig"), TextBox)
            Dim TxtImporto As TextBox = DirectCast(Grd_Cig.Rows(i).FindControl("TxtImporto"), TextBox)

            If TxtImporto.Text <>"" Then

                If TxtDataInizio.Text = "" Then
                    ClientScript.RegisterClientScriptBlock(Me.GetType(), "Errore", "VisualizzaErrore('Devi indicare data inzio CIG');", True)
                    Exit Sub
                End If


                If TxtDataFine.Text = "" Then
                    ClientScript.RegisterClientScriptBlock(Me.GetType(), "Errore", "VisualizzaErrore('Devi indicare data fine CIG');", True)
                    Exit Sub
                End If


                If Not IsDate(TxtDataInizio.Text) Then
                    ClientScript.RegisterClientScriptBlock(Me.GetType(), "Errore", "VisualizzaErrore('Devi indicare data inzio CIG');", True)
                    Exit Sub
                End If


                If Not IsDate(TxtDataFine.Text) Then
                    ClientScript.RegisterClientScriptBlock(Me.GetType(), "Errore", "VisualizzaErrore('Devi indicare data fine CIG');", True)
                    Exit Sub
                End If

                Dim DataInizio As Date
                Dim DataFine As Date
                DataInizio = TxtDataInizio.Text
                DataFine = TxtDataFine.Text

                If Format(DataInizio, "yyyyMMdd") > Format(DataFine, "yyyyMMdd") Then
                    ClientScript.RegisterClientScriptBlock(Me.GetType(), "Errore", "VisualizzaErrore('Data inizio non può essere maggionre di data fine');", True)
                    Exit Sub
                End If

            End IF
        Next



        If Request.Item("CODICE") = "" Then
            Dim Log As New Cls_LogPrivacy
            Log.LogPrivacy(Application("SENIOR"), Session("CLIENTE"), Session("UTENTE"), Session("UTENTEIMPER"), Page.GetType.Name.ToUpper, 0, 0, "", "", 0, Txt_RagioneSociale.Text, "I", "CLIFOR", "")
        Else

            Dim Log As New Cls_LogPrivacy
            Dim ConvT As New Cls_DataTableToJson
            Dim OldTable As New System.Data.DataTable("tabellaOld")


            Dim OldDatiPar As New Cls_ClienteFornitore

            OldDatiPar.CODICEDEBITORECREDITORE = Val(Txt_Codice.Text)
            OldDatiPar.Leggi(Session("DC_OSPITE"))
            Dim AppoggioJS As String


            AppoggioJS = ConvT.SerializeObject(OldDatiPar)

            Log.LogPrivacy(Application("SENIOR"), Session("CLIENTE"), Session("UTENTE"), Session("UTENTEIMPER"), Page.GetType.Name.ToUpper, Session("CODICEOSPITE"), 0, "", "", 0, Txt_RagioneSociale.Text, "M", "CLIFOR", AppoggioJS)

        End If
        Dim K As New Cls_ClienteFornitore
        Dim Codice As Long
        Dim Vettore(100) As String


        Codice = Val(Txt_Codice.Text)


        K.CODICEDEBITORECREDITORE = Codice
        K.Leggi(Session("DC_OSPITE"))

        K.Nome = Txt_RagioneSociale.Text

        K.RESIDENZAINDIRIZZO1 = Txt_Indirizzo.Text
        K.RESIDENZACAP1 = Txt_Cap.Text

        Vettore = SplitWords(Txt_Comune.Text)

        K.RESIDENZAPROVINCIA1 = ""
        K.RESIDENZACOMUNE1 = ""
        If Vettore.Length >= 2 Then
            K.RESIDENZAPROVINCIA1 = Vettore(0)
            K.RESIDENZACOMUNE1 = Vettore(1)
        End If


        
        K.Mail =Txt_Mail.Text
        K.Cellulare =Txt_Cellulare.Text



        K.Telefono1 = Txt_Telefono.Text
        K.Telefono2 = Txt_Fax.Text
        K.CodiceFiscale = Txt_CodiceFiscale.Text

        K.PARTITAIVA = Val(Txt_PartitaIVA.Text)

        If RB_SiEsterometro.Checked = True Then
            K.Contratto = 1
        End If
        If RB_NoEsterometro.Checked = True Then
            K.Contratto = 0
        End If
        If RB_NODATIFATTURE.Checked = True Then
            K.Contratto = 99
        End If
        

        If RB_PersonaFisica.Checked = True Then
            K.FisicaGiuridica = "F"
        End If
        If RB_PersonaGiuridica.Checked = True Then
            K.FisicaGiuridica = "G"
        End If
        If RB_Altro.Checked = True Then
            K.FisicaGiuridica = "A"
        End If
        If RB_PubblicaAmministrazione.Checked = True Then
            K.FisicaGiuridica = "P"
        End If

        If RB_SiP.Checked = True Then
            K.Professionista = "S"
        End If
        If Rb_NoP.Checked = True Then
            K.Professionista = "N"
        End If

        If RB_SiSB.Checked = True Then
            K.SoggettoABollo = "S"
        End If
        If RB_NoSB.Checked = True Then
            K.SoggettoABollo = "N"
        End If

        If RB_SiEN.Checked = True Then
            K.Economo = "S"
        End If
        If RB_NoEN.Checked = True Then
            K.Economo = "N"
        End If

        Dim Mastro As Long
        Dim Conto As Long
        Dim Sottoconto As Long

        Vettore = SplitWords(Txt_ContoCliente.Text)

        If Vettore.Length >= 2 Then
            Mastro = Val(Vettore(0))
            Conto = Val(Vettore(1))
            Sottoconto = Val(Vettore(2))
        Else
            Mastro = 0
            Conto = 0
            Sottoconto = 0
        End If




        Dim DecPC As New Cls_Pianodeiconti

        K.MastroCliente = Mastro
        K.ContoCliente = Conto
        K.SottoContoCliente = Sottoconto


        Vettore = SplitWords(Txt_ContoRicavo.Text)

        If Vettore.Length >= 2 Then
            Mastro = Val(Vettore(0))
            Conto = Val(Vettore(1))
            Sottoconto = Val(Vettore(2))
        Else
            Mastro = 0
            Conto = 0
            Sottoconto = 0
        End If


        K.MastroRicavo = Mastro
        K.ContoRicavo = Conto
        K.SottocontoRicavo = Sottoconto



        Vettore = SplitWords(Txt_ContoAllegato.Text)

        If Vettore.Length >= 2 Then
            Mastro = Val(Vettore(0))
            Conto = Val(Vettore(1))
            Sottoconto = Val(Vettore(2))
        Else
            Mastro = 0
            Conto = 0
            Sottoconto = 0
        End If


        K.MastroClienteAllegato = Mastro
        K.ContoClienteAllegato = Conto
        K.SottocontoClienteAllegato = Sottoconto

        K.DescrizioneCliente = Txt_DescrizioneCli.Text
        K.CCPostaleCliente = Txt_PostaleCli.Text
        K.ModalitaPagamento = DD_IncassoCli.SelectedValue
        K.CausaleContabileCliente = DD_CausaleContabileCli.SelectedValue
        K.IntCliente = Txt_IntCli.Text
        K.NumeroControlloCliente = Val(Txt_NumeroControlloCli.Text)
        K.CinCliente = Txt_CodiceCinCli.Text
        K.ABICliente = Val(Txt_CodiceABICli.Text)
        K.CABCliente = Val(TxT_CodiceCABCli.Text)

        K.CCBancarioCliente = Txt_CCBancarioCli.Text

        If RB_No.Checked = True Then
            K.AllegatoCliente = "N"
        End If
        If RB_SI.Checked = True Then
            K.AllegatoCliente = "S"
        End If


        If Rb_SISospCli.Checked = True Then
            K.IVASospesaCli = "S"
        End If
        If Rb_NOSospCli.Checked = True Then
            K.IVASospesaCli = ""
        End If
        If Rb_NSSospCli.Checked = True Then
            K.IVASospesaCli = "O"
        End If

        K.Dichiarazione = Txt_Dichiarazione.Text



        Vettore = SplitWords(Txt_ContoFornitore.Text)

        If Vettore.Length >= 2 Then
            Mastro = Val(Vettore(0))
            Conto = Val(Vettore(1))
            Sottoconto = Val(Vettore(2))
        Else
            Mastro = 0
            Conto = 0
            Sottoconto = 0
        End If

        K.MastroFornitore = Mastro
        K.ContoFornitore = Conto
        K.SottoContoFornitore = Sottoconto



        Vettore = SplitWords(Txt_ContoCosto.Text)

        If Vettore.Length >= 2 Then
            Mastro = Val(Vettore(0))
            Conto = Val(Vettore(1))
            Sottoconto = Val(Vettore(2))
        Else
            Mastro = 0
            Conto = 0
            Sottoconto = 0
        End If


        K.MastroCosto = Mastro
        K.ContoCosto = Conto
        K.SottocontoCosto = Sottoconto

        Vettore = SplitWords(Txt_ContoAllegatoFornitore.Text)

        If Vettore.Length >= 2 Then
            Mastro = Val(Vettore(0))
            Conto = Val(Vettore(1))
            Sottoconto = Val(Vettore(2))
        Else
            Mastro = 0
            Conto = 0
            Sottoconto = 0
        End If


        K.MastroFornitoreAllegato = Mastro
        K.ContoFornitoreAllegato = Conto
        K.SottocontoFornitoreAllegato = Sottoconto

        If RB_AllegoFornitoreNo.Checked = True Then
            K.AllegatoFornitore = "N"
        End If
        If RB_AllegoFornitoreSI.Checked = True Then
            K.AllegatoFornitore = "S"
        End If

        If Rb_SISospFor.Checked = True Then
            K.IVASospesa = "S"
        End If
        If Rb_NoSospFor.Checked = True Then
            K.IVASospesa = ""
        End If
        If Rb_NSSospFor.Checked = True Then
            K.IVASospesa = "O"
        End If

        K.DescrizioneFornitore = Txt_DescrizioneFornitore.Text
        K.CCPostaleFornitore = Txt_PostaleFor.Text
        K.ModalitaPagamentoFornitore = DD_IncassoFor.SelectedValue
        K.CausaleContabileFornitore = DD_CausaleContabileFor.SelectedValue
        K.IntFornitore = Txt_IntFor.Text
        K.NumeroControlloFornitore = Txt_NumeroControlloFor.Text
        K.CinFornitore = Txt_CodiceCinFor.Text
        K.ABIFornitore = Val(Txt_CodiceABIFor.Text)
        K.CABFornitore = Val(TxT_CodiceCABFor.Text)
        K.CCBancarioFornitore = Txt_CCBancarioFor.Text

        K.DetrabilitaFornitore = DD_DetraiblitaFor.SelectedValue

        K.PEC = Txt_Pec.Text
        K.CodiceDestinatario = Txt_CodiceUnivoco.Text


        K.NumItem = Txt_NumItem.Text

        K.EGO = DD_EGO.SelectedValue


        K.CodiceCommessaConvezione = Txt_CodiceCommessaConvezione.Text


        K.Note = Txt_NoteFe.Text

        K.CausalE = Txt_Causale.Text

        K.IdDocumento = Txt_IdDocumento.Text
        K.RiferimentoAmministrazione = Txt_RiferimentoAmministrazione.Text
        If Chk_NFDtt.Checked = True Then
            K.NumeroFatturaDDT = 1
        Else
            K.NumeroFatturaDDT = 0
        End If
        K.IdDocumentoData = Txt_IdDocumentoData.Text


        K.Utente = Session("UTENTE")
        K.Scrivi(Session("DC_OSPITE"), Session("DC_GENERALE"))


        Dim Cig As New Cls_CigClientiFornitori


        UpDateTable()
        Tabella = Session("TABELLA")

        Cig.CodiceDebitoreCreditore = val(Txt_Codice.Text)
        Cig.Utente = Session("UTENTE")
        Cig.AggiornaDaTabella(Session("DC_GENERALE"),Tabella)


        If Request.Item("PAGINA") = "" Then
            Response.Redirect("ElencoClientiFornitori.aspx")
        Else
            Response.Redirect("ElencoClientiFornitori.aspx?CHIAMATA=RITORNO&PAGINA=" & Request.Item("PAGINA"))
        End If
    End Sub

    Private Sub EseguiJS()
        Dim MyJs As String

        MyJs = " $(" & Chr(34) & "#" & Txt_Comune.ClientID & Chr(34) & ").autocomplete(""/WebHandler/AutocompleteComune.ashx?Utente=" & Session("UTENTE") & """, {delay:5,minChars:4}); "
        MyJs = MyJs & " $(" & Chr(34) & "#" & Txt_ContoFornitore.ClientID & Chr(34) & ").autocomplete(""GestioneConto.ashx?Utente=" & Session("UTENTE") & """, {delay:5,minChars:4}); "
        MyJs = MyJs & " $(" & Chr(34) & "#" & Txt_ContoCosto.ClientID & Chr(34) & ").autocomplete(""GestioneConto.ashx?Utente=" & Session("UTENTE") & """, {delay:5,minChars:4}); "
        MyJs = MyJs & " $(" & Chr(34) & "#" & Txt_ContoCliente.ClientID & Chr(34) & ").autocomplete(""GestioneConto.ashx?Utente=" & Session("UTENTE") & """, {delay:5,minChars:4}); "
        MyJs = MyJs & " $(" & Chr(34) & "#" & Txt_ContoRicavo.ClientID & Chr(34) & ").autocomplete(""GestioneConto.ashx?Utente=" & Session("UTENTE") & """, {delay:5,minChars:4}); "
        MyJs = MyJs & " $(" & Chr(34) & "#" & Txt_ContoAllegato.ClientID & Chr(34) & ").autocomplete(""GestioneConto.ashx?Utente=" & Session("UTENTE") & """, {delay:5,minChars:4}); "
        MyJs = MyJs & " $(" & Chr(34) & "#" & Txt_ContoAllegatoFornitore.ClientID & Chr(34) & ").autocomplete(""GestioneConto.ashx?Utente=" & Session("UTENTE") & """, {delay:5,minChars:4}); "

        ScriptManager.RegisterStartupScript(Me, Me.GetType(), "InizializzaCampi", MyJs, True)


        MyJs = "$(document).ready(function()  { "
        MyJs = MyJs & "var els = document.getElementsByTagName(""*"");"

        MyJs = MyJs & "for (var i=0;i<els.length;i++)"
        MyJs = MyJs & "if ( els[i].id ) { "
        MyJs = MyJs & " var appoggio =els[i].id; "


        MyJs = MyJs & " if ((appoggio.match('TxtImporto')!= null)) {  "
        MyJs = MyJs & " $(els[i]).keypress(function() { ForceNumericInput($(this).val(), true, true); } ); "
        MyJs = MyJs & " $(els[i]).blur(function() { var ap = formatNumber($(this).val(),2); $(this).val(ap); });"
        MyJs = MyJs & "    }"

        MyJs = MyJs & " if (appoggio.match('TxtData')!= null) {    "
        MyJs = MyJs & " $(els[i]).mask(""99/99/9999"");"
        MyJs = MyJs & " $(els[i]).datepicker({ changeMonth: true,changeYear: true,  yearRange: ""1990:" & Year(Now) + 5 & """},$.datepicker.regional[""it""]);"
        MyJs = MyJs & "    }"

        MyJs = MyJs & "} "


        MyJs = MyJs & "});"

        ScriptManager.RegisterStartupScript(Me, Me.GetType(), "EseguiJS", MyJs, True)
    End Sub

     Private Sub InserisciRiga()
        UpDateTable()
        Tabella = Session("Tabella")
        Dim myriga As System.Data.DataRow = Tabella.NewRow()
        myriga(0) = ""
        myriga(1) = ""
        myriga(2) = ""
        myriga(3) = 0
        myriga(4) = ""
        Tabella.Rows.Add(myriga)

        Session("Tabella") = Tabella
        Grd_Cig.AutoGenerateColumns = False
        Grd_Cig.DataSource = Tabella
        Grd_Cig.DataBind()

        Call EseguiJS()
    End Sub

    Private Sub UpDateTable()

        

        Tabella.Clear()
        Tabella.Columns.Clear
        Tabella.Columns.Add("Data Inizio", GetType(String))
        Tabella.Columns.Add("Data Fine", GetType(String))
        Tabella.Columns.Add("Cig", GetType(String))
        Tabella.Columns.Add("ImportoBudget", GetType(String))
        Tabella.Columns.Add("Descrizione", GetType(String))
        For i = 0 To Grd_Cig.Rows.Count - 1

            Dim TxtDataInizio As TextBox = DirectCast(Grd_Cig.Rows(i).FindControl("TxtDataInizio"), TextBox)
            Dim TxtDataFine As TextBox = DirectCast(Grd_Cig.Rows(i).FindControl("TxtDataFine"), TextBox)
            Dim TxtCig As TextBox = DirectCast(Grd_Cig.Rows(i).FindControl("TxtCig"), TextBox)
            Dim TxtImporto As TextBox = DirectCast(Grd_Cig.Rows(i).FindControl("TxtImporto"), TextBox)
            Dim TxtDescrizione As TextBox = DirectCast(Grd_Cig.Rows(i).FindControl("TxtDescrizione"), TextBox)

            Dim myrigaR As System.Data.DataRow = Tabella.NewRow()

            myrigaR(0) = TxtDataInizio.Text
            myrigaR(1) = TxtDataFine.Text
            myrigaR(2) = TxtCig.Text
            If TxtImporto.Text.Trim ="" Then
                TxtImporto.Text ="0"
            End If

            myrigaR(3) = TxtImporto.Text
            myrigaR(4) = TxtDescrizione.Text

            
            Tabella.Rows.Add(myrigaR)

        Next

        Session("Tabella") = Tabella
    End Sub

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Trim(Session("UTENTE")) = "" Then
            If Request.ServerVariables("URL").ToUpper.IndexOf("SENIORWEBBLANCO") > 0 Then
                Response.Redirect("/ODV/Login.aspx")
                Exit Sub
            Else
                Response.Redirect("/SeniorWeb/Login.aspx")
                Exit Sub
            End If
        End If

        
        call EseguiJS()


        If Trim(Session("UTENTE")) = "" Then
            Response.Redirect("../Login_generale.aspx")
            Exit Sub
        End If



        If Session("TIPOAPP") <> "RSA" Then
            LBL_Economo.Visible = False
     
            RB_SiEN.Visible = False
            RB_NoEN.Visible = False
        End If

        If Page.IsPostBack = True Then Exit Sub



        Dim KModaPag As New Cls_TipoPagamento

        KModaPag.UpDateDropBox(Session("DC_TABELLE"), DD_IncassoFor)

        KModaPag.UpDateDropBox(Session("DC_TABELLE"), DD_IncassoCli)

        Txt_Codice.Enabled = False

        Dim K1 As New Cls_SqlString

        Dim Barra As New Cls_BarraSenior

        If Not IsNothing(Session("RicercaGeneraleSQLString")) Then
            K1 = Session("RicercaGeneraleSQLString")
        End If

        Lbl_BarraSenior.Text = Barra.CodiceBarra(Application("SENIOR"), Session("UTENTE"), Page, K1)

        Dim K As New Cls_CausaleContabile




        K.UpDateDropBox(Session("DC_TABELLE"), DD_CausaleContabileFor)

        K.UpDateDropBox(Session("DC_TABELLE"), DD_CausaleContabileCli)


        Dim M As New ClsDetraibilita

        M.UpDateDropBox(Session("DC_TABELLE"), DD_DetraiblitaFor)



        If Val(Request.Item("Codice")) > 0 Then
            Call CaricaCliFor(0)
            Txt_Codice.Enabled = False
        End If

        'If CHIAMANTE=DOCUMENTI

        If Request.Item("CHIAMANTE") = "IMPXML" Then
            Txt_RagioneSociale.Text = Request.Item("RAGIONESOCIALE")
            Txt_Indirizzo.Text = Request.Item("INDIRIZZO").ToString.Replace("%20", " ")
            Txt_Cap.Text = Request.Item("CAP")

            Dim MC As New ClsComune

            MC.Descrizione = Request.Item("COMUNE")
            MC.LeggiDescrizione(Session("DC_OSPITE"))

            Txt_Comune.Text = MC.Provincia & " " & MC.Comune & " " & Request.Item("COMUNE")

            Txt_CodiceFiscale.Text = Request.Item("CODICEFISCALE")
            Txt_PartitaIVA.Text = Request.Item("PARTITAIVA")

        End If
        If Request.Item("CHIAMANTE") = "DOCUMENTI" Then
            If Val(Session("NumeroRegistrazione")) > 0 Then
                Dim ML As New Cls_MovimentoContabile

                ML.NumeroRegistrazione = Val(Session("NumeroRegistrazione"))
                ML.Leggi(Session("DC_GENERALE"), ML.NumeroRegistrazione)


                Dim CausaleCont As New Cls_CausaleContabile

                CausaleCont.Codice = ML.CausaleContabile
                CausaleCont.Leggi(Session("Dc_TABELLE"), CausaleCont.Codice)

                Dim PianoConti As New Cls_Pianodeiconti

                PianoConti.Mastro = ML.Righe(0).MastroPartita
                PianoConti.Conto = ML.Righe(0).ContoPartita
                PianoConti.Sottoconto = ML.Righe(0).SottocontoPartita
                PianoConti.Decodfica(Session("DC_GENERALE"))


                Dim CliFor As New Cls_ClienteFornitore

                If PianoConti.TipoAnagrafica = "D" Then
                    If CausaleCont.VenditaAcquisti = "V" Then
                        CliFor.MastroCliente = PianoConti.Mastro
                        CliFor.ContoCliente = PianoConti.Conto
                        CliFor.SottoContoCliente = PianoConti.Sottoconto
                        CliFor.Leggi(Session("DC_OSPITE"))

                        Call CaricaCliFor(CliFor.CODICEDEBITORECREDITORE)
                    Else
                        CliFor.MastroFornitore = PianoConti.Mastro
                        CliFor.ContoFornitore = PianoConti.Conto
                        CliFor.SottoContoFornitore = PianoConti.Sottoconto
                        CliFor.Leggi(Session("DC_OSPITE"))

                        Call CaricaCliFor(CliFor.CODICEDEBITORECREDITORE)
                    End If
                    Txt_Codice.Enabled = False
                End If
            End If
        End If

        
        call EseguiJS()

    End Sub

    

    Protected Sub ImageButton1_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles ImageButton1.Click
        Dim K As New Cls_ClienteFornitore
        Dim Codice As Long
        Dim Vettore(100) As String
    
        Codice = Val(Txt_Codice.Text)


        K.CODICEDEBITORECREDITORE = Codice
        K.Leggi(Session("DC_OSPITE"))

        Dim xp As New Cls_Pianodeiconti

        xp.Mastro = K.MastroCliente
        xp.Conto = K.ContoCliente
        xp.Sottoconto = K.SottoContoCliente
        xp.Decodfica(Session("DC_GENERALE"))

        If xp.Descrizione <> "" Then
            ClientScript.RegisterClientScriptBlock(Me.GetType(), "Errore", "VisualizzaErrore('Conto cliente presente nel piano dei conti non posso eliminare');", True)
            REM Lbl_Errori.Text = "Conto cliente presente nel piano dei conti non posso eliminare"
        End If

        Dim xp1 As New Cls_Pianodeiconti

        xp1.Mastro = K.MastroFornitore
        xp1.Conto = K.ContoFornitore
        xp1.Sottoconto = K.SottoContoFornitore
        xp1.Decodfica(Session("DC_GENERALE"))

        If xp1.Descrizione <> "" Then
            ClientScript.RegisterClientScriptBlock(Me.GetType(), "Errore", "VisualizzaErrore('Conto presente presente nel piano dei conti non posso eliminare');", True)
            REM Lbl_Errori.Text = "Conto presente presente nel piano dei conti non posso eliminare"
            Exit Sub
        End If



        Dim Log As New Cls_LogPrivacy
        Dim ConvT As New Cls_DataTableToJson
        Dim OldTable As New System.Data.DataTable("tabellaOld")


        Dim OldDatiPar As New Cls_ClienteFornitore

        OldDatiPar.CODICEDEBITORECREDITORE = Val(Txt_Codice.Text)
        OldDatiPar.Leggi(Session("DC_OSPITE"))
        Dim AppoggioJS As String


        AppoggioJS = ConvT.SerializeObject(OldDatiPar)

        Log.LogPrivacy(Application("SENIOR"), Session("CLIENTE"), Session("UTENTE"), Session("UTENTEIMPER"), Page.GetType.Name.ToUpper, Session("CODICEOSPITE"), 0, "", "", 0, Txt_RagioneSociale.Text, "D", "CLIFOR", AppoggioJS)



        K.Cancella(Session("DC_OSPITE"))

        If Request.Item("PAGINA") = "" Then
            Response.Redirect("ElencoClientiFornitori.aspx")
        Else
            Response.Redirect("ElencoClientiFornitori.aspx?CHIAMATA=RITORNO&PAGINA=" & Request.Item("PAGINA"))
        End If
    End Sub

    Protected Sub Btn_Esci_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles Btn_Esci.Click

        If Request.Item("PAGINA") = "" Then
            Response.Redirect("ElencoClientiFornitori.aspx")
        Else
            Response.Redirect("ElencoClientiFornitori.aspx?CHIAMATA=RITORNO&PAGINA=" & Request.Item("PAGINA"))
        End If
        
    End Sub

    


    Protected Sub Btn_DecodificaCap_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles Btn_DecodificaCap.Click

        Dim Xs As New ClsComune
        Dim Vettore(100) As String

        Vettore = SplitWords(Txt_Comune.Text)

        Xs.Provincia = ""
        Xs.Comune = ""
        If Vettore.Length >= 2 Then
            Xs.Provincia = Vettore(0)
            Xs.Comune = Vettore(1)
            Xs.Leggi(Session("DC_OSPITE"))
            Txt_Cap.Text = Xs.RESIDENZACAP1
        End If
    End Sub


    Protected Sub Chk_Cliente_CheckedChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles Chk_Cliente.CheckedChanged
        If Chk_Cliente.Checked = True Then
            Txt_ContoCliente.Enabled = True
            Txt_ContoCliente.BackColor = Drawing.Color.White
            Txt_ContoRicavo.Enabled = True
            Txt_ContoRicavo.BackColor = Drawing.Color.White

            Dim k As New Cls_DatiGenerali

            k.LeggiDati(Session("DC_TABELLE"))

            Dim PianoConti As New Cls_Pianodeiconti

            PianoConti.Mastro = k.ClientiMastro
            PianoConti.Conto = k.ClientiConto
            PianoConti.Sottoconto = 0
            PianoConti.Decodfica(Session("DC_GENERALE"))

            Txt_ContoCliente.Text = PianoConti.Mastro & " " & PianoConti.Conto & " " & PianoConti.Sottoconto & " " & PianoConti.Descrizione
        Else
            Dim Vettore(100) As String
            Dim Mastro As Long
            Dim Conto As Long
            Dim Sottoconto As Long
            Vettore = SplitWords(Txt_ContoCliente.Text)

            If Vettore.Length >= 2 Then
                Mastro = Val(Vettore(0))
                Conto = Val(Vettore(1))
                Sottoconto = Val(Vettore(2))
            Else
                Mastro = 0
                Conto = 0
                Sottoconto = 0
            End If


            Dim PianoConti As New Cls_Pianodeiconti

            PianoConti.Descrizione = ""
            PianoConti.Mastro = Mastro
            PianoConti.Conto = Conto
            PianoConti.Sottoconto = Sottoconto
            PianoConti.Decodfica(Session("DC_GENERALE"))
            If PianoConti.Descrizione <> "" Then
                ClientScript.RegisterClientScriptBlock(Me.GetType(), "Errore", "VisualizzaErrore('Conto presente presente nel piano dei conti non posso eliminare');", True)
                Chk_Cliente.Checked = True
                Exit Sub
            End If


            Txt_ContoCliente.Text = ""
            Txt_ContoRicavo.Text = ""
            Txt_ContoCliente.Enabled = False
            Txt_ContoCliente.BackColor = Drawing.Color.LightGray
            Txt_ContoRicavo.Enabled = False
            Txt_ContoRicavo.BackColor = Drawing.Color.LightGray
        End If
    End Sub

    Protected Sub Chk_Fornitore_CheckedChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles Chk_Fornitore.CheckedChanged
        If Chk_Fornitore.Checked = True Then
            Txt_ContoFornitore.Enabled = True
            Txt_ContoFornitore.BackColor = Drawing.Color.White
            Txt_ContoCosto.Enabled = True
            Txt_ContoCosto.BackColor = Drawing.Color.White


            Dim k As New Cls_DatiGenerali

            k.LeggiDati(Session("DC_TABELLE"))

            Dim PianoConti As New Cls_Pianodeiconti

            PianoConti.Mastro = k.FornitoriMastro
            PianoConti.Conto = k.FornitoriConto
            PianoConti.Sottoconto = 0
            PianoConti.Decodfica(Session("DC_GENERALE"))

            Txt_ContoFornitore.Text = PianoConti.Mastro & " " & PianoConti.Conto & " " & PianoConti.Sottoconto & " " & PianoConti.Descrizione
        Else
            Dim Vettore(100) As String
            Dim Mastro As Long
            Dim Conto As Long
            Dim Sottoconto As Long
            Vettore = SplitWords(Txt_ContoFornitore.Text)

            If Vettore.Length >= 2 Then
                Mastro = Val(Vettore(0))
                Conto = Val(Vettore(1))
                Sottoconto = Val(Vettore(2))
            Else
                Mastro = 0
                Conto = 0
                Sottoconto = 0
            End If


            Dim PianoConti As New Cls_Pianodeiconti

            PianoConti.Descrizione = ""
            PianoConti.Mastro = Mastro
            PianoConti.Conto = Conto
            PianoConti.Sottoconto = Sottoconto
            PianoConti.Decodfica(Session("DC_GENERALE"))
            If PianoConti.Descrizione <> "" Then
                ClientScript.RegisterClientScriptBlock(Me.GetType(), "Errore", "VisualizzaErrore('Conto presente presente nel piano dei conti non posso eliminare');", True)
                Chk_Fornitore.Checked = True
                Exit Sub
            End If

            Txt_ContoFornitore.Text = ""
            Txt_ContoCosto.Text = ""
            Txt_ContoFornitore.Enabled = False
            Txt_ContoFornitore.BackColor = Drawing.Color.LightGray
            Txt_ContoCosto.Enabled = False
            Txt_ContoCosto.BackColor = Drawing.Color.LightGray
        End If

    End Sub

    Protected Sub Txt_CodiceFiscale_TextChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles Txt_CodiceFiscale.TextChanged
        '~/images/Blanco.png
        Img_VerificaCF.ImageUrl = "~/images/Blanco.png"
        If Txt_CodiceFiscale.Text.Trim <> "" Then
            Img_VerificaCF.ImageUrl = "~/images/corretto.gif"

            Dim x As New Cls_CodiceFiscale


            If Txt_CodiceFiscale.Text.Length = 11 Then
                If Not x.CheckPartitaIva(Txt_CodiceFiscale.Text) Then
                    Img_VerificaCF.ImageUrl = "~/images/errore.gif"
                End If
            Else
                If Not x.Check_CodiceFiscale(Txt_CodiceFiscale.Text) Then
                    Img_VerificaCF.ImageUrl = "~/images/errore.gif"
                End If
            End If
        End If


        If Txt_CodiceFiscale.Text <> "" And Val(Txt_Codice.Text) = 0 Then
            Dim Anag As New Cls_ClienteFornitore
            Anag.CodiceFiscale = Txt_CodiceFiscale.Text
            Anag.CercaCF(Session("DC_OSPITE"))

            If Anag.CODICEDEBITORECREDITORE > 0 And Anag.CODICEDEBITORECREDITORE <> Val(Txt_Codice.Text) Then
                ClientScript.RegisterClientScriptBlock(Me.GetType(), "Errore", "VisualizzaErrore('Codice Fiscale già usato per " & Anag.Nome & "');", True)
                Exit Sub
            End If
        End If


    End Sub

    Protected Sub Txt_PartitaIVA_TextChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles Txt_PartitaIVA.TextChanged
        Img_VerificaPIVA.ImageUrl = "~/images/Blanco.png"
        If Txt_PartitaIVA.Text.Trim <> "" Then
            Img_VerificaPIVA.ImageUrl = "~/images/corretto.gif"

            Dim x As New Cls_CodiceFiscale


            If Not x.CheckPartitaIva(Format(Val(Txt_PartitaIVA.Text), "00000000000")) Then
                Img_VerificaPIVA.ImageUrl = "~/images/errore.gif"
            End If
        End If

        If Val(Txt_PartitaIVA.Text) > 0 And Val(Txt_Codice.Text) = 0 Then
            Dim Anag As New Cls_ClienteFornitore
            Anag.PARTITAIVA = Val(Txt_PartitaIVA.Text)
            Anag.CercaCF(Session("DC_OSPITE"))

            If Anag.CODICEDEBITORECREDITORE > 0 And Anag.CODICEDEBITORECREDITORE <> Val(Txt_Codice.Text) Then
                ClientScript.RegisterClientScriptBlock(Me.GetType(), "Errore", "VisualizzaErrore('P.IVA già usato per " & Anag.Nome & "');", True)
                Exit Sub
            End If
        End If
    End Sub

    Protected Sub Txt_RagioneSociale_TextChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles Txt_RagioneSociale.TextChanged
        Img_VerificaDes.ImageUrl = "~/images/Blanco.png"
        If Txt_RagioneSociale.Text.Trim <> "" Then
            Img_VerificaDes.ImageUrl = "~/images/corretto.gif"

            Dim x As New Cls_ClienteFornitore

            x.CODICEDEBITORECREDITORE = 0
            x.Nome = Txt_RagioneSociale.Text
            x.RicercaPerDescrizione(Session("DC_OSPITE"))


            If x.CODICEDEBITORECREDITORE > 0 And x.CODICEDEBITORECREDITORE <> Val(Txt_Codice.Text) Then
                Img_VerificaDes.ImageUrl = "~/images/errore.gif"
            End If
        End If
    End Sub

    Protected Sub Grd_Cig_RowCommand(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewCommandEventArgs) Handles Grd_Cig.RowCommand
        If (e.CommandName = "Inserisci") Then
            Call InserisciRiga()
        End If
    End Sub

    Protected Sub Grd_Cig_RowDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs) Handles Grd_Cig.RowDataBound
        If e.Row.RowType = DataControlRowType.DataRow Then


            Dim TxtDataInizio As TextBox = DirectCast(e.Row.FindControl("TxtDataInizio"), TextBox)

            TxtDataInizio.Text = Tabella.Rows(e.Row.RowIndex).Item(0).ToString

            Dim TxtDataFine As TextBox = DirectCast(e.Row.FindControl("TxtDataFine"), TextBox)

            TxtDataFine.Text = Tabella.Rows(e.Row.RowIndex).Item(1).ToString

            Dim TxtCig As TextBox = DirectCast(e.Row.FindControl("TxtCig"), TextBox)

            TxtCig.Text = Tabella.Rows(e.Row.RowIndex).Item(2).ToString


            Dim TxtImporto As TextBox = DirectCast(e.Row.FindControl("TxtImporto"), TextBox)

            TxtImporto.Text = Tabella.Rows(e.Row.RowIndex).Item(3).ToString


            Dim TxtDescrizione As TextBox = DirectCast(e.Row.FindControl("TxtDescrizione"), TextBox)

            TxtDescrizione.Text = Tabella.Rows(e.Row.RowIndex).Item(4).ToString

        End If
    End Sub

    Protected Sub Grd_Cig_RowDeleting(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewDeleteEventArgs) Handles Grd_Cig.RowDeleting

        UpDateTable()
        Tabella = Session("Tabella")
        Try
            If Request.UserAgent.Contains("Chrome") Then
                If IsDBNull(Session("RI_TIMER")) Or DateDiff("s", Session("RI_TIMER"), Now) > 1 Then
                    Tabella.Rows.RemoveAt(e.RowIndex)
                    If Tabella.Rows.Count = 0 Then
                        Dim myriga As System.Data.DataRow = Tabella.NewRow()
                        myriga(1) = 0
                        myriga(2) = 0
                        Tabella.Rows.Add(myriga)
                    End If
                    Session("RI_TIMER") = Now
                End If
            Else
                Tabella.Rows.RemoveAt(e.RowIndex)
                If Tabella.Rows.Count = 0 Then
                    Dim myriga As System.Data.DataRow = Tabella.NewRow()
                    myriga(1) = 0
                    myriga(2) = 0
                    Tabella.Rows.Add(myriga)
                End If
            End If
        Catch ex As Exception

        End Try


        Session("Tabella") = Tabella

        Grd_Cig.AutoGenerateColumns = False

        Grd_Cig.DataSource = Tabella

        Grd_Cig.DataBind()
        Call EseguiJS()

        e.Cancel = True
    End Sub
End Class
