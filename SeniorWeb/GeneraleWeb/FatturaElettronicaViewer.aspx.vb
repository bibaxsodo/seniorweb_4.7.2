﻿Imports System.Xml
Imports System.Xml.XPath
Imports System.Xml.Xsl
Imports System.IO


Partial Class GeneraleWeb_FatturaElettronicaViewer
    Inherits System.Web.UI.Page

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Page.IsPostBack = True Then Exit Sub


        'Dim objStreamReaderxml As StreamReader
        'objStreamReaderxml = File.OpenText(Server.MapPath(Session("FILENAME")))
        'Dim contenutoxml As String = objStreamReaderxml.ReadToEnd()
        'objStreamReaderxml.Close()

        Dim NomeXsl As String = Server.MapPath("..\Modulo\fatturapa_v12.xsl")
        DD_Tipo.SelectedValue = "0"
        If (Request.Cookies("FattViewer") IsNot Nothing) Then
            If Request.Cookies("FattViewer").Value.ToUpper = "1" Then
                NomeXsl = Server.MapPath("..\Modulo\FoglioStileAssoSoftware.xsl")
                DD_Tipo.SelectedValue = "1"
            End If
        End If


        Dim objStreamReader As StreamReader
        objStreamReader = File.OpenText(NomeXsl)
        'objStreamReader = File.OpenText(Server.MapPath("..\Modulo\FoglioStileAssoSoftware.xsl"))
        Dim contenuto As String = objStreamReader.ReadToEnd()
        objStreamReader.Close()

        Session("FATTURAXML") = Regex.Replace(Session("FATTURAXML"), "[^\u0000-\u007F]+", String.Empty)
        Session("FATTURAXML") = Session("FATTURAXML").Replace("&", "")
        Session("FATTURAXML") = Session("FATTURAXML").Replace("'", "")
        
        LblViewer.Text = TransformXMLToHTML(Session("FATTURAXML"), contenuto)


    End Sub

    Public Shared Function TransformXMLToHTML(ByVal inputXml As String, ByVal xsltString As String) As String
        Dim transform As XslCompiledTransform = New XslCompiledTransform()

        Using reader As XmlReader = XmlReader.Create(New StringReader(xsltString))
            transform.Load(reader)
        End Using

        Dim results As StringWriter = New StringWriter()

        Using reader As XmlReader = XmlReader.Create(New StringReader(inputXml))
            transform.Transform(reader, Nothing, results)
        End Using

        Return results.ToString()
    End Function


    Protected Sub DD_Tipo_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles DD_Tipo.SelectedIndexChanged
        Dim objStreamReader As StreamReader

        If DD_Tipo.SelectedValue = "0" Then
            objStreamReader = File.OpenText(Server.MapPath("..\Modulo\fatturapa_v12.xsl"))
        End If
        If DD_Tipo.SelectedValue = "1" Then
            objStreamReader = File.OpenText(Server.MapPath("..\Modulo\FoglioStileAssoSoftware.xsl"))
        End If
        Dim contenuto As String = objStreamReader.ReadToEnd()
        objStreamReader.Close()




        Session("FATTURAXML") = Regex.Replace(Session("FATTURAXML"), "[^\u0000-\u007F]+", String.Empty)
        Session("FATTURAXML") = Session("FATTURAXML").Replace("&", "")
        Session("FATTURAXML") = Session("FATTURAXML").Replace("'", "")


        If (Request.Cookies("FattViewer") IsNot Nothing) Then
            Dim cookie As HttpCookie = HttpContext.Current.Request.Cookies("FattViewer")

            cookie.Value = DD_Tipo.SelectedValue

            cookie.Expires = DateTime.Now.AddYears(2)

            Response.Cookies.Add(cookie)


        Else

            Dim aCookie As New HttpCookie("FattViewer")

            aCookie.Value = DD_Tipo.SelectedValue

            aCookie.Expires = DateTime.Now.AddYears(2)

            Response.Cookies.Add(aCookie)
        End If

        LblViewer.Text = TransformXMLToHTML(Session("FATTURAXML"), contenuto)
    End Sub
End Class
