﻿
Partial Class GeneraleWeb_VarazioneBudget
    Inherits System.Web.UI.Page

    Private Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load


        If Page.IsPostBack = True Then Exit Sub



        Dim K1 As New Cls_SqlString

        Dim Barra As New Cls_BarraSenior

        If Not IsNothing(Session("RicercaAnagraficaSQLString")) Then
            Try
                K1 = Session("RicercaAnagraficaSQLString")
            Catch ex As Exception
                K1 = Nothing
            End Try
        End If

        Lbl_BarraSenior.Text = Barra.CodiceBarra(Application("SENIOR"), Session("UTENTE"), Page, K1)


        If Val(Request.Item("ANNO")) > 0 Then
            Txt_Anno.Text = Val(Request.Item("ANNO"))
        End If


        Dim x As New Cls_colonnebudget

        x.Anno = Val(Txt_Anno.Text)
        x.UpDateDropBox(Session("DC_GENERALE"), DD_Colonna)


        If Val(Request.Item("Livello1")) > 0 Then
            Dim k As New Cls_ContoBudget

            k.ANNO = Txt_Anno.Text
            Txt_Livello.Text = k.DecodificaSottoconto(Session("DC_GENERALE"), Val(Request.Item("Livello1")) & " " & Val(Request.Item("Livello2")) & " " & Val(Request.Item("Livello3")))
            Txt_Data.Text = Request.Item("Data")

            DD_Colonna.SelectedValue = Val(Request.Item("Colonna"))

            Dim VarK As New Cls_VariazioniBudget

            VarK.Anno = Txt_Anno.Text
            VarK.Livello1 = Val(Request.Item("Livello1"))
            VarK.Livello2 = Val(Request.Item("Livello2"))
            VarK.Livello3 = Val(Request.Item("Livello3"))
            VarK.Colonna = Val(Request.Item("Colonna"))
            VarK.Data = Txt_Data.Text
            VarK.Leggi(Session("DC_GENERALE"))
            Txt_Descrizione.Text = VarK.Descrizione

            Txt_Importo.Text = Format(VarK.Importo, "#,##0.00")

            Txt_Anno.Enabled = False
            Txt_Livello.Enabled = False
            DD_Colonna.Enabled = False
            Txt_Data.Enabled = False
        End If


        Call EseguiJS()

    End Sub


    Private Sub EseguiJS()
        Dim MyJs As String
        MyJs = "$(document).ready(function() {"

        MyJs = MyJs & "var els = document.getElementsByTagName(""*"");"

        MyJs = MyJs & "for (var i=0;i<els.length;i++)"
        MyJs = MyJs & "if ( els[i].id ) { "
        MyJs = MyJs & " var appoggio =els[i].id; "
        MyJs = MyJs & " if (appoggio.match('Txt_Livello')!= null) {  "
        MyJs = MyJs & " $(els[i]).autocomplete('autocompletecontobudget.ashx?Anno=" & Txt_Anno.Text & "&Utente=" & Session("UTENTE") & "', {delay:5,minChars:3});"
        MyJs = MyJs & "    }"

        MyJs = MyJs & " if ((appoggio.match('Txt_Importo')!= null) || appoggio.match('TxtAvere')!= null) {  "
        MyJs = MyJs & " $(els[i]).keypress(function() { ForceNumericInput($(this).val(), true, true); } ); "
        MyJs = MyJs & " $(els[i]).blur(function() { var ap = formatNumber($(this).val(),2); $(this).val(ap); });"
        MyJs = MyJs & "    }"

        MyJs = MyJs & " if (appoggio.match('Txt_Data')!= null) {  "
        MyJs = MyJs & " $(els[i]).mask(""99/99/9999""); "

        MyJs = MyJs & " $(els[i]).datepicker({ changeMonth: true,changeYear: true,  yearRange: ""1890:" & Year(Now) + 1 & """},$.datepicker.regional[""it""]);"
        MyJs = MyJs & "    }"

        MyJs = MyJs & "} "
        MyJs = MyJs & "});"

        ScriptManager.RegisterStartupScript(Me, Me.GetType(), "JS_GESTPRE", MyJs, True)
    End Sub

    Protected Sub Btn_Modifica_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles Btn_Modifica.Click
        Dim K As New Cls_VariazioniBudget

        Dim Vettore(100) As String
        Dim APPOGGIO As String

        APPOGGIO = Txt_Livello.Text
        Vettore = SplitWords(APPOGGIO)

        K.Anno = Val(Txt_Anno.Text)

        If Val(Vettore(0)) > 0 Then
            K.Livello1 = Val(Vettore(0))
        End If

        If Val(Vettore(1)) > 0 Then
            K.Livello2 = Val(Vettore(1))
        End If
        If Val(Vettore(2)) > 0 Then
            K.Livello3 = Val(Vettore(2))
        End If

        K.Colonna = DD_Colonna.SelectedValue

        K.Descrizione = Txt_Descrizione.Text

        K.Data = Txt_Data.Text
        K.Importo = Txt_Importo.Text

        K.Scrivi(Session("DC_GENERALE"))

        Dim MyJs As String
        MyJs = "alert('Modifica Effettuata');"
        ScriptManager.RegisterClientScriptBlock(Me, Me.GetType(), "ModScM", MyJs, True)

        Txt_Anno.Enabled = False
        Txt_Data.Enabled = False
        Txt_Descrizione.Enabled = False
        Txt_Livello.Enabled = False
        DD_Colonna.Enabled = False

    End Sub



    Private Function SplitWords(ByVal s As String) As String()
        '
        ' Call Regex.Split function from the imported namespace.
        ' Return the result array.
        '
        Return Regex.Split(s, "\W+")
    End Function

    Protected Sub Btn_Esci_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles Btn_Esci.Click
        Response.Redirect("ElencoVariazioniBudget.aspx")
    End Sub

    Protected Sub ImageButton1_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles ImageButton1.Click

        Dim K As New Cls_VariazioniBudget

        Dim Vettore(100) As String
        Dim APPOGGIO As String

        APPOGGIO = Txt_Livello.Text
        Vettore = SplitWords(APPOGGIO)

        K.Anno = Val(Txt_Anno.Text)
        If Val(Vettore(0)) > 0 Then
            K.Livello1 = Val(Vettore(0))
        End If

        If Val(Vettore(1)) > 0 Then
            K.Livello2 = Val(Vettore(1))
        End If
        If Val(Vettore(2)) > 0 Then
            K.Livello3 = Val(Vettore(2))
        End If

        K.Colonna = DD_Colonna.SelectedValue

        K.Descrizione = Txt_Descrizione.Text

        K.Data = Txt_Data.Text

        K.Elimina(Session("DC_GENERALE"))

        Response.Redirect("ElencoVariazioniBudget.aspx")
    End Sub
End Class
