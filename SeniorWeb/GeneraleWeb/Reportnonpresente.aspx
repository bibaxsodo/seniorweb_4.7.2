﻿<%@ Page Language="VB" AutoEventWireup="false" Inherits="GeneraleWeb_Reportnonpresente" CodeFile="Reportnonpresente.aspx.vb" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>Report non presente</title>
    <asp:PlaceHolder runat="server">
        <%: System.Web.Optimization.Styles.Render("~/Content/AjaxControlToolkit/Styles/Bundle") %>
    </asp:PlaceHolder>
    <link rel="shortcut icon" href="../images/SENIOR.ico" />
</head>
<body>
    <form id="form1" runat="server">
        <asp:ScriptManager ID="ScriptManager1" runat="server" EnableScriptGlobalization="true">
            <Scripts>
                <asp:ScriptReference Path="~/Scripts/AjaxControlToolkit/Bundle" />
            </Scripts>
        </asp:ScriptManager>
        <div>
            <table style="width: 100%;">
                <tr>
                    <td>
                        <h1 class="Titolo">Report non presente</asp:Label></h1>
                    </td>
                    <td style="text-align: right;">
                        <a href="javascript:window.close();">
                            <img src="images/esci.jpg" tooltip="Chiudi" /></a>
                    </td>
                </tr>
            </table>
            <br />
            <asp:Label ID="Lbl_NomeReport" runat="server" Text=""></asp:Label>
        </div>
    </form>
</body>
</html>
