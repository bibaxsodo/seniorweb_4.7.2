﻿Imports System
Imports System.Web
Imports System.Data.OleDb
Imports System.Configuration
Imports System.Text
Imports System.Web.Hosting

Partial Class GeneraleWeb_AliquotaIVA
    Inherits System.Web.UI.Page

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Trim(Session("UTENTE")) = "" Then
            If Request.ServerVariables("URL").ToUpper.IndexOf("SENIORWEBBLANCO") > 0 Then
                Response.Redirect("/ODV/Login.aspx")
                Exit Sub
            Else
                Response.Redirect("/SeniorWeb/Login.aspx")
                Exit Sub
            End If
        End If


        If Page.IsPostBack = True Then Exit Sub

        Dim K1 As New Cls_SqlString

        DD_Natura.Items.Clear()
        DD_Natura.Items.Add("escluse ex art. 15")
        DD_Natura.Items(DD_Natura.Items.Count - 1).Value = "N1"
        DD_Natura.Items.Add("non soggette")
        DD_Natura.Items(DD_Natura.Items.Count - 1).Value = "N2"
        DD_Natura.Items.Add("non imponibili")
        DD_Natura.Items(DD_Natura.Items.Count - 1).Value = "N3"
        DD_Natura.Items.Add("esenti")
        DD_Natura.Items(DD_Natura.Items.Count - 1).Value = "N4"
        DD_Natura.Items.Add("regime del margine")
        DD_Natura.Items(DD_Natura.Items.Count - 1).Value = "N5"
        DD_Natura.Items.Add("Reverse Charge")
        DD_Natura.Items(DD_Natura.Items.Count - 1).Value = "N6"
        DD_Natura.Items.Add("")
        DD_Natura.Items(DD_Natura.Items.Count - 1).Value = ""

        Dim Barra As New Cls_BarraSenior

        If Not IsNothing(Session("RicercaGeneraleSQLString")) Then
            K1 = Session("RicercaGeneraleSQLString")
        End If

        Lbl_BarraSenior.Text = Barra.CodiceBarra(Application("SENIOR"), Session("UTENTE"), Page, K1)


        Dim l As New ClsDetraibilita

        l.UpDateDropBox(Session("DC_TABELLE"), DD_Detraibile)

        If Request.Item("Codice") = "" Then
            Exit Sub
        End If

        Dim Xs As New Cls_IVA

        Txt_Codice.Text = Request.Item("Codice")
        Xs.Leggi(Session("DC_TABELLE"), Txt_Codice.Text)

        Txt_Codice.Enabled = False

        Txt_Codice.Text = Xs.Codice
        Txt_Descrizione.Text = Xs.Descrizione
        Txt_Aliquota.Text = Xs.Aliquota
        Txt_Clienti.Text = Xs.AllegatoClienti
        Txt_Fornitori.Text = Xs.AllegatoFornitori
        RB_Soggetta.Checked = False
        RB_Esente.Checked = False
        RB_NonImponibile.Checked = False
        RB_NonSoggetta.Checked = False
        DD_Natura.SelectedValue = Xs.Natura

        If Xs.Tipo = "" Then
            RB_Soggetta.Checked = True
        End If
        If Xs.Tipo = "ES" Then
            RB_Esente.Checked = True
        End If
        If Xs.Tipo = "NI" Then
            RB_NonImponibile.Checked = True
        End If
        If Xs.Tipo = "NS" Then
            RB_NonSoggetta.Checked = True
        End If
        If Xs.InLiquidazione = 1 Then
            Chk_NonInLiquidazione.Checked = True
        Else
            Chk_NonInLiquidazione.Checked = False
        End If

        If Xs.InSpesometro = 1 Then
            Chk_Spesometro.Checked = True
        Else
            Chk_Spesometro.Checked = False
        End If
        DD_Detraibile.SelectedValue = Xs.DetraibilitaDefault


        If Xs.NonInUso = 1 Then
            Chk_NonInUso.Checked = True
        Else
            Chk_NonInUso.Checked = False
        End If

        EseguiJS()
    End Sub


    Private Sub Pulisci()
        Txt_Codice.Text = ""
        Txt_Descrizione.Text = ""
        Txt_Aliquota.Text = 0
        Txt_Clienti.Text = 0
        Txt_Fornitori.Text = 0
        RB_Soggetta.Checked = True
        RB_Esente.Checked = False
        RB_NonImponibile.Checked = False
        RB_NonSoggetta.Checked = False

        DD_Natura.SelectedValue = ""
        Chk_NonInLiquidazione.Checked = False
        Chk_Spesometro.Checked = False

        DD_Detraibile.SelectedValue = ""
        EseguiJS()
    
    End Sub


    Protected Sub Btn_Modifica_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles Btn_Modifica.Click
        Lbl_Errori.Text = ""
        If Trim(Txt_Codice.Text) = "" Then
            EseguiJS()
            ClientScript.RegisterClientScriptBlock(Me.GetType(), "Errore", "VisualizzaErrore('Devi indicare un codice');", True)
            Exit Sub
        End If

        If Not IsNumeric(Txt_Aliquota.Text) Then
            EseguiJS()
            ClientScript.RegisterClientScriptBlock(Me.GetType(), "Errore", "VisualizzaErrore('Aliquota iva non numerica');", True)
            Exit Sub
        End If
        If Not IsNumeric(Txt_Clienti.Text) Then
            ClientScript.RegisterClientScriptBlock(Me.GetType(), "Errore", "VisualizzaErrore('Allegato clienti numerico');", True)
            EseguiJS()
            Exit Sub
        End If
        If Not IsNumeric(Txt_Fornitori.Text) Then
            ClientScript.RegisterClientScriptBlock(Me.GetType(), "Errore", "VisualizzaErrore('Allegato fornitori numerico');", True)
            EseguiJS()
            Exit Sub
        End If

        If Txt_Codice.Enabled = True Then
            Dim xVerifica As New Cls_IVA

            xVerifica.Leggi(Session("DC_TABELLE"), Txt_Codice.Text)

            If xVerifica.Descrizione <> "" Then
                ClientScript.RegisterClientScriptBlock(Me.GetType(), "Errore", "VisualizzaErrore('Codice già utilizzato');", True)
                EseguiJS()
                Exit Sub
            End If
        End If


        Dim Xs As New Cls_IVA

        Xs.Leggi(Session("DC_TABELLE"), Txt_Codice.Text)



        Dim Log As New Cls_LogPrivacy
        Dim ConvT As New Cls_DataTableToJson
        Dim AppoggioJS As String


        AppoggioJS = ConvT.SerializeObject(Xs)
        Log.LogPrivacy(Application("SENIOR"), Session("CLIENTE"), Session("UTENTE"), Session("UTENTEIMPER"), Page.GetType.Name.ToUpper, 0, 0, "", "", 0, "", "M", "IVA", AppoggioJS)


        Xs.Codice = Txt_Codice.Text
        Xs.Descrizione = Txt_Descrizione.Text
        Xs.Aliquota = Txt_Aliquota.Text
        Xs.AllegatoClienti = Val(Txt_Clienti.Text)
        Xs.AllegatoFornitori = Val(Txt_Fornitori.Text)
        
        Xs.Natura = DD_Natura.SelectedValue

        If RB_Soggetta.Checked = True Then
            Xs.Tipo = ""
        End If
        If RB_Esente.Checked = True Then
            Xs.Tipo = "ES"
        End If
        If RB_NonImponibile.Checked = True Then
            Xs.Tipo = "NI"
        End If
        If RB_NonSoggetta.Checked = True Then
            Xs.Tipo = "NS"
        End If

        If Chk_NonInLiquidazione.Checked = True Then
            Xs.InLiquidazione = 1
        Else
            Xs.InLiquidazione = 0
        End If

        If Chk_Spesometro.Checked = True Then
            Xs.InSpesometro = 1
        Else
            Xs.InSpesometro = 0
        End If
        Xs.DetraibilitaDefault = DD_Detraibile.SelectedValue


        If Chk_NonInUso.Checked = True Then
            Xs.NonInUso = 1
        Else
            Xs.NonInUso = 0
        End If

        Xs.Scrivi(Session("DC_TABELLE"))

        Response.Redirect("ElencoAliquotaIVA.aspx")
    End Sub

    Protected Sub ImageButton1_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles ImageButton1.Click
        If Txt_Codice.Text = "" Then
            ClientScript.RegisterClientScriptBlock(Me.GetType(), "Errore", "VisualizzaErrore('Specifica codice');", True)
            REM Lbl_Errori.Text = "Specifica codice"
            Exit Sub
        End If

        Dim x As New Cls_IVA


        x.Leggi(Session("DC_TABELLE"), Txt_Codice.Text)

        If x.Descrizione = "" Then
            ClientScript.RegisterClientScriptBlock(Me.GetType(), "Errore", "VisualizzaErrore('Codice Inesistente');", True)
            REM Lbl_Errori.Text = "Codice inesitente"
            Exit Sub
        End If

        Dim cn As OleDbConnection

        cn = New Data.OleDb.OleDbConnection(Session("DC_GENERALE"))

        cn.Open()
        Dim cmd As New OleDbCommand()
        cmd.CommandText = ("select * from MovimentiContabiliRiga where " & _
                           "CodiceIVA = ? ")
        cmd.Parameters.AddWithValue("@CodiceIVA", Txt_Codice.Text)
        cmd.Connection = cn

        Dim myPOSTreader As OleDbDataReader = cmd.ExecuteReader()
        If myPOSTreader.Read Then
            ClientScript.RegisterClientScriptBlock(Me.GetType(), "Errore", "VisualizzaErrore('Non posso eliminare vi sono registrazioni con questo Codice IVA');", True)
            REM Lbl_Errori.Text = "Non posso eliminare vi sono registrazioni con questo Codice IVA"
            cn.Close()
            Exit Sub
        End If
        cn.Close()


        Dim Log As New Cls_LogPrivacy
        Dim ConvT As New Cls_DataTableToJson
        Dim AppoggioJS As String


        AppoggioJS = ConvT.SerializeObject(x)
        Log.LogPrivacy(Application("SENIOR"), Session("CLIENTE"), Session("UTENTE"), Session("UTENTEIMPER"), Page.GetType.Name.ToUpper, 0, 0, "", "", 0, "", "D", "IVA", AppoggioJS)


        x.Codice = Txt_Codice.Text
        x.Elimina(Session("DC_TABELLE"))
        Response.Redirect("ElencoAliquotaIVA.aspx")
    End Sub


  

    Protected Sub Btn_Esci_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles Btn_Esci.Click        
        Response.Redirect("ElencoAliquotaIVA.aspx")
    End Sub


    Private Sub EseguiJS()
        Dim MyJs As String
        MyJs = "$(document).ready(function() {"

        MyJs = MyJs & "var els = document.getElementsByTagName(""*"");"

        MyJs = MyJs & "for (var i=0;i<els.length;i++)"
        MyJs = MyJs & "if ( els[i].id ) { "
        MyJs = MyJs & " var appoggio =els[i].id; "
        MyJs = MyJs & " if (appoggio.match('Txt_Sottoconto')!= null) {  "
        MyJs = MyJs & " $(els[i]).autocomplete('/WebHandler/GestioneConto.ashx?Utente=" & Session("UTENTE") & "', {delay:5,minChars:3});"
        MyJs = MyJs & "    }"

        MyJs = MyJs & " if (appoggio.match('Txt_Aliquota')!= null) {  "
        MyJs = MyJs & " $(els[i]).keypress(function() { ForceNumericInput($(this).val(), true, true); } ); "
        MyJs = MyJs & " $(els[i]).blur(function() { var ap = formatNumber($(this).val(),2); $(this).val(ap); });"
        MyJs = MyJs & "    }"


        MyJs = MyJs & "} "

        MyJs = MyJs & "if (window.innerHeight>0) { $(""#BarraLaterale"").css(""height"",(window.innerHeight - 94) + ""px""); } else"
        MyJs = MyJs & "{ $(""#BarraLaterale"").css(""height"",(document.documentElement.offsetHeight - 94) + ""px"");  }"
        MyJs = MyJs & "});"


        ScriptManager.RegisterStartupScript(Me, Me.GetType(), "visualizzaRitIm", MyJs, True)
    End Sub


    Protected Sub Txt_Codice_TextChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles Txt_Codice.TextChanged
        If Txt_Codice.Enabled = True Then
            Img_VerificaCodice.ImageUrl = "~/images/corretto.gif"

            Dim x As New Cls_IVA

            x.Codice = Txt_Codice.Text
            x.Leggi(Session("DC_TABELLE"), x.Codice)

            If x.Descrizione <> "" Then
                Img_VerificaCodice.ImageUrl = "~/images/errore.gif"
            End If
        End If
    End Sub

    Protected Sub Txt_Descrizione_TextChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles Txt_Descrizione.TextChanged
        If Txt_Codice.Enabled = True Then
            Img_VerificaDescrizione.ImageUrl = "~/images/corretto.gif"

            Dim x As New Cls_IVA

            x.Tipo = 0
            x.Descrizione = Txt_Descrizione.Text.Trim
            x.LeggiDescrizione(Session("DC_TABELLE"), x.Descrizione.Trim)

            If Val(x.Tipo) <> 0 Then
                Img_VerificaDescrizione.ImageUrl = "~/images/errore.gif"
            End If
        End If
    End Sub
End Class
