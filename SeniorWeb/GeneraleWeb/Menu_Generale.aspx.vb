﻿
Partial Class GeneraleWeb_Menu_Generale
    Inherits System.Web.UI.Page

 

    

    Protected Sub Btn_Esci_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles Btn_Esci.Click        
        If Session("ABILITAZIONI").IndexOf("<SOLOGENERALE>") > 0 Then
            Response.Redirect("..\Login.aspx")
            Exit Sub
        End If
        If Session("DACSV") = "CSV" Then
            Response.Redirect("/CSV/Menu_CSV.aspx")
            Exit Sub
        End If
        If Session("ODV") = "ODV" Then
            Response.Redirect("/ODV/Menu.aspx")
        Else
            Response.Redirect("..\MainMenu.aspx")
        End If
    End Sub

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Trim(Session("UTENTE")) = "" Then
            If Request.ServerVariables("URL").ToUpper.IndexOf("SENIORWEBBLANCO") > 0 Then
                Response.Redirect("/ODV/Login.aspx")
                Exit Sub
            Else
                Response.Redirect("/SeniorWeb/Login.aspx")
                Exit Sub
            End If
        End If

        If Session("ABILITAZIONI").IndexOf("<NONGENERALE>") > 0 Then
            Response.Redirect("..\MainMenu.aspx")
        End If


        If Request.Item("TIPO") = "CSV" Then
            Session("DACSV") = "CSV"
        End If

        Session("ULTIMOPROGRAMMA") = "MENUGENERALE"



        Dim k As New Cls_Login

        k.Utente = Session("UTENTE")
        k.LeggiSP(Application("SENIOR"))
        If IsNothing(k.ABILITAZIONI) Then
            k.ABILITAZIONI = ""
        End If

        If k.ABILITAZIONI.IndexOf("<PIANOCONTI>") < 0 Then
            Dim MyJs As String

            MyJs = "$(document).ready(function() {"

            MyJs = MyJs & "var els = document.getElementsByTagName(""*"");"

            MyJs = MyJs & "for (var i=0;i<els.length;i++)"
            MyJs = MyJs & "if ( els[i].id ) { "
            MyJs = MyJs & " var appoggio =els[i].id; "
            MyJs = MyJs & " if ((appoggio.match('Href_PianoConti')!= null) ) {   "
            MyJs = MyJs & " $(els[i]).attr('href','#');"
            MyJs = MyJs & " $(els[i]).click(function() { alert('Non sei abilitato a questa funzione'); });"
            MyJs = MyJs & "    }"
            MyJs = MyJs & "} "
            MyJs = MyJs & "});"
            ScriptManager.RegisterStartupScript(Me, Me.GetType(), "MettiInsibileJS1", MyJs, True)
        End If

        If k.ABILITAZIONI.IndexOf("<BUDGET>") < 0 Then
            Dim MyJs As String

            MyJs = "$(document).ready(function() {"

            MyJs = MyJs & "var els = document.getElementsByTagName(""*"");"

            MyJs = MyJs & "for (var i=0;i<els.length;i++)"
            MyJs = MyJs & "if ( els[i].id ) { "
            MyJs = MyJs & " var appoggio =els[i].id; "
            MyJs = MyJs & " if ((appoggio.match('Href_Budget')!= null) ) {   "
            MyJs = MyJs & " $(els[i]).attr('href','#');"
            MyJs = MyJs & " $(els[i]).click(function() { alert('Non sei abilitato a questa funzione'); });"
            MyJs = MyJs & "    }"
            MyJs = MyJs & "} "
            MyJs = MyJs & "});"
            ScriptManager.RegisterStartupScript(Me, Me.GetType(), "MettiInsibileJS2", MyJs, True)
        End If

        If k.ABILITAZIONI.IndexOf("<REGISTRAZIONE>") < 0 Then
            Dim MyJs As String

            MyJs = "$(document).ready(function() {"

            MyJs = MyJs & "var els = document.getElementsByTagName(""*"");"

            MyJs = MyJs & "for (var i=0;i<els.length;i++)"
            MyJs = MyJs & "if ( els[i].id ) { "
            MyJs = MyJs & " var appoggio =els[i].id; "
            MyJs = MyJs & " if ((appoggio.match('Href_Registrazioni')!= null) ) {   "
            MyJs = MyJs & " $(els[i]).attr('href','#');"
            MyJs = MyJs & " $(els[i]).click(function() { alert('Non sei abilitato a questa funzione'); });"
            MyJs = MyJs & "    }"
            MyJs = MyJs & "} "
            MyJs = MyJs & "});"
            ScriptManager.RegisterStartupScript(Me, Me.GetType(), "MettiInsibileJS3", MyJs, True)
        End If


        If k.ABILITAZIONI.IndexOf("<REGISTRAZIONE>") < 0 Then
            Dim MyJs As String

            MyJs = "$(document).ready(function() {"

            MyJs = MyJs & "var els = document.getElementsByTagName(""*"");"

            MyJs = MyJs & "for (var i=0;i<els.length;i++)"
            MyJs = MyJs & "if ( els[i].id ) { "
            MyJs = MyJs & " var appoggio =els[i].id; "
            MyJs = MyJs & " if ((appoggio.match('Href_Registrazioni')!= null) ) {   "
            MyJs = MyJs & " $(els[i]).attr('href','#');"
            MyJs = MyJs & " $(els[i]).click(function() { alert('Non sei abilitato a questa funzione'); });"
            MyJs = MyJs & "    }"
            MyJs = MyJs & "} "
            MyJs = MyJs & "});"
            ScriptManager.RegisterStartupScript(Me, Me.GetType(), "MettiInsibileJS3", MyJs, True)
        End If


        If k.ABILITAZIONI.IndexOf("<REGISTRAZIONE>") < 0 Then
            Dim MyJs As String

            MyJs = "$(document).ready(function() {"

            MyJs = MyJs & "var els = document.getElementsByTagName(""*"");"

            MyJs = MyJs & "for (var i=0;i<els.length;i++)"
            MyJs = MyJs & "if ( els[i].id ) { "
            MyJs = MyJs & " var appoggio =els[i].id; "
            MyJs = MyJs & " if ((appoggio.match('Href_ClientiFornitori')!= null) ) {   "
            MyJs = MyJs & " $(els[i]).attr('href','#');"
            MyJs = MyJs & " $(els[i]).click(function() { alert('Non sei abilitato a questa funzione'); });"
            MyJs = MyJs & "    }"
            MyJs = MyJs & "} "
            MyJs = MyJs & "});"
            ScriptManager.RegisterStartupScript(Me, Me.GetType(), "MettiInsibileJS4", MyJs, True)
        End If

        If k.ABILITAZIONI.IndexOf("<REGISTRAZIONE>") < 0 Then
            Dim MyJs As String

            MyJs = "$(document).ready(function() {"

            MyJs = MyJs & "var els = document.getElementsByTagName(""*"");"

            MyJs = MyJs & "for (var i=0;i<els.length;i++)"
            MyJs = MyJs & "if ( els[i].id ) { "
            MyJs = MyJs & " var appoggio =els[i].id; "
            MyJs = MyJs & " if ((appoggio.match('Href_Visualizzazioni')!= null) ) {   "
            MyJs = MyJs & " $(els[i]).attr('href','#');"
            MyJs = MyJs & " $(els[i]).click(function() { alert('Non sei abilitato a questa funzione'); });"
            MyJs = MyJs & "    }"
            MyJs = MyJs & "} "
            MyJs = MyJs & "});"
            ScriptManager.RegisterStartupScript(Me, Me.GetType(), "MettiInsibileJS5", MyJs, True)
        End If

        If k.ABILITAZIONI.IndexOf("<REGISTRAZIONE>") < 0 Then
            Dim MyJs As String

            MyJs = "$(document).ready(function() {"

            MyJs = MyJs & "var els = document.getElementsByTagName(""*"");"

            MyJs = MyJs & "for (var i=0;i<els.length;i++)"
            MyJs = MyJs & "if ( els[i].id ) { "
            MyJs = MyJs & " var appoggio =els[i].id; "
            MyJs = MyJs & " if ((appoggio.match('Href_Tabelle')!= null) ) {   "
            MyJs = MyJs & " $(els[i]).attr('href','#');"
            MyJs = MyJs & " $(els[i]).click(function() { alert('Non sei abilitato a questa funzione'); });"
            MyJs = MyJs & "    }"
            MyJs = MyJs & "} "
            MyJs = MyJs & "});"
            ScriptManager.RegisterStartupScript(Me, Me.GetType(), "MettiInsibileJS6", MyJs, True)
        End If

        If k.ABILITAZIONI.IndexOf("<REGISTRAZIONE>") < 0 Then
            Dim MyJs As String

            MyJs = "$(document).ready(function() {"

            MyJs = MyJs & "var els = document.getElementsByTagName(""*"");"

            MyJs = MyJs & "for (var i=0;i<els.length;i++)"
            MyJs = MyJs & "if ( els[i].id ) { "
            MyJs = MyJs & " var appoggio =els[i].id; "
            MyJs = MyJs & " if ((appoggio.match('Href_Tabelle')!= null) ) {   "
            MyJs = MyJs & " $(els[i]).attr('href','#');"
            MyJs = MyJs & " $(els[i]).click(function() { alert('Non sei abilitato a questa funzione'); });"
            MyJs = MyJs & "    }"
            MyJs = MyJs & "} "
            MyJs = MyJs & "});"
            ScriptManager.RegisterStartupScript(Me, Me.GetType(), "MettiInsibileJS6", MyJs, True)
        End If

        If k.ABILITAZIONI.IndexOf("<REGISTRAZIONE>") < 0 Then
            Dim MyJs As String

            MyJs = "$(document).ready(function() {"

            MyJs = MyJs & "var els = document.getElementsByTagName(""*"");"

            MyJs = MyJs & "for (var i=0;i<els.length;i++)"
            MyJs = MyJs & "if ( els[i].id ) { "
            MyJs = MyJs & " var appoggio =els[i].id; "
            MyJs = MyJs & " if ((appoggio.match('Href_Cespiti')!= null) ) {   "
            MyJs = MyJs & " $(els[i]).attr('href','#');"
            MyJs = MyJs & " $(els[i]).click(function() { alert('Non sei abilitato a questa funzione'); });"
            MyJs = MyJs & "    }"
            MyJs = MyJs & "} "
            MyJs = MyJs & "});"
            ScriptManager.RegisterStartupScript(Me, Me.GetType(), "MettiInsibileJS7", MyJs, True)
        End If

        If k.ABILITAZIONI.IndexOf("<REGISTRAZIONE>") < 0 Then
            Dim MyJs As String

            MyJs = "$(document).ready(function() {"

            MyJs = MyJs & "var els = document.getElementsByTagName(""*"");"

            MyJs = MyJs & "for (var i=0;i<els.length;i++)"
            MyJs = MyJs & "if ( els[i].id ) { "
            MyJs = MyJs & " var appoggio =els[i].id; "
            MyJs = MyJs & " if ((appoggio.match('Href_Strumenti')!= null) ) {   "
            MyJs = MyJs & " $(els[i]).attr('href','#');"
            MyJs = MyJs & " $(els[i]).click(function() { alert('Non sei abilitato a questa funzione'); });"
            MyJs = MyJs & "    }"
            MyJs = MyJs & "} "
            MyJs = MyJs & "});"
            ScriptManager.RegisterStartupScript(Me, Me.GetType(), "MettiInsibileJS8", MyJs, True)
        End If

        'Href_ClientiFornitori

        Dim DatiGenerale As New Cls_DatiGenerali

        DatiGenerale.LeggiDati(Session("DC_TABELLE"))

        If DatiGenerale.BudgetAnalitica = 1 Then
            lbl_BudgeAnalitica.Text = "ANALITICA"
        End If

        lblImpXML.Visible = False
        Btn_ImpXML.Visible = False
        lblExpXML.Visible = False
        Btn_ExpXML.Visible = False
        If Val(DatiGenerale.TestAgyo) = 1 Then
            lblImpXML.Visible = True
            Btn_ImpXML.Visible = True
            lblExpXML.Visible = True
            Btn_ExpXML.Visible = True
        End If


        Dim K1 As New Cls_SqlString

        Dim Barra As New Cls_BarraSenior

        If Not IsNothing(Session("RicercaGeneraleSQLString")) Then
            Try
                K1 = Session("RicercaGeneraleSQLString")
            Catch ex As Exception
                Session("RicercaGeneraleSQLString") = Nothing
            End Try
        End If

        Lbl_BarraSenior.Text = Barra.CodiceBarra(Application("SENIOR"), Session("UTENTE"), Page, K1)

        VerificaNews()
    End Sub

    Protected Sub Btn_ImpXML_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles Btn_ImpXML.Click
        Response.Redirect("ImportXML.aspx?MENU=GENERALE")
    End Sub

    Protected Sub Btn_ExpXML_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles Btn_ExpXML.Click
        Response.Redirect("FatturaElettronicaPrivati.aspx?MENU=GENERALE")
    End Sub


    Private Sub VerificaNews()


        If (Request.Cookies("Entrata") IsNot Nothing) Then
            If Request.Cookies("Entrata").Value.ToUpper = "14" Then
                ScriptManager.RegisterStartupScript(Me, Me.GetType(), "chiudinews", "$(document).ready(function() { chiudinews(); });", True)
            End If
        End If
        If DateDiff(DateInterval.Day, DateSerial(2020, 3, 1), Now) > 1 Then
            ScriptManager.RegisterStartupScript(Me, Me.GetType(), "chiudinews", "$(document).ready(function() { chiudinews(); });", True)
        End If

    End Sub
End Class
