﻿Imports System.Web.Hosting
Imports System.Data.OleDb


Partial Class GeneraleWeb_RicercaRegistrazioniBudget
    Inherits System.Web.UI.Page
    Dim Tabella As New System.Data.DataTable("tabella")

    Protected Sub Esegui_Ricerca(ByVal ToExcel As Boolean)
        Dim ConnectionString As String = Session("DC_TABELLE")
        Dim ConnectionStringGenerale As String = Session("DC_GENERALE")
        Dim MySql As String
        Dim Condizione As String
        Dim Tipo As Integer = 0
        Dim MySqlrs As String
        Dim MySqlrs2 As String
        Dim Saldo As Double


        Condizione = ""
        Dim cn As OleDbConnection

        cn = New Data.OleDb.OleDbConnection(ConnectionStringGenerale)

        cn.Open()

        MySql = "Select * From MovimentiContabiliTesta"
        If DD_CausaleContabile.SelectedValue <> "" Then
            If Condizione <> "" Then
                Condizione = Condizione & " And "
            End If
            Condizione = Condizione & "( CausaleContabile = '" & DD_CausaleContabile.SelectedValue & "'"
            
            Condizione = Condizione & ") "
        End If


        If IsDate(Txt_DataDal.Text) Then
            If Condizione <> "" Then
                Condizione = Condizione & " And "
            End If
            Condizione = Condizione & " DataRegistrazione >= ?"
        End If

        If IsDate(Txt_DataAl.Text) Then
            If Condizione <> "" Then
                Condizione = Condizione & " And "
            End If
            Condizione = Condizione & " DataRegistrazione <= ? "
        End If

        
        If Trim(Txt_Sottoconto.Text) <> "" Then
            Dim Vettore(100) As String
            Dim APPOGGIO As String

            APPOGGIO = Txt_Sottoconto.Text
            Vettore = SplitWords(APPOGGIO)

            'If Val(Vettore(0)) > 0 Then
            '    If Condizione <> "" Then
            '        Condizione = Condizione & " AND "
            '    End If
            '    Condizione = Condizione & " (Select count(*) From MovimentiContabiliRiga where Numero =NumeroRegistrazione And MastroPartita = " & Val(Vettore(0)) & ") > 0"
            'End If


            'If Val(Vettore(1)) > 0 Then
            '    If Condizione <> "" Then
            '        Condizione = Condizione & " AND "
            '    End If
            '    Condizione = Condizione & " (Select count(*) From MovimentiContabiliRiga where Numero =NumeroRegistrazione And ContoPartita = " & Val(Vettore(1)) & ") > 0"
            'End If

            If Val(Vettore(2)) > 0 Then
                If Condizione <> "" Then
                    Condizione = Condizione & " AND "
                End If
                Condizione = Condizione & " (Select count(*) From MovimentiContabiliRiga where Numero =NumeroRegistrazione And MastroPartita = " & Val(Vettore(0)) & " And ContoPartita = " & Val(Vettore(1)) & " And SottoContoPartita = " & Val(Vettore(2)) & ") > 0"
            End If
        End If

        If Val(txt_anno.text) > 0 And Txt_Livello.Text <> "" Then
            Dim Vettore(100) As String
            Dim APPOGGIO As String

            APPOGGIO = Txt_Livello.Text
            Vettore = SplitWords(APPOGGIO)


            If Val(Vettore(2)) > 0 Then
                If Condizione <> "" Then
                    Condizione = Condizione & " AND "
                End If
                Condizione = Condizione & " (Select count(*) From LegameBudgetRegistrazione where LegameBudgetRegistrazione.NumeroRegistrazione = MovimentiContabiliTesta.NumeroRegistrazione And Livello1 = " & Val(Vettore(0)) & " And Livello2 = " & Val(Vettore(1)) & " And Livello3 = " & Val(Vettore(2)) & ") > 0"
            End If
        End If
        Dim cmd As New OleDbCommand()

        cmd.CommandText = MySql & " Where " & Condizione
        If IsDate(Txt_DataDal.Text) Then
            Dim DataDal As Date = Txt_DataDal.Text
            cmd.Parameters.AddWithValue("@DataDal", DataDal)
        End If

        If IsDate(Txt_DataAl.Text) Then
            Dim DataAl As Date = Txt_DataAl.Text
            cmd.Parameters.AddWithValue("@DataAl", DataAl)
        End If

        cmd.Connection = cn

        Tabella.Clear()
        Tabella.Columns.Clear()
        Tabella.Columns.Add("Numero", GetType(String))
        Tabella.Columns.Add("Dt.Reg.", GetType(String))
        Tabella.Columns.Add("N.Doc.", GetType(String))
        Tabella.Columns.Add("Dt.Doc.", GetType(String))

        Tabella.Columns.Add("N.Protoc.", GetType(Long))
        Tabella.Columns.Add("Anno Protoc.", GetType(Long))
        Tabella.Columns.Add("Causale", GetType(String))
        Tabella.Columns.Add("Descrizione", GetType(String))
        If Chk_Colonne.Checked = False Then
            Tabella.Columns.Add("Importo Legato", GetType(String))
        Else
            Tabella.Columns.Add("Non Assegnato", GetType(String))
            Dim SqlColonne As String
            SqlColonne = "Select * FROM ColonneBudget WHERE Anno = " & Year(Txt_DataAl.Text) & " Order by Livello1"

            Dim CmdColonne As New OleDbCommand(SqlColonne, cn)
            Dim ReadColonne As OleDbDataReader = CmdColonne.ExecuteReader()
            Do While ReadColonne.Read
                Tabella.Columns.Add(campodb(ReadColonne.Item("Descrizione")), GetType(String))
            Loop
            ReadColonne.Close()
        End If
        Tabella.Columns.Add("Importo Documento", GetType(String))
        Tabella.Columns.Add("Importo Costo/Ricavi", GetType(String))

        Dim myPOSTreader As OleDbDataReader = cmd.ExecuteReader()
        Do While myPOSTreader.Read


            Dim myriga As System.Data.DataRow = Tabella.NewRow()
            myriga(0) = campodbn(myPOSTreader.Item("NumeroRegistrazione"))
            If IsDBNull(myPOSTreader.Item("DataRegistrazione")) Then
                myriga(1) = ""
            Else
                myriga(1) = Format(myPOSTreader.Item("DataRegistrazione"), "dd/MM/yyyy")
            End If
            myriga(2) = myPOSTreader.Item("NumeroDocumento")
            If IsDBNull(myPOSTreader.Item("DataDocumento")) Then
                myriga(3) = ""
            Else
                myriga(3) = Format(myPOSTreader.Item("DataDocumento"), "dd/MM/yyyy")
            End If
            myriga(4) = myPOSTreader.Item("NumeroProtocollo")
            myriga(5) = myPOSTreader.Item("AnnoProtocollo")

            Dim dC As New Cls_CausaleContabile

            dC.Leggi(Session("DC_TABELLE"), campodb(myPOSTreader.Item("CausaleContabile")))
            myriga(6) = dC.Descrizione

            myriga(7) = campodb(myPOSTreader.Item("Descrizione"))

            Dim AppoSql As String
            Dim Importo As Double = 0
            Dim VettoreColonne(1000) As Double
            Dim Indice As Integer = 0
            AppoSql = "Select Colonna,SUM(IMPORTO) FROM LegameBudgetRegistrazione WHERE NumeroRegistrazione = " & Val(campodb(myPOSTreader.Item("NumeroRegistrazione"))) & " Group by Colonna"

            Dim CommandBdRegR As New OleDbCommand(AppoSql, cn)
            Dim myBdRegR As OleDbDataReader = CommandBdRegR.ExecuteReader()
            Do While myBdRegR.Read
                VettoreColonne(campodbn(myBdRegR.Item(0))) = campodbn(myBdRegR.Item(1))
            Loop
            myBdRegR.Close()
            Importo = 0
            For Indice = 0 To 1000
                Importo = Importo + VettoreColonne(Indice)
            Next
            If Importo = 0 Then
                AppoSql = "select MovimentiContabiliRiga.Importo,MovimentiContabiliRiga.DareAvere,RegoleBudget.Percentuale,RegoleBudget.ImportoFisso,RegoleBudget.Colonna,RegoleBudget.CentroServizio AS CENTROSERVIZIO,(SELECT TOP 1 CentroServizio FROM MovimentiContabiliTesta WHERE MovimentiContabiliRiga.Numero = MovimentiContabiliTesta.NumeroRegistrazione) AS MovTCentroServizio from MovimentiContabiliRiga inner join RegoleBudget on MastroPartita = MastroPianoDeiConti and ContoPartita = ContoPianoDeiConti and SottocontoPartita = SottoContoPianoDeiConti where numero = ? And RegoleBudget.Anno = " & Year(myPOSTreader.Item("DataRegistrazione"))

                Dim CmdR As New OleDbCommand(AppoSql, cn)
                CmdR.Parameters.AddWithValue("@NumeroRegistrazione", Val(campodb(myPOSTreader.Item("NumeroRegistrazione"))))
                Dim ReadR As OleDbDataReader = CmdR.ExecuteReader()
                Do While ReadR.Read
                    If campodb(ReadR.Item("CentroServizio")) = "****" Or campodb(ReadR.Item("CentroServizio")) = campodb(ReadR.Item("MovTCentroServizio")) Then
                        If dC.VenditaAcquisti = "A" Then
                            If campodb(ReadR.Item("DareAVere")) = "D" Then
                                VettoreColonne(campodbn(ReadR.Item("Colonna"))) = VettoreColonne(campodbn(ReadR.Item("Colonna"))) + Math.Round(campodbn(ReadR.Item("Importo")) * campodbn(ReadR.Item("Percentuale")) / 100, 2)
                                VettoreColonne(campodbn(ReadR.Item("Colonna"))) = VettoreColonne(campodbn(ReadR.Item("Colonna"))) + campodbn(ReadR.Item("ImportoFisso"))
                            Else
                                VettoreColonne(campodbn(ReadR.Item("Colonna"))) = VettoreColonne(campodbn(ReadR.Item("Colonna"))) - Math.Round(campodbn(ReadR.Item("Importo")) * campodbn(ReadR.Item("Percentuale")) / 100, 2)
                                VettoreColonne(campodbn(ReadR.Item("Colonna"))) = VettoreColonne(campodbn(ReadR.Item("Colonna"))) - campodbn(ReadR.Item("ImportoFisso"))
                            End If
                        Else
                            If campodb(ReadR.Item("DareAVere")) = "A" Then
                                VettoreColonne(campodbn(ReadR.Item("Colonna"))) = VettoreColonne(campodbn(ReadR.Item("Colonna"))) + Math.Round(campodbn(ReadR.Item("Importo")) * campodbn(ReadR.Item("Percentuale")) / 100, 2)
                                VettoreColonne(campodbn(ReadR.Item("Colonna"))) = VettoreColonne(campodbn(ReadR.Item("Colonna"))) + campodbn(ReadR.Item("ImportoFisso"))
                            Else
                                VettoreColonne(campodbn(ReadR.Item("Colonna"))) = VettoreColonne(campodbn(ReadR.Item("Colonna"))) - Math.Round(campodbn(ReadR.Item("Importo")) * campodbn(ReadR.Item("Percentuale")) / 100, 2)
                                VettoreColonne(campodbn(ReadR.Item("Colonna"))) = VettoreColonne(campodbn(ReadR.Item("Colonna"))) - campodbn(ReadR.Item("ImportoFisso"))
                            End If
                        End If
                    End If
                Loop
                ReadR.Close()
            End If
            If Chk_Colonne.Checked = False Then
                Importo = 0
                For Indice = 0 To 1000
                    Importo = Importo + VettoreColonne(Indice)
                Next
                myriga(8) = Format(Importo, "#,##0.00")
                Indice = 8
            Else
                myriga(8) = Format(VettoreColonne(0), "#,##0.00")

                Indice = 8
                Dim SqlColonne As String
                SqlColonne = "Select * FROM ColonneBudget WHERE Anno = " & Year(Txt_DataAl.Text) & " Order by Livello1"

                Dim CmdColonne As New OleDbCommand(SqlColonne, cn)
                Dim ReadColonne As OleDbDataReader = CmdColonne.ExecuteReader()
                Do While ReadColonne.Read
                    Indice = Indice + 1
                    myriga(Indice) = Format(VettoreColonne(campodbn(ReadColonne.Item("Livello1"))), "#,##0.00")
                Loop
                ReadColonne.Close()
            End If

            Dim MovR As New Cls_MovimentoContabile

            MovR.Leggi(Session("DC_GENERALE"), campodbn(myPOSTreader.Item("NumeroRegistrazione")))

            Indice = Indice + 1
            myriga(Indice) = Format(MovR.ImportoDocumento(Session("DC_TABELLE")), "#,##0.00")

            Dim Riga As Integer = 0
            Importo = 0
            For Riga = 0 To 100
                If Not IsNothing(MovR.Righe(Riga)) Then
                    If MovR.Righe(Riga).Tipo = "" Then
                        Dim kConto As New Cls_Pianodeiconti

                        kConto.Mastro = MovR.Righe(Riga).MastroPartita
                        kConto.Conto = MovR.Righe(Riga).ContoPartita
                        kConto.Sottoconto = MovR.Righe(Riga).SottocontoPartita

                        kConto.Decodfica(Session("DC_GENERALE"))

                        If kConto.Tipo = "C" Or kConto.Tipo = "R" Then
                            Importo = Importo + MovR.Righe(Riga).Importo
                        End If
                    End If
                End If
            Next
            Indice = Indice + 1
            myriga(Indice) = Format(Importo, "#,##0.00")

            Tabella.Rows.Add(myriga)

        Loop
        myPOSTreader.Close()
        cn.Close()

        If ToExcel = True Then
            Session("AppoggioExcel") = Tabella
        Else
            Grid.AutoGenerateColumns = True
            Grid.DataSource = Tabella
            Grid.DataBind()



        End If



    End Sub

    Function campodb(ByVal oggetto As Object) As String
        If IsDBNull(oggetto) Then
            Return ""
        Else
            Return oggetto
        End If
    End Function

    Function campodbn(ByVal oggetto As Object) As Double
        If IsDBNull(oggetto) Then
            Return 0
        Else
            Return oggetto
        End If
    End Function



    Private Function SplitWords(ByVal s As String) As String()
        '
        ' Call Regex.Split function from the imported namespace.
        ' Return the result array.
        '
        Return Regex.Split(s, "\W+")
    End Function

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Trim(Session("UTENTE")) = "" Then
            If Request.ServerVariables("URL").ToUpper.IndexOf("SENIORWEBBLANCO") > 0 Then
                Response.Redirect("/ODV/Login.aspx")
                Exit Sub
            Else
                Response.Redirect("/SeniorWeb/Login.aspx")
                Exit Sub
            End If
        End If

        Call EseguiJS()

        If Page.IsPostBack = True Then Exit Sub

        Dim K1 As New Cls_SqlString

        Dim Barra As New Cls_BarraSenior

        If Not IsNothing(Session("RicercaAnagraficaSQLString")) Then
            Try
                K1 = Session("RicercaAnagraficaSQLString")
            Catch ex As Exception

            End Try
        End If

        Lbl_BarraSenior.Text = Barra.CodiceBarra(Application("SENIOR"), Session("UTENTE"), Page, K1)



        Dim k As New Cls_CausaleContabile

        Dim ConnectionString As String = Session("DC_TABELLE")

        k.UpDateDropBox(ConnectionString, DD_CausaleContabile)

        Txt_DataDal.Text = Format(DateSerial(Year(Now), Month(Now), 1), "dd/MM/yyyy")
        Txt_DataAl.Text = Format(DateSerial(Year(Now), 12, 31), "dd/MM/yyyy")

        EseguiJS()

    End Sub

    Private Sub EseguiJS()
        Dim MyJs As String
        MyJs = "$(document).ready(function() {"

        MyJs = MyJs & "var els = document.getElementsByTagName(""*"");"

        MyJs = MyJs & "for (var i=0;i<els.length;i++)"
        MyJs = MyJs & "if ( els[i].id ) { "
        MyJs = MyJs & " var appoggio =els[i].id; "
        MyJs = MyJs & " if (appoggio.match('Txt_Sottoconto')!= null) {  "
        MyJs = MyJs & " $(els[i]).autocomplete('/WebHandler/GestioneConto.ashx?Utente=" & Session("UTENTE") & "', {delay:5,minChars:3});"
        MyJs = MyJs & "    }"


        MyJs = MyJs & " if (appoggio.match('Txt_Data')!= null) {  "
        MyJs = MyJs & " $(els[i]).mask(""99/99/9999"");  "
        MyJs = MyJs & " $(els[i]).datepicker({ changeMonth: true,changeYear: true,  yearRange: ""1890:" & Year(Now) + 1 & """},$.datepicker.regional[""it""]);"
        MyJs = MyJs & "    }"

        MyJs = MyJs & " if (appoggio.match('Txt_Importo')!= null)  {  "
        MyJs = MyJs & " $(els[i]).keypress(function() { ForceNumericInput($(this).val(), true, true); } ); "
        MyJs = MyJs & " $(els[i]).blur(function() { var ap = formatNumber($(this).val(),2); $(this).val(ap); });"
        MyJs = MyJs & "    }"

        MyJs = MyJs & "$("".chosen-select"").chosen({"
        MyJs = MyJs & "no_results_text: ""Causale non trovata"","
        MyJs = MyJs & "display_disabled_options: true,"
        'MyJs = MyJs & "allow_single_deselect: true,"
        MyJs = MyJs & "width: ""350px"","
        MyJs = MyJs & "placeholder_text_single: ""Seleziona Causale"""
        MyJs = MyJs & "});"


        MyJs = MyJs & "} "
        MyJs = MyJs & "});"

        'MyJs = "$(document).ready(function() { $('.myClass').keypress(function() { handleEnter($('.myClass').val(), true, true); } );     $('#" & Txt_ImportoPagato.ClientID & "').blur(function() { var ap = formatNumber ($('#" & Txt_ImportoPagato.ClientID & "').val(),2); $('#" & Txt_ImportoPagato.ClientID & "').val(ap); }); });"
        ScriptManager.RegisterStartupScript(Me, Me.GetType(), "visualizzaRitIm", MyJs, True)
    End Sub


    Protected Sub ImageButton1_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles ImageButton1.Click
        Call Esegui_Ricerca(False)
    End Sub

    Protected Sub ImageButton2_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles ImageButton2.Click

        Call Esegui_Ricerca(True)

        Response.Redirect("RicercaExcel.aspx")

    End Sub

    Protected Sub Btn_Esci_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles Btn_Esci.Click


        Response.Redirect("Menu_Budget.aspx")

    End Sub



    Protected Sub Txt_Anno_TextChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles Txt_Anno.TextChanged
        If Val(Txt_Anno.Text) > 0 Then
            Dim MyJs1 As String

            MyJs1 = "$(document).ready(function() { $(" & Chr(34) & "#" & Txt_Livello.ClientID & Chr(34) & ").autocomplete('autocompletecontobudget.ashx?Anno=" & Val(Txt_Anno.Text) & "&Utente=" & Session("UTENTE") & "', {delay:5,minChars:3}); });"
            ScriptManager.RegisterStartupScript(Me, Me.GetType(), "autocompleteCONTO", MyJs1, True)
        End If
    End Sub
End Class
