﻿Imports System
Imports System.Web
Imports System.Data.OleDb
Imports System.Configuration
Imports System.Text
Imports System.Web.Hosting

Partial Class GeneraleWeb_GruppoRegistriTesta
    Inherits System.Web.UI.Page
    Dim MyTable As New System.Data.DataTable("tabella")
    Dim MyDataSet As New System.Data.DataSet()


    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load


        If Trim(Session("UTENTE")) = "" Then

            Response.Redirect("/SeniorWeb/Login.aspx")
            Exit Sub
        End If


        If Page.IsPostBack = True Then Exit Sub


        Dim K1 As New Cls_SqlString

        Dim Barra As New Cls_BarraSenior

        If Not IsNothing(Session("RicercaAnagraficaSQLString")) Then
            Try
                K1 = Session("RicercaAnagraficaSQLString")
            Catch ex As Exception

            End Try
        End If

        Lbl_BarraSenior.Text = Barra.CodiceBarra(Application("SENIOR"), Session("UTENTE"), Page, K1)




        'Dim M As New Cls_RegistroIVA

        'M.UpDateDropBox(Session("DC_TABELLE"), DD_RegistroIVA1)
        

        MyTable.Clear()
        MyTable.Columns.Clear()
        MyTable.Columns.Add("Registro", GetType(String))


        Dim cn As New OleDbConnection


        Dim ConnectionString As String = Session("DC_TABELLE")


        If Request.Item("Codice") <> "" Then
            cn = New Data.OleDb.OleDbConnection(ConnectionString)

            cn.Open()

            Dim cmd As New OleDbCommand

            cmd.CommandText = ("select * from GruppoRegistriTesta  " & _
                                   "  Where Codice = ? ")
            cmd.Connection = cn
            cmd.Parameters.AddWithValue("@Codice", Request.Item("Codice"))
            Dim myPOSTreader As OleDbDataReader = cmd.ExecuteReader()
            If myPOSTreader.Read Then
                Txt_Codice.Text = Request.Item("Codice")
                Txt_Descrizione.Text = campodb(myPOSTreader.Item("Descrizione"))
            End If
            myPOSTreader.Close()




            Dim cmdR As New OleDbCommand

            cmdR.CommandText = ("select * from GruppoRegistriRiga  " & _
                                   "  Where Codice = ? ")
            cmdR.Connection = cn
            cmdR.Parameters.AddWithValue("@Codice", Request.Item("Codice"))
            Dim RdRighe As OleDbDataReader = cmdR.ExecuteReader()
            Do While RdRighe.Read
                Dim myriga As System.Data.DataRow = MyTable.NewRow()
                myriga(0) = campodbn(RdRighe.Item("Registro"))
          
                MyTable.Rows.Add(myriga)
            Loop
            RdRighe.Close()



            cn.Close()

            Txt_Codice.Enabled = False

        Else
            Dim MaxRegistrazione As New Cls_Gruppi
            Txt_Codice.Text = MaxRegistrazione.MaxGruppo(ConnectionString)
        End If

        If MyTable.Rows.Count < 1 Then
            Dim myriga As System.Data.DataRow = MyTable.NewRow()
            myriga(0) = 0

            MyTable.Rows.Add(myriga)
        End If


        ViewState("App_Retta") = MyTable

        Grid.AutoGenerateColumns = False
        Grid.DataSource = MyTable
        Grid.DataBind()


    End Sub

    Function campodb(ByVal oggetto As Object) As String
        If IsDBNull(oggetto) Then
            Return ""
        Else
            Return oggetto
        End If
    End Function
    Function campodbn(ByVal oggetto As Object) As Double
        If IsDBNull(oggetto) Then
            Return 0
        Else
            Return oggetto
        End If
    End Function

    Protected Sub Grid_RowCommand(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewCommandEventArgs) Handles Grid.RowCommand
        If (e.CommandName = "Inserisci") Then
            Call InserisciRiga()
        End If
    End Sub

    Protected Sub Grid_RowDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs) Handles Grid.RowDataBound
        If e.Row.RowType = DataControlRowType.DataRow Then




            Dim DD_RegistroIVA As DropDownList = DirectCast(e.Row.FindControl("DD_RegistroIVA"), DropDownList)

            Dim M As New Cls_RegistroIVA

            M.UpDateDropBox(Session("DC_TABELLE"), DD_RegistroIVA)
            DD_RegistroIVA.SelectedValue = MyTable.Rows(e.Row.RowIndex).Item(0).ToString

        End If
    End Sub


    Private Sub UpDateTable()



        Dim i As Integer

        MyTable.Clear()
        MyTable.Columns.Clear()
        MyTable.Columns.Add("Registro", GetType(String))
    

        For i = 0 To Grid.Rows.Count - 1



            Dim DD_RegistroIVA As DropDownList = DirectCast(Grid.Rows(i).FindControl("DD_RegistroIVA"), DropDownList)
            Dim myriga As System.Data.DataRow = MyTable.NewRow()
            myriga(0) = DD_RegistroIVA.SelectedValue
            MyTable.Rows.Add(myriga)
        Next
        ViewState("App_Retta") = MyTable
    End Sub

    Private Sub InserisciRiga()
        UpDateTable()
        MyTable = ViewState("App_Retta")
        Dim myriga As System.Data.DataRow = MyTable.NewRow()
        myriga(0) = 0
        
        MyTable.Rows.Add(myriga)

        ViewState("App_Retta") = MyTable
        Grid.AutoGenerateColumns = False

        Grid.DataSource = MyTable
        Grid.DataBind()



    End Sub

    Protected Sub Grid_RowDeleted(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewDeletedEventArgs) Handles Grid.RowDeleted

    End Sub

    Protected Sub Grid_RowDeleting(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewDeleteEventArgs) Handles Grid.RowDeleting
        UpDateTable()
        MyTable = ViewState("App_Retta")
        Try
            If Request.UserAgent.Contains("Chrome") Then
                If IsDBNull(Session("RI_TIMER")) Or DateDiff("s", Session("RI_TIMER"), Now) > 1 Then
                    MyTable.Rows.RemoveAt(e.RowIndex)
                    If MyTable.Rows.Count = 0 Then
                        Dim myriga As System.Data.DataRow = MyTable.NewRow()
                        myriga(0) = 0
                        
                        MyTable.Rows.Add(myriga)
                    End If
                    Session("RI_TIMER") = Now
                End If
            Else
                MyTable.Rows.RemoveAt(e.RowIndex)
                If MyTable.Rows.Count = 0 Then
                    Dim myriga As System.Data.DataRow = MyTable.NewRow()
                    myriga(0) = 0
                    
                    MyTable.Rows.Add(myriga)
                End If
            End If
        Catch ex As Exception

        End Try


        ViewState("App_Retta") = MyTable

        Grid.AutoGenerateColumns = False

        Grid.DataSource = MyTable
        Grid.DataBind()

        e.Cancel = True
    End Sub

    Protected Sub Btn_Modifica_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles Btn_Modifica.Click
        If Txt_Codice.Text = "" Then
            ClientScript.RegisterClientScriptBlock(Me.GetType(), "Errore", "VisualizzaErrore('Specificare codice');", True)

            REM Lbl_Errori.Text = "Specificare codice"
            Exit Sub
        End If
        If Txt_Descrizione.Text = "" Then
            ClientScript.RegisterClientScriptBlock(Me.GetType(), "Errore", "VisualizzaErrore('Specificare la descrizione');", True)

            REM Lbl_Errori.Text = "Specificare descrizione"
            Exit Sub
        End If

        UpDateTable()




        Dim cn As New OleDbConnection


        Dim ConnectionString As String = Session("DC_TABELLE")


        cn = New Data.OleDb.OleDbConnection(ConnectionString)

        cn.Open()

        Dim cmd As New OleDbCommand

        cmd.CommandText = ("DELETE from GruppoRegistriTesta  " & _
                               "  Where Codice = ? ")
        cmd.Connection = cn
        cmd.Parameters.AddWithValue("@Codice", Txt_Codice.Text)
        cmd.ExecuteNonQuery()


        Dim cmdRiga As New OleDbCommand

        cmdRiga.CommandText = ("DELETE from GruppoRegistriRiga  " & _
                               "  Where Codice = ? ")
        cmdRiga.Connection = cn
        cmdRiga.Parameters.AddWithValue("@Codice", Txt_Codice.Text)
        cmdRiga.ExecuteNonQuery()



        Dim cmdInserimentoTesta As New OleDbCommand

        cmdInserimentoTesta.CommandText = ("INSERT INTO GruppoRegistriTesta  (CODICE,DESCRIZIONE) VALUES (?,?) ")
        cmdInserimentoTesta.Connection = cn
        cmdInserimentoTesta.Parameters.AddWithValue("@Codice", Txt_Codice.Text)
        cmdInserimentoTesta.Parameters.AddWithValue("@Descrizione", Txt_Descrizione.Text)
        cmdInserimentoTesta.ExecuteNonQuery()

        For i = 0 To Grid.Rows.Count - 1
            Dim cmdInserimentoRiga As New OleDbCommand

            cmdInserimentoRiga.CommandText = ("INSERT INTO GruppoRegistriRiga  (CODICE,REGISTRO) VALUES (?,?) ")
            cmdInserimentoRiga.Connection = cn
            cmdInserimentoRiga.Parameters.AddWithValue("@Codice", Txt_Codice.Text)
            cmdInserimentoRiga.Parameters.AddWithValue("@REGISTRO", MyTable.Rows(i).Item(0))
            cmdInserimentoRiga.ExecuteNonQuery()


        Next



        cn.Close()

        Response.Redirect("ElencoGruppiLiquidazione.aspx")

    End Sub

    Protected Sub Btn_Esci_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles Btn_Esci.Click
        Response.Redirect("ElencoGruppiLiquidazione.aspx")
    End Sub


    Protected Sub Txt_Codice_TextChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles Txt_Codice.TextChanged
        If Txt_Codice.Enabled = True Then
            Img_VerificaCodice.ImageUrl = "~/images/corretto.gif"

            Dim x As New Cls_Gruppi

            x.Codice = Txt_Codice.Text
            x.Leggi(Session("DC_TABELLE"))

            If x.Descrizione <> "" Then
                Img_VerificaCodice.ImageUrl = "~/images/errore.gif"
            End If
        End If
    End Sub

    Protected Sub Txt_Descrizione_TextChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles Txt_Descrizione.TextChanged
        If Txt_Codice.Enabled = True Then
            Img_VerificaDescrizione.ImageUrl = "~/images/corretto.gif"

            Dim x As New Cls_Gruppi

            x.Codice = ""
            x.Descrizione = Txt_Descrizione.Text.Trim
            x.LeggiDescrizione(Session("DC_TABELLE"))

            If x.Codice <> "" Then
                Img_VerificaDescrizione.ImageUrl = "~/images/errore.gif"
            End If
        End If
    End Sub
End Class
