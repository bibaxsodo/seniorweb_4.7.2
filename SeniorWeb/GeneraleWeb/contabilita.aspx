﻿<%@ Page Language="VB" AutoEventWireup="false" Inherits="GeneraleWeb_contabilita" CodeFile="contabilita.aspx.vb" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
    <title>Generale Web</title>
    <asp:PlaceHolder runat="server">
        <%: System.Web.Optimization.Styles.Render("~/Content/AjaxControlToolkit/Styles/Bundle") %>
    </asp:PlaceHolder>
    <link rel="stylesheet" href="js/jquery.mobile-1.0b1.min.css" />
    <script type="text/javascript" src="js/jquery-1.6.1.min.js"></script>
    <script type="text/javascript" src="js/jquery.mobile-1.0b1.min.js"></script>
    <style>
        <!--
        .commentato {
            position: relative;
            color: black;
            text-decoration: none;
        }

            .commentato div {
                position: absolute;
                visibility: hidden;
                background-color: #c8ffb4;
                border: dotted #c8ffb4 1px;
                color: black;
                text-decoration: none;
                padding: 3px;
                top: 20;
                left: 40;
            }

            .commentato:hover div {
                visibility: visible;
                index-z: 100;
            }

        // -->
    </style>
    <script type="text/javascript">
        var appoggio = 0;
        var indice = 25;
        var Timer = 0;
        var Timer1 = 0;

        function CaricaAltroFrame() {
            var winW = 630, winH = 460;
            if (document.body && document.body.offsetWidth) {
                winW = document.body.offsetWidth;
                winH = document.body.offsetHeight;
            }
            if (document.compatMode == 'CSS1Compat' &&
                document.documentElement &&
                document.documentElement.offsetWidth) {
                winW = document.documentElement.offsetWidth;
                winH = document.documentElement.offsetHeight;
            }
            if (window.innerWidth && window.innerHeight) {
                winW = window.innerWidth;
                winH = window.innerHeight;
            }

            if (document.getElementById("FRAME2") == null) {
                document.getElementById("output").width = '80%';
                $('#<%= lbl_container.ClientID %>').html($('#<%= lbl_container.ClientID %>').html() + '<iframe width="19%" style="border-width:0;" height="' + (winH - 20) + 'px" marginheight="0" marginwidth="0" src="EasyViewer.aspx" id="FRAME2"  ></iframe>');
     }
     else {
         document.getElementById("output").width = '100%';
         $('#<%= lbl_container.ClientID %>').html('<%= lbl_container.text %>');
                $("#output").height(winH - 20);

                var r1 = document.getElementById("FRAME2");
                r1.removeAttribute('style');
                r1.removeAttribute('innerHTML');
                r1.removeAttribute('id');
            }
        }


        (function ($, undefined) {
            $.mobile.currentMenu = '';
            $.widget("mobile.dualColumn", $.mobile.widget, {
                _create: function () {
                    if ($('html').hasClass('min-width-768px')) { //if there's enough place
                        var M = function (e) { //builds the menu everywhere
                            var $t = $(this), $n = $t.find(":jqmData(role='menu')");

                            $t.children('.ui-header,.ui-footer').width('auto');
                            var c = $t.children().eq(0);

                            if ($n.length) {
                                $.mobile.currentMenu = $n;
                                $n.menu();
                            } else {
                                if ($.mobile.currentMenu.length) c.before($.mobile.currentMenu = $.mobile.currentMenu.clone());
                            }
                        }
                        //builds it the first time. It must be bound to a good event so that it's not needed. Waiting for the next jqm release
                        M.apply(this.element);
                        $('div').live('pagebeforeshow', M);

                    }
                }
            });

            $.widget("mobile.menu", $.mobile.widget, {
                options: {
                    width: "20%"
                },
                _create: function () {
                    var $h = $('html'),
                        $m = this.element,
                        $p = $m.parents(":jqmData(role='page')"),
                        o = this.options;
                    if ($m.is(":jqmData(role='menu')")) { //if there's enough place
                        if ($m.hasClass('ui-menu')) { //if it's a menu I reproduced
                            return;
                        }
                        $p.children('.ui-header,.ui-footer').width('auto');
                        var c = $p.children().eq(0);

                        c.before($m);
                        //$m.css({"width":o.width,"height":"100%","float":"left"}).addClass('ui-menu ui-content').page();	      
                        $m.css({ "width": o.width, "height": "100%", "float": "left" }).addClass('ui-menu ui-content').page();

                    }
                }
            });

            $(function () { $("[data-role='page']:first").dualColumn(); });
        })(jQuery);

        $(document).ready(function () {
            if ($("#output").length != 0) {
                $("#output").height(document.body.scrollHeight - 40);
            }
        });

        function scompare() {
            if (indice < 0) {
                return;
            }

            if (indice < 2) {
                indice = indice - 1;
            } else {
                indice = indice - 2;
            }

            stappo = indice + '%';
            $('div.ui-menu.ui-content').css({ "width": stappo, "height": "100%", "float": "left" }).addClass('ui-menu ui-content').page();

            stappo = indice + 1.6 + '%';
            $('#myresize').css({ position: 'absolute', left: stappo });

            Timer = setTimeout("scompare()", 10);
        }


        function appare() {
            if (indice > 24) {
                return;
            }
            if (indice > 23) {
                indice = indice + 1;
            } else {
                indice = indice + 2;
            }

            stappo = indice + '%';
            $('div.ui-menu.ui-content').css({ "width": stappo, "height": "100%", "float": "left" }).addClass('ui-menu ui-content').page();

            stappo = indice + 1.6 + '%';
            $('#myresize').css({ position: 'absolute', left: stappo });
            Timer1 = setTimeout("appare()", 10);
        }

        function sauro() {
            if (appoggio == 0) {

                Timer = setTimeout("scompare()", 10);
                appoggio = 1;
            } else {
                appoggio = 0;
                Timer1 = setTimeout("appare()", 10);
            }

        }
    </script>

</head>
<body>
    <form id="form1" runat="server">
        <asp:ScriptManager ID="ScriptManager1" runat="server" EnableScriptGlobalization="true">
            <Scripts>
                <asp:ScriptReference Path="~/Scripts/AjaxControlToolkit/Bundle" />
            </Scripts>
        </asp:ScriptManager>
        <div data-role="page">
            <div data-role="menu" data-width="25%">
                <div data-role="header" data-nobackbtn="true">
                    <span class="ui-li-count"></span>
                    <h1>Senior Contabilità Web</h1>
                </div>
                <!-- /header -->
                <asp:Label ID="Label1" runat="server" Text="Label"></asp:Label>
                <div data-role="footer">
                    <span class="ui-li-count"></span>
                    <h1>Senior Contabilità Web</h1>
                </div>
            </div>
            <!-- /menu -->
            <div id="myresize" style="position: absolute; left: 26.7%; top: 20px;">
                <a href="#" onclick="sauro();">
                    <img alt="ridimensiona" src="..\images\resize.png" /></a>
            </div>
            <div data-role="content">
                <asp:Label ID="Lbl_Registrazione" runat="server" Text=""></asp:Label>
                <asp:Label ID="lbl_container" runat="server" Text="Label"></asp:Label>
            </div>
            <!-- /content -->
        </div>
    </form>
</body>
</html>
