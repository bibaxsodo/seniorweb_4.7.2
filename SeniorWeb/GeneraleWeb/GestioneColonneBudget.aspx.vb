﻿
Partial Class GeneraleWeb_GestioneColonneBudget
    Inherits System.Web.UI.Page

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Trim(Session("UTENTE")) = "" Then
            If Request.ServerVariables("URL").ToUpper.IndexOf("SENIORWEBBLANCO") > 0 Then
                Response.Redirect("/ODV/Login.aspx")
                Exit Sub
            Else
                Response.Redirect("/SeniorWeb/Login.aspx")
                Exit Sub
            End If
        End If


        If Page.IsPostBack = True Then Exit Sub

        Dim K1 As New Cls_SqlString

        Dim Barra As New Cls_BarraSenior

        If Not IsNothing(Session("RicercaGeneraleSQLString")) Then
            K1 = Session("RicercaGeneraleSQLString")
        End If

        Lbl_BarraSenior.Text = Barra.CodiceBarra(Application("SENIOR"), Session("UTENTE"), Page, K1)

        If Val(Request.Item("Anno")) > 0 Then
            Txt_Anno.Text = Val(Request.Item("Anno"))
            TxT_Livello1.Text = Val(Request.Item("Livello"))
            Txt_Anno.Enabled = False
            TxT_Livello1.Enabled = False

            Dim x As New Cls_colonnebudget

            x.Anno = Txt_Anno.Text
            x.Livello1 = TxT_Livello1.Text
            x.Leggi(Session("DC_GENERALE"))
            Txt_Descrizione.Text = x.Descrizione

        Else
            Txt_Anno.Text = Year(Now)
            TxT_Livello1.Text = 0
        End If


    End Sub

    Private Sub Pulisci()
        TxT_Livello1.Text = "0"
        Txt_Descrizione.Text = ""
        Txt_Anno.Text = Year(Now)


    End Sub

    Protected Sub Btn_Modifica_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles Btn_Modifica.Click
        Dim x As New Cls_colonnebudget

        If Val(Txt_Anno.Text) = 0 Then
            ClientScript.RegisterClientScriptBlock(Me.GetType(), "Errore", "VisualizzaErrore('Digitare Anno');", True)
            Exit Sub
        End If
        If Val(TxT_Livello1.Text) = 0 Then
            ClientScript.RegisterClientScriptBlock(Me.GetType(), "Errore", "VisualizzaErrore('Digitare Colonna');", True)
            Exit Sub
        End If


        x.Anno = Txt_Anno.Text
        x.Livello1 = TxT_Livello1.Text
        x.Leggi(Session("DC_GENERALE"))
        If x.Descrizione <> "" And Val(Request.Item("Livello")) = 0 Then
            ClientScript.RegisterClientScriptBlock(Me.GetType(), "Errore", "VisualizzaErrore('Colonna già presente');", True)
            Exit Sub
        End If

        x.Livello1 = TxT_Livello1.Text
        x.Descrizione = Txt_Descrizione.Text
        x.Scrivi(Session("DC_GENERALE"))

        Response.Redirect("ElencoColonneBudget.aspx")
    End Sub

    

    Protected Sub Btn_Elimina_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles Btn_Elimina.Click
        Dim x As New Cls_colonnebudget

        x.Anno = Txt_Anno.Text
        x.Livello1 = TxT_Livello1.Text
        x.Elimina(Session("DC_GENERALE"))

        Response.Redirect("ElencoColonneBudget.aspx")
    End Sub

    


    Protected Sub Btn_Esci_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles Btn_Esci.Click
        Response.Redirect("ElencoColonneBudget.aspx")
    End Sub
End Class
