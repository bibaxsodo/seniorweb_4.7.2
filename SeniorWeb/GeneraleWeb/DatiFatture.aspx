﻿<%@ Page Language="VB" AutoEventWireup="false" Inherits="GeneraleWeb_DatiFatture" CodeFile="DatiFatture.aspx.vb" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="xsau" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
    <meta http-equiv="x-ua-compatible" content="IE=9" />
    <title>Spesometro</title>
    <asp:PlaceHolder runat="server">
        <%: System.Web.Optimization.Styles.Render("~/Content/AjaxControlToolkit/Styles/Bundle") %>
    </asp:PlaceHolder>
    <link href="ospiti.css?ver=1" rel="stylesheet" type="text/css" />
    <link rel="shortcut icon" href="images/SENIOR.ico" />
    <link href="js/jquery.autocomplete.css" rel="stylesheet" type="text/css" />

    <script src="js/jquery-1.5.1.min.js" type="text/javascript"></script>
    <script src="js/jquery.autocomplete.js" type="text/javascript"></script>
    <script src="js/soapclient.js" type="text/javascript"></script>
    <script src="/js/convertinumero.js" type="text/javascript"></script>

    <script src="js/jquery.maskedinput-1.3.min.js" type="text/javascript"></script>
    <script src="/js/formatnumer.js" type="text/javascript"></script>

    <script src="/js/pianoconti.js" type="text/javascript"></script>
    <script src="js/JSErrore.js" type="text/javascript"></script>
    <link rel="stylesheet" href="dialogbox/redips-dialog.css" type="text/css" media="screen" />
    <script type="text/javascript" src="dialogbox/redips-dialog-min.js"></script>
    <script type="text/javascript" src="dialogbox/script.js"></script>
    <link rel="stylesheet" href="jqueryui/jquery-ui.css" type="text/css" />
    <script src="jqueryui/jquery-ui.js" type="text/javascript"></script>
    <script src="jqueryui/datapicker-it.js" type="text/javascript"></script>

    <style>
        th {
            font-weight: normal;
        }

        .tabella {
            border: 1px solid #ccc;
            border-collapse: collapse;
            margin: 0;
            padding: 0;
            width: 100%;
            table-layout: fixed;
        }

        .miotr {
            background: #f8f8f8;
            border: 1px solid #ddd;
            padding: .35em;
        }

        .miacella {
            padding: .625em;
            text-align: center;
        }

        .miaintestazione {
            padding: .625em;
            text-align: center;
            font-size: .85em;
            letter-spacing: .1em;
            text-transform: uppercase;
        }
    </style>
    <script type="text/javascript">
        $(document).ready(function () {
            if (window.innerHeight > 0) { $("#BarraLaterale").css("height", (window.innerHeight - 105) + "px"); } else { $("#BarraLaterale").css("height", (document.documentElement.offsetHeight - 105) + "px"); }
        });
    </script>
</head>
<body>
    <form id="form1" runat="server" autocomplete="off">
        <asp:ScriptManager ID="ScriptManager1" runat="server" EnableScriptGlobalization="true">
            <Scripts>
                <asp:ScriptReference Path="~/Scripts/AjaxControlToolkit/Bundle" />
            </Scripts>
        </asp:ScriptManager>
        <asp:Label ID="Lbl_BarraSenior" runat="server" Text=""></asp:Label>
        <div align="left">
            <table style="width: 100%;" cellpadding="0" cellspacing="0">
                <tr>
                    <td style="width: 160px; background-color: #F0F0F0;"></td>
                    <td>
                        <div class="Titolo">
                            Contabilità - Servizi - Dati Fatture<br />
                            <br />
                        </div>
                    </td>

                    <td style="text-align: right; vertical-align: top;">
                        <div class="DivTasti">
                            <asp:ImageButton ID="ImageButton1" runat="server" Height="38px" ImageUrl="~/images/elabora.png" class="EffettoBottoniTondi" Width="38px" />
                            <asp:ImageButton ID="Img_Download" runat="server" Height="38px" ImageUrl="~/images/download.png" class="EffettoBottoniTondi" Width="38px" />
                            <asp:ImageButton ID="Img_Excel" runat="server" Height="38px" ImageUrl="~/images/excel.png" class="EffettoBottoniTondi" Width="38px" />
                        </div>
                    </td>
                </tr>
            </table>

            <table style="width: 100%;" cellpadding="0" cellspacing="0">
                <tr>
                    <td style="width: 160px; vertical-align: top; background-color: #F0F0F0; text-align: center;" id="BarraLaterale">
                        <asp:ImageButton ID="ImgMenu" ImageUrl="images/Home.jpg" Width="112px" alt="Menù" class="Effetto" runat="server" /><br />
                        <asp:ImageButton ID="Btn_Esci" runat="server" BackColor="Transparent" ImageUrl="../images/Menu_Indietro.png" class="Effetto" ToolTip="Chiudi" />
                        <br />
                        <br />
                        <br />

                    </td>
                    <td colspan="2" style="background-color: #FFFFFF; vertical-align: top;">
                        <xsau:TabContainer ID="TabContainer1" runat="server" ActiveTabIndex="0" CssClass="TabSenior"
                            Width="100%" BorderStyle="None" Style="margin-right: 39px">
                            <xsau:TabPanel runat="server" HeaderText="Prima Nota" ID="Tab_Anagrafica">
                                <HeaderTemplate>
                                    Dati Fatture
                                </HeaderTemplate>
                                <ContentTemplate>



                                    <br />

                                    <label class="LabelCampo">Data Al:</label>
                                    <asp:TextBox ID="Txt_DataDal" runat="server" Width="90px"></asp:TextBox>

                                    <br />
                                    <br />
                                    <label class="LabelCampo">Data Dal:</label>
                                    <asp:TextBox ID="Txt_DataAl" runat="server" Width="90px"></asp:TextBox>
                                    <br />
                                    <br />
                                    <label class="LabelCampo">Registro :</label>
                                    <asp:DropDownList ID="DD_Registro" runat="server"></asp:DropDownList>
                                    <br />
                                    <br />
                                    <label class="LabelCampo">Registro 2 (opzionale) :</label>
                                    <asp:DropDownList ID="DD_Registro2" runat="server"></asp:DropDownList>
                                    <br />
                                    <br />
                                    <label class="LabelCampo">Registro 3 (opzionale) :</label>
                                    <asp:DropDownList ID="DD_Registro3" runat="server"></asp:DropDownList>
                                    <br />
                                    <br />

                                    <label class="LabelCampo">Progressivo Invio:</label>
                                    <asp:TextBox ID="Txt_Progressivo" runat="server" Width="90px"></asp:TextBox>
                                    <br />
                                    <br />
                                    <label class="LabelCampo">Codice Fiscale :</label>
                                    <asp:TextBox ID="Txt_CodiceFiscale" runat="server" Width="150px"></asp:TextBox>
                                    <br />
                                    <br />
                                    <label class="LabelCampo">Ver. Partita IVA, web :</label>
                                    <asp:CheckBox ID="Chk_PartitaIva" runat="server" Text="" />
                                    <br />
                                    <br />
                                    <label class="LabelCampo">Escludi Comuni e Regioni:</label>
                                    <asp:CheckBox ID="Chk_EsclComuniRegioni" runat="server" Text="" />
                                    <br />
                                    <br />
                                    <label class="LabelCampo">Includi solo (Esterometro):</label>
                                    <asp:CheckBox ID="Chk_Esterometro" runat="server" Text="" />

                                    <asp:Label ID="Lbl_Errori" runat="server" Text="."></asp:Label>


                                </ContentTemplate>
                            </xsau:TabPanel>
                        </xsau:TabContainer>
                    </td>
                </tr>
            </table>
            <asp:GridView ID="GridExcel" runat="server">
            </asp:GridView>
        </div>
    </form>

</body>
</html>
