﻿<%@ Page Language="VB" AutoEventWireup="false" Inherits="GeneraleWeb_ImportXML" CodeFile="ImportXML.aspx.vb" %>



<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="xsau" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
    <meta http-equiv="x-ua-compatible" content="IE=9" />
    <title>Imp. Fattura XML</title>
    <asp:PlaceHolder runat="server">
        <%: System.Web.Optimization.Styles.Render("~/Content/AjaxControlToolkit/Styles/Bundle") %>
    </asp:PlaceHolder>
    <link href="ospiti.css?ver=11" rel="stylesheet" type="text/css" />
    <link rel="shortcut icon" href="images/SENIOR.ico" />
    <link href="js/jquery.autocomplete.css" rel="stylesheet" type="text/css" />

    <script src="js/jquery-1.5.1.min.js" type="text/javascript"></script>
    <script src="js/jquery.autocomplete.js?ver=4" type="text/javascript"></script>
    <script src="js/soapclient.js" type="text/javascript"></script>
    <script src="/js/convertinumero.js" type="text/javascript"></script>

    <script src="js/jquery.maskedinput-1.3.min.js" type="text/javascript"></script>
    <script src="/js/formatnumer.js" type="text/javascript"></script>

    <script src="/js/pianoconti.js" type="text/javascript"></script>
    <script src="js/JSErrore.js" type="text/javascript"></script>
    <script src="js/NoEnter.js" type="text/javascript"></script>
    <script src="js/chosen/chosen.jquery.js" type="text/javascript"></script>
    <script src="js/chosen/docsupport/prism.js" type="text/javascript" charset="utf-8"></script>


    <link rel="stylesheet" href="js/chosen/docsupport/style.css">
    <link rel="stylesheet" href="js/chosen/docsupport/prism.css">
    <link rel="stylesheet" href="js/chosen/chosen.css">

    <link rel="stylesheet" href="dialogbox/redips-dialog.css" type="text/css" media="screen" />
    <script type="text/javascript" src="dialogbox/redips-dialog-min.js"></script>
    <script type="text/javascript" src="dialogbox/script.js"></script>

    <link rel="stylesheet" href="jqueryui/jquery-ui.css" type="text/css" />
    <script src="jqueryui/jquery-ui.js" type="text/javascript"></script>
    <script src="jqueryui/datapicker-it.js" type="text/javascript"></script>

    <style>
        th {
            font-weight: normal;
        }

        .tabella {
            border: 1px solid #ccc;
            border-collapse: collapse;
            margin: 0;
            padding: 0;
            width: 100%;
            table-layout: fixed;
        }

        .miotr {
            background: #f8f8f8;
            border: 1px solid #ddd;
            padding: .35em;
        }

        .miacella {
            padding: .625em;
            text-align: center;
        }

        .miaintestazione {
            padding: .625em;
            text-align: center;
            font-size: .85em;
            letter-spacing: .1em;
            text-transform: uppercase;
        }

        #progress {
            z-index: 200;
            background-color: White;
            position: absolute;
            top: 0pt;
            left: 0pt;
            border: solid 1px black;
            padding: 5px 5px 5px 5px;
            text-align: center;
        }
    </style>
    <script type="text/javascript">

        function DialogBox(Path) {
            try {
                myTimeOut = setTimeout('window.location.href="/Seniorweb/Login.aspx";', sessionTimeout);
            }
            catch (err) {

            }

            var winW = 630, winH = 460;
            if (document.body && document.body.offsetWidth) {
                winW = document.body.offsetWidth;
                winH = document.body.offsetHeight;
            }
            if (document.compatMode == 'CSS1Compat' &&
                document.documentElement &&
                document.documentElement.offsetWidth) {
                winW = document.documentElement.offsetWidth;
                winH = document.documentElement.offsetHeight;
            }
            if (window.innerWidth && window.innerHeight) {
                winW = window.innerWidth;
                winH = window.innerHeight;
            }

            var APPO = winH - 100;

            REDIPS.dialog.show(winW - 200, winH - 100, '<iframe id="output" src="' + Path + '" height="' + APPO + '" width="100%" ></iframe>');
            return false;

        }
    </script>
    <script type="text/javascript">
        $(document).ready(function () {
            if (window.innerHeight > 0) { $("#BarraLaterale").css("height", (window.innerHeight - 105) + "px"); } else { $("#BarraLaterale").css("height", (document.documentElement.offsetHeight - 105) + "px"); }
        });

        function openInNewTab(url) {
            var win = window.open(url, '_blank');
            win.focus();
        }
    </script>
</head>
<body>
    <form id="form1" runat="server" autocomplete="off">
        <asp:ScriptManager ID="ScriptManager2" runat="server" AsyncPostBackTimeout="36000" EnableScriptGlobalization="true">
            <Scripts>
                <asp:ScriptReference Path="~/Scripts/AjaxControlToolkit/Bundle" />
            </Scripts>
        </asp:ScriptManager>
        <asp:Label ID="Lbl_BarraSenior" runat="server" Text=""></asp:Label>
        <div align="left">
            <table style="width: 100%;" cellpadding="0" cellspacing="0">
                <tr>
                    <td style="width: 160px; background-color: #F0F0F0;"></td>
                    <td>
                        <div class="Titolo">
                            Contabilità - Servizi - Imp. Fattura XML<br />
                            <br />
                        </div>
                    </td>

                    <td style="text-align: right; vertical-align: top;">
                        <div class="DivTasti">
                            <asp:ImageButton ID="ImageButton1" runat="server" Height="38px" ImageUrl="~/images/elabora.png" class="EffettoBottoniTondi" Width="38px" />
                        </div>
                    </td>
                </tr>
            </table>

            <table style="width: 100%;" cellpadding="0" cellspacing="0">
                <tr>
                    <td style="width: 160px; vertical-align: top; background-color: #F0F0F0; text-align: center;" id="BarraLaterale">
                        <asp:ImageButton ID="ImgMenu" ImageUrl="images/Home.jpg" Width="112px" alt="Menù" class="Effetto" runat="server" /><br />
                        <asp:ImageButton ID="Btn_Esci" runat="server" BackColor="Transparent" ImageUrl="../images/Menu_Indietro.png" class="Effetto" ToolTip="Chiudi" />
                        <br />
                        <br />
                        <br />

                    </td>
                    <td colspan="2" style="background-color: #FFFFFF; vertical-align: top;">
                        <xsau:TabContainer ID="TabContainer1" runat="server" ActiveTabIndex="0" CssClass="TabSenior"
                            Width="100%" BorderStyle="None" Style="margin-right: 39px">
                            <xsau:TabPanel runat="server" HeaderText="Prima Nota" ID="Tab_Anagrafica">
                                <HeaderTemplate>
                                    Imp. Fattura XML
                                </HeaderTemplate>
                                <ContentTemplate>



                                    <br />


                                    <label style="display: block; float: left; width: 160px;">File Zip o XML  :</label>
                                    <asp:FileUpload ID="FileUpload1" runat="server" />
                                    <br />
                                    <br />
                                    <label style="display: block; float: left; width: 160px;">Raggruppa Costi  :</label>
                                    <asp:CheckBox ID="Chk_RaggruppaCosti" runat="server" Text="" Checked />
                                    <asp:CheckBox ID="Chk_RaggruppaPerAliquota" runat="server" Text="Raggruppa Per Aliquota" />
                                    <br />
                                    <br />
                                    <label style="display: block; float: left; width: 160px;">Escludi già importati  :</label>
                                    <asp:CheckBox ID="Chk_EscludiImportati" runat="server" Text="" />
                                    <br />
                                    <br />
                                    <label style="display: block; float: left; width: 160px;">Causali :</label>
                                    <asp:RadioButton ID="RB_Attivita1" runat="server" GroupName="Attivita" Text="Attivita 1" Checked AutoPostBack="true" />
                                    <asp:RadioButton ID="RB_Attivita2" runat="server" GroupName="Attivita" Text="Attivita 2" AutoPostBack="true" />
                                    <br />
                                    <br />
                                    <asp:Label ID="lbl_Causali" runat="server" Text=""></asp:Label>
                                    <br />
                                    <br />


                                    <asp:GridView ID="Grid" runat="server" CellPadding="3"
                                        Width="100%" BackColor="White" BorderColor="#999999" BorderStyle="None"
                                        BorderWidth="1px" GridLines="Vertical">
                                        <RowStyle ForeColor="#565151" BackColor="#EEEEEE" />
                                        <Columns>
                                            <asp:TemplateField ItemStyle-Width="40px">
                                                <ItemTemplate>
                                                    <asp:ImageButton ID="VisualizzaXML" CommandName="Richiama" runat="Server" ImageUrl="~/images/esegui.png" class="EffettoBottoniTondi" BackColor="Transparent" CommandArgument="<%#   Container.DataItemIndex  %>" />
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                            <asp:TemplateField HeaderText="" HeaderStyle-Width="26px" ItemStyle-Width="26px" FooterStyle-Width="26px">





                                                <ItemTemplate>
                                                    <asp:ImageButton ID="Seleziona" CommandName="Seleziona" runat="Server"
                                                        ImageUrl="~/images/select.png" class="EffettoBottoniTondi" BackColor="Transparent"
                                                        CommandArgument="<%#   Container.DataItemIndex  %>" ToolTip="Richiama" />
                                                </ItemTemplate>
                                                <FooterStyle Width="26px"></FooterStyle>
                                                <HeaderStyle Width="26px"></HeaderStyle>
                                                <ItemStyle Width="26px"></ItemStyle>
                                            </asp:TemplateField>


                                            <asp:BoundField DataField="Numero Documento" HeaderText="Numero Documento" />
                                            <asp:BoundField DataField="Data Documento" HeaderText="Data Documento" />
                                            <asp:BoundField DataField="Tipo Documento" HeaderText="Tipo Documento" />
                                            <asp:BoundField DataField="Ragione Sociale" HeaderText="Ragione Sociale" />
                                            <asp:BoundField DataField="Totale Documento" HeaderText="Totale Documento" />
                                            <asp:BoundField DataField="receiptDate" HeaderText="Data Ricezione" />
                                        </Columns>
                                        <FooterStyle BackColor="#CCCCCC" ForeColor="Black" />
                                        <PagerStyle BackColor="#999999" ForeColor="Black" HorizontalAlign="Center" />
                                        <SelectedRowStyle BackColor="#008A8C" Font-Bold="True" ForeColor="White" />
                                        <HeaderStyle BackColor="#565151" Font-Bold="True" ForeColor="White" />
                                        <AlternatingRowStyle BackColor="#DCDCDC" />
                                    </asp:GridView>



                                </ContentTemplate>
                            </xsau:TabPanel>

                            <xsau:TabPanel runat="server" HeaderText="Prima Nota" ID="TabPanel1">
                                <HeaderTemplate>
                                    Agyo
                                </HeaderTemplate>
                                <ContentTemplate>


                                    <label class="LabelCampo">Data Al:</label>
                                    <asp:TextBox ID="Txt_DataDal" runat="server" Width="90px"></asp:TextBox>

                                    <br />
                                    <br />


                                </ContentTemplate>
                            </xsau:TabPanel>



                        </xsau:TabContainer>
                    </td>
                </tr>
            </table>
            <asp:GridView ID="GridExcel" runat="server">
            </asp:GridView>
            <asp:Button ID="Btn_Fattura" runat="server" Text="" />

        </div>
    </form>

</body>
</html>
