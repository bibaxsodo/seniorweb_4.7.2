﻿Imports Microsoft.VisualBasic
Imports System.Data.OleDb
Imports System.Web.Hosting
Imports System.Data.SqlTypes

Partial Class GeneraleWeb_Elenco_RegoleBudget
    Inherits System.Web.UI.Page

    Dim Tabella As New System.Data.DataTable("tabella")


    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Session("UTENTE") = "" Then
            Response.Redirect("..\Login.aspx")
            Exit Sub
        End If

        If Page.IsPostBack = True Then
            Exit Sub
        End If


        Dim K1 As New Cls_SqlString

        Dim Barra As New Cls_BarraSenior

        If Not IsNothing(Session("RicercaGeneraleSQLString")) Then
            K1 = Session("RicercaGeneraleSQLString")
        End If

        Lbl_BarraSenior.Text = Barra.CodiceBarra(Application("SENIOR"), Session("UTENTE"), Page, K1)

        Txt_Anno.Text = Year(Now)

        Call CaricaTabella()
        'Btn_Nuovo.Visible = True
    End Sub

    Function campodb(ByVal oggetto As Object) As String
        If IsDBNull(oggetto) Then
            Return ""
        Else
            Return oggetto
        End If
    End Function
    Function campodbn(ByVal oggetto As Object) As Double
        If IsDBNull(oggetto) Then
            Return 0
        Else
            Return oggetto
        End If
    End Function

    Protected Sub Grid_PageIndexChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewPageEventArgs) Handles Grid.PageIndexChanging
        Tabella = ViewState("Appoggio")
        Grid.PageIndex = e.NewPageIndex
        Grid.DataSource = Tabella
        Grid.DataBind()
    End Sub

    Protected Sub Grid_RowCommand(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewCommandEventArgs) Handles Grid.RowCommand
        If e.CommandName = "Seleziona" Then


            Dim d As Integer


            Tabella = ViewState("Appoggio")

            d = Val(e.CommandArgument)



            Dim ID As String
            

            ID = Tabella.Rows(d).Item(0).ToString
            

            Response.Redirect("REGOLEbudget.aspx?ID=" & ID)
        End If
    End Sub

    Protected Sub ImgRicerca_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles ImgRicerca.Click
        Response.Redirect("REGOLEbudget.aspx")
    End Sub




    Private Function SplitWords(ByVal s As String) As String()
        '
        ' Call Regex.Split function from the imported namespace.
        ' Return the result array.
        '
        Return Regex.Split(s, "\W+")
    End Function

    Protected Sub Btn_Esci_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles Btn_Esci.Click
        Response.Redirect("Menu_Budget.aspx")
    End Sub

    Protected Sub ImgHome_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles ImgHome.Click
        Response.Redirect("Menu_Budget.aspx")
    End Sub


    Private Sub CaricaTabella()
        Dim cn As New OleDbConnection


        Dim ConnectionString As String = Session("DC_GENERALE")

        cn = New Data.OleDb.OleDbConnection(ConnectionString)

        cn.Open()

        Dim cmd As New OleDbCommand

        cmd.CommandText = ("select top 50  * from RegoleBudget Where Anno  = " & Val(Txt_Anno.Text) & _
                               "  Order By Anno,Livello1,Livello2,Livello3")
        cmd.Connection = cn




        Tabella.Clear()
        Tabella.Columns.Add("ID", GetType(String))
        Tabella.Columns.Add("Anno", GetType(String))
        Tabella.Columns.Add("ContoContabile", GetType(String))
        Tabella.Columns.Add("Conto", GetType(String))
        Tabella.Columns.Add("Colonna", GetType(String))
        Tabella.Columns.Add("Percentuale", GetType(String))
        Tabella.Columns.Add("Imp.Fisso", GetType(String))

        Dim myPOSTreader As OleDbDataReader = cmd.ExecuteReader()
        Do While myPOSTreader.Read
            Dim myriga As System.Data.DataRow = Tabella.NewRow()

            myriga(0) = campodbn(myPOSTreader.Item("Id"))
            myriga(1) = campodbn(myPOSTreader.Item("Anno"))

            Dim PC As New Cls_Pianodeiconti

            PC.Mastro = campodbn(myPOSTreader.Item("MastroPianoDeiConti"))
            PC.Conto = campodbn(myPOSTreader.Item("ContoPianoDeiConti"))
            PC.Sottoconto = campodbn(myPOSTreader.Item("SottoContoPianoDeiConti"))

            PC.Decodfica(Session("DC_GENERALE"))

            myriga(2) = PC.Descrizione

            Dim PCB As New Cls_TipoBudget

            PCB.Anno = campodbn(myPOSTreader.Item("Anno"))
            PCB.Livello1 = campodbn(myPOSTreader.Item("Livello1"))
            PCB.Livello2 = campodbn(myPOSTreader.Item("Livello2"))
            PCB.Livello3 = campodbn(myPOSTreader.Item("Livello3"))

            PCB.Decodfica(Session("DC_GENERALE"))

            myriga(3) = PCB.Descrizione

            Dim Colonna As New Cls_colonnebudget

            Colonna.Anno = campodbn(myPOSTreader.Item("Anno"))
            Colonna.Livello1 = campodbn(myPOSTreader.Item("Colonna"))
            Colonna.Leggi(Session("DC_GENERALE"))

            myriga(4) = Colonna.Descrizione
            myriga(5) = campodb(myPOSTreader.Item("Percentuale"))
            myriga(6) = campodb(myPOSTreader.Item("ImportoFisso"))

            Tabella.Rows.Add(myriga)
        Loop

        myPOSTreader.Close()
        cn.Close()


        ViewState("Appoggio") = Tabella

        Grid.AutoGenerateColumns = True

        Grid.DataSource = Tabella

        Grid.DataBind()

    End Sub

    Protected Sub ImageButton1_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles ImageButton1.Click
        Call CaricaTabella()
    End Sub

End Class
