﻿
Partial Class GeneraleWeb_GestionePrenotazione
    Inherits System.Web.UI.Page
    Dim Tabella As New System.Data.DataTable("tabella")
    Protected Sub Grid_RowCancelingEdit(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewCancelEditEventArgs) Handles Grid.RowCancelingEdit
        Grid.EditIndex = -1
        Call faibind()
    End Sub

    Private Sub faibind()
        Tabella = ViewState("RighePrenotazione")

        Grid.AutoGenerateColumns = False
        Grid.DataSource = Tabella
        Grid.DataBind()
    End Sub


    Private Sub CaricaPagina()
        
        Tabella.Clear()
        Tabella.Columns.Clear()
        Tabella.Columns.Add("Livello", GetType(String))
        Tabella.Columns.Add("Colonna", GetType(String))
        Tabella.Columns.Add("Importo", GetType(Double))
        Tabella.Columns.Add("Descrizione", GetType(String))
        Tabella.Columns.Add("Riga", GetType(Long))
        Dim myriga As System.Data.DataRow = Tabella.NewRow()        
        myriga(2) = 0
        myriga(4) = 0
        Tabella.Rows.Add(myriga)
        Grid.AutoGenerateColumns = False
        Grid.DataSource = Tabella
        Grid.DataBind()



        ViewState("RighePrenotazione") = Tabella


    End Sub

    Protected Sub Grid_RowCommand(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewCommandEventArgs) Handles Grid.RowCommand
        If (e.CommandName = "Inserisci") Then

            UpDateTable()

            Lbl_Errore.Text = ""

            Tabella = ViewState("RighePrenotazione")
            Dim myriga As System.Data.DataRow = Tabella.NewRow()
            myriga(2) = 0
            myriga(4) = 0
            Tabella.Rows.Add(myriga)

            ViewState("RighePrenotazione") = Tabella
            Call faibind()
            Call EseguiJS()
        End If
        If (e.CommandName = "Diminuzione") Then


            Dim Numero As Integer

            Numero = Val(e.CommandArgument.ToString())
            Dim gvRow As GridViewRow = CType(CType(e.CommandSource, Control).NamingContainer, GridViewRow)

            Session("RIGAPRENOTAZIONE") = 0
            Dim LblRiga As Label = DirectCast(gvRow.FindControl("LblRiga"), Label)
            If Not IsNothing(LblRiga) Then
                Session("RIGAPRENOTAZIONE") = LblRiga.Text
            End If

            If Session("RIGAPRENOTAZIONE") > 0 Then
                ScriptManager.RegisterStartupScript(Me, Me.GetType(), "Diminuzione", "DialogBox('diminuzionebudget.aspx');", True)
            End If
            Call EseguiJS()
        End If
    End Sub

    Protected Sub Grid_RowDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs) Handles Grid.RowDataBound
        If e.Row.RowType = DataControlRowType.DataRow Then




            Dim LblLivello As Label = DirectCast(e.Row.FindControl("LblLivello"), Label)
            If Not IsNothing(LblLivello) Then

            Else
                Dim TxtLivello As TextBox = DirectCast(e.Row.FindControl("TxtLivello"), TextBox)
                Dim MyDv As System.Data.DataRowView = e.Row.DataItem
                TxtLivello.Text = MyDv(0).ToString

                REM TxtSottoconto.Text = Tabella.Rows(e.Row.RowIndex).Item(0).ToString

                Dim MyJs As String

                MyJs = "$(document).ready(function() { $(" & Chr(34) & "#" & TxtLivello.ClientID & Chr(34) & ").autocomplete('autocompletecontobudget.ashx?Anno=" & Val(Txt_Anno.Text) & "&Utente=" & Session("UTENTE") & "', {delay:5,minChars:3}); });"
                ScriptManager.RegisterStartupScript(Me, Me.GetType(), "visualizza", MyJs, True)
            End If


            Dim LblColonna As Label = DirectCast(e.Row.FindControl("LblColonna"), Label)
            If Not IsNothing(LblColonna) Then

            Else
                Dim DDColonna As DropDownList = DirectCast(e.Row.FindControl("DDColonna"), DropDownList)

                Dim x As New Cls_colonnebudget

                x.Anno = Val(Txt_Anno.Text)
                x.UpDateDropBox(Session("DC_GENERALE"), DDColonna)
                DDColonna.SelectedValue = Val(Tabella.Rows(e.Row.RowIndex).Item(1).ToString)
            End If


            Dim LblImporto As Label = DirectCast(e.Row.FindControl("LblImporto"), Label)
            If Not IsNothing(LblImporto) Then

            Else
                Dim TxtImporto As TextBox = DirectCast(e.Row.FindControl("TxtImporto"), TextBox)
                Dim Appoggio As Double

                If IsDBNull(Tabella.Rows(e.Row.RowIndex).Item(2).ToString) Then
                    Appoggio = 0
                Else
                    Appoggio = Tabella.Rows(e.Row.RowIndex).Item(2).ToString
                End If

                TxtImporto.Text = Format(Appoggio, "#,##0.00")
                Dim MyJs As String

                MyJs = "$(document).ready(function() { $('#" & TxtImporto.ClientID & "').keypress(function() { ForceNumericInput($('#" & TxtImporto.ClientID & "').val(), true, true); } );  $('#" & TxtImporto.ClientID & "').blur(function() { var ap = formatNumber ($('#" & TxtImporto.ClientID & "').val(),2); $('#" & TxtImporto.ClientID & "').val(ap); }); });"
                ScriptManager.RegisterStartupScript(Me, Me.GetType(), "visualizzaD", MyJs, True)
                End If




                Dim LblDescrizione As Label = DirectCast(e.Row.FindControl("LblDescrizione"), Label)
                If Not IsNothing(LblDescrizione) Then

                Else
                    Dim TxtDescrizione As TextBox = DirectCast(e.Row.FindControl("TxtDescrizione"), TextBox)
                    TxtDescrizione.Text = Tabella.Rows(e.Row.RowIndex).Item(3).ToString
                End If


                Dim LblRiga As Label = DirectCast(e.Row.FindControl("LblRiga"), Label)
                If Not IsNothing(LblRiga) Then
                    LblRiga.Text = Tabella.Rows(e.Row.RowIndex).Item(4).ToString
                End If
        End If
    End Sub

    Protected Sub Grid_RowDeleted(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewDeletedEventArgs) Handles Grid.RowDeleted

    End Sub

    Protected Sub Grid_RowDeleting(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewDeleteEventArgs) Handles Grid.RowDeleting
        Lbl_Errore.Text = ""
        Tabella = ViewState("RighePrenotazione")

        Try

            If Request.UserAgent.Contains("Chrome") Then
                If IsDBNull(Session("RI_TIMER")) Or DateDiff("s", Session("RI_TIMER"), Now) > 1 Then
                    Tabella.Rows.RemoveAt(e.RowIndex)
                    If Tabella.Rows.Count = 0 Then
                        Dim myriga As System.Data.DataRow = Tabella.NewRow()
                        myriga(2) = 0
                        myriga(4) = 0
                        Tabella.Rows.Add(myriga)
                    End If
                    Session("RI_TIMER") = Now
                End If
            Else
                Tabella.Rows.RemoveAt(e.RowIndex)
                If Tabella.Rows.Count = 0 Then
                    Dim myriga As System.Data.DataRow = Tabella.NewRow()
                    myriga(2) = 0
                    myriga(4) = 0
                    Tabella.Rows.Add(myriga)
                End If
            End If
        Catch ex As Exception

        End Try


        ViewState("RighePrenotazione") = Tabella

        Call faibind()

        e.Cancel = True
    End Sub

    Protected Sub Grid_RowEditing(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewEditEventArgs) Handles Grid.RowEditing
        Grid.EditIndex = e.NewEditIndex
        Call faibind()
    End Sub

    Protected Sub Grid_RowUpdated(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewUpdatedEventArgs) Handles Grid.RowUpdated

    End Sub


    Protected Sub Grid_RowUpdating(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewUpdateEventArgs) Handles Grid.RowUpdating
        Dim LblRiga As Label = DirectCast(Grid.Rows(e.RowIndex).FindControl("LblRiga"), Label)
        Dim TxtLivello As TextBox = DirectCast(Grid.Rows(e.RowIndex).FindControl("TxtLivello"), TextBox)
        Dim DDColonna As DropDownList = DirectCast(Grid.Rows(e.RowIndex).FindControl("DDColonna"), DropDownList)
        Dim TxtImporto As TextBox = DirectCast(Grid.Rows(e.RowIndex).FindControl("TxtImporto"), TextBox)
        Dim TxtDescrizione As TextBox = DirectCast(Grid.Rows(e.RowIndex).FindControl("TxtDescrizione"), TextBox)
        Lbl_Errore.Text = ""
        Dim x As New Cls_ContoBudget

        x.ANNO = Val(Txt_Anno.Text)


        If Not x.ControllaSottoconto(TxtLivello.Text) Then
            ScriptManager.RegisterClientScriptBlock(Me, Me.GetType(), "Errorecr1", "VisualizzaErrore('Conto partita errato');", True)
            REM Lbl_Errore.Text = "Conto partita errato"

            Dim MyJs As String

            MyJs = "$(document).ready(function() { $(" & Chr(34) & "#" & TxtLivello.ClientID & Chr(34) & ").autocomplete('/WebHandler/GestioneConto.ashx?Anno=" & Val(Txt_Anno.Text) & "&Utente=" & Session("UTENTE") & "', {delay:5,minChars:3}); });"
            ScriptManager.RegisterStartupScript(Me, Me.GetType(), "visualizza", MyJs, True)

            MyJs = "$(document).ready(function() { $('#" & TxtImporto.ClientID & "').keypress(function() { ForceNumericInput($('#" & TxtImporto.ClientID & "').val(), true, true); } );  $('#" & TxtImporto.ClientID & "').blur(function() { var ap = formatNumber ($('#" & TxtImporto.ClientID & "').val(),2); $('#" & TxtImporto.ClientID & "').val(ap); }); });"
            ScriptManager.RegisterStartupScript(Me, Me.GetType(), "visualizzaD", MyJs, True)

            Exit Sub
        Else
            TxtLivello.Text = x.DecodificaSottoconto(Session("DC_GENERALE"), TxtLivello.Text)
        End If

        If TxtImporto.Text = "" Then
            TxtImporto.Text = "0"
        End If


        Tabella = ViewState("RighePrenotazione")
        Dim row = Grid.Rows(e.RowIndex)

        Tabella.Rows(row.DataItemIndex).Item(0) = TxtLivello.Text
        Tabella.Rows(row.DataItemIndex).Item(1) = DDColonna.SelectedItem.Text
        Tabella.Rows(row.DataItemIndex).Item(2) = TxtImporto.Text
        Tabella.Rows(row.DataItemIndex).Item(3) = TxtDescrizione.Text
        Tabella.Rows(row.DataItemIndex).Item(4) = LblRiga.Text

        ViewState("RighePrenotazione") = Tabella

        Grid.EditIndex = -1
        Call faibind()
    End Sub

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Trim(Session("UTENTE")) = "" Then
            If Request.ServerVariables("URL").ToUpper.IndexOf("SENIORWEBBLANCO") > 0 Then
                Response.Redirect("/ODV/Login.aspx")
                Exit Sub
            Else
                Response.Redirect("/SeniorWeb/Login.aspx")
                Exit Sub
            End If
        End If


        If Page.IsPostBack = True Then Exit Sub

        Dim K1 As New Cls_SqlString

        Dim Barra As New Cls_BarraSenior

        If Not IsNothing(Session("RicercaGeneraleSQLString")) Then
            K1 = Session("RicercaGeneraleSQLString")
        End If

        Lbl_BarraSenior.Text = Barra.CodiceBarra(Application("SENIOR"), Session("UTENTE"), Page, K1)


        Txt_Anno.Text = Val(Request.Item("Anno"))

        Txt_Anno.Enabled = False

        If Val(Request.Item("NUMERO")) = 0 Then
            Txt_Numero.Text = "0"
            Txt_Numero.Enabled = False
            Session("NumeroPrenotazione") = 0
            Session("AnnoPrenotazione") = Txt_Anno.Text
        End If


        Dim TipoAtto As New Cls_TipoAtto

        TipoAtto.UpDateDropBox(Session("DC_TABELLE"), DD_Tipo)


        Call CaricaPagina()


        If Val(Session("AnnoPrenotazione")) > 0 And Val(Session("NumeroPrenotazione")) > 0 Then
            Txt_Anno.Text = Session("AnnoPrenotazione")
            Txt_Numero.Text = Session("NumeroPrenotazione")
            Call Txt_Numero_TextChanged(sender, e)
            Txt_Anno.Enabled = False
            Txt_Numero.Enabled = False
        End If

        If Val(Request.Item("Anno")) > 0 And Val(Request.Item("NUMERO")) > 0 Then
            Txt_Anno.Text = Val(Request.Item("Anno"))
            Txt_Numero.Text = Val(Request.Item("NUMERO"))
            Call Txt_Numero_TextChanged(sender, e)
            Txt_Anno.Enabled = False
            Txt_Numero.Enabled = False
        End If


        Call EseguiJS()
    End Sub

    Protected Sub Grid_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles Grid.SelectedIndexChanged

    End Sub


    Private Sub EseguiJS()
        Dim MyJs As String
        MyJs = "$(document).ready(function() {"

        MyJs = MyJs & "var els = document.getElementsByTagName(""*"");"

        MyJs = MyJs & "for (var i=0;i<els.length;i++)"
        MyJs = MyJs & "if ( els[i].id ) { "
        MyJs = MyJs & " var appoggio =els[i].id; "
        MyJs = MyJs & " if (appoggio.match('TxtLivello')!= null) {  "
        MyJs = MyJs & " $(els[i]).autocomplete('autocompletecontobudget.ashx?Anno=" & Txt_Anno.Text & "&Utente=" & Session("UTENTE") & "', {delay:5,minChars:3});"
        MyJs = MyJs & "    }"

        MyJs = MyJs & " if ((appoggio.match('TxtImporto')!= null) || appoggio.match('TxtAvere')!= null) {  "
        MyJs = MyJs & " $(els[i]).keypress(function() { ForceNumericInput($(this).val(), true, true); } ); "
        MyJs = MyJs & " $(els[i]).blur(function() { var ap = formatNumber($(this).val(),2); $(this).val(ap); });"
        MyJs = MyJs & "    }"

        MyJs = MyJs & " if (appoggio.match('Txt_Data')!= null) {  "
        MyJs = MyJs & " $(els[i]).mask(""99/99/9999""); "

        MyJs = MyJs & " $(els[i]).datepicker({ changeMonth: true,changeYear: true,  yearRange: ""1990:" & Year(Now) + 5 & """},$.datepicker.regional[""it""]);"

        MyJs = MyJs & "    }"

        MyJs = MyJs & "} "
        MyJs = MyJs & "});"

        ScriptManager.RegisterStartupScript(Me, Me.GetType(), "JS_GESTPRE", MyJs, True)
    End Sub

    Private Sub UpDateTable()
        Dim i As Integer


        Tabella.Clear()
        Tabella.Columns.Clear()
        Tabella.Columns.Add("Livello", GetType(String))
        Tabella.Columns.Add("Colonna", GetType(String))
        Tabella.Columns.Add("Importo", GetType(Double))
        Tabella.Columns.Add("Descrizione", GetType(String))
        Tabella.Columns.Add("Riga", GetType(Long))

        For i = 0 To Grid.Rows.Count - 1
            Dim LblRiga As Label = DirectCast(Grid.Rows(i).FindControl("LblRiga"), Label)
            Dim TxtLivello As TextBox = DirectCast(Grid.Rows(i).FindControl("TxtLivello"), TextBox)
            Dim DDColonna As DropDownList = DirectCast(Grid.Rows(i).FindControl("DDColonna"), DropDownList)
            Dim TxtImporto As TextBox = DirectCast(Grid.Rows(i).FindControl("TxtImporto"), TextBox)
            Dim TxtDescrizione As TextBox = DirectCast(Grid.Rows(i).FindControl("TxtDescrizione"), TextBox)
            Lbl_Errore.Text = ""
            Dim x As New Cls_ControllaSottoconto



            Dim myriga As System.Data.DataRow = Tabella.NewRow()

            myriga(0) = TxtLivello.Text
            myriga(1) = DDColonna.SelectedValue
            myriga(2) = TxtImporto.Text
            myriga(3) = TxtDescrizione.Text
            myriga(4) = LblRiga.Text
            Tabella.Rows.Add(myriga)
        Next

        ViewState("RighePrenotazione") = Tabella

        Call faibind()

    End Sub


    Protected Sub Btn_Modifica_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles Btn_Modifica.Click


        If Not IsDate(Txt_DataRegistrazione.Text) Then
            ClientScript.RegisterClientScriptBlock(Me.GetType(), "Errore", "VisualizzaErrore('Specificare data di registrazione');", True)
            Call EseguiJS()
            Exit Sub
        End If


        Call UpDateTable()
        Dim xPrenotazione As New Cls_PrenotazioniTesta


        xPrenotazione.Anno = Txt_Anno.Text
        xPrenotazione.Data = Txt_DataRegistrazione.Text

        If IsDate(Txt_Data.Text) Then
            xPrenotazione.DataAtto = Txt_Data.Text
        Else
            xPrenotazione.DataAtto = Nothing
        End If
        xPrenotazione.Descrizione = Txt_Descrizione.Text
        xPrenotazione.Numero = Val(Txt_Numero.Text)
        xPrenotazione.NumeroAtto = Txt_NumeroAtto.Text
        xPrenotazione.TipoAtto = DD_Tipo.SelectedValue

        Dim I As Long
        Dim xColonne As New Cls_colonnebudget
        Tabella = ViewState("RighePrenotazione")

        Dim DC As New Cls_ContoBudget
        For I = 0 To Tabella.Rows.Count - 1
            If IsNothing(xPrenotazione.Righe(I)) Then
                xPrenotazione.Righe(I) = New Cls_PrenotazioniRighe
            End If

            xPrenotazione.Righe(I).Livello1 = DC.Livello1(Tabella.Rows(I).Item(0))
            xPrenotazione.Righe(I).Livello2 = DC.Livello2(Tabella.Rows(I).Item(0))
            xPrenotazione.Righe(I).Livello3 = DC.Livello3(Tabella.Rows(I).Item(0))


            
            xPrenotazione.Righe(I).Colonna = Tabella.Rows(I).Item(1)
            xPrenotazione.Righe(I).Importo = Tabella.Rows(I).Item(2)
            xPrenotazione.Righe(I).Descrizione = Tabella.Rows(I).Item(3)

            If Val(Tabella.Rows(I).Item(4)) <> 0 Then
                xPrenotazione.Righe(I).Riga = Tabella.Rows(I).Item(4)
            Else
                xPrenotazione.Righe(I).Riga = 0
            End If
        Next
        xPrenotazione.Scrivi(Session("DC_GENERALE"))

        Txt_Anno.Text = xPrenotazione.Anno
        Txt_Numero.Text = xPrenotazione.Numero

        ScriptManager.RegisterStartupScript(Me, Me.GetType(), "visualizza", "alert('Prenotazione Modificata');", True)


        

        Response.Redirect("ElencoPrenotazioni.aspx")
    End Sub


    Private Sub Pulisci()
        Txt_Anno.Text = Year(Now)
        Txt_Numero.Text = 0
        Session("PRENOTAZIONEANNO") = 0
        Session("PRENOTAZIONENUMERO") = 0

        Txt_DataRegistrazione.Text = Format(Now, "dd/MM/yyyy")
        Txt_Data.Text = ""
        
        Txt_Descrizione.Text = ""
        Txt_NumeroAtto.Text = ""        
        DD_Tipo.SelectedValue = ""

        Tabella.Clear()
        Tabella.Columns.Clear()
        Tabella.Columns.Add("Livello", GetType(String))
        Tabella.Columns.Add("Colonna", GetType(String))
        Tabella.Columns.Add("Importo", GetType(Double))
        Tabella.Columns.Add("Descrizione", GetType(String))
        Tabella.Columns.Add("Riga", GetType(Long))
        Dim myriga As System.Data.DataRow = Tabella.NewRow()
        myriga(2) = 0
        myriga(4) = 0
        Tabella.Rows.Add(myriga)
        Grid.AutoGenerateColumns = False
        Grid.DataSource = Tabella
        Grid.DataBind()

        Txt_Anno.Text = Year(Now)

        Session("AnnoPrenotazione") = 0
        Session("NumeroPrenotazione") = 0

        ViewState("RighePrenotazione") = Tabella

        Call EseguiJS()
    End Sub
    Protected Sub Txt_Numero_TextChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles Txt_Numero.TextChanged
        Session("PRENOTAZIONEANNO") = 0
        Session("PRENOTAZIONENUMERO") = 0

        If Val(Txt_Anno.Text) > 0 And Val(Txt_Numero.Text) > 0 Then
            Dim xPrenotazione As New Cls_PrenotazioniTesta

            xPrenotazione.Anno = Txt_Anno.Text
            xPrenotazione.Numero = Txt_Numero.Text

            Session("PRENOTAZIONEANNO") = Txt_Anno.Text
            Session("PRENOTAZIONENUMERO") = Txt_Numero.Text

            xPrenotazione.Leggi(Session("DC_GENERALE"))
            If xPrenotazione.Numero = 0 Then
                Call Pulisci()
                Exit Sub
            End If


            Txt_DataRegistrazione.Text = Format(xPrenotazione.Data, "dd/MM/yyyy")
            Txt_Data.Text = ""
            If Year(xPrenotazione.DataAtto) > 1900 Then
                Txt_Data.Text = Format(xPrenotazione.DataAtto, "dd/MM/yyyy")
            End If
            Txt_Descrizione.Text = xPrenotazione.Descrizione
            Txt_NumeroAtto.Text = xPrenotazione.NumeroAtto
            Try
                DD_Tipo.SelectedValue = xPrenotazione.TipoAtto
            Catch ex As Exception

            End Try
            Txt_Descrizione.Text = xPrenotazione.Descrizione


            Tabella.Clear()
            Tabella.Columns.Clear()
            Tabella.Columns.Add("Livello", GetType(String))
            Tabella.Columns.Add("Colonna", GetType(String))
            Tabella.Columns.Add("Importo", GetType(Double))
            Tabella.Columns.Add("Descrizione", GetType(String))
            Tabella.Columns.Add("Riga", GetType(Long))

            Dim I As Long

            For I = 0 To 400
                If Not IsNothing(xPrenotazione.Righe(I)) Then
                    Dim myriga As System.Data.DataRow = Tabella.NewRow()
                    Dim DecT As New Cls_TipoBudget

                    DecT.Livello1 = xPrenotazione.Righe(I).Livello1
                    DecT.Livello2 = xPrenotazione.Righe(I).Livello2
                    DecT.Livello3 = xPrenotazione.Righe(I).Livello3
                    DecT.Anno = Txt_Anno.Text
                    DecT.Decodfica(Session("DC_GENERALE"))

                    myriga(0) = xPrenotazione.Righe(I).Livello1 & " " & xPrenotazione.Righe(I).Livello2 & " " & xPrenotazione.Righe(I).Livello3 & " " & DecT.Descrizione

                    Dim DecColonna As New Cls_colonnebudget

                    'DecColonna.Anno = Txt_Anno.Text
                    'DecColonna.Livello1 = xPrenotazione.Righe(I).Colonna
                    'DecColonna.Leggi(Session("DC_GENERALE"))
                    myriga(1) = xPrenotazione.Righe(I).Colonna
                    myriga(2) = Format(xPrenotazione.Righe(I).Importo, "#,##0.00")
                    myriga(3) = xPrenotazione.Righe(I).Descrizione
                    myriga(4) = xPrenotazione.Righe(I).Riga
                    Tabella.Rows.Add(myriga)
                End If
            Next
            ViewState("RighePrenotazione") = Tabella
            Grid.AutoGenerateColumns = False
            Grid.DataSource = Tabella
            Grid.DataBind()
            Txt_Anno.Enabled = False
            Txt_Numero.Enabled = False
        End If

    End Sub



    Protected Sub Btn_Elimina_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles Btn_Elimina.Click
        If Val(Txt_Numero.Text) = 0 Then
            ClientScript.RegisterClientScriptBlock(Me.GetType(), "Errore", "VisualizzaErrore('Seleziona prenotazione');", True)
            Exit Sub
        End If



        If Not IsDate(Txt_DataRegistrazione.Text) Then
            ClientScript.RegisterClientScriptBlock(Me.GetType(), "Errore", "VisualizzaErrore('Seleziona prenotazione');", True)
            Exit Sub
        End If


        Dim xPrenotazione As New Cls_PrenotazioniTesta

        xPrenotazione.Anno = Txt_Anno.Text
        xPrenotazione.Numero = Txt_Numero.Text

        xPrenotazione.Elimina(Session("DC_GENERALE"))

        Response.Redirect("ElencoPrenotazioni.aspx")
    End Sub


    Protected Sub Btn_Esci_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles Btn_Esci.Click
        Response.Redirect("ElencoPrenotazioni.aspx")
    End Sub

    Protected Sub Txt_Anno_TextChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles Txt_Anno.TextChanged
        Response.Redirect("ElencoPrenotazioni.aspx")
    End Sub



End Class
