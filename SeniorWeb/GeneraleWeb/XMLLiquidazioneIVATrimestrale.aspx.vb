﻿Imports System.IO
Imports System.Text
Imports System.Data
Imports System.Data.OleDb
Imports System.Configuration
Imports System.Security.Cryptography
Imports System.Net.Mail


Partial Class GeneraleWeb_XMLLiquidazioneIVATrimestrale
    Inherits System.Web.UI.Page


    Private Sub EseguiJS()
        Dim MyJs As String
        MyJs = "$(document).ready(function() {"

        MyJs = MyJs & "var els = document.getElementsByTagName(""*"");"

        MyJs = MyJs & "for (var i=0;i<els.length;i++)"
        MyJs = MyJs & "if ( els[i].id ) { "
        MyJs = MyJs & " var appoggio =els[i].id; "
        
        MyJs = MyJs & " if ((appoggio.match('Txt_Importo') != null) || (appoggio.match('Txt_Credito')!= null) || (appoggio.match('Txt_Totale')!= null) || (appoggio.match('Txt_Iva')!= null)) {  "
        MyJs = MyJs & " $(els[i]).keypress(function() { ForceNumericInput($(this).val(), true, true); } ); "
        MyJs = MyJs & " $(els[i]).blur(function() { var ap = formatNumber($(this).val(),2); $(this).val(ap); });"
        MyJs = MyJs & "    }"


        MyJs = MyJs & " if ((appoggio.match('Txt_Data')!= null) || (appoggio.match('Txt_AllaData')!= null) || (appoggio.match('Txt_DataReversale') != null)) {  "
        MyJs = MyJs & " $(els[i]).mask(""99/99/9999"");  "
        MyJs = MyJs & " $(els[i]).datepicker({ changeMonth: true,changeYear: true,  yearRange: ""1890:" & Year(Now) + 1 & """},$.datepicker.regional[""it""]);"
        MyJs = MyJs & "    }"

        MyJs = MyJs & "} "

        MyJs = MyJs & "if (window.innerHeight>0) { $(""#BarraLaterale"").css(""height"",(window.innerHeight - 94) + ""px""); } else"
        MyJs = MyJs & "{ $(""#BarraLaterale"").css(""height"",(document.documentElement.offsetHeight - 94) + ""px"");  }"


        MyJs = MyJs & "$("".chosen-select"").chosen({"
        MyJs = MyJs & "no_results_text: ""Causale non trovata"","
        MyJs = MyJs & "display_disabled_options: true,"
        'MyJs = MyJs & "allow_single_deselect: true,"
        MyJs = MyJs & "width: ""350px"","
        MyJs = MyJs & "placeholder_text_single: ""Seleziona Causale"""
        MyJs = MyJs & "});"

        MyJs = MyJs & "});"

        'MyJs = "$(document).ready(function() { $('.myClass').keypress(function() { handleEnter($('.myClass').val(), true, true); } );     $('#" & Txt_ImportoPagato.ClientID & "').blur(function() { var ap = formatNumber ($('#" & Txt_ImportoPagato.ClientID & "').val(),2); $('#" & Txt_ImportoPagato.ClientID & "').val(ap); }); });"
        ScriptManager.RegisterStartupScript(Me, Me.GetType(), "visualizzaRitIm", MyJs, True)
    End Sub


    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Call EseguiJS()

        If Page.IsPostBack = True Then
            Exit Sub
        End If
        Dim K1 As New Cls_SqlString

        Dim Barra As New Cls_BarraSenior

        If Not IsNothing(Session("RicercaAnagraficaSQLString")) Then
            Try
                K1 = Session("RicercaAnagraficaSQLString")
            Catch ex As Exception
                K1 = Nothing
            End Try
        End If

        Lbl_BarraSenior.Text = Barra.CodiceBarra(Application("SENIOR"), Session("UTENTE"), Page, K1)


        Dim Soc As New Cls_TabellaSocieta


        Soc.Leggi(Session("DC_TABELLE"))

        Txt_CodiceFiscale.Text = Soc.CodiceFiscale

        Txt_PartitaIVA.Text = Format(Val(Soc.PartitaIVA), "00000000000")

        Txt_CodiceFornitura.Text = "IVP18"

        Txt_CodiceCaricaDichiarante.Text = Soc.CodiceFiscale

        Txt_CodiceCarica.Text = 1

        Txt_AnnoImposta.Text = Year(Now)

        Txt_CodiceCaricaDichiarante.Text = 1
        Txt_FirmaDichiarazione.Text = 1
        Txt_ImpegnoPresentazione.Text = 1

        Txt_DataImpegno.Text = Format(Now, "dd/MM/yyyy")

        Txt_FirmaIntermediario.Text = 1


        Txt_Mese1.Text = 1
        Txt_Mese2.Text = 2
        Txt_Mese3.Text = 3



        Dim MyGruppi As New Cls_Gruppi

        MyGruppi.UpDateDropBox(Session("DC_TABELLE"), DD_Gruppo)

    End Sub

    Protected Sub Btn_Modifica_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles Btn_Modifica.Click
        If Not IsDate(Txt_DataImpegno.Text) Then
            ClientScript.RegisterClientScriptBlock(Me.GetType(), "Errore", "VisualizzaErrore('Indica Data Impegno');", True)
            Call EseguiJS()
            Exit Sub
        End If

        If Txt_CodiceFiscaleDichiarante.Text = "" Then
            ClientScript.RegisterClientScriptBlock(Me.GetType(), "Errore", "VisualizzaErrore('Indica Codice fiscale Dichiarante');", True)
            Call EseguiJS()
            Exit Sub
        End If

        If Val(Txt_AnnoImposta.Text) = 0 Then
            ClientScript.RegisterClientScriptBlock(Me.GetType(), "Errore", "VisualizzaErrore('Indica Anno imposta');", True)
            Call EseguiJS()
            Exit Sub
        End If

        If Val(Txt_PartitaIVA.Text) = 0 Then
            ClientScript.RegisterClientScriptBlock(Me.GetType(), "Errore", "VisualizzaErrore('Indica Partita IVA');", True)
            Call EseguiJS()
            Exit Sub
        End If

        If Txt_CFDichiarante.Text = "" Then
            ClientScript.RegisterClientScriptBlock(Me.GetType(), "Errore", "VisualizzaErrore('Indica Codice Fiscale Dichiarante');", True)
            Call EseguiJS()
            Exit Sub
        End If

        If Txt_ImpegnoPresentazione.Text = "" Then
            ClientScript.RegisterClientScriptBlock(Me.GetType(), "Errore", "VisualizzaErrore('Indica Impegno Presentazione ');", True)
            Call EseguiJS()
            Exit Sub
        End If

        If Val(Txt_Mese1.Text) = 0 Then
            ClientScript.RegisterClientScriptBlock(Me.GetType(), "Errore", "VisualizzaErrore('Indica Mese 1');", True)
            Call EseguiJS()
            Exit Sub
        End If
        If Val(Txt_Mese2.Text) = 0 Then
            ClientScript.RegisterClientScriptBlock(Me.GetType(), "Errore", "VisualizzaErrore('Indica Mese 2');", True)
            Call EseguiJS()
            Exit Sub
        End If
        If Val(Txt_Mese2.Text) = 0 Then
            ClientScript.RegisterClientScriptBlock(Me.GetType(), "Errore", "VisualizzaErrore('Indica Mese 3');", True)
            Call EseguiJS()
            Exit Sub
        End If



        Dim objStreamReader As StreamReader
        objStreamReader = File.OpenText(Server.MapPath("..\Modulo\IvaTrimestrale.xml"))
        Dim contenuto As String = objStreamReader.ReadToEnd()
        objStreamReader.Close()


        contenuto = contenuto.Replace("@CODICEFORNITURA@", Txt_CodiceFornitura.Text)
        contenuto = contenuto.Replace("@CODICEFISCALEDICHIARANTE@", Txt_CodiceFiscaleDichiarante.Text)
        contenuto = contenuto.Replace("@CODICECARICA@", Txt_CodiceCarica.Text)
        contenuto = contenuto.Replace("@IDENTIFICATIVO@", "00001")
        contenuto = contenuto.Replace("@CODICEFISCALE@", Txt_CodiceFiscale.Text)
        contenuto = contenuto.Replace("@ANNOIMPOSTA@", Txt_AnnoImposta.Text)
        contenuto = contenuto.Replace("@PARTITAIVA@", Txt_PartitaIVA.Text)
        contenuto = contenuto.Replace("@CFDICHIARANTE@", Txt_CFDichiarante.Text)
        contenuto = contenuto.Replace("@CODICECARICADICHIARANTE@", Txt_CodiceCaricaDichiarante.Text)
        contenuto = contenuto.Replace("@FIRMADICHIARAZIONE@", Txt_FirmaDichiarazione.Text)
        contenuto = contenuto.Replace("@CFINTERMEDIARIO@", Txt_CFIntermediario.Text)
        contenuto = contenuto.Replace("@IMPEGNOPRESENTAZIONE@", Txt_ImpegnoPresentazione.Text)

        Dim DataAppoggio As Date
        Try
            DataAppoggio = Txt_DataImpegno.Text
        Catch ex As Exception

        End Try

        contenuto = contenuto.Replace("@DATAIMPEGNO@", Format(DataAppoggio, "ddMMyyyy")) ' cambiare data
        contenuto = contenuto.Replace("@FIRMAINTERMEDIARIO@", Txt_FirmaIntermediario.Text)

        Dim Testo As String = ""        
        Testo = Testo & "<iv:Mese>" & Txt_Mese1.Text & "</iv:Mese>"
        Testo = Testo & "<iv:NumeroModulo>1</iv:NumeroModulo>"

        If Txt_TotaleOperazioniAttive1.Text <> "" Then
            If CDbl(Txt_TotaleOperazioniAttive1.Text) <> 0 Then
                Testo = Testo & "<iv:TotaleOperazioniAttive>" & Txt_TotaleOperazioniAttive1.Text.Replace(".", "") & "</iv:TotaleOperazioniAttive>"
            End If
        End If
        If Txt_TotaleOperazioniPassive1.Text <> "" Then
            If CDbl(Txt_TotaleOperazioniPassive1.Text) <> 0 Then
                Testo = Testo & "<iv:TotaleOperazioniPassive>" & Txt_TotaleOperazioniPassive1.Text.Replace(".", "") & "</iv:TotaleOperazioniPassive>"
            End If
        End If

        If Txt_IvaEsigibile1.Text <> "" Then
            If CDbl(Txt_IvaEsigibile1.Text) <> 0 Then
                Testo = Testo & "<iv:IvaEsigibile>" & Txt_IvaEsigibile1.Text.Replace(".", "") & "</iv:IvaEsigibile>"
            End If
        End If

        If Txt_IvaDetratta1.Text <> "" Then
            If CDbl(Txt_IvaDetratta1.Text) <> 0 Then
                Testo = Testo & "<iv:IvaDetratta>" & Txt_IvaDetratta1.Text.Replace(".", "") & "</iv:IvaDetratta>"
            End If
        End If

        If Txt_IvaCredito1.Text <> "" Then
            If CDbl(Txt_IvaCredito1.Text) <> 0 Then
                Testo = Testo & "<iv:IvaCredito>" & Txt_IvaCredito1.Text.Replace(".", "") & "</iv:IvaCredito>"
            End If
        End If

        If Txt_IvaDovuta1.Text <> "" Then
            If CDbl(Txt_IvaDovuta1.Text) <> 0 Then
                Testo = Testo & "<iv:IvaDovuta>" & Txt_IvaDovuta1.Text.Replace(".", "") & "</iv:IvaDovuta>"
            End If
        End If


        If Txt_CreditoPeriodoPrecedente1.Text <> "" Then
            If CDbl(Txt_CreditoPeriodoPrecedente1.Text) <> 0 Then
                Testo = Testo & "<iv:CreditoPeriodoPrecedente>" & Txt_CreditoPeriodoPrecedente1.Text.Replace(".", "") & "</iv:CreditoPeriodoPrecedente>"
            End If
        End If


        If Txt_Accontodovuto1.Text <> "" Then
            If CDbl(Txt_Accontodovuto1.Text) <> 0 Then
                Testo = Testo & "<iv:Accontodovuto>" & Txt_Accontodovuto1.Text.Replace(".", "") & "</iv:Accontodovuto>"
            End If
        End If


        If Txt_ImportoACredito1.Text <> "" Then
            If CDbl(Txt_ImportoACredito1.Text) <> 0 Then
                Testo = Testo & "<iv:ImportoACredito>" & Txt_ImportoACredito1.Text.Replace(".", "") & "</iv:ImportoACredito>"
            End If
        End If

        If Txt_ImportoDaVersare1.Text <> "" Then
            If CDbl(Txt_ImportoDaVersare1.Text) <> 0 Then
                Testo = Testo & "<iv:ImportoDaVersare>" & Txt_ImportoDaVersare1.Text.Replace(".", "") & "</iv:ImportoDaVersare>"
            End If
        End If

        'ImportoDaVersare

        contenuto = contenuto.Replace("@MESE1@", Testo)

        Testo = ""
        Testo = Testo & "<iv:NumeroModulo>2</iv:NumeroModulo>"
        Testo = Testo & "<iv:Mese>" & Txt_Mese2.Text & "</iv:Mese>"
        If Txt_TotaleOperazioniAttive2.Text <> "" Then
            If CDbl(Txt_TotaleOperazioniAttive2.Text) <> 0 Then
                Testo = Testo & "<iv:TotaleOperazioniAttive>" & Txt_TotaleOperazioniAttive2.Text.Replace(".", "") & "</iv:TotaleOperazioniAttive>"
            End If
        End If
        If Txt_TotaleOperazioniPassive2.Text <> "" Then
            If CDbl(Txt_TotaleOperazioniPassive2.Text) <> 0 Then
                Testo = Testo & "<iv:TotaleOperazioniPassive>" & Txt_TotaleOperazioniPassive2.Text.Replace(".", "") & "</iv:TotaleOperazioniPassive>"
            End If
        End If
        If Txt_IvaEsigibile2.Text <> "" Then
            If CDbl(Txt_IvaEsigibile2.Text) <> 0 Then
                Testo = Testo & "<iv:IvaEsigibile>" & Txt_IvaEsigibile2.Text.Replace(".", "") & "</iv:IvaEsigibile>"
            End If
        End If
        If Txt_IvaDetratta2.Text <> "" Then
            If CDbl(Txt_IvaDetratta2.Text) <> 0 Then
                Testo = Testo & "<iv:IvaDetratta>" & Txt_IvaDetratta2.Text.Replace(".", "") & "</iv:IvaDetratta>"
            End If
        End If
        If Txt_IvaCredito2.Text <> "" Then
            If CDbl(Txt_IvaCredito2.Text) <> 0 Then
                Testo = Testo & "<iv:IvaCredito>" & Txt_IvaCredito2.Text.Replace(".", "") & "</iv:IvaCredito>"
            End If
        End If
        If Txt_IvaDovuta2.Text <> "" Then
            If CDbl(Txt_IvaDovuta2.Text) <> 0 Then
                Testo = Testo & "<iv:IvaDovuta>" & Txt_IvaDovuta2.Text.Replace(".", "") & "</iv:IvaDovuta>"
            End If
        End If
        If Txt_CreditoPeriodoPrecedente2.Text <> "" Then
            If CDbl(Txt_CreditoPeriodoPrecedente2.Text) <> 0 Then
                Testo = Testo & "<iv:CreditoPeriodoPrecedente>" & Txt_CreditoPeriodoPrecedente2.Text.Replace(".", "") & "</iv:CreditoPeriodoPrecedente>"
            End If
        End If

        If Txt_Accontodovuto2.Text <> "" Then
            If CDbl(Txt_Accontodovuto2.Text) <> 0 Then
                Testo = Testo & "<iv:Accontodovuto>" & Txt_Accontodovuto1.Text.Replace(".", "") & "</iv:Accontodovuto>"
            End If
        End If

        If Txt_ImportoACredito2.Text <> "" Then
            If CDbl(Txt_ImportoACredito2.Text) <> 0 Then
                Testo = Testo & "<iv:ImportoACredito>" & Txt_ImportoACredito2.Text.Replace(".", "") & "</iv:ImportoACredito>"
            End If
        End If

        If Txt_ImportoDaVersare2.Text <> "" Then
            If CDbl(Txt_ImportoDaVersare2.Text) <> 0 Then
                Testo = Testo & "<iv:ImportoDaVersare>" & Txt_ImportoDaVersare2.Text.Replace(".", "") & "</iv:ImportoDaVersare>"
            End If
        End If


        contenuto = contenuto.Replace("@MESE2@", Testo)

        Testo = ""
        Testo = Testo & "<iv:NumeroModulo>3</iv:NumeroModulo>"

        Testo = Testo & "<iv:Mese>" & Txt_Mese3.Text & "</iv:Mese>"
        If Txt_TotaleOperazioniAttive3.Text <> "" Then
            If CDbl(Txt_TotaleOperazioniAttive3.Text) <> 0 Then
                Testo = Testo & "<iv:TotaleOperazioniAttive>" & Txt_TotaleOperazioniAttive3.Text.Replace(".", "") & "</iv:TotaleOperazioniAttive>"
            End If
        End If
        If Txt_TotaleOperazioniPassive3.Text <> "" Then
            If CDbl(Txt_TotaleOperazioniPassive3.Text) > 0 Then
                Testo = Testo & "<iv:TotaleOperazioniPassive>" & Txt_TotaleOperazioniPassive3.Text.Replace(".", "") & "</iv:TotaleOperazioniPassive>"
            End If            
        End If
        If Txt_IvaEsigibile3.Text <> "" Then
            If CDbl(Txt_IvaEsigibile3.Text) <> 0 Then
                Testo = Testo & "<iv:IvaEsigibile>" & Txt_IvaEsigibile3.Text.Replace(".", "") & "</iv:IvaEsigibile>"
            End If
        End If
        If Txt_IvaDetratta3.Text <> "" Then
            If CDbl(Txt_IvaDetratta3.Text) <> 0 Then
                Testo = Testo & "<iv:IvaDetratta>" & Txt_IvaDetratta3.Text.Replace(".", "") & "</iv:IvaDetratta>"
            End If
        End If
        If Txt_IvaCredito3.Text <> "" Then
            If CDbl(Txt_IvaCredito3.Text) <> 0 Then
                Testo = Testo & "<iv:IvaCredito>" & Txt_IvaCredito3.Text.Replace(".", "") & "</iv:IvaCredito>"
            End If
        End If
        If TXt_IvaDovuta3.Text <> "" Then
            If CDbl(TXt_IvaDovuta3.Text) <> 0 Then
                Testo = Testo & "<iv:IvaDovuta>" & TXt_IvaDovuta3.Text.Replace(".", "") & "</iv:IvaDovuta>"
            End If
        End If
        If Txt_CreditoPeriodoPrecedente3.Text <> "" Then
            If CDbl(Txt_CreditoPeriodoPrecedente3.Text) <> 0 Then
                Testo = Testo & "<iv:CreditoPeriodoPrecedente>" & Txt_CreditoPeriodoPrecedente3.Text.Replace(".", "") & "</iv:CreditoPeriodoPrecedente>"
            End If
        End If


        If Txt_Accontodovuto3.Text <> "" Then
            If CDbl(Txt_Accontodovuto3.Text) <> 0 Then
                Testo = Testo & "<iv:Accontodovuto>" & Txt_Accontodovuto3.Text.Replace(".", "") & "</iv:Accontodovuto>"
            End If
        End If

        If Txt_ImportoACredito3.Text <> "" Then
            If CDbl(Txt_ImportoACredito3.Text) <> 0 Then
                Testo = Testo & "<iv:ImportoACredito>" & Txt_ImportoACredito3.Text.Replace(".", "") & "</iv:ImportoACredito>"
            End If
        End If
        If Txt_ImportoDaVersare3.Text <> "" Then
            If CDbl(Txt_ImportoDaVersare3.Text) <> 0 Then
                Testo = Testo & "<iv:ImportoDaVersare>" & Txt_ImportoDaVersare3.Text.Replace(".", "") & "</iv:ImportoDaVersare>"
            End If
        End If


        contenuto = contenuto.Replace("@MESE3@", Testo)


        Dim sb As New StringBuilder()
        'sb.Append("<html>")
        'sb.AppendFormat("<body onload='document.forms[""form""].submit()'>")
        'sb.AppendFormat("<form name='form' action='http://5.249.153.145/DaHtmlaPdf/dagetapdf.aspx?&VERIFICA=" & SHA1("SAURO" & contenuto & "GABELLINI") & "' method='post'>")
        'sb.AppendFormat("<textarea name='URLHTML' style='display:none;'>" & contenuto & "</textarea>")

        'sb.Append("</form>")
        'sb.Append("</body>")
        'sb.Append("</html>")

        Response.Clear()
        Response.AddHeader("content-disposition", "attachment;filename=IT" & Txt_PartitaIVA.Text & "_LI_" & Format(1, "00000") & ".xml")
        Response.Charset = String.Empty
        Response.Cache.SetCacheability(HttpCacheability.NoCache)
        Response.ContentType = "text/xml"
        Response.ContentEncoding = Encoding.UTF8
        REM Response.ContentEncoding = Encoding.UTF16
        Response.ContentType = "text/xml; charset=utf-8"
        REM Response.ContentType = "text/xml; charset=utf-16"
        Response.Write(contenuto)
        Response.End()

    End Sub

    Protected Sub Btn_CaricaValori_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles Btn_CaricaValori.Click
        Dim Liquidazione As New Cls_LiquidazioneIva


        Liquidazione.DC_GENERALE = Session("DC_GENERALE")
        Liquidazione.DC_TABELLE = Session("DC_TABELLE")

        Liquidazione.STAMPAFINZIARIA = Session("STAMPEFINANZIARIA")
        Liquidazione.PRINTERKEY = Format(Now, "yyyyMMddHHmmss") & Session.SessionID

        Liquidazione.Gruppo = DD_Gruppo.SelectedValue
        Liquidazione.GruppoDescrizione = DD_Gruppo.SelectedItem.Text


        Liquidazione.MeseDescrizione = Txt_Mese1.Text
        Dim Errori As String


        Dim Cn As New OleDbConnection

        Cn.ConnectionString = Liquidazione.STAMPAFINZIARIA
        Cn.Open()

        Dim MyDel As New OleDbCommand

        MyDel.CommandText = "DELETE From [StampaLiquidazione] Where PRINTERKEY =? "
        MyDel.Parameters.AddWithValue("@PRINTERKEY", Liquidazione.PRINTERKEY)
        MyDel.Connection = Cn
        MyDel.ExecuteNonQuery()


        Liquidazione.StampaVisualizzaLiquidazioneIVA(Txt_AnnoImposta.Text, Txt_Mese1.Text, Errori, False)

        Dim MyCmd As New OleDbCommand

        MyCmd.Connection = Cn
        MyCmd.CommandText = "Select Sum(Imponibile) + Sum(Indetraibile) + Sum(Esenti) + Sum(Nonsoggetti)  + Sum(NonImpobile)  From [StampaLiquidazione] Where PRINTERKEY =? And VA = 'V' "
        MyCmd.Parameters.AddWithValue("@PRINTERKEY", Liquidazione.PRINTERKEY)
        Dim RdRead As OleDbDataReader = MyCmd.ExecuteReader
        If RdRead.Read Then
            Txt_TotaleOperazioniAttive1.Text = Format(campodbN(RdRead.Item(0)), "#,##0.00")
        End If
        RdRead.Close()

        MyCmd.Parameters.Clear()
        MyCmd.Connection = Cn
        MyCmd.CommandText = "Select Sum(Imponibile) + Sum(Indetraibile) + Sum(Esenti) + Sum(Nonsoggetti)  + Sum(NonImpobile)   From [StampaLiquidazione] Where PRINTERKEY =? And VA = 'A' "
        MyCmd.Parameters.AddWithValue("@PRINTERKEY", Liquidazione.PRINTERKEY)
        RdRead = MyCmd.ExecuteReader
        If RdRead.Read Then
            Txt_TotaleOperazioniPassive1.Text = Format(campodbN(RdRead.Item(0)), "#,##0.00")
        End If
        RdRead.Close()


        MyCmd.Parameters.Clear()
        MyCmd.Connection = Cn
        MyCmd.CommandText = "Select IvaEsigibile  From [StampaLiquidazione] Where PRINTERKEY =? "
        MyCmd.Parameters.AddWithValue("@PRINTERKEY", Liquidazione.PRINTERKEY)
        RdRead = MyCmd.ExecuteReader
        If RdRead.Read Then
            Txt_IvaEsigibile1.Text = Format(campodbN(RdRead.Item(0)), "#,##0.00")
        End If
        RdRead.Close()

        Txt_IvaDetratta1.Text = 0
        MyCmd.Parameters.Clear()
        MyCmd.Connection = Cn
        MyCmd.CommandText = "Select IvaDetraibile  From [StampaLiquidazione] Where PRINTERKEY =? "
        MyCmd.Parameters.AddWithValue("@PRINTERKEY", Liquidazione.PRINTERKEY)
        RdRead = MyCmd.ExecuteReader
        If RdRead.Read Then
            Txt_IvaDetratta1.Text = Format(campodbN(RdRead.Item(0)), "#,##0.00")
        End If
        RdRead.Close()

        'if {StampaLiquidazione.ImportoProgr} < 0  then "Credito d'imposta periodo precedente : " else
        'if {StampaLiquidazione.ImportoProgr} > 0  then "Debito d'imposta periodo precedente : "
        MyCmd.Parameters.Clear()
        MyCmd.Connection = Cn
        MyCmd.CommandText = "Select ImportoProgr From [StampaLiquidazione] Where PRINTERKEY =? "
        MyCmd.Parameters.AddWithValue("@PRINTERKEY", Liquidazione.PRINTERKEY)
        RdRead = MyCmd.ExecuteReader
        If RdRead.Read Then
            If campodbN(RdRead.Item(0)) > 0 Then
                Txt_DebitoPeriodoPrecedente1.Text = Format(Math.Abs(campodbN(RdRead.Item(0))), "#,##0.00")
                Txt_DebitoPeriodoPrecedente1.Text = 0
            Else
                Txt_CreditoPeriodoPrecedente1.Text = Format(Math.Abs(campodbN(RdRead.Item(0))), "#,##0.00")
                Txt_DebitoPeriodoPrecedente1.Text = 0
            End If

        End If
        RdRead.Close()



        'if {Stam\paLiquidazione.IvaDebitoCredito} < 0  then "Credito d'imposta periodo in corso : " else
        'if {StampaLiquidazione.IvaDebitoCredito} > 0  then "Debito d'imposta periodo in corso : "
        MyCmd.Parameters.Clear()
        MyCmd.Connection = Cn
        MyCmd.CommandText = "Select IvaDebitoCredito From [StampaLiquidazione] Where PRINTERKEY =? "
        MyCmd.Parameters.AddWithValue("@PRINTERKEY", Liquidazione.PRINTERKEY)
        RdRead = MyCmd.ExecuteReader
        If RdRead.Read Then
            If campodbN(RdRead.Item(0)) > 0 Then
                Txt_IvaDovuta1.Text = Format(Math.Abs(campodbN(RdRead.Item(0))), "#,##0.00")
                Txt_IvaCredito1.Text = 0
            Else
                Txt_IvaCredito1.Text = Format(Math.Abs(campodbN(RdRead.Item(0))), "#,##0.00")
                Txt_IvaDovuta1.Text = 0
            End If

        End If
        RdRead.Close()


        Txt_ImportoDaVersare1.Text = 0
        MyCmd.Parameters.Clear()
        MyCmd.Connection = Cn
        MyCmd.CommandText = "Select ImportoDaVersare From [StampaLiquidazione] Where PRINTERKEY =? "
        MyCmd.Parameters.AddWithValue("@PRINTERKEY", Liquidazione.PRINTERKEY)
        RdRead = MyCmd.ExecuteReader
        If RdRead.Read Then
            Txt_ImportoDaVersare1.Text = Format(Math.Abs(campodbN(RdRead.Item(0))), "#,##0.00")
        End If
        RdRead.Close()

        Txt_ImportoACredito1.Text = 0
        MyCmd.Parameters.Clear()
        MyCmd.Connection = Cn
        MyCmd.CommandText = "Select IvaDovutaDritta From [StampaLiquidazione] Where PRINTERKEY =? "
        MyCmd.Parameters.AddWithValue("@PRINTERKEY", Liquidazione.PRINTERKEY)
        RdRead = MyCmd.ExecuteReader
        If RdRead.Read Then
            If campodbN(RdRead.Item(0)) < 0 Then
                Txt_ImportoACredito1.Text = Format(Math.Abs(campodbN(RdRead.Item(0))), "#,##0.00")
            End If
        End If
        RdRead.Close()


        REM ************************************************************************************************
        REM * 2°
        REM ************************************************************************************************



        MyDel.ExecuteNonQuery()

        Liquidazione.StampaVisualizzaLiquidazioneIVA(Txt_AnnoImposta.Text, Txt_Mese2.Text, Errori, False)



        MyCmd.Parameters.Clear()
        MyCmd.Connection = Cn
        MyCmd.CommandText = "Select Sum(Imponibile) + Sum(Indetraibile) + Sum(Esenti) + Sum(Nonsoggetti)  + Sum(NonImpobile)  From [StampaLiquidazione] Where PRINTERKEY =? And VA = 'V' "
        MyCmd.Parameters.AddWithValue("@PRINTERKEY", Liquidazione.PRINTERKEY)
        RdRead = MyCmd.ExecuteReader
        If RdRead.Read Then
            Txt_TotaleOperazioniAttive2.Text = Format(campodbN(RdRead.Item(0)), "#,##0.00")
        End If
        RdRead.Close()

        MyCmd.Parameters.Clear()
        MyCmd.Connection = Cn
        MyCmd.CommandText = "Select Sum(Imponibile) + Sum(Indetraibile) + Sum(Esenti) + Sum(Nonsoggetti)  + Sum(NonImpobile)   From [StampaLiquidazione] Where PRINTERKEY =? And VA = 'A' "
        MyCmd.Parameters.AddWithValue("@PRINTERKEY", Liquidazione.PRINTERKEY)
        RdRead = MyCmd.ExecuteReader
        If RdRead.Read Then
            Txt_TotaleOperazioniPassive2.Text = Format(campodbN(RdRead.Item(0)), "#,##0.00")
        End If
        RdRead.Close()


        MyCmd.Parameters.Clear()
        MyCmd.Connection = Cn
        MyCmd.CommandText = "Select IvaEsigibile  From [StampaLiquidazione] Where PRINTERKEY =? "
        MyCmd.Parameters.AddWithValue("@PRINTERKEY", Liquidazione.PRINTERKEY)
        RdRead = MyCmd.ExecuteReader
        If RdRead.Read Then
            Txt_IvaEsigibile2.Text = Format(campodbN(RdRead.Item(0)), "#,##0.00")
        End If
        RdRead.Close()

        Txt_IvaDetratta2.Text = 0
        MyCmd.Parameters.Clear()
        MyCmd.Connection = Cn
        MyCmd.CommandText = "Select IvaDetraibile  From [StampaLiquidazione] Where PRINTERKEY =? "
        MyCmd.Parameters.AddWithValue("@PRINTERKEY", Liquidazione.PRINTERKEY)
        RdRead = MyCmd.ExecuteReader
        If RdRead.Read Then
            Txt_IvaDetratta2.Text = Format(campodbN(RdRead.Item(0)), "#,##0.00")
        End If
        RdRead.Close()

        'if {StampaLiquidazione.ImportoProgr} < 0  then "Credito d'imposta periodo precedente : " else
        'if {StampaLiquidazione.ImportoProgr} > 0  then "Debito d'imposta periodo precedente : "
        MyCmd.Parameters.Clear()
        MyCmd.Connection = Cn
        MyCmd.CommandText = "Select ImportoProgr From [StampaLiquidazione] Where PRINTERKEY =? "
        MyCmd.Parameters.AddWithValue("@PRINTERKEY", Liquidazione.PRINTERKEY)
        RdRead = MyCmd.ExecuteReader
        If RdRead.Read Then
            If campodbN(RdRead.Item(0)) > 0 Then
                Txt_DebitoPeriodoPrecedente2.Text = Format(Math.Abs(campodbN(RdRead.Item(0))), "#,##0.00")
                Txt_DebitoPeriodoPrecedente2.Text = 0
            Else
                Txt_CreditoPeriodoPrecedente2.Text = Format(Math.Abs(campodbN(RdRead.Item(0))), "#,##0.00")
                Txt_DebitoPeriodoPrecedente2.Text = 0
            End If

        End If
        RdRead.Close()



        'if {Stam\paLiquidazione.IvaDebitoCredito} < 0  then "Credito d'imposta periodo in corso : " else
        'if {StampaLiquidazione.IvaDebitoCredito} > 0  then "Debito d'imposta periodo in corso : "
        MyCmd.Parameters.Clear()
        MyCmd.Connection = Cn
        MyCmd.CommandText = "Select IvaDebitoCredito From [StampaLiquidazione] Where PRINTERKEY =? "
        MyCmd.Parameters.AddWithValue("@PRINTERKEY", Liquidazione.PRINTERKEY)
        RdRead = MyCmd.ExecuteReader
        If RdRead.Read Then
            If campodbN(RdRead.Item(0)) > 0 Then
                Txt_IvaDovuta2.Text = Format(Math.Abs(campodbN(RdRead.Item(0))), "#,##0.00")
                Txt_IvaCredito2.Text = 0
            Else
                Txt_IvaCredito2.Text = Format(Math.Abs(campodbN(RdRead.Item(0))), "#,##0.00")
                Txt_IvaDovuta2.Text = 0
            End If

        End If
        RdRead.Close()


        Txt_ImportoDaVersare2.Text = 0
        MyCmd.Parameters.Clear()
        MyCmd.Connection = Cn
        MyCmd.CommandText = "Select ImportoDaVersare From [StampaLiquidazione] Where PRINTERKEY =? "
        MyCmd.Parameters.AddWithValue("@PRINTERKEY", Liquidazione.PRINTERKEY)
        RdRead = MyCmd.ExecuteReader
        If RdRead.Read Then
            Txt_ImportoDaVersare2.Text = Format(Math.Abs(campodbN(RdRead.Item(0))), "#,##0.00")
        End If
        RdRead.Close()

        Txt_ImportoACredito2.Text = 0
        MyCmd.Parameters.Clear()
        MyCmd.Connection = Cn
        MyCmd.CommandText = "Select IvaDovutaDritta From [StampaLiquidazione] Where PRINTERKEY =? "
        MyCmd.Parameters.AddWithValue("@PRINTERKEY", Liquidazione.PRINTERKEY)
        RdRead = MyCmd.ExecuteReader
        If RdRead.Read Then
            If campodbN(RdRead.Item(0)) < 0 Then
                Txt_ImportoACredito2.Text = Format(Math.Abs(campodbN(RdRead.Item(0))), "#,##0.00")
            End If
        End If
        RdRead.Close()



        REM ************************************************************************************************
        REM * 3°
        REM ************************************************************************************************


        MyDel.ExecuteNonQuery()

        Liquidazione.StampaVisualizzaLiquidazioneIVA(Txt_AnnoImposta.Text, Txt_Mese3.Text, Errori, False)



        MyCmd.Parameters.Clear()
        MyCmd.Connection = Cn
        MyCmd.CommandText = "Select Sum(Imponibile) + Sum(Indetraibile) + Sum(Esenti) + Sum(Nonsoggetti)  + Sum(NonImpobile)  From [StampaLiquidazione] Where PRINTERKEY =? And VA = 'V' "
        MyCmd.Parameters.AddWithValue("@PRINTERKEY", Liquidazione.PRINTERKEY)
        RdRead = MyCmd.ExecuteReader
        If RdRead.Read Then
            Txt_TotaleOperazioniAttive3.Text = Format(campodbN(RdRead.Item(0)), "#,##0.00")
        End If
        RdRead.Close()

        MyCmd.Parameters.Clear()
        MyCmd.Connection = Cn
        MyCmd.CommandText = "Select Sum(Imponibile) + Sum(Indetraibile) + Sum(Esenti) + Sum(Nonsoggetti)  + Sum(NonImpobile)   From [StampaLiquidazione] Where PRINTERKEY =? And VA = 'A' "
        MyCmd.Parameters.AddWithValue("@PRINTERKEY", Liquidazione.PRINTERKEY)
        RdRead = MyCmd.ExecuteReader
        If RdRead.Read Then
            Txt_TotaleOperazioniPassive3.Text = Format(campodbN(RdRead.Item(0)), "#,##0.00")
        End If
        RdRead.Close()


        MyCmd.Parameters.Clear()
        MyCmd.Connection = Cn
        MyCmd.CommandText = "Select IvaEsigibile  From [StampaLiquidazione] Where PRINTERKEY =? "
        MyCmd.Parameters.AddWithValue("@PRINTERKEY", Liquidazione.PRINTERKEY)
        RdRead = MyCmd.ExecuteReader
        If RdRead.Read Then
            Txt_IvaEsigibile3.Text = Format(campodbN(RdRead.Item(0)), "#,##0.00")
        End If
        RdRead.Close()

        Txt_IvaDetratta3.Text = 0
        MyCmd.Parameters.Clear()
        MyCmd.Connection = Cn
        MyCmd.CommandText = "Select IvaDetraibile  From [StampaLiquidazione] Where PRINTERKEY =? "
        MyCmd.Parameters.AddWithValue("@PRINTERKEY", Liquidazione.PRINTERKEY)
        RdRead = MyCmd.ExecuteReader
        If RdRead.Read Then
            Txt_IvaDetratta3.Text = Format(campodbN(RdRead.Item(0)), "#,##0.00")
        End If
        RdRead.Close()

        'if {StampaLiquidazione.ImportoProgr} < 0  then "Credito d'imposta periodo precedente : " else
        'if {StampaLiquidazione.ImportoProgr} > 0  then "Debito d'imposta periodo precedente : "
        MyCmd.Parameters.Clear()
        MyCmd.Connection = Cn
        MyCmd.CommandText = "Select ImportoProgr From [StampaLiquidazione] Where PRINTERKEY =? "
        MyCmd.Parameters.AddWithValue("@PRINTERKEY", Liquidazione.PRINTERKEY)
        RdRead = MyCmd.ExecuteReader
        If RdRead.Read Then
            If campodbN(RdRead.Item(0)) > 0 Then
                Txt_DebitoPeriodoPrecedente3.Text = Format(Math.Abs(campodbN(RdRead.Item(0))), "#,##0.00")
                Txt_DebitoPeriodoPrecedente3.Text = 0
            Else
                Txt_CreditoPeriodoPrecedente3.Text = Format(Math.Abs(campodbN(RdRead.Item(0))), "#,##0.00")
                Txt_DebitoPeriodoPrecedente3.Text = 0
            End If

        End If
        RdRead.Close()



        'if {Stam\paLiquidazione.IvaDebitoCredito} < 0  then "Credito d'imposta periodo in corso : " else
        'if {StampaLiquidazione.IvaDebitoCredito} > 0  then "Debito d'imposta periodo in corso : "
        MyCmd.Parameters.Clear()
        MyCmd.Connection = Cn
        MyCmd.CommandText = "Select IvaDebitoCredito From [StampaLiquidazione] Where PRINTERKEY =? "
        MyCmd.Parameters.AddWithValue("@PRINTERKEY", Liquidazione.PRINTERKEY)
        RdRead = MyCmd.ExecuteReader
        If RdRead.Read Then
            If campodbN(RdRead.Item(0)) > 0 Then
                TXt_IvaDovuta3.Text = Format(Math.Abs(campodbN(RdRead.Item(0))), "#,##0.00")
                Txt_IvaCredito3.Text = 0
            Else
                Txt_IvaCredito3.Text = Format(Math.Abs(campodbN(RdRead.Item(0))), "#,##0.00")
                TXt_IvaDovuta3.Text = 0
            End If

        End If
        RdRead.Close()


        Txt_ImportoDaVersare3.Text = 0
        MyCmd.Parameters.Clear()
        MyCmd.Connection = Cn
        MyCmd.CommandText = "Select ImportoDaVersare From [StampaLiquidazione] Where PRINTERKEY =? "
        MyCmd.Parameters.AddWithValue("@PRINTERKEY", Liquidazione.PRINTERKEY)
        RdRead = MyCmd.ExecuteReader
        If RdRead.Read Then
            Txt_ImportoDaVersare3.Text = Format(Math.Abs(campodbN(RdRead.Item(0))), "#,##0.00")
        End If
        RdRead.Close()

        Txt_ImportoACredito3.Text = 0
        MyCmd.Parameters.Clear()
        MyCmd.Connection = Cn
        MyCmd.CommandText = "Select IvaDovutaDritta From [StampaLiquidazione] Where PRINTERKEY =? "
        MyCmd.Parameters.AddWithValue("@PRINTERKEY", Liquidazione.PRINTERKEY)
        RdRead = MyCmd.ExecuteReader
        If RdRead.Read Then
            If campodbN(RdRead.Item(0)) < 0 Then
                Txt_ImportoACredito3.Text = Format(Math.Abs(campodbN(RdRead.Item(0))), "#,##0.00")
            End If
        End If
        RdRead.Close()


        MyDel.ExecuteNonQuery()

        Cn.Close()



    End Sub

    Function campodb(ByVal oggetto As Object) As String
        If IsDBNull(oggetto) Then
            Return ""
        Else
            Return oggetto
        End If
    End Function

    Function campodbN(ByVal oggetto As Object) As Double
        If IsDBNull(oggetto) Then
            Return 0
        Else
            Return oggetto
        End If
    End Function

    Protected Sub Btn_Esci_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles Btn_Esci.Click
        Response.Redirect("Menu_Servizi.aspx")
    End Sub
End Class
