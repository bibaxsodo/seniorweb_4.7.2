﻿Imports System.Data.OleDb

Partial Class GeneraleWeb_StampaReversali
    Inherits System.Web.UI.Page
    Dim Tabella As New System.Data.DataTable("tabella")

    Function campodbd(ByVal oggetto As Object) As Date
        If IsDBNull(oggetto) Then
            Return Nothing
        Else
            Return oggetto
        End If
    End Function


    Function campodb(ByVal oggetto As Object) As String
        If IsDBNull(oggetto) Then
            Return ""
        Else
            Return oggetto
        End If
    End Function

    Function campodbN(ByVal oggetto As Object) As Double
        If IsDBNull(oggetto) Then
            Return 0
        Else
            Return oggetto
        End If
    End Function



    Private Sub EseguiJS()
        Dim MyJs As String
        MyJs = "$(document).ready(function() {"

        MyJs = MyJs & "var els = document.getElementsByTagName(""*"");"

        MyJs = MyJs & "for (var i=0;i<els.length;i++)"
        MyJs = MyJs & "if ( els[i].id ) { "
        MyJs = MyJs & " var appoggio =els[i].id; "

        MyJs = MyJs & " if ((appoggio.match('Txt_DallaData')!= null) || (appoggio.match('Txt_AllaData')!= null) || (appoggio.match('Txt_DataReversale') != null)) {  "
        MyJs = MyJs & " $(els[i]).mask(""99/99/9999"");  "

        MyJs = MyJs & " $(els[i]).datepicker({ changeMonth: true,changeYear: true,  yearRange: ""1890:" & Year(Now) + 1 & """},$.datepicker.regional[""it""]);"
        MyJs = MyJs & "    }"


        MyJs = MyJs & "} "
        MyJs = MyJs & "});"

        ScriptManager.RegisterStartupScript(Me, Me.GetType(), "visualizzaRitIm", MyJs, True)
    End Sub

    Protected Sub Btn_Aggiorna_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles Btn_Aggiorna.Click
        Dim cn As OleDbConnection        
        Dim Condizione As String


        Dim XPar As New Cls_ParametriGenerale


        XPar.LeggiParametri(Session("DC_GENERALE"))


        Dim Appoggio(1000) As String
        Dim Indice As Integer = 0
        Dim x As Integer = 0

        Condizione = ""
        If Not IsNothing(XPar.CausaliReversali) Then
            Appoggio = SplitWords(XPar.CausaliReversali)

            For Indice = 0 To Appoggio.Length - 1
                If Not IsNothing(Appoggio(Indice)) Then
                    If Condizione <> "" Then Condizione = Condizione & " Or "
                    Condizione = Condizione & " CausaleContabile = '" & Appoggio(Indice) & "' "
                End If
            Next
        End If

        If Condizione <> "" Then
            Condizione = " And (" & Condizione & " ) "
        End If


        Tabella.Clear()
        Tabella.Columns.Clear()        
        Tabella.Columns.Add("Seleziona", GetType(String))
        Tabella.Columns.Add("NumeroRegistrazione", GetType(String))
        Tabella.Columns.Add("DataRegistrazione", GetType(String))
        Tabella.Columns.Add("Cliente", GetType(String))
        Tabella.Columns.Add("NumeroReversale", GetType(String))
        Tabella.Columns.Add("DataReversale", GetType(String))
        Tabella.Columns.Add("CausaleContabile", GetType(String))
        Tabella.Columns.Add("Descrizione", GetType(String))


        Dim MastroCli As Long
        Dim ContoCli As Long
        Dim SottoContoCli As Long
        Dim DecoEDes As String

        cn = New Data.OleDb.OleDbConnection(Session("DC_GENERALE"))


        Dim DataDal As Date
        Dim DataAl As Date

        DataDal = Txt_DallaData.Text
        DataAl = Txt_AllaData.Text

        cn.Open()
        Dim cmd As New OleDbCommand()

        If Chk_SoloNonGenerate.Checked = True Then
            cmd.CommandText = "Select * From MovimentiContabiliTesta Where NumeroReversale = 0 And ((NumeroReversale > 0 And (TipoStorno  = '' OR TipoStorno Is Null)) Or (NumeroReversale Is Null Or NumeroReversale = 0) ) And (NumeroMandato= 0 or NumeroMandato is null) And  DataRegistrazione >= {ts '" & Format(DataDal, "yyyy-MM-dd") & " 00:00:00'} And DataRegistrazione <= {ts '" & Format(DataAl, "yyyy-MM-dd") & " 00:00:00'} " & Condizione & " Order by year(DataReversale), NumeroReversale,NumeroRegistrazione"
        Else
            cmd.CommandText = "Select * From MovimentiContabiliTesta Where ((NumeroReversale > 0 And (TipoStorno  = '' OR TipoStorno Is Null)) Or (NumeroReversale Is Null Or NumeroReversale = 0) ) And (NumeroMandato= 0 or NumeroMandato is null) And  DataRegistrazione >= {ts '" & Format(DataDal, "yyyy-MM-dd") & " 00:00:00'} And DataRegistrazione <= {ts '" & Format(DataAl, "yyyy-MM-dd") & " 00:00:00'} " & Condizione & " Order by year(DataReversale), NumeroReversale,NumeroRegistrazione"
        End If
        cmd.Connection = cn
        Dim myPOSTreader As OleDbDataReader = cmd.ExecuteReader()

        Do While myPOSTreader.Read
            Dim K As New Cls_MovimentoContabile

            K.Leggi(Session("DC_GENERALE"), campodb(myPOSTreader.Item("NumeroRegistrazione")))

            Dim i As Integer

            For i = 0 To K.Righe.Length - 1
                If Not IsNothing(K.Righe(i)) Then
                    If K.Righe(i).RigaDaCausale = 1 Then
                        MastroCli = K.Righe(i).MastroPartita
                        ContoCli = K.Righe(i).ContoPartita
                        SottoContoCli = K.Righe(i).SottocontoPartita
                    End If
                End If
            Next

            Dim xDec As New Cls_Pianodeiconti

            xDec.Mastro = MastroCli
            xDec.Conto = ContoCli
            xDec.Sottoconto = SottoContoCli
            xDec.Decodfica(Session("DC_GENERALE"))
            DecoEDes = xDec.Descrizione


            Dim myriga As System.Data.DataRow = Tabella.NewRow()
            myriga(0) = 0
            myriga(1) = campodb(myPOSTreader.Item("NumeroRegistrazione"))

            If Year(campodbd(myPOSTreader.Item("DataRegistrazione"))) < 1910 Then
                myriga(2) = ""
            Else
                myriga(2) = Format(campodbd(myPOSTreader.Item("DataRegistrazione")), "dd/MM/yyyy")
            End If
            myriga(3) = DecoEDes
            myriga(4) = campodb(myPOSTreader.Item("NumeroReversale"))

            If Year(campodbd(myPOSTreader.Item("DataReversale"))) < 1910 Then
                myriga(5) = ""
            Else
                myriga(5) = Format(campodbd(myPOSTreader.Item("DataReversale")), "dd/MM/yyyy")
            End If

            'myriga(5) = campodbd(myPOSTreader.Item("DataReversale"))

            Dim DCau As New Cls_CausaleContabile

            DCau.Codice = campodb(myPOSTreader.Item("CausaleContabile"))
            DCau.Leggi(Session("DC_TABELLE"), campodb(myPOSTreader.Item("CausaleContabile")))

            myriga(6) = DCau.Descrizione
            myriga(7) = campodb(myPOSTreader.Item("Descrizione"))

            Tabella.Rows.Add(myriga)

        Loop
        myPOSTreader.Close()

        cn.Close()


        Grid.AutoGenerateColumns = False
        Grid.DataSource = Tabella
        Grid.DataBind()


    End Sub

    Protected Sub Img_Stampa_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles Img_Stampa.Click
        Dim Importo As Double

        Dim Mastro As Long
        Dim Conto As Long
        Dim Sottoconto As Long

        Dim RagSoc As String
        Dim Indirizzo As String
        Dim Cap As String
        Dim Comune As String
        Dim CodiceFiscale As String
        Dim PARTITAIVA As String
        Dim CodiceInterno As Long
        Dim CCPOSTALE As String
        Dim Descrizione As String = ""
        Dim IBAN As String = ""

        Dim NumeroAtto As String
        Dim TipoAtto As String
        Dim DataAtto As Date
        Dim AnnoCollegamento As Long

        Dim AnnoPrenotazione As Long
        Dim NumeroPrenotazione As Long
        Dim NumeroDocumento As String
        Dim ImpPagato As Double
        Dim Riga As Long

        Dim NumeroBolletta As String
        Dim DataBolletta As Date

        Dim MySql As String

        Dim cn As OleDbConnection
        Dim cnOspiti As OleDbConnection

        Dim cnStampa As OleDbConnection

        If Not IsDate(Txt_DataReversale.Text) Then
            ' MsgBox("Data Reversale formalmente errata", vbInformation)
            Exit Sub
        End If


        ViewState("PRINTERKEY") = Format(Now, "yyyyMMddHHmmss") & Session.SessionID

        cnOspiti = New Data.OleDb.OleDbConnection(Session("DC_OSPITE"))

        cn = New Data.OleDb.OleDbConnection(Session("DC_GENERALE"))

        cnStampa = New Data.OleDb.OleDbConnection(Session("STAMPEFINANZIARIA"))

        cn.Open()

        cnStampa.Open()

        cnOspiti.Open()

        Tabella = ViewState("TABELLA")


        Dim cmd1 As New OleDbCommand()
        cmd1.CommandText = "Delete From ReversaliTesta"
        cmd1.Connection = cnStampa
        cmd1.ExecuteNonQuery()

        Dim cmd2 As New OleDbCommand()
        cmd2.CommandText = "Delete From ReversaliRighe"
        cmd2.Connection = cnStampa
        cmd2.ExecuteNonQuery()

        
        For I = 0 To Grid.Rows.Count - 1

            Dim Checked As CheckBox = DirectCast(Grid.Rows(I).FindControl("Chk_Seleziona"), CheckBox)

            If Checked.Checked = True Then
                Dim NumeroRegistrazione As Long
                Dim NumeroReversale As Long
                Dim DataReversale As Date
                Dim DataRegistrazione As Date
                Dim AnnoImpegno As Long
                Dim NumeroImpegno As Long
                Dim DescrizioneTesta As String


                NumeroRegistrazione = Grid.Rows(I).Cells(1).Text

                Dim cmd As New OleDbCommand()
                cmd.CommandText = "Select * From MovimenticontabiliTesta where NumeroRegistrazione = " & NumeroRegistrazione
                cmd.Connection = cn
                Dim myPOSTreader As OleDbDataReader = cmd.ExecuteReader()

                If myPOSTreader.Read Then
                    NumeroReversale = campodbN(myPOSTreader.Item("NumeroReversale"))
                    DataReversale = campodbd(myPOSTreader.Item("DataReversale"))
                    DataRegistrazione = campodbd(myPOSTreader.Item("DataRegistrazione"))

                    AnnoImpegno = campodbN(myPOSTreader.Item("AnnoImpegno"))
                    NumeroImpegno = campodbN(myPOSTreader.Item("NumeroImpegno"))
                    DescrizioneTesta = campodb(myPOSTreader.Item("Descrizione"))

                End If
                myPOSTreader.Close()

                If Val(NumeroReversale) = 0 Then
                    Dim cmdTot As New OleDbCommand()
                    cmdTot.CommandText = "Select max(NumeroReversale) as tot From MovimenticontabiliTesta Where Year(DataRegistrazione) = " & Year(DataRegistrazione)
                    cmdTot.Connection = cn
                    Dim myRdTot As OleDbDataReader = cmdTot.ExecuteReader()

                    If myRdTot.Read Then
                        NumeroReversale = campodbN(myRdTot.Item("Tot")) + 1
                        DataReversale = Txt_DataReversale.Text
                    End If
                    myRdTot.Close()

                    Dim cmdReg As New OleDbCommand()
                    cmdReg.CommandText = "Select max(NumeroReversale) as tot From MovimenticontabiliTesta Where Year(DataRegistrazione) = " & Year(DataRegistrazione)
                    cmdReg.Connection = cn
                    Dim myRdReg As OleDbDataReader = cmdReg.ExecuteReader()

                    If myRdReg.Read Then
                        NumeroReversale = campodbN(myRdReg.Item("Tot")) + 1
                        DataReversale = Txt_DataReversale.Text
                    End If
                    myRdReg.Close()

                    Dim cmdUp As New OleDbCommand()
                    cmdUp.CommandText = "UPDATE MovimenticontabiliTesta Set NumeroReversale = ?,DataReversale = ?  Where NumeroRegistrazione = ? " '& NumeroRegistrazione
                    cmdUp.Connection = cn

                    cmdUp.Parameters.AddWithValue("@NumeroReversale", NumeroReversale)
                    cmdUp.Parameters.AddWithValue("@DataReversale", DataReversale)
                    cmdUp.Parameters.AddWithValue("@NumeroRegistrazione", NumeroRegistrazione)

                    cmdUp.ExecuteNonQuery()

                End If

                Importo = 0

                Dim cmdRegR1 As New OleDbCommand()
                cmdRegR1.CommandText = "Select Importo From MovimenticontabiliRiga where  MastroPartita = 6 And ContoPartita = 30 And Numero = " & NumeroRegistrazione
                cmdRegR1.Connection = cn
                Dim myRdReg1 As OleDbDataReader = cmdRegR1.ExecuteReader()

                If myRdReg1.Read Then
                    Importo = campodbN(myRdReg1.Item("Importo"))
                End If
                myRdReg1.Close()


                If Importo = 0 Then
                    Dim cmdRegM1 As New OleDbCommand()
                    cmdRegM1.CommandText = "Select Importo From MovimenticontabiliRiga where RigaDaCausale = 1 And Numero = " & NumeroRegistrazione
                    cmdRegM1.Connection = cn
                    Dim myRdRegM1 As OleDbDataReader = cmdRegM1.ExecuteReader()

                    If myRdRegM1.Read Then
                        Importo = campodbN(myRdRegM1.Item("Importo"))
                    End If
                    myRdRegM1.Close()
                End If
                If Importo = 0 Then
                    Dim cmdRegM2 As New OleDbCommand()
                    cmdRegM2.CommandText = "Select SUM(Importo) AS IMPORTO From MovimenticontabiliRiga where DareAvere = 'D' And Numero = " & NumeroRegistrazione
                    cmdRegM2.Connection = cn
                    Dim myRdReM2 As OleDbDataReader = cmdRegM2.ExecuteReader()

                    If myRdReM2.Read Then
                        Importo = campodbN(myRdReM2.Item("Importo"))
                    End If
                    myRdReM2.Close()
                End If

                Mastro = 0
                Conto = 0
                Sottoconto = 0

                Dim cmdRegR2 As New OleDbCommand()
                cmdRegR2.CommandText = "SELECT * FROM MovimentiContabiliRiga WHERE MovimentiContabiliRiga.Tipo = 'CF' AND MovimentiContabiliRiga.Numero = " & NumeroRegistrazione & " "
                cmdRegR2.Connection = cn
                Dim myRdReg2 As OleDbDataReader = cmdRegR2.ExecuteReader()
                If myRdReg2.Read() Then
                    Mastro = campodbN(myRdReg2.Item("MastroPartita"))
                    Conto = campodbN(myRdReg2.Item("ContoPartita"))
                    Sottoconto = campodbN(myRdReg2.Item("SottocontoPartita"))
                End If
                myRdReg2.Close()

                If Mastro = 0 And Conto = 0 And Sottoconto = 0 Then
                    Dim cmdRegM2 As New OleDbCommand()
                    cmdRegM2.CommandText = "SELECT * FROM MovimentiContabiliRiga WHERE MovimentiContabiliRiga.RigaDaCausale =1  AND MovimentiContabiliRiga.Numero = " & NumeroRegistrazione & " "
                    cmdRegM2.Connection = cn
                    Dim myRdRegM2 As OleDbDataReader = cmdRegM2.ExecuteReader()
                    If myRdRegM2.Read() Then
                        Mastro = campodbN(myRdRegM2.Item("MastroPartita"))
                        Conto = campodbN(myRdRegM2.Item("ContoPartita"))
                        Sottoconto = campodbN(myRdRegM2.Item("SottocontoPartita"))
                    End If
                    myRdRegM2.Close()
                End If

                RagSoc = ""
                Indirizzo = ""
                Cap = ""
                Comune = ""
                CodiceFiscale = ""
                PARTITAIVA = 0
                CodiceInterno = 0
                CCPOSTALE = ""

                Dim ContoT As New Cls_Pianodeiconti

                ContoT.Mastro = Mastro
                ContoT.Conto = Conto
                ContoT.Sottoconto = Sottoconto
                ContoT.Decodfica(Session("DC_GENERALE"))


                MySql = ""
                If ContoT.TipoAnagrafica = "O" Or ContoT.TipoAnagrafica = "P" Then
                    If Int(Sottoconto / 100) = Sottoconto / 100 Then
                        MySql = "Select * From AnagraficaComune where CodiceOspite = " & Int(Sottoconto / 100)
                    Else
                        MySql = "Select * From AnagraficaComune where CodiceOspite = " & Int(Sottoconto / 100) & " And CodiceParente = " & (Sottoconto - (Int(Sottoconto / 100) * 100))
                    End If
                Else
                    MySql = "Select * From AnagraficaComune where MastroCliente = " & Mastro & " And ContoCliente = " & Conto & " And SottoContoCliente = " & Sottoconto
                End If

                Dim cmdAnagrafica As New OleDbCommand()
                cmdAnagrafica.CommandText = MySql
                cmdAnagrafica.Connection = cnOspiti
                Dim RdAnagrafica As OleDbDataReader = cmdAnagrafica.ExecuteReader()

                If RdAnagrafica.Read Then
                    RagSoc = campodb(RdAnagrafica.Item("Nome"))
                    Indirizzo = campodb(RdAnagrafica.Item("RESIDENZAINDIRIZZO1"))
                    Cap = campodb(RdAnagrafica.Item("RESIDENZACAP1"))

                    Dim k As New ClsComune

                    k.Comune = campodb(RdAnagrafica.Item("RESIDENZAPROVINCIA1"))
                    k.Provincia = campodb(RdAnagrafica.Item("RESIDENZACOMUNE1"))
                    k.DecodficaComune(Session("DC_OSPITE"))
                    Comune = k.Descrizione

                    CodiceFiscale = campodb(RdAnagrafica.Item("CodiceFiscale"))
                    PARTITAIVA = campodb(RdAnagrafica.Item("PARTITAIVA"))
                    CodiceInterno = campodbN(RdAnagrafica.Item("CODICEDEBITORECREDITORE"))
                    Descrizione = campodb(RdAnagrafica.Item("DescrizioneCliente"))
                    IBAN = Format(campodb(RdAnagrafica.Item("IntCliente")), "##") & Format(campodbN(RdAnagrafica.Item("NumeroControlloCliente")), "00") & Format(campodb(RdAnagrafica.Item("CinCliente")), "#") & Format(campodb(RdAnagrafica.Item("ABICliente")), "00000") & Format(campodb(RdAnagrafica.Item("CABCliente")), "00000") & Format(campodb(RdAnagrafica.Item("CCBancarioCliente")), "000000000000")
                    CCPOSTALE = campodb(RdAnagrafica.Item("CCPostaleCliente"))
                End If
                RdAnagrafica.Close()

                If RagSoc = "" Then
                    Dim dx As New Cls_Pianodeiconti
                    dx.Mastro = Mastro
                    dx.Conto = Conto
                    dx.Sottoconto = Sottoconto
                    dx.Decodfica(Session("DC_GENERALE"))
                    RagSoc = dx.Descrizione
                    Indirizzo = ""
                    Cap = ""
                    Comune = ""
                    CodiceFiscale = ""
                    PARTITAIVA = 0
                    CodiceInterno = 0
                    Descrizione = ""
                    IBAN = ""
                    CCPOSTALE = ""
                End If

                NumeroAtto = ""
                TipoAtto = ""
                DataAtto = Nothing
                AnnoCollegamento = Year(Txt_DataReversale.Text)

                Dim cmdLegami As New OleDbCommand()
                cmdLegami.CommandText = "Select * From TabellaLegami Where  CodicePagamento = " & NumeroRegistrazione
                cmdLegami.Connection = cn
                Dim RdLegami As OleDbDataReader = cmdLegami.ExecuteReader()

                If RdLegami.Read Then
                    Dim Rt As New Cls_MovimentoContabile
                    Rt.Leggi(Session("DC_GENERALE"), campodbN(RdLegami.Item("CodiceDocumento")))

                    AnnoCollegamento = Year(Rt.DataRegistrazione)
                    AnnoPrenotazione = 0
                    NumeroPrenotazione = 0

                    Dim cmdAnnoNumero As New OleDbCommand()
                    cmdAnnoNumero.CommandText = "Select * From PrenotazioniUsate Where   NumeroRegistrazione = " & campodbN(RdLegami.Item("CodiceDocumento"))
                    cmdAnnoNumero.Connection = cn
                    Dim myAnnoNumero As OleDbDataReader = cmdAnnoNumero.ExecuteReader()
                    If myAnnoNumero.Read() Then
                        AnnoPrenotazione = campodbN(myAnnoNumero.Item("AnnoPrenotazione"))
                        NumeroPrenotazione = campodbN(myAnnoNumero.Item("NumeroPrenotazione"))
                    End If
                    myAnnoNumero.Close()


                    Dim cmdAnnoNumero1 As New OleDbCommand()
                    cmdAnnoNumero1.CommandText = "Select * From PrenotazioniTesta Where  Anno = " & AnnoPrenotazione & " And  Numero = " & NumeroPrenotazione
                    cmdAnnoNumero1.Connection = cn
                    Dim myAnnoNumero1 As OleDbDataReader = cmdAnnoNumero1.ExecuteReader()
                    If myAnnoNumero1.Read() Then
                        NumeroAtto = campodbN(myAnnoNumero1.Item("NumeroAtto"))
                        TipoAtto = campodb(myAnnoNumero1.Item("TipoAtto"))
                        DataAtto = campodbd(myAnnoNumero1.Item("DataAtto"))
                    End If
                    myAnnoNumero1.Close()
                End If
                RdLegami.Close()


                If Trim(NumeroAtto) = "" And Trim(TipoAtto) = "" And IsNothing(DataAtto) Then

                    AnnoPrenotazione = 0
                    NumeroPrenotazione = 0

                    Dim cmdAnnoNumero As New OleDbCommand()
                    cmdAnnoNumero.CommandText = "Select * From PrenotazioniUsate Where   NumeroRegistrazione = " & NumeroRegistrazione
                    cmdAnnoNumero.Connection = cn
                    Dim myAnnoNumero As OleDbDataReader = cmdAnnoNumero.ExecuteReader()
                    If myAnnoNumero.Read() Then
                        AnnoPrenotazione = campodbN(myAnnoNumero.Item("AnnoPrenotazione"))
                        NumeroPrenotazione = campodbN(myAnnoNumero.Item("NumeroPrenotazione"))
                    End If
                    myAnnoNumero.Close()



                    Dim cmdAnnoNumero1 As New OleDbCommand()
                    cmdAnnoNumero1.CommandText = "Select * From PrenotazioniTesta Where  Anno = " & AnnoPrenotazione & " And  Numero = " & NumeroPrenotazione
                    cmdAnnoNumero1.Connection = cn
                    Dim myAnnoNumero1 As OleDbDataReader = cmdAnnoNumero1.ExecuteReader()
                    If myAnnoNumero.Read() Then
                        NumeroAtto = campodbN(myAnnoNumero1.Item("NumeroAtto"))
                        TipoAtto = campodb(myAnnoNumero1.Item("TipoAtto"))
                        DataAtto = campodbd(myAnnoNumero1.Item("DataAtto"))
                    End If
                    myAnnoNumero1.Close()

                End If

                If Trim(NumeroAtto) = "" And Trim(TipoAtto) = "" And IsNothing(DataAtto) Then

                    Dim cmdAnnoNumero1 As New OleDbCommand()
                    cmdAnnoNumero1.CommandText = "Select * FRom AttoBilancio Where Anno = " & AnnoCollegamento
                    cmdAnnoNumero1.Connection = cn
                    Dim myAnnoNumero1 As OleDbDataReader = cmdAnnoNumero1.ExecuteReader()
                    If myAnnoNumero1.Read() Then
                        NumeroAtto = campodbN(myAnnoNumero1.Item("NumeroAtto"))
                        TipoAtto = campodb(myAnnoNumero1.Item("TipoAtto"))
                        DataAtto = campodbd(myAnnoNumero1.Item("DataAtto"))
                    End If
                    myAnnoNumero1.Close()
                End If


                Dim cmdRevTesta As New OleDbCommand()


                cmdRevTesta.Connection = cnStampa
                cmdRevTesta.CommandText = "Insert INTO ReversaliTesta (NumeroReversale,DataReversale,Competenza,Importo,ImportoInLettere,RagSoc,Indirizzo,Cap,Comune,CodiceFiscale,PARTITAIVA,CodiceInterno,NumeroAtto,TipoAtto,DataAtto,ModalitaPagamento,IBAN,ContoCorrente,PRINTERKEY) VALUES " & _
                                " (?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)"

                cmdRevTesta.Parameters.AddWithValue("NumeroReversale", NumeroReversale)
                cmdRevTesta.Parameters.AddWithValue("DataReversale", DataReversale)
                cmdRevTesta.Parameters.AddWithValue("Competenza", Year(DataReversale))
                cmdRevTesta.Parameters.AddWithValue("Importo", Importo)

                Dim DecTesto As New Cls_ImportoInLettere


                cmdRevTesta.Parameters.AddWithValue("ImportoInLettere", DecTesto.ImportoInTesto(Importo, 50))
                cmdRevTesta.Parameters.AddWithValue("RagSoc", RagSoc)
                cmdRevTesta.Parameters.AddWithValue("Indirizzo", Indirizzo)
                cmdRevTesta.Parameters.AddWithValue("Cap", Cap)
                cmdRevTesta.Parameters.AddWithValue("Comune", Comune)
                cmdRevTesta.Parameters.AddWithValue("CodiceFiscale", CodiceFiscale)
                cmdRevTesta.Parameters.AddWithValue("PARTITAIVA", PARTITAIVA)
                cmdRevTesta.Parameters.AddWithValue("CodiceInterno", CodiceInterno)
                cmdRevTesta.Parameters.AddWithValue("NumeroAtto", NumeroAtto)
                Dim kAtto As New Cls_TipoAtto

                'DecodificaTipoAtto(TipoAtto)
                cmdRevTesta.Parameters.AddWithValue("TipoAtto", "")
                If Year(DataAtto) < 1902 Then
                    cmdRevTesta.Parameters.AddWithValue("DataAtto", Nothing)
                Else
                    cmdRevTesta.Parameters.AddWithValue("DataAtto", DataAtto)
                End If
                cmdRevTesta.Parameters.AddWithValue("ModalitaPagamento", Descrizione & vbNewLine)
                cmdRevTesta.Parameters.AddWithValue("@IBAN", IBAN)
                If Trim(CCPOSTALE) <> "" And Val(CCPOSTALE) <> 0 Then
                    cmdRevTesta.Parameters.AddWithValue("@ContoCorrente", "Conto corrente postale : " & CCPOSTALE)
                Else
                    cmdRevTesta.Parameters.AddWithValue("@ContoCorrente", "")
                End If
                cmdRevTesta.Parameters.AddWithValue("@PRINTERKEY", ViewState("PRINTERKEY"))

                cmdRevTesta.ExecuteNonQuery()



                Riga = 1

                Dim cmdLegami1 As New OleDbCommand()
                cmdLegami1.CommandText = "Select * From TabellaLegami Where   CodicePagamento = " & NumeroRegistrazione
                cmdLegami1.Connection = cn
                Dim myLegami As OleDbDataReader = cmdLegami1.ExecuteReader()
                Do While myLegami.Read()
                    NumeroDocumento = 0
                    DataRegistrazione = Nothing
                    Importo = 0
                    ImpPagato = 0

                    Dim cmdMT As New OleDbCommand()
                    cmdMT.CommandText = "Select * From TabellaLegami Where   CodicePagamento = " & NumeroRegistrazione
                    cmdMT.Connection = cn
                    Dim myMT As OleDbDataReader = cmdMT.ExecuteReader()

                    If myMT.Read Then

                        Dim kImp As New Cls_MovimentoContabile

                        kImp.Leggi(Session("DC_GENERALE"), campodbN(myMT.Item("CodiceDocumento")))

                        NumeroDocumento = kImp.NumeroDocumento
                        DataRegistrazione = kImp.DataRegistrazione

                        Importo = kImp.ImportoDocumento(Session("DC_TABELLE"))
                        ImpPagato = campodbN(myMT.Item("Importo"))
                    End If
                    myMT.Close()

                    Dim cmdRevRiga As New OleDbCommand()


                    cmdRevRiga.CommandText = "INSERT INTO ReversaliRighe (numero,Anno,Riga,NumeroDocumento,Data,Importo,ImpPagato,Riferimento,Descrizione,PRINTERKEY) " & _
                                                  " VALUES (?,?,?,?,?,?,?,?,?,?)"
                    cmdRevRiga.Connection = cnStampa

                    cmdRevRiga.Parameters.AddWithValue("@numero", NumeroReversale)
                    cmdRevRiga.Parameters.AddWithValue("@Anno", Year(DataReversale))
                    cmdRevRiga.Parameters.AddWithValue("@Riga", Riga)
                    cmdRevRiga.Parameters.AddWithValue("@NumeroDocumento", NumeroDocumento)
                    cmdRevRiga.Parameters.AddWithValue("@Data", DataRegistrazione)
                    cmdRevRiga.Parameters.AddWithValue("@Importo", Importo)
                    cmdRevRiga.Parameters.AddWithValue("@ImpPagato", ImpPagato)
                    cmdRevRiga.Parameters.AddWithValue("@Riferimento", "")

                    Dim DocTest As New Cls_MovimentoContabile

                    DocTest.Leggi(Session("DC_GENERALE"), NumeroRegistrazione)
                    NumeroBolletta = DocTest.NumeroBolletta
                    DataBolletta = DocTest.DataBolletta
                    If NumeroBolletta <> "" Then
                        cmdRevRiga.Parameters.AddWithValue("@Riferimento", " Bolletta N° " & NumeroBolletta & " del " & DataBolletta)
                    End If
                    cmdRevRiga.Parameters.AddWithValue("@Descrizione", "")

                    cmdRevRiga.Parameters.AddWithValue("@PRINTERKEY", ViewState("PRINTERKEY"))

                    cmdRevRiga.ExecuteNonQuery()


                    Riga = Riga + 1

                Loop
                myLegami.Close()

                If DescrizioneTesta <> "" And Riga > 1 Then

                    Dim cmdRevRiga As New OleDbCommand()


                    cmdRevRiga.CommandText = "INSERT INTO ReversaliRighe (numero,Anno,Riga,NumeroDocumento,Data,Importo,ImpPagato,Riferimento,Descrizione,PRINTERKEY) " & _
                                                  " VALUES (?,?,?,?,?,?,?,?,?,?)"
                    cmdRevRiga.Connection = cnStampa

                    cmdRevRiga.Parameters.AddWithValue("@numero", NumeroReversale)
                    cmdRevRiga.Parameters.AddWithValue("@Anno", Year(DataReversale))
                    cmdRevRiga.Parameters.AddWithValue("@Riga", Riga)
                    cmdRevRiga.Parameters.AddWithValue("@NumeroDocumento", 0)
                    cmdRevRiga.Parameters.AddWithValue("@Data", Nothing)
                    cmdRevRiga.Parameters.AddWithValue("@Importo", 0)
                    cmdRevRiga.Parameters.AddWithValue("@ImpPagato", 0)
                    cmdRevRiga.Parameters.AddWithValue("@Riferimento", "")
                    cmdRevRiga.Parameters.AddWithValue("@Descrizione", DescrizioneTesta & vbNewLine & "Rif ." & NumeroRegistrazione)

                    cmdRevRiga.Parameters.AddWithValue("@PRINTERKEY", ViewState("PRINTERKEY"))

                    cmdRevRiga.ExecuteNonQuery()

                End If
                If Riga = 1 Then
                    Dim cmdRevRiga As New OleDbCommand()


                    cmdRevRiga.CommandText = "INSERT INTO ReversaliRighe (numero,Anno,Riga,NumeroDocumento,Data,Importo,ImpPagato,Riferimento,Descrizione,PRINTERKEY) " & _
                                                  " VALUES (?,?,?,?,?,?,?,?,?,?)"
                    cmdRevRiga.Connection = cnStampa

                    cmdRevRiga.Parameters.AddWithValue("@numero", NumeroReversale)
                    cmdRevRiga.Parameters.AddWithValue("@Anno", Year(DataReversale))
                    cmdRevRiga.Parameters.AddWithValue("@Riga", Riga)
                    cmdRevRiga.Parameters.AddWithValue("@NumeroDocumento", "")
                    cmdRevRiga.Parameters.AddWithValue("@Data", Nothing)
                    cmdRevRiga.Parameters.AddWithValue("@Importo", 0)
                    cmdRevRiga.Parameters.AddWithValue("@ImpPagato", 0)
                    cmdRevRiga.Parameters.AddWithValue("@Riferimento", "")
                    cmdRevRiga.Parameters.AddWithValue("@Descrizione", DescrizioneTesta & vbNewLine & "Rif ." & NumeroRegistrazione)

                    cmdRevRiga.Parameters.AddWithValue("@PRINTERKEY", ViewState("PRINTERKEY"))

                    cmdRevRiga.ExecuteNonQuery()
                End If
            End If
        Next
        cnStampa.Close()

        cn.Close()

        cnOspiti.Close()

        Session("SelectionFormula") = "{ReversaliTesta.PRINTERKEY} = " & Chr(34) & ViewState("PRINTERKEY") & Chr(34) & " And " & "{ReversaliRighe.PRINTERKEY} = " & Chr(34) & ViewState("PRINTERKEY") & Chr(34)

        ScriptManager.RegisterClientScriptBlock(Me, Me.GetType(), "Stampa", "openPopUp('StampaReport.aspx?REPORT=STAMPAREVERSALI&PRINTERKEY=ON','Stampe','width=800,height=600');", True)


    End Sub

    Protected Sub Btn_Esci_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles Btn_Esci.Click
        Response.Redirect("Menu_Stampe.aspx")
    End Sub


    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load


        Dim K1 As New Cls_SqlString

        Dim Barra As New Cls_BarraSenior

        If Not IsNothing(Session("RicercaAnagraficaSQLString")) Then
            Try
                K1 = Session("RicercaAnagraficaSQLString")
            Catch ex As Exception

            End Try
        End If

        Lbl_BarraSenior.Text = Barra.CodiceBarra(Application("SENIOR"), Session("UTENTE"), Page, K1)

        Call EseguiJS()
    End Sub

    Protected Sub ImgMenu_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles ImgMenu.Click
        Response.Redirect("Menu_Generale.aspx")
    End Sub

    Private Function SplitWords(ByVal s As String) As String()
        '
        ' Call Regex.Split function from the imported namespace.
        ' Return the result array.
        '
        Return Regex.Split(s, "\W+")
    End Function
End Class
