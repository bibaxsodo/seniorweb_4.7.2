﻿Imports System.Data.OleDb

Public Class GetDocumenti
    Implements System.Web.IHttpHandler

    Public Sub ProcessRequest(ByVal context As HttpContext) Implements IHttpHandler.ProcessRequest
        Dim DbC As New Cls_Login
        Dim ANNO As String = context.Request.QueryString("ANNO")
        Dim UTENTE As String = context.Request.QueryString("UTENTE")


        DbC.Utente = UTENTE
        DbC.LeggiSP(context.Application("SENIOR"))

        Dim cn As OleDbConnection

        cn = New Data.OleDb.OleDbConnection(DbC.Generale)

        cn.Open()

        Dim myXML As String

        myXML = "<tabella>"
        '<righe><col1>Colonna1</col1><col2>Colonna2</col2><col3>Colonna3</col3><col4>Colonna4</col4></righe><righe><col1>rColonna1</col1><col2>rColonna2</col2><col3>rColonna3</col3><col4>rColonna4</col4></righe></tabella>
        myXML = myXML & "<righe>"
        myXML = myXML & "<colonna1>" & "NumeroDocumento" & "</colonna1>"
        myXML = myXML & "<colonna2>" & "DataRegistrazione" & "</colonna2>"
        myXML = myXML & "<colonna3>" & "NumeroProtocollo" & "</colonna3>"
        myXML = myXML & "<colonna4>" & "TipoDocumento" & "</colonna4>"
        myXML = myXML & "<colonna5>" & "CodiceFiscale" & "</colonna5>"
        myXML = myXML & "<colonna6>" & "Importo" & "</colonna6>"
        myXML = myXML & "</righe>"

        Dim Cmd As New OleDbCommand

        Cmd.Connection = cn
        Cmd.CommandText = "Select * From MovimentIContabiliTesta where DataRegistrazione  >= ? And DataRegistrazione <= ?"
        Cmd.Parameters.AddWithValue("@DataRegistrazione", DateSerial(ANNO, 1, 1))
        Cmd.Parameters.AddWithValue("@DataRegistrazione", DateSerial(ANNO, 12, 31))
        Dim ReadRegistrazioni As OleDbDataReader = Cmd.ExecuteReader()
        Do While ReadRegistrazioni.Read
            Dim Registrazione As New Cls_MovimentoContabile


            Registrazione.NumeroRegistrazione = campodb(ReadRegistrazioni("NumeroRegistrazione"))
            Registrazione.Leggi(DbC.Generale, Registrazione.NumeroRegistrazione)

            Dim CauConta As New Cls_CausaleContabile

            CauConta.Codice = Registrazione.CausaleContabile
            CauConta.Leggi(DbC.TABELLE, CauConta.Codice)

            Dim Ospiti As New ClsOspite
            Dim Parente As New Cls_Parenti
            Dim Comune As New ClsComune
            Dim Regioni As New ClsUSL
            Dim CodiceFiscale As String

            If Registrazione.Tipologia = "" Then
                Dim CercaCf As Integer
                Dim SottocontoCf As Integer

                For CercaCf = 0 To Registrazione.Righe.Length - 1
                    If Not IsNothing(Registrazione.Righe(CercaCf)) Then
                        If Registrazione.Righe(CercaCf).Tipo = "CF" Then
                            SottocontoCf = Registrazione.Righe(CercaCf).SottocontoPartita
                        End If
                    End If
                Next

                Ospiti.CodiceOspite = Int(SottocontoCf / 100)
                Ospiti.Leggi(DbC.Ospiti, Ospiti.CodiceOspite)
                CodiceFiscale = Ospiti.CODICEFISCALE

                'If Int(SottocontoCf / 100) = Math.Round(SottocontoCf / 100, 2) Then
                '    Ospiti.CodiceOspite = Int(SottocontoCf / 100)
                '    Ospiti.Leggi(DbC.Ospiti, Ospiti.CodiceOspite)
                '    CodiceFiscale = Ospiti.CODICEFISCALE
                'Else
                '    Parente.CodiceParente = SottocontoCf - (Int(SottocontoCf / 100) * 100)
                '    Parente.CodiceOspite = Int(SottocontoCf / 100)
                '    Parente.Leggi(DbC.Ospiti, Parente.CodiceOspite, Parente.CodiceParente)
                '    CodiceFiscale = Parente.CODICEFISCALE
                'End If
            Else
                If Mid(Registrazione.Tipologia & Space(10), 1, 1) = "C" Then
                    Comune.Provincia = Mid(Registrazione.Tipologia & Space(10), 2, 3)
                    Comune.Comune = Mid(Registrazione.Tipologia & Space(10), 5, 3)
                    Comune.Leggi(DbC.Ospiti)

                    CodiceFiscale = Comune.CodiceFiscale
                    If CodiceFiscale = "" Then
                        CodiceFiscale = Comune.PartitaIVA
                    End If
                End If
                If Mid(Registrazione.Tipologia & Space(10), 1, 1) = "J" Then
                    Comune.Provincia = Mid(Registrazione.Tipologia & Space(10), 2, 3)
                    Comune.Comune = Mid(Registrazione.Tipologia & Space(10), 5, 3)
                    Comune.Leggi(DbC.Ospiti)

                    CodiceFiscale = Comune.CodiceFiscale
                    If CodiceFiscale = "" Then
                        CodiceFiscale = Comune.PartitaIVA
                    End If
                End If
                If Mid(Registrazione.Tipologia & Space(10), 1, 1) = "R" Then
                    Regioni.CodiceRegione = Mid(Registrazione.Tipologia & Space(10), 2, 4)
                    Regioni.Leggi(DbC.Ospiti)


                    CodiceFiscale = Regioni.CodiceFiscale
                    If CodiceFiscale = "" Then
                        CodiceFiscale = Regioni.PARTITAIVA
                    End If
                End If
            End If
            myXML = myXML & "<righe>"
            myXML = myXML & "<colonna1>" & Registrazione.NumeroRegistrazione & "</colonna1>"
            myXML = myXML & "<colonna2>" & Registrazione.DataRegistrazione & "</colonna2>"
            myXML = myXML & "<colonna3>" & Registrazione.NumeroProtocollo & "</colonna3>"
            myXML = myXML & "<colonna4>" & CauConta.Tipo & " " & CauConta.TipoDocumento & "</colonna4>"
            myXML = myXML & "<colonna5>" & CodiceFiscale & "</colonna5>"
            myXML = myXML & "<colonna6>" & Registrazione.ImportoRegistrazioneDocumento(DbC.TABELLE) & "</colonna6>"

            myXML = myXML & "</righe>"
        Loop
        ReadRegistrazioni.Close()

        cn.Close()


        context.Response.Write(myXML)

    End Sub

    Public ReadOnly Property IsReusable() As Boolean Implements IHttpHandler.IsReusable
        Get
            Return False
        End Get
    End Property



    Function campodb(ByVal oggetto As Object) As String
        If IsDBNull(oggetto) Then
            Return ""
        Else
            Return oggetto
        End If
    End Function

End Class