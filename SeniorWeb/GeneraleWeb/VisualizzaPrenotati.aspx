﻿<%@ Page Language="VB" AutoEventWireup="false" Inherits="GeneraleWeb_VisualizzaPrenotati" CodeFile="VisualizzaPrenotati.aspx.vb" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
    <meta http-equiv="x-ua-compatible" content="IE=9" />
    <title>Untitled Page</title>
    <asp:PlaceHolder runat="server">
        <%: System.Web.Optimization.Styles.Render("~/Content/AjaxControlToolkit/Styles/Bundle") %>
    </asp:PlaceHolder>
</head>
<body>
    <form id="form1" runat="server">
        <asp:ScriptManager ID="ScriptManager1" runat="server" AsyncPostBackTimeout="900" EnableScriptGlobalization="true">
            <Scripts>
                <asp:ScriptReference Path="~/Scripts/AjaxControlToolkit/Bundle" />
            </Scripts>
        </asp:ScriptManager>
        <div>
            <label class="LabelCampo">Conto Budget : </label>
            <asp:TextBox ID="Txt_Livello1" Enabled="false" runat="server" Width="48px"></asp:TextBox>
            <asp:TextBox ID="Txt_Livello2" Enabled="false" runat="server" Width="48px"></asp:TextBox>
            <asp:TextBox ID="Txt_Livello3" Enabled="false" runat="server" Width="48px"></asp:TextBox>
            <br />
            <br />

            <label class="LabelCampo">Colonna : </label>
            <asp:TextBox ID="Txt_Colonna" Enabled="false" runat="server" Width="48px"></asp:TextBox>
            <asp:Label ID="Lbl_ContoBudget" runat="server" Text="Label" Width="528px"></asp:Label><br />
            <br />

            <asp:GridView ID="DaGrid" runat="server" CellPadding="3"
                Width="600px" BackColor="White" BorderColor="#CCCCCC" BorderStyle="None"
                BorderWidth="1px">
                <FooterStyle BackColor="White" ForeColor="#000066" />
                <RowStyle ForeColor="#000066" />
                <SelectedRowStyle BackColor="#669999" Font-Bold="True" ForeColor="White" />
                <PagerStyle BackColor="White" ForeColor="#000066" HorizontalAlign="Left" />
                <HeaderStyle BackColor="#006699" Font-Bold="True" ForeColor="White" />
            </asp:GridView>
        </div>
    </form>
</body>
</html>
