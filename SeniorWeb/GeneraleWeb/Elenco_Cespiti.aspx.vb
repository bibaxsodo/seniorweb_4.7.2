﻿Imports Microsoft.VisualBasic
Imports System.Data.OleDb
Imports System.Web.Hosting
Imports System.Data.SqlTypes

Partial Class GeneraleWeb_Elenco_Cespiti
    Inherits System.Web.UI.Page
    Dim Tabella As New System.Data.DataTable("tabella")


    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Session("UTENTE") = "" Then
            Response.Redirect("..\Login.aspx")
            Exit Sub
        End If

        If Page.IsPostBack = True Then
            Exit Sub
        End If

        Dim K1 As New Cls_SqlString

        Dim Barra As New Cls_BarraSenior

        If Not IsNothing(Session("RicercaGeneraleSQLString")) Then
            K1 = Session("RicercaGeneraleSQLString")
        End If

        Lbl_BarraSenior.Text = Barra.CodiceBarra(Application("SENIOR"), Session("UTENTE"), Page, K1)


        Dim cn As New OleDbConnection


        Dim ConnectionString As String = Session("DC_GENERALE")

        cn = New Data.OleDb.OleDbConnection(ConnectionString)

        cn.Open()

        Dim cmd As New OleDbCommand

        cmd.CommandText = ("select top 20 * from Cespiti  " & _
                               "  Order By Descrizione")
        cmd.Connection = cn



        Tabella.Clear()
        Tabella.Columns.Add("CODICE", GetType(String))
        Tabella.Columns.Add("Descrizione", GetType(String))

        Tabella.Columns.Add("Dimesso", GetType(String))

        Dim myPOSTreader As OleDbDataReader = cmd.ExecuteReader()
        Do While myPOSTreader.Read
            Dim myriga As System.Data.DataRow = Tabella.NewRow()

            myriga(0) = campodb(myPOSTreader.Item("CODICE"))

            myriga(1) = campodb(myPOSTreader.Item("Descrizione"))
            If campodbn(myPOSTreader.Item("Dismesso")) = 1 Then
                myriga(2) = "SI"
            End If
            Tabella.Rows.Add(myriga)
        Loop

        myPOSTreader.Close()
        cn.Close()


        ViewState("Appoggio") = Tabella

        Grid.AutoGenerateColumns = True

        Grid.DataSource = Tabella

        Grid.DataBind()

        'Btn_Nuovo.Visible = True
    End Sub


    Function campodb(ByVal oggetto As Object) As String
        If IsDBNull(oggetto) Then
            Return ""
        Else
            Return oggetto
        End If
    End Function
    Function campodbn(ByVal oggetto As Object) As Double
        If IsDBNull(oggetto) Then
            Return 0
        Else
            Return oggetto
        End If
    End Function

    Protected Sub Grid_PageIndexChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewPageEventArgs) Handles Grid.PageIndexChanging
        Tabella = ViewState("Appoggio")
        Grid.PageIndex = e.NewPageIndex
        Grid.DataSource = Tabella
        Grid.DataBind()
    End Sub

    Protected Sub Grid_RowCommand(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewCommandEventArgs) Handles Grid.RowCommand
        If e.CommandName = "Seleziona" Then


            Dim d As Integer


            Tabella = ViewState("Appoggio")

            d = Val(e.CommandArgument)


            Dim Codice As String


            Codice = Tabella.Rows(d).Item(0).ToString


            Response.Redirect("GestioneCespiti.aspx?Codcie=" & Codice)
        End If
    End Sub

    Protected Sub ImgRicerca_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles ImgRicerca.Click
        Response.Redirect("GestioneCespiti.aspx")
    End Sub




    Private Function SplitWords(ByVal s As String) As String()
        '
        ' Call Regex.Split function from the imported namespace.
        ' Return the result array.
        '
        Return Regex.Split(s, "\W+")
    End Function

    Protected Sub Btn_Esci_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles Btn_Esci.Click
        Response.Redirect("Menu_Generale.aspx")

    End Sub




    Protected Sub Btn_Ricerca_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles Btn_Ricerca.Click

        Dim cn As New OleDbConnection


        Dim ConnectionString As String = Session("DC_GENERALE")

        cn = New Data.OleDb.OleDbConnection(ConnectionString)

        cn.Open()

        Dim cmd As New OleDbCommand
        Dim Condizione As String = ""

        If Val(Txt_Codice.Text) > 0 Then
            Condizione = Condizione & " CODICE = " & Val(Txt_Codice.Text)
        End If
        If Txt_Descrizione.Text.Trim <> "" Then
            If Condizione <> "" Then
                Condizione = Condizione & " And "
            End If
            Condizione = Condizione & " Descrizione like ? "
        End If

        If RD_Dismessi.Checked = True Then
            If Condizione <> "" Then
                Condizione = Condizione & " And "
            End If
            Condizione = Condizione & " Dismesso = 1 "
        End If

        If RD_NonDismessi.Checked = True Then
            If Condizione <> "" Then
                Condizione = Condizione & " And "
            End If
            Condizione = Condizione & " Dismesso = 0 "
        End If


        If Condizione = "" Then
            cmd.CommandText = ("select  * from Cespiti  " & _
                                   "  Order By Descrizione")
        Else
            cmd.CommandText = ("select  * from Cespiti  Where " & Condizione & _
                                   "  Order By Descrizione")
        End If
        cmd.Connection = cn
        If Txt_Descrizione.Text.Trim <> "" Then
            cmd.Parameters.AddWithValue("@Descrizione", Txt_Descrizione.Text)
        End If



        Tabella.Clear()
        Tabella.Columns.Add("CODICE", GetType(String))
        Tabella.Columns.Add("Descrizione", GetType(String))
        Tabella.Columns.Add("Dimesso", GetType(String))

        Dim myPOSTreader As OleDbDataReader = cmd.ExecuteReader()
        Do While myPOSTreader.Read
            Dim myriga As System.Data.DataRow = Tabella.NewRow()

            myriga(0) = campodb(myPOSTreader.Item("CODICE"))

            myriga(1) = campodb(myPOSTreader.Item("Descrizione"))

            If campodbn(myPOSTreader.Item("Dismesso")) = 1 Then
                myriga(2) = "SI"
            End If

            Tabella.Rows.Add(myriga)
        Loop

        myPOSTreader.Close()
        cn.Close()


        ViewState("Appoggio") = Tabella

        Grid.AutoGenerateColumns = True

        Grid.DataSource = Tabella

        Grid.DataBind()
    End Sub
End Class
