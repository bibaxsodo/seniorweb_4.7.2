﻿<%@ Page Language="VB" AutoEventWireup="false" EnableEventValidation="false" Inherits="GeneraleWeb_RateiRisconti" CodeFile="RateiRisconti.aspx.vb" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
    <meta http-equiv="x-ua-compatible" content="IE=9" />
    <title>Ratei e Risconti</title>
    <asp:PlaceHolder runat="server">
        <%: System.Web.Optimization.Styles.Render("~/Content/AjaxControlToolkit/Styles/Bundle") %>
    </asp:PlaceHolder>
    <link href="ospiti.css?ver=11" rel="stylesheet" type="text/css" />
    <link rel="shortcut icon" href="images/SENIOR.ico" />
    <link href="js/jquery.autocomplete.css" rel="stylesheet" type="text/css" />

    <script src="js/jquery-1.5.1.min.js" type="text/javascript"></script>
    <script src="js/jquery.autocomplete.js" type="text/javascript"></script>
    <script src="js/soapclient.js" type="text/javascript"></script>
    <script src="/js/convertinumero.js" type="text/javascript"></script>

    <script src="js/jquery.maskedinput-1.3.min.js" type="text/javascript"></script>
    <script src="/js/formatnumer.js" type="text/javascript"></script>

    <script src="/js/pianoconti.js" type="text/javascript"></script>

    <script src="js/JSErrore.js" type="text/javascript"></script>

    <link rel="stylesheet" href="dialogbox/redips-dialog.css" type="text/css" media="screen" />
    <script type="text/javascript" src="dialogbox/redips-dialog-min.js"></script>
    <script type="text/javascript" src="dialogbox/script.js"></script>
    <script src="js/NoEnter.js" type="text/javascript"></script>
    <link rel="stylesheet" href="jqueryui/jquery-ui.css" type="text/css" />
    <script src="jqueryui/jquery-ui.js" type="text/javascript"></script>
    <script src="jqueryui/datapicker-it.js" type="text/javascript"></script>
</head>
<body>
    <form id="form1" runat="server">
        <asp:ScriptManager ID="ScriptManager1" runat="server" EnableScriptGlobalization="true">
            <Scripts>
                <asp:ScriptReference Path="~/Scripts/AjaxControlToolkit/Bundle" />
            </Scripts>
        </asp:ScriptManager>
        <div>
            <asp:Label ID="Lbl_Erore" runat="server" Text="" ForeColor="Red"></asp:Label>
            <asp:GridView ID="Grid" runat="server" CellPadding="4" Height="60px"
                ShowFooter="True" BackColor="White" BorderColor="#6FA7D1"
                BorderStyle="Dotted" BorderWidth="1px">
                <RowStyle ForeColor="#333333" BackColor="White" />
                <Columns>
                    <asp:CommandField ButtonType="Image" ShowDeleteButton="True" DeleteImageUrl="~/images/cancella.png" />
                    <asp:TemplateField HeaderText="Tipo">
                        <ItemTemplate>
                            <asp:RadioButton ID="RB_Rateo" runat="server" Text="Rateo" GroupName="Tipo" />
                            <asp:RadioButton ID="RB_Risconto" runat="server" Text="Risconto" GroupName="Tipo" />
                        </ItemTemplate>
                        <FooterTemplate>
                            <div style="text-align: left">
                                <asp:ImageButton ID="ImageButton1" ImageUrl="~/images/inserisci.png" CommandName="Inserisci" runat="server" />
                            </div>
                        </FooterTemplate>
                    </asp:TemplateField>

                    <asp:TemplateField HeaderText="Data Dal">
                        <ItemTemplate>
                            <asp:TextBox ID="Txt_DataDal" Width="90px" runat="server" Text=""></asp:TextBox>
                        </ItemTemplate>
                    </asp:TemplateField>

                    <asp:TemplateField HeaderText="Data Al">
                        <ItemTemplate>
                            <asp:TextBox ID="Txt_DataAl" Width="90px" runat="server" Text=""></asp:TextBox>
                        </ItemTemplate>
                    </asp:TemplateField>

                    <asp:TemplateField HeaderText="Rateo/Risconto">
                        <ItemTemplate>
                            <asp:TextBox ID="Txt_RateoRisconto" Width="200px" onkeypress="return handleEnter(this, event)" CssClass="MyAutoComplete" runat="server" Text=""></asp:TextBox>
                        </ItemTemplate>
                    </asp:TemplateField>

                    <asp:TemplateField HeaderText="Costo">
                        <ItemTemplate>
                            <asp:TextBox ID="Txt_Costo" Width="200px" onkeypress="return handleEnter(this, event)" CssClass="MyAutoComplete" runat="server" Text=""></asp:TextBox>
                        </ItemTemplate>
                    </asp:TemplateField>

                    <asp:TemplateField HeaderText="Importo">
                        <ItemTemplate>
                            <asp:TextBox ID="Txt_Importo" runat="server" Text=""></asp:TextBox>
                        </ItemTemplate>
                    </asp:TemplateField>

                    <asp:TemplateField HeaderText="Descrizione">
                        <ItemTemplate>
                            <asp:TextBox ID="Txt_Descrizione" Width="300px" runat="server" Text=""></asp:TextBox>
                        </ItemTemplate>
                    </asp:TemplateField>
                </Columns>
                <FooterStyle BackColor="White" ForeColor="#023102" />
                <PagerStyle BackColor="#336666" ForeColor="White" HorizontalAlign="Center" />
                <SelectedRowStyle BackColor="#339966" Font-Bold="True" ForeColor="White" />
                <HeaderStyle BackColor="#A6C9E2" Font-Bold="False" ForeColor="White" BorderColor="#6FA7D1" BorderWidth="1" />
            </asp:GridView>
            <br />
            <asp:ImageButton ID="Btn_Modifica" runat="server" Height="48px" ImageUrl="~/images/salva.jpg" class="EffettoBottoniTondi"
                Width="48px" />
        </div>
    </form>
</body>
</html>
