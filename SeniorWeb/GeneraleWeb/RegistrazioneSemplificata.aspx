﻿<%@ Page Language="VB" AutoEventWireup="false" Inherits="GeneraleWeb_RegistrazioneSemplificata" EnableEventValidation="false" CodeFile="RegistrazioneSemplificata.aspx.vb" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="xasp" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
    <meta http-equiv="x-ua-compatible" content="IE=9" />
    <title>Contabilità Semplificata</title>
    <asp:PlaceHolder runat="server">
        <%: System.Web.Optimization.Styles.Render("~/Content/AjaxControlToolkit/Styles/Bundle") %>
    </asp:PlaceHolder>
    <link href="ospiti.css?ver=11" rel="stylesheet" type="text/css" />
    <link rel="shortcut icon" href="images/SENIOR.ico" />
    <link href="js/jquery.autocomplete.css" rel="stylesheet" type="text/css" />

    <script src="js/jquery-1.5.1.min.js" type="text/javascript"></script>
    <script src="js/jquery.autocomplete.js" type="text/javascript"></script>
    <script src="js/soapclient.js" type="text/javascript"></script>
    <script src="/js/convertinumero.js" type="text/javascript"></script>

    <script src="js/jquery.maskedinput-1.3.min.js" type="text/javascript"></script>
    <script src="/js/formatnumer.js" type="text/javascript"></script>

    <script src="/js/pianoconti.js" type="text/javascript"></script>

    <script src="js/JSErrore.js" type="text/javascript"></script>

    <link rel="stylesheet" href="dialogbox/redips-dialog.css" type="text/css" media="screen" />
    <script type="text/javascript" src="dialogbox/redips-dialog-min.js"></script>
    <script type="text/javascript" src="dialogbox/script.js"></script>
    <script src="js/NoEnter.js" type="text/javascript"></script>

    <script type="text/javascript">     
        function soloNumeri(evt) {
            var charCode = (evt.which) ? evt.which : event.keyCode
            if (charCode > 31 && (charCode < 48 || charCode > 57)) {
                return false;
            }
            return true;
        }

        function DialogBoxBig(Path) {

            var winW = 630, winH = 460;
            if (document.body && document.body.offsetWidth) {
                winW = document.body.offsetWidth;
                winH = document.body.offsetHeight;
            }
            if (document.compatMode == 'CSS1Compat' &&
                document.documentElement &&
                document.documentElement.offsetWidth) {
                winW = document.documentElement.offsetWidth;
                winH = document.documentElement.offsetHeight;
            }
            if (window.innerWidth && window.innerHeight) {
                winW = window.innerWidth;
                winH = window.innerHeight;
            }

            var APPO = winH - 100;

            REDIPS.dialog.show(winW - 200, winH - 100, '<iframe id="output" src="' + Path + '" height="' + APPO + '" width="100%" ></iframe>');
            return false;

        }
        function DialogBox(Path) {

            REDIPS.dialog.show(700, 500, '<iframe id="output" src="' + Path + '" height="490px" width="690"></iframe>');
            return false;

        }
        function DialogBoxSlim(Path) {

            REDIPS.dialog.show(600, 200, '<iframe id="output" src="' + Path + '" height="190px" width="590"></iframe>');
            return false;

        }

        $(document).ready(function () {
            $('html').keyup(function (event) {

                if (event.keyCode == 113) {
                    __doPostBack("Btn_Modifica", "0");
                }

                if (event.keyCode == 119) {
                    __doPostBack("Btn_Pulisci", "0");
                }

            });
        });

        $(document).ready(function () {
            if (window.innerHeight > 0) { $("#BarraLaterale").css("height", (window.innerHeight - 94) + "px"); } else { $("#BarraLaterale").css("height", (document.documentElement.offsetHeight - 94) + "px"); }
        });
    </script>

</head>
<body>
    <form id="form1" runat="server">
        <asp:ScriptManager ID="ScriptManager1" runat="server" EnableScriptGlobalization="true">
            <Scripts>
                <asp:ScriptReference Path="~/Scripts/AjaxControlToolkit/Bundle" />
            </Scripts>
        </asp:ScriptManager>
        <div style="text-align: left;">
            <div id="dialog-modal" title="Legami">
                <asp:Label ID="Lbl_Dialog" runat="server" Text=""></asp:Label>
            </div>

            <table style="width: 100%;" cellpadding="0" cellspacing="0">
                <tr>
                    <td style="width: 160px; background-color: #F0F0F0;"></td>
                    <td>
                        <div class="Titolo">Contabilità - Principale - Contabilità Semplificata</div>
                        <div class="SottoTitolo">
                            <br />
                            <br />
                        </div>
                    </td>

                    <td style="text-align: right; vertical-align: top;">
                        <span class="BenvenutoText">Benvenuto
                        <asp:Label ID="Lbl_Utente" runat="server" Text=""></asp:Label>&nbsp;&nbsp;&nbsp;&nbsp;</span>
                        <asp:Label ID="lbl_tasti" runat="server" Text=""></asp:Label>
                        <img height="38px" src="../images/Btn_PianoConti.jpg" title="Piano Conti" onclick="DialogBoxBig('Pianoconti.aspx');" />&nbsp;     
      <asp:ImageButton ID="Btn_Duplica" Height="38px" runat="server" ImageUrl="~/images/duplica.png" class="EffettoBottoniTondi" ToolTip="Duplica" />&nbsp;    
      <asp:ImageButton ID="Btn_Modifica" Height="38px" runat="server" ImageUrl="~/images/salva.jpg" class="EffettoBottoniTondi" ToolTip="Salva (F2)" />&nbsp;
      <asp:ImageButton ID="Btn_Elimina" Height="38px" OnClientClick="return window.confirm('Eliminare?');" runat="server" ImageUrl="~/images/elimina.jpg" class="EffettoBottoniTondi" ToolTip="Elimina" />&nbsp;
      <asp:ImageButton ID="Btn_Pulisci" Height="38px" runat="server" BackColor="Transparent" ImageUrl="~/images/PULISCI.JPG" ToolTip="Pulisci (F8)" Visible="false" />&nbsp;
       <asp:ImageButton ID="Btn_Esci" runat="server" BackColor="Transparent" Height="38px" ImageUrl="images/esci.jpg" ToolTip="Chiudi" />
                    </td>
                </tr>



                <tr>
                    <td style="width: 160px; background-color: #F0F0F0; vertical-align: top; text-align: center;" id="BarraLaterale">
                        <a href="Menu_Semplificata.aspx" style="border-width: 0px;">
                            <img src="images/Home.jpg" alt="Menù" class="Effetto" /></a><br />
                        <asp:ImageButton ID="Btn_ADD" src="../images/Menu_PrimaNota.png" Width="112px" Height="100px" alt="Nuova Registrazione" ToolTip="Nuova Registrazione" runat="server" /><br />
                        <asp:ImageButton ID="ImgRicerca" src="images/ricerca.png" Width="112px" Height="100px" alt="Ricerca Registrazioni/Saldo" ToolTip="Ricerca Registrazioni/Saldo" runat="server" /><br />
                        <br />
                        <label class="MenuDestra">&nbsp;&nbsp;<a href="#" onclick="window.open('VsPianoConti.aspx','Stampe','width=450,height=600,scrollbars=yes');"><img src="images/arrow.gif" />Piano Conti</a></label>
                    </td>
                    <td colspan="2" style="background-color: #FFFFFF; vertical-align: top;">
                        <xasp:TabContainer ID="TabContainer1" runat="server" ActiveTabIndex="0" CssClass="TabSenior" Width="100%" BorderStyle="None" Style="margin-right: 39px">
                            <xasp:TabPanel runat="server" HeaderText="Prima Nota" ID="Tab_Anagrafica">
                                <HeaderTemplate>
                                    Prima Nota
                                </HeaderTemplate>
                                <ContentTemplate>
                                    <label class="LabelCampo">Numero :</label>
                                    <asp:TextBox ID="Txt_Numero" onkeypress="return soloNumeri(event);" runat="server" AutoPostBack="True" Width="104px"></asp:TextBox>
                                    <asp:Label ID="Lbl_Progressivo" runat="server" Text=""></asp:Label>
                                    <br />
                                    <br />
                                    <label class="LabelCampo">Data :</label>
                                    <asp:TextBox ID="Txt_DataRegistrazione" onkeypress="return handleEnter(this, event)" autocomplete="off" runat="server" Width="104px"></asp:TextBox>
                                    <asp:ImageButton runat="server" ID="ImageButton3" ImageUrl="~/Images/calendario.png" Width="16px" Height="16px" AlternateText="Visualizza Calendario" />
                                    <xasp:CalendarExtender ID="CalendarioExt1" runat="server" TargetControlID="Txt_DataRegistrazione" PopupButtonID="ImageButton3" Format="dd/MM/yyyy" Enabled="true"></xasp:CalendarExtender>
                                    <br />
                                    <br />
                                    <label class="LabelCampo">Descrizione:</label><br />
                                    <asp:TextBox ID="Txt_Descrizione" autocomplete="off" runat="server" Height="72px" TextMode="MultiLine" Width="562px"></asp:TextBox>
                                    <br />
                                    <label class="LabelCampo">Tipo Movimento :</label>
                                    <asp:RadioButton ID="RB_Entrate" runat="server" Text="Entrate" AutoPostBack="true" Checked="False" GroupName="tipomov" />&nbsp;&nbsp;
    <asp:RadioButton ID="RB_Uscite" runat="server" Text="Uscita" AutoPostBack="true" Checked="False" GroupName="tipomov" />&nbsp;&nbsp;
    <asp:RadioButton ID="RB_Giroconto" runat="server" Text="Giroconto" AutoPostBack="true" Checked="True" GroupName="tipomov" /><br />

                                    <br />
                                    <label class="LabelCampo">
                                        <asp:Label ID="Lbl_Dare" runat="server" Text="Prelievo/Versamento :"></asp:Label></label>
                                    <asp:TextBox ID="Txt_ContoPrelievoVersamento" onkeypress="return handleEnter(this, event)" autocomplete="off" runat="server" Width="562px"></asp:TextBox><br />

                                    <br />
                                    <label class="LabelCampo">
                                        <asp:Label ID="Lbl_Avere" runat="server" Text="Costo/Ricavo :"></asp:Label>
                                    </label>
                                    <asp:TextBox ID="Txt_CostoRicavo" onkeypress="return handleEnter(this, event)" autocomplete="off" runat="server" Width="562px"></asp:TextBox><br />
                                    <br />
                                    <label class="LabelCampo">Importo :</label>
                                    <asp:TextBox ID="Txt_Importo" autocomplete="off" runat="server" Width="100px" Style="text-align: right;"></asp:TextBox><br />

                                    <center><asp:Label ID="Lbl_TipoRegistrazione" runat="server" Text=""></asp:Label></center>

                                </ContentTemplate>

                            </xasp:TabPanel>
                        </xasp:TabContainer>
                    </td>
                </tr>
            </table>
        </div>
        <asp:Timer ID="Timer1" runat="server">
        </asp:Timer>
    </form>
</body>
</html>
