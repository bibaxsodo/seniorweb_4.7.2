﻿Imports Microsoft.VisualBasic
Imports System.Data.OleDb
Imports System.Web.Hosting

Public Class GeneraRegistrazioniDaDistinta
    Inherits System.Web.UI.Page
        Dim Tabella As New System.Data.DataTable("tabella")

    
    Function AdattaLunghezzaNumero(ByVal Testo As String, ByVal Lunghezza As Integer) As String
        If Len(Testo) > Lunghezza Then
            AdattaLunghezzaNumero = Mid(Testo, 1, Lunghezza)
            Exit Function
        End If
        AdattaLunghezzaNumero = Replace(Space(Lunghezza - Len(Testo)) & Testo, " ", "0")
    End Function

    Function AdattaLunghezza(ByVal Testo As String, ByVal Lunghezza As Integer) As String
        If Len(Testo) > Lunghezza Then
            Testo = Mid(Testo, 1, Lunghezza)
        End If

        AdattaLunghezza = Testo & Space(Lunghezza - Len(Testo) + 1)

        If Len(AdattaLunghezza) > Lunghezza Then
            AdattaLunghezza = Mid(AdattaLunghezza, 1, Lunghezza)
        End If
    End Function
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
                
        Call EseguiJS()

        If Page.IsPostBack = True Then
            Exit Sub
        End If


        Dim K1 As New Cls_SqlString

        Dim Barra As New Cls_BarraSenior

        If Not IsNothing(Session("RicercaGeneraleSQLString")) Then
            K1 = Session("RicercaGeneraleSQLString")
        End If


        Img_Modifica.Visible=False
        Lbl_BarraSenior.Text = Barra.CodiceBarra(Application("SENIOR"), Session("UTENTE"), Page, K1)

        Dim CausaleContabile as new Cls_CausaleContabile

        CausaleContabile.UpDateDropBoxPag(Session("DC_TABELLE"),DD_CausaleContabile)
    End Sub

    Protected Sub Img_Etrai_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles Img_Etrai.Click
        Dim cn As OleDbConnection

        
        cn = New Data.OleDb.OleDbConnection(Session("DC_GENERALE"))


        cn.Open()
        
        Tabella.Clear()
        Tabella.Columns.Clear()
        Tabella.Columns.Add("NumeroDistinta", GetType(Integer))
        Tabella.Columns.Add("DataDistinta", GetType(String))
        Tabella.Columns.Add("Descrizione", GetType(String))
        Tabella.Columns.Add("Importo", GetType(Double))
        
                Dim MySql As String = ""


        MySql = "SELECT * From DistintaIncassoPagamento Where DataDistinta >= ? And DataDistinta <= ? Order by  DataDistinta,Id"

        Dim cmd As New OleDbCommand()
        cmd.CommandText = MySql

        cmd.CommandTimeout = 120
        cmd.Connection = cn
        cmd.Parameters.AddWithValue("DataInizio",Txt_DataDal.Text)
        cmd.Parameters.AddWithValue("DataFine",Txt_DataAl.Text)

        Dim ReadDP As OleDbDataReader = cmd.ExecuteReader()
        Do While ReadDP.Read

               Dim myriga As System.Data.DataRow = Tabella.NewRow()
               myriga(0) = campodbN(ReadDP.Item("Id"))
               myriga(1) = Format(campodbd(ReadDP.Item("DataDistinta")),"dd/MM/yyyy")

               myriga(2) = campodb(ReadDP.Item("Descrizione"))
            
                Dim cmdTotaleImpporto As New OleDbCommand()
                cmdTotaleImpporto.CommandText = "Select sum(Importo) as TImporto from DistintaIncassoPagamentoRiga where IdDistinta  = ?"
                cmdTotaleImpporto.Parameters.AddWithValue("@IdDistinta",campodbN(ReadDP.Item("Id")))
                cmdTotaleImpporto.CommandTimeout = 120
                cmdTotaleImpporto.Connection = cn

                Dim ReadImporto As OleDbDataReader = cmdTotaleImpporto.ExecuteReader()
                if ReadImporto.Read then
                   myriga(3) = campodbN(ReadImporto.Item("TImporto"))     
                end if
                ReadImporto.Close
               
               Tabella.Rows.Add(myriga)
        Loop
        ReadDP.Close

        
        cn.Close
        
        Grid.AutoGenerateColumns = False
        Grid.DataSource = Tabella
        Grid.DataBind()

        
        Session("TABELLA") = Tabella

        Img_Modifica.Visible =True

    end Sub

    Function campodb(ByVal oggetto As Object) As String
        If IsDBNull(oggetto) Then
            Return ""
        Else
            Return oggetto
        End If
    End Function
    Function campodbN(ByVal oggetto As Object) As Double
        If IsDBNull(oggetto) Then
            Return 0
        Else
            Return oggetto
        End If
    End Function
    Function campodbD(ByVal oggetto As Object) As Date
        If IsDBNull(oggetto) Then
            Return Nothing
        Else
            Return oggetto
        End If
    End Function


    Private Sub EseguiJS()
        Dim MyJs As String
        MyJs = "$(document).ready(function() {"

        MyJs = MyJs & "var els = document.getElementsByTagName(""*"");"

        MyJs = MyJs & "for (var i=0;i<els.length;i++)"
        MyJs = MyJs & "if ( els[i].id ) { "
        MyJs = MyJs & " var appoggio =els[i].id; "


        MyJs = MyJs & " if ((appoggio.match('Txt_Import')!= null) || appoggio.match('Base')!= null) {  "
        MyJs = MyJs & " $(els[i]).keypress(function() { ForceNumericInput($(this).val(), true, true); } ); "
        MyJs = MyJs & " $(els[i]).blur(function() { var ap = formatNumber($(this).val(),2); $(this).val(ap); });"
        MyJs = MyJs & "    }"

        MyJs = MyJs & " if (appoggio.match('Txt_Data')!= null) {  "
        MyJs = MyJs & " $(els[i]).mask(""99/99/9999""); "

        MyJs = MyJs & " $(els[i]).datepicker({ changeMonth: true,changeYear: true,  yearRange: ""1990:" & Year(Now) + 5 & """},$.datepicker.regional[""it""]);"

        MyJs = MyJs & "    }"

        MyJs = MyJs & "} "
        MyJs = MyJs & "});"

        ScriptManager.RegisterStartupScript(Me, Me.GetType(), "JS_GESTPRE", MyJs, True)
    End Sub


    Private Sub Img_Modifica_Click(sender As Object, e As ImageClickEventArgs) Handles Img_Modifica.Click
        Dim I as  Integer
        Dim x as  Integer =0

        Dim Distinta as new Cls_DistintaIncassoPagamento


        Tabella =Session("TABELLA")

        For I =0 to  Tabella.Rows.Count -1
            Dim CheckBox As CheckBox = DirectCast(Grid.Rows(i).FindControl("ChkImporta"), CheckBox)

            if CheckBox.Checked = True then
                GeneraRegistrazioni(val(Tabella.Rows(I).Item(0).ToString))
            End If
        Next

        
       Dim MyJs  as  String
       Dim BottoneChiudi As String = "<br/><a href=""#"" onclick=""ChiudiConfermaModifica();""  style=""position: inherit;bottom: 20px;left: 30%;width: 40%;""><input type=""button"" class=""SeniorButton"" value=""CHIUDI"" style=""width: 100%;""></a>"
       MyJs = "<div id=""ConfermaModifica"" class=""confermamodifca""  style=""position:absolute; top:20%; left:42%; width:24%;  height:23%;""><br />Salvato Distinta " & Distinta.Id & BottoneChiudi & "</div>"
       LblBox.Text = MyJs

        Img_Modifica.Visible=False
    End Sub



    Private Sub GeneraRegistrazioni(ByVal ID As Integer)
        Dim cn As OleDbConnection

        
        cn = New Data.OleDb.OleDbConnection(Session("DC_GENERALE"))


        cn.Open()

        Dim MySql As String = ""


        MySql = "SELECT * From DistintaIncassoPagamentoRiga Where IdDistinta = ?  "

        Dim cmd As New OleDbCommand()
        cmd.CommandText = MySql

        cmd.CommandTimeout = 120
        cmd.Connection = cn
        cmd.Parameters.AddWithValue("IdDistinta", id)
        
        Dim ReadDP As OleDbDataReader = cmd.ExecuteReader()
        Do While ReadDP.Read
           if campodbN(ReadDP.Item("IdScadenza")) > 0 then
                RegistraIncasso(Txt_Data.Text,DD_CausaleContabile.SelectedValue, campodbN(ReadDP.Item("Importo")),campodbN(ReadDP.Item("NumeroRegistrazioneDocumento")), campodbN(ReadDP.Item("IdDistinta")), campodbN(ReadDP.Item("IdScadenza")))
              else
                RegistraIncasso(Txt_Data.Text,DD_CausaleContabile.SelectedValue,campodbN(ReadDP.Item("Importo")),campodbN(ReadDP.Item("NumeroRegistrazioneDocumento")),campodbN(ReadDP.Item("IdDistinta")) , 0)
           End If
        Loop
        ReadDP.Close


        cn.Close

    End Sub


    Private Sub RegistraIncasso(ByVal DataIncasso As Date, ByVal CausaleContabile as String ,ByVal Importo as Double,ByVal NumeroDocumento as  Integer,ByVal NumeroDistinta as  Integer, ByVal Scadenza as Integer)
        Dim RegistrazioneDocuemnto as new Cls_MovimentoContabile

        RegistrazioneDocuemnto.NumeroRegistrazione = NumeroDocumento
        RegistrazioneDocuemnto.Leggi(Session("DC_GENERALE"),RegistrazioneDocuemnto.NumeroRegistrazione)


        Dim  clsCausaleContabile as new Cls_CausaleContabile

        clsCausaleContabile.Codice = CausaleContabile
        clsCausaleContabile.Leggi(Session("DC_TABELLE"),clsCausaleContabile.Codice)


        Dim RegistrazioneIncasso as new Cls_MovimentoContabile


        RegistrazioneIncasso.NumeroRegistrazione =0
        RegistrazioneIncasso.DataRegistrazione = DataIncasso
        RegistrazioneIncasso.Descrizione ="Incasso distinta " & NumeroDistinta
        RegistrazioneIncasso.CausaleContabile = CausaleContabile



        RegistrazioneIncasso.Righe(0) = New Cls_MovimentiContabiliRiga

        RegistrazioneIncasso.Righe(0).MastroPartita = RegistrazioneDocuemnto.Righe(0).MastroPartita
        RegistrazioneIncasso.Righe(0).ContoPartita = RegistrazioneDocuemnto.Righe(0).ContoPartita
        RegistrazioneIncasso.Righe(0).SottocontoPartita = RegistrazioneDocuemnto.Righe(0).SottocontoPartita
        RegistrazioneIncasso.Righe(0).DareAvere = clsCausaleContabile.Righe(0).DareAvere
        RegistrazioneIncasso.Righe(0).Importo = Importo


        RegistrazioneIncasso.Righe(1) = New Cls_MovimentiContabiliRiga

        RegistrazioneIncasso.Righe(1).MastroPartita = clsCausaleContabile.Righe(1).Mastro
        RegistrazioneIncasso.Righe(1).ContoPartita = clsCausaleContabile.Righe(1).Conto
        RegistrazioneIncasso.Righe(1).SottocontoPartita = clsCausaleContabile.Righe(1).Sottoconto
        RegistrazioneIncasso.Righe(1).DareAvere = clsCausaleContabile.Righe(1).DareAvere
        RegistrazioneIncasso.Righe(1).Importo = Importo


        RegistrazioneIncasso.Scrivi(Session("DC_GENERALE"), RegistrazioneIncasso.NumeroRegistrazione)
               
        Dim Legame as new Cls_Legami

        legame.NumeroDocumento(0) = RegistrazioneDocuemnto.NumeroRegistrazione
        legame.NumeroPagamento(0) = RegistrazioneIncasso.NumeroRegistrazione
        Legame.Importo(0) = Importo
        legame.IdDistinta(0)  = NumeroDistinta
        legame.Scrivi(Session("DC_GENERALE"),0, RegistrazioneIncasso.NumeroRegistrazione)

        if Scadenza >0 then
                Dim cn As OleDbConnection
            
                cn = New Data.OleDb.OleDbConnection(Session("DC_GENERALE"))
                cn.Open()
                Dim cmd As New OleDbCommand()


                cmd.CommandText = ("update  Scadenzario set  Chiusa = 1, RegistrazioneIncasso = ? ,  Importo = ?,IdDistinta = ? where " & _
                                       "id = ? ")
                
                cmd.Parameters.AddWithValue("@RegistrazioneIncasso", RegistrazioneIncasso.NumeroRegistrazione)
                cmd.Parameters.AddWithValue("@Importo", Importo)
                cmd.Parameters.AddWithValue("@IdDistinta", NumeroDistinta)
                cmd.Parameters.AddWithValue("@IdScadenza", Scadenza)
                cmd.Connection = cn        
                cmd.ExecuteNonQuery()
        End If
    End Sub
End Class