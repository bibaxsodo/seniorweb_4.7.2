﻿<%@ Page Language="VB" AutoEventWireup="false" Inherits="GeneraleWeb_WizardFattura" CodeFile="WizardFattura.aspx.vb" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <meta http-equiv="x-ua-compatible" content="IE=9" />
    <title>Wizard Fattura</title>
    <asp:PlaceHolder runat="server">
        <%: System.Web.Optimization.Styles.Render("~/Content/AjaxControlToolkit/Styles/Bundle") %>
    </asp:PlaceHolder>
    <link href="ospiti.css?ver=11" rel="stylesheet" type="text/css" />
    <link rel="shortcut icon" href="images/SENIOR.ico" />
    <link href="js/jquery.autocomplete.css" rel="stylesheet" type="text/css" />

    <script src="js/jquery-1.5.1.min.js" type="text/javascript"></script>
    <script src="js/jquery.autocomplete.js" type="text/javascript"></script>
    <script src="js/soapclient.js" type="text/javascript"></script>
    <script src="/js/convertinumero.js" type="text/javascript"></script>

    <script src="js/jquery.maskedinput-1.3.min.js" type="text/javascript"></script>
    <script src="/js/formatnumer.js" type="text/javascript"></script>

    <script src="/js/pianoconti.js" type="text/javascript"></script>

    <script type="text/javascript" src="js/jquery.corner.js"></script>
    <link rel="stylesheet" href="dialogbox/redips-dialog.css" type="text/css" media="screen" />
    <script type="text/javascript" src="dialogbox/redips-dialog-min.js"></script>
    <script type="text/javascript" src="dialogbox/script.js"></script>
    <script src="js/JSErrore.js" type="text/javascript"></script>
    <script type="text/javascript">
        $(document).ready(function () {

            $('#<%=Txt_ClienteFornitore.ClientID %>').autocomplete('/WebHandler/GestioneConto.ashx?Utente=<%=Session("UTENTE") %>', { delay: 5, minChars: 4 });


        $('#<%=Txt_Conto1.ClientID %>').autocomplete('/WebHandler/GestioneConto.ashx?Utente=<%=Session("UTENTE") %>', { delay: 5, minChars: 4 });
        $('#<%=Txt_Conto2.ClientID %>').autocomplete('/WebHandler/GestioneConto.ashx?Utente=<%=Session("UTENTE") %>', { delay: 5, minChars: 4 });
        $('#<%=Txt_Conto3.ClientID %>').autocomplete('/WebHandler/GestioneConto.ashx?Utente=<%=Session("UTENTE") %>', { delay: 5, minChars: 4 });
        $('#<%=Txt_Conto4.ClientID %>').autocomplete('/WebHandler/GestioneConto.ashx?Utente=<%=Session("UTENTE") %>', { delay: 5, minChars: 4 });
        $('#<%=Txt_Conto5.ClientID %>').autocomplete('/WebHandler/GestioneConto.ashx?Utente=<%=Session("UTENTE") %>', { delay: 5, minChars: 4 });

        $('#<%=Txt_DataRegistrazione.ClientID%>').mask("99/99/9999");
        $('#<%=Txt_DataDocumento.ClientID%>').mask("99/99/9999");

        $('#<%=Txt_AnnoProtocollo.ClientID%>').mask("9999");
    });

    </script>
</head>

<body>
    <form id="form1" runat="server">
        <asp:ScriptManager ID="ScriptManager1" runat="server" AsyncPostBackTimeout="900" EnableScriptGlobalization="true">
            <Scripts>
                <asp:ScriptReference Path="~/Scripts/AjaxControlToolkit/Bundle" />
            </Scripts>
        </asp:ScriptManager>
        <div style="text-align: left;">
            Causale :
         <asp:DropDownList ID="Dd_CausaleContabile" runat="server" Width="286px"
             AutoPostBack="true"
             OnSelectedIndexChanged="Dd_CausaleContabile_SelectedIndexChanged">
         </asp:DropDownList>
            &nbsp; Pagamento :
         <asp:DropDownList ID="DD_ModalitaPagamento" runat="server" Width="200px">
         </asp:DropDownList>
            <br />
            Data Registrazione :
        <asp:TextBox ID="Txt_DataRegistrazione" autocomplete="off" runat="server"
            Width="80px"></asp:TextBox>
            Data Documento :
        <asp:TextBox ID="Txt_DataDocumento" autocomplete="off" runat="server"
            Width="80px"></asp:TextBox>
            Numero Documento :&nbsp;
        <asp:TextBox ID="Txt_NumeroDocumento" autocomplete="off" runat="server"
            AutoPostBack="True" Width="80px" MaxLength="20"></asp:TextBox>&nbsp;
    <br />

            Cliente/Fornitore : 
         <asp:TextBox ID="Txt_ClienteFornitore" AutoPostBack="True"
             OnDisposed="Txt_ClienteFornitore_Disposed" runat="server" Width="476px"></asp:TextBox>
            <asp:Button ID="Button1" runat="server" Text="Aggiorna" Width="61px" />

            <br />

            <asp:Label ID="Lbl_RegistroIVA" runat="server" Text=""></asp:Label>
            Anno :&nbsp;
        <asp:TextBox ID="Txt_AnnoProtocollo" autocomplete="off"
            onkeypress="return soloNumeri(event);" runat="server" AutoPostBack="True"
            Width="60px" MaxLength="4"></asp:TextBox>&nbsp;
        Numero Protocollo :&nbsp;
        <asp:TextBox ID="Txt_NumeroProtocollo" autocomplete="off"
            onkeypress="return soloNumeri(event);" runat="server" AutoPostBack="True"
            Width="80px" MaxLength="10"></asp:TextBox>&nbsp;
    
         <br />
            Descrizione :
            <asp:TextBox ID="Txt_Descrizione" runat="server" Width="547px"></asp:TextBox>
            <br />
            Tipo Detraibilitìa : 
        <asp:DropDownList ID="DD_Detraibilita" runat="server">
        </asp:DropDownList>



            <table width="100%">
                <tr>
                    <td style="background-color: #99FF99; width: 60%;">Conto
                    </td>
                    <td style="background-color: #66CCFF; width: 20%;">IVA
                    </td>
                    <td style="background-color: #66FFFF; width: 20%;">Imponibile 
                    </td>
                </tr>
                <tr>
                    <td style="background-color: #99FF99; width: 60%;">
                        <asp:TextBox ID="Txt_Conto1" runat="server" Width="98%"></asp:TextBox>
                    </td>
                    <td style="background-color: #66FFFF; width: 20%;">
                        <asp:DropDownList ID="DD_IVA1" runat="server" Width="98%">
                        </asp:DropDownList>
                    </td>
                    <td style="background-color: #66CCFF; width: 20%;">
                        <asp:TextBox ID="Txt_Imponibile1" runat="server" onkeypress="ForceNumericInput(this, true, true)" Width="98%"></asp:TextBox>
                    </td>
                </tr>

                <tr>
                    <td style="background-color: #99FF99; width: 60%;">
                        <asp:TextBox ID="Txt_Conto2" runat="server" Width="98%"></asp:TextBox>
                    </td>
                    <td style="background-color: #66FFFF; width: 20%;">
                        <asp:DropDownList ID="DD_IVA2" runat="server" Width="98%">
                        </asp:DropDownList>
                    </td>
                    <td style="background-color: #66CCFF; width: 20%;">
                        <asp:TextBox ID="Txt_Imponibile2" runat="server" onkeypress="ForceNumericInput(this, true, true)" Width="98%"></asp:TextBox>
                    </td>
                </tr>

                <tr>
                    <td style="background-color: #99FF99; width: 60%;">
                        <asp:TextBox ID="Txt_Conto3" runat="server" Width="98%"></asp:TextBox>
                    </td>
                    <td style="background-color: #66CCFF; width: 20%;">
                        <asp:DropDownList ID="DD_IVA3" runat="server" Width="98%">
                        </asp:DropDownList>
                    </td>
                    <td style="background-color: #66FFFF; width: 20%;">
                        <asp:TextBox ID="Txt_Imponibile3" runat="server" onkeypress="ForceNumericInput(this, true, true)" Width="98%"></asp:TextBox>
                    </td>
                </tr>


                <tr>
                    <td style="background-color: #99FF99; width: 60%;">
                        <asp:TextBox ID="Txt_Conto4" runat="server" Width="98%"></asp:TextBox>
                    </td>

                    <td style="background-color: #66FFFF; width: 20%;">
                        <asp:DropDownList ID="DD_IVA4" runat="server" Width="98%">
                        </asp:DropDownList>
                    </td>
                    <td style="background-color: #66CCFF; width: 20%;">
                        <asp:TextBox ID="Txt_Imponibile4" runat="server" onkeypress="ForceNumericInput(this, true, true)" Width="98%"></asp:TextBox>
                    </td>
                </tr>

                <tr>
                    <td style="background-color: #99FF99; width: 60%;">
                        <asp:TextBox ID="Txt_Conto5" runat="server" Width="98%"></asp:TextBox>
                    </td>
                    <td style="background-color: #66FFFF; width: 20%;">
                        <asp:DropDownList ID="DD_IVA5" runat="server" Width="98%">
                        </asp:DropDownList>
                    </td>
                    <td style="background-color: #66CCFF; width: 20%;">
                        <asp:TextBox ID="Txt_Imponibile5" runat="server" onkeypress="ForceNumericInput(this, true, true)" Width="98%"></asp:TextBox>
                    </td>
                </tr>
            </table>



        </div>
        <p style="text-align: left;">
            <asp:ImageButton ID="Btn_Modifica" runat="server" ImageUrl="~/images/salva.jpg" class="EffettoBottoniTondi" Height="48px" Width="48px" ToolTip="Modifica / Inserisci" />
        </p>
    </form>
</body>
</html>
