﻿Imports Microsoft.VisualBasic
Imports System.Data.OleDb
Imports System.Web.Hosting
Imports System.IO.Compression
Imports System.IO
Imports System.Xml
Imports Ionic
Imports System.Net
Imports System
Imports System.Collections.Generic
Imports System.ComponentModel
Imports System.Data
Imports System.Drawing
Imports System.Linq
Imports System.Text
Imports System.Xml.Schema


Partial Class GeneraleWeb_FatturaElettronicaPrivati
    Inherits System.Web.UI.Page
    Dim Tabella As New System.Data.DataTable("tabella")





    Function Allegato(ByVal NumeroRegistrazione As Long) As String
        Dim NomeSocieta As String
        Dim NomeFile As String = ""
        Dim Datix64 As String = ""
        Dim XML As String

        Dim k As New Cls_Login

        k.Utente = Session("UTENTE")
        k.LeggiSP(Application("SENIOR"))

        NomeSocieta = k.RagioneSociale


        Dim Appo As String

        Appo = Dir(HostingEnvironment.ApplicationPhysicalPath() & "\Public\" & NomeSocieta)
        If Dir(HostingEnvironment.ApplicationPhysicalPath() & "\Public\" & NomeSocieta, FileAttribute.Directory) = "" Then
            MkDir(HostingEnvironment.ApplicationPhysicalPath() & "\Public\" & NomeSocieta)
        End If



        If Dir(HostingEnvironment.ApplicationPhysicalPath() & "\Public\" & NomeSocieta & "\Registrazione_" & NumeroRegistrazione, FileAttribute.Directory) = "" Then
            MkDir(HostingEnvironment.ApplicationPhysicalPath() & "\Public\" & NomeSocieta & "\Registrazione_" & NumeroRegistrazione)
        End If


        Datix64 = ""
        XML = ""
        Dim objDI As New DirectoryInfo(HostingEnvironment.ApplicationPhysicalPath() & "\Public\" & NomeSocieta & "\Registrazione_" & NumeroRegistrazione & "\")
        Dim aryItemsInfo() As FileSystemInfo
        Dim objItem As FileSystemInfo

        aryItemsInfo = objDI.GetFileSystemInfos()
        For Each objItem In aryItemsInfo

            Datix64 = ""

            NomeFile = objItem.Name
            Dim readFile As System.IO.TextReader = New StreamReader(HostingEnvironment.ApplicationPhysicalPath() & "\Public\" & NomeSocieta & "\Registrazione_" & NumeroRegistrazione & "\" & objItem.Name)

            Datix64 = readFile.ReadToEnd()



            readFile.Close()


            XML = XML & "<Allegati>" & vbNewLine
            XML = XML & "<NomeAttachment>" & NomeFile & "</NomeAttachment>" & vbNewLine
            XML = XML & "<FormatoAttachment>PDF</FormatoAttachment>" & vbNewLine
            XML = XML & "<DescrizioneAttachment>" & NomeFile & "</DescrizioneAttachment>" & vbNewLine
            XML = XML & "<Attachment>"
            XML = XML & Datix64
            XML = XML & "</Attachment>" & vbNewLine
            XML = XML & "</Allegati>" & vbNewLine



        Next


        Return XML
    End Function



    Function AllegatoPresente(ByVal NumeroRegistrazione As Long) As Boolean
        Dim NomeSocieta As String
        Dim NomeFile As String = ""

        Dim k As New Cls_Login

        k.Utente = Session("UTENTE")
        k.LeggiSP(Application("SENIOR"))

        NomeSocieta = k.RagioneSociale


        Dim Appo As String

        Appo = Dir(HostingEnvironment.ApplicationPhysicalPath() & "\Public\" & NomeSocieta)
        If Dir(HostingEnvironment.ApplicationPhysicalPath() & "\Public\" & NomeSocieta, FileAttribute.Directory) = "" Then
            MkDir(HostingEnvironment.ApplicationPhysicalPath() & "\Public\" & NomeSocieta)
        End If



        If Dir(HostingEnvironment.ApplicationPhysicalPath() & "\Public\" & NomeSocieta & "\Registrazione_" & NumeroRegistrazione, FileAttribute.Directory) = "" Then
            Return False
            Exit Function
        End If



        Dim objDI As New DirectoryInfo(HostingEnvironment.ApplicationPhysicalPath() & "\Public\" & NomeSocieta & "\Registrazione_" & NumeroRegistrazione & "\")
        Dim aryItemsInfo() As FileSystemInfo
        Dim objItem As FileSystemInfo

        aryItemsInfo = objDI.GetFileSystemInfos()
        For Each objItem In aryItemsInfo

            NomeFile = objItem.Name


            Exit For
        Next

        If NomeFile = "" Then
            Return False
        Else
            Return True
        End If


    End Function
    Function campodb(ByVal oggetto As Object) As String
        If IsDBNull(oggetto) Then
            Return ""
        Else
            Return oggetto
        End If
    End Function
    Function campodbN(ByVal oggetto As Object) As Double
        If IsDBNull(oggetto) Then
            Return 0
        Else
            Return oggetto
        End If
    End Function



    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Trim(Session("UTENTE")) = "" Then
            Response.Redirect("/SeniorWeb/Login.aspx")
            Exit Sub
        End If

        EseguiJS()

        If Page.IsPostBack = True Then
            Exit Sub
        End If

        Dim K1 As New Cls_SqlString

        Dim Barra As New Cls_BarraSenior

        If Not IsNothing(Session("RicercaGeneraleSQLString")) Then
            K1 = Session("RicercaGeneraleSQLString")
        End If

        Lbl_BarraSenior.Text = Barra.CodiceBarra(Application("SENIOR"), Session("UTENTE"), Page, K1)


        Dim MyReg As New Cls_RegistroIVA

        MyReg.UpDateDropBox(Session("DC_TABELLE"), DD_RegIVa)

        Btn_Send.Visible = False

        Dim DatiGen As New Cls_DatiGenerali

        DatiGen.LeggiDati(Session("DC_TABELLE"))

        If DatiGen.TokenAgyo.Trim <> "" Then
            Btn_Send.Visible = True
        End If

        Dim M As New Cls_CausaleContabile

        M.UpDateDropBoxDoc(Session("DC_TABELLE"), Dd_CausaleContabile)



    End Sub


    Private Sub GrigliaProcedureAbilitate()

        Dim cnTabelle As OleDbConnection

        cnTabelle = New Data.OleDb.OleDbConnection(Session("DC_TABELLE"))

        cnTabelle.Open()

        Dim cmdProcedureAbilitate As New OleDbCommand()
        Dim ProcedureAbilitate As String = ""

        cmdProcedureAbilitate.CommandText = ("select * from Societa ")
        cmdProcedureAbilitate.Connection = cnTabelle
        Dim VerProcedureAbilitate As OleDbDataReader = cmdProcedureAbilitate.ExecuteReader()
        If VerProcedureAbilitate.Read Then
            ProcedureAbilitate = campodb(VerProcedureAbilitate.Item("ProcedureAbilitate"))
        End If
        VerProcedureAbilitate.Close()
        cnTabelle.Close()

        If ProcedureAbilitate.IndexOf("<ALLEGATIDOCUMENTI>") >= 0 Then
            GridView1.Columns(2).Visible = True
        Else
            GridView1.Columns(2).Visible = False
        End If
    End Sub

    Private Sub Visualizza()
        Dim cn As OleDbConnection
        Dim MySql As String
        Dim Param As New Cls_DatiGenerali


        Param.LeggiDati(Session("DC_TABELLE"))

        Dim DatiSocieta As New Cls_TabellaSocieta

        DatiSocieta.Leggi(Session("DC_TABELLE"))


        Tabella.Clear()
        Tabella.Columns.Clear()
        Tabella.Columns.Add("NumeroRegistrazione", GetType(String))

        Tabella.Columns.Add("Progressivo", GetType(String))
        Tabella.Columns.Add("DataRegistrazione", GetType(String))
        Tabella.Columns.Add("NumeroProtocollo", GetType(String))
        Tabella.Columns.Add("Tipo Doc", GetType(String))
        Tabella.Columns.Add("Ragione Sociale", GetType(String))
        Tabella.Columns.Add("ImportoDocumento", GetType(String))
        Tabella.Columns.Add("Segnalazione", GetType(String))
        Tabella.Columns.Add("Bollo", GetType(String))

        Tabella.Columns.Add("AllegatoPresente", GetType(String))


        'TipoDocumento

        cn = New Data.OleDb.OleDbConnection(Session("DC_GENERALE"))

        cn.Open()

        Dim cmdSaldo As New OleDbCommand()


        If Dd_CausaleContabile.SelectedValue = "" Then
            If DD_RegIVa.SelectedValue = "" Or DD_RegIVa.SelectedValue = "0" Then
                cmdSaldo.CommandText = "SELECT *,(select top 1 Progressivo  from FatturaElettronica where FatturaElettronica.NumeroRegistrazione = MovimentiContabiliTesta.NumeroRegistrazione) as Progressivo FROM MovimentiContabiliTesta Where DataRegistrazione >= ? And DataRegistrazione <= ? And RegistroIVA > 0  Order by NumeroProtocollo"
            Else
                cmdSaldo.CommandText = "SELECT *,(select top 1 Progressivo  from FatturaElettronica where FatturaElettronica.NumeroRegistrazione = MovimentiContabiliTesta.NumeroRegistrazione) as Progressivo FROM MovimentiContabiliTesta Where DataRegistrazione >= ? And DataRegistrazione <= ? And RegistroIVA = " & Val(DD_RegIVa.SelectedValue) & "  Order by NumeroProtocollo"
            End If
        Else
            If DD_RegIVa.SelectedValue = "" Or DD_RegIVa.SelectedValue = "0" Then
                cmdSaldo.CommandText = "SELECT *,(select top 1 Progressivo  from FatturaElettronica where FatturaElettronica.NumeroRegistrazione = MovimentiContabiliTesta.NumeroRegistrazione) as Progressivo FROM MovimentiContabiliTesta Where DataRegistrazione >= ? And DataRegistrazione <= ? And RegistroIVA > 0 And CausaleContabile = '" & Dd_CausaleContabile.SelectedValue & "' Order by NumeroProtocollo"
            Else
                cmdSaldo.CommandText = "SELECT *,(select top 1 Progressivo  from FatturaElettronica where FatturaElettronica.NumeroRegistrazione = MovimentiContabiliTesta.NumeroRegistrazione) as Progressivo FROM MovimentiContabiliTesta Where DataRegistrazione >= ? And DataRegistrazione <= ? And RegistroIVA = " & Val(DD_RegIVa.SelectedValue) & " And CausaleContabile = '" & Dd_CausaleContabile.SelectedValue & "'" & "  Order by NumeroProtocollo"
            End If
        End If

        cmdSaldo.Connection = cn
        cmdSaldo.Parameters.AddWithValue("@DataRegistrazione", Txt_DataDal.Text)
        cmdSaldo.Parameters.AddWithValue("@DataRegistrazione", Txt_DataAl.Text)


        Dim ReadSaldo As OleDbDataReader = cmdSaldo.ExecuteReader()
        Do While ReadSaldo.Read
            Dim Mov As New Cls_MovimentoContabile
            Dim FatturaElettronica As String = ""

            Mov.NumeroRegistrazione = ReadSaldo.Item("NumeroRegistrazione")
            Mov.Leggi(Session("DC_GENERALE"), Mov.NumeroRegistrazione)

            Dim CausaleContabile As New Cls_CausaleContabile

            CausaleContabile.Codice = Mov.CausaleContabile
            CausaleContabile.Leggi(Session("DC_TABELLE"), CausaleContabile.Codice)

            If CausaleContabile.VenditaAcquisti = "V" And (Chk_generato.Checked = False Or (Chk_generato.Checked = True And campodbN(ReadSaldo.Item("Progressivo")) = 0)) Then
                Dim DatiAna As New DatiIntestatario

                DatiAna = IntestatarioFattura(Mov.NumeroRegistrazione)

                If Mid(DatiAna.CodiceCatastale & Space(10), 1, 1) <> "Z" Then

                    Dim myriga As System.Data.DataRow = Tabella.NewRow()

                    myriga(0) = Mov.NumeroRegistrazione
                    myriga(1) = campodbN(ReadSaldo.Item("Progressivo"))
                    myriga(2) = Format(Mov.DataRegistrazione, "dd/MM/yyyy")
                    myriga(3) = Mov.NumeroProtocollo
                    myriga(4) = CausaleContabile.TipoDocumento

                    Dim PianoConti As New Cls_Pianodeiconti

                    PianoConti.Mastro = Mov.Righe(0).MastroPartita
                    PianoConti.Conto = Mov.Righe(0).ContoPartita
                    PianoConti.Sottoconto = Mov.Righe(0).SottocontoPartita
                    PianoConti.Decodfica(Session("DC_GENERALE"))

                    myriga(5) = PianoConti.Descrizione

                    myriga(6) = Format(Mov.ImportoDocumento(Session("DC_TABELLE")), "#,##0.00")

                    Dim Segnalazioni As String = ""
                    If IsNothing(DatiAna.Provincia) Then
                        DatiAna.Provincia = ""
                    End If
                    If IsNothing(DatiAna.Cap) Then
                        DatiAna.Cap = ""
                    End If
                    If IsNothing(DatiAna.Indirizzo) Then
                        DatiAna.Indirizzo = ""
                    End If
                    If IsNothing(DatiAna.Citta) Then
                        DatiAna.Citta = ""
                    End If
                    If IsNothing(DatiAna.PartitaIVA) Then
                        DatiAna.PartitaIVA = ""
                    End If
                    If IsNothing(DatiAna.CodiceFiscale) Then
                        DatiAna.CodiceFiscale = ""
                    End If

                    If DatiAna.Cap.Trim = "" Then
                        Segnalazioni = Segnalazioni & " Cap non inidicato"
                    End If
                    If DatiAna.Indirizzo.Trim = "" Then
                        Segnalazioni = Segnalazioni & " Indirizzo non inidicato"
                    End If
                    If DatiAna.Provincia.Trim = "" And DatiAna.Stato = "SI" Then
                        Segnalazioni = Segnalazioni & " Provincia non inidicata"
                    End If
                    If DatiAna.Citta.Trim = "" Then
                        Segnalazioni = Segnalazioni & " Città non inidicato"
                    End If
                    If Val(DatiAna.PartitaIVA) = 0 And DatiAna.CodiceFiscale.Trim = "" Then
                        Segnalazioni = Segnalazioni & " Non indicato Piva o Cf"
                    End If

                    If DatiAna.TipoAnagrafica = "" Then
                        If DatiAna.CodiceDestinatario = "" Or DatiAna.CodiceDestinatario = "0000000" Then
                            Segnalazioni = Segnalazioni & " Devi inserire codice destinatario"
                        End If
                    End If
                    If DatiAna.TipoInvio = "N" Then
                        If DatiAna.IdDocumento <> "" Then
                            Segnalazioni = Segnalazioni & " Hai selezionato [Non Indicare] l ID Documento non sarà inserito nell xml (" & Mov.Tipologia & ")"
                        End If
                        If DatiAna.Cup <> "" Then
                            Segnalazioni = Segnalazioni & " Hai selezionato [Non Indicare] il CodiceCup non sarà inserito nell xml (" & Mov.Tipologia & ")"
                        End If
                        If DatiAna.Cig <> "" Then
                            Segnalazioni = Segnalazioni & " Hai selezionato [Non Indicare] il CodiceCig non sarà inserito nell xml (" & Mov.Tipologia & ")"
                        End If
                    End If

                    If Param.FatturazionePrivati = 0 And DatiAna.TipoAnagrafica = "P" Then
                        Segnalazioni = Segnalazioni & " FATTURE ELETTRONICHE PRIVATI NON ATTIVO"
                    End If
                    myriga(7) = Segnalazioni


                    Dim TipoRegistroXBollo As New Cls_RegistroIVA

                    TipoRegistroXBollo.Tipo = Mov.RegistroIVA
                    TipoRegistroXBollo.Leggi(Session("DC_TABELLE"), TipoRegistroXBollo.Tipo)

                    If Format(Mov.DataRegistrazione, "yyyyMMdd") < "20190418" Then
                        Dim kInd As Integer
                        Dim TrovaBollo As Boolean = False


                        If CausaleContabile.Tipo = "R" Then
                            For kInd = 0 To 100
                                If Not IsNothing(Mov.Righe(kInd)) Then
                                    If Mov.Righe(kInd).RigaDaCausale = 9 Then
                                        TrovaBollo = True
                                    End If
                                End If
                            Next
                        End If
                        If TrovaBollo = False Then
                            Dim DatiBollo As New Cls_bolli

                            DatiBollo.Leggi(Session("DC_TABELLE"), Mov.DataRegistrazione)

                            For kInd = 0 To 100
                                If Not IsNothing(Mov.Righe(kInd)) Then
                                    If Mov.Righe(kInd).CodiceIVA = DatiBollo.CodiceIVA And Mov.Righe(kInd).Importo = DatiBollo.ImportoBollo And _
                                     Mov.Righe(kInd).MastroPartita = Param.MastroBollo And Mov.Righe(kInd).ContoPartita = Param.ContoBollo And Mov.Righe(kInd).SottocontoPartita = Param.SottoContoBollo Then
                                        TrovaBollo = True
                                    End If
                                End If
                            Next
                        End If


                        If DatiAna.SoggettoABollo = "N" Then
                            TipoRegistroXBollo.RegistroCartaceo = 1
                        End If

                        myriga(8) = ""
                        If TipoRegistroXBollo.RegistroCartaceo = 0 Then
                            If DatiSocieta.BOLLOVIRTUALE > 0 And TrovaBollo Then
                                myriga(8) = "SI"
                            End If
                        End If

                        If DatiAna.BolloVirtuale = 1 Then
                            myriga(8) = "SI"
                        End If
                    Else
                        If TipoRegistroXBollo.RegistroCartaceo = 0 Then
                            If Mov.BolloVirtuale = 1 Then
                                myriga(8) = "SI"
                            End If
                        End If
                    End If


                    If AllegatoPresente(Mov.NumeroRegistrazione) = True Then
                        myriga(9) = "SI"
                    Else
                        myriga(9) = ""
                    End If


                    Tabella.Rows.Add(myriga)
                End If
            End If
        Loop
        ReadSaldo.Close()

        cn.Close()


        ViewState("Appoggio") = Tabella

        GridView1.AutoGenerateColumns = False

        GridView1.DataSource = Tabella

        GridView1.DataBind()

        GridView1.Visible = True

        Dim Riga As Integer

        For Riga = 0 To GridView1.Rows.Count - 1
            Dim CheckBox As CheckBox = DirectCast(GridView1.Rows(Riga).FindControl("ChkImporta"), CheckBox)


            If campodb(Tabella.Rows(Riga).Item("Segnalazione")) <> "" Then
                CheckBox.Enabled = False
                Dim Colonna As Integer
                For Colonna = 1 To Tabella.Columns.Count
                    GridView1.Rows(Riga).Cells(Colonna).BackColor = Drawing.Color.Red
                Next
            End If
        Next

        GrigliaProcedureAbilitate()
    End Sub


    Protected Sub Btn_Movimenti_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles Btn_Movimenti.Click

        If DD_RegIVa.SelectedValue = "" Then
            ScriptManager.RegisterStartupScript(Me, Me.GetType(), "visualizzaA", "VisualizzaErrore('Devi Selezionare il registro');", True)
            Exit Sub
        End If

        If Val(DD_RegIVa.SelectedValue) = 0 Then
            ScriptManager.RegisterStartupScript(Me, Me.GetType(), "visualizzaA", "VisualizzaErrore('Devi Selezionare il Registro IVA');", True)
            Exit Sub
        End If


        If Not IsDate(Txt_DataDal.Text) Then
            ScriptManager.RegisterStartupScript(Me, Me.GetType(), "visualizzaA", "VisualizzaErrore('Data Dal formalmente errata');", True)
            Exit Sub
        End If


        If Not IsDate(Txt_DataAl.Text) Then
            ScriptManager.RegisterStartupScript(Me, Me.GetType(), "visualizzaA", "VisualizzaErrore('Data Al formalmente errata');", True)
            Exit Sub
        End If

        Visualizza()

    End Sub

    Private Sub EseguiJS()
        Dim MyJs As String
        MyJs = "$(document).ready(function() {"

        MyJs = MyJs & "var els = document.getElementsByTagName(""*"");"

        MyJs = MyJs & "for (var i=0;i<els.length;i++)"
        MyJs = MyJs & "if ( els[i].id ) { "
        MyJs = MyJs & " var appoggio =els[i].id; "
        MyJs = MyJs & " if (appoggio.match('Txt_RateoRisconto')!= null || appoggio.match('Txt_Costo')!= null) {  "
        MyJs = MyJs & " $(els[i]).autocomplete('/WebHandler/GestioneConto.ashx?Utente=" & Session("UTENTE") & "', {delay:5,minChars:3});"
        MyJs = MyJs & "    }"

        MyJs = MyJs & " if (appoggio.match('Txt_Importo')!= null)  {  "
        MyJs = MyJs & " $(els[i]).keypress(function() { ForceNumericInput($(this).val(), true, true); } ); "
        MyJs = MyJs & " $(els[i]).blur(function() { var ap = formatNumber($(this).val(),2); $(this).val(ap); });"
        MyJs = MyJs & "    }"

        MyJs = MyJs & " if (appoggio.match('Txt_DataDal')!= null || appoggio.match('Txt_DataAl')!= null)  {  "
        MyJs = MyJs & " $(els[i]).mask(""99/99/9999""); "
        MyJs = MyJs & " $(els[i]).datepicker({ changeMonth: true,changeYear: true,  yearRange: ""1890:" & Year(Now) + 1 & """},$.datepicker.regional[""it""]);"
        MyJs = MyJs & "    }"


        MyJs = MyJs & "} "
        MyJs = MyJs & "});"

        'MyJs = "$(document).ready(function() { $('.myClass').keypress(function() { handleEnter($('.myClass').val(), true, true); } );     $('#" & Txt_ImportoPagato.ClientID & "').blur(function() { var ap = formatNumber ($('#" & Txt_ImportoPagato.ClientID & "').val(),2); $('#" & Txt_ImportoPagato.ClientID & "').val(ap); }); });"
        ScriptManager.RegisterStartupScript(Me, Me.GetType(), "visualizzaRitIm", MyJs, True)
    End Sub

    Protected Sub Btn_Modifica_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles Btn_Modifica.Click
        CreateXML(False, 0)
    End Sub


    Private Function IntestatarioFattura(ByVal NumeroRegistrazione As Integer) As DatiIntestatario
        Dim Registrazione As New Cls_MovimentoContabile
        Dim Tipologia As String = ""
        Dim XCodiceRegione As String = ""
        Dim XCodiceComune As String = ""
        Dim XCodiceProvincia As String = ""
        Dim DT As New DatiIntestatario

        Dim cn As OleDbConnection

        cn = New Data.OleDb.OleDbConnection(Session("DC_OSPITE"))

        cn.Open()

        If NumeroRegistrazione = 49453 Then
            NumeroRegistrazione = 49453
        End If

        DT.TipoAnagrafica = "P"
        DT.RagioneSociale = ""
        DT.PartitaIVA = ""
        DT.CodiceFiscale = ""
        DT.Indirizzo = ""
        DT.Cap = ""
        DT.Citta = ""
        DT.Provincia = ""
        DT.Stato = ""
        DT.CodiceDestinatario = ""
        DT.TipoInvio = ""
        DT.Cup = ""
        DT.Cig = ""
        DT.Iban = ""
        DT.Banca = ""
        DT.IdDocumento = ""
        DT.IDDataDoc = ""
        DT.RifAmministrazione = ""
        DT.NumeroFatturaDDT = ""
        DT.RigaNote = ""
        DT.NoteFattura = ""
        DT.ModalitaPagamento = ""
        DT.NumItem = ""
        DT.CodiceCommessaConvezione = ""
        DT.Stato = "IT"
        DT.Causale = ""
        DT.SoggettoABollo = ""
        DT.BolloVirtuale = 0
        DT.NonScadenza = ""
        DT.DescrizioneXML = ""

        Registrazione.NumeroRegistrazione = NumeroRegistrazione
        Registrazione.Leggi(Session("DC_GENERALE"), Registrazione.NumeroRegistrazione)

        Tipologia = Registrazione.Tipologia

        If Mid(Tipologia & Space(10), 1, 1) = "R" Then
            XCodiceRegione = Mid(Tipologia & Space(10), 2, 4)
            Tipologia = "R"
        End If
        If Mid(Tipologia & Space(10), 1, 1) = "C" Then
            XCodiceProvincia = Mid(Tipologia & Space(10), 2, 3)
            XCodiceComune = Mid(Tipologia & Space(10), 5, 3)
            Tipologia = "C"
        End If
        If Mid(Tipologia & Space(10), 1, 1) = "J" Then
            XCodiceProvincia = Mid(Tipologia & Space(10), 2, 3)
            XCodiceComune = Mid(Tipologia & Space(10), 5, 3)
            Tipologia = "J"
        End If
        If Tipologia = "R" Then
            Dim Regioni As New ClsUSL

            Regioni.CodiceRegione = XCodiceRegione
            Regioni.Leggi(Session("DC_OSPITE"))


            DT.CodiceDestinatario = Regioni.CodiceDestinatario
            DT.CodiceFiscale = Regioni.CodiceFiscale
            DT.PartitaIVA = Format(Regioni.PARTITAIVA, "00000000000")
            DT.RagioneSociale = Regioni.Nome
            DT.Indirizzo = Regioni.RESIDENZAINDIRIZZO1
            DT.Cap = Regioni.RESIDENZACAP1

            Dim ksCom As New ClsComune

            ksCom.Provincia = Regioni.RESIDENZAPROVINCIA1
            ksCom.Comune = Regioni.RESIDENZACOMUNE1
            ksCom.Leggi(Session("DC_OSPITE"))

            DT.Citta = ksCom.Descrizione



            DT.TipoInvio = Regioni.EGo

            If ksCom.CodificaProvincia = "" Then
                ksCom.Provincia = Regioni.RESIDENZAPROVINCIA1
                ksCom.Comune = ""
                ksCom.Leggi(Session("DC_OSPITE"))
            End If

            DT.Provincia = ksCom.CodificaProvincia


            DT.Cup = Regioni.CodiceCup
            DT.Cig = Regioni.CodiceCig
            DT.Iban = AdattaLunghezza(Regioni.IntCliente, 2) & AdattaLunghezzaNumero(Regioni.NumeroControlloCliente, 2) & AdattaLunghezza(Regioni.CINCLIENTE, 1) & AdattaLunghezzaNumero(Regioni.ABICLIENTE, 5) & AdattaLunghezzaNumero(Regioni.CABCLIENTE, 5) & AdattaLunghezzaNumero(Regioni.CCBANCARIOCLIENTE, 12)
            DT.Banca = Regioni.BancaCliente

            Dim ModPAg As New ClsModalitaPagamento

            ModPAg.Codice = Regioni.ModalitaPagamento
            ModPAg.Leggi(Session("DC_OSPITE"))


            If campodb(ModPAg.Codice) <> "" Then
                Dim ModPAgGen As New Cls_TipoPagamento
                ModPAgGen.Codice = campodb(ModPAg.Codice)
                ModPAgGen.Leggi(Session("DC_TABELLE"))
                DT.ModalitaPagamento = ModPAgGen.CampoFE
            End If


            Dim TipoOperarazione As New Cls_TipoOperazione

            TipoOperarazione.Codice = Regioni.TipoOperazione
            TipoOperarazione.Leggi(Session("DC_OSPITE"), TipoOperarazione.Codice)

            Dim Eccezioni As New Cls_DatiComnueRegioneServizio

            Eccezioni.CodiceRegione = Regioni.CodiceRegione
            Eccezioni.CentroServizio = Registrazione.CentroServizio
            Eccezioni.Leggi(Session("DC_OSPITE"))
            If Eccezioni.TipoOperazione <> "" Then
                TipoOperarazione.Codice = Eccezioni.TipoOperazione
                TipoOperarazione.Leggi(Session("DC_OSPITE"), TipoOperarazione.Codice)
            End If


            If TipoOperarazione.BolloVirtuale = 1 Then
                DT.BolloVirtuale = 1
            End If

            DT.IdDocumento = Regioni.IdDocumento
            DT.IDDataDoc = Regioni.IdDocumentoData
            DT.RifAmministrazione = Regioni.RiferimentoAmministrazione
            DT.NumeroFatturaDDT = Regioni.NumeroFatturaDDT
            DT.NumItem = Regioni.NumItem
            DT.RigaNote = Regioni.Note
            DT.DescrizioneXML = Regioni.DescrizioneXML

            Dim k As New Cls_NoteFattureOspiti

            k.CodiceServizio = Registrazione.CentroServizio
            k.CodiceRegione = XCodiceRegione
            k.LeggiRegioneCserv(Session("DC_OSPITE"), k.CodiceServizio, k.CodiceRegione)
            DT.NoteFattura = k.NoteUp & Chr(254) & k.NoteDown


            DT.TipoAnagrafica = ""
        End If

        If Tipologia = "C" Then
            Dim Comune As New ClsComune

            Comune.Provincia = XCodiceProvincia
            Comune.Comune = XCodiceComune
            Comune.Leggi(Session("DC_OSPITE"))
            DT.RagioneSociale = Comune.Descrizione

            DT.CodiceDestinatario = Comune.CodiceDestinatario
            DT.CodiceFiscale = Comune.CodiceFiscale
            DT.PartitaIVA = Format(Comune.PartitaIVA, "00000000000")

            DT.Citta = Comune.Descrizione
            DT.Indirizzo = Comune.RESIDENZAINDIRIZZO1
            DT.Cap = Comune.RESIDENZACAP1

            DT.TipoInvio = Comune.EGo

            DT.Causale = Comune.Causale

            If Comune.Privato = 1 Then
                DT.TipoAnagrafica = "P"
            Else
                DT.TipoAnagrafica = ""
            End If


            DT.SoggettoABollo = Comune.SoggettoABollo


            If Comune.IdDocumentoData <> "" Then
                DT.IDDataDoc = Comune.IdDocumentoData
            End If

            Dim ksCom As New ClsComune

            ksCom.Provincia = Comune.RESIDENZAPROVINCIA1
            ksCom.Comune = Comune.RESIDENZACOMUNE1
            ksCom.Leggi(Session("DC_OSPITE"))

            DT.Citta = ksCom.Descrizione

            If ksCom.CodificaProvincia = "" Then
                ksCom.Provincia = Comune.RESIDENZAPROVINCIA1
                ksCom.Comune = ""
                ksCom.Leggi(Session("DC_OSPITE"))
            End If

            DT.Provincia = ksCom.CodificaProvincia
            DT.Cup = Comune.CodiceCup
            DT.Cig = Comune.CodiceCig
            DT.Iban = AdattaLunghezza(Comune.IntCliente, 2) & AdattaLunghezzaNumero(Comune.NumeroControlloCliente, 2) & AdattaLunghezza(Comune.CINCLIENTE, 1) & AdattaLunghezzaNumero(Comune.ABICLIENTE, 5) & AdattaLunghezzaNumero(Comune.CABCLIENTE, 5) & AdattaLunghezzaNumero(Comune.CCBANCARIOCLIENTE, 12)
            DT.Banca = Comune.BANCACLIENTE

            DT.CodiceCommessaConvezione = Comune.CodiceCommessaConvezione

            Dim ModPAg As New ClsModalitaPagamento

            ModPAg.Codice = Comune.ModalitaPagamento
            ModPAg.Leggi(Session("DC_OSPITE"))


            If campodb(ModPAg.Codice) <> "" Then
                Dim ModPAgGen As New Cls_TipoPagamento
                ModPAgGen.Codice = campodb(ModPAg.Codice)
                ModPAgGen.Leggi(Session("DC_TABELLE"))
                DT.ModalitaPagamento = ModPAgGen.CampoFE
            End If

            DT.IdDocumento = Comune.IdDocumento
            DT.RifAmministrazione = Comune.RiferimentoAmministrazione
            DT.NumeroFatturaDDT = Comune.NumeroFatturaDDT
            DT.NumItem = Comune.NumItem
            DT.RigaNote = Comune.Note
            DT.DescrizioneXML = Comune.DescrizioneXML

            Dim k As New Cls_NoteFattureOspiti

            k.CodiceServizio = Registrazione.CentroServizio
            k.CodiceProvincia = XCodiceProvincia
            k.CodiceComune = XCodiceComune
            k.LeggiComuneCserv(Session("DC_OSPITE"), k.CodiceServizio, k.CodiceProvincia, k.CodiceComune)
            DT.NoteFattura = k.NoteUp & Chr(254) & k.NoteDown



            Dim TipoOperarazione As New Cls_TipoOperazione

            TipoOperarazione.Codice = Comune.TipoOperazione
            TipoOperarazione.Leggi(Session("DC_OSPITE"), TipoOperarazione.Codice)

            Dim Eccezioni As New Cls_DatiComnueRegioneServizio

            Eccezioni.CodiceProvincia = Comune.Provincia
            Eccezioni.CodiceComune = Comune.Comune
            Eccezioni.CentroServizio = Registrazione.CentroServizio
            Eccezioni.Leggi(Session("DC_OSPITE"))
            If Eccezioni.TipoOperazione <> "" Then
                TipoOperarazione.Codice = Eccezioni.TipoOperazione
                TipoOperarazione.Leggi(Session("DC_OSPITE"), TipoOperarazione.Codice)
            End If

            If TipoOperarazione.BolloVirtuale = 1 Then
                DT.BolloVirtuale = 1
            End If
        End If

        If Tipologia = "J" Then
            Dim Comune As New ClsComune

            Comune.Provincia = XCodiceProvincia
            Comune.Comune = XCodiceComune
            Comune.Leggi(Session("DC_OSPITE"))


            DT.CodiceDestinatario = Comune.CodiceDestinatario
            DT.CodiceFiscale = Comune.CodiceFiscale
            DT.PartitaIVA = Format(Comune.PartitaIVA, "00000000000")

            DT.RagioneSociale = Comune.Descrizione
            DT.Indirizzo = Comune.RESIDENZAINDIRIZZO1
            DT.Cap = Comune.RESIDENZACAP1

            DT.TipoInvio = Comune.EGo

            If Comune.Privato = 1 Then
                DT.TipoAnagrafica = "P"
            Else
                DT.TipoAnagrafica = ""
            End If

            If Comune.IdDocumentoData <> "" Then
                DT.IDDataDoc = Comune.IdDocumentoData
            End If

            Dim ksCom As New ClsComune

            ksCom.Provincia = Comune.RESIDENZAPROVINCIA1
            ksCom.Comune = Comune.RESIDENZACOMUNE1
            ksCom.Leggi(Session("DC_OSPITE"))

            DT.Citta = ksCom.Descrizione

            If ksCom.CodificaProvincia = "" Then
                ksCom.Provincia = Comune.RESIDENZAPROVINCIA1
                ksCom.Comune = ""
                ksCom.Leggi(Session("DC_OSPITE"))
            End If


            DT.Provincia = ksCom.CodificaProvincia
            DT.Cup = Comune.CodiceCup
            DT.Cig = Comune.CodiceCig
            DT.Iban = AdattaLunghezza(Comune.IntCliente, 2) & AdattaLunghezzaNumero(Comune.NumeroControlloCliente, 2) & AdattaLunghezza(Comune.CINCLIENTE, 1) & AdattaLunghezzaNumero(Comune.ABICLIENTE, 5) & AdattaLunghezzaNumero(Comune.CABCLIENTE, 5) & AdattaLunghezzaNumero(Comune.CCBANCARIOCLIENTE, 12)
            DT.Banca = Comune.BANCACLIENTE



            Dim ModPAg As New ClsModalitaPagamento

            ModPAg.Codice = Comune.ModalitaPagamento
            ModPAg.Leggi(Session("DC_OSPITE"))


            If campodb(ModPAg.Codice) <> "" Then
                Dim ModPAgGen As New Cls_TipoPagamento
                ModPAgGen.Codice = campodb(ModPAg.Codice)
                ModPAgGen.Leggi(Session("DC_TABELLE"))
                DT.ModalitaPagamento = ModPAgGen.CampoFE
            End If

            DT.IdDocumento = Comune.IdDocumento
            DT.RifAmministrazione = Comune.RiferimentoAmministrazione
            DT.NumeroFatturaDDT = Comune.NumeroFatturaDDT
            DT.NumItem = Comune.NumItem

            DT.RigaNote = Comune.Note

            DT.DescrizioneXML = Comune.DescrizioneXML


            Dim k As New Cls_NoteFattureOspiti

            k.CodiceServizio = Registrazione.CentroServizio
            k.CodiceProvincia = XCodiceProvincia
            k.CodiceComune = XCodiceComune
            k.LeggiComuneCserv(Session("DC_OSPITE"), k.CodiceServizio, k.CodiceProvincia, k.CodiceComune)
            DT.NoteFattura = k.NoteUp & Chr(254) & k.NoteDown





            Dim TipoOperarazione As New Cls_TipoOperazione

            TipoOperarazione.Codice = Comune.TipoOperazione
            TipoOperarazione.Leggi(Session("DC_OSPITE"), TipoOperarazione.Codice)

            Dim Eccezioni As New Cls_DatiComnueRegioneServizio

            Eccezioni.CodiceProvincia = Comune.Provincia
            Eccezioni.CodiceComune = Comune.Comune
            Eccezioni.CentroServizio = Registrazione.CentroServizio
            Eccezioni.Leggi(Session("DC_OSPITE"))
            If Eccezioni.TipoOperazione <> "" Then
                TipoOperarazione.Codice = Eccezioni.TipoOperazione
                TipoOperarazione.Leggi(Session("DC_OSPITE"), TipoOperarazione.Codice)
            End If


            If TipoOperarazione.BolloVirtuale = 1 Then
                DT.BolloVirtuale = 1
            End If
        End If

        If Tipologia = "" Or Tipologia = "O" Or Tipologia = "P" Then
            Dim cmdTipologiaCF As New OleDbCommand
            Dim MastroPartita As Long = Registrazione.Righe(0).MastroPartita
            Dim ContoPartita As Long = Registrazione.Righe(0).ContoPartita
            Dim SottocontoPartita As Long = Registrazione.Righe(0).SottocontoPartita
            Dim PianoConti As New Cls_Pianodeiconti

            PianoConti.Mastro = MastroPartita
            PianoConti.Conto = ContoPartita
            PianoConti.Sottoconto = SottocontoPartita
            PianoConti.Decodfica(Session("DC_GENERALE"))

            If PianoConti.TipoAnagrafica = "D" Then
                cmdTipologiaCF.CommandText = "Select  * From AnagraficaComune Where MastroCliente = " & MastroPartita & " And ContoCliente = " & ContoPartita & " And SottocontoCliente  = " & SottocontoPartita
            Else
                If Int(SottocontoPartita / 100) = SottocontoPartita / 100 Then
                    cmdTipologiaCF.CommandText = "Select  * From AnagraficaComune Where CodiceParente = 0 And CodiceOspite  = " & Int(SottocontoPartita / 100)
                Else
                    cmdTipologiaCF.CommandText = "Select  * From AnagraficaComune Where CodiceOspite  = " & Int(SottocontoPartita / 100) & " And CodiceParente = " & SottocontoPartita - (Int(SottocontoPartita / 100) * 100)
                End If
            End If

            cmdTipologiaCF.Connection = cn
            Dim ReadCliFor As OleDbDataReader = cmdTipologiaCF.ExecuteReader()
            If ReadCliFor.Read() Then
                DT.CodiceDestinatario = campodb(ReadCliFor.Item("CodiceDESTINATARIO"))
                DT.CodiceFiscale = campodb(ReadCliFor.Item("CodiceFiscale"))
                DT.PartitaIVA = Format(campodbN(ReadCliFor.Item("PARTITAIVA")), "00000000000")

                If DT.CodiceDestinatario = "" Then
                    DT.CodiceDestinatario = "0000000"
                End If

                'If 999 241 SVIZZERA CH*

                DT.RagioneSociale = campodb(ReadCliFor.Item("Nome"))

                If DT.RagioneSociale = "GALIMBERTI MARISA" Then
                    DT.RagioneSociale = "GALIMBERTI MARISA"
                End If

                DT.Cognome = campodb(ReadCliFor.Item("CogNomeOspite"))
                DT.Nome = campodb(ReadCliFor.Item("NomeOspite"))


                DT.Indirizzo = campodb(ReadCliFor.Item("RESIDENZAINDIRIZZO1"))
                DT.Cap = campodb(ReadCliFor.Item("RESIDENZACAP1"))

                Dim ksCom As New ClsComune

                ksCom.Provincia = campodb(ReadCliFor.Item("RESIDENZAPROVINCIA1"))
                ksCom.Comune = campodb(ReadCliFor.Item("RESIDENZACOMUNE1"))
                ksCom.Leggi(Session("DC_OSPITE"))

                DT.CodiceCatastale = ksCom.CODXCODF


                If ksCom.Provincia = "999" And ksCom.Comune = "241" Then
                    If DT.CodiceDestinatario = "0000000" Then
                        DT.CodiceDestinatario = "XXXXXXX"
                        DT.Stato = "CH"
                    End If
                End If


                DT.Citta = ksCom.Descrizione
                DT.Provincia = ksCom.CodificaProvincia

                If ksCom.CodificaProvincia = "" Then
                    Dim ksProv As New ClsComune

                    ksProv.Provincia = campodb(ReadCliFor.Item("RESIDENZAPROVINCIA1"))
                    ksProv.Comune = ""
                    ksProv.Leggi(Session("DC_OSPITE"))

                    DT.Provincia = ksProv.CodificaProvincia
                End If



                If campodb(ReadCliFor.Item("FisicaGiuridica")) = "P" Then
                    DT.TipoAnagrafica = ""
                End If


                If Val(campodb(ReadCliFor.Item("CCBANCARIOCLIENTE"))) > 0 Then
                    DT.Iban = AdattaLunghezza(campodb(ReadCliFor.Item("IntCliente")), 2) & AdattaLunghezzaNumero(campodbN(ReadCliFor.Item("NumeroControlloCliente")), 2) & AdattaLunghezza(campodb(ReadCliFor.Item("CINCLIENTE")), 1) & AdattaLunghezzaNumero(campodb(ReadCliFor.Item("ABICLIENTE")), 5) & AdattaLunghezzaNumero(campodbN(ReadCliFor.Item("CABCLIENTE")), 5) & AdattaLunghezzaNumero(campodb(ReadCliFor.Item("CCBANCARIOCLIENTE")), 12)
                    DT.Banca = campodb(ReadCliFor.Item("BANCACLIENTE"))
                End If


                DT.Cup = campodb(ReadCliFor.Item("CodiceCup"))
                DT.Cig = campodb(ReadCliFor.Item("CodiceCig"))

                DT.IdDocumento = campodb(ReadCliFor.Item("IdDocumento"))
                DT.RifAmministrazione = campodb(ReadCliFor.Item("RiferimentoAmministrazione"))
                DT.RigaNote = campodb(ReadCliFor.Item("Note"))

                DT.Pec = campodb(ReadCliFor.Item("PEC"))

                DT.IdDocumento = campodb(ReadCliFor.Item("IdDocumento"))
                DT.RifAmministrazione = campodb(ReadCliFor.Item("RiferimentoAmministrazione"))
                DT.NumeroFatturaDDT = campodb(ReadCliFor.Item("NumeroFatturaDDT"))
                DT.NumItem = campodb(ReadCliFor.Item("NumItem"))
                DT.RigaNote = campodb(ReadCliFor.Item("Note"))
                DT.DescrizioneXML = campodb(ReadCliFor.Item("DescrizioneXML"))
                DT.CodiceCommessaConvezione = campodb(ReadCliFor.Item("CodiceCommessaConvezione"))
                If campodb(ReadCliFor.Item("IdDocumentoData")) <> "" Then
                    DT.IDDataDoc = campodb(ReadCliFor.Item("IdDocumentoData"))
                End If
                DT.TipoInvio = campodb(ReadCliFor.Item("EGo"))
                DT.Causale = campodb(ReadCliFor.Item("Causale"))


                'DT.CodiceDestinatario = campodb(ReadCliFor.Item("CodiceDestinatario"))
            End If
            ReadCliFor.Close()


        End If

        DT.NoteFattura = DT.NoteFattura & Registrazione.Descrizione.Replace(vbNewLine, "")
        Return DT

    End Function

    Private Sub CreateXML(ByVal Send As Boolean, ByVal RegistrazioneContabile As Integer)
        Dim XML As String = ""
        Dim Zipfile As New Ionic.Zip.ZipFile
        Dim MaxID As Integer = 0
        Dim DatiSocioeta As New Cls_TabellaSocieta

        Dim cn As OleDbConnection
        Dim cnOsp As OleDbConnection

        Dim Modifica As Boolean = False

        Dim Parametri As New Cls_Parametri
        Dim Totale As Double = 0


        cn = New Data.OleDb.OleDbConnection(Session("DC_GENERALE"))

        cn.Open()



        cnOsp = New Data.OleDb.OleDbConnection(Session("DC_OSPITE"))

        cnOsp.Open()


        DatiSocioeta.Leggi(Session("DC_TABELLE"))

        ' in dati società aggiungere
        ' Bollo Virtuale
        ' RegimeFiscale

        If DatiSocioeta.REGIMEFISCALE = "" Then
            ScriptManager.RegisterClientScriptBlock(Me, Me.GetType(), "Errore", "VisualizzaErrore('Indica regime fiscale');", True)
            Exit Sub
        End If




        Tabella = ViewState("Appoggio")


        For Riga = 0 To GridView1.Rows.Count - 1
            Dim CheckBox As CheckBox = DirectCast(GridView1.Rows(Riga).FindControl("ChkImporta"), CheckBox)
            Dim Chk_Bollo As CheckBox = DirectCast(GridView1.Rows(Riga).FindControl("ChkBollo"), CheckBox)



            Parametri.LeggiParametri(Session("DC_OSPITE"))


            If RegistrazioneContabile > 0 Then
                CheckBox.Checked = False
                If Tabella.Rows(Riga).Item(0) = RegistrazioneContabile Then
                    CheckBox.Checked = True
                End If
            End If

            If CheckBox.Checked = True Then

                MaxID = Val(campodb(Tabella.Rows(Riga).Item(1)))
                If MaxID = 0 Then
                    Dim cmd As New OleDbCommand
                    cmd.CommandText = "Select max(Progressivo) From FatturaElettronica"
                    cmd.Connection = cn
                    Dim sb As StringBuilder = New StringBuilder
                    Dim myPOSTreader As OleDbDataReader = cmd.ExecuteReader()
                    If myPOSTreader.Read Then
                        MaxID = campodbN(myPOSTreader.Item(0)) + 1
                    End If
                    myPOSTreader.Close()
                End If

                Dim Registrazione As New Cls_MovimentoContabile
                Dim CausaleContabile As New Cls_CausaleContabile

                Dim DI As New DatiIntestatario

                Registrazione.NumeroRegistrazione = Tabella.Rows(Riga).Item(0)
                Registrazione.Leggi(Session("DC_GENERALE"), Registrazione.NumeroRegistrazione)

                CausaleContabile.Codice = Registrazione.CausaleContabile
                CausaleContabile.Leggi(Session("DC_TABELLE"), CausaleContabile.Codice)

                DI = IntestatarioFattura(Tabella.Rows(Riga).Item(0))

                If Registrazione.CodiceCig.Trim <> "" Then
                    DI.Cig = Registrazione.CodiceCig
                End If


                If DI.DescrizioneXML.IndexOf("<PERSONOLIZZA>") >= 0 Then
                    If DI.DescrizioneXML.IndexOf("<FE_RaggruppaRicavi>") >= 0 Then
                        Parametri.FE_RaggruppaRicavi = 1
                    Else
                        Parametri.FE_RaggruppaRicavi = 0
                    End If

                    If DI.DescrizioneXML.IndexOf("<FE_IndicaMastroPartita>") >= 0 Then
                        Parametri.FE_IndicaMastroPartita = 1
                    Else
                        Parametri.FE_IndicaMastroPartita = 0
                    End If
                    If DI.DescrizioneXML.IndexOf("<FE_Competenza>") >= 0 Then
                        Parametri.FE_Competenza = 1
                    Else
                        Parametri.FE_Competenza = 0
                    End If
                    If DI.DescrizioneXML.IndexOf("<FE_CentroServizio>") >= 0 Then
                        Parametri.FE_CentroServizio = 1
                    Else
                        Parametri.FE_CentroServizio = 0
                    End If

                    If DI.DescrizioneXML.IndexOf("<FE_GiorniInDescrizione>") >= 0 Then
                        Parametri.FE_GiorniInDescrizione = 1
                    Else
                        Parametri.FE_GiorniInDescrizione = 0
                    End If
                    If DI.DescrizioneXML.IndexOf("<FE_TipoRetta>") >= 0 Then
                        Parametri.FE_TipoRetta = 1
                    Else
                        Parametri.FE_TipoRetta = 0
                    End If

                    If DI.DescrizioneXML.IndexOf("<FE_SoloIniziali>") >= 0 Then
                        Parametri.FE_SoloIniziali = 1
                    Else
                        Parametri.FE_SoloIniziali = 0
                    End If

                    If DI.DescrizioneXML.IndexOf("<FE_NoteFatture>") >= 0 Then
                        Parametri.FE_NoteFatture = 1
                    Else
                        Parametri.FE_NoteFatture = 0
                    End If


                    If DI.DescrizioneXML.IndexOf("<SoloDescrizioneRigaFattura>") >= 0 Then
                        Parametri.SoloDescrizioneRigaFattura = 1
                    Else
                        Parametri.SoloDescrizioneRigaFattura = 0
                    End If
                End If


                XML = "<?xml version=""1.0"" encoding=""UTF-8""?>" & vbNewLine
                If DI.TipoAnagrafica = "P" Then
                    XML = XML & "<p:FatturaElettronica versione=""FPR12"" xmlns:ds=""http://www.w3.org/2000/09/xmldsig#"" "
                Else
                    XML = XML & "<p:FatturaElettronica versione=""FPA12"" xmlns:ds=""http://www.w3.org/2000/09/xmldsig#"" "
                End If

                XML = XML & "xmlns:p=""http://ivaservizi.agenziaentrate.gov.it/docs/xsd/fatture/v1.2"" "
                XML = XML & "xmlns:xsi=""http://www.w3.org/2001/XMLSchema-instance"" "
                XML = XML & "xsi:schemaLocation=""http://ivaservizi.agenziaentrate.gov.it/docs/xsd/fatture/v1.2 http://www.fatturapa.gov.it/export/fatturazione/sdi/fatturapa/v1.2/Schema_del_file_xml_FatturaPA_versione_1.2.xsd"">" & vbNewLine
                XML = XML & "<FatturaElettronicaHeader>" & vbNewLine
                XML = XML & "<DatiTrasmissione>" & vbNewLine
                XML = XML & "<IdTrasmittente>" & vbNewLine
                XML = XML & "<IdPaese>IT</IdPaese>" & vbNewLine
                XML = XML & "<IdCodice>" & DatiSocioeta.CodiceFiscale & "</IdCodice>" & vbNewLine
                XML = XML & "</IdTrasmittente>" & vbNewLine
                XML = XML & "<ProgressivoInvio>" & MaxID & "</ProgressivoInvio>" & vbNewLine
                If DI.TipoAnagrafica = "P" Then
                    XML = XML & "<FormatoTrasmissione>FPR12</FormatoTrasmissione>" & vbNewLine
                Else
                    XML = XML & "<FormatoTrasmissione>FPA12</FormatoTrasmissione>" & vbNewLine
                End If
                XML = XML & "<CodiceDestinatario>" & Trim(DI.CodiceDestinatario) & "</CodiceDestinatario>" & vbNewLine
                XML = XML & "<ContattiTrasmittente>" & vbNewLine
                If DatiSocioeta.Telefono.Trim <> "" Then
                    XML = XML & "<Telefono>" & DatiSocioeta.Telefono & "</Telefono>" & vbNewLine
                End If
                If DatiSocioeta.EMail.Trim <> "" Then
                    XML = XML & "<Email>" & DatiSocioeta.EMail & "</Email>" & vbNewLine
                End If
                XML = XML & "</ContattiTrasmittente>" & vbNewLine

                If DI.Pec <> "" Then
                    XML = XML & "<PECDestinatario>" & DI.Pec & "</PECDestinatario>" & vbNewLine
                End If
                XML = XML & "</DatiTrasmissione>" & vbNewLine

                XML = XML & "<CedentePrestatore>" & vbNewLine
                XML = XML & "<DatiAnagrafici>" & vbNewLine
                XML = XML & "<IdFiscaleIVA>" & vbNewLine
                XML = XML & "<IdPaese>IT</IdPaese>" & vbNewLine
                XML = XML & "<IdCodice>" & Format(Val(DatiSocioeta.PartitaIVA), "00000000000") & "</IdCodice>" & vbNewLine
                XML = XML & "</IdFiscaleIVA>" & vbNewLine
                XML = XML & "<Anagrafica>" & vbNewLine
                XML = XML & "<Denominazione>" & DatiSocioeta.RagioneSociale & "</Denominazione>" & vbNewLine
                XML = XML & "</Anagrafica>" & vbNewLine
                XML = XML & "<RegimeFiscale>" & DatiSocioeta.REGIMEFISCALE & "</RegimeFiscale>" & vbNewLine
                XML = XML & "</DatiAnagrafici>" & vbNewLine
                XML = XML & "<Sede>" & vbNewLine
                XML = XML & "<Indirizzo>" & DatiSocioeta.Indirizzo & "</Indirizzo>" & vbNewLine
                XML = XML & "<CAP>" & Format(Val(DatiSocioeta.Cap), "00000") & "</CAP>" & vbNewLine
                XML = XML & "<Comune>" & DatiSocioeta.Localita & "</Comune>" & vbNewLine
                XML = XML & "<Provincia>" & DatiSocioeta.Provincia & "</Provincia>" & vbNewLine
                XML = XML & "<Nazione>IT</Nazione>" & vbNewLine
                XML = XML & "</Sede>" & vbNewLine
                If DI.RifAmministrazione <> "" Then
                    XML = XML & "<RiferimentoAmministrazione>" & DI.RifAmministrazione & "</RiferimentoAmministrazione>" & vbNewLine
                End If

                XML = XML & "</CedentePrestatore>" & vbNewLine

                XML = XML & "<CessionarioCommittente>" & vbNewLine
                XML = XML & "<DatiAnagrafici>" & vbNewLine

                If DI.PartitaIVA > 0 Then
                    XML = XML & "<IdFiscaleIVA>" & vbNewLine
                    XML = XML & "<IdPaese>IT</IdPaese>" & vbNewLine
                    XML = XML & "<IdCodice>" & DI.PartitaIVA & "</IdCodice>" & vbNewLine
                    XML = XML & "</IdFiscaleIVA>" & vbNewLine
                Else
                    XML = XML & "<CodiceFiscale>" & DI.CodiceFiscale & "</CodiceFiscale>" & vbNewLine
                End If
                XML = XML & "<Anagrafica>" & vbNewLine
                If DI.Cognome <> "" And DI.Nome <> "" Then
                    XML = XML & "<Nome>" & DI.Nome & "</Nome>" & vbNewLine
                    XML = XML & "<Cognome>" & DI.Cognome & "</Cognome>" & vbNewLine
                Else
                    XML = XML & "<Denominazione>" & DI.RagioneSociale & "</Denominazione>" & vbNewLine
                End If
                XML = XML & "</Anagrafica>" & vbNewLine
                XML = XML & "</DatiAnagrafici>" & vbNewLine
                XML = XML & "<Sede>" & vbNewLine
                XML = XML & "<Indirizzo>" & DI.Indirizzo & "</Indirizzo>" & vbNewLine
                XML = XML & "<CAP>" & DI.Cap & "</CAP>" & vbNewLine
                XML = XML & "<Comune>" & DI.Citta & "</Comune>" & vbNewLine
                If DI.Stato = "IT" Then
                    XML = XML & "<Provincia>" & DI.Provincia & "</Provincia>" & vbNewLine
                End If
                XML = XML & "<Nazione>" & DI.Stato & "</Nazione>" & vbNewLine
                XML = XML & "</Sede>" & vbNewLine
                XML = XML & "</CessionarioCommittente>" & vbNewLine
                REM XML = XML & "<SoggettoEmittente>CC</SoggettoEmittente>" & vbNewLine
                XML = XML & "</FatturaElettronicaHeader>" & vbNewLine

                XML = XML & "<FatturaElettronicaBody>" & vbNewLine
                XML = XML & "<DatiGenerali>" & vbNewLine
                XML = XML & "<DatiGeneraliDocumento>" & vbNewLine

                If CausaleContabile.TipoDocumento = "NC" Then
                    XML = XML & "<TipoDocumento>TD04</TipoDocumento>" & vbNewLine
                Else
                    If CausaleContabile.TipoDocumento = "NB" Then
                        XML = XML & "<TipoDocumento>TD05</TipoDocumento>" & vbNewLine
                    Else
                        XML = XML & "<TipoDocumento>TD01</TipoDocumento>" & vbNewLine
                    End If

                End If
                XML = XML & "<Divisa>EUR</Divisa>" & vbNewLine
                XML = XML & "<Data>" & Format(Registrazione.DataDocumento, "yyyy-MM-dd") & "</Data>" & vbNewLine

                Dim RegistroIVA As New Cls_RegistroIVA

                RegistroIVA.Leggi(Session("DC_TABELLE"), Registrazione.RegistroIVA)



                If RegistroIVA.IndicatoreRegistro.Trim = "" Then
                    XML = XML & "<Numero>" & Registrazione.NumeroProtocollo & "</Numero>" & vbNewLine
                Else
                    If Parametri.XMLSEZIONALEPRIMAPROTOCOLLO = 1 Then
                        XML = XML & "<Numero>" & RegistroIVA.IndicatoreRegistro & "/" & Registrazione.NumeroProtocollo & "</Numero>" & vbNewLine
                    Else
                        XML = XML & "<Numero>" & Registrazione.NumeroProtocollo & "/" & RegistroIVA.IndicatoreRegistro & "</Numero>" & vbNewLine
                    End If

                End If

                'Dim kInd As Integer
                'Dim TrovaBollo As Boolean = False
                'For kInd = 0 To 100
                '    If Not IsNothing(Registrazione.Righe(kInd)) Then
                '        If Registrazione.Righe(kInd).RigaDaCausale = 9 Then
                '            TrovaBollo = True
                '        End If
                '    End If
                'Next


                ''And TrovaBollo = True
                'Dim TipoRegistroXBollo As New Cls_RegistroIVA

                'TipoRegistroXBollo.Tipo = Registrazione.RegistroIVA
                'TipoRegistroXBollo.Leggi(Session("DC_TABELLE"), TipoRegistroXBollo.Tipo)


                'If DI.SoggettoABollo = "N" Then
                '    TipoRegistroXBollo.RegistroCartaceo = 1
                'End If


                'If TipoRegistroXBollo.RegistroCartaceo = 0 Then
                '    Dim DatiBollo As New Cls_bolli

                '    DatiBollo.Leggi(Session("DC_TABELLE"), Registrazione.DataRegistrazione)

                '    Dim VerificaIVA As Boolean = True
                '    Dim CicloIndice As Integer

                '    For CicloIndice = 0 To 300
                '        If Not IsNothing(Registrazione.Righe(CicloIndice)) Then
                '            If Registrazione.Righe(CicloIndice).Importo > 0 And Registrazione.Righe(CicloIndice).Tipo = "IV" And TrovaBollo = False Then
                '                VerificaIVA = False
                '            End If
                '        End If
                '    Next


                '    If DatiSocioeta.BOLLOVIRTUALE > 0 And Math.Abs(Registrazione.ImportoDocumento(Session("DC_TABELLE"))) > DatiBollo.ImportoApplicazione And VerificaIVA Then
                '        XML = XML & "<DatiBollo>" & vbNewLine
                '        XML = XML & "<BolloVirtuale>SI</BolloVirtuale>" & vbNewLine
                '        XML = XML & "<ImportoBollo>2.00</ImportoBollo>" & vbNewLine
                '        XML = XML & "</DatiBollo>" & vbNewLine
                '    End If
                'End If

                If Chk_Bollo.Checked = True Then
                    XML = XML & "<DatiBollo>" & vbNewLine
                    XML = XML & "<BolloVirtuale>SI</BolloVirtuale>" & vbNewLine
                    XML = XML & "<ImportoBollo>2.00</ImportoBollo>" & vbNewLine
                    XML = XML & "</DatiBollo>" & vbNewLine
                End If


                XML = XML & "<ImportoTotaleDocumento>" & Replace(Format(Math.Round(Math.Abs(Registrazione.ImportoDocumento(Session("DC_TABELLE"))), 2), "0.00"), ",", ".") & "</ImportoTotaleDocumento>" & vbNewLine

                If DI.Causale <> "" Then
                    XML = XML & "<Causale>" & DI.Causale & "</Causale>" & vbNewLine
                End If

                XML = XML & "</DatiGeneraliDocumento>" & vbNewLine


                If DI.TipoInvio <> "N" Then
                    If DI.TipoInvio = "O" Then
                        XML = XML & "<DatiOrdineAcquisto>" & vbNewLine
                    End If
                    If DI.TipoInvio = "C" Then
                        XML = XML & "<DatiContratto>" & vbNewLine
                    End If
                    If DI.TipoInvio = "Z" Then
                        XML = XML & "<DatiConvenzione>" & vbNewLine
                    End If

                    If DI.IdDocumento.Trim <> "" Then
                        XML = XML & "<IdDocumento>" & DI.IdDocumento & "</IdDocumento>" & vbNewLine
                        If DI.IDDataDoc = "" Then
                            XML = XML & "<Data>" & Format(Registrazione.DataDocumento, "yyyy-MM-dd") & "</Data>" & vbNewLine
                        Else
                            Dim DataOrdine As Date = Registrazione.DataRegistrazione
                            Try
                                DataOrdine = DI.IDDataDoc
                            Catch ex As Exception

                            End Try
                            XML = XML & "<Data>" & Format(DataOrdine, "yyyy-MM-dd") & "</Data>" & vbNewLine
                        End If
                    Else
                        If DI.TipoInvio = "O" Or DI.TipoInvio = "C" Or DI.TipoInvio = "Z" Then

                            If RegistroIVA.IndicatoreRegistro.Trim = "" Then
                                XML = XML & "<IdDocumento>" & Registrazione.NumeroProtocollo & "</IdDocumento>" & vbNewLine
                            Else
                                If Parametri.XMLSEZIONALEPRIMAPROTOCOLLO = 1 Then
                                    XML = XML & "<IdDocumento>" & RegistroIVA.IndicatoreRegistro & "/" & Registrazione.NumeroProtocollo & "</IdDocumento>" & vbNewLine
                                Else
                                    XML = XML & "<IdDocumento>" & Registrazione.NumeroProtocollo & "/" & RegistroIVA.IndicatoreRegistro & "</IdDocumento>" & vbNewLine
                                End If
                            End If
                            If DI.IDDataDoc = "" Then
                                XML = XML & "<Data>" & Format(Registrazione.DataDocumento, "yyyy-MM-dd") & "</Data>" & vbNewLine
                            Else
                                Dim DataOrdine As Date = Registrazione.DataRegistrazione
                                Try
                                    DataOrdine = DI.IDDataDoc
                                Catch ex As Exception

                                End Try
                                XML = XML & "<Data>" & Format(DataOrdine, "yyyy-MM-dd") & "</Data>" & vbNewLine
                            End If

                        End If
                    End If


                    If DI.Cup <> "" Then
                        XML = XML & "<CodiceCUP>" & DI.Cup & "</CodiceCUP>" & vbNewLine
                    End If
                    If DI.Cig <> "" And DI.Cig <> "0" Then
                        XML = XML & "<CodiceCIG>" & DI.Cig & "</CodiceCIG>" & vbNewLine
                    End If

                    If DI.NumItem <> "" Then
                        XML = XML & "<NumItem>" & DI.NumItem & "</NumItem>" & vbNewLine
                    End If

                    If DI.CodiceCommessaConvezione <> "" Then
                        XML = XML & "<CodiceCommessaConvenzione>" & DI.CodiceCommessaConvezione & "</CodiceCommessaConvenzione>" & vbNewLine
                    End If

                    If DI.TipoInvio = "Z" Then
                        XML = XML & "</DatiConvenzione>" & vbNewLine
                    End If
                    If DI.TipoInvio = "C" Then
                        XML = XML & "</DatiContratto>" & vbNewLine
                    End If
                    If DI.TipoInvio = "O" Then
                        XML = XML & "</DatiOrdineAcquisto>" & vbNewLine
                    End If
                End If

                If Val(DI.NumeroFatturaDDT) = 1 Then
                    XML = XML & "<DatiDDT>" & vbNewLine
                    If RegistroIVA.IndicatoreRegistro.Trim = "" Then
                        XML = XML & "<NumeroDDT>" & Registrazione.NumeroProtocollo & "</NumeroDDT>" & vbNewLine
                    Else
                        XML = XML & "<NumeroDDT>" & Registrazione.NumeroProtocollo & "/" & RegistroIVA.IndicatoreRegistro & "</NumeroDDT>" & vbNewLine
                    End If

                    XML = XML & "<DataDDT>" & Format(Registrazione.DataDocumento, "yyyy-MM-dd") & "</DataDDT>"
                    XML = XML & "</DatiDDT>" & vbNewLine
                End If

                XML = XML & "<DatiTrasporto>" & vbNewLine
                XML = XML & "</DatiTrasporto>" & vbNewLine
                XML = XML & "</DatiGenerali>" & vbNewLine
                XML = XML & "<DatiBeniServizi>" & vbNewLine

                Dim Indice As Integer = 0
                Dim MySql As String = ""
                Dim RagruppaRicavi As Integer = 0
                Dim ULTIMAALIQUOTA As Double = 0
                Dim ULTIMANATURA As String = ""
                Dim Scorporo As Boolean = False



                Indice = 0
                For Indice = 0 To 300
                    If Not IsNothing(Registrazione.Righe(Indice)) Then

                        Dim MyIva As New Cls_IVA

                        If Registrazione.Righe(Indice).Tipo = "IV" Then

                            MyIva.Leggi(Session("DC_TABELLE"), Registrazione.Righe(Indice).CodiceIVA)

                            ULTIMAALIQUOTA = MyIva.Aliquota
                            ULTIMANATURA = MyIva.Natura
                        End If

                        If Not IsNothing(Registrazione.Righe(Indice).Descrizione) Then
                            If Registrazione.Righe(Indice).Descrizione.ToUpper.IndexOf("Scorporo IVA".ToUpper) Then
                                Scorporo = True
                            End If
                        End If

                    End If

                Next

                Totale = 0
                Indice = 0

                If Parametri.FE_NoteFatture = 1 Then
                    Dim Appoggio As String = ""

                    Dim AppoggioCentroServizio As String = ""

                    If Parametri.FE_Competenza = 1 Then
                        Indice = Indice + 1

                        XML = XML & "<DettaglioLinee>" & vbNewLine
                        XML = XML & "<NumeroLinea>" & Indice & "</NumeroLinea>" & vbNewLine
                        XML = XML & "<Descrizione>Competenza : " & Registrazione.AnnoCompetenza & "/" & DecodificaMese(Registrazione.MeseCompetenza) & "</Descrizione>"
                        XML = XML & "<Quantita>0.00</Quantita>"
                        XML = XML & "<PrezzoUnitario>0.00</PrezzoUnitario>"
                        XML = XML & "<PrezzoTotale>0.00</PrezzoTotale>"
                        XML = XML & "<AliquotaIVA>" & Replace(Format(ULTIMAALIQUOTA * 100, "0.00"), ",", ".") & "</AliquotaIVA>" & vbNewLine

                        If ULTIMANATURA <> "" Then
                            XML = XML & "<Natura>" & ULTIMANATURA & "</Natura>" & vbNewLine
                        End If

                        XML = XML & "</DettaglioLinee>"
                    End If

                    If Parametri.FE_CentroServizio = 1 Then
                        Indice = Indice + 1

                        Dim AppoServ As New Cls_CentroServizio

                        AppoServ.Leggi(Session("DC_OSPITE"), Registrazione.CentroServizio)

                        XML = XML & "<DettaglioLinee>" & vbNewLine
                        XML = XML & "<NumeroLinea>" & Indice & "</NumeroLinea>" & vbNewLine
                        XML = XML & "<Descrizione>Servizio : " & AppoServ.DESCRIZIONE & "</Descrizione>"
                        XML = XML & "<Quantita>0.00</Quantita>"
                        XML = XML & "<PrezzoUnitario>0.00</PrezzoUnitario>"
                        XML = XML & "<PrezzoTotale>0.00</PrezzoTotale>"
                        XML = XML & "<AliquotaIVA>" & Replace(Format(ULTIMAALIQUOTA * 100, "0.00"), ",", ".") & "</AliquotaIVA>" & vbNewLine

                        If ULTIMANATURA <> "" Then
                            XML = XML & "<Natura>" & ULTIMANATURA & "</Natura>" & vbNewLine
                        End If
                        XML = XML & "</DettaglioLinee>"
                    End If


                    If DI.NoteFattura <> "" Then
                        Appoggio = DI.NoteFattura.ToString.Replace(vbNewLine, "")
                    End If
                    Dim LineaDes As String = 0
                    Dim I1 As Integer
                    Dim N1 As Integer = 0
                    LineaDes = ""
                    For I1 = 1 To Appoggio.Length - 1
                        If (Mid(Appoggio, I1, 1) = " " And N1 > 65) Or Mid(Appoggio, I1, 1) = Chr(254) Then
                            If LineaDes.Trim <> "" Then
                                Indice = Indice + 1

                                If Mid(Appoggio, I1, 1) <> Chr(254) Then
                                    LineaDes = LineaDes & Mid(Appoggio, I1, 1)
                                End If

                                XML = XML & "<DettaglioLinee>" & vbNewLine
                                XML = XML & "<NumeroLinea>" & Indice & "</NumeroLinea>" & vbNewLine
                                XML = XML & "<Descrizione>" & LineaDes & "</Descrizione>"
                                XML = XML & "<Quantita>0.00</Quantita>"
                                XML = XML & "<PrezzoUnitario>0.00</PrezzoUnitario>"
                                XML = XML & "<PrezzoTotale>0.00</PrezzoTotale>"
                                XML = XML & "<AliquotaIVA>" & Replace(Format(ULTIMAALIQUOTA * 100, "0.00"), ",", ".") & "</AliquotaIVA>" & vbNewLine

                                If ULTIMANATURA <> "" Then
                                    XML = XML & "<Natura>" & ULTIMANATURA & "</Natura>" & vbNewLine
                                End If

                                XML = XML & "</DettaglioLinee>"

                                LineaDes = ""
                                N1 = 0
                            End If
                        Else
                            N1 = N1 + 1
                            LineaDes = LineaDes & Mid(Appoggio, I1, 1)
                        End If
                    Next
                    If LineaDes.Trim <> "" Then
                        Indice = Indice + 1
                        LineaDes = LineaDes & Mid(Appoggio, I1, 1)
                        XML = XML & "<DettaglioLinee>" & vbNewLine
                        XML = XML & "<NumeroLinea>" & Indice & "</NumeroLinea>" & vbNewLine
                        XML = XML & "<Descrizione>" & LineaDes & "</Descrizione>"
                        XML = XML & "<Quantita>0.00</Quantita>"
                        XML = XML & "<PrezzoUnitario>0.00</PrezzoUnitario>"
                        XML = XML & "<PrezzoTotale>0.00</PrezzoTotale>"
                        XML = XML & "<AliquotaIVA>" & Replace(Format(ULTIMAALIQUOTA * 100, "0.00"), ",", ".") & "</AliquotaIVA>" & vbNewLine

                        If ULTIMANATURA <> "" Then
                            XML = XML & "<Natura>" & ULTIMANATURA & "</Natura>" & vbNewLine
                        End If

                        XML = XML & "</DettaglioLinee>"
                    End If
                End If




                Indice = 0

                If Scorporo Then
                    If CausaleContabile.Tipo = "R" Then
                        MySql = "Select * From MovimentiContabiliRiga Where Numero = " & Registrazione.NumeroRegistrazione & " And (RigaDaCausale =3 or RigaDaCausale =4 or RigaDaCausale =5 or RigaDaCausale =6 or RigaDaCausale =9 or RigaDaCausale =12) ORDER BY Descrizione,(SELECT TOP 1 dESCRIZIONE FROM  PIANOCONTI WHERE MASTRO = MASTROCONTROPARTITA AND CONTO = CONTOCONTROPARTITA AND SOTTOCONTO = SOTTOCONTOCONTROPARTITA)"
                    Else
                        MySql = "Select * From MovimentiContabiliRiga Where Numero = " & Registrazione.NumeroRegistrazione & " And (RigaDaCausale =2 or RigaDaCausale =9 ) ORDER BY Descrizione,(SELECT TOP 1 dESCRIZIONE FROM  PIANOCONTI WHERE MASTRO = MASTROCONTROPARTITA AND CONTO = CONTOCONTROPARTITA AND SOTTOCONTO = SOTTOCONTOCONTROPARTITA)"
                    End If
                Else
                    If CausaleContabile.Tipo = "R" Then
                        MySql = "Select * From MovimentiContabiliRiga Where Numero = " & Registrazione.NumeroRegistrazione & " And (RigaDaCausale =3 or RigaDaCausale =4 or RigaDaCausale =5 or RigaDaCausale =6 or RigaDaCausale =9 or RigaDaCausale =12) ORDER BY (SELECT TOP 1 dESCRIZIONE FROM  PIANOCONTI WHERE MASTRO = MASTROCONTROPARTITA AND CONTO = CONTOCONTROPARTITA AND SOTTOCONTO = SOTTOCONTOCONTROPARTITA)"
                    Else
                        MySql = "Select * From MovimentiContabiliRiga Where Numero = " & Registrazione.NumeroRegistrazione & " And (RigaDaCausale =2 or RigaDaCausale =9 ) ORDER BY (SELECT TOP 1 dESCRIZIONE FROM  PIANOCONTI WHERE MASTRO = MASTROCONTROPARTITA AND CONTO = CONTOCONTROPARTITA AND SOTTOCONTO = SOTTOCONTOCONTROPARTITA)"
                    End If
                End If

                Dim cmdRicavi As New OleDbCommand

                cmdRicavi.CommandText = MySql
                cmdRicavi.Connection = cn
                Dim ReadRicavi As OleDbDataReader = cmdRicavi.ExecuteReader()
                Do While ReadRicavi.Read()


                    If Parametri.FE_RaggruppaRicavi = 0 Then
                        Indice = Indice + 1

                        XML = XML & "<DettaglioLinee>" & vbNewLine
                        XML = XML & "<NumeroLinea>" & Indice & "</NumeroLinea>" & vbNewLine

                        Dim DecPianoConti As New Cls_Pianodeiconti

                        DecPianoConti.Mastro = campodbN(ReadRicavi.Item("MastroContropartita"))
                        DecPianoConti.Conto = campodbN(ReadRicavi.Item("ContoContropartita"))
                        DecPianoConti.Sottoconto = campodbN(ReadRicavi.Item("SottocontoContropartita"))
                        DecPianoConti.Decodfica(Session("DC_GENERALE"))

                        If DecPianoConti.Descrizione = "" Then
                            DecPianoConti.Mastro = campodbN(ReadRicavi.Item("Mastropartita"))
                            DecPianoConti.Conto = campodbN(ReadRicavi.Item("ContoPartita"))
                            DecPianoConti.Sottoconto = campodbN(ReadRicavi.Item("Sottocontopartita"))
                            DecPianoConti.Decodfica(Session("DC_GENERALE"))

                        End If

                        Dim MyIva As New Cls_IVA

                        MyIva.Leggi(Session("DC_TABELLE"), campodb(ReadRicavi.Item("CodiceIva")))

                        ULTIMAALIQUOTA = MyIva.Aliquota
                        ULTIMANATURA = MyIva.Natura


                        Dim AppoggioDescrizione As String = ""
                        If DI.RigaNote <> "" Then
                            AppoggioDescrizione = " " & DI.RigaNote
                        End If
                        If Parametri.FE_IndicaMastroPartita = 1 And MyIva.Natura <> "N1" Then
                            Dim DecPianoContiPartita As New Cls_Pianodeiconti

                            DecPianoContiPartita.Mastro = campodbN(ReadRicavi.Item("MastroContropartita"))
                            DecPianoContiPartita.Conto = campodbN(ReadRicavi.Item("ContoContropartita"))
                            DecPianoContiPartita.Sottoconto = campodbN(ReadRicavi.Item("SottocontoContropartita"))
                            DecPianoContiPartita.Decodfica(Session("DC_GENERALE"))
                            AppoggioDescrizione = AppoggioDescrizione & " " & DecPianoContiPartita.Descrizione
                        End If
                        If Parametri.FE_Competenza = 1 And MyIva.Natura <> "N1" Then
                            AppoggioDescrizione = AppoggioDescrizione & " competenza : " & Registrazione.AnnoCompetenza & "/" & Registrazione.MeseCompetenza
                        End If
                        If Parametri.FE_CentroServizio = 1 And MyIva.Natura <> "N1" Then
                            Dim AppoServ As New Cls_CentroServizio

                            AppoServ.Leggi(Session("DC_OSPITE"), Registrazione.CentroServizio)
                            AppoggioDescrizione = AppoggioDescrizione & " " & AppoServ.DESCRIZIONE
                        End If


                        If Parametri.FE_GiorniInDescrizione = 1 Then
                            If campodbN(ReadRicavi.Item("Quantita")) > 0 Then
                                AppoggioDescrizione = AppoggioDescrizione & " Giorni " & campodbN(ReadRicavi.Item("Quantita"))
                            End If
                        End If

                        If Parametri.FE_TipoRetta = 1 Then
                            If campodbN(ReadRicavi.Item("Quantita")) > 0 Then
                                Dim K As New Cls_rettatotale

                                K.CODICEOSPITE = Int(campodbN(ReadRicavi.Item("SottocontoContropartita")) / 100)
                                K.UltimaData(Session("DC_OSPITE"), K.CODICEOSPITE, Registrazione.CentroServizio)

                                If K.TipoRetta <> "" Then
                                    Dim MyTipoRetta As New Cls_TipoRetta

                                    MyTipoRetta.Codice = K.TipoRetta
                                    MyTipoRetta.Leggi(Session("DC_OSPITE"), MyTipoRetta.Codice)

                                    AppoggioDescrizione = AppoggioDescrizione & " " & MyTipoRetta.Descrizione
                                End If
                            End If
                        End If

                        Dim Appoggio As String = ""


                        If Parametri.FE_SoloIniziali = 0 Then
                            If MyIva.Natura = "N1" Or CausaleContabile.Tipo = "I" Then
                                If campodb(ReadRicavi.Item("Descrizione")) = "" Then
                                    XML = XML & "<Descrizione>" & DecPianoConti.Descrizione & "</Descrizione>" & vbNewLine
                                Else
                                    XML = XML & "<Descrizione>" & campodb(ReadRicavi.Item("Descrizione")) & "</Descrizione>" & vbNewLine
                                End If
                            Else
                                If Parametri.SoloDescrizioneRigaFattura = 0 Then
                                    XML = XML & "<Descrizione>" & campodb(ReadRicavi.Item("Descrizione")) & Appoggio & AppoggioDescrizione & " " & DecPianoConti.Descrizione & "</Descrizione>" & vbNewLine
                                Else
                                    XML = XML & "<Descrizione>" & campodb(ReadRicavi.Item("Descrizione")) & "</Descrizione>" & vbNewLine
                                End If
                            End If
                        Else
                            Dim Cognome As String
                            Dim Nome As String

                            Dim Ospite As New ClsOspite

                            Ospite.CodiceOspite = Int(campodbN(ReadRicavi.Item("SottocontoContropartita")) / 100)
                            Ospite.Leggi(Session("DC_OSPITE"), Ospite.CodiceOspite)


                            Cognome = Trim(Ospite.CognomeOspite)
                            Nome = Trim(Ospite.NomeOspite)

                            Dim MyIvx As New Cls_IVA

                            MyIvx.Leggi(Session("DC_TABELLE"), campodb(ReadRicavi.Item("CodiceIva")))

                            ULTIMAALIQUOTA = MyIvx.Aliquota
                            ULTIMANATURA = MyIvx.Natura



                            If MyIva.Natura = "N1" Then
                                If campodb(ReadRicavi.Item("Descrizione")) = "" Then
                                    Dim DecPianoContiP As New Cls_Pianodeiconti

                                    DecPianoContiP.Mastro = campodbN(ReadRicavi.Item("Mastropartita"))
                                    DecPianoContiP.Conto = campodbN(ReadRicavi.Item("Contopartita"))
                                    DecPianoContiP.Sottoconto = campodbN(ReadRicavi.Item("Sottocontopartita"))
                                    DecPianoContiP.Decodfica(Session("DC_GENERALE"))


                                    XML = XML & "<Descrizione>" & DecPianoContiP.Descrizione & "</Descrizione>" & vbNewLine
                                Else
                                    XML = XML & "<Descrizione>" & campodb(ReadRicavi.Item("Descrizione")) & "</Descrizione>" & vbNewLine
                                End If
                            Else

                                XML = XML & "<Descrizione>" & campodb(ReadRicavi.Item("Descrizione")) & Appoggio & AppoggioDescrizione & " " & Mid(Cognome & Space(4), 1, 1) & ". " & Mid(Nome & Space(4), 1, 1) & ". " & "</Descrizione>" & vbNewLine

                            End If

                        End If




                        Dim negativo As String = ""
                        If CausaleContabile.TipoDocumento = "NC" Then
                            If campodb(ReadRicavi.Item("DareAvere")) = "A" Then
                                negativo = "-"
                            End If
                        Else
                            If campodb(ReadRicavi.Item("DareAvere")) = "D" Then
                                negativo = "-"
                            End If
                        End If

                        Dim IndicaQuantita As Double


                        IndicaQuantita = Modulo.MathRound(campodbN(ReadRicavi.Item("Importo")) / campodbN(ReadRicavi.Item("Quantita")), 2)

                        If Modulo.MathRound(IndicaQuantita * campodbN(ReadRicavi.Item("Quantita")), 2) = Modulo.MathRound(campodbN(ReadRicavi.Item("Importo")), 2) Then

                            XML = XML & "<Quantita>" & Replace(Format(Math.Round(campodbN(ReadRicavi.Item("Quantita")), 2), "0.00"), ",", ".") & "</Quantita>" & vbNewLine

                            XML = XML & "<PrezzoUnitario>" & negativo & Replace(Format(Math.Round(IndicaQuantita, 2), "0.00"), ",", ".") & "</PrezzoUnitario>" & vbNewLine

                            XML = XML & "<PrezzoTotale>" & negativo & Replace(Format(Math.Round(campodbN(ReadRicavi.Item("Importo")), 2), "0.00"), ",", ".") & "</PrezzoTotale>" & vbNewLine
                        Else

                            XML = XML & "<Quantita>1.00</Quantita>" & vbNewLine

                            XML = XML & "<PrezzoUnitario>" & negativo & Replace(Format(Math.Round(campodbN(ReadRicavi.Item("Importo")), 2), "0.00"), ",", ".") & "</PrezzoUnitario>" & vbNewLine

                            XML = XML & "<PrezzoTotale>" & negativo & Replace(Format(Math.Round(campodbN(ReadRicavi.Item("Importo")), 2), "0.00"), ",", ".") & "</PrezzoTotale>" & vbNewLine
                        End If

                        XML = XML & "<AliquotaIVA>" & Replace(Format(MyIva.Aliquota * 100, "0.00"), ",", ".") & "</AliquotaIVA>" & vbNewLine
                        'End If
                        If MyIva.Natura <> "" Then
                            XML = XML & "<Natura>" & MyIva.Natura & "</Natura>" & vbNewLine
                        End If

                        XML = XML & "</DettaglioLinee>" & vbNewLine
                    End If
                Loop
                ReadRicavi.Close()



                If Parametri.FE_RaggruppaRicavi = 1 Then

                    If CausaleContabile.Tipo = "R" Then
                        MySql = "Select mastropartita,contopartita,sottocontopartita,dareavere,codiceIVA,sum(importo) as timp From MovimentiContabiliRiga Where Numero = " & Registrazione.NumeroRegistrazione & " And (RigaDaCausale =3 or RigaDaCausale =4 or RigaDaCausale =5 or RigaDaCausale =6 or RigaDaCausale =9 or RigaDaCausale =12) Group by mastropartita,contopartita,sottocontopartita,dareavere,codiceIVA"
                    Else
                        MySql = "Select mastropartita,contopartita,sottocontopartita,dareavere,codiceIVA,sum(importo) as timp From MovimentiContabiliRiga Where Numero = " & Registrazione.NumeroRegistrazione & " And (RigaDaCausale =2 or RigaDaCausale =9 )  Group by mastropartita,contopartita,sottocontopartita,dareavere,codiceIVA"
                    End If
                    Dim cmdRicS As New OleDbCommand

                    cmdRicS.CommandText = MySql
                    cmdRicS.Connection = cn
                    Dim ReadRiS As OleDbDataReader = cmdRicS.ExecuteReader()
                    Do While ReadRiS.Read()
                        Indice = Indice + 1

                        XML = XML & "<DettaglioLinee>" & vbNewLine
                        XML = XML & "<NumeroLinea>" & Indice & "</NumeroLinea>" & vbNewLine

                        Dim DecPianoConti As New Cls_Pianodeiconti

                        DecPianoConti.Mastro = campodbN(ReadRiS.Item("mastropartita"))
                        DecPianoConti.Conto = campodbN(ReadRiS.Item("contopartita"))
                        DecPianoConti.Sottoconto = campodbN(ReadRiS.Item("sottocontopartita"))
                        DecPianoConti.Decodfica(Session("DC_GENERALE"))

                        XML = XML & "<Descrizione>" & DecPianoConti.Descrizione & "</Descrizione>" & vbNewLine


                        XML = XML & "<Quantita>1.00</Quantita>" & vbNewLine

                        Dim MyIva As New Cls_IVA

                        MyIva.Leggi(Session("DC_TABELLE"), campodb(ReadRiS.Item("CodiceIva")))

                        ULTIMAALIQUOTA = MyIva.Aliquota
                        ULTIMANATURA = MyIva.Natura


                        Dim negativo As String = ""
                        If CausaleContabile.TipoDocumento = "NC" Then
                            If campodb(ReadRiS.Item("DareAvere")) = "A" Then
                                negativo = "-"
                            End If
                        Else
                            If campodb(ReadRiS.Item("DareAvere")) = "D" Then
                                negativo = "-"
                            End If
                        End If

                        XML = XML & "<PrezzoUnitario>" & negativo & Replace(Format(Math.Round(campodbN(ReadRiS.Item("timp")), 2), "0.00"), ",", ".") & "</PrezzoUnitario>" & vbNewLine

                        XML = XML & "<PrezzoTotale>" & negativo & Replace(Format(Math.Round(campodbN(ReadRiS.Item("timp")), 2), "0.00"), ",", ".") & "</PrezzoTotale>" & vbNewLine


                        XML = XML & "<AliquotaIVA>" & Replace(Format(MyIva.Aliquota * 100, "0.00"), ",", ".") & "</AliquotaIVA>" & vbNewLine
                        'End If
                        If MyIva.Natura <> "" Then
                            XML = XML & "<Natura>" & MyIva.Natura & "</Natura>" & vbNewLine
                        End If

                        XML = XML & "</DettaglioLinee>" & vbNewLine
                    Loop
                    ReadRiS.Close()
                End If



                Totale = 0

                If CausaleContabile.Tipo = "R" Then
                    MySql = "Select * From MovimentiContabiliRiga Where Numero = " & Registrazione.NumeroRegistrazione & " And RigaDaCausale =2 "
                Else
                    MySql = "Select * From MovimentiContabiliRiga Where Numero = " & Registrazione.NumeroRegistrazione & " And RigaDaCausale = 3 "
                End If

                Dim In4 As Integer = 0

                Dim cmd1 As New OleDbCommand

                cmd1.CommandText = MySql
                cmd1.Connection = cn
                Dim ReadRS As OleDbDataReader = cmd1.ExecuteReader()

                Do While ReadRS.Read()
                    XML = XML & "<DatiRiepilogo>" & vbNewLine

                    Dim MyIva As New Cls_IVA

                    MyIva.Leggi(Session("DC_TABELLE"), campodb(ReadRS.Item("CodiceIVA")))

                    XML = XML & "<AliquotaIVA>" & Replace(Format(MyIva.Aliquota * 100, "0.00"), ",", ".") & "</AliquotaIVA>" & vbNewLine
                    If MyIva.Natura.Trim <> "" Then
                        XML = XML & "<Natura>" & MyIva.Natura & "</Natura>" & vbNewLine
                    End If
                    If MyIva.Natura = "N4" Then
                        In4 = 1
                    End If
                    Dim negativo As String = ""
                    If CausaleContabile.TipoDocumento = "NC" Then
                        If campodb(ReadRS.Item("DareAvere")) = "A" Then
                            negativo = "-"
                        End If
                    Else
                        If campodb(ReadRS.Item("DareAvere")) = "D" Then
                            negativo = "-"
                        End If
                    End If

                    XML = XML & "<ImponibileImporto>" & negativo & Replace(Format(Math.Round(campodbN(ReadRS.Item("Imponibile")), 2), "0.00"), ",", ".") & "</ImponibileImporto>" & vbNewLine
                    XML = XML & "<Imposta>" & negativo & Replace(Format(Math.Round(campodbN(ReadRS.Item("Importo")), 2), "0.00"), ",", ".") & "</Imposta>" & vbNewLine

                    If campodbN(ReadRS.Item("Importo")) > 0 Then
                        If DI.TipoAnagrafica = "P" Then
                            XML = XML & "<EsigibilitaIVA>I</EsigibilitaIVA>"
                        Else
                            XML = XML & "<EsigibilitaIVA>S</EsigibilitaIVA>"
                        End If
                    End If

                    If MyIva.Natura = "N1" And Math.Round(campodbN(ReadRS.Item("Imponibile")), 2) = 2 Then
                        Dim TipoRegistro As New Cls_RegistroIVA

                        TipoRegistro.Tipo = Registrazione.RegistroIVA
                        TipoRegistro.Leggi(Session("DC_TABELLE"), TipoRegistro.Tipo)

                        If TipoRegistro.RegistroCartaceo = 1 Then
                            XML = XML & "<RiferimentoNormativo>Imposta di bollo</RiferimentoNormativo>" & vbNewLine
                        Else

                            XML = XML & "<RiferimentoNormativo>Imposta di bollo assolta in modo virtuale ai sensi del D.M. 17/6/2014</RiferimentoNormativo>" & vbNewLine
                        End If
                    Else
                        XML = XML & "<RiferimentoNormativo>" & MyIva.Descrizione & "</RiferimentoNormativo>" & vbNewLine
                    End If
                    XML = XML & "</DatiRiepilogo>" & vbNewLine

                    If CausaleContabile.TipoDocumento = "NC" Then
                        If campodb(ReadRS.Item("DareAvere")) = "A" Then
                            Totale = Totale - Math.Round(campodbN(ReadRS.Item("Imponibile")), 2)
                        Else
                            Totale = Totale + Math.Round(campodbN(ReadRS.Item("Imponibile")), 2)
                        End If
                    Else
                        If campodb(ReadRS.Item("DareAvere")) = "D" Then
                            Totale = Totale - Math.Round(campodbN(ReadRS.Item("Imponibile")), 2)
                        Else
                            Totale = Totale + Math.Round(campodbN(ReadRS.Item("Imponibile")), 2)
                        End If
                    End If
                Loop
                ReadRS.Close()


                XML = XML & "</DatiBeniServizi>" & vbNewLine
                XML = XML & "<DatiPagamento>" & vbNewLine
                XML = XML & "<CondizioniPagamento>TP02</CondizioniPagamento>" & vbNewLine
                XML = XML & "<DettaglioPagamento>" & vbNewLine

                If DI.ModalitaPagamento = "" Then
                    DI.ModalitaPagamento = "MP05"
                End If
                If Server.MachineName.ToUpper = "COP64" Then
                    DI.ModalitaPagamento = "MP15"  ' COMUNE DI LUCCA (?)
                End If

                XML = XML & "<ModalitaPagamento>" & DI.ModalitaPagamento & "</ModalitaPagamento>" & vbNewLine


                If Session("DC_OSPITE").ToString.ToUpper.IndexOf("SanGiuseppeMoscati".ToUpper) > 0 Or Session("DC_OSPITE").ToString.ToUpper.IndexOf("casadicurasanvincenzo".ToUpper) > 0 Then
                    If DI.TipoAnagrafica <> "P" Then
                        XML = XML & "<DataScadenzaPagamento>" & Format(Registrazione.DataRegistrazione.AddDays(60), "yyyy-MM-dd") & "</DataScadenzaPagamento>" & vbNewLine
                    End If
                Else
                    Dim cmd2 As New OleDbCommand

                    cmd2.CommandText = "Select * From Scadenzario where NumeroRegistrazioneContabile = " & Registrazione.NumeroRegistrazione
                    cmd2.Connection = cn
                    Dim ReadRS2 As OleDbDataReader = cmd2.ExecuteReader()
                    If ReadRS2.Read Then
                        XML = XML & "<DataScadenzaPagamento>" & Format(ReadRS2.Item("DataScadenza"), "yyyy-MM-dd") & "</DataScadenzaPagamento>" & vbNewLine
                    End If
                    ReadRS2.Close()
                End If



                If DI.TipoAnagrafica = "P" Then
                    XML = XML & "<ImportoPagamento>" & Replace(Format(Math.Round(Math.Abs(Registrazione.ImportoDocumento(Session("DC_TABELLE"))), 2), "0.00"), ",", ".") & "</ImportoPagamento>" & vbNewLine
                Else
                    If CausaleContabile.TipoDocumento = "NC" Then
                        XML = XML & "<ImportoPagamento>" & Replace(Format(Math.Abs(Math.Round(Totale, 2)), "0.00"), ",", ".") & "</ImportoPagamento>" & vbNewLine
                    Else
                        XML = XML & "<ImportoPagamento>" & Replace(Format(Math.Round(Totale, 2), "0.00"), ",", ".") & "</ImportoPagamento>" & vbNewLine
                    End If
                End If

                If DI.ModalitaPagamento = "MP05" Then
                    If DI.Iban = "##00#0000000000000000000000" Or DI.Iban = "" Or DI.Iban = "  00 0000000000000000000000" Then
                        XML = XML & "<IBAN>" & DatiSocioeta.IBAN & "</IBAN>" & vbNewLine
                    Else
                        XML = XML & "<IBAN>" & DI.Iban & "</IBAN>" & vbNewLine
                    End If
                End If


                If Server.MachineName.ToUpper = "COP64" Then
                    XML = XML & "<IstitutoFinanziario>Banca D'Italia</IstitutoFinanziario>"
                    XML = XML & "<CodicePagamento>0062618</CodicePagamento>" & vbNewLine
                End If

                XML = XML & "</DettaglioPagamento>" & vbNewLine
                XML = XML & "</DatiPagamento>" & vbNewLine



                If AllegatoPresente(Registrazione.NumeroRegistrazione) Then
                    Try
                        XML = XML & Allegato(Registrazione.NumeroRegistrazione)
                    Catch ex As Exception

                    End Try
                End If

                XML = XML & "</FatturaElettronicaBody>" & vbNewLine
                XML = XML & "</p:FatturaElettronica>" & vbNewLine



                XML = Regex.Replace(XML, "[^\u0000-\u007F]+", String.Empty)
                XML = XML.Replace("&", "")

                Dim NomeFileMovim As String

                If DI.TipoAnagrafica = "P" Then
                    NomeFileMovim = HostingEnvironment.ApplicationPhysicalPath() & "\Public\IT" & Format(Val(DatiSocioeta.PartitaIVA), "00000000000") & "_" & Format(MaxID, "00000") & ".xml"
                Else
                    NomeFileMovim = HostingEnvironment.ApplicationPhysicalPath() & "\Public\IT" & Format(Val(DatiSocioeta.PartitaIVA), "00000000000") & "_" & Format(MaxID, "00000") & ".xml"
                End If


                Dim FileClienti As System.IO.TextWriter = System.IO.File.CreateText(NomeFileMovim)
                FileClienti.Write(XML)
                FileClienti.Close()

                Dim cmdw As New OleDbCommand

                cmdw.CommandText = "INSERT INTO FatturaElettronica  (XMLOriginale,Progressivo,NumeroRegistrazione,DataOra,Utente) VALUES (?,?,?,?,?)"
                cmdw.Connection = cn
                cmdw.Parameters.AddWithValue("@XMLOriginale", XML)
                cmdw.Parameters.AddWithValue("@Progressivo", MaxID)
                cmdw.Parameters.AddWithValue("@NumeroRegistrazione", Registrazione.NumeroRegistrazione)
                cmdw.Parameters.AddWithValue("@DataOra", Now)
                cmdw.Parameters.AddWithValue("@Utente", Session("UTENTE"))
                cmdw.ExecuteNonQuery()


                If RegistrazioneContabile = 0 Then
                    If Send = True Then
                        Try
                            If SendFile("IT" & Format(Val(DatiSocioeta.PartitaIVA), "00000000000") & "_" & Format(MaxID, "00000") & ".xml", NomeFileMovim) Then
                                GridView1.Rows(Riga).Cells(8).Text = "OK"
                            Else
                                GridView1.Rows(Riga).Cells(8).Text = "ERRORE"
                            End If
                        Catch ex As Exception
                            GridView1.Rows(Riga).Cells(8).Text = "ERRORE " & ex.Message
                        End Try

                    Else
                        Zipfile.AddFile(NomeFileMovim, "\")
                    End If
                Else
                    Session("FATTURAXML") = XML
                    'ScriptManager.RegisterStartupScript(Me, Me.GetType(), "temp", "<script language='javascript'>DialogBoxx('FatturaElettronicaViewer.aspx');</script>", False)


                    ClientScript.RegisterClientScriptBlock(Me.GetType(), "MyBox", "$(window).load(function() {  DialogBox('../GeneraleWeb/FatturaElettronicaViewer.aspx');  });", True)
                    'ClientScript.RegisterClientScriptBlock(Me.GetType(), "temp", "<script language='javascript'> $(document).ready( function() { DialogBox('FatturaElettronicaViewer.aspx');  });</script>", False)
                End If
            End If
        Next

        'DocumentiIncassi_20180613180148
        If Send = False Then
            Zipfile.Save(HostingEnvironment.ApplicationPhysicalPath() & "\Public\I_" & Session("CLIENTE") & "_FattureElettroniche.zip")
        End If

        Try
            Kill(HostingEnvironment.ApplicationPhysicalPath() & "\Public\IT" & Format(Val(DatiSocioeta.PartitaIVA), "00000000000") & "_*.xml")
        Catch ex As Exception

        End Try


        If Send = False Then
            Try
                Response.Clear()
                Response.Buffer = True
                Response.ContentType = "text/plain"
                Response.AppendHeader("content-disposition", "attachment;filename=FattureElettroniche.zip")
                Response.WriteFile(HostingEnvironment.ApplicationPhysicalPath() & "\Public\I_" & Session("CLIENTE") & "_FattureElettroniche.zip")
                Response.Flush()
                Response.End()
            Finally
                Kill(HostingEnvironment.ApplicationPhysicalPath() & "\Public\I_" & Session("CLIENTE") & "_FattureElettroniche.zip")
            End Try
        End If

    End Sub

    Private Function SendFile(ByVal Nomefile As String, ByVal NomefilePath As String) As Boolean
        Dim _WebRequest As HttpWebRequest = CreateWebRequestverifyDigest()

        SendFile = False

        Dim XmlDocument As New XmlDocument()
        Dim MyDatiGen As New Cls_DatiGenerali
        Dim MyDatiSoc As New Cls_DecodificaSocieta


        MyDatiGen.LeggiDati(Session("DC_TABELLE"))
        MyDatiSoc.Leggi(Session("DC_TABELLE"))

        Dim MyXML As String = ""


        MyXML = "<?xml version=""1.0"" encoding=""UTF-8""?>" & _
                "<SOAP-ENV:Envelope xmlns:SOAP-ENV=""http://schemas.xmlsoap.org/soap/envelope/"" xmlns:ns1=""http://schema.write.b2b.hub.teamsystem.com/v7"">" & _
                  "<SOAP-ENV:Body>" & _
                    "<ns1:uploadFiles_request>" & _
                     " <ns1:auth>" & _
                      "  <ns1:id>" & MyDatiGen.IdAgyo & "</ns1:id>" & _
                       " <ns1:securityToken>" & MyDatiGen.TokenAgyo & "</ns1:securityToken>" & _
                       " <ns1:appName>VR140</ns1:appName>" & _
                      "</ns1:auth>" & _
                      "<ns1:transmitterId>" & Format(MyDatiSoc.PartitaIVA, "00000000000") & "</ns1:transmitterId>" & _
                      "<ns1:uploads>" & _
                      "  <ns1:upload>" & _
                      "    <ns1:senderId>" & Format(MyDatiSoc.PartitaIVA, "00000000000") & "</ns1:senderId>          " & _
                      "    <ns1:recipientId>" & Format(MyDatiSoc.PartitaIVA, "00000000000") & "</ns1:recipientId>" & _
                      "    <ns1:files>" & _
                      "      <ns1:file>" & _
                      "        <ns1:name>" & Nomefile & "</ns1:name>" & _
                      "        <ns1:content>" & ConvertFileToBase64(NomefilePath) & "</ns1:content>                           " & _
                      "      </ns1:file>" & _
                      "    </ns1:files>" & _
                      "   <ns1:flow>" & _
                      "      <ns1:type>SDI</ns1:type>" & _
                      "    </ns1:flow>" & _
                      "  </ns1:upload>" & _
                      "</ns1:uploads>" & _
                    "</ns1:uploadFiles_request>" & _
                  "</SOAP-ENV:Body>" & _
                "</SOAP-ENV:Envelope>"


        XmlDocument.LoadXml(MyXML)


        Using MyStream As Stream = _WebRequest.GetRequestStream()

            XmlDocument.Save(MyStream)
        End Using



        Using MyResponse As WebResponse = _WebRequest.GetResponse()

            Using MyReader As StreamReader = New StreamReader(MyResponse.GetResponseStream())

                Dim Appoggio As String
                Appoggio = MyReader.ReadToEnd()

                Dim Inizio As Integer
                Dim Fine As Integer
                Dim Status As String
                Inizio = Appoggio.IndexOf("<status>") + 9
                Fine = Appoggio.IndexOf("</status>")

                Status = Mid(Appoggio, Inizio, Fine - Inizio + 1)
                If Status.IndexOf("CARICATO") Then
                    SendFile = True
                End If

                'GetToken = Mid(Appoggio, Inizio, Fine - Inizio + 1)
            End Using
        End Using
    End Function


    Public Function ConvertFileToBase64(ByVal fileName As String) As String

        Dim ReturnValue As String = ""

        If My.Computer.FileSystem.FileExists(fileName) Then
            Using BinaryFile As FileStream = New FileStream(fileName, FileMode.Open)
                Dim BinRead As BinaryReader = New BinaryReader(BinaryFile)
                Dim BinBytes As Byte() = BinRead.ReadBytes(CInt(BinaryFile.Length))
                ReturnValue = Convert.ToBase64String(BinBytes)
                BinaryFile.Close()
            End Using
        End If
        Return ReturnValue
    End Function

    REM Invio Con Agyo 
    Private Function CreateWebRequestverifyDigest() As HttpWebRequest
        Dim DatiGen As New Cls_DatiGenerali

        DatiGen.LeggiDati(Session("DC_TABELLE"))



        Dim webRequest As HttpWebRequest

        If Val(DatiGen.TestAgyo) = 1 Then
            webRequest = HttpWebRequest.Create("https://soap-b2bapi-b2bhub.agyo.io/B2BWriteApi_v7/B2BWriteApi.ws")
        Else
            webRequest = HttpWebRequest.Create("https://soap-b2bapi-b2bhub-test.agyo.io/B2BWriteApi_v7/B2BWriteApi.ws")
        End If


        webRequest.Headers.Add("SOAP:Action")
        '   //webRequest.ContentType = "text/xml;charset=\"utf-8\"";
        webRequest.ContentType = "application/soap+xml;charset=UTF-8;action=uploadFiles"
        webRequest.Accept = "text/xml"
        webRequest.Method = "POST"
        Return webRequest
    End Function


    Public Sub Comprimi(ByVal origine As String, ByVal Destinazione As String)

        'Verifico che il file esista
        If File.Exists(origine) = False Then

            Exit Sub
        End If

        ' creazione dello stream del file da zippatre
        Dim buffer As Byte() = Nothing
        Dim StreamOrigine As FileStream = Nothing
        Dim StreamDestinazione As FileStream = Nothing
        Dim FileZip As GZipStream = Nothing

        Try
            ' Leggo lo stream dati del file
            StreamOrigine = New FileStream(origine, FileMode.Open, FileAccess.Read, FileShare.Read)

            ' recupero i byte del file
            buffer = New Byte(StreamOrigine.Length) {}
            Dim dimensione As Integer = StreamOrigine.Read(buffer, 0, buffer.Length)

            ' Apro un stream per il file di scrittura (zip)
            StreamDestinazione = New FileStream(Destinazione, FileMode.OpenOrCreate, FileAccess.Write)

            'imposto il file compresso
            FileZip = New GZipStream(StreamDestinazione, CompressionMode.Compress, True)

            'Scrivo nel file compresso il buffer
            FileZip.Write(buffer, 0, buffer.Length)

        Catch ex As ApplicationException

        Finally
            'libero la memoria
            If Not (StreamOrigine Is Nothing) Then
                StreamOrigine.Close()
            End If
            If Not (FileZip Is Nothing) Then
                FileZip.Close()
            End If
            If Not (StreamDestinazione Is Nothing) Then
                StreamDestinazione.Close()
            End If
        End Try

    End Sub


    Public Function ZipDirectory(ByVal DirPath As String) As Byte()
        If Not Directory.Exists(DirPath) Then Return Nothing

        Dim Directories = Directory.GetDirectories(DirPath, "*", SearchOption.AllDirectories)
        Dim Files = Directory.GetFiles(DirPath, "*", SearchOption.AllDirectories)

        Dim X As New XmlDocument
        Dim RootNode = X.CreateElement("Content")
        Dim DirsNode = X.CreateElement("Directories")
        Dim FilesNode = X.CreateElement("Directories")

        X.AppendChild(RootNode)
        RootNode.AppendChild(DirsNode)
        RootNode.AppendChild(FilesNode)

        For Each d In Directories
            Dim DirNode = X.CreateElement("Directory")
            Dim PathAttrib = X.CreateAttribute("Path")
            PathAttrib.Value = d.Replace(DirPath & "\", "") 'Create relative paths
            DirNode.Attributes.Append(PathAttrib)
            DirsNode.AppendChild(DirNode)
        Next

        For Each f In Files
            Dim FileNode = X.CreateElement("File")
            Dim PathAttrib = X.CreateAttribute("Path")
            PathAttrib.Value = f.Replace(DirPath & "\", "") 'Create relative paths
            FileNode.Attributes.Append(PathAttrib)
            FileNode.InnerText = Convert.ToBase64String(File.ReadAllBytes(f))
            FilesNode.AppendChild(FileNode)
        Next

        Using Mem As New MemoryStream()
            X.Save(Mem)
            Dim AllContentsAsByteArray = Mem.ToArray()
            Dim CompressedContent = CompressArray(AllContentsAsByteArray)
            Return CompressedContent
        End Using
    End Function

    Public Sub UnzipDirectory(ByVal compressed() As Byte, ByVal outputPath As String)
        If Not Directory.Exists(outputPath) Then Directory.CreateDirectory(outputPath)

        Dim Uncompressed = DecompressArray(compressed)

        Dim X As New XmlDocument

        Using Mem As New MemoryStream(Uncompressed)
            X.Load(Mem)

            Dim RootNode = X.FirstChild
            Dim DirsNode = RootNode.FirstChild
            Dim FilesNode = RootNode.FirstChild.NextSibling

            For Each ChildDir In DirsNode.ChildNodes
                Directory.CreateDirectory(Path.Combine(outputPath, DirectCast(ChildDir, XmlNode).Attributes.Item(0).Value))
            Next

            For Each ChildFile In FilesNode.ChildNodes
                Dim FilePath = Path.Combine(outputPath, DirectCast(ChildFile, XmlNode).Attributes.Item(0).Value)
                Dim Content = Convert.FromBase64String(DirectCast(ChildFile, XmlNode).InnerText)
                File.WriteAllBytes(FilePath, Content)
            Next
        End Using
    End Sub

    Private Function CompressArray(ByVal content() As Byte) As Byte()
        Using outFile As New MemoryStream()
            Using Compress As New GZipStream(outFile, CompressionMode.Compress)
                Compress.Write(content, 0, content.Length)
            End Using

            Return outFile.ToArray()
        End Using
    End Function

    Private Function DecompressArray(ByVal content() As Byte) As Byte()
        Using outFile As New MemoryStream()
            Using inFile As New MemoryStream(content)
                Using Compress As New GZipStream(inFile, CompressionMode.Decompress)
                    Dim buffer(1023) As Byte
                    Dim nRead As Integer
                    Do
                        nRead = Compress.Read(buffer, 0, buffer.Length)
                        outFile.Write(buffer, 0, nRead)
                    Loop While nRead > 0
                End Using
            End Using

            Return outFile.ToArray()
        End Using
    End Function

    Function AdattaLunghezzaNumero(ByVal Testo As String, ByVal Lunghezza As Integer) As String
        If Len(Testo) > Lunghezza Then
            AdattaLunghezzaNumero = Mid(Testo, 1, Lunghezza)
            Exit Function
        End If
        AdattaLunghezzaNumero = Replace(Space(Lunghezza - Len(Testo)) & Testo, " ", "0")
    End Function

    Function AdattaLunghezza(ByVal Testo As String, ByVal Lunghezza As Integer) As String
        If Len(Testo) > Lunghezza Then
            Testo = Mid(Testo, 1, Lunghezza)
        End If

        AdattaLunghezza = Testo & Space(Lunghezza - Len(Testo) + 1)

        If Len(AdattaLunghezza) > Lunghezza Then
            AdattaLunghezza = Mid(AdattaLunghezza, 1, Lunghezza)
        End If
    End Function


    Class DatiIntestatario
        Public TipoAnagrafica As String
        Public RagioneSociale As String
        Public Cognome As String
        Public Nome As String
        Public PartitaIVA As String
        Public CodiceFiscale As String
        Public Indirizzo As String
        Public Cap As String
        Public Citta As String
        Public Provincia As String
        Public Stato As String
        Public CodiceDestinatario As String
        Public TipoInvio As String
        Public Cup As String
        Public Cig As String
        Public Iban As String
        Public Banca As String
        Public IdDocumento As String
        Public RifAmministrazione As String
        Public NumeroFatturaDDT As String
        Public RigaNote As String
        Public NoteFattura As String
        Public ModalitaPagamento As String
        Public NumItem As String
        Public CodiceCommessaConvezione As String
        Public Causale As String
        Public IDDataDoc As String
        Public CodiceCatastale As String
        Public Pec As String
        Public SoggettoABollo As String
        Public BolloVirtuale As String
        Public NonScadenza As String
        Public DescrizioneXML As String
    End Class

    Protected Sub Btn_Esci_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles Btn_Esci.Click
        If Request.Item("MENU") = "GENERALE" Then
            Response.Redirect("Menu_Generale.aspx")
        Else
            Response.Redirect("../OspitiWeb/Menu_Export.aspx")
        End If
    End Sub

    Protected Sub Btn_Seleziona_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles Btn_Seleziona.Click
        Dim Riga As Integer = 0
        For Riga = 0 To GridView1.Rows.Count - 1
            Dim CheckBox As CheckBox = DirectCast(GridView1.Rows(Riga).FindControl("ChkImporta"), CheckBox)

            If CheckBox.Enabled = True Then
                CheckBox.Checked = True
            End If
        Next
    End Sub

    Protected Sub Btn_Send_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles Btn_Send.Click
        CreateXML(True, 0)
    End Sub

    Protected Sub GridView1_RowCommand(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewCommandEventArgs) Handles GridView1.RowCommand
        If (e.CommandName = "Richiama") Then
            Dim Registrazione As Long
            Registrazione = Val(e.CommandArgument)

            CreateXML(True, Registrazione)
            Exit Sub
        End If
        If (e.CommandName = "UpLoadDoc") Then
            Dim Registrazione As Long
            Registrazione = Val(e.CommandArgument)

            Upload(Registrazione)
            Exit Sub
        End If
    End Sub


    Private Sub Upload(ByVal NumeroRegistrazione As Long)
        ClientScript.RegisterClientScriptBlock(Me.GetType(), "MyBox", "$(window).load(function() {  DialogBoxSmall('AllegatoRegistrazione.aspx?NumeroRegistrazione=" & NumeroRegistrazione & "');  });", True)

    End Sub

    Protected Sub GridView1_RowDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs) Handles GridView1.RowDataBound
        If e.Row.RowType = DataControlRowType.DataRow Then
            'Dim CheckBox As CheckBox = DirectCast(GridView1.Rows(Riga).FindControl("ChkImporta"), CheckBox)

            Dim ChkBollo As CheckBox = DirectCast(e.Row.FindControl("ChkBollo"), CheckBox)
            ChkBollo.Checked = False
            If Tabella.Rows(e.Row.RowIndex).Item(8).ToString = "SI" Then
                ChkBollo.Checked = True
            End If
            Dim DataLettura As Date = Nothing
            Try
                DataLettura = Tabella.Rows(e.Row.RowIndex).Item(2).ToString
            Catch ex As Exception

            End Try
            If Year(DataLettura) >= 2019 Then
                If Format(DataLettura, "yyyyMMdd") >= "20190418" Then
                    ChkBollo.Enabled = False
                End If
            End If


            Dim UpLoadDoc As ImageButton = DirectCast(e.Row.FindControl("UpLoadDoc"), ImageButton)
            If Tabella.Rows(e.Row.RowIndex).Item(9).ToString = "SI" Then
                UpLoadDoc.BackColor = Color.Blue
            Else
                UpLoadDoc.BackColor = Color.Transparent
            End If

        End If
    End Sub

    Public Function DecodificaMese(ByVal Mese As Integer) As String
        DecodificaMese = ""
        If Mese = 1 Then
            DecodificaMese = "Gennaio"
        End If
        If Mese = 2 Then
            DecodificaMese = "Febbraio"
        End If
        If Mese = 3 Then
            DecodificaMese = "Marzo"
        End If
        If Mese = 4 Then
            DecodificaMese = "Aprile"
        End If
        If Mese = 5 Then
            DecodificaMese = "Maggio"
        End If
        If Mese = 6 Then
            DecodificaMese = "Giugno"
        End If
        If Mese = 7 Then
            DecodificaMese = "Luglio"
        End If
        If Mese = 8 Then
            DecodificaMese = "Agosto"
        End If
        If Mese = 9 Then
            DecodificaMese = "Settembre"
        End If
        If Mese = 10 Then
            DecodificaMese = "Ottobre"
        End If
        If Mese = 11 Then
            DecodificaMese = "Novembre"
        End If
        If Mese = 12 Then
            DecodificaMese = "Dicembre"
        End If
    End Function

    Protected Overrides Sub Finalize()
        MyBase.Finalize()
    End Sub
End Class
