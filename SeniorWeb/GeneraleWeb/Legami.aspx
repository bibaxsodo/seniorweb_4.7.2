﻿<%@ Page Language="VB" EnableEventValidation="false" AutoEventWireup="false" Inherits="GeneraleWeb_Legami" CodeFile="Legami.aspx.vb" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
    <title>LEGAMI</title>
    <asp:PlaceHolder runat="server">
        <%: System.Web.Optimization.Styles.Render("~/Content/AjaxControlToolkit/Styles/Bundle") %>
    </asp:PlaceHolder>
    <link href="ospiti.css?ver=11" rel="stylesheet" type="text/css" />
    <link rel="shortcut icon" href="images/SENIOR.ico" />
    <link href="js/jquery.autocomplete.css" rel="stylesheet" type="text/css" />

    <script src="js/jquery-1.5.1.min.js" type="text/javascript"></script>
    <script src="js/jquery.autocomplete.js" type="text/javascript"></script>
    <script src="js/soapclient.js" type="text/javascript"></script>
    <script src="/js/convertinumero.js" type="text/javascript"></script>

    <script src="js/jquery.maskedinput-1.3.min.js" type="text/javascript"></script>
    <script src="/js/formatnumer.js" type="text/javascript"></script>

    <script src="/js/pianoconti.js" type="text/javascript"></script>
    <script src="js/JSErrore.js" type="text/javascript"></script>
    <style>
        .Registrazione {
            width: 98%;
            height: 24px;
            -ms-filter: "progid:DXImageTransform.Microsoft.Alpha(Opacity=96)";
            filter: alpha(opacity=96);
            -moz-opacity: 0.96;
            -khtml-opacity: 0.96;
            opacity: 0.96;
            border: 1px #888888 solid;
            background-color: #86d5f9;
            padding: 2px;
            -moz-box-shadow: 4px 3px 2px 1px #110e0e;
            -webkit-box-shadow: 4px 3px 2px 1px #110e0e;
            box-shadow: 4px 3px 2px 1px #110e0e;
        }
    </style>
    <script type="text/javascript">     

        $(document).ready(function () {

            $('#<%=Txt_DataDal.ClientID%>').mask("99/99/9999");


            $('#<%=Txt_Dataal.ClientID%>').mask("99/99/9999");
        });


    </script>
</head>
<body>
    <form id="form1" runat="server" autocomplete="off">
        <asp:ScriptManager ID="ScriptManager1" runat="server" EnableScriptGlobalization="true">
            <Scripts>
                <asp:ScriptReference Path="~/Scripts/AjaxControlToolkit/Bundle" />
            </Scripts>
        </asp:ScriptManager>
        <div style="text-align: left;">
            <asp:Label ID="Lbl_DatiRegistrazione" runat="server" Text="Label"></asp:Label><br />
            <asp:GridView ID="Grid" runat="server" CellPadding="3" Height="60px"
                Width="100%" ShowFooter="True" BackColor="White" BorderColor="#CCCCCC"
                BorderStyle="None" BorderWidth="1px">
                <RowStyle ForeColor="#000066" />
                <Columns>
                    <asp:CommandField ButtonType="Image" ShowDeleteButton="True" DeleteImageUrl="~/images/cancella.png" />
                    <asp:TemplateField HeaderText="Numero Documento">
                        <EditItemTemplate>
                            <asp:Label ID="LblNumeroDocumento" runat="server" Text='<%# Eval("NumeroDocumento") %>' Width="136px"></asp:Label>
                        </EditItemTemplate>
                        <ItemTemplate>
                            <asp:Label ID="LblNumeroDocumento" runat="server" Text='<%# Eval("NumeroDocumento") %>' Width="136px"></asp:Label>
                        </ItemTemplate>
                    </asp:TemplateField>

                    <asp:TemplateField HeaderText="Numero Incasso/Pagamento">
                        <EditItemTemplate>
                            <asp:Label ID="LblNumIncPag" runat="server" Text='<%# Eval("NumIncPag") %>' Width="136px"></asp:Label>
                        </EditItemTemplate>
                        <ItemTemplate>
                            <div style="vertical-align: middle; height: 100%; display: inline-table;">
                                <asp:ImageButton ID="Seleziona" CommandName="Seleziona" runat="Server"
                                    ImageUrl="~/images/select.png" class="EffettoBottoniTondi" BackColor="Transparent"
                                    CommandArgument="<%#   Container.DataItemIndex  %>" ToolTip="Richiama" />
                            </div>
                            <div style="vertical-align: middle; height: 100%; display: inline-table;">
                                <asp:Label ID="LblNumIncPag" runat="server" Text='<%# Eval("NumIncPag") %>' Width="136px"></asp:Label>
                            </div>
                        </ItemTemplate>
                    </asp:TemplateField>

                    <asp:TemplateField HeaderText="Importo">
                        <EditItemTemplate>
                            <asp:Label ID="LblImporto" runat="server" Text='<%# Eval("Importo") %>' Width="136px"></asp:Label>
                        </EditItemTemplate>
                        <ItemTemplate>
                            <asp:Label ID="LblImporto" runat="server" Text='<%# Eval("Importo") %>' Width="136px"></asp:Label>
                        </ItemTemplate>
                    </asp:TemplateField>
                </Columns>
                <FooterStyle BackColor="White" ForeColor="#000066" />
                <PagerStyle BackColor="White" ForeColor="#000066" HorizontalAlign="Left" />
                <SelectedRowStyle BackColor="#669999" Font-Bold="True" ForeColor="White" />
                <HeaderStyle BackColor="#006699" Font-Bold="True" ForeColor="White" />
            </asp:GridView>
            <br />
            Data Dal :
        <asp:TextBox ID="Txt_DataDal" runat="server" Width="88px"></asp:TextBox>
            Data Al :
        <asp:TextBox ID="Txt_DataAl" runat="server" Width="88px"></asp:TextBox><br />
            <br />
            Numero Registrazione Dal:
        <asp:TextBox ID="Txt_NumeroRegistrazioneDal" onkeypress="return soloNumeri(event);" runat="server" Width="80px"></asp:TextBox>
            al :
        <asp:TextBox ID="Txt_NumeroRegistrazioneAl" onkeypress="return soloNumeri(event);" runat="server" Width="88px"></asp:TextBox>
            <br />
            <br />
            Conto :<asp:TextBox ID="Txt_Sottoconto" CssClass="MyAutoComplete" runat="server" Width="420px"></asp:TextBox>
            <br />
            <br />
            <asp:Button ID="Btn_Ricerca" runat="server" Text="Ricerca" />
            <br />
            <br />
            <asp:GridView ID="GridEstrazione" runat="server" CellPadding="3" Height="60px"
                Width="590px" ShowFooter="True" BackColor="White" BorderColor="#CCCCCC"
                BorderStyle="None" BorderWidth="1px">
                <RowStyle ForeColor="#000066" />
                <Columns>
                    <asp:CommandField ButtonType="Image" EditImageUrl="~/images/modifica.png" ShowEditButton="True" CancelImageUrl="~/images/annulla.png" UpdateImageUrl="~/images/aggiorna.png" />
                    <asp:TemplateField HeaderText="Numero Registrazione">
                        <EditItemTemplate>
                            <asp:Label ID="LblNumeroRegistrazione" runat="server" Text='<%# Eval("NumeroRegistrazione") %>' Width="136px"></asp:Label>
                        </EditItemTemplate>
                        <ItemTemplate>
                            <asp:Label ID="LblNumeroRegistrazione" runat="server" Text='<%# Eval("NumeroRegistrazione") %>' Width="136px"></asp:Label>
                        </ItemTemplate>
                    </asp:TemplateField>

                    <asp:TemplateField HeaderText="Data Registrazione">
                        <EditItemTemplate>
                            <asp:Label ID="LblDataRegistrazione" runat="server" Text='<%# Eval("DataRegistrazione") %>' Width="136px"></asp:Label>
                        </EditItemTemplate>
                        <ItemTemplate>
                            <asp:Label ID="LblDataRegistrazione" runat="server" Text='<%# Eval("DataRegistrazione") %>' Width="136px"></asp:Label>
                        </ItemTemplate>
                    </asp:TemplateField>

                    <asp:TemplateField HeaderText="Importo Documento">
                        <EditItemTemplate>
                            <asp:Label ID="LblImportoDocumento" runat="server" Text='<%# Eval("ImportoDocumento") %>' Width="136px"></asp:Label>
                        </EditItemTemplate>
                        <ItemTemplate>
                            <asp:Label ID="LblImportoDeocumento" runat="server" Text='<%# Eval("ImportoDocumento") %>' Width="136px"></asp:Label>
                        </ItemTemplate>
                    </asp:TemplateField>

                    <asp:TemplateField HeaderText="Importo da Legare">
                        <EditItemTemplate>
                            <asp:Label ID="LblImportoLegare" runat="server" Text='<%# Eval("ImportoLegare") %>' Width="136px"></asp:Label>
                        </EditItemTemplate>
                        <ItemTemplate>
                            <asp:Label ID="LblImportoLegare" runat="server" Text='<%# Eval("ImportoLegare") %>' Width="136px"></asp:Label>
                        </ItemTemplate>
                    </asp:TemplateField>

                    <asp:TemplateField HeaderText="Importo Legame">
                        <EditItemTemplate>
                            <asp:TextBox ID="TxtImportoLegame" Width="150px" runat="server"></asp:TextBox>
                        </EditItemTemplate>
                        <ItemTemplate>
                            <asp:Label ID="LblImportoLegame" runat="server" Text='<%# Eval("ImportoLegame") %>' Width="136px"></asp:Label>
                        </ItemTemplate>
                    </asp:TemplateField>


                    <asp:TemplateField HeaderText="Causale Contabile">
                        <EditItemTemplate>
                            <asp:Label ID="LblCausaleContabile" runat="server" Text='<%# Eval("CausaleContabile") %>' Width="136px"></asp:Label>
                        </EditItemTemplate>
                        <ItemTemplate>
                            <asp:Label ID="LblCausaleContabile" runat="server" Text='<%# Eval("CausaleContabile") %>' Width="136px"></asp:Label>
                        </ItemTemplate>
                    </asp:TemplateField>
                </Columns>
                <FooterStyle BackColor="White" ForeColor="#000066" />
                <PagerStyle BackColor="White" ForeColor="#000066" HorizontalAlign="Left" />
                <SelectedRowStyle BackColor="#669999" Font-Bold="True" ForeColor="White" />
                <HeaderStyle BackColor="#006699" Font-Bold="True" ForeColor="White" />
            </asp:GridView>
        </div>
    </form>
</body>
</html>
