﻿
Partial Class GeneraleWeb_caricaregistrazione
    Inherits System.Web.UI.Page

    Protected Sub GeneraleWeb_caricaregistrazione_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Dim STRREG As String

        STRREG = Request.Item("REGISTRAZIONE")


        Dim Registrazione As Long
        Registrazione = Val(STRREG)

        Dim Reg As New Cls_MovimentoContabile

        Reg.Leggi(Session("DC_GENERALE"), Registrazione)



        If Year(Reg.DataRegistrazione) < 1990 Then
            Dim PaginaRedirect As String

            PaginaRedirect = Request.Item("PAGINA").ToUpper

            PaginaRedirect = PaginaRedirect.Replace("generaleweb_".ToUpper, "")
            PaginaRedirect = PaginaRedirect.Replace("_ASPX", ".ASPX")

            Response.Redirect(PaginaRedirect)
            Exit Sub

        End If

        Dim CauCon As New Cls_CausaleContabile

        CauCon.Leggi(Session("DC_TABELLE"), Reg.CausaleContabile)
        If CauCon.Tipo = "I" Or CauCon.Tipo = "R" Then
            Session("NumeroRegistrazione") = Registrazione
            Response.Redirect("Documenti.aspx")            
            Exit Sub
        End If
        If CauCon.Tipo = "P" Then
            Session("NumeroRegistrazione") = Registrazione
            Response.Redirect("incassipagamenti.aspx")

            Exit Sub
        End If
        Session("NumeroRegistrazione") = Registrazione
        Response.Redirect("primanota.aspx?CHIAMATAESTERNA")
        REM Response
    End Sub
End Class
