﻿Imports Microsoft.VisualBasic
Imports System.Data.Odbc
Imports System.Web.Hosting
Imports System.IO
Imports System.Text
Imports System.Data
Imports System.Data.OleDb
Imports System.Configuration
Imports System.Security.Cryptography
Imports System.Net.Mail

Partial Class GeneraleWeb_ImportIncassi
    Inherits System.Web.UI.Page

    Protected Sub ImageButton1_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles ImageButton1.Click

        Dim TestoOutput As String = ""
        Dim NomeSocieta As String


        If FileUpload1.FileName = "" Then
            ClientScript.RegisterClientScriptBlock(Me.GetType(), "Errore", "VisualizzaErrore('File non indicato');", True)
            Exit Sub
        End If


        Dim CausaleContabile As New Cls_CausaleContabile

        CausaleContabile.Codice = Dd_CausaleContabile.SelectedValue
        CausaleContabile.Leggi(Session("DC_TABELLE"), CausaleContabile.Codice)

        Dim k As New Cls_Login

        k.Utente = Session("UTENTE")
        k.LeggiSP(Application("SENIOR"))

        NomeSocieta = k.RagioneSociale

        Dim Appo As String

        Appo = Dir(HostingEnvironment.ApplicationPhysicalPath() & "\Public\" & NomeSocieta)
        If Dir(HostingEnvironment.ApplicationPhysicalPath() & "\Public\" & NomeSocieta, FileAttribute.Directory) = "" Then
            MkDir(HostingEnvironment.ApplicationPhysicalPath() & "\Public\" & NomeSocieta)
        End If
        Dim Appoggio As String = Format(Now, "yyyyMMddHHmmss")

        FileUpload1.SaveAs(HostingEnvironment.ApplicationPhysicalPath() & "\Public\" & NomeSocieta & "\ExcelImport" & Appoggio & ".TXT")
        Dim Riga As Integer = 0


        Dim objStreamReader As StreamReader
        objStreamReader = File.OpenText(HostingEnvironment.ApplicationPhysicalPath() & "\Public\" & NomeSocieta & "\ExcelImport" & Appoggio & ".TXT")

        Do While Not objStreamReader.EndOfStream
            Dim Testo As String = ""
            Riga = Riga + 1
            Testo = objStreamReader.ReadLine
            If Len(Testo) >= 57 Then
                Dim NumeroIncasso As String
                Dim NumeroDocumento As String
                Dim AnnoProtocollo As String
                Dim SezionaleProtocolloIncasso As String
                Dim DataIncassoRead As String
                Dim ImportoIncasso As String
                Dim ModalitaPagamento As String
                Dim CodiceFiscale As String
                Dim Rest As Integer

                Rest = 1
                NumeroIncasso = Mid(Testo, 1, 8) : Rest = Rest + 8
                NumeroDocumento = Mid(Testo, Rest, 8) : Rest = Rest + 8
                AnnoProtocollo = Mid(Testo, Rest, 4) : Rest = Rest + 4
                SezionaleProtocolloIncasso = Mid(Testo, Rest, 2) : Rest = Rest + 2
                DataIncassoRead = Mid(Testo, Rest, 8) : Rest = Rest + 8

                Dim DataIncasso As Date

                DataIncasso = DateSerial(Mid(DataIncassoRead, 1, 4), Mid(DataIncassoRead, 5, 2), Mid(DataIncassoRead, 7, 2))




                ImportoIncasso = math.Round(Mid(Testo, Rest, 9) / 100,2) : Rest = Rest + 9
                ModalitaPagamento = Mid(Testo, Rest, 2) : Rest = Rest + 2
                CodiceFiscale = Mid(Testo, Rest, 16) : Rest = Rest + 16


                Dim Registrazione As New Cls_MovimentoContabile

                Registrazione.NumeroRegistrazione = 0
                Registrazione.LeggiRegistrazionePerNumeroProtocollo(Session("DC_GENERALE"), Val(SezionaleProtocolloIncasso), Val(NumeroDocumento), Val(AnnoProtocollo))
                If Registrazione.NumeroRegistrazione > 0 Then
                    Dim MastroCliente As Integer = 0
                    Dim ContoCliente As Long = 0
                    Dim Sottocontocliente As Long = 0

                    For i = 0 To Registrazione.Righe.Length - 1
                        If Not IsNothing(Registrazione.Righe(i)) Then
                            If Registrazione.Righe(i).RigaDaCausale = 1 Then
                                MastroCliente = Registrazione.Righe(i).MastroPartita
                                ContoCliente = Registrazione.Righe(i).ContoPartita
                                Sottocontocliente = Registrazione.Righe(i).SottocontoPartita
                            End If
                        End If
                    Next

                    Dim Errore As Boolean = False

                    If Int(Sottocontocliente / 100) = Math.Round(Sottocontocliente / 100, 10) Then
                        Dim Ospite As New ClsOspite

                        Ospite.CodiceOspite = Int(Sottocontocliente / 100)
                        Ospite.Leggi(Session("DC_OSPITE"), Ospite.CodiceOspite)

                        If Ospite.CODICEFISCALE <> CodiceFiscale Then
                            Errore = True
                            TestoOutput = TestoOutput & "<tr class=""miotr""><td class=""miacella"">CF non conicidente con documento </td><td class=""miacella"">" & "</td><td class=""miacella"">" & Registrazione.NumeroDocumento & "/" & Registrazione.AnnoProtocollo & "</td></tr>"
                        End If
                    Else
                        Dim Parenti As New Cls_Parenti

                        Parenti.CodiceOspite = Int(Sottocontocliente / 100)
                        Parenti.CodiceParente = Sottocontocliente - (Parenti.CodiceOspite * 100)
                        Parenti.Leggi(Session("DC_OSPITE"), Parenti.CodiceOspite, Parenti.CodiceParente)

                        If Parenti.CODICEFISCALE <> CodiceFiscale Then
                            Errore = True
                            TestoOutput = TestoOutput & "<tr class=""miotr""><td class=""miacella"">CF non conicidente con documento</td><td class=""miacella"">" & "</td><td class=""miacella"">" & Registrazione.NumeroDocumento & "/" & Registrazione.AnnoProtocollo & "</td></tr>"
                        End If
                    End If


                    If Not Errore Then

                        Dim Legame As New Cls_Legami

                        If Legame.TotaleLegame(Session("DC_GENERALE"), Registrazione.NumeroRegistrazione) = 0 Then
                            Dim ScriviRegistrazione As New Cls_MovimentoContabile

                            ScriviRegistrazione.NumeroRegistrazione = 0
                            ScriviRegistrazione.DataRegistrazione = DataIncasso
                            ScriviRegistrazione.CausaleContabile = Dd_CausaleContabile.SelectedValue
                            ScriviRegistrazione.Descrizione = "Incasso " & NumeroIncasso

                            ScriviRegistrazione.Righe(0) = New Cls_MovimentiContabiliRiga


                            ScriviRegistrazione.Righe(0).MastroPartita = MastroCliente
                            ScriviRegistrazione.Righe(0).ContoPartita = ContoCliente
                            ScriviRegistrazione.Righe(0).SottocontoPartita = Sottocontocliente

                            ScriviRegistrazione.Righe(0).DareAvere = CausaleContabile.Righe(0).DareAvere
                            ScriviRegistrazione.Righe(0).Segno = "+"
                            ScriviRegistrazione.Righe(0).Importo = ImportoIncasso


                            ScriviRegistrazione.Righe(1) = New Cls_MovimentiContabiliRiga

                            if ModalitaPagamento <>"" then
                                Dim ModPag As new ClsModalitaPagamento


                                ModPag.Codice = ModalitaPagamento
                                ModPag.Leggi(session("DC_OSPITE"))

                                ScriviRegistrazione.Righe(1).MastroPartita =ModPag.Mastro
                                ScriviRegistrazione.Righe(1).ContoPartita =ModPag.Conto
                                ScriviRegistrazione.Righe(1).SottocontoPartita = ModPag.Sottoconto
                            else
                                ScriviRegistrazione.Righe(1).MastroPartita = CausaleContabile.Righe(1).Mastro
                                ScriviRegistrazione.Righe(1).ContoPartita = CausaleContabile.Righe(1).Conto
                                ScriviRegistrazione.Righe(1).SottocontoPartita = CausaleContabile.Righe(1).Sottoconto
                            End If

                            
                            ScriviRegistrazione.Righe(1).DareAvere = CausaleContabile.Righe(1).DareAvere
                            ScriviRegistrazione.Righe(1).Segno = "+"
                            ScriviRegistrazione.Righe(1).Importo = ImportoIncasso

                            if ScriviRegistrazione.Scrivi(Session("DC_GENERALE"), 0) = True then
                                Dim LegameScrivi As New Cls_Legami

                                LegameScrivi.NumeroDocumento(0) = Registrazione.NumeroRegistrazione
                                LegameScrivi.NumeroPagamento(0) = ScriviRegistrazione.NumeroRegistrazione
                                LegameScrivi.Importo(0) = ImportoIncasso
                                LegameScrivi.Scrivi(Session("DC_GENERALE"), Registrazione.NumeroRegistrazione, 0)

                                TestoOutput = TestoOutput & "<tr class=""miotr""><td class=""miacella"">Inserito Incasso </td><td class=""miacella"">" & ScriviRegistrazione.NumeroRegistrazione & "</td><td class=""miacella"">" & Registrazione.NumeroDocumento & "/" & Registrazione.AnnoProtocollo & "</td></tr>"
                            Else
                                TestoOutput = TestoOutput & "<tr class=""miotr""><td class=""miacella"">Errore registrazione incasso </td><td class=""miacella"">" & "</td><td class=""miacella"">" & Registrazione.NumeroDocumento & "/" & Registrazione.AnnoProtocollo & "</td></tr>"
                            end If
                        Else
                            TestoOutput = TestoOutput & "<tr class=""miotr""><td class=""miacella"">Documento già incassato </td><td class=""miacella"">" & "</td><td class=""miacella"">" & Registrazione.NumeroDocumento & "/" & Registrazione.AnnoProtocollo & "</td></tr>"
                        End If

                    End If
                   else
                      TestoOutput = TestoOutput & "<tr class=""miotr""><td class=""miacella"">Documento non trovato </td><td class=""miacella"">" & "</td><td class=""miacella"">" & Val(SezionaleProtocolloIncasso) & " / " & Val(NumeroDocumento) & " / " & Val(AnnoProtocollo) & "</td></tr>"
                End If
            End If

        Loop

        objStreamReader.Close()

        Lbl_Errori.Text = "<table><tr class=""miotr""><th class=""miaintestazione"">Messaggio</th><th class=""miaintestazione"">Registrazione</th><th class=""miaintestazione"">Riferimento</th></tr>"
        Lbl_Errori.Text = Lbl_Errori.Text & TestoOutput & "</table>"
        Lbl_Errori.Visible = True

    End Sub

    Sub RaggruppaRegistrazioni(ByVal NumeroRegistrazione As Long)
        Dim cn As OleDbConnection

        Dim ImportoTotale As Double = 0
        Dim DareAvere As String = ""

        Dim VettoreMastro(100)
        Dim VettoreConto(100)
        Dim VettoreSottoconto(100)
        Dim VettoreImporto(100)
        Dim VettoreDareAVere(100)
        Dim Indice As Integer = 0


        cn = New Data.OleDb.OleDbConnection(Session("DC_GENERALE"))

        cn.Open()
        Dim cmd As New OleDbCommand()
        cmd.CommandText = " SELECT MastroPartita,ContoPartita,SottocontoPartita,SUM(CASE WHEN DareAvere = 'D' THEN importo  ELSE 0 END)  AS TOTDARE,SUM(CASE WHEN DareAvere = 'A' THEN importo ELSE 0 END)  AS TOTAVERE FROM MovimentiContabiliRiga Where Numero = ? Group by MastroPartita,ContoPartita,SottocontoPartita"
        cmd.Connection = cn
        cmd.Parameters.AddWithValue("NumeroRegistrazione", NumeroRegistrazione)
        Dim myPOSTreader As OleDbDataReader = cmd.ExecuteReader()
        Do While myPOSTreader.Read
            VettoreMastro(Indice) = campodb(myPOSTreader.Item("MastroPartita"))
            VettoreConto(Indice) = campodb(myPOSTreader.Item("ContoPartita"))
            VettoreSottoconto(Indice) = campodb(myPOSTreader.Item("SottocontoPartita"))
            If CDbl(campodb(myPOSTreader.Item("TOTAVERE"))) > CDbl(campodb(myPOSTreader.Item("TOTDARE"))) Then
                VettoreImporto(Indice) = campodb(myPOSTreader.Item("TOTAVERE")) - campodb(myPOSTreader.Item("TOTDARE"))
                VettoreDareAVere(Indice) = "A"
            Else
                VettoreImporto(Indice) = campodb(myPOSTreader.Item("TOTDARE")) - campodb(myPOSTreader.Item("TOTAVERE"))
                VettoreDareAVere(Indice) = "D"
            End If

            Indice = Indice + 1
        Loop
        myPOSTreader.Close()

        Dim cmdDelete As New OleDbCommand()
        cmdDelete.CommandText = " Delete FROM MovimentiContabiliRiga Where Numero = ?  "
        cmdDelete.Connection = cn
        cmdDelete.Parameters.AddWithValue("NumeroRegistrazione", NumeroRegistrazione)
        cmdDelete.ExecuteNonQuery()



        Dim I As Integer

        For I = 0 To Indice

            If Math.Abs(VettoreImporto(I)) > 0 Then
                Dim cmdInsert As New OleDbCommand()
                Dim MySql As String = ""

                MySql = " INSERT INTO MovimentiContabiliRiga (Utente,DataAggiornamento,Numero,MastroPartita,ContoPartita,SottocontoPartita,MastroContropartita,ContoContropartita,SottocontoContropartita,Descrizione,DareAvere,Segno,Importo) values   "
                MySql = MySql & " (?,?,?,?,?,?,?,?,?,?,?,?,?) "

                cmdInsert.CommandText = MySql

                cmdInsert.Connection = cn
                cmdInsert.Parameters.AddWithValue("Utente", "IMPREG") '1
                cmdInsert.Parameters.AddWithValue("DataAggiornamento", Now) '2
                cmdInsert.Parameters.AddWithValue("Numero", NumeroRegistrazione) '3
                cmdInsert.Parameters.AddWithValue("MastroPartita", VettoreMastro(I)) '4
                cmdInsert.Parameters.AddWithValue("ContoPratita", VettoreConto(I)) '5
                cmdInsert.Parameters.AddWithValue("SottocontoPartita", VettoreSottoconto(I)) '6
                cmdInsert.Parameters.AddWithValue("MastroContropartita", 0) '7
                cmdInsert.Parameters.AddWithValue("ContoContropartita", 0) '8
                cmdInsert.Parameters.AddWithValue("SottocontoContropartita", 0) '9
                cmdInsert.Parameters.AddWithValue("Descrizione", "") '10            
                cmdInsert.Parameters.AddWithValue("DareAvere", VettoreDareAVere(I)) '12
                cmdInsert.Parameters.AddWithValue("Segno", "+") '11
                cmdInsert.Parameters.AddWithValue("Importo", Math.Abs(VettoreImporto(I))) '13
                cmdInsert.ExecuteNonQuery()
            End If
        Next


        cn.Close()
    End Sub


    Function MaxRegistrazione() As Long
        Dim cn As OleDbConnection



        cn = New Data.OleDb.OleDbConnection(Session("DC_GENERALE"))

        cn.Open()
        Dim cmd As New OleDbCommand()
        cmd.CommandText = ("select max(NumeroRegistrazione) from MovimentiContabiliTesta ")
        cmd.Connection = cn
        Dim myPOSTreader As OleDbDataReader = cmd.ExecuteReader()
        If myPOSTreader.Read Then
            MaxRegistrazione = Val(campodb(myPOSTreader.Item(0))) + 1
        End If
        myPOSTreader.Close()

        cn.Close()
    End Function

    Function campodb(ByVal oggetto As Object) As String
        If IsDBNull(oggetto) Then
            Return ""
        Else
            Return oggetto
        End If
    End Function

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load

        Call EseguiJS()

        If Page.IsPostBack = True Then
            Exit Sub
        End If

        Dim K1 As New Cls_SqlString

        Dim Barra As New Cls_BarraSenior

        If Not IsNothing(Session("RicercaGeneraleSQLString")) Then
            K1 = Session("RicercaGeneraleSQLString")
        End If

        Lbl_BarraSenior.Text = Barra.CodiceBarra(Application("SENIOR"), Session("UTENTE"), Page, K1)


        Dim MCausaliContabili As New Cls_CausaleContabile

        MCausaliContabili.UpDateDropBox(Session("DC_TABELLE"), Dd_CausaleContabile)
    End Sub


    Private Sub EseguiJS()
        Dim MyJs As String
        MyJs = "$(document).ready(function() {"

        MyJs = MyJs & "var els = document.getElementsByTagName(""*"");"

        MyJs = MyJs & "for (var i=0;i<els.length;i++)"
        MyJs = MyJs & "if ( els[i].id ) { "
        MyJs = MyJs & " var appoggio =els[i].id; "
        MyJs = MyJs & " if (appoggio.match('Txt_Sottoconto')!= null) {  "
        MyJs = MyJs & " $(els[i]).autocomplete('/WebHandler/GestioneConto.ashx?Utente=" & Session("UTENTE") & "', {delay:5,minChars:3});"
        MyJs = MyJs & "    }"
        MyJs = MyJs & " if (appoggio.match('Txt_Delegato')!= null) {  "
        MyJs = MyJs & " $(els[i]).autocomplete('autocompleteclientifornitori.ashx?Utente=" & Session("UTENTE") & "', {delay:5,minChars:3});"
        MyJs = MyJs & "    }"



        MyJs = MyJs & " if ((appoggio.match('Txt_Importo')!= null) || appoggio.match('TxtAvere')!= null) {  "
        MyJs = MyJs & " $(els[i]).keypress(function() { ForceNumericInput($(this).val(), true, true); } ); "
        MyJs = MyJs & " $(els[i]).blur(function() { var ap = formatNumber($(this).val(),2); $(this).val(ap); });"
        MyJs = MyJs & "    }"

        MyJs = MyJs & " if (appoggio.match('Txt_Data')!= null) {  "
        MyJs = MyJs & " $(els[i]).mask(""99/99/9999"");"

        MyJs = MyJs & " $(els[i]).datepicker({ changeMonth: true,changeYear: true,  yearRange: ""1990:" & Year(Now) + 5 & """},$.datepicker.regional[""it""]);"

        MyJs = MyJs & "    }"



        MyJs = MyJs & "} "



        MyJs = MyJs & "if (window.innerHeight>0) { $(""#BarraLaterale"").css(""height"",(window.innerHeight - 94) + ""px""); } else"
        MyJs = MyJs & "{ $(""#BarraLaterale"").css(""height"",(document.documentElement.offsetHeight - 94) + ""px"");  }"
        MyJs = MyJs & "});"

        'MyJs = "$(document).ready(function() { $('.myClass').keypress(function() { handleEnter($('.myClass').val(), true, true); } );     $('#" & Txt_ImportoPagato.ClientID & "').blur(function() { var ap = formatNumber ($('#" & Txt_ImportoPagato.ClientID & "').val(),2); $('#" & Txt_ImportoPagato.ClientID & "').val(ap); }); });"
        ScriptManager.RegisterStartupScript(Me, Me.GetType(), "visualizzaRitIm", MyJs, True)
    End Sub

    Protected Sub Btn_Esci_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles Btn_Esci.Click
        Response.Redirect("Menu_Servizi.aspx")
    End Sub

    Protected Sub ImgMenu_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles ImgMenu.Click
        Response.Redirect("Menu_Servizi.aspx")
    End Sub
End Class
