﻿Imports Microsoft.VisualBasic
Imports System.Data.OleDb

Partial Class GeneraleWeb_DatiFatture
    Inherits System.Web.UI.Page
    Dim Tabella As New System.Data.DataTable("tabella")


    Function AdattaLunghezza(ByVal Testo As String, ByVal Lunghezza As Integer) As String

        If Len(Testo) > Lunghezza Then
            Testo = Mid(Testo, 1, Lunghezza)
        End If

        AdattaLunghezza = Testo & Space(Lunghezza - Len(Testo) + 1)

        If Len(AdattaLunghezza) > Lunghezza Then
            AdattaLunghezza = Mid(AdattaLunghezza, 1, Lunghezza)
        End If

    End Function


    Function AdattaLunghezzaNumero(ByVal Testo As String, ByVal Lunghezza As Integer) As String

        AdattaLunghezzaNumero = Replace(Space(Lunghezza - Len(Testo)) & Testo, " ", "0")
        REM AdattaLunghezzaNumero = Space(Lunghezza - Len(Testo)) & Testo


    End Function

    Function AdattaLunghezzaAdestra(ByVal Testo As String, ByVal Lunghezza As Integer) As String

        AdattaLunghezzaAdestra = Space(Lunghezza - Len(Testo)) & Testo


    End Function

    Private Sub EsportaSpesometro()
        Dim MySql As String
        Dim cn As OleDbConnection
        Dim cnGenerale As OleDbConnection
        Dim Progressivo As Integer
        Dim Xml As String = ""
        Dim M As New Cls_RegistroIVA

        M.Tipo = DD_Registro.SelectedValue
        M.Leggi(Session("DC_TABELLE"), M.Tipo)


        Tabella.Clear()
        Tabella.Columns.Add("Ragione Sociale/Cognome Nome", GetType(String))
        Tabella.Columns.Add("Codice Fiscale", GetType(String))
        Tabella.Columns.Add("Partita IVA", GetType(String))
        Tabella.Columns.Add("Data Registrazione", GetType(String))
        Tabella.Columns.Add("Numero Protocollo", GetType(String))
        Tabella.Columns.Add("Imponibile", GetType(String))
        Tabella.Columns.Add("Imposta", GetType(String))



        Session("ERRORIDATIFATTURE") = "<tr class=""miotr""><th class=""miaintestazione"">Numero Registrazione</th><th class=""miaintestazione"">Conto</th><th class=""miaintestazione"">Errore</th><th class=""miaintestazione"">Identificativo</th></tr>"

        Xml = "<?xml version=""1.0"" encoding=""UTF-8""?>" & vbNewLine
        Xml = Xml & "<n1:DatiFattura versione=""DAT20"" xmlns:n1=""http://ivaservizi.agenziaentrate.gov.it/docs/xsd/fatture/v2.0"" xmlns:ds=""http://www.w3.org/2000/09/xmldsig#"">" & vbNewLine
        Xml = Xml & "<DatiFatturaHeader>" & vbNewLine
        Xml = Xml & "<ProgressivoInvio>" & Txt_Progressivo.Text & "</ProgressivoInvio>" & vbNewLine
        If Txt_CodiceFiscale.Text <> "" Then
            Xml = Xml & "<Dichiarante>"
            Xml = Xml & "<CodiceFiscale>" & Txt_CodiceFiscale.Text & "</CodiceFiscale>"
            Xml = Xml & "<Carica>1</Carica>"
            Xml = Xml & "</Dichiarante>"
        End If

        Xml = Xml & "</DatiFatturaHeader>" & vbNewLine

        If M.TipoIVA = "V" Then
            Xml = Xml & "<DTE>" & vbNewLine
        Else
            Xml = Xml & "<DTR>" & vbNewLine
        End If




        cn = New Data.OleDb.OleDbConnection(Session("DC_TABELLE"))

        cnGenerale = New Data.OleDb.OleDbConnection(Session("DC_GENERALE"))

        cn.Open()

        cnGenerale.Open()

        Dim CmdDelete As New OleDbCommand

        MySql = "Delete From Temp_MovimentiContabiliTesta"
        CmdDelete.CommandText = MySql
        CmdDelete.Connection = cnGenerale
        CmdDelete.ExecuteNonQuery()

        Dim Condizione As String = " (RegistroIva = " & DD_Registro.SelectedValue

        If DD_Registro2.SelectedValue > 0 Then
            Condizione = Condizione & " Or RegistroIva = " & DD_Registro2.SelectedValue
        End If
        If DD_Registro3.SelectedValue > 0 Then
            Condizione = Condizione & " Or RegistroIva = " & DD_Registro3.SelectedValue
        End If
        Condizione = Condizione & " ) "
        

        'Condizione = " (NumeroRegistrazione = 124326 Or "
        'Condizione = Condizione & " NumeroRegistrazione = 124329 Or "
        'Condizione = Condizione & " NumeroRegistrazione = 125076 Or "
        'Condizione = Condizione & " NumeroRegistrazione =  125079 Or "
        'Condizione = Condizione & " NumeroRegistrazione = 125909 Or "
        'Condizione = Condizione & " NumeroRegistrazione = 125912 Or "
        'Condizione = Condizione & " NumeroRegistrazione = 126684 Or "
        'Condizione = Condizione & " NumeroRegistrazione = 126687 Or "
        'Condizione = Condizione & " NumeroRegistrazione = 127431 Or "
        'Condizione = Condizione & " NumeroRegistrazione = 127443 Or "
        'Condizione = Condizione & " NumeroRegistrazione = 128381 Or "
        'Condizione = Condizione & " NumeroRegistrazione = 128433)"


        MySql = "Select * From MovimentiContabiliTesta Where " & Condizione & " and DataRegistrazione >= ? And DataRegistrazione <= ?"
        Dim CmdI As New OleDbCommand

        Dim DataDal As Date = Txt_DataDal.Text
        Dim DataAl As Date = Txt_DataAl.Text
        Dim NonIncludere As Integer

        CmdI.CommandText = MySql
        CmdI.Connection = cnGenerale
        CmdI.Parameters.AddWithValue("@DataRegistrazione", DataDal)
        CmdI.Parameters.AddWithValue("@DataRegistrazione", DataAl)
        Dim CreaClienti As OleDbDataReader = CmdI.ExecuteReader()
        Do While CreaClienti.Read
            Dim Fattura As New Cls_MovimentoContabile
            Dim Chiave As String = ""

            NonIncludere = 0

            Fattura.Leggi(Session("DC_GENERALE"), campodb(CreaClienti.Item("NumeroRegistrazione")))

            Dim Indice As Integer = 0
            Dim ImponibileVerifica As Double = 0
            For Indice = 0 To Fattura.Righe.Length - 1
                If Not IsNothing(Fattura.Righe(Indice)) Then

                    If Fattura.Righe(Indice).Tipo = "IV" Then

                        Dim negativo As String = ""

                        Dim Aliquota As New Cls_IVA

                        Aliquota.Codice = Fattura.Righe(Indice).CodiceIVA
                        Aliquota.Leggi(Session("DC_TABELLE"), Aliquota.Codice)
                        If Aliquota.InSpesometro = 0 Then
                            ImponibileVerifica = ImponibileVerifica + Fattura.Righe(Indice).Imponibile
                        End If

                    End If
                End If
            Next




            Dim PC As New Cls_Pianodeiconti

            PC.Mastro = Fattura.Righe(0).MastroPartita
            PC.Conto = Fattura.Righe(0).ContoPartita
            PC.Sottoconto = Fattura.Righe(0).SottocontoPartita
            PC.Decodfica(Session("DC_GENERALE"))

            

            If PC.TipoAnagrafica = "O" Then
                Dim Ospite As New ClsOspite

                Ospite.CodiceOspite = Int(PC.Sottoconto / 100)
                Ospite.Leggi(Session("DC_OSPITE"), Ospite.CodiceOspite)

                Chiave = Ospite.CODICEFISCALE

            End If

            If PC.TipoAnagrafica = "P" Then
                Dim Parente As New Cls_Parenti

                Parente.CodiceOspite = Int(PC.Sottoconto / 100)
                Parente.CodiceParente = PC.Sottoconto - (Parente.CodiceOspite * 100)
                Parente.Leggi(Session("DC_OSPITE"), Parente.CodiceOspite, Parente.CodiceParente)

                Chiave = Parente.CODICEFISCALE
            End If
            If PC.TipoAnagrafica = "R" Or PC.TipoAnagrafica = "C" Then
                If Mid(Fattura.Tipologia & Space(1), 1, 1) = "C" Or Mid(Fattura.Tipologia & Space(1), 1, 1) = "J" Then
                    Dim Comune As New ClsComune

                    Comune.Provincia = Mid(Fattura.Tipologia & Space(7), 2, 3)
                    Comune.Comune = Mid(Fattura.Tipologia & Space(7), 5, 3)
                    Comune.Leggi(Session("DC_OSPITE"))


                    If Comune.PartitaIVA > 0 Then
                        Chiave = Comune.PartitaIVA
                    Else
                        Chiave = Comune.CodiceFiscale
                    End If
                End If
            End If

            If PC.TipoAnagrafica = "R" Or PC.TipoAnagrafica = "C" Then
                If Mid(Fattura.Tipologia & Space(1), 1, 1) = "R" Then
                    Dim Regione As New ClsUSL

                    Regione.CodiceRegione = Mid(Fattura.Tipologia & Space(7), 2, 4)
                    Regione.Leggi(Session("DC_OSPITE"))

                    If Regione.PARTITAIVA > 0 Then
                        Chiave = Regione.PARTITAIVA
                    Else
                        Chiave = Regione.CodiceFiscale
                    End If
                End If
            End If

            If PC.TipoAnagrafica = "D" Then

                Dim cnOspite As New OleDbConnection

                cnOspite = New Data.OleDb.OleDbConnection(Session("DC_OSPITE"))

                cnOspite.Open()

                Dim CliFor As New Cls_ClienteFornitore

                Dim Causale As New Cls_CausaleContabile

                Causale.Codice = Fattura.CausaleContabile

                Causale.Leggi(Session("DC_TABELLE"), Causale.Codice)
                If Causale.VenditaAcquisti = "V" Then
                    Dim cmdTipologiaCF As New OleDbCommand

                    cmdTipologiaCF.CommandText = "Select  * From AnagraficaComune Where MastroCliente = " & PC.Mastro & " And ContoCliente = " & PC.Conto & " And SottocontoCliente  = " & PC.Sottoconto
                    cmdTipologiaCF.Connection = cnOspite
                    Dim ReadCliFor As OleDbDataReader = cmdTipologiaCF.ExecuteReader()
                    If ReadCliFor.Read() Then
                        CliFor.CODICEDEBITORECREDITORE = campodb(ReadCliFor.Item("CODICEDEBITORECREDITORE"))
                    End If
                    ReadCliFor.Close()
                Else
                    Dim cmdTipologiaCF As New OleDbCommand

                    cmdTipologiaCF.CommandText = "Select  * From AnagraficaComune Where MastroFornitore = " & PC.Mastro & " And ContoFornitore = " & PC.Conto & " And SottocontoFornitore = " & PC.Sottoconto
                    cmdTipologiaCF.Connection = cnOspite
                    Dim ReadCliFor As OleDbDataReader = cmdTipologiaCF.ExecuteReader()
                    If ReadCliFor.Read() Then
                        CliFor.CODICEDEBITORECREDITORE = campodb(ReadCliFor.Item("CODICEDEBITORECREDITORE"))
                    End If
                    ReadCliFor.Close()
                End If
                CliFor.Leggi(Session("DC_OSPITE"))

                If CliFor.PARTITAIVA > 0 Then
                    Chiave = CliFor.PARTITAIVA
                Else
                    Chiave = CliFor.CodiceFiscale
                End If

                If CliFor.Contratto = 99 Then
                    NonIncludere = 1
                End If
                If Chk_Esterometro.Checked = True Then
                    If CliFor.Contratto = 1 Then
                        NonIncludere = 1
                    End If
                End If
            End If

            If Chk_EsclComuniRegioni.Checked = True Then

                If PC.TipoAnagrafica = "C" Or PC.TipoAnagrafica = "R" Then
                    NonIncludere = 1
                End If
            End If




            If Chiave = "" And NonIncludere = 0 Then
                Session("ERRORIDATIFATTURE") = Session("ERRORIDATIFATTURE") & "<tr class=""miotr"">"
                Session("ERRORIDATIFATTURE") = Session("ERRORIDATIFATTURE") & "<td class=""miacella"">" & Fattura.NumeroRegistrazione & "</td>"
                Session("ERRORIDATIFATTURE") = Session("ERRORIDATIFATTURE") & "<td class=""miacella"">" & PC.Mastro & "." & PC.Conto & "." & PC.Sottoconto & "</td>"
                Session("ERRORIDATIFATTURE") = Session("ERRORIDATIFATTURE") & "<td class=""miacella"">Cliente Non trovato</td>"
                Session("ERRORIDATIFATTURE") = Session("ERRORIDATIFATTURE") & "<td class=""miacella"">" & PC.TipoAnagrafica & "</td>"
                Session("ERRORIDATIFATTURE") = Session("ERRORIDATIFATTURE") & "</tr>"
                NonIncludere = 1
            End If

            If ImponibileVerifica = 0 Then
                NonIncludere = 1
            End If

            If NonIncludere = 0 Then
                Dim CmdITemp As New OleDbCommand

                CmdITemp.CommandText = "INSERT INTO Temp_MovimentiContabiliTesta (NumeroRegistrazione,CodiceCig) Values (?,?) "
                CmdITemp.Parameters.AddWithValue("@NumeroRegistrazione", campodb(CreaClienti.Item("NumeroRegistrazione")))
                CmdITemp.Parameters.AddWithValue("@CodiceCig", Chiave)
                CmdITemp.Connection = cnGenerale
                CmdITemp.ExecuteNonQuery()
            End If

        Loop
        CreaClienti.Close()



        If M.TipoIVA = "V" Then
            Xml = Xml & "<CedentePrestatoreDTE>" & vbNewLine
            Xml = Xml & DatiSocieta()
            Xml = Xml & "</CedentePrestatoreDTE>" & vbNewLine
        Else
            Xml = Xml & "<CessionarioCommittenteDTR>" & vbNewLine
            Xml = Xml & DatiSocieta()
            Xml = Xml & "</CessionarioCommittenteDTR>" & vbNewLine
        End If


        Session("TABELLA") = Tabella


        MySql = "Select * From Temp_MovimentiContabiliTesta Order by CodiceCig"
        Dim CmdT As New OleDbCommand
        Dim UltimoCFPIV As String = ""

        CmdT.CommandText = MySql
        CmdT.Connection = cnGenerale
        Dim Clienti As OleDbDataReader = CmdT.ExecuteReader()
        Do While Clienti.Read

            Dim Fattura As New Cls_MovimentoContabile


            Fattura.Leggi(Session("DC_GENERALE"), campodb(Clienti.Item("NumeroRegistrazione")))


            If UltimoCFPIV <> campodb(Clienti.Item("CodiceCig")) Then

                If M.TipoIVA = "V" Then
                    If UltimoCFPIV <> "" Then
                        Xml = Xml & "</CessionarioCommittenteDTE>" & vbNewLine
                    End If
                    Xml = Xml & "<CessionarioCommittenteDTE>" & vbNewLine
                    Xml = Xml & DatiCedentePrestatore(Fattura)

                Else
                    If UltimoCFPIV <> "" Then
                        Xml = Xml & "</CedentePrestatoreDTR>" & vbNewLine
                    End If
                    Xml = Xml & "<CedentePrestatoreDTR>" & vbNewLine
                    Xml = Xml & DatiCedentePrestatore(Fattura)
                End If
            End If
            If M.TipoIVA = "V" Then
                Xml = Xml & "<DatiFatturaBodyDTE>" & vbNewLine
                Xml = Xml & DatiFattura(Fattura)
                Xml = Xml & "</DatiFatturaBodyDTE>" & vbNewLine
            Else
                Xml = Xml & "<DatiFatturaBodyDTR>" & vbNewLine
                Xml = Xml & DatiFattura(Fattura)
                Xml = Xml & "</DatiFatturaBodyDTR>" & vbNewLine
            End If

            UltimoCFPIV = campodb(Clienti.Item("CodiceCig"))
        Loop
        cnGenerale.Close()

        cn.Close()

        If M.TipoIVA = "V" Then
            If UltimoCFPIV <> "" Then
                Xml = Xml & "</CessionarioCommittenteDTE>" & vbNewLine
            End If
        Else
            If UltimoCFPIV <> "" Then
                Xml = Xml & "</CedentePrestatoreDTR>" & vbNewLine
            End If
        End If

        If M.TipoIVA = "V" Then
            Xml = Xml & "</DTE>" & vbNewLine
        Else
            Xml = Xml & "</DTR>" & vbNewLine
        End If

        Xml = Xml & "</n1:DatiFattura>" & vbNewLine


        REM Xml = ChTesto_AE(Xml)

        Lbl_Errori.Text = "<table>" & Session("ERRORIDATIFATTURE") & "</table>"


        Session("XML") = Xml
        Img_Download.Visible = True
        Img_Excel.Visible = True



        Call EseguiJS()
    End Sub

    Function DatiSocieta() As String
        Dim Xml As String = ""
        Dim M As New Cls_DecodificaSocieta

        M.Leggi(Session("DC_TABELLE"))


        Xml = Xml & "<IdentificativiFiscali>" & vbNewLine
        If M.PartitaIVA > 0 Then
            Xml = Xml & "<IdFiscaleIVA>" & vbNewLine
            Xml = Xml & "<IdPaese>IT</IdPaese>" & vbNewLine
            Xml = Xml & "<IdCodice>" & Format(M.PartitaIVA, "00000000000") & "</IdCodice>" & vbNewLine
            Xml = Xml & "</IdFiscaleIVA>" & vbNewLine
        End If
        If M.CodiceFiscale <> "" Then
            Xml = Xml & "<CodiceFiscale>" & M.CodiceFiscale & "</CodiceFiscale>" & vbNewLine
        End If
        Xml = Xml & "</IdentificativiFiscali>" & vbNewLine
        Xml = Xml & "<AltriDatiIdentificativi>" & vbNewLine
        Xml = Xml & "<Denominazione>" & M.RagioneSociale & "</Denominazione>" & vbNewLine
        Xml = Xml & "<Sede>" & vbNewLine
        Xml = Xml & "<Indirizzo>" & M.Indirizzo & "</Indirizzo>" & vbNewLine
        Xml = Xml & "<CAP>" & M.CAP & "</CAP>" & vbNewLine
        Xml = Xml & "<Comune>" & M.Localita & "</Comune>" & vbNewLine
        Xml = Xml & "<Provincia>" & M.Provincia & "</Provincia>" & vbNewLine
        Xml = Xml & "<Nazione>IT</Nazione>" & vbNewLine
        Xml = Xml & "</Sede>" & vbNewLine
        Xml = Xml & "</AltriDatiIdentificativi>" & vbNewLine

        Return Xml
    End Function

    Function DatiFattura(ByVal Fattura As Cls_MovimentoContabile) As String
        Dim Xml As String = ""

        Dim Causale As New Cls_CausaleContabile

        Causale.Codice = Fattura.CausaleContabile

        Causale.Leggi(Session("DC_TABELLE"), Causale.Codice)

        Dim TotaleImponibile As Double
        Dim TotaleImposta As Double

        Dim TipoRegistro As New Cls_RegistroIVA


        TipoRegistro.Leggi(Session("DC_TABELLE"), Fattura.RegistroIVA)


        Xml = Xml & "<DatiGenerali>" & vbNewLine
        If Causale.TipoDocumento = "NC" Then
            Xml = Xml & "<TipoDocumento>TD04</TipoDocumento>" & vbNewLine
        Else
            Xml = Xml & "<TipoDocumento>TD01</TipoDocumento>" & vbNewLine
        End If
        If Causale.VenditaAcquisti = "A" Then
            Xml = Xml & "<Data>" & Format(Fattura.DataDocumento, "yyyy-MM-dd") & "</Data>" & vbNewLine

            If Format(Fattura.DataDocumento, "yyyyMMdd") > Format(Fattura.DataRegistrazione, "yyyyMMdd") Then
                Session("ERRORIDATIFATTURE") = Session("ERRORIDATIFATTURE") & "<tr class=""miotr"">"
                Session("ERRORIDATIFATTURE") = Session("ERRORIDATIFATTURE") & "<td class=""miacella"">" & Fattura.NumeroRegistrazione & "</td>"
                Session("ERRORIDATIFATTURE") = Session("ERRORIDATIFATTURE") & "<td class=""miacella"">" & "</td>"
                Session("ERRORIDATIFATTURE") = Session("ERRORIDATIFATTURE") & "<td class=""miacella"">Data Documento > Data Registrazione</td>"
                Session("ERRORIDATIFATTURE") = Session("ERRORIDATIFATTURE") & "<td class=""miacella"">" & Fattura.DataDocumento & " > " & Fattura.DataRegistrazione & "</td>"
                Session("ERRORIDATIFATTURE") = Session("ERRORIDATIFATTURE") & "</tr>"
            End If
        Else
            Xml = Xml & "<Data>" & Format(Fattura.DataRegistrazione, "yyyy-MM-dd") & "</Data>" & vbNewLine
        End If

        If Causale.VenditaAcquisti = "A" Then
            If TipoRegistro.IndicatoreRegistro = "" Then
                Xml = Xml & "<Numero>" & Fattura.NumeroDocumento & "</Numero>" & vbNewLine
            Else
                Xml = Xml & "<Numero>" & Fattura.NumeroDocumento & "/" & TipoRegistro.IndicatoreRegistro & "</Numero>" & vbNewLine
            End If
        Else
            If TipoRegistro.IndicatoreRegistro = "" Then
                Xml = Xml & "<Numero>" & Fattura.NumeroProtocollo & "</Numero>" & vbNewLine
            Else
                Xml = Xml & "<Numero>" & Fattura.NumeroProtocollo & "/" & TipoRegistro.IndicatoreRegistro & "</Numero>" & vbNewLine
            End If
        End If

        If Causale.VenditaAcquisti = "A" Then
            Xml = Xml & "<DataRegistrazione>" & Format(Fattura.DataRegistrazione, "yyyy-MM-dd") & "</DataRegistrazione>" & vbNewLine
        End If

        Xml = Xml & "</DatiGenerali>" & vbNewLine

        Dim Indice As Integer
        Dim Imponibile As Double = 0
        Dim Imposta As Double = 0

        For Indice = 0 To Fattura.Righe.Length - 1
            If Not IsNothing(Fattura.Righe(Indice)) Then

                If Fattura.Righe(Indice).Tipo = "IV" And (Fattura.Righe(Indice).Imponibile > 0 Or Fattura.Righe(Indice).Importo > 0) Then

                    Dim negativo As String = ""

                    Dim Aliquota As New Cls_IVA

                    Aliquota.Codice = Fattura.Righe(Indice).CodiceIVA
                    Aliquota.Leggi(Session("DC_TABELLE"), Aliquota.Codice)

                    If Aliquota.InSpesometro = 0 Then

                        If Causale.VenditaAcquisti = "V" Then
                            If Causale.TipoDocumento = "NC" Then
                                If Fattura.Righe(Indice).DareAvere = "A" Then
                                    negativo = "-"
                                End If
                            Else
                                If Fattura.Righe(Indice).DareAvere = "D" Then
                                    negativo = "-"
                                End If
                            End If
                        Else
                            If Causale.TipoDocumento = "NC" Then
                                If Fattura.Righe(Indice).DareAvere = "D" Then
                                    negativo = "-"
                                End If
                            Else
                                If Fattura.Righe(Indice).DareAvere = "A" Then
                                    negativo = "-"
                                End If
                            End If
                        End If

                        Xml = Xml & "<DatiRiepilogo>" & vbNewLine
                        If Math.Round(Fattura.Righe(Indice).Imponibile, 2) = 0 Then
                            Xml = Xml & "<ImponibileImporto>" & Replace(Format(Math.Round(Fattura.Righe(Indice).Imponibile, 2), "0.00"), ",", ".") & "</ImponibileImporto>" & vbNewLine
                        Else
                            Xml = Xml & "<ImponibileImporto>" & negativo & Replace(Format(Math.Round(Fattura.Righe(Indice).Imponibile, 2), "0.00"), ",", ".") & "</ImponibileImporto>" & vbNewLine
                        End If

                        Xml = Xml & "<DatiIVA>" & vbNewLine
                        If Math.Round(Fattura.Righe(Indice).Importo, 2) = 0 Then
                            Xml = Xml & "<Imposta>" & Replace(Format(Math.Round(Fattura.Righe(Indice).Importo, 2), "0.00"), ",", ".") & "</Imposta>" & vbNewLine
                        Else
                            Xml = Xml & "<Imposta>" & negativo & Replace(Format(Math.Round(Fattura.Righe(Indice).Importo, 2), "0.00"), ",", ".") & "</Imposta>" & vbNewLine
                        End If

                        If Causale.TipoDocumento = "NC" Then
                            If negativo = "-" Then
                                TotaleImponibile = TotaleImponibile + Fattura.Righe(Indice).Imponibile
                                TotaleImposta = TotaleImposta + Fattura.Righe(Indice).Importo
                            Else
                                TotaleImponibile = TotaleImponibile - Fattura.Righe(Indice).Imponibile
                                TotaleImposta = TotaleImposta - Fattura.Righe(Indice).Importo
                            End If
                        Else
                            If negativo = "-" Then
                                TotaleImponibile = TotaleImponibile - Fattura.Righe(Indice).Imponibile
                                TotaleImposta = TotaleImposta - Fattura.Righe(Indice).Importo
                            Else
                                TotaleImponibile = TotaleImponibile + Fattura.Righe(Indice).Imponibile
                                TotaleImposta = TotaleImposta + Fattura.Righe(Indice).Importo
                            End If
                        End If



                        Xml = Xml & "<Aliquota>" & Replace(Format(Aliquota.Aliquota * 100, "0.00"), ",", ".") & "</Aliquota>" & vbNewLine
                        'End If                        

                        Xml = Xml & "</DatiIVA>" & vbNewLine
                        If Aliquota.Natura <> "" Then
                            Xml = Xml & "<Natura>" & Aliquota.Natura & "</Natura>" & vbNewLine
                        End If


                        If Aliquota.Aliquota = 0 And Fattura.Righe(Indice).Importo > 0 Then
                            Session("ERRORIDATIFATTURE") = Session("ERRORIDATIFATTURE") & "<tr class=""miotr"">"
                            Session("ERRORIDATIFATTURE") = Session("ERRORIDATIFATTURE") & "<td class=""miacella"">" & Fattura.NumeroRegistrazione & "</td>"
                            Session("ERRORIDATIFATTURE") = Session("ERRORIDATIFATTURE") & "<td class=""miacella"">" & "</td>"
                            Session("ERRORIDATIFATTURE") = Session("ERRORIDATIFATTURE") & "<td class=""miacella"">Aliquota iva a zero ma con imposta</td>"
                            Session("ERRORIDATIFATTURE") = Session("ERRORIDATIFATTURE") & "<td class=""miacella"">" & Aliquota.Codice & "</td>"
                            Session("ERRORIDATIFATTURE") = Session("ERRORIDATIFATTURE") & "</tr>"
                        End If
                        If Aliquota.Natura = "" And Aliquota.Aliquota = 0 Then
                            Session("ERRORIDATIFATTURE") = Session("ERRORIDATIFATTURE") & "<tr class=""miotr"">"
                            Session("ERRORIDATIFATTURE") = Session("ERRORIDATIFATTURE") & "<td class=""miacella"">" & Fattura.NumeroRegistrazione & "</td>"
                            Session("ERRORIDATIFATTURE") = Session("ERRORIDATIFATTURE") & "<td class=""miacella"">" & "</td>"
                            Session("ERRORIDATIFATTURE") = Session("ERRORIDATIFATTURE") & "<td class=""miacella"">Codice iva con aliquota 0 e senza natura</td>"
                            Session("ERRORIDATIFATTURE") = Session("ERRORIDATIFATTURE") & "<td class=""miacella"">" & Aliquota.Codice & "</td>"
                            Session("ERRORIDATIFATTURE") = Session("ERRORIDATIFATTURE") & "</tr>"
                        End If


                        Dim Detraibilita As New ClsDetraibilita

                        If Fattura.Righe(Indice).Detraibile <> "" Then
                            Detraibilita.Codice = Fattura.Righe(Indice).Detraibile
                            Detraibilita.Leggi(Session("DC_TABELLE"))

                            If Detraibilita.Aliquota > 0 Then
                                Xml = Xml & "<Detraibile>" & Replace(Format(Detraibilita.Aliquota * 100, "0.00"), ",", ".") & "</Detraibile>" & vbNewLine
                            End If

                        End If
                        'If Aliquota.Natura <> "N6" Then
                        If Fattura.IVASospesa = "S" Then
                            Xml = Xml & "<EsigibilitaIVA>D</EsigibilitaIVA>" & vbNewLine
                        Else

                            Xml = Xml & "<EsigibilitaIVA>I</EsigibilitaIVA>" & vbNewLine
                        End If
                        'End If
                        Xml = Xml & "</DatiRiepilogo>" & vbNewLine
                    End If
                End If
            End If
        Next


        Dim myriga As System.Data.DataRow = Tabella.NewRow()

        myriga(0) = Session("DENOMINAZIONE")
        myriga(1) = Session("CODICEFISCALE")
        myriga(2) = Session("PIVA")
        myriga(3) = Fattura.DataRegistrazione
        myriga(4) = Fattura.NumeroProtocollo
        myriga(5) = TotaleImponibile
        myriga(6) = TotaleImposta

        Tabella.Rows.Add(myriga)


        Return Xml
    End Function

    Function DatiCedentePrestatore(ByVal Fattura As Cls_MovimentoContabile) As String
        Dim Mastro As Integer
        Dim Conto As Double
        Dim Sottoconto As Double
        Dim Verifiica As New Cls_CodiceFiscale
        Dim Xml As String = ""

        Mastro = Fattura.Righe(0).MastroPartita
        Conto = Fattura.Righe(0).ContoPartita
        Sottoconto = Fattura.Righe(0).SottocontoPartita



        Xml = Xml & "<IdentificativiFiscali>" & vbNewLine

        Dim PC As New Cls_Pianodeiconti

        PC.Mastro = Mastro
        PC.Conto = Conto
        PC.Sottoconto = Sottoconto
        PC.Decodfica(Session("DC_GENERALE"))
        If PC.TipoAnagrafica = "O" Then
            Dim Ospite As New ClsOspite

            Ospite.CodiceOspite = Int(Sottoconto / 100)
            Ospite.Leggi(Session("DC_OSPITE"), Ospite.CodiceOspite)

            If Verifiica.Check_CodiceFiscale(Ospite.CODICEFISCALE) = False Then
                Session("ERRORIDATIFATTURE") = Session("ERRORIDATIFATTURE") & "<tr class=""miotr"">"
                Session("ERRORIDATIFATTURE") = Session("ERRORIDATIFATTURE") & "<td class=""miacella"">" & Fattura.NumeroRegistrazione & "</td>"
                Session("ERRORIDATIFATTURE") = Session("ERRORIDATIFATTURE") & "<td class=""miacella"">" & Mastro & "." & Conto & "." & Sottoconto & "</td>"
                Session("ERRORIDATIFATTURE") = Session("ERRORIDATIFATTURE") & "<td class=""miacella"">Codice Fiscale Errato</td>"
                Session("ERRORIDATIFATTURE") = Session("ERRORIDATIFATTURE") & "<td class=""miacella"">" & Ospite.CODICEFISCALE & "</td>"
                Session("ERRORIDATIFATTURE") = Session("ERRORIDATIFATTURE") & "</tr>"
            End If

            Xml = Xml & "<CodiceFiscale>" & Ospite.CODICEFISCALE & "</CodiceFiscale>" & vbNewLine
            Xml = Xml & "</IdentificativiFiscali>" & vbNewLine


            Xml = Xml & "<AltriDatiIdentificativi>" & vbNewLine
            Xml = Xml & "<Nome>" & Ospite.NomeOspite & "</Nome>" & vbNewLine
            Xml = Xml & "<Cognome>" & Ospite.CognomeOspite & "</Cognome>" & vbNewLine
            Xml = Xml & "<Sede>" & vbNewLine
            Xml = Xml & "<Indirizzo>" & Ospite.RESIDENZAINDIRIZZO1 & "</Indirizzo>" & vbNewLine

            If Trim(Ospite.RESIDENZAINDIRIZZO1) = "" Then
                Session("ERRORIDATIFATTURE") = Session("ERRORIDATIFATTURE") & "<tr class=""miotr"">"
                Session("ERRORIDATIFATTURE") = Session("ERRORIDATIFATTURE") & "<td class=""miacella"">" & Fattura.NumeroRegistrazione & "</td>"
                Session("ERRORIDATIFATTURE") = Session("ERRORIDATIFATTURE") & "<td class=""miacella"">" & Mastro & "." & Conto & "." & Sottoconto & "</td>"
                Session("ERRORIDATIFATTURE") = Session("ERRORIDATIFATTURE") & "<td class=""miacella"">Indirizzo non indicato</td>"
                Session("ERRORIDATIFATTURE") = Session("ERRORIDATIFATTURE") & "<td class=""miacella"">" & Ospite.Nome & "</td>"
                Session("ERRORIDATIFATTURE") = Session("ERRORIDATIFATTURE") & "</tr>"
            End If

            ' Xml = Xml & "<NumeroCivico>1</NumeroCivico>" NUMERO CIVICO ...SARA' OBBLIGATORIO?
            Xml = Xml & "<CAP>" & Ospite.RESIDENZACAP1 & "</CAP>" & vbNewLine

            Session("DENOMINAZIONE") = Ospite.Nome
            Session("CODICEFISCALE") = Ospite.CODICEFISCALE
            Session("PIVA") = 0

            Dim DecodificaCome As New ClsComune
            DecodificaCome.Provincia = Ospite.RESIDENZAPROVINCIA1
            DecodificaCome.Comune = Ospite.RESIDENZACOMUNE1
            DecodificaCome.DecodficaComune(Session("DC_OSPITE"))

            Dim Provincia As New ClsComune
            Provincia.Provincia = Ospite.RESIDENZAPROVINCIA1
            Provincia.Comune = ""
            Provincia.Leggi(Session("DC_OSPITE"))

            If Provincia.CodificaProvincia = "" Then
                Session("ERRORIDATIFATTURE") = Session("ERRORIDATIFATTURE") & "<tr class=""miotr"">"
                Session("ERRORIDATIFATTURE") = Session("ERRORIDATIFATTURE") & "<td class=""miacella"">" & Fattura.NumeroRegistrazione & "</td>"
                Session("ERRORIDATIFATTURE") = Session("ERRORIDATIFATTURE") & "<td class=""miacella"">" & Mastro & "." & Conto & "." & Sottoconto & "</td>"
                Session("ERRORIDATIFATTURE") = Session("ERRORIDATIFATTURE") & "<td class=""miacella"">Provincia non indicata</td>"
                Session("ERRORIDATIFATTURE") = Session("ERRORIDATIFATTURE") & "<td class=""miacella"">" & Ospite.Nome & "</td>"
                Session("ERRORIDATIFATTURE") = Session("ERRORIDATIFATTURE") & "</tr>"
            End If


            If Len(Ospite.RESIDENZACAP1) < 5 Then
                Session("ERRORIDATIFATTURE") = Session("ERRORIDATIFATTURE") & "<tr class=""miotr"">"
                Session("ERRORIDATIFATTURE") = Session("ERRORIDATIFATTURE") & "<td class=""miacella"">" & Fattura.NumeroRegistrazione & "</td>"
                Session("ERRORIDATIFATTURE") = Session("ERRORIDATIFATTURE") & "<td class=""miacella"">" & Mastro & "." & Conto & "." & Sottoconto & "</td>"
                Session("ERRORIDATIFATTURE") = Session("ERRORIDATIFATTURE") & "<td class=""miacella"">Cap non indicato</td>"
                Session("ERRORIDATIFATTURE") = Session("ERRORIDATIFATTURE") & "<td class=""miacella"">" & Ospite.Nome & "</td>"
                Session("ERRORIDATIFATTURE") = Session("ERRORIDATIFATTURE") & "</tr>"
            End If

            Xml = Xml & "<Comune>" & DecodificaCome.Descrizione & "</Comune>" & vbNewLine
            Xml = Xml & "<Provincia>" & Provincia.CodificaProvincia & "</Provincia>" & vbNewLine
            Xml = Xml & "<Nazione>IT</Nazione>" & vbNewLine
            Xml = Xml & "</Sede>" & vbNewLine
            Xml = Xml & "</AltriDatiIdentificativi>" & vbNewLine
        End If
        If PC.TipoAnagrafica = "P" Then
            Dim Parente As New Cls_Parenti

            Parente.CodiceOspite = Int(Sottoconto / 100)
            Parente.CodiceParente = Sottoconto - (Parente.CodiceOspite * 100)
            Parente.Leggi(Session("DC_OSPITE"), Parente.CodiceOspite, Parente.CodiceParente)

            If Verifiica.Check_CodiceFiscale(Parente.CODICEFISCALE) = False Then
                Session("ERRORIDATIFATTURE") = Session("ERRORIDATIFATTURE") & "<tr class=""miotr"">"
                Session("ERRORIDATIFATTURE") = Session("ERRORIDATIFATTURE") & "<td class=""miacella"">" & Fattura.NumeroRegistrazione & "</td>"
                Session("ERRORIDATIFATTURE") = Session("ERRORIDATIFATTURE") & "<td class=""miacella"">" & Mastro & "." & Conto & "." & Sottoconto & "</td>"
                Session("ERRORIDATIFATTURE") = Session("ERRORIDATIFATTURE") & "<td class=""miacella"">Codice Fiscale Errato</td>"
                Session("ERRORIDATIFATTURE") = Session("ERRORIDATIFATTURE") & "<td class=""miacella"">" & Parente.CODICEFISCALE & "</td>"
                Session("ERRORIDATIFATTURE") = Session("ERRORIDATIFATTURE") & "</tr>"
            End If

            Xml = Xml & "<CodiceFiscale>" & Parente.CODICEFISCALE & "</CodiceFiscale>" & vbNewLine
            Xml = Xml & "</IdentificativiFiscali>" & vbNewLine
            Xml = Xml & "<AltriDatiIdentificativi>" & vbNewLine
            Xml = Xml & "<Nome>" & Parente.NomeParente & "</Nome>" & vbNewLine
            Xml = Xml & "<Cognome>" & Parente.CognomeParente & "</Cognome>" & vbNewLine
            Xml = Xml & "<Sede>" & vbNewLine
            Xml = Xml & "<Indirizzo>" & Parente.RESIDENZAINDIRIZZO1 & "</Indirizzo>" & vbNewLine
            ' Xml = Xml & "<NumeroCivico>1</NumeroCivico>" NUMERO CIVICO ...SARA' OBBLIGATORIO?
            Xml = Xml & "<CAP>" & Parente.RESIDENZACAP1 & "</CAP>" & vbNewLine

            If Trim(Parente.RESIDENZAINDIRIZZO1) = "" Then
                Session("ERRORIDATIFATTURE") = Session("ERRORIDATIFATTURE") & "<tr class=""miotr"">"
                Session("ERRORIDATIFATTURE") = Session("ERRORIDATIFATTURE") & "<td class=""miacella"">" & Fattura.NumeroRegistrazione & "</td>"
                Session("ERRORIDATIFATTURE") = Session("ERRORIDATIFATTURE") & "<td class=""miacella"">" & Mastro & "." & Conto & "." & Sottoconto & "</td>"
                Session("ERRORIDATIFATTURE") = Session("ERRORIDATIFATTURE") & "<td class=""miacella"">Residenza indirizzo non indicato</td>"
                Session("ERRORIDATIFATTURE") = Session("ERRORIDATIFATTURE") & "<td class=""miacella"">" & Parente.Nome & "</td>"
                Session("ERRORIDATIFATTURE") = Session("ERRORIDATIFATTURE") & "</tr>"
            End If

            If Len(Parente.RESIDENZACAP1) < 5 Then
                Session("ERRORIDATIFATTURE") = Session("ERRORIDATIFATTURE") & "<tr class=""miotr"">"
                Session("ERRORIDATIFATTURE") = Session("ERRORIDATIFATTURE") & "<td class=""miacella"">" & Fattura.NumeroRegistrazione & "</td>"
                Session("ERRORIDATIFATTURE") = Session("ERRORIDATIFATTURE") & "<td class=""miacella"">" & Mastro & "." & Conto & "." & Sottoconto & "</td>"
                Session("ERRORIDATIFATTURE") = Session("ERRORIDATIFATTURE") & "<td class=""miacella"">Cap non indicato</td>"
                Session("ERRORIDATIFATTURE") = Session("ERRORIDATIFATTURE") & "<td class=""miacella"">" & Parente.Nome & "</td>"
                Session("ERRORIDATIFATTURE") = Session("ERRORIDATIFATTURE") & "</tr>"
            End If

            Session("DENOMINAZIONE") = Parente.Nome
            Session("CODICEFISCALE") = Parente.CODICEFISCALE
            Session("PIVA") = 0

            Dim DecodificaCome As New ClsComune
            DecodificaCome.Provincia = Parente.RESIDENZAPROVINCIA1
            DecodificaCome.Comune = Parente.RESIDENZACOMUNE1
            DecodificaCome.DecodficaComune(Session("DC_OSPITE"))

            Dim Provincia As New ClsComune
            Provincia.Provincia = Parente.RESIDENZAPROVINCIA1
            Provincia.Comune = ""
            Provincia.Leggi(Session("DC_OSPITE"))

            If Provincia.CodificaProvincia = "" Then
                Session("ERRORIDATIFATTURE") = Session("ERRORIDATIFATTURE") & "<tr class=""miotr"">"
                Session("ERRORIDATIFATTURE") = Session("ERRORIDATIFATTURE") & "<td class=""miacella"">" & Fattura.NumeroRegistrazione & "</td>"
                Session("ERRORIDATIFATTURE") = Session("ERRORIDATIFATTURE") & "<td class=""miacella"">" & Mastro & "." & Conto & "." & Sottoconto & "</td>"
                Session("ERRORIDATIFATTURE") = Session("ERRORIDATIFATTURE") & "<td class=""miacella"">Provincia non indicata</td>"
                Session("ERRORIDATIFATTURE") = Session("ERRORIDATIFATTURE") & "<td class=""miacella"">" & Parente.Nome & "</td>"
                Session("ERRORIDATIFATTURE") = Session("ERRORIDATIFATTURE") & "</tr>"
            End If


            Xml = Xml & "<Comune>" & DecodificaCome.Descrizione & "</Comune>" & vbNewLine
            Xml = Xml & "<Provincia>" & Provincia.CodificaProvincia & "</Provincia>" & vbNewLine
            Xml = Xml & "<Nazione>IT</Nazione>" & vbNewLine
            Xml = Xml & "</Sede>" & vbNewLine
            Xml = Xml & "</AltriDatiIdentificativi>" & vbNewLine

        End If
        If PC.TipoAnagrafica = "C" Then
            If Mid(Fattura.Tipologia & Space(1), 1, 1) = "C" Or Mid(Fattura.Tipologia & Space(1), 1, 1) = "J" Then
                Dim Comune As New ClsComune

                Comune.Provincia = Mid(Fattura.Tipologia & Space(7), 2, 3)
                Comune.Comune = Mid(Fattura.Tipologia & Space(7), 5, 3)
                Comune.Leggi(Session("DC_OSPITE"))

                If Comune.CodiceFiscale <> "" Then
                    If Verifiica.Check_CodiceFiscale(Comune.CodiceFiscale) = False Then
                        Session("ERRORIDATIFATTURE") = Session("ERRORIDATIFATTURE") & "<tr class=""miotr"">"
                        Session("ERRORIDATIFATTURE") = Session("ERRORIDATIFATTURE") & "<td class=""miacella"">" & Fattura.NumeroRegistrazione & "</td>"
                        Session("ERRORIDATIFATTURE") = Session("ERRORIDATIFATTURE") & "<td class=""miacella"">" & Mastro & "." & Conto & "." & Sottoconto & "</td>"
                        Session("ERRORIDATIFATTURE") = Session("ERRORIDATIFATTURE") & "<td class=""miacella"">Codice Fiscale Errato</td>"
                        Session("ERRORIDATIFATTURE") = Session("ERRORIDATIFATTURE") & "<td class=""miacella"">" & Comune.CodiceFiscale & "</td>"
                        Session("ERRORIDATIFATTURE") = Session("ERRORIDATIFATTURE") & "</tr>"
                    End If
                    Xml = Xml & "<CodiceFiscale>" & Comune.CodiceFiscale & "</CodiceFiscale>" & vbNewLine
                Else
                    If Verifiica.CheckPartitaIva(Format(Comune.PartitaIVA, "00000000000")) = False Then
                        Session("ERRORIDATIFATTURE") = Session("ERRORIDATIFATTURE") & "<tr class=""miotr"">"
                        Session("ERRORIDATIFATTURE") = Session("ERRORIDATIFATTURE") & "<td class=""miacella"">" & Fattura.NumeroRegistrazione & "</td>"
                        Session("ERRORIDATIFATTURE") = Session("ERRORIDATIFATTURE") & "<td class=""miacella"">" & Mastro & "." & Conto & "." & Sottoconto & "</td>"
                        Session("ERRORIDATIFATTURE") = Session("ERRORIDATIFATTURE") & "<td class=""miacella"">Partita Iva Errata</td>"
                        Session("ERRORIDATIFATTURE") = Session("ERRORIDATIFATTURE") & "<td class=""miacella"">" & Format(Comune.PartitaIVA, "00000000000") & "</td>"
                        Session("ERRORIDATIFATTURE") = Session("ERRORIDATIFATTURE") & "</tr>"
                    Else
                        If Chk_PartitaIva.Checked = True Then
                            If Verifiica.VerificaPIVA(Format(Val(Comune.PartitaIVA), "00000000000")) = False Then
                                Session("ERRORIDATIFATTURE") = Session("ERRORIDATIFATTURE") & "<tr class=""miotr"">"
                                Session("ERRORIDATIFATTURE") = Session("ERRORIDATIFATTURE") & "<td class=""miacella"">" & Fattura.NumeroRegistrazione & "</td>"
                                Session("ERRORIDATIFATTURE") = Session("ERRORIDATIFATTURE") & "<td class=""miacella"">" & Mastro & "." & Conto & "." & Sottoconto & "</td>"
                                Session("ERRORIDATIFATTURE") = Session("ERRORIDATIFATTURE") & "<td class=""miacella"">Partita Iva Errata</td>"
                                Session("ERRORIDATIFATTURE") = Session("ERRORIDATIFATTURE") & "<td class=""miacella"">" & Format(Val(Comune.PartitaIVA), "00000000000") & "</td>"
                                Session("ERRORIDATIFATTURE") = Session("ERRORIDATIFATTURE") & "</tr>"
                            End If
                        End If
                    End If
                    Xml = Xml & "<IdFiscaleIVA>" & vbNewLine
                    Xml = Xml & "<IdPaese>IT</IdPaese>" & vbNewLine
                    Xml = Xml & "<IdCodice>" & Format(Comune.PartitaIVA, "00000000000") & "</IdCodice>" & vbNewLine
                    Xml = Xml & "</IdFiscaleIVA>" & vbNewLine
                End If




                If Len(Comune.RESIDENZACAP1) < 5 Then
                    Session("ERRORIDATIFATTURE") = Session("ERRORIDATIFATTURE") & "<tr class=""miotr"">"
                    Session("ERRORIDATIFATTURE") = Session("ERRORIDATIFATTURE") & "<td class=""miacella"">" & Fattura.NumeroRegistrazione & "</td>"
                    Session("ERRORIDATIFATTURE") = Session("ERRORIDATIFATTURE") & "<td class=""miacella"">" & Mastro & "." & Conto & "." & Sottoconto & "</td>"
                    Session("ERRORIDATIFATTURE") = Session("ERRORIDATIFATTURE") & "<td class=""miacella"">Cap non indicato</td>"
                    Session("ERRORIDATIFATTURE") = Session("ERRORIDATIFATTURE") & "<td class=""miacella""></td>"
                    Session("ERRORIDATIFATTURE") = Session("ERRORIDATIFATTURE") & "</tr>"
                End If


                Session("DENOMINAZIONE") = Comune.Descrizione
                Session("CODICEFISCALE") = Comune.CodiceFiscale
                Session("PIVA") = Comune.PartitaIVA

                Xml = Xml & "</IdentificativiFiscali>" & vbNewLine

                Xml = Xml & "<AltriDatiIdentificativi>" & vbNewLine
                Xml = Xml & "<Denominazione>" & Comune.Descrizione & "</Denominazione>" & vbNewLine
                Xml = Xml & "<Sede>" & vbNewLine
                Xml = Xml & "<Indirizzo>" & Comune.RESIDENZAINDIRIZZO1 & "</Indirizzo>" & vbNewLine
                ' Xml = Xml & "<NumeroCivico>1</NumeroCivico>" NUMERO CIVICO ...SARA' OBBLIGATORIO?
                Xml = Xml & "<CAP>" & Comune.RESIDENZACAP1 & "</CAP>" & vbNewLine

                Dim DecodificaCome As New ClsComune
                DecodificaCome.Provincia = Comune.RESIDENZAPROVINCIA1
                DecodificaCome.Comune = Comune.RESIDENZACOMUNE1
                DecodificaCome.DecodficaComune(Session("DC_OSPITE"))

                Dim Provincia As New ClsComune
                Provincia.Provincia = Comune.RESIDENZAPROVINCIA1
                Provincia.Comune = ""
                Provincia.Leggi(Session("DC_OSPITE"))

                Xml = Xml & "<Comune>" & DecodificaCome.Descrizione & "</Comune>" & vbNewLine
                Xml = Xml & "<Provincia>" & Provincia.CodificaProvincia & "</Provincia>" & vbNewLine
                Xml = Xml & "<Nazione>IT</Nazione>" & vbNewLine
                Xml = Xml & "</Sede>" & vbNewLine
                Xml = Xml & "</AltriDatiIdentificativi>" & vbNewLine
            Else
                Session("ERRORIDATIFATTURE") = Session("ERRORIDATIFATTURE") & "<tr class=""miotr"">"
                Session("ERRORIDATIFATTURE") = Session("ERRORIDATIFATTURE") & "<td class=""miacella"">" & Fattura.NumeroRegistrazione & "</td>"
                Session("ERRORIDATIFATTURE") = Session("ERRORIDATIFATTURE") & "<td class=""miacella"">" & Mastro & "." & Conto & "." & Sottoconto & "</td>"
                Session("ERRORIDATIFATTURE") = Session("ERRORIDATIFATTURE") & "<td class=""miacella"">Registrazione Comune senza indicato il comune</td>"
                Session("ERRORIDATIFATTURE") = Session("ERRORIDATIFATTURE") & "<td class=""miacella""></td>"
                Session("ERRORIDATIFATTURE") = Session("ERRORIDATIFATTURE") & "</tr>"
            End If
        End If
        If PC.TipoAnagrafica = "R" Then
            If Mid(Fattura.Tipologia & Space(1), 1, 1) = "R" Then
                Dim Regione As New ClsUSL

                Regione.CodiceRegione = Mid(Fattura.Tipologia & Space(7), 2, 4)
                Regione.Leggi(Session("DC_OSPITE"))

                If Regione.CodiceFiscale <> "" Then
                    If Verifiica.Check_CodiceFiscale(Regione.CodiceFiscale) = False Then
                        Session("ERRORIDATIFATTURE") = Session("ERRORIDATIFATTURE") & "<tr class=""miotr"">"
                        Session("ERRORIDATIFATTURE") = Session("ERRORIDATIFATTURE") & "<td class=""miacella"">" & Fattura.NumeroRegistrazione & "</td>"
                        Session("ERRORIDATIFATTURE") = Session("ERRORIDATIFATTURE") & "<td class=""miacella"">" & Mastro & "." & Conto & "." & Sottoconto & "</td>"
                        Session("ERRORIDATIFATTURE") = Session("ERRORIDATIFATTURE") & "<td class=""miacella"">Codice Fiscale Errato</td>"
                        Session("ERRORIDATIFATTURE") = Session("ERRORIDATIFATTURE") & "<td class=""miacella"">" & Regione.CodiceFiscale & "</td>"
                        Session("ERRORIDATIFATTURE") = Session("ERRORIDATIFATTURE") & "</tr>"
                    End If
                    Xml = Xml & "<CodiceFiscale>" & Regione.CodiceFiscale & "</CodiceFiscale>" & vbNewLine
                Else
                    If Verifiica.CheckPartitaIva(Regione.PARTITAIVA) = False Then
                        Session("ERRORIDATIFATTURE") = Session("ERRORIDATIFATTURE") & "<tr class=""miotr"">"
                        Session("ERRORIDATIFATTURE") = Session("ERRORIDATIFATTURE") & "<td class=""miacella"">" & Fattura.NumeroRegistrazione & "</td>"
                        Session("ERRORIDATIFATTURE") = Session("ERRORIDATIFATTURE") & "<td class=""miacella"">" & Mastro & "." & Conto & "." & Sottoconto & "</td>"
                        Session("ERRORIDATIFATTURE") = Session("ERRORIDATIFATTURE") & "<td class=""miacella"">Partita Iva Errata</td>"
                        Session("ERRORIDATIFATTURE") = Session("ERRORIDATIFATTURE") & "<td class=""miacella"">" & Regione.PARTITAIVA & "</td>"
                        Session("ERRORIDATIFATTURE") = Session("ERRORIDATIFATTURE") & "</tr>"
                    End If
                    Xml = Xml & "<IdFiscaleIVA>" & vbNewLine
                    Xml = Xml & "<IdPaese>IT</IdPaese>" & vbNewLine
                    Xml = Xml & "<IdCodice>" & Format(Regione.PARTITAIVA, "00000000000") & "</IdCodice>" & vbNewLine
                    Xml = Xml & "</IdFiscaleIVA>" & vbNewLine
                End If

                Xml = Xml & "</IdentificativiFiscali>" & vbNewLine


                Session("DENOMINAZIONE") = Regione.Nome
                Session("CODICEFISCALE") = Regione.CodiceFiscale
                Session("PIVA") = Regione.PARTITAIVA


                If Len(Regione.RESIDENZACAP1) < 5 Then
                    Session("ERRORIDATIFATTURE") = Session("ERRORIDATIFATTURE") & "<tr class=""miotr"">"
                    Session("ERRORIDATIFATTURE") = Session("ERRORIDATIFATTURE") & "<td class=""miacella"">" & Fattura.NumeroRegistrazione & "</td>"
                    Session("ERRORIDATIFATTURE") = Session("ERRORIDATIFATTURE") & "<td class=""miacella"">" & Mastro & "." & Conto & "." & Sottoconto & "</td>"
                    Session("ERRORIDATIFATTURE") = Session("ERRORIDATIFATTURE") & "<td class=""miacella"">Cap non indicato</td>"
                    Session("ERRORIDATIFATTURE") = Session("ERRORIDATIFATTURE") & "<td class=""miacella""></td>"
                    Session("ERRORIDATIFATTURE") = Session("ERRORIDATIFATTURE") & "</tr>"
                End If




                Xml = Xml & "<AltriDatiIdentificativi>" & vbNewLine
                Xml = Xml & "<Denominazione>" & Regione.Nome & "</Denominazione>" & vbNewLine
                Xml = Xml & "<Sede>" & vbNewLine
                Xml = Xml & "<Indirizzo>" & Regione.RESIDENZAINDIRIZZO1 & "</Indirizzo>" & vbNewLine
                ' Xml = Xml & "<NumeroCivico>1</NumeroCivico>" NUMERO CIVICO ...SARA' OBBLIGATORIO?
                Xml = Xml & "<CAP>" & Regione.RESIDENZACAP1 & "</CAP>" & vbNewLine

                Dim DecodificaCome As New ClsComune
                DecodificaCome.Provincia = Regione.RESIDENZAPROVINCIA1
                DecodificaCome.Comune = Regione.RESIDENZACOMUNE1
                DecodificaCome.DecodficaComune(Session("DC_OSPITE"))

                Dim Provincia As New ClsComune
                Provincia.Provincia = Regione.RESIDENZAPROVINCIA1
                Provincia.Comune = ""
                Provincia.Leggi(Session("DC_OSPITE"))

                Xml = Xml & "<Comune>" & DecodificaCome.Descrizione & "</Comune>" & vbNewLine
                Xml = Xml & "<Provincia>" & Provincia.CodificaProvincia & "</Provincia>" & vbNewLine
                Xml = Xml & "<Nazione>IT</Nazione>" & vbNewLine
                Xml = Xml & "</Sede>" & vbNewLine
                Xml = Xml & "</AltriDatiIdentificativi>" & vbNewLine
            Else
                Session("ERRORIDATIFATTURE") = Session("ERRORIDATIFATTURE") & "<tr class=""miotr"">"
                Session("ERRORIDATIFATTURE") = Session("ERRORIDATIFATTURE") & "<td class=""miacella"">" & Fattura.NumeroRegistrazione & "</td>"
                Session("ERRORIDATIFATTURE") = Session("ERRORIDATIFATTURE") & "<td class=""miacella"">" & Mastro & "." & Conto & "." & Sottoconto & "</td>"
                Session("ERRORIDATIFATTURE") = Session("ERRORIDATIFATTURE") & "<td class=""miacella"">Registrazione Regione senza indicato il Regione</td>"
                Session("ERRORIDATIFATTURE") = Session("ERRORIDATIFATTURE") & "<td class=""miacella""></td>"
                Session("ERRORIDATIFATTURE") = Session("ERRORIDATIFATTURE") & "</tr>"
            End If
        End If

        If PC.TipoAnagrafica = "D" Then

            Dim cn As New OleDbConnection

            cn = New Data.OleDb.OleDbConnection(Session("DC_OSPITE"))

            cn.Open()

            Dim CliFor As New Cls_ClienteFornitore

            Dim Causale As New Cls_CausaleContabile

            Causale.Codice = Fattura.CausaleContabile

            Causale.Leggi(Session("DC_TABELLE"), Causale.Codice)
            If Causale.VenditaAcquisti = "V" Then
                Dim cmdTipologiaCF As New OleDbCommand

                cmdTipologiaCF.CommandText = "Select  * From AnagraficaComune Where MastroCliente = " & Mastro & " And ContoCliente = " & Conto & " And SottocontoCliente  = " & Sottoconto
                cmdTipologiaCF.Connection = cn
                Dim ReadCliFor As OleDbDataReader = cmdTipologiaCF.ExecuteReader()
                If ReadCliFor.Read() Then
                    CliFor.CODICEDEBITORECREDITORE = campodb(ReadCliFor.Item("CODICEDEBITORECREDITORE"))
                End If
                ReadCliFor.Close()
            Else
                Dim cmdTipologiaCF As New OleDbCommand

                cmdTipologiaCF.CommandText = "Select  * From AnagraficaComune Where MastroFornitore = " & Mastro & " And ContoFornitore = " & Conto & " And SottocontoFornitore = " & Sottoconto
                cmdTipologiaCF.Connection = cn
                Dim ReadCliFor As OleDbDataReader = cmdTipologiaCF.ExecuteReader()
                If ReadCliFor.Read() Then
                    CliFor.CODICEDEBITORECREDITORE = campodb(ReadCliFor.Item("CODICEDEBITORECREDITORE"))
                End If
                ReadCliFor.Close()
            End If
            If CliFor.CODICEDEBITORECREDITORE = 0 Then
                Session("ERRORIDATIFATTURE") = Session("ERRORIDATIFATTURE") & "<tr class=""miotr"">"
                Session("ERRORIDATIFATTURE") = Session("ERRORIDATIFATTURE") & "<td class=""miacella"">" & Fattura.NumeroRegistrazione & "</td>"
                Session("ERRORIDATIFATTURE") = Session("ERRORIDATIFATTURE") & "<td class=""miacella"">" & Mastro & "." & Conto & "." & Sottoconto & "</td>"
                If Causale.VenditaAcquisti = "V" Then
                    Session("ERRORIDATIFATTURE") = Session("ERRORIDATIFATTURE") & "<td class=""miacella"">Cliente non trovato</td>"
                Else
                    Session("ERRORIDATIFATTURE") = Session("ERRORIDATIFATTURE") & "<td class=""miacella"">Fornitore non trovato</td>"
                End If
                Session("ERRORIDATIFATTURE") = Session("ERRORIDATIFATTURE") & "<td class=""miacella""></td>"
                Session("ERRORIDATIFATTURE") = Session("ERRORIDATIFATTURE") & "</tr>"
            End If


            CliFor.Leggi(Session("DC_OSPITE"))

            If CliFor.PARTITAIVA = 0 Then
                If Verifiica.Check_CodiceFiscale(CliFor.CodiceFiscale) = False Then
                    Session("ERRORIDATIFATTURE") = Session("ERRORIDATIFATTURE") & "<tr class=""miotr"">"
                    Session("ERRORIDATIFATTURE") = Session("ERRORIDATIFATTURE") & "<td class=""miacella"">" & Fattura.NumeroRegistrazione & "</td>"
                    Session("ERRORIDATIFATTURE") = Session("ERRORIDATIFATTURE") & "<td class=""miacella"">" & Mastro & "." & Conto & "." & Sottoconto & "</td>"
                    Session("ERRORIDATIFATTURE") = Session("ERRORIDATIFATTURE") & "<td class=""miacella"">Codice Fiscale Errato</td>"
                    Session("ERRORIDATIFATTURE") = Session("ERRORIDATIFATTURE") & "<td class=""miacella"">" & CliFor.CodiceFiscale & "</td>"
                    Session("ERRORIDATIFATTURE") = Session("ERRORIDATIFATTURE") & "</tr>"
                End If
                Xml = Xml & "<CodiceFiscale>" & CliFor.CodiceFiscale & "</CodiceFiscale>" & vbNewLine
            Else
                If Verifiica.CheckPartitaIva(Format(Val(CliFor.PARTITAIVA), "00000000000")) = False Then
                    Session("ERRORIDATIFATTURE") = Session("ERRORIDATIFATTURE") & "<tr class=""miotr"">"
                    Session("ERRORIDATIFATTURE") = Session("ERRORIDATIFATTURE") & "<td class=""miacella"">" & Fattura.NumeroRegistrazione & "</td>"
                    Session("ERRORIDATIFATTURE") = Session("ERRORIDATIFATTURE") & "<td class=""miacella"">" & Mastro & "." & Conto & "." & Sottoconto & "</td>"
                    Session("ERRORIDATIFATTURE") = Session("ERRORIDATIFATTURE") & "<td class=""miacella"">Partita Iva Errata</td>"
                    Session("ERRORIDATIFATTURE") = Session("ERRORIDATIFATTURE") & "<td class=""miacella"">" & Format(Val(CliFor.PARTITAIVA), "00000000000") & "</td>"
                    Session("ERRORIDATIFATTURE") = Session("ERRORIDATIFATTURE") & "</tr>"
                Else
                    If Chk_PartitaIva.Checked = True Then
                        If Verifiica.VerificaPIVA(Format(Val(CliFor.PARTITAIVA), "00000000000")) = False Then
                            Session("ERRORIDATIFATTURE") = Session("ERRORIDATIFATTURE") & "<tr class=""miotr"">"
                            Session("ERRORIDATIFATTURE") = Session("ERRORIDATIFATTURE") & "<td class=""miacella"">" & Fattura.NumeroRegistrazione & "</td>"
                            Session("ERRORIDATIFATTURE") = Session("ERRORIDATIFATTURE") & "<td class=""miacella"">" & Mastro & "." & Conto & "." & Sottoconto & "</td>"
                            Session("ERRORIDATIFATTURE") = Session("ERRORIDATIFATTURE") & "<td class=""miacella"">Partita Non Trovata In Servizio</td>"
                            Session("ERRORIDATIFATTURE") = Session("ERRORIDATIFATTURE") & "<td class=""miacella"">" & Format(Val(CliFor.PARTITAIVA), "00000000000") & "</td>"
                            Session("ERRORIDATIFATTURE") = Session("ERRORIDATIFATTURE") & "</tr>"
                        Else
                            If Chk_PartitaIva.Checked = True Then
                                CliFor.PARTITAIVA = CliFor.PARTITAIVA
                            End If
                        End If
                    End If
                End If
                Xml = Xml & "<IdFiscaleIVA>" & vbNewLine
                Xml = Xml & "<IdPaese>IT</IdPaese>" & vbNewLine
                Xml = Xml & "<IdCodice>" & Format(Val(CliFor.PARTITAIVA), "00000000000") & "</IdCodice>" & vbNewLine
                Xml = Xml & "</IdFiscaleIVA>" & vbNewLine
            End If

            Xml = Xml & "</IdentificativiFiscali>" & vbNewLine


            If Len(CliFor.RESIDENZACAP1) < 5 Then
                Session("ERRORIDATIFATTURE") = Session("ERRORIDATIFATTURE") & "<tr class=""miotr"">"
                Session("ERRORIDATIFATTURE") = Session("ERRORIDATIFATTURE") & "<td class=""miacella"">" & Fattura.NumeroRegistrazione & "</td>"
                Session("ERRORIDATIFATTURE") = Session("ERRORIDATIFATTURE") & "<td class=""miacella"">" & Mastro & "." & Conto & "." & Sottoconto & "</td>"
                Session("ERRORIDATIFATTURE") = Session("ERRORIDATIFATTURE") & "<td class=""miacella"">Cap non indicato</td>"
                Session("ERRORIDATIFATTURE") = Session("ERRORIDATIFATTURE") & "<td class=""miacella"">" & CliFor.Nome & "</td>"
                Session("ERRORIDATIFATTURE") = Session("ERRORIDATIFATTURE") & "</tr>"
            End If


            Session("DENOMINAZIONE") = CliFor.Nome
            Session("CODICEFISCALE") = CliFor.CodiceFiscale
            Session("PIVA") = CliFor.PARTITAIVA

            Xml = Xml & "<AltriDatiIdentificativi>" & vbNewLine

            If Val(CliFor.PARTITAIVA) = 0 And CliFor.Nome.IndexOf(" ") > 0 And CliFor.FisicaGiuridica = "F" Then
                Xml = Xml & "<Nome>" & ChTesto_AE(Mid(CliFor.Nome, 1, CliFor.Nome.IndexOf(" "))) & "</Nome>" & vbNewLine
                Xml = Xml & "<Cognome>" & ChTesto_AE(Mid(CliFor.Nome, CliFor.Nome.IndexOf(" ") + 1, Len(CliFor.Nome) - CliFor.Nome.IndexOf(" ") + 1)) & "</Cognome>" & vbNewLine
            Else
                Xml = Xml & "<Denominazione>" & ChTesto_AE(CliFor.Nome) & "</Denominazione>" & vbNewLine
            End If

            Xml = Xml & "<Sede>" & vbNewLine
            Xml = Xml & "<Indirizzo>" & ChTesto_AE(CliFor.RESIDENZAINDIRIZZO1) & "</Indirizzo>" & vbNewLine
            ' Xml = Xml & "<NumeroCivico>1</NumeroCivico>" NUMERO CIVICO ...SARA' OBBLIGATORIO?
            Xml = Xml & "<CAP>" & CliFor.RESIDENZACAP1 & "</CAP>" & vbNewLine

            Dim DecodificaCome As New ClsComune
            DecodificaCome.Provincia = CliFor.RESIDENZAPROVINCIA1
            DecodificaCome.Comune = CliFor.RESIDENZACOMUNE1
            DecodificaCome.Leggi(Session("DC_OSPITE"))


            Dim Provincia As New ClsComune
            Provincia.Provincia = CliFor.RESIDENZAPROVINCIA1
            Provincia.Comune = ""
            Provincia.Leggi(Session("DC_OSPITE"))

            'CliFor.RESIDENZAINDIRIZZO1
            If Trim(CliFor.RESIDENZAINDIRIZZO1) = "" Then
                Session("ERRORIDATIFATTURE") = Session("ERRORIDATIFATTURE") & "<tr class=""miotr"">"
                Session("ERRORIDATIFATTURE") = Session("ERRORIDATIFATTURE") & "<td class=""miacella"">" & Fattura.NumeroRegistrazione & "</td>"
                Session("ERRORIDATIFATTURE") = Session("ERRORIDATIFATTURE") & "<td class=""miacella"">" & Mastro & "." & Conto & "." & Sottoconto & "</td>"
                Session("ERRORIDATIFATTURE") = Session("ERRORIDATIFATTURE") & "<td class=""miacella"">Indirizzo Residenza Non Indicato</td>"
                Session("ERRORIDATIFATTURE") = Session("ERRORIDATIFATTURE") & "<td class=""miacella"">" & CliFor.Nome & "</td>"
                Session("ERRORIDATIFATTURE") = Session("ERRORIDATIFATTURE") & "</tr>"
            End If

            If Trim(Provincia.CodificaProvincia) = "" Then
                Session("ERRORIDATIFATTURE") = Session("ERRORIDATIFATTURE") & "<tr class=""miotr"">"
                Session("ERRORIDATIFATTURE") = Session("ERRORIDATIFATTURE") & "<td class=""miacella"">" & Fattura.NumeroRegistrazione & "</td>"
                Session("ERRORIDATIFATTURE") = Session("ERRORIDATIFATTURE") & "<td class=""miacella"">" & Mastro & "." & Conto & "." & Sottoconto & "</td>"
                Session("ERRORIDATIFATTURE") = Session("ERRORIDATIFATTURE") & "<td class=""miacella"">Provincia non indicata</td>"
                Session("ERRORIDATIFATTURE") = Session("ERRORIDATIFATTURE") & "<td class=""miacella"">" & CliFor.Nome & "</td>"
                Session("ERRORIDATIFATTURE") = Session("ERRORIDATIFATTURE") & "</tr>"
            End If


            If DecodificaCome.Provincia = "999" And Mid(DecodificaCome.CODXCODF & Space(2), 1, 1) = "Z" Then
                Xml = Xml & "<Comune>" & DecodificaCome.Descrizione & "</Comune>" & vbNewLine
                Xml = Xml & "<Nazione>" & DecodificaCome.CodificaProvincia & "</Nazione>" & vbNewLine
            Else
                Xml = Xml & "<Comune>" & DecodificaCome.Descrizione & "</Comune>" & vbNewLine
                Xml = Xml & "<Provincia>" & Provincia.CodificaProvincia & "</Provincia>" & vbNewLine
                Xml = Xml & "<Nazione>IT</Nazione>" & vbNewLine
            End If
            
            Xml = Xml & "</Sede>" & vbNewLine
            Xml = Xml & "</AltriDatiIdentificativi>" & vbNewLine

            cn.Close()
        End If

        Return Xml
    End Function

    Function campodb(ByVal oggetto As Object) As String
        If IsDBNull(oggetto) Then
            Return ""
        Else
            Return oggetto
        End If
    End Function





    Private Function TotaleImponibileVerifica(ByVal NumeroRegistrazione As Long) As Double
        Dim CausaleContabile As String
        Dim TipoDoc As String
        Dim I As Integer
        Dim Registrazione As New Cls_MovimentoContabile


        If NumeroRegistrazione = 76125 Then
            NumeroRegistrazione = 76125
        End If

        Registrazione.Leggi(Session("DC_GENERALE"), NumeroRegistrazione)

        CausaleContabile = Registrazione.CausaleContabile
        Dim CausCont As New Cls_CausaleContabile

        CausCont.Leggi(Session("DC_TABELLE"), CausaleContabile)
        TipoDoc = CausCont.TipoDocumento
        TotaleImponibileVerifica = 0


        For I = 0 To Registrazione.Righe.Length - 1
            If Not IsNothing(Registrazione.Righe(I)) Then
                If Registrazione.Righe(I).Tipo = "IV" Then
                    If Registrazione.Righe(I).CodiceIVA <> "" Then
                        Dim TipoIva As New Cls_IVA

                        TipoIva.Leggi(Session("DC_TABELLE"), Registrazione.Righe(I).CodiceIVA)
                        If TipoIva.InSpesometro <> 1 Then
                            If CausCont.VenditaAcquisti = "V" Then
                                If Registrazione.Righe(I).DareAvere = "D" Then
                                    If Registrazione.Righe(I).Segno <> "-" And TipoDoc <> "NC" Then
                                        TotaleImponibileVerifica = TotaleImponibileVerifica - Registrazione.Righe(I).Imponibile
                                    Else
                                        TotaleImponibileVerifica = TotaleImponibileVerifica + Registrazione.Righe(I).Imponibile
                                    End If
                                Else
                                    If Registrazione.Righe(I).Segno <> "-" And TipoDoc <> "NC" Then
                                        TotaleImponibileVerifica = TotaleImponibileVerifica + Registrazione.Righe(I).Imponibile
                                    Else
                                        TotaleImponibileVerifica = TotaleImponibileVerifica - Registrazione.Righe(I).Imponibile
                                    End If
                                End If
                            Else
                                If Registrazione.Righe(I).DareAvere = "A" Then
                                    If Registrazione.Righe(I).Segno <> "-" And TipoDoc <> "NC" Then
                                        TotaleImponibileVerifica = TotaleImponibileVerifica - Registrazione.Righe(I).Imponibile
                                    Else
                                        TotaleImponibileVerifica = TotaleImponibileVerifica + Registrazione.Righe(I).Imponibile
                                    End If
                                Else
                                    If Registrazione.Righe(I).Segno <> "-" And TipoDoc <> "NC" Then
                                        TotaleImponibileVerifica = TotaleImponibileVerifica + Registrazione.Righe(I).Imponibile
                                    Else
                                        TotaleImponibileVerifica = TotaleImponibileVerifica - Registrazione.Righe(I).Imponibile
                                    End If
                                End If
                            End If
                        End If
                    End If
                End If
            End If
        Next
    End Function


    Private Function TotaleImpostaVerifica(ByVal NumeroRegistrazione As Long) As Double
        Dim CausaleContabile As String
        Dim TipoDoc As String
        Dim I As Integer
        Dim Registrazione As New Cls_MovimentoContabile


        If NumeroRegistrazione = 76125 Then
            NumeroRegistrazione = 76125
        End If

        Registrazione.Leggi(Session("DC_GENERALE"), NumeroRegistrazione)

        CausaleContabile = Registrazione.CausaleContabile
        Dim CausCont As New Cls_CausaleContabile

        CausCont.Leggi(Session("DC_TABELLE"), CausaleContabile)
        TipoDoc = CausCont.TipoDocumento
        TotaleImpostaVerifica = 0


        For I = 0 To Registrazione.Righe.Length - 1
            If Not IsNothing(Registrazione.Righe(I)) Then
                If Registrazione.Righe(I).Tipo = "IV" Then
                    If Registrazione.Righe(I).CodiceIVA <> "" Then
                        Dim TipoIva As New Cls_IVA

                        TipoIva.Leggi(Session("DC_TABELLE"), Registrazione.Righe(I).CodiceIVA)
                        If TipoIva.InSpesometro <> 1 Then
                            If CausCont.VenditaAcquisti = "V" Then
                                If Registrazione.Righe(I).DareAvere = "D" Then
                                    If Registrazione.Righe(I).Segno <> "-" And TipoDoc <> "NC" Then
                                        TotaleImpostaVerifica = TotaleImpostaVerifica - Registrazione.Righe(I).Importo
                                    Else
                                        TotaleImpostaVerifica = TotaleImpostaVerifica + Registrazione.Righe(I).Importo
                                    End If
                                Else
                                    If Registrazione.Righe(I).Segno <> "-" And TipoDoc <> "NC" Then
                                        TotaleImpostaVerifica = TotaleImpostaVerifica + Registrazione.Righe(I).Importo
                                    Else
                                        TotaleImpostaVerifica = TotaleImpostaVerifica - Registrazione.Righe(I).Importo
                                    End If
                                End If
                            Else
                                If Registrazione.Righe(I).DareAvere = "A" Then
                                    If Registrazione.Righe(I).Segno <> "-" And TipoDoc <> "NC" Then
                                        TotaleImpostaVerifica = TotaleImpostaVerifica - Registrazione.Righe(I).Importo
                                    Else
                                        TotaleImpostaVerifica = TotaleImpostaVerifica + Registrazione.Righe(I).Importo
                                    End If
                                Else
                                    If Registrazione.Righe(I).Segno <> "-" And TipoDoc <> "NC" Then
                                        TotaleImpostaVerifica = TotaleImpostaVerifica + Registrazione.Righe(I).Importo
                                    Else
                                        TotaleImpostaVerifica = TotaleImpostaVerifica - Registrazione.Righe(I).Imponibile
                                    End If
                                End If
                            End If
                        End If
                    End If
                End If
            End If
        Next
    End Function


    Protected Sub ImageButton1_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles ImageButton1.Click

        Server.ScriptTimeout = 1000

        Call EsportaSpesometro()

    End Sub

    Protected Sub GeneraleWeb_Spesometro_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load

        If Trim(Session("UTENTE")) = "" Then
            Response.Redirect("/SeniorWeb/Login.aspx")
            Exit Sub
        End If

        If Session("ABILITAZIONI").IndexOf("<SPESOMETRO>") < 1 Then
            Response.Redirect("Menu_Servizi.aspx")
            Exit Sub
        End If


        If Page.IsPostBack = True Then
            Exit Sub
        End If


        Call EseguiJS()
        Dim K1 As New Cls_SqlString

        Dim Barra As New Cls_BarraSenior


        Lbl_BarraSenior.Text = Barra.CodiceBarra(Application("SENIOR"), Session("UTENTE"), Page, K1)

        Dim Appo As New Cls_RegistroIVA


        Appo.UpDateDropBox(Session("DC_TABELLE"), DD_Registro)
        Appo.UpDateDropBox(Session("DC_TABELLE"), DD_Registro2)
        Appo.UpDateDropBox(Session("DC_TABELLE"), DD_Registro3)

        Img_Download.Visible = False
        Img_Excel.Visible = False
    End Sub
    Private Sub EseguiJS()
        Dim MyJs As String
        MyJs = "$(document).ready(function() {"

        MyJs = MyJs & "var els = document.getElementsByTagName(""*"");"

        MyJs = MyJs & "for (var i=0;i<els.length;i++)"
        MyJs = MyJs & "if ( els[i].id ) { "
        MyJs = MyJs & " var appoggio =els[i].id; "


        MyJs = MyJs & " if (appoggio.match('Txt_Data')!= null) {  "
        MyJs = MyJs & " $(els[i]).mask(""99/99/9999"");  "
        MyJs = MyJs & " $(els[i]).datepicker({ changeMonth: true,changeYear: true,  yearRange: ""1890:" & Year(Now) + 1 & """},$.datepicker.regional[""it""]);"
        MyJs = MyJs & "    }"


        MyJs = MyJs & " if (appoggio.match('Txt_Importo')!= null)  {  "
        MyJs = MyJs & " $(els[i]).keypress(function() { ForceNumericInput($(this).val(), true, true); } ); "
        MyJs = MyJs & " $(els[i]).blur(function() { var ap = formatNumber($(this).val(),2); $(this).val(ap); });"
        MyJs = MyJs & "    }"

        MyJs = MyJs & "} "
        MyJs = MyJs & "});"

        'MyJs = "$(document).ready(function() { $('.myClass').keypress(function() { handleEnter($('.myClass').val(), true, true); } );     $('#" & Txt_ImportoPagato.ClientID & "').blur(function() { var ap = formatNumber ($('#" & Txt_ImportoPagato.ClientID & "').val(),2); $('#" & Txt_ImportoPagato.ClientID & "').val(ap); }); });"
        ScriptManager.RegisterStartupScript(Me, Me.GetType(), "visualizzaRitIm", MyJs, True)
    End Sub

    

    
    Function ChTesto_AE(ByVal TestoCercato As String) As String
        Dim TmpTesto As String, Not_Valid_Char As String, Valid_Char As String, Current_Char As String, CAR As String
        Dim I As Integer, Z As Integer
        TmpTesto = TestoCercato

        TmpTesto = Replace(TmpTesto, "&#", "ççççç")
        TmpTesto = Replace(TmpTesto, "&", "&amp;")
        TmpTesto = Replace(TmpTesto, "<", "&lt;")
        TmpTesto = Replace(TmpTesto, ">", "&gt;")
        TmpTesto = Replace(TmpTesto, Chr(34), "&quot;")
        TmpTesto = Replace(TmpTesto, "'", "&apos;")
        TmpTesto = Replace(TmpTesto, "ççççç", "&#")


        Not_Valid_Char = "ÀÁÂÃÄÅÆÇÈÉÊËÌÍÎÏÐÑÒÓÔÕÖÙÚÛÜÝßàáâãäåæçèéêëìíîïñòóôõöùúûüýÿ"
        Valid_Char = "AAAAAAACEEEEIIIIDNOOOOOUUUUYBaaaaaaaceeeeiiiinooooouuuuyy"

        For I = 1 To Len(TmpTesto)
            CAR = Mid(TmpTesto, I, 1)
            Z = InStr(Not_Valid_Char, CAR)
            If Z > 0 Then
                Mid(TmpTesto, I, 1) = Mid(Valid_Char, Z, 1)
                GoTo PROSSIMO
            End If


            If Asc(CAR) > 41 And Asc(CAR) < 58 Then GoTo PROSSIMO
            If Asc(CAR) > 64 And Asc(CAR) < 91 Then GoTo PROSSIMO
            If Asc(CAR) > 96 And Asc(CAR) < 123 Then GoTo PROSSIMO
            Mid(TmpTesto, I, 1) = " "

PROSSIMO:

        Next I

        Return TmpTesto

    End Function

    Protected Sub ImgMenu_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles ImgMenu.Click
        Response.Redirect("Menu_Generale.aspx")
    End Sub

    Protected Sub Btn_Esci_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles Btn_Esci.Click
        Response.Redirect("Menu_Servizi.aspx")
    End Sub

    Protected Sub Img_Download_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles Img_Download.Click

        Dim Xml As String = Session("XML")

        Dim DatiSocietaCls As New Cls_DecodificaSocieta

        DatiSocietaCls.Leggi(Session("DC_TABELLE"))

        Dim responseText As String '= Codice.Replace("\r\n", "<br/>")
        Response.Clear()
        Response.AddHeader("content-disposition", "attachment;filename=IT" & DatiSocietaCls.PartitaIVA & "_DF_" & Format(Val(Txt_Progressivo.Text), "00000") & ".xml")
        Response.Charset = String.Empty
        Response.Cache.SetCacheability(HttpCacheability.NoCache)
        Response.ContentType = "text/xml"
        Response.ContentEncoding = Encoding.UTF8
        REM Response.ContentEncoding = Encoding.UTF16
        Response.ContentType = "text/xml; charset=utf-8"
        REM Response.ContentType = "text/xml; charset=utf-16"
        Response.Write(Xml)
        Response.End()
    End Sub

    Protected Sub Img_excel_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles Img_excel.Click

        Session("GrigliaSoloStampa") = Session("TABELLA")
        ScriptManager.RegisterClientScriptBlock(Me, Me.GetType(), "Stampa", "window.open('../Ospitiweb/ExportExcel.aspx','Excel','width=800,height=600');", True)
    End Sub


End Class
