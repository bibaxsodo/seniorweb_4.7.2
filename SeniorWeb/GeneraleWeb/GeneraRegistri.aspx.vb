﻿Imports Microsoft.VisualBasic
Imports System.Data.OleDb
Imports System.Web.Hosting
Partial Class GeneraleWeb_GeneraRegistri
    Inherits System.Web.UI.Page
    Dim Tabella As New System.Data.DataTable("tabella")

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load

        If Page.IsPostBack = True Then
            Exit Sub
        End If


        Dim K1 As New Cls_SqlString

        Dim Barra As New Cls_BarraSenior

        If Not IsNothing(Session("RicercaGeneraleSQLString")) Then
            K1 = Session("RicercaGeneraleSQLString")
        End If

        Lbl_BarraSenior.Text = Barra.CodiceBarra(Application("SENIOR"), Session("UTENTE"), Page, K1)





        Txt_Anno.Text = Year(Now)
    End Sub

    Protected Sub ImageButton1_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles ImageButton1.Click
        If Val(Txt_Anno.Text) = 0 Then
            ScriptManager.RegisterStartupScript(Me, Me.GetType(), "visualizzaA", "VisualizzaErrore('Specificare anno');", True)
            Exit Sub
        End If

        Tabella.Clear()
        Tabella.Columns.Clear()
        Tabella.Columns.Add("Numero", GetType(String))
        Tabella.Columns.Add("Tipo", GetType(String))
        Tabella.Columns.Add("Descrizione", GetType(String))


        Dim cn As OleDbConnection



        cn = New Data.OleDb.OleDbConnection(Session("DC_TABELLE"))

        cn.Open()

        Dim cmd As New OleDbCommand()
        cmd.CommandText = ("select * from TipoRegistro ")
        cmd.Connection = cn


        Dim TipoRegistroRead As OleDbDataReader = cmd.ExecuteReader()
        Do While TipoRegistroRead.Read

            Dim cmdRegistroIVA As New OleDbCommand()
            cmdRegistroIVA.CommandText = ("select * from Registri where Anno = ? And Tipo = ? ")
            cmdRegistroIVA.Connection = cn
            cmdRegistroIVA.Parameters.AddWithValue("@Anno", Val(Txt_Anno.Text))
            cmdRegistroIVA.Parameters.AddWithValue("@Tipo", campodbN(TipoRegistroRead.Item("Tipo")))

            Dim RegistroIVA As OleDbDataReader = cmdRegistroIVA.ExecuteReader()
            If Not RegistroIVA.Read Then
                Dim cmdRegistroIVAInsert As New OleDbCommand()
                cmdRegistroIVAInsert.CommandText = ("INSERT INTO Registri (Utente,DataAggiornamento,Anno,Tipo,DataStampa,UltimaPaginaStampata) values (?,?,?,?,?,?)")
                cmdRegistroIVAInsert.Connection = cn

                cmdRegistroIVAInsert.Parameters.AddWithValue("@Utente", Session("UTENTE"))
                cmdRegistroIVAInsert.Parameters.AddWithValue("@DataAggiornamento", Now)
                cmdRegistroIVAInsert.Parameters.AddWithValue("@Anno", Val(Txt_Anno.Text))
                cmdRegistroIVAInsert.Parameters.AddWithValue("@Tipo", campodbN(TipoRegistroRead.Item("Tipo")))
                cmdRegistroIVAInsert.Parameters.AddWithValue("@DataStampa", DateSerial(Year(Now) - 1, 12, 31))
                cmdRegistroIVAInsert.Parameters.AddWithValue("@UltimaPaginaStampata", 0)
                cmdRegistroIVAInsert.ExecuteNonQuery()
                Dim myriga As System.Data.DataRow = Tabella.NewRow()
                myriga(0) = campodbN(TipoRegistroRead.Item("Tipo"))
                myriga(1) = campodb(TipoRegistroRead.Item("TipoIva"))
                myriga(2) = campodb(TipoRegistroRead.Item("Descrizione"))


                Tabella.Rows.Add(myriga)
            End If
            RegistroIVA.Close()


        Loop
        TipoRegistroRead.Close()
        cn.Close()

        GridView1.AutoGenerateColumns = True
        GridView1.DataSource = Tabella
        GridView1.DataBind()

    End Sub

    Function campodb(ByVal oggetto As Object) As String
        If IsDBNull(oggetto) Then
            Return ""
        Else
            Return oggetto
        End If
    End Function

    Function campodbN(ByVal oggetto As Object) As Double
        If IsDBNull(oggetto) Then
            Return 0
        Else
            Return oggetto
        End If
    End Function
End Class
