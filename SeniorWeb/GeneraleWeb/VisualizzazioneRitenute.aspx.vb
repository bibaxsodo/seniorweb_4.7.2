﻿Imports System.Web.Hosting
Imports System.Data.OleDb

Partial Class GeneraleWeb_VisualizzazioneRitenute
    Inherits System.Web.UI.Page
    Dim Tabella As New System.Data.DataTable("tabella")
    'CommandArgument= <%#  Eval("NumeroRegistrazione") %>


    Private Sub CaricaGriglia(ByVal ToExcel As Boolean)
        Dim Condizione As String
        Dim cn As OleDbConnection

        Condizione = ""

        If RB_DataDoc.Checked = True Then
            If IsDate(Txt_DataDal.Text) Then
                If Condizione <> "" Then
                    Condizione = Condizione & " And "
                End If
                Condizione = Condizione & " (Select DataRegistrazione From MovimentiContabiliTesta Where NumeroRegistrazione = NumeroRegistrazioneDocumento) >= ?"
            End If

            If IsDate(Txt_DataAl.Text) Then
                If Condizione <> "" Then
                    Condizione = Condizione & " And "
                End If
                Condizione = Condizione & " (Select DataRegistrazione From MovimentiContabiliTesta Where NumeroRegistrazione = NumeroRegistrazioneDocumento) <= ? "
            End If
        End If
        If RB_DataPag.Checked = True Then
            If IsDate(Txt_DataDal.Text) Then
                If Condizione <> "" Then
                    Condizione = Condizione & " And "
                End If
                Condizione = Condizione & " (Select DataRegistrazione From MovimentiContabiliTesta Where NumeroRegistrazione = NumeroRegistrazionePagamento) >= ?"
            End If

            If IsDate(Txt_DataAl.Text) Then
                If Condizione <> "" Then
                    Condizione = Condizione & " And "
                End If
                Condizione = Condizione & " (Select DataRegistrazione From MovimentiContabiliTesta Where NumeroRegistrazione = NumeroRegistrazionePagamento) <= ? "
            End If
        End If

        If RB_DataF24.Checked = True Then
            If IsDate(Txt_DataDal.Text) Then
                If Condizione <> "" Then
                    Condizione = Condizione & " And "
                End If
                Condizione = Condizione & " (Select DataRegistrazione From MovimentiContabiliTesta Where NumeroRegistrazione = NumeroRegistrazionePagamentoRitenuta) >= ?"
            End If

            If IsDate(Txt_DataAl.Text) Then
                If Condizione <> "" Then
                    Condizione = Condizione & " And "
                End If
                Condizione = Condizione & " (Select DataRegistrazione From MovimentiContabiliTesta Where NumeroRegistrazione = NumeroRegistrazionePagamentoRitenuta) <= ? "
            End If
        End If

        If DD_DocNonPagati.Checked = True Then
            If Condizione <> "" Then
                Condizione = Condizione & " And "
            End If
            Condizione = Condizione & " (Select ImportoPagamento From MovimentiContabiliTesta Where NumeroRegistrazione = NumeroRegistrazioneDocumento) = 0"
        End If

        If DD_DocPagati.Checked = True Then
            If Condizione <> "" Then
                Condizione = Condizione & " And "
            End If
            Condizione = Condizione & " (Select ImportoPagamento From MovimentiContabiliTesta Where NumeroRegistrazione = NumeroRegistrazioneDocumento) > 0"
        End If

        If DD_RitenutaPagata.Checked = True Then
            If Condizione <> "" Then
                Condizione = Condizione & " And "
            End If
            Condizione = Condizione & " (Select ImportoPagamentoRitenuta From MovimentiContabiliTesta Where NumeroRegistrazione = NumeroRegistrazioneDocumento) > 0"
        End If


        Tabella.Clear()
        Tabella.Columns.Clear()
        Tabella.Columns.Add("Tipo Doc.", GetType(String))
        Tabella.Columns.Add("N. Regis.", GetType(String))
        Tabella.Columns.Add("Num.Doc.", GetType(String))
        Tabella.Columns.Add("Data Doc.", GetType(String))
        Tabella.Columns.Add("Cod. Tributo", GetType(String))
        Tabella.Columns.Add("Tributo", GetType(String))
        Tabella.Columns.Add("Imponibile Rit.", GetType(String))
        Tabella.Columns.Add("Tipo Rit.", GetType(String))
        Tabella.Columns.Add("Imp. Ritenuta", GetType(String))
        Tabella.Columns.Add("Pagamento", GetType(String))
        Tabella.Columns.Add("Data Pagamento", GetType(String))
        Tabella.Columns.Add("Imp. Pagamento", GetType(String))
        Tabella.Columns.Add("Reg. Pag. Ritenuta", GetType(String))
        Tabella.Columns.Add("Imp. Pag. Ritenuta", GetType(String))
        Tabella.Columns.Add("Id", GetType(String))
        Tabella.Columns.Add("Professionista", GetType(String))
        Dim StringaConnessione As String = Session("DC_GENERALE")
        Dim StringaConnessioneTabelle As String = Session("DC_TABELLE")

        cn = New Data.OleDb.OleDbConnection(StringaConnessione)

        cn.Open()

        Dim cmd As New OleDbCommand()


        If Trim(Txt_Sottoconto.Text) <> "" Then
            Dim Mastro As Long
            Dim Conto As Long
            Dim Sottoconto As Long
            Dim Vettore(100) As String
            Vettore = SplitWords(Txt_Sottoconto.Text)

            If Vettore.Length > 2 Then
                Mastro = Val(Vettore(0))
                Conto = Val(Vettore(1))
                Sottoconto = Val(Vettore(2))

                If Condizione <> "" Then
                    Condizione = Condizione & " And "
                End If
                Condizione = Condizione & " (Select top 1 Numero From MovimentiContabiliRiga Where Numero = NumeroRegistrazioneDocumento And MastroPartita = " & Mastro & " And ContoPartita = " & Conto & " And SottocontoPartita = " & Sottoconto & ") = NumeroRegistrazioneDocumento "
            End If

        End If

        cmd.CommandText = "Select * From MovimentiRitenute where " & Condizione
        If IsDate(Txt_DataDal.Text) Then
            Dim Txt_DataDalText As Date = Txt_DataDal.Text
            cmd.Parameters.AddWithValue("@DataDal", Txt_DataDalText)
        End If

        If IsDate(Txt_DataAl.Text) Then
            Dim Txt_DataAlText As Date = Txt_DataAl.Text
            cmd.Parameters.AddWithValue("@DataAl", Txt_DataAlText)
        End If


        cmd.Connection = cn
        Dim myPOSTreader As OleDbDataReader = cmd.ExecuteReader()
        Do While myPOSTreader.Read


            Dim myriga As System.Data.DataRow = Tabella.NewRow()


            Dim RegistrazioneContabile As New Cls_MovimentoContabile

            RegistrazioneContabile.NumeroRegistrazione = campodbN(myPOSTreader.Item("NumeroRegistrazioneDocumento"))
            RegistrazioneContabile.Leggi(StringaConnessione, RegistrazioneContabile.NumeroRegistrazione)

            Dim CausaleContabile As New Cls_CausaleContabile

            CausaleContabile.Codice = RegistrazioneContabile.CausaleContabile
            CausaleContabile.Leggi(StringaConnessioneTabelle, CausaleContabile.Codice)


            myriga(0) = CausaleContabile.TipoDocumento
            myriga(1) = RegistrazioneContabile.NumeroRegistrazione

            myriga(2) = RegistrazioneContabile.NumeroDocumento

            myriga(3) = Format(RegistrazioneContabile.DataDocumento, "dd/MM/yyyy")

            myriga(4) = campodb(myPOSTreader.Item("CodiceTributo"))

            Dim Tributo As New Cls_TabellaTributo

            Tributo.Codice = campodb(myPOSTreader.Item("CodiceTributo"))
            Tributo.Leggi(StringaConnessioneTabelle, Tributo.Codice)

            myriga(5) = Tributo.Descrizione

            myriga(6) = Format(campodbN(myPOSTreader.Item("ImponibileRitenuta")), "#,##0.00")

            Dim TipoRitenuta As New ClsRitenuta

            TipoRitenuta.Codice = campodb(myPOSTreader.Item("TipoRitenuta"))
            TipoRitenuta.Leggi(StringaConnessioneTabelle)

            myriga(7) = TipoRitenuta.Descrizione

            myriga(8) = Format(campodbN(myPOSTreader.Item("ImportoRitenuta")), "#,##0.00")


            Dim RegistrazioneContabilePagamento As New Cls_MovimentoContabile

            RegistrazioneContabilePagamento.NumeroRegistrazione = campodbN(myPOSTreader.Item("NumeroRegistrazionePagamento"))
            RegistrazioneContabilePagamento.Leggi(StringaConnessione, RegistrazioneContabile.NumeroRegistrazione)

            myriga(9) = campodbN(myPOSTreader.Item("NumeroRegistrazionePagamento"))


            If campodbN(myPOSTreader.Item("NumeroRegistrazionePagamento")) = 0 Then
                myriga(10) = ""
            Else

                myriga(10) = Format(RegistrazioneContabilePagamento.DataRegistrazione, "dd/MM/yyyy")
            End If
            myriga(11) = Format(campodbN(myPOSTreader.Item("ImportoPagamento")), "#,##0.00")
            myriga(12) = campodbN(myPOSTreader.Item("NumeroRegistrazionePagamentoRitenuta"))
            myriga(13) = Format(campodbN(myPOSTreader.Item("ImportoPagamentoRitenuta")), "#,##0.00")

            myriga(14) = campodbN(myPOSTreader.Item("ID"))

            Dim DecPC As New Cls_Pianodeiconti


            DecPC.Mastro = RegistrazioneContabile.Righe(0).MastroPartita
            DecPC.Conto = RegistrazioneContabile.Righe(0).ContoPartita
            DecPC.Sottoconto = RegistrazioneContabile.Righe(0).SottocontoPartita
            DecPC.Decodfica(Session("DC_GENERALE"))

            myriga(15) = DecPC.Descrizione


            Tabella.Rows.Add(myriga)
        Loop
        myPOSTreader.Close()
        cn.Close()



        Session("DocumentScoperti") = Tabella

        If ToExcel = False Then
            Grid.AutoGenerateColumns = True
            Grid.DataSource = Tabella
            Grid.Font.Size = 10
            Grid.DataBind()
        Else
            GridView1.AutoGenerateColumns = True
            GridView1.DataSource = Tabella
            GridView1.Font.Size = 10
            GridView1.DataBind()
        End If
    End Sub

    Function campodbN(ByVal oggetto As Object) As Double
        If IsDBNull(oggetto) Then
            Return 0
        Else
            Return oggetto
        End If
    End Function
    Function campodb(ByVal oggetto As Object) As String
        If IsDBNull(oggetto) Then
            Return ""
        Else
            Return oggetto
        End If
    End Function
    Private Function SplitWords(ByVal s As String) As String()
        '
        ' Call Regex.Split function from the imported namespace.
        ' Return the result array.
        '
        Return Regex.Split(s, "\W+")
    End Function

    Protected Sub ImageButton1_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles ImageButton1.Click
        If Not IsDate(Txt_DataDal.Text) Then
            REM ClientScript.RegisterClientScriptBlock(Me.GetType(), "Errore", "VisualizzaErrore('Data dal formalemente errata');", True)
            ScriptManager.RegisterStartupScript(Me, Me.GetType(), "visualizzaA", "VisualizzaErrore('Data dal formalemente errata');", True)
            REM Lbl_Errori.Text = "Data dal formalemente errata"
            Call EseguiJS()
            Exit Sub
        End If
        If Not IsDate(Txt_DataAl.Text) Then
            REM ClientScript.RegisterClientScriptBlock(Me.GetType(), "Errore", "VisualizzaErrore('Data dal formalemente errata');", True)
            ScriptManager.RegisterStartupScript(Me, Me.GetType(), "visualizzaA", "VisualizzaErrore('Data al formalemente errata');", True)
            REM Lbl_Errori.Text = "Data dal formalemente errata"
            Call EseguiJS()
            Exit Sub
        End If

        CaricaGriglia(False)

        Call EseguiJS()
    End Sub



    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Trim(Session("UTENTE")) = "" Then
            If Request.ServerVariables("URL").ToUpper.IndexOf("SENIORWEBBLANCO") > 0 Then
                Response.Redirect("/ODV/Login.aspx")
                Exit Sub
            Else
                Response.Redirect("/SeniorWeb/Login.aspx")
                Exit Sub
            End If
        End If


        If Page.IsPostBack = True Then Exit Sub



        Dim K1 As New Cls_SqlString

        Dim Barra As New Cls_BarraSenior

        If Not IsNothing(Session("RicercaGeneraleSQLString")) Then
            K1 = Session("RicercaGeneraleSQLString")
        End If

        Lbl_BarraSenior.Text = Barra.CodiceBarra(Application("SENIOR"), Session("UTENTE"), Page, K1)

        Call EseguiJS()
        ViewState("XLINEA") = -1
        ViewState("XBACKUP") = 0
    End Sub

    Protected Sub ImageButton2_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles ImageButton2.Click
        If Not IsDate(Txt_DataDal.Text) Then
            ClientScript.RegisterClientScriptBlock(Me.GetType(), "Errore", "VisualizzaErrore('Data dal formalemente errata');", True)
            REM Lbl_Errori.Text = "Data dal formalemente errata"
            Call EseguiJS()
            Exit Sub
        End If
        If Not IsDate(Txt_DataAl.Text) Then
            ClientScript.RegisterClientScriptBlock(Me.GetType(), "Errore", "VisualizzaErrore('Data al formalemente errata');", True)
            REM Lbl_Errori.Text = "Data dal formalemente errata"ù
            Call EseguiJS()
            Exit Sub
        End If

        CaricaGriglia(False)

        Session("GrigliaSoloStampa") = Session("DocumentScoperti")

        ScriptManager.RegisterClientScriptBlock(Me, Me.GetType(), "Stampa", "window.open('ExportExcel.aspx','Excel','width=800,height=600');", True)

        Call EseguiJS()
    End Sub

    Protected Sub Grid_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles Grid.SelectedIndexChanged

    End Sub

    Protected Sub Btn_Esci_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles Btn_Esci.Click
        Response.Redirect("Menu_Visualizzazioni.aspx")
    End Sub


    Private Sub EseguiJS(Optional ByVal MClient As Boolean = False)
        Dim MyJs As String
        'MyJs = "alert('r');"
        MyJs = ""
        MyJs = MyJs & "var els = document.getElementsByTagName(""*"");"

        MyJs = MyJs & "for (var i=0;i<els.length;i++)"
        MyJs = MyJs & "if ( els[i].id ) { "
        MyJs = MyJs & " var appoggio =els[i].id; "
        MyJs = MyJs & " if ((appoggio.match('Txt_Sottoconto')!= null)) {  "
        MyJs = MyJs & " $(els[i]).autocomplete('/WebHandler/GestioneConto.ashx?Utente=" & Session("UTENTE") & "', {delay:5,minChars:3});"
        MyJs = MyJs & "    }"

        MyJs = MyJs & " if ((appoggio.match('Txt_Data')!= null)) {  "
        MyJs = MyJs & " $(els[i]).mask(""99/99/9999""); "
        MyJs = MyJs & " $(els[i]).datepicker({ changeMonth: true,changeYear: true,  yearRange: ""1990:" & Year(Now) + 5 & """},$.datepicker.regional[""it""]);"

        MyJs = MyJs & "    }"

        MyJs = MyJs & "} "
        'MyJs = MyJs & "});"

        'MyJs = "$(document).ready(function() { $('.myClass').keypress(function() { handleEnter($('.myClass').val(), true, true); } );     $('#" & Txt_ImportoPagato.ClientID & "').blur(function() { var ap = formatNumber ($('#" & Txt_ImportoPagato.ClientID & "').val(),2); $('#" & Txt_ImportoPagato.ClientID & "').val(ap); }); });"

        ScriptManager.RegisterStartupScript(Me, Me.GetType(), "EseguiJSGriglie", MyJs, True)

    End Sub

End Class
