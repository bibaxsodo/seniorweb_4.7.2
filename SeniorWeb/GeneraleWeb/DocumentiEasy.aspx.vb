﻿
Partial Class GeneraleWeb_DocumentiEasy
    Inherits System.Web.UI.Page

    Private Sub RicalcolaImportiDocumento()
        Dim XTI As New Cls_Legami



        Lbl_Importo.Text = "<table><td width=""118px"" height=""50px"" align=""center"" style=""background-image: url('images/totaledocumento.jpg')""><font color=white><div id=""my_totdoc"">" & Format(TotaleDocumento(), "#,##0.00") & "</div></font></td>" & _
                           "<td width=""118px"" height=""50px"" align=""center"" style=""background-image: url('images/totalenetto.jpg')""><font color=white><div id=""my_totrit"">" & Format(TotaleDocumento() - TotaleRitenuta(), "#,##0.00") & "</div></font></td>" & _
                            "<td width=""118px"" height=""50px"" align=""center"" style=""background-image: url('images/totalepagato.jpg')""><font color=white><div id=""my_totnetto"">" & Format(Math.Abs(XTI.TotaleLegame(Session("DC_GENERALE"), Val(Txt_Numero.Text))), "#,##0.00") & "</div></font></td></table>"

        Dim MyJs As String


        MyJs = "$(document).ready(function() { "
        MyJs = MyJs & "$('#my_totdoc').html('" & Format(TotaleDocumento(), "#,##0.00") & "');  "
        MyJs = MyJs & "$('#my_totrit').html('" & Format(TotaleDocumento() - TotaleRitenuta(), "#,##0.00") & "');  "
        MyJs = MyJs & "$('#my_totnetto').html('" & Format(XTI.TotaleLegame(Session("DC_GENERALE"), Val(Txt_Numero.Text)), "#,##0.00") & "');  "
        MyJs = MyJs & "});"

        ScriptManager.RegisterStartupScript(Me, Me.GetType(), "Vs_ITD", MyJs, True)

    End Sub
    Private Function TotaleRitenuta() As Double
        Dim X As New Cls_CausaleContabile
        Dim i As Long = 0
        Dim TotInterno As Double = 0

        X.Leggi(Session("DC_TABELLE"), Dd_CausaleContabile.SelectedValue)

        If X.TipoDocumento = "FA" Then
            TotaleRitenuta = TotaleRitenuta + CDbl(Txt_Ritenuta1.Text)  '+ CDbl(Txt_Ritenuta2.Text) + CDbl(Txt_Ritenuta3.Text) + CDbl(Txt_Ritenuta4.Text) + CDbl(Txt_Ritenuta5.Text)
        Else
            TotaleRitenuta = TotaleRitenuta - CDbl(Txt_Ritenuta1.Text) '- CDbl(Txt_Ritenuta2.Text) - CDbl(Txt_Ritenuta3.Text) - CDbl(Txt_Ritenuta4.Text) - CDbl(Txt_Ritenuta5.Text)
        End If

    End Function
    Private Function TotaleDocumento() As Double
        Dim X As New Cls_CausaleContabile        
        Dim TotInterno As Double = 0

        If Dd_CausaleContabile.SelectedValue = "" Then
            Return 0
            Exit Function
        End If
        X.Leggi(Session("DC_TABELLE"), Dd_CausaleContabile.SelectedValue)

        Dim DareAvere As String

        If Rb_Avere1.Checked = True Then
            DareAvere = "A"
        Else
            DareAvere = "D"
        End If
        If DareAvere <> X.Righe(0).DareAvere Then
            TotInterno = TotInterno + CDbl(Txt_Imponibile1.Text)
            TotInterno = TotInterno + CDbl(Txt_Imposta1.Text)
        Else
            TotInterno = TotInterno - CDbl(Txt_Imponibile1.Text)
            TotInterno = TotInterno - CDbl(Txt_Imposta1.Text)
        End If

        If Rb_Avere2.Checked = True Then
            DareAvere = "A"
        Else
            DareAvere = "D"
        End If
        If DareAvere <> X.Righe(0).DareAvere Then
            TotInterno = TotInterno + CDbl(Txt_Imponibile2.Text)
            TotInterno = TotInterno + CDbl(Txt_Imposta2.Text)
        Else
            TotInterno = TotInterno - CDbl(Txt_Imponibile2.Text)
            TotInterno = TotInterno - CDbl(Txt_Imposta2.Text)
        End If

        If Rb_Avere3.Checked = True Then
            DareAvere = "A"
        Else
            DareAvere = "D"
        End If
        If DareAvere <> X.Righe(0).DareAvere Then
            TotInterno = TotInterno + CDbl(Txt_Imponibile3.Text)
            TotInterno = TotInterno + CDbl(Txt_Imposta3.Text)
        Else
            TotInterno = TotInterno - CDbl(Txt_Imponibile3.Text)
            TotInterno = TotInterno - CDbl(Txt_Imposta3.Text)
        End If

        If Rb_Avere4.Checked = True Then
            DareAvere = "A"
        Else
            DareAvere = "D"
        End If
        If DareAvere <> X.Righe(0).DareAvere Then
            TotInterno = TotInterno + CDbl(Txt_Imponibile4.Text)
            TotInterno = TotInterno + CDbl(Txt_Imposta4.Text)
        Else
            TotInterno = TotInterno - CDbl(Txt_Imponibile4.Text)
            TotInterno = TotInterno - CDbl(Txt_Imposta4.Text)
        End If

        If Rb_Avere5.Checked = True Then
            DareAvere = "A"
        Else
            DareAvere = "D"
        End If
        If DareAvere <> X.Righe(0).DareAvere Then
            TotInterno = TotInterno + CDbl(Txt_Imponibile5.Text)
            TotInterno = TotInterno + CDbl(Txt_Imposta5.Text)
        Else
            TotInterno = TotInterno - CDbl(Txt_Imponibile5.Text)
            TotInterno = TotInterno - CDbl(Txt_Imposta5.Text)
        End If


        Return TotInterno

    End Function
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Trim(Session("UTENTE")) = "" Then
            If Request.ServerVariables("URL").ToUpper.IndexOf("SENIORWEBBLANCO") > 0 Then
                Response.Redirect("/ODV/Login.aspx")
                Exit Sub
            Else
                Response.Redirect("/SeniorWeb/Login.aspx")
                Exit Sub
            End If
        End If

        If Request.Item("CHIAMATAESTERNA") > 0 Then
            Btn_Esci.Visible = False
            ImgRicerca.Visible = False
            Call MettiInvisibileJS()
        End If

        Timer1.Interval = 900 * 1000


        If Page.IsPostBack = True Then Exit Sub


        Lbl_Importo.Text = "<table><td width=""118px"" height=""50px"" align=""center"" style=""background-image: url('images/totaledocumento.jpg')""><font color=white><div id=""my_totdoc"">0,00</div></font></td>" & _
                     "<td width=""118px"" height=""50px"" align=""center"" style=""background-image: url('images/totalenetto.jpg')""><font color=white><div id=""my_totrit"">0,00</div></font></td>" & _
                      "<td width=""118px"" height=""50px"" align=""center"" style=""background-image: url('images/totalepagato.jpg')""><font color=white><div id=""my_totnetto"">0,00</div></font></td></table>"


        Dim CodiciIva As New Cls_IVA

        CodiciIva.UpDateDropBox(Session("DC_TABELLE"), DD_IVA1)
        CodiciIva.UpDateDropBox(Session("DC_TABELLE"), DD_IVA2)
        CodiciIva.UpDateDropBox(Session("DC_TABELLE"), DD_IVA3)
        CodiciIva.UpDateDropBox(Session("DC_TABELLE"), DD_IVA4)
        CodiciIva.UpDateDropBox(Session("DC_TABELLE"), DD_IVA5)

        Txt_DataRegistrazione.Text = Format(Now, "dd/MM/yyyy")

        Txt_AnnoProtocollo.Text = Year(Now)

        Dim XCausali As New Cls_CausaleContabile

        XCausali.UpDateDropBoxDoc(Session("DC_TABELLE"), Dd_CausaleContabile)

        Dim x As New Cls_TipoPagamento

        x.UpDateDropBox(Session("DC_TABELLE"), DD_ModalitaPagamento)

        Txt_Imponibile1.Text = 0
        Txt_Imponibile2.Text = 0
        Txt_Imponibile3.Text = 0
        Txt_Imponibile4.Text = 0
        Txt_Imponibile5.Text = 0

        DD_IVA1.SelectedValue = ""
        DD_IVA2.SelectedValue = ""
        DD_IVA3.SelectedValue = ""
        DD_IVA4.SelectedValue = ""
        DD_IVA5.SelectedValue = ""


        Txt_Imposta1.Text = 0
        Txt_Imposta2.Text = 0
        Txt_Imposta3.Text = 0
        Txt_Imposta4.Text = 0
        Txt_Imposta5.Text = 0


        Txt_Ritenuta1.Text = 0
        'Txt_Ritenuta2.Text = 0
        'Txt_Ritenuta3.Text = 0
        'Txt_Ritenuta4.Text = 0
        'Txt_Ritenuta5.Text = 0

        Dim kP As New Cls_MovimentoContabile


        Lbl_Progressivo.Text = kP.MaxProgressivoAnno(Session("DC_GENERALE"), Year(Txt_DataRegistrazione.Text)) + 1 & "/" & Year(Txt_DataRegistrazione.Text)



        If Val(Session("NumeroRegistrazione")) = 0 Then
            Txt_Numero.Text = 0
            Call Pulisci()
            Call RicalcolaImportiDocumento()
            Call EseguiJS()
        Else
            Txt_Numero.Text = Val(Session("NumeroRegistrazione"))
            Call Txt_Numero_TextChanged(sender, e)
            Call RicalcolaImportiDocumento()
            Call EseguiJS()

            Exit Sub
        End If

    End Sub

    Protected Sub Dd_CausaleContabile_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles Dd_CausaleContabile.SelectedIndexChanged
        Dim X As New Cls_CausaleContabile

        X.Leggi(Session("DC_TABELLE"), Dd_CausaleContabile.SelectedValue)

        Dim DecReg As New Cls_RegistroIVA

        DecReg.Leggi(Session("DC_TABELLE"), X.RegistroIVA)

        Lbl_RegistroIVA.Text = "(" & DecReg.Descrizione & ")"

        DD_IVA1.SelectedValue = X.CodiceIva


        If X.Tipo = "I" Then
            If X.Righe(2).DareAvere = "A" Then
                Rb_Avere1.Checked = True
                Rb_Avere2.Checked = True
                Rb_Avere3.Checked = True
                Rb_Avere4.Checked = True
                Rb_Avere5.Checked = True
                Rb_Dare1.Checked = False
                Rb_Dare2.Checked = False
                Rb_Dare3.Checked = False
                Rb_Dare4.Checked = False
                Rb_Dare5.Checked = False
            Else
                Rb_Avere1.Checked = False
                Rb_Avere2.Checked = False
                Rb_Avere3.Checked = False
                Rb_Avere4.Checked = False
                Rb_Avere5.Checked = False
                Rb_Dare1.Checked = True
                Rb_Dare2.Checked = True
                Rb_Dare3.Checked = True
                Rb_Dare4.Checked = True
                Rb_Dare5.Checked = True
            End If
        End If

        If X.Ritenuta = "" Then
            Txt_Ritenuta1.Text = 0
            'Txt_Ritenuta2.Text = 0
            'Txt_Ritenuta3.Text = 0
            'Txt_Ritenuta4.Text = 0
            'Txt_Ritenuta5.Text = 0
            Txt_Ritenuta1.Enabled = False
            'Txt_Ritenuta2.Enabled = False
            'Txt_Ritenuta3.Enabled = False
            'Txt_Ritenuta4.Enabled = False
            'Txt_Ritenuta5.Enabled = False
        Else
            Txt_Ritenuta1.Enabled = True
            'Txt_Ritenuta2.Enabled = True
            'Txt_Ritenuta3.Enabled = True
            'Txt_Ritenuta4.Enabled = True
            'Txt_Ritenuta5.Enabled = True

        End If

        Txt_AnnoProtocollo.Text = Year(Txt_DataRegistrazione.Text)

        Txt_Ritenuta1.Enabled = True
        If X.VenditaAcquisti = "V" Then

            Dim Mc As New Cls_MovimentoContabile
            Txt_DataDocumento.Text = Txt_DataRegistrazione.Text

            Txt_NumeroDocumento.Text = Mc.MaxProtocollo(Session("DC_GENERALE"), Txt_AnnoProtocollo.Text, X.RegistroIVA) + 1
            Txt_Ritenuta1.Enabled = False
        End If

        Call EseguiJS()
    End Sub



    Protected Sub Btn_Modifica_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles Btn_Modifica.Click


        If Trim(Dd_CausaleContabile.SelectedValue) = "" Then

            ClientScript.RegisterClientScriptBlock(Me.GetType(), "Errore", "VisualizzaErrore('Specifica causale contabile');", True)
            REM Lbl_errori.Text = "Specifica causale contabile"
            Call EseguiJS()
            Exit Sub
        End If

        If Val(Txt_AnnoProtocollo.Text) = 0 Then
            ClientScript.RegisterClientScriptBlock(Me.GetType(), "Errore", "VisualizzaErrore('Specifica anno protocollo');", True)
            REM Lbl_errori.Text = "Specifica anno protocollo"
            Call EseguiJS()
            Exit Sub
        End If

        If Not IsDate(Txt_DataRegistrazione.Text) Then
            ClientScript.RegisterClientScriptBlock(Me.GetType(), "Errore", "VisualizzaErrore('Data Registrazione formalmente errata');", True)
            REM Lbl_errori.Text = "Data Registrazione formalmente errata"
            Call EseguiJS()
            Exit Sub
        End If

        If Not IsDate(Txt_DataDocumento.Text) Then
            ClientScript.RegisterClientScriptBlock(Me.GetType(), "Errore", "VisualizzaErrore('Data Documento formalmente errata');", True)
            REM Lbl_errori.Text = "Data Documento formalmente errata"
            Call EseguiJS()
            Exit Sub
        End If

        If Trim(Txt_ClienteFornitore.Text) = "" Then
            ClientScript.RegisterClientScriptBlock(Me.GetType(), "Errore", "VisualizzaErrore('Cliente / Fornitore non specificato');", True)
            REM Lbl_errori.Text = "Cliente / Fornitore non specificato"
            Call EseguiJS()
            Exit Sub
        End If



        Dim XDatiGenerali As New Cls_DatiGenerali
        Dim AppoggioData As Date

        AppoggioData = Txt_DataRegistrazione.Text
        XDatiGenerali.LeggiDati(Session("DC_TABELLE"))

        If Format(XDatiGenerali.DataChiusura, "yyyyMMdd") >= Format(AppoggioData, "yyyyMMdd") Then
            ClientScript.RegisterClientScriptBlock(Me.GetType(), "Errore", "VisualizzaErrore('Non posso inserire o modificare registrazione,<BR/> già effettuato la chiusura');", True)
            Exit Sub
        End If


        Dim Vettore(100) As String
        Vettore = SplitWords(Txt_ClienteFornitore.Text)

        If Vettore.Length < 3 Then

            ClientScript.RegisterClientScriptBlock(Me.GetType(), "Errore", "VisualizzaErrore('Cliente fornitore non corretamente inserito');", True)
            REM Lbl_errori.Text = "Cliente fornitore non corretamente inserito"
            Exit Sub
        End If
        If Val(Vettore(2)) = 0 Then
            ClientScript.RegisterClientScriptBlock(Me.GetType(), "Errore", "VisualizzaErrore('Cliente fornitore non corretamente inserito');", True)
            REM Lbl_errori.Text = "Cliente fornitore non corretamente inserito"
            Exit Sub
        End If

        Dim TotaleDocumento As Double

        If Txt_Imponibile1.Text <> "" Then

            Dim Xs As New Cls_IVA

            Xs.Leggi(Session("DC_TABELLE"), DD_IVA1.SelectedValue)


            TotaleDocumento = TotaleDocumento + CDbl(Txt_Imponibile1.Text) + Modulo.MathRound(CDbl(Txt_Imponibile1.Text) * Xs.Aliquota, 2)

        Else
            Txt_Imponibile1.Text = 0
        End If
        If Txt_Imponibile2.Text <> "" Then
            Dim Xs As New Cls_IVA

            Xs.Leggi(Session("DC_TABELLE"), DD_IVA2.SelectedValue)


            TotaleDocumento = TotaleDocumento + CDbl(Txt_Imponibile2.Text) + Modulo.MathRound(CDbl(Txt_Imponibile2.Text) * Xs.Aliquota, 2)
        Else
            Txt_Imponibile2.Text = 0
        End If
        If Txt_Imponibile3.Text <> "" Then
            Dim Xs As New Cls_IVA

            Xs.Leggi(Session("DC_TABELLE"), DD_IVA3.SelectedValue)




            TotaleDocumento = TotaleDocumento + CDbl(Txt_Imponibile3.Text) + Modulo.MathRound(CDbl(Txt_Imponibile3.Text) * Xs.Aliquota, 2)
        Else
            Txt_Imponibile3.Text = 0
        End If
        If Txt_Imponibile4.Text <> "" Then
            Dim Xs As New Cls_IVA

            Xs.Leggi(Session("DC_TABELLE"), DD_IVA4.SelectedValue)


            TotaleDocumento = TotaleDocumento + CDbl(Txt_Imponibile4.Text) + Modulo.MathRound(CDbl(Txt_Imponibile4.Text) * Xs.Aliquota, 2)
        Else
            Txt_Imponibile4.Text = 0
        End If
        If Txt_Imponibile5.Text <> "" Then

            Dim Xs As New Cls_IVA

            Xs.Leggi(Session("DC_TABELLE"), DD_IVA5.SelectedValue)



            TotaleDocumento = TotaleDocumento + CDbl(Txt_Imponibile5.Text) + Modulo.MathRound(CDbl(Txt_Imponibile5.Text) * Xs.Aliquota, 2)
        Else
            Txt_Imponibile5.Text = 0
        End If

        If TotaleDocumento = 0 Then
            ClientScript.RegisterClientScriptBlock(Me.GetType(), "Errore", "VisualizzaErrore('Specificare almeno un importo');", True)
            REM Lbl_errori.Text = "Cliente / Fornitore non specificato"
            Exit Sub
        End If

        Dim MyCau As New Cls_CausaleContabile

        MyCau.Leggi(Session("DC_TABELLE"), Dd_CausaleContabile.SelectedValue)

        If MyCau.Ritenuta = "" Then
            'Or CDbl(Txt_Ritenuta2.Text) > 0 Or CDbl(Txt_Ritenuta3.Text) > 0 Or CDbl(Txt_Ritenuta4.Text) > 0 Or CDbl(Txt_Ritenuta5.Text) > 0 
            If CDbl(Txt_Ritenuta1.Text) > 0 Then
                ClientScript.RegisterClientScriptBlock(Me.GetType(), "Errore", "VisualizzaErrore('Non posso usare ritenuta, della causale non indicato il tipo ritenuta');", True)
                REM Lbl_errori.Text = "Cliente / Fornitore non specificato"
                Exit Sub
            End If
        End If



        Dim x As New Cls_MovimentoContabile

        Dim i As Integer
        Dim ConnectionStringGenerale As String = Session("DC_GENERALE")
        x.Leggi(ConnectionStringGenerale, 0)



        x.NumeroRegistrazione = Val(Txt_Numero.Text)
        x.DataRegistrazione = Txt_DataRegistrazione.Text
        x.CausaleContabile = Dd_CausaleContabile.SelectedValue



        x.Descrizione = Txt_Descrizione.Text
        x.DataDocumento = Txt_DataDocumento.Text
        x.NumeroDocumento = Txt_NumeroDocumento.Text
        x.RegistroIVA = MyCau.RegistroIVA

        x.CausaleContabile = Dd_CausaleContabile.SelectedValue

        x.AnnoProtocollo = Val(Txt_AnnoProtocollo.Text)
        x.NumeroProtocollo = Val(Txt_NumeroProtocollo.Text)

        x.ModalitaPagamento = DD_ModalitaPagamento.SelectedValue


        x.ModalitaPagamento = DD_ModalitaPagamento.SelectedValue
        x.IVASospesa = "N"

        x.Utente = Session("UTENTE")

        x.Righe(0) = New Cls_MovimentiContabiliRiga


        Dim Mastro As Long
        Dim Conto As Long
        Dim Sottoconto As Long

        Vettore = SplitWords(Txt_ClienteFornitore.Text)

        If Vettore.Length > 2 Then
            Mastro = Val(Vettore(0))
            Conto = Val(Vettore(1))
            Sottoconto = Val(Vettore(2))
        End If

        x.Righe(0).MastroPartita = Mastro
        x.Righe(0).ContoPartita = Conto
        x.Righe(0).SottocontoPartita = Sottoconto

        x.Righe(0).DareAvere = MyCau.Righe(0).DareAvere
        x.Righe(0).Segno = "+"
        x.Righe(0).Importo = TotaleDocumento
        x.Righe(0).Tipo = "CF"

        x.Righe(0).Descrizione = ""
        x.Righe(0).RigaDaCausale = 1

        x.Righe(0).MastroContropartita = 0
        x.Righe(0).ContoContropartita = 0
        x.Righe(0).SottocontoContropartita = 0
        x.Righe(0).Numero = 0

        Dim Indice As Integer = 1


        If CDbl(Txt_Imponibile1.Text) > 0 Then

            Dim Xs As New Cls_IVA

            Xs.Leggi(Session("DC_TABELLE"), DD_IVA1.SelectedValue)

            Dim Imponibile As Double
            Imponibile = Txt_Imponibile1.Text

            x.Righe(Indice) = New Cls_MovimentiContabiliRiga
            x.Righe(Indice).Imponibile = Txt_Imponibile1.Text
            x.Righe(Indice).CodiceIVA = DD_IVA1.SelectedValue
            x.Righe(Indice).Importo = Txt_Imposta1.Text

            x.Righe(Indice).Detraibile = ""
            x.Righe(Indice).Numero = 0
            x.Righe(Indice).Tipo = "IV"
            x.Righe(Indice).Invisibile = 1
            x.Righe(Indice).Segno = "+"

            Dim Cau As New Cls_CausaleContabile
            Dim DareAvere As String

            Cau.Leggi(Session("DC_TABELLE"), Dd_CausaleContabile.SelectedValue)

            If Cau.Tipo = "R" Then
                Mastro = Cau.Righe(1).Mastro
                Conto = Cau.Righe(1).Conto
                Sottoconto = Cau.Righe(1).Sottoconto
                DareAvere = Cau.Righe(1).DareAvere
            Else
                Mastro = Cau.Righe(2).Mastro
                Conto = Cau.Righe(2).Conto
                Sottoconto = Cau.Righe(2).Sottoconto
                DareAvere = Cau.Righe(2).DareAvere
            End If

            x.Righe(Indice).MastroPartita = Mastro
            x.Righe(Indice).ContoPartita = Conto
            x.Righe(Indice).SottocontoPartita = Sottoconto
            If Rb_Dare1.Checked = True Then
                x.Righe(Indice).DareAvere = "D"
            Else
                x.Righe(Indice).DareAvere = "A"
            End If
            x.Righe(Indice).Descrizione = ""

            If Cau.Tipo = "R" Then
                x.Righe(Indice).RigaDaCausale = 2
            Else
                x.Righe(Indice).RigaDaCausale = 3
            End If

            Vettore = SplitWords(Txt_ClienteFornitore.Text)

            If Vettore.Length > 2 Then
                Mastro = Val(Vettore(0))
                Conto = Val(Vettore(Indice))
                Sottoconto = Val(Vettore(2))
            End If

            x.Righe(Indice).MastroContropartita = Mastro
            x.Righe(Indice).ContoContropartita = Conto
            x.Righe(Indice).SottocontoContropartita = Sottoconto

            Indice = Indice + 1

            x.Righe(Indice) = New Cls_MovimentiContabiliRiga
            x.Righe(Indice).Imponibile = 0
            x.Righe(Indice).CodiceIVA = ""
            x.Righe(Indice).Importo = Txt_Imposta1.Text

            x.Righe(Indice).Detraibile = ""
            x.Righe(Indice).Numero = 0
            x.Righe(Indice).Tipo = ""
            x.Righe(Indice).Invisibile = 1
            x.Righe(Indice).Segno = "+"



            If Cau.Tipo = "R" Then
                Mastro = Cau.Righe(1).Mastro
                Conto = Cau.Righe(1).Conto
                Sottoconto = Cau.Righe(1).Sottoconto
                DareAvere = Cau.Righe(1).DareAvere
            Else
                Mastro = Cau.Righe(2).Mastro
                Conto = Cau.Righe(2).Conto
                Sottoconto = Cau.Righe(2).Sottoconto
                DareAvere = Cau.Righe(2).DareAvere
            End If

            x.Righe(Indice).MastroPartita = Mastro
            x.Righe(Indice).ContoPartita = Conto
            x.Righe(Indice).SottocontoPartita = Sottoconto

            If Rb_Dare1.Checked = True Then
                x.Righe(Indice).DareAvere = "A"
            Else
                x.Righe(Indice).DareAvere = "D"
            End If
            x.Righe(Indice).Descrizione = ""

            If Cau.Tipo = "R" Then
                x.Righe(Indice).RigaDaCausale = 2
            Else
                x.Righe(Indice).RigaDaCausale = 3
            End If

            Vettore = SplitWords(Txt_ClienteFornitore.Text)

            If Vettore.Length > 2 Then
                Mastro = Val(Vettore(0))
                Conto = Val(Vettore(1))
                Sottoconto = Val(Vettore(2))
            End If

            x.Righe(Indice).MastroContropartita = Mastro
            x.Righe(Indice).ContoContropartita = Conto
            x.Righe(Indice).SottocontoContropartita = Sottoconto

            Indice = Indice + 1
            'Indice = CreaRighe(Modulo.MathRound(Imponibile * Xs.Aliquota, 2), Imponibile, Mastro & " " & Conto & " " & Sottoconto, DareAvere, x, Indice, DD_IVA1.SelectedValue)
        End If


        If CDbl(Txt_Ritenuta1.Text) > 0 Then
            Dim Xs As New ClsRitenuta

            Dim Cau As New Cls_CausaleContabile
            Dim DareAvere As String

            Cau.Leggi(Session("DC_TABELLE"), Dd_CausaleContabile.SelectedValue)

            Xs.Codice = Cau.Ritenuta
            Xs.Leggi(Session("DC_TABELLE"))

            Dim ImportoRitenuta As Double
            ImportoRitenuta = Txt_Ritenuta1.Text

            x.Righe(Indice) = New Cls_MovimentiContabiliRiga
            x.Righe(Indice).Imponibile = Modulo.MathRound(ImportoRitenuta * (100 / (Xs.Percentuale * 100)), 2)
            x.Righe(Indice).CodiceRitenuta = Cau.Ritenuta
            x.Righe(Indice).Importo = ImportoRitenuta

            x.Righe(Indice).Detraibile = ""
            x.Righe(Indice).Numero = 0
            x.Righe(Indice).Tipo = "RI"
            x.Righe(Indice).Invisibile = 0
            x.Righe(Indice).Segno = "+"



            Mastro = Cau.Righe(8).Mastro
            Conto = Cau.Righe(8).Conto
            Sottoconto = Cau.Righe(8).Sottoconto
            DareAvere = Cau.Righe(8).DareAvere


            x.Righe(Indice).MastroPartita = Mastro
            x.Righe(Indice).ContoPartita = Conto
            x.Righe(Indice).SottocontoPartita = Sottoconto
            If Cau.Righe(9).DareAvere = "D" Then
                x.Righe(Indice).DareAvere = "D"
            Else
                x.Righe(Indice).DareAvere = "A"
            End If

            x.Righe(Indice).Descrizione = ""


            x.Righe(Indice).RigaDaCausale = 9


            Vettore = SplitWords(Txt_ClienteFornitore.Text)

            If Vettore.Length > 2 Then
                Mastro = Val(Vettore(0))
                Conto = Val(Vettore(Indice))
                Sottoconto = Val(Vettore(2))
            End If

            x.Righe(Indice).MastroContropartita = Mastro
            x.Righe(Indice).ContoContropartita = Conto
            x.Righe(Indice).SottocontoContropartita = Sottoconto

            Indice = Indice + 1

            x.Righe(Indice) = New Cls_MovimentiContabiliRiga
            x.Righe(Indice).Imponibile = Modulo.MathRound(ImportoRitenuta * (100 / (Xs.Percentuale * 100)), 2)
            x.Righe(Indice).CodiceRitenuta = Cau.Ritenuta
            x.Righe(Indice).Importo = ImportoRitenuta

            x.Righe(Indice).Detraibile = ""
            x.Righe(Indice).Numero = 0
            x.Righe(Indice).Tipo = "RI"
            x.Righe(Indice).Invisibile = 0
            x.Righe(Indice).Segno = "+"


            Mastro = Cau.Righe(8).Mastro
            Conto = Cau.Righe(8).Conto
            Sottoconto = Cau.Righe(8).Sottoconto
            DareAvere = Cau.Righe(8).DareAvere


            x.Righe(Indice).MastroContropartita = Mastro
            x.Righe(Indice).ContoContropartita = Conto
            x.Righe(Indice).SottocontoContropartita = Sottoconto

            If Cau.Righe(9).DareAvere = "D" Then
                x.Righe(Indice).DareAvere = "A"
            Else
                x.Righe(Indice).DareAvere = "D"
            End If
            x.Righe(Indice).Descrizione = ""

            x.Righe(Indice).RigaDaCausale = 9

            Vettore = SplitWords(Txt_ClienteFornitore.Text)

            If Vettore.Length > 2 Then
                Mastro = Val(Vettore(0))
                Conto = Val(Vettore(1))
                Sottoconto = Val(Vettore(2))
            End If

            x.Righe(Indice).MastroPartita = Mastro
            x.Righe(Indice).ContoPartita = Conto
            x.Righe(Indice).SottocontoPartita = Sottoconto

            Indice = Indice + 1
        End If

        If CDbl(Txt_Imponibile2.Text) > 0 Then

            Dim Xs As New Cls_IVA

            Xs.Leggi(Session("DC_TABELLE"), DD_IVA2.SelectedValue)

            Dim Imponibile As Double
            Imponibile = Txt_Imponibile2.Text

            x.Righe(Indice) = New Cls_MovimentiContabiliRiga
            x.Righe(Indice).Imponibile = Txt_Imponibile2.Text
            x.Righe(Indice).CodiceIVA = DD_IVA2.SelectedValue
            x.Righe(Indice).Importo = Txt_Imposta2.Text

            x.Righe(Indice).Detraibile = ""
            x.Righe(Indice).Numero = 0
            x.Righe(Indice).Tipo = "IV"
            x.Righe(Indice).Invisibile = 1
            x.Righe(Indice).Segno = "+"

            Dim Cau As New Cls_CausaleContabile
            Dim DareAvere As String

            Cau.Leggi(Session("DC_TABELLE"), Dd_CausaleContabile.SelectedValue)

            If Cau.Tipo = "R" Then
                Mastro = Cau.Righe(1).Mastro
                Conto = Cau.Righe(1).Conto
                Sottoconto = Cau.Righe(1).Sottoconto
                DareAvere = Cau.Righe(1).DareAvere
            Else
                Mastro = Cau.Righe(2).Mastro
                Conto = Cau.Righe(2).Conto
                Sottoconto = Cau.Righe(2).Sottoconto
                DareAvere = Cau.Righe(2).DareAvere
            End If

            x.Righe(Indice).MastroPartita = Mastro
            x.Righe(Indice).ContoPartita = Conto
            x.Righe(Indice).SottocontoPartita = Sottoconto
            If Rb_Dare2.Checked = True Then
                x.Righe(Indice).DareAvere = "D"
            Else
                x.Righe(Indice).DareAvere = "A"
            End If

            x.Righe(Indice).Descrizione = ""

            If Cau.Tipo = "R" Then
                x.Righe(Indice).RigaDaCausale = 2
            Else
                x.Righe(Indice).RigaDaCausale = 3
            End If

            Vettore = SplitWords(Txt_ClienteFornitore.Text)

            If Vettore.Length > 2 Then
                Mastro = Val(Vettore(0))
                Conto = Val(Vettore(Indice))
                Sottoconto = Val(Vettore(2))
            End If

            x.Righe(Indice).MastroContropartita = Mastro
            x.Righe(Indice).ContoContropartita = Conto
            x.Righe(Indice).SottocontoContropartita = Sottoconto

            Indice = Indice + 1

            x.Righe(Indice) = New Cls_MovimentiContabiliRiga
            x.Righe(Indice).Imponibile = 0
            x.Righe(Indice).CodiceIVA = ""
            x.Righe(Indice).Importo = Txt_Imposta2.Text

            x.Righe(Indice).Detraibile = ""
            x.Righe(Indice).Numero = 0
            x.Righe(Indice).Tipo = ""
            x.Righe(Indice).Invisibile = 1
            x.Righe(Indice).Segno = "+"



            If Cau.Tipo = "R" Then
                Mastro = Cau.Righe(1).Mastro
                Conto = Cau.Righe(1).Conto
                Sottoconto = Cau.Righe(1).Sottoconto
                DareAvere = Cau.Righe(1).DareAvere
            Else
                Mastro = Cau.Righe(2).Mastro
                Conto = Cau.Righe(2).Conto
                Sottoconto = Cau.Righe(2).Sottoconto
                DareAvere = Cau.Righe(2).DareAvere
            End If

            x.Righe(Indice).MastroPartita = Mastro
            x.Righe(Indice).ContoPartita = Conto
            x.Righe(Indice).SottocontoPartita = Sottoconto

            If Rb_Dare2.Checked = True Then
                x.Righe(Indice).DareAvere = "A"
            Else
                x.Righe(Indice).DareAvere = "D"
            End If
            x.Righe(Indice).Descrizione = ""

            If Cau.Tipo = "R" Then
                x.Righe(Indice).RigaDaCausale = 2
            Else
                x.Righe(Indice).RigaDaCausale = 3
            End If

            Vettore = SplitWords(Txt_ClienteFornitore.Text)

            If Vettore.Length > 2 Then
                Mastro = Val(Vettore(0))
                Conto = Val(Vettore(1))
                Sottoconto = Val(Vettore(2))
            End If

            x.Righe(Indice).MastroContropartita = Mastro
            x.Righe(Indice).ContoContropartita = Conto
            x.Righe(Indice).SottocontoContropartita = Sottoconto

            Indice = Indice + 1
            'Indice = CreaRighe(Modulo.MathRound(Imponibile * Xs.Aliquota, 2), Imponibile, Mastro & " " & Conto & " " & Sottoconto, DareAvere, x, Indice, DD_IVA1.SelectedValue)
        End If

        'If CDbl(Txt_Ritenuta2.Text) > 0 Then
        '    Dim Xs As New ClsRitenuta

        '    Dim Cau As New Cls_CausaleContabile
        '    Dim DareAvere As String

        '    Cau.Leggi(Session("DC_TABELLE"), Dd_CausaleContabile.SelectedValue)

        '    Xs.Codice = Cau.Ritenuta
        '    Xs.Leggi(Session("DC_TABELLE"))

        '    Dim ImportoRitenuta As Double
        '    ImportoRitenuta = Txt_Ritenuta2.Text

        '    x.Righe(Indice) = New Cls_MovimentiContabiliRiga
        '    x.Righe(Indice).Imponibile = Modulo.MathRound(ImportoRitenuta * (100 / (Xs.Percentuale * 100)), 2)
        '    x.Righe(Indice).CodiceRitenuta = Cau.Ritenuta
        '    x.Righe(Indice).Importo = ImportoRitenuta

        '    x.Righe(Indice).Detraibile = ""
        '    x.Righe(Indice).Numero = 0
        '    x.Righe(Indice).Tipo = "RI"
        '    x.Righe(Indice).Invisibile = 0
        '    x.Righe(Indice).Segno = "+"



        '    Mastro = Cau.Righe(8).Mastro
        '    Conto = Cau.Righe(8).Conto
        '    Sottoconto = Cau.Righe(8).Sottoconto
        '    DareAvere = Cau.Righe(8).DareAvere


        '    x.Righe(Indice).MastroPartita = Mastro
        '    x.Righe(Indice).ContoPartita = Conto
        '    x.Righe(Indice).SottocontoPartita = Sottoconto
        '    If Cau.Righe(9).DareAvere = "D" Then
        '        x.Righe(Indice).DareAvere = "D"
        '    Else
        '        x.Righe(Indice).DareAvere = "A"
        '    End If

        '    x.Righe(Indice).Descrizione = ""


        '    x.Righe(Indice).RigaDaCausale = 9


        '    Vettore = SplitWords(Txt_ClienteFornitore.Text)

        '    If Vettore.Length > 2 Then
        '        Mastro = Val(Vettore(0))
        '        Conto = Val(Vettore(Indice))
        '        Sottoconto = Val(Vettore(2))
        '    End If

        '    x.Righe(Indice).MastroContropartita = Mastro
        '    x.Righe(Indice).ContoContropartita = Conto
        '    x.Righe(Indice).SottocontoContropartita = Sottoconto

        '    Indice = Indice + 1

        '    x.Righe(Indice) = New Cls_MovimentiContabiliRiga
        '    x.Righe(Indice).Imponibile = Modulo.MathRound(ImportoRitenuta * (100 / (Xs.Percentuale * 100)), 2)
        '    x.Righe(Indice).CodiceRitenuta = Cau.Ritenuta
        '    x.Righe(Indice).Importo = ImportoRitenuta

        '    x.Righe(Indice).Detraibile = ""
        '    x.Righe(Indice).Numero = 0
        '    x.Righe(Indice).Tipo = "RI"
        '    x.Righe(Indice).Invisibile = 0
        '    x.Righe(Indice).Segno = "+"


        '    Mastro = Cau.Righe(8).Mastro
        '    Conto = Cau.Righe(8).Conto
        '    Sottoconto = Cau.Righe(8).Sottoconto
        '    DareAvere = Cau.Righe(8).DareAvere


        '    x.Righe(Indice).MastroContropartita = Mastro
        '    x.Righe(Indice).ContoContropartita = Conto
        '    x.Righe(Indice).SottocontoContropartita = Sottoconto

        '    If Cau.Righe(9).DareAvere = "D" Then
        '        x.Righe(Indice).DareAvere = "A"
        '    Else
        '        x.Righe(Indice).DareAvere = "D"
        '    End If
        '    x.Righe(Indice).Descrizione = ""

        '    x.Righe(Indice).RigaDaCausale = 9

        '    Vettore = SplitWords(Txt_ClienteFornitore.Text)

        '    If Vettore.Length > 2 Then
        '        Mastro = Val(Vettore(0))
        '        Conto = Val(Vettore(1))
        '        Sottoconto = Val(Vettore(2))
        '    End If

        '    x.Righe(Indice).MastroPartita = Mastro
        '    x.Righe(Indice).ContoPartita = Conto
        '    x.Righe(Indice).SottocontoPartita = Sottoconto

        '    Indice = Indice + 1
        'End If

        If CDbl(Txt_Imponibile3.Text) > 0 Then

            Dim Xs As New Cls_IVA

            Xs.Leggi(Session("DC_TABELLE"), DD_IVA3.SelectedValue)

            Dim Imponibile As Double
            Imponibile = Txt_Imponibile3.Text

            x.Righe(Indice) = New Cls_MovimentiContabiliRiga
            x.Righe(Indice).Imponibile = Txt_Imponibile3.Text
            x.Righe(Indice).CodiceIVA = DD_IVA3.SelectedValue
            x.Righe(Indice).Importo = Txt_Imposta3.Text

            x.Righe(Indice).Detraibile = ""
            x.Righe(Indice).Numero = 0
            x.Righe(Indice).Tipo = "IV"
            x.Righe(Indice).Invisibile = 1
            x.Righe(Indice).Segno = "+"

            Dim Cau As New Cls_CausaleContabile
            Dim DareAvere As String

            Cau.Leggi(Session("DC_TABELLE"), Dd_CausaleContabile.SelectedValue)

            If Cau.Tipo = "R" Then
                Mastro = Cau.Righe(1).Mastro
                Conto = Cau.Righe(1).Conto
                Sottoconto = Cau.Righe(1).Sottoconto
                DareAvere = Cau.Righe(1).DareAvere
            Else
                Mastro = Cau.Righe(2).Mastro
                Conto = Cau.Righe(2).Conto
                Sottoconto = Cau.Righe(2).Sottoconto
                DareAvere = Cau.Righe(2).DareAvere
            End If

            x.Righe(Indice).MastroPartita = Mastro
            x.Righe(Indice).ContoPartita = Conto
            x.Righe(Indice).SottocontoPartita = Sottoconto

            If Rb_Dare3.Checked = True Then
                x.Righe(Indice).DareAvere = "D"
            Else
                x.Righe(Indice).DareAvere = "A"
            End If

            x.Righe(Indice).Descrizione = ""

            If Cau.Tipo = "R" Then
                x.Righe(Indice).RigaDaCausale = 2
            Else
                x.Righe(Indice).RigaDaCausale = 3
            End If

            Vettore = SplitWords(Txt_ClienteFornitore.Text)

            If Vettore.Length > 2 Then
                Mastro = Val(Vettore(0))
                Conto = Val(Vettore(Indice))
                Sottoconto = Val(Vettore(2))
            End If

            x.Righe(Indice).MastroContropartita = Mastro
            x.Righe(Indice).ContoContropartita = Conto
            x.Righe(Indice).SottocontoContropartita = Sottoconto

            Indice = Indice + 1

            x.Righe(Indice) = New Cls_MovimentiContabiliRiga
            x.Righe(Indice).Imponibile = 0
            x.Righe(Indice).CodiceIVA = ""
            x.Righe(Indice).Importo = Txt_Imposta3.Text

            x.Righe(Indice).Detraibile = ""
            x.Righe(Indice).Numero = 0
            x.Righe(Indice).Tipo = ""
            x.Righe(Indice).Invisibile = 1
            x.Righe(Indice).Segno = "+"



            If Cau.Tipo = "R" Then
                Mastro = Cau.Righe(1).Mastro
                Conto = Cau.Righe(1).Conto
                Sottoconto = Cau.Righe(1).Sottoconto
                DareAvere = Cau.Righe(1).DareAvere
            Else
                Mastro = Cau.Righe(2).Mastro
                Conto = Cau.Righe(2).Conto
                Sottoconto = Cau.Righe(2).Sottoconto
                DareAvere = Cau.Righe(2).DareAvere
            End If

            x.Righe(Indice).MastroPartita = Mastro
            x.Righe(Indice).ContoPartita = Conto
            x.Righe(Indice).SottocontoPartita = Sottoconto

            If Rb_Dare3.Checked = True Then
                x.Righe(Indice).DareAvere = "A"
            Else
                x.Righe(Indice).DareAvere = "D"
            End If
            x.Righe(Indice).Descrizione = ""

            If Cau.Tipo = "R" Then
                x.Righe(Indice).RigaDaCausale = 2
            Else
                x.Righe(Indice).RigaDaCausale = 3
            End If

            Vettore = SplitWords(Txt_ClienteFornitore.Text)

            If Vettore.Length > 2 Then
                Mastro = Val(Vettore(0))
                Conto = Val(Vettore(1))
                Sottoconto = Val(Vettore(2))
            End If

            x.Righe(Indice).MastroContropartita = Mastro
            x.Righe(Indice).ContoContropartita = Conto
            x.Righe(Indice).SottocontoContropartita = Sottoconto

            Indice = Indice + 1
            'Indice = CreaRighe(Modulo.MathRound(Imponibile * Xs.Aliquota, 2), Imponibile, Mastro & " " & Conto & " " & Sottoconto, DareAvere, x, Indice, DD_IVA1.SelectedValue)
        End If

        'If CDbl(Txt_Ritenuta3.Text) > 0 Then
        '    Dim Xs As New ClsRitenuta

        '    Dim Cau As New Cls_CausaleContabile
        '    Dim DareAvere As String

        '    Cau.Leggi(Session("DC_TABELLE"), Dd_CausaleContabile.SelectedValue)

        '    Xs.Codice = Cau.Ritenuta
        '    Xs.Leggi(Session("DC_TABELLE"))

        '    Dim ImportoRitenuta As Double
        '    ImportoRitenuta = Txt_Ritenuta3.Text

        '    x.Righe(Indice) = New Cls_MovimentiContabiliRiga
        '    x.Righe(Indice).Imponibile = Modulo.MathRound(ImportoRitenuta * (100 / (Xs.Percentuale * 100)), 2)
        '    x.Righe(Indice).CodiceRitenuta = Cau.Ritenuta
        '    x.Righe(Indice).Importo = ImportoRitenuta

        '    x.Righe(Indice).Detraibile = ""
        '    x.Righe(Indice).Numero = 0
        '    x.Righe(Indice).Tipo = "RI"
        '    x.Righe(Indice).Invisibile = 0
        '    x.Righe(Indice).Segno = "+"



        '    Mastro = Cau.Righe(8).Mastro
        '    Conto = Cau.Righe(8).Conto
        '    Sottoconto = Cau.Righe(8).Sottoconto
        '    DareAvere = Cau.Righe(8).DareAvere


        '    x.Righe(Indice).MastroPartita = Mastro
        '    x.Righe(Indice).ContoPartita = Conto
        '    x.Righe(Indice).SottocontoPartita = Sottoconto
        '    If Cau.Righe(9).DareAvere = "D" Then
        '        x.Righe(Indice).DareAvere = "D"
        '    Else
        '        x.Righe(Indice).DareAvere = "A"
        '    End If

        '    x.Righe(Indice).Descrizione = ""


        '    x.Righe(Indice).RigaDaCausale = 9


        '    Vettore = SplitWords(Txt_ClienteFornitore.Text)

        '    If Vettore.Length > 2 Then
        '        Mastro = Val(Vettore(0))
        '        Conto = Val(Vettore(Indice))
        '        Sottoconto = Val(Vettore(2))
        '    End If

        '    x.Righe(Indice).MastroContropartita = Mastro
        '    x.Righe(Indice).ContoContropartita = Conto
        '    x.Righe(Indice).SottocontoContropartita = Sottoconto

        '    Indice = Indice + 1

        '    x.Righe(Indice) = New Cls_MovimentiContabiliRiga
        '    x.Righe(Indice).Imponibile = Modulo.MathRound(ImportoRitenuta * (100 / (Xs.Percentuale * 100)), 2)
        '    x.Righe(Indice).CodiceRitenuta = Cau.Ritenuta
        '    x.Righe(Indice).Importo = ImportoRitenuta

        '    x.Righe(Indice).Detraibile = ""
        '    x.Righe(Indice).Numero = 0
        '    x.Righe(Indice).Tipo = "RI"
        '    x.Righe(Indice).Invisibile = 0
        '    x.Righe(Indice).Segno = "+"


        '    Mastro = Cau.Righe(8).Mastro
        '    Conto = Cau.Righe(8).Conto
        '    Sottoconto = Cau.Righe(8).Sottoconto
        '    DareAvere = Cau.Righe(8).DareAvere


        '    x.Righe(Indice).MastroContropartita = Mastro
        '    x.Righe(Indice).ContoContropartita = Conto
        '    x.Righe(Indice).SottocontoContropartita = Sottoconto

        '    If Cau.Righe(9).DareAvere = "D" Then
        '        x.Righe(Indice).DareAvere = "A"
        '    Else
        '        x.Righe(Indice).DareAvere = "D"
        '    End If
        '    x.Righe(Indice).Descrizione = ""

        '    x.Righe(Indice).RigaDaCausale = 9

        '    Vettore = SplitWords(Txt_ClienteFornitore.Text)

        '    If Vettore.Length > 2 Then
        '        Mastro = Val(Vettore(0))
        '        Conto = Val(Vettore(1))
        '        Sottoconto = Val(Vettore(2))
        '    End If

        '    x.Righe(Indice).MastroPartita = Mastro
        '    x.Righe(Indice).ContoPartita = Conto
        '    x.Righe(Indice).SottocontoPartita = Sottoconto

        '    Indice = Indice + 1
        'End If



        If CDbl(Txt_Imponibile4.Text) > 0 Then

            Dim Xs As New Cls_IVA

            Xs.Leggi(Session("DC_TABELLE"), DD_IVA4.SelectedValue)

            Dim Imponibile As Double
            Imponibile = Txt_Imponibile4.Text

            x.Righe(Indice) = New Cls_MovimentiContabiliRiga
            x.Righe(Indice).Imponibile = Txt_Imponibile4.Text
            x.Righe(Indice).CodiceIVA = DD_IVA4.SelectedValue
            x.Righe(Indice).Importo = Txt_Imposta4.Text

            x.Righe(Indice).Detraibile = ""
            x.Righe(Indice).Numero = 0
            x.Righe(Indice).Tipo = "IV"
            x.Righe(Indice).Invisibile = 1
            x.Righe(Indice).Segno = "+"

            Dim Cau As New Cls_CausaleContabile
            Dim DareAvere As String

            Cau.Leggi(Session("DC_TABELLE"), Dd_CausaleContabile.SelectedValue)

            If Cau.Tipo = "R" Then
                Mastro = Cau.Righe(1).Mastro
                Conto = Cau.Righe(1).Conto
                Sottoconto = Cau.Righe(1).Sottoconto
                DareAvere = Cau.Righe(1).DareAvere
            Else
                Mastro = Cau.Righe(2).Mastro
                Conto = Cau.Righe(2).Conto
                Sottoconto = Cau.Righe(2).Sottoconto
                DareAvere = Cau.Righe(2).DareAvere
            End If

            x.Righe(Indice).MastroPartita = Mastro
            x.Righe(Indice).ContoPartita = Conto
            x.Righe(Indice).SottocontoPartita = Sottoconto

            If Rb_Dare4.Checked = True Then
                x.Righe(Indice).DareAvere = "D"
            Else
                x.Righe(Indice).DareAvere = "A"
            End If

            x.Righe(Indice).Descrizione = ""

            If Cau.Tipo = "R" Then
                x.Righe(Indice).RigaDaCausale = 2
            Else
                x.Righe(Indice).RigaDaCausale = 3
            End If

            Vettore = SplitWords(Txt_ClienteFornitore.Text)

            If Vettore.Length > 2 Then
                Mastro = Val(Vettore(0))
                Conto = Val(Vettore(Indice))
                Sottoconto = Val(Vettore(2))
            End If

            x.Righe(Indice).MastroContropartita = Mastro
            x.Righe(Indice).ContoContropartita = Conto
            x.Righe(Indice).SottocontoContropartita = Sottoconto

            Indice = Indice + 1

            x.Righe(Indice) = New Cls_MovimentiContabiliRiga
            x.Righe(Indice).Imponibile = 0
            x.Righe(Indice).CodiceIVA = ""
            x.Righe(Indice).Importo = Txt_Imposta4.Text

            x.Righe(Indice).Detraibile = ""
            x.Righe(Indice).Numero = 0
            x.Righe(Indice).Tipo = ""
            x.Righe(Indice).Invisibile = 1
            x.Righe(Indice).Segno = "+"



            If Cau.Tipo = "R" Then
                Mastro = Cau.Righe(1).Mastro
                Conto = Cau.Righe(1).Conto
                Sottoconto = Cau.Righe(1).Sottoconto
                DareAvere = Cau.Righe(1).DareAvere
            Else
                Mastro = Cau.Righe(2).Mastro
                Conto = Cau.Righe(2).Conto
                Sottoconto = Cau.Righe(2).Sottoconto
                DareAvere = Cau.Righe(2).DareAvere
            End If

            x.Righe(Indice).MastroPartita = Mastro
            x.Righe(Indice).ContoPartita = Conto
            x.Righe(Indice).SottocontoPartita = Sottoconto

            If Rb_Dare4.Checked = True Then
                x.Righe(Indice).DareAvere = "A"
            Else
                x.Righe(Indice).DareAvere = "D"
            End If
            x.Righe(Indice).Descrizione = ""

            If Cau.Tipo = "R" Then
                x.Righe(Indice).RigaDaCausale = 2
            Else
                x.Righe(Indice).RigaDaCausale = 3
            End If

            Vettore = SplitWords(Txt_ClienteFornitore.Text)

            If Vettore.Length > 2 Then
                Mastro = Val(Vettore(0))
                Conto = Val(Vettore(1))
                Sottoconto = Val(Vettore(2))
            End If

            x.Righe(Indice).MastroContropartita = Mastro
            x.Righe(Indice).ContoContropartita = Conto
            x.Righe(Indice).SottocontoContropartita = Sottoconto

            Indice = Indice + 1
            'Indice = CreaRighe(Modulo.MathRound(Imponibile * Xs.Aliquota, 2), Imponibile, Mastro & " " & Conto & " " & Sottoconto, DareAvere, x, Indice, DD_IVA1.SelectedValue)
        End If

        'If CDbl(Txt_Ritenuta4.Text) > 0 Then
        '    Dim Xs As New ClsRitenuta

        '    Dim Cau As New Cls_CausaleContabile
        '    Dim DareAvere As String

        '    Cau.Leggi(Session("DC_TABELLE"), Dd_CausaleContabile.SelectedValue)

        '    Xs.Codice = Cau.Ritenuta
        '    Xs.Leggi(Session("DC_TABELLE"))

        '    Dim ImportoRitenuta As Double
        '    ImportoRitenuta = Txt_Ritenuta4.Text

        '    x.Righe(Indice) = New Cls_MovimentiContabiliRiga
        '    x.Righe(Indice).Imponibile = Modulo.MathRound(ImportoRitenuta * (100 / (Xs.Percentuale * 100)), 2)
        '    x.Righe(Indice).CodiceRitenuta = Cau.Ritenuta
        '    x.Righe(Indice).Importo = ImportoRitenuta

        '    x.Righe(Indice).Detraibile = ""
        '    x.Righe(Indice).Numero = 0
        '    x.Righe(Indice).Tipo = "RI"
        '    x.Righe(Indice).Invisibile = 0
        '    x.Righe(Indice).Segno = "+"



        '    Mastro = Cau.Righe(8).Mastro
        '    Conto = Cau.Righe(8).Conto
        '    Sottoconto = Cau.Righe(8).Sottoconto
        '    DareAvere = Cau.Righe(8).DareAvere


        '    x.Righe(Indice).MastroPartita = Mastro
        '    x.Righe(Indice).ContoPartita = Conto
        '    x.Righe(Indice).SottocontoPartita = Sottoconto
        '    If Cau.Righe(9).DareAvere = "D" Then
        '        x.Righe(Indice).DareAvere = "D"
        '    Else
        '        x.Righe(Indice).DareAvere = "A"
        '    End If

        '    x.Righe(Indice).Descrizione = ""


        '    x.Righe(Indice).RigaDaCausale = 9


        '    Vettore = SplitWords(Txt_ClienteFornitore.Text)

        '    If Vettore.Length > 2 Then
        '        Mastro = Val(Vettore(0))
        '        Conto = Val(Vettore(Indice))
        '        Sottoconto = Val(Vettore(2))
        '    End If

        '    x.Righe(Indice).MastroContropartita = Mastro
        '    x.Righe(Indice).ContoContropartita = Conto
        '    x.Righe(Indice).SottocontoContropartita = Sottoconto

        '    Indice = Indice + 1

        '    x.Righe(Indice) = New Cls_MovimentiContabiliRiga
        '    x.Righe(Indice).Imponibile = Modulo.MathRound(ImportoRitenuta * (100 / (Xs.Percentuale * 100)), 2)
        '    x.Righe(Indice).CodiceRitenuta = Cau.Ritenuta
        '    x.Righe(Indice).Importo = ImportoRitenuta

        '    x.Righe(Indice).Detraibile = ""
        '    x.Righe(Indice).Numero = 0
        '    x.Righe(Indice).Tipo = "RI"
        '    x.Righe(Indice).Invisibile = 0
        '    x.Righe(Indice).Segno = "+"


        '    Mastro = Cau.Righe(8).Mastro
        '    Conto = Cau.Righe(8).Conto
        '    Sottoconto = Cau.Righe(8).Sottoconto
        '    DareAvere = Cau.Righe(8).DareAvere


        '    x.Righe(Indice).MastroContropartita = Mastro
        '    x.Righe(Indice).ContoContropartita = Conto
        '    x.Righe(Indice).SottocontoContropartita = Sottoconto

        '    If Cau.Righe(9).DareAvere = "D" Then
        '        x.Righe(Indice).DareAvere = "A"
        '    Else
        '        x.Righe(Indice).DareAvere = "D"
        '    End If
        '    x.Righe(Indice).Descrizione = ""

        '    x.Righe(Indice).RigaDaCausale = 9

        '    Vettore = SplitWords(Txt_ClienteFornitore.Text)

        '    If Vettore.Length > 2 Then
        '        Mastro = Val(Vettore(0))
        '        Conto = Val(Vettore(1))
        '        Sottoconto = Val(Vettore(2))
        '    End If

        '    x.Righe(Indice).MastroPartita = Mastro
        '    x.Righe(Indice).ContoPartita = Conto
        '    x.Righe(Indice).SottocontoPartita = Sottoconto

        '    Indice = Indice + 1
        'End If

        If CDbl(Txt_Imponibile5.Text) > 0 Then

            Dim Xs As New Cls_IVA

            Xs.Leggi(Session("DC_TABELLE"), DD_IVA5.SelectedValue)

            Dim Imponibile As Double
            Imponibile = Txt_Imponibile5.Text

            x.Righe(Indice) = New Cls_MovimentiContabiliRiga
            x.Righe(Indice).Imponibile = Txt_Imponibile5.Text
            x.Righe(Indice).CodiceIVA = DD_IVA5.SelectedValue
            x.Righe(Indice).Importo = Txt_Imposta5.Text

            x.Righe(Indice).Detraibile = ""
            x.Righe(Indice).Numero = 0
            x.Righe(Indice).Tipo = "IV"
            x.Righe(Indice).Invisibile = 1
            x.Righe(Indice).Segno = "+"

            Dim Cau As New Cls_CausaleContabile
            Dim DareAvere As String

            Cau.Leggi(Session("DC_TABELLE"), Dd_CausaleContabile.SelectedValue)

            If Cau.Tipo = "R" Then
                Mastro = Cau.Righe(1).Mastro
                Conto = Cau.Righe(1).Conto
                Sottoconto = Cau.Righe(1).Sottoconto
                DareAvere = Cau.Righe(1).DareAvere
            Else
                Mastro = Cau.Righe(2).Mastro
                Conto = Cau.Righe(2).Conto
                Sottoconto = Cau.Righe(2).Sottoconto
                DareAvere = Cau.Righe(2).DareAvere
            End If

            x.Righe(Indice).MastroPartita = Mastro
            x.Righe(Indice).ContoPartita = Conto
            x.Righe(Indice).SottocontoPartita = Sottoconto
            If Rb_Dare5.Checked = True Then
                x.Righe(Indice).DareAvere = "D"
            Else
                x.Righe(Indice).DareAvere = "A"
            End If
            x.Righe(Indice).Descrizione = ""

            If Cau.Tipo = "R" Then
                x.Righe(Indice).RigaDaCausale = 2
            Else
                x.Righe(Indice).RigaDaCausale = 3
            End If

            Vettore = SplitWords(Txt_ClienteFornitore.Text)

            If Vettore.Length > 2 Then
                Mastro = Val(Vettore(0))
                Conto = Val(Vettore(Indice))
                Sottoconto = Val(Vettore(2))
            End If

            x.Righe(Indice).MastroContropartita = Mastro
            x.Righe(Indice).ContoContropartita = Conto
            x.Righe(Indice).SottocontoContropartita = Sottoconto

            Indice = Indice + 1

            x.Righe(Indice) = New Cls_MovimentiContabiliRiga
            x.Righe(Indice).Imponibile = 0
            x.Righe(Indice).CodiceIVA = ""
            x.Righe(Indice).Importo = Txt_Imposta5.Text

            x.Righe(Indice).Detraibile = ""
            x.Righe(Indice).Numero = 0
            x.Righe(Indice).Tipo = ""
            x.Righe(Indice).Invisibile = 1
            x.Righe(Indice).Segno = "+"



            If Cau.Tipo = "R" Then
                Mastro = Cau.Righe(1).Mastro
                Conto = Cau.Righe(1).Conto
                Sottoconto = Cau.Righe(1).Sottoconto
                DareAvere = Cau.Righe(1).DareAvere
            Else
                Mastro = Cau.Righe(2).Mastro
                Conto = Cau.Righe(2).Conto
                Sottoconto = Cau.Righe(2).Sottoconto
                DareAvere = Cau.Righe(2).DareAvere
            End If

            x.Righe(Indice).MastroPartita = Mastro
            x.Righe(Indice).ContoPartita = Conto
            x.Righe(Indice).SottocontoPartita = Sottoconto

            If Rb_Dare5.Checked = True Then
                x.Righe(Indice).DareAvere = "A"
            Else
                x.Righe(Indice).DareAvere = "D"
            End If
            x.Righe(Indice).Descrizione = ""

            If Cau.Tipo = "R" Then
                x.Righe(Indice).RigaDaCausale = 2
            Else
                x.Righe(Indice).RigaDaCausale = 3
            End If

            Vettore = SplitWords(Txt_ClienteFornitore.Text)

            If Vettore.Length > 2 Then
                Mastro = Val(Vettore(0))
                Conto = Val(Vettore(1))
                Sottoconto = Val(Vettore(2))
            End If

            x.Righe(Indice).MastroContropartita = Mastro
            x.Righe(Indice).ContoContropartita = Conto
            x.Righe(Indice).SottocontoContropartita = Sottoconto

            Indice = Indice + 1
            'Indice = CreaRighe(Modulo.MathRound(Imponibile * Xs.Aliquota, 2), Imponibile, Mastro & " " & Conto & " " & Sottoconto, DareAvere, x, Indice, DD_IVA1.SelectedValue)
        End If

        'If CDbl(Txt_Ritenuta5.Text) > 0 Then
        '    Dim Xs As New ClsRitenuta

        '    Dim Cau As New Cls_CausaleContabile
        '    Dim DareAvere As String

        '    Cau.Leggi(Session("DC_TABELLE"), Dd_CausaleContabile.SelectedValue)

        '    Xs.Codice = Cau.Ritenuta
        '    Xs.Leggi(Session("DC_TABELLE"))

        '    Dim ImportoRitenuta As Double
        '    ImportoRitenuta = Txt_Ritenuta5.Text

        '    x.Righe(Indice) = New Cls_MovimentiContabiliRiga
        '    x.Righe(Indice).Imponibile = Modulo.MathRound(ImportoRitenuta * (100 / (Xs.Percentuale * 100)), 2)
        '    x.Righe(Indice).CodiceRitenuta = Cau.Ritenuta
        '    x.Righe(Indice).Importo = ImportoRitenuta

        '    x.Righe(Indice).Detraibile = ""
        '    x.Righe(Indice).Numero = 0
        '    x.Righe(Indice).Tipo = "RI"
        '    x.Righe(Indice).Invisibile = 0
        '    x.Righe(Indice).Segno = "+"



        '    Mastro = Cau.Righe(8).Mastro
        '    Conto = Cau.Righe(8).Conto
        '    Sottoconto = Cau.Righe(8).Sottoconto
        '    DareAvere = Cau.Righe(8).DareAvere


        '    x.Righe(Indice).MastroPartita = Mastro
        '    x.Righe(Indice).ContoPartita = Conto
        '    x.Righe(Indice).SottocontoPartita = Sottoconto
        '    If Cau.Righe(9).DareAvere = "D" Then
        '        x.Righe(Indice).DareAvere = "D"
        '    Else
        '        x.Righe(Indice).DareAvere = "A"
        '    End If

        '    x.Righe(Indice).Descrizione = ""


        '    x.Righe(Indice).RigaDaCausale = 9


        '    Vettore = SplitWords(Txt_ClienteFornitore.Text)

        '    If Vettore.Length > 2 Then
        '        Mastro = Val(Vettore(0))
        '        Conto = Val(Vettore(Indice))
        '        Sottoconto = Val(Vettore(2))
        '    End If

        '    x.Righe(Indice).MastroContropartita = Mastro
        '    x.Righe(Indice).ContoContropartita = Conto
        '    x.Righe(Indice).SottocontoContropartita = Sottoconto

        '    Indice = Indice + 1

        '    x.Righe(Indice) = New Cls_MovimentiContabiliRiga
        '    x.Righe(Indice).Imponibile = Modulo.MathRound(ImportoRitenuta * (100 / (Xs.Percentuale * 100)), 2)
        '    x.Righe(Indice).CodiceRitenuta = Cau.Ritenuta
        '    x.Righe(Indice).Importo = ImportoRitenuta

        '    x.Righe(Indice).Detraibile = ""
        '    x.Righe(Indice).Numero = 0
        '    x.Righe(Indice).Tipo = "RI"
        '    x.Righe(Indice).Invisibile = 0
        '    x.Righe(Indice).Segno = "+"


        '    Mastro = Cau.Righe(8).Mastro
        '    Conto = Cau.Righe(8).Conto
        '    Sottoconto = Cau.Righe(8).Sottoconto
        '    DareAvere = Cau.Righe(8).DareAvere


        '    x.Righe(Indice).MastroContropartita = Mastro
        '    x.Righe(Indice).ContoContropartita = Conto
        '    x.Righe(Indice).SottocontoContropartita = Sottoconto

        '    If Cau.Righe(9).DareAvere = "D" Then
        '        x.Righe(Indice).DareAvere = "A"
        '    Else
        '        x.Righe(Indice).DareAvere = "D"
        '    End If
        '    x.Righe(Indice).Descrizione = ""

        '    x.Righe(Indice).RigaDaCausale = 9

        '    Vettore = SplitWords(Txt_ClienteFornitore.Text)

        '    If Vettore.Length > 2 Then
        '        Mastro = Val(Vettore(0))
        '        Conto = Val(Vettore(1))
        '        Sottoconto = Val(Vettore(2))
        '    End If

        '    x.Righe(Indice).MastroPartita = Mastro
        '    x.Righe(Indice).ContoPartita = Conto
        '    x.Righe(Indice).SottocontoPartita = Sottoconto

        '    Indice = Indice + 1
        'End If


        If Txt_Conto1.Text <> "" And CDbl(Txt_Imponibile1.Text) > 0 Then
            Dim Cau As New Cls_CausaleContabile
            Dim DareAvere As String

            Cau.Leggi(Session("DC_TABELLE"), Dd_CausaleContabile.SelectedValue)

            x.Righe(Indice) = New Cls_MovimentiContabiliRiga

            If Cau.Tipo = "R" Then
                DareAvere = Cau.Righe(1).DareAvere
                x.Righe(Indice).RigaDaCausale = 2
            Else
                DareAvere = Cau.Righe(2).DareAvere
                x.Righe(Indice).RigaDaCausale = 3
            End If



            Vettore = SplitWords(Txt_Conto1.Text)

            Mastro = Val(Vettore(0))
            Conto = Val(Vettore(1))
            Sottoconto = Val(Vettore(2))

            x.Righe(Indice).MastroPartita = Mastro
            x.Righe(Indice).ContoPartita = Conto
            x.Righe(Indice).SottocontoPartita = Sottoconto

            x.Righe(Indice).Numero = 0
            x.Righe(Indice).Segno = "+"

            x.Righe(Indice).DareAvere = DareAvere
            x.Righe(Indice).Prorata = 0

            x.Righe(Indice).Quantita = 0


            Dim Xs As New Cls_IVA

            Xs.Leggi(Session("DC_TABELLE"), DD_IVA1.SelectedValue)


            x.Righe(Indice).Importo = CDbl(Txt_Imponibile1.Text) + CDbl(Txt_Imposta1.Text)
            x.Righe(Indice).CodiceIVA = DD_IVA1.SelectedValue


            If Not IsDBNull(Txt_ClienteFornitore.Text) Then
                Vettore = SplitWords(Txt_ClienteFornitore.Text)

                If Vettore.Length > 2 Then
                    Mastro = Val(Vettore(0))
                    Conto = Val(Vettore(1))
                    Sottoconto = Val(Vettore(2))
                End If

                x.Righe(Indice).MastroContropartita = Mastro
                x.Righe(Indice).ContoContropartita = Conto
                x.Righe(Indice).SottocontoContropartita = Sottoconto
            Else
                x.Righe(Indice).MastroContropartita = 0
                x.Righe(Indice).ContoContropartita = 0
                x.Righe(Indice).SottocontoContropartita = 0
            End If

            x.Righe(Indice).Descrizione = ""

            x.Righe(Indice).AnnoRiferimento = 0
            x.Righe(Indice).MeseRiferimento = 0
            If MyCau.Tipo = "R" Then
                x.Righe(Indice).RigaDaCausale = 3
            Else
                x.Righe(Indice).RigaDaCausale = 2
            End If
            x.Righe(Indice).RigaRegistrazione = Indice
            Indice = Indice + 1
        End If


        If Txt_Conto2.Text <> "" And CDbl(Txt_Imponibile2.Text) > 0 Then
            Dim Cau As New Cls_CausaleContabile
            Dim DareAvere As String

            Cau.Leggi(Session("DC_TABELLE"), Dd_CausaleContabile.SelectedValue)

            If Cau.Tipo = "R" Then
                DareAvere = Cau.Righe(1).DareAvere
            Else
                DareAvere = Cau.Righe(2).DareAvere
            End If


            x.Righe(Indice) = New Cls_MovimentiContabiliRiga
            Vettore = SplitWords(Txt_Conto2.Text)

            Mastro = Val(Vettore(0))
            Conto = Val(Vettore(1))
            Sottoconto = Val(Vettore(2))

            x.Righe(Indice).MastroPartita = Mastro
            x.Righe(Indice).ContoPartita = Conto
            x.Righe(Indice).SottocontoPartita = Sottoconto

            x.Righe(Indice).Numero = 0
            x.Righe(Indice).Segno = "+"

            x.Righe(Indice).DareAvere = DareAvere
            x.Righe(Indice).Prorata = 0

            x.Righe(Indice).Quantita = 0

            Dim Xs As New Cls_IVA

            Xs.Leggi(Session("DC_TABELLE"), DD_IVA2.SelectedValue)


            x.Righe(Indice).Importo = CDbl(Txt_Imponibile2.Text) + CDbl(Txt_Imposta2.Text)
            x.Righe(Indice).CodiceIVA = DD_IVA2.SelectedValue


            If Not IsDBNull(Txt_ClienteFornitore.Text) Then
                Vettore = SplitWords(Txt_ClienteFornitore.Text)

                If Vettore.Length > 2 Then
                    Mastro = Val(Vettore(0))
                    Conto = Val(Vettore(1))
                    Sottoconto = Val(Vettore(2))
                End If

                x.Righe(Indice).MastroContropartita = Mastro
                x.Righe(Indice).ContoContropartita = Conto
                x.Righe(Indice).SottocontoContropartita = Sottoconto
            Else
                x.Righe(Indice).MastroContropartita = 0
                x.Righe(Indice).ContoContropartita = 0
                x.Righe(Indice).SottocontoContropartita = 0
            End If

            x.Righe(Indice).Descrizione = ""

            x.Righe(Indice).AnnoRiferimento = 0
            x.Righe(Indice).MeseRiferimento = 0
            If MyCau.Tipo = "R" Then
                x.Righe(Indice).RigaDaCausale = 3
            Else
                x.Righe(Indice).RigaDaCausale = 2
            End If
            x.Righe(Indice).RigaRegistrazione = Indice
            Indice = Indice + 1
        End If


        If Txt_Conto3.Text <> "" And CDbl(Txt_Imponibile3.Text) > 0 Then
            Dim Cau As New Cls_CausaleContabile
            Dim DareAvere As String

            Cau.Leggi(Session("DC_TABELLE"), Dd_CausaleContabile.SelectedValue)

            If Cau.Tipo = "R" Then
                DareAvere = Cau.Righe(1).DareAvere
            Else
                DareAvere = Cau.Righe(2).DareAvere
            End If


            x.Righe(Indice) = New Cls_MovimentiContabiliRiga
            Vettore = SplitWords(Txt_Conto3.Text)

            Mastro = Val(Vettore(0))
            Conto = Val(Vettore(1))
            Sottoconto = Val(Vettore(2))

            x.Righe(Indice).MastroPartita = Mastro
            x.Righe(Indice).ContoPartita = Conto
            x.Righe(Indice).SottocontoPartita = Sottoconto

            x.Righe(Indice).Numero = 0
            x.Righe(Indice).Segno = "+"

            x.Righe(Indice).DareAvere = DareAvere
            x.Righe(Indice).Prorata = 0

            x.Righe(Indice).Quantita = 0

            Dim Xs As New Cls_IVA

            Xs.Leggi(Session("DC_TABELLE"), DD_IVA3.SelectedValue)


            x.Righe(Indice).Importo = CDbl(Txt_Imponibile3.Text) + CDbl(Txt_Imposta3.Text)
            x.Righe(Indice).CodiceIVA = DD_IVA3.SelectedValue


            If Not IsDBNull(Txt_ClienteFornitore.Text) Then
                Vettore = SplitWords(Txt_ClienteFornitore.Text)

                If Vettore.Length > 2 Then
                    Mastro = Val(Vettore(0))
                    Conto = Val(Vettore(1))
                    Sottoconto = Val(Vettore(2))
                End If

                x.Righe(Indice).MastroContropartita = Mastro
                x.Righe(Indice).ContoContropartita = Conto
                x.Righe(Indice).SottocontoContropartita = Sottoconto
            Else
                x.Righe(Indice).MastroContropartita = 0
                x.Righe(Indice).ContoContropartita = 0
                x.Righe(Indice).SottocontoContropartita = 0
            End If

            x.Righe(Indice).Descrizione = ""

            x.Righe(Indice).AnnoRiferimento = 0
            x.Righe(Indice).MeseRiferimento = 0
            If MyCau.Tipo = "R" Then
                x.Righe(Indice).RigaDaCausale = 3
            Else
                x.Righe(Indice).RigaDaCausale = 2
            End If
            x.Righe(Indice).RigaRegistrazione = Indice
            Indice = Indice + 1
        End If



        If Txt_Conto4.Text <> "" And CDbl(Txt_Imponibile4.Text) > 0 Then
            Dim Cau As New Cls_CausaleContabile
            Dim DareAvere As String

            Cau.Leggi(Session("DC_TABELLE"), Dd_CausaleContabile.SelectedValue)

            If Cau.Tipo = "R" Then
                DareAvere = Cau.Righe(1).DareAvere
            Else
                DareAvere = Cau.Righe(2).DareAvere
            End If


            x.Righe(Indice) = New Cls_MovimentiContabiliRiga
            Vettore = SplitWords(Txt_Conto4.Text)

            Mastro = Val(Vettore(0))
            Conto = Val(Vettore(1))
            Sottoconto = Val(Vettore(2))

            x.Righe(Indice).MastroPartita = Mastro
            x.Righe(Indice).ContoPartita = Conto
            x.Righe(Indice).SottocontoPartita = Sottoconto

            x.Righe(Indice).Numero = 0
            x.Righe(Indice).Segno = "+"

            x.Righe(Indice).DareAvere = DareAvere
            x.Righe(Indice).Prorata = 0

            x.Righe(Indice).Quantita = 0

            Dim Xs As New Cls_IVA

            Xs.Leggi(Session("DC_TABELLE"), DD_IVA4.SelectedValue)


            x.Righe(Indice).Importo = CDbl(Txt_Imponibile4.Text) + CDbl(Txt_Imposta4.Text)
            x.Righe(Indice).CodiceIVA = DD_IVA4.SelectedValue


            If Not IsDBNull(Txt_ClienteFornitore.Text) Then
                Vettore = SplitWords(Txt_ClienteFornitore.Text)

                If Vettore.Length > 2 Then
                    Mastro = Val(Vettore(0))
                    Conto = Val(Vettore(1))
                    Sottoconto = Val(Vettore(2))
                End If

                x.Righe(Indice).MastroContropartita = Mastro
                x.Righe(Indice).ContoContropartita = Conto
                x.Righe(Indice).SottocontoContropartita = Sottoconto
            Else
                x.Righe(Indice).MastroContropartita = 0
                x.Righe(Indice).ContoContropartita = 0
                x.Righe(Indice).SottocontoContropartita = 0
            End If

            x.Righe(Indice).Descrizione = ""

            x.Righe(Indice).AnnoRiferimento = 0
            x.Righe(Indice).MeseRiferimento = 0
            If MyCau.Tipo = "R" Then
                x.Righe(Indice).RigaDaCausale = 3
            Else
                x.Righe(Indice).RigaDaCausale = 2
            End If
            x.Righe(Indice).RigaRegistrazione = Indice
            Indice = Indice + 1
        End If


        If Txt_Conto5.Text <> "" And CDbl(Txt_Imponibile5.Text) > 0 Then
            Dim Cau As New Cls_CausaleContabile
            Dim DareAvere As String

            Cau.Leggi(Session("DC_TABELLE"), Dd_CausaleContabile.SelectedValue)

            If Cau.Tipo = "R" Then
                DareAvere = Cau.Righe(1).DareAvere
            Else
                DareAvere = Cau.Righe(2).DareAvere
            End If


            x.Righe(Indice) = New Cls_MovimentiContabiliRiga
            Vettore = SplitWords(Txt_Conto5.Text)

            Mastro = Val(Vettore(0))
            Conto = Val(Vettore(1))
            Sottoconto = Val(Vettore(2))

            x.Righe(Indice).MastroPartita = Mastro
            x.Righe(Indice).ContoPartita = Conto
            x.Righe(Indice).SottocontoPartita = Sottoconto

            x.Righe(Indice).Numero = 0
            x.Righe(Indice).Segno = "+"

            x.Righe(Indice).DareAvere = DareAvere
            x.Righe(Indice).Prorata = 0

            x.Righe(Indice).Quantita = 0

            Dim Xs As New Cls_IVA

            Xs.Leggi(Session("DC_TABELLE"), DD_IVA5.SelectedValue)


            x.Righe(Indice).Importo = CDbl(Txt_Imponibile5.Text) + CDbl(Txt_Imposta5.Text)
            x.Righe(Indice).CodiceIVA = DD_IVA5.SelectedValue


            If Not IsDBNull(Txt_ClienteFornitore.Text) Then
                Vettore = SplitWords(Txt_ClienteFornitore.Text)

                If Vettore.Length > 2 Then
                    Mastro = Val(Vettore(0))
                    Conto = Val(Vettore(1))
                    Sottoconto = Val(Vettore(2))
                End If

                x.Righe(Indice).MastroContropartita = Mastro
                x.Righe(Indice).ContoContropartita = Conto
                x.Righe(Indice).SottocontoContropartita = Sottoconto
            Else
                x.Righe(Indice).MastroContropartita = 0
                x.Righe(Indice).ContoContropartita = 0
                x.Righe(Indice).SottocontoContropartita = 0
            End If

            x.Righe(Indice).Descrizione = ""

            x.Righe(Indice).AnnoRiferimento = 0
            x.Righe(Indice).MeseRiferimento = 0
            If MyCau.Tipo = "R" Then
                x.Righe(Indice).RigaDaCausale = 3
            Else
                x.Righe(Indice).RigaDaCausale = 2
            End If
            x.Righe(Indice).RigaRegistrazione = Indice
            Indice = Indice + 1
        End If

        If x.Scrivi(Session("DC_GENERALE"), Val(Txt_Numero.Text)) = False Then
            ClientScript.RegisterClientScriptBlock(Me.GetType(), "Errore", "VisualizzaErrore('Errore in registrazione documento');", True)
            Exit Sub
        End If


        If Val(Txt_Numero.Text) = 0 Then
            Dim k As New Cls_CausaleContabile

            k.Codice = Dd_CausaleContabile.SelectedValue
            k.Leggi(Session("DC_TABELLE"), k.Codice)
            Dim Km As New Cls_MovimentoContabile

            Km.Leggi(Session("DC_GENERALE"), x.NumeroRegistrazione)
            If k.VenditaAcquisti = "V" Then
                If Km.Descrizione = "" Then

                    Vettore = SplitWords(Txt_ClienteFornitore.Text)

                    If Vettore.Length > 2 Then
                        Mastro = Val(Vettore(0))
                        Conto = Val(Vettore(1))
                        Sottoconto = Val(Vettore(2))
                    End If
                    Dim PianoConti As New Cls_Pianodeiconti

                    PianoConti.Mastro = Mastro
                    PianoConti.Conto = Conto
                    PianoConti.Sottoconto = Sottoconto
                    PianoConti.Decodfica(Session("DC_GENERALE"))


                    Km.Descrizione = "Nota prest. N. " & Km.NumeroProtocollo & " del " & Format(Km.DataRegistrazione, "dd/MM/yyyy") & " " & PianoConti.Descrizione
                End If
            Else
                If Km.Descrizione = "" Then
                    Vettore = SplitWords(Txt_ClienteFornitore.Text)

                    If Vettore.Length > 2 Then
                        Mastro = Val(Vettore(0))
                        Conto = Val(Vettore(1))
                        Sottoconto = Val(Vettore(2))
                    End If
                    Dim PianoConti As New Cls_Pianodeiconti

                    PianoConti.Mastro = Mastro
                    PianoConti.Conto = Conto
                    PianoConti.Sottoconto = Sottoconto
                    PianoConti.Decodfica(Session("DC_GENERALE"))


                    Km.Descrizione = "Fattura ric. N." & Km.NumeroDocumento & " del " & Format(Km.DataDocumento, "dd/MM/yyyy") & " " & PianoConti.Descrizione
                End If
            End If
            Km.Scrivi(Session("DC_GENERALE"), x.NumeroRegistrazione)

        End If



        Dim xModalita As New Cls_TipoPagamento

        xModalita.Codice = x.ModalitaPagamento
        xModalita.Leggi(Session("DC_TABELLE"))




        Dim ImportoNetto As Double = TotaleDocumento - TotaleRitenuta()
        Dim XS2 As New Cls_Scadenziario

        If Val(Txt_Numero.Text) > 0 Then
            If XS2.ControllaScadenza(Session("DC_GENERALE"), Val(Txt_Numero.Text), ImportoNetto) = False Then
                Dim SScript As String
                SScript = " $(document).ready(function(){   alert('Verifica Scadenzario'); });"
                ClientScript.RegisterClientScriptBlock(Me.GetType(), "OpenScadenziario", SScript, True)
                'Exit Sub
            End If
        Else
            Dim DataScadenza As New Date

            XS2.NumeroRegistrazioneContabile = x.NumeroRegistrazione

            DataScadenza = DateAdd(DateInterval.Day, xModalita.GiorniPrima, x.DataDocumento)
            If xModalita.GiorniPrima > 59 And Month(DataScadenza) And Day(DataScadenza) And xModalita.GiorniPrima < 5 And xModalita.Tipo = "F" Then
                DataScadenza = DateAdd(DateInterval.Day, xModalita.GiorniPrima, x.DataDocumento)
            End If
            If xModalita.Tipo = "F" Then
                If Month(x.DataDocumento) = 1 And xModalita.GiorniPrima >= 30 And xModalita.GiorniPrima <= 40 Then
                    DataScadenza = DateSerial(Year(DataScadenza), 2, GiorniMese(2, Year(DataScadenza)))
                Else
                    DataScadenza = DateSerial(Year(DataScadenza), Month(DataScadenza), GiorniMese(Month(DataScadenza), Year(DataScadenza)))
                End If
            End If
            XS2.DataScadenza = DataScadenza


            If xModalita.Scadenze > 0 Then
                XS2.Importo = Modulo.MathRound(ImportoNetto / xModalita.Scadenze, 2)
                XS2.Chiusa = 0
                XS2.Descrizione = ""
                XS2.ScriviScadenza(Session("DC_GENERALE"))
            End If

            If xModalita.Scadenze > 1 Then
                Dim XS1 As New Cls_Scadenziario
                XS1.NumeroRegistrazioneContabile = x.NumeroRegistrazione
                XS1.Importo = Modulo.MathRound(ImportoNetto / xModalita.Scadenze, 2)

                DataScadenza = DateAdd("d", xModalita.GiorniSeconda + xModalita.GiorniPrima, x.DataDocumento)
                If xModalita.Tipo = "F" Then DataScadenza = DateSerial(Year(DataScadenza), Month(DataScadenza), GiorniMese(Month(DataScadenza), Year(DataScadenza)))
                XS1.DataScadenza = DataScadenza

                XS1.Chiusa = 0
                XS1.Descrizione = ""
                XS1.ScriviScadenza(Session("DC_GENERALE"))
            End If

            If xModalita.Scadenze > 2 Then
                Dim GIORNIDA As Long
                GIORNIDA = xModalita.GiorniSeconda + xModalita.GiorniPrima
                For i = 3 To xModalita.Scadenze
                    GIORNIDA = GIORNIDA + xModalita.GiorniAltre
                    Dim XS1 As New Cls_Scadenziario
                    XS1.NumeroRegistrazioneContabile = x.NumeroRegistrazione
                    DataScadenza = DateAdd("d", GIORNIDA, x.DataDocumento)
                    If xModalita.Tipo = "F" Then DataScadenza = DateSerial(Year(DataScadenza), Month(DataScadenza), GiorniMese(Month(DataScadenza), Year(DataScadenza)))
                    XS1.DataScadenza = DataScadenza
                    XS1.Importo = Modulo.MathRound(ImportoNetto / xModalita.Scadenze, 2)
                    XS1.Chiusa = 0
                    XS1.Descrizione = ""
                    XS1.ScriviScadenza(Session("DC_GENERALE"))
                Next i
            End If

        End If




        x.Leggi(Session("DC_GENERALE"), x.NumeroRegistrazione)

        ScriptManager.RegisterStartupScript(Me, Me.GetType(), "Errore", "alert('Inserito documento registrazione n. " & x.NumeroRegistrazione & " Progessivo : " & x.ProgressivoNumero & " ');", True)
        Call EseguiJS()
    End Sub




    Public Function GiorniMese(ByVal Mese As Byte, ByVal Anno As Integer) As Byte
        If Mese = 1 Or Mese = 3 Or Mese = 5 Or Mese = 7 Or Mese = 8 Or _
           Mese = 10 Or Mese = 12 Then
            GiorniMese = 31
        Else
            If Mese <> 2 Then
                GiorniMese = 30
            Else
                If Day(DateSerial(Anno, Mese, 29)) = 29 Then
                    GiorniMese = 29
                Else
                    GiorniMese = 28
                End If
            End If
        End If
    End Function
    Private Function SplitWords(ByVal s As String) As String()
        '
        ' Call Regex.Split function from the imported namespace.
        ' Return the result array.
        '
        Return Regex.Split(s, "\W+")
    End Function


    Protected Sub Txt_ClienteFornitore_Disposed(ByVal sender As Object, ByVal e As System.EventArgs) Handles Txt_ClienteFornitore.Disposed
        Call Txt_ClienteFornitore_TextChanged(sender, e)

        Call EseguiJS()
    End Sub

    Protected Sub Txt_ClienteFornitore_TextChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles Txt_ClienteFornitore.TextChanged

        Dim k As New Cls_ClienteFornitore
        Dim Mastro As Long
        Dim Conto As Long
        Dim Sottoconto As Long
        Dim Vettore(100) As String
        Dim Appoggio1 As String = ""
        Dim Appoggio2 As String = ""

        Vettore = SplitWords(Txt_ClienteFornitore.Text)

        If Vettore.Length > 2 Then
            Mastro = Val(Vettore(0))
            Conto = Val(Vettore(1))
            Sottoconto = Val(Vettore(2))
            k.MastroCliente = Mastro
            k.ContoCliente = Conto
            k.SottoContoCliente = Sottoconto
            k.Leggi(Session("DC_OSPITE"))
            If k.Nome = "" Then
                k.MastroCliente = 0
                k.ContoCliente = 0
                k.SottoContoCliente = 0
                k.MastroFornitore = Mastro
                k.ContoFornitore = Conto
                k.SottoContoFornitore = Sottoconto
                k.Leggi(Session("DC_OSPITE"))

                Dim xS As New Cls_Pianodeiconti

                xS.Mastro = k.MastroCosto
                xS.Conto = k.ContoCosto
                xS.Sottoconto = k.SottocontoCosto
                If k.MastroCosto <> 0 Then
                    xS.Decodfica(Session("DC_GENERALE"))
                    Txt_Conto1.Text = xS.Mastro & " " & xS.Conto & " " & xS.Sottoconto & " " & xS.Descrizione
                    Txt_Conto2.Text = xS.Mastro & " " & xS.Conto & " " & xS.Sottoconto & " " & xS.Descrizione
                    Txt_Conto3.Text = xS.Mastro & " " & xS.Conto & " " & xS.Sottoconto & " " & xS.Descrizione
                    Txt_Conto4.Text = xS.Mastro & " " & xS.Conto & " " & xS.Sottoconto & " " & xS.Descrizione
                    Txt_Conto5.Text = xS.Mastro & " " & xS.Conto & " " & xS.Sottoconto & " " & xS.Descrizione
                End If
            Else
                Dim xS As New Cls_Pianodeiconti

                xS.Mastro = k.MastroCosto
                xS.Conto = k.ContoCosto
                xS.Sottoconto = k.SottocontoCosto
                If k.MastroCosto <> 0 Then
                    xS.Decodfica(Session("DC_GENERALE"))
                    Txt_Conto1.Text = xS.Mastro & " " & xS.Conto & " " & xS.Sottoconto & " " & xS.Descrizione
                    Txt_Conto2.Text = xS.Mastro & " " & xS.Conto & " " & xS.Sottoconto & " " & xS.Descrizione
                    Txt_Conto3.Text = xS.Mastro & " " & xS.Conto & " " & xS.Sottoconto & " " & xS.Descrizione
                    Txt_Conto4.Text = xS.Mastro & " " & xS.Conto & " " & xS.Sottoconto & " " & xS.Descrizione
                    Txt_Conto5.Text = xS.Mastro & " " & xS.Conto & " " & xS.Sottoconto & " " & xS.Descrizione
                End If
            End If
        End If

        Call EseguiJS()
    End Sub

    Protected Sub Button1_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles Button1.Click
        Call Txt_ClienteFornitore_TextChanged(sender, e)
    End Sub

    Private Sub EseguiJS()
        Dim MyJs As String
        MyJs = "$(document).ready(function() {"

        MyJs = MyJs & "var els = document.getElementsByTagName(""*"");"

        MyJs = MyJs & "for (var i=0;i<els.length;i++)"
        MyJs = MyJs & "if ( els[i].id ) { "
        MyJs = MyJs & " var appoggio =els[i].id; "
        MyJs = MyJs & " if (appoggio.match('Txt_Conto')!= null) {  "
        MyJs = MyJs & " $(els[i]).autocomplete('/WebHandler/GestioneConto.ashx?Utente=" & Session("UTENTE") & "', {delay:5,minChars:3});"
        MyJs = MyJs & "    }"

        MyJs = MyJs & " if (appoggio.match('Txt_ClienteFornitore')!= null) {  "
        MyJs = MyJs & " $(els[i]).autocomplete('/WebHandler/GestioneConto.ashx?Utente=" & Session("UTENTE") & "', {delay:5,minChars:3});"
        MyJs = MyJs & "    }"

        MyJs = MyJs & " if (appoggio.match('Txt_Data')!= null) {  "
        MyJs = MyJs & " $(els[i]).mask(""99/99/9999"");"
        MyJs = MyJs & "    }"

        MyJs = MyJs & " if (appoggio.match('Txt_Anno')!= null) {  "
        MyJs = MyJs & " $(els[i]).mask(""9999"");"
        MyJs = MyJs & "    }"


        MyJs = MyJs & " if ((appoggio.match('Txt_Imponibile')!= null) ||  (appoggio.match('Txt_Imposta')!= null) ||  (appoggio.match('Txt_Ritenuta')!= null)) {  "
        MyJs = MyJs & " $(els[i]).keypress(function() { ForceNumericInput($(this).val(), true, true); } ); "
        MyJs = MyJs & " $(els[i]).blur(function() { var ap = formatNumber($(this).val(),2); $(this).val(ap); });"
        MyJs = MyJs & "    }"



        MyJs = MyJs & "} "
        MyJs = MyJs & "});"

        'MyJs = "$(document).ready(function() { $('.myClass').keypress(function() { handleEnter($('.myClass').val(), true, true); } );     $('#" & Txt_ImportoPagato.ClientID & "').blur(function() { var ap = formatNumber ($('#" & Txt_ImportoPagato.ClientID & "').val(),2); $('#" & Txt_ImportoPagato.ClientID & "').val(ap); }); });"
        ScriptManager.RegisterStartupScript(Me, Me.GetType(), "visualizzaRitIm", MyJs, True)
    End Sub

    Private Sub Pulisci()
        Txt_Numero.Text = 0
        Txt_Numero.Enabled = True

        Dd_CausaleContabile.SelectedValue = ""
        Txt_DataRegistrazione.Text = Format(Now, "dd/MM/yyyy")
        Txt_DataDocumento.Text = ""
        Txt_NumeroDocumento.Text = ""
        Txt_ClienteFornitore.Text = ""
        Txt_AnnoProtocollo.Text = Year(Now)
        Txt_NumeroProtocollo.Text = ""
        Txt_Descrizione.Text = ""
        Txt_Conto1.Text = ""
        Txt_Conto2.Text = ""
        Txt_Conto3.Text = ""
        Txt_Conto4.Text = ""
        Txt_Conto5.Text = ""

        DD_ModalitaPagamento.SelectedValue = ""

        Txt_Imponibile1.Text = 0
        Txt_Imponibile2.Text = 0
        Txt_Imponibile3.Text = 0
        Txt_Imponibile4.Text = 0
        Txt_Imponibile5.Text = 0

        DD_IVA1.SelectedValue = ""
        DD_IVA2.SelectedValue = ""
        DD_IVA3.SelectedValue = ""
        DD_IVA4.SelectedValue = ""
        DD_IVA5.SelectedValue = ""


        Txt_Imposta1.Text = 0
        Txt_Imposta2.Text = 0
        Txt_Imposta3.Text = 0
        Txt_Imposta4.Text = 0
        Txt_Imposta5.Text = 0


        Txt_Ritenuta1.Text = 0
        'Txt_Ritenuta2.Text = 0
        'Txt_Ritenuta3.Text = 0
        'Txt_Ritenuta4.Text = 0
        'Txt_Ritenuta5.Text = 0

        Dim kP As New Cls_MovimentoContabile


        Lbl_Progressivo.Text = kP.MaxProgressivoAnno(Session("DC_GENERALE"), Year(Txt_DataRegistrazione.Text)) + 1 & "/" & Year(Txt_DataRegistrazione.Text)


        RicalcolaImportiDocumento()
    End Sub

    Protected Sub Btn_Pulisci_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles Btn_Pulisci.Click


        Call Pulisci()
        Call EseguiJS()
    End Sub

    Protected Sub Btn_Elimina_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles Btn_Elimina.Click
        Dim xs As New Cls_Legami
        'Lbl_errori.Text = ""
        If xs.TotaleLegame(Session("DC_GENERALE"), Val(Txt_Numero.Text)) Then
            ClientScript.RegisterClientScriptBlock(Me.GetType(), "Errore", "VisualizzaErrore('Vi sono legami non posso cancellare la registrazione');", True)
            Call EseguiJS()
            Exit Sub
        End If
        If Val(Txt_Numero.Text) > 0 Then
            Dim VerificaBollato As New Cls_MovimentoContabile
            VerificaBollato.NumeroRegistrazione = Val(Txt_Numero.Text)
            If VerificaBollato.Bollato(Session("DC_GENERALE")) = True Then
                ClientScript.RegisterClientScriptBlock(Me.GetType(), "Errore", "VisualizzaErrore('Già creato il giornale bollato');", True)
                Call EseguiJS()
                Exit Sub
            End If
        End If
        If Val(Txt_Numero.Text) = 0 Then
            ClientScript.RegisterClientScriptBlock(Me.GetType(), "Errore", "VisualizzaErrore('Specificare registrazione');", True)
            Call EseguiJS()
            Exit Sub
        End If
        Dim XDatiGenerali As New Cls_DatiGenerali
        Dim AppoggioData As Date

        AppoggioData = Txt_DataRegistrazione.Text
        XDatiGenerali.LeggiDati(Session("DC_TABELLE"))

        If Format(XDatiGenerali.DataChiusura, "yyyyMMdd") >= Format(AppoggioData, "yyyyMMdd") Then
            ClientScript.RegisterClientScriptBlock(Me.GetType(), "Errore", "VisualizzaErrore('Non posso eliminare la registrazione,<BR/> già effettuato la chiusura');", True)
            Call EseguiJS()
            Exit Sub
        End If

        Dim XScd As New Cls_Scadenziario
        Dim TbEScd As New System.Data.DataTable("TbEScd")
        XScd.NumeroRegistrazioneContabile = Val(Txt_Numero.Text)
        XScd.loaddati(Session("DC_GENERALE"), TbEScd)

        If TbEScd.Rows.Count > 1 Then
            Exit Sub
        End If

        If TbEScd.Rows.Count = 1 Then
            If Not IsDBNull(TbEScd.Rows(0).Item(1)) Then
                If Val(TbEScd.Rows(0).Item(1)) > 0 Then
                    ClientScript.RegisterClientScriptBlock(Me.GetType(), "Errore", "VisualizzaErrore('Vi sono scadenze legate alla registrazione non posso cancellare');", True)
                    Call EseguiJS()
                    Exit Sub
                End If
            End If
        End If

        Dim XReg As New Cls_MovimentoContabile

        XReg.NumeroRegistrazione = Txt_Numero.Text
        XReg.EliminaRegistrazione(Session("DC_GENERALE"))

        Dd_CausaleContabile.SelectedValue = ""
        Txt_DataRegistrazione.Text = Format(Now, "dd/MM/yyyy")
        Txt_DataDocumento.Text = ""
        Txt_NumeroDocumento.Text = ""
        Txt_ClienteFornitore.Text = ""
        Txt_AnnoProtocollo.Text = Year(Now)
        Txt_NumeroProtocollo.Text = ""
        Txt_Descrizione.Text = ""
        Txt_Conto1.Text = ""
        Txt_Conto2.Text = ""
        Txt_Conto3.Text = ""
        Txt_Conto4.Text = ""
        Txt_Conto5.Text = ""

        DD_ModalitaPagamento.SelectedValue = ""

        Txt_Numero.Text = "0"

        Txt_Imponibile1.Text = 0
        Txt_Imponibile2.Text = 0
        Txt_Imponibile3.Text = 0
        Txt_Imponibile4.Text = 0
        Txt_Imponibile5.Text = 0

        DD_IVA1.SelectedValue = ""
        DD_IVA2.SelectedValue = ""
        DD_IVA3.SelectedValue = ""
        DD_IVA4.SelectedValue = ""
        DD_IVA5.SelectedValue = ""
        Call Pulisci()

        Call EseguiJS()
    End Sub

    Protected Sub Txt_Numero_TextChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles Txt_Numero.TextChanged
        If Val(Txt_Numero.Text) > 0 Then
            Dim x As New Cls_MovimentoContabile
            x.Leggi(Session("DC_GENERALE"), Val(Txt_Numero.Text))
            Dim Ks As New Cls_CausaleContabile
            Ks.Leggi(Session("DC_TABELLE"), x.CausaleContabile)
            If Ks.RegistroIVA = 0 Then
                Exit Sub
            End If
            If x.NumeroRegistrazione > 0 Then
                Txt_Numero.Enabled = False
                Call leggiregistrazione()

                Call EseguiJS()
            End If
        End If
    End Sub


    Private Sub leggiregistrazione()
        Dim x As New Cls_MovimentoContabile
        Dim i As Integer
        Dim ConnectionStringGenerale As String = Session("DC_GENERALE")
        x.Leggi(ConnectionStringGenerale, Txt_Numero.Text)

        Txt_Numero.Text = x.NumeroRegistrazione
        Txt_DataRegistrazione.Text = x.DataRegistrazione
        Dd_CausaleContabile.SelectedValue = x.CausaleContabile

        Txt_Numero.Enabled = False
        Txt_DataDocumento.Text = x.DataDocumento
        Txt_NumeroDocumento.Text = x.NumeroDocumento

        Txt_AnnoProtocollo.Text = x.AnnoProtocollo
        Txt_NumeroProtocollo.Text = x.NumeroProtocollo

        Txt_Descrizione.Text = x.Descrizione

        Lbl_Progressivo.Text = x.ProgressivoNumero & "/" & x.ProgressivoAnno


        DD_ModalitaPagamento.SelectedValue = x.ModalitaPagamento

        Dim Riga As Integer = 0
        Dim RigaCR As Integer = 0
        Dim RigaDaCausale As Integer
        Dim Cau As New Cls_CausaleContabile
        Dim DareAvere As String

        Cau.Leggi(Session("DC_TABELLE"), Dd_CausaleContabile.SelectedValue)

        If Cau.Tipo = "R" Then
            DareAvere = Cau.Righe(1).DareAvere
            RigaDaCausale = 3
        Else
            DareAvere = Cau.Righe(2).DareAvere
            RigaDaCausale = 2
        End If


        Dim RigaIni As Integer = 0


        For i = 0 To x.Righe.Length - 1
            If Not IsNothing(x.Righe(i)) Then

                If x.Righe(i).RigaDaCausale = 9 Then
                    RigaIni = RigaIni + 1
                    If Riga = 1 Then
                        Txt_Ritenuta1.Text = Format(x.Righe(i).Importo, "#,##0.00")
                    End If
                    'If Riga = 2 Then
                    '    Txt_Ritenuta2.Text = Format(x.Righe(i).Importo, "#,##0.00")
                    'End If
                    'If Riga = 3 Then
                    '    Txt_Ritenuta3.Text = Format(x.Righe(i).Importo, "#,##0.00")
                    'End If
                    'If Riga = 4 Then
                    '    Txt_Ritenuta4.Text = Format(x.Righe(i).Importo, "#,##0.00")
                    'End If
                    'If Riga = 5 Then
                    '    Txt_Ritenuta5.Text = Format(x.Righe(i).Importo, "#,##0.00")
                    'End If

                End If
                If x.Righe(i).RigaDaCausale = 1 Then
                    Dim xc As New Cls_Pianodeiconti

                    xc.Mastro = x.Righe(i).MastroPartita
                    xc.Conto = x.Righe(i).ContoPartita
                    xc.Sottoconto = x.Righe(i).SottocontoPartita

                    xc.Decodfica(ConnectionStringGenerale)
                    Txt_ClienteFornitore.Text = xc.Mastro & " " & xc.Conto & " " & xc.Sottoconto & " " & xc.Descrizione
                End If
                If x.Righe(i).Tipo = "IV" Then
                    Riga = Riga + 1
                    If Riga = 1 Then
                        Txt_Imponibile1.Text = Format(x.Righe(i).Imponibile, "#,##0.00")
                        DD_IVA1.SelectedValue = x.Righe(i).CodiceIVA
                        Txt_Imposta1.Text = Format(x.Righe(i).Importo, "#,##0.00")
                        If x.Righe(i).DareAvere = "D" Then
                            Rb_Dare1.Checked = True
                            Rb_Avere1.Checked = False
                        Else
                            Rb_Dare1.Checked = False
                            Rb_Avere1.Checked = True
                        End If
                    End If
                    If Riga = 2 Then
                        Txt_Imponibile2.Text = Format(x.Righe(i).Imponibile, "#,##0.00")
                        DD_IVA2.SelectedValue = x.Righe(i).CodiceIVA
                        Txt_Imposta2.Text = Format(x.Righe(i).Importo, "#,##0.00")
                        If x.Righe(i).DareAvere = "D" Then
                            Rb_Dare2.Checked = True
                            Rb_Avere2.Checked = False
                        Else
                            Rb_Dare2.Checked = False
                            Rb_Avere2.Checked = True
                        End If
                    End If
                    If Riga = 3 Then
                        Txt_Imponibile3.Text = Format(x.Righe(i).Imponibile, "#,##0.00")
                        DD_IVA3.SelectedValue = x.Righe(i).CodiceIVA
                        Txt_Imposta3.Text = Format(x.Righe(i).Importo, "#,##0.00")
                        If x.Righe(i).DareAvere = "D" Then
                            Rb_Dare3.Checked = True
                            Rb_Avere3.Checked = False
                        Else
                            Rb_Dare3.Checked = False
                            Rb_Avere3.Checked = True
                        End If
                    End If
                    If Riga = 4 Then
                        Txt_Imponibile4.Text = Format(x.Righe(i).Imponibile, "#,##0.00")
                        DD_IVA4.SelectedValue = x.Righe(i).CodiceIVA
                        Txt_Imposta4.Text = Format(x.Righe(i).Importo, "#,##0.00")
                        If x.Righe(i).DareAvere = "D" Then
                            Rb_Dare4.Checked = True
                            Rb_Avere4.Checked = False
                        Else
                            Rb_Dare4.Checked = False
                            Rb_Avere4.Checked = True
                        End If
                    End If
                    If Riga = 5 Then
                        Txt_Imponibile5.Text = Format(x.Righe(i).Imponibile, "#,##0.00")
                        DD_IVA5.SelectedValue = x.Righe(i).CodiceIVA
                        Txt_Imposta5.Text = Format(x.Righe(i).Importo, "#,##0.00")
                        If x.Righe(i).DareAvere = "D" Then
                            Rb_Dare5.Checked = True
                            Rb_Avere5.Checked = False
                        Else
                            Rb_Dare5.Checked = False
                            Rb_Avere5.Checked = True
                        End If
                    End If
                End If
                If x.Righe(i).RigaDaCausale = RigaDaCausale Then
                    RigaCR = RigaCR + 1
                    Dim k As New Cls_Pianodeiconti

                    k.Mastro = x.Righe(i).MastroPartita
                    k.Conto = x.Righe(i).ContoPartita
                    k.Sottoconto = x.Righe(i).SottocontoPartita
                    k.Decodfica(Session("DC_GENERALE"))

                    If RigaCR = 1 Then
                        Txt_Conto1.Text = x.Righe(i).MastroPartita & " " & x.Righe(i).ContoPartita & " " & x.Righe(i).SottocontoPartita & " " & k.Descrizione
                    End If
                    If RigaCR = 2 Then
                        Txt_Conto2.Text = x.Righe(i).MastroPartita & " " & x.Righe(i).ContoPartita & " " & x.Righe(i).SottocontoPartita & " " & k.Descrizione
                    End If
                    If RigaCR = 3 Then
                        Txt_Conto3.Text = x.Righe(i).MastroPartita & " " & x.Righe(i).ContoPartita & " " & x.Righe(i).SottocontoPartita & " " & k.Descrizione
                    End If
                    If RigaCR = 4 Then
                        Txt_Conto4.Text = x.Righe(i).MastroPartita & " " & x.Righe(i).ContoPartita & " " & x.Righe(i).SottocontoPartita & " " & k.Descrizione
                    End If
                    If RigaCR = 5 Then
                        Txt_Conto5.Text = x.Righe(i).MastroPartita & " " & x.Righe(i).ContoPartita & " " & x.Righe(i).SottocontoPartita & " " & k.Descrizione
                    End If
                End If
            End If
        Next

        Dim Xl As New Cls_CausaleContabile

        Xl.Leggi(Session("DC_TABELLE"), Dd_CausaleContabile.SelectedValue)

        Dim DecReg As New Cls_RegistroIVA

        DecReg.Leggi(Session("DC_TABELLE"), Xl.RegistroIVA)

        Lbl_RegistroIVA.Text = "(" & DecReg.Descrizione & ")"

        RicalcolaImportiDocumento()


        Lbl_BtnLegami.Text = "<a href=""#"" class="""" onclick=""DialogBox('Legami.aspx?Numero=" & Txt_Numero.Text & "');""><img src=""images/Legami.jpg"" title=""Legami"" /></a>"

        Lbl_BtnScadenzario.Text = "<a href=""#"" class="""" onclick=""DialogBoxSlim('gestionescadenzario.aspx?Numero=" & Txt_Numero.Text & "');""><img src=""images/scadenziario.png""  title=""Scadenzario""  /></a>"



    End Sub

    Protected Sub Btn_Esci_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles Btn_Esci.Click
        If Request.Item("PAGINA") = "" Then
            Response.Redirect("UltimiMovimenti.aspx?TIPO=DOC")
        Else
            Response.Redirect("UltimiMovimenti.aspx?CHIAMATA=RITORNO&PAGINA=" & Request.Item("PAGINA") & "&TIPO=DOC")
        End If
        REM Response.Redirect("UltimiMovimenti.aspx?TIPO=DOC")
    End Sub

    Protected Sub ImgRicerca_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles ImgRicerca.Click
        Response.Redirect("Ricerca.aspx?TIPO=DOC")
    End Sub

    Protected Sub Timer1_Tick(ByVal sender As Object, ByVal e As System.EventArgs) Handles Timer1.Tick
        If Trim(Session("UTENTE")) = "" Then
            If Request.ServerVariables("URL").ToUpper.IndexOf("SENIORWEBBLANCO") > 0 Then
                Response.Redirect("/ODV/Login.aspx")
                Exit Sub
            Else
                Response.Redirect("/SeniorWeb/Login.aspx")
                Exit Sub
            End If
        End If
    End Sub

    Protected Sub Txt_DataRegistrazione_TextChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles Txt_DataRegistrazione.TextChanged
        Txt_AnnoProtocollo.Text = Year(Txt_DataRegistrazione.Text)

        Dim k As New Cls_CausaleContabile

        k.Codice = Dd_CausaleContabile.SelectedValue

        k.Leggi(Session("DC_TABELLE"), k.Codice)

        If k.VenditaAcquisti = "V" Then

            Dim Mc As New Cls_MovimentoContabile
            Txt_DataDocumento.Text = Txt_DataRegistrazione.Text

            Txt_NumeroDocumento.Text = Mc.MaxProtocollo(Session("DC_GENERALE"), Txt_AnnoProtocollo.Text, k.RegistroIVA) + 1

        End If


        Call EseguiJS()
    End Sub

    Private Sub MettiInvisibileJS()
        Dim MyJs As String


        MyJs = "var els = document.getElementsByTagName(""*"");"

        MyJs = MyJs & "for (var i=0;i<els.length;i++)"
        MyJs = MyJs & "if ( els[i].id ) { "
        MyJs = MyJs & " var appoggio =els[i].id; "

        MyJs = MyJs & " if ((appoggio.match('BOTTONEHOME')!= null) || (appoggio.match('BOTTONERICERCA')!= null) || (appoggio.match('BOTTONEADDSOCIO')!= null)) {  "
        MyJs = MyJs & " $(els[i]).hide();"
        MyJs = MyJs & "    }"


        MyJs = MyJs & "} "


        'MyJs = "$(document).ready(function() { $('.myClass').keypress(function() { handleEnter($('.myClass').val(), true, true); } );     $('#" & Txt_ImportoPagato.ClientID & "').blur(function() { var ap = formatNumber ($('#" & Txt_ImportoPagato.ClientID & "').val(),2); $('#" & Txt_ImportoPagato.ClientID & "').val(ap); }); });"
        ScriptManager.RegisterStartupScript(Me, Me.GetType(), "MettiInsibileJS", MyJs, True)
        'ClientScript.RegisterClientScriptBlock(Me.GetType(), "MettiInsibileJSCS", MyJs, True)
    End Sub

    Protected Sub DD_IVA1_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles DD_IVA1.SelectedIndexChanged

        If Txt_Imponibile1.Text = "" Then Exit Sub

        Dim x As New Cls_IVA


        x.Leggi(Session("DC_TABELLE"), DD_IVA1.SelectedValue)

        Txt_Imposta1.Text = Format(Modulo.MathRound(Txt_Imponibile1.Text * x.Aliquota, 2), "#,##0.00")

        Call RicalcolaImportiDocumento()
        Call EseguiJS()
    End Sub

    Protected Sub DD_IVA2_TextChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles DD_IVA2.TextChanged
        If Txt_Imponibile2.Text = "" Then Exit Sub

        Dim x As New Cls_IVA
        x.Leggi(Session("DC_TABELLE"), DD_IVA2.SelectedValue)

        Txt_Imposta2.Text = Format(Modulo.MathRound(Txt_Imponibile2.Text * x.Aliquota, 2), "#,##0.00")

        Call RicalcolaImportiDocumento()
        Call EseguiJS()
    End Sub

    Protected Sub DD_IVA3_TextChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles DD_IVA3.TextChanged

        If Txt_Imponibile3.Text = "" Then Exit Sub

        Dim x As New Cls_IVA


        x.Leggi(Session("DC_TABELLE"), DD_IVA3.SelectedValue)

        Txt_Imposta3.Text = Format(Modulo.MathRound(Txt_Imponibile3.Text * x.Aliquota, 2), "#,##0.00")

        Call RicalcolaImportiDocumento()
        Call EseguiJS()

    End Sub

    Protected Sub DD_IVA4_TextChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles DD_IVA4.TextChanged
        If Txt_Imponibile4.Text = "" Then Exit Sub

        Dim x As New Cls_IVA


        x.Leggi(Session("DC_TABELLE"), DD_IVA4.SelectedValue)

        Txt_Imposta4.Text = Format(Modulo.MathRound(Txt_Imponibile4.Text * x.Aliquota, 2), "#,##0.00")

        Call RicalcolaImportiDocumento()
        Call EseguiJS()
    End Sub

    Protected Sub DD_IVA5_TextChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles DD_IVA5.TextChanged
        If Txt_Imponibile5.Text = "" Then Exit Sub

        Dim x As New Cls_IVA


        x.Leggi(Session("DC_TABELLE"), DD_IVA5.SelectedValue)

        Txt_Imposta5.Text = Format(Modulo.MathRound(Txt_Imponibile5.Text * x.Aliquota, 2), "#,##0.00")

        Call RicalcolaImportiDocumento()
        Call EseguiJS()
    End Sub

    Protected Sub Btn_Duplica_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles Btn_Duplica.Click
        Txt_Numero.Text = "0"
        Txt_NumeroProtocollo.Text = "0"
    End Sub

    
    Protected Sub Txt_Imponibile1_TextChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles Txt_Imponibile1.TextChanged
        Call RicalcolaImportiDocumento()
    End Sub

    Protected Sub Txt_Imponibile2_TextChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles Txt_Imponibile2.TextChanged
        Call RicalcolaImportiDocumento()
    End Sub

    Protected Sub Txt_Imponibile3_TextChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles Txt_Imponibile3.TextChanged
        Call RicalcolaImportiDocumento()
    End Sub

    Protected Sub Txt_Imponibile4_TextChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles Txt_Imponibile4.TextChanged
        Call RicalcolaImportiDocumento()
    End Sub

    Protected Sub Txt_Imponibile5_TextChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles Txt_Imponibile5.TextChanged
        Call RicalcolaImportiDocumento()
    End Sub

    Protected Sub Txt_Imposta1_TextChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles Txt_Imposta1.TextChanged
        Call RicalcolaImportiDocumento()
    End Sub

    Protected Sub Txt_Imposta2_TextChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles Txt_Imposta2.TextChanged
        Call RicalcolaImportiDocumento()
    End Sub

    Protected Sub Txt_Imposta3_TextChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles Txt_Imposta3.TextChanged
        Call RicalcolaImportiDocumento()
    End Sub

    Protected Sub Txt_Imposta4_TextChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles Txt_Imposta4.TextChanged
        Call RicalcolaImportiDocumento()
    End Sub

    Protected Sub Txt_Imposta5_TextChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles Txt_Imposta5.TextChanged
        Call RicalcolaImportiDocumento()
    End Sub

    Protected Sub Txt_Ritenuta1_TextChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles Txt_Ritenuta1.TextChanged
        Call RicalcolaImportiDocumento()
    End Sub

    'Protected Sub Txt_Ritenuta2_TextChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles Txt_Ritenuta2.TextChanged
    '    Call RicalcolaImportiDocumento()
    'End Sub

    'Protected Sub Txt_Ritenuta3_TextChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles Txt_Ritenuta3.TextChanged
    '    Call RicalcolaImportiDocumento()
    'End Sub

    'Protected Sub Txt_Ritenuta4_TextChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles Txt_Ritenuta4.TextChanged
    '    Call RicalcolaImportiDocumento()
    'End Sub

    'Protected Sub Txt_Ritenuta5_TextChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles Txt_Ritenuta5.TextChanged
    '    Call RicalcolaImportiDocumento()
    'End Sub

    Protected Sub ImageButton1_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles ImageButton1.Click

    End Sub

    Protected Sub Btn_AddDoc_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles Btn_AddDoc.Click
        Session("NumeroRegistrazione") = 0

        Response.Redirect("DocumentiEasy.aspx")
    End Sub
End Class
