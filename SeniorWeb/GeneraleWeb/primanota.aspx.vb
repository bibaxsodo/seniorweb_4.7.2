﻿Imports System.Data.OleDb
Imports System.Web.Script.Serialization

Partial Class GeneraleWeb_primanota
    Inherits System.Web.UI.Page

    Dim Tabella As New System.Data.DataTable("tabella")

    Private Sub PulisciRegistrazione()
        Txt_Numero.Text = 0

        BtnOpen.Visible = False
        BtnOpen2.Visible = False


        If IsDate(Txt_DataRegistrazione.Text) And Txt_DataRegistrazione.Text <> "" Then
        Else
            Txt_DataRegistrazione.Text = Format(Now, "dd/MM/yyyy")
        End If

        Dd_CausaleContabile.SelectedValue = ""
        Txt_Descrizione.Text = ""
        Txt_Numero.Enabled = True
        DD_CSERV.SelectedValue = ""

        Txt_AnnoRif.Text = 0
        Txt_ExportData.Text = ""
        Txt_RegistrazioneRiferimento.Text = 0

        DD_Cig.SelectedValue = ""
        DD_MeseRif.SelectedValue = 0

        Txt_DataReversale.Text = ""
        Txt_DataMandato.Text = ""
        Txt_NumeroMandato.Text = ""
        Txt_NumeroReversale.Text = ""



        Dim DatiGenerali As New Cls_DatiGenerali

        DatiGenerali.LeggiDati(Session("DC_TABELLE"))


        If DatiGenerali.BudgetAnalitica = 0 Then
            Lbl_BtnLegami.Text = "<label class=""MenuDestra"">&nbsp;&nbsp;Legame Prenotazioni</label>"
            'Lbl_BtnLegami.Text = Lbl_BtnLegami.Text & "<label class=""MenuDestra"">&nbsp;&nbsp;Legame Prenotazioni</label>"
        Else
            Lbl_BtnLegami.Text = Lbl_BtnLegami.Text & "<label class=""MenuDestra"">&nbsp;&nbsp;Legame Analitica</label>"
        End If

        Lbl_BtnLegami.Text = Lbl_BtnLegami.Text & "<label class=""MenuDestra"">&nbsp;&nbsp;Ratei Risconti</label>"
        Lbl_BtnLegami.Text = Lbl_BtnLegami.Text & "<label class=""MenuDestra"">&nbsp;&nbsp;Dati Mandati/Reversali</label>"
        Lbl_BtnScadenzario.Text = "<label class=""MenuDestra"">&nbsp;&nbsp;Scadenzario</label>"


        Tabella.Clear()
        Tabella.Columns.Clear()
        Tabella.Columns.Add("Sottoconto", GetType(String))
        Tabella.Columns.Add("Dare", GetType(String))
        Tabella.Columns.Add("Avere", GetType(String))
        Tabella.Columns.Add("Descrizione", GetType(String))
        Tabella.Columns.Add("SottocontoControPartita", GetType(String))
        Tabella.Columns.Add("RigaRegistrazione", GetType(String))
        Dim myriga As System.Data.DataRow = Tabella.NewRow()

        Tabella.Rows.Add(myriga)

        Grid.AutoGenerateColumns = False
        Grid.DataSource = Tabella
        Grid.DataBind()

        If Session("TIPOAPP") <> "RSA" Then
            Grid.Columns(4).Visible = False
            Grid.Columns(5).Visible = False
        End If

        Lbl_Errore.Text = ""
        Lbl_TipoRegistrazione.Text = ""


        Dim k As New Cls_MovimentoContabile



        ViewState("RighePrimaNota") = Tabella

        If Request.Item("PROVENIENTE") = "SEMPLIFICATA" Then
            ImgRicerca.Visible = False
            Lbl_BtnLegami.Text = ""
            Lbl_BtnScadenzario.Text = ""
            Call MettiInvisibileSemplificataJS()
        End If
    End Sub

    Private Sub leggiregistrazione()
        Dim x As New Cls_MovimentoContabile
        Dim ConnectionStringGenerale As String = Session("DC_GENERALE")
        x.Leggi(ConnectionStringGenerale, Txt_Numero.Text)


        Dim CausaleContabile As New Cls_CausaleContabile

        CausaleContabile.Codice = x.CausaleContabile
        CausaleContabile.Leggi(Session("DC_TABELLE"), CausaleContabile.Codice)


        BtnOpen.Visible = False
        BtnOpen2.Visible = False
        If CausaleContabile.Tipo = "P" Then
            BtnOpen.ImageUrl = "../images/Menu_GestioneIncassiPagamenti.png"
            BtnOpen.Visible = True
            BtnOpen.ToolTip = "Incassi e Pagamenti"

            BtnOpen2.ImageUrl = "../images/Menu_IncassoPagamentoScadenziario.png"
            BtnOpen2.Visible = True
            BtnOpen2.ToolTip = "Incassi e Pagamenti Con Scadenziario"
        End If
        If CausaleContabile.Tipo = "R" Or CausaleContabile.Tipo = "I" Then
            BtnOpen.ImageUrl = "../images/Bottone_Documenti.png"
            BtnOpen.ToolTip = "Gestione Documenti"
            BtnOpen.Visible = True
        End If

        ViewState("NonModificabile") = 0

        Txt_Numero.Text = x.NumeroRegistrazione
        Txt_DataRegistrazione.Text = x.DataRegistrazione
        Dd_CausaleContabile.SelectedValue = x.CausaleContabile
        Txt_Descrizione.Text = x.Descrizione
        DD_CSERV.SelectedValue = x.CentroServizio


        Txt_AnnoRif.Text = x.AnnoCompetenza

        DD_MeseRif.Text = x.MeseCompetenza


        DD_Cig.SelectedValue = x.CodiceCig
        Txt_RegistrazioneRiferimento.Text = x.RegistrazioneRiferimento


        If Year(x.ExportDATA) > 2000 Then
            Txt_ExportData.Text = Format(x.ExportDATA, "dd/MM/yyyy HH:mm:ss")
        Else
            Txt_ExportData.Text = ""
        End If


        If Year(x.DataDistinta) > 2000 Then
            Txt_DataDistinta.Text = Format(x.DataDistinta, "dd/MM/yyyy HH:mm:ss")
        Else
            Txt_DataDistinta.Text = ""
        End If

        If Year(x.DataMandato) > 2000 Then
            Txt_DataMandato.Text = Format(x.DataMandato, "dd/MM/yyyy HH:mm:ss")
        Else
            Txt_DataMandato.Text = ""
        End If

        If Year(x.DataReversale) > 2000 Then
            Txt_DataReversale.Text = Format(x.DataReversale, "dd/MM/yyyy HH:mm:ss")
        Else
            Txt_DataReversale.Text = ""
        End If


        Txt_NumeroDistinta.Text = x.NumeroDistinta
        Txt_NumeroMandato.Text = x.NumeroMandato
        Txt_NumeroReversale.Text = x.NumeroReversale


        Txt_Numero.Enabled = False

        Tabella.Clear()
        Tabella.Columns.Clear()
        Tabella.Columns.Add("Sottoconto", GetType(String))
        Tabella.Columns.Add("Dare", GetType(String))
        Tabella.Columns.Add("Avere", GetType(String))
        Tabella.Columns.Add("Descrizione", GetType(String))
        Tabella.Columns.Add("SottocontoControPartita", GetType(String))
        Tabella.Columns.Add("RigaRegistrazione", GetType(String))

        Dim i As Integer
        For i = 0 To x.Righe.Length
            If Not IsNothing(x.Righe(i)) Then
                If Not x.Righe(i).Invisibile = 1 Then
                    Dim myriga As System.Data.DataRow = Tabella.NewRow()
                    Dim xc As New Cls_Pianodeiconti

                    xc.Mastro = x.Righe(i).MastroPartita
                    xc.Conto = x.Righe(i).ContoPartita
                    xc.Sottoconto = x.Righe(i).SottocontoPartita

                    xc.Decodfica(ConnectionStringGenerale)

                    myriga(0) = xc.Mastro & " " & xc.Conto & " " & xc.Sottoconto & " " & xc.Descrizione
                    If x.Righe(i).DareAvere = "D" Then
                        myriga(1) = Format(x.Righe(i).Importo, "#,##0.00")
                    Else
                        myriga(1) = 0
                    End If
                    If x.Righe(i).DareAvere = "A" Then
                        myriga(2) = Format(x.Righe(i).Importo, "#,##0.00")
                    Else
                        myriga(2) = 0
                    End If
                    myriga(3) = x.Righe(i).Descrizione

                    REM myriga(8) = x.Righe(i).MastroPartita
                    REM myriga(9) = x.Righe(i).ContoPartita
                    REM myriga(10) = x.Righe(i).SottocontoPartita

                    xc.Mastro = x.Righe(i).MastroContropartita
                    xc.Conto = x.Righe(i).ContoContropartita
                    xc.Sottoconto = x.Righe(i).SottocontoContropartita

                    xc.Decodfica(ConnectionStringGenerale)

                    myriga(4) = xc.Mastro & " " & xc.Conto & " " & xc.Sottoconto & " " & xc.Descrizione

                    myriga(5) = x.Righe(i).RigaRegistrazione

                    Tabella.Rows.Add(myriga)
                Else
                    ViewState("NonModificabile") = 1
                End If
            Else
                Exit For
            End If
        Next


        Grid.AutoGenerateColumns = False
        Grid.DataSource = Tabella
        Grid.DataBind()

        If Session("TIPOAPP") <> "RSA" Then
            Grid.Columns(4).Visible = False
            Grid.Columns(5).Visible = False
        End If

        Lbl_BtnLegami.Text = ""

        If x.VerificaRegistroIVA(Session("DC_GENERALE"), Session("DC_TABELLE")) Then
            Lbl_BtnLegami.Text = Lbl_BtnLegami.Text & "<label class=""MenuDestra"">&nbsp;&nbsp;<a href=""#"" class="""" onclick=""DialogBox('Legami.aspx?Numero=" & Txt_Numero.Text & "');"">Legami</a></label>"
        End If

        Dim DatiGenerali As New Cls_DatiGenerali

        DatiGenerali.LeggiDati(Session("DC_TABELLE"))


        If DatiGenerali.BudgetAnalitica = 0 Then
            Lbl_BtnLegami.Text = Lbl_BtnLegami.Text & "<label class=""MenuDestra"">&nbsp;&nbsp;<a href=""#"" class="""" onclick=""DialogBox('LegameRegistrazionePrenotazione.aspx?Numero=" & Txt_Numero.Text & "');"">Legame Prenot.</a></label>"
            Lbl_BtnLegami.Text = Lbl_BtnLegami.Text & "<label class=""MenuDestra"">&nbsp;&nbsp;<a href=""#"" class="""" onclick=""DialogBox('LegameRegistrazioneBudget.aspx?Numero=" & Txt_Numero.Text & "');"">Legame Budget</a></label>"
        Else
            Lbl_BtnLegami.Text = Lbl_BtnLegami.Text & "<label class=""MenuDestra"">&nbsp;&nbsp;<a href=""#"" class="""" onclick=""DialogBox('LegameRegistrazioneBudget.aspx?Numero=" & Txt_Numero.Text & "');"">Legame Analitica</a></label>"
        End If

        Lbl_BtnLegami.Text = Lbl_BtnLegami.Text & "<label class=""MenuDestra"">&nbsp;&nbsp;<a href=""#"" class="""" onclick=""DialogBox('RateiRisconti.aspx?Numero=" & Txt_Numero.Text & "');"">Ratei Risconti</a></label>"
        Lbl_BtnLegami.Text = Lbl_BtnLegami.Text & "<label class=""MenuDestra"">&nbsp;&nbsp;<a href=""#"" class="""" onclick=""DialogBox('DatiMandatiReversali.aspx?Numero=" & Txt_Numero.Text & "');"">Dati Man/Rev</a></label>"

        Lbl_BtnScadenzario.Text = "<label class=""MenuDestra"">&nbsp;&nbsp;<a href=""#"" class="""" onclick=""DialogBoxSlim('gestionescadenzario.aspx?Numero=" & Txt_Numero.Text & "');"">Scadenzario</a></label>"

        'Lbl_BtnLegami.Text = Lbl_BtnLegami.Text & "<a href=""#"" class="""" onclick=""DialogBox('Legami.aspx?Numero=" & Txt_Numero.Text & "');""><img src=""images/Legami.jpg"" title=""Legami"" /></a>"
        'Lbl_BtnLegami.Text = Lbl_BtnLegami.Text & "<a href=""#"" class="""" onclick=""DialogBox('LegameRegistrazionePrenotazione.aspx?Numero=" & Txt_Numero.Text & "');""><img src=""images/prenotazione.png"" title=""Legame Prenotazioni"" /></a>"
        'Lbl_BtnLegami.Text = Lbl_BtnLegami.Text & "<a href=""#"" class="""" onclick=""DialogBox('LegameRegistrazioneBudget.aspx?Numero=" & Txt_Numero.Text & "');""><img src=""images/budget.png"" title=""Legame Budget"" /></a>"
        'Lbl_BtnLegami.Text = Lbl_BtnLegami.Text & "<a href=""#"" class="""" onclick=""DialogBoxW('RateiRisconti.aspx?Numero=" & Txt_Numero.Text & "');""><img src=""images/rateirisconti.png"" title=""Ratei Risconti"" /></a>"

        'Lbl_BtnScadenzario.Text = "<a href=""#"" class="""" onclick=""DialogBoxSlim('gestionescadenzario.aspx?Numero=" & Txt_Numero.Text & "');""><img src=""images/scadenziario.png"" title=""Scadenzario"" /></a>"


        Lbl_TipoRegistrazione.Text = ""
        ViewState("RighePrimaNota") = Tabella
        If Session("TIPOAPP") <> "RSA" Then

            If x.IdProgettoODV > 0 Then
                Lbl_TipoRegistrazione.Text = "<font color=""red"">REGISTRAZIONE PROGETTO</font>"
            End If
            If x.IDODV > 0 Then
                Lbl_TipoRegistrazione.Text = "<font color=""red"">REGISTRAZIONE TESSERAMENTO</font>"
                Exit Sub
            End If

            If x.TipoODV > 0 Then
                Lbl_TipoRegistrazione.Text = "<font color=""red"">REGISTRAZIONE CAUZIONE</font>"
            End If
        End If

        Dim LabelTotDare As Double = 0
        Dim LabelTotAvere As Double = 0

        Try
            For i = 0 To Tabella.Rows.Count - 1
                If CDbl(Tabella.Rows(i).Item("Dare")) > 0 Then
                    LabelTotDare = LabelTotDare + CDbl(Tabella.Rows(i).Item("Dare"))
                End If
                If CDbl(Tabella.Rows(i).Item("Avere")) > 0 Then
                    LabelTotAvere = LabelTotAvere + CDbl(Tabella.Rows(i).Item("Avere"))
                End If
            Next
        Catch ex As Exception

        End Try


        Grid.FooterRow.Cells(2).HorizontalAlign = HorizontalAlign.Right
        Grid.FooterRow.Cells(2).Text = Format(LabelTotDare, "#,##0.00")
        Grid.FooterRow.Cells(3).Text = Format(LabelTotAvere, "#,##0.00")
        Grid.FooterRow.Cells(3).HorizontalAlign = HorizontalAlign.Right


    End Sub

    Function DecodificaCauzione(ByVal StringaConnessione As String) As String
        Dim cn As OleDbConnection


        DecodificaCauzione = ""

        cn = New Data.OleDb.OleDbConnection(StringaConnessione)

        cn.Open()
        Dim cmd As New OleDbCommand()
        cmd.CommandText = ("select * from Parametri")
        cmd.Connection = cn

        Dim myPOSTreader As OleDbDataReader = cmd.ExecuteReader()
        If myPOSTreader.Read Then

            DecodificaCauzione = campodb(myPOSTreader.Item("CausaleDotazioni"))


        End If
        myPOSTreader.Close()
        cn.Close()
    End Function

    Private Sub CaricaPagina()
        Dim k As New Cls_CausaleContabile

        Dim ConnectionString As String = Session("DC_TABELLE")

        k.UpDateDropBox(ConnectionString, Dd_CausaleContabile)

        If Val(Session("NumeroRegistrazione")) > 0 Then
            Txt_Numero.Text = Val(Session("NumeroRegistrazione"))
            Call leggiregistrazione()
        Else
            Tabella.Clear()
            Tabella.Columns.Clear()
            Tabella.Columns.Add("Sottoconto", GetType(String))
            Tabella.Columns.Add("Dare", GetType(String))
            Tabella.Columns.Add("Avere", GetType(String))
            Tabella.Columns.Add("Descrizione", GetType(String))
            Tabella.Columns.Add("SottocontoControPartita", GetType(String))
            Tabella.Columns.Add("RigaRegistrazione", GetType(String))
            Dim myriga As System.Data.DataRow = Tabella.NewRow()
            myriga(1) = 0
            myriga(2) = 0
            Tabella.Rows.Add(myriga)
            Grid.AutoGenerateColumns = False
            Grid.DataSource = Tabella
            Grid.DataBind()

            If Session("TIPOAPP") <> "RSA" Then
                Grid.Columns(4).Visible = False
                Grid.Columns(5).Visible = False
            End If


            ViewState("RighePrimaNota") = Tabella
            Call PulisciRegistrazione()
        End If
        EseguiJS()
    End Sub
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Trim(Session("UTENTE")) = "" Then
            If Request.ServerVariables("URL").ToUpper.IndexOf("SENIORWEBBLANCO") > 0 Then
                Response.Redirect("/ODV/Login.aspx")
                Exit Sub
            Else
                Response.Redirect("/SeniorWeb/Login.aspx")
                Exit Sub
            End If
        End If

        If Session("ABILITAZIONI").ToString.IndexOf("<REGISTRAZIONE>") < 0 Then
            Response.Redirect("../MainMenu.aspx")
            Exit Sub
        End If

        LblBox.Text = ""

        Dim MyJs As String

        MyJs = "$(" & Chr(34) & "#" & Txt_DataRegistrazione.ClientID & Chr(34) & ").mask(""99/99/9999""); "
        MyJs = MyJs & "$(" & Chr(34) & "#" & Txt_DataMandato.ClientID & Chr(34) & ").mask(""99/99/9999""); "
        MyJs = MyJs & "$(" & Chr(34) & "#" & Txt_DataReversale.ClientID & Chr(34) & ").mask(""99/99/9999""); "
        MyJs = MyJs & "$(" & Chr(34) & "#" & Txt_DataDistinta.ClientID & Chr(34) & ").mask(""99/99/9999""); "

        MyJs = MyJs & " $(" & Chr(34) & "#" & Txt_DataRegistrazione.ClientID & Chr(34) & ").datepicker({ changeMonth: true,changeYear: true,  yearRange: ""1990:" & Year(Now) + 5 & """},$.datepicker.regional[""it""]);"
        MyJs = MyJs & " $(" & Chr(34) & "#" & Txt_DataMandato.ClientID & Chr(34) & ").datepicker({ changeMonth: true,changeYear: true,  yearRange: ""1990:" & Year(Now) + 5 & """},$.datepicker.regional[""it""]);"
        MyJs = MyJs & " $(" & Chr(34) & "#" & Txt_DataReversale.ClientID & Chr(34) & ").datepicker({ changeMonth: true,changeYear: true,  yearRange: ""1990:" & Year(Now) + 5 & """},$.datepicker.regional[""it""]);"
        MyJs = MyJs & " $(" & Chr(34) & "#" & Txt_DataDistinta.ClientID & Chr(34) & ").datepicker({ changeMonth: true,changeYear: true,  yearRange: ""1990:" & Year(Now) + 5 & """},$.datepicker.regional[""it""]);"


        MyJs = MyJs & " $(" & Chr(34) & "#" & Txt_DataRegistrazione.ClientID & Chr(34) & ").blur(function() {" & vbNewLine
        MyJs = MyJs & " var apponumero = $(" & Chr(34) & "#" & Txt_Numero.ClientID & Chr(34) & ").val();" & vbNewLine
        MyJs = MyJs & " var prova  = parseInt(apponumero);" & vbNewLine
        MyJs = MyJs & " if (prova  != 0) {" & vbNewLine
        MyJs = MyJs & " return true; }" & vbNewLine
        MyJs = MyJs & " var today = new Date(); " & vbNewLine
        MyJs = MyJs & " var appoggio = '' + $(" & Chr(34) & "#" & Txt_DataRegistrazione.ClientID & Chr(34) & ").val();"
        MyJs = MyJs & " var dateArray = appoggio.split('/');" & vbNewLine
        MyJs = MyJs & " if (today.getFullYear() != dateArray[2]) { " & vbNewLine
        MyJs = MyJs & " $(" & Chr(34) & "#" & Txt_DataRegistrazione.ClientID & Chr(34) & ").css(""border"",""3px solid #FFAA00"");"
        MyJs = MyJs & " } else { " & vbNewLine
        MyJs = MyJs & " $(" & Chr(34) & "#" & Txt_DataRegistrazione.ClientID & Chr(34) & ").css(""border"",""1px solid #8894A0"");"
        MyJs = MyJs & " } "
        MyJs = MyJs & " });"
        ScriptManager.RegisterStartupScript(Me, Me.GetType(), "FormatDataRegistrazione", MyJs, True)

        Timer1.Interval = 900 * 1000



        Dim Param As New Cls_DatiGenerali

        Param.LeggiDati(Session("DC_TABELLE"))

        If Param.TipoVisualizzazione = 1 Then
            Grid.CellPadding = "0"
            Grid.CellSpacing = "0"
        End If

        If Page.IsPostBack = True Then
            Exit Sub
        End If


        Dim Cserv As New Cls_CentroServizio

        Cserv.UpDateDropBox(Session("DC_OSPITE"), DD_CSERV)

        Dim Into As New Cls_DatiGenerali

        Into.LeggiDati(Session("DC_TABELLE"))

        If Into.AttivaCServPrimanoIncassi = 1 Then

            DD_CSERV.Visible = True
            Lbl_CentroSerivizio.Visible = True
        End If

        BtnOpen.Visible = False
        BtnOpen2.Visible = False
        If Val(Request.Item("NumeroRegistrazione")) > 0 Then
            Session("NumeroRegistrazione") = Val(Request.Item("NumeroRegistrazione"))
        End If

        Dim K1 As New Cls_SqlString

        Dim Barra As New Cls_BarraSenior

        If Not IsNothing(Session("RicercaGeneraleSQLString")) Then
            Try
                K1 = Session("RicercaGeneraleSQLString")
            Catch ex As Exception

            End Try
        End If

        Lbl_BarraSenior.Text = Barra.CodiceBarra(Application("SENIOR"), Session("UTENTE"), Page, K1)


        If Request.Item("Data") <> "" Then
            Dim DataCopia As String
            DataCopia = Request.Item("Data") & "00000000"
            Try
                Txt_DataRegistrazione.Text = Format(DateSerial(Val(Mid(DataCopia, 1, 4)), Val(Mid(DataCopia, 5, 2)), Val(Mid(DataCopia, 7, 2))), "dd/MM/yyyy")
            Catch ex As Exception

            End Try
        End If

        Call CaricaPagina()

        If Request.Item("CHIAMATAESTERNA") > 0 Then
            Btn_Esci.Visible = False
            ImgRicerca.Visible = False
            Call MettiInvisibileJS()
        End If
        If Request.Item("PROVENIENTE") = "SEMPLIFICATA" Then
            ImgRicerca.Visible = False
            Lbl_BtnLegami.Text = ""
            Lbl_BtnScadenzario.Text = ""
            Call MettiInvisibileSemplificataJS()
        End If



    End Sub

    Private Sub faibind()

        Tabella = ViewState("RighePrimaNota")

        Grid.AutoGenerateColumns = False
        Grid.DataSource = Tabella
        Grid.DataBind()

        If Session("TIPOAPP") <> "RSA" Then
            Grid.Columns(4).Visible = False
            Grid.Columns(5).Visible = False
        End If

    End Sub

    Protected Sub Grid_RowCancelingEdit(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewCancelEditEventArgs) Handles Grid.RowCancelingEdit
        ' Grid.EditIndex = -1
        ' Call faibind()
    End Sub


    Private Sub InserisciRiga()
        If Dd_CausaleContabile.SelectedValue = "" Then
            ScriptManager.RegisterClientScriptBlock(Me, Me.GetType(), "Errore", "VisualizzaErrore('<center>Non è possibile inserire una riga senza selezionare la causale</center>');", True)
            EseguiJS()
            Exit Sub
        End If

        Lbl_Errore.Text = ""

        UpDateTable()
        Tabella = ViewState("RighePrimaNota")
        Dim myriga As System.Data.DataRow = Tabella.NewRow()
        myriga(1) = 0
        myriga(2) = 0
        Tabella.Rows.Add(myriga)





        If Tabella.Rows.Count >= 2 Then
            Dim TotDare As Double
            Dim TotAvere As Double
            Dim Dare As Double
            Dim Avere As Double

            For i = 0 To Tabella.Rows.Count - 1
                TotDare = TotDare + Tabella.Rows(i).Item("Dare")
                TotAvere = TotAvere + Tabella.Rows(i).Item("Avere")
            Next

            For i = 0 To Tabella.Rows.Count - 1
                Dare = Tabella.Rows(i).Item("Dare")
                Avere = Tabella.Rows(i).Item("Avere")

                If TotAvere > TotDare And Dare = 0 And Avere = 0 Then
                    Tabella.Rows(i).Item("Dare") = Format(TotAvere - TotDare, "#,##0.00")
                End If
                If TotAvere < TotDare And Avere = 0 And Dare = 0 Then
                    Tabella.Rows(i).Item("Avere") = Format(TotDare - TotAvere, "#,##0.00")
                End If
            Next
        End If

        ViewState("RighePrimaNota") = Tabella
        Call faibind()

        Dim LabelTotDare As Double = 0
        Dim LabelTotAvere As Double = 0

        Try
            For i = 0 To Tabella.Rows.Count - 1
                If CDbl(Tabella.Rows(i).Item("Dare")) > 0 Then
                    LabelTotDare = LabelTotDare + CDbl(Tabella.Rows(i).Item("Dare"))
                End If
                If CDbl(Tabella.Rows(i).Item("Avere")) > 0 Then
                    LabelTotAvere = LabelTotAvere + CDbl(Tabella.Rows(i).Item("Avere"))
                End If
            Next
        Catch ex As Exception

        End Try

        Grid.FooterRow.Cells(2).HorizontalAlign = HorizontalAlign.Right
        Grid.FooterRow.Cells(2).Text = Format(LabelTotDare, "#,##0.00")
        Grid.FooterRow.Cells(3).Text = Format(LabelTotAvere, "#,##0.00")
        Grid.FooterRow.Cells(3).HorizontalAlign = HorizontalAlign.Right

        If Session("TIPOAPP") <> "RSA" Then
            Grid.Columns(4).Visible = False
            Grid.Columns(5).Visible = False
        End If

        If Val(Txt_Numero.Text) = 0 Then
            Dim XT As New Cls_CausaleContabile
            XT.Leggi(Session("DC_TABELLE"), Dd_CausaleContabile.SelectedValue)

            For I = 0 To Grid.Rows.Count - 1
                Grid.Rows(I).Cells(2).BackColor = Drawing.Color.White
                Grid.Rows(I).Cells(3).BackColor = Drawing.Color.White
                If Not IsNothing(XT.Righe(I)) Then
                    If XT.Righe(I).Sottoconto <> 0 Or XT.Righe(I).Conto <> 0 Or XT.Righe(I).Mastro <> 0 Then
                        Dim NumeroRiga As Integer = I
                        If campodbn(Tabella.Rows(I).Item("RigaRegistrazione")) > 0 Then
                            NumeroRiga = campodbn(Tabella.Rows(I).Item("RigaRegistrazione"))
                        End If

                        If XT.Righe(NumeroRiga).DareAvere = "A" Then
                            Grid.Rows(I).Cells(3).BackColor = Drawing.Color.Yellow
                        End If
                        If XT.Righe(NumeroRiga).DareAvere = "D" Then
                            Grid.Rows(I).Cells(2).BackColor = Drawing.Color.Yellow
                        End If
                    End If
                End If
            Next
        End If


        Call EseguiJS()

    End Sub
    Protected Sub Grid_RowCommand(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewCommandEventArgs) Handles Grid.RowCommand
        If (e.CommandName = "Inserisci") Then
            InserisciRiga()
        End If
    End Sub
    Function campodb(ByVal oggetto As Object) As String
        If IsDBNull(oggetto) Then
            Return ""
        Else
            Return oggetto
        End If
    End Function
    Function campodbn(ByVal oggetto As Object) As Double
        If IsDBNull(oggetto) Then
            Return 0
        Else
            Return oggetto
        End If
    End Function




    Protected Sub Grid_RowCreated(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs) Handles Grid.RowCreated

        REM  e.Row.Cells(8).Visible = False
        REM   e.Row.Cells(9).Visible = False
        REM  e.Row.Cells(10).Visible = False
        REM  e.Row.Cells(11).Visible = False
    End Sub


    Protected Sub Grid_RowDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs) Handles Grid.RowDataBound
        If e.Row.RowType = DataControlRowType.DataRow Then

            Dim LblRiga As Label = DirectCast(e.Row.FindControl("LblRiga"), Label)
            If Not IsNothing(LblRiga) Then

            Else
                Dim TxtRiga As TextBox = DirectCast(e.Row.FindControl("TxtRiga"), TextBox)
                Dim MyDv As System.Data.DataRowView = e.Row.DataItem
                TxtRiga.Text = Tabella.Rows(e.Row.RowIndex).Item(5).ToString
            End If

            Dim LblSottoconto As Label = DirectCast(e.Row.FindControl("LblSottoconto"), Label)
            If Not IsNothing(LblSottoconto) Then

            Else
                Dim TxtSottoconto As TextBox = DirectCast(e.Row.FindControl("TxtSottoconto"), TextBox)
                Dim MyDv As System.Data.DataRowView = e.Row.DataItem
                TxtSottoconto.Text = MyDv(0).ToString

                REM TxtSottoconto.Text = Tabella.Rows(e.Row.RowIndex).Item(0).ToString

                Dim MyJs As String

                MyJs = "$(" & Chr(34) & "#" & TxtSottoconto.ClientID & Chr(34) & ").autocomplete('GestioneConto.ashx?Utente=" & Session("UTENTE") & "', {delay:5,minChars:3});"
                ScriptManager.RegisterStartupScript(Me, Me.GetType(), "visualizza", MyJs, True)
            End If


            Dim LblDare As Label = DirectCast(e.Row.FindControl("LblDare"), Label)
            If Not IsNothing(LblDare) Then

            Else
                Dim TxtDare As TextBox = DirectCast(e.Row.FindControl("TxtDare"), TextBox)
                TxtDare.Text = Tabella.Rows(e.Row.RowIndex).Item(1).ToString
                Dim MyJs As String

                MyJs = "$('#" & TxtDare.ClientID & "').keypress(function() { ForceNumericInput($('#" & TxtDare.ClientID & "').val(), true, true); } );  $('#" & TxtDare.ClientID & "').blur(function() { var ap = formatNumber ($('#" & TxtDare.ClientID & "').val(),2); $('#" & TxtDare.ClientID & "').val(ap); }); "
                ScriptManager.RegisterStartupScript(Me, Me.GetType(), "visualizzaD", MyJs, True)
            End If

            Dim LblAvere As Label = DirectCast(e.Row.FindControl("LblAvere"), Label)
            If Not IsNothing(LblAvere) Then

            Else
                Dim TxtAvere As TextBox = DirectCast(e.Row.FindControl("TxtAvere"), TextBox)
                TxtAvere.Text = Tabella.Rows(e.Row.RowIndex).Item(2).ToString

                Dim MyJs As String

                MyJs = "$('#" & TxtAvere.ClientID & "').keypress(function() { ForceNumericInput($('#" & TxtAvere.ClientID & "').val(), true, true); } );  $('#" & TxtAvere.ClientID & "').blur(function() { var ap = formatNumber ($('#" & TxtAvere.ClientID & "').val(),2); $('#" & TxtAvere.ClientID & "').val(ap); }); "
                REM ClientScript.RegisterClientScriptBlock(Me.GetType(), "visualizzaA", MyJs, True)
                ScriptManager.RegisterStartupScript(Me, Me.GetType(), "visualizzaA", MyJs, True)
            End If


            Dim LblDescrizione As Label = DirectCast(e.Row.FindControl("LblDescrizione"), Label)
            If Not IsNothing(LblDescrizione) Then

            Else
                Dim TxtDescrizione As TextBox = DirectCast(e.Row.FindControl("TxtDescrizione"), TextBox)
                TxtDescrizione.Text = Tabella.Rows(e.Row.RowIndex).Item(3).ToString
            End If


            Dim LblSottocontoControPartita As Label = DirectCast(e.Row.FindControl("LblSottocontoControPartita"), Label)
            If Not IsNothing(LblSottocontoControPartita) Then

            Else
                Dim TxtSottocontoControPartita As TextBox = DirectCast(e.Row.FindControl("TxtSottocontoControPartita"), TextBox)
                TxtSottocontoControPartita.Text = Tabella.Rows(e.Row.RowIndex).Item(4).ToString

                Dim MyJs As String

                MyJs = "$(" & Chr(34) & "#" & TxtSottocontoControPartita.ClientID & Chr(34) & ").autocomplete('GestioneConto.ashx?Utente=" & Session("UTENTE") & "', {delay:5,minChars:3});"
                REM MyJs = "$(document).ready(function() { alert('ww'); } );"
                REM ClientScript.RegisterClientScriptBlock(Me.GetType(), "visualizza1", MyJs, True)
                ScriptManager.RegisterStartupScript(Me, Me.GetType(), "visualizza1", MyJs, True)
            End If
        End If
    End Sub

    Protected Sub Grid_RowDeleted(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewDeletedEventArgs) Handles Grid.RowDeleted

    End Sub

    Protected Sub Grid_RowDeleting(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewDeleteEventArgs) Handles Grid.RowDeleting

        Lbl_Errore.Text = ""
        UpDateTable()
        Tabella = ViewState("RighePrimaNota")
        Try
            If Request.UserAgent.Contains("Chrome") Then
                If IsDBNull(Session("RI_TIMER")) Or DateDiff("s", Session("RI_TIMER"), Now) > 1 Then
                    Tabella.Rows.RemoveAt(e.RowIndex)
                    If Tabella.Rows.Count = 0 Then
                        Dim myriga As System.Data.DataRow = Tabella.NewRow()
                        myriga(1) = 0
                        myriga(2) = 0
                        Tabella.Rows.Add(myriga)
                    End If
                    Session("RI_TIMER") = Now
                End If
            Else
                Tabella.Rows.RemoveAt(e.RowIndex)
                If Tabella.Rows.Count = 0 Then
                    Dim myriga As System.Data.DataRow = Tabella.NewRow()
                    myriga(1) = 0
                    myriga(2) = 0
                    Tabella.Rows.Add(myriga)
                End If
            End If
        Catch ex As Exception

        End Try


        ViewState("RighePrimaNota") = Tabella

        Call faibind()

        Dim LabelTotDare As Double = 0
        Dim LabelTotAvere As Double = 0

        Try
            For i = 0 To Tabella.Rows.Count - 1
                If CDbl(Tabella.Rows(i).Item("Dare")) > 0 Then
                    LabelTotDare = LabelTotDare + CDbl(Tabella.Rows(i).Item("Dare"))
                End If
                If CDbl(Tabella.Rows(i).Item("Avere")) > 0 Then
                    LabelTotAvere = LabelTotAvere + CDbl(Tabella.Rows(i).Item("Avere"))
                End If
            Next
        Catch ex As Exception

        End Try


        Grid.FooterRow.Cells(2).HorizontalAlign = HorizontalAlign.Right
        Grid.FooterRow.Cells(2).Text = Format(LabelTotDare, "#,##0.00")
        Grid.FooterRow.Cells(3).Text = Format(LabelTotAvere, "#,##0.00")
        Grid.FooterRow.Cells(3).HorizontalAlign = HorizontalAlign.Right

        If Val(Txt_Numero.Text) = 0 Then
            Dim XT As New Cls_CausaleContabile
            XT.Leggi(Session("DC_TABELLE"), Dd_CausaleContabile.SelectedValue)

            For I = 0 To Grid.Rows.Count - 1
                Grid.Rows(I).Cells(2).BackColor = Drawing.Color.White
                Grid.Rows(I).Cells(3).BackColor = Drawing.Color.White
                If Not IsNothing(XT.Righe(I)) Then
                    If XT.Righe(I).Sottoconto <> 0 Or XT.Righe(I).Conto <> 0 Or XT.Righe(I).Mastro <> 0 Then
                        Dim NumeroRiga As Integer = I
                        If campodbn(Tabella.Rows(I).Item("RigaRegistrazione")) > 0 Then
                            NumeroRiga = campodbn(Tabella.Rows(I).Item("RigaRegistrazione"))
                        End If

                        If XT.Righe(NumeroRiga).DareAvere = "A" Then
                            Grid.Rows(I).Cells(3).BackColor = Drawing.Color.Yellow
                        End If
                        If XT.Righe(NumeroRiga).DareAvere = "D" Then
                            Grid.Rows(I).Cells(2).BackColor = Drawing.Color.Yellow
                        End If
                    End If
                End If
            Next
        End If
        Call EseguiJS()

        e.Cancel = True

    End Sub




    Protected Sub Grid_RowEditing(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewEditEventArgs) Handles Grid.RowEditing
        ' Grid.EditIndex = e.NewEditIndex
        ' Call faibind()
    End Sub

    Protected Sub Grid_RowUpdated(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewUpdatedEventArgs) Handles Grid.RowUpdated

    End Sub



    Private Sub UpDateTable()
        Dim i As Integer

        Tabella.Clear()
        Tabella.Columns.Clear()
        Tabella.Columns.Add("Sottoconto", GetType(String))
        Tabella.Columns.Add("Dare", GetType(String))
        Tabella.Columns.Add("Avere", GetType(String))
        Tabella.Columns.Add("Descrizione", GetType(String))
        Tabella.Columns.Add("SottocontoControPartita", GetType(String))
        Tabella.Columns.Add("RigaRegistrazione", GetType(String))


        For i = 0 To Grid.Rows.Count - 1

            Dim TxtSottoconto As TextBox = DirectCast(Grid.Rows(i).FindControl("TxtSottoconto"), TextBox)
            Dim TxtDare As TextBox = DirectCast(Grid.Rows(i).FindControl("TxtDare"), TextBox)
            Dim TxtAvere As TextBox = DirectCast(Grid.Rows(i).FindControl("TxtAvere"), TextBox)
            Dim TxtDescrizione As TextBox = DirectCast(Grid.Rows(i).FindControl("TxtDescrizione"), TextBox)
            Dim TxtSottocontoControPartita As TextBox = DirectCast(Grid.Rows(i).FindControl("TxtSottocontoControPartita"), TextBox)
            Dim TxtRiga As TextBox = DirectCast(Grid.Rows(i).FindControl("TxtRiga"), TextBox)

            Lbl_Errore.Text = ""
            Dim x As New Cls_ControllaSottoconto



            If TxtAvere.Text = "" Then
                TxtAvere.Text = "0"
            End If

            If TxtDare.Text = "" Then
                TxtDare.Text = "0"
            End If

            Dim myriga As System.Data.DataRow = Tabella.NewRow()

            myriga(0) = TxtSottoconto.Text
            myriga(1) = TxtDare.Text
            myriga(2) = TxtAvere.Text
            myriga(3) = TxtDescrizione.Text
            myriga(4) = TxtSottocontoControPartita.Text
            myriga(5) = Val(TxtRiga.Text)
            Tabella.Rows.Add(myriga)
        Next

        ViewState("RighePrimaNota") = Tabella

        Call faibind()
    End Sub

    Private Sub EseguiJS()
        Dim MyJs As String
        MyJs = "$(document).ready(function() {  "

        'MyJs = MyJs & " alert(myTimeReminder);"

        'Dim int_MilliSecondsTimeReminder As Integer
        'Dim int_MilliSecondsTimeOut As Integer
        'Dim msgSession As String
        'msgSession = "Warning: Se nei prossimi 3 minuti, non fai nessuna operazione, il sistema tornerà alla pagina di login. Salva i tuoi dati."

        'int_MilliSecondsTimeReminder = ((Page.Session.Timeout - 3) * 50 * 1000)
        'int_MilliSecondsTimeOut = ((Page.Session.Timeout) * 50 * 1000)

        'MyJs = MyJs & " clearTimeout(myTimeReminder); " & vbNewLine & _
        '        " clearTimeout(myTimeOut); " & vbNewLine & _
        '        "var sessionTimeReminder = " & int_MilliSecondsTimeReminder.ToString() & ";" & vbNewLine & _
        '        "var sessionTimeout = " & int_MilliSecondsTimeOut.ToString() & ";" & vbNewLine & _
        '        "function doReminder(){ alert('" + msgSession & "'); }" & vbNewLine & _
        '        "function doRedirect(){ window.location.href='/Seniorweb/Login.aspx'; }" & vbNewLine & _
        '        " myTimeReminder=setTimeout('document.getElementById(""IdTimeOut"").style.visibility= ""visible"";',sessionTimeReminder); myTimeOut=setTimeout('window.location.href=""/Seniorweb/Login.aspx"";',sessionTimeout); " & vbNewLine

        If Request.Item("NONMENU") = "OK" Then
            MyJs = MyJs & "$('.destraclasse').css('display','none');"
            MyJs = MyJs & "$('.Barra').css('display','none');"
        End If

        MyJs = MyJs & "var els = document.getElementsByTagName(""*"");"

        MyJs = MyJs & "for (var i=0;i<els.length;i++)"
        MyJs = MyJs & "if ( els[i].id ) { "
        MyJs = MyJs & " var appoggio =els[i].id; "
        MyJs = MyJs & " if (appoggio.match('TxtSottoconto')!= null) {  "
        MyJs = MyJs & " $(els[i]).autocomplete('GestioneConto.ashx?Utente=" & Session("UTENTE") & "', {delay:5,cacheLength: 50,minChars:3});"
        MyJs = MyJs & "    }"

        MyJs = MyJs & " if ((appoggio.match('TxtDare')!= null) || appoggio.match('TxtAvere')!= null) {  "
        MyJs = MyJs & " $(els[i]).keypress(function() { ForceNumericInput($(this).val(), true, true); } ); "
        MyJs = MyJs & " $(els[i]).blur(function() { var ap = formatNumber($(this).val(),2); $(this).val(ap); });"
        MyJs = MyJs & "    }"


        MyJs = MyJs & "} "

        MyJs = MyJs & "if (window.innerHeight>0) { $(""#BarraLaterale"").css(""height"",(window.innerHeight - 94) + ""px""); } else"
        MyJs = MyJs & "{ $(""#BarraLaterale"").css(""height"",(document.documentElement.offsetHeight - 94) + ""px"");  }"


        MyJs = MyJs & "$("".chosen-select"").chosen({"
        MyJs = MyJs & "no_results_text: ""Causale non trovata"","
        MyJs = MyJs & "display_disabled_options: true,"
        'MyJs = MyJs & "allow_single_deselect: true,"
        MyJs = MyJs & "width: ""350px"","
        MyJs = MyJs & "placeholder_text_single: ""Seleziona Causale"""
        MyJs = MyJs & "});"


        MyJs = MyJs & "$("".chosen-selectcs"").chosen({"
        MyJs = MyJs & "no_results_text: ""Centro Servizio non trovata"","
        MyJs = MyJs & "display_disabled_options: true,"
        'MyJs = MyJs & "allow_single_deselect: true,"
        MyJs = MyJs & "width: ""350px"","
        MyJs = MyJs & "placeholder_text_single: ""Seleziona Centro Servizio"""
        MyJs = MyJs & "});"


        MyJs = MyJs & "});"

        'MyJs = "$(document).ready(function() { $('.myClass').keypress(function() { handleEnter($('.myClass').val(), true, true); } );     $('#" & Txt_ImportoPagato.ClientID & "').blur(function() { var ap = formatNumber ($('#" & Txt_ImportoPagato.ClientID & "').val(),2); $('#" & Txt_ImportoPagato.ClientID & "').val(ap); }); });"
        ScriptManager.RegisterStartupScript(Me, Me.GetType(), "visualizzaRitIm", MyJs, True)
    End Sub


    Protected Sub Grid_RowUpdating(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewUpdateEventArgs) Handles Grid.RowUpdating
        'Dim TxtSottoconto As TextBox = DirectCast(Grid.Rows(e.RowIndex).FindControl("TxtSottoconto"), TextBox)
        'Dim TxtDare As TextBox = DirectCast(Grid.Rows(e.RowIndex).FindControl("TxtDare"), TextBox)
        'Dim TxtAvere As TextBox = DirectCast(Grid.Rows(e.RowIndex).FindControl("TxtAvere"), TextBox)
        'Dim TxtDescrizione As TextBox = DirectCast(Grid.Rows(e.RowIndex).FindControl("TxtDescrizione"), TextBox)
        'Dim TxtSottocontoControPartita As TextBox = DirectCast(Grid.Rows(e.RowIndex).FindControl("TxtSottocontoControPartita"), TextBox)
        'Lbl_Errore.Text = ""
        'Dim x As New Cls_ControllaSottoconto



        'If Not x.ControllaSottoconto(TxtSottoconto.Text) Then            
        '    ScriptManager.RegisterClientScriptBlock(Me, Me.GetType(), "Errorecr1", "VisualizzaErrore('Conto partita errato');", True)
        '    REM Lbl_Errore.Text = "Conto partita errato"

        '    Dim MyJs As String

        '    MyJs = "$(" & Chr(34) & "#" & TxtSottoconto.ClientID & Chr(34) & ").autocomplete('GestioneConto.ashx?Utente=" & Session("UTENTE") & "', {delay:5,minChars:3}); "
        '    ScriptManager.RegisterStartupScript(Me, Me.GetType(), "visualizza", MyJs, True)



        '    MyJs = "$('#" & TxtDare.ClientID & "').keypress(function() { ForceNumericInput($('#" & TxtDare.ClientID & "').val(), true, true); } );  $('#" & TxtDare.ClientID & "').blur(function() { var ap = formatNumber ($('#" & TxtDare.ClientID & "').val(),2); $('#" & TxtDare.ClientID & "').val(ap); });"
        '    ScriptManager.RegisterStartupScript(Me, Me.GetType(), "visualizzaD", MyJs, True)

        '    MyJs = "$('#" & TxtAvere.ClientID & "').keypress(function() { ForceNumericInput($('#" & TxtAvere.ClientID & "').val(), true, true); } );  $('#" & TxtAvere.ClientID & "').blur(function() { var ap = formatNumber ($('#" & TxtAvere.ClientID & "').val(),2); $('#" & TxtAvere.ClientID & "').val(ap); }); "
        '    ScriptManager.RegisterStartupScript(Me, Me.GetType(), "visualizzaA", MyJs, True)
        '    Exit Sub
        'Else
        '    TxtSottoconto.Text = x.DecodificaSottoconto(Session("DC_GENERALE"), TxtSottoconto.Text)
        'End If
        'If TxtSottocontoControPartita.Text.Trim <> "" Then
        '    If Not x.ControllaSottoconto(TxtSottocontoControPartita.Text) Then            
        '        ScriptManager.RegisterClientScriptBlock(Me, Me.GetType(), "Errorecr1", "VisualizzaErrore(""Conto contro partita errato"");", True)

        '        Dim MyJs As String

        '        MyJs = "$(" & Chr(34) & "#" & TxtSottoconto.ClientID & Chr(34) & ").autocomplete('GestioneConto.ashx?Utente=" & Session("UTENTE") & "', {delay:5,minChars:3}); "
        '        ScriptManager.RegisterStartupScript(Me, Me.GetType(), "visualizza", MyJs, True)

        '        MyJs = "$('#" & TxtDare.ClientID & "').keypress(function() { ForceNumericInput($('#" & TxtDare.ClientID & "').val(), true, true); } );  $('#" & TxtDare.ClientID & "').blur(function() { var ap = formatNumber ($('#" & TxtDare.ClientID & "').val(),2); $('#" & TxtDare.ClientID & "').val(ap); }); "
        '        ScriptManager.RegisterStartupScript(Me, Me.GetType(), "visualizzaD", MyJs, True)

        '        MyJs = "$('#" & TxtAvere.ClientID & "').keypress(function() { ForceNumericInput($('#" & TxtAvere.ClientID & "').val(), true, true); } );  $('#" & TxtAvere.ClientID & "').blur(function() { var ap = formatNumber ($('#" & TxtAvere.ClientID & "').val(),2); $('#" & TxtAvere.ClientID & "').val(ap); }); "
        '        ScriptManager.RegisterStartupScript(Me, Me.GetType(), "visualizzaA", MyJs, True)
        '        Exit Sub
        '    Else
        '        TxtSottocontoControPartita.Text = x.DecodificaSottoconto(Session("DC_GENERALE"), TxtSottocontoControPartita.Text)
        '    End If
        'End If
        'If TxtAvere.Text = "" Then
        '    TxtAvere.Text = "0"
        'End If

        'If TxtDare.Text = "" Then
        '    TxtDare.Text = "0"
        'End If

        'If CDbl(TxtDare.Text) > 0 And CDbl(TxtAvere.Text) > 0 Then
        '    ScriptManager.RegisterClientScriptBlock(Me, Me.GetType(), "Errorecr1", "VisualizzaErrore(""Possibile inserire il valore solo in dare o in avere"");", True)

        '    Dim MyJs As String

        '    MyJs = "$(" & Chr(34) & "#" & TxtSottoconto.ClientID & Chr(34) & ").autocomplete('GestioneConto.ashx?Utente=" & Session("UTENTE") & "', {delay:5,minChars:3}); "
        '    ScriptManager.RegisterStartupScript(Me, Me.GetType(), "visualizza", MyJs, True)

        '    MyJs = "$('#" & TxtDare.ClientID & "').keypress(function() { ForceNumericInput($('#" & TxtDare.ClientID & "').val(), true, true); } );  $('#" & TxtDare.ClientID & "').blur(function() { var ap = formatNumber ($('#" & TxtDare.ClientID & "').val(),2); $('#" & TxtDare.ClientID & "').val(ap); });"
        '    ScriptManager.RegisterStartupScript(Me, Me.GetType(), "visualizzaD", MyJs, True)

        '    MyJs = "$('#" & TxtAvere.ClientID & "').keypress(function() { ForceNumericInput($('#" & TxtAvere.ClientID & "').val(), true, true); } );  $('#" & TxtAvere.ClientID & "').blur(function() { var ap = formatNumber ($('#" & TxtAvere.ClientID & "').val(),2); $('#" & TxtAvere.ClientID & "').val(ap); }); "
        '    ScriptManager.RegisterStartupScript(Me, Me.GetType(), "visualizzaA", MyJs, True)

        '    REM Lbl_Errore.Text = "E' possibile inserire il valore solo in dare o in avere"
        '    Exit Sub
        'End If

        'Tabella = ViewState("RighePrimaNota")
        'Dim row = Grid.Rows(e.RowIndex)

        'Tabella.Rows(row.DataItemIndex).Item(0) = TxtSottoconto.Text
        'Tabella.Rows(row.DataItemIndex).Item(1) = TxtDare.Text
        'Tabella.Rows(row.DataItemIndex).Item(2) = TxtAvere.Text
        'Tabella.Rows(row.DataItemIndex).Item(3) = TxtDescrizione.Text
        'Tabella.Rows(row.DataItemIndex).Item(4) = TxtSottocontoControPartita.Text


        'ViewState("RighePrimaNota") = Tabella

        'Grid.EditIndex = -1
        'Call faibind()
    End Sub


    Private Function SplitWords(ByVal s As String) As String()
        '
        ' Call Regex.Split function from the imported namespace.
        ' Return the result array.
        '
        Return Regex.Split(s, "\W+")
    End Function



    Protected Sub Txt_Numero_TextChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles Txt_Numero.TextChanged
        If Val(Txt_Numero.Text) > 0 Then
            Dim x As New Cls_MovimentoContabile
            x.Leggi(Session("DC_GENERALE"), Val(Txt_Numero.Text))
            If x.NumeroRegistrazione > 0 Then
                Call leggiregistrazione()
                Call EseguiJS()
            End If
        End If
    End Sub

    Protected Sub Btn_Modifica_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles Btn_Modifica.Click
        If Trim(Session("UTENTE")) = "" Then
            Response.Redirect("..\Login.aspx")
            Exit Sub
        End If

        If Dd_CausaleContabile.SelectedValue = "" Then
            ScriptManager.RegisterClientScriptBlock(Me, Me.GetType(), "Errore", "VisualizzaErrore('<center>Specificare causale contabile</center>');", True)
            EseguiJS()
            Exit Sub
        End If

        If ViewState("NonModificabile") = 1 Then
            ScriptManager.RegisterClientScriptBlock(Me, Me.GetType(), "Errore", "VisualizzaErrore('<center>Non è possibile modificare questa registrazione</center>');", True)
            EseguiJS()
            Exit Sub
        End If

        Dim MovRitenuta As New Cls_RitenuteAcconto

        MovRitenuta.NumeroRegistrazionePagamento = Val(Txt_Numero.Text)

        If MovRitenuta.RitenutePagateSuPagamento(Session("DC_GENERALE"), MovRitenuta.NumeroRegistrazionePagamento) Then
            ScriptManager.RegisterClientScriptBlock(Me, Me.GetType(), "Errore", "VisualizzaErrore('Non posso modificare registrazione,<BR/> ritenute pagate');", True)
            Exit Sub
        End If



        If MovRitenuta.RitenutePagateSuPagamentoRitenuta(Session("DC_GENERALE"), MovRitenuta.NumeroRegistrazionePagamento) Then
            ScriptManager.RegisterClientScriptBlock(Me, Me.GetType(), "Errore", "VisualizzaErrore('Non posso modificare registrazione,<BR/> ritenute pagate');", True)
            Exit Sub
        End If


        UpDateTable()


        Tabella = ViewState("RighePrimaNota")
        Dim i As Integer
        Dim TotDare As Double = 0
        Dim TotAvere As Double = 0


        For i = 0 To Tabella.Rows.Count - 1
            Dim x1 As New Cls_ControllaSottoconto


            If IsDBNull(Tabella.Rows(i).Item(1)) Then
                Tabella.Rows(i).Item(1) = 0
            End If
            If IsDBNull(Tabella.Rows(i).Item(2)) Then
                Tabella.Rows(i).Item(2) = 0
            End If

            If CDbl(Tabella.Rows(i).Item(1)) > 0 And CDbl(Tabella.Rows(i).Item(2)) > 0 Then
                ScriptManager.RegisterClientScriptBlock(Me, Me.GetType(), "Errore", "VisualizzaErrore('<center>Possibile inserire il valore solo in dare o in avere in riga " & i + 1 & "</center>');", True)
                EseguiJS()
                Exit Sub
            End If

            If Not x1.ControllaSottoconto(Tabella.Rows(i).Item(0)) Then
                ScriptManager.RegisterClientScriptBlock(Me, Me.GetType(), "Errorecr1", "VisualizzaErrore('Conto partita errato in riga " & i + 1 & "');", True)
                EseguiJS()
                Exit Sub
            End If

            Dim MyVettore(100) As String

            MyVettore = SplitWords(Tabella.Rows(i).Item(0))
            Try
                If Val(MyVettore(2)) = 0 Then
                    ScriptManager.RegisterClientScriptBlock(Me, Me.GetType(), "Errorecr1", "VisualizzaErrore('Conto partita errato in riga " & i + 1 & "');", True)
                    EseguiJS()
                    Exit Sub
                End If
            Catch ex As Exception
                ScriptManager.RegisterClientScriptBlock(Me, Me.GetType(), "Errorecr1", "VisualizzaErrore('Conto partita errato in riga " & i + 1 & "');", True)
                EseguiJS()
                Exit Sub
            End Try

            Dim k1 As New Cls_Pianodeiconti

            k1.Mastro = Val(MyVettore(0))
            k1.Conto = Val(MyVettore(1))
            k1.Sottoconto = Val(MyVettore(2))
            k1.Descrizione = ""
            k1.Decodfica(Session("DC_GENERALE"))
            If k1.Descrizione = "" Then
                ScriptManager.RegisterClientScriptBlock(Me, Me.GetType(), "Errore", "VisualizzaErrore('<center>Specificare conto prelievo versamento</center>');", True)
                Call EseguiJS()
                Exit Sub
            End If


            If Tabella.Rows(i).Item(4).ToString.Trim <> "" Then
                If Not x1.ControllaSottoconto(Tabella.Rows(i).Item(4)) Then
                    ScriptManager.RegisterClientScriptBlock(Me, Me.GetType(), "Errorecr1", "VisualizzaErrore('Conto contro partita errato in riga " & i + 1 & "');", True)
                    EseguiJS()
                    Exit Sub
                End If
            End If


        Next

        For i = 0 To Tabella.Rows.Count - 1
            If Not IsDBNull(Tabella.Rows(i).Item(1)) Then
                TotDare = TotDare + CDbl(Tabella.Rows(i).Item(1))
            End If
            If Not IsDBNull(Tabella.Rows(i).Item(2)) Then
                TotAvere = TotAvere + CDbl(Tabella.Rows(i).Item(2))
            End If
        Next


        If Not IsDate(Txt_DataRegistrazione.Text) Then
            ScriptManager.RegisterClientScriptBlock(Me, Me.GetType(), "Errore", "VisualizzaErrore('<center>Specificare data di registrazione</center>');", True)
            Exit Sub
        End If

        If Dd_CausaleContabile.SelectedValue = "" Then
            ScriptManager.RegisterClientScriptBlock(Me, Me.GetType(), "Errore", "VisualizzaErrore('<center>Specificare causale contabile</center>');", True)
            Exit Sub
        End If

        If Val(Txt_Numero.Text) > 0 Then
            Dim VerificaBollato As New Cls_MovimentoContabile
            VerificaBollato.NumeroRegistrazione = Val(Txt_Numero.Text)
            If VerificaBollato.Bollato(Session("DC_GENERALE")) = True Then
                ScriptManager.RegisterClientScriptBlock(Me, Me.GetType(), "Errore", "VisualizzaErrore('<center>Già creato il giornale bollato</center>');", True)
                Call EseguiJS()
                Exit Sub
            End If
        End If

        If Session("TIPOAPP") <> "RSA" Then
            Dim cn As OleDbConnection


            cn = New Data.OleDb.OleDbConnection(Session("DC_GENERALE"))

            cn.Open()
            Dim cmd As New OleDbCommand() '
            cmd.CommandText = ("Select * from MovimentiContabiliTESTA where NumeroRegistrazione = ?")
            cmd.Parameters.AddWithValue("@NumeroRegistrazione", Txt_Numero.Text)
            cmd.Connection = cn
            Dim myPOSTreader As OleDbDataReader = cmd.ExecuteReader()
            If myPOSTreader.Read Then
                If campodbn(myPOSTreader.Item("IDODV")) > 0 Or campodbn(myPOSTreader.Item("IdProgettoODV")) > 0 Or campodbn(myPOSTreader.Item("TipoODV")) > 0 Then
                    ScriptManager.RegisterClientScriptBlock(Me, Me.GetType(), "Errore", "VisualizzaErrore('<center>Registrazione creata automaticamente</center>');", True)
                    Call EseguiJS()
                    Exit Sub
                End If
            End If
            myPOSTreader.Close()
            cn.Close()
        End If


        Dim XDatiGenerali As New Cls_DatiGenerali
        Dim AppoggioData As Date

        AppoggioData = Txt_DataRegistrazione.Text
        XDatiGenerali.LeggiDati(Session("DC_TABELLE"))

        If Format(XDatiGenerali.DataChiusura, "yyyyMMdd") >= Format(AppoggioData, "yyyyMMdd") Then
            ScriptManager.RegisterClientScriptBlock(Me, Me.GetType(), "Errore", "VisualizzaErrore('<center>Non posso inserire o modificare registrazione,<BR/> già effettuato la chiusura</center>');", True)
            Call EseguiJS()
            Exit Sub
        End If


        If Math.Round(TotDare, 2) <> Math.Round(TotAvere, 2) Then

            ScriptManager.RegisterClientScriptBlock(Me, Me.GetType(), "Errore", "VisualizzaErrore('<center>Dare avere diverso non posso procedere</center>');", True)
            REM Lbl_Errore.Text = "Dare avere diverso non posso procedere"
            Call EseguiJS()
            Exit Sub
        End If

        Dim Inserimento As Boolean = False

        If Val(Txt_Numero.Text) = 0 Then
            Inserimento = True
        End If



        Dim x As New Cls_MovimentoContabile
        Dim ConnectionStringGenerale As String = Session("DC_GENERALE")
        x.Leggi(ConnectionStringGenerale, Val(Txt_Numero.Text))

        x.NumeroRegistrazione = Val(Txt_Numero.Text)
        x.DataRegistrazione = Txt_DataRegistrazione.Text
        x.CausaleContabile = Dd_CausaleContabile.SelectedValue
        x.Descrizione = Txt_Descrizione.Text

        x.CentroServizio = DD_CSERV.SelectedValue


        x.MeseCompetenza = DD_MeseRif.SelectedValue
        x.AnnoCompetenza = Val(Txt_AnnoRif.Text)

        x.CodiceCig = DD_Cig.SelectedValue
        x.RegistrazioneRiferimento = Val(Txt_RegistrazioneRiferimento.Text)



        If IsDate(Txt_ExportData.Text) Then
            x.ExportDATA = Txt_ExportData.Text
        Else
            x.ExportDATA = Nothing
        End If

        If IsDate(Txt_DataDistinta.Text) Then
            x.DataDistinta = Txt_DataDistinta.Text
        Else
            x.DataDistinta = Nothing
        End If

        If IsDate(Txt_DataMandato.Text) Then
            x.DataMandato = Txt_DataMandato.Text
        Else
            x.DataMandato = Nothing
        End If

        If IsDate(Txt_DataReversale.Text) Then
            x.DataReversale = Txt_DataReversale.Text
        Else
            x.DataReversale = Nothing
        End If

        x.NumeroMandato = Val(Txt_NumeroMandato.Text)
        x.NumeroReversale = Val(Txt_NumeroReversale.Text)
        x.NumeroDistinta = Val(Txt_NumeroDistinta.Text)



        For i = 0 To Tabella.Rows.Count - 1
            Dim VerSot As New Cls_ControllaSottoconto

            If VerSot.ControllaSottoconto(campodb(Tabella.Rows(i).Item(0))) Then

                If IsNothing(x.Righe(i)) Then
                    x.Righe(i) = New Cls_MovimentiContabiliRiga
                End If
                Dim Mastro As Long
                Dim Conto As Long
                Dim Sottoconto As Long
                Dim Vettore(100) As String
                Vettore = SplitWords(Tabella.Rows(i).Item(0))

                Mastro = Val(Vettore(0))
                Conto = Val(Vettore(1))
                Sottoconto = Val(Vettore(2))


                x.Righe(i).MastroPartita = Mastro
                x.Righe(i).ContoPartita = Conto
                x.Righe(i).SottocontoPartita = Sottoconto
                If CDbl(Tabella.Rows(i).Item(1)) <> 0 Then
                    x.Righe(i).DareAvere = "D"
                    x.Righe(i).Segno = "+"
                    x.Righe(i).Importo = Tabella.Rows(i).Item(1)
                Else
                    x.Righe(i).DareAvere = "A"
                    x.Righe(i).Segno = "+"
                    x.Righe(i).Importo = Tabella.Rows(i).Item(2)
                End If
                x.Righe(i).Descrizione = campodb(Tabella.Rows(i).Item(3))

                Mastro = 0
                Conto = 0
                Sottoconto = 0
                If VerSot.ControllaSottoconto(campodb(Tabella.Rows(i).Item(4))) Then
                    Vettore = SplitWords(campodb(Tabella.Rows(i).Item(4)))

                    Mastro = Val(Vettore(0))
                    Conto = Val(Vettore(1))
                    Sottoconto = Val(Vettore(2))
                End If

                x.Righe(i).MastroContropartita = Mastro
                x.Righe(i).ContoContropartita = Conto
                x.Righe(i).SottocontoContropartita = Sottoconto

                x.Righe(i).RigaRegistrazione = Val(campodb(Tabella.Rows(i).Item(5)))
            End If
        Next i
        For i = Tabella.Rows.Count To x.Righe.Length - 1
            x.Righe(i) = Nothing
        Next

        x.Utente = Session("UTENTE")

        Dim VerificaValidita As String = x.VerificaValidita(Session("DC_GENERALE"), Session("DC_TABELLE"))

        If VerificaValidita <> "OK" Then
            ScriptManager.RegisterClientScriptBlock(Me, Me.GetType(), "Errore", "VisualizzaErrore('<center>" & VerificaValidita & "</center>');", True)
            Exit Sub
        End If


        If x.Scrivi(ConnectionStringGenerale, Val(Txt_Numero.Text)) = False Then
            ScriptManager.RegisterClientScriptBlock(Me, Me.GetType(), "Errore", "VisualizzaErrore('<center>Erore in creazione registrazione</center>');", True)
            Call EseguiJS()
            Exit Sub
        End If

        Txt_Numero.Text = x.NumeroRegistrazione

        Session("NumeroRegistrazione") = x.NumeroRegistrazione
        Txt_Numero.Enabled = False
        Dim MyJs As String

        x.Leggi(ConnectionStringGenerale, x.NumeroRegistrazione)
        'MyJs = "alert('Salvato Registrazione " & x.NumeroRegistrazione & " del " & Txt_DataRegistrazione.Text & "');  "
        'ScriptManager.RegisterStartupScript(Me, Me.GetType(), "ModReg", MyJs, True)

        Dim BottoneChiudi As String = "<br/><a href=""#"" onclick=""ChiudiConfermaModifica();""  style=""position: inherit;bottom: 20px;left: 30%;width: 40%;""><input type=""button"" class=""SeniorButton"" value=""CHIUDI"" style=""width: 100%;""></a>"
        MyJs = "<div id=""ConfermaModifica"" class=""confermamodifca""  style=""position:absolute; top:20%; left:40%; width:20%;  height:20%;""><br />Salvato Registrazione " & x.NumeroRegistrazione & "<br/>del " & Txt_DataRegistrazione.Text & BottoneChiudi & "</div>"
        LblBox.Text = MyJs
        Dim CausaleContabile As New Cls_CausaleContabile

        CausaleContabile.Leggi(Session("DC_TABELLE"), Dd_CausaleContabile.SelectedValue)


        If CausaleContabile.AnaliticaObbligatoria = 1 And Inserimento = True Then
            MyJs = "$(document).ready( function() { setTimeout(function(){ DialogBoxAnalitica('LegameRegistrazioneBudget.aspx?Numero=" & Txt_Numero.Text & "'); }, 1000);}); "
            ScriptManager.RegisterStartupScript(Me, Me.GetType(), "BudgetDialogBox", MyJs, True)
        End If


        Call PulisciRegistrazione()
        Call EseguiJS()
    End Sub




    Protected Sub Btn_Pulisci_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles Btn_Pulisci.Click
        Call PulisciRegistrazione()
        Call EseguiJS()
    End Sub




    Protected Sub Btn_Elimina_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles Btn_Elimina.Click
        Dim xs As New Cls_Legami
        Lbl_Errore.Text = ""
        If xs.TotaleLegame(Session("DC_GENERALE"), Txt_Numero.Text) > 0 Then
            ScriptManager.RegisterClientScriptBlock(Me, Me.GetType(), "Errore", "VisualizzaErrore('Vi sono legami non posso cancellare la registrazione');", True)
            REM Lbl_Errore.Text = "Vi sono legami non posso cancellare la registrazione"
            Exit Sub
        End If
        If Val(Txt_Numero.Text) > 0 Then
            Dim VerificaBollato As New Cls_MovimentoContabile
            VerificaBollato.NumeroRegistrazione = Val(Txt_Numero.Text)
            If VerificaBollato.Bollato(Session("DC_GENERALE")) = True Then
                ScriptManager.RegisterClientScriptBlock(Me, Me.GetType(), "Errore", "VisualizzaErrore('Già creato il giornale bollato');", True)
                Exit Sub
            End If
        End If

        If ViewState("NonModificabile") = 1 Then
            ScriptManager.RegisterClientScriptBlock(Me, Me.GetType(), "Errore", "VisualizzaErrore('<center>Non è possibile eliminare questa registrazione</center>');", True)
            EseguiJS()
            Exit Sub
        End If

        If Val(Txt_Numero.Text) = 0 Then
            ClientScript.RegisterClientScriptBlock(Me.GetType(), "Errore", "VisualizzaErrore('Specificare registrazione');", True)
        End If
        Dim MovRitenuta As New Cls_RitenuteAcconto

        MovRitenuta.NumeroRegistrazionePagamento = Val(Txt_Numero.Text)

        If MovRitenuta.RitenutePagateSuPagamento(Session("DC_GENERALE"), MovRitenuta.NumeroRegistrazionePagamento) Then
            ScriptManager.RegisterClientScriptBlock(Me, Me.GetType(), "Errore", "VisualizzaErrore('Non posso modificare registrazione,<BR/> ritenute pagate');", True)
            Exit Sub
        End If


        If MovRitenuta.RitenutePagateSuPagamentoRitenuta(Session("DC_GENERALE"), MovRitenuta.NumeroRegistrazionePagamento) Then
            ScriptManager.RegisterClientScriptBlock(Me, Me.GetType(), "Errore", "VisualizzaErrore('Non posso modificare registrazione,<BR/> ritenute pagate');", True)
            Exit Sub
        End If


        If Session("TIPOAPP") <> "RSA" Then
            Dim cn As OleDbConnection


            cn = New Data.OleDb.OleDbConnection(Session("DC_GENERALE"))

            cn.Open()
            Dim cmd As New OleDbCommand() '
            cmd.CommandText = ("Select * from MovimentiContabiliTESTA where NumeroRegistrazione = ?")
            cmd.Parameters.AddWithValue("@NumeroRegistrazione", Txt_Numero.Text)
            cmd.Connection = cn
            Dim myPOSTreader As OleDbDataReader = cmd.ExecuteReader()
            If myPOSTreader.Read Then
                If campodbn(myPOSTreader.Item("IDODV")) > 0 Or campodbn(myPOSTreader.Item("IdProgettoODV")) > 0 Then
                    ScriptManager.RegisterClientScriptBlock(Me, Me.GetType(), "Errore", "VisualizzaErrore('<center>Registrazione creata automaticamente</center>');", True)
                    Exit Sub
                End If
            End If
            myPOSTreader.Close()
            cn.Close()
        End If


        Dim XDatiGenerali As New Cls_DatiGenerali
        Dim AppoggioData As Date

        If Not IsDate(Txt_DataRegistrazione.Text) Then
            ClientScript.RegisterClientScriptBlock(Me.GetType(), "Errore", "VisualizzaErrore('Non posso eliminare registrazione');", True)
            Exit Sub
        End If
        If Txt_DataRegistrazione.Text = "" Then
            ClientScript.RegisterClientScriptBlock(Me.GetType(), "Errore", "VisualizzaErrore('Non posso eliminare registrazione');", True)
            Exit Sub
        End If
        AppoggioData = Txt_DataRegistrazione.Text

        XDatiGenerali.LeggiDati(Session("DC_TABELLE"))

        If Format(XDatiGenerali.DataChiusura, "yyyyMMdd") >= Format(AppoggioData, "yyyyMMdd") Then
            ClientScript.RegisterClientScriptBlock(Me.GetType(), "Errore", "VisualizzaErrore('Non posso eliminare registrazione,<BR/> già effettuato la chiusura');", True)
            Exit Sub
        End If

        Dim XScd As New Cls_Scadenziario
        Dim TbEScd As New System.Data.DataTable("TbEScd")

        XScd.NumeroRegistrazioneContabile = Txt_Numero.Text
        XScd.loaddati(Session("DC_GENERALE"), TbEScd)

        If TbEScd.Rows.Count > 1 Then
            ScriptManager.RegisterClientScriptBlock(Me, Me.GetType(), "Errore", "VisualizzaErrore('Vi sono scadenze legate alla registrazione non posso cancellare');", True)
            REM Lbl_Errore.Text = "Vi sono scadenze legate alla registrazione non posso cancellare"
            Exit Sub
        End If

        If TbEScd.Rows.Count = 1 Then
            If Not IsDBNull(TbEScd.Rows(0).Item(1)) Then
                ScriptManager.RegisterClientScriptBlock(Me, Me.GetType(), "Errore", "VisualizzaErrore('Vi sono scadenze legate alla registrazione non posso cancellare');", True)
                REM Lbl_Errore.Text = "Vi sono scadenze legate alla registrazione non posso cancellare"
                Exit Sub
            End If
        End If

        Dim XReg As New Cls_MovimentoContabile

        XReg.NumeroRegistrazione = Txt_Numero.Text

        XReg.Leggi(Session("DC_GENERALE"), Txt_Numero.Text)

        Dim Verifica As String = XReg.VerificaElimina(Session("DC_GENERALE"), Session("DC_TABELLE"))
        If Verifica <> "OK" Then
            ScriptManager.RegisterClientScriptBlock(Me, Me.GetType(), "Errore", "VisualizzaErrore('" & Verifica.Replace("'", "") & "');", True)
            Exit Sub
        End If

        Dim Log As New Cls_LogPrivacy

        Dim serializer As JavaScriptSerializer
        serializer = New JavaScriptSerializer()
        Log.LogPrivacy(Application("SENIOR"), Session("CLIENTE"), Session("UTENTE"), Session("UTENTEIMPER"), Page.GetType.Name.ToUpper, 0, 0, "", "", Val(Txt_Numero.Text), "", "D", "PRIMANOTA", serializer.Serialize(XReg))




        XReg.EliminaRegistrazione(Session("DC_GENERALE"))

        Call PulisciRegistrazione()
    End Sub


    Protected Sub Dd_CausaleContabile_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles Dd_CausaleContabile.SelectedIndexChanged
        If Txt_Numero.Text = "0" Then
            Dim Xt As New Cls_CausaleContabile
            Dim I As Long
            Dim Entra As Boolean = False

            Tabella.Clear()
            Tabella.Columns.Clear()
            Tabella.Columns.Add("Sottoconto", GetType(String))
            Tabella.Columns.Add("Dare", GetType(String))
            Tabella.Columns.Add("Avere", GetType(String))
            Tabella.Columns.Add("Descrizione", GetType(String))
            Tabella.Columns.Add("SottocontoControPartita", GetType(String))
            Tabella.Columns.Add("RigaRegistrazione", GetType(String))

            Xt.Leggi(Session("DC_TABELLE"), Dd_CausaleContabile.SelectedValue)

            BtnOpen.Visible = False
            BtnOpen2.Visible = False
            If Xt.Tipo = "P" Then
                BtnOpen.ImageUrl = "../images/Menu_GestioneIncassiPagamenti.png"
                BtnOpen.Visible = True
                BtnOpen.ToolTip = "Incassi e Pagamenti"

                BtnOpen2.ImageUrl = "../images/Menu_IncassoPagamentoScadenziario.png"
                BtnOpen2.Visible = True

                BtnOpen2.ToolTip = "Incassi e Pagamenti Con Scadenziario"
            End If
            If Xt.Tipo = "R" Or Xt.Tipo = "I" Then
                BtnOpen.ImageUrl = "../images/Bottone_Documenti.png"
                BtnOpen.Visible = True
                BtnOpen.ToolTip = "Gestione Documenti"
            End If

            For I = 0 To 100
                If Not IsNothing(Xt.Righe(I)) Then
                    If Xt.Righe(I).Sottoconto <> 0 Or Xt.Righe(I).Conto <> 0 Or Xt.Righe(I).Mastro <> 0 Then
                        Dim myriga As System.Data.DataRow = Tabella.NewRow()
                        Dim xPc As New Cls_Pianodeiconti
                        xPc.Mastro = Xt.Righe(I).Mastro
                        xPc.Conto = Xt.Righe(I).Conto
                        xPc.Sottoconto = Xt.Righe(I).Sottoconto
                        xPc.Decodfica(Session("DC_GENERALE"))
                        myriga(0) = xPc.Mastro & " " & xPc.Conto & " " & xPc.Sottoconto & " " & xPc.Descrizione
                        myriga(1) = 0
                        myriga(2) = 0
                        myriga("RigaRegistrazione") = I
                        Tabella.Rows.Add(myriga)
                        Entra = True
                    End If
                End If
            Next
            If Entra = False Then
                Dim myriga As System.Data.DataRow = Tabella.NewRow()
                myriga(1) = 0
                myriga(2) = 0
                Tabella.Rows.Add(myriga)
            End If

            ViewState("RighePrimaNota") = Tabella
            Grid.AutoGenerateColumns = False
            Grid.DataSource = Tabella
            Grid.DataBind()
            For I = 0 To Grid.Rows.Count - 1
                Grid.Rows(I).Cells(2).BackColor = Drawing.Color.White
                Grid.Rows(I).Cells(3).BackColor = Drawing.Color.White
                If Not IsNothing(Xt.Righe(I)) Then
                    If Xt.Righe(I).Sottoconto <> 0 Or Xt.Righe(I).Conto <> 0 Or Xt.Righe(I).Mastro <> 0 Then
                        Dim NumeroRiga As Integer = I
                        If campodbn(Tabella.Rows(I).Item("RigaRegistrazione")) > 0 Then
                            NumeroRiga = campodbn(Tabella.Rows(I).Item("RigaRegistrazione"))
                        End If
                        If Xt.Righe(NumeroRiga).DareAvere = "A" Then
                            Grid.Rows(I).Cells(3).BackColor = Drawing.Color.Yellow
                        End If
                        If Xt.Righe(NumeroRiga).DareAvere = "D" Then
                            Grid.Rows(I).Cells(2).BackColor = Drawing.Color.Yellow
                        End If
                    End If
                End If
            Next
            If Session("TIPOAPP") <> "RSA" Then
                Grid.Columns(4).Visible = False
                Grid.Columns(5).Visible = False
            End If
        End If
        EseguiJS()
    End Sub



    Protected Sub ImgRicerca_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles ImgRicerca.Click
        'Response.Redirect("Ricerca.aspx?TIPO=PRNT")
        Dim MyJs As String

        Dim Mastro As Long
        Dim Conto As Long
        Dim Sottoconto As Long
        Dim Vettore(100) As String
        Dim Appoggio1 As String = ""


        Try
            Dim TxtSottoconto As TextBox = DirectCast(Grid.Rows(0).FindControl("TxtSottoconto"), TextBox)

            Appoggio1 = TxtSottoconto.Text
        Catch ex As Exception
            Appoggio1 = ""
        End Try


        Vettore = SplitWords(Appoggio1)

        If Vettore.Length > 2 Then
            Mastro = Val(Vettore(0))
            Conto = Val(Vettore(1))
            Sottoconto = Val(Vettore(2))
        End If

        If Mastro > 0 Then
            MyJs = "$(document).ready(function() {  setTimeout(function(){ DialogBoxBig('Ricerca.aspx?TIPO=PRNT&MASTRO=" & Mastro & "&CONTO=" & Conto & "&SOTTOCONTO=" & Sottoconto & "');}, 500);});"
        Else
            MyJs = "$(document).ready(function() {  setTimeout(function(){   DialogBoxBig('Ricerca.aspx?TIPO=PRNT');}, 500);});"
        End If


        ScriptManager.RegisterClientScriptBlock(Me, Me.GetType(), "RicercaDOC", MyJs, True)



    End Sub

    Protected Sub Btn_Esci_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles Btn_Esci.Click

        If Request.Item("PROVENIENTE") = "REVMAN" Then
            If Request.Item("PAGINA") = "" Then
                Response.Redirect("UltimiMovimenti.aspx?TIPO=MANREV")
            Else
                Response.Redirect("UltimiMovimenti.aspx?CHIAMATA=RITORNO&PAGINA=" & Request.Item("PAGINA") & "&TIPO=MANREV")
            End If
            Exit Sub
        End If
        If Request.Item("PROVENIENTE") = "SEMPLIFICATA" Then
            If Request.Item("PAGINA") = "" Then
                Response.Redirect("UltimiMovimenti.aspx?TIPO=SMPPRN")
            Else
                Response.Redirect("UltimiMovimenti.aspx?CHIAMATA=RITORNO&PAGINA=" & Request.Item("PAGINA") & "&TIPO=SMPPRN")
            End If
            Exit Sub
        End If

        If Request.Item("PAGINA") = "" Then
            Response.Redirect("UltimiMovimenti.aspx?TIPO=PRNT")
        Else
            Response.Redirect("UltimiMovimenti.aspx?CHIAMATA=RITORNO&PAGINA=" & Request.Item("PAGINA") & "&TIPO=PRNT")
        End If

    End Sub

    Protected Sub Btn_Duplica_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles Btn_Duplica.Click
        If Val(Txt_Numero.Text) > 0 Then

            Txt_Numero.Text = 0
            Txt_Numero.Enabled = True
            Call EseguiJS()
        End If
    End Sub

    Protected Sub Txt_Dare_TextChanged(ByVal sender As Object, ByVal e As System.EventArgs)
        UpDateTable()
        Tabella = ViewState("RighePrimaNota")
        Dim RigheAZero As Integer = 0
        Dim LabelTotDare As Double = 0
        Dim LabelTotAvere As Double = 0

        Try
            For i = 0 To Tabella.Rows.Count - 1
                If CDbl(Tabella.Rows(i).Item("Dare")) > 0 Then
                    LabelTotDare = LabelTotDare + CDbl(Tabella.Rows(i).Item("Dare"))
                End If
                If CDbl(Tabella.Rows(i).Item("Avere")) > 0 Then
                    LabelTotAvere = LabelTotAvere + CDbl(Tabella.Rows(i).Item("Avere"))
                End If
                If CDbl(Tabella.Rows(i).Item("Dare")) = 0 And CDbl(Tabella.Rows(i).Item("Avere")) = 0 Then
                    RigheAZero = RigheAZero + 1
                End If
            Next
        Catch ex As Exception

        End Try

        Grid.FooterRow.Cells(2).HorizontalAlign = HorizontalAlign.Right
        Grid.FooterRow.Cells(2).Text = Format(LabelTotDare, "#,##0.00")
        Grid.FooterRow.Cells(3).Text = Format(LabelTotAvere, "#,##0.00")
        Grid.FooterRow.Cells(3).HorizontalAlign = HorizontalAlign.Right

        If RigheAZero = 1 Then

            If Tabella.Rows.Count >= 2 Then
                Dim TotDare As Double
                Dim TotAvere As Double
                Dim Dare As Double
                Dim Avere As Double



                For i = 0 To Tabella.Rows.Count - 1
                    TotDare = TotDare + Tabella.Rows(i).Item("Dare")
                    TotAvere = TotAvere + Tabella.Rows(i).Item("Avere")
                Next

                For i = 0 To Tabella.Rows.Count - 1
                    Dare = Tabella.Rows(i).Item("Dare")
                    Avere = Tabella.Rows(i).Item("Avere")

                    If TotAvere > TotDare And Dare = 0 And Avere = 0 Then
                        Tabella.Rows(i).Item("Dare") = Format(TotAvere - TotDare, "#,##0.00")
                        Exit For
                    End If
                    If TotAvere < TotDare And Dare = 0 And Avere = 0 Then
                        Tabella.Rows(i).Item("Avere") = Format(TotDare - TotAvere, "#,##0.00")
                        Exit For
                    End If
                Next

                ViewState("RighePrimaNota") = Tabella



                Grid.AutoGenerateColumns = False
                Grid.DataSource = Tabella
                Grid.DataBind()


                If Session("TIPOAPP") <> "RSA" Then
                    Grid.Columns(4).Visible = False
                    Grid.Columns(5).Visible = False
                End If
            End If
        End If


        Dim Xt As New Cls_CausaleContabile

        EseguiJS()

        Xt.Leggi(Session("DC_TABELLE"), Dd_CausaleContabile.SelectedValue)


        Try

            For I = 0 To Grid.Rows.Count - 1
                Grid.Rows(I).Cells(2).BackColor = Drawing.Color.White
                Grid.Rows(I).Cells(3).BackColor = Drawing.Color.White
                If Not IsNothing(Xt.Righe(I)) Then
                    If Xt.Righe(I).Sottoconto <> 0 Or Xt.Righe(I).Conto <> 0 Or Xt.Righe(I).Mastro <> 0 Then
                        Dim NumeroRiga As Integer = I
                        If campodbn(Tabella.Rows(I).Item("RigaRegistrazione")) > 0 Then
                            NumeroRiga = campodbn(Tabella.Rows(I).Item("RigaRegistrazione"))
                        End If

                        If Xt.Righe(NumeroRiga).DareAvere = "A" Then
                            Grid.Rows(I).Cells(3).BackColor = Drawing.Color.Yellow
                        End If
                        If Xt.Righe(NumeroRiga).DareAvere = "D" Then
                            Grid.Rows(I).Cells(2).BackColor = Drawing.Color.Yellow
                        End If
                    End If
                End If
            Next
        Catch ex As Exception

        End Try

        Call EseguiJS()

    End Sub

    Protected Sub Txt_Avere_TextChanged(ByVal sender As Object, ByVal e As System.EventArgs)
        UpDateTable()
        Tabella = ViewState("RighePrimaNota")
        Dim LabelTotDare As Double = 0
        Dim LabelTotAvere As Double = 0
        Dim RigheAZero As Integer = 0

        Try
            For i = 0 To Tabella.Rows.Count - 1
                If CDbl(Tabella.Rows(i).Item("Dare")) > 0 Then
                    LabelTotDare = LabelTotDare + CDbl(Tabella.Rows(i).Item("Dare"))
                End If
                If CDbl(Tabella.Rows(i).Item("Avere")) > 0 Then
                    LabelTotAvere = LabelTotAvere + CDbl(Tabella.Rows(i).Item("Avere"))
                End If

                If CDbl(Tabella.Rows(i).Item("Dare")) = 0 And CDbl(Tabella.Rows(i).Item("Avere")) = 0 Then
                    RigheAZero = RigheAZero + 1
                End If
            Next
        Catch ex As Exception

        End Try


        Grid.FooterRow.Cells(2).HorizontalAlign = HorizontalAlign.Right
        Grid.FooterRow.Cells(2).Text = Format(LabelTotDare, "#,##0.00")
        Grid.FooterRow.Cells(3).Text = Format(LabelTotAvere, "#,##0.00")
        Grid.FooterRow.Cells(3).HorizontalAlign = HorizontalAlign.Right

        If RigheAZero = 1 Then
            If Tabella.Rows.Count >= 2 Then
                Dim TotDare As Double
                Dim TotAvere As Double
                Dim Dare As Double
                Dim Avere As Double

                For i = 0 To Tabella.Rows.Count - 1
                    TotDare = TotDare + Tabella.Rows(i).Item("Dare")
                    TotAvere = TotAvere + Tabella.Rows(i).Item("Avere")
                Next



                For i = 0 To Tabella.Rows.Count - 1
                    Dare = Tabella.Rows(i).Item("Dare")
                    Avere = Tabella.Rows(i).Item("Avere")

                    If TotAvere > TotDare And Dare = 0 And Avere = 0 Then
                        Tabella.Rows(i).Item("Dare") = Format(TotAvere - TotDare, "#,##0.00")
                        Exit For
                    End If
                    If TotAvere < TotDare And Avere = 0 And Dare = 0 Then
                        Tabella.Rows(i).Item("Avere") = Format(TotDare - TotAvere, "#,##0.00")
                        Exit For
                    End If
                Next

                ViewState("RighePrimaNota") = Tabella
                Grid.AutoGenerateColumns = False
                Grid.DataSource = Tabella
                Grid.DataBind()


                If Session("TIPOAPP") <> "RSA" Then
                    Grid.Columns(4).Visible = False
                    Grid.Columns(5).Visible = False
                End If
            End If
        End If
        Dim Xt As New Cls_CausaleContabile

        Xt.Leggi(Session("DC_TABELLE"), Dd_CausaleContabile.SelectedValue)

        Try

            For I = 0 To Grid.Rows.Count - 1
                Grid.Rows(I).Cells(2).BackColor = Drawing.Color.White
                Grid.Rows(I).Cells(3).BackColor = Drawing.Color.White
                If Not IsNothing(Xt.Righe(I)) Then
                    If Xt.Righe(I).Sottoconto <> 0 Or Xt.Righe(I).Conto <> 0 Or Xt.Righe(I).Mastro <> 0 Then
                        Dim NumeroRiga As Integer = I
                        If campodbn(Tabella.Rows(I).Item("RigaRegistrazione")) > 0 Then
                            NumeroRiga = campodbn(Tabella.Rows(I).Item("RigaRegistrazione"))
                        End If

                        If Xt.Righe(NumeroRiga).DareAvere = "A" Then
                            Grid.Rows(I).Cells(3).BackColor = Drawing.Color.Yellow
                        End If
                        If Xt.Righe(NumeroRiga).DareAvere = "D" Then
                            Grid.Rows(I).Cells(2).BackColor = Drawing.Color.Yellow
                        End If
                    End If
                End If
            Next
        Catch ex As Exception

        End Try

        EseguiJS()
    End Sub

    Protected Sub Timer1_Tick(ByVal sender As Object, ByVal e As System.EventArgs) Handles Timer1.Tick
        If Trim(Session("UTENTE")) = "" Then
            If Request.ServerVariables("URL").ToUpper.IndexOf("SENIORWEBBLANCO") > 0 Then
                Response.Redirect("/ODV/Login.aspx")
                Exit Sub
            Else
                Response.Redirect("/SeniorWeb/Login.aspx")
                Exit Sub
            End If
        End If
    End Sub

    Private Sub MettiInvisibileJS()
        Dim MyJs As String
        MyJs = "$(document).ready(function() {"

        MyJs = MyJs & "var els = document.getElementsByTagName(""*"");"

        MyJs = MyJs & "for (var i=0;i<els.length;i++)"
        MyJs = MyJs & "if ( els[i].id ) { "
        MyJs = MyJs & " var appoggio =els[i].id; "

        MyJs = MyJs & " if ((appoggio.match('BOTTONEHOME')!= null) || (appoggio.match('BOTTONERICERCA')!= null) || (appoggio.match('BOTTONEADDSOCIO')!= null)) {  "
        MyJs = MyJs & " $(els[i]).hide();"
        MyJs = MyJs & "    }"


        MyJs = MyJs & "} "

        MyJs = MyJs & "});"

        'MyJs = "$(document).ready(function() { $('.myClass').keypress(function() { handleEnter($('.myClass').val(), true, true); } );     $('#" & Txt_ImportoPagato.ClientID & "').blur(function() { var ap = formatNumber ($('#" & Txt_ImportoPagato.ClientID & "').val(),2); $('#" & Txt_ImportoPagato.ClientID & "').val(ap); }); });"
        ScriptManager.RegisterStartupScript(Me, Me.GetType(), "MettiInsibileJS", MyJs, True)
    End Sub


    Private Sub MettiInvisibileSemplificataJS()
        Dim MyJs As String
        MyJs = "$(document).ready(function() {"

        MyJs = MyJs & "var els = document.getElementsByTagName(""*"");"

        MyJs = MyJs & "for (var i=0;i<els.length;i++)"
        MyJs = MyJs & "if ( els[i].id ) { "
        MyJs = MyJs & " var appoggio =els[i].id; "

        MyJs = MyJs & " if ((appoggio.match('BOTTONEHOME')!= null) || (appoggio.match('BOTTONERICERCA')!= null) || (appoggio.match('BOTTONEADDSOCIO')!= null) || (appoggio.match('BottoneCliFor')!= null) || (appoggio.match('pianoconti')!= null)) {  "
        MyJs = MyJs & " $(els[i]).hide();"
        MyJs = MyJs & "    }"


        MyJs = MyJs & "} "

        MyJs = MyJs & "});"

        'MyJs = "$(document).ready(function() { $('.myClass').keypress(function() { handleEnter($('.myClass').val(), true, true); } );     $('#" & Txt_ImportoPagato.ClientID & "').blur(function() { var ap = formatNumber ($('#" & Txt_ImportoPagato.ClientID & "').val(),2); $('#" & Txt_ImportoPagato.ClientID & "').val(ap); }); });"
        ScriptManager.RegisterStartupScript(Me, Me.GetType(), "MettiInsibileJS", MyJs, True)
    End Sub

    Protected Sub BTN_InserisciRiga_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles BTN_InserisciRiga.Click
        InserisciRiga()
    End Sub

    Protected Sub Txt_DataRegistrazione_TextChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles Txt_DataRegistrazione.TextChanged

    End Sub



    Protected Sub BtnOpen_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles BtnOpen.Click
        Dim CausaleContabile As New Cls_CausaleContabile

        CausaleContabile.Codice = Dd_CausaleContabile.SelectedValue
        CausaleContabile.Leggi(Session("DC_TABELLE"), CausaleContabile.Codice)



        Dim Data As Date = Now


        Try
            Data = Txt_DataRegistrazione.Text
        Catch ex As Exception

        End Try

        If CausaleContabile.Tipo = "P" Then
            If Val(Txt_Numero.Text) = 0 Then

                Session("NumeroRegistrazione") = 0
                Response.Redirect("incassipagamenti.aspx?Chiamante=PrimaNota&Data=" & Format(Data, "yyyyMMdd") & "&CausaleContabile=" & CausaleContabile.Codice)
            Else
                Response.Redirect("incassipagamenti.aspx?NumeroRegistrazione=" & Val(Txt_Numero.Text))
            End If
        End If
        If CausaleContabile.Tipo = "R" Or CausaleContabile.Tipo = "I" Then
            If Val(Txt_Numero.Text) = 0 Then
                Session("NumeroRegistrazione") = 0

                Response.Redirect("documenti.aspx?Chiamante=PrimaNota&Data=" & Format(Data, "yyyyMMdd") & "&CausaleContabile=" & CausaleContabile.Codice)
            Else
                Response.Redirect("documenti.aspx?NumeroRegistrazione=" & Val(Txt_Numero.Text))
            End If
        End If
    End Sub

    Protected Sub BtnOpen2_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles BtnOpen2.Click
        Dim CausaleContabile As New Cls_CausaleContabile

        CausaleContabile.Codice = Dd_CausaleContabile.SelectedValue
        CausaleContabile.Leggi(Session("DC_TABELLE"), CausaleContabile.Codice)



        Dim Data As Date = Now


        Try
            Data = Txt_DataRegistrazione.Text
        Catch ex As Exception

        End Try

        If CausaleContabile.Tipo = "P" Then
            If Val(Txt_Numero.Text) = 0 Then

                Session("NumeroRegistrazione") = 0
                Response.Redirect("incassipagamenti.aspx?TIPO=SCADENZARIO&Chiamante=PrimaNota&Data=" & Format(Data, "yyyyMMdd") & "&CausaleContabile=" & CausaleContabile.Codice)
            Else
                Response.Redirect("incassipagamenti.aspx?TIPO=SCADENZARIO&NumeroRegistrazione=" & Val(Txt_Numero.Text))
            End If

        End If
        If CausaleContabile.Tipo = "R" Or CausaleContabile.Tipo = "I" Then
            If Val(Txt_Numero.Text) = 0 Then
                Session("NumeroRegistrazione") = 0

                Response.Redirect("documenti.aspx?Chiamante=PrimaNota&Data=" & Format(Data, "yyyyMMdd") & "&CausaleContabile=" & CausaleContabile.Codice)
            Else
                Response.Redirect("documenti.aspx?NumeroRegistrazione=" & Val(Txt_Numero.Text))
            End If
        End If

    End Sub

    Private Sub TabContainer1_ActiveTabChanged(sender As Object, e As EventArgs) Handles TabContainer1.ActiveTabChanged
        Try
            Dim TxtSottoconto As TextBox = DirectCast(Grid.Rows(0).FindControl("TxtSottoconto"), TextBox)


            Dim k As New Cls_ClienteFornitore
            Dim Mastro As Long
            Dim Conto As Long
            Dim Sottoconto As Long
            Dim Vettore(100) As String
            Dim Appoggio1 As String = ""
            Dim Appoggio2 As String = ""

            Vettore = SplitWords(TxtSottoconto.Text)

            If Vettore.Length > 2 Then
                Mastro = Val(Vettore(0))
                Conto = Val(Vettore(1))
                Sottoconto = Val(Vettore(2))
                k.MastroCliente = Mastro
                k.ContoCliente = Conto
                k.SottoContoCliente = Sottoconto
                k.Leggi(Session("DC_OSPITE"))
                If k.Nome = "" Then
                    k.MastroCliente = 0
                    k.ContoCliente = 0
                    k.SottoContoCliente = 0
                    k.MastroFornitore = Mastro
                    k.ContoFornitore = Conto
                    k.SottoContoFornitore = Sottoconto


                    k.Leggi(Session("DC_OSPITE"))

                    Dim Cig As New Cls_CigClientiFornitori

                    Try
                        Cig.UpDateDropBoxAttivi(Session("DC_GENERALE"), DD_Cig, k.CODICEDEBITORECREDITORE, Txt_DataRegistrazione.Text)
                    Catch ex As Exception

                    End Try
                Else
                    Dim Cig As New Cls_CigClientiFornitori

                    Try
                        Cig.UpDateDropBoxAttivi(Session("DC_GENERALE"), DD_Cig, k.CODICEDEBITORECREDITORE, Txt_DataRegistrazione.Text)
                    Catch ex As Exception

                    End Try
                End If
            End If

        Catch ex As Exception

        End Try

    End Sub
End Class


