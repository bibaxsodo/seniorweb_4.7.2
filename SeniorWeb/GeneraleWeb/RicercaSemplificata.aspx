﻿<%@ Page Language="VB" EnableEventValidation="false" AutoEventWireup="false" Inherits="GeneraleWeb_RicercaSemplificata" CodeFile="RicercaSemplificata.aspx.vb" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="xasp" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
    <title>Ricerca</title>
    <asp:PlaceHolder runat="server">
        <%: System.Web.Optimization.Styles.Render("~/Content/AjaxControlToolkit/Styles/Bundle") %>
    </asp:PlaceHolder>
    <link href="ospiti.css?ver=11" rel="stylesheet" type="text/css" />
    <link rel="shortcut icon" href="images/SENIOR.ico" />
    <link href="js/jquery.autocomplete.css" rel="stylesheet" type="text/css" />

    <script src="js/jquery-1.5.1.min.js" type="text/javascript"></script>
    <script src="js/jquery.autocomplete.js" type="text/javascript"></script>
    <script src="js/soapclient.js" type="text/javascript"></script>
    <script src="/js/convertinumero.js" type="text/javascript"></script>

    <script src="js/jquery.maskedinput-1.3.min.js" type="text/javascript"></script>
    <script src="/js/formatnumer.js" type="text/javascript"></script>

    <script src="/js/pianoconti.js" type="text/javascript"></script>
    <script src="js/JSErrore.js" type="text/javascript"></script>
    <link rel="stylesheet" href="dialogbox/redips-dialog.css" type="text/css" media="screen" />
    <script type="text/javascript" src="dialogbox/redips-dialog-min.js"></script>
    <script type="text/javascript" src="dialogbox/script.js"></script>
    <script src="js/NoEnter.js" type="text/javascript"></script>
    <link rel="stylesheet" href="jqueryui/jquery-ui.css" type="text/css" />
    <script src="jqueryui/jquery-ui.js" type="text/javascript"></script>
    <script src="jqueryui/datapicker-it.js" type="text/javascript"></script>
    <style>
        #blur {
            width: 100%;
            background-color: black;
            moz-opacity: 0.5;
            khtml-opacity: .5;
            opacity: .5;
            filter: alpha(opacity=50);
            z-index: 120;
            height: 100%;
            position: absolute;
            top: 0;
            left: 0;
        }

        #progress {
            z-index: 200;
            background-color: White;
            position: absolute;
            top: 0pt;
            left: 0pt;
            border: solid 1px black;
            padding: 5px 5px 5px 5px;
            text-align: center;
        }
    </style>
    <script type="text/javascript"> 
        function soloNumeri(evt) {
            var charCode = (evt.which) ? evt.which : event.keyCode
            if (charCode > 31 && (charCode < 48 || charCode > 57)) {
                return false;
            }
            return true;
        }



        function DialogBox(Path) {

            var winW = 630, winH = 460;
            if (document.body && document.body.offsetWidth) {
                winW = document.body.offsetWidth;
                winH = document.body.offsetHeight;
            }
            if (document.compatMode == 'CSS1Compat' &&
                document.documentElement &&
                document.documentElement.offsetWidth) {
                winW = document.documentElement.offsetWidth;
                winH = document.documentElement.offsetHeight;
            }
            if (window.innerWidth && window.innerHeight) {
                winW = window.innerWidth;
                winH = window.innerHeight;
            }

            var APPO = winH - 100;

            REDIPS.dialog.show(winW - 200, winH - 100, '<iframe id="output" src="' + Path + '" height="' + APPO + '" width="100%" ></iframe>');
            return false;

        }

        $(document).ready(function () {
            if (window.innerHeight > 0) { $("#BarraLaterale").css("height", (window.innerHeight - 94) + "px"); } else { $("#BarraLaterale").css("height", (document.documentElement.offsetHeight - 94) + "px"); }
        });
    </script>
</head>
<body>
    <form id="form1" runat="server" autocomplete="off">
        <asp:ScriptManager ID="ScriptManager1" runat="server" AsyncPostBackTimeout="900" EnableScriptGlobalization="true">
            <Scripts>
                <asp:ScriptReference Path="~/Scripts/AjaxControlToolkit/Bundle" />
            </Scripts>
        </asp:ScriptManager>
        <asp:Label ID="Lbl_BarraSenior" runat="server" Text=""></asp:Label>
        <div align="left">
            <table style="width: 100%;" cellpadding="0" cellspacing="0">
                <tr>
                    <td style="width: 160px; background-color: #F0F0F0;"></td>
                    <td>
                        <div class="Titolo">Contabilità Semplificata – Ricerca movimenti</div>
                        <div class="SottoTitolo">
                            <br />
                            <br />
                        </div>
                    </td>
                    <td style="text-align: right; vertical-align: top;">
                        <span class="BenvenutoText">Benvenuto 
       <asp:Label ID="Lbl_Utente" runat="server" Text="Label"></asp:Label>&nbsp;&nbsp;&nbsp;&nbsp;</span>

                        <asp:ImageButton ID="ImageButton1" runat="server" Height="38px" ImageUrl="~/images/esegui.png" class="EffettoBottoniTondi" Width="38px" />
                        <asp:ImageButton ID="ImageButton2" runat="server" Height="38px" ImageUrl="~/images/Excel.png" class="EffettoBottoniTondi" Style="width: 38px" />
                        <asp:ImageButton ID="Btn_Esci" runat="server" BackColor="Transparent" Height="38px" ImageUrl="images/esci.jpg" ToolTip="Chiudi" />
                    </td>
                </tr>
                <tr>
                    <td style="width: 160px; background-color: #F0F0F0; vertical-align: top; text-align: center;" id="BarraLaterale">
                        <a href="Menu_Semplificata.aspx" style="border-width: 0px;">
                            <img src="images/Home.jpg" alt="Menù" class="Effetto" /></a><br />
                    </td>
                    <td colspan="2" style="background-color: #FFFFFF; vertical-align: top;">

                        <label class="LabelCampo">Descrizione:</label>
                        <asp:TextBox ID="Txt_Descrizione" onkeypress="return handleEnter(this, event)"
                            runat="server" Width="398px"></asp:TextBox>
                        <br />
                        <label class="LabelCampo">Tipo Movimento :</label>
                        <asp:RadioButton ID="RB_Entrate" runat="server" Text="Entrate" Checked="False" GroupName="tipomov" />
                        <asp:RadioButton ID="RB_Uscite" runat="server" Text="Uscita" Checked="False" GroupName="tipomov" />
                        <asp:RadioButton ID="RB_Giroconto" runat="server" Text="Giroconto" Checked="False" GroupName="tipomov" />
                        <asp:RadioButton ID="RB_Tutte" runat="server" Text="Tutte" Checked="True" GroupName="tipomov" /><br />
                        <br />


                        <label class="LabelCampo">Conto Contabilità:</label>
                        <asp:TextBox ID="Txt_Sottoconto" onkeypress="return handleEnter(this, event)"
                            CssClass="MyAutoComplete" runat="server" Width="402px"></asp:TextBox><br />
                        <table style="border-width: 0px; margin-left: 0px; border-collapse: collapse;">
                            <tr>
                                <td>
                                    <label class="LabelCampo">Data Dal: </label>
                                    <asp:TextBox ID="Txt_DataDal" onkeypress="return handleEnter(this, event)" runat="server" Width="90px"></asp:TextBox>
                                </td>
                                <td>
                                    <label style="display: block; float: left; width: 110px; text-align: right;">Data Al:</label>
                                    <asp:TextBox ID="Txt_DataAl" onkeypress="return handleEnter(this, event)" runat="server" Width="90px"></asp:TextBox>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <label class="LabelCampo">Importo Dal: </label>
                                    <asp:TextBox ID="Txt_ImportoDal" onkeypress="return handleEnter(this, event)" runat="server" Width="120px"></asp:TextBox>
                                </td>
                                <td>
                                    <label class="LabelCampo">Importo Al: </label>
                                    <asp:TextBox ID="Txt_ImportoAl" onkeypress="return handleEnter(this, event)" runat="server" Width="120px"></asp:TextBox>
                                </td>
                            </tr>
                        </table>


                        <br />
                        <asp:UpdatePanel ID="UpdatePanel1" runat="server">
                            <ContentTemplate>
                                <asp:Label ID="Lbl_Errori" runat="server" ForeColor="Red" Width="384px"></asp:Label>
                                &nbsp;<asp:GridView ID="Grid" runat="server" CellPadding="3"
                                    Width="100%" BackColor="White" BorderColor="#999999" BorderStyle="None"
                                    BorderWidth="1px" GridLines="Vertical">
                                    <RowStyle ForeColor="#565151" BackColor="#EEEEEE" />
                                    <Columns>
                                        <asp:TemplateField HeaderText="">
                                            <ItemTemplate>
                                                <asp:ImageButton ID="Richiama" CommandName="Richiama" runat="Server"
                                                    ImageUrl="~/images/select.png" class="EffettoBottoniTondi"
                                                    CommandArgument='<%#  Eval("NumeroRegistrazione") %>' />
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                    </Columns>
                                    <FooterStyle BackColor="#CCCCCC" ForeColor="Black" />
                                    <PagerStyle BackColor="#999999" ForeColor="Black" HorizontalAlign="Center" />
                                    <SelectedRowStyle BackColor="#008A8C" Font-Bold="True" ForeColor="White" />
                                    <HeaderStyle BackColor="#565151" Font-Bold="True" ForeColor="White" />
                                    <AlternatingRowStyle BackColor="#DCDCDC" />
                                </asp:GridView>
                                <asp:GridView ID="GridView1" runat="server" Height="70px" Width="760px">
                                </asp:GridView>
                            </ContentTemplate>
                            <Triggers>
                                <asp:PostBackTrigger ControlID="Grid" />
                                <asp:PostBackTrigger ControlID="GridView1" />
                                <asp:PostBackTrigger ControlID="ImageButton2" />
                            </Triggers>
                        </asp:UpdatePanel>

                        <br />
                        <asp:UpdateProgress ID="UpdateProgress2" runat="server"
                            AssociatedUpdatePanelID="UpdatePanel1">
                            <ProgressTemplate>
                                <div id="blur">&nbsp;</div>

                                <div id="progress" style="width: 200px; height: 50px; left: 40%; position: absolute; top: 372px; text-align: center;">
                                    Attendere prego.....<br />
                                    &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<img height="30px" src="images/loading.gif">
                                </div>
                            </ProgressTemplate>
                        </asp:UpdateProgress>
                    </td>
                </tr>
            </table>
        </div>
    </form>
</body>
</html>
