﻿<%@ Page Language="VB" AutoEventWireup="false" Inherits="GeneraleWeb_FatturaElettronicaViewer" CodeFile="FatturaElettronicaViewer.aspx.vb" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
    <asp:PlaceHolder runat="server">
        <%: System.Web.Optimization.Styles.Render("~/Content/AjaxControlToolkit/Styles/Bundle") %>
    </asp:PlaceHolder>
    <link href="ospiti.css?ver=11" rel="stylesheet" type="text/css" />
    <link rel="shortcut icon" href="images/SENIOR.ico" />
</head>
<script>
    function Stampa() {
        document.getElementById("PrintLogo").style.display = "none";
        window.print();
        document.getElementById("PrintLogo").style.display = "block";
    }
</script>
<body>
    <form id="form1" runat="server">
        <asp:ScriptManager ID="ScriptManager1" runat="server" EnableScriptGlobalization="true">
            <Scripts>
                <asp:ScriptReference Path="~/Scripts/AjaxControlToolkit/Bundle" />
            </Scripts>
        </asp:ScriptManager>
        <div>
            <label class="LabelCampo">Tipo Visualizzazione:</label>
            <asp:DropDownList ID="DD_Tipo" runat="server" AutoPostBack="true">
                <asp:ListItem Value="0" Text="Agenzia Delle Entrate"></asp:ListItem>
                <asp:ListItem Value="1" Text="Asso software"></asp:ListItem>
            </asp:DropDownList>

        </div>
        <div style="float: right;">
            <a href="#" onclick="javascript:Stampa();">
                <img id="PrintLogo" src="../images/printer-blue.png" alt="Print" />
            </a>
        </div>
        <br />
        <br />
        <br />
        <div style="width: 100%; height: 100%; border-width: 1px; background-color: White;">
            <asp:Label ID="LblViewer" runat="server" Text=""></asp:Label>
        </div>
    </form>
</body>
</html>
