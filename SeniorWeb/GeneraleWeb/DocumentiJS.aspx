﻿<%@ Page Language="VB" AutoEventWireup="false" Inherits="GeneraleWeb_DocumentiJS" CodeFile="DocumentiJS.aspx.vb" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="xasp" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
    <asp:PlaceHolder runat="server">
        <%: System.Web.Optimization.Styles.Render("~/Content/AjaxControlToolkit/Styles/Bundle") %>
    </asp:PlaceHolder>

    <link href="ospiti.css?ver=11" rel="stylesheet" type="text/css" />
    <link rel="shortcut icon" href="images/SENIOR.ico" />
    <link href='http://fonts.googleapis.com/css?family=Lato:300,400,700' rel='stylesheet' type='text/css' />
    <link href="js/jquery.autocomplete.css" rel="stylesheet" type="text/css" />

    <link href="documentijs/bootstrap.min.css" rel="stylesheet" type="text/css" />
    <link href="documentijs/style.css" rel="stylesheet" type="text/css" />
    <link href="js/jquery.autocomplete.css" rel="stylesheet" type="text/css" />

    <script src="js/jquery-1.5.1.min.js" type="text/javascript"></script>

    <script src="js/jquery.autocomplete.js?ver=4" type="text/javascript"></script>
    <script src="/js/formatnumer.js" type="text/javascript"></script>

    <script src="js/jquery.maskedinput-1.3.min.js" type="text/javascript"></script>
    <link rel="stylesheet" href="jqueryui/jquery-ui.css" type="text/css" />
    <script src="jqueryui/jquery-ui.js" type="text/javascript"></script>
    <script src="jqueryui/datapicker-it.js" type="text/javascript"></script>


    <link rel="stylesheet" href="js/chosen/chosen.css" />
    <script src="js/chosen/chosen.jquery.js" type="text/javascript"></script>
    <script src="js/chosen/docsupport/prism.js" type="text/javascript" charset="utf-8"></script>

    <link rel="stylesheet" href="dialogbox/redips-dialog.css" type="text/css" media="screen" />
    <script type="text/javascript" src="dialogbox/redips-dialog-min.js"></script>
    <script type="text/javascript" src="dialogbox/script.js"></script>
    <script src="js/JSErrore.js" type="text/javascript"></script>

    <style>
        input[type="text"] {
            height: 34px;
        }

        .ui-widget-header {
            color: black;
        }

        .custom-menu li {
            font-family: "Source Sans Pro",sans-serif;
            font-size: 16px;
        }
    </style>
    <script type="text/javascript">

        function inserisciriga() {
            $(document).ready(function () {
                setTimeout(function () {
                    __doPostBack('BtnInserisci', "");
                }, 1);
            });
        }


        function cancellariga(riga) {
            $(document).ready(function () { setTimeout(function () { __doPostBack('BtnCancellaRiga', riga); }, 1); });
        }


        function ProseguiRegistrazione() {
            $(document).ready(function () { setTimeout(function () { __doPostBack('ProseguiRegistrazione', ""); }, 1); });
        }

        function inserimentorighesupplementari(Numero) {
            $(document).ready(function () { setTimeout(function () { __doPostBack('InserisciRigheExtra', Numero); }, 1); });
        }



        function openriga2(riga) {

            if ($("#item-row_aggiuntiva" + riga).css("display") == 'none') {
                $("#item-row_aggiuntiva" + riga).css("display", "block");
            } else {
                $("#item-row_aggiuntiva" + riga).css("display", "none");
            }

        }

        $(document).ready(function () {
            if (window.innerHeight > 0) { $("#BarraLaterale").css("height", (window.innerHeight - 94) + "px"); } else { $("#BarraLaterale").css("height", (document.documentElement.offsetHeight - 94) + "px"); }
        });

        function DialogBoxBig(Path) {


            var winW = 630, winH = 460;
            if (document.body && document.body.offsetWidth) {
                winW = document.body.offsetWidth;
                winH = document.body.offsetHeight;
            }
            if (document.compatMode == 'CSS1Compat' &&
                document.documentElement &&
                document.documentElement.offsetWidth) {
                winW = document.documentElement.offsetWidth;
                winH = document.documentElement.offsetHeight;
            }
            if (window.innerWidth && window.innerHeight) {
                winW = window.innerWidth;
                winH = window.innerHeight;
            }

            var APPO = winH - 100;


            REDIPS.dialog.show(winW - 200, winH - 100, '<iframe id="output" src="' + Path + '" height="' + APPO + '" width="100%" ></iframe>');

            return false;

        }

        function DialogBoxW(Path) {
            var tot = 0;

            tot = document.body.offsetWidth - 100;
            REDIPS.dialog.show(tot, 500, '<iframe id="output" src="' + Path + '" height="490px" width="' + tot + 'px"></iframe>');
            return false;

        }

        function DialogBox(Path) {

            var tot = 0;

            tot = document.body.offsetWidth - 300;

            REDIPS.dialog.show(tot, 520, '<iframe id="output" src="' + Path + '" height="510px" width="' + tot + 'px"></iframe>');
            return false;

        }
        function DialogBoxSlim(Path) {

            REDIPS.dialog.show(900, 400, '<iframe id="output" src="' + Path + '" height="390px" width="890"></iframe>');
            return false;

        }

    </script>
</head>
<body style="background-color: White;">
    <form id="form1" runat="server">
        <asp:ScriptManager ID="ScriptManager1" runat="server" EnableScriptGlobalization="true">
            <Scripts>
                <asp:ScriptReference Path="~/Scripts/AjaxControlToolkit/Bundle" />
            </Scripts>
        </asp:ScriptManager>

        <asp:Button ID="BTN_InserisciRiga" runat="server" Visible="False" />
        <asp:Button ID="BtnInsertRiga3" runat="server" Text="" Visible="false" />
        <asp:Button ID="BtnInsertRiga4" runat="server" Text="" Visible="false" />
        <asp:Button ID="BtnInsertRiga5" runat="server" Text="" Visible="false" />
        <asp:Button ID="BtnInsertRiga6" runat="server" Text="" Visible="false" />
        <asp:Button ID="BtnInsertRiga7" runat="server" Text="" Visible="false" />
        <asp:Button ID="BtnInsertRiga8" runat="server" Text="" Visible="false" />
        <asp:Button ID="BtnInsertRiga9" runat="server" Text="" Visible="false" />


        <asp:Label runat="server" ID="Lbl_BarraSenior" Text=""></asp:Label>
        <div align="left">
            <table style="width: 100%;" cellpadding="0" cellspacing="0">
                <tr>
                    <td style="width: 160px; background-color: #F0F0F0;" class="destraclasse"></td>
                    <td class="destraclasse">
                        <div class="Titolo">
                            Contabilità - Documenti<br />
                            <br />
                        </div>
                    </td>
                    <td style="text-align: right; vertical-align: top;">
                        <div class="DivTasti">
                            <img src="../images/goright.png" class="EffettoBottoniTondi" style="width: 38px" onclick="ProseguiRegistrazione();" tooltip="Prosegui" />
                        </div>
                    </td>
                </tr>

                <tr>
                    <td style="width: 160px; background-color: #F0F0F0; vertical-align: top; text-align: center;" id="BarraLaterale" class="destraclasse">
                        <a href="Menu_Generale.aspx" style="border-width: 0px;">
                            <img src="images/Home.jpg" alt="Menù" class="Effetto" /></a><br />
                        <asp:ImageButton ID="Btn_Esci" runat="server" BackColor="Transparent" ImageUrl="../images/Menu_Indietro.png" class="Effetto" ToolTip="Chiudi" />

                        <asp:ImageButton ID="ImgRic" src="images/ricerca.jpg" Width="112px" class="Effetto" ToolTip="Ricerca Registrazioni" runat="server" />
                        <br />
                        <label class="MenuDestra">&nbsp;&nbsp;<a href="#" onclick="window.open('VsPianoConti.aspx','Stampe','width=450,height=600,scrollbars=yes');">Vis. Conti</a></label>
                        <br />
                        <label class="MenuDestra">&nbsp;&nbsp;<a href="#" onclick="DialogBoxBig('Pianoconti.aspx');">Piano Conti</a></label>
                        <label class="MenuDestra">&nbsp;&nbsp;<a href="#" onclick="DialogBoxBig('AnagraficaClientiFornitori.aspx?CHIAMANTE=DOCUMENTI');">Cli./For.</a></label><br />

                        <br />
                        <asp:Label ID="Lbl_BtnLegami" runat="server" Text=""></asp:Label>
                        <asp:Label ID="Lbl_BtnScadenzario" runat="server" Text=""></asp:Label>
                    </td>
                    <td colspan="2" style="background-color: #FFFFFF;" valign="top">


                        <!--  class="container" id="paper-wrapper" -->
                        <div style="width: 98%; margin-top: 100px;">


                            <div class="row">

                                <div class="col-sm-6 cmp-pnl">
                                    <div class="inner-cmp-pnl">
                                        <h2></h2>
                                        <div class="form-group">
                                            <label for="frmBizName" class="caption">Causale Contabile<span style="color: red;">*</span></label>
                                            <asp:DropDownList ID="DDCausaliContabili" runat="server" AutoPostBack="true" class="form-control chosen-select">
                                            </asp:DropDownList>
                                        </div>

                                    </div>
                                </div>

                                <div class="col-sm-6 cmp-pnl" style="border-left: solid 1px #f5f6f7;">
                                    <div class="inner-cmp-pnl">
                                        <h2></h2>
                                        <div class="form-group">
                                            <label for="toBizName" class="caption">Modalita Pagamento<span style="color: red;">*</span></label>
                                            <asp:DropDownList ID="DD_ModalitaPagamento" runat="server" class="form-control chosen-select">
                                            </asp:DropDownList>
                                        </div>

                                    </div>
                                </div>

                            </div>


                            <div class="row">

                                <div class="col-sm-2">
                                    <div class="form-group">
                                        <div class="input-group">
                                            <div class="input-group-addon"><span aria-hidden="true" title="Numero Registrazione">NR</span></div>
                                            <asp:TextBox ID="Txt_Numero" autocomplete="off" class="form-control" placeholder="Numero Registrazione" runat="server" Text=""></asp:TextBox>
                                        </div>
                                    </div>
                                </div>



                                <div class="col-sm-2">

                                    <div class="form-group">
                                        <div class="input-group">
                                            <div class="input-group-addon"><span aria-hidden="true" title="Data Registrazione">DR</span></div>
                                            <asp:TextBox ID="Txt_DataRegistrazione" autocomplete="off" class="form-control" placeholder="Data Registrazione" runat="server" Text=""></asp:TextBox>
                                        </div>
                                    </div>

                                </div>

                                <div class="col-sm-2">
                                    <div class="form-group">
                                        <div class="input-group">
                                            <div class="input-group-addon"><span aria-hidden="true" title="Registro IVA">R.</span></div>
                                            <asp:TextBox ID="TxtRegistroIVA" autocomplete="off" class="form-control" Enabled="false" runat="server" Text=""></asp:TextBox>
                                        </div>
                                    </div>
                                </div>

                                <div class="col-sm-3">
                                    <div class="form-group">
                                        <div class="input-group">
                                            <div class="input-group-addon"><span aria-hidden="true" title="Protcollo">P.</span></div>
                                            <asp:TextBox ID="Txt_AnnoProtcollo" Width="30%" autocomplete="off" class="form-control" placeholder="Anno Prot." runat="server" Text=""></asp:TextBox>
                                            <asp:TextBox ID="Txt_NumeroPRotocollo" Width="30%" autocomplete="off" class="form-control" placeholder="Numero Prot." runat="server" Text=""></asp:TextBox>
                                            <asp:TextBox ID="Txt_EstensionePRotocollo" Width="30%" autocomplete="off" class="form-control" placeholder="Bis" runat="server" Text=""></asp:TextBox>
                                        </div>
                                    </div>
                                </div>

                                <div class="col-sm-3">
                                </div>

                            </div>
                            <div class="row">

                                <div class="col-sm-2">
                                    <div class="form-group">
                                        <div class="input-group">
                                            <div class="input-group-addon"><span aria-hidden="true" title="Data Documento">DD</span></div>
                                            <asp:TextBox ID="Txt_DataDocumento" autocomplete="off" class="form-control" placeholder="Data Documento" runat="server" Text=""></asp:TextBox>
                                        </div>
                                    </div>
                                </div>

                                <div class="col-sm-2">
                                    <div class="form-group">
                                        <div class="input-group">
                                            <div class="input-group-addon"><span aria-hidden="true" title="Numero Documento">ND</span></div>
                                            <asp:TextBox ID="Txt_NumeroDocumento" autocomplete="off" class="form-control" placeholder="Numero Documento" runat="server" Text=""></asp:TextBox>
                                        </div>
                                    </div>
                                </div>

                                <div class="col-sm-4">
                                    <div class="form-group">
                                        <div class="input-group">
                                            <div class="input-group-addon"><span aria-hidden="true"></span></div>
                                            <span class="form-control">IVA SOSPESA :&nbsp;
                            <asp:RadioButton ID="RB_IvaSospesaSI" runat="server" Text="Si" Width="30px" GroupName="IvaSospesa" />
                                                <asp:RadioButton ID="RB_IvaSospesaNO" runat="server" Text="No" Width="50px" GroupName="IvaSospesa" />

                                                Bollo Virtuale :&nbsp;<asp:CheckBox ID="Chk_BolloVirtuale" runat="server" />
                                            </span>
                                        </div>
                                    </div>
                                </div>

                                <div class="col-sm-4">
                                </div>

                            </div>


                            <div class="row">

                                <div class="col-sm-12 cmp-pnl">
                                    <div class="inner-cmp-pnl">
                                        <h2></h2>
                                        <div class="form-group">
                                            <label for="frmBizName" class="caption">Cliente/Fornitore<span style="color: red;">*</span></label>
                                            <asp:TextBox ID="Txt_ClienteFornitore" class="form-control" onBlur='javascript:setTimeout(function() {__doPostBack("ModificaFornitore", "0"); }, 300);' runat="server" Text=""></asp:TextBox>
                                        </div>

                                    </div>
                                </div>


                            </div>

                            <div id="item-pnl">

                                <div class="row items-pnl-head">
                                    <div class="col-sm-1 col"></div>
                                    <div class="col-sm-6 col extendable" style="text-align: left"></div>
                                    <div class="col-sm-1 col"></div>
                                    <div class="col-sm-2 col"></div>
                                    <div class="col-sm-2 col"></div>
                                </div>

                                <asp:UpdatePanel ID="UpdatePanel1" runat="server">
                                    <ContentTemplate>
                                        <asp:Label ID="Lbl_Righe" runat="server" Text="Label">
                                        </asp:Label>

                                        <asp:Button ID="BtnInserisci" runat="server" Text="" Visible="true" Style="display: none;" />
                                    </ContentTemplate>
                                </asp:UpdatePanel>


                                <div class="row items-pnl-body" id="item-rownew"></div>
                            </div>



                            <ul class="custom-menu" id="rightmenu" style="z-index: 9999;">
                            </ul>
                            <ul class="custom-menu" id="rightmenuIVA">
                            </ul>
                            <div style="height: 40px;"></div>
                        </div>
                    </td>
                </tr>
            </table>
        </div>
    </form>
</body>
</html>
