﻿Imports System.Threading
Imports System.Data.OleDb

Partial Class mastrino
    Inherits System.Web.UI.Page
    Private Delegate Sub DoWorkDelegate(ByRef data As Object)
    Private _work As DoWorkDelegate


    Private Sub DoWork(ByVal data As Object)
        Call Stampa(data)
    End Sub


    Private Function TroncateString(ByVal Stringa As String) As String
        TroncateString = Stringa.Trim
        If TroncateString.Length >= 250 Then
            TroncateString = Mid(TroncateString, 1, 249)
        End If
    End Function

    Function CondizioneCserv() As String
        Dim Condizione As String = ""
        If DD_CServ.SelectedValue = "" Then
            If DD_Struttura.SelectedValue <> "" Then
                Dim Indice As Integer
                Call AggiornaCServ()
                Condizione = Condizione & " ("
                For Indice = 0 To DD_CServ.Items.Count - 1
                    If DD_CServ.Items(Indice).Value <> "" Then
                        If Indice >= 1 Then
                            If Condizione <> "" Then
                                Condizione = Condizione & " Or "
                            End If
                        End If

                        Condizione = Condizione & "  MovimentiContabiliTesta.CentroServizio = '" & DD_CServ.Items(Indice).Value & "'"
                    End If
                Next
                Condizione = Condizione & ") "
            End If
        Else
            Condizione = Condizione & "  MovimentiContabiliTesta.CentroServizio = '" & DD_CServ.SelectedValue & "'"
        End If

        Return Condizione
    End Function

    Protected Sub Stampa(ByVal data As Object)

        Dim SessioneTP As System.Web.SessionState.HttpSessionState = Data


        Dim MyRs As New ADODB.Recordset

        Dim RsReg As New ADODB.Recordset
        Dim RsCercaCF As New ADODB.Recordset
        Dim RsDescrizione As New ADODB.Recordset
        Dim RsControPartitaCostiRicavi As New ADODB.Recordset

        Dim AppoggioMastro As Long
        Dim AppoggioConto As Long
        Dim AppoggioSottoConto As Long
        Dim AppoggioDescrizioneMastro, AppoggioDescrizioneConto, AppoggioDescrizioneSottoConto As String

        Dim OspitiDb As New ADODB.Connection
        Dim GeneraleDb As New ADODB.Connection
        Dim MySql As String
        Dim WSocieta As String
        Dim Condizione As String
        Dim Tutti As Boolean
        Dim OldMastro As Long
        Dim OldConto As Long
        Dim OldSottoconto As Long
        Dim Importo As Double
        Dim xTstotaleSaldo As Double
        Dim xTsAppoggioMastro As Long
        Dim xTsAppoggioConto As Long
        Dim xTsAppoggioSottoConto As Long
        Dim SaldoAppoggio As Double
        Dim AppoggioVerificaCausale As String
        Dim Vettore(100) As String
        Dim TotaleRecord As Long
        Dim RecordElaborato As Long = 0

        Dim Saldo As Double

        OspitiDb.Open(Session("DC_OSPITE"))
        GeneraleDb.Open(Session("DC_GENERALE"))

        Dim XSoc As New Cls_DecodificaSocieta

        WSocieta = XSoc.DecodificaSocieta(Session("DC_TABELLE"))

        Condizione = ""
        Tutti = True



        If Condizione <> "" Then
            Condizione = Condizione & " AND"
        End If
        Dim Txt_DataDalText As Date
        Dim Txt_DataAlText As Date

        Dim DataInizio As Date



        Dim Txt_MastroDalText As Long
        Dim Txt_MastroAlText As Long
        Dim Txt_ContoDalText As Long
        Dim Txt_ContoAlText As Long
        Dim Txt_SottocontoDalText As Long
        Dim Txt_SottocontoAlText As Long

        Dim Stampa As New StampeGenerale

        Try
            Vettore = SplitWords(Txt_SottocontoDal.Text)

            Txt_MastroDalText = Val(Vettore(0))
            Txt_ContoDalText = Val(Vettore(1))
            Txt_SottocontoDalText = Val(Vettore(2))

        Catch ex As Exception

        End Try


        Try
            Vettore = SplitWords(Txt_SottocontoAl.Text)

            Txt_MastroAlText = Val(Vettore(0))
            Txt_ContoAlText = Val(Vettore(1))
            Txt_SottocontoAlText = Val(Vettore(2))

        Catch ex As Exception

        End Try

        If Txt_SottocontoDalText = 0 And Txt_SottocontoAlText = 0 Then
            Txt_SottocontoAlText = 999999999
        End If

        If Txt_ContoDalText = 0 And Txt_ContoDalText = 0 Then
            Txt_ContoalText = 999999999
        End If


        Txt_DataDalText = DateSerial(Mid(Txt_DataDal.Text, 7, 4), Mid(Txt_DataDal.Text, 4, 2), Mid(Txt_DataDal.Text, 1, 2))
        Txt_DataAlText = DateSerial(Mid(Txt_DataAl.Text, 7, 4), Mid(Txt_DataAl.Text, 4, 2), Mid(Txt_DataAl.Text, 1, 2))


        If Chk_SaldiAnnoPrecedenti.Checked = True Then
            DataInizio = DateSerial(Year(Txt_DataDalText) - 1, 1, 1)
        Else
            DataInizio = DateSerial(Year(Txt_DataDalText), 1, 1)
        End If


        Condizione = Condizione & " DataRegistrazione >= {ts'" & Format(Txt_DataDalText, "yyyy-MM-dd") & " 00:00:00'}"
        Condizione = Condizione & " AND DataRegistrazione <= {ts'" & Format(Txt_DataAlText, "yyyy-MM-dd") & " 00:00:00'}"


        If Txt_MastroDalText = Txt_MastroAlText And Txt_ContoAlText = Txt_ContoDalText And Txt_SottocontoDalText = Txt_SottocontoAlText Then
            If Condizione <> "" Then
                Condizione = Condizione & " AND"
            End If
            Condizione = Condizione & " (MastroPartita = " & Txt_MastroDalText & " And ContoPartita = " & Txt_ContoDalText & " And SottoContoPartita = " & Txt_SottocontoDalText & ") "
        Else
            If Txt_SottocontoDalText <> 0 Or Txt_ContoDalText <> 0 Or Txt_MastroDalText <> 0 Then
                If Condizione <> "" Then
                    Condizione = Condizione & " AND"
                End If
                Condizione = Condizione & " (MastroPartita > " & Txt_MastroDalText & " Or ( " & _
                                  " MastroPartita = " & Txt_MastroDalText & " AND " & _
                                  " (ContoPartita > " & Txt_ContoDalText & " OR " & _
                                  " (ContoPartita = " & Txt_ContoDalText & " AND " & _
                                  "  SottoContoPartita >= " & Txt_SottocontoDalText & " )))) "
            End If

            If Txt_SottocontoAlText <> 0 Or Txt_ContoAlText <> 0 Or Txt_MastroAlText <> 0 Then
                If Condizione <> "" Then
                    Condizione = Condizione & " AND"
                End If
                Condizione = Condizione & " (MastroPartita < " & Txt_MastroAlText & " Or ( " & _
                                  " MastroPartita = " & Txt_MastroAlText & " AND " & _
                                  " (ContoPartita < " & Txt_ContoAlText & " OR " & _
                                  " (ContoPartita = " & Txt_ContoAlText & " AND " & _
                                  "  SottoContoPartita <= " & Txt_SottocontoAlText & " )))) "
            End If
        End If



        If Chk_ImponibileZERO.Checked = False Or Chk_ImportoZERO.Checked = False Then
            If Chk_ImportoZERO.Checked = True Then
                Condizione = Condizione & " And (MovimentiContabiliRiga.Importo <> 0)"
            End If
            If Chk_ImponibileZERO.Checked = True Then
                Condizione = Condizione & " And (MovimentiContabiliRiga.Imponibile <> 0)"
            End If
            If Chk_ImponibileZERO.Checked = False And Chk_ImportoZERO.Checked = False Then
                Condizione = Condizione & " And (MovimentiContabiliRiga.Imponibile <> 0  Or MovimentiContabiliRiga.Importo <> 0) "
            End If
        End If


        If Chk_ImportoZERO.Checked = True And Chk_ImponibileZERO.Checked = True Then
            Condizione = Condizione & " And (Importo <> 0  And Imponibile <> 0)"
        End If

        If CondizioneCserv() <> "" Then
            Condizione = Condizione & " And " & CondizioneCserv()
        End If


        'WrRs.Open("Select * From Mastrino Where PrinterKey = '" & ViewState("PRINTERKEY") & "'", StampeDb, ADODB.CursorTypeEnum.adOpenDynamic, ADODB.LockTypeEnum.adLockOptimistic)




        OldMastro = 0
        OldConto = 0
        OldSottoconto = 0

        AppoggioMastro = 0
        AppoggioConto = 0
        AppoggioSottoConto = 0
        AppoggioDescrizioneMastro = ""
        AppoggioDescrizioneConto = ""
        AppoggioDescrizioneSottoConto = ""


        MySql = "SELECT MovimentiContabiliRiga.ID  FROM MovimentiContabiliTesta " & _
                " INNER JOIN MovimentiContabiliRiga " & _
                " ON MovimentiContabiliTesta.NumeroRegistrazione = MovimentiContabiliRiga.Numero " & _
                " WHERE " & Condizione

        ID = 0

        MyRs.Open("SELECT Count(*)  as Totale From (" & MySql & ") AS TabUno", GeneraleDb, ADODB.CursorTypeEnum.adOpenDynamic, ADODB.LockTypeEnum.adLockOptimistic)
        If Not MyRs.EOF Then
            TotaleRecord = MoveFromDbWC(MyRs, "Totale")
        End If
        MyRs.Close()

        MySql = "SELECT *,MovimentiContabiliRiga.ID, MovimentiContabiliRiga.Descrizione AS DescrizioneRiga,MovimentiContabiliTesta.Descrizione as DescrizioneTesta  FROM MovimentiContabiliTesta " & _
                " INNER JOIN MovimentiContabiliRiga " & _
                " ON MovimentiContabiliTesta.NumeroRegistrazione = MovimentiContabiliRiga.Numero " & _
                " WHERE " & Condizione & _
                " ORDER BY MastroPartita, ContoPartita, SottocontoPartita, DataRegistrazione, NumeroRegistrazione,CausaleContabile, MovimentiContabiliRiga.ID"


        MyRs.Open(MySql, GeneraleDb, ADODB.CursorTypeEnum.adOpenDynamic, ADODB.LockTypeEnum.adLockOptimistic)
        Do While Not MyRs.EOF

            RecordElaborato = RecordElaborato + 1
            Importo = MoveFromDbWC(MyRs, "Importo")

            SessioneTP("CampoProgressBar") = Math.Round(RecordElaborato * 100 / TotaleRecord, 2)
            System.Web.HttpRuntime.Cache("CampoProgressBar" + Session.SessionID) = Math.Round(RecordElaborato * 100 / TotaleRecord, 2)
            If Chk_saldoDivZero.Checked = True Then
                If MoveFromDbWC(MyRs, "MastroPartita") <> xTsAppoggioMastro Or _
                   MoveFromDbWC(MyRs, "ContoPartita") <> xTsAppoggioConto Or _
                   MoveFromDbWC(MyRs, "SottocontoPartita") <> xTsAppoggioSottoConto Then
                    xTstotaleSaldo = totaleSaldo(MoveFromDbWC(MyRs, "MastroPartita"), MoveFromDbWC(MyRs, "ContoPartita"), MoveFromDbWC(MyRs, "SottocontoPartita"), DataInizio)
                    xTsAppoggioMastro = MoveFromDbWC(MyRs, "MastroPartita")
                    xTsAppoggioConto = MoveFromDbWC(MyRs, "ContoPartita")
                    xTsAppoggioSottoConto = MoveFromDbWC(MyRs, "SottocontoPartita")
                End If
            End If
            If Chk_saldoDivZero.Checked = False Or (Chk_saldoDivZero.Checked = True And xTstotaleSaldo <> 0) Then

                'WrRs.AddNew()
                Dim WrRs As System.Data.DataRow = Stampa.Tables("Mastrino").NewRow
                ID = ID + 1
                WrRs.Item("Id") = ID

                If MoveFromDbWC(MyRs, "MastroPartita") <> AppoggioMastro Or _
                   MoveFromDbWC(MyRs, "ContoPartita") <> AppoggioConto Or _
                   MoveFromDbWC(MyRs, "SottocontoPartita") <> AppoggioSottoConto Then
                    SaldoAppoggio = totaleSottoConto(MoveFromDbWC(MyRs, "MastroPartita"), MoveFromDbWC(MyRs, "ContoPartita"), MoveFromDbWC(MyRs, "SottocontoPartita"), DataInizio)
                    AppoggioMastro = MoveFromDbWC(MyRs, "MastroPartita")
                    AppoggioConto = MoveFromDbWC(MyRs, "ContoPartita")
                    AppoggioSottoConto = MoveFromDbWC(MyRs, "SottocontoPartita")

                    Dim X1 As New Cls_Pianodeiconti

                    X1.Mastro = MoveFromDbWC(MyRs, "MastroPartita")
                    X1.Conto = 0
                    X1.Sottoconto = 0
                    X1.Decodfica(Session("DC_GENERALE"))

                    AppoggioDescrizioneMastro = Trim(X1.Descrizione)

                    X1.Mastro = MoveFromDbWC(MyRs, "MastroPartita")
                    X1.Conto = MoveFromDbWC(MyRs, "ContoPartita")
                    X1.Sottoconto = 0
                    X1.Decodfica(Session("DC_GENERALE"))

                    AppoggioDescrizioneConto = Trim(X1.Descrizione)

                    X1.Mastro = MoveFromDbWC(MyRs, "MastroPartita")
                    X1.Conto = MoveFromDbWC(MyRs, "ContoPartita")
                    X1.Sottoconto = MoveFromDbWC(MyRs, "SottocontoPartita")
                    X1.Decodfica(Session("DC_GENERALE"))

                    AppoggioDescrizioneSottoConto = Trim(X1.Descrizione)
                End If


                WrRs.Item("ProgressivoAnno") = MoveFromDbWC(MyRs, "ProgressivoAnno")
                WrRs.Item("ProgressivoNumero") = MoveFromDbWC(MyRs, "ProgressivoNumero")

                WrRs.Item("DescrizioneMastroPartita") = AppoggioDescrizioneMastro
                WrRs.Item("DescrizioneContoPartita") = AppoggioDescrizioneConto
                WrRs.Item("DescrizioneSottocontoPartita") = AppoggioDescrizioneSottoConto


                WrRs.Item("SaldoIniziale") = SaldoAppoggio

                WrRs.Item("chiaveselezione") = Format(MoveFromDbWC(MyRs, "MastroPartita"), "000") & Format(MoveFromDbWC(MyRs, "ContoPartita"), "0000") & Format(MoveFromDbWC(MyRs, "SottocontoPartita"), "00000000000")


                WrRs.Item("MastroPartita") = MoveFromDbWC(MyRs, "MastroPartita")

                WrRs.Item("ContoPartita") = MoveFromDbWC(MyRs, "ContoPartita")


                WrRs.Item("SottocontoPartita") = MoveFromDbWC(MyRs, "SottocontoPartita")

                WrRs.Item("NumeroRegistrazione") = MoveFromDbWC(MyRs, "NumeroRegistrazione")
                WrRs.Item("DataRegistrazione") = MoveFromDbWC(MyRs, "DataRegistrazione")
                WrRs.Item("NumeroDocumento") = MoveFromDbWC(MyRs, "NumeroDocumento")
                If IsDate(MoveFromDbWC(MyRs, "DataDocumento")) Then
                    WrRs.Item("DataDocumento") = MoveFromDbWC(MyRs, "DataDocumento")
                End If
                WrRs.Item("NumeroProtocollo") = MoveFromDbWC(MyRs, "NumeroProtocollo")
                WrRs.Item("CausaleContabile") = MoveFromDbWC(MyRs, "CausaleContabile")


                WrRs.Item("DataDal") = Txt_DataDalText
                WrRs.Item("DataAl") = Txt_DataAlText



                If MoveFromDbWC(MyRs, "Tipo") = "RI" Then
                    WrRs.Item("DecodificaCausaleContabile") = "RITENUTA D'ACCONTO"
                Else
                    Dim MyCas As New Cls_CausaleContabile

                    MyCas.Leggi(Session("DC_TABELLE"), MoveFromDbWC(MyRs, "CausaleContabile"))
                    WrRs.Item("DecodificaCausaleContabile") = MyCas.Descrizione
                End If

                If Chk_DescrizioneRiga.checked = False Then
                    If Trim(MoveFromDbWC(MyRs, "DescrizioneRiga")) <> Trim(MoveFromDbWC(MyRs, "DescrizioneTesta")) Then
                        WrRs.Item("Descrizione") = TroncateString(Trim(MoveFromDbWC(MyRs, "DescrizioneRiga") & " " & MoveFromDbWC(MyRs, "DescrizioneTesta")))
                    Else
                        WrRs.Item("Descrizione") = TroncateString(Trim(MoveFromDbWC(MyRs, "DescrizioneRiga")))
                    End If
                Else
                    If Trim(MoveFromDbWC(MyRs, "DescrizioneRiga")) <> "" Then
                        WrRs.Item("Descrizione") = TroncateString(Trim(MoveFromDbWC(MyRs, "DescrizioneRiga")))
                    Else
                        WrRs.Item("Descrizione") = TroncateString(Trim(MoveFromDbWC(MyRs, "DescrizioneTesta")))
                    End If
                End If

                WrRs.Item("MastroContropartita") = MoveFromDbWC(MyRs, "MastroContropartita")
                WrRs.Item("ContoContropartita") = MoveFromDbWC(MyRs, "ContoContropartita")
                WrRs.Item("SottocontoContropartita") = MoveFromDbWC(MyRs, "SottocontoContropartita")


                If IsDate(MoveFromDbWC(MyRs, "DataValuta")) Then
                    WrRs.Item("DataValuta") = MoveFromDbWC(MyRs, "DataValuta")
                End If



                Dim MyCas1 As New Cls_CausaleContabile

                MyCas1.Leggi(Session("DC_TABELLE"), MoveFromDbWC(MyRs, "CausaleContabile"))
                AppoggioVerificaCausale = MyCas1.Tipo
                If AppoggioVerificaCausale = "I" Or AppoggioVerificaCausale = "P" Then
                    WrRs.Item("FornitoreCliente") = SottoContoRigaUno(MoveFromDbWC(MyRs, "NumeroRegistrazione"))
                Else
                    WrRs.Item("FornitoreCliente") = ""
                End If

                If Val(WrRs.Item("MastroContropartita")) > 0 Then
                    Dim OPi As New Cls_Pianodeiconti
                    OPi.Mastro = WrRs.Item("MastroContropartita")
                    OPi.Conto = WrRs.Item("ContoContropartita")
                    OPi.Sottoconto = WrRs.Item("SottocontoContropartita")
                    OPi.Decodfica(Session("DC_GENERALE"))
                    WrRs.Item("DescrizioneContropartita") = Trim(OPi.Descrizione)
                End If
                If OldMastro <> MoveFromDbWC(MyRs, "MastroPartita") Or OldConto <> MoveFromDbWC(MyRs, "ContoPartita") Or OldSottoconto <> MoveFromDbWC(MyRs, "SottocontoPartita") Then
                    OldMastro = MoveFromDbWC(MyRs, "MastroPartita")
                    OldConto = MoveFromDbWC(MyRs, "ContoPartita")
                    OldSottoconto = MoveFromDbWC(MyRs, "SottocontoPartita")
                    Saldo = WrRs.Item("SaldoIniziale")
                End If
                WrRs.Item("IntestaSocieta") = WSocieta
                WrRs.Item("DataElaborazione") = Now
                If MoveFromDbWC(MyRs, "DareAvere") = "D" Then
                    Saldo = Math.Round(Saldo + Importo, 2)
                    WrRs.Item("Dare") = Importo
                End If
                If MoveFromDbWC(MyRs, "DareAvere") = "A" Then
                    Saldo = Math.Round(Saldo - Importo, 2)
                    WrRs.Item("Avere") = Importo
                End If
                WrRs.Item("Saldo") = Saldo

                'WrRs.Item("PRINTERKEY") = ViewState("PRINTERKEY")

                Stampa.Tables("Mastrino").Rows.Add(WrRs)
            End If

            MyRs.MoveNext()
        Loop
        MyRs.Close()



        SessioneTP("stampa") = Stampa
        SessioneTP("CampoProgressBar") = "FINE"
        System.Web.HttpRuntime.Cache("stampa" + Session.SessionID) = Stampa
        System.Web.HttpRuntime.Cache("CampoProgressBar" + Session.SessionID) = "FINE"
        'Response.Redirect("StampaReport.aspx?REPORT=MASTRINO")
  
    End Sub
    Public Function SottoContoRigaUno(ByVal NumeroReg As Long) As String
        Dim MyRs As New ADODB.Recordset
        Dim MySql As String        
        Dim MyTipo As String

        Dim GeneraleDb As New ADODB.Connection


        GeneraleDb.Open(Session("DC_GENERALE"))
        SottoContoRigaUno = ""

        MySql = "Select MastroPartita,ContoPartita,SottoContoPartita From MovimentiContabiliRiga Where Numero = " & NumeroReg & " And RigaDaCausale = 1"
        

        MyRs.Open(MySql, GeneraleDb, ADODB.CursorTypeEnum.adOpenDynamic, ADODB.LockTypeEnum.adLockOptimistic)
        If Not MyRs.EOF Then
            Dim Ks1 As New Cls_Pianodeiconti

            Ks1.Mastro = MoveFromDbWC(MyRs, "MastroPartita")
            Ks1.Conto = MoveFromDbWC(MyRs, "ContoPartita")
            Ks1.Sottoconto = MoveFromDbWC(MyRs, "SottocontoPartita")
            Ks1.Decodfica(Session("DC_GENERALE"))
            MyTipo = Ks1.TipoAnagrafica
            If MyTipo = "C" Or MyTipo = "O" Or MyTipo = "P" Or MyTipo = "R" Or MyTipo = "D" Then
                SottoContoRigaUno = Ks1.Descrizione
            End If            
        End If
        MyRs.Close()

        GeneraleDb.Close()
        Return SottoContoRigaUno
    End Function

    Public Function MoveFromDbWC(ByVal MyRs As ADODB.Recordset, ByVal Campo As String) As Object
        Dim MyField As ADODB.Field

#If FLAG_DEBUG = False Then
        On Error GoTo ErroreCampoInesistente
#End If
        MyField = MyRs.Fields(Campo)

        Select Case MyField.Type
            Case ADODB.DataTypeEnum.adDBDate
                If IsDBNull(MyField.Value) Then
                    MoveFromDbWC = "__/__/____"
                Else
                    If Not IsDate(MyField.Value) Or MyField.Value = "0.00.00" Then
                        MoveFromDbWC = "__/__/____"
                    Else
                        MoveFromDbWC = MyField.Value
                    End If
                End If
            Case ADODB.DataTypeEnum.adDBTimeStamp
                If IsDBNull(MyField.Value) Then
                    MoveFromDbWC = "__/__/____"
                Else
                    If Not IsDate(MyField.Value) Then
                        MoveFromDbWC = "__/__/____"
                    Else
                        MoveFromDbWC = MyField.Value
                    End If
                End If
            Case ADODB.DataTypeEnum.adSmallInt
                If IsDBNull(MyField.Value) Then
                    MoveFromDbWC = 0
                Else
                    MoveFromDbWC = MyField.Value
                End If
            Case ADODB.DataTypeEnum.adInteger
                If IsDBNull(MyField.Value) Then
                    MoveFromDbWC = 0
                Else
                    MoveFromDbWC = MyField.Value
                End If
            Case ADODB.DataTypeEnum.adNumeric
                If IsDBNull(MyField.Value) Then
                    MoveFromDbWC = 0
                Else
                    MoveFromDbWC = MyField.Value
                End If
            Case ADODB.DataTypeEnum.adTinyInt
                If IsDBNull(MyField.Value) Then
                    MoveFromDbWC = 0
                Else
                    MoveFromDbWC = MyField.Value
                End If
            Case ADODB.DataTypeEnum.adSingle
                If IsDBNull(MyField.Value) Then
                    MoveFromDbWC = 0
                Else
                    MoveFromDbWC = MyField.Value
                End If
            Case ADODB.DataTypeEnum.adLongVarBinary
                If IsDBNull(MyField.Value) Then
                    MoveFromDbWC = 0
                Else
                    MoveFromDbWC = MyField.Value
                End If
            Case ADODB.DataTypeEnum.adDouble
                If IsDBNull(MyField.Value) Then
                    MoveFromDbWC = 0
                Else
                    MoveFromDbWC = MyField.Value
                End If
            Case ADODB.DataTypeEnum.adVarChar
                If IsDBNull(MyField.Value) Then
                    MoveFromDbWC = vbNullString
                Else
                    MoveFromDbWC = CStr(MyField.Value)
                End If
            Case ADODB.DataTypeEnum.adUnsignedTinyInt
                If IsDBNull(MyField.Value) Then
                    MoveFromDbWC = 0
                Else
                    MoveFromDbWC = MyField.Value
                End If
            Case Else
                If IsDBNull(MyField.Value) Then
                    MoveFromDbWC = ""
                End If
                If IsDBNull(MyField.Value) Then
                    MoveFromDbWC = ""
                Else
                    MoveFromDbWC = MyField.Value
                End If
        End Select

        On Error GoTo 0
        Exit Function
ErroreCampoInesistente:

        On Error GoTo 0
    End Function

    Private Function totaleSaldo(ByVal Mastro As Integer, ByVal Conto As Integer, ByVal Sottoconto As Long, ByVal DataInizio As Date) As Double
        Dim MyRs As New ADODB.Recordset
        Dim Dare As Double
        Dim Avere As Double
        Dim MySql As String


        Dim GeneraleDb As New ADODB.Connection
        Dim Txt_DataDalText As Date
        Dim Txt_DataAlText As Date

        Txt_DataDalText = DateSerial(Mid(Txt_DataDal.Text, 7, 4), Mid(Txt_DataDal.Text, 4, 2), Mid(Txt_DataDal.Text, 1, 2))
        Txt_DataAlText = DateSerial(Mid(Txt_DataAl.Text, 7, 4), Mid(Txt_DataAl.Text, 4, 2), Mid(Txt_DataAl.Text, 1, 2))


        GeneraleDb.Open(Session("DC_GENERALE"))
        MySql = "SELECT SUM (Importo) As Totale FROM MovimentiContabiliTesta " & _
                " INNER JOIN MovimentiContabiliRiga " & _
                " ON MovimentiContabiliTesta.NumeroRegistrazione = MovimentiContabiliRiga.Numero " & _
                " WHERE MastroPartita = " & Mastro & _
                " AND ContoPartita = " & Conto & _
                " AND SottocontoPartita = " & Sottoconto & _
                " AND DareAvere = 'D' " & _
                " AND DataRegistrazione >= {ts'" & Format(DataInizio, "yyyy-MM-dd") & " 00:00:00'}" & _
                " AND DataRegistrazione <= {ts'" & Format(Txt_DataAlText, "yyyy-MM-dd") & " 00:00:00'}"
        If CondizioneCserv() <> "" Then
            MySql = MySql & " And " & CondizioneCserv()
        End If
        MyRs.Open(MySql, GeneraleDb, ADODB.CursorTypeEnum.adOpenDynamic, ADODB.LockTypeEnum.adLockOptimistic)
        If Not MyRs.EOF Then
            Dare = MoveFromDbWC(MyRs, "Totale")
        End If
        MyRs.Close()


        MySql = "SELECT SUM (Importo) As Totale FROM MovimentiContabiliTesta " & _
                " INNER JOIN MovimentiContabiliRiga " & _
                " ON MovimentiContabiliTesta.NumeroRegistrazione = MovimentiContabiliRiga.Numero " & _
                " WHERE MastroPartita = " & Mastro & _
                " AND ContoPartita = " & Conto & _
                " AND SottocontoPartita = " & Sottoconto & _
                " AND DareAvere = 'A' " & _
                " AND DataRegistrazione >= {ts'" & Format(DataInizio, "yyyy-MM-dd") & " 00:00:00'}" & _
                " AND DataRegistrazione <= {ts'" & Format(Txt_DataAlText, "yyyy-MM-dd") & " 00:00:00'}"
        If CondizioneCserv() <> "" Then
            MySql = MySql & " And " & CondizioneCserv()
        End If
        MyRs.Open(MySql, GeneraleDb, ADODB.CursorTypeEnum.adOpenDynamic, ADODB.LockTypeEnum.adLockOptimistic)
        If Not MyRs.EOF Then
            Avere = MoveFromDbWC(MyRs, "Totale")
        End If
        MyRs.Close()

        totaleSaldo = Math.Round(Dare - Avere, 2)

        GeneraleDb.Close()
    End Function


    Private Function totaleSottoConto(ByVal Mastro As Integer, ByVal Conto As Integer, ByVal Sottoconto As Long, ByVal DataInizio As Date) As Double
        Dim MyRs As New ADODB.Recordset
        Dim GeneraleDb As New ADODB.Connection
        Dim MySql As String
        Dim Dare As Double
        Dim Avere As Double

        Dim Txt_DataDalText As Date
        Dim Txt_DataAlText As Date

        Txt_DataDalText = DateSerial(Mid(Txt_DataDal.Text, 7, 4), Mid(Txt_DataDal.Text, 4, 2), Mid(Txt_DataDal.Text, 1, 2))
        Txt_DataAlText = DateSerial(Mid(Txt_DataAl.Text, 7, 4), Mid(Txt_DataAl.Text, 4, 2), Mid(Txt_DataAl.Text, 1, 2))

        GeneraleDb.Open(Session("DC_GENERALE"))

        MySql = "SELECT SUM (Importo) As Totale FROM MovimentiContabiliTesta " & _
                " INNER JOIN MovimentiContabiliRiga " & _
                " ON MovimentiContabiliTesta.NumeroRegistrazione = MovimentiContabiliRiga.Numero " & _
                " WHERE MastroPartita = " & Mastro & _
                " AND ContoPartita = " & Conto & _
                " AND SottocontoPartita = " & Sottoconto & _
                " AND DareAvere = 'D' " & _
                " AND DataRegistrazione >= {ts'" & Format(DataInizio, "yyyy-MM-dd") & " 00:00:00'}" & _
                " AND DataRegistrazione < {ts'" & Format(Txt_DataDalText, "yyyy-MM-dd") & " 00:00:00'}"
        If CondizioneCserv() <> "" Then
            MySql = MySql & " And " & CondizioneCserv()
        End If
        MyRs.Open(MySql, GeneraleDb, ADODB.CursorTypeEnum.adOpenDynamic, ADODB.LockTypeEnum.adLockOptimistic)
        If Not MyRs.EOF Then
            Dare = MoveFromDbWC(MyRs, "Totale")
        End If
        MyRs.Close()

        MySql = "SELECT SUM (Importo) As Totale FROM MovimentiContabiliTesta " & _
                    " INNER JOIN MovimentiContabiliRiga " & _
                    " ON MovimentiContabiliTesta.NumeroRegistrazione = MovimentiContabiliRiga.Numero " & _
                    " WHERE MastroPartita = " & Mastro & _
                    " AND ContoPartita = " & Conto & _
                    " AND SottocontoPartita = " & Sottoconto & _
                    " AND DareAvere = 'A' " & _
                    " AND DataRegistrazione >= {ts'" & Format(DataInizio, "yyyy-MM-dd") & " 00:00:00'}" & _
                    " AND DataRegistrazione < {ts'" & Format(Txt_DataDalText, "yyyy-MM-dd") & " 00:00:00'}"
        If CondizioneCserv() <> "" Then
            MySql = MySql & " And " & CondizioneCserv()
        End If
        MyRs.Open(MySql, GeneraleDb, ADODB.CursorTypeEnum.adOpenDynamic, ADODB.LockTypeEnum.adLockOptimistic)
        If Not MyRs.EOF Then
            Avere = MoveFromDbWC(MyRs, "Totale")
        End If
        MyRs.Close()

        totaleSottoConto = Math.Round(Dare - Avere, 2)
        GeneraleDb.Close()
    End Function
    Public Sub MoveToDb(ByRef MyField As ADODB.Field, ByVal MyVal As Object)
        Select Case Val(MyField.Type)
            Case ADODB.DataTypeEnum.adDBDate Or ADODB.DataTypeEnum.adDBTimeStamp
                If IsDate(MyVal) Then
                    MyField.Value = MyVal
                Else
                    MyField.Value = Nothing
                End If
            Case ADODB.DataTypeEnum.adDouble
                If IsNumeric(MyVal) Then
                    MyField.Value = MyVal
                Else
                    MyField.Value = 0
                End If
            Case ADODB.DataTypeEnum.adSmallInt
                If IsNumeric(MyVal) Then
                    MyField.Value = MyVal
                Else : MyField.Value = 0
                End If
            Case ADODB.DataTypeEnum.adInteger
                If IsNumeric(MyVal) Then
                    MyField.Value = MyVal
                Else
                    MyField.Value = 0
                End If
            Case ADODB.DataTypeEnum.adNumeric
                If IsNumeric(MyVal) Then
                    MyField.Value = MyVal
                Else
                    MyField.Value = 0
                End If
            Case ADODB.DataTypeEnum.adTinyInt
                If IsNumeric(MyVal) Then
                    MyField.Value = MyVal
                Else
                    MyField.Value = 0
                End If
            Case ADODB.DataTypeEnum.adSingle
                If IsNumeric(MyVal) Then
                    MyField.Value = MyVal
                Else
                    MyField.Value = 0
                End If
            Case ADODB.DataTypeEnum.adLongVarBinary
                If IsNumeric(MyVal) Then
                    MyField.Value = MyVal
                Else
                    MyField.Value = 0
                End If
            Case ADODB.DataTypeEnum.adUnsignedTinyInt
                If IsNumeric(MyVal) Then
                    MyField.Value = MyVal
                Else
                    MyField.Value = 0
                End If
            Case ADODB.DataTypeEnum.adVarChar Or ADODB.DataTypeEnum.adVarWChar Or ADODB.DataTypeEnum.adWChar Or ADODB.DataTypeEnum.adBSTR
                If IsDBNull(MyVal) Then
                    MyField.Value = ""
                Else
                    If Len(MyVal) > MyField.DefinedSize Then
                        MyField.Value = Mid(MyVal, 1, MyField.DefinedSize)
                    Else
                        MyField.Value = MyVal
                    End If
                End If
            Case Else
                If Val(MyField.Type) = ADODB.DataTypeEnum.adVarChar Or Val(MyField.Type) = ADODB.DataTypeEnum.adVarWChar Or _
                   Val(MyField.Type) = ADODB.DataTypeEnum.adWChar Or Val(MyField.Type) = ADODB.DataTypeEnum.adBSTR Then
                    If IsDBNull(MyVal) Then
                        MyField.Value = ""
                    Else
                        If Len(MyVal) > MyField.DefinedSize Then
                            MyField.Value = Mid(MyVal, 1, MyField.DefinedSize)
                        Else
                            MyField.Value = MyVal
                        End If
                    End If
                Else
                    MyField.Value = MyVal
                End If
        End Select
    End Sub

    Private Function SplitWords(ByVal s As String) As String()
        '
        ' Call Regex.Split function from the imported namespace.
        ' Return the result array.
        '
        Return Regex.Split(s, "\W+")
    End Function


    Protected Sub ImageButton1_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles ImageButton1.Click
        Dim Vettore(100) As String

        Lbl_Errori.Text = ""
        If Not IsDate(Txt_DataDal.Text) Then
            ScriptManager.RegisterClientScriptBlock(Me, Me.GetType(), "Errore", "VisualizzaErrore('Data dal formalemente errata');", True)
            REM Lbl_Errori.Text = "Data dal formalemente errata"
            Exit Sub
        End If
        If Not IsDate(Txt_DataAl.Text) Then
            ScriptManager.RegisterClientScriptBlock(Me, Me.GetType(), "Errore", "VisualizzaErrore('Data dal formalemente errata');", True)
            REM Lbl_Errori.Text = "Data dal formalemente errata"
            Exit Sub
        End If

        Dim DataDal As Date
        Dim DataAl As Date
        DataDal = Txt_DataDal.Text
        DataAl = Txt_DataAl.Text

        If Format(DataDal, "yyyyMMdd") > Format(DataAl, "yyyyMMdd") Then
            ScriptManager.RegisterClientScriptBlock(Me, Me.GetType(), "Errore", "VisualizzaErrore('Data Dal Maggiore data Al');", True)

            Exit Sub
        End If


        If Trim(Txt_SottocontoDal.Text) = "" Then
            ScriptManager.RegisterClientScriptBlock(Me, Me.GetType(), "Errore", "VisualizzaErrore('Indicare sottoconto dal ');", True)
            REM Lbl_Errori.Text = "Indicare sottoconto dal "
            Exit Sub
        End If

        If Trim(Txt_SottocontoAl.Text) = "" Then
            ScriptManager.RegisterClientScriptBlock(Me, Me.GetType(), "Errore", "VisualizzaErrore('Indicare sottoconto al');", True)
            REM Lbl_Errori.Text = "Indicare sottoconto dal "
            Exit Sub
        End If

        Vettore = SplitWords(Txt_SottocontoDal.Text)

        If Vettore.Length < 2 Then
            ScriptManager.RegisterClientScriptBlock(Me, Me.GetType(), "Errore", "VisualizzaErrore('Indicare sottoconto dal');", True)
            REM Lbl_Errori.Text = "Indicare sottoconto dal "
            Exit Sub
        End If


        Vettore = SplitWords(Txt_SottocontoAl.Text)
        If Vettore.Length < 2 Then
            ScriptManager.RegisterClientScriptBlock(Me, Me.GetType(), "Errore", "VisualizzaErrore('Indicare sottoconto al');", True)
            REM Lbl_Errori.Text = "Indicare sottoconto al "
            Exit Sub
        End If

        Timer1.Enabled = True

        'DoWork(Session)

        Dim t As New Thread(New ParameterizedThreadStart(AddressOf DoWork))

        t.Start(Session)
    End Sub

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Trim(Session("UTENTE")) = "" Then
            Response.Redirect("../Login.aspx")
            Exit Sub
        End If
        Dim MyJs As String

        'MyJs = "$(" & Chr(34) & "#" & Txt_SottocontoDal.ClientID & Chr(34) & ").autocomplete('/WebHandler/GestioneConto.ashx?Utente=" & Session("UTENTE") & "', {delay:5,minChars:3});"

        'MyJs = "$(" & Chr(34) & ".Sottoconto" & Chr(34) & ").autocomplete('/WebHandler/GestioneConto.ashx?Utente=" & Session("UTENTE") & "', {delay:5,minChars:3});"
        'ScriptManager.RegisterStartupScript(Me, Me.GetType(), "ScriptS1", MyJs, True)


        'MyJs = "$(" & Chr(34) & "#" & Txt_SottocontoAl.ClientID & Chr(34) & ").autocomplete('/WebHandler/GestioneConto.ashx?Utente=" & Session("UTENTE") & "', {delay:5,minChars:3});"
        'ScriptManager.RegisterStartupScript(Me, Me.GetType(), "ScriptS2", MyJs, True)

        MyJs = "$(" & Chr(34) & "#" & Txt_DataDal.ClientID & Chr(34) & ").mask(""99/99/9999""); "
        MyJs = MyJs & " $(" & Chr(34) & "#" & Txt_DataDal.ClientID & Chr(34) & ").datepicker({ changeMonth: true,changeYear: true,  yearRange: ""1990:" & Year(Now) + 5 & """},$.datepicker.regional[""it""]);"

        ScriptManager.RegisterStartupScript(Me, Me.GetType(), "ScriptS3", MyJs, True)

        MyJs = "$(" & Chr(34) & "#" & Txt_DataAl.ClientID & Chr(34) & ").mask(""99/99/9999""); "
        MyJs = MyJs & " $(" & Chr(34) & "#" & Txt_DataAl.ClientID & Chr(34) & ").datepicker({ changeMonth: true,changeYear: true,  yearRange: ""1990:" & Year(Now) + 5 & """},$.datepicker.regional[""it""]);"
        ScriptManager.RegisterStartupScript(Me, Me.GetType(), "ScriptS4", MyJs, True)

        MyJs = "$(" & Chr(34) & "#" & Txt_SottocontoDal.ClientID & Chr(34) & ").blur(function() { $('#" & Txt_SottocontoAl.ClientID & "').val($(" & Chr(34) & "#" & Txt_SottocontoDal.ClientID & Chr(34) & ").val());}); "
        ScriptManager.RegisterStartupScript(Me, Me.GetType(), "ScriptS5", MyJs, True)



        MyJs = "$(document).ready(function() {"

        MyJs = MyJs & "var els = document.getElementsByTagName(""*"");"

        MyJs = MyJs & "for (var i=0;i<els.length;i++)"
        MyJs = MyJs & "if ( els[i].id ) { "
        MyJs = MyJs & " var appoggio =els[i].id;"
        MyJs = MyJs & " if (appoggio.match('Txt_Sottoconto')!= null) {  "
        MyJs = MyJs & " $(els[i]).autocomplete('/WebHandler/GestioneConto.ashx?Utente=" & Session("UTENTE") & "', {delay:5,minChars:3});"
        MyJs = MyJs & "    }"
        MyJs = MyJs & "} "
        MyJs = MyJs & "});"

        'MyJs = "$(document).ready(function() { $('.myClass').keypress(function() { handleEnter($('.myClass').val(), true, true); } );     $('#" & Txt_ImportoPagato.ClientID & "').blur(function() { var ap = formatNumber ($('#" & Txt_ImportoPagato.ClientID & "').val(),2); $('#" & Txt_ImportoPagato.ClientID & "').val(ap); }); });"
        ClientScript.RegisterClientScriptBlock(Me.GetType(), "visualizzaRitIm", MyJs, True)




        If Page.IsPostBack = True Then
            Exit Sub
        End If

        Dim K1 As New Cls_SqlString

        Dim Barra As New Cls_BarraSenior

        If Not IsNothing(Session("RicercaGeneraleSQLString")) Then
            K1 = Session("RicercaGeneraleSQLString")
        End If

        Lbl_BarraSenior.Text = Barra.CodiceBarra(Application("SENIOR"), Session("UTENTE"), Page, K1)

        Timer1.Enabled = False



        Dim kVilla As New Cls_TabelleDescrittiveOspitiAccessori


        kVilla.UpDateDropBox(Session("DC_OSPITIACCESSORI"), "VIL", DD_Struttura)
        If DD_Struttura.Items.Count = 1 Then
            Call AggiornaCServ()
        End If

        Dim Into As New Cls_DatiGenerali

        Into.LeggiDati(Session("DC_TABELLE"))

        If Into.AttivaCServPrimanoIncassi = 1 Then

            DD_CServ.Visible = True
            lblCentroservizio.Visible = True

            DD_Struttura.Visible = True
            lblStruttura.Visible = True
        End If

    End Sub

    Protected Sub Btn_Esci_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles Btn_Esci.Click

        Response.Redirect("Menu_Stampe.aspx")

    End Sub

    Protected Sub ImgMenu_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles ImgMenu.Click
        Response.Redirect("Menu_Generale.aspx")
 
    End Sub


    Protected Sub Timer1_Tick(ByVal sender As Object, ByVal e As System.EventArgs) Handles Timer1.Tick

        If Val(Cache("CampoProgressBar" + Session.SessionID)) = 0 Then
            If Cache("CampoProgressBar" + Session.SessionID) = "FINE" Then
                Cache("CampoProgressBar" + Session.SessionID) = ""

                Session("stampa") = Cache("stampa" + Session.SessionID)

                ScriptManager.RegisterClientScriptBlock(Me, Me.GetType(), "Stampa", "openPopUp('Stampa_ReportXSD.aspx?REPORT=MASTRINO','Stampe','width=800,height=600');", True)

                If Chk_StampaSaldi.Checked = True Then
                    ScriptManager.RegisterClientScriptBlock(Me, Me.GetType(), "Stampa1", "openPopUp('Stampa_ReportXSD.aspx?REPORT=SALDI','Stampe1','width=800,height=600');", True)
                End If
                Lbl_Waiting.Text = ""
                Exit Sub
            End If
        End If
        If Val(Cache("CampoProgressBar" + Session.SessionID)) > 0 Then
            Lbl_Waiting.Text = "<div id=""blur"">&nbsp;</div><div id=""pippo""  class=""wait"">"
            Lbl_Waiting.Text = Lbl_Waiting.Text & "<br/>"
            Lbl_Waiting.Text = Lbl_Waiting.Text & "<font size=""7"">" & Cache("CampoProgressBar" + Session.SessionID) & "%</font>"
            Lbl_Waiting.Text = Lbl_Waiting.Text & "<img  height=30px src=""images/loading.gif""><br />"
            Lbl_Waiting.Text = Lbl_Waiting.Text & "</div>"
        End If
    End Sub


    Private Sub AggiornaCServ()
        Dim kCsrv As New Cls_CentroServizio

        If DD_Struttura.SelectedValue = "" Then
            kCsrv.UpDateDropBox(Session("DC_OSPITE"), DD_CServ)
        Else
            kCsrv.UpDateDropBoxStruttura(Session("DC_OSPITE"), DD_CServ, DD_Struttura.SelectedValue)
        End If
    End Sub



    Protected Sub DD_Struttura_TextChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles DD_Struttura.TextChanged
        AggiornaCServ()
    End Sub
End Class


