﻿Imports Microsoft.VisualBasic
Imports System.Data.OleDb
Imports System.Web.Hosting
Imports System.Diagnostics
Imports System.ComponentModel
Imports System.Threading
Partial Class GeneraleWeb_StampaFatture
    Inherits System.Web.UI.Page

    Dim myRequest As System.Net.WebRequest
    Private Delegate Sub DoWorkDelegate()
    Private _work As DoWorkDelegate
    Private CampoTimer As String
    Private CampoProgressBar As String
    Private CampoErrori As String

    Protected Sub GeneraleWeb_StampaFatture_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Call EseguiJS()

        If Page.IsPostBack = True Then
            Exit Sub
        End If

        Dim Ks As New Cls_RegistroIVA

        Ks.UpDateDropBox(Session("DC_TABELLE"), DD_RegistroIVA)

        Dim kNote As New Cls_NoteFattura

        kNote.UpDateDropBox(Session("DC_GENERALE"), DD_NoteFattura)


        Timer1.Enabled = False


        Dim K1 As New Cls_SqlString

        Dim Barra As New Cls_BarraSenior

        If Not IsNothing(Session("RicercaAnagraficaSQLString")) Then
            Try
                K1 = Session("RicercaAnagraficaSQLString")
            Catch ex As Exception

            End Try
        End If

        Lbl_BarraSenior.Text = Barra.CodiceBarra(Application("SENIOR"), Session("UTENTE"), Page, K1)

        Call EseguiJS()


        Dim XS As New Cls_Login

        XS.Utente = Session("UTENTE")
        XS.LeggiSP(Application("SENIOR"))



        DD_Report.Items.Clear()

        DD_Report.Items.Add("")
        DD_Report.Items(DD_Report.Items.Count - 1).Value = ""

        If Trim(XS.ReportPersonalizzato("STAMPAFATTURACONTABILITA2")) <> "" Then
            DD_Report.Items.Add(XS.ReportPersonalizzato("STAMPAFATTURACONTABILITA2").Replace(".rpt", ""))
            DD_Report.Items(DD_Report.Items.Count - 1).Value = "STAMPAFATTURACONTABILITA2"
        End If

        If Trim(XS.ReportPersonalizzato("STAMPAFATTURACONTABILITA3")) <> "" Then
            DD_Report.Items.Add(XS.ReportPersonalizzato("STAMPAFATTURACONTABILITA3").Replace(".rpt", ""))
            DD_Report.Items(DD_Report.Items.Count - 1).Value = "STAMPAFATTURACONTABILITA3"
        End If

        If Trim(XS.ReportPersonalizzato("STAMPAFATTURACONTABILITA4")) <> "" Then
            DD_Report.Items.Add(XS.ReportPersonalizzato("STAMPAFATTURACONTABILITA4").Replace(".rpt", ""))
            DD_Report.Items(DD_Report.Items.Count - 1).Value = "STAMPAFATTURACONTABILITA4"
        End If

        If Trim(XS.ReportPersonalizzato("STAMPAFATTURACONTABILITA5")) <> "" Then
            DD_Report.Items.Add(XS.ReportPersonalizzato("STAMPAFATTURACONTABILITA5").Replace(".rpt", ""))
            DD_Report.Items(DD_Report.Items.Count - 1).Value = "STAMPAFATTURACONTABILITA5"
        End If

    End Sub



    Private Sub EseguiJS()
        Dim MyJs As String
        MyJs = "$(document).ready(function() {"

        MyJs = MyJs & "var els = document.getElementsByTagName(""*"");"

        MyJs = MyJs & "for (var i=0;i<els.length;i++)"
        MyJs = MyJs & "if ( els[i].id ) { "
        MyJs = MyJs & " var appoggio =els[i].id; "

        MyJs = MyJs & " if ((appoggio.match('Txt_DataInizio')!= null) ) {  "
        MyJs = MyJs & " $(els[i]).mask(""99/99/9999"");  "
        MyJs = MyJs & " $(els[i]).datepicker({ changeMonth: true,changeYear: true,  yearRange: ""1890:" & Year(Now) + 1 & """},$.datepicker.regional[""it""]);"
        MyJs = MyJs & "    }"


        MyJs = MyJs & " if ((appoggio.match('Txt_DataFine')!= null) ) {  "
        MyJs = MyJs & " $(els[i]).mask(""99/99/9999"");  "
        MyJs = MyJs & " $(els[i]).datepicker({ changeMonth: true,changeYear: true,  yearRange: ""1890:" & Year(Now) + 1 & """},$.datepicker.regional[""it""]);"
        MyJs = MyJs & "    }"

        MyJs = MyJs & "} "
        MyJs = MyJs & "});"

        ScriptManager.RegisterStartupScript(Me, Me.GetType(), "visualizzaRitIm", MyJs, True)
    End Sub


    Protected Sub Btn_Modifica_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles Btn_Modifica.Click
        Dim Appoggio As String
        If Val(Txt_DalDocumento.Text) = 0 Then
            Appoggio = "Numero Documento Dal obbligatorio"
            ClientScript.RegisterClientScriptBlock(Me.GetType(), "Errore", "VisualizzaErrore('" & Appoggio & "');", True)
            Exit Sub
        End If
        If Val(Txt_AlDocumento.Text) = 0 Then
            Appoggio = "Numero Documento al obbligatorio"
            ClientScript.RegisterClientScriptBlock(Me.GetType(), "Errore", "VisualizzaErrore('" & Appoggio & "');", True)
            Exit Sub
        End If
        If Not IsDate(Txt_DataInizio.Text) Then
            Appoggio = "Data Dal Errata"
            ClientScript.RegisterClientScriptBlock(Me.GetType(), "Errore", "VisualizzaErrore('" & Appoggio & "');", True)
            Exit Sub
        End If
        If Not IsDate(Txt_DataFine.Text) Then
            Appoggio = "Data Al Errata"
            ClientScript.RegisterClientScriptBlock(Me.GetType(), "Errore", "VisualizzaErrore('" & Appoggio & "');", True)
            Exit Sub
        End If

        If DD_RegistroIVA.SelectedValue = 0 Then
            Appoggio = "Registro Iva obbligatorio"
            ClientScript.RegisterClientScriptBlock(Me.GetType(), "Errore", "VisualizzaErrore('" & Appoggio & "');", True)
            Exit Sub
        End If

        Timer1.Enabled = True
        Session("CampoProgressBar") = 0
        Session("RagioneSocialeFattura") = ""
        Cache("CampoProgressBar" + Session.SessionID) = 0
        Cache("RagioneSocialeFattura" + Session.SessionID) = ""


        REM DoWork(Session)

        REM Exit Sub
        Dim t As New Thread(New ParameterizedThreadStart(AddressOf DoWork))

        t.Start(Session)
    End Sub

    Private Sub DoWork(ByVal data As Object)
        Dim k As New Cls_StampaFattureContabilita

        Dim DataInizio As Date
        Dim DataFine As Date

        If IsDate(Txt_DataInizio.Text) And Txt_DataInizio.Text <> "" Then
            DataInizio = Txt_DataInizio.Text
        Else
            DataInizio = Nothing
        End If

        If IsDate(Txt_DataFine.Text) And Txt_DataFine.Text <> "" Then
            DataFine = Txt_DataFine.Text
        Else
            DataFine = Nothing
        End If

        Dim testo As String = ""

        If DD_NoteFattura.SelectedValue <> "" Then
            Dim KNote As New Cls_NoteFattura

            KNote.Codice = DD_NoteFattura.SelectedValue
            KNote.Leggi(Session("DC_GENERALE"))

            testo = KNote.Note
        End If

        k.CreaStampa(DataInizio, DataFine, Val(Txt_DalDocumento.Text), Val(Txt_AlDocumento.Text), DD_RegistroIVA.SelectedValue, data, testo)
    End Sub




    Protected Sub Timer1_Tick(ByVal sender As Object, ByVal e As System.EventArgs) Handles Timer1.Tick
        If Val(Cache("CampoProgressBar" + Session.SessionID)) = 999 Then
            Cache("CampoProgressBar" + Session.SessionID) = 0
            Lbl_Waiting.Text = "<p align=left>" & Cache("CampoErrori" + Session.SessionID) & "</p>"
            Exit Sub
        End If

        If Val(Cache("CampoProgressBar" + Session.SessionID)) > 100 Then
            Lbl_Waiting.Text = ""
            Timer1.Enabled = False


            Session("stampa") = Cache("stampa" + Session.SessionID)

            Session("SelectionFormula") = "{FatturaTesta.PRINTERKEY} = " & Chr(34) & Session.SessionID & Chr(34) & " AND " & "{FatturaRighe.PRINTERKEY} = " & Chr(34) & Session.SessionID & Chr(34)

            Dim XS As New Cls_Login

            XS.Utente = Session("UTENTE")
            XS.LeggiSP(Application("SENIOR"))


            If DD_Report.SelectedValue = "" Then
                ScriptManager.RegisterClientScriptBlock(Me, Me.GetType(), "Stampa1", "openPopUp('Stampa_ReportXSD.aspx?REPORT=STAMPAFATTURACONTABILITA&PRINTERKEY=ON','Stampe1','width=800,height=600');", True)
            Else
                ScriptManager.RegisterClientScriptBlock(Me, Me.GetType(), "Stampa1", "openPopUp('Stampa_ReportXSD.aspx?REPORT=" & DD_Report.SelectedValue & "&PRINTERKEY=ON','Stampe1','width=800,height=600');", True)
            End If


            Exit Sub
        End If
        If Val(Cache("CampoProgressBar" + Session.SessionID)) > 0 Then
            Lbl_Waiting.Text = "<div id=""blur"">&nbsp;</div><div id=""pippo""  class=""wait"">"
            Lbl_Waiting.Text = Lbl_Waiting.Text & "<font size=""7"">" & Session("CampoProgressBar") & "%</font>"
            Lbl_Waiting.Text = Lbl_Waiting.Text & "<img  height=30px src=""images/loading.gif""><br />"
            Lbl_Waiting.Text = Lbl_Waiting.Text & "<br />"
            Lbl_Waiting.Text = Lbl_Waiting.Text & "<font color=""007dc4"">" & Session("RagioneSocialeFattura") & " </font><br />"
            Lbl_Waiting.Text = Lbl_Waiting.Text & "</div>"
        End If
    End Sub

    Protected Sub Btn_Esci_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles Btn_Esci.Click
        Response.Redirect("Menu_Stampe.aspx")
    End Sub
End Class
