﻿Imports System.Web.Hosting
Imports System.Data.OleDb
Partial Class GeneraleWeb_VisualizzaCig
    Inherits System.Web.UI.Page
    Dim Tabella As New System.Data.DataTable("tabella")
    
    Function campodbN(ByVal oggetto As Object) As Double
        If IsDBNull(oggetto) Then
            Return 0
        Else
            Return oggetto
        End If
    End Function
    Function campodb(ByVal oggetto As Object) As String
        If IsDBNull(oggetto) Then
            Return ""
        Else
            Return oggetto
        End If
    End Function
    Private Function SplitWords(ByVal s As String) As String()
        '
        ' Call Regex.Split function from the imported namespace.
        ' Return the result array.
        '
        Return Regex.Split(s, "\W+")
    End Function

    Private Sub Esegui(ByVal ToExcel As Boolean)
        Dim MySql As String 
        Dim Condizione As String 
             Dim DATE1 As Date = Txt_DataDal.Text
        Dim DATE2 As Date = Txt_DataAl.Text

        Condizione = ""
        If IsDate(Txt_DataDal.Text) Then
            Condizione = " And Scadenzario.DataScadenza >= {ts '" & Format(DATE1, "yyyy-MM-dd") & " 00:00:00'} And Scadenzario.DataScadenza <= {ts '" & Format(DATE2, "yyyy-MM-dd") & " 00:00:00'}"
        End If

        If Trim(Txt_Sottoconto.Text) <> "" Then
            Dim Vettore(100) As String
            Dim APPOGGIO As String

            APPOGGIO = Txt_Sottoconto.Text
            Vettore = SplitWords(APPOGGIO)

            If Vettore.Length > 0 Then
                If Val(Vettore(0)) > 0 Then
                    If Condizione <> "" Then
                        Condizione = Condizione & " AND "
                    End If
                    Condizione = Condizione & " MovimentiContabiliRiga.MastroPartita = " & Val(Vettore(0))
                End If
            End If
            If Vettore.Length > 1 Then
                If Val(Vettore(1)) > 0 Then
                    If Condizione <> "" Then
                        Condizione = Condizione & " AND "
                    End If
                    Condizione = Condizione & " MovimentiContabiliRiga.ContoPartita = " & Val(Vettore(1))
                End If
            End If

            If Vettore.Length > 2 Then
                If Val(Vettore(2)) > 0 Then
                    If Condizione <> "" Then
                        Condizione = Condizione & " AND "
                    End If
                    Condizione = Condizione & " MovimentiContabiliRiga.SottoContoPartita = " & Val(Vettore(2))
                End If
            End If
        End If



        Tabella.Clear()
        Tabella.Columns.Clear()
        Tabella.Columns.Add("Cig", GetType(String))
        Tabella.Columns.Add("Data Inizio", GetType(String))
        Tabella.Columns.Add("Data Fine", GetType(String))
        Tabella.Columns.Add("NumeroRegistrazione", GetType(String))        
        Tabella.Columns.Add("Numero Protocollo", GetType(String))
        Tabella.Columns.Add("Numero Documento", GetType(String))
        Tabella.Columns.Add("Data Documento", GetType(String))
        Tabella.Columns.Add("Importo", GetType(String))



        MySql = "SELECT MovimentiContabiliRiga.Numero,Scadenzario.Numero as ScNumero, Scadenzario.Importo, Scadenzario.DataScadenza, MovimentiContabiliRiga.MastroPartita, MovimentiContabiliRiga.ContoPartita, MovimentiContabiliRiga.SottocontoPartita,MovimentiContabiliTesta.NumeroProtocollo, MovimentiContabiliTesta.NumeroDocumento, MovimentiContabiliTesta.DataDocumento, (Select Sum(Importo) from TabellaLegami where CodiceDocumento = NumeroRegistrazione )  as ImportoPagato, Scadenzario.Chiusa,Scadenzario.Descrizione, MovimentiContabiliTesta.CodiceCig " & _
                " FROM (Scadenzario INNER JOIN MovimentiContabiliRiga ON Scadenzario.NumeroRegistrazioneContabile = MovimentiContabiliRiga.Numero) " & _
                " INNER JOIN MovimentiContabiliTesta ON MovimentiContabiliRiga.Numero = MovimentiContabiliTesta.NumeroRegistrazione " & _
                " WHERE ((MovimentiContabiliRiga.Tipo ='CF' And NumeroProtocollo > 0)  Or (NumeroProtocollo = 0)) " & Condizione & " Order by CodiceCig,DataRegistrazione"



       
        Dim cn As OleDbConnection
        Dim RotturaCig as String =""
        Dim TotaleARottura as Double =0
        Dim TotaleBudget as Double = 0

        cn = New Data.OleDb.OleDbConnection(Session("DC_GENERALE"))

        cn.Open()
        Dim cmd As New OleDbCommand()

        cmd.CommandText = (MySql)

        cmd.Connection = cn
        Dim ReadCig As OleDbDataReader = cmd.ExecuteReader()
        Do While ReadCig.Read
             if RotturaCig <> campodb(ReadCig.Item("CodiceCig")) then
                Dim RigaTotale As System.Data.DataRow = Tabella.NewRow()
                RigaTotale(0) = "Totale Budget "
                RigaTotale(1) = TotaleBudget
                RigaTotale(2) = "Importo usato"
                RigaTotale(3) = TotaleARottura
                RigaTotale(4) = "Disponibile "
                RigaTotale(5) = TotaleBudget - TotaleARottura
                Tabella.Rows.Add(RigaTotale)

                TotaleARottura=0
                TotaleBudget=0
             End If
             Dim myriga As System.Data.DataRow = Tabella.NewRow()
             myriga(0) = campodb(ReadCig.Item("CodiceCig"))
              
             Dim CF As New Cls_ClienteFornitore

            CF.CODICEDEBITORECREDITORE=0
            CF.Nome =""
            CF.MastroFornitore = campodbn(ReadCig.Item("MastroPartita"))
            CF.ContoFornitore = campodbn(ReadCig.Item("ContoPartita"))
            CF.SottoContoFornitore = campodbn(ReadCig.Item("SottocontoPartita"))
            CF.Leggi(Session("DC_OSPITE"))

            If  CF.CODICEDEBITORECREDITORE <> 0 Then
                  Dim DatiCig As New Cls_CigClientiFornitori

                DatiCig.CodiceDebitoreCreditore = CF.CODICEDEBITORECREDITORE
                DatiCig.LeggiCig(Session("DC_GENERALE"), CF.CODICEDEBITORECREDITORE, campodb(ReadCig.Item("CodiceCig")))

                IF IsDate(DatiCig.DataInizio) THEN
                    myriga(1) = format(DatiCig.DataInizio,"dd/MM/yyyy")
                End If
                IF IsDate(DatiCig.DataInizio) THEN
                    myriga(2) = format(DatiCig.DataInizio,"dd/MM/yyyy")
                End If

                TotaleBudget=DatiCig.ImportoBudget
            End If
            Dim Registrazione as new Cls_MovimentoContabile
            Dim ImportoTotale as Double = 0

            Registrazione.NumeroRegistrazione =  campodbn(ReadCig.Item("MastroPartita"))
            Registrazione.Leggi(Session("DC_GENERALE"), Registrazione.NumeroRegistrazione)

            ImportoTotale = Registrazione.ImportoDocumento(Session("DC_TABELLE"))


            myriga(3) = Registrazione.NumeroRegistrazione
            myriga(4) = Registrazione.NumeroProtocollo
            myriga(5) = Registrazione.NumeroDocumento
            myriga(6) = Format(Registrazione.DataDocumento,"dd/MM/yyyy")
            myriga(7) = Format(ImportoTotale,"#,##0.00") 

            Tabella.Rows.Add(myriga)

            TotaleARottura += ImportoTotale

            RotturaCig = campodb(ReadCig.Item("CodiceCig"))
            
        Loop
        ReadCig.Close

        cn.Close

        try

        Catch ex As Exception
             ex.Message.Replace("'","")
        End Try
    End Sub


    Protected Sub Grid_RowCommand(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewCommandEventArgs) Handles Grid.RowCommand
        If e.CommandName = "Seleziona" Then
            Dim ConnectionString As String = Session("DC_OSPITE")
            Dim d As Integer

            If ViewState("XLINEA") <> -1 Then
                Grid.Rows(ViewState("XLINEA")).BackColor = ViewState("XBACKUP")
            End If


            Tabella = Session("Scadenziario")
            d = Val(e.CommandArgument)
            Session("NumeroRegistrazione") = Tabella.Rows(d).Item(0).ToString



            ViewState("XLINEA") = d
            ViewState("XBACKUP") = Grid.Rows(d).BackColor
            Grid.Rows(d).BackColor = Drawing.Color.Violet
        End If

        If (e.CommandName = "Richiama") Then
            Dim Registrazione As Long
            Dim d As Integer

            Tabella = Session("Scadenziario")
            d = Val(e.CommandArgument)
            Registrazione = Tabella.Rows(d).Item(0).ToString



            Dim Reg As New Cls_MovimentoContabile

            Reg.Leggi(Session("DC_GENERALE"), Registrazione)
            Dim CauCon As New Cls_CausaleContabile

            CauCon.Leggi(Session("DC_TABELLE"), Reg.CausaleContabile)
            If CauCon.Tipo = "I" Or CauCon.Tipo = "R" Then
                Session("NumeroRegistrazione") = Registrazione
                ClientScript.RegisterClientScriptBlock(Me.GetType(), "MyBox", "$(window).load(function() {  DialogBox('../GeneraleWeb/Documenti.aspx');  });", True)
                Exit Sub
            End If
            If CauCon.Tipo = "P" Then
                Session("NumeroRegistrazione") = Registrazione
                ClientScript.RegisterClientScriptBlock(Me.GetType(), "MyBox", "$(window).load(function() {  DialogBox('../GeneraleWeb/incassipagamenti.aspx');  });", True)
                Exit Sub
            End If
            Session("NumeroRegistrazione") = Registrazione
            ClientScript.RegisterClientScriptBlock(Me.GetType(), "MyBox", "$(window).load(function() {  DialogBox('../GeneraleWeb/primanota.aspx');  });", True)
            Exit Sub
        End If
    End Sub

    Private Sub GeneraleWeb_VisualizzaCig_Load(sender As Object, e As EventArgs) Handles Me.Load
        if Trim(Session("UTENTE")) = "" Then
            If Request.ServerVariables("URL").ToUpper.IndexOf("SENIORWEBBLANCO") > 0 Then
                Response.Redirect("/ODV/Login.aspx")
                Exit Sub
            Else
                Response.Redirect("/SeniorWeb/Login.aspx")
                Exit Sub
            End If
        End If


        Call EseguiJS()

        Dim K1 As New Cls_SqlString

        Dim Barra As New Cls_BarraSenior


        Lbl_BarraSenior.Text = Barra.CodiceBarra(Application("SENIOR"), Session("UTENTE"), Page, K1)
    End Sub

   Private Sub EseguiJS()
        Dim MyJs As String
        MyJs = "$(document).ready(function() {"

        MyJs = MyJs & "var els = document.getElementsByTagName(""*"");"

        MyJs = MyJs & "for (var i=0;i<els.length;i++)"
        MyJs = MyJs & "if ( els[i].id ) { "
        MyJs = MyJs & " var appoggio =els[i].id; "
        MyJs = MyJs & " if (appoggio.match('Txt_Sottoconto')!= null) {  "
        MyJs = MyJs & " $(els[i]).autocomplete('/WebHandler/GestioneConto.ashx?Utente=" & Session("UTENTE") & "', {delay:5,minChars:3});"
        MyJs = MyJs & "    }"

        MyJs = MyJs & " if (appoggio.match('Txt_Data')!= null) {  "
        MyJs = MyJs & " $(els[i]).mask(""99/99/9999"");  "
        MyJs = MyJs & " $(els[i]).datepicker({ changeMonth: true,changeYear: true,  yearRange: ""1890:" & Year(Now) + 1 & """},$.datepicker.regional[""it""]);"
        MyJs = MyJs & "    }"

        MyJs = MyJs & " if (appoggio.match('Txt_Importo')!= null)  {  "
        MyJs = MyJs & " $(els[i]).keypress(function() { ForceNumericInput($(this).val(), true, true); } ); "
        MyJs = MyJs & " $(els[i]).blur(function() { var ap = formatNumber($(this).val(),2); $(this).val(ap); });"
        MyJs = MyJs & "    }"

        MyJs = MyJs & "} "
        MyJs = MyJs & "});"

        'MyJs = "$(document).ready(function() { $('.myClass').keypress(function() { handleEnter($('.myClass').val(), true, true); } );     $('#" & Txt_ImportoPagato.ClientID & "').blur(function() { var ap = formatNumber ($('#" & Txt_ImportoPagato.ClientID & "').val(),2); $('#" & Txt_ImportoPagato.ClientID & "').val(ap); }); });"
        ScriptManager.RegisterStartupScript(Me, Me.GetType(), "visualizzaRitIm", MyJs, True)
    End Sub

    Private Sub Img_Esegui_Click(sender As Object, e As ImageClickEventArgs) Handles Img_Esegui.Click
        If Not IsDate(Txt_DataDal.Text) Then
            ScriptManager.RegisterStartupScript(Me, Me.GetType(), "visualizzaA", "VisualizzaErrore('Data dal formalemente errata');", True)
            Exit Sub
        End If
        If Not IsDate(Txt_DataAl.Text) Then
            ScriptManager.RegisterStartupScript(Me, Me.GetType(), "visualizzaA", "VisualizzaErrore('Data al formalemente errata');", True)
            Exit Sub
        End If


        Call Esegui(False)
    End Sub
End Class
