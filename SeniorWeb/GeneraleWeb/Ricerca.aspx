﻿<%@ Page Language="VB" AutoEventWireup="false" Inherits="Ricerca" MaintainScrollPositionOnPostback="true" CodeFile="Ricerca.aspx.vb" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="xasp" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
    <meta http-equiv="x-ua-compatible" content="IE=9" />
    <title>Ricerca</title>
    <asp:PlaceHolder runat="server">
        <%: System.Web.Optimization.Styles.Render("~/Content/AjaxControlToolkit/Styles/Bundle") %>
    </asp:PlaceHolder>
    <link href="ospiti.css?ver=11" rel="stylesheet" type="text/css" />
    <link rel="shortcut icon" href="images/SENIOR.ico" />
    <link href="js/jquery.autocomplete.css" rel="stylesheet" type="text/css" />

    <script src="js/jquery-1.5.1.min.js" type="text/javascript"></script>
    <script src="js/jquery.autocomplete.js?ver=4" type="text/javascript"></script>
    <script src="js/soapclient.js" type="text/javascript"></script>
    <script src="/js/convertinumero.js" type="text/javascript"></script>

    <script src="js/jquery.maskedinput-1.3.min.js" type="text/javascript"></script>
    <script src="/js/formatnumer.js" type="text/javascript"></script>

    <script src="/js/pianoconti.js" type="text/javascript"></script>
    <script src="js/JSErrore.js" type="text/javascript"></script>
    <script src="js/NoEnter.js" type="text/javascript"></script>
    <script src="js/chosen/chosen.jquery.js" type="text/javascript"></script>
    <script src="js/chosen/docsupport/prism.js" type="text/javascript" charset="utf-8"></script>


    <link rel="stylesheet" href="js/chosen/docsupport/style.css">
    <link rel="stylesheet" href="js/chosen/docsupport/prism.css">
    <link rel="stylesheet" href="js/chosen/chosen.css">

    <link rel="stylesheet" href="dialogbox/redips-dialog.css" type="text/css" media="screen" />
    <script type="text/javascript" src="dialogbox/redips-dialog-min.js"></script>
    <script type="text/javascript" src="dialogbox/script.js"></script>

    <link rel="stylesheet" href="jqueryui/jquery-ui.css" type="text/css" />
    <script src="jqueryui/jquery-ui.js" type="text/javascript"></script>
    <script src="jqueryui/datapicker-it.js" type="text/javascript"></script>
    <script type="text/javascript">
        function SalvaInStorage() {

            localStorage.setItem("Chk_Campi_0", $("#TabContainer1_Tab_Anagrafica_Chk_Campi_0").is(':checked'));
            localStorage.setItem("Chk_Campi_1", $("#TabContainer1_Tab_Anagrafica_Chk_Campi_1").is(':checked'));
            localStorage.setItem("Chk_Campi_2", $("#TabContainer1_Tab_Anagrafica_Chk_Campi_2").is(':checked'));
            localStorage.setItem("Chk_Campi_3", $("#TabContainer1_Tab_Anagrafica_Chk_Campi_3").is(':checked'));
            localStorage.setItem("Chk_Campi_4", $("#TabContainer1_Tab_Anagrafica_Chk_Campi_4").is(':checked'));
            localStorage.setItem("Chk_Campi_5", $("#TabContainer1_Tab_Anagrafica_Chk_Campi_5").is(':checked'));
            localStorage.setItem("Chk_Campi_6", $("#TabContainer1_Tab_Anagrafica_Chk_Campi_6").is(':checked'));
            localStorage.setItem("Chk_Campi_7", $("#TabContainer1_Tab_Anagrafica_Chk_Campi_7").is(':checked'));
            localStorage.setItem("Chk_Campi_8", $("#TabContainer1_Tab_Anagrafica_Chk_Campi_8").is(':checked'));
            localStorage.setItem("Chk_Campi_9", $("#TabContainer1_Tab_Anagrafica_Chk_Campi_9").is(':checked'));
            localStorage.setItem("Chk_Campi_10", $("#TabContainer1_Tab_Anagrafica_Chk_Campi_10").is(':checked'));
            localStorage.setItem("Chk_Campi_11", $("#TabContainer1_Tab_Anagrafica_Chk_Campi_11").is(':checked'));
            localStorage.setItem("Chk_Campi_12", $("#TabContainer1_Tab_Anagrafica_Chk_Campi_12").is(':checked'));
            localStorage.setItem("Chk_Campi_13", $("#TabContainer1_Tab_Anagrafica_Chk_Campi_13").is(':checked'));
            localStorage.setItem("Chk_Campi_14", $("#TabContainer1_Tab_Anagrafica_Chk_Campi_14").is(':checked'));

            return true;
        }

        $(document).ready(function () {
            if (localStorage.Chk_Campi_0 == null) {
                $('#TabContainer1_Tab_Anagrafica_Chk_Campi_0').attr('checked', true);
                $('#TabContainer1_Tab_Anagrafica_Chk_Campi_1').attr('checked', true);
                $('#TabContainer1_Tab_Anagrafica_Chk_Campi_2').attr('checked', true);
                $('#TabContainer1_Tab_Anagrafica_Chk_Campi_3').attr('checked', true);
                $('#TabContainer1_Tab_Anagrafica_Chk_Campi_4').attr('checked', true);
                $('#TabContainer1_Tab_Anagrafica_Chk_Campi_5').attr('checked', true);
                $('#TabContainer1_Tab_Anagrafica_Chk_Campi_6').attr('checked', true);
                $('#TabContainer1_Tab_Anagrafica_Chk_Campi_7').attr('checked', true);
                $('#TabContainer1_Tab_Anagrafica_Chk_Campi_8').attr('checked', true);
                $('#TabContainer1_Tab_Anagrafica_Chk_Campi_9').attr('checked', true);
                $('#TabContainer1_Tab_Anagrafica_Chk_Campi_10').attr('checked', true);
                $('#TabContainer1_Tab_Anagrafica_Chk_Campi_11').attr('checked', true);
                $('#TabContainer1_Tab_Anagrafica_Chk_Campi_12').attr('checked', true);
                $('#TabContainer1_Tab_Anagrafica_Chk_Campi_13').attr('checked', true);
                $('#TabContainer1_Tab_Anagrafica_Chk_Campi_14').attr('checked', true);
            }

            if (localStorage.Chk_Campi_0 == "true") {
                $('#TabContainer1_Tab_Anagrafica_Chk_Campi_0').attr('checked', localStorage.Chk_Campi_0);
            }
            if (localStorage.Chk_Campi_1 == "true") {
                $('#TabContainer1_Tab_Anagrafica_Chk_Campi_1').attr('checked', localStorage.Chk_Campi_1);
            }
            if (localStorage.Chk_Campi_2 == "true") {
                $('#TabContainer1_Tab_Anagrafica_Chk_Campi_2').attr('checked', localStorage.Chk_Campi_2);
            }
            if (localStorage.Chk_Campi_3 == "true") {
                $('#TabContainer1_Tab_Anagrafica_Chk_Campi_3').attr('checked', localStorage.Chk_Campi_3);
            }
            if (localStorage.Chk_Campi_4 == "true") {
                $('#TabContainer1_Tab_Anagrafica_Chk_Campi_4').attr('checked', localStorage.Chk_Campi_4);
            }
            if (localStorage.Chk_Campi_5 == "true") {
                $('#TabContainer1_Tab_Anagrafica_Chk_Campi_5').attr('checked', localStorage.Chk_Campi_5);
            }
            if (localStorage.Chk_Campi_6 == "true") {
                $('#TabContainer1_Tab_Anagrafica_Chk_Campi_6').attr('checked', localStorage.Chk_Campi_6);
            }
            if (localStorage.Chk_Campi_7 == "true") {
                $('#TabContainer1_Tab_Anagrafica_Chk_Campi_7').attr('checked', localStorage.Chk_Campi_7);
            }
            if (localStorage.Chk_Campi_8 == "true") {
                $('#TabContainer1_Tab_Anagrafica_Chk_Campi_8').attr('checked', localStorage.Chk_Campi_8);
            }
            if (localStorage.Chk_Campi_9 == "true") {
                $('#TabContainer1_Tab_Anagrafica_Chk_Campi_9').attr('checked', localStorage.Chk_Campi_9);
            }
            if (localStorage.Chk_Campi_10 == "true") {
                $('#TabContainer1_Tab_Anagrafica_Chk_Campi_10').attr('checked', localStorage.Chk_Campi_10);
            }
            if (localStorage.Chk_Campi_11 == "true") {
                $('#TabContainer1_Tab_Anagrafica_Chk_Campi_11').attr('checked', localStorage.Chk_Campi_11);
            }
            if (localStorage.Chk_Campi_12 == "true") {
                $('#TabContainer1_Tab_Anagrafica_Chk_Campi_12').attr('checked', localStorage.Chk_Campi_12);
            }
            if (localStorage.Chk_Campi_13 == "true") {
                $('#TabContainer1_Tab_Anagrafica_Chk_Campi_13').attr('checked', localStorage.Chk_Campi_13);
            }
            if (localStorage.Chk_Campi_14 == "true") {
                $('#TabContainer1_Tab_Anagrafica_Chk_Campi_14').attr('checked', localStorage.Chk_Campi_14);
            }

            if (window.innerHeight > 0) { $("#BarraLaterale").css("height", (window.innerHeight - 105) + "px"); } else { $("#BarraLaterale").css("height", (document.documentElement.offsetHeight - 105) + "px"); }
        });
    </script>
    <style>
        #blur {
            width: 100%;
            background-color: black;
            moz-opacity: 0.5;
            khtml-opacity: .5;
            opacity: .5;
            filter: alpha(opacity=50);
            z-index: 120;
            height: 100%;
            position: absolute;
            top: 0;
            left: 0;
        }

        #progress {
            z-index: 200;
            background-color: White;
            position: absolute;
            top: 0pt;
            left: 0pt;
            border: solid 1px black;
            padding: 5px 5px 5px 5px;
            text-align: center;
        }
    </style>
    <script type="text/javascript">               

        function DialogBox(Path) {

            var winW = 630, winH = 460;
            if (document.body && document.body.offsetWidth) {
                winW = document.body.offsetWidth;
                winH = document.body.offsetHeight;
            }
            if (document.compatMode == 'CSS1Compat' &&
                document.documentElement &&
                document.documentElement.offsetWidth) {
                winW = document.documentElement.offsetWidth;
                winH = document.documentElement.offsetHeight;
            }
            if (window.innerWidth && window.innerHeight) {
                winW = window.innerWidth;
                winH = window.innerHeight;
            }

            var APPO = winH - 100;

            REDIPS.dialog.show(winW - 200, winH - 100, '<iframe id="output" src="' + Path + '" height="' + APPO + '" width="100%" ></iframe>');
            return false;

        }
    </script>

</head>
<body>
    <form id="form1" runat="server" autocomplete="off">
        <asp:ScriptManager ID="ScriptManager1" runat="server" AsyncPostBackTimeout="900" EnableScriptGlobalization="true">
            <Scripts>
                <asp:ScriptReference Path="~/Scripts/AjaxControlToolkit/Bundle" />
            </Scripts>
        </asp:ScriptManager>
        <div align="left">
            <asp:Label ID="Lbl_BarraSenior" runat="server" Text=""></asp:Label>
            <table style="width: 100%;" cellpadding="0" cellspacing="0">
                <tr>
                    <td style="width: 160px; background-color: #F0F0F0;"></td>
                    <td>
                        <div class="Titolo">Contabilità - Principale - Ricerca</div>
                        <div class="SottoTitolo">
                            <br />
                            <br />
                        </div>
                    </td>
                    <td style="text-align: right; vertical-align: top;">
                        <div class="DivTasti">
                            <asp:ImageButton ID="ImageButton1" runat="server" Height="38px" ImageUrl="~/images/esegui.png" class="EffettoBottoniTondi" OnClientClick="return SalvaInStorage();" ToolTip="Visualizza Dati" Width="38px" />
                            <asp:ImageButton ID="ImageButton2" runat="server" Height="38px" ImageUrl="~/images/Excel.png" class="EffettoBottoniTondi" OnClientClick="return SalvaInStorage();" ToolTip="Esporta in excel" Style="width: 38px" />
                        </div>
                    </td>
                </tr>
            </table>
            <table style="border-width: 0px;" cellspacing="0" cellpadding="0">
                <tr>

                    <td style="width: 160px; background-color: #F0F0F0; vertical-align: top; text-align: center;" id="BarraLaterale">
                        <a href="Menu_Generale.aspx" style="border-width: 0px;">
                            <img src="images/Home.jpg" alt="Menù" class="Effetto" /></a><br />
                        <asp:ImageButton ID="Btn_Esci" runat="server" BackColor="Transparent" ImageUrl="../images/Menu_Indietro.png" class="Effetto" ToolTip="Chiudi" />
                    </td>

                    <td colspan="2" style="background-color: #FFFFFF; vertical-align: top;">
                        <xasp:TabContainer ID="TabContainer1" runat="server" ActiveTabIndex="0" CssClass="TabSenior" Width="100%" BorderStyle="None" Style="margin-right: 39px">
                            <xasp:TabPanel runat="server" HeaderText="Prima Nota" Width="100%" ID="Tab_Anagrafica">
                                <HeaderTemplate>
                                    Ricerca Registrazioni
                                </HeaderTemplate>
                                <ContentTemplate>

                                    <table>
                                        <tr>
                                            <td>
                                                <label class="LabelCampo">Descrizione:</label>
                                                <asp:TextBox ID="Txt_Descrizione" onkeypress="return handleEnter(this, event)"
                                                    runat="server" Width="398px"></asp:TextBox>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                <label class="LabelCampo">Centro Servizio:</label>
                                                <asp:DropDownList ID="DD_CSERV" onkeypress="return handleEnter(this, event)" class="chosen-select" runat="server" Width="400px"></asp:DropDownList>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                <label class="LabelCampo">Causale Contabile:</label>
                                                <asp:DropDownList ID="DD_CausaleContabile" onkeypress="return handleEnter(this, event)" class="chosen-select" runat="server" Width="400px"></asp:DropDownList><asp:Button ID="Btn_Piu1" runat="server" Text="+" />
                                                <asp:DropDownList ID="DD_CausaleContabile1" onkeypress="return handleEnter(this, event)" class="chosen-select" runat="server" Width="400px" Visible="false"></asp:DropDownList><asp:Button ID="Btn_Piu2" runat="server" Text="+" Visible="false" />
                                                <asp:DropDownList ID="DD_CausaleContabile2" onkeypress="return handleEnter(this, event)" class="chosen-select" runat="server" Width="400px" Visible="false"></asp:DropDownList><asp:Button ID="Btn_Piu3" runat="server" Text="+" Visible="false" />
                                                <asp:DropDownList ID="DD_CausaleContabile3" onkeypress="return handleEnter(this, event)" class="chosen-select" runat="server" Width="400px" Visible="false"></asp:DropDownList>
                                            </td>
                                        </tr>
                                        <td>
                                            <label class="LabelCampo">Registro IVA:</label>
                                            <asp:DropDownList ID="DD_RegistroIVA"
                                                onkeypress="return handleEnter(this, event)" runat="server" Width="401px">
                                            </asp:DropDownList><br />
                                        </td>
                                        </tr>
     
     <tr>
         <td>
             <label class="LabelCampo">Aliquota IVA:</label>
             <asp:DropDownList ID="DD_IVA" onkeypress="return handleEnter(this, event)" class="chosen-select" runat="server" Width="400px"></asp:DropDownList>
         </td>
     </tr>

                                        <tr>
                                            <td>
                                                <label class="LabelCampo">Detraibilita :</label>
                                                <asp:DropDownList ID="DD_Detraibilita" onkeypress="return handleEnter(this, event)" class="chosen-select" runat="server" Width="400px"></asp:DropDownList>
                                            </td>
                                        </tr>
                                    </table>
                                    <table>
                                        <tr>
                                            <td>
                                                <label class="LabelCampo">Data Bolletta:</label>
                                                <asp:TextBox ID="Txt_DataBolletta" onkeypress="return handleEnter(this, event)" runat="server" Width="90px"></asp:TextBox>
                                            </td>
                                            <td>
                                                <label class="LabelCampo">Numero Bolletta:</label>
                                                <asp:TextBox ID="Txt_NumeroBolletta" onkeypress="return handleEnter(this, event)" runat="server" Width="120px"></asp:TextBox>
                                            </td>
                                        </tr>
                                    </table>

                                    <label class="LabelCampo">Conto Contabilità:</label>
                                    <asp:TextBox ID="Txt_Sottoconto" onkeypress="return handleEnter(this, event)"
                                        CssClass="MyAutoComplete" runat="server" Width="402px"></asp:TextBox>
                                    <asp:CheckBox ID="Chk_ControPartita" runat="server" Text="Contro Partita" /><br />
                                    <table>
                                        <tr>
                                            <td>
                                                <label class="LabelCampo">Data Dal:</label>
                                                <asp:TextBox ID="Txt_DataDal" onkeypress="return handleEnter(this, event)" runat="server" Width="90px"></asp:TextBox>
                                            </td>
                                            <td>
                                                <label class="LabelCampo">Data Al:</label>
                                                <asp:TextBox ID="Txt_DataAl" onkeypress="return handleEnter(this, event)" runat="server" Width="90px"></asp:TextBox>
                                            </td>
                                        </tr>
                                    </table>

                                    <table>
                                        <tr>
                                            <td>
                                                <label class="LabelCampo">Anno Protocollo:</label>
                                                <asp:TextBox ID="Txt_AnnoProtocollo" onkeypress="return handleEnterSoloNumero(this, event);" runat="server" MaxLength="4" Width="120px"></asp:TextBox>
                                            </td>
                                            <td>
                                                <label class="LabelCampo">Numero Protocollo:</label>
                                                <asp:TextBox ID="Txt_NumeroProtocollo" onkeypress="return handleEnterSoloNumero(this, event);" runat="server" Width="120px"></asp:TextBox>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                <label class="LabelCampo">Importo:</label>
                                                <asp:TextBox ID="Txt_Importo" Style="text-align: right;" autocomplete="off" runat="server" Width="104px"></asp:TextBox>
                                            </td>
                                            <td></td>
                                        </tr>
                                    </table>

                                    <table>
                                        <tr>
                                            <td>
                                                <label class="LabelCampo">Dal numero:</label>
                                                <asp:TextBox ID="Txt_NumeroDal" onkeypress="return handleEnterSoloNumero(this, event)" runat="server" Width="100px"></asp:TextBox>
                                            </td>
                                            <td>
                                                <label class="LabelCampo">Al numero:</label>
                                                <asp:TextBox ID="Txt_NumeroAl" onkeypress="return handleEnterSoloNumero(this, event)" runat="server" Width="100px"></asp:TextBox>
                                            </td>
                                        </tr>

                                        <tr>
                                            <td>
                                                <label class="LabelCampo">Numero Documento:</label>
                                                <asp:TextBox ID="Txt_NumeroDocumento" runat="server" Width="100px"></asp:TextBox>
                                            </td>
                                            <td></td>
                                        </tr>

                                        <tr>
                                            <td>
                                                <label class="LabelCampo">Inserisci Saldo:</label>
                                                <asp:CheckBox ID="Chk_InserisicSaldo" runat="server" Text="" />
                                            </td>
                                        </tr>

                                        <tr>
                                            <td>
                                                <label class="LabelCampo">Dati Ospite :</label>
                                                <asp:CheckBox ID="Chk_DatiOspite" runat="server" Text="" />
                                            </td>
                                        </tr>


                                        <tr>
                                            <td>
                                                <label class="LabelCampo">Bollo Virtuale :</label>
                                                <asp:CheckBox ID="Chk_BolloVirtuale" runat="server" Text="" />
                                            </td>
                                        </tr>


                                        <tr>
                                            <td>Campi :</td>
                                            <td></td>
                                        </tr>
                                    </table>
                                    <asp:CheckBoxList ID="Chk_Campi" runat="server" Height="114px" RepeatColumns="3"
                                        RepeatDirection="Horizontal" Style="margin-bottom: 0px" Width="676px">
                                        <asp:ListItem>Data Registrazione</asp:ListItem>
                                        <asp:ListItem>Numero Documento</asp:ListItem>
                                        <asp:ListItem>Data Documento</asp:ListItem>
                                        <asp:ListItem>Numero Protocollo</asp:ListItem>
                                        <asp:ListItem>Anno Protocollo</asp:ListItem>
                                        <asp:ListItem>Causale Contabile</asp:ListItem>
                                        <asp:ListItem>Descrizione</asp:ListItem>
                                        <asp:ListItem>Dare</asp:ListItem>
                                        <asp:ListItem>Avere</asp:ListItem>
                                        <asp:ListItem>Saldo</asp:ListItem>
                                        <asp:ListItem>Riga</asp:ListItem>
                                        <asp:ListItem>Conto Partita</asp:ListItem>
                                        <asp:ListItem>Conto Contro Partita</asp:ListItem>
                                        <asp:ListItem>Imponibile</asp:ListItem>
                                        <asp:ListItem>Codice Ritenuta</asp:ListItem>
                                    </asp:CheckBoxList>
                                    <br />

                                    <asp:Label ID="Lbl_Errori" runat="server" ForeColor="Red" Width="384px"></asp:Label>
                                    &nbsp;<asp:GridView ID="Grid" runat="server" CellPadding="3"
                                        Width="100%" BackColor="White" BorderColor="#999999" BorderStyle="None"
                                        BorderWidth="1px" GridLines="Vertical">
                                        <RowStyle ForeColor="#565151" BackColor="#EEEEEE" />
                                        <Columns>
                                            <asp:TemplateField HeaderText="">
                                                <ItemTemplate>
                                                    <asp:ImageButton ID="Richiama" CommandName="Richiama" runat="Server"
                                                        ImageUrl="~/images/select.png" class="EffettoBottoniTondi" BackColor="Transparent"
                                                        CommandArgument='<%#  Eval("Numero") %>' />
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                        </Columns>
                                        <FooterStyle BackColor="#CCCCCC" ForeColor="Black" />
                                        <PagerStyle BackColor="#999999" ForeColor="Black" HorizontalAlign="Center" />
                                        <SelectedRowStyle BackColor="#008A8C" Font-Bold="True" ForeColor="White" />
                                        <HeaderStyle BackColor="#565151" Font-Bold="True" ForeColor="White" />
                                        <AlternatingRowStyle BackColor="#DCDCDC" />
                                    </asp:GridView>

                                </ContentTemplate>

                            </xasp:TabPanel>
                        </xasp:TabContainer>
                    </td>
                </tr>
            </table>
        </div>
        <asp:GridView ID="GridView1" runat="server" Height="70px" Width="760px">
        </asp:GridView>

    </form>
</body>
</html>
