﻿Imports System.Web.Hosting
Imports System.Data.OleDb

Partial Class GeneraleWeb_ControlloRegistrazioniAnalitica
    Inherits System.Web.UI.Page
    Dim Tabella As New System.Data.DataTable("tabella")



    Protected Sub Esegui_Ricerca(ByVal ToExcel As Boolean)
        Dim ConnectionString As String = Session("DC_TABELLE")
        Dim ConnectionStringGenerale As String = Session("DC_GENERALE")
        Dim MySql As String
        Dim Condizione As String
        Dim Tipo As Integer = 0
        Dim MySqlrs As String
        Dim MySqlrs2 As String
        Dim Saldo As Double


        Condizione = ""
        Dim cn As OleDbConnection

        cn = New Data.OleDb.OleDbConnection(ConnectionStringGenerale)

        cn.Open()




        MySql = "Select *,(Select sum(Importo) From LegameBudgetRegistrazione Where  LegameBudgetRegistrazione.NumeroRegistrazione = MovimentiContabiliTesta.NumeroRegistrazione )  as LegameBudget From MovimentiContabiliTesta"


        If IsDate(Txt_DataDal.Text) Then
            If Condizione <> "" Then
                Condizione = Condizione & " And "
            End If
            Condizione = Condizione & " DataRegistrazione >= ?"
        End If

        If IsDate(Txt_DataAl.Text) Then
            If Condizione <> "" Then
                Condizione = Condizione & " And "
            End If
            Condizione = Condizione & " DataRegistrazione <= ? "
        End If



        Dim cmd As New OleDbCommand()

        Dim SubSelectLegameBudgetNull As String = ""

        Dim ContoDiCosto2 As String = ""
        Dim SelectPDC As String = ""

        SelectPDC = " ((select count(*) From PianoConti Where Mastro = MastroPartita And Conto =ContoPartita And SottocontoPartita = Sottoconto And (Tipo = 'C' or Tipo ='R')) > 0)"



        ContoDiCosto2 = "(Select sum(importo) from MovimentiContabiliRiga Where MovimentiContabiliRiga.Numero = MovimentiContabiliTesta.NumeroRegistrazione And " & SelectPDC & ") > 0"



        SubSelectLegameBudgetNull = " ((Select sum(Importo) From LegameBudgetRegistrazione Where  LegameBudgetRegistrazione.NumeroRegistrazione = MovimentiContabiliTesta.NumeroRegistrazione) Is Null) "

        Dim SubSelectLegameBudgetDiverso As String = ""
        Dim SubSelectLegameBudgetDiverso2 As String = ""

        SubSelectLegameBudgetDiverso = "(Select sum(Importo) From LegameBudgetRegistrazione Where  LegameBudgetRegistrazione.NumeroRegistrazione = MovimentiContabiliTesta.NumeroRegistrazione ) "


        Dim ContoDiCosto As String = ""

        ContoDiCosto = " ((select count(*) From PianoConti Where Mastro = MastroPartita And Conto =ContoPartita And SottocontoPartita = Sottoconto And (Tipo = 'C' or Tipo ='R')) > 0)"

        SubSelectLegameBudgetDiverso2 = "(Select sum(importo) from MovimentiContabiliRiga Where MovimentiContabiliRiga.Numero = MovimentiContabiliTesta.NumeroRegistrazione And " & ContoDiCosto & ")"

        cmd.CommandText = MySql & " Where ( (" & SubSelectLegameBudgetNull & " And " & ContoDiCosto2 & ") OR " & SubSelectLegameBudgetDiverso & "  <>  " & SubSelectLegameBudgetDiverso2 & " )  And " & Condizione
        If IsDate(Txt_DataDal.Text) Then
            Dim DataDal As Date = Txt_DataDal.Text
            cmd.Parameters.AddWithValue("@DataDal", DataDal)
        End If

        If IsDate(Txt_DataAl.Text) Then
            Dim DataAl As Date = Txt_DataAl.Text
            cmd.Parameters.AddWithValue("@DataAl", DataAl)
        End If

        cmd.Connection = cn

        Tabella.Clear()
        Tabella.Columns.Clear()
        Tabella.Columns.Add("Numero", GetType(String))
        Tabella.Columns.Add("Dt.Reg.", GetType(String))
        Tabella.Columns.Add("N.Doc.", GetType(String))
        Tabella.Columns.Add("Dt.Doc.", GetType(String))
        Tabella.Columns.Add("Importo Registrazione", GetType(String))
        Tabella.Columns.Add("Importo Costi/Ricavi", GetType(String))
        Tabella.Columns.Add("Importo Legato", GetType(String))

        Dim myPOSTreader As OleDbDataReader = cmd.ExecuteReader()
        Do While myPOSTreader.Read

            Dim Importo As Double = 0

            'If Importo = 0 Then
            '    Dim AppoSql As String

            '    AppoSql = "select MovimentiContabiliRiga.Importo,MovimentiContabiliRiga.DareAvere,RegoleBudget.Percentuale,RegoleBudget.ImportoFisso,RegoleBudget.Colonna,RegoleBudget.CentroServizio AS CENTROSERVIZIO,(SELECT TOP 1 CentroServizio FROM MovimentiContabiliTesta WHERE MovimentiContabiliRiga.Numero = MovimentiContabiliTesta.NumeroRegistrazione) AS MovTCentroServizio from MovimentiContabiliRiga inner join RegoleBudget on MastroPartita = MastroPianoDeiConti and ContoPartita = ContoPianoDeiConti and SottocontoPartita = SottoContoPianoDeiConti where numero = ? And RegoleBudget.Anno = " & Year(myPOSTreader.Item("DataRegistrazione"))

            '    Dim CmdR As New OleDbCommand(AppoSql, cn)
            '    CmdR.Parameters.AddWithValue("@NumeroRegistrazione", Val(campodb(myPOSTreader.Item("NumeroRegistrazione"))))
            '    Dim ReadR As OleDbDataReader = CmdR.ExecuteReader()
            '    Do While ReadR.Read
            '        If campodb(ReadR.Item("CentroServizio")) = "****" Or campodb(ReadR.Item("CentroServizio")) = campodb(ReadR.Item("MovTCentroServizio")) Then
            '            If dC.VenditaAcquisti = "A" Then
            '                If campodb(ReadR.Item("DareAVere")) = "D" Then
            '                    Importo = Importo + Math.Round(campodbn(ReadR.Item("Importo")) * campodbn(ReadR.Item("Percentuale")) / 100, 2)
            '                    Importo = Importo + campodbn(ReadR.Item("ImportoFisso"))
            '                Else
            '                    VettoreColonne(campodbn(ReadR.Item("Colonna"))) = VettoreColonne(campodbn(ReadR.Item("Colonna"))) - Math.Round(campodbn(ReadR.Item("Importo")) * campodbn(ReadR.Item("Percentuale")) / 100, 2)
            '                    VettoreColonne(campodbn(ReadR.Item("Colonna"))) = VettoreColonne(campodbn(ReadR.Item("Colonna"))) - campodbn(ReadR.Item("ImportoFisso"))
            '                End If
            '            Else
            '                If campodb(ReadR.Item("DareAVere")) = "A" Then
            '                    VettoreColonne(campodbn(ReadR.Item("Colonna"))) = VettoreColonne(campodbn(ReadR.Item("Colonna"))) + Math.Round(campodbn(ReadR.Item("Importo")) * campodbn(ReadR.Item("Percentuale")) / 100, 2)
            '                    VettoreColonne(campodbn(ReadR.Item("Colonna"))) = VettoreColonne(campodbn(ReadR.Item("Colonna"))) + campodbn(ReadR.Item("ImportoFisso"))
            '                Else
            '                    VettoreColonne(campodbn(ReadR.Item("Colonna"))) = VettoreColonne(campodbn(ReadR.Item("Colonna"))) - Math.Round(campodbn(ReadR.Item("Importo")) * campodbn(ReadR.Item("Percentuale")) / 100, 2)
            '                    VettoreColonne(campodbn(ReadR.Item("Colonna"))) = VettoreColonne(campodbn(ReadR.Item("Colonna"))) - campodbn(ReadR.Item("ImportoFisso"))
            '                End If
            '            End If
            '        End If
            '    Loop
            '    ReadR.Close()
            'End If


            Dim myriga As System.Data.DataRow = Tabella.NewRow()
            myriga(0) = campodbn(myPOSTreader.Item("NumeroRegistrazione"))

            Dim RegN As New Cls_MovimentoContabile

            RegN.NumeroRegistrazione = campodbn(myPOSTreader.Item("NumeroRegistrazione"))
            RegN.Leggi(Session("DC_GENERALE"), RegN.NumeroRegistrazione)


            myriga(1) = Format(RegN.DataRegistrazione, "dd/MM/yyyy")
            myriga(2) = RegN.NumeroDocumento

            myriga(3) = Format(RegN.DataDocumento, "dd/MM/yyyy")


            myriga(4) = Format(RegN.ImportoRegistrazioneDocumento(Session("DC_TABELLE")), "#,##0.00")


            Dim SommaCostiRicavi As Double = 0
            Dim Indice As Integer
            For Indice = 0 To 300
                If Not IsNothing(RegN.Righe(Indice)) Then
                    Dim PDC As New Cls_Pianodeiconti

                    PDC.Mastro = RegN.Righe(Indice).MastroPartita
                    PDC.Conto = RegN.Righe(Indice).ContoPartita
                    PDC.Sottoconto = RegN.Righe(Indice).SottocontoPartita
                    PDC.Decodfica(Session("DC_GENERALE"))

                    If PDC.Tipo = "C" Or PDC.Tipo = "R" Then
                        If RegN.Righe(Indice).DareAvere = "D" Then
                            SommaCostiRicavi = SommaCostiRicavi + RegN.Righe(Indice).Importo
                        Else
                            SommaCostiRicavi = SommaCostiRicavi - RegN.Righe(Indice).Importo
                        End If

                    End If

                End If
            Next


            myriga(5) = Format(Math.Abs(SommaCostiRicavi), "#,##0.00")


            myriga(6) = Format(campodbn(myPOSTreader.Item("LegameBudget")), "#,##0.00")


            Tabella.Rows.Add(myriga)

        Loop
        myPOSTreader.Close()
        cn.Close()

        If ToExcel = True Then
            GridView1.AutoGenerateColumns = True
            GridView1.DataSource = Tabella
            GridView1.DataBind()


            Response.Clear()
            Response.AddHeader("content-disposition", "attachment;filename=Ricerca.xls")
            Response.Charset = String.Empty
            Response.Cache.SetCacheability(HttpCacheability.NoCache)
            Response.ContentType = "application/vnd.xls"
            Dim stringWrite As New System.IO.StringWriter
            Dim htmlWrite As New HtmlTextWriter(stringWrite)

            form1.Controls.Clear()
            form1.Controls.Add(GridView1)

            form1.RenderControl(htmlWrite)

            Response.Write(stringWrite.ToString())
            Response.End()

        Else
            Grid.AutoGenerateColumns = True
            Grid.DataSource = Tabella
            Grid.DataBind()



        End If



    End Sub

    Function campodb(ByVal oggetto As Object) As String
        If IsDBNull(oggetto) Then
            Return ""
        Else
            Return oggetto
        End If
    End Function

    Function campodbn(ByVal oggetto As Object) As Double
        If IsDBNull(oggetto) Then
            Return 0
        Else
            Return oggetto
        End If
    End Function



    Private Function SplitWords(ByVal s As String) As String()
        '
        ' Call Regex.Split function from the imported namespace.
        ' Return the result array.
        '
        Return Regex.Split(s, "\W+")
    End Function

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Trim(Session("UTENTE")) = "" Then
            If Request.ServerVariables("URL").ToUpper.IndexOf("SENIORWEBBLANCO") > 0 Then
                Response.Redirect("/ODV/Login.aspx")
                Exit Sub
            Else
                Response.Redirect("/SeniorWeb/Login.aspx")
                Exit Sub
            End If
        End If

        Call EseguiJS()

        If Page.IsPostBack = True Then Exit Sub

        Dim K1 As New Cls_SqlString

        Dim Barra As New Cls_BarraSenior

        If Not IsNothing(Session("RicercaAnagraficaSQLString")) Then
            Try
                K1 = Session("RicercaAnagraficaSQLString")
            Catch ex As Exception

            End Try
        End If

        Lbl_BarraSenior.Text = Barra.CodiceBarra(Application("SENIOR"), Session("UTENTE"), Page, K1)



        Dim k As New Cls_CausaleContabile

        Dim ConnectionString As String = Session("DC_TABELLE")


        Txt_DataDal.Text = Format(DateSerial(Year(Now), Month(Now), 1), "dd/MM/yyyy")
        Txt_DataAl.Text = Format(DateSerial(Year(Now), 12, 31), "dd/MM/yyyy")

        EseguiJS()

    End Sub

    Private Sub EseguiJS()
        Dim MyJs As String
        MyJs = "$(document).ready(function() {"

        MyJs = MyJs & "var els = document.getElementsByTagName(""*"");"

        MyJs = MyJs & "for (var i=0;i<els.length;i++)"
        MyJs = MyJs & "if ( els[i].id ) { "
        MyJs = MyJs & " var appoggio =els[i].id; "
        MyJs = MyJs & " if (appoggio.match('Txt_Sottoconto')!= null) {  "
        MyJs = MyJs & " $(els[i]).autocomplete('/WebHandler/GestioneConto.ashx?Utente=" & Session("UTENTE") & "', {delay:5,minChars:3});"
        MyJs = MyJs & "    }"


        MyJs = MyJs & " if (appoggio.match('Txt_Data')!= null) {  "
        MyJs = MyJs & " $(els[i]).mask(""99/99/9999"");  "
        MyJs = MyJs & " $(els[i]).datepicker({ changeMonth: true,changeYear: true,  yearRange: ""1890:" & Year(Now) + 1 & """},$.datepicker.regional[""it""]);"
        MyJs = MyJs & "    }"

        MyJs = MyJs & " if (appoggio.match('Txt_Importo')!= null)  {  "
        MyJs = MyJs & " $(els[i]).keypress(function() { ForceNumericInput($(this).val(), true, true); } ); "
        MyJs = MyJs & " $(els[i]).blur(function() { var ap = formatNumber($(this).val(),2); $(this).val(ap); });"
        MyJs = MyJs & "    }"

        MyJs = MyJs & "$("".chosen-select"").chosen({"
        MyJs = MyJs & "no_results_text: ""Causale non trovata"","
        MyJs = MyJs & "display_disabled_options: true,"
        'MyJs = MyJs & "allow_single_deselect: true,"
        MyJs = MyJs & "width: ""350px"","
        MyJs = MyJs & "placeholder_text_single: ""Seleziona Causale"""
        MyJs = MyJs & "});"


        MyJs = MyJs & "} "
        MyJs = MyJs & "});"

        'MyJs = "$(document).ready(function() { $('.myClass').keypress(function() { handleEnter($('.myClass').val(), true, true); } );     $('#" & Txt_ImportoPagato.ClientID & "').blur(function() { var ap = formatNumber ($('#" & Txt_ImportoPagato.ClientID & "').val(),2); $('#" & Txt_ImportoPagato.ClientID & "').val(ap); }); });"
        ScriptManager.RegisterStartupScript(Me, Me.GetType(), "visualizzaRitIm", MyJs, True)
    End Sub


    Protected Sub ImageButton1_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles ImageButton1.Click
        Call Esegui_Ricerca(False)
    End Sub

    Protected Sub ImageButton2_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles ImageButton2.Click

        Call Esegui_Ricerca(True)

        Response.Redirect("RicercaExcel.aspx")

    End Sub

    Protected Sub Btn_Esci_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles Btn_Esci.Click


        Response.Redirect("Menu_Budget.aspx")

    End Sub




End Class
