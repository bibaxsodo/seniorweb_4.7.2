﻿
Partial Class GeneraleWeb_RitenuteModificaDocumenti
    Inherits System.Web.UI.Page
    Protected Sub Dd_Ritenuta_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs)

        Dim Xs As New ClsRitenuta

        Xs.Codice = sender.SelectedValue
        Xs.Leggi(Session("DC_TABELLE"))


        If Not IsNothing(Txt_Imponibile) Then
            Dim MyJs As String

            MyJs = "$(document).ready(function() { $('#" & Txt_Imponibile.ClientID & "').keypress(function() { ForceNumericInput($('#" & Txt_Imponibile.ClientID & "').val(), true, true); } );     $('#" & Txt_Imponibile.ClientID & "').blur(function() { var ap = formatNumber ($('#" & Txt_Imponibile.ClientID & "').val(),2); $('#" & Txt_Imponibile.ClientID & "').val(ap); }); });"
            ScriptManager.RegisterStartupScript(Me, Me.GetType(), "visualizzaRitIm", MyJs, True)
        End If


        If Not IsNothing(Txt_Imposta) Then
            Dim MyJs As String

            MyJs = "$(document).ready(function() { $('#" & Txt_Imposta.ClientID & "').keypress(function() { ForceNumericInput($('#" & Txt_Imposta.ClientID & "').val(), true, true); } );      $('#" & Txt_Imposta.ClientID & "').blur(function() { var ap = formatNumber ($('#" & Txt_Imposta.ClientID & "').val(),2); $('#" & Txt_Imposta.ClientID & "').val(ap); }); });"
            REM ClientScript.RegisterClientScriptBlock(Me.GetType(), "visualizzaRit", MyJs, True)
            ScriptManager.RegisterStartupScript(Me, Me.GetType(), "visualizzaRit", MyJs, True)
        End If

        If Val(Txt_Imponibile.Text) = 0 Then
            Exit Sub
        End If
        Dim Imponibile As Double
        Imponibile = Txt_Imponibile.Text


        Txt_Imposta.Text = Modulo.MathRound(Imponibile * Xs.Percentuale, 2)

    End Sub

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load

        If Page.IsPostBack = True Then Exit Sub


        Dim Ritenuta As New Cls_RitenuteAcconto


        Ritenuta.ID = Request.Item("ID")
        Ritenuta.Leggi(Session("DC_GENERALE"))


        Txt_Imponibile.Text = Format(Ritenuta.ImponibileRitenuta, "#,##0.00")
        Txt_Imposta.Text = Format(Ritenuta.ImportoRitenuta, "#,##0.00")

        Dim MRit As New ClsRitenuta

        MRit.UpDateDropBox(Session("DC_TABELLE"), DDRitenuta)

        DDRitenuta.SelectedValue = Ritenuta.TipoRitenuta

        Dim registrazionecontabile As New Cls_MovimentoContabile


        registrazionecontabile.NumeroRegistrazione = Ritenuta.NumeroRegistrazioneDocumento
        registrazionecontabile.Leggi(Session("DC_GENERAlE"), registrazionecontabile.NumeroRegistrazione)

        Lbl_DatiDoc.Text = "Num. Doc. " & registrazionecontabile.NumeroDocumento & " " & Format(registrazionecontabile.DataRegistrazione, "dd/MM/yyyy")


        LblDati.text = "<br/><br/>Numero Registrazione Documento :" & registrazionecontabile.NumeroRegistrazione
        LblDati.text = LblDati.text & "<br/><br/>Numero Documento :" & registrazionecontabile.NumeroDocumento
        LblDati.text = LblDati.text & "<br/><br/>Data Documento :" & Format(registrazionecontabile.DataRegistrazione, "dd/MM/yyyy")

        Dim Riga As Integer
        For Riga = 0 To 300

            If Not IsNothing(registrazionecontabile.Righe(Riga)) Then
                If registrazionecontabile.Righe(Riga).Tipo = "RI" And registrazionecontabile.Righe(Riga).DareAvere = "D" Then
                    LblDati.text = LblDati.text & "<br/><br/>Imponibile Ritenuta :" & Format(registrazionecontabile.Righe(Riga).Imponibile, "#,##0.00")

                    Dim Rit As New ClsRitenuta

                    Rit.Codice = registrazionecontabile.Righe(Riga).CodiceRitenuta
                    Rit.Leggi(Session("DC_TABELLE"))

                    LblDati.text = LblDati.text & "<br/><br/>Tipo Ritenuta :" & Rit.Descrizione
                    LblDati.text = LblDati.text & "<br/><br/>Importo Ritenuta :" & Format(registrazionecontabile.Righe(Riga).Importo, "#,##0.00")

                End If
            End If

        Next

        

    End Sub

    Protected Sub Btn_Elimina_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles Btn_Elimina.Click

        Dim Ritenuta As New Cls_RitenuteAcconto


        Ritenuta.ID = Request.Item("ID")
        Ritenuta.Leggi(Session("DC_GENERALE"))


        Ritenuta.Delete(Session("DC_GENERALE"))


        Response.Redirect("RitenuteDocumneti.aspx?NumeroRegistrazione=" & Request.Item("NUMERO"))
    End Sub

    Protected Sub Btn_Modifica_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles Btn_Modifica.Click
        Dim Ritenuta As New Cls_RitenuteAcconto


        Dim registrazionecontabile As New Cls_MovimentoContabile


        registrazionecontabile.NumeroRegistrazione = Request.Item("NUMERO")
        registrazionecontabile.Leggi(Session("DC_GENERAlE"), registrazionecontabile.NumeroRegistrazione)

        If registrazionecontabile.ImportoRitenuta(Session("DC_TABELLE")) < Ritenuta.RitenuteSuDocumento(Session("DC_GENERALE"), Request.Item("NUMERO"), Request.Item("ID")) + Txt_Imposta.Text Then
            Exit Sub
        End If


        Ritenuta.ID = Request.Item("ID")
        Ritenuta.Leggi(Session("DC_GENERALE"))

        Ritenuta.ImportoRitenuta = Txt_Imposta.Text

        Ritenuta.ImponibileRitenuta = Txt_Imponibile.Text

        Ritenuta.TipoRitenuta = DDRitenuta.SelectedValue

        Ritenuta.Scrivi(Session("DC_GENERALE"))

        Response.Redirect("RitenuteDocumneti.aspx?NumeroRegistrazione=" & Request.Item("NUMERO"))
    End Sub

    Protected Sub Btn_Duplica_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles Btn_Duplica.Click
        Dim Ritenuta As New Cls_RitenuteAcconto
        Dim Imponibile As Double
        Dim Imposta As Double



        Ritenuta.ID = Request.Item("ID")
        Ritenuta.Leggi(Session("DC_GENERALE"))


        Imponibile = Math.Round(Ritenuta.ImponibileRitenuta / 2, 2)
        Imposta = Math.Round(Ritenuta.ImportoRitenuta / 2, 2)

        Ritenuta.ImportoRitenuta = Imposta

        Ritenuta.ImponibileRitenuta = Imponibile

        Ritenuta.TipoRitenuta = DDRitenuta.SelectedValue

        Ritenuta.Scrivi(Session("DC_GENERALE"))



        Ritenuta.ID = 0


        Ritenuta.ImportoRitenuta = Imposta

        Ritenuta.ImponibileRitenuta = Imponibile

        Ritenuta.TipoRitenuta = DDRitenuta.SelectedValue

        Ritenuta.Scrivi(Session("DC_GENERALE"))


        Response.Redirect("RitenuteDocumneti.aspx?NumeroRegistrazione=" & Request.Item("NUMERO"))
    End Sub
End Class
