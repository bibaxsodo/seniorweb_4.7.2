﻿Imports System.Web.Routing
Imports Microsoft.AspNet.FriendlyUrls

Public Module RouteConfig
    Sub RegisterRoutes(ByVal routes As RouteCollection)
        Dim settings As FriendlyUrlSettings = New FriendlyUrlSettings()  
        
        rem With {            
        rem    .AutoRedirectMode = RedirectMode.Permanent          
        rem }
        
        routes.EnableFriendlyUrls(settings)
    End Sub
End Module
