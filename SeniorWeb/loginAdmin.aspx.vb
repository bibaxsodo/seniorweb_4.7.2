﻿
Partial Class loginAdmin
    Inherits System.Web.UI.Page

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Trim(Session("UTENTE")) = "" Then
            Response.Redirect("Login.aspx")
            Exit Sub
        End If
        If Trim(Session("UTENTEIMPER")) = "" Then
            Response.Redirect("Login.aspx")
            Exit Sub
        End If


        Dim k As New Cls_Login
        k.Utente = Request.Item("UTENTE")
        k.Ip = Context.Request.ServerVariables("REMOTE_ADDR")
        k.Sistema = Request.UserAgent & " " & Request.Browser.Browser & " " & Request.Browser.MajorVersion & " " & Request.Browser.MinorVersion
        k.LeggiSP(Application("SENIOR"))

        Session("UTENTE") = k.Utente
        Session("DC_OSPITE") = k.Ospiti
        Session("DC_OSPITIACCESSORI") = k.OspitiAccessori
        Session("DC_TABELLE") = k.TABELLE
        Session("DC_GENERALE") = k.Generale
        Session("DC_TURNI") = k.Turni
        Session("STAMPEOSPITI") = k.STAMPEOSPITI
        Session("STAMPEFINANZIARIA") = k.STAMPEFINANZIARIA
        Session("ProgressBar") = ""
        Session("CODICEREGISTRO") = ""
        Session("ChiaveCr") = k.ChiaveCr
        Session("NomeEPersonam") = k.NomeEPersonam

        Session("CLIENTE") = k.Cliente
        Session("SENIOR") = Application("SENIOR")

        k.LeggiEpersonamUser(Application("SENIOR"))

        If k.EPersonamUser.IndexOf("w") > 0 Then
            k.EPersonamUser = k.EPersonamUser.Replace("w", "")
        End If

        Session("EPersonamUser") = k.EPersonamUser
        Session("EPersonamPSW") = k.EPersonamPSW
        Session("EPersonamPSWCRYPT") = k.EPersonamPSWCRYPT
        Session("GDPRAttivo") = k.GDPRAttivo

        If k.Ospiti <> "" Then
            Session("ABILITAZIONI") = k.ABILITAZIONI

            
            'CallUrlSeID()
            Session("USER_SODO") = True

            Response.Redirect("CreaUtenti.aspx?IDCLIENTE=" & k.Cliente)
        End If


    End Sub


    Private Sub CallUrlSeID()
        If Not IsNothing(Request.UrlReferrer) Then
            If Request.UrlReferrer.ToString.ToUpper.IndexOf("OSPITIWEB") > 0 Then
                Response.Redirect("OspitiWeb/Menu_Ospiti.aspx")
            End If
            If Request.UrlReferrer.ToString.ToUpper.IndexOf("GENERALEWEB") > 0 Then
                Response.Redirect("GeneraleWeb/Menu_Generale.aspx")
            End If
        End If
    End Sub

End Class
