﻿Imports Microsoft.VisualBasic
Imports System.Data.OleDb
Imports System.Web.Hosting

Partial Class Stmp_Protocollo
    Inherits System.Web.UI.Page



    Private Sub EseguiJS()
        Dim MyJs As String
        MyJs = "$(document).ready(function() {"



        MyJs = MyJs & "var els = document.getElementsByTagName(""*"");"

        MyJs = MyJs & "for (var i=0;i<els.length;i++)"
        MyJs = MyJs & "if ( els[i].id ) { "
        MyJs = MyJs & " var appoggio =els[i].id; "

        MyJs = MyJs & " if (appoggio.match('Txt_Anagrafica')!= null) {  "
        MyJs = MyJs & " $(els[i]).autocomplete('Anagrafiche.ashx?Utente=" & Session("Utente") & "', {delay:5,minChars:3});"
        MyJs = MyJs & "    }"


        MyJs = MyJs & " if ((appoggio.match('Txt_Data')!= null) || (appoggio.match('Txt_AllaData')!= null) || (appoggio.match('TxtDataRegistrazione') != null)) {  "
        MyJs = MyJs & " $(els[i]).mask(""99/99/9999"");  "
        MyJs = MyJs & "    }"

        MyJs = MyJs & " if (appoggio.match('TxtImporto')!= null)  {  "
        MyJs = MyJs & " $(els[i]).keypress(function() { ForceNumericInput($(this).val(), true, true); } ); "
        MyJs = MyJs & " $(els[i]).blur(function() { var ap = formatNumber($(this).val(),2); $(this).val(ap); });"
        MyJs = MyJs & "    }"

        MyJs = MyJs & "} "
        MyJs = MyJs & "});"

        'MyJs = "$(document).ready(function() { $('.myClass').keypress(function() { handleEnter($('.myClass').val(), true, true); } );     $('#" & Txt_ImportoPagato.ClientID & "').blur(function() { var ap = formatNumber ($('#" & Txt_ImportoPagato.ClientID & "').val(),2); $('#" & Txt_ImportoPagato.ClientID & "').val(ap); }); });"
        ScriptManager.RegisterStartupScript(Me, Me.GetType(), "visualizzaRitIm", MyJs, True)
    End Sub

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load


        Call EseguiJS()

        If Page.IsPostBack = True Then Exit Sub


        Dim Barra As New Cls_BarraSenior


        Lbl_BarraSenior.Text = Barra.CodiceBarra(Application("SENIOR"), Session("UTENTE"), Page)

        Dim K As New Cls_UfficioDestinatario

        K.UpDropDownList(Session("DC_TABELLE"), DD_UfficioDestinatario)
    End Sub

    Protected Sub Btn_Stampa_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles Btn_Stampa.Click
        Dim TabProtcollo As New Xsd_StampaProtocollo


        Dim cn As OleDbConnection
        Dim I As Integer


        cn = New Data.OleDb.OleDbConnection(Session("DC_TABELLE"))

        cn.Open()
        Dim cmd As New OleDbCommand()



        Dim MySql As String
        MySql = "select * from Protocollo where "
        If Txt_Anno.Text <> "" Then
            MySql = MySql & " Anno = ?"
            cmd.Parameters.AddWithValue("@Anno", Txt_Anno.Text)
        End If

        If Chk_Entrata.Checked = True And Chk_Uscita.Checked = True Then
            If MySql <> "select * from Protocollo where " Then
                MySql = MySql & " And "
            End If
            MySql = MySql & " (Tipo = 'E' or Tipo = 'U')"
        Else
            If Chk_Entrata.Checked = True Then
                If MySql <> "select * from Protocollo where " Then
                    MySql = MySql & " And "
                End If
                MySql = MySql & " Tipo = 'E'"
            End If
            If Chk_Uscita.Checked = True Then
                If MySql <> "select * from Protocollo where " Then
                    MySql = MySql & " And "
                End If
                MySql = MySql & " Tipo = 'U'"
            End If
        End If
        If Txt_IDDal.Text <> "" Then
            If MySql <> "select * from Protocollo where " Then
                MySql = MySql & " And "
            End If
            MySql = MySql & " Numero >= ?"
            cmd.Parameters.AddWithValue("@Numero", Txt_IDDal.Text)
        End If
        If Txt_IDAl.Text <> "" Then
            If MySql <> "select * from Protocollo where " Then
                MySql = MySql & " And "
            End If
            MySql = MySql & " Numero <= ?"
            cmd.Parameters.AddWithValue("@Numero", Txt_IDAl.Text)
        End If
        If IsDate(Txt_DataDal.Text) Then
            If MySql <> "select * from Protocollo where " Then
                MySql = MySql & " And "
            End If
            MySql = MySql & " DataArrivo >= ?"
            cmd.Parameters.AddWithValue("@DataArrivo", Txt_DataDal.Text)
        End If
        If IsDate(Txt_DataAl.Text) Then
            If MySql <> "select * from Protocollo where " Then
                MySql = MySql & " And "
            End If
            MySql = MySql & " DataArrivo <= ?"
            cmd.Parameters.AddWithValue("@DataArrivo", Txt_DataAl.Text)
        End If
        If DD_UfficioDestinatario.SelectedValue <> 0 Then
            If MySql <> "select * from Protocollo where " Then
                MySql = MySql & " And "
            End If
            MySql = MySql & " UfficioDestinatario = ?"
            cmd.Parameters.AddWithValue("@UfficioDestinatario", DD_UfficioDestinatario.SelectedValue)
        End If


        If MySql.Trim = "select * from Protocollo where" Then
            ClientScript.RegisterClientScriptBlock(Me.GetType(), "Errore", "VisualizzaErrore('<center>Specificare un criterio di ricerca</center>');", True)
            Exit Sub
        End If

        cmd.CommandText = MySql & "  Order By Anno,Numero "



        cmd.Connection = cn
        Dim sb As StringBuilder = New StringBuilder

        Dim myPOSTreader As OleDbDataReader = cmd.ExecuteReader()
        Do While myPOSTreader.Read

            Dim KlS As New Cls_ProtocolloAnagrafiche
            Dim RagioneSociale As String = ""
            Dim ForSearchRagioneSociale As String = ""
            Dim Indirizzo As String = ""
            Dim Cap As String = ""
            Dim Comune As String = ""
            Dim Provincia As String = ""

            Dim Mezzo As String = ""

            KlS.Anno = campodb(myPOSTreader.Item("Anno"))
            KlS.Numero = campodb(myPOSTreader.Item("Numero"))
            KlS.Tipo = campodb(myPOSTreader.Item("Tipo"))
            KlS.Leggi(Session("DC_TABELLE"))



            For I = 0 To 1000
                If KlS.IdRubrica(I) > 0 Then
                    Dim Kppp As New Cls_Rubrica
                    Kppp.Id = KlS.IdRubrica(I)
                    Kppp.Leggi(Session("DC_TABELLE"))
                    If RagioneSociale = "" Then
                        RagioneSociale = Kppp.Denominazione & " " & Kppp.Cognome & " " & Kppp.Nome
                        Indirizzo = Kppp.Indirizzo
                        Cap = Kppp.Cap

                        Dim DecTipo As New ClsComune

                        DecTipo.Provincia = Kppp.Provincia
                        DecTipo.Comune = Kppp.Comune
                        DecTipo.Leggi(Session("DC_OSPITE"))
                        Comune = DecTipo.Descrizione
                        Provincia = DecTipo.CodificaProvincia
                    End If
                    ForSearchRagioneSociale = ForSearchRagioneSociale & Kppp.Denominazione & " " & Kppp.Cognome & " " & Kppp.Nome & ","
                End If
                If KlS.IdClienteFornitore(I) > 0 Then
                    Dim KpppS As New Cls_ClienteFornitore
                    KpppS.CODICEDEBITORECREDITORE = KlS.IdClienteFornitore(I)
                    KpppS.Leggi(Session("DC_OSPITE"))
                    If RagioneSociale = "" Then
                        RagioneSociale = KpppS.Nome
                        Indirizzo = KpppS.RESIDENZAINDIRIZZO1
                        Cap = KpppS.RESIDENZACAP1

                        Dim DecTipo As New ClsComune

                        DecTipo.Provincia = KpppS.RESIDENZAPROVINCIA1
                        DecTipo.Comune = KpppS.RESIDENZACOMUNE1
                        DecTipo.Leggi(Session("DC_OSPITE"))
                        Comune = DecTipo.Descrizione
                        Provincia = DecTipo.CodificaProvincia
                    End If
                    ForSearchRagioneSociale = ForSearchRagioneSociale & KpppS.Nome & ","
                End If

                If KlS.IdOspite(I) > 0 And KlS.IdParente(I) = 0 Then
                    Dim KpppS As New ClsOspite
                    KpppS.CodiceOspite = KlS.IdOspite(I)
                    KpppS.Leggi(Session("DC_OSPITE"), KpppS.CodiceOspite)
                    If RagioneSociale = "" Then
                        RagioneSociale = KpppS.Nome
                        Indirizzo = KpppS.RESIDENZAINDIRIZZO1
                        Cap = KpppS.RESIDENZACAP1

                        Dim DecTipo As New ClsComune

                        DecTipo.Provincia = KpppS.RESIDENZAPROVINCIA1
                        DecTipo.Comune = KpppS.RESIDENZACOMUNE1
                        DecTipo.Leggi(Session("DC_OSPITE"))
                        Comune = DecTipo.Descrizione
                        Provincia = DecTipo.CodificaProvincia
                    End If
                    ForSearchRagioneSociale = ForSearchRagioneSociale & KpppS.Nome & ","
                End If
                If KlS.IdOspite(I) > 0 And KlS.IdParente(I) > 0 Then
                    Dim KpppS As New Cls_Parenti
                    KpppS.CodiceOspite = KlS.IdOspite(I)
                    KpppS.CodiceParente = KlS.IdParente(I)
                    KpppS.Leggi(Session("DC_OSPITE"), KpppS.CodiceOspite, KpppS.CodiceParente)
                    If RagioneSociale = "" Then
                        RagioneSociale = KpppS.Nome
                        Indirizzo = KpppS.RESIDENZAINDIRIZZO1
                        Cap = KpppS.RESIDENZACAP1

                        Dim DecTipo As New ClsComune

                        DecTipo.Provincia = KpppS.RESIDENZAPROVINCIA1
                        DecTipo.Comune = KpppS.RESIDENZACOMUNE1
                        DecTipo.Leggi(Session("DC_OSPITE"))
                        Comune = DecTipo.Descrizione
                        Provincia = DecTipo.CodificaProvincia
                    End If
                    ForSearchRagioneSociale = ForSearchRagioneSociale & KpppS.Nome & ","
                End If


            Next
            If RagioneSociale = "" Then
                RagioneSociale = campodb(myPOSTreader.Item("RagioneSociale"))
            End If

            If Txt_MittenteDestinatario.Text.Trim = "" Or (Txt_MittenteDestinatario.Text.Trim <> "" And ForSearchRagioneSociale.ToUpper.IndexOf(Txt_MittenteDestinatario.Text.ToUpper.Replace("%", "")) >= 0) Then

                Dim k As System.Data.DataRow = TabProtcollo.Tables("Protocollo").NewRow

                k.Item("Anno") = campodb(myPOSTreader.Item("Anno"))
                k.Item("Numero") = campodb(myPOSTreader.Item("Numero"))
                k.Item("Tipo") = campodb(myPOSTreader.Item("Tipo"))
                k.Item("Data") = campodb(myPOSTreader.Item("DataArrivo"))
                Dim lk As New Cls_Titolo

                lk.idTitolo = Val(campodb(myPOSTreader.Item("Titolo")))
                lk.Leggi(Session("DC_TABELLE"))


                k.Item("Titolo") = lk.Descrizione
                k.Item("CodiceTitolo") = lk.Codice

                Dim lkClasse As New Cls_Classe

                lkClasse.idClasse = Val(campodb(myPOSTreader.Item("Classe")))
                lkClasse.Leggi(Session("DC_TABELLE"))
                k.Item("Classe") = lkClasse.Descrizione
                k.Item("IdClasse") = lkClasse.idClasse

                Dim lkIndice As New Cls_Indice


                lkIndice.idClasse = Val(campodb(myPOSTreader.Item("Indice")))
                lkIndice.Leggi(Session("DC_TABELLE"))
                k.Item("Indice") = lkIndice.Descrizione
                k.Item("idIndice") = lkIndice.idIndice

                k.Item("Fascicolo") = campodb(myPOSTreader.Item("Fascicolo"))

                k.Item("Oggetto") = campodb(myPOSTreader.Item("Oggetto"))

                k.Item("Destinatario") = RagioneSociale
                k.Item("Indirizzo") = Indirizzo
                k.Item("Comune") = Comune
                k.Item("Provincia") = Provincia
                k.Item("Cap") = Cap


                Dim XMezzo As New Cls_Mezzo

                XMezzo.idMezzo = Val(campodb(myPOSTreader.Item("Mezzo")))
                XMezzo.Leggi(Session("DC_TABELLE"))

                k.Item("Mezzo") = XMezzo.Descrizione

                Dim XUfficio As New Cls_UfficioDestinatario

                XUfficio.idUfficioDestinatario = Val(campodb(myPOSTreader.Item("UfficioDestinatario")))
                XUfficio.Leggi(Session("DC_TABELLE"))

                k.Item("Ufficio") = XUfficio.Descrizione


                TabProtcollo.Tables("Protocollo").Rows.Add(k)

            End If
        Loop
        myPOSTreader.Close()
        cn.Close()

        Session("stampa") = TabProtcollo

        ScriptManager.RegisterClientScriptBlock(Me, Me.GetType(), "Stampa", "window.open('Stampa_Report.aspx?REPORT=Stampa_Protocollo.rpt','Stampe','width=800,height=600');", True)
    End Sub

    Function campodb(ByVal oggetto As Object) As String
        If IsDBNull(oggetto) Then
            Return ""
        Else
            Return oggetto
        End If
    End Function

    Protected Sub Btn_Esci_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles Btn_Esci.Click
        Response.Redirect("Menu_Protocollo.aspx")
    End Sub
End Class
