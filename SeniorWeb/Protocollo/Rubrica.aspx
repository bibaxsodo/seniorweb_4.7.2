﻿<%@ Page Language="VB" AutoEventWireup="false" EnableEventValidation="FALSE" Inherits="Protocollo_Rubrica" CodeFile="Rubrica.aspx.vb" %>


<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit.HTMLEditor"
    TagPrefix="cc2" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="AJAX" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
    <title>Rubrica</title>
    <asp:PlaceHolder runat="server">
        <%: System.Web.Optimization.Styles.Render("~/Content/AjaxControlToolkit/Styles/Bundle") %>
    </asp:PlaceHolder>
    <link rel="stylesheet" href="css/csv.css?Versione=8" type="text/css" />
    <link rel="shortcut icon" href="../images/SENIOR.ico" />
    <link href="js/jquery.autocomplete.css" rel="stylesheet" type="text/css" />
    <script src="js/jquery-1.5.1.min.js" type="text/javascript"></script>
    <script src="js/jquery.maskedinput-1.3.min.js" type="text/javascript"></script>
    <script src="js/jquery.autocomplete.js" type="text/javascript"></script>
    <script src="/js/formatnumer.js" type="text/javascript"></script>
    <script src="js/JSErrore.js" type="text/javascript"></script>
    <script src="js/NoEnter.js" type="text/javascript"></script>
    <link rel="stylesheet" href="dialogbox/redips-dialog.css" type="text/css" media="screen" />
    <script type="text/javascript" src="dialogbox/redips-dialog-min.js"></script>
    <script type="text/javascript" src="dialogbox/script.js"></script>
    <style>
        th {
            font-weight: normal;
        }
    </style>
    <script type="text/javascript">
        var windowObjectReference;

        function openRequestedPopup() {

            windowObjectReference = window.open("PdfDaHtml.aspx",
                "DescriptiveWindowName",
                "menubar=yes,location=yes,resizable=yes,scrollbars=yes,status=yes");

        }
        function DialogBoxMail(Path) {

            REDIPS.dialog.show(500, 300, '<iframe id="output" src="' + Path + '" height="500px" width="700"></iframe>');
            return false;

        }
        $(document).ready(function () {
            if (window.innerHeight > 0) { $("#BarraLaterale").css("height", (window.innerHeight - 94) + "px"); } else { $("#BarraLaterale").css("height", (document.documentElement.offsetHeight - 94) + "px"); }
        });
    </script>
</head>
<body>
    <form id="form1" runat="server">
        <asp:ScriptManager ID="ScriptManager1" runat="server" EnableScriptGlobalization="true">
            <Scripts>
                <asp:ScriptReference Path="~/Scripts/AjaxControlToolkit/Bundle" />
            </Scripts>
        </asp:ScriptManager>
        <asp:Label ID="Lbl_BarraSenior" runat="server" Text=""></asp:Label>
        <div>
            <asp:Button ID="Btn_RefreshPostali" runat="server" Text="" Visible="false" />
            <table style="width: 100%;" cellpadding="0" cellspacing="0">
                <tr>
                    <td style="width: 140px; background-color: #F0F0F0;"></td>
                    <td>
                        <div class="Titolo">Protocollo - Rubrica</div>
                        <div class="SottoTitolo">
                            <br />
                            <br />
                        </div>
                    </td>
                    <td style="text-align: right; vertical-align: top;">
                        <div class="DivTasti">
                            <asp:ImageButton ID="Btn_Duplica" Height="38px" runat="server" ImageUrl="~/images/duplica.png" class="EffettoBottoniTondi" ToolTip="Duplica" Visible="false" />&nbsp;     
        <asp:ImageButton ID="Btn_Elimina" runat="server" ImageUrl="images\elimina.jpg" ToolTip="Elimina" />&nbsp;
        <asp:ImageButton ID="Btn_Modifica" runat="server" ImageUrl="images\salva.jpg" ToolTip="Modifica / Inserisci" />
                        </div>
                    </td>
                </tr>

                <tr>
                    <td style="width: 140px; background-color: #F0F0F0; vertical-align: top; text-align: center;" id="BarraLaterale">
                        <a href="Menu_Rubrica.aspx">
                            <img src="images/Home.jpg" alt="Menù" title="Menù" /></a>
                        <asp:ImageButton ID="Btn_Esci" runat="server" BackColor="Transparent" ImageUrl="../images/Menu_Indietro.png" ToolTip="Chiudi" />
                        <br />
                        <asp:Label ID="Lbl_Organizzazione" runat="server" Text=""></asp:Label>
                    </td>
                    <td colspan="2" style="background-color: #FFFFFF; vertical-align: top;">
                        <AJAX:TabContainer ID="TabContainer1" runat="server" ActiveTabIndex="0" Height="665px" Width="100%" CssClass="TabSenior">
                            <AJAX:TabPanel runat="server" HeaderText="Rubrica" ID="TabPanel1">
                                <ContentTemplate>
                                    <br />
                                    <label style="display: block; float: left; width: 180px;">Id :</label>
                                    <asp:TextBox ID="Txt_ID" onkeypress="return handleEnter(this, event)" Enabled="false" Width="70px" runat="server"></asp:TextBox>

                                    <br />
                                    <br />
                                    <label style="display: block; float: left; width: 180px;">Appellativo :</label>
                                    <asp:DropDownList ID="DD_Appellativo" runat="server"></asp:DropDownList>
                                    <br />
                                    <br />

                                    <label style="display: block; float: left; width: 180px;">Denominazione :<font color="red">*</font></label>
                                    <asp:TextBox ID="Txt_Denominazione" onkeypress="return handleEnter(this, event)" MaxLength="50" Width="600px" runat="server"></asp:TextBox>
                                    <asp:Button ID="Btn_Open1" runat="server" Text="Apri" />


                                    <br />
                                    <br />

                                    <label style="display: block; float: left; width: 180px;">Cognome :<font color="red">*</font></label>
                                    <asp:TextBox ID="Txt_Cognome" onkeypress="return handleEnter(this, event)" MaxLength="50" Width="400px" runat="server"></asp:TextBox>
                                    <asp:Button ID="Btn_Open2" runat="server" Text="Apri" />



                                    <br />
                                    <br />

                                    <label style="display: block; float: left; width: 180px;">Nome :</label>
                                    <asp:TextBox ID="Txt_Nome" onkeypress="return handleEnter(this, event)" MaxLength="30" Width="400px" runat="server"></asp:TextBox>



                                    <br />
                                    <br />


                                    <label style="display: block; float: left; width: 180px;">Indirizzo :</label>
                                    <asp:TextBox ID="Txt_Indirizzo" onkeypress="return handleEnter(this, event)" MaxLength="50" Width="500px" runat="server"></asp:TextBox>


                                    <br />
                                    <br />

                                    <label style="display: block; float: left; width: 180px;">Comune :</label>
                                    <asp:TextBox ID="Txt_Comune" onkeypress="return handleEnter(this, event)" MaxLength="30" Width="400px" runat="server"></asp:TextBox>

                                    <br />
                                    <br />

                                    <label style="display: block; float: left; width: 180px;">Località :</label>
                                    <asp:TextBox ID="Txt_Localita" onkeypress="return handleEnter(this, event)" MaxLength="30" Width="400px" runat="server"></asp:TextBox>

                                    <br />
                                    <br />

                                    <label style="display: block; float: left; width: 180px;">CAP :</label>
                                    <asp:TextBox ID="Txt_Cap" onkeypress="return handleEnter(this, event)" MaxLength="6" Width="100px" runat="server"></asp:TextBox>


                                    <br />
                                    <br />

                                    <label style="display: block; float: left; width: 180px;">Telefono 1 :</label>
                                    <asp:TextBox ID="Txt_Telefono1" onkeypress="return handleEnter(this, event)" MaxLength="30" Width="100px" runat="server"></asp:TextBox>

                                    <br />
                                    <br />

                                    <label style="display: block; float: left; width: 180px;">Telefono  2 :</label>
                                    <asp:TextBox ID="Txt_Telefono2" onkeypress="return handleEnter(this, event)" MaxLength="30" Width="100px" runat="server"></asp:TextBox>


                                    <br />
                                    <br />

                                    <label style="display: block; float: left; width: 180px;">EMail 1:</label>
                                    <asp:TextBox ID="Txt_Mail1" onkeypress="return handleEnter(this, event)" MaxLength="30" Width="100px" runat="server"></asp:TextBox>
                                    <br />
                                    <br />
                                    <label style="display: block; float: left; width: 180px;">EMail 2:</label>
                                    <asp:TextBox ID="Txt_Mail2" onkeypress="return handleEnter(this, event)" MaxLength="30" Width="100px" runat="server"></asp:TextBox>

                                    <br />
                                    <br />

                                    <label style="display: block; float: left; width: 180px;">Raggruppamento :<font color="red">*</font></label>
                                    <asp:DropDownList ID="DD_Raggruppamento" runat="server"></asp:DropDownList>
                                    <br />
                                    <br />

                                    <label style="display: block; float: left; width: 180px;">Invio Comunicazione :<font color="red">*</font></label>
                                    <asp:DropDownList ID="DD_InvioComunicazione" runat="server">
                                        <asp:ListItem Value="99" Text="" Selected></asp:ListItem>
                                        <asp:ListItem Value="0" Text="NO"></asp:ListItem>
                                        <asp:ListItem Value="1" Text="SI"></asp:ListItem>
                                    </asp:DropDownList>
                                    <br />
                                    <br />
                                </ContentTemplate>
                            </AJAX:TabPanel>
                            <AJAX:TabPanel runat="server" HeaderText="Note" ID="TabPanel2">
                                <ContentTemplate>


                                    <label style="display: block; float: left; width: 150px;">Note :</label><br />
                                    <asp:TextBox ID="Txt_Note" onkeypress="return handleEnter(this, event)" TextMode="MultiLine" Width="90%" Height="300px" runat="server"></asp:TextBox>

                                </ContentTemplate>
                            </AJAX:TabPanel>

                            <AJAX:TabPanel runat="server" HeaderText="Consensi" ID="TabPanel4">
                                <ContentTemplate>
                                    <p style="text-align: center;">Consenso all’inserimento dei dati personali nel sistema (l’informativa è stata compresa e firmata dall’interessato)</p>
                                    <p style="text-align: center;">
                                        <asp:RadioButton ID="RB_SIConsenso1" runat="server" Text="SI" GroupName="Consenso1" />
                                        <asp:RadioButton ID="RB_NOConsenso1" runat="server" Text="NO" GroupName="Consenso1" />
                                    </p>
                                    <br />
                                    <p style="text-align: center;">Consenso al trattamento per finalita’ di marketing</p>
                                    <p style="text-align: center;">
                                        <asp:RadioButton ID="RB_SIConsenso2" runat="server" Text="SI" GroupName="Consenso2" />
                                        <asp:RadioButton ID="RB_NOConsenso2" runat="server" Text="NO" GroupName="Consenso2" />
                                    </p>
                                    <br />

                                    <label class="LabelCampo">Seleziona Report:</label>
                                    <asp:DropDownList ID="DD_Report" runat="server"></asp:DropDownList>

                                    <asp:ImageButton runat="server" ImageUrl="~/images/download.png" OnClientClick="javascript:return openRequestedPopup();" class="EffettoBottoniTondi" Height="38px" ToolTip="Download" ID="IB_Download"></asp:ImageButton>

                                </ContentTemplate>

                            </AJAX:TabPanel>
                        </AJAX:TabContainer>
                    </td>
                </tr>
            </table>


        </div>
    </form>
</body>
</html>
