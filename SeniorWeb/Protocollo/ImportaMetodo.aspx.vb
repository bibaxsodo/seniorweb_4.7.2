﻿Imports Microsoft.VisualBasic
Imports System.Data.OleDb
Imports System.Web.Hosting
Partial Class Protocollo_ImportaMetodo
    Inherits System.Web.UI.Page





    Function campodb(ByVal oggetto As Object) As String
        If IsDBNull(oggetto) Then
            Return ""
        Else
            Return oggetto
        End If
    End Function




    Protected Sub Protocollo_ImportaMetodo_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Trim(Session("UTENTE")) = "" Then
            Response.Redirect("Login.aspx")
            Exit Sub
        End If

        If Page.IsPostBack = True Then Exit Sub


        Dim Barra As New Cls_BarraSenior


        Lbl_BarraSenior.Text = Barra.CodiceBarra(Application("SENIOR"), Session("UTENTE"), Page)
    End Sub

    Protected Sub Btn_Esci_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles Btn_Esci.Click
        Response.Redirect("Menu_Protocollo.aspx")

    End Sub

    Protected Sub ImageButton3_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles ImageButton3.Click
        Dim DataBaseMetodo As String = ""


        If Session("NomeEPersonam") = "MAGIERA" Then
            DataBaseMetodo = "Provider=SQLOLEDB;Data Source=localhost\sqlexpress;Initial Catalog=ASPMAGANS;User Id=sa;Password=advenias2012;"
        End If

        If Session("NomeEPersonam") = "CHARITAS" Then
            DataBaseMetodo = "Provider=SQLOLEDB;Data Source=localhost\sqlexpress;Initial Catalog=ASPCHARITAS;User Id=sa;Password=advenias2012;"
        End If




        Dim cn As OleDbConnection
        Dim cnMetodo As OleDbConnection



        cn = New Data.OleDb.OleDbConnection(Session("DC_OSPITE"))

        cnMetodo = New Data.OleDb.OleDbConnection(DataBaseMetodo)


        cn.Open()
        cnMetodo.Open()

        Dim cmd As New OleDbCommand()

        cmd.CommandText = ("select * from ANAGRAFICACF  where TipoConto = 'F' order by DataModifica")
        cmd.Connection = cnMetodo
        Dim sb As StringBuilder = New StringBuilder

        Dim myPOSTreader As OleDbDataReader = cmd.ExecuteReader()
        Do While myPOSTreader.Read
            Dim CodiceFiscale As String
            Dim PIVA As String
            Dim Trovato As Boolean = False

            CodiceFiscale = campodb(myPOSTreader.Item("CODFISCALE"))
            PIVA = campodb(myPOSTreader.Item("PARTITAIVA"))


            Dim cmdRead As New OleDbCommand()

            cmdRead.CommandText = ("select * from AnagraficaComune where CODICEFISCALE = ?")
            cmdRead.Parameters.AddWithValue("@CODICEFISCALE", CodiceFiscale)
            cmdRead.Connection = cn
            Dim Reader1 As OleDbDataReader = cmdRead.ExecuteReader()
            If Reader1.Read Then
                Trovato = True
            End If
            Reader1.Close()

            Dim cmdRead1 As New OleDbCommand()

            cmdRead1.CommandText = ("select * from AnagraficaComune where PARTITAIVA = ?")
            cmdRead1.Parameters.AddWithValue("@PARTITAIVA", PIVA)
            cmdRead1.Connection = cn
            Dim Reader2 As OleDbDataReader = cmdRead1.ExecuteReader()
            If Reader2.Read Then
                Trovato = True
            End If
            Reader2.Close()


            If Trovato = False Then
                Lbl_ImportaMetodo.Text = Lbl_ImportaMetodo.Text & "<b>Anagrafica " & campodb(myPOSTreader.Item("DSCCONTO1")) & "</b><br/>"
                Dim k As New Cls_ClienteFornitore

                k.Nome = campodb(myPOSTreader.Item("DSCCONTO1"))
                k.RESIDENZAINDIRIZZO1 = campodb(myPOSTreader.Item("INDIRIZZO"))
                k.RESIDENZACAP1 = campodb(myPOSTreader.Item("CAP"))

                Dim xComune As New ClsComune

                xComune.Descrizione = campodb(myPOSTreader.Item("LOCALITA"))
                xComune.LeggiDescrizione(Session("DC_OSPITE"))
                If xComune.CodificaProvincia = "" Then
                    k.RESIDENZACOMUNE1 = xComune.Comune
                    k.RESIDENZAPROVINCIA1 = xComune.Provincia
                End If

                k.Telefono1 = campodb(myPOSTreader.Item("TELEFONO"))
                k.Telefono2 = campodb(myPOSTreader.Item("FAX"))

                k.PARTITAIVA = campodb(myPOSTreader.Item("PARTITAIVA"))
                k.CodiceFiscale = campodb(myPOSTreader.Item("CODFISCALE"))

                k.Scrivi(Session("DC_OSPITE"), Session("DC_GENERALE"))

            End If


        Loop
        myPOSTreader.Close()



        cn.Close()

        cnMetodo.Close()

    End Sub
End Class
