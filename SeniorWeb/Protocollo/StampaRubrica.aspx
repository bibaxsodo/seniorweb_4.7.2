﻿<%@ Page Language="VB" AutoEventWireup="false" Inherits="StampaRubrica" CodeFile="StampaRubrica.aspx.vb" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit.HTMLEditor" TagPrefix="AJAX" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="xasp" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
    <title>Stampa Rubrica</title>
    <asp:PlaceHolder runat="server">
        <%: System.Web.Optimization.Styles.Render("~/Content/AjaxControlToolkit/Styles/Bundle") %>
    </asp:PlaceHolder>
    <link rel="stylesheet" href="css/csv.css?Versione=8" type="text/css" />
    <link rel="shortcut icon" href="../images/SENIOR.ico" />
    <link href="js/jquery.autocomplete.css" rel="stylesheet" type="text/css" />

    <script src="js/jquery-1.5.1.min.js" type="text/javascript"></script>
    <script src="js/jquery.autocomplete.js" type="text/javascript"></script>
    <script src="js/soapclient.js" type="text/javascript"></script>
    <script src="/js/convertinumero.js" type="text/javascript"></script>

    <script src="js/jquery.maskedinput-1.3.min.js" type="text/javascript"></script>
    <script src="/js/formatnumer.js" type="text/javascript"></script>

    <script src="/js/pianoconti.js" type="text/javascript"></script>
    <script src="/js/formatnumer.js" type="text/javascript"></script>
    <script src="js/JSErrore.js" type="text/javascript"></script>
    <script src="js/NoEnter.js" type="text/javascript"></script>
    <script type="text/javascript">

</script>

    <style>
        #blur {
            width: 100%;
            background-color: black;
            moz-opacity: 0.5;
            khtml-opacity: .5;
            opacity: .5;
            filter: alpha(opacity=50);
            z-index: 120;
            height: 100%;
            position: absolute;
            top: 0;
            left: 0;
        }

        #progress {
            z-index: 200;
            background-color: White;
            position: absolute;
            top: 0pt;
            left: 0pt;
            border: solid 1px black;
            padding: 5px 5px 5px 5px;
            text-align: center;
        }

        .style1 {
            width: 1114px;
        }
    </style>
    <script type="text/javascript">
        $(document).ready(function () {
            if (window.innerHeight > 0) { $("#BarraLaterale").css("height", (window.innerHeight - 94) + "px"); } else { $("#BarraLaterale").css("height", (document.documentElement.offsetHeight - 94) + "px"); }
        });
    </script>
</head>
<body>
    <form id="form1" runat="server" autocomplete="off">
        <asp:ScriptManager ID="ScriptManager1" runat="server" EnableScriptGlobalization="true">
            <Scripts>
                <asp:ScriptReference Path="~/Scripts/AjaxControlToolkit/Bundle" />
            </Scripts>
        </asp:ScriptManager>
        <asp:Label ID="Lbl_BarraSenior" runat="server" Text=""></asp:Label>
        <div align="left">
            <table style="width: 100%;" cellpadding="0" cellspacing="0">
                <tr>
                    <td style="width: 140px; background-color: #F0F0F0;"></td>
                    <td>
                        <div class="Titolo">Protocollo - Stampa Rubrica</div>
                        <div class="SottoTitolo">
                            <br />
                            <br />
                        </div>
                    </td>
                    <td style="text-align: right; vertical-align: top;">
                        <div class="DivTasti">
                            <asp:UpdatePanel ID="UpdatePanel1" runat="server">
                                <ContentTemplate>
                                    <asp:ImageButton ID="Btn_Stampa" runat="server" ToolTip="Stampa Elenco" ImageUrl="images\printer-blue.png" />&nbsp;                                        
           <asp:ImageButton ID="Btn_Stampa_Rubrica" runat="server" ToolTip="Stampa Elenco" ImageUrl="images\printer-blue.png" />&nbsp;                                                   
                                </ContentTemplate>
                            </asp:UpdatePanel>
                        </div>
                    </td>
                </tr>

                <tr>
                    <td style="width: 140px; background-color: #F0F0F0; vertical-align: top; text-align: center;" id="BarraLaterale">
                        <a href="Menu_Rubrica.aspx" style="border-width: 0px;">
                            <img src="images/Home.jpg" alt="Menù" class="Effetto" /></a><br />
                        <asp:ImageButton ID="Btn_Esci" runat="server" BackColor="Transparent" ImageUrl="../images/Menu_Indietro.png" ToolTip="Chiudi" />
                        <br />
                        <br />
                        <br />
                        <br />
                        <br />
                        <br />
                        <br />
                        <br />
                        <br />
                        <br />
                        <br />
                        <br />
                        <br />
                        <br />
                        <br />
                        <br />
                        <br />
                        <br />
                        <br />
                        <br />
                        <br />
                        <br />
                        <br />
                        <br />
                        <br />
                        <br />
                        <br />
                        <br />
                        <br />
                        <br />
                    </td>
                    <td colspan="2" style="background-color: #FFFFFF;" valign="top">
                        <xasp:TabContainer ID="TabContainer1" runat="server" ActiveTabIndex="0" Height="700px" Width="99%" BorderStyle="None" Style="margin-right: 39px" CssClass="TabSenior">
                            <xasp:TabPanel runat="server" HeaderText="Stampa Rubrica" ID="Tab_Anagrafica">
                                <HeaderTemplate>
                                    Stampa Rubrica
                                </HeaderTemplate>
                                <ContentTemplate>


                                    <br />

                                    <br />

                                    <label style="display: block; float: left; width: 180px;">Raggruppamento :<font color="red">*</font></label>
                                    &nbsp;&nbsp;<asp:DropDownList ID="DD_Raggruppamento" runat="server"></asp:DropDownList>



                                    <br />
                                    <br />





                                    <label style="display: block; float: left; width: 180px;">Invio Comunicazione :<font color="red">*</font></label>
                                    &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<asp:DropDownList ID="DD_InvioComunicazione" runat="server">
                                        <asp:ListItem Value="99" Selected="True"></asp:ListItem>

                                        <asp:ListItem Value="0" Text="NO"></asp:ListItem>

                                        <asp:ListItem Value="1" Text="SI"></asp:ListItem>

                                    </asp:DropDownList>






                                    <br />
                                    &nbsp;&nbsp;
 
             <br />

                                    <asp:UpdateProgress ID="UpdateProgress2" runat="server"
                                        AssociatedUpdatePanelID="UpdatePanel1">
                                        <ProgressTemplate>


                                            <div id="blur">&nbsp;</div>


                                            <div id="progress" style="width: 200px; height: 50px; left: 40%; position: absolute; top: 372px; text-align: center;">
                                                Attendere prego.....<br />
                                                &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<img height="30px" src="images/loading.gif">
                                            </div>
                                        </ProgressTemplate>

                                    </asp:UpdateProgress>
                                    <br />
                                </ContentTemplate>
                            </xasp:TabPanel>
                        </xasp:TabContainer>
                    </td>
                </tr>
            </table>
        </div>
    </form>
</body>
</html>
