﻿Imports Microsoft.VisualBasic
Imports System.Data.OleDb
Imports System.Web.Hosting
Partial Class StampaRubrica
    Inherits System.Web.UI.Page

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load


        If Page.IsPostBack = True Then Exit Sub




        Dim Barra As New Cls_BarraSenior


        Lbl_BarraSenior.Text = Barra.CodiceBarra(Application("SENIOR"), Session("UTENTE"), Page)


        Dim k As New Cls_RaggruppamentoRubrica


        k.UpDropDown(Session("DC_TABELLE"), DD_Raggruppamento)
    End Sub


    Private Sub CaricaDBStampe()
        Dim Rubrica As New Xsd_StampaRubrica
        Dim Condizione As String

        Dim cn As OleDbConnection
        Dim I As Integer


        cn = New Data.OleDb.OleDbConnection(Session("DC_TABELLE"))

        cn.Open()
        Dim cmd As New OleDbCommand()


        Condizione = ""
        If DD_Raggruppamento.SelectedValue <> "" Then
            Condizione = Condizione & " Ragruppamento = '<" & DD_Raggruppamento.SelectedValue & ">'"
        End If
        If DD_InvioComunicazione.SelectedValue <> "99" Then
            If Condizione <> "" Then
                Condizione = Condizione & " And "
            End If
            Condizione = Condizione & " InvioComunicazione = " & DD_InvioComunicazione.SelectedValue
        End If

        If Condizione = "" Then
            cmd.CommandText = "Select * From Rubrica  Order By Cognome,Nome,Denominazione"
        Else
            cmd.CommandText = "Select * From Rubrica  Where " & Condizione & " Order By Cognome,Nome,Denominazione"
        End If

        cmd.Connection = cn

        Dim myPOSTreader As OleDbDataReader = cmd.ExecuteReader()
        Do While myPOSTreader.Read
            Dim k As System.Data.DataRow = Rubrica.Tables("Rubrica").NewRow


            k.Item("Appellativo") = campodb(myPOSTreader.Item("Appellativo"))
            k.Item("Denominazione") = campodb(myPOSTreader.Item("Denominazione"))
            k.Item("Cognome") = campodb(myPOSTreader.Item("Cognome"))
            k.Item("Nome") = campodb(myPOSTreader.Item("Nome"))
            k.Item("DataNascita") = campodb(myPOSTreader.Item("DataNascita"))
            k.Item("Indirizzo") = campodb(myPOSTreader.Item("Indirizzo"))
            k.Item("Provincia") = campodb(myPOSTreader.Item("Provincia"))
            k.Item("Comune") = campodb(myPOSTreader.Item("Comune"))
            k.Item("Cap") = campodb(myPOSTreader.Item("Cap"))
            k.Item("Localita") = campodb(myPOSTreader.Item("Localita"))
            k.Item("Ragruppamento") = campodb(myPOSTreader.Item("Ragruppamento"))
            k.Item("Note") = campodb(myPOSTreader.Item("Note"))
            k.Item("DataModifica") = campodb(myPOSTreader.Item("DataModifica"))
            k.Item("Mail") = campodb(myPOSTreader.Item("Mail"))
            k.Item("Utente") = campodb(myPOSTreader.Item("Utente"))
            k.Item("EMail1") = campodb(myPOSTreader.Item("EMail1"))
            k.Item("EMail2") = campodb(myPOSTreader.Item("EMail2"))
            k.Item("Telefono1") = campodb(myPOSTreader.Item("Telefono1"))
            k.Item("Telefono2") = campodb(myPOSTreader.Item("Telefono2"))
            k.Item("InvioComunicazione") = campodb(myPOSTreader.Item("InvioComunicazione"))


            Rubrica.Tables("Rubrica").Rows.Add(k)
        Loop
        myPOSTreader.Close()

        Session("stampa") = Rubrica
    End Sub

    Protected Sub Btn_Stampa_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles Btn_Stampa.Click


        Call CaricaDBStampe()

        ScriptManager.RegisterClientScriptBlock(Me, Me.GetType(), "Stampa", "window.open('Stampa_Report.aspx?REPORT=Stampa_Rubrica.rpt','Stampe','width=800,height=600');", True)

    End Sub

    Function campodb(ByVal oggetto As Object) As String
        If IsDBNull(oggetto) Then
            Return ""
        Else
            Return oggetto
        End If
    End Function

    Protected Sub Btn_Stampa_Rubrica_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles Btn_Stampa_Rubrica.Click
        Call CaricaDBStampe()

        ScriptManager.RegisterClientScriptBlock(Me, Me.GetType(), "Stampa", "window.open('Stampa_Report.aspx?REPORT=Stampa_Rubrica_Etichette.rpt','Stampe','width=800,height=600');", True)

    End Sub

    Protected Sub Btn_Esci_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles Btn_Esci.Click
        Response.Redirect("Menu_Rubrica.aspx")
    End Sub
End Class
