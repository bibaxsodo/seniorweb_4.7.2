﻿Imports System.Data.OleDb

Public Class Rubrica
    Implements System.Web.IHttpHandler, IRequiresSessionState

    Public Sub ProcessRequest(ByVal context As HttpContext) Implements IHttpHandler.ProcessRequest
        context.Response.ContentType = "text/plain"
        Dim RICERCA As String = context.Request.QueryString("q")
        Dim UTENTE As String = context.Request.QueryString("UTENTE")
        Dim sb As StringBuilder = New StringBuilder
        context.Response.Cache.SetCacheability(HttpCacheability.NoCache)


        Dim cn As OleDbConnection


        cn = New Data.OleDb.OleDbConnection(context.Session("DC_TABELLE"))

        cn.Open()
        Dim cmd As New OleDbCommand()

        cmd.Connection = cn

        If Val(RICERCA) > 0 Then
            cmd.CommandText = ("select * from Rubrica where    " &
                " ID = ?")

            cmd.Parameters.AddWithValue("@IdAnagrafica", Val(RICERCA))
        Else
            cmd.CommandText = ("select * from Rubrica where " &
                "Cognome + ' ' + NOME Like ? Or Denominazione Like ? ")

            cmd.Parameters.AddWithValue("@Descrizione", "%" & RICERCA & "%")
            cmd.Parameters.AddWithValue("@Descrizione", "%" & RICERCA & "%")
        End If
        Dim Counter As Integer = 0
        Dim myPOSTreader As OleDbDataReader = cmd.ExecuteReader()
        Do While myPOSTreader.Read

            sb.Append(myPOSTreader.Item("ID") & " " & myPOSTreader.Item("Denominazione") & " " & myPOSTreader.Item("Cognome") & " " & myPOSTreader.Item("Nome")).Append(Environment.NewLine)
            Counter = Counter + 1
            If Counter > 10 Then
                Exit Do
            End If
        Loop

        context.Response.Write(sb.ToString)
    End Sub

    Public ReadOnly Property IsReusable() As Boolean Implements IHttpHandler.IsReusable
        Get
            Return False
        End Get
    End Property

End Class