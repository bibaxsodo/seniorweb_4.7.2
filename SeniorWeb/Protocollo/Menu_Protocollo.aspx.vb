﻿
Partial Class Menu_Protocollo
    Inherits System.Web.UI.Page

    Protected Sub ImageButton1_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles ImageButton1.Click

        If Session("ABILITAZIONI").IndexOf("<PROTOCOLLO>") >= 0 Then
            Response.Redirect("../Login.aspx")
            Exit Sub
        End If

        Response.Redirect("../MainMenu.aspx")

    End Sub

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Trim(Session("UTENTE")) = "" Then
            Response.Redirect("Login.aspx")
            Exit Sub
        End If


        Dim Barra As New Cls_BarraSenior


        Lbl_BarraSenior.Text = Barra.CodiceBarra(Application("SENIOR"), Session("UTENTE"), Page)

        If Session("ABILITAZIONI").IndexOf("<NONPROTO>") > 0 Then
            Response.Redirect("..\MainMenu.aspx")
        End If
    End Sub
End Class
