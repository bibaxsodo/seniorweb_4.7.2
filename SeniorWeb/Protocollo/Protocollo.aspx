﻿<%@ Page Language="VB" AutoEventWireup="false" EnableEventValidation="false" Inherits="Protocollo" CodeFile="Protocollo.aspx.vb" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit.HTMLEditor" TagPrefix="cc2" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="AJAX" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
    <title>Protocollo</title>
    <asp:PlaceHolder runat="server">
        <%: System.Web.Optimization.Styles.Render("~/Content/AjaxControlToolkit/Styles/Bundle") %>
    </asp:PlaceHolder>
    <link rel="stylesheet" href="css/csv.css?Versione=8" type="text/css" />
    <link rel="shortcut icon" href="../images/SENIOR.ico" />
    <link href="js/jquery.autocomplete.css" rel="stylesheet" type="text/css" />
    <script src="js/jquery-1.5.1.min.js" type="text/javascript"></script>
    <script src="js/jquery.maskedinput-1.3.min.js" type="text/javascript"></script>
    <script src="js/jquery.autocomplete.js" type="text/javascript"></script>
    <script src="/js/formatnumer.js" type="text/javascript"></script>
    <script src="js/JSErrore.js" type="text/javascript"></script>
    <script src="js/NoEnter.js" type="text/javascript"></script>
    <link rel="stylesheet" href="dialogbox/redips-dialog.css" type="text/css" media="screen" />
    <script type="text/javascript" src="dialogbox/redips-dialog-min.js"></script>
    <script type="text/javascript" src="dialogbox/script.js"></script>
    <style>
        th {
            font-weight: normal;
        }

        .MenuDestra {
            font-family: "Cuprum","Roboto","Calibri",Lucida Grande,"Helvetica Neue",Helvetica,Arial,sans-serif;
            font-size: 16px;
            color: #777777;
            display: block;
            vertical-align: text-top;
            text-align: left;
            width: 150px;
            margin-left: 35px;
            padding: 3px 0px;
            text-transform: uppercase;
        }

            .MenuDestra a {
                color: #000000;
                text-decoration: none;
            }

                .MenuDestra a:hover {
                    //color :#007DC4;
                    color: white;
                }

            .MenuDestra.active {
                color: white;
                background-color: lightblue;
            }


            .MenuDestra:hover {
                background-color: lightblue;
            }
    </style>
    <script type="text/javascript">
        var windowObjectReference;

        function openRequestedPopup() {

            windowObjectReference = window.open("../Ospitiweb/PdfDaHtml.aspx",
                "DescriptiveWindowName",
                "menubar=yes,location=yes,resizable=yes,scrollbars=yes,status=yes");

        }

        $(document).ready(function () {
            if (window.innerHeight > 0) { $("#BarraLaterale").css("height", (window.innerHeight - 94) + "px"); } else { $("#BarraLaterale").css("height", (document.documentElement.offsetHeight - 94) + "px"); }
        });

        function Abilita(Oggetto) {
            if ($("#" + Oggetto).css('visibility') == 'visible') {
                $("#" + Oggetto).css('visibility', 'hidden');
            }
            else {
                $("#" + Oggetto).css('visibility', 'visible');
            }

        }
        function DialogBox(Path) {
            try {
                myTimeOut = setTimeout('window.location.href="/Seniorweb/Login.aspx";', sessionTimeout);
            }
            catch (err) {

            }

            var winW = 630, winH = 460;
            if (document.body && document.body.offsetWidth) {
                winW = document.body.offsetWidth;
                winH = document.body.offsetHeight;
            }
            if (document.compatMode == 'CSS1Compat' &&
                document.documentElement &&
                document.documentElement.offsetWidth) {
                winW = document.documentElement.offsetWidth;
                winH = document.documentElement.offsetHeight;
            }
            if (window.innerWidth && window.innerHeight) {
                winW = window.innerWidth;
                winH = window.innerHeight;
            }

            var APPO = winH - 100;

            REDIPS.dialog.show(winW - 200, winH - 100, '<iframe id="output" src="' + Path + '" height="' + APPO + '" width="100%" ></iframe>');
            return false;

        }
    </script>
</head>
<body>
    <form id="form1" runat="server">
        <asp:ScriptManager ID="ScriptManager1" runat="server" EnableScriptGlobalization="true">
            <Scripts>
                <asp:ScriptReference Path="~/Scripts/AjaxControlToolkit/Bundle" />
            </Scripts>
        </asp:ScriptManager>
        <asp:Label ID="Lbl_BarraSenior" runat="server" Text=""></asp:Label>
        <div>
            <table style="width: 100%;" cellpadding="0" cellspacing="0">
                <tr>
                    <td style="width: 140px; background-color: #F0F0F0;"></td>
                    <td>
                        <div class="Titolo">Protocollo - Gestione Protocollo</div>
                        <div class="SottoTitolo">
                            <asp:Label ID="Lbl_Titolo" runat="server" Text=""></asp:Label>
                            <br />
                            <br />
                        </div>
                    </td>
                    <td style="text-align: right; vertical-align: top;">
                        <div class="DivTasti">
                            <asp:ImageButton ID="Btn_Elimina" OnClientClick="return window.confirm('Eliminare?');" runat="server" ImageUrl="images\elimina.jpg" ToolTip="Elimina Protocllo" />&nbsp;
        <asp:ImageButton ID="Btn_Modifica" runat="server" ImageUrl="images\salva.jpg" ToolTip="Modifica / Inserisci" />
                        </div>
                    </td>
                </tr>

                <tr>
                    <td style="width: 140px; background-color: #F0F0F0; vertical-align: top; text-align: center;" id="BarraLaterale">
                        <a href="Menu_Protocollo.aspx">
                            <img src="images/Home.jpg" alt="Menù" title="Menù" /></a>
                        <asp:ImageButton ID="Btn_Esci" runat="server" BackColor="Transparent" ImageUrl="../images/Menu_Indietro.png" ToolTip="Chiudi" />
                        <label class="MenuDestra">&nbsp;&nbsp;<a href="#" onclick="DialogBox('Rubrica.aspx');">Rubrica</a></label><br />
                    </td>
                    <td colspan="2" style="background-color: #FFFFFF; vertical-align: top;">
                        <AJAX:TabContainer ID="TabContainer1" runat="server" ActiveTabIndex="0" Height="565px" Width="100%" CssClass="TabSenior">
                            <AJAX:TabPanel runat="server" HeaderText="Testata" ID="TabPanel1">
                                <ContentTemplate>
                                    <br />

                                    <label style="display: block; float: left; width: 150px;">Anno :<font color="red">*</font></label>
                                    <asp:TextBox ID="Txt_Anno" onkeypress="return handleEnterSoloNumero(this, event)" MaxLength="50" Width="100px" runat="server"></asp:TextBox>
                                    <br />
                                    <br />

                                    <label style="display: block; float: left; width: 150px;">Num. Prot. :</label>
                                    <asp:TextBox ID="Txt_Numero" onkeypress="return handleEnterSoloNumero(this, event)" BackColor="LightGray" Enabled="false" MaxLength="50" Width="100px" runat="server"></asp:TextBox>
                                    <br />
                                    <br />

                                    <label style="display: block; float: left; width: 150px;">Protocollo :<font color="red">*</font></label>
                                    <asp:DropDownList ID="DD_Tipo" AutoPostBack="true" runat="server">
                                    </asp:DropDownList><asp:Label ID="Lbl_TipoEU" runat="server" Text=""></asp:Label>
                                    <br />
                                    <br />

                                    <label style="display: block; float: left; width: 150px;">
                                        <asp:Label ID="Lbl_ufficiodestinatario" runat="server" Text="Ufficio destinatario :"></asp:Label></label>
                                    <asp:DropDownList ID="DD_UfficioDestinatario" AutoPostBack="true" runat="server"></asp:DropDownList>
                                    <br />
                                    <br />


                                    <label style="display: block; float: left; width: 150px;">Titolo :</label>
                                    <asp:DropDownList ID="DD_Titolo" AutoPostBack="true" runat="server">
                                    </asp:DropDownList>
                                    <br />
                                    <br />

                                    <label style="display: block; float: left; width: 150px;">Classe :</label>
                                    <asp:DropDownList ID="DD_Classe" AutoPostBack="true" runat="server">
                                    </asp:DropDownList>
                                    <br />
                                    <br />

                                    <label style="display: block; float: left; width: 150px;">Indice :</label>
                                    <asp:DropDownList ID="DD_Indice" runat="server">
                                    </asp:DropDownList>
                                    <br />
                                    <br />

                                    <label style="display: block; float: left; width: 150px;">
                                        <asp:Label ID="Label2" runat="server" Text="Fascicolo :"></asp:Label></label>
                                    <asp:DropDownList ID="DD_Fascicolo" AutoPostBack="true" runat="server"></asp:DropDownList><asp:Label ID="Lbl_segnala" runat="server" Text=""></asp:Label>
                                    <br />
                                    <br />

                                    <label style="display: block; float: left; width: 150px;">Fascicolo :</label>
                                    <asp:TextBox ID="Txt_Fascicolo" onkeypress="return handleEnter(this, event)" MaxLength="50" Width="100px" runat="server"></asp:TextBox>
                                    <br />
                                    <br />

                                    <label style="display: block; float: left; width: 150px;">Prot. Precedente :</label>
                                    <asp:TextBox ID="Txt_AnnoPrecedente" onkeypress="return handleEnterSoloNumero(this, event)" MaxLength="50" Width="100px" runat="server"></asp:TextBox>
                                    <asp:TextBox ID="Txt_NumeroPrecedente" onkeypress="return handleEnterSoloNumero(this, event)" MaxLength="50" Width="100px" runat="server"></asp:TextBox>
                                    <asp:DropDownList ID="DD_TipoPrecedente" runat="server"></asp:DropDownList>
                                    <a href="#" onclick="Abilita('Id_ProtPrecedenti');">
                                        <img src="images/information.jpg" /></a>

                                    <div id="Id_ProtPrecedenti" style="top: 310px; left: 330px; width: 300px; height: 350px; border-width: 1px; border-style: double; visibility: hidden; background-color: White; position: absolute;">
                                        <a href="#" onclick="Abilita('Id_ProtPrecedenti');">
                                            <img src="images/annulla.png" style="float: right;" /></a>
                                        <label style="display: block; float: left; width: 150px;">Oggetto :</label>
                                        <asp:TextBox ID="Txt_OggettoRPrec" runat="server" Width="200px"></asp:TextBox><br />
                                        <label style="display: block; float: left; width: 150px;">Data Dal :</label>
                                        <asp:TextBox ID="Txt_DataDalRPrec" runat="server"></asp:TextBox>
                                        <label style="display: block; float: left; width: 150px;">Data Al :</label>
                                        <asp:TextBox ID="Txt_DataAlRPrec" runat="server"></asp:TextBox><asp:Button ID="Btn_CercaPre" runat="server" Text="Cerca" /><br />
                                        <br />
                                        <asp:ListBox ID="Lst_ListaPrec" runat="server" Width="280px" Height="150px"
                                            AutoPostBack="True" OnSelectedIndexChanged="Lst_ListaPrec_SelectedIndexChanged"></asp:ListBox>
                                    </div>


                                    <br />
                                    <br />

                                    <label style="display: block; float: left; width: 150px;">Prot. Successivo :</label>
                                    <asp:TextBox ID="Txt_AnnoSuccessivo" onkeypress="return handleEnterSoloNumero(this, event)" MaxLength="50" Width="100px" runat="server"></asp:TextBox>
                                    <asp:TextBox ID="Txt_NumeroSuccessivo" onkeypress="return handleEnterSoloNumero(this, event)" MaxLength="50" Width="100px" runat="server"></asp:TextBox>
                                    <asp:DropDownList ID="DD_TipoSuccessivo" runat="server"></asp:DropDownList>
                                    <a href="#" onclick="Abilita('Id_ProtSuccessivo');">
                                        <img src="images/information.jpg" /></a>

                                    <div id="Id_ProtSuccessivo" style="top: 355px; left: 330px; width: 300px; height: 360px; border-width: 1px; border-style: double; visibility: hidden; background-color: White; position: absolute;">
                                        <a href="#" onclick="Abilita('Id_ProtSuccessivo');">
                                            <img src="images/annulla.png" style="float: right;" /></a>
                                        <label style="display: block; float: left; width: 150px;">Oggetto :</label>
                                        <asp:TextBox ID="Txt_OggettoRSuc" runat="server" Width="200px"></asp:TextBox><br />
                                        <label style="display: block; float: left; width: 150px;">Data Dal :</label>
                                        <asp:TextBox ID="Txt_DataDalRSuc" runat="server"></asp:TextBox>
                                        <label style="display: block; float: left; width: 150px;">Data Al :</label>
                                        <asp:TextBox ID="Txt_DataAlRSuc" runat="server"></asp:TextBox><asp:Button ID="Btn_CercaSuc" runat="server" Text="Cerca" /><br />
                                        <br />
                                        <asp:ListBox ID="Lst_ListaSuc" runat="server" Width="280px" Height="150px"
                                            AutoPostBack="True" OnSelectedIndexChanged="Lst_ListaSuc_SelectedIndexChanged"></asp:ListBox>
                                    </div>

                                    <br />
                                    <br />




                                    <label style="display: block; float: left; width: 150px;">Data Lettera :</label>
                                    <asp:TextBox ID="Txt_DataLettere" onkeypress="return handleEnter(this, event)" MaxLength="50" Width="100px" runat="server"></asp:TextBox>
                                    <br />
                                    <br />

                                    <label style="display: block; float: left; width: 150px;">Rif. Num. Doc. :</label>
                                    <asp:TextBox ID="Txt_NumeroDocumento" onkeypress="return handleEnter(this, event)" MaxLength="50" Width="100px" runat="server"></asp:TextBox>
                                    <br />
                                    <br />

                                    <label style="display: block; float: left; width: 150px;">
                                        <asp:Label ID="Lbl_DataArrivo" runat="server" Text="Data Arrivo :"></asp:Label></label>
                                    <asp:TextBox ID="Txt_DataArrivo" onkeypress="return handleEnter(this, event)" MaxLength="50" Width="100px" runat="server"></asp:TextBox>
                                    <br />
                                    <br />

                                    <label style="display: block; float: left; width: 150px;">
                                        <asp:Label ID="Label1" runat="server" Text="Ora :"></asp:Label></label>
                                    <asp:TextBox ID="Txt_OraArrivo" onkeypress="return handleEnter(this, event)" MaxLength="50" Width="100px" runat="server"></asp:TextBox>
                                    <br />
                                    <br />

                                    <label style="display: block; float: left; width: 150px;">Oggetto :<font color="red">*</font></label>
                                    <asp:TextBox ID="Txt_Oggetto" onkeypress="return handleEnter(this, event)" MaxLength="150" Width="600px" runat="server"></asp:TextBox>
                                    <br />
                                    <br />

                                    <label style="display: block; float: left; width: 150px;">Mezzo :<font color="red">*</font></label>
                                    <asp:DropDownList ID="DD_Mezzo" runat="server"></asp:DropDownList>
                                    <br />
                                    <br />



                                </ContentTemplate>
                            </AJAX:TabPanel>
                            <AJAX:TabPanel runat="server" HeaderText="Mittente/Destinatario da Anagrafica" ID="TabPanel2">
                                <ContentTemplate>


                                    <table>
                                        <tr>
                                            <td>
                                                <AJAX:TabContainer ID="Tab_Liste" runat="server" ActiveTabIndex="0" Height="300px" Width="450px" CssClass="TabSenior">
                                                    <AJAX:TabPanel runat="server" HeaderText="Rubrica" ID="TabPanelRubrica">
                                                        <ContentTemplate>
                                                            <label style="display: block; float: left; width: 150px;">Raggruppamento :</label>
                                                            <asp:DropDownList ID="DD_Ragruppamento" runat="server" Width="200px"></asp:DropDownList>
                                                            <br />
                                                            <asp:Button ID="Btn_EstraiRubrica" runat="server" Text="Ricerca" />
                                                            <br />

                                                            <div style="width: 420px; height: 200px; padding: 2px; overflow: auto; border: 1px solid #ccc;">
                                                                <asp:CheckBoxList ID="Lst_Rubrica" runat="server">
                                                                </asp:CheckBoxList>
                                                            </div>
                                                            <asp:LinkButton ID="Btn_SelectAllR" runat="server" Text="Seleziona Tutto"></asp:LinkButton>&nbsp;&nbsp;
              <asp:LinkButton ID="Btn_InvertiR" runat="server" Text="Inverti Selezione"></asp:LinkButton>&nbsp;&nbsp;
              <asp:LinkButton ID="Btn_AggiungiAnagraficaR" runat="server" Text="Aggiungi Protocollo"></asp:LinkButton>
                                                            <br />
                                                            <br />
                                                        </ContentTemplate>
                                                    </AJAX:TabPanel>

                                                    <AJAX:TabPanel runat="server" HeaderText="Clienti/Fornitori" ID="TabClientiFornitori" Visible="true">
                                                        <ContentTemplate>
                                                            <label style="display: block; float: left; width: 150px;">Ragione Sociale :</label>
                                                            <asp:TextBox ID="Txt_RagioneSociale" runat="server" Width="300px" Text=""></asp:TextBox>
                                                            <br />
                                                            <asp:Button ID="Btn_EstraiCliFor" runat="server" Text="Ricerca" />
                                                            <br />

                                                            <div style="width: 420px; height: 200px; padding: 2px; overflow: auto; border: 1px solid #ccc;">
                                                                <asp:CheckBoxList ID="Lst_ClientiFornitori" runat="server">
                                                                </asp:CheckBoxList>
                                                            </div>
                                                            <asp:LinkButton ID="Btn_SelecAllCF" runat="server" Text="Seleziona Tutto"></asp:LinkButton>&nbsp;&nbsp;
              <asp:LinkButton ID="Btn_InvertiCF" runat="server" Text="Inverti Selezione"></asp:LinkButton>&nbsp;&nbsp;
              <asp:LinkButton ID="Btn_AggiungiAnagraficaCF" runat="server" Text="Aggiungi Protocollo"></asp:LinkButton>
                                                            <br />
                                                            <br />
                                                        </ContentTemplate>
                                                    </AJAX:TabPanel>
                                                </AJAX:TabContainer>
                                            </td>
                                            <td>
                                                <label style="display: block; float: left; width: 150px;">Selezionato <font color="red">*</font>:</label><br />
                                                <div style="width: 450px; height: 300px; padding: 2px; overflow: auto; border: 1px solid #ccc;">
                                                    <asp:CheckBoxList ID="Chk_Selezionati" runat="server">
                                                    </asp:CheckBoxList>
                                                </div>
                                                <asp:LinkButton ID="Btn_Rimuovi" runat="server" Text="Rimuovi"></asp:LinkButton>
                                            </td>
                                        </tr>
                                    </table>

                                    <br />
                                    <br />
                                    <label style="display: block; float: left; width: 150px;">Cerca in anagrafica :</label>
                                    <asp:TextBox ID="Txt_Anagrafica" onkeypress="return handleEnter(this, event)" axLength="50" Width="600px" runat="server"></asp:TextBox>
                                    <asp:LinkButton ID="Btn_AggiungiAnagrafica" runat="server" Text="Aggiungi Protocollo"></asp:LinkButton>
                                    <br />
                                    <br />


                                </ContentTemplate>
                            </AJAX:TabPanel>
                            <AJAX:TabPanel runat="server" HeaderText="Dati anagrafici" ID="TabPanel3">
                                <ContentTemplate>
                                    <label style="display: block; float: left; width: 150px;">Dati Anagrafici :</label>
                                    <asp:TextBox ID="Txt_DatiAnagrafici" Enabled="true" onkeypress="return handleEnter(this, event)" MaxLength="150" Width="400px" runat="server"></asp:TextBox>
                                    <br />
                                    <br />
                                    <label style="display: block; float: left; width: 150px;">Indirizzo :</label>
                                    <asp:TextBox ID="Txt_Indirizzo" Enabled="true" onkeypress="return handleEnter(this, event)" MaxLength="150" Width="300px" runat="server"></asp:TextBox>
                                    <br />
                                    <br />
                                    <label style="display: block; float: left; width: 150px;">Citta :</label>
                                    <asp:TextBox ID="Txt_Citta" Enabled="true" onkeypress="return handleEnter(this, event)" MaxLength="150" Width="200px" runat="server"></asp:TextBox>
                                    <br />
                                    <br />
                                    <label style="display: block; float: left; width: 150px;">Cap :</label>
                                    <asp:TextBox ID="Txt_Cap" Enabled="true" onkeypress="return handleEnter(this, event)" MaxLength="150" Width="100px" runat="server"></asp:TextBox>
                                    <br />
                                    <br />
                                    <label style="display: block; float: left; width: 150px;">Provincia :</label>
                                    <asp:TextBox ID="Txt_Provincia" Enabled="true" onkeypress="return handleEnter(this, event)" MaxLength="2" Width="70px" runat="server"></asp:TextBox>
                                    <br />
                                    <br />
                                    <label style="display: block; float: left; width: 150px;">Stato :</label>
                                    <asp:TextBox ID="Txt_Stato" Enabled="true" onkeypress="return handleEnter(this, event)" MaxLength="150" Width="100px" runat="server"></asp:TextBox>
                                    <br />
                                    <br />
                                    <p style="text-align: center;">Consenso all’inserimento dei dati personali nel sistema (l’informativa è stata compresa e firmata dall’interessato)</p>
                                    <p style="text-align: center;">
                                        <asp:RadioButton ID="RB_SIConsenso1" runat="server" Text="SI" GroupName="Consenso1" />
                                        <asp:RadioButton ID="RB_NOConsenso1" runat="server" Text="NO" GroupName="Consenso1" />
                                    </p>
                                    <br />
                                    <p style="text-align: center;">Consenso al trattamento per finalita’ di marketing</p>
                                    <p style="text-align: center;">
                                        <asp:RadioButton ID="RB_SIConsenso2" runat="server" Text="SI" GroupName="Consenso2" />
                                        <asp:RadioButton ID="RB_NOConsenso2" runat="server" Text="NO" GroupName="Consenso2" />
                                    </p>
                                    <br />

                                    <label class="LabelCampo">Seleziona Report:</label>
                                    <asp:DropDownList ID="DD_Report" runat="server"></asp:DropDownList>

                                    <asp:ImageButton runat="server" ImageUrl="~/images/download.png" OnClientClick="javascript:return openRequestedPopup();" class="EffettoBottoniTondi" Height="38px" ToolTip="Download" ID="IB_Download"></asp:ImageButton>


                                </ContentTemplate>
                            </AJAX:TabPanel>
                            <AJAX:TabPanel runat="server" HeaderText="Note" ID="TabPanel4">
                                <ContentTemplate>
                                    <label style="display: block; float: left; width: 150px;">Note :</label><br />
                                    <asp:TextBox ID="Txt_Note" runat="server" TextMode="MultiLine" Height="300px" Width="600px"></asp:TextBox>
                                </ContentTemplate>
                            </AJAX:TabPanel>
                            <AJAX:TabPanel runat="server" HeaderText="Allegati" ID="TabPanel5">
                                <ContentTemplate>

                                    <asp:GridView ID="Grid" runat="server" CellPadding="4" Height="60px" Width="1000px"
                                        ShowFooter="True" BackColor="White" BorderColor="#CCCCCC"
                                        BorderStyle="Dotted" BorderWidth="1px">
                                        <RowStyle ForeColor="#333333" BackColor="White" />
                                        <Columns>
                                            <asp:TemplateField>
                                                <ItemTemplate>
                                                    <asp:ImageButton ID="IB_Modify" CommandName="Edit" runat="Server" ImageUrl="~/images/modifica.png" />
                                                </ItemTemplate>
                                                <EditItemTemplate>
                                                    <asp:ImageButton ID="IB_Update" CommandName="Update" runat="Server" ImageUrl="~/images/aggiorna.png" />
                                                    <asp:ImageButton ID="IB_Cancel" CommandName="Cancel" runat="Server" ImageUrl="~/images/annulla.png" />
                                                </EditItemTemplate>
                                            </asp:TemplateField>
                                            <asp:TemplateField>
                                                <ItemTemplate>
                                                    <asp:ImageButton ID="IB_Printer" CommandName="DOWNLOAD" runat="Server" ImageUrl="images/download.png" CommandArgument='<%#  Eval("NomeFile") %>' />
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                            <asp:TemplateField HeaderText="NOME FILE">
                                                <EditItemTemplate>
                                                    <asp:Label ID="LblNomeFile" runat="server" Text='<%# Eval("NomeFile") %>' Width="136px"></asp:Label>
                                                </EditItemTemplate>
                                                <ItemTemplate>
                                                    <asp:Label ID="LblNomeFile" runat="server" Text='<%# Eval("NomeFile") %>' Width="136px"></asp:Label>
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                            <asp:TemplateField HeaderText="DESCRIZIONE">
                                                <EditItemTemplate>
                                                    <asp:TextBox ID="TxtDescrizione" onkeypress="return handleEnter(this, event)" MaxLength="100" Width="300px" runat="server"></asp:TextBox>
                                                </EditItemTemplate>
                                                <ItemTemplate>
                                                    <asp:Label ID="LblDescrizione" runat="server" Text='<%# Eval("Descrizione") %>' Width="136px"></asp:Label>
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                            <asp:TemplateField HeaderText="DATA">
                                                <EditItemTemplate>
                                                    <asp:TextBox ID="TxtData" onkeypress="return handleEnter(this, event)" Width="100px" runat="server"></asp:TextBox>
                                                    <asp:ImageButton runat="server" ID="Img_DataDOC" ImageUrl="~/Images/calendario.png" Width="16px" Height="16px" AlternateText="Visualizza Calendario" />
                                                </EditItemTemplate>
                                                <ItemTemplate>
                                                    <asp:Label ID="LblData" runat="server" Text='<%# Eval("Data") %>' Width="136px"></asp:Label>
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                            <asp:TemplateField>
                                                <ItemTemplate>
                                                    <asp:ImageButton ID="IB_DELETEFILE" CommandName="CANCELLAFILE" runat="Server" ImageUrl="images/cancella.png" CommandArgument='<%#  Container.DataItemIndex  %>' />
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                        </Columns>
                                        <FooterStyle BackColor="White" ForeColor="#023102" />
                                        <PagerStyle BackColor="#336666" ForeColor="White" HorizontalAlign="Center" />
                                        <SelectedRowStyle BackColor="#339966" Font-Bold="True" ForeColor="White" />
                                        <HeaderStyle BackColor="#58A4D5" Font-Bold="True" ForeColor="Black" />
                                    </asp:GridView>
                                    Carica un file :<br />
                                    <asp:FileUpload ID="FileUpload1" runat="server" Width="599px" /><asp:ImageButton ID="UpLoadFile" runat="server" Height="48px" ImageUrl="~/images/upload.png" Width="48px" ToolTip="Up Load File" />
                                </ContentTemplate>
                            </AJAX:TabPanel>
                        </AJAX:TabContainer>


                    </td>
                </tr>
            </table>


        </div>
    </form>
</body>
</html>

