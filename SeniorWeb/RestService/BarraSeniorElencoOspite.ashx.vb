﻿Imports System.Data.OleDb
Imports System.Web
Imports System.Web.Services

Public Class BarraSeniorElencoOspite
    Implements System.Web.IHttpHandler, IRequiresSessionState

    Sub ProcessRequest(ByVal context As HttpContext) Implements IHttpHandler.ProcessRequest
        context.Response.ContentType = "text/plain"
        Dim CodiceBarra As String = ""
        Dim Pagina As String = context.Request.QueryString("Pagina")
        Dim ClientQueryString As String = context.Request.QueryString("ClientQueryString")
        context.Response.Cache.SetCacheability(HttpCacheability.NoCache)



        Dim k As New Cls_Login

        k.Utente = context.Session("UTENTE")
        k.LeggiSP(context.Application("SENIOR"))


        Dim HTML As String

        HTML = CaricaOspiti(context.Session("DC_OSPITE"), k.Utente, Pagina, context, ClientQueryString)

        context.Response.Write(HTML)

    End Sub

    ReadOnly Property IsReusable() As Boolean Implements IHttpHandler.IsReusable
        Get
            Return False
        End Get
    End Property

    Private Function CaricaOspiti(ByVal Connessione As String, ByVal Utente As String, ByVal NomePagina As String, ByVal context As HttpContext, ByVal ClientQueryString As String) As String
        Dim cn As OleDbConnection
        Dim Riga As Long = 0


        Dim k As New Cls_Login

        k.Utente = Utente
        k.LeggiSP(Connessione)

        cn = New Data.OleDb.OleDbConnection(Connessione)

        cn.Open()
        Dim cmd As New OleDbCommand()

        Dim CodiceBarra As String = ""

        Dim Indice As Integer

        Indice = 0

        'If NomePagina.ToUpper = "ospitiweb_Anagrafica_aspx".ToUpper Or NomePagina.ToUpper = "ospitiweb_DatiAnagrafici_aspx".ToUpper Or _
        '   NomePagina.ToUpper = "ospitiweb_DatiSanitari_aspx".ToUpper Or NomePagina.ToUpper = "ospitiweb_Residenza_aspx".ToUpper Or _
        '   NomePagina.ToUpper = "ospitiweb_RettaTotale_aspx".ToUpper Or NomePagina.ToUpper = "ospitiweb_StatoAuto_aspx".ToUpper Or _
        '   NomePagina.ToUpper = "ospitiweb_ModalitaCalcolo_aspx".ToUpper Or NomePagina.ToUpper = "ospitiweb_ImportoOspite_aspx".ToUpper Or _
        '   NomePagina.ToUpper = "ospitiweb_ImportoComune_aspx".ToUpper Or NomePagina.ToUpper = "ospitiweb_ImportoJolly_aspx".ToUpper Or _
        '   NomePagina.ToUpper = "ospitiweb_Impegnativa_aspx".ToUpper Or NomePagina.ToUpper = "ospitiweb_SelezionaParente_aspx".ToUpper Or _
        '   NomePagina.ToUpper = "ospitiweb_quotemensili_aspx".ToUpper Or NomePagina.ToUpper = "ospitiweb_movimenti_aspx".ToUpper Or _
        '   NomePagina.ToUpper = "ospitiweb_MesiDiurno_aspx".ToUpper Or NomePagina.ToUpper = "ospitiweb_Diurno_aspx".ToUpper Or NomePagina.ToUpper = "ospitiweb_ElencoRegistrazione_aspx".ToUpper Or _
        '   NomePagina.ToUpper = "ospitiweb_GrigliaAddebitiAccrediti_aspx".ToUpper Or NomePagina.ToUpper = "ospitiweb_ElencoMovimentiDenaro_aspx".ToUpper Or _
        '   NomePagina.ToUpper = "ospitiweb_Calcolo_aspx".ToUpper Or NomePagina.ToUpper = "ospitiweb_ricalcolo_aspx".ToUpper Or NomePagina.ToUpper = "ospitiweb_DocumentazioneLocal_aspx".ToUpper Or _
        '   NomePagina.ToUpper = "ospitiweb_FatturaNC_aspx".ToUpper Or NomePagina.ToUpper = "ospitiweb_IncassiAnticipi_aspx".ToUpper Or NomePagina.ToUpper = "ospitiweb_ElencoMovimentiStanze_aspx".ToUpper Then

        '    If Val(context.Session("CodiceOspite")) > 0 Then
        '        Dim cmddDeleteS As New OleDbCommand()
        '        cmddDeleteS.CommandText = "delete FROM UltimeRicerca Where CentroServizio = ? and CodiceOspite = ? AND UTENTE = ?"
        '        cmddDeleteS.Parameters.AddWithValue("@CentroServizio", context.Session("CodiceServizio"))
        '        cmddDeleteS.Parameters.AddWithValue("@CodiceOspite", context.Session("CodiceOspite"))
        '        cmddDeleteS.Parameters.AddWithValue("@UTENTE", context.Session("UTENTE"))
        '        cmddDeleteS.Connection = cn
        '        cmddDeleteS.ExecuteNonQuery()
        '    End If
        'End If

        'If cn.ServerVersion < "10" Then
        '    Dim cmddDelete As New OleDbCommand()
        '    cmddDelete.CommandText = "DELETE  FROM UltimeRicerca  where UTENTE = ? And DataOraRicerca < ?  "
        '    cmddDelete.Parameters.AddWithValue("@UTENTE", context.Session("UTENTE"))
        '    cmddDelete.Parameters.AddWithValue("@DataOraRicerca", DateAdd(DateInterval.Day, -10, Now))
        '    cmddDelete.Connection = cn
        '    cmddDelete.ExecuteNonQuery()
        'Else
        '    Dim cmddDelete As New OleDbCommand()
        '    cmddDelete.CommandText = "with todelete as (SELECT * FROM ( SELECT ROW_NUMBER() OVER (ORDER BY DataOraRicerca dESC) NUM,* FROM UltimeRicerca WHERE UTENTE = ? ) L WHERE NUM > 10) DELETE  FROM todelete;"
        '    cmddDelete.Parameters.AddWithValue("@UTENTE", context.Session("UTENTE"))
        '    cmddDelete.Connection = cn
        '    cmddDelete.ExecuteNonQuery()
        'End If

        Dim cmd1 As New OleDbCommand
        Dim Entrato As Boolean = False

        cmd1.CommandText = ("select AnagraficaComune.CodiceOspite as CodiceOSpite,AnagraficaComune.DataNascita, MOVIMENTI.Data, MOVIMENTI.CentroServizio as CentroServizio, Nome  from AnagraficaComune INNER JOIN MOVIMENTI ON AnagraficaComune.CODICEOSPITE = MOVIMENTI.CODICEOSPITE where TIPOLOGIA = 'O'  AND MOVIMENTI.DATA = (SELECT MAX(DATA) FROM MOVIMENTI AS K WHERE K.CODICEOSPITE = AnagraficaComune.CODICEOSPITE AND K.CentroServizio  = MOVIMENTI.CentroServizio) AND MOVIMENTI.PROGRESSIVO = (SELECT MAX(PROGRESSIVO) FROM MOVIMENTI AS K WHERE K.CODICEOSPITE = AnagraficaComune.CODICEOSPITE AND K.CentroServizio  = MOVIMENTI.CentroServizio And K.DATA = (SELECT MAX(DATA) FROM MOVIMENTI AS K1 WHERE K1.CODICEOSPITE = AnagraficaComune.CODICEOSPITE AND K1.CentroServizio  = MOVIMENTI.CentroServizio) )  And AnagraficaComune.CodiceOspite = " & Val(context.Session("CODICEOSPITE")) & "  ORDER BY NOME,Data ,CENTROSERVIZIO")
        cmd1.Connection = cn
        Dim Ps1 As OleDbDataReader = cmd1.ExecuteReader()
        Do While Ps1.Read
            If campodb(Ps1.Item("CentroServizio")) <> context.Session("CODICESERVIZIO") Then

                Dim Nome As String

                Nome = ""
                '"ospitiweb_Anagrafica_aspx".ToUpper Or NomePagina.ToUpper = "ospitiweb_DatiAnagrafici_aspx".ToUpper Then

                If ClientQueryString.ToUpper = "TIPO=EstrattoContoDenaro".ToUpper Then
                    Nome = "EstrattoContoDenaro.aspx"
                End If


                If ClientQueryString.ToUpper = "TIPO=MODIFICARETTA".ToUpper Then
                    Nome = "ModificaRetta.aspx"
                End If
                'MODIFICACSERV
                If ClientQueryString.ToUpper = "TIPO=MODIFICACSERV".ToUpper Then
                    Nome = "MODIFICACENTROSERVIZIO.aspx"
                End If

                'EstrattoContoDenaro
                If NomePagina.ToUpper = "ospitiweb_EstrattoContoDenaro_aspx".ToUpper Then
                    Nome = "EstrattoContoDenaro.aspx"
                End If

                If NomePagina.ToUpper = "ospitiweb_Anagrafica_aspx".ToUpper Then
                    Nome = "Anagrafica.aspx"
                End If
                If NomePagina.ToUpper = "ospitiweb_DatiAnagrafici_aspx".ToUpper Then
                    Nome = "DatiAnagrafici.aspx"
                End If

                If NomePagina.ToUpper = "ospitiweb_DatiSanitari_aspx".ToUpper Then
                    Nome = "DatiSanitari.aspx"
                End If

                If NomePagina.ToUpper = "ospitiweb_Residenza_aspx".ToUpper Then
                    Nome = "Residenza.aspx"
                End If
                If NomePagina.ToUpper = "ospitiweb_RettaTotale_aspx".ToUpper Then
                    Nome = "RettaTotale.aspx"
                End If
                If NomePagina.ToUpper = "ospitiweb_StatoAuto_aspx".ToUpper Then
                    Nome = "StatoAuto.aspx"
                End If

                If NomePagina.ToUpper = "ospitiweb_ModalitaCalcolo_aspx".ToUpper Then
                    Nome = "ModalitaCalcolo.aspx"
                End If

                If NomePagina.ToUpper = "ospitiweb_ImportoOspite_aspx".ToUpper Then
                    Nome = "ImportoOspite.aspx"
                End If
                If NomePagina.ToUpper = "ospitiweb_ImportoComune_aspx".ToUpper Then
                    Nome = "ImportoComune.aspx"
                End If
                If NomePagina.ToUpper = "ospitiweb_ImportoJolly_aspx".ToUpper Then
                    Nome = "ImportoJolly.aspx"
                End If
                If NomePagina.ToUpper = "ospitiweb_Impegnativa_aspx".ToUpper Then
                    Nome = "Impegnativa.aspx"
                End If
                If NomePagina.ToUpper = "ospitiweb_SelezionaParente_aspx".ToUpper Then
                    Nome = "SelezionaParente.aspx"
                End If
                If NomePagina.ToUpper = "ospitiweb_quotemensili_aspx".ToUpper Then
                    Nome = "quotemensili.aspx"
                End If
                If NomePagina.ToUpper = "ospitiweb_movimenti_aspx".ToUpper Then
                    Nome = "movimenti.aspx"
                End If
                If NomePagina.ToUpper = "ospitiweb_MesiDiurno_aspx".ToUpper Then
                    Nome = "MesiDiurno.aspx"
                End If
                If NomePagina.ToUpper = "ospitiweb_ElencoRegistrazione_aspx".ToUpper Then
                    Nome = "ElencoRegistrazione.aspx"
                End If

                If NomePagina.ToUpper = "ospitiweb_Diurno_aspx".ToUpper Then
                    Nome = "Diurno.aspx"
                End If
                If NomePagina.ToUpper = "ospitiweb_GrigliaAddebitiAccrediti_aspx".ToUpper Then
                    Nome = "GrigliaAddebitiAccrediti.aspx"
                End If
                If NomePagina.ToUpper = "ospitiweb_ElencoMovimentiDenaro_aspx".ToUpper Then
                    Nome = "ElencoMovimentiDenaro.aspx"
                End If
                If NomePagina.ToUpper = "ospitiweb_Calcolo_aspx".ToUpper Then
                    Nome = "Calcolo.aspx"
                End If
                If NomePagina.ToUpper = "ospitiweb_ricalcolo_aspx".ToUpper Then
                    Nome = "ricalcolo.aspx"
                End If
                If NomePagina.ToUpper = "ospitiweb_FatturaNC_aspx".ToUpper Then
                    Nome = "FatturaNC.aspx"
                End If
                If NomePagina.ToUpper = "ospitiweb_IncassiAnticipi_aspx".ToUpper Then
                    Nome = "IncassiAnticipi.aspx"
                End If
                If NomePagina.ToUpper = "ospitiweb_ElencoMovimentiStanze_aspx".ToUpper Then
                    Nome = "ElencoMovimentiStanze.aspx"
                End If

                If NomePagina.ToUpper.IndexOf("APIV1") > 0 Or NomePagina.ToUpper.IndexOf("PUSH") > 0 Or NomePagina.ToUpper.IndexOf("EXPORT") > 0 Then
                    Nome = "../" & Nome
                End If
                If Nome.Trim <> "" Then
                    CodiceBarra = CodiceBarra & "<li>"
                    CodiceBarra = CodiceBarra & "<a href=""" & Nome & "?CodiceOspite=" & campodb(Ps1.Item("CodiceOspite")) & "&CentroServizio=" & campodb(Ps1.Item("CentroServizio")) & """ >" & campodb(Ps1.Item("CentroServizio")) & " " & campodb(Ps1.Item("Nome")) & " " & campodb(Ps1.Item("DataNascita")) & "</a>"
                    CodiceBarra = CodiceBarra & "</li>"
                    Entrato = True
                End If

            End If
        Loop
        Ps1.Close()

        If Entrato Then
            CodiceBarra = CodiceBarra & "<li><hr>"
            CodiceBarra = CodiceBarra & "</li>"
        End If



        cmd.CommandText = ("select  CentroServizio,AnagraficaComune.CodiceOspite,DataNascita, Nome  FROM UltimeRicerca inner join AnagraficaComune  ON anagraficaComune.CODICEOSPITE = UltimeRicerca.CODICEOSPITE  aND TIPOLOGIA = 'O' where UltimeRicerca.UTENTE = ?  ORDER BY DATAORARICERCA DESC")
        cmd.Parameters.AddWithValue("@UTENTE", context.Session("UTENTE"))

        cmd.Connection = cn
        Dim myPOSTreader As OleDbDataReader = cmd.ExecuteReader()
        Do While myPOSTreader.Read

            CodiceBarra = CodiceBarra & "<li>"
            Dim Nome As String

            Nome = ""


            If NomePagina.ToUpper = "ospitiweb_EstrattoContoDenaro_aspx".ToUpper Then
                Nome = "EstrattoContoDenaro.aspx"
            End If

            If ClientQueryString.ToUpper = "TIPO=EstrattoContoDenaro".ToUpper Then
                Nome = "EstrattoContoDenaro.aspx"
            End If
            If ClientQueryString.ToUpper = "TIPO=MODIFICARETTA".ToUpper Then
                Nome = "ModificaRetta.aspx"
            End If
            If ClientQueryString.ToUpper = "TIPO=MODIFICACSERV".ToUpper Then
                Nome = "MODIFICACENTROSERVIZIO.aspx"
            End If



            '"ospitiweb_Anagrafica_aspx".ToUpper Or NomePagina.ToUpper = "ospitiweb_DatiAnagrafici_aspx".ToUpper Then
            If NomePagina.ToUpper = "ospitiweb_Anagrafica_aspx".ToUpper Then
                Nome = "Anagrafica.aspx"
            End If
            If NomePagina.ToUpper = "ospitiweb_DatiAnagrafici_aspx".ToUpper Then
                Nome = "DatiAnagrafici.aspx"
            End If

            If NomePagina.ToUpper = "ospitiweb_DatiSanitari_aspx".ToUpper Then
                Nome = "DatiSanitari.aspx"
            End If

            If NomePagina.ToUpper = "ospitiweb_Residenza_aspx".ToUpper Then
                Nome = "Residenza.aspx"
            End If
            If NomePagina.ToUpper = "ospitiweb_RettaTotale_aspx".ToUpper Then
                Nome = "RettaTotale.aspx"
            End If
            If NomePagina.ToUpper = "ospitiweb_StatoAuto_aspx".ToUpper Then
                Nome = "StatoAuto.aspx"
            End If

            If NomePagina.ToUpper = "ospitiweb_ModalitaCalcolo_aspx".ToUpper Then
                Nome = "ModalitaCalcolo.aspx"
            End If

            If NomePagina.ToUpper = "ospitiweb_ImportoOspite_aspx".ToUpper Then
                Nome = "ImportoOspite.aspx"
            End If
            If NomePagina.ToUpper = "ospitiweb_ImportoComune_aspx".ToUpper Then
                Nome = "ImportoComune.aspx"
            End If
            If NomePagina.ToUpper = "ospitiweb_ImportoJolly_aspx".ToUpper Then
                Nome = "ImportoJolly.aspx"
            End If
            If NomePagina.ToUpper = "ospitiweb_Impegnativa_aspx".ToUpper Then
                Nome = "Impegnativa.aspx"
            End If
            If NomePagina.ToUpper = "ospitiweb_SelezionaParente_aspx".ToUpper Then
                Nome = "SelezionaParente.aspx"
            End If
            If NomePagina.ToUpper = "ospitiweb_quotemensili_aspx".ToUpper Then
                Nome = "quotemensili.aspx"
            End If
            If NomePagina.ToUpper = "ospitiweb_movimenti_aspx".ToUpper Then
                Nome = "movimenti.aspx"
            End If
            If NomePagina.ToUpper = "ospitiweb_MesiDiurno_aspx".ToUpper Then
                Nome = "MesiDiurno.aspx"
            End If
            If NomePagina.ToUpper = "ospitiweb_ElencoRegistrazione_aspx".ToUpper Then
                Nome = "ElencoRegistrazione.aspx"
            End If
            If NomePagina.ToUpper = "OSPITIWEB_ELENCO_MOVIMENTIDOMICILIARE_ASPX".ToUpper Then
                Nome = "ELENCO_MOVIMENTIDOMICILIARE.aspx"
            End If


            If NomePagina.ToUpper = "ospitiweb_Diurno_aspx".ToUpper Then
                Nome = "Diurno.aspx"
            End If
            If NomePagina.ToUpper = "ospitiweb_GrigliaAddebitiAccrediti_aspx".ToUpper Then
                Nome = "GrigliaAddebitiAccrediti.aspx"
            End If
            If NomePagina.ToUpper = "ospitiweb_ElencoMovimentiDenaro_aspx".ToUpper Then
                Nome = "ElencoMovimentiDenaro.aspx"
            End If
            If NomePagina.ToUpper = "ospitiweb_Calcolo_aspx".ToUpper Then
                Nome = "Calcolo.aspx"
            End If
            If NomePagina.ToUpper = "ospitiweb_ricalcolo_aspx".ToUpper Then
                Nome = "ricalcolo.aspx"
            End If
            If NomePagina.ToUpper = "ospitiweb_FatturaNC_aspx".ToUpper Then
                Nome = "FatturaNC.aspx"
            End If
            If NomePagina.ToUpper = "ospitiweb_IncassiAnticipi_aspx".ToUpper Then
                Nome = "IncassiAnticipi.aspx"
            End If
            If NomePagina.ToUpper = "ospitiweb_ElencoMovimentiStanze_aspx".ToUpper Then
                Nome = "ElencoMovimentiStanze.aspx"
            End If

            If Nome.Trim = "" Then
                Nome = "Anagrafica.aspx"
            End If

            If NomePagina.ToUpper.IndexOf("APIV1") > 0 Or NomePagina.ToUpper.IndexOf("PUSH") > 0 Or NomePagina.ToUpper.IndexOf("EXPORT/") > 0 Or NomePagina.ToUpper.IndexOf("EXPORT\") > 0 Then
                If NomePagina.ToUpper.IndexOf("Menu_Export".ToUpper) < 0 Then
                    Nome = "../" & Nome
                End If
            End If

            CodiceBarra = CodiceBarra & "<a href=""" & Nome & "?CodiceOspite=" & campodb(myPOSTreader.Item("CodiceOspite")) & "&CentroServizio=" & campodb(myPOSTreader.Item("CentroServizio")) & """ >" & campodb(myPOSTreader.Item("CentroServizio")) & " " & campodb(myPOSTreader.Item("Nome")) & " " & campodb(myPOSTreader.Item("DataNascita")) & "</a>"
            CodiceBarra = CodiceBarra & "</li>"

            Indice = Indice + 1
        Loop
        myPOSTreader.Close()

        If Indice >= 15 Then
            CodiceBarra = CodiceBarra & "<li>"
            CodiceBarra = CodiceBarra & "<a href=""../RicercaAnagrafica.aspx?CHIAMATA=RITORNO"">" & "..." & "</a>"
            CodiceBarra = CodiceBarra & "</li>"

        End If

        'If NomePagina.ToUpper = "ospitiweb_Anagrafica_aspx".ToUpper Or NomePagina.ToUpper.ToUpper = "ospitiweb_DatiAnagrafici_aspx".ToUpper Or _
        '   NomePagina.ToUpper = "ospitiweb_DatiSanitari_aspx".ToUpper Or NomePagina.ToUpper.ToUpper = "ospitiweb_Residenza_aspx".ToUpper Or _
        '   NomePagina.ToUpper.ToUpper = "ospitiweb_RettaTotale_aspx".ToUpper Or NomePagina.ToUpper.ToUpper = "ospitiweb_StatoAuto_aspx".ToUpper Or _
        '   NomePagina.ToUpper.ToUpper = "ospitiweb_ModalitaCalcolo_aspx".ToUpper Or NomePagina.ToUpper.ToUpper = "ospitiweb_ImportoOspite_aspx".ToUpper Or _
        '   NomePagina.ToUpper.ToUpper = "ospitiweb_ImportoComune_aspx".ToUpper Or NomePagina.ToUpper.ToUpper = "ospitiweb_ImportoJolly_aspx".ToUpper Or _
        '   NomePagina.ToUpper.ToUpper = "ospitiweb_Impegnativa_aspx".ToUpper Or NomePagina.ToUpper.ToUpper = "ospitiweb_SelezionaParente_aspx".ToUpper Or _
        '   NomePagina.ToUpper.ToUpper = "ospitiweb_quotemensili_aspx".ToUpper Or NomePagina.ToUpper.ToUpper = "ospitiweb_movimenti_aspx".ToUpper Or _
        '   NomePagina.ToUpper.ToUpper = "ospitiweb_MesiDiurno_aspx".ToUpper Or NomePagina.ToUpper.ToUpper = "ospitiweb_Diurno_aspx".ToUpper Or NomePagina.ToUpper.ToUpper = "ospitiweb_ElencoRegistrazione_aspx".ToUpper Or _
        '   NomePagina.ToUpper.ToUpper = "ospitiweb_GrigliaAddebitiAccrediti_aspx".ToUpper Or NomePagina.ToUpper.ToUpper = "ospitiweb_ElencoMovimentiDenaro_aspx".ToUpper Or _
        '   NomePagina.ToUpper.ToUpper = "ospitiweb_Calcolo_aspx".ToUpper Or NomePagina.ToUpper.ToUpper = "ospitiweb_ricalcolo_aspx".ToUpper Or NomePagina.ToUpper.ToUpper = "ospitiweb_DocumentazioneLocal_aspx".ToUpper Or _
        '   NomePagina.ToUpper.ToUpper = "ospitiweb_FatturaNC_aspx".ToUpper Or NomePagina.ToUpper.ToUpper = "ospitiweb_IncassiAnticipi_aspx".ToUpper Or NomePagina.ToUpper.ToUpper = "ospitiweb_ElencoMovimentiStanze_aspx".ToUpper Then

        '    If Val(context.Session("CodiceOspite")) > 0 Then

        '        Dim cmdInsert As New OleDbCommand()

        '        cmdInsert.CommandText = "INSERT INTO UltimeRicerca ([DataOraRicerca],[CentroServizio],[CodiceOspite],Utente) VALUES (?,?,?,?) WHERE NOT EXISTS ( SELECT * FROM  UltimeRicerca Where CentroServizio = ? And CodiceOspite = ? And Utente = ?) "
        '        cmdInsert.Connection = cn
        '        cmdInsert.Parameters.AddWithValue("@DataRicerca", Now)
        '        cmdInsert.Parameters.AddWithValue("@CentroServizio", context.Session("CodiceServizio"))
        '        cmdInsert.Parameters.AddWithValue("@CodiceOspite", context.Session("CodiceOspite"))
        '        cmdInsert.Parameters.AddWithValue("@Utente", context.Session("UTENTE"))
        '        cmdInsert.Parameters.AddWithValue("@CentroServizio", context.Session("CodiceServizio"))
        '        cmdInsert.Parameters.AddWithValue("@CodiceOspite", context.Session("CodiceOspite"))
        '        cmdInsert.Parameters.AddWithValue("@Utente", context.Session("UTENTE"))
        '        cmdInsert.ExecuteNonQuery()
        '    End If
        'End If

        cn.Close()

        Return CodiceBarra
    End Function


    Function campodb(ByVal oggetto As Object) As String


        If IsDBNull(oggetto) Then
            Return ""
        Else
            Return oggetto
        End If
    End Function
End Class