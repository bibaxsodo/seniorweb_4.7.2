﻿Imports System.Data.OleDb

Public Class BarraSeniorElencoRegistrazione
    Implements System.Web.IHttpHandler, IRequiresSessionState

    Public Sub ProcessRequest(ByVal context As HttpContext) Implements IHttpHandler.ProcessRequest
        context.Response.ContentType = "text/plain"
        Dim CodiceBarra As String = ""
        Dim Pagina As String = context.Request.QueryString("Pagina")
        Dim ClientQueryString As String = context.Request.QueryString("ClientQueryString")
        context.Response.Cache.SetCacheability(HttpCacheability.NoCache)



        Dim k As New Cls_Login

        k.Utente = context.Session("UTENTE")
        k.LeggiSP(context.Application("SENIOR"))


        Dim HTML As String

        HTML = CaricaRegistrazioni(context.Session("DC_OSPITE"), k.Utente, Pagina, context, ClientQueryString)

        context.Response.Write(HTML)
    End Sub

    Public ReadOnly Property IsReusable() As Boolean Implements IHttpHandler.IsReusable
        Get
            Return False
        End Get
    End Property

    Private Function CaricaRegistrazioni(ByVal Connessione As String, ByVal Utente As String, ByVal NomePagina As String, ByVal context As HttpContext, ByVal ClientQueryString As String) As String
        Dim cn As OleDbConnection
        Dim cnTB As OleDbConnection
        Dim Riga As Long = 0
        Dim MySql As String = ""

        Dim k As New Cls_Login
        Dim Indice As Integer
        Indice = 0

        k.Utente = Utente
        k.LeggiSP(Connessione)

        cn = New Data.OleDb.OleDbConnection(context.Session("DC_GENERALE"))

        cn.Open()


        cnTB = New Data.OleDb.OleDbConnection(context.Session("DC_TABELLE"))

        cnTB.Open()

        Dim cmd As New OleDbCommand()
        Dim CodiceBarra As String = ""
        Dim DatiGenerali As New Cls_DatiGenerali

        DatiGenerali.LeggiDati(context.Session("DC_TABELLE"))

        Dim cmd1 As New OleDbCommand()
        Dim Record As String

        Record = "Top 15 *"


        MySql = "Select " & Record & " From UltimeRicerca inner join  MovimentiContabiliTesta on UltimeRicerca.NumeroRegistrazione =MovimentiContabiliTesta.NumeroRegistrazione WHERE UltimeRicerca.UTENTE = ? Order By DataOraRicerca Desc "

        cmd1.CommandText = MySql
        cmd1.Parameters.AddWithValue("@UTENTE", Utente)
        cmd1.Connection = cn
        Dim myPOSTreader As OleDbDataReader = cmd1.ExecuteReader()
        Do While myPOSTreader.Read

            CodiceBarra = CodiceBarra & "<li>"
            Dim Nome As String


            Dim MyCausali As New Cls_CausaleContabile

            MyCausali.Leggi(context.Session("DC_TABELLE"), campodb(myPOSTreader.Item("CausaleContabile")))

            Nome = "PrimaNota.aspx"


            If MyCausali.Tipo = "I" Or MyCausali.Tipo = "R" Then
                Nome = "Documenti.aspx"
            End If

            If MyCausali.Tipo = "P" Then
                Nome = "incassipagamenti.aspx"
            End If

            If DatiGenerali.ScadenziarioCheckChiuso = 1 And MyCausali.Tipo = "P" Then
                CodiceBarra = CodiceBarra & "<a href=""" & Nome & "?TIPO=SCADENZARIO&NumeroRegistrazione=" & campodb(myPOSTreader.Item("NumeroRegistrazione")) & """ >" & campodb(myPOSTreader.Item("NumeroRegistrazione")) & " " & campodb(myPOSTreader.Item("DataRegistrazione")) & " " & MyCausali.Descrizione & "</a>"
            Else
                CodiceBarra = CodiceBarra & "<a href=""" & Nome & "?NumeroRegistrazione=" & campodb(myPOSTreader.Item("NumeroRegistrazione")) & """ >" & campodb(myPOSTreader.Item("NumeroRegistrazione")) & " " & campodb(myPOSTreader.Item("DataRegistrazione")) & " " & MyCausali.Descrizione & "</a>"
            End If
            CodiceBarra = CodiceBarra & "</li>"

            Indice = Indice + 1
        Loop
        myPOSTreader.Close()

        If Indice >= 15 Then
            CodiceBarra = CodiceBarra & "<li>"
            CodiceBarra = CodiceBarra & "<a href=""UltimiMovimenti.aspx?CHIAMATA=RITORNO"">" & "..." & "</a>"
            CodiceBarra = CodiceBarra & "</li>"

        End If

        Return CodiceBarra
    End Function


    Function campodb(ByVal oggetto As Object) As String


        If IsDBNull(oggetto) Then
            Return ""
        Else
            Return oggetto
        End If
    End Function
End Class