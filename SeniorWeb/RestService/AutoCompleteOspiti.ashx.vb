﻿Imports System.Data.OleDb

Public Class AutoCompleteOspiti
    Implements System.Web.IHttpHandler

    Public Sub ProcessRequest(ByVal context As HttpContext) Implements IHttpHandler.ProcessRequest
        context.Response.Clear()
        context.Response.ContentType = "application/json"
        Dim RICERCA As String = context.Request.QueryString("q")
        Dim UTENTE As String = context.Request.QueryString("UTENTE")
        Dim CSERV As String = context.Request.QueryString("CSERV")
        context.Response.Cache.SetCacheability(HttpCacheability.NoCache)

        Dim sb As StringBuilder = New StringBuilder

        Dim cn As OleDbConnection
        Dim DbC As New Cls_Login

        DbC.Utente = UTENTE
        DbC.LeggiSP(context.Application("SENIOR"))

        cn = New Data.OleDb.OleDbConnection(DbC.Ospiti)

        cn.Open()
        Dim cmd As New OleDbCommand()

        Dim Valore1 As String = ""
        Dim Valore2 As String = ""

        Dim appoggio As String

        appoggio = Replace(Replace(Replace(RICERCA, ",", ""), ".", ""), " ", "")
        If IsNumeric(appoggio) And Len(appoggio) >= 2 Then
            Dim Vettore(100) As String

            Vettore = SplitWords(RICERCA)

            If Vettore.Length >= 2 Then
                Valore1 = Vettore(0)

                cmd.CommandText = ("select * from AnagraficaComune where Tipologia = 'O' And " &
                                   "CodiceOspite = ? ")
                cmd.Parameters.AddWithValue("@CodiceOspite", Valore1)
            Else

                cmd.CommandText = ("select * from AnagraficaComune where Tipologia = 'O' And " &
                                   "Nome Like ?")
                cmd.Parameters.AddWithValue("@Nome", "%" & RICERCA & "%")
            End If
        Else
            cmd.CommandText = ("select * from AnagraficaComune where Tipologia = 'O' And " &
                   "Nome Like ?")
            cmd.Parameters.AddWithValue("@Nome", "%" & RICERCA & "%")
        End If

        cmd.Connection = cn

        Dim Counter As Integer = 0
        Dim myPOSTreader As OleDbDataReader = cmd.ExecuteReader()
        Do While myPOSTreader.Read

            Dim k As New Cls_Movimenti

            k.CodiceOspite = Val(myPOSTreader.Item("CodiceOspite"))
            k.CENTROSERVIZIO = CSERV
            'If k.CServizioUsato(DbC.Ospiti) Then
            sb.Append(myPOSTreader.Item("CodiceOspite") & " " & myPOSTreader.Item("Nome")).Append(Environment.NewLine)
            Counter = Counter + 1
            If Counter > 10 Then
                Exit Do
            End If
            'End If

        Loop


        context.Response.Write(sb.ToString)
        context.Response.End()
    End Sub

    Public ReadOnly Property IsReusable() As Boolean Implements IHttpHandler.IsReusable
        Get
            Return False
        End Get
    End Property


    Private Function SplitWords(ByVal s As String) As String()
        '
        ' Call Regex.Split function from the imported namespace.
        ' Return the result array.
        '
        Return Regex.Split(s, "\W+")
    End Function
End Class