﻿Partial Class strumenti
    Inherits System.Web.UI.Page
    Dim TabellaGenerale As New System.Data.DataTable("TabellaGenerale")
    Dim TabellaOspiti As New System.Data.DataTable("TabellaOspiti")
    'Dim TabellaTurni As New System.Data.DataTable("TabellaTurni")

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Session("UTENTEIMPER") <> "" And Session("USER_SODO") = True And Session("UTENTE") = "" Then
            Exit Sub
        End If

        If Trim(Session("UTENTE")) = "" Then
            Response.Redirect("/SeniorWeb/Login.aspx")
            Exit Sub
        End If
        If Not IsNothing(Session("ABILITAZIONI")) Then
            If Session("ABILITAZIONI").IndexOf("<SOLO730>") >= 0 Then
                Response.Redirect("/SeniorWeb/Login.aspx")
                Exit Sub
            End If
        End If


        If Page.IsPostBack = True Then
            Exit Sub
        End If


        Dim K1 As New Cls_SqlString

        Dim Barra As New Cls_BarraSenior

        If Not IsNothing(Session("RicercaAnagraficaSQLString")) Then
            Try
                K1 = Session("RicercaAnagraficaSQLString")
            Catch ex As Exception

            End Try
        End If

        Lbl_BarraSenior.Text = Barra.CodiceBarra(Application("SENIOR"), Session("UTENTE"), Page, K1)

        Dim Kx As New Cls_Login
        Dim TipoStampa As String
        Dim Stampa As String
        Dim AppoG As Long
        Dim N As Integer
        Dim Inserisci As Boolean = False
        Dim TipiStampeGenerale(200) As String
        Dim TipiStampeOspiti(200) As String
        'Dim TipiStampeTurni(200) As String

        Dim MaxGenerale As Integer = 32
        Dim MaxOspiti As Integer = 22
        'Dim MaxTurni As Integer = 0


        TipiStampeGenerale(0) = "MASTRINO"
        TipiStampeGenerale(1) = "BILANCIO"
        TipiStampeGenerale(2) = "REGISTROIVA"
        TipiStampeGenerale(3) = "REGISTROIVATOT"
        TipiStampeGenerale(4) = "LIQUIDAZIONEIVA"

        TipiStampeGenerale(5) = "PIANOCONTI"
        TipiStampeGenerale(6) = "GIORNALECONTABILITA"
        TipiStampeGenerale(7) = "STAMPAREVERSALI"
        TipiStampeGenerale(8) = "STAMPAMANDATI"
        TipiStampeGenerale(9) = "STAMPADISTINTA"
        TipiStampeGenerale(10) = "BILANCIOIV"
        TipiStampeGenerale(11) = "PRIMANOTA"
        TipiStampeGenerale(12) = "PRIMANOTAGIORNO"
        TipiStampeGenerale(13) = "PRIMANOTAREGISTRAZIONE"
        TipiStampeGenerale(14) = "CLIENTIFORNITORI"
        TipiStampeGenerale(15) = "PIANOCONTICONTROLLO"
        TipiStampeGenerale(16) = "PIANOCONTIANOMALIE"
        TipiStampeGenerale(17) = "STAMPAFATTURACONTABILITA"
        TipiStampeGenerale(18) = "SALDI"
        TipiStampeGenerale(19) = "LIQUIDAZIONEIVA1"
        TipiStampeGenerale(20) = "LIQUIDAZIONEIVA2"
        TipiStampeGenerale(21) = "STAMPAFATTURACONTABILITA2"
        TipiStampeGenerale(22) = "STAMPAFATTURACONTABILITA3"
        TipiStampeGenerale(23) = "STAMPAFATTURACONTABILITA4"
        TipiStampeGenerale(24) = "STAMPAFATTURACONTABILITA5"
        TipiStampeGenerale(25) = "PROFORMAOPERATORI"
        TipiStampeGenerale(26) = "STAMPACESPITI"
        TipiStampeGenerale(27) = "STAMPARATEIRISCONTI"
        TipiStampeGenerale(28) = "APPALTIALLEGATI"
        TipiStampeGenerale(29) = "BILANCIOANALITICA"
        TipiStampeGenerale(30) = "APPALTIALLEGATI2"
        TipiStampeGenerale(31) = "APPALTISTAMPAPRESTAZIONI"
        TipiStampeGenerale(32) = "PRESTAZIONEPAZIENTE"

        'PrestazionePaziente

        'APPALTIALLEGATI

        'STAMPARATEIRISCONTI
        'STAMPACESPITI



        TipiStampeOspiti(0) = "FATTUREDETTAGLIATESANITARIO"
        TipiStampeOspiti(1) = "FATTUREDETTAGLIATESOCIALE"
        TipiStampeOspiti(2) = "STAMPADOCUMENTIOSPITI1"
        TipiStampeOspiti(3) = "STAMPADOCUMENTIOSPITI2"
        TipiStampeOspiti(4) = "STAMPADOCUMENTIOSPITI3"
        TipiStampeOspiti(5) = "STAMPADOCUMENTIOSPITI4"
        TipiStampeOspiti(6) = "DOCUMENTOPERSONALIZZATO1"
        TipiStampeOspiti(7) = "DOCUMENTOPERSONALIZZATO2"
        TipiStampeOspiti(8) = "DOCUMENTOPERSONALIZZATO3"
        TipiStampeOspiti(9) = "DOCUMENTOPERSONALIZZATO4"
        TipiStampeOspiti(10) = "RENDICONTOCOMUNE"
        TipiStampeOspiti(11) = "RENDICONTOREGIONE"
        TipiStampeOspiti(12) = "RICEVUTA"
        TipiStampeOspiti(13) = "STAMPASCHEDA"
        TipiStampeOspiti(14) = "RICEVUTADENARO"
        TipiStampeOspiti(15) = "SITUAZIONECONTABILE"
        TipiStampeOspiti(16) = "STAMPAETICHETTE"
        TipiStampeOspiti(17) = "ESTRATTOCONTO"
        TipiStampeOspiti(18) = "RENDICONTOCOMUNESOLORETTA"
        TipiStampeOspiti(19) = "RENDICONTOPERSONALIZZATO1"
        TipiStampeOspiti(20) = "RENDICONTOPERSONALIZZATO2"
        TipiStampeOspiti(21) = "RENDICONTOPERSONALIZZATO3"
        TipiStampeOspiti(22) = "RENDICONTOPERSONALIZZATO4"


        Kx.Utente = Session("UTENTE")
        Kx.LeggiSP(Application("SENIOR"))

        Txt_EMail.Text = Kx.EMail
        Txt_Smtp.Text = Kx.SMTP
        Txt_UserName.Text = Kx.UserName
        Txt_PasswordSmtp.Text = Kx.Passwordsmtp
        Txt_EMailUtente.Text = Kx.EmailUtente

        Chk_Porta587.Checked = False
        If Kx.Porta587 = 1 Then
            Chk_Porta587.Checked = True
        End If
        Chk_UsoSSl.Checked = False
        If Kx.SSL = 1 Then
            Chk_UsoSSl.Checked = True
        End If
        chk_UsaServerDefault.Checked = Kx.Usa_SMTP_Default
        Txt_Smtp.Enabled = Not Kx.Usa_SMTP_Default
        Txt_UserName.Enabled = Not Kx.Usa_SMTP_Default
        Txt_PasswordSmtp.Enabled = Not Kx.Usa_SMTP_Default
        Chk_Porta587.Enabled = Not Kx.Usa_SMTP_Default
        Chk_UsoSSl.Enabled = Not Kx.Usa_SMTP_Default

        Dim Vettore(100) As String
        Dim i As Integer
        If Not IsNothing(Kx.PERSONALIZZAZIONI) Then
            Vettore = SplitWords(Kx.PERSONALIZZAZIONI)
        End If

        TabellaGenerale.Clear()
        TabellaGenerale.Columns.Clear()
        TabellaGenerale.Columns.Add("Report", GetType(String))
        TabellaGenerale.Columns.Add("Personalizzazione", GetType(String))
        For i = 0 To MaxGenerale
            Dim myriga As System.Data.DataRow = TabellaGenerale.NewRow()
            myriga(0) = TipiStampeGenerale(i)
            myriga(1) = ""
            TabellaGenerale.Rows.Add(myriga)
        Next


        For i = 0 To Vettore.Length - 1
            If Not IsNothing(Vettore(i)) Then
                If Vettore(i).IndexOf("=") > 0 Then
                    TipoStampa = Mid(Vettore(i), 1, Vettore(i).IndexOf("="))
                    AppoG = Vettore(i).IndexOf("=") + 1
                    Stampa = Mid(Vettore(i), AppoG + 1, Len(Vettore(i)) - AppoG + 1)
                    For N = 0 To MaxGenerale
                        If TipoStampa = TipiStampeGenerale(N) Then
                            TabellaGenerale.Rows(N).Item(1) = Stampa
                        End If
                    Next
                End If
            End If
        Next


        Session("TabellaGenerale") = TabellaGenerale
        Call BindGenerale()



        TabellaOspiti.Clear()
        TabellaOspiti.Columns.Clear()
        TabellaOspiti.Columns.Add("Report", GetType(String))
        TabellaOspiti.Columns.Add("Personalizzazione", GetType(String))
        For i = 0 To MaxOspiti
            Dim myriga As System.Data.DataRow = TabellaOspiti.NewRow()
            myriga(0) = TipiStampeOspiti(i)
            myriga(1) = ""
            TabellaOspiti.Rows.Add(myriga)
        Next


        For i = 0 To Vettore.Length - 1
            If Not IsNothing(Vettore(i)) Then
                If Vettore(i).IndexOf("=") > 0 Then
                    TipoStampa = Mid(Vettore(i), 1, Vettore(i).IndexOf("="))
                    AppoG = Vettore(i).IndexOf("=") + 1
                    Stampa = Mid(Vettore(i), AppoG + 1, Len(Vettore(i)) - AppoG + 1)
                    For N = 0 To MaxOspiti
                        If TipoStampa = TipiStampeOspiti(N) Then
                            TabellaOspiti.Rows(N).Item(1) = Stampa
                        End If
                    Next
                End If
            End If
        Next


        Session("TabellaOspiti") = TabellaOspiti
        Call BindOspiti()

        Dim TabSoc As New Cls_TabellaSocieta

        Try
            TabSoc.Leggi(Session("DC_TABELLE"))
        Catch ex As Exception

        End Try


        Txt_RagioneSociale.Text = TabSoc.RagioneSociale
        Txt_Indirizzo.Text = TabSoc.Indirizzo
        Txt_Cap.Text = TabSoc.Cap
        Txt_Localita.Text = TabSoc.Localita
        Txt_Provincia.Text = TabSoc.Provincia

        Txt_CodiceFiscale.Text = TabSoc.CodiceFiscale
        Txt_PartitaIva.Text = TabSoc.PartitaIVA
        Txt_Telefono.Text = TabSoc.Telefono
        Txt_Banca.Text = TabSoc.BANCA
        Txt_Iban.Text = TabSoc.IBAN

        Txt_CodiceDitta.Text = TabSoc.CodiceDitta
        Txt_CodiceInstallazione.Text = TabSoc.CodiceInstallazione

        Txt_NomeFile.Text = TabSoc.NomeFile


        Txt_IBANRID.Text = TabSoc.IBANRID
        Txt_EndToEndId.Text = TabSoc.EndToEndId
        Txt_PrvtId.Text = TabSoc.PrvtId
        Txt_MmbId.Text = TabSoc.MmbId
        Txt_OrgId.Text = TabSoc.OrgId

        Txt_CodiceMittente.Text = TabSoc.CodiceMittente
        Txt_CodiceRicevente.Text = TabSoc.CodiceRicevente
        Txt_CodiceCab.Text = TabSoc.CODICECAB
        Txt_CodiceABi.Text = TabSoc.CODICEABI
        Txt_CodiceConto.Text = TabSoc.Conto

        Txt_EMailStruttura.Text = TabSoc.EMail

        If TabSoc.BOLLOVIRTUALE = 1 Then
            Chk_BolloVirtuale.Checked = True
        Else
            Chk_BolloVirtuale.Checked = False
        End If


        DD_RegimeFiscale.SelectedValue = TabSoc.REGIMEFISCALE
    End Sub

    Private Sub BindOspiti()
        TabellaOspiti = Session("TabellaOspiti")
        Grid.AutoGenerateColumns = False
        Grid.DataSource = TabellaOspiti
        Grid.DataBind()
    End Sub

    Private Sub BindGenerale()
        TabellaGenerale = Session("TabellaGenerale")
        GridGenerale.AutoGenerateColumns = False
        GridGenerale.DataSource = TabellaGenerale
        GridGenerale.DataBind()
    End Sub

    Protected Sub GridGenerale_RowCancelingEdit(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewCancelEditEventArgs) Handles GridGenerale.RowCancelingEdit
        GridGenerale.EditIndex = -1
        Call BindGenerale()
    End Sub

    Protected Sub GridGenerale_RowDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs) Handles GridGenerale.RowDataBound
        If e.Row.RowType = DataControlRowType.DataRow Then


            Dim LblPersonalizzazione As Label = DirectCast(e.Row.FindControl("LblPersonalizzazione"), Label)
            If Not IsNothing(LblPersonalizzazione) Then

            Else
                Dim TxtPersonalizzazione As TextBox = DirectCast(e.Row.FindControl("TxtPersonalizzazione"), TextBox)
                TxtPersonalizzazione.Text = TabellaGenerale.Rows(e.Row.RowIndex).Item(1).ToString

            End If
        End If
    End Sub
    Protected Sub GridGenerale_RowEditing(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewEditEventArgs) Handles GridGenerale.RowEditing
        GridGenerale.EditIndex = e.NewEditIndex
        Call BindGenerale()
    End Sub

    Protected Sub GridGenerale_RowUpdated(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewUpdatedEventArgs) Handles GridGenerale.RowUpdated

    End Sub

    Protected Sub GridGenerale_RowUpdating(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewUpdateEventArgs) Handles GridGenerale.RowUpdating
        Dim TxtPersonalizzazione As TextBox = DirectCast(GridGenerale.Rows(e.RowIndex).FindControl("TxtPersonalizzazione"), TextBox)


        TabellaGenerale = Session("TabellaGenerale")
        Dim row = GridGenerale.Rows(e.RowIndex)

        TabellaGenerale.Rows(row.DataItemIndex).Item(1) = TxtPersonalizzazione.Text



        Session("TabellaGenerale") = TabellaGenerale

        GridGenerale.EditIndex = -1
        Call BindGenerale()


    End Sub



    Protected Sub Grid_RowCancelingEdit(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewCancelEditEventArgs) Handles Grid.RowCancelingEdit
        Grid.EditIndex = -1
        Call BindOspiti()
    End Sub

    Protected Sub Grid_RowDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs) Handles Grid.RowDataBound
        If e.Row.RowType = DataControlRowType.DataRow Then


            Dim LblPersonalizzazione As Label = DirectCast(e.Row.FindControl("LblPersonalizzazione"), Label)
            If Not IsNothing(LblPersonalizzazione) Then

            Else
                Dim TxtPersonalizzazione As TextBox = DirectCast(e.Row.FindControl("TxtPersonalizzazione"), TextBox)
                TxtPersonalizzazione.Text = TabellaOspiti.Rows(e.Row.RowIndex).Item(1).ToString

            End If
        End If
    End Sub
    Protected Sub Grid_RowEditing(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewEditEventArgs) Handles Grid.RowEditing
        Grid.EditIndex = e.NewEditIndex
        Call BindOspiti()
    End Sub


    Protected Sub Grid_RowUpdating(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewUpdateEventArgs) Handles Grid.RowUpdating
        Dim TxtPersonalizzazione As TextBox = DirectCast(Grid.Rows(e.RowIndex).FindControl("TxtPersonalizzazione"), TextBox)


        TabellaOspiti = Session("TabellaOspiti")
        Dim row = Grid.Rows(e.RowIndex)

        TabellaOspiti.Rows(row.DataItemIndex).Item(1) = TxtPersonalizzazione.Text



        Session("TabellaOspiti") = TabellaOspiti

        Grid.EditIndex = -1
        Call BindOspiti()


    End Sub



    Private Function SplitWords(ByVal s As String) As String()
        '
        ' Call Regex.Split function from the imported namespace.
        ' Return the result array.
        '
        Return Regex.Split(s, " ")
    End Function

    Protected Sub BTN_Modifica_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles BTN_Modifica.Click
        Dim I As Integer
        Dim AppoggioString As String = ""
        TabellaGenerale = Session("TabellaGenerale")

        For I = 0 To TabellaGenerale.Rows.Count - 1
            AppoggioString = AppoggioString & TabellaGenerale.Rows(I).Item(0).ToString & "=" & TabellaGenerale.Rows(I).Item(1).ToString & " "
        Next

        TabellaOspiti = Session("TabellaOspiti")

        For I = 0 To TabellaOspiti.Rows.Count - 1
            AppoggioString = AppoggioString & TabellaOspiti.Rows(I).Item(0).ToString & "=" & TabellaOspiti.Rows(I).Item(1).ToString & " "
        Next

        'TabellaTurni = Session("TabellaTurni")

        'For I = 0 To TabellaTurni.Rows.Count - 1
        '    AppoggioString = AppoggioString & TabellaTurni.Rows(I).Item(0).ToString & "=" & TabellaTurni.Rows(I).Item(1).ToString & " "
        'Next


        Dim Kx As New Cls_Login
        Kx.Utente = Session("UTENTE")
        Kx.LeggiSP(Application("SENIOR"))
        Kx.PERSONALIZZAZIONI = AppoggioString
        Kx.Scrivi(Application("SENIOR"))


        Dim TabSoc As New Cls_TabellaSocieta

        TabSoc.Leggi(Session("DC_TABELLE"))


        TabSoc.RagioneSociale = Txt_RagioneSociale.Text
        TabSoc.Indirizzo = Txt_Indirizzo.Text
        TabSoc.Cap = Txt_Cap.Text
        TabSoc.Localita = Txt_Localita.Text
        TabSoc.Provincia = Txt_Provincia.Text

        TabSoc.CodiceFiscale = Txt_CodiceFiscale.Text
        TabSoc.PartitaIVA = Txt_PartitaIva.Text
        TabSoc.Telefono = Txt_Telefono.Text
        TabSoc.BANCA = Txt_Banca.Text
        TabSoc.IBAN = Txt_Iban.Text

        TabSoc.CodiceDitta = Txt_CodiceDitta.Text
        TabSoc.CodiceInstallazione = Txt_CodiceInstallazione.Text
        TabSoc.NomeFile = Txt_NomeFile.Text



        TabSoc.IBANRID = Txt_IBANRID.Text
        TabSoc.EndToEndId = Txt_EndToEndId.Text
        TabSoc.PrvtId = Txt_PrvtId.Text
        TabSoc.MmbId = Txt_MmbId.Text
        TabSoc.OrgId = Txt_OrgId.Text


        TabSoc.CodiceMittente = Txt_CodiceMittente.Text
        TabSoc.CodiceRicevente = Txt_CodiceRicevente.Text

        TabSoc.CODICEABI = Txt_CodiceABi.Text
        TabSoc.CODICECAB = Txt_CodiceCab.Text
        TabSoc.Conto = Txt_CodiceConto.Text


        TabSoc.EMail = Txt_EMailStruttura.Text

        If Chk_BolloVirtuale.Checked = True Then
            TabSoc.BOLLOVIRTUALE = 1
        Else
            TabSoc.BOLLOVIRTUALE = 0
        End If

        TabSoc.REGIMEFISCALE = DD_RegimeFiscale.SelectedValue

        TabSoc.Scrivi(Session("DC_TABELLE"))
    End Sub

    Protected Sub btn_DatiPersonali_ModificaPassword_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btn_DatiPersonali_ModificaPassword.Click

        If Txt_Password.Text <> Txt_Password2.Text Then
            Lbl_Errori.Text = "Le password non coincidono"
            Exit Sub
        End If
        If Len(Txt_Password.Text) < 6 Then
            Lbl_Errori.Text = "Password troppo corta"
            Exit Sub
        End If
        Dim Kx As New Cls_Login

        If Session("UTENTE") = "" Then
            Kx.Utente = Session("UTENTEIMPER")
        Else
            Kx.Utente = Session("UTENTE")
        End If

        Kx.LeggiSP(Application("SENIOR"))
        If Kx.EmailUtente.Trim = "" Then
            Lbl_Errori.Text = "Devi aver indicato e salvato la tua mail"
            Exit Sub
        End If


        Dim ChiaveCriptata As String = BCrypt.Net.BCrypt.HashPassword(Txt_Password.Text)

        If Session("UTENTE").ToString.IndexOf(">") > 0 Then
            Dim Appoggio As String = Session("UTENTE").ToString

            For i = 1 To 20
                Appoggio = Appoggio.Replace("<" & i & ">", "")
            Next i
            Kx.Utente = Appoggio
            Kx.LeggiSP(Application("SENIOR"))
            Kx.Chiave = Txt_Password.Text
            Kx.PasswordCriptata = ChiaveCriptata
            Kx.ScadenzaPassword = DateAdd(DateInterval.Month, 3, Now)
            Kx.Scrivi(Application("SENIOR"))

            For i = 1 To 20
                Kx.Utente = Appoggio & "<" & i & ">"
                Kx.Cliente = 0
                Kx.LeggiSP(Application("SENIOR"))
                If Kx.Cliente > 0 Then
                    Kx.Chiave = Txt_Password.Text
                    Kx.PasswordCriptata = ChiaveCriptata
                    Kx.ScadenzaPassword = DateAdd(DateInterval.Month, 3, Now)
                    Kx.Scrivi(Application("SENIOR"))
                End If
            Next
        Else
            If Session("UTENTE") = "" Then
                Kx.Utente = Session("UTENTEIMPER")
            Else
                Kx.Utente = Session("UTENTE")
            End If
            Kx.LeggiSP(Application("SENIOR"))
            Kx.Chiave = Txt_Password.Text
            Kx.PasswordCriptata = ChiaveCriptata
            Kx.ScadenzaPassword = DateAdd(DateInterval.Month, 3, Now)
            Kx.Scrivi(Application("SENIOR"))
        End If

        Response.Redirect("\Seniorweb\Login.aspx")
    End Sub

    Protected Sub btn_SaveMailSettings_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btn_SaveMailSettings.Click
        Dim Kx As New Cls_Login
        Kx.Utente = Session("UTENTE")
        Kx.LeggiSP(Application("SENIOR"))

        Kx.Usa_SMTP_Default = chk_UsaServerDefault.Checked
        Kx.EMail = Txt_EMail.Text

        Kx.SMTP = Txt_Smtp.Text
        Kx.UserName = Txt_UserName.Text
        Kx.Passwordsmtp = Txt_PasswordSmtp.Text

        If Chk_Porta587.Checked = True Then
            Kx.Porta587 = 1
        Else
            Kx.Porta587 = 0
        End If

        If Chk_UsoSSl.Checked = True Then
            Kx.SSL = 1
        Else
            Kx.SSL = 0
        End If

        Kx.Scrivi(Application("SENIOR"))
    End Sub

    Function EnCrypt(ByVal strCryptThis As String) As String
        Dim g_Key As String = "GIUV2SPBNI99212CON"
        Dim iCryptChar As String
        Dim strEncrypted As String = ""
        Dim iKeyChar, iStringChar, i As Integer

        For i = 1 To Len(strCryptThis)
            iKeyChar = Asc(Mid(g_Key, i, 1))
            iStringChar = Asc(Mid(strCryptThis, i, 1))
            ' *** uncomment below to encrypt with addition,
            ' iCryptChar = iStringChar + iKeyChar
            iCryptChar = iKeyChar Xor iStringChar
            strEncrypted = strEncrypted & Chr(iCryptChar)
        Next
        EnCrypt = strEncrypted
    End Function




    Protected Sub Btn_Esci_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles Btn_Esci.Click

        If Request.Item("Url") = "" Or Request.Item("Url") = "/Seniorweb/Strumenti.aspx" Then
            Response.Redirect("/Seniorweb/MainMenu.aspx")
        Else
            Response.Redirect(Request.Item("Url"))
        End If

    End Sub

    Protected Sub Btn_ModificaEMail_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles Btn_ModificaEMail.Click
        Dim Kx As New Cls_Login
        If Session("UTENTE").ToString.IndexOf(">") > 0 Then
            Dim Appoggio As String = Session("UTENTE").ToString

            For i = 1 To 20
                Appoggio = Appoggio.Replace("<" & i & ">", "")
            Next i


            Kx.Utente = Appoggio
            Kx.LeggiSP(Application("SENIOR"))

            Kx.EmailUtente = Txt_EMailUtente.Text
            Kx.Scrivi(Application("SENIOR"))

            For i = 1 To 20
                Kx.Utente = Appoggio & "<" & i & ">"
                Kx.Cliente = 0
                Kx.LeggiSP(Application("SENIOR"))
                If Kx.Cliente > 0 Then
                    Kx.EmailUtente = Txt_EMailUtente.Text
                    Kx.Scrivi(Application("SENIOR"))
                End If
            Next

        Else
            'Kx.Utente = Session("UTENTE")
            If Session("UTENTE") = "" Then
                Kx.Utente = Session("UTENTEIMPER")
            Else
                Kx.Utente = Session("UTENTE")
            End If
            Kx.LeggiSP(Application("SENIOR"))
            Kx.EmailUtente = Txt_EMailUtente.Text
            Kx.Scrivi(Application("SENIOR"))
        End If
    End Sub

    Protected Sub chk_UsaServerDefault_CheckedChanged(sender As Object, e As EventArgs)
        DisabilitaCampiPersonalizzazione()
    End Sub

    Private Sub DisabilitaCampiPersonalizzazione()
        Txt_Smtp.Enabled = False
        Chk_UsoSSl.Enabled = False
        Txt_PasswordSmtp.Enabled = False
        Txt_Smtp.ReadOnly = False
        Txt_PasswordSmtp.ReadOnly = False
        Txt_UserName.Enabled = False
        Txt_UserName.ReadOnly = True
    End Sub
End Class
