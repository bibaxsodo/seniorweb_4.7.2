﻿<%@ Page Language="VB" AutoEventWireup="false" Inherits="QrCodeUpload" CodeFile="QrCodeUpload.aspx.vb" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <meta charset="utf-8">
    <title>Carica Allegati</title>
    <asp:PlaceHolder runat="server">
        <%: System.Web.Optimization.Styles.Render("~/Content/AjaxControlToolkit/Styles/Bundle") %>
    </asp:PlaceHolder>
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js"></script>

    <style>
        .jumbotron {
            background-color: #007dc4;
        }

        #upload-file-container {
            background: url(ospitiweb/images/UPLOAD.jpg) no-repeat center;
            height: 178px;
        }

            #upload-file-container input {
                filter: alpha(opacity=0);
                height: 178px;
                width: 100%;
                opacity: 0;
            }
    </style>
    <script type="text/javascript">
       

                 function fileSelected() {

                     var count = document.getElementById('fileToUpload').files.length;

                     document.getElementById('details').innerHTML = "";

                     for (var index = 0; index < count; index++) {

                         var file = document.getElementById('fileToUpload').files[index];

                         var fileSize = 0;

                         if (file.size > 1024 * 1024)

                             fileSize = (Math.round(file.size * 100 / (1024 * 1024)) / 100).toString() + 'MB';

                         else

                             fileSize = (Math.round(file.size * 100 / 1024) / 100).toString() + 'KB';

                         document.getElementById('details').innerHTML += 'Name: ' + file.name + '<br>Size: ' + fileSize + '<br>Type: ' + file.type;

                         document.getElementById('details').innerHTML += '<p>';

                     }

                 }
                 function getUrlVars() {
                     var vars = {};
                     var parts = window.location.href.replace(/[?&]+([^=&]+)=([^&]*)/gi, function (m, key, value) {
                         vars[key] = value;
                     });
                     return vars;
                 }


                 function getUrlParam(parameter, defaultvalue) {
                     var urlparameter = defaultvalue;
                     if (window.location.href.indexOf(parameter) > -1) {
                         urlparameter = getUrlVars()[parameter];
                     }
                     return urlparameter;
                 }

                 function uploadFile() {

                     var fd = new FormData();

                     var count = document.getElementById('fileToUpload').files.length;

                     for (var index = 0; index < count; index++) {

                         var file = document.getElementById('fileToUpload').files[index];

                         fd.append(file.name, file);

                     }

                     var xhr = new XMLHttpRequest();

                     xhr.upload.addEventListener("progress", uploadProgress, false);

                     xhr.addEventListener("load", uploadComplete, false);

                     xhr.addEventListener("error", uploadFailed, false);

                     xhr.addEventListener("abort", uploadCanceled, false);

                     xhr.open("POST", "savetofile.aspx?DEST=" + getUrlParam("DEST", "ERR"));

                     xhr.send(fd);

                 }

                 function uploadProgress(evt) {

                     if (evt.lengthComputable) {

                         var percentComplete = Math.round(evt.loaded * 100 / evt.total);

                         document.getElementById('progress').innerHTML = percentComplete.toString() + '%';

                     }

                     else {

                         document.getElementById('progress').innerHTML = 'unable to compute';

                     }

                 }

                 function uploadComplete(evt) {

                     /* This event is raised when the server send back a response */

                     alert(evt.target.responseText);

                 }

                 function uploadFailed(evt) {

                     alert("There was an error attempting to upload the file.");

                 }

                 function uploadCanceled(evt) {

                     alert("The upload has been canceled by the user or the browser dropped the connection.");

                 }

    </script >

</head>
<body>
    <form id="form1" runat="server">
        <asp:ScriptManager ID="ScriptManager1" runat="server" EnableScriptGlobalization="true">
            <Scripts>
                <asp:ScriptReference Path="~/Scripts/AjaxControlToolkit/Bundle" />
            </Scripts>
        </asp:ScriptManager>
        <div>

            <div class="jumbotron text-center ">
                <h1>SENIOR</h1>
                <asp:Label ID="lbl_nome" runat="server" Text=""></asp:Label>
            </div>
            <div style="text-align: center;">

                <label for="fileToUpload">Scatta la foto</label><br />

                <div id="upload-file-container">
                    <input type="file" name="fileToUpload" id="fileToUpload" onchange="fileSelected();" accept="image/*" capture="camera" />
                </div>
            </div>

            <div id="details"></div>

            <div style="text-align: center;">

                <input type="button" onclick="uploadFile()" value="Invia" class="btn btn-primary" />

            </div>

            <div id="progress"></div>

        </div>
    </form>
</body>


</html>


