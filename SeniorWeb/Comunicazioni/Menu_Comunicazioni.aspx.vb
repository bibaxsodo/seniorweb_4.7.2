﻿
Partial Class Menu_Comunicazioni
    Inherits System.Web.UI.Page

    Protected Sub ImageButton1_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles ImageButton1.Click
        Response.Redirect("../MainMenu.aspx")
    End Sub

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load


        Dim kBarra As New Cls_BarraSenior

        Lbl_BarraSenior.Text = kBarra.CodiceBarra(Application("SENIOR"), Session("UTENTE"), Page)



        If Page.IsPostBack = True Then Exit Sub



        If (Request.Cookies("UserSettingsM") IsNot Nothing) Then
            If (Request.Cookies("UserSettingsM")("MailingList") IsNot Nothing) Then
                If Request.Cookies("UserSettingsM")("MailingList").ToUpper = "3" Then

                    ClientScript.RegisterClientScriptBlock(Me.GetType(), "chiudinews", "$(document).ready(function() { chiudinews(); });", True)
                End If
            End If
        End If



        Response.Cookies("UserSettingsM")("MailingList") = "3"
        Response.Cookies("UserSettingsM").Expires = "July 31, 2030"

    End Sub
End Class
