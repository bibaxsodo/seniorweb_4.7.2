﻿Imports System.Web.Hosting
Imports System.Diagnostics
Imports System.ComponentModel
Imports System
Imports System.Xml
Imports System.Xml.Schema
Imports System.IO
Imports System.Data
Imports System.Data.OleDb
Partial Class Comunicazioni_EstazioniOneri
    Inherits System.Web.UI.Page
    Dim MyTable As New System.Data.DataTable("tabella")
    Dim MyDataSet As New System.Data.DataSet()


    Function campodbN(ByVal oggetto As Object) As Double


        If IsDBNull(oggetto) Then
            Return 0
        Else
            Return oggetto
        End If
    End Function
    Function campodb(ByVal oggetto As Object) As String


        If IsDBNull(oggetto) Then
            Return ""
        Else
            Return oggetto
        End If
    End Function
    Function campodbD(ByVal oggetto As Object) As Date


        If IsDBNull(oggetto) Then
            Return Nothing
        Else
            Return oggetto
        End If
    End Function



    Private Sub Esport730()
        Dim cn As OleDbConnection

        cn = New Data.OleDb.OleDbConnection(Session("DC_GENERALE"))

        cn.Open()

        MyTable.Clear()
        MyTable.Columns.Clear()

        MyTable.Columns.Add("Nome", GetType(String))
        MyTable.Columns.Add("codicefiscale", GetType(String))
        MyTable.Columns.Add("datadocumento", GetType(String))
        MyTable.Columns.Add("numerodocumento", GetType(String))
        MyTable.Columns.Add("datapagamento", GetType(String))
        MyTable.Columns.Add("Operazione", GetType(String))
        MyTable.Columns.Add("importo", GetType(String))

        MyTable.Columns.Add("QuotaSanitaria", GetType(String))
        MyTable.Columns.Add("NomeOspite", GetType(String))
        MyTable.Columns.Add("CodiceFiscaleOspite", GetType(String))

        If Chk_Sosia.Checked Then
            MyTable.Columns.Add("Classe", GetType(String))
            MyTable.Columns.Add("Tipologia", GetType(String))
            MyTable.Columns.Add("DataAccoglimento", GetType(String))
            MyTable.Columns.Add("DataUscitaDefinitiva", GetType(String))
            MyTable.Columns.Add("Quota", GetType(String))
            MyTable.Columns.Add("GiorniPresenza", GetType(String))

        End If


        Dim cmd As New OleDbCommand()


        If Rd_Incassato.Checked = True Then
            cmd.CommandText = "Select * From MovimentiContabiliTesta  where DataRegistrazione >= ? And DataRegistrazione <= ? And ( CausaleContabile = ?"

            cmd.Parameters.AddWithValue("@DataRegistrazioneDal", Txt_DataDalIncasso.Text)
            cmd.Parameters.AddWithValue("@DataRegistrazioneAl", Txt_DataAlIncasso.Text)
            cmd.Parameters.AddWithValue("@RegistroIVA", Val(DD_RegistroIVA.SelectedValue))
            cmd.Parameters.AddWithValue("@CausaleContabile", DD_CausaleIncasso.SelectedValue)
            If DD_CausaleIncasso1.SelectedValue <> "" Then
                cmd.CommandText = cmd.CommandText & " OR CausaleContabile = ? "
                cmd.Parameters.AddWithValue("@CausaleContabile", DD_CausaleIncasso1.SelectedValue)
            End If
            If DD_CausaleIncasso2.SelectedValue <> "" Then
                cmd.CommandText = cmd.CommandText & " OR CausaleContabile = ? "
                cmd.Parameters.AddWithValue("@CausaleContabile", DD_CausaleIncasso2.SelectedValue)
            End If
            If DD_CausaleIncasso3.SelectedValue <> "" Then
                cmd.CommandText = cmd.CommandText & " OR CausaleContabile = ? "
                cmd.Parameters.AddWithValue("@CausaleContabile", DD_CausaleIncasso3.SelectedValue)
            End If
            If DD_CausaleIncasso4.SelectedValue <> "" Then
                cmd.CommandText = cmd.CommandText & " OR CausaleContabile = ? "
                cmd.Parameters.AddWithValue("@CausaleContabile", DD_CausaleIncasso4.SelectedValue)
            End If
            cmd.CommandText = cmd.CommandText & " )"
        Else
            cmd.CommandText = "Select * From MovimentiContabiliTesta  where DataRegistrazione >= ? And DataRegistrazione <= ? And RegistroIVA = ?"

            cmd.Parameters.AddWithValue("@DataRegistrazioneDal", Txt_DataDalIncasso.Text)
            cmd.Parameters.AddWithValue("@DataRegistrazioneAl", Txt_DataAlIncasso.Text)

            cmd.Parameters.AddWithValue("@RegistroIVA", Val(DD_RegistroIVA.SelectedValue))
        End If


        cmd.Connection = cn
        Dim RsOspiteParente As OleDbDataReader
        Try
            RsOspiteParente = cmd.ExecuteReader()
        Catch ex As Exception
            cn.Close()
            Exit Sub
        End Try

        Do While RsOspiteParente.Read

            If Rd_Incassato.Checked = True Then
                Dim cmdLegami As New OleDbCommand()

                cmdLegami.CommandText = "Select * From TabellaLegami Where CodicePagamento = ? "
                cmdLegami.Parameters.AddWithValue("@Legami", campodbN(RsOspiteParente.Item("NumeroRegistrazione")))
                cmdLegami.Connection = cn
                Dim RsLegami As OleDbDataReader = cmdLegami.ExecuteReader
                Do While RsLegami.Read

                    If Val(DD_RegistroIVA.SelectedValue) > 0 Then
                        Dim Documento As New Cls_MovimentoContabile


                        Documento.NumeroRegistrazione = campodbN(RsLegami.Item("CodiceDocumento"))
                        Documento.Leggi(Session("DC_GENERALE"), Documento.NumeroRegistrazione)

                        If Documento.RegistroIVA = Val(DD_RegistroIVA.SelectedValue) Then
                            AddDocumento(campodbN(RsLegami.Item("CodiceDocumento")), campodbD(RsOspiteParente.Item("DataRegistrazione")), campodbN(RsLegami.Item("Importo")))
                        End If
                    Else

                        AddDocumento(campodbN(RsLegami.Item("CodiceDocumento")), campodbD(RsOspiteParente.Item("DataRegistrazione")), campodbN(RsLegami.Item("Importo")))
                    End If
                Loop
                RsLegami.Close()
            Else

                Dim Documento As New Cls_MovimentoContabile

                Documento.NumeroRegistrazione = campodbN(RsOspiteParente.Item("NumeroRegistrazione"))
                Documento.Leggi(Session("DC_GENERALE"), Documento.NumeroRegistrazione)
                AddDocumento(campodbN(RsOspiteParente.Item("NumeroRegistrazione")), campodbD(RsOspiteParente.Item("DataRegistrazione")), Documento.ImportoDocumento(Session("DC_TABELLE")))
            End If


        Loop
        RsOspiteParente.Close()

        cn.Close()



        Dim dv As System.Data.DataView = MyTable.DefaultView
        dv.Sort = "Nome"
        MyTable = dv.ToTable





        GridView1.AutoGenerateColumns = True
        GridView1.DataSource = RaggruppaTabella(MyTable)
        GridView1.Font.Size = 10
        GridView1.DataBind()

    End Sub


    Private Function RaggruppaTabella(ByRef TabRaggruppa As System.Data.DataTable) As System.Data.DataTable
        Dim OldCF As String = ""
        Dim OldQSan As String = ""
        Dim OldQSoc As String = ""

        RaggruppaTabella = New System.Data.DataTable("tabellaraggruppata")

        RaggruppaTabella.Clear()
        RaggruppaTabella.Columns.Clear()

        RaggruppaTabella.Columns.Add("Nome", GetType(String))
        RaggruppaTabella.Columns.Add("codicefiscale", GetType(String))
        RaggruppaTabella.Columns.Add("datadocumento", GetType(String))
        RaggruppaTabella.Columns.Add("numerodocumento", GetType(String))
        RaggruppaTabella.Columns.Add("datapagamento", GetType(String))
        RaggruppaTabella.Columns.Add("Operazione", GetType(String))
        RaggruppaTabella.Columns.Add("importo", GetType(String))

        RaggruppaTabella.Columns.Add("QuotaSanitaria", GetType(String))
        RaggruppaTabella.Columns.Add("NomeOspite", GetType(String))
        RaggruppaTabella.Columns.Add("CodiceFiscaleOspite", GetType(String))

        If Chk_Sosia.Checked Then
            RaggruppaTabella.Columns.Add("Classe", GetType(String))
            RaggruppaTabella.Columns.Add("Tipologia", GetType(String))
            RaggruppaTabella.Columns.Add("DataAccoglimento", GetType(String))
            RaggruppaTabella.Columns.Add("DataUscitaDefinitiva", GetType(String))
            RaggruppaTabella.Columns.Add("Quota", GetType(String))

        End If

        Dim OldRiga As System.Data.DataRow = RaggruppaTabella.NewRow()
        Dim Importo As Double = 0

        For i = 1 To TabRaggruppa.Rows.Count - 1
            If TabRaggruppa.Rows(i).Item(1) <> OldCF And OldCF <> "" Then

                OldRiga.Item("importo") = Importo

                Dim NuovaRiga As System.Data.DataRow = RaggruppaTabella.NewRow()
                For Column = 0 To TabRaggruppa.Columns.Count - 1
                    NuovaRiga.Item(Column) = OldRiga(Column)
                Next

                RaggruppaTabella.Rows.Add(NuovaRiga)
                Importo = 0
            End If

            OldCF = TabRaggruppa.Rows(i).Item(1)
            OldQSan = TabRaggruppa.Rows(i).Item("QuotaSanitaria")
            
            If OldCF = "BLLBTL34H50L349O" Then
                OldCF = "BLLBTL34H50L349O"
            End If
            OldRiga = TabRaggruppa.Rows(i)


            Importo = Importo + CDbl(TabRaggruppa.Rows(i).Item("importo"))
        Next



        Return RaggruppaTabella
    End Function


    Private Sub AddDocumento(ByVal NumeroDocumento As Long, ByVal DataIncasso As Date, ByVal TotaleIncassato As Double)
        Dim Documento As New Cls_MovimentoContabile

        Documento.NumeroRegistrazione = NumeroDocumento
        Documento.Leggi(Session("DC_GENERALE"), Documento.NumeroRegistrazione)

        If Documento.Tipologia = "" Or Documento.Tipologia = "O" Or Documento.Tipologia = "P" Then
            Dim Sottoconto As Integer
            Dim Nome As String = ""
            Dim CodiceFiscale As String = ""

            Sottoconto = Documento.Righe(0).SottocontoPartita

            If Int(Sottoconto / 100) = Math.Round(Sottoconto / 100, 2) Then
                Dim Ospite As New ClsOspite

                Ospite.CodiceOspite = Int(Sottoconto / 100)
                Ospite.Leggi(Session("DC_OSPITE"), Ospite.CodiceOspite)

                Nome = Ospite.Nome
                CodiceFiscale = Ospite.CODICEFISCALE
            Else
                Dim Parenti As New Cls_Parenti

                Parenti.CodiceOspite = Int(Sottoconto / 100)
                Parenti.CodiceParente = Sottoconto - (Int(Sottoconto / 100) * 100)

                Parenti.Leggi(Session("DC_OSPITE"), Parenti.CodiceOspite, Parenti.CodiceParente)


                Nome = Parenti.Nome
                CodiceFiscale = Parenti.CODICEFISCALE
            End If


            If DD_CServ2.SelectedValue = "" Or (DD_CServ2.SelectedValue <> "" And Documento.CentroServizio = DD_CServ2.SelectedValue) Then

                Dim myriga1 As System.Data.DataRow = MyTable.NewRow()


                Dim Registro As New Cls_RegistroIVA


                Registro.Tipo = Documento.RegistroIVA
                Registro.Leggi(Session("DC_TABELLE"), Registro.Tipo)




                myriga1(0) = Nome
                myriga1(1) = CodiceFiscale
                myriga1(2) = Format(Documento.DataDocumento, "dd/MM/yyyy")
                If Registro.IndicatoreRegistro = "" Then
                    myriga1(3) = Documento.NumeroProtocollo
                Else
                    myriga1(3) = Documento.NumeroProtocollo & "/" & Registro.IndicatoreRegistro
                End If

                myriga1(4) = Format(DataIncasso, "dd/MM/yyyy")

                Dim CausaleContabile As New Cls_CausaleContabile

                CausaleContabile.Codice = Documento.CausaleContabile
                CausaleContabile.Leggi(Session("DC_TABELLE"), CausaleContabile.Codice)

                If CausaleContabile.TipoDocumento = "NC" Then
                    myriga1(5) = "NC"
                Else
                    myriga1(5) = "FT"
                End If

                Dim TotDocumento As Double = 0
                Dim TotIncasso As Double = 0
                Dim Proporzione As Double = 0
                TotDocumento = Documento.ImportoDocumento(Session("DC_TABELLE"))

                TotIncasso = Math.Abs(TotaleIncassato)
                'If TotDocumento > 77.47 Then
                '    Proporzione = Math.Round(2 / TotDocumento * TotIncasso, 2)
                'End If

                myriga1(6) = Math.Abs(TotaleIncassato) - Proporzione


                'MyTable.Columns.Add("QuotaIntestatario", GetType(String))
                'MyTable.Columns.Add("NomeOspite", GetType(String))
                'MyTable.Columns.Add("CodiceFiscaleOspite", GetType(String))

                Dim CentroServizio As String = Documento.CentroServizio

                Dim x As New Cls_CalcoloRette
                x.STRINGACONNESSIONEDB = Session("DC_OSPITE")

                x.ApriDB(Session("DC_OSPITE"), Session("DC_OSPITIACCESSORI"))


                myriga1("QuotaSanitaria") = Math.Round(x.QuoteGiornaliere(CentroServizio, Int(Sottoconto / 100), "R", 0, Documento.DataDocumento), 2)


                myriga1("NomeOspite") = ""
                myriga1("CodiceFiscaleOspite") = ""
                If Int(Sottoconto / 100) <> Math.Round(Sottoconto / 100, 4) Then
                    Dim Ospite As New ClsOspite

                    Ospite.CodiceOspite = Int(Sottoconto / 100)
                    Ospite.Leggi(Session("DC_OSPITE"), Ospite.CodiceOspite)

                    myriga1("NomeOspite") = Ospite.Nome
                    myriga1("CodiceFiscaleOspite") = Ospite.CODICEFISCALE
                End If

                If Chk_Sosia.Checked Then

                    myriga1("Classe") = Classe(Int(Sottoconto / 100), Documento.DataDocumento)
                    myriga1("Tipologia") = TipoClasse(Int(Sottoconto / 100), Documento.DataDocumento)

                    Dim Movimenti As New Cls_Movimenti


                    Movimenti.CodiceOspite = Int(Sottoconto / 100)
                    Movimenti.CENTROSERVIZIO = Documento.CentroServizio
                    Movimenti.Data = Nothing
                    Movimenti.UltimaDataAccoglimento(Session("DC_OSPITE"))
                    If Year(Movimenti.Data) < 1990 Then
                        Movimenti.UltimaDataAccoglimentoSenzaCserv(Session("DC_OSPITE"))
                        CentroServizio = Movimenti.CENTROSERVIZIO
                    End If
                    myriga1("DataAccoglimento") = Format(Movimenti.Data, "dd/MM/yyyy")



                    Movimenti.Data = Nothing

                    Movimenti.CodiceOspite = Int(Sottoconto / 100)
                    Movimenti.CENTROSERVIZIO = Documento.CentroServizio

                    Movimenti.UltimaDataUscitaDefinitiva(Session("DC_OSPITE"))

                    If Year(Movimenti.Data) > 2000 Then
                        myriga1("DataUscitaDefinitiva") = Format(Movimenti.Data, "dd/MM/yyyy")
                    Else
                        myriga1("DataUscitaDefinitiva") = ""
                    End If



                    Dim ImportoOspite As Double = 0
                    Dim ImportoParenti As Double = 0


                    ImportoOspite = Math.Round(x.QuoteGiornaliere(CentroServizio, Int(Sottoconto / 100), "O", 0, Documento.DataDocumento), 2)
                    For i = 1 To 10
                        ImportoParenti = ImportoParenti + Math.Round(CDbl(x.QuoteGiornaliere(CentroServizio, Int(Sottoconto / 100), "P", i, Documento.DataDocumento)), 2)
                    Next

                    myriga1("Quota") = Format(ImportoOspite + ImportoParenti, "#,##0.00")
                    Dim ContaGiorni As Integer = 0
                    Dim Giorni As Integer = 0
                    For ContaGiorni = 0 To 300
                        If Not IsNothing(Documento.Righe(ContaGiorni)) Then
                            If Documento.Righe(ContaGiorni).Descrizione.IndexOf("Presenz") > 0 And Documento.Righe(ContaGiorni).RigaDaCausale = 3 Then
                                Giorni = Giorni + 1
                            End If
                        End If
                    Next

                    myriga1("GiorniPresenza") = Giorni
                End If

                x.ChiudiDB()

                MyTable.Rows.Add(myriga1)
            End If
        End If
    End Sub


    Private Sub EseguiJS()
        Dim MyJs As String
        MyJs = "$(document).ready(function() {"

        MyJs = MyJs & "var els = document.getElementsByTagName(""*"");"

        MyJs = MyJs & "for (var i=0;i<els.length;i++)"
        MyJs = MyJs & "if ( els[i].id ) { "
        MyJs = MyJs & " var appoggio =els[i].id; "
        MyJs = MyJs & " if (appoggio.match('Txt_Data')!= null) {  "
        MyJs = MyJs & " $(els[i]).mask(""99/99/9999"");"

        MyJs = MyJs & " $(els[i]).datepicker({ changeMonth: true,changeYear: true,  yearRange: ""1990:" & Year(Now) + 5 & """},$.datepicker.regional[""it""]);"

        MyJs = MyJs & "    }"
        MyJs = MyJs & "} "
        MyJs = MyJs & "if (window.innerHeight>0) { $(""#BarraLaterale"").css(""height"",(window.innerHeight - 94) + ""px""); } else"
        MyJs = MyJs & "{ $(""#BarraLaterale"").css(""height"",(document.documentElement.offsetHeight - 94) + ""px"");  }"
        MyJs = MyJs & "});"

        'MyJs = "$(document).ready(function() { $('.myClass').keypress(function() { handleEnter($('.myClass').val(), true, true); } );     $('#" & Txt_ImportoPagato.ClientID & "').blur(function() { var ap = formatNumber ($('#" & Txt_ImportoPagato.ClientID & "').val(),2); $('#" & Txt_ImportoPagato.ClientID & "').val(ap); }); });"
        ScriptManager.RegisterStartupScript(Me, Me.GetType(), "visualizzaRitIm", MyJs, True)
    End Sub


    Private Function TipoClasse(ByVal CodiceOspite As Integer, ByVal Data As Date) As String
        Dim Cn As New OleDbConnection(Session("DC_OSPITE"))
        Dim MySql As String = ""

        TipoClasse = ""

        Cn.Open()

        MySql = "Select * FROM TempSosia where CodiceOspite = ? And Data <= ? Order by Data Desc"
        Dim cmddel As New OleDbCommand()
        cmddel.CommandText = (MySql)
        cmddel.Connection = Cn
        cmddel.Parameters.AddWithValue("@Codiceospite", CodiceOspite)
        cmddel.Parameters.AddWithValue("@Data", Data)
        Dim myPOSTreader As OleDbDataReader = cmddel.ExecuteReader()
        If myPOSTreader.Read Then
            TipoClasse = campodb(myPOSTreader.Item("Tipologia"))
        End If
        myPOSTreader.Close()

        Cn.Close()

        Return TipoClasse
    End Function

    Private Function Classe(ByVal CodiceOspite As Integer, ByVal Data As Date) As String
        Dim Cn As New OleDbConnection(Session("DC_OSPITE"))
        Dim MySql As String = ""

        Classe = ""

        Cn.Open()

        MySql = "Select * FROM TempSosia where CodiceOspite = ? And Data <= ? Order by Data Desc"
        Dim cmddel As New OleDbCommand()
        cmddel.CommandText = (MySql)
        cmddel.Connection = Cn
        cmddel.Parameters.AddWithValue("@Codiceospite", CodiceOspite)
        cmddel.Parameters.AddWithValue("@Data", Data)
        Dim myPOSTreader As OleDbDataReader = cmddel.ExecuteReader()
        If myPOSTreader.Read Then
            Classe = campodb(myPOSTreader.Item("Sosia"))
        End If
        myPOSTreader.Close()

        Cn.Close()

        Return Classe
    End Function

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Trim(Session("UTENTE")) = "" Then
            Response.Redirect("../Login.aspx")
            Exit Sub
        End If

        Call EseguiJS()

        If Page.IsPostBack = True Then Exit Sub


        Dim K1 As New Cls_SqlString

        Dim Barra As New Cls_BarraSenior

        If Not IsNothing(Session("RicercaAnagraficaSQLString")) Then
            Try
                K1 = Session("RicercaAnagraficaSQLString")
            Catch ex As Exception
                K1 = Nothing
            End Try
        End If

        Lbl_BarraSenior.Text = Barra.CodiceBarra(Application("SENIOR"), Session("UTENTE"), Page, K1)

        Dim kVilla As New Cls_TabelleDescrittiveOspitiAccessori




        Dim X As New Cls_RegistroIVA

        kVilla.UpDateDropBox(Session("DC_OSPITIACCESSORI"), "VIL", DD_Struttura2)
        If DD_Struttura2.Items.Count = 1 Then
            Call AggiornaCServ2()
        End If

        Dim M As New Cls_CausaleContabile

        M.UpDateDropBoxPag(Session("DC_TABELLE"), DD_CausaleIncasso)

        M.UpDateDropBoxPag(Session("DC_TABELLE"), DD_CausaleIncasso1)

        M.UpDateDropBoxPag(Session("DC_TABELLE"), DD_CausaleIncasso2)

        M.UpDateDropBoxPag(Session("DC_TABELLE"), DD_CausaleIncasso3)

        M.UpDateDropBoxPag(Session("DC_TABELLE"), DD_CausaleIncasso4)


        Dim RegIVA As New Cls_RegistroIVA


        RegIVA.UpDateDropBox(Session("DC_TABELLE"), DD_RegistroIVA)


    End Sub


    Private Sub AggiornaCServ2()
        Dim kCsrv As New Cls_CentroServizio

        If DD_Struttura2.SelectedValue = "" Then
            kCsrv.UpDateDropBox(Session("DC_OSPITE"), DD_CServ2)
        Else
            kCsrv.UpDateDropBoxStruttura(Session("DC_OSPITE"), DD_CServ2, DD_Struttura2.SelectedValue)
        End If
    End Sub

    Protected Sub DD_Struttura2_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles DD_Struttura2.SelectedIndexChanged
        AggiornaCServ2()
        Call EseguiJS()
    End Sub

    Protected Sub ImageButton3_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles ImageButton3.Click
        If Not IsDate(Txt_DataDalIncasso.Text) Then
            ScriptManager.RegisterClientScriptBlock(Me, Me.GetType(), "Errore", "VisualizzaErrore('Indicare data dal ');", True)
            Exit Sub
        End If
        If Not IsDate(Txt_DataAlIncasso.Text) Then
            ScriptManager.RegisterClientScriptBlock(Me, Me.GetType(), "Errore", "VisualizzaErrore('Indicare data al ');", True)
            Exit Sub
        End If

        Call Esport730()

    End Sub

    Protected Sub ImageButton4_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles ImageButton4.Click
        If Not IsDate(Txt_DataDalIncasso.Text) Then
            ScriptManager.RegisterClientScriptBlock(Me, Me.GetType(), "Errore", "VisualizzaErrore('Indicare data dal ');", True)
            Exit Sub
        End If
        If Not IsDate(Txt_DataAlIncasso.Text) Then
            ScriptManager.RegisterClientScriptBlock(Me, Me.GetType(), "Errore", "VisualizzaErrore('Indicare data al ');", True)
            Exit Sub
        End If

        Call Esport730()


        If MyTable.Rows.Count > 1 Then
            Response.Clear()
            Response.AddHeader("content-disposition", "attachment;filename=730telematico.xls")
            Response.Charset = String.Empty
            Response.Cache.SetCacheability(HttpCacheability.NoCache)
            Response.ContentType = "application/vnd.ms-excel"
            Dim stringWrite As New System.IO.StringWriter
            Dim htmlWrite As New HtmlTextWriter(stringWrite)

            form1.Controls.Clear()
            form1.Controls.Add(GridView1)

            form1.RenderControl(htmlWrite)

            Response.Write(stringWrite.ToString())
            Response.End()
        End If
    End Sub

    Protected Sub Btn_Esci_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles Btn_Esci.Click
        Response.Redirect("Menu_Comunicazioni.aspx")
    End Sub

    Protected Sub Imb_Download_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles Imb_Download.Click
        If Not IsDate(Txt_DataDalIncasso.Text) Then
            ScriptManager.RegisterClientScriptBlock(Me, Me.GetType(), "Errore", "VisualizzaErrore('Indicare data dal ');", True)
            Exit Sub
        End If
        If Not IsDate(Txt_DataAlIncasso.Text) Then
            ScriptManager.RegisterClientScriptBlock(Me, Me.GetType(), "Errore", "VisualizzaErrore('Indicare data al ');", True)
            Exit Sub
        End If

        Dim I As Integer
        Dim Colonna As Integer
        Dim Csv As String = ""
        Call Esport730()


        For Colonna = 0 To MyTable.Columns.Count - 1
            Csv = Csv & MyTable.Columns(Colonna).ColumnName & ";"
        Next

        Csv = Csv & vbNewLine

        For I = 0 To MyTable.Rows.Count - 1
            For Colonna = 0 To MyTable.Columns.Count - 1
                Csv = Csv & MyTable.Rows(I).Item(Colonna).ToString & ";"
            Next
            Csv = Csv & vbNewLine
        Next


        Response.Clear()
        Response.ContentType = "text/csv"
        Response.AddHeader("content-disposition", "attachment;filename=Oneri.csv")
        Response.Write(CSV)
        Response.End()
    End Sub

    Protected Sub Rd_Incassato_CheckedChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles Rd_Incassato.CheckedChanged


        DD_CausaleIncasso.Enabled = True
        DD_CausaleIncasso1.Enabled = True
        DD_CausaleIncasso2.Enabled = True
        DD_CausaleIncasso3.Enabled = True
        DD_CausaleIncasso4.Enabled = True

    End Sub

    Protected Sub Rd_Fatturata_CheckedChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles Rd_Fatturata.CheckedChanged
        DD_CausaleIncasso.Enabled = False
        DD_CausaleIncasso1.Enabled = False
        DD_CausaleIncasso2.Enabled = False
        DD_CausaleIncasso3.Enabled = False
        DD_CausaleIncasso4.Enabled = False

    End Sub

    Protected Sub Rd_Fatturata_DataBinding(ByVal sender As Object, ByVal e As System.EventArgs) Handles Rd_Fatturata.DataBinding

    End Sub
End Class
