﻿Imports Microsoft.VisualBasic
Imports System.Data.Odbc
Imports System.Web.Hosting
Imports System.Data.OleDb
Partial Class Comunicazioni_ExportSolleciti
    Inherits System.Web.UI.Page
    Dim Tabella As New System.Data.DataTable("tabella")


    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Session("UTENTE") = "" Then
            Response.Redirect("..\Login.aspx")
            Exit Sub
        End If


        If Page.IsPostBack = True Then
            Exit Sub
        End If

        Dim K1 As New Cls_SqlString

        Dim Barra As New Cls_BarraSenior

        If Not IsNothing(Session("RicercaGeneraleSQLString")) Then
            K1 = Session("RicercaGeneraleSQLString")
        End If

        Lbl_BarraSenior.Text = Barra.CodiceBarra(Application("SENIOR"), Session("UTENTE"), Page, K1)



        If Page.IsPostBack = True Then
            Exit Sub
        End If

        Dim Solleciti As New Cls_Solleciti

        Solleciti.UpDropDownList(Session("DC_OSPITE"), DD_Oneri)

    End Sub

    Protected Sub ImageButton3_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles ImageButton3.Click
        Dim Appoggio As String = Format(Now, "yyyyMMddHHmmss")
        Try
            Dim NomeFile As String

            NomeFile = ImportFileXLS(Appoggio)
        Finally
            Try
                If FileUpload1.FileName.ToString.IndexOf(".csv") > 0 Then
                    Kill(HostingEnvironment.ApplicationPhysicalPath() & "Public\CsvImport" & Appoggio & ".csv")
                Else
                    Kill(HostingEnvironment.ApplicationPhysicalPath() & "Public\ExcelImport" & Appoggio & ".xls")
                End If
            Catch ex As Exception

            End Try
        End Try
    End Sub


    Private Function ImportFileXLS(ByVal Appoggio As String) As String
        Dim cn As OdbcConnection
        Dim nomefileexcel As String
        Dim HTMLDocumenti As String = ""

        Dim NomeSocieta As String


        If FileUpload1.FileName = "" Then
            ClientScript.RegisterClientScriptBlock(Me.GetType(), "Errore", "VisualizzaErrore('File non indicato');", True)
            Exit Function
        End If

        Txt_NonTrovati.Visible = True


        Dim k As New Cls_Login

        k.Utente = Session("UTENTE")
        k.LeggiSP(Application("SENIOR"))

        NomeSocieta = k.NomeEPersonam



        If FileUpload1.FileName.ToString.IndexOf(".csv") > 0 Then
            FileUpload1.SaveAs(HostingEnvironment.ApplicationPhysicalPath() & "\Public\CsvImport" & Appoggio & ".csv")

            If Dir(HostingEnvironment.ApplicationPhysicalPath() & "\Public\CsvImport" & Appoggio & ".csv") = "" Then
                ClientScript.RegisterClientScriptBlock(Me.GetType(), "Errore", "VisualizzaErrore('File csv non trovata');", True)
                Exit Function
            End If
        Else
            FileUpload1.SaveAs(HostingEnvironment.ApplicationPhysicalPath() & "\Public\ExcelImport" & Appoggio & ".xls")

            If Dir(HostingEnvironment.ApplicationPhysicalPath() & "\Public\ExcelImport" & Appoggio & ".xls") = "" Then
                ClientScript.RegisterClientScriptBlock(Me.GetType(), "Errore", "VisualizzaErrore('File excel non trovata');", True)
                Exit Function
            End If
        End If


        Dim Oldcodicefiscale As String = ""

        Dim OldNome As String = ""
        Dim Olddatadocumento As String = ""
        Dim Oldnumerodocumento As String = ""
        Dim Olddatapagamento As String = ""
        Dim OldOperazione As String = ""
        Dim Oldimporto As String = ""
        Dim OldQuotaSanitaria As String = ""
        Dim OldNomeOspite As String = ""
        Dim OldCodiceFiscaleOspite As String = ""
        Dim OldImportoIncassato As String = ""

        Dim TotaleDaIncassare As Double = 0
        Dim TotaleInteresse As Double = 0
        



        Dim GigliaTab As String = ""
        GigliaTab = "<table>"
        GigliaTab = GigliaTab & "<tr>"
        GigliaTab = GigliaTab & "<td>Data Documento</td>"
        GigliaTab = GigliaTab & "<td>Numero Documento</td>"
        GigliaTab = GigliaTab & "<td>Data Pagamento</td>"
        GigliaTab = GigliaTab & "<td>Importo Documento</td>"
        GigliaTab = GigliaTab & "<td>Incassato</td>"
        GigliaTab = GigliaTab & "<td>Da Incassare</td>"
        GigliaTab = GigliaTab & "<td>Interesse</td>"

        'cn = New Data.Odbc.OdbcConnection("Driver={Microsoft Excel Driver (*.xls, *.xlsx, *.xlsm, *.xlsb)};DriverId=790;Dbq=" & HostingEnvironment.ApplicationPhysicalPath() & "\Public\ExcelImport" & Appoggio & ".xls;")
        If FileUpload1.FileName.ToString.IndexOf(".csv") > 0 Then

            cn = New Data.Odbc.OdbcConnection("Driver={Microsoft Text Driver (*.txt; *.csv)};Dbq=" & HostingEnvironment.ApplicationPhysicalPath() & "Public\;HDR=Yes;")
        Else
            cn = New Data.Odbc.OdbcConnection("Driver={Microsoft Excel Driver (*.xls, *.xlsx, *.xlsm, *.xlsb)};DriverId=790;Dbq=" & HostingEnvironment.ApplicationPhysicalPath() & "\Public\ExcelImport" & Appoggio & ".xls;")
        End If


        cn.Open()
        ''Provider=Microsoft.Jet.OLEDB.4.0;Data Source={0}\\;Extended Properties='Text;HDR=Yes;FMT=CSVDelimited(;)
        'Driver={Microsoft Excel Driver (*.xls)};DriverId=790;Dbq=" & Txt_Rsl.text & ";"

        Dim cmd As New OdbcCommand()

        If FileUpload1.FileName.ToString.IndexOf(".csv") > 0 Then
            cmd.CommandText = ("select * from [" & "CsvImport" & Appoggio & ".csv" & "]")

            'CsvImport" & Appoggio & ".csv"
        Else
            cmd.CommandText = ("select * from [FOGLIO$]")
        End If

        cmd.Connection = cn
        Dim myPOSTreader As OdbcDataReader

        Try
            myPOSTreader = cmd.ExecuteReader()
        Catch ex As Exception
            cn.Close()
            ClientScript.RegisterClientScriptBlock(Me.GetType(), "Errore", "VisualizzaErrore('Errore nella struttura del file');", True)
            Try
                Kill(HostingEnvironment.ApplicationPhysicalPath() & "Public\ExcelImport" & Appoggio & ".xls")
            Catch elimino As Exception

            End Try
            Try
                Kill(HostingEnvironment.ApplicationPhysicalPath() & "Public\CsvImport" & Appoggio & ".csv")
            Catch elimino As Exception

            End Try

            Exit Function
        End Try

        Do While myPOSTreader.Read
            Dim Nome As String = ""
            Dim codicefiscale As String = ""
            Dim datadocumento As String = ""
            Dim numerodocumento As String = ""
            Dim datapagamento As String = ""
            Dim Operazione As String = ""
            Dim importo As String = ""
            Dim ImportoIncassato As String = ""
            Dim Interesse As String = ""
            Dim NomeOspite As String = ""
            Dim CodiceFiscaleOspite As String = ""


            If FileUpload1.FileName.ToString.IndexOf(".csv") > 0 Then
                Dim Vettore(100) As String
                Dim CampoInput As String

                CampoInput = myPOSTreader.Item(0)
                Try
                    CampoInput = CampoInput & "," & myPOSTreader.Item(1)
                    CampoInput = CampoInput & "," & myPOSTreader.Item(2)
                Catch ex As Exception

                End Try

                Vettore = SplitWords(CampoInput)
                Try
                    Nome = Vettore(0)
                    codicefiscale = Vettore(1)
                    datadocumento = Vettore(2)
                    numerodocumento = Vettore(3)
                    datapagamento = Vettore(4)
                    Operazione = Vettore(5)
                    importo = Vettore(6)
                    ImportoIncassato = Vettore(7)
                    Interesse = Vettore(8)



                Catch ex As Exception
                    cn.Close()
                    ClientScript.RegisterClientScriptBlock(Me.GetType(), "Errore", "VisualizzaErrore('Errore nella struttura del file excel');", True)
                    Kill(HostingEnvironment.ApplicationPhysicalPath() & "Public\CsvImport" & Appoggio & ".csv")
                    Exit Function
                End Try

                Try
                    NomeOspite = Vettore(9)
                    CodiceFiscaleOspite = Vettore(10)
                Catch ex As Exception

                End Try
            Else

                Try
                    Nome = campodb(myPOSTreader.Item("Nome")).Trim

                    codicefiscale = campodb(myPOSTreader.Item("codicefiscale"))

                    datadocumento = campodb(myPOSTreader.Item("datadocumento"))
                    numerodocumento = campodb(myPOSTreader.Item("numerodocumento"))
                    datapagamento = campodb(myPOSTreader.Item("datapagamento"))

                    Operazione = campodb(myPOSTreader.Item("Operazione"))

                    importo = campodb(myPOSTreader.Item("importo"))

                    ImportoIncassato = campodb(myPOSTreader.Item("ImportoIncassato"))

                    Interesse = campodb(myPOSTreader.Item("Interesse"))

                    'Interesse = Vettore(8)

                    CodiceFiscaleOspite = campodb(myPOSTreader.Item("CodiceFiscaleOspite"))

                    NomeOspite = campodb(myPOSTreader.Item("NomeOspite"))
                Catch ex As Exception
                    cn.Close()
                    ClientScript.RegisterClientScriptBlock(Me.GetType(), "Errore", "VisualizzaErrore('Errore nella struttura del file excel');", True)
                    Kill(HostingEnvironment.ApplicationPhysicalPath() & "Public\ExcelImport" & Appoggio & ".xls")
                    Exit Function

                End Try


            End If




            If Oldcodicefiscale <> codicefiscale And Oldcodicefiscale <> "" Then
                GigliaTab = GigliaTab & "</table>"
                ExportPDF(HTMLDocumenti, OldNome, Oldcodicefiscale, Olddatadocumento, Oldnumerodocumento, Olddatapagamento, OldOperazione, Oldimporto, OldImportoIncassato, OldNomeOspite, OldCodiceFiscaleOspite, GigliaTab, TotaleDaIncassare)

                GigliaTab = "<table>"
                GigliaTab = GigliaTab & "<tr>"
                GigliaTab = GigliaTab & "<td>Data Documento</td>"
                GigliaTab = GigliaTab & "<td>Numero Documento</td>"
                GigliaTab = GigliaTab & "<td>Data Pagamento</td>"
                GigliaTab = GigliaTab & "<td>Importo Documento</td>"
                GigliaTab = GigliaTab & "<td>Incassato</td>"
                GigliaTab = GigliaTab & "<td>Da Incassare</td>"
                GigliaTab = GigliaTab & "<td>Interesse</td>"
                GigliaTab = GigliaTab & "</tr>"


                Oldcodicefiscale = codicefiscale
                OldNome = Nome
                Olddatadocumento = datadocumento
                Oldnumerodocumento = numerodocumento
                Olddatapagamento = datapagamento
                OldOperazione = Operazione
                Oldimporto = importo
                OldImportoIncassato = ImportoIncassato
                OldNomeOspite = NomeOspite
                OldCodiceFiscaleOspite = CodiceFiscaleOspite


                GigliaTab = GigliaTab & "<tr>"
                GigliaTab = GigliaTab & "<td>" & datadocumento & "</td>"
                GigliaTab = GigliaTab & "<td>" & numerodocumento & "</td>"
                GigliaTab = GigliaTab & "<td>" & datapagamento & "</td>"
                GigliaTab = GigliaTab & "<td>" & importo & "</td>"
                GigliaTab = GigliaTab & "<td>" & ImportoIncassato & "</td>"
                GigliaTab = GigliaTab & "<td>" & importo - ImportoIncassato & "</td>"
                GigliaTab = GigliaTab & "<td>" & Interesse & "</td>"

                GigliaTab = GigliaTab & "</tr>"

                TotaleDaIncassare = 0
                Try
                    TotaleDaIncassare = (CDbl(importo) - CDbl(ImportoIncassato))
                Catch ex As Exception

                End Try

                TotaleInteresse = 0
                Try
                    TotaleInteresse = CDbl(TotaleInteresse)
                Catch ex As Exception

                End Try

            Else

                GigliaTab = GigliaTab & "<tr>"
                GigliaTab = GigliaTab & "<td>" & datadocumento & "</td>"
                GigliaTab = GigliaTab & "<td>" & numerodocumento & "</td>"
                GigliaTab = GigliaTab & "<td>" & datapagamento & "</td>"
                GigliaTab = GigliaTab & "<td>" & importo & "</td>"
                GigliaTab = GigliaTab & "<td>" & ImportoIncassato & "</td>"
                GigliaTab = GigliaTab & "<td>" & importo - ImportoIncassato & "</td>"
                GigliaTab = GigliaTab & "<td>" & Interesse & "</td>"

                GigliaTab = GigliaTab & "</tr>"

                Oldcodicefiscale = codicefiscale
                OldNome = Nome
                Olddatadocumento = datadocumento
                Oldnumerodocumento = numerodocumento
                Olddatapagamento = datapagamento
                OldOperazione = Operazione
                OldImportoIncassato = ImportoIncassato
                OldNomeOspite = NomeOspite
                OldCodiceFiscaleOspite = CodiceFiscaleOspite


                Try
                    TotaleDaIncassare = TotaleDaIncassare + (CDbl(importo) - CDbl(ImportoIncassato))
                Catch ex As Exception

                End Try
                Try
                    TotaleInteresse = TotaleInteresse + CDbl(TotaleInteresse)
                Catch ex As Exception

                End Try
            End If
        Loop


        '

        'Session("PdfDaHTML") = contenuto


        If Rb_WORD.Checked = True Then

            Dim strBody As New StringBuilder()

            strBody.Append("<html xmlns:o='urn:schemas-microsoft-com:office:office' " & _
               "xmlns:w='urn:schemas-microsoft-com:office:word'" & _
               "xmlns='http://www.w3.org/TR/REC-html40'>" & _
               "<head><title>Time</title>")


            

            strBody.Append("<!--[if gte mso 9]>" & _
                "<xml>" & _
                "<w:WordDocument>" & _
                "<w:View>Print</w:View>" & _
                "<w:Zoom>90</w:Zoom>" & _
                "<w:DoNotOptimizeForBrowser/>" & _
                "</w:WordDocument>" & _
                "</xml>" & _
                "<![endif]-->")

            strBody.Append("<style>" & _
                "<!-- /* Style Definitions */" & _
                "@page Section1" & _
                "   {size:8.5in 11.0in; " & _
                "   margin:1.0in 1.25in 1.0in 1.25in ; " & _
                "   mso-header-margin:.5in; " & _
                "   mso-footer-margin:.5in; mso-paper-source:0;}" & _
                " div.Section1" & _
                "   {page:Section1;}" & _
                "-->" & _
                "</style></head>")

            strBody.Append("<body lang=EN-US style='tab-interval:.5in'><div class=Section1>")

            strBody.Append(HTMLDocumenti)

            strBody.Append("</div></body></html>")


            HttpContext.Current.Response.Clear()
            HttpContext.Current.Response.Charset = ""
            HttpContext.Current.Response.ContentType = "application/msword"
            HttpContext.Current.Response.AddHeader("Content-Disposition", "inline;filename=Contratto.doc")

            HttpContext.Current.Response.Write(strBody)
            HttpContext.Current.Response.End()
            HttpContext.Current.Response.Flush()
        Else

            Dim Contenuto As String = HTMLDocumenti

            HTMLDocumenti = Server.UrlEncode(HTMLDocumenti.Replace(vbNewLine, ""))

            Session("PdfDaHTML") = HTMLDocumenti
            Response.Redirect("PdfDaHTml.aspx")
        End If


    End Function



    Public Function SHA1(ByVal StringaDaConvertire As String) As String
        Dim sha2 As New System.Security.Cryptography.SHA1CryptoServiceProvider()
        Dim hash() As Byte
        Dim bytes() As Byte
        Dim output As String = ""
        Dim i As Integer

        bytes = System.Text.Encoding.UTF8.GetBytes(StringaDaConvertire)
        hash = sha2.ComputeHash(bytes)

        For i = 0 To hash.Length - 1
            output = output & hash(i).ToString("x2")
        Next

        Return output
    End Function


    Private Sub ExportPDF(ByRef HTML As String, ByVal Nome As String, ByVal codicefiscale As String, ByVal datadocumento As String, ByVal numerodocumento As String, ByVal datapagamento As String, ByVal Operazione As String, ByVal importo As String, ByVal ImportoIncassato As String, ByVal NomeOspite As String, ByVal CodiceFiscaleOspite As String, ByVal Griglia As String, ByVal TotaleDaIncassare As Double)

        Dim TabParametri As New Cls_Parametri

        TabParametri.LeggiParametri(Session("DC_OSPITE"))

        Dim Appo As New Cls_Solleciti


        Appo.Id = Val(DD_Oneri.SelectedValue)

        Appo.Leggi(Session("DC_OSPITE"))


        ' Leggo il contenuto del file sino alla fine (ReadToEnd)
        Dim contenuto As String = Appo.Testo


        'contenuto = contenuto.Replace("@COMUNEPARENTE2@", ApoC1.Descrizione)
        contenuto = contenuto.Replace("@COGNOMENOME@", Nome)
        contenuto = contenuto.Replace("@CODICEFISCALE@", codicefiscale)


        Dim Ospite As New ClsOspite

        Ospite.Nome = ""
        Ospite.CODICEFISCALE = codicefiscale
        Ospite.LeggiPerCodiceFiscale(Session("DC_OSPITE"), Ospite.CODICEFISCALE)

        If Ospite.Nome <> "" Then

            contenuto = contenuto.Replace("@DATANASCITA@", Format(Ospite.DataNascita, "dd/MM/yyyy"))

            Dim Comune As New ClsComune

            Comune.Provincia = Ospite.ProvinciaDiNascita
            Comune.Comune = Ospite.ComuneDiNascita
            Comune.Leggi(Session("DC_OSPITE"))

            contenuto = contenuto.Replace("@LUOGONASCITA@", Comune.Descrizione)

            Dim ComuneRes As New ClsComune

            ComuneRes.Provincia = Ospite.RESIDENZAPROVINCIA1
            ComuneRes.Comune = Ospite.RESIDENZACOMUNE1
            ComuneRes.Leggi(Session("DC_OSPITE"))



            contenuto = contenuto.Replace("@INTESTATARIOINDIRIZZO@", Ospite.RESIDENZAINDIRIZZO1)
            contenuto = contenuto.Replace("@INTESTATARIOCOMUNE@", ComuneRes.Descrizione)
            contenuto = contenuto.Replace("@INTESTATARIOCAP@", Ospite.RESIDENZACAP1)
        Else
            Dim Parente As New Cls_Parenti

            Parente.Nome = ""
            Parente.CODICEFISCALE = codicefiscale
            Parente.LeggiPerCodiceFiscaleSenzaOspite(Session("DC_OSPITE"), Parente.CODICEFISCALE)



            contenuto = contenuto.Replace("@DATANASCITA@", Format(Parente.DataNascita, "dd/MM/yyyy"))

            Dim Comune As New ClsComune

            Comune.Provincia = Parente.ProvinciaDiNascita
            Comune.Comune = Parente.ComuneDiNascita
            Comune.Leggi(Session("DC_OSPITE"))

            contenuto = contenuto.Replace("@LUOGONASCITA@", Comune.Descrizione)


            Dim ComuneRes As New ClsComune

            ComuneRes.Provincia = Parente.RESIDENZAPROVINCIA1
            ComuneRes.Comune = Parente.RESIDENZACOMUNE1
            ComuneRes.Leggi(Session("DC_OSPITE"))



            contenuto = contenuto.Replace("@INTESTATARIOINDIRIZZO@", Parente.RESIDENZAINDIRIZZO1)
            contenuto = contenuto.Replace("@INTESTATARIOCOMUNE@", ComuneRes.Descrizione)
            contenuto = contenuto.Replace("@INTESTATARIOCAP@", Parente.RESIDENZACAP1)
        End If

        contenuto = contenuto.Replace("@TABELLADOCUMENTI@", Griglia)


        If contenuto.IndexOf("@TABELLADOCUMENTI-") > 0 Then
            Dim Indice As Integer = 0
            Dim Fine As Integer = 0

            Indice = contenuto.IndexOf("@TABELLADOCUMENTI-") + Len("@TABELLADOCUMENTI-")

            Fine = contenuto.IndexOf("@", Indice)

            Dim Appoggio As String = ""

            Appoggio = contenuto.Substring(Indice, Fine - Indice)
            Dim Colonna1 As String = ""
            Dim Colonna2 As String = ""
            Dim Colonna3 As String = ""
            Dim Colonna4 As String = ""
            Dim Colonna5 As String = ""
            Dim Colonna6 As String = ""

            If Appoggio.IndexOf("1=") >= 0 Then
                Dim PosColonna As Integer
                PosColonna = Appoggio.IndexOf("1=") + 2

                Colonna1 = Appoggio.Substring(PosColonna, Appoggio.IndexOf(";", PosColonna) - PosColonna)
            End If

            If Appoggio.IndexOf("2=") > 0 Then
                Dim PosColonna As Integer
                PosColonna = Appoggio.IndexOf("2=") + 2

                Colonna2 = Appoggio.Substring(PosColonna, Appoggio.IndexOf(";", PosColonna) - PosColonna)
            End If

        

            If Appoggio.IndexOf("3=") > 0 Then
                Dim PosColonna As Integer
                PosColonna = Appoggio.IndexOf("3=") + 2

                Colonna3 = Appoggio.Substring(PosColonna, Appoggio.IndexOf(";", PosColonna) - PosColonna)
            End If

            If Appoggio.IndexOf("4=") > 0 Then
                Dim PosColonna As Integer
                PosColonna = Appoggio.IndexOf("4=") + 2

                Colonna4 = Appoggio.Substring(PosColonna, Appoggio.IndexOf(";", PosColonna) - PosColonna)
            End If

            If Appoggio.IndexOf("5=") > 0 Then
                Dim PosColonna As Integer
                PosColonna = Appoggio.IndexOf("5=") + 2

                Colonna5 = Appoggio.Substring(PosColonna, Appoggio.IndexOf(";", PosColonna) - PosColonna)
            End If


            If Appoggio.IndexOf("6=") > 0 Then
                Dim PosColonna As Integer
                PosColonna = Appoggio.IndexOf("6=") + 2

                Colonna6 = Appoggio.Substring(PosColonna, Appoggio.IndexOf(";", PosColonna) - PosColonna)
            End If


            Griglia = Griglia.Replace("<td>Data Documento</td>", "<td>" & Colonna1 & "</td>")
            Griglia = Griglia.Replace("<td>Numero Documento</td>", "<td>" & Colonna2 & "</td>")
            Griglia = Griglia.Replace("<td>Data Pagamento</td>", "<td>" & Colonna3 & "</td>")
            Griglia = Griglia.Replace("<td>Importo Documento</td>", "<td>" & Colonna4 & "</td>")
            Griglia = Griglia.Replace("<td>Incassato</td>", "<td>" & Colonna5 & "</td>")
            Griglia = Griglia.Replace("<td>Da Incassare</td>", "<td>" & Colonna6 & "</td>")

            contenuto = contenuto.Replace("@TABELLADOCUMENTI-" & Appoggio & "@", Griglia)
        End If



        contenuto = contenuto.Replace("@DATADOCUMENTO@", datadocumento)
        contenuto = contenuto.Replace("@NUMERODOCUMENTO@", numerodocumento)
        contenuto = contenuto.Replace("@DATAPAGAMENTO@", datapagamento)
        contenuto = contenuto.Replace("@IMPORTO@", importo)
        contenuto = contenuto.Replace("@IMPORTOINCASSATO@", ImportoIncassato)
        contenuto = contenuto.Replace("@NOMEOSPITE@", NomeOspite)
        contenuto = contenuto.Replace("@CODICEFISCALEOSPITE@", CodiceFiscaleOspite)



        Ospite.Nome = ""
        Ospite.CODICEFISCALE = CodiceFiscaleOspite
        Ospite.LeggiPerCodiceFiscale(Session("DC_OSPITE"), Ospite.CODICEFISCALE)

        If Ospite.Nome <> "" Then

            contenuto = contenuto.Replace("@DATANASCITAOSPITE@", Format(Ospite.DataNascita, "dd/MM/yyyy"))

            Dim Comune As New ClsComune

            Comune.Provincia = Ospite.ProvinciaDiNascita
            Comune.Comune = Ospite.ComuneDiNascita
            Comune.Leggi(Session("DC_OSPITE"))

            contenuto = contenuto.Replace("@LUOGONASCITAOSPITE@", Comune.Descrizione)
        End If




        contenuto = contenuto.Replace("@TOTALEDAINCASSARE@", Format(TotaleDaIncassare, "#,##0.00"))

        'TotaleDaIncassare


        contenuto = contenuto.Replace("é", "&eacute;")
        contenuto = contenuto.Replace("è", "&egrave;")
        contenuto = contenuto.Replace("à", "&agrave;")
        contenuto = contenuto.Replace("ò", "&ograve;")
        contenuto = contenuto.Replace("ù", "&ugrave;")
        contenuto = contenuto.Replace("ì", "&igrave;")

        contenuto = contenuto.Replace("é", "&eacute;")
        contenuto = contenuto.Replace("È", "&Egrave;")
        contenuto = contenuto.Replace("À", "&Aacute;")
        contenuto = contenuto.Replace("Ò", "&Ograve;")
        contenuto = contenuto.Replace("Ù", "&Ugrave;")
        contenuto = contenuto.Replace("Ì", "&Igrave;")
        contenuto = contenuto.Replace("'", "&#39;")

        contenuto = contenuto.Replace("´", "&#180;")
        contenuto = contenuto.Replace("`", "&#96;")

        If Rb_WORD.Checked = True Then
            HTML = HTML & contenuto & "<br clear=all style=""mso-special-character:page-break;page-break-before:always"">"
        Else
            HTML = HTML & contenuto & "<div style=""page-break-before: always;""></div>"
        End If

    End Sub



    Function campodb(ByVal oggetto As Object) As String
        If IsDBNull(oggetto) Then
            Return ""
        Else
            Return oggetto
        End If
    End Function

    Private Function SplitWords(ByVal s As String) As String()
        '
        ' Call Regex.Split function from the imported namespace.
        ' Return the result array.
        '
        Return Regex.Split(s, ";")
    End Function

    Protected Sub Btn_Esci_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles Btn_Esci.Click
        Response.Redirect("Menu_Comunicazioni.aspx")

    End Sub
End Class
