﻿<%@ Page Language="VB" AutoEventWireup="false" Inherits="SelezionaSocieta" CodeFile="SelezionaSocieta.aspx.vb" %>

<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
    <title>Menu Senior</title>
    <asp:PlaceHolder runat="server">
        <%: System.Web.Optimization.Styles.Render("~/Content/AjaxControlToolkit/Styles/Bundle") %>
    </asp:PlaceHolder>
    <link rel="stylesheet" href="ospiti.css?ver=10" type="text/css" />
    <link href="js/jquery.autocomplete.css" rel="stylesheet" type="text/css" />

    <script src="js/jquery-1.5.1.min.js" type="text/javascript"></script>
    <script src="js/jquery.autocomplete.js" type="text/javascript"></script>
    <script src="js/jquery.maskedinput-1.3.min.js" type="text/javascript"></script>
    <script src="js/NoEnter.js" type="text/javascript"></script>

    <script src="Ospitiweb/js/chosen/chosen.jquery.js" type="text/javascript"></script>
    <script src="Ospitiweb/js/chosen/docsupport/prism.js" type="text/javascript" charset="utf-8"></script>
    <link rel="stylesheet" href="Ospitiweb/js/chosen/docsupport/style.css">
    <link rel="stylesheet" href="Ospitiweb/js/chosen/docsupport/prism.css">
    <link rel="stylesheet" href="Ospitiweb/js/chosen/chosen.css">
    <script type="text/javascript">
        function removeElement(divNum) {
            var d = document.getElementById(divNum).parentNode;
            var d2 = document.getElementById(divNum);
            d.removeChild(d2);
        }
    </script>
    <script type="text/javascript">
        $(document).ready(function () {

            $(".chosen-select").chosen({
                no_results_text: "Non trovata",
                display_disabled_options: true,
                allow_single_deselect: true,
                width: "350px",
                placeholder_text_single: "Seleziona Società"
            });
            if (window.innerHeight > 0) { $("#BarraLaterale").css("height", (window.innerHeight - 105) + "px"); } else { $("#BarraLaterale").css("height", (document.documentElement.offsetHeight - 105) + "px"); }
        });
    </script>
</head>
<body>
    <form id="form1" runat="server">
        <asp:ScriptManager ID="ScriptManager1" runat="server" EnableScriptGlobalization="true">
            <Scripts>
                <asp:ScriptReference Path="~/Scripts/AjaxControlToolkit/Bundle" />
            </Scripts>
        </asp:ScriptManager>
        <div>
            <asp:Label ID="Lbl_BarraSenior" runat="server" Text=""></asp:Label>

            <table width="100%" cellpadding="0" cellspacing="0">
                <tr>
                    <td style="width: 160px; background-color: #F0F0F0;"></td>
                    <td>
                        <div class="Titolo">Menu Senior</div>
                        <div class="SottoTitolo">
                            <br />
                            <br />
                        </div>
                    </td>
                    <td style="text-align: right; vertical-align: top;">
                        <div class="DivTasti">
                            <asp:ImageButton ID="IB_ProssimaPagina" runat="server" ImageUrl="~/images/avanti.jpg" />
                        </div>
                    </td>
                </tr>
                <tr>

                    <td style="width: 160px; background-color: #F0F0F0; text-align: center; vertical-align: top;" id="BarraLaterale">
                        <asp:ImageButton ID="Btn_Home" runat="server" BackColor="Transparent" ImageUrl="images/Menu_Indietro.png" ToolTip="Home" /><br />
                        <asp:LinkButton ID="Btn_AllineaDB" runat="server" Text="Allinea DB"></asp:LinkButton>
                        <hr />
                        <asp:LinkButton ID="Btn_StatoFatturazione" runat="server" Text="Stato Fatturazione"></asp:LinkButton>
                        <hr />
                        <asp:LinkButton ID="Btn_Attivo" runat="server" Text="Clienti Attivi (30min)"></asp:LinkButton>
                        <hr />
                        <asp:LinkButton ID="Lnk_AbilitaProcedure" runat="server" Text="Abilita Procedure"></asp:LinkButton>
                    </td>
                    <td colspan="2" style="vertical-align: top;">
                        <label class="LabelCampo">Seleziona Societa : </label>
                        <asp:DropDownList ID="DD_Societa" runat="server" class="chosen-select" Height="20px"></asp:DropDownList>
                        <br />
                        <br />
                        <label class="LabelCampo"><a href="AddSocieta.aspx">Aggiungi Società</a></label>
                        <br />
                        <br />
                        <asp:Label ID="LblSegnalazioni" runat="server" Text=""></asp:Label>

                    </td>
                </tr>

                <tr>
                    <td style="width: 160px; background-color: #F0F0F0;">&nbsp;<br />
                        &nbsp;<br />
                        &nbsp;<br />
                        &nbsp;<br />
                        &nbsp;<br />
                        &nbsp;<br />
                        &nbsp;<br />
                        &nbsp;<br />
                        &nbsp;<br />
                    </td>
                    <td></td>
                    <td></td>
                </tr>
            </table>


        </div>
    </form>
</body>
</html>
