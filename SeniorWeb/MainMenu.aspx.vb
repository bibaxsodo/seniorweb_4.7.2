﻿Imports System
Imports System.Web
Imports Microsoft.VisualBasic
Imports System.Data.OleDb
Imports System.Web.Hosting
Imports System.Data.SqlTypes
Imports System.Net
Imports System.Collections.Generic
Imports System.IO
Imports System.Security.Cryptography.X509Certificates
Imports System.Net.Security
Imports System.Web.Script.Serialization
Imports org.jivesoftware.util
Imports Newtonsoft.Json
Imports Newtonsoft.Json.Linq

Partial Class MainMenu
    Inherits System.Web.UI.Page


    Private Sub DisabilitaCache()

        Response.Cache.SetCacheability(HttpCacheability.NoCache)

        Response.Headers.Add("Cache-Control", "no-cache, no-store")


        Response.Cache.SetExpires(DateTime.UtcNow.AddYears(-1))
    End Sub

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load

        Dim Barra As New Cls_BarraSenior

        Lbl_BarraSenior.Text = Barra.CodiceBarra(Application("SENIOR"), Session("UTENTE"), Page)

        Response.AppendHeader("Refresh", (Session.Timeout * 60) + 5 & "; Url=Login_generale.aspx")

        If Page.IsPostBack = True Then
            Exit Sub
        End If

        'DisabilitaCache()

        If Trim(Session("UTENTE")) = "" Then
            Response.Redirect("Login.aspx")
            Exit Sub
        End If


        Dim k As New Cls_Login

        k.Utente = Session("UTENTE")
        k.LeggiSP(Application("SENIOR"))
        'Lbl_User.Text = ""
        'If k.ABILITAZIONI.IndexOf("<USER>") >= 0 Then
        '    '   Lbl_User.Text = "<a href=""creautenti.aspx""><img title=""Gestione Utenti"" width=32px height=32px src=""images\user.png""></a>"
        'End If



        Dim cnSenior As OleDbConnection
        Dim Max As Long

        cnSenior = New Data.OleDb.OleDbConnection(Application("SENIOR"))

        cnSenior.Open()


        Dim cnTabelle As OleDbConnection

        cnTabelle = New Data.OleDb.OleDbConnection(Session("DC_TABELLE"))

        cnTabelle.Open()

        'ProcedureAbilitate

        Dim cmdProcedureAbilitate As New OleDbCommand()
        Dim ProcedureAbilitate As String = ""

        cmdProcedureAbilitate.CommandText = ("select * from Societa ")
        cmdProcedureAbilitate.Connection = cnTabelle
        Dim VerProcedureAbilitate As OleDbDataReader = cmdProcedureAbilitate.ExecuteReader()
        If VerProcedureAbilitate.Read Then
            ProcedureAbilitate = campodb(VerProcedureAbilitate.Item("ProcedureAbilitate"))
        End If
        VerProcedureAbilitate.Close()
        cnTabelle.Close()

        Dim MyJs As String = ""

        If ProcedureAbilitate <> "" Then

            

            MyJs = "       $(document).ready(function() {   "
            
            MyJs = MyJs & "  $(""#ColomOspiti"").css(""width"",""150px"");"
            MyJs = MyJs & "  $(""#ColomGenerale"").css(""width"",""150px"");"
            MyJs = MyJs & "  $(""#ColomProtocollo"").css(""width"",""150px"");"
            MyJs = MyJs & "  $(""#ColomMailinglist"").css(""width"",""150px"");"
            MyJs = MyJs & "  $(""#ColomAppalti"").css(""width"",""150px"");"

            MyJs = MyJs & "  $(""#ColomComunicazioni"").css(""width"",""150px"");"



            'ColomOspiti
            MyJs = MyJs & " });"
            ScriptManager.RegisterStartupScript(Me, Me.GetType(), "impostabarra", MyJs, True)

            If ProcedureAbilitate.IndexOf("<OSPITI>") < 0 Then
                MyJs = "       $(document).ready(function() {   "
                MyJs = MyJs & "  $(""#ospitiimg"").css(""filter"",""opacity(0.5) drop-shadow(0 0 0 gray)"");"
                MyJs = MyJs & "  $(""#ospititext"").css(""filter"",""opacity(0.5) drop-shadow(0 0 0 gray)"");"
                MyJs = MyJs & "  $('#ColomOspiti a').click(function(){ return false });"
                MyJs = MyJs & " });"
                ScriptManager.RegisterStartupScript(Me, Me.GetType(), "impostabarraOS1", MyJs, True)
            End If


            If ProcedureAbilitate.IndexOf("<GENERALE>") < 0 Then
                MyJs = "       $(document).ready(function() {   "
                MyJs = MyJs & "  $(""#generaleimg"").css(""filter"",""opacity(0.5) drop-shadow(0 0 0 gray)"");"
                MyJs = MyJs & "  $(""#generaletext"").css(""filter"",""opacity(0.5) drop-shadow(0 0 0 gray)"");"
                MyJs = MyJs & "  $('#ColomGenerale a').click(function(){ return false });"
                MyJs = MyJs & " });"
                ScriptManager.RegisterStartupScript(Me, Me.GetType(), "impostabarraOS2", MyJs, True)
            End If


            If ProcedureAbilitate.IndexOf("<PROTOCOLLO>") < 0 Then
                MyJs = "       $(document).ready(function() {   "
                MyJs = MyJs & "  $(""#protocolloimg"").css(""filter"",""opacity(0.5) drop-shadow(0 0 0 gray)"");"
                MyJs = MyJs & "  $(""#protocollotext"").css(""filter"",""opacity(0.5) drop-shadow(0 0 0 gray)"");"
                MyJs = MyJs & "  $('#ColomProtocollo a').click(function(){ return false });"
                MyJs = MyJs & " });"
                ScriptManager.RegisterStartupScript(Me, Me.GetType(), "impostabarraOS3", MyJs, True)
            End If


            If ProcedureAbilitate.IndexOf("<MAILING>") < 0 Then
                MyJs = "       $(document).ready(function() {   "
                MyJs = MyJs & "  $(""#mailinglistimg"").css(""filter"",""opacity(0.5) drop-shadow(0 0 0 gray)"");"
                MyJs = MyJs & "  $(""#mailinglisttext"").css(""filter"",""opacity(0.5) drop-shadow(0 0 0 gray)"");"
                MyJs = MyJs & "  $('#ColomMailinglist a').click(function(){ return false });"
                MyJs = MyJs & " });"
                ScriptManager.RegisterStartupScript(Me, Me.GetType(), "impostabarraOS4", MyJs, True)
            End If

            If ProcedureAbilitate.IndexOf("<APPALTI>") < 0 Then
                MyJs = "       $(document).ready(function() {   "
                MyJs = MyJs & "  $(""#appaltiimg"").css(""filter"",""opacity(0.5) drop-shadow(0 0 0 gray)"");"
                MyJs = MyJs & "  $(""#appaltitext"").css(""filter"",""opacity(0.5) drop-shadow(0 0 0 gray)"");"
                MyJs = MyJs & "  $('#ColomAppalti a').click(function(){ return false });"
                MyJs = MyJs & " });"
                ScriptManager.RegisterStartupScript(Me, Me.GetType(), "impostabarraOS5", MyJs, True)
            End If


            If ProcedureAbilitate.IndexOf("<COMUNICAZIONI>") < 0 Then
                MyJs = "       $(document).ready(function() {   "
                MyJs = MyJs & "  $(""#comunicazioniimg"").css(""filter"",""opacity(0.5) drop-shadow(0 0 0 gray)"");"
                MyJs = MyJs & "  $(""#comunicazionitext"").css(""filter"",""opacity(0.5) drop-shadow(0 0 0 gray)"");"
                MyJs = MyJs & "  $('#ColomComunicazioni a').click(function(){ return false });"
                MyJs = MyJs & " });"
                ScriptManager.RegisterStartupScript(Me, Me.GetType(), "impostabarraOS6", MyJs, True)
            End If

            
            If ProcedureAbilitate.IndexOf("<AMBULATORIALE>") < 0 Then
                MyJs = "       $(document).ready(function() {   "
                MyJs = MyJs & "  $(""#Ambulatorialeimg"").css(""filter"",""opacity(0.5) drop-shadow(0 0 0 gray)"");"
                MyJs = MyJs & "  $(""#Ambulatorialetext"").css(""filter"",""opacity(0.5) drop-shadow(0 0 0 gray)"");"
                MyJs = MyJs & "  $('#ColomAmbulatoriale a').click(function(){ return false });"
                MyJs = MyJs & " });"
                ScriptManager.RegisterStartupScript(Me, Me.GetType(), "impostabarraOS6", MyJs, True)
            End If

            
            
            If ProcedureAbilitate.IndexOf("<RICOVERI>") < 0 Then
                MyJs = "       $(document).ready(function() {   "
                MyJs = MyJs & "  $(""#Ricoveriimg"").css(""filter"",""opacity(0.5) drop-shadow(0 0 0 gray)"");"
                MyJs = MyJs & "  $(""#Ricoveritext"").css(""filter"",""opacity(0.5) drop-shadow(0 0 0 gray)"");"
                MyJs = MyJs & "  $('#ColomRicoveri a').click(function(){ return false });"
                MyJs = MyJs & " });"
                ScriptManager.RegisterStartupScript(Me, Me.GetType(), "impostabarraOS7", MyJs, True)
            End If
        End If
        Try
            If Session("ABILITAZIONI").IndexOf("<ANAGRAFICA>") < 1 Then
                If k.ABILITAZIONI.IndexOf("<REGISTRAZIONE>") < 0 Then

                    MyJs = "       $(document).ready(function() {   "
                    MyJs = MyJs & "  $(""#comunicazioniimg"").css(""filter"",""opacity(0.5) drop-shadow(0 0 0 gray)"");"
                    MyJs = MyJs & "  $(""#comunicazionitext"").css(""filter"",""opacity(0.5) drop-shadow(0 0 0 gray)"");"
                    MyJs = MyJs & "  $('#ColomComunicazioni a').click(function(){ return false });"
                    MyJs = MyJs & " });"
                    ScriptManager.RegisterStartupScript(Me, Me.GetType(), "impostabarraOS6", MyJs, True)
                    MyJs = "       $(document).ready(function() {   "
                    MyJs = MyJs & "  $(""#mailinglistimg"").css(""filter"",""opacity(0.5) drop-shadow(0 0 0 gray)"");"
                    MyJs = MyJs & "  $(""#mailinglisttext"").css(""filter"",""opacity(0.5) drop-shadow(0 0 0 gray)"");"
                    MyJs = MyJs & "  $('#ColomMailinglist a').click(function(){ return false });"
                    MyJs = MyJs & " });"
                    ScriptManager.RegisterStartupScript(Me, Me.GetType(), "impostabarraOS4", MyJs, True)
                End If
            End If
        Catch ex As Exception

        End Try

        Dim cn As OleDbConnection


        cn = New Data.OleDb.OleDbConnection(Session("DC_GENERALE"))

        cn.Open()

        Dim Tipo As String
        Dim Numero As Long
        Dim NumeroRegistrazioneContabile As Long
        Dim DataScadenza As Date
        Dim Importo As Double
        Dim Descrizione As String
        Dim Chiusa As Integer


        Dim DataInizio As Date


        DataInizio = DateAdd(DateInterval.Day, -1, Now)

        Dim cmdData As New OleDbCommand()


        cmdData.CommandText = ("select max(DataOra) from Log_Login where DataOra < ? And Login = ? ")
        cmdData.Connection = cnSenior
        cmdData.Parameters.AddWithValue("@DataScadenza", DateSerial(Year(Now), Month(Now), Day(Now)))
        cmdData.Parameters.AddWithValue("@Utente", Session("UTENTE"))
        Dim VerData As OleDbDataReader = cmdData.ExecuteReader()
        If VerData.Read Then
            If campodb(VerData.Item(0)) <> "" Then
                DataInizio = DateSerial(Year(campodb(VerData.Item(0))), Month(campodb(VerData.Item(0))), Day(campodb(VerData.Item(0))))
            End If
        End If
        VerData.Close()
        cnSenior.Close()




        Dim cmdVer As New OleDbCommand()
        Dim NumeroPost As Long = 0

        cmdVer.CommandText = ("select count(*) from Scadenzario where " & _
                               "DataScadenza >= ? And DataScadenza <= ? ")


        cmdVer.Parameters.AddWithValue("@DataScadenza", DataInizio)
        Dim DataFine As Date = Format(DateAdd(DateInterval.Day, 2, Now), "dd/MM/yyyy")
        cmdVer.Parameters.AddWithValue("@DataScadenza", DataFine)

        cmdVer.Connection = cn

        Dim ReaderVer As OleDbDataReader = cmdVer.ExecuteReader()
        If ReaderVer.Read Then
            NumeroPost = ReaderVer.Item(0)
        End If
        ReaderVer.Close()
        If NumeroPost < 5 Then
            Dim cmd As New OleDbCommand()


            cmd.CommandText = ("select * from Scadenzario where " & _
                                   "DataScadenza >= ? And DataScadenza <= ? ")


            cmd.Parameters.AddWithValue("@DataScadenza", DataInizio)
            cmd.Parameters.AddWithValue("@DataScadenza", DataFine)


            cmd.Connection = cn
            Max = 120
            Dim VerReader As OleDbDataReader = cmd.ExecuteReader()
            Do While VerReader.Read
                Tipo = campodb(VerReader.Item("Tipo"))
                Numero = campodbN(VerReader.Item("Numero"))
                NumeroRegistrazioneContabile = campodbN(VerReader.Item("NumeroRegistrazioneContabile"))
                DataScadenza = campodb(VerReader.Item("DataScadenza"))
                Importo = campodbN(VerReader.Item("Importo"))
                Descrizione = campodb(VerReader.Item("Descrizione"))
                Chiusa = campodbN(VerReader.Item("Chiusa"))
                If (Descrizione <> "" Or NumeroRegistrazioneContabile <> 0) And Chiusa = 0 Then
                    Lbl_PostIT.Text = Lbl_PostIT.Text & "<div id=""codice" & Max & """ style=""text-align:left; position:absolute; left:" & Max & "px; top:300px;"">"
                    Lbl_PostIT.Text = Lbl_PostIT.Text & "<a href=""#"" onclick=""removeElement('codice" & Max & "'); removeElement('TESTO" & Max & "'); "" ><img src=""images/postit.png"" height=""200px"" width=""200px"" alt=""PostIT""/></a></div>"
                    Lbl_PostIT.Text = Lbl_PostIT.Text & "<div id=""TESTO" & Max & """ style=""text-align:center; position:absolute; left:" & Max + 10 & "px; top:340px; width:180px;"">"
                    Lbl_PostIT.Text = Lbl_PostIT.Text & "<p align=""right"">" & DataScadenza & "<p><p align=""left"">Registrazione :" & NumeroRegistrazioneContabile & "<br/><br/>" & Descrizione & "<p></div>"
                End If
                Max = Max + 20
                If Max > 180 Then
                    Exit Do
                End If
            Loop
            VerReader.Close()
            cn.Close()
        Else
            Max = 120
            Lbl_PostIT.Text = Lbl_PostIT.Text & "<div id=""codice" & Max & """ style=""text-align:left; position:absolute; left:" & Max & "px; top:300px;"">"
            Lbl_PostIT.Text = Lbl_PostIT.Text & "<a href=""#"" onclick=""removeElement('codice" & Max & "'); removeElement('TESTO" & Max & "'); "" ><img src=""images/postit.png"" height=""200px"" width=""200px"" alt=""PostIT""/></a></div>"
            Lbl_PostIT.Text = Lbl_PostIT.Text & "<div id=""TESTO" & Max & """ style=""text-align:center; position:absolute; left:" & Max + 10 & "px; top:340px; width:180px;"">"
            Lbl_PostIT.Text = Lbl_PostIT.Text & "<p align=""right""><p align=""left"">Numero Registrazioni in scadenza :" & NumeroPost & "<br/><br/><p></div>"
            Max = Max + 20
        End If


        Dim cnOspiti As OleDbConnection


        cnOspiti = New Data.OleDb.OleDbConnection(Session("DC_OSPITE"))

        cnOspiti.Open()

        Dim cmdOspiti As New OleDbCommand()
        cmdOspiti.CommandText = ("select * from AnagraficaComune where " & _
                       "(DataScadenza >= ? and DataScadenza <= ?)  or (DataScadenza2 >= ? And DataScadenza2 <= ?)   ")
        cmdOspiti.Parameters.AddWithValue("@DataScadenza", DataInizio)
        Dim DataFineT As Date = Format(DateAdd(DateInterval.Day, 2, Now), "dd/MM/yyyy")
        cmdOspiti.Parameters.AddWithValue("@DataScadenza_F", DataFineT)


        cmdOspiti.Parameters.AddWithValue("@DataScadenza2", DataInizio)
        Dim DataFineT2 As Date = Format(DateAdd(DateInterval.Day, 2, Now), "dd/MM/yyyy")
        cmdOspiti.Parameters.AddWithValue("@DataScadenza_F2", DataFineT2)


        cmdOspiti.Connection = cnOspiti
        Dim Anagrafica As OleDbDataReader = cmdOspiti.ExecuteReader()
        Do While Anagrafica.Read
            If campodbN(Anagrafica.Item("CODICEOSPITE")) > 0 Then
                Lbl_PostIT.Text = Lbl_PostIT.Text & "<div id=""codice" & Max & """ style=""text-align:left; position:absolute; left:" & Max & "px; top:300px;"">"
                Lbl_PostIT.Text = Lbl_PostIT.Text & "<a href=""#"" onclick=""removeElement('codice" & Max & "'); removeElement('TESTO" & Max & "'); "" ><img src=""images/postitticket.png"" height=""200px"" width=""200px"" alt=""PostIT""/></a></div>"
                Lbl_PostIT.Text = Lbl_PostIT.Text & "<div id=""TESTO" & Max & """ style=""text-align:center; position:absolute; left:" & Max + 10 & "px; top:320px; width:180px;"">"
                Lbl_PostIT.Text = Lbl_PostIT.Text & "<p align=""right"">" & Format(Now, "dd/MM/yyyy") & "<p><p align=""left"">Scadenza Ticket" & "<br/><br/>" & campodb(Anagrafica.Item("NOME")) & "<p></div>"

            End If
            Max = Max + 20
            If Max > 200 Then
                Exit Do
            End If
        Loop
        Anagrafica.Close()

        Dim cmdOspitiSC As New OleDbCommand()
        cmdOspitiSC.CommandText = ("select * from AnagraficaComune where " & _
                       "(ScadenzaDocumento >= ? and ScadenzaDocumento <= ?)   ")
        cmdOspitiSC.Parameters.AddWithValue("@ScadenzaDocumento", DataInizio)
        Dim DataFineTsc As Date = Format(DateAdd(DateInterval.Day, 2, Now), "dd/MM/yyyy")
        cmdOspitiSC.Parameters.AddWithValue("@ScadenzaDocumentoA", DataFineTsc)


        cmdOspitiSC.Connection = cnOspiti
        Dim AnagraficaSC As OleDbDataReader = cmdOspitiSC.ExecuteReader()
        Do While AnagraficaSC.Read
            If campodbN(AnagraficaSC.Item("CODICEOSPITE")) > 0 Then
                Lbl_PostIT.Text = Lbl_PostIT.Text & "<div id=""codice" & Max & """ style=""text-align:left; position:absolute; left:" & Max & "px; top:300px;"">"
                Lbl_PostIT.Text = Lbl_PostIT.Text & "<a href=""#"" onclick=""removeElement('codice" & Max & "'); removeElement('TESTO" & Max & "'); "" ><img src=""images/postitticket.png"" height=""200px"" width=""200px"" alt=""PostIT""/></a></div>"
                Lbl_PostIT.Text = Lbl_PostIT.Text & "<div id=""TESTO" & Max & """ style=""text-align:center; position:absolute; left:" & Max + 10 & "px; top:320px; width:180px;"">"
                Lbl_PostIT.Text = Lbl_PostIT.Text & "<p align=""right"">" & Format(Now, "dd/MM/yyyy") & "<p><p align=""left"">Scadenza Documento " & "<br/><br/>" & campodb(AnagraficaSC.Item("NOME")) & "<p></div>"
            End If
            Max = Max + 20
            If Max > 220 Then
                Exit Do
            End If

        Loop
        AnagraficaSC.Close()


        Dim cmdOspiti1 As New OleDbCommand()
        cmdOspiti1.CommandText = ("select * from IMPEGNATIVE where " & _
                       "DATAFINE >= ? and DATAFINE <= ?   ")
        cmdOspiti1.Parameters.AddWithValue("@DataScadenza", DataInizio)
        Dim DataFineI2 As Date = Format(DateAdd(DateInterval.Day, 2, Now), "dd/MM/yyyy")
        cmdOspiti1.Parameters.AddWithValue("@DataScadenza_F", DataFineI2)

        cmdOspiti1.Connection = cnOspiti
        Dim Impegnativa As OleDbDataReader = cmdOspiti1.ExecuteReader()
        Do While Impegnativa.Read
            If campodbN(Impegnativa.Item("CODICEOSPITE")) > 0 Then
                Dim Kosp As New ClsOspite
                Kosp.Leggi(Session("DC_OSPITE"), campodbN(Impegnativa.Item("CODICEOSPITE")))

                Lbl_PostIT.Text = Lbl_PostIT.Text & "<div id=""codice" & Max & """ style=""text-align:left; position:absolute; left:" & Max & "px; top:300px;"">"
                Lbl_PostIT.Text = Lbl_PostIT.Text & "<a href=""#"" onclick=""removeElement('codice" & Max & "'); removeElement('TESTO" & Max & "'); "" ><img src=""images/postitimpegnativa.png"" height=""200px"" width=""200px"" alt=""PostIT""/></a></div>"
                Lbl_PostIT.Text = Lbl_PostIT.Text & "<div id=""TESTO" & Max & """ style=""text-align:center; position:absolute; left:" & Max + 10 & "px; top:320px; width:180px;"">"
                Lbl_PostIT.Text = Lbl_PostIT.Text & "<p align=""right"">" & Format(Impegnativa.Item("DATAFINE"), "dd/MM/yyyy") & "<p><p align=""left"">Scadenza Impegnativa" & "<br/><br/>" & Kosp.Nome & "<p></div>"
            End If
            Max = Max + 20
            If Max > 240 Then
                Exit Do
            End If

        Loop
        Impegnativa.Close()




        Dim cmdOspiti2 As New OleDbCommand()
        cmdOspiti2.CommandText = ("select * from DATIISE where " & _
                       "DATAFINE >= ? and DATAFINE <= ?   ")
        cmdOspiti2.Parameters.AddWithValue("@DataScadenza", DataInizio)
        cmdOspiti2.Parameters.AddWithValue("@DataScadenza_F", DataFineI2)

        cmdOspiti2.Connection = cnOspiti
        Dim Ise As OleDbDataReader = cmdOspiti2.ExecuteReader()
        Do While Ise.Read
            If campodbN(Ise.Item("CODICEOSPITE")) > 0 Then
                Dim Kosp As New ClsOspite
                Kosp.Leggi(Session("DC_OSPITE"), campodbN(Ise.Item("CODICEOSPITE")))


                Lbl_PostIT.Text = Lbl_PostIT.Text & "<div id=""codice" & Max & """ style=""text-align:left; position:absolute; left:" & Max & "px; top:300px;"">"
                Lbl_PostIT.Text = Lbl_PostIT.Text & "<a href=""#"" onclick=""removeElement('codice" & Max & "'); removeElement('TESTO" & Max & "'); "" ><img src=""images/postitimpegnativa.png"" height=""200px"" width=""200px"" alt=""PostIT""/></a></div>"
                Lbl_PostIT.Text = Lbl_PostIT.Text & "<div id=""TESTO" & Max & """ style=""text-align:center; position:absolute; left:" & Max + 10 & "px; top:320px; width:180px;"">"
                Lbl_PostIT.Text = Lbl_PostIT.Text & "<p align=""right"">" & Format(Ise.Item("DATAFINE"), "dd/MM/yyyy") & "<p><p align=""left"">Scadenza Ise" & "<br/><br/>" & Kosp.Nome & "<p></div>"
            End If
            Max = Max + 20
            If Max > 240 Then
                Exit Do
            End If

        Loop
        Ise.Close()

        cnOspiti.Close()

        Try

            Dim Param As New Cls_Parametri

            Param.LeggiParametri(Session("DC_OSPITE"))

            If (Request.Cookies("V1MovimentiEpersonam") IsNot Nothing) Then
                If Request.Cookies("V1MovimentiEpersonam").Value.ToUpper = "1" Then
                    Param.V1MovimentiEpersonam = 0
                End If
            End If

            If Param.V1MovimentiEpersonam = 1 Then
                StartImport(Max)

                If (Request.Cookies("V1MovimentiEpersonam") IsNot Nothing) Then
                    Dim cookie As HttpCookie = HttpContext.Current.Request.Cookies("V1MovimentiEpersonam")

                    cookie.Value = 1

                    cookie.Expires = DateTime.Now.AddHours(2)

                    Response.Cookies.Add(cookie)


                Else

                    Dim aCookie As New HttpCookie("V1MovimentiEpersonam")

                    aCookie.Value = 1

                    aCookie.Expires = DateTime.Now.AddHours(2)

                    Response.Cookies.Add(aCookie)
                End If
            End If
        Catch ex As Exception

        End Try


    End Sub
    Function campodbN(ByVal oggetto As Object) As Double


        If IsDBNull(oggetto) Then
            Return 0
        Else
            Return oggetto
        End If
    End Function
    Function campodb(ByVal oggetto As Object) As String


        If IsDBNull(oggetto) Then
            Return ""
        Else
            Return oggetto
        End If
    End Function








    Protected Sub Btn_Home_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles Btn_Home.Click
        If Session("UTENTE") = "" Then
            Response.Redirect("Login.aspx")
            Exit Sub
        End If

        If Session("UTENTE").ToString.IndexOf("<") > 0 Then
            Session("UTENTE") = Mid(Session("UTENTE"), 1, Session("UTENTE").ToString.IndexOf("<"))
            Response.Redirect("Menu_Societa.aspx")
        Else
            Response.Redirect("Login.aspx")
        End If

    End Sub




    Private Sub StartImport(ByVal Max As Integer)



        Dim cn As OleDbConnection
        Dim Trovato As Boolean = False
        Dim Token As String

        cn = New Data.OleDb.OleDbConnection(Session("DC_OSPITE"))

        cn.Open()


        Token = LoginPersonam(Context)

        BusinessUnit(Token, Context, Max)
        cn.Close()
    End Sub


    Private Function LoginPersonam(ByVal context As HttpContext) As String
        Try


            Dim request As New NameValueCollection


            request.Add("client_id", "98217ca6670c660e22f42d58e231d4e56c84fb34e216b0f66f1f30284fd3ec9d")
            request.Add("client_secret", "5570a684f69ca74d75d16bba669c156ec092eccb4111d0974adec3edb2fa4d6f")




            request.Add("username", context.Session("EPersonamUser"))



            request.Add("password", context.Session("EPersonamPSW"))
            System.Net.ServicePointManager.ServerCertificateValidationCallback = AddressOf CertificateHandler


            Dim client As New WebClient()

            Dim result As Object = client.UploadValues("https://api.e-personam.com/api/v1.0/token/password", "POST", request)


            Dim stringa As String = Encoding.Default.GetString(result)


            Dim serializer As JavaScriptSerializer


            serializer = New JavaScriptSerializer()




            Dim s As Autentificazione = serializer.Deserialize(Of Autentificazione)(stringa)



            Return s.access_token
        Catch ex As Exception
            Return ""
        End Try
    End Function


    Public Class Autentificazione
        Public access_token As String
        Public expires_in As String
        Public scope As String
        Public token_type As String
        Public refresh_token As String
    End Class

    Private Shared Function CertificateHandler(ByVal sender As Object, ByVal certificate As X509Certificate, ByVal chain As X509Chain, ByVal SSLerror As SslPolicyErrors) As Boolean
        Return True
    End Function

    Private Function BusinessUnit(ByVal Token As String, ByVal context As HttpContext, ByVal Max As Integer) As Long
        Dim rawresp As String

        Dim Param As New Cls_Parametri

        Param.LeggiParametri(context.Session("DC_OSPITE"))

        BusinessUnit = 0
        Try
            Dim client As HttpWebRequest = WebRequest.Create("https://api.e-personam.com/api/v1.0/business_units")


            client.Method = "GET"
            client.Headers.Add("Authorization", "Bearer " & Token)
            client.ContentType = "Content-Type: application/json"

            Dim reader As StreamReader
            Dim response As HttpWebResponse = Nothing

            response = DirectCast(client.GetResponse(), HttpWebResponse)

            reader = New StreamReader(response.GetResponseStream())

            rawresp = reader.ReadToEnd()


            If Param.DebugV1 = 1 Then
                Dim NomeFile As String

                NomeFile = HostingEnvironment.ApplicationPhysicalPath() & "\Public\Json_" & Format(Now, "yyyyMMdd") & ".xml"
                Dim tw As System.IO.TextWriter = System.IO.File.CreateText(NomeFile)
                tw.Write(rawresp)
                tw.Close()
            End If

            Dim jArr As JArray = JArray.Parse(rawresp)


            For Each jResults As JToken In jArr

                BusinessUnit = Val(jResults.Item("id").ToString)

                If ImportaMovimenti(BusinessUnit, Token, Max) Then Exit For
                REM Exit For
            Next
        Catch ex As Exception
            REM ClientScript.RegisterClientScriptBlock(Me.GetType(), "Errore", "alert('" & ex.Message & "');", True)
            Exit Function
        End Try

    End Function

     


    Private Function ImportaMovimenti(ByVal BusinessID As Integer, ByVal Token As String, ByVal Max As Integer) As Boolean
        ImportaMovimenti = False
        Try

            Dim request As New NameValueCollection
            Dim rawresp As String
            Dim cn As OleDbConnection




            Dim Param As New Cls_Parametri

            Param.LeggiParametri(Context.Session("DC_OSPITE"))


            Dim DataFatt As String

            If Param.MeseFatturazione > 9 Then
                DataFatt = Param.AnnoFatturazione & "/" & Param.MeseFatturazione
            Else
                DataFatt = Param.AnnoFatturazione & "/0" & Param.MeseFatturazione
            End If

            System.Net.ServicePointManager.ServerCertificateValidationCallback = AddressOf CertificateHandler


            Dim client As HttpWebRequest = WebRequest.Create("https://api.e-personam.com/api/v1.0/business_units/" & BusinessID & "/movements/" & DataFatt)

            client.Method = "GET"
            client.Headers.Add("Authorization", "Bearer " & Token)
            client.ContentType = "Content-Type: application/json"


            Dim reader As StreamReader
            Dim response As HttpWebResponse = Nothing

            response = DirectCast(client.GetResponse(), HttpWebResponse)

            reader = New StreamReader(response.GetResponseStream())


            rawresp = reader.ReadToEnd()



            Dim LastMov As String

            Dim jResults As JArray = JArray.Parse(rawresp)

            Dim SalvaPrecedenteTipo As String = ""
            Dim SalvaPrecedenteData As String = ""

            cn = New Data.OleDb.OleDbConnection(Session("DC_OSPITE"))

            cn.Open()


            For Each jTok As JToken In jResults
                rawresp = rawresp & jTok.Item("guest_movements").ToString
                For Each jTok1 As JToken In jTok.Item("guest_movements").Children
                    Dim CodiceFiscale As String
                    Dim ward_id As Integer
                    Dim Fullname As String


                    CodiceFiscale = jTok1.Item("cf").ToString()
                    Fullname = jTok1.Item("fullname").ToString()




                    Dim Ospite As New ClsOspite



                    SalvaPrecedenteTipo = ""

                    Ospite.CODICEFISCALE = CodiceFiscale
                    Ospite.LeggiPerCodiceFiscale(Context.Session("DC_OSPITE"), CodiceFiscale)

                    If CodiceFiscale = "NDRLJA44H60A343O" Then
                        CodiceFiscale = "NDRLJA44H60A343O"
                    End If
                    REM If Ospite.CodiceOspite > 0 Then
                    For Each jTok2 As JToken In jTok1.Item("movements").Children




                        Dim idEpersonam As String = ""
                        Try
                            idEpersonam = jTok2.Item("id").ToString.ToUpper
                        Catch ex As Exception

                        End Try

                        Dim Presente As Boolean = False

                        Dim CmdVerMovimenti_EPersonam As New OleDbCommand

                        CmdVerMovimenti_EPersonam.Connection = cn
                        CmdVerMovimenti_EPersonam.CommandText = ("select * from Movimenti_EPersonam Where IdEpersonam = ? ")
                        CmdVerMovimenti_EPersonam.Parameters.AddWithValue("@idepersonam", idEpersonam)
                        Dim ReadVerMovimenti_EPersonam As OleDbDataReader = CmdVerMovimenti_EPersonam.ExecuteReader
                        If ReadVerMovimenti_EPersonam.Read Then
                            Presente = True
                        End If
                        ReadVerMovimenti_EPersonam.Close()
                        If Presente = False Then
                            If Lbl_PostIT.Text.IndexOf("codice99") < 0 Then
                                Lbl_PostIT.Text = Lbl_PostIT.Text & "<div id=""codice99"" style=""text-align:left; position:absolute; left:" & Max & "px; top:300px;"">"
                                Lbl_PostIT.Text = Lbl_PostIT.Text & "<a href=""#"" onclick=""removeElement('codice99'); removeElement('TESTO99'); "" ><img src=""images/postit.png"" height=""200px"" width=""200px"" alt=""PostIT""/></a></div>"
                                Lbl_PostIT.Text = Lbl_PostIT.Text & "<div id=""TESTO99"" style=""text-align:center; position:absolute; left:" & Max + 10 & "px; top:340px; width:180px;"">"
                                Lbl_PostIT.Text = Lbl_PostIT.Text & "<p align=""right""><p align=""left"">Ci sono modifiche in epersonam<br/><br/><p></div>"
                                ImportaMovimenti = True
                            End If
                            Exit Function
                        End If
                    Next
                    REM End If
                Next
            Next



            cn.Close()



        Catch ex As Exception

        End Try


    End Function


End Class
